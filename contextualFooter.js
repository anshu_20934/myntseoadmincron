"use strict";
var request = require('request'),
    mysql   = require('mysql'),
    redis   = require("redis"),
    config  = require('./lib/config'),
    async   = require('async'),
         _  = require('underscore'),
        Q   = require('q'),
    crypto  = require('crypto'),
    adminKeywords = [],
    searchStaticFooter,
    pdpStaticFooter,
    ShopStaticFooter,
    staticStaticFooter,
    homeStaticFooter,
    completedKeywords = 0,
    searchServiceReq = 0,
    pdpServiceReq = 0,
    promises = {},
    footerLinksPromises = [],
    superPromises = [],
    redisClient,
    failure = [],
    databasePromise,
    sitemapkeywords = [],
    search_vol,
    onlineShoppingFooterEntry = {name:"Online Shopping", linkUrl : ""};    

// get all the search keywords from db.


var connection = mysql.createConnection({
    host    : config.mysql_ro.host,
    user    : config.mysql_ro.username,
    password: config.mysql_ro.password,
    database: config.mysql_ro.database,
    debug   : false,
    insecureAuth: true
});


var staticPages = ['contactus','aboutus','careers','faqs','privacypolicy','termsofuse'];

var  BRAND_AT_SIZE= 17,
     BRAND_FOOTER_SIZE = 17,
     ARTICLETYPE_FOOTER_SIZE =17,
     SEARCH_CACHE_PREFIX = 'search';

var FOOTER_TYPE_MAP = {
    1 :'brandFooter',
    2 :'articleTypeFooter',
    3 :'brandATFooter'
};

redisClient = redis.createClient(config.footer_redis_rw.port, config.footer_redis_rw.host, config.footer_redis_rw);


var REDIS_FOOTER_PREFIX = "seofooter:";

connection.connect();
function titleCase(str) {
    return str.replace(/\w\S*/g,function(txt){
        return txt.charAt(0).toUpperCase()+txt.substr(1).toLowerCase();
    });
}
function generateFooterFromDB(rows) {
    var footer = {};
    _.each(rows,function(row) {
        if(footer[FOOTER_TYPE_MAP[row.type]] !== undefined){
            footer[FOOTER_TYPE_MAP[row.type]][row.link_url] = row.link_name;
        } else {
            footer[FOOTER_TYPE_MAP[row.type]] = {};
            footer[FOOTER_TYPE_MAP[row.type]][row.link_url] = row.link_name;
        }
    });
    return footer;
}
function getStaticFooter(type,callback) {
    var footer = {};
    async.series([function(callback){
        connection.query('SELECT link_name,link_url,type from adseo_pagefooter where pagerule_id =(select id from adseo_pagerule where url ="'+type+'")', function(err, rows) {
        if(err) {
            console.log(err.code);
            throw err;
        } 
        footer = generateFooterFromDB(rows);
        callback();
    });
    },function(callback){
    connection.query('SELECT defined_footer from adseo_pagerule where url ="'+type+'"', function(err, row) {
        if(err) {
            
             console.log(err.code);
            throw err;
        }
        footer.global_override=row[0]?row[0].defined_footer:0;
        callback();
    });
    }],function(err){
        callback(null,footer);
    });
}

function getFooterLinks(ruleId) {
    var deferred = Q.defer(),
        footerLinks = {},
        footer;
    connection.query('SELECT link_name,link_url,type from adseo_pagefooter where  pagerule_id='+ruleId+'',function(err,rows){
        if(err) {
            deferred.reject(err);
            console.log(err.code);
        }
        footer = generateFooterFromDB(rows);
        deferred.resolve(footer);
    });
    return deferred.promise;
}

databasePromise = (function() {
    var deferred = Q.defer();
    async.series({
        listStaticFooter: function(callback){
            getStaticFooter('http://search.search/',function(err,result){
                if(err) {
                    callback(err);
                    return;
                }
                publishToRedis('search.search',buildStaticFooter(result));
                callback(null,result);
                });
       },
       pdpStaticFooter: function(callback){
            getStaticFooter('http://pdp.pdp/',function(err,result){
                if(err) {
                    callback(err);
                    return;
                }
	            publishToRedis('pdp.pdp',buildStaticFooter(result));
                callback(null,result);
            });
       },
       homeStaticFooter: function(callback){
           getStaticFooter('http://home.home/',function(err,result){
               if(err) {
                   callback(err);
                   return;
               }
               publishToRedis('home.home',buildStaticFooter(result));
               callback(null,result);
           });
       },
       ShopStaticFooter: function(callback){
            getStaticFooter('http://shop.shop/',function(err,result){
                if(err) {
                    callback(err);
                    return;
                }
                publishToRedis('shop.shop',buildStaticFooter(result));
                callback(null,result);
            });
        },
        staticStaticFooter: function(callback){
            getStaticFooter('http://static.static/',function(err,result){
                if(err) {
                    callback(err);
                    return;
                }
                publishToRedis('static.static',buildStaticFooter(result));
                callback(null,result);
            });
        },
        
        search_vol: function(callback){
        	sitemapkeywords=[];
    		connection.query('SELECT keyword as kk,search_volume from adseo_sitemapkeyword where keyword != ""  && search_volume>6000 order by search_volume desc ', function(err, rows2) {
            	if(err) {
            	   console.log(err.code);
        		   throw err;
                }
                _.each(rows2,function(row2) {
    			    sitemapkeywords.push(row2.kk);
    			});
                callback(null,sitemapkeywords);
        	});
    	}
    },
    function(err,results) {
        searchStaticFooter = results.listStaticFooter;
        pdpStaticFooter = results.pdpStaticFooter;
        ShopStaticFooter = results.ShopStaticFooter;
        staticStaticFooter = results.staticStaticFooter;
        homeStaticFooter = results.homeStaticFooter;
        var tempPromise;

        connection.query('SELECT url,id,defined_footer from adseo_pagerule where url != " "', function(err, rows) {
            if(err) {
                console.log(err.code);
                throw err;
            }
            _.each(rows,function(row) {
                var matches,
                keyword;
                matches = row.url.match(/.*?com\/(.*)/);

                if(matches) {
                   keyword = matches[1];
                }
                tempPromise = getFooterLinks(row.id);
                footerLinksPromises.push(tempPromise);
                tempPromise.then(function(footer){
                    if(keyword !== undefined) {
                        adminKeywords.push(keyword);
                        superPromises.push(fireRequest(keyword,footer,row.defined_footer));
                    }
                },function(err){
                    console.log(err);
                });
            });
            Q.all(footerLinksPromises).then(function(result){
                deferred.resolve();
            },function(err){
                deferref.reject();
                console.log(err);
            });
        });
    });
    return deferred.promise;
})();

function buildStaticFooter(data){
   if(_.isEmpty(data)) return false;
   var finalFooter = [];
   var footerTypes = ['brandFooter','articleTypeFooter','brandATFooter'];
   _.each(footerTypes,function(footerType) {
  	var tempFooter = [];
	_.each(data[footerType],function(name,linkUrl) {
	   tempFooter.push({name:name,linkUrl:linkUrl});
	});
	finalFooter.push(tempFooter);
   });
   finalFooter[2].pop();
   finalFooter[2].unshift(onlineShoppingFooterEntry);
   return finalFooter;
}

function sortObjectByValue(obj){
    var sortable = [];

    for(var key in obj){
        sortable.push([key,obj[key]]);
    }
    sortable.sort(function(a,b){
        return b[1]-a[1];
    });
    
    return sortable;
}

function parseSearchResponse(data,key){
//console.log('data ------', data.response1.filters);
    var brands = data.response1.filters.brands_filter_facet || false,
        articleTypes = data.response1.filters.global_attr_article_type_facet || false,
 //   var brands = data.response1.filters && data.response1.filters.brands_filter_facet || false,
   //     articleTypes =data.response1.filters &&  data.response1.filters.global_attr_article_type_facet || false,
	topBrand,
        brandSets = [],
        articleTypeSets = [],
        tempvar=0,
        topArticleType,
        result;

    if(brands) {
        brandSets = sortObjectByValue(brands);
        topBrand = brandSets[0][0];
    }

    if(articleTypes) {
        articleTypeSets = sortObjectByValue(articleTypes);
        topArticleType = articleTypeSets[0][0];
    }
    return { 
        brands: brandSets,
        articleTypes: articleTypeSets,
        topBrand: topBrand,
        topArticleType: topArticleType
    };
}

function solrRequestAndParse(keyword,callback){
    if(promises[keyword]) {
        promises[keyword].then(function(value){
            callback(null,value);
        },function(err){
            callback(err,null);
        });
    } else {
        ++searchServiceReq;
        promises[keyword] = (function(){
            var deferred = Q.defer();
            request.get({
                uri: 'http://charles.myntra.com/search-service/searchservice/search/getquery/'+keyword+'?is_facet=true',
                headers: {
                    'content-type': 'application/json',
                    'Accept' : 'application/json'
                }
               // body: JSON.stringify({
                  //  "query": keyword,
                  //  "return_docs": true,
                  //  "is_facet": true
                //})
            },function(err,response,body){
                if(err) {
                    deferred.reject(err)
                }
                try {
                    var solrResponse = JSON.parse(body).data.queryResult,
                        parsedData;
                } catch(e) {
                    console.log(e.message);
                    console.log(keyword);
                    failure.push(keyword);
                    solrResponse = {};
                }
                if(solrResponse.response1 == undefined){
                    deferred.reject(0);
                    console.log('error in solr response for keyword ', keyword);
                } else {
                    parsedData = parseSearchResponse(solrResponse,keyword);
                    deferred.resolve(parsedData);
                }
            });
            return deferred.promise;
        })();
        promises[keyword].then(function(data){
            callback(null,data);
        },function(err){
            callback(err,null);
        });
    }
}

function styleServiceRequest(styleId,callback){
    ++pdpServiceReq;
    request.get({
        uri: 'http://www.myntra.com/myntapi/style/style-service/pdpservice/style/pdp/'+styleId,
        headers: {
            'Content-Type': 'application/json',
            'Accept' : 'application/json'
        },
    },function(err,res,body){
        var isJson =  true,
        parsedBody;
        try {
            parsedBody = JSON.parse(body);
        } catch(e) {
            err = "body not a Json";
            parsedBody = {};
            isJson = false;
        }
        callback(err,res,parsedBody,styleId,isJson);
    });
}

function popFooterLinks(footerLinks) {

    var tempArr = [];
    _.each(footerLinks,function(val){
        tempArr.push(val[0]);
    });

    return tempArr;
}

function getRandomLinks(links) {
   return _.sample(links,17);
}


function getFooter(footerLinks,footerType,staticFooter,definedFooter,override,keyword,global_override) {
    var footerSize,
        footerLinksDiff,
        definedFooter =  definedFooter || {},
        finalFooter = [],
        randomLinks,
        footer,
        contextual_footer=[];
    
    footerLinks = popFooterLinks(footerLinks);

    if(footerLinks.indexOf(keyword) !== -1) {
        footerLinks.splice(footerLinks.indexOf(keyword),1);
    }

    randomLinks = footerLinks.map(function(item){return item.toLowerCase();});

    switch(footerType) {
        case 0: 
            footerSize = BRAND_AT_SIZE;
        break;
        case 1:
            footerSize = BRAND_FOOTER_SIZE;
        break;
        case 2:
            footerSize = ARTICLETYPE_FOOTER_SIZE;
        break;
    }

    footerLinksDiff = footerSize - footerLinks.length;
    
    var indices=[];
    for(var i=0;i<randomLinks.length;i++){
        if(sitemapkeywords.indexOf(randomLinks[i])!=-1){
            indices.push(sitemapkeywords.indexOf(randomLinks[i]));
        }
    }
    indices.sort(function(a,b) {
        return a-b;
    }).slice(0,17);
    for(var i=0;i<indices.length;i++){
        contextual_footer.push(sitemapkeywords[indices[i]]);
    }
   //console.log(contextual_footer);
    if(global_override){
       footer = _.union(_.keys(definedFooter),_.keys(staticFooter)).slice(0,17);    
    } else {
        if(footerLinksDiff <= 0) {
         footer = override ? _.union(_.keys(definedFooter),contextual_footer,_.keys(staticFooter)).slice(0,17) : _.union(contextual_footer,_.keys(staticFooter)).slice(0,17);
        } else {
            if(override) {
                footer = _.union(_.keys(definedFooter), contextual_footer, _.keys(staticFooter)).slice(0,17);
            } else {
                 footer = _.union(contextual_footer, _.keys(staticFooter)).slice(0,17);
            }
        }
    }

    _.map(footer,function(link){
        var key = definedFooter[link] || staticFooter[link] || link;
        var tempHash = {};
        tempHash.name = titleCase(key.replace(/-/g,' '));
        tempHash.linkUrl = link.replace(/\s/g,'-').toLowerCase();
        finalFooter.push(tempHash);
    });
    
	return finalFooter;
}

function getBrandATFooter(topBrand,topArticleType,brands,articleTypes,definedFooter,override,staticFooter,global_override) {
    var definedFooter =  definedFooter || {},
        brandATFooter = [],
        finalFooter,
        finalFooterMap = [],
        footerCount = 17,
        footerDiff,
        staticFooterSample;
  

    _.each(articleTypes,function(link){
        brandATFooter.push([topBrand+'-'+link[0],link[1]]);
    });
    _.each(brands,function(link){
        brandATFooter.push([link[0]+'-'+topArticleType,link[1]]);
    });


    brandATFooter.sort(function(a,b) {
        return b[1]-a[1];
    });
    
    brandATFooter =  _.unique(popFooterLinks(brandATFooter));

    footerDiff = footerCount - brandATFooter.length;
    var indices=[];
    for(var i=0;i<brandATFooter.length;i++){
        var x = titleCase(brandATFooter[i].replace(/-/g,' ')).toLowerCase();
        //console.log(x);
        if(sitemapkeywords.indexOf(x)!=-1){
            indices.push(sitemapkeywords.indexOf(x));
        }
    }
    //console.log(indices);
    indices.sort(function(a,b) {
        return a-b;
    }).slice(0,footerCount);
    //console.log(indices);
    var contextual_footer = [];
    for(var i=0;i<indices.length;i++){
        contextual_footer.push(sitemapkeywords[indices[i]]);
    }
   //console.log(contextual_footer); 
    if(global_override){
       finalFooter = _.union(_.keys(definedFooter),_.keys(staticFooter));
    } else {   
        if(footerDiff <= 0 ) {
            finalFooter = override ? _.union(_.keys(definedFooter), contextual_footer,_.keys(staticFooter)) : _.union(contextual_footer,_.keys(staticFooter));
        } else {
            if(override) {
                finalFooter = _.union(_.keys(definedFooter), contextual_footer ,_.keys(staticFooter));
            } else {
                finalFooter = _.union(contextual_footer,_.keys(staticFooter));
            }
        }
    }
    finalFooter = finalFooter.slice(0,footerCount);
    _.map(finalFooter,function(link){
        var key = definedFooter[link] || staticFooter[link] || link;
        var tempHash = {};
        key = key.replace(/-/g,' ');
        tempHash.name = titleCase(key);
        tempHash.linkUrl = link.replace(/\s/g,'-').toLowerCase(); 
        finalFooterMap.push(tempHash);
    });
    
    var foundOnlineLink= _.find(finalFooterMap, function(footerEle){
        return footerEle['name'] === "Online Shopping";
    });
    if(!foundOnlineLink){
        finalFooterMap.pop();
        finalFooterMap.unshift(onlineShoppingFooterEntry);   
    }
    return finalFooterMap;
}

function deepRequest(parsedData,preDefined,footerOverride,superDeferred,keyword) {
    var brandsCount,
        articleTypeCount,
        topBrand,
        topArticleType,
        brandFooter,
        articleTypeFooter,
        brandATFooter,
        brands,
        articleTypes,
        brandsPromise,
        articleTypesPromise,
        allPromise,
        fallbackFooter,
        allPromises = [];
    brandsCount = parsedData.brands.length;
    articleTypeCount = parsedData.articleTypes.length;
    topBrand = parsedData.topBrand;
    topArticleType =  parsedData.topArticleType;

    if(staticPages.indexOf(keyword)!=-1)
        fallbackFooter = staticStaticFooter;
    else if(keyword.indexOf('shop')!=-1)
        fallbackFooter = ShopStaticFooter
    else if(!isNaN(keyword))
        fallbackFooter = pdpStaticFooter;
    else fallbackFooter = searchStaticFooter;

    if(brandsCount > 1) {
        brands = parsedData.brands;
        brandFooter = getFooter(brands,1,fallbackFooter.brandFooter,preDefined.brandFooter,footerOverride,keyword,!!fallbackFooter.global_override);
    } else if(brandsCount == 1) {
        brandsPromise = (function(){
            var deferred = Q.defer();
            solrRequestAndParse(topArticleType, function(err,parsedData) {
                if(err) deferred.reject(err);
                if(err == 0) {
                    deferred.resolve();
                } else { 
                    brands = parsedData.brands;
                    brandFooter = getFooter(brands,1,fallbackFooter.brandFooter,preDefined.brandFooter,footerOverride,keyword,!!fallbackFooter.global_override);
                    deferred.resolve(brandFooter);
                }
            });
            return deferred.promise;
        })();
    } 

    if(articleTypeCount > 1) {
        articleTypes = parsedData.articleTypes; 
        articleTypeFooter =  getFooter(articleTypes,2,
                fallbackFooter.articleTypeFooter,
                preDefined.articleTypeFooter,
                footerOverride,
                keyword,
                !!fallbackFooter.global_override
                );
    } else if(articleTypeCount == 1) {
        articleTypesPromise = (function(){        
            var deferred = Q.defer();
            solrRequestAndParse(topBrand, function(err,parsedData) {
                if(err) deferred.reject(err);
                if(err == 0) {
                    deferred.resolve();
                } else {
                    articleTypes = parsedData.articleTypes;
                    articleTypeFooter = getFooter(articleTypes,2,
                        fallbackFooter.articleTypeFooter,
                        preDefined.articleTypeFooter,
                        footerOverride,
                        keyword,
                        !!fallbackFooter.global_override
                    );
                    deferred.resolve(articleTypeFooter);
                }
            });
            return deferred.promise;        
        })();
    }

    if(articleTypesPromise !== undefined) {
        allPromises.push(articleTypesPromise);
    } 

    if(brandsPromise !== undefined) {
        allPromises.push(brandsPromise);
    }

    if(allPromises.length > 0) {
        allPromise = Q.all(allPromises);
        allPromise.then(function(result){
            brandATFooter = getBrandATFooter(topBrand,
                topArticleType,
                brands,
               articleTypes,
                preDefined.brandATFooter,
                footerOverride,
                fallbackFooter.brandATFooter,
               !!fallbackFooter.global_override
              );
            ++completedKeywords;
            console.log(completedKeywords);
            superDeferred.resolve(brandATFooter);
            //console.log('--------------------------',brandFooter,articleTypeFooter,brandATFooter);
            publishToRedis(keyword,[brandFooter,articleTypeFooter,brandATFooter]);
       	   //publishToRedis(keyword, null);
	 },function(err){
            superDeferred.reject(err);
            console.log(err);
        });
    } else {
       	brandATFooter = getBrandATFooter(topBrand,
                topArticleType,
                brands,
                articleTypes,
                preDefined.brandATFooter,
                footerOverride,
                fallbackFooter.brandATFooter,
                !!fallbackFooter.global_override
              );
        ++completedKeywords;
        console.log(completedKeywords);
        superDeferred.resolve(brandATFooter);
        //publishToRedis(keyword, null);
	publishToRedis(keyword,[brandFooter,articleTypeFooter,brandATFooter]);
        //console.log('--------------------------',brandFooter,articleTypeFooter,brandATFooter);
    }
}

function publishToRedis(keyword,data){
    console.log('',data);
    console.log(keyword);
    var keyword = keyword.toLowerCase().split('-').sort().join('-');
    var key = REDIS_FOOTER_PREFIX + crypto.createHash('md5').update(keyword).digest("hex");
    redisClient.set(key,JSON.stringify(data),function(err){
        if(err) {
            console.log("setting key got problem ", key);
            console.log(err);
        }
    });

}

function fireRequest(keyword,preDefined,footerOverride) {
    var parsedData,
        isPDP,
        styleId,
        superDeferred; 
    isPDP = keyword.match(/.*\/(\d+)\/buy$/);
    superDeferred = Q.defer();
    
    if(isPDP) {
        styleId = isPDP[1];
        styleServiceRequest(styleId,function(err,response,parsedBody,styleId){
            var result,
                parsedData = {};
            if(err) {
                console.log(err);
                superDeferred.reject();
            } else {
                if(!parsedBody){
                    superDeferred.resolve();
                } else {
                    try {
                        result = parsedBody;
                        parsedData.brands = [result.data[0].brandName];
                        parsedData.articleTypes = [result.data[0].articleType.typeName];
                        parsedData.topBrand = result.data[0].brandName
                        parsedData.topArticleType = result.data[0].articleType.typeName;
                        deepRequest(parsedData,preDefined,footerOverride,superDeferred,styleId);
                    } catch(e) {
                        console.log(e.message);
                        failure.push(styleId);
                        superDeferred.resolve();
                    }
                }
            } 
        });
    } else {
        var brandsPromise,
            articleTypesPromise,
            allPromise,
            allPromises = [];

        solrRequestAndParse(keyword.replace(/shop\//,''), function(err,parsedData){
            if(err == 0) {
                // special case for error solr repsonse
                superDeferred.resolve();
            }
            deepRequest(parsedData,preDefined,footerOverride,superDeferred,keyword);
        });
    }
  return superDeferred.promise;
}

databasePromise.then(function(){
    Q.all(superPromises).then(function(result){
        console.log("Total stuff finsihed");
        console.log('Completed Keywords: ',completedKeywords);
        console.log('Search Service Req: ',searchServiceReq);
        console.log('PDP Service Req: ',pdpServiceReq);
        console.log('failure keywords: ', failure);
        redisClient.quit();
        connection.end(function(err){
            console.log(err);
        });

    },function(err){
        console.log(err);
    });
},function(err){
    console.log(err);
});

