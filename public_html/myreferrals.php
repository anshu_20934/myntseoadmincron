<?php
include_once $xcart_dir."/include/func/func.mkcore.php";
include_once $xcart_dir."/include/class/class.mymyntra.php";
include_once $xcart_dir."/include/func/func.utilities.php";
include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
include_once $xcart_dir."/include/func/func.numberToWords.php";
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";

$errors = array();
$mymyntra = new MyMyntra(mysql_real_escape_string($login));
$couponAdapter = CouponAdapter::getInstance();

$actionResult = array();
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$mode = $_POST["mode"];
	return;
	if(!empty($mode)){
		switch($mode){
			case "invite-friends" :
		        {
		        	if(checkCSRF($_POST["_token"], "invite-friends")){
		        		$email = mysql_real_escape_string(filter_input(INPUT_POST,'email',FILTER_SANITIZE_STRING));
		        		$updateusername=mysql_real_escape_string(filter_input(INPUT_POST,'updateusername',FILTER_SANITIZE_STRING));
		        		if($updateusername == "true"){
		        			$username=mysql_real_escape_string(filter_input(INPUT_POST,'username',FILTER_SANITIZE_STRING));
		        			$namearr = MyMyntra::splitFirstLastName($username);
							if(!empty($namearr)){
								$data = array ('firstname' => $namearr[0], 'lastname' => $namearr[1] );
								$actionResult = array_merge($actionResult, $mymyntra->updateProfile($data));
							}
		        		}
		        		$ret_msg=array();
		        		if(empty($actionResult[MyMyntra::$ERROR])){
		        			generateInvitationEmailsNew($email,$login);
		        		}
		        		else{
		        			$ret_msg['status']="failure";
		        			$ret_msg['error']=empty($actionResult[MyMyntra::$ERROR]);
							return $ret_msg;
		        		}
		        	}else{
		        		$ret_msg['status']="failure";
		        		$ret_msg['error']=empty($actionResult[MyMyntra::$ERROR]);
						return $ret_msg;
		        	}
		            break;
		        }
		    case "invite-multiple-friends" :
		        {
		        	if(checkCSRF($_POST["_token"], "invite-multiple-friends")){
		        		$email_list = mysql_real_escape_string(filter_input(INPUT_POST,'email_list',FILTER_SANITIZE_STRING));
		        		$updateusername=mysql_real_escape_string(filter_input(INPUT_POST,'updateusername',FILTER_SANITIZE_STRING));
		        		if($updateusername == "true"){
		        			$username=mysql_real_escape_string(filter_input(INPUT_POST,'username',FILTER_SANITIZE_STRING));
		        			$namearr = MyMyntra::splitFirstLastName($username);
							if(!empty($namearr)){
								$data = array ('firstname' => $namearr[0], 'lastname' => $namearr[1] );
								$actionResult = array_merge($actionResult, $mymyntra->updateProfile($data));
							}
		        		}
		        		$ret_msg=array();
		        		if(empty($actionResult[MyMyntra::$ERROR])){
		        			$ret_msg = generateInvitationEmailsNew($email_list,$login,true);
		        			echo json_encode($ret_msg);exit;
		        		}
		        		else{
		        			$ret_msg['status']="failure";
		        			$ret_msg['error']=empty($actionResult[MyMyntra::$ERROR]);
							echo  json_encode($ret_msg);exit;
		        		}
		        	}else{
		        		$ret_msg['status']="failure";
		        		$ret_msg['error']=empty($actionResult[MyMyntra::$ERROR]);
						echo  json_encode($ret_msg);exit;
		        	}
		            exit;
		        }   	
		}
		
	}
}



// Open invites.
$userProfileData = $mymyntra->getUserProfileData();
$open_invites = $couponAdapter->getOpenInvitesForReferer($userProfileData['login']);
$smarty->assign("open_invites", $open_invites);
$smarty->assign("num_open_invites", count($open_invites));
//echo count($open_invites);
	// Accepted invites.
$accepted_invites = $couponAdapter->getAcceptedInvitesForReferer($userProfileData['login']);
$smarty->assign("accepted_invites", $accepted_invites);
$smarty->assign("num_accepted_invites", count($accepted_invites));
// Active invites
$active_invites = $couponAdapter->getActiveInvitesForReferer($userProfileData['login']);
$smarty->assign("active_invites", $active_invites);
$smarty->assign("num_active_invites", count($active_invites));
$smarty->assign("userProfileData", $userProfileData);
$smarty->assign("today", time());

global $mrpCouponConfiguration;
// amount earnable due to referral registration.
$smarty->assign("mrpAmount",$mrpCouponConfiguration['mrp_refRegistration_numCoupons'] * $mrpCouponConfiguration['mrp_refRegistration_mrpAmount'] );
// amount earnable by referral first purchase.
$smarty->assign("refTotalValue", $mrpCouponConfiguration['mrp_refFirstPurchase_numCoupons'] * $mrpCouponConfiguration['mrp_refFirstPurchase_mrpAmount']) ;
$smarty->assign("num_coupons", $mrpCouponConfiguration['mrp_firstLogin_nonfbreg_numCoupons']);
$smarty->assign("value_coupons", $mrpCouponConfiguration['mrp_firstLogin_nonfbreg_mrpAmount']);

$smarty->assign("mrp_refRegistration_mrpAmount", $mrpCouponConfiguration['mrp_refRegistration_mrpAmount']);
$smarty->assign("mrp_refRegistration_minCartValue", $mrpCouponConfiguration['mrp_refRegistration_minCartValue']);
$smarty->assign("mrp_refFirstPurchase_numCoupons", convert_number_to_words($mrpCouponConfiguration['mrp_refFirstPurchase_numCoupons']));
$smarty->assign("mrp_refFirstPurchase_mrpAmount", $mrpCouponConfiguration['mrp_refFirstPurchase_mrpAmount']);
$smarty->assign("mrp_refFirstPurchase_minCartValue", $mrpCouponConfiguration['mrp_refFirstPurchase_minCartValue']);

function generateInvitationEmailsNew($email_list, $login,  $multipleEmails = false){
	global $couponAdapter, $mrpCouponConfiguration, $xcart_dir;
	$ret_msg='';
	if(empty($email_list))
    {
    	$ret_msg['status']="emptylist";
        exit;
    }

	$row = func_query_first("SELECT firstname, lastname FROM xcart_customers WHERE login = '$login';");
    $name = $row['firstname'] . " " . $row['lastname'];
    $name = (empty($row['firstname']) ? $login : $name);
	$login_tw = str_replace("@", "%40", $login);

	//pick up additional information about the coupons to pass it to mailers
	$num_coupons = $mrpCouponConfiguration['mrp_firstLogin_nonfbreg_numCoupons'];
    $each_coupon_value = $mrpCouponConfiguration['mrp_firstLogin_nonfbreg_mrpAmount'];
    $validity = $mrpCouponConfiguration['mrp_firstLogin_nonfbreg_validity'];
    $minCartValue = $mrpCouponConfiguration['mrp_firstLogin_nonfbreg_minCartValue'];
    $mrp_amount = $num_coupons*$each_coupon_value;

    include_once "$xcart_dir/modules/coupon/database/MRPDataAdapter.php";
    include_once "$xcart_dir/modules/coupon/mrp/exception/MrpCustomerNotFoundException.php";
    require_once "$xcart_dir/include/func/func.mail.php";
    require_once "$xcart_dir/include/func/func.db.php";

    $mrpDataAdapter = MRPDataAdapter::getInstance();

    $sent=0;
    $already_registered=0;
    $already_sent=0;
    $invalid_email=0;
    $emails=explode(',',$email_list);

    foreach($emails as $email)
    {
    	if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email))
    	{
            if(!$multipleEmails){
            	echo "failure";
            	exit;
            }
            $invalid_email++;
            continue;
		}

		try {
        	$customer = $mrpDataAdapter->fetchCustomer($email);

            //already registered
            if(!$multipleEmails){
            	echo "custfailure";
                exit;
            }
            $already_registered++;
            continue;
        }
        catch (MrpCustomerNotFoundException $e) {

            // Send the invitation email.
            $template = "mrp_referral_invitation";
            $reg_discount = FeatureGateKeyValuePairs::getFeatureGateValueForKey('referer.registration.discount');
            global $http_location;
            $args =
            	array (
                	"REFERER"          =>  $login,
                    "NAME-REFERER"     =>  $name,
                    "MRP_AMOUNT"	   => $mrp_amount,
                    "REG_DISCOUNT"     => $reg_discount,
                    "INVITATIONLINK"   =>  $http_location."/register.php?ref=".$login_tw."&utm_source=MRP_mailer&utm_medium=referral_invitation&utm_campaign=accept_invitation",
            		"NUM_COUPONS"	=> $num_coupons,
        			"EACH_COUPON_VALUE" => $each_coupon_value,
            		"VALIDITY"		=> $validity,
        			"MIN_PURCHASE_AMOUNT" => $minCartValue
                );

			$subject_args=array ("NAME-REFERER"     =>  $name, "MRP_AMOUNT" => $mrp_amount, "REG_DISCOUNT" => $reg_discount);
			$retval = $couponAdapter->addNewReferral($login, $email);

            if($retval)	{
            	sendMessageDynamicSubject($template, $args, $email, "MRP",$subject_args);
            	if(!$multipleEmails)	{
            		echo "success";
            		exit;
            	}

				// Open invites.
                $sent++;
			}	else	{
				if(!$multipleEmails)	{
					echo "timestampfailure";
					exit;
				}
				$already_sent++;
			}
        } // end of catch block

	} // end of foreach block

	$ret_msg['status']="success";
	$ret_msg['sent']=$sent;
	$ret_msg['already_registered']=$already_registered;
	$ret_msg['already_sent']=$already_sent;
	$ret_msg['invalid_email']=$invalid_email;

	return $ret_msg;
}

$smarty->assign("view",$view);
$smarty->assign("login",$login);

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setContextualPageData(AnalyticsBase::MYMYNTRA);
$analyticsObj->setTemplateVars($smarty);

$tracker->fireRequest();
        
$smarty->display("my/referrals.tpl");

