<?php
include_once $xcart_dir."/include/func/func.mkcore.php";
include_once $xcart_dir."/include/class/class.mymyntra.php";
include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";
include_once "$xcart_dir/modules/myntCash/MyntCashService.php";
include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalClient.php";
include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalHelper.php";

$errors = array();
$mymyntra = new MyMyntra(mysql_real_escape_string($login));
$couponAdapter = CouponAdapter::getInstance();

$userProfileData = $mymyntra->getUserProfileData();
// Cashback coupon.
/*$cashbackCoupon = $couponAdapter->getCashbackCoupon($userProfileData['login']);
$cashback_coupon = (empty($cashbackCoupon)) ? '' : $cashbackCoupon['coupon'];
$cashback_balance = (empty($cashbackCoupon)) ? '' : $cashbackCoupon['MRPAmount'];
$smarty->assign("cashback_coupon", $cashback_coupon);
$smarty->assign("cashback_balance", $cashback_balance);
// Cashback transactions.
//$cashback_transactions = $couponAdapter->getCashbackTransactions($userProfileData['login']);
//$smarty->assign("cashback_transactions", $cashback_transactions);
*/


//If loyalty is enabled MyMyntra layout and flow is different 
// $loyaltyEnabled is fetched in auth.php 
if($loyaltyEnabled && !$isXHR){
    $smarty->display("my/myntcredits-v2.tpl");
    exit;
}elseif ($loyaltyEnabled && $isXHR) {
    $defaultSubView = 'mycoupons';
    $subView = $_GET['subview'];
    $subView = isset($_GET['subview'])
            ? filter_input(INPUT_GET,'subview',FILTER_SANITIZE_STRING) 
            : $defaultSubView;
    $subView = (!in_array($subView, array('mycoupons','mycashback','myprivilege'))) 
            ? $defaultView 
            : $subView;
    if($subview == 'mycoupons'){
        include_once "$xcart_dir/mycoupons.php";
        $smarty->display("my/myntcoupons.tpl");
    }elseif ($subview == 'mycashback') {
        include_once "$xcart_dir/mycashback.php";
        $smarty->display("my/myntcashback.tpl");
    }elseif ($subview == 'myprivilege') {
        include_once "$xcart_dir/myloyalty.php";
    }
    exit;
}



