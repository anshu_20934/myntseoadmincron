<?php

require_once 'env/System.config.php';

$displayRemarkettingtag = false;
if ($cookieContent['utm_source'] == 'aff'){
	$label = SystemConfig::$affGCLabel;
	$color = SystemConfig::$affGCColor;
	$displayRemarkettingtag = true;
}
else if(((stripos($cookieContent['utm_medium'], 'google') !== false)||
	(stripos($cookieContent['utm_medium'], 'yahoo') !== false) ||
	(stripos($cookieContent['utm_medium'], 'bing') !== false) ||
	(stripos($cookieContent['utm_medium'], 'search') !== false)
	) && $cookieContent['utm_source'] == 'referral'){
	// medium for seo can be google/yahoo/bing/search
	$label = SystemConfig::$seoGCLabel;
	$color = SystemConfig::$seoGCColor;
	$displayRemarkettingtag = true;
}
else if(substr($cookieContent['utm_source'], 0, strlen('fb')) === 'fb'){
	// Remarketting for FB and SEO visitors
	$label = SystemConfig::$fbGCLabel;
	$color = SystemConfig::$fbGCColor;
	$displayRemarkettingtag = true;
}
else if($_SERVER['REQUEST_URI'] == '/') {
	$label = SystemConfig::$homeGCLabel;
	$color = SystemConfig::$homeGCColor;
	$displayRemarkettingtag = true;
}

$smarty->assign("displayRemarkettingTag",$displayRemarkettingtag);
$smarty->assign('googe_conversion_label',$label);
$smarty->assign('googe_conversion_color',$color);

?>
