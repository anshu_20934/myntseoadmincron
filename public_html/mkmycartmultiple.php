<?php
require "./auth.php";
include_once("./checkOnlineUser.php");
if( isOnlineUser() )
{
        include_once("./mkmycartmultiple_re.php");
        exit;
}
include_once("./include/class/class.orders.php");
include_once("./include/func/func.mkcore.php");
include_once("./include/func/func.db.php");
include_once("./include/func/func.mkmisc.php");
include_once("./include/func/func.mkspecialoffer.php");
include_once("./include/class/class.orders.php");
include_once("./mkdiscountrules.php");
include_once("./include/func/func.affiliates.php"); 
include_once("include/func/func.shop.php");
require_once ("include/func/func.RRRedeemPoints.php");
include_once($xcart_dir."/modules/organization/orgaInIt.php");

include_once ("$xcart_dir/modules/coupon/database/CouponAdapter.php");
include_once ("$xcart_dir/modules/coupon/CouponValidator.php");
include_once ("$xcart_dir/modules/coupon/CouponDiscountCalculator.php");
include_once ("$xcart_dir/modules/cart/Cart.php");
include_once ("$xcart_dir/modules/coupon/exception/CouponNotFoundException.php");

######## added file to show the affiliate link on side nav bar  ########
include($xcart_dir."/modules/affiliates/affiliateinit.php");




$validator = CouponValidator::getInstance();
$adapter = CouponAdapter::getInstance();
$calculator = CouponDiscountCalculator::getInstance();
$userName = $XCART_SESSION_VARS["login"];

/***User register successfully*****/
$register = base64_decode($_GET['success']);
$smarty->assign("register_success", $register);
/************************************/
/** Code for displaying the popup message **/
$smarty->assign("pagename",$pagename = "mycart");
$filetype = base64_decode($_GET['filetype']);
$smarty->assign("login_event", $filetype);

/********* setting the referrer url if user register or logged in incorrectly **********/
$pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
     $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
$smarty->assign("referer",base64_encode(urlencode($pageURL)));

/******** end  ******/

//Code for POS. Setting vat to zero
$userType = $XCART_SESSION_VARS['login_type'];
$shopMasterId = 0;
$accountType = 0;
/***************************************************************************/
//Recalculate cart by pulling unit price and vat from database to avoid 0 cost orders.
$productsInCart = orders::recalculate_productsInCart($XCART_SESSION_VARS['productsInCart'],$XCART_SESSION_VARS['orgId']);
$XCART_SESSION_VARS['productsInCart'] = $productsInCart;

$productids = $XCART_SESSION_VARS['productids'];
$grandTotal = $XCART_SESSION_VARS['grandTotal'];
$masterCustomizationArray = $XCART_SESSION_VARS['masterCustomizationArray'];
$masterCustomizationArrayD = $XCART_SESSION_VARS['masterCustomizationArrayD'];
$chosenProductConfiguration = $XCART_SESSION_VARS['chosenProductConfiguration'];

x_session_register('masterCustomizationArrayD');

/*********************************************************	******************/
 $pageType=strip_tags($_GET['pagetype']);
 $pageType=sanitize_paranoid_string($pageType);
if($pageType == "productdetail")
{
	if($accountType == 1)
		$selectedIndex = '1';
	else
		$selectedIndex = '2';
	/* collect */
	$styleid = $chosenProductConfiguration['productStyleId'];
	$typeid =  $chosenProductConfiguration['productTypeId'];
	$productid =$chosenProductConfiguration['productId'];
	$label =   $chosenProductConfiguration['productTypeLabel'];
	//$sizenames = $chosenProductConfiguration['sizename'];
	//$sizevalues = $chosenProductConfiguration['sizequantity'];
    $smarty->assign("selectedIndex",$selectedIndex);
	//$smarty->assign("sizenames",$sizenames);
	//$smarty->assign("sizevalues",$sizevalues);
	$breadcrumbLink = array('reference'=>"products/$productid",'title'=>'product detail');

}

if($pageType == "createproduct")
{
	if($accountType == 1)
		$selectedIndex = '2';
	else
		$selectedIndex = '3';
	$styleID = $chosenProductConfiguration['productTypeId'];
	$styleLabel = $chosenProductConfiguration['productTypeLabel'];
	$smarty->assign("selectedIndex",$selectedIndex);
	//$breadcrumbLink[]= array('reference'=>"mkproductstyles.php?id=$styleID&type=$styleLabel",'title'=>'select style');
	$breadcrumbLink= array('reference'=>"mkcreateproductpreview.php",'title'=>'create product');
}

$smarty->assign("pagetype",$pageType);

$weblog->info("Creation of masterCustomizationArrayD array started");
foreach($productids AS $key=>$value){
	$checkProductId = "SELECT productid FROM $sql_tbl[products] WHERE productid=".$value;
	$checkResult  = db_query($checkProductId);
	$sqllog->debug("File Name::>".$scriptname."####"."##SQL Error::>".$checkProductId);
	$checkRow = db_num_rows($checkResult);

	if($checkRow == 0){
		$parentKey = array_keys($masterCustomizationArrayD);
		$isexist = false;
		foreach($parentKey AS $key1=>$value1){
			if($value1 == $value){
				$isexist = true;
			    break;
			}
		}
		if(!$isexist){
			$masterCustomizationArrayD[$value] = $masterCustomizationArray;
			x_session_register('masterCustomizationArrayD');
			$uuid = stripslashes(getMyntraCookie('MYNTRA_SHOPPING_ID'));
			if(empty($affiliateId) && $uuid != "") {				
				store_cart_products($uuid,$login,$productsInCart,$productids,$masterCustomizationArrayD); 
			}
		}
	}
	$i++;
}

$weblog->debug("masterCustomizationArrayD array created###".$masterCustomizationArrayD);
$weblog->info("masterCustomizationArrayD has been created successfuly");


# Checking for user type as kiosk user to grant additional functionality
$query = "select source_manager_email from mk_sources where source_manager_email = '$login' and source_type_id = 9 and EXISTS(select * from xcart_customers where login='$login' and account_type='PU')";
$source_data=func_query_first($query);
if(array_key_exists('source_manager_email',$source_data))
	$XCART_SESSION_VARS['couponCode'] = 'MyntraPOS25';


if(empty($productsInCart)){
	$productsInCart = null;
	if($XCART_SESSION_VARS['couponCode']){
		unset($XCART_SESSION_VARS['couponCode']);
		unset($XCART_SESSION_VARS['coupondiscount']);
		unset($XCART_SESSION_VARS['couponpercent']);
		unset($XCART_SESSION_VARS['CCstyleid']);
		x_session_unregister('CCstyleid');
		x_session_unregister('coupondiscount');
		x_session_unregister('couponpercent');
		x_session_unregister('couponCode');
	}
}

$grandTotal = 0;
$totaldiscount = 0;
$subTotal = 0;
$optionCounter = array();
$tb_product_category = $sql_tbl["products_categories"];
//Computing grand total for cart for the session
$i = 0;
$ratioArray = array();
$count4By6 = 0;
$priceOf4By6 = 0;
$totalDiscountForIbUsers = 0;
$discountForIbiboProducts = 0;
$amountOf99corner = 0; ############## for 99 corner promotional offer ###############

$smarty->assign("accountType", $accountType);
foreach($productids AS $key=>$value){
  $optionCount = count($productsInCart[$value]['optionNames']);
  $productsInCart[$value]['optionCount'] = $optionCount;
  $productDetail =   $productsInCart[$value];
  //$tid = func_query_first_cell("select product_type_id from xcart_products where productid = '$value'");
  $tid=$productDetail["producTypeId"];
  if($accountType == 1)
  	$vatRate = 0;
  else
  	$vatRate = func_query_first_cell("select vat from mk_product_type where id='$tid'");
 /***************************************/
	################### Designd for the Ibibo User discount ###############
	if($productDetail['productStyleId'] == "138")
	{
		$count4By6 = $count4By6 + $productDetail['quantity'];
		$priceOf4By6 = $productDetail['productPrice'];
	}
	if(($productDetail['producTypeId'] == "3" || $productDetail['producTypeId'] == "14") && $affiliateId == 21)
	{
		$totalDiscountForIbUsers += ($productDetail['actualPriceOfProduct'] * $productDetail['quantity']) - ($productDetail['productPrice'] * $productDetail['quantity']);
		$discountForIbiboProducts = $totalDiscountForIbUsers;
	}
	if($productDetail['producTypeId'] == 58){
		$amountOf99corner += $productDetail['productPrice'] * $productDetail['quantity'];
	}
	
	
	
    $subTotal = $subTotal + $productDetail['totalPrice'];
      if($productsInCart[$value]['updateddiscount'])
      {
          $cartTotalPrice = $cartTotalPrice + $productDetail['totalPrice'];
          $grandTotal = $grandTotal + ( $productDetail['totalPrice'] - $productDetail['updateddiscount']) ;
          $totaldiscount = $totaldiscount + $productDetail['updateddiscount'];
          $grandTotalTax = $grandTotalTax+($vatRate*( $productDetail['totalPrice'] - $productDetail['updateddiscount']))/100;
      }
      else
      {
          $cartTotalPrice = $cartTotalPrice + $productDetail['totalPrice'];
          $grandTotal = $grandTotal + ($productDetail['totalPrice'] - $productDetail['discount']) ;
          $totaldiscount = $totaldiscount + $productDetail['discount'];
          $grandTotalTax = $grandTotalTax+($vatRate*( $productDetail['totalPrice']))/100;
      }
	
	
	$ratioArray[$i] = ($productDetail['totalPrice']*$vatRate)/100;
	$ratioArrayTotal = $ratioArrayTotal+$ratioArray[$i];
	//To get the category of the product
	$query = "select $tb_product_category.categoryid from $tb_product_category where $tb_product_category.productid =$value";
	$result=db_query($query);
	$sqllog->debug("File Name::>".$scriptname."####"."##SQL Error::>".$query); 
	if($result)
	{
		$row=db_fetch_row($result);
		if((!in_array($row[0], $categoryids)) && ($row[0] != ""))
		{
				$categoryids[$i] = $row[0];
		}
	}

   $i++;
}

$grandTotalForTaxCalculation = $cartTotalPrice;
$offers = array_merge($productOffer, $categoryOffer);
$countoffer = count($offers);
if(!x_session_is_registered('offers')){
    x_session_register('offers');
}
$XCART_SESSION_VARS['offers'] = $offers;


for($j=0; $j<3; $j++)
{
	$optionCounter[$j]= $j;
}

$smarty->assign("optionCounter",$optionCounter);
$smarty->assign("productsInCart",$productsInCart);
$smarty->assign("productids",$productids);

//Adding Gift certifcates to the grand total

$giftcerts = $cart["giftcerts"];

$count =count($giftcerts);
$certificate ;
$gc2cart= $XCART_SESSION_VARS['gc2cart'];

if($gc2cart =="true"){
	foreach($giftcerts AS $key=>$value){
		$certificate = $cart["giftcerts"][$key];
		$subTotal = $subTotal + $certificate["totalamount"];
		$grandTotal = $grandTotal + $certificate["totalamount"];
	}
}
else{
	$giftcerts = null ;
}

$weblog->info("Function call for the calculation of special offer discount for total.##Function name is::>func_get_special_offer_discount_for_Total($grandTotal)");
// Get special offer for discount on total
$discountOnTotal = func_get_special_offer_discount_for_Total($grandTotal);
$weblog->info("func_get_special_offer_discount_for_Total() returned discount on total::>".$discountOnTotal);
$grandTotal = $grandTotal - $discountOnTotal;

if($XCART_SESSION_VARS['couponCode'] != NULL && $XCART_SESSION_VARS['couponCode'] != "") {
	$couponCode = $XCART_SESSION_VARS['couponCode'];
}

//Compute coupons discount
$c=strip_tags($HTTP_GET_VARS['c']);
$c=sanitize_int($c);
$couponRedemption=$c;

/* checking for couponcode exists in session
 * as well for the case of promo portal
 */
if ($couponRedemption == '1' || (!empty($couponCode))) {
	$couponCode = ($HTTP_POST_VARS['couponcode'])?$HTTP_POST_VARS['couponcode']:$XCART_SESSION_VARS['couponCode'];
	$exists = true;

	// Fetch the coupon.
	$coupon = null;
	try {
	    $coupon = $adapter->fetchCoupon($couponCode);
	}
	catch (CouponNotFoundException $cnfe) {
	    $exists = false;
	    $couponMessage = $cnfe->getMessage();
	    $couponMessage = "<font color=\"red\">$couponMessage</font>";
	}
	
	// Check validity of the coupon for the user.
	$isValid = false;
	try {
	    if ($exists) {
	        $weblog->info("gonna validate coupon.. which exists");
	        $isValid = $validator->isCouponValidForUser($coupon, $userName);
	    }
	    else {
	        $couponMessage = "Coupon <b>$couponCode</b> does not exist.";
	        $couponMessage = "<font color=\"red\">$couponMessage</font>";
	    }
	}
	catch (CouponExpiredException $ex) {
	    $couponMessage = $ex->getMessage();
	    $couponMessage = "<font color=\"red\">$couponMessage</font>";
	}
	catch (CouponNotStartedException $ex) {
	    $couponMessage = $ex->getMessage();
	    $couponMessage = "<font color=\"red\">$couponMessage</font>";
	}
	catch (CouponUsageExpiredException $ex) {
	    $couponMessage = $ex->getMessage();
	    $couponMessage = "<font color=\"red\">$couponMessage</font>";
	}
	catch (CouponLockedException $ex) {
	    $couponMessage = $ex->getMessage();
	    $couponMessage = "<font color=\"red\">$couponMessage</font>";
	}
	catch (CouponUsageExpiredForUserException $ex) {
	    $couponMessage = $ex->getMessage();
	    $couponMessage = "<font color=\"red\">$couponMessage</font>";
	}
	catch (CouponNotActiveException $ex) {
	    $couponMessage = $ex->getMessage();
	    $couponMessage = "<font color=\"red\">$couponMessage</font>";
	}
	catch (CouponLockedForUserException $ex) {
	    $couponMessage = $ex->getMessage();
	    $couponMessage = "<font color=\"red\">$couponMessage</font>";
	}
	catch (CouponNotValidForUserException $ex) {
	    $couponMessage = $ex->getMessage();
	    $couponMessage = "<font color=\"red\">$couponMessage</font>";
	}
	catch (CouponChannelException $ex) {
	    $couponMessage = $ex->getMessage();
	    $couponMessage = "<font color=\"red\">$couponMessage</font>";
	}
	catch (CouponUserNotLoggedInException $ex) {
	    $couponMessage = $ex->getMessage();
	    $couponMessage = "<font color=\"red\">$couponMessage</font>";
	}

	if ($isValid) {
	    
	    // Construct the cart.
	    $myCart = new Cart($productsInCart);
	    $myCart->clearDiscount();
	    $applied = false;
	    
	    try {
	        $calculator->applyCouponOnCart($coupon, $myCart);
	        $applied = true;
	    }
	    catch (CouponNotApplicableException $ex) {
	        $couponMessage = $ex->getMessage();
	        $couponMessage = "<font color=\"red\">$couponMessage</font>";
	    }
	    catch (CouponAmountException $ex) {
	        $couponMessage = $ex->getMessage();
	        $couponMessage = "<font color=\"red\">$couponMessage</font>";
	    }
	    catch (CouponCorruptedException $ex) {
	        $couponMessage = $ex->getMessage();
	        $couponMessage = "<font color=\"red\">$couponMessage</font>";
	    }

	    //
	    // Indicate success, if the coupon was applied on at least one
	    // product in the cart.
	    //
	    if ($applied) {
	        $couponMessage =
		    		"<font color=\"green\">Coupon <b>$couponCode</b> was "
	        . "successfully redeemed on the cart.</font>";

	        $smarty->assign("divid", "message");

	        // Update the session array to include per product discount.
	        $myCart->deconstructCart($productsInCart);
	        $XCART_SESSION_VARS['productsInCart'] = $productsInCart;
	        $coupondiscount = $myCart->getDiscount();
	        x_session_register('coupondiscount');
	        $XCART_SESSION_VARS['coupondiscount'] = $coupondiscount;
	        $couponpercent = $coupon->getDescription();
	        x_session_register('couponpercent');
	        $XCART_SESSION_VARS['couponpercent'] = $couponpercent;
	        $smarty->assign("couponpercent", $couponpercent);
	        $XCART_SESSION_VARS['couponCode'] = $couponCode;
            $weblog->info("coupon details::coupon=$couponCode::coupondiscount=$coupondiscount::couponpercent::$couponpercent");
	    }
	    else
	    {
	        $smarty->assign("divid","error");
	    }
	}
	else{
	    $smarty->assign("divid","error");
	}

	$smarty->assign("couponMessage",$couponMessage);
}
$coupondiscount = $XCART_SESSION_VARS['coupondiscount'];
// Call function to get special offers for total

$totaloffers = func_get_special_offers_for_Total();
$counttotaloffer = count($totaloffers);
if(!x_session_is_registered('totaloffers')){
    x_session_register('totaloffers');
}
$XCART_SESSION_VARS['totaloffers'] = $totaloffers;

//$totaldiscountonoffer = $discountOnTotal + $totaldiscount;
$totaldiscountonoffer = $discountOnTotal;
if(!x_session_is_registered('totaldiscountonoffer')){
    x_session_register('totaldiscountonoffer');
}
$XCART_SESSION_VARS['totaldiscountonoffer'] = $totaldiscountonoffer;

//Deducting discount on total;
$amt = $subTotal - $totaldiscountonoffer;
$subTotal = $amt;
$amountAfterDiscount = $subTotal - $coupondiscount;
if($amountAfterDiscount < 0)
$amountAfterDiscount =0;

if(!x_session_is_registered('amountAfterDiscount')){
    x_session_register('amountAfterDiscount');
}
$XCART_SESSION_VARS['amountwithouttax'] = $amountAfterDiscount;

$productMessage = "Your cart has no items. Please add items in your cart.";
$totalAfterDiscount = $amountAfterDiscount;
$totalCouponAndDiscountOnTotal = $coupondiscount+$discountOnTotal;


if($amountAfterDiscount > 0) {
    $vatCst = $grandTotalTax-(($ratioArrayTotal*$totalCouponAndDiscountOnTotal)/$grandTotalForTaxCalculation);
}
else {
    $vatCst = 0;
}
$vatInSession = $vatCst;
### For amma.com ##
 if (x_session_is_registered("vatInSession"))
       x_session_unregister("vatInSession");
 x_session_register("vatInSession");

$amountAfterDiscount = $amountAfterDiscount + $vatCst;
if(!empty($count4By6))
{
	if($count4By6 >= 10)
	$dicountPriceForIbibo = 10 * $priceOf4By6;
	else
	$dicountPriceForIbibo = $count4By6 * $priceOf4By6;
	$smarty->assign("ibibodiscountonphotos",$dicountPriceForIbibo);
	$totalDiscountForIbUsers = $totalDiscountForIbUsers + $dicountPriceForIbibo;
	$amountAfterDiscount = $amountAfterDiscount - $dicountPriceForIbibo;
	$totalAfterDiscount = $totalAfterDiscount-$dicountPriceForIbibo;
	
	if($amountAfterDiscount < 0)
		$amountAfterDiscount=0;
	
	if($totalAfterDiscount < 0)
		$totalAfterDiscount=0;
}

//##brandstore code
$accountname = $XCART_SESSION_VARS['orgName'];
$accountname = strtolower($accountname);
$smarty->assign("accountname",$accountname);

#
#Is user admin of organization
#
$admincheck = func_is_user_admin_of_organization($orgId);
$smarty->assign("admincheck",$admincheck);

#
#If amount data greater than or equal to offer amount
#
$amountAfterDiscount = number_format($amountAfterDiscount,2,".",'');
if($amountAfterDiscount >= 999.00){
$offers=func_query_first("SELECT offer_text, offer_key FROM $sql_tbl[mk_offers]");
$monthly_offers[$offers[offer_key]]=$offers[offer_text];

$smarty->assign("offers", $monthly_offers);
}

/* added by arun for orgid=23(ING MGM)
 * redirecting to payment page not showing 
 * the cart for gifts
 */
if($promoportal === true){
	x_session_register('couponMessage');
	func_header_location("mkpaymentoptions.php");
}

if($rewardsUser = isCustomerRewardsUser($login)){
	$checkadmin = isCustomerRewardsAdmin($login);
	$smarty->assign("rr_master",($checkadmin == 1)?true:false);
	$smarty->assign('RRtype',$rewardsUser);
	$smarty->assign("allInclusivePricing", "1");
	$smarty->assign("noDiscountCoupons", "1");
	$availablePoints=GetCustomerPersonalRewardsAccountBalance($login);
	$smarty->assign("rewardPointBalance",$availablePoints);
	$smarty->assign("remainingRewardPoints", $availablePoints-$grandTotal);
}

#
#Private interface code ends here
#
$mastername = $XCART_SESSION_VARS['identifiers']['C']['master_name'];
if($orgId){
	if($orgId == 22){
		$breadcrumbLink = array('reference'=>$_SERVER['HTTP_REFERER'] ,'title'=>'product details');
		$breadCrumb=array(array('reference'=>'modules/organization/index.php','title'=>'home'),
		                  array('reference'=>$http_location.'/modules/organization/orgBrowseAllProducts.php','title'=>'browse products'),
		                  $breadcrumbLink,
		                  array('reference'=>'#','title'=>"Cart"));
	}elseif (isCustomerRewardsUser($login) == "1"){
		$breadcrumbLink = array('reference'=>$_SERVER['HTTP_REFERER'] ,'title'=>'my cart');
		$breadCrumb=array(
						array('reference'=>'modules/organization/index.php','title'=>'home'),
						array('reference'=>'modules/organization/orgBrowseAllProducts.php','title'=>'browse products'),
			    		array('reference'=>'#','title'=>"my cart"),
		    		);
	}else{
		$breadcrumbLink = array('reference'=>$_SERVER['HTTP_REFERER'] ,'title'=>'create products');
		$breadCrumb=array(array('reference'=>'modules/organization/index.php','title'=>'home'),
		                  array('reference'=>$http_location.'/modules/organization/orgSelectProduct.php','title'=>'select products'),
		                  $breadcrumbLink,
		                  array('reference'=>'#','title'=>"Checkout"));		
	}

}else{
	if(!empty($mastername)){
		$breadCrumb=array(array('reference'=>strtolower($mastername),'title'=>'home'),
			  	array('reference'=>'mkstartshopping.php','title'=>'start shopping'),
                $breadcrumbLink,
                array('reference'=>'#','title'=>"Checkout"));
	}else{
		$breadCrumb=array(array('reference'=>'home.php','title'=>'home'),
			  	array('reference'=>'mkstartshopping.php','title'=>'start shopping'),
                $breadcrumbLink,
                array('reference'=>'#','title'=>"Checkout"));
	}
}
$smarty->assign("breadCrumb",$breadCrumb);

$promotion_key = $XCART_SESSION_VARS['PROMOTION_KEY'];
$smarty->assign("promotion_key",$promotion_key);


/* code to have sports jersey store products 
 * name and number to show in cart irrespective of mkmycart.php
 * or mkmycartmultiple.php(assumed to have removed mkmycart.php soon) 
 */
$reebok_ipl_products = $XCART_SESSION_VARS['product_custom_display_forcart'];
$smarty->assign("reebok_ipl_products",$reebok_ipl_products);

//code for dynamic size and color
$styleid_result = db_query("select distinct name from mk_product_options where style='".$chosenProductConfiguration['productStyleId']."' ");
$row_styletype=db_fetch_array($styleid_result);
$smarty->assign("styletype",$row_styletype);
//end code for dynamic size and color

$smarty->assign("ibiboDiscountProducts",$discountForIbiboProducts);
$smarty->assign("ibibousersdiscount",$totalDiscountForIbUsers);

//#########Assign coupon style id##############
$smarty->assign("CCodeStyleId",$couponCodeDetails[16]);
$smarty->assign("CCexist",$couponCodeDetails[1]);
//######################################
$smarty->assign("productMessage",$productMessage);
$smarty->assign("totaldiscountonoffer", $totaldiscountonoffer);
$smarty->assign("totaloffers", $totaloffers);
$smarty->assign("counttotaloffer", $counttotaloffer);
//$smarty->assign("offers", $offers);
$smarty->assign("countoffers", $countoffer);
$smarty->assign("subTotal",number_format($subTotal,2,".",''));
// added the vat and orignal total
$smarty->assign("totalAfterDiscount",number_format($totalAfterDiscount,2,".",''));
$smarty->assign("grandTotal",number_format($amountAfterDiscount,2,".",''));
$smarty->assign("vat",number_format($vatCst,2,".",''));
$smarty->assign("couponCode",$XCART_SESSION_VARS['couponCode']);
$smarty->assign("coupondiscount",$coupondiscount);
$smarty->assign("giftcerts",$giftcerts);
$smarty->assign("totalInCart",count($productsInCart));
//to show common cart for brandshop
if($loadCommonBrandshopEntities)
    func_display("modules/organization/orgCommonTpl/orgCart.tpl", $smarty);
else
    func_display("customer/mkaddtocart.tpl", $smarty);
?>
