<?php 
define("QUICK_START", 1);
include_once './auth.php';
use revenue\payments\service\PaymentServiceInterface;
global $weblog;

$login = $XCART_SESSION_VARS['login'];
$weblog->info("login :$login");
$orderID = $XCART_SESSION_VARS['orderID'];
$gcorderID = $XCART_SESSION_VARS['gcorderID'];
$reasonCode = sanitize_int($_POST['reason_code']);
$order_type = filter_input(INPUT_POST, 'order_type', FILTER_SANITIZE_STRING);

$paymentServiceEnabled = PaymentServiceInterface::getPaymentServiceEnabledForUser($login);

$weblog->info("Decline order paymentServiceEnabled: ".$paymentServiceEnabled);


if(!empty($orderID) && ($order_type == 'normal'||$order_type=='express_order')) {
	$weblog->info("Decline order : ".$orderID);
	$orderID = mysql_real_escape_string($orderID);
	$lastOrderStatus = func_query_first_cell("select status from xcart_orders where orderid = '$orderID'");
	
	//echo "last os : ".$lastOrderStatus;
	$weblog->info("Decline order : ".$lastOrderStatus);
	if($lastOrderStatus=='PP') {
		// update mk_payments log.
		$reason='';
		switch ($reasonCode) {
			case 1:
				$reason="User closed the overlay window";
				break;
			case 2:
				$reason="User clicked on cancel payment";
				break;
			default: 
				$reason="User intentionally cancelled payment";
				break;
		}
		$toPaymentsLog = array();		

		
		
		if(!$paymentServiceEnabled) {		
                        $toPaymentsLog['time_return'] = time();
                        $toPaymentsLog['is_tampered'] = 0;		
                        $toPaymentsLog['is_complete'] = 0;
                        $toPaymentsLog['is_flagged'] = 0;
                        $toPaymentsLog['response_code'] = 3;
                        $toPaymentsLog['response_message'] = $reason;
                        $toPaymentsLog['completed_via'] = 'Auto';

                 //$sql = "UPDATE xcart_orders SET tf_status ='TF',status='D',response_server='$server_name' WHERE orderid = '$orderID'";  ### Update the table in case transaction get failed
		//db_query($sql);
			func_array2updateWithTypeCheck("mk_payments_log", $toPaymentsLog,"orderid='$orderID'");
			$weblog->info("paymentServiceEnabled : ".$paymentServiceEnabled);
		}
		else {
                        $toPaymentsLog['returnTime'] = time();
                        $toPaymentsLog['isTampered'] = 0;		
                        $toPaymentsLog['isComplete'] = 0;
                        $toPaymentsLog['isFlagged'] = 0;
                        $toPaymentsLog['responseCode'] = 3;
                        $toPaymentsLog['responseMessage'] = $reason;
                        $toPaymentsLog['completedVia'] = 'Auto';
			
			PaymentServiceInterface::updatePaymentLog($orderID, $toPaymentsLog);
			$weblog->info("paymentServiceEnabled : ".$paymentServiceEnabled);
		}
		
		echo "redirect";
	} else {
		echo "Order status error";
	}
	
} else if(!empty($gcorderID) && $order_type == 'gifting') {
	$gcorderID = mysql_real_escape_string($gcorderID);
	$lastOrderStatus = 'PP';
	if($lastOrderStatus=='PP') {
		// update mk_payments log.
		$reason='';
		switch ($reasonCode) {
			case 1:
				$reason="User closed the overlay window";
				break;
			case 2:
				$reason="User clicked on cancel payment";
				break;
			default: 
				$reason="User intentionally cancelled payment";
				break;
		}
		$toPaymentsLog = array();		
		
		if(!$paymentServiceEnabled) {
		//$sql = "UPDATE xcart_orders SET tf_status ='TF',status='D',response_server='$server_name' WHERE orderid = '$orderID'";  ### Update the table in case transaction get failed
		//db_query($sql);
                        $toPaymentsLog['time_return'] = time();
                        $toPaymentsLog['is_tampered'] = 0;		
                        $toPaymentsLog['is_complete'] = 0;
                        $toPaymentsLog['is_flagged'] = 0;
                        $toPaymentsLog['response_code'] = 3;
                        $toPaymentsLog['response_message'] = $reason;
                        $toPaymentsLog['completed_via'] = 'Auto';
			func_array2updateWithTypeCheck("mk_payments_log", $toPaymentsLog,"orderid='$gcorderID'");
		}
		else{
                        $toPaymentsLog['returnTime'] = time();
                        $toPaymentsLog['isTampered'] = 0;		
                        $toPaymentsLog['isComplete'] = 0;
                        $toPaymentsLog['isFlagged'] = 0;
                        $toPaymentsLog['responseCode'] = 3;
                        $toPaymentsLog['responseMessage'] = $reason;
                        $toPaymentsLog['completedVia'] = 'Auto';
			PaymentServiceInterface::updatePaymentLog($gcorderID, $toPaymentsLog);
		}
		echo "redirect";
	} else {
		echo "Order status error";
	}
	
} else {
	echo "Orderid error";
}
exit;

?>
