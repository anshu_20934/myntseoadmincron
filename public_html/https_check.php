<?php

if (empty($_SERVER['HTTPS'])) {
    require_once __DIR__.'/env/Host.config.php';
    header('Location: https://' . HostConfig::$httpsHost . $_SERVER['REQUEST_URI']);
    exit;
}

