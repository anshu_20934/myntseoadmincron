<?php
require "./auth.php";
//putenv("TZ=Asia/Calcutta");
if($_GET['action'] == 'reterive')
{
	$ordertable = "";	
	$cdate = time();
	if($_GET['param'] == '7'){
		$pastdate = $cdate-(6*23*60*60);
		$all = false;
	}elseif($_GET['param'] == '1M'){
		$pastdate = $cdate-(30*23*60*60);
		$all = false;
	}elseif($_GET['param'] == '3M'){
		$pastdate = $cdate-(90*23*60*60);
		$all = false;
	}elseif($_GET['param'] == '1Y'){
		$pastdate = $cdate-(365*23*60*60);
		$all = false;
	}elseif($_GET['param'] == 'all'){
		$all = true;
	}elseif($_GET['param'] == '0'){
		$select = "nodata";
	}

	if(($select != "nodata") && ($all == true || $all == false)){

		$query =	"SELECT orderid, invoiceid, login, firstname, lastname, total, shipping_cost, tax, 
		date_format(FROM_UNIXTIME(date,'%Y-%m-%d'), '%d/%m/%Y') as date,
	    status,tracking,shipping_method, gift_status, gift_charges, cod, payment_method, b.courier_service,b.website 
	    FROM $sql_tbl[orders] a LEFT JOIN $sql_tbl[courier_service] b ON a.courier_service=b.code
	    WHERE login='$_GET[login]' AND status != 'PP' AND status != 'F' ";

		if($all == false){
			$query .= " AND (date>='$pastdate' AND date<='$cdate')";			
		}
			$query .= "ORDER BY orderid DESC";
		$sqllog->debug("File Name::>ajax_orderhistory.php######SQL ::>".$query);
		$result = func_query($query);
		if($result)
		{
				$ordertable .= "<div>";
				$ordertable .= "<table cellpadding='2' class='table'>";
				$ordertable .= "<tr class='font_bold'>";
				$ordertable .= "<td class='align_left' style='padding-left:7px;'>Order no.</td>";
				$ordertable .= "<td class='align_left' style='padding-left:7px;'>Invoice no.</td>";

				$ordertable .= "<td class='align_left'>Status</td>";
				//$ordertable .= "<td class='align_left'>Customer </td>";
				$ordertable .= "<td class='align_left'>Courier Service</td>";
				$ordertable .= "<td class='align_left'>Tracking No</td>";
				//$ordertable .= "<td class='align_left'>Date </td>";

				//$ordertable .= "<td class='align_right'>Status</td>";
				$ordertable .= "<td class='align_left'>Date </td>";

				$ordertable .= "<td class='align_right'>Total(in Rs.)</td>";
				$ordertable .= "</tr>";

			for($i=0; $i<count($result); $i++)
			{
				$arr = $result[$i];
				$amounttobepaid = $arr['total']+$arr['tax'];
				if($arr['payment_method'] == 'cod')
						$amounttobepaid = $amounttobepaid + $arr['cod'];
				$ordertable .= "<tr>";
				$ordertable .=	"<td class='align_left' style='padding-left:7px;'><a href=javascript:popUpWindow('mk_final_invoice.php?orderid=$arr[orderid]&t=d');><strong>#$arr[orderid]</strong></a></td>";
				$ordertable .="<td class='align_left'><a href=javascript:popUpWindow('mk_final_invoice.php?orderid=$arr[orderid]&t=u');><strong>$arr[invoiceid]</strong></a></td>";

				if($arr['status'] == "I") $ordertable .="<td class='align_left'>Finished</td>";
				elseif($arr['status'] == "Q") $ordertable .="<td class='align_left'>Queued</td>";
				elseif($arr['status'] == "P") $ordertable .="<td class='align_left'>Processed</td>";
				elseif($arr['status'] == "B") $ordertable .="<td class='align_left'>Backordered</td>";
				elseif($arr['status'] == "D") $ordertable .="<td class='align_left'>Declined</td>";
				elseif($arr['status'] == "F") $ordertable .="<td class='align_left'>Failed</td>";
				elseif($arr['status'] == "C") $ordertable .="<td class='align_left'>Complete</td>";
				elseif($arr['status'] == "PD") $ordertable .="<td class='align_left'>Pending Check Collection</td>";
				//elseif($arr['status'] == "CD") $ordertable .="<td class='align_left'>Pending COD Verification</td>";
                elseif($arr['status'] == "CD") $ordertable .="<td>
                                                 <form name='verifyform' action='mkOrderMobileVerification.php' method='POST'>
                                                 <input type='hidden' name='orderid' id='orderid' value='$arr[orderid]'>
                                                 <input class='submit' type='button'  id='submitButton' name='submitButton' value='Click For Verification' onclick=\"javascript:popUpWindow('mkOrderMobileVerification.php?orderid=$arr[orderid]'); \">
                                                 </form>
                                                 </td>";
				else $ordertable .="<td class='align_left'>Still to update</td>";
				
				//$ordertable .="<td class='align_left'>$arr[firstname] $arr[lastname] ($arr[login])</td>";
				$ordertable .="<td class='align_left'><a href='".$arr['website']."' target='_blank'><b>$arr[courier_service]</b></a></td>";
				if(!empty($arr['tracking']))
				{
					$trackingnum = $arr['tracking'];
					if ( $arr['courier_service'] == 'DTDC'){
							$ordertable .="<td class='align_left'><a href=javascript:trackDTDCStatus('$trackingnum');>$trackingnum</a></td>";
					}else {
							$ordertable .="<td class='align_left'>$trackingnum</td>";
					}
	}			
	else 
				    $ordertable .="<td class='align_left'>NA</td>"; 	
			
				$ordertable .="<td class='align_left'>$arr[date]</td>";

				$ordertable .="<td class='align_right'>$amounttobepaid</td>";
				$ordertable .= "</tr>";
			}
				$ordertable .="<tr class='font_bold'>";
				$ordertable .="<td class='align_right'>&nbsp;</td>";
				$ordertable .="<td class='align_right'>&nbsp;</td>";
				$ordertable .="<td class='align_right' colspan='3'>&nbsp;</td>";
				$ordertable .="<td class='align_right'>&nbsp;</td>";
				$ordertable .="<td class='align_right'>&nbsp;</td>";
				$ordertable .="</tr>";
				$ordertable .="<tr class='font_bold'>";
				$ordertable .="<td class='align_right'>&nbsp;</td>";
				$ordertable .="<td class='align_right'>&nbsp;</td>";
				$ordertable .="<td class='align_right' colspan='3'>&nbsp;</td>";
				$ordertable .="<td class='align_right'>&nbsp;</td>";
				$ordertable .="</tr>";
				$ordertable .="</table>";
				$ordertable .="</div>";
				echo $ordertable;
		}
	}
	else{
		echo $ordertable = '0';
	}
}
?>
