<?php
require "auth.php";
include_once("$xcart_dir/include/func/func.mk_orderbook.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/include/func/func.mkcore.php");
include_once("$xcart_dir/modules/apiclient/WarehouseApiClient.php");
include_once("$xcart_dir/modules/apiclient/PincodeApiClient.php");
include_once("$xcart_dir/modules/apiclient/UniqueInvoiceApiClient.php");
include_once($xcart_dir."/include/class/class.html2fpdf.php");
include_once($xcart_dir."/class.imageconverter.php");

if($_SERVER['SERVER_NAME'] == 'www.myntra.com'){
	echo "<meta http-equiv=\"Refresh\" content=\"0;URL=$http_location \" >";
    exit;
}

$invoiceType = $_GET['invoiceType'];
$copies      = $_GET['copies'];
$smarty->assign('invoiceType', $invoiceType);
$smarty->assign('print', $_GET['print']);
$orderids = trim($_GET['orderids']);
$orderids = explode(",", $orderids);
$original_orders = $orderids;
$warehouses = WarehouseApiClient::getAllWarehouses();
$whId2warehouse = array();
foreach($warehouses as $warehouse) {
	$whId2warehouse[$warehouse['id']] = $warehouse;
}

$orders = func_select_orders($orderids);
$orderid2OrderMap=array();
$logins = array();
$group_ids = array();
foreach($orders as $order) {
	if(!in_array($order['login'], $logins))
		$logins[] = $order['login'];
	if(!in_array($order['group_id'], $group_ids))
		$group_ids[] = $order['group_id'];
	
	$orderid2OrderMap[$order['orderid']] = $order;
}

$login2Userinfo = array();
if(!empty($logins)) {
	$userinfos = func_query("Select * from $sql_tbl[customers] where login in ('".implode("','", $logins)."')");
	if(!empty($userinfos)) {
		foreach($userinfos as $userinfo) {
			$login2Userinfo[strtolower($userinfo['login'])] = $userinfo;
		}
	}
}
if(!empty($group_ids)){
$groupid2MoreOrders = array();
$other_orders = func_query("Select orderid, group_id from xcart_orders where group_id in (".implode(",",$group_ids).") and status not in ('D', 'F')");
if($other_orders) {
        foreach($other_orders as $order) {
                if(!isset($groupid2MoreOrders[$order['group_id']]))
                        $groupid2MoreOrders[$order['group_id']] = array();

                $groupid2MoreOrders[$order['group_id']][] = $order;

                if(!in_array($order['orderid'], $orderids))
                        $orderids[] = $order['orderid'];
        }
	}
}

$weblog->info("OrderIds & OrderIds in Group - ". implode(",", array_keys($groupid2MoreOrders)));

$order2items = array();
$item_details = func_create_array_products_of_order($orderids);
foreach($item_details as $item) {
	if(!isset($order2items[$item['orderid']]))
		$order2items[$item['orderid']] = array();
	
	$order2items[$item['orderid']][] = $item;
}
$invoices = array();
$counter =0;
foreach($original_orders as $oid) {
	$order = $orderid2OrderMap[$oid];
	if(!$login2Userinfo[strtolower($order['login'])] || !$order2items[$order['orderid']]) {
		continue;
	}
	$counter++;
	$invoice = array();	
	$invoice['invoiceid'] = $order['invoiceid'];
	$invoice['orderid'] = $order['orderid'];	
	$invoice['group_id'] = $order['group_id'];	
	$invoice['gift_status'] = $order['gift_status'];
	$invoice['shipping_method'] = $order['shipping_method'];
	
	if($invoiceType == 'special'){
		$specialPincodeToWarehouseIdMap = json_decode(WidgetKeyValuePairs::getWidgetValueForKey('specialPincodeToWarehouseIdMap'), true);
		$specialPincodes = array_keys($specialPincodeToWarehouseIdMap);
		if(in_array($order['s_zipcode'], $specialPincodes)){
			$warehouseId = $specialPincodeToWarehouseIdMap["$order[s_zipcode]"];
			$warehouse = $whId2warehouse[$warehouseId];
                        $specialPincodeMap = json_decode(WidgetKeyValuePairs::getWidgetValueForKey('specialPincodeMap'), true);
                        $specialPincodeWhIds = array_keys($specialPincodeMap);
                        if(in_array($warehouseId, $specialPincodeWhIds)){
                            $map = $specialPincodeMap[$warehouseId];
                            $data = array();
                            foreach($map as $key => $value){
                                array_push($data, "$key: $value");
                            }
                            //For kerela orders, getting unique Invoice number

                            $uid = UniqueInvoiceApiClient::getUniqueInvoice($order['orderid'], $warehouseId);
                            if($uid == ''){
                                echo 'Unable to generate Unique Invoice Number';
                                exit;
                            }
                            $invoice['uniqueInvoiceId'] = $uid;
                            $invoice['specialPincodeData'] = $data;
                        }

		}else {
			$warehouse = $whId2warehouse[$order['warehouseid']];
		}
	} else {
		$warehouse = $whId2warehouse[$order['warehouseid']];
	}
	$invoice['warehouse'] = $order['warehouseid'];
	$invoice['weight'] = $order['weight'];
	$invoice['warehouse_tin'] = $warehouse['tinNumber'];
	$address = explode("\n", $warehouse['address']);
	
	if($invoiceType == 'finance'){
		$invoice['companyName'] = "Myntra Designs Pvt Ltd.";
		if($order['warehouseid'] == 1){
			$invoice['warehouse_addressline1'] = "3rd Floor AKR Teck Park";
			$invoice['warehouse_addressline2'] = "7th Mile, Krishna Reddy Industrial Area, Kudlu Gate";
			$invoice['warehouse_city'] = "Bangalore";
			$invoice['warehouse_zipcode'] = "560068";
			$invoice['warehouse_state'] = "Karnataka";
			$invoice['warehouse_tin'] = "29910754899";
		}
		else{
			$invoice['warehouse_addressline2'] = $address[0]." ".$address[1]." ".$address[2];
			$invoice['warehouse_city'] = $warehouse['city'];
			$invoice['warehouse_zipcode'] = $warehouse['postalCode'];
			$invoice['warehouse_state'] = $warehouse['state'];
			$invoice['warehouse_tin'] = "06391939110";
		}
		$invoice['warehouse_country'] = $warehouse['country'];
		$invoice['firstname'] = "Vector E-Commerce Pvt Ltd.";
		$invoice['address'] = $address[0]." ".$address[1]."\n".$address[2];
		$invoice['city'] = $warehouse['city'];
		$invoice['zipcode'] = $warehouse['postalCode'];
		$invoice['state'] = $warehouse['state'];
		$invoice['country'] = $warehouse['country'];
		$invoice['tinNumber'] = $warehouse['tinNumber'];;
		$invoice['invoiceid'] = "MYN".substr($invoice['invoiceid'], 7);
	}else{
		$invoice['companyName'] = "Vector E-Commerce Pvt Ltd.";
		$invoice['warehouse_addressline1'] = $address[0]." ".$address[1];
		$invoice['warehouse_addressline2'] = $address[2];
		$invoice['warehouse_city'] = $warehouse['city'];
		$invoice['warehouse_zipcode'] = $warehouse['postalCode'];
		$invoice['warehouse_state'] = $warehouse['state'];
		$invoice['warehouse_country'] = $warehouse['country'];
		$invoice['firstname'] = $order['s_firstname'];
		$invoice['lastname'] = $order['s_lastname'];
		$invoice['address'] = nl2br($order['s_address']);
		$invoice['locality'] = $order['s_locality'];
		$invoice['city'] = $order['s_city'];
		$invoice['zipcode'] = $order['s_zipcode'];
		$invoice['state'] = $order['s_statename'];
		$invoice['country'] = $order['s_countryname'];
		$invoice['phone'] = $order['mobile'];
		$userinfo = $login2Userinfo[$order['login']];
		$invoice['customer_phone'] = $userinfo['mobile'];
	}
	
	$itemsinorder = $order2items[$order['orderid']];

	if($order['queueddate'] != null) {
            if($order['shipping_method'] == 'XPRESS') {
		$invoice['promiseDate'] = date("dm", getDeliveryDateForOrder($order['queueddate'], $itemsinorder, $order['s_zipcode'], $order['payment_method'],$order['courier_service'], $order['warehouseid'], $order['orderid']));
            } else {
                $invoice['promiseDate'] = date("dm", getDeliveryDateForOrder($order['queueddate'], $itemsinorder, $order['s_zipcode'], $order['payment_method'],$order['courier_service'], $order['warehouseid'], $order['orderid']));
            }
	}
	
	$invoice['payment_method'] = $order['payment_method'];
	$invoice['ordertype'] = $order['ordertype'];
	
	$invoice['order_date'] = date('jS M, Y', $order['queueddate']); 
	if($order['shippeddate'] != NULL && $order['shippeddate'] != 0 && $invoiceType != 'special'){
		$invoice['invoice_date'] = date('jS M, Y', $order['shippeddate']);
    }
	else {
        $invoice['invoice_date'] = date('jS M, Y', time());
    }
	
	$invoice['payable_amount'] = number_format(round(func_get_order_payable_amount($order)), 2);

	$url = "{$baseurl}/include/class/class.barcode.php?code=" . $order['orderid'] . "&text=0&type=.png";
    $invoice['shipmentbarcodeUrl'] =  $url;
    
    $data = PincodeApiClient::getAreaDetailForPincode($order['courier_service'], trim($order['s_zipcode']));
    $city_area_code = "";
    if (!$data) {
        $city_area_code = $order['s_city'];
    } else {
        $city_area_code = $data[0][cityCode] . "/" . $data[0][areaCode];
        if($order['courier_service'] == 'FD'){
            $city_area_code = $data[0][areaCode];
        }
    }
    $invoice['city_area_code'] = $city_area_code;
    $invoice['courier_code'] = $order['courier_service'];
    $invoice['gift_card_amount'] = $order['gift_card_amount'];
    if(!empty($order['tracking'])) {
    	$invoice['tracking_no'] = $order['tracking'];
        if($order['courier_service'] == 'FD'){
            $astra_barcode = func_query_first_cell("select `value` from order_additional_info where order_id_fk = $order[orderid] and `key` = 'FF_ASTRA_BARCODE'");
            
            $invoice['tracking_code_url'] = "{$baseurl}/include/class/oms/128encoding/fdbarcode.php?code=".$astra_barcode."&type=.png";
            $invoice['tracking_no'] = $astra_barcode;
            
            if ($order['payment_method'] == 'cod') {
                $result = func_query("select `key`,`value` from order_additional_info where order_id_fk = $order[orderid] and `key` in ('COD_REVERSE_TRACKING_BARCODE','COD_REVERSE_TRACKING_FORMID')");
                if ($result && !empty($result)) {
                    $new = array();
                    foreach ($result as $item) {
                        foreach ($item as $key => $value) {
                            if ($key == 'key') {
                                $k = $value;
                            } else {
                                $new[$k] = $value;
                            }
                        }
                    }
                    $invoice['cod_tracking_no'] = $new['COD_REVERSE_TRACKING_BARCODE'];
                    $invoice['cod_tracking_code_url'] = "{$baseurl}/include/class/oms/128encoding/fdbarcode.php?code=" . $invoice['cod_tracking_no'] . "&type=.png";
                    $invoice['codFormId'] = $new['COD_REVERSE_TRACKING_FORMID'];
                }
            }
        } else {
            $invoice['tracking_code_url'] = "{$baseurl}/include/class/class.barcode.php?code=".$order['tracking']."&text=0&type=.png";
        }
    }
    
    $items_in_different_shipments = array();
    $other_orders = $groupid2MoreOrders[$order['group_id']];
    if($other_orders && !empty($other_orders)) {
    	foreach($other_orders as $other_order) {
    		if($other_order['orderid'] != $order['orderid']) {
	    		$other_order_items = $order2items[$other_order['orderid']];
	    		if($other_order_items && !empty($other_order_items)) {
	    			foreach($other_order_items as $item) 
	    				$items_in_different_shipments[] = $item;
	    		}
    		}
    	} 
    }
    $margin = explode(",", WidgetKeyValuePairs::getWidgetValueForKey('marginForWarehouse'));
    $showGovtTax = WidgetKeyValuePairs::getWidgetValueForKey('oms.invoiceShowGovtTax');
    if ($showGovtTax == NULL) {
        $showGovtTax = false;
    }
    $invoice['show_govt_tax'] = $showGovtTax;
    $orderdate = getdate($order['date']);
    $order['loyalty_credit'] = $order['loyalty_points_used']*$order['loyalty_points_conversion_factor'];
    $invoice['items_html'] = create_product_detail_table_new($order2items[$order['orderid']], $order, "email", "", $items_in_different_shipments, $invoiceType, $margin[$order['warehouseid']]);
    
    $smarty->assign("invoice", $invoice);
	$smarty->assign("pageno",$counter);
	$smarty->assign("totalpages",count($orders));
	$invoice_pages[] = func_display("order_invoice_new.tpl", $smarty, false);
} 

if(count($invoice_pages) > 1) {
	$content = implode("<div style='display:block; page-break-after:avoid;'>&nbsp;</div>", $invoice_pages);
} else {
	$content = $invoice_pages[0];
}

$smarty->assign("content", $content);

if($copies == null || $copies == '' || $copies == 0){
    $copies=1;
}
for($i=0;$i<$copies;$i++){
func_display("order_invoice_bulk.tpl", $smarty);
}
/*
if($skin === "skin2") {
    $html = $smarty->fetch("order_invoice_bulk.tpl");
} else {
    $html = func_display("order_invoice_bulk.tpl", $smarty,false);
}
require_once("$xcart_dir/include/mpdf/mpdf.php");

$mpdf = new mPDF('en-GB','A4','','',5,5,10,0,5,0);
$mpdf->WriteHTML($html);
$mpdf->Output("invoice.pdf","D");*/
?>
