<?php
define("ONE_CLICK", 1);
use enums\revenue\payments\Gateways;
use revenue\payments\gateway\ICICIBankGateway;
use revenue\payments\gateway\CITIBankGateway;
use web\utils\MobileConstants;
use abtest\MABTest;
use enums\cart\CartContext;
use revenue\payments\service\PaymentServiceInterface;
include_once("./auth.php");
include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
$tracker = new BaseTracker(BaseTracker::PAYMENT_PAGE);

include_once "$xcart_dir/include/func/func.mkcore.php";
include_once "$xcart_dir/include/func/func.randnum.php";
include_once "$xcart_dir/include/func/func.mk_orderbook.php";
include_once "$xcart_dir/include/class/class.mymyntra.php";
include_once "$xcart_dir/include/func/func.refer.php";
include_once "$xcart_dir/include/class/class.orders.php";
include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
include_once "$xcart_dir/modules/coupon/CouponValidator.php";
include_once "$xcart_dir/modules/coupon/exception/CouponNotFoundException.php";
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";
include_once "$xcart_dir/include/class/mcart/class.MCart.php";
include_once "$xcart_dir/modules/discount/DiscountEngine.php";
include_once "$xcart_dir/modules/coupon/CouponDiscountCalculator.php";
include_once "$xcart_dir/modules/coupon/CashCouponCalculator.php";
include_once "$xcart_dir/include/class/PaymentGatewayDiscount.php";
include_once "$xcart_dir/include/class/netbankingHelper.php";
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";
require_once "$xcart_dir/include/func/func.courier.php";
include_once "$xcart_dir/pincode_city_no_of_orders_cache.php";
include_once "$xcart_dir/modules/myntCash/MyntCashService.php";
include_once "$xcart_dir/include/func/func.loginhelper.php";

$expressBuyEnabledForUser = PaymentServiceInterface:: showExpressBuyForUser($login);
if(empty($expressBuyEnabledForUser)){
	func_header_location($http_location."/mkmycartmultiple.php?at=c&pagetype=productdetail&src=po",false);
}
//Check whether User account is suspended , if yes then invalidate the session and redirect to home page
$userStatus = func_query_first_cell("SELECT status FROM $sql_tbl[customers] WHERE login='$login'");
if($userStatus == 'N'){
    clearSessionVarsOnLogout(true);
    func_header_location($http_location,false);
}


if(empty($_SERVER['HTTPS'])){
	//Page to be accessed only over http redirect him to https
	 func_header_location($https_location .$_SERVER['REQUEST_URI'],false);
}

if($_SERVER['HTTP_HOST'] != $xcart_https_host){
	//redirect if someone opened with any other url
	func_header_location($https_location . $_SERVER['REQUEST_URI'],false);
}

$pageName = 'checkout';
include_once $xcart_dir."/webengage.php";

// User can choose a address to use, in that case we will send the address id, else assume it to be the default ..
if(isset($_GET['promptlogin'])){
    setMyntraCookie('__prompt_login',"1");
}

if(x_session_is_registered("lastAddressUsedID")){
	$lastAddressUsed = $XCART_SESSION_VARS['lastAddressUsedID'];
	x_session_unregister("lastAddressUsedID");
}
	
if(!empty($_GET['address'])){
	$address=mysql_real_escape_string($address);
	x_session_register("lastAddressUsedID", $address);
	$address = func_select_first('mk_customer_address', array('id' => $address));
} elseif(!empty($lastAddressUsed)){
	x_session_register("lastAddressUsedID", $lastAddressUsed);
	$address = func_select_first('mk_customer_address', array('id' => $lastAddressUsed));
} else {
    $address = func_select_first('mk_customer_address', array('login' => $login , 'default_address' => 1));
}
if (!empty($address) && ($address['country'] != 'IN' || !is_zipcode_servicable($address['pincode']))) {
    $address = array();
}
if(!empty($address)){
	$result = func_select_first('xcart_states', array('code' => $address['state']));
	$address['statename']= empty($result['state'])?$address['state']:$result['state'];;
	$result = func_select_first('xcart_languages', array('name' => 'country_'.$address['country'],'topic' => 'Countries'));
	$address['countryname']= $result['value'];


}
$hasAddress=true;
// If we don't find a address or user is trying to pass some other ID, kick the user back to address page ..
if(empty($address) || strcasecmp($login,$address['login']) !=0) {
    if ($skin === "skinmobile")
	    func_header_location($https_location . "/checkout-address.php?change=1&expressbuy",false);
	//set smarty variable to show the add address form; 
	$address = array();
	
	$hasAddress=false;
	
}



$couponAdapter = CouponAdapter::getInstance();

$mcartFactory= new MCartFactory();
$myCart= $mcartFactory->getCurrentUserCart(false,CartContext::OneClickContext);
//var_dump($myCart);
if($myCart!=null)
	$cartItems=$myCart->getProducts();
if( $myCart==null || empty($cartItems) )
{
	//stale cart , cart is made empty from abother tab/window - cannot proceed. Rebuild the cart again
	func_header_location($http_location."/mkmycartmultiple.php?at=c&pagetype=productdetail&src=po",false);
}

//function to replace last occurance of a search in a string
function stringLastReplace($search, $replace, $subject)
{
    $pos = strrpos($subject, $search);

    if($pos === false)
    {
        return $subject;
    }
    else
    {
        return substr_replace($subject, $replace, $pos, strlen($search));
    }
}


//repeat the logic to recalculate the price here
function getCouponType($type,$MRPAmount,$MRPpercentage)
{
    switch ($type) {
        case "absolute" :
            return "Rs " . $MRPAmount . " off";
            break;
        case "percentage" :
            return $MRPpercentage . "% off";
            break;
        case "dual" :
            return $MRPpercentage . "% off upto Rs "
                . $MRPAmount;
    }
}
$smsVerifier = SmsVerifier::getInstance();
$couponAdapter = CouponAdapter::getInstance();

$mymyntra = new MyMyntra(mysql_real_escape_string($XCART_SESSION_VARS['login']));

$userProfileData = $mymyntra->getUserProfileData();
$smarty->assign("codWindow","displayCaptchaScreen");
$smarty->assign("notAllowedMsg","Access not allowed");

switch($mode) {
    case "verify-mobile-code" :
        {

            $userProfileData = $mymyntra->getUserProfileData();
            $code = mysql_real_escape_string($_POST['mobile-code']);
            if (!empty($code)) {
                $ret = $smsVerifier->verifyMobileCode($userProfileData['mobile'], $code,$userProfileData['login']);

                $weblog->info("ret: >>$ret<<");

                if ($ret) {
                    $verStatus = $smsVerifier->verifyMapping($userProfileData['mobile'], $userProfileData['login']);

                    if ($verStatus) {
                    	$returnCode = 1;
                        $returnMsg = "Mobile number successfully verified.";
                    }
                    else {
                    	$returnCode = 2;
                        $returnMsg = "The mobile number is registered with Myntra for another customer. Please retry using another mobile number.";
                    }
                }
                else {
					$num_attempts = $smsVerifier->getAttempts($userProfileData['mobile'], $userProfileData['login']);
					if($num_attempts == 4) {
						$smsVerifier->lockMobileLogin($userProfileData['mobile'], $userProfileData['login']);
					}
                    $returnCode = 0;
                    $returnMsg = "Mobile code verification failed. Please retry again.";
                }
            }
            $msg=array("status"=>$returnCode,"message"=>$returnMsg,"attempts"=>$num_attempts);
            echo json_encode($msg);
            exit;
            break;
        }
    case "update-mobile-number" :
    	{
    		$mobile = mysql_real_escape_string($_POST['new-mobile-number']);
    		$login= mysql_real_escape_string($_POST['login']);

			$mobile = substr($mobile, -10);
            $actionResult = $mymyntra->updateProfile(array ('mobile' => $mobile));

            global $skipMobileVerification;
            $smsVerifier->addNewMapping($mobile,$login);
            if($skipMobileVerification){
            	$smsVerifier->skipVerification($mobile,$login);
            }

            $verified_for = $smsVerifier->getVerifiedLoginFor($mobile, true);
            if(empty($verified_for)){
            	// number up for grab
            	echo "0";
            	exit;
            }

            if($verified_for != $login){
            	echo "1"; //error
            	exit;
            }

    		break;
    	}
    case "verify-mobile":
    	{
    		$login= mysql_real_escape_string($_POST['login']);
    		$mobile= mysql_real_escape_string($_POST['mobile']);
    		$retval = $smsVerifier->verifyViaCellNext($mobile, $login, "cod");
    		echo $retval;
    		exit;
    		break;
    	}
}

// Instance of the coupon database liaison.
$adapter = CouponAdapter::getInstance();

// Coupons widget.
// cashcoupons deprecated, use MyntCash
//$cashcoupons = $adapter->getMyntraCreditsCoupon($XCART_SESSION_VARS["login"]);

$coupons = $adapter->getCouponsForCartPage($XCART_SESSION_VARS["login"], $myCart, $skin);
$rest_coupons = array();
$top_coupons = array();
$showVerifyMobileMsg=false;
for ($i = 0; $i < count($coupons); $i++)
{
	if(in_array($coupons[$i]['groupName'], array('FirstLogin', 'MrpReferrals')))
	    $showVerifyMobileMsg=true;

    $couponTypeDescription=getCouponType($coupons[$i]['couponType'],$coupons[$i]['MRPAmount'],$coupons[$i]['MRPpercentage']);
    $coupons[$i]['couponTypeDescription']=$couponTypeDescription;
}
if(!empty($applied) && !empty($couponCode))
{
        for ($i = 0; $i < count($coupons); $i++)
        {
            if($coupons[$i]['coupon']==$couponCode)
            {
                $top_coupons= $coupons[$i];
				break;
            }
        }
}
for ($i = 0; $i < count($coupons); $i++) {
        if($coupons[$i]['coupon']==$couponCode)continue;
        array_push($rest_coupons, $coupons[$i]);
}
$smarty->assign("showVerifyMobileMsg", $showVerifyMobileMsg);
$smarty->assign("cashcoupons", $cashcoupons[0]);
$smarty->assign("rest_coupons", $rest_coupons);
$smarty->assign("top_coupons", $top_coupons);
//coupon widget ends


$calcCouponDiscount = false;
$showCouponMessage = false;
$calcCashCouponDiscount = false;
$cashCouponMessage = false;
//coupone code logic


if (!empty($HTTP_POST_VARS["couponcode"])) {
	$couponCode = $HTTP_POST_VARS["couponcode"];
	$myCart->addCouponCode($couponCode, MCart::$__COUPON_TYPE_DEFAULT);
}else if(!empty($HTTP_POST_VARS["cashcoupon"]) && $HTTP_POST_VARS["cashcoupon"]=='removecoupon'){
	$cashCouponCodeArrayRem=array_keys($myCart->getAllCouponCodes(MCart::$__COUPON_TYPE_CASHBACK));
	$myCart->removeCoupon($cashCouponCodeArrayRem[0], MCart::$__COUPON_TYPE_CASHBACK);
	$smarty->assign("appliedCashbackCode", "");
}
else if(!empty($HTTP_POST_VARS["useMyntCash"]) && $HTTP_POST_VARS["useMyntCash"]=='use'){
	//$cashCouponCode = $HTTP_POST_VARS["cashcoupon"];
	$userCashAmount = $HTTP_POST_VARS["userAmount"];
	$myCart->enableMyntCashUsage($userCashAmount);
	//$myCart->addCouponCode($cashCouponCode, MCart::$__COUPON_TYPE_CASHBACK,array('userCashAmount'=>$userCashAmount));
}else if(!empty($HTTP_POST_VARS["useMyntCash"]) && $HTTP_POST_VARS["useMyntCash"]=='remove'){
	$myCart->removeMyntCashUsage();
	//$cashCouponCodeArrayRem=array_keys($myCart->getAllCouponCodes(MCart::$__COUPON_TYPE_CASHBACK));
	//$myCart->removeCoupon($cashCouponCodeArrayRem[0], MCart::$__COUPON_TYPE_CASHBACK);
	//$smarty->assign("appliedCashbackCode", "");
}
else if(!empty($HTTP_POST_VARS["removecouponcode"]) && $HTTP_POST_VARS["removecouponcode"]=='remove')
{
	$couponCodeArrayRem=array_keys($myCart->getAllCouponCodes(MCart::$__COUPON_TYPE_DEFAULT));
	$myCart->removeCoupon($couponCodeArrayRem[0], MCart::$__COUPON_TYPE_DEFAULT);
	$smarty->assign("appliedCouponCode", "");
}else{
	//$myCart->addCouponCode($cashcoupons[0]['coupon'], MCart::$__COUPON_TYPE_CASHBACK,array('userCashAmount'=>$cashcoupons[0]['MRPAmount']));
	//$smarty->assign("appliedCashbackCode", "");
	
	//apply cashback/myntcash by default
	if($myntCashDetails["balance"]>0){
		$myCart->enableMyntCashUsage($myntCashDetails["balance"]);
	}	
	
}

$myCart->refresh(true,true,false,true);

if(!$myCart->isReadyForCheckout()){
	MCartUtils::persistCart($myCart);
	func_header_location($https_location."/mkmycartmultiple.php?at=c&pagetype=productdetail&src=po",false);
}
$productsInCart = MCartUtils::convertCartToArray($myCart);
$smarty->assign("cartLevelDiscount", $cartDiscount);
$cartDisplayData = MCartUtils::getCartDisplayData($myCart);
if(!empty($cartDisplayData) && $cartDisplayData->id=="Voucher_Cart_CartLevel_CM"){
    foreach ($cartDisplayData->params as $k => $v) {
        $smarty->assign("dre_cart_".$k, $v);
    }
    $smarty->assign("cartDiscountMessage", $_rst[$cartDisplayData->id]);
    $smarty->assign("cartVoucherLinkTT",$_rst[$cartDisplayData->tooltip_id]);
}


$discountCouponArray=array_keys($myCart->getAllCouponCodes(MCart::$__COUPON_TYPE_DEFAULT));
//Currently We have just one coupon for each type
$discountCouponCode=$discountCouponArray[0];

if(!empty($discountCouponCode)){
	$smarty->assign("appliedCouponCode", $discountCouponCode);
	$smarty->assign("couponcode", $discountCouponCode);
	$applicationCode = $myCart->getCouponApplicationCode($discountCouponCode, MCart::$__COUPON_TYPE_DEFAULT);
	if($applicationCode===MCart::$__COUPON_APP_CODE_SUCCESS){
		$discount = number_format($myCart->getDiscount(), 2, ".", ',');
		if($skin === "skin2") {
			$couponMessage = "You have saved <em>Rs. $discount</em> by using coupon $discountCouponCode";
		} else {
			$couponMessage = "<font color=\"green\"  style=\"display:inline\">Congratulations! You have "
				. "successfully saved Rs. $discount by using coupon "
				. "<b>".strtoupper($discountCouponCode)."</b></font>";
		}
		$smarty->assign("divid", "success");
	}else{
		if($skin === "skin2") {
			$couponMessage = $myCart->getCouponApplicationErrorMessage($discountCouponCode,  MCart::$__COUPON_TYPE_DEFAULT);
		}else{
			$couponMessage = "<font color=\"red\">".$myCart->getCouponApplicationErrorMessage($discountCouponCode,  MCart::$__COUPON_TYPE_DEFAULT)."</font>";
		}
		$myCart->removeCoupon($discountCouponCode, MCart::$__COUPON_TYPE_DEFAULT);
		$smarty->assign("divid", "error");
	}

	$showCouponMessage=true;
}
/*cashback coupons are deprecated
 * $cashCouponCodeArray=array_keys($myCart->getAllCouponCodes(MCart::$__COUPON_TYPE_CASHBACK));
//Currently We have just one coupon for each type

$cashCouponCode=$cashCouponCodeArray[0];*/
/*	
if(!empty($cashCouponCode)){
	$showCashCouponMessage=true;
	$smarty->assign("appliedCashbackCode", $cashCouponCode);
	$applicationCode = $myCart->getCouponApplicationCode($cashCouponCode, MCart::$__COUPON_TYPE_CASHBACK);
	if($applicationCode===MCart::$__COUPON_APP_CODE_SUCCESS){
		$cashdiscount = number_format($myCart->getCashDiscount(), 2, ".", ',');
		$smarty->assign("cashdiscount", $cashdiscount);
		$abs_discount=$cashcoupons[0]['MRPAmount'];
		if($cashdiscount <= $abs_discount)
		{
			$cash_left=$abs_discount - $cashdiscount;
			$cashCouponDesc= "Use your Myntra credits. Current balance: Rs $cash_left ";
		}
	    if($skin === "skin2") {
			$cashCouponMessage = "You have redeemed <em>Rs. $cashdiscount</em> of your <em>Rs. ".number_format($cashcoupons[0]['MRPAmount'],2)."</em> cashback";
		} else {	
			$cashCouponMessage ="<font color=\"green\">Congratulations! You have "
			. "successfully saved  Rs. $cashdiscount by using cashback"
			. "</font>";
		}
		$smarty->assign("cashdivid", "success");
	}else{
		if($skin === "skin2") {
			$cashCouponMessage = $myCart->getCouponApplicationErrorMessage($cashCouponCode,  MCart::$__COUPON_TYPE_CASHBACK);
		}else{
			$cashCouponMessage = "<font color=\"red\">".$myCart->getCouponApplicationErrorMessage($cashCouponCode,  MCart::$__COUPON_TYPE_CASHBACK)."</font>";
		}
		$myCart->removeCoupon($cashCouponCode, MCart::$__COUPON_TYPE_CASHBACK);
		$smarty->assign("cashdivid", "error");
	}
}
*/

/* fetch balance from MyntCashService */
$myntCashDetails = MyntCashService::getUserMyntCashDetails($login);
$smarty->assign("myntCashDetails",$myntCashDetails);


if($myCart->isMyntCashUsed() && $myCart->getTotalMyntCashUsage() >0){
	$showCashCouponMessage=true;
	//$smarty->assign("appliedCashbackCode", $cashCouponCode);
	//$applicationCode = $myCart->getCouponApplicationCode($cashCouponCode, MCart::$__COUPON_TYPE_CASHBACK);
	
	$myntCashUsage = number_format($myCart->getTotalMyntCashUsage(), 2, ".", ',');
	$smarty->assign("myntCashUsage", $myntCashUsage);
	/*$abs_discount=$cashcoupons[0]['MRPAmount'];
		if($cashdiscount <= $abs_discount)
		{
			$cash_left=$abs_discount - $cashdiscount;
			$cashCouponDesc= "Use your Myntra credits. Current balance: Rs $cash_left ";
		}*/
		$myntCashUsageMessage = "You have redeemed <em>Rs. $myntCashUsage</em> of your <em>Rs. ".number_format($myntCashDetails['balance'],2)."</em> cashback";
		$smarty->assign("myntCashdivid", "success");
		$smarty->assign("myntCashUsageMessage", $myntCashUsageMessage);
		
}



MCartUtils::persistCart($myCart);
x_session_unregister("LAST_CART_UPDATE_TIME");
unset($XCART_SESSION_VARS["LAST_CART_UPDATE_TIME"]);
$lastUpdatedTime=$myCart->getLastContentUpdatedTime();
x_session_register('LAST_CART_UPDATE_TIME',$lastUpdatedTime);


$additionalCharges = number_format($myCart->getAdditionalCharges(), 2, ".", '');
$totalMRP = number_format($myCart->getMrp(), 2, ".", '');
$vat = number_format($myCart->getVat(), 2, ".", '');
$totalQuantity = number_format($myCart->getItemQuantity(), 0);
$couponDiscount = number_format($myCart->getDiscount(), 2, ".", '');
$cashDiscount = number_format($myCart->getCashDiscount(), 2, ".", '');
$myntCashUsage = number_format($myCart->getTotalMyntCashUsage(), 2, ".", '');
$productDiscount = number_format($myCart->getDiscountAmount(), 2, ".", '');
$shippingCharge = number_format($myCart->getShippingCharge(), 2, ".",'');
$cartDiscount = number_format($myCart->getCartLevelDiscount(), 2, ".", '');
$giftCharges = number_format($myCart->getGiftCharge(), 2, ".",'');
$totalAmount = $totalMRP + $additionalCharges - $productDiscount - $couponDiscount - $myntCashUsage + $shippingCharge - $cartDiscount + $giftCharges;

$totalItemAmount = $totalMRP + $additionalCharges - $productDiscount;

$coupondiscount = $couponDiscount;
$cashdiscount = $cashDiscount;
$mrp = $totalMRP;
$amount = $totalMRP + $additionalCharges;
$cashback_tobe_given=$myCart->getCashbackToGivenOnCart();
$nonDiscountedItemAmount=$myCart->getNonDiscountedItemsSubtotal();
$nonDiscountedItemCount=$myCart->getNonDiscountedItemCount();
//Here assign the variables for coupon applicability
$couponApplicableItemAmount=$myCart->getCouponApplicableItemsSubtotal();
$couponApplicableItemCount=$myCart->getCouponApplicableItemCount();
$couponAppliedItemsCount=$myCart->getCouponAppliedItemCount();
$couponAppliedSubtotal=$myCart->getCouponAppliedSubtotal();


$giftWrapEnabled = FeatureGateKeyValuePairs::getBoolean('giftwrap.enabled');
$smarty->assign('giftWrapEnabled', $giftWrapEnabled);

$giftingCharges = WidgetKeyValuePairs::getInteger('giftwrap.charges');
$smarty->assign('giftingCharges', $giftingCharges);


if($showCouponMessage) {
	$smarty->assign("couponMessage", $couponMessage);
}
if($showCashCouponMessage) {
	$smarty->assign("cashCouponMessage", $cashCouponMessage);
}
if(!empty($cashback_tobe_given))
        $smarty->assign("cashback_tobe_given",$cashback_tobe_given);


$smarty->assign("cashCouponDesc", $cashCouponDesc);

if( $totalAmount < 1) { //less than 1re payment is not accepted by banks to making the amount to zero in case final amount falls below 1re.
	$totalAmount = 0;
}

$totalAmount = round($totalAmount);
$vat = ($vat < 0 ? 0 : $vat);
global $enable_cashback_discounted_products;

if(!empty($enable_cashback_discounted_products)){
	if($enable_cashback_discounted_products == "true")	{
		$totalCashBackAmount = $totalAmount;
	} else {
		$totalCashBackAmount = $myCart->getTotalCashBackAmount();
	}
} else {
	$totalCashBackAmount = $myCart->getTotalCashBackAmount();
}

if($totalCashBackAmount < 0)	{
	$totalCashBackAmount = 0;
}

//end recalculation of price.


//get countries list
$returnCountry = func_get_countries();
// load shipment options
$shippingoptions = func_load_shipping_options();

$XCART_SESSION_VARS['selected_address'] = $_GET['address'];

$smarty->assign("addressID",$_GET['address']);

//load customer details
$shippingCity = strtolower($address ['city']);
$country = $address['country'];
$state = $address['statename'];

$coupon = null;
$couponCode = $XCART_SESSION_VARS['couponCode'];
if(!empty($couponCode)) {
    try {
        $coupon = $couponAdapter->fetchCoupon($couponCode);
    }
    catch (CouponNotFoundException $e) {
        $coupon = null;
    }
}

$logisticId = $shippingoptions[0]['code'] ;

//$shippingRate = func_shipping_rate_re($country,$state, $shippingCity, $productsInCart, $productids, $coupon,$logisticId, null ,$amount);
//$shippingTotalAmount = number_format($shippingRate['totalshippingamount'],2,".",'');
$box_count = $shippingRate['box_count'];
$smarty->assign("shippingRate", $shippingCharge);
$smarty->assign("box_count",$box_count);

$amountForCitiDiscount = $totalAmount + $coupondiscount;
$smarty->assign("amountForCitiDiscount",$amountForCitiDiscount);
$smarty->assign("amount",$amount);
$smarty->assign("vat",$vat);
$smarty->assign("mrp",$mrp);
$smarty->assign("couponCode",$couponCode);
$smarty->assign("coupondiscount",$coupondiscount);
$smarty->assign("couponAppliedItemsCount", $couponAppliedItemsCount);
$smarty->assign("couponAppliedSubtotal", $couponAppliedSubtotal);
$smarty->assign("cashdiscount",$myntCashUsage);
$smarty->assign("coupondiscounttodisplay",$coupondiscounttodisplay);
$smarty->assign("couponpercent", $couponpercent);
$smarty->assign("totalAmount",$totalAmount);
$smarty->assign("totalItemAmount",$totalItemAmount);
$smarty->assign("totalQuantity",$totalQuantity);
$smarty->assign("productDiscount",$productDiscount);
$smarty->assign("cartLevelDiscount",$cartDiscount);
$smarty->assign("totalCashBackAmount", $totalCashBackAmount);
$smarty->assign("nonDiscountedItemAmount", $nonDiscountedItemAmount);
$smarty->assign("nonDiscountedItemCount", $nonDiscountedItemCount);
// Add two more variables couponApplicableItemCount
$smarty->assign("couponApplicableItemAmount", $couponApplicableItemAmount);
$smarty->assign("couponApplicableItemCount", $couponApplicableItemCount);

$condNoDiscountCouponFG = FeatureGateKeyValuePairs::getBoolean('condNoDiscountCoupon');
$smarty->assign('condNoDiscountCouponFG', $condNoDiscountCouponFG);
$condApplyOnAllCoupon = FeatureGateKeyValuePairs::getBoolean('condApplyOnAllCoupon');
$smarty->assign('condApplyOnAllCoupon',$condApplyOnAllCoupon);

$styleExclusionFG = FeatureGateKeyValuePairs::getBoolean('styleExclusion');
$smarty->assign('styleExclusionFG', $styleExclusionFG);

$noStyleExclusionForUserCoupon = FeatureGateKeyValuePairs::getBoolean('noStyleExclusionForUserCoupon');
$smarty->assign('noStyleExclusionForUserCoupon', $noStyleExclusionForUserCoupon);

$noStyleExclusionForSomeGroups = FeatureGateKeyValuePairs::getBoolean('noStyleExclusionForSomeGroups');
$smarty->assign('noStyleExclusionForSomeGroups', $noStyleExclusionForSomeGroups);

$couponExcludedProductsMsg = WidgetKeyValuePairs::getWidgetValueForKey('cart-tooltip-for-coupon');
$smarty->assign('couponExcludedProductsMsg', $couponExcludedProductsMsg);

//PORTAL-667: Kundan: made order of online payments configurable via admin interface
$defaultPaymentOrder = array("credit_card", "emi", "debit_card", "net_banking", "cod","pay_by_phone");
$payment_options = FeatureGateKeyValuePairs::getStrArray("PaymentOptionsOrder", $defaultPaymentOrder);
if($myCart->getIsGiftOrder()){
	foreach($payment_options as $i => $payment_option){
		if($payment_option == 'cod'){
			unset($payment_options[$i]);
			break;
		}
	}
	$payment_options = array_values($payment_options);
}
$smarty->assign("payment_options",$payment_options);
//Kundan: the selected payment option will always be the first payment option
$smarty->assign("defaultPaymentType", $payment_options[0]);
if(isset($_GET['payment_option'])&& ($_GET['payment_option'] == 'cod')) $smarty->assign("defaultPaymentType", "cod");

$zipcode = strtolower($address['pincode']);
$mobile = $address['mobile'];

// For COD option, we maintain a codErrorCode, and codErrorMessage.
// The codErrorCode value can be used for better alignment of some information shown in the payment.tpl / codlearnmore.tpl
$codErrorCode = 0;
$codErrorMessage = '';
//$jewelleryItems = explode(',', trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('jewellery.article.types')));
$jewelleryItemsInOrder = 'none'; // other values can be 'some', 'all'
$jewelleryItemsCount = 0;

foreach($productsInCart as $product){
	$fineJewelleryItem = func_query("select * from mk_attribute_type at, mk_attribute_type_values av, mk_style_article_type_attribute_values sav where at.id = av.attribute_type_id and av.id = sav.article_type_attribute_value_id and at.catalogue_classification_id = ".$product['article_type']." and sav.product_style_id = ".$product['productStyleId'] ." and av.attribute_value = 'Fine Jewellery'", true);
	if($fineJewelleryItem && count($fineJewelleryItem)>0){
		$jewelleryItemsCount++;
		$jewelleryItemsInOrder = 'some';
	}
}

if($jewelleryItemsCount == count($productsInCart)){
	$jewelleryItemsInOrder = 'all';
}

//this block below is used for first time user who doesnt have address. By pass Country and zipcode and mobile
if(empty($hasAddress)){
        $mobile=$userProfileData["mobile"];
        $countryForCODCheck= 'IN';
}
else {
        $countryForCODCheck=$address['country'];
}

$codElegiblity = verifyCODElegiblityForUser($login,$mobile,$countryForCODCheck,$totalAmount,$zipcode,$productsInCart,$productids,$cashDiscount,$jewelleryItemsInOrder);
$codErrorCode = $codElegiblity['errorCode'];
$codErrorMessage = $codElegiblity['errorMessage'];

x_session_unregister("codErrorCode");
x_session_register("codErrorCode",$codErrorCode);

if(!empty($zipcode)){
	$onlineServiceable = func_check_prepaid_serviceability($zipcode,$productsInCart);
}
else {
	$onlineServiceable=true;	
}
if(!$onlineServiceable) {
	$onlineErrorMessage = "Prepaid delivery is not available for your Pincode.";
	$smarty->assign("onlineErrorMessage",$onlineErrorMessage);
}

if(!empty($codErrorMessage)){
	$codErrorMessage.="<br/><br/>";
	$codErrorMessage.="<b>You may pay for this order using any of our other payment modes.</b><br/><br/>";
	$codErrorMessage.="Have a query? Our Customer Champions are available (".$customerSupportTime.") and will be glad to help you with completing this order or answer any other questions that you may have. You can reach our Customer Champion at ".$customerSupportCall." or <a href=\"mailto:support@myntra.com\" target=\"_blank\">support@myntra.com</a>.";
}

$smarty->assign("codErrorCode",$codErrorCode);
$smarty->assign("codErrorMessage",$codErrorMessage);
$smarty->assign("codCreditBalance",$codCreditBalance);
$smarty->assign("loginid",$login);
$smarty->assign("logisticId", $logisticId);

//Geting the @myntra.com login
$myntraSufix=0;
$sufixLogin = strrpos($login,"@myntra.com");
if((strlen($login)-11)==$sufixLogin){
	$myntraSufix = 1;
}

$smarty->assign("myntraLoginSuffix",$myntraSufix);

if($shippingCity!=null){
	$smarty->assign("s_city",1);
} else {
	$smarty->assign("s_city",0);
}

$checksumkey = uniqid();
$smarty->assign("checksumkey", $checksumkey);

// For PTG specific campaign.
global $system_ptg_campaign;
if ($system_ptg_campaign) {
    if (!empty($XCART_SESSION_VARS["qualified_for_ptg_campaign"])) {
        $smarty->assign("ptg_campaign_qualified", true);
        global $system_ptg_campaign_threshold;
        $smarty->assign("ptg_threshold", $system_ptg_campaign_threshold);
    }
}

$address['address2'] = $address['address'];
$address['address2'] = str_replace("\r\n", ',', $address['address2']);

$smarty->assign("address",$address );
$smarty->assign("countries", $returnCountry);
$smarty->assign("countriesJson",json_encode($returnCountry));
if(isset($_GET['transaction_status'])&& ($_GET['transaction_status'] == 'EMI' ||$_GET['transaction_status'] == 'B' || $_GET['transaction_status'] == 'N')) $smarty->assign("transaction_status",$_GET['transaction_status']);

if(!empty($_GET['vbv'])&&$_GET['vbv'] =='F' ) {
	$smarty->assign("vbvfail",true);
} else {
	$smarty->assign("vbvfail",false);
}

$bankVBVLink = null;

$paymentServiceEnabled = PaymentServiceInterface::getPaymentServiceEnabledForUser($login);
if($paymentServiceEnabled) {
	$weblog->info("Payment service used for login $login as per featuregate");
}


if(!empty($_GET['bank_name'])) {
	$bank_name = trim(strtoupper($_GET['bank_name']));
	$bankVBVLink = func_query_first_cell("select card_bank_registration_link from mk_cards_domestic_bins where card_bank_code='$bank_name' limit 1");
}

if(!empty($bankVBVLink)) {
	$smarty->assign("bankVBVLink", $bankVBVLink);
	$smarty->assign("bankName", $bank_name);
} else {
	$smarty->assign("bankVBVLink", false);
}

$icici_gateway_up = FeatureGateKeyValuePairs::getBoolean('payments.iciciPG');
$axis_gateway_up =  FeatureGateKeyValuePairs::getBoolean('payments.axisPG');
$icici_gateway_available = FeatureGateKeyValuePairs::getBoolean('payments.isICICIPGAvailable');
$axis_gateway_available = FeatureGateKeyValuePairs::getBoolean('payments.isAxisPGAvailable');
$citi_gateway_up = FeatureGateKeyValuePairs::getBoolean('payments.citiPG');
$citi_gateway_available = FeatureGateKeyValuePairs::getBoolean('payments.isCITIPGAvailable');

$smarty->assign("is_icici_gateway_up", (($icici_gateway_up&&$icici_gateway_available)||($axis_gateway_up&&$axis_gateway_available)));


$citi_discount = FeatureGateKeyValuePairs::getInteger('payments.citiBankCardDiscount');
//additional discount for citibank
if($citi_discount>0&&$amountForCitiDiscount>1000) {
	$smarty->assign("citi_discount", $citi_discount);
} else {
	$smarty->assign("citi_discount", 0);
}

$icici_discount = FeatureGateKeyValuePairs::getInteger('payments.iciciBankCardDiscount');
//additional discount for citibank
if($icici_discount>0) {
	$smarty->assign("icici_discount", $icici_discount);
} else {
	$smarty->assign("icici_discount", 0);
}

if(isset($XCART_SESSION_VARS["oneClickCODElegible"])){
	$checkCODElegible = $XCART_SESSION_VARS["oneClickCODElegible"];
} else {
	$checkCODPaid = func_query_first_cell("select login from xcart_orders where login='$login' and payment_method='cod' and (cod_pay_status='paidtobluedart' or cod_pay_status='paid') limit 1");
    x_session_unregister("oneClickCODElegible");
    if(!empty($checkCODPaid)){
    	x_session_register("oneClickCODElegible","true");
    } else {
    	x_session_register("oneClickCODElegible","false");
    }
    $checkCODElegible = $XCART_SESSION_VARS["oneClickCODElegible"];
}

$oneClickCODEnabled = FeatureGateKeyValuePairs::getBoolean('oneClickCOD');
$oneClickCOD = false;
if($oneClickCODEnabled&&$checkCODElegible == "true" && $codErrorCode == 0) {
	$smarty->assign("oneClickCod", 1);
	$oneClickCOD = true;
} else {
       $smarty->assign("oneClickCod", 0);
}
if($codErrorCode == 0 && !$onlineServiceable) {
	$smarty->assign("oneClickCod", 1);
	$oneClickCOD = true;
}

$codCharge = FeatureGateKeyValuePairs::getInteger('codCharge');
//additional discount for citibank
if($codCharge>0) {
	$smarty->assign("cod_charge", $codCharge);
} else {
	$smarty->assign("cod_charge", 0);
}

// SMS verification overlay.
$login = $XCART_SESSION_VARS["login"];
$query = "SELECT * FROM xcart_customers WHERE login = '$login';";
$userProfileData = func_query_first($query);
$smarty->assign("userProfileData", $userProfileData);

// Mobile verification status.
$mobile_status = $smsVerifier->getStatus($userProfileData['mobile'], $userProfileData['login']);
global $skipMobileVerification;
if($mobile_status != 'V' && $skipMobileVerification){
	$smsVerifier->skipVerification($userProfileData['mobile'], $userProfileData['login']);
	$mobile_status = $smsVerifier->getStatus($userProfileData['mobile'], $userProfileData['login']);
}

// Show verify button.
$verified_for_another = 0;
$verifiedLogin = $smsVerifier->getVerifiedLoginFor($userProfileData['mobile']);
if ($verifiedLogin !== null && $verifiedLogin != $userProfileData['login']) {
    $verified_for_another = 1;
}
$smarty->assign("verified_for_another", $verified_for_another);
$smarty->assign("mobile_status", $mobile_status);
$smarty->assign("userMobile",$userProfileData['mobile']);


$payByPhoneNumber = WidgetKeyValuePairs::getWidgetValueForKey('payByPhoneNumber');
if($payByPhoneNumber != NULL) {
	$smarty->assign("pay_by_phone_number", $payByPhoneNumber);
} else {
	//fall back should not happen
	$smarty->assign("pay_by_phone_number", $customerSupportCall);
}



$citiDiscountBins = PaymentGatewayDiscount::$cardsBin['CITI'];
$iciciDiscountBins = PaymentGatewayDiscount::$cardsBin['ICICI'];
$smarty->assign("citi_discount_bins", $citiDiscountBins);
$smarty->assign("icici_discount_bins", $iciciDiscountBins);


$bank_list_text = NetBankingHelper::getNetBankingSelectBoxText();
$smarty->assign("bank_list_text",$bank_list_text);

$netbankingDownBankList = trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('payments.netBankingDownBanks'));
$creditDebitDownBankList = trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('payments.creditDebitDownBanks'));

if(!empty($netbankingDownBankList)) {
	$netbankingDownBankListStr= stringLastReplace(',',' and',$netbankingDownBankList);
	$netbankingDownBankListStr .= " are currently facing technical difficulties and are currently unavailable.<br/>";
	$netbankingDownBankListStr .= "Try using some other bank, or ";
	if($codErrorCode > 0){
		$netbankingDownBankListStr .= "try again later";
	} else {
		$netbankingDownBankListStr .= "buy with Cash on Delivery";
	}
	$smarty->assign("netbankingDownBankListStr", $netbankingDownBankListStr);
}

if(!empty($creditDebitDownBankList)) {
	$creditDebitDownBankListStr = stringLastReplace(',',' and ',$creditDebitDownBankList);
	$creditDebitDownBankListStr .= " are currently facing technical difficulties.<br/>";
	$creditDebitDownBankListStr .= "Please pay using a card from a different bank, or ";
	if($codErrorCode > 0) {
		$creditDebitDownBankListStr .= "try again later";
	} else {
		$creditDebitDownBankListStr .= "choose Cash on Delivery";
	}
	$smarty->assign("creditDebitDownBankListStr", $creditDebitDownBankListStr);
}


///////////////////////////////////////////////////////////////////////////////////////////////////

$GatewayUp = array();
$GatewayUp['ICICI']= (($icici_gateway_up&&$icici_gateway_available));
$GatewayUp['CITI']= (($citi_gateway_up&&$citi_gateway_available));


$EMIBanks = \FeatureGateKeyValuePairs::getFeatureGateValueForKey('Payments.EMIBanks');
$EMIBanks = json_decode($EMIBanks, true);
$EMIBanksAvailable = array();
$EMIenabled = false;
$minAmountforEMI = PHP_INT_MAX;

$EMIEligibleGatewayUp = false;
$EMIEligibleGatewayDownList = array();

foreach ($EMIBanks as $key => $value) {
	$bankArray = array();
	$bankData = \FeatureGateKeyValuePairs::getFeatureGateValueForKey('Payments.EMIBanks.'.$value);
	$bankData = json_decode($bankData, true);
	$bankArray['BankName'] = $bankData['BankName'];
	$bankArray['Up'] = $GatewayUp[$value];
	$bankArray['Enabled'] = false;
	$bankArray['Durations'] = array();
	
	foreach ($bankData[Durations] as $option => $duration) {
		$durationData = \FeatureGateKeyValuePairs::getFeatureGateValueForKey('Payments.EMIBanks.'.$value.'.'.$duration);
		$durationData = json_decode($durationData,true);
		$bankArray['Durations'][$durationData['Installments'].'MonthsEMI']['Charge'] = $durationData['Charge'];
		$bankArray['Durations'][$durationData['Installments'].'MonthsEMI']['Installments'] = $durationData['Installments'];
		$bankArray['Durations'][$durationData['Installments'].'MonthsEMI']['MinAmount'] = $durationData['MinAmount'];
		
		if ($minAmountforEMI > $durationData['MinAmount'] ) {$minAmountforEMI = $durationData['MinAmount'];}
				
		$bankArray['Durations'][$durationData['Installments'].'MonthsEMI']['TotEMIAmount'] = $totalAmount+$durationData['Charge'];
		$bankArray['Durations'][$durationData['Installments'].'MonthsEMI']['PerMonthEMI'] = round(($totalAmount+$durationData['Charge'])/$durationData['Installments']);
		if ($totalAmount >= $durationData['MinAmount']) {
			$bankArray['Durations'][$durationData['Installments'].'MonthsEMI']['Enabled'] = true;
			$bankArray['Enabled'] = true;
			$EMIenabled = true;
		}
		else {
			$bankArray['Durations'][$durationData['Installments'].'MonthsEMI']['Enabled'] = false;
		}
	}
	
	$bankBins = \FeatureGateKeyValuePairs::getStrArray('Payments.EMIBanks.'.$value.'.Bins');
	$bankArray['Bins'] = $bankBins;
	
	if ($bankArray['Enabled']) {
		if ($bankArray['Up']){
			$EMIEligibleGatewayUp = true;
		}
		else{
			$EMIEligibleGatewayDownList[] = $bankArray['BankName'];
		}
	}
	
	if ($bankArray['Enabled'] && $bankArray['Up']){
		$EMIBanksAvailable[$value]= $bankArray;
	}
};

$EMIGatewayDownListErrorMsg = "";

if (sizeof($EMIEligibleGatewayDownList) > 0){
	if (sizeof($EMIEligibleGatewayDownList) == 1){
		$EMIGatewayDownListErrorMsg .="Currently ".$EMIEligibleGatewayDownList[0]." is facing technical difficulties. Please "; 
		if($codErrorCode > 0){
			$EMIGatewayDownListErrorMsg .= "try again later.";
		} else {
			$EMIGatewayDownListErrorMsg .= "buy with Cash on Delivery.";
		}
	}
	else{
		$BanksString = "";
		foreach ($EMIEligibleGatewayDownList as $value) {
			$BanksString .= " ".$value.","; 	
		}
		$BanksString = rtrim($BanksString, ',');
		print_r($BanksString);
		$EMIGatewayDownListErrorMsg .="Currently".$BanksString." are facing technical difficulties. Please ";

		if($codErrorCode > 0){
			$EMIGatewayDownListErrorMsg .= "try again later.";
		} else {
			$EMIGatewayDownListErrorMsg .= "buy with Cash on Delivery.";
		}
	
	}
}
$smarty->assign('EMIBanksAvailable', $EMIBanksAvailable);
$smarty->assign('EMIenabled', $EMIenabled);
$smarty->assign('EMIEligibleGatewayUp', $EMIEligibleGatewayUp);
$smarty->assign('EMIEligibleGatewayDownList', $EMIEligibleGatewayDownList);
$smarty->assign('minAmountforEMI', $minAmountforEMI);
$smarty->assign('EMIGatewayDownListErrorMsg', $EMIGatewayDownListErrorMsg);



///////////////////////////////////////////////////////////////////////////////////////////////////

//The php to which payment form is to be submitted is set here
$smarty->assign('paymentphp','/mkorderinsert.php');

$smarty->assign("paymentServiceEnabled", $paymentServiceEnabled);

if($paymentServiceEnabled) {	
    //Hack for bypassing the pincode check for COD availability
    //This is only incase of a user who doesnt have a saved address
    if(empty($hasAddress) && $codErrorCode==2){
                $codErrorCode=0;
                $codErrorMessage ='';
    }

	$serviceRequestParams = "totalAmount=". urlencode($totalAmount);
	$serviceRequestParams .= "&addressid=" . urlencode(!empty($address['id'])?$address['id']:0);
    if ($skin == MobileConstants::MOBILE_SKIN)
        $serviceRequestParams .= "&clientCode=moneclick";
    else
	    $serviceRequestParams .= "&clientCode=oneclick";
	$serviceRequestParams .= "&cartContext=".CartContext::OneClickContext;
	if($oneClickCOD && !empty($address['id'])) {
		$serviceRequestParams .= "&oneClickCOD=1";
	}
	$serviceRequestParams .= "&addressname=" . urlencode($address['name']);
	$serviceRequestParams .= "&addressaddress=" . urlencode($address['address']);
	$serviceRequestParams .= "&addresscity=" . urlencode($address['city']);
	$serviceRequestParams .= "&addressstatename=" . urlencode($address['statename']);
	$serviceRequestParams .= "&addresspincode=" . urlencode($address['pincode']);
	$serviceRequestParams .= "&addresscountry=" . urlencode($address['country']);
	$serviceRequestParams .= "&coderrorcode=" . urlencode($codErrorCode);
	$serviceRequestParams .= "&login=" . urlencode($login);
	$profile = HostConfig::$httpHost;
	$serviceRequestParams .= "&profile=" . urlencode($profile);
	
	if(!empty($onlineErrorMessage)) {
		$serviceRequestParams .= "&onlineErrorMessage=". urlencode($onlineErrorMessage);
	}
	
	if(!empty($codErrorMessage)) {
		
		$serviceRequestParams .= "&codErrorMessage=".urlencode($codErrorMessage);
	}
	
 	$url = HostConfig::$paymentServiceInternalHost.'/payment?'. $serviceRequestParams;
 	
 //	echo "$url<br/>";
//    exit;
	$restRequest = new RestRequest($url, 'GET');

	$headers = array();
	array_push($headers, 'Accept: text/html');
	array_push($headers,'Content-Type: application/json');

	$restRequest ->setHttpHeaders($headers);
	$restRequest ->execute();
	$responseInfo = $restRequest ->getResponseInfo();
	$responseBody = $restRequest ->getResponseBody();
	//echo $responseBody; exit;
	//print_r($responseInfo);
	//exit;
	if(!$responseBody || empty($responseBody) || $responseInfo['http_code'] != 200) {
		$errorlog->error("Express Buy Payment options could not be loaded.");		
    }
	
    $smarty->assign("paymentPageUI", $responseBody);
    
}

$tracker->fireRequest();

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setContextualPageData(AnalyticsBase::PAYMENT_PAGE);
$analyticsObj->setPaymentEventVars();
$analyticsObj->setTemplateVars($smarty);

//AB Test for payment helpline
$smarty->assign("payHelpline", MABTest::getInstance()->getUserSegment('payHelpline'));
//AB Test for payment failure options
$smarty->assign("payFailOpt", MABTest::getInstance()->getUserSegment('payFailOpt'));
//AB Test for user closing overlay
$smarty->assign("payUserCloseOpt", MABTest::getInstance()->getUserSegment('payUserCloseOpt'));

require_once "$xcart_dir/cart_details.php";
$smarty->assign('expressCheckout',true);
$smarty->assign('hasAddress',$hasAddress);

foreach($productsInCart as $products){
		$pID = $products['productStyleId'];
		$pSKUID = $products['skuid'];
	}
$smarty->assign('pStyleID',$pID);
$smarty->assign('pSKUID',$pSKUID);
$smarty->display("express_checkout/express-payment.tpl");
