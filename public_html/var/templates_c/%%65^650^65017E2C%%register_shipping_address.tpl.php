<?php /* Smarty version 2.6.12, created on 2017-03-27 12:58:50
         compiled from main/register_shipping_address.tpl */ ?>
<?php if ($this->_tpl_vars['is_areas']['S'] == 'Y'): ?>

<?php if ($this->_tpl_vars['hide_header'] == ""): ?>
<div class="subhead"><p>shipping address</p></div>
<?php endif; ?>

<?php if ($this->_tpl_vars['action'] == 'cart'): ?>

<input type="hidden" name="action" value="cart" />
<input type="hidden" name="paymentid" value="<?php echo $this->_tpl_vars['paymentid']; ?>
" />

<?php endif; ?>





<?php if ($this->_tpl_vars['default_fields']['s_address']['avail'] == 'Y'): ?>



<div class="links_980"><div class="legend_980"><p>Address</p></div>
    <div class="field_980"><label>
     <textarea id="s_address" name="s_address" class="address" onKeyPress=check_length("s_address"); onKeyDown=check_length("s_address");><?php echo $this->_tpl_vars['userinfo']['s_address']; ?>
</textarea>
     <input type=hidden size=2 value=255 name="text_num" id="text_num">
      <!-- <input  type="text" id="s_address" name="s_address"  value="<?php echo $this->_tpl_vars['userinfo']['s_address']; ?>
" /> -->
      <?php if ($this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['s_address'] == "" && $this->_tpl_vars['default_fields']['s_address']['required'] == 'Y'):  endif; ?>
     </label> </div><div class="clearall"></div>
</div>

<?php endif; ?>


<!-- <div class="links_980"><div class="legend_980"><p>Address (Line2)</p></div>
    <div class="field_980"><label>
      
       <input  type="text" id="s_address_2" name="s_address_2"  value="<?php echo $this->_tpl_vars['userinfo']['s_address_2']; ?>
" />
	<?php if ($this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['s_address_2'] == "" && $this->_tpl_vars['default_fields']['s_address_2']['required'] == 'Y'):  endif; ?>
    </label> </div><div class="clearall"></div>
</div>-->


<?php if ($this->_tpl_vars['default_fields']['s_country']['avail'] == 'Y'): ?>

<div class="links_980"><div class="legend_980"><p>Country</p> </div>
	<div class="field_980">

<select name="s_country" id="s_country" size="1" class="countryselect" onchange="check_zip_code()">
<?php unset($this->_sections['country_idx']);
$this->_sections['country_idx']['name'] = 'country_idx';
$this->_sections['country_idx']['loop'] = is_array($_loop=$this->_tpl_vars['countries']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['country_idx']['show'] = true;
$this->_sections['country_idx']['max'] = $this->_sections['country_idx']['loop'];
$this->_sections['country_idx']['step'] = 1;
$this->_sections['country_idx']['start'] = $this->_sections['country_idx']['step'] > 0 ? 0 : $this->_sections['country_idx']['loop']-1;
if ($this->_sections['country_idx']['show']) {
    $this->_sections['country_idx']['total'] = $this->_sections['country_idx']['loop'];
    if ($this->_sections['country_idx']['total'] == 0)
        $this->_sections['country_idx']['show'] = false;
} else
    $this->_sections['country_idx']['total'] = 0;
if ($this->_sections['country_idx']['show']):

            for ($this->_sections['country_idx']['index'] = $this->_sections['country_idx']['start'], $this->_sections['country_idx']['iteration'] = 1;
                 $this->_sections['country_idx']['iteration'] <= $this->_sections['country_idx']['total'];
                 $this->_sections['country_idx']['index'] += $this->_sections['country_idx']['step'], $this->_sections['country_idx']['iteration']++):
$this->_sections['country_idx']['rownum'] = $this->_sections['country_idx']['iteration'];
$this->_sections['country_idx']['index_prev'] = $this->_sections['country_idx']['index'] - $this->_sections['country_idx']['step'];
$this->_sections['country_idx']['index_next'] = $this->_sections['country_idx']['index'] + $this->_sections['country_idx']['step'];
$this->_sections['country_idx']['first']      = ($this->_sections['country_idx']['iteration'] == 1);
$this->_sections['country_idx']['last']       = ($this->_sections['country_idx']['iteration'] == $this->_sections['country_idx']['total']);
?>
<option value="<?php echo $this->_tpl_vars['countries'][$this->_sections['country_idx']['index']]['country_code']; ?>
"<?php if ($this->_tpl_vars['userinfo']['s_country'] == $this->_tpl_vars['countries'][$this->_sections['country_idx']['index']]['country_code']): ?> selected="selected"<?php elseif ($this->_tpl_vars['countries'][$this->_sections['country_idx']['index']]['country_code'] == $this->_tpl_vars['config']['General']['default_country'] && $this->_tpl_vars['userinfo']['s_country'] == ""): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['countries'][$this->_sections['country_idx']['index']]['country']; ?>
</option>
<?php endfor; endif; ?>
</select>
<?php if ($this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['s_country'] == "" && $this->_tpl_vars['default_fields']['s_country']['required'] == 'Y'):  endif; ?>

	</div><div class="clearall"></div>
</div>

<?php endif; ?>

<?php if ($this->_tpl_vars['default_fields']['s_state']['avail'] == 'Y'): ?>

<div class="links_980"><div class="legend_980"><p>State</p> </div>
    <div class="field_980"> 
    <label>

	    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/states.tpl", 'smarty_include_vars' => array('states' => $this->_tpl_vars['states'],'name' => 's_state','default' => $this->_tpl_vars['userinfo']['s_state'],'default_country' => $this->_tpl_vars['userinfo']['s_country'],'country_name' => 's_country')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	    <?php if ($this->_tpl_vars['error'] == 's_statecode' || ( $this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['s_state'] == "" && $this->_tpl_vars['default_fields']['s_state']['required'] == 'Y' )):  endif; ?>

   </label> 
   </div>
   <div class="clearall"></div>
  </div>


<?php endif; ?>


<?php if ($this->_tpl_vars['default_fields']['s_city']['avail'] == 'Y'): ?>

<div class="links_980"><div class="legend_980"><p>City</p> </div>
    <div class="field_980"><label>
      
<input  type="text" id="s_city" name="s_city"  value="<?php echo $this->_tpl_vars['userinfo']['s_city']; ?>
" />
<?php if ($this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['s_city'] == "" && $this->_tpl_vars['default_fields']['s_city']['required'] == 'Y'):  endif; ?>

    </label> </div><div class="clearall"></div>
</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['default_fields']['s_state']['avail'] == 'Y' && $this->_tpl_vars['default_fields']['s_country']['avail'] == 'Y' && $this->_tpl_vars['js_enabled'] == 'Y' && $this->_tpl_vars['config']['General']['use_js_states'] == 'Y'): ?>
<div class="field_980" style="display: none;">
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/register_states.tpl", 'smarty_include_vars' => array('state_name' => 's_state','country_name' => 's_country','county_name' => 's_county','state_value' => $this->_tpl_vars['userinfo']['s_state'],'county_value' => $this->_tpl_vars['userinfo']['s_county'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	</div>
<?php endif; ?>

<?php if ($this->_tpl_vars['default_fields']['mobile']['avail'] == 'Y'): ?>

<div class="links_980"><div class="legend_980"><p>Mobile</p> </div>
    <div class="field_980"> <label> 
      
      <input type="text" id="mobile" name="mobile"  value="<?php echo $this->_tpl_vars['userinfo']['mobile']; ?>
" maxlength="11" />
    </label> </div><div class="clearall"></div>
  </div>

<?php endif; ?>

<?php if ($this->_tpl_vars['default_fields']['phone']['avail'] == 'Y'): ?>

<div class="links_980"><div class="legend_980"><p>Phone</p> </div>
    <div class="field_980"> <label> 
      
      <input type="text" id="phone" name="phone"  value="<?php echo $this->_tpl_vars['userinfo']['phone']; ?>
" />
    </label> </div><div class="clearall"></div>
  </div>

<?php endif; ?>


<?php if ($this->_tpl_vars['default_fields']['s_zipcode']['avail'] == 'Y'): ?>

<div class="links_980"><div class="legend_980"><p>Zip/Postal Code</p></div>

            <div class="field_980"><label>
              
	      <input  type="text" id="s_zipcode" name="s_zipcode"  value="<?php echo $this->_tpl_vars['userinfo']['s_zipcode']; ?>
" onchange="check_zip_code()"  size="7" maxlength="6" />
	      </label>
	      <?php if ($this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['s_zipcode'] == "" && $this->_tpl_vars['default_fields']['s_zipcode']['required'] == 'Y'):  endif; ?>
              &nbsp;&nbsp;
	      <input class="checkbox" type="checkbox" id="ship2diff" name="ship2diff" value="Y" onclick="javascript: ship2diffOpen();"<?php if ($this->_tpl_vars['ship2diff']): ?> checked="checked"<?php endif; ?> />
             
              &nbsp;Billing <!--Ship--> to Different Address </div><div class="clearall"></div>
          </div>

<?php endif; ?>







<div class="foot"></div>
<!--</div> -->
 <br>


<?php endif; ?>
