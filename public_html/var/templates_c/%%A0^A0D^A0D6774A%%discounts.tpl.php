<?php /* Smarty version 2.6.12, created on 2017-03-28 15:11:47
         compiled from provider/main/discounts.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'provider/main/discounts.tpl', 40, false),array('modifier', 'formatprice', 'provider/main/discounts.tpl', 42, false),array('modifier', 'escape', 'provider/main/discounts.tpl', 57, false),)), $this); ?>
<?php func_load_lang($this, "provider/main/discounts.tpl","lbl_discounts,txt_discounts_note,lbl_check_all,lbl_uncheck_all,lbl_order_subtotal,lbl_discount,lbl_discount_type,lbl_membership,lbl_percent,lbl_absolute,lbl_delete_selected,lbl_update,lbl_no_discounts_defined,lbl_add_new_discount,lbl_percent,lbl_absolute,lbl_add_update,lbl_edit_purchase_discounts"); ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "page_title.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_discounts'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php echo $this->_tpl_vars['lng']['txt_discounts_note']; ?>


<?php ob_start(); ?>

<?php if ($this->_tpl_vars['discounts']): ?>

<script type="text/javascript" language="JavaScript 1.2">
<!--
checkboxes_form = 'discountsform';
checkboxes = new Array(<?php $_from = $this->_tpl_vars['discounts']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
 if ($this->_tpl_vars['k'] > 0): ?>,<?php endif; ?>'posted_data[<?php echo $this->_tpl_vars['v']['discountid']; ?>
][to_delete]'<?php endforeach; endif; unset($_from); ?>);
 
--> 
</script>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "change_all_checkboxes.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div style="line-height:170%"><a href="javascript:change_all(true);"><?php echo $this->_tpl_vars['lng']['lbl_check_all']; ?>
</a> / <a href="javascript:change_all(false);"><?php echo $this->_tpl_vars['lng']['lbl_uncheck_all']; ?>
</a></div>

<?php endif; ?>

<form action="discounts.php" method="post" name="discountsform">
<input type="hidden" name="mode" value="update" />

<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="10">&nbsp;</td>
	<td width="25%"><?php echo $this->_tpl_vars['lng']['lbl_order_subtotal']; ?>
</td>
	<td width="25%"><?php echo $this->_tpl_vars['lng']['lbl_discount']; ?>
</td>
	<td width="25%"><?php echo $this->_tpl_vars['lng']['lbl_discount_type']; ?>
</td>
	<td width="25%"><?php echo $this->_tpl_vars['lng']['lbl_membership']; ?>
</td>
</tr>

<?php if ($this->_tpl_vars['discounts']): ?>

<?php $_from = $this->_tpl_vars['discounts']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['discount']):
?>

<tr<?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this);?>
>
	<td><input type="checkbox" name="posted_data[<?php echo $this->_tpl_vars['discount']['discountid']; ?>
][to_delete]" /></td>
	<td><input type="text" name="posted_data[<?php echo $this->_tpl_vars['discount']['discountid']; ?>
][minprice]" size="12" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['discount']['minprice'])) ? $this->_run_mod_handler('formatprice', true, $_tmp) : smarty_modifier_formatprice($_tmp)); ?>
" /></td>
	<td><input type="text" name="posted_data[<?php echo $this->_tpl_vars['discount']['discountid']; ?>
][discount]" size="12" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['discount']['discount'])) ? $this->_run_mod_handler('formatprice', true, $_tmp) : smarty_modifier_formatprice($_tmp)); ?>
" /></td>
	<td>
	<select name="posted_data[<?php echo $this->_tpl_vars['discount']['discountid']; ?>
][discount_type]">
		<option value="percent"<?php if ($this->_tpl_vars['discount']['discount_type'] == 'percent'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_percent']; ?>
, %</option>
		<option value="absolute"<?php if ($this->_tpl_vars['discount']['discount_type'] == 'absolute'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_absolute']; ?>
, <?php echo $this->_tpl_vars['config']['General']['currency_symbol']; ?>
</option>
	</select>
	</td>
	<td><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/membership_selector.tpl", 'smarty_include_vars' => array('field' => "posted_data[".($this->_tpl_vars['discount']['discountid'])."][membershipids][]",'data' => $this->_tpl_vars['discount'],'is_short' => 'Y')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>

<?php endforeach; endif; unset($_from); ?>

<tr>
	<td colspan="5" class="SubmitBox">
	<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_delete_selected'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: submitForm(this, 'delete');" />
	<input type="submit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_update'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" />
	</td>
</tr>

<?php else: ?>

<tr>
	<td colspan="5" align="center"><?php echo $this->_tpl_vars['lng']['lbl_no_discounts_defined']; ?>
</td>
</tr>

<?php endif; ?>

<tr>
	<td colspan="5">&nbsp;</td>
</tr>

<tr>
	<td colspan="5"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_add_new_discount'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>

<tr>
	<td>&nbsp;</td>
	<td><input type="text" name="minprice_new" size="12" value="<?php echo $this->_tpl_vars['zero']; ?>
" /></td>
	<td><input type="text" name="discount_new" size="12" value="<?php echo $this->_tpl_vars['zero']; ?>
" /></td>
	<td>
	<select name="discount_type_new">
		<option value="percent"><?php echo $this->_tpl_vars['lng']['lbl_percent']; ?>
, %</option>
		<option value="absolute"><?php echo $this->_tpl_vars['lng']['lbl_absolute']; ?>
, <?php echo $this->_tpl_vars['config']['General']['currency_symbol']; ?>
</option>
	</select>
	</td>
	<td><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/membership_selector.tpl", 'smarty_include_vars' => array('field' => "discount_membershipids_new[]",'data' => "",'is_short' => 'Y')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>

<tr>
	<td colspan="5" class="SubmitBox"><input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_add_update'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: submitForm(this, 'add');" /></td>
</tr>

</table>
</form>

<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_edit_purchase_discounts'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>