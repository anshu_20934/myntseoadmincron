<?php /* Smarty version 2.6.12, created on 2017-03-24 21:19:00
         compiled from main/admin_register_assign_wms_roles.tpl */ ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['http_location']; ?>
/skin1/js_script/jquery-1.4.2.min.js"></script>
<?php echo '
<script type="text/javascript">
	function array2json(arr) {
	    var parts = [];
	    var is_list = (Object.prototype.toString.apply(arr) === \'[object Array]\');
	
	    for(var key in arr) {
	    	var value = arr[key];
	        if(typeof value == "object") { //Custom handling for arrays
	            if(is_list) parts.push(array2json(value)); /* :RECURSION: */
	            else parts[key] = array2json(value); /* :RECURSION: */
	        } else {
	            var str = "";
	            if(!is_list) str = \'"\' + key + \'":\';
	
	            //Custom handling for multiple data types
	            if(typeof value == "number") str += value; //Numbers
	            else if(value === false) str += \'false\'; //The booleans
	            else if(value === true) str += \'true\';
	            else str += \'"\' + value + \'"\'; //All other things
	            parts.push(str);
	        }
	    }
	    var json = parts.join(",");
	    
	    if(is_list) return \'[\' + json + \']\';//Return numerical JSON
	    return \'{\' + json + \'}\';//Return associative JSON
	}

	function displayVals() {
      var multipleValues = $("#wms_type").val() || [];
      $("#new_permissions").text("Loading...");
      $.post(\'/user/permission/\', {data: array2json(multipleValues)},
              function(data){
				$("#new_permissions").text(data);
              }, \'json\');
    }

	$(document).ready(function(){
    	$("#wms_type").change(displayVals);
	});
</script>
'; ?>


<tr>
<td colspan="3" class="RegSectionTitle">Assign WMS Roles<hr size="1" noshade="noshade" /></td>
</tr>

<tr>
	<td align="right" >Assign Roles </td>
	<td align="left">
		<table cellspacing="1" cellpadding="2" width="100%" border="1">
			<tbody>
				<tr><td colspan="2" align="center">(Can select multiple roles by pressing control)</td></tr>
				<tr>
					<th>Current Persmissions</th>
					<th>New Permissions</th>
				</tr>
				<tr>
					<td><?php echo $this->_tpl_vars['current_permissions']; ?>
</td>
					<td><span id="new_permissions"><?php echo $this->_tpl_vars['current_permissions']; ?>
</span></td>
				</tr>
			</tbody>
		</table>
	</td>
	<td nowrap="nowrap">
		<select name="wms_type[]" id="wms_type" size="4" multiple="multiple" style="width:300px;">
		<?php $_from = $this->_tpl_vars['all_roles']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
			<?php if ($this->_tpl_vars['wms_user_role_ids'][$this->_tpl_vars['key']] == 1): ?>
				<option name="<?php echo $this->_tpl_vars['key']; ?>
" value="<?php echo $this->_tpl_vars['key']; ?>
" selected><?php echo $this->_tpl_vars['item']; ?>
</option>
			<?php else: ?>
				<option name="<?php echo $this->_tpl_vars['key']; ?>
" value="<?php echo $this->_tpl_vars['key']; ?>
"><?php echo $this->_tpl_vars['item']; ?>
</option>
			<?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
		</select>
	</td>
</tr>