<?php /* Smarty version 2.6.12, created on 2017-11-12 15:06:28
         compiled from provider/main/inv_update.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'provider/main/inv_update.tpl', 32, false),)), $this); ?>
<?php func_load_lang($this, "provider/main/inv_update.tpl","lbl_update_inventory,lbl_update,lbl_pricing,lbl_in_stock,lbl_csv_delimiter,lbl_csv_file,lbl_warning,txt_max_file_size_that_can_be_uploaded,lbl_update,lbl_update_inventory"); ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "page_title.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_update_inventory'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php ob_start(); ?>
<form method="post" action="inv_update.php" enctype="multipart/form-data">

<table cellpadding="0" cellspacing="4" width="100%">

<tr>
	<td><?php echo $this->_tpl_vars['lng']['lbl_update']; ?>
</td>
	<td>
	<select name="what">
		 <option value="p" selected="selected"><?php echo $this->_tpl_vars['lng']['lbl_pricing']; ?>
</option>
		 <option value="q" selected="selected"><?php echo $this->_tpl_vars['lng']['lbl_in_stock']; ?>
</option>
	</select>
	</td>
</tr>
<tr>
	<td><?php echo $this->_tpl_vars['lng']['lbl_csv_delimiter']; ?>
</td>
	<td><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "provider/main/ie_delimiter.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>
<tr>
	<td><?php echo $this->_tpl_vars['lng']['lbl_csv_file']; ?>
</td>
	<td><input type="file" name="userfile" />
<?php if ($this->_tpl_vars['upload_max_filesize']): ?>
<br /><font class="Star"><?php echo $this->_tpl_vars['lng']['lbl_warning']; ?>
!</font> <?php echo $this->_tpl_vars['lng']['txt_max_file_size_that_can_be_uploaded']; ?>
: <?php echo $this->_tpl_vars['upload_max_filesize']; ?>
b.
<?php endif; ?> 
	</td>
</tr>

<tr>
	<td colspan="2" class="SubmitBox"><input type="submit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_update'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" /></td>
</tr>

</table>
</form>
<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('content' => $this->_smarty_vars['capture']['dialog'],'title' => $this->_tpl_vars['lng']['lbl_update_inventory'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>