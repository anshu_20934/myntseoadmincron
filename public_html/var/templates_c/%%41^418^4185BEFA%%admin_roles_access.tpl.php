<?php /* Smarty version 2.6.12, created on 2017-03-30 15:19:55
         compiled from admin/main/admin_roles_access.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'admin/main/admin_roles_access.tpl', 93, false),)), $this); ?>
<?php func_load_lang($this, "admin/main/admin_roles_access.tpl","lbl_upd_selected"); ?>
<?php echo '
<script type="text/javascript">
function delete_roleaccesspair(id){
    var r=confirm("Are you sure you want to delete this role access pair");
    if(r == true){
        document.updateroleaccesspairs.mode.value = \'delete\';
        document.updateroleaccesspairs.roleaccess_id.value = id;
        document.updateroleaccesspairs.submit();
    }
}

function add_roleaccesspair(){
	if($(\'.access_new\').val().trim() ==\'\'){
        alert(\'Page address cannot be empty\');
        return;
    }
    document.updateroleaccesspairs.mode.value = \'add\';
    document.updateroleaccesspairs.submit();
}

function update_roleaccesspair(){
	var updatetype = true;
	$(\'textarea\').each(function() {
		if(!($(this).hasClass("access_new") || $(this).hasClass("extra_logins_allowed_tarea"))){
			if($(this).val().trim() == \'\'){
				updatetype = false;
				return;
			}
		}
		
	});
	if(updatetype){		
		document.updateroleaccesspairs.mode.value = \'update\';
		document.updateroleaccesspairs.submit();
	}
	else {
		alert(\'Page address cannot be empty\');
		return
	}
}
</script>
'; ?>

<?php ob_start();  if ($this->_tpl_vars['roletypes']): ?>
<form action="admin_roles_access.php" method="post" name="updateroleaccesspairs">
<input type="hidden" name="mode" />
<input type="hidden" name="roleaccess_id" />
<table cellpadding="3" cellspacing="1" width="100%">
<thead>
<tr class="TableHead">
	<td>Update</td>
	<td>Page Address</td>
	<?php unset($this->_sections['role_num']);
$this->_sections['role_num']['name'] = 'role_num';
$this->_sections['role_num']['loop'] = is_array($_loop=$this->_tpl_vars['roletypes']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['role_num']['show'] = true;
$this->_sections['role_num']['max'] = $this->_sections['role_num']['loop'];
$this->_sections['role_num']['step'] = 1;
$this->_sections['role_num']['start'] = $this->_sections['role_num']['step'] > 0 ? 0 : $this->_sections['role_num']['loop']-1;
if ($this->_sections['role_num']['show']) {
    $this->_sections['role_num']['total'] = $this->_sections['role_num']['loop'];
    if ($this->_sections['role_num']['total'] == 0)
        $this->_sections['role_num']['show'] = false;
} else
    $this->_sections['role_num']['total'] = 0;
if ($this->_sections['role_num']['show']):

            for ($this->_sections['role_num']['index'] = $this->_sections['role_num']['start'], $this->_sections['role_num']['iteration'] = 1;
                 $this->_sections['role_num']['iteration'] <= $this->_sections['role_num']['total'];
                 $this->_sections['role_num']['index'] += $this->_sections['role_num']['step'], $this->_sections['role_num']['iteration']++):
$this->_sections['role_num']['rownum'] = $this->_sections['role_num']['iteration'];
$this->_sections['role_num']['index_prev'] = $this->_sections['role_num']['index'] - $this->_sections['role_num']['step'];
$this->_sections['role_num']['index_next'] = $this->_sections['role_num']['index'] + $this->_sections['role_num']['step'];
$this->_sections['role_num']['first']      = ($this->_sections['role_num']['iteration'] == 1);
$this->_sections['role_num']['last']       = ($this->_sections['role_num']['iteration'] == $this->_sections['role_num']['total']);
?>
	<td><?php echo $this->_tpl_vars['roletypes'][$this->_sections['role_num']['index']]['name']; ?>
</td>
	<?php endfor; endif; ?>
	<td>Extra Logins Allowed</td>
</tr>
</thead>
<tbody>
<?php if ($this->_tpl_vars['roleaccesspairs']):  unset($this->_sections['key_num']);
$this->_sections['key_num']['name'] = 'key_num';
$this->_sections['key_num']['loop'] = is_array($_loop=$this->_tpl_vars['roleaccesspairs']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['key_num']['show'] = true;
$this->_sections['key_num']['max'] = $this->_sections['key_num']['loop'];
$this->_sections['key_num']['step'] = 1;
$this->_sections['key_num']['start'] = $this->_sections['key_num']['step'] > 0 ? 0 : $this->_sections['key_num']['loop']-1;
if ($this->_sections['key_num']['show']) {
    $this->_sections['key_num']['total'] = $this->_sections['key_num']['loop'];
    if ($this->_sections['key_num']['total'] == 0)
        $this->_sections['key_num']['show'] = false;
} else
    $this->_sections['key_num']['total'] = 0;
if ($this->_sections['key_num']['show']):

            for ($this->_sections['key_num']['index'] = $this->_sections['key_num']['start'], $this->_sections['key_num']['iteration'] = 1;
                 $this->_sections['key_num']['iteration'] <= $this->_sections['key_num']['total'];
                 $this->_sections['key_num']['index'] += $this->_sections['key_num']['step'], $this->_sections['key_num']['iteration']++):
$this->_sections['key_num']['rownum'] = $this->_sections['key_num']['iteration'];
$this->_sections['key_num']['index_prev'] = $this->_sections['key_num']['index'] - $this->_sections['key_num']['step'];
$this->_sections['key_num']['index_next'] = $this->_sections['key_num']['index'] + $this->_sections['key_num']['step'];
$this->_sections['key_num']['first']      = ($this->_sections['key_num']['iteration'] == 1);
$this->_sections['key_num']['last']       = ($this->_sections['key_num']['iteration'] == $this->_sections['key_num']['total']);
?>
<tr>
	<td style="text-align: center; vertical-align: middle;"> <input type="checkbox" name="update_id[]" value="<?php echo $this->_tpl_vars['roleaccesspairs'][$this->_sections['key_num']['index']]['id']; ?>
"></td>
	<td align="center" style="left-padding:30px"><textarea name="update_access_field[<?php echo $this->_tpl_vars['roleaccesspairs'][$this->_sections['key_num']['index']]['id']; ?>
]"><?php echo $this->_tpl_vars['roleaccesspairs'][$this->_sections['key_num']['index']]['pagename']; ?>
</textarea></td>
		<?php unset($this->_sections['role_num']);
$this->_sections['role_num']['name'] = 'role_num';
$this->_sections['role_num']['loop'] = is_array($_loop=$this->_tpl_vars['roletypes']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['role_num']['show'] = true;
$this->_sections['role_num']['max'] = $this->_sections['role_num']['loop'];
$this->_sections['role_num']['step'] = 1;
$this->_sections['role_num']['start'] = $this->_sections['role_num']['step'] > 0 ? 0 : $this->_sections['role_num']['loop']-1;
if ($this->_sections['role_num']['show']) {
    $this->_sections['role_num']['total'] = $this->_sections['role_num']['loop'];
    if ($this->_sections['role_num']['total'] == 0)
        $this->_sections['role_num']['show'] = false;
} else
    $this->_sections['role_num']['total'] = 0;
if ($this->_sections['role_num']['show']):

            for ($this->_sections['role_num']['index'] = $this->_sections['role_num']['start'], $this->_sections['role_num']['iteration'] = 1;
                 $this->_sections['role_num']['iteration'] <= $this->_sections['role_num']['total'];
                 $this->_sections['role_num']['index'] += $this->_sections['role_num']['step'], $this->_sections['role_num']['iteration']++):
$this->_sections['role_num']['rownum'] = $this->_sections['role_num']['iteration'];
$this->_sections['role_num']['index_prev'] = $this->_sections['role_num']['index'] - $this->_sections['role_num']['step'];
$this->_sections['role_num']['index_next'] = $this->_sections['role_num']['index'] + $this->_sections['role_num']['step'];
$this->_sections['role_num']['first']      = ($this->_sections['role_num']['iteration'] == 1);
$this->_sections['role_num']['last']       = ($this->_sections['role_num']['iteration'] == $this->_sections['role_num']['total']);
?>
	<?php if ($this->_tpl_vars['pageroleaccess'][$this->_sections['key_num']['index']][$this->_sections['role_num']['index']] == 1): ?>
	
	<td style="text-align: center; vertical-align: middle;"> <input type="checkbox" name="roletypes[<?php echo $this->_tpl_vars['roleaccesspairs'][$this->_sections['key_num']['index']]['id']; ?>
][]" value="<?php echo $this->_tpl_vars['roletypes'][$this->_sections['role_num']['index']]['type']; ?>
" checked="yes"></td>
	
	<?php else: ?>
	<td style="text-align: center; vertical-align: middle;"> <input type="checkbox" name="roletypes[<?php echo $this->_tpl_vars['roleaccesspairs'][$this->_sections['key_num']['index']]['id']; ?>
][]" value="<?php echo $this->_tpl_vars['roletypes'][$this->_sections['role_num']['index']]['type']; ?>
" ></td>
	<?php endif; ?>
	<?php endfor; endif; ?>
	<td align="center" style="left-padding:30px"><textarea name="update_extra_logins_allowed[<?php echo $this->_tpl_vars['roleaccesspairs'][$this->_sections['key_num']['index']]['id']; ?>
]" class="extra_logins_allowed_tarea"><?php echo $this->_tpl_vars['roleaccesspairs'][$this->_sections['key_num']['index']]['extra_logins_allowed']; ?>
</textarea></td>
	<td align="center"> <input type="button" value="Delete" onclick="javascript: delete_roleaccesspair('<?php echo $this->_tpl_vars['roleaccesspairs'][$this->_sections['key_num']['index']]['id']; ?>
')" /> </td>	
</tr>
<?php endfor; endif;  else: ?>
<tr>
	<td colspan="2" align="center">No Page access pairs added as yet</td>
</tr>
<?php endif; ?>
<tr>
	<td></td>
	<td></td>
	<?php unset($this->_sections['role_num']);
$this->_sections['role_num']['name'] = 'role_num';
$this->_sections['role_num']['loop'] = is_array($_loop=$this->_tpl_vars['roletypes']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['role_num']['show'] = true;
$this->_sections['role_num']['max'] = $this->_sections['role_num']['loop'];
$this->_sections['role_num']['step'] = 1;
$this->_sections['role_num']['start'] = $this->_sections['role_num']['step'] > 0 ? 0 : $this->_sections['role_num']['loop']-1;
if ($this->_sections['role_num']['show']) {
    $this->_sections['role_num']['total'] = $this->_sections['role_num']['loop'];
    if ($this->_sections['role_num']['total'] == 0)
        $this->_sections['role_num']['show'] = false;
} else
    $this->_sections['role_num']['total'] = 0;
if ($this->_sections['role_num']['show']):

            for ($this->_sections['role_num']['index'] = $this->_sections['role_num']['start'], $this->_sections['role_num']['iteration'] = 1;
                 $this->_sections['role_num']['iteration'] <= $this->_sections['role_num']['total'];
                 $this->_sections['role_num']['index'] += $this->_sections['role_num']['step'], $this->_sections['role_num']['iteration']++):
$this->_sections['role_num']['rownum'] = $this->_sections['role_num']['iteration'];
$this->_sections['role_num']['index_prev'] = $this->_sections['role_num']['index'] - $this->_sections['role_num']['step'];
$this->_sections['role_num']['index_next'] = $this->_sections['role_num']['index'] + $this->_sections['role_num']['step'];
$this->_sections['role_num']['first']      = ($this->_sections['role_num']['iteration'] == 1);
$this->_sections['role_num']['last']       = ($this->_sections['role_num']['iteration'] == $this->_sections['role_num']['total']);
?>
	<td></td>
	<?php endfor; endif; ?>
	<td class="SubmitBox" align="right">
	   <input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_upd_selected'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: update_roleaccesspair()" />
	</td>
	<td></td>
</tr>
</tbody>
</table>


<h2>Add new page and define its role access: </h2>
<table>
<tr class="TableHead">
	<td>Page Address</td>
	<?php unset($this->_sections['role_num']);
$this->_sections['role_num']['name'] = 'role_num';
$this->_sections['role_num']['loop'] = is_array($_loop=$this->_tpl_vars['roletypes']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['role_num']['show'] = true;
$this->_sections['role_num']['max'] = $this->_sections['role_num']['loop'];
$this->_sections['role_num']['step'] = 1;
$this->_sections['role_num']['start'] = $this->_sections['role_num']['step'] > 0 ? 0 : $this->_sections['role_num']['loop']-1;
if ($this->_sections['role_num']['show']) {
    $this->_sections['role_num']['total'] = $this->_sections['role_num']['loop'];
    if ($this->_sections['role_num']['total'] == 0)
        $this->_sections['role_num']['show'] = false;
} else
    $this->_sections['role_num']['total'] = 0;
if ($this->_sections['role_num']['show']):

            for ($this->_sections['role_num']['index'] = $this->_sections['role_num']['start'], $this->_sections['role_num']['iteration'] = 1;
                 $this->_sections['role_num']['iteration'] <= $this->_sections['role_num']['total'];
                 $this->_sections['role_num']['index'] += $this->_sections['role_num']['step'], $this->_sections['role_num']['iteration']++):
$this->_sections['role_num']['rownum'] = $this->_sections['role_num']['iteration'];
$this->_sections['role_num']['index_prev'] = $this->_sections['role_num']['index'] - $this->_sections['role_num']['step'];
$this->_sections['role_num']['index_next'] = $this->_sections['role_num']['index'] + $this->_sections['role_num']['step'];
$this->_sections['role_num']['first']      = ($this->_sections['role_num']['iteration'] == 1);
$this->_sections['role_num']['last']       = ($this->_sections['role_num']['iteration'] == $this->_sections['role_num']['total']);
?>
	<td><?php echo $this->_tpl_vars['roletypes'][$this->_sections['role_num']['index']]['name']; ?>
</td>
	<?php endfor; endif; ?>
	<td>Extra Logins Allowed</td> 
</tr>

<tr>
	<td align="center" style="left-padding:30px"><textarea name="access_field_new" class="access_new"></textarea></td>
	<!-- td align="center" style="left-padding:30px"><input name="role_field_new" type="text" value="" /></td> -->
		<?php unset($this->_sections['role_num']);
$this->_sections['role_num']['name'] = 'role_num';
$this->_sections['role_num']['loop'] = is_array($_loop=$this->_tpl_vars['roletypes']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['role_num']['show'] = true;
$this->_sections['role_num']['max'] = $this->_sections['role_num']['loop'];
$this->_sections['role_num']['step'] = 1;
$this->_sections['role_num']['start'] = $this->_sections['role_num']['step'] > 0 ? 0 : $this->_sections['role_num']['loop']-1;
if ($this->_sections['role_num']['show']) {
    $this->_sections['role_num']['total'] = $this->_sections['role_num']['loop'];
    if ($this->_sections['role_num']['total'] == 0)
        $this->_sections['role_num']['show'] = false;
} else
    $this->_sections['role_num']['total'] = 0;
if ($this->_sections['role_num']['show']):

            for ($this->_sections['role_num']['index'] = $this->_sections['role_num']['start'], $this->_sections['role_num']['iteration'] = 1;
                 $this->_sections['role_num']['iteration'] <= $this->_sections['role_num']['total'];
                 $this->_sections['role_num']['index'] += $this->_sections['role_num']['step'], $this->_sections['role_num']['iteration']++):
$this->_sections['role_num']['rownum'] = $this->_sections['role_num']['iteration'];
$this->_sections['role_num']['index_prev'] = $this->_sections['role_num']['index'] - $this->_sections['role_num']['step'];
$this->_sections['role_num']['index_next'] = $this->_sections['role_num']['index'] + $this->_sections['role_num']['step'];
$this->_sections['role_num']['first']      = ($this->_sections['role_num']['iteration'] == 1);
$this->_sections['role_num']['last']       = ($this->_sections['role_num']['iteration'] == $this->_sections['role_num']['total']);
?>
	<td style="text-align: center; vertical-align: middle;"> <input type="checkbox" name="access_roletype_new[]" value="<?php echo $this->_tpl_vars['roletypes'][$this->_sections['role_num']['index']]['type']; ?>
" ></td>
	<?php endfor; endif; ?>
	<td align="center" style="left-padding:30px"><textarea name="extra_logins_allowed_new" class="extra_logins_allowed_tarea"></textarea></td>
	<td align="center">
		<input type="button" value="Add" onclick="javascript: add_roleaccesspair()" />
	</td>
</tr>
</table>
</form>
<?php else: ?>
There are no role types.
<?php endif;  $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean();  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => "Page access-Role type Pairs",'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>