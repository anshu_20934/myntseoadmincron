<?php /* Smarty version 2.6.12, created on 2017-05-26 17:53:22
         compiled from admin/main/filtergroups.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'admin/main/filtergroups.tpl', 48, false),array('modifier', 'escape', 'admin/main/filtergroups.tpl', 79, false),)), $this); ?>
<?php func_load_lang($this, "admin/main/filtergroups.tpl","txt_featured_products,lbl_yes,lbl_no,lbl_add_product,lbl_delete_selected,lbl_update,lbl_yes,lbl_no,lbl_add"); ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "main/popup_product.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
  echo '
  <script language="Javascript">
      function confirmDelete()
      {
          var flag = confirm(\'Are you sure you want to delete?\')
          if(!flag)
          {
             return false;
          }
          else
          {
          	 document.groupfrm.mode.value = \'delete\';
          	 document.groupfrm.submit();
          	 return true;
          }
      }
  </script>
'; ?>

<a name="featured" />

<?php echo $this->_tpl_vars['lng']['txt_featured_products']; ?>


<br /><br />

<?php ob_start(); ?>

<form action="<?php echo $this->_tpl_vars['form_action']; ?>
" method="post" name="groupfrm" enctype="multipart/form-data">
<input type="hidden" name="mode"  />

<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="10%">&nbsp;</td>
	<td width="20%">Name</td>
	<td width="20%">Label</td>
	<td width="20%" align="center">Description</td>
	<td width="20%">Config</td>
	<td width="10%" align="center">Display Order</td>
	<td width="10%" align="center">Active</td>
</tr>

<?php if ($this->_tpl_vars['groups']): ?>

<?php unset($this->_sections['grid']);
$this->_sections['grid']['name'] = 'grid';
$this->_sections['grid']['loop'] = is_array($_loop=$this->_tpl_vars['groups']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['grid']['show'] = true;
$this->_sections['grid']['max'] = $this->_sections['grid']['loop'];
$this->_sections['grid']['step'] = 1;
$this->_sections['grid']['start'] = $this->_sections['grid']['step'] > 0 ? 0 : $this->_sections['grid']['loop']-1;
if ($this->_sections['grid']['show']) {
    $this->_sections['grid']['total'] = $this->_sections['grid']['loop'];
    if ($this->_sections['grid']['total'] == 0)
        $this->_sections['grid']['show'] = false;
} else
    $this->_sections['grid']['total'] = 0;
if ($this->_sections['grid']['show']):

            for ($this->_sections['grid']['index'] = $this->_sections['grid']['start'], $this->_sections['grid']['iteration'] = 1;
                 $this->_sections['grid']['iteration'] <= $this->_sections['grid']['total'];
                 $this->_sections['grid']['index'] += $this->_sections['grid']['step'], $this->_sections['grid']['iteration']++):
$this->_sections['grid']['rownum'] = $this->_sections['grid']['iteration'];
$this->_sections['grid']['index_prev'] = $this->_sections['grid']['index'] - $this->_sections['grid']['step'];
$this->_sections['grid']['index_next'] = $this->_sections['grid']['index'] + $this->_sections['grid']['step'];
$this->_sections['grid']['first']      = ($this->_sections['grid']['iteration'] == 1);
$this->_sections['grid']['last']       = ($this->_sections['grid']['iteration'] == $this->_sections['grid']['total']);
?>

<tr<?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this);?>
>
    <input type="hidden" name="posted_data[<?php echo $this->_tpl_vars['groups'][$this->_sections['grid']['index']]['id']; ?>
][gid]" value=<?php echo $this->_tpl_vars['products'][$this->_sections['grid']['index']]['id']; ?>
 />
	<td><input type="checkbox" name="posted_data[<?php echo $this->_tpl_vars['groups'][$this->_sections['grid']['index']]['id']; ?>
][to_delete]" /></td>
	<td align="center"><input type="text" name="posted_data[<?php echo $this->_tpl_vars['groups'][$this->_sections['grid']['index']]['id']; ?>
][group_name]" value="<?php echo $this->_tpl_vars['groups'][$this->_sections['grid']['index']]['group_name']; ?>
" /></td>
	<td align="center"><input type="text" name="posted_data[<?php echo $this->_tpl_vars['groups'][$this->_sections['grid']['index']]['id']; ?>
][group_label]" value="<?php echo $this->_tpl_vars['groups'][$this->_sections['grid']['index']]['group_label']; ?>
" /></td>
	<td align="center"><textarea name="posted_data[<?php echo $this->_tpl_vars['groups'][$this->_sections['grid']['index']]['id']; ?>
][description]" ><?php echo $this->_tpl_vars['groups'][$this->_sections['grid']['index']]['description']; ?>
</textarea></td>
	<td align="center"><input type="text" name="posted_data[<?php echo $this->_tpl_vars['groups'][$this->_sections['grid']['index']]['id']; ?>
][config]" value="<?php echo $this->_tpl_vars['groups'][$this->_sections['grid']['index']]['config']; ?>
" /></td>
	<td align="center"><input type="text" name="posted_data[<?php echo $this->_tpl_vars['groups'][$this->_sections['grid']['index']]['id']; ?>
][display_order]" value="<?php echo $this->_tpl_vars['groups'][$this->_sections['grid']['index']]['display_order']; ?>
"  size="1" /></td>
	<td align="center">
	<select name="posted_data[<?php echo $this->_tpl_vars['groups'][$this->_sections['grid']['index']]['id']; ?>
][is_active]">
		<option value="1"<?php if ($this->_tpl_vars['groups'][$this->_sections['grid']['index']]['is_active'] == '1'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_yes']; ?>
</option>
		<option value="0"<?php if ($this->_tpl_vars['groups'][$this->_sections['grid']['index']]['is_active'] == '0'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_no']; ?>
</option>
	</select>
	</td>
</tr>

<?php endfor; endif; ?>
<td colspan="5"><br /><br /></td>
<?php else: ?>

<tr>
 <td colspan="4" align="center">No groups available.</td>
</tr>
<?php endif; ?>

<!--<tr>
<td colspan="5"><br /><br /><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_add_product'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr> -->

<tr>
	<td colspan="5" class="SubmitBox">
	<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_delete_selected'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="return confirmDelete();" />
	<input type="submit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_update'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: document.groupfrm.mode.value = 'update'; document.groupfrm.submit();" />

	</td>
</tr>


</table>
</form>
<form name="frmG" action="<?php echo $this->_tpl_vars['form_action']; ?>
" method="post" enctype="multipart/form-data" >
<input type="hidden" name="mode" value="addgroup" />
<table cellpadding="3" cellspacing="1" width="100%">
   <tr>
        <td><hr/></td>
        <td><hr /></td>
   </tr>
   <tr>
        <td><h2>Add Group</h2></td>
        <td>&nbsp;</td>
   </tr>
   <tr>
        <td>Group Name </td>
        <td><input type="text" name="group_name" value=""  /></td>
        <td>Group Label </td>
        <td><input type="text" name="group_label" value=""  /></td>

   </tr>
   <tr>
    <td>Description </td>
        <td colspan="2"><textarea name="description" cols="25" rows="5" ></textarea></td>
   </tr>
   <tr>
     <td>Display Order </td>
     <td><input type="text" name="display_order" /></td>
     <td>Config </td>
     <td><input type="text" name="config" /></td>

     <td>Active</td>
     <td align="center">
        <select name="is_active">
            <option value="1" ><?php echo $this->_tpl_vars['lng']['lbl_yes']; ?>
</option>
            <option value="0"><?php echo $this->_tpl_vars['lng']['lbl_no']; ?>
</option>
        </select>
	</td>
   </tr>
   <tr>
   <td colspan="2" align="center">
   <input type="submit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_add'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" name="sbtFrm" />
   </td>
   </tr>

</table>
</form>
<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean();  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'Filter Groups','content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<br /><br />

