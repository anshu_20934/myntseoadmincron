<?php /* Smarty version 2.6.12, created on 2017-03-27 11:55:19
         compiled from admin/telesales_view_new.tpl */ ?>

<?php echo '
<style type="text/css">

.telesales-overlay {
	position:fixed;
	top:0;
	left:0;
	opacity:0.5;
	background:#000;
	display:none;
	filter:alpha(opacity=50);
	height:100%;
	width:100%;
	z-index:1000;
}

.telesales-form{
	display:none;
    position: fixed;
    top:25%;
    right:35%;
    z-index:1001;
    background: none repeat scroll 0 0 #FFFFFF;
    margin: 5px;
    padding:5px 10px;
    z-index:1001;
}
.telesales-form2{
	display:none;
    position: fixed;
    z-index:1001;
    background: none repeat scroll 0 0 #FFFFFF;
    margin: 5px;
    padding:5px 10px;
    height: 75%;
    left: 20%;
    overflow: auto;
    right: 30%;
    top: 10%;
    width: 50%;
}
 
.tele-loading-overlay{
	width:100%;
	height:100%;
	position:absolute;
	top:0;
	left:0;
	text-align:center;
	z-index:1002;
	background:#ffffff;
	opacity:0.5;
	filter:alpha(opacity=50);
}
.telesales_error{
		color:red;
		font-weight:bold;
	}
.telesales_success{
		color:green;
		font-weight:bold;
	}
	
.bgeven{
	background-color: lightgrey;
}
.discounted-mrp{
	display: inline;color: #666666; font-size: 13px; font-weight: bold; margin-top: -5px;text-decoration: line-through;
}
.final-price{
	display: inline;padding:2px 5px;color: #BD1A00;font-size: 13px;font-weight: bold;
}
</style>

'; ?>



<div>
	<div id="logged" style="border: 2px solid black;float: left;padding: 2px 15px 2px 65px;width: 700px;display: none;">
			<div class="tele-loading-overlay " style="display: none">
						<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/myntra_images/loading-graphic.gif" alt="loading graphic">
			</div>
			
			<div style="float: left; padding-top: 7px;padding-bottom: 7px;line-height:14px; height: 41px; width:600px;"><label id='loginname'></label><a href="javascript:void(0);" onclick="cclogout('S');$('#logged').hide();$('#switchuser').show();" style="padding-left: 15px">Switch User</a>
                        </div>
	</div>
	<div id="unlogged" style="border: 2px solid black;float: left;padding: 15px 15px 15px 65px;width: 700px;">
			<div class="tele-loading-overlay " style="display: none">
						<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/myntra_images/loading-graphic.gif" alt="loading graphic">
			</div>
			<label ><b>No user logged in</b></label>
			<a href="javascript:void(0);" onclick="$('#unlogged').hide();$('#switchuser').show();" style="padding-left: 15px">Login as user</a>
			<a href="javascript:void(0);" onclick="$('.telesales-overlay').show();$('#adduserdiv').show();$('#adduserdiv input.newuserinputtext').val('');$('#customerSearchResults tr:gt(0)').remove();" style="padding-left: 15px">Create New User</a>
	</div>
	<div id='switchuser' style="border: 2px solid black;float: left;padding: 15px 15px 15px 65px;width: 700px;display:none;">
		<div class="tele-loading-overlay " style="display: none">
						<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/myntra_images/loading-graphic.gif" alt="loading graphic">
		</div>
		<form id="switchuserform" action="telesales-new.php">
			<input type="hidden" name="action" value="login">	
			<label>Email</label>
			<input class="ccemailId" type="text" name="loginid" value="" onkeydown="<?php echo 'if (event.keyCode == \'13\'){cclogin();return false;	}'; ?>
" style="padding-left: 15px 15px;">
			<input type="button" value="Login" style="padding-left: 15px 15px; margin-left: 15px;background-color: orange;color: white;" onclick="cclogin();">
			<input type="button" value="Cancel" style="padding-left: 15px 15px; margin-left: 15px;" onclick="$('#switchuser').hide();$('#unlogged').show();">
		</form>
	</div>
	<div style="clear:both;">
	&nbsp;
	</div>
	
	
	<div class="telesales-overlay" onclick="$('#adduserdiv').hide();$('#cartpage').hide();$('.telesales-overlay').hide();$('#shippingaddresspage').hide();"></div>
	<div id='adduserdiv' class="telesales-form" style="border: 2px solid black;float: left;padding: 15px 15px 15px 65px;width: 400px;display:none;">
		
		<div class="tele-loading-overlay " style="display: none">
						<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/myntra_images/loading-graphic.gif" alt="loading graphic">
		</div>
		<div>
		<form id="adduserform" action="telesales-new.php">
			<input type="hidden" name="action" value="registeruser">
			<table style="width: 100%;">
				<tr>
					<td style="width: 50%;"><label>Name</label></td>
					<td><input class='newuserinputtext' type="text" name="username" value="" id="newusername"></td>
				</tr>
				<tr>
					<td style="width: 50%;"><label>Email Address</label></td>
					<td><input class='newuserinputtext' type="text" name="loginid" value=""  id="newuserloginid"></td>
				</tr>
				<tr>
					<td style="width: 50%;"><label>Phone number</label></td>
					<td><input class='newuserinputtext' type="text" name="phoneno" value=""  id="newuserphoneno"></td>
				</tr>
			</table>
		</form>
		</div>
		<div>
			<input id="createAccountButton" type="button" style=" margin-left: 160px;margin-top: 20px;margin-right:20px;" value="Create Account" onclick="registerNewUser()">
			<input type="button" value="Cancel" onclick="$('#adduserdiv').hide();$('.telesales-overlay').hide();">
		</div>
		<h1 style="margin-top: 20px;">Accounts Found</h1>
		<hr noshade size=7>
		<div style="height: 150px;overflow: auto;">
			<table id="customerSearchResults" style="width: 100%;">				
			<tr><td></td></tr>
			</table>
		</div>

	</div>
	
	<div style="clear:both;">
	&nbsp;
	</div>
        <div id='codavailability' style="border: 2px solid black;float: left;padding: 2px 15px 2px 65px;width: 700px;">
		<div class="tele-loading-overlay " style="display: none">
						<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/myntra_images/loading-graphic.gif" alt="loading graphic">
		</div>
		<form id="codavailabilityform" action="telesales-new.php" onsubmit="return false;">
		<div style="float: left;padding:10px;">
			<input type="hidden" name="action" value="codcheck">	
			<input type="text" name="pincode" id="pincodevalue" maxlength="10" value="Enter Zipcode"  onkeydown="<?php echo 'if (event.keyCode == \'13\'){codAvailability();return false;	}'; ?>
" onfocus="<?php echo 'if(this.value==\'Enter Zipcode\'){this.value=\'\';}'; ?>
" onblur="<?php echo 'if($.trim(this.value)==\'\'){this.value=\'Enter Zipcode\';}'; ?>
" style="padding-left: 15px;">
			<input type="button" value="COD Available?" style="padding-left: 15px 15px; margin-left: 15px;margin-right: 120px;background-color: orange;color: white;" onclick="codAvailability();">
		</div>
		<div>
				<div id='codavailabilitysuccess'  style="display: none;"><div style="float: left; margin-right: 10px;padding-top:5px;"><img style="width: 32px"
					src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/accept.png"> </div><div class="telesales_success" style="padding-top:12px;color: green;">ZipCode <label class="zipcode"></label> is serviceable by cod</div></div>
				<div id='codavailabilityfailed'  style="display: none"><div style="float: left; margin-right: 10px;padding-top:5px;"><img style="width: 32px"
					src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/delete_te.png"></div><div class="telesales_error" style="padding-top:12px; color: red"> ZipCode <label class="zipcode"></label> is not serviceable by cod</div></div>
		</div>
		</form>
	</div>
</div>
<?php echo '
<script type="text/javascript">
var loggeduser="";
var emailReg = /^([\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4})?$/;
var numericReg  =  /(^-?\\d\\d*\\.\\d*$)|(^-?\\d\\d*$)|(^-?\\.\\d\\d*$)/;

$(\'.igonore-enter-press\').keydown(function(event){
	if (event.keyCode == \'13\'){
		return false;
	}
});

function cclogout(switchto){
	 loggeduser="";
}

function cclogin(){
	var form=$(\'#switchuserform\');
        //alert ("here");
	$("#switchuser .tele-loading-overlay").css("display",\'block\');
	$.post(form.attr("action"), form.serialize() , function(data){
           // alert("here3");
		$("#switchuser .tele-loading-overlay").hide();
		if(data==null || $.trim(data)==\'\'){
			alert(\'Connection Problem!\');
			return;
		}
		var response=jQuery.parseJSON(data);
		if(response[\'status\']==\'S\'){
                        if (response[\'passwd\']=="N"){
                                alert("this is a suspended user");
                                return;
                        }
			$(\'#logged\').show();
                        $(\'#loginname\').html("<b>"+"Name:"+response[\'firstname\']+" "+response[\'lastname\']+"</b><br>Username: "+response[\'loginid\']+"<br>" + "<a href=\'"+response[\'root\']+"mymyntra.php?usern="+response[\'loginid\']+"&passwd="+response[\'passwd\']+"&forcelogout=true\' target=\'_blank\'>Launch myntra as this user</a>");
                        var url = response[\'root\']+"?forcelogout&usern="+response[\'loginid\']+"&passwd="+response[\'passwd\'];
			$(\'#switchuser\').hide();
			window.open(url,\'_blank\');
                        //$(\'#loginname  a\').click();
                        window.focus();
			loggeduser=response[\'loginid\'];
			
		}else if(response[\'status\']==\'F\' && response[\'errorcode\']==\'0\'){
			alert(\'Login (\'+response[\'loginid\']+\') doesn\\\'t exists\' );
			return;
		}
			
	});
	
}
function codAvailability(){
	if($(\'#pincodevalue\').val()=="Enter Zipcode" || $.trim($(\'#pincodevalue\').val())==""  ){
		alert("Please enter Zipcode!");
		return false;
	}
	var form=$(\'#codavailabilityform\');
	$("#codavailability .tele-loading-overlay").css("display",\'block\');
	$.post(form.attr("action"), form.serialize() , function(data){
		$("#codavailability .tele-loading-overlay").hide();
		if(data==null || $.trim(data)==\'\'){
			alert(\'Connection Problem!\');
			return false;
		}
		var response=jQuery.parseJSON(data);
		if(response[\'status\']==\'S\' && response[\'available\']==\'true\'){
			$(\'.zipcode\').html(response[\'zipcode\']);
			$(\'#codavailabilitysuccess\').show();
			$(\'#codavailabilityfailed\').hide();
		}else {
			$(\'.zipcode\').html(response[\'zipcode\']);
			$(\'#codavailabilityfailed\').show();
			$(\'#codavailabilitysuccess\').hide();
		}
			
		return false;
	});
	return false;
}
function registerNewUser(){

	if(!validateNewCustomer()){
		return false;
	}
	var form=$(\'#adduserform\');
	$("#adduserdiv .tele-loading-overlay").css("display",\'block\');
	$.post(form.attr("action"), form.serialize() , function(data){
		$("#adduserdiv .tele-loading-overlay").hide();
		if(data==null || $.trim(data)==\'\'){
			alert(\'Connection Problem!\');
			return;
		}
		var response=jQuery.parseJSON(data);
		if(response[\'status\']==\'S\'){
			alert(\'Account created successfully!\');
                        createccLogin($("#newuserloginid").val());
		}else {
			alert(\'Email id already registered!\');
		}
		$(\'#customerSearchResults tr:gt(0)\').remove();
		rowi=0;
		if(response[\'customers\']!=null){
			for (var key in response[\'customers\']) {
				addCustomerSearchResult(response[\'customers\'][key]);
			}
		}
	
	});
}
var rowi=0;

function addCustomerSearchResult(custinfoarray){
	 $("#customerSearchResults tr:last").after("<tr "+(rowi%2==1?"style=\'background-color: #F0F0F0;\'":"")+" ><td style=\'width:50%\'><div style=\'padding: 15px;\'><label><b>"+custinfoarray[\'firstname\']+" "+
			 custinfoarray[\'lastname\']+"</b><br>"+custinfoarray[\'loginid\']+"<br>"+custinfoarray[\'mobile\']
	 +\'</label></div></td><td style="text-align: center;"><div id="reset\'+rowi+\'"><input type=\\"button\\" value=\\"Reset Password\\" onclick="resetPassword(\\\'reset\'+rowi+\'\\\',\\\'\'+custinfoarray[\'loginid\']+\'\\\')"><br><input type=\\"button\\" value=\\"Login\\" style="margin-top: 10px;" onclick="createccLogin(\\\'\'+custinfoarray[\'loginid\']+\'\\\')"></div></td></tr>\');
	 rowi++;
}
function createccLogin(email){
        $(\'#adduserdiv\').hide();$(\'.telesales-overlay\').hide();
        $(\'#unlogged\').hide();$(\'#switchuser\').show();
        $(\'#switchuserform .ccemailId\').val(email);
        cclogin();
    }
function resetPassword(key,email){
	$(".tele-loading-overlay").css("display",\'block\');
	$.post("telesales-new.php", "action=resetpassword&loginid="+email , function(data){
		$(".tele-loading-overlay").hide();
		if(data==null || $.trim(data)==\'\'){
			alert(\'Connection Problem!\');
			return;
		}
		var response=jQuery.parseJSON(data);
		if(response[\'status\']==\'S\'){
			$("#"+key).html("<label style=\'color: green;\'>Password sent to user\'s email</label>");
		}else {
			alert("Couldn\'t reset the password");
		}
	});
}

function validateNewCustomer(){
	var error=false;
	var nameVal=$("#newusername").val();
	var mobileVal=$("#newuserphoneno").val();
	var emailVal=$("#newuserloginid").val();
	var str="";
	if(nameVal == "" || $.trim(nameVal)==""){
		error=true;
		str+="Invalid Name\\n";
	}

	
	if(mobileVal == "" || mobileVal.length != 10 || !numericReg.test(mobileVal) ){
		error=true;
		str+="Invalid Mobile number\\n";
	}
	
	if(emailVal == "" || !emailReg.test(emailVal)){
		error=true;
		str+="Invalid Email id\\n";
	}

	if(error){
		alert("Error! \\n"+str);
	}
    return !error;
}



function stripHtml(str) {
	if($.trim(str)=="")
		return str;
    return str.replace(/<\\/?[^>]+>/gi, \'\');
}

</script>
'; ?>
