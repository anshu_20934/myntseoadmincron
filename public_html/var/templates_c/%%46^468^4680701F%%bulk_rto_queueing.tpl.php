<?php /* Smarty version 2.6.12, created on 2017-09-27 15:24:34
         compiled from admin/main/bulk_rto_queueing.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "main/popup_product.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['SkinDir']; ?>
/skin1_admin_operations.css" />

<?php echo '
<script type="text/javascript">

    function resetScanning(type) {
        $("#return_"+type+"_div").hide();  
        $("#"+type+"_table_div").hide();    
        $("#"+type+"_table_contents").html(\'\');
        $("#scanned_"+type).val(\'\');
        
        $("#results_table_div").hide();
	    $("#results_table_contents").html(\'\'); 
	    $("#queue_rtos").attr(\'disabled\', true);
	    $("#valid_rto_orderids").val(\'\');
    }
    
    $(document).ready(function(){
        $("#orderid").focus();
        $(\'#preloader\').hide(); 
         
        resetScanning(\'orders\');
        resetScanning(\'items\');
        
        $(\'#preloader\')
            .ajaxStart(function(){
                $(this).show();
            }).ajaxStop(function(){
                $(this).hide();
            }); 
            
        $("input[name=\'scan_by\']").change(function(){
            $("#items_table").html(\'\');
            if( $(this).val() == "scan_by_orderid" ) {
                $(\'#return_orders_div\').show();
                resetScanning(\'items\');
            } else {
                $(\'#return_items_div\').show();   
                resetScanning(\'orders\');
            }   
        });
        
        $("#orderid").keydown(function(event) {
            // Allow only backspace and delete
            if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode ==13) {
                // let it happen, don\'t do anything
            }
            else {
                // Ensure that it is a number and stop the keypress
                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault(); 
                }   
            }
        });
        
        $("#itembarcode").keydown(function(event) {
            // Allow only backspace and delete
            if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode ==13) {
                // let it happen, don\'t do anything
            }
            else {
                // Ensure that it is a number and stop the keypress
                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault(); 
                }   
            }
        });
    });
    
    function createOrderIdsHtml(orderIds) {
        var orderIds_html = "";
        for(var i in orderIds){
            orderIds_html +=  orderIds==""?"":"<br>";
            orderIds_html +=  orderIds[i];
        }
        
        return (orderIds_html==""?"-":orderIds_html);
    }
    
    function pushOrdersToRTOTracker() {
        $(\'#preloaderText\').text(\'Pushing...\');
        $("#return_orders_div").hide();
        $("#orders_table_div").hide();
        var valid_rto_orderids = $("#valid_rto_orderids").val();
        var adminLogin = $("#admin_login").val();
        $.ajax({
            type: "POST",
            url: "",
            data: "action=queue_rto_orders&orderids="+valid_rto_orderids+"&login="+adminLogin+"&warehouseid="+$("#warehouse_id").val(),
            success: function(response){
                
                var queuedOrdersCOD = response[\'queued\'][\'cod\'];
                var queuedOrdersNONCOD = response[\'queued\'][\'on\'];
                
                var failedOrdersCOD = response[\'failed\'][\'cod\'];
                var failedOrdersNONCOD = response[\'failed\'][\'on\'];
                
                var countsRow = \'<tr><td align=center>Total</td>\';
                countsRow += \'<td align=center>\'+ queuedOrdersCOD.length+\'</td>\';
                countsRow += \'<td align=center>\'+queuedOrdersNONCOD.length+\'</td>\';
                countsRow += \'<td align=center>\'+failedOrdersCOD.length+\'</td>\';
                countsRow += \'<td align=center>\'+failedOrdersNONCOD.length+\'</td></tr>\';
                
                $("#results_table_contents").append(countsRow);
                
                var orderidsRow = \'<tr><td align=center>Order Ids</td>\';
                orderidsRow += \'<td align=center>\'+createOrderIdsHtml(queuedOrdersCOD)+\'</td>\';
                orderidsRow += \'<td align=center>\'+createOrderIdsHtml(queuedOrdersNONCOD)+\'</td>\';
                orderidsRow += \'<td align=center>\'+createOrderIdsHtml(failedOrdersCOD)+\'</td>\';
                orderidsRow += \'<td align=center>\'+createOrderIdsHtml(failedOrdersNONCOD)+\'</td>\';
                orderidsRow += \'</tr>\';
                
                $("#results_table_contents").append(orderidsRow);
                $(\'#results_table_div\').show();
            }
        });
    }
    
    function clearScanning() {
        this.window.location.reload();
    }
    
    function orderScanned(){
        var orderid = $("#orderid").val();
        if(orderid == null || orderid.trim() == "" || orderid == "") {
            alert("Blank Order Id");
            return false;
        }

        if($("#warehouse_id").val() == "") {
            alert("Select an Ops Location");
            return false;
        }

        if($("#scanned_orders").val().indexOf(orderid) != -1){
            alert("ERROR: Order already scanned: " + orderid);
            $("#orderid").val(\'\');
            return false;
        }
        
        $(\'#preloaderText\').text(\'Receiving...\');
        $.ajax({
            type: "POST",
            url: "",
            data: "action=order_check&orderid="+orderid+"&warehouseid="+$("#warehouse_id").val(),
            success: function(response){
                var bgcolorcode = response[\'color\'];
                if(response[\'status\'] == "Success") {
                    var valid_rto_orderids = $("#valid_rto_orderids").val();
	                valid_rto_orderids += valid_rto_orderids==""?"":",";
	                valid_rto_orderids += response[\'orderid\'];
	                $("#valid_rto_orderids").val(valid_rto_orderids);
	                $("#queue_rtos").attr(\'disabled\', false);
                }
                
                var newRow = \'<tr bgcolor="\'+bgcolorcode+\'"><td align=center>\'+response[\'orderid\']+\'</td>\';
                newRow += \'<td align=center>\'+response[\'type\']+\'</td>\';
                newRow += \'<td align=center>\'+response[\'shippingdate\']+\'</td>\';
                newRow += \'<td align=center>\'+response[\'remarks\']+\'</td></tr>\';
                $("#orders_table_contents").prepend(newRow);
                $(\'#orders_table_div\').show();
                var scanned_orders = $("#scanned_orders").val();
                scanned_orders += scanned_orders==""?"":",";
                scanned_orders += response[\'orderid\'];
                $("#scanned_orders").val(scanned_orders);
                if(scanned_orders != "")
                    $("#warehouse_id").attr("disabled", true);
            }
        });
         
        $("#orderid").val(\'\');
        $("#orderid").focus();
        return false;
    }
    
    function itemScanned(){
        var itembarcode = $("#itembarcode").val();
        if(itembarcode == null || itembarcode.trim() == "" || itembarcode == "") {
            alert("Blank Item Barcode");
            return false;
        }

        if($("#warehouse_id").val() == "") {
            alert("Select an Ops Location");
            return false;
        }

        if($("#scanned_items").val().indexOf(itembarcode) != -1){
            alert("ERROR: Item already scanned: " + itembarcode);
            $("#itembarcode").val(\'\');
            return false;
        }
        
        $(\'#preloaderText\').text(\'Receiving...\');
        $.ajax({
            type: "POST",
            url: "",
            data: "action=item_check&itembarcode="+itembarcode+"&warehouseid="+$("#warehouse_id").val(),
            success: function(response){
                var bgcolorcode = "";
                if(response[\'status\'] == "Failure") {
                    bgcolorcode = "#F21633";
                } else {
                    bgcolorcode = "#22B804";
                }
                
                var newRow = \'<tr bgcolor="\'+bgcolorcode+\'"><td align=center>\'+response[\'itembarcode\']+\'</td>\';
                newRow += \'<td align=center>\'+response[\'remarks\']+\'</td></tr>\';
                $("#items_table_contents").prepend(newRow);
                $(\'#items_table_div\').show();
                var scanned_items = $("#scanned_items").val();
                scanned_items += scanned_items==""?"":",";
                scanned_items += response[\'itembarcode\'];
                $("#scanned_items").val(scanned_items);
                if(scanned_items != "")
                    $("#warehouse_id").attr("disabled", true);
            }
        });
         
        $("#itembarcode").val(\'\');
        $("#itembarcode").focus();
        return false;
    }
    
</script>
'; ?>

<p align="center"><font color="red" size="30">Use REJOY for RTO processing</font></p>