<?php /* Smarty version 2.6.12, created on 2017-06-02 16:19:48
         compiled from main/orders.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'default', 'main/orders.tpl', 19, false),array('modifier', 'date_format', 'main/orders.tpl', 19, false),array('modifier', 'escape', 'main/orders.tpl', 404, false),array('modifier', 'formatprice', 'main/orders.tpl', 454, false),array('modifier', 'trademark', 'main/orders.tpl', 483, false),array('modifier', 'strip_tags', 'main/orders.tpl', 514, false),array('modifier', 'substitute', 'main/orders.tpl', 732, false),array('function', 'html_select_date', 'main/orders.tpl', 262, false),)), $this); ?>
<?php func_load_lang($this, "main/orders.tpl","txt_adm_search_orders_result_header,txt_search_orders_header,txt_search_orders_text,lbl_date_period,lbl_this_month,lbl_this_week,lbl_today,lbl_all_dates,lbl_specify_period_below,lbl_order_date_from,lbl_order_date_through,lbl_order_id,lbl_order_status,lbl_ccavenue_status,lbl_search_and_export,lbl_search,lbl_advanced_search_options,lbl_advanced_search_options,txt_adv_search_orders_text,lbl_order_id,lbl_order_total,lbl_payment_method,lbl_delivery,lbl_order_status,lbl_provider,lbl_order_features,lbl_entirely_or_partially_payed_by_gc,lbl_global_discount_applied,lbl_discount_coupon_applied,lbl_free_shipping,lbl_tax_exempt,lbl_gc_purchased,lbl_orders_with_notes_assigned,lbl_hold_ctrl_key,lbl_search_by_ordered_products,lbl_search_for_pattern,lbl_search_in,lbl_product_title,lbl_options,lbl_sku,lbl_productid,lbl_price,lbl_search_by_customer,lbl_customer,lbl_search_in,lbl_username,lbl_first_name,lbl_last_name,lbl_search_by_address,lbl_ignore_address,lbl_billing,lbl_shipping,lbl_both,lbl_city,lbl_state,lbl_country,lbl_please_select_one,lbl_zip_code,lbl_phone,lbl_fax,lbl_email,lbl_search,lbl_reset,txt_N_results_found,txt_displaying_X_Y_results,txt_N_results_found,lbl_export_delete_orders,lbl_export_orders"); ?>

<?php if ($this->_tpl_vars['orders'] != ""): ?>
<?php if ($this->_tpl_vars['usertype'] == 'A' || ( $this->_tpl_vars['usertype'] == 'P' && $this->_tpl_vars['active_modules']['Simple_Mode'] )): ?>
<?php echo $this->_tpl_vars['lng']['txt_adm_search_orders_result_header']; ?>

<?php elseif ($this->_tpl_vars['usertype'] == 'P'): ?>
<?php echo $this->_tpl_vars['lng']['txt_search_orders_header']; ?>

<?php endif; ?>
<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "reset.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<script language="javascript" type="text/javascript" src="<?php echo $this->_tpl_vars['http_location']; ?>
/skin1/main/ajax_orderhistory.js"></script>
<script type="text/javascript">
<!--
var searchform_def = new Array();
searchform_def[0] = new Array('posted_data[date_period]', '');
searchform_def[1] = new Array('StartDay', '<?php echo ((is_array($_tmp=((is_array($_tmp=@$this->_tpl_vars['search_prefilled']['start_date'])) ? $this->_run_mod_handler('default', true, $_tmp, time()) : smarty_modifier_default($_tmp, time())))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d") : smarty_modifier_date_format($_tmp, "%d")); ?>
');
searchform_def[2] = new Array('StartMonth', '<?php echo ((is_array($_tmp=((is_array($_tmp=@$this->_tpl_vars['search_prefilled']['start_date'])) ? $this->_run_mod_handler('default', true, $_tmp, time()) : smarty_modifier_default($_tmp, time())))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m") : smarty_modifier_date_format($_tmp, "%m")); ?>
');
searchform_def[3] = new Array('StartYear', '<?php echo ((is_array($_tmp=((is_array($_tmp=@$this->_tpl_vars['search_prefilled']['start_date'])) ? $this->_run_mod_handler('default', true, $_tmp, time()) : smarty_modifier_default($_tmp, time())))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y") : smarty_modifier_date_format($_tmp, "%Y")); ?>
');
searchform_def[4] = new Array('EndDay', '<?php echo ((is_array($_tmp=((is_array($_tmp=@$this->_tpl_vars['search_prefilled']['end_date'])) ? $this->_run_mod_handler('default', true, $_tmp, time()) : smarty_modifier_default($_tmp, time())))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d") : smarty_modifier_date_format($_tmp, "%d")); ?>
');
searchform_def[5] = new Array('EndMonth', '<?php echo ((is_array($_tmp=((is_array($_tmp=@$this->_tpl_vars['search_prefilled']['end_date'])) ? $this->_run_mod_handler('default', true, $_tmp, time()) : smarty_modifier_default($_tmp, time())))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m") : smarty_modifier_date_format($_tmp, "%m")); ?>
');
searchform_def[6] = new Array('EndYear', '<?php echo ((is_array($_tmp=((is_array($_tmp=@$this->_tpl_vars['search_prefilled']['end_date'])) ? $this->_run_mod_handler('default', true, $_tmp, time()) : smarty_modifier_default($_tmp, time())))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y") : smarty_modifier_date_format($_tmp, "%Y")); ?>
');
searchform_def[7] = new Array('posted_data[total_min]', '<?php echo $this->_tpl_vars['zero']; ?>
');
searchform_def[8] = new Array('posted_data[by_title]', true);
searchform_def[9] = new Array('posted_data[by_options]', true);
searchform_def[10] = new Array('posted_data[price_min]', '<?php echo $this->_tpl_vars['zero']; ?>
');
searchform_def[11] = new Array('posted_data[address_type]', '');

<?php echo '


function managedate(type, status) {
	if (type != \'date\')
		var fields = [\'posted_data[city]\',\'posted_data[state]\',\'posted_data[country]\',\'posted_data[zipcode]\'];
	else
		var fields = [\'StartDay\',\'StartMonth\',\'StartYear\',\'EndDay\',\'EndMonth\',\'EndYear\'];
	
	for (i in fields)
		if (document.searchform.elements[fields[i]])
			document.searchform.elements[fields[i]].disabled = status;
}

function dateValidate()
{

	if(document.searchform.StartYear.disabled == true)
	{
		document.searchform.submit();
	}
	else
	{
		var startYear = document.searchform.StartYear.value;
		var startMonth = document.searchform.StartMonth.value;
		var startDay = document.searchform.StartDay.value;
		var endYear = document.searchform.EndYear.value;
		var endMonth = document.searchform.EndMonth.value;
		var endDay = document.searchform.EndDay.value;

		if(startYear == endYear)
		{
			if(startMonth <= endMonth)
			{
				if(startMonth == endMonth)
				{
					if(startDay <= endDay)
					{
						return true;
					}
					else
					{
						alert("Start date can not greater then end date");
						return false;
					}
				}
				document.searchform.submit();
			}
			else
			{
				alert("Start month can not greater then end month");
				return false;
			}
		}
		

		if(startYear > endYear)
		{
			alert("Start year can not greater then end year");
			return false;
		}
		else
		{
			document.searchform.submit();
		}

		
		document.searchform.submit();
	}
}
function checkDate()
{
     dateObj1 = document.getElementById(\'startdate\');
     dateObj2 = document.getElementById(\'enddate\');
        
	 if(dateObj1.value != "" &&  dateObj2.value != "")
	 {
	     if(!isDate(dateObj1.value)) 
	     {
	     	  dateObj1.focus();
	     	  return false;
	     }
	     else if (!isDate(dateObj2.value))
	     {
	     	 dateObj2.focus();
	     	 return false;
	     }
	     else
	     {
	     
			var startArray = (dateObj1.value).split("/")
			var endArray = (dateObj2.value).split("/")
			
			var flag = dateDiff(startArray[2],startArray[0],startArray[1],endArray[2],endArray[0],endArray[1])
			if(flag)
			{
			   document.getElementById(\'date_period_null\').value = "true"
			}
			return flag
	     }
	     
	 }
	 else if ((dateObj1.value == "" ||  dateObj2.value == "") &&  document.getElementById(\'odId\').value == "")
      {
         alert("Please input invoice number or valid date range to search.")
	   	 return false;
      }
      else
        return true;
     
}

function popUpWindow(URL)
{
	
	window.open(URL,"Invoice", "menubar=1,resizable=1,toolbar=1,width=800,height=500");
}
'; ?>

-->
</script>


<?php if ($this->_tpl_vars['usertype'] == 'C'): ?>
<form name="searchform" action="orders.php" method="post">

<input type="hidden" name="mode" id="mode" value="" />

<div class="super"><p><?php echo $this->_tpl_vars['firstname']; ?>
's order history</p></div>
<div class="head"><p>search order history</p></div>

<div class="links">
<div class="field">
	<label>
	<p>
		<strong>Search by date</strong>
		<select name="dateduration" id="dateduration" class="select" onChange="javascript:searchOrder('dateduration', '<?php echo $this->_tpl_vars['login']; ?>
');">
			<option value="0">--select duration--</option>
			<option value="7">&nbsp;Last 7 days</option>
			<option value="1M">&nbsp;Last 1 month</option>
			<option value="3M">&nbsp;Last 3 month</option>
			<option value="1Y">&nbsp;Last 1 year</option>
			<!-- <option value="all">&nbsp;All</option> -->
		</select>
	</p>
	</label>
    </div>   

<div class="clearall"></div>

</div>


<div class="foot"></div>
        
</form>


<?php endif; ?>

<?php if ($this->_tpl_vars['usertype'] == 'C'): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/ajax_orderslist.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>

<?php if ($this->_tpl_vars['mode'] == 'search'): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/orders_list.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>

<?php if ($this->_tpl_vars['search_prefilled']['need_advanced_options']): ?>

<?php endif; ?>


<?php if (( $this->_tpl_vars['usertype'] == 'A' || $this->_tpl_vars['usertype'] == 'P' ) && $this->_tpl_vars['mode'] != 'search'): ?>


<?php ob_start(); ?>
<form name="searchform" action="orders.php" method="post">
<input type="hidden" name="mode" value="" />

<table cellpadding="0" cellspacing="0" width="100%">

<tr>
	<td>

<table cellpadding="1" cellspacing="5" width="100%" >

<tr>
	<td colspan="3">
<?php echo $this->_tpl_vars['lng']['txt_search_orders_text']; ?>

<br /><br />
	</td>
<td >
<input type="button" value="Add offline order" onclick="javascript: window.location='offline_orders.php'" />
<br /><br />
	</td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_date_period']; ?>
:</td>
	<td width="10">&nbsp;</td>
	<td>
<table cellpadding="0" cellspacing="0">
<tr>
	<td width="5"><input type="radio" id="date_period_M" name="posted_data[date_period]" value="M"<?php if ($this->_tpl_vars['search_prefilled']['date_period'] == 'M' || $this->_tpl_vars['search_prefilled']['date_period'] == ""): ?> checked="checked"<?php endif; ?> onclick="javascript:managedate('date',true)" /></td>
	<td class="OptionLabel"><label for="date_period_M"><?php echo $this->_tpl_vars['lng']['lbl_this_month']; ?>
</label></td>

	<td width="5"><input type="radio" id="date_period_W" name="posted_data[date_period]" value="W"<?php if ($this->_tpl_vars['search_prefilled']['date_period'] == 'W'): ?> checked="checked"<?php endif; ?> onclick="javascript:managedate('date',true)" /></td>
	<td class="OptionLabel"><label for="date_period_W"><?php echo $this->_tpl_vars['lng']['lbl_this_week']; ?>
</label></td>

	<td width="5"><input type="radio" id="date_period_D" name="posted_data[date_period]" value="D"<?php if ($this->_tpl_vars['search_prefilled']['date_period'] == 'D'): ?> checked="checked"<?php endif; ?> onclick="javascript:managedate('date',true)" /></td>
	<td class="OptionLabel"><label for="date_period_D"><?php echo $this->_tpl_vars['lng']['lbl_today']; ?>
</label></td>
	<!--
	<td width="5"><input type="radio" id="date_period_A" name="posted_data[date_period]" value="A"<?php if ($this->_tpl_vars['search_prefilled'] == 'A' || $this->_tpl_vars['search_prefilled']['date_period'] == 'A'): ?> checked="checked"<?php endif; ?> onclick="javascript:managedate('date',true)" /></td>
	<td class="OptionLabel"><label for="date_period_A"><?php echo $this->_tpl_vars['lng']['lbl_all_dates']; ?>
</label></td>
	-->
</tr>
<tr>
	<td width="5"><input type="radio" id="date_period_C" name="posted_data[date_period]" value="C"<?php if ($this->_tpl_vars['search_prefilled']['date_period'] == 'C'): ?> checked="checked"<?php endif; ?> onclick="javascript:managedate('date',false)" /></td>
	<td colspan="7" class="OptionLabel"><label for="date_period_C"><?php echo $this->_tpl_vars['lng']['lbl_specify_period_below']; ?>
</label></td>
</tr>
</table>
</td>
</tr>

<tr> 
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_order_date_from']; ?>
:</td>
	<td width="10">&nbsp;</td>
	<td> 
	<?php echo smarty_function_html_select_date(array('prefix' => 'Start','time' => $this->_tpl_vars['search_prefilled']['start_date'],'start_year' => $this->_tpl_vars['config']['Company']['start_year'],'end_year' => $this->_tpl_vars['config']['Company']['end_year']), $this);?>

	</td>
</tr>

<tr> 
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_order_date_through']; ?>
:</td>
	<td width="10">&nbsp;</td>
	<td> 
	<?php echo smarty_function_html_select_date(array('prefix' => 'End','time' => $this->_tpl_vars['search_prefilled']['end_date'],'start_year' => $this->_tpl_vars['config']['Company']['start_year'],'end_year' => $this->_tpl_vars['config']['Company']['end_year'],'display_days' => true), $this);?>

	</td>
</tr>
<!-- added later to filter by orderid and status -->
<tr> 
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_order_id']; ?>
:</td>
	<td width="10">&nbsp;</td>
	<td> 
	  <input type="text" name="posted_data[orderid]" size="15" maxlength="15" 
		value="<?php echo $this->_tpl_vars['search_prefilled']['orderid']; ?>
" />
	</td>
</tr>
<tr> 
	<td class="FormButton" nowrap="nowrap">Customer Login:</td>
	<td width="10">&nbsp;</td>
	<td> 
	  <input type="text" name="posted_data[login]" size="50" maxlength="100" 
		value="<?php echo $this->_tpl_vars['search_prefilled']['login']; ?>
" />
	</td>
</tr>
<tr> 
	<td class="FormButton" nowrap="nowrap">Customer Phone No.:</td>
	<td width="10">&nbsp;</td>
	<td> 
	  <input type="text" name="posted_data[issues_contact_number]" size="20" maxlength="100" 
		value="<?php echo $this->_tpl_vars['search_prefilled']['issues_contact_number']; ?>
" />
	</td>
</tr>
<tr> 
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_order_status']; ?>
:</td>
	<td width="10">&nbsp;</td>
	<td> 
	  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/order_status.tpl", 'smarty_include_vars' => array('status' => $this->_tpl_vars['search_prefilled']['orderstatus'],'mode' => 'select','name' => "posted_data[orderstatus]",'extended' => 'Y','extra' => "style='width:70%'")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	</td>
</tr>
<!-- 
<tr> 
	<td class="FormButton" nowrap="nowrap">Order type:</td>
	<td width="10">&nbsp;</td>
	<td> 
	  <select name="posted_data[ordertype]" id="ordertype">
		<option value="0">---select ordertype---</option>
		<option value="on">Online</option>
		<option value="off">Offline</option>
	  </select>
	</td>
</tr>
 -->
<tr> 
	<td class="FormButton" nowrap="nowrap">Payment Method:</td>
	<td width="10">&nbsp;</td>
	<td> 
	  <select name="posted_data[payment_method]" id="payment_method">
		<option value="0">---select payment method---</option>
		<option value="on">Online</option>
		<option value="cod">Cash On Delivery</option>
	  </select>
	</td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap">Courier Operator:</td>
	<td width="10">&nbsp;</td>
	<td>
	  <select name="posted_data[courier_service]" id="courier_service">
            <option value="">---Select courier operator---</option>
	  <?php unset($this->_sections['courDet']);
$this->_sections['courDet']['name'] = 'courDet';
$this->_sections['courDet']['loop'] = is_array($_loop=$this->_tpl_vars['courier_details']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['courDet']['show'] = true;
$this->_sections['courDet']['max'] = $this->_sections['courDet']['loop'];
$this->_sections['courDet']['step'] = 1;
$this->_sections['courDet']['start'] = $this->_sections['courDet']['step'] > 0 ? 0 : $this->_sections['courDet']['loop']-1;
if ($this->_sections['courDet']['show']) {
    $this->_sections['courDet']['total'] = $this->_sections['courDet']['loop'];
    if ($this->_sections['courDet']['total'] == 0)
        $this->_sections['courDet']['show'] = false;
} else
    $this->_sections['courDet']['total'] = 0;
if ($this->_sections['courDet']['show']):

            for ($this->_sections['courDet']['index'] = $this->_sections['courDet']['start'], $this->_sections['courDet']['iteration'] = 1;
                 $this->_sections['courDet']['iteration'] <= $this->_sections['courDet']['total'];
                 $this->_sections['courDet']['index'] += $this->_sections['courDet']['step'], $this->_sections['courDet']['iteration']++):
$this->_sections['courDet']['rownum'] = $this->_sections['courDet']['iteration'];
$this->_sections['courDet']['index_prev'] = $this->_sections['courDet']['index'] - $this->_sections['courDet']['step'];
$this->_sections['courDet']['index_next'] = $this->_sections['courDet']['index'] + $this->_sections['courDet']['step'];
$this->_sections['courDet']['first']      = ($this->_sections['courDet']['iteration'] == 1);
$this->_sections['courDet']['last']       = ($this->_sections['courDet']['iteration'] == $this->_sections['courDet']['total']);
?>
	  	<option value="<?php echo $this->_tpl_vars['courier_details'][$this->_sections['courDet']['index']]['code']; ?>
"><?php echo $this->_tpl_vars['courier_details'][$this->_sections['courDet']['index']]['courier_service']; ?>
</option>
	  <?php endfor; endif; ?>
	  </select>
	</td>
</tr>

<tr>
    <td class="FormButton" nowrap="nowrap">Ops Location:</td>
    <td width="10">&nbsp;</td>
    <td>
        <select name="posted_data[warehouseid]" id="warehouseid">
            <option value="">---Select Ops Location---</option>
            <?php unset($this->_sections['idx']);
$this->_sections['idx']['name'] = 'idx';
$this->_sections['idx']['loop'] = is_array($_loop=$this->_tpl_vars['warehouses']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['idx']['show'] = true;
$this->_sections['idx']['max'] = $this->_sections['idx']['loop'];
$this->_sections['idx']['step'] = 1;
$this->_sections['idx']['start'] = $this->_sections['idx']['step'] > 0 ? 0 : $this->_sections['idx']['loop']-1;
if ($this->_sections['idx']['show']) {
    $this->_sections['idx']['total'] = $this->_sections['idx']['loop'];
    if ($this->_sections['idx']['total'] == 0)
        $this->_sections['idx']['show'] = false;
} else
    $this->_sections['idx']['total'] = 0;
if ($this->_sections['idx']['show']):

            for ($this->_sections['idx']['index'] = $this->_sections['idx']['start'], $this->_sections['idx']['iteration'] = 1;
                 $this->_sections['idx']['iteration'] <= $this->_sections['idx']['total'];
                 $this->_sections['idx']['index'] += $this->_sections['idx']['step'], $this->_sections['idx']['iteration']++):
$this->_sections['idx']['rownum'] = $this->_sections['idx']['iteration'];
$this->_sections['idx']['index_prev'] = $this->_sections['idx']['index'] - $this->_sections['idx']['step'];
$this->_sections['idx']['index_next'] = $this->_sections['idx']['index'] + $this->_sections['idx']['step'];
$this->_sections['idx']['first']      = ($this->_sections['idx']['iteration'] == 1);
$this->_sections['idx']['last']       = ($this->_sections['idx']['iteration'] == $this->_sections['idx']['total']);
?>
                <option value="<?php echo $this->_tpl_vars['warehouses'][$this->_sections['idx']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['warehouses'][$this->_sections['idx']['index']]['name']; ?>
</option>
            <?php endfor; endif; ?>
        </select>
    </td>
</tr>

<!-- 
<tr> 
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_ccavenue_status']; ?>
:</td>
	<td width="10">&nbsp;</td>
	<td> 
	  <select name="posted_data[ccavenue]" id="ordertype">
		<option value="0">---select status---</option>
		<option value="Y">Paid</option>
		<option value="N">Not Paid</option>
	  </select>
	</td>
</tr>

<tr> 
	<td class="FormButton" nowrap="nowrap">Select Organization:</td>
	<td width="10">&nbsp;</td>
	<td> 
	
	  <select name="posted_data[orgid]" id="orgid">
	  <option value="0">---select organization---</option>
	  <?php unset($this->_sections['org']);
$this->_sections['org']['name'] = 'org';
$this->_sections['org']['loop'] = is_array($_loop=$this->_tpl_vars['organization']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['org']['show'] = true;
$this->_sections['org']['max'] = $this->_sections['org']['loop'];
$this->_sections['org']['step'] = 1;
$this->_sections['org']['start'] = $this->_sections['org']['step'] > 0 ? 0 : $this->_sections['org']['loop']-1;
if ($this->_sections['org']['show']) {
    $this->_sections['org']['total'] = $this->_sections['org']['loop'];
    if ($this->_sections['org']['total'] == 0)
        $this->_sections['org']['show'] = false;
} else
    $this->_sections['org']['total'] = 0;
if ($this->_sections['org']['show']):

            for ($this->_sections['org']['index'] = $this->_sections['org']['start'], $this->_sections['org']['iteration'] = 1;
                 $this->_sections['org']['iteration'] <= $this->_sections['org']['total'];
                 $this->_sections['org']['index'] += $this->_sections['org']['step'], $this->_sections['org']['iteration']++):
$this->_sections['org']['rownum'] = $this->_sections['org']['iteration'];
$this->_sections['org']['index_prev'] = $this->_sections['org']['index'] - $this->_sections['org']['step'];
$this->_sections['org']['index_next'] = $this->_sections['org']['index'] + $this->_sections['org']['step'];
$this->_sections['org']['first']      = ($this->_sections['org']['iteration'] == 1);
$this->_sections['org']['last']       = ($this->_sections['org']['iteration'] == $this->_sections['org']['total']);
?>
	  	<option value="<?php echo $this->_tpl_vars['organization'][$this->_sections['org']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['organization'][$this->_sections['org']['index']]['subdomain']; ?>
</option>
	  <?php endfor; endif; ?>
	  </select>
	</td>
</tr>


-->
<!-- end -->
<tr>
	<td colspan="2"></td>
	<td>
	<hr />
<table cellpadding="0" cellspacing="0">
<tr>
	<td><input type="checkbox" id="posted_data_is_export" name="posted_data[is_export]" value="Y" /></td>
	<td>&nbsp;</td>
	<td class="FormButton" nowrap="nowrap"><label for="posted_data_is_export"><?php echo $this->_tpl_vars['lng']['lbl_search_and_export']; ?>
</label></td>
</tr>
</table>
	</td>
</tr>

<tr>
	<td colspan="2">&nbsp;</td>
	<td colspan="3" class="SubmitBox">
	<input type="submit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_search'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: document.searchform.mode.value=''; return dateValidate();" />

<?php if ($this->_tpl_vars['search_prefilled']['date_period'] != 'C'): ?>
<script type="text/javascript" language="JavaScript 1.2">
<!--
managedate('date',true);
-->
</script>
<?php endif; ?>
	</td>
</tr>

</table>

<br />

<!-- 
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/visiblebox_link.tpl", 'smarty_include_vars' => array('mark' => '1','title' => $this->_tpl_vars['lng']['lbl_advanced_search_options'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<br />
 -->

<table cellpadding="1" cellspacing="5" width="100%"<?php if ($this->_tpl_vars['js_enabled'] == 'Y'): ?> style="display: none;"<?php endif; ?> id="box1">

<tr>
	<td colspan="3"><br /><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_advanced_search_options'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>

<tr>
	<td colspan="3"><?php echo $this->_tpl_vars['lng']['txt_adv_search_orders_text']; ?>
<br /><br /></td>
</tr>

<tr>
	<td width="25%" class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_order_id']; ?>
:</td>
	<td width="10">&nbsp;</td>
	<td width="75%">
<input type="text" name="posted_data[orderid1]" size="10" maxlength="15" value="<?php echo $this->_tpl_vars['search_prefilled']['orderid1']; ?>
" />
-
<input type="text" name="posted_data[orderid2]" size="10" maxlength="15"value="<?php echo $this->_tpl_vars['search_prefilled']['orderid2']; ?>
" />
	</td>
</tr>

<?php if ($this->_tpl_vars['usertype'] != 'C'): ?>
<tr>
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_order_total']; ?>
 (<?php echo $this->_tpl_vars['config']['General']['currency_symbol']; ?>
):</td>
	<td width="10">&nbsp;</td>
	<td>

<table cellpadding="0" cellspacing="0">
<tr>
	<td><input type="text" size="10" maxlength="15" name="posted_data[total_min]" value="<?php if ($this->_tpl_vars['search_prefilled'] == ""):  echo $this->_tpl_vars['zero'];  else:  echo ((is_array($_tmp=$this->_tpl_vars['search_prefilled']['total_min'])) ? $this->_run_mod_handler('formatprice', true, $_tmp) : smarty_modifier_formatprice($_tmp));  endif; ?>" /></td>
	<td>&nbsp;-&nbsp;</td>
	<td><input type="text" size="10" maxlength="15" name="posted_data[total_max]" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['search_prefilled']['total_max'])) ? $this->_run_mod_handler('formatprice', true, $_tmp) : smarty_modifier_formatprice($_tmp)); ?>
" /></td>
</tr>
</table>

	</td>
</tr>
<!-- 
<tr>
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_payment_method']; ?>
:</td>
	<td width="10">&nbsp;</td>
	<td>
	<select name="posted_data[payment_method]" style="width:70%">
		<option value=""></option>
<?php unset($this->_sections['pm']);
$this->_sections['pm']['name'] = 'pm';
$this->_sections['pm']['loop'] = is_array($_loop=$this->_tpl_vars['payment_methods']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['pm']['show'] = true;
$this->_sections['pm']['max'] = $this->_sections['pm']['loop'];
$this->_sections['pm']['step'] = 1;
$this->_sections['pm']['start'] = $this->_sections['pm']['step'] > 0 ? 0 : $this->_sections['pm']['loop']-1;
if ($this->_sections['pm']['show']) {
    $this->_sections['pm']['total'] = $this->_sections['pm']['loop'];
    if ($this->_sections['pm']['total'] == 0)
        $this->_sections['pm']['show'] = false;
} else
    $this->_sections['pm']['total'] = 0;
if ($this->_sections['pm']['show']):

            for ($this->_sections['pm']['index'] = $this->_sections['pm']['start'], $this->_sections['pm']['iteration'] = 1;
                 $this->_sections['pm']['iteration'] <= $this->_sections['pm']['total'];
                 $this->_sections['pm']['index'] += $this->_sections['pm']['step'], $this->_sections['pm']['iteration']++):
$this->_sections['pm']['rownum'] = $this->_sections['pm']['iteration'];
$this->_sections['pm']['index_prev'] = $this->_sections['pm']['index'] - $this->_sections['pm']['step'];
$this->_sections['pm']['index_next'] = $this->_sections['pm']['index'] + $this->_sections['pm']['step'];
$this->_sections['pm']['first']      = ($this->_sections['pm']['iteration'] == 1);
$this->_sections['pm']['last']       = ($this->_sections['pm']['iteration'] == $this->_sections['pm']['total']);
?>
		<option value="<?php echo $this->_tpl_vars['payment_methods'][$this->_sections['pm']['index']]['payment_method']; ?>
"<?php if ($this->_tpl_vars['search_prefilled']['payment_method'] == $this->_tpl_vars['payment_methods'][$this->_sections['pm']['index']]['payment_method']): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['payment_methods'][$this->_sections['pm']['index']]['payment_method']; ?>
</option>
<?php endfor; endif; ?>
	</select>
	</td>
</tr>
 -->
<tr>
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_delivery']; ?>
:</td>
	<td width="10">&nbsp;</td>
	<td>
	<select name="posted_data[shipping_method]" style="width:70%">
		<option value=""></option>
<?php unset($this->_sections['sm']);
$this->_sections['sm']['name'] = 'sm';
$this->_sections['sm']['loop'] = is_array($_loop=$this->_tpl_vars['shipping_methods']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sm']['show'] = true;
$this->_sections['sm']['max'] = $this->_sections['sm']['loop'];
$this->_sections['sm']['step'] = 1;
$this->_sections['sm']['start'] = $this->_sections['sm']['step'] > 0 ? 0 : $this->_sections['sm']['loop']-1;
if ($this->_sections['sm']['show']) {
    $this->_sections['sm']['total'] = $this->_sections['sm']['loop'];
    if ($this->_sections['sm']['total'] == 0)
        $this->_sections['sm']['show'] = false;
} else
    $this->_sections['sm']['total'] = 0;
if ($this->_sections['sm']['show']):

            for ($this->_sections['sm']['index'] = $this->_sections['sm']['start'], $this->_sections['sm']['iteration'] = 1;
                 $this->_sections['sm']['iteration'] <= $this->_sections['sm']['total'];
                 $this->_sections['sm']['index'] += $this->_sections['sm']['step'], $this->_sections['sm']['iteration']++):
$this->_sections['sm']['rownum'] = $this->_sections['sm']['iteration'];
$this->_sections['sm']['index_prev'] = $this->_sections['sm']['index'] - $this->_sections['sm']['step'];
$this->_sections['sm']['index_next'] = $this->_sections['sm']['index'] + $this->_sections['sm']['step'];
$this->_sections['sm']['first']      = ($this->_sections['sm']['iteration'] == 1);
$this->_sections['sm']['last']       = ($this->_sections['sm']['iteration'] == $this->_sections['sm']['total']);
?>
		<option value="<?php echo $this->_tpl_vars['shipping_methods'][$this->_sections['sm']['index']]['shippingid']; ?>
"<?php if ($this->_tpl_vars['search_prefilled']['shipping_method'] == $this->_tpl_vars['shipping_methods'][$this->_sections['sm']['index']]['shippingid']): ?> selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['shipping_methods'][$this->_sections['sm']['index']]['shipping'])) ? $this->_run_mod_handler('trademark', true, $_tmp) : smarty_modifier_trademark($_tmp)); ?>
</option>
<?php endfor; endif; ?>
	</select>
	</td>
</tr>

<?php endif; ?>

<tr> 
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_order_status']; ?>
:</td>
	<td width="10">&nbsp;</td>
	<td><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/order_status.tpl", 'smarty_include_vars' => array('status' => $this->_tpl_vars['search_prefilled']['status'],'mode' => 'select','name' => "posted_data[status]",'extended' => 'Y','extra' => "style='width:70%'")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>

<?php if ($this->_tpl_vars['usertype'] != 'C'): ?>
<?php if ($this->_tpl_vars['usertype'] == 'A'): ?>
<tr>
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_provider']; ?>
:</td>
	<td width="10">&nbsp;</td>
	<td>
	<input type="text" name="posted_data[provider]" size="30" value="<?php echo $this->_tpl_vars['search_prefilled']['provider']; ?>
" style="width:70%" />
	</td>
</tr>
<?php endif; ?>

<tr> 
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_order_features']; ?>
:</td>
	<td width="10">&nbsp;</td>
	<td>
<?php $this->assign('features', $this->_tpl_vars['search_prefilled']['features']); ?>
	<select name="posted_data[features][]" multiple="multiple" size="7" style="width:70%">
		<option value="gc_applied"<?php if ($this->_tpl_vars['features']['gc_applied']): ?> selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_entirely_or_partially_payed_by_gc'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
</option>
		<option value="discount_applied"<?php if ($this->_tpl_vars['features']['discount_applied']): ?> selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_global_discount_applied'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
</option>
		<option value="coupon_applied"<?php if ($this->_tpl_vars['features']['coupon_applied']): ?> selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_discount_coupon_applied'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
</option>
		<option value="free_ship"<?php if ($this->_tpl_vars['features']['free_ship']): ?> selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_free_shipping'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
</option>
		<option value="free_tax"<?php if ($this->_tpl_vars['features']['free_tax']): ?> selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_tax_exempt'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
</option>
		<option value="gc_ordered"<?php if ($this->_tpl_vars['features']['gc_ordered']): ?> selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_gc_purchased'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
</option>
		<option value="notes"<?php if ($this->_tpl_vars['features']['notes']): ?> selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_orders_with_notes_assigned'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
</option>
	</select><br />
<?php echo $this->_tpl_vars['lng']['lbl_hold_ctrl_key']; ?>

	</td>
</tr>

<?php endif; ?>

<?php if ($this->_tpl_vars['usertype'] != 'C'): ?>

<tr>
	<td colspan="3"><br /><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_search_by_ordered_products'],'class' => 'grey')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_search_for_pattern']; ?>
:</td>
	<td width="10"><font class="CustomerMessage"></font></td>
	<td>
	<input type="text" name="posted_data[product_substring]" size="30" value="<?php echo $this->_tpl_vars['search_prefilled']['product_substring']; ?>
" style="width:70%" />
	</td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_search_in']; ?>
:</td>
	<td width="10"><font class="CustomerMessage"></font></td>
	<td>

<table cellpadding="0" cellspacing="0">
<tr>
	<td width="5"><input type="checkbox" id="posted_data_by_title" name="posted_data[by_title]"<?php if ($this->_tpl_vars['search_prefilled'] == "" || $this->_tpl_vars['search_prefilled']['by_title']): ?> checked="checked"<?php endif; ?> /></td>
	<td nowrap="nowrap"><label for="posted_data_by_title"><?php echo $this->_tpl_vars['lng']['lbl_product_title']; ?>
</label>&nbsp;&nbsp;</td>

	<td width="5"><input type="checkbox" id="posted_data_by_options" name="posted_data[by_options]"<?php if ($this->_tpl_vars['search_prefilled'] == "" || $this->_tpl_vars['search_prefilled']['by_options']): ?> checked="checked"<?php endif; ?> /></td>
	<td nowrap="nowrap"><label for="posted_data_by_options"><?php echo $this->_tpl_vars['lng']['lbl_options']; ?>
</label></td>
</tr>
</table>

	</td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_sku']; ?>
:</td>
	<td width="10"><font class="CustomerMessage"></font></td>
	<td>
	<input type="text" maxlength="64" name="posted_data[productcode]" value="<?php echo $this->_tpl_vars['search_prefilled']['productcode']; ?>
" style="width:70%" />
	</td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_productid']; ?>
#:</td>
	<td width="10"><font class="CustomerMessage"></font></td>
	<td>
	<input type="text" maxlength="64" name="posted_data[productid]" value="<?php echo $this->_tpl_vars['search_prefilled']['productid']; ?>
" style="width:70%" />
	</td>
</tr>

<tr> 
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_price']; ?>
 (<?php echo $this->_tpl_vars['config']['General']['currency_symbol']; ?>
):</td>
	<td width="10">&nbsp;</td>
	<td>
<table cellpadding="0" cellspacing="0">
<tr>
	<td><input type="text" size="10" maxlength="15" name="posted_data[price_min]" value="<?php if ($this->_tpl_vars['search_prefilled'] == ""):  echo $this->_tpl_vars['zero'];  else:  echo ((is_array($_tmp=$this->_tpl_vars['search_prefilled']['price_min'])) ? $this->_run_mod_handler('formatprice', true, $_tmp) : smarty_modifier_formatprice($_tmp));  endif; ?>" /></td>
	<td>&nbsp;-&nbsp;</td>
	<td><input type="text" size="10" maxlength="15" name="posted_data[price_max]" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['search_prefilled']['price_max'])) ? $this->_run_mod_handler('formatprice', true, $_tmp) : smarty_modifier_formatprice($_tmp)); ?>
" /></td>
</tr>
</table>
	</td>
</tr>

<?php endif; ?>

<?php if ($this->_tpl_vars['usertype'] != 'C'): ?>

<tr>
	<td colspan="3"><br /><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_search_by_customer'],'class' => 'grey')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>

<tr> 
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_customer']; ?>
:</td>
	<td width="10">&nbsp;</td>
	<td><input type="text" name="posted_data[customer]" size="30" value="<?php echo $this->_tpl_vars['search_prefilled']['customer']; ?>
" style="width:70%" /></td>
</tr>

<tr>
	<td class="FormButton"><?php echo $this->_tpl_vars['lng']['lbl_search_in']; ?>
:</td>
	<td width="10">&nbsp;</td>
	<td>
<table cellspacing="0" cellpadding="0">
<tr>
    <td width="5"><input type="checkbox" id="posted_data_by_username" name="posted_data[by_username]"<?php if ($this->_tpl_vars['search_prefilled'] == "" || $this->_tpl_vars['search_prefilled']['by_username']): ?> checked="checked"<?php endif; ?> /></td>
    <td nowrap="nowrap"><label for="posted_data_by_username"><?php echo $this->_tpl_vars['lng']['lbl_username']; ?>
</label>&nbsp;&nbsp;</td>

	<td width="5"><input type="checkbox" id="posted_data_by_firstname" name="posted_data[by_firstname]"<?php if ($this->_tpl_vars['search_prefilled'] == "" || $this->_tpl_vars['search_prefilled']['by_firstname']): ?> checked="checked"<?php endif; ?> /></td>
	<td nowrap="nowrap"><label for="posted_data_by_firstname"><?php echo $this->_tpl_vars['lng']['lbl_first_name']; ?>
</label>&nbsp;&nbsp;</td>
	<!-- 
	<td width="5"><input type="checkbox" id="posted_data_by_lastname" name="posted_data[by_lastname]"<?php if ($this->_tpl_vars['search_prefilled'] == "" || $this->_tpl_vars['search_prefilled']['by_lastname']): ?> checked="checked"<?php endif; ?> /></td>
	<td nowrap="nowrap"><label for="posted_data_by_lastname"><?php echo $this->_tpl_vars['lng']['lbl_last_name']; ?>
</label></td>
	-->
</tr>
</table>
	</td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_search_by_address']; ?>
:</td>
	<td width="10"><font class="CustomerMessage"></font></td>
	<td>
<table cellpadding="0" cellspacing="0">
<tr>
	<td width="5"><input type="radio" id="address_type_null" name="posted_data[address_type]" value=""<?php if ($this->_tpl_vars['search_prefilled'] == "" || $this->_tpl_vars['search_prefilled']['address_type'] == ""): ?> checked="checked"<?php endif; ?> onclick="javascript:managedate('address',true)" /></td>
	<td class="OptionLabel"><label for="address_type_null"><?php echo $this->_tpl_vars['lng']['lbl_ignore_address']; ?>
</label></td>

	<td width="5"><input type="radio" id="address_type_B" name="posted_data[address_type]" value="B"<?php if ($this->_tpl_vars['search_prefilled']['address_type'] == 'B'): ?> checked="checked"<?php endif; ?> onclick="javascript:managedate('address',false)" /></td>
	<td class="OptionLabel"><label for="address_type_B"><?php echo $this->_tpl_vars['lng']['lbl_billing']; ?>
</label></td>

	<td width="5"><input type="radio" id="address_type_S" name="posted_data[address_type]" value="S"<?php if ($this->_tpl_vars['search_prefilled']['address_type'] == 'S'): ?> checked="checked"<?php endif; ?> onclick="javascript:managedate('address',false)" /></td>
	<td class="OptionLabel"><label for="address_type_S"><?php echo $this->_tpl_vars['lng']['lbl_shipping']; ?>
</label></td>

	<td width="5"><input type="radio" id="address_type_both" name="posted_data[address_type]" value="Both"<?php if ($this->_tpl_vars['search_prefilled']['address_type'] == 'Both'): ?> checked="checked"<?php endif; ?> onclick="javascript:managedate('address',false)" /></td>
	<td class="OptionLabel"><label for="address_type_both"><?php echo $this->_tpl_vars['lng']['lbl_both']; ?>
</label></td>
</tr>
</table>
	</td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_city']; ?>
:</td>
	<td width="10"><font class="CustomerMessage"></font></td>
	<td><input type="text" maxlength="64" name="posted_data[city]" value="<?php echo $this->_tpl_vars['search_prefilled']['city']; ?>
" style="width:70%" /></td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_state']; ?>
:</td>
	<td width="10"><font class="CustomerMessage"></font></td>
	<td><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/states.tpl", 'smarty_include_vars' => array('states' => $this->_tpl_vars['states'],'name' => "posted_data[state]",'default' => $this->_tpl_vars['search_prefilled']['state'],'required' => 'N','style' => "style='width:70%'")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_country']; ?>
:</td>
	<td width="10"><font class="CustomerMessage"></font></td>
	<td>
	<select name="posted_data[country]" style="width:70%">
		<option value="">[<?php echo $this->_tpl_vars['lng']['lbl_please_select_one']; ?>
]</option>
<?php unset($this->_sections['country_idx']);
$this->_sections['country_idx']['name'] = 'country_idx';
$this->_sections['country_idx']['loop'] = is_array($_loop=$this->_tpl_vars['countries']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['country_idx']['show'] = true;
$this->_sections['country_idx']['max'] = $this->_sections['country_idx']['loop'];
$this->_sections['country_idx']['step'] = 1;
$this->_sections['country_idx']['start'] = $this->_sections['country_idx']['step'] > 0 ? 0 : $this->_sections['country_idx']['loop']-1;
if ($this->_sections['country_idx']['show']) {
    $this->_sections['country_idx']['total'] = $this->_sections['country_idx']['loop'];
    if ($this->_sections['country_idx']['total'] == 0)
        $this->_sections['country_idx']['show'] = false;
} else
    $this->_sections['country_idx']['total'] = 0;
if ($this->_sections['country_idx']['show']):

            for ($this->_sections['country_idx']['index'] = $this->_sections['country_idx']['start'], $this->_sections['country_idx']['iteration'] = 1;
                 $this->_sections['country_idx']['iteration'] <= $this->_sections['country_idx']['total'];
                 $this->_sections['country_idx']['index'] += $this->_sections['country_idx']['step'], $this->_sections['country_idx']['iteration']++):
$this->_sections['country_idx']['rownum'] = $this->_sections['country_idx']['iteration'];
$this->_sections['country_idx']['index_prev'] = $this->_sections['country_idx']['index'] - $this->_sections['country_idx']['step'];
$this->_sections['country_idx']['index_next'] = $this->_sections['country_idx']['index'] + $this->_sections['country_idx']['step'];
$this->_sections['country_idx']['first']      = ($this->_sections['country_idx']['iteration'] == 1);
$this->_sections['country_idx']['last']       = ($this->_sections['country_idx']['iteration'] == $this->_sections['country_idx']['total']);
?>
	<option value="<?php echo $this->_tpl_vars['countries'][$this->_sections['country_idx']['index']]['country_code']; ?>
"<?php if ($this->_tpl_vars['search_prefilled']['country'] == $this->_tpl_vars['countries'][$this->_sections['country_idx']['index']]['country_code']): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['countries'][$this->_sections['country_idx']['index']]['country']; ?>
</option>
<?php endfor; endif; ?>
	</select>
	</td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_zip_code']; ?>
:</td>
	<td width="10"><font class="CustomerMessage"></font></td>
	<td>
<input type="text" maxlength="32" name="posted_data[zipcode]" value="<?php echo $this->_tpl_vars['search_prefilled']['zipcode']; ?>
" style="width:70%" />
<?php if ($this->_tpl_vars['search_prefilled'] == "" || $this->_tpl_vars['search_prefilled']['address_type'] == ""): ?>
<script type="text/javascript" language="JavaScript 1.2">
<!--
managedate('address',true);
-->
</script>
<?php endif; ?>
	</td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_phone']; ?>
/<?php echo $this->_tpl_vars['lng']['lbl_fax']; ?>
:</td>
	<td width="10"><font class="CustomerMessage"></font></td>
	<td><input type="text" maxlength="32" name="posted_data[phone]" value="<?php echo $this->_tpl_vars['search_prefilled']['phone']; ?>
" style="width:70%" /></td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_email']; ?>
:</td>
	<td width="10">&nbsp;</td>
	<td><input type="text" maxlength="128" name="posted_data[email]" value="<?php echo $this->_tpl_vars['search_prefilled']['email']; ?>
" style="width:70%" /></td>
</tr>

<?php endif; ?>

<tr>
	<td colspan="2">&nbsp;</td>
	<td>
	<br /><br />
	<input type="submit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_search'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: submitForm(this, '');" />
	&nbsp;&nbsp;&nbsp;
	<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_reset'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: reset_form('searchform', searchform_def);" />
	</td>
</tr>

</table>

	</td>
</tr>

</table>
</form>

<?php if ($this->_tpl_vars['search_prefilled']['need_advanced_options']): ?>
<script type="text/javascript" language="JavaScript 1.2">
<!--
visibleBox('1');
-->
</script>
<?php endif; ?>


<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>

<?php if ($this->_tpl_vars['mode'] == 'search'): ?>
<br /><br />
<?php if ($this->_tpl_vars['total_items'] >= '1'): ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_N_results_found'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'items', $this->_tpl_vars['total_items']) : smarty_modifier_substitute($_tmp, 'items', $this->_tpl_vars['total_items'])); ?>
<br />
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_displaying_X_Y_results'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'first_item', $this->_tpl_vars['first_item'], 'last_item', $this->_tpl_vars['last_item']) : smarty_modifier_substitute($_tmp, 'first_item', $this->_tpl_vars['first_item'], 'last_item', $this->_tpl_vars['last_item'])); ?>

<?php else: ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_N_results_found'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'items', 0) : smarty_modifier_substitute($_tmp, 'items', 0)); ?>

<?php endif; ?>
<?php endif; ?>



<?php if ($this->_tpl_vars['usertype'] == 'A' || $this->_tpl_vars['active_modules']['Simple_Mode'] != ""): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_export_delete_orders'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php else: ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_export_orders'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>

<?php endif; ?>
