<?php /* Smarty version 2.6.12, created on 2017-05-19 10:57:54
         compiled from admin/pageconfig/pageconfig-location-chooser.tpl */ ?>
<div><H1><?php if ($this->_tpl_vars['variantPage']): ?>Varient Configurator<?php else: ?>Page Configuration<?php endif; ?> for <span style="text-transform: capitalize;"><?php echo $this->_tpl_vars['locationType']; ?>
 - <?php echo $this->_tpl_vars['pageLocation']; ?>
</span></H1></div>
<?php if (! $this->_tpl_vars['variantPage']):  if ($this->_tpl_vars['showLocation']):  echo '
<script language="javascript">
	$(document).ready(function(){
		var locationsJson = ';  echo $this->_tpl_vars['locationsJson'];  echo ';
				
		$(".page-location-autocomplete").autocomplete({
			source: locationsJson,
			appendTo: "#resultsDiv"
		});
		$(".page-locations-head").bind("click",function(){
			$(".page-locations").toggle();
		});
		$(".location-link").bind("click",function(){
			$(".page-location-autocomplete").val($(this).text());
			$("#SetPageLocationForm").submit();
		});
		$("#resultsDiv ul li.ui-menu-item a.ui-corner-all").live("click", function(){
			$("#SetPageLocationForm").trigger("submit");
		});		
	});
</script>
'; ?>

<div class="section location-choose-head">
	<div class="section-title">
	<?php if ($this->_tpl_vars['pageLocation'] == ""): ?>
	Choose a Page Location
	<?php else: ?>
	Selected Page Location: <?php echo $this->_tpl_vars['pageLocation']; ?>

	<?php endif; ?>
	</div>
	<div style="width:100%;">
		<form id="SetPageLocationForm" method="GET" action="<?php echo $this->_tpl_vars['postBackURL']; ?>
">
			<div class="page-locations-head"> 
				<span class="show-hide"></span>
				<span style="padding-left: 10px">All Page-Locations</span>
				<span style="margin-left: 50px">
					<input class="page-location-autocomplete" type="text" name="pageLocation" value="<?php echo $this->_tpl_vars['pageLocation']; ?>
" style="font-size: 15px"/>
					<input type="submit" value="Set Location" style="font-size: 15px"/>
				</span>	
			</div>
		</form>	
		<div class="page-locations hide">
			<?php $_from = $this->_tpl_vars['locations']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['locations'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['locations']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['i'] => $this->_tpl_vars['location']):
        $this->_foreach['locations']['iteration']++;
?>
				<span style="margin: 10px"><a class="location-link"><?php echo $this->_tpl_vars['location']; ?>
</a></span> 
			<?php endforeach; endif; unset($_from); ?>
		</div>
	</div>
	<div id="resultsDiv" style="width: 300px; font-size: 1.2em;">
	</div>	
</div>
<div class="divider"></div>
<?php endif;  endif; ?>