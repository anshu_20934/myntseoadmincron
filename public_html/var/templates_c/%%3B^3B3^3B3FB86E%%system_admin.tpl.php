<?php /* Smarty version 2.6.12, created on 2017-03-24 12:40:05
         compiled from admin/system_admin.tpl */ ?>
<?php func_load_lang($this, "admin/system_admin.tpl","lbl_summary,lbl_db_backup_restore,lbl_import_export,lbl_membership_levels,lbl_credit_card_types,lbl_titles,lbl_edit_templates,lbl_files,lbl_general_settings,lbl_images_location,lbl_languages,lbl_webmaster_mode,lbl_modules,lbl_payment_methods,lbl_patch_upgrade,lbl_speed_bar"); ?><?php ob_start(); ?>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/general.php" class="VertMenuItems"><?php echo $this->_tpl_vars['lng']['lbl_summary']; ?>
</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/db_backup.php" class="VertMenuItems"><?php echo $this->_tpl_vars['lng']['lbl_db_backup_restore']; ?>
</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/import.php" class="VertMenuItems"><?php echo $this->_tpl_vars['lng']['lbl_import_export']; ?>
</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/memberships.php" class="VertMenuItems"><?php echo $this->_tpl_vars['lng']['lbl_membership_levels']; ?>
</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/card_types.php" class="VertMenuItems"><?php echo $this->_tpl_vars['lng']['lbl_credit_card_types']; ?>
</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/titles.php" class="VertMenuItems"><?php echo $this->_tpl_vars['lng']['lbl_titles']; ?>
</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/file_edit.php" class="VertMenuItems"><?php echo $this->_tpl_vars['lng']['lbl_edit_templates']; ?>
</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/file_manage.php" class="VertMenuItems"><?php echo $this->_tpl_vars['lng']['lbl_files']; ?>
</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/configuration.php" class="VertMenuItems"><?php echo $this->_tpl_vars['lng']['lbl_general_settings']; ?>
</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/images_location.php" class="VertMenuItems"><?php echo $this->_tpl_vars['lng']['lbl_images_location']; ?>
</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/languages.php" class="VertMenuItems"><?php echo $this->_tpl_vars['lng']['lbl_languages']; ?>
</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/editor_mode.php" class="VertMenuItems"><?php echo $this->_tpl_vars['lng']['lbl_webmaster_mode']; ?>
</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/modules.php" class="VertMenuItems"><?php echo $this->_tpl_vars['lng']['lbl_modules']; ?>
</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/payment_methods.php" class="VertMenuItems"><?php echo $this->_tpl_vars['lng']['lbl_payment_methods']; ?>
</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/patch.php" class="VertMenuItems"><?php echo $this->_tpl_vars['lng']['lbl_patch_upgrade']; ?>
</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/speed_bar.php" class="VertMenuItems"><?php echo $this->_tpl_vars['lng']['lbl_speed_bar']; ?>
</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/admin_roles_access.php" class="VertMenuItems">Manage Pages Roles Access</a></li>
<?php $this->_smarty_vars['capture']['menu'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu.tpl", 'smarty_include_vars' => array('dingbats' => "dingbats_categorie.gif",'menu_title' => 'System Admin','menu_content' => $this->_smarty_vars['capture']['menu'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>