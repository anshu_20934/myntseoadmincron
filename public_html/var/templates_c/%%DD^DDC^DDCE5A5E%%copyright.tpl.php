<?php /* Smarty version 2.6.12, created on 2017-03-24 12:21:47
         compiled from copyright.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'copyright.tpl', 2, false),)), $this); ?>
<?php func_load_lang($this, "copyright.tpl","lbl_copyright"); ?><?php echo $this->_tpl_vars['lng']['lbl_copyright']; ?>
 &copy; <?php echo $this->_tpl_vars['config']['Company']['start_year'];  if ($this->_tpl_vars['config']['Company']['start_year'] < $this->_tpl_vars['config']['Company']['end_year']): ?>-<?php echo ((is_array($_tmp=time())) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y") : smarty_modifier_date_format($_tmp, "%Y"));  endif; ?> <?php echo $this->_tpl_vars['config']['Company']['company_name']; ?>
