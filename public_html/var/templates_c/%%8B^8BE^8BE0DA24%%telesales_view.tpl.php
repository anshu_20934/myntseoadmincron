<?php /* Smarty version 2.6.12, created on 2017-05-17 14:36:12
         compiled from admin/telesales_view.tpl */ ?>

<?php echo '
<style type="text/css">

.telesales-overlay {
	position:fixed;
	top:0;
	left:0;
	opacity:0.5;
	background:#000;
	display:none;
	filter:alpha(opacity=50);
	height:100%;
	width:100%;
	z-index:1000;
}

.telesales-form{
	display:none;
    position: fixed;
    top:25%;
    right:35%;
    z-index:1001;
    background: none repeat scroll 0 0 #FFFFFF;
    margin: 5px;
    padding:5px 10px;
    z-index:1001;
}
.telesales-form2{
	display:none;
    position: fixed;
    z-index:1001;
    background: none repeat scroll 0 0 #FFFFFF;
    margin: 5px;
    padding:5px 10px;
    height: 75%;
    left: 20%;
    overflow: auto;
    right: 30%;
    top: 10%;
    width: 50%;
}
 
.tele-loading-overlay{
	width:100%;
	height:100%;
	position:absolute;
	top:0;
	left:0;
	text-align:center;
	z-index:1002;
	background:#ffffff;
	opacity:0.5;
	filter:alpha(opacity=50);
}
.telesales_error{
		color:red;
		font-weight:bold;
	}
.telesales_success{
		color:green;
		font-weight:bold;
	}
	
.bgeven{
	background-color: lightgrey;
}
.discounted-mrp{
	display: inline;color: #666666; font-size: 13px; font-weight: bold; margin-top: -5px;text-decoration: line-through;
}
.final-price{
	display: inline;padding:2px 5px;color: #BD1A00;font-size: 13px;font-weight: bold;
}
</style>

'; ?>



<div>
	<div id="logged" style="border: 2px solid black;float: left;padding: 2px 15px 2px 65px;width: 700px;display: none;">
			<div class="tele-loading-overlay " style="display: none">
						<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/myntra_images/loading-graphic.gif" alt="loading graphic">
			</div>
			
			<div style="float: left; padding-top: 18px; height: 30px; width:600px;"><label id='loginname'></label><a href="javascript:void(0);" onclick="cclogout('S');$('#logged').hide();$('#switchuser').show();" style="padding-left: 15px">Switch User</a>
			<a href="javascript:void(0);" onclick="cclogout('L');$('#logged').hide();$('#unlogged').show();" style="padding-left: 15px">logout</a></div>
			<div style="float: left; text-align: center;">
			<div><a href="javascript:void(0);" onclick="updateCart('','',true);"><img style="width: 48px;height: 48px;"  src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/shopping_cart.png" alt="Cart Image" ></a></div>
			<div><a  href="javascript:void(0);" onclick="updateCart('','',true);" id="itemsincart" style="font-weight: bolder;">Cart(0)</a></div>
			</div>
	</div>
	<div id="unlogged" style="border: 2px solid black;float: left;padding: 15px 15px 15px 65px;width: 700px;">
			<div class="tele-loading-overlay " style="display: none">
						<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/myntra_images/loading-graphic.gif" alt="loading graphic">
			</div>
			<label ><b>No user logged in</b></label>
			<a href="javascript:void(0);" onclick="$('#unlogged').hide();$('#switchuser').show();" style="padding-left: 15px">Login as user</a>
			<a href="javascript:void(0);" onclick="$('.telesales-overlay').show();$('#adduserdiv').show();$('#adduserdiv input.newuserinputtext').val('');$('#customerSearchResults tr:gt(0)').remove();" style="padding-left: 15px">Create New User</a>
	</div>
	<div id='switchuser' style="border: 2px solid black;float: left;padding: 15px 15px 15px 65px;width: 700px;display:none;">
		<div class="tele-loading-overlay " style="display: none">
						<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/myntra_images/loading-graphic.gif" alt="loading graphic">
		</div>
		<form id="switchuserform" action="telesales.php">
			<input type="hidden" name="action" value="login">	
			<label>Email</label>
			<input type="text" name="loginid" value="" onkeydown="<?php echo 'if (event.keyCode == \'13\'){cclogin();return false;	}'; ?>
" style="padding-left: 15px 15px;">
			<input type="button" value="Switch" style="padding-left: 15px 15px; margin-left: 15px;background-color: orange;color: white;" onclick="cclogin();">
			<input type="button" value="Cancel" style="padding-left: 15px 15px; margin-left: 15px;" onclick="$('#switchuser').hide();$('#unlogged').show();">
		</form>
	</div>
	<div style="clear:both;">
	&nbsp;
	</div>
	
	<div id='codavailability' style="border: 2px solid black;float: left;padding: 2px 15px 2px 65px;width: 700px;">
		<div class="tele-loading-overlay " style="display: none">
						<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/myntra_images/loading-graphic.gif" alt="loading graphic">
		</div>
		<form id="codavailabilityform" action="telesales.php" onsubmit="return false;">
		<div style="float: left;padding:10px;">
			<input type="hidden" name="action" value="codcheck">	
			<input type="text" name="pincode" id="pincodevalue" maxlength="10" value="Enter Zipcode"  onkeydown="<?php echo 'if (event.keyCode == \'13\'){codAvailability();return false;	}'; ?>
" onfocus="<?php echo 'if(this.value==\'Enter Zipcode\'){this.value=\'\';}'; ?>
" onblur="<?php echo 'if($.trim(this.value)==\'\'){this.value=\'Enter Zipcode\';}'; ?>
" style="padding-left: 15px;">
			<input type="button" value="COD Available?" style="padding-left: 15px 15px; margin-left: 15px;margin-right: 120px;background-color: orange;color: white;" onclick="codAvailability();">
		</div>
		<div>
				<div id='codavailabilitysuccess'  style="display: none;"><div style="float: left; margin-right: 10px;padding-top:5px;"><img style="width: 32px"
					src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/accept.png"> </div><div class="telesales_success" style="padding-top:12px;color: green;">ZipCode <label class="zipcode"></label> is serviceable by cod</div></div>
				<div id='codavailabilityfailed'  style="display: none"><div style="float: left; margin-right: 10px;padding-top:5px;"><img style="width: 32px"
					src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/delete_te.png"></div><div class="telesales_error" style="padding-top:12px; color: red"> ZipCode <label class="zipcode"></label> is not serviceable by cod</div></div>
		</div>
		</form>
	</div>
	
	<div class="telesales-overlay" onclick="$('#adduserdiv').hide();$('#cartpage').hide();$('.telesales-overlay').hide();$('#shippingaddresspage').hide();"></div>
	<div id='adduserdiv' class="telesales-form" style="border: 2px solid black;float: left;padding: 15px 15px 15px 65px;width: 400px;display:none;">
		
		<div class="tele-loading-overlay " style="display: none">
						<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/myntra_images/loading-graphic.gif" alt="loading graphic">
		</div>
		<div>
		<form id="adduserform" action="telesales.php">
			<input type="hidden" name="action" value="registeruser">
			<table style="width: 100%;">
				<tr>
					<td style="width: 50%;"><label>Name</label></td>
					<td><input class='newuserinputtext' type="text" name="username" value="" id="newusername"></td>
				</tr>
				<tr>
					<td style="width: 50%;"><label>Email Address</label></td>
					<td><input class='newuserinputtext' type="text" name="loginid" value=""  id="newuserloginid"></td>
				</tr>
				<tr>
					<td style="width: 50%;"><label>Phone number</label></td>
					<td><input class='newuserinputtext' type="text" name="phoneno" value=""  id="newuserphoneno"></td>
				</tr>
			</table>
		</form>
		</div>
		<div>
			<input id="createAccountButton" type="button" style=" margin-left: 160px;margin-top: 20px;margin-right:20px;" value="Create Account" onclick="registerNewUser()">
			<input type="button" value="Cancel" onclick="$('#adduserdiv').hide();$('.telesales-overlay').hide();">
		</div>
		<h1 style="margin-top: 20px;">Accounts Found</h1>
		<hr noshade size=7>
		<div style="height: 150px;overflow: auto;">
			<table id="customerSearchResults" style="width: 100%;">				
			<tr><td></td></tr>
			</table>
		</div>

	</div>
	
	<div style="clear:both;">
	&nbsp;
	</div>
	
	<div id="searchpage" style="border: 2px solid black;float: left;padding: 15px 15px 15px 65px;width: 700px;">
		<div class="tele-loading-overlay " style="display: none">
						<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/myntra_images/loading-graphic.gif" alt="loading graphic">
		</div>
		<form action="ajax_product_search.php" id="searchPageForm" method="post">
			<div><input type="text" id="productSearchInput" name="query" style="border-radius: 20px 20px 20px 20px;margin-right: 55px;padding-left: 15px;width: 75%;" ><input type="button" value="Find Products" onclick="findProducts(1);"></div>
			<div id="productSearchPageHeader" style="width: 100%;text-align: center;"></div>
			<div id="productSearchResults" style="width: 100%;">				
			</div>
			<div id="productSearchPageFooter" style="width: 100%;text-align: center;"></div>
		</form>
	</div>
	<div id="cartpage" class="telesales-form2" style="border: 2px solid black;float: left;padding: 15px 15px 15px 15px;display:none;">
		<div class="tele-loading-overlay " style="display: none">
						<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/myntra_images/loading-graphic.gif" alt="loading graphic">
		</div>
	</div>
	<div id="shippingaddresspage" class="telesales-form2" style="border: 2px solid black;float: left;padding: 15px 15px 15px 15px;display:none;">
	
	</div>
</div>
<?php echo '
<script type="text/javascript">
var loggeduser="";
var cartid=\'\';
var emailReg = /^([\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4})?$/;
var numericReg  =  /(^-?\\d\\d*\\.\\d*$)|(^-?\\d\\d*$)|(^-?\\.\\d\\d*$)/;
var selectedAddress="";
var selectedAddressHtml="";
var orderid="";
$(\'#productSearchInput\').keydown(function(event){
	if (event.keyCode == \'13\'){
		findProducts(1);
		return false;
	}
});

$(\'.igonore-enter-press\').keydown(function(event){
	if (event.keyCode == \'13\'){
		return false;
	}
});

function cclogout(switchto){
	 selectedAddress="";
	 selectedAddressHtml="";
	 orderid="";
	 loggeduser="";
	 cartid=\'\';
}
function cclogin(){
	var form=$(\'#switchuserform\');
	$("#switchuser .tele-loading-overlay").css("display",\'block\');
	$.post(form.attr("action"), form.serialize() , function(data){
		$("#switchuser .tele-loading-overlay").hide();
		if(data==null || $.trim(data)==\'\'){
			alert(\'Connection Problem!\');
			return;
		}
		var response=jQuery.parseJSON(data);
		if(response[\'status\']==\'S\'){
			$(\'#logged\').show();
			$(\'#loginname\').html("<b>"+response[\'firstname\']+" "+response[\'lastname\']+"</b>("+response[\'loginid\']+")");
			$(\'#switchuser\').hide();
			$(\'#itemsincart\').html("Cart("+response[\'itemsincart\']+")");
			loggeduser=response[\'loginid\'];
			if(orderid!="" && response[\'itemsincart\']=="0"){
				orderid="";
			}
		}else if(response[\'status\']==\'F\' && response[\'errorcode\']==\'0\'){
			alert(\'Login (\'+response[\'loginid\']+\') doesn\\\'t exists\' );
			return;
		}
			
	});
	
}
function codAvailability(){
	if($(\'#pincodevalue\').val()=="Enter Zipcode" || $.trim($(\'#pincodevalue\').val())==""  ){
		alert("Please enter Zipcode!");
		return false;
	}
	var form=$(\'#codavailabilityform\');
	$("#codavailability .tele-loading-overlay").css("display",\'block\');
	$.post(form.attr("action"), form.serialize() , function(data){
		$("#codavailability .tele-loading-overlay").hide();
		if(data==null || $.trim(data)==\'\'){
			alert(\'Connection Problem!\');
			return false;
		}
		var response=jQuery.parseJSON(data);
		if(response[\'status\']==\'S\' && response[\'available\']==\'true\'){
			$(\'.zipcode\').html(response[\'zipcode\']);
			$(\'#codavailabilitysuccess\').show();
			$(\'#codavailabilityfailed\').hide();
		}else {
			$(\'.zipcode\').html(response[\'zipcode\']);
			$(\'#codavailabilityfailed\').show();
			$(\'#codavailabilitysuccess\').hide();
		}
			
		return false;
	});
	return false;
}
function registerNewUser(){

	if(!validateNewCustomer()){
		return false;
	}
	var form=$(\'#adduserform\');
	$("#adduserdiv .tele-loading-overlay").css("display",\'block\');
	$.post(form.attr("action"), form.serialize() , function(data){
		$("#adduserdiv .tele-loading-overlay").hide();
		if(data==null || $.trim(data)==\'\'){
			alert(\'Connection Problem!\');
			return;
		}
		var response=jQuery.parseJSON(data);
		if(response[\'status\']==\'S\'){
			alert(\'Account created successfully!\');
		}else {
			alert(\'Email id already registered!\');
		}
		$(\'#customerSearchResults tr:gt(0)\').remove();
		rowi=0;
		if(response[\'customers\']!=null){
			for (var key in response[\'customers\']) {
				addCustomerSearchResult(response[\'customers\'][key]);
			}
		}
	
	});
}

function resetPassword(key,email){
	$(".tele-loading-overlay").css("display",\'block\');
	$.post("telesales.php", "action=resetpassword&loginid="+email , function(data){
		$(".tele-loading-overlay").hide();
		if(data==null || $.trim(data)==\'\'){
			alert(\'Connection Problem!\');
			return;
		}
		var response=jQuery.parseJSON(data);
		if(response[\'status\']==\'S\'){
			$("#"+key).html("<label style=\'color: green;\'>Password sent to user\'s email</label>");
		}else {
			alert("Couldn\'t reset the password");
		}
	});
}
var rowi=0;
function addCustomerSearchResult(custinfoarray){
	 $("#customerSearchResults tr:last").after("<tr "+(rowi%2==1?"style=\'background-color: #F0F0F0;\'":"")+" ><td style=\'width:50%\'><div style=\'padding: 15px;\'><label><b>"+custinfoarray[\'firstname\']+" "+
			 custinfoarray[\'lastname\']+"</b><br>"+custinfoarray[\'loginid\']+"<br>"+custinfoarray[\'mobile\']
	 +\'</label></div></td><td style="text-align: center;"><div id="reset\'+rowi+\'"><input type=\\"button\\" value=\\"Reset Password\\" onclick="resetPassword(\\\'reset\'+rowi+\'\\\',\\\'\'+custinfoarray[\'loginid\']+\'\\\')"></div></td></tr>\');
	 rowi++;
}

function validateNewCustomer(){
	var error=false;
	var nameVal=$("#newusername").val();
	var mobileVal=$("#newuserphoneno").val();
	var emailVal=$("#newuserloginid").val();
	var str="";
	if(nameVal == "" || $.trim(nameVal)==""){
		error=true;
		str+="Invalid Name\\n";
	}

	
	if(mobileVal == "" || mobileVal.length != 10 || !numericReg.test(mobileVal) ){
		error=true;
		str+="Invalid Mobile number\\n";
	}
	
	if(emailVal == "" || !emailReg.test(emailVal)){
		error=true;
		str+="Invalid Email id\\n";
	}

	if(error){
		alert("Error! \\n"+str);
	}
    return !error;
}

function findProducts(page){
	if($.trim($("#productSearchInput").val())=="")
		return;
	var form=$(\'#searchPageForm\');
	$("#searchPage .tele-loading-overlay").css("display",\'block\');
	$.post(form.attr("action"), form.serialize()+"&page="+page , function(data){
		$("#searchPage .tele-loading-overlay").hide();
		if(data==null || $.trim(data)==\'\'){
			alert(\'No Products\');
			return;
		}
		var response=jQuery.parseJSON(data);
		var str="";
		if(response[\'current_page\']==0){
		}else{
			str+="<a href=\'javascript:void(0);\' style=\'font-size: 12px;font-weight: bold;margin: 10px;\' onclick=\'findProducts(1);\'>First</a>";
		}
		var pageStart=response[\'current_page\']-4;
		var pageEnd=response[\'current_page\']+4;
		if(pageStart<0)
			pageStart=0;
		if(pageStart>0){
			str+=" ... "
		}
		if(pageEnd>response[\'page_count\'])
			pageEnd=response[\'page_count\'];
		for(var i=pageStart;i < pageEnd;i++){
			if(i==response[\'current_page\'])
				str+="<b style=\'font-size: 15px;font-weight: bold;margin: 10px;\'>"+(i+1)+"</b>";
			else
				str+="<a href=\'javascript:void(0);\' style=\'font-size: 12px;font-weight: bold;margin: 10px;\' onclick=\'findProducts("+(i+1)+");\'>"+(i+1)+"</a>";
		}
		if(pageEnd<response[\'page_count\']){
			str+=" ... "
		}
		if(response[\'current_page\']==(response[\'page_count\']-1)||response[\'page_count\']==0){
		}else{
			str+="<a href=\'javascript:void(0);\' style=\'font-size: 12px;font-weight: bold;margin: 10px;\' onclick=\'findProducts("+(response[\'page_count\'])+");\'>Last</a>";
		}
		$(\'#productSearchPageHeader\').html(str);
		$(\'#productSearchPageFooter\').html(str);
		$(\'#productSearchResults\').html(response[\'html\']);
		$(\'#productSearchResults > table tr:even\').addClass(\'bgeven\');
		
	});
}

function updateSize(selectid,styleid){
	if($("#"+selectid+" > option").length > 1)
		return;
	$.post(\'telesales.php\', "action=getavailabilityselect&styleid="+styleid , function(data){
		if(data==null || $.trim(data)==\'\'){
			return;
		}
		var response=jQuery.parseJSON(data);
		var str="";
		var hasAny=false;
		for (var key in response) {
			str+="<option value=\'"+key+"\'>"+response[key]+"</option>";
			hasAny=true;
		}
		if(!hasAny){
			$("#"+selectid).html("<option>Sold Out</option>"+str);
			$("#"+selectid).attr(\'disabled\',\'true\');
			$("#"+selectid).attr(\'style\',\';color:red;font-size:10:px\');
		}else{
			$("#"+selectid).html("<option>Select Size</option>"+str);
		}
	});
}

function addToCart(styleid){
	if($("#"+styleid+"sizeselect").length==0 || $("#"+styleid+"sizeselect").val()=="Sold Out"){
		alert(\'Product is Sold Out!\');
		return false;
	}
	if($("#"+styleid+"sizeselect").val()==\'Click To Load\' || $("#"+styleid+"sizeselect").val()==\'Select Size\'){
		alert(\'Invalid Size\');
		$("#"+styleid+"sizeselect").focus();
		return false;
	}
	if(!numericReg.test($("#"+styleid+"quantity").val()) || $("#"+styleid+"quantity").val()<1){
		alert(\'Invalid quantity\');
		$("#"+styleid+"quantity").focus();
		return false;
	}
	updateCart(\'addtocart\',"styleid="+styleid+"&skuid="+$("#"+styleid+"sizeselect").val()+"&quantity="+$("#"+styleid+"quantity").val(),true);
}
function removeFromCart(productid){
	updateCart(\'removefromcart\',\'productid=\'+productid,true);
}

function updateCart(subaction,urlparams,showloading){
	if(orderid!="" && subaction!=\'telesalesconverttocod\'){
		$("#cartpage").show();
		$(\'.telesales-overlay\').show();
		return;
	}
	$("#cartpage .tele-loading-overlay").css("display",\'block\');
	$.post(\'telesales.php\', "action=updatecart&loginid="+loggeduser+"&subaction="+subaction+"&orderid="+orderid+"&addressid="+selectedAddress+"&"+urlparams , function(data){
		if(data==null || $.trim(data)==\'\'){
			alert(\'Connection Problem!\');
			return;
		}
		var response=jQuery.parseJSON(data);
		if(response[\'status\']==\'F\' && response[\'errorcode\']==3){
			alert(response[\'errormessage\']);
			//$("#cartpage .tele-loading-overlay").css("display",\'none\');
			//return;
		}
		$("#cartpage").html(response[\'html\']);
		$("#cartpage").show();
		$(\'.telesales-overlay\').show();
		$(\'#cartItemsTable > table tr:even\').addClass(\'bgeven\');
		if(selectedAddress!=""){
			$("#addshippingaddresscartbutton").val(\'Change Shipping Address\');
			$("#selectedCartShippingAddress").html(selectedAddressHtml);
			$("#selectedCartShippingAddress").show();
			$("#selectedCartShippingAddress > input").hide();
		}
		$(\'#itemsincart\').html("Cart("+response[\'itemsincart\']+")");
		$("#cartpage .tele-loading-overlay").css("display",\'none\');
	});
}

function showShippingAddress(){
	$.post(\'telesales.php\', "action=getshippingaddress&loginid="+loggeduser, function(data){
		if(data==null || $.trim(data)==\'\'){
			alert(\'Connection Problem!\');
			return;
		}
		var response=jQuery.parseJSON(data);
		if(response[\'status\']==\'F\' && response[\'errorcode\']==3){
			alert(\'Not logged in!\');
			return;
		}
		$("#shippingaddresspage").html(response[\'html\']);
		$("#shippingaddresspage").show();
		$(\'.telesales-overlay\').show();
	});
}
function selectAddress(addressid){
	selectedAddress=addressid;
	selectedAddressHtml=$("#addressDiv"+addressid).html();
	$("#addshippingaddresscartbutton").val(\'Change Shipping Address\');
	$("#selectedCartShippingAddress").html(selectedAddressHtml);
	$("#selectedCartShippingAddress").show();
	$("#selectedCartShippingAddress > input").hide();
	$("#cartpage").show();
	$(\'#shippingaddresspage\').hide();
	updateCart("","",true);
}
function saveNewAddress(){
	var str="";
	var error=false;
	var state="";
	if($.trim($("#newaddressname").val())==\'\'){
		error=true;
		str+=\'Invalid Recipient Name\\n\';
	}
	if($.trim($("#newaddressaddress").val())==\'\'){
		error=true;
		str+=\'Invalid Address\\n\';
	}
	if($.trim($("#newaddresscity").val())==\'\'){
		error=true;
		str+=\'Invalid City Name\\n\';
	}
	if($.trim($("#newaddresscountry").val())==\'IN\' && $.trim($("#state_select").val())==""){
		error=true;
		str+=\'Invalid State Name\\n\';
	}
	if($.trim($("#newaddresscountry").val())!=\'IN\' && $.trim($("#newaddressstateinput").val())==""){
		error=true;
		str+=\'Invalid State Name\\n\';
	}
	if($("#newaddresscountry").val()==\'IN\'){
		state=$("#state_select").val();
	}else{
		state=$("#newaddressstateinput").val();
	}
	if(!numericReg.test($("#newaddresspincode").val())){
		error=true;
		str+=\'Invalid Pin Code\\n\';
	}
	if($(\'#newaddressmobile\').val() == "" || $(\'#newaddressmobile\').val() .length != 10 || !numericReg.test($(\'#newaddressmobile\').val())){
		error=true;
		str+="Invalid Mobile number\\n";
	}
	
	if($(\'#newaddressemail\').val()  == "" || !emailReg.test($(\'#newaddressemail\').val() )){
		error=true;
		str+="Invalid Email id\\n";
	}
	if(error){
		alert(str);
		return false;
	}

	$.post(\'telesales.php\', "action=addshippingaddress&loginid="+loggeduser+"&name="+$("#newaddressname").val()+"&address="+$("#newaddressaddress").val()+"&city="+$("#newaddresscity").val()+"&country="+$("#newaddresscountry").val()+"&state="+state+"&pincode="+$("#newaddresspincode").val()+"&mobile="+$(\'#newaddressmobile\').val()+"&email="+$(\'#newaddressemail\').val()+"&telephone="+$(\'#newaddresstelephone\').val(), function(data){
		if(data==null || $.trim(data)==\'\'){
			alert(\'Connection Problem!\');
			return;
		}
		var response=jQuery.parseJSON(data);
		if(response[\'status\']==\'F\' && response[\'errorcode\']==3){
			alert(\'Not logged in!\');
			return;
		}
		$("#shippingaddresspage").html(response[\'html\']);
		$("#shippingaddresspage").show();
		$(\'.telesales-overlay\').show();
	});
	
}

function stripHtml(str) {
	if($.trim(str)=="")
		return str;
    return str.replace(/<\\/?[^>]+>/gi, \'\');
}


function placeOrder(pm){
	$("#cartpage .tele-loading-overlay").css("display",\'block\');
	$.post(\'telesales.php\', "action=placeorder&loginid="+loggeduser+"&orderid="+orderid+"&addressid="+selectedAddress+"&pm="+pm , function(data){
		if(data==null || $.trim(data)==\'\'){
			alert(\'Connection Problem!\');
			return;
		}
		var response=jQuery.parseJSON(data);
		if(response[\'status\']==\'F\' ){
			alert(stripHtml(response[\'errormessage\']));
			$("#cartpage .tele-loading-overlay").css("display",\'none\');
			return;
		}

		$("#cartpage").html(response[\'html\']);
		orderid=response[\'orderid\'];

		$("#cartpage").show();
		$(\'.telesales-overlay\').show();
		$(\'#cartItemsTable > table tr:even\').addClass(\'bgeven\');
		if(selectedAddress!=""){
			$("#addshippingaddresscartbutton").val(\'Change Shipping Address\');
			$("#selectedCartShippingAddress").html(selectedAddressHtml);
			$("#selectedCartShippingAddress").show();
			$("#selectedCartShippingAddress > input").hide();
		}
		$("#cartpage .tele-loading-overlay").css("display",\'none\');
		if(pm==\'cod\'){
			//orderid needs to be a string otherwise $.trim() will fail if applied on ann int value	
			convertToCod("\'"+response[\'orderid\']+"\'",\'telesalesdirectcod\');
		}
		
	});

}
function convertToCod(orderid,calltype){
	$("#cartpage .tele-loading-overlay").css("display",\'block\');
	if($.trim(orderid)==\'\'){
		alert(\'Orderid is null!\');
		$("#cartpage .tele-loading-overlay").css("display",\'none\');
		return false;
	}
	$.post("';  echo $this->_tpl_vars['http_location']; ?>
/<?php echo 'pptocod.php", "telesales="+calltype+"&orderid=" + orderid , function(data){
		if(data==null || $.trim(data)==\'\'){
			alert(\'Connection Problem!\');
			$("#cartpage .tele-loading-overlay").css("display",\'none\');
			return;
		}
		alert(stripHtml(data));
		cclogin();
		$("#cartpage .tele-loading-overlay").css("display",\'none\');
	});
}
</script>
'; ?>
