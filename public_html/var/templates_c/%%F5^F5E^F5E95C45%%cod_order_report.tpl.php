<?php /* Smarty version 2.6.12, created on 2017-03-29 23:11:02
         compiled from admin/main/cod_order_report.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "main/popup_product.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['SkinDir']; ?>
/skin1_admin_operations.css" />
<script type="text/javascript" src="<?php echo $this->_tpl_vars['http_location']; ?>
/skin1/js_script/jquery.js"></script>

<?php echo '
<script Language="JavaScript" Type="text/javascript">
function checkAction(){

   if(document.getElementById("action")!==null && document.getElementById("action").value=="queue"){
     if(!confirm("Please confirm if you have verified the address and the phone number of the Customer. \\nCOD Orders can be queued only after verification"))
         return false;
   }
   if(document.getElementById("resendaction")!=null &&document.getElementById("resendaction").value=="resend"){
     if(!confirm("Please confirm if you have verified the address and the phone number of the Customer again. \\n(This order has failed delivery once)"))
         return false;
   }
}
</script>
'; ?>

<?php ob_start(); ?>
<?php ob_start(); ?>

<script type="text/javascript" src="<?php echo $this->_tpl_vars['SkinDir']; ?>
/js_script/calender.js" ></script>
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['SkinDir']; ?>
/css/calender.css" type="text/css" media="screen" />
<form name="codReportForm" action="cod_order_report.php" method="POST">
	<input type="hidden" name="mode" id="mode" value="search"/>
	<table width="100%" cellspacing="5" cellpadding="1">

<tbody>
<tr>
	<td nowrap="nowrap" class="FormButton">Order Date Period:</td>
	<td width="10">&nbsp;</td>
	<td class="FormButton">
	Start Date::<input type="text" name="orderStartDate" id="orderStartDate" value="<?php echo $this->_tpl_vars['post_data']['orderStartDate']; ?>
"/>&nbsp;
	<img border="0" onclick="displayDatePicker('orderStartDate');" alt="Click Here to Pick up the date" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/images/cal.gif">&nbsp;
	End Date::<input type="text" name="orderEndDate"id="orderEndDate" value="<?php echo $this->_tpl_vars['post_data']['orderEndDate']; ?>
"/>&nbsp;
	<img border="0" onclick="displayDatePicker('orderEndDate');" alt="Click Here to Pick up the date" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/images/cal.gif">
	</td> 
</tr>
<tr>
	<td nowrap="nowrap" class="FormButton">Shipping Date Period:</td>
	<td width="10">&nbsp;</td>
	<td class="FormButton">
		Start Date::<input type="text" name="shippingStartDate" id="shippingStartDate" value="<?php echo $this->_tpl_vars['post_data']['shippingStartDate']; ?>
" />&nbsp;
		<img border="0" onclick="displayDatePicker('shippingStartDate');" alt="Click Here to Pick up the date" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/images/cal.gif">&nbsp;
		End Date::<input type="text" name="shippingEndDate" id="shippingEndDate" value="<?php echo $this->_tpl_vars['post_data']['shippingEndDate']; ?>
"/>&nbsp;
		<img border="0" onclick="displayDatePicker('shippingEndDate');" alt="Click Here to Pick up the date" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/images/cal.gif">
	</td>	
</tr>
<tr>
	<td nowrap="nowrap" class="FormButton">Order Value:</td>
	<td width="10">&nbsp;</td>
	<td class="FormButton">Start Value::<input type="text" name="orderStartValue" value="<?php echo $this->_tpl_vars['post_data']['orderStartValue']; ?>
"/>&nbsp;End Value::<input type="text" name="orderEndValue" value="<?php echo $this->_tpl_vars['post_data']['orderEndValue']; ?>
" /></td>	
</tr>
<tr><td class="FormButton"> Pay Status::</td> <td width="10">&nbsp;</td><td> <input type="text" name="payStatus"  value="<?php echo $this->_tpl_vars['post_data']['payStatus']; ?>
"/></td></tr>
<tr><td class="FormButton"> Order Id::</td> <td width="10">&nbsp;</td><td> <input type="text" name="orderId" value="<?php echo $this->_tpl_vars['post_data']['orderId']; ?>
"/></td></tr>
<tr><td class="FormButton"> AWB No::</td> <td width="10">&nbsp;</td><td> <input type="text" name="awbno" value="<?php echo $this->_tpl_vars['post_data']['awbno']; ?>
"/></td></tr>

<!--<tr>
	<td nowrap="nowrap" class="FormButton">Payment Date Period:</td>
	<td width="10">&nbsp;</td>
	<td class="FormButton">
	Start Date::<input type="text" name="paymentStartDate" id="paymentStartDate" value="<?php echo $this->_tpl_vars['post_data']['paymentStartDate']; ?>
"/>&nbsp;
	<img border="0" onclick="displayDatePicker('paymentStartDate');" alt="Click Here to Pick up the date" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/images/cal.gif">&nbsp;
	End Date::<input type="text" name="paymentEndDate" id="paymentEndDate" value="<?php echo $this->_tpl_vars['post_data']['paymentEndDate']; ?>
"/>&nbsp;
	<img border="0" onclick="displayDatePicker('paymentEndDate');" alt="Click Here to Pick up the date" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/images/cal.gif">
	</td>	
</tr> -->
</tbody></table>
<center>
	<input type="submit" value="Search" name="codSearchBtn">
	<input type="submit" value="DownloadAsCsv" name="codSearchBtn">
	
</center>
</form>
<p><b> 1.Please select some criteria to see any report.<br/> 2.Start and end date(for any date criteria) is mandatory for any report.<br/> 3.Order start value should be greater than 0.</b></p>
<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'Cod Data Search','content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php ob_start(); ?>
<br/><br/><br/>
<table width="100%" cellspacing="5" cellpadding="1" border="1px">
	<tbody>
	    <tr> <td width="35%">
			<table cellspacing="5" cellpadding="1" border="1px">
				<p style="font-size:15px;font-weight:bold;text-align:center;">Transactions Info</p> 
				<tr class="tablehead">
					<th nowrap="nowrap">Pay Status</th>
					<th nowrap="nowrap">Total Transactions</th>
					<th nowrap="nowrap">Total Value</th>
				</tr>
				<?php $_from = $this->_tpl_vars['pay_status_total']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['result_row']):
?>
					<tr class="tablehead">
						<?php $_from = $this->_tpl_vars['result_row'][0]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['result_col']):
?>				
							<td width="5%" nowrap="nowrap"><?php echo $this->_tpl_vars['result_col']; ?>
</td>	
						<?php endforeach; endif; unset($_from); ?>
					</tr>
			 <?php endforeach; endif; unset($_from); ?>
			</table>
		 </td>
	 	<td width="15%">
		<table cellspacing="5" cellpadding="1" border="1px" style="text-align:center;">
			<p style="font-size:15px;font-weight:bold;text-align:center;">Avg. Settlement Cycle</p>
			<tr class="tablehead"><th nowrap="nowrap"><?php echo $this->_tpl_vars['avgPaymentCycle']; ?>
 Days</th></tr>
		</table>
		</td> 
		<td width="25%">
		<table width="25%" cellspacing="5" cellpadding="1" border="1px">
		    <p style="font-size:15px;font-weight:bold;text-align:center;">Pending Payments</p> 
			<tr class="tablehead">
				<th nowrap="nowrap">No. of Days</th>
				<th nowrap="nowrap">Total Value</th>
			</tr>
			<?php $_from = $this->_tpl_vars['pending_payment_data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['result_row']):
?>
					<tr class="tablehead">
						<?php $_from = $this->_tpl_vars['result_row'][0]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['result_col']):
?>				
							<td width="5%" nowrap="nowrap"><?php echo $this->_tpl_vars['result_col']; ?>
</td>	
						<?php endforeach; endif; unset($_from); ?>
					</tr>
			 <?php endforeach; endif; unset($_from); ?>
		</table>
		</td>
		<td width="25%">
		<table width="25%" cellspacing="5" cellpadding="1" border="1px" >
			<p style="font-size:15px;font-weight:bold;text-align:center;">Items Delivery Status</p>
				<tr class="tablehead">
				<th nowrap="nowrap">Item Status </th>
				<th nowrap="nowrap">Item Count</th>
			</tr>	
			<?php $_from = $this->_tpl_vars['item_status_data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['result_row']):
?>
					<tr class="tablehead">
						<?php $_from = $this->_tpl_vars['result_row'][0]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['result_col']):
?>				
							<td width="5%" nowrap="nowrap"><?php echo $this->_tpl_vars['result_col']; ?>
</td>	
						<?php endforeach; endif; unset($_from); ?>
					</tr>
			 <?php endforeach; endif; unset($_from); ?>			
		</table>
		</td>
		</tr>
	</tbody>
</table>
<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'Cod Analysis Data ','content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php ob_start(); ?>
<p align="center"><font color="red"><?php echo $this->_tpl_vars['message']; ?>
</font></p>
<table width="100%" cellspacing="1" cellpadding="2">
				<tbody>
					<tr class="tablehead">
					<th style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap">Order Id</th>
					<th style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap">Shipping Date</th>
					<th style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap">Payment Date</th>
					<th style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap">Order Date</th>
					<th style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap">Customer Login</th>
					<th style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap">AWB No</th>
					<th style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap">Value(Rs)</th>
					<th style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap">Pay Status</th>
					<th style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap">More Info</th>
				</tr> 
				<?php if ($this->_tpl_vars['cod_search_data']): ?> 
				    <?php $_from = $this->_tpl_vars['cod_search_data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['result_row']):
?>
					<tr>
						<?php $_from = $this->_tpl_vars['result_row']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['result_col']):
?>				
						<td style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap"><?php echo $this->_tpl_vars['result_col']; ?>
</td>	
						<?php endforeach; endif; unset($_from); ?>
							<td style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap"><a href="order.php?orderid=<?php echo $this->_tpl_vars['result_row']['orderid']; ?>
">View</a></td>
						</tr>
						<tr><td colspan="11"><hr/></td></tr>
					<?php endforeach; endif; unset($_from); ?>
				<?php endif; ?>
				</tbody>
</table>
<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?> <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'Cod Search Result ','content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?> <?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?> <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'Cod Data Report','content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?> <?php echo '
<script type="text/javascript">
$(document).ready(function()
{
	$("#codreportSelectAll").click(function()				
	{
		//alert( " I am here");
		var checked_status = this.checked;
		$("input[name=codreportSelect[]]").each(function()
		{
			//alert("coming here");
			this.checked = checked_status;
		});
	});		
					
});

</script>
'; ?>
