<?php /* Smarty version 2.6.12, created on 2017-03-30 00:36:35
         compiled from admin/payment_keyvaluepairs_common.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'admin/payment_keyvaluepairs_common.tpl', 76, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "main/popup_product.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>



<script type="text/javascript">


<?php echo '
function delete_keyvaluepair(id,keyvalue){
   	var r=confirm("Are you sure you want to delete this key value pair?");
    if(r == true){
    	document.updatekeyvaluepairs.mode.value = \'delete\';
        document.updatekeyvaluepairs.delete_id.value = id;
        document.updatekeyvaluepairs.delete_name.value = keyvalue;
        document.updatekeyvaluepairs.submit();
    }
}

function add_keyvaluepair(){
    if ( document.updatekeyvaluepairs.key_field_new == \'\' || 
    	    document.updatekeyvaluepairs.value_field_new.value == \'\') {
        alert(\'Key or Value cannot be empty\');
        return;
    } 
    document.updatekeyvaluepairs.mode.value = \'add\';
    document.updatekeyvaluepairs.submit();
}

function update_keyvaluepair(id){

	name = document.updatekeyvaluepairs["name_"+id].value;
	value = document.updatekeyvaluepairs["value_"+id].value;
	description =  document.updatekeyvaluepairs["desc_"+id].value;
	
    if ( name == \'\' || value == \'\') {
        alert(\'Key or Value cannot be empty\');
        return;
    } 
    document.updatekeyvaluepairs.mode.value = \'update\';
    document.updatekeyvaluepairs.update_id.value = id;
    document.updatekeyvaluepairs.update_name.value = name;
    document.updatekeyvaluepairs.update_value.value = value;
    document.updatekeyvaluepairs.update_desc.value = description;
    document.updatekeyvaluepairs.submit();
}


</script>
'; ?>


<div id="contentPanel"></div>


<?php ob_start(); ?>
<form action="<?php echo $this->_tpl_vars['payment_action_php']; ?>
" method="post" name="updatekeyvaluepairs">
<input type="hidden" name="mode" />
<input type="hidden" name="delete_id" />
<input type="hidden" name="delete_name" />
<input type="hidden" name="update_id" />
<input type="hidden" name="update_name" />
<input type="hidden" name="update_value" />
<input type="hidden" name="update_desc" />
<table cellpadding="3" cellspacing="1" width="100%">
<tr class="TableHead">
	<td width="20%">Key</td>
	<td width="30%">Value</td>
	<td width="30%">Description</td>
</tr>

<?php if ($this->_tpl_vars['payment_keyvaluepairs']):  unset($this->_sections['key_num']);
$this->_sections['key_num']['name'] = 'key_num';
$this->_sections['key_num']['loop'] = is_array($_loop=$this->_tpl_vars['payment_keyvaluepairs']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['key_num']['show'] = true;
$this->_sections['key_num']['max'] = $this->_sections['key_num']['loop'];
$this->_sections['key_num']['step'] = 1;
$this->_sections['key_num']['start'] = $this->_sections['key_num']['step'] > 0 ? 0 : $this->_sections['key_num']['loop']-1;
if ($this->_sections['key_num']['show']) {
    $this->_sections['key_num']['total'] = $this->_sections['key_num']['loop'];
    if ($this->_sections['key_num']['total'] == 0)
        $this->_sections['key_num']['show'] = false;
} else
    $this->_sections['key_num']['total'] = 0;
if ($this->_sections['key_num']['show']):

            for ($this->_sections['key_num']['index'] = $this->_sections['key_num']['start'], $this->_sections['key_num']['iteration'] = 1;
                 $this->_sections['key_num']['iteration'] <= $this->_sections['key_num']['total'];
                 $this->_sections['key_num']['index'] += $this->_sections['key_num']['step'], $this->_sections['key_num']['iteration']++):
$this->_sections['key_num']['rownum'] = $this->_sections['key_num']['iteration'];
$this->_sections['key_num']['index_prev'] = $this->_sections['key_num']['index'] - $this->_sections['key_num']['step'];
$this->_sections['key_num']['index_next'] = $this->_sections['key_num']['index'] + $this->_sections['key_num']['step'];
$this->_sections['key_num']['first']      = ($this->_sections['key_num']['iteration'] == 1);
$this->_sections['key_num']['last']       = ($this->_sections['key_num']['iteration'] == $this->_sections['key_num']['total']);
?>
<tr>
	
	<td align="center" style="left-padding:30px"><input id="name_<?php echo $this->_tpl_vars['payment_keyvaluepairs'][$this->_sections['key_num']['index']]['id']; ?>
" name="update_key_field[<?php echo $this->_tpl_vars['payment_keyvaluepairs'][$this->_sections['key_num']['index']]['id']; ?>
]" type="text" value="<?php echo $this->_tpl_vars['payment_keyvaluepairs'][$this->_sections['key_num']['index']]['name']; ?>
" ></td>
	<td align="center" style="left-padding:30px"><input id="value_<?php echo $this->_tpl_vars['payment_keyvaluepairs'][$this->_sections['key_num']['index']]['id']; ?>
" name="update_value_field[<?php echo $this->_tpl_vars['payment_keyvaluepairs'][$this->_sections['key_num']['index']]['id']; ?>
]" type="text" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['payment_keyvaluepairs'][$this->_sections['key_num']['index']]['value'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" /></td>
	
	<td align="center" style="left-padding:30px"><input id="desc_<?php echo $this->_tpl_vars['payment_keyvaluepairs'][$this->_sections['key_num']['index']]['id']; ?>
" name="update_description_field[<?php echo $this->_tpl_vars['payment_keyvaluepairs'][$this->_sections['key_num']['index']]['id']; ?>
]" type="text" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['payment_keyvaluepairs'][$this->_sections['key_num']['index']]['description'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" /></td>
	<td align="center">
				<input type="button" value="Delete" onclick="javascript: delete_keyvaluepair('<?php echo $this->_tpl_vars['payment_keyvaluepairs'][$this->_sections['key_num']['index']]['id']; ?>
','<?php echo $this->_tpl_vars['payment_keyvaluepairs'][$this->_sections['key_num']['index']]['name']; ?>
')" />
	</td>
	<td align="center">
				<input type="button" value="Update" onclick="javascript: update_keyvaluepair(<?php echo $this->_tpl_vars['payment_keyvaluepairs'][$this->_sections['key_num']['index']]['id']; ?>
)" />
	</td>	
</tr>
<?php endfor; endif;  else: ?>
<tr>
<td colspan="2" align="center">No Key Value Pairs added as yet</td>
</tr>
<?php endif; ?>

<tr>
	<td align="center" style="left-padding:30px"><input name="key_field_new" type="text" value=""></td>
	<td align="center" style="left-padding:30px"><input name="value_field_new" type="text" value="" /></td>
	<td align="center" style="left-padding:30px"><input name="description_field_new" type="text" value="" /></td>
	<td align="center">
		<input type="button" value="Add" onclick="javascript: add_keyvaluepair()" />
	</td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
</table>
</form>

<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean();  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => "Payment Key-Value Pairs",'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>



