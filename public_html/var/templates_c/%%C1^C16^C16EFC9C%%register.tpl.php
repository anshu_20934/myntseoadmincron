<?php /* Smarty version 2.6.12, created on 2017-03-27 12:58:50
         compiled from provider/main/register.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'provider/main/register.tpl', 147, false),)), $this); ?>
<?php func_load_lang($this, "provider/main/register.tpl","lbl_modify_profile,lbl_create_admin_profile,lbl_create_provider_profile,lbl_modify_admin_profile,lbl_modify_provider_profile,txt_create_admin_profile,txt_modify_admin_profile,txt_create_provider_profile,txt_modify_provider_profile,txt_fields_are_mandatory,lbl_return_to_search_results,txt_registration_error,txt_email_already_exists,txt_user_already_exists,err_billing_state,err_shipping_state,txt_email_invalid,lbl_save,txt_newbie_registration_bottom,lbl_terms_n_conditions,txt_user_registration_bottom,txt_profile_modified,txt_profile_created,lbl_profile_details"); ?><?php if ($this->_tpl_vars['js_enabled'] == 'Y'): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "check_email_script.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "check_zipcode_js.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "generate_required_fields_js.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "check_required_fields_js.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php if ($this->_tpl_vars['config']['General']['use_js_states'] == 'Y'): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "change_states_js.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>
<?php endif; ?>

<?php if ($this->_tpl_vars['newbie'] == 'Y'): ?>
<?php $this->assign('title', $this->_tpl_vars['lng']['lbl_modify_profile']); ?>
<?php else: ?>
<?php if ($this->_tpl_vars['main'] == 'user_add'): ?>
<?php if ($this->_tpl_vars['active_modules']['Simple_Mode']): ?>
<?php $this->assign('title', $this->_tpl_vars['lng']['lbl_create_admin_profile']); ?>
<?php else: ?>
<?php $this->assign('title', $this->_tpl_vars['lng']['lbl_create_provider_profile']); ?>
<?php endif; ?>
<?php else: ?>
<?php if ($this->_tpl_vars['active_modules']['Simple_Mode']): ?>
<?php $this->assign('title', $this->_tpl_vars['lng']['lbl_modify_admin_profile']); ?>
<?php else: ?>
<?php $this->assign('title', $this->_tpl_vars['lng']['lbl_modify_provider_profile']); ?>
<?php endif; ?>
<?php endif; ?>
<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "page_title.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['title'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- IN THIS SECTION -->

<?php if ($this->_tpl_vars['newbie'] != 'Y'): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog_tools.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>

<!-- IN THIS SECTION -->

<font class="Text">

<?php if ($this->_tpl_vars['newbie'] != 'Y'): ?>
<br />
<?php if ($this->_tpl_vars['active_modules']['Simple_Mode']): ?>
<?php if ($this->_tpl_vars['main'] == 'user_add'): ?>
<?php echo $this->_tpl_vars['lng']['txt_create_admin_profile']; ?>

<?php else: ?>
<?php echo $this->_tpl_vars['lng']['txt_modify_admin_profile']; ?>

<?php endif; ?>
<?php else: ?>
<?php if ($this->_tpl_vars['main'] == 'user_add'): ?> 
<?php echo $this->_tpl_vars['lng']['txt_create_provider_profile']; ?>

<?php else: ?> 
<?php echo $this->_tpl_vars['lng']['txt_modify_provider_profile']; ?>

<?php endif; ?> 
<?php endif; ?>
<br /><br />
<?php endif; ?>

<?php echo $this->_tpl_vars['lng']['txt_fields_are_mandatory']; ?>


</font>

<br /><br />

<?php ob_start(); ?>

<?php if ($this->_tpl_vars['newbie'] != 'Y' && $this->_tpl_vars['main'] != 'user_add' && ( $this->_tpl_vars['usertype'] == 'P' && $this->_tpl_vars['active_modules']['Simple_Mode'] == 'Y' || $this->_tpl_vars['usertype'] == 'A' )): ?>
<div align="right"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/button.tpl", 'smarty_include_vars' => array('button_title' => $this->_tpl_vars['lng']['lbl_return_to_search_results'],'href' => "users.php?mode=search")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div>
<?php endif; ?>

<?php $this->assign('reg_error', $this->_tpl_vars['top_message']['reg_error']); ?>
<?php $this->assign('error', $this->_tpl_vars['top_message']['error']); ?>
<?php $this->assign('emailerror', $this->_tpl_vars['top_message']['emailerror']); ?>

<?php if ($this->_tpl_vars['registered'] == ""): ?>
<?php if ($this->_tpl_vars['reg_error']): ?>
<font class="Star">
<?php if ($this->_tpl_vars['reg_error'] == 'F'): ?>
<?php echo $this->_tpl_vars['lng']['txt_registration_error']; ?>

<?php elseif ($this->_tpl_vars['reg_error'] == 'E'): ?>
<?php echo $this->_tpl_vars['lng']['txt_email_already_exists']; ?>

<?php elseif ($this->_tpl_vars['reg_error'] == 'U'): ?>
<?php echo $this->_tpl_vars['lng']['txt_user_already_exists']; ?>

<?php endif; ?>
</font>
<br />
<?php endif; ?>

<?php if ($this->_tpl_vars['error'] != ""): ?>
<font class="Star">
<?php if ($this->_tpl_vars['error'] == 'b_statecode'): ?>
<?php echo $this->_tpl_vars['lng']['err_billing_state']; ?>

<?php elseif ($this->_tpl_vars['error'] == 's_statecode'): ?>
<?php echo $this->_tpl_vars['lng']['err_shipping_state']; ?>

<?php elseif ($this->_tpl_vars['error'] == 'email'): ?>
<?php echo $this->_tpl_vars['lng']['txt_email_invalid']; ?>

<?php else: ?> 
<?php echo $this->_tpl_vars['error']; ?>

<?php endif; ?>
</font>
<br />
<?php endif; ?>

<form action="<?php echo $this->_tpl_vars['register_script_name']; ?>
?<?php echo $GLOBALS['HTTP_SERVER_VARS']['QUERY_STRING']; ?>
" method="post" name="registerform"<?php if ($this->_tpl_vars['js_enabled']): ?> onsubmit="javascript: if (check_zip_code()<?php if ($this->_tpl_vars['default_fields']['email']['required'] == 'Y'): ?> && checkEmailAddress(document.registerform.email)<?php endif; ?> && checkRequired(requiredFields)) return true; else return false;"<?php endif; ?>>
<?php if ($this->_tpl_vars['config']['Security']['use_https_login'] == 'Y'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['XCARTSESSNAME']; ?>
" value="<?php echo $this->_tpl_vars['XCARTSESSID']; ?>
" />
<?php endif; ?>

<table cellspacing="1" cellpadding="2" width="100%">

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/register_personal_info.tpl", 'smarty_include_vars' => array('userinfo' => $this->_tpl_vars['userinfo'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/register_billing_address.tpl", 'smarty_include_vars' => array('userinfo' => $this->_tpl_vars['userinfo'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/register_shipping_address.tpl", 'smarty_include_vars' => array('userinfo' => $this->_tpl_vars['userinfo'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/register_contact_info.tpl", 'smarty_include_vars' => array('userinfo' => $this->_tpl_vars['userinfo'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/register_additional_info.tpl", 'smarty_include_vars' => array('section' => 'A')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/register_account.tpl", 'smarty_include_vars' => array('userinfo' => $this->_tpl_vars['userinfo'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


<?php if ($this->_tpl_vars['active_modules']['News_Management'] && $this->_tpl_vars['newslists']): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "modules/News_Management/register_newslists.tpl", 'smarty_include_vars' => array('userinfo' => $this->_tpl_vars['userinfo'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>

<tr>
	<td colspan="2">&nbsp;</td>
	<td><br />

<br /><br />

<font class="FormButton">
<?php if ($GLOBALS['HTTP_GET_VARS']['mode'] == 'update'): ?>
<input type="hidden" name="mode" value="update" />
<?php endif; ?>
<input type="hidden" name="anonymous" value="<?php echo $this->_tpl_vars['anonymous']; ?>
" />

<input type="submit" value=" <?php echo $this->_tpl_vars['lng']['lbl_save']; ?>
 " />

	</td>
</tr>

</table>
<input type="hidden" name="usertype" value="<?php if ($GLOBALS['HTTP_GET_VARS']['usertype'] != ""):  echo ((is_array($_tmp=$GLOBALS['HTTP_GET_VARS']['usertype'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html'));  else:  echo $this->_tpl_vars['usertype'];  endif; ?>" />
</form>

<br /><br />

<?php if ($this->_tpl_vars['newbie'] == 'Y' && $this->_tpl_vars['active_modules']['Simple_Mode'] != ""): ?>
<?php echo $this->_tpl_vars['lng']['txt_newbie_registration_bottom']; ?>

<br /><a href="help.php?section=conditions"><font class="Text"><b><?php echo $this->_tpl_vars['lng']['lbl_terms_n_conditions']; ?>
</b>&nbsp;</font><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/go.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></a>
<?php else: ?>
<?php echo $this->_tpl_vars['lng']['txt_user_registration_bottom']; ?>

<?php endif; ?>

<br />

<?php else: ?>

<?php if ($GLOBALS['HTTP_POST_VARS']['mode'] == 'update' || $GLOBALS['HTTP_GET_VARS']['mode'] == 'update'): ?>
<?php echo $this->_tpl_vars['lng']['txt_profile_modified']; ?>

<?php else: ?>
<?php echo $this->_tpl_vars['lng']['txt_profile_created']; ?>

<?php endif; ?>

<?php endif; ?>

<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_profile_details'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>