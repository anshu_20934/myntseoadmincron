<?php /* Smarty version 2.6.12, created on 2017-03-24 12:38:35
         compiled from modules/Discount_Coupons/viewcoupongroups.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'modules/Discount_Coupons/viewcoupongroups.tpl', 115, false),array('modifier', 'date_format', 'modules/Discount_Coupons/viewcoupongroups.tpl', 134, false),array('modifier', 'substitute', 'modules/Discount_Coupons/viewcoupongroups.tpl', 217, false),array('modifier', 'escape', 'modules/Discount_Coupons/viewcoupongroups.tpl', 260, false),)), $this); ?>
<?php func_load_lang($this, "modules/Discount_Coupons/viewcoupongroups.tpl","lbl_store_coupons_title,lbl_coupon_used,lbl_coupon_active,lbl_coupon_disabled,txt_no_discount_coupons,lbl_coupon_contains_product,lbl_coupon_contains_products_cat_rec_href,lbl_coupon_contains_products_cat_href,lbl_coupon_contains_products_cat_rec,lbl_coupon_contains_products_cat,lbl_coupon_greater_than,lbl_coupon_apply_once,lbl_coupon_apply_each_item,lbl_coupon_apply_once,lbl_coupon_apply_each_item_cat,lbl_coupon_apply_each_title_cat,lbl_modify_selected,lbl_delete_selected,lbl_store_coupons_title"); ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "page_title.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_store_coupons_title'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<script type="text/javascript" language="JavaScript 1.2">
//<!--

function formValid(mode) 
{
	var elm = document.getElementById("cgroup");
	var hiddObj = document.getElementById("mode");
	hiddObj.value = mode;
	if (elm.value == "")
	{
		if (confirm("show all coupon groups ??")){
		return true;
		}
		else{
		elm.focus();
		return false;
		}
	}
	
	//elm.style.display = (status == true) ? "" : "none";
}
function formValidsingle(mode) 
{
	var elm = document.getElementById("cgroup");
	var hiddObj = document.getElementById("singlemode");
	hiddObj.value = mode;
	if (elm.value == "")
	{
		if (confirm("show all coupons??")){
		return true;
		}
		else{
		elm.focus();
		return false;
		}
	}
	
	//elm.style.display = (status == true) ? "" : "none";
}

function popup(mylink, windowname)
{
	if (! window.focus)return true;
		var href;
	if (typeof(mylink) == \'string\')
			href=mylink;
	else
			href=mylink.href;
	window.open(href, windowname, \'width=800,height=500,scrollbars=yes\');
	return false;
}

//-->
</script>
'; ?>


<br /><br />

<?php ob_start(); ?>

<form action="viewcoupongroups.php" method="post" name="couponsform">
<input type="hidden" name="mode" id="mode"  >
<input type="hidden" name="pageType" id="pageType"  >

<input type="hidden" name="singlemode" id="singlemode"  >

<!-- filter to search for groups -->

<table cellpadding="3" cellspacing="1" width="100%">
     <tr >
			<td width="30%"><strong> Search by pattern  :</strong> </td>
			<td width="40%">
			       <input type="text" name="cgroup" id="cgroup"  size="20" />
 		  	</td>
 		  	<td width="30%"><strong><a href="searchCoupons.php">Advanced Coupons Search</a></strong></td>
	</tr>
 	
 	 <tr >
			<td width="20%">
			   <input  type="submit" name="search" value=" search coupon groups "   onclick="return formValid('search');" />
      	 	</td>
      	 	<td width="20%">
			   <input  type="submit" name="search" value="search coupons"   onclick="return formValidsingle('search');" />
      	 	</td>
      	 	<td width="30%"><strong><a href="coupon_templates.php">Coupon Templates</a></strong></td>
	</tr>
 	
</table>
<br/>
</br>
<!-- end -->
<!-- display group listing -->
<?php if ($this->_tpl_vars['search_method'] == 'group'): ?>
<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="5%">&nbsp;</td>
	<td width="15%">NAME</td>
	<td width="10%" align="center">CHANNEL</td>
	<td width="15" align="center">TEMPLATE</td>
	<td width="10%">STATUS</td>
	<td width="15%" align="center">COUNT</td>
	<td width="15%" align="center">LAST MODIFIED</td>
	<td width="15%" align="center">LAST MODIFIED BY</td>
</tr>


	<?php unset($this->_sections['prod_num']);
$this->_sections['prod_num']['name'] = 'prod_num';
$this->_sections['prod_num']['loop'] = is_array($_loop=$this->_tpl_vars['coupongroups']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['prod_num']['show'] = true;
$this->_sections['prod_num']['max'] = $this->_sections['prod_num']['loop'];
$this->_sections['prod_num']['step'] = 1;
$this->_sections['prod_num']['start'] = $this->_sections['prod_num']['step'] > 0 ? 0 : $this->_sections['prod_num']['loop']-1;
if ($this->_sections['prod_num']['show']) {
    $this->_sections['prod_num']['total'] = $this->_sections['prod_num']['loop'];
    if ($this->_sections['prod_num']['total'] == 0)
        $this->_sections['prod_num']['show'] = false;
} else
    $this->_sections['prod_num']['total'] = 0;
if ($this->_sections['prod_num']['show']):

            for ($this->_sections['prod_num']['index'] = $this->_sections['prod_num']['start'], $this->_sections['prod_num']['iteration'] = 1;
                 $this->_sections['prod_num']['iteration'] <= $this->_sections['prod_num']['total'];
                 $this->_sections['prod_num']['index'] += $this->_sections['prod_num']['step'], $this->_sections['prod_num']['iteration']++):
$this->_sections['prod_num']['rownum'] = $this->_sections['prod_num']['iteration'];
$this->_sections['prod_num']['index_prev'] = $this->_sections['prod_num']['index'] - $this->_sections['prod_num']['step'];
$this->_sections['prod_num']['index_next'] = $this->_sections['prod_num']['index'] + $this->_sections['prod_num']['step'];
$this->_sections['prod_num']['first']      = ($this->_sections['prod_num']['iteration'] == 1);
$this->_sections['prod_num']['last']       = ($this->_sections['prod_num']['iteration'] == $this->_sections['prod_num']['total']);
?>
	
	<tr<?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'",'advance' => false), $this);?>
>
		<td><input type="checkbox" name="posted_data[<?php echo $this->_tpl_vars['coupongroups'][$this->_sections['prod_num']['index']]['groupname']; ?>
][to_delete]" /></td>
		<td><b><a href="<?php echo $this->_tpl_vars['self']; ?>
?searchGroup=<?php echo $this->_tpl_vars['coupongroups'][$this->_sections['prod_num']['index']]['groupname']; ?>
"><?php echo $this->_tpl_vars['coupongroups'][$this->_sections['prod_num']['index']]['groupname']; ?>
</a></b></td>
		<td align="center"><?php echo $this->_tpl_vars['coupongroups'][$this->_sections['prod_num']['index']]['channel']; ?>
</td>
		<td align="center"><?php echo $this->_tpl_vars['coupongroups'][$this->_sections['prod_num']['index']]['templateName']; ?>
</td>
		<td align="center">
			<select name="posted_data[<?php echo $this->_tpl_vars['coupongroups'][$this->_sections['prod_num']['index']]['groupname']; ?>
][isActive]"
				<?php if ($this->_tpl_vars['coupongroups'][$this->_sections['prod_num']['index']]['active'] == 'U'): ?>
					disabled="true">
					<option value="U" selected="selected"><?php echo $this->_tpl_vars['lng']['lbl_coupon_used']; ?>
</option>
				<?php else: ?>
					>
					<option value="A"<?php if ($this->_tpl_vars['coupongroups'][$this->_sections['prod_num']['index']]['active'] == 'A'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_coupon_active']; ?>
</option>
					<option value="D"<?php if ($this->_tpl_vars['coupongroups'][$this->_sections['prod_num']['index']]['active'] == 'D'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_coupon_disabled']; ?>
</option>
				<?php endif; ?>
			</select>
		</td>
		<!--  td align="center"><?php echo $this->_tpl_vars['coupongroups'][$this->_sections['prod_num']['index']]['active_count']; ?>
 / <?php echo $this->_tpl_vars['coupongroups'][$this->_sections['prod_num']['index']]['count']; ?>
</td -->
		<td align="center"><?php echo $this->_tpl_vars['coupongroups'][$this->_sections['prod_num']['index']]['count']; ?>
</td>
		<td align="center"><?php if ($this->_tpl_vars['coupongroups'][$this->_sections['prod_num']['index']]['lastEdited'] != 0):  echo ((is_array($_tmp=$this->_tpl_vars['coupongroups'][$this->_sections['prod_num']['index']]['lastEdited'])) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['config']['Appearance']['datetime_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['config']['Appearance']['datetime_format']));  endif; ?></td>
		<td align="center"><?php echo $this->_tpl_vars['coupongroups'][$this->_sections['prod_num']['index']]['lastEditedBy']; ?>
</td>
	</tr>
<?php endfor; endif; ?>

	<tr>
		<td colspan="7"><br />
		<input type="submit" value="Add New Group" onclick="javascript:document.couponsform.mode.value='add';document.couponsform.submit();"/>
		<input type="submit" value="Modify Group" onclick="javascript:document.couponsform.mode.value='modify';document.couponsform.submit();"/>
		<!--<input type="button" value="Delete Group" onclick="javascript:document.couponsform.mode.value='delete';document.couponsform.submit();" />-->
		<input type="submit" value="Update Status" onclick="javascript:document.couponsform.mode.value='update';document.couponsform.submit();"/>
		<!-- <input type="submit" value="Explore Group"  onclick="javascript:document.couponsform.mode.value='export';document.couponsform.submit();" /> -->
		<input type="submit" value="Add Styles Excl Incl Group" onclick="javascript:document.couponsform.pageType.value='exclIncl';document.couponsform.submit();"/>
		<input type="submit" value="Product Capping Actions" onclick="javascript:document.couponsform.pageType.value='productCapping';document.couponsform.submit();"/>
		</td>
	</tr>



 <?php if ($this->_tpl_vars['coupongroups'] == ''): ?>
	<tr>
		<td colspan="6" align="center"><br /><?php echo $this->_tpl_vars['lng']['txt_no_discount_coupons']; ?>
</td>
	</tr>

<?php endif; ?>

</table>
<?php elseif ($this->_tpl_vars['search_method'] == 'single'): ?>
<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
    <td width="">&nbsp;</td>
	<td width="">COUPON</td>
	<td width="">GROUP</td>
	<td width="">TYPE</td>
	<td width="">DISCOUNT</td>
	<td width="">TIMES</td>
	<td width="">EXPIRES ON</td>
	<td width="">STATUS</td>
	<td width="">LAST MODIFIED</td>
	<td width="">LAST MODIFIED BY</td>
</tr>

<?php unset($this->_sections['prod_num']);
$this->_sections['prod_num']['name'] = 'prod_num';
$this->_sections['prod_num']['loop'] = is_array($_loop=$this->_tpl_vars['coupons']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['prod_num']['show'] = true;
$this->_sections['prod_num']['max'] = $this->_sections['prod_num']['loop'];
$this->_sections['prod_num']['step'] = 1;
$this->_sections['prod_num']['start'] = $this->_sections['prod_num']['step'] > 0 ? 0 : $this->_sections['prod_num']['loop']-1;
if ($this->_sections['prod_num']['show']) {
    $this->_sections['prod_num']['total'] = $this->_sections['prod_num']['loop'];
    if ($this->_sections['prod_num']['total'] == 0)
        $this->_sections['prod_num']['show'] = false;
} else
    $this->_sections['prod_num']['total'] = 0;
if ($this->_sections['prod_num']['show']):

            for ($this->_sections['prod_num']['index'] = $this->_sections['prod_num']['start'], $this->_sections['prod_num']['iteration'] = 1;
                 $this->_sections['prod_num']['iteration'] <= $this->_sections['prod_num']['total'];
                 $this->_sections['prod_num']['index'] += $this->_sections['prod_num']['step'], $this->_sections['prod_num']['iteration']++):
$this->_sections['prod_num']['rownum'] = $this->_sections['prod_num']['iteration'];
$this->_sections['prod_num']['index_prev'] = $this->_sections['prod_num']['index'] - $this->_sections['prod_num']['step'];
$this->_sections['prod_num']['index_next'] = $this->_sections['prod_num']['index'] + $this->_sections['prod_num']['step'];
$this->_sections['prod_num']['first']      = ($this->_sections['prod_num']['iteration'] == 1);
$this->_sections['prod_num']['last']       = ($this->_sections['prod_num']['iteration'] == $this->_sections['prod_num']['total']);
?>
	
	 <?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['times_used'] + $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['times_locked'] >= $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['times']): ?>  
	    <?php $this->assign('color', 'red'); ?> 
	 <?php else: ?>
	 	<?php $this->assign('color', ""); ?> 	   
	 <?php endif; ?>
	 
	 <?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['isInfinite'] == 1): ?>
	 	<?php $this->assign('color', ""); ?>
	 <?php endif; ?>

<tr<?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'",'advance' => false), $this);?>
 >
	<td><input type="checkbox" name="posted_data[<?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['code']; ?>
][to_delete]" /></td>
	<td style="color:<?php echo $this->_tpl_vars['color']; ?>
;"><b><?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['code']; ?>
</b></td>
	<td><?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['groupName']; ?>
</td>
	<td align="center"><?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['couponType']; ?>
</td>
	<td align="center">
		<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['couponType'] == 'absolute'): ?>
	 		Upto <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "currency.tpl", 'smarty_include_vars' => array('value' => $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['mrpAmount'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?> off
	 	<?php elseif ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['couponType'] == 'percentage'): ?>
	 		<?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['mrpPercentage']; ?>
% off
	 	<?php elseif ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['couponType'] == 'dual'): ?>
	 		<?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['mrpPercentage']; ?>
% off upto <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "currency.tpl", 'smarty_include_vars' => array('value' => $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['mrpAmount'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	 	<?php else: ?>
	 		<?php $this->assign('discount', 'Free shipping'); ?>
	 	<?php endif; ?>
	</td>
	<td align="center"><a href="coupon_usage.php?coupon=<?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['code']; ?>
" onClick="return popup(this, 'Coupon Usage')">
		(<font color="green"><?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['times_used']; ?>
</font> + <font color="red"><?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['times_locked']; ?>
</font>)/<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['isInfinite']): ?>-1<?php else:  echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['times'];  endif; ?>
	</a></td>
	<td align="center" nowrap="nowrap"><?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['expire'] != 0):  echo ((is_array($_tmp=$this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['expire'])) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['config']['Appearance']['date_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['config']['Appearance']['date_format']));  endif; ?></td>
	<td align="center"><?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['status'] == 'A'): ?>Active<?php elseif ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['status'] == 'D'): ?>Disabled<?php elseif ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['status'] == 'U'): ?>Used<?php else: ?>Unknown<?php endif; ?></td>
	<td align="center" nowrap="nowrap"><?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['lastEdited'] != 0):  echo ((is_array($_tmp=$this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['lastEdited'])) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['config']['Appearance']['datetime_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['config']['Appearance']['datetime_format']));  endif; ?></td>
	<td align="center"><?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['lastEditedBy']; ?>
</td>
</tr>

<!--<tr<?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this);?>
>
	<td colspan="7">
<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['productid'] != 0): ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_coupon_contains_product'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'productid', $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['productid']) : smarty_modifier_substitute($_tmp, 'productid', $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['productid'])); ?>

<?php elseif ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid'] != 0): ?>
<?php if ($this->_tpl_vars['active_modules']['Simple_Mode']): ?>
<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['recursive'] == 'Y'): ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_coupon_contains_products_cat_rec_href'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'categoryid', $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid'], 'path', $this->_tpl_vars['catalogs']['admin']) : smarty_modifier_substitute($_tmp, 'categoryid', $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid'], 'path', $this->_tpl_vars['catalogs']['admin'])); ?>

<?php else: ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_coupon_contains_products_cat_href'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'categoryid', $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid'], 'path', $this->_tpl_vars['catalogs']['admin']) : smarty_modifier_substitute($_tmp, 'categoryid', $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid'], 'path', $this->_tpl_vars['catalogs']['admin'])); ?>

<?php endif; ?>
<?php else: ?>
<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['recursive'] == 'Y'): ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_coupon_contains_products_cat_rec'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'categoryid', $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid']) : smarty_modifier_substitute($_tmp, 'categoryid', $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid'])); ?>

<?php else: ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_coupon_contains_products_cat'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'categoryid', $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid']) : smarty_modifier_substitute($_tmp, 'categoryid', $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid'])); ?>

<?php endif; ?>
<?php endif; ?>
<?php else: ?>
<?php ob_start();  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "currency.tpl", 'smarty_include_vars' => array('value' => $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['minimum'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
  $this->_smarty_vars['capture']['minamount'] = ob_get_contents(); ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_coupon_greater_than'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'amount', $this->_smarty_vars['capture']['minamount']) : smarty_modifier_substitute($_tmp, 'amount', $this->_smarty_vars['capture']['minamount'])); ?>

<?php endif; ?>
<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['coupon_type'] == 'absolute' && ( $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['productid'] != 0 || $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid'] != 0 )): ?>
<br />
<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['productid'] != 0): ?>
<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['apply_product_once'] == 'Y'): ?>
<?php echo $this->_tpl_vars['lng']['lbl_coupon_apply_once']; ?>

<?php else: ?>
<?php echo $this->_tpl_vars['lng']['lbl_coupon_apply_each_item']; ?>

<?php endif; ?>
<?php elseif ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid'] != 0): ?>
<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['apply_product_once'] == 'Y' && $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['apply_category_once'] == 'Y'): ?>
<?php echo $this->_tpl_vars['lng']['lbl_coupon_apply_once']; ?>

<?php elseif ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['apply_product_once'] == 'N' && $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['apply_category_once'] == 'N'): ?>
<?php echo $this->_tpl_vars['lng']['lbl_coupon_apply_each_item_cat']; ?>

<?php else: ?>
<?php echo $this->_tpl_vars['lng']['lbl_coupon_apply_each_title_cat']; ?>

<?php endif; ?>
<?php endif; ?>
<?php endif; ?>
	</td>
</tr> -->

<?php endfor; endif; ?>
<tr>
		<td colspan="7"><br />
		<input type="submit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_modify_selected'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript:document.couponsform.mode.value='modifysingle';document.couponsform.submit();"/>
		<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_delete_selected'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript:document.couponsform.mode.value='deletesingle';document.couponsform.submit();" />
		<input type="button" value="Export Properties" onclick="javascript:window.location.href='exportfile.php?search=<?php echo $this->_tpl_vars['groupname']; ?>
&mode=properties';"  target="_blank" />
		<input type="button" value="Export Usage" onclick="javascript:window.location.href='exportfile.php?search=<?php echo $this->_tpl_vars['groupname']; ?>
&mode=usage';"  target="_blank" />
		</td>
	</tr>
</table>
<?php endif; ?>

</form>

<table cellpadding="3" cellspacing="1" width="100%">
<tr><td><b>Download bulk generated coupon-list with user tagging:</b></td></tr>
<?php $_from = $this->_tpl_vars['bulkCouponGenerationOutputFiles']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['fileName']):
?>
	<tr><td>
	<a href="/admin/operations/download.php?filename=<?php echo $this->_tpl_vars['fileName']; ?>
&type=bulk_coupon_generation_output" target="_blank"><?php echo $this->_tpl_vars['fileName']; ?>
</a></br>
	</td></tr>
<?php endforeach; endif; unset($_from); ?>
</table>
<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_store_coupons_title'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>