<?php /* Smarty version 2.6.12, created on 2017-04-26 15:57:51
         compiled from modules/Discount_Coupons/viewalltemplates.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'modules/Discount_Coupons/viewalltemplates.tpl', 27, false),array('modifier', 'date_format', 'modules/Discount_Coupons/viewalltemplates.tpl', 47, false),)), $this); ?>
<!-- 
  Page for viewing all coupon templates.
  Author: Rohan.Railkar
 -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "page_title.tpl", 'smarty_include_vars' => array('title' => 'Coupon Templates')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<form action="coupon_templates.php" name="templatesForm" method="post">
<input type="hidden" name="action" id="action" />

<table cellpadding="3" cellspacing="1" width="100%">
	<tr class="TableHead">
		<td width="3%">&nbsp;</td>
		<td width="10%">TEMPLATE</td>
		<td width="10%">VALIDITY</td>
		<td width="10%">TYPE</td>
		<td width="10%">DISCOUNT</td>
		<td width="10%">PER USER / TOTAL USAGE</td>
		<td width="12%">MIN / MAX CART VALUE</td>
		<td width="15%">COMMENTS</td>
		<td width="10%">LAST MODIFIED</td>
		<td width="10%">LAST MODIFIED BY</td>
	</tr>
	
<?php unset($this->_sections['prod_num']);
$this->_sections['prod_num']['name'] = 'prod_num';
$this->_sections['prod_num']['loop'] = is_array($_loop=$this->_tpl_vars['templates']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['prod_num']['show'] = true;
$this->_sections['prod_num']['max'] = $this->_sections['prod_num']['loop'];
$this->_sections['prod_num']['step'] = 1;
$this->_sections['prod_num']['start'] = $this->_sections['prod_num']['step'] > 0 ? 0 : $this->_sections['prod_num']['loop']-1;
if ($this->_sections['prod_num']['show']) {
    $this->_sections['prod_num']['total'] = $this->_sections['prod_num']['loop'];
    if ($this->_sections['prod_num']['total'] == 0)
        $this->_sections['prod_num']['show'] = false;
} else
    $this->_sections['prod_num']['total'] = 0;
if ($this->_sections['prod_num']['show']):

            for ($this->_sections['prod_num']['index'] = $this->_sections['prod_num']['start'], $this->_sections['prod_num']['iteration'] = 1;
                 $this->_sections['prod_num']['iteration'] <= $this->_sections['prod_num']['total'];
                 $this->_sections['prod_num']['index'] += $this->_sections['prod_num']['step'], $this->_sections['prod_num']['iteration']++):
$this->_sections['prod_num']['rownum'] = $this->_sections['prod_num']['iteration'];
$this->_sections['prod_num']['index_prev'] = $this->_sections['prod_num']['index'] - $this->_sections['prod_num']['step'];
$this->_sections['prod_num']['index_next'] = $this->_sections['prod_num']['index'] + $this->_sections['prod_num']['step'];
$this->_sections['prod_num']['first']      = ($this->_sections['prod_num']['iteration'] == 1);
$this->_sections['prod_num']['last']       = ($this->_sections['prod_num']['iteration'] == $this->_sections['prod_num']['total']);
?>

<tr<?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'",'advance' => false), $this);?>
 >
	
	<td><input type="checkbox" name="posted_data[<?php echo $this->_tpl_vars['templates'][$this->_sections['prod_num']['index']]['templateName']; ?>
][selected]" /></td>
	<td style="color:<?php echo $this->_tpl_vars['color']; ?>
;"><b><?php echo $this->_tpl_vars['templates'][$this->_sections['prod_num']['index']]['templateName']; ?>
</b></td>
	<td align="center"><?php echo $this->_tpl_vars['templates'][$this->_sections['prod_num']['index']]['validity']; ?>
</td>
	<td align="center"><?php echo $this->_tpl_vars['templates'][$this->_sections['prod_num']['index']]['couponType']; ?>
</td>
	<td align="center">
		<?php if ($this->_tpl_vars['templates'][$this->_sections['prod_num']['index']]['couponType'] == 'absolute'): ?>
	 		Upto <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "currency.tpl", 'smarty_include_vars' => array('value' => $this->_tpl_vars['templates'][$this->_sections['prod_num']['index']]['MRPAmount'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?> off
	 	<?php elseif ($this->_tpl_vars['templates'][$this->_sections['prod_num']['index']]['couponType'] == 'percentage'): ?>
	 		<?php echo $this->_tpl_vars['templates'][$this->_sections['prod_num']['index']]['MRPpercentage']; ?>
% off
	 	<?php elseif ($this->_tpl_vars['templates'][$this->_sections['prod_num']['index']]['couponType'] == 'dual'): ?>
	 		<?php echo $this->_tpl_vars['templates'][$this->_sections['prod_num']['index']]['MRPpercentage']; ?>
% off upto <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "currency.tpl", 'smarty_include_vars' => array('value' => $this->_tpl_vars['templates'][$this->_sections['prod_num']['index']]['MRPAmount'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	 	<?php else: ?>
	 		<?php $this->assign('discount', 'Free shipping'); ?>
	 	<?php endif; ?>
	</td>
	<td align="center"><?php if ($this->_tpl_vars['templates'][$this->_sections['prod_num']['index']]['isInfinitePerUser']): ?>inf.<?php else:  echo $this->_tpl_vars['templates'][$this->_sections['prod_num']['index']]['maxUsageByUser'];  endif; ?> / <?php if ($this->_tpl_vars['templates'][$this->_sections['prod_num']['index']]['isInfinite']): ?>inf.<?php else:  echo $this->_tpl_vars['templates'][$this->_sections['prod_num']['index']]['times'];  endif; ?></td>
	<td align="center"><?php echo $this->_tpl_vars['templates'][$this->_sections['prod_num']['index']]['minimum']; ?>
 / <?php echo $this->_tpl_vars['templates'][$this->_sections['prod_num']['index']]['maxAmount']; ?>
</td>
	<td align="center"><?php echo $this->_tpl_vars['templates'][$this->_sections['prod_num']['index']]['comments']; ?>
</td>
	<td align="center" nowrap="nowrap"><?php echo ((is_array($_tmp=$this->_tpl_vars['templates'][$this->_sections['prod_num']['index']]['lastEdited'])) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['config']['Appearance']['datetime_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['config']['Appearance']['datetime_format'])); ?>
</td>
	<td align="center"><?php echo $this->_tpl_vars['templates'][$this->_sections['prod_num']['index']]['lastEditedBy']; ?>
</td>
	
</tr>
<?php endfor; endif; ?>

<tr>
	<td colspan="12"><br />
		<input type="submit" value="Delete Template(s)" onclick="javascript:document.templatesForm.action.value='deleteTemplate';document.templatesForm.submit();" />
		&nbsp;
		<input type="submit" value="Modify template" onclick="javascript:document.templatesForm.action.value='modifyTemplate';document.templatesForm.submit();"/>
		&nbsp;
		<input type="submit" value="Add New Template" onClick="javascript:document.templatesForm.action.value='addTemplate';document.templatesForm.submit();" />
	</td>
</tr>
	
</table>
</form>