<?php /* Smarty version 2.6.12, created on 2017-03-27 21:34:30
         compiled from admin/main/upload_mailer.tpl */ ?>
<?php func_load_lang($this, "admin/main/upload_mailer.tpl","txt_featured_products"); ?><script type="text/javascript" src="<?php echo $this->_tpl_vars['http_location']; ?>
/modules/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['http_location']; ?>
/modules/uploadify/swfobject.js"></script>
<link href="<?php echo $this->_tpl_vars['http_location']; ?>
/modules/uploadify/uploadify.css" rel="stylesheet" type="text/css">

<a name="featured" />
<?php echo $this->_tpl_vars['lng']['txt_featured_products']; ?>

<br /><br />

<?php ob_start(); ?>
    <form name='uplMailer' method='POST'></form>

    <div><b>10 files</b> can max be uploaded at a time, and each file size should not exceed <b>200KB</b></div>
    <br/>

    <script type="text/javascript">
    <?php echo '
    $(document).ready(function() {
        $(\'#file_upload\').uploadify({
            \'uploader\'      : \'../modules/uploadify/uploadify.swf\',
            \'script\'        : \'../modules/uploadify/uploadify.php\',//script which uploads files
            \'cancelImg\'     : \'../modules/uploadify/cancel.png\',//cancel button imahe path
            \'folder\'        : \'';  echo $this->_tpl_vars['upload_location'];  echo '\',//to set in uploadify.php and check.php to secure -- to store uploaded files
            \'auto\'          : false,//uploads automatically
            \'multi\'         : true,//multi-upload
            \'expressInstall\': \'../modules/uploadify/expressInstall.swf\',//tells to install flash if not installed
            \'checkScript\'   : null,// \'../modules/uploadify/check.php\' --- ,//checks already uploaded file on name
            \'buttonText\'    : \'Select Images\',
            \'height\'        : 30,//height of button
            \'width\'         : 120,//width of button
            \'queueSizeLimit\': 10,//max number files can be uploaded
            \'removeCompleted\' : false,//removes files from the progress lists once uploaded
            \'sizeLimit\'     : 204800,//each file size can be at max 200KB

            \'onSelectOnce\'  : function(event,data) {
                                $(\'#upload_status\').text(data.fileCount + \' files to upload\').css({color:"#000000",fontWeight:\'bold\'});
                              },

            \'onAllComplete\' : function(event,data) {
                                $(\'#upload_status\').text(data.filesUploaded + \' files uploaded\').css({color:"green",fontWeight:\'bold\'});

                                if(data.errors){
                                    $(\'#upload_error_status\').text(data.errors + \' files are not uploaded\').css({color:"red",fontWeight:\'bold\'});
                                }
                              },

            \'onClearQueue\'  : function(event,data) {
                                $(\'#upload_status\').text(\'\');
                                $(\'#upload_error_status\').text(\'\');
                              },

            \'onCancel\'      : function(event,ID,fileObj,data) {
                                $(\'#upload_status\').text(data.fileCount + \' files to upload\').css({color:"#000000",fontWeight:\'bold\'});
                              }

        });
    });
    '; ?>

    </script>

    <input id="file_upload" name="file_upload" type="file" />
    <br/>
    <div id="upload_status"></div><div id="upload_error_status"></div><div id="s3load" style="display:none"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/myntra_images/loading_240_320.gif" alt="uploading..."></div>
    <br/>
    <a href="javascript:$('#file_upload').uploadifyUpload();">Upload Images</a>  |  <a href="javascript:$('#file_upload').uploadifyClearQueue();">Cancel All Upload</a>  |  <a href="javascript:$('#s3load').show();$('form[name=&quot;uplMailer&quot;]').submit();">Move All Images to S3</a>
<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>

<?php ob_start(); ?>
    <div style="margin-top:10px;">
        <?php echo $this->_tpl_vars['html']; ?>

    </div>
<?php $this->_smarty_vars['capture']['S3list'] = ob_get_contents(); ob_end_clean(); ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'Mailer management','content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<br/>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'Mailer Images in S3','content' => $this->_smarty_vars['capture']['S3list'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>