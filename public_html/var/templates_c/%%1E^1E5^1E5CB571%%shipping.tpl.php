<?php /* Smarty version 2.6.12, created on 2017-12-12 11:40:50
         compiled from admin/main/shipping.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'admin/main/shipping.tpl', 120, false),array('modifier', 'escape', 'admin/main/shipping.tpl', 123, false),array('modifier', 'formatprice', 'admin/main/shipping.tpl', 131, false),array('modifier', 'substitute', 'admin/main/shipping.tpl', 153, false),array('modifier', 'trademark', 'admin/main/shipping.tpl', 179, false),array('modifier', 'default', 'admin/main/shipping.tpl', 182, false),)), $this); ?>
<?php func_load_lang($this, "admin/main/shipping.tpl","lbl_shipping_methods,txt_shipping_methods_top_text,txt_shipping_disabled,lbl_shipping_methods,txt_shipping_methods_top_text,lbl_realtime_shipping_methods,lbl_expand_all,lbl_collapse_all,lbl_new_shipping_methods,lbl_shipping_method,lbl_delivery_time,lbl_destination,lbl_service_code,lbl_weight_limit,lbl_pos,lbl_active,lbl_cod,lbl_national,lbl_international,lbl_delete,lbl_X_from_Y_shipping_methods_enabled,lbl_options,lbl_ups_online_tools_configure,lbl_check_all,lbl_uncheck_all,lbl_shipping_method,lbl_delivery_time,lbl_destination,lbl_weight_limit,lbl_pos,lbl_active,lbl_cod,lbl_national,lbl_international,lbl_defined_shipping_methods,lbl_shipping_method,lbl_delivery_time,lbl_destination,lbl_weight_limit,lbl_pos,lbl_active,lbl_cod,lbl_international,lbl_national,lbl_delete,lbl_add_shipping_method,lbl_international,lbl_national,lbl_update,lbl_enable_all,lbl_disable_all,lbl_shipping_methods"); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "page_title.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_shipping_methods'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($this->_tpl_vars['config']['Shipping']['disable_shipping'] == 'Y'): ?>

<?php echo $this->_tpl_vars['lng']['txt_shipping_methods_top_text']; ?>


<br /><br />

<?php ob_start(); ?>

<br />

<?php echo $this->_tpl_vars['lng']['txt_shipping_disabled']; ?>


<br />

<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_shipping_methods'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php else: ?>

<!-- IN THIS SECTION -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog_tools.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- IN THIS SECTION -->

<br />

<?php echo $this->_tpl_vars['lng']['txt_shipping_methods_top_text']; ?>


<br /><br />

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "change_all_checkboxes.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<script type="text/javascript" language="JavaScript 1.2"><!--
checkboxes_form = 'shippingmethodsform';
--></script>

<?php ob_start(); ?>

<br />

<form action="shipping.php" method="post" name="shippingmethodsform">
<input type="hidden" name="carrier" value="<?php echo $this->_tpl_vars['carrier']; ?>
" />

<script type="text/javascript"><!--
var expands = new Array(<?php $_from = $this->_tpl_vars['carriers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['car']):
?>'<?php echo $this->_tpl_vars['car']['code']; ?>
',<?php endforeach; endif; unset($_from); ?>'');
<?php echo '
function expand_all(flag) {
	var x;
	for (x = 0; x < expands.length; x++) {
		if (expands[x].length == 0)
			continue;
	  	var elm1 = document.getElementById("open"+expands[x]);
		var elm2 = document.getElementById("close"+expands[x]);
		var elm3 = document.getElementById("box"+expands[x]);

		if(!elm3 || !elm1 || !elm2)
			continue;

		if (!flag) {
			elm1.style.display = "none";
			elm2.style.display = "";
			elm3.style.display = "none";
		} else {
			elm1.style.display = "";
			elm2.style.display = "none";
			elm3.style.display = "";
		}
	}
}
'; ?>

--></script>

<table cellpadding="2" cellspacing="1" width="100%">

<?php if ($this->_tpl_vars['carriers'] != ''): ?>
<tr>
	<td colspan="2"><a name="rt"/><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_realtime_shipping_methods'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>

<tr>
	<td colspan="2">
	<div align="right" style="line-height:170%"><a href="javascript: expand_all(true);"><?php echo $this->_tpl_vars['lng']['lbl_expand_all']; ?>
</a> / <a href="javascript: expand_all(false);"><?php echo $this->_tpl_vars['lng']['lbl_collapse_all']; ?>
</a></div>
	</td>
</tr>

<?php if ($this->_tpl_vars['new_shipping'] == 'Y'): ?>
<tr class="TableSubHead">
	<td colspan="2">
<table cellpadding="1" cellspacing="0" width="100%">
<tr>
	<td width="25%"><b><?php echo $this->_tpl_vars['lng']['lbl_new_shipping_methods']; ?>
</b></td>
	<td width="40%"></td>
	<td>&nbsp;</td>
</tr>
</table>
	</td>
</tr>

<tr>
	<td colspan="2">
<table cellpadding="2" cellspacing="1" width="100%">
<tr class="TableHead">
	<td><?php echo $this->_tpl_vars['lng']['lbl_shipping_method']; ?>
</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_delivery_time']; ?>
</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_destination']; ?>
</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_service_code']; ?>
</td>
	<td nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_weight_limit']; ?>
 (<?php echo $this->_tpl_vars['config']['General']['weight_symbol']; ?>
)</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_pos']; ?>
</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_active']; ?>
</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_cod']; ?>
</td>
</tr>

<?php $_from = $this->_tpl_vars['shipping']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['s']):
?>
<?php if ($this->_tpl_vars['s']['is_new'] == 'Y'): ?>

<tr<?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this);?>
>
	<td>
	<input type="hidden" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][is_new]" value="" />
	<input type="text" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][shipping]" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['s']['shipping'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" />
	</td>
	<td align="center"><input type="text" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][shipping_time]" size="8" value="<?php echo $this->_tpl_vars['s']['shipping_time']; ?>
" /></td>
	<td align="center"><select name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][destination]">
	<option value="L"<?php if ($this->_tpl_vars['s']['destination'] == 'L'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_national']; ?>
</option>
	<option value="I"<?php if ($this->_tpl_vars['s']['destination'] == 'I'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_international']; ?>
</option>
	</SElECT></td>
	<td align="center"><input type="text" size="8" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][service_code]" value="<?php echo $this->_tpl_vars['s']['service_code']; ?>
" /></td>
	<td align="center"><input type="text" size="8" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][weight_min]" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['s']['weight_min'])) ? $this->_run_mod_handler('formatprice', true, $_tmp) : smarty_modifier_formatprice($_tmp)); ?>
" /> - <input type="text" size="8" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][weight_limit]" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['s']['weight_limit'])) ? $this->_run_mod_handler('formatprice', true, $_tmp) : smarty_modifier_formatprice($_tmp)); ?>
" /></td>
	<td align="center"><input type="text" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][orderby]" size="4" value="<?php echo $this->_tpl_vars['s']['orderby']; ?>
" /></td>
	<td align="center"><input type="checkbox" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][active]" value="Y"<?php if ($this->_tpl_vars['s']['active'] == 'Y'): ?> checked="checked"<?php endif; ?> /></td>
	<td align="center"><input type="checkbox" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][is_cod]" value="Y"<?php if ($this->_tpl_vars['s']['is_cod'] == 'Y'): ?> checked="checked"<?php endif; ?> /></td>
	<td><input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_delete'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="self.location='shipping.php?mode=delete&shippingid=<?php echo $this->_tpl_vars['s']['shippingid']; ?>
'" /></td>
</tr>
<?php endif; ?>
<?php endforeach; endif; unset($_from); ?>

</table>
<br />
	</td>
</tr>
<?php endif; ?>

<?php $_from = $this->_tpl_vars['carriers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['car']):
?>

<tr class="TableSubHead">
	<td colspan="2">
<table cellpadding="1" cellspacing="0" width="100%">
<tr>
	<td width="25%"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/visiblebox_link.tpl", 'smarty_include_vars' => array('mark' => $this->_tpl_vars['car']['code'],'title' => $this->_tpl_vars['car']['shipping'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
	<td width="40%"><?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_X_from_Y_shipping_methods_enabled'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'enabled', $this->_tpl_vars['car']['total_enabled'], 'methods', $this->_tpl_vars['car']['total_methods']) : smarty_modifier_substitute($_tmp, 'enabled', $this->_tpl_vars['car']['total_enabled'], 'methods', $this->_tpl_vars['car']['total_methods'])); ?>
</td>
	<td><?php if ($this->_tpl_vars['config']['Shipping']['realtime_shipping'] == 'Y' && $this->_tpl_vars['config']['Shipping']['use_intershipper'] != 'Y' && $this->_tpl_vars['active_modules']['UPS_OnLineTools'] == "" && ( $this->_tpl_vars['car']['code'] == 'CPC' || $this->_tpl_vars['car']['code'] == 'FDX' || $this->_tpl_vars['car']['code'] == 'USPS' || $this->_tpl_vars['car']['code'] == 'ARB' || $this->_tpl_vars['car']['code'] == 'APOST' || $this->_tpl_vars['car']['code'] == 'DHL' )): ?><a href="shipping_options.php?carrier=<?php echo $this->_tpl_vars['car']['code']; ?>
"><?php echo $this->_tpl_vars['lng']['lbl_options']; ?>
 &gt;&gt;</a><?php elseif ($this->_tpl_vars['config']['Shipping']['realtime_shipping'] == 'Y' && $this->_tpl_vars['active_modules']['UPS_OnLine_Tools'] && $this->_tpl_vars['config']['Shipping']['use_intershipper'] != 'Y' && $this->_tpl_vars['car']['code'] == 'UPS'): ?><a href="ups.php"><?php echo $this->_tpl_vars['lng']['lbl_ups_online_tools_configure']; ?>
 &gt;&gt;</a><?php endif; ?></td>
</tr>
</table>
	</td>
</tr>
<tr id="box<?php echo $this->_tpl_vars['car']['code']; ?>
" style="display: none;">
	<td colspan="2">

<div align="right" style="line-height:170%"><a href="javascript:change_all(true, false, new Array(<?php $_from = $this->_tpl_vars['shipping']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
 if ($this->_tpl_vars['v']['code'] == $this->_tpl_vars['car']['code']): ?>'data[<?php echo $this->_tpl_vars['v']['shippingid']; ?>
][active]',<?php endif;  endforeach; endif; unset($_from); ?>''));"><?php echo $this->_tpl_vars['lng']['lbl_check_all']; ?>
</a> / <a href="javascript:change_all(false, false, new Array(<?php $_from = $this->_tpl_vars['shipping']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
 if ($this->_tpl_vars['v']['code'] == $this->_tpl_vars['car']['code']): ?>'data[<?php echo $this->_tpl_vars['v']['shippingid']; ?>
][active]',<?php endif;  endforeach; endif; unset($_from); ?>''));"><?php echo $this->_tpl_vars['lng']['lbl_uncheck_all']; ?>
</a></div>

<table cellpadding="2" cellspacing="1" width="100%">
<tr class="TableHead">
	<td><?php echo $this->_tpl_vars['lng']['lbl_shipping_method']; ?>
</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_delivery_time']; ?>
</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_destination']; ?>
</td>
	<td nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_weight_limit']; ?>
 (<?php echo $this->_tpl_vars['config']['General']['weight_symbol']; ?>
)</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_pos']; ?>
</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_active']; ?>
</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_cod']; ?>
</td>
</tr>

<?php $_from = $this->_tpl_vars['shipping']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['s']):
?>
<?php if ($this->_tpl_vars['s']['code'] == $this->_tpl_vars['car']['code'] && $this->_tpl_vars['s']['is_new'] != 'Y'): ?>

<tr<?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this);?>
>
	<td><?php echo ((is_array($_tmp=$this->_tpl_vars['s']['shipping'])) ? $this->_run_mod_handler('trademark', true, $_tmp, $this->_tpl_vars['insert_trademark']) : smarty_modifier_trademark($_tmp, $this->_tpl_vars['insert_trademark'])); ?>
</td>
	<td align="center"><input type="text" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][shipping_time]" size="8" value="<?php echo $this->_tpl_vars['s']['shipping_time']; ?>
" /></td>
	<td align="center"><?php if ($this->_tpl_vars['s']['destination'] == 'L'):  echo $this->_tpl_vars['lng']['lbl_national'];  else:  echo $this->_tpl_vars['lng']['lbl_international'];  endif; ?></td>
	<td align="center" nowrap="nowrap"><input type="text" size="8" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][weight_min]" value="<?php echo ((is_array($_tmp=((is_array($_tmp=@$this->_tpl_vars['s']['weight_min'])) ? $this->_run_mod_handler('default', true, $_tmp, 0) : smarty_modifier_default($_tmp, 0)))) ? $this->_run_mod_handler('formatprice', true, $_tmp) : smarty_modifier_formatprice($_tmp)); ?>
" /> - <input type="text" size="8" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][weight_limit]" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['s']['weight_limit'])) ? $this->_run_mod_handler('formatprice', true, $_tmp) : smarty_modifier_formatprice($_tmp)); ?>
" /></td>
	<td align="center"><input type="text" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][orderby]" size="4" value="<?php echo $this->_tpl_vars['s']['orderby']; ?>
" /></td>
	<td align="center"><input type="checkbox" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][active]" value="Y"<?php if ($this->_tpl_vars['s']['active'] == 'Y'): ?> checked="checked"<?php endif; ?> /></td>
	<td align="center"><input type="checkbox" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][is_cod]" value="Y"<?php if ($this->_tpl_vars['s']['is_cod'] == 'Y'): ?> checked="checked"<?php endif; ?> /></td>
</tr>

<?php endif; ?>
<?php endforeach; endif; unset($_from); ?>

</table>
<br />
	</td>
</tr>

<?php endforeach; endif; unset($_from); ?>

<tr>
	<td colspan="2">
<?php if ($this->_tpl_vars['carrier'] != ''): ?>
<script type="text/javascript"><!--
visibleBox('<?php echo $this->_tpl_vars['carrier']; ?>
');
--></script>
<?php endif; ?>
	</td>
</tr>
<?php endif; ?>
<tr>
	<td colspan="2"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_defined_shipping_methods'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>

<tr>
	<td colspan="2">
<table cellpadding="2" cellspacing="1" width="100%">

<tr class="TableHead">
	<td><?php echo $this->_tpl_vars['lng']['lbl_shipping_method']; ?>
</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_delivery_time']; ?>
</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_destination']; ?>
</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_weight_limit']; ?>
 (<?php echo $this->_tpl_vars['config']['General']['weight_symbol']; ?>
)</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_pos']; ?>
</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_active']; ?>
</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_cod']; ?>
</td>
</tr>

<?php $_from = $this->_tpl_vars['shipping']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['s']):
?>
<?php if ($this->_tpl_vars['s']['code'] == ""): ?>
<tr>
	<td><input type="text" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][shipping]" size="32" value="<?php echo $this->_tpl_vars['s']['shipping']; ?>
" /></td>
	<td align="center"><input type="text" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][shipping_time]" size="8" value="<?php echo $this->_tpl_vars['s']['shipping_time']; ?>
" /></td>
	<td align="center"><select name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][destination]">
		<option value="I" <?php if ($this->_tpl_vars['s']['destination'] == 'I'): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_international']; ?>
</option>
		<option value="L" <?php if ($this->_tpl_vars['s']['destination'] == 'L'): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_national']; ?>
</option>
	</select></td>
	<td align="center"><input type="text" size="8" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][weight_min]" value="<?php echo ((is_array($_tmp=((is_array($_tmp=@$this->_tpl_vars['s']['weight_min'])) ? $this->_run_mod_handler('default', true, $_tmp, 0) : smarty_modifier_default($_tmp, 0)))) ? $this->_run_mod_handler('formatprice', true, $_tmp) : smarty_modifier_formatprice($_tmp)); ?>
" /> - <input type="text" size="8" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][weight_limit]" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['s']['weight_limit'])) ? $this->_run_mod_handler('formatprice', true, $_tmp) : smarty_modifier_formatprice($_tmp)); ?>
" /></td>
	<td align="center"><input type="text" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][orderby]" size="4" value="<?php echo $this->_tpl_vars['s']['orderby']; ?>
" /></td>
	<td nowrap="nowrap" align="center"><input type="checkbox" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][active]" value="Y"<?php if ($this->_tpl_vars['s']['active'] == 'Y'): ?> checked="checked"<?php endif; ?> /></td>
	<td nowrap="nowrap" align="center"><input type="checkbox" name="data[<?php echo $this->_tpl_vars['s']['shippingid']; ?>
][is_cod]" value="Y"<?php if ($this->_tpl_vars['s']['is_cod'] == 'Y'): ?> checked="checked"<?php endif; ?> /></td>
	<td><input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_delete'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="self.location='shipping.php?mode=delete&shippingid=<?php echo $this->_tpl_vars['s']['shippingid']; ?>
'" /></td>
</tr>
<?php endif; ?>
<?php endforeach; endif; unset($_from); ?>

<tr>
	<td colspan="6"><br /><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_add_shipping_method'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>

<tr>
	<td><input type="text" name="add[shipping]" size="32" /></td>
	<td align="center"><input type="text" name="add[shipping_time]" size="10" /></td>
	<td align="center"><select name="add[destination]">
		<option value="I"><?php echo $this->_tpl_vars['lng']['lbl_international']; ?>
</option>
		<option value="L"><?php echo $this->_tpl_vars['lng']['lbl_national']; ?>
</option>
	</select></td>
	<td align="center" nowrap="nowrap"><input type="text" name="add[weight_min]" size="8" value="<?php echo ((is_array($_tmp=0)) ? $this->_run_mod_handler('formatprice', true, $_tmp) : smarty_modifier_formatprice($_tmp)); ?>
" /> - <input type="text" name="add[weight_limit]" size="8" value="<?php echo ((is_array($_tmp=0)) ? $this->_run_mod_handler('formatprice', true, $_tmp) : smarty_modifier_formatprice($_tmp)); ?>
" /></td>
	<td align="center"><input type="text" name="add[orderby]" size="4" /></td>
	<td align="center"><input type="checkbox" name="add[active]" value="Y" checked="checked" /></td>
	<td align="center"><input type="checkbox" name="add[is_cod]" value="Y" /></td>
</tr>

</table>
	</td>
</tr>

<tr>
	<td colspan="6" class="SubmitBox"><input type="submit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_update'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" /></td>
</tr>

<?php if ($this->_tpl_vars['shipping']): ?>
<tr>
	<td colspan="6" align="right">
	<input type="button" onclick="javascript: self.location='shipping.php?mode=enable_all&amp;carrier=<?php echo $this->_tpl_vars['carrier']; ?>
'" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_enable_all'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" />
	&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="button" onclick="javascript: self.location='shipping.php?mode=disable_all&amp;carrier=<?php echo $this->_tpl_vars['carrier']; ?>
'" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_disable_all'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" />
	</td>
</tr>
<?php endif; ?>

</table>
</form>

<br /><br />

<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_shipping_methods'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php endif; ?>