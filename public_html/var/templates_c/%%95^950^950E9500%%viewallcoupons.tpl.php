<?php /* Smarty version 2.6.12, created on 2017-03-24 12:30:00
         compiled from modules/Discount_Coupons/viewallcoupons.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'modules/Discount_Coupons/viewallcoupons.tpl', 79, false),array('modifier', 'date_format', 'modules/Discount_Coupons/viewallcoupons.tpl', 100, false),array('modifier', 'substitute', 'modules/Discount_Coupons/viewallcoupons.tpl', 109, false),array('modifier', 'escape', 'modules/Discount_Coupons/viewallcoupons.tpl', 167, false),array('modifier', 'strip_tags', 'modules/Discount_Coupons/viewallcoupons.tpl', 167, false),)), $this); ?>
<?php func_load_lang($this, "modules/Discount_Coupons/viewallcoupons.tpl","lbl_coupon_contains_product,lbl_coupon_contains_products_cat_rec_href,lbl_coupon_contains_products_cat_href,lbl_coupon_contains_products_cat_rec,lbl_coupon_contains_products_cat,lbl_coupon_greater_than,lbl_coupon_apply_once,lbl_coupon_apply_each_item,lbl_coupon_apply_once,lbl_coupon_apply_each_item_cat,lbl_coupon_apply_each_title_cat,lbl_delete_selected,txt_delete_products_warning,lbl_update,lbl_modify_selected,lbl_export,lbl_export_all_found,lbl_preview_product,lbl_clone_product,lbl_generate_html_links,lbl_search_results"); ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "page_title.tpl", 'smarty_include_vars' => array('title' => 'Coupon Management')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog_tools.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php echo '
<SCRIPT TYPE="text/javascript">
<!--
	function popup(mylink, windowname)
	{
		if (! window.focus)return true;
			var href;
		if (typeof(mylink) == \'string\')
   			href=mylink;
		else
   			href=mylink.href;
		window.open(href, windowname, \'width=800,height=500,scrollbars=yes\');
		return false;
	}
//-->
</SCRIPT>
'; ?>


<form action="viewcoupongroups.php" method="post" name="exportCouponsForm">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="exportedgroup" id="exportedgroup" value="<?php echo $this->_tpl_vars['groupname']; ?>
" />

<a name="results"/>



<?php if ($this->_tpl_vars['mode'] == 'export'): ?>

<?php ob_start(); ?>

<div align="right"><?php if ($this->_tpl_vars['previous']):  echo $this->_tpl_vars['previous'];  endif; ?>|<?php if ($this->_tpl_vars['next']):  echo $this->_tpl_vars['next'];  endif; ?>(Total Pages:<?php echo $this->_tpl_vars['totalpages']; ?>
)</div>

<!--<form action="import.php" method="post" name="processproductform">
<input type="hidden" name="mode" value="update" />
<input type="hidden" name="navpage" value="<?php echo $this->_tpl_vars['navpage']; ?>
" /> -->

<table cellpadding="0" cellspacing="0" width="100%">

<tr>
	<td>

<!-- show all the available  coupom here  -->
<?php if ($this->_tpl_vars['coupons'] != ""): ?>

<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">

	<td width="3%">&nbsp;</td>
	<td width="10%">COUPON</td>
	<td width="8%">TYPE</td>
	<td width="10%">DISCOUNT</td>
	<td width="10%">TIMES</td>
	<td width="10%">SUBTOTAL</td>
	<td width="12%">DISCOUNT OFFERED</td>
	<td width="10%">EXPIRES ON</td>
	<td width="7%">STATUS</td>
	<td width="10%">LAST MODIFIED</td>
	<td width="10%">LAST MODIFIED BY</td>
</tr>

<?php unset($this->_sections['prod_num']);
$this->_sections['prod_num']['name'] = 'prod_num';
$this->_sections['prod_num']['loop'] = is_array($_loop=$this->_tpl_vars['coupons']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['prod_num']['show'] = true;
$this->_sections['prod_num']['max'] = $this->_sections['prod_num']['loop'];
$this->_sections['prod_num']['step'] = 1;
$this->_sections['prod_num']['start'] = $this->_sections['prod_num']['step'] > 0 ? 0 : $this->_sections['prod_num']['loop']-1;
if ($this->_sections['prod_num']['show']) {
    $this->_sections['prod_num']['total'] = $this->_sections['prod_num']['loop'];
    if ($this->_sections['prod_num']['total'] == 0)
        $this->_sections['prod_num']['show'] = false;
} else
    $this->_sections['prod_num']['total'] = 0;
if ($this->_sections['prod_num']['show']):

            for ($this->_sections['prod_num']['index'] = $this->_sections['prod_num']['start'], $this->_sections['prod_num']['iteration'] = 1;
                 $this->_sections['prod_num']['iteration'] <= $this->_sections['prod_num']['total'];
                 $this->_sections['prod_num']['index'] += $this->_sections['prod_num']['step'], $this->_sections['prod_num']['iteration']++):
$this->_sections['prod_num']['rownum'] = $this->_sections['prod_num']['iteration'];
$this->_sections['prod_num']['index_prev'] = $this->_sections['prod_num']['index'] - $this->_sections['prod_num']['step'];
$this->_sections['prod_num']['index_next'] = $this->_sections['prod_num']['index'] + $this->_sections['prod_num']['step'];
$this->_sections['prod_num']['first']      = ($this->_sections['prod_num']['iteration'] == 1);
$this->_sections['prod_num']['last']       = ($this->_sections['prod_num']['iteration'] == $this->_sections['prod_num']['total']);
?>
	
	<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['times_used'] + $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['times_locked'] >= $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['times']): ?>  
		<?php $this->assign('color', 'red'); ?> 
	<?php else: ?>
		<?php $this->assign('color', ""); ?> 	   
	<?php endif; ?>
	
	<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['isInfinite'] == 1): ?>
		<?php $this->assign('color', ""); ?>
	<?php endif; ?>

<tr<?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'",'advance' => false), $this);?>
 >
	
	<td><input type="checkbox" name="posted_data[<?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['code']; ?>
][to_delete]" /></td>
	<td style="color:<?php echo $this->_tpl_vars['color']; ?>
;"><b><?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['code']; ?>
</b></td>
	<td align="center"><?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['couponType']; ?>
</td>
	<td align="center">
		<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['couponType'] == 'absolute'): ?>
	 		Upto <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "currency.tpl", 'smarty_include_vars' => array('value' => $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['mrpAmount'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?> off
	 	<?php elseif ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['couponType'] == 'percentage'): ?>
	 		<?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['mrpPercentage']; ?>
% off
	 	<?php elseif ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['couponType'] == 'dual'): ?>
	 		<?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['mrpPercentage']; ?>
% off upto <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "currency.tpl", 'smarty_include_vars' => array('value' => $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['mrpAmount'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	 	<?php else: ?>
	 		<?php $this->assign('discount', 'Free shipping'); ?>
	 	<?php endif; ?>
	</td>
	<td align="center"><a href="coupon_usage.php?coupon=<?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['coupon']; ?>
" onClick="return popup(this, 'Coupon Usage')">
		(<font color="green"><?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['times_used']; ?>
</font> + <font color="red"><?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['times_locked']; ?>
</font>)/<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['isInfinite'] == 1): ?>-1<?php else:  echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['times'];  endif; ?>
	</a></td>
	<td align="center"><?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['subtotal']; ?>
</td>
	<td align="center"><?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['discountOffered']; ?>
</td>
	<td align="center" nowrap="nowrap"><?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['expire'] != 0):  echo ((is_array($_tmp=$this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['expire'])) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['config']['Appearance']['date_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['config']['Appearance']['date_format']));  endif; ?></td>
	<td align="center"><?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['status'] == 'A'): ?>Active<?php elseif ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['status'] == 'D'): ?>Disabled<?php elseif ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['status'] == 'U'): ?>Used<?php else: ?>Unknown<?php endif; ?></td>
	<td align="center" nowrap="nowrap"><?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['lastEdited'] != 0):  echo ((is_array($_tmp=$this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['lastEdited'])) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['config']['Appearance']['datetime_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['config']['Appearance']['datetime_format']));  endif; ?></td>
	<td align="center"><?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['lastEditedBy']; ?>
</td>
</tr>

<!--<tr<?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this);?>
>
	<td colspan="7">
<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['productid'] != 0): ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_coupon_contains_product'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'productid', $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['productid']) : smarty_modifier_substitute($_tmp, 'productid', $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['productid'])); ?>

<?php elseif ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid'] != 0): ?>
<?php if ($this->_tpl_vars['active_modules']['Simple_Mode']): ?>
<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['recursive'] == 'Y'): ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_coupon_contains_products_cat_rec_href'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'categoryid', $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid'], 'path', $this->_tpl_vars['catalogs']['admin']) : smarty_modifier_substitute($_tmp, 'categoryid', $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid'], 'path', $this->_tpl_vars['catalogs']['admin'])); ?>

<?php else: ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_coupon_contains_products_cat_href'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'categoryid', $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid'], 'path', $this->_tpl_vars['catalogs']['admin']) : smarty_modifier_substitute($_tmp, 'categoryid', $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid'], 'path', $this->_tpl_vars['catalogs']['admin'])); ?>

<?php endif; ?>
<?php else: ?>
<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['recursive'] == 'Y'): ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_coupon_contains_products_cat_rec'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'categoryid', $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid']) : smarty_modifier_substitute($_tmp, 'categoryid', $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid'])); ?>

<?php else: ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_coupon_contains_products_cat'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'categoryid', $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid']) : smarty_modifier_substitute($_tmp, 'categoryid', $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid'])); ?>

<?php endif; ?>
<?php endif; ?>
<?php else: ?>
<?php ob_start();  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "currency.tpl", 'smarty_include_vars' => array('value' => $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['minimum'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
  $this->_smarty_vars['capture']['minamount'] = ob_get_contents(); ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_coupon_greater_than'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'amount', $this->_smarty_vars['capture']['minamount']) : smarty_modifier_substitute($_tmp, 'amount', $this->_smarty_vars['capture']['minamount'])); ?>

<?php endif; ?>
<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['coupon_type'] == 'absolute' && ( $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['productid'] != 0 || $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid'] != 0 )): ?>
<br />
<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['productid'] != 0): ?>
<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['apply_product_once'] == 'Y'): ?>
<?php echo $this->_tpl_vars['lng']['lbl_coupon_apply_once']; ?>

<?php else: ?>
<?php echo $this->_tpl_vars['lng']['lbl_coupon_apply_each_item']; ?>

<?php endif; ?>
<?php elseif ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['categoryid'] != 0): ?>
<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['apply_product_once'] == 'Y' && $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['apply_category_once'] == 'Y'): ?>
<?php echo $this->_tpl_vars['lng']['lbl_coupon_apply_once']; ?>

<?php elseif ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['apply_product_once'] == 'N' && $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['apply_category_once'] == 'N'): ?>
<?php echo $this->_tpl_vars['lng']['lbl_coupon_apply_each_item_cat']; ?>

<?php else: ?>
<?php echo $this->_tpl_vars['lng']['lbl_coupon_apply_each_title_cat']; ?>

<?php endif; ?>
<?php endif; ?>
<?php endif; ?>
	</td>
</tr> -->

<?php endfor; endif; ?>

</table>
</td>
</tr>
<?php endif; ?>


<tr>
<td>

<!-- end -->
<br />



<br />

<!--<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_delete_selected'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: if (checkMarks(this.form, new RegExp('productids\[[0-9]+\]', 'gi'))) if (confirm('<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_delete_products_warning'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
')) submitForm(document.processproductform, 'delete');" /> 
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="submit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_update'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" />
&nbsp;&nbsp;&nbsp;&nbsp; 
<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_modify_selected'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: if (checkMarks(this.form, new RegExp('productids\[[0-9]+\]', 'gi'))) { document.processproductform.action='product_modify.php'; submitForm(document.processproductform, 'list'); }" />-->

<br /><br />
<!--<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_export'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: submitForm(document.processproductform, 'export');" /> -->
<input type="submit" value="Add New Coupon(s)" onclick="javascript:document.exportCouponsForm.mode.value='add';document.searchCouponsForm.submit();"/>
<input type="submit" value="Modify Coupon" onclick="javascript:document.exportCouponsForm.mode.value='modifysingle';document.searchCouponsForm.submit();"/>
<input type="submit" value="Delete Coupon(s)" onclick="javascript:document.exportCouponsForm.mode.value='deletesingle';document.searchCouponsForm.submit();" />
<input type="button" value="Export Properties" onclick="javascript:window.location.href='exportfile.php?group=<?php echo $this->_tpl_vars['groupname']; ?>
&mode=properties';"  target="_blank" />
<input type="button" value="Export Usage" onclick="javascript:window.location.href='exportfile.php?group=<?php echo $this->_tpl_vars['groupname']; ?>
&mode=usage';"  target="_blank" /> 
&nbsp;&nbsp;&nbsp;&nbsp;
<!--<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_export_all_found'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: self.location='search.php?mode=search&amp;export=export_found';" /> -->

<br /><br /><br />


<br /><br />

<!--<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_preview_product'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: if (checkMarks(this.form, new RegExp('productids\[[0-9]+\]', 'gi'))) submitForm(document.processproductform, 'details');" />
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_clone_product'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: if (checkMarks(this.form, new RegExp('productids\[[0-9]+\]', 'gi'))) submitForm(document.processproductform, 'clone');" />
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_generate_html_links'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: if (checkMarks(this.form, new RegExp('productids\[[0-9]+\]', 'gi'))) submitForm(document.processproductform, 'links');" /> -->

	</td>
</tr>

</table>
<!--</form> -->

<br />

<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_search_results'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php endif; ?>

<br /><br />

</form>
