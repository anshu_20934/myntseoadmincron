<?php /* Smarty version 2.6.12, created on 2017-04-03 11:04:56
         compiled from admin/main/admin_product_type.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'admin/main/admin_product_type.tpl', 53, false),array('modifier', 'escape', 'admin/main/admin_product_type.tpl', 87, false),)), $this); ?>
<?php func_load_lang($this, "admin/main/admin_product_type.tpl","txt_featured_products,lbl_product_type,lbl_product_styles,lbl_product_num,lbl_active,lbl_pos,lbl_yes,lbl_no,txt_no_product_types,lbl_add_product,lbl_delete_selected,lbl_add,lbl_modify_selected,lbl_upd_selected,lbl_producttype_heading"); ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "main/popup_product.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
  <script language="Javascript">
      function confirmDelete()
      {
          var flag = confirm(\'Are you sure you want to delete?\')
          if(!flag)
          {
             return false;
          }
          else
          {
             document.producttypeform.mode.value = \'deltype\';
             document.producttypeform.submit();
          	 return true;
          }
      }
  </script>
'; ?>


<a name="featured" />

<?php echo $this->_tpl_vars['lng']['txt_featured_products']; ?>


<br /><br />

<?php ob_start(); ?>

<?php if ($this->_tpl_vars['producttype'] != ""): ?>
<?php endif; ?>

<form action="product_type_action.php" method="post" name="producttypeform">
<input type="hidden" name="mode" />


<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="10">&nbsp;</td>
	<td width="40%"><?php echo $this->_tpl_vars['lng']['lbl_product_type']; ?>
</td>
	<td width="20%">#<?php echo $this->_tpl_vars['lng']['lbl_product_styles']; ?>
</td>
	<td width="15%" align="center">#<?php echo $this->_tpl_vars['lng']['lbl_product_num']; ?>
</td>
	<td width="15%" align="center"><?php echo $this->_tpl_vars['lng']['lbl_active']; ?>
</td>
	<td width="10%" align="center"><?php echo $this->_tpl_vars['lng']['lbl_pos']; ?>
</td>
</tr>

<?php if ($this->_tpl_vars['producttype']): ?>

<?php unset($this->_sections['prod_num']);
$this->_sections['prod_num']['name'] = 'prod_num';
$this->_sections['prod_num']['loop'] = is_array($_loop=$this->_tpl_vars['producttype']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['prod_num']['show'] = true;
$this->_sections['prod_num']['max'] = $this->_sections['prod_num']['loop'];
$this->_sections['prod_num']['step'] = 1;
$this->_sections['prod_num']['start'] = $this->_sections['prod_num']['step'] > 0 ? 0 : $this->_sections['prod_num']['loop']-1;
if ($this->_sections['prod_num']['show']) {
    $this->_sections['prod_num']['total'] = $this->_sections['prod_num']['loop'];
    if ($this->_sections['prod_num']['total'] == 0)
        $this->_sections['prod_num']['show'] = false;
} else
    $this->_sections['prod_num']['total'] = 0;
if ($this->_sections['prod_num']['show']):

            for ($this->_sections['prod_num']['index'] = $this->_sections['prod_num']['start'], $this->_sections['prod_num']['iteration'] = 1;
                 $this->_sections['prod_num']['iteration'] <= $this->_sections['prod_num']['total'];
                 $this->_sections['prod_num']['index'] += $this->_sections['prod_num']['step'], $this->_sections['prod_num']['iteration']++):
$this->_sections['prod_num']['rownum'] = $this->_sections['prod_num']['iteration'];
$this->_sections['prod_num']['index_prev'] = $this->_sections['prod_num']['index'] - $this->_sections['prod_num']['step'];
$this->_sections['prod_num']['index_next'] = $this->_sections['prod_num']['index'] + $this->_sections['prod_num']['step'];
$this->_sections['prod_num']['first']      = ($this->_sections['prod_num']['iteration'] == 1);
$this->_sections['prod_num']['last']       = ($this->_sections['prod_num']['iteration'] == $this->_sections['prod_num']['total']);
?>

<tr<?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this);?>
>
	<td style="left-padding:10px"><input type="checkbox" name="posted_data[<?php echo $this->_tpl_vars['producttype'][$this->_sections['prod_num']['index']]['id']; ?>
][delete]"   /></td>
	<td align="left" style="left-padding:30px"><b><?php echo $this->_tpl_vars['producttype'][$this->_sections['prod_num']['index']]['name']; ?>
</b></td>
	<td align="center"><b><?php if ($this->_tpl_vars['producttype'][$this->_sections['prod_num']['index']]['styles'] == 0): ?>0 <?php else:  echo $this->_tpl_vars['producttype'][$this->_sections['prod_num']['index']]['styles'];  endif; ?></b></td>
	<td align="center"><b><?php if ($this->_tpl_vars['no_of_products'][$this->_sections['prod_num']['index']] == 0): ?>0 <?php else:  echo $this->_tpl_vars['no_of_products'][$this->_sections['prod_num']['index']];  endif; ?></b></td>
	<td align="center">
	<select name="posted_data[<?php echo $this->_tpl_vars['producttype'][$this->_sections['prod_num']['index']]['id']; ?>
][avail]">
		<option value="1"<?php if ($this->_tpl_vars['producttype'][$this->_sections['prod_num']['index']]['is_active'] == '1'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_yes']; ?>
</option>
		<option value="0"<?php if ($this->_tpl_vars['producttype'][$this->_sections['prod_num']['index']]['is_active'] == '0'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_no']; ?>
</option>
	</select>
	</td>
	<td align="center"><input type="text" name="posted_data[<?php echo $this->_tpl_vars['producttype'][$this->_sections['prod_num']['index']]['id']; ?>
][order_by]" size="5" value="<?php echo $this->_tpl_vars['producttype'][$this->_sections['prod_num']['index']]['display_order']; ?>
" /></td>

</tr>

<?php endfor; endif; ?>



<?php else: ?>

<tr>
<td colspan="5" align="center"><?php echo $this->_tpl_vars['lng']['txt_no_product_types']; ?>
</td>
</tr>

<?php endif; ?>

<!--<tr>
<td colspan="5"><br /><br /><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_add_product'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr> -->


<tr>
	<td colspan="5" class="SubmitBox">
	<!--<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_delete_selected'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="return confirmDelete();" />-->
	<input type="submit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_add'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"  onclick="javascript: submitForm(this, 'add');"/>
        <input type="submit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_modify_selected'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: submitForm(this, 'modify');" />
	<input type="submit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_upd_selected'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: submitForm(this, 'update');" />
	</td>
</tr>

</table>
</form>

<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_producttype_heading'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<br /><br />
