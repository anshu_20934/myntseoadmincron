<?php /* Smarty version 2.6.12, created on 2017-03-30 01:18:26
         compiled from main/reports_list.tpl */ ?>
<?php ob_start(); ?>
<?php echo '
<script Language="JavaScript" Type="text/javascript">
		function FrontPage_Form1_Validator(theform)
		{
         //alert("Please Generate a valid sku first");
         var groupname=theform.groupname;
          if (groupname.value==null||groupname.value == "")
		  {
			alert("Please enter the group name");
			groupname.focus();
			return false;
		  }
		  
		  return true;
		  
		}
		function delReport(reportid){
		var del="delete";
		  if(confirm("Do you really want to delete this report")){
		  
		  window.open(\'edit_report.php?reportid=\'+reportid+\'&action=\'+del+\'\',\'null\', \'menubar=1,resizable=1,toolbar=1,width=800,height=500\');
		  
		  }
		
		}
	
		</script>
'; ?>

<form action="reports_management.php" method="post" onsubmit="return FrontPage_Form1_Validator(this)" language="JavaScript" name="groupaddform">
<input type="hidden" name="reports_mode" value="addgroup" />
<table cellpadding="3" cellspacing="1" width="100%">
	<tr>
	     <td><b><?php echo $this->_tpl_vars['grpmsg']; ?>
</b></td>
         <td><b>Group Name</b></td>
         <td><input type="text" name="groupname" size="50"  /></td>
    </tr>
	<tr>
		<td colspan=2><input type="submit" style="background-color:#eeeddd;font-size:10px;" value="Add group" /></td>
	</tr>
</table>
</form>
<?php $this->_smarty_vars['capture']['gdialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'Add New Group','content' => $this->_smarty_vars['capture']['gdialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<br>
<?php ob_start(); ?>

<table cellpadding="3" cellspacing="1" width="100%">
<tr >
		
		<td>
			<input type="button" style="background-color:#eeeddd;font-size:10px;" value="Add new Report" onClick="javascript:window.open('add_new_report.php','null', 'menubar=0,resizable=1,toolbar=0,width=900,height=700')">
		<input type="button" style="background-color:#eeeddd;font-size:10px;" value="View All Schedules" onClick="javascript:window.open('edit_report.php?action=viewschedules','null', 'menubar=0,resizable=1,toolbar=0,width=900,height=700')">
		</td>
	</tr>

<?php if ($this->_tpl_vars['reports']): ?>

<?php $_from = $this->_tpl_vars['reports']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['reportgroup'] => $this->_tpl_vars['report']):
?>

<tr style="background-color:#d0ddff">
	<td style="font-size:14px;color:#000077"><b><?php echo $this->_tpl_vars['reportgroup']; ?>
</b></td>
</tr>
<?php if ($this->_tpl_vars['report']): ?>
<tr>
	<td>
	<table width=40%>		 
<?php $_from = $this->_tpl_vars['report']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['reportid'] => $this->_tpl_vars['reportname']):
?>
<?php if ($this->_tpl_vars['reportname']): ?>
			<tr style="background-color:#eeeddd;padding:5px;margin-bottom:2px;">
			<td style="font-size:12px;"><?php echo $this->_tpl_vars['reportname']; ?>
</td>
			<td style="font-size:12px;width:50px;">
			<a href="#"  onClick="javascript:window.open('view_report.php?reportid=<?php echo $this->_tpl_vars['reportid']; ?>
','null', 'menubar=0,resizable=1,toolbar=0,width=800,height=550')">Run</a>
			</td>
			<td style="font-size:12px;width:50px;">
			<a href="#"  onClick="javascript:window.open('edit_report.php?reportid=<?php echo $this->_tpl_vars['reportid']; ?>
&action=edit','null', 'menubar=0,resizable=1,toolbar=0,width=900,height=700')">edit</a>
			</td>
			<td style="font-size:12px;width:50px;">
			<a href="#"  onClick="javascript:delReport(<?php echo $this->_tpl_vars['reportid']; ?>
)">delete</a>
			</td>
			<td style="font-size:12px;width:50px;">

            <a href="#"  onClick="javascript:window.open('edit_report.php?action=viewschedules&reportid=<?php echo $this->_tpl_vars['reportid']; ?>
','null', 'menubar=0,resizable=1,toolbar=0,width=900,height=700')">schedules</a>
		
			</td>
			</tr>
<?php endif; ?>
<?php endforeach; endif; unset($_from); ?>
	</table>	
	</td>
</tr>
<?php endif; ?>
<?php endforeach; endif; unset($_from); ?>

<?php else: ?>

<tr>
<td colspan="5" align="center">No Reports Found</td>
</tr>

<?php endif; ?>

</table>

<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'Reports','content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
