<?php /* Smarty version 2.6.12, created on 2017-03-24 12:29:00
         compiled from main/category_selector_multiple.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'default', 'main/category_selector_multiple.tpl', 3, false),)), $this); ?>
<?php func_load_lang($this, "main/category_selector_multiple.tpl","lbl_please_select_category"); ?><?php if ($this->_tpl_vars['display_field'] == ''):  $this->assign('display_field', 'category_path');  endif; ?>
<select MULTIPLE style="height:100px;" name="<?php echo ((is_array($_tmp=@$this->_tpl_vars['field'])) ? $this->_run_mod_handler('default', true, $_tmp, 'categoryid') : smarty_modifier_default($_tmp, 'categoryid')); ?>
"<?php echo $this->_tpl_vars['extra']; ?>
>
<?php if ($this->_tpl_vars['display_empty'] == 'P'): ?>
<option value=""><?php echo $this->_tpl_vars['lng']['lbl_please_select_category']; ?>
</option>
<?php elseif ($this->_tpl_vars['display_empty'] == 'E'): ?>
<option value="0">----select category----</option>
<?php else: ?>
<option value="0">----select category----</option>
<?php endif; ?>
<?php $_from = $this->_tpl_vars['allcategories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['catid'] => $this->_tpl_vars['c']):
?>
	<option value="<?php echo $this->_tpl_vars['catid']; ?>
"
		<?php $_from = $this->_tpl_vars['coupon']['catids']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i'] => $this->_tpl_vars['cid']):
?>
			<?php if ($this->_tpl_vars['catid'] == $this->_tpl_vars['cid']): ?>
				selected="selected"
			<?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
	><?php echo $this->_tpl_vars['c'][$this->_tpl_vars['display_field']]; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select>
