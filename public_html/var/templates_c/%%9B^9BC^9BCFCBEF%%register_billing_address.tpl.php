<?php /* Smarty version 2.6.12, created on 2017-03-27 12:58:50
         compiled from main/register_billing_address.tpl */ ?>
<?php if ($this->_tpl_vars['is_areas']['B'] == 'Y'): ?>

<?php if ($this->_tpl_vars['hide_header'] == ""): ?>


	

<?php echo '
<script type="text/javascript">
function ship2diffOpen() {
	var obj = document.getElementById(\'ship2diff\');
	var box = document.getElementById(\'ship_box\');
	if (!obj || !box)
		return;

	box.style.display = obj.checked ? "" : "none";
	if (obj.checked && window.start_js_states && document.getElementById(\'s_country\') && localBFamily == \'Opera\')
		start_js_states(document.getElementById(\'b_country\'));
}
</script>
'; ?>


<?php endif; ?>


<div id="ship_box" <?php if (! $this->_tpl_vars['ship2diff']): ?> style="display: none;"<?php endif; ?>>
 <div class="subhead"><p>billing address</p></div>




<?php if ($this->_tpl_vars['default_fields']['b_address']['avail'] == 'Y'): ?>

<div class="links_980"><div class="legend_980"><p>Address</p> </div>
    <div class="field_980"> <label>
    <textarea id="b_address" name="b_address" class="address" onKeyPress=check_length("b_address"); onKeyDown=check_length("b_address");><?php echo $this->_tpl_vars['userinfo']['b_address']; ?>
</textarea>
    <input type=hidden size=2 value=255 name="text_num" id="text_num">
      <!-- <input type="text" id="b_address" name="b_address"  value="<?php echo $this->_tpl_vars['userinfo']['b_address']; ?>
" /> -->
      <?php if ($this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['b_address'] == "" && $this->_tpl_vars['default_fields']['b_address']['required'] == 'Y'):  endif; ?>
    </label> </div><div class="clearall"></div>
</div>

<?php endif; ?>



<!-- <div class="links_980"><div class="legend_980"><p>Address (Line2)</p></div>
    <div class="field_980"><label>
      <input  type="text" id="b_address_2" name="b_address_2"  value="<?php echo $this->_tpl_vars['userinfo']['b_address_2']; ?>
" />
       <?php if ($this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['b_address_2'] == "" && $this->_tpl_vars['default_fields']['b_address_2']['required'] == 'Y'):  endif; ?>

    </label> </div><div class="clearall"></div>
</div> -->


<?php if ($this->_tpl_vars['default_fields']['b_country']['avail'] == 'Y'): ?>

<div class="links_980"><div class="legend_980"><p>Country</p> </div>
	<div class="field_980">
		<select name="b_country" id="b_country" class="countryselect" onchange="check_zip_code()">
		<?php unset($this->_sections['country_idx']);
$this->_sections['country_idx']['name'] = 'country_idx';
$this->_sections['country_idx']['loop'] = is_array($_loop=$this->_tpl_vars['countries']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['country_idx']['show'] = true;
$this->_sections['country_idx']['max'] = $this->_sections['country_idx']['loop'];
$this->_sections['country_idx']['step'] = 1;
$this->_sections['country_idx']['start'] = $this->_sections['country_idx']['step'] > 0 ? 0 : $this->_sections['country_idx']['loop']-1;
if ($this->_sections['country_idx']['show']) {
    $this->_sections['country_idx']['total'] = $this->_sections['country_idx']['loop'];
    if ($this->_sections['country_idx']['total'] == 0)
        $this->_sections['country_idx']['show'] = false;
} else
    $this->_sections['country_idx']['total'] = 0;
if ($this->_sections['country_idx']['show']):

            for ($this->_sections['country_idx']['index'] = $this->_sections['country_idx']['start'], $this->_sections['country_idx']['iteration'] = 1;
                 $this->_sections['country_idx']['iteration'] <= $this->_sections['country_idx']['total'];
                 $this->_sections['country_idx']['index'] += $this->_sections['country_idx']['step'], $this->_sections['country_idx']['iteration']++):
$this->_sections['country_idx']['rownum'] = $this->_sections['country_idx']['iteration'];
$this->_sections['country_idx']['index_prev'] = $this->_sections['country_idx']['index'] - $this->_sections['country_idx']['step'];
$this->_sections['country_idx']['index_next'] = $this->_sections['country_idx']['index'] + $this->_sections['country_idx']['step'];
$this->_sections['country_idx']['first']      = ($this->_sections['country_idx']['iteration'] == 1);
$this->_sections['country_idx']['last']       = ($this->_sections['country_idx']['iteration'] == $this->_sections['country_idx']['total']);
?>
			<option value="<?php echo $this->_tpl_vars['countries'][$this->_sections['country_idx']['index']]['country_code']; ?>
"<?php if ($this->_tpl_vars['userinfo']['b_country'] == $this->_tpl_vars['countries'][$this->_sections['country_idx']['index']]['country_code']): ?> selected="selected"<?php elseif ($this->_tpl_vars['countries'][$this->_sections['country_idx']['index']]['country_code'] == $this->_tpl_vars['config']['General']['default_country'] && $this->_tpl_vars['userinfo']['b_country'] == ""): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['countries'][$this->_sections['country_idx']['index']]['country']; ?>
</option>
		<?php endfor; endif; ?>
		</select>
		<?php if ($this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['b_country'] == "" && $this->_tpl_vars['default_fields']['b_country']['required'] == 'Y'):  endif; ?>
	</div><div class="clearall"></div>
</div>

<?php endif; ?>

<?php if ($this->_tpl_vars['default_fields']['b_state']['avail'] == 'Y'): ?>

<div class="links_980"><div class="legend_980"><p>State</p> </div>
    <div class="field_980"><label>
      
      <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/states.tpl", 'smarty_include_vars' => array('states' => $this->_tpl_vars['states'],'name' => 'b_state','default' => $this->_tpl_vars['userinfo']['b_state'],'default_country' => $this->_tpl_vars['userinfo']['b_country'],'country_name' => 'b_country')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php if ($this->_tpl_vars['error'] == 'b_statecode' || ( $this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['b_state'] == "" && $this->_tpl_vars['default_fields']['b_state']['required'] == 'Y' )):  endif; ?>

    </label> </div><div class="clearall"></div>
  </div>

<?php endif; ?>



<?php if ($this->_tpl_vars['default_fields']['b_city']['avail'] == 'Y'): ?>

<div class="links_980"><div class="legend_980"><p>City</p> </div>
    <div class="field_980"><label>
      <input  type="text" id="b_city" name="b_city"  value="<?php echo $this->_tpl_vars['userinfo']['b_city']; ?>
" />
<?php if ($this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['b_city'] == "" && $this->_tpl_vars['default_fields']['b_city']['required'] == 'Y'): ?><font class="Star">&lt;&lt;</font><?php endif; ?>

    </label> </div><div class="clearall"></div>
</div>

<?php endif; ?>



<?php if ($this->_tpl_vars['default_fields']['b_county']['avail'] == 'Y' && $this->_tpl_vars['config']['General']['use_counties'] == 'Y'): ?>


<!-- <div class="links_980"><div class="legend_980"><p>Country<span class="mandatory">*</span></p> </div>
 <div class="field_980"> <label> 
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/counties.tpl", 'smarty_include_vars' => array('counties' => $this->_tpl_vars['counties'],'name' => 'b_county','default' => $this->_tpl_vars['userinfo']['b_county'],'country_name' => 'b_country')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php if (( $this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['b_county'] == "" && $this->_tpl_vars['default_fields']['b_county']['required'] == 'Y' ) || $this->_tpl_vars['error'] == 'b_county'):  endif; ?>
</label> </div><div class="clearall"></div>
</div> -->

<?php endif; ?>





<?php if ($this->_tpl_vars['default_fields']['b_state']['avail'] == 'Y' && $this->_tpl_vars['default_fields']['b_country']['avail'] == 'Y' && $this->_tpl_vars['js_enabled'] == 'Y' && $this->_tpl_vars['config']['General']['use_js_states'] == 'Y'): ?>
<div class="field_980" style="display: none;">
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/register_states.tpl", 'smarty_include_vars' => array('state_name' => 'b_state','country_name' => 'b_country','county_name' => 'b_county','state_value' => $this->_tpl_vars['userinfo']['b_state'],'county_value' => $this->_tpl_vars['userinfo']['b_county'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	</div>
<?php endif; ?>

<?php if ($this->_tpl_vars['default_fields']['phone']['avail'] == 'Y'): ?>

<div class="links_980"><div class="legend_980"><p>Phone</p> </div>
    <div class="field_980"> <label> 
      
      <input type="text" id="b_phone" name="b_phone"  value="<?php echo $this->_tpl_vars['userinfo']['phone']; ?>
" />
    </label> </div><div class="clearall"></div>
  </div>

<?php endif; ?>


<?php if ($this->_tpl_vars['default_fields']['b_zipcode']['avail'] == 'Y'): ?>

<div class="links_980"><div class="legend_980"><p>Zip/Postal Code</p></div>

  

            <div class="field_980"><label>
              
	     <input  type="text" id="b_zipcode" name="b_zipcode"  value="<?php echo $this->_tpl_vars['userinfo']['b_zipcode']; ?>
" onchange="check_zip_code()" size="7" maxlength="6"/>
<?php if ($this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['b_zipcode'] == "" && $this->_tpl_vars['default_fields']['b_zipcode']['required'] == 'Y'):  endif; ?>

          </label></div><div class="clearall"></div>
       </div> 



 
<?php endif; ?>

<div class="foot"></div>
</div>
 <br>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/register_additional_info.tpl", 'smarty_include_vars' => array('section' => 'B')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>