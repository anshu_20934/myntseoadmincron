<?php /* Smarty version 2.6.12, created on 2017-06-14 09:15:27
         compiled from admin/ServiceRequest/manageServiceRequest.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'admin/ServiceRequest/manageServiceRequest.tpl', 629, false),)), $this); ?>
<link href="<?php echo $this->_tpl_vars['http_location']; ?>
/skin1/myntra_css/jquery-ui-datepicker.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo $this->_tpl_vars['http_location']; ?>
/skin1/myntra_js/jquery-ui-datepicker.js"></script>

<?php echo '    
<style type="text/css">
    .selectTR{background-color:#f0c620}
</style>
<script type="text/javascript">

    $(document).ready(function(){


        //initializing datepicker
        $( "#fStartCrDate, #fEndCrDate, #fStartDuDate, #fEndDuDate, #fStartClDate, #fEndClDate" ).datepicker({
            dateFormat:\'mm/dd/yy\',
            changeMonth: true,
            changeYear: true,
            showOn: \'both\',
            buttonImageOnly: true,
            buttonImage: \'http://myntra.myntassets.com/images/cal.gif\',
            buttonText: \'Calendar\'
        });

        //initializing datetimepicker
        $( "#fStartCallback, #fEndCallback" ).datetimepicker({
            dateFormat:\'mm/dd/yy\',
            changeMonth: true,
            changeYear: true,
            showOn: \'both\',
            buttonImageOnly: true,
            buttonImage: \'http://myntra.myntassets.com/images/cal.gif\',
            buttonText: \'Calendar\'
        });
        
        
        $(\'#SRListTable tr\').toggle(function(){
            if(!$(this).hasClass(\'TableHead\')){
                $(this).addClass(\'selectTR\');
            }
        }, function(){
            if(!$(this).hasClass(\'TableHead\')){
                $(this).removeClass(\'selectTR\');

            }

        });

        $(\'#SRListTable tr a\').click(function(){
            location.href = $(this).attr(\'href\');
        });

        $(\'#exportSR\').click(function(){
        
            var crDateObj = $(\'#fStartCrDate\');
            var endDateObj = $(\'#fEndCrDate\');

            if(crDateObj.val() == \'\' || endDateObj.val() == \'\'){
                alert("please enter \'Create Date Start\' and \'Create Date End\' to generate the report");

                if(crDateObj.val() == \'\'){
                    crDateObj.focus();
                } else {
                    endDateObj.focus();
                }
                return false;

            } else {
                location.href = $(this).attr(\'href\')+\'&fStartCrDate=\'+crDateObj.val()+\'&fEndCrDate=\'+endDateObj.val();
                return false;
            }
            
        });



    });

    function deleteNonOrderSR(nonOrderSRId){
        if(confirm(\'Are you sure to delete?\')){
            makePostAjax(http_loc+\'/admin/ServiceRequest/manageNonOrderSR.php\', \'mode=deleteNonOrderServieRequest&non_order_sr_id=\'+nonOrderSRId, false, deleteSRCallBack);
        } else {
            return false;
        }
    }

    function deleteOrderSR(orderSRId){
        if(confirm(\'Are you sure to delete?\')){
            makePostAjax(http_loc+\'/admin/manageOrderSR.php\', \'mode=deleteServieRequest&order_sr_id=\'+orderSRId, false, deleteSRCallBack);
        } else {
            return false;
        }
    }

    function makePostAjax(url, inputData, cache, callBackFunc){
        $.ajax({
            type:\'POST\',
            url: url,
            data: inputData,
            cache:cache,
            success: callBackFunc
        });
    }

    function deleteSRCallBack(data){
        alert(data);
        location.reload();
    }
</script>
'; ?>

<?php ob_start(); ?>
    <div>
        <div style="float:left;background-color:#f0f0f0;">
            <form name="searchSR"  action="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/ServiceRequest/manageServiceRequest.php">
            <input type="hidden" name="pagelimit" value="<?php echo $this->_tpl_vars['pagelimit']; ?>
">
            <table cellpadding="3" cellspacing="1" width="100%">
                <tr>
                    <td>
                        <table>

                            <tr>
                                <td>Callback Time Start</td>
                                <td>
                                    <input type="text" name="fStartCallback" readonly="readonly" id="fStartCallback" value="<?php echo $GLOBALS['HTTP_GET_VARS']['fStartCallback']; ?>
"/>
                                </td>
                                <td>Callback Time End</td>
                                <td>
                                    <input type="text" name="fEndCallback" readonly="readonly" id="fEndCallback" value="<?php echo $GLOBALS['HTTP_GET_VARS']['fEndCallback']; ?>
"/>
                                </td>
                            </tr>

                            <tr>
                                <td>Create Date Start</td>
                                <td>
                                    <input type="text" name="fStartCrDate" readonly="readonly" id="fStartCrDate" value="<?php echo $GLOBALS['HTTP_GET_VARS']['fStartCrDate']; ?>
"/>
                                </td>
                                <td>Create Date End</td>
                                <td>
                                    <input type="text" name="fEndCrDate" readonly="readonly" id="fEndCrDate" value="<?php echo $GLOBALS['HTTP_GET_VARS']['fEndCrDate']; ?>
"/>                                    
                                </td>
                            </tr>

                            <tr>
                                <td>Due Date Start</td>
                                <td>
                                    <input type="text" name="fStartDuDate" readonly="readonly" id="fStartDuDate" value="<?php echo $GLOBALS['HTTP_GET_VARS']['fStartDuDate']; ?>
"/>
                                </td>
                                <td>Due Date End</td>
                                <td>
                                    <input type="text" name="fEndDuDate" readonly="readonly" id="fEndDuDate" value="<?php echo $GLOBALS['HTTP_GET_VARS']['fEndDuDate']; ?>
"/>                                    
                                </td>
                            </tr>

                            <tr>
                                <td>Close Date Start</td>
                                <td>
                                    <input type="text" name="fStartClDate" readonly="readonly" id="fStartClDate" value="<?php echo $GLOBALS['HTTP_GET_VARS']['fStartClDate']; ?>
"/>                                    
                                </td>
                                <td>Close Date End</td>
                                <td>
                                    <input type="text" name="fEndClDate" readonly="readonly" id="fEndClDate" value="<?php echo $GLOBALS['HTTP_GET_VARS']['fEndClDate']; ?>
"/>                                    
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">SR ID</td>
                                <td colspan="2"><input type="text" name="fSRid" maxlength="11" value="<?php echo $GLOBALS['HTTP_GET_VARS']['fSRid']; ?>
"/></td>
                            </tr>

                            <tr>
                                <td colspan="2">Customer SR ID</td>
                                <td colspan="2"><input type="text" name="fCSRid" maxlength="10" value="<?php echo $GLOBALS['HTTP_GET_VARS']['fCSRid']; ?>
"/></td>
                            </tr>

                            <tr>
                                <td colspan="2">Order Id</td>
                                <td colspan="2"><input type="text" name="fSROid" maxlength="11" value="<?php echo $GLOBALS['HTTP_GET_VARS']['fSROid']; ?>
"/>&nbsp; (relevant only for order SRs..)</td>
                            </tr>

                            <tr>
                                <td colspan="2">Refund</td>
                                <td colspan="2">
                                    <select name="fSRRef">
                                        <option value="0">--All--</option>
                                        <?php if ($GLOBALS['HTTP_GET_VARS']['fSRRef'] == 'Y'): ?>
                                            <option value="Y" selected="selected">Y</option>
                                            <option value="N">N</option>
                                        <?php elseif ($GLOBALS['HTTP_GET_VARS']['fSRRef'] == 'N'): ?>
                                            <option value="Y">Y</option>
                                            <option value="N" selected="selected">N</option>
                                        <?php else: ?>
                                            <option value="Y">Y</option>
                                            <option value="N">N</option>
                                        <?php endif; ?>
                                    </select>&nbsp; (relevant only for order SRs..)
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">Compensation</td>
                                <td colspan="2">
                                    <select name="fSRComp">
                                        <option value="0">--All--</option>
                                        <?php if ($GLOBALS['HTTP_GET_VARS']['fSRComp'] == 'Y'): ?>
                                            <option value="Y" selected="selected">Y</option>
                                            <option value="N">N</option>
                                        <?php elseif ($GLOBALS['HTTP_GET_VARS']['fSRComp'] == 'N'): ?>
                                            <option value="Y">Y</option>
                                            <option value="N" selected="selected">N</option>
                                        <?php else: ?>
                                            <option value="Y">Y</option>
                                            <option value="N">N</option>
                                        <?php endif; ?>
                                    </select>&nbsp; (relevant only for order SRs..)
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">SR Type</td>
                                <td colspan="2">
                                    <select name="fSRType">
                                        <option value="0">--All--</option>
                                        <?php if ($GLOBALS['HTTP_GET_VARS']['fSRType'] == 'order'): ?>
                                            <option value="order" selected="selected">Order</option>
                                            <option value="non-order">Non-Order</option>
                                        <?php elseif ($GLOBALS['HTTP_GET_VARS']['fSRType'] == 'non-order'): ?>
                                            <option value="order">Order</option>
                                            <option value="non-order" selected="selected">Non-Order</option>
                                        <?php else: ?>
                                            <option value="order">Order</option>
                                            <option value="non-order">Non-Order</option>
                                        <?php endif; ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">Category</td>
                                <td colspan="2">
                                    <select name="fCat">
                                        <option value="0">--All--</option>
                                        <?php unset($this->_sections['category']);
$this->_sections['category']['name'] = 'category';
$this->_sections['category']['loop'] = is_array($_loop=$this->_tpl_vars['sr_category']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['category']['show'] = true;
$this->_sections['category']['max'] = $this->_sections['category']['loop'];
$this->_sections['category']['step'] = 1;
$this->_sections['category']['start'] = $this->_sections['category']['step'] > 0 ? 0 : $this->_sections['category']['loop']-1;
if ($this->_sections['category']['show']) {
    $this->_sections['category']['total'] = $this->_sections['category']['loop'];
    if ($this->_sections['category']['total'] == 0)
        $this->_sections['category']['show'] = false;
} else
    $this->_sections['category']['total'] = 0;
if ($this->_sections['category']['show']):

            for ($this->_sections['category']['index'] = $this->_sections['category']['start'], $this->_sections['category']['iteration'] = 1;
                 $this->_sections['category']['iteration'] <= $this->_sections['category']['total'];
                 $this->_sections['category']['index'] += $this->_sections['category']['step'], $this->_sections['category']['iteration']++):
$this->_sections['category']['rownum'] = $this->_sections['category']['iteration'];
$this->_sections['category']['index_prev'] = $this->_sections['category']['index'] - $this->_sections['category']['step'];
$this->_sections['category']['index_next'] = $this->_sections['category']['index'] + $this->_sections['category']['step'];
$this->_sections['category']['first']      = ($this->_sections['category']['iteration'] == 1);
$this->_sections['category']['last']       = ($this->_sections['category']['iteration'] == $this->_sections['category']['total']);
?>
                                            <?php if ($this->_tpl_vars['sr_category'][$this->_sections['category']['index']]['cat_active'] == 'Y'): ?>
                                                <?php if ($GLOBALS['HTTP_GET_VARS']['fCat'] == $this->_tpl_vars['sr_category'][$this->_sections['category']['index']]['id']): ?>
                                                    <option value="<?php echo $this->_tpl_vars['sr_category'][$this->_sections['category']['index']]['id']; ?>
" selected="selected"><?php echo $this->_tpl_vars['sr_category'][$this->_sections['category']['index']]['SR_category']; ?>
-(<?php echo $this->_tpl_vars['sr_category'][$this->_sections['category']['index']]['category_type']; ?>
)</option>
                                                <?php else: ?>
                                                    <option value="<?php echo $this->_tpl_vars['sr_category'][$this->_sections['category']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['sr_category'][$this->_sections['category']['index']]['SR_category']; ?>
-(<?php echo $this->_tpl_vars['sr_category'][$this->_sections['category']['index']]['category_type']; ?>
)</option>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endfor; endif; ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">Sub Category</td>
                                <td colspan="2">
                                    <select name="fSCat">
                                        <option value="0">--All--</option>
                                        <?php unset($this->_sections['category']);
$this->_sections['category']['name'] = 'category';
$this->_sections['category']['loop'] = is_array($_loop=$this->_tpl_vars['sr_category']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['category']['show'] = true;
$this->_sections['category']['max'] = $this->_sections['category']['loop'];
$this->_sections['category']['step'] = 1;
$this->_sections['category']['start'] = $this->_sections['category']['step'] > 0 ? 0 : $this->_sections['category']['loop']-1;
if ($this->_sections['category']['show']) {
    $this->_sections['category']['total'] = $this->_sections['category']['loop'];
    if ($this->_sections['category']['total'] == 0)
        $this->_sections['category']['show'] = false;
} else
    $this->_sections['category']['total'] = 0;
if ($this->_sections['category']['show']):

            for ($this->_sections['category']['index'] = $this->_sections['category']['start'], $this->_sections['category']['iteration'] = 1;
                 $this->_sections['category']['iteration'] <= $this->_sections['category']['total'];
                 $this->_sections['category']['index'] += $this->_sections['category']['step'], $this->_sections['category']['iteration']++):
$this->_sections['category']['rownum'] = $this->_sections['category']['iteration'];
$this->_sections['category']['index_prev'] = $this->_sections['category']['index'] - $this->_sections['category']['step'];
$this->_sections['category']['index_next'] = $this->_sections['category']['index'] + $this->_sections['category']['step'];
$this->_sections['category']['first']      = ($this->_sections['category']['iteration'] == 1);
$this->_sections['category']['last']       = ($this->_sections['category']['iteration'] == $this->_sections['category']['total']);
?>
                                            <?php if ($this->_tpl_vars['sr_category'][$this->_sections['category']['index']]['cat_active'] == 'Y'): ?>
                                                <optgroup label="<?php echo $this->_tpl_vars['sr_category'][$this->_sections['category']['index']]['SR_category']; ?>
-(<?php echo $this->_tpl_vars['sr_category'][$this->_sections['category']['index']]['category_type']; ?>
)"></optgroup>

                                                <?php $this->assign('subcategories', $this->_tpl_vars['sr_category'][$this->_sections['category']['index']]['subCategories']); ?>
                                                <?php unset($this->_sections['index']);
$this->_sections['index']['name'] = 'index';
$this->_sections['index']['loop'] = is_array($_loop=$this->_tpl_vars['subcategories']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['index']['show'] = true;
$this->_sections['index']['max'] = $this->_sections['index']['loop'];
$this->_sections['index']['step'] = 1;
$this->_sections['index']['start'] = $this->_sections['index']['step'] > 0 ? 0 : $this->_sections['index']['loop']-1;
if ($this->_sections['index']['show']) {
    $this->_sections['index']['total'] = $this->_sections['index']['loop'];
    if ($this->_sections['index']['total'] == 0)
        $this->_sections['index']['show'] = false;
} else
    $this->_sections['index']['total'] = 0;
if ($this->_sections['index']['show']):

            for ($this->_sections['index']['index'] = $this->_sections['index']['start'], $this->_sections['index']['iteration'] = 1;
                 $this->_sections['index']['iteration'] <= $this->_sections['index']['total'];
                 $this->_sections['index']['index'] += $this->_sections['index']['step'], $this->_sections['index']['iteration']++):
$this->_sections['index']['rownum'] = $this->_sections['index']['iteration'];
$this->_sections['index']['index_prev'] = $this->_sections['index']['index'] - $this->_sections['index']['step'];
$this->_sections['index']['index_next'] = $this->_sections['index']['index'] + $this->_sections['index']['step'];
$this->_sections['index']['first']      = ($this->_sections['index']['iteration'] == 1);
$this->_sections['index']['last']       = ($this->_sections['index']['iteration'] == $this->_sections['index']['total']);
?>
                                                    <?php if ($this->_tpl_vars['subcategories'][$this->_sections['index']['index']]['subcat_active'] == 'Y'): ?>
                                                        <?php if ($GLOBALS['HTTP_GET_VARS']['fSCat'] == $this->_tpl_vars['subcategories'][$this->_sections['index']['index']]['id']): ?>
                                                            <option value="<?php echo $this->_tpl_vars['subcategories'][$this->_sections['index']['index']]['id']; ?>
" selected="selected"><?php echo $this->_tpl_vars['subcategories'][$this->_sections['index']['index']]['SR_subcategory']; ?>
</option>
                                                        <?php else: ?>
                                                            <option value="<?php echo $this->_tpl_vars['subcategories'][$this->_sections['index']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['subcategories'][$this->_sections['index']['index']]['SR_subcategory']; ?>
</option>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                <?php endfor; endif; ?>
                                            <?php endif; ?>
                                        <?php endfor; endif; ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">Department</td>
                                <td colspan="2">
                                    <select name="SR_department">
                                        <option value="0">--All--</option>
                                        <?php unset($this->_sections['department']);
$this->_sections['department']['name'] = 'department';
$this->_sections['department']['loop'] = is_array($_loop=$this->_tpl_vars['sr_department']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['department']['show'] = true;
$this->_sections['department']['max'] = $this->_sections['department']['loop'];
$this->_sections['department']['step'] = 1;
$this->_sections['department']['start'] = $this->_sections['department']['step'] > 0 ? 0 : $this->_sections['department']['loop']-1;
if ($this->_sections['department']['show']) {
    $this->_sections['department']['total'] = $this->_sections['department']['loop'];
    if ($this->_sections['department']['total'] == 0)
        $this->_sections['department']['show'] = false;
} else
    $this->_sections['department']['total'] = 0;
if ($this->_sections['department']['show']):

            for ($this->_sections['department']['index'] = $this->_sections['department']['start'], $this->_sections['department']['iteration'] = 1;
                 $this->_sections['department']['iteration'] <= $this->_sections['department']['total'];
                 $this->_sections['department']['index'] += $this->_sections['department']['step'], $this->_sections['department']['iteration']++):
$this->_sections['department']['rownum'] = $this->_sections['department']['iteration'];
$this->_sections['department']['index_prev'] = $this->_sections['department']['index'] - $this->_sections['department']['step'];
$this->_sections['department']['index_next'] = $this->_sections['department']['index'] + $this->_sections['department']['step'];
$this->_sections['department']['first']      = ($this->_sections['department']['iteration'] == 1);
$this->_sections['department']['last']       = ($this->_sections['department']['iteration'] == $this->_sections['department']['total']);
?>
                                            <?php if ($GLOBALS['HTTP_GET_VARS']['SR_department'] == $this->_tpl_vars['sr_department'][$this->_sections['department']['index']]): ?>
                                                <option value="<?php echo $this->_tpl_vars['sr_department'][$this->_sections['department']['index']]; ?>
" selected="selected"><?php echo $this->_tpl_vars['sr_department'][$this->_sections['department']['index']]; ?>
</option>
                                            <?php else: ?>
                                                <option value="<?php echo $this->_tpl_vars['sr_department'][$this->_sections['department']['index']]; ?>
"><?php echo $this->_tpl_vars['sr_department'][$this->_sections['department']['index']]; ?>
</option>
                                            <?php endif; ?>
                                        <?php endfor; endif; ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">Channel</td>
                                <td colspan="2">
                                    <select name="SR_channel">
                                        <option value="0">--All--</option>
                                        <?php unset($this->_sections['channel']);
$this->_sections['channel']['name'] = 'channel';
$this->_sections['channel']['loop'] = is_array($_loop=$this->_tpl_vars['sr_channel']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['channel']['show'] = true;
$this->_sections['channel']['max'] = $this->_sections['channel']['loop'];
$this->_sections['channel']['step'] = 1;
$this->_sections['channel']['start'] = $this->_sections['channel']['step'] > 0 ? 0 : $this->_sections['channel']['loop']-1;
if ($this->_sections['channel']['show']) {
    $this->_sections['channel']['total'] = $this->_sections['channel']['loop'];
    if ($this->_sections['channel']['total'] == 0)
        $this->_sections['channel']['show'] = false;
} else
    $this->_sections['channel']['total'] = 0;
if ($this->_sections['channel']['show']):

            for ($this->_sections['channel']['index'] = $this->_sections['channel']['start'], $this->_sections['channel']['iteration'] = 1;
                 $this->_sections['channel']['iteration'] <= $this->_sections['channel']['total'];
                 $this->_sections['channel']['index'] += $this->_sections['channel']['step'], $this->_sections['channel']['iteration']++):
$this->_sections['channel']['rownum'] = $this->_sections['channel']['iteration'];
$this->_sections['channel']['index_prev'] = $this->_sections['channel']['index'] - $this->_sections['channel']['step'];
$this->_sections['channel']['index_next'] = $this->_sections['channel']['index'] + $this->_sections['channel']['step'];
$this->_sections['channel']['first']      = ($this->_sections['channel']['iteration'] == 1);
$this->_sections['channel']['last']       = ($this->_sections['channel']['iteration'] == $this->_sections['channel']['total']);
?>
                                            <?php if ($GLOBALS['HTTP_GET_VARS']['SR_channel'] == $this->_tpl_vars['sr_channel'][$this->_sections['channel']['index']]): ?>
                                                <option value="<?php echo $this->_tpl_vars['sr_channel'][$this->_sections['channel']['index']]; ?>
" selected="selected"><?php echo $this->_tpl_vars['sr_channel'][$this->_sections['channel']['index']]; ?>
</option>
                                            <?php else: ?>
                                                <option value="<?php echo $this->_tpl_vars['sr_channel'][$this->_sections['channel']['index']]; ?>
"><?php echo $this->_tpl_vars['sr_channel'][$this->_sections['channel']['index']]; ?>
</option>
                                            <?php endif; ?>
                                        <?php endfor; endif; ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">Ageing</td>
                                <td colspan="2">
                                    <select name="fAge">
                                        <option value="0">--All--</option>
                                        <?php unset($this->_sections['index']);
$this->_sections['index']['name'] = 'index';
$this->_sections['index']['loop'] = is_array($_loop=$this->_tpl_vars['ageingarray']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['index']['show'] = true;
$this->_sections['index']['max'] = $this->_sections['index']['loop'];
$this->_sections['index']['step'] = 1;
$this->_sections['index']['start'] = $this->_sections['index']['step'] > 0 ? 0 : $this->_sections['index']['loop']-1;
if ($this->_sections['index']['show']) {
    $this->_sections['index']['total'] = $this->_sections['index']['loop'];
    if ($this->_sections['index']['total'] == 0)
        $this->_sections['index']['show'] = false;
} else
    $this->_sections['index']['total'] = 0;
if ($this->_sections['index']['show']):

            for ($this->_sections['index']['index'] = $this->_sections['index']['start'], $this->_sections['index']['iteration'] = 1;
                 $this->_sections['index']['iteration'] <= $this->_sections['index']['total'];
                 $this->_sections['index']['index'] += $this->_sections['index']['step'], $this->_sections['index']['iteration']++):
$this->_sections['index']['rownum'] = $this->_sections['index']['iteration'];
$this->_sections['index']['index_prev'] = $this->_sections['index']['index'] - $this->_sections['index']['step'];
$this->_sections['index']['index_next'] = $this->_sections['index']['index'] + $this->_sections['index']['step'];
$this->_sections['index']['first']      = ($this->_sections['index']['iteration'] == 1);
$this->_sections['index']['last']       = ($this->_sections['index']['iteration'] == $this->_sections['index']['total']);
?>
                                            <?php if ($GLOBALS['HTTP_GET_VARS']['fAge'] == $this->_tpl_vars['ageingarray'][$this->_sections['index']['index']]): ?>
                                                <option value="<?php echo $this->_tpl_vars['ageingarray'][$this->_sections['index']['index']]; ?>
" selected="selected"><?php echo $this->_tpl_vars['ageingarray'][$this->_sections['index']['index']]; ?>
 or more days from created</option>
                                            <?php else: ?>
                                                <option value="<?php echo $this->_tpl_vars['ageingarray'][$this->_sections['index']['index']]; ?>
"><?php echo $this->_tpl_vars['ageingarray'][$this->_sections['index']['index']]; ?>
 or more days from created</option>
                                            <?php endif; ?>
                                        <?php endfor; endif; ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">Status</td>
                                <td colspan="2">
                                    <select name="fStat">
                                        <option value="0">--All--</option>
                                        <?php unset($this->_sections['index']);
$this->_sections['index']['name'] = 'index';
$this->_sections['index']['loop'] = is_array($_loop=$this->_tpl_vars['sr_status']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['index']['show'] = true;
$this->_sections['index']['max'] = $this->_sections['index']['loop'];
$this->_sections['index']['step'] = 1;
$this->_sections['index']['start'] = $this->_sections['index']['step'] > 0 ? 0 : $this->_sections['index']['loop']-1;
if ($this->_sections['index']['show']) {
    $this->_sections['index']['total'] = $this->_sections['index']['loop'];
    if ($this->_sections['index']['total'] == 0)
        $this->_sections['index']['show'] = false;
} else
    $this->_sections['index']['total'] = 0;
if ($this->_sections['index']['show']):

            for ($this->_sections['index']['index'] = $this->_sections['index']['start'], $this->_sections['index']['iteration'] = 1;
                 $this->_sections['index']['iteration'] <= $this->_sections['index']['total'];
                 $this->_sections['index']['index'] += $this->_sections['index']['step'], $this->_sections['index']['iteration']++):
$this->_sections['index']['rownum'] = $this->_sections['index']['iteration'];
$this->_sections['index']['index_prev'] = $this->_sections['index']['index'] - $this->_sections['index']['step'];
$this->_sections['index']['index_next'] = $this->_sections['index']['index'] + $this->_sections['index']['step'];
$this->_sections['index']['first']      = ($this->_sections['index']['iteration'] == 1);
$this->_sections['index']['last']       = ($this->_sections['index']['iteration'] == $this->_sections['index']['total']);
?>
                                            <?php if ($this->_tpl_vars['sr_status'][$this->_sections['index']['index']]['status_active'] == 'Y'): ?>
                                                <?php if ($GLOBALS['HTTP_GET_VARS']['fStat'] == $this->_tpl_vars['sr_status'][$this->_sections['index']['index']]['id']): ?>
                                                    <option value="<?php echo $this->_tpl_vars['sr_status'][$this->_sections['index']['index']]['id']; ?>
" selected="selected"><?php echo $this->_tpl_vars['sr_status'][$this->_sections['index']['index']]['SR_status']; ?>
</option>
                                                <?php else: ?>
                                                    <option value="<?php echo $this->_tpl_vars['sr_status'][$this->_sections['index']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['sr_status'][$this->_sections['index']['index']]['SR_status']; ?>
</option>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endfor; endif; ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">Priority</td>
                                <td colspan="2">
                                    <select name="fPri">
                                        <option value="0">--All--</option>
                                        <?php unset($this->_sections['index']);
$this->_sections['index']['name'] = 'index';
$this->_sections['index']['loop'] = is_array($_loop=$this->_tpl_vars['sr_priority']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['index']['show'] = true;
$this->_sections['index']['max'] = $this->_sections['index']['loop'];
$this->_sections['index']['step'] = 1;
$this->_sections['index']['start'] = $this->_sections['index']['step'] > 0 ? 0 : $this->_sections['index']['loop']-1;
if ($this->_sections['index']['show']) {
    $this->_sections['index']['total'] = $this->_sections['index']['loop'];
    if ($this->_sections['index']['total'] == 0)
        $this->_sections['index']['show'] = false;
} else
    $this->_sections['index']['total'] = 0;
if ($this->_sections['index']['show']):

            for ($this->_sections['index']['index'] = $this->_sections['index']['start'], $this->_sections['index']['iteration'] = 1;
                 $this->_sections['index']['iteration'] <= $this->_sections['index']['total'];
                 $this->_sections['index']['index'] += $this->_sections['index']['step'], $this->_sections['index']['iteration']++):
$this->_sections['index']['rownum'] = $this->_sections['index']['iteration'];
$this->_sections['index']['index_prev'] = $this->_sections['index']['index'] - $this->_sections['index']['step'];
$this->_sections['index']['index_next'] = $this->_sections['index']['index'] + $this->_sections['index']['step'];
$this->_sections['index']['first']      = ($this->_sections['index']['iteration'] == 1);
$this->_sections['index']['last']       = ($this->_sections['index']['iteration'] == $this->_sections['index']['total']);
?>
                                            <?php if ($this->_tpl_vars['sr_priority'][$this->_sections['index']['index']]['priority_active'] == 'Y'): ?>
                                                <?php if ($GLOBALS['HTTP_GET_VARS']['fPri'] == $this->_tpl_vars['sr_priority'][$this->_sections['index']['index']]['id']): ?>
                                                    <option value="<?php echo $this->_tpl_vars['sr_priority'][$this->_sections['index']['index']]['id']; ?>
" selected="selected"><?php echo $this->_tpl_vars['sr_priority'][$this->_sections['index']['index']]['SR_priority']; ?>
</option>
                                                <?php else: ?>
                                                    <option value="<?php echo $this->_tpl_vars['sr_priority'][$this->_sections['index']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['sr_priority'][$this->_sections['index']['index']]['SR_priority']; ?>
</option>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endfor; endif; ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">Creator</td>
                                <td colspan="2"><input type="text" name="fRepo" maxlength="100" value="<?php echo $GLOBALS['HTTP_GET_VARS']['fRepo']; ?>
"/></td>
                            </tr>

                            <tr>
                                <td colspan="2">Resolver</td>
                                <td colspan="2"><input type="text" name="fReso" maxlength="100" value="<?php echo $GLOBALS['HTTP_GET_VARS']['fReso']; ?>
"/></td>
                            </tr>

                            <tr>
                                <td colspan="2">Customer</td>
                                <td colspan="2"><input type="text" name="fCust" maxlength="100" value="<?php echo $GLOBALS['HTTP_GET_VARS']['fCust']; ?>
"/></td>
                            </tr>

                            <tr>
                               <td colspan="2">&nbsp;</td>
                                <td colspan="2"><input type="submit" value="Search Service Request" class="button"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            </form>
        </div>
        <div style="clear:both;"></div>
    </div>

    <br/>
    <hr>
    <table>
        <tr>
            <td align="left">
                <input type="button" name="Add Non-Order Service Request" value="Add Non-Order Service Request" class="button" onClick="javascript:window.open(http_loc+'/admin/ServiceRequest/manageNonOrderSR.php?mode=addNonOrderServieRequest','null', 'menubar=0,resizable=1,toolbar=0,width=800,height=500')">
                &nbsp;|&nbsp;<a href="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/ServiceRequest/manageServiceRequest.php?pg=<?php echo $this->_tpl_vars['pg']; ?>
&pagelimit=<?php echo $this->_tpl_vars['pagelimit']; ?>
">Clear Sort and Search</a>
                &nbsp;|&nbsp;<a href="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/ServiceRequest/manageServiceRequest.php?mode=exportSR&<?php echo $this->_tpl_vars['query_string']; ?>
" id="exportSR">Export to XLS</a>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="2" cellspacing="1" width="100%">
                    <tr>
                        <td width="100px">Records In Page:</td>
                        <td align="left">
                            <select name="pagelimit" onchange="location.href='<?php echo $this->_tpl_vars['url']; ?>
&pg=1&pagelimit='+$(this).val();">
                                <?php unset($this->_sections['idx']);
$this->_sections['idx']['name'] = 'idx';
$this->_sections['idx']['loop'] = is_array($_loop=$this->_tpl_vars['limitarray']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['idx']['show'] = true;
$this->_sections['idx']['max'] = $this->_sections['idx']['loop'];
$this->_sections['idx']['step'] = 1;
$this->_sections['idx']['start'] = $this->_sections['idx']['step'] > 0 ? 0 : $this->_sections['idx']['loop']-1;
if ($this->_sections['idx']['show']) {
    $this->_sections['idx']['total'] = $this->_sections['idx']['loop'];
    if ($this->_sections['idx']['total'] == 0)
        $this->_sections['idx']['show'] = false;
} else
    $this->_sections['idx']['total'] = 0;
if ($this->_sections['idx']['show']):

            for ($this->_sections['idx']['index'] = $this->_sections['idx']['start'], $this->_sections['idx']['iteration'] = 1;
                 $this->_sections['idx']['iteration'] <= $this->_sections['idx']['total'];
                 $this->_sections['idx']['index'] += $this->_sections['idx']['step'], $this->_sections['idx']['iteration']++):
$this->_sections['idx']['rownum'] = $this->_sections['idx']['iteration'];
$this->_sections['idx']['index_prev'] = $this->_sections['idx']['index'] - $this->_sections['idx']['step'];
$this->_sections['idx']['index_next'] = $this->_sections['idx']['index'] + $this->_sections['idx']['step'];
$this->_sections['idx']['first']      = ($this->_sections['idx']['iteration'] == 1);
$this->_sections['idx']['last']       = ($this->_sections['idx']['iteration'] == $this->_sections['idx']['total']);
?>
                                    <?php if ($GLOBALS['HTTP_GET_VARS']['pagelimit'] == $this->_tpl_vars['limitarray'][$this->_sections['idx']['index']]): ?>
                                        <option value="<?php echo $this->_tpl_vars['limitarray'][$this->_sections['idx']['index']]; ?>
" selected=selected><?php echo $this->_tpl_vars['limitarray'][$this->_sections['idx']['index']]; ?>
</option>
                                    <?php else: ?>
                                        <option value="<?php echo $this->_tpl_vars['limitarray'][$this->_sections['idx']['index']]; ?>
"><?php echo $this->_tpl_vars['limitarray'][$this->_sections['idx']['index']]; ?>
</option>
                                    <?php endif; ?>
                                <?php endfor; endif; ?>
                            </select>
                        </td>
                        <td width="700px" align="center">page <b><?php echo $this->_tpl_vars['pg']; ?>
</b> of <b><?php echo $this->_tpl_vars['totalpages']; ?>
</b>&nbsp;[showing <b><?php echo $this->_tpl_vars['offset_start']; ?>
-<?php echo $this->_tpl_vars['offset_end']; ?>
</b> of <b><?php echo $this->_tpl_vars['total_record']; ?>
]</b></td>
                        <td align="right"><?php echo $this->_tpl_vars['paginator']; ?>
</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <div style="width:1125px;overflow:scroll;height:500px;">
        <table cellpadding="2" cellspacing="1" width="100%" title="scroll horizontally to view all columns" id="SRListTable">

            <tr class="TableHead">
                <th style="padding: 5px;">
                    <?php if ($GLOBALS['HTTP_GET_VARS']['obSRid'] == 'desc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_bottom.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbSRid=SR_id&obSRid=asc" >Id</a>
                    <?php elseif ($GLOBALS['HTTP_GET_VARS']['obSRid'] == 'asc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_top.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbSRid=SR_id&obSRid=desc" >Id</a>
                    <?php else: ?>
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbSRid=SR_id&obSRid=desc" >Id</a>
                    <?php endif; ?>
                </th>

                <th style="padding: 5px;">
                    Customer SR Id
                </th>

                <th style="padding: 5px;">
                    <?php if ($GLOBALS['HTTP_GET_VARS']['obCat'] == 'desc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_bottom.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbCat=SR_category&obCat=asc" >Category</a>
                    <?php elseif ($GLOBALS['HTTP_GET_VARS']['obCat'] == 'asc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_top.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbCat=SR_category&obCat=desc" >Category</a>
                    <?php else: ?>
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbCat=SR_category&obCat=desc" >Category</a>
                    <?php endif; ?>
                </th>

                <th style="padding: 5px;">
                    <?php if ($GLOBALS['HTTP_GET_VARS']['obSCat'] == 'desc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_bottom.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbSCat=SR_subcategory&obSCat=asc" >Sub Category</a>
                    <?php elseif ($GLOBALS['HTTP_GET_VARS']['obSCat'] == 'asc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_top.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbSCat=SR_subcategory&obSCat=desc" >Sub Category</a>
                    <?php else: ?>
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbSCat=SR_subcategory&obSCat=desc" >Sub Category</a>
                    <?php endif; ?>
                </th>

                <th style="padding: 5px;">
                    <?php if ($GLOBALS['HTTP_GET_VARS']['obPri'] == 'desc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_bottom.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbPri=SR_priority&obPri=asc" >Priority</a>
                    <?php elseif ($GLOBALS['HTTP_GET_VARS']['obPri'] == 'asc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_top.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbPri=SR_priority&obPri=desc" >Priority</a>
                    <?php else: ?>
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbPri=SR_priority&obPri=desc" >Priority</a>
                    <?php endif; ?>
                </th>

                <th style="padding: 5px;">
                    <?php if ($GLOBALS['HTTP_GET_VARS']['obStat'] == 'desc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_bottom.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbStat=SR_status&obStat=asc" >Status</a>
                    <?php elseif ($GLOBALS['HTTP_GET_VARS']['obStat'] == 'asc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_top.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbStat=SR_status&obStat=desc" >Status</a>
                    <?php else: ?>
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbStat=SR_status&obStat=desc" >Status</a>
                    <?php endif; ?>
                </th>

                <th style="padding: 5px;">
                    Department
                </th>

                <th style="padding: 5px;">
                    Channel
                </th>

                <th style="padding: 5px;">
                    <?php if ($GLOBALS['HTTP_GET_VARS']['obCbDate'] == 'desc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_bottom.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbCbDate=callbacktime&obCbDate=asc" >Callback Time</a>
                    <?php elseif ($GLOBALS['HTTP_GET_VARS']['obCbDate'] == 'asc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_top.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbCbDate=callbacktime&obCbDate=desc" >Callback Time</a>
                    <?php else: ?>
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbCbDate=callbacktime&obCbDate=desc" >Callback Time</a>
                    <?php endif; ?>
                </th>

                <th style="padding: 5px;">
                    <?php if ($GLOBALS['HTTP_GET_VARS']['obCrDate'] == 'desc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_bottom.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbCrDate=created_date&obCrDate=asc" >Create Date</a>
                    <?php elseif ($GLOBALS['HTTP_GET_VARS']['obCrDate'] == 'asc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_top.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbCrDate=created_date&obCrDate=desc" >Create Date</a>
                    <?php else: ?>
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbCrDate=created_date&obCrDate=desc" >Create Date</a>
                    <?php endif; ?>
                </th>

                <th style="padding: 5px;">
                    Ageing
                </th>

                <th style="padding: 5px;">
                    <?php if ($GLOBALS['HTTP_GET_VARS']['obDuDate'] == 'desc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_bottom.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbDuDate=due_date&obDuDate=asc" >Due Date</a>
                    <?php elseif ($GLOBALS['HTTP_GET_VARS']['obDuDate'] == 'asc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_top.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbDuDate=due_date&obDuDate=desc" >Due Date</a>
                    <?php else: ?>
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbDuDate=due_date&obDuDate=desc" >Due Date</a>
                    <?php endif; ?>
                </th>

                <th style="padding: 5px;">
                    Deviation
                </th>

                <th style="padding: 5px;">
                    <?php if ($GLOBALS['HTTP_GET_VARS']['obRepo'] == 'desc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_bottom.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbRepo=reporter&obRepo=asc" >Creator</a>
                    <?php elseif ($GLOBALS['HTTP_GET_VARS']['obRepo'] == 'asc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_top.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbRepo=reporter&obRepo=desc" >Creator</a>
                    <?php else: ?>
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbRepo=reporter&obRepo=desc" >Creator</a>
                    <?php endif; ?>
                </th>

                <th style="padding: 5px;">Summary</th>

                <th style="padding: 5px;">
                    Updated by
                </th>

                <th style="padding: 5px;">
                    Modified Date
                </th>

                <th style="padding: 5px;">
                    <?php if ($GLOBALS['HTTP_GET_VARS']['obCust'] == 'desc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_bottom.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbCust=customer_email&obCust=asc" >Customer</a>
                    <?php elseif ($GLOBALS['HTTP_GET_VARS']['obCust'] == 'asc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_top.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbCust=customer_email&obCust=desc" >Customer</a>
                    <?php else: ?>
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbCust=customer_email&obCust=desc" >Customer</a>
                    <?php endif; ?>
                </th>

                <th style="padding: 5px;">Name</th>
                <th style="padding: 5px;">Mobile</th>

                <th style="padding: 5px;">
                    <?php if ($GLOBALS['HTTP_GET_VARS']['obClDate'] == 'desc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_bottom.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbClDate=closed_date&obClDate=asc" >Close Date</a>
                    <?php elseif ($GLOBALS['HTTP_GET_VARS']['obClDate'] == 'asc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_top.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbClDate=closed_date&obClDate=desc" >Close Date</a>
                    <?php else: ?>
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbClDate=closed_date&obClDate=desc" >Close Date</a>
                    <?php endif; ?>
                </th>

                <th style="padding: 5px;">
                    <?php if ($GLOBALS['HTTP_GET_VARS']['obResol'] == 'desc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_bottom.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbResol=resolver&obResol=asc" >Resolver</a>
                    <?php elseif ($GLOBALS['HTTP_GET_VARS']['obResol'] == 'asc'): ?>
                        <img width="7" height="6" alt="" src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/r_top.gif">
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbResol=resolver&obResol=desc" >Resolver</a>
                    <?php else: ?>
                        <a href="<?php echo $this->_tpl_vars['url']; ?>
&sbResol=resolver&obResol=desc" >Resolver</a>
                    <?php endif; ?>
                </th>

                <th style="padding: 5px;">Close Summary</th>
                <th style="padding: 5px;">Order</th>
                <th style="padding: 5px;">Order Status</th>
                <th style="padding: 5px;">Payment Method</th>                
                <th style="padding: 5px;">Delivery Ageing</th>
                <th style="padding: 5px;">Warehouse ID</th>
                <th style="padding: 5px;">Courier</th>
                <th style="padding: 5px;">Waybill Number</th>
                <th style="padding: 5px;">Address</th>
                <th style="padding: 5px;">State</th>
                <th style="padding: 5px;">City</th>
                <th style="padding: 5px;">Pin Code</th>
                <th style="padding: 5px;">DC Info</th>
                <th style="padding: 5px;">Refund</th>
                <th style="padding: 5px;">Refund Type</th>
                <th style="padding: 5px;">Refund Val</th>
                <th style="padding: 5px;">Compensation</th>
                <th style="padding: 5px;">Compensation Type</th>
                <th style="padding: 5px;">Comp Value</th>
                <th style="padding: 5px;">Action</th>
            </tr>
            <?php $_from = $this->_tpl_vars['all_sr']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['SR'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['SR']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['index']):
        $this->_foreach['SR']['iteration']++;
?>
                <tr <?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this);?>
>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['SR_id']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['customer_SR_id']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['SR_category']; ?>
</td>
                    <td style="padding: 5px;"><textarea><?php echo $this->_tpl_vars['index']['SR_subcategory']; ?>
</textarea></td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['SR_priority']; ?>
</td>
                    <td style="padding: 5px;">
                        <?php if ($this->_tpl_vars['index']['SR_status'] == 'close'): ?>
                            closed
                        <?php else: ?>
                            <?php echo $this->_tpl_vars['index']['SR_status']; ?>

                        <?php endif; ?>
                    </td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['SR_department']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['SR_channel']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['callbacktime_date']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['created_date']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['ageing']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['due_date']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['deviation']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['reporter']; ?>
</td>
                    <td style="padding: 5px;"><pre><textarea><?php echo $this->_tpl_vars['index']['create_summary']; ?>
</textarea></pre></td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['updatedby']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['modified_date']; ?>
</td>
                    <td style="padding: 5px;">
                        <?php if ($this->_tpl_vars['index']['order_id']): ?>
                            <?php echo $this->_tpl_vars['index']['order_customer_email']; ?>

                        <?php else: ?>
                            <?php echo $this->_tpl_vars['index']['customer_email']; ?>

                        <?php endif; ?>
                    </td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['first_name']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['mobile']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['closed_date']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['resolver']; ?>
</td>
                    <td style="padding: 5px;"><textarea><?php echo $this->_tpl_vars['index']['close_summary']; ?>
</textarea></td>
                    <td style="padding: 5px;">
                        <?php if ($this->_tpl_vars['index']['order_id']): ?>
                            <a href="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/order.php?orderid=<?php echo $this->_tpl_vars['index']['order_id']; ?>
"><?php echo $this->_tpl_vars['index']['order_id']; ?>
</a>
                        <?php else: ?>
                            <?php echo $this->_tpl_vars['index']['order_id']; ?>

                        <?php endif; ?>
                    </td>
                    <td style="padding: 5px;">
                        <?php if ($this->_tpl_vars['index']['order_id']): ?>
                            <a href="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/order.php?orderid=<?php echo $this->_tpl_vars['index']['order_id']; ?>
"><?php echo $this->_tpl_vars['index']['status']; ?>
</a>
                        <?php else: ?>
                            <?php echo $this->_tpl_vars['index']['status']; ?>

                        <?php endif; ?>
                    </td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['payment_method']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['delivery_ageing']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['warehouseid']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['courier_service']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['tracking']; ?>
</td>
                    <td style="padding: 5px;"><textarea><?php echo $this->_tpl_vars['index']['order_address']; ?>
</textarea></td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['order_state']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['order_city']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['order_zipcode']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['DC_info']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['refund']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['refund_type']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['refund_value']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['compensation']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['compensation_type']; ?>
</td>
                    <td style="padding: 5px;"><?php echo $this->_tpl_vars['index']['compensation_value']; ?>
</td>
                    <td style="padding: 5px;">
                    
                        <?php if ($this->_tpl_vars['index']['SR_status'] != 'close'): ?>
                            <?php if (! $this->_tpl_vars['index']['order_id']): ?>
                                <?php if ($this->_tpl_vars['SR_admin']): ?>
                                    <a href="javascript:void(0);"  onClick="javascript:windowOpener('<?php echo $this->_tpl_vars['http_location']; ?>
/admin/ServiceRequest/manageNonOrderSR.php?non_order_sr_id=<?php echo $this->_tpl_vars['index']['id']; ?>
&mode=updateNonOrderServieRequest','<?php echo $this->_tpl_vars['index']['SR_id']; ?>
', 'menubar=0,resizable=1,toolbar=0,width=800,height=500')">Edit</a>
                                    &nbsp;|&nbsp;
                                <?php endif; ?>
                                <a href="javascript:void(0);"  onClick="javascript:windowOpener('<?php echo $this->_tpl_vars['http_location']; ?>
/admin/ServiceRequest/manageNonOrderSR.php?non_order_sr_id=<?php echo $this->_tpl_vars['index']['id']; ?>
&mode=closeNonOrderServieRequest','<?php echo $this->_tpl_vars['index']['SR_id']; ?>
', 'menubar=0,resizable=1,toolbar=0,width=800,height=500')">Close</a>
                                &nbsp;|&nbsp;
                                
                            <?php else: ?>
                                <?php if ($this->_tpl_vars['SR_admin']): ?>
                                    <a href="javascript:void(0);"  onClick="javascript:windowOpener('<?php echo $this->_tpl_vars['http_location']; ?>
/admin/manageOrderSR.php?orderid=<?php echo $this->_tpl_vars['index']['order_id']; ?>
&order_sr_id=<?php echo $this->_tpl_vars['index']['order_sr_id']; ?>
&mode=updateServieRequest','<?php echo $this->_tpl_vars['index']['SR_id']; ?>
', 'menubar=0,resizable=1,toolbar=0,width=800,height=500');">Edit</a>
                                    &nbsp;|&nbsp;
                                <?php endif; ?>
                                <a href="javascript:void(0);"  onClick="javascript:windowOpener('<?php echo $this->_tpl_vars['http_location']; ?>
/admin/manageOrderSR.php?orderid=<?php echo $this->_tpl_vars['index']['order_id']; ?>
&order_sr_id=<?php echo $this->_tpl_vars['index']['order_sr_id']; ?>
&mode=closeServieRequest','<?php echo $this->_tpl_vars['index']['SR_id']; ?>
', 'menubar=0,resizable=1,toolbar=0,width=800,height=500');">Close</a>
                                &nbsp;|&nbsp;
                            <?php endif; ?>
                        <?php endif; ?>
                        
                        <?php if ($this->_tpl_vars['SR_admin']): ?>
                            <?php if (! $this->_tpl_vars['index']['order_id']): ?>
                                <a href="javascript:void(0)" onclick="javascript:deleteNonOrderSR('<?php echo $this->_tpl_vars['index']['id']; ?>
');">Delete</a>
                            <?php else: ?>
                                <a href="javascript:void(0)" onclick="javascript:deleteOrderSR('<?php echo $this->_tpl_vars['index']['order_sr_id']; ?>
');">Delete</a>
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; else: ?>
                <tr>
                    <td colspan="12" align="center">No data is available!</td>
                </tr>
            <?php endif; unset($_from); ?>
        </table>
    </div>

    <table cellpadding="2" cellspacing="1" width="100%">
        <tr>
            <td width="100px">Records In Page:</td>
            <td align="left">
                <select name="pagelimit" onchange="location.href='<?php echo $this->_tpl_vars['url']; ?>
&pg=1&pagelimit='+$(this).val();">
                    <?php unset($this->_sections['idx']);
$this->_sections['idx']['name'] = 'idx';
$this->_sections['idx']['loop'] = is_array($_loop=$this->_tpl_vars['limitarray']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['idx']['show'] = true;
$this->_sections['idx']['max'] = $this->_sections['idx']['loop'];
$this->_sections['idx']['step'] = 1;
$this->_sections['idx']['start'] = $this->_sections['idx']['step'] > 0 ? 0 : $this->_sections['idx']['loop']-1;
if ($this->_sections['idx']['show']) {
    $this->_sections['idx']['total'] = $this->_sections['idx']['loop'];
    if ($this->_sections['idx']['total'] == 0)
        $this->_sections['idx']['show'] = false;
} else
    $this->_sections['idx']['total'] = 0;
if ($this->_sections['idx']['show']):

            for ($this->_sections['idx']['index'] = $this->_sections['idx']['start'], $this->_sections['idx']['iteration'] = 1;
                 $this->_sections['idx']['iteration'] <= $this->_sections['idx']['total'];
                 $this->_sections['idx']['index'] += $this->_sections['idx']['step'], $this->_sections['idx']['iteration']++):
$this->_sections['idx']['rownum'] = $this->_sections['idx']['iteration'];
$this->_sections['idx']['index_prev'] = $this->_sections['idx']['index'] - $this->_sections['idx']['step'];
$this->_sections['idx']['index_next'] = $this->_sections['idx']['index'] + $this->_sections['idx']['step'];
$this->_sections['idx']['first']      = ($this->_sections['idx']['iteration'] == 1);
$this->_sections['idx']['last']       = ($this->_sections['idx']['iteration'] == $this->_sections['idx']['total']);
?>
                        <?php if ($GLOBALS['HTTP_GET_VARS']['pagelimit'] == $this->_tpl_vars['limitarray'][$this->_sections['idx']['index']]): ?>
                            <option value="<?php echo $this->_tpl_vars['limitarray'][$this->_sections['idx']['index']]; ?>
" selected=selected><?php echo $this->_tpl_vars['limitarray'][$this->_sections['idx']['index']]; ?>
</option>
                        <?php else: ?>
                            <option value="<?php echo $this->_tpl_vars['limitarray'][$this->_sections['idx']['index']]; ?>
"><?php echo $this->_tpl_vars['limitarray'][$this->_sections['idx']['index']]; ?>
</option>
                        <?php endif; ?>
                    <?php endfor; endif; ?>
                </select>
            </td>
            <td width="700px" align="center">page <b><?php echo $this->_tpl_vars['pg']; ?>
</b> of <b><?php echo $this->_tpl_vars['totalpages']; ?>
</b>&nbsp;[showing <b><?php echo $this->_tpl_vars['offset_start']; ?>
-<?php echo $this->_tpl_vars['offset_end']; ?>
</b> of <b><?php echo $this->_tpl_vars['total_record']; ?>
]</b></td>            
            <td align="right"><?php echo $this->_tpl_vars['paginator']; ?>
</td>
        </tr>
    </table>

<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'Service Requests','content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>