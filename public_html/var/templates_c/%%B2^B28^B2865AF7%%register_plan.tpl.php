<?php /* Smarty version 2.6.12, created on 2017-03-28 06:18:30
         compiled from partner/main/register_plan.tpl */ ?>
<?php func_load_lang($this, "partner/main/register_plan.tpl","lbl_affiliate_plans,lbl_affiliate_plan,lbl_signup_for_partner_plan"); ?><?php if ($this->_tpl_vars['plans']): ?>

<tr> 
	<td height="20" colspan="3"><b><?php echo $this->_tpl_vars['lng']['lbl_affiliate_plans']; ?>
</b><hr size="1" noshade="noshade" /></td>
</tr>

<?php if ($this->_tpl_vars['usertype'] == 'A' || ( $this->_tpl_vars['usertype'] == 'P' && $this->_tpl_vars['active_modules']['Simple_Mode'] != "" )): ?>

<tr>
	<td align="right"><?php echo $this->_tpl_vars['lng']['lbl_affiliate_plan']; ?>
</td>
	<td>&nbsp;</td>
	<td nowrap="nowrap">
	<select name="plan_id">
<?php $_from = $this->_tpl_vars['plans']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['v']):
?>
		<option value="<?php echo $this->_tpl_vars['v']['plan_id']; ?>
"<?php if ($this->_tpl_vars['userinfo']['plan_id'] == $this->_tpl_vars['v']['plan_id']): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['v']['plan_title']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
	</select>
	</td>
</tr>

<?php else: ?>

<input type="hidden" name="plan_id" value="<?php echo $this->_tpl_vars['userinfo']['plan_id']; ?>
" />

<?php endif; ?>

<tr>
	<td align="right"><?php echo $this->_tpl_vars['lng']['lbl_signup_for_partner_plan']; ?>
</td>
	<td>&nbsp;</td>
	<td nowrap="nowrap">
	<select name="pending_plan_id">
<?php $_from = $this->_tpl_vars['plans']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['v']):
?>
		<option value="<?php echo $this->_tpl_vars['v']['plan_id']; ?>
"<?php if ($this->_tpl_vars['userinfo']['pending_plan_id'] == $this->_tpl_vars['v']['plan_id']): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['v']['plan_title']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
	</select>
	</td>
</tr>

<?php endif; ?>