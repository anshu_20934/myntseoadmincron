<?php /* Smarty version 2.6.12, created on 2017-09-02 23:23:55
         compiled from admin/main/manageBots.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "main/popup_product.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php ob_start(); ?>

<table cellpadding="3" cellspacing="1" width="100%">

	<tr class="TableHead">
		<td align="left">Information</td>
	</tr>
	<tr>
		<td><b>Internet bots</b>, also known as <b>web robots, WWW robots</b> or simply <b>bots</b>, 
		are software applications that run automated tasks over the Internet. <br/>
		Typically, bots perform tasks that are both simple and structurally repetitive, 
		at a much higher rate than would be possible for a human alone. <br/>
		The largest use of bots is in web spidering, in which an automated script fetches, 
		analyzes and files information from web servers at many times the speed of a human.
		</td>
	</tr>
	<tr>
		<td>	
			These bots have been a cause of concern over excess session management data being created on www.myntra.com servers. <br/>
			To avoid such session management issues, we manage a list of such bots from admin settings. <br/><br/>
			<b>Note: </b>Adding bot information here, does not impact SEO functionality.
		</td>
	</tr>

</table>
<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'About this page','content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php ob_start(); ?>

<form name="addNewBot" action="manageBots.php" method="post">
	<input type="hidden" name="mode" id="mode">
	<table cellpadding="3" cellspacing="1" width="60%">

		<tr class="TableHead">
			<td align="left" width="30%">Bot Type</td>
			<td align="left" width="30%">Bot Name</td>
		</tr>
		
		<tr>
			<td><input type="text" name="botType" size="50"/></td>
			<td><input type="text" name="botName" size="50"/></td>
			<td class="SubmitBox" align="left">
			   <input type="submit" value="Add New Bot" onclick="javascript: document.addNewBot.mode.value = 'add_bot'; document.addNewBot.submit();" />
			</td>
		</tr>

	</table>
		
</form>

<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'Add New Bot','content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<br/><br/>

<?php ob_start(); ?>

<form name="remove_bot" action="manageBots.php" method="post">
	<input type="hidden" name="mode" id="mode">
	<table cellpadding="3" cellspacing="1" width="50%">

		<tr class="TableHead">
			<td align="left" width="5%">No.</td>
			<td align="left" width="10%">Bot Type</td>
			<td align="left" width="10%">Bot Name</td>
			<td align="left" width="5%">Remove ?</td>
			
		</tr>
		
		<?php unset($this->_sections['bot_num']);
$this->_sections['bot_num']['name'] = 'bot_num';
$this->_sections['bot_num']['loop'] = is_array($_loop=$this->_tpl_vars['bots']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['bot_num']['show'] = true;
$this->_sections['bot_num']['max'] = $this->_sections['bot_num']['loop'];
$this->_sections['bot_num']['step'] = 1;
$this->_sections['bot_num']['start'] = $this->_sections['bot_num']['step'] > 0 ? 0 : $this->_sections['bot_num']['loop']-1;
if ($this->_sections['bot_num']['show']) {
    $this->_sections['bot_num']['total'] = $this->_sections['bot_num']['loop'];
    if ($this->_sections['bot_num']['total'] == 0)
        $this->_sections['bot_num']['show'] = false;
} else
    $this->_sections['bot_num']['total'] = 0;
if ($this->_sections['bot_num']['show']):

            for ($this->_sections['bot_num']['index'] = $this->_sections['bot_num']['start'], $this->_sections['bot_num']['iteration'] = 1;
                 $this->_sections['bot_num']['iteration'] <= $this->_sections['bot_num']['total'];
                 $this->_sections['bot_num']['index'] += $this->_sections['bot_num']['step'], $this->_sections['bot_num']['iteration']++):
$this->_sections['bot_num']['rownum'] = $this->_sections['bot_num']['iteration'];
$this->_sections['bot_num']['index_prev'] = $this->_sections['bot_num']['index'] - $this->_sections['bot_num']['step'];
$this->_sections['bot_num']['index_next'] = $this->_sections['bot_num']['index'] + $this->_sections['bot_num']['step'];
$this->_sections['bot_num']['first']      = ($this->_sections['bot_num']['iteration'] == 1);
$this->_sections['bot_num']['last']       = ($this->_sections['bot_num']['iteration'] == $this->_sections['bot_num']['total']);
?>
		<?php $this->assign('indexnum', $this->_sections['bot_num']['index']+1); ?>
		<tr> 
        	<td><?php echo $this->_tpl_vars['indexnum']; ?>
</td>
        	<td><?php echo $this->_tpl_vars['bots'][$this->_sections['bot_num']['index']]['bot_type']; ?>
</td>
        	<td><?php echo $this->_tpl_vars['bots'][$this->_sections['bot_num']['index']]['bot_name']; ?>
</td>
        	<td><INPUT TYPE=CHECKBOX NAME="removeBots[]" value="<?php echo $this->_tpl_vars['bots'][$this->_sections['bot_num']['index']]['id']; ?>
"></td>
        	<?php if (!($this->_tpl_vars['indexnum'] % 20)): ?>
        		<td>
        		<input type="submit" value="Remove Bots" 
			   	onclick="javascript: document.remove_bot.mode.value = 'remove_bot'; document.remove_bot.submit();" />
			   	</td>
        	<?php endif; ?>
        </tr>
		<?php endfor; endif; ?>
		
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td class="SubmitBox" align="left">
			   <input type="submit" value="Remove Bots" 
			   	onclick="javascript: document.remove_bot.mode.value = 'remove_bot'; document.remove_bot.submit();" />
			</td>
		</tr>

	</table>
		
</form>

<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'Manage Bots','content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>