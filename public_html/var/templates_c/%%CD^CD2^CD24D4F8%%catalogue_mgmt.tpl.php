<?php /* Smarty version 2.6.12, created on 2017-03-24 12:40:05
         compiled from admin/catalogue_mgmt.tpl */ ?>
<?php ob_start(); ?>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/catalog/search_styles.php" class="VertMenuItems">Search Styles</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/style_bulk_upload.php" class="VertMenuItems">Styles Bulk Upload</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/catalog/styleUploadImages.php" class="VertMenuItems">Style Images Upload</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/catalog/ImageUploadController.php" class="VertMenuItems">New Image Size Chart Uploader</a></li> 
<!-- Display name is kept related to size chart (New Image Size Chart Uploader) because for now it only implements size chart uploader. Later on when it will be used for generic
image upload, this name will be changed to generic image uploader-->

<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/attribute_grouping.php" class="VertMenuItems">Manage Style Groups</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/addfilters.php" class="VertMenuItems">Manage filters</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/filtergroups.php" class="VertMenuItems">Manage filter groups</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/catalog/catalog_classification.php" class="VertMenuItems">Configure Product Categories</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/catalog/attribute_values.php" class="VertMenuItems">Configure Attributes</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/catalog/styleSpecificAttribute.php" class="VertMenuItems">Configure Specific Attributes</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/catalog/display_categories.php" class="VertMenuItems">Display Categories</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/catalog/photoshoot_issue_rules.php" class="VertMenuItems">Photoshoot Issue Rule</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/vtr_enable_disable_styles.php" class="VertMenuItems">VTR-Enable/Disable Style</a></li>
<hr/>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/size_unification_admin_new.php" class="VertMenuItems">Size Unification(style-wise)</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/catalog/sizeunification/size_scales_unif.php" class="VertMenuItems">Size Scales</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/catalog/sizeunification/size_mapping_unif.php" class="VertMenuItems">Size Mappings</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/catalog/sizeunification/size_measurement_unif.php" class="VertMenuItems">Size Measurements</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/manual_style_solr_indexing.php" class="VertMenuItems">Manual Style Solr Indexing</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/catalog/timer.php" class="VertMenuItems">Timer</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/catalog/timerMeta.php" class="VertMenuItems">Timer Meta Data Update</a></li>
<?php $this->_smarty_vars['capture']['menu'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu.tpl", 'smarty_include_vars' => array('dingbats' => "dingbats_categorie.gif",'menu_title' => 'Catalog Mgmt','menu_content' => $this->_smarty_vars['capture']['menu'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>