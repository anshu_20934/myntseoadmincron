<?php /* Smarty version 2.6.12, created on 2017-03-27 12:39:15
         compiled from admin/main/catalog_classification.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'admin/main/catalog_classification.tpl', 66, false),array('modifier', 'escape', 'admin/main/catalog_classification.tpl', 270, false),)), $this); ?>
<?php func_load_lang($this, "admin/main/catalog_classification.tpl","txt_featured_products,lbl_yes,lbl_no,lbl_yes,lbl_no,lbl_yes,lbl_no,lbl_yes,lbl_no,lbl_yes,lbl_no,lbl_add,lbl_yes,lbl_no,lbl_add,lbl_yes,lbl_no,lbl_yes,lbl_no,lbl_add"); ?><base href="<?php echo $this->_tpl_vars['http_location']; ?>
/">
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "main/popup_product.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<a name="featured" />
<?php echo '
<style type="text/css">
.article_table {font-size:14px;}
.article_table tr td {padding:10px}
.article_table tr td input,select{font-size:11px;line-height:12px}
.error_block{
 	background:red;
 }
</style>
 <script language="Javascript">
	function checkForDuplicateFilterOrder(){
		var arr_order=[];
		var no_error=true;
		$(\'.data_row\').each(function(index) {
				var dataRow = $(this);
				if($(this).find(":checkbox").attr(\'checked\')){
					if(jQuery.inArray($(this).find(\'.filter_order\').val(),arr_order) != -1){
						alert(\'filter order:\'+$(this).find(\'.filter_order\').val()+\' already exist. Please specify different orders\');
						no_error=false;
						jQuery(dataRow).addClass(\'error_block\');
						return false;
					}
					arr_order.push($(this).find(\'.filter_order\').val());
				}
			});
		return no_error;
		}
</script>
'; ?>


<?php echo $this->_tpl_vars['lng']['txt_featured_products']; ?>


<br /><br />

<?php ob_start(); ?>

<form action="<?php echo $this->_tpl_vars['form_action']; ?>
" method="post" name="groupfrm" enctype="multipart/form-data" onsubmit="return checkForDuplicateFilterOrder();">
<input type="hidden" name="mode"  />
<table width="100%">
<tr>
<td colspan="5"><br /><br /><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => 'Article Types')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>
</table>

<table cellpadding="3" cellspacing="1" width="100%" class="article_table">

<tr class="TableHead">
	<td width="10%">&nbsp;</td>
	<td width="10%">Type Name</td>
	<td width="10%">Type Code</td>
	<td width="15%" align="center">Sub Category</td>
	<td width="15%">Master Category</td>
	<td width="10%">filter order</td>
	<td width="10%" align="center">Active</td>
	<td width="10%" align="center">Enable Social Sharing</td>
</tr>

<?php if ($this->_tpl_vars['article_types']): ?>

<?php unset($this->_sections['grid']);
$this->_sections['grid']['name'] = 'grid';
$this->_sections['grid']['loop'] = is_array($_loop=$this->_tpl_vars['article_types']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['grid']['show'] = true;
$this->_sections['grid']['max'] = $this->_sections['grid']['loop'];
$this->_sections['grid']['step'] = 1;
$this->_sections['grid']['start'] = $this->_sections['grid']['step'] > 0 ? 0 : $this->_sections['grid']['loop']-1;
if ($this->_sections['grid']['show']) {
    $this->_sections['grid']['total'] = $this->_sections['grid']['loop'];
    if ($this->_sections['grid']['total'] == 0)
        $this->_sections['grid']['show'] = false;
} else
    $this->_sections['grid']['total'] = 0;
if ($this->_sections['grid']['show']):

            for ($this->_sections['grid']['index'] = $this->_sections['grid']['start'], $this->_sections['grid']['iteration'] = 1;
                 $this->_sections['grid']['iteration'] <= $this->_sections['grid']['total'];
                 $this->_sections['grid']['index'] += $this->_sections['grid']['step'], $this->_sections['grid']['iteration']++):
$this->_sections['grid']['rownum'] = $this->_sections['grid']['iteration'];
$this->_sections['grid']['index_prev'] = $this->_sections['grid']['index'] - $this->_sections['grid']['step'];
$this->_sections['grid']['index_next'] = $this->_sections['grid']['index'] + $this->_sections['grid']['step'];
$this->_sections['grid']['first']      = ($this->_sections['grid']['iteration'] == 1);
$this->_sections['grid']['last']       = ($this->_sections['grid']['iteration'] == $this->_sections['grid']['total']);
?>

<tr class="<?php echo smarty_function_cycle(array('values' => "data_row ,data_row TableSubHead"), $this);?>
">
    <input type="hidden" name="posted_data[<?php echo $this->_tpl_vars['article_types'][$this->_sections['grid']['index']]['id']; ?>
][id]" value=<?php echo $this->_tpl_vars['article_types'][$this->_sections['grid']['index']]['id']; ?>
 />
	<td align="center"><input type="checkbox" name="posted_data[<?php echo $this->_tpl_vars['article_types'][$this->_sections['grid']['index']]['id']; ?>
][to_delete]" /></td>
	<td align="center"><input type="text" name="posted_data[<?php echo $this->_tpl_vars['article_types'][$this->_sections['grid']['index']]['id']; ?>
][typename]" value="<?php echo $this->_tpl_vars['article_types'][$this->_sections['grid']['index']]['typename']; ?>
" /></td>
	<td align="center"><input type="text" name="posted_data[<?php echo $this->_tpl_vars['article_types'][$this->_sections['grid']['index']]['id']; ?>
][typecode]" value="<?php echo $this->_tpl_vars['article_types'][$this->_sections['grid']['index']]['typecode']; ?>
" /></td>
	<td align="center">
	<select name="posted_data[<?php echo $this->_tpl_vars['article_types'][$this->_sections['grid']['index']]['id']; ?>
][parent1]" onchange="selectMasterCat(this.value,'article_sec_master_cat')">
		<option value="-1">NA</option>
		<?php $_from = $this->_tpl_vars['sub_categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['types']):
?>
		    <option value="<?php echo $this->_tpl_vars['types']['id']; ?>
" <?php if ($this->_tpl_vars['article_types'][$this->_sections['grid']['index']]['parent1'] == $this->_tpl_vars['types']['id']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['types']['typename']; ?>
</option>
		<?php endforeach; endif; unset($_from); ?>

	</select>
	</td>
	<td align="center">
	<select name="posted_data[<?php echo $this->_tpl_vars['article_types'][$this->_sections['grid']['index']]['id']; ?>
][parent2]" id="article_sec_master_cat">
		<option value="-1">NA</option>
		<?php $_from = $this->_tpl_vars['master_categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['types']):
?>
		    <option value="<?php echo $this->_tpl_vars['types']['id']; ?>
" <?php if ($this->_tpl_vars['article_types'][$this->_sections['grid']['index']]['parent2'] == $this->_tpl_vars['types']['id']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['types']['typename']; ?>
</option>
		<?php endforeach; endif; unset($_from); ?>

	</select>
	</td>
	<td align="center"><input type="text" class="filter_order" name="posted_data[<?php echo $this->_tpl_vars['article_types'][$this->_sections['grid']['index']]['id']; ?>
][filter_order]" value="<?php echo $this->_tpl_vars['article_types'][$this->_sections['grid']['index']]['filter_order']; ?>
" /></td>
	<td align="center">
	<select name="posted_data[<?php echo $this->_tpl_vars['article_types'][$this->_sections['grid']['index']]['id']; ?>
][is_active]">
		<option value="1"<?php if ($this->_tpl_vars['article_types'][$this->_sections['grid']['index']]['is_active'] == '1'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_yes']; ?>
</option>
		<option value="0"<?php if ($this->_tpl_vars['article_types'][$this->_sections['grid']['index']]['is_active'] == '0'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_no']; ?>
</option>
	</select>
	</td>
	<td align="center">
	<select name="posted_data[<?php echo $this->_tpl_vars['article_types'][$this->_sections['grid']['index']]['id']; ?>
][enable_social_sharing]">
		<option value="1"<?php if ($this->_tpl_vars['article_types'][$this->_sections['grid']['index']]['enable_social_sharing'] == '1'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_yes']; ?>
</option>
		<option value="0"<?php if ($this->_tpl_vars['article_types'][$this->_sections['grid']['index']]['enable_social_sharing'] == '0'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_no']; ?>
</option>
	</select>
	</td>
</tr>

<?php endfor; endif; ?>

<?php else: ?>

<tr>
 <td colspan="4" align="center">No Article types available.</td>
</tr>
<?php endif; ?>



<tr>
	<td colspan="6" class="SubmitBox" align="center">
	<input type="button" value="Delete Selected" onclick="return confirmDelete();" />
	<input type="submit" value="Update Selected" onclick="javascript: document.groupfrm.mode.value = 'update'; " />
    <input type="button" value="Add Article Type" onclick="showDialog('addArticle')" />
	</td>
</tr>


</table>
<table width="100%">
<tr>
<td colspan="5"><br /><br /><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => 'Sub Categories')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>
</table>
<!-- subcategory starts here -->
<table cellpadding="3" cellspacing="1" width="100%" class="article_table">

<tr class="TableHead">
	<td width="10%">&nbsp;</td>
	<td width="20%">Sub Category Name</td>
	<td width="20%">Master Category</td>
	<td width="10%" align="center">Active</td>
</tr>

<?php if ($this->_tpl_vars['sub_categories']): ?>

<?php unset($this->_sections['grid']);
$this->_sections['grid']['name'] = 'grid';
$this->_sections['grid']['loop'] = is_array($_loop=$this->_tpl_vars['sub_categories']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['grid']['show'] = true;
$this->_sections['grid']['max'] = $this->_sections['grid']['loop'];
$this->_sections['grid']['step'] = 1;
$this->_sections['grid']['start'] = $this->_sections['grid']['step'] > 0 ? 0 : $this->_sections['grid']['loop']-1;
if ($this->_sections['grid']['show']) {
    $this->_sections['grid']['total'] = $this->_sections['grid']['loop'];
    if ($this->_sections['grid']['total'] == 0)
        $this->_sections['grid']['show'] = false;
} else
    $this->_sections['grid']['total'] = 0;
if ($this->_sections['grid']['show']):

            for ($this->_sections['grid']['index'] = $this->_sections['grid']['start'], $this->_sections['grid']['iteration'] = 1;
                 $this->_sections['grid']['iteration'] <= $this->_sections['grid']['total'];
                 $this->_sections['grid']['index'] += $this->_sections['grid']['step'], $this->_sections['grid']['iteration']++):
$this->_sections['grid']['rownum'] = $this->_sections['grid']['iteration'];
$this->_sections['grid']['index_prev'] = $this->_sections['grid']['index'] - $this->_sections['grid']['step'];
$this->_sections['grid']['index_next'] = $this->_sections['grid']['index'] + $this->_sections['grid']['step'];
$this->_sections['grid']['first']      = ($this->_sections['grid']['iteration'] == 1);
$this->_sections['grid']['last']       = ($this->_sections['grid']['iteration'] == $this->_sections['grid']['total']);
?>

<tr<?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this);?>
>
    <input type="hidden" name="posted_data[<?php echo $this->_tpl_vars['sub_categories'][$this->_sections['grid']['index']]['id']; ?>
][id]" value=<?php echo $this->_tpl_vars['sub_categories'][$this->_sections['grid']['index']]['id']; ?>
 />
	<td align="center"><input type="checkbox" name="posted_data[<?php echo $this->_tpl_vars['sub_categories'][$this->_sections['grid']['index']]['id']; ?>
][to_delete]" /></td>
	<td align="center"><input type="text" name="posted_data[<?php echo $this->_tpl_vars['sub_categories'][$this->_sections['grid']['index']]['id']; ?>
][typename]" value="<?php echo $this->_tpl_vars['sub_categories'][$this->_sections['grid']['index']]['typename']; ?>
" /></td>
	<td align="center">
    <select name="disabled_master_category" disabled="disabled" >
    		<?php $_from = $this->_tpl_vars['master_categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['types']):
?>
    		    <option value="<?php echo $this->_tpl_vars['types']['id']; ?>
" <?php if ($this->_tpl_vars['sub_categories'][$this->_sections['grid']['index']]['parent1'] == $this->_tpl_vars['types']['id']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['types']['typename']; ?>
</option>
    		<?php endforeach; endif; unset($_from); ?>

    	</select>

	<select name="posted_data[<?php echo $this->_tpl_vars['sub_categories'][$this->_sections['grid']['index']]['id']; ?>
][parent1]" style="display:none;">
		<?php $_from = $this->_tpl_vars['master_categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['types']):
?>
		    <option value="<?php echo $this->_tpl_vars['types']['id']; ?>
" <?php if ($this->_tpl_vars['sub_categories'][$this->_sections['grid']['index']]['parent1'] == $this->_tpl_vars['types']['id']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['types']['typename']; ?>
</option>
		<?php endforeach; endif; unset($_from); ?>

	</select>

	</td>
	<td align="center">
	<select name="posted_data[<?php echo $this->_tpl_vars['sub_categories'][$this->_sections['grid']['index']]['id']; ?>
][is_active]" >
		<option value="1"<?php if ($this->_tpl_vars['sub_categories'][$this->_sections['grid']['index']]['is_active'] == '1'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_yes']; ?>
</option>
		<option value="0"<?php if ($this->_tpl_vars['sub_categories'][$this->_sections['grid']['index']]['is_active'] == '0'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_no']; ?>
</option>
	</select>
	</td>
</tr>

<?php endfor; endif; ?>

<?php else: ?>

<tr>
 <td colspan="4" align="center">No Sub Categories available.</td>
</tr>
<?php endif; ?>



<tr>
	<td colspan="5" class="SubmitBox" align="center">
	<input type="button" value="Delete Selected" onclick="return confirmDelete();" />
	<input type="submit" value="Update Selected" onclick="javascript: document.groupfrm.mode.value = 'update'; document.groupfrm.submit();" />
    <input type="button" value="Add Sub Category" onclick="showDialog('addSubCategory')" />
	</td>
</tr>


</table>
<table width="100%">
<tr>
<td colspan="5"><br /><br /><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => 'Master Categories')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>
</table>
<!-- subcategory starts here -->
<table cellpadding="3" cellspacing="1" width="100%" class="article_table">

<tr class="TableHead">
	<td width="10%">&nbsp;</td>
	<td width="20%">Master Category</td>
	<td width="10%" align="center">Active</td>
</tr>

<?php if ($this->_tpl_vars['master_categories']): ?>

<?php unset($this->_sections['grid']);
$this->_sections['grid']['name'] = 'grid';
$this->_sections['grid']['loop'] = is_array($_loop=$this->_tpl_vars['master_categories']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['grid']['show'] = true;
$this->_sections['grid']['max'] = $this->_sections['grid']['loop'];
$this->_sections['grid']['step'] = 1;
$this->_sections['grid']['start'] = $this->_sections['grid']['step'] > 0 ? 0 : $this->_sections['grid']['loop']-1;
if ($this->_sections['grid']['show']) {
    $this->_sections['grid']['total'] = $this->_sections['grid']['loop'];
    if ($this->_sections['grid']['total'] == 0)
        $this->_sections['grid']['show'] = false;
} else
    $this->_sections['grid']['total'] = 0;
if ($this->_sections['grid']['show']):

            for ($this->_sections['grid']['index'] = $this->_sections['grid']['start'], $this->_sections['grid']['iteration'] = 1;
                 $this->_sections['grid']['iteration'] <= $this->_sections['grid']['total'];
                 $this->_sections['grid']['index'] += $this->_sections['grid']['step'], $this->_sections['grid']['iteration']++):
$this->_sections['grid']['rownum'] = $this->_sections['grid']['iteration'];
$this->_sections['grid']['index_prev'] = $this->_sections['grid']['index'] - $this->_sections['grid']['step'];
$this->_sections['grid']['index_next'] = $this->_sections['grid']['index'] + $this->_sections['grid']['step'];
$this->_sections['grid']['first']      = ($this->_sections['grid']['iteration'] == 1);
$this->_sections['grid']['last']       = ($this->_sections['grid']['iteration'] == $this->_sections['grid']['total']);
?>

<tr<?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this);?>
>
    <input type="hidden" name="posted_data[<?php echo $this->_tpl_vars['master_categories'][$this->_sections['grid']['index']]['id']; ?>
][id]" value=<?php echo $this->_tpl_vars['master_categories'][$this->_sections['grid']['index']]['id']; ?>
 />
	<td align="center"><input type="checkbox" name="posted_data[<?php echo $this->_tpl_vars['master_categories'][$this->_sections['grid']['index']]['id']; ?>
][to_delete]" /></td>
	<td align="center"><input type="text" name="posted_data[<?php echo $this->_tpl_vars['master_categories'][$this->_sections['grid']['index']]['id']; ?>
][typename]" value="<?php echo $this->_tpl_vars['master_categories'][$this->_sections['grid']['index']]['typename']; ?>
" /></td>
	<td align="center">
	<select name="posted_data[<?php echo $this->_tpl_vars['master_categories'][$this->_sections['grid']['index']]['id']; ?>
][is_active]">
		<option value="1"<?php if ($this->_tpl_vars['master_categories'][$this->_sections['grid']['index']]['is_active'] == '1'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_yes']; ?>
</option>
		<option value="0"<?php if ($this->_tpl_vars['master_categories'][$this->_sections['grid']['index']]['is_active'] == '0'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_no']; ?>
</option>
	</select>
	</td>
</tr>

<?php endfor; endif; ?>

<?php else: ?>

<tr>
 <td colspan="4" align="center">No Master Categories Available.</td>
</tr>
<?php endif; ?>



<tr>
	<td colspan="5" class="SubmitBox" align="center">
	<input type="button" value="Delete Selected" onclick="return confirmDelete();" />
	<input type="submit" value="Update Selected" onclick="javascript: document.groupfrm.mode.value = 'update'; document.groupfrm.submit();" />
    <input type="button" value="Add Master Category" onclick="showDialog('addMasterCategory')" />
	</td>
</tr>


</table>
</form>
<div id="addMasterCategory" style="display:none">
<form name="frmG" action="<?php echo $this->_tpl_vars['form_action']; ?>
" method="post" enctype="multipart/form-data" id="master_form">
<input type="hidden" name="mode" value="addgroup" />
<input type="hidden" name="addtype" value="master" />
<table cellpadding="3" cellspacing="1" width="100%" class="article_table">

   <tr>
        <td><h2>Add Master Category</h2></td>
        <td>&nbsp;</td>
   </tr>
   <tr>
        <td>Type Name </td>
        <td><input type="text" name="typename" value="" style="width: 150px;" id="master_typename"/></td>
   </tr>
   <tr>
          <td>Active</td>
             <td>
                <select name="is_active" style="width: 150px;">
                    <option value="1" ><?php echo $this->_tpl_vars['lng']['lbl_yes']; ?>
</option>
                    <option value="0"><?php echo $this->_tpl_vars['lng']['lbl_no']; ?>
</option>
                </select>
        	</td>
   </tr>
   <tr>
   <td colspan="2" align="center">
   <input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_add'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" name="sbtFrm" style="padding:5px" onclick="addArticle('master')"/>
   &nbsp;&nbsp;&nbsp;<input type="button" onclick="cancel_dialog('addMasterCategory')" style="padding:5px" value="Cancel"/>
   </td>
   </tr>

</table>
</form>
</div>

<div id="addSubCategory" style="display:none">
<form name="frmG" action="<?php echo $this->_tpl_vars['form_action']; ?>
" method="post" enctype="multipart/form-data" id="sub_form">
<input type="hidden" name="mode" value="addgroup" />
<input type="hidden" name="addtype" value="sub" />
<table cellpadding="3" cellspacing="1" width="100%" class="article_table">

   <tr>
        <td><h2>Add Sub Category</h2></td>
        <td>&nbsp;</td>
   </tr>
   <tr>
        <td>Type Name </td>
        <td><input type="text" name="typename" value=""  id="sub_typename" style="width: 150px;"/></td>
   </tr>
   <tr>
        <td>Master Category</td>
        <td >
        <select name="parent1" id="sub_parent1" style="width: 150px;">
            <option value="-1">NA</option>
            <?php $_from = $this->_tpl_vars['master_categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['types']):
?>
                <option value="<?php echo $this->_tpl_vars['types']['id']; ?>
"><?php echo $this->_tpl_vars['types']['typename']; ?>
</option>
		    <?php endforeach; endif; unset($_from); ?>
        </select>
        </td>
   </tr>
   <tr>
          <td>Active</td>
             <td>
                <select name="is_active" style="width: 150px;">
                    <option value="1" ><?php echo $this->_tpl_vars['lng']['lbl_yes']; ?>
</option>
                    <option value="0"><?php echo $this->_tpl_vars['lng']['lbl_no']; ?>
</option>
                </select>
        	</td>
   </tr>
   <tr>
   <td colspan="2" align="center">
   <input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_add'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" name="sbtFrm" style="padding:5px" onclick="addArticle('sub')" />
   &nbsp;&nbsp;&nbsp;<input type="button" onclick="cancel_dialog('addSubCategory')" style="padding:5px" value="Cancel"/>
   </td>
   </tr>

</table>
</form>
</div>
<div id="addArticle" style="display:none">
<form name="frmG" action="<?php echo $this->_tpl_vars['form_action']; ?>
" method="post" enctype="multipart/form-data" id="article_form">
<input type="hidden" name="mode" value="addgroup" />
<input type="hidden" name="addtype" value="article" />
<table cellpadding="3" cellspacing="1" width="100%" class="article_table">
   
   <tr>
        <td><h2>Add Article</h2></td>
        <td>&nbsp;</td>
   </tr>
   <tr>
        <td>Type Name </td>
        <td><input type="text" name="typename" value="" id="article_typename" style="width: 150px;" /></td>
   </tr>
   <tr>
        <td>Type Code </td>
        <td><input type="text" name="typecode" value="" style="width: 150px;" /></td>
   </tr>
   <tr>
        <td>Sub Category</td>
        <td >
        <select name="parent1" id="article_parent1" style="width: 150px;" onchange="selectMasterCat(this.value,'article_parent2')">
            <option value="-1">NA</option>
            <?php $_from = $this->_tpl_vars['sub_categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['types']):
?>
                <option value="<?php echo $this->_tpl_vars['types']['id']; ?>
"><?php echo $this->_tpl_vars['types']['typename']; ?>
</option>
		    <?php endforeach; endif; unset($_from); ?>
        </select>
        </td>
   </tr>
   <tr>
        <td>Master Category</td>
             <td>
             <select name="parent2" id="article_parent2" style="width: 150px;">
        		<option value="-1">NA</option>
        		<?php $_from = $this->_tpl_vars['master_categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['types']):
?>
        		    <option value="<?php echo $this->_tpl_vars['types']['id']; ?>
"><?php echo $this->_tpl_vars['types']['typename']; ?>
</option>
        		<?php endforeach; endif; unset($_from); ?>

        	</select>
             </td>
   </tr>
   <tr>
        <td>Filter Order </td>
        <td><input type="text" name="filter_order" value="" style="width: 150px;" /></td>
   </tr>
   <tr>
          <td>Active</td>
             <td>
                <select name="is_active" style="width: 150px;">
                    <option value="1" ><?php echo $this->_tpl_vars['lng']['lbl_yes']; ?>
</option>
                    <option value="0"><?php echo $this->_tpl_vars['lng']['lbl_no']; ?>
</option>
                </select>
        	</td>
   </tr>
   <tr>
          <td>Enable Social Sharing</td>
             <td>
                <select name="enable_social_sharing" style="width: 150px;">
                    <option value="1" ><?php echo $this->_tpl_vars['lng']['lbl_yes']; ?>
</option>
                    <option value="0"><?php echo $this->_tpl_vars['lng']['lbl_no']; ?>
</option>
                </select>
        	</td>
   </tr>
   <tr>
   <td colspan="2" align="center">
   <input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_add'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" style="padding:5px" name="sbtFrm" onclick="addArticle('article')" />
   &nbsp;&nbsp;&nbsp;<input type="button" onclick="cancel_dialog('addArticle')" style="padding:5px"  value="Cancel"/>
   </td>
   </tr>

</table>
</form>
</div>
<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<script
	type="text/javascript"
	src="<?php echo $this->_tpl_vars['http_location']; ?>
/skin1/myntra_js/jquery.min.js"></script>
<link
	rel="stylesheet" type="text/css"
	href="<?php echo $this->_tpl_vars['http_location']; ?>
/skin1/myntra_css/thickbox3.css" />
<script
	type='text/javascript'
	src='<?php echo $this->_tpl_vars['http_location']; ?>
/skin1/myntra_js/thickbox3.js'></script>
<?php echo '
  <script language="Javascript">
      function confirmDelete()
      {
          var flag = confirm(\'Are you sure you want to delete?\')
          if(!flag)
          {
             return false;
          }
          else
          {
          	 document.groupfrm.mode.value = \'delete\';
          	 document.groupfrm.submit();
          	 return true;
          }
      }
    function showDialog(dg){
	$("#"+dg).css("display","block");
	tb_show("","#TB_inline?height=350&amp;width=500&amp;inlineId="+dg+";modal=true","");
	}
	function cancel_dialog(dg){
	$("#"+dg).css("display","none");
	tb_remove();
}
function addArticle(id)
{
    if(id==\'article\')
    {
        if($.trim($("#article_typename").val()) ==\'\')
        {
            alert("Please enter Type Name ");
            return false;
        }
        else if($.trim($("#article_parent1").val()) ==\'-1\')
        {
            alert("Please Select Sub Category");
            return false;
        }
        else if($.trim($("#article_parent2").val()) ==\'-1\')
        {
            alert("Please Select Master Category");
            return false;
        }
        else
            $("#article_form").trigger("submit");
    }
    else if(id == \'sub\')
    {
        if($.trim($("#sub_typename").val()) ==\'\')
        {
            alert("Please enter Type Name ");
            return false;
        }
        else if($.trim($("#sub_parent1").val()) ==\'-1\')
        {
            alert("Please Select Master Category");
            return false;
        }
        else
            $("#sub_form").trigger("submit");
    }
    else if(id ==\'master\')
    {
        if($.trim($("#master_typename").val()) ==\'\')
        {
            alert("Please enter Type Name ");
            return false;
        }
        else
            $("#master_form").trigger("submit");
    }
}
function selectMasterCat(val,id)
{
    $("#"+id).val(sub_category[val]);
}
var sub_category = new Array();
  </script>
'; ?>

<script type="text/javascript">
<?php $_from = $this->_tpl_vars['sub_categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['types']):
?>
    sub_category['<?php echo $this->_tpl_vars['types']['id']; ?>
']= '<?php echo $this->_tpl_vars['types']['parent1']; ?>
';
<?php endforeach; endif; unset($_from); ?>
</script>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'Catalog Classification','content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<br /><br />

