<?php /* Smarty version 2.6.12, created on 2017-03-24 12:21:47
         compiled from menu.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'menu.tpl', 53, false),array('modifier', 'default', 'menu.tpl', 60, false),)), $this); ?>
<?php if ($this->_tpl_vars['usertype'] == 'C'): ?> 
<table cellpadding="0" cellspacing="0" width="100%">
<tr>
	<td width="100%">
	
	<table cellpadding="0" cellspacing="0" width="100%" class="VertMenuTitleBox">
	<tr>
		<td class="VertMenuTitleBorder" valign="top"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="1" height="22" alt="" /></td>
		<td class="VertMenuTitleShadow" align="center" width="100%">
		<?php if ($this->_tpl_vars['link_href']): ?>
		<a href="<?php echo $this->_tpl_vars['link_href']; ?>
"><font class="VertMenuTitle"><?php echo $this->_tpl_vars['menu_title']; ?>
</font></a>
		<?php else: ?>
		<font class="VertMenuTitle"><?php echo $this->_tpl_vars['menu_title']; ?>
</font>
		<?php endif; ?>
		</td>
		<td class="VertMenuTitleBorder" valign="top"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="1" height="22" alt="" /></td>
	</tr>
	<tr>
		<td class="VertMenuBottomBorder" valign="top"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="1" height="2" alt="" /></td>
		<td class="VertMenuBottom" valign="top"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="1" height="1" alt="" /></td>
		<td class="VertMenuBottomBorder" valign="top"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="1" height="2" alt="" /></td>
	</tr>
	</table>
	
	</td>
</tr>
<tr>
	<td><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="1" height="3" alt="" /></td>
</tr>
<tr>
	<td class="VertMenuBox">
	
	<table <?php if ($this->_tpl_vars['menu_style'] == 'categories'): ?> cellspacing="0" cellpadding="0"<?php else: ?>cellpadding="5" cellspacing="5"<?php endif; ?> width="100%">
	<tr>
		<td><?php echo $this->_tpl_vars['menu_content']; ?>
</td>
	</tr>
    </table>
	
	</td>
</tr>
<tr>
	<td><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="1" height="4" alt="" /></td>
</tr>
</table>

<?php else: ?> 
<table cellspacing="1" width="100%" class="VertMenuBorder">
<tr>
<td class="VertMenuTitle">
<table cellspacing="0" cellpadding="0" width="100%"><tr>
<td><?php echo $this->_tpl_vars['link_begin']; ?>
<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/<?php if ($this->_tpl_vars['dingbats'] != ''):  echo $this->_tpl_vars['dingbats'];  else: ?>spacer.gif<?php endif; ?>" class="VertMenuTitleIcon" alt="<?php echo ((is_array($_tmp=$this->_tpl_vars['menu_title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" /><?php echo $this->_tpl_vars['link_end']; ?>
</td>
<td width="100%"><?php if ($this->_tpl_vars['link_href']): ?><a href="<?php echo $this->_tpl_vars['link_href']; ?>
"><?php endif; ?><font class="VertMenuTitle"><?php echo $this->_tpl_vars['menu_title']; ?>
</font><?php if ($this->_tpl_vars['link_href']): ?></a><?php endif; ?></td>
</tr></table>
</td>
</tr>
<tr> 
<td class="VertMenuBox">
<table cellpadding="<?php echo ((is_array($_tmp=@$this->_tpl_vars['cellpadding'])) ? $this->_run_mod_handler('default', true, $_tmp, '5') : smarty_modifier_default($_tmp, '5')); ?>
" cellspacing="0" width="100%">
<tr><td><?php echo $this->_tpl_vars['menu_content']; ?>
<br /></td></tr>
</table>
</td></tr>
</table>

<?php endif; ?> 