<?php /* Smarty version 2.6.12, created on 2017-03-30 01:18:31
         compiled from admin/main/set_sorting_order.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'admin/main/set_sorting_order.tpl', 52, false),array('modifier', 'escape', 'admin/main/set_sorting_order.tpl', 74, false),)), $this); ?>
<?php func_load_lang($this, "admin/main/set_sorting_order.tpl","txt_featured_products,lbl_add_product,lbl_update"); ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "main/popup_product.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
  echo '
  <script language="Javascript">
      function confirmDelete()
      {
          var flag = confirm(\'Are you sure you want to delete?\')
          if(!flag)
          {
             return false;
          }
          else
          {
          	 document.groupfrm.mode.value = \'delete\';
          	 document.groupfrm.submit();
          	 return true;
          }
      }
	function addPriority()
	{
		var w = document.frmG.name.selectedIndex;
		var selected_text = document.frmG.name.options[w].text;
		 document.frmG.label.value = selected_text;
		document.frmG.submit();
	}
  </script>
'; ?>

<a name="featured" />

<?php echo $this->_tpl_vars['lng']['txt_featured_products']; ?>


<br /><br />

<?php ob_start(); ?>

<form action="<?php echo $this->_tpl_vars['form_action']; ?>
" method="post" name="groupfrm" enctype="multipart/form-data">
<input type="hidden" name="mode"  />

<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="20%">Name</td>
	<td width="20%">Weightage</td>
	<td width="20%" align="center">Cutoff days</td>

</tr>

<?php if ($this->_tpl_vars['transients']): ?>

<?php unset($this->_sections['grid']);
$this->_sections['grid']['name'] = 'grid';
$this->_sections['grid']['loop'] = is_array($_loop=$this->_tpl_vars['transients']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['grid']['show'] = true;
$this->_sections['grid']['max'] = $this->_sections['grid']['loop'];
$this->_sections['grid']['step'] = 1;
$this->_sections['grid']['start'] = $this->_sections['grid']['step'] > 0 ? 0 : $this->_sections['grid']['loop']-1;
if ($this->_sections['grid']['show']) {
    $this->_sections['grid']['total'] = $this->_sections['grid']['loop'];
    if ($this->_sections['grid']['total'] == 0)
        $this->_sections['grid']['show'] = false;
} else
    $this->_sections['grid']['total'] = 0;
if ($this->_sections['grid']['show']):

            for ($this->_sections['grid']['index'] = $this->_sections['grid']['start'], $this->_sections['grid']['iteration'] = 1;
                 $this->_sections['grid']['iteration'] <= $this->_sections['grid']['total'];
                 $this->_sections['grid']['index'] += $this->_sections['grid']['step'], $this->_sections['grid']['iteration']++):
$this->_sections['grid']['rownum'] = $this->_sections['grid']['iteration'];
$this->_sections['grid']['index_prev'] = $this->_sections['grid']['index'] - $this->_sections['grid']['step'];
$this->_sections['grid']['index_next'] = $this->_sections['grid']['index'] + $this->_sections['grid']['step'];
$this->_sections['grid']['first']      = ($this->_sections['grid']['iteration'] == 1);
$this->_sections['grid']['last']       = ($this->_sections['grid']['iteration'] == $this->_sections['grid']['total']);
?>

<tr<?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this);?>
>
    <input type="hidden" name="posted_data[<?php echo $this->_tpl_vars['transients'][$this->_sections['grid']['index']]['action']; ?>
][gid]" value=<?php echo $this->_tpl_vars['products'][$this->_sections['grid']['index']]['action']; ?>
 />
	<td align="center"><?php echo $this->_tpl_vars['transients'][$this->_sections['grid']['index']]['action']; ?>
</td>
    <td align="center"><input type="text" name="posted_data[<?php echo $this->_tpl_vars['transients'][$this->_sections['grid']['index']]['action']; ?>
][weightage]" value="<?php echo $this->_tpl_vars['transients'][$this->_sections['grid']['index']]['weightage']; ?>
"  size="2" /></td>
    <td align="center"><input type="text" name="posted_data[<?php echo $this->_tpl_vars['transients'][$this->_sections['grid']['index']]['action']; ?>
][cutoff_days]" value="<?php echo $this->_tpl_vars['transients'][$this->_sections['grid']['index']]['cutoff_days']; ?>
"  size="2" /></td>
</tr>

<?php endfor; endif; ?>
<td colspan="5"><br /><br /></td>
<?php else: ?>

<tr>
 <td colspan="4" align="center">No Priority available.</td>
</tr>
<?php endif; ?>

<!--<tr>
<td colspan="5"><br /><br /><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_add_product'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr> -->

<tr>
	<td colspan="5" class="SubmitBox">
	<input type="submit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_update'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: document.groupfrm.mode.value = 'update'; document.groupfrm.submit();" />

	</td>
</tr>


</table>
</form>
<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean();  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'Solr Fields Priority List','content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<br /><br />

