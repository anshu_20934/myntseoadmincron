<?php /* Smarty version 2.6.12, created on 2017-03-28 11:52:03
         compiled from help/Password_Recovery.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'help/Password_Recovery.tpl', 13, false),)), $this); ?>
<?php func_load_lang($this, "help/Password_Recovery.tpl","lbl_email"); ?><form action="help.php" method="post" name="processform">
<input type="hidden" name="action" value="recover_password" />
<div class="super"><p>recover password</p></div>
<div class="links"><p>Please enter your valid e-mail address (the one you used for registration), your account information 
will be mailed to you shortly.</p></div>


<div class="links">
     <div class="legend"><p><strong><?php echo $this->_tpl_vars['lng']['lbl_email']; ?>
<span class="mandatory">*</span></strong></p></div>
     <div class="field">
        <label>
              <input type="text"  name="email" id="email" size="30" value="<?php echo ((is_array($_tmp=$GLOBALS['HTTP_GET_VARS']['email'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
"  />
            </label> 
	 </div>
     <div class="clearall"></div>
	
</div>


<div class="button">
         <input class="submit" type="submit" name="submitButton" value="submit" border="0"  onClick="javascript:return EmailValidation(); ">
</div>

<div class="foot"></div>
</form>
<br>
