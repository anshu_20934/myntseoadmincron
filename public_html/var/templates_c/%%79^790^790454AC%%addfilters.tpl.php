<?php /* Smarty version 2.6.12, created on 2017-03-27 10:03:21
         compiled from admin/main/addfilters.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'admin/main/addfilters.tpl', 233, false),array('modifier', 'replace', 'admin/main/addfilters.tpl', 236, false),)), $this); ?>
<?php func_load_lang($this, "admin/main/addfilters.tpl","txt_featured_products"); ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "main/popup_product.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
  <style type="text/css">
    .modify input , select{width:200px}
    .modify textarea{width:700px}
    
  </style>
  <script language="Javascript">
      function confirmDelete()
      {
          var flag = confirm(\'Are you sure you want to delete?\')
          if(!flag)
          {
             return false;
          }
          else
          {
          	 document.groupfrm.mode.value = \'delete\';
          	 document.groupfrm.submit();
          	 return true;
          }
      }
      var i=1;
      function addfilter(type,filter_group_id){
          var add_html=\'<tr id="new_\'+type+\'_tr_\'+i+\'" style="background-color:pink;"><td colspan="5"><form action="" id="new_\'+type+\'_form_\'+i+\'" method="post"><input type="hidden" name="mode" value="addNewFilter" ><input type="hidden" name="filter_group_id" value="\'+filter_group_id+\'" ><table width="100%"><tr><td width="20%" align="center"><input type="text" name="filter_name"></td><td width="20%" align="center"><select name="filter_map_id" id="\'+type+\'_mapsto_\'+i+\'"></select></td><td width="10%" align="center">Yes</td><td width="10%" align="center"><input type="text" name="display_order"></td><td width="20%" align="center"><a style="cursor:pointer;padding-right:10px" onclick="savefilter(\\\'\'+type+\'\\\',\\\'\'+i+\'\\\')">Save</a><a style="cursor:pointer" onclick="cancelfilter(\\\'\'+type+\'\\\',\\\'\'+i+\'\\\')">Cancel</a></td></tr></table></form></td></tr>\';
          $(\'#\'+type+\'_filter\').append(add_html);

          $.ajax({
               type: "POST",
               url: http_loc+"/admin/addfilters.php",
               data: "mode=getAllFilters",
               success: function(msg){
                 var all_filter_html=$.trim(msg);
                 $("#"+type+"_mapsto_"+i).html(all_filter_html);
                  i++;
               }
             });


      }
      function savefilter(type_i,value)
      {
            var params=$("#new_"+type_i+"_form_"+value).serializeArray();
            $.ajax({

               type: "POST",
               url: http_loc+"/admin/addfilters.php",
               data: params,
               success: function(msg){
                 var filter_saved_html=$.trim(msg);
                 
                 $(\'#new_\'+type_i+\'_tr_\'+value).remove();
                 $(\'#\'+type_i+\'_filter\').append(filter_saved_html);

               }
             });
      }
      function cancelfilter(type_i,value)
      {
        $("#new_"+type_i+\'_tr_\'+value).remove();
      }
      function changeOrder(id,value)
      {
        $.get(http_loc+"/admin/addfilters.php", { mode: "changeorder", id: id,order:value },
           function(data){
                
           });
      }
  </script>
'; ?>

<a name="featured" />

<?php echo $this->_tpl_vars['lng']['txt_featured_products']; ?>


<br /><br />

<?php ob_start(); ?>
<?php if ($this->_tpl_vars['method'] == 'modifyfilter'): ?>
    <form action="<?php echo $this->_tpl_vars['form_action']; ?>
" method="post" name="filter_form" enctype="multipart/form-data">
        <input type="hidden" name="mode" value="savefilter">
        <input type="hidden" name="filter_id" value="<?php echo $this->_tpl_vars['filter_id']; ?>
">
        <table border="0" cellspacing="5" cellpadding="5"  width="100%" class="modify">
            <tr>
                <td>Filter Name</td>
                <td><input type="text" name="filter_name" value="<?php if ($this->_tpl_vars['filter_details']['filter_name']):  echo $this->_tpl_vars['filter_details']['filter_name']; ?>
 <?php endif; ?>"> </td>
            </tr>
            <tr>
                <td>Display Order</td>
                <td><input type="text" name="display_order" value="<?php if ($this->_tpl_vars['filter_details']['display_order']):  echo $this->_tpl_vars['filter_details']['display_order']; ?>
 <?php endif; ?>"> </td>
            </tr>

            <tr>
                <td>Filter Url</td>
                <td><input type="text" name="filter_url" value="<?php if ($this->_tpl_vars['filter_details']['filter_url']):  echo $this->_tpl_vars['filter_details']['filter_url']; ?>
 <?php endif; ?>">
                For example if the url is <?php echo $this->_tpl_vars['http_location']; ?>
/sportswear/sporting-goods , then enter only <b>sporting-goods</b> here 
                </td>
            </tr>
            <tr>
                <td>Filter Type</td>
                <td>
                    <select name="filter_group_id" class="">
                       <?php unset($this->_sections['num']);
$this->_sections['num']['name'] = 'num';
$this->_sections['num']['loop'] = is_array($_loop=$this->_tpl_vars['groups']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['num']['show'] = true;
$this->_sections['num']['max'] = $this->_sections['num']['loop'];
$this->_sections['num']['step'] = 1;
$this->_sections['num']['start'] = $this->_sections['num']['step'] > 0 ? 0 : $this->_sections['num']['loop']-1;
if ($this->_sections['num']['show']) {
    $this->_sections['num']['total'] = $this->_sections['num']['loop'];
    if ($this->_sections['num']['total'] == 0)
        $this->_sections['num']['show'] = false;
} else
    $this->_sections['num']['total'] = 0;
if ($this->_sections['num']['show']):

            for ($this->_sections['num']['index'] = $this->_sections['num']['start'], $this->_sections['num']['iteration'] = 1;
                 $this->_sections['num']['iteration'] <= $this->_sections['num']['total'];
                 $this->_sections['num']['index'] += $this->_sections['num']['step'], $this->_sections['num']['iteration']++):
$this->_sections['num']['rownum'] = $this->_sections['num']['iteration'];
$this->_sections['num']['index_prev'] = $this->_sections['num']['index'] - $this->_sections['num']['step'];
$this->_sections['num']['index_next'] = $this->_sections['num']['index'] + $this->_sections['num']['step'];
$this->_sections['num']['first']      = ($this->_sections['num']['iteration'] == 1);
$this->_sections['num']['last']       = ($this->_sections['num']['iteration'] == $this->_sections['num']['total']);
?>
                            <option value="<?php echo $this->_tpl_vars['groups'][$this->_sections['num']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['filter_details']['filter_group_id'] == $this->_tpl_vars['groups'][$this->_sections['num']['index']]['id']): ?>selected <?php endif; ?> ><?php echo $this->_tpl_vars['groups'][$this->_sections['num']['index']]['group_name']; ?>
</option>
                       <?php endfor; endif; ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Maps To</td>
                <td>
                    <select name="filter_map_id" class="">
                       <option value="">None</option>
                       <?php unset($this->_sections['num']);
$this->_sections['num']['name'] = 'num';
$this->_sections['num']['loop'] = is_array($_loop=$this->_tpl_vars['filters']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['num']['show'] = true;
$this->_sections['num']['max'] = $this->_sections['num']['loop'];
$this->_sections['num']['step'] = 1;
$this->_sections['num']['start'] = $this->_sections['num']['step'] > 0 ? 0 : $this->_sections['num']['loop']-1;
if ($this->_sections['num']['show']) {
    $this->_sections['num']['total'] = $this->_sections['num']['loop'];
    if ($this->_sections['num']['total'] == 0)
        $this->_sections['num']['show'] = false;
} else
    $this->_sections['num']['total'] = 0;
if ($this->_sections['num']['show']):

            for ($this->_sections['num']['index'] = $this->_sections['num']['start'], $this->_sections['num']['iteration'] = 1;
                 $this->_sections['num']['iteration'] <= $this->_sections['num']['total'];
                 $this->_sections['num']['index'] += $this->_sections['num']['step'], $this->_sections['num']['iteration']++):
$this->_sections['num']['rownum'] = $this->_sections['num']['iteration'];
$this->_sections['num']['index_prev'] = $this->_sections['num']['index'] - $this->_sections['num']['step'];
$this->_sections['num']['index_next'] = $this->_sections['num']['index'] + $this->_sections['num']['step'];
$this->_sections['num']['first']      = ($this->_sections['num']['iteration'] == 1);
$this->_sections['num']['last']       = ($this->_sections['num']['iteration'] == $this->_sections['num']['total']);
?>
                            <option value="<?php echo $this->_tpl_vars['filters'][$this->_sections['num']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['mapping_details'][0]['filter_map_id'] == $this->_tpl_vars['filters'][$this->_sections['num']['index']]['id']): ?>selected <?php endif; ?> ><?php echo $this->_tpl_vars['filters'][$this->_sections['num']['index']]['filter_name']; ?>
</option>
                       <?php endfor; endif; ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Narrow By Section</td>
                <td>
                    <select name="narrow_by[]" class="" multiple="multiple">

                       <option value="">None</option>
                       <?php unset($this->_sections['n']);
$this->_sections['n']['name'] = 'n';
$this->_sections['n']['loop'] = is_array($_loop=$this->_tpl_vars['groups']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['n']['show'] = true;
$this->_sections['n']['max'] = $this->_sections['n']['loop'];
$this->_sections['n']['step'] = 1;
$this->_sections['n']['start'] = $this->_sections['n']['step'] > 0 ? 0 : $this->_sections['n']['loop']-1;
if ($this->_sections['n']['show']) {
    $this->_sections['n']['total'] = $this->_sections['n']['loop'];
    if ($this->_sections['n']['total'] == 0)
        $this->_sections['n']['show'] = false;
} else
    $this->_sections['n']['total'] = 0;
if ($this->_sections['n']['show']):

            for ($this->_sections['n']['index'] = $this->_sections['n']['start'], $this->_sections['n']['iteration'] = 1;
                 $this->_sections['n']['iteration'] <= $this->_sections['n']['total'];
                 $this->_sections['n']['index'] += $this->_sections['n']['step'], $this->_sections['n']['iteration']++):
$this->_sections['n']['rownum'] = $this->_sections['n']['iteration'];
$this->_sections['n']['index_prev'] = $this->_sections['n']['index'] - $this->_sections['n']['step'];
$this->_sections['n']['index_next'] = $this->_sections['n']['index'] + $this->_sections['n']['step'];
$this->_sections['n']['first']      = ($this->_sections['n']['iteration'] == 1);
$this->_sections['n']['last']       = ($this->_sections['n']['iteration'] == $this->_sections['n']['total']);
?>
                            <?php $this->assign('is_selected', 'notselected'); ?>
                            <?php if ($this->_tpl_vars['selected_narrow_by']): ?>
                                <?php unset($this->_sections['inner']);
$this->_sections['inner']['name'] = 'inner';
$this->_sections['inner']['loop'] = is_array($_loop=$this->_tpl_vars['selected_narrow_by']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['inner']['show'] = true;
$this->_sections['inner']['max'] = $this->_sections['inner']['loop'];
$this->_sections['inner']['step'] = 1;
$this->_sections['inner']['start'] = $this->_sections['inner']['step'] > 0 ? 0 : $this->_sections['inner']['loop']-1;
if ($this->_sections['inner']['show']) {
    $this->_sections['inner']['total'] = $this->_sections['inner']['loop'];
    if ($this->_sections['inner']['total'] == 0)
        $this->_sections['inner']['show'] = false;
} else
    $this->_sections['inner']['total'] = 0;
if ($this->_sections['inner']['show']):

            for ($this->_sections['inner']['index'] = $this->_sections['inner']['start'], $this->_sections['inner']['iteration'] = 1;
                 $this->_sections['inner']['iteration'] <= $this->_sections['inner']['total'];
                 $this->_sections['inner']['index'] += $this->_sections['inner']['step'], $this->_sections['inner']['iteration']++):
$this->_sections['inner']['rownum'] = $this->_sections['inner']['iteration'];
$this->_sections['inner']['index_prev'] = $this->_sections['inner']['index'] - $this->_sections['inner']['step'];
$this->_sections['inner']['index_next'] = $this->_sections['inner']['index'] + $this->_sections['inner']['step'];
$this->_sections['inner']['first']      = ($this->_sections['inner']['iteration'] == 1);
$this->_sections['inner']['last']       = ($this->_sections['inner']['iteration'] == $this->_sections['inner']['total']);
?>
                                    <?php if ($this->_tpl_vars['selected_narrow_by'][$this->_sections['inner']['index']] == $this->_tpl_vars['groups'][$this->_sections['n']['index']]['id']): ?>
                                        <?php $this->assign('is_selected', 'selected'); ?>
                                    <?php endif; ?>
                                <?php endfor; endif; ?>
                            <?php endif; ?>
                            <option value="<?php echo $this->_tpl_vars['groups'][$this->_sections['n']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['is_selected'] == 'selected'): ?>selected<?php endif; ?> ><?php echo $this->_tpl_vars['groups'][$this->_sections['n']['index']]['group_name']; ?>
</option>
                       <?php endfor; endif; ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Icon/Logo</td>
                <td>
                    <img src="<?php if ($this->_tpl_vars['filter_details']['logo']):  echo $this->_tpl_vars['filter_details']['logo']; ?>
 <?php endif; ?>" alt="" height="50px" width="50px">
                    <br>
                    <input type="file" name="logo" value="">
                    <br>
                    <?php if ($this->_tpl_vars['filter_details']['logo']): ?> Remove Logo :<input type="checkbox" value="1" name="remove_logo"> <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td>Icon / Logo Url</td>
                <td><input type="text" name="logo_url" value="<?php if ($this->_tpl_vars['filter_details']['logo_url']):  echo $this->_tpl_vars['filter_details']['logo_url']; ?>
 <?php endif; ?>"> </td>
            </tr>
            <tr>
                <td>Icon / Logo Alt tag</td>
                <td><input type="text" name="logo_alt_tag" value="<?php if ($this->_tpl_vars['filter_details']['logo_alt_tag']):  echo $this->_tpl_vars['filter_details']['logo_alt_tag']; ?>
 <?php endif; ?>"> </td>
            </tr>
            <tr>
                <td>Top Banner</td>
                <td>
                    <img src="<?php if ($this->_tpl_vars['filter_details']['top_banner']):  echo $this->_tpl_vars['filter_details']['top_banner']; ?>
 <?php endif; ?>" alt="" height="50px" width="700px">
                    <br>
                    <input type="file" name="top_banner" value="">
                    <br>
                    <?php if ($this->_tpl_vars['filter_details']['top_banner']): ?> Remove Banner : <input type="checkbox" value="1" name="remove_top_banner"> <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td>Top Banner Url</td>
                <td><input type="text" name="top_banner_url" value="<?php if ($this->_tpl_vars['filter_details']['top_banner_url']):  echo $this->_tpl_vars['filter_details']['top_banner_url']; ?>
 <?php endif; ?>"> </td>
            </tr>
            <tr>
                <td>Top Banner Alt Tag</td>
                <td><input type="text" name="top_banner_alt_tag" value="<?php if ($this->_tpl_vars['filter_details']['top_banner_alt_tag']):  echo $this->_tpl_vars['filter_details']['top_banner_alt_tag']; ?>
 <?php endif; ?>"> </td>
            </tr>
            <tr>
                <td>About Filter</td>
                <td><textarea name="about_filter" style="height:100px;"><?php if ($this->_tpl_vars['filter_details']['about_filter']):  echo $this->_tpl_vars['filter_details']['about_filter']; ?>
 <?php endif; ?></textarea> </td>
            </tr>
			
			<tr>
                <td>Page Title</td>
                <td><textarea name="page_title" ><?php if ($this->_tpl_vars['filter_details']['page_title']):  echo $this->_tpl_vars['filter_details']['page_title']; ?>
 <?php endif; ?></textarea> </td>
            </tr>
			<tr>
                <td>H1 Heading</td>
                <td><textarea name="h1_title" ><?php if ($this->_tpl_vars['filter_details']['h1_title']):  echo $this->_tpl_vars['filter_details']['h1_title']; ?>
 <?php endif; ?></textarea> </td>
            </tr>
			<tr>
                <td>Meta Keywords</td>
                <td><textarea name="meta_keywords" ><?php if ($this->_tpl_vars['filter_details']['meta_keywords']):  echo $this->_tpl_vars['filter_details']['meta_keywords']; ?>
 <?php endif; ?></textarea> </td>
            </tr>
			<tr>
                <td>Meta Description</td>
                <td><textarea name="meta_description" ><?php if ($this->_tpl_vars['filter_details']['meta_description']):  echo $this->_tpl_vars['filter_details']['meta_description']; ?>
 <?php endif; ?></textarea> </td>
            </tr>
			
            <tr>
                <td>Active</td>
                <td>
                     <select name="is_active" class="">
                       <option value="1" <?php if ($this->_tpl_vars['filter_details']['is_active'] == 1): ?>selected <?php endif; ?> >Yes</option>
                       <option value="0" <?php if ($this->_tpl_vars['filter_details']['is_active'] == 0): ?>selected <?php endif; ?> >No</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><input type="submit" value="Update" style="width:100px"> </td>
                <td><a href="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/addfilters.php">Cancel</a> </td>
             </tr>

        </table>
    </form>
<?php else: ?>
    <div style="width:100%;padding:5px" align="right"><a href="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/filtergroups.php">Create New Filter Group</a></div>
    <table border="1" cellspacing="0" cellpadding="0"  width="100%">
        <tr class="TableHead">
            <td width="20%">Filter Type</td>
            <td width="80%" align="center" colspan="4">
                <table cellpadding="5" cellspacing="1" width="100%">
                    <tr class="TableHead" >
                        <td width="20%" align="center">Filter</td>
                        <td width="20%">Maps To</td>
                        <td width="10%" align="center">Active</td>
                        <td width="10%" align="center">Display Order</td>
                        <td width="20%" align="center">Action</td>
                    </tr>
                 </table>
            </td>
        </tr>
        <?php if ($this->_tpl_vars['groups']): ?>
        <?php $_from = $this->_tpl_vars['filters']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['filter']):
?>
        <tr <?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this);?>
>
            <td align="center"><?php echo $this->_tpl_vars['key']; ?>

                <br><br>
                <a style="cursor:pointer;text-decoration:underline;" onclick="addfilter('<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['key'])) ? $this->_run_mod_handler('replace', true, $_tmp, ' ', '_') : smarty_modifier_replace($_tmp, ' ', '_')))) ? $this->_run_mod_handler('replace', true, $_tmp, "/", '_') : smarty_modifier_replace($_tmp, "/", '_')); ?>
','<?php echo $this->_tpl_vars['groups'][$this->_tpl_vars['key']]['id']; ?>
')">Add New <?php echo $this->_tpl_vars['key']; ?>
</a>
            </td>
            <td align="center" colspan="4">
                <table cellpadding="3" cellspacing="1" width="100%" id="<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['key'])) ? $this->_run_mod_handler('replace', true, $_tmp, ' ', '_') : smarty_modifier_replace($_tmp, ' ', '_')))) ? $this->_run_mod_handler('replace', true, $_tmp, "/", '_') : smarty_modifier_replace($_tmp, "/", '_')); ?>
_filter">
                    <?php $_from = $this->_tpl_vars['filter']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['f']):
?>
                            <tr style="background-color:<?php echo smarty_function_cycle(array('values' => ", #ccc"), $this);?>
">
                                <td width="20%" align="center"><?php echo $this->_tpl_vars['f']['filter_name']; ?>
</td>
                                <td width="20%" align="center"><?php echo $this->_tpl_vars['f']['mapsto']; ?>
</td>
                                <td width="10%" align="center"><?php if ($this->_tpl_vars['f']['is_active']): ?>Yes<?php else: ?>No<?php endif; ?></td>
                                <td width="10%" align="center"><input type="text" value="<?php echo $this->_tpl_vars['f']['display_order']; ?>
" style="width:30px" onblur="changeOrder('<?php echo $this->_tpl_vars['f']['id']; ?>
',this.value)" ></td>
                                <td width="20%" align="center"><a href="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/addfilters.php?mode=modifyfilter&filterid=<?php echo $this->_tpl_vars['f']['id']; ?>
">Modify</a></td>
                            </tr>
                    <?php endforeach; endif; unset($_from); ?>
                 </table>
            </td>
        </tr>

    <?php endforeach; endif; unset($_from); ?>
    <?php else: ?>

        <tr>
         <td colspan="4" align="center">No groups available.</td>
        </tr>
    <?php endif; ?>

    </table>
<?php endif; ?>
<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => "Add/Modify Filter",'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<br /><br />