<?php /* Smarty version 2.6.12, created on 2017-04-05 12:39:40
         compiled from admin/main/styleBulkUpdate.tpl */ ?>
<div>Change Article Group: <select name="select_aid"  id="select_aid" onChange="reloadPage()">
	<?php $_from = $this->_tpl_vars['articleIds']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['article']):
?>
		<option value="<?php echo $this->_tpl_vars['article']['id']; ?>
" <?php if ($this->_tpl_vars['articleTypeId'] == $this->_tpl_vars['article']['id']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['article']['name']; ?>
</option>
	<?php endforeach; endif; unset($_from); ?>
</select>
<br>
</div>
<script type="text/javascript">
	var articleTypeId = <?php echo $this->_tpl_vars['articleTypeId']; ?>
;
	var style_ids = '<?php echo $this->_tpl_vars['style_ids']; ?>
';
	var styleIdList    = <?php echo $this->_tpl_vars['styleIdList']; ?>
;
	var noStyleColumns = styleIdList.length;
	
	var styleAll = <?php echo $this->_tpl_vars['styleAll']; ?>
;
	var styleColKeys = <?php echo $this->_tpl_vars['styleColKeys']; ?>
;
	var styleStatusData = <?php echo $this->_tpl_vars['styleStatus']; ?>
;
	var possibleStatusTrans = <?php echo $this->_tpl_vars['possibleStatusTrans']; ?>
;
	var stylePropertyName = <?php echo $this->_tpl_vars['stylePropertyName']; ?>
;
	
    var brandData         = <?php echo $this->_tpl_vars['brand']; ?>
;
    var ageGroupData      = <?php echo $this->_tpl_vars['ageGroup']; ?>
;
    var genderData        = <?php echo $this->_tpl_vars['gender']; ?>
;
    var fashionTypeData   = <?php echo $this->_tpl_vars['fashionType']; ?>
;
    var colourData        = <?php echo $this->_tpl_vars['colour']; ?>
;
    var seasonData        = <?php echo $this->_tpl_vars['season']; ?>
;
    var yearData          = <?php echo $this->_tpl_vars['year']; ?>
;
    var usageData         = <?php echo $this->_tpl_vars['usage']; ?>
;

    var classificationBrandData = <?php echo $this->_tpl_vars['classificationBrand']; ?>
;
    
    var specificAttributeExist = <?php echo $this->_tpl_vars['specificAttributeExist']; ?>
;
    
    <?php if (isset ( $this->_tpl_vars['specificAttribute'] )): ?>
    	var specificAttributeData = <?php echo $this->_tpl_vars['specificAttribute']; ?>
;
    	var specificAttributeName = <?php echo $this->_tpl_vars['specificAttributeName']; ?>
;
    <?php endif; ?>
    
    var masterCategoryData = <?php echo $this->_tpl_vars['masterCategory']; ?>
;
	var subCategoryData    = <?php echo $this->_tpl_vars['subCategory']; ?>
;
	var articleTypeData    = <?php echo $this->_tpl_vars['articleType']; ?>
;
	var productTypeData    = <?php echo $this->_tpl_vars['productType']; ?>
;

    var httpLocation       = '<?php echo $this->_tpl_vars['http_location']; ?>
';
    <?php echo '
    function reloadPage()
    {
    	var r = confirm("Are you sure want to change the group? Any unsaved changes will be lost.");
	    if(r == true)
	    {
	    	var aid = $("#select_aid").val();
	        var url = httpLocation+\'/admin/catalog/styleBulkUpdate.php?aid=\'+aid+\'&style_ids=\'+style_ids;
	    	window.open(url, \'_self\', false);
	    }
	    else
	    {
	    	$("#select_aid").val(articleTypeId);
	    }
    }
    '; ?>

</script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/extjs/resources/css/ext-all.css"/>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/extjs/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/extjs/ext-all.js"></script>
<script type="text/javascript" src="styleContentHtmlEditor.js"></script>
<script type="text/javascript" src="StylePickerDialog.js"></script>
<script type="text/javascript" src="styleSearchCopy.js"></script>
<script type="text/javascript" src="styleBulkUpdate.js"></script>
<div id="contentPanel"></div>