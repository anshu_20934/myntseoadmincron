<?php /* Smarty version 2.6.12, created on 2017-03-27 12:58:50
         compiled from main/register_personal_info.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'default', 'main/register_personal_info.tpl', 36, false),)), $this); ?>
<?php func_load_lang($this, "main/register_personal_info.tpl","lbl_first_name,lbl_last_name,lbl_email,lbl_password,lbl_confirm_password,lbl_email"); ?><?php if ($this->_tpl_vars['is_areas']['P'] == 'Y'): ?>
<?php if ($this->_tpl_vars['hide_header'] == ""): ?>

<div class="super"><p><?php echo $this->_tpl_vars['firstname']; ?>
 account information</p> </div>
<div class="head"><p>fill your login information</p></div>
<?php endif; ?>
<div class="links_980"><div class="legend_980"><p>(<span class="mandatory">*</span>)&nbsp;<span class="mandatory">Mandatory fields</span></p></div></div>
<?php if ($this->_tpl_vars['default_fields']['firstname']['avail'] == 'Y'): ?>



<div class="links_980"><div class="legend_980"><p><?php echo $this->_tpl_vars['lng']['lbl_first_name']; ?>
<span class="mandatory">*</span></p></div>
	<div class="registerfield_980">
		<label>
			<input type="text" id="firstname" name="firstname"  value="<?php echo $this->_tpl_vars['userinfo']['firstname']; ?>
" class="inputtextbox" tabindex="0"/>
		</label> 
	</div><div class="clearall"></div>
</div>
<?php endif; ?>

<?php if ($this->_tpl_vars['default_fields']['lastname']['avail'] == 'Y'): ?>
<div class="links_980"><div class="legend_980"><p><?php echo $this->_tpl_vars['lng']['lbl_last_name']; ?>
<span class="mandatory">*</span></p></div>
	<div class="registerfield_980">
	<label>
		<input type="text" id="lastname" name="lastname"  value="<?php echo $this->_tpl_vars['userinfo']['lastname']; ?>
" class="inputtextbox" tabindex="1" />
		<?php if ($this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['lastname'] == "" && $this->_tpl_vars['default_fields']['lastname']['required'] == 'Y'):  endif; ?>
	</label>
	</div><div class="clearall"></div>
</div>
<?php endif; ?>

<div class="links_980"><div class="legend_980"><p><?php echo $this->_tpl_vars['lng']['lbl_email']; ?>
<span class="mandatory">*</span></p></div>
<div class="registerfield_980"><label>
<?php if ($this->_tpl_vars['userinfo']['login'] != "" || ( $this->_tpl_vars['login'] == $this->_tpl_vars['userinfo']['uname'] && $this->_tpl_vars['login'] != '' )): ?>
<b><?php echo ((is_array($_tmp=@$this->_tpl_vars['userinfo']['login'])) ? $this->_run_mod_handler('default', true, $_tmp, @$this->_tpl_vars['userinfo']['uname']) : smarty_modifier_default($_tmp, @$this->_tpl_vars['userinfo']['uname'])); ?>
</b>
<input type="hidden" name="uname" value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['userinfo']['login'])) ? $this->_run_mod_handler('default', true, $_tmp, @$this->_tpl_vars['userinfo']['uname']) : smarty_modifier_default($_tmp, @$this->_tpl_vars['userinfo']['uname'])); ?>
" />
<?php else: ?>
<input  type="text" id="uname" name="uname"  value="<?php if ($this->_tpl_vars['userinfo']['uname']):  echo $this->_tpl_vars['userinfo']['uname'];  else:  echo $this->_tpl_vars['userinfo']['login'];  endif; ?>" class="inputtextbox" />
&nbsp;<span class="greymandatory">(your e-mail will be your username)</span>
<?php endif; ?>
</label></div><div class="clearall"></div>
</div>

<div class="links_980"><div class="legend_980"><p><?php echo $this->_tpl_vars['lng']['lbl_password']; ?>
<span class="mandatory">*</span></p></div>
<div class="registerfield_980"><label>
<input  type="password" id="passwd1" name="passwd1"  value="<?php echo $this->_tpl_vars['userinfo']['passwd1']; ?>
" class="inputtextbox" tabindex="3" />
&nbsp;<span class="greymandatory">(minimum 6 character)</span>
<?php if ($this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['passwd1'] == ""):  endif; ?> 
</label></div><div class="clearall"></div>
</div>

<div class="links_980"><div class="legend_980"><p><?php echo $this->_tpl_vars['lng']['lbl_confirm_password']; ?>
<span class="mandatory">*</span></p></div>
<div class="registerfield_980"><label>
<input type="password" id="passwd2" name="passwd2"  value="<?php echo $this->_tpl_vars['userinfo']['passwd2']; ?>
" class="inputtextbox" tabindex="4"/>
<?php if ($this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['passwd2'] == ""):  endif; ?> 
</label></div><div class="clearall"></div>
</div>

<div class="links_980"><div class="legend_980"><p>Gender<span class="mandatory">*</span></p></div>
<div class="registerfield_980"><label>
	<select name="gender">
		<?php if ($this->_tpl_vars['userinfo']['gender'] == 'M'): ?>					
			<option value="M" selected>Male</option>
			<option value="F">Female</option>
		<?php else: ?>
			<option value="M" selected>Male</option>
			<option value="F" >Female</option>
		<?php endif; ?>
	</select>	
</label></div><div class="clearall"></div>
</div>

<div class="links_980"><div class="legend_980"><p>Date of Birth</p></div>
<div class="registerfield_980">
	<?php 
		$userinfo = $this->get_template_vars('userinfo');	
		$year = date("Y");
		$month = 12;
		$day = 31; 
		$dayselect = "<select style='font-weight:normal;font-size:9pt;width:50px;' name='day'><option value=''>day</option>";
		for($i=1;$i<=$day;$i++){
			if($userinfo['DOB_day'] == $i)
				$dayselect .= "<option value='$i' selected>$i</option>";
			else
				$dayselect .= "<option value='$i'>$i</option>";
		}
		$dayselect .= "</select>";
		$i=1;
		$monthselect = " <select style='font-weight:normal;font-size:9pt;width:60px;' name='month'><option value=''>month</option>";
		for($i=1;$i<=$month;$i++){
			if($userinfo['DOB_month'] == $i)
				$monthselect .= "<option value='$i' selected>$i</option>";
			else
				$monthselect .= "<option value='$i'>$i</option>";
		}		
		$monthselect .= "</select>";
		$i=1;
		$yearselect = " <select style='font-weight:normal;font-size:9pt;width:60px;' name='year'><option value=''>year</option>";
		for($i=$year;$i>=1900;$i--){
			if($userinfo['DOB_year'] == $i)
				$yearselect .= "<option value='$i' selected>$i</option>";
			else
				$yearselect .= "<option value='$i'>$i</option>";
		}
		$yearselect .= "</select>";
		
		echo $dayselect.$monthselect.$yearselect;	 	
	 ?>		
</div><div class="clearall"></div>
</div>	

<!-- added in later stage for about me, interest and  links -->

<div class="links_980"><div class="legend_980"><p> About  Me</p></div>
<div class="registerfield_980">
<label> 
<textarea id="about_me" name="about_me" class="address"><?php echo $this->_tpl_vars['userinfo']['about_me']; ?>
</textarea>
</label>
</div><div class="clearall"></div>
</div>

<div class="links_980"><div class="legend_980"><p>My Interests</p></div>
<div class="registerfield_980"><label>
<textarea id="interests" name="interests" class="address"><?php echo $this->_tpl_vars['userinfo']['interests']; ?>
</textarea>
</label></div><div class="clearall"></div>
</div>

<div class="links_980"><div class="legend_980"><p>My Links </p></div>
<div class="registerfield_980"><label>
<textarea id="mylinks" name="mylinks" class="address"><?php echo $this->_tpl_vars['userinfo']['mylinks']; ?>
</textarea>
</label></div><div class="clearall"></div>
</div>
<!-- end -->

<?php if ($this->_tpl_vars['default_fields']['image']['avail'] == 'Y'): ?>
<div class="links_980"><div class="legend_980"><p>Upload Your image</p></div>
      <div class="registerfield_980">
      <label>
           <input type="file" id="myimage" name="myimage" >
	   </label>
              <!-- <input class="submit" type="submit" name="submitButton" value="Upload" border="0"  onClick="return checkImageFile(document.registerform.myimage); "> -->
	      
	
       </div> <div class="clearall"></div>         
</div>
<?php endif; ?>

<?php if ($this->_tpl_vars['userinfo']['myimage'] != ""): ?>

<div class="links_980"><div class="legend_980"><p>Your uploaded image</p></div>

<div class="registerfield_980">
<!-- <span class="tip" onmouseover='doTooltip(event, "./images/designer/T/<?php echo $this->_tpl_vars['userinfo']['myimage']; ?>
")' onmouseout="hideTip()"> -->
           <p><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/images/designer/T/<?php echo $this->_tpl_vars['userinfo']['myimage']; ?>
"   />
           <input class="submit" type="button" name="deleteImage" value="Delete image" border="0" 
	   onClick= "javascript:deleteUserImage('<?php echo $this->_tpl_vars['userinfo']['myimage']; ?>
');"></p>
<!-- </span> -->
</div> 
	<div class="clearall"></div>         
</div>
<?php endif; ?>
<div class="links_980">
	<div class="legend_980"><p>Subscribe daily design</p> </div>
	<div class="field">
		<label> 
			<input type="checkbox" id="dailydesign" name="dailydesign" value= "1" class="address" <?php if ($this->_tpl_vars['userinfo']['dailydesign'] == '1'): ?>checked <?php else:  endif; ?>/>
		</label>
	</div>
	
	<div class="clearall"></div>
</div>


<!-- <div class="links_980"><div class="legend_980"><p><?php echo $this->_tpl_vars['lng']['lbl_email']; ?>
<span class="mandatory">*</span></p></div>
<div class="registerfield_980">
<input  type="text" id="email" name="email"  value="<?php echo $this->_tpl_vars['userinfo']['email']; ?>
" tabindex="2" />
<?php if ($this->_tpl_vars['emailerror'] != "" || ( $this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['email'] == "" && $this->_tpl_vars['default_fields']['email']['required'] == 'Y' )):  endif; ?>
</label></div><div class="clearall"></div>
</div> -->
<div class="foot"></div>

<div class='addressbox'>
<div class="addressminusimage" id="addressminusimage" style="cursor:pointer;visibility:hidden;display:none" onclick='javascript: hideAddress("addressinfo");'></div>
<div class="addressplusimage" id="addressplusimage" style='cursor:pointer;' onclick='javascript: displayAddress("addressinfo");'></div>&nbsp;
<strong style="vertical-align:top;">shipping and billing address</strong>&nbsp;<span class="greymandatory">(optional)</span>
</div>

	
<br>
<br>

<?php endif; ?>