<?php /* Smarty version 2.6.12, created on 2017-05-19 10:57:54
         compiled from admin/pageconfig/pageconfig-imagerules.tpl */ ?>
<div class="section">
	<?php if (! $this->_tpl_vars['variantPage']): ?>
	<div class="section-title"><?php echo $this->_tpl_vars['rule']['location_type']; ?>
 
	>>
<?php if ($this->_tpl_vars['rule']['parent_rule_id'] > 0):  $this->assign('rule_id', $this->_tpl_vars['rule']['parent_rule_id']);  echo $this->_tpl_vars['rules'][$this->_tpl_vars['rule_id']]['section']; ?>
 >> <?php endif; ?> 
<?php if ($this->_tpl_vars['pageLocation']):  echo $this->_tpl_vars['pageLocation']; ?>
 >>
<?php endif; ?> 
	<?php echo $this->_tpl_vars['rule']['section']; ?>
  <br/> 
		<a class="addBannerLink" data-lightbox-id="<?php echo $this->_tpl_vars['i']; ?>
" href="#">Add more banner(s) >></a>
	</div>
	<?php endif; ?>
		<!-- <input type="button" value="Add more banner(s)" class="addBannerLink" data-lightbox-id="<?php echo $this->_tpl_vars['i']; ?>
" /> -->
	<div class="section-body">Config rules for <?php echo $this->_tpl_vars['rule']['location_type']; ?>
 - Section: <?php echo $this->_tpl_vars['rule']['section']; ?>
<br/>
	
	<?php if ($this->_tpl_vars['rule']['height'] != -1): ?>
	Image Dimensions: <?php echo $this->_tpl_vars['rule']['width']; ?>
 x <?php echo $this->_tpl_vars['rule']['height']; ?>
 pixels ; Max file-size: <?php echo $this->_tpl_vars['rule']['size']/1024; ?>
 KB
	<?php else: ?>
	Image Dimensions: <?php echo $this->_tpl_vars['rule']['width']; ?>
 x (variable height) pixels ; Max file-size: <?php echo $this->_tpl_vars['rule']['size']/1024; ?>
 KB
	Each batch of images uploaded need to have the same height in pixels
	<?php endif; ?>
	<?php if (! $this->_tpl_vars['variantPage']): ?><br/>Number of images:  Min: <?php echo $this->_tpl_vars['rule']['min']; ?>
, Max: <?php echo $this->_tpl_vars['rule']['max']; ?>
 in steps of <?php echo $this->_tpl_vars['rule']['step']; ?>
<br/><?php endif; ?>
	</div>
</div>