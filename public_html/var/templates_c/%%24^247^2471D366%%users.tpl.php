<?php /* Smarty version 2.6.12, created on 2017-03-24 21:18:50
         compiled from admin/main/users.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'default', 'admin/main/users.tpl', 30, false),array('modifier', 'date_format', 'admin/main/users.tpl', 30, false),array('modifier', 'escape', 'admin/main/users.tpl', 120, false),array('modifier', 'substitute', 'admin/main/users.tpl', 399, false),array('modifier', 'amp', 'admin/main/users.tpl', 428, false),array('modifier', 'urlencode', 'admin/main/users.tpl', 463, false),array('function', 'html_select_date', 'admin/main/users.tpl', 330, false),array('function', 'cycle', 'admin/main/users.tpl', 449, false),)), $this); ?>
<?php func_load_lang($this, "admin/main/users.tpl","lbl_users_management,txt_users_management_top_text,txt_search_users_text,lbl_search_for_pattern,lbl_search,lbl_search_in,lbl_username,lbl_first_name,lbl_last_name,lbl_email,lbl_company,lbl_search_and_export,lbl_advanced_search_options,lbl_advanced_search_options,lbl_search_for_user_type,lbl_all,lbl_pending_membership,lbl_pending_membership,lbl_customer_registration_type,lbl_all,lbl_anonymous,lbl_registered,lbl_search_by_address,lbl_ignore_address,lbl_billing,lbl_shipping,lbl_both,lbl_city,lbl_state,lbl_country,lbl_please_select_one,lbl_zip_code,lbl_phone,lbl_fax,lbl_web_site,lbl_search_for_users_that_is,lbl_registered,lbl_last_logged_in,lbl_during_date_period,lbl_this_month,lbl_this_week,lbl_today,lbl_specify_period_below,lbl_from,lbl_through,txt_users_search_note,lbl_search,lbl_reset,lbl_search_for_user,txt_N_results_found,txt_displaying_X_Y_results,txt_N_results_found,lbl_search_again,lbl_username,lbl_name,lbl_email,lbl_usertype,lbl_last_logged_in,lbl_modify_profile,lbl_modify_profile,lbl_no_membership,lbl_no_membership,lbl_unapproved,lbl_declined,lbl_approved,lbl_disabled,lbl_account_status_suspended,txt_N_products,lbl_never_logged_in,lbl_delete_selected,lbl_export,lbl_export_all_found,lbl_reg_chpass_admin,lbl_reg_account_status_admin,lbl_reg_do_not_change_admin,lbl_reg_account_status_suspend,lbl_reg_account_status_enable,lbl_reg_account_activity_admin,lbl_reg_account_activity_note_admin,lbl_reg_do_not_change_admin,lbl_reg_account_activity_disable,lbl_reg_account_activity_enable,lbl_of_selected_users,lbl_of_all_found_users,lbl_apply,lbl_search_results"); ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "page_title.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_users_management'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php echo $this->_tpl_vars['lng']['txt_users_management_top_text']; ?>


<br /><br />

<!-- IN THIS SECTION -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog_tools.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- IN THIS SECTION -->

<br />

<?php if ($this->_tpl_vars['mode'] == "" || $this->_tpl_vars['users'] == ""): ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "reset.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<script type="text/javascript">
<!--
var searchform_def = new Array();
searchform_def[0] = new Array('posted_data[by_username]', true);
searchform_def[1] = new Array('posted_data[by_firstname]', true);
searchform_def[2] = new Array('posted_data[by_lastname]', true);
searchform_def[3] = new Array('posted_data[by_email]', true);
searchform_def[4] = new Array('posted_data[by_company]', true);
searchform_def[5] = new Array('posted_data[by_username]', true);
searchform_def[6] = new Array('posted_data[by_username]', true);
searchform_def[7] = new Array('posted_data[by_username]', true);
searchform_def[8] = new Array('StartDay', '<?php echo ((is_array($_tmp=((is_array($_tmp=@$this->_tpl_vars['search_prefilled']['start_date'])) ? $this->_run_mod_handler('default', true, $_tmp, time()) : smarty_modifier_default($_tmp, time())))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d") : smarty_modifier_date_format($_tmp, "%d")); ?>
');
searchform_def[9] = new Array('StartMonth', '<?php echo ((is_array($_tmp=((is_array($_tmp=@$this->_tpl_vars['search_prefilled']['start_date'])) ? $this->_run_mod_handler('default', true, $_tmp, time()) : smarty_modifier_default($_tmp, time())))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m") : smarty_modifier_date_format($_tmp, "%m")); ?>
');
searchform_def[10] = new Array('StartYear', '<?php echo ((is_array($_tmp=((is_array($_tmp=@$this->_tpl_vars['search_prefilled']['start_date'])) ? $this->_run_mod_handler('default', true, $_tmp, time()) : smarty_modifier_default($_tmp, time())))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y") : smarty_modifier_date_format($_tmp, "%Y")); ?>
');
searchform_def[11] = new Array('EndDay', '<?php echo ((is_array($_tmp=((is_array($_tmp=@$this->_tpl_vars['search_prefilled']['end_date'])) ? $this->_run_mod_handler('default', true, $_tmp, time()) : smarty_modifier_default($_tmp, time())))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d") : smarty_modifier_date_format($_tmp, "%d")); ?>
');
searchform_def[12] = new Array('EndMonth', '<?php echo ((is_array($_tmp=((is_array($_tmp=@$this->_tpl_vars['search_prefilled']['end_date'])) ? $this->_run_mod_handler('default', true, $_tmp, time()) : smarty_modifier_default($_tmp, time())))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m") : smarty_modifier_date_format($_tmp, "%m")); ?>
');
searchform_def[13] = new Array('EndYear', '<?php echo ((is_array($_tmp=((is_array($_tmp=@$this->_tpl_vars['search_prefilled']['end_date'])) ? $this->_run_mod_handler('default', true, $_tmp, time()) : smarty_modifier_default($_tmp, time())))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y") : smarty_modifier_date_format($_tmp, "%Y")); ?>
');
-->
</script>

<?php echo $this->_tpl_vars['lng']['txt_search_users_text']; ?>


<br /><br />

<!-- SEARCH FORM START -->

<script type="text/javascript" language="JavaScript 1.2">
<!--
var date_selected = '<?php if ($this->_tpl_vars['search_prefilled']['date_period'] == "" || $this->_tpl_vars['search_prefilled']['date_period'] == 'M'): ?>M<?php else:  echo $this->_tpl_vars['search_prefilled']['date_period'];  endif; ?>';
<?php echo '
function managedate(type, status) {

	if (type == \'address\')
		var fields = new Array(\'posted_data[city]\',\'posted_data[state]\',\'posted_data[country]\',\'posted_data[zipcode]\');
	else if (type == \'date\')
		var fields = new Array(\'StartDay\',\'StartMonth\',\'StartYear\',\'EndDay\',\'EndMonth\',\'EndYear\');
	else if (type == \'date_type\') {
		status = document.searchform.elements[\'posted_data[registration_date]\'].checked + document.searchform.elements[\'posted_data[last_login_date]\'].checked;
		status = !(status != 0);
	
		for (var i = 0; i < document.searchform.elements.length; i++)
			if (document.searchform.elements[i].name == \'posted_data[date_period]\')
				document.searchform.elements[i].disabled = status;
	
		disable_dates = false;
		
		if (status)
			disable_dates = true;
		else if (date_selected != \'C\')
			disable_dates = true;
		
		managedate(\'date\', disable_dates);
		return true;

	}
	
	for (var i in fields)
		document.searchform.elements[fields[i]].disabled = status;
}
'; ?>

-->
</script>

<?php ob_start(); ?>

<br />

<form name="searchform" action="users.php" method="post">

<table cellpadding="0" cellspacing="0" width="100%">

<tr>
	<td>

<table cellpadding="4" cellspacing="0" width="100%">

<tr>
	<td height="10" width="20%" class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_search_for_pattern']; ?>
:</td>
	<td width="10" height="10"><font class="CustomerMessage"></font></td>
	<td height="10" width="80%">
	<input type="text" name="posted_data[substring]" size="30" style="width:70%" value="<?php echo $this->_tpl_vars['search_prefilled']['substring']; ?>
" />
	</td>
</tr>

<tr>
	<td height="10" width="20%" class="FormButton" nowrap="nowrap">Customer type:</td>
	<td width="10" height="10"><font class="CustomerMessage"></font></td>
	<td height="10" width="80%">
	
	  <select name="posted_data[offline]" id="offline">
		<option value="0">---select customer type---</option>
		<option value="N">Online</option>
		<option value="Y">Offline</option>
	  </select>
	</td>
</tr>

<tr>
	<td height="10" width="20%" >&nbsp;</td>
	<td width="10" height="10">&nbsp;</td>
	<td height="10" width="80%">
	<input type="submit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_search'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" />
	</td>
</tr>

<tr>
	<td height="10" class="FormButton"><?php echo $this->_tpl_vars['lng']['lbl_search_in']; ?>
:</td>
	<td>&nbsp;</td>
	<td>
<table cellpadding="0" cellspacing="0">
<tr>
	<td width="5"><input type="checkbox" id="posted_data_by_username" name="posted_data[by_username]"<?php if ($this->_tpl_vars['search_prefilled'] == "" || $this->_tpl_vars['search_prefilled']['by_username']): ?> checked="checked"<?php endif; ?> /></td>
	<td class="OptionLabel"><label for="posted_data_by_username"><?php echo $this->_tpl_vars['lng']['lbl_username']; ?>
</label></td>

	<td width="5"><input type="checkbox" id="posted_data_by_firstname"name="posted_data[by_firstname]"<?php if ($this->_tpl_vars['search_prefilled'] == "" || $this->_tpl_vars['search_prefilled']['by_firstname']): ?> checked="checked"<?php endif; ?> /></td>
	<td class="OptionLabel"><label for="posted_data_by_firstname"><?php echo $this->_tpl_vars['lng']['lbl_first_name']; ?>
</label></td>
	<!--
	<td width="5"><input type="checkbox" id="posted_data_by_lastname" name="posted_data[by_lastname]"<?php if ($this->_tpl_vars['search_prefilled'] == "" || $this->_tpl_vars['search_prefilled']['by_lastname']): ?> checked="checked"<?php endif; ?> /></td>
	<td class="OptionLabel"><label for="posted_data_by_lastname"><?php echo $this->_tpl_vars['lng']['lbl_last_name']; ?>
</label></td>
	-->
	<td width="5"><input type="checkbox" id="posted_data_by_email" name="posted_data[by_email]"<?php if ($this->_tpl_vars['search_prefilled'] == "" || $this->_tpl_vars['search_prefilled']['by_email']): ?> checked="checked"<?php endif; ?> /></td>
	<td class="OptionLabel"><label for="posted_data_by_email"><?php echo $this->_tpl_vars['lng']['lbl_email']; ?>
</label></td>
	<!-- 
	<td width="5"><input type="checkbox" id="posted_data_by_company" name="posted_data[by_company]"<?php if ($this->_tpl_vars['search_prefilled'] == "" || $this->_tpl_vars['search_prefilled']['by_company']): ?> checked="checked"<?php endif; ?> /></td>
	<td class="OptionLabel"><label for="posted_data_by_company"><?php echo $this->_tpl_vars['lng']['lbl_company']; ?>
</label></td>
	-->
</tr>
</table>
	</td>
</tr>

<tr>
	<td colspan="2"></td>
	<td>
<table cellpadding="0" cellspacing="0">
<tr>
	<td><input type="checkbox" id="posted_data_is_export" name="posted_data[is_export]" value="Y" /></td>
	<td>&nbsp;</td>
	<td class="FormButton" nowrap="nowrap"><label for="posted_data_is_export"><?php echo $this->_tpl_vars['lng']['lbl_search_and_export']; ?>
</label></td>
</tr>
</table> 
	</td>
</tr>


</table>

<br />

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/visiblebox_link.tpl", 'smarty_include_vars' => array('mark' => '1','title' => $this->_tpl_vars['lng']['lbl_advanced_search_options'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<br />

<table cellpadding="4" cellspacing="0" width="100%" style="display: none;" id="box1">

<tr>
	<td colspan="3"><br /><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_advanced_search_options'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>

<tr class="TableSubHead">
	<td height="10" class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_search_for_user_type']; ?>
:</td>
	<td width="10" height="10"><font class="CustomerMessage"></font></td>
	<td height="10">
	<select name="posted_data[membershipid]">
		<option value=""><?php echo $this->_tpl_vars['lng']['lbl_all']; ?>
</option>
<?php if ($this->_tpl_vars['config']['General']['membership_signup'] == 'Y'): ?>
		<option value="-pending_membership"<?php if ($this->_tpl_vars['search_prefilled']['usertype'] == "" && $this->_tpl_vars['search_prefilled']['membershipid'] == 'pending_membership'): ?> selected="selected" <?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_pending_membership']; ?>
</option>
<?php endif; ?>
<?php $_from = $this->_tpl_vars['memberships']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['lvls']):
?>
		<option value="<?php echo $this->_tpl_vars['k']; ?>
-"<?php if ($this->_tpl_vars['search_prefilled']['usertype'] == $this->_tpl_vars['k'] && $this->_tpl_vars['search_prefilled']['membershipid'] == ''): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['memberships_lbls'][$this->_tpl_vars['k']]; ?>
</option>
<?php if ($this->_tpl_vars['config']['General']['membership_signup'] == 'Y' && $this->_tpl_vars['lvls'] != ''): ?>
		<option value="<?php echo $this->_tpl_vars['k']; ?>
-pending_membership"<?php if ($this->_tpl_vars['search_prefilled']['usertype'] == $this->_tpl_vars['k'] && $this->_tpl_vars['search_prefilled']['membershipid'] == 'pending_membership'): ?> selected="selected" <?php endif; ?>>&nbsp;&nbsp;&nbsp;<?php echo $this->_tpl_vars['lng']['lbl_pending_membership']; ?>
</option>
<?php endif; ?>
<?php $_from = $this->_tpl_vars['lvls']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['v']):
?>
		<option value="<?php echo $this->_tpl_vars['k']; ?>
-<?php echo $this->_tpl_vars['v']['membershipid']; ?>
"<?php if ($this->_tpl_vars['search_prefilled']['usertype'] == $this->_tpl_vars['k'] && $this->_tpl_vars['search_prefilled']['membershipid'] == $this->_tpl_vars['v']['membershipid']): ?> selected="selected"<?php endif; ?>>&nbsp;&nbsp;&nbsp;<?php echo $this->_tpl_vars['v']['membership']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
<?php endforeach; endif; unset($_from); ?>
	</select>
	</td>
</tr>

<tr class="TableSubHead">
	<td height="10" class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_customer_registration_type']; ?>
: *</td>
	<td height="10"></td>
	<td height="10">
	<select name="posted_data[registration_type]">
		<option value=""><?php echo $this->_tpl_vars['lng']['lbl_all']; ?>
</option>
		<option value="1" <?php if ($this->_tpl_vars['search_prefilled']['registration_type'] == '1'): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_anonymous']; ?>
</option>
		<option value="2" <?php if ($this->_tpl_vars['search_prefilled']['registration_type'] == '2'): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_registered']; ?>
</option>
	</select>
	</td>
</tr>

<tr>
	<td height="10" width="20%" class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_search_by_address']; ?>
:</td>
	<td width="10" height="10"><font class="CustomerMessage"></font></td>
	<td>
<table cellpadding="0" cellspacing="0">
<tr>
	<td width="5"><input type="radio" id="address_type_null" name="posted_data[address_type]" value=""<?php if ($this->_tpl_vars['search_prefilled'] == "" || $this->_tpl_vars['search_prefilled']['address_type'] == ""): ?> checked="checked"<?php endif; ?> onclick="javascript:managedate('address',true)" /></td>
	<td class="OptionLabel"><label for="address_type_null"><?php echo $this->_tpl_vars['lng']['lbl_ignore_address']; ?>
</label></td>

	<td width="5"><input type="radio" id="address_type_B" name="posted_data[address_type]" value="B"<?php if ($this->_tpl_vars['search_prefilled']['address_type'] == 'B'): ?> checked="checked"<?php endif; ?> onclick="javascript:managedate('address',false)" /></td>
	<td class="OptionLabel"><label for="address_type_B"><?php echo $this->_tpl_vars['lng']['lbl_billing']; ?>
</label></td>

	<td width="5"><input type="radio" id="address_type_S" name="posted_data[address_type]" value="S"<?php if ($this->_tpl_vars['search_prefilled']['address_type'] == 'S'): ?> checked="checked"<?php endif; ?> onclick="javascript:managedate('address',false)" /></td>
	<td class="OptionLabel"><label for="address_type_S"><?php echo $this->_tpl_vars['lng']['lbl_shipping']; ?>
</label></td>

	<td width="5"><input type="radio" id="address_type_both" name="posted_data[address_type]" value="Both"<?php if ($this->_tpl_vars['search_prefilled']['address_type'] == 'Both'): ?> checked="checked"<?php endif; ?> onclick="javascript:managedate('address',false)" /></td>
	<td class="OptionLabel"><label for="address_type_both"><?php echo $this->_tpl_vars['lng']['lbl_both']; ?>
</label></td>
</tr>
</table>
	</td>
</tr>

<tr>
	<td height="10" width="20%" class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_city']; ?>
:</td>
	<td width="10" height="10"><font class="CustomerMessage"></font></td>
	<td height="10" width="80%">
	<input type="text" maxlength="64" name="posted_data[city]" value="<?php echo $this->_tpl_vars['search_prefilled']['city']; ?>
" style="width:70%" />
	</td>
</tr>

<tr>
	<td height="10" width="20%" class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_state']; ?>
:</td>
	<td width="10" height="10"><font class="CustomerMessage"></font></td>
	<td height="10" width="80%">
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/states.tpl", 'smarty_include_vars' => array('states' => $this->_tpl_vars['states'],'name' => "posted_data[state]",'default' => $this->_tpl_vars['search_prefilled']['state'],'required' => 'N','style' => "style='width:70%'")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	</td>
</tr>

<tr>
	<td height="10" width="20%" class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_country']; ?>
:</td>
	<td width="10" height="10"><font class="CustomerMessage"></font></td>
	<td height="10" width="80%">
	<select name="posted_data[country]" style="width:70%">
		<option value="">[<?php echo $this->_tpl_vars['lng']['lbl_please_select_one']; ?>
]</option>
<?php unset($this->_sections['country_idx']);
$this->_sections['country_idx']['name'] = 'country_idx';
$this->_sections['country_idx']['loop'] = is_array($_loop=$this->_tpl_vars['countries']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['country_idx']['show'] = true;
$this->_sections['country_idx']['max'] = $this->_sections['country_idx']['loop'];
$this->_sections['country_idx']['step'] = 1;
$this->_sections['country_idx']['start'] = $this->_sections['country_idx']['step'] > 0 ? 0 : $this->_sections['country_idx']['loop']-1;
if ($this->_sections['country_idx']['show']) {
    $this->_sections['country_idx']['total'] = $this->_sections['country_idx']['loop'];
    if ($this->_sections['country_idx']['total'] == 0)
        $this->_sections['country_idx']['show'] = false;
} else
    $this->_sections['country_idx']['total'] = 0;
if ($this->_sections['country_idx']['show']):

            for ($this->_sections['country_idx']['index'] = $this->_sections['country_idx']['start'], $this->_sections['country_idx']['iteration'] = 1;
                 $this->_sections['country_idx']['iteration'] <= $this->_sections['country_idx']['total'];
                 $this->_sections['country_idx']['index'] += $this->_sections['country_idx']['step'], $this->_sections['country_idx']['iteration']++):
$this->_sections['country_idx']['rownum'] = $this->_sections['country_idx']['iteration'];
$this->_sections['country_idx']['index_prev'] = $this->_sections['country_idx']['index'] - $this->_sections['country_idx']['step'];
$this->_sections['country_idx']['index_next'] = $this->_sections['country_idx']['index'] + $this->_sections['country_idx']['step'];
$this->_sections['country_idx']['first']      = ($this->_sections['country_idx']['iteration'] == 1);
$this->_sections['country_idx']['last']       = ($this->_sections['country_idx']['iteration'] == $this->_sections['country_idx']['total']);
?>
		<option value="<?php echo $this->_tpl_vars['countries'][$this->_sections['country_idx']['index']]['country_code']; ?>
"<?php if ($this->_tpl_vars['search_prefilled']['country'] == $this->_tpl_vars['countries'][$this->_sections['country_idx']['index']]['country_code']): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['countries'][$this->_sections['country_idx']['index']]['country']; ?>
</option>
<?php endfor; endif; ?>
	</select>
	</td>
</tr>

<tr>
	<td height="10" width="20%" class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_zip_code']; ?>
:</td>
	<td width="10" height="10"><font class="CustomerMessage"></font></td>
	<td height="10" width="80%">
	<input type="text" maxlength="16" name="posted_data[zipcode]" value="<?php echo $this->_tpl_vars['search_prefilled']['zipcode']; ?>
" style="width:70%" />
	</td>
</tr>

<tr>
	<td height="10" width="20%" class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_phone']; ?>
/<?php echo $this->_tpl_vars['lng']['lbl_fax']; ?>
:</td>
	<td width="10" height="10"><font class="CustomerMessage"></font></td>
	<td height="10" width="80%">
	<input type="text" maxlength="25" name="posted_data[phone]" value="<?php echo $this->_tpl_vars['search_prefilled']['phone']; ?>
" style="width:70%" />
	</td>
</tr>

<tr>
	<td height="10" width="20%" class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_web_site']; ?>
:</td>
	<td width="10" height="10"><font class="CustomerMessage"></font></td>
	<td height="10" width="80%">
	<input type="text" maxlength="128" name="posted_data[url]" value="<?php echo $this->_tpl_vars['search_prefilled']['url']; ?>
" style="width:70%" />
	</td>
</tr>

<tr class="TableSubHead">
	<td height="10" class="FormButton"><?php echo $this->_tpl_vars['lng']['lbl_search_for_users_that_is']; ?>
:</td>
	<td height="10"></td>
	<td height="10">
<table cellpadding="0" cellspacing="0">
<tr>
	<td width="5"><input type="checkbox" id="posted_data_registration_date" name="posted_data[registration_date]" value="Y"<?php if ($this->_tpl_vars['search_prefilled']['registration_date'] != ""): ?> checked="checked"<?php endif; ?> onclick="javascript: managedate('date_type')" /></td>
	<td class="OptionLabel"><label for="posted_data_registration_date"><?php echo $this->_tpl_vars['lng']['lbl_registered']; ?>
</label></td>

	<td width="5"><input type="checkbox" id="posted_data_last_login_date" name="posted_data[last_login_date]" value="Y"<?php if ($this->_tpl_vars['search_prefilled']['last_login_date'] != ""): ?> checked="checked"<?php endif; ?> onclick="javascript:managedate('date_type')" /></td>
	<td class="OptionLabel"><label for="posted_data_last_login_date"><?php echo $this->_tpl_vars['lng']['lbl_last_logged_in']; ?>
</label></td>
</tr>
</table>
	</td>
</tr>

<tr class="TableSubHead">
	<td class="FormButton" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_during_date_period']; ?>
:</td>
	<td width="10">&nbsp;</td>
	<td>
<table cellpadding="0" cellspacing="0">
<tr>
	<td width="5"><input type="radio" id="date_period_M" name="posted_data[date_period]" value="M"<?php if ($this->_tpl_vars['search_prefilled']['date_period'] == "" || $this->_tpl_vars['search_prefilled']['date_period'] == 'M'): ?> checked="checked"<?php endif; ?> onclick="javascript:date_selected='M';managedate('date',true)" /></td>
	<td class="OptionLabel"><label for="date_period_M"><?php echo $this->_tpl_vars['lng']['lbl_this_month']; ?>
</label></td>

	<td width="5"><input type="radio" id="date_period_W" name="posted_data[date_period]" value="W"<?php if ($this->_tpl_vars['search_prefilled']['date_period'] == 'W'): ?> checked="checked"<?php endif; ?> onclick="javascript:date_selected='W';managedate('date',true)" /></td>
	<td class="OptionLabel"><label for="date_period_W"><?php echo $this->_tpl_vars['lng']['lbl_this_week']; ?>
</label></td>

	<td width="5"><input type="radio" id="date_period_D" name="posted_data[date_period]" value="D"<?php if ($this->_tpl_vars['search_prefilled']['date_period'] == 'D'): ?> checked="checked"<?php endif; ?> onclick="javascript:date_selected='D';managedate('date',true)" /></td>
	<td class="OptionLabel"><label for="date_period_D"><?php echo $this->_tpl_vars['lng']['lbl_today']; ?>
</label></td>
</tr>
<tr>
	<td width="5"><input type="radio" id="date_period_C" name="posted_data[date_period]" value="C"<?php if ($this->_tpl_vars['search_prefilled']['date_period'] == 'C'): ?> checked="checked"<?php endif; ?> onclick="javascript:date_selected='C';managedate('date',false)" /></td>
	<td colspan="7" class="OptionLabel"><label for="date_period_C"><?php echo $this->_tpl_vars['lng']['lbl_specify_period_below']; ?>
</label></td>
</tr>
</table>
	</td>
</tr>

<tr class="TableSubHead">
	<td class="FormButton" align="right" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_from']; ?>
:</td>
	<td width="10">&nbsp;</td>
	<td>
	<?php echo smarty_function_html_select_date(array('prefix' => 'Start','time' => $this->_tpl_vars['search_prefilled']['start_date'],'start_year' => $this->_tpl_vars['config']['Company']['start_year'],'end_year' => $this->_tpl_vars['config']['Company']['end_year']), $this);?>

	</td>
</tr>

<tr class="TableSubHead">
	<td class="FormButton" align="right" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_through']; ?>
:</td>
	<td width="10">&nbsp;</td>
	<td>
	<?php echo smarty_function_html_select_date(array('prefix' => 'End','time' => $this->_tpl_vars['search_prefilled']['end_date'],'start_year' => $this->_tpl_vars['config']['Company']['start_year'],'end_year' => $this->_tpl_vars['config']['Company']['end_year'],'display_days' => true), $this);?>

	</td>
</tr>


<tr>
	<td colspan="3"><br />
<?php echo $this->_tpl_vars['lng']['txt_users_search_note']; ?>

<script type="text/javascript" language="JavaScript 1.2">
<!--
<?php if ($this->_tpl_vars['search_prefilled'] == "" || $this->_tpl_vars['search_prefilled']['address_type'] == ""): ?>
managedate('address',true);
<?php endif; ?>
managedate('date_type');
<?php if (( $this->_tpl_vars['search_prefilled']['registration_date'] != "" || $this->_tpl_vars['search_prefilled']['last_login_date'] != "" ) && $this->_tpl_vars['search_prefilled']['date_period'] != 'C'): ?>
managedate('date', true);
<?php endif; ?>
-->
</script>
	</td>
</tr>

<tr>
	<td colspan="2">&nbsp;</td>
	<td>
	<input type="submit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_search'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" />
	&nbsp;&nbsp;&nbsp;
	<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_reset'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: reset_form('searchform', searchform_def);" /></td>
</tr>

</table>

	</td>
</tr>

</table>
</form>

<?php if ($this->_tpl_vars['search_prefilled']['need_advanced_options']): ?>
<script type="text/javascript" language="JavaScript 1.2">
<!--
visibleBox('1');
-->
</script>
<?php endif; ?>

<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_search_for_user'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<br />

<!-- SEARCH FORM DIALOG END -->

<?php endif; ?>

<!-- SEARCH RESULTS SUMMARY -->

<a name="results" />

<?php if ($this->_tpl_vars['mode'] == 'search'): ?>
<?php if ($this->_tpl_vars['total_items'] > '0'): ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_N_results_found'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'items', $this->_tpl_vars['total_items']) : smarty_modifier_substitute($_tmp, 'items', $this->_tpl_vars['total_items'])); ?>
<br />
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_displaying_X_Y_results'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'first_item', $this->_tpl_vars['first_item'], 'last_item', $this->_tpl_vars['last_item']) : smarty_modifier_substitute($_tmp, 'first_item', $this->_tpl_vars['first_item'], 'last_item', $this->_tpl_vars['last_item'])); ?>

<?php else: ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_N_results_found'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'items', 0) : smarty_modifier_substitute($_tmp, 'items', 0)); ?>

<?php endif; ?>
<?php endif; ?>

<?php if ($this->_tpl_vars['mode'] == 'search' && $this->_tpl_vars['users'] != ""): ?>

<!-- SEARCH RESULTS START -->

<br /><br />

<?php ob_start(); ?>

<div align="right"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/button.tpl", 'smarty_include_vars' => array('button_title' => $this->_tpl_vars['lng']['lbl_search_again'],'href' => "users.php")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div>

<?php if ($this->_tpl_vars['total_pages'] < 3): ?>
<br />
<?php else: ?>
<?php $this->assign('pagestr', "&page=".($this->_tpl_vars['navigation_page'])); ?>
<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "customer/main/navigation.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/check_all_row.tpl", 'smarty_include_vars' => array('style' => "line-height: 170%;",'form' => 'processuserform','prefix' => 'user')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<form action="process_user.php" method="post" name="processuserform">
<input type="hidden" name="mode" value="" />
<input type="hidden" name="pagestr" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['pagestr'])) ? $this->_run_mod_handler('amp', true, $_tmp) : smarty_modifier_amp($_tmp)); ?>
" />

<table cellpadding="2" cellspacing="1" width="100%">

<tr class="TableHead">
	<td>&nbsp;</td>
	<td><?php if ($this->_tpl_vars['search_prefilled']['sort_field'] == 'username'):  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/sort_pointer.tpl", 'smarty_include_vars' => array('dir' => $this->_tpl_vars['search_prefilled']['sort_direction'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>&nbsp;<?php endif; ?><a href="users.php?mode=search<?php echo ((is_array($_tmp=$this->_tpl_vars['pagestr'])) ? $this->_run_mod_handler('amp', true, $_tmp) : smarty_modifier_amp($_tmp)); ?>
&amp;sort=username"><?php echo $this->_tpl_vars['lng']['lbl_username']; ?>
</a></td>
	<td><?php if ($this->_tpl_vars['search_prefilled']['sort_field'] == 'name'):  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/sort_pointer.tpl", 'smarty_include_vars' => array('dir' => $this->_tpl_vars['search_prefilled']['sort_direction'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>&nbsp;<?php endif; ?><a href="users.php?mode=search<?php echo ((is_array($_tmp=$this->_tpl_vars['pagestr'])) ? $this->_run_mod_handler('amp', true, $_tmp) : smarty_modifier_amp($_tmp)); ?>
&amp;sort=name"><?php echo $this->_tpl_vars['lng']['lbl_name']; ?>
</a> / <?php if ($this->_tpl_vars['search_prefilled']['sort_field'] == 'email'):  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/sort_pointer.tpl", 'smarty_include_vars' => array('dir' => $this->_tpl_vars['search_prefilled']['sort_direction'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>&nbsp;<?php endif; ?><a href="users.php?mode=search<?php echo ((is_array($_tmp=$this->_tpl_vars['pagestr'])) ? $this->_run_mod_handler('amp', true, $_tmp) : smarty_modifier_amp($_tmp)); ?>
&amp;sort=email"><?php echo $this->_tpl_vars['lng']['lbl_email']; ?>
</a></td>
	<td><?php if ($this->_tpl_vars['search_prefilled']['sort_field'] == 'usertype'):  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/sort_pointer.tpl", 'smarty_include_vars' => array('dir' => $this->_tpl_vars['search_prefilled']['sort_direction'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>&nbsp;<?php endif; ?><a href="users.php?mode=search<?php echo ((is_array($_tmp=$this->_tpl_vars['pagestr'])) ? $this->_run_mod_handler('amp', true, $_tmp) : smarty_modifier_amp($_tmp)); ?>
&amp;sort=usertype"><?php echo $this->_tpl_vars['lng']['lbl_usertype']; ?>
</a></td>
	<td><?php if ($this->_tpl_vars['search_prefilled']['sort_field'] == 'last_login'):  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/sort_pointer.tpl", 'smarty_include_vars' => array('dir' => $this->_tpl_vars['search_prefilled']['sort_direction'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>&nbsp;<?php endif; ?><a href="users.php?mode=search<?php echo ((is_array($_tmp=$this->_tpl_vars['pagestr'])) ? $this->_run_mod_handler('amp', true, $_tmp) : smarty_modifier_amp($_tmp)); ?>
&amp;sort=last_login"><?php echo $this->_tpl_vars['lng']['lbl_last_logged_in']; ?>
</a></td>
	<td>Referral Details</td>
</tr>

<?php unset($this->_sections['cat_num']);
$this->_sections['cat_num']['name'] = 'cat_num';
$this->_sections['cat_num']['loop'] = is_array($_loop=$this->_tpl_vars['users']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cat_num']['show'] = true;
$this->_sections['cat_num']['max'] = $this->_sections['cat_num']['loop'];
$this->_sections['cat_num']['step'] = 1;
$this->_sections['cat_num']['start'] = $this->_sections['cat_num']['step'] > 0 ? 0 : $this->_sections['cat_num']['loop']-1;
if ($this->_sections['cat_num']['show']) {
    $this->_sections['cat_num']['total'] = $this->_sections['cat_num']['loop'];
    if ($this->_sections['cat_num']['total'] == 0)
        $this->_sections['cat_num']['show'] = false;
} else
    $this->_sections['cat_num']['total'] = 0;
if ($this->_sections['cat_num']['show']):

            for ($this->_sections['cat_num']['index'] = $this->_sections['cat_num']['start'], $this->_sections['cat_num']['iteration'] = 1;
                 $this->_sections['cat_num']['iteration'] <= $this->_sections['cat_num']['total'];
                 $this->_sections['cat_num']['index'] += $this->_sections['cat_num']['step'], $this->_sections['cat_num']['iteration']++):
$this->_sections['cat_num']['rownum'] = $this->_sections['cat_num']['iteration'];
$this->_sections['cat_num']['index_prev'] = $this->_sections['cat_num']['index'] - $this->_sections['cat_num']['step'];
$this->_sections['cat_num']['index_next'] = $this->_sections['cat_num']['index'] + $this->_sections['cat_num']['step'];
$this->_sections['cat_num']['first']      = ($this->_sections['cat_num']['iteration'] == 1);
$this->_sections['cat_num']['last']       = ($this->_sections['cat_num']['iteration'] == $this->_sections['cat_num']['total']);
?>
<?php $this->assign('_usertype', $this->_tpl_vars['users'][$this->_sections['cat_num']['index']]['usertype']); ?>
<?php if ($this->_tpl_vars['_usertype'] == 'P' && $this->_tpl_vars['single_mode'] == ""): ?>
<?php $this->assign('products', $this->_tpl_vars['users'][$this->_sections['cat_num']['index']]['products']); ?>
<?php else: ?>
<?php $this->assign('products', ""); ?>
<?php endif; ?>

<tr<?php echo smarty_function_cycle(array('values' => ', class="TableSubHead"'), $this);?>
>
	<td width="5"><input type="checkbox" name="user[<?php echo $this->_tpl_vars['users'][$this->_sections['cat_num']['index']]['login']; ?>
]"<?php if ($this->_tpl_vars['users'][$this->_sections['cat_num']['index']]['login'] == $this->_tpl_vars['login']): ?> disabled="disabled"<?php endif; ?> /></td>
	<td><a href="user_modify.php?user=<?php echo ((is_array($_tmp=$this->_tpl_vars['users'][$this->_sections['cat_num']['index']]['login'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
&amp;usertype=<?php echo $this->_tpl_vars['users'][$this->_sections['cat_num']['index']]['usertype'];  echo ((is_array($_tmp=$this->_tpl_vars['pagestr'])) ? $this->_run_mod_handler('amp', true, $_tmp) : smarty_modifier_amp($_tmp)); ?>
" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_modify_profile'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><?php echo $this->_tpl_vars['users'][$this->_sections['cat_num']['index']]['login']; ?>
</a></td>
	<td><a href="user_modify.php?user=<?php echo ((is_array($_tmp=$this->_tpl_vars['users'][$this->_sections['cat_num']['index']]['login'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
&amp;usertype=<?php echo $this->_tpl_vars['users'][$this->_sections['cat_num']['index']]['usertype'];  echo ((is_array($_tmp=$this->_tpl_vars['pagestr'])) ? $this->_run_mod_handler('amp', true, $_tmp) : smarty_modifier_amp($_tmp)); ?>
" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_modify_profile'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><font class="ItemsList"><?php echo $this->_tpl_vars['users'][$this->_sections['cat_num']['index']]['firstname']; ?>
 <?php echo $this->_tpl_vars['users'][$this->_sections['cat_num']['index']]['lastname']; ?>
</font></a> / <?php echo $this->_tpl_vars['users'][$this->_sections['cat_num']['index']]['email']; ?>
</td>
	<td>
	<span title="<?php echo ((is_array($_tmp=@$this->_tpl_vars['users'][$this->_sections['cat_num']['index']]['membership'])) ? $this->_run_mod_handler('default', true, $_tmp, @$this->_tpl_vars['lng']['lbl_no_membership']) : smarty_modifier_default($_tmp, @$this->_tpl_vars['lng']['lbl_no_membership'])); ?>
"><?php echo $this->_tpl_vars['usertypes'][$this->_tpl_vars['_usertype']]; ?>
</span>
<?php if ($this->_tpl_vars['_usertype'] == 'B'): ?>
<br /><font class="SmallText"><i>(<?php if ($this->_tpl_vars['users'][$this->_sections['cat_num']['index']]['status'] == 'Q'):  echo $this->_tpl_vars['lng']['lbl_unapproved'];  elseif ($this->_tpl_vars['users'][$this->_sections['cat_num']['index']]['status'] == 'D'):  echo $this->_tpl_vars['lng']['lbl_declined'];  elseif ($this->_tpl_vars['users'][$this->_sections['cat_num']['index']]['status'] == 'Y'):  echo $this->_tpl_vars['lng']['lbl_approved'];  else:  echo $this->_tpl_vars['lng']['lbl_disabled'];  endif; ?>)</i></font>
<?php elseif ($this->_tpl_vars['users'][$this->_sections['cat_num']['index']]['status'] != 'Y' && $this->_tpl_vars['users'][$this->_sections['cat_num']['index']]['status'] != 'A'): ?>
<br /><font class="SmallText"><i>(<?php echo $this->_tpl_vars['lng']['lbl_account_status_suspended']; ?>
)</i></font>
<?php endif; ?>
<?php if ($this->_tpl_vars['products'] != ""): ?> <span style="white-space: nowrap;">(<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_N_products'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'products', $this->_tpl_vars['products']) : smarty_modifier_substitute($_tmp, 'products', $this->_tpl_vars['products'])); ?>
)</span><?php endif; ?>
	</td>
	<td nowrap="nowrap"><?php if (( $this->_tpl_vars['users'][$this->_sections['cat_num']['index']]['last_login'] != 0 )):  echo ((is_array($_tmp=$this->_tpl_vars['users'][$this->_sections['cat_num']['index']]['last_login'])) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['config']['Appearance']['datetime_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['config']['Appearance']['datetime_format']));  else:  echo $this->_tpl_vars['lng']['lbl_never_logged_in'];  endif; ?></td>
	<td ><a href="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/referral_stats_for_user.php?login=<?php echo ((is_array($_tmp=$this->_tpl_vars['users'][$this->_sections['cat_num']['index']]['login'])) ? $this->_run_mod_handler('urlencode', true, $_tmp) : smarty_modifier_urlencode($_tmp)); ?>
" target="_blank">Referral Details</a></td>
</tr>

<?php endfor; endif; ?>

<tr>
	<td colspan="5" class="SubmitBox">
	<!--<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_delete_selected'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: if (checkMarks(this.form, new RegExp('^user\\[.+\\]', 'gi'))) submitForm(this, 'delete');" /> -->
	<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_export'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: if (checkMarks(this.form, new RegExp('^user\\[.+\\]', 'gi'))) submitForm(this, 'export');" />
	<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_export_all_found'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: self.location='users.php?mode=search&amp;export=export_found';" />
	</td>
</tr>

<tr>
	<td colspan="5">
<br /><br /><br />
<table cellpadding="5">

<tr>
	<td><font class="FormButton"><?php echo $this->_tpl_vars['lng']['lbl_reg_chpass_admin']; ?>
</font></td>
	<td><input type="checkbox" name="op_change_password" /></td>
</tr>

<tr>
	<td><font class="FormButton"><?php echo $this->_tpl_vars['lng']['lbl_reg_account_status_admin']; ?>
</font></td>
	<td>
	<select name="op_change_status">
		<option value=""><?php echo $this->_tpl_vars['lng']['lbl_reg_do_not_change_admin']; ?>
</option>
		<option value="N" class="UsersActionDisable"><?php echo $this->_tpl_vars['lng']['lbl_reg_account_status_suspend']; ?>
</option>
		<option value="Y" class="UsersActionEnable"><?php echo $this->_tpl_vars['lng']['lbl_reg_account_status_enable']; ?>
</option>
	</select>
	</td>
</tr>
<tr>
	<td>
	<font class="FormButton"><?php echo $this->_tpl_vars['lng']['lbl_reg_account_activity_admin']; ?>
</font>
	<br />
	<font class="SmallText"><?php echo $this->_tpl_vars['lng']['lbl_reg_account_activity_note_admin']; ?>
</font>
	</td>
	<td>
	<select name="op_change_activity">
		<option value=""><?php echo $this->_tpl_vars['lng']['lbl_reg_do_not_change_admin']; ?>
</option>
		<option value="N" class="UsersActionDisable"><?php echo $this->_tpl_vars['lng']['lbl_reg_account_activity_disable']; ?>
</option>
		<option value="Y" class="UsersActionEnable"><?php echo $this->_tpl_vars['lng']['lbl_reg_account_activity_enable']; ?>
</option>
	</select>
	</td>
</tr>
</table>
	</td>
</tr>
<tr>
	<td colspan="5">
<table cellpadding="2" cellspacing="1">
<tr>
  <td><input type="radio" id="for_users_S" name="for_users" value="S" checked="checked" /></td>
  <td class="OptionLabel"><label for="for_users_S"><?php echo $this->_tpl_vars['lng']['lbl_of_selected_users']; ?>
</label></td>
  <td>&nbsp;&nbsp;</td>
  <td><input type="radio" id="for_users_A" name="for_users" value="A" /></td>
  <td class="OptionLabel"><label for="for_users_A"><?php echo $this->_tpl_vars['lng']['lbl_of_all_found_users']; ?>
</label></td>
</tr>
</table>
<br />
<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_apply'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: submitForm(this, 'group_operation');" />
</td>
</tr>

</table>
</form>

<br />

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "customer/main/navigation.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_search_results'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- SEARCH RESULTS START -->

<?php endif; ?>

<br />
