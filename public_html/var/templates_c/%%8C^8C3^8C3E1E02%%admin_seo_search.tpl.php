<?php /* Smarty version 2.6.12, created on 2018-01-16 12:30:18
         compiled from admin/main/admin_seo_search.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'admin/main/admin_seo_search.tpl', 113, false),array('modifier', 'replace', 'admin/main/admin_seo_search.tpl', 165, false),array('function', 'cycle', 'admin/main/admin_seo_search.tpl', 146, false),)), $this); ?>
<?php func_load_lang($this, "admin/main/admin_seo_search.tpl","txt_featured_products,lbl_search,txt_no_featured_products,lbl_productstyle_heading"); ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "main/popup_product.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['http_location']; ?>
/skin1/js_script/jquery.js">
</script>
<?php echo '
  <script language="Javascript">
      function confirmDelete()
      {
          var flag = confirm(\'Are you sure you want to delete?\')
          if(!flag)
          {
             return false;
          }
          else
          {
          	 document.productstyleform.mode.value = \'delete\';
          	 document.productstyleform.submit();
          	 return true;
          }
      }
     
	function getHTTPObject(){
		if(window.XMLHttpRequest){
			http=new XMLHttpRequest();}
		else if(window.ActiveXObject){
			http=new ActiveXObject("Microsoft.XMLHTTP");}
		return http;
	}

	function updatesearchtable(key_id,fieldArray){		
		//to make ajax on check box selected	
		var fieldvalue = \'\';
		var qString = \'\';
		for(i=0;i<fieldArray.length;i++){
			if(typeof document.getElementById(fieldArray[i]+\'_\'+key_id) != "undefined" && document.getElementById(fieldArray[i]+\'_\'+key_id) != null){
				fieldvalue = document.getElementById(fieldArray[i]+\'_\'+key_id).value;
				if(i==fieldArray.length-1){
					qString += fieldArray[i]+\'=\'+encodeURIComponent(fieldvalue);	
				} else {
					qString += fieldArray[i]+\'=\'+encodeURIComponent(fieldvalue)+\'&\';
				}	
			}
		}			
		
		var http = new getHTTPObject();			
		var params = "keyid="+key_id+"&"+qString;			
		var searchURL = http_loc+"/admin/admin_update_search_seo.php";			
		
		http.open("POST", searchURL, true); 
		http.setRequestHeader(\'Content-Type\',\'application/x-www-form-urlencoded\'); 
		http.onreadystatechange = function(){
			if(http.readyState==4){
				if(http.status==200){
					var result=http.responseText;						
				}
			}    	
		}
		http.send(params);			
	}
	
function checkall(){

if($("#checkallbox").attr("checked"))
	$(".abc").attr("checked","checked");
else
	$(".abc").removeAttr("checked");

}
  </script>
'; ?>

<a name="featured" />

<?php echo $this->_tpl_vars['lng']['txt_featured_products']; ?>


<br /><br />
<div class="pagination" style="float:right;margin-bottom:5px;">
<!--pagination-->
	
		<?php if ($this->_tpl_vars['totalpages'] > 1):  echo $this->_tpl_vars['paginator'];  endif; ?>
	
</div><!--pagination-->
<?php ob_start(); ?>

<form action="admin_seo_search.php" method="post" name="Addkeywordform" enctype="multipart/form-data">
<div style="font-size:14px;color:red"><?php echo $this->_tpl_vars['addkeyword_message']; ?>
</div>
<input type="hidden" name="mode"  value="addnewkeyword"/>
<div>
	<div style="float:left;width:6%;font-weight:bold;"> Add new keyword</div>
	<div style="float:left;">
		<input type="text" value="" name="addNewKeyword" style='width:275px;height:20px;' />
	</div>
	<div style="float:left;margin-left:5px;">
		<input type="submit" name="AddKeyword" value="Add New Keyword" />
	</div>
	<div style="float:none;clear:both;">&nbsp;</div>
</div>
</form>
<form action="admin_seo_search.php" method="post" name="stylesearchform" enctype="multipart/form-data">
<input type="hidden" name="mode"/>
<table cellpadding="3" cellspacing="1" width="100%">
<tr>
	<td align="left" width="20"><b>Keyword</b></td>
	<td align="left" width="50"><input type="text"  style='width:275px;height:20px;' name="keyword" value="<?php echo $this->_tpl_vars['selectedkeyword']; ?>
" /></td>
	<td align="left" width="80"><b>Search Type</b></td>
	<td align="left" width="30">
		<select name="searchtype">
			<option value="keyword" <?php if ($this->_tpl_vars['searchtype'] == 'keyword'): ?>selected<?php endif; ?> >keyword</option>
			<option value="keyid" <?php if ($this->_tpl_vars['searchtype'] == 'keyid'): ?>selected<?php endif; ?>>keyid</option>
			
		</select>
	</td>
	<td class="SubmitBox" style="padding-bottom:10px;">
		<input type="submit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_search'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: document.stylesearchform.mode.value = 'search'; document.stylesearchform.submit();"/>
	</td>
</tr>
</table>
</form>


<form action="admin_seo_search.php" method="post" name="productstyleform" enctype="multipart/form-data">
<input type="hidden" name="mode"  />
<input type="hidden" name="updatemode" value="update"/>

<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="5%">ID<!--input type='checkbox' onchange='javascript:checkall();' id='checkallbox'/--></td>
	<td width="20%">Keyword</td>
    <td width="20%">Page Title</td>
	<td width="20%">Page H1</td>
    <td width="20%">Meta Dsrciption</td>
    <td width="20%">Meta Keyword</td>
	<td width="20%">Search URL</td>
	<td width="5%" align="center"><a href='<?php echo $this->_tpl_vars['sort_count_of_products']; ?>
'>Count of products</a></td>
	<td width="5%" align="center"><a href='<?php echo $this->_tpl_vars['sort_count_of_searches']; ?>
'>Count of searches</a></td>
	<td width="40%" align="center">Description</td>
	<td width="20%" align="center">Banner</td>
	<td width="30%" align="center">HTML Banner</td>
	<td width="10%" align="center">Action</td>
</tr>

<?php if ($this->_tpl_vars['products']): ?>

<?php unset($this->_sections['prod_num']);
$this->_sections['prod_num']['name'] = 'prod_num';
$this->_sections['prod_num']['loop'] = is_array($_loop=$this->_tpl_vars['products']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['prod_num']['show'] = true;
$this->_sections['prod_num']['max'] = $this->_sections['prod_num']['loop'];
$this->_sections['prod_num']['step'] = 1;
$this->_sections['prod_num']['start'] = $this->_sections['prod_num']['step'] > 0 ? 0 : $this->_sections['prod_num']['loop']-1;
if ($this->_sections['prod_num']['show']) {
    $this->_sections['prod_num']['total'] = $this->_sections['prod_num']['loop'];
    if ($this->_sections['prod_num']['total'] == 0)
        $this->_sections['prod_num']['show'] = false;
} else
    $this->_sections['prod_num']['total'] = 0;
if ($this->_sections['prod_num']['show']):

            for ($this->_sections['prod_num']['index'] = $this->_sections['prod_num']['start'], $this->_sections['prod_num']['iteration'] = 1;
                 $this->_sections['prod_num']['iteration'] <= $this->_sections['prod_num']['total'];
                 $this->_sections['prod_num']['index'] += $this->_sections['prod_num']['step'], $this->_sections['prod_num']['iteration']++):
$this->_sections['prod_num']['rownum'] = $this->_sections['prod_num']['iteration'];
$this->_sections['prod_num']['index_prev'] = $this->_sections['prod_num']['index'] - $this->_sections['prod_num']['step'];
$this->_sections['prod_num']['index_next'] = $this->_sections['prod_num']['index'] + $this->_sections['prod_num']['step'];
$this->_sections['prod_num']['first']      = ($this->_sections['prod_num']['iteration'] == 1);
$this->_sections['prod_num']['last']       = ($this->_sections['prod_num']['iteration'] == $this->_sections['prod_num']['total']);
?>
<?php if ($this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['count_of_products'] < 3): ?>
<tr <?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this);?>
 style='background-color:red;'>
<?php else: ?>
<tr <?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this);?>
>
<?php endif; ?>
        <input type="hidden" name="posted_data[<?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['key_id']; ?>
][key_id]" value=<?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['key_id']; ?>
 />
	<td><?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['key_id']; ?>
<br><!--input class='abc' type="checkbox" name="posted_data[<?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['key_id']; ?>
][key_id_checkbox]" id="<?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['key_id']; ?>
"/--></td>
	<td align="center"><b><?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['keyword']; ?>
</b></td>
	<td align="center">
		<textarea style='width:400px;height:100px;' id="page_title_<?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['key_id']; ?>
" name="posted_data[<?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['key_id']; ?>
][page_title]" > <?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['page_title']; ?>
</textarea>
	</td>
	<td align="center">
		<textarea style='width:100px;height:100px;' id="page_h1_<?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['key_id']; ?>
" name="posted_data[<?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['key_id']; ?>
][page_h1]"> <?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['page_h1']; ?>
</textarea>
	</td>
	<td align="center">
		<textarea style='width:400px;height:100px;' id="meta_description_<?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['key_id']; ?>
" name="posted_data[<?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['key_id']; ?>
][meta_description]"> <?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['meta_description']; ?>
</textarea>
	</td>
	<td align="center">
		<textarea style='width:400px;height:100px;' id="meta_keyword_<?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['key_id']; ?>
" name="posted_data[<?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['key_id']; ?>
][meta_keyword]"> <?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['meta_keyword']; ?>
</textarea>
	</td>
	<td align="center"><b><a href='http://www.myntra.com/<?php echo ((is_array($_tmp=$this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['keyword'])) ? $this->_run_mod_handler('replace', true, $_tmp, ' ', "-") : smarty_modifier_replace($_tmp, ' ', "-")); ?>
/' target='_blank'>http://www.myntra.com/<?php echo ((is_array($_tmp=$this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['keyword'])) ? $this->_run_mod_handler('replace', true, $_tmp, ' ', "-") : smarty_modifier_replace($_tmp, ' ', "-")); ?>
/</a></b></td>
	<td align="center"><b><?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['count_of_products']; ?>
</b></td>
	<td align="center"><b><?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['count_of_searches']; ?>
</b></td>
	<td align="center">
		<textarea style='width:400px;height:100px;' id="description_<?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['key_id']; ?>
" name="posted_data[<?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['key_id']; ?>
][description]"><?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['description']; ?>
</textarea>
	</td>
	<td align="center">
	    <img src="../<?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['top_banner']; ?>
" alt="" height="50px" width="50px"><br>
		<input type="file" name="<?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['key_id']; ?>
_banner" id="banner_<?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['key_id']; ?>
">
	</td>
	<td align="center">
		<textarea style='width:400px;height:100px;' id="page_promotion_<?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['key_id']; ?>
" name="posted_data[<?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['key_id']; ?>
][page_promotion]"><?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['page_promotion']; ?>
</textarea>
	</td>
	<td align="center">	    
		<input type="button" name="update" value="Update" onclick="updatesearchtable(<?php echo $this->_tpl_vars['products'][$this->_sections['prod_num']['index']]['key_id']; ?>
,Array('page_title','page_h1','meta_description','meta_keyword','is_valid','description','page_promotion'));">
	</td>

</tr>

<?php endfor; endif; ?>

<td colspan="5"><br /><br /></td>


<?php else: ?>

<tr>
<td colspan="5" align="center"><?php echo $this->_tpl_vars['lng']['txt_no_featured_products']; ?>
</td>
</tr>

<?php endif; ?>


</table>
</form>

<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_productstyle_heading'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="pagination" style="float:right;margin-bottom:5px;">
<!--pagination-->
	
		<?php if ($this->_tpl_vars['totalpages'] > 1):  echo $this->_tpl_vars['paginator'];  endif; ?>
	
</div><!--pagination-->
<br /><br />

