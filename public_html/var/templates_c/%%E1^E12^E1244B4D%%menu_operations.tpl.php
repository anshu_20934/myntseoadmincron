<?php /* Smarty version 2.6.12, created on 2017-03-24 12:22:29
         compiled from admin/menu_operations.tpl */ ?>
<?php ob_start(); ?>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/OperationSLAReport.php" class="VertMenuItems">Operations SLA Dashboard</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/orders.php" class="VertMenuItems">Order Search</a></li>
<BR><BR>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/operations/item_assignment.php" class="VertMenuItems">Item Assignment</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/upload_qa_report.php" class="VertMenuItems">QA Worksheet</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/cancel-lost-orders.php" class="VertMenuItems">Lost Orders</a></li>
<!--
    <li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/operations/downloader_dashboard.php" class="VertMenuItems">Downloader Dashboard</a></li>
    <li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/assignee_worksheet.php" class="VertMenuItems">Assignee Worksheet</a></li>
    <li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/location_worksheet.php" class="VertMenuItems">Location Worksheet</a></li>
    <li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/assigned_items_report.php" class="VertMenuItems">Assigned item report</a></li>
    <li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/all_queued_orders.php" class="VertMenuItems">All queued orders</a></li>
    <BR>
    <li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/operations/sku_picklist_report.php" class="VertMenuItems">SKU Picklist report</a></li>
    <BR>
    <li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/operations/processitemexit.php" class="VertMenuItems">Material Exit Movement</a></li>
    <BR>
    <li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/oosordersreport.php" class="VertMenuItems">OOS Orders Report</a></li>     
-->
<BR>
<BR>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/courier_serviceability.php" class="VertMenuItems">Courier Serviceability</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/courier_status_info.php" class="VertMenuItems">Courier Status Info</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/orders_ship_report.php?mode=set_courier_pref" class="VertMenuItems">Courier Preference setup</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/ready_to_ship_report.php?mode=gen_rts_report" class="VertMenuItems">Ready To Ship Report</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/updateordersinbulk.php" class="VertMenuItems">Bulk Uploader(Ship Orders)</a></li>
<BR>
<BR>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/shipped_orders.php" class="VertMenuItems">Shipped Orders Report</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/update_delivered_orders.php" class="VertMenuItems">Mark Shipped Orders as Delivered</a></li>
<BR>
<BR>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/upload_payment_completed_orders.php" class="VertMenuItems">Upload paid orders</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/upload_payment_completed_giftorders.php" class="VertMenuItems">Upload paid Gift Orders</a></li>
<BR>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/operations/addordersbulkcomment.php" class="VertMenuItems">Add Orders Comment in bulk</a></li>
<BR>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/operations/updatecoreitemsbulk.php" class="VertMenuItems">Warehouse Items Bulk Changes</a></li>
<BR>
<BR>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/manage_resources.php" class="VertMenuItems">Manage Resources</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/item_assignee.php" class="VertMenuItems">Add Assignee</a></li>
<?php $this->_smarty_vars['capture']['menu'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu.tpl", 'smarty_include_vars' => array('dingbats' => "dingbats_categorie.gif",'menu_title' => 'Operations','menu_content' => $this->_smarty_vars['capture']['menu'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>