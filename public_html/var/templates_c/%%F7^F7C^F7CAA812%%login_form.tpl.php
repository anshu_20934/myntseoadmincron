<?php /* Smarty version 2.6.12, created on 2017-03-28 17:51:21
         compiled from main/login_form.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'default', 'main/login_form.tpl', 20, false),)), $this); ?>
<?php func_load_lang($this, "main/login_form.tpl","lbl_login,lbl_password,lbl_submit,lbl_recover_password"); ?><?php if ($this->_tpl_vars['config']['Security']['use_https_login'] == 'Y' && $this->_tpl_vars['usertype'] == 'C'): ?>
<?php $this->assign('form_url', $this->_tpl_vars['https_location']); ?>
<?php else: ?>
<?php $this->assign('form_url', $this->_tpl_vars['current_location']); ?>
<?php endif; ?>

<?php if ($this->_tpl_vars['referer'] != ''): ?>
<?php $this->assign('redirect', $this->_tpl_vars['referer']); ?>
<?php endif; ?>

<form action="/include/login.php" method="post" name="errorform">
<input type="hidden" name="is_remember" value="<?php echo $this->_tpl_vars['is_remember']; ?>
" />
<input type="hidden" name="filetype" value="<?php echo $this->_tpl_vars['pagename']; ?>
">

         <div class="super"><p>account information</p> </div>
	<div class="head"><p>please login </p></div>
	<div class="links"><div class="legend"><p><?php echo $this->_tpl_vars['lng']['lbl_login']; ?>
<span class="mandatory">*</span></p></div>
            <div class="field"><label>
              <input  type="text" name="username"  value="<?php echo ((is_array($_tmp=@$this->_config[0]['vars']['default_login'])) ? $this->_run_mod_handler('default', true, $_tmp, @$this->_tpl_vars['remember_login'], 'escape', 'htmlall') : smarty_modifier_default($_tmp, @$this->_tpl_vars['remember_login'], 'escape', 'htmlall')); ?>
" />
            </label> </div><div class="clearall"></div>
          </div>

	<div class="links"><div class="legend"><p><?php echo $this->_tpl_vars['lng']['lbl_password']; ?>
<span class="mandatory">*</span></p></div>
            <div class="field"><label>
              <input  type="password" name="password"  value="<?php echo $this->_config[0]['vars']['default_password']; ?>
" />
            </label> </div><div class="clearall"></div>

          </div>



<?php if ($this->_tpl_vars['active_modules']['Simple_Mode'] != "" && $this->_tpl_vars['usertype'] != 'C' && $this->_tpl_vars['usertype'] != 'B'): ?>
<input type="hidden" name="usertype" value="P" />
<?php else: ?>
<input type="hidden" name="usertype" value="<?php echo $this->_tpl_vars['usertype']; ?>
" />
<?php endif; ?>
<input type="hidden" name="redirect" value="<?php echo $this->_tpl_vars['redirect']; ?>
" />
<input type="hidden" name="appreferer" value="<?php echo $this->_tpl_vars['referer']; ?>
" />
<input type="hidden" name="encodedUrl" value="<?php echo $this->_tpl_vars['encodedUrl']; ?>
" />

<input type="hidden" name="mode" value="login" />



	

<?php if ($this->_tpl_vars['js_enabled']): ?>

<div class="button">
         <input class="submit" type="submit" name="signinButton" value="signin" border="0"  onClick="javascript:document.errorform.submit()">
	 <br><br>
	 <?php if ($this->_tpl_vars['encodedUrl']): ?>
		New user - register <input class="submit" type="button" name="registerButton" value="here" border="0"  onClick="javascript:window.location.href= 'register.php?redirecturl=<?php echo $this->_tpl_vars['encodedUrl']; ?>
' ">
	<?php else: ?>
                New user - register <input class="submit" type="button" name="registerButton" value="here" border="0"  onClick="javascript:window.location.href= 'register.php' ">
	<?php endif; ?>

</div>
<div class="foot"></div>

<?php else: ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "submit_wo_js.tpl", 'smarty_include_vars' => array('value' => $this->_tpl_vars['lng']['lbl_submit'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>

</form>

<div class="subhead"></div>

<div class="links">
<p><?php echo $this->_tpl_vars['lng']['lbl_recover_password']; ?>

<input class="submit" type="button" name="submitButton" value="go" border="0"  onClick="javascript:window.location.href='help.php?section=Password_Recovery'"></p>
</div>
<div class="foot"></div>