<?php /* Smarty version 2.6.12, created on 2017-03-27 12:58:50
         compiled from main/register_account.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'default', 'main/register_account.tpl', 94, false),)), $this); ?>
<?php func_load_lang($this, "main/register_account.tpl","txt_anonymous_account_msg,lbl_username,lbl_password,lbl_confirm_password,lbl_email,lbl_username,lbl_password,lbl_confirm_password,lbl_email,lbl_account_status,lbl_account_status_suspended,lbl_account_status_enabled,lbl_account_status_not_approved,lbl_account_status_declined,lbl_account_activity,lbl_account_activity_enabled,lbl_account_activity_disabled,lbl_reg_chpass"); ?><?php if ($this->_tpl_vars['hide_account_section'] != 'Y'): ?>

<?php if ($this->_tpl_vars['hide_header'] == ""): ?>


<?php endif; ?>

<?php if ($this->_tpl_vars['anonymous'] != "" && $this->_tpl_vars['config']['General']['disable_anonymous_checkout'] != 'Y'): ?>


<tr>
<td colspan="3"><?php echo $this->_tpl_vars['lng']['txt_anonymous_account_msg']; ?>
</td>
</tr>

<?php endif; ?>

<?php if ($this->_tpl_vars['userinfo']['login'] == $this->_tpl_vars['login'] && $this->_tpl_vars['login'] && $this->_tpl_vars['userinfo']['usertype'] != 'C'): ?>


<tr style="display: none;">
<td>
<input type="hidden" name="membershipid" value="<?php echo $this->_tpl_vars['userinfo']['membershipid']; ?>
" />
<input type="hidden" name="pending_membershipid" value="<?php echo $this->_tpl_vars['userinfo']['pending_membershipid']; ?>
" />
</td>
</tr>

<?php else: ?>




<?php endif; ?>

<?php if ($this->_tpl_vars['anonymous'] != "" && $this->_tpl_vars['config']['General']['disable_anonymous_checkout'] != 'Y'): ?>


<tr>
<td align="right"><?php echo $this->_tpl_vars['lng']['lbl_username']; ?>
</td>
<td>&nbsp;</td>
<td nowrap="nowrap">
<input type="text" name="uname" size="32" maxlength="32" value="<?php echo $this->_tpl_vars['userinfo']['login']; ?>
" />
</td>
</tr>

<tr>
<td align="right"><?php echo $this->_tpl_vars['lng']['lbl_password']; ?>
</td>
<td>&nbsp;</td>
<td nowrap="nowrap"><input type="password" name="passwd1" size="32" maxlength="32" value="<?php echo $this->_tpl_vars['userinfo']['passwd1']; ?>
" />
</td>
</tr>

<tr>
<td align="right"><?php echo $this->_tpl_vars['lng']['lbl_confirm_password']; ?>
</td>
<td>&nbsp;</td>
<td nowrap="nowrap"><input type="password" name="passwd2" size="32" maxlength="32" value="<?php echo $this->_tpl_vars['userinfo']['passwd2']; ?>
" />
</td>
</tr>

<?php if ($this->_tpl_vars['default_fields']['email']['avail'] == 'Y'): ?>
<tr>
<td align="right"><?php echo $this->_tpl_vars['lng']['lbl_email']; ?>
</td>
<td><?php if ($this->_tpl_vars['default_fields']['email']['required'] == 'Y'): ?><font class="Star">*</font><?php else: ?>&nbsp;<?php endif; ?></td>
<td nowrap="nowrap">
<input type="text" id="email" name="email" size="32" maxlength="128" value="<?php echo $this->_tpl_vars['userinfo']['email']; ?>
" />
<?php if ($this->_tpl_vars['emailerror'] != "" || ( $this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['email'] == "" && $this->_tpl_vars['default_fields']['email']['required'] == 'Y' )): ?><font class="Star">&lt;&lt;</font><?php endif; ?>
</td>
</tr>
<?php endif; ?>


<?php else: ?>


 <div class="subhead"><p>login details</p></div>

<div class="links"><div class="legend"><p><?php echo $this->_tpl_vars['lng']['lbl_username']; ?>
<span class="mandatory">*</span></p></div>

            <div class="field"><label>
              
	      <?php if ($this->_tpl_vars['userinfo']['login'] != "" || ( $this->_tpl_vars['login'] == $this->_tpl_vars['userinfo']['uname'] && $this->_tpl_vars['login'] != '' )): ?>
<b><?php echo ((is_array($_tmp=@$this->_tpl_vars['userinfo']['login'])) ? $this->_run_mod_handler('default', true, $_tmp, @$this->_tpl_vars['userinfo']['uname']) : smarty_modifier_default($_tmp, @$this->_tpl_vars['userinfo']['uname'])); ?>
</b>
<input type="hidden" name="uname" value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['userinfo']['login'])) ? $this->_run_mod_handler('default', true, $_tmp, @$this->_tpl_vars['userinfo']['uname']) : smarty_modifier_default($_tmp, @$this->_tpl_vars['userinfo']['uname'])); ?>
" />
<?php else: ?>
<input  type="text" id="uname" name="uname"  value="<?php if ($this->_tpl_vars['userinfo']['uname']):  echo $this->_tpl_vars['userinfo']['uname'];  else:  echo $this->_tpl_vars['userinfo']['login'];  endif; ?>" />

<?php if (( $this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['uname'] == "" && $this->_tpl_vars['userinfo']['login'] == "" ) || $this->_tpl_vars['reg_error'] == 'U'):  endif; ?>
<?php endif; ?>
            </label></div><div class="clearall"></div>
          </div>

          <div class="links"><div class="legend"><p><?php echo $this->_tpl_vars['lng']['lbl_password']; ?>
<span class="mandatory">*</span></p></div>
            <div class="field"><label>
<input  type="password" id="passwd1" name="passwd1"  value="<?php echo $this->_tpl_vars['userinfo']['passwd1']; ?>
" />
&nbsp;<span class="mandatory">(minimum 6 character)</span>
<?php if ($this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['passwd1'] == ""):  endif; ?> 


           </label></div><div class="clearall"></div>

          </div>

          <div class="links"><div class="legend"><p><?php echo $this->_tpl_vars['lng']['lbl_confirm_password']; ?>
<span class="mandatory">*</span></p></div>
            <div class="field"><label>

	    <input type="password" id="passwd2" name="passwd2"  value="<?php echo $this->_tpl_vars['userinfo']['passwd2']; ?>
" />
<?php if ($this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['passwd2'] == ""): ?><font class="Star">&lt;&lt;</font><?php endif; ?> 

            </label></div><div class="clearall"></div>
          </div>


<div class="links"><div class="legend"><p><?php echo $this->_tpl_vars['lng']['lbl_email']; ?>
<span class="mandatory">*</span></p></div>

            <div class="field">

<input  type="text" id="email" name="email"  value="<?php echo $this->_tpl_vars['userinfo']['email']; ?>
" />
<?php if ($this->_tpl_vars['emailerror'] != "" || ( $this->_tpl_vars['reg_error'] != "" && $this->_tpl_vars['userinfo']['email'] == "" && $this->_tpl_vars['default_fields']['email']['required'] == 'Y' )):  endif; ?>

              
            </label></div><div class="clearall"></div>
          </div>


          
	  <div class="foot"></div> 
	  <br>




<?php endif; ?>

<?php if (( ( $this->_tpl_vars['active_modules']['Simple_Mode'] != "" && $this->_tpl_vars['usertype'] == 'P' ) || $this->_tpl_vars['usertype'] == 'A' ) && ( $this->_tpl_vars['userinfo']['uname'] && $this->_tpl_vars['userinfo']['uname'] != $this->_tpl_vars['login'] || ! $this->_tpl_vars['userinfo']['uname'] && $this->_tpl_vars['userinfo']['login'] != $this->_tpl_vars['login'] )): ?>

<?php if ($this->_tpl_vars['userinfo']['status'] != 'A'): ?><tr valign="middle">
<td align="right"><?php echo $this->_tpl_vars['lng']['lbl_account_status']; ?>
:</td>
<td>&nbsp;</td>
<td nowrap="nowrap">
<select name="status">
<option value="N"<?php if ($this->_tpl_vars['userinfo']['status'] == 'N'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_account_status_suspended']; ?>
</option>
<option value="Y"<?php if ($this->_tpl_vars['userinfo']['status'] == 'Y'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_account_status_enabled']; ?>
</option>
<?php if ($this->_tpl_vars['active_modules']['XAffiliate'] != "" && ( $this->_tpl_vars['userinfo']['usertype'] == 'B' || $GLOBALS['HTTP_GET_VARS']['usertype'] == 'B' )): ?>
<option value="Q"<?php if ($this->_tpl_vars['userinfo']['status'] == 'Q'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_account_status_not_approved']; ?>
</option>
<option value="D"<?php if ($this->_tpl_vars['userinfo']['status'] == 'D'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_account_status_declined']; ?>
</option>
<?php endif; ?>
</select>
</td>
</tr>

<?php if ($this->_tpl_vars['display_activity_box'] == 'Y'): ?>
<tr valign="middle">
<td align="right"><?php echo $this->_tpl_vars['lng']['lbl_account_activity']; ?>
:</td>
<td>&nbsp;</td>
<td nowrap="nowrap">
<select name="activity">
<option value="Y"<?php if ($this->_tpl_vars['userinfo']['activity'] == 'Y'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_account_activity_enabled']; ?>
</option>
<option value="N"<?php if ($this->_tpl_vars['userinfo']['activity'] == 'N'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_account_activity_disabled']; ?>
</option>
</select>
</td>
</tr>
<?php endif; ?>

<?php endif; ?>
<tr valign="middle">
	<td colspan="2">&nbsp;</td>
	<td nowrap="nowrap">

<table>
<tr>
	<td><input type="checkbox" id="change_password" name="change_password" value="Y"<?php if ($this->_tpl_vars['userinfo']['change_password'] == 'Y'): ?> checked="checked"<?php endif; ?> /></td>
	<td><label for="change_password"><?php echo $this->_tpl_vars['lng']['lbl_reg_chpass']; ?>
</label></td>
</tr>
</table>

	</td>
</tr>

<?php endif; ?>

<?php else: ?>
<tr style="display: none;">
<td>
<input type="hidden" name="uname" value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['userinfo']['login'])) ? $this->_run_mod_handler('default', true, $_tmp, @$this->_tpl_vars['userinfo']['uname']) : smarty_modifier_default($_tmp, @$this->_tpl_vars['userinfo']['uname'])); ?>
" />
<input type="hidden" name="passwd1" value="<?php echo $this->_tpl_vars['userinfo']['passwd1']; ?>
" />
<input type="hidden" name="passwd2" value="<?php echo $this->_tpl_vars['userinfo']['passwd2']; ?>
" />
</td>
</tr>
<?php endif; ?>