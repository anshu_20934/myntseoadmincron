<?php /* Smarty version 2.6.12, created on 2017-06-02 14:52:08
         compiled from main/export_specs.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'lower', 'main/export_specs.tpl', 4, false),array('modifier', 'replace', 'main/export_specs.tpl', 11, false),array('function', 'math', 'main/export_specs.tpl', 5, false),)), $this); ?>
<?php func_load_lang($this, "main/export_specs.tpl","lbl_all,lbl_change_data_range,lbl_remove_data_range"); ?><?php $_from = $this->_tpl_vars['export_spec']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
<tr<?php if ($this->_tpl_vars['level'] == 0): ?> class="TableSubHead"<?php endif; ?>>
	<td><?php if ($this->_tpl_vars['level'] == 0): ?><input type="checkbox" id="check_<?php echo ((is_array($_tmp=$this->_tpl_vars['k'])) ? $this->_run_mod_handler('lower', true, $_tmp) : smarty_modifier_lower($_tmp)); ?>
" name="check[<?php echo ((is_array($_tmp=$this->_tpl_vars['k'])) ? $this->_run_mod_handler('lower', true, $_tmp) : smarty_modifier_lower($_tmp)); ?>
]" value="Y" /><?php endif; ?></td>
	<td nowrap="nowrap"<?php if ($this->_tpl_vars['level'] > 0): ?> style="padding-left: <?php echo smarty_function_math(array('equation' => "(x-1)*25",'x' => $this->_tpl_vars['level']), $this);?>
px;"<?php endif; ?>>
		<table cellspacing="0" cellpadding="0">
		<tr>
		<?php if ($this->_tpl_vars['level'] > 0): ?>
			<td width="25"><input type="checkbox" id="check_<?php echo ((is_array($_tmp=$this->_tpl_vars['k'])) ? $this->_run_mod_handler('lower', true, $_tmp) : smarty_modifier_lower($_tmp)); ?>
" name="check[<?php echo ((is_array($_tmp=$this->_tpl_vars['k'])) ? $this->_run_mod_handler('lower', true, $_tmp) : smarty_modifier_lower($_tmp)); ?>
]" value="Y" /></td>
		<?php endif; ?>
			<td nowrap="nowrap"><label for="check_<?php echo ((is_array($_tmp=$this->_tpl_vars['k'])) ? $this->_run_mod_handler('lower', true, $_tmp) : smarty_modifier_lower($_tmp)); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['k'])) ? $this->_run_mod_handler('replace', true, $_tmp, '_', ' ') : smarty_modifier_replace($_tmp, '_', ' ')); ?>
</label></td>
		</tr>
		</table>
	</td>
	<?php if ($this->_tpl_vars['level'] > 0 || $this->_tpl_vars['v']['is_range'] == ''): ?>
	<td colspan="2">&nbsp;</td>
	<?php else: ?>
	<td width="25" align="center" nowrap="nowrap"><?php if ($this->_tpl_vars['v']['range_count'] == -1):  echo $this->_tpl_vars['lng']['lbl_all'];  else:  echo $this->_tpl_vars['v']['range_count'];  endif; ?></td>
	<td nowrap="nowrap"><a href="<?php echo $this->_tpl_vars['v']['is_range']; ?>
"><?php echo $this->_tpl_vars['lng']['lbl_change_data_range']; ?>
</a><?php if ($this->_tpl_vars['v']['range_count'] != -1): ?>&nbsp;/&nbsp;<a href="import.php?mode=export&amp;action=clear_range&amp;section=<?php echo ((is_array($_tmp=$this->_tpl_vars['k'])) ? $this->_run_mod_handler('lower', true, $_tmp) : smarty_modifier_lower($_tmp)); ?>
"><?php echo $this->_tpl_vars['lng']['lbl_remove_data_range']; ?>
</a><?php endif; ?></td>
	<?php endif; ?>
</tr>
<?php if ($this->_tpl_vars['level'] == 0): ?>
<tr>
	<td colspan="4" class="SubHeaderGreyLine"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" class="Spc" alt="" /></td>
</tr>
<?php endif; ?>
<?php if ($this->_tpl_vars['v']['subsections'] != ''): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/export_specs.tpl", 'smarty_include_vars' => array('export_spec' => $this->_tpl_vars['v']['subsections'],'level' => $this->_tpl_vars['level']+1)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>
<?php endforeach; endif; unset($_from); ?>
