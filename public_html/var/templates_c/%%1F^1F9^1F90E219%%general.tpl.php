<?php /* Smarty version 2.6.12, created on 2017-06-14 11:25:34
         compiled from admin/main/general.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'admin/main/general.tpl', 77, false),array('modifier', 'escape', 'admin/main/general.tpl', 79, false),array('modifier', 'substitute', 'admin/main/general.tpl', 85, false),array('function', 'math', 'admin/main/general.tpl', 140, false),)), $this); ?>
<?php func_load_lang($this, "admin/main/general.tpl","lbl_summary,txt_summary_admin_top_text,txt_auth_code_text,lbl_license_info,txt_license_message,lbl_general_info,txt_single_mode_enabled_text,txt_single_mode_disabled_text,txt_store_disabled_text,txt_store_enabled_text,txt_db_never_backuped,txt_db_last_backup_date,lbl_backup_database,lbl_click_here_to_backup,txt_N_products_with_empty_price,lbl_search_products,lbl_click_here_to_check,txt_processor_in_test_mode,txt_change_settings,lbl_click_here_to_check,txt_sb_processor_in_test_mode,txt_change_settings,lbl_click_here_to_check,lbl_orders_info,lbl_search_orders,lbl_click_here_for_details,lbl_status,lbl_since_last_log_in,lbl_today,lbl_this_week,lbl_this_month,lbl_processed,lbl_queued,lbl_failed,lbl_declined,lbl_not_finished,txt_shipping_disabled_text,lbl_general_settings,lbl_shipping_options,lbl_click_here_to_change,lbl_shipping_methods_info,lbl_shipping_methods,lbl_click_here_to_define,txt_N_shipping_methods_enabled,lbl_carrier,lbl_methods_enabled,lbl_user_defined,txt_no_shipping_methods_enabled,lbl_shipping_rates_info,lbl_shipping_rates,lbl_click_here_to_define,txt_only_providers_able_to_define_this,txt_N_shipping_rates_defined,lbl_carrier,lbl_rates_enabled,lbl_user_defined,txt_no_shipping_rates,txt_realtime_shipping_enabled_text,txt_realtime_shipping_disabled_text,lbl_payments_methods_info,txt_payment_methods_hiding_text,lbl_payments_methods_info,lbl_payment_method,lbl_status,lbl_disfunctional,lbl_ok,lbl_in_test_mode,lbl_environment_info,txt_environment_info_text,lbl_environment_components_info,lbl_component,lbl_status,lbl_details,lbl_directories_must_have_write_permissions,lbl_directory,lbl_status,lbl_not_exists,lbl_not_writable,lbl_ok,lbl_summary"); ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "page_title.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_summary'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog_tools.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<br />

<?php echo $this->_tpl_vars['lng']['txt_summary_admin_top_text']; ?>


<br /><br />

<?php ob_start(); ?>
<table cellpadding="2" cellspacing="0" width="100%">

<tr>
	<td width="10"></td>
	<td width="100%"></td>
</tr>

<tr>
<td colspan="2">
<?php echo $this->_tpl_vars['lng']['txt_auth_code_text']; ?>
 <b><?php echo $this->_tpl_vars['auth_code']; ?>
</b>
</td>
</tr>

<tr><td colspan="2"><br /><br /></td></tr>

<tr>
<td colspan="2"><a name="License" /><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_license_info'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>

<tr><td colspan="2" height="5"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="1" height="5" alt="" /></td></tr>


<tr>
<td colspan="2">
<?php echo $this->_tpl_vars['lng']['txt_license_message']; ?>

</td>
</tr>

<tr><td colspan="2"><br /><br /></td></tr>

<tr>
<td colspan="2"><a name="General" /><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_general_info'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>

<tr>
<td colspan="2" height="5"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="1" height="5" alt="" /></td>
</tr>


<?php if ($this->_tpl_vars['active_modules']['Simple_Mode'] == ""): ?>
<tr>
<td colspan="2">
<?php if ($this->_tpl_vars['single_mode']):  echo $this->_tpl_vars['lng']['txt_single_mode_enabled_text'];  else:  echo $this->_tpl_vars['lng']['txt_single_mode_disabled_text'];  endif; ?>
</td>
</tr>
<?php endif; ?>

<tr>
<td colspan="2">
<?php if ($this->_tpl_vars['config']['General']['shop_closed'] == 'Y'): ?>
<?php echo $this->_tpl_vars['lng']['txt_store_disabled_text']; ?>

<?php else: ?>
<?php echo $this->_tpl_vars['lng']['txt_store_enabled_text']; ?>

<?php endif; ?>
</td>
</tr>

<tr>
<td colspan="2">
<?php if ($this->_tpl_vars['config']['db_backup_date'] == ""): ?>
<font class="AdminTitle"><?php echo $this->_tpl_vars['lng']['txt_db_never_backuped']; ?>
</font>
<?php else: ?>
<?php echo $this->_tpl_vars['lng']['txt_db_last_backup_date']; ?>
 <?php echo ((is_array($_tmp=$this->_tpl_vars['config']['db_backup_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['config']['Appearance']['datetime_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['config']['Appearance']['datetime_format'])); ?>

<?php endif; ?>
&nbsp;&nbsp;&nbsp;<a href="db_backup.php" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_backup_database'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><?php echo $this->_tpl_vars['lng']['lbl_click_here_to_backup']; ?>
 &gt;&gt;</a>
</td>
</tr>

<?php if ($this->_tpl_vars['empty_prices']): ?>
<tr>
<td colspan="2"><font class="AdminTitle"><?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_N_products_with_empty_price'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'products', $this->_tpl_vars['empty_prices']) : smarty_modifier_substitute($_tmp, 'products', $this->_tpl_vars['empty_prices'])); ?>
</font>
&nbsp;&nbsp;&nbsp;<a href="search.php" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_search_products'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><?php echo $this->_tpl_vars['lng']['lbl_click_here_to_check']; ?>
 &gt;&gt;</a>
</td>
</tr>
<?php endif; ?>

<?php if ($this->_tpl_vars['active_cc']['in_testmode']): ?>
<tr>
<td colspan="2"><font class="AdminTitle"><?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_processor_in_test_mode'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'processor_name', $this->_tpl_vars['active_cc_params']['module_name']) : smarty_modifier_substitute($_tmp, 'processor_name', $this->_tpl_vars['active_cc_params']['module_name'])); ?>
</font>
&nbsp;&nbsp;&nbsp;<a href="cc_processing.php?cc_processor=<?php echo ((is_array($_tmp=$this->_tpl_vars['active_cc_params']['module_name'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
&amp;mode=update" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_change_settings'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><?php echo $this->_tpl_vars['lng']['lbl_click_here_to_check']; ?>
 &gt;&gt;</a>
</td>
</tr>
<?php endif; ?>

<?php if ($this->_tpl_vars['active_sb']['in_testmode']): ?>
<tr>
<td colspan="2"><font class="AdminTitle"><?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_sb_processor_in_test_mode'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'processor_name', $this->_tpl_vars['active_sb_params']['module_name']) : smarty_modifier_substitute($_tmp, 'processor_name', $this->_tpl_vars['active_sb_params']['module_name'])); ?>
</font>
&nbsp;&nbsp;&nbsp;<a href="cc_processing.php?cc_processor=<?php echo ((is_array($_tmp=$this->_tpl_vars['active_sb_params']['module_name'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
&amp;subscribe=yes&amp;mode=update" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_change_settings'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><?php echo $this->_tpl_vars['lng']['lbl_click_here_to_check']; ?>
 &gt;&gt;</a>
</td>
</tr>
<?php endif; ?>


<tr><td colspan="2">&nbsp;</td></tr>


<tr><td colspan="2">

<table cellpadding="0" cellspacing="0" width="100%">
<tr>
<td colspan="2" height="16">&nbsp;<font class="TopLabel"><?php echo $this->_tpl_vars['lng']['lbl_orders_info']; ?>
</font>&nbsp;&nbsp;&nbsp;<a href="orders.php" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_search_orders'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><?php echo $this->_tpl_vars['lng']['lbl_click_here_for_details']; ?>
 &gt;&gt;</a></td>
</tr>

<tr><td colspan="2" height="3"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="1" height="3" alt="" /></td></tr>

<tr>
<td><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="30" height="1" alt="" /><br /></td>
<td width="100%">
<table cellpadding="3" cellspacing="1" width="90%">
<tr>
<td class="TableHead"><?php echo $this->_tpl_vars['lng']['lbl_status']; ?>
</td>
<td class="TableHead" nowrap="nowrap" align="center"><?php echo $this->_tpl_vars['lng']['lbl_since_last_log_in']; ?>
</td>
<td class="TableHead" align="center"><?php echo $this->_tpl_vars['lng']['lbl_today']; ?>
</td>
<td class="TableHead" nowrap="nowrap" align="center"><?php echo $this->_tpl_vars['lng']['lbl_this_week']; ?>
</td>
<td class="TableHead" nowrap="nowrap" align="center"><?php echo $this->_tpl_vars['lng']['lbl_this_month']; ?>
</td>
</tr>
<?php $this->assign('index', '0'); ?>
<?php $_from = $this->_tpl_vars['orders']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
<tr <?php if (( $this->_tpl_vars['index'] % 2 ) == 0): ?> class="TableLine"<?php endif; ?>>
<td nowrap="nowrap"><?php if ($this->_tpl_vars['key'] == 'P'):  echo $this->_tpl_vars['lng']['lbl_processed'];  elseif ($this->_tpl_vars['key'] == 'Q'):  echo $this->_tpl_vars['lng']['lbl_queued'];  elseif ($this->_tpl_vars['key'] == 'F' || $this->_tpl_vars['key'] == 'D'):  echo $this->_tpl_vars['lng']['lbl_failed']; ?>
/<?php echo $this->_tpl_vars['lng']['lbl_declined'];  elseif ($this->_tpl_vars['key'] == 'I'):  echo $this->_tpl_vars['lng']['lbl_not_finished'];  endif; ?>:</td>
<?php unset($this->_sections['period']);
$this->_sections['period']['name'] = 'period';
$this->_sections['period']['loop'] = is_array($_loop=$this->_tpl_vars['item']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['period']['show'] = true;
$this->_sections['period']['max'] = $this->_sections['period']['loop'];
$this->_sections['period']['step'] = 1;
$this->_sections['period']['start'] = $this->_sections['period']['step'] > 0 ? 0 : $this->_sections['period']['loop']-1;
if ($this->_sections['period']['show']) {
    $this->_sections['period']['total'] = $this->_sections['period']['loop'];
    if ($this->_sections['period']['total'] == 0)
        $this->_sections['period']['show'] = false;
} else
    $this->_sections['period']['total'] = 0;
if ($this->_sections['period']['show']):

            for ($this->_sections['period']['index'] = $this->_sections['period']['start'], $this->_sections['period']['iteration'] = 1;
                 $this->_sections['period']['iteration'] <= $this->_sections['period']['total'];
                 $this->_sections['period']['index'] += $this->_sections['period']['step'], $this->_sections['period']['iteration']++):
$this->_sections['period']['rownum'] = $this->_sections['period']['iteration'];
$this->_sections['period']['index_prev'] = $this->_sections['period']['index'] - $this->_sections['period']['step'];
$this->_sections['period']['index_next'] = $this->_sections['period']['index'] + $this->_sections['period']['step'];
$this->_sections['period']['first']      = ($this->_sections['period']['iteration'] == 1);
$this->_sections['period']['last']       = ($this->_sections['period']['iteration'] == $this->_sections['period']['total']);
?>
<td align="center"><?php echo $this->_tpl_vars['item'][$this->_sections['period']['index']]; ?>
</td>
<?php endfor; endif; ?>
</tr>
<?php echo smarty_function_math(array('equation' => "x+1",'x' => $this->_tpl_vars['index'],'assign' => 'index'), $this);?>

<?php endforeach; endif; unset($_from); ?>
</table>

</td>
</tr>
</table>

</td></tr>

<tr><td colspan="2">&nbsp;</td></tr>


<?php if ($this->_tpl_vars['config']['Shipping']['disable_shipping'] == 'Y'): ?>

<tr>
<td colspan="2">&nbsp;<font class="AdminTitle"><?php echo $this->_tpl_vars['lng']['txt_shipping_disabled_text']; ?>
</font>&nbsp;&nbsp;&nbsp;<a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/configuration.php?option=Shipping" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_general_settings'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
/<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_shipping_options'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><?php echo $this->_tpl_vars['lng']['lbl_click_here_to_change']; ?>
 &gt;&gt;</a></td>
</tr>

<?php else: ?>

<tr><td colspan="2">

<table cellpadding="0" cellspacing="0" width="100%">
<tr>
<td colspan="2" height="16">&nbsp;<font class="TopLabel"><?php echo $this->_tpl_vars['lng']['lbl_shipping_methods_info']; ?>
</font>&nbsp;&nbsp;&nbsp;<a href="shipping.php" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_shipping_methods'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><?php echo $this->_tpl_vars['lng']['lbl_click_here_to_define']; ?>
 >></a></td>
</tr>

<tr><td colspan="2" height="3"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="1" height="3" alt="" /></td></tr>

<tr>
<td><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="30" height="1" alt="" /><br /></td>
<td width="100%">
<?php if ($this->_tpl_vars['shipping_methods_count'] > '0'): ?>
<font class="Text"><?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_N_shipping_methods_enabled'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'count', $this->_tpl_vars['shipping_methods_count']) : smarty_modifier_substitute($_tmp, 'count', $this->_tpl_vars['shipping_methods_count'])); ?>
:</font>
<br />
<table cellpadding="1" cellspacing="2" width="80%">

<tr>
<td height="14" class="TableHead"><?php echo $this->_tpl_vars['lng']['lbl_carrier']; ?>
</td>
<td height="14" class="TableHead" nowrap="nowrap" align="center"><?php echo $this->_tpl_vars['lng']['lbl_methods_enabled']; ?>
</td>
</tr>

<?php unset($this->_sections['idx']);
$this->_sections['idx']['name'] = 'idx';
$this->_sections['idx']['loop'] = is_array($_loop=$this->_tpl_vars['shipping_mod_enabled']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['idx']['show'] = true;
$this->_sections['idx']['max'] = $this->_sections['idx']['loop'];
$this->_sections['idx']['step'] = 1;
$this->_sections['idx']['start'] = $this->_sections['idx']['step'] > 0 ? 0 : $this->_sections['idx']['loop']-1;
if ($this->_sections['idx']['show']) {
    $this->_sections['idx']['total'] = $this->_sections['idx']['loop'];
    if ($this->_sections['idx']['total'] == 0)
        $this->_sections['idx']['show'] = false;
} else
    $this->_sections['idx']['total'] = 0;
if ($this->_sections['idx']['show']):

            for ($this->_sections['idx']['index'] = $this->_sections['idx']['start'], $this->_sections['idx']['iteration'] = 1;
                 $this->_sections['idx']['iteration'] <= $this->_sections['idx']['total'];
                 $this->_sections['idx']['index'] += $this->_sections['idx']['step'], $this->_sections['idx']['iteration']++):
$this->_sections['idx']['rownum'] = $this->_sections['idx']['iteration'];
$this->_sections['idx']['index_prev'] = $this->_sections['idx']['index'] - $this->_sections['idx']['step'];
$this->_sections['idx']['index_next'] = $this->_sections['idx']['index'] + $this->_sections['idx']['step'];
$this->_sections['idx']['first']      = ($this->_sections['idx']['iteration'] == 1);
$this->_sections['idx']['last']       = ($this->_sections['idx']['iteration'] == $this->_sections['idx']['total']);
?>
<tr <?php if ($this->_sections['idx']['index'] % 2 == 0): ?> class="TableLine"<?php endif; ?>>
<td><?php if ($this->_tpl_vars['shipping_mod_enabled'][$this->_sections['idx']['index']]['code'] == 'FDX'): ?>FedEx<?php elseif ($this->_tpl_vars['shipping_mod_enabled'][$this->_sections['idx']['index']]['code'] == 'UPS'): ?>UPS<?php elseif ($this->_tpl_vars['shipping_mod_enabled'][$this->_sections['idx']['index']]['code'] == 'USPS'): ?>U.S.P.S.<?php elseif ($this->_tpl_vars['shipping_mod_enabled'][$this->_sections['idx']['index']]['code'] == 'DHL'): ?>DHL<?php elseif ($this->_tpl_vars['shipping_mod_enabled'][$this->_sections['idx']['index']]['code'] == 'ABX'): ?>Airborne<?php elseif ($this->_tpl_vars['shipping_mod_enabled'][$this->_sections['idx']['index']]['code'] == 'EWW'): ?>Emery Worldwide<?php elseif ($this->_tpl_vars['shipping_mod_enabled'][$this->_sections['idx']['index']]['code'] == 'ANX'): ?>AirNet Express<?php elseif ($this->_tpl_vars['shipping_mod_enabled'][$this->_sections['idx']['index']]['code']):  echo $this->_tpl_vars['shipping_mod_enabled'][$this->_sections['idx']['index']]['code'];  else:  echo $this->_tpl_vars['lng']['lbl_user_defined'];  endif; ?></td>
<td align="center"><?php echo $this->_tpl_vars['shipping_mod_enabled'][$this->_sections['idx']['index']]['count']; ?>
</td>
</tr>
<?php endfor; endif; ?>

</table>
<?php else: ?>
<font class="AdminTitle"><?php echo $this->_tpl_vars['lng']['txt_no_shipping_methods_enabled']; ?>
</font>
<?php endif; ?>
</td>
</tr>
</table>

</td></tr>

<tr><td colspan="2">&nbsp;</td></tr>


<tr><td colspan="2">

<table cellpadding="0" cellspacing="0" width="100%">
<tr>
<td colspan="2" height="16">&nbsp;<font class="TopLabel"><?php echo $this->_tpl_vars['lng']['lbl_shipping_rates_info']; ?>
</font>&nbsp;&nbsp;&nbsp;<?php if ($this->_tpl_vars['active_modules']['Simple_Mode']): ?><a href="<?php echo $this->_tpl_vars['catalogs']['provider']; ?>
/shipping_rates.php" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_shipping_rates'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><?php echo $this->_tpl_vars['lng']['lbl_click_here_to_define']; ?>
 &gt;&gt;</a><?php else: ?>(<?php echo $this->_tpl_vars['lng']['txt_only_providers_able_to_define_this']; ?>
)<?php endif; ?></td>
</tr>

<tr><td colspan="2" height="3"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="1" height="3" alt="" /></td></tr>

<tr>
<td><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="30" height="1" alt="" /><br /></td>
<td width="100%"><font class="Text">
<?php if ($this->_tpl_vars['shipping_rates_count'] > '0'): ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_N_shipping_rates_defined'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'count', $this->_tpl_vars['shipping_rates_count']) : smarty_modifier_substitute($_tmp, 'count', $this->_tpl_vars['shipping_rates_count'])); ?>
:
</font>
<br />
<table cellpadding="1" cellspacing="2" width="80%">
<tr>
<td height="14" class="TableHead"><?php echo $this->_tpl_vars['lng']['lbl_carrier']; ?>
</td>
<td height="14" class="TableHead" nowrap="nowrap" align="center"><?php echo $this->_tpl_vars['lng']['lbl_rates_enabled']; ?>
</td>
</tr>

<?php unset($this->_sections['idx']);
$this->_sections['idx']['name'] = 'idx';
$this->_sections['idx']['loop'] = is_array($_loop=$this->_tpl_vars['shipping_rates_enabled']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['idx']['show'] = true;
$this->_sections['idx']['max'] = $this->_sections['idx']['loop'];
$this->_sections['idx']['step'] = 1;
$this->_sections['idx']['start'] = $this->_sections['idx']['step'] > 0 ? 0 : $this->_sections['idx']['loop']-1;
if ($this->_sections['idx']['show']) {
    $this->_sections['idx']['total'] = $this->_sections['idx']['loop'];
    if ($this->_sections['idx']['total'] == 0)
        $this->_sections['idx']['show'] = false;
} else
    $this->_sections['idx']['total'] = 0;
if ($this->_sections['idx']['show']):

            for ($this->_sections['idx']['index'] = $this->_sections['idx']['start'], $this->_sections['idx']['iteration'] = 1;
                 $this->_sections['idx']['iteration'] <= $this->_sections['idx']['total'];
                 $this->_sections['idx']['index'] += $this->_sections['idx']['step'], $this->_sections['idx']['iteration']++):
$this->_sections['idx']['rownum'] = $this->_sections['idx']['iteration'];
$this->_sections['idx']['index_prev'] = $this->_sections['idx']['index'] - $this->_sections['idx']['step'];
$this->_sections['idx']['index_next'] = $this->_sections['idx']['index'] + $this->_sections['idx']['step'];
$this->_sections['idx']['first']      = ($this->_sections['idx']['iteration'] == 1);
$this->_sections['idx']['last']       = ($this->_sections['idx']['iteration'] == $this->_sections['idx']['total']);
?>
<tr>
<td><?php if ($this->_tpl_vars['shipping_rates_enabled'][$this->_sections['idx']['index']]['code'] == 'FDX'): ?>FedEx<?php elseif ($this->_tpl_vars['shipping_rates_enabled'][$this->_sections['idx']['index']]['code'] == 'UPS'): ?>UPS<?php elseif ($this->_tpl_vars['shipping_rates_enabled'][$this->_sections['idx']['index']]['code'] == 'USPS'): ?>U.S.P.S.<?php elseif ($this->_tpl_vars['shipping_rates_enabled'][$this->_sections['idx']['index']]['code'] == 'DHL'): ?>DHL<?php elseif ($this->_tpl_vars['shipping_rates_enabled'][$this->_sections['idx']['index']]['code'] == 'ABX'): ?>Airborne<?php elseif ($this->_tpl_vars['shipping_rates_enabled'][$this->_sections['idx']['index']]['code'] == 'EWW'): ?>Emery Worldwide<?php elseif ($this->_tpl_vars['shipping_rates_enabled'][$this->_sections['idx']['index']]['code'] == 'ANX'): ?>AirNet Express<?php elseif ($this->_tpl_vars['shipping_rates_enabled'][$this->_sections['idx']['index']]['code']):  echo $this->_tpl_vars['shipping_rates_enabled'][$this->_sections['idx']['index']]['code'];  else:  echo $this->_tpl_vars['lng']['lbl_user_defined'];  endif; ?></td>
<td align="center"><?php echo $this->_tpl_vars['shipping_rates_enabled'][$this->_sections['idx']['index']]['count']; ?>
</td>
</tr>
<?php endfor; endif; ?>

</table>
<?php else: ?>
<font class="AdminTitle"><?php echo $this->_tpl_vars['lng']['txt_no_shipping_rates']; ?>
</font>
</font>
<?php endif; ?>
</td>
</tr>
</table>

</td></tr>

<tr>
<td><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="30" height="1" alt="" /><br /></td>
<td width="100%"><font class="Text">
<?php if ($this->_tpl_vars['config']['Shipping']['realtime_shipping'] == 'Y'): ?>
<?php echo $this->_tpl_vars['lng']['txt_realtime_shipping_enabled_text']; ?>

<?php else: ?>
<?php echo $this->_tpl_vars['lng']['txt_realtime_shipping_disabled_text']; ?>

<?php endif; ?>
</font>
</td>
</tr>

<?php endif; ?>

<tr><td colspan="2"><br /><br /></td></tr>


<tr>
<td colspan="2"><a name="PaymentMethods" /><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_payments_methods_info'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>

<tr><td colspan="2" height="5"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="1" height="5" alt="" /></td></tr>

<tr><td colspan="2">
<?php echo $this->_tpl_vars['lng']['txt_payment_methods_hiding_text']; ?>

</td></tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td colspan="2">

<table cellpadding="0" cellspacing="0" width="100%">
<tr><td colspan="2" height="16">&nbsp;<font class="TopLabel"><?php echo $this->_tpl_vars['lng']['lbl_payments_methods_info']; ?>
:</font></td></tr>

<tr><td colspan="2" height="5"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="1" height="5" alt="" /></td></tr>
<tr>
<td><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="30" height="1" alt="" /><br /></td>
<td width="100%" valign="top">
<table cellpadding="1" cellspacing="2" width="80%">
<tr>
<td height="14" class="TableHead" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_payment_method']; ?>
</td>
<td height="14" class="TableHead" nowrap="nowrap" width="20%"><?php echo $this->_tpl_vars['lng']['lbl_status']; ?>
</td>
</tr>
<?php unset($this->_sections['idx']);
$this->_sections['idx']['name'] = 'idx';
$this->_sections['idx']['loop'] = is_array($_loop=$this->_tpl_vars['payment_methods']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['idx']['show'] = true;
$this->_sections['idx']['max'] = $this->_sections['idx']['loop'];
$this->_sections['idx']['step'] = 1;
$this->_sections['idx']['start'] = $this->_sections['idx']['step'] > 0 ? 0 : $this->_sections['idx']['loop']-1;
if ($this->_sections['idx']['show']) {
    $this->_sections['idx']['total'] = $this->_sections['idx']['loop'];
    if ($this->_sections['idx']['total'] == 0)
        $this->_sections['idx']['show'] = false;
} else
    $this->_sections['idx']['total'] = 0;
if ($this->_sections['idx']['show']):

            for ($this->_sections['idx']['index'] = $this->_sections['idx']['start'], $this->_sections['idx']['iteration'] = 1;
                 $this->_sections['idx']['iteration'] <= $this->_sections['idx']['total'];
                 $this->_sections['idx']['index'] += $this->_sections['idx']['step'], $this->_sections['idx']['iteration']++):
$this->_sections['idx']['rownum'] = $this->_sections['idx']['iteration'];
$this->_sections['idx']['index_prev'] = $this->_sections['idx']['index'] - $this->_sections['idx']['step'];
$this->_sections['idx']['index_next'] = $this->_sections['idx']['index'] + $this->_sections['idx']['step'];
$this->_sections['idx']['first']      = ($this->_sections['idx']['iteration'] == 1);
$this->_sections['idx']['last']       = ($this->_sections['idx']['iteration'] == $this->_sections['idx']['total']);
?>
<tr <?php if ($this->_sections['idx']['index'] % 2 == 0): ?> class="TableLine"<?php endif; ?>>
<td><?php echo $this->_tpl_vars['payment_methods'][$this->_sections['idx']['index']]['payment_method']; ?>
</td>
<td nowrap="nowrap"><?php if ($this->_tpl_vars['payment_methods'][$this->_sections['idx']['index']]['is_down']): ?><font class="AdminTitle"><?php echo $this->_tpl_vars['lng']['lbl_disfunctional']; ?>
</font><?php else:  echo $this->_tpl_vars['lng']['lbl_ok'];  endif;  if ($this->_tpl_vars['payment_methods'][$this->_sections['idx']['index']]['in_testmode']): ?> / <font class="AdminTitle"><?php echo $this->_tpl_vars['lng']['lbl_in_test_mode']; ?>
</font><?php endif; ?></td>
</tr>
<?php endfor; endif; ?>
</table>
</td></tr>
</table>
</td></tr>

<tr><td colspan="2"><br /><br /></td></tr>


<tr>
<td colspan="2"><a name="Environment" /><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_environment_info'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>

<tr><td colspan="2" height="5"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="1" height="5" alt="" /></td></tr>

<tr><td colspan="2">
<?php echo $this->_tpl_vars['lng']['txt_environment_info_text']; ?>

</td></tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td colspan="2">

<table cellpadding="0" cellspacing="0" width="100%">
<tr><td colspan="2" height="16">&nbsp;<font class="TopLabel"><?php echo $this->_tpl_vars['lng']['lbl_environment_components_info']; ?>
:</font></td></tr>

<tr><td colspan="2" height="5"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="1" height="5" alt="" /></td></tr>
<tr>
<td><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="30" height="1" alt="" /><br /></td>
<td width="100%" valign="top">
<table cellpadding="1" cellspacing="2" width="80%">
<tr>
<td height="14" class="TableHead" nowrap="nowrap" width="100"><?php echo $this->_tpl_vars['lng']['lbl_component']; ?>
</td>
<td height="14" class="TableHead" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_status']; ?>
</td>
<td height="14" class="TableHead" nowrap="nowrap" width="70">&nbsp;</td>
</tr>
<?php unset($this->_sections['idx']);
$this->_sections['idx']['name'] = 'idx';
$this->_sections['idx']['loop'] = is_array($_loop=$this->_tpl_vars['environment_info']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['idx']['show'] = true;
$this->_sections['idx']['max'] = $this->_sections['idx']['loop'];
$this->_sections['idx']['step'] = 1;
$this->_sections['idx']['start'] = $this->_sections['idx']['step'] > 0 ? 0 : $this->_sections['idx']['loop']-1;
if ($this->_sections['idx']['show']) {
    $this->_sections['idx']['total'] = $this->_sections['idx']['loop'];
    if ($this->_sections['idx']['total'] == 0)
        $this->_sections['idx']['show'] = false;
} else
    $this->_sections['idx']['total'] = 0;
if ($this->_sections['idx']['show']):

            for ($this->_sections['idx']['index'] = $this->_sections['idx']['start'], $this->_sections['idx']['iteration'] = 1;
                 $this->_sections['idx']['iteration'] <= $this->_sections['idx']['total'];
                 $this->_sections['idx']['index'] += $this->_sections['idx']['step'], $this->_sections['idx']['iteration']++):
$this->_sections['idx']['rownum'] = $this->_sections['idx']['iteration'];
$this->_sections['idx']['index_prev'] = $this->_sections['idx']['index'] - $this->_sections['idx']['step'];
$this->_sections['idx']['index_next'] = $this->_sections['idx']['index'] + $this->_sections['idx']['step'];
$this->_sections['idx']['first']      = ($this->_sections['idx']['iteration'] == 1);
$this->_sections['idx']['last']       = ($this->_sections['idx']['iteration'] == $this->_sections['idx']['total']);
?>
<?php if ($this->_tpl_vars['environment_info'][$this->_sections['idx']['index']]['row_txt'] != ""): ?>
<tr class="TableHead"><td colspan="3" height="14"><i><?php echo $this->_tpl_vars['environment_info'][$this->_sections['idx']['index']]['row_txt']; ?>
</i></td></tr>
<?php else: ?>
<tr <?php if ($this->_sections['idx']['index'] % 2 == 0): ?> class="TableLine"<?php endif; ?>>
<td><?php echo $this->_tpl_vars['environment_info'][$this->_sections['idx']['index']]['item']; ?>
</td>
<td>
<?php if ($this->_tpl_vars['environment_info'][$this->_sections['idx']['index']]['data'] != ""): ?>
<?php echo $this->_tpl_vars['environment_info'][$this->_sections['idx']['index']]['data']; ?>

<?php else: ?>
<?php if ($this->_tpl_vars['environment_info'][$this->_sections['idx']['index']]['warning'] != ""): ?>
<font class="AdminTitle"><?php echo $this->_tpl_vars['environment_info'][$this->_sections['idx']['index']]['default']; ?>
</font>
<?php else:  echo $this->_tpl_vars['environment_info'][$this->_sections['idx']['index']]['default'];  endif; ?>
<?php endif; ?>
</td>
<?php if ($this->_tpl_vars['environment_info'][$this->_sections['idx']['index']]['details'] != ""): ?>
<td><a href="javascript:void(0);" onclick="<?php echo $this->_tpl_vars['environment_info'][$this->_sections['idx']['index']]['details']; ?>
"><?php if ($this->_tpl_vars['environment_info'][$this->_sections['idx']['index']]['details_txt'] != ""):  echo $this->_tpl_vars['environment_info'][$this->_sections['idx']['index']]['details_txt'];  else:  echo $this->_tpl_vars['lng']['lbl_details'];  endif; ?> &gt;&gt;</a></td>
<?php elseif ($this->_tpl_vars['environment_info'][$this->_sections['idx']['index']]['details_txt'] != ""): ?>
<td><?php echo $this->_tpl_vars['environment_info'][$this->_sections['idx']['index']]['details_txt']; ?>
</td>
<?php else: ?>
<td>&nbsp;</td>
<?php endif; ?>
</tr>
<?php endif; ?>
<?php endfor; endif; ?>
</table>
</td></tr>
</table></td></tr>

<tr><td colspan="2">&nbsp;</td></tr>
<tr><td colspan="2">

<table cellpadding="0" cellspacing="0" width="100%">
<tr>
<td colspan="2" height="16">&nbsp;<font class="TopLabel"><?php echo $this->_tpl_vars['lng']['lbl_directories_must_have_write_permissions']; ?>
:</font></td>
</tr>
<tr><td colspan="2" height="5"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="1" height="5" alt="" /></td></tr>
<tr>
<td><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="30" height="1" alt="" /><br /></td>
<td width="100%">
<table cellpadding="1" cellspacing="2" width="80%">
<tr>
<td height="14" class="TableHead" nowrap="nowrap" width="100"><?php echo $this->_tpl_vars['lng']['lbl_directory']; ?>
</td>
<td height="14" class="TableHead" nowrap="nowrap"><?php echo $this->_tpl_vars['lng']['lbl_status']; ?>
</td>
</tr>
<?php unset($this->_sections['dir']);
$this->_sections['dir']['name'] = 'dir';
$this->_sections['dir']['loop'] = is_array($_loop=$this->_tpl_vars['test_dirs_rights']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['dir']['show'] = true;
$this->_sections['dir']['max'] = $this->_sections['dir']['loop'];
$this->_sections['dir']['step'] = 1;
$this->_sections['dir']['start'] = $this->_sections['dir']['step'] > 0 ? 0 : $this->_sections['dir']['loop']-1;
if ($this->_sections['dir']['show']) {
    $this->_sections['dir']['total'] = $this->_sections['dir']['loop'];
    if ($this->_sections['dir']['total'] == 0)
        $this->_sections['dir']['show'] = false;
} else
    $this->_sections['dir']['total'] = 0;
if ($this->_sections['dir']['show']):

            for ($this->_sections['dir']['index'] = $this->_sections['dir']['start'], $this->_sections['dir']['iteration'] = 1;
                 $this->_sections['dir']['iteration'] <= $this->_sections['dir']['total'];
                 $this->_sections['dir']['index'] += $this->_sections['dir']['step'], $this->_sections['dir']['iteration']++):
$this->_sections['dir']['rownum'] = $this->_sections['dir']['iteration'];
$this->_sections['dir']['index_prev'] = $this->_sections['dir']['index'] - $this->_sections['dir']['step'];
$this->_sections['dir']['index_next'] = $this->_sections['dir']['index'] + $this->_sections['dir']['step'];
$this->_sections['dir']['first']      = ($this->_sections['dir']['iteration'] == 1);
$this->_sections['dir']['last']       = ($this->_sections['dir']['iteration'] == $this->_sections['dir']['total']);
?>
<tr <?php if ($this->_sections['dir']['index'] % 2 == 0): ?> class="TableLine"<?php endif; ?>>
<td><?php echo $this->_tpl_vars['test_dirs_rights'][$this->_sections['dir']['index']]['directory']; ?>
</td>
<td>
<?php if ($this->_tpl_vars['test_dirs_rights'][$this->_sections['dir']['index']]['exists'] != '1'): ?><font class="AdminTitle"><?php echo $this->_tpl_vars['lng']['lbl_not_exists']; ?>
</font>
<?php elseif ($this->_tpl_vars['test_dirs_rights'][$this->_sections['dir']['index']]['writable'] != '1'): ?><font class="AdminTitle"><?php echo $this->_tpl_vars['lng']['lbl_not_writable']; ?>
</font>
<?php else:  echo $this->_tpl_vars['lng']['lbl_ok'];  endif; ?>
</td></tr>
<?php endfor; endif; ?>
</table>
</td></tr></table>
</td>
</tr>

</table>

<br />

<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_summary'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>