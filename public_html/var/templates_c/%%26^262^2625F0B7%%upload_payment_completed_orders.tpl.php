<?php /* Smarty version 2.6.12, created on 2018-01-16 12:30:00
         compiled from admin/main/upload_payment_completed_orders.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'string_format', 'admin/main/upload_payment_completed_orders.tpl', 48, false),array('modifier', 'cat', 'admin/main/upload_payment_completed_orders.tpl', 63, false),array('modifier', 'count', 'admin/main/upload_payment_completed_orders.tpl', 108, false),array('function', 'cycle', 'admin/main/upload_payment_completed_orders.tpl', 110, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "main/popup_product.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php echo '
    <script type="text/javascript">
	    $(function() {
	       $("#uploadxls").click( function() {
                var overlay = new ProcessingOverlay();
                overlay.show();
	       });
	       $("#confirm_queue").click( function() {
                var overlay = new ProcessingOverlay();
                overlay.show();
           });
	    });
    </script>
'; ?>


<?php ob_start();  if ($this->_tpl_vars['action'] == 'upload'): ?>
    <?php if ($this->_tpl_vars['errorMsg']): ?>
        <div style="color: red; font-weight: bold;"><?php echo $this->_tpl_vars['errorMsg']; ?>
</div>
    <?php endif; ?>
	<form action="" method="post" name="queue_orders" enctype="multipart/form-data" >
		Upload the csv file : <input type="file" name="orderdetail" ><?php if ($this->_tpl_vars['message']): ?><font color='red'><?php echo $this->_tpl_vars['message']; ?>
</font><?php endif; ?>
		<input type='submit'  name='uploadxls' id='uploadxls' value="Upload">
		<input type='hidden' name='action' value='verify'>
	</form>
	
	<?php if ($this->_tpl_vars['summary']): ?>
	   <div id="summary" style="margin-top: 20px;"> 
	       <?php echo $this->_tpl_vars['summary']; ?>

	   </div>
	<?php endif; ?>

	<?php if ($this->_tpl_vars['googleEcommerceTracking']): ?>	
		<script type="text/javascript">
    		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    	</script>

		<script type="text/javascript">
			var pageTracker = _gat._getTracker("<?php echo $this->_tpl_vars['ga_acc']; ?>
");
            pageTracker._trackPageview();
			<?php unset($this->_sections['idx']);
$this->_sections['idx']['name'] = 'idx';
$this->_sections['idx']['loop'] = is_array($_loop=$this->_tpl_vars['ecomtrackingdata']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['idx']['show'] = true;
$this->_sections['idx']['max'] = $this->_sections['idx']['loop'];
$this->_sections['idx']['step'] = 1;
$this->_sections['idx']['start'] = $this->_sections['idx']['step'] > 0 ? 0 : $this->_sections['idx']['loop']-1;
if ($this->_sections['idx']['show']) {
    $this->_sections['idx']['total'] = $this->_sections['idx']['loop'];
    if ($this->_sections['idx']['total'] == 0)
        $this->_sections['idx']['show'] = false;
} else
    $this->_sections['idx']['total'] = 0;
if ($this->_sections['idx']['show']):

            for ($this->_sections['idx']['index'] = $this->_sections['idx']['start'], $this->_sections['idx']['iteration'] = 1;
                 $this->_sections['idx']['iteration'] <= $this->_sections['idx']['total'];
                 $this->_sections['idx']['index'] += $this->_sections['idx']['step'], $this->_sections['idx']['iteration']++):
$this->_sections['idx']['rownum'] = $this->_sections['idx']['iteration'];
$this->_sections['idx']['index_prev'] = $this->_sections['idx']['index'] - $this->_sections['idx']['step'];
$this->_sections['idx']['index_next'] = $this->_sections['idx']['index'] + $this->_sections['idx']['step'];
$this->_sections['idx']['first']      = ($this->_sections['idx']['iteration'] == 1);
$this->_sections['idx']['last']       = ($this->_sections['idx']['iteration'] == $this->_sections['idx']['total']);
?>
                pageTracker._addTrans(
            		"<?php echo $this->_tpl_vars['ecomtrackingdata'][$this->_sections['idx']['index']]['orderid']; ?>
",
            		"<?php echo $this->_tpl_vars['ecomtrackingdata'][$this->_sections['idx']['index']]['userinfo']['b_firstname']; ?>
 <?php echo $this->_tpl_vars['ecomtrackingdata'][$this->_sections['idx']['index']]['userinfo']['b_lastname']; ?>
", 
            		"<?php echo ((is_array($_tmp=$this->_tpl_vars['ecomtrackingdata'][$this->_sections['idx']['index']]['amountafterdiscount'])) ? $this->_run_mod_handler('string_format', true, $_tmp, '%.2f') : smarty_modifier_string_format($_tmp, '%.2f')); ?>
",  
        			"<?php echo $this->_tpl_vars['ecomtrackingdata'][$this->_sections['idx']['index']]['vat']; ?>
",        
        			"<?php echo $this->_tpl_vars['ecomtrackingdata'][$this->_sections['idx']['index']]['shippingRate']; ?>
",    
        			"<?php echo $this->_tpl_vars['ecomtrackingdata'][$this->_sections['idx']['index']]['userinfo']['s_city']; ?>
",           
        			"<?php echo $this->_tpl_vars['ecomtrackingdata'][$this->_sections['idx']['index']]['userinfo']['s_state']; ?>
",      
        			"<?php echo $this->_tpl_vars['ecomtrackingdata'][$this->_sections['idx']['index']]['userinfo']['s_country']; ?>
" 
        		);
                <?php $this->assign('netTotalPrice', 0); ?>
            	<?php $this->assign('productidDelimString', ''); ?>
            	<?php $this->assign('productTypeDelimString', ''); ?>
                
            	<?php $_from = $this->_tpl_vars['ecomtrackingdata'][$this->_sections['idx']['index']]['productsInCart']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['outer'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['outer']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['product']):
        $this->_foreach['outer']['iteration']++;
?>
                    <?php $_from = $this->_tpl_vars['product']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                    	<?php if ($this->_tpl_vars['key'] == 'productId'): ?>
                    		<?php $this->assign('productId', $this->_tpl_vars['item']); ?>
                    		<?php $this->assign('productidDelimString', ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['productidDelimString'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['productId']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['productId'])))) ? $this->_run_mod_handler('cat', true, $_tmp, "|") : smarty_modifier_cat($_tmp, "|"))); ?>
                    	<?php endif; ?>

                    	<?php if ($this->_tpl_vars['key'] == 'productStyleName'): ?>
                    		<?php $this->assign('productStyleName', $this->_tpl_vars['item']); ?>
                    	<?php endif; ?>

                    	<?php if ($this->_tpl_vars['key'] == 'quantity'): ?>
                    		<?php $this->assign('quantity', $this->_tpl_vars['item']); ?>
                    	<?php endif; ?>

                    	<?php if ($this->_tpl_vars['key'] == 'productTypeLabel'): ?>
                    		<?php $this->assign('productTypeLabel', $this->_tpl_vars['item']); ?>
                    		<?php $this->assign('productTypeDelimString', ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['productTypeDelimString'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['productTypeLabel']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['productTypeLabel'])))) ? $this->_run_mod_handler('cat', true, $_tmp, "|") : smarty_modifier_cat($_tmp, "|"))); ?>
                    	<?php endif; ?>

                    	<?php if ($this->_tpl_vars['key'] == 'totalPrice'): ?>
                    		<?php $this->assign('totalPrice', $this->_tpl_vars['item']); ?>
                    		<?php $this->assign('netTotalPrice', $this->_tpl_vars['netTotalPrice']+$this->_tpl_vars['totalPrice']); ?>
                    	<?php endif; ?>
                    <?php endforeach; endif; unset($_from); ?>	
                    pageTracker._addItem(
            			"<?php echo $this->_tpl_vars['ecomtrackingdata'][$this->_sections['idx']['index']]['orderid']; ?>
",   
            			"<?php echo $this->_tpl_vars['productId']; ?>
",    
            			"<?php echo $this->_tpl_vars['productStyleName']; ?>
",
            			"<?php echo $this->_tpl_vars['productTypeLabel']; ?>
",
            			"<?php echo $this->_tpl_vars['totalPrice']; ?>
",   
            			"<?php echo $this->_tpl_vars['quantity']; ?>
"   
            		);
                <?php endforeach; endif; unset($_from); ?>
                pageTracker._trackTrans();
			<?php endfor; endif; ?>
		</script>
	<?php endif;  elseif ($this->_tpl_vars['action'] == verify): ?>
	
	<?php if ($this->_tpl_vars['dataFormatError']): ?>
		<b><?php echo $this->_tpl_vars['dataFormatError']; ?>
</b>
	<?php else: ?>	
		<form action="" method="post" name="queue_orders" enctype="multipart/form-data" >
			<table cellpadding="2" cellspacing="1" width="100%">
	            <input type='hidden' name='action' value='confirm'>
	            <tr class="TableHead">
	                <td>Orderids to queue</td>
				</tr>
				<?php if (count($this->_tpl_vars['orderListDisplay']) > 0): ?>
		            <?php unset($this->_sections['idx']);
$this->_sections['idx']['name'] = 'idx';
$this->_sections['idx']['loop'] = is_array($_loop=$this->_tpl_vars['orderListDisplay']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['idx']['show'] = true;
$this->_sections['idx']['max'] = $this->_sections['idx']['loop'];
$this->_sections['idx']['step'] = 1;
$this->_sections['idx']['start'] = $this->_sections['idx']['step'] > 0 ? 0 : $this->_sections['idx']['loop']-1;
if ($this->_sections['idx']['show']) {
    $this->_sections['idx']['total'] = $this->_sections['idx']['loop'];
    if ($this->_sections['idx']['total'] == 0)
        $this->_sections['idx']['show'] = false;
} else
    $this->_sections['idx']['total'] = 0;
if ($this->_sections['idx']['show']):

            for ($this->_sections['idx']['index'] = $this->_sections['idx']['start'], $this->_sections['idx']['iteration'] = 1;
                 $this->_sections['idx']['iteration'] <= $this->_sections['idx']['total'];
                 $this->_sections['idx']['index'] += $this->_sections['idx']['step'], $this->_sections['idx']['iteration']++):
$this->_sections['idx']['rownum'] = $this->_sections['idx']['iteration'];
$this->_sections['idx']['index_prev'] = $this->_sections['idx']['index'] - $this->_sections['idx']['step'];
$this->_sections['idx']['index_next'] = $this->_sections['idx']['index'] + $this->_sections['idx']['step'];
$this->_sections['idx']['first']      = ($this->_sections['idx']['iteration'] == 1);
$this->_sections['idx']['last']       = ($this->_sections['idx']['iteration'] == $this->_sections['idx']['total']);
?>
				     	<tr<?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this);?>
>
		                    <td align="center"><input type=hidden name = orderids[] value="<?php echo $this->_tpl_vars['orderListDisplay'][$this->_sections['idx']['index']]; ?>
"><a href="order.php?orderid=<?php echo $this->_tpl_vars['orderListDisplay'][$this->_sections['idx']['index']]; ?>
" target="_blank"><?php echo $this->_tpl_vars['orderListDisplay'][$this->_sections['idx']['index']]; ?>
</a></td>
						</tr>
		            <?php endfor; endif; ?>
	                <tr><td align=right><input type='submit' name='confirm' id="confirm_queue" value='Confirm' /></td></tr>
	            <?php else: ?>
	               <tr<?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this);?>
><td align="center">No Orders to Queue.</td></tr>
	            <?php endif; ?>
			</table>
		</form>
		
		<div style="height: 30px;"><hr/></div>	
		
		<?php if ($this->_tpl_vars['failedOrderListDisplay']): ?>
			<table cellpadding="2" cellspacing="1" width="100%">
				<tr class="TableHead">
					<td>Failed Orderids</td><td>Reason</td>
				</tr>
				
				<?php unset($this->_sections['idx1']);
$this->_sections['idx1']['name'] = 'idx1';
$this->_sections['idx1']['loop'] = is_array($_loop=$this->_tpl_vars['failedOrderListDisplay']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['idx1']['show'] = true;
$this->_sections['idx1']['max'] = $this->_sections['idx1']['loop'];
$this->_sections['idx1']['step'] = 1;
$this->_sections['idx1']['start'] = $this->_sections['idx1']['step'] > 0 ? 0 : $this->_sections['idx1']['loop']-1;
if ($this->_sections['idx1']['show']) {
    $this->_sections['idx1']['total'] = $this->_sections['idx1']['loop'];
    if ($this->_sections['idx1']['total'] == 0)
        $this->_sections['idx1']['show'] = false;
} else
    $this->_sections['idx1']['total'] = 0;
if ($this->_sections['idx1']['show']):

            for ($this->_sections['idx1']['index'] = $this->_sections['idx1']['start'], $this->_sections['idx1']['iteration'] = 1;
                 $this->_sections['idx1']['iteration'] <= $this->_sections['idx1']['total'];
                 $this->_sections['idx1']['index'] += $this->_sections['idx1']['step'], $this->_sections['idx1']['iteration']++):
$this->_sections['idx1']['rownum'] = $this->_sections['idx1']['iteration'];
$this->_sections['idx1']['index_prev'] = $this->_sections['idx1']['index'] - $this->_sections['idx1']['step'];
$this->_sections['idx1']['index_next'] = $this->_sections['idx1']['index'] + $this->_sections['idx1']['step'];
$this->_sections['idx1']['first']      = ($this->_sections['idx1']['iteration'] == 1);
$this->_sections['idx1']['last']       = ($this->_sections['idx1']['iteration'] == $this->_sections['idx1']['total']);
?>
					<tr<?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this);?>
>
						<td align="center">
	                        <input type=hidden name=failedorderids[] value="<?php echo $this->_tpl_vars['failedOrderListDisplay'][$this->_sections['idx1']['index']]['orderid']; ?>
"/>
	                        <a href="order.php?orderid=<?php echo $this->_tpl_vars['failedOrderListDisplay'][$this->_sections['idx1']['index']]['orderid']; ?>
"><?php echo $this->_tpl_vars['failedOrderListDisplay'][$this->_sections['idx1']['index']]['orderid']; ?>
</a>
						</td>
						<td align="center">
							<b><?php echo $this->_tpl_vars['failedOrderListDisplay'][$this->_sections['idx1']['index']]['msg']; ?>
</b>
						</td>
					</tr>
				<?php endfor; endif; ?>
			</table>
		<?php endif; ?>
	<?php endif;  endif;  $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean();  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'Upload Payment Recieved Orders csv to queue','content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>