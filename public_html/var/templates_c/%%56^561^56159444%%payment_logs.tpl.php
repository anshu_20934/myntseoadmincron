<?php /* Smarty version 2.6.12, created on 2017-03-30 01:18:30
         compiled from admin/payment_logs.tpl */ ?>
<?php ob_start(); ?>
    <div>
        <div style="float:left;background-color:#f0f0f0;">
            <form name="retrieve_pl"  action="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/payment_logs.php" method="post">
            <table cellpadding="3" cellspacing="1" width="100%">
                <tr>
                    <td>
                        <table>
                            <tr>    
                                <div>
                                    Retrieve for these Order Ids (Comma Seperated Values) <textarea cols="52" rows="2" name="csv-file-list" id="csv-file-list"></textarea>
                                </div>
                            </tr>    

                            <tr>
                               <td colspan="4">&nbsp;</td>                                
                            </tr>

                            <tr>
                               <td colspan="2">&nbsp;</td>
                                <td colspan="2"><input type="submit" value="Retrieve Payment Logs" class="button"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            </form>
        </div>
        <div style="clear:both;"></div>
    </div>

    <br/><hr>
   
    <div style="width:1125px;">
        <table cellpadding="3" cellspacing="1" width="90%">
            <tr class="TableHead">
                <th rowspan="1">Order Id</th>
                <th rowspan="1">Payment Gateway Name</th>
                <th rowspan="1">Payment Option</th>
                <th rowspan="1">Amount Paid</th>
                <th rowspan="1">Is Complete</th>
            </tr>

            <?php $_from = $this->_tpl_vars['plogs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['orderid'] => $this->_tpl_vars['orderlog']):
?> 
                <tr style="background-color:#f0f0f0;" align="center">
                    <td><?php echo $this->_tpl_vars['orderlog']['orderId']; ?>
</td>
                    <td><?php echo $this->_tpl_vars['orderlog']['gatewayName']; ?>
</td>
                    <td><?php echo $this->_tpl_vars['orderlog']['paymentOption']; ?>
</td>
                    <td><?php echo $this->_tpl_vars['orderlog']['amount']; ?>
</td>
                    <td><?php echo $this->_tpl_vars['orderlog']['isComplete']; ?>
</td>
                </tr>
            <?php endforeach; endif; unset($_from); ?>

        </table>

            </div>
<?php $this->_smarty_vars['capture']['pl_report'] = ob_get_contents(); ob_end_clean();  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'Payment Service Logs','content' => $this->_smarty_vars['capture']['pl_report'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>