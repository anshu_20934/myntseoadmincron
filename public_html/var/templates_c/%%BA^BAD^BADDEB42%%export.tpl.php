<?php /* Smarty version 2.6.12, created on 2017-06-02 14:52:08
         compiled from main/export.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'default', 'main/export.tpl', 68, false),array('modifier', 'escape', 'main/export.tpl', 105, false),array('modifier', 'date_format', 'main/export.tpl', 170, false),array('modifier', 'replace', 'main/export.tpl', 185, false),array('function', 'cycle', 'main/export.tpl', 167, false),)), $this); ?>
<?php func_load_lang($this, "main/export.tpl","txt_export_note,lbl_import_data_provider,lbl_import_data_provider,txt_data_provider_login_export,lbl_data_provider_login,lbl_export_data,lbl_csv_delimiter,lbl_data_rows_per_file,lbl_data_rows_per_file_expl,lbl_data_type,lbl_data_range,lbl_export,lbl_reset,lbl_view_export_log,lbl_export_packs,lbl_export_packs,lbl_check_all,lbl_uncheck_all,lbl_date,lbl_files,lbl_language,lbl_data_types,lbl_temporary_directory_for_images,lbl_delete_selected,lbl_export_data"); ?><?php ob_start(); ?>

<?php echo $this->_tpl_vars['lng']['txt_export_note']; ?>
<br />
<br />

<?php if ($this->_tpl_vars['need_select_provider']): ?>

<?php if ($this->_tpl_vars['data_provider'] != ''): ?>
<?php $this->assign('display_none_open', "display: none; "); ?>
<?php else: ?>
<?php $this->assign('display_none_close', "display: none; "); ?>
<?php endif; ?>

<div align="right">
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/visiblebox_link.tpl", 'smarty_include_vars' => array('mark' => '4','title' => $this->_tpl_vars['lng']['lbl_import_data_provider'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>

<br /><br />

<table cellpadding="0" cellspacing="0" width="100%" style="<?php echo $this->_tpl_vars['display_none_close']; ?>
" id="box4"><tr><td>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_import_data_provider'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php echo $this->_tpl_vars['lng']['txt_data_provider_login_export']; ?>


<form action="import.php" method="post" name="changeproviderform">
<input type="hidden" name="mode" value="export" />
<input type="hidden" name="action" value="change_provider" />

<table cellpadding="0" cellspacing="3">

<tr>
	<td><b><?php echo $this->_tpl_vars['lng']['lbl_data_provider_login']; ?>
:</b></td>
	<td><input type="text" size="35" name="data_provider" value="<?php echo $this->_tpl_vars['export_data']['provider']; ?>
" /></td>
	<td><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/button.tpl", 'smarty_include_vars' => array('href' => "javascript: document.changeproviderform.submit();",'type' => 'input')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>

</table>
</form>

<br /><br /><br />

	</td>
</tr>
</table>
<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_export_data'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<form action="import.php" method="post" name="exportdata_form">
<input type="hidden" name="mode" value="export" />

<table cellpadding="3" cellspacing="1" width="100%">

<tr>
	<td valign="top">
	<b><?php echo $this->_tpl_vars['lng']['lbl_csv_delimiter']; ?>
:</b><br />
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "provider/main/ie_delimiter.tpl", 'smarty_include_vars' => array('field_name' => "data[delimiter]",'saved_delimiter' => $this->_tpl_vars['export_data']['delimiter'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	</td>
</tr>

<tr><td>&nbsp;</td></tr>

<tr>
	<td valign="top">
	<b><?php echo $this->_tpl_vars['lng']['lbl_data_rows_per_file']; ?>
:</b><br />
<input type="text" name="data[rows_per_file]" value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['export_data']['rows_per_file'])) ? $this->_run_mod_handler('default', true, $_tmp, "") : smarty_modifier_default($_tmp, "")); ?>
" /><br />
<?php echo $this->_tpl_vars['lng']['lbl_data_rows_per_file_expl']; ?>

	</td>
</tr>

<?php if ($this->_tpl_vars['export_options'] != ''): ?>
<?php $_from = $this->_tpl_vars['export_options']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['v']):
?>
<tr><td>&nbsp;</td></tr>

<tr>
	<td><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['v'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr>
<?php endforeach; endif; unset($_from); ?>
<?php endif; ?>

<tr><td>&nbsp;</td></tr>

<tr><td valign="top">
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/check_all_row.tpl", 'smarty_include_vars' => array('style' => "line-height: 170%;",'form' => 'exportdata_form','prefix' => 'check')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<table cellspacing="1" cellpadding="0" width="100%">
<tr class="TableHead">
	<td width="15">&nbsp;</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_data_type']; ?>
</td>
	<td colspan="2"><?php echo $this->_tpl_vars['lng']['lbl_data_range']; ?>
</td>
</tr>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/export_specs.tpl", 'smarty_include_vars' => array('export_spec' => $this->_tpl_vars['export_spec'],'level' => 0)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</table>

	</td>
</tr>

<tr>
	<td>
	<table cellpadding="1" cellspacing="1" width="100%">
	<tr>
		<td class="SubmitBox">
		<input type="submit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_export'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" />
		</td>
		<td class="SubmitBox" align="right">
		<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_reset'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: reset_form('exportdata_form', exportdata_form_def); change_all(false);" />
		</td>
	</tr>
	</table>
	</td>
</tr>

</table>
</form>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "reset.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<script type="text/javascript">
<!--
var exportdata_form_def = [
	['data[delimiter]', ';'],
	['data[rows_per_file]', ''],
	['options[category_sep]', '/'],
	['options[export_images]', 'Y'],
	['options[images_directory]', '<?php echo $this->_tpl_vars['export_images_dir']; ?>
']
];
-->
</script>

<?php if ($this->_tpl_vars['export_log_url']): ?>
<br />
<div align="right"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/button.tpl", 'smarty_include_vars' => array('href' => $this->_tpl_vars['export_log_url'],'button_title' => $this->_tpl_vars['lng']['lbl_view_export_log'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div>
<?php endif; ?>

<?php if ($this->_tpl_vars['export_packs'] != ''): ?>
<br />
<a name="packs" />
<div align="right"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/visiblebox_link.tpl", 'smarty_include_vars' => array('mark' => 'epacks','title' => $this->_tpl_vars['lng']['lbl_export_packs'],'visible' => $GLOBALS['HTTP_GET_VARS']['status'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div>

<br /><br />

<form action="import.php" method="post" name="exportpacks_form">
<input type="hidden" name="mode" value="export" />
<input type="hidden" id="exportpacks_action" name="action" value="" />

<table cellpadding="0" cellspacing="0" width="100%" style="<?php if ($GLOBALS['HTTP_GET_VARS']['status'] != 'success'): ?>display: none;<?php endif; ?>" id="boxepacks"><tr><td>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_export_packs'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<script type="text/javascript" language="JavaScript 1.2"><!--
var ep_checkboxes = new Array(<?php $_from = $this->_tpl_vars['export_packs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>'packs[<?php echo $this->_tpl_vars['k']; ?>
]',<?php endforeach; endif; unset($_from); ?>'');
 
--></script>

<div style="line-height:170%"><a href="javascript:change_all(true, 'exportpacks_form', ep_checkboxes);"><?php echo $this->_tpl_vars['lng']['lbl_check_all']; ?>
</a> / <a href="javascript:change_all(false, 'exportpacks_form', ep_checkboxes);"><?php echo $this->_tpl_vars['lng']['lbl_uncheck_all']; ?>
</a></div>

<table cellpadding="2" cellspacing="1">
<tr class="TableHead">
	<td width="15">&nbsp;</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_date']; ?>
</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_files']; ?>
</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_language']; ?>
</td>
	<td><?php echo $this->_tpl_vars['lng']['lbl_data_types']; ?>
</td>
</tr>
<?php $_from = $this->_tpl_vars['export_packs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['kpack'] => $this->_tpl_vars['pack']):
?>
<tr<?php echo smarty_function_cycle(array('name' => 'ep','values' => " , class='TableSubHead'",'advance' => false), $this);?>
>
	<td valign="top" rowspan="<?php echo $this->_tpl_vars['pack']['count']; ?>
" height="20"><input type="checkbox" name="packs[<?php echo $this->_tpl_vars['kpack']; ?>
]" value="<?php echo $this->_tpl_vars['kpack']; ?>
" /></td>
	<td valign="top" rowspan="<?php echo $this->_tpl_vars['pack']['count']; ?>
" height="20">
		<table cellspacing="0" cellpadding="0" height="20"><tr><td><?php echo ((is_array($_tmp=$this->_tpl_vars['pack']['date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['config']['Appearance']['date_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['config']['Appearance']['date_format'])); ?>
 <?php echo ((is_array($_tmp=$this->_tpl_vars['pack']['date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['config']['Appearance']['time_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['config']['Appearance']['time_format'])); ?>
</td></tr></table>
	</td>
<?php $this->assign('is_first', 'Y'); ?>
<?php $_from = $this->_tpl_vars['pack']['files']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['fn'] => $this->_tpl_vars['f']):
?>
<?php if ($this->_tpl_vars['is_first'] == 'Y'): ?>
<?php $this->assign('is_first', 'F'); ?>
<?php else: ?>
<tr<?php echo smarty_function_cycle(array('name' => 'ep','values' => " , class='TableSubHead'",'advance' => false), $this);?>
>
<?php endif; ?>
	<td valign="top" rowspan="<?php echo ((is_array($_tmp=@$this->_tpl_vars['f']['sections_count'])) ? $this->_run_mod_handler('default', true, $_tmp, 1) : smarty_modifier_default($_tmp, 1)); ?>
"><a href="get_export.php?file=<?php echo $this->_tpl_vars['fn']; ?>
"><?php echo $this->_tpl_vars['fn']; ?>
</a></td>
	<td valign="top" rowspan="<?php echo ((is_array($_tmp=@$this->_tpl_vars['f']['sections_count'])) ? $this->_run_mod_handler('default', true, $_tmp, 1) : smarty_modifier_default($_tmp, 1)); ?>
" align="center"><?php if ($this->_tpl_vars['f']['code'] == ''): ?>-<?php else:  echo ((is_array($_tmp=@$this->_tpl_vars['f']['code_name'])) ? $this->_run_mod_handler('default', true, $_tmp, @$this->_tpl_vars['f']['code']) : smarty_modifier_default($_tmp, @$this->_tpl_vars['f']['code']));  endif; ?></td>
<?php if ($this->_tpl_vars['f']['sections_count'] > 0): ?>
<?php $this->assign('is_first2', 'Y'); ?>
<?php $_from = $this->_tpl_vars['f']['sections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['sec']):
?>
<?php if ($this->_tpl_vars['is_first2'] == 'Y'):  $this->assign('is_first2', 'F');  else: ?><tr<?php echo smarty_function_cycle(array('name' => 'ep','values' => " , class='TableSubHead'",'advance' => false), $this);?>
><?php endif; ?>
	<td nowrap="nowrap"><?php echo ((is_array($_tmp=$this->_tpl_vars['sec'])) ? $this->_run_mod_handler('replace', true, $_tmp, '_', ' ') : smarty_modifier_replace($_tmp, '_', ' ')); ?>
</td>
<?php if ($this->_tpl_vars['is_first2'] == 'F' && $this->_tpl_vars['is_first'] == 'F'): ?>
	<?php $this->assign('is_first', ""); ?>
	<?php $this->assign('is_first2', ""); ?>
<?php endif; ?>
</tr>
<?php endforeach; endif; unset($_from); ?>
<?php endif; ?>
</tr>
<?php endforeach; endif; unset($_from); ?>
</tr>
<?php if ($this->_tpl_vars['pack']['dir_exists']): ?>
<tr<?php echo smarty_function_cycle(array('name' => 'ep','values' => " , class='TableSubHead'",'advance' => false), $this);?>
>
	<td colspan="3" style="padding-top: 10px;"><?php echo $this->_tpl_vars['lng']['lbl_temporary_directory_for_images']; ?>
: <?php echo $this->_tpl_vars['pack']['dir_exists']; ?>
</td>
</tr>
<?php endif; ?>
<?php echo smarty_function_cycle(array('name' => 'ep','values' => " , class='TableSubHead'",'print' => false), $this);?>

<?php endforeach; endif; unset($_from); ?>
</table>

<br />

<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_delete_selected'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: document.getElementById('exportpacks_action').value='delete_pack'; document.exportpacks_form.submit();" />

</td></tr>
</table>

</form>
<?php endif; ?>

<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_export_data'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>