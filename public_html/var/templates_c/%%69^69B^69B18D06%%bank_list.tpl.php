<?php /* Smarty version 2.6.12, created on 2017-05-10 18:33:43
         compiled from admin/netbanking/bank_list.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "main/popup_product.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div style="padding-left: 320px;padding-bottom: 20px;font-size: 14px;">
	<a style="padding-left: 10px;" href="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/netbanking/activate_gateway.php"> ADD/EDIT GATEWAY MAPPING</a>
	<a style="padding-left: 10px;" href="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/netbanking/add_gateway.php"> ADD/EDIT GATEWAYS </a>
</div>
<?php echo '
<script type="text/javascript">
function delete_bank_name(id){
    var r=confirm("Are you sure you want to delete this bank");
    if(r == true){
        document.updatebankname.mode.value = \'delete\';
        document.updatebankname.bank_id.value = id;
        document.updatebankname.submit();
    }
}

function add_BankName(){
    if (jQuery.trim(document.updatebankname.bank_name_new.value) == \'\') {
        alert(\'Bank Name cannot be empty\');
        return;
    } 
    document.updatebankname.mode.value = \'add\';
    document.updatebankname.submit();
}
function edit_bank_gateway(id){
	window.location = "add_bank_gateway.php?id="+id;
}

</script>
'; ?>


<?php ob_start(); ?>
<form action="<?php echo $this->_tpl_vars['action_php']; ?>
" method="post" name="updatebankname">
<input type="hidden" name="mode" />
<input type="hidden" name="bank_id" />
<table cellpadding="3" cellspacing="1" width="100%">
<tr class="TableHead">
	<td width="20%">ID</td>
	<td width="40%">Name</td>
	<td width="20%">Popular</td>
	<td width="20%">Action</td>
</tr>

<?php if ($this->_tpl_vars['bank_list']): ?>
<?php unset($this->_sections['key_num']);
$this->_sections['key_num']['name'] = 'key_num';
$this->_sections['key_num']['loop'] = is_array($_loop=$this->_tpl_vars['bank_list']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['key_num']['show'] = true;
$this->_sections['key_num']['max'] = $this->_sections['key_num']['loop'];
$this->_sections['key_num']['step'] = 1;
$this->_sections['key_num']['start'] = $this->_sections['key_num']['step'] > 0 ? 0 : $this->_sections['key_num']['loop']-1;
if ($this->_sections['key_num']['show']) {
    $this->_sections['key_num']['total'] = $this->_sections['key_num']['loop'];
    if ($this->_sections['key_num']['total'] == 0)
        $this->_sections['key_num']['show'] = false;
} else
    $this->_sections['key_num']['total'] = 0;
if ($this->_sections['key_num']['show']):

            for ($this->_sections['key_num']['index'] = $this->_sections['key_num']['start'], $this->_sections['key_num']['iteration'] = 1;
                 $this->_sections['key_num']['iteration'] <= $this->_sections['key_num']['total'];
                 $this->_sections['key_num']['index'] += $this->_sections['key_num']['step'], $this->_sections['key_num']['iteration']++):
$this->_sections['key_num']['rownum'] = $this->_sections['key_num']['iteration'];
$this->_sections['key_num']['index_prev'] = $this->_sections['key_num']['index'] - $this->_sections['key_num']['step'];
$this->_sections['key_num']['index_next'] = $this->_sections['key_num']['index'] + $this->_sections['key_num']['step'];
$this->_sections['key_num']['first']      = ($this->_sections['key_num']['iteration'] == 1);
$this->_sections['key_num']['last']       = ($this->_sections['key_num']['iteration'] == $this->_sections['key_num']['total']);
?>
<tr>
	<td align="center" style="left-padding:30px"><?php echo $this->_tpl_vars['bank_list'][$this->_sections['key_num']['index']]['id']; ?>
.</td>
	<td align="center" style="left-padding:30px"><input name="update_name_field[<?php echo $this->_tpl_vars['bank_list'][$this->_sections['key_num']['index']]['id']; ?>
]" type="text" value="<?php echo $this->_tpl_vars['bank_list'][$this->_sections['key_num']['index']]['name']; ?>
" /></td>
	<td align="center" style="left-padding:30px"><input name="update_popular_field[<?php echo $this->_tpl_vars['bank_list'][$this->_sections['key_num']['index']]['id']; ?>
]" type="checkbox" <?php if ($this->_tpl_vars['bank_list'][$this->_sections['key_num']['index']]['is_popular'] != 0): ?>checked="true"<?php endif; ?> /></td>
	<td align="center"><input type="button" value="Add/Edit Gateways" onclick="javascript: edit_bank_gateway('<?php echo $this->_tpl_vars['bank_list'][$this->_sections['key_num']['index']]['id']; ?>
')" /><input type="button" value="Delete" onclick="javascript: delete_bank_name('<?php echo $this->_tpl_vars['bank_list'][$this->_sections['key_num']['index']]['id']; ?>
')" /></td>	
</tr>
<?php endfor; endif; ?>
<?php else: ?>
<tr>
<td colspan="4" align="center">No Banks added as yet</td>
</tr>
<?php endif; ?>

<tr>
	<td></td>
	<td align="center" style="left-padding:30px"><input name="bank_name_new" type="text" value="" /></td>
	<td align="center" style="left-padding:30px"><input name="bank_popular_new" type="checkbox" /></td>
	<td align="center">
		<input type="button" value="Add" onclick="javascript: add_BankName()" />
	</td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td class="SubmitBox" align="right">
	   <input type="button" value="Update" onclick="javascript: document.updatebankname.mode.value = 'update'; document.updatebankname.submit();" />
	</td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td class="SubmitBox" align="right">
	   <input type="button" value="Clear XCache" onclick="javascript: document.updatebankname.mode.value = 'clearCache'; document.updatebankname.submit();" />
	</td>
</tr>
</table>
</form>

<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'Bank Name List','content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>