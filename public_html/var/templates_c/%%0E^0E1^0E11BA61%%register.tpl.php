<?php /* Smarty version 2.6.12, created on 2017-03-28 06:18:30
         compiled from partner/main/register.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'default', 'partner/main/register.tpl', 116, false),array('modifier', 'strip_tags', 'partner/main/register.tpl', 163, false),array('modifier', 'escape', 'partner/main/register.tpl', 163, false),)), $this); ?>
<?php func_load_lang($this, "partner/main/register.tpl","lbl_modify_profile,lbl_create_profile,lbl_create_partner_profile,lbl_modify_partner_profile,txt_create_partner_profile,txt_modify_partner_profile,txt_create_profile_msg_partner,txt_fields_are_mandatory,lbl_return_to_search_results,txt_registration_error,txt_email_already_exists,txt_user_already_exists,err_billing_state,err_shipping_state,txt_email_invalid,txt_terms_and_conditions_newbie_note,lbl_save,txt_newbie_registration_bottom,lbl_terms_n_conditions,txt_user_registration_bottom,txt_profile_modified,txt_partner_created,txt_profile_created,lbl_profile_details,txt_partner_profile_is_not_approved,lbl_approved,lbl_declined,lbl_approve_or_decline_partner_profile"); ?><?php if ($this->_tpl_vars['js_enabled'] == 'Y'): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "check_email_script.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "check_zipcode_js.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "generate_required_fields_js.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "check_required_fields_js.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php if ($this->_tpl_vars['config']['General']['use_js_states'] == 'Y'): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "change_states_js.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>
<?php endif; ?>

<?php if ($this->_tpl_vars['newbie'] == 'Y'): ?>
<?php if ($this->_tpl_vars['login'] != ""): ?>
<?php $this->assign('title', $this->_tpl_vars['lng']['lbl_modify_profile']); ?>
<?php else: ?>
<?php $this->assign('title', $this->_tpl_vars['lng']['lbl_create_profile']); ?>
<?php endif; ?>
<?php else: ?>
<?php if ($this->_tpl_vars['main'] == 'user_add'): ?>
<?php $this->assign('title', $this->_tpl_vars['lng']['lbl_create_partner_profile']); ?>
<?php else: ?>
<?php $this->assign('title', $this->_tpl_vars['lng']['lbl_modify_partner_profile']); ?>
<?php endif; ?>
<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "page_title.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['title'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- IN THIS SECTION -->

<?php if ($this->_tpl_vars['newbie'] != 'Y'): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog_tools.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>

<!-- IN THIS SECTION -->

<font class="Text">

<?php if ($this->_tpl_vars['usertype'] != 'B'): ?>
<br />
<?php if ($this->_tpl_vars['main'] == 'user_add'): ?>
<?php echo $this->_tpl_vars['lng']['txt_create_partner_profile']; ?>

<?php else: ?>
<?php echo $this->_tpl_vars['lng']['txt_modify_partner_profile']; ?>

<?php endif; ?>
<?php else: ?>
<?php echo $this->_tpl_vars['lng']['txt_create_profile_msg_partner']; ?>

<?php endif; ?>
<br /><br />

<?php echo $this->_tpl_vars['lng']['txt_fields_are_mandatory']; ?>


</font>

<br /><br />

<?php ob_start(); ?>

<?php if ($this->_tpl_vars['newbie'] != 'Y' && $this->_tpl_vars['main'] != 'user_add' && ( $this->_tpl_vars['usertype'] == 'P' && $this->_tpl_vars['active_modules']['Simple_Mode'] == 'Y' || $this->_tpl_vars['usertype'] == 'A' )): ?>
<div align="right"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/button.tpl", 'smarty_include_vars' => array('button_title' => $this->_tpl_vars['lng']['lbl_return_to_search_results'],'href' => "users.php?mode=search")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div>
<?php endif; ?>

<?php $this->assign('reg_error', $this->_tpl_vars['top_message']['reg_error']); ?>
<?php $this->assign('error', $this->_tpl_vars['top_message']['error']); ?>
<?php $this->assign('emailerror', $this->_tpl_vars['top_message']['emailerror']); ?>

<?php if ($this->_tpl_vars['registered'] == ""): ?>
<?php if ($this->_tpl_vars['reg_error']): ?>
<font class="Star">
<?php if ($this->_tpl_vars['reg_error'] == 'F'): ?>
<?php echo $this->_tpl_vars['lng']['txt_registration_error']; ?>

<?php elseif ($this->_tpl_vars['reg_error'] == 'E'): ?>
<?php echo $this->_tpl_vars['lng']['txt_email_already_exists']; ?>

<?php elseif ($this->_tpl_vars['reg_error'] == 'U'): ?>
<?php echo $this->_tpl_vars['lng']['txt_user_already_exists']; ?>

<?php endif; ?>
</font>
<br />
<?php endif; ?>

<?php if ($this->_tpl_vars['error'] != ""): ?>
<font class="Star">
<?php if ($this->_tpl_vars['error'] == 'b_statecode'): ?>
<?php echo $this->_tpl_vars['lng']['err_billing_state']; ?>

<?php elseif ($this->_tpl_vars['error'] == 's_statecode'): ?>
<?php echo $this->_tpl_vars['lng']['err_shipping_state']; ?>

<?php elseif ($this->_tpl_vars['error'] == 'email'): ?>
<?php echo $this->_tpl_vars['lng']['txt_email_invalid']; ?>

<?php else: ?>
<?php echo $this->_tpl_vars['error']; ?>

<?php endif; ?>
</font>
<br />
<?php endif; ?>

<script type="text/javascript" language="JavaScript 1.2">
<!--
var is_run = false;
function check_registerform_fields() {
	if(is_run)
		return false;
	is_run = true;
	if (check_zip_code()<?php if ($this->_tpl_vars['default_fields']['email']['required'] == 'Y'): ?> && checkEmailAddress(document.registerform.email)<?php endif; ?> && checkRequired(requiredFields)) {
		document.registerform.submit();
		return true;
	}
	is_run = false;
	return false;
}
-->
</script>
 
<table cellspacing="1" cellpadding="2" width="100%">

<form action="<?php echo $this->_tpl_vars['register_script_name']; ?>
?<?php echo $GLOBALS['HTTP_SERVER_VARS']['QUERY_STRING']; ?>
" method="post" name="registerform" onsubmit="check_registerform_fields(); return false;">

<input type="hidden" name="parent" value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['parent'])) ? $this->_run_mod_handler('default', true, $_tmp, @$this->_tpl_vars['userinfo']['parent']) : smarty_modifier_default($_tmp, @$this->_tpl_vars['userinfo']['parent'])); ?>
" />

<?php if ($this->_tpl_vars['config']['Security']['use_https_login'] == 'Y'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['XCARTSESSNAME']; ?>
" value="<?php echo $this->_tpl_vars['XCARTSESSID']; ?>
" />
<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/register_personal_info.tpl", 'smarty_include_vars' => array('userinfo' => $this->_tpl_vars['userinfo'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/register_billing_address.tpl", 'smarty_include_vars' => array('userinfo' => $this->_tpl_vars['userinfo'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/register_shipping_address.tpl", 'smarty_include_vars' => array('userinfo' => $this->_tpl_vars['userinfo'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/register_contact_info.tpl", 'smarty_include_vars' => array('userinfo' => $this->_tpl_vars['userinfo'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/register_additional_info.tpl", 'smarty_include_vars' => array('section' => 'A')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "partner/main/register_plan.tpl", 'smarty_include_vars' => array('userinfo' => $this->_tpl_vars['userinfo'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/register_account.tpl", 'smarty_include_vars' => array('userinfo' => $this->_tpl_vars['userinfo'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


<?php if ($this->_tpl_vars['active_modules']['News_Management'] && $this->_tpl_vars['newslists']): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "modules/News_Management/register_newslists.tpl", 'smarty_include_vars' => array('userinfo' => $this->_tpl_vars['userinfo'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>


<tr>
<td colspan="3" align="center">
<br /><br />
<?php if ($this->_tpl_vars['newbie'] == 'Y'): ?>
<?php echo $this->_tpl_vars['lng']['txt_terms_and_conditions_newbie_note']; ?>

<?php endif; ?>
</td>
</tr>

<tr>
<td colspan="2">&nbsp;</td>
<td>

<font class="FormButton">
<?php if ($GLOBALS['HTTP_GET_VARS']['mode'] == 'update'): ?>
<input type="hidden" name="mode" value="update" />
<?php endif; ?>

<?php if ($this->_tpl_vars['js_enabled'] && $this->_tpl_vars['usertype'] == 'B'): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/submit.tpl", 'smarty_include_vars' => array('type' => 'input','style' => 'button','href' => "javascript: return check_registerform_fields();")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php else: ?>
<input type="submit" value=" <?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['lng']['lbl_save'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp, false) : smarty_modifier_strip_tags($_tmp, false)))) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
 " />
<?php endif; ?>

</td>
</tr>

<input type="hidden" name="usertype" value="<?php if ($GLOBALS['HTTP_GET_VARS']['usertype'] != ""):  echo ((is_array($_tmp=$GLOBALS['HTTP_GET_VARS']['usertype'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html'));  else:  echo $this->_tpl_vars['usertype'];  endif; ?>" />

</form>

</table>

<br /><br />

<?php if ($this->_tpl_vars['newbie'] == 'Y'): ?>
<?php echo $this->_tpl_vars['lng']['txt_newbie_registration_bottom']; ?>

<br /><a href="help.php?section=conditions"><font class="Text"><b><?php echo $this->_tpl_vars['lng']['lbl_terms_n_conditions']; ?>
</b>&nbsp;</font><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/go.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></a>
<?php else: ?>
<?php echo $this->_tpl_vars['lng']['txt_user_registration_bottom']; ?>

<?php endif; ?>

<br />

<?php else: ?>

<?php if ($GLOBALS['HTTP_POST_VARS']['mode'] == 'update' || $GLOBALS['HTTP_GET_VARS']['mode'] == 'update'): ?>
<?php echo $this->_tpl_vars['lng']['txt_profile_modified']; ?>

<?php elseif ($GLOBALS['HTTP_GET_VARS']['usertype'] == 'B' || $this->_tpl_vars['usertype'] == 'B'): ?>
<?php echo $this->_tpl_vars['lng']['txt_partner_created']; ?>

<?php else: ?>
<?php echo $this->_tpl_vars['lng']['txt_profile_created']; ?>

<?php endif; ?>
<?php endif; ?>

<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_profile_details'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($this->_tpl_vars['userinfo']['status'] == 'Q' && $this->_tpl_vars['usertype'] != 'B'): ?>

<br />

<?php ob_start(); ?>

<form action="<?php echo $this->_tpl_vars['register_script_name']; ?>
?<?php echo $GLOBALS['HTTP_SERVER_VARS']['QUERY_STRING']; ?>
" method="post" name="decisionform">

<input type="hidden" name="mode" value="" />

<?php echo $this->_tpl_vars['lng']['txt_partner_profile_is_not_approved']; ?>

<br /><br />

<textarea name="reason" cols="40" rows="5"></textarea>

<br />

<input type="button" value="<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['lng']['lbl_approved'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp, false) : smarty_modifier_strip_tags($_tmp, false)))) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: document.decisionform.mode.value='approved'; document.decisionform.submit();" />
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" value="<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['lng']['lbl_declined'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp, false) : smarty_modifier_strip_tags($_tmp, false)))) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: document.decisionform.mode.value='declined'; document.decisionform.submit();" />

</form>

<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_approve_or_decline_partner_profile'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php endif; ?>
