<?php /* Smarty version 2.6.12, created on 2017-04-19 11:19:29
         compiled from admin/sem/semPromo.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'admin/sem/semPromo.tpl', 139, false),array('modifier', 'count', 'admin/sem/semPromo.tpl', 160, false),)), $this); ?>

<?php echo '
<script type="text/javascript">
$(document).ready(function(){

	/*
	 * onclick event to select all promos
	 */	
	$("#selectallpromo").click(function(){
		if($("#selectallpromo").attr("checked")){
			$("form input:checkbox").attr("checked",true);
		} else {
			$("form input:checkbox").attr("checked",false);
		}
		
	});	
});

/*
 * selects a promo and sets hidden field to selected promo id
 */
function selectPromo(selectedPromoId){
	$("form input:checkbox").attr("checked",false);
	$("#promo_"+selectedPromoId+"_selector").attr("checked",true);
	$("#selectedpromo").val(selectedPromoId);
}

/*
 * validates an elemet value for empty and shows the error message on empty
 * @param:(object)checkElementObj - element to be checked
 * @param:(string)msgShowId - error element id
 * @return:(bool)
 */
function validateElementForEmpty(checkElementObj,msgShowId){
	if($(checkElementObj).val()==\'\'){
		$(checkElementObj).focus();
		$("#"+msgShowId).text(\'The field can not be empty\');
		return false;
		
	} else {
		$("#"+msgShowId).text(\'\');
		return true;
	}	
}

/*
 * validates all the text elemets under parentObj
 * @param:(object)parentObj - element under which all the input:text will be validated
 * @param:(string)msgShowId - error element id
 * @return:(bool)false /(void)
 */
function validateAddForm(parentId,msgShowId){
	var hasError=false;
	$("#"+parentId+" input:text").each(function(){		
		if(!validateElementForEmpty(this,msgShowId)){
			hasError=true;
			return false;//break
		}		
	});
	if(!hasError){
		$("#mode").val("addpromo");
		$("#addpromo").trigger(\'submit\');
	}
}

/*
 * validates all the text elemets under parentObj
 * @param:(object)parentObj - element under which all the input:text will be validated
 * @param:(string)msgShowId - error element id
 * @return:(bool)false /(void)
 */
function validateUpdateForm(parentId,promoId){
	var hasError=false;
	//update single promo
	if (promoId){	
		$("#"+parentId+" input[name$=\'["+promoId+"]\']").each(function(){		
			if(!validateElementForEmpty(this,\'promo_\'+promoId+\'_message\')){
				hasError=true;
				return false;//break from loop
			}		
		});
		if(!hasError){
			$(\'#mode\').val(\'updatepromo\');
			selectPromo(promoId);
			$("#addpromo").trigger(\'submit\');
		}
		
	} else {//update multiple promos	
		$("#"+parentId+" input:text").each(function(){
			if(!validateElementForEmpty(this,\'promo_all_update_message\')){
				hasError=true;
				return false;//break from loop
			}		
		});
		if(!hasError){
			$(\'#mode\').val(\'updateallpromo\');			
			$("#addpromo").trigger(\'submit\');
		}
	}

}
</script>
'; ?>


<?php ob_start(); ?>
	<form action="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/sem/semPromo.php" method="post" name="addpromo" id="addpromo">
		<input type="hidden" name="mode" id="mode"/>
		<input type="hidden" name="selectedpromo" id="selectedpromo"/>		
		<table cellpadding="3" cellspacing="1" width="100%">		
			<tr><td align="center" style="font-size:15px;"><?php echo $this->_tpl_vars['promomessage']; ?>
</td></tr>
			<tr>				
				<td>
					<table id="addFormTable">
						<tr><td id="promo_add_message" style="color:red"></td></tr>
						<tr>
							<td align="center"><label>Identifier:</label><input name="identifier" type="text" value=""/></td>
							<td align="center"><label>Content:</label><input name="promo_content" type="text" value="" maxlength="130"/></td>
							<td align="center"><label>URL:</label><input name="promo_url" type="text" value="" /></td>
							<td align="center"><input type="button" value="Add New Promo" onclick="javascript:validateAddForm('addFormTable','promo_add_message');"/></td>							
						</tr>
					</table><br/><br/>
				</td>
			</tr>
			<tr><td style="color:green;">Note: example url structure for these identifiers to work: http://www.myntra.com?semp=identifier&... </td></tr>
		</table>
		<table cellpadding="3" cellspacing="1" width="100%" id="updateFormTable">	
			<tr class="TableHead">
				<th>Select All<input type="checkbox" name="selectallpromo" id="selectallpromo"/></th>
				<th width="10%">Identifier</th>
				<th width="40%">Content</th>
				<th width="30%">URL</th>
				<th width="10%">Date Created</th>
				<th width="10%">Action</th>
			</tr>			
			<tr><td colspan="6" id="promo_all_update_message" style="color:red"></td></tr>
			<?php unset($this->_sections['idx']);
$this->_sections['idx']['name'] = 'idx';
$this->_sections['idx']['loop'] = is_array($_loop=$this->_tpl_vars['promos']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['idx']['show'] = true;
$this->_sections['idx']['max'] = $this->_sections['idx']['loop'];
$this->_sections['idx']['step'] = 1;
$this->_sections['idx']['start'] = $this->_sections['idx']['step'] > 0 ? 0 : $this->_sections['idx']['loop']-1;
if ($this->_sections['idx']['show']) {
    $this->_sections['idx']['total'] = $this->_sections['idx']['loop'];
    if ($this->_sections['idx']['total'] == 0)
        $this->_sections['idx']['show'] = false;
} else
    $this->_sections['idx']['total'] = 0;
if ($this->_sections['idx']['show']):

            for ($this->_sections['idx']['index'] = $this->_sections['idx']['start'], $this->_sections['idx']['iteration'] = 1;
                 $this->_sections['idx']['iteration'] <= $this->_sections['idx']['total'];
                 $this->_sections['idx']['index'] += $this->_sections['idx']['step'], $this->_sections['idx']['iteration']++):
$this->_sections['idx']['rownum'] = $this->_sections['idx']['iteration'];
$this->_sections['idx']['index_prev'] = $this->_sections['idx']['index'] - $this->_sections['idx']['step'];
$this->_sections['idx']['index_next'] = $this->_sections['idx']['index'] + $this->_sections['idx']['step'];
$this->_sections['idx']['first']      = ($this->_sections['idx']['iteration'] == 1);
$this->_sections['idx']['last']       = ($this->_sections['idx']['iteration'] == $this->_sections['idx']['total']);
?>			
			<tr><td colspan="6" id="promo_<?php echo $this->_tpl_vars['promos'][$this->_sections['idx']['index']]['id']; ?>
_message" style="color:red"></td></tr>
			<tr <?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this);?>
>		
				<td align="center"><input name="promo[selector][<?php echo $this->_tpl_vars['promos'][$this->_sections['idx']['index']]['id']; ?>
]" id="promo_<?php echo $this->_tpl_vars['promos'][$this->_sections['idx']['index']]['id']; ?>
_selector" type="checkbox"/></td>
				<td align="center"><input name="promo[identifier][<?php echo $this->_tpl_vars['promos'][$this->_sections['idx']['index']]['id']; ?>
]" type="text" value="<?php echo $this->_tpl_vars['promos'][$this->_sections['idx']['index']]['identifier']; ?>
"/></td>
				<td align="center"><input name="promo[promo_content][<?php echo $this->_tpl_vars['promos'][$this->_sections['idx']['index']]['id']; ?>
]" type="text" value="<?php echo $this->_tpl_vars['promos'][$this->_sections['idx']['index']]['promo_content']; ?>
" style="width:350px;" maxlength="130"/></td>
				<td align="center"><input name="promo[promo_url][<?php echo $this->_tpl_vars['promos'][$this->_sections['idx']['index']]['id']; ?>
]" type="text" value="<?php echo $this->_tpl_vars['promos'][$this->_sections['idx']['index']]['promo_url']; ?>
" style="width:300px;"/></td>
				<td align="center"><label><?php echo $this->_tpl_vars['promos'][$this->_sections['idx']['index']]['created_date']; ?>
</label></td>				
				<td align="center">
					<table>
						<tr>
							<td><input type="button" value="Update" onclick="if(confirm('Are you sure to update?')){validateUpdateForm('updateFormTable','<?php echo $this->_tpl_vars['promos'][$this->_sections['idx']['index']]['id']; ?>
');}else{return false;}"/></td>
							<td><input type="submit" value="Delete" onclick="if(confirm('Are you sure to delete?')){$('#mode').val('deletepromo');selectPromo('<?php echo $this->_tpl_vars['promos'][$this->_sections['idx']['index']]['id']; ?>
');}else{return false;}"/></td>
						</tr>
					</table>
				</td>	
			</tr>			
			<?php endfor; else: ?>
				<tr>
					<td colspan="2" align="center">No Promo is available!</td>
				</tr>
			<?php endif; ?>
			
			<?php if (count($this->_tpl_vars['promos']) > 1): ?>
			<tr>
				<td colspan="5">&nbsp;</td>
				<td align="center"><input type="button" value="Update All Selected Promos" onclick="if(confirm('Are you sure to update all promos?')){validateUpdateForm('updateFormTable','');}else{return false;}" /><!--$(this).attr('disabled','disabled');$(this).val('Updating all promos, Please wait...');-->
				</td>
				<td></td>
			</tr>
			<?php endif; ?>
		</table>
	</form>
	
<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'SEM Promos','content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>