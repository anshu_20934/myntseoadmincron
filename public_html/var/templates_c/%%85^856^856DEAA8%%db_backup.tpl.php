<?php /* Smarty version 2.6.12, created on 2017-09-01 21:02:18
         compiled from admin/main/db_backup.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'substitute', 'admin/main/db_backup.tpl', 22, false),array('modifier', 'escape', 'admin/main/db_backup.tpl', 27, false),array('modifier', 'strip_tags', 'admin/main/db_backup.tpl', 34, false),)), $this); ?>
<?php func_load_lang($this, "admin/main/db_backup.tpl","lbl_database_backup_restore,txt_database_backup_restore_top_text,lbl_backup_database,txt_backup_database_text,txt_write_sql_dump_to_file,lbl_generate_sql_file,txt_backup_database_note,txt_operation_is_irreversible_warning,lbl_restore_database,txt_restore_database_text,lbl_restore,txt_restore_database_from_file,lbl_restore_from_file,txt_max_file_size_warning,txt_restore_database_note,lbl_database_backup_restore"); ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "page_title.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_database_backup_restore'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php echo $this->_tpl_vars['lng']['txt_database_backup_restore_top_text']; ?>


<br /><br />

<?php ob_start(); ?>
<form action="db_backup.php" method="post">

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_backup_database'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<br />

<?php echo $this->_tpl_vars['lng']['txt_backup_database_text']; ?>


<br /><br />

<table cellpadding="0" cellspacing="0">
<tr>
	<td><input type="checkbox" id="write_to_file" name="write_to_file" value="Y" /></td>
	<td><label for="write_to_file"><?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_write_sql_dump_to_file'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'file', $this->_tpl_vars['sqldump_file']) : smarty_modifier_substitute($_tmp, 'file', $this->_tpl_vars['sqldump_file'])); ?>
</label></td>
</tr>
</table>

<br />
<input type="submit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_generate_sql_file'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" />
<input type="hidden" name="mode" value="backup" />
</form>
<?php echo $this->_tpl_vars['lng']['txt_backup_database_note']; ?>

<br />
<br />
<br />
<form action="db_backup.php" method="post" name="dbrestoreform" enctype="multipart/form-data" onsubmit='javascript: return confirm("<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_operation_is_irreversible_warning'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
")'>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_restore_database'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<br />

<?php echo $this->_tpl_vars['lng']['txt_restore_database_text']; ?>


<br /><br />

<?php if ($this->_tpl_vars['file_exists']): ?>
<input type="hidden" name="local_file" value="" />
<table cellpadding="0" cellspacing="0">
<tr>
	<td valign="top"><input type="submit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_restore'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: document.dbrestoreform.local_file.value = 'on';" /></td>
	<td>&nbsp;-&nbsp;</td>
	<td><?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_restore_database_from_file'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'file', $this->_tpl_vars['sqldump_file']) : smarty_modifier_substitute($_tmp, 'file', $this->_tpl_vars['sqldump_file'])); ?>
</td>
</tr>
</table>
<br />
<?php endif; ?>
<input type="file" name="userfile" />&nbsp;
<input type="submit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_restore_from_file'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" /><br />
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_max_file_size_warning'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'size', $this->_tpl_vars['upload_max_filesize']) : smarty_modifier_substitute($_tmp, 'size', $this->_tpl_vars['upload_max_filesize'])); ?>

<input type="hidden" name="mode" value="restore" />
</form>
<?php echo $this->_tpl_vars['lng']['txt_restore_database_note']; ?>

<br /><br />
<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_database_backup_restore'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>