<?php /* Smarty version 2.6.12, created on 2017-07-07 10:01:04
         compiled from admin/netbanking/activate_gateway.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "main/popup_product.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div style="padding-left: 320px;padding-bottom: 20px;font-size: 14px;">
	<a style="padding-left: 10px;" href="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/netbanking/add_gateway.php"> ADD/EDIT GATEWAY </a>
	<a style="padding-left: 10px;" href="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/netbanking/bank_list.php"> ADD/EDIT BANKS </a>
</div>
<?php echo '
<script type="text/javascript">
function delete_gateway(id){
	document.addgateway.mode.value = \'delete\';
	document.addgateway.mapping_id.value = id;
	document.addgateway.submit();
}

function add_gateway(id){
    document.addgateway.mode.value = \'add\';
	document.addgateway.mapping_id.value = id;
    document.addgateway.submit();
}

function activateAll(name) {
	document.addgateway.mode.value = \'activate\';
	document.addgateway.mapping_id.value = name;
    document.addgateway.submit();
}

function addnew_gateway(name) {
	document.addgateway.mode.value = \'add_gateway\';
	document.addgateway.mapping_id.value = name;
    document.addgateway.submit();
}

</script>
'; ?>


<?php ob_start(); ?>
<form action="<?php echo $this->_tpl_vars['action_php']; ?>
" method="post" name="addgateway">
<input type="hidden" name="mode" />
<input type="hidden" name="mapping_id" value=""/>
<table cellpadding="3" cellspacing="5" width="100%">
<tr class="TableHead">
	<td width="40%">Bank Name</td>
	<td width="40%">Gateways</td>
	<td width="20%">Action</td>
</tr>

<?php if ($this->_tpl_vars['bank_gateway_mapping']): ?>
<?php $_from = $this->_tpl_vars['bank_gateway_mapping']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['name'] => $this->_tpl_vars['entry']):
?> 
<tr style="margin-top:5px;">
	<td align="center" style="left-padding:30px"><?php echo $this->_tpl_vars['name']; ?>
</td>
	<td align="center" style="left-padding:30px">
	<?php unset($this->_sections['key_num']);
$this->_sections['key_num']['name'] = 'key_num';
$this->_sections['key_num']['loop'] = is_array($_loop=$this->_tpl_vars['entry']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['key_num']['show'] = true;
$this->_sections['key_num']['max'] = $this->_sections['key_num']['loop'];
$this->_sections['key_num']['step'] = 1;
$this->_sections['key_num']['start'] = $this->_sections['key_num']['step'] > 0 ? 0 : $this->_sections['key_num']['loop']-1;
if ($this->_sections['key_num']['show']) {
    $this->_sections['key_num']['total'] = $this->_sections['key_num']['loop'];
    if ($this->_sections['key_num']['total'] == 0)
        $this->_sections['key_num']['show'] = false;
} else
    $this->_sections['key_num']['total'] = 0;
if ($this->_sections['key_num']['show']):

            for ($this->_sections['key_num']['index'] = $this->_sections['key_num']['start'], $this->_sections['key_num']['iteration'] = 1;
                 $this->_sections['key_num']['iteration'] <= $this->_sections['key_num']['total'];
                 $this->_sections['key_num']['index'] += $this->_sections['key_num']['step'], $this->_sections['key_num']['iteration']++):
$this->_sections['key_num']['rownum'] = $this->_sections['key_num']['iteration'];
$this->_sections['key_num']['index_prev'] = $this->_sections['key_num']['index'] - $this->_sections['key_num']['step'];
$this->_sections['key_num']['index_next'] = $this->_sections['key_num']['index'] + $this->_sections['key_num']['step'];
$this->_sections['key_num']['first']      = ($this->_sections['key_num']['iteration'] == 1);
$this->_sections['key_num']['last']       = ($this->_sections['key_num']['iteration'] == $this->_sections['key_num']['total']);
?>
		<span style="background: #FD6;padding: 5px;margin:5px;border: 1px solid;-moz-border-radius: 5px !important;-webkit-border-radius: 5px !important;-khtml-border-radius: 5px !important;border-radius: 5px;">
		<?php if ($this->_tpl_vars['entry'][$this->_sections['key_num']['index']]['is_active']): ?>
			<input type='hidden' name="update_name_field[<?php echo $this->_tpl_vars['entry'][$this->_sections['key_num']['index']]['id']; ?>
]">
				<?php echo $this->_tpl_vars['entry'][$this->_sections['key_num']['index']]['gateway_name']; ?>

			</input>
			<a href="javascript:void(0);" onclick="delete_gateway(<?php echo $this->_tpl_vars['entry'][$this->_sections['key_num']['index']]['id']; ?>
)"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/clear.gif" /></a>
		<?php else: ?>
			<input type='hidden' name="update_name_field[<?php echo $this->_tpl_vars['entry'][$this->_sections['key_num']['index']]['id']; ?>
]">
				<?php echo $this->_tpl_vars['entry'][$this->_sections['key_num']['index']]['gateway_name']; ?>

			</input>
			<a href="javascript:void(0);" onclick="add_gateway(<?php echo $this->_tpl_vars['entry'][$this->_sections['key_num']['index']]['id']; ?>
)"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/plus.gif" /></a>
		<?php endif; ?>
		</span>
	<?php endfor; endif; ?>
	</td>
	<td align="center">
		<input type="button" value="Activate All" onclick="javascript: activateAll('<?php echo $this->_tpl_vars['name']; ?>
');" />
		<input type="button" value="Assign a new gateway for this bank" onclick="javascript: addnew_gateway('<?php echo $this->_tpl_vars['name']; ?>
');" />		
	</td>
	
</tr>
<?php endforeach; endif; unset($_from); ?> 
<?php else: ?>
<tr>
	<td align="center" colspan="2">No Bank Gateways mapping exists</td>
</tr>
<?php endif; ?>
<tr>
	<td></td>
	<td></td>
	<td class="SubmitBox" align="right">
	   <input type="button" value="Reset All to defaults" onclick="javascript: document.addgateway.mode.value = 'reset'; document.addgateway.mapping_id.value = '1';document.addgateway.submit();" />
	</td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td class="SubmitBox" align="right">
	   <input type="button" value="Clear XCache" onclick="javascript: document.addgateway.mode.value = 'clearCache'; document.addgateway.mapping_id.value = '1';document.addgateway.submit();" />
	</td>
</tr>

</table>
</form>

<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'Add New Netbanking Payment Gateways','content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>