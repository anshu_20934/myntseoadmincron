<?php /* Smarty version 2.6.12, created on 2017-05-20 14:49:03
         compiled from admin/main/update_core_items.tpl */ ?>

<link rel="stylesheet" href="<?php echo $this->_tpl_vars['SkinDir']; ?>
/skin1_admin_operations.css" />
<?php echo '
<script Language="JavaScript" Type="text/javascript">
	
	$(function() {
		$("input[name=\'property\']").click(function(){
			if( $(this).val() == "status" ) {
				$("#status_change_row").css("display", "table-row");
				$("#quality_change_row").css("display", "none");
			} else {
				$("#quality_change_row").css("display", "table-row");
				$("#status_change_row").css("display", "none");
			}
		});

		$(\'#update-items-form\').submit(function(){
			var property = $("input[name=\'property\']:checked").val();
			if(!property || property == "") {
				alert("Please select a property to update");
				return false;
			}

			var actionMsg = "Are you sure you want to ";
			
			if(property == "status") {
				if($("#status_change").val() == ""){
					alert("Please select a status transition to perform");	
					return false;
				}
				actionMsg += "change status from "+$("#status_change").val();
			} else {
				if($("#quality_change_reason").val() == ""){
					alert("Please select a reject reason to change");	
					return false;
				}
				actionMsg += "reject item with reason - "+$("#quality_change_reason option:selected").text();
			}

			var itembarcodes = $("#itembarcodes").val();
			itembarcodes = itembarcodes.split(\'\\n\');
			var validBarcodes = "";
			var validItemsCount = 0;
			for(var i in itembarcodes) {
				var itembarcode = itembarcodes[i].trim();	
				if(itembarcode.replace(/[\\d]/g, \'\') != "") {
					alert("Invalid separators used for item barcodes");
					return false;
				} else {
					if(itembarcode.trim() != "") {
						validBarcodes += validBarcodes==""?"":",";
						validBarcodes += itembarcode.trim();
						validItemsCount++;
						if(validItemsCount > 50) {
						  alert("Only 50 items can be scanned in one operation");
						  return false;
						}
					}
				}
			}
			
			if(validBarcodes == "") {
				alert("Please enter item barcodes to update");
				return false;
			}
			$("#item_barcodes_list").val(validBarcodes);

			if(confirm(actionMsg)) {
				return true;
			}
			return false;
		});
	});
</script>
'; ?>
	

<?php ob_start(); ?>
	<?php if ($this->_tpl_vars['errorMsg'] && $this->_tpl_vars['errorMsg'] != ''): ?>
		<div style="color: red; font-size: 12px; font-weight: bold; text-align:center;"><?php echo $this->_tpl_vars['errorMsg']; ?>
</div>
	<?php endif; ?>
	<?php if ($this->_tpl_vars['successMsg'] && $this->_tpl_vars['successMsg'] != ''): ?>
		<div style="font-size: 12px; font-weight: bold; text-align:center;"><?php echo $this->_tpl_vars['successMsg']; ?>
</div>
	<?php endif; ?>
	<form action="updatecoreitemsbulk.php" id="update-items-form" method="POST">
		<input type="hidden" name="action" value="updateitems">
		<input type="hidden" name="user" value="<?php echo $this->_tpl_vars['login']; ?>
">
		<input type="hidden" name="item_barcodes_list" id="item_barcodes_list" value="">
		<table border=0 cellspacing=0 cellpadding=0 width=100%>
			<tr>
				<td width=300 valign=top>
					Please Scan the item barcodes below (newline separated):
				</td>
				<td width=520>
					<textarea id="itembarcodes" name="itembarcodes" style="border: 1px solid #cdcdcd; padding: 2px; width: 500px; height: 300px;"></textarea>
				</td>
			</tr>	
			<tr>
				<td width=300 valign=top>
					Select property to be update:
				</td>
				<td>
					<input type="radio" name="property" value="status"> Item Status
					<input type="radio" name="property" value="quality"> Quality Level
				</td>
			</tr>	
			<tr id="status_change_row" style="display:none;">
				<td width=300 valign=top>
					Select Status transition:
				</td>
				<td>
					<select name="status_change" id="status_change">
						<option value="">--Select Transition--</option>
						<option value="NOT_FOUND-SHRINKAGE">NOT FOUND TO SHRINKAGE</option>
						<option value="FOUND-STORED">FOUND TO STORED</option>
						<option value="STORED-RETURNED">STORED TO RETURNED</option>
						<option value="STORED-PROCESSING">STORED TO PROCESSING</option>
					</select>
				</td>
			</tr>	
			
			<tr id="quality_change_row" style="display:none;">
				<td width=300 valign=top>
					Select Quality Change Reason:
				</td>
				<td>
					<select name="quality_change_reason" id="quality_change_reason">
                    	<option value="">-- Select Reject Reason --</option>
                        <?php $_from = $this->_tpl_vars['quality_check_codes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['qcReason']):
?>
                        	<option value="<?php echo $this->_tpl_vars['qcReason']['id']; ?>
"><?php echo $this->_tpl_vars['qcReason']['quality']; ?>
 - <?php echo $this->_tpl_vars['qcReason']['rejectReason']; ?>
 - <?php echo $this->_tpl_vars['qcReason']['rejectReasonDescription']; ?>
</option>
                        <?php endforeach; endif; unset($_from); ?>
                    </select>
				</td>
			</tr>	
			<tr height=5><td colspan=2>&nbsp;</td></tr>
			<tr><td colspan=2 align=right><input type=submit name="update-item-btn" value="Update"></td></tr>
		</table>
	</form>
<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean();  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'Bulk Items Editing Brahmastra','content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>