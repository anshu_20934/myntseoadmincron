<?php /* Smarty version 2.6.12, created on 2017-05-19 10:57:54
         compiled from admin/pageconfig/pageconfig-imagedialog.tpl */ ?>
<div id="addBannerDiv-<?php echo $this->_tpl_vars['i']; ?>
" class="lightbox">
	<div class="mod" style="width: 480px">
		<div class="hd"><H2>Add <?php if ($this->_tpl_vars['rule']['parent_rule_id'] > 0):  $this->assign('rule_id', $this->_tpl_vars['rule']['parent_rule_id']);  echo $this->_tpl_vars['rules'][$this->_tpl_vars['rule_id']]['section']; ?>
 >> <?php endif;  echo $this->_tpl_vars['rule']['section']; ?>
 image(s) for <?php echo $this->_tpl_vars['rule']['location_type']; ?>
 - <?php echo $this->_tpl_vars['pageLocation']; ?>
 </H2></div>
		<form enctype="multipart/form-data" action="<?php echo $this->_tpl_vars['postBackURL']; ?>
" method="POST" name="add">
			<input type="hidden" name="locationType" value="<?php echo $this->_tpl_vars['rule']['location_type']; ?>
"/>
			<input type="hidden" name="pageLocation" value="<?php echo $this->_tpl_vars['pageLocation']; ?>
"/>
			<input type="hidden" name="section" value="<?php echo $this->_tpl_vars['rule']['section']; ?>
"/>
			<input type="hidden" name="actionType" value="add"/>
			<table style="width: 100%">
				<tr>
					<td colspan="2"><hr/></td>
				</tr>
				<?php unset($this->_sections['dialogfields']);
$this->_sections['dialogfields']['name'] = 'dialogfields';
$this->_sections['dialogfields']['loop'] = is_array($_loop=$this->_tpl_vars['rule']['step']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['dialogfields']['show'] = true;
$this->_sections['dialogfields']['max'] = $this->_sections['dialogfields']['loop'];
$this->_sections['dialogfields']['step'] = 1;
$this->_sections['dialogfields']['start'] = $this->_sections['dialogfields']['step'] > 0 ? 0 : $this->_sections['dialogfields']['loop']-1;
if ($this->_sections['dialogfields']['show']) {
    $this->_sections['dialogfields']['total'] = $this->_sections['dialogfields']['loop'];
    if ($this->_sections['dialogfields']['total'] == 0)
        $this->_sections['dialogfields']['show'] = false;
} else
    $this->_sections['dialogfields']['total'] = 0;
if ($this->_sections['dialogfields']['show']):

            for ($this->_sections['dialogfields']['index'] = $this->_sections['dialogfields']['start'], $this->_sections['dialogfields']['iteration'] = 1;
                 $this->_sections['dialogfields']['iteration'] <= $this->_sections['dialogfields']['total'];
                 $this->_sections['dialogfields']['index'] += $this->_sections['dialogfields']['step'], $this->_sections['dialogfields']['iteration']++):
$this->_sections['dialogfields']['rownum'] = $this->_sections['dialogfields']['iteration'];
$this->_sections['dialogfields']['index_prev'] = $this->_sections['dialogfields']['index'] - $this->_sections['dialogfields']['step'];
$this->_sections['dialogfields']['index_next'] = $this->_sections['dialogfields']['index'] + $this->_sections['dialogfields']['step'];
$this->_sections['dialogfields']['first']      = ($this->_sections['dialogfields']['iteration'] == 1);
$this->_sections['dialogfields']['last']       = ($this->_sections['dialogfields']['iteration'] == $this->_sections['dialogfields']['total']);
?>
					<?php $this->assign('count', $this->_sections['dialogfields']['index']); ?>
				<tr>
					<td colspan="2"><?php echo $this->_tpl_vars['count']+1; ?>
. Enter Image Details</td>
				</tr>
				<?php if ($this->_tpl_vars['rule']['parent_images']): ?>
				<tr>
					<td>Select the <?php echo $this->_tpl_vars['rule']['location_type']; ?>
>><?php $this->assign('rule_id', $this->_tpl_vars['rule']['parent_rule_id']);  echo $this->_tpl_vars['rules'][$this->_tpl_vars['rule_id']]['section']; ?>
 Image </td>
					<td>
						<select name="image[<?php echo $this->_tpl_vars['count']; ?>
][parent_image_id]">
							<?php $_from = $this->_tpl_vars['rule']['parent_images']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['parent_images'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['parent_images']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['parent_image_id'] => $this->_tpl_vars['parent_image_url']):
        $this->_foreach['parent_images']['iteration']++;
?>
								<option value="<?php echo $this->_tpl_vars['parent_image_id']; ?>
"><?php echo $this->_tpl_vars['parent_image_id']; ?>
</option>
							<?php endforeach; endif; unset($_from); ?>
						</select>
					</td>
				</tr>
				<?php endif; ?>
				<tr>
					<td>Image File to upload</td>
					<td><input name="image[<?php echo $this->_tpl_vars['count']; ?>
][image_file]" type="file" id="image_<?php echo $this->_tpl_vars['count']; ?>
"/></td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: center">OR</td>
				</tr>
				<tr>
					<td>Image Source URL (CDN)</td>
					<td><input type="text" id="image_url_<?php echo $this->_tpl_vars['count']; ?>
" name="image[<?php echo $this->_tpl_vars['count']; ?>
][image_url]" size="40"/></td>
				</tr>
				<tr>
					<td>Alt Text</td>
					<td><input type="text" name="image[<?php echo $this->_tpl_vars['count']; ?>
][alt_text]" size="40"/></td>
				</tr>
				<tr>
					<td>Target URL(should start with /)</td>
					<td><input type="text" name="image[<?php echo $this->_tpl_vars['count']; ?>
][target_url]" size="40"/></td>
				</tr>
				<tr>
					<td>Title</td>
					<td><input type="text" name="image[<?php echo $this->_tpl_vars['count']; ?>
][title]" size="40"/></td>
				</tr>
				<tr>
					<td>Description</td>
					<td><textarea name="image[<?php echo $this->_tpl_vars['count']; ?>
][description]" rows="5" cols="38" style="border: 1px solid lightgray"> </textarea></td>
				</tr>
				<tr>
					<td>Fashion Banners (Only for Search Pages)</td>
					<td><textarea name="image[<?php echo $this->_tpl_vars['count']; ?>
][fashion_banners]" rows="5" cols="38" style="border: 1px solid lightgray"> </textarea></td>
				</tr>
				<tr>
					<td colspan="2"><hr/></td>
				</tr>	
				<?php endfor; endif; ?>
				<tr>
					<td colspan="2" align="center">
						<input type="submit" value="Add" style="font-size: larger"/>
					</td>
				</tr>				
			</table>
		</form>
	</div>
</div>
<div class="divider">&nbsp;</div>