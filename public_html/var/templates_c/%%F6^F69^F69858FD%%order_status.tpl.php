<?php /* Smarty version 2.6.12, created on 2017-03-24 12:22:30
         compiled from main/order_status.tpl */ ?>
<?php func_load_lang($this, "main/order_status.tpl","lbl_wrong_status,lbl_queued,lbl_wip,lbl_declined,lbl_failed,lbl_complete,lbl_preprocessed,lbl_cod,lbl_onhold,lbl_delivery_failed,lbl_order_shipped,lbl_not_finished,lbl_queued,lbl_processed,lbl_declined,lbl_onhold,lbl_backordered,lbl_failed,lbl_complete,lbl_pending,lbl_preprocessed,lbl_cod,lbl_delivery_failed,lbl_wip,lbl_cod,lbl_order_packed,lbl_order_shipped,lbl_onhold,lbl_failed"); ?><?php if ($this->_tpl_vars['extended'] == "" && $this->_tpl_vars['status'] == ""): ?>
    <?php echo $this->_tpl_vars['lng']['lbl_wrong_status']; ?>

<?php elseif ($this->_tpl_vars['mode'] == 'select'): ?>
	<select name="<?php echo $this->_tpl_vars['name']; ?>
" id="<?php echo $this->_tpl_vars['name']; ?>
" <?php echo $this->_tpl_vars['extra']; ?>
 <?php if ($this->_tpl_vars['action'] == 1): ?>onchange="javascript:showOrHideCommentBox(this.value);"<?php endif; ?> disabled="disabled">
		<?php if ($this->_tpl_vars['extended'] != ""): ?><option value="">--Select order status--</option><?php endif; ?>
		<option value="Q"<?php if ($this->_tpl_vars['status'] == 'Q'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_queued']; ?>
</option>
		<option value="WP"<?php if ($this->_tpl_vars['status'] == 'WP'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_wip']; ?>
</option>
		<option value="D"<?php if ($this->_tpl_vars['status'] == 'D'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_declined']; ?>
</option>
		<option value="F"<?php if ($this->_tpl_vars['status'] == 'F'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_failed']; ?>
</option>
		<option value="C"<?php if ($this->_tpl_vars['status'] == 'C'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_complete']; ?>
</option>
		<option value="PP"<?php if ($this->_tpl_vars['status'] == 'PP'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_preprocessed']; ?>
</option>
		<option value="PV"<?php if ($this->_tpl_vars['status'] == 'PV'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_cod']; ?>
</option>
		<option value="OH"<?php if ($this->_tpl_vars['status'] == 'OH'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_onhold']; ?>
</option>
		<option value="DL"<?php if ($this->_tpl_vars['status'] == 'DL'): ?> selected="selected"<?php endif; ?>>Delivered</option>
		<option value="FD"<?php if ($this->_tpl_vars['status'] == 'FD'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_delivery_failed']; ?>
</option>
		<option value="PK"<?php if ($this->_tpl_vars['status'] == 'PK'): ?> selected="selected"<?php endif; ?>>Packed</option>
		<option value="SH"<?php if ($this->_tpl_vars['status'] == 'SH'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_order_shipped']; ?>
</option>
		<option value="L"<?php if ($this->_tpl_vars['status'] == 'L'): ?> selected="selected"<?php endif; ?>>Lost</option>
		<option value="RFR"<?php if ($this->_tpl_vars['status'] == 'RFR'): ?> selected="selected"<?php endif; ?>>Ready For Release</option>
        <option value="RTO"<?php if ($this->_tpl_vars['status'] == 'RTO'): ?> selected="selected"<?php endif; ?>>RTO</option>
	</select>
<?php elseif ($this->_tpl_vars['mode'] == 'static'): ?>
	<?php if ($this->_tpl_vars['status'] == 'I'):  echo $this->_tpl_vars['lng']['lbl_not_finished']; ?>

	<?php elseif ($this->_tpl_vars['status'] == 'Q'):  echo $this->_tpl_vars['lng']['lbl_queued']; ?>

	<?php elseif ($this->_tpl_vars['status'] == 'P'):  echo $this->_tpl_vars['lng']['lbl_processed']; ?>

	<?php elseif ($this->_tpl_vars['status'] == 'D'):  echo $this->_tpl_vars['lng']['lbl_declined']; ?>

	<?php elseif ($this->_tpl_vars['status'] == 'OH'):  echo $this->_tpl_vars['lng']['lbl_onhold']; ?>

	<?php elseif ($this->_tpl_vars['status'] == 'B'):  echo $this->_tpl_vars['lng']['lbl_backordered']; ?>

	<?php elseif ($this->_tpl_vars['status'] == 'F'):  echo $this->_tpl_vars['lng']['lbl_failed']; ?>

	<?php elseif ($this->_tpl_vars['status'] == 'C'):  echo $this->_tpl_vars['lng']['lbl_complete']; ?>

	<?php elseif ($this->_tpl_vars['status'] == 'PD'):  echo $this->_tpl_vars['lng']['lbl_pending']; ?>

	<?php elseif ($this->_tpl_vars['status'] == 'PP'):  echo $this->_tpl_vars['lng']['lbl_preprocessed']; ?>

	<?php elseif ($this->_tpl_vars['status'] == 'CD'):  echo $this->_tpl_vars['lng']['lbl_cod']; ?>

	<?php elseif ($this->_tpl_vars['status'] == 'FD'):  echo $this->_tpl_vars['lng']['lbl_delivery_failed']; ?>

	<?php elseif ($this->_tpl_vars['status'] == 'WP'):  echo $this->_tpl_vars['lng']['lbl_wip']; ?>

	<?php elseif ($this->_tpl_vars['status'] == 'PV'):  echo $this->_tpl_vars['lng']['lbl_cod']; ?>

	<?php elseif ($this->_tpl_vars['status'] == 'PK'):  echo $this->_tpl_vars['lng']['lbl_order_packed']; ?>

	<?php elseif ($this->_tpl_vars['status'] == 'SH'):  echo $this->_tpl_vars['lng']['lbl_order_shipped']; ?>

	<?php elseif ($this->_tpl_vars['status'] == 'L'): ?>Lost
	<?php elseif ($this->_tpl_vars['status'] == 'RFR'): ?>Ready For Release
	<?php elseif ($this->_tpl_vars['status'] == 'DL'): ?>Delivered
    <?php elseif ($this->_tpl_vars['status'] == 'RTO'): ?>RTO
    <?php endif; ?>
<?php elseif ($this->_tpl_vars['mode'] == "oos-select"): ?>
    <select name="<?php echo $this->_tpl_vars['name']; ?>
" id="<?php echo $this->_tpl_vars['name']; ?>
" <?php echo $this->_tpl_vars['extra']; ?>
 <?php if ($this->_tpl_vars['action'] == 1): ?>onchange="javascript:showOrHideCommentBox(this.value);"<?php endif; ?> disabled>
        <option value="OH"<?php if ($this->_tpl_vars['status'] == 'OH'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_onhold']; ?>
</option>
        <option value="F"<?php if ($this->_tpl_vars['status'] == 'F'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_failed']; ?>
</option>
    </select>
<?php endif; ?>