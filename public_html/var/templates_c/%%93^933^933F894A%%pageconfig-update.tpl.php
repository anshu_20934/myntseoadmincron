<?php /* Smarty version 2.6.12, created on 2017-05-19 10:57:54
         compiled from admin/pageconfig/pageconfig-update.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strpos', 'admin/pageconfig/pageconfig-update.tpl', 52, false),)), $this); ?>
<?php if ($this->_tpl_vars['rules']): ?>
	<?php $this->assign('i', -1); ?>
	<?php $_from = $this->_tpl_vars['rules']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['rules'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['rules']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['index'] => $this->_tpl_vars['rule']):
        $this->_foreach['rules']['iteration']++;
?>
	<div class="DIV_TABLE">
	<?php $this->assign('i', $this->_tpl_vars['i']+1); ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/pageconfig/pageconfig-imagerules.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/pageconfig/pageconfig-imagedialog.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/pageconfig/pageconfig-imagedialog-variant.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<?php if ($this->_tpl_vars['imagesSets']): ?>
	<?php $this->assign('images', $this->_tpl_vars['imagesSets'][$this->_tpl_vars['i']]); ?>
		<?php if ($this->_tpl_vars['images']): ?>
		<?php if (! $this->_tpl_vars['variantPage']): ?>
		<form enctype="multipart/form-data" action="/admin/pageconfig/editVariant.php" method="post" name='variant_update_<?php echo $this->_tpl_vars['i']; ?>
' class='variantUpdateForm'>
			<input type="hidden" name="locationType" value="<?php echo $this->_tpl_vars['rule']['location_type']; ?>
"/>
			<input type="hidden" name="section" value="<?php echo $this->_tpl_vars['rule']['section']; ?>
"/>
			<input type="hidden" name="pageLocation" value="<?php echo $this->_tpl_vars['pageLocation']; ?>
"/>
			<input type="hidden" name="imageRuleId" value="<?php echo $this->_tpl_vars['rule']['image_rule_id']; ?>
" />			
			<input type="hidden" class='parentImageId' name="parentImageId" value=""/>			
			<input type="hidden" class='variantFlag' name="variantFlag" value=""/>			
		</form>
		<?php endif; ?> 
		<form enctype="multipart/form-data" action="<?php if (! $this->_tpl_vars['variantPage']):  echo $this->_tpl_vars['postBackURL'];  else: ?>/admin/pageconfig/editVariant.php<?php endif; ?>" method="post" name="update_<?php echo $this->_tpl_vars['i']; ?>
">
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/pageconfig/pageconfig-button-panel.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			<input type="hidden" name="locationType" value="<?php echo $this->_tpl_vars['rule']['location_type']; ?>
"/>
			<input type="hidden" name="section" value="<?php echo $this->_tpl_vars['rule']['section']; ?>
"/>
			<input type="hidden" name="step" value="<?php if (! $this->_tpl_vars['variantPage']):  echo $this->_tpl_vars['rule']['step'];  else: ?>1<?php endif; ?>"/>
			<input type="hidden" name="pageLocation" value="<?php echo $this->_tpl_vars['pageLocation']; ?>
"/>
			<input type="hidden" name="pageLocationId" value="<?php echo $this->_tpl_vars['pageLocationId']; ?>
"/>
			<input type="hidden" name="actionType" value="<?php echo $this->_tpl_vars['updateAction']; ?>
"/>
			<input type="hidden" name="imageId" />
			<?php if ($this->_tpl_vars['variantPage']): ?>
			<input type='hidden' name='variantFlag' value='<?php echo $this->_tpl_vars['sVariantFlag']; ?>
' />
			<input type='hidden' name='parentImageId' value='<?php echo $this->_tpl_vars['sParentImageId']; ?>
' />
			<input type='hidden' name='imageRuleId' value='<?php echo $this->_tpl_vars['sImageRuleId']; ?>
' />
			<?php endif; ?>
				<div style="clear:both;"></div>
				<?php $_from = $this->_tpl_vars['images']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['banners'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['banners']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['image']):
        $this->_foreach['banners']['iteration']++;
?>
					<div class="DIV_TR bannerItemWrapper" style="float:none; height:100%">
						<div class="DIV_TR_TD" style="width:30px; height:100%">
							<?php echo $this->_foreach['banners']['iteration']; ?>

							<input type="hidden" name="image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][image_id]" value="<?php echo $this->_tpl_vars['image']['image_id']; ?>
"/>
							<input type="hidden" name="image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][image_rule_id]" value="<?php echo $this->_tpl_vars['image']['image_rule_id']; ?>
"/>
							<?php if ($this->_tpl_vars['image']['parent_image_id'] > 0): ?>
							<input type="hidden" name="image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][parent_image_id]" value="<?php echo $this->_tpl_vars['image']['parent_image_id']; ?>
"/>
							<?php endif; ?>
						</div>
						<?php if ($this->_tpl_vars['image']['parent_image_id'] > 0 && ! $this->_tpl_vars['variantPage']): ?>
						<?php $this->assign('rule_id', $this->_tpl_vars['rule']['parent_rule_id']); ?>
						<div class="DIV_TR_TD" style="position:relative;">
						<div style="position: absolute; z-index: 1;">
                            <img class="banner parent_image" 
                                src="<?php if (((is_array($_tmp=$this->_tpl_vars['image']['parent_image_url'])) ? $this->_run_mod_handler('strpos', true, $_tmp, 'http') : strpos($_tmp, 'http')) !== false):  echo $this->_tpl_vars['image']['parent_image_url'];  else:  echo $this->_tpl_vars['http_location']; ?>
/<?php echo $this->_tpl_vars['image']['parent_image_url'];  endif; ?>" 
                            	data-height="<?php echo $this->_tpl_vars['rules'][$this->_tpl_vars['rule_id']]['height']; ?>
"
                                data-width="<?php echo $this->_tpl_vars['rules'][$this->_tpl_vars['rule_id']]['width']; ?>
"/>
                        </div>
						<?php endif; ?>
						<div <?php if ($this->_tpl_vars['image']['parent_image_id'] > 0 && ! $this->_tpl_vars['variantPage']): ?>style="position: absolute; z-index: 2;"<?php else: ?>class="DIV_TR_TD resizeWrapper"<?php endif; ?>>
							<img class="banner <?php if ($this->_tpl_vars['image']['parent_image_id'] > 0): ?>child_image<?php endif; ?>" 
								src="<?php if (((is_array($_tmp=$this->_tpl_vars['image']['image_url'])) ? $this->_run_mod_handler('strpos', true, $_tmp, 'http') : strpos($_tmp, 'http')) !== false):  echo $this->_tpl_vars['image']['image_url'];  else:  echo $this->_tpl_vars['http_location']; ?>
/<?php echo $this->_tpl_vars['image']['image_url'];  endif; ?>" 
								alt="<?php echo $this->_tpl_vars['image']['alt_text']; ?>
" 
								title="<?php echo $this->_tpl_vars['image']['title']; ?>
" 
								data-height="<?php echo $this->_tpl_vars['image']['height']; ?>
"
								data-width="<?php echo $this->_tpl_vars['image']['width']; ?>
"/>
							<?php if (! $this->_tpl_vars['variantPage']): ?>
							<p>
								<a href='javascript:void(0);' class='addVariant' data-lightbox-id='addBannerDivVariant-<?php echo $this->_tpl_vars['i']; ?>
' title='Add Variant' data-parent-id='<?php echo $this->_tpl_vars['image']['image_id']; ?>
' data-lightbox-index='<?php echo $this->_tpl_vars['i']; ?>
'>Add Variant</a>
								<a href='#' class='editVariant' title='Edit Variants' data-parent-id='<?php echo $this->_tpl_vars['image']['image_id']; ?>
'>Edit Variants</a>
							</p>
							<div class='variantThumbnail' id='variantThumbnailId<?php echo $this->_tpl_vars['image']['image_id']; ?>
'>
								<h1>Variants</h1>
								<?php $_from = $this->_tpl_vars['variantImages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['variantItem']):
?>
									<?php if ($this->_tpl_vars['image']['image_id'] == $this->_tpl_vars['variantItem']['parent_image_id']): ?>
										<img src='<?php echo $this->_tpl_vars['variantItem']['image_url']; ?>
' alt='Variant Image' title='Group: <?php echo $this->_tpl_vars['variantItem']['group_id']; ?>
' />
									<?php endif; ?>
								<?php endforeach; endif; unset($_from); ?>
							</div>
							<?php endif; ?>
						</div>
						<?php if ($this->_tpl_vars['image']['parent_image_id'] > 0 && ! $this->_tpl_vars['variantPage']): ?>
						</div>
						<?php endif; ?>
						<div class="DIV_TR_TD bannerItem" style="width:350px; height:100%">
							<div class="banner-details" style="padding-top: 2px">
								<span class="lblleft">Browse File to change Image:</span>
								<input name="image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][image_file]" type="file"/>
							</div>	
							<div class="banner-details">
								<div class="lblleft">Source URL</div>
								<input type="text" name="image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][image_url]" value="<?php echo $this->_tpl_vars['image']['image_url']; ?>
" size="40"/>
							</div>
							<?php if ($this->_tpl_vars['variantPage']): ?>
							<div class='banner-details'>
								<div class="lblleft">User Group</div>
								<select name='image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][group_id]'>
									<option selected="selected" value='-1'>Select Group</option>
									<?php $_from = $this->_tpl_vars['userGroup']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['userGroupItem']):
?>
										<option value='<?php echo $this->_tpl_vars['userGroupItem']['id']; ?>
' <?php if ($this->_tpl_vars['image']['group_id'] == $this->_tpl_vars['userGroupItem']['id']): ?>selected='selected'<?php endif; ?>><?php echo $this->_tpl_vars['userGroupItem']['title']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select> 
							</div>
							<div class='banner-details'>
								<div class="lblleft">Location Group</div>
								<select name='image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][location_group_id]'>
									<option selected="selected" value='-1'>Select Group</option>
									<?php $_from = $this->_tpl_vars['locationGroup']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['locationGroupItem']):
?>
										<option value='<?php echo $this->_tpl_vars['locationGroupItem']['id']; ?>
' <?php if ($this->_tpl_vars['image']['location_group_id'] == $this->_tpl_vars['locationGroupItem']['id']): ?>selected='selected'<?php endif; ?>><?php echo $this->_tpl_vars['locationGroupItem']['title']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select> 
							</div>
							<?php if ($this->_tpl_vars['rule']['section'] == 'slideshow'): ?>
							<div class='banner-details'>
								<div class='lblleft'>Variant Location</div>
								<select name="image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][variant_location]">
									<option value='replace' <?php if ($this->_tpl_vars['image']['variant_location'] == 'replace'): ?>selected='selected'<?php endif; ?>>Replace Image</option>
									<option value='first' <?php if ($this->_tpl_vars['image']['variant_location'] == 'first'): ?>selected='selected'<?php endif; ?>>Add As First Image</option>			
								</select>
							</div>
							<?php endif; ?>
							<?php endif; ?>							
							<div class="multi-check"><input class="cb-multi" name="image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][multiFlag]" type="checkbox" value="<?php if ($this->_tpl_vars['image']['multiclick'] == 1): ?>1<?php else: ?>0<?php endif; ?>" <?php if ($this->_tpl_vars['image']['multiclick'] == 1): ?>checked<?php endif; ?>/><label>MultiClick Banner:</label></div>
							<div class="multi-config clear <?php if ($this->_tpl_vars['image']['multiclick'] == 1): ?>show<?php endif; ?>">
								<a href="javascript:void(0);" class="addZone">Add Another Zone</a>				
								<ul class="zone-map" data-image-id="<?php echo $this->_tpl_vars['image']['image_id']; ?>
">
									<?php $_from = $this->_tpl_vars['multiBannerData'][$this->_tpl_vars['image']['image_id']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['imageMap']):
?>
										<?php if ($this->_tpl_vars['imageMap']['image_id']): ?>
										<li data-zone-index="<?php echo $this->_tpl_vars['k']; ?>
">
											<h3>Zone <?php echo $this->_tpl_vars['k']+1; ?>
</h3>
											<div class="coord"><label>X1:<sup>*</sup> </label><input class="coordinateValue" name="image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][multi][<?php echo $this->_tpl_vars['k']; ?>
][0]}"  type="text" value="<?php echo $this->_tpl_vars['imageMap']['coordinates'][0]; ?>
" /></div>
											<div class="coord"><label>Y1:<sup>*</sup> </label><input class="coordinateValue" name="image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][multi][<?php echo $this->_tpl_vars['k']; ?>
][1]}" type="text" value="<?php echo $this->_tpl_vars['imageMap']['coordinates'][1]; ?>
" /></div>
											<div class="coord"><label>X2:<sup>*</sup> </label><input class="coordinateValue" name="image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][multi][<?php echo $this->_tpl_vars['k']; ?>
][2]}" type="text" value="<?php echo $this->_tpl_vars['imageMap']['coordinates'][2]; ?>
" /></div>
											<div class="coord"><label>Y2:<sup>*</sup> </label><input class="coordinateValue" name="image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][multi][<?php echo $this->_tpl_vars['k']; ?>
][3]}" type="text" value="<?php echo $this->_tpl_vars['imageMap']['coordinates'][3]; ?>
" /></div>
											<div class="url"><label>URL:<sup>*</sup> </label><input class="urlValue" name="image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][multi][<?php echo $this->_tpl_vars['k']; ?>
][target]}"type="text" value="<?php echo $this->_tpl_vars['imageMap']['target_url']; ?>
" /></div>
											<div class="title"><label>Title: </label><input type="text" class="titleValue" name="image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][multi][<?php echo $this->_tpl_vars['k']; ?>
][title]}" value="<?php echo $this->_tpl_vars['imageMap']['title']; ?>
" /></div>
											<div class="addRemoveZone">											
												<a href="javascript:void(0);" class="removeZone">Remove Zone</a>				
											</div> 
										</li>
										<?php endif; ?>
									<?php endforeach; endif; unset($_from); ?>
									</ul>
									<a href="javascript:void(0);" class="addZone">Add Another Zone</a>
							</div>
							<div class="banner-details singleTarget">
								<div class="lblleft">Target URL </div>
								<input type="text" name="image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][target_url]" value="<?php echo $this->_tpl_vars['image']['target_url']; ?>
" size="40"/>
							</div>
							<div class="banner-details">
								<div class="lblleft">Alt Text</div>
								<input type="text" name="image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][alt_text]" value="<?php echo $this->_tpl_vars['image']['alt_text']; ?>
" size="40"/>
							</div>							
							<div class="banner-details">
								<div class="lblleft">Title</div>
								<input type="text" name="image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][title]" value="<?php echo $this->_tpl_vars['image']['title']; ?>
" size="40"/>
							</div> 
							<div class="banner-details">
								<div class="lblleft">Description</div>
								<textarea name="image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][description]" cols="38" rows="4"><?php echo $this->_tpl_vars['image']['description']; ?>
</textarea>
								
							</div>
							<div class="banner-details">
								<div class="lblleft">Fashion Banners (Only for Search Pages)</div>
								<textarea name="image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][fashion_banners]" cols="38" rows="4"><?php echo $this->_tpl_vars['image']['fashion_banners']; ?>
</textarea>
							</div>
							<?php if ($this->_tpl_vars['image']['parent_image_id'] > 0 && ! $this->_tpl_vars['variantPage']): ?>
								<div class="banner-details"><input class="cb-delete" name="image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][delete]" type="checkbox" value="<?php echo $this->_tpl_vars['image']['image_id']; ?>
" text="Delete"/> <label>Delete:</label></div>
							<?php endif; ?>
						</div>
						<div class="DIV_TR_TD" style="width:100px; height:100%;">
							<div style="padding:10px">
								<div class="banner-action"><input type="submit" value="Up" onclick="javascript: document.update_<?php echo $this->_tpl_vars['i']; ?>
.actionType.value = '<?php echo $this->_tpl_vars['moveUpAction']; ?>
';  document.update_<?php echo $this->_tpl_vars['i']; ?>
.imageId.value = '<?php echo $this->_tpl_vars['image']['image_id']; ?>
'; document.update_<?php echo $this->_tpl_vars['i']; ?>
.variantFlag.value = '<?php echo $this->_tpl_vars['variantPage']; ?>
'; document.update_<?php echo $this->_tpl_vars['i']; ?>
.submit();" /></div>
								<div class="banner-action"><input type="submit" value="Down" onclick="javascript: document.update_<?php echo $this->_tpl_vars['i']; ?>
.actionType.value = '<?php echo $this->_tpl_vars['moveDownAction']; ?>
';  document.update_<?php echo $this->_tpl_vars['i']; ?>
.imageId.value = '<?php echo $this->_tpl_vars['image']['image_id']; ?>
'; document.update_<?php echo $this->_tpl_vars['i']; ?>
.variantFlag.value = '<?php echo $this->_tpl_vars['variantPage']; ?>
'; document.update_<?php echo $this->_tpl_vars['i']; ?>
.submit();" /></div>
								<div class="banner-action"><label>Delete:</label><input class="cb-delete" name="image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][delete]" type="checkbox" value="<?php echo $this->_tpl_vars['image']['image_id']; ?>
"/></div>								
							</div>
						</div>
						<?php if ($this->_tpl_vars['variantPage']): ?>
							<div class="DIV_TR_TD" style="width:100px; height:100%;">
								<div style="padding:10px">
									<div class="banner-action"><label>Delete:</label><input class="cb-delete" name="image[<?php echo $this->_tpl_vars['image']['image_id']; ?>
][delete]" type="checkbox" value="<?php echo $this->_tpl_vars['image']['image_id']; ?>
"/></div>								
								</div>
							</div>
						<?php endif; ?>
					</div>
					<div style="clear:both;"></div>
				<?php endforeach; endif; unset($_from); ?>
				<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/pageconfig/pageconfig-button-panel.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		</form>
		
		<?php endif; ?>
	<?php endif; ?>
	</div>
	<div class="divider"></div>
	<?php endforeach; endif; unset($_from); ?>
<?php endif; ?>
<link rel="stylesheet" type="text/css"
    href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/base/jquery-ui.css"/>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['http_location']; ?>
/skin1/admin/multiclick/multiclick.js"></script>