<?php /* Smarty version 2.6.12, created on 2017-03-24 18:27:58
         compiled from admin/main/styleContentUpdate.tpl */ ?>
<script type="text/javascript">
	var httpLocation = '<?php echo $this->_tpl_vars['http_location']; ?>
';
	var style_ids = '<?php echo $this->_tpl_vars['style_ids']; ?>
';
	var styleIdData = <?php echo $this->_tpl_vars['styleId']; ?>
;
	var updateContentStyleId = '';
	var styleStatusData = <?php echo $this->_tpl_vars['styleStatus']; ?>
;
	var possibleStatusTrans = <?php echo $this->_tpl_vars['possibleStatusTrans']; ?>
;
	
</script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/extjs/resources/css/ext-all.css"/>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/extjs/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/extjs/ext-all.js"></script>
<script type="text/javascript" src="styleContentHtmlEditor.js"></script>
<script type="text/javascript" src="StylePickerDialog.js"></script>
<script type="text/javascript" src="styleSearchCopy.js"></script>
<script type="text/javascript" src="styleContentUpdate.js"></script>
<div id="formPanel"></div>
<div id="contentPanel"></div>