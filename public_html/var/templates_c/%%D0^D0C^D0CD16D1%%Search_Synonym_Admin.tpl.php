<?php /* Smarty version 2.6.12, created on 2017-03-30 01:20:41
         compiled from admin/Search_Synonym_Admin.tpl */ ?>
<?php func_load_lang($this, "admin/Search_Synonym_Admin.tpl","txt_featured_products"); ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "main/popup_product.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/widgets/header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php ob_start(); ?>
<div class="notification-message"><?php echo $this->_tpl_vars['message']; ?>
</div>
<?php echo '
  <script language="Javascript">
	function addRule()
	{
		document.frmG.submit();
	}
  </script>
'; ?>

<a name="featured" />

<?php echo $this->_tpl_vars['lng']['txt_featured_products']; ?>




<?php if ($this->_tpl_vars['looktype'] == 'Search'): ?>
<form action="Search_Synonym_Admin.php" method="post" name="mapping-add-form" class="mapping-add-form">
<input type="hidden" name="mode" class="mode" value="AddMode"/>
<input type="submit" value="Go to Add Mode">
</form>
<table cellpadding="3" cellspacing="1" width="100%">
	<tr>
        <td><h2>Search Field</h2></td>
        <td>&nbsp;</td>
   </tr>
   <form action="Search_Synonym_Admin.php" method="post" name="mapping-add-form" class="mapping-add-form">
	<input type="hidden" name="mode" class="mode" value="StringSearch"/>
	<input type="hidden" name="offset" class="mode" value="<?php echo $this->_tpl_vars['offset']; ?>
"/>	
	<input type="text" name="searchLanding" class="mode" value="<?php echo $this->_tpl_vars['searchLanding']; ?>
"/>
	<input type="submit" value="Search String">
   <tr>
   </tr>
   <tr>
   	<td colspan="2">
   	<?php if ($this->_tpl_vars['offset'] > 0): ?>
   	<form action="Search_Synonym_Admin.php" method="post" name="mapping-add-form" class="mapping-add-form">
	<input type="hidden" name="mode" class="mode" value="Previous"/>
	<input type="hidden" name="searchLanding" class="mode" value="<?php echo $this->_tpl_vars['searchLanding']; ?>
"/>
	<input type="hidden" name="offset" class="mode" value="<?php echo $this->_tpl_vars['offset']; ?>
"/>
	<input type="submit" value="Previous">
	<?php endif; ?>
</form>
   	
   	</td>
   	<td colspan="2">
   	<?php if ($this->_tpl_vars['offset'] < $this->_tpl_vars['maxoffset']): ?>
   	<form action="Search_Synonym_Admin.php" method="post" name="mapping-add-form" class="mapping-add-form">
	<input type="hidden" name="mode" class="mode" value="Next"/>
	<input type="hidden" name="searchLanding" class="mode" value="<?php echo $this->_tpl_vars['searchLanding']; ?>
"/>
	<input type="hidden" name="offset" class="mode" value="<?php echo $this->_tpl_vars['offset']; ?>
"/>
   	<P align="right">
	<input type="submit" value="Next">
   	</p>
	<?php endif; ?>
   	</form>
   	</td>
   </tr>
   <tr>
   <td><b>Original</b></td>
   <td><b>Corrected</b></td>
   <td></td>
   <td></td>
   </tr>
   <break>
   <tr>
   	<?php $_from = $this->_tpl_vars['searchresult']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['dataloop'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['dataloop']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['row']):
        $this->_foreach['dataloop']['iteration']++;
?>
   	<form action="Search_Synonym_Admin.php" method="post" name="mapping-add-form" class="mapping-add-form">
	<tr class="TableSubHead">
	<td><input type="text" name="output[<?php echo $this->_tpl_vars['row']['id']; ?>
][original]" value="<?php echo $this->_tpl_vars['row']['original']; ?>
"  width = "80"></td>
	<td><input type="text" name="output[<?php echo $this->_tpl_vars['row']['id']; ?>
][corrected]" value="<?php echo $this->_tpl_vars['row']['corrected']; ?>
"  width = "80"></td>
	<td>
		<form name="frmG" action="<?php echo $this->_tpl_vars['form_action']; ?>
" method="post" enctype="multipart/form-data" >
		<input type="hidden" name="mode" class="mode" value="update"/>
		<input type="hidden" name="searchLanding" class="mode" value="<?php echo $this->_tpl_vars['searchLanding']; ?>
"/>
		<input type="hidden" name="offset" class="mode" value="<?php echo $this->_tpl_vars['offset']; ?>
"/>
		<input type="hidden" name="id" class="mode" value="<?php echo $this->_tpl_vars['row']['id']; ?>
" />
	   	<P align="left">
		<input type="submit" value="Update">
		</P>
		</form>
	</td>
	<td>
		<form action="Search_Synonym_Admin.php" method="post" name="mapping-add-form" class="mapping-add-form">
		<input type="hidden" name="mode" class="mode" value="delete"/>
		<input type="hidden" name="searchLanding" class="mode" value="<?php echo $this->_tpl_vars['searchLanding']; ?>
"/>
		<input type="hidden" name="offset" class="mode" value="<?php echo $this->_tpl_vars['offset']; ?>
"/>
		<input type="hidden" name="id" class="mode" value="<?php echo $this->_tpl_vars['row']['id']; ?>
"/>
	   	<P align="right">
		<input type="submit" value="Delete">
		</P>
		</form>
	</td>
	</tr>
	<?php endforeach; endif; unset($_from); ?>
   </tr>
   </table>
   <?php endif; ?>
   <?php if ($this->_tpl_vars['looktype'] == 'Add'): ?>
   
 <form action="Search_Synonym_Admin.php" method="post" name="mapping-add-form" class="mapping-add-form">
<input type="hidden" name="mode" class="mode" value="SearchMode"/>
<input type="submit" value="Go to Search Mode">
</form>  
 <form action="Search_Synonym_Admin.php" method="post" name="mapping-add-form" class="mapping-add-form">
<input type="hidden" name="mode" value="addgroup" />
<input type="hidden" name="searchLanding" class="mode" value="<?php echo $this->_tpl_vars['searchLanding']; ?>
"/>
<input type="hidden" name="offset" class="mode" value="<?php echo $this->_tpl_vars['offset']; ?>
"/>
<h2>Enter Values in both the columns, you don't need to fill all the rows</h2>
<table cellpadding="3" cellspacing="1" width="100%">
   <td><b>Original</b></td>
   <td><b>Corrected</b></td>
	<?php $_from = $this->_tpl_vars['addArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['dataloop'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['dataloop']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['row']):
        $this->_foreach['dataloop']['iteration']++;
?>
	<tr>
	<td><input type="text" name="addOutput[<?php echo $this->_tpl_vars['row']; ?>
][original]" class="mode"}"/></td>
	<td><input type="text" name="addOutput[<?php echo $this->_tpl_vars['row']; ?>
][corrected]" class="mode"}"/></td>
	</tr>
	<?php endforeach; endif; unset($_from); ?>
</table>
 <input type="submit" value="Add">

</form>
<?php endif; ?>



<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => ' Paramaterised Search Fields','content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php echo '
  <script language="Javascript">
     $(document).ready(function(){
    	 
    	 $(\'#landing-check\').change(function(){
        	 if($(this).is(\':checked\')){
				$(\'input[name=landingPage]\').removeAttr(\'disabled\');
        	 }
        	 else{
        		 $(\'input[name=landingPage]\').attr(\'disabled\',\'disabled\');
           	 }
         });
 			
    	 
     })
  </script>
'; ?>
