5741872097c05f875e91c58d868de399a:3:{s:15:"lbl_quick_start";s:11:"Quick start";s:20:"lbl_quick_start_text";s:21:"How to set up a store";s:19:"txt_how_setup_store";s:5918:"This section walks you through the basic steps of building a store and provides brief step-by-step instructions on what should be done after X-Cart is installed. We did not mean it to be anything more than a quick overview, so please refer to X-Cart shopping software reference manual for more information on administrating X-Cart based online stores.

<br /><br />

<b>ENVIRONMENT</b>

<br /><br />

<dd />- Check your environment information (Summary section) and make sure all the necessary components are installed and the necessary directories have write permissions.

<br /><br />

<b>USERS MANAGEMENT</b>

<br /><br />

<dd />- Modify your profile. Change the default username/password that you used to log into the administrative zone.
<dd />- Set up the membership levels to be used in your store (X-Cart has a few preset membership levels used to control access permissions for different groups of store users. If necessary, you can add custom levels or remove the preset membership levels you do not need).
<dd />- Change all default user account passwords. If needed, add more admin/provider accounts (provider accounts are a specific feature of X-Cart PRO) and assign them to appropriate membership levels.
<dd />- Set up User Profiles options: define what information should be included into the profile details form for different types of users.
 
<br /><br />
 
<b>GENERAL SETTINGS</b>
 
<br /><br />
 
<dd />- Set up Company options: specify your company name, location (will be used for shipping rates calculation) and contact information; set up the e-mail addresses for notifications about new registered users, orders, etc.
<dd />- Set up General options to define the most general configuration of your store.

<br /><br />
 
<b>MODULES</b>
 
<br /><br />
 
<dd />- Enable the modules you want to use in your store.
<dd />- Set up Modules options and the options for the add-ons you installed and enabled (like Gift Registry, Fancy Categories, X-Affiliate etc)

<br /><br />
 
<b>SECURITY</b>
 
<br /><br />
 
<dd />- If you are going to use PGP or GnuPG encryption for e-mail notifications, set up PGP options or GnuPG options.
<dd />- Set up Security options including Blowfish encryption, filter for the extensions of files that can be uploaded to the server, etc.
 
<br /><br />

<b>COUNTRIES AND STATES SETTINGS</b>

<br /><br />

<dd />- Activate the countries to which your store is going to sell products, edit the additional countries features such as 'Has states' flag.
<dd />- Edit the lists of states for the countries you made active: remove the states you do not need or add the states that are not listed currently.

<br /><br />

<b>SHIPPING SETTINGS</b>

<br /><br />

<dd />- Set up Shipping options: enable shipping (if you need shipping in your store), define additional shipping settings such as using real time processing for shipping rates calculation.
<dd />- Edit the list of shipping methods to be used in your store (enable/disable the pre-set shipping methods or add shipping methods of your own).
<dd />- Define real-time processors options (if you selected to use shipping rates real time processing).

<br /><br />

<b>TAXES SETTINGS</b>

<br /><br />

<dd />- Set up Taxes options: select the taxing system for the store and set up additional settings for taxes application.

<br /><br />

<b>PAYMENT SYSTEM SETTINGS</b>

<br /><br />

<dd />- Define payment methods that your customers will be able to use when making orders at your store.
<dd />- If you are going to allow your customers to use credit cards to pay for their orders, specify what credit card types you are going to accept.
<dd />- Set up the options for the payment methods you are planning to use for your store (i.e. PayPal options, NOCHEX options)
<dd />- Set up payment gateway(s) for CC processing (if you are going to use Credit Card payment method), check processing (if you are going to use Pay by Check payment method) and subscription processing.

<br /><br />

<b>LOOK AND FEEL SETTINGS</b>

<br /><br />

<dd />- Set up Appearance options to configure the look of your site (set the limit of products per page, define your products list style, etc).
<dd />- Edit templates (if needed), upload your logo and graphics, change the skin1.css file.
<dd />- Edit the Speed bar to improve navigation within the customer area of your store.
<dd />- Add Static pages that can be displayed as part of your store (e.g. sections 'About Us', 'FAQs', etc)

<br /><br />

<b>LANGUAGES MANAGEMENT</b>
 
<br /><br />
 
<dd />- Edit the language labels values (such as labels, text, error messages, and email text).
<dd />- Use Webmaster mode to facilitate the language labels modifications (Webmaster mode allows you to make changes to the language labels stored in the database directly from the pages on which they appear).
<dd />- If necessary, add or import a new language.
<dd />- Set up the default customer and admin language.

<br /><br />

<b>PRODUCTS MANAGEMENT</b>

<br /><br />

<dd />- Set up categories so your products could be classified for your customers. convenience.
<dd />- Create a list of manufacturers that will allow you to classify your products by the brand name.
<dd />- Add new products.
<dd />- Create featured products lists that will be displayed within categories.

<br /><br />

<b>NEWS MANAGEMENT</b>

<br /><br />

<dd />- Create one or more news lists for your customers.
<dd />- Create news content (e.g. you could set up 'Site announcements' news list with the first news message about the launching of your site)

<br /><br />

<b>SEARCH ENGINE FRIENDLINESS</b>

<br /><br />

<dd />- Set up SEO options: enter the meta tags (keyword and description) that will be common for all the pages of your site, select the page title format, etc.
<dd />- When your store is ready for launching generate an HTML catalog to increase the site rating in the search engines.
";}