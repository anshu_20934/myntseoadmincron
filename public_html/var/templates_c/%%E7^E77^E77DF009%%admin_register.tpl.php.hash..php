63379485b0cf4ff5d29474b1d528c71ea:26:{s:18:"err_billing_county";s:89:"Incorrect county for billing address (the county does not belong to the specified state).";s:17:"err_billing_state";s:89:"Incorrect state for billing address (the state does not belong to the specified country).";s:19:"err_shipping_county";s:90:"Incorrect county for shipping address (the county does not belong to the specified state).";s:18:"err_shipping_state";s:90:"Incorrect state for shipping address (the state does not belong to the specified country).";s:27:"lbl_create_customer_profile";s:23:"Create customer profile";s:18:"lbl_create_profile";s:14:"Create Profile";s:27:"lbl_modify_customer_profile";s:23:"Modify customer profile";s:18:"lbl_modify_profile";s:14:"Modify profile";s:19:"lbl_profile_details";s:15:"Profile details";s:28:"lbl_return_to_search_results";s:28:"Return to the search results";s:8:"lbl_save";s:4:"Save";s:22:"lbl_terms_n_conditions";s:22:"Terms &amp; Conditions";s:27:"txt_create_customer_profile";s:52:"This section allows you to create customer profiles.";s:22:"txt_create_profile_msg";s:161:"The form below allows you to create a profile which is necessary to place orders. Do not forget that this information is essential to use our services correctly.";s:24:"txt_email_already_exists";s:42:"Email address already exists in database !";s:17:"txt_email_invalid";s:41:"E-mail address is invalid! Please correct";s:24:"txt_fields_are_mandatory";s:73:"The fields marked with <font class=Star""><b>*</b></font> are mandatory."";s:27:"txt_modify_customer_profile";s:51:"This section allows you to modify customer profile.";s:22:"txt_modify_profile_msg";s:79:"This form enables you to modify your profile so that your data is always valid.";s:30:"txt_newbie_registration_bottom";s:561:"<b>Information
</b><br /><br />
Your password must be different from your username. We recommend you using passwords of 5 or more characters.
<br />
<br />
Your e-mail address must be valid. We use e-mail for communication purposes (order notifications, etc).  Therefore, it is essential to provide a valid e-mail address to be able to use our services correctly.
<br />
<br />
All your private data is confidential. We will never sell, exchange or market it in any way.
<br />
For further information on the responsibilities of both parts, you may refer to our";s:19:"txt_profile_created";s:357:"Thank you, your user profile has been successfully created.<br />
<br />
A confirmation e-mail has been sent to the email address that you entered. You can use these details to log in to our website in future. This will make your purchase even easier.<br />
<br />
Now that your account is active and you are logged in you may continue with your purchases.
";s:20:"txt_profile_modified";s:31:"Your profile has been modified.";s:22:"txt_registration_error";s:142:"The form is filled in with errors !<br />Check that you have entered all mandatory values.<br />Check that your password matches confirmation.";s:36:"txt_terms_and_conditions_newbie_note";s:126:"Clicking SUBMIT"" you agree with our <a href=""help.php?section=conditions"" target=""_blank"">""Terms &amp; Conditions""</a>"";s:23:"txt_user_already_exists";s:20:"User already exists!";s:28:"txt_user_registration_bottom";s:272:"<b>Information
</b>
<br /><br />
The password must be different from the username. It is strongly recommended to use passwords of 5 or more characters.
<br /><br />
E-mail address must be valid because it will be used for communication purposes (order notifications, etc).";}