<?php /* Smarty version 2.6.12, created on 2017-03-29 23:11:06
         compiled from admin/order_dump.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'config_load', 'admin/order_dump.tpl', 1, false),)), $this); ?>
<?php func_load_lang($this, "admin/order_dump.tpl","txt_site_title"); ?><?php echo smarty_function_config_load(array('file' => ($this->_tpl_vars['skin_config'])), $this);?>

<html>
<head>
<title><?php echo $this->_tpl_vars['lng']['txt_site_title']; ?>
</title>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "meta.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['SkinDir']; ?>
/skin1_admin.css" />
<script type="text/javascript">
    var http_loc = '<?php echo $this->_tpl_vars['http_location']; ?>
'
 </script>
</head>
<body <?php echo $this->_tpl_vars['reading_direction_tag']; ?>
>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "rectangle_top.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "head_admin.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!-- main area -->
<table width="100%" cellpadding="0" cellspacing="0" align="center">
<tr>
<td class="VertMenuLeftColumn">


<?php if ($this->_tpl_vars['login'] == ""): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "auth.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<br />
<?php else: ?>
<?php $this->assign('user_count', 1); ?>
<?php $this->assign('operation', 1); ?>
<?php $this->assign('order_types', 1); ?>
<?php $_from = $this->_tpl_vars['roles']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['user']):
?>
<?php if (( $this->_tpl_vars['user']['role'] == 'SA' || $this->_tpl_vars['user']['role'] == 'MA' || $this->_tpl_vars['user']['role'] == 'CS' ) && $this->_tpl_vars['user_count'] == 1): ?>
<?php $this->assign('user_count', $this->_tpl_vars['user_count']+1); ?>
<br />
<?php endif; ?>

<!--order search-->
<?php if (( $this->_tpl_vars['user']['role'] == 'AD' || $this->_tpl_vars['user']['role'] == 'CS' || $this->_tpl_vars['user']['role'] == 'OP' ) && $this->_tpl_vars['order_types'] == 1): ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/order_type_search.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <?php $this->assign('order_types', $this->_tpl_vars['order_types']+1); ?>
<br/>
<?php endif; ?>
<!--order search-->

<?php if (( $this->_tpl_vars['user']['role'] == 'OP' || $this->_tpl_vars['user']['role'] == 'CS' ) && $this->_tpl_vars['operation'] == 1): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/menu_operations.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $this->assign('operation', $this->_tpl_vars['operation']+1); ?>
<br />
<?php endif; ?>

<?php if ($this->_tpl_vars['user']['role'] == 'CS'): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/customer_care_admin.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<br />
<?php endif; ?>

<?php if ($this->_tpl_vars['user']['role'] == 'AD'): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/myntra_admin.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<br />
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/system_admin.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<br />
<?php endif; ?>

<?php if ($this->_tpl_vars['user']['role'] == 'FI'): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/menu_reports.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<br />
<?php endif; ?>



<?php if ($this->_tpl_vars['user']['role'] == 'MA'): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/menu_marketing.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<br />
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/menu_reports.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<br />
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/website_content.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<br />
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/website_seo.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<br />
<?php endif; ?>
<?php if ($this->_tpl_vars['user']['role'] == 'PM'): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/product_mgmt.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>
<?php endforeach; endif; unset($_from); ?>
<?php if ($this->_tpl_vars['active_modules']['XAffiliate'] != ''): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/menu_affiliate.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>
<br />
<?php endif; ?>
<br />
<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="150" height="1" alt="" />
</td>
<td valign="top">

<!-- central space -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "location.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog_message.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($GLOBALS['HTTP_GET_VARS']['mode'] == 'subscribed'): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subscribe_confirmation.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php elseif ($this->_tpl_vars['auth_status'] == 'NotAuthorized'): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/access_notallowed.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
 <?php else: ?>
<?php 
 $messages = $this->get_template_vars('messages');
 foreach($messages as $message) {
  echo '<p> '.$message.'</p>';
 }
 ?>
<form enctype="multipart/form-data" action="/admin/order_dump.php" method="POST">
    Choose a file to upload:
    <br />
    <br />
    <table>
    <tr>
    <td>
    Collection file :
    </td>
    <td>
    <input name="collection_file" type="file"/>
    </td>
    </tr>
    <tr>
    <td>
    Refund file :
    </td>
    <td>
     <input name="refund_file" type="file"/><br/>
    </td>
    </tr>
    </table> 
 
 <br />
    <select name="gateway">
        <option value="">Select a gateway</option>
        <option value="ccavenue" <?php if ($this->_tpl_vars['gateway'] == 'ccavenue'): ?> selected="true"<?php endif; ?> >CC Avenue</option>
        <option value="ebs" <?php if ($this->_tpl_vars['gateway'] == 'ebs'): ?> selected="true"<?php endif; ?>>EBS</option>
        <option value="icici" <?php if ($this->_tpl_vars['gateway'] == 'icici'): ?> selected="true"<?php endif; ?>>ICICI</option>
        <option value="axis" <?php if ($this->_tpl_vars['gateway'] == 'axis'): ?> selected="true"<?php endif; ?>>Axis</option>
        <option value="atom" <?php if ($this->_tpl_vars['gateway'] == 'atom'): ?> selected="true"<?php endif; ?>>Atom</option>
        <option value="tekprocess" <?php if ($this->_tpl_vars['gateway'] == 'tekprocess'): ?> selected="true"<?php endif; ?>>Tekprocess</option>
        <option value="hdfc" <?php if ($this->_tpl_vars['gateway'] == 'hdfc'): ?> selected="true"<?php endif; ?>>HDFC</option>
    </select>
<br />
<br />
     <input type="submit" value="Upload"/>
<br />
 <br />

</form>
<?php endif; ?>