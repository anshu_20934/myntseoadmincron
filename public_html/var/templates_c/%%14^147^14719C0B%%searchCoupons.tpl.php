<?php /* Smarty version 2.6.12, created on 2017-04-15 12:02:27
         compiled from modules/Discount_Coupons/searchCoupons.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'modules/Discount_Coupons/searchCoupons.tpl', 212, false),array('modifier', 'date_format', 'modules/Discount_Coupons/searchCoupons.tpl', 233, false),)), $this); ?>
<!-- 
  Page for user-specific coupons search.
  Author: Rohan.Railkar
 -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "main/calendar2.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "page_title.tpl", 'smarty_include_vars' => array('title' => 'Coupon Management')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php echo '
<script type="text/javascript" language="JavaScript 1.2">

	function validateSearch()
	{
		document.getElementById(\'page\').value = 1;
		document.getElementById(\'npage\').value = 2;
		return true;
	}

	function getPrevPage()
	{
		document.getElementById(\'page\').value = document.getElementById(\'ppage\').value;
	}

	function getNextPage()
	{
		document.getElementById(\'page\').value = document.getElementById(\'npage\').value;
	}

</script>
'; ?>


<form action="searchCoupons.php" method="post" name="searchCouponsForm" id="searchCouponsForm">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="page" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
" />
<input type="hidden" name="npage" id="npage" value="<?php echo $this->_tpl_vars['npage']; ?>
" />
<input type="hidden" name="ppage" id="ppage" value="<?php echo $this->_tpl_vars['ppage']; ?>
" />
<table cellpadding="0" cellspacing="0" width="100%">


	<tr><h1>Search Coupons</h1></tr>
	<tr>
		<td> <!-- Form -->
			<table>
				<tr>
					<td><strong>Start Date:</strong></td>
					<td>
						<input type="text" id="searchStartDate" name="searchStartDate" value="<?php echo $this->_tpl_vars['searchStartDate']; ?>
" size="15" />
						<a href="javascript:cal1.popup();">
							<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"> 
						</a>
					</td>
				</tr>
				<tr>
					<td><strong>End Date:</strong></td>
					<td>
						<input type="text" id="searchEndDate" name="searchEndDate" value="<?php echo $this->_tpl_vars['searchEndDate']; ?>
" size="15" />
						<a href="javascript:cal2.popup();">
							<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"> 
						</a>
					</td>
				</tr>
				
				<script language="javascript">
					var cal1 = new calendar2(document.forms['searchCouponsForm'].elements['searchStartDate']);
    				cal1.year_scroll = true;
    				cal1.time_comp = false;

    				var cal2 = new calendar2(document.forms['searchCouponsForm'].elements['searchEndDate']);
    				cal2.year_scroll = true;
    				cal2.time_comp = false;
				</script>
				
				<tr>
					<td><strong>Coupon Group:</strong></td>
					<td>
						<select id="searchGroup" name="searchGroup">
							<option value="" <?php if ($this->_tpl_vars['searchGroup'] == ""): ?>selected="true"<?php endif; ?>">All</option>
							<?php $_from = $this->_tpl_vars['groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['name']):
?>
								<option value="<?php echo $this->_tpl_vars['name']; ?>
" <?php if ($this->_tpl_vars['searchGroup'] == $this->_tpl_vars['name']): ?>selected="true"<?php endif; ?>"><?php echo $this->_tpl_vars['name']; ?>
</option>
							<?php endforeach; endif; unset($_from); ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><strong>Channel:</strong></td>
					<td>
						<select id="searchChannel" name="searchChannel">
							<option value="" <?php if ($this->_tpl_vars['searchChannel'] == ""): ?>selected="true"<?php endif; ?>">All</option>
							<?php $_from = $this->_tpl_vars['channels']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['name']):
?>
								<option value="<?php echo $this->_tpl_vars['name']; ?>
" <?php if ($this->_tpl_vars['searchChannel'] == $this->_tpl_vars['name']): ?>selected="true"<?php endif; ?>"><?php echo $this->_tpl_vars['name']; ?>
</option>
							<?php endforeach; endif; unset($_from); ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><strong>Usage:</strong></td>
					<td>
						<input type="text" id="searchUsageGTE" name="searchUsageGTE" value=<?php echo $this->_tpl_vars['searchUsageGTE']; ?>
></input>
						&nbsp; &le; Usage &le; &nbsp;
						<input type="text" id="searchUsageLTE" name="searchUsageLTE" value=<?php echo $this->_tpl_vars['searchUsageLTE']; ?>
></input>
					</td>
				</tr>
				<tr>
					<td><strong>Per User Usage:</strong></td>
					<td>
						<input type="text" id="searchUsagePerUserGTE" name="searchUsagePerUserGTE" value=<?php echo $this->_tpl_vars['searchUsagePerUserGTE']; ?>
></input>
						&nbsp; &le; Per User Usage &le; &nbsp;
						<input type="text" id="searchUsagePerUserLTE" name="searchUsagePerUserLTE" value=<?php echo $this->_tpl_vars['searchUsagePerUserLTE']; ?>
></input>
					</td>
				</tr>
				<tr>
					<td><strong>Percentage Discount:</strong></td>
					<td>
						<input type="text" id="searchPercentageGTE" name="searchPercentageGTE" value=<?php echo $this->_tpl_vars['searchPercentageGTE']; ?>
></input>
						% &nbsp; &le; Percentage Discount &le; &nbsp;
						<input type="text" id="searchPercentageLTE" name="searchPercentageLTE" value=<?php echo $this->_tpl_vars['searchPercentageLTE']; ?>
></input>
						%
					</td>
				</tr>
				<tr>
					<td><strong>Absolute Discount:</strong></td>
					<td>
						Rs. <input type="text" id="searchAbsGTE" name="searchAbsGTE" value=<?php echo $this->_tpl_vars['searchAbsGTE']; ?>
></input>
						&nbsp; &le; Absolute Discount &le; &nbsp;
						Rs. <input type="text" id="searchAbsLTE" name="searchAbsLTE" value=<?php echo $this->_tpl_vars['searchAbsLTE']; ?>
></input>
					</td>
				</tr>
				<tr>
					<td><strong>Free shipping:</strong></td>
					<td>
						<select id="searchFreeShipping" name="searchFreeShipping">
							<option value="" <?php if ($this->_tpl_vars['searchFreeShipping'] == ""): ?>selected="true"<?php endif; ?>>Any</option>
							<option value="1" <?php if ($this->_tpl_vars['searchFreeShipping'] == '1'): ?>selected="true"<?php endif; ?>>Free Shipping</option>
							<option value="0" <?php if ($this->_tpl_vars['searchFreeShipping'] == '0'): ?>selected="true"<?php endif; ?>>No Free Shipping</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><strong>Min. Cart Amount:</strong></td>
					<td>
						Rs. <input type="text" id="searchMinGTE" name="searchMinGTE" value=<?php echo $this->_tpl_vars['searchMinGTE']; ?>
></input>
						&nbsp; &le; Min. Cart Amount &le; &nbsp;
						Rs. <input type="text" id="searchMinLTE" name="searchMinLTE" value=<?php echo $this->_tpl_vars['searchMinLTE']; ?>
></input>
					</td>
				</tr>
				<tr>
					<td><strong>Max. Cart Amount:</strong></td>
					<td>
						Rs. <input type="text" id="searchMaxGTE" name="searchMaxGTE" value=<?php echo $this->_tpl_vars['searchMaxGTE']; ?>
></input>
						&nbsp; &le; Max. Cart Amount &le; &nbsp;
						Rs. <input type="text" id="searchMaxLTE" name="searchMaxLTE" value=<?php echo $this->_tpl_vars['searchMaxLTE']; ?>
></input>
					</td>
				</tr>
				<tr>
					<td><strong>Allowed User Patterns:</strong></td>
					<td><input type="text" id="searchAllowedUsers" name="searchAllowedUsers" value=<?php echo $this->_tpl_vars['searchAllowedUsers']; ?>
></input></td>
				</tr>
				<tr>
					<td><strong>Excluded User Patterns:</strong></td>
					<td><input type="text" id="searchExclUsers" name="searchExclUsers" value=<?php echo $this->_tpl_vars['searchExclUsers']; ?>
></input></td>
				</tr>
				<tr>
					<td><strong>Description contains:</strong></td>
					<td><input type="text" id="searchDesc" name="searchDesc" value=<?php echo $this->_tpl_vars['searchDesc']; ?>
></input></td>
				</tr>
				<tr>
					<td><input type="submit" value="Search" onClick="return validateSearch();"></td>
				</tr>
			</table>
		</td>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td align="right">
			<?php if ($this->_tpl_vars['previous']): ?><input type="submit" value="Previous" onClick="getPrevPage();"><?php endif; ?>
			| (Current page: <?php echo $this->_tpl_vars['page']; ?>
/<?php echo $this->_tpl_vars['totalpages']; ?>
) 
			| <?php if ($this->_tpl_vars['next']): ?><input type="submit" value="Next" onClick="getNextPage();"><?php endif; ?>
		</td>
	</tr>

</table>

&nbsp;
<hr />

<!-- The results table. -->
<table cellpadding="3" cellspacing="1" width="100%">
	<tr class="TableHead">
		<td width="3%">&nbsp;</td>
		<td width="10%">COUPON</td>
		<td width="10">GROUP</td>
		<td width="10%">USERS</td>
		<td width="5%">TYPE</td>
		<td width="10%">DISCOUNT</td>
		<td width="10%">TIMES</td>
		<td width="10%">SUBTOTAL</td>
		<td width="10%">DISCOUNT OFFERED</td>
		<td width="9%">EXPIRES ON</td>
		<td width="4%">STATUS</td>
		<td width="9%">LAST EDITED</td>
	</tr>
	
<?php unset($this->_sections['prod_num']);
$this->_sections['prod_num']['name'] = 'prod_num';
$this->_sections['prod_num']['loop'] = is_array($_loop=$this->_tpl_vars['coupons']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['prod_num']['show'] = true;
$this->_sections['prod_num']['max'] = $this->_sections['prod_num']['loop'];
$this->_sections['prod_num']['step'] = 1;
$this->_sections['prod_num']['start'] = $this->_sections['prod_num']['step'] > 0 ? 0 : $this->_sections['prod_num']['loop']-1;
if ($this->_sections['prod_num']['show']) {
    $this->_sections['prod_num']['total'] = $this->_sections['prod_num']['loop'];
    if ($this->_sections['prod_num']['total'] == 0)
        $this->_sections['prod_num']['show'] = false;
} else
    $this->_sections['prod_num']['total'] = 0;
if ($this->_sections['prod_num']['show']):

            for ($this->_sections['prod_num']['index'] = $this->_sections['prod_num']['start'], $this->_sections['prod_num']['iteration'] = 1;
                 $this->_sections['prod_num']['iteration'] <= $this->_sections['prod_num']['total'];
                 $this->_sections['prod_num']['index'] += $this->_sections['prod_num']['step'], $this->_sections['prod_num']['iteration']++):
$this->_sections['prod_num']['rownum'] = $this->_sections['prod_num']['iteration'];
$this->_sections['prod_num']['index_prev'] = $this->_sections['prod_num']['index'] - $this->_sections['prod_num']['step'];
$this->_sections['prod_num']['index_next'] = $this->_sections['prod_num']['index'] + $this->_sections['prod_num']['step'];
$this->_sections['prod_num']['first']      = ($this->_sections['prod_num']['iteration'] == 1);
$this->_sections['prod_num']['last']       = ($this->_sections['prod_num']['iteration'] == $this->_sections['prod_num']['total']);
?>
	
	<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['times_used'] + $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['times_locked'] >= $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['times']): ?>  
	    <?php $this->assign('color', 'red'); ?> 
	 <?php else: ?>
	 	<?php $this->assign('color', ""); ?> 	   
	 <?php endif; ?>

<tr<?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'",'advance' => false), $this);?>
 >
	
	<td><input type="checkbox" name="posted_data[<?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['code']; ?>
][selected]" /></td>
	<td style="color:<?php echo $this->_tpl_vars['color']; ?>
;"><b><?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['code']; ?>
</b></td>
	<td align="center"><?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['groupName']; ?>
</td>
	<td align="center"><?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['users']; ?>
</td>
	<td align="center"><?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['couponType']; ?>
</td>
	<td align="center">
		<?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['couponType'] == 'absolute'): ?>
	 		Upto <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "currency.tpl", 'smarty_include_vars' => array('value' => $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['mrpAmount'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?> off
	 	<?php elseif ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['couponType'] == 'percentage'): ?>
	 		<?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['mrpPercentage']; ?>
% off
	 	<?php elseif ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['couponType'] == 'dual'): ?>
	 		<?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['mrpPercentage']; ?>
% off upto <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "currency.tpl", 'smarty_include_vars' => array('value' => $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['mrpAmount'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	 	<?php else: ?>
	 		<?php $this->assign('discount', 'Free shipping'); ?>
	 	<?php endif; ?>
	</td>
	<td align="center">(<font color="green"><?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['times_used']; ?>
</font> + <font color="red"><?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['times_locked']; ?>
</font>)/<?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['times']; ?>
</td>
	<td align="center"><?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['subtotal']; ?>
</td>
	<td align="center"><?php echo $this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['discountOffered']; ?>
</td>
	<td align="center" nowrap="nowrap"><?php echo ((is_array($_tmp=$this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['expire'])) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['config']['Appearance']['date_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['config']['Appearance']['date_format'])); ?>
</td>
	<td align="center"><?php if ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['status'] == 'A'): ?>Active<?php elseif ($this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['status'] == 'A'): ?>Disabled<?php else: ?>Unknown<?php endif; ?></td>
	<td align="center" nowrap="nowrap"><?php echo ((is_array($_tmp=$this->_tpl_vars['coupons'][$this->_sections['prod_num']['index']]['lastEdited'])) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['config']['Appearance']['time_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['config']['Appearance']['time_format'])); ?>
</td>
	
</tr>
<?php endfor; endif; ?>

<tr>
	<td colspan="12"><br />
		<input type="submit" value="Delete Coupon(s)" onclick="javascript:document.searchCouponsForm.mode.value='deleteCoupon';document.searchCouponsForm.submit();" />
		&nbsp;
		<input type="submit" value="Modify Coupon" onclick="javascript:document.searchCouponsForm.mode.value='modifyCoupon';document.searchCouponsForm.submit();"/>
	</td>
</tr>
	
</table>

</form>
