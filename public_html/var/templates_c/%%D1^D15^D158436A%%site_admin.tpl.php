<?php /* Smarty version 2.6.12, created on 2017-03-24 12:40:05
         compiled from admin/site_admin.tpl */ ?>
<?php func_load_lang($this, "admin/site_admin.tpl","lbl_general_settings"); ?><?php ob_start(); ?>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/configuration.php" class="VertMenuItems"><?php echo $this->_tpl_vars['lng']['lbl_general_settings']; ?>
</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/manageBots.php" class="VertMenuItems">Manage Bots</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/set_solr_fields_priority.php" class="VertMenuItems">Set Solr Priority</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/Search_Synonym_Admin.php" class="VertMenuItems">Search Synonym</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/set_sorting_order.php" class="VertMenuItems">Set Sorting Order</a></li>
<br>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/widgets/popular_widget.php" class="VertMenuItems">Most Popular Links</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/widgets/banners_widget.php" class="VertMenuItems">Home Page Banners</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/widgets/homepagelists_widget.php" class="VertMenuItems">Home Page Scrollable Lists</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/widgets/topnavigation_widget.php" class="VertMenuItems">Top Navigation Menus</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/widgets/topnavigation_widget_v2.php" class="VertMenuItems">Top Navigation Menus V2</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/widgets/topnavigation_widget_v3.php" class="VertMenuItems">Top Navigation Menus V3</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/widgets/keyvaluepairs_widget.php" class="VertMenuItems">Widget Key Value Pairs</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/Search_Feature_Gate.php" class="VertMenuItems">Boosted Search Parameters</a></li>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/solr_search.php" class="VertMenuItems">Add solr Search</a></li>
<!-- <li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/size_unification_filter_order.php" class="VertMenuItems">Size Filter Order</a></li> -->
<br>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/feature_gate.php?mode=view&object=keyvaluepairs" class="VertMenuItems">Feature Gate</a></li>
<br>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/abtesting_gate.php?mode=view&object=list" class="VertMenuItems">AB Tests List</a></li>
<br>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/xcache_common.php" class="VertMenuItems">XCache</a></li>
<br>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/jpegminiConfig.php" class="VertMenuItems">Jpegmini Config</a></li>
<br>
<li><a href="<?php echo $this->_tpl_vars['catalogs']['admin']; ?>
/mobileUAWhitelist.php" class="VertMenuItems">Mobile UA Whitelist</a></li>
<br>
<?php $this->_smarty_vars['capture']['menu'] = ob_get_contents(); ob_end_clean();  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu.tpl", 'smarty_include_vars' => array('dingbats' => "dingbats_categorie.gif",'menu_title' => 'Site Admin','menu_content' => $this->_smarty_vars['capture']['menu'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>