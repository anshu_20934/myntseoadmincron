<?php /* Smarty version 2.6.12, created on 2017-03-24 13:09:22
         compiled from admin/main/search_styles.tpl */ ?>
<script type="text/javascript">
    var brandsData         = <?php echo $this->_tpl_vars['brands']; ?>
;
    var ageGroupsData      = <?php echo $this->_tpl_vars['ageGroups']; ?>
;
    var gendersData        = <?php echo $this->_tpl_vars['genders']; ?>
;
    var fashionTypesData   = <?php echo $this->_tpl_vars['fashionTypes']; ?>
;
    var coloursData        = <?php echo $this->_tpl_vars['colours']; ?>
;
    var seasonsData        = <?php echo $this->_tpl_vars['seasons']; ?>
;
    var yearsData          = <?php echo $this->_tpl_vars['years']; ?>
;
    var usagesData         = <?php echo $this->_tpl_vars['usages']; ?>
;
    
    var masterCategoryData = <?php echo $this->_tpl_vars['masterCategory']; ?>
;
	var subCategoryData    = <?php echo $this->_tpl_vars['subCategory']; ?>
;
	var articleTypeData    = <?php echo $this->_tpl_vars['articleType']; ?>
;

	var styleStatusData    = <?php echo $this->_tpl_vars['styleStatus']; ?>
;
	var progresskey        = '<?php echo $this->_tpl_vars['progresskey']; ?>
';
    var httpLocation       = '<?php echo $this->_tpl_vars['http_location']; ?>
';
    
</script>

<link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/extjs/resources/css/ext-all.css"/>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/extjs/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/extjs/ext-all.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/extjs/plugins/superselectbox/SuperBoxSelect.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['http_location']; ?>
/admin/extjs/plugins/superselectbox/superboxselect.css"/>
<script type="text/javascript" src="search_styles.js"></script>

<div id="contentPanel"></div>