<?php /* Smarty version 2.6.12, created on 2017-04-07 12:00:06
         compiled from admin/main/attribute_values.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'admin/main/attribute_values.tpl', 138, false),)), $this); ?>
<?php func_load_lang($this, "admin/main/attribute_values.tpl","txt_featured_products,lbl_yes,lbl_no,lbl_yes,lbl_no,lbl_yes,lbl_no,lbl_add_product,lbl_yes,lbl_no"); ?><base href="<?php echo $this->_tpl_vars['http_location']; ?>
/">
<script type="text/javascript" src="<?php echo $this->_tpl_vars['http_location']; ?>
/skin1/myntra_js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['http_location']; ?>
/skin1/js_script/jquery.validate.js"></script>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "main/popup_product.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<style>
 .error_block{
 	background:red;
 }
</style>
  <script language="Javascript">
	function checkForDuplicateFilterOrder(){
		var arr_order=[];
		var no_error=true;
		$(\'.data_row\').each(function(index) {
				var dataRow = $(this);
				if($(this).find(":checkbox").attr(\'checked\')){
					if(jQuery.inArray($(this).find(\'.filter_order\').val(),arr_order) != -1){
						alert(\'filter order:\'+$(this).find(\'.filter_order\').val()+\' already exist. Please specify different orders\');
						no_error=false;
						jQuery(dataRow).addClass(\'error_block\');
						return false;
					}
					arr_order.push($(this).find(\'.filter_order\').val());
				}
			});
		return no_error;
		}
  	function confirmDelete()
      {
          var flag = confirm(\'Are you sure you want to delete?\')
          if(!flag)
          {
             return false;
          }
          else
          {
          	 document.groupfrm.mode.value = \'delete\';
          	 document.groupfrm.submit();
          	 return true;
          }
      }
      
      function onChangeArticleType(obj,issearch)
      {
      	  	
      	  $.get(http_loc+\'/admin/catalog/attribute_values.php\',{mode:"getArticleAttributes",article_id:obj.value}, function(data) {
	   
	   		var jsonData = jQuery.parseJSON(data);
	       	jQuery(obj).parent().parent().find("select:eq(1)").find("option").remove();
			var optionshtml=\'\';
			if(issearch==\'true\'){
      	  		optionshtml+=\'<option value="" >All</option>\';
      	  	}
			$.each(jsonData.attributes, function(intIndex,attribute){
				optionshtml+=\'<option value="\'+attribute.id+\'" >\'+attribute.name+\'</option>\';
			});
			jQuery(obj).parent().parent().find("select:eq(1)").append(optionshtml);
	   		
      	  });
      }

      function checkBrandCode4Char()
      {
    	  var article = document.frmG.article_type_id;
    	  var article_selected_index = article.selectedIndex;
    	  var article_text = article.options[article_selected_index].text;

    	  var attribute = document.frmG.attribute_type_id;
    	  var attribute_selected_index = attribute.selectedIndex;
    	  var attribute_text = attribute.options[attribute_selected_index].text;

    	  if(article_text==\'Global\' && attribute_text==\'Brand\'){
    		  var code = document.frmG.attribute_code.value;
    		  if(code.length !=4){
        		  alert("Brand Code must be 4 characters long");
        		  return false;
    		  }
    	  }
    	  document.frmG.submit();
          return true;
      }
  </script>
'; ?>

<a name="featured" />

<?php echo $this->_tpl_vars['lng']['txt_featured_products']; ?>


<br /><br />

<?php ob_start(); ?>
<form  action="<?php echo $this->_tpl_vars['form_action']; ?>
" method="post" name="groupfrm1" enctype="multipart/form-data">
<input type="hidden" name="mode" value="search" >
&nbsp;&nbsp;Search By Article Type :&nbsp;&nbsp;<select name="search_article_type_id" onchange="onChangeArticleType(this,'true');">
        <option value="">Global</option>
        <?php $_from = $this->_tpl_vars['article_types']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['types']):
?>
		    <option value="<?php echo $this->_tpl_vars['types']['id']; ?>
" <?php if ($this->_tpl_vars['search_article_type_id'] == $this->_tpl_vars['types']['id']): ?>selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['types']['typename']; ?>
</option>
		<?php endforeach; endif; unset($_from); ?>
	</select>
    &nbsp;&nbsp;Search By Type :&nbsp;&nbsp;<select name="search_type">
        <option value="">All</option>
		<?php $_from = $this->_tpl_vars['article_type_attributes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['attr_type_id'] => $this->_tpl_vars['attr_types']):
?>
			<?php if (( $this->_tpl_vars['attr_type_id'] == $this->_tpl_vars['search_article_type_id'] || ( $this->_tpl_vars['search_article_type_id'] == "" && $this->_tpl_vars['attr_type_id'] == 'global' ) )): ?>
				<?php $_from = $this->_tpl_vars['attr_types']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['types']):
?>
		    		<option value="<?php echo $this->_tpl_vars['types']['id']; ?>
" <?php if ($this->_tpl_vars['search_type'] == $this->_tpl_vars['types']['id']): ?> selected<?php endif; ?> ><?php echo $this->_tpl_vars['types']['attribute_type']; ?>
</option>
		    	<?php endforeach; endif; unset($_from); ?>
		    <?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
	</select>
	&nbsp;&nbsp;&nbsp;<input type="submit" value="search" name="sub"> 


</form>
<br>
<form action="<?php echo $this->_tpl_vars['form_action']; ?>
" method="post" name="groupfrm" enctype="multipart/form-data" onsubmit="return checkForDuplicateFilterOrder();">
<input type="hidden" name="mode"  />
<input type="hidden" name="search_type" value="<?php echo $GLOBALS['HTTP_POST_VARS']['search_type']; ?>
" />
<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="5%">&nbsp;</td>
	<td width="10%">Article Type</td>
	<td width="15%">Attribute Type</td>
	<td width="20%">Attribute Value</td>
	<td width="20%">Attribute Code</td>
	<td width="10%">filter order</td>
	<td width="20%" align="center"  style="display:none;">Applicable Set</td>
	<td width="3%" align="center">Active</td>
	<td width="3%" align="center">Featured</td>
	<td width="4%" align="center">Latest</td>
</tr>

<?php if ($this->_tpl_vars['attributes']): ?>

<?php unset($this->_sections['grid']);
$this->_sections['grid']['name'] = 'grid';
$this->_sections['grid']['loop'] = is_array($_loop=$this->_tpl_vars['attributes']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['grid']['show'] = true;
$this->_sections['grid']['max'] = $this->_sections['grid']['loop'];
$this->_sections['grid']['step'] = 1;
$this->_sections['grid']['start'] = $this->_sections['grid']['step'] > 0 ? 0 : $this->_sections['grid']['loop']-1;
if ($this->_sections['grid']['show']) {
    $this->_sections['grid']['total'] = $this->_sections['grid']['loop'];
    if ($this->_sections['grid']['total'] == 0)
        $this->_sections['grid']['show'] = false;
} else
    $this->_sections['grid']['total'] = 0;
if ($this->_sections['grid']['show']):

            for ($this->_sections['grid']['index'] = $this->_sections['grid']['start'], $this->_sections['grid']['iteration'] = 1;
                 $this->_sections['grid']['iteration'] <= $this->_sections['grid']['total'];
                 $this->_sections['grid']['index'] += $this->_sections['grid']['step'], $this->_sections['grid']['iteration']++):
$this->_sections['grid']['rownum'] = $this->_sections['grid']['iteration'];
$this->_sections['grid']['index_prev'] = $this->_sections['grid']['index'] - $this->_sections['grid']['step'];
$this->_sections['grid']['index_next'] = $this->_sections['grid']['index'] + $this->_sections['grid']['step'];
$this->_sections['grid']['first']      = ($this->_sections['grid']['iteration'] == 1);
$this->_sections['grid']['last']       = ($this->_sections['grid']['iteration'] == $this->_sections['grid']['total']);
?>

<tr class="<?php echo smarty_function_cycle(array('values' => "data_row ,data_row TableSubHead"), $this);?>
">
    <input type="hidden" name="posted_data[<?php echo $this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['id']; ?>
][id]" value=<?php echo $this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['id']; ?>
 />
	<td><input type="checkbox" name="posted_data[<?php echo $this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['id']; ?>
][to_delete]" /></td>
	<td align="center">
	<select name="posted_data[<?php echo $this->_tpl_vars['article_type'][$this->_sections['grid']['index']]['id']; ?>
][article_type_id]" onchange="onChangeArticleType(this);">
				<option value="">Global</option>
				<?php $_from = $this->_tpl_vars['article_types']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['types']):
?>
				<option value="<?php echo $this->_tpl_vars['types']['id']; ?>
" <?php if ($this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['catalogue_classification_id'] == $this->_tpl_vars['types']['id']): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['types']['typename']; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
	</select>
	</td>	
	<td align="center">
	<select name="posted_data[<?php echo $this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['id']; ?>
][attribute_type_id]">

		<?php $_from = $this->_tpl_vars['article_type_attributes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['attr_type_id'] => $this->_tpl_vars['attr_types']):
?>
			<?php if (( $this->_tpl_vars['attr_type_id'] == $this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['catalogue_classification_id'] ) || ( $this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['catalogue_classification_id'] == "" && $this->_tpl_vars['attr_type_id'] == 'global' )): ?>
				<?php $_from = $this->_tpl_vars['attr_types']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['types']):
?>
		    		<option value="<?php echo $this->_tpl_vars['types']['id']; ?>
" <?php if ($this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['attribute_type_id'] == $this->_tpl_vars['types']['id']): ?> selected<?php endif; ?> ><?php echo $this->_tpl_vars['types']['attribute_type']; ?>
</option>
		    	<?php endforeach; endif; unset($_from); ?>
		    <?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>

	</select>
	</td>
	<td align="center"><input type="text" name="posted_data[<?php echo $this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['id']; ?>
][attribute_value]" value="<?php echo $this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['attribute_value']; ?>
" /></td>
	<td align="center">
		<label><?php echo $this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['attribute_code']; ?>
</label>
	</td>
	<td align="center"  style="display:none;">
	<input type="text" value="<?php echo $this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['applicable_set']; ?>
" name="posted_data[<?php echo $this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['id']; ?>
][applicable_set]">
	</td>
	
	</td>
	<td align="center"  >
	<input type="text" class="filter_order" value="<?php echo $this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['filter_order']; ?>
" name="posted_data[<?php echo $this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['id']; ?>
][filter_order]">
	</td>
	
	<td align="center">
	<select name="posted_data[<?php echo $this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['id']; ?>
][is_active]">
		<option value="1"<?php if ($this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['is_active'] == '1'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_yes']; ?>
</option>
		<option value="0"<?php if ($this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['is_active'] == '0'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_no']; ?>
</option>
	</select>
	</td>
	<td align="center">
	<select name="posted_data[<?php echo $this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['id']; ?>
][is_featured]">
		<option value="1"<?php if ($this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['is_featured'] == '1'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_yes']; ?>
</option>
		<option value="0"<?php if ($this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['is_featured'] == '0'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_no']; ?>
</option>
	</select>
	</td>
	<td align="center">
	<select name="posted_data[<?php echo $this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['id']; ?>
][is_latest]">
		<option value="1"<?php if ($this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['is_latest'] == '1'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_yes']; ?>
</option>
		<option value="0"<?php if ($this->_tpl_vars['attributes'][$this->_sections['grid']['index']]['is_latest'] == '0'): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lng']['lbl_no']; ?>
</option>
	</select>
	</td>
</tr>

<?php endfor; endif; ?>
<td colspan="5"><br /><br /></td>
<?php else: ?>

<tr>
 <td colspan="4" align="center">No Article types available.</td>
</tr>
<?php endif; ?>

<!--<tr>
<td colspan="5"><br /><br /><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_add_product'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
</tr> -->

<tr>
	<td colspan="5" class="SubmitBox">
	<input type="button" value="Delete Selected" onclick="return confirmDelete();" />
	<input type="submit" value="Update Selected" onclick="javascript:document.groupfrm.mode.value = 'update';" />

	</td>
</tr>


</table>
</form>
<form name="frmG" action="<?php echo $this->_tpl_vars['form_action']; ?>
" method="post" enctype="multipart/form-data" >
<input type="hidden" name="mode" value="addgroup" />
<input type="hidden" name="search_type" value="<?php echo $GLOBALS['HTTP_POST_VARS']['search_type']; ?>
" />
<table cellpadding="3" cellspacing="1" width="100%">
   <tr>
        <td><hr/></td>
        <td><hr /></td>
   </tr>
   <tr>
        <td><h2>Add Attribute</h2></td>
        <td>&nbsp;</td>
   </tr>
   <tr>
   		<td>Article Type </td>
		<td>
		<select name="article_type_id" onchange="onChangeArticleType(this);">
				<option value="">Global</option>
				<?php $_from = $this->_tpl_vars['article_types']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['types']):
?>
				<option value="<?php echo $this->_tpl_vars['types']['id']; ?>
" <?php if ($this->_tpl_vars['search_article_type_id'] == $this->_tpl_vars['types']['id']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['types']['typename']; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
		</select>        
		</td>
		   			
        <td>Attribute Type </td>
        <td>
        <select name="attribute_type_id">
        <?php $_from = $this->_tpl_vars['attribute_types']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['types']):
?>
		    <option value="<?php echo $this->_tpl_vars['types']['id']; ?>
" <?php if ($this->_tpl_vars['search_type'] == $this->_tpl_vars['types']['id']): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['types']['attribute_type']; ?>
</option>
		<?php endforeach; endif; unset($_from); ?>
		</select>
        </td>
        </tr>
        <tr>
        <td>Attribute Value</td>
        <td><input type="text" name="attribute_value" value=""  /></td>
        <td>Attribute Code</td>
        <td><input type="text" name="attribute_code" value=""  /></td>
        <td>Filter Order</td>
        <td><input type="text" name="filter_order" value="0"  /></td>
        <td><input type="button" value="Add Attribute" name="sbtFrm" onClick="checkBrandCode4Char()" /></td>
        <td  style="visibility:hidden;">

        <select name="applicable_set">
            <option value="Default">Default</option>

        </select>
        </td>
        <td style="visibility:hidden;">Active</td>
         <td align="center" style="visibility:hidden;">
         
            <select name="is_active">
                <option value="1" ><?php echo $this->_tpl_vars['lng']['lbl_yes']; ?>
</option>
                <option value="0"><?php echo $this->_tpl_vars['lng']['lbl_no']; ?>
</option>
            </select>
        </td>
   </tr>
   <tr><td colspan="5"></td> </tr>
   <tr><td colspan="5"></td> </tr>
   <tr>
   <td colspan="4" align="center">
   
   </td>
   </tr>

</table>
</form>
<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'Attribute Type Values','content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<br /><br />


