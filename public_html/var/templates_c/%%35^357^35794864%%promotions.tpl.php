<?php /* Smarty version 2.6.12, created on 2017-03-24 16:31:29
         compiled from provider/main/promotions.tpl */ ?>
<?php func_load_lang($this, "provider/main/promotions.tpl","lbl_welcome_to_the_providers_zone,txt_personal_provider_area,txt_provider_promotion_note,lbl_add_new_product,txt_provider_promotion_add_new_product_note,lbl_product_modify,lbl_delete_product,txt_provider_promotion_modify_product_note,lbl_import_products,lbl_export_products,txt_provider_promotion_ie_product_note,lbl_extra_fields,lbl_provider_promotion_ef_note,lbl_shipping_charges,txt_provider_promotion_sc_note,lbl_destination_zones,txt_provider_promotion_dz_note,lbl_discounts,txt_provider_promotion_discounts_note,lbl_coupons,txt_provider_promotion_coupons_note,lbl_tax_rates,txt_provider_promotion_taxes_note,lbl_new_orders,txt_provider_promotion_no_note,lbl_search_orders_menu,txt_provider_promotion_so_note,lbl_provider_menu"); ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "page_title.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_welcome_to_the_providers_zone'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php ob_start(); ?>
<h3><?php echo $this->_tpl_vars['lng']['txt_personal_provider_area']; ?>
</h3>
<p align="justify">
<?php echo $this->_tpl_vars['lng']['txt_provider_promotion_note']; ?>

</p>
<p />
<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/rarrow.gif" width="9" height="9" alt="" /> <b><a href="<?php echo $this->_tpl_vars['catalogs']['provider']; ?>
/product_modify.php"><?php echo $this->_tpl_vars['lng']['lbl_add_new_product']; ?>
</a></b><br />
<?php echo $this->_tpl_vars['lng']['txt_provider_promotion_add_new_product_note']; ?>

<p />
<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/rarrow.gif" width="9" height="9" alt="" /> <b><a href="<?php echo $this->_tpl_vars['catalogs']['provider']; ?>
/search.php"><?php echo $this->_tpl_vars['lng']['lbl_product_modify']; ?>
</a></b><br />
<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/rarrow.gif" width="9" height="9" alt="" /> <b><a href="<?php echo $this->_tpl_vars['catalogs']['provider']; ?>
/search.php"><?php echo $this->_tpl_vars['lng']['lbl_delete_product']; ?>
</a></b><br />
<?php echo $this->_tpl_vars['lng']['txt_provider_promotion_modify_product_note']; ?>

<p />
<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/rarrow.gif" width="9" height="9" alt="" /> <b><a href="<?php echo $this->_tpl_vars['catalogs']['provider']; ?>
/import.php"><?php echo $this->_tpl_vars['lng']['lbl_import_products']; ?>
</a></b><br />
<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/rarrow.gif" width="9" height="9" alt="" /> <b><a href="<?php echo $this->_tpl_vars['catalogs']['provider']; ?>
/import.php?mode=export"><?php echo $this->_tpl_vars['lng']['lbl_export_products']; ?>
</a></b><br />
<?php echo $this->_tpl_vars['lng']['txt_provider_promotion_ie_product_note']; ?>

<?php if ($this->_tpl_vars['active_modules']['Extra_Fields'] != ""): ?>
<p />
<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/rarrow.gif" width="9" height="9" alt="" /> <b><a href="<?php echo $this->_tpl_vars['catalogs']['provider']; ?>
/extra_fields.php"><?php echo $this->_tpl_vars['lng']['lbl_extra_fields']; ?>
</a></b><br />
<?php echo $this->_tpl_vars['lng']['lbl_provider_promotion_ef_note']; ?>

<?php endif; ?>
<p />
<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/rarrow.gif" width="9" height="9" alt="" /> <b><a href="<?php echo $this->_tpl_vars['catalogs']['provider']; ?>
/shipping_rates.php"><?php echo $this->_tpl_vars['lng']['lbl_shipping_charges']; ?>
</a></b><br />
<?php echo $this->_tpl_vars['lng']['txt_provider_promotion_sc_note']; ?>

<p />
<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/rarrow.gif" width="9" height="9" alt="" /> <b><a href="<?php echo $this->_tpl_vars['catalogs']['provider']; ?>
/zones.php"><?php echo $this->_tpl_vars['lng']['lbl_destination_zones']; ?>
</a></b><br />
<?php echo $this->_tpl_vars['lng']['txt_provider_promotion_dz_note']; ?>

<p />
<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/rarrow.gif" width="9" height="9" alt="" /> <b><a href="<?php echo $this->_tpl_vars['catalogs']['provider']; ?>
/discounts.php"><?php echo $this->_tpl_vars['lng']['lbl_discounts']; ?>
</a></b><br />
<?php echo $this->_tpl_vars['lng']['txt_provider_promotion_discounts_note']; ?>

<p />
<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/rarrow.gif" width="9" height="9" alt="" /> <b><a href="<?php echo $this->_tpl_vars['catalogs']['provider']; ?>
/viewcoupongroups.php?action=group"><?php echo $this->_tpl_vars['lng']['lbl_coupons']; ?>
</a></b><br />
<?php echo $this->_tpl_vars['lng']['txt_provider_promotion_coupons_note']; ?>

<p />
<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/rarrow.gif" width="9" height="9" alt="" /> <b><a href="<?php echo $this->_tpl_vars['catalogs']['provider']; ?>
/taxes.php"><?php echo $this->_tpl_vars['lng']['lbl_tax_rates']; ?>
</a></b><br />
<?php echo $this->_tpl_vars['lng']['txt_provider_promotion_taxes_note']; ?>

<p />
<?php if ($this->_tpl_vars['active_modules']['Simple_Mode'] == ""): ?>
<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/rarrow.gif" width="9" height="9" alt="" /> <b><a href="<?php echo $this->_tpl_vars['catalogs']['provider']; ?>
/orders.php?substring=&amp;status=Q"><?php echo $this->_tpl_vars['lng']['lbl_new_orders']; ?>
</a></b><br />
<?php echo $this->_tpl_vars['lng']['txt_provider_promotion_no_note']; ?>

<p />
<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/rarrow.gif" width="9" height="9" alt="" /> <b><a href="<?php echo $this->_tpl_vars['catalogs']['provider']; ?>
/orders.php"><?php echo $this->_tpl_vars['lng']['lbl_search_orders_menu']; ?>
</a></b><br />
<?php echo $this->_tpl_vars['lng']['txt_provider_promotion_so_note']; ?>

<?php endif; ?>
<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_provider_menu'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>