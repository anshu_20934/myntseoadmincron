<?php /* Smarty version 2.6.12, created on 2018-03-16 18:43:57
         compiled from admin/main/upload_rtos_to_queue.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'admin/main/upload_rtos_to_queue.tpl', 24, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/include_js.tpl", 'smarty_include_vars' => array('src' => "main/popup_product.js")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
  ob_start(); ?>

<?php if ($this->_tpl_vars['action'] == 'upload'): ?>
        <form action="" method="post" name="queue_orders" enctype="multipart/form-data" >
                 * Input file format : <font color='blue'>Orderid, RTO reason</font>
                <br/>
                * First line of CSV file is ignored.
                <br />
                * If remark contains comma then give remark in double quotes.
		<br />
		<br />
		Upload the csv file : <input type="file" name="orderdetail" ><?php if ($this->_tpl_vars['message']): ?><font color='red'><?php echo $this->_tpl_vars['message']; ?>
</font><?php endif; ?>
                <input type='submit'  name='uploadxls'  value="Upload">
                <input type='hidden' name='action' value='verify'>
        </form>
<?php elseif ($this->_tpl_vars['action'] == verify): ?>
	<?php if ($this->_tpl_vars['dataFormatError']): ?>
                <b><?php echo $this->_tpl_vars['dataFormatError']; ?>
</b>
        <?php else: ?>
                <table cellpadding="2" cellspacing="1" width="100%">
			<tr class="TableHead"><td>Queued RTO</td></tr>
			<?php unset($this->_sections['idx']);
$this->_sections['idx']['name'] = 'idx';
$this->_sections['idx']['loop'] = is_array($_loop=$this->_tpl_vars['orders']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['idx']['show'] = true;
$this->_sections['idx']['max'] = $this->_sections['idx']['loop'];
$this->_sections['idx']['step'] = 1;
$this->_sections['idx']['start'] = $this->_sections['idx']['step'] > 0 ? 0 : $this->_sections['idx']['loop']-1;
if ($this->_sections['idx']['show']) {
    $this->_sections['idx']['total'] = $this->_sections['idx']['loop'];
    if ($this->_sections['idx']['total'] == 0)
        $this->_sections['idx']['show'] = false;
} else
    $this->_sections['idx']['total'] = 0;
if ($this->_sections['idx']['show']):

            for ($this->_sections['idx']['index'] = $this->_sections['idx']['start'], $this->_sections['idx']['iteration'] = 1;
                 $this->_sections['idx']['iteration'] <= $this->_sections['idx']['total'];
                 $this->_sections['idx']['index'] += $this->_sections['idx']['step'], $this->_sections['idx']['iteration']++):
$this->_sections['idx']['rownum'] = $this->_sections['idx']['iteration'];
$this->_sections['idx']['index_prev'] = $this->_sections['idx']['index'] - $this->_sections['idx']['step'];
$this->_sections['idx']['index_next'] = $this->_sections['idx']['index'] + $this->_sections['idx']['step'];
$this->_sections['idx']['first']      = ($this->_sections['idx']['iteration'] == 1);
$this->_sections['idx']['last']       = ($this->_sections['idx']['iteration'] == $this->_sections['idx']['total']);
?>
                        	<tr<?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this);?>
> <td align="center"><input type=hidden name = orderids[] value="<?php echo $this->_tpl_vars['orders'][$this->_sections['idx']['index']]; ?>
"><a href="old_rto_tracking.php?order_id=<?php echo $this->_tpl_vars['orders'][$this->_sections['idx']['index']]; ?>
" target="_blank"><?php echo $this->_tpl_vars['orders'][$this->_sections['idx']['index']]; ?>
</a></td></tr>
			<?php endfor; endif; ?>
		</table>
		
			<?php if (failedorders): ?>
				<table cellpadding="2" cellspacing="1" width="100%">
                                	<tr class="TableHead"> <td colspan="2">Failed orderids</td></tr>
					<?php unset($this->_sections['idx1']);
$this->_sections['idx1']['name'] = 'idx1';
$this->_sections['idx1']['loop'] = is_array($_loop=$this->_tpl_vars['failedorders']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['idx1']['show'] = true;
$this->_sections['idx1']['max'] = $this->_sections['idx1']['loop'];
$this->_sections['idx1']['step'] = 1;
$this->_sections['idx1']['start'] = $this->_sections['idx1']['step'] > 0 ? 0 : $this->_sections['idx1']['loop']-1;
if ($this->_sections['idx1']['show']) {
    $this->_sections['idx1']['total'] = $this->_sections['idx1']['loop'];
    if ($this->_sections['idx1']['total'] == 0)
        $this->_sections['idx1']['show'] = false;
} else
    $this->_sections['idx1']['total'] = 0;
if ($this->_sections['idx1']['show']):

            for ($this->_sections['idx1']['index'] = $this->_sections['idx1']['start'], $this->_sections['idx1']['iteration'] = 1;
                 $this->_sections['idx1']['iteration'] <= $this->_sections['idx1']['total'];
                 $this->_sections['idx1']['index'] += $this->_sections['idx1']['step'], $this->_sections['idx1']['iteration']++):
$this->_sections['idx1']['rownum'] = $this->_sections['idx1']['iteration'];
$this->_sections['idx1']['index_prev'] = $this->_sections['idx1']['index'] - $this->_sections['idx1']['step'];
$this->_sections['idx1']['index_next'] = $this->_sections['idx1']['index'] + $this->_sections['idx1']['step'];
$this->_sections['idx1']['first']      = ($this->_sections['idx1']['iteration'] == 1);
$this->_sections['idx1']['last']       = ($this->_sections['idx1']['iteration'] == $this->_sections['idx1']['total']);
?>
						<tr<?php echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this);?>
>
                                                	<td align="right"> <input type=hidden name = failedorderids[] value="<?php echo $this->_tpl_vars['failedorders'][$this->_sections['idx1']['index']]['orderid']; ?>
-<?php echo $this->_tpl_vars['failedorders'][$this->_sections['idx1']['index']]['reason']; ?>
"/> <a href="../order.php?orderid=<?php echo $this->_tpl_vars['failedorders'][$this->_sections['idx1']['index']]['orderid']; ?>
"><?php echo $this->_tpl_vars['failedorders'][$this->_sections['idx1']['index']]['orderid']; ?>
</a> </td>
                                                	<td align="left"> <b><?php echo $this->_tpl_vars['failedorders'][$this->_sections['idx1']['index']]['reason']; ?>
</b></td>
                                        	</tr>	
					<?php endfor; endif; ?>
				</table>
			<?php endif; ?>			
	
	<?php endif;  endif;  $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean();  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => 'Upload CSV of RTOs to be queue','content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>