<?php /* Smarty version 2.6.12, created on 2017-03-24 12:58:58
         compiled from main/change_password.tpl */ ?>
<?php func_load_lang($this, "main/change_password.tpl","txt_chpass_msg,lbl_username,lbl_old_password,lbl_new_password,lbl_confirm_password,lbl_chpass"); ?><?php ob_start(); ?>
<form action="change_password.php" method="post">
<table>
<tr>
	<td colspan="3"><?php echo $this->_tpl_vars['lng']['txt_chpass_msg']; ?>
</td>
</tr>
<tr>
	<td colspan="3">&nbsp;</td>
</tr>
<tr>
	<td><?php echo $this->_tpl_vars['lng']['lbl_username']; ?>
:</td>
	<td>&nbsp;</td>
	<td><b><?php echo $this->_tpl_vars['username']; ?>
</b></td>
</tr>
<tr>
	<td><?php echo $this->_tpl_vars['lng']['lbl_old_password']; ?>
:</td>
	<td><font class="Star">*</font></td>
	<td><input type="password" size="30" name="old_password" value="<?php echo $this->_tpl_vars['old_password']; ?>
" /></td>
</tr>
<tr>
	<td><?php echo $this->_tpl_vars['lng']['lbl_new_password']; ?>
:</td>
	<td><font class="Star">*</font></td>
	<td><input type="password" size="30" name="new_password" value="<?php echo $this->_tpl_vars['new_password']; ?>
" /></td>
</tr>
<tr>
	<td><?php echo $this->_tpl_vars['lng']['lbl_confirm_password']; ?>
:</td>
	<td><font class="Star">*</font></td>
	<td><input type="password" size="30" name="confirm_password" value="<?php echo $this->_tpl_vars['confirm_password']; ?>
" /></td>
</tr>
<tr>
	<td colspan="3">&nbsp;</td>
</tr>
<tr>
	<td colspan="3" align="center"><input type="submit" /></td>
</tr>
</table>
</form>
<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_chpass'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>