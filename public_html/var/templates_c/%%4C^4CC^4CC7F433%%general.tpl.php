<?php /* Smarty version 2.6.12, created on 2017-04-04 14:46:35
         compiled from provider/main/general.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'substitute', 'provider/main/general.tpl', 26, false),array('modifier', 'escape', 'provider/main/general.tpl', 27, false),array('function', 'math', 'provider/main/general.tpl', 90, false),)), $this); ?>
<?php func_load_lang($this, "provider/main/general.tpl","lbl_summary,txt_summary_provider_top_text,lbl_general_info,txt_N_products_with_empty_price,lbl_search_products,lbl_click_here_to_check,txt_store_admin_has_disabled_shipping_calculations,txt_have_not_defined_shipping_rates,lbl_shipping_rates,lbl_click_here_to_define,lbl_taxes_info,lbl_tax_rates,lbl_click_here_for_details,lbl_tax,lbl_tax_rates_count,txt_warn_no_tax_rates_defined,lbl_orders_info,lbl_search_orders,lbl_click_here_for_details,lbl_since_last_log_in,lbl_today,lbl_this_week,lbl_this_month,lbl_orders_processed,lbl_orders_queued,lbl_orders_failed,lbl_orders_declined,lbl_orders_not_finished,lbl_shipping_rates_info,lbl_shipping_rates_info,lbl_click_here_to_define,txt_N_shipping_rates_defined,lbl_carrier,lbl_rates_enabled,lbl_user_defined,txt_you_have_defined_shipping_rates_for_the_following_zones,lbl_zone,lbl_rates_enabled,txt_no_shipping_rates,lbl_enabled,lbl_disabled,txt_realtime_shipping_rates_is,lbl_summary"); ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "page_title.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_summary'],'last_url' => "general.php")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php echo $this->_tpl_vars['lng']['txt_summary_provider_top_text']; ?>

<p />
<?php ob_start(); ?>
<table cellpadding="2" cellspacing="0" width="100%">
<tr>
	<td width="10"></td>
	<td width="100%"></td>
</tr>

<tr>
	<td class="TableHead" colspan="2" height="16"><font class="TopLabel "><?php echo $this->_tpl_vars['lng']['lbl_general_info']; ?>
</font></td>
</tr>

<tr>
	<td colspan="2" height="5"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="1" height="5" alt="" /></td>
</tr>


<?php if ($this->_tpl_vars['empty_prices']): ?>
<tr>
	<td colspan="2">
<font class="AdminTitle"><?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_N_products_with_empty_price'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'products', $this->_tpl_vars['empty_prices']) : smarty_modifier_substitute($_tmp, 'products', $this->_tpl_vars['empty_prices'])); ?>
</font>
&nbsp;&nbsp;&nbsp;<a href="search.php" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_search_products'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><?php echo $this->_tpl_vars['lng']['lbl_click_here_to_check']; ?>
 &gt;&gt;</a>
	</td>
</tr>
<?php endif; ?>

<?php if ($this->_tpl_vars['config']['Shipping']['disable_shipping'] == 'Y'): ?>
<tr>
	<td colspan="2">
	<font class="AdminTitle"><?php echo $this->_tpl_vars['lng']['txt_store_admin_has_disabled_shipping_calculations']; ?>
</font>
	</td>
</tr>
<?php endif; ?>

<?php if ($this->_tpl_vars['config']['Shipping']['realtime_shipping'] != 'Y' && $this->_tpl_vars['shipping_rates_count'] <= '0' && $this->_tpl_vars['config']['Shipping']['disable_shipping'] != 'Y'): ?>
<tr>
	<td colspan="2">
	<font class="AdminTitle"><?php echo $this->_tpl_vars['lng']['txt_have_not_defined_shipping_rates']; ?>
</font>
&nbsp;&nbsp;&nbsp;<a href="shipping_rates.php" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_shipping_rates'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><?php echo $this->_tpl_vars['lng']['lbl_click_here_to_define']; ?>
 &gt;&gt;</a>
	</td>
</tr>
<?php endif; ?>

<tr>
	<td colspan="2">&nbsp;</td>
</tr>


<tr>
	<td colspan="2">

<table cellpadding="0" cellspacing="0" width="100%">
<tr>
	<td colspan="2" height="16">
&nbsp;<font class="TopLabel"><?php echo $this->_tpl_vars['lng']['lbl_taxes_info']; ?>
</font>&nbsp;&nbsp;&nbsp;<a href="taxes.php" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_tax_rates'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><?php echo $this->_tpl_vars['lng']['lbl_click_here_for_details']; ?>
 &gt;&gt;</a>
	</td>
</tr>

<tr>
	<td colspan="2" height="3"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="1" height="3" alt="" /></td>
</tr>

<tr>
	<td><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="30" height="1" alt="" /><br /></td>
	<td width="100%">
<table cellpadding="1" cellspacing="2" border="0" width="80%">
<tr>
	<td height="14" class="TableHead" nowrap="nowrap" align="center"><?php echo $this->_tpl_vars['lng']['lbl_tax']; ?>
</td>
	<td height="14" class="TableHead" nowrap="nowrap" align="center"><?php echo $this->_tpl_vars['lng']['lbl_tax_rates_count']; ?>
</td>

</tr>
<?php $this->assign('index', '0'); ?>
<?php unset($this->_sections['idx']);
$this->_sections['idx']['name'] = 'idx';
$this->_sections['idx']['loop'] = is_array($_loop=$this->_tpl_vars['tax_info']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['idx']['show'] = true;
$this->_sections['idx']['max'] = $this->_sections['idx']['loop'];
$this->_sections['idx']['step'] = 1;
$this->_sections['idx']['start'] = $this->_sections['idx']['step'] > 0 ? 0 : $this->_sections['idx']['loop']-1;
if ($this->_sections['idx']['show']) {
    $this->_sections['idx']['total'] = $this->_sections['idx']['loop'];
    if ($this->_sections['idx']['total'] == 0)
        $this->_sections['idx']['show'] = false;
} else
    $this->_sections['idx']['total'] = 0;
if ($this->_sections['idx']['show']):

            for ($this->_sections['idx']['index'] = $this->_sections['idx']['start'], $this->_sections['idx']['iteration'] = 1;
                 $this->_sections['idx']['iteration'] <= $this->_sections['idx']['total'];
                 $this->_sections['idx']['index'] += $this->_sections['idx']['step'], $this->_sections['idx']['iteration']++):
$this->_sections['idx']['rownum'] = $this->_sections['idx']['iteration'];
$this->_sections['idx']['index_prev'] = $this->_sections['idx']['index'] - $this->_sections['idx']['step'];
$this->_sections['idx']['index_next'] = $this->_sections['idx']['index'] + $this->_sections['idx']['step'];
$this->_sections['idx']['first']      = ($this->_sections['idx']['iteration'] == 1);
$this->_sections['idx']['last']       = ($this->_sections['idx']['iteration'] == $this->_sections['idx']['total']);
?>
<tr <?php if (( $this->_tpl_vars['index'] % 2 ) == 0): ?> class="TableLine"<?php endif; ?>>
	<td align="center"><a href="taxes.php?taxid=<?php echo $this->_tpl_vars['tax_info'][$this->_sections['idx']['index']]['taxid']; ?>
"><?php echo $this->_tpl_vars['tax_info'][$this->_sections['idx']['index']]['tax_name']; ?>
</a></td>
	<td align="center">
<?php if ($this->_tpl_vars['tax_info'][$this->_sections['idx']['index']]['count'] == 0): ?>
<font class="AdminTitle"><?php echo $this->_tpl_vars['lng']['txt_warn_no_tax_rates_defined']; ?>
</font>
<?php else: ?>
<?php echo $this->_tpl_vars['tax_info'][$this->_sections['idx']['index']]['count']; ?>

<?php endif; ?>
	</td>
</tr>
<?php echo smarty_function_math(array('equation' => "x+1",'x' => $this->_tpl_vars['index'],'assign' => 'index'), $this);?>

<?php endfor; endif; ?>
</table>

	</td>
</tr>
</table>

	</td>
</tr>

<tr>
	<td colspan="2">&nbsp;</td>
</tr>


<tr>
	<td colspan="2">

<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
	<td colspan="2" height="16">
&nbsp;<font class="TopLabel"><?php echo $this->_tpl_vars['lng']['lbl_orders_info']; ?>
</font>&nbsp;&nbsp;&nbsp;<a href="orders.php" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_search_orders'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><?php echo $this->_tpl_vars['lng']['lbl_click_here_for_details']; ?>
 &gt;&gt;</a>
	</td>
</tr>

<tr>
	<td colspan="2" height="3"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="1" height="3" alt="" /></td>
</tr>

<tr>
	<td><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="30" height="1" alt="" /><br /></td>
	<td width="100%">
<table cellpadding="1" cellspacing="2" width="80%">
<tr>
	<td height="14" class="TableHead">&nbsp;</td>
	<td height="14" class="TableHead" nowrap="nowrap" align="center"><?php echo $this->_tpl_vars['lng']['lbl_since_last_log_in']; ?>
</td>
	<td height="14" class="TableHead" align="center"><?php echo $this->_tpl_vars['lng']['lbl_today']; ?>
</td>
	<td height="14" class="TableHead" nowrap="nowrap" align="center"><?php echo $this->_tpl_vars['lng']['lbl_this_week']; ?>
</td>
	<td height="14" class="TableHead" nowrap="nowrap" align="center"><?php echo $this->_tpl_vars['lng']['lbl_this_month']; ?>
</td>

</tr>
<?php $this->assign('index', '0'); ?>
<?php $_from = $this->_tpl_vars['orders']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
<tr <?php if (( $this->_tpl_vars['index'] % 2 ) == 0): ?> class="TableLine"<?php endif; ?>>
	<td nowrap="nowrap"><?php if ($this->_tpl_vars['key'] == 'P'):  echo $this->_tpl_vars['lng']['lbl_orders_processed'];  elseif ($this->_tpl_vars['key'] == 'Q'):  echo $this->_tpl_vars['lng']['lbl_orders_queued'];  elseif ($this->_tpl_vars['key'] == 'F'):  echo $this->_tpl_vars['lng']['lbl_orders_failed'];  elseif ($this->_tpl_vars['key'] == 'D'):  echo $this->_tpl_vars['lng']['lbl_orders_declined'];  elseif ($this->_tpl_vars['key'] == 'I'):  echo $this->_tpl_vars['lng']['lbl_orders_not_finished'];  endif; ?>:</td>
<?php unset($this->_sections['period']);
$this->_sections['period']['name'] = 'period';
$this->_sections['period']['loop'] = is_array($_loop=$this->_tpl_vars['item']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['period']['show'] = true;
$this->_sections['period']['max'] = $this->_sections['period']['loop'];
$this->_sections['period']['step'] = 1;
$this->_sections['period']['start'] = $this->_sections['period']['step'] > 0 ? 0 : $this->_sections['period']['loop']-1;
if ($this->_sections['period']['show']) {
    $this->_sections['period']['total'] = $this->_sections['period']['loop'];
    if ($this->_sections['period']['total'] == 0)
        $this->_sections['period']['show'] = false;
} else
    $this->_sections['period']['total'] = 0;
if ($this->_sections['period']['show']):

            for ($this->_sections['period']['index'] = $this->_sections['period']['start'], $this->_sections['period']['iteration'] = 1;
                 $this->_sections['period']['iteration'] <= $this->_sections['period']['total'];
                 $this->_sections['period']['index'] += $this->_sections['period']['step'], $this->_sections['period']['iteration']++):
$this->_sections['period']['rownum'] = $this->_sections['period']['iteration'];
$this->_sections['period']['index_prev'] = $this->_sections['period']['index'] - $this->_sections['period']['step'];
$this->_sections['period']['index_next'] = $this->_sections['period']['index'] + $this->_sections['period']['step'];
$this->_sections['period']['first']      = ($this->_sections['period']['iteration'] == 1);
$this->_sections['period']['last']       = ($this->_sections['period']['iteration'] == $this->_sections['period']['total']);
?>
	<td align="center"><?php echo $this->_tpl_vars['item'][$this->_sections['period']['index']]; ?>
</td>
<?php endfor; endif; ?>
</tr>
<?php echo smarty_function_math(array('equation' => "x+1",'x' => $this->_tpl_vars['index'],'assign' => 'index'), $this);?>

<?php endforeach; endif; unset($_from); ?>
</table>

	</td>
</tr>
</table>

	</td>
</tr>

<tr>
	<td colspan="2">&nbsp;</td>
</tr>

<tr>
	<td colspan="2">

<table cellpadding="0" cellspacing="0" width="100%">
<tr>
	<td colspan="2" height="16">
&nbsp;<font class="TopLabel"><?php echo $this->_tpl_vars['lng']['lbl_shipping_rates_info']; ?>
</font>&nbsp;&nbsp;&nbsp;<a href="shipping_rates.php" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_shipping_rates_info'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><?php echo $this->_tpl_vars['lng']['lbl_click_here_to_define']; ?>
 &gt;&gt;</a>
</td>
</tr>

<tr>
	<td colspan="2" height="3"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="1" height="3" alt="" /></td>
</tr>

<tr>
	<td><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="30" height="1" alt="" /><br /></td>
	<td width="100%">
<?php if ($this->_tpl_vars['shipping_rates_count'] > '0'): ?>
<font class="Text">
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_N_shipping_rates_defined'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'count', $this->_tpl_vars['shipping_rates_count']) : smarty_modifier_substitute($_tmp, 'count', $this->_tpl_vars['shipping_rates_count'])); ?>
:
</font>
<br />
<table cellpadding="1" cellspacing="2" border="0" width="80%">
<tr>
	<td height="14" class="TableHead"><?php echo $this->_tpl_vars['lng']['lbl_carrier']; ?>
</td>
	<td height="14" class="TableHead" nowrap="nowrap" align="center"><?php echo $this->_tpl_vars['lng']['lbl_rates_enabled']; ?>
</td>
</tr>
<?php unset($this->_sections['idx']);
$this->_sections['idx']['name'] = 'idx';
$this->_sections['idx']['loop'] = is_array($_loop=$this->_tpl_vars['shipping_rates_enabled']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['idx']['show'] = true;
$this->_sections['idx']['max'] = $this->_sections['idx']['loop'];
$this->_sections['idx']['step'] = 1;
$this->_sections['idx']['start'] = $this->_sections['idx']['step'] > 0 ? 0 : $this->_sections['idx']['loop']-1;
if ($this->_sections['idx']['show']) {
    $this->_sections['idx']['total'] = $this->_sections['idx']['loop'];
    if ($this->_sections['idx']['total'] == 0)
        $this->_sections['idx']['show'] = false;
} else
    $this->_sections['idx']['total'] = 0;
if ($this->_sections['idx']['show']):

            for ($this->_sections['idx']['index'] = $this->_sections['idx']['start'], $this->_sections['idx']['iteration'] = 1;
                 $this->_sections['idx']['iteration'] <= $this->_sections['idx']['total'];
                 $this->_sections['idx']['index'] += $this->_sections['idx']['step'], $this->_sections['idx']['iteration']++):
$this->_sections['idx']['rownum'] = $this->_sections['idx']['iteration'];
$this->_sections['idx']['index_prev'] = $this->_sections['idx']['index'] - $this->_sections['idx']['step'];
$this->_sections['idx']['index_next'] = $this->_sections['idx']['index'] + $this->_sections['idx']['step'];
$this->_sections['idx']['first']      = ($this->_sections['idx']['iteration'] == 1);
$this->_sections['idx']['last']       = ($this->_sections['idx']['iteration'] == $this->_sections['idx']['total']);
?>
<tr>
	<td>
<?php if ($this->_tpl_vars['shipping_rates_enabled'][$this->_sections['idx']['index']]['code'] == 'FDX'): ?>FedEx<?php elseif ($this->_tpl_vars['shipping_rates_enabled'][$this->_sections['idx']['index']]['code'] == 'UPS'): ?>UPS<?php elseif ($this->_tpl_vars['shipping_rates_enabled'][$this->_sections['idx']['index']]['code'] == 'USPS'): ?>U.S.P.S.<?php elseif ($this->_tpl_vars['shipping_rates_enabled'][$this->_sections['idx']['index']]['code'] == 'DHL'): ?>DHL<?php elseif ($this->_tpl_vars['shipping_rates_enabled'][$this->_sections['idx']['index']]['code'] == 'ABX'): ?>Airborne<?php elseif ($this->_tpl_vars['shipping_rates_enabled'][$this->_sections['idx']['index']]['code'] == 'EWW'): ?>Emery Worldwide<?php elseif ($this->_tpl_vars['shipping_rates_enabled'][$this->_sections['idx']['index']]['code'] == 'ANX'): ?>AirNet Express<?php elseif ($this->_tpl_vars['shipping_rates_enabled'][$this->_sections['idx']['index']]['code']):  echo $this->_tpl_vars['shipping_rates_enabled'][$this->_sections['idx']['index']]['code'];  else:  echo $this->_tpl_vars['lng']['lbl_user_defined'];  endif; ?>
	</td>
	<td align="center"><?php echo $this->_tpl_vars['shipping_rates_enabled'][$this->_sections['idx']['index']]['count']; ?>
</td>
</tr>
<?php endfor; endif; ?>

<tr>
	<td colspan="2"><br /></td>
</tr>
<tr>
	<td colspan="2" width="100%"><font class="Text"><?php echo $this->_tpl_vars['lng']['txt_you_have_defined_shipping_rates_for_the_following_zones']; ?>
:</font><br /></td>
</tr>

<tr>
	<td height="14" class="TableHead"><?php echo $this->_tpl_vars['lng']['lbl_zone']; ?>
</td>
	<td height="14" class="TableHead" nowrap="nowrap" align="center"><?php echo $this->_tpl_vars['lng']['lbl_rates_enabled']; ?>
</td>
</tr>
<?php unset($this->_sections['idx']);
$this->_sections['idx']['name'] = 'idx';
$this->_sections['idx']['loop'] = is_array($_loop=$this->_tpl_vars['zone_rates']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['idx']['show'] = true;
$this->_sections['idx']['max'] = $this->_sections['idx']['loop'];
$this->_sections['idx']['step'] = 1;
$this->_sections['idx']['start'] = $this->_sections['idx']['step'] > 0 ? 0 : $this->_sections['idx']['loop']-1;
if ($this->_sections['idx']['show']) {
    $this->_sections['idx']['total'] = $this->_sections['idx']['loop'];
    if ($this->_sections['idx']['total'] == 0)
        $this->_sections['idx']['show'] = false;
} else
    $this->_sections['idx']['total'] = 0;
if ($this->_sections['idx']['show']):

            for ($this->_sections['idx']['index'] = $this->_sections['idx']['start'], $this->_sections['idx']['iteration'] = 1;
                 $this->_sections['idx']['iteration'] <= $this->_sections['idx']['total'];
                 $this->_sections['idx']['index'] += $this->_sections['idx']['step'], $this->_sections['idx']['iteration']++):
$this->_sections['idx']['rownum'] = $this->_sections['idx']['iteration'];
$this->_sections['idx']['index_prev'] = $this->_sections['idx']['index'] - $this->_sections['idx']['step'];
$this->_sections['idx']['index_next'] = $this->_sections['idx']['index'] + $this->_sections['idx']['step'];
$this->_sections['idx']['first']      = ($this->_sections['idx']['iteration'] == 1);
$this->_sections['idx']['last']       = ($this->_sections['idx']['iteration'] == $this->_sections['idx']['total']);
?>
<tr>
	<td><?php echo $this->_tpl_vars['zone_rates'][$this->_sections['idx']['index']]['name']; ?>
</td>
	<td align="center"><?php echo $this->_tpl_vars['zone_rates'][$this->_sections['idx']['index']]['count']; ?>
</td>
</tr>
<?php endfor; endif; ?>
</table>
<?php else: ?>
<font class="AdminTitle"><?php echo $this->_tpl_vars['lng']['txt_no_shipping_rates']; ?>
</font>
<?php endif; ?>
	</td>
</tr>

</table>
	</td>
</tr>

<tr>
	<td><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="30" height="1" alt="" /><br /></td>
	<td width="100%"><font class="Text">
<?php if ($this->_tpl_vars['config']['Shipping']['realtime_shipping'] == 'Y'): ?>
<?php $this->assign('flag', $this->_tpl_vars['lng']['lbl_enabled']); ?>
<?php else: ?>
<?php $this->assign('flag', $this->_tpl_vars['lng']['lbl_disabled']); ?>
<?php endif; ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_realtime_shipping_rates_is'])) ? $this->_run_mod_handler('substitute', true, $_tmp, 'flag', $this->_tpl_vars['flag']) : smarty_modifier_substitute($_tmp, 'flag', $this->_tpl_vars['flag'])); ?>

	</font>
	</td>
</tr>

<tr>
	<td colspan="2"><br /><br /></td>
</tr>

</table>
<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_summary'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>