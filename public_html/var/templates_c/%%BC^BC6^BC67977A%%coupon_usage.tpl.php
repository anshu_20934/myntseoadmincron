<?php /* Smarty version 2.6.12, created on 2017-03-28 17:32:10
         compiled from modules/Discount_Coupons/coupon_usage.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'modules/Discount_Coupons/coupon_usage.tpl', 33, false),)), $this); ?>
<h3>Coupon details:</h3>
<table border="1" cellpadding="3">
	<tr bgcolor="gray">
		<td>COUPON</td>
		<td>TIMES LOCKED</td>
		<td>TIMES USED</td>
		<td>MAX. USAGE</td>
		<td>SUBTOTAL</td>
		<td>DISCOUNT</td>
	</tr>
	<tr>
		<td><?php echo $this->_tpl_vars['coupon_data']['coupon']; ?>
</td>
		<td><?php echo $this->_tpl_vars['coupon_data']['times_locked']; ?>
</td>
		<td><?php echo $this->_tpl_vars['coupon_data']['times_used']; ?>
</td>
		<td><?php if ($this->_tpl_vars['coupon_data']['isInfinite'] == 1): ?>inf.<?php else:  echo $this->_tpl_vars['coupon_data']['times'];  endif; ?></td>
		<td><?php echo $this->_tpl_vars['coupon_data']['subtotal']; ?>
</td>
		<td><?php echo $this->_tpl_vars['coupon_data']['discountOffered']; ?>
</td>
	</tr>
</table>

&nbsp;

<h3>Coupon locks:</h3>
<table border="1" cellpadding="3">
	<tr bgcolor="gray">
		<td>TIMESTAMP</td>
		<td>ORDER ID</td>
		<td>USER ID</td>
		<td>ORDER STATUS</td>
	</tr>
	<?php unset($this->_sections['prod_num']);
$this->_sections['prod_num']['name'] = 'prod_num';
$this->_sections['prod_num']['loop'] = is_array($_loop=$this->_tpl_vars['lock_orders']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['prod_num']['show'] = true;
$this->_sections['prod_num']['max'] = $this->_sections['prod_num']['loop'];
$this->_sections['prod_num']['step'] = 1;
$this->_sections['prod_num']['start'] = $this->_sections['prod_num']['step'] > 0 ? 0 : $this->_sections['prod_num']['loop']-1;
if ($this->_sections['prod_num']['show']) {
    $this->_sections['prod_num']['total'] = $this->_sections['prod_num']['loop'];
    if ($this->_sections['prod_num']['total'] == 0)
        $this->_sections['prod_num']['show'] = false;
} else
    $this->_sections['prod_num']['total'] = 0;
if ($this->_sections['prod_num']['show']):

            for ($this->_sections['prod_num']['index'] = $this->_sections['prod_num']['start'], $this->_sections['prod_num']['iteration'] = 1;
                 $this->_sections['prod_num']['iteration'] <= $this->_sections['prod_num']['total'];
                 $this->_sections['prod_num']['index'] += $this->_sections['prod_num']['step'], $this->_sections['prod_num']['iteration']++):
$this->_sections['prod_num']['rownum'] = $this->_sections['prod_num']['iteration'];
$this->_sections['prod_num']['index_prev'] = $this->_sections['prod_num']['index'] - $this->_sections['prod_num']['step'];
$this->_sections['prod_num']['index_next'] = $this->_sections['prod_num']['index'] + $this->_sections['prod_num']['step'];
$this->_sections['prod_num']['first']      = ($this->_sections['prod_num']['iteration'] == 1);
$this->_sections['prod_num']['last']       = ($this->_sections['prod_num']['iteration'] == $this->_sections['prod_num']['total']);
?>
		<tr>
			<td><?php echo ((is_array($_tmp=$this->_tpl_vars['lock_orders'][$this->_sections['prod_num']['index']]['date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['config']['Appearance']['datetime_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['config']['Appearance']['datetime_format'])); ?>
</td>
			<td><?php echo $this->_tpl_vars['lock_orders'][$this->_sections['prod_num']['index']]['orderid']; ?>
</td>
			<td><?php echo $this->_tpl_vars['lock_orders'][$this->_sections['prod_num']['index']]['login']; ?>
</td>
			<td><?php echo $this->_tpl_vars['lock_orders'][$this->_sections['prod_num']['index']]['status']; ?>
</td>
		</tr>
	<?php endfor; endif; ?>
</table>

&nbsp;

<h3>Coupon usage:</h3>
<table border="1" cellpadding="3">
	<tr bgcolor="gray">
		<td>TIMESTAMP</td>
		<td>ORDER ID</td>
		<td>USER ID</td>
		<td>ORDER STATUS</td>
	</tr>
	<?php unset($this->_sections['prod_num']);
$this->_sections['prod_num']['name'] = 'prod_num';
$this->_sections['prod_num']['loop'] = is_array($_loop=$this->_tpl_vars['usage_orders']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['prod_num']['show'] = true;
$this->_sections['prod_num']['max'] = $this->_sections['prod_num']['loop'];
$this->_sections['prod_num']['step'] = 1;
$this->_sections['prod_num']['start'] = $this->_sections['prod_num']['step'] > 0 ? 0 : $this->_sections['prod_num']['loop']-1;
if ($this->_sections['prod_num']['show']) {
    $this->_sections['prod_num']['total'] = $this->_sections['prod_num']['loop'];
    if ($this->_sections['prod_num']['total'] == 0)
        $this->_sections['prod_num']['show'] = false;
} else
    $this->_sections['prod_num']['total'] = 0;
if ($this->_sections['prod_num']['show']):

            for ($this->_sections['prod_num']['index'] = $this->_sections['prod_num']['start'], $this->_sections['prod_num']['iteration'] = 1;
                 $this->_sections['prod_num']['iteration'] <= $this->_sections['prod_num']['total'];
                 $this->_sections['prod_num']['index'] += $this->_sections['prod_num']['step'], $this->_sections['prod_num']['iteration']++):
$this->_sections['prod_num']['rownum'] = $this->_sections['prod_num']['iteration'];
$this->_sections['prod_num']['index_prev'] = $this->_sections['prod_num']['index'] - $this->_sections['prod_num']['step'];
$this->_sections['prod_num']['index_next'] = $this->_sections['prod_num']['index'] + $this->_sections['prod_num']['step'];
$this->_sections['prod_num']['first']      = ($this->_sections['prod_num']['iteration'] == 1);
$this->_sections['prod_num']['last']       = ($this->_sections['prod_num']['iteration'] == $this->_sections['prod_num']['total']);
?>
		<tr>
			<td><?php echo ((is_array($_tmp=$this->_tpl_vars['usage_orders'][$this->_sections['prod_num']['index']]['date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['config']['Appearance']['datetime_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['config']['Appearance']['datetime_format'])); ?>
</td>
			<td><?php echo $this->_tpl_vars['usage_orders'][$this->_sections['prod_num']['index']]['orderid']; ?>
</td>
			<td><?php echo $this->_tpl_vars['usage_orders'][$this->_sections['prod_num']['index']]['login']; ?>
</td>
			<td><?php echo $this->_tpl_vars['usage_orders'][$this->_sections['prod_num']['index']]['status']; ?>
</td>
		</tr>
	<?php endfor; endif; ?>
</table>