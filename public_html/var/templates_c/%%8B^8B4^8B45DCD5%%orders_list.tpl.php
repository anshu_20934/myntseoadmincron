<?php /* Smarty version 2.6.12, created on 2017-06-02 16:19:48
         compiled from main/orders_list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'math', 'main/orders_list.tpl', 68, false),array('function', 'cycle', 'main/orders_list.tpl', 84, false),array('modifier', 'date_format', 'main/orders_list.tpl', 131, false),array('modifier', 'escape', 'main/orders_list.tpl', 169, false),array('modifier', 'strip_tags', 'main/orders_list.tpl', 182, false),)), $this); ?>
<?php func_load_lang($this, "main/orders_list.tpl","lbl_search_again,lbl_status,lbl_payment_mode,lbl_customer,lbl_qtyInOrder,lbl_date,lbl_payment_status,lbl_total,lbl_gross_total,lbl_total_paid,lbl_update_status,lbl_invoices_for_selected,lbl_upd_selected,lbl_labels_for_selected,lbl_delete_selected,txt_delete_selected_orders_warning,lbl_get_shipping_labels,lbl_export_orders,txt_export_all_found_orders_text,lbl_export_file_format,lbl_standart,lbl_40x_compatible,lbl_with_tab_delimiter,lbl_40x_compatible,lbl_with_semicolon_delimiter,lbl_40x_compatible,lbl_with_comma_delimiter,lbl_export,lbl_export_all_found,lbl_search_results"); ?>





<?php if ($this->_tpl_vars['usertype'] == 'A' || $this->_tpl_vars['usertype'] == 'P'):  if ($this->_tpl_vars['mode'] == 'search'): ?>

<?php $this->assign('total', 0.00);  $this->assign('total_paid', 0.00); ?>

<?php ob_start(); ?>

<div align="right"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/button.tpl", 'smarty_include_vars' => array('button_title' => $this->_tpl_vars['lng']['lbl_search_again'],'href' => "orders.php")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "customer/main/navigation.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/check_all_row.tpl", 'smarty_include_vars' => array('form' => 'processorderform','prefix' => 'orderids')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<form action="process_order.php" method="post" name="processorderform">
<input type="hidden" name="mode" value="" />


<table cellpadding="2" cellspacing="1" width="100%" >

<?php $this->assign('colspan', 6); ?>

<tr class="TableHead"  >
	<td width="5">&nbsp;</td>
	<td width="5%" nowrap="nowrap"><?php if ($this->_tpl_vars['search_prefilled']['sort_field'] == 'orderid'):  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/sort_pointer.tpl", 'smarty_include_vars' => array('dir' => $this->_tpl_vars['search_prefilled']['sort_direction'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>&nbsp;<?php endif; ?><a href="orders.php?mode=search&amp;sort=group_id">Order ID</a></td>
	<td width="5%" nowrap="nowrap"><?php if ($this->_tpl_vars['search_prefilled']['sort_field'] == 'orderid'):  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/sort_pointer.tpl", 'smarty_include_vars' => array('dir' => $this->_tpl_vars['search_prefilled']['sort_direction'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>&nbsp;<?php endif; ?><a href="orders.php?mode=search&amp;sort=orderid">Shipment ID</a></td>
	<td width="3%" nowrap="nowrap"><?php if ($this->_tpl_vars['search_prefilled']['sort_field'] == 'status'):  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/sort_pointer.tpl", 'smarty_include_vars' => array('dir' => $this->_tpl_vars['search_prefilled']['sort_direction'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>&nbsp;<?php endif; ?><a href="orders.php?mode=search&amp;sort=status"><?php echo $this->_tpl_vars['lng']['lbl_status']; ?>
</a></td>
	<td width="5%" nowrap="nowrap"><?php if ($this->_tpl_vars['search_prefilled']['sort_field'] == 'status'):  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/sort_pointer.tpl", 'smarty_include_vars' => array('dir' => $this->_tpl_vars['search_prefilled']['sort_direction'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>&nbsp;<?php endif; ?><a href="orders.php?mode=search&amp;sort=status"><?php echo $this->_tpl_vars['lng']['lbl_payment_mode']; ?>
</a></td>
	<td width="30%" nowrap="nowrap"><?php if ($this->_tpl_vars['search_prefilled']['sort_field'] == 'customer'):  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/sort_pointer.tpl", 'smarty_include_vars' => array('dir' => $this->_tpl_vars['search_prefilled']['sort_direction'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>&nbsp;<?php endif; ?><a href="orders.php?mode=search&amp;sort=customer"><?php echo $this->_tpl_vars['lng']['lbl_customer']; ?>
</a></td>
<?php if ($this->_tpl_vars['usertype'] == 'A' && $this->_tpl_vars['single_mode'] == ""):  $this->assign('colspan', 7); ?>
	<td width="30%" nowrap="nowrap"><?php if ($this->_tpl_vars['search_prefilled']['sort_field'] == 'contact'):  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/sort_pointer.tpl", 'smarty_include_vars' => array('dir' => $this->_tpl_vars['search_prefilled']['sort_direction'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>&nbsp;<?php endif; ?><a href="orders.php?mode=search&amp;sort=contact">Contact No.</a></td>
	<td width="5%" nowrap="nowrap">
	<?php if ($this->_tpl_vars['search_prefilled']['sort_field'] == 'qtyInOrder'): ?>
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/sort_pointer.tpl", 'smarty_include_vars' => array('dir' => $this->_tpl_vars['search_prefilled']['sort_direction'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>&nbsp;<?php endif; ?>
			<a href="orders.php?mode=search&amp;sort=qtyInOrder"><?php echo $this->_tpl_vars['lng']['lbl_qtyInOrder']; ?>
</a></td>
  	<?php endif; ?>
<td width="5%" nowrap="nowrap"><?php if ($this->_tpl_vars['search_prefilled']['sort_field'] == 'gift_status'):  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/sort_pointer.tpl", 'smarty_include_vars' => array('dir' => $this->_tpl_vars['search_prefilled']['sort_direction'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>&nbsp;<?php endif; ?><a href="orders.php?mode=search&amp;sort=gift_status">Gift</a></td>
<td width="20%" nowrap="nowrap"><?php if ($this->_tpl_vars['search_prefilled']['sort_field'] == 'date'):  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/sort_pointer.tpl", 'smarty_include_vars' => array('dir' => $this->_tpl_vars['search_prefilled']['sort_direction'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>&nbsp;<?php endif; ?><a href="orders.php?mode=search&amp;sort=date"><?php echo $this->_tpl_vars['lng']['lbl_date']; ?>
</a></td>
<td width="20%" nowrap="nowrap"><?php if ($this->_tpl_vars['search_prefilled']['sort_field'] == 'date'):  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/sort_pointer.tpl", 'smarty_include_vars' => array('dir' => $this->_tpl_vars['search_prefilled']['sort_direction'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>&nbsp;<?php endif; ?><a href="orders.php?mode=search&amp;sort=date">Queued Date</a></td>
<td width="5%" nowrap="nowrap"><?php if ($this->_tpl_vars['search_prefilled']['sort_field'] == 'date'):  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/sort_pointer.tpl", 'smarty_include_vars' => array('dir' => $this->_tpl_vars['search_prefilled']['sort_direction'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>&nbsp;<?php endif; ?><a href="orders.php?mode=search&amp;sort=date"><?php echo $this->_tpl_vars['lng']['lbl_payment_status']; ?>
</a></td>
<td width="10%" align="right" nowrap="nowrap"><?php if ($this->_tpl_vars['search_prefilled']['sort_field'] == 'total'):  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "buttons/sort_pointer.tpl", 'smarty_include_vars' => array('dir' => $this->_tpl_vars['search_prefilled']['sort_direction'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>&nbsp;<?php endif; ?><a href="orders.php?mode=search&amp;sort=total"><?php echo $this->_tpl_vars['lng']['lbl_total']; ?>
</a></td>
</tr>
<?php if ($this->_tpl_vars['orders']):  unset($this->_sections['oid']);
$this->_sections['oid']['name'] = 'oid';
$this->_sections['oid']['loop'] = is_array($_loop=$this->_tpl_vars['orders']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['oid']['show'] = true;
$this->_sections['oid']['max'] = $this->_sections['oid']['loop'];
$this->_sections['oid']['step'] = 1;
$this->_sections['oid']['start'] = $this->_sections['oid']['step'] > 0 ? 0 : $this->_sections['oid']['loop']-1;
if ($this->_sections['oid']['show']) {
    $this->_sections['oid']['total'] = $this->_sections['oid']['loop'];
    if ($this->_sections['oid']['total'] == 0)
        $this->_sections['oid']['show'] = false;
} else
    $this->_sections['oid']['total'] = 0;
if ($this->_sections['oid']['show']):

            for ($this->_sections['oid']['index'] = $this->_sections['oid']['start'], $this->_sections['oid']['iteration'] = 1;
                 $this->_sections['oid']['iteration'] <= $this->_sections['oid']['total'];
                 $this->_sections['oid']['index'] += $this->_sections['oid']['step'], $this->_sections['oid']['iteration']++):
$this->_sections['oid']['rownum'] = $this->_sections['oid']['iteration'];
$this->_sections['oid']['index_prev'] = $this->_sections['oid']['index'] - $this->_sections['oid']['step'];
$this->_sections['oid']['index_next'] = $this->_sections['oid']['index'] + $this->_sections['oid']['step'];
$this->_sections['oid']['first']      = ($this->_sections['oid']['iteration'] == 1);
$this->_sections['oid']['last']       = ($this->_sections['oid']['iteration'] == $this->_sections['oid']['total']);
?>

<?php $this->assign('amount', $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['total']);  $this->assign('shipcost', $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['shipping_cost']);  $this->assign('ref_discount', $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['ref_discount']);  $this->assign('gift_charges', $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['gift_charges']);  $this->assign('cod', $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['cod']);  $this->assign('amounttobepaid', $this->_tpl_vars['amount']+$this->_tpl_vars['cod']);  $this->assign('amounttobepaid', $this->_tpl_vars['amounttobepaid']-$this->_tpl_vars['ref_discount']);  $this->assign('codneedscsverification', 0); ?>


<?php echo smarty_function_math(array('equation' => "x + ordertotal",'x' => $this->_tpl_vars['total'],'ordertotal' => $this->_tpl_vars['amounttobepaid'],'assign' => 'total'), $this);?>

<?php if ($this->_tpl_vars['orders'][$this->_sections['oid']['index']]['status'] == 'P' || $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['status'] == 'C'):  echo smarty_function_math(array('equation' => "x + ordertotal",'x' => $this->_tpl_vars['total_paid'],'ordertotal' => $this->_tpl_vars['amounttobepaid'],'assign' => 'total_paid'), $this);?>

<?php endif; ?>

<?php if ($this->_tpl_vars['orders'][$this->_sections['oid']['index']]['payment_method'] == 'cod' && $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['cod_needs_cs_verification'] == 1): ?> 
<?php $this->assign('codneedscsverification', 1);  endif; ?>

<?php if ($this->_tpl_vars['orders'][$this->_sections['oid']['index']]['cod_onhold_title_text']): ?>
	<?php $this->assign('codonholdalttext', $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['cod_onhold_title_text']);  else: ?>
	<?php $this->assign('codonholdalttext', '');  endif; ?>

<?php if ($this->_tpl_vars['orders'][$this->_sections['oid']['index']]['cod_onhold_title_text']): ?>
<tr style="background-color:white" <?php if ($this->_tpl_vars['orders'][$this->_sections['oid']['index']]['product_style'] && ( $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['product_style'] == '1092' || $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['product_style'] == '1093' )): ?>style="background-color:red;font-weight:normal;"<?php else:  echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this); endif; ?>>
<?php elseif ($this->_tpl_vars['orders'][$this->_sections['oid']['index']]['payment_method'] == 'cod'): ?>
<tr style="background-color:#FFCC33" <?php if ($this->_tpl_vars['orders'][$this->_sections['oid']['index']]['product_style'] && ( $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['product_style'] == '1092' || $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['product_style'] == '1093' )): ?>style="background-color:red;font-weight:normal;"<?php else:  echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this); endif; ?>>
<?php else: ?>
<tr <?php if ($this->_tpl_vars['orders'][$this->_sections['oid']['index']]['product_style'] && ( $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['product_style'] == '1092' || $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['product_style'] == '1093' )): ?>style="background-color:red;font-weight:normal;"<?php else:  echo smarty_function_cycle(array('values' => ", class='TableSubHead'"), $this); endif; ?>>
<?php endif; ?>
	<td width="5"><input type="checkbox" name="orderids[<?php echo $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['orderid']; ?>
]" /></td>
	<td>
    <a href="order.php?orderid=<?php echo $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['group_id']; ?>
" target="_blank">
		#<?php echo $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['group_id']; ?>

	</a>
	</td>
	<td>
    <?php if ($this->_tpl_vars['orders'][$this->_sections['oid']['index']]['is_customizable'] == '0'): ?><span style="background-color:red;font-weight:bold;padding-left:4px;padding-right:4px;">NP</span><?php endif; ?>
    <a href="order.php?orderid=<?php echo $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['orderid']; ?>
" target="_blank" <?php echo $this->_tpl_vars['codonholdalttext']; ?>
 >
        #<?php echo $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['orderid']; ?>

    </a>
    </td>
	<td nowrap="nowrap" align="center">

<?php if ($this->_tpl_vars['usertype'] == 'A' || ( $this->_tpl_vars['usertype'] == 'P' && $this->_tpl_vars['active_modules']['Simple_Module'] != "" )): ?>
<!--<input type="hidden" name="order_status_old[<?php echo $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['orderid']; ?>
]" value="<?php echo $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['status']; ?>
" />-->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/order_status.tpl", 'smarty_include_vars' => array('status' => $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['status'],'mode' => 'static')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
  else: ?>
<a href="order.php?orderid=<?php echo $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['orderid']; ?>
" <?php echo $this->_tpl_vars['codonholdalttext']; ?>
>
	<b><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/order_status.tpl", 'smarty_include_vars' => array('status' => $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['status'],'mode' => 'static')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></b></a>
<?php endif; ?>

<?php if ($this->_tpl_vars['active_modules']['Stop_List'] != '' && $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['blocked'] == 'Y'): ?>
<img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/no_ip.gif" style="vertical-align: middle;" alt="" />
<?php endif; ?>
	</td>
	<td align="center">
	<?php if ($this->_tpl_vars['orders'][$this->_sections['oid']['index']]['payment_method'] == 'chq'): ?> Check
		<?php elseif ($this->_tpl_vars['orders'][$this->_sections['oid']['index']]['payment_method'] == 'cod'): ?> Cash
		<?php elseif ($this->_tpl_vars['orders'][$this->_sections['oid']['index']]['payment_method'] == 'cash'): ?> Cash
		<?php elseif ($this->_tpl_vars['orders'][$this->_sections['oid']['index']]['payment_method'] == 'inm'): ?> Invoice Me
		<?php else: ?> CC/Net Banking
	<?php endif; ?>
	</td>
	<td><?php echo $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['firstname']; ?>
 <?php echo $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['lastname']; ?>
 (<?php echo $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['login']; ?>
)</td>
	<td align="center"><?php echo $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['issues_contact_number']; ?>
</td>
<?php if ($this->_tpl_vars['usertype'] == 'A' && $this->_tpl_vars['single_mode'] == ""): ?>
	<td align="center"><?php echo $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['qtyInOrder']; ?>
</td>
<?php endif; ?>
	<td nowrap="nowrap" align="center"><a href="order.php?orderid=<?php echo $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['orderid']; ?>
" <?php echo $this->_tpl_vars['codonholdalttext']; ?>
><?php echo $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['gift_status']; ?>
</a></td>
	<td nowrap="nowrap" align="center"><a href="order.php?orderid=<?php echo $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['orderid']; ?>
" <?php echo $this->_tpl_vars['codonholdalttext']; ?>
><?php echo ((is_array($_tmp=$this->_tpl_vars['orders'][$this->_sections['oid']['index']]['date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['config']['Appearance']['datetime_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['config']['Appearance']['datetime_format'])); ?>
</a></td>
	<td nowrap="nowrap" align="center">
		<?php if ($this->_tpl_vars['codneedscsverification'] == 1): ?> 
			Pending Customer Care Verification
		<?php else: ?>
			<a href="order.php?orderid=<?php echo $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['orderid']; ?>
" <?php echo $this->_tpl_vars['codonholdalttext']; ?>
><?php echo ((is_array($_tmp=$this->_tpl_vars['orders'][$this->_sections['oid']['index']]['queueddate'])) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['config']['Appearance']['datetime_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['config']['Appearance']['datetime_format'])); ?>
</a>
		<?php endif; ?>
	</td>
	<td align="center"><input type="checkbox" name="ccavenue[<?php echo $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['orderid']; ?>
]"  <?php if ($this->_tpl_vars['orders'][$this->_sections['oid']['index']]['ccavenue'] == 'Y'): ?> checked='checked'<?php endif; ?>/></td>
	<td nowrap="nowrap" align="right">
	<a href="order.php?orderid=<?php echo $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['orderid']; ?>
" <?php echo $this->_tpl_vars['codonholdalttext']; ?>
><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "currency.tpl", 'smarty_include_vars' => array('value' => $this->_tpl_vars['amounttobepaid'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></a>
	</td>
</tr>
<input type="hidden" name="order_email[<?php echo $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['orderid']; ?>
]" value="<?php echo $this->_tpl_vars['orders'][$this->_sections['oid']['index']]['email']; ?>
" />
<?php endfor; endif; ?>
<tr>
	<td colspan="8"><img src="<?php echo $this->_tpl_vars['cdn_base']; ?>
/skin1/images/spacer.gif" width="100%" height="1" alt="" /></td>
</tr>

<tr>
	<td colspan="8" align="right"><?php echo $this->_tpl_vars['lng']['lbl_gross_total']; ?>
: <b><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "currency.tpl", 'smarty_include_vars' => array('value' => $this->_tpl_vars['total'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></b></td>
</tr>

<tr>
	<td colspan="8" align="right"><?php echo $this->_tpl_vars['lng']['lbl_total_paid']; ?>
: <b><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "currency.tpl", 'smarty_include_vars' => array('value' => $this->_tpl_vars['total_paid'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></b></td>
</tr>

<?php else: ?>
<tr><td colspan="8" align="center"><strong>No order found for the given criteria</strong></td></tr>
<?php endif; ?>


<tr>
	<td colspan="8"><br />

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "customer/main/navigation.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($this->_tpl_vars['usertype'] == 'A' || ( $this->_tpl_vars['usertype'] == 'P' && $this->_tpl_vars['active_modules']['Simple_Mode'] )): ?>
<!--<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_update_status'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: submitForm(this, 'update');" /> 
&nbsp;&nbsp;&nbsp;&nbsp;
<br /><br />-->
<?php endif; ?>

<!--<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_invoices_for_selected'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: if (checkMarks(this.form, new RegExp('orderids\[[0-9]+\]', 'gi'))) { document.processorderform.target='invoices'; submitForm(this, 'invoice'); document.processorderform.target=''; }" />-->
<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_upd_selected'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: if (checkMarks(this.form, new RegExp('ccavenue\[[0-9]+\]', 'gi'))) submitForm(this, 'update'); " />
&nbsp;&nbsp;&nbsp;&nbsp;
<?php if ($this->_tpl_vars['usertype'] != 'C'): ?>
<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_labels_for_selected'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: if (checkMarks(this.form, new RegExp('orderids\[[0-9]+\]', 'gi'))) { document.processorderform.target='labels'; submitForm(this, 'label'); document.processorderform.target=''; }" />
&nbsp;&nbsp;&nbsp;&nbsp;
<?php endif;  if ($this->_tpl_vars['usertype'] == 'A' || ( $this->_tpl_vars['usertype'] == 'P' && $this->_tpl_vars['active_modules']['Simple_Mode'] )): ?>
<!--<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_delete_selected'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: if (checkMarks(this.form, new RegExp('orderids\[[0-9]+\]', 'gi'))) if (confirm('<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['txt_delete_selected_orders_warning'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
')) submitForm(this, 'delete');" />
&nbsp;&nbsp;&nbsp;&nbsp; -->
<?php endif;  if ($this->_tpl_vars['active_modules']['Shipping_Label_Generator'] != '' && ( $this->_tpl_vars['usertype'] == 'A' || ( $this->_tpl_vars['usertype'] == 'P' && $this->_tpl_vars['active_modules']['Simple_Mode'] ) )): ?>
<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_get_shipping_labels'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: if (checkMarks(this.form, new RegExp('orderids\[[0-9]+\]', 'gi'))) { document.processorderform.action='generator.php'; submitForm(this, ''); }" />
&nbsp;&nbsp;&nbsp;&nbsp;
<?php endif; ?>

<?php if ($this->_tpl_vars['usertype'] != 'C'): ?>
<br />
<br />
<br />
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/subheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_export_orders'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
  echo $this->_tpl_vars['lng']['txt_export_all_found_orders_text']; ?>

<br /><br />
<?php echo $this->_tpl_vars['lng']['lbl_export_file_format']; ?>
:<br />
<select id="export_fmt" name="export_fmt">
	<option value="std"><?php echo $this->_tpl_vars['lng']['lbl_standart']; ?>
</option>
	<option value="csv_tab"><?php echo $this->_tpl_vars['lng']['lbl_40x_compatible']; ?>
: CSV <?php echo $this->_tpl_vars['lng']['lbl_with_tab_delimiter']; ?>
</option>
	<option value="csv_semi"><?php echo $this->_tpl_vars['lng']['lbl_40x_compatible']; ?>
: CSV <?php echo $this->_tpl_vars['lng']['lbl_with_semicolon_delimiter']; ?>
</option>
	<option value="csv_comma"><?php echo $this->_tpl_vars['lng']['lbl_40x_compatible']; ?>
: CSV <?php echo $this->_tpl_vars['lng']['lbl_with_comma_delimiter']; ?>
</option>
<?php if ($this->_tpl_vars['active_modules']['QuickBooks'] == 'Y'):  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "modules/QuickBooks/orders.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
  endif; ?>
</select>
<br />
<br />
<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_export'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: if (checkMarks(this.form, new RegExp('orderids\[[0-9]+\]', 'gi'))) submitForm(this, 'export');" />&nbsp;&nbsp;&nbsp;
<input type="button" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lng']['lbl_export_all_found'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onclick="javascript: self.location='orders.php?mode=search&amp;export=export_found&amp;export_fmt='+document.getElementById('export_fmt').value;" />
<?php endif; ?>
</td>
</tr>

</table>
</form>

<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean();  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_search_results'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


<?php endif;  endif; ?>
