<?php /* Smarty version 2.6.12, created on 2017-06-07 11:16:20
         compiled from admin/main/promotions.tpl */ ?>
<?php func_load_lang($this, "admin/main/promotions.tpl","lbl_quick_start,txt_how_setup_store,lbl_quick_start_text"); ?><?php if ($this->_tpl_vars['display'] == 'news'): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/main/xcart_news.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php else: ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "page_title.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_quick_start'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- IN THIS SECTION -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog_tools.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- IN THIS SECTION -->

<br />

<!-- QUICK MENU -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "main/quick_menu.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- QUICK MENU -->

<a name="qs" />
<?php ob_start(); ?>

<div align="justify"><?php echo $this->_tpl_vars['lng']['txt_how_setup_store']; ?>
</div>

<?php echo '
<script type="text/javascript" language="JavaScript 1.2">
<!--
var url =  document.URL;
var re = /^https/;
if ( !url.match(re) ) {
	document.write("<img src=\\"http://www.x-cart.com/img/background.gif\\" width=\\"1\\" height=\\"1\\" alt=\\"\\" />");
}
-->
</script>
'; ?>


<?php $this->_smarty_vars['capture']['dialog'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "dialog.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['lng']['lbl_quick_start_text'],'content' => $this->_smarty_vars['capture']['dialog'],'extra' => 'width="100%"')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<br />
<?php endif; ?>
