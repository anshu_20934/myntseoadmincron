<?php /* Smarty version 2.6.12, created on 2017-05-19 10:57:54
         compiled from admin/pageconfig/pageconfig-header.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'admin/pageconfig/pageconfig-header.tpl', 11, false),)), $this); ?>
<?php echo '
<link rel="stylesheet" type="text/css" href="/skin1/myntra_css/lightbox_admin.css">
<link rel="stylesheet" type="text/css" href="/skin1/myntra_css/pageconfig.css">
<script language="javascript" src="/skin1/js_script/jquery.validate.js"> </script>
<script language="javascript" src="/skin1/myntra_js/ns.js"> </script>
<script language="javascript" src="/skin1/myntra_js/mk-base.js"> </script>
<script language="javascript" src="/skin1/myntra_js/lightbox.js"> </script>
<script language="javascript" src="/skin1/myntra_js/jquery.ui.js"> </script>
<script type="text/javascript">
$(document).ready(function() {
	var numRules = ';  echo count($this->_tpl_vars['rules']);  echo ';
	Myntra.dialogObj = [];
	Myntra.variantDialog = [];
	for(var i=0; i<numRules; i++) {
		Myntra.dialogObj[i] = Myntra.LightBox("#addBannerDiv-"+i);
		Myntra.variantDialog[i] = Myntra.LightBox(\'#addBannerDivVariant-\'+i);
	}
	$(".addBannerLink").bind("click",function() {
		Myntra.dialogObj[$(this).attr("data-lightbox-id")].show();
	});
	$(\'form[name="add"]\').validate({
		rules: {
			title: "required",
			alt_text: "required",
			target_url: "required",
			image: {
				required: "#image_url: blank"
			}
		}
	});
	$(".btndelete").bind("click",function() {
		
		var form = $(this).closest("form");
		var step = form.find("[name=step]").val();
		var section = form.find("[name=section]").val();
						
		var imageIds2del = "";
		var selectedCBs = form.find(\'input.cb-delete[type=checkbox]:checked\');
		if(selectedCBs.length != step) {
			alert("You can delete only "+step+" images at a time in section: "+section);
			return;
		} 
		selectedCBs.each(function(i,el){
			if(imageIds2del === \'\') {
				imageIds2del = imageIds2del+$(el).val();
			} else {
				imageIds2del = imageIds2del+","+ $(el).val();
			}
		});
		var r=confirm("Are you sure you want to delete these banners?");
	    if(r == true){
	        form.find("input[name=actionType]").val('; ?>
"<?php echo $this->_tpl_vars['deleteAction']; ?>
"<?php echo ');
	        form.find("input[name=imageId]").val(imageIds2del);
	        form.submit();
	    }; 
	});

	var parent_image_reduced_by = $(".parent_image").width()/$(".parent_image").attr("data-width");
	$(".child_image").parents(".DIV_TR_TD").css("width",$(".parent_image").width() + "px");
	$(".child_image").height($(".child_image").attr("data-height")*parent_image_reduced_by);
	$(".child_image").width($(".child_image").attr("data-width")*parent_image_reduced_by);
	$(".child_image").parent().css("top",374*parent_image_reduced_by+"px");
	$(".child_image").parent().css("left",150*parent_image_reduced_by+"px");	
});	


</script>
'; ?>

<?php if ($this->_tpl_vars['pageLocationId']): ?>
<div class="link-lpcontroller">
	<a href="/admin/landingpage/lpController.php?lpid=<?php echo $this->_tpl_vars['pageLocationId']; ?>
"/> Back to Landing Page</a>
	
</div>
<?php else: ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/pageconfig/pageconfig-location-chooser.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>

<?php if ($this->_tpl_vars['errorMessage'] != ''): ?>
<div class="errorDiv">
	<span class="errorMsg"><?php echo $this->_tpl_vars['errorMessage']; ?>
</span>
	<span class="dismiss"> <a href="#" onclick="javascript: $('.errorDiv').hide()">Dismiss</a></span>
</div>
<?php endif; ?>
