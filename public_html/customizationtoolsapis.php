<?php
require_once "include/TempImages.php";

function func_generate_marked_customization_area_image($customizationAreaImage,&$smarty,$customizationAreaWidth,$customizationAreaHeight,$xcuststart,$ycuststart,$chosenProductConfiguration,&$custarray,$customizationAreaId,$oId){
   global $isPreviewPage;
   global $weblog;
   global  $isDetailPage,$isProto;
   global $bgcolor;
   global $multioption,$template_to_be_used;
   $caImageKey = $customizationAreaId."_".$oId."_".'imagepath';

   global $isJPG;
   if(empty($custarray['common_generated_img'])){
		if ($isJPG){
		    $image  = imagecreatefromjpeg($customizationAreaImage);
		}else{
	    	$image  = imagecreatefrompng($customizationAreaImage);
	    }
    }else{
    	if ($isJPG){
    		$image  = imagecreatefromjpeg($custarray['common_generated_img']);
    	}else{
    		$image  = imagecreatefrompng($custarray['common_generated_img']);
    	}
    }
    
    // fill the background color
    $bg = imagecolorallocate($image, 0, 0, 0);
    $w = imagecolorallocate($image, 255, 255, 255);
    $b = imagecolorallocate($image, 0, 0, 0);
	$style = array($b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,
					$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w);
    imagesetstyle($image,$style);
    imagesetthickness($image,1);
    
    //check the previous color stored for bgcolor in the session
    
   // if (empty($bgcolor)) $bgcolor = $custarray[$oId]['bgcolor'];
   // echo "Bg ".$custarray[$oId]['bgcolor'];
    $bgcolor = $custarray[$oId]['bgcolor'];
    
    if (empty($bgcolor)){
    	imagepolygon($image,
	                 array (
	                        $xcuststart, $ycuststart,
	                        $xcuststart, $ycuststart+$customizationAreaHeight,
	                        $xcuststart+$customizationAreaWidth, $ycuststart+$customizationAreaHeight,
	                        $xcuststart+$customizationAreaWidth, $ycuststart
	                 ),
	                 4,
	                 IMG_COLOR_STYLED);
	                      
    }else{
    	$bgcolorRGB = html2rgb($bgcolor);
	    list($rgb, $r, $g, $b) = split('[(,)]', $bgcolorRGB);
    	$backgroundColor = imagecolorallocate($image,$r,$g,$b);
    	imagefilledpolygon($image,
	                 array (
	                        $xcuststart, $ycuststart,
	                        $xcuststart, $ycuststart+$customizationAreaHeight,
	                        $xcuststart+$customizationAreaWidth, $ycuststart+$customizationAreaHeight,
	                        $xcuststart+$customizationAreaWidth, $ycuststart
	                 ),
	                 4,
	                 $backgroundColor);
	    	                 
    }
    $len = 4;
    if ($isJPG){
		$tempBaseFile = './images/customized/cust1'.time().get_rand_id($len).'.jpg';
		if(!imagejpeg($image,$tempBaseFile))
			$weblog->info("unable to create $tempBaseFile in customizationtoolsa");
	}else{
		$tempBaseFile = './images/customized/cust1'.time().get_rand_id($len).'.png';
	    if(!imagepng($image,$tempBaseFile))
			$weblog->info("unable to create $tempBaseFile in customizationtoolsa");
		
	}

	addToTempImagesList($tempBaseFile);
	if(!empty($bgTempImage)){
		addToTempImagesList( $bgTempImage);
	}
	if(!empty($custarray[$oId]['imageAfterResizing'])){
		$isresizedimage=1;
	   $uploadedImage  = $custarray[$oId]['imageAfterResizing']; // uploeaded image 
	}
	else  
	   $uploadedImage  = $chosenProductConfiguration[$customizationAreaId."_".$oId."_"."uploadedImagePath"]; // uploeaded image 
     
	
	if (!empty($uploadedImage)){
		if ($isJPG){
	        $pasteImage=imagecreatefromjpeg($uploadedImage);
	    }else{
	        $pasteImage=imagecreatefrompng($uploadedImage);
		}
		list($width_orig, $height_orig) = getimagesize($uploadedImage);
    	$ratio_orig = $width_orig/$height_orig;
        $widthMax = max($customizationAreaWidth,$width_orig);
        $heightMax = max($customizationAreaHeight,$height_orig);
		 //add the original uploaded image path in the session
        $custarray[$oId]['pasteimagepath'] = $uploadedImage;
        $widthinCA;//width of the image pasted in customization area
        $heightinCA;//height of the image pasted in customization area
        if (($width_orig > $customizationAreaWidth) && ($width_orig >= $height_orig)){
            $widthinCA = $customizationAreaWidth;
            $heightinCA = $widthinCA/$ratio_orig;
            if ($heightinCA > $customizationAreaHeight){
            	$heightinCA = $customizationAreaHeight;
            	$widthinCA = $heightinCA * $ratio_orig;
            }
        }elseif (($height_orig > $customizationAreaHeight) && ($height_orig >= $width_orig)){
            $heightinCA = $customizationAreaHeight;
            $widthinCA = $ratio_orig * $heightinCA;
            if ($widthinCA > $customizationAreaWidth){
            	$widthinCA = $customizationAreaWidth;
            	$heightinCA = $widthinCA/$ratio_orig;
            }
        }elseif (($width_orig <= $customizationAreaWidth) && ($width_orig >= $height_orig)){
            $widthinCA = $width_orig;
            $heightinCA = $widthinCA/$ratio_orig;
            if ($heightinCA > $customizationAreaHeight){
            	$heightinCA = $customizationAreaHeight;
            	$widthinCA = $heightinCA * $ratio_orig;
            }
        }elseif (($height_orig <= $customizationAreaHeight) && ($height_orig >= $width_orig)){
            $heightinCA = $height_orig;
            $widthinCA = $ratio_orig * $heightinCA;
            if ($widthinCA > $customizationAreaWidth){
            	$widthinCA = $customizationAreaWidth;
            	$heightinCA = $widthinCA/$ratio_orig;
            }
        }
		 ####
        //TODO carry out resizing only for those images which are larger in size than cust area
        $imageManipulator = new mkImageTransform();
        $imageManipulator->sourceFile = $custarray[$oId]['pasteimagepath'];
	    if ($isJPG){
	        $imageManipulator->jpegOutputQuality = 100;
	        $imageManipulator->targetFile = './images/customized/cust.'.time().get_rand_id($len).'.jpg';
	    }else{
	        $imageManipulator->targetFile = './images/customized/cust.'.time().get_rand_id($len).'.png';
		}
		addToTempImagesList($imageManipulator->targetFile);
		$len =4;
		$imageManipulator->maintainAspectRatio = 0;
		if(	!$isresizedimage)
			list($widthinCA,$heightinCA) = _rethinkImageDimensions($customizationAreaId, $customizationAreaHeight, $customizationAreaWidth,$width_orig, $height_orig);
        $imageManipulator->resizeToWidth = $widthinCA;
        $imageManipulator->resizeToHeight = $heightinCA;
        $imageManipulator->resize();
	    if ($isJPG){
	        $pasteImage = imagecreatefromjpeg($imageManipulator->targetFile);
	    }else{
    	    $pasteImage = imagecreatefrompng($imageManipulator->targetFile);
		}
        //store the resized file path in the session array
        $custarray[$oId]['pastefile'] = $imageManipulator->targetFile;
        //by default dx,dy are 0
        $dx = array_key_exists('dxstart',$custarray[$oId])?$custarray[$oId][dxstart]:($customizationAreaWidth - $widthinCA)/2;
        $dy = array_key_exists('dystart',$custarray[$oId])?$custarray[$oId][dystart]:($customizationAreaHeight - $heightinCA)/2;
        //$dx = ($customizationAreaWidth - $widthinCA)/2;
        //$dy = ($customizationAreaHeight - $heightinCA)/2;

        //cannot change dx and dy since guiding image generation code needs these values
        $dxtmp=($dx<0)?0:$dx;
        $dytmp=($dy<0)?0:$dy;
 
        imagecopyresampled($image, $pasteImage, $xcuststart+$dxtmp,$ycuststart+$dytmp, 0, 0, $widthinCA, $heightinCA, $widthinCA, $heightinCA);
	
	    $len = 4;
	    if ($isJPG){
    	    $tempCustImageFile = "./images/customized/custtemp".time().get_rand_id($len).".jpg";
			imagejpeg($image,$tempCustImageFile);
			$baseimage = imagecreatefromjpeg($tempCustImageFile);
			#for bg Image
			if(!empty($bgImage)){
				$tempCustBgImageFile = "./images/customized/bgimages/custtemp".time().get_rand_id($len).".jpg";
				imagejpeg($bgImage,$tempCustBgImageFile);
				$baseBgimage = imagecreatefromjpeg($tempCustBgImageFile);
			}
	    }else{
    	    $tempCustImageFile = "./images/customized/custtemp".time().get_rand_id($len).".png";
			if(!imagepng($image,$tempCustImageFile))
			  $weblog->info("unable to create $tempCustImageFile in customizationtoolsapis.php");
			$baseimage = imagecreatefrompng($tempCustImageFile);
			#for bg Image
			if(!empty($bgImage)){
				$tempCustBgImageFile = "./images/customized/bgimages/custtemp".time().get_rand_id($len).".png";
				if(!imagepng($bgImage,$tempCustBgImageFile))
			         $weblog->info("unable to create $tempCustBgImageFile in customizationtoolsapis.php");
				$baseBgimage = imagecreatefrompng($tempCustBgImageFile);
			}
		}
		addToTempImagesList($tempCustImageFile);
		if(!empty($tempCustBgImageFile)){
			addToTempImagesList($tempCustBgImageFile);
		}


        $custarray[$oId]['dxstart'] = $dx;
        $custarray[$oId]['dystart'] = $dy;
        $custarray[$oId]['generatedimage'] = $tempCustImageFile;
        $custarray[$oId]['generatedimagenotext'] = $tempCustImageFile;
		
        $custarray[$oId]['orientationid'] = $oId;
       	$custarray[$oId]['bgcolor'] = $bgcolor;
       	// added common image for both orientation 
       	$custarray['common_generated_img'] = $tempCustImageFile;
        $custarray['common_generated_final_image'] = $tempCustImageFile;
       	//this code is for handling the situation wherein somebody uploads an image after putting some text in the customization
       	//area.
     
       	$customizedText = $custarray[$oId]['customizedtext'];
        $smarty->assign("customizedImage",$tempCustImageFile);
        // check if the any template is defined 
        if(!empty($template_to_be_used)){

           // multiple customization for orientation
			 $common_generated_txt_image = $custarray['common_generated_img'];
			 foreach($chosenProductConfiguration["customizationOrientationDetails"] as $_k => $orinfo){
				$orid = $orinfo[7];
				$customizedText = $custarray[$orid]['customizedtext'];

				if (!empty($customizedText)){
		     	    $fcolor = "#".dechex($customizedText['fontcolor']);
					$tempBaseFile = func_put_text_on_generated_commonimage($customizedText['fontbold'],$fcolor,$customizedText['fontitalics'],$customizedText['fontsize'],$customizedText['fonttypename'],$customizedText['fonttext'],/*$customizedText['textX']*/0,/*$customizedText['textY']*/0,$custarray,$smarty,$orid,$common_generated_txt_image);

	       		}
            }
            $smarty->assign("customizedImage",$custarray['common_generated_final_image']);
			func_display($template_to_be_used,$smarty);
        }

		else if($isPreviewPage == 1 && !empty($isPreviewPage))
		{
			// multiple customization for orientation  
			 $common_generated_txt_image = $custarray['common_generated_img'];
			 foreach($chosenProductConfiguration["customizationOrientationDetails"] as $_k => $orinfo){
				$orid = $orinfo[7];
				$customizedText = $custarray[$orid]['customizedtext'];
				
				if (!empty($customizedText)){
		     	    $fcolor = "#".dechex($customizedText['fontcolor']);
					$tempBaseFile = func_put_text_on_generated_commonimage($customizedText['fontbold'],$fcolor,$customizedText['fontitalics'],$customizedText['fontsize'],$customizedText['fonttypename'],$customizedText['fonttext'],/*$customizedText['textX']*/0,/*$customizedText['textY']*/0,$custarray,$smarty,$orid,$common_generated_txt_image);
					
	       		}
            }
           	$smarty->assign("customizedImage",$custarray['common_generated_final_image']);
			func_display("mkcreatecustomizeimageforpreview.tpl",$smarty);
		}
		elseif($isDetailPage ==1 && !empty($isDetailPage))
		{

			 func_display("mkcreatecustomizeimgproductdetail.tpl",$smarty);
		}
		elseif($isProto ==1 && !empty($isProto))
		{
			 func_display("protoincustomize.tpl",$smarty);
		}
		else
		{
			// multiple customization for orientation  
			 $common_generated_txt_image = $custarray['common_generated_img'];
			 foreach($chosenProductConfiguration["customizationOrientationDetails"] as $_k => $orinfo){
				$orid = $orinfo[7];
				$customizedText = $custarray[$orid]['customizedtext'];
				
				if (!empty($customizedText)){
		     	    $fcolor = "#".dechex($customizedText['fontcolor']);
					$tempBaseFile = func_put_text_on_generated_commonimage($customizedText['fontbold'],$fcolor,$customizedText['fontitalics'],$customizedText['fontsize'],$customizedText['fonttypename'],$customizedText['fonttext'],/*$customizedText['textX']*/0,/*$customizedText['textY']*/0,$custarray,$smarty,$orid,$common_generated_txt_image);
					
	       		}
            }
           	$smarty->assign("customizedImage",$custarray['common_generated_final_image']);
			func_display("mkcreatecustomizeimage.tpl",$smarty);
		}
        return;
    }
   
    $custarray[$oId]['generatedimage'] = $tempBaseFile;
    $custarray[$oId]['generatedimagenotext'] = $tempBaseFile;
	
    $custarray[$oId]['orientationid'] = $oId;
    $custarray['common_generated_img'] = $tempBaseFile; // common image for both orientation
    $custarray['common_generated_final_image'] = $tempBaseFile;
    $smarty->assign("customizedImage",$tempBaseFile);
    // check if the any template is defined
    if(!empty($template_to_be_used)){

          // multiple customization for orientation
			 $common_generated_txt_image = $custarray['common_generated_img'];
			 foreach($chosenProductConfiguration["customizationOrientationDetails"] as $_k => $orinfo){
				$orid = $orinfo[7];
				$customizedText = $custarray[$orid]['customizedtext'];

				if (!empty($customizedText)){
		     	    $fcolor = "#".dechex($customizedText['fontcolor']);
					$tempBaseFile = func_put_text_on_generated_commonimage($customizedText['fontbold'],$fcolor,$customizedText['fontitalics'],$customizedText['fontsize'],$customizedText['fonttype'],$customizedText['fonttext'],/*$customizedText['textX']*/0,/*$customizedText['textY']*/0,$custarray,$smarty,$orid,$common_generated_txt_image);
		    	}
           	}
           	$smarty->assign("customizedImage",$custarray['common_generated_final_image']);
           	//$smarty->assign("customizedImage",$tempBaseFile);
           	func_display($template_to_be_used,$smarty);
     }
    
     else if($isPreviewPage == 1  &&  !empty($isPreviewPage))
	 {
         // multiple customization for orientation  
			 $common_generated_txt_image = $custarray['common_generated_img'];
			 foreach($chosenProductConfiguration["customizationOrientationDetails"] as $_k => $orinfo){
				$orid = $orinfo[7];
				$customizedText = $custarray[$orid]['customizedtext'];
				
				if (!empty($customizedText)){
		     	    $fcolor = "#".dechex($customizedText['fontcolor']);
					$tempBaseFile = func_put_text_on_generated_commonimage($customizedText['fontbold'],$fcolor,$customizedText['fontitalics'],$customizedText['fontsize'],$customizedText['fonttypename'],$customizedText['fonttext'],/*$customizedText['textX']*/0,/*$customizedText['textY']*/0,$custarray,$smarty,$orid,$common_generated_txt_image);
		    	}
           	}
           	$smarty->assign("customizedImage",$custarray['common_generated_final_image']);
           	//$smarty->assign("customizedImage",$tempBaseFile);
		   	func_display("mkcreatecustomizeimageforpreview.tpl",$smarty);
		
     	}
		elseif($isDetailPage ==1 && !empty($isDetailPage))
		{
			 func_display("mkcreatecustomizeimgproductdetail.tpl",$smarty);
		}
		elseif($isProto ==1 && !empty($isProto))
		{
			 func_display("protoincustomize.tpl",$smarty);
		}
		else
		{
		      
			 // multiple customization for orientation  
			 $common_generated_txt_image = $custarray['common_generated_img'];
			 foreach($chosenProductConfiguration["customizationOrientationDetails"] as $_k => $orinfo){
				$orid = $orinfo[7];
				$customizedText = $custarray[$orid]['customizedtext'];
				
				if (!empty($customizedText)){
		     	    $fcolor = "#".dechex($customizedText['fontcolor']);
					$tempBaseFile = func_put_text_on_generated_commonimage($customizedText['fontbold'],$fcolor,$customizedText['fontitalics'],$customizedText['fontsize'],$customizedText['fonttypename'],$customizedText['fonttext'],/*$customizedText['textX']*/0,/*$customizedText['textY']*/0,$custarray,$smarty,$orid,$common_generated_txt_image);
					
	       		}
           	}
           	$smarty->assign("customizedImage",$custarray['common_generated_final_image']);
           	func_display("mkcreatecustomizeimage.tpl",$smarty);
		
}
    	}
function func_generate_marked_customization_area_image_and_crop_image($customizationAreaImage,&$smarty,$customizationAreaWidth,$customizationAreaHeight,$xcuststart,$ycuststart,$chosenProductConfiguration,&$custarray,$customizationAreaId,$oId)
{
   global $isPreviewPage;
   global  $isDetailPage,$isProto;
   global $bgcolor;
   global $multioption,$template_to_be_used;
   global $weblog;

   $caImageKey = $customizationAreaId."_".$oId."_".'imagepath';

   global $isJPG;
   if(empty($custarray['common_generated_img'])){
		if ($isJPG){
		    $image  = imagecreatefromjpeg($customizationAreaImage);
		}else{
	    	$image  = imagecreatefrompng($customizationAreaImage);
	    }
    }else{
    	if ($isJPG){
    		$image  = imagecreatefromjpeg($custarray['common_generated_img']);
    	}else{
    		$image  = imagecreatefrompng($custarray['common_generated_img']);
    	}
    }
    
    // fill the background color
    $bg = imagecolorallocate($image, 0, 0, 0);
    $w = imagecolorallocate($image, 255, 255, 255);
    $b = imagecolorallocate($image, 0, 0, 0);
	$style = array($b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,
					$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w);
    imagesetstyle($image,$style);
    imagesetthickness($image,1);
    
    //check the previous color stored for bgcolor in the session
    
   // if (empty($bgcolor)) $bgcolor = $custarray[$oId]['bgcolor'];
   // echo "Bg ".$custarray[$oId]['bgcolor'];
    $bgcolor = $custarray[$oId]['bgcolor'];
    
    if (empty($bgcolor)){
    	imagepolygon($image,
	                 array (
	                        $xcuststart, $ycuststart,
	                        $xcuststart, $ycuststart+$customizationAreaHeight,
	                        $xcuststart+$customizationAreaWidth, $ycuststart+$customizationAreaHeight,
	                        $xcuststart+$customizationAreaWidth, $ycuststart
	                 ),
	                 4,
	                 IMG_COLOR_STYLED);
	                      
    }else{
    	$bgcolorRGB = html2rgb($bgcolor);
	    list($rgb, $r, $g, $b) = split('[(,)]', $bgcolorRGB);
    	$backgroundColor = imagecolorallocate($image,$r,$g,$b);
    	imagefilledpolygon($image,
	                 array (
	                        $xcuststart, $ycuststart,
	                        $xcuststart, $ycuststart+$customizationAreaHeight,
	                        $xcuststart+$customizationAreaWidth, $ycuststart+$customizationAreaHeight,
	                        $xcuststart+$customizationAreaWidth, $ycuststart
	                 ),
	                 4,
	                 $backgroundColor);
	    	                 
    }
    $len = 4;
    if ($isJPG){
		$tempBaseFile = './images/customized/cust1'.time().get_rand_id($len).'.jpg';
	    if(!imagejpeg($image,$tempBaseFile))
			$weblog->info("unable to create $tempBaseFile in customizationtoolsa");
	}else{
		$tempBaseFile = './images/customized/cust1'.time().get_rand_id($len).'.png';
	    if(!imagepng($image,$tempBaseFile))
			$weblog->info("unable to create $tempBaseFile in customizationtoolsa");
		
	}

	addToTempImagesList($tempBaseFile);
	if(!empty($bgTempImage)){
		addToTempImagesList($bgTempImage);
	}
	if(!empty($custarray[$oId]['imageAfterResizing'])){
		$isresizedimage=1;
	   $uploadedImage  = $custarray[$oId]['imageAfterResizing']; // uploeaded image 
	}
	else  
	   $uploadedImage  = $chosenProductConfiguration[$customizationAreaId."_".$oId."_"."uploadedImagePath"]; // uploeaded image 
     
	
	if (!empty($uploadedImage)){
		if ($isJPG){
	        $pasteImage=imagecreatefromjpeg($uploadedImage);
	    }else{
	        $pasteImage=imagecreatefrompng($uploadedImage);
		}
	
		$imageproperties =getimagesize($uploadedImage);
		$width_orig=$imageproperties[0];
		$height_orig=$imageproperties[1];
		$ratio_orig = $width_orig/$height_orig;
		$widthMax = max($customizationAreaWidth,$width_orig);
		$heightMax = max($customizationAreaHeight,$height_orig);
        $custarray[$oId]['pasteimagepath'] = $uploadedImage;
		$widthinCA =$customizationAreaWidth;//width of the image pasted in customization area
		$heightinCA=$customizationAreaHeight;//height of the image pasted in customization area
		
		$customizatio_aspect_ratio =$customizationAreaWidth/$customizationAreaHeight;

		
		$srcx=0;
		$srcy=0;
		$height_new=0;
		$width_new=0;
		if($customizatio_aspect_ratio >= 1)
		{
			$height_new = $width_orig/$customizatio_aspect_ratio;
			$width_new=$width_orig;
			$srcy=($height_orig-$height_new)/2;
		}
		else
		{
			$width_new = $height_orig * $customizatio_aspect_ratio;
			$height_new=$height_orig;
			$srcx=($width_orig-$width_new)/2;
		}


		$tempimg ='';
		$path='';
		$imageCropper = new mkImageTransform();
		$imageCropper->sourceFile = $uploadedImage;
		$imageCropper->maintainAspectRatio = 0;
		$imageCropper->resizeToWidth = $width_new;
		$imageCropper->resizeToHeight = $height_new;	
		if ($imageproperties['mime'] =='image/jpeg'){
			$imageCropper->jpegOutputQuality = 100;
			$imageCropper->targetFile = './images/customized/cust_crop.'.time().get_rand_id($len).'.jpg';
		}else{
			$imageCropper->targetFile = './images/customized/cust_crop.'.time().get_rand_id($len).'.png';
		}	 
		$imageCropper->crop($srcx,$srcy);

		$imageManipulator = new mkImageTransform();
        $imageManipulator->sourceFile = $imageCropper->targetFile;
	    if ($isJPG){
	        $imageManipulator->jpegOutputQuality = 100;
	        $imageManipulator->targetFile = './images/customized/cust.'.time().get_rand_id($len).'.jpg';
	    }else{
	        $imageManipulator->targetFile = './images/customized/cust.'.time().get_rand_id($len).'.png';
		}
		
		addToTempImagesList( $imageManipulator->targetFile);
		$len =4;
		$imageManipulator->maintainAspectRatio = 0;
		if(	!$isresizedimage)
			//list($widthinCA,$heightinCA) = _rethinkImageDimensions($customizationAreaId, $customizationAreaHeight, $customizationAreaWidth,$width_orig, $height_orig);
        $imageManipulator->resizeToWidth = $widthinCA;
        $imageManipulator->resizeToHeight = $heightinCA;
        $imageManipulator->resize();
	    if ($isJPG){
	        $pasteImage = imagecreatefromjpeg($imageManipulator->targetFile);
	    }else{
    	    $pasteImage = imagecreatefrompng($imageManipulator->targetFile);
		}
        //store the resized file path in the session array
        $custarray[$oId]['pastefile'] = $imageManipulator->targetFile;
        //by default dx,dy are 0
        $dx = array_key_exists('dxstart',$custarray[$oId])?$custarray[$oId][dxstart]:($customizationAreaWidth - $widthinCA)/2;
        $dy = array_key_exists('dystart',$custarray[$oId])?$custarray[$oId][dystart]:($customizationAreaHeight - $heightinCA)/2;
        //$dx = ($customizationAreaWidth - $widthinCA)/2;
        //$dy = ($customizationAreaHeight - $heightinCA)/2;

        //cannot change dx and dy since guiding image generation code needs these values
        $dxtmp=($dx<0)?0:$dx;
        $dytmp=($dy<0)?0:$dy;
        
        imagecopyresampled($image, $pasteImage, $xcuststart+$dxtmp,$ycuststart+$dytmp, 0, 0, $widthinCA, $heightinCA, $widthinCA, $heightinCA);
	
	    $len = 4;
	    if ($isJPG){
    	    $tempCustImageFile = "./images/customized/custtemp".time().get_rand_id($len).".jpg";
			if(!imagejpeg($image,$tempCustImageFile))
			   $weblog->info("unable to create $tempCustImageFile in customizationtoolsa");
			$baseimage = imagecreatefromjpeg($tempCustImageFile);
			#for bg Image
			if(!empty($bgImage)){
				$tempCustBgImageFile = "./images/customized/bgimages/custtemp".time().get_rand_id($len).".jpg";
				if(!imagejpeg($bgImage,$tempCustBgImageFile))
			       $weblog->info("unable to create $tempCustBgImageFile in customizationtoolsa");
				$baseBgimage = imagecreatefromjpeg($tempCustBgImageFile);
			}
	    }else{
    	    $tempCustImageFile = "./images/customized/custtemp".time().get_rand_id($len).".png";
			if(!imagepng($image,$tempCustImageFile))
			   $weblog->info("unable to create $tempCustImageFile in customizationtoolsa");
			$baseimage = imagecreatefrompng($tempCustImageFile);
			#for bg Image
			if(!empty($bgImage)){
				$tempCustBgImageFile = "./images/customized/bgimages/custtemp".time().get_rand_id($len).".png";
				if(!imagepng($bgImage,$tempCustBgImageFile))
			       $weblog->info("unable to create $tempCustBgImageFile in customizationtoolsapis.php");
				$baseBgimage = imagecreatefrompng($tempCustBgImageFile);
			}
		}
		addToTempImagesList( $tempCustImageFile);
		if(!empty($tempCustBgImageFile)){
			addToTempImagesList( $tempCustBgImageFile);
		}


        $custarray[$oId]['dxstart'] = $dx;
        $custarray[$oId]['dystart'] = $dy;
        $custarray[$oId]['generatedimage'] = $tempCustImageFile;
        $custarray[$oId]['generatedimagenotext'] = $tempCustImageFile;
		
        $custarray[$oId]['orientationid'] = $oId;
       	$custarray[$oId]['bgcolor'] = $bgcolor;
       	// added common image for both orientation 
       	$custarray['common_generated_img'] = $tempCustImageFile;
        $custarray['common_generated_final_image'] = $tempCustImageFile;
       	//this code is for handling the situation wherein somebody uploads an image after putting some text in the customization
       	//area.
     
       	$customizedText = $custarray[$oId]['customizedtext'];
        $smarty->assign("customizedImage",$tempCustImageFile);
        // check if the any template is defined 
        if(!empty($template_to_be_used)){

           // multiple customization for orientation
			 $common_generated_txt_image = $custarray['common_generated_img'];
			 foreach($chosenProductConfiguration["customizationOrientationDetails"] as $_k => $orinfo){
				$orid = $orinfo[7];
				$customizedText = $custarray[$orid]['customizedtext'];

				if (!empty($customizedText)){
		     	    $fcolor = "#".dechex($customizedText['fontcolor']);
					$tempBaseFile = func_put_text_on_generated_commonimage($customizedText['fontbold'],$fcolor,$customizedText['fontitalics'],$customizedText['fontsize'],$customizedText['fonttype'],$customizedText['fonttext'],/*$customizedText['textX']*/0,/*$customizedText['textY']*/0,$custarray,$smarty,$orid,$common_generated_txt_image);

	       		}
            }
            $smarty->assign("customizedImage",$custarray['common_generated_final_image']);
			func_display($template_to_be_used,$smarty);
        }

		else if($isPreviewPage == 1 && !empty($isPreviewPage))
		{
			// multiple customization for orientation  
			 $common_generated_txt_image = $custarray['common_generated_img'];
			 foreach($chosenProductConfiguration["customizationOrientationDetails"] as $_k => $orinfo){
				$orid = $orinfo[7];
				$customizedText = $custarray[$orid]['customizedtext'];
				
				if (!empty($customizedText)){
		     	    $fcolor = "#".dechex($customizedText['fontcolor']);
					$tempBaseFile = func_put_text_on_generated_commonimage($customizedText['fontbold'],$fcolor,$customizedText['fontitalics'],$customizedText['fontsize'],$customizedText['fonttype'],$customizedText['fonttext'],/*$customizedText['textX']*/0,/*$customizedText['textY']*/0,$custarray,$smarty,$orid,$common_generated_txt_image);
					
	       		}
            }
           	$smarty->assign("customizedImage",$custarray['common_generated_final_image']);
			func_display("mkcreatecustomizeimageforpreview.tpl",$smarty);
		}
		elseif($isDetailPage ==1 && !empty($isDetailPage))
		{

			 func_display("mkcreatecustomizeimgproductdetail.tpl",$smarty);
		}
		elseif($isProto ==1 && !empty($isProto))
		{
			 func_display("protoincustomize.tpl",$smarty);
		}
		else
		{
			// multiple customization for orientation  
			 $common_generated_txt_image = $custarray['common_generated_img'];
			 foreach($chosenProductConfiguration["customizationOrientationDetails"] as $_k => $orinfo){
				$orid = $orinfo[7];
				$customizedText = $custarray[$orid]['customizedtext'];
				
				if (!empty($customizedText)){
		     	    $fcolor = "#".dechex($customizedText['fontcolor']);
					$tempBaseFile = func_put_text_on_generated_commonimage($customizedText['fontbold'],$fcolor,$customizedText['fontitalics'],$customizedText['fontsize'],$customizedText['fonttype'],$customizedText['fonttext'],/*$customizedText['textX']*/0,/*$customizedText['textY']*/0,$custarray,$smarty,$orid,$common_generated_txt_image);
					
	       		}
            }
           	$smarty->assign("customizedImage",$custarray['common_generated_final_image']);
			func_display("mkcreatecustomizeimage.tpl",$smarty);
		}
        return;
    }
   
    $custarray[$oId]['generatedimage'] = $tempBaseFile;
    $custarray[$oId]['generatedimagenotext'] = $tempBaseFile;
	
    $custarray[$oId]['orientationid'] = $oId;
    $custarray['common_generated_img'] = $tempBaseFile; // common image for both orientation
    $custarray['common_generated_final_image'] = $tempBaseFile;
    $smarty->assign("customizedImage",$tempBaseFile);
    // check if the any template is defined
    if(!empty($template_to_be_used)){

          // multiple customization for orientation
			 $common_generated_txt_image = $custarray['common_generated_img'];
			 foreach($chosenProductConfiguration["customizationOrientationDetails"] as $_k => $orinfo){
				$orid = $orinfo[7];
				$customizedText = $custarray[$orid]['customizedtext'];

				if (!empty($customizedText)){
		     	    $fcolor = "#".dechex($customizedText['fontcolor']);
					$tempBaseFile = func_put_text_on_generated_commonimage($customizedText['fontbold'],$fcolor,$customizedText['fontitalics'],$customizedText['fontsize'],$customizedText['fonttype'],$customizedText['fonttext'],/*$customizedText['textX']*/0,/*$customizedText['textY']*/0,$custarray,$smarty,$orid,$common_generated_txt_image);
		    	}
           	}
           	$smarty->assign("customizedImage",$custarray['common_generated_final_image']);
           	//$smarty->assign("customizedImage",$tempBaseFile);
           	func_display($template_to_be_used,$smarty);
     }
    
     else if($isPreviewPage == 1  &&  !empty($isPreviewPage))
	 {
         // multiple customization for orientation  
			 $common_generated_txt_image = $custarray['common_generated_img'];
			 foreach($chosenProductConfiguration["customizationOrientationDetails"] as $_k => $orinfo){
				$orid = $orinfo[7];
				$customizedText = $custarray[$orid]['customizedtext'];
				
				if (!empty($customizedText)){
		     	    $fcolor = "#".dechex($customizedText['fontcolor']);
					$tempBaseFile = func_put_text_on_generated_commonimage($customizedText['fontbold'],$fcolor,$customizedText['fontitalics'],$customizedText['fontsize'],$customizedText['fonttype'],$customizedText['fonttext'],/*$customizedText['textX']*/0,/*$customizedText['textY']*/0,$custarray,$smarty,$orid,$common_generated_txt_image);
		    	}
           	}
           	$smarty->assign("customizedImage",$custarray['common_generated_final_image']);
           	//$smarty->assign("customizedImage",$tempBaseFile);
		   	func_display("mkcreatecustomizeimageforpreview.tpl",$smarty);
		
     	}
		elseif($isDetailPage ==1 && !empty($isDetailPage))
		{
			 func_display("mkcreatecustomizeimgproductdetail.tpl",$smarty);
		}
		elseif($isProto ==1 && !empty($isProto))
		{
			 func_display("protoincustomize.tpl",$smarty);
		}
		else
		{
		      
			 // multiple customization for orientation  
			 $common_generated_txt_image = $custarray['common_generated_img'];
			 foreach($chosenProductConfiguration["customizationOrientationDetails"] as $_k => $orinfo){
				$orid = $orinfo[7];
				$customizedText = $custarray[$orid]['customizedtext'];
				
				if (!empty($customizedText)){
		     	    $fcolor = "#".dechex($customizedText['fontcolor']);
					$tempBaseFile = func_put_text_on_generated_commonimage($customizedText['fontbold'],$fcolor,$customizedText['fontitalics'],$customizedText['fontsize'],$customizedText['fonttype'],$customizedText['fonttext'],/*$customizedText['textX']*/0,/*$customizedText['textY']*/0,$custarray,$smarty,$orid,$common_generated_txt_image);
					
	       		}
           	}
           	$smarty->assign("customizedImage",$custarray['common_generated_final_image']);
           	func_display("mkcreatecustomizeimage.tpl",$smarty);
		
		}
}

//** function in case user switch to different style 
function func_generate_marked_customization_area_for_changed_style($customizationAreaImage,&$smarty,$customizationAreaWidth,$customizationAreaHeight,$xcuststart,$ycuststart,$chosenProductConfiguration,&$custarray,$customizationAreaId,$oId,$bgChange=''){
   global $isPreviewPage;
   global  $isDetailPage,$isProto;
   global $bgcolor;
   global $multioption;
   global $weblog;
   $caImageKey = $customizationAreaId."_".$oId."_".'imagepath';

   global $isJPG;
   if(empty($custarray['common_generated_img'])){
		if ($isJPG){
		    $image  = imagecreatefromjpeg($customizationAreaImage);
		}else{
	    	$image  = imagecreatefrompng($customizationAreaImage);
	    }
    }else{
    	if ($isJPG){
    		$image  = imagecreatefromjpeg($custarray['common_generated_img']);
    	}else{
    		$image  = imagecreatefrompng($custarray['common_generated_img']);
    	}
    }
    
    // fill the background color
    $bg = imagecolorallocate($image, 0, 0, 0);
    $w = imagecolorallocate($image, 255, 255, 255);
    $b = imagecolorallocate($image, 0, 0, 0);
	$style = array($b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,
					$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w);
    imagesetstyle($image,$style);
    imagesetthickness($image,1);
    
    //check the previous color stored for bgcolor in the session
    if(!empty($bgChange)){
    	  //$bgcolor = $custarray[$oId]['bgcolor'];
    	   $backgrdcolor =  $bgChange;
    }else{
    	 if (empty($bgcolor)) $bgcolor = $custarray[$oId]['bgcolor'];
    	 $backgrdcolor = $bgcolor;
    	 //echo "bgc ".$bgcolor;
    }
   
    if (empty($backgrdcolor)){
    	imagepolygon($image,
	                 array (
	                        $xcuststart, $ycuststart,
	                        $xcuststart, $ycuststart+$customizationAreaHeight,
	                        $xcuststart+$customizationAreaWidth, $ycuststart+$customizationAreaHeight,
	                        $xcuststart+$customizationAreaWidth, $ycuststart
	                 ),
	                 4,
	                 IMG_COLOR_STYLED);
	                      
    }else{
    	$bgcolorRGB = html2rgb($backgrdcolor);
	    list($rgb, $r, $g, $b) = split('[(,)]', $bgcolorRGB);
    	$backgroundColor = imagecolorallocate($image,$r,$g,$b);
    	imagefilledpolygon($image,
	                 array (
	                        $xcuststart, $ycuststart,
	                        $xcuststart, $ycuststart+$customizationAreaHeight,
	                        $xcuststart+$customizationAreaWidth, $ycuststart+$customizationAreaHeight,
	                        $xcuststart+$customizationAreaWidth, $ycuststart
	                 ),
	                 4,
	                 $backgroundColor);
	    	                 
    }
    $len = 4;
    if ($isJPG){
		$tempBaseFile = './images/customized/cust1'.time().get_rand_id($len).'.jpg';
	    if(!imagejpeg($image,$tempBaseFile))
			$weblog->info("unable to create $tempBaseFile in customizationtoolsapis.php");
	}else{
		$tempBaseFile = './images/customized/cust1'.time().get_rand_id($len).'.png';
	    if(!imagepng($image,$tempBaseFile))
			$weblog->info("unable to create $tempBaseFile in customizationtoolsapis.php");
		
	}

	addToTempImagesList($tempBaseFile);

	  if(!empty($custarray[$oId]['imageAfterResizing'])){
			$isresizedimage = 1;
	  	 $uploadedImage  = $custarray[$oId]['imageAfterResizing']; // resized image
		}
	  else	 
		  $uploadedImage  = $chosenProductConfiguration[$customizationAreaId."_".$oId."_"."uploadedImagePath"]; // orignal uploaded 
		  
	  if(!empty($uploadedImage)){  
        if ($isJPG){
	        $pasteImage=imagecreatefromjpeg($uploadedImage);
	    }else{
	        $pasteImage=imagecreatefrompng($uploadedImage);
		}
		list($width_orig, $height_orig) = getimagesize($uploadedImage);
    	$ratio_orig = $width_orig/$height_orig;
        $widthMax = max($customizationAreaWidth,$width_orig);
        $heightMax = max($customizationAreaHeight,$height_orig);
		 //add the original uploaded image path in the session
        $custarray[$oId]['pasteimagepath'] = $uploadedImage;
        $widthinCA;//width of the image pasted in customization area
        $heightinCA;//height of the image pasted in customization area
        if (($width_orig > $customizationAreaWidth) && ($width_orig >= $height_orig)){
            $widthinCA = $customizationAreaWidth;
            $heightinCA = $widthinCA/$ratio_orig;
            if ($heightinCA > $customizationAreaHeight){
            	$heightinCA = $customizationAreaHeight;
            	$widthinCA = $heightinCA * $ratio_orig;
            }
        }elseif (($height_orig > $customizationAreaHeight) && ($height_orig >= $width_orig)){
            $heightinCA = $customizationAreaHeight;
            $widthinCA = $ratio_orig * $heightinCA;
            if ($widthinCA > $customizationAreaWidth){
            	$widthinCA = $customizationAreaWidth;
            	$heightinCA = $widthinCA/$ratio_orig;
            }
        }elseif (($width_orig <= $customizationAreaWidth) && ($width_orig >= $height_orig)){
            $widthinCA = $width_orig;
            $heightinCA = $widthinCA/$ratio_orig;
            if ($heightinCA > $customizationAreaHeight){
            	$heightinCA = $customizationAreaHeight;
            	$widthinCA = $heightinCA * $ratio_orig;
            }
        }elseif (($height_orig <= $customizationAreaHeight) && ($height_orig >= $width_orig)){
            $heightinCA = $height_orig;
            $widthinCA = $ratio_orig * $heightinCA;
            if ($widthinCA > $customizationAreaWidth){
            	$widthinCA = $customizationAreaWidth;
            	$heightinCA = $widthinCA/$ratio_orig;
            }
        }
		 ####
        //TODO carry out resizing only for those images which are larger in size than cust area
        $imageManipulator = new mkImageTransform();
        $imageManipulator->sourceFile = $custarray[$oId]['pasteimagepath'];
	    if ($isJPG){
	        $imageManipulator->jpegOutputQuality = 100;
	        $imageManipulator->targetFile = './images/customized/cust.'.time().get_rand_id($len).'.jpg';
	    }else{
	        $imageManipulator->targetFile = './images/customized/cust.'.time().get_rand_id($len).'.png';
		}
		addToTempImagesList($imageManipulator->targetFile);
		$len =4;
        $imageManipulator->maintainAspectRatio = 0;
		if(!$isresizedimage)
			list($widthinCA,$heightinCA) = _rethinkImageDimensions($customizationAreaId, $customizationAreaHeight, $customizationAreaWidth,$width_orig, $height_orig);
        $imageManipulator->resizeToWidth = $widthinCA;
        $imageManipulator->resizeToHeight = $heightinCA;
        $imageManipulator->resize();
	    if ($isJPG){
	        $pasteImage = imagecreatefromjpeg($imageManipulator->targetFile);
	    }else{
    	    $pasteImage = imagecreatefrompng($imageManipulator->targetFile);
		}
        //store the resized file path in the session array
        $custarray[$oId]['pastefile'] = $imageManipulator->targetFile;
        //by default dx,dy are 0
        $dx = array_key_exists('dxstart',$custarray[$oId])?$custarray[$oId][dxstart]:($customizationAreaWidth - $widthinCA)/2;
        $dy = array_key_exists('dystart',$custarray[$oId])?$custarray[$oId][dystart]:($customizationAreaHeight - $heightinCA)/2;
        //$dx = ($customizationAreaWidth - $widthinCA)/2;
        //$dy = ($customizationAreaHeight - $heightinCA)/2;

        //cannot change dx and dy since guiding image generation code needs these values
        $dxtmp=($dx<0)?0:$dx;
        $dytmp=($dy<0)?0:$dy;
        
        imagecopyresampled($image, $pasteImage, $xcuststart+$dxtmp,$ycuststart+$dytmp, 0, 0, $widthinCA, $heightinCA, $widthinCA, $heightinCA);
		
	    $len = 4;
	    if ($isJPG){
    	    $tempCustImageFile = "./images/customized/custtemp".time().get_rand_id($len).".jpg";
			imagejpeg($image,$tempCustImageFile);
			$baseimage = imagecreatefromjpeg($tempCustImageFile);
			#for bg Image
			if(!empty($bgImage)){
				$tempCustBgImageFile = "./images/customized/bgimages/custtemp".time().get_rand_id($len).".jpg";
				imagejpeg($bgImage,$tempCustBgImageFile);
				$baseBgimage = imagecreatefromjpeg($tempCustBgImageFile);
			}
	    }else{
    	    $tempCustImageFile = "./images/customized/custtemp".time().get_rand_id($len).".png";
			if(!imagepng($image,$tempCustImageFile))
			   $weblog->info("unable to create $tempCustImageFile in customizationtoolsapis.php");
			$baseimage = imagecreatefrompng($tempCustImageFile);
			#for bg Image
			if(!empty($bgImage)){
				$tempCustBgImageFile = "./images/customized/bgimages/custtemp".time().get_rand_id($len).".png";
				if(!imagepng($bgImage,$tempCustBgImageFile))
			       $weblog->info("unable to create $tempCustImageFile in customizationtoolsapis.php");
				$baseBgimage = imagecreatefrompng($tempCustBgImageFile);
			}
		}
		addToTempImagesList($tempCustImageFile);
		if(!empty($tempCustBgImageFile)){
			addToTempImagesList($tempCustBgImageFile);
		}


        $custarray[$oId]['dxstart'] = $dx;
        $custarray[$oId]['dystart'] = $dy;
        $custarray[$oId]['generatedimage'] = $tempCustImageFile;
        $custarray[$oId]['generatedimagenotext'] = $tempCustImageFile;
		
        $custarray[$oId]['orientationid'] = $oId;
       	$custarray[$oId]['bgcolor'] = $backgrdcolor;
       	// added common image for both orientation 
       	$custarray['common_generated_img'] = $tempCustImageFile;
        $custarray['common_generated_final_image'] = $tempCustImageFile;
       	//this code is for handling the situation wherein somebody uploads an image after putting some text in the customization
       	//area.
	  }
	  else{
	  	(!empty($backgrdcolor))	? $custarray[$oId]['bgcolor'] = $backgrdcolor : '';
	    $custarray[$oId]['generatedimage'] = $tempBaseFile;
        $custarray[$oId]['generatedimagenotext'] = $tempBaseFile;
	    $custarray[$oId]['orientationid'] = $oId;
        $custarray['common_generated_img'] = $tempBaseFile; // common image for both orientation
        $custarray['common_generated_final_image'] = $tempCustImageFile;
	  }
	 	
   
}

function func_generate_marked_customization_area_for_changed_style_with_crop($customizationAreaImage,&$smarty,$customizationAreaWidth,$customizationAreaHeight,$xcuststart,$ycuststart,$chosenProductConfiguration,&$custarray,$customizationAreaId,$oId,$bgChange='')
{
   global $isPreviewPage;
   global  $isDetailPage,$isProto;
   global $bgcolor;
   global $multioption;
   global $weblog;

   $caImageKey = $customizationAreaId."_".$oId."_".'imagepath';

   global $isJPG;
   if(empty($custarray['common_generated_img'])){
		if ($isJPG){
		    $image  = imagecreatefromjpeg($customizationAreaImage);
		}else{
	    	$image  = imagecreatefrompng($customizationAreaImage);
	    }
    }else{
    	if ($isJPG){
    		$image  = imagecreatefromjpeg($custarray['common_generated_img']);
    	}else{
    		$image  = imagecreatefrompng($custarray['common_generated_img']);
    	}
    }
    
    // fill the background color
    $bg = imagecolorallocate($image, 0, 0, 0);
    $w = imagecolorallocate($image, 255, 255, 255);
    $b = imagecolorallocate($image, 0, 0, 0);
	$style = array($b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,
					$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w);
    imagesetstyle($image,$style);
    imagesetthickness($image,1);
    
    //check the previous color stored for bgcolor in the session
    if(!empty($bgChange)){
    	  //$bgcolor = $custarray[$oId]['bgcolor'];
    	   $backgrdcolor =  $bgChange;
    }else{
    	 if (empty($bgcolor)) $bgcolor = $custarray[$oId]['bgcolor'];
    	 $backgrdcolor = $bgcolor;
    	 //echo "bgc ".$bgcolor;
    }
   
    if (empty($backgrdcolor)){
    	imagepolygon($image,
	                 array (
	                        $xcuststart, $ycuststart,
	                        $xcuststart, $ycuststart+$customizationAreaHeight,
	                        $xcuststart+$customizationAreaWidth, $ycuststart+$customizationAreaHeight,
	                        $xcuststart+$customizationAreaWidth, $ycuststart
	                 ),
	                 4,
	                 IMG_COLOR_STYLED);
	                      
    }else{
    	$bgcolorRGB = html2rgb($backgrdcolor);
	    list($rgb, $r, $g, $b) = split('[(,)]', $bgcolorRGB);
    	$backgroundColor = imagecolorallocate($image,$r,$g,$b);
    	imagefilledpolygon($image,
	                 array (
	                        $xcuststart, $ycuststart,
	                        $xcuststart, $ycuststart+$customizationAreaHeight,
	                        $xcuststart+$customizationAreaWidth, $ycuststart+$customizationAreaHeight,
	                        $xcuststart+$customizationAreaWidth, $ycuststart
	                 ),
	                 4,
	                 $backgroundColor);
	    	                 
    }
    $len = 4;
    if ($isJPG){
		$tempBaseFile = './images/customized/cust1'.time().get_rand_id($len).'.jpg';
	    if(!imagejpeg($image,$tempBaseFile))
			$weblog->info("unable to create $tempBaseFile in customizationtoolsapis.php");
	}else{
		$tempBaseFile = './images/customized/cust1'.time().get_rand_id($len).'.png';
	    if(!imagepng($image,$tempBaseFile))
			$weblog->info("unable to create $tempBaseFile in customizationtoolsapis.php");
		
	}

	addToTempImagesList($tempBaseFile);

	  if(!empty($custarray[$oId]['imageAfterResizing'])){
			$isresizedimage = 1;
	  	 $uploadedImage  = $custarray[$oId]['imageAfterResizing']; // resized image
		}
	  else	 
		  $uploadedImage  = $chosenProductConfiguration[$customizationAreaId."_".$oId."_"."uploadedImagePath"]; // orignal uploaded 
		  
	  if(!empty($uploadedImage)){  
        if ($isJPG){
	        $pasteImage=imagecreatefromjpeg($uploadedImage);
	    }else{
	        $pasteImage=imagecreatefrompng($uploadedImage);
		}
		$custarray[$oId]['pasteimagepath'] = $uploadedImage;
		$imageproperties =getimagesize($uploadedImage);
		$width_orig=$imageproperties[0];
		$height_orig=$imageproperties[1];
		$ratio_orig = $width_orig/$height_orig;
		$widthMax = max($customizationAreaWidth,$width_orig);
		$heightMax = max($customizationAreaHeight,$height_orig);
		$custarray[$oId]['pasteimagepath'] = $uploadedImage;
		$widthinCA =$customizationAreaWidth;//width of the image pasted in customization area
		$heightinCA=$customizationAreaHeight;//height of the image pasted in customization area
		
		$customizatio_aspect_ratio =$customizationAreaWidth/$customizationAreaHeight;

		$srcx=0;
		$srcy=0;
		$height_new=0;
		$width_new=0;
		if($customizatio_aspect_ratio >= 1)
		{
			$height_new = $width_orig/$customizatio_aspect_ratio;
			$width_new=$width_orig;
			$srcy=($height_orig-$height_new)/2;
		}
		else
		{
			$width_new = $height_orig * $customizatio_aspect_ratio;
			$height_new=$height_orig;
			$srcx=($width_orig-$width_new)/2;
		}


		$tempimg ='';
		$path='';
		$imageCropper = new mkImageTransform();
		$imageCropper->sourceFile = $uploadedImage;
		$imageCropper->maintainAspectRatio = 0;
		$imageCropper->resizeToWidth = $width_new;
		$imageCropper->resizeToHeight = $height_new;	
 	 
		if ($imageproperties['mime'] =='image/jpeg'){
			$imageCropper->jpegOutputQuality = 100;
			$imageCropper->targetFile = './images/customized/cust_crop.'.time().get_rand_id($len).'.jpg';
		}else{
			$imageCropper->targetFile = './images/customized/cust_crop.'.time().get_rand_id($len).'.png';
		}
		
		$imageCropper->crop($srcx,$srcy);

		$imageManipulator = new mkImageTransform();
        $imageManipulator->sourceFile =$imageCropper->targetFile;
	    if ($isJPG){
	        $imageManipulator->jpegOutputQuality = 100;
	        $imageManipulator->targetFile = './images/customized/cust.'.time().get_rand_id($len).'.jpg';
	    }else{
	        $imageManipulator->targetFile = './images/customized/cust.'.time().get_rand_id($len).'.png';
		}
		addToTempImagesList($imageManipulator->targetFile);
		$len =4;
        $imageManipulator->maintainAspectRatio = 0;
		if(!$isresizedimage)
			//list($widthinCA,$heightinCA) = _rethinkImageDimensions($customizationAreaId, $customizationAreaHeight, $customizationAreaWidth,$width_orig, $height_orig);
        $imageManipulator->resizeToWidth = $widthinCA;
        $imageManipulator->resizeToHeight = $heightinCA;
        $imageManipulator->resize();
	    if ($isJPG){
	        $pasteImage = imagecreatefromjpeg($imageManipulator->targetFile);
	    }else{
    	    $pasteImage = imagecreatefrompng($imageManipulator->targetFile);
		}
        //store the resized file path in the session array
        $custarray[$oId]['pastefile'] = $imageManipulator->targetFile;
        //by default dx,dy are 0
        $dx = $custarray[$oId][dxstart]?$custarray[$oId][dxstart]:($customizationAreaWidth - $widthinCA)/2;
        $dy = $custarray[$oId][dystart]?$custarray[$oId][dystart]:($customizationAreaHeight - $heightinCA)/2;
        //$dx = ($customizationAreaWidth - $widthinCA)/2;
        //$dy = ($customizationAreaHeight - $heightinCA)/2;
        
        $dx=($dx<0)?0:$dx;
        $dy=($dy<0)?0:$dy;

        imagecopyresampled($image, $pasteImage, $xcuststart+$dx,$ycuststart+$dy, 0, 0, $widthinCA, $heightinCA, $widthinCA, $heightinCA);
		
	    $len = 4;
	    if ($isJPG){
    	    $tempCustImageFile = "./images/customized/custtemp".time().get_rand_id($len).".jpg";
			imagejpeg($image,$tempCustImageFile);
			$baseimage = imagecreatefromjpeg($tempCustImageFile);
			#for bg Image
			if(!empty($bgImage)){
				$tempCustBgImageFile = "./images/customized/bgimages/custtemp".time().get_rand_id($len).".jpg";
				imagejpeg($bgImage,$tempCustBgImageFile);
				$baseBgimage = imagecreatefromjpeg($tempCustBgImageFile);
			}
	    }else{
    	    $tempCustImageFile = "./images/customized/custtemp".time().get_rand_id($len).".png";
			if(!imagepng($image,$tempCustImageFile))
			   $weblog->info("unable to create $tempCustImageFile in customizationtoolsapis.php");
			$baseimage = imagecreatefrompng($tempCustImageFile);
			#for bg Image
			if(!empty($bgImage)){
				$tempCustBgImageFile = "./images/customized/bgimages/custtemp".time().get_rand_id($len).".png";
				if(!imagepng($bgImage,$tempCustBgImageFile))
			       $weblog->info("unable to create $tempCustBgImageFile in customizationtoolsapis.php");
				$baseBgimage = imagecreatefrompng($tempCustBgImageFile);
			}
		}
		addToTempImagesList($tempCustImageFile);
		if(!empty($tempCustBgImageFile)){
			addToTempImagesList($tempCustBgImageFile);
		}


        $custarray[$oId]['dxstart'] = $dx;
        $custarray[$oId]['dystart'] = $dy;
        $custarray[$oId]['generatedimage'] = $tempCustImageFile;
        $custarray[$oId]['generatedimagenotext'] = $tempCustImageFile;
		
        $custarray[$oId]['orientationid'] = $oId;
       	$custarray[$oId]['bgcolor'] = $backgrdcolor;
       	// added common image for both orientation 
       	$custarray['common_generated_img'] = $tempCustImageFile;
        $custarray['common_generated_final_image'] = $tempCustImageFile;
       	//this code is for handling the situation wherein somebody uploads an image after putting some text in the customization
       	//area.
	  }
	  else{
	  	(!empty($backgrdcolor))	? $custarray[$oId]['bgcolor'] = $backgrdcolor : '';
	    $custarray[$oId]['generatedimage'] = $tempBaseFile;
        $custarray[$oId]['generatedimagenotext'] = $tempBaseFile;
	    $custarray[$oId]['orientationid'] = $oId;
        $custarray['common_generated_img'] = $tempBaseFile; // common image for both orientation
        $custarray['common_generated_final_image'] = $tempCustImageFile;
	  }
	 	
   

}
/** end ***/

function rotateImage($pasteFile,$degrees){
    $imageManipulator = new mkImageTransform();
    $imageManipulator->sourceFile = $pasteFile;
    $imageManipulator->jpegOutputQuality = 100;
    //$imageManipulator->chmodValue = "777";
    $imageManipulator->targetFile = $pasteFile;
    $imageManipulator->rotate($degrees,0);
    return $imageManipulator->targetFile;
}

function resizeImage($pasteFile,$percent,&$custarray,$customizationAreaWidth,$customizationAreaHeight){
	global $isJPG, $orientationId;
	if ($isJPG){
	    $pasteImage=imagecreatefromjpeg($pasteFile);
	}else{
	    $pasteImage=imagecreatefrompng($pasteFile);
	}
    list($width_orig, $height_orig) = getimagesize($pasteFile);
    $ratio_orig = $width_orig/$height_orig;

    $widthinCA = $custarray[$orientationId]['widthinCA'];//width of the image pasted in customization area
    $heightinCA = $custarray[$orientationId]['heightinCA'];//height of the image pasted in customization area
    if (($widthinCA==0) && ($heightinCA==0)){
        //echo "setting width and height explicitly";
        if (($width_orig >= $customizationAreaWidth) && ($width_orig >= $height_orig)){
            $widthinCA = $customizationAreaWidth;
            $heightinCA = $widthinCA/$ratio_orig;
        }elseif (($height_orig >= $customizationAreaHeight) && ($height_orig >= $width_orig)){
            $heightinCA = $customizationAreaHeight;
            $widthinCA = $ratio_orig * $heightinCA;
        }elseif (($width_orig < $customizationAreaWidth) && ($width_orig >= $height_orig)){
            $widthinCA = $width_orig;
            $heightinCA = $widthinCA/$ratio_orig;
        }elseif (($height_orig < $customizationAreaHeight) && ($height_orig >= $width_orig)){
            $heightinCA = $height_orig;
            $widthinCA = $ratio_orig * $heightinCA;
        }
    }

    $imageManipulator = new mkImageTransform();
    $imageManipulator->sourceFile = $pasteFile;
    $imageManipulator->jpegOutputQuality = 100;
    $imageManipulator->maintainAspectRatio = 1;
    $widthinCA = $widthinCA*(1.0 + ($percent/100));
    $heightinCA = $heightinCA*(1.0 + ($percent/100));
    $imageManipulator->resizeToWidth = $widthinCA;
    $imageManipulator->resizeToHeight = $heightinCA;
    //set the new dimensions in session array
    $custarray[$orientationId]['widthinCA']= $widthinCA;//width of the image pasted in customization area
    $custarray[$orientationId]['heightinCA']= $heightinCA;//height of the image pasted in customization area
    $imageManipulator->targetFile = $pasteFile;
    $imageManipulator->resize();
    return $imageManipulator->targetFile;
}



function autosizeImage($pasteFile,$autosize,&$custarray,$customizationAreaWidth,$customizationAreaHeight){
	global $isJPG,$orientationId;
	if ($isJPG){
	    $pasteImage=imagecreatefromjpeg($pasteFile);
	}else{
	    $pasteImage=imagecreatefrompng($pasteFile);
	}
    list($width_orig, $height_orig) = getimagesize($pasteFile);
    $ratio_orig = $width_orig/$height_orig;
    if ($autosize == 'h'){
        $heightinCA = $customizationAreaHeight;
        $widthinCA = $heightinCA * $ratio_orig;
        $custarray[$orientationId]['dystart'] = 0;
        $custarray[$orientationId]['dxstart'] = ($customizationAreaWidth - $widthinCA)/2;
    }elseif ($autosize == 'w'){
        $widthinCA = $customizationAreaWidth;
        $heightinCA = $widthinCA/$ratio_orig;
        $custarray[$orientationId]['dxstart'] = 0;
        $custarray[$orientationId]['dystart'] = ($customizationAreaHeight - $heightinCA)/2;
    }
    $imageManipulator = new mkImageTransform();
    $imageManipulator->sourceFile = $pasteFile;
    $imageManipulator->jpegOutputQuality = 100;
    //$imageManipulator->chmodValue = "777";
    $imageManipulator->maintainAspectRatio = 1;
    $imageManipulator->resizeToWidth = $widthinCA;
    $imageManipulator->resizeToHeight = $heightinCA;
    //set the new dimensions in session array
    $custarray[$orientationId]['widthinCA']= $widthinCA;//width of the image pasted in customization area
    $custarray[$orientationId]['heightinCA']= $heightinCA;//height of the image pasted in customization area
    $imageManipulator->targetFile = $pasteFile;
    $imageManipulator->resize();
    return $imageManipulator->targetFile;
}

function func_set_customize_area_width_height($pasteFile,$customizationAreaWidth,$customizationAreaHeight){
	global $widthinCA,$heightinCA,$width_orig,$height_orig;
	
	
	if (($width_orig > $customizationAreaWidth) && ($width_orig >= $height_orig)){
	    $widthinCA = $customizationAreaWidth;
	    $heightinCA = $widthinCA/$ratio_orig;
	    if ($heightinCA > $customizationAreaHeight){
	    	$heightinCA = $customizationAreaHeight;
	    	$widthinCA = $heightinCA * $ratio_orig;
	    }
	}elseif (($height_orig > $customizationAreaHeight) && ($height_orig >= $width_orig)){
	    $heightinCA = $customizationAreaHeight;
	    $widthinCA = $ratio_orig * $heightinCA;
	    if ($widthinCA > $customizationAreaWidth){
	    	$widthinCA = $customizationAreaWidth;
	    	$heightinCA = $widthinCA/$ratio_orig;
	    }
	}elseif (($width_orig < $customizationAreaWidth) && ($width_orig >= $height_orig)){
	    $widthinCA = $width_orig;
	    $heightinCA = $widthinCA/$ratio_orig;
	    if ($heightinCA > $customizationAreaHeight){
	    	$heightinCA = $customizationAreaHeight;
	    	$widthinCA = $heightinCA * $ratio_orig;
	    }
	}elseif (($height_orig < $customizationAreaHeight) && ($height_orig >= $width_orig)){
	    $heightinCA = $height_orig;
	    $widthinCA = $ratio_orig * $heightinCA;
	    if ($widthinCA > $customizationAreaWidth){
	    	$widthinCA = $customizationAreaWidth;
	    	$heightinCA = $widthinCA/$ratio_orig;
	    }
	}
}



function func_set_customization_boundry(&$image,$custarray,$orientationId){
    global $customizationAreaWidth,$customizationAreaHeight;
	//this is specific to each product style and cust area
	$xcuststart=$custarray[$orientationId]['x'];
	$ycuststart=$custarray[$orientationId]['y'];

   //check the previous color stored for bgcolor in the session
   if (empty($bgcolor))
	   $bgcolor = $custarray[$orientationId]['bgcolor'];

	
	if (empty($bgcolor)){
	imagepolygon($image,
                 array (
                        $xcuststart, $ycuststart,
                        $xcuststart, $ycuststart+$customizationAreaHeight,
                        $xcuststart+$customizationAreaWidth, $ycuststart+$customizationAreaHeight,
                        $xcuststart+$customizationAreaWidth, $ycuststart
                 ),
                 4,
                 IMG_COLOR_STYLED);
	 
	}else{
		$bgcolorRGB = html2rgb($bgcolor);
	    list($rgb, $r, $g, $b) = split('[(,)]', $bgcolorRGB);
	
		$backgroundColor = imagecolorallocate($image,$r,$g,$b);
		imagefilledpolygon($image,
	                 array (
	                        $xcuststart, $ycuststart,
	                        $xcuststart, $ycuststart+$customizationAreaHeight,
	                        $xcuststart+$customizationAreaWidth, $ycuststart+$customizationAreaHeight,
	                        $xcuststart+$customizationAreaWidth, $ycuststart
	                 ),
	                 4,
	                 $backgroundColor);
		
	}
}

function _rethinkImageDimensions($customizationAreaId, $customizationAreaHeight, $customizationAreaWidth, $width_orig, $height_orig){
	global $sql_tbl;
	$query = "select height_in,width_in from {$sql_tbl['mk_customization_orientation']} where area_id='{$customizationAreaId}'";
	$orig_dim = func_query_first($query);
	if($orig_dim['height_in']*96 <= $height_orig && $orig_dim['width_in']*96 <= $width_orig){
		if($customizationAreaHeight*$width_orig > $customizationAreaWidth*$height_orig){
			$returnval['height'] = floor($height_orig*($customizationAreaWidth/$width_orig));
			$returnval['width'] = $customizationAreaWidth;			
		}else{
			$returnval['height'] = $customizationAreaHeight;			
			$returnval['width'] = floor($width_orig*($customizationAreaHeight/$height_orig));
		}
	}elseif($orig_dim['height_in']*96 <= $height_orig && $orig_dim['width_in']*96 > $width_orig){
		$returnval['height'] = $customizationAreaHeight;
		$returnval['width'] = floor($width_orig*($customizationAreaHeight/$height_orig));
	}elseif($orig_dim['height_in']*96 > $height_orig && $orig_dim['width_in']*96 <= $width_orig){
		$returnval['height'] = floor($height_orig*($customizationAreaWidth/$width_orig));
		$returnval['width'] = $customizationAreaWidth;
	}else{
		if( $height_orig*$orig_dim['height_in'] > $width_orig*$orig_dim['height_in'] ) {
			$returnval['height'] = $customizationAreaWidth * ($height_orig/($orig_dim['width_in']*96));
			$returnval['width'] = $customizationAreaWidth * ($width_orig/($orig_dim['width_in']*96));
		}else {
			$returnval['height'] = $customizationAreaHeight * ($height_orig/($orig_dim['height_in']*96));
			$returnval['width'] = $customizationAreaHeight * ($width_orig/($orig_dim['height_in']*96));
		}
	}
	return array($returnval['width'],$returnval['height']);
}
?>
