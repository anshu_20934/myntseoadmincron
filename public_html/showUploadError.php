<?php
require "./auth.php";


$fileUploaded = $XCART_SESSION_VARS['fileUploaded'];
$fileUploadedMime = $XCART_SESSION_VARS['fileUploadedMime'];
$fileUploadedSize = $XCART_SESSION_VARS['fileUploadedSize'];

if ($fileUploaded != null && $fileUploaded == "incorrect") {
	x_session_unregister("fileUploaded");
	$smarty->assign("error", "error");
}
if ($fileUploadedMime != null && $fileUploadedMime == "incorrect") {
	x_session_unregister("fileUploadedMime");
	$smarty->assign("error", "errorMime");
}
if ($fileUploadedSize != null && $fileUploadedSize == "incorrect") {
	x_session_unregister("fileUploadedSize");
	$smarty->assign("error", "errorSize");
} 


func_display("showUploadError.tpl", $smarty);

?>
