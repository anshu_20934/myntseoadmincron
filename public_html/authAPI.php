<?php
if (!defined('AUTH_API_INIT')) {
       define('AUTH_API_INIT', 1);
}
@include "./top.inc.php";
@include "../top.inc.php";
@include "../../top.inc.php";
@include "../../../top.inc.php";

include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";


define('MYNTRA_HOME_DIR',$xcart_dir);
if (!defined('DIR_CUSTOMER')) die("ERROR: Can not initiate application! Please check configuration.");

################ LOGGERS INITIALIZED  #################
define('LOG4PHP_DIR', MYNTRA_HOME_DIR."/log4php");
define('LOG4PHP_CONFIGURATION', MYNTRA_HOME_DIR."/myntralogger.properties");
define('LOG4PHP_CONFIGURATOR_CLASS',LOG4PHP_DIR."/LoggerPropertyConfigurator");

@include_once(LOG4PHP_DIR . '/LoggerManager.php');

$logger_manager     = new LoggerManager();
$weblog             = $logger_manager->getLogger('MYNTRAWEBLOGGER');
$errorlog           = $logger_manager->getLogger('MYNTRAERRORLOGGER');
$sqllog             = $logger_manager->getLogger('MYNTRASQLLOGGER');
$debuglog           = $logger_manager->getLogger('MYNTRADEBUGLOGGER');
$apilog             = $logger_manager->getLogger('WMSAPILOGGER');
$portalapilog		= $logger_manager->getLogger('PORTALAPILOGGER');
$solrlog            = $logger_manager->getLogger('SOLRLOGGER');
$maillog 			= $logger_manager->getLogger('MAILLOGGER');

if (defined('REPORT_SCRIPT')){
	include_once $xcart_dir."/admininit.php";
}else{
	include_once $xcart_dir."/init.php";
}

$cashback_gateway_status = FeatureGateKeyValuePairs::getFeatureGateValueForKey('cashbackenabled');
if($cashback_gateway_status == "internal") {
	$pos = strpos($XCART_SESSION_VARS['login'], "@myntra.com");
	if($pos === false)
	$cashback_gateway_status = "off";
	else
	$cashback_gateway_status = "on";
}

?>
