{extends file="layout.tpl"}

{block name=body}
<script>
Myntra.Data.pageName="home";
Myntra.Data.bannerCycleInterval = {$bannerCycleInterval};
</script>
<div class="mk-one-column home-page">
	<section class="mk-site-main">
        {include file="inc/home-slideshow.tpl"}
        <div class="mk-promises gray">{$newUiTopCpsMessage}</div>
        {include file="inc/newTabWidget.tpl"}
        {include file="inc/home-content-new.tpl"}    	
	</section>
</div>
{/block}
{block name=macros}
    {include file="macros/base-macros.tpl"}
{/block}
