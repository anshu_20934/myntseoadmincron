{extends file="layout.tpl"}

{block name=body}
<script>
Myntra.Data.pageName="home";
</script>
<div class="mk-one-column home-page">
	<section class="mk-site-main">
        {include file="inc/home-slideshow.tpl"}
		<div class="mk-recent-viewed mk-hide mk-carousel-four mk-carousel-five rs-carousel mk-inline-size-selector"></div>
		{include file="inc/most-popular-widget.tpl"}
        {include file="inc/home-content.tpl"}
	</section>
</div>
{/block}
{block name=macros}
    {include file="macros/base-macros.tpl"}
{/block}
