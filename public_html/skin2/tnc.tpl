{extends file="layout.tpl"}

{block name=body}
{* Followin template given by marketing. Just putting it as it is *}
{literal}
<style>
html {
    text-transform: none;
}
b {
    font-weight: bold;
}
td.big {
    padding: 7px;
}
/* - - - - - HOTMAIL FIXES*/
.ReadMsgBody {
width: 100%;
}.
ExternalClasas {
width: 100%;
}

.policy_desc {
    font-family:Arial, Helvetica, sans-serif;
    font-size:14px;
    color:#ffffff;
}

#mobile .coupon,#desktop .coupon {
    font-family:Arial, Helvetica, sans-serif;
    font-size:18px;
    color:#000000;
    border:1px dotted #ff0000;
    background:#f0dc87;
    }

.coupon_desc {
    font-family:Arial, Helvetica, sans-serif;
    font-size:13px;
    color:#ffffff;
}

@media screen and (max-width: 540px), screen and (max-device-width: 540px) {
body { -webkit-text-size-adjust: none;}
div[id=desktop] {
display:none !important;
width:0px !important;
overflow:hidden !important;
}
div[id=mobile] {
display:block !important;
width:100% !important;
height:auto !important;
max-height:inherit !important;
overflow:visible !important;
 } 
table[id=body]{
width:100% !important; display:block !important;
}

.accord-header{cursor:pointer; font-weight:bold; padding-bottom:10px;}
.accord-content {display: none; padding-bottom:15px; padding-left:20px; }

.bold{
    font-size:18px;
    font-weight:bold;
    margin:0px; 
    }
    
.main-head{
    font-size:26px;
    font-weight:bold;
    }

.steps {
    -webkit-border-radius: 8px;
    -moz-border-radius: 8px;
    border-radius: 8px;
    background-color:#ffffff;
    padding:15px 20px;
}
.circle {
    -webkit-border-radius: 17px;
    -moz-border-radius: 8px;
    border-radius: 35px;
    background-color:#333333;
    width:35px;
    height:35px;
    color:#cccccc;
    text-align:center;
    font-size:15px;
    font-weight:bold;
    line-height:35px;
}


</style>

{/literal}

<script type="text/javascript" >

function init() {
document.getElementById('expandablePanel-term').style.display = 'block';
}

function toggleDiv(divid,img,u,d) {
var obj=document.getElementById(divid),img=document.getElementById(img);
if (obj){
obj.style.display=obj.style.display!='none'?'none':'block';
if (img){
img.src=obj.style.display=='none'?u:d;
}
}
}

</script>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $(".accordion .accord-header").click(function() {
      if($(this).next("div").is(":visible")){
        $(this).next("div").slideUp("slow");
      } else {
        $(".accordion .accord-content").slideUp("slow");
      }
    });
  });
</script>

</head>

<body onLoad="init();">


<!-- Mobile Version -->
<div id="desktop">
<table width="775" cellspacing="0" cellpadding="0" border="0" align="center">
    <tr>
      <td>
      
               <tr><td height="25" style="font-size:5px;">&nbsp;</td></tr>
                
                <tr><td height="70" align="center" style="font-size:24px; font-weight:bold; border-top:1px solid #cccccc;">Program Terms and Conditions</td></tr>

                <tr><td>
                
                <table width="775" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333;">
                        <tr><td height="25" style="font-size:5px;">&nbsp;</td></tr>
                
                
                        <tr><td>

1. This online “Myntra Shopping Festival” Shopping Festival is open and valid for all the registered end customers/participants of myntra.com, permanently residing in India (excluding Tamil Nadu), and above the age of 18 years as on the start date of the Festival.</br></br>
2. This is an online Shopping Festival, which shall take place on the online domain url: www.myntra.com/shop/myntra-shopping-festival starting April 24th, 2014 (the festival Period). The Shopping Festival Period is for one week and ends on April 30th, 2014 midnight (both days included). The Shopping Festival is subject to change at the absolute discretion of Myntra.</br></br>
3. Winners for the 1st, 2nd and 3rd prizes shall be announced by a lucky draw on May 5th, 2014. Winners for the daily and hourly prizes shall also be announced on May 5th, 2014 only.
4. The registered end customers shall be required to log in with their respective valid registered email id and shop during the shopping festival to win the prize. However, there is no compulsion to participate in the Shopping Festival.</br></br>
5. To participate in the Shopping Festival the customer needs to shop at Myntra with a valid registered email id in myntra.com. Basis the value of shopping the customer does, they could qualify for various prizes that they can win on a daily basis and at the end of the Myntra Shopping Festival. The prizes that the customers can win and the conditions to qualify are as follows:-</br></br>
                        </td></tr>
                 </table>
                
                <div id="expandableArea-term">
                <div id="expandablePanel-term" style="display : block">
                    <table width="775" border="1" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333;">
                
            <colgroup>
                <col width="130"/>
            </colgroup>
            <colgroup>
                <col width="166"/>
            </colgroup>
            <colgroup>
                <col width="172"/>
                <col width="75"/>
            </colgroup>
            <tbody>
                <tr>
                    <td width="130" height="37">
                        <p align="CENTER">
                            <strong>Prize</strong>
                        </p>
                    </td>
                    <td width="166">
                        <p align="CENTER">
                            <strong>Who gets this</strong>
                        </p>
                    </td>
                    <td width="172">
                        <p align="CENTER">
                            <strong>Criteria for Participation</strong>
                        </p>
                    </td>
                    <td width="75">
                        <p align="CENTER">
                            <strong>Result Declaration Date</strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" width="585" height="6">
                        <p align="CENTER">
                            <strong>Shopping Festival (24th - 30th Apr)</strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" width="585" height="6">
                        <p align="CENTER">
                            <strong>Top Shopper / Lucky Draw prizes</strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td width="130" height="88">
                        <p align="CENTER">
                            Hyundai Xcent car Base Model Petrol Variant
                        </p>
                    </td>
                    <td width="166">
                        <p align="CENTER">
                            1st prize based on lucky draw
                        </p>
                    </td>
                    <td width="172">
                        <p align="CENTER">
                            All customers having purchased merchandize worth more than Rs.15000 during the shopping festival are eligible for lucky draw
                        </p>
                    </td>
                    <td width="75">
                        <p align="CENTER">
                            5th May
                        </p>
                    </td>
                </tr>
                <tr>
                    <td width="130" height="88">
                        <p align="CENTER">
                            Travel voucher worth Rs. 2 lacs
                        </p>
                    </td>
                    <td width="166">
                        <p align="CENTER">
                            2nd prize based on lucky draw
                        </p>
                    </td>
                    <td width="172">
                        <p align="CENTER">
                            All customers having purchased merchandize worth more than Rs.15000 during the shopping festival are eligible for lucky draw
                        </p>
                    </td>
                    <td width="75">
                        <p align="CENTER">
                            5th May
                        </p>
                    </td>
                </tr>
                <tr>
                    <td width="130" height="88">
                        <p align="CENTER">
                            Sony LED TV worth 1.5 lacs
                        </p>
                    </td>
                    <td width="166">
                        <p align="CENTER">
                            3nd prize based on lucky draw
                        </p>
                    </td>
                    <td width="172">
                        <p align="CENTER">
                            All customers having purchased merchandize worth more than Rs.15000 during the shopping festival are eligible for lucky draw
                        </p>
                    </td>
                    <td width="75">
                        <p align="CENTER">
                            5th May
                        </p>
                    </td>
                </tr>
                <tr>
                    <td width="130" height="37">
                        <p align="CENTER">
                            Gold voucher worth Rs 1 lac from Tanishq
                        </p>
                    </td>
                    <td width="166">
                        <p align="CENTER">
                            Top shopper of the week (Men)
                        </p>
                    </td>
                    <td width="172">
                        <p align="CENTER">
                            Highest value of shopping basis the Weekly leaderboard results
                        </p>
                    </td>
                    <td width="75">
                        <p align="CENTER">
                            5th May
                        </p>
                    </td>
                </tr>
                <tr>
                    <td width="130" height="38">
                        <p align="CENTER">
                            Gold voucher worth Rs 1 lac from Tanishq
                        </p>
                    </td>
                    <td width="166">
                        <p align="CENTER">
                            Top shopper of the week (Women)
                        </p>
                    </td>
                    <td width="172">
                        <p align="CENTER">
                            Highest value of shopping basis the Weekly leaderboard results
                        </p>
                    </td>
                    <td width="75">
                        <p align="CENTER">
                            5th May
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" width="585" height="6">
                        <p align="CENTER">
                            <strong>Daily prizes - 24th to 29th </strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td width="130" height="37">
                        <p align="CENTER">
                            iPhone 5s (16 GB)
                        </p>
                    </td>
                    <td width="166">
                        <p align="CENTER">
                            Daily top shopper (Men)
                        </p>
                    </td>
                    <td width="172">
                        <p align="CENTER">
                            Highest value of shopping basis the daily leaderboard results
                        </p>
                    </td>
                    <td width="75">
                        <p align="CENTER">
                            5th May
                        </p>
                    </td>
                </tr>
                <tr>
                    <td width="130" height="38">
                        <p align="CENTER">
                            iPhone 5s (16 GB)
                        </p>
                    </td>
                    <td width="166">
                        <p align="CENTER">
                            Daily top shopper (Women)
                        </p>
                    </td>
                    <td width="172">
                        <p align="CENTER">
                            Highest value of shopping basis the daily leaderboard results
                        </p>
                    </td>
                    <td width="75">
                        <p align="CENTER">
                            5th May
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" width="585" height="6">
                        <p align="CENTER">
                            <strong>Final Day prizes (30th – 12 noon to midnight)</strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td width="130" height="37">
                        <p align="CENTER">
                            iPad Mini (16GB Wifi)
                        </p>
                    </td>
                    <td width="166">
                        <p align="CENTER">
                            Top shopper of each hour
                        </p>
                    </td>
                    <td width="172">
                        <p align="CENTER">
                            Highest value of shopping basis the daily leaderboard results
                        </p>
                    </td>
                    <td width="75">
                        <p align="CENTER">
                            5th May
                        </p>
                    </td>
                </tr>
            </tbody>

               </table>


                <table width="775" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333;">
                        <tr><td height="25" style="font-size:5px;">&nbsp;</td></tr>

                        <tr><td>
6. It is clarified that:</br></br>
&nbsp;&nbsp;&nbsp;&nbsp;a. “Car” shall mean and imply a Hyundai Xcent (Variant:- Base Model, petrol Variant), at the showroom Price;  “Price” shall mean the ex-showroom basic price of the car and not the “on-road” price and specifically excludes all taxes, registration costs, insurance, octroi, levies, etc. (as may be applicable).</br></br>
&nbsp;&nbsp;&nbsp;&nbsp;b. “Free Couple holiday Package” shall mean an international holiday package to an international destination, excluding the visa requirement or any other expenses/ charges, which shall be borne by the winner. Cancellation of the holiday for any reason whatsoever shall be at the winner’s cost and Myntra.com shall have no role or liability. The travel vouchers are a single use voucher.  Travel package partners:- Yatra.com. The unclaimed amount from the package is non-refundable. If a customer wishes to redeem his voucher for a package of a higher cost, then the customer can pay for the differential amount and redeem his travel voucher. The travel voucher will come with a maximum validity of 6 months. The customer can redeem the voucher for an international Holiday and the final package itinerary will depend on the package he redeems the voucher for.</br></br> 
&nbsp;&nbsp;&nbsp;&nbsp;c.  “Sony TV worth 1.5 Lacs:”- The Sony TV model for the winner is as follows - 55 inches (139 cm) W850 Series Full HD 3D BRAVIA TV with an approximate MRP price of Rs. 1.5 Lacs. This does not include installation. Installation charges to be borne by the customer.</br></br>
&nbsp;&nbsp;&nbsp;&nbsp;d. “Tanishq Gift Vouchers” :- Each winner will be given 4 Voucher worth Rs. 25000/- Each. The said Vouchers need to be redeemed at Tanishq any of the Outlets across. Vouchers cannot be redeemed online on Tanishq website. Multiple Vouchers can be combined for a single purchase. The Gift Voucher will have a maximum Validity of 6 months from the date of issue.</br></br>
&nbsp;&nbsp;&nbsp;&nbsp;e. All forms of taxes, including income tax, gift tax, levies or other taxes, registration charges, insurance, etc. will be borne by the winner.</br></br>
7. A single customer can qualify for multiple prizes during the Shopping Festival depending on the value of his purchase on Myntra during the Shopping Festival. </br></br>
8. Any purchase made before and post the dates of the Myntra Shopping Festival shall be void and will not be considered for the customers’ participation. Purchases made by Gold and Platinum customers of Myntra on 23rd April 2014 will also be considered for the overall prizes.</br></br>
9. Terms and Conditions of 100% cashback offer.</br></br>
10. 100% cashback will be provided in the form of a special cashback coupon with every order placed on Myntra.</br></br>
&nbsp;&nbsp;&nbsp;&nbsp;a. The cashback allows you a maximum discount equivalent to your voucher value or a 40% off, whichever is lower. For e.g. if you place an order worth Rs.1200, a coupon of 40% off with a maximum benefit of 1200 on a subsequent order will be generated.</br></br>
&nbsp;&nbsp;&nbsp;&nbsp;b. This coupon can be redeemed only with ONE subsequent order.</br></br>
&nbsp;&nbsp;&nbsp;&nbsp;c. For subsequent orders placed using this coupon, no additional cashback coupons will be provided</br></br>
&nbsp;&nbsp;&nbsp;&nbsp;d. Cashback coupons generated with orders placed during the Shopping Festival will be valid only till 15th May 2014</br></br>
&nbsp;&nbsp;&nbsp;&nbsp;e. If a customer cancels the order placed during Shopping Festival, the cashback coupons generated will automatically be invalidated. If an order has been already placed during the cashback coupon, this order also will be cancelled.</br></br>
&nbsp;&nbsp;&nbsp;&nbsp;f. These coupons will not be applicable on certain brands / products and categories.</br></br>
11. The time of order as per Myntra’s records will be considered as time of purchase for the daily and overall prizes.</br></br>
12. Myntra shall not be responsible if some purchase is not registered or is lost due to any network problems such as breakdown of machinery, unclear network, disruption in the network and/or the cost(s) charged by the network operator(s). Any dispute in connection to the same shall be settled between the end customer and the network operator without involving Myntra.</br></br>

13. The winners shall be informed individually through their Myntra registered email id/ phone number. The winners will then be contacted and if found authentic and satisfying all required conditions, will be eligible for the prize. In case the winners do not acknowledge the communication (call or email) from Myntra informing the lucky winner(s) within 48 hours, the winner may be deemed, in Myntra’s sole discretion, to have forfeited his win. Myntra.com reserves the right to reallocate the prize to another participant.</br></br>

14. Winners agree that Myntra.com has the sole right to decide all matters and disputes arising from this Shopping Festival and that all decisions of Myntra.com are final and binding.</br></br>

15. The prizes are personal to the winners and are not transferrable and cannot be sold under any circumstances. Prizes cannot be exchanged for cash or kind.</br></br>

16. All the participants shall be required to ensure that the mobile number(s), e-mail address and/or other details provided by them to Myntra.com via the website or otherwise are true, accurate and in use. Any liability, consequence or claim arising on account of any incorrect or outdated information provided by the participants to Myntra.com shall solely be borne by the affected/concerned participants. Myntra.com shall at no time be required to verify the accuracy and/or correctness of the information so provided by the participants. </br></br>

17. All lucky customers will receive the prizes announced during the promotion by Myntra within 40 working days starting from 5th of May 2014.</br></br>
18. The colour of the Prizes is subject to the availability at the time of the dispatch by Myntra or business partners.
19. Any cancellation / non-acceptance of an order for any reason whatsoever will render the order number in-eligible for the lucky draw or for any other gift under this contest/promotion or delivery of the prize won. This is valid only for customer initiated cancellations. Customers for whom Myntra has cancelled the order will still be considered for the prizes.</br></br>
20. If a customer who is an announced winner returns part of or all items in the order placed during shopping festival, he/she will not be eligible for any of the prizes if an online refund is processed for the order.</br></br>
21. The prizes are subject to standard warranty and after sales service conditions of the respective brands/ dealers. Myntra will not be responsible for any product/service related query/complaint and the winner has to coordinate directly with the dealer for their preference.</br></br>
22. No requests will be entertained for the exchange of one prize/gift for another.</br></br>
23. If Myntra has suspicion or knowledge, that any entrant has been involved in any fraudulent or illegal activity, Myntra reserves the right to disqualify that entrant and any related entrants. </br></br>
24. Myntra does not warrant that the Call Center will run concurrently and error free during and/or after the promotion period, and Myntra shall not be liable for issues related to technical and/or human error whatsoever.</br></br>

25. Further, you are responsible for maintaining the confidentiality of your mobile phones, e-mail accounts and passwords. </br></br>

26. Myntra.com’s decision on the verification of all the participants shall be final and no correspondence will be entered into in relation to any decision taken by Myntra.com.</br></br>

27. Myntra.com may at its sole discretion choose to publish the name of the winner(s) on its website and/or any other publicity media that Myntra.com may, at its sole discretion, choose to employ without any further compensation to the winning participant. You waive your right to object to such publication and by participating in this Shopping Festival; you grant Myntra.com the right to use your name for the above mentioned purpose.</br></br>

28. Myntra.com accepts no responsibility for any tax implications that may arise from the prize winnings. Tax is to be borne by the winner. Winners will have to bear incidental costs, if any, that may arise for redemption of the Prizes.</br></br>.

29. Myntra.com shall not be responsible for any loss, injury or any other liability arising due to participation by any person in the Shopping Festival.</br></br>

30. Myntra.com shall not be liable for any loss or damage due to Act of God, Governmental actions, other force majeure circumstances, or any other reason beyond its control, and shall not be liable to pay any amount as compensation or otherwise.</br></br>

31. Myntra.com reserves the right to amend, modify, cancel, update or withdraw this Shopping Festival at any time without notice. Upon such premature suspension, cessation, withdrawal, termination or closure of the Shopping Festival by the Company, no person shall be entitled to claim loss of any kind whatsoever.</br></br>

32. By participating in this deal, all eligible participants agree to be bound by these Terms and Conditions, the Terms of Use, Privacy Policy and other relevant documentation that are available on Myntra.com including any modifications, alterations or updates that we make. </br></br>

33. Myntra.com employees and relatives of the employees are not eligible to participate in this Shopping Festival.</br></br> 

34. Myntra.com reserves the right, in its sole discretion, to disqualify any participant that tampers or attempts to tamper with the deal or violates these Terms and Conditions or acts in a disruptive manner.</br></br>

35. The images of the products shown are for visual representation only and may vary from the actual product.</br></br>

By participating in this Shopping Festival, you, the end customer, (and, if you are a minor, your parents or legal guardian) indemnify Myntra.com, and any of their respective parent companies, subsidiaries, affiliates, directors, officers, employees and agencies (collectively, the "Indemnified Parties") from any liability whatsoever, and waive any and all causes of action, related to any claims, costs, injuries, losses, or damages of any kind arising out of or in connection with the Shopping Festival or delivery, non-delivery, acceptance, possession, use of or inability to use any Prize The winners, and in case of Winners being below the age of 18 years their parents or guardians may, at the absolute discretion of Myntra, be required to execute a deed of release and indemnity in a form prescribed by Myntra, in order to receive the Prize, all Prizes will be awarded to the parent or guardian, as the case may be, of the winner. It is the sole responsibility of parent or guardian, as the case may be, to monitor or supervise the use of any of the prizes received by the winners. Myntra does not endorse any of the Prizes being offered under the Shopping Festival and will not accept any liability pertaining to the quality, delivery or after sales service of such products which shall be at the sole liability of such product manufacture. </br></br>

36. Pictures of Prizes shown in the Shopping Festival communication sent to the customer either through mailers or advertised on our website, are representative only and may not bear a resemblance to the actual products. Neither Myntra nor Merchant shall under any circumstances be responsible towards the same.</br></br>

37. Shopping Festival is subject to the laws of India and all disputes arising hereunder shall be subject to the jurisdiction of courts, tribunals or any other applicable forum at Bangalore. </br></br>

38. References to “Myntra” and “Myntra.com” mean Vector E-commerce Pvt. Ltd., situated at 7th Mile, Krishna Reddy Industrial Area, Kudlu Gate, Opp Macaulay High School, behind Andhra Bank ATM, Bangalore – 560068, India.</br></br>
 
                </td></tr>

                 </table>
                </div>
            
        
            </div>
                
                
                
                
                </td></tr>
                
                
                

                
            </table>
{/block}
