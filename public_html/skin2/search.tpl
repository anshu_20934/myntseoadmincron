{extends file="layout.tpl"}
{block name=navigation}
	{if !$navflag && !$bannerImageProperties.image_url && $layoutVariant neq 'test'}
		{include file="inc/navigation-collapsed.tpl"}
	{else}
		{include file="inc/navigation-default.tpl"}
	{/if}
{/block}

{block name=body}
<script>
var pageName="search";
Myntra.Data.pageName="search";
{if $searchSizeGroup}Myntra.Data.sizeGroup = "{$searchSizeGroup}";{/if}
{if $fpcSizeGroup}Myntra.Data.fpcSizeGroup = "{$fpcSizeGroup}";{/if}
{if $showStickySize}Myntra.Data.showStickySize = "{$showStickySize}";{/if}
Myntra.Search = Myntra.Search || {};
Myntra.Search.Data = Myntra.Search.Data || {};
Myntra.Search.Data = {
    noResultsPage : {if $total_items ge 1}false{else}true{/if},
    navId : "{$nav_id}",
    userQuery : {if $userQuery}true{else}false{/if},
    query : "{$query|lower}",
    priceRangeMin : parseInt({$rangeMin}),
    priceRangeMax : parseInt({$rangeMax}),
    listGroup : "{$listGroup}",
    searchCategoryList : "{$productListData.searchCategoryList}",
    searchTopCategory : "{$productListData.searchTopCategory}",
    searchSubCategoryList : "{$productListData.searchSubCategoryList}",
    searchTopSubCategory : "{$productListData.searchTopSubCategory}",
    searchBrandList: "{$productListData.searchBrandList}",
    searchTopBrand : "{$productListData.searchTopBrand}",
    searchGenderList : "{$productListData.searchGenderList}",
    searchTopGender : "{$productListData.searchTopGender}",
    searchArticleTypeList : "{$productListData.searchArticleTypeList}",
    searchTopArticleType : "{$productListData.searchTopArticleType}",
    searchColorList : "{$productListData.searchColorList}",
    searchTopColor : "{$productListData.searchTopColor}"
}

Myntra.Search.Data.stickyFiltersEnabled = '{$scrollLockVariant}' == 'test' ? true : false;
Myntra.Search.Data.Seo = Myntra.Search.Data.Seo || {};
Myntra.Search.Data.Seo.searchType = '{$searchMetaDataForGAStats.searchType}';
Myntra.Search.Data.Seo.searchResultsCount = {if $searchMetaDataForGAStats.resultsCount}{$searchMetaDataForGAStats.resultsCount}{else}0{/if};
Myntra.Search.Data.optimEnabled = {$optimEnabled};
//google conversion analytics track events
var gaEventCategory = Myntra.Search.Data.searchCategoryList.split(',').length ==  1 ? Myntra.Search.Data.searchCategoryList : '',
    gaEventArticleType = Myntra.Search.Data.searchArticleTypeList.split(',').length ==  1 ? Myntra.Search.Data.searchArticleTypeList : '',
    gaEventGender = Myntra.Search.Data.searchGenderList.split(',').length ==  1 ? Myntra.Search.Data.searchGenderList : '',
    gaEventBrand = Myntra.Search.Data.searchBrandList.split(',').length ==  1 ? Myntra.Search.Data.searchBrandList : '';

</script>
<section class="mk-site-main">
	<div class="mk-ajax-loader">
		<div class="mk-ajax-loader-msg"></div>
	</div>
	{if $LocationBreadcrumbABTest eq 'test' && $showBreadCrumb}		
	<div class="mk-search-breadcrumbs mk-location-breadcrumbs">{$locationBasedBreadCrumb}</div>
	{/if}
	<div class="mk-category-page mk-cf">
		{if $total_items ge 1}
			<div class="mk-filters-container mk-f-left">
				{if $layoutVariant eq 'test' && $searchLeftPaneBanner.image_url}
					<div>
						<a href="{$http_location}{$searchLeftPaneBanner.target_url}" onClick="_gaq.push(['_trackEvent', 'banner', Myntra.Data.pageName, '{$searchLeftPaneBanner.target_url}']);return true;">
							<img style="margin-left:-7px;" src="{myntraimage key='search_banner' src=$searchLeftPaneBanner.image_url}" alt="{$searchLeftPaneBanner.alt_text}"/>
						</a>
					</div>
				{/if}

				<div class="mk-left-nav">
					{*if $optimEnabled != 'true'*}
						{include file="inc/leftnav-filters.tpl"}
					{*/if*}
				</div>
			</div>
			<div class="mk-results-container mk-f-left">
				<div class="mk-search-contents">
					{if $layoutVariant eq 'test' && $bannerImageProperties.image_url}
						<div class="mk-search-promo-banner">							
							<a href="{$http_location}{$bannerImageProperties.target_url}" onClick="_gaq.push(['_trackEvent', 'banner', Myntra.Data.pageName, '{$bannerImageProperties.target_url}']);return true;">
								<img src="{myntraimage key='search_banner' src=$bannerImageProperties.image_url}" alt="{$bannerImageProperties.alt_text}"/>
							</a>
							{if $bannerImageProperties.multiclick eq 1}
								{if $bannerImageProperties.group_id}
									{$bannerImageProperties.image_id = "{$bannerImageProperties.image_id}-{$bannerImageProperties.group_id}"}
								{/if}
								<div class="multiContainer" id="multi{$bannerImageProperties.image_id}">
									{foreach name=multiClickData item=multiClickData from=$multiBannerData.{$bannerImageProperties.image_id}}
										<a href="{$multiClickData.target_url}" title="{$multiClickData.title}" style="left:{$multiClickData.coordinates.0}px; top:{$multiClickData.coordinates.1}px; width:{$multiClickData.width}px; height:{$multiClickData.height}px;">&nbsp;</a>
									{/foreach}
								</div>			
							{/if}
						</div>
					{/if}
					{if $bannerImageProperties.description|replace:' ':''}
						<div class="mk-fashion-story">
							{if $bannerImageProperties.title|replace:' ':''}
								<h2>{$bannerImageProperties.title}</h2>
							{/if}
							<div class="mk-detail">{$bannerImageProperties.description}</div>
							<span class="more-less">Read More</span>
						</div>
					{else}
						{if $bannerImageProperties.fashion_banners|replace:' ':''}
							<div class="mk-fashion-banners">{$bannerImageProperties.fashion_banners}</div>
						{/if}
						{if $navflag && $LocationBreadcrumbABTest neq 'test'}
							<h1>{$navString}</h1>
						{else}
							<h1>
								{if $clickedFromBC neq 1}
									<span class="search-icon"></span> 
								{/if}
								{if $h1 && $h1|trim neq ""}{$h1}{elseif $synonymQuery && $synonymQuery|trim neq ""}{$synonymQuery}{else}{$query|truncate:40|replace:"-":" "|capitalize}{/if}</h1>
						{/if}
						{if $query_level=='FULL'}
							<div class="mk-sub-text mk-help-text">NO MATCHES FOUND / SHOWING RESULTS FOR PARTIAL MATCHES INSTEAD</div>
						{elseif $synonymQuery && $synonymQuery|trim neq ""}
							<div class="mk-sub-text mk-help-text"> SHOWING RESULTS FOR <span class="mk-spl-text"><a class="link-synonym" href="{$query}">{$query}</a></span> INSTEAD</div>
						{else}
						{/if}
					{/if}
					<div class="mk-product-count">{if $total_items}{$total_items}{else}no{/if} Products</span> found</div>
						<div class="mk-product-sizes"><div><span class="mk-size-text">Showing Saved Sizes:</span> </div></div>
						{include file="inc/sort-filters.tpl"}
						<div class="mk-search-grid" id="mk-search-results">
							<ul class="mk-cf">{*if $optimEnabled != 'true'*}
								{include file="inc/product_unit.tpl"}
							{*/if*}</ul>
						</div>
						{if $page_count gt 1}
							<div class="mk-infscroll-loader {if $optimEnabled == 'true'}mk-hide{/if}">
								<a class="mk-more-products-link btn normal-btn"href="{if $current_page+1 le $page_count && $current_page ge 1}{$http_location}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page+1}{/if}" rel="{if $current_page+1 le $page_count && $current_page ge 1}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page+1}{/if}" onclick="return Myntra.Search.Utils.triggerManualInfiniteScroll();">Show more products <span class="products-left-content">(<span class="products-left-count">{if $total_items > 24}{$total_items - $noofitems}{/if}</span>)</span></a>
								<div class="more-products-loading-indicator" style="display:none;">
							    	<img src="{$cdn_base}/skin2/images/loader_transparent.gif">
								</div>
							</div>
						{/if}
					</div>
				</div>
		{else}
				<div class="mk-search-contents mk-f-left mk-no-results">
					<h1><span class="search-icon"></span> {if $h1 && $h1|trim neq ""}{$h1}{elseif $synonymQuery && $synonymQuery|trim neq ""}{$synonymQuery}{else}{$query|truncate:40|replace:"-":" "|capitalize}{/if}</h1>
					<div class="mk-no-results-sub-section-first">
						<div class="mk-sub-text">OOPS - WE COULDN'T FIND ANY MATCHES!</div>
					</div>
					<div class="mk-no-results-sub-section-second">
						<div class="mk-sub-text">HOW ABOUT SEARCHING AGAIN?</div>
						<div class="mk-sub-text mk-help-text">PLEASE CHECK THE SPELLING, OR TRY LESS SPECIFIC SEARCH TERMS</div>
					</div>
					<form class="mk-search-form mk-search-results-page-form mk-cf" action="" method="" onsubmit="return menuSearchFormSubmit(this);">
						<input type="text" class="mk-site-search mk-placeholder" value="" id="search_query">
						<span class="mk-placeholder-text">SEARCH AGAIN</span>
						<button type="submit" name="" class="mk-site-search-btn"></button>
					</form>
					{if $topNavigationWidget}
						<div class="mk-no-results-sub-section-third">
							<div class="mk-sub-text">OR BROWSE OUR EXTENSIVE CATALOG BY:</div>
							<ul>
								{foreach key=indexA item=itemsA from=$topNavigationWidget name=topNavigationWidgetLoop}
									<li><a class="mk-level1" href="{$itemsA.link_url}?nav_id={$itemsA.id}">{$itemsA.link_name}</a></li>
								{/foreach}
				    		</ul>
				    	</div>
			    	{/if}
				</div>
		{/if}
		</div>
    <div class="mk-recent-viewed mk-hide mk-carousel-four mk-carousel-five rs-carousel mk-inline-size-selector"></div>
</section>

{literal}
<script>
	var nav_id='{/literal}{$nav_id}{literal}';
	var total_pages={/literal}{$page_count}{literal};
	var total_pages_temp=total_pages;
	var show_additional_product_details = "{/literal}{$show_additional_product_details}{literal}";
	var query="{/literal}{$query|lower}{literal}";
        var parameter_query='{/literal}{$parameter_query}{literal}';
        var boosted_query='{/literal}{$boosted_query}{literal}';
	var pageTitleNoPage="{/literal}{$pageTitleNoPage}{literal}";
	var metaDescNoPage="{/literal}{$metaDescriptionNoPage}{literal}";
	var sortByDefault="";
	var pageDefault={/literal}{$current_page}-1{literal};
    var current_page={/literal}{$current_page}-1{literal};
	if(pageDefault > total_pages){
		pageDefault=0;
	}
	pageName="search";
	avail_cookie_ids={/literal}'{$avail_cookie_ids}';
	AVAIL_USER_ID='{$AVAIL_USER_ID}'{literal};
	var configArray=new Object();
	configArray["url"]=query;
    configArray["parameter_query"]=parameter_query;
    configArray["boosted_query"]=boosted_query;
	 configArray["query_level"] = "{/literal}{$query_level}{literal}";
    configArray["page"]={/literal}{$current_page}-1{literal};
	configArray["noofitems"]={/literal}{$noofitems}{literal};
	configArray["gender"]=[];
	configArray["sizes"]=[];
	configArray["brands"]=[];
	configArray["articletypes"]=[];
	configArray["pricerange"]={"rangeMin" : "{/literal}{$rangeMin}{literal}", "rangeMax" : "{/literal}{$rangeMax}{literal}"};
	var scrollConfigArray=new Object();
	scrollConfigArray["scrollPos"]=0;
	scrollConfigArray["scrollPage"]=0;
	scrollConfigArray["scrollProcessed"]=false;
  	var filterProperties=new Object();
  	var scrollProperties=new Object();
  	var searchpagescrolltype="{/literal}{$searchpagescrolltype}{literal}";
  	var priceRangeMin=parseInt("{/literal}{$rangeMin}{literal}");
	var priceRangeMax=parseInt("{/literal}{$rangeMax}{literal}");
	var batchProcess=false;
	var onBeforeUnloadFired = false;
	var pageSetToFirst=false;
	var filterClicked=false;
	var filterClickedName="none";
	var userTriggered=false;
	var onPageLoad=true;
	sortByDefault="{/literal}{if $sort_param_in_url}{$sort_param_in_url}{/if}{literal}";
	var shownewui="{/literal}{$shownewui}{literal}";
	var default_noofitems={/literal}{$noofitems}{literal};
	configArray["sortby"]=sortByDefault;
	configArray["uq"]={/literal}'{if $userQuery == true}{$userQuery}{else}false{/if}'{literal};
	var trackingBooleans=new Object();
	trackingBooleans["gender"]=false;
	trackingBooleans["sizes"]=false;
	trackingBooleans["brands"]=false;
	trackingBooleans["pricerange"]=false;
	trackingBooleans["noofitems"]=false;
	var dynamicFilters=[];
	var priceRangeMin=parseInt("{/literal}{$rangeMin}{literal}");
	var priceRangeMax=parseInt("{/literal}{$rangeMax}{literal}");
	var articleTypeAttributesList=[];
	var innerhtml="";
	var noOfAutoInfiniteScrolls="{/literal}{$noOfAutoInfiniteScrolls}{literal}";
	var ajaxCallFunc="";
	var heightFromLast=1170;
	var noResultsPage=true;
</script>
{/literal}

{if $total_items ge 1}
<script>
	noResultsPage=false;
</script>
{if $article_type_attributes}
	<script>
	{foreach key=attribute item=props from=$article_type_attributes}
		configArray["{$props.title|replace:' ':'_'}_article_attr"]=[];
		trackingBooleans["{$props.title|replace:' ':'_'}_article_attr"]=false;
		articleTypeAttributesList.push("{$props.title|replace:' ':'_'}_article_attr");
	{/foreach}
	</script>
{/if}

{if $article_type_attributes}
	<script>
	{foreach key=attribute item=props from=$article_type_attributes}
		{literal}
		filterProperties["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"]={
			getURLParam: function(){
				var ataStr=configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"].join(":");
				return (ataStr != "")?"{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"+"="+ataStr:"";
			},
			processURL: function(str){
				configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"]=Myntra.Search.getFilterValues(str);
				if (Myntra.Search.Data.optimEnabled) {
					Myntra.Search.Data.initJSON.search.fq.push(("{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}" + ':("' + configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"].join('" OR "') + '")').replace('~-', '-').replace('~/', '\/'));
				}
				return true;
			},
			updateUI: function(){
				//unselect all brands
				$(".article_type_attr .mk-labelx_check").removeClass("checked").addClass("unchecked");

				//update UI selection for brands in configArray
				for(var attr=0;attr<configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"].length;attr++){
					var selector=configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"][attr].toLowerCase().replace(/\W+/g, "") + "-ata-filter";
					$("."+selector).removeClass("unchecked").addClass("checked");
				}
				return true;
			},
			setToDefault: function(){
				configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"]=[];
				return true;
			},
			resetParams: function(){
				configArray["page"]=0;
				return true;
		    },
            updateFilterVisibility: function(filters){
                if((filterClickedName != "" && filterClickedName!="{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}") || (filterClickedName != "" && filterClickedName=="{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}" && configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"].length == 0)){
                    $(".mk-{/literal}{$props.title|replace:' ':'_'}{literal}_article_attr-filters .mk-labelx_check").each(function (index,element){
                        if(typeof filters.article_type_attributes["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"] == 'undefined' || typeof filters.article_type_attributes["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"].values[$(element).attr('data-value')] == 'undefined'){
                            $(element).addClass('disabled');
                        }else{
                            $(element).removeClass("disabled");
                        }
                    });
                }
            },
            setFilterClickedName: function(){
                filterClickedName = "{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}";
			},
			updateClearFilterUI: function(){
				var clearFilterEl=$(".mk-{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"+"-filters").closest("li").find(".mk-clear-filter");
				var clearAllFilterEl=$(".mk-clear-all-filters");
				if(configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"].length >= 1){
					clearFilterEl.removeClass("mk-hide");
				}
				else{
					clearFilterEl.addClass("mk-hide");
				}
				if(Myntra.Search.Utils.getNoOfFiltersSet()){
					clearAllFilterEl.removeClass("mk-hide");
				}
				else{
					clearAllFilterEl.addClass("mk-hide");
				}
			}};
		{/literal}
	{/foreach}
	</script>
{/if}
{/if}

{/block}
{block name=searchdescription}
	{if isset($seoStrategy)}
		{$seoStrategy->getBodyWidget()}
	{elseif $description && $page<2 && $total_items ge 1 && $description neq " "}
		<div id="mk-search-description">
			<h2>About {if $h1 && $h1|trim neq ""}{$h1}{elseif $synonymQuery && $synonymQuery|trim neq ""}{$synonymQuery}{else}{$query|truncate:40|replace:"-":" "|capitalize}{/if}</h2>
			<p>{$description}</p>
		</div>
	{/if}
{/block}

{block name=pagejs}
	{if $optimEnabled == 'true'}
		<script>
			Myntra.Search.Data = Myntra.Search.Data || {};
			Myntra.Search.Data.initJSON = {$jsonSearchData};
			Myntra.Data.jpegminiconf = {$jpegminiconf};
		</script>
		{fetch file="skin2/search/templates/products.html"}
		{fetch file="skin2/search/templates/filters.html"}
	{/if}
{/block}


{block name=macros}
    {include file="macros/search-macros.tpl"}
{/block}
