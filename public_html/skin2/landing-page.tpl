{extends file="layout.tpl"}

{block name=body}
<script>
var pageName = '{$landingPage}';
Myntra = Myntra || {};
Myntra.Data = Myntra.Data || {};
Myntra.Data.bannerCycleInterval = {$bannerCycleInterval};
</script>
<div class="mk-one-column mk-landing-page">
	<h1>{$lpdetails.title}</h1>
	
	{if $lpdetails.subtitle}<h3>
		{*if $landingPage eq 'men'}
			The largest collection of Men's clothing, footwear and accessories
		{elseif $landingPage eq 'women'}
			The largest collection of Women's clothing, footwear and accessories
		{elseif $landingPage eq 'kids'}
			The largest collection of Kids' clothing, footwear and accessories
	    {elseif $landingPage eq 'stylezone' }
			INSPIRATION&nbsp;&nbsp;&#183;&nbsp;&nbsp;TRENDS&nbsp;&nbsp;&#183;&nbsp;&nbsp;CREATE & SHARE LOOKS&nbsp;&nbsp;&#183;&nbsp;&nbsp;STYLE TIPS&nbsp;&nbsp;&#183;&nbsp;&nbsp;CELEBRITIES 
		{/if*}
		{$lpdetails.subtitle}
	</h3>{/if}	
	<section class="mk-site-main">
        {include file="inc/home-slideshow.tpl"}
        {include file="inc/home-content.tpl"}
        {include file="inc/lp-sections.tpl"}
        {*if $landingPage eq "men" || $landingPage eq "women" || $landingPage eq "kids"}
			{foreach key=indexA item=itemsA from=$topNavigationWidget name=topNavigationWidgetLoop}
				{if ($landingPage eq "men" && $itemsA.link_name eq "Men") || ($landingPage eq "women" && $itemsA.link_name eq "Women") || ($landingPage eq "kids" && $itemsA.link_name eq "Kids")}
                    {foreach key=indexB item=itemsB from=$itemsA.child name=clist}
						<div class="category-list {if $smarty.foreach.clist.last}no-border{/if}">
							<div>
								<a href="{$itemsB.link_url}?nav_id={$itemsB.id}">
									{if $itemsA.link_name neq "Kids"}
										{$itemsA.link_name}'s {$itemsB.link_name}
									{/if}
								</a>
							</div>
							<ul class="mk-cf">
								{foreach key=indexC item=itemsC from=$itemsB.child name=category}
									<li class="mk-f-left {if $smarty.foreach.category.index % 2 == 1}no-margin{/if}"><a href="{$itemsC.link_url}?nav_id={$itemsC.id}">{$itemsC.link_name}</a></li>
								{/foreach}
							</ul>
						</div>
					{/foreach}
				{/if}
			{/foreach}
        {/if*}
	</section>
</div>
{/block}

