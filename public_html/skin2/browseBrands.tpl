{extends file="layout.tpl"}

{block name=body}
<script>
var pageName = '{$landingPage}';
</script>
<div class="mk-one-column mk-landing-page mk-brands-page">
	<h1>Brands</h1>
	<h3>
		Brands Listed in Alphabetical Order
		{foreach key=brandidx item=brandentry from=$global_brands}
			&nbsp;/&nbsp;<a href="#{$brandentry.range|lower|replace:' ':''}">{$brandentry.range}</a>
		{/foreach}		
	</h3>
	<section class="mk-site-main">
		{foreach key=brandidx item=brandentry from=$global_brands}
			<h2 id="{$brandentry.range|lower|replace:' ':''}">{$brandentry.range}</h2>
			<ul class="brands-list mk-cf">
				{foreach key=brand item=count from=$brandentry.result.brands_filter_facet}
					{if $brand_logos[$brand]}
					<li class="mk-f-left">
						<a href="{$http_location}/{myntralink brands_filter_facet="$brand" is_landingpage="y" generatelink="y"}" class="no-decoration-link">
							<img src="{$brand_logos[$brand]}" width="120" height="70" alt="{$brand}">
							<span>{$brand}</span>
						</a>	
					</li>
					{/if}
				{/foreach}
			</ul>
		{/foreach}
	</section>
</div>
{/block}
