
{extends file="layout.tpl"}

{block name=body}
{if $showPage}
	<script>
		Myntra.Data.pageName="myreturns";
        Myntra.Data.token = "{$userToken}";
        Myntra.Data.refundEnabled = {if $refund_enabled}true{else}false{/if};
	</script>
	<div class="mymyntra mk-account-pages mk-returns-page">
		
    <h1 id="top">My Myntra <span class="sep">/</span> Returns</h1>
    {include file="my/tabs.tpl"}
        {if $customerUser}
		<div class="return-an-item">
			<h3>Want to Return an Item?</h3>
			<p>Returning Items is Easy - Just use our Returns Wizard!</p>
            {if !$telesales}
			{if $exchanges_enabled eq "true"}
				<button class="return btn small-btn normal-btn" {if !$hasReturnableItems}disabled="disabled"{/if}> Return/Exchange an Item</button>
				{else}
				<button class="return btn small-btn normal-btn" {if !$hasReturnableItems}disabled="disabled"{/if}>
			Return an Item</button>
		       {/if}
            {/if}

			{if !$hasReturnableItems}
				<p>You have no Returnable Items at the moment.<p>
			{/if}
			<p>For more Information, read our <a href="{$http_loc}/faqs#returns" target="_blank">Returns Policy & FAQs</a>.</p>
		</div>
	{/if}
	{if $returns.pending}
		<h2>Pending</h2>
	    {include file="my/returns-table.tpl" returnsArray=$returns.pending tableName="pending"}
    {/if}
	
    {if $returns.completed}
	    <h2>Completed</h2>
	    {include file="my/returns-table.tpl" returnsArray=$returns.completed tableName="completed"}
    {/if}
    
    {if !$returns.pending && !$returns.completed}
    	<div class="no-data-msg">You have not placed any return requests yet.</div>
    {/if}
	</div>
{else}

    <div class="mymyntra mk-account-pages my-orders-page">
    <h1 id="top">My Myntra <span class="sep">/</span> Orders</h1>
        {include file="my/tabs.tpl"}
        <br> Login to see this page.
    </div>
{/if}

{/block}
	
{block name=lightboxes}
{if $showPage}
	{include file="my/fork-exchange.tpl"}
	
	{foreach item=return from=$returns.pending}
		{include file="my/returns-lightbox.tpl" tableName="pending"}
	{/foreach}
	
	{foreach item=return from=$returns.completed}
		{include file="my/returns-lightbox.tpl" tableName="completed"}
	{/foreach}
	
	{if $hasReturnableItems}
		<div id="return-item" class="lightbox orderbox">
			<div class="mod">
				<div class="hd">
				{if $exchanges_enabled eq "true"}
			        <h2 class="title">Select an Item to Return / Exchange</h2>
				{else}
			        <h2 class="title">Select an Item to Return </h2>
				{/if}
			        <p class="gray">Only items that are eligible for returns {if $exchanges_enabled eq "true"} / exchanges {/if} as per Myntra's <a href="/faqs#returns" target="_blank">Returns {if $exchanges_enabled eq "true"} & exchanges {/if} Policy</a> are shown here.</p>
				</div>
				<div class="bd">
					{foreach key=grouporderid item=ordermap from=$orders}
						{assign var=baseorder value=$ordermap.orders[0]}
						{assign var=subordersCount value=$ordermap.orders|@count}
						{assign var=gtotal value=$orderTotals[$grouporderid]}
						{if $ordermap.returnAllowed}
					        <div class="subhd">
					    	   	<h4>Order No. {$grouporderid}</h4>
						       	<p>{$ordermap.count} item(s) / Order Placed on {$ordermap.orders[0].date|date_format:"%d %b, %Y"}</p>
					        </div>
					        {foreach item=order from=$ordermap.orders}
								<div class="suborder">
					            <ul class="items">
						                {foreach item=item from=$order.items}
						                	{if $item.return_criteria eq 'allowed' && !$item.returnid && !$item.exchange_orderid}
						                		{include file="my/order-item.tpl" source="myreturns"}
											{/if}
						                {/foreach}
									</ul>
			<div class="shipping-address" style="display:none;">
	                <!-- Shipping Address -->

	                <address data-zipcode="{$baseorder.s_zipcode}" data-address='"name":"{$baseorder.s_firstname} {$baseorder.s_lastname}","address":"{$baseorder.s_address|nl2br}","locality":"{$baseorder.s_locality}","city":"{$baseorder.s_city}","state_name":"{$baseorder.s_state_name}","state":"{$baseorder.s_state}","zipcode":"{$baseorder.s_zipcode}","mobile":"{$baseorder.mobile}","country":"{$baseorder.s_country}","email":"{$baseorder.login}"' >
	                    <div class="name">{$baseorder.s_firstname} {$baseorder.s_lastname}</div>
	                    <div>{$baseorder.s_address|nl2br}</div>
	                    <div>{$baseorder.s_locality}</div>
	                    <div>{$baseorder.s_city}</div>
	                    <div>{$baseorder.s_state_name} - {$baseorder.s_zipcode}</div>
	                    <div><label>Mobile:</label> {$baseorder.mobile}</div>
	                </address>
	                <!-- Shipping Address -->
	       		</div>
	
	
							</div>
				        {/foreach}
                        {/if}
					{/foreach}
                </div>
            </div>
        </div>
	{/if}
{/if}
{/block}

{block name=pagejs}
{if !$showLogin}
<script type="text/json" id="order-items-json">
    {$orderItemsJson}
</script>
{/if}
{/block}
