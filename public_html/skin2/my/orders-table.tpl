<table border="0" cellspacing="0" style="width:90%">
	<col width="125">
	<col width="150">
	<col width="120">
	<col width="160">
	<col width="100">
	<tr class="mk-table-head">
		<th>Order No.</th>
		<th>Date Placed </th>
		<th>Total</th>
		<th>{if $tableName eq 'pending'}Delivered / {/if}Items</th>
		<th>Actions</th>
	</tr>
	{foreach key=grouporderid item=ordermap from=$orders}
		{if ($tableName eq 'pending' && $ordermap.status eq 'PENDING') || ($tableName eq 'completed' && $ordermap.status eq 'COMPLETED')}
			<tr class="
				{if $tableName eq 'pending'}
					{cycle name='p_order_cycle' values='mk-odd,mk-even'}
				{elseif $tableName eq 'completed'}
					{cycle name='c_order_cycle' values='mk-odd,mk-even'}
				{/if}
			order-row">
				<td>
					<div class="mk-order-number">{$grouporderid}</div>
					<div class="mk-order-tip myntra-tooltip mk-hide">
						<h4>Items in this order :</h4>
						<ul>
							{foreach from=$ordermap.titles item=title name=title}
							<li>{$smarty.foreach.title.index + 1}. {$title}</li>
							{/foreach}
						</ul>
						{assign var=shipcount value=0}
						{foreach name=order item=order from=$ordermap.orders}
							{if $order.customerstatusmessage}
								{assign var=shipcount value=$shipcount+1}
							{/if}
						{/foreach}
						{if $shipcount gt 1}
							<h4 class="date">Multiple Shipments</h4>
						{elseif $ordermap.orders.0.delivereddate}
							<h4 class="date">Order Delivered On {$ordermap.orders.0.delivereddate|date_format:"%d %b, %Y"}</h4>
						{elseif $ordermap.orders.0.shippeddate}
							<h4 class="date">Order Shipped On {$ordermap.orders.0.shippeddate|date_format:"%d %b, %Y"}</h4>
						{elseif $ordermap.orders.0.queueddate}
							<h4 class="date">Order Processed On {$ordermap.orders.0.queueddate|date_format:"%d %b, %Y"}</h4>
						{/if}
						<div class="discount-tool-tip-{$ordermap.orderid}"></div>
					</div>
				</td>
				<td>{$ordermap.orders[0].date|date_format:"%d %b, %Y"}</td>
				<td><span class="rupees">{$rupeesymbol} {$ordermap.total}</span></td>
				<td>{if $tableName eq 'pending'}{$ordermap.delivered} / {/if}{$ordermap.itemsCount}</td>
				<td class="order-action" id="order-actions-{$grouporderid}"><div>
					{if $tableName eq 'pending'}
						<span class="track-order" data-orderid="{$grouporderid}" onclick="javascript:_gaq.push(['_trackEvent', 'my_orders', 'orders_table_track_order', '{$order.courier_service}']);">Track Order</span>
						{if $ordermap.returnAllowed || (!$nodeCancelEnabled && $ordermap.cancellable eq "true")}
							<span class="l-gray">/</span>
						{/if}
                                                {if $customerUser && !$nodeCancelEnabled}
                                                    {if $ordermap.cancellable eq "true"}
                                                            <span class="mk-cancel-order" data-orderid="{$grouporderid}" id="cancel-order-link-{$grouporderid}">Cancel</span>
                                                    {/if}
                                                {/if}
					{/if}
					
					{if $ordermap.returnAllowed}
                                            {if !$telesales}
						{if $exchanges_enabled eq "true"} 
							<span class="return">Return <span class="l-gray" style="text-decoration: none;padding: 0px 2px 0px 2px;display: inline-block;" > / </span> Exchange</span>
						{else}
							<span class="return">Return</span>
						{/if}
                                            {/if}
					{/if}
					{if $loyaltyEnabled && ($tableName eq 'completed' || $tableName eq 'pending')  && !$ordermap.allItemsReturned && $ordermap.canClaimLoyaltyPoints}
						{if !$telesales}
							{if $ordermap.returnAllowed || $tableName eq 'pending'}<span class="l-gray"> / </span> {/if}
								<span class="mk-get-points" data-orderid="{$grouporderid}" id="get-points-link-{$grouporderid}">Get Points</span>
							
						{/if}
					{/if}
				</div></td>
			</tr>
		{/if}
	{/foreach}
</table>

