<li
	{if $source eq 'myorders'}
		class="{if ($item.return_criteria neq 'allowed' && $item.return_criteria neq 'jewellery') || $item.returnid || $item.exchange_orderid}return-hide{/if}"
	{/if}
>
	<div class="photo">
		{if $item.product_style|trim}<a href="{$http_location}/{$item.product_style}" onclick="_gaq.push(['_trackEvent','pdp_click', Myntra.Data.pageName, 'cart_image', parseInt({$item.product_style})]);" target="_blank">{/if}
			<img src="{myntraimage key="style_96_128" src=$item.default_image}" />
		{if $item.product_style|trim}</a>{/if}
	</div>
	<div class="details">
		<div><label>Product Code:</label> {$item.product_style}</div>
		<div class="title">
			{if $item.product_style|trim}<a href="{$http_location}/{$item.product_style}" onclick="_gaq.push(['_trackEvent','pdp_click', Myntra.Data.pageName, 'cart_title', parseInt({$item.product_style})]);" target="_blank">{/if}
				{$item.stylename}
			{if $item.product_style|trim}</a>{/if}
		</div>
		<div><label>Size:</label> {if $item.unifiedsize}{$item.unifiedsize}{else}{$item.size}{/if}<label> / Qty:</label> {$item.quantity}</div>
		{assign var="showLoyaltyMsg" value=false}
		{if $source eq 'myorders'}
			{if $item.returnid}
				<div class="return-placed">You have placed a return request for this item</div>
			{elseif $item.exchange_orderid}
				<div class="return-placed">You have placed exchange request for this item</div>
			{elseif !$item.is_returnable && $item.item_loyalty_points_awarded > 0}
				{assign var="showLoyaltyMsg" value=true}
			{elseif $item.return_criteria eq 'allowed'}
				<div class="return-placed hide">You have placed a return/exchange request for this item</div>
                {if !$telesales}
				{if $exchanges_enabled eq "true"}
				<button class="btn small-btn normal-btn btn-fork return-hide" data-return-key="{$item.orderid}-{$item.itemid}">Return/Exchange This</button>{else}
				<button class="btn small-btn normal-btn btn-return-only return-hide" data-return-key="{$item.orderid}-{$item.itemid}"> Return This </button>
                {/if}
				{/if}
                {if !$telesales}
				{if $exchanges_enabled eq "true"}
				<button class="btn small-btn normal-btn btn-fork return-show" data-return-key="{$item.orderid}-{$item.itemid}">Return/Exchange This</button>{else}
				<button class="btn small-btn normal-btn btn-return-only return-show" data-return-key="{$item.orderid}-{$item.itemid}"> Return This </button>
                {/if}
				{/if}

			{elseif $item.return_criteria eq 'jewellery'}
				Please get in touch with our Customer Care at {$customerSupportCall} to return jewellery item.
			{elseif $item.return_criteria eq 'not_returnable'}
				<div class="no-return-msg">THIS ITEM CANNOT BE RETURNED {if $exchanges_enabled eq "true"} / EXCHANGED {/if}.
					PLEASE REFER TO <a href="/faqs#returns" target="_blank">OUR RETURNS {if $exchanges_enabled eq "true"} & EXCHANGES {/if}POLICY</a></div>
			{elseif $item.return_criteria eq 'time_exceeded'}
				<div class="no-return-msg">THIS ITEM CANNOT BE RETURNED AS YOUR {if $item.return_criteria eq 'jewellery'}15{else}30{/if}-DAY RETURN {if $exchanges_enabled eq "true"} / EXCHANGE {/if} PERIOD HAS EXPIRED</div>
			{/if}
		{elseif $source eq 'myreturns'}
			<div class="return-placed hide">You have placed a return request for this item</div>
            {if !$telesales}
			{if $exchanges_enabled eq "true"}
				<button class="btn small-btn normal-btn btn-fork return-show" data-return-key="{$item.orderid}-{$item.itemid}">Return/Exchange This</button>
			{else}
				<button class="btn small-btn normal-btn btn-return-only return-show" data-return-key="{$item.orderid}-{$item.itemid}"> Return This </button>
			{/if}
            {/if}
		{/if}
		<div data-return-key="{$item.orderid}-{$item.itemid}" class="no-return-loyalty {if !$showLoyaltyMsg}hide{/if}">You cannot return/exchange this product <a class="no-return-why">[WHY]</a></div>
		<div class="no-return-loyalty-desc hide">
			You have chosen to get Privilege Points for this product.<br/> You cannot return/exchange this ... <a class="read-more-faq" href="/faqs" onclick="_gaq.push(['_trackEvent','read_more_faq_loyalty', Myntra.Data.pageName]);" >Read more in FAQs</a>
		</div>		
	</div>
	<div class="amount">
		<div class="final-price">
			{if $item.final_price_paid lt $item.subtotal_price}
				<span class="ico ico-expand"></span>
			{/if}
			{if $item.discount === $item.subtotal_price}
				FREE
			{else}
			<span class="rupees">{$rupeesymbol} </span>{math equation="x + y" x=$item.final_price_paid y=$item.taxamount format="%.2f"}
			{/if}
		</div>
		{if $item.final_price_paid lt $item.subtotal_price}
			<div class="mrp hide">
				MRP : <span class="rupees">{$rupeesymbol}<span>
					<span>{$item.subtotal_price|string_format:"%.2f"}</span>
			</div>
			
			<div class="discounts hide">
				{if $item.discount ne 0.00}
					Item Discount : (-) <span class="rupees">{$rupeesymbol} </span> {$item.discount|string_format:"%.2f"}<br />
				{/if}
				{if $item.cart_discount_split_on_ratio ne 0.00}
					Bag Discount : (-) <span class="rupees">{$rupeesymbol} </span> {$item.cart_discount_split_on_ratio|string_format:"%.2f"}<br />
				{/if}				
				{if $item.coupon_discount_product ne 0.00}
					Coupon : (-) <span class="rupees">{$rupeesymbol} </span> {$item.coupon_discount_product|string_format:"%.2f"}<br />
				{/if}
				{if $item.cash_redeemed ne 0.00}
					Cashback : (-) <span class="rupees">{$rupeesymbol} </span> {$item.cash_redeemed|string_format:"%.2f"}<br />
				{/if}
				{if $item.loyalty_points_used_rupees ne 0.00}
					Points : (-) <span class="rupees">{$rupeesymbol} </span> {$item.loyalty_points_used_rupees|string_format:"%.2f"}<br />
				{/if}
				{if $item.pg_discount ne 0.00}
					Gateway : (-) <span class="rupees">{$rupeesymbol} </span> {$item.pg_discount|string_format:"%.2f"}<br />
				{/if}                           
			</div>
		{/if}
	</div>
</li>
			
		
