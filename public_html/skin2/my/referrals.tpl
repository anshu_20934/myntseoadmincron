{extends file="layout.tpl"}

{block name=body}
{if $showPage}
<div class="mymyntra mk-account-pages mk-referrals-page">
    <h1 id="top">My Myntra <span class="sep">/</span> Referrals</h1>
    {include file="my/tabs.tpl"}
			<div class="mk-mynt-content mk-bordered-page">
                            {if $customerUser}
				<div class="mk-referral-desc">
					<p>Earn coupons worth up to <span class="mk-price-spl">Rs. {$mrpAmount + $refTotalValue}</span> for each friend that you refer to Myntra.com.<br/>
                    For every friend that registers, you get a coupon of <span class="mk-price-spl">Rs. {$mrp_refRegistration_mrpAmount}</span> off your next order of <span class="mk-price-spl">Rs. {$mrp_refRegistration_minCartValue}</span><br/>
                    Once your friend makes a purchase, you get {$mrp_refFirstPurchase_numCoupons} more coupons of <span class="mk-price-spl">Rs. {$mrp_refFirstPurchase_mrpAmount}</span> off your next {$mrp_refFirstPurchase_numCoupons} orders of <span class="mk-price-spl">Rs. {$mrp_refFirstPurchase_minCartValue}</span>.</br> 
                    Your friend will also receive coupons for registering with Myntra.com
					<br/></p>
				</div> <!-- End .referral-desc -->
<!-- Set your options, including the domain_key -->
<script type="text/javascript">
var csPageOptions = {
  domain_key:"BYD4BK35GQ6DZYKRW2WM", 
  textarea_id:"recipient_list",
  include:["email"],
  skipSourceMenu:true
};
</script>
				
				<div class="mk-referral-social">
					<a href="javascript:fb_share_referral('{$login}');" class="fb"></a>
        			{assign var=encodedEmail value=$login|replace:'@':'[AT]'}
            		<a href="javascript:twitter_share_referral('{$encodedEmail}','{$num_coupons}','{$value_coupons}');" class="tw"></a>
				</div> <!-- End.referral-social -->
				<div class="mk-referral-social">
					<p class="mk-referral-social-msg">YOU CAN ALSO POST THIS LINK ON YOUR SOCIAL NETWORK PAGES* AND KEEP COLLECTING REFERRAL CREDITS</p>
					<p class="mk-referral-link green">{$http_location}/register.php?ref={$login}</p>
				</div>
				<div class="mk-referral-myinvites">
					<h2> My Invites </h2>
					<div class="mk-referral-invite-tabs">
					<ul class="mk-mynt-nav">
						<li><a href="#tabs-1">Open Invites <sub>{if $num_open_invites gt 0}{$num_open_invites}{/if}</sub></a></li>
						<li><a href="#tabs-2">Registered <sub>{if $num_accepted_invites gt 0}{$num_accepted_invites}{/if}</sub></a></li>
						<li><a href="#tabs-3">Placed First Order <sub>{if $num_active_invites gt 0}{$num_active_invites}{/if}</sub></a></li>
					</ul> 
					{if $num_open_invites > 0}
					<ul id="tabs-1">
						<li>
						<table>
							<col width="235" />
							<col width="235" />
							<col width="210" />
							<col width="75" />
							<tr class="mk-table-head">
								<th>Referred Email</th>
								<th>Last Invite Sent On</th>
								<th>No. Of Invites Sent</th>
								<th></th>
							</tr>
							{section name=prod_num loop=$open_invites}
							<tr class="{cycle name='open_invite_cycle' values='mk-odd,mk-even'}">
								<td>{$open_invites[prod_num].referred|truncate:65:"..."}</td>
								<td>{$open_invites[prod_num].dateReferred|date_format:$config.Appearance.date_format}</td>
								<td>{$open_invites[prod_num].invitesSent}</td>
							</tr>
							{/section}
						</table>
			            </li>
			        </ul>
			        {else}
			        <ul id="tabs-1">
						<li class="body-text">
			        	You do not have any open invites. You can send out invites and earn referral credits by simply following the
			            steps listed above.
			            </li>
			        </ul>    
			        {/if}
			
					{if $num_accepted_invites > 0}
			        <ul id="tabs-2">
			        	<li>
						<table>
							<col width="260" />
							<col width="260" />
							<col width="230" />
							<tr class="mk-table-head">
								<th>Referred Email</th>
								<th>Registered on Myntra on</th>
			                    <th>Coupon awarded</th>
							</tr>
							{section name=prod_num loop=$accepted_invites}
							<tr class="{cycle name='accepted_invite_cycle' values='mk-odd,mk-even'}">
								<td>{$accepted_invites[prod_num].referred|truncate:65:"..."}</td>
								<td>{$accepted_invites[prod_num].dateJoined|date_format:$config.Appearance.date_format}</td>
								<td>{$accepted_invites[prod_num].regcompleteCoupon|upper}</td>
							</tr>
							{/section}
						</table>
			            </li>
			        </ul>
			        {else}
			        <ul id="tabs-2">
			        	<li class="body-text">
			        	Currently you have no referrals in this category. Urge your friends to accept your invite and sign up on
			            Myntra. For each friend who registers, you get a credit of Rs {$mrpAmount}!
			            </li>
			        </ul>
			        {/if}
			
					{if $num_active_invites > 0 }
			        <ul id="tabs-3">
			        	<li>
						<table>
							<col width="260" />
							<col width="260" />
							<col width="230" />
							<tr class="mk-table-head">
								<th>Referral email</th>
			                    <th>Placed first order</th>
			                    <th>Coupon(s) awarded</th>
							</tr>
							{section name=prod_num loop=$active_invites}
							<tr class="{cycle name='active_invite_cycle' values='mk-odd,mk-even'}">
								<td>{$active_invites[prod_num].referred|truncate:65:"..."}</td>
								<td>{$active_invites[prod_num].dateFirstPurchase|date_format:$config.Appearance.date_format}</td>
								<td>{$active_invites[prod_num].ordqueuedCoupon|upper}</td>
							</tr>
							{/section}
						</table>
			            </li>			        
			        </ul>
			        {else}
			        <ul id="tabs-3">
			        	<li class="body-text">
			            You have no referrals who have made their first purchase yet. Enhance your Mynt Credits balance by
			            recommending your friends to purchase at Myntra - the one stop destination for lifestyle products. Don't
			            forget that on the 1st purchase of each friend you get Rs {$refTotalValue} worth of Mynt credits!
			            </li>
			        </ul>    
			        {/if}
					</div>
					<div class="mk-resend-notifications"></div>
				</div> <!-- End .referral-myinvites -->
                             {else}
                                {$notAllowedMsg} 
                             {/if}
			</div><!-- End .mynt-content -->
</div>
<script type="text/javascript" src="http://www.plaxo.com/css/m/js/util.js"></script>
<script type="text/javascript" src="http://www.plaxo.com/css/m/js/basic.js"></script>
<script type="text/javascript" src="http://www.plaxo.com/css/m/js/abc_launcher.js"></script>
<script type="text/javascript" src="https://api.cloudsponge.com/address_books.js"></script>
{else}

<div class="mymyntra mk-account-pages my-orders-page">
    <h1 id="top">My Myntra <span class="sep">/</span> Orders</h1>
        {include file="my/tabs.tpl"}
        <br> Login to this page.
    </div>
{/if}

{/block}

