
{extends file="layout-checkout.tpl"}

{block name=body}
<div class="mymyntra mk-account-pages my-profile-page">   
  <h1 id="top">My Myntra <span class="sep">/</span> Payment Settings</h1>
  {include file="my/tabs.tpl"}
  {*START EXPRESS BUY BLOK*}
  <div class="express-block" id="express-block">
    {if !$telesales}
    {if $mymyntraExpressBuy} 
    <h2>EXPRESS BUY SETTINGS</h2>
    <p class="info-text">
      Use our Express Buy option to make your next purchase faster and easier.<br >
      If you opt to pay by credit / debit card, your card information will be encrypted and safe with us.
    </p>
    <div class="express-settings">
      <input type="checkbox" name="express-buy-enable" id="express-buy-enable"><span>TURN ON QUICK BUY FOR YOUR ACCOUNT</span>
      <div class="express-default-options">
        <p>SELECT MODE OF PAYMENT FOR QUICK BUY:</p>
        <div class="row">
          <input type="radio" checked="true" name="default-option" value="cod" id="sel_cod"><span>Cash On Delivery</span>
        </div>
        <div class="row">
          <input type="radio" name="default-option" value="creditcard" id="sel_credit_card"><span>Credit/Debit Cards</span>
        </div>        
      </div>
    </div>
    {/if}
    {if true}
    {if $mymyntraStoredCards} 
    <div class="express-cards">
      <h2>SAVED CARDS</h2>
      <p class="info-text">
        Save your payment details for a faster & easier checkout !      
    </p>
      <button class="btn small-btn normal-btn add-card">ADD A NEW CARD</button>
      <ul id="list-cards" class="list-cards">    
      </ul>
    </div>
    {/if}
    {/if}
    <!--<button class="btn small-btn primary-btn" id="exp-settings-save">Save Changes</button>-->
    {else}
    {$notAllowedMsg} 
    {/if}
  </div>
  {*END EXPRESS BUY BLOK*}
</div>    
{/block}

{block name=pagejs}
<script>
Myntra.Data.secureSessionExpireMessage = '{$secureSessionExpireMessage}';
Myntra.Data.paySerHost = '{$paySerHost}';
Myntra.Data.countryList ={if $countryList} {$countryList} {else}''{/if};
Myntra.Data.expressbuyEnabled = {if $mymyntraExpressBuy}true{else}false{/if};
</script>
{/block}

