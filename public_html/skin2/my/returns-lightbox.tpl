{if $customerUser}
<div id="return-{$return.returnid}" class="lightbox orderbox returnbox thisone">
	<div class="mod">
	    <div class="hd">
	        <h2 class="title">Return No: {$return.returnid}</h2>
	        <span class="date">placed on {$return.createddate_display|date_format:"%d %b, %Y"}</span>
	        <span class="count">{if $return.return_mode eq 'self'}Self-Shipped{else}Myntra Pickup{/if}</span>
	        <span class="total">order no. : <strong>{$return.group_id}</strong></span>
	    </div>
	    <div class="bd">
	    	<div class="return-top-border"></div>
	    	<div class="suborder">
	     	<div class="return-status subhd">
				<h4>{$return.mymyntra_status}</h4>
            	<div id = "return_message_{$return.returnid}" class="return_msg">{$return.status_msg}</div>
            	{if $return.status eq 'RRQP' || $return.status eq 'RPI' || $return.status eq 'RRQS' || $return.status eq 'RDU'}
		        	{if $return.return_mode eq 'self'}
		            	<div id= "courier_update_div_{$return.returnid}_{$return.itemid}" style="display:none;" class="courier-update-div{if $return.courier_service|trim} updated{/if}"></div>
						<p id="links-div-{$return.returnid}" style="margin-top:5px;">
			    	    	{if $return.courier_service neq ''}
			        	    	<a href="javascript:void(1);" class="mk-return-shipping-details mk-hide" id ="courier_update_href_{$return.returnid}" data-returnid="{$return.returnid}" data-itemid="{$return.itemid}">Edit Your Return Shipping Details</a>
			            	{else}
			            		<a href="javascript:void(1);" class="mk-return-shipping-details mk-hide" id ="courier_update_href_{$return.returnid}" data-returnid="{$return.returnid}" data-itemid="{$return.itemid}" data-enterdetails="true">Share Your Return Shipping Details</a>
			                {/if}
		        		</p>
					{/if}    
					{if $return.status eq 'RRQP' || $return.status eq 'RPI' || $return.status eq 'RRQS' || $return.status eq 'RPI2' || $return.status eq 'RPI3' || $return.status eq 'RPUQ2' || $return.status eq 'RPUQ3'}
	            		<div class="dl-returns-form">
	            			Please ensure that you include the Returns Form in your Return Shipment.<br />
	            			If you have access to a printer, you can download and print your <a href="generatelabel.php?for=returndetails&id={$return.returnid}" target="_blank">Pre-Filled Returns Form</a> here.<br />
	            			In case you are not able to download the return form then please mention your Order ID and Return ID on a blank sheet and include it in your return shipment.           			
            			</div>
					{/if}
	            {/if}
            </div>
            <ul class="items">
                <li>
                    <div class="photo">
	                	{if $return.style_id|trim}<a href="{$http_location}/{$return.style_id}" onclick="_gaq.push(['_trackEvent','pdp_click', Myntra.Data.pageName, 'cart_image'], parseInt({$item.product_style}));" target="_blank">{/if}
	                		<img src="{myntraimage key="style_96_128" src=$return.designImagePath}" alt="{$singleitem.productStyleName}">
                		{if $return.style_id|trim}</a>{/if}
                    </div>
                    <div class="details">
		                <div><label>product code:</label> {$return.style_id}</div>
		                <div class="title">
		                	{if $return.style_id|trim}<a href="{$http_location}/{$return.style_id}" onclick="_gaq.push(['_trackEvent','pdp_click', Myntra.Data.pageName, 'cart_title'], parseInt({$item.product_style}));" target="_blank">{/if}
		                		{$return.productStyleName}
	                		{if $return.style_id|trim}</a>{/if}
		                </div>
						<div><label>Size:</label> {$return.size}<label> / Qty:</label> {$return.quantity}</div>
            		</div>
            		<div class="amount">
                        <div class="final-price clearfix right">
                            {if $return.final_price_before_cashback lt $return.subtotal}
                                <span class="ico ico-expand"></span>
                            {/if}
                            <span class="rupees">{$rupeesymbol} </span>{math equation="x + y" x=$return.final_price_before_cashback y=$return.taxamount format="%.2f"}
                        </div>
                        {if $return.final_price_paid lt $return.subtotal}
	                        <div class="mrp hide clearfix right">
	                            MRP: {$rupeesymbol}{$return.subtotal|string_format:"%.2f"} <br/>
	                        </div>
	                        <div class="discounts hide clearfix right">
	                            {if $return.item_discount ne 0.00}
	                                Item Discount: (-) <span class="rupees">{$rupeesymbol} </span>{$return.item_discount|string_format:"%.2f"} <br>
	                            {/if}
	                            {if $return.cart_discount ne 0.00}
	                                Bag Discount: (-) <span class="rupees">{$rupeesymbol} </span>{$return.cart_discount|string_format:"%.2f"} <br>
	                            {/if} 
	                            {if $return.item_coupon_discount ne 0.00}
	                                Coupon: (-) <span class="rupees">{$rupeesymbol} </span>{$return.item_coupon_discount|string_format:"%.2f"} <br>
	                            {/if}
	                            {if $return.pg_discount ne 0.00}
	                                Gateway: (-) <span class="rupees">{$rupeesymbol} </span>{$return.pg_discount|string_format:"%.2f"} <br>
	                            {/if}
	                        </div>
                        {/if}
                    </div>
                </li>
            </ul>
            </div>
            <div class="return-top-border"></div>
            {if $tableName eq 'completed' && $return.is_refunded}
	            <div class="suborder">
		            <ul class="items">
		                <li>
		                    <div class="refund-details details">
		                    <table style="width:100%">
		                    	<tr>
		                    	<td class="title-block" style="text-align:right;width:50%" align=right><label>Refund Amount: </label></td><td style="text-align:left;" align=left><span class="rupees">{$rupeesymbol} </span><span>{$return.refundamount}</span></td></tr>
		                    	{if $return.return_mode == 'self' && $return.delivery_credit != 0.00}
				                <tr><td style="text-align:right;width:50%" align=right><label>Includes Self-Shipping Credit of: </label></td><td style="text-align:left;" align=left><span class="rupees">{$rupeesymbol} </span><span>{$return.delivery_credit}</span></td></tr>
				                {/if}
						{if $return.difference_refund != 0.00 }
						        	<tr><td style="text-align:right;width:50%" align=right><label>Price Mismatch Difference Refunded Earlier: </label></td><td style="text-align:left;" align=left><span class="rupees">{$rupeesymbol}</span><span>{$return.difference_refund}</span></td></tr>
						{/if}
				                {if $return.cashback_issued != 0.00 }
						        	<tr><td style="text-align:right;width:50%" align=right><label>Reversal of Earned Cashback: </label></td><td style="text-align:left;" align=left>(-) <span class="rupees">{$rupeesymbol} </span><span>{$return.cashback_issued}</span></td></tr>
						    	{/if}
						    	{if $return.item_coupon_discount != 0.00}
				            	<tr><td colspan="2"><span>A Replacement Coupon has been added to your Mynt Credits</span></td></tr>
								{/if}
				                <tr><td class="title-block" style="text-align:right;width:50%" align=right><label><span class="ico ico-expand"></span>  You Paid: </label></td><td style="text-align:left;" align=left><span class="rupees">{$rupeesymbol} </span><span>{$return.final_price_before_cashback + $return.payment_surcharge + $return.taxamount}</span></td></tr>
				            </table>
				            <table class="refund-details-expand" style="text-align: center; width: 100%;display:none">
						        {if $return.item_cash_redeemed ne 0.00}
	                                <tr><td style="text-align:right;width:50%" align=right><label>Cashback Used: </label></td><td style="text-align:left;" align=left><span class="rupees">{$rupeesymbol} </span><span>{$return.item_cash_redeemed}</span></td></tr>                    
	                            {/if}
						        {if $return.payment_method != ""}
									<tr><td style="text-align:right;width:50%" align=right><label>
										{if $return.payment_method eq "on"}Online Payment: {elseif $return.payment_method eq "cod"}Cash on Delivery :{else}Actual Payment :{/if}</label></td><td style="text-align:left;" align=left><span class="rupees">{$rupeesymbol} </span><span>{$return.final_payment_made + $return.taxamount}</span></td></tr>
								{/if}
								{if $return.payment_surcharge > 0}
									<tr><td style="text-align:right;width:50%" align=right><label>
										EMI Charge: </label></td><td style="text-align:left;" align=left><span class="rupees">{$rupeesymbol} </span><span>{$return.payment_surcharge}</span></td></tr>
								{/if}
							</table>
		            		</div>
		                </li>
		            </ul>
	            </div>
	        {/if}    
	    </div>
	</div>
</div>
{/if}	
          
