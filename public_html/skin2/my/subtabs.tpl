<ul class="main-subtabs">
    <li><a href="#mycoupons" onClick="_gaq.push(['_trackEvent', 'mymyntra', 'coupons','click']);return true;">Coupons <i class="icon-right-arrow"></i></a></li>
    <li><a href="#mycashback" onClick="_gaq.push(['_trackEvent', 'mymyntra', 'cashback','click']);return true;">Cashback <i class="icon-right-arrow"></i></a></li>
    <li><a href="#myprivilege" onClick="_gaq.push(['_trackEvent', 'mymyntra', 'loyaltyPoints','click']);return true;">My Privilege <i class="icon-right-arrow"></i></a>
        <ul class="subtabs">
            <li><a href="#myprivilege-how-it-works" onClick="_gaq.push(['_trackEvent', 'mymyntra', 'loyalty-how-it-works','click']);return true;">How it works</a></li>
            <li><a href="#myprivilege-earn-more-points" onClick="_gaq.push(['_trackEvent', 'mymyntra', 'loyalty-earn-more-points','click']);return true;">Earn more points</a></li>
            <li><a href="#myprivilege-rules" onClick="_gaq.push(['_trackEvent', 'mymyntra', 'loyalty-rules','click']);return true;">Rules</a></li>
        </ul>
    </li>
</ul>
<!-- class="my-mynt-selected-tab" -->