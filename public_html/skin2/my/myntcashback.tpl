     <div class="mk-mynt-content">
		{if $myntCashBalance neq '' || myntCashBalance eq 0}	
			<div class="mk-mynt-promo-codes mk-mynt-credits">
				<h2> CASHBACK </h2>
				<h6 class="clickable"> You have <span class="rupees red">Rs. {if $myntCashBalance neq ''}{$myntCashBalance}{else}0{/if}</span> in your cashback account. </h6>
				<div class="mk-cashback-content">
					<table> 
						<col width="145" />
						<col width="345" />
						<col width="90" />
						<col width="90" />
						<col width="105" />						
						<tr class="mk-table-head">
							<th>Date</th>
	                        <th class="left">Transaction</th>
	                        <th>Credit <span class="rupees">(Rs.)</span></th>
	                        <th>Debit <span class="rupees">(Rs.)</span></th>
	                        <th>Balance <span class="rupees">(Rs.)</span></th>
						</tr>
						{section name=prod_num loop=$myntCashTransactions}
						<tr class="{cycle name='cashback_cycle' values='mk-odd,mk-even'}">
							<td>{$myntCashTransactions[prod_num].modifiedOn|date_format:"%e %b, %Y"}</td>
							<td class="left">{$myntCashTransactions[prod_num].description}</td>
							<td>{if $myntCashTransactions[prod_num].creditInflow gt 0}{$myntCashTransactions[prod_num].creditInflow}{else} - {/if}</td>
							<td>{if $myntCashTransactions[prod_num].creditOutflow gt 0}{$myntCashTransactions[prod_num].creditOutflow}{else} - {/if}</td>
							<td>{$myntCashTransactions[prod_num].balance}</td>
						</tr>
						{/section}
					</table>
				</div>
			</div> <!-- End .mynt-credits -->
		{else}
			<div class="mk-mynt-promo-codes mk-mynt-credits">
				<h2> My Cashback </h2>
				<h6> You do not have any cashback. </h6>
			</div>	
		{/if}
	</div>	