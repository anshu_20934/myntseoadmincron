
<ul class="mk-mynt-nav">
    <li><a href="{$http_location}/mymyntra.php?view=myprofile" {if $view eq 'myprofile'}class="active"{/if} onClick="_gaq.push(['_trackEvent', 'mymyntra', 'myprofile','click']);return true;">Profile</a></li>
    {if $PCIDSSCompliant}
    	<li><a href="{$https_location}/mySavedCards.php" {if $expressCheckout}class="active"{/if} onClick="_gaq.push(['_trackEvent', 'mymyntra', 'mycards','click']);return true;">Payments</a></li>
    {/if}
    <li><a href="{$http_location}/mymyntra.php?view=myorders" {if $view eq 'myorders'}class="active"{/if} onClick="_gaq.push(['_trackEvent', 'mymyntra', 'myorders','click']);return true;">Orders</a></li>
    <li><a href="{$http_location}/mymyntra.php?view=mymyntcredits" {if $view eq 'mymyntcredits'}class="active"{/if} onClick="_gaq.push(['_trackEvent', 'mymyntra', 'mymyntcredits','click']);return true;">Mynt Credits</a></li>
    <li><a href="{$http_location}/mymyntra.php?view=myreferrals" {if $view eq 'myreferrals'}class="active"{/if} onClick="_gaq.push(['_trackEvent', 'mymyntra', 'myreferrals','click']);return true;">Referrals</a></li>
    <li><a href="{$http_location}/mymyntra.php?view=myreturns" {if $view eq 'myreturns'}class="active"{/if} onClick="_gaq.push(['_trackEvent', 'mymyntra', 'myreturns','click']);return true;">Returns</a></li>
</ul>

