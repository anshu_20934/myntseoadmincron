<div id="courierdetailspopup">
	<div id="courierdetails-main">
		<div id="courierdetails-content">
			<div>
				<div id="courierdetails-progress" class="mymyntra-ajax-loader mk-hide">
					<div class="mymyntra-ajax-loader-icon"></div>
				</div>
				<div class="notification err hide courierdetails-errorMsg">We were not able to update courier details. Please try later.</div>
				<div class="notification success hide courierdetails-successMsg">Your return shipping details have been updated.</div>
				<input type="hidden" name="current_return_id" id="current_return_id" value="{$returnid}">
				<input type="hidden" name="current_item_id" id="current_item_id" value="{$itemid}">
				<div id="courierdetails" class="mk-cf">
					<div class="mk-f-left">
						{assign "flag" 0}
						<label>Return Sent Via <span>*</span></label>
						<div class="field courier-service-div">
							<select name="courier_service" id="courier_service" class="myselect-box courier-service">
								<option value="">-- Select Courier --</option>
								{section name=idx loop=$couriers}
									<option value="{$couriers[idx].code}"{if $couriers[idx].code eq $return_details.courier_service} {assign flag 1} selected="selected"{/if}>{$couriers[idx].display_name}</option>
								{/section}
								<option value="other" {if $return_details.tracking_no|trim && $flag eq 0}selected="selected"{/if}>Other Service</option>
							</select>
							<div class="err"></div>
						</div>
						<div id="courier_service_custom_div" class="field courier-service-custom-div {if $return_details.tracking_no|trim && $flag eq 0}{else}mk-hide{/if}">
							<input type="text" name="courier_service_custom" id="courier_service_custom" class="myfield courier-service-custom" {if $return_details.tracking_no|trim && $flag eq 0}value="{$return_details.courier_service}"{/if}>
							<div class="err"></div>
					</div>
					</div>
					<div class="mk-f-right">
						<label>Tracking Number <span>*</span></label>
						<div class="field tracking-no-div">
							<input type="text" name="tracking_no" id="tracking_no" class="myfield tracking-no" value="{$return_details.tracking_no}">
							<div class="err"></div>
						</div>
						
					</div>
				</div>
				<div>
					<input type="button" class="btn primary-btn save-btn small-btn" value="Save" onclick="updateTrackingDetails({$returnid},{$itemid});">
					<input type="button" class="btn normal-btn cancel-btn small-btn" value="Cancel" onclick="closeCourierDetailsWindow({$returnid},{$itemid});">
					<input type="button" class="btn normal-btn edit-btn small-btn mk-hide" value="Edit" onclick="editCourierDetails({$returnid},{$itemid});">
				</div>
				</div>
			
		</div>
	</div>
</div>