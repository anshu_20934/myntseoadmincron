{extends file="layout.tpl"}

{block name=body}
{if $showPage}
<div class="mymyntra mk-account-pages mk-myntcredits-page">
            <h1 id="top"> My Myntra <span class="sep">/</span> Mynt Credits </h1>
            {include file="my/tabs.tpl"}
            <div class="mk-account-pages">
                <div class="mk-mynt-section clearfix">
                    <div class="mk-mynt-tabs">
                        {include file="my/subtabs.tpl"}
                    </div>
                    <div class="mk-mynt-tabs-content-wrapper">
                        <div class="mk-mynt-tabs-content">
                            
                        </div>    
                    </div>
                </div>
            </div>
</div>
{else}

<div class="mymyntra mk-account-pages my-orders-page">
    <h1 id="top">My Myntra <span class="sep">/</span> Orders</h1>
        {include file="my/tabs.tpl"}
        <br> Login to see your mynt credits.
    </div>
{/if}
{/block}

