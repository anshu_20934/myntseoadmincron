{* Recursive function for creating tnc hierarchy *}
{function name=termMarkup level=0}
    <li class="li-level-{$level}">{$data.term}
        {if is_array($data.subterms)}
            {foreach $data.subterms as $st}
                <div class="subterms">{$st.subterm}</div>
            {/foreach}
        {/if}
        {if is_array($data.terms)}
            <ul class="ul-level-{$level+1}">
                {foreach $data.terms as $trm}
                    {termMarkup data=$trm level=$level+1}
                {/foreach}
            </ul>
        {/if}
    </li>
{/function}

<ul class="top-ul">
    {foreach item=tnc from=$loyaltyTermsCondition key=num}
            <li class="tnc-term-title">{$tnc.label}
            <ul>
                {foreach item=term from=$tnc.terms}
                    {termMarkup data=$term}
                {/foreach}        
            </ul>
            </li>
    {/foreach}
</ul>
