{extends file="layout.tpl"}

{block name=body}
{if $showPage}
<div class="mymyntra mk-account-pages">
    <h1 id="top">My Myntra <span class="sep">/</span> Gift Cards</h1>
    {include file="my/tabs.tpl"}

    <div class="mk-mynt-content mk-mygiftcard-page">
        {if $customerUser}
    	<div class="mk-giftcard-activation-section">



    <div class="mod">
        <div class="hd">
            <h2>USE GIFT CARD/VOUCHER</h2>
            The amount will be added to your cashback account
        </div>


        <br>
        <div class="bd clearfix">
            <div style="text-align: left;width: 400px;margin: auto;">
                <div class="mk-gc-instruction" style="text-transform: none;">
                    <div class="mk-gc-inputs" style="padding: 10px;">
                        <div class="input-row" style="width: 200px;margin: auto;text-align: left">
                            <label class="lbl">GIFT CARD/VOUCHER*</label>
                            <input type="text" name="card" class="mk-gc-card" placeholder="Gift Code/Voucher Number">
                            <div class="mk-gc-activation-err hide" id="card-err"></div>
                            <br><br>
                            <label class=" lbl">PIN</label>
                            <div class="mk-gc-activation-err hide" id="pin-err"></div>
                            <input type="text" name="pin" class="mk-gc-pin" placeholder="PIN"> &nbsp;
                            <label style="font-size: 11px;text-transform: none;font-family:'Arial'">(Enter only if provided)</label>

                            <br><br><br>
                            <div class="lbl">VERIFY*</div>
                            <div style="position:relative">
                                <img src="{$https_location}/captcha/captcha.php?id=giftCaptcha"  id="captcha" 
                                onload="
                                    document.getElementById('giftcard-cap-loading').style.display = 'none';
                                "
                                />
                                <span id="giftcard-cap-loading"></span>
                            
                                <input type="text" name="captcha" class="mk-gc-captcha" placeholder="Type in the text above">
                                <div class="mk-gc-activation-err hide" id="captcha-err"></div>
                                <label class="giftcard-captcha-change-text">
                                        <a  href="javascript:void(0)"
                                            onclick="
                                                document.getElementById('giftcard-cap-loading').style.display = 'block';
                                                document.getElementById('captcha').src=
                                                '{$https_location}/captcha/captcha.php?id=giftCaptcha&rand='+Math.random();
                                            "
                                            id="change-image" >Change text</a></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>

        <div class="mk-gc-activation-success hide"></div>
        <div class="mk-gc-activation-error hide"></div>
        <div class="ft">
            <p> 
                <button class="activate-gift-card btn primary-btn" style="height: 30px;">Use</button>
                &nbsp; &nbsp;
            </p>
        </div>
    </div>
<br>

    		<p class="foot-notes">For more information, read our <a href="{$http_location}/faqs">gift card policies  & faqs</a></p>
    	</div>
    	
    	{if $gc_received|count >0}
    	<div class="mk-giftcard-received">
    		<div class="mk-mynt-promo-codes mk-gc-received">
					<h6>My Gift Cards<span> / {$gc_received|count}</span></h6>
					<p class="mk-sub-desc">If you have received a Gift Card and added it to your account, the amount is credited to your Cashback automatically.</p>
					<div class="mk-coupons-content">
					<table>
						<col width="150" />
						<col width="450" />
						<col width="250" />
						{foreach name=gc_receivedloop item=gc from=$gc_received}
						{if $gc->status eq 'Activated'}
						{assign var=gc_message value=$gc->giftCardMessage}
						{assign var=gc_ocacsion value=$gc_message->giftCardOccasion}
						<tr class="{cycle name='active_cycle' values='mk-odd,mk-even'}">
							<td>
								<img src="{if $smarty.server.HTTPS}{$secure_cdn_base}{else}{$cdn_base}{/if}/{$gc_ocacsion->previewImageThumbnail}"/>
								<a class="mk-gc-preview-link preview-link" data-id="{$gc->id}">view</a>
							</td>
							<td align="center">
								<span class="cat-label">Sender: </span><span>{$gc->senderEmail}</span><br/>
								<span class="cat-label">Value: </span> Rs. <span>{$gc->amount}</span><br/>
								{if $gc->type eq 'Email'}
								<span class="cat-label">Delivery Mode: </span><span>Send Via Email</span><br/>
								{/if}
								<span class="cat-label">Received on: </span><span>{$gc->deliveredDate|date_format:"%d %B, %Y"}</span><br/>
								<span class="cat-label">Sent to: </span><span>{$gc->recipientEmail}</span><br/>
								{*<span class="cat-label">Pin: </span><span>{$gc->pin}</span>*}
							</td>
							<td class="mk-activation-section">
								{*{if $gc->status neq 'Activated'}
									<a class="mk-gc-activate-now" data-id="{$gc->id}">Activate Now</a>
								{else}*}
									<span class="mk-gc-activation-info"><span class='mk-span-success-ico'></span>Activated on {$gc->activatedDate|date_format:"%d %B, %Y"}</span>
								{*{/if}*}
							</td>
						</tr>
						{/if}
    					{/foreach}
					</table>
					</div>
			</div> <!-- End .mynt-promo-codes -->
    	</div>
    	{foreach name=gc_receivedloop item=gc from=$gc_received}
			{assign var=gc_message value=$gc->giftCardMessage}
			{assign var=gc_occasion value=$gc_message->giftCardOccasion}
			{assign var=previewtype value="senderpreview"}
				{include file="my/giftcards-preview-template.tpl"}
		{/foreach}
    	{else}
	    	{if $gc_received|count>0 && $gco_bought|count>0}
	    	{else}
	    	<div class="mk-giftcard-received">
	    		<p class="mk-giftcard-default-msg">You have not added any giftcards yet.</p>
	    	</div>
	    	{/if}
    	{/if}
    	
    	{if $gco_bought|count > 0}
    	<div class="mk-giftcard-sent">
	    	<div class="mk-mynt-promo-codes mk-gc-bought">
					<h6>Purchased Gift Cards<span> / {$gco_bought|count}</span></h6>
					<p class="mk-sub-desc">If you have chosen a "send via email" Gift Card and your recipient hasn't activated it yet, you can opt to resend the email.</p>
					<div class="mk-coupons-content">
					<table>
						<col width="150" />
						<col width="450" />
						<col width="250" />
						{foreach name=gco_boughtloop item=gco from=$gco_bought}
						{assign var=gc value=$gco->giftCard}
						{assign var=gc_message value=$gc->giftCardMessage}
						{assign var=gc_ocacsion value=$gc_message->giftCardOccasion}
						{if $gco->status eq 'Completed'}	
						<tr class="{cycle name='active_cycle' values='mk-odd,mk-even'}">
							<td>
								<img src="{if $smarty.server.HTTPS}{$secure_cdn_base}{else}{$cdn_base}{/if}/{$gc_ocacsion->previewImageThumbnail}"/>
								<a class="mk-gc-preview-link preview-link" data-id="{$gc->id}">view</a>
							</td>
							<td align="center">
								<span class="cat-label">Recipient: </span><span>{$gc->recipientEmail}</span><br/>
								<span class="cat-label">Value: </span> Rs. <span>{$gc->amount}</span><br/>
								{if $gc->type eq 'Email'}
								<span class="cat-label">Delivery Mode: </span><span>Send Via Email</span><br/>
								{/if}
								<span class="cat-label">Purchased on: </span><span>{$gc->deliveredDate|date_format:"%d %B, %Y %H:%M"}</span><br/>
								{if $gc->deliveredDate neq $gc->emailLastSent}
								<span class="cat-label">Last Email on: </span><span>{$gc->emailLastSent|date_format:"%d %B, %Y %H:%M"}</span><br/>
								{/if}
								<span class="cat-label">Order id: </span><span>{$gco->orderId}</span><br/>
								{*<span class="cat-label">Pin: </span><span>{$gc->pin}</span>*}
							</td>
							<td class="mk-activation-section {$gc->status} test">
								{if $gc->status neq 'Activated'}
									Not Activated Yet<br/>
									<a class="mk-gc-resend-email" data-id="{$gc->id}">Resend Gift Email</a>
								{else}
									<span class="mk-gc-activation-info"><span class='mk-span-success-ico'></span>Activated on {$gc->activatedDate|date_format:"%d %B, %Y"}</span>
								{/if}
							</td>
						</tr>
						{/if}
    					{/foreach}
					</table>
					</div>
			</div> <!-- End .mynt-promo-codes -->
    	</div>
    	{foreach name=gco_boughtloop item=gco from=$gco_bought}
    	{assign var=gc value=$gco->giftCard}
		{assign var=gc_message value=$gc->giftCardMessage}
		{assign var=gc_occasion value=$gc_message->giftCardOccasion}
			{if $gco->status eq 'Completed'}
				{include file="my/giftcards-preview-template.tpl"}
			{/if}
		{/foreach}
    	{else}
	    	{if $gc_received|count>0 && $gco_bought|count>0}
		    {else}
		    <div class="mk-giftcard-sent">
	    		<p class="mk-giftcard-default-msg">You have not bought/sent any giftcards yet.</p>
	    	</div>
		    {/if}
    	{/if}
    	
    	{if $gc_received|count le 0 && $gco_bought|count le 0}
    	<div class="mk-giftcard-sent">
    		<p class="mk-giftcard-default-msg">Start <a href="{$http_location}/giftcards">Gifting</a> now.</p>
    	</div>	
    	{/if}
        {else}
             {$notAllowedMsg} 
        {/if}
    </div>
</div>    
{else}

<div class="mymyntra mk-account-pages my-orders-page">
    <h1 id="top">My Myntra <span class="sep">/</span> Orders</h1>
        {include file="my/tabs.tpl"}
        <br> Login to see this page.
    </div>
{/if}
{/block}
