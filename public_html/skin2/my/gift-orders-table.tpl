<table border="0" cellspacing="0" style="width:90%">
	<col width="125">
	<col width="150">
	<col width="120">
	<col width="160">
	<col width="0">
	<tr class="mk-table-head">
		<th>Order No.</th>
		<th>Date Placed </th>
		<th>Total</th>
		<th>Items</th>
	</tr>
	{foreach key=gcoid item=gco from=$gco_bought}
		{assign var=gc value=$gco->giftCard}
		{assign var=gc_message value=$gc->giftCardMessage}
		{assign var=gc_occasion value=$gc_message->giftCardOccasion}
		{if $gco->status eq 'Completed'}
			<tr class="{cycle name='gift_order_cycle' values='mk-odd,mk-even'}	order-row">
				<td>
					<div class="mk-order-number">{$gco->orderId}</div>
					<div class="mk-order-tip myntra-tooltip mk-hide">
						<h4>Gift Card Details</h4>
						<ul>
							<li>Gift Card worth <span class="red">Rs. {$gc->amount}</span> sent via email to {$gc->recipientName} ({$gc->recipientEmail})</li>
						</ul>
					</div>
				</td>
				<td>{$gc->deliveredDate|date_format:"%d %B, %Y"}</td>
				<td><span class="rupees">{$rupeesymbol} {$gc->amount}</span></td>
				<td>Giftcard</td>
			</tr>
		{/if}
	{/foreach}
</table>

