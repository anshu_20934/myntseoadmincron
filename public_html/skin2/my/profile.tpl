
{extends file="layout.tpl"}

{block name=body}
{if $showPage}
<div class="mymyntra mk-account-pages my-profile-page">
    <h1 id="top">My Myntra <span class="sep">/</span> Profile</h1>
    {include file="my/tabs.tpl"}

    <div class="mk-mynt-content">
    {if $customerUser}
        {if $successMsg}
            <div class="success">
                {$successMsg}
            </div>
        {/if}
        <div class="mk-acct-information" id="mk-acct-block">
        	<div class="my-block-links">SCROLL TO <button class="btn link-btn btn-more" value="more-info">MORE ABOUT YOU</button> / <button class="btn link-btn btn-address" value="addresses">ADDRESS BOOK</button></div>
            <h2>Account Information</h2>

            {if $userProfileData.mobile eq ''}
                <div class="note"><span class="alert-icon"></span>
                    Note: Please provide a contact number on which we can reach you for any issues related to your transactions.
                    This number is also required to be verified for usage of any user-specific coupons that you may have been issued through Myntra.
                </div>
            {/if}

           <!--  <p><label>Name :</label> {if $userProfileData.firstname|trim}{$userProfileData.firstname} {$userProfileData.lastname}{else}-{/if}</p>
            <p><label>Email :</label> {$userProfileData.login}</p>
            <p><label>Mobile :</label> {if $userProfileData.mobile}{$userProfileData.mobile}{else}-{/if}</p>
            {if $userProfileData.gender}<p><label>Gender :</label> {$userProfileData.gender}</p>{/if}
             -->
 

            <form action="{$https_location}/mymyntra.php?view=myprofile" method="post" id="form-profile"
                    class="prof-pass show {if $smarty.post._action eq 'change-password' and $errors}pwd-show{/if}">
                <input type="hidden" name="_token" value="{$USER_TOKEN}">
                <input id="action_type" type="hidden" name="_action" value="update-profile-data">

                {foreach $errors as $error}
                <div class="err">
                    {$error}
                </div>
                {/foreach}

                <div class="row pro-grp">
                	<div class="mk-f-left">
                    	<label>Name *</label>
                    </div>
                    <div class="mk-f-right">
                    	<input type="text" name="name" id="name"
                            value="{$userProfileData.name}" data-value="{$userProfileData.name}">
                    	<span class="err"></span>
                    </div>
                </div>
                
                <div class="row pro-grp">
                	<div class="mk-f-left">
                    	<label>Email *</label>
                    </div>
                    <div class="mk-f-right">
                    	<input disabled type="text" name="login" id="login"
                            value="{$userProfileData.login}">
                    	<span class="err"></span>
                    </div>
                </div>
               
                <div class="row pro-grp pwd-block">
                	<div class="mk-f-left">
                    	<label>Password</label>
                 	</div>
                 	<div class="mk-f-right">
                    	<input disabled type="text" name="old-password-disp" id="old-password-disp" value="******">
                    	 <a href="#changepass" id="edit-pass">Edit Password</a> 
                    </div>
                </div>
                
                <div class="row hide pwd-grp">
                	<div class="mk-f-left">
                    	<label>Old Password</label>
                  </div>
					      <div class="mk-f-right">                    	
                    	<input type="password" name="old-password" id="old-password" value="" autocomplete="off">
                    	<a href="#changepass" id="cancel-edit">Cancel Edit</a>
                    	<span class="err"></span>
                    </div>
                </div>
                <div class="row hide pwd-grp">
                	<div class="mk-f-left">
                    	<label>New Password</label>
                    </div>
                    <div class="mk-f-right">
                    	<input type="password" name="new-password" id="new-password" value="" autocomplete="off">
                    	<span class="err"></span>
                    </div>
                </div>
                <div class="row hide pwd-grp">
                	<div class="mk-f-left">
                    	<label>Confirm New Password</label>
                    </div>
                    <div class="mk-f-right">
                    	<input type="password" name="confirm-new-password" id="confirm-new-password" value="" autocomplete="off">
                    	<span class="err"></span>
                    </div>
                </div>
                <div class="row pro-grp">
                    <div class="mk-f-left">
                    	<label>Mobile (10 Digit) *</label>
                    </div>
                    <div class="mk-f-right">
                    	<input type="text" name="mobile" maxlength="10" id="mobile"
                            value="{$userProfileData.mobile}" data-val="{$userProfileData.mobile}">
                    	<span class="err"></span>
                    </div>
                </div>
                <div class="row radio-block pro-grp">
                    <div class="mk-f-left">
                    	<label>GENDER</label>
                    </div>
                    <div class="mk-f-right">
                    	<input class="gender-sel" type="radio" name="gender-sel" value="M" {if $userProfileData.gender|lower eq 'm' || $userProfileData.gender eq ''}checked data-val="M"{/if}><span>Male</span>
                    	<input class="gender-sel" type="radio" name="gender-sel" value="F" {if $userProfileData.gender|lower eq 'f'}checked data-val="F"{/if}><span>Female</span>
                    </div> 
                </div>
                <div class="row action">
                    <button type="submit" class="btn primary-btn small-btn btn-save disabled-btn">Save</button>
                    
                </div>
            </form>
        </div>
		
		<!-- MORE ABOUT YOU -->
		<div class="mk-acct-more" id="more-info">
		<div class="my-block-links">SCROLL TO <button class="btn link-btn btn-more" value="mk-acct-block">ACCOUNT INFORMATION</button> / <button class="btn link-btn btn-address" value="addresses">ADDRESS BOOK</button></div>
		  <div id="more-info-control"><span class="open-state icon"></span><span class="text">HIDE THIS</span></div>
		  <h2>MORE ABOUT YOU</h2>
		  <p class="more-info-text">Tell us more about yourself so that we can recommend you products and send you more relevant mailers.
Your personal information is safe with us and will not be shared with anyone.</p>
	<div id="more-info-inner" >
		{if !$userPersonnaData || $userPersonnaData.update_from_fb} <p class="more-from-option-text">SHARE YOUR PROFILE INFORMATION BY CONNECTING VIA FACEBOOK:</p>
		  <button data-referer="" class="btn normal-btn small-btn prof-btn-fb-connect" type="button">
				<span class="ico-fb-small"></span> Connect with Facebook
		  </button>{/if}
		  <p class="more-from-option-text">{if !$userPersonnaData || $userPersonnaData.update_from_fb}OR,{/if} SIMPLY FILL IN THE DETAILS BELOW</p>
			<form action="{$http_location}/mymyntra.php?view=myprofile#more-info" method="post" id="form-profile-more">
				<input type="hidden" name="_token" value="{$USER_TOKEN}">
            	<input type="hidden" name="_action" value="update-more-info">
            	<div class="mk-custom-drop-down prof-year row">
                    <div class="mk-f-left">
                    	<label>YEAR OF BIRTH</label>
                    </div>
                     <div class="mk-f-right">
                  	 	<select name="sel-year" class="sel-year">
                   			<option value="0000">Year</option>
                	  		{section name=year loop=$smarty.now|date_format:'%Y' max=100 step=-1}
               	    		<option value="{$smarty.section.year.index}" {if $userProfileData.DOB|date_format:'%Y' eq $smarty.section.year.index}selected{/if}>{$smarty.section.year.index}</option>
                   			{/section}
						</select>
						{if $userProfileData.DOB && $userProfileData.DOB|date_format:'%Y-%m-%d' != $smarty.now|date_format:'%Y-%m-%d'}<span class="success-icon"></span>{/if}
					</div>
				</div>
				<div class="mk-custom-drop-down prof-birthday  row">
				 	<div class="mk-f-left">
				  		<label>BIRTHDAY</label>
				  	</div>
				  	 <div class="mk-f-right">
                 	  	<select name="sel-date" class="sel-date">
                   			<option value="00">Date</option>
                   			{section name=date start=1 loop=32 step=1}
                  	 		<option value="{$smarty.section.date.index}" {if $userProfileData.DOB|substr:8:2 eq $smarty.section.date.index}selected{/if}>{$smarty.section.date.index}</option>
                   			{/section}
						</select>
						<select name="sel-month" class="sel-month">
                   			<option value="00">Month</option>
                   			{foreach name=month key=key item=month from=$months}
                   			<option value="{$smarty.foreach.month.index+1}" {if $userProfileData.DOB|substr:5:2 eq $smarty.foreach.month.index+1}selected{/if}>{$month}</option>
                   			{/foreach}
						</select>
						{if $userProfileData.DOB}{if $userProfileData.DOB|substr:5:2 neq '00' && $userProfileData.DOB|substr:8:2 neq '00'}<span class="success-icon"></span>{/if}{/if}
					</div>
				</div>
				<div class="mk-custom-drop-down prof-rel-status row">
					 <div class="mk-f-left">
                  	<label>RELATIONSHIP STATUS</label>
                  	</div>
                  	 <div class="mk-f-right">
                   		<select name="sel-rel-status" class="sel-rel-status">
                   			<option value="">SELECT</option>
                   			{foreach name=relationship key=key item=relationship from=$relationship_status}
                   			<option value="{$relationship}" {if $userPersonnaData.relationship_status|lower eq $relationship|lower}selected{/if}>{$relationship}</option>
                   			{/foreach}
						</select>
							{if $userPersonnaData.relationship_status}<span class="success-icon"></span>{/if} 
					</div>
				</div>
				<div class="mk-custom-drop-down prof-employment row">
				 	<div class="mk-f-left">
                   		<label>EMPLOYMENT</label>
                   	</div>
                   	 <div class="mk-f-right">
                   		<select name="sel-employment" class="sel-employment">
                   			<option value="">SELECT</option>
                   			{foreach name=employment key=key item=employmentItem from=$employment}
                   			<option value="{$employmentItem}" {if $userPersonnaData.employment eq $employmentItem}selected{/if}>{$employmentItem}</option>
                   			{/foreach}
						</select>
						{if $userPersonnaData.employment}<span class="success-icon"></span>{/if}
					</div>
				</div>
				<div class="mk-custom-drop-down prof-education row">
				 	<div class="mk-f-left">
                   		<label>EDUCATION</label>
                   	</div>
                   	 <div class="mk-f-right">
                   		<select name="sel-education" class="sel-education">
                   			<option value="">SELECT</option>
                   			{foreach name=education key=key item=educationItem from=$education}
                   			<option value="{$educationItem}" {if $userPersonnaData.education eq $educationItem}selected{/if}>{$educationItem}</option>
                   			{/foreach}
                   		</select>
                   		{if $userPersonnaData.education}<span class="success-icon"></span>{/if}
                   	</div>
				</div>
				<div class="mk-preferences row">
					<div class="mk-filter-content mk-f-left">
						<label class="box-title">I LIKE TO WEAR{if $userBrands}<span class="success-icon"></span>{/if}</label>
						<input type="hidden" name="_existing_brands" value='{$existingBrands}' />
						<input type="hidden" name="_new_brands" value="" />
						<input type="hidden" name="_existing_interests" value='{$existingInterests}' />
						<input type="hidden" name="_new_interests" value="" />
						<input class="ui-autocomplete-input brands-autocomplete-input" value="SEARCH BRANDS" data-placeholder="SEARCH BRANDS" type="text" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true">
						<span class="mk-clear-brands-input"></span>
		    			<div id="mk-brands-search-results" class="mk-filter-wrapper mk-brands-filters" style="display:none">
		    			</div>
		    			<div id="mk-brands" class="mk-filter-wrapper mk-brands-filters ui-widget-content mk-resizable-box">
						{foreach name=brand key=key item=brand from=$brandsResults}
							<span class="mk-labelx_check  clearfix {$brand[0]|lower|trim|regex_replace:'/\W+/':''}-brand-filter {if $brand[1]}checked{else}unchecked{/if}" data-key="brands" data-value="{$brand[0]}" data-id="{$key}"><span class="cbx"></span><span class="mk-filter-display">{$brand[0]}</span></span>
						{/foreach}
						</div>
						<span class="selected-count"><strong>{$userBrands|count}</strong> selected</span>
					</div>
								
					<div class="mk-filter-content mk-f-right">
						<label class="box-title">I AM A{if $userInterests}<span class="success-icon"></span>{/if}</label>
						<input class="ui-autocomplete-input interests-autocomplete-input" value="SEARCH INTERESTS" data-placeholder="SEARCH INTERESTS" type="text" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true">
						<span class="mk-clear-interests-input"></span>
		    			<div id="mk-interests-search-results" class="mk-filter-wrapper mk-interests-filters" style="display:none">
		    			</div>
		    			<div id="mk-interests" class="mk-filter-wrapper mk-interests-filters ui-widget-content mk-resizable-box">
						{foreach name=interest key=key item=interest from=$interestsResults}
							<span class="mk-labelx_check clearfix {$interest[0]|lower|trim|regex_replace:'/\W+/':''}-interest-filter {if $interest[1]}checked{else}unchecked{/if}" data-key="interests" data-value="{$interest[0]}" data-id="{$key}"><span class="cbx"></span><span class="mk-filter-display">{$interest[0]}</span></span>
						{/foreach}
						</div>
						<span class="selected-count"><strong>{$userInterests|count}</strong> selected</span>
					</div>
				</div>
				
				
				<button type="submit" class="btn primary-btn small-btn btn-save more-about-you">Save Changes</button>
				
			</form>
			</div>
		
		</div>
		<!-- MORE ABOUT YOU END-->
		
		
        {if $defaultAddress}
        <div class="mk-default-addresses" id="addresses">
        	<div class="my-block-links">SCROLL TO <button class="btn link-btn btn-more" value="mk-acct-block">ACCOUNT INFORMATION</button> / <button class="btn link-btn btn-more" value="more-info">MORE ABOUT YOU</button>  </div>
            <h2>Default Shipping Address</h2>
            <address class="mk-default-shipping">
                <p class="name">{$defaultAddress.name}</p>
                <p>{$defaultAddress.address}</p>
                <p>{$defaultAddress.locality}</p>
                <p>{$defaultAddress.city} - {$defaultAddress.pincode}</p>
                <p>{$defaultAddress.state_display}</p>
                <p><label class="mobile">Mobile : </label>{$defaultAddress.mobile}</p>
            </address>
        </div>
        <div class="mk-address-book">
        {else}
        <div class="mk-address-book" id="addresses">
        	<div class="my-block-links">SCROLL TO <button class="btn link-btn btn-more" value="mk-acct-block">ACCOUNT INFORMATION</button> / <button class="btn link-btn btn-more" value="more-info">MORE ABOUT YOU</button>  </div>
        {/if}
            <h2>Address Book</h2>
            <h6><a href="#add-address" class="btn normal-btn small-btn add-new-address">Add a New Address</a></h6>
            <div class="address-list-wrap">
            </div>
        </div>
    </div>
    {else}
       {$notAllowedMsg} 
    {/if}
    {else}
        <div class="mymyntra mk-account-pages my-orders-page">
          <h1 id="top">My Myntra <span class="sep">/</span> Orders</h1>
          {include file="my/tabs.tpl"}
       <br> Login to see your profile.
       </div>
    {/if}

</div>

{/block}
{block name=pagejs}
<script>
var showSection='{$showSection}';

</script>
{/block}

