{extends file="layout.tpl"}

{block name=body}
{if $showPage}
<div class="mymyntra mk-account-pages mk-myntcredits-page">
    <h1 id="top"> My Myntra <span class="sep">/</span> Mynt Credits </h1>
    {include file="my/tabs.tpl"}
     <div class="mk-mynt-content">
        <h2> COUPONS </h2>
     		<div class="gray coupon-cashback-msg"> APPLY COUPONS ON BAG AND CASHBACK ON PAYMENT PAGE </div>
     	{if $styleExclusionFG}
     		<div class="coupon-app-info"> COUPONS ARE NOT APPLICABLE ON 
     								<span id="coupon-exclusion-tt" style="color:#808080;text-decoration:underline;" 
						title='{$couponExclusionTT}' >
						SOME PRODUCTS</span></div>     		
        {elseif $condNoDiscountCouponFG }
            <div class="coupon-app-info"> COUPONS CAN BE APPLIED ONLY ON NON-DISCOUNTED ITEMS</div>     		
     	{/if}
        {if $active_coupons}
			<div class="mk-mynt-promo-codes mk-active-coupons">
				<h6 class="clickable">My Active Coupons<span> [{$active_coupons|count}]</span></h6>
				<div class="mk-coupons-content">
				<table>
					<col width="150" />
					<col width="450" />
					<col width="250" />
					<col width="0" />
					<tr class="mk-table-head"> 
						<th>Coupon</th>
						<th class="left">Description</th>
						<th>Exp Date</th>
						
					</tr>
					{section name=prod_num loop=$active_coupons}
					<tr class="{cycle name='active_cycle' values='mk-odd,mk-even'}" id="coupon-{$active_coupons[prod_num].coupon|lower}">
						<td>{$active_coupons[prod_num].code|upper}</td>
						<td class="left">
						{if $active_coupons[prod_num].couponType eq "absolute"}
                           	<span class="rupees">Rs. {$active_coupons[prod_num].mrpAmount|round}</span> off
                           {elseif $active_coupons[prod_num].couponType eq "percentage"}
                           	{$active_coupons[prod_num].mrpPercentage}% off
                           {elseif $active_coupons[prod_num].couponType eq "dual"}
                           	{$active_coupons[prod_num].mrpPercentage}% off upto
                           	<span class="rupees">Rs. {$active_coupons[prod_num].mrpAmount|round}</span>
                           	
                           	{elseif $active_coupons[prod_num].couponType eq "max_percentage"}
                           	upto {$active_coupons[prod_num].mrpPercentage}% off
                           	
                           	{elseif $active_coupons[prod_num].couponType eq "max_percentage_dual"}
                           	{$active_coupons[prod_num].mrpPercentage}% off*
                           	
                        	{/if}
                       	{if $active_coupons[prod_num].minimum neq '' && $active_coupons[prod_num].minimum gt 0}
                           	on a minimum purchase of <span class="rupees">Rs. {$active_coupons[prod_num].minimum|round}</span>
                       	{/if}
						</td>
						<td>{$active_coupons[prod_num].expire|date_format:"%e %b, %Y"}
                       	({math equation= "ceil(x/86400) - ceil(y/86400) + 1" x=$active_coupons[prod_num].expire y=$today assign="valid_days"}{$valid_days}
                       	{if $valid_days eq 1}day{else}days{/if} to expiry)</td>
                       	{***for special shopping fest coupon share coupon with friends***}
                       	<td class="share-coupon">
                       	{if $myntraShopFestCoupons && $active_coupons[prod_num].groupName|upper eq $shopFestCouponGroupName}
								{*<div><button class="link-btn" data-coupon-code="{$active_coupons[prod_num].code|lower}">TRANSFER</button></div>*}
			{/if}
                       	</td>
					</tr> 
					{/section}
				</table>
				</div>
			</div> <!-- End .mynt-promo-codes -->
		{else}
			<div class="mk-mynt-promo-codes mk-active-coupons">
				<h6 class="clickable">My Active Coupons<span> [0]</span></h6>
				<div class="mk-coupons-content no-data-msg mk-hide">You do not have any active coupons.</div>
			</div>		
		{/if}	
		{if $used_locked_coupons}
			<div class="mk-mynt-promo-codes mk-used-coupons">
				<h6 class="clickable"> My Used Coupons<span> [{$used_locked_coupons|count}]</span></h6>
				<div class="mk-coupons-content" style="display:none;">
				<table>
					<col width="150" />
					<col width="450" />
					<col width="250" />
						<tr class="mk-table-head"> 
						<th>Coupon</th>
						<th class="left">Description</th>
						<th>Used Date</th>
					</tr>
					{section name=prod_num loop=$used_locked_coupons}
					<tr class="{cycle name='used_cycle' values='mk-odd,mk-even'}">
						<td>{$used_locked_coupons[prod_num].code|upper}</td>
						<td class="left">
						{if $used_locked_coupons[prod_num].couponType eq "absolute"}
                           	<span class="rupees">Rs. {$used_locked_coupons[prod_num].mrpAmount|round}</span> off
                           {elseif $used_locked_coupons[prod_num].couponType eq "percentage"}
                           	{$used_locked_coupons[prod_num].mrpPercentage}% off
                           {elseif $used_locked_coupons[prod_num].couponType eq "dual"}
                           	{$used_locked_coupons[prod_num].mrpPercentage}% off upto
                           	<span class="rupees">Rs. {$used_locked_coupons[prod_num].mrpAmount|round}</span>
                           	
                           	{elseif $active_coupons[prod_num].couponType eq "max_percentage"}
                           	upto {$active_coupons[prod_num].mrpPercentage}% off                           	
                       	{/if}
                       	{if $used_locked_coupons[prod_num].minimum neq '' && $used_locked_coupons[prod_num].minimum gt 0}
                           	on a minimum purchase of <span class="rupees">Rs. {$used_locked_coupons[prod_num].minimum|round}</span>
                       	{/if}
                       	</td>
						<td>{$used_locked_coupons[prod_num].used_on|date_format:"%e %b, %Y"}</td>
					</tr>
					{/section} 
				</table>
				</div>
			</div> <!-- End .mynt-promo-codes -->
		{/if}
		{if $expired_coupons}
			<div class="mk-mynt-promo-codes mk-expired-coupons">
				<h6 class="clickable"> My Expired Coupons<span> [{$expired_coupons|count}]</span></h6>
				<div class="mk-coupons-content" style="display:none;">
				<table>
					<col width="150" />
					<col width="450" />
					<col width="250" />
					<tr class="mk-table-head"> 
						<th>Coupon</th>
						<th class="left">Description</th>
						<th>Exp Date</th>
					</tr>
					{section name=prod_num loop=$expired_coupons}
					<tr class="{cycle name='expired_cycle' values='mk-odd,mk-even'}">
						<td>{$expired_coupons[prod_num].code|upper}</td>
						<td class="left">
						{if $expired_coupons[prod_num].couponType eq "absolute"}
                               <span class="rupees">Rs. {$expired_coupons[prod_num].mrpAmount|round}</span> off
                           {elseif $expired_coupons[prod_num].couponType eq "percentage"}
                               {$expired_coupons[prod_num].mrpPercentage}% off
                           {elseif $expired_coupons[prod_num].couponType eq "dual"}
                               {$expired_coupons[prod_num].mrpPercentage}% off upto
                               <span class="rupees">Rs. {$expired_coupons[prod_num].mrpAmount|round}</span>
                            {elseif $active_coupons[prod_num].couponType eq "max_percentage"}
                           	upto {$active_coupons[prod_num].mrpPercentage}% off
                           {/if}
                           {if $expired_coupons[prod_num].minimum neq '' && $expired_coupons[prod_num].minimum gt 0}
                               on a minimum purchase of <span class="rupees">Rs. {$expired_coupons[prod_num].minimum|round}</span>
                           {/if}
						</td>
						<td>{$expired_coupons[prod_num].expire|date_format:"%e %b, %Y"}</td>
					</tr>
					{/section}
				</table>
				</div>
			</div> <!-- End .mynt-promo-codes -->
		{/if}
		{if $myntCashBalance neq '' || myntCashBalance eq 0}	
			<div class="mk-mynt-promo-codes mk-mynt-credits">
				<h2> CASHBACK </h2>
				<h6 class="clickable"> You have <span class="rupees red">Rs. {if $myntCashBalance neq ''}{$myntCashBalance}{else}0{/if}</span> in your cashback account. </h6>
				<div class="mk-cashback-content">
					<table> 
						<col width="145" />
						<col width="345" />
						<col width="90" />
						<col width="90" />
						<col width="105" />						
						<tr class="mk-table-head">
							<th>Date</th>
	                        <th class="left">Transaction</th>
	                        <th>Credit <span class="rupees">(Rs.)</span></th>
	                        <th>Debit <span class="rupees">(Rs.)</span></th>
	                        <th>Balance <span class="rupees">(Rs.)</span></th>
						</tr>
						{section name=prod_num loop=$myntCashTransactions}
						<tr class="{cycle name='cashback_cycle' values='mk-odd,mk-even'}">
							<td>{$myntCashTransactions[prod_num].modifiedOn|date_format:"%e %b, %Y"}</td>
							<td class="left">{$myntCashTransactions[prod_num].description}</td>
							<td>{if $myntCashTransactions[prod_num].creditInflow gt 0}{$myntCashTransactions[prod_num].creditInflow}{else} - {/if}</td>
							<td>{if $myntCashTransactions[prod_num].creditOutflow gt 0}{$myntCashTransactions[prod_num].creditOutflow}{else} - {/if}</td>
							<td>{$myntCashTransactions[prod_num].balance}</td>
						</tr>
						{/section}
					</table>
				</div>
			</div> <!-- End .mynt-credits -->
		{else}
			<div class="mk-mynt-promo-codes mk-mynt-credits">
				<h2> My Cashback </h2>
				<h6> You do not have any cashback. </h6>
			</div>	
		{/if}
          
	</div>
</div>
{else}

<div class="mymyntra mk-account-pages my-orders-page">
    <h1 id="top">My Myntra <span class="sep">/</span> Orders</h1>
        {include file="my/tabs.tpl"}
        <br> Login to see your mynt credits.
    </div>
{/if}
{/block}

