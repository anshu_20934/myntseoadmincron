{if $showLoyaltyWelcomeMessage}
<div class="mk-loyalty-block loyalty-welcome-msg">
    <div class="lp-welcome-greet-msg">
        <span class="icon-success"></span><span> Congratulations!</span>
    </div>
    <div class="lp-welcome-success-msg">You have successfully activated your My Privilege account</div>
    <div class="close"></div>
</div>
{/if}

<div class="mk-loyalty-block">
    <h3>Current Profile</h3>
    {if isset($loyaltyDisableMessage) && trim($loyaltyDisableMessage)!==''}
    <h4>{$loyaltyDisableMessage}</h4>
    {/if}
    <div class="loyalty-user-profile-block clearfix">
        {if $currentTierInfo.tierNumber eq 0}
            <h4>Shop for atleast Rs. {$nextTierInfo.minPurchaseValue|number_format:0:".":","} to start earning Privilege Points</h4>
        {else}
            <div class="loyalty-user-current-tier clearfix">
                <i class="loyalty-icon level-{$currentTierInfo.tierNumber}-small-icon fleft"></i>
                <div class="current-tier-info">
                    <div class="current-tier-label">{$currentTierInfo.tierLabel} Membership</div>
                    <ul class="current-tier-desc">
                        <li><i class="icon-tick-green"></i><span>You Earn {$currentTierInfo.awardForHundred|string_format:"%d"} points for every Rs. 100 spent.</span></li>
                        {foreach from=$currentTierInfo.tierMessages key=k item=v}
                            <li>
                                <i class="icon-tick-green"></i>
                                <span>{strip}{$v}{/strip}</span>
                                {if $k == $currentTierInfo.tierMessagesCount-1}<span class="icon-new"></span>{/if}
                            </li>
                        {/foreach}
                    </ul>
                </div>
            </div>
            {if $nextTierInfo.show}
            <div class="loyalty-user-next-tier clearfix">
                <i class="loyalty-icon level-{$nextTierInfo.tierNumber}-tiny-icon fleft"></i>
                <div class="next-tier-info">
                    <div class="next-tier-label">Earn More Points</div>
                    <div class="next-tier-desc">
                        Shop for Rs. {$nextTierInfo.minPurchaseValue|number_format:0:".":","} to upgrade.
                    </div>
                    <ul class="next-tier-desc">
                        <li><i class="icon-tick-gray"></i> <span>Earn {$nextTierInfo.awardForHundred|string_format:"%d"} points for every Rs. 100 spent.</span></li>
                        {foreach from=$nextTierInfo.tierMessages key=k item=v}
                            <li>
                              <i class="icon-tick-gray"></i><span>{$v}</span>
                              {if $k == $currentTierInfo.tierMessagesCount-1}<span class="icon-new"></span>{/if}
                            </li>
                        {/foreach}
                    </ul>    
            </div>
            {/if}
        {/if}
    </div>
</div>


<div class="mk-loyalty-block">
    <h3 style="margin-bottom:0px;">My points</h3>
    <div class="loyalty-mypoints active-points clearfix" style="margin-top: 0px;">
        <div class="lp-circle"><div class="lp-points green">{$activePointsBalance}</div></div>
        <div class="loyalty-mypoints-desc">
            <div class="mypoints-label">Active Points</div>
            <div class="mypoints-desc">
                {if $currentTierInfo.tierNumber eq 0}
                    Please upgrade to Level 1 to start earning points
                {else}
                    {if $activePointsBalance > 0}<span class="green">worth Rs. {$activePointsBalanceRupees}.</span> Use them in payment section.{/if}
                {/if}
            </div>
        </div>
    </div>
    {if $inActivePointsBalance > 0}
    <div class="loyalty-mypoints pending-points clearfix">
        <div class="lp-circle"><div class="lp-points">{$inActivePointsBalance}</div></div>
        <div class="loyalty-mypoints-desc">
            <div class="mypoints-label">Pending Points</div>
            <div class="mypoints-desc gray">Available for purchase after return period of your purchase is over.</div>
        </div>
        <a class="btn normal-btn get-them-now" href="/mymyntra.php?view=myorders" onClick="_gaq.push(['_trackEvent', 'mymyntra', 'loyalty-claim-points','click']);return true;">Get them now</a>
    </div>
    {/if}
</div> 

<div class="mk-loyalty-block lp-recent-activity">
    <h3>My Activity <span class="info-msg">&nbsp;&nbsp;{$loyaltyDisclamer}</span></h3>
    <input type="hidden" name="transactionHistoryStart" value="{$transactionHistoryStart}" />
    <input type="hidden" name="transactionHistorySize" value="{$transactionHistorySize}"/>
    <table class="recent-activity-table">
        {foreach item=activity from=$recentActivity key=num}
            {if $activity.showRow}
                <tr>
                    <td class="points-stat"> {if $activity.creditInflow}+{$activity.creditInflow}{else}-{$activity.creditOutflow}{/if} Points</td>
                    <td class="points-activity-desc">{$activity.description}</td>
                    {math equation="a/1000" a=$activity.modifiedOn assign=modOn}
                    <td class="points-activity-date {if $modOn > $fiveDaysAgo}timeago{/if}" title='{$modOn|date_format:"%Y-%m-%dT%H:%M:%S"}'>{$modOn|date_format:"%B %e, %Y"}</td>
                </tr>
            {/if}
        {/foreach}
    </table>
    {if $showMoreRecentActivity}
    <div class="see-more-activity">
        <span class="showLoading hide"><img src="http://myntra.myntassets.com/skin2/images/loader.gif"/><span>Loading...</span></span>
        <span class="see-more-activity-label">See more activity...</span>
    </div>
    {elseif $recentActivity|@count ==0}
    <div class="no-recent-activity">
        No recent activity...
    </div>
    {/if}
</div>    