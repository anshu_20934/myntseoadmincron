{if $isActivatedUser}
    {include file="my/myntloyalty-user-loyalty-info.tpl"}
    {include file="my/myntloyalty-general-gyan.tpl"}
{else}
    {include file="my/myntloyalty-general-gyan.tpl"}
    {* Change in design we are showing tnc in modal for all user *}
    <div id="mkloyalty-terms-modal" class="lightbox mk-hide loyalty-tc-lightbox">
    	<div class="mod">
		    <div class="hd">
		       <h2>Get started</h2>
		    </div>
		    <div class="bd">
		        <div class="terms-title">Terms & Condition</div>
		            <div class="terms-n-conditions">
		                <div class="terms-n-condition-content">
        					{include file="my/myntloyalty-tnc.tpl"}
			            </div>
				    </div>
			        <div class="ft">
			            <div class="terms-n-condition-control">
			                <form>
			                    <input type="hidden" name="_token" value="{$USER_TOKEN}"/>
			                    <label><input type="checkbox" name="loyalty-agree-tnc" /><span>I accept all terms and conditions</span></label><br/>
			                    <button class="btn primary-btn agree-btn disabled-btn">Agree</button>
			                </form>
			            </div>
			        </div>
			</div>
		</div>	
    </div>
{/if}

