     <div class="mk-mynt-content">
        <h2> COUPONS </h2>
     		<div class="gray coupon-cashback-msg"> APPLY YOUR <span class="red">COUPONS AND CASHBACK</span> ON YOUR PAYMENT PAGE </div>
     	{if $styleExclusionFG}
     		<div class="coupon-app-info"> COUPONS ARE NOT APPLICABLE ON 
     								<span id="coupon-exclusion-tt" style="color:#808080;text-decoration:underline;" 
						title='{$couponExclusionTT}' >
						SOME PRODUCTS</span></div>     		
        {elseif $condNoDiscountCouponFG }
            <div class="coupon-app-info"> COUPONS CAN BE APPLIED ONLY ON NON-DISCOUNTED ITEMS</div>     		
     	{/if}
        {if $active_coupons}
			<div class="mk-mynt-promo-codes mk-active-coupons">
				<h6 class="clickable">My Active Coupons<span> [{$active_coupons|count}]</span></h6>
				<div class="mk-coupons-content">
				<table>
					<col width="150" />
					<col width="450" />
					<col width="250" />
					<col width="0" />
					<tr class="mk-table-head"> 
						<th>Coupon</th>
						<th class="left">Description</th>
						<th>Exp Date</th>
						
					</tr>
					{section name=prod_num loop=$active_coupons}
					<tr class="{cycle name='active_cycle' values='mk-odd,mk-even'}" id="coupon-{$active_coupons[prod_num].coupon|lower}">
						<td>{$active_coupons[prod_num].coupon|upper}</td>
						<td class="left">
						{if $active_coupons[prod_num].couponType eq "absolute"}
                           	<span class="rupees">Rs. {$active_coupons[prod_num].MRPAmount|round}</span> off
                           {elseif $active_coupons[prod_num].couponType eq "percentage"}
                           	{$active_coupons[prod_num].MRPpercentage}% off
                           {elseif $active_coupons[prod_num].couponType eq "dual"}
                           	{$active_coupons[prod_num].MRPpercentage}% off upto
                           	<span class="rupees">Rs. {$active_coupons[prod_num].MRPAmount|round}</span>
                        	{/if}
                       	{if $active_coupons[prod_num].minimum neq '' && $active_coupons[prod_num].minimum gt 0}
                           	on a minimum purchase of <span class="rupees">Rs. {$active_coupons[prod_num].minimum|round}</span>
                       	{/if}
						</td>
						<td>{$active_coupons[prod_num].expire|date_format:"%e %b, %Y"}
                       	({math equation= "ceil(x/86400) - ceil(y/86400) + 1" x=$active_coupons[prod_num].expire y=$today assign="valid_days"}{$valid_days}
                       	{if $valid_days eq 1}day{else}days{/if} to expiry)</td>
                       	{***for special shopping fest coupon share coupon with friends***}
                       	<td class="share-coupon">
                       	{if $myntraShopFestCoupons && $active_coupons[prod_num].groupName|upper eq $shopFestCouponGroupName}
								{*<div><button class="link-btn" data-coupon-code="{$active_coupons[prod_num].coupon|lower}">TRANSFER</button></div>*}
			{/if}
                       	</td>
					</tr> 
					{/section}
				</table>
				</div>
			</div> <!-- End .mynt-promo-codes -->
		{else}
			<div class="mk-mynt-promo-codes mk-active-coupons">
				<h6 class="clickable">My Active Coupons<span> [0]</span></h6>
				<div class="mk-coupons-content no-data-msg mk-hide">You do not have any active coupons.</div>
			</div>		
		{/if}	
		{if $used_locked_coupons}
			<div class="mk-mynt-promo-codes mk-used-coupons">
				<h6 class="clickable"> My Used Coupons<span> [{$used_locked_coupons|count}]</span></h6>
				<div class="mk-coupons-content" style="display:none;">
				<table>
					<col width="150" />
					<col width="450" />
					<col width="250" />
						<tr class="mk-table-head"> 
						<th>Coupon</th>
						<th class="left">Description</th>
						<th>Used Date</th>
					</tr>
					{section name=prod_num loop=$used_locked_coupons}
					<tr class="{cycle name='used_cycle' values='mk-odd,mk-even'}">
						<td>{$used_locked_coupons[prod_num].coupon|upper}</td>
						<td class="left">
						{if $used_locked_coupons[prod_num].couponType eq "absolute"}
                           	<span class="rupees">Rs. {$used_locked_coupons[prod_num].MRPAmount|round}</span> off
                           {elseif $used_locked_coupons[prod_num].couponType eq "percentage"}
                           	{$used_locked_coupons[prod_num].MRPpercentage}% off
                           {elseif $used_locked_coupons[prod_num].couponType eq "dual"}
                           	{$used_locked_coupons[prod_num].MRPpercentage}% off upto
                           	<span class="rupees">Rs. {$used_locked_coupons[prod_num].MRPAmount|round}</span>
                       	{/if}
                       	{if $used_locked_coupons[prod_num].minimum neq '' && $used_locked_coupons[prod_num].minimum gt 0}
                           	on a minimum purchase of <span class="rupees">Rs. {$used_locked_coupons[prod_num].minimum|round}</span>
                       	{/if}
                       	</td>
						<td>{$used_locked_coupons[prod_num].used_on|date_format:"%e %b, %Y"}</td>
					</tr>
					{/section} 
				</table>
				</div>
			</div> <!-- End .mynt-promo-codes -->
		{/if}
		{if $expired_coupons}
			<div class="mk-mynt-promo-codes mk-expired-coupons">
				<h6 class="clickable"> My Expired Coupons<span> [{$expired_coupons|count}]</span></h6>
				<div class="mk-coupons-content" style="display:none;">
				<table>
					<col width="150" />
					<col width="450" />
					<col width="250" />
					<tr class="mk-table-head"> 
						<th>Coupon</th>
						<th class="left">Description</th>
						<th>Exp Date</th>
					</tr>
					{section name=prod_num loop=$expired_coupons}
					<tr class="{cycle name='expired_cycle' values='mk-odd,mk-even'}">
						<td>{$expired_coupons[prod_num].coupon|upper}</td>
						<td class="left">
						{if $expired_coupons[prod_num].couponType eq "absolute"}
                               <span class="rupees">Rs. {$expired_coupons[prod_num].MRPAmount|round}</span> off
                           {elseif $expired_coupons[prod_num].couponType eq "percentage"}
                               {$expired_coupons[prod_num].MRPpercentage}% off
                           {elseif $expired_coupons[prod_num].couponType eq "dual"}
                               {$expired_coupons[prod_num].MRPpercentage}% off upto
                               <span class="rupees">Rs. {$expired_coupons[prod_num].MRPAmount|round}</span>
                           {/if}
                           {if $expired_coupons[prod_num].minimum neq '' && $expired_coupons[prod_num].minimum gt 0}
                               on a minimum purchase of <span class="rupees">Rs. {$expired_coupons[prod_num].minimum|round}</span>
                           {/if}
						</td>
						<td>{$expired_coupons[prod_num].expire|date_format:"%e %b, %Y"}</td>
					</tr>
					{/section}
				</table>
				</div>
			</div> <!-- End .mynt-promo-codes -->
		{/if}
	</div>

