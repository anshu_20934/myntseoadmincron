<li>
	<div class="photo">
		{if $item.product_style|trim}<a href="{$http_location}/{$item.product_style}" onclick="_gaq.push(['_trackEvent','pdp_click', Myntra.Data.pageName, 'cart_image', parseInt({$item.product_style})]);" target="_blank">{/if}
			<img src="{myntraimage key="style_96_128" src=$item.default_image}" />
		{if $item.product_style|trim}</a>{/if}
	</div>
	<div class="details">
        <div class="title">
            {if $item.product_style|trim}<a href="{$http_location}/{$item.product_style}" onclick="_gaq.push(['_trackEvent','pdp_click', Myntra.Data.pageName, 'cart_title', parseInt({$item.product_style})]);" target="_blank">{/if}
				{$item.stylename}
			{if $item.product_style|trim}</a>{/if}
        </div>
        <div class="gray"><label>Size:</label> {if $item.unifiedsize}{$item.unifiedsize}{else}{$item.size}{/if}<label> / Qty:</label> {$item.quantity} &nbsp;&nbsp;<label>Product code:</label> {$item.product_style}</div>
		{assign var=loyalty_points_awarded value=round($item.item_loyalty_points_awarded)}
		
		{if (($item['return_criteria'] == 'allowed' || $item['return_criteria'] == 'not_returnable' || $item['return_criteria'] == 'jewellery') && !$item['returnid'] && !$item['exchange_orderid'])}
			{assign var=canClaimLoyaltyPoints value=1}
		{else}
			{assign var=canClaimLoyaltyPoints value=0}
		{/if}
		{if $item.is_returnable && $item.item_loyalty_points_awarded > 0}
		<div class="clearfix lp-action-container">
            <div class="lp-circle"><div class="lp-points">{$loyalty_points_awarded}</div></div> 
            {if $delivered && $canClaimLoyaltyPoints}
	            <div class="lp-action">
	            	<div class="lp-confirm-msg"><span>Points <span class="light-grey">/</span> Once you claim points you <span class="red">cannot return</span> this item.</span>&nbsp;<a data-orderid="{$grouporderid}" class="btn small-btn normal-btn lp-confirm-get-points">Confirm</a></div>
	            	<div class="lp-confirm-dialog hide"><span>Points <span class="light-grey">/</span> <span class="red">Are you sure </span> you don't want to return this item </span>&nbsp;<a data-orderid="{$grouporderid}" data-getpoints-key="{$item.orderid}-{$item.itemid}" class="btn small-btn normal-btn lp-get-points-yes">Yes</a><a data-orderid="{$grouporderid}" class="btn small-btn normal-btn lp-get-points-no">No</a></div>
	            </div>
            {elseif $delivered && !$canClaimLoyaltyPoints}
            	<div class="lp-action" style="margin-top: 13px;">
            		<div class="lp-confirm-msg">
            			<span>Points <span class="light-grey">/</span> 
            				{if $item['returnid'] && !empty($item['is_refunded']) &&  $item['is_refunded']}
            				    You cannot get points for returned items
            				{else}            				
	            				{if $item['return_criteria'] == 'time_exceeded'}
	            				    Points have already been activated for this item. Please note that return/exchange period is over for this item.
	            				{else}
	            					You cannot get points for return/exchange initiated items.      				
	            				{/if}
	            				
            				{/if}
            			</span>
            		</div>
            	</div>
            {else}
            	<div class="lp-action" style="margin-top: 13px;">
            		<div class="lp-confirm-msg"><span>Points <span class="light-grey">/</span> Can be claimed after this product gets delivered</span></div>
            	</div>	
            {/if}
        </div>
        {/if}
       <div class="loyalty-award-msg  {if !$item.is_returnable && $item.item_loyalty_points_awarded > 0}show{else}hide{/if}">
            <span class="icon-success"></span><span class="msg"> {$loyalty_points_awarded} Privilege Points added to your account <a href="/mymyntra.php?view=myprivilege">Check now</a></span>
       </div>
	</div>
	<div class="amount">
		<div class="final-price">
			{if $item.final_price_paid lt $item.subtotal_price}
				<span class="ico ico-expand"></span>
			{/if}
			{if $item.discount === $item.subtotal_price}
				FREE
			{else}
			<span class="rupees">{$rupeesymbol} </span>{math equation="x + y" x=$item.final_price_paid y=$item.taxamount format="%.2f"}
			{/if}
		</div>
		{if $item.final_price_paid lt $item.subtotal_price}
			<div class="mrp hide">
				MRP : <span class="rupees">{$rupeesymbol}<span>
					<span>{$item.subtotal_price|string_format:"%.2f"}</span>
			</div>
			
			<div class="discounts hide">
				{if $item.discount ne 0.00}
					Item Discount : (-) <span class="rupees">{$rupeesymbol} </span> {$item.discount|string_format:"%.2f"}<br />
				{/if}
				{if $item.cart_discount_split_on_ratio ne 0.00}
					Bag Discount : (-) <span class="rupees">{$rupeesymbol} </span> {$item.cart_discount_split_on_ratio|string_format:"%.2f"}<br />
				{/if}				
				{if $item.coupon_discount_product ne 0.00}
					Coupon : (-) <span class="rupees">{$rupeesymbol} </span> {$item.coupon_discount_product|string_format:"%.2f"}<br />
				{/if}
				{if $item.cash_redeemed ne 0.00}
					Cashback : (-) <span class="rupees">{$rupeesymbol} </span> {$item.cash_redeemed|string_format:"%.2f"}<br />
				{/if}
				{if $item.loyalty_points_used_rupees ne 0.00}
					Points : (-) <span class="rupees">{$rupeesymbol} </span> {$item.loyalty_points_used_rupees|string_format:"%.2f"}<br />
				{/if}
				{if $item.pg_discount ne 0.00}
					Gateway : (-) <span class="rupees">{$rupeesymbol} </span> {$item.pg_discount|string_format:"%.2f"}<br />
				{/if}                           
			</div>
		{/if}
	</div>
</li>
			
		
