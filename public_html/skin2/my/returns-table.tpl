<table border="0" cellspacing="0">
	<col width="125">
	<col width="150">
	<col width="120">
	<col width="160">
	<tr class="mk-table-head">
		<th>Return No.</th>
		<th>Date Placed</th>
		<th>Order No.</th>
		{if $tableName eq 'completed'}<th>Refund</th>{/if}
		<th>Status</th>
	</tr>
	{foreach item=return from=$returnsArray}
	<tr class="
		{if $tableName eq 'completed'}
			{cycle name='c_return_cycle' values='mk-odd,mk-even'}
		{elseif $tableName eq 'pending'}
			{cycle name='p_return_cycle' values='mk-odd,mk-even'}
		{/if}
	return-row">
			
		<td>
			<div class="mk-return-number">{$return.returnid}</div>
			<div class="mk-return-tip myntra-tooltip mk-hide">
				<h4>Item being returned :</h4> 
				<ul>
					<li>{$return.productStyleName}</li>
				</ul>
				<h4 class="method">{if $return.return_mode eq 'self'}Self-Shipped{else}Myntra Pickup{/if}</h4>
				<div class="arrow arrow-bottom-outer"><div class="arrow arrow-bottom-inner"></div></div>
			</div>
		</td>
		<td>{$return.createddate_display|date_format:"%d %b, %Y"}</td>
		<td>{$return.group_id}</td>
		{if $tableName eq 'completed'}
			<td>{if $return.is_refunded}<span class="rupees">{$rupeesymbol} </span><strong>{$return.refundamount}</strong>{/if}&nbsp;</td>
		{/if}
		<td>{$return.mymyntra_status}</td>
	</tr>
	{/foreach}
</table>
