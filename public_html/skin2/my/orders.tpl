{extends file="layout.tpl"}

{block name=body}

    <script>
        Myntra.Data.pageName="myorders";
        var shopFest = '{$shopFest}';
        //deeplinking changes
        var oid = {if $oid} {$oid} {else}''{/if};
        var order_action = {if $order_action} "{$order_action}" {else}''{/if};
        Myntra.Data.refundEnabled = {if $refund_enabled}true{else}false{/if};

    </script>
    {if $showPage}
    <div class="mymyntra mk-account-pages my-orders-page">
        <h1 id="top">My Myntra <span class="sep">/</span> Orders</h1>
        {include file="my/tabs.tpl"}
        {if $pendingFlag}
            <h2>Pending</h2>
            {include file="my/orders-table.tpl" tableName='pending'}
        {/if}
    
        {if $completedFlag}
            <h2>Completed</h2>
            {include file="my/orders-table.tpl" tableName='completed'}
        {/if}
        
        {if $giftCardOrdersFlag}
            <h2>Gift Cards</h2>
            {include file="my/gift-orders-table.tpl"}
        {/if}
    
        {if !$pendingFlag && !$completedFlag && !$giftCardOrdersFlag}
            <div class="no-data-msg">You have not placed any orders yet.</div>
        {/if}
    
    </div>
    {else}
         <div class="mymyntra mk-account-pages my-orders-page">
            <h1 id="top">My Myntra <span class="sep">/</span> Orders</h1>
            {include file="my/tabs.tpl"}
               <br> Login to see your orders.
        </div>
        {/if}

{/block}


{block name=lightboxes}
{if $showPage}

{include file="my/fork-exchange.tpl"}   


{foreach key=gcoid item=gco from=$gco_bought}
    {assign var=gc value=$gco->giftCard}
    {assign var=gc_message value=$gc->giftCardMessage}
    {assign var=gc_occasion value=$gc_message->giftCardOccasion}
    {assign var=previewtype value="mymyntraorders"}
    {if $gco->status eq 'Completed'}
        {include file="my/giftcards-preview-template.tpl"}  
    {/if}
{/foreach}      

{foreach key=grouporderid item=ordermap from=$orders}
{assign var=baseorder value=$ordermap.orders[0]}
{assign var=subordersCount value=$ordermap.orders|@count}
{assign var=gtotal value=$orderTotals[$grouporderid]}
<div id="order-{$grouporderid}" class="lightbox orderbox" data-orderid="{$grouporderid}">
    <div class="mod">
        <div class="hd return-hide">
            <h2 class="title">Order No. {$grouporderid}</h2>
            <span class="date">placed on {$ordermap.orders[0].date|date_format:"%d %b, %Y"}</span>
             <span class="count gray">
                {$ordermap.count} {if $ordermap.count eq 1}item{else}items{/if}
                {if $ordermap.orders|@count gt 1} / Multiple Shipments{/if}
                {if $ordermap.showTrackingLink}/ <span class="track-order" onclick="javascript:_gaq.push(['_trackEvent', 'my_orders', 'orders_overlay_track_order', '{$order.courier_service}']);">Track Order</span>{/if}
            </span>
            <span class="total">
                Total <span class="rupees">{$rupeesymbol} {$ordermap.total|string_format:"%.2f"}</span>
            </span>
            {if !$ordermap.showTrackingLink}<div class="gray">
                Tracking Details for Orders Delivered more than 30 days ago are not available.              
            </div>{/if}
            {if $ordermap.orders[0].gift_status eq 'Y'}
                {assign var=giftDetails value="___"|explode:$ordermap.orders[0].notes}
                <span class="my-gift-msg" >This is a gift <span data-gift-to="{$giftDetails[0]}" data-gift-from="{$giftDetails[1]}" data-gift-readonly="true" data-gift-msg="{$giftDetails[2]}" class="gift-data gift-msg-edit" >VIEW MESSAGE</span></span>
            {/if}
        </div>
        {if $ordermap.returnAllowed}
            <div class="hd return-show">
            {if $exchanges_enabled eq "true"}
                <h2 class="title">Select an Item to Return / Exchange</h2>
            {else}
                <h2 class="title">Select an Item to Return </h2>
            {/if}
                <p class="gray">Only items that are eligible for returns {if $exchanges_enabled eq "true"} / exchanges {/if} as per Myntra's <a href="faqs#returns" target="_blank">Returns {if $exchanges_enabled eq "true"} & exchanges {/if} Policy</a> are shown here.</p>
                <p class="gray">View the <span class="order-details-link">Order Details</span> for a list of all items in this order</p> 
            </div>
        {/if}
        <div class="bd">
        <script type="text/json" id="track-{$grouporderid}">{$ordermap.trackingRequestData}</script>
            <div class="discount-area"></div>
            {if $ordermap.returnAllowed}
                <div class="subhd return-show">
                    <h4>Order No. {$grouporderid}</h4>
                    <p>
                        {$ordermap.count} {if $ordermap.count eq 1}item{else}items{/if}
                        &nbsp;&nbsp;/&nbsp;&nbsp;Order Placed on {$ordermap.orders[0].date|date_format:"%d %b, %Y"}
                    </p>
                </div>
            {/if}
            {foreach name=order item=order from=$ordermap.orders}
                {assign var=shipped value=0}
                <div class="suborder">
                    <div class="subhd return-hide">
                        {if $order.customerstatusmessage|upper == 'SHIPPED' || $order.customerstatusmessage|upper == 'DELIVERED'}
                            {assign var=shipped value=1}
                            <h4>Shipment #{$order.shipmentNo} ({$order.qtyInOrder} {if $order.qtyInOrder eq 1}item{else}items{/if}) <span class="l-gray">/</span> {$order.customerstatusmessage}</h4>
                            <span>
                                via {$order.courier_service} on 
                                {if $order.customerstatusmessage|upper == 'SHIPPED'}
                                    {$order.shippeddate|date_format:"%d %b, %Y"}&nbsp;&nbsp;/&nbsp;&nbsp;Expect Delivery By {$order.expectedDelivery|date_format:"%d %b, %Y"} 
                                {else}
                                    {$order.delivereddate|date_format:"%d %b, %Y"}
                                {/if}
                                {if $order.tracking}
                                    <br /> tracking no.: {$order.tracking}
                                    {if $ordermap.showTrackingLink}
                                        <a href="{$order.website}" target="_blank" class="track-shipment" data-shipmentid="{$order.orderid}"
                                            onclick="javascript:_gaq.push(['_trackEvent', 'my_orders', 'orders_overlay_track_shipment', '{$order.courier_service}']);">Track Shipment</a>
                                    {/if}
                                {/if}
                            </span>
                        {else}
                            <h4>{$order.customerstatusmessage} ({$order.qtyInOrder} {if $order.qtyInOrder eq 1}item{else}items{/if})</h4>
                            <span>We will update the tracking details for {if $order.qtyInOrder eq 1}this item{else}these items{/if} once {if $order.qtyInOrder eq 1}it is{else}they are{/if} shipped.</span>
                        {/if}
                    </div>
                    <ul class="items">
                        {foreach item=item from=$order.items}
                            {include file="my/order-item.tpl" source="myorders"}
                        {/foreach}
                    </ul>
                    {if $subordersCount gt 1}
                    <div class="total return-hide">
                        {if $order.shipping_cost > 0 }
                            shipping: <strong>{$rupeesymbol}{$order.shipping_cost|string_format:"%.2f"}</strong><br/>
                        {/if}
                        {if $order.gift_charges > 0 }
                            Gift Charge: <span class="rupees">{$rupeesymbol}{$order.gift_charges|string_format:"%.2f"}</span><br/>
                        {/if}
                        {if $shipped}Shipment #{$order.shipmentNo}{/if} Total: <span class="rupees">{$rupeesymbol} {$order.total|string_format:"%.2f"}</span>
                    </div>
                    {/if}
                </div>
            {/foreach}
            <div class="mk-cf grand-total return-hide">
                <div class="payment-mode">
                    <label class="gray">Payment Mode</label>
                     {if $gtotal.total == 0}
                    Online Payment  
                     {else}
                    {if $baseorder.payment_method eq 'cod'}
                            Cash On Delivery
                        {elseif $baseorder.payment_method eq 'netbanking'}
                            Net Banking
                        {elseif $baseorder.payment_method eq 'debitcard'}
                            Debit Card
                        {elseif $baseorder.payment_method eq 'creditcards'}
                            Credit Card
                            {elseif $baseorder.payment_method eq 'emi'}
                            EMI (Credit Card)
                            {else}
                                &nbsp;
                        {/if}
                {/if}
                </div>
                <div class="shipping-address">
                    <h4>Shipping Address</h4>
                     <address data-zipcode="{$baseorder.s_zipcode}" data-address='"name":"{$baseorder.s_firstname} {$baseorder.s_lastname}","address":"{$baseorder.s_address|escape:html|nl2br}","locality":"{$baseorder.s_locality}","city":"{$baseorder.s_city}","state_name":"{$baseorder.s_state_name}","state":"{$baseorder.s_state}","zipcode":"{$baseorder.s_zipcode}","mobile":"{$baseorder.mobile}","country":"{$baseorder.s_country}","email":"{$baseorder.login}"' >
                            <div class="name">{$baseorder.s_firstname|unescape: "html"} {$baseorder.s_lastname|unescape: "html"}</div>
                            <div>{$baseorder.s_address|unescape: "html"|nl2br}</div>
                            <div>{$baseorder.s_locality|unescape: "html"}</div>
                            <div>{$baseorder.s_city|unescape: "html"}</div>
                            <div>{$baseorder.s_state_name|unescape: "html"} - {$baseorder.s_zipcode|unescape: "html"}</div>
                            <div><label>Mobile:</label> {$baseorder.mobile|unescape: "html"}</div>
                    </address>
                </div>
                <div class="amount">
                    <div class="final-price">
                        {if $gtotal.total ne $gtotal.subtotal}
                            <span class="ico ico-expand"></span>
                        {/if}
                        Total : <span class="rupees">{$rupeesymbol} </span>{$gtotal.total|string_format:"%.2f"}
                    </div>
                    {if $gtotal.total ne $gtotal.subtotal}
                        <div class="mrp hide">
                            MRP : <span class="rupees">{$rupeesymbol}</span> <span>{$gtotal.subtotal|string_format:"%.2f"}</span>
                        </div>
                        
                        <div class="discounts hide">
                            {if $gtotal.discount ne 0.00}
                                Item Discount : (-) <span class="rupees">{$rupeesymbol} </span> {$gtotal.discount|string_format:"%.2f"}<br />
                            {/if}
                            {if $gtotal.cart_discount ne 0.00}
                                Bag Discount : (-) <span class="rupees">{$rupeesymbol} </span> {$gtotal.cart_discount|string_format:"%.2f"}<br />
                            {/if}
                            {if $gtotal.coupon_discount ne 0.00}
                                Coupon : (-) <span class="rupees">{$rupeesymbol} </span> {$gtotal.coupon_discount|string_format:"%.2f"}<br />
                            {/if}
                            {if $gtotal.cash_redeemed ne 0.00}
                                Cashback : (-) <span class="rupees">{$rupeesymbol} </span> {$gtotal.cash_redeemed|string_format:"%.2f"}<br />
                            {/if}
                            {if $gtotal.pg_discount ne 0.00}
                                Gateway : (-) <span class="rupees">{$rupeesymbol} </span> {$gtotal.pg_discount|string_format:"%.2f"}
                            {/if}
                            {if $gtotal.shipping_cost ne 0.00}
                                <br><span class="shipping-cost">Shipping : <span class="rupees">{$rupeesymbol} </span>{$gtotal.shipping_cost|string_format:"%.2f"}</span>
                            {/if}
                            {if $gtotal.gift_charges ne 0.00}
                                <br><span class="shipping-cost">Gift Charge : <span class="rupees">{$rupeesymbol} </span> {$gtotal.gift_charges|string_format:"%.2f"}</span>
                            {/if}
                            {if $gtotal.emi_charge gt 0.00}
                                <br><span class="shipping-cost">EMI Charge : <span class="rupees">{$rupeesymbol} </span> {$gtotal.emi_charge|string_format:"%.2f"}</span>                      
                            {/if}
                        </div>
                    {/if}
                </div>
            </div>
        </div>
    </div>
</div>
{* Below section is for loyalty*}
{if $loyaltyEnabled}
    <div id="loyalbox-{$grouporderid}" class="lightbox loyalbox" data-orderid="{$grouporderid}">
        <div class="mod">
            <div class="hd">
                <h2 class="title">To get points</h2>
                <p class="gray">Pending points become active after expiry of <span>30 days return/exchange period.</span></p>
                <p class="gray">You can choose to <span>Not return/exchange items</span> in order to activate your pending points.</p>
            </div>
            <div class="bd">
                <div class="subhd">
                    <h4>Order No. {$grouporderid}</h4>
                    <p class="gray">
                        {$ordermap.count} {if $ordermap.count eq 1}item{else}items{/if}
                        &nbsp;&nbsp;/&nbsp;&nbsp;Order Placed on {$ordermap.orders[0].date|date_format:"%d %b, %Y"}
                    </p>
                </div>
                {foreach name=order item=order from=$ordermap.orders}
                    {assign var=delivered value=0}
                    {if $order.customerstatusmessage|upper == 'DELIVERED'}
                        {assign var=delivered value=1}
                    {/if}
                    <div class="suborder">
                        <ul class="items">
                            {foreach item=item from=$order.items}
                                {include file="my/loyal-item.tpl" source="myorders"}
                            {/foreach}
                        </ul>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
{/if}
{*loyalty section ends*}
{/foreach}


{* the below section is for cancellations *}
{if !$nodeCancelEnabled}
{foreach key=grouporderid item=ordermap from=$orders}
{assign var=baseorder value=$ordermap.orders[0]}
{assign var=subordersCount value=$ordermap.orders|@count}
{assign var=gtotal value=$orderTotals[$grouporderid]}
<div id="loading-{$grouporderid}" class="loading" style="display:none">
</div>
<div id="cancel-order-{$grouporderid}" class="lightbox orderbox">
<div class="mod">
    <div class="hd">
        <h2>Cancel Order No. {$grouporderid}</h2>
        <span class="date">placed on {$ordermap.orders[0].date|date_format:"%d %b, %Y"}</span>
        <span>{$ordermap.count} {if $ordermap.count eq 1}item{else}items{/if}</span>
        <span class="total">
            total: <strong>{$rupeesymbol}{$ordermap.total|string_format:"%.2f"}</strong>
        </span>
    </div>
    
    <div class="bd">
    <div class="suborder">
            <div style="padding-top:7px;display:none;color:red;font-size:16px;" id="msg-box-{$grouporderid}">
            </div>
            <div id="cancel-panel-{$grouporderid}">
            <div class="subhd">
                <h5 style=" font-size: 14px;line-height: 2em;">if you have any questions, have a look at our <a target="_blank" href="faqs#cancel">cancellation policy</a></h5>
                {if $shopFest}<span class="shopfest-cancel-msg">{$fest_cancel_message}<br/></span>{/if}         
                <span style="font-size:14px;color:#666666;">ANY COUPONS APPLIED ON THE ORDER WILL BE REINSTATED TO YOUR MYNT CREDITS<br/></span>
                <span style="font-size:14px;color:#666666;">any cashback used with this order will be refunded</span>
            </div>
            <div style="padding-top:16px;font-size:14px;font-family:din-med;padding-bottom:16px;border-bottom:1px solid #666666;">
                {if $shopFest}<span id="shopfest-cancel-child-orders-msg"></span>{/if}
                    {if $gtotal.refundable_amount > 0}
                        <div>you will be refunded {$rupeesymbol} {$gtotal.refundable_amount|string_format:"%.2f"} in the form of cashback</div>
                    {/if}
                    <input type="hidden" name="groupid" value="{$grouporderid}" />
                <div style="padding-top:25px;">
                        <label class="center-align">reason for cancellation *</label>
                    </div>
                    <div id="reason-validation-div-{$grouporderid}" class="cancel-valid">
                        Please select Reason for Cancellation 
                    </div>
                    <div style="padding-top:10px;text-align: center;">
                        <select id="cancel-reason-{$grouporderid}" name="return-reason" style="width: 240px;" class="return-select" onchange='javascript:handleReturnReasonChange(this.value);'>
                            <option value="0">Select Reason</option>
                            {foreach from=$cancellation_reasons key=reasonKey item=reasonValue}
                                   <option value="{$reasonKey}">{$reasonValue}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="tpad25">
                        <label class="center-align">Additional remarks *</label>
                    </div>
                    <div id="remarks-validation-div-{$grouporderid}" class="cancel-valid">
                        Please enter remarks
                    </div>  
                    <div style="padding-top:5px;">  
                        <textarea id="details-{$grouporderid}" name="details" style="height: 60px;width: 240px;"></textarea>
                    </div>
                    <div class="tpad25">
                        <label class="center-align">ARE YOU SURE YOU WANT TO CANCEL THIS ORDER?</label>
                    </div>
                    <div style="padding-top:7px;" id = "cancel-buttons-{$grouporderid}">
                        <button data-orderid = "{$grouporderid}" class="btn normal-btn cancel-submit" style="padding: 5px 10px">Yes</button> &nbsp;&nbsp;
                        <button data-orderid = "{$grouporderid}" class="btn normal-btn cancel-no" style="padding: 5px 10px">no</button>
                    </div>
            </div>
            </div>
            </div>
        {foreach name=order item=order from=$ordermap.orders}
        <div class="suborder">
            <ul class="items">
                {foreach item=item from=$order.items}
                    {include file="my/order-item.tpl" source="myorders"}
                {/foreach}
            </ul>
        </div>
        {/foreach}
         <div class="mk-cf grand-total return-hide">
                <div class="payment-mode">
                    <label class="gray">Payment Mode</label>
                {if $gtotal.total == 0}
                    Online Payment
                {else}
                        {if $baseorder.payment_method eq 'cod'}
                            Cash On Delivery
                        {elseif $baseorder.payment_method eq 'netbanking'}
                            Net Banking
                        {elseif $baseorder.payment_method eq 'debitcard'}
                            Debit Card
                        {elseif $baseorder.payment_method eq 'creditcards'}
                            Credit Card
                            {elseif $baseorder.payment_method eq 'emi'}
                            EMI (Credit Card)
                            {else}
                                &nbsp;
                        {/if}
                {/if}
                </div>
                <div class="shipping-address">
                    <h4>Shipping Address</h4>
                    <address data-zipcode="{$baseorder.s_zipcode}" data-address='"name":"{$baseorder.s_firstname} {$baseorder.s_lastname}","address":"{$baseorder.s_address|escape:html|nl2br}","locality":"{$baseorder.s_locality}","city":"{$baseorder.s_city}","state_name":"{$baseorder.s_state_name}","state":"{$baseorder.s_state}","zipcode":"{$baseorder.s_zipcode}","mobile":"{$baseorder.mobile}","country":"{$baseorder.s_country}","email":"{$baseorder.login}"' >
                        <div class="name">{$baseorder.s_firstname} {$baseorder.s_lastname}</div>
                        <div>{$baseorder.s_address|nl2br}</div>
                        <div>{$baseorder.s_locality}</div>
                        <div>{$baseorder.s_city}</div>
                        <div>{$baseorder.s_state_name} - {$baseorder.s_zipcode}</div>
                        <div><label>Mobile:</label> {$baseorder.mobile}</div>
                    </address>
                </div>
                <div class="amount">
                    <div class="final-price">
                        {if $gtotal.total ne $gtotal.subtotal}
                            <span class="ico ico-expand"></span>
                        {/if}
                        Total : <span class="rupees">{$rupeesymbol} </span>{$gtotal.total|string_format:"%.2f"}
                    </div>
                    {if $gtotal.total ne $gtotal.subtotal}
                        <div class="mrp hide">
                            MRP : <span class="rupees">{$rupeesymbol}</span> <span>{$gtotal.subtotal|string_format:"%.2f"}</span>
                        </div>
                        
                        <div class="discounts hide">
                            {if $gtotal.discount ne 0.00}
                                Item Discount : (-) <span class="rupees">{$rupeesymbol} </span> {$gtotal.discount|string_format:"%.2f"}<br />
                            {/if}
                            {if $gtotal.cart_discount ne 0.00}
                                Bag Discount : (-) <span class="rupees">{$rupeesymbol} </span> {$gtotal.cart_discount|string_format:"%.2f"}<br />
                            {/if}
                            {if $gtotal.coupon_discount ne 0.00}
                                Coupon : (-) <span class="rupees">{$rupeesymbol} </span> {$gtotal.coupon_discount|string_format:"%.2f"}<br />
                            {/if}
                            {if $gtotal.cash_redeemed ne 0.00}
                                Cashback : (-) <span class="rupees">{$rupeesymbol} </span> {$gtotal.cash_redeemed|string_format:"%.2f"}<br />
                            {/if}
                            {if $gtotal.pg_discount ne 0.00}
                                Gateway : (-) <span class="rupees">{$rupeesymbol} </span> {$gtotal.pg_discount|string_format:"%.2f"}   <br />
                            {/if}
                            {if $gtotal.shipping_cost ne 0.00}
                                <span class="shipping-cost">Shipping : <span class="rupees">{$rupeesymbol} </span> {$gtotal.shipping_cost|string_format:"%.2f"}</span> <br />
                            {/if}
                            {if $gtotal.gift_charges ne 0.00}
                                <span class="shipping-cost">Gift Charge : <span class="rupees">{$rupeesymbol} </span> {$gtotal.gift_charges|string_format:"%.2f"}</span> <br >
                            {/if}
                            {if $gtotal.emi_charge gt 0.00}
                                <span class="shipping-cost">EMI Charge : <span class="rupees">{$rupeesymbol} </span> {$gtotal.emi_charge|string_format:"%.2f"}</span>                      
                            {/if}
                        </div>
                    {/if}
                </div>
            </div>
    </div>
</div>
</div>
{/foreach}
{/if}
{/if}
{/block}


{block name=pagejs}
<script type="text/json" id="order-items-json">
    {$orderItemsJson}
</script>
<script>
   {if !$showPage}
      var showLogin = true;
   {/if}
</script>

{/block}

