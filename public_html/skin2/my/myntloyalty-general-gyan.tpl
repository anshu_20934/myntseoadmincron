<div class="mk-loyalty-block top-banner loyalty-top-banner">
   <img class="loyalty-top-banner-img" src="{$banner_img_url}">
   <p class="mk-loyalty-gyan-general gray">
        {$loyalty_description}
   </p>
    <div class="loyalty-top-banner-gyan {if $isActivatedUser}accepted-member{/if}">
        <h2>My Privilege</h2>
        <div class="sub-gyan">Shop and earn</div>
        {if !$isActivatedUser}<a href="#myloyalty-accept-terms" class="btn normal-btn start-now-button">Start Now</a>{/if}
    </div>
</div>


<div class="mk-loyalty-block mk-loyalty-gyan-flow" id="myprivilege-how-it-works">
    <h2>How it works</h2>
    <div class="gray mk-loyalty-block-msg">Get started with 3 simple steps</div>
    <div class="clearfix mk-loyalty-gyan-flow-blocks">
        <div class="mk-loyalty-gyan-flow-block">
            <i class="loyalty-icon shop-myntra-bag"></i>
            <div class="gyan-flow-block-title">{$how_it_works_block1.heading}</div>
            <div class="gyan-flow-block-desc">{$how_it_works_block1.description}</div>
        </div>
        <div class="mk-loyalty-gyan-flow-block mk-loyalty-arrow">
            <i class="icon-right-arrow"></i>
        </div>
        <div class="mk-loyalty-gyan-flow-block">
            <i class="loyalty-icon earn-points-icon"></i>
            <div class="gyan-flow-block-title">{$how_it_works_block2.heading}</div>
            <div class="gyan-flow-block-desc">{$how_it_works_block2.description}</div>
        </div>
        <div class="mk-loyalty-gyan-flow-block mk-loyalty-arrow">
            <i class="icon-right-arrow"></i>
        </div>
        <div class="mk-loyalty-gyan-flow-block">
            <i class="loyalty-icon shop-myntra-bags"></i>
            <div class="gyan-flow-block-title">{$how_it_works_block3.heading}</div>
            <div class="gyan-flow-block-desc">{$how_it_works_block3.description}</div>
        </div>
    </div>
</div>


<div class="mk-loyalty-block mk-loyalty-gyan-earning" id="myprivilege-earn-more-points">
    <h2>How to earn points</h2>
    <table class="loyalty-earning-table">
        <thead>
            <tr>
                <th>What should I do ?</th>
                <th class="align-right">Benefits</th>
            </tr>
        </thead>
        <tbody>
            <!-- 
            <tr>
                <td><i class="loyalty-icon fb-icon"></i><span>Share</span></td>
                <td class="align-right"><span>Earn 10 points</span> <span class="gray">when you share your purchase</span></td>
            </tr>
            <tr>
                <td><i class="loyalty-icon like-icon"></i><span>Like</span></td>
                <td class="align-right"><span>Earn 10 points <span class="gray">when you share your purchase</span></td>
            </tr>
            -->
            <tr>
                <td><i class="loyalty-icon bag-icon-small"></i><span>Shop</span></td>
                <td class="align-right"><span>Earn points as per your membership level</span></td>
            </tr>
        </tbody>
    </table>
    <div class="mk-loyalty-gyan-levels clearfix">
        {foreach from=$tierSlabsInfo item=tsinfo}
            <div class="loyalty-gyan-level-block {if $tsinfo.isCurrentTier}selected-block{/if}">
                <i class="loyalty-icon level-{$tsinfo.tierNumber}-big-icon-{if $tsinfo.tierNumber <= $currentTierInfo.tierNumber}filled{else}filled{/if}"></i>
                <div class="{if $tsinfo.tierNumber <= $currentTierInfo.tierNumber}loyalty-current-level{/if} loyalty-gyan-level-block-benefits">
                    {if $tsinfo.isCurrentTier}
                        <p class="loyalty-gyan-current-tier">Current Membership</p>
                    {elseif !$isActivatedUser && $tsinfo.tierNumber eq 1}
                        <p class="loyalty-gyan-shopmore">Join my privilege by<br>accepting T&amp;C</p>
                    {elseif $tsinfo.tierNumber > $currentTierInfo.tierNumber}
                        <p class="loyalty-gyan-shopmore">Shop for Rs. {$tsinfo.minPurchaseValue|number_format:0:".":","}<br/>more to upgrade</p> 
                    {/if}    
                    <p><strong>{$tsinfo.awardForHundred} points</strong> for every Rs. 100<br/>worth of purchase</p>
                    {foreach from=$tsinfo.tierMessages key=k item=v}
                        <p class="{if $k == $tsinfo.tierMessagesCount-1}plast{/if}"><strong>{$v}</strong></p>
                    {/foreach}
                </div>
            </div>
        {/foreach}
    </div>
</div>

<div class="mk-loyalty-block mk-loyalty-gyan-earning" id="myprivilege-rules">
    <h2>Rules, Terms & Conditions</h2>
    <div class="loyalty-rules-terms-condition clearfix">
        <div class="recent-activity-mask" style="height:300px">
            {include file="my/myntloyalty-tnc.tpl"}
        </div>
        <a class="read-more read-more-tnc">Read more...</a>
    </div>
</div>
{if !$isActivatedUser}
<div class="mk-loyalty-block">
    <button class="btn primary-btn start-now-button start-now-bottom">Start Now</button>
</div>    
{/if}