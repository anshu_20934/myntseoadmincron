	{* Gift Card Template Preview *}
	<div id="{if $previewtype eq "mymyntraorders"}order-{$gco->orderId}{else}{if $gc->id}mk-giftcardpreview-overlay-{$gc->id}{else}mk-giftcard-preview{/if}{/if}" class="lightbox mk-giftcardpreview-overlay {if $previewtype eq "mymyntraorders"}orderbox{/if}">
		<div class="mod">
			<div class="hd">
			{if $previewtype eq "mymyntraorders"}
				<h2 class="title">Order No. {$gco->orderId}</h2>
		        <span class="date">placed on {$gc->deliveredDate|date_format:"%d %b, %Y"}</span>
		        <span class="count gray">Gift Card</span>
		        <span class="total">
		            Total <span class="rupees">{$rupeesymbol} {$gc->amount|string_format:"%.2f"}</span>
		        </span>
			{else}
				<div class="mk-gp-title">GIFT CARD PREVIEW</div>
				<div class="mk-gp-subtitle"><span>DELIVERY MODE: </span>SEND VIA EMAIL</div>
			{/if}	
			</div>
		    <div class="bd clearfix">
		    	{if $previewtype neq "mymyntraorders" && $previewtype neq "senderpreview"}	
		    	<div class="mk-gp-helptext">This is what your Gift Card email will look like in your recipient's inbox.</div>
		    	{/if}
		    	<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="text-transform:none;">
					<tbody>
						<tr>
							<td style="font-family:arial,helvetica,sans-serif" align="center" valign="top">
								<table align="center" border="0" cellpadding="0" cellspacing="0" {if $previewtype neq "mymyntraorders"}width="630"{else}width="100%"{/if}>
									<tbody>
									<tr>
										<td align="center" background="{if $smarty.server.HTTPS}{$secure_cdn_base}{else}{$cdn_base}{/if}/skin2/images/border-shadow.jpg" valign="top">
											<table border="0" cellpadding="0" cellspacing="0" {if $previewtype neq "mymyntraorders"}width="600"{else}width="100%"{/if}>
												<tbody>
												<tr>
													<td valign="top">
														<div {if $previewtype neq "mymyntraorders"}style="width:600px"{/if}>
														{if $previewtype neq "mymyntraorders"}
														<div style="padding:0px;min-height:100px;border-bottom: 2px solid #D5D5D5;">
															<div style="width:580px;padding-top:0px;margin-left:20px;" align="center">
																<a href="http://www.myntra.com/" rel="nofollow" target="_blank"><img src="{if $smarty.server.HTTPS}{$secure_cdn_base}{else}{$cdn_base}{/if}/skin2/images/myntra-logo.jpg" alt="MYNTRA.COM" title="MYNTRA.COM" style="border:0px none #ffffff;color:#f00c6b;font-family:arial,helvetica,serif;font-size:26px" height="77" width="220"></a>
															</div>
															<div style="color:#838383;min-height:16px;width:600px;font-family:Arial,serif;font-size:11px;margin-top:5px" align="center">
																 100% ORIGINAL PRODUCTS &nbsp;&nbsp;&nbsp;&nbsp; FREE SHIPPING &nbsp;&nbsp;&nbsp;&nbsp; 24 HOUR DISPATCH &nbsp;&nbsp;&nbsp;&nbsp; CASH ON DELIVERY &nbsp;&nbsp;&nbsp;&nbsp; 30-DAY RETURNS  
															</div>
														</div>
														{/if}
														<div {if $previewtype neq "mymyntraorders"}style="width:600px;"{/if} align="center">
															{if $previewtype neq "mymyntraorders"}
																<p style="text-transform:none;margin-top:5px;">Hello <strong><span class="mk-gc-preview-to-name">{$gc->recipientName}</span></strong>,</p>
																<p style="text-transform:none;margin-top:5px;">You have received a <span style="font-weight:bold;color:#D14747;">Rs. </span><span class="mk-gc-preview-amount" style="font-weight:bold;color:#D14747;">{$gc->amount}</span> <strong>Myntra.com Gift Card</strong> from <strong><span class="mk-gc-preview-from-name">{$gc->senderName}</span></strong>!</p>
															{else}
																<div class="subhd" style="text-transform:uppercase;margin-bottom:10px;padding-bottom:5px">
																	<span style="display:inline-block;margin-bottom:2px;color:#808080;font-family:'din-med';font-size:12px;padding:0;margin-right:3px;">Recipient: </span><span style="color:#333;padding:0;display:inline-block;margin-bottom:2px;font-family:'din-med';font-size:12px;">{$gc->recipientEmail}</span><br/>
																	<span style="display:inline-block;margin-bottom:2px;color:#808080;font-family:'din-med';font-size:12px;padding:0;margin-right:3px;">Value: </span><span style="color:#333;padding:0;display:inline-block;margin-bottom:2px;font-family:'din-med';font-size:12px;">{$rupeesymbol} {$gc->amount}</span><br/>
																	<span style="display:inline-block;margin-bottom:2px;color:#808080;font-family:'din-med';font-size:12px;padding:0;margin-right:3px;">Delivery mode: </span><span style="color:#333;padding:0;display:inline-block;margin-bottom:2px;font-family:'din-med';font-size:12px;">Send via email</span><br/>
																	{if $gc->status eq 'Activated'}
																		<span style="color:#808080;font-family:'din-med';font-size:12px;padding:0;margin-right:3px;">Activated on: </span><span style="color:#333;padding:0;display:inline-block;margin-bottom:2px;font-family:'din-med';font-size:12px;">{$gc->deliveredDate|date_format:"%d %b, %Y"}</span><br/>
																	{else}
																		<span style="color:#808080;font-family:'din-med';font-size:12px;padding:0;margin-right:3px;">Not activated yet </span><br/>	
																	{/if}
																</div>
															{/if}
															<div class="mk-giftcards-preview" style="position:relative;text-align:center;margin-top:5px;width:600px">
																<div class="mk-giftcards-preview-section">
																	<img class="mk-gc-preview-images" src="{if $smarty.server.HTTPS}{$secure_cdn_base}{else}{$cdn_base}{/if}/{$gc_occasion->previewImage}"/>
																	<div class="mk-gc-onpage-preview" style="color:#FFFFFF;font-family:georgia,helvetica,sans-serif;height:227px;position:absolute;right:47px;text-transform:none;top:62px;width:216px;">
																		<div class="mk-gc-preview-to" style="font-size:16px;margin-top:25px;">Dear <span class="mk-gc-preview-to-name">{$gc->recipientName}</span>,</div>
																		<div class="mk-gc-preview-message" style="font-size:16px;margin-top: 25px;">{$gc_message->message}</div>
																		<div class="mk-gc-preview-from" style="bottom:30px;font-size:16px;position:absolute;text-align:center;width:100%;">From <br/><span class="mk-gc-preview-from-name">{$gc->senderName}</span></div>
																	</div>
																</div>
															</div>
															{if $previewtype neq "mymyntraorders"}
															<p style='text-tranform:none;font-size:20px;font-weight:normal;color:#808080;margin-top:5px;margin-bottom:5px;font-size:14px'>Go ahead and shop for</p>
															<p style='text-tranform:none;font-size:24px;background:#D14747;padding:5px;color:#FFF;width:140px;margin-bottom:5px;margin-top:5px;'>Rs. <span class="mk-gc-preview-amount" style="color:#FFF;">{$gc->amount}</span></p>
															<p style='text-tranform:none;font-size:14px;font-weight:normal;margin-top:5px;'><span style='color:#808080;font-size:14px;'><span style="color:#000;">GIFT CODE</span>: XXXX</p>
															<p style="text-transform:none;margin-top:5px;padding-top:10px;border-top:1px solid #d5d5d5;font-weight:normal;width:580px;">Once you add this Gift Card to your Myntra account, the gift amount will be credited to your Mynt Credits as
Cashback and will be available for any subsequent purchase you make on Myntra.com.</p>
															<span style='text-transform:none;display:block;margin-top:15px;margin-bottom:10px;vertical-align: middle; cursor: pointer;border: none; margin-left: auto; margin-right: auto; width:221px;height:41px;position:relative;' target='_blank'><img src='{if $smarty.server.HTTPS}{$secure_cdn_base}{else}{$cdn_base}{/if}/skin2/images/AddGiftCardButton.png'/></span>
															<p style="text-transform:none;margin-top:5px;padding-top:10px;font-weight:normal;width:580px;">Do note that you can always add this Gift Card at a later date by copy-pasting the Gift Code in the Gift
Cards section of your <a href="{$http_location}/mymyntra.php?view=mygiftcards" target="_blank">My Myntra page</a>.</p>
															<p style="text-transform:none;margin-top:15px;width:580px;">For more information, please refer to the <a href="{$http_location}/faqs">FAQs</a>, or <a href="{$http_location}/contactus">contact us</a>. Happy shopping!</p>
															{/if}		
														</div>
														</div>
													</td>
												</tr>
												</tbody>
											</table>
										</td>
									</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
		    </div>
		  	<div class="ft">
		  		<p>Need Help? Call Us at <span>{$customerSupportCall}</span></p>
		  	</div>
		</div>
	</div>
	{* Gift Card Template Preview - End *}