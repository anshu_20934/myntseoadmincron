{if $hasAddress}

<div class="default-address">
	<div id="address-change">Change Shipping address</div>
	<address>
        <strong>{$address.name}</strong><br>
          {$address.address|nl2br}</br>
          {if $address.locality}{$address.locality}<br>{/if}
          {$address.city} - {$address.pincode}, {$address.statename}<br>
          <span class="lbl">Mobile:</span> {$address.mobile}
    </address>
</div>
<div class="address-list-wrap">
</div>
{else}
<div class="address-form-wrap">
</div>
{/if}