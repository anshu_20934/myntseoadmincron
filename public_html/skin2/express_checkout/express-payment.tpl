{extends file="layout-checkout.tpl"}
{block name=body}
<script>
Myntra.Data.pageName="expresss_payment";
Myntra.Data.countryList = {if $countriesJson}{$countriesJson}{else}''{/if};
Myntra.Data.OrderType="express_order";
Myntra.Payments = Myntra.Payments || {};
Myntra.Payments.showPayHelpline = '{$payHelpline}' == 'test' ? true : false;
Myntra.Payments.showPayFailOpt= '{$payFailOpt}' == 'test' ? true : false;
Myntra.Payments.payUserCloseOpt= '{$payUserCloseOpt}' == 'test' ? true : false;
Myntra.Payments.showPayZipOrders= '{$payUserCloseOpt}' == 'test' ? true : false;
Myntra.Payments.codErrorCode= '{$codErrorCode}' == '0' ? true : false;
Myntra.Payments.isGiftCardOrder = false;

var isGiftCardOrder = 0;
var ispayUserCloseOpt = '{$payUserCloseOpt}';
var hasAddress = '{$hasAddress}';
{if $pStyleID}var  pStyleID = {$pStyleID} {/if};
{if $pSKUID}var  pSKUID = {$pSKUID} {/if};
</script>
<div class="checkout payment-page exp-payment-page mk-cf">
    {* mainContent *}
    <div class="main-content">
    	{if $payFailOpt == 'control'}
		    {include file="checkout/paymentError.tpl"}
		{/if}

        {if $citi_discount > 0}
            <div id="citi_discount_div"> Avail {$citi_discount}% discount by paying with your Citibank <a href="#pay_by_dc" onClick="$('#pay_by_dc').click();return false;">Debit Card</a></div>
        {/if}

        {if $icici_discount > 0}
            <div id="icici_discount_div"> Avail {$icici_discount}% discount on payment through <img alt="ICICI" src="{$secure_cdn_base}/skin1/images/ICICI.png"> (credit card, debit card or netbanking)</div>
        {/if}

        {* pay button is used in top and bottom. so, capture is used here *}
        {capture name="pay_button"}
        <div class="pay-btn-wrap">
            <a href="#paybtn" {if $totalAmount eq 0} onclick="return submitPaymentForm(false);" {elseif $is_icici_gateway_up} onclick="return submitPaymentForm(true);" {else} onclick="return submitPaymentForm(false);" {/if} class="btn primary-btn pay-btn">
                 {if $totalAmount eq 0}
                    Confirm Order
                 {elseif $is_icici_gateway_up}
                    {if $paymentType == "cod" }
                        Confirm Order
                    {else}
                        Pay Now
                    {/if}
                 {else}
                    Proceed to Payment
                 {/if}
            </a>
        </div>
        {/capture}
      
		<div class="address-wrap mk-cf">
        	<h3 class="title">1. SHIPPING ADDRESS</h3>
            {include file="express_checkout/exp-address.tpl"}
        </div>
        <div class="coupon-wrap mk-cf">
        	<h3 class="title">2. CASHBACK AND COUPONS</h3>
            {include file="cart/coupon_widget.tpl"}
        </div>
        
        {*PyamentBlock*}
        <div class="mk-cf exp-payment-cont">
        	<h3 class="title">3. PAYMENT</h3>
            {$paymentPageUI}
		</div>
    </div>
    {*mainContent@End*}
	
    {include file="checkout/summary.tpl" flow="payment_page"}


</div>
{/block}

{block name=lightboxes}
<div id="coupon-info" class="lightbox infobox">
	<div class="mod">
		<div class="hd">
			<div class="title">How do I Apply a Coupon?</div>
		</div>
		<div class="bd body-text">
			<p></p>
			<ol>{if $condNoDiscountCouponFG eq 1 || $styleExclusionFG eq 1}
					{assign var="message" value=$couponExcludedProductsMsg}
					<li>{$message}</li>
					<li>Most coupons can be applied only when the value of the products in your bag is above a minimum value. The minimum value is calculated after applying all discounts.</li>
				{else}
					<li>Most coupons can be applied only when the value of the products in your bag is above a minimum value. The minimum value is calculated after applying all discounts.</li>
				{/if}
			</ol>
		</div>
		<div class="ft">
			<p></p>
		</div>
	</div>
</div>
{* Do not change id of any div below it is used in orderProcessing.tpl *}
<div id="splash-payments-processing" class="lightbox">
	<div class="mod" id="processing-payment">
	    <div class="hd" id="splash-payments-processing-hd">
	        <h2>Processing Payment</h2>
	    </div>
	    <div class="bd clearfix" id="splash-payments-processing-bd">
	    	<center><img src="{$secure_cdn_base}/images/image_loading.gif"></center>
	        <div class="processing-payments" id="splash-payments-processing-bd-pp">
	            Enter your card/bank authentication details in the new window once it appears on your screen. <br/>
				Please do not refresh the page or click the back button of your browser.<br/>
				You will be redirected back to order confirmation page after the authentication. It might take a few seconds.<br/><br/>
	        </div>		

	        <center id="cancel-payment-btn"><button class="action-btn cancel-payment-btn" type="button">Cancel Payment</button></center>
	    </div>
	</div>
	<div class="mod mk-hide" id="result-succPayment">	 
    	<div class="hd" id="splash-payments-processing-hd-succPayment">
    		<h2>Payment Successful</h2>
        </div>
        <div class="bd clearfix" id="splash-payments-processing-bd-succPayment">
        	<center><img src="{$secure_cdn_base}/images/image_loading.gif"></center>
    		<div class="processing-payments bd" id="splash-payments-processing-bd-pp-succPayment">    
    			Please wait while redirecting       
       		</div>    
       	</div>    
    </div>
    <div class="mod mk-hide" id="result-errPayment">	 
    	<div class="hd" id="splash-payments-processing-hd-errPayment">
    		<h2>Payment Failure</h2>
        </div>
        <div class="bd clearfix" id="splash-payments-processing-bd-errPayment">
        	<center><img src="{$secure_cdn_base}/images/image_loading.gif"></center>
    		<div class="processing-payments bd" id="splash-payments-processing-bd-pp-errPayment">    
    			Please wait while redirecting       
    		</div>
        </div>        
    </div>
    
{if $payFailOpt == 'test'}
	<div class="mod mk-hide" id="result-payFailOver-err">	 
    	<div class="hd" id="splash-payments-processing-hd-payFailOver-err">
    		<h2>Payment Failed</h2>
        </div>
        <div class="bd clearfix" id="splash-payments-processing-bd-payFailOver-err">
    		<div class="processing-payments bd" id="splash-payments-processing-bd-pp-payFailOver-err">
    		</div>    		      
        </div>        
    </div>
{/if}

{if $payUserCloseOpt == 'test'}
	<div class="mod mk-hide" id="result-payUserClose">	 
    	<div class="hd" id="splash-payments-processing-hd-payUserClose">
    		<h2>Payment Cancelled</h2>
        </div>
        <div class="bd clearfix" id="splash-payments-processing-bd-payUserClose">
    		<div class="processing-payments bd" id="splash-payments-processing-bd-pp-payUserClose"><center>	 
    			You have cancelled your payment. You can try to pay again {if $codErrorCode eq 0}or alternatively pay by COD{/if}
</center>
	    		<div class="pay-btn-wrap inline-btns">
				{if $codErrorCode eq 0}
				   	<a class="btn primary-btn codbtn">
		    		Pay by COD
		     		</a>
		     	{/if}
		     	<a class="btn primary-btn tryagainbtn">
		     		Try Again
		    	 </a>			    
		 		</div>
		 		<div class="close"></div>	  		
    		</div>    		      
        </div>        
    </div>
{/if}
</div>

{/block}

{block name=pagejs}
<script type="text/javascript">

	Myntra.Payments = Myntra.Payments || {};
	Myntra.Payments.amount = '{$rupeesymbol} {$totalAmount|round|number_format:0:".":","}';
	var total_amount = 	{$totalAmount};
	var shipping_countrycode = "{$address.country|escape:'html'}";
	var codErrorCode = {if $codErrorCode}{$codErrorCode}{else}""{/if};
    if(codErrorCode==2){
        _gaq.push(['_trackEvent', 'payment_page', 'cod_unserviceable_zipcode', $('#addressPincode').val()]);
    }
	{if $onlineErrorMessage } var onlinePaymentEnabled = 0; {else} var onlinePaymentEnabled = 1; {/if}
	var codTabClicked = false;
	{literal}
	var toggle_text_elements = {};
	{/literal}
	var loginid = "{$loginid}";
	var citi_discount = {$citi_discount};
	var icici_discount = {$icici_discount};
	var icici_discount_text = "Avail {$icici_discount}% discount on payment through <img alt='ICICI' src='{$secure_cdn_base}/skin1/images/ICICI.png'> (credit card, debit card or netbanking)";
	var oneClickCod = {$oneClickCod};
	{if $is_icici_gateway_up}
	var is_icici_gateway_up = true;
	{else}
	var is_icici_gateway_up = false;
	{/if}
	{if $shippingRate neq 0}
		{assign var=amountToDiscount value=$totalAmount-$shippingRate}
		var amountToDiscount = {$amountToDiscount};
	{else}
		var amountToDiscount = {$totalAmount};
	{/if}

	{if $citi_discount>0}
		var	citiCardArray = new Array();
		{foreach from=$citi_discount_bins item=array_item key=id}
			citiCardArray.push('{$array_item}');
		{/foreach}
	{/if}

	{if $icici_discount>0}
		var iciciCardArray = new Array();
		{foreach from=$icici_discount_bins item=array_item key=id}
			iciciCardArray.push('{$array_item}');
		{/foreach}
	{/if}


	{if !$paymentServiceEnabled}
		var emiEnabled = false;
		{if $EMIenabled}
			emiEnabled = true;
		
			Myntra = Myntra || {};
			Myntra.Payments = Myntra.Payments || {};
			Myntra.Payments.EMICardArray = {};
			{foreach from=$EMIBanksAvailable item=emibank key=bank}
				(function() {
					var binArray = new Array();
					{foreach from=$emibank.Bins item=array_item key=id}
						binArray.push('{$array_item}');
			{/foreach}
					Myntra.Payments.EMICardArray['{$bank}'] = binArray;
				})();
			{/foreach}
		{/if}
	{/if}

	var mobileVerificationSubmitCount = 0;
	var codCharge = {$cod_charge};
	var orderSummaryDislplayed=false;
	{if $cashcoupons.MRPAmount>0}
		var cashBackAmount = {$cashcoupons.MRPAmount};
	{else}
		var cashBackAmount = 0;
	{/if}

	var userMobileNumber = "{$userMobile}";
	var paymentChildWindow;
	var checkPaymentWindow;
	var paymentProcessingPopup;
	
	$('.your-coupons .tt-ctx').each(function(el, i) {
		var ctx = $(this), 
			tip = ctx.children('.tt-tip');
		Myntra.InitTooltip(tip, ctx,{ldelim}side:'right'{rdelim});
	});
	{*$('a.coupon-info-link').each(function(el,i){ldelim}$(this).data('lb', Myntra.LightBox('#coupon-info'));{rdelim});
	$('a.coupon-info-link').each(function(el,i){ldelim}$(this).click(function(){ldelim}$(this).data('lb').show();{rdelim}){rdelim});*}
	
	$('a.coupon-info-link').data('lb', Myntra.LightBox('#coupon-info'));
	$('a.coupon-info-link').click(function(){ldelim}$(this).data('lb').show();{rdelim});
	
</script>
{/block}
{block name=macros}
    {include file="macros/cart-macros.tpl"}
{/block}
