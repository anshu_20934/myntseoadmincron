
<span class="sort-by">Sort By:</span> <span class="sort-selected">{$sortSelectedText}</span>
<div id="vtr-search-sort-menu">
    <ul>
    {foreach $sortOptions as $opt}
        <li data-value="{$opt.val}">{$opt.txt}</li>
    {/foreach}
    </ul>
</div>

