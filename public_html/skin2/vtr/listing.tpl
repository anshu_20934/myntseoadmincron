
{if $products|@count eq 0}
    <div class="no-items">No products found</div>
{/if}
{if not $isXHR}<ol>{/if}
{foreach $products as $prod}
    <li data-styleid="{$prod.styleid}">
        <a href="{$prod.page_url}">
            <img title="{$prod.title}" src="{myntraimage key="style_81_108" src=$prod.image_url}">
            <div class="title">
                <strong class="brand">{$prod.brand}</strong> {*$prod.article_type|truncate:20*}
            </div>
            <div class="price">Rs. {$prod.price}</div>
            <div class="mrp">{if $prod.price != $prod.mrp}Rs. {$prod.mrp}{/if}</div>
        </a>
        {if not $prod.is_vtr_compatible}
            <div class="shim">This is not yet ready for the Style Studio</div>
        {else if not $prod.is_torso_compatible}
            <div class="shim">Try this on in the {$prod.other_torso_type_name}'s Style Studio</div>
        {else}
            <span class="load-look">Load Look</span>
        {/if}
    </li>
{/foreach}
{if not $isXHR}</ol>{/if}

