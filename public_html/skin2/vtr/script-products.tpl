
$(document).ready(function() {ldelim}
    var filters = {$jsonQueryFilters};
    var params = {$jsonQueryParams};
    var data = {$jsonData};
    if (Myntra.isCanvasSupported) {
        var products = new Myntra.VtrProducts('.vtr-products', filters, params, data);
        Myntra.VtrManager.register('products', products);
    }
{rdelim});

