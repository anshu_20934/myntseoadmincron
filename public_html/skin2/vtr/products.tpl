
<section id="vtr-products" class="vtr-products">
    <div class="top">
        <div class="listing-info">
            <span class="count"></span>
            <span class="sorting">{$markupSorting}</span>
        </div>
    </div>
    <div class="lpane">
        <a class="tab source{if $source eq 'search'} selected{/if}" href="{$urlSourceSearch}" data-source="search">
            <span class="ico"></span>Products &amp; filters
            <span class="reset-filters {if $source != 'search'}hide{/if}" href="?{$urlSourceSearch}">Reset</span>
        </a>
        <form class="search-form{if $source neq 'search'} hide{/if}">
            <input name="query" size="20" placeholder="Search Products/Brands">
            <button type="submit">Go</button>
            <span class="close"></span>
        </form>
        <div class="filters">
            {$markupFilters}
        </div>
        <a class="tab source{if $source eq 'recent'} selected{/if}" href="{$urlSourceRecent}" data-source="recent">
            <span class="ico"></span>Recently Viewed</a>
        <a class="tab source{if $source eq 'wish'} selected{/if}" href="{$urlSourceWish}" data-source="wish">
            <span class="ico"></span>Wishlist</a>
        <a class="tab source{if $source eq 'look'} selected{/if}" href="{$urlSourceLook}" data-source="look">
            <span class="ico"></span>My Looks</a>
    </div>
    <div class="listing {$source}" data-source="{$source}">
        {$markupListing}
        <div class="load-more">
            <button class="btn btn-normal">Show more products</button>
            <div class="spinner"></div>
        </div>
    </div>
    {*<div class="pagination">{$markupPagination}</div>*}
    <div class="loading-spinner"></div>
</section>

