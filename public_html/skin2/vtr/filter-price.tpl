
{if !$isXHR}
<div class="filter filter-{$filterName} expanded filter-price" data-name="{$filterName}">
    <div class="hd">{$filterTitle}</div>
    <div class="bd">
        <ul>
{/if}
        {foreach $filterOptions as $opt}
            <li data-value="{$opt[0]}*{$opt[1]}" class="{if $opt[3]}checked{/if}">
                <a href="{$opt[2]}">
                    <span class="rbtn"></span>
                    Rs. {$opt[0]|number_format:0:".":","} - Rs. {$opt[1]|number_format:0:".":","}
                </a>
            </li>
        {/foreach}
{if !$isXHR}
        </ul>
        <div class="slider" data-min="{$lowest}" data-max="{$highest}"></div>
        <div class="slider-value">Rs. {$lowest|number_format:0:".":","} - Rs. {$highest|number_format:0:".":","}</div>
    </div>
</div>
{/if}

