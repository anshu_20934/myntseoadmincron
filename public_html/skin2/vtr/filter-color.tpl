
<div class="filter filter-{$filterName} expanded" data-name="{$filterName}">
    <div class="hd">{$filterTitle}</div>
    <div class="bd">
        {if $hasSearchBox}
        <div class="search-box">
            <input type="text" class="txt" placeholder="Find {$filterTitle}">
            <span class="close"></span>
        </div>
        {/if}
        <ul class="items">
        {foreach $filterOptions as $opt}
            <li data-value="{$opt[0]}" class="{if $opt[3]}checked{/if}">
                <a href="{$opt[2]}">
                    <span class="cbx"></span>
                    <span class="color-swatch" 
                        style="background-position:-783px -{math equation='((x-1) * 11) + (x*2)' x=$colorSwatchMap[$opt[0]]}px"></span> 
                    {$opt[0]} <span class="count">[{$opt[1]}]</span>
                </a>
            </li>
        {/foreach}
        </ul>
    </div>
</div>

