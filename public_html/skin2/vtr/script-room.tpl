
$(document).ready(function() {ldelim}
    var params = {$jsonParams};
    var data = {$jsonData};
    if (Myntra.isCanvasSupported) {
        var room = new Myntra.VtrRoom('.vtr-room', params, data);
        Myntra.VtrManager.register('room', room);
    }
{rdelim});

