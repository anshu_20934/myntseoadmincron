
{if $products|@count eq 0}
    <div class="no-items">No looks found</div>
{/if}
{if not $isXHR}<ol>{/if}
{foreach $products as $prod}
    <li data-uuid="{$prod.uuid}">
        <a href="{$prod.url}" class="title"
            onclick="_gaq.push(['_trackEvent', 'vtr', 'saved-look-click', '{$prod.uuid}'])">{$prod.name}</a>
    </li>
{/foreach}
{if not $isXHR}</ol>{/if}

