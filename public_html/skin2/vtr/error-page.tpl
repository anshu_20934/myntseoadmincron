{extends file="layout.tpl"}

{block name=navigation}
    {if $layoutVariant eq 'test'}
		{include file="inc/navigation-default.tpl"}
	{else}
		{include file="inc/navigation-collapsed.tpl"}
	{/if}
{/block}

{block name=body}
<script>
    Myntra.Data.pageName="vtr";
</script>
<div id="vtr" class="vtr">
    <h1>Style Studio</h1>
    <div id="vtr-empty-area">
        {if $errorCode eq 'ACCESS_DENIED'}
            {if $login}
                <div class="msg">
                    <p class="err-txt">Sorry, this look is not shared by the creator.</p>
                    <p><a href="/stylestudio/">Go Back to the Style Studio</a></p>
                </div>
            {else}
                <div class="msg">
                    <p>Please 
                        <a href="#login" onclick="$(document).trigger('myntra.login.show'); return false;">Login</a>
                        to access this Look.</p>
                </div>
            {/if}
        {else}
            <div class="msg">
                <p>oops! something went wrong!</p>
            </div>
        {/if}
    </div>
    <div style="clear:both"></div>
</div>
{/block}

