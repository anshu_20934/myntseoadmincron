
<span class="vtr-torso-types">
    {foreach $torsoTypes as $id => $name}
        <a class="{if $id eq $torsoType}selected{/if}" href="/stylestudio/?tt={$id}" 
            onclick="_gaq.push(['trackEvent', 'vtr', 'changetorsotype', '{$id}'])">{$name}</a>
    {/foreach}
</span>
<section id="vtr-room" class="vtr-room">
    <div class="nav">
        <ul class="actions">
            {*<li data-action="undo">Undo</li>*}
            <li data-action="new">New</li>
            {*<li data-action="reset">Reset</li>*}
            <li data-action="saveas" class="save">Save</li>
            {*<li data-action="saveas" class="saveas">Save As</li>*}
            <li data-action="customize" class="customize">Models &amp; Backgrounds</li>
        </ul>
    </div>
    <div class="stage">
        <div class="bg"></div>
        <div class="model"></div>
        <div class="tried-items invisible">
            <div class="hd">
                <span class="icon ico-expand"></span>
                <span class="count">0 Items</span>
            </div>
            <ul class="bd"></ul>
            <div class="ft"></div>
        </div>
        <div class="buy-btns">
            <button class="btn primary-btn buy">Buy The Look</button>
            {*<button class="link-btn wish">Save in Wishlist</button>*}
        </div>
        <div class="share-btns">
            <div class="label">Share</div>
            <button class="share-btn facebook" title="Share this look in Facebook">Facebook</button>
            <button class="share-btn twitter" title="Tweet this look">Twitter</button>
        </div>
    </div>
    <div class="look-title"><span class="label">Look / </span><span class="name"></span></div>
    <div class="views"></div>
    <div class="vtr-pane vtr-cpane">
        <div class="faces">
            <h3>Model Appearance</h3>
            <div class="items"></div>
        </div>
        <div class="backgrounds">
            <h3>Backgrounds</h3>
            <div class="items"></div>
        </div>
        <span class="close"></span>
    </div>
    <div class="vtr-pane vtr-spane">
        <input type="text" name="lookname" size="25" maxlength="40" placeholder="Enter a name for the Look">
        <div class="error-msg">Please enter a name</div>
        <button class="btn normal-btn save">Save</button>
        <div class="success-msg"><span class="icon"></span> Look Saved</div>
        <div class="loading-spinner"></div>
        <span class="close"></span>
    </div>
    <div class="loading-spinner"></div>
</section>

