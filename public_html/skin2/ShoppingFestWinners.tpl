{extends file="layout.tpl"}
{block name=pagecss}
    <link href="skin2/css/my.css" rel="stylesheet" type="text/css">

{/block}

{block name=body}
<div class="mk-account-pages">
{section name=prod_num loop=$resultsBoards}
    <table>
        <col width="150" />
        <col width="450" />
        <col width="250" />
             <caption>{$resultsBoards[prod_num].title}</caption>
            <tr class="mk-table-head">
            {section name=head_num loop=$resultsBoards[prod_num].headers}
                <th>{$resultsBoards[prod_num].headers[head_num]}</th>
            {/section}
        </tr>
        {section name=data_num loop=$resultsBoards[prod_num].data}
        <tr class="{cycle name='used_cycle' values='mk-odd,mk-even'}">
            {section name=cell_num loop=$resultsBoards[prod_num].data[data_num]}
            <td>{$resultsBoards[prod_num].data[data_num][cell_num]}</td>
            {/section}
        </tr>
        {/section}
    </table>
{/section}
</div>
{/block}
