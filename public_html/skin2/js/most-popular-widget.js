$(function() {
	if (!Myntra.Data.enableMostPopular) return;
	
	$(window).load(function () {
		$.ajax({
			type:"GET",
			url:http_loc+'/widgetcontroller.php?widget_name=MostPopular',
			dataType:"json",
			success: function(data){
				if(data.status==='SUCCESS'){
					var index=0;
					
					$.each(data.tabinfo, function(tabname, category ) {
						//var link = $("<a href=''>" + tabname+ "</a>");
						//$(mostPopularHeaderLeft).append(link).append("/");
						$('.dummyWidget').clone(true).insertBefore('[data-rel-content="recent"]');
						var tempObj = $('.mk-tabContentWrap').find('.dummyWidget:last');	
						tempObj.removeClass('dummyWidget');
						$.each(category, function(categoryname, categorycontent) {							
							tempObj.find('.category').append($("<a data-tab-name='"+tabname.replace(/ /g,'-')+"' class='mk-hide' href='"+categorycontent.link_url+"'>"+categoryname+" / <span>View More</span></a>"));
							tempObj.find('ul.rs-carousel-runner').append(categorycontent.div_content);
							index++
						});
					});					
					
					$('.mk-most-popular').not('.dummyWidget').each(function(){
						var currObj = $(this);
						$(this).fadeIn(200, function() {
							if(currObj.find('.rs-carousel-runner > li').size()>5){
								currObj.carousel({
						            mask: '.rs-carousel-mask',
									itemsPerPage: 5, // number of items to show on each page
									itemsPerTransition: 5, // number of items moved with each transition
									nextPrevActions: true, // whether next and prev links will be included
									pagination: true, // whether pagination links will be included
									autoScroll: true,
									pause: 9000,
									loop: true,
									insertPagination:function(pagination){
										$(pagination).appendTo(currObj.find('.top-seller-pagination'));
										return $(pagination);
									},
									before:function(event,data) {
										currObj.find('.category a').hide();
									},
									after: function(event,data) {
										var linkIndex = currObj.find(".rs-carousel-pagination-link-active").index();
										currObj.find('.category a:eq(' + linkIndex + ')').fadeIn();
										var tabName = currObj.find('.category a:eq(' + linkIndex + ')').data('tab-name');
										currObj.find('.mk-most-popular .header a[data-tab-name='+tabName+']').addClass('selected');
									},
									create:function(event,data){
										currObj.find('.category a').first().css('display','block');
										currObj.find('.header a').first().addClass('selected');
									}
								});
							}							
							currObj.find('.mk-most-popular-ul').css('left','0px');
						});
					});
					if(Myntra.Data.newTabWidget){
		            	Myntra.Data.newTabWidget['tabinfo'] = data.tabinfo;	
		            	Myntra.Data.newTabWidget['abValue'] = data.popular_ab_value;
		            }				
	            }	            
			},	            
	        complete: function(){
	        	Myntra.newTabWidget.init();
	        }
		});
	});
});
