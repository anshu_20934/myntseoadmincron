function resize()
{
    var banner_default_height = 444;
	var banner_default_width = 1024;
	var height_of_mandatory_top_elements = 308;
	var aspect_ratio = banner_default_width/banner_default_height;
	var width = $(window).innerWidth() >= banner_default_width ? $(window).innerWidth() : banner_default_width;
	var height = width / aspect_ratio;//$(window).innerHeight()-100 >= banner_default_height ? $(window).innerHeight()-100 : banner_default_height;
	var top_bottom_elements = $(window).innerHeight()-100 >= height_of_mandatory_top_elements ? $(window).innerHeight()-100 : height_of_mandatory_top_elements;
	top_bottom_elements = top_bottom_elements > height ? height: top_bottom_elements;
   
	/* change the height of the banner image */
    $(".mk-wrapper-outer .mk-marquee").css("height", top_bottom_elements);
    $(".mk-wrapper-banner-inner").css("height", top_bottom_elements);
    $(".mk-wrapper-outer .mk-marquee a .home_page_ss_parent img").css("height", height);
	$(".mk-wrapper-outer .mk-marquee a .home_page_ss_parent img").css("width", width);

	/* setting the width of all the above elements */
	$(".mk-wrapper-outer .mk-marquee").css("width", width);

	/* setting the lefts of opaque divs */
	var l = $(window).width() >= 980 ? ($(window).width()-980)/2 : 0;
	$(".mk-header-opaque").css("left", l);	
	$(".mk-header-extended-opaque").css("left", l+192);

	/* cycle navigation dots position */
	$('.mk-cycle-nav').css("right", l + 28);

	/* banner text change */
	var minh = height_of_mandatory_top_elements + 20;
	var totalH = minh + 20 + 120;
	console.log(minh);
	console.log(totalH);
	console.log((height - totalH)/2);
	var t = height < totalH ? minh : minh + ((height - totalH)/2);
	
    $('.mk-wrapper-outer .mk-marquee a .home_page_ss_child').css("top", t);
    $('.mk-wrapper-outer .mk-marquee a .home_page_ss_child').css("left",l);
}

$(window).load(function(){
	resize();
});

$(window).resize(function() {
	resize();
});

