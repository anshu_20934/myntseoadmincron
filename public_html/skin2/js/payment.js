$(function() {
    Myntra.InitTooltip('.tt-cc-cvv', '.cc-cvv');
    Myntra.InitTooltip('.tt-dc-cvv', '.dc-cvv');
    
    if($('#smsVerification').length){
    	var lb_smsVerifiy = Myntra.LightBox('#smsVerification');
        $('#showSMSVerification').click( function() {
        	lb_smsVerifiy.show();
        });
    }
    
    // Coupon Code Placeholder
    $('.coupons-widget .code-enter .placeholder').click( function() {
    	$(this).hide();
    	$('#othercouponcode').focus();
    });
    
    $('#othercouponcode').blur( function() {
    	var placeholder = $(this).siblings('.placeholder');
    	if ($(this).val() == '') {
    		placeholder.show();
    	} else {
    		placeholder.hide();
    	}
    });
    

    var $items = $('.pay-block > .tabs > ul > li');

    var pushToGA = function (tabClicked){
                // Seperated GA calls from normal logic
                var tabClickedId = $(tabClicked).attr('id');
				_gaq.push(['_trackEvent', 'payment_page', 'switch_payment_mode', tabClickedId]);
    }

    var initPaymentOptionTabClick = function (tabObj){
	        $items.removeClass('selected');
	        $(tabObj).addClass('selected');
	        var index = $items.index($(tabObj)),
	            form = $('.pay-block > .tab-content > form').hide().eq(index).show(),
                formtoSubmit = form.attr("id");
			$("#paymentFormToSubmit").val(formtoSubmit);			

			$(".pay-btn").show();

            // TODO: Will be enabled later once we find the keyboard support for jqTransSelect
            //$('.cc-expiry', form).not('.jqtransformdone').jqTransform();
            //$('.cc-bill-country', form).not('.jqtransformdone').jqTransform();
            //$('.net-banks-list', form).not('.jqtransformdone').jqTransform();

			if(is_icici_gateway_up) {
				var buttonText = "Pay Now";
			} else {
				var buttonText = "Proceed to Payment";
			}

			if(citi_discount>0) {
				$("#citi_discount_div").html('Avail ' + citi_discount +'% discount by paying with your Citibank <a href="#pay_by_dc" onClick="$(\'#pay_by_dc\').click();return false;">Debit Card</a>');
			}

			if(icici_discount>0) {
				$("#icici_discount_div").html(icici_discount_text);
			}

            $(".order-summary .cod-charges").hide();
            $(".order-summary .additional-discount").hide();
            $(".order-summary .grand-total > .value").text("Rs. " + formatNumber(total_amount));
            if (telesales)
                $(".pay-btn").hide();

            if(onlinePaymentEnabled == 1) {
	            if(formtoSubmit == 'credit_card') {
					if(is_icici_gateway_up)	showUserCardType(document.getElementById('credit_card').card_number.value);
				} else if (formtoSubmit == 'net_banking') {
					// Pay with Net Banking -or- Pay with Cash Card option is selected
					buttonText = "Proceed to Payment";
					netBankingOptionChange();
				} else if (formtoSubmit == 'debit_card') {
					if(is_icici_gateway_up) showUserCardType(document.getElementById('debit_card').card_number.value);
				} else if(formtoSubmit == 'phone_payment') {
					$(".pay-btn").hide();
				} else if (formtoSubmit == 'emi') {
					if(!emiEnabled) {
                        $(".pay-btn").hide(); 
                    }else{
					    showUserCardType(document.getElementById('emi').card_number.value);
                    }
				} else if(formtoSubmit == 'cod') {
					if(codCharge>0) {
						$(".order-summary .cod-charges").show().find(" > .value").text("Rs. " + formatNumber(codCharge));
						var finalAmount = total_amount + codCharge;
						finalAmount = finalAmount<0?0:finalAmount;
						updateTotal(finalAmount);
					}
					$(".pay-btn").hide();
					codTabClicked = true;
				}
	            
            } else {
            	if(formtoSubmit == 'cod') {
					if(codCharge>0) {
						$(".order-summary .cod-charges").show().find(" > .value").text("Rs. " + formatNumber(codCharge));
						var finalAmount = total_amount + codCharge;
						finalAmount = finalAmount<0?0:finalAmount;
						updateTotal(finalAmount);
					}
					// Cash on delivery option is selected
					buttonText = "Place the Order";
					$(".pay-btn").hide();
					if(codErrorCode>0) {
						if(!codTabClicked){
							 _gaq.push(['_trackEvent', 'payment_page', 'cod_error_message', $('#codErrorCode').val()]);
							 if(codErrorCode==2){
								_gaq.push(['_trackEvent', 'payment_page', 'cod_unserviceable_zipcode', $('#addressPincode').val()]);
							 }
						}
					}
					codTabClicked = true;
            	}
            	$(".pay-btn").hide();
 			} 
            if (formtoSubmit != 'emi') {
				$('.order-summary .you-pay').html(Myntra.Payments.amount);
				$('.order-summary .emi-charges').hide();
			} else {
				var bankName = $('#emi-bank').val(),
					selectedRow = $('form#emi .emi-duration#' + bankName + ' .selected');
				if (selectedRow.length) {
					 selectedRow.click();
				} else {
					$('form#emi .emi-duration#' + bankName + ' tr').first().click();
			}
			}
			$(".pay-btn").html(buttonText);
    }

    if(total_amount==0){
        $items.click(function() {
            return false;
        });
    } else {
	    $items.click(function() {
            initPaymentOptionTabClick(this);
            pushToGA(this);
	    });

        var firstTab = $items.filter('[class*=selected]').first(); //.click();
        initPaymentOptionTabClick(firstTab);

    	if(oneClickCod)  {
    		$('#pay_by_cod').click();
    	}
    }

/*    // Close icon for Coupon SMS Verification
    $('#smsVerification .close').click( function(e) {
    	$('#smsVerification').hide();
    });
*/
});

$(document).ready(function(){
	var tmrOut;
    // Check if time limit for notification is set for the user on payment page
    if(typeof Myntra.Payments.paymentPageUserTimeLimit != "undefined"){
    	tmrOut =setTimeout(function(){
        var url = https_loc + "/notification";
        $.ajax({
          type: "POST",
          url: url,
          data:"notification_page="+Myntra.Data.pageName+"&notification_type=timeLimit&notification_message=User waiting on this page for more then "+Myntra.Payments.paymentPageUserTimeLimit+" min",
          dataType:'text'
        });
      }, Myntra.Payments.paymentPageUserTimeLimit*1000*60);
    }

	/* Toggle Text */
	toggle_text_all(".toggle-text");

    /*var obj = Myntra.LightBox('#splash-mobile-verify');*/
       /*$('.mynt-verify-btn').click(function(){
    	   var mobinum = $('input.mobile-number').val();
    	   $('#mynt-verify-btn').addClass("disable-btn");
    	   $('#edit-btn-mobile-verify').attr("disabled", "true");
    	   $.ajax({
   			type: "POST",
   			url: "mkpaymentoptions.php",
   			data: "&mode=verify-mobile&mobile="+mobinum+"&login="+loginid,
   			success: function(data) {
           		var resp=$.trim(data);
           		if(resp == "0")	{
           			$('.mynt-success').show();
           			$('.your-number').hide();
           			$('.edit-your-number').hide();
           			$('.verify-block').hide();
           			$('.warning').hide();

           			$('#confirmCodOrderBtn').removeClass("disable-btn");
           			$('#confirmCodOrderBtn').addClass("mynt-process-btn");
           			$('#confirmCodOrderBtn').removeAttr('disabled');
           		}
           		if(resp == "2"){
           			$('.warning').hide();
         			$('.your-number').hide();
         			$('.edit-your-number').show();
         			$('.num-error').show();
         			$('.verify-block').hide();
           		}
           		if(resp == "1"){
                    obj.show();
           		}

           		$('#mynt-verify-btn').removeClass("disable-btn");
           		$('#edit-btn-mobile-verify').removeAttr('disabled');
           	},
   			error:function (xhr, ajaxOptions, thrownError){
           		 $('.warning').show();
           		$('#mynt-verify-btn').removeClass("disable-btn");
           		$('#edit-btn-mobile-verify').removeAttr('disabled');
           	}
           });



       });*/

    /*$('.ok-btn').click(function(){
          $('#splash-mobile-verify .close').click();
          $('#mynt-verify-btn').addClass('disabled-btn');
          $('#edit-btn-mobile-verify').attr("disabled","disabled");
   	   	var mobinum = $('input.mobile-number').val();
          $.ajax({
     			type: "POST",
     			url: "mkpaymentoptions.php",
     			data: "&mode=verify-mobile&mobile="+mobinum+"&login="+loginid,
     			success: function(data) {
             		var resp=$.trim(data);
             		if(resp == "0")	{
             			$('.mynt-success').show();
             			$('.your-number').hide();
             			$('.edit-your-number').hide();
             			$('.verify-block').hide();
             			$('.warning').hide();

             			$('#confirmCodOrderBtn').removeClass("disable-btn");
             			$('#confirmCodOrderBtn').addClass("mynt-process-btn");
             			$('#confirmCodOrderBtn').removeAttr('disabled');
             		}
             		if(resp == "2"){
             			$('.warning').hide();
             			$('.your-number').hide();
             			$('.edit-your-number').show();
             			$('.num-error').show();
             			$('.verify-block').hide();
             		}
             		$('#mynt-verify-btn').removeClass('disable-btn');
             		$('#edit-btn-mobile-verify').removeAttr('disabled');
             	},
     			error:function (xhr, ajaxOptions, thrownError){
             		 $('.warning').show();
             		$('#mynt-verify-btn').removeClass('disable-btn');
             		$('#edit-btn-mobile-verify').removeAttr('disabled');
             	}
             });

     });*/

    $('.cancel-payment-btn').click(function() {
    	if(paymentProcessingPopup!=null) {
    		if(checkPaymentWindow !=null) {
    			checkPaymentWindow = window.clearInterval(checkPaymentWindow);
    		}
    		if(paymentChildWindow!=null) {
    			paymentChildWindow.close();
    		}
    		paymentProcessingPopup.hide();
    		window.focus();
    		$.ajax({
    			type: "POST",
     			url: "declineLastOrder.php",
     			data: "reason_code=2&order_type="+Myntra.Data.OrderType,
     			success: function(data) {
     			},
     			error:function (xhr, ajaxOptions, thrownError){
     			}
    		});
    	}
    });
    
    $('form#emi .emi-duration table tr').live('click', function() {
    	if(!$(this).hasClass("emi-disabled")){
	    	$(this).closest('table').find('.radio-btn').removeClass('selected');
	    	$(this).find('.radio-btn').addClass('selected');
	    	$('.order-summary .emi-charges').show().find('.value').html($(this).find('.fee').html());
	    	$('.order-summary .you-pay').html($(this).find('.total').html());
	    	//TODO: update hidden form field for EMI Options here
	    	$('form#emi #emi_bank').val($(this).find('.duration').attr('data-duration'));
    	}	
    });

    $('form#emi select#emi-bank').live('change', function() {
    	var tableToShow = $('form#emi .emi-duration#' + $(this).val());
    	console.log(tableToShow);
    	$('form#emi .emi-duration').hide();
    	tableToShow.show().find('.radio-btn.selected').click();
    });
    if(Myntra.Data.pageName != 'expresss_payment'){
    var giftCardLightBoxObj = Myntra.LightBox('#mk-payment-gc-activate-popup-content');
    var giftCardAjaxHelperUrl = https_loc+"/giftCardsAjaxHelper.php?";
	var successPrefix = "<span class='mk-span-success-ico'></span>";
	var errPrefix = "<span class='mk-span-err-ico'></span>";
    
    $('.mk-payment-gc-activate').data('lb', giftCardLightBoxObj);
	$('.mk-payment-gc-activate').click(function() {
		$(this).data('lb').show();
	});
	$('.mk-giftcard-activation-section .btn-ok').click(function(){
		giftCardLightBoxObj.hide();
		_gaq.push(['_trackEvent', 'payment_page', 'gift_card', 'click']);
	});

	$(".activate-gift-card").click(function(){
		$(".mk-gc-activation-err").html("").removeClass("err").removeClass("success").slideUp('slow');

		var inputEl = $(".mk-gc-card");
		inputEl.removeClass("mk-err");
		var card=inputEl.val();
		
		var inputEl = $(".mk-gc-pin");
		inputEl.removeClass("mk-err");
		var pin=inputEl.val();

		var inputEl = $(".mk-gc-captcha");
		inputEl.removeClass("mk-err");
		var captcha=inputEl.val();


		if(card == ""){
			$("#card-err.mk-gc-activation-err").html(errPrefix + " <span class='msg'>Please enter a gift card number.</span>").addClass("err").slideDown('slow');
			inputEl.addClass("mk-err");
			return; 
		}

		if(captcha == ""){
			$("#captcha-err.mk-gc-activation-err").html(errPrefix + " <span class='msg'>Please verify the code above.</span>").addClass("err").slideDown('slow');
			inputEl.addClass("mk-err");
			return; 
		}

		$.ajax({
		  url: giftCardAjaxHelperUrl+"type=pin&mode=activate&pin="+pin+"&captcha="+captcha+"&card="+card,
		  success: function(data) {
			  var result = $.parseJSON(data);
			  if(typeof result.status != "undefined" && result.status != "failed"){
                    $(".mk-gc-activation-success").html(successPrefix + " <span class='msg'>" + result.message + "</span>").addClass("success").slideDown('slow');
                    $(".mk-gc-card").val("");
                    $(".mk-gc-pin").val("");
                    $(".mk-gc-captcha").val("");
                    $('#captcha').attr("src", https_loc+'/captcha/captcha.php?id=giftCaptcha&rand='+Math.random());
				  //setTimeout(function() {location.reload()}, 2000); // delays 2 secs and then reloads the pge so that cashback information is updated
				  //This should be a success message notification
			  } else {
                if (typeof result.status != "undefined") {
                    if (result.type == "card") {
                        $("#card-err.mk-gc-activation-err")
                            .html(errPrefix + " <span class='msg'>" + result.message + "</span>")
                            .addClass("err").slideDown('slow');
                    } else if (result.type == "captcha") {
                        $("#captcha-err.mk-gc-activation-err")
                            .html(errPrefix + " <span class='msg'>" + result.message + "</span>")
                            .addClass("err").slideDown('slow');
                    } else {
                        $(".mk-gc-activation-error")
                            .html(errPrefix + " <span class='msg'>" + result.message + "</span>")
                            .addClass("err").slideDown('slow');
                    }
                }
			  }
		}});	
	});
    
	$(document).bind('myntra.payment.success',function(){
		if (tmrOut) {
	        clearTimeout(tmrOut);
		}
	});	
    }

});


/*
 * This function finds all HTML elements with "toggle-text" class and binds the "focus"/"blur" events for
 * toggling the default text in those elements.
 */
function toggle_text_all(selector) {
    $(selector).each(function(e) {
        toggle_text_elements[this.id] = $(this).val();
        addToggleText(this);
    });
}

function addToggleText(elem) {
    $(elem).addClass("blurred");
    $(elem).focus(function() {
        if ($(this).val() == toggle_text_elements[this.id]) {
            $(this).val("");
            $(this).removeClass("blurred");
        }
    }).blur(function() {
        if ($(this).val() == "") {
            $(this).addClass("blurred");
            $(this).val(toggle_text_elements[this.id]);
        }
    });
}

function updateTotal(total) {
    var markup = '<span class="rupees">Rs. </span>' + formatNumber(total);
    $(".order-summary .grand-total > .value").html(markup);
    $(".order-summary .total-amount").html(markup);
}

function checkClose() {	
	if(paymentChildWindow!=null) {
		if(paymentChildWindow.closed) {			
			checkPaymentWindow = window.clearInterval(checkPaymentWindow);			
			$.ajax({
    			type: "POST",
     			url: "declineLastOrder.php",
     			data: "reason_code=1&order_type="+Myntra.Data.OrderType,
     			success: function(data) {
     				var resp=$.trim(data);	
     				if(resp=="redirect") {
     					if(isGiftCardOrder!= '1')
     					{
	     					if(ispayUserCloseOpt == 'test' )
	     					{
		     					$("#result-payUserClose").show();
		     					$("#processing-payment").hide();
	     					}
	     					else
	     					{
		     					paymentProcessingPopup.hide();
		     					window.focus();
	     					}
     					}
     					else
     					{
	     					paymentProcessingPopup.hide();
	     					window.focus();
     					}
     				}
     			},
     			error:function (xhr, ajaxOptions, thrownError){
     				paymentProcessingPopup.hide();
 					window.focus();
     			}
    		});
		}
	}
}

function receiveMessage(event)
{
    // do something with event.data;
	if(paymentChildWindow!=null&&event.data!=null&&event.data!="") {
		paymentChildWindow.close();
		if(paymentChildWindow.closed) {
			checkPaymentWindow = window.clearInterval(checkPaymentWindow);
		}
		window.location = event.data;
	}
}

/**
* Unnecessary call to declinelastorder.php happening for 16% time. Fixing the issue.
* Payment service response javascript will call this function.
**/
function notifyPaymentStatus(){
    if(checkPaymentWindow) window.clearInterval(checkPaymentWindow);
}

function copycontactaddress(emi){
	if(Myntra.Data.pageName=='expresss_payment' && !hasAddress){
		$('.pay-block').find('#customer-shipping-name').val($('.addr-form').find('input[name="name"]').val());
        $('.pay-block').find('#customer-shipping-address').val($('.addr-form').find('textarea[name="address"]').val());
        $('.pay-block').find('#customer-shipping-city').val($('.addr-form').find('input[name="city"]').val());
        $('.pay-block').find('#customer-shipping-statename').val($('.addr-form').find('input[name="state"]').val());
        $('.pay-block').find('#customer-shipping-pincode').val($('.addr-form').find('input[name="pincode"]').val());
        $('.pay-block').find('#customer-shipping-country').val($('.addr-form').find('input[name="country"]').val());

	}
	copyContactAddressToBillingAddress(emi);
	
}

function copyContactAddressToBillingAddress(emi) {
	if (emi) {
		$("form#emi #cc_b_firstname").val($('#customer-shipping-name').val());
		$("form#emi #cc_b_address").val($('#customer-shipping-address').val());
		$("form#emi #cc_b_city").val($('#customer-shipping-city').val());
		$("form#emi #cc_b_state").val($('#customer-shipping-statename').val());
		$("form#emi #cc_b_zipcode").val($('#customer-shipping-pincode').val());
		$("form#emi #cc_b_country").val($('#customer-shipping-country').val());
		$('body').scrollTop($('form#emi .cc-bill-address').offset().top);
		_gaq.push(['_trackEvent', 'payment_page', 'copy_shipping_address', 'success']);
	} else {
		$("form#credit_card #cc_b_firstname").val($('#customer-shipping-name').val());
		$("form#credit_card #cc_b_address").val($('#customer-shipping-address').val());
		$("form#credit_card #cc_b_city").val($('#customer-shipping-city').val());
		$("form#credit_card #cc_b_state").val($('#customer-shipping-statename').val());
		$("form#credit_card #cc_b_zipcode").val($('#customer-shipping-pincode').val());
		$("form#credit_card #cc_b_country").val($('#customer-shipping-country').val());
		$('body').scrollTop($('form#credit_card .cc-bill-address').offset().top);
		_gaq.push(['_trackEvent', 'payment_page', 'copy_shipping_address', 'success']);
	}
}

$(document).ready(function(){
	
	if($(".mk-summary-gc-thumbnail").length){
		$('.mk-summary-gc-thumbnail img').data('lb', Myntra.LightBox("#" + $(".mk-giftcardpreview-overlay").attr("id")));
		$(".mk-summary-gc-thumbnail img").click(function(){
			$(this).data('lb').show();
		});
	}
	
	$('#captcha-form').keydown(function(event) {
		if (event.keyCode == '13') {
		event.preventDefault();
		$("#cod-verify-captcha").trigger("click");
		}
	});
});

function verifyCaptcha() {

	$(document).trigger('myntra.payment.success');

	if($("#cod-verify-captcha").hasClass("btn-disabled")){
		return;
	}
	if($('#captcha-form').val() == ""){
		$("#cod-captcha-message").html("Please enter the text.");
		$("#cod-captcha-message").addClass("error");
		$("#cod-captcha-message").slideDown();
		return;
	}
	$("#cod-captcha-loading").show();
	$("#cod-verify-captcha").addClass("btn-disabled")
	$("#cod-captcha-message").html("").removeClass("error");
	$("#cod-captcha-message").slideUp();
	$.ajax({
		type: "POST",
		url: https_loc+"/cod_verification.php",
		data: "codloginid="+ loginid + "&condition=clickverifycaptcha&confirmmode=customerpvqueued&mobile=" + $('#cod-mobile').val() + "&userinputcaptcha=" + $('#captcha-form').val() ,
		success: function(msg){

			var resp=$.trim(msg);
			$("#cod-captcha-loading").hide();
			$("#cod-verify-captcha").removeClass("btn-disabled");
			if(resp=='CaptchaConfirmed') {
                            if (!telesales || confirm('Are you sure you want to place the order?') ) {
				$("#cod-verify-captcha").hide();//addClass("btn-disabled");
				$("#cod-captcha-loading").show();
				$("#cod-captcha-message").html('Code verified. Please wait while your order is being processed.');
				$("#cod-captcha-message").addClass('success');
				$("#cod-captcha-message").slideDown();
				$("#cod").trigger("submit");
				_gaq.push(['_trackEvent', 'payment_page', 'cod_captcha', 'success']);
                            }
                            else{
                                location.reload();
                                return ;
                            }
			} else if(resp=='WrongCaptcha') {
				$("#cod-captcha-message").html('Wrong Code entered');
				$("#cod-captcha-message").addClass('error');
				$("#cod-captcha-message").slideDown();
				_gaq.push(['_trackEvent', 'payment_page', 'cod_captcha', 'failure']);
			}
		},
		error:function (xhr, ajaxOptions, thrownError) {
			$("#cod-captcha-loading").hide();
			$("#cod-verify-captcha").removeClass("btn-disabled");
			$("#cod-captcha-message").html('Wrong Code entered');
			$("#cod-captcha-message").addClass('error');
			$("#cod-captcha-message").slideDown();
		}
	});
}

/* Cart Page counpon redeem */
function redeemCoupon(){
	var couponForm = $("#couponform");
	var string = $("#othercouponcode", couponForm).val();
	var pattern = /^[a-zA-Z0-9\s]+$/g;
    if(string == '' || !pattern.test(string) || string == "Enter a Coupon") {
        alert("Please enter a valid coupon code to redeem.");
        return;
    }
    _gaq.push(['_trackEvent', 'payment_page', 'coupon_widget', 'coupon_redeem']);
    $("#couponcode", couponForm).val(string);
    couponForm.submit();
}
function redeemMyntCash(){
	var userCashAmount =  parseFloat($("#userCashAmount").val());
	if(userCashAmount !='' && userCashAmount>0) {
		_gaq.push(['_trackEvent', 'payment_page', 'coupon_widget', 'redeem_cashcoupon']);
		$("#redeemMyntCashButton").hide();
		document.myntCashForm.submit();
	}
}
function removeCoupon()
{
	_gaq.push(['_trackEvent', 'payment_page', 'coupon_widget', 'remove_coupon']);
    var couponForm = $("#couponform");
	$("#couponcode", couponForm).val('');
    $("#removecouponcode", couponForm).val('remove');
	document.couponform.submit();
}

function removeMyntCash()
{
	_gaq.push(['_trackEvent', 'payment_page', 'coupon_widget', 'remove_cashcoupon']);
    	var couponForm = $("#couponform");
	$("#useMyntCash", couponForm).val('remove');
	var appliedCoupon=$("#c-code").val();
	if(appliedCoupon != ''){
	  $("#code", couponForm).val(appliedCoupon);
	}else{
		 $("#code", couponForm).val('');
	}
	document.couponform.submit();
}

$("#discount_coupon_message").hover(
  function (is) {
    $(this).find("span").find("a").attr("style","color:#FF0000");
  },
  function () {
    $(this).find("span").find("a").attr("style","color:#COCOCO");
  }
);

/*$("#othercouponcode").live('blur', function() {
	var coupon=$("#othercouponcode").val();
	if(coupon != '') {
		redeemCoupon();
	}
});
*/

function clearTextArea(){
	$("#othercouponcode").val('').blur();
}
$("#discount_cashcoupon_message").hover(
  function (is) {
    $(this).find("span").find("a").attr("style","color:#FF0000");
  },
  function () {
    $(this).find("span").find("a").attr("style","color:#COCOCO");
  }
);

$(".select-coupons li").hover(
  function () {
    $(".select_this_coupon a",this).addClass("mk-hover");
  },
  function () {
    $(".select_this_coupon a",this).removeClass("mk-hover");
  }
);

$(".your-coupons .select-coupons li .inline-block").click( function () {
    $('.your-coupons .select-coupons').slideUp('fast');
	$(this).parent().find(".select_this_coupon a").click();
});

function redeemCoupon2(couponCode){
	$('.your-coupons .select-coupons').slideUp('fast');
	var couponForm = $("#couponform");
	var string = couponCode;
	$("input#othercouponcode").val(couponCode).blur();
	//redeemCoupon();
	$('.coupon-apply-btn').removeAttr('disabled');
	/*$("#couponcode").val(couponCode);
	var pattern = /^[a-zA-Z0-9\s]+$/g;
    if(string == '' || !pattern.test(string)) {
        alert("Please enter a valid coupon code to redeem.");
        return;
    }
    _gaq.push(['_trackEvent', 'coupon_widget', 'coupon_select', 'click']);*/
    //couponForm.submit();
	$('.coupon-apply-btn', couponForm).click();
}

function verifyMobileCommon(id)	{
	$("#verify-mobile").hide();
	$("#verify-mobile-code").slideUp();
	var profileForm=$("#change-profile");
	$("#mobile-verify-loading").show();
    var mobile = $("#mobile", profileForm).val();
    var login = $("#login", profileForm).val();
    $("#mobile",profileForm).attr("disabled","disabled");
	
    $.ajax({
    	type: "POST",
        url: "mymyntra.php",
        data: "&mode=check-mobile-user&mobile=" + $("#mobile", profileForm).val() + "&login=" + $("#login", profileForm).val()+"&_token="+$("#_token", profileForm).val(),
        success: function(msg){
			msg = $.trim(msg);
			if (msg == "sendSMS") {
   				$.ajax({
   			        type: "POST",
   			        url: "mymyntra.php",
   			        data: "&mode=generate-code&mobile=" + $("#mobile", profileForm).val() + "&login=" + $("#login", profileForm).val()+"&_token="+$("#_token", profileForm).val(),
   			        success: function(msg){
   						msg = $.trim(msg);
   						$("#mobile-verify-loading").hide();
   			   			if (msg == "showCaptcha")	{
	   			   			$("#v-code-note-message").hide();
	   		   				$("#v-code-note-message").removeClass("error");
	   		   				$("#v-code-note-message").text("Note: Please verify your specified contact number to be able to use any user-specific coupons that you may have been issued through Myntra. We may use this number to contact you for issues related to any of your transactions on Myntra.").fadeIn('slow');
   			   				$("#verify-mobile").attr("value","Send Again")
   		   					$("#mobile-captcha").attr("src", https_loc+'/captcha/captcha.php?id='+id+'&rand='+Math.random());
   			   				$("#mobile-captcha-form").val("");
   			   				$("#mobile-captcha-block").show();
   			    			$(".change-password-link").hide();
   			    		} else if (msg == "verifyFailed"){
   			    			$("#verify-mobile").show();
   			    			$("#mobile",profileForm).removeAttr("disabled");
   			    			$("#verify-mobile").attr("value","VERIFY");
   			    			$("#v-code-note-message").hide();
   			   				$("#v-code-note-message").addClass("error");
   			   				$("#v-code-note-message").text("You have exceeded the maximum no. of attempts to verify this mobile number. If you have still not received the verification code or are facing any other difficulties, please provide an alternate number and retry. Or, try again after 6 hours.").fadeIn('slow');
   			    		}
   			    		else{
   			    			$("#verify-mobile").show();
   			    			$("#mobile",profileForm).removeAttr("disabled");
   			    			$("#v-code-note-message").hide();
   	   		   				$("#v-code-note-message").removeClass("error");
   	   		   				$("#v-code-note-message").text("Note: Please verify your specified contact number to be able to use any user-specific coupons that you may have been issued through Myntra. We may use this number to contact you for issues related to any of your transactions on Myntra.").fadeIn('slow');
   			    			mobileVerificationSubmitCount=0;
   			    			$("#verify-mobile").attr("value","Send Again")
   			                $("#verify-mobile-code").slideDown();
   			    			$(".mobile-code-entry").show();
   							$("#mobile-message").html("Please enter the verification code that was sent to <strong>" + mobile + "</strong>");

   							$("#mobile-message").show();
   							$("#mobile-code").val("");
   			    			$("#mobile-verification-error").removeClass("error");
   			    			$("#mobile-verification-error").text("If you have not received your verification code, please wait for atleast 2 minutes before pressing submit button");
   			    		}

   					},
   			   		error:function (xhr, ajaxOptions, thrownError) {
   			    		$("#mobile-verify-loading").hide();
   						$("#verify-mobile").show();
   						$("#mobile",profileForm).removeAttr("disabled");
   						$("#v-code-note-message").hide();
   						$("#v-code-note-message").addClass("error");
   						$("#v-code-note-message").text("An error has occured while sending the sms please retry again").fadeIn('slow');
   			   		}
   				});
   			} else if (msg=="exceed"){
   	   			//number of attempts to be tried exceeds disable the edit button and show him the message to retry after 6 hours
   	   			$("#mobile",profileForm).removeAttr("disabled");
   				$("#mobile-verify-loading").hide();
   				$("#verify-mobile").show();
   				$("#v-code-note-message").hide();
   				$("#v-code-note-message").addClass("error");
   				$("#v-code-note-message").text("You have exceeded the maximum no. of attempts to verify this mobile number. If you have still not received the verification code or are facing any other difficulties, please provide an alternate number and retry. Or, try again after 6 hours.").fadeIn('slow');

   			} else if (msg == "verified"){
   	   			//The number is already verified by some other user
   	   			$("#mobile-verify-loading").hide();
   				$("#verify-mobile").show();
   				$("#mobile",profileForm).removeAttr("disabled");
   				$("#v-code-note-message").hide();
   				$("#v-code-note-message").addClass("error");
   				$("#v-code-note-message").text("This number has been verified with us by another user. Please use another mobile number.").fadeIn('slow');
   			} else if (msg == "skipVerify"){
   				$("#mobile-verify-loading").hide();
   				$("#v-code-note-message").hide();
   				$("#verify-mobile").hide();
   				window.location.reload();

   			}else {
   	   			//Some other error has occured asked him to retry.
   	   			$("#mobile-verify-loading").hide();
   				$("#verify-mobile").show();
   				$("#mobile",profileForm).removeAttr("disabled");
   				$("#v-code-note-message").hide();
   				$("#v-code-note-message").addClass("error");
   				$("#v-code-note-message").text("An error has occured while sending the sms please retry again").fadeIn('slow');
   			}
    	},
   		error:function (xhr, ajaxOptions, thrownError) {
    		$("#mobile-verify-loading").hide();
			$("#verify-mobile").show();
			$("#mobile",profileForm).removeAttr("disabled");
			$("#v-code-note-message").hide();
			$("#v-code-note-message").addClass("error");
			$("#v-code-note-message").text("An error has occured while sending the sms please retry again").fadeIn('slow');
   		}
    });
    _gaq.push(['_trackEvent', 'payment_page', 'coupon_widget', 'verify_mobile']);
}

function validateMobileNumber() {
	var numericReg  =  /(^[1-9]\d*$)/;
	var error = false;
	var profileForm=$("#change-profile");
	var mobileVal=$("#mobile", profileForm).val();
	$("#mobile-error", profileForm).hide();
	if(mobileVal == "" || mobileVal.length < 10 || !numericReg.test(mobileVal)){
		error=true;
		$("#mobile-error", profileForm).text("Enter a valid mobile number.").slideDown();
	}
	return error;

}



function verifyMobileCaptcha() {
	if($("#mobile-verify-captcha").hasClass("btn-disabled")){
		return;
	}
	if($('#mobile-captcha-form').val() == ""){
		$("#mobile-captcha-message").html("Please enter the text.");
		$("#mobile-captcha-message").addClass("error");
		$("#mobile-captcha-message").slideDown();
		return;
	}
	$("#mobile-captcha-loading").show();
	$("#mobile-verify-captcha").addClass("btn-disabled")
	$("#mobile-captcha-message").html("").removeClass("error");
	$("#mobile-captcha-message").slideUp();
	$.ajax({
		type: "POST",
		url: https_loc+"/mobile_verification.php",
		data: "mobile=" + $('#mobile').val() + "&userinputcaptcha=" + $('#mobile-captcha-form').val() + "&login=" + $('#mobile-login').val() + "&id=paymentpage",
		success: function(msg){

			var resp=$.trim(msg);
			$("#mobile-captcha-loading").hide();
			$("#mobile-verify-captcha").removeClass("btn-disabled");
			if(resp=='CaptchaConfirmed') {
				$("#mobile-captcha-block").hide();
				$(".verify-mobile-cart-page").trigger("click");

			} else if(resp=='WrongCaptcha') {
				$("#mobile-captcha-message").html('Wrong Code entered');
				$("#mobile-captcha-message").addClass('error');
				$("#mobile-captcha-message").slideDown();
		   }
		},
		error:function (xhr, ajaxOptions, thrownError){
			$("#mobile-captcha-loading").hide();
			$("#mobile-verify-captcha").removeClass("btn-disabled");
			$("#mobile-captcha-message").html('Internal Error Occured. Please Retry again');
			$("#mobile-captcha-message").addClass('error');
			$("#mobile-captcha-message").slideDown();
		}
	});
	_gaq.push(['_trackEvent', 'payment_page', 'coupon_widget', 'verify_mobile_captcha']);
}

function netBankingOptionChange() {
		var selectText = $("#netBanking option:selected").text();
		selectText  = selectText.toLowerCase();
		if(selectText.indexOf('icici') != -1) {
			if(icici_discount>0) {
				var iciciDiscount = amountToDiscount*icici_discount/100;
				iciciDiscount = Math.round(iciciDiscount);
				var finalAmount = total_amount - iciciDiscount;
				finalAmount = finalAmount<0?0:finalAmount;
                $(".order-summary .additional-discount").show().find("> .value").text("(-) Rs. " + formatNumber(iciciDiscount));
                updateTotal(finalAmount);
				$("#icici_discount_div").fadeOut('slow',function() {
					$("#icici_discount_div").css("background-color","#ffffcc");
					$("#icici_discount_div").html("Congrats! A discount of <span style='color:#d20000'>Rs. " + formatNumber(iciciDiscount) +"</span> has been applied").fadeIn(3000,function() {
						$("#icici_discount_div").css("background-color","#ffffff");
					});
				});
			}
		} else {
            $(".order-summary .additional-discount").hide();
            updateTotal(total_amount);
			$("#icici_discount_div").html(icici_discount_text);
		}
}

$(document).ready(function() {

	$('.your-coupons > a').click(function(e) {
		var couponList = $('.your-coupons .select-coupons'); 
		if(couponList.length > 0){
			if(couponList.css('display') == "none")	{
				_gaq.push(['_trackEvent', 'payment_page', 'coupon_widget', 'coupon_dropdown']);
			}
			couponList.slideToggle();
            //e.stopPropagation();
		}
	});
	
	$('.coupons-widget .err-div .close').click( function() {
		$(this).closest('.err-div').fadeOut("fast");
		$(this).parents(".coupons-widget").find(".wrong-large-icon").fadeOut("fast");
		//$(this).parents(".coupons-widget").find(".wrong-large-icon").remove();
	});

	$('.your-coupons .select-coupons .close').click( function() {
		$('.your-coupons .select-coupons').slideUp();
	});

	$('.your-coupons .select-coupons li').hover(
		function () {
		  $(this).addClass('couponSelect');
		},
		function () {
			$('.your-coupons .select-coupons li').removeClass('couponSelect');
		}
	);

	$("#lmore").click(function(){
		$('.lmore').toggleClass("hide");
	});

	$(".code-enter .coupon-code").keydown(function(event) {
		if(event.keyCode == '13'){
			event.preventDefault();
			redeemCoupon();
		}
	}).keyup(function(event) {
		if($(this).val()=='' || $(this).val()==null) {
			$('.coupon-apply-btn').attr('disabled','disabled');
		} else {
			$('.coupon-apply-btn').removeAttr('disabled');
		}
	});
	
	$('#mobile').keydown(function(event) {
		if (event.keyCode == '13') {
			event.preventDefault();
			if ($(".verify-mobile-cart-page").css("display") != "none") {
				$(".verify-mobile-cart-page").trigger("click");
			}
		}
	});

	$('#mobile-captcha-form').keydown(function(event) {
		if (event.keyCode == '13') {
		event.preventDefault();
		$("#mobile-verify-captcha").trigger("click");
		}
	});

	$(".verify-mobile-cart-page").live('click',function(){
		if(!validateMobileNumber()) {
			verifyMobileCommon("paymentpage");
		}
	});

	$(".submit-code").live('click',function(){
		var profileForm=$("#verify-mobile-code");
		var codeStr = $("#mobile-code", profileForm).val();
		if (codeStr != "") {
			$("#submit-code").hide();
			$("#code-verify-loading").show();
			$.ajax({
				type: "POST",
				url: "mkpaymentoptions.php",
				data: "&mode=verify-mobile-code&mobile-code=" +codeStr,
				success: function(data) {
					var responseObj=$.parseJSON(data);
					var status=responseObj.status;
					var message = responseObj.message;
					var num_attempts = responseObj.attempts;
					if(status==1){
						//reload the page verification was successful.
						$("#mobile-verification-error").hide();
						$("#mobile-verification-error").removeClass("error");
						$("#mobile-verification-error").text(message).fadeIn('slow');
						window.location.reload();
					} else {
						$("#submit-code").show();
						$("#code-verify-loading").hide();
						mobileVerificationSubmitCount++;
						if(mobileVerificationSubmitCount>5) {
							//hide and ask him to resend the sms.
							$("#verify-mobile-code").slideUp();
							$("#v-code-note-message").hide();
							$("#v-code-note-message").addClass("error");
							$("#v-code-note-message").text("You have exceeded the maximum no. of attempts to validate the SMS code. To try verifying your mobile again please try with a new verification code.").fadeIn('slow');
						} else {
							$("#mobile-verification-error").text(message);
							$("#mobile-verification-error").addClass("error");
						}
					}
				},
				error:function (xhr, ajaxOptions, thrownError){
					$("#submit-code").show();
					$("#code-verify-loading").hide();
					$("#mobile-verification-error").text("Internal Error occured. Please retry again.");
					$("#mobile-verification-error").addClass("error");
				}
			});
			_gaq.push(['_trackEvent', 'payment_page', 'coupon_widget', 'submit_mobile_code']);
		}
	});
	$("#order-summary").hover(function(){
		//$(this).toggleClass('black-bg4');
		$(this).toggleClass("bgcolor");
	});
	$("#order-summary").live('click',function(){
		$("#order-summary").toggleClass('downarrow', 'uparrow');
		$("#order-cart-items").toggle(400);
		if(!orderSummaryDislplayed) {
			$("#cart-pre-loading").show();
			orderSummaryDislplayed = true;
			$.ajax({
				type: "POST",
				url: "getMyCartData.php",
				data: "",
				success: function(data) {
					 $("#cart-pre-loading").hide();
					if(data!=null) $("#order-cart-items").html(data);
				},
				error:function (xhr, ajaxOptions, thrownError){
					orderSummaryDislplayed = false;
					 $("#cart-pre-loading").hide();
				}
			});
			_gaq.push(['_trackEvent', 'payment_page', 'order_summary']);
		}

	});

	$("#phone-order-confirm").live('click',function(){
		$("#phone_payment").submit();
		_gaq.push(['_trackEvent', 'payment_page', 'pay_by_phone', 'confirm_order']);
	});
//When page loads...
    //On Click Event
    $("span.discount_option input").click(function() {
           $(".tab_content").hide();
            var activeTab = $(this).attr("class");
            $(activeTab).fadeIn();
        });

    });

// Cash on Delivery Mobile verification
$('.num-edit-btn').click(function(){
    $(this).hide();
    $('.edit-your-number').show();
    $('.your-number').hide();
    $('.verify-block').hide();
});

$('.cancel-btn').click(function(){
    $('.edit-your-number').hide();
    $('.num-error').hide();
    $('.warning').show();
    $('.num-edit-btn').show();
    $('.your-number').show();
    $('.verify-block').show();

});


$('.save-btn').click(function(){
//alert($('input.mobile-number').val());
    var mobinum = $('input.mobile-number').val();
    var numericReg  =  /(^-?\d\d*\.\d*$)|(^-?\d\d*$)|(^-?\.\d\d*$)/;

    if((mobinum == userMobileNumber) ||
    	(mobinum == '') ||
    	(!numericReg.test(mobinum)) ||
    	(mobinum.length < 10)
    	){
    		return;
    }else{

    	$('.your-number #userMobile').html(mobinum);
    	$('.verify-sms #userMobile').html(mobinum);
        $('.verify-block').show();
        $('.num-edit-btn').show();
        $('.your-number').show();
        $('.edit-your-number').hide();

        $('#mynt-verify-btn').addClass('disable-btn');
        $('#edit-btn-mobile-verify').attr('disabled','true');
        $.ajax({
			type: "POST",
			url: "mkpaymentoptions.php",
			data: "&mode=update-mobile-number&new-mobile-number="+mobinum+"&login="+loginid,
			success: function(data) {
        		var resp=$.trim(data);
        		if(resp == "0")	{
        			//prompt for verification
        			$('.num-error').hide();
        			$('.warning').show();
        		}
        		if(resp == "1"){
        			$('.warning').hide();
        			$('.num-error').show();
        		}
        		userMobileNumber = mobinum;
        		$('#mynt-verify-btn').removeClass('disable-btn');
        		$('#edit-btn-mobile-verify').removeAttr('disabled');
        	},
			error:function (xhr, ajaxOptions, thrownError){
        		 $('.warning').show();
        		 $('#mynt-verify-btn').removeClass('disable-btn');
        		 $('#edit-btn-mobile-verify').removeAttr('disabled');
        	}
        });
    }

    try { $(this).blur(); } catch (ex) {}
});

$("#result-payFailOver-err a.codbtn").live("click", function(){
	_gaq.push(['_trackEvent', 'payment_page', 'failover','cod']);
	if(paymentProcessingPopup==null)paymentProcessingPopup = Myntra.LightBox('#splash-payments-processing',cfg);
	$("#result-payFailOver-err").hide();
	$("#processing-payment").show();
	paymentProcessingPopup.hide();
	$('#pay_by_cod').click();
	Myntra.LightBox.scrollTop = $('.pay-block .tabs').offset().top;	
});

$("#result-payFailOver-err a.tryagainbtn").live("click", function(){
	if($("a.codbtn").length){
		_gaq.push(['_trackEvent', 'payment_page', 'failover','tryagain',1]);		
	}else{
		_gaq.push(['_trackEvent', 'payment_page', 'failover','tryagain',0]);		
	}

	if(paymentProcessingPopup==null)paymentProcessingPopup = Myntra.LightBox('#splash-payments-processing',cfg);
	$("#result-payFailOver-err").hide();
	$("#processing-payment").show();	
	paymentProcessingPopup.hide();
	$(window).scrollTop($('.pay-block .tabs').offset().top);
});

$("#result-payUserClose a.codbtn").live("click", function(){
	_gaq.push(['_trackEvent', 'payment_page', 'userClose','cod']);
	if(paymentProcessingPopup==null)paymentProcessingPopup = Myntra.LightBox('#splash-payments-processing',cfg);
	$("#result-payUserClose").hide();
	$("#processing-payment").show();
	paymentProcessingPopup.hide();
	$('#pay_by_cod').click();
	Myntra.LightBox.scrollTop = $('.pay-block .tabs').offset().top;	
});

$("#result-payUserClose a.tryagainbtn").live("click", function(){
	if($("a.codbtn").length){
		_gaq.push(['_trackEvent', 'payment_page', 'userClose','tryagain',1]);		
	}else{
		_gaq.push(['_trackEvent', 'payment_page', 'userClose','tryagain',0]);		
	}

	if(paymentProcessingPopup==null)paymentProcessingPopup = Myntra.LightBox('#splash-payments-processing',cfg);
	$("#result-payUserClose").hide();
	$("#processing-payment").show();	
	paymentProcessingPopup.hide();
	$(window).scrollTop($('.pay-block .tabs').offset().top);
});

$(document).ready(function(){
    if(Myntra.Data.promptLogin){
        Myntra.expiryLoginPrompt();
        return false;
    }
});

$(window).load(function() {

	var addressNoOfOrders = $('.shipping-address .zipcode-city-state'),
        pincodeTooltipElem = $('#pincode-city-orders-tooltip');
    if (!addressNoOfOrders.length || !pincodeTooltipElem.length) return;
	var pincodeCityOrdersTooltip = new Myntra.Tooltip(pincodeTooltipElem, addressNoOfOrders, {side: 'left', nohover: true, noclick: true});
	pincodeCityOrdersTooltip.show(null, addressNoOfOrders);
	pincodeCityOrdersTooltip.triggerAutoHide(10*1000); //10 seconds auto-hide	
});

$("#result-payUserClose .close").on("click", function(){	
	if(paymentProcessingPopup==null)paymentProcessingPopup = Myntra.LightBox('#splash-payments-processing',cfg);
	$("#result-payUserClose").hide();
	$("#processing-payment").show();
	paymentProcessingPopup.hide();	
});
