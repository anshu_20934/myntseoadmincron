
Myntra.Header = {
    init: function() {
  	
        $(".btn-login").click(function(e) {
            e.preventDefault();
            _gaq.push(['_trackEvent', 'header', 'signin', 'click']);
            var cfg = {},
            	href = this.href;
            if (href) {
            	cfg.action = 'login';
            	cfg.onLoginDone = function() {
            		window.location = href;
            	};
        	} else if ($(this).text().toLowerCase().replace(' ', '') == 'login') {
            	cfg.action = 'login';
            }
            if(Myntra.Data.rememberUser != '') {
                cfg.action = 'login';
            }
            $(document).trigger('myntra.login.show', cfg);
            return false;
        });
        $('#header-logout-link').click(function(e) {
            e.preventDefault();
            _gaq.push(['_trackEvent', 'header', 'signout', 'click']);
            window.localStorage.removeItem('PincodeWidget.ServiceabilityData');
            $('#header-logout-form').submit();
        });

        // Save Recent Bag ------------------------
        /*$('.mk-header-hover').click(function(){
        	var id_=$(this).find('em').attr('data-id');
        	_gaq.push(['_trackEvent', "pdp_click", Myntra.Data.pageName, "header_"+id_]);
        	return true;
        });*/

    	function showDashboardItems(data_id) {
    		var dashboardItems = $('.mk-' + data_id + '-items');
    		if (!Myntra.Data.newLayout) {
    			$('.mk-site-nav-wrapper, .mk-site-nav').hide();
    			$('.mk-pdp-carousel-sub-items').show();
    			dashboardItems.css('z-index','1');
    		}
    		dashboardItems.show();
			$('.mk-header-hover em.mk-' + data_id).closest('.mk-header-hover').addClass('mk-hover').find(".mk-arrow").removeClass("mk-hide");
    	}
    	
    	function hideDashboardItems(data_id) {
    		var dashboardItems = $('.mk-' + data_id + '-items');
    		if (!Myntra.Data.newLayout) {
    			$('.mk-site-nav-wrapper, .mk-site-nav').show();
    			$('.mk-pdp-carousel-sub-items').hide();
    			dashboardItems.css('z-index','0');
    		}
    		dashboardItems.hide();
			$('.mk-header-hover em.mk-' + data_id).closest('.mk-header-hover').removeClass('mk-hover').find(".mk-arrow").addClass("mk-hide");
    	}

    	$('.mk-header-hover').hover(function(){
    		var em = $(this).find('em');
    		if(em.attr('data-show') == 'true'){
    			if(em.attr('data-load') == 'true'){
    				var id_ = em.attr('data-id');
    				Myntra.Header.fetchData(id_);
    				_gaq.push(['_trackEvent', id_,window.location.toString(), 'hover']);
    			}
    			showDashboardItems(em.attr('data-id'));
    		}
    	}, function(){
    		var em = $(this).find('em');
    		if(em.attr('data-show') == 'true'){
    			hideDashboardItems(em.attr('data-id'));
			}
    	});
    	
    	$('.mk-mymyntra-items').hover(
    		function() {
    			showDashboardItems('mymyntra');
			},
    		function() {
				hideDashboardItems('mymyntra');
			}
    	);

    	$('.mk-saved-items').hover(
    		function() {
    			showDashboardItems('saved');
    		},
    		function() {
    			hideDashboardItems('saved');
    		}
    	);

    	$('.mk-recent-items').hover(
    		function() {
    			showDashboardItems('recent');
    		},
    		function() {
    			hideDashboardItems('recent');
    		}
    	);

   	  	$('.mk-bag-items').hover(
    		function() {
    			showDashboardItems('bag');
    		},
    		function() {
    			hideDashboardItems('bag');
    		}
    	);
   	  	
   	  $('.mk-mymyntra-items, .mk-mymyntra-dash').on('click', 'a', function() {
   	  		_gaq.push(['_trackEvent', 'header', 'myMyntraDashboard', $(this).text()]);
   	  	});
   	 /* 	$('.mk-mymyntra-items a, .mk-mymyntra-dash a').live('click', function() {
   	  		_gaq.push(['_trackEvent', 'header', 'myMyntraDashboard', $(this).text()]);
   	  	});*/

    },

    setCount: function(type, val) {
        // valid types: recent saved bag
        var tnode = $('.mk-site-hd .mk-' + type),
            cnode = tnode.siblings('.mk-count'),
            snode = cnode.find('strong');
        if (!snode.length) {
            cnode.html('[<strong></strong>]');
        }
        cnode.find('strong').text(val);
        tnode.attr('data-show','true').attr('data-load','true');
    },

    getCount: function(type) {
        // valid types: recent saved bag
        return +$('.mk-site-hd .mk-' + type).siblings('.mk-count').find('strong').text();
    },

    fetchData: function(type){
    	$('.mk-site-hd .mk-' + type).attr('data-load','false');
    	//valid types : recent saved bag mymyntra
    	//Do Ajax loading here
    	$('.mk-'+type+'-items ul').html('<li class="rs-carousel-item"><img height="150" width="112" src="http://myntra.myntassets.com/skin2/images/loader_150x200.gif"></li>');
    	var rUrl=http_loc+"/getHeaderCarouselItems.php?type="+type;
    	if(type == "mymyntra") {
    		rUrl = http_loc + '/mymyntradashboard.php';
    	}
    	$.ajax({
			type:"GET",
			url:rUrl,
			dataType:"json",
			success: function(data){
				if(data.status==='SUCCESS'){
					Myntra.Header.loadData(type,data.content);
				}
				else {
					$('.mk-site-hd .mk-' + type).attr('data-show','false');
				}
			}
		});
    },
    loadData: function(type,data){
    	//add/replace the items and intiate the scroller
    	 //$('.mk-'+type+'-items ul').hide();
    	$('.mk-'+type+'-items ul').html(data);
    	if (type == "mymyntra") {
    		return;
    	}
    	if($('.mk-'+type+'-items ul > li').size()>5){
    		$('.mk-'+type+'-items').carousel({
    			itemsPerPage: 5, // number of items to show on each page
    			itemsPerTransition: 1, // number of items moved with each transition
    			nextPrevActions: true, // whether next and prev links will be included
    			pagination: false, // whether pagination links will be included
    			onHover: true // whether hover will act like click for next/prev
    		});
    	}
    	else
    	{
    		$('.mk-'+type+'-items ul').width('940');
    		$('.mk-'+type+'-items ul li').css({'float':'right','margin-right':'15px'});
    	}
//    	/$('.mk-'+type+'-items ul').show();
    },
    setBagTotal : function(val){
        var tnode = $('.mk-site-hd .bag-cont');
        if(val){
            tnode.find('.rupees').text('Rs.'+Myntra.Utils.formatNumber(Math.round(val))).show();    
            tnode.find('i').hide(); 
        }
        else {
            tnode.find('.rupees').text('Rs.'+Myntra.Utils.formatNumber(Math.round(val))).hide();    
            tnode.find('i').show();    
        }
        
    }

};


$(document).ready(function() {
    Myntra.Header.init();
    var trackOrderTT = new Myntra.Tooltip(null, '#orders-track-link', {side: 'below', useTitle: true});
    //Search Form Default Text Behaviour
    $(".mk-placeholder-text").click(function(){
        $(this).hide();
        $(this).closest("form").find(".mk-site-search").focus();
    });

    $(".mk-site-search").each(function(){
        $(this).focus(function(event){
            $(this).closest("form").find(".mk-placeholder-text").hide();
        }).blur(function(event){
            if($(event.currentTarget).val() == ""){
                $(this).closest("form").find(".mk-placeholder-text").show();
            }
            return true;
        });
    });

});

