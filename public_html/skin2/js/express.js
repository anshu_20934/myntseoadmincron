$(document).ready(function() {
	var tabsDefaultPaymentFormToSubmit = $('#paymentFormToSubmit').val();
	if($('.pay-block-default form').length){
		$('#paymentFormToSubmit').val($('.pay-block-default form').attr('id'))
	}
	//$('.pay-block-default .pay-btn').show();
    Myntra.PaymentUIObj.fillCountries(Myntra.Data.countryList);
	if(!hasAddress){
		$(document).trigger('myntra.address.form.render', {
			_type:'shipping',
			container:'.address-form-wrap',
			isPopup:false
		});
        //this will be when the adress is saved
	    $(document).bind('myntra.address.form.done', function(e, data) {
            hasAddress=true;
	    	var obj = data;
	    		obj.hasAddressId = false;
	    	$(document).trigger('express.payment.submit',data);
	       
	    	
	    });
	    $(document).on('myntra.address.form.oninit',function(){
	    	var pcode = '';
	    	$('.exp-payment-page .lightbox-nopop input[name="pincode"]').on('blur',function(){
	    		if(pcode === $(this).val()){
	    			return;
	    		}
	    		pcode = $(this).val();
            	if (!/\d{6}/.test(pcode)) {
                	return
            	}
            	var pData ={};
	    		pData.styleid = pStyleID;
	    		pData.skuid = pSKUID;
	    		pData.pincode = pcode ;
	    		pData.codErrorCode = codErrorCode;
	    		Myntra.PaymentUIObj.ajax(pData);

		    });
	    
	    });
	    
	}
	else{
		$('#address-change').bind('click',function(){
			$(document).trigger('myntra.address.list.show', {
				_type:'shipping',
		        container:'.address-list-wrap',
		        isPopup:true
		    });
			$('#lb-address-list .ft .btn-save').text('continue').addClass('primary-btn');
		});
		$(document).bind('myntra.address.list.done', function(e,selectedAddress){
			//STORE ADDRESS AND THEN RELOAD THE PAGE
			window.location="/expressbuy.php?address="+selectedAddress.id;
		});

	}
	$('.more-pay-options').on('click',function(){
		//show other payment options
		$('.pay-block-tab').show();
		$('.pay-block-default').remove();
		$('#paymentFormToSubmit').val(tabsDefaultPaymentFormToSubmit);
	});

	$('.show-credit-card-form').click(function(){
        $(this).closest('.saved-cards').hide().siblings('.card-details').show();
		$(this).closest('form').find('input[name="use_saved_card"]').val('false');	
        $(this).closest('form').find('input[name="cvv_code"]').val('');  
        $('.err-card').html("");
 //       $(".pay-btn").removeAttr("disabled");
	});
	$('.show-saved-cards').click(function(){
        $(this).closest('.card-details').hide().siblings('.saved-cards').show();
		$(this).closest('form').find('input[name="use_saved_card"]').val('true');
        $('.err-card').html("");
 //       $(".pay-btn").removeAttr("disabled");
	});

	//For card Select 
	//Set the insrument id
	//Skip Validation also / validate only CVV
   $('.saved-cards .card-listing').on('click', 'li', function(e) {
            var el = $(e.currentTarget);
            if (el.hasClass('expired')) { return; }
            if(el.find('.card-select').hasClass('card-selected')){return;} //Clicking on selected card again should do nothing
            el.closest('.card-listing').find('.card-select').removeClass('card-selected');
            // Clear cvv numbers input  
            el.closest('.card-listing').find('input[name="saved_cvv_code"]').val('');
            el.find('.card-select').addClass('card-selected');
            var indexClicked = el.closest('ul li').index();
            $('.saved-cards .cc-bill-address ul li:eq('+indexClicked+')').show().siblings().hide();
        }.bind(this));

	/*$('.card-listing .card-select ').on('click',function(){
		$(this).closest('ul').find('.card-select').removeClass('card-selected');
		$(this).addClass('card-selected');
		var indexClicked = $(this).closest('ul li').index();
		$('.saved-cards .cc-bill-address ul li:eq('+indexClicked+')').show().siblings().hide();
		
	});*/
	

	$(document).on('express.payment.submit',function(e,data){
		//Use this for validating the used card and the normal one 
		var elem = $('#paymentFormToSubmit').val();
    	var form = $('#'+elem);
		
	    if(!data.hasAddressId){ //set address id if no saved address
	    	form.find('input[name="address"]').val(data.data.id);	
	    }
	    
	   	return validateAndSubmitPaymentForm(data.validate);
	});

});
//block for pincode widget

Myntra.PaymentUIObj = {
        //check for serviceablity in this block
        init:function(){
        	 this._initialized = true;
        	 this.root = $('.exp-payment-cont');
        	 this.defPayBlock = this.root.find('.pay-block-default');
        	 this.morePayBlock = this.root.find('.pay-block-tab');
        	 this.moreBlockTabs = this.root.find('.tabs ul');
        	 this.moreTabContent = this.root.find('.tab-content');
        	 this.defPayBlockMsgCont = $('<div class="mk-hide def--err-msg"><p></p></div>').appendTo(this.defPayBlock);
        	 this.morePayBlockMsgCont =$('<div class="mk-hide more-options-msg"><p></p></div>').appendTo(this.morePayBlock);
        	 this.errMsg = ''; 
        	 this.morePayBlock
        	 this.moreOptionsBtn = this.root.find('.more-pay-options');

        },
        ajax : function(postData){
        	!this._initialized && this.init();
        	var ajaxUrl = (location.protocol === 'https:' ? 's_' : '') + 'ajaxPincodeWidget.php?styleid=' + postData.styleid + '&skuid=' + postData.skuid + '&gotCod=1&pincode=' +postData.pincode  ;
        	var that=this;
            $.ajax({
                type:'GET',
                url: ajaxUrl,
                success:function(data){
	                if(data.status=='SUCCESS'){
   	               		//data={"status":"SUCCESS","data":{"a":["cod","on"],"DELIVERY_PROMISE_TIME":"5-7","pincode":"671315"}}
        	          	//if not serviceable then hide the COD part or show the COD not available message
            	      	//if default is one-click-cod then append a div and hide the form, switch based on the pincode 
            	      	handleMoreOnly =that.root.find('.more-pay-options').length?false:true;
            	      	that.handlePayBlockBasedOnPincode(data.data,handleMoreOnly);//passing only the actual data //tabs Only to check if the user clicked more options
                	}
                },
     	        error : function(){
                    return false;
                },
                complete : function(){
                }
            });
        },

        handlePayBlockBasedOnPincode : function(data,handleMoreOnly){
        	if (!data.SERVICEABLE.length){
        		if(handleMoreOnly){
        			this.setTabBlockServiceable(false);
        		}
        		else {
        			this.setDefaultBlockServiceable(false);	
        		}
        		return false;
        	}
        	else {
				//if(data.pincode==111111){this.setDefaultBlockServiceable(true);return;}
				if(handleMoreOnly){
        			this.setTabBlockServiceable(true);
        			this.setTabsForPincode(data);
        		}
        		else {
        			this.setDefaultBlockServiceable(true);
        	   		if(this.defPayBlock.find('#one-click-cod').length){
	        			this.defaultCODhandle(data);
    	    		}else if (this.defPayBlock.find('#one-click-credit_card').length){
        				this.defaultCreditCardhandle(data);
        			}
        		}

        	}
        },

        defaultCODhandle : function(data){
        	this.errMsg='';
        	//check if COD available
        	if($.inArray('cod', data.SERVICEABLE) > -1) {
        		this.errMsg='';
        		this.defPayBlockMsgCont.find('p').text(this.errMsg);
        		this.defPayBlockMsgCont.hide().siblings('form').show();
        		//Check for online
        		this.moreBlockTabs.find('#pay_by_cod').show();
        		if ($.inArray('on', data.SERVICEABLE) > -1) {
        			//show more payment options
        			this.moreOptionsBtn.show();
        		}
        		else {
        			this.moreOptionsBtn.hide();
        		}

        	}
        	else {//show COD error message an
        		this.errMsg='Cash On Delivery not available for this pincode. Please use other options';
        		//hide COD and the tab COD /and show the message and more options 
        		this.defPayBlockMsgCont.find('p').text(this.errMsg);
        		this.defPayBlockMsgCont.show().siblings('form').hide();
        		//hide the COD tab and initPaymentOptionTabClick() on credit_card
        		//#pay_by_cc
        		this.moreBlockTabs.find('#pay_by_cc').trigger('click');
			$('#pay_by_cc').click();
        		this.moreBlockTabs.find('#pay_by_cod').hide();
        	}
        	
        },

        defaultCreditCardhandle : function(data){
        	this.errMsg='';
        	if ($.inArray('on', data.SERVICEABLE) > -1) {
        		this.errMsg='';
        		this.defPayBlockMsgCont.find('p').text(this.errMsg);
        		this.defPayBlockMsgCont.hide().siblings('form').show();
        		//Check for online
        		this.moreBlockTabs.find('#pay_by_cod').siblings().show();
        		if ($.inArray('cod', data.SERVICEABLE) > -1) {
        			this.moreBlockTabs.find('#pay_by_cod').hide();
        		}
        		else {
        			//CODnot avaiable
        			this.moreBlockTabs.find('#pay_by_cc').trigger('click');
				$('#pay_by_cc').click();
        			this.moreBlockTabs.find('#pay_by_cod').hide();
        		}

        	}
        	else {//show online error message an
        		this.errMsg='Online payment is not available for this pincode. Please use other options';
        		//hide COD and the tab COD /and show the message and more options 
        		this.defPayBlockMsgCont.find('p').text(this.errMsg);
        		this.defPayBlockMsgCont.show().siblings('form').hide();
        		//hide the COD tab and initPaymentOptionTabClick() on credit_card
        		//#pay_by_cc
        		this.moreBlockTabs.find('#pay_by_cod').trigger('click');
			$('#pay_by_cod').click();
        		this.moreBlockTabs.find('#pay_by_cod').siblings().hide();
        	}
        },
        setDefaultBlockServiceable : function(serviceable){
        	if(serviceable){
        		//show more options link and pay block if hidden
        		this.errMsg='';
        		this.defPayBlockMsgCont.find('p').text('');
        		this.defPayBlockMsgCont.hide().siblings().show();

        	}
        	else {
        		this.errMsg='Unfortunately, we do not ship to your PIN Code!';
        		this.defPayBlockMsgCont.find('p').text(this.errMsg);
        		//hide more options link and show a message
        		this.defPayBlockMsgCont.show().siblings().hide();
        	}
        	
        	//hide more options link and show a message
        },
        setTabBlockServiceable : function(serviceable){//Pincode not servic
        	if(serviceable){
        		//show more options link and pay block if hidden
        		this.errMsg='';
        		this.morePayBlockMsgCont.find('p').text('');
        		this.morePayBlockMsgCont.hide().siblings().show();
        		
        	}
        	else {
        		this.errMsg='Unfortunately, we do not ship to your PIN Code!';
        		this.morePayBlockMsgCont.find('p').text(this.errMsg);
        		//hide more options link and show a message
        		this.morePayBlockMsgCont.show().siblings().hide();
        	}
        	
        },
        setTabsForPincode : function(data){

        	if($.inArray('cod', data.SERVICEABLE) > -1) {
        		this.moreBlockTabs.find('#pay_by_cod').show();
        		if ($.inArray('on', data.SERVICEABLE) > -1){
        			this.moreBlockTabs.find('#pay_by_cod').siblings().show();
        		}
        		else{
        			this.moreBlockTabs.find('#pay_by_cod').trigger('click');
				$('#pay_by_cod').click();
        			this.moreBlockTabs.find('#pay_by_cod').siblings().hide();
        		}
        	}
        	else {
        		this.moreBlockTabs.find('#pay_by_cc').trigger('click');
			$('#pay_by_cc').click();
        		this.moreBlockTabs.find('#pay_by_cod').hide().siblings().show();
			}
        },
        fillCountries : function(countriesJSON){
        //iterate through the select button and add to it.
        var  opts = [];
        for(key in countriesJSON){
            opts.push('<option value="' + key + '"'+ ((key=="IN")?"selected":"") +'>' + countriesJSON[key] + '</option>');
        }
        $('select[name="b_country"]').html(opts.join(''));
    }
}
