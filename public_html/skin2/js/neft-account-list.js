
Myntra.NeftAccountListBox = function(id, cfg) {
    cfg = cfg || {};
    cfg.isPopup = (typeof cfg.isPopup === 'undefined') ? true : cfg.isPopup;
    cfg.isModal = (typeof cfg.isModal === 'undefined') ? true : cfg.isModal;

    cfg.css = cfg.css || '';
    cfg.css += cfg.isPopup ? ' lightbox' : 'lightbox-nopop';

    var isInited = false, selectedNeftAccount, neft_accounts = {},
        idstr = id.substr(0, 1) === '#' ? id.substr(1) : id,
        obj,
        markup = [
        '<div id="' + idstr + '" class="address-list-box ' + cfg.css + '">',
        '<div class="mod">',
        '  <div class="hd">',
        '    <div class="title"></div>',
        '    <div class="subtitle"></div>',
        '  </div>',
        '  <div class="bd">',
        '    <button class="link-btn btn-create"></button>',
        '    <div class="list"></div>',
        '  </div>',
        '  <div class="ft">',
        '    <span class="note"></span>',
        '    <button class="btn normal-btn btn-save">Done</button>',
        '    <button class="btn normal-btn btn-cancel">Cancel</button>',
        '  </div>',
        '</div>',
        '</div>'
    ].join('');
    cfg.container ? $(cfg.container).html(markup) : $('body').append(markup);
    obj = Myntra.LightBox(id, cfg);
    obj.root = $(id);
    obj.set('ajax_url', '/myntra/' + (location.protocol === 'https:' ? 's_' : '') + 'neft_account.php');

    var hd = $('.mod > .hd', obj.root),
        bd = $('.mod > .bd', obj.root),
        ft = $('.mod > .ft', obj.root),
        title = $('.mod > .hd > .title', obj.root),
        subTitle = $('.mod > .hd > .subtitle', obj.root),
        ftNote = $('.mod > .ft > .note', obj.root),
        btnSave = $('.btn-save', obj.root),
        btnCancel = $('.btn-cancel', obj.root),
        btnCreate = $('.btn-create', obj.root),
        list = $('.mod > .bd > .list', obj.root),
        items;

    obj.setTitle = function(markup) { title.html(markup) };
    obj.setSubTitle = function(markup) { subTitle.html(markup) };
    obj.setBody = function(markup) { bd.html(markup) };
    obj.setFootNote = function(markup) { ftNote.html(markup) };

    obj.enableSave = function() { btnSave.removeAttr('disabled') };
    obj.disableSave = function() { btnSave.attr('disabled', 'disabled') };
    obj.showSave = function() { btnSave.removeClass('hide') };
    obj.hideSave = function() { btnSave.addClass('hide') };
    obj.setSaveText = function(txt) { btnSave.text(txt) };

    if (!cfg.isPopup) {
        hd.remove();
        ft.remove();
        btnCreate.remove();
    }

    obj.buildAddrMarkup = function(neft_account) {
        var tpl = [
        '   <td class="address">',
        '       <strong>{accountName}</strong><br>',        
        '       <span class="lbl">Account Number:</span> {accountNumber}<br>',
        '       <span class="lbl">Type:</span> {accountType}<br>',
        '       <span class="lbl">Bank:</span> {bankName}<br>',
        '       <span class="lbl">Branch:</span> {branch}<br>',
        '       <span class="lbl">IFSC:</span> {ifscCode}<br>',        
        '   </td>'
        ].join('');

        function htmlEntities(str) {
            return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        }
        var markup = tpl.replace(/\{(.*?)\}/g, function(match, s1) {
            if (s1 == 'branch') {
                return htmlEntities(neft_account[s1]).replace('\n', '<br>');
            }
            else if (s1 == 'bank') {
                return htmlEntities(neft_account[s1])? (htmlEntities(neft_account[s1])+'<br>') : '';
            }
            return htmlEntities(neft_account[s1]);
        });

        return markup;
    }

    obj.buildItem = function(neft_account, css) {
        var actMarkup = '';

        if (cfg._editdelete) {
            actMarkup = [
            '<td class="action">',
            '   <span class="buttons">',                
            '   <button class="btn link-btn btn-edit">Edit</button>',
            '   <span class="sep">/</span>',
            '   <button class="btn link-btn btn-delete">Remove</button>',
            '   </span>',
            '</td>'
            ].join('');
        }

        var addrMarkup = obj.buildAddrMarkup(neft_account),            
            markup = [
            '<tr data-id="' + neft_account.neftAccountId + '" class="', css, '">',
            '   <td class="opt">',
            '       <input type="radio" name="address-sel" value="' + neft_account.neftAccountId + '">',
            '   </td>',
                addrMarkup,
                actMarkup,
            '</tr>'
            ].join('');

        /*if (neft_accounts.is_servicable && neft_accounts.id == selAddrId) {
            console.log("in neft account list build item: list select");
            $(document).trigger('myntra.address.list.select', neft_accounts);
        }*/

        return markup;
    };


    obj.buildList = function(ids) {
        var markups = [], count = 0, css,
            i, j, n;            

        list.html('');
        items = $('<table class="items"></table>').appendTo(list);
        for (j = 0, n = ids.length; j < n; j += 1) {
            i = ids[j];
            count += 1;
            css = count % 2 == 0 ? 'odd' : 'even';
            markups.push(obj.buildItem(neft_accounts[i], css));
        }
        items.html(markups.join(''));
        $('tr', items).click(function(e) { obj.onItemClick(e) });

        items.jqTransform();

        obj.setSubTitle('&nbsp;');
        obj.showSave();
    };


    btnCreate.click(function(e) {        
        var conf = {_action:'create', isModal:true};
        $(document).trigger('myntra.neft.account.form.show', conf);
    });


    btnSave.click(function(e) {
        obj.hide();    
        $(document).trigger('myntra.neft.account.list.done', selectedNeftAccount);    
    });


    btnCancel.click(function(e) {
        obj.hide();
    });


    $(document).bind('myntra.neft.account.form.done', function(e, resp) {
        if (resp._action == 'create') {
            neft_accounts[resp.data.neftAccountId] = resp.data;
            items ? obj.addItem(resp.data) : obj.buildList([resp.data.neftAccountId]);                        
            obj.hide();
            $(document).trigger('myntra.neft.account.list.done', resp.data);            
        }
        else if (resp._action == 'edit') {
            obj.updateItem(resp.data);
        }
    });


    // adds the newly added neft account on top of the list
    obj.addItem = function(data) {
        var firstItem = items.find('tr').first(),
            css = firstItem.hasClass('odd') ? 'even' : 'odd',
            markup = obj.buildItem(data, css),
            item = $(markup).insertBefore(firstItem);

        item.jqTransform();
        // note: only serviceable address can be added. so, we can safely show the save button
        obj.showSave();
        obj.setSubTitle('&nbsp;');
        item.click(function(e){ obj.onItemClick(e) });
    };


    obj.updateItem = function(data) {
        var markup = obj.buildAddrMarkup(data),
            item = items.find('tr[data-id="' + data.id + '"] > td.address');
        item.replaceWith(markup);
    };

    obj.deleteItem = function(neftAccountId) {
        var item = items.find('tr[data-id="' + neftAccountId + '"]');
        item.animate({opacity:0}, 600, function() {
            // store the pointer to the previous item
            var pitem = item.prev();

            item.remove();
            if (selectedNeftAccount && selectedNeftAccount.neftAccountId == neftAccountId) {
                selectedNeftAccount = null;
                obj.disableSave();
            }

            // flip the zebra bg for the rest of items
            if (pitem) {
                var cnt = 0;
                while (pitem = pitem.next()) {
                    pitem.hasClass('odd')
                        ? pitem.removeClass('odd').addClass('even')
                        : pitem.removeClass('even').addClass('odd');
                    // to avoid getting into infinite loop
                    cnt += 1;
                    if (cnt > 100) {
                        break;
                    }
                }
            }
        });
    };
    

    obj.onItemClick = function(e) {
        var neftAccountId = $(e.currentTarget).attr('data-id'),
            target = $(e.target);

        switch (e.target.nodeName.toLowerCase()) {            
            case 'button':
                if (target.hasClass('btn-edit')) {
                    var conf = {_type:cfg._type, _action:'edit', id:neftAccountId};
                    $(document).trigger('myntra.neft.account.form.show', conf);
                }
                else if (target.hasClass('btn-delete')) {
                    var params = {_view:'address-delete', id:neftAccountId, _token:Myntra.Data.token};
                    obj.ajax(params, 'delete', 'POST');
                }                
                break;

            case 'a':                
                if (target.hasClass('jqTransformRadio') && !target.siblings('input:radio').is(':disabled')) {
                    selectedNeftAccount = neft_accounts[neftAccountId];
                    obj.enableSave();
                    $(document).trigger('myntra.address.list.select', selectedNeftAccount);
                }
                break;

            default:
                $(target).siblings('.opt').find('a.jqTransformRadio').click();
                break;
        }
    };

    obj.onAjaxSuccess = function(resp, name) {
        if (name === 'init') {
            isInited = true;
            obj.disableSave();
            btnCreate.text('Create a neft account');
            
            //obj.setFootNote('Need help? ' + resp.customerSupportTime + ' on <em>' + resp.customerSupportCall + '</em>');
            obj.setFootNote('');
            
            if (resp.count > 0) {
                neft_accounts = resp.neft_accounts;                
                obj.buildList(resp.ids);
                obj.center();

            } else {
                list.html('<div class="no-data">There is no account detail, please add one by clicking the link above.</div>');
            }
            
        } else if (name === 'delete') {
            obj.deleteItem(resp.id);
        }        
    };

    var p_show = obj.show;
    obj.show = function(a_cfg) {
        $.extend(cfg, a_cfg);        
        obj.setTitle('Select account for '+ cfg.displayName);
        obj.setSubTitle('&nbsp;');
        p_show.apply(obj, arguments);
        
        var params = {_view:'neft-account-list'};        
        btnCreate.text('Create NEFT account');        

        !isInited && obj.ajax(params, 'init');
    };
    
    return obj;
};

$(document).ready(function() {
    var obj;
    $(document).bind('myntra.neft.account.list.show', function(e, cfg) {    
        obj = obj || Myntra.NeftAccountListBox('#lb-neft-account-list', cfg);
            obj.show(cfg);
    });

    
});

