

Myntra.InitNewRegLogin = function(id) {
    if (!$(id).length) { return; }
    var cfg = {isPopup:false,isModal:true};

    var obj = Myntra.LoginBox(id, cfg);
    //$('.gender-sel').parent().jqTransform();
        var frmLogin = $(id + ' .frm-login'),
        frmSignup = $(id + ' .frm-signup'),
        loginUserErr = frmLogin.find('.err-user'),
        loginPassErr = frmLogin.find('.err-pass'),
        signupUserErr = frmSignup.find('.err-user'),
        signupPassErr = frmSignup.find('.err-pass'),
        signupMobileErr = frmSignup.find('.err-mobile'),
        signupGenderErr = frmSignup.find('.err-gender'),
        signupNameErr = frmSignup.find('.err-name'),
        signupBirthYearErr = frmSignup.find('.err-birthyear');
        obj.setInvokeType('newRegister');


    var ckLoginErr = decodeURIComponent(Myntra.Utils.getQueryParam('msg'));

    if (ckLoginErr) {
         var data = {},
                kvPairs = ckLoginErr.split(';'),
                errs, err, i, n, kv;
            
            for (i = 0, n = kvPairs.length; i < n; i += 1) {
                kv = kvPairs[i].split('=');
                data[kv[0]] = kv[1];
        }
        if (data.status === 'data-error') {
            _gaq.push(['_trackEvent', 'newRegister', data.type, 'data_error']);

            errs = data.errors.split('~');
            for (i = 0, n = errs.length; i < n; i += 1) {
                err = errs[i];
                if (err === 'email') {
                    val = 'Email address is invalid';
                    data.type === 'signup' ? signupUserErr.html(val) : loginUserErr.html(val);
                }
                if (err === 'pass') {
                    val = 'Please enter your password';
                    data.type === 'signup' ? signupPassErr.html(val) : loginPassErr.html(val);
                }
                if (err === 'mobile') {
                    val = 'Mobile number is invalid';
                    data.type === 'signup' ? signupMobileErr.html(val) : loginMobileErr.html(val);
                }
            }
            if(data.type === 'signup'){
                obj.showSignup();
            }else{
                obj.showLogin();
            }
        }
       
        else if (data.status === 'error') {
            
            if (data.errorcode === '1') {
                if (data.type === 'signup') {
                    _gaq.push(['_trackEvent','newRegister', data.type,'failure_username_already_exists']);
                    obj.showSignup();
                    signupUserErr.html('Email Address already exists.');
                }
                else if (data.type === 'signin') {
                    _gaq.push(['_trackEvent','newRegister', data.type, 'failure_password_invalid']);
                    obj.showLogin();
                    loginUserErr.html('Email Address and Password do not match.');


                }
            }
            else if (data.errorcode === '2') {
                if (data.type === 'signin') {
                     _gaq.push(['_trackEvent','newRegister', data.type, 'failure_login_not_found']);
                     obj.showLogin();
                    loginUserErr.html('Email Address and Password do not match.');
                }
            }
            else if(data.errorcode === '3') {
                if (data.type === 'signin') {
                    _gaq.push(['_trackEvent','newRegister', data.type, 'failure_suspended_user']);
                    obj.showLogin();
                    loginUserErr.html('Email Address and Password do not match.');
                }
            }
        }
        else if(data.status==='timeout'){
            if (data.type === 'signup') {
                _gaq.push(['_trackEvent','newRegister','signup', 'timeout']);
                data.action = 'signup';
                data.message = 'Something went wrong. Please signup again';
                obj.showSignup();
            }else{
                _gaq.push(['_trackEvent','newRegister','signin', 'timeout']);
                data.action = 'signin';
                data.message = 'Something went wrong. Please signin again';
                obj.showLogin();
            }
            obj.showErrMsg(data);

        }
        Myntra.Utils.Cookie.del('_loginerr');
     
    }

    return true;
};

$(document).ready(function() {
    if($('#newreg-login').length){
        Myntra.InitNewRegLogin('#newreg-login');
    }
});

