/*var liTemplate = '<li class="mk-product mk-product-style-details <%= mod_val %>" data-id="prod_<%= styleid %>" id="prod_<%= styleid>">'+        
        '<span class="quick-look " data-widget="search_results" data-styleid="<%= styleid >" data-href="<%= landingpageurl>"></span>'+
        '<a style="display:block;" class="clearfix" href="<%= landingpageurl>">'+
        '<div class="mk-prod-img">'+
        '<img <% if (index > 4) %>}class="jquery-lazyload" original="{myntraimage key="style_180_240" src=$widgetdata.search_image}" {/if} src="{if $smarty.foreach.widgetdataloop.index gt 4}{$cdn_base/skin2/images/loader_180x240.gif{else}{myntraimage key="style_180_240" src=$widgetdata.search_image}{/if}" alt="{$widgetdata.product}">'+
            '</div>'+
            '<div class="mk-prod-info">'+
                '<% if (widgetdata.generic_type != "design") {%>'+
                    '<div class="availability" data-sizes="<%= sizes %>">'+
                        '<div class="tooltip-arrow"><div class="tooltip-arrow-inner"></div></div>'+
                        '<div class="tooltip-content"></div>'+
                    '</div>'+
                '<% } %>'+
                '<span class="mk-prod-name"><span class="mk-prod-brand-name"><%= brands_filter_facet %></span> <%= product %></span>'+
                '<span class="mk-prod-price red"><%= prodPrice %></span>'+
            '</div>'+
        '</a>'+
       '<span class="combo-flag hideComboFlag"></span>  '+
    '</li>';
*/
function createDataProvider() {
     var len = uiParams.data.length;
     var retVal=[];
     for (var x=0; x< len;x++){
        if (x%5==0){
            var curVal = [];
            retVal[x/5]=curVal;
        }
        curVal.push(uiParams.data[x]);

     }

     return retVal;
}
var curDataRecvd ;
function listItemLabelFunction( item ) {
     var curStr = "";
     for (var i=0;i <item.length;i++){
        curStr +='<span class = "mk-product mk-product-style-details" style="display:inline-block"><span class="quick-look " data-widget="search_results" data-styleid="'+item[i].styleid+'" data-href="'+item[i].landingpageurl+'"></span>';
        curStr += '<a class="clearfix" style="display:inline-block" href="'+item[i].landingpageurl+'">';
        curStr += "<div class='mk-prod-img'><img src='"+item[i].search_image+"' alt='"+item[i].product +"'/></div></a>";

        var discPrice = "";
        if (item[i].discount>0){
            discPrice = item[i].discounted_price+'<span class="strike gray">'+item[i].price+'</span>';
        }else{
            discPrice = item[i].price;
        }
        
        var price = ' <span class="mk-prod-price red">Rs'+discPrice+'</span>';
        //price += '<span class="mk-discount red">'+item[i].discount_label+'</span>';
        if (item[i].sizes != '' && item[i].sizes != ' ')
            var sizeData = "Sizes: "+ item[i].sizes.replace(",",", ");
        else
            var sizeData = "SOLD OUT";
        
        var completeSizeDom= '<div class="availability" data-sizes="'+sizeData+'" >'+
                    '<div class="tooltip-arrow"><div class="tooltip-arrow-inner"></div></div>'+
                    '<div class="tooltip-content"></div>'+
                    '</div>';
        curStr += '<div class="mk-prod-info">'+completeSizeDom;
        curStr += '<span class="mk-prod-name">' +item[i].product.substring(0,53)+'</span>';
        curStr += price;
        curStr += '</div>';
        curStr += '<span class="combo-flag"></span>';
        curStr += '</span>';
        
     }
     var curIndex = uiParams.data.indexOf(item[4]);
     //console.log(item[4]);
     //console.log(curIndex);
     //console.log((curIndex>curPage*pageSize) && (curIndex<(curPage+1)*pageSize));
     if ((curIndex>curPage*pageSize)&&!finalPageReached  ){
	
        $.get(url,{}).done(function(data){
             var dataObj = jQuery.parseJSON(data);
                    if (dataObj.status == "success")
                        dataObj= dataObj.data;
                    else
                        dataObj = null;
                if (dataObj.data != null){
                        curDataRecvd = dataObj.data;
                        for (var i = 0 ; i < dataObj.data.length;i++){
                                uiParams.data[(pageSize)*(curPage)+i]=dataObj.data[i];
                            }
                       url = baseUrl+cid+"&mini=1&all=true&page="+(curPage+1);
                }
                else
                        finalPageReached=true;
        });
     curPage++; 
     }
     return curStr;
}

var curPage=0;
var finalPageReached=false;
var pageSize= 1000;
var baseUrl = "/comboController.php?cid=";
var url;
$(document).ready(function(){
    url = "/comboController.php?cid="+cid+"&mini=1&all=true&page=1";
    $.get('comboController.php',{cid:cid,mini:1,all:true,page:0}).done(function(data) {
        var dataObj = jQuery.parseJSON(data);
        if (dataObj.status == "success")
            dataObj= dataObj.data;
        else
            dataObj = null;
        uiParams = dataObj;
        for (var i = uiParams.data.length;i<uiParams.relatedProdNum;i++)
            uiParams.data.push(0);
        curPage = 0;
        if (uiParams.length < pageSize)
            finalPageReached = true;

        $('#mk-combo-grid').megalist();
	$('#mk-combo-grid').megalist('setLabelFunction', listItemLabelFunction );
        $('#mk-combo-grid').megalist('setDataProvider', createDataProvider() );
    });
});



//CREATE  SELF INVOKING FUNCTION FOR COMBO WIDGET
(function() { "use strict";

var comboItem = [
'<li class="rs-carousel-item">',
'  <span href="javascript:void(0)" data-href="'+http_loc+'/{landingpageurl}" data-styleid="{styleid}" data-widget="header_saved" class="quick-look" style="display: none;"></span>',
'   <a href="'+http_loc+'/{landingpageurl}" title="{stylename}">',
'   	<div class="mk-product-image">',
'         <img alt="{stylename}" src="{search_image}">',
'       </div>',
'   </a>',
'   <p class="title-text">{stylename}</p>',
'   <p class="price-text red"> <span class="rupees"> {price} </span></p>',
'   <p class="price-text"><label>SIZE:</label>{sizenameunified}<label>/ QTY:</label>{quantity}</p>',
'   <span class="remove" data-styleid="{styleid}" data-itemid="{itemid}" data-tooltip="remove from bag and combo"></span>',
'	<span class="combo-plus"></span>',
'</li>'
].join('');

var comboItemHolderSingle = '<li class="rs-carousel-item combo-item-dummy-single"><div></div></li>';
var comboItemHolder = '<li class="rs-carousel-item combo-item-dummy"><div></div><span class="combo-plus"></span></li>';

Myntra.ComboPageWidget =  { 
	init: function(){
		this.myntraCombo = {};
		this.root = $('#mk-combo-box');
		this.input=this.root.find('input,button');
		this.gotoCart=this.root.find('#goto-bag');
		this.statMsg=this.root.find('.status-msg');
		this.successMsg=this.root.find('.success-msg');
		this.miniDetails=this.root.find('.combo-mini-details');
		this.comboProgress=this.root.find('.combo-progress');
		this.comboSummary=this.root.find('.combo-summary');
		this.comboItems=this.root.find('.combo-items ul');
		this.comboItemOuter=this.root.find('.combo-items-outer');
		this.comboItemCont=this.root.find('.combo-items');
		this.boxControl = this.root.find('.box-control').on('click', $.proxy(this.toggleBox, this));
		this.comboItems.on('click','li .remove',$.proxy(this.removeItem, this));
		this.progressStatus = 0;
		this.minMoreMessage = this.root.find('.min-more-message');
    	this._initialized = true;
    	this.carouselCount = 5;
    	this.dummyCount = 0;	
    	this.styelObj = {};
    	this.myntraCombo.removeItemId ='' ;
    	this.myntraCombo.removeStyleId = '';
    	this.carouselPoniter ='';

    },

	setData: function(myntraCombo){
		!this._initialized && this.init();
		this.myntraCombo = myntraCombo;
		this.setProgress();
		if(this.myntraCombo.freeItem.name){
			this.carouselCount = 4;
			this.handleFreeItem();
		}
		this.renderItems();
		this.setMinMoreMessage();

	},

	renderItems: function() {
		//construct the html and add to UL
		var data, styleId, markup, item,dispData,sku,skuData,markupFinal = [],itemIdData;
		for (styleId in this.myntraCombo.products) {
			dispData = this.myntraCombo.solrProducts[styleId];
			skuData= this.myntraCombo.products[styleId];

			for(sku in skuData){
				itemIdData= this.myntraCombo.selectedStyleProductMap[sku];
				data = skuData[sku];
				markup = comboItem.replace(/\{(.*?)\}/g, function(str, key) {
	                if(key in dispData) {
	                	if(key === 'price' && (data.discountAmount == data.productPrice)){
	                		return '<em>FREE</em>';
	                	}else if(key === 'price'){
	                		return 'Rs.'+Myntra.Utils.formatNumber(dispData[key]);
	                	}else{
	                		return dispData[key];	
	                	}
	                	
	                }
	                return str;
	            }).replace(/\{(.*?)\}/g, function(str, key) {
	                if(key in data) {
	                	return data[key];
	                }
	                return str;
	            }).replace(/\{(.*?)\}/g, function(str, key) {
	                if(key in itemIdData) {
	                	return itemIdData[key];
	                }
	                return str;
	            }).replace(/180_240/g,'96_128_mini').replace(/style_search_image/g,'properties');
	            markupFinal.push(markup);
			}		
		}
		if(this.dummyCount){
			if(this.myntraCombo.isAmount){
				markupFinal.push(comboItemHolderSingle);
			}else{
				for(var i = 0; i < this.dummyCount ;i++){
					markupFinal.push(comboItemHolder);	
				}
			}		
		}

		this.comboItems.html(markupFinal.join(''));
		this.comboItems.find('li:last .combo-plus').remove();
		if(this.comboItems.find('li').length > this.carouselCount) {
			this.initCarousel();
		}
		else if(this.carouselPoniter.length){
			this.carouselPoniter.carousel('destroy');
		}

		
	},
	
	setProgress : function(){
		if(this.myntraCombo.isAmount){
			this.setProgressAmount();
		}else{
			this.setProgressCount();
		}


	},

	setProgressAmount : function(){
		var progressHTML='',miniHTML='',_width=80;
		this.dummyCount = 0;
		if(typeof this.myntraCombo.productsSet.dre_minMore == 'undefined' || this.myntraCombo.productsSet.dre_minMore > 0){
			if(!this.myntraCombo.productsSet.dre_minMore){
				_width=0;
			}else{
				var _percent = this.myntraCombo.min/(this.myntraCombo.min - this.myntraCombo.productsSet.dre_minMore);	
				if(_percent){
					_width = _width/_percent;	
				}
			}
			progressHTML = '<div class="single-block"><span style="width:'+_width+'px"></span></div>';
			this.dummyCount = 1;
		}
		else {
			progressHTML = '<span class="complete"><em></em>Combo Total</span>';
			
		}
		miniHTML = '<span class="price">Rs.'+Myntra.Utils.formatNumber((this.myntraCombo.cartTotal-this.myntraCombo.cartSavings))+'</span> / <span class="total">'+this.myntraCombo.selectedStyles.length+' item'+((this.myntraCombo.selectedStyles.length>1)?'s':'')+'</span>';
		this.comboProgress.html(progressHTML);
		this.miniDetails.html(miniHTML);
		//Also upddtae the dummy image section. single which shows multilayer
	},

	setProgressCount : function(){
		var progressHTML='',miniHTML='' ,_count = this.myntraCombo.min,blocks = [];
		this.dummyCount = 0;
		if(this.myntraCombo.productsSet.dre_minMore > 0 || typeof this.myntraCombo.productsSet.dre_minMore == 'undefined'){
			//count is the nummber of green blocks
			_count=this.myntraCombo.min - this.myntraCombo.productsSet.dre_minMore;
			for(var i = 0; i < this.myntraCombo.min ;i++){
				if(i < _count){
					blocks.push('<span class="small-box green"></span>');	
				}
				else{
					this.dummyCount = this.dummyCount+1;
					blocks.push('<span class="small-box"></span>');	
				}
			}
			
		}
		else {
			blocks.push('<span class="complete"><em></em>Combo Total</span>');
			
		}
		progressHTML = blocks.join('');
		miniHTML = '<span class="total">'+this.myntraCombo.selectedStyles.length+' item'+((this.myntraCombo.selectedStyles.length>1)?'s':'')+'</span> / <span class="price">Rs.'+Myntra.Utils.formatNumber((this.myntraCombo.cartTotal-this.myntraCombo.cartSavings))+'</span>';
		this.comboProgress.html(progressHTML);
		this.miniDetails.html(miniHTML);
		//Also upddtae the dummy image section.. number of products
	},

	update : function(ajaxResp){
		//use this to call carosel reinit
		//console.log(ajaxResp);
		this.styelObj = ajaxResp.styleObj;//Added or removed Styleid
		this.myntraCombo.solrProducts = ajaxResp.dataMap;
		this.myntraCombo.products = ajaxResp.products;
		this.myntraCombo.selectedStyles =  ajaxResp.selectedStyles;
		this.myntraCombo.isConditionMet = ajaxResp.isConditionMet;
	    this.myntraCombo.min = parseInt(ajaxResp.min+0.5);
	    this.myntraCombo.cartSavings = parseInt(ajaxResp.comboSavings+0.5);
	    this.myntraCombo.cartTotal = parseInt(ajaxResp.comboTotal+0.5);
	    this.myntraCombo.discountedCount = parseInt(ajaxResp.productsSet.dre_count);
	    this.myntraCombo.selectedStyleProductMap = ajaxResp.selectedStyleProductMap;
	    this.myntraCombo.itemName = ajaxResp.dre_itemName;
	    this.myntraCombo.itemPrice = ajaxResp.dre_itemPrice;
	    this.myntraCombo.comboTotal = ajaxResp.comboTotal;
	    this.myntraCombo.min = ajaxResp.min;
	    this.myntraCombo.productsSet = ajaxResp.productsSet;
	    if(ajaxResp.itemName != undefined && ajaxResp.itemPrice != undefined){
	        this.myntraCombo.freeItem = { name : ajaxResp.itemName , price : ajaxResp.itemPrice};
	    } 
	    this.setProgress();
		this.renderItems();
		this.setMinMoreMessage();
		$(document).trigger('myntra.combowidget.update.done');
	
	},

	initCarousel : function(){
		//reinitialize the carousel
		this.carouselPoniter = this.comboItemCont.carousel({
    			itemsPerPage: this.carouselCount, // number of items to show on each page
    			itemsPerTransition: 1, // number of items moved with each transition
    			nextPrevActions: true, // whether next and prev links will be included
    			pagination: false, // whether pagination links will be included
    			onHover: true // whether hover will act like click for next/prev
    	});

	},

	toggleBox : function(e){
		var target = $(e.target),
        el = $(e.currentTarget);
        el.toggleClass('close');
        this.comboItemOuter.slideToggle('slow');
	},

	removeItem : function(e){
		var target = $(e.target),
        el = $(e.currentTarget);
        this.myntraCombo.removeItemId = el.data('itemid');
        this.myntraCombo.removeStyleId = el.data('styleid');
		var that=this;
		$.ajax({
			type:"POST",
			url:http_loc+"/modifycart.php",
			data :"itemid="+that.myntraCombo.removeItemId+"&_token="+Myntra.Data.token+"&comboProduct=1&removecombo=1",
			beforeSend:function(){
				// /show loader in combo box
				if($('.combo-box-overlay').length){
					$('.combo-box-overlay').show();
				}else{
					$('<div class="combo-box-overlay"><img style="margin-top:100px" src="http://myntra.myntassets.com/skin2/images/loader_150x200.gif" /></div>').css({'position':'fixed','left':'0','top':'0','height':'100%','width':'100%','overflow':'hidden','background':'#fff','opacity':'.6','text-align':'center'}).appendTo('.combo-items-outer');
				}
				
			},
			success: function(ret){
				$(document).trigger('myntra.removedfromcombo.getcombostatus',{cid:that.myntraCombo.ComboId,sid:that.myntraCombo.removeStyleId,resp:$.parseJSON(ret)});
			},
			complete:function(){
				
			}

		});

	},
	handleFreeItem : function(){
		var _img ='';
		if(this.myntraCombo.productsSet.dre_freeItems[0] && this.myntraCombo.freeItemDetails && this.myntraCombo.freeItemDetails[this.myntraCombo.productsSet.dre_freeItems[0]]){
			 _img = this.myntraCombo.freeItemDetails[this.myntraCombo.productsSet.dre_freeItems[0]].search_image.replace(/180_240/g,'96_128_mini').replace(/style_search_image/g,'properties');	
		}
		
		this.comboItemCont.width('744px');
		this.comboItemOuter.append('<div class="free-item-det"><img src="'+_img+'"><h6>'+this.myntraCombo.freeItem.name+'</h6><div class="tag">GIFT</div></div>');
	},

	setMinMoreMessage : function(){
		var msg ='';
		if(typeof this.myntraCombo.productsSet.dre_minMore === 'undefined'){

			if(this.myntraCombo.isAmount){
				msg ='Buy for <span>Rs.'+Myntra.Utils.formatNumber(this.myntraCombo.productsSet.dre_buyAmount) + '</span> more to complete combo'; 
			}else{
				msg ='Buy <span>'+this.myntraCombo.productsSet.dre_buyCount + ' more</span> to complete combo';
			}
			this.gotoCart.removeClass('primary-btn');
		
		}else if(this.myntraCombo.productsSet.dre_minMore > 0){

			if(this.myntraCombo.isAmount){
				msg ='Buy for <span>Rs.'+Myntra.Utils.formatNumber(this.myntraCombo.productsSet.dre_minMore) + '</span> more to complete combo'; 
			}else{
				msg ='Buy <span>'+this.myntraCombo.productsSet.dre_minMore + ' more</span> to complete combo';
			}
			this.gotoCart.removeClass('primary-btn');
		}
		else { 
			msg = '<span class="combo-complete">Congratulations! Combo Complete</span>';
			//also chnage the color of the button
			this.gotoCart.addClass('primary-btn');

		}
		this.minMoreMessage.html(msg);		
	}

};
})();// end of "use strict" wrapper



Myntra.initComboPage = function(){ "use strict";
	if(Myntra.Combo.ComboId){
		Myntra.ComboPageWidget.setData(Myntra.Combo);
		//bind the event which is fired by addto combo
		$(document).on('myntra.addedtocombo.done',function(e,dataObj){
			Myntra.ComboPageWidget.update(dataObj);
			$('#prod_'+dataObj.styleObj.sid).find('.combo-flag').show();
		});
		$(document).on('myntra.removedfromcombo.done',function(e,dataObj){
			Myntra.Header.setCount('bag',dataObj.styleObj.resp.count);
			Myntra.Header.setBagTotal(dataObj.styleObj.resp.total);
			Myntra.ComboPageWidget.update(dataObj);
			$('#prod_'+dataObj.styleObj.sid).find('.combo-flag').hide();
			
		});

		$(document).on('myntra.combowidget.update.done',function(){
			if($('.combo-box-overlay').length)
				$('.combo-box-overlay').hide();
		});

		$('.mk-product-sort').on('click','li a', function(e){
			//if price clicked and ASC show Asc
			var target = $(e.target),
	        el = $(e.currentTarget);
			el.parent().parent().find('a').removeClass('mk-sort-selected');
			el.addClass('mk-sort-selected');
			var __sortby = el.data('val');
			if(el.hasClass('price-sort-filter')){
				if(__sortby=='PRICEA'){
					el.find('span.mk-price-asc').show().siblings().hide();
					el.data('val','PRICED');
				}else{
					el.find('span.mk-price-desc').show().siblings().hide();
					el.data('val','PRICEA');
				}
			}
			else {
				el.parent().parent().find('.price-sort-filter span').hide();
			}
			$.ajax({
				type:"GET",
                                url:http_loc+"/getAjaxComboItemsSorted.php?all=1&sortby="+__sortby+"&cid="+Myntra.Combo.ComboId+"&page="+"0",
                                beforeSend:function(){
                                        // /show loader in combo box
                                        curPage=0;
                                        finalPageReached=false;
                                        pageSize= 1000;
                                        baseUrl = "/getAjaxComboItemsSorted.php?all=1&sortby="+__sortby+"&cid=";
                                        url= "/getAjaxComboItemsSorted.php?all=1&sortby="+__sortby+"&cid="+Myntra.Combo.ComboId+"&page="+"1";
                                        $('#mk-combo-grid').html('<ul class="mk-cf"> </ul>');
                                        if($('.body-overlay').length){
                                                $('.body-overlay').show();
                                        }else{
                                                $('<div class="body-overlay"><img style="margin-top:100px" src="http://myntra.myntassets.com/skin2/images/loader_150x200.gif" /></div>').css({'position':'fixed','left':'0','top':'0','height':'100%','width':'100%','overflow':'hidden','background':'#fff','opacity':'.6','text-align':'center'}).appendTo('#mk-combo-grid');
                                        }

                                },
                                success: function(ret){
                                        //show loader
                                        ret = $.parseJSON(ret);
                                        if(ret.status == 'success'){
                                                //$('#mk-combo-grid ul').html(ret.data);
                                                //Myntra.Utils.TriggerLazyLoad();
                                                $('#mk-combo-grid').remove();
                                                $(".mk-product-filters").after(' <div class="mk-combo-grid mk-search-grid megalist" id="mk-combo-grid">   <ul class="mk-cf"> </ul></div>');
                                                var dataRcvd = ret.data;
                                                if (dataRcvd.data != null){
                                                        curDataRecvd = dataRcvd.data;
                                                        for (var i = 0 ; i < dataRcvd.data.length;i++){
                                                                uiParams.data[(pageSize)*(curPage)+i]=dataRcvd.data[i];
                                                            }
                                                        for (var i = dataRcvd.data.length;i<dataRcvd.relatedProdNum;i++)
                                                            uiParams.data.push(0);
                                                        url = baseUrl+cid+"&mini=1&all=true&page="+(curPage+1);
                                                }
                                                else
                                                        finalPageReached=true;
                                                $('#mk-combo-grid').megalist();
                                                $('#mk-combo-grid').megalist('setDataProvider', createDataProvider() );
                                                $('#mk-combo-grid').megalist('setLabelFunction', listItemLabelFunction );
                                        }

                                },
                                complete:function(){
					if($('.body-overlay').length)
						$('.body-overlay').hide();
				}	

			});
		});

		$(document).on('myntra.removedfromcombo.getcombostatus',function(e,styleObj){
			$.ajax({
				type:"GET",
				url:http_loc+"/comboController.php?mini=1&cid="+styleObj.cid,
				
				success: function(ret){
					var retObj = $.parseJSON(ret);
					if(retObj.status =='success'){
						retObj.data.styleObj = styleObj;
						$(document).trigger('myntra.removedfromcombo.done',retObj.data);
					}
					
				},
				complete: function(){
				
				}
			});	
		});
	}
	else {
			$('.mk-product-filters').hide();
			$('.mk-combo-box-cont').html('This products is not associated with any combo offer')
	}
};

$(document).ready(function(){
	//handle the selected flag for products
	Myntra.initComboPage();

});

$(window).load(function(){
    Myntra.Utils.TriggerLazyLoad();
});


