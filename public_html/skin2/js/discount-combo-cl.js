Myntra.sCombo = {
    Err: function(msg){
        return false;
    },
    //selectedStyles: {},
    cartTotal: 0,
    minMore: 0,
    isAmount: false,
    cartSavings: 0,
    discountedTotal: 0,
    quantity_in_combo: 0,
    amount_in_combo: 0,
	Test: {},
	getCollection: function(combotype){
		if(!combotype)
			combotype = 'defaultitemlist';
		switch(combotype){
			case 'defaultitemlist':return Myntra.sCombo.cSetStyles; break;
			case 'freeitem'		:return Myntra.sCombo.cFreeStyles; break;
		}
		return this.Err('no collection found '+ combotype);
	}
}
Myntra.sCombo.ComboOverlay= {};
Myntra.sCombo.styleModel= function(){
		//private properties. dont access
    	var	_id = null,
            _uid = null,
	    	_isModified= false,
		    _initialAttributes ={},
    		_inCart= false,
    		_isChecked=	false,
    		_isSizeLoaded=	false,
            _isQuantityLoaded= false,
			_isAvailable=true,
			_itemAddedByDiscountEngine=false;
        this._cp_attributes = {};
		var attributes =  {
			uid: null,
			quantityList:	[],
			sizeList:	[],
			skuid: null,
			quantity: null,
            styleid: null,
            size: null
		};
        this.tojson= function(){
            return attributes;
        },
		//public
        this.getId= function(id){
            return _id;
        };
		this.get= function(attr)	{
			return attributes[attr];
		};
		this.geti = function(attr){
			return _initialAttributes[attr];
		};
		this.set= function(attrs){
			if (!attrs) return attributes;
			var now = attributes;
			if(!_initialAttributes){
				_initialAttributes = now;
			}
			for (var attr in attrs) {
				//if(!now[attr]) return false; //addition of new attributes not allowed 
				if(!_initialAttributes[attr])
					_initialAttributes[attr] = attrs[attr];
		        attributes[attr] = attrs[attr];
                this._cp_attributes[attr] = attrs[attr];
		    }
			return attributes;
		};
		this.restore = function(){
			if(_initialAttributes){
				this.set(_initialAttributes);
			}
		};
		this.setInCart= function(){
			_inCart = true;
		};
		this.unsetInCart= function(){
			_inCart = false;
		};
		this.setItemAddedByDiscountEngine= function(){
			_itemAddedByDiscountEngine = true;
		};
		this.itemAddedByDiscountEngine= function(){
			return _itemAddedByDiscountEngine;
		};
		this.setChecked= function(){
			if(_inCart && 
					_initialAttributes['quantity'] == this.get('quantity') &&
					_initialAttributes['skuid'] == this.get('skuid') ){
				_isModified = false;
			}
			_isChecked = true;
		};
        this.setSize= function(size){
            //check for size existence
            this.set({size: size});
        };
        this.setQuantity= function(q){
            this.set({quantity: parseInt(q)});
            //check for existence
        };
		this.unsetChecked= function(){
			if(_inCart){
				_isModified = true;
			}
			_isChecked = false;
		};
		this.isAvailable= function(){
			return _isAvailable;
		};
		this.setIsAvailable= function(isA){
			_isAvailable = isA;
		};
        this.isInCart= function(){
            return _inCart;
        };
		this.isChecked= function(){
			return _isChecked;
		};
		this.isRemovedFromCart= function(){
			return !this.isChecked() && _inCart;
		};
		this.isEditedFromCart= function(){
			return this.isChecked() && _isModified && _inCart && this.get('skuid') != this.geti('skuid');
		};
		this.isAddedToCart= function(){
			return this.isChecked() && (!_inCart || (this.get('skuid') != this.geti('skuid')));
		};

		this.elLi = function(){
			if(this.get('uid') == null) return Myntra.sCombo.Err('stylemodla el li no uid');
			var a = $("#item_combo_lay_"+this.get('uid'));
			return a;
		};
		this.elCheckbox = function(){
			if(this.get('uid') == null) return Myntra.sCombo.Err('stylemodla el checkbbox no uid');
			return $("#item_combo_lay_"+this.get('uid')+"  .checkbox");
		};
        this.elSizeS= function(){
            if(this.get('uid') == null) return Myntra.sCombo.Err('stylemodla el size sleect no uidid');
			if(_isSizeLoaded ){
				var s = this.get('sizeList');
				var cnt=0;
				for(var i in s) cnt++;
					if(cnt <= 1) return $("#item_combo_lay_"+this.get('uid')+" .size-info-box");
					else return $("#item_combo_lay_"+this.get('uid')+" select.cow-size-loader");
			} else {
	            return $("#item_combo_lay_"+this.get('uid')+" select.cow-size-loader");
			}
        };

        this.elSizeD=function(){
            if(!this.get('uid')) return Myntra.sCombo.Err('stylemodla el sizeD no uid');
			return $("#item_combo_lay_"+this.get('uid')+" select.cow-size-loader");
        };
		this.elSizeSkuid=function(){
			if(!this.get('uid')) return Myntra.sCombo.Err('stylemodla el sizeD no uid');
			if(_isSizeLoaded ){
                var s = this.get('sizeList');
                var cnt=0;
                for(var i in s) cnt++;
                    if(cnt <= 1) return $("#item_combo_lay_"+this.get('uid')+" .size-info-box").attr("skuid");
                    else return $("#item_combo_lay_"+this.get('uid')+" select.cow-size-loader").val();
            } else {
                return $("#item_combo_lay_"+this.get('uid')+" select.cow-size-loader").val();
            }	
		};
        this.elQuanS= function(){
            if(!this.get('uid')) return Myntra.sCombo.Err('style modal el quanS no styleid');
			var a = $("#item_combo_lay_"+this.get('uid')+" select.quantity");
			return a;
        };
        this.elQuanD= function(){
            if(!this.get('styleid')) return Myntra.sCombo.Err('style modal el quanD no styleid');
            return $('.quantity-selection-row-'+this.get('styleid'));
        };

        this.loadSizes = function(callback_fn){
            if(_isSizeLoaded){
				if(1){//this.isAvailable())
					callback_fn();
				}
				else {
					alert('no units available');
				}
                //Myntra.sCombo.UI.showFlip(this);
            } else {
                var that = this;
                $.ajax({
                    type: 'GET',
                    url: '/myntra/ajax_getsizes.php?ids=' + this.get('styleid'),
                    dataType: 'json',
                    beforeSend:function(){
                        $(that.elSizeD()).empty().append('<option value="-">-</option>');
                        //$(".size-selection-row-"+styleId+" .size-info-box").html("&nbsp;");
                    },
                    success: function(data){
						for(var i in data){
							that.set({sizeList: data[i]});
							for(var j in data[i]){
								if(data[i][j][2] == 0 ){
									that.setIsAvailable(false);
								}
							}
						}
                        _isSizeLoaded = true;
						if(1)//that.isAvailable())
							callback_fn();
						else {
							//that.elLi().remove();
						}
                    }
                });
            }
		};
	}

	Myntra.sCombo.styles = function() {
		this.collection	=	{
            models: [],
			length: function(){
                    return this.models.length;
                }
		};

	};

	Myntra.sCombo.styles.prototype = {
/*		collection :   {
            models: [],
            length: function(){
                    return this.models.length;
                }
        },*/

		get : function(attrs){
            var attrs = attrs || {};
            if(attrs.uid){
                for(var a in this.collection.models){
                    if(this.collection.models[a].get('uid') == attrs.uid)
                        return this.collection.models[a];
                }
            } else {
                return Myntra.sCombo.Err('not supported');
            }
            return false;
		},
		add : function(obj) {
			//if(!obj.get('styleid')) return false;
			this.collection.models.push(obj);
            return true;
		},
		removeLast : function(){
			this.collection.models.pop();
		},
		remove: function(uid) {
			var uid = uid;
			var y = $.grep(this.collection.models, function(styleObj,i) {
				return styleObj.get('uid') != uid.get('uid');
			});
			this.collection.models = y;
			return true;
		},
		unsetAll: function(){
			for(var i in this.collection){
				this.collection[i].unsetChecked();
			}
		},
		getAllChecked: function() {
			var checkedList = [];
			for(var i in this.collection.models){
				if(this.collection.models[i].isChecked()){
					checkedList.push(this.collection.models[i]);
				}
			}
			return checkedList;
		},
		//Edit already in cart
		getAllModified: function() {
			var checkedList = [];
			for(var i in this.collection.models){
				if(this.collection.models[i].isEditedFromCart()){
					checkedList.push(this.collection.models[i]);
				}
			}
			return checkedList;
		},
		getAllRemoved: function() {
			var checkedList = [];
			for(var i in this.collection.models){
				if(this.collection.models[i].isRemovedFromCart()){
					checkedList.push(this.collection.models[i]);
				}
			}
			return checkedList;
		},
		getAllAdded: function() {
			var checkedList = [];
			for(var i in this.collection.models){
				if(this.collection.models[i].isAddedToCart()){
					checkedList.push(this.collection.models[i]);
				}
			}
			return checkedList;
		},
        setCartData: function(){
            var styleid,
                cartTotal=0,
				getAmount=0,
				quantity=0;
            var allchecked = this.getAllChecked();
			Myntra.sCombo.quantity_in_combo = 0;
			Myntra.sCombo.amount_in_combo	= 0;
            for(var i in allchecked){
                styleid = allchecked[i].get('styleid');
				quantity = allchecked[i].get('quantity') ? parseInt(allchecked[i].get('quantity')) : 0;
                cartTotal += Myntra.sCombo.solrProducts[styleid].price * quantity;
				Myntra.sCombo.quantity_in_combo += quantity;
				Myntra.sCombo.amount_in_combo += Myntra.sCombo.solrProducts[styleid].price * quantity;
            }
            Myntra.sCombo.cartTotal = cartTotal;
            if(!Myntra.sCombo.isAmount)
                Myntra.sCombo.minMore = Myntra.sCombo.min - Myntra.sCombo.quantity_in_combo;
            else
                Myntra.sCombo.minMore = Myntra.sCombo.min - Myntra.sCombo.amount_in_combo;
            Myntra.sCombo.isConditionMet = Myntra.sCombo.minMore<=0?true:false;
			if(Myntra.sCombo.isConditionMet){
				if(Myntra.sCombo.isAmount & Myntra.sCombo.getAmount > 0 ){
					Myntra.sCombo.cartSavings = Myntra.sCombo.getAmount;
				} else if(Myntra.sCombo.getPercent){
					Myntra.sCombo.cartSavings = Myntra.sCombo.cartTotal * Myntra.sCombo.getPercent / 100 ;
				}
			} else {
				Myntra.sCombo.cartSavings = 0;
			}
			Myntra.sCombo.discountedTotal = Myntra.sCombo.cartTotal - Myntra.sCombo.cartSavings;
        },

        // To abstract
        eventCheckboxStyle: function(attrs){
            var uid = attrs['uid'];
            var styleObj = this.get({uid:uid});
            if(!styleObj){
				var _tmp_style = Myntra.sCombo.comboSet[uid];
				if(!_tmp_style) return Myntra.sCombo.Err('no match in solr');
                var styleobj = new Myntra.sCombo.styleModel();
                styleobj.set({uid: uid , styleid: _tmp_style.styleid , skuid: 0, quantity: 0,size: ''});
                this.add(styleobj);
                styleObj = this.get({uid: uid});
            }
            if(!styleObj) return Myntra.sCombo.Err('no style object constructed');

            if(!styleObj.isChecked()){
                styleObj.setChecked();
				styleObj.elCheckbox().removeClass("unchecked").addClass("checked");
				Myntra.sCombo.UI.showFlip(styleObj);
                styleObj.loadSizes(function(){
					Myntra.sCombo.UI.showFlip(styleObj);
					if(styleObj.isAvailable()){
						Myntra.sCombo.UI.checkStyle(styleObj);
						/*var qnty = 1;
            		    if(!styleObj.isInCart()){
        	        	    styleObj.set({quantity:qnty});
	    	            }*/
					} else {
						//styleObj.elLi().remove();
						//collection.removeLast();
					}
					Myntra.sCombo.UI.refreshUI();
				});
				Myntra.sCombo.UI.refreshUI();
            } else {
                styleObj.unsetChecked();
				if(styleObj.geti('size') && styleObj.geti('quantity')){
				} else {
					styleObj.set({size:null,skuid:null,quantity:null});
				}
                Myntra.sCombo.UI.uncheckStyle(styleObj);
				Myntra.sCombo.UI.refreshUI();
            }
            return true;
        },
        eventChangeSize: function(attrs){
            var uid = attrs['uid'];
            var styleObj = this.get({uid:uid});
			if(!styleObj) Myntra.sCombo.Err('size change style obj not fould');
			//styleObj.set({skuid:skuid});
			var size = $("#item_combo_lay_"+styleObj.get('uid')+" .cow-size-loader option:selected").html();
			styleObj.set({size:size});
			Myntra.sCombo.UI.ModifySizeSetQuantity(styleObj);
			Myntra.sCombo.UI.refreshUI();
        }
	};

	
	Myntra.sCombo.setStyles = function() {
		Myntra.sCombo.styles.apply(this, arguments);
	};
	Myntra.Utils.extend(Myntra.sCombo.setStyles, Myntra.sCombo.styles, {
	});

    Myntra.sCombo.freeStyles = function() {
		Myntra.sCombo.styles.apply(this, arguments);
	};
	Myntra.Utils.extend(Myntra.sCombo.freeStyles, Myntra.sCombo.styles, {
		eventCheckboxStyle: function(attrs){
            var uid = attrs['uid'];
            var styleObj = this.get({uid:uid});
            if(!styleObj){
				var _tmp_style = Myntra.sCombo.comboSet[uid];
				if(!_tmp_style) return Myntra.sCombo.Err('no match in solr');
                var styleobj = new Myntra.sCombo.styleModel();
                styleobj.set({uid: uid , styleid: _tmp_style.styleid , skuid: 0, quantity: 0,size: ''});
				styleobj.setItemAddedByDiscountEngine();
                this.add(styleobj);
                styleObj = this.get({uid: uid});
            }
            if(!styleObj) return Myntra.sCombo.Err('no style object constructed');

            if(!styleObj.isChecked()){
		 _gaq.push(['_trackEvent', 'combo_free', 'select_style', 'check', parseInt(styleObj.get('styleid'))]);
				var alreadyChecked = Myntra.sCombo.cFreeStyles.getAllChecked();
				for(var i in alreadyChecked){
					alreadyChecked[i].unsetChecked();
					Myntra.sCombo.UI.uncheckBoxStyle(alreadyChecked[i]);
					Myntra.sCombo.UI.priceMessageContent(false,
						alreadyChecked[i].get('uid'),alreadyChecked[i].get('size'),alreadyChecked[i].get('quantity'));
				}
                styleObj.setChecked();
				Myntra.sCombo.UI.checkBoxStyle(styleObj);
                styleObj.loadSizes(function(){
					if(styleObj.isAvailable()){
						Myntra.sCombo.UI.checkBoxStyle(styleObj);
						var sizeList = styleObj.get('sizeList');
						var cnt = 0;
						for(var i in sizeList){
							cnt++;
						}
						if(cnt == 1){
							for(var i in sizeList){
								styleObj.set({skuid: i , size: sizeList[i][0], quantity : 1 });
							}
							Myntra.sCombo.UI.priceMessageContent(true,styleObj.get('uid'),styleObj.get('size'),styleObj.get('quantity'));
							$("#item_combo_lay_"+styleObj.get('uid')+" .main-flip .edit-btn").addClass("hide");	
						} else {
							return Myntra.Combo.Err('more than one item');
						}
					} else {
					}
					Myntra.sCombo.UI.refreshUI();
				});
				Myntra.sCombo.UI.refreshUI();
            } else {
                styleObj.unsetChecked();
		_gaq.push(['_trackEvent', 'combo_free', 'select_style', 'uncheck', parseInt(styleObj.get('styleid'))]);
				if(styleObj.geti('size') && styleObj.geti('quantity')){
				} else {
					styleObj.set({size:null,skuid:null,quantity:null});
				}
                Myntra.sCombo.UI.uncheckStyle(styleObj);
				Myntra.sCombo.UI.refreshUI();
            }
            return true;
        }

	});




	//Events
	Myntra.sCombo.eventCheckboxStyle= function(){
        $(".scombo .checkbox").click(function(event) {
            var input = $(this)[0];
			var eluid = $(input).parents(".cart-widget-item");
			var uid = $(eluid).attr("data-uid");
			var collection = Myntra.sCombo.getCollection($(eluid).attr("data-combotype"));
            var attrs = { uid : uid};
            return collection.eventCheckboxStyle(attrs);
        });
    }
	Myntra.sCombo.eventChangeSize= function(){
		$(".scombo .size-info select").change(function(event){
        	var input = $(this)[0];
			var eluid = $(input).parents(".cart-widget-item");
			var skuid = $(input).val();
			var uid = $(eluid).attr("data-uid");
			var collection = Myntra.sCombo.getCollection($(eluid).attr("data-combotype"));
            var attrs = { uid: uid}
            return collection.eventChangeSize(attrs);
			
    	});
	}
	Myntra.sCombo.eventChangeQuantity= function(){
		//bind on change to the quantity
	    $(".scombo .quantity-info .quantity").change(function(event){
			Myntra.sCombo.UI.refreshUI();
		});
	}
	Myntra.sCombo.eventSetStyle= function(){
		//OK button
		$(".scombo .edit-flip .save-btn").click(function(event) {
			if($(this).attr('disabled')=="disabled") return false;
			var input = $(this)[0];
			//Also sets initialAttributes that dont change
			var eluid = $(input).parents(".cart-widget-item");
			var uid = $(eluid).attr("data-uid");
			var collection = Myntra.sCombo.getCollection($(eluid).attr("data-combotype"));
            var styleObj = collection.get({uid:uid});
            if(!styleObj) Myntra.sCombo.Err('size change style obj not fould');
			
			var skuid = styleObj.elSizeSkuid();
			var sizeL = styleObj.get('sizeList');
            var size = sizeL[skuid][0]; 
			styleObj.set({size: size , skuid: skuid , quantity: styleObj.elQuanS().val()});
			$("#item_combo_lay_"+styleObj.get('uid')+" .main-flip .edit-btn").removeClass("hide");
			$("#item_combo_lay_"+styleObj.get('uid')+" .edit-flip").addClass("hide");
			Myntra.sCombo.UI.priceMessageContent(true,styleObj.get('uid'),styleObj.get('size'),styleObj.get('quantity'));
			Myntra.sCombo.UI.refreshUI();
//#			 _gaq.push(['_trackEvent', 'combo', 'edit-details', 'save', parseInt(styleObj.get('styleid'))]);
		});


		$(".scombo .main-flip .edit-btn").click(function(event) {
			$(this).addClass("hide");
			var uid = $(this).parents(".cart-widget-item").attr("data-uid");
			var collection = Myntra.sCombo.getCollection($(this).parents(".cart-widget-item").attr("data-combotype"));
			var styleObj = collection.get({uid:uid});
			if(!styleObj) Myntra.sCombo.Err('size change style obj not fould');
			
//			 _gaq.push(['_trackEvent', 'combo', 'click_edit_details', '', parseInt(styleObj.get('styleid'))]);
			styleObj.setChecked();
  //          styleObj.elCheckbox().removeClass("unchecked").addClass("checked");
//			$(this).parents(".cart-combo-widget-image").find(".edit-flip").removeClass("hide");
			Myntra.sCombo.UI.checkStyle(styleObj);
		});
		//Cancel button
		$(".scombo .edit-flip .cancel-btn").click(function(event) {
			var input = $(this)[0];
            var eluid = $(input).parents(".cart-widget-item");
            var uid = $(eluid).attr("data-uid");
			var collection = Myntra.sCombo.getCollection($(eluid).attr("data-combotype"));
            var styleObj = collection.get({uid:uid});
            if(!styleObj) Myntra.sCombo.Err('size change style obj not fould');
			
			Myntra.sCombo.UI.priceMessageContent(true,styleObj.get('uid'),styleObj.get('size'),styleObj.get('quantity'));
//			styleObj.restore();
			$("#item_combo_lay_"+styleObj.get('uid')+" .edit-flip").addClass("hide");
			if(styleObj.get('size') && styleObj.get('quantity')){
				styleObj.unsetChecked();
				//
			} else {
				styleObj.unsetChecked();
				Myntra.sCombo.UI.uncheckBoxStyle(styleObj);
			}

//        	 _gaq.push(['_trackEvent', 'combo', 'edit-details', 'cancel', parseInt(styleid)]);*/
    	});
	}
	Myntra.sCombo.eventAddToBag= function(){
	$('.scombo .combo-add-to-cart-button').click(function(){
        var isValidated = true;
        if(!$.isEmptyObject(Myntra.sCombo.products)){
			var allChecked = Myntra.sCombo.cSetStyles().getAllChecked();
			if(allChecked.length == 0)
				isValidated = false;
		}
		if(!$.isEmptyObject(Myntra.sCombo.freeproducts)){
			var allChecked = Myntra.sCombo.cFreeStyles.getAllChecked();
			if(allChecked.length != 1 )
				 isValidated = false;
		}
        if(isValidated){
			$(".scombo .combo-add-to-cart-button").removeAttr("disabled");
		}else{	
            $(".scombo .combo-add-to-cart-button").attr("disabled","true");
            return false;
		}
		
        //gaTrackEvent('discount_combo_layer_add_to_cart','submit','');
		
        _gaq.push(['_trackEvent', 'buy_now', Myntra.Data.pageName, 'combo_overlay_free', parseInt(Myntra.sCombo.ComboId)]);
		Myntra.sCombo.removeItemsFromCart();
		//addItemsToCart();
    });
	}
	Myntra.sCombo.addItemsToCart= function(){
		var addedToCartList = Myntra.sCombo.cSetStyles.getAllAdded();
		var freeItemAddedToCartList = Myntra.sCombo.cFreeStyles.getAllAdded();
		for( var i in freeItemAddedToCartList){
			addedToCartList.push(freeItemAddedToCartList[i]);
		}
		var addedToCartArr = [];
		if(addedToCartList.length){
			for(var uid in addedToCartList){
				var postItem = Object();
				postItem["skuid"] = addedToCartList[uid].get('skuid');
				postItem["quantity"] = addedToCartList[uid].get('quantity');
				postItem["itemAddedByDiscountEngine"] = addedToCartList[uid].itemAddedByDiscountEngine();
				if(postItem["quantity"]!=null && postItem["quantity"] != 0) addedToCartArr.push(postItem);			
			}
		
		$.ajax({
            type: 'POST',
            url: "/mkretrievedataforcart.php?pagetype=productdetail&lead=recently_viewed",
            data:     {
                'fromComboOverlay' : 1,
                'cartPostData' : JSON.stringify(addedToCartArr),
                '_token' : $('input:hidden[name=_token]').val()
            },
            beforeSend:function(){
                Myntra.LightBox('#cart-combo-overlay').showLoading();
            },
            success: function(data){
				window.parent.location = document.location.protocol+'//'+document.location.hostname+"/mkmycart.php";
            },
            error: function(data){
                return false;
            }
        });
		} else {
			window.parent.location = document.location.protocol+'//'+document.location.hostname+"/mkmycart.php";
		}
	}
	Myntra.sCombo.modifyItemsInCart= function(){
		Myntra.sCombo.addItemsToCart();
		return;
		var modifiedInCartList = Myntra.sCombo.cSetStyles.getAllModified();
		var modifiedInCartArr = [];
		if(0){//modifiedInCartList.length){
			for(var uid in modifiedInCartList){
				var postItem = Object();
				postItem["skuid"] = modifiedInCartList[uid].get('skuid');
				postItem["quantity"] = modifiedInCartList[uid].get('quantity');
				postItem['cartitemid'] = Myntra.sCombo.selectedStyleProductMap[postItem["skuid"]];
				if(postItem["quantity"]!=null && postItem["quantity"] != 0) modifiedInCartArr.push(postItem);
			}
			var postItem, qtyStr, sizeStr, cartItemId,
				postData = Object();
			for(var i in modifiedInCartArr){
				postItem = modifiedInCartArr[i];
				cartItemId = postItem.cartitemid;
				qtyStr = cartItemId+'qty';
				sizeStr = cartItemId+'size';
				postData[qtyStr+''] = postItem.quantity;
				postData[sizeStr+''] = postItem.skuid;
				postData['cartitem'] = postItem.cartitemid;
				postData['fromComboOverlay'] = 1;
				postData['update'] = 'size';
				postData['_token'] = $('input:hidden[name=_token]').val();
				$.ajax({
    		        type: 'POST',
        		    url: "/modifycart.php",
            		data: postData,
    	        	beforeSend:function(){
        	        	Myntra.LightBox('#cart-combo-overlay').showLoading();
	        	    },
	    	        success: function(data){
						Myntra.sCombo.addItemsToCart();
	    	        },
    	    	    error: function(data){
        	    	    return false;
	            	}
    	   	 	});
			}
		} else {
			Myntra.sCombo.addItemsToCart();
		}
	}
	Myntra.sCombo.removeItemsFromCart= function(){
		var removedFromCartList = Myntra.sCombo.cSetStyles.getAllRemoved();
		var removedFromCartArr = [];
		var removingFreeItem = false;
		var freeItemAddedToCartList = Myntra.sCombo.cFreeStyles.getAllRemoved();
        for( var i in freeItemAddedToCartList){
            removedFromCartList.push(freeItemAddedToCartList[i]);
			removingFreeItem = true;
        }
		if(removedFromCartList.length){
			for(var uid in removedFromCartList){
				var postItem = Object();
				postItem["skuid"] = removedFromCartList[uid].geti('skuid');
				postItem["quantity"] = removedFromCartList[uid].geti('quantity');
				postItem['cartitemid'] = Myntra.sCombo.selectedStyleProductMap[postItem["skuid"]].itemid;
				if(postItem["quantity"]!=null && postItem["quantity"] != 0) removedFromCartArr.push(postItem);
			}
			for(var i in removedFromCartArr){
				postItem = removedFromCartArr[i]; 
				$.ajax({
        		    type: 'GET',
		            url: "/modifycart.php",
        		    data:     {
		                'fromComboOverlay' : 1,
						'remove'	:	'true',
						'itemid'   :  postItem.cartitemid,
		                '_token' : $('input:hidden[name=_token]').val(),
						'removingFreeItem' : removingFreeItem
        		    },
		            beforeSend:function(){
        		        Myntra.LightBox('#cart-combo-overlay').showLoading();
		            },
        		    success: function(data){
						Myntra.sCombo.modifyItemsInCart();		
        		    },
        		    error: function(data){
                		return false;
		            }
        		});
			}
		} else {
			Myntra.sCombo.modifyItemsInCart();		
		}
	}

	Myntra.sCombo.InitQuickLook= function() {
		var lb = null;
		$(".scombo .combo-quick-look").live('click', function(){
			var styleid=$(this).attr("data-styleid");
            var styleurl=$(this).attr("data-href");
			var uid = $(this).attr("data-uid");
            var fromCart= new Object();
			fromCart.is = 1;
			fromCart.combouid = uid;
            if(typeof styleid != "undefined" || styleid != ""){
                lb = Myntra.QuickLook.loadQuickLook(styleid, styleurl, fromCart);
                if(typeof $(this).attr("data-widget") != "undefined"){
    				_gaq.push(['_trackEvent', 'quicklook', Myntra.Data.pageName, $(this).attr("data-widget"), parseInt(styleid)]);
    			}
    			else{
    				_gaq.push(['_trackEvent', 'quicklook', window.location.toString(), parseInt(styleid)]);
    			}
    		}
		});

		$(".scombo .main-flip").live("mouseover", function(event){
                $(this).find(".combo-quick-look").show();
	    });

    	$(".scombo .main-flip").live("mouseout", function(event){
            $(this).find(".combo-quick-look").hide();
	    });

    	/*$("#back-to-combo-actn-btn").live("click", function(event){
        	lb.hide();
			 _gaq.push(['_trackEvent', 'combo', 'cancel_quicklook', '']);
    	    event.stopPropagation();
	    });*/
		 var sizeSelected= function(sizeDropCont){
        	if(sizeDropCont.find('.mk-size').val()==0){
    	        return false;
	        }
    	    else
        	{
            	return true;
	        }
    };

    /*$(".scombo .quick-look-combo-btn").live("click", function(event){
		var sizeDropCont=$(this).closest('.mk-product-option-cont');
        if(sizeSelected(sizeDropCont)){
            //gaTrackEvent('quick_look_add_to_combo','submit','');
            var styleid = $('#hiddenFormMiniPIP').find('input[name="productStyleId"]').val();
			var uid = $('#hiddenFormMiniPIP').find('input[name="combouid"]').val();
            var skuid = parseInt($('#hiddenFormMiniPIP').find('input[name="productSKUID"]').val());
			if(isNaN(skuid))
				skuid = parseInt(sizeDropCont.find('.mk-size').val());
            lb.hide();
			//_gaq.push(['_trackEvent', 'combo', 'select_style', 'quicklook', parseInt(styleid)]);
            Myntra.sCombo.AddSkuFromMiniPip(styleid, skuid,uid);
        } else {
			$(sizeDropCont).find('.mk-size-error').removeClass('mk-hide');
		}
        event.stopPropagation();
    });*/
	}

	/*Myntra.sCombo.AddSkuFromMiniPip = function(styleid, skuid,uid){
		//NOT FOR FREE GIFTS
    	Myntra.sCombo.MiniPipMode = true;
        var styleObj = Myntra.sCombo.cSetStyles.get({uid:uid});
        if(!styleObj){
        	var _tmp_style = Myntra.sCombo.comboSet[uid];
            if(!_tmp_style || _tmp_style.styleid!=styleid) return Myntra.sCombo.Err('no match in solr : styleid tmp_style_id '+ styleid+ _tmp_style.styleid);
            var styleobj = new Myntra.sCombo.styleModel();
            styleobj.set({uid: uid , styleid: styleid , skuid: skuid, quantity: 1,size: ''});
                Myntra.sCombo.cSetStyles.add(styleobj);
                styleObj = Myntra.sCombo.cSetStyles.get({uid: uid});
       	} else {
			styleObj.set({styleid: styleid, skuid: skuid, quantity: 1 , size: ''});
		}
		if(!styleObj) return false;
       if(!styleObj.isChecked()){
	       styleObj.setChecked();
                styleObj.elCheckbox().removeClass("unchecked").addClass("checked");
                styleObj.loadSizes(function(){
                    if(styleObj.isAvailable()){
						var sizeL = styleObj.get('sizeList');
						var size = sizeL[styleObj.get('skuid')][0];
						styleObj.set({size:size});
                        Myntra.sCombo.UI.checkStyle(styleObj);
						Myntra.sCombo.UI.showFlip(styleObj);
						Myntra.sCombo.UI.hideEditFlip(styleObj);
						Myntra.sCombo.UI.priceMessageContent(true,styleObj.get('uid'),styleObj.get('size'),styleObj.get('quantity'));
                    } else {
                        styleObj.elLi().remove();
                        Myntra.sCombo.cSetStyles.removeLast();
                    }
                    Myntra.sCombo.UI.refreshUI();
                });
                Myntra.sCombo.UI.refreshUI();
    	} else {
			styleObj.loadSizes(function(){
				var sizeL = styleObj.get('sizeList');
                var size = sizeL[styleObj.get('skuid')][0];
                styleObj.set({size:size});
				Myntra.sCombo.UI.showFlip(styleObj);
				Myntra.sCombo.UI.hideEditFlip(styleObj);
				Myntra.sCombo.UI.priceMessageContent(true,styleObj.get('uid'),styleObj.get('size'),styleObj.get('quantity'));
				Myntra.sCombo.UI.refreshUI();
			});
        	Myntra.sCombo.UI.refreshUI();
		}
  	}*/

    Myntra.sCombo.RegisterEvents= function() {
		//$(".mk-combo-tabs").tabs();
        Myntra.sCombo.eventCheckboxStyle();
        Myntra.sCombo.eventChangeSize();
        Myntra.sCombo.eventChangeQuantity();
		Myntra.sCombo.eventSetStyle();
		Myntra.sCombo.eventAddToBag();
		//Myntra.sCombo.InitQuickLook();
		//Myntra.sCombo.tabEvents();
		 $('.scombo .quantity-box').keydown(function(event) {
            var key = event.charCode || event.keyCode || 0;
            return (
            key == 8 || key == 9 || key == 46 || key == 13 || (key >= 48 && key <= 57)) && (!event.shiftKey);
    	});
        lb = Myntra.LightBox('#cart-combo-overlay');
        //cancel the pop-up event
        $(".scombo .combo-overlay-cancel").click(function(e){
	    _gaq.push(['_trackEvent', 'combo_free', 'cancel', Myntra.Data.pageName,parseInt(Myntra.sCombo.ComboId)]);
            lb.hide();
        });
        Myntra.sCombo.UI.refreshUI();
        /*var noOfElementsAdded = $(".scombo .combo-type-class-freeitemlist.cart-widget-box .checked").length;
        if($(".scombo .checked").attr('disabled')!=null)
            noOfElementsAdded -= $(".scombo .checked").attr('disabled').length;
        if(noOfElementsAdded>0){
            $(".combo-add-to-cart-button").removeAttr("disabled");
        }else{
            $(".combo-add-to-cart-button").attr("disabled","true");
        }*/
    }
    Myntra.sCombo.ComboOverlay= {};



