$(document).ready(function(){
    // Search autocomeplete
if (Myntra.Data.showAutoSuggest == 1) {
    var cache = {},lastXhr;
    $(document).ready(function() {
        $(".mk-site-search").autocomplete({
        minlength:  1,
        delay:      0,
        autoFocus:  false,
        type:       "searchbox",
        select:     function(event,ui){
                        $('input.mk-site-search').val(ui.item.value);
                        menuSearchFormSubmit($('.mk-search-form'));
                    },
        source:     function( request, response ) {
                        var term = request.term;
                        if ( term in cache ) {
                            response( cache[ term ] );
                            return;
                        }

                        lastXhr = $.getJSON( "/myntra/auto_suggest.php", request, function( data, status, xhr ) {
                        cache[ term ] = data;
                        if ( xhr === lastXhr ) {
                            response( data );
                        }
                    });
            },
        appendTo:   ".mk-body",
        position:   {
                        my: "left top",
                        at: "left bottom"
                    }
        });
    });
}

});


// NOTE:
// Removed all the code to separate js files based on functionality.
function menuSearchFormSubmit(form)
{
    var str=$.trim($(form).find(".mk-site-search").val());
	if(str != '')
    {
		_gaq.push(['_trackEvent', 'search_button', Myntra.Data.pageName, str]);
        str = str.replace(/\s+/g, '-').replace(/\//g, '__');
        window.location=http_loc+"/"+encodeURIComponent(str)+"?userQuery=true&s=sr";
        return false;
    }
    else{
        return false;
    }
}
