
$(document).ready(function() {
	var carttooltipcoupon = new Myntra.Tooltip(null, '#coupon-exclusion-tt', {side: 'below', useTitle: true});
	
	Myntra.InitTooltipFromTitle($('#cart-discount-exclusion-tt'));
	
	var newCartCreatedTime = Myntra.Utils.Cookie.get("ncc");
	if(typeof newCartCreatedTime != "undefined" && newCartCreatedTime != "null"){
		_gaq.push(['_trackEvent', 'new_cart_created', newCartCreatedTime, null, null, true]);
	}
	Myntra.Utils.Cookie.del("ncc");

    $('.cart-page .savings-hdr .ico').click(function(e) {
        var ico = $(this),
            pnode = ico.parent().parent(),
            savings_detail = pnode.find('.savings-details'),
            savings_hdr = pnode.find(".savings-hdr .savings");

        if (ico.hasClass('ico-expand')) {
            savings_detail.removeClass('hide');
            ico.removeClass('ico-expand').addClass('ico-collapse');
            savings_hdr.addClass("savings-border");
        }
        else if (ico.hasClass('ico-collapse')) {
            savings_detail.addClass('hide');
            ico.removeClass('ico-collapse').addClass('ico-expand');
            savings_hdr.removeClass("savings-border");
        }
    });

    $(".move-to-wishlist").bind("click",function() {
    	_gaq.push(['_trackEvent', 'cart', 'save_item']);
		$(this).closest("div.row").find("form.form-save-item").trigger("submit");
    });

    $(".cart-remove-item").bind("click",function() {
    	_gaq.push(['_trackEvent', 'cart', 'remove_item']);
    	$(this).closest("div.row").find("form.form-remove-item").trigger("submit");
    });

    $('.cart-page .btn-place-order').click(function(e) {
        e.preventDefault();
        if (Myntra.Data.userEmail || Myntra.Data.enableGuestLogin) {
        	location.href = https_loc + '/checkout-address.php';
        }
        else {
        $(document).trigger('myntra.login.show', [{onLoginDone: function() {location.href = https_loc + '/checkout-address.php';},invoke:'cart'}]);
        }
    });

    var dataSizes = {}, styleIds = [];
    var loadSizes = function(frm) {
        if (frm.data('sizes-loaded')) { return; }
        var sizeNode = frm.find('.sel-size'),
            styleId = frm.find('input[name="styleid"]').val(),
            selectedSize = sizeNode.val(),
            sizes = dataSizes[styleId],
            markup = [];
        if (!sizes) { return; }
		var count=0;
       	for (key in sizes) {
        	count++;
        }

		if(count == 1) {
			var sizename = $(sizeNode).attr("name");
			var sizetext = $(sizeNode).find("option").text();
			$(sizeNode).before('<span class="temp-size-sel mk-freesize"></span>');
			$(sizeNode).remove();
			tempSizeSpan = frm.find('.temp-size-sel');
			$(tempSizeSpan).html(sizetext);
			$(tempSizeSpan).append('<input type="hidden" name="'+sizename+'" val="'+selectedSize+'" />');
			return;
		}
        var key, val, sel, title, dis;
        for (key in sizes) {
            title = ''; dis = '';
            sel = key == selectedSize ? ' selected="true"' : '';
            val = sizes[key][0];
            if (!sizes[key][1]) {
            	//Kundan: PORTAL-2115: in enabled state
                //continue; //If we don't want to show thShow OOS sizes also in the combo for size on cart page e unavailable sizes, uncomment this line
            	//title = 'title="SOLD-OUT"';//On hover of the option, show sold-out
                //dis = ' disabled="disabled"';//disable the out of stock option
                //val += ' - SOLD OUT';//append sold-out next to the size-option
            }
            markup.push(['<option value="', key, '"', sel, dis, title, '>', val, '</option>'].join(''));
        };
        if(markup.length) {
        	sizeNode.html(markup.join("\n")); //.removeClass('jqTransformHidden').jqTransSelect();
        }
        frm.data('sizes-loaded', true);
    };

    $('.cart-page input[name="styleid"]').each(function(i, el) {
        styleIds.push(el.value);
    });
    $.ajax({
        type: 'GET',
        url: '/myntra/ajax_getsizes.php?ids=' + styleIds.join(','),
        dataType: 'json',
        beforeSend:function() {
        },
        success: function(resp) {
            if (!resp) {
                return;
            }
            dataSizes = resp;
            $('.form-cart-item, .form-saved-item').each(function(i, el) {
                loadSizes($(el));
            });
        },
        complete: function(xhr, status) {
        },
        error: function(xhr, textStatus, errorThrown) {
        }
    });

    /*
    $('.form-cart-item').on('mouseenter', function() {
        var frm = $(this);
        if (frm.data('sizes-loaded')) { return; }
        loadSizes(frm);
    });
    */

    $('.form-cart-item .sel-qty').each(function () {
    	this.onchange = function() {
	        var frm = $(this).closest('form'),
	            act = frm.find('input[name="update"]');
	        act.val('qty');
	        frm.submit();
	        var selectedQuantity = $(this).find('option:selected').val();
	        _gaq.push(['_trackEvent', 'cart', 'quantity_update_bag', selectedQuantity]);
	    };
    });

    function disablePlaceOrder() {
    	$(".btn-place-order").attr("disabled","disabled");
    }

    function enablePlaceOrder() {
    	$(".btn-place-order").removeAttr("disabled");
    }

    $('.form-cart-item .sel-size').each(function () {
	    this.onchange = function() {
	        var frm = $(this).closest('form'),
	            act = frm.find('input[name="update"]'),
	            qty = frm.find('.sel-qty');
	        act.val('size');
	        qty.find("option:selected").removeAttr("disabled");
	        disablePlaceOrder();
	        frm.submit();
	        var selectedSKUID = $(this).find('option:selected').val();
	        _gaq.push(['_trackEvent', 'cart', 'size_update_bag', selectedSKUID]);
	    };
    });

    $('.form-save-item').on('submit', function(e) {
        if (Myntra.Data.userEmail) {
            return;
        }
        e.preventDefault();
		var frm = $(this);
        $(document).trigger('myntra.login.show', {
            onLoginDone : function(){ frm[0].submit() },
            afterHide   : function(){ enablePlaceOrder() }
        });
    });

    $('.form-saved-item .sel-qty').each(function () {
    	this.onchange = function() {
	        var frm = $(this).closest('form'),
	            act = frm.find('input[name="actionType"]');
	        act.val('updateQuantity');
	        frm.submit();
	        var selectedQuantity = $(this).find('option:selected').val();
	        _gaq.push(['_trackEvent', 'cart', 'quantity_update_saved', selectedQuantity]);
	    };
    });

    $('.form-saved-item .sel-size').each(function () {
	    this.onchange = function() {
	        var frm = $(this).closest('form'),
	            act = frm.find('input[name="actionType"]');
	        act.val('updateSku');
	        frm.submit();
	        var selectedSKUID = $(this).find('option:selected').val();
	        _gaq.push(['_trackEvent', 'cart', 'size_update_saved', selectedSKUID]);
	    };
    });

	Myntra.InitCartComboOverlay();

	$(".mk-remove, .mk-move-to-wishlist").click(function(){
		disablePlaceOrder();
	});
    
        var ctx = $('.earn-cashback');
        if (!ctx.length) { return; }

        var lb = Myntra.LightBox('#cashback-info');
        ctx.click(function(e) {
            e.preventDefault();
	    lb.show();
        });

});

