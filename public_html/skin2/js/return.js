Myntra.WizardReturnReasonPolicy = function(conf) {
    var content, wiz, agree, items,
        isReset = true,
        reason, details, qty, lqty, loyaltyPoints,
        obj = Myntra.WizardStep();
    obj.set('title', 'Policy Agreement');
    obj.set('ajax_url', '/myntra/ajax_return.php');
    $('.returnbox .close').show();

    obj.init = function() {
        content = obj.get('content');
        wiz = obj.get('wiz');
        wiz.data('orig_qty', wiz.data('qty'));
        $([
            '<div class="step-agree">',
            '    <h2> Return Policy </h2>',
            '    <label>Please review our',
            '            <a href="/faqs#returns" target="_blank">Returns Policy</a></label>',
            '   <ol class="statements">',
            '       <li>',
            '           <input type="checkbox" name="i-agree" value="1">',
            '           I confirm that the product is unused and with original tags. All free gift(s)/Offer item(s) associated with this order are returned along with this product.',
            '       </li>',
            '    </ol>',
            '</div>'
        ].join('')).appendTo(content);

        agree = content.find('input[name="i-agree"]');
        items = content.find('.statements > li');
        agree.click(function() {
            var li = $(this).parent();
            li.next().length && li.next().removeClass('hide');
            obj.validate();
        });
        obj.agreeReset();
        obj.agreeUpdate();

        qty = content.find('select[name="qty"]');
        if (qty) {
            obj.validate();
            qty.change(function() {
                obj.validate();
            });
        }
        reason = content.find('select[name="reason"]');
        if (reason) {
            obj.validate();
            reason.change(function() {
                obj.validate();
            });
        }
        if (Myntra.Data.exchangesEnabled) {
            $('.returnbox .return-to-fork').show();
        }
        obj.ajax({
            _view: 'reason'
        }, 'init');

    };

    obj.agreeUpdate = function() {
        agree.filter(':not(:checked)').length ? wiz.disableNext('Please agree to the policy to proceed') : wiz.enableNext();

        qty = content.find('select[name="qty"]');
        if (qty) {
            if (wiz.data('qty') > 1 && !qty.val()) {
                wiz.disableNext('Please select a quantity');
            }
        }
        reason = content.find('select[name="reason"]');
        if (reason) {
            if (!reason.val()) {
                wiz.disableNext('Please select a reason');
            }
        }
    };

    obj.agreeReset = function() {
        agree.removeAttr('checked');
        items.addClass('hide').eq(0).removeClass('hide');
    };


    obj.update = function() {
        if (Myntra.Data.exchangesEnabled) {
            $('.return .return-to-fork').show();
        }
        if ($('.returnbox .close').html() === null) {
            var closeMarkup = '<div class="close"></div>';
            $('.returnbox .mod').append(closeMarkup);
            var close = $('.returnbox .mod .close');
            var wizObj = obj.get('wiz');
            close.click(function(e) {
                wizObj.hide();
            });
        }

        $('.returnbox .return-to-fork').click(function() {
            $('.returnbox .close').click();
            $(document).trigger('myntra.fork.show', conf);
        });


        // update the UI only after a reset
        if (!isReset) {
            return;
        }
        var val = wiz.data('qty');
        if (val > 1) {
            var markup = ['<option value="">Select a quantity</option>'];
            for (var i = 1; i <= val; i += 1) {
                markup[markup.length] = '<option value="' + i + '">' + i + '</option>';
            }
            qty.html(markup.join(' '));
            lqty.show();
            qty.parent().show();
        } else {
            qty.html('<option value="1" selected="true">1</option>');
            lqty.hide();
            qty.parent().hide();
        }
        isReset = false;
        obj.validate();
    };

    obj.onAjaxSuccess = function(resp, name) {
        $(resp.markup).appendTo(content);
        wiz.data('amtCredit', resp.amtCredit);
        //wiz.setFootNote('Need help? ' + resp.customerSupportTime + ' on <em>' + resp.customerSupportCall + '</em>');
        wiz.setFootNote('');

        reason = content.find('select[name="reason"]');
        details = content.find('textarea[name="details"]');
        qty = content.find('select[name="qty"]');
        lqty = content.find('.lbl-qty');

        reason.change(function() {
            obj.validate();
        });
        qty.change(function() {
            obj.validate();
        });
        obj.update();
    };

    obj.reset = function() {
        if (reason) {
            reason.val('');
        }
        if (details) {
            details.val('');
        }
        if (qty) {
            qty.val('');
        }
        isReset = true;
    };

    obj.validate = function() {
        wiz.enableNext();
        reason = content.find('select[name="reason"]');
        if (reason) {
            if (!reason.val()) {
                wiz.disableNext('Please select a reason');
            }
        }
        qty = content.find('select[name="qty"]');
        if (qty) {
            if (wiz.data('orig_qty') && !qty.val()) {
                wiz.disableNext('Please select a quantity');
            }
        }
        if (agree.filter(':not(:checked)').length) {
            wiz.disableNext('Please agree to the policy to proceed');
        }
    };

    obj.next = function() {
        wiz.data('reason', reason.val());
        wiz.data('details', details.val());
        wiz.data('qty', qty.val());
        wiz.data('i-agree', 'yes');
        wiz.notify('next');
    };

    return obj;
};


Myntra.WizardReturnAgree = function() {
    var content, wiz, agree, items,
        obj = Myntra.WizardStep();
    obj.set('title', 'Policy Agreement');
    obj.set('ajax_url', '/myntra/ajax_return.php');

    obj.init = function() {
        content = obj.get('content');
        wiz = obj.get('wiz');

        $([
            '<div class="step-agree">',
            '    <p>Please read and confirm the conditions for returning ',
            '            this item, as per our <a href="/faqs#returns" target="_blank">Returns Policy</a></p>',
            '   <ol class="statements">',
            '       <li>',
            '           <input type="checkbox" name="i-agree" value="1">',
            '           I agree that product being returned is unused and unwashed.',
            '       </li>',
            '       <li>',
            '           <input type="checkbox" name="i-agree" value="2">',
            '           I agree that product has all tags/stickers/accompanying material that the product was shipped with originally.',
            '       </li>',
            '       <li>',
            '           <input type="checkbox" name="i-agree" value="3">',
            '           I agree to ship the original order invoice and the packaging for the product.',
            '       </li>',
            '       <li>',
            '           <input type="checkbox" name="i-agree" value="4">',
            '           I have read and I am in agreement with all terms and conditions in ',
            '           <a href="/faqs#returns" target="_blank">Myntra’s Returns Policy</a>.',
            '       </li>',
            '    </ol>',
            '</div>'
        ].join('')).appendTo(content);

        agree = content.find('input[name="i-agree"]');
        items = content.find('.statements > li');
        agree.click(function() {
            var li = $(this).parent();
            li.next().length && li.next().removeClass('hide');
            obj.update();
        });
        obj.reset();
        obj.update();
    };

    obj.update = function() {
        agree.filter(':not(:checked)').length ? wiz.disableNext('Please agree to the policy to proceed') : wiz.enableNext();
    };

    obj.reset = function() {
        agree.removeAttr('checked');
        items.addClass('hide').eq(0).removeClass('hide');
    };

    obj.next = function() {
        wiz.data('i-agree', 'yes');
        wiz.notify('next');
    };

    return obj;
};


Myntra.WizardReturnShipping = function(cfg) {
    var content, wiz,
        isReset = true,
        obj = Myntra.WizardStep();
    obj.set('title', 'Shipping Details');
    obj.set('ajax_url', '/myntra/ajax_return.php');

    obj.init = function() {
        content = obj.get('content');
        wiz = obj.get('wiz');

        $([
            '<div class="step-shipping">',
            '   <p class="how">How do you want to return the item?</p>',
            '   <ul>',
            '       <li>',
            '           <input type="radio" name="ship-opt" value="self" class="ship-opt-self">',
            '           <div class="ship-type">ship it yourself</div>',
            '           <div class="ship-note">get a Self-Shipping Credit of <span class="rupees red">Rs. 100.00</span> per shipment.</div>',
            '       </li>',
            '       <li>',
            '           <input type="radio" name="ship-opt" value="pickup" class="ship-opt-normal">',
            '           <div class="ship-type">Select an address for pickup</div>',
            '           <div class="ship-note">where serviceable</div>',
            '       </li>',
            '   </ul>',
            '</div>'
        ].join('')).appendTo(content);
        content.jqTransform();


        content.find(':radio').click(function() {
            wiz.enableNext();
            wiz.data('ship-opt', $(this).val());
        });

        $(document).bind('myntra.address.list.done.return', function(e, data) {
            if (data.exchangeId) {
                return;
            }
            wiz.data('address', data);
            wiz.notify('next');
        });

        obj.update();
    };

    obj.update = function() {

        //pickup changes

        if (cfg && cfg.data && cfg.data.pickup_excluded) {
            content.find('.step-shipping ul li:eq(1)').hide();
            content.find('.step-shipping ul li:eq(0)').css('border-right', 'none');
        } else {
            content.find('.step-shipping ul li:eq(1)').show();
            content.find('.step-shipping ul li:eq(0)').css('border-right', '1px solid #d6d6d6');
        }


        $('.returnbox .return-to-fork').hide();
        if (!content.find(':radio').is(':checked')) {
            wiz.disableNext('Please choose how you want to return the item');
        }
    };

    obj.reset = function() {
        content.find(':radio').prop('checked', false).trigger('change');
    };

    obj.next = function() {
        if (wiz.data('ship-opt') == 'self') {
            wiz.notify('next');
        } else {
            var conf = {
                _type: 'return',
                isModal: true,
                displayName: 'pickup',
                itemid: wiz.data('itemId')
            };
            $(document).trigger('myntra.address.list.show', conf);
        }
    };

    return obj;
};


Myntra.WizardReturnRefundMode = function(cfg) {
    var content, wiz,
        isReset = true,
    obj = Myntra.WizardStep();
    obj.set('title', 'Mode of Refund');
    obj.set('ajax_url', '/myntra/ajax_return.php');

    obj.init = function() {
        content = obj.get('content');
        wiz = obj.get('wiz');

        // on payment method online show these refund options
        if(cfg.data.pm == "on"){

            $([
                '<div class="step-shipping">',
                '   <p class="how">How do you want the amount to be refunded?</p>',
                '   <ul>',
                '       <li>',
                '           <input type="radio" name="refundmode-opt" value="CASHBACK" class="ship-opt-self">',
                '           <div class="ship-type">Return as myntra.com cashback</div>',
                '           <div class="ship-note">We will credit as soon as your return is processed.', 
                '                You can use this cashback for your next purchase on myntra.com</div>',
                '       </li>',
                '       <li>',
                '           <input type="radio" name="refundmode-opt" value="OR" class="ship-opt-normal">',
                '           <div class="ship-type">Refund to credit/debit card</div>',
                '           <div class="ship-note">Amount will be refunded to your bank or card used in the order. ',
                '               <span id="neft-card-blocked" class="rupees" style="color:blue;cursor:pointer;">[card lost/blocked]</span>',                
                '           </div>',
                '       </li>',
                '   </ul>',
                '</div>'
            ].join('')).appendTo(content);

        // on payment method cod show these refund options
        } else if(cfg.data.pm == "cod"){

            $([
            '<div class="step-shipping">',
            '   <p class="how">How do you want the amount to be refunded?</p>',
            '   <ul>',
            '       <li>',
            '           <input type="radio" name="refundmode-opt" value="CASHBACK" class="ship-opt-self">',
            '           <div class="ship-type">Return as Myntra.com cashback</div>',
            '           <div class="ship-note">We will credit as soon as your return is processed.', 
            '                You can use this cashback for your next purchase on myntra.com</div>',
            '       </li>',
            '       <li>',
            '           <input type="radio" name="refundmode-opt" value="NEFT" class="ship-opt-normal">',
            '           <div class="ship-type">Refund to bank account via NEFT</div>',
            '           <div class="ship-note">You will be taken to a secure form for providing your bank account details</div>',
            '       </li>',
            '   </ul>',
            '</div>'
        ].join('')).appendTo(content);
        }
        
        content.jqTransform();

        // tooltip on card lost/blocked
        if($("#card-block-tooltip").length){
            $("#card-block-tooltip").remove();
        }

        tip = $([
            '<div id="card-block-tooltip" class="myntra-tooltip">',
            '   In case your card is lost or your account',            
            '   is blocked, please make use of "Return as Myntra.com cashback"',
            '</div>'
        ].join('')).appendTo('body');
        
        var ctx = $('.step-shipping span#neft-card-blocked');
        Myntra.InitTooltip(tip, ctx, {
                side: 'below'
            });
        

        // in the content markup, cashback option is made as default 
        // and hence trigger click so that next button in the wizard is enabled
        //$('input[type="radio"]:checked',content).trigger('click');
        

        content.find(':radio').click(function() {            
            wiz.enableNext();
            wiz.data('refundmode-opt', $(this).val());
        });
        
        $(document).bind('myntra.neft.account.list.done', function(e, data) {            
            wiz.data('neft-account', data);
            wiz.notify('next');
        });

        obj.update();
    };

    obj.update = function() {
        $('.returnbox .return-to-fork').hide();
        if (!content.find(':radio').is(':checked')) {
            wiz.disableNext('Please select a mode of refund');
        }
    };

    obj.reset = function() {
        content.find(':radio').prop('checked', false).trigger('change');
    };

    obj.next = function() {
        if (wiz.data('refundmode-opt') == 'CASHBACK') {
            wiz.notify('next');

        } else if (wiz.data('refundmode-opt') == 'OR') {
            wiz.notify('next');

        } else if (wiz.data('refundmode-opt') == 'NEFT') {
            var conf = {
                _type: 'NEFT',
                isModal: true,
                displayName: 'NEFT'                
            };
            $(document).trigger('myntra.neft.account.list.show', conf);

        } else {

        }
    };

    return obj;
};


Myntra.WizardReturnSummary = function(cfg) {
    var content, wiz,
        amountRefund = {},
        cashbackRefund = {},
    obj = Myntra.WizardStep();

    obj.set('title', 'Summary');
    obj.set('ajax_url', '/myntra/ajax_return.php');

    obj.init = function() {
        content = obj.get('content');
        wiz = obj.get('wiz');
        $([
            '<div class="step-summary">',
            '   <div class="product-details"></div>',
            '   <div class="address">',
            '       <div>',
            '          <div class="addr"></div>',
            '          <div class="refund-account"></div>',
            '       </div>',
            '          <div class="amount"></div>',
            '   </div>',
            '</div>'
        ].join('')).appendTo(content);        
        obj.update();
    };

    obj.onAjaxSuccess = function(resp, name) {
        var ckey = [resp.itemId, resp.qty, resp.shipOpt].join('-');
        amountRefund[ckey] = resp.amtRefund;
        cashbackRefund[ckey] = resp.cashbackRefund;        
        obj.update();
    };

    var num = function(amount) {
        return Myntra.Utils.formatNumber(amount) + '.00';
    }

    obj.update = function() {
        var address = wiz.data('address'),            
            neft_account = wiz.data('neft-account') ? wiz.data('neft-account') : {},
            addrMarkup = [],
            refundAccountMarkup = [],
            amtMarkup = [];        

        var ckey = [wiz.data('itemId'), wiz.data('qty'), wiz.data('ship-opt')].join('-');
        if (amountRefund[ckey] === undefined) {
            obj.ajax({
                _view: 'amt-refund',
                itemid: wiz.data('itemId'),
                qty: wiz.data('qty'),
                'shipopt': wiz.data('ship-opt')
            }, 'amt-refund');
            return;
        } else {
            wiz.data('amtRefund', amountRefund[ckey]);
            wiz.data('cbRefund', cashbackRefund[ckey]);            
        }

        content.find('.product-details').html("<div class='pcode'>Product Code : " + wiz.data('styleId') + " </div> " + wiz.data('styleName') + ", QTY:" + wiz.data('qty') + " <br/> Ordered Size:" + wiz.data('option') + "<br/><div><img src='" + cfg.img_url + "' /></div>");

        if (wiz.data('ship-opt') == 'self') {
            addrMarkup.push('<span style="margin-bottom:5px;color:black;font-size:14px;font-family:din-med, Arial, Sans-Serif;">SELF-SHIP</span>');
            //addrMarkup.push('<div class"amount"></div>');
        } else {
            addrMarkup.push('<span style="margin-bottom:5px;color:black;font-size:14px;font-family:din-med, Arial, Sans-Serif;">PICKUP ADDRESS</span>');
            addrMarkup.push('<span class="name">' + address.name + '</span>');
            addrMarkup.push(address.address.replace("\n", "<br>"));
            address.locality && addrMarkup.push(address.locality);
            addrMarkup.push(address.city + ' - ' + address.pincode);
            addrMarkup.push(address.state + ', India');
            addrMarkup.push('Mobile : ' + address.mobile);
        }
        content.find('.addr').html(addrMarkup.join('<br>'));        
        
        // remove previously selected NEFT account detail from summary on cashback
        if(wiz.data('refundmode-opt') == 'CASHBACK'){
           content.find('.refund-account').empty();
        }

        // add refund account detail here for OR/NEFT
        if (wiz.data('refundmode-opt') == 'OR') {
            refundAccountMarkup.push('<span style="margin-bottom:5px;color:black;font-size:14px;font-family:din-med, Arial, Sans-Serif;">ONLINE REFUND</span>');
            refundAccountMarkup.push('Refund will be made to your bank account or credit/debit card used against the original order.');
            content.find('.refund-account').html(refundAccountMarkup.join('<br>'));

        } else if(wiz.data('refundmode-opt') == 'NEFT'){
            refundAccountMarkup.push('<span style="margin-bottom:5px;color:black;font-size:14px;font-family:din-med, Arial, Sans-Serif;">NEFT REFUND</span>');
            refundAccountMarkup.push('<span class="name">' + neft_account.accountName + '</span>');            
            refundAccountMarkup.push('Account No : ' + neft_account.accountNumber);
            refundAccountMarkup.push('Type : ' + neft_account.accountType);
            refundAccountMarkup.push('Bank : ' + neft_account.bankName);            
            refundAccountMarkup.push('Branch : ' + neft_account.branch.replace("\n", "<br>"));
            refundAccountMarkup.push('IFSC : ' + neft_account.ifscCode);
            content.find('.refund-account').html(refundAccountMarkup.join('<br>'));
        }

        if (wiz.data('refundmode-opt') == 'OR' || wiz.data('refundmode-opt') == 'NEFT') {            
            amtMarkup.push('<div style="color:black;font-size:14px;font-family:din-med, Arial, Sans-Serif;"> REFUND AMOUNT </div> <br/><div style="font-size:12px;font-family:din-med, Arial, Sans-Serif;color:gray;text-transform:uppercase;"> You\'ll receive <span class="rupees red">Rs.', (parseFloat(wiz.data('amtRefund'))-parseFloat(wiz.data('cbRefund'))).toFixed(2), '</span></div>');            

        // cashback
        } else {
            amtMarkup.push('<div style="color:black;font-size:14px;font-family:din-med, Arial, Sans-Serif;"> REFUND AMOUNT </div> <br/><div style="font-size:12px;font-family:din-med, Arial, Sans-Serif;color:gray;text-transform:uppercase;"> You\'ll receive <span class="rupees red">Rs.', parseFloat(wiz.data('amtRefund')).toFixed(2), '</span></div>');    
        }

        
        if (wiz.data('ship-opt') == 'self') {
            amtMarkup.push('<div style="font-size:12px;text-transform:uppercase;color:gray;font-family:din-med, Arial, Sans-Serif;">And a Self-Ship Credit of <span class="rupees red">Rs.', num(wiz.data('amtCredit')), '</span>Per order/parcel*</div>');

            // show disclaimer in case of selfship return
            amtMarkup.push(
            '<div style="font-size:11px;text-transform:uppercase;color:green;;font-family:din-med, Arial, Sans-Serif;">',
            '   <br/><strong>Disclaimer : </strong> Rs.',num(wiz.data('amtCredit')),' on self ship will be added to your myntra cashback account.',
            '   This amount can not be transferred to card/bank account.',
            '</div>'
            );            
        }

        
        if (wiz.data('refundmode-opt') == 'OR') {
            amtMarkup.push('<div style="text-transform:uppercase;color:gray;font-size:10px;"><br/>WE TAKE 7-10 WORKING DAYS AFTER RECEIVING THE ITEM TO CREDIT YOUR REFUND AMOUNT BACK TO RESPECTIVE BANK ACCOUNT OR CREDIT/DEBIT CARD.</div>');

        } else if(wiz.data('refundmode-opt') == 'NEFT'){
            amtMarkup.push('<div style="text-transform:uppercase;color:gray;font-size:10px;"><br/>WE TAKE 3-5 WORKING DAYS AFTER RECEIVING THE ITEM TO CREDIT YOUR REFUND AMOUNT BACK TO THE ACCOUNT DETAIL YOU SHARED WITH US.</div>');
        // CASHBACK
        } else {
            amtMarkup.push('<div style="text-transform:uppercase;color:gray;font-size:10px;"><br/>WE TAKE 3-5 WORKING DAYS AFTER RECEIVING THE ITEM TO CREDIT YOUR MYNTRA CASHBACK ACCOUNT.</div>');    
        }
        
        if (Myntra.Data.loyaltyEnabled) {
            var lpAwarded = parseFloat(wiz.data('loyaltyPointsAwarded')),
                qty = parseInt(wiz.data('qty')),
                orig_qty = parseInt(wiz.data('orig_qty'));
            if (lpAwarded > 0 && qty > 0 && orig_qty > 0) {
                var loyaltyPointsDeducted = Math.round((qty / orig_qty) * lpAwarded);
                amtMarkup.push('<div style="color:black;font-size:14px;font-family:din-med, Arial, Sans-Serif;text-transform: uppercase;margin-top: 30px;">Privilege points</div>');
                amtMarkup.push('<div style="font-size:12px;text-transform:uppercase;color:#f7931e;font-family: din-med, Arial, Sans-Serif;margin-top: 7px;"><span class="icon-alert" style="display: inline-block;vertical-align: middle;"></span><span style="vertical-align: middle;display: inline-block;text-align: left;margin-left: 5px;width: 230px"> ' + loyaltyPointsDeducted + ' Points awarded for order will be reversed once this product is returned</span></div>');
            }
        }
        content.find('.amount').html(amtMarkup.join(' '));
    };

    return obj;
};


Myntra.WizardReturnConfirm = function(cfg) {
    var content, wiz,
        obj = Myntra.WizardStep();
    obj.set('title', '');
    obj.set('ajax_url', '/myntra/ajax_return.php');
    obj.set('hide_title', 1);

    obj.init = function() {
        content = obj.get('content');
        wiz = obj.get('wiz');
        obj.update();
    };

    obj.reset = function() {
        content.html('');
    };

    obj.onAjaxError = function(resp, name) {
        content.html(resp.markup);
        wiz.notify('error');
    };

    obj.onAjaxSuccess = function(resp, name) {

        content.html(resp.markup);
        $('.returnbox .close').hide();
        wiz.notify('done');
        wiz.setTitle('<span class="success-icon"></span> <span>Return Request Submitted!</span>');
        $(document).trigger('myntra.return.done', resp.data);

    };

    obj.update = function() {
        var params = $.extend({
            _view: 'confirm'
        }, wiz.data());
        if (params.address) {
            params.addressId = params.address.id;
            params.pickupCourier = params.address.pickupCourier;
            // TODO why do this, do i need to do the same for neft_account
            delete params.address;
        }
        $.extend(params, {
            _token: Myntra.Data.token
        });
        obj.ajax(params, 'next', 'POST');
    };

    return obj;
};


Myntra.WizardReturnBox = function(conf) {
    var cfg = conf.cfg || {};
    cfg.css = 'returnbox';
    
    var id = '#lb-item-return-'+cfg.key;
    
    if($(id).length){        
        $(id).remove();
    }

    var obj = Myntra.WizardBox(id, cfg),
    step1 = Myntra.WizardReturnReasonPolicy(conf),
    //step2 = Myntra.WizardReturnAgree(),
    step3 = Myntra.WizardReturnShipping(cfg);
    
    // add refund step only if feature gate is enabled and order amount>0
    // we are checking for order amount 1 because vat at order level is rounded to higher prise
    // which creates a difference of fraction and a Rs.0 order may become a Rs.0.** value order.
    if(Myntra.Data.refundEnabled && conf.data.orderTotal >= 1){
        var step4 = Myntra.WizardReturnRefundMode(cfg);
    }
    
    var step5 = Myntra.WizardReturnSummary(cfg);
    var step6 = Myntra.WizardReturnConfirm(cfg);

    obj.add(step1);
    //   obj.add(step2);
    obj.add(step3);

    // add refund step only if feature gate is enabled and order amount>0
    if(Myntra.Data.refundEnabled && conf.data.orderTotal >= 1){
        obj.add(step4);
    }
    
    obj.add(step5);
    obj.add(step6);

    var p_show = obj.show;
    obj.show = function(conf) {
        p_show.apply(obj, arguments);
        obj.setTitle('Return Item');
        obj.setSubTitle(obj.data('styleName'));
    };    
    return obj;
};


$(document).ready(function() {
    var obj;
    $(document).bind('myntra.return.show', function(e, cfg) {
        obj = Myntra.WizardReturnBox(cfg);        
        obj.show(cfg);
    });
});