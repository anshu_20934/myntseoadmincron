$(function() {
	//if (!Myntra.Data.enableMostPopular) return;
	
	$(window).load(function () {
		var pSection = $('.mk-product-section');
		$.ajax({
			type:"GET",
			url:http_loc+'/widgetcontroller.php?widget_name=ProductSections&lpname='+location.pathname.substring(1),
			dataType:"json",
			success: function(data){
				if(data.status==='SUCCESS'){
					//iterate thorugh the data initiate carousel if the flag is true.
					$.each(data.sectiondata,function(key,value){
						pSection.append(value['div_content']);	
						if(value['is_carousel']){
							$('.lp-section-carousel').carousel({
			    			itemsPerPage: 5, // number of items to show on each page
			    			itemsPerTransition:5, // number of items moved with each transition
			    			nextPrevActions: true, // whether next and prev links will be included
			    			pagination: false // whether pagination links will be included
			    			
			    		});
						}
						
    				});

				}
				// Colour Options
				Myntra.Search.Utils.initColourOptions();	
				var footer_block = $('<div class="lp-details"></div>'),__show_detail = 0;
				if(data.lpdetails && data.lpdetails.description){
					footer_block.append('<div class="lp-desc">'+data.lpdetails.description+'</div>');
					__show_detail = 1;
				
				}
				if(data.lpdetails && data.lpdetails.image){
					footer_block.prepend('<div class="lp-image"><img src="'+data.lpdetails.image+'" alt="'+data.lpdetails.name+'" /></div>');	
					__show_detail = 1;
				}
				
				if(__show_detail){
					footer_block.appendTo(pSection);
				}
					
				//GA calls
				$('.mk-product-section').on('click','h4 a',function(){
					_gaq.push(['_trackEvent', 'landing', pageName, $(this).text()]);
				}).on('click','ul li',function(){
					var sid=$(this).find('.quick-look').data('styleid');
					_gaq.push(['_trackEvent', 'landing', pageName, ''+sid]);
				});
						
			},	            
	        complete: function(){

	        }
		});
	});
});
