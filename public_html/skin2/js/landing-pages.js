$(function() {
	var ol_width=0;

	$('.trends-carousel').carousel({
		itemsPerPage: 1, // number of items to show on each page
		itemsPerTransition: 1, // number of items moved with each transition
		noOfRows: 1, // number of rows (see demo)
		nextPrevLinks: true, // whether next and prev links will be included
		pagination: true, // whether pagination links will be included
		speed: 'slow', // animation speed
		easing: 'swing',
		insertPagination:function(pagination){
			$(pagination).appendTo($('.thumbs-inner'));
			$(pagination).find('a').each(function(index){
				ol_width=ol_width+$(this).parent().width()+8;
				$(this).css('background','transparent url("'+Myntra.Data.trendsThumbs[index].image_url+'") no-repeat 0 0');
			});
			$('.rs-carousel-pagination').width(ol_width);
			$('.thumbs-inner').jScrollPane();
			return $(pagination);
		}
	});
	
	$('#trends-carousel').on('click', '.rs-carousel-pagination-link', function(){
		$('html, body').animate({scrollTop: $('h1').offset().top}, 800);
	}).on('click', '.rs-carousel-action-next', function(){
		_gaq.push(['_trackEvent', 'landing', 'fashion_trends', 'next']);
	}).on('click', '.rs-carousel-action-prev', function(){
		_gaq.push(['_trackEvent', 'landing', 'fashion_trends', 'prev']);
	}).on('click', '.rs-carousel-item a', function(){
		_gaq.push(['_trackEvent', 'landing', 'fashion_trends', 'click']);
	});
	
   	$(document).bind('keydown',function(e){
   		if(!$('#quick-look-mod').is(':visible')){
   			if(e.keyCode==39){    	   			
   				$('#trends-carousel .rs-carousel-action-next').trigger('click');
   			}
   			else if(e.keyCode==37){    				
   				$('#trends-carousel .rs-carousel-action-prev').trigger('click');
   			}
   		}
	});
});

$(window).load(function () {
	// Cycle Banners Gallery ----------------------
	$('.mk-cycle-wrapper').cycle({
		timeout: Myntra.Data.bannerCycleInterval,
		pager: '.mk-cycle-nav',
		before: function(currSlideElement, nextSlideElement, options, forwardFlag){
			$(".multiContainerHide").removeClass("multiContainerHide");
		}
	});	
});