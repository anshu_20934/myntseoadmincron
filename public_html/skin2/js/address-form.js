
Myntra.AddressFormBox = function(id, cfg) {
    cfg = cfg || {};
    cfg.isPopup = (typeof cfg.isPopup === 'undefined') ? true : cfg.isPopup;
    cfg.isModal = (typeof cfg.isModal === 'undefined') ? true : cfg.isModal;

    cfg.css = cfg.css || '';
    cfg.css += cfg.isPopup ? ' lightbox' : 'lightbox-nopop';

    var isInited = false, form, errors, elements, cache = {}, globalCache = {}, returnCache = {}, exchgCache = {}, inprogress = {},
        a_id, a_name, yourname, address, locality, selectedLocality, city, selCity,
        state, stateTxt, country, pincode, email, mobile,
        defaultName = '', defaultMobile = '',
        errPincode, pinwarn,
        idstr = id.substr(0, 1) === '#' ? id.substr(1) : id,
        obj,
        markup = [
        '<div id="' + idstr + '" class="address-form-box ' + cfg.css + '">',
        '<div class="mod">',
        '  <div class="hd">',
        '    <div class="title"></div>',
        '    <div class="subtitle"></div>',
        '  </div>',
        '  <div class="bd">',
        '  </div>',
        '  <div class="ft">',
        '    <span class="note"></span>',
        '    <button class="btn primary-btn btn-save">Save</button>',
        '    <button class="btn normal-btn btn-cancel">Cancel</button>',
        '  </div>',
        '</div>',
        '</div>'
    ].join('');
    cfg.container ? $(cfg.container).html(markup) : $('body').append(markup);
    obj = Myntra.LightBox(id, cfg);
    obj.root = $(id);
    obj.set('ajax_url', '/myntra/' + (location.protocol === 'https:' ? 's_' : '') + 'ajax_address.php');

    var hd = $('.mod > .hd', obj.root),
        bd = $('.mod > .bd', obj.root),
        ft = $('.mod > .ft', obj.root),
        title = $('.mod > .hd > .title', obj.root),
        subTitle = $('.mod > .hd > .subtitle', obj.root),
        ftNote = $('.mod > .ft > .note', obj.root),
        btnSave = $('.btn-save', obj.root),
        btnCancel = $('.btn-cancel', obj.root);

    obj.setTitle = function(markup) { title.html(markup) };
    obj.setSubTitle = function(markup) { subTitle.html(markup) };
    obj.setBody = function(markup) { bd.html(markup) };
    obj.setFootNote = function(markup) { ftNote.html(markup) };

    obj.enableSave = function() { btnSave.removeAttr('disabled') };
    obj.disableSave = function() { btnSave.attr('disabled', 'disabled') };
    obj.showSave = function() { btnSave.removeClass('hide') };
    obj.hideSave = function() { btnSave.addClass('hide') };
    obj.setSaveText = function(txt) { btnSave.text(txt) };

    if (!cfg.isPopup) {
        hd.remove();
        ft.remove();
    }

    btnSave.click(function(e) {
        obj.onSaveClick();
    });
    $(document).bind('myntra.address.form.save', function() {
        obj.onSaveClick();
    });

    btnCancel.click(function(e) {
        obj.hide();
    });

    var lockCityState = function() {
        state.addClass('hide');
        stateTxt.removeClass('hide');
        stateTxt.attr('disabled', 'true');
        city.attr('disabled', 'true');
    };

    var unlockCityState = function() {
        state.removeClass('hide');
        stateTxt.addClass('hide');
        stateTxt.removeAttr('disabled');
        city.removeAttr('disabled');
    };

    var stateName = function() {
        return $('select[name="state"] option:selected', obj.root).text();
    };

	var locSuccess = function(data) {
		var loc = locality.val(),
            opts, sel;
        if (loc.length && $.inArray(loc, data.locality) === -1) {
            locality.val('');
        }
		locality.autocomplete('option', 'source', data.locality);
        if (data.cities.length > 1) {
            opts = [];
            $.each(data.cities, function(i, val) {
                sel = val === city.val() ? ' selected ' : '';
                opts.push('<option value="' + val + '"' + sel + '>' + val + '</option>');
            });
            selCity.html(opts.join('')).removeClass('hide');
            city.addClass('hide').val(selCity.val());
        }
        else if (data.city) {
            selCity.html('').addClass('hide');
            city.val(data.city).removeClass('hide');
            city.data('err').text('');
            city.removeClass('error');
        }
        if (data.state) {
            state.val(data.state);
            state.data('err').text('');
            state.removeClass('error');
        }
        stateTxt.val($('select[name="state"] option:selected', obj.root).text());
        data.locality.length ? lockCityState() : unlockCityState();
	};

    var locFetch = function(pcode) {
        if (inprogress[pcode]) return;
        inprogress[pcode] = 1;
		$.ajax({
			url: '/myntra/' + (location.protocol === 'https:' ? 's_' : '') + 'ajax_pincode_locality.php?pincode=' + pcode,
			dataType: "json",
			beforeSend: function() {
				locality.attr('disabled', 'disabled');
				locality.addClass('ac_loading');
                lockCityState();
			},
			success: function(data) {
				if(cfg._type === "return"){
					cache = returnCache;
				}	
				else if(cfg._type === "exchange"){
					cache = exchgCache;
				}
				else{
					cache = globalCache;
				}
				cache[pcode] = data;
				locSuccess(data);
                //locality.trigger('keydown.autocomplete');
			},
			complete: function(xhr, status) {
				locality.removeAttr('disabled');
				locality.removeClass('ac_loading');
                delete inprogress[pcode];
			}
		});
    };

    var locInit = function() {
        locality.autocomplete({
            minLength: 0,
            source:[]
        });
        pincode.blur(function() {
            var pcode = pincode.val();
            if (!/\d{6}/.test(pcode)) {
                return
            }
            if ($(this).data('pcode') === pcode) {
                return;
            }
            $(this).data('pcode', pcode);
	    if(cfg._type === "return"){
            	cache = returnCache;
            }
            else if(cfg._type === "exchange"){
                cache = exchgCache;
            }
            else{
                cache = globalCache;
            }

            if (pcode in cache) {
                locSuccess(cache[pcode]);
                //locality.trigger('keydown.autocomplete');
                return;
            }
            locality.autocomplete('option', 'source', []);
            locFetch(pcode);
        });
        locality.bind('result', function(e, data) {
            selectedLocality.val(data[0]);
        });
        locality.focus(function(e) {
            var pcode = pincode.val();
            if (!/\d{6}/.test(pcode)) {
                return
            }
	    if(cfg._type === "return"){
                cache = returnCache;
            }
            else if(cfg._type === "exchange"){
                cache = exchgCache;
            }
            else{
                cache = globalCache;
            }

            if (pcode in cache) {
                if (locality.val().length == 0) {
                    locality.trigger('keydown.autocomplete');
                }
            }
            else {
                $(this).autocomplete('option', 'source', []);
                locFetch(pcode);
            }
        });
        // fetch the locality in Edit mode
        var pcode = pincode.val();
        if (pcode) { locFetch(pcode); }
    };

    var clearErrors = function() {
        elements = $('input, select, textarea', obj.root);
        elements.each(function(i) {
            var el = $(this),
                er = $('.err-' + el.attr('name'), obj.root);
            el.removeClass('error');
            er.text('');
        });
    };

    var resetForm = function() {
        clearErrors();
        unlockCityState();
        a_name.val(defaultName);
        address.val('');
        locality.val('');
        selCity.html('').addClass('hide');
        city.val('').removeClass('hide');
        state.val('');
        stateTxt.val('');
        pincode.val('');
        pincode.data('pcode', '');
        mobile.val(defaultMobile);
        try { pincode.focus() } catch (ex) {}
    };

    var toggleYourName = function(flag) {
        if (flag) {
            $('.copyname', obj.root).show();
            $('.yourname', obj.root).hide();
        }
        else {
            $('.copyname', obj.root).hide();
            $('.yourname', obj.root).hide();
        }
    };

    obj.onSaveClick = function() {
        var params = {_view:'address-form', _type:cfg._type, itemid: cfg.itemid};
        elements.each(function(i) {
            var el = $(this);
            if (el.attr('name') == 'state-txt') {
                return;
            }
            else if (el.attr('name') == 'copy-name') {
                if ($('.copyname', obj.root).is(':hidden')) {
                    return;
                }
                else if (!el.prop('checked')) {
                    return;
                }
            }
            params[el.attr('name')] = el.val();
        });
        obj.ajax(params, 'submit', 'POST');
    };

    obj.onAjaxError = function(resp, name) {
        if (name === 'submit') {
            if (resp.errors) {
                errors.text('');
                elements.removeClass('error');
                $.each(resp.errors, function(key, val) {
                    $('.err-' + key, obj.root).text(val);
                    $('.' + key, form).addClass('error');
                });
                if ('pincode' in resp.errors) {
                    _gaq.push(['_trackEvent', 'shipping_address', 'non_serviceable_zipcode', pincode.val()]);
                }
            }
            else {
                alert(resp.message);
            }
        }
    };

    obj.onAjaxSuccess = function(resp, name) {
        if (name === 'init') {
            isInited = true;

            obj.setBody(resp.markup);
            //obj.setFootNote('Need help? ' + resp.customerSupportTime + ' on <em>' + resp.customerSupportCall + '</em>');
            obj.setFootNote('');

            form = $('form', obj.root);
            errors = $('.err', obj.root);
            pinwarn = $('.pin-warn', obj.root);
            errPincode = $('.err-pincode', obj.root);

            a_id = $('input[name="id"]', obj.root);
            a_name = $('input[name="name"]', obj.root);
            yourname = $('input[name="yourname"]', obj.root);
            address = $('textarea[name="address"]', obj.root);
            city = $('input[name="city"]', obj.root);
            selCity = $('select[name="sel-city"]', obj.root);
            mobile = $('input[name="mobile"]', obj.root);
            pincode = $('input[name="pincode"]', obj.root);
            locality = $('input[name="locality"]', obj.root);
            selectedLocality = $('input[name="selected-locality"]', obj.root);
            state = $('select[name="state"]', obj.root);
            stateTxt = $('input[name="state-txt"]', obj.root);

            defaultName = a_name.val();
            defaultMobile = mobile.val();

            $('.copy-name', obj.root).change(function() {
                this.checked ? $('.yourname', obj.root).hide() : $('.yourname', obj.root).show();
            });

            selCity.change(function() {
                city.val(selCity.val());
            });

            elements = $('input, select, textarea', obj.root);
            elements.each(function(i) {
                var el = $(this),
                    er = $('.err-' + el.attr('name'), obj.root);
                el.data('err', er);
            });
            elements.blur(function() {
                var el = $(this);
                if (el.val().length > 0) {
                    el.removeClass('error');
                    el.data('err').text('');
                }
            });

            toggleYourName(resp.promptForName);
            locInit();
            obj.center();

            if (cfg.isPopup) {
                try { pincode.focus() } catch (ex) {}
            }
            $(document).trigger('myntra.address.form.oninit');
        }
        else if (name === 'load') {
            var d = resp.data;

            a_id.val(d.id);
            a_name.val(d.name);
            yourname.val(d.yourname);
            address.val(d.address);
            locality.val(d.locality);
            selCity.html('').addClass('hide');
            city.val(d.city).removeClass('hide');
            state.val(d.state);
            stateTxt.val(stateName());
            pincode.val(d.pincode);
            mobile.val(d.mobile);

            var pcode = pincode.val();
            if(cfg._type === "return"){
                cache = returnCache;
            }
            else if(cfg._type === "exchange"){
                cache = exchgCache;
            }
            else{
                cache = globalCache;
            }

            if (pcode in cache) {
                locSuccess(cache[pcode]);
            }

            toggleYourName(resp.promptYourName);
            clearErrors();
            obj.center();
            try { pincode.focus() } catch (ex) {}
        }
        else if (name === 'submit') {
            if (cfg.isPopup) {
                obj.hide();
            }
            $(document).trigger('myntra.address.form.done', resp);
        }
    };

    var p_show = obj.show;
    obj.show = function(a_cfg) {
        $.extend(cfg, a_cfg);
        if (cfg._type === 'return') {
            obj.setTitle('Create a return address');
        }
        else if (cfg._type === 'exchange') {
            obj.setTitle('Create a exchange address');
        }
        else if (cfg._action === 'edit') {
            obj.setTitle('Edit address');
        }
        else {
            obj.setTitle('Create address');
        }
        obj.setSubTitle('&nbsp;');
        p_show.apply(obj, arguments);

        var params = {_view:'address-form', _action:cfg._action};
        if (cfg._action === 'edit') { params.id = cfg.id; }
        params.markup = isInited ? 'no' : 'yes';

        !isInited ? obj.ajax(params, 'init') : cfg._action === 'edit' ? obj.ajax(params, 'load') : resetForm();
    };

    obj.render = function(a_cfg) {
        $.extend(cfg, a_cfg);
        var params = {_view:'address-form', markup:'yes', 'state-zips':1};
        obj.ajax(params, 'init');
    }

    return obj;
};

$(document).ready(function() {
    var obj;
    $(document).bind('myntra.address.form.show', function(e, cfg) {
        obj = obj || Myntra.AddressFormBox('#lb-address-form', cfg);
        obj.show(cfg);
    });
    $(document).bind('myntra.address.form.render', function(e, cfg) {
        obj = obj || Myntra.AddressFormBox('#lb-address-form', cfg);
        obj.render(cfg);
    });
});

