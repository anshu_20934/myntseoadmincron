
Myntra.NeftAccountFormBox = function(id, cfg) {
    cfg = cfg || {};
    cfg.isPopup = (typeof cfg.isPopup === 'undefined') ? true : cfg.isPopup;
    cfg.isModal = (typeof cfg.isModal === 'undefined') ? true : cfg.isModal;

    cfg.css = cfg.css || '';
    cfg.css += cfg.isPopup ? ' lightbox' : 'lightbox-nopop';

    var isInited = false, form, errors, elements,
        accNeftId, accName, accNumber, reAccNumber, accType, bankName, branch, ifsc,         
        idstr = id.substr(0, 1) === '#' ? id.substr(1) : id,
        obj,
        markup = [
        '<div id="' + idstr + '" class="address-form-box ' + cfg.css + '">',
        '<div class="mod">',
        '  <div class="hd">',
        '    <div class="title"></div>',
        '    <div class="subtitle"></div>',
        '    <div style="color:red;font-size:12px;"><strong>Disclaimer: </strong>Please ensure that the account details entered in this page are correct. The refund amount will be credited to this account after your returned product passes our quality check process.</div>',
        '  </div>',
        '  <div class="bd">',
        '  </div>',
        '  <div class="ft">',
        '    <span class="note"></span>',
        '    <button class="btn primary-btn btn-save">Save</button>',
        '    <button class="btn normal-btn btn-cancel">Cancel</button>',
        '  </div>',
        '</div>',
        '</div>'
    ].join('');
    cfg.container ? $(cfg.container).html(markup) : $('body').append(markup);
    obj = Myntra.LightBox(id, cfg);
    obj.root = $(id);
    obj.set('ajax_url', '/myntra/' + (location.protocol === 'https:' ? 's_' : '') + 'neft_account.php');

    var hd = $('.mod > .hd', obj.root),
        bd = $('.mod > .bd', obj.root),
        ft = $('.mod > .ft', obj.root),
        title = $('.mod > .hd > .title', obj.root),
        subTitle = $('.mod > .hd > .subtitle', obj.root),
        ftNote = $('.mod > .ft > .note', obj.root),
        btnSave = $('.btn-save', obj.root),
        btnCancel = $('.btn-cancel', obj.root);

    obj.setTitle = function(markup) { title.html(markup) };
    obj.setSubTitle = function(markup) { subTitle.html(markup) };
    obj.setBody = function(markup) { bd.html(markup) };
    obj.setFootNote = function(markup) { ftNote.html(markup) };

    obj.enableSave = function() { btnSave.removeAttr('disabled') };
    obj.disableSave = function() { btnSave.attr('disabled', 'disabled') };
    obj.showSave = function() { btnSave.removeClass('hide') };
    obj.hideSave = function() { btnSave.addClass('hide') };
    obj.setSaveText = function(txt) { btnSave.text(txt) };

    if (!cfg.isPopup) {
        hd.remove();
        ft.remove();
    }

    btnSave.click(function(e) {
        obj.onSaveClick();
    });
    

    btnCancel.click(function(e) {
        obj.hide();
    });

   
    var clearErrors = function() {
        elements = $('input, select, textarea', obj.root);
        elements.each(function(i) {
            var el = $(this),
                er = $('.err-' + el.attr('name'), obj.root);
            el.removeClass('error');
            er.text('');
        });
    };

    var resetForm = function() {
        clearErrors();        
        
        accNeftId.val('');
        accName.val('');
        accNumber.val('');
        reAccNumber.val('');
        //accType.val('');
        bankName.val('');
        branch.val('');
        ifsc.val('');

        try { accName.focus() } catch (ex) {}
    };
    

    obj.onSaveClick = function() { 
        var flag=confirm("Almost done! Just checking if your account details are correct and you want to proceed?");        
        
        if(flag){
            var params = {_view:'neft-account-form', _type:cfg._type};
            elements.each(function(i) {
                var el = $(this);            
                params[el.attr('name')] = el.val();
            });

            obj.ajax(params, 'submit', 'POST');
        }
        
    };

    obj.onAjaxError = function(resp, name) {        
        if (name === 'submit') {
            if (resp.errors) {
                errors.text('');
                elements.removeClass('error');
                $.each(resp.errors, function(key, val) {
                    $('.err-' + key, obj.root).text(val);
                    $('.' + key, form).addClass('error');
                });
                
            }
            else {
                alert(resp.message);
            }
        }
    };

    obj.onAjaxSuccess = function(resp, name) {        
        if (name === 'init') {
            isInited = true;

            obj.setBody(resp.markup);
            //obj.setFootNote('Need help? ' + resp.customerSupportTime + ' on <em>' + resp.customerSupportCall + '</em>');
            obj.setFootNote('');

            form = $('form', obj.root);
            errors = $('.err', obj.root);            
            
            accNeftId = $('input[name="id"]', obj.root);
            accName = $('input[name="account-name"]', obj.root);
            accNumber = $('input[name="account-number"]', obj.root);
            reAccNumber = $('input[name="re-account-number"]', obj.root);
            accType = $('select[name="account-type"]', obj.root);
            bankName = $('input[name="bank-name"]', obj.root);
            branch = $('input[name="branch"]', obj.root);
            ifsc = $('input[name="ifsc"]', obj.root);

            // disable cut, copy and paste for re enter account number field
            reAccNumber.bind("cut copy paste",function(e) {
                e.preventDefault();
            });
            
            elements = $('input, select, textarea', obj.root);
            elements.each(function(i) {
                var el = $(this),
                    er = $('.err-' + el.attr('name'), obj.root);
                el.data('err', er);
            });

            elements.blur(function() {
                var el = $(this);
                if (el.val().length > 0) {
                    el.removeClass('error');
                    el.data('err').text('');
                }
            });
            
            obj.center();

            if (cfg.isPopup) {
                try { accName.focus() } catch (ex) {}
            }
            $(document).trigger('myntra.address.form.oninit');

        } else if (name === 'load') {
            var d = resp.data;

            accNeftId.val(d.neftAccountId);
            accName.val(d.accountName);
            accNumber.val(d.accountNumber);
            reAccNumber.val(d.accountNumber);
            accType.val(d.accountType);
            bankName.val(d.bankName);
            branch.val(d.branch);
            ifsc.val(d.ifscCode);

            clearErrors();
            obj.center();
            try { accName.focus() } catch (ex) {}

        } else if (name === 'submit') {            
            if (cfg.isPopup) {
                obj.hide();
            }
            $(document).trigger('myntra.neft.account.form.done', resp);
        }
    };

    var p_show = obj.show;
    obj.show = function(a_cfg) {        
        $.extend(cfg, a_cfg);        
        obj.setTitle('Create a neft account');
        
        if (cfg._action === 'edit') {
            obj.setTitle('Edit account');
        }        

        obj.setSubTitle('&nbsp;');
        p_show.apply(obj, arguments);

        var params = {_view:'neft-account-form', _action:cfg._action};
        if (cfg._action === 'edit') { params.id = cfg.id; }
        params.markup = isInited ? 'no' : 'yes';

        !isInited ? obj.ajax(params, 'init') : cfg._action === 'edit' ? obj.ajax(params, 'load') : resetForm();
    };    

    return obj;
};

$(document).ready(function() {
    var obj;
    $(document).bind('myntra.neft.account.form.show', function(e, cfg) {
        obj = obj || Myntra.NeftAccountFormBox('#lb-neft-account-form', cfg);
        obj.show(cfg);
    });
});

