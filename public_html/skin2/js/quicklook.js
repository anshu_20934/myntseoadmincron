$(document).ready(function() {
        Myntra.InitQuickLook();
});


Myntra.InitQuickLook = function(){
	$(".quick-look").live('click', function(e){        
		var styleid=$(this).attr("data-styleid");
		var styleurl=$(this).attr("data-href");
		var uid = $(this).attr("data-uid");
        var fromCart= new Object();
            fromCart.is = 0;
            fromCart.combouid = uid;

		if($(this).hasClass('combo-quick-look')){
			fromCart.is = 1;
			return ;
		}
		if(typeof styleid !== "undefined" || styleid !== ""){
			Myntra.QuickLook.loadQuickLook(styleid, styleurl, fromCart);
			if(typeof $(this).attr("data-widget") != "undefined"){
				_gaq.push(['_trackEvent', 'quicklook', Myntra.Data.pageName, $(this).attr("data-widget"), parseInt(styleid)]);
			}
			else{
				_gaq.push(['_trackEvent', 'quicklook', window.location.toString(), styleid]);
			}
		}
        $(this).find( ".prev-next-btns" ).hide();
	});
	
	$(".mk-product, .mk-cycle-inner, .mk-saved-wrapper li, .cart-recommend-block ul > li,.look-styles ul >li,.mk-most-popular-ul >li,#mk-combo-box .combo-items li,.lp-section-carousel li,.look-carousel .product-section li,.look-items li").live("mouseenter", function(){
		$(this).find(".quick-look").show();
		var availability = $(this).find(".availability");
		if (availability.length) {
            $(this).css('z-index', '1');
			availability.find(".tooltip-content").text(availability.attr("data-sizes"));
            var __pos = availability.outerHeight();
			availability.css({visibility: 'visible', bottom: '-' + __pos + 'px'}).animate({opacity: 1}, 250);
		}
	});
	
	$(".mk-product, .mk-cycle-inner, .mk-saved-wrapper li, .cart-recommend-block ul > li,.look-styles ul >li,.mk-most-popular-ul >li,#mk-combo-box .combo-items li, .lp-section-carousel li,.look-carousel .product-section li,.look-items li").live("mouseleave", function(){
		$(this).find(".quick-look").hide();
		var availability = $(this).find(".availability"); 
		if (availability.length) {
			$(this).css('z-index', '');
			availability.css({bottom: 0, opacity: 0, visibility: 'hidden'});
			availability.find(".tooltip-content").text("");
		}
	});
    
    $( ".mk-product-large-image" ).live("mouseenter", function() {
        $(this).find( ".prev-next-btns" ).show();
    });
    $( ".mk-product-large-image" ).live("mouseleave", function() {
        $(this).find( ".prev-next-btns" ).hide();        
    }); 
};

Myntra.QuickLook = (function(){
	var obj = {};
	var lb;
	var quicklookthumbimginfo;
    
    obj.loadQuickLook = function(id, url, fromCart){
    	var t_code = Myntra.Utils.getQueryParam("t_code",url);
    	if(typeof Emark !== 'undefined'){
    		Myntra.emark = new Emark();
    		if (t_code != '') {
    			Myntra.emark.logClickedOn(id, t_code);
    		}
    		Myntra.emark.commit(function() {});
    	}
        
        /* Kuliza Code Start 
         * 
         * Handled the use case where a person does multiple fb sharing via quick look specifically on the PDP page
         * So the below div was going in the body tag since it is a lightbox div and hence not removed on removing #quick-look-mod. 
         * So, we had to remove it explicitly.
         * 
         * */
        if($('.echo-pdp-lightbox-quicklook-yes').length > 0){
            $('.echo-pdp-lightbox-quicklook-yes').remove();
        }
        /* Kuliza Code End */
        
    	if(!lb){
    		lb = Myntra.LightBox('#quick-look-mod');
    	}
    	lb.show();
    	lb.clearPopupContent();
    	obj.getData(id, url, fromCart);
    	lb.beforeHide=function(){
    		delete Myntra.Data.MiniPIPsocialExclude;
       	};
    	return lb;
    };
    
    obj.getData = function(id, url, fromCart){
    	lb.showLoading();
    	lb.center();
		var gurl = http_loc+"/getMiniPIP.php?id="+id+"&fc="+fromCart.is;
		if(fromCart.is && fromCart.combouid != undefined && fromCart.combouid != ''){
			gurl = gurl + '&combouid='+fromCart.combouid;
		}
    	$.get(gurl, null, function(data){
    		lb.hideLoading();
    		$("#quick-look-mod .bd").html(data);
    		$("#quick-look-mod .mk-more-info, #quick-look-mod h1 a").attr("href", url);
    		Myntra.Utils.loadImageQueue();
    		Myntra.Utils.SizesTooltip.InitTooltip();
    		obj.initMiniPIPThumbnails();
    		lb.center();
    		init();
            if(showQuickLookCouponWidget){
                obj.initOfferDetails();
            }            
    	});
    };
    var initKeyNextPrev = function(){
    	$(document).unbind('keydown.minipip');
    	 $(document).bind('keydown.minipip',function(e){
			 if($('#quick-look-mod').is(':visible')){
				 if(e.keyCode==37){
					$('#quick-look-mod #view-prev-minipip').trigger('click');
				}
				 else if(e.keyCode==39){
					 $('#quick-look-mod #view-prev-minipip').trigger('click');
				 }
			 }
		 });
    };
    var init = function(){
    	var minipipPrGuide = $('#minipip-product-guide .mk-product-option-cont');
    	initKeyNextPrev();
    	$('.mk-colour li.rs-carousel-item a', minipipPrGuide).die().live('click', function () {
    		var url = this.href,
    			id = $(this).attr('data-id');
    		obj.getData(id, url, {is:0});
    		_gaq.push(['_trackEvent', 'minipip', 'color_select', ''+$(this).data('id')]);
    		return false;
        });
    	Myntra.Utils.initCarousel(minipipPrGuide);
    	
    	$("#quick-look-mod .mk-more-info").die().live("click", function(event){
    		_gaq.push(['_trackEvent', 'pdp_click', Myntra.Data.pageName, 'quicklook_view_details']);
            event.stopPropagation();
    	});
    	
    	$("#quick-look-mod .rs-carousel-action-next").die().live("click", function(event){
    		_gaq.push(['_trackEvent', 'minipip', 'next_image', '']);
            event.stopPropagation();
    	});
    	
    	$("#quick-look-mod .rs-carousel-action-prev").die().live("click", function(event){
    		_gaq.push(['_trackEvent', 'minipip', 'prev_image', '']);
            event.stopPropagation();
    	});
    	
    	$("#quick-look-mod .mk-save-for-later").die().live("click", function(event){
    		_gaq.push(['_trackEvent', 'minipip', 'saveinwishlist', '']);
            event.stopPropagation();
    	});
    	
    	$('#quick-look-mod .mk-size').die().live('change',function (event) {
    		_gaq.push(['_trackEvent', 'minipip', 'size_select', $(this).find('option:selected').text()]);
    		event.stopPropagation();
    	});
    	//SET VARIABLE FOR SOCIAL
    	Myntra.Data.MiniPIPsocialExclude=$('#minipip-data').attr('data-mini-exclude')=='false'?false:true;
    	Myntra.Data.socialAction=$('#minipip-data').attr('data-socialaction');
    };
    
    obj.initMiniPIPThumbnails = function(){
		// Product Image Switch ---------------------
		$('#quick-look-mod .mk-more-views li a').click(function(e){e.preventDefault();});
		$('#quick-look-mod .mk-more-views li img').click(function() {
			$('#quick-look-mod .mk-more-views li').removeClass('img-selected');
			$(this).parent().parent().addClass('img-selected');
			var ind=$('#quick-look-mod .mk-more-views li.img-selected').index();
			$('#quick-look-mod .mk-product-large-image img').attr('src', $(this).attr('data-src'));
		});
		$('#quick-look-mod .mk-more-views img').live("mouseenter", function() {
			$(this).click();
		});
		if($('#quick-look-mod .mk-more-views li').length > 1){
			$('#quick-look-mod #view-next-minipip, #quick-look-mod #view-prev-minipip').hide();
			$('#quick-look-mod #view-next-minipip').bind('click',function(e){
				var ind=$('#quick-look-mod .mk-more-views li.img-selected').index();
				((ind+1)==$('#quick-look-mod .mk-more-views li').length)?ind=0:ind=ind+1;
				obj.transitionMiniPIPImage(ind);
				e.stopPropagation();
			});
			$('#quick-look-mod #view-prev-minipip').bind('click',function(e){
				var ind=$('#quick-look-mod .mk-more-views li.img-selected').index();
				(ind==0)?ind=($('#quick-look-mod .mk-more-views li').length)-1:ind=ind-1;
				obj.transitionMiniPIPImage(ind);
				e.stopPropagation();
			});
		}
		else{
			$('#quick-look-mod #view-next-minipip, #quick-look-mod #view-prev-minipip').hide();
		}
		$('#quick-look-mod .mk-more-views li img').each(function(){
			var img = new Image();
			img.src = $(this).attr('data-src');
		});
    };	

    obj.transitionMiniPIPImage = function(ind){
    	$('#quick-look-mod .mk-more-views li:eq('+(ind)+') img').trigger('click');
    };

    obj.initOfferDetails = function(){
        console.log('init');
        this.couponMore = '';
        this.couponData = '';
        this.couponMessage = '';
        this.couponMessagecont = '';
        this.__couponText ='';
        this.numberOfCoupons = 0;        
        this.couponAbTest = quickLookCouponWidgetABTest;
        couponPlaceholder();
        this.couponAjaxcall = false;          
        var that = this;

        if (that.couponAbTest == 'test') {                
            that.CouponWidgetObj = $("#quicklook-coupon-widget");
            $(".ql-coupon-offer-launcher").click( function(e) {
                that.CouponWidgetObj.show();
                hideColorGroup();
                hideDiscount();
                showOfferArrow(); 
                that.CouponWidgetObj.append('<div class="loading"></div>');
                that.CouponWidgetObj.find('.loading').show();                                
                
                if (that.couponData === '') {
                    if (that.couponAjaxcall === false) {
                        _gaq.push(['_trackEvent','quicklook_offer_button', 'first_click', that.id, 1, true]);
                        obj.getCouponData();
                        that.couponAjaxcall = true;
                    } else {
                        that.renderNoCouponMessage(that.couponMessage);
                    }
                }
                else{
                    _gaq.push(['_trackEvent','quicklook_offer_button', 'subsequent_click', that.id, 1, true]);                    
                    that.renderItems(that.couponData);
                }
            });

            $("#quicklook-coupon-widget .close").click( function(e) {
                that.CouponWidgetObj.hide();
                showDiscount();
                showColorGroup();
                hideOfferArrow();
            }
            );
        }
    };

    var couponPlaceholder = function(){
        this.couponRoot = $("#quicklook-coupon-widget");
    };

    var hideColorGroup = function(){
        $(".mk-product-option-cont").find(".mk-colour").hide();
    };

    var showColorGroup = function(){
        $(".mk-product-option-cont").find(".mk-colour").show();
    };

    var hideDiscount = function(){
        $("#minipip-product-guide").find(".mk-discount").hide();
    };

    var showDiscount = function(){
        $("#minipip-product-guide").find(".mk-discount").show();
    };

    var showOfferArrow = function(){
        $(".ql-coupon-offer-launcher").find(".offer-arrow").show();
    };

    var hideOfferArrow = function(){
        $(".ql-coupon-offer-launcher").find(".offer-arrow").hide();
    };

    obj.getCouponData = function(){
        //fire ajax to get JSON.
        //call render
        var that = this;
        $.get(
            http_loc+"/ajaxPDPCouponWidget.php",
            { styleid: styleId, price:parseInt(originalprice)}
            ).done(function ( data ) {
                data = $.parseJSON(data);                    
                if(data.status == 'success'){
                    that.couponData = data.data;
                    that.couponMessage = data.message || '';
                    that.numberOfCoupons = data.numberOfCoupons;
                    $("#quicklook-coupon-widget").show();
                    that.renderItems(that.couponData);
                }
                else if(data.status == 'nocoupon' && data.message){
                    that.couponMessage = data.message;
                    $("#quicklook-coupon-widget").show();
                    that.renderNoCouponMessage(data.message);
                }
            }
        );
    };

    obj.renderNoCouponMessage = function(message){
        $('#quicklook-coupon-widget .no-coupon .message').html(message);
        $('#quicklook-coupon-widget .no-coupon').show();
        this.CouponWidgetObj.find('.loading').hide();
    }; 

    obj.renderItems = function(data){        
        var coupon = data.coupons[0];
        //computations for showing whether VAT is applicable or not
        var percentageDiscount = (coupon.price/coupon.originalPrice)*100;
        $('#quicklook-coupon-widget .coupon-discounted-price').html(coupon.price);
        if( coupon.couponExtraTNC != ""){
        	$('#quicklook-coupon-widget .coupon-tnc').html(coupon.couponExtraTNC);
        }
        $('#quicklook-coupon-widget .coupon-code').html(coupon.coupon);
        $('#quicklook-coupon-widget .coupon-discount').html(coupon.coupondiscount);
        $('#quicklook-coupon-widget .coupon-validity').html(coupon.enddate);
        $('#quicklook-coupon-widget .offer-details').show();
        if (coupon.minimum > 0) {
            $('#quicklook-coupon-widget .offer-details .coupon-details .min-amount').html(coupon.minimum);
            $('#quicklook-coupon-widget .offer-details .coupon-details .is-min-condition').removeClass('hide');
        }
        if(showVatMessage){
            $('#quicklook-coupon-widget .coupon-vat-message').html("* "+vatCouponMessage);
        }
        else if(percentageDiscount > parseInt(vatThreshold)){                
            $('#quicklook-coupon-widget .coupon-vat-message').html("* "+vatCouponMessage);   
        }
        this.CouponWidgetObj.find('.loading').hide();
    };
    
    return obj;

})();

