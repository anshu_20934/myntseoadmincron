
(function() { "use strict";
	var pdpCouponBox = [
	'<div id="pdp-coupon-box">',
		//'	<ul id="coupon-item-first"></ul>',
		//'	<div class="slide-cont hide">',
		//'		<ul id="coupon-items">',
		//'		</ul>',	
	//	'	</div>',
	'</div>'
	].join(''),

	pdpCouponTitleText = [
	'<div id="pdp-coupon-title-text">'+Myntra.PDP.couponWidgetTitleText,        
	'</div>'

	].join(''),

	pdpCouponDescription = [
	'<div id="pdp-coupon-description" class="hide">',
	'</div>'
	].join(''),

	pdpCouponItem = [
	'<li class="coupon-item">',
	'   <p class="price red">Rs. {price}</p>',
	'   <p class="coupon">by applying coupon "{coupon}" in the bag.<br><span class="coupon-details">see Details</span>',
	'   </p>',
	'</li>'
	].join('');

	Myntra.PDPCouponWidget = {
		init:function(){
			//initialise the widget
			console.log('init');
			this.more = '';
			this.data = '';
			this.message = '';
			this.messagecont = '';
			this.__text ='';
			this.numberOfCoupons = 0;
			this.ttip = {};
            this.abTest = Myntra.PDP.couponWidgetABTest;
			this.placeholder();
			this.ajaxcall = false;			
			var that = this;

            if (that.abTest == 'test') {
                var pholder = this.root.find('#pdp-coupon-title-text');
                pholder.click(function(){
                    if($('#pdp-coupon-description').length > 0){
                        _gaq.push(['_trackEvent','pdp_offer_button', 'subsequent_click', Myntra.PDP.Data.id, 1, true]);
                        $('#pdp-coupon-description').css("display","");					
                        $('#pdp-coupon-description').toggleClass("hide");
                    }
                    else{
                        if(that.ajaxcall == false){
                            _gaq.push(['_trackEvent','pdp_offer_button', 'first_click', Myntra.PDP.Data.id, 1, true]);
                            that.getData();
                            that.ajaxcall = true;
                        }
                    }

                });
            } else if (that.abTest == 'test2') {
                //the following for version 2 of this pdpCouponWidget launcher
                that.LightBoxObj = Myntra.LightBox("#lb-coupon-widget");
                $(".coupon-offer-launcher").click( function(e) {
                        that.LightBoxObj.show();
                        that.LightBoxObj.showLoading();
                        if (that.data == '') {
                            if (that.ajaxcall == false) {
                                _gaq.push(['_trackEvent','pdp_offer_button', 'first_click', Myntra.PDP.Data.id, 1, true]);
                                that.getData();
                                that.ajaxcall = true;
                            } else {
                                that.renderNoCouponMessageV2(that.message);
                            }
                        } else {
                            _gaq.push(['_trackEvent','pdp_offer_button', 'subsequent_click', Myntra.PDP.Data.id, 1, true]);
                            that.renderItemsV2(that.data);
                        }
                    }
                );

                $("#lb-coupon-widget .close").click( function(e) {
                        that.LightBoxObj.hide();
                    }
                );
            }
		},

		placeholder: function(){
            if (this.abTest == 'test') {
                this.root = $(pdpCouponBox).prependTo('.mk-product-sidebar');
                this.root.html(pdpCouponTitleText);
            } else if (this.abTest == 'test2') {
                $(".coupon-offer-launcher").show();
                this.root = $("#lb-coupon-widget");
            }
		},


		getData: function(){
			//fire ajax to get JSON.
			//call render
			var that = this;
			$.get(
				http_loc+"/ajaxPDPCouponWidget.php",
				{ styleid: Myntra.PDP.Data.id, price:parseInt(Myntra.PDP.Data.price)}
				).done(function ( data ) {
					data = $.parseJSON(data);
					console.log('done',data.status);
					if(data.status == 'success'){
                        that.data = data.data;
                        that.message = data.message || '';
                        that.numberOfCoupons = data.numberOfCoupons;


                        if (that.abTest == 'test') {
                            that.root = $(pdpCouponDescription).appendTo('#pdp-coupon-box');
                                console.log('success');
                                that.root.prepend('<h4>GET THIS PRODUCT FOR</h4><ul id="coupon-item-first"></ul>');
                                that.couponItemsFirst = that.root.find('#coupon-item-first');
                            //that.couponItems = that.root.find('#coupon-items');
                            that.slideCont = that.root.find('.slide-cont');
                            that.renderItems();
                            if(data.message)
                            {
                                $('<div id="pdp-coupon-message-separator"></div>').appendTo(that.root)
                                $('<div class="fest-pdp-text" id="fest-pdp-text">'+data.message+'</div>').appendTo(that.root);
                                var ctx = $('#fest-pdp-text');
                                var ppup = $('#fest-pdp-popup');
                                if (ctx.length && ppup.length) {
                                    var lb = Myntra.LightBox('#fest-pdp-popup');
                                    ctx.on('click', function(e) {
                                        e.preventDefault();
                                        lb.show();
                                    });
                                }
                                that.root.slideDown(500);
                                $('#pdp-coupon-description').toggleClass("hide");
                            }
                        } else if (that.abTest == 'test2') {
                            that.renderItemsV2(that.data);
                        }
				}
				else if(data.status == 'nocoupon' && data.message){
                    that.message = data.message;

                    if (that.abTest == 'test') {
                        that.root = $(pdpCouponDescription).appendTo('#pdp-coupon-box');
                        $('<div class="fest-pdp-text" id="fest-pdp-text">'+data.message+'</div>').prependTo(that.root);
                        var ctx = $('#fest-pdp-text');
                        var ppup = $('#fest-pdp-popup');
                        if (ctx.length && ppup.length) {
                            var lb = Myntra.LightBox('#fest-pdp-popup');
                            ctx.on('click', function(e) {
                                e.preventDefault();
                                lb.show();
                            });
                        }
                        that.root.slideDown(500);
                        $('#pdp-coupon-description').toggleClass("hide");
                    } else if (that.abTest == 'test2') {
                        that.renderNoCouponMessageV2(data.message);
                    }
				}
			});
}, 

renderNoCouponMessageV2:function(message){
    $('#lb-coupon-widget .no-coupon .message').html(message);
    $('#lb-coupon-widget .no-coupon').show();
    this.LightBoxObj.hideLoading();
}, 
renderItems:function(){
			//render through the JSON and template and append to dom,
			//initialise events
			var data, markup, item,dispData,coupons,couponData,markupFinal = [],count=1;
			for(coupons in this.data.coupons){
				couponData = this.data.coupons[coupons];
				if(count == 1){
					//construct first item and append
					markup = pdpCouponItem.replace(/\{(.*?)\}/g, function(str, key) {
						if(key in couponData) {

							return couponData[key];	
						}
						return str;
					});
					console.log(couponData);
					var item = this.couponItemsFirst.html(markup);
	            	// '<p>GET '.$coupondiscount.' off on '.($minimum>0?'a minimum purchase of Rs'.$minimum:'this product').'.</p><br/>'
	            	var tip = $([
	            		'<div class="coupon-tooltip myntra-tooltip">',
	            		'   <div class="hd">Coupon Details</div>',
	            		'   <div class="bd">',
	            		'		<p class="details">',
	            		'			Get ',couponData['coupondiscount'],' off on ',
	            		'			',(couponData['minimum']>0?'a minimum purchase of Rs: '+couponData['minimum']:'this product'),
	            		'			<br/>',
	            		'			Valid till ',couponData['enddate'],
	            		'		</p>',
	            		'   </div>',	            		
	            		'</div>'
	            		].join('')).appendTo('body');

	            	 //= $('<div class="coupon-tooltip myntra-tooltip">'+couponData['tooltip']+'</div>').appendTo('body');

	            	 this.ttip[couponData['coupon']] = new Myntra.Tooltip(tip, item.find('.coupon'), {side:'above'});
					/******CCOMMENTED . SHOWING ONLY ONE COUPON. Uncomment later to show all coupons in slide option
					/*if(this.message && this.message != '')
						this.messagecont = $('<p class="message">'+this.message+'</p>').appendTo(this.slideCont);		
					if(this.numberOfCoupons && this.numberOfCoupons > 1)
						this.more = $('<p class="more-button close">more offers (<span>'+(this.numberOfCoupons-1)+'</span>)</p>').appendTo(this.root);*/
				}else{
					//******CCOMMENTED . SHOWING ONLY ONE COUPON. Uncomment later to show all coupons in slide option
				/*	console.log(couponData);
					markup = pdpCouponItem.replace(/\{(.*?)\}/g, function(str, key) {
	            	   	if(key in couponData) {

	                		return couponData[key];	
	                	}
	                	return str;
	            	});
	            	//markupFinal.push(markup);
	            	var item = $(markup).appendTo(this.couponItems);
	            	var tip = $('<div class="coupon-tooltip myntra-tooltip">'+couponData['tooltip']+'</div>').appendTo('body');
	            	this.ttip[couponData['coupon']] = new Myntra.Tooltip(tip, item.find('.coupon'), {side:'above'});*/
	            }
	            count ++;
	        }
			//this.couponItems.html(markupFinal.join(''));

			/******CCOMMENTED . SHOWING ONLY ONE COUPON. Uncomment later to show all coupons in slide option
			if(this.numberOfCoupons == 1){
				this.more.on('click',$.proxy(this.toggleBox, this));
				this.__text = this.more.html();
			}
			if(this.numberOfCoupons == 1){
				this.slideCont.show();
			}*/
			this.root.slideDown(500);

		},

        renderItemsV2:function(data){            
            var coupon = data.coupons[0];
            //computations for showing whether VAT is applicable or not
            var percentageDiscount = (coupon.price/coupon.originalPrice)*100;            
            $('#lb-coupon-widget .coupon-discounted-price').html(coupon.price);
            if( coupon.couponExtraTNC != ""){
            	$('#lb-coupon-widget .coupon-tnc').html(coupon.couponExtraTNC);
            }
            $('#lb-coupon-widget .coupon-code').html(coupon.coupon);
            $('#lb-coupon-widget .coupon-discount').html(coupon.coupondiscount);
            $('#lb-coupon-widget .coupon-validity').html(coupon.enddate);
            $('#lb-coupon-widget .offer-details').show();
            if (coupon.minimum > 0) {
                $('#lb-coupon-widget .offer-details .coupon-details .min-amount').html(coupon.minimum);
                $('#lb-coupon-widget .offer-details .coupon-details .is-min-condition').removeClass('hide');
            }
            if(showVatMessage){
                $('#lb-coupon-widget .coupon-vat-message').html("* "+vatCouponMessage);
            }
            else if(percentageDiscount > parseInt(vatThreshold)){                
                $('#lb-coupon-widget .coupon-vat-message').html("* "+vatCouponMessage);   
            }
            this.LightBoxObj.hideLoading();
        }


		/******CCOMMENTED . SHOWING ONLY ONE COUPON. Uncomment later to show all coupons in slide option*/
		/*toggleBox : function(){
			var that = this;
			this.slideCont.slideToggle('slow',function(){
				if(that.more.hasClass('close')){
					that.more.html('hide offers');
					that.more.removeClass('close');
					
				}
				else {
					that.more.html(that.__text);
					that.more.addClass('close');
				}
			});
}*/


}
}());

/* Load Recently Viewed Widget*/
$(window).load(function () {
	console.log(Myntra.PDP.showCouponWidget);
	Myntra.PDP.showCouponWidget && Myntra.PDPCouponWidget.init();
});
