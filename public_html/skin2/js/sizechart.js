/*
On click of "Find my size", If the mastercatogory is footwear 3 jax requests(Sizechart page, brand list, size list) are fired async.
And on response of all 3, the page is bind with data and displayed.
If master catogory is not footwear only the page request will be fired. Since Brand to brand size compare is not available for non-footwear.
*/

(function() {
	//Constants
	var NoToSCROLL = 5;
	//Llobals
	var AjaxResponse = {
		skuToMeasurementsMap: {},
		brandList: {}
	},
		AjaxWaitingCount = 0,
		LightBoxObj,
		lightBoxCfg = {},
		passObj; //Check where this is used and optimise by making it local


	//Create a lightbox.
	$(document).ready(function() {
		//Code for Size chart link --- Start
		//check if it is 'size chart' link 
		if ($('#sizechart-old')[0]) {
			$('body').append('<div id="lb-sizechart-old" class="lightbox lb-sizechart-old">' +
				'	<div class="mod">' +
				'		<div class="bd" style="max-width: 840px;">' +
				'			<img class="sizechart-old-img" src="' + $('#sizechart-old').attr('data-src') + '">' +
				'		</div>' +
				'	</div>' +
				'</div>');
			lightBoxCfg = {
				isModal: false,
				afterShow: function() {
					LightBoxObj.center();
				}
			};
			LightBoxObj = Myntra.LightBox("#lb-sizechart-old", lightBoxCfg);

			$('#sizechart-old').click(function(e) {
				e.preventDefault();
				LightBoxObj.show();
				if (typeof Myntra.Data.sizechartCookieConstants != "undefined") {
					trackClick();
				}
			});

			/**
			 * Kundan: now we track: which were the styles for which user clicked on their size charts: old or new
			 * We maintain the list of unique style ids in cookie.
			 * After order is placed sucessfully,this cookie is read, the style-ids in order are matched with these styles
			 * and the matched style-ids are then pushed to Mongo DB eventually.
			 */

			function trackClick() {
				var currCookieValue = Myntra.Utils.Cookie.get(Myntra.Data.sizechartCookieConstants.OldSizechartCookie);
				if (currCookieValue != null) {
					currCookieValue = decodeURIComponent(currCookieValue);
				}
				var newCookieValue = Myntra.Utils.setCompositeData(curr_style_id, currCookieValue, true, Myntra.Data.sizechartCookieConstants.Delimiter, Myntra.Data.sizechartCookieConstants.MaxStylesPerCookie);
				Myntra.Utils.Cookie.set(Myntra.Data.sizechartCookieConstants.OldSizechartCookie, newCookieValue, Myntra.Data.sizechartCookieConstants.Expiry);
			}

			return;
		}
		//Code for Size chart link --- End

		//Code of 'Find my size' link --Bigins and continue till end of this page
		$('body').append('<div id="lb-sizechart" class="lightbox lb-sizechart"><div style="min-height:478px" class="mod"></div></div>');
		LightBoxObj = Myntra.LightBox("#lb-sizechart", lightBoxCfg);

		$("#sizechart-new").click(function(e) {
			e.preventDefault();
			_gaq.push(['_trackEvent', 'sizechart', 'sizechart', 'sizechart-pdp-link']);
			show();
			if (typeof Myntra.Data.sizechartCookieConstants != "undefined") {
				trackClick(curr_style_id);
			}
		}); //Show is a singleton function

		$("#sizechart-new").hover(
			function(e) {
				$('.sizechart-icon,.sizechart-link-text').addClass('hover');
			},
			function(e) {
				$('.sizechart-icon,.sizechart-link-text').removeClass('hover');
			});
	});

	lightBoxCfg.beforeHide = function() {
		var sizeSelBlock = $("#size-select");
		if (sizeSelBlock.find('li.size-val-sel').length) {
			updatePdpSize(sizeSelBlock.find('li.size-val-sel').attr('data-value'));
		} else if (sizeSelBlock.find('li.size-val-sel-inactive').length) {
			updatePdpSize(sizeSelBlock.find('li.size-val-sel-inactive').attr('data-value'));
		}
	};

	function onAjaxLoad(caller) {
		AjaxWaitingCount -= 1;
		onAjaxLoad.callers = onAjaxLoad.callers || [];
		if (AjaxWaitingCount != 0) {
			(caller && onAjaxLoad.callers.push(caller));
		} else {
			onAjaxLoad.callers.forEach(function(c) {
				c();
			});
			(caller && caller());
			LightBoxObj.hideLoading();
		}
	}

	function show(sizeObj) {
		LightBoxObj.show();
		LightBoxObj.showLoading();

		//Ajax for popup page
		ajaxMethods.loadPage(onPageLoad, onPageLoadComplete);
		AjaxWaitingCount += 1;
		if (Myntra.PDP.Data.masterCategory === "Footwear") {
			//Ajax for size list object
			ajaxMethods.getSizeList();
			AjaxWaitingCount += 1;
			//Ajax for Brandlist
			ajaxMethods.getBrandlist();
			AjaxWaitingCount += 1
		}

		//This scrolling should not happen for the initial loading, that's y setting here.
		lightBoxCfg.afterShow = function() {
			setTimeout(scrollToSelection, 0)
		};

		//Singleton.
		show = function(sizeObj) {
			LightBoxObj.show();
		}

		function onPageLoad(data) {
			if (data) {
				if (!$('#lb-sizechart .size-scrollable-box').length) {
					$('#lb-sizechart .mod').append(data);
					onAjaxLoad(InitPagePresentation);
				}
			}
		}

		function onPageLoadComplete() {
			lightBoxCfg.beforeShow = updateSelectedSize;
			//LightBoxObj.hideLoading();
		}
	}

	function scrollToSelection() {
		//This is the block in scroll to function
		var sizeSelBlock = $("#size-select");
		var sizeScroll = $(".size-scrollable");
		var countLi = sizeSelBlock.find('li').length;
		if (countLi > NoToSCROLL) {
			//SCROLL PAGE DETERMINE HERE
			//get the index of item to scroll here
			var selectedIndex = (sizeSelBlock.find('li.size-val-sel-inactive').length) ? sizeSelBlock.find('li.size-val-sel-inactive').index() : sizeSelBlock.find('li.size-val-sel').index();
			sizeScroll.each(function() {
				$(this).data('carousel').goToItem(selectedIndex);
			});
		}
	}

	function InitPagePresentation() {

		LightBoxObj.center();
		
		var sizeSelBlock = $("#size-select");
		passObj = {
			"text": sizeSelBlock.find('li.size-val-sel').text(),
			"val": sizeSelBlock.find('li.size-val-sel').length ? sizeSelBlock.find('li.size-val-sel').attr('data-value') : ''
		}
		//replace this block with a single fun call with createMeasureBlock, and do this inside that.
		if (typeof sizePositionObj[Myntra.Data.ageGroup.toLowerCase()] !== 'undefined') {
			if (typeof sizePositionObj[Myntra.Data.ageGroup.toLowerCase()][Myntra.Data.articleType.toLowerCase()] !== 'undefined') {
				createMeasureBlocks(sizePositionObj[Myntra.Data.ageGroup.toLowerCase()][Myntra.Data.articleType.toLowerCase()]);
			}
		}
		//Moved here after the measurement blocks are created
		$('.size-image').css('background-image', 'url(' + Myntra.Data.sizeImg + ')'); //Loading Main Image(size);

		//infoOpen is not seen in that page, confirm and check if this block required.
		var infoOpen = $('#info-open');
		infoOpen.toggle(
			function() {
				infoOpen.removeClass('inactive').addClass('active');
				infoBlock.slideUp();
			},
			function() {
				infoOpen.removeClass('active').addClass('inactive');
				infoBlock.slideDown();
			});

		//Move these blocks to a seperate function addbuttonclickhandlers
		var sizeSelLi = $("#size-select li.size-available");
		var addToBtn = $('#sub-btn .sub-btn');

		sizeSelLi.bind("click", function() {
			_gaq.push(['_trackEvent', 'sizechart', 'sizechart', 'sizechart-size-available']);
			var sizeInd = $(this).index();
			var selVal = $(this).text();
			var jsonObj = Myntra.Data.sizeMeasurementsObj[sizeInd];
			switchMeasurements(jsonObj, selVal);
			$(this).parent().find('li').removeClass('size-val-sel size-val-sel-inactive');
			$(this).addClass('size-val-sel');
			passObj.val = $(this).attr('data-value');
			passObj.text = $(this).text();
			addToBtn.css('cursor', 'pointer').removeAttr('disabled');
			$('.out-of-stock').removeClass('visible');

		});

		var sizeOutOfStock = $("#size-select li.size-inactive");
		sizeOutOfStock.bind("click", function() {
			_gaq.push(['_trackEvent', 'sizechart', 'sizechart', 'sizechart-size-unavailable']);
			var sizeInd = $(this).index();
			var selVal = $(this).text();
			var jsonObj = Myntra.Data.sizeMeasurementsObj[sizeInd];
			switchMeasurements(jsonObj, selVal);
			$(this).parent().find('li').removeClass('size-val-sel size-val-sel-inactive');
			$(this).addClass('size-val-sel-inactive');
			passObj.val = '';
			passObj.text = '';
			addToBtn.css('cursor', 'not-allowed').attr('disabled', 'disabled');
			$('.out-of-stock').addClass('visible');
		});

		addToBtn.bind('click', function() {
			_gaq.push(['_trackEvent', 'sizechart', 'sizechart', 'sizechart-buy-button']);
			if (sizeSelBlock.find('li.size-val-sel').length) {
				passObj.val = sizeSelBlock.find('li.size-val-sel').attr('data-value');
			}

			if (passObj.val != '') {
				addtocart(passObj.val);
			} else return;
		});

		//Check what is this block doing
		var sizeScroll = $(".size-scrollable");
		var equiSizeUL = '.equi-sizes';
		var countLi = sizeSelBlock.find('li').length;
		var liWidth = sizeSelBlock.find('li').width();
		if (countLi < NoToSCROLL) {
			var ulWidthToSet = (liWidth * countLi) + (countLi - 1);
			//calculte the margin inorder to align to center
			var marginToSet = ((250 - ulWidthToSet) / 2);
			sizeScroll.css({
				'width': ulWidthToSet + 'px',
				'margin-left': marginToSet + 'px',
				'margin-right': marginToSet + 'px'
			});
		}

		//Update Background of Equivalent sizes
		$(equiSizeUL + ':odd').addClass('odd');


		$('.lb-sizechart .tab-btn').live('click', function() {
			_gaq.push(['_trackEvent', 'sizechart', 'tab-open', $(this).attr('data-tabid')]);
		});

		$("select.brand").live('change', function() {
			var selectedAgeGroup = Myntra.Data.ageGroup;
			var selectedArticleType = Myntra.Data.articleTypeId;
			var selectedBrand = $("option:selected", this).val();
			var selectedSize = $("option:selected", $("select.size")).val();
			if (selectedAgeGroup != -1 && selectedArticleType != -1) {
				ajaxMethods.getBrandSizelist(selectedBrand);
			}
			$('.brand-size .brand-size-size-selector').removeClass('hide');
			$('.brand-size .suggested').addClass('hide');
			$(".myntra-tooltip.measurement-tooltip").remove();
			$(".brand-size-size-selector").addClass("unselected");
			_gaq.push(['_trackEvent', 'sizechart', 'brand2brand', 'brand-select']);
		});

		$("select.size").live('change', function() {
			var selectedValue = $("option:selected", this).val();
			if (selectedValue != -1) {
				var selectedSizeData = $.parseJSON($("option:selected", this).attr("data-extra"));
				var targetMeasurements = "";
				for (var measureName in selectedSizeData) {
					if (selectedSizeData.hasOwnProperty(measureName)) {
						targetMeasurements += measureName + " : " + selectedSizeData[measureName]['value']['value'] + " " + selectedSizeData[measureName]['value']['unit'] + "<BR>";
					}
				}

				var selectedBrandName = $("option:selected", $("select.brand")).text();
				var selectedBrandSize = $("option:selected", this).text();
				populateMeasurementDiffs("update");
				// Since we are initializing measurements we
				// pass noTracking flag as true to remove any
				// tracking calls during initialization
				displayMeasurementDiffs(true);
				var bestFitSize = populateMeasurementDiffs()["best-fit-size"];
				var sizeDisplayName = $(".size-scrollable-box span:eq(0)").text();
				$(".target-size").text(sizeDisplayName + " " + bestFitSize);
				$(".best-fit-size-text").html(sizeDisplayName + " " + bestFitSize + " in " + Myntra.Data.brand + " " + Myntra.Data.articleType + " is the closest fit to " + selectedBrandName + " " + selectedBrandSize);
				if (!$('.brand-size-title').hasClass('selected')) {
					$('.brand-size-title').addClass('selected');
				}
				$('.brand-size-title').removeClass('tooltip');
				$('.brand-size-title-arrow, .brand-size-title-arrow-inner').hide();
				$(".suggested").removeClass("hide");
				$(".brand-size-size-selector").removeClass("unselected");
				_gaq.push(['_trackEvent', 'sizechart', 'brand2brand', 'size-select']);
			} else {
				$(".target-size").text("?");
				$(".suggested").addClass("hide");
				$(".best-fit-size-text").html("Select Brand & Size to get the best fit in " + Myntra.Data.brand + " " + Myntra.Data.articleType);
			}
		});

		//Bind tab clicks so that measurements are updated on tab switching
		$(".tab-btn").bind("click", function() {
			var tabid = $(this).attr('data-tabid');
			var otherTabid = tabid == "0" ? 1 : 0;
			var otherTab = $('.tab-btn[data-tabid="' + otherTabid + '"]');
			var sizeBtn;
			if ($(otherTab.length > 0)) {
				if (tabid === "0") {
					sizeBtn = $('#size-select .item.size-val-sel', '.tab[data-tabid="0"]');
				} else {
					sizeBtn = $('.suggested-sizes .size-btn.selected', '.tab[data-tabid="1"]');
				}
				var sizeInd = $(sizeBtn).index();
				var selVal = $(sizeBtn).text();
				var jsonObj = Myntra.Data.sizeMeasurementsObj[sizeInd];
				switchMeasurements(jsonObj, selVal);
			}
			//Scrolling to the selection when the first tab is clicked
			((tabid === "0") && scrollToSelection());
		});

		//This is a block moved from onAfterShow
		$(equiSizeUL).each(function() {
			var h_ = $(this).height();
			$(this).parent().height(h_);
		});
		var inOfSel = sizeSelBlock.find('li.size-val-sel').index();
		if (inOfSel == -1) {
			inOfSel = sizeSelBlock.find('li.size-val-sel-inactive').index();
		}
		//inti scroll after load	
		if (sizeScroll.length && $('.size-scroll-btns .rs-carousel-runner > li').size() > 5) {
			sizeScroll.carousel({
				itemsPerPage: 5, // number of items to show on each page
				itemsPerTransition: 5, // number of items moved with each transition
				noOfRows: 1, // number of rows (see demo)
				nextPrevLinks: true, // whether next and prev links will be included
				pagination: false, // whether pagination links will be included
				speed: 'normal' // animation speed	
			});
			$('.size-scroll-btns a.rs-carousel-action-next').click(function() {
				$('.size-scroll-equi a.rs-carousel-action-next').trigger('click');
			});
			$('.size-scroll-btns a.rs-carousel-action-prev').click(function() {
				$('.size-scroll-equi a.rs-carousel-action-prev').trigger('click');
			});

		}

		updateSelectedSize();
		scrollToSelection();
		//selecting the 2nd tab by default
		$('.tab-btns  li.default').click();

		//Populating the Brandlist
		Myntra.Utils.populateComboViaJson(AjaxResponse.brandList, $("select.brand"), "Brand I wear");

		function displayMeasurementDiffs(noTracking) {
			var sizeDisplayName = $(".size-scrollable-box span:eq(0)").text();
			var bestFitSkuid = populateMeasurementDiffs()["best-fit-skuid"];
			var diffs = populateMeasurementDiffs()["diffs"];
			var selectedBrandName = $("option:selected", $("select.brand")).text();
			var selectedBrandSize = $("option:selected", $("select.size")).text();
			$(".suggested").removeClass("hide");
			$('.size-unavailable-text').hide();;
			$(".suggested-sizes").empty();
			$(".suggested-sizes").append($('.flat-size-options button').clone());
			$(".suggested-sizes .size-btn").removeClass('vtooltip').removeClass('best-fit').removeClass('selected').removeAttr('data-tooltiptop').removeAttr('data-tooltiptext').removeAttr('title');
			$(".select-size-div").show();
			if ($(".suggested-sizes .size-btn[value=" + bestFitSkuid + "]").hasClass('unavailable')) {
				$('.size-unavailable-text').show();
			}
			$(".myntra-tooltip.measurement-tooltip").remove();
			$(".suggested-sizes .size-btn").each(function() {
				var skuid = $(this).attr("value");
				var diff = diffs[skuid];
				var diffMsg = '';
				var diffTitle = '';
				var size = $(this).remove('span').html();
				//console.log(measurement);
				for (var measurement in diff) {
					if (measurement != "ssd") {
						var measurementValue = diff[measurement];
						var diffVal = Math.abs(measurementValue[2]).toFixed(2) + " " + measurementValue[3];
						var moreOrLess = (measurementValue[2] == 0) ? "exactly same as" : (measurementValue[2] > 0) ? diffVal + " more than" : diffVal + " less than";
						var diffEachMeasureMsg = Myntra.Data.brand + " measures " + moreOrLess + " " + selectedBrandName + " in " + measurement;
						diffMsg += "<div>" + diffEachMeasureMsg + "</div>";
						if (diffTitle == '') {
							if (skuid == bestFitSkuid) {
								diffTitle = sizeDisplayName + " " + size + " in " + Myntra.Data.brand + " " + Myntra.Data.articleType + " is the closest fit to " + selectedBrandName + " " + selectedBrandSize;
								(new Myntra.Tooltip($('<div class="measurement-tooltip closest-fit myntra-tooltip">Closest Fit</div>'), ".suggested-sizes .size-btn[value=" + skuid + "]", {
									side: 'above',
									useTitle: true,
									nohover: true
								}));
							} else if (measurementValue[2] > 0) {
								diffTitle = sizeDisplayName + " " + size + " in " + Myntra.Data.brand + " " + Myntra.Data.articleType + " is <span class='red'>larger fit</span> to " + selectedBrandName + " " + selectedBrandSize;
								(new Myntra.Tooltip($('<div class="measurement-tooltip myntra-tooltip">Larger</div>'), ".suggested-sizes .size-btn[value=" + skuid + "]", {
									side: 'above',
									useTitle: true,
									nohover: true
								}));
							} else {
								diffTitle = sizeDisplayName + " " + size + " in " + Myntra.Data.brand + " " + Myntra.Data.articleType + " is the <span class='red'>smaller fit</span> to " + selectedBrandName + " " + selectedBrandSize;
								(new Myntra.Tooltip($('<div class="measurement-tooltip myntra-tooltip">Smaller</div>'), ".suggested-sizes .size-btn[value=" + skuid + "]", {
									side: 'above',
									useTitle: true,
									nohover: true
								}));
							}
						}
					}
				}
				$(this).attr('data-measurement-diff-text', diffMsg);
				$(this).attr('data-measurement-diff-title', diffTitle);
				$(this).click(function() {
					if (!$(this).hasClass('unavailable')) {
						$('.size-unavailable-text').hide();
					} else {
						$('.size-unavailable-text').show();
					}
					if (skuid == bestFitSkuid) {
						$(this).removeClass('highlighted');
					} else {
						$('.suggested-sizes .size-btn[value="' + bestFitSkuid + '"]').addClass('highlighted');
					}
					$('.measurement-diff-text').html($(this).attr('data-measurement-diff-text')).fadeIn();
					$('.best-fit-size-text').html($(this).attr('data-measurement-diff-title')).fadeIn();
					var sizeInd = $(this).index();
					var selVal = $(this).text();
					var jsonObj = Myntra.Data.sizeMeasurementsObj[sizeInd];
					switchMeasurements(jsonObj, selVal);
					_gaq.push(['_trackEvent', 'sizechart', 'brand2brand', 'size-click']);
				});
			});
			//setTimeOut added because the tooltip will be hidden othervise.(Because of the propagation of the click even in size select, now this timeout will ensure this click of the element to be after that sizeselect click event propagation)
			setTimeout(function() {
				$(".suggested-sizes .size-btn[value=" + bestFitSkuid + "]").addClass('best-fit').click();
			}, 0);
			if (!noTracking) {
				setTimeout(function() {
					$(".suggested-sizes .size-btn[value=" + bestFitSkuid + "]").click();
				}, 500);
			}
			$('.brand-size button.select-size').click(function() {
				LightBoxObj.hide();
				var selectedSKU = $(".suggested-sizes .size-btn.selected").attr('value');
				//Select the size in size options tab
				$('#size-select li[data-value="' + selectedSKU + '"]').click();
				updatePdpSize(selectedSKU);
				_gaq.push(['_trackEvent', 'sizechart', 'brand2brand', 'select-size-button']);
			});
		}

		function populateMeasurementDiffs(update) {
			if (!update && populateMeasurementDiffs.measurementDiffs) {
				return populateMeasurementDiffs.measurementDiffs;
			}

			var measurementDiffs = {};
			var selectedSizeMeasurements = $("option:selected", $("select.size")).attr("data-extra");
			var minSSD = 10000;
			var minSSDSku = -1;
			for (var skuid in AjaxResponse.skuToMeasurementsMap) {
				measurementDiffs[skuid] = compareMeasurements(selectedSizeMeasurements, AjaxResponse.skuToMeasurementsMap[skuid]);
				//find the minimum SSD & Index
				if (measurementDiffs[skuid].ssd < minSSD) {
					minSSDSku = skuid;
					minSSD = measurementDiffs[skuid].ssd;
				}
			}
			populateMeasurementDiffs.measurementDiffs = {
				"best-fit-skuid": minSSDSku,
				"best-fit-size": getTargetSizes().skuToSizeMap[minSSDSku],
				"diffs": measurementDiffs
			};

			function compareMeasurements(mjson1, mjson2) {
				var mjobj1 = JSON.parse(mjson1.toLowerCase());
				var mjobj2 = JSON.parse(mjson2.toLowerCase());
				var measurementsToCompare = {};
				var ssd = 0; //represents sum of squared differences
				for (measurement in mjobj1) {
					if (typeof mjobj2[measurement] !== 'undefined') {
						//now check if their type is flat and their units are the same
						if (mjobj2[measurement]["type"] === "flat" && mjobj1[measurement]["type"] === "flat" && mjobj1[measurement]["value"]["unit"] === mjobj2[measurement]["value"]["unit"]) {
							var value1 = parseFloat(mjobj1[measurement]["value"]["value"]);
							var value2 = parseFloat(mjobj2[measurement]["value"]["value"]);
							var diff = value2 - value1;
							ssd += diff * diff;
							measurementsToCompare[measurement] = [value1, value2, diff, mjobj2[measurement]["value"]["unit"]];
						}
					}
				}
				measurementsToCompare["ssd"] = parseFloat(Math.sqrt(ssd).toFixed(2));
				return measurementsToCompare;
			}
		}

		function addtocart(selSizeName) {
			var formToSubmit = $('#sizeForm-' + curr_style_id);
			var quantity = formToSubmit.find(":hidden[name='quantity']").val();
			var sku_ = selSizeName;

			var size = sizeSelBlock.find('li.size-val-sel').text();
			var productStyleId = formToSubmit.find(":hidden[name='productStyleId']").val();
			formToSubmit.find(":hidden[name='sizequantity[]']").val(quantity);
			formToSubmit.find(":hidden[name='productSKUID']").val(sku_);
			formToSubmit.find(":hidden[name='selectedsize']").val(size);
			formToSubmit.submit();
			$('.mk-size-error').css('display', 'none');
		}

		function createMeasureBlocks(mPosObj) {
			var sizeImgCont = $('#measurement-box');
			var blockHeight = 400;
			var index = 1;
			for (var key in mPosObj) {
				if (mPosObj.hasOwnProperty(key)) {
					//console.log('key->',key,'<===> value->',mPosObj[key]);
					$('<span></span>').addClass('measurement ' + key.replace(/ /g, '-').toLowerCase()).css({
						'left': mPosObj[key][0] + 'px',
						'top': mPosObj[key][1] + 'px'
					}).appendTo(sizeImgCont);
					//Create Illustration block and position it use sizePositionObj[indexed]
					$('<span></span>').addClass('size-image ' + key.replace(/ /g, '-').toLowerCase() + '-dotted').css({
						'background-position': '0 -' + (blockHeight * index) + 'px',
						'visibility': 'hidden'
					}).appendTo(sizeImgCont);
				}
				index = index + 1;
			}

		}
	}

	//Getting the values for skuids and skuToSizeMap

	function getTargetSizes() {
		var skuids = [],
			skuToSizeMap = {},
			sizeSelBlock = $("#size-select"),
			skuid;

		$("li", sizeSelBlock).each(function() {
			skuid = $(this).attr("data-value");
			skuToSizeMap[skuid] = $(this).text();
			skuids.push(skuid);
		});

		return {
			skuids: skuids.join(","),
			skuToSizeMap: skuToSizeMap
		}

	}

	function switchMeasurements(jsonObj, selVal) {
		var sizeImgCont = $('#measurement-box');
		for (var key in jsonObj) {

			if (jsonObj.hasOwnProperty(key)) {
				switch (jsonObj[key]['type']) {
					case "flat":
						if (jsonObj[key]['value']['value'] != null && jsonObj[key]['value']['value'] != '' && jsonObj[key]['value']['value'] != "null") { //show/hide the illustration lines here
							$('.' + key.replace(/ /g, '-').toLowerCase() + '-dotted').css({
								'visibility': 'visible'
							});
							if ($('span.' + key.replace(/ /g, '-').toLowerCase()).length) {
								$('span.' + key.replace(/ /g, '-').toLowerCase()).hide();
								$('span.' + key.replace(/ /g, '-').toLowerCase()).html('<span >' + key + '</span><br/>' + jsonObj[key]['value']['value'] + ' ' + (jsonObj[key]['value']['unit'] == 'Inches' ? 'inches' : jsonObj[key]['value']['unit'])).fadeIn(1200);
							}
							//Give info incase of footwear
							if (Myntra.Data.articleTypeId == 127 || Myntra.Data.articleTypeId == 92 || Myntra.Data.articleTypeId == 93 || Myntra.Data.articleTypeId == 94 || Myntra.Data.articleTypeId == 95 || Myntra.Data.articleType == 96) {
								var infoText = '* Foot size ' + jsonObj[key]['value']['value'] + ' ' + (jsonObj[key]['value']['unit'] == 'Inches' ? 'inches' : jsonObj[key]['value']['unit']) + ' fits Indian Size <b>' + selVal + '</b>';
								if ($('.foot-size').length) {
									$('.foot-size').fadeOut(function() {
										$(this).html(infoText).fadeIn();
									});
								} else {
									$('<span></span>').addClass('foot-size').html(infoText).appendTo(sizeImgCont);
								}
							}
						}
						break;
					case "variable":
						var variableKey = Myntra.Data.styleSizeAttributes[key];
						sizeImgCont.find('.' + key.replace(/ /g, '-').toLowerCase()).hide();
						sizeImgCont.find('.' + key.replace(/ /g, '-').toLowerCase()).html('<span>' + key + '</span><br/>' + jsonObj[key][variableKey]['value'] + ' ' + (jsonObj[key][variableKey]['unit'] == 'Inches' ? 'inches' : jsonObj[key]['value']['unit'])).fadeIn(1200);
						break;
					default:
						break;
				}
			}
		}
	}


	function updatePdpSize(value) {
		$('.flat-size-options .size-btn[value="' + value + '"]').trigger('click');
		$('#productSKUID').val('');
	}

	function getBrandComplete(argument) {
		$("div.tab.brand-size select").removeAttr("disabled");
	}

	function getBrandBeforeSend() {
		$("div.tab.brand-size select").attr("disabled", "disabled");
	}

	function updateSelectedSize(sizeObj) {
		sizeObj = sizeObj || getSelectedSize();
		// Check whether the page loaded before updating.

		var selCount = 0;
		var sizeSelLi = $("#size-select li.size-available");
		var sizeOutOfStock = $("#size-select li.size-inactive");
		var addToBtn = $('#sub-btn .sub-btn');
		var sizeSelBlock = $("#size-select");

		if (sizeObj.text != '' && sizeObj.text != 'SELECT A SIZE') {

			sizeSelLi.removeClass('size-val-sel');
			sizeOutOfStock.removeClass('size-val-sel-inactive');
			$('#size-select li').each(function() {
				if ($(this).attr('data-value') == sizeObj.val) {
					if ($(this).hasClass('size-available')) {
						$(this).addClass('size-val-sel');
						addToBtn.css('cursor', 'pointer').removeAttr('disabled');
						$('.out-of-stock').removeClass('visible');
						passObj.val = $(this).attr('data-value');
					} else {
						$(this).addClass('size-val-sel-inactive');
						addToBtn.css('cursor', 'not-allowed').attr('disabled', 'disabled');
						$('.out-of-stock').addClass('visible');
						passObj.val = '';
						passObj.text = '';
					}
					var sizeInd = $(this).index();
					var selVal = $(this).text();
					var jsonObj = Myntra.Data.sizeMeasurementsObj[sizeInd];
					switchMeasurements(jsonObj, selVal);


				}

			});
		} else {
			if (!sizeSelBlock.find('li.size-val-sel-inactive').length) {
				selCount = sizeSelLi.length;
				if (selCount > 1) {
					var midVal = Math.ceil(selCount / 2);
					var sizeInd = sizeSelBlock.find('li.size-available:eq(' + (midVal - 1) + ')').index();
					var selVal = sizeSelBlock.find('li.size-available:eq(' + (midVal - 1) + ')').text();
					var jsonObj = Myntra.Data.sizeMeasurementsObj[sizeInd];
					switchMeasurements(jsonObj, selVal);
					sizeSelLi.removeClass('size-val-sel size-val-sel-inactive');
					sizeSelBlock.find('li.size-available:eq(' + (midVal - 1) + ')').addClass('size-val-sel');
				} else if (selCount == 1) {
					var sizeInd = sizeSelLi.index();
					var selVal = sizeSelLi.text();
					var jsonObj = Myntra.Data.sizeMeasurementsObj[sizeInd];
					switchMeasurements(jsonObj, selVal);
					sizeSelLi.addClass('size-val-sel');
				}
			}
		}
	}

	function getSelectedSize() {
		return {
			text: ($('.mk-product-guide .mk-size').length ? $('.flat-size-options .size-btn.selected').text() : ''),
			val: ($('.mk-product-guide .mk-size').length ? $('.flat-size-options .size-btn.selected').val() : '')
		};
	}

	function trackClick(styleId) {
		var currCookieValue = Myntra.Utils.Cookie.get(Myntra.Data.sizechartCookieConstants.NewSizechartCookie);
		if (currCookieValue != null) {
			currCookieValue = decodeURIComponent(currCookieValue);
		}
		var newCookieValue = Myntra.Utils.setCompositeData(styleId, currCookieValue, true, Myntra.Data.sizechartCookieConstants.Delimiter, Myntra.Data.sizechartCookieConstants.MaxStylesPerCookie);
		Myntra.Utils.Cookie.set(Myntra.Data.sizechartCookieConstants.NewSizechartCookie, newCookieValue, Myntra.Data.sizechartCookieConstants.Expiry);
	}

	var ajaxMethods = {
		loadPage: function(onSuccess, onComplete) {
			$.ajax({
				type: "POST",
				url: "/size_scaling.php",
				data: "isd=" + Myntra.PDP.Data.isStyleDisabled + "&styleid=" + Myntra.PDP.Data.id + "&options=" + JSON.stringify(allProductOptionDetails),
				success: onSuccess,
				complete: onComplete
			});
		},
		getSizeList: function() {
			$.ajax({
				type: "GET",
				url: "/brand_sizing.php",
				data: "actionType=fetchMeasurementsForSkus&skuids=" + getSkuids(),
				dataType: "json",
				success: function(data) {
					if (data) {
						AjaxResponse.skuToMeasurementsMap = data;
					}
					onAjaxLoad();
				}
			});

			function getSkuids() {
				var skuids = [];
				for (var obj in allProductOptionDetails) {
					skuids.push(allProductOptionDetails[obj]["sku_id"]);
				}
				return skuids.join(",");
			}
		},
		getBrandlist: function(onSuccess) {
			var params = "actionType=fetchBrands&ageGroup=" + Myntra.PDP.Data.ageGroup;
			if (typeof Myntra.PDP.Data.articleTypeId != 'undefined') {
				params += "&articleTypeId=" + Myntra.PDP.Data.articleTypeId;
			}
			$.ajax({
				type: "GET",
				url: "/brand_sizing.php",
				dataType: "json",
				data: params,
				success: function(data) {
					if (data) {
						AjaxResponse.brandList = data;
					}
					onAjaxLoad();
				}
			});
		},
		getBrandSizelist: function(brand) {
			var params = "actionType=fetchSizes&ageGroup=" + Myntra.PDP.Data.ageGroup;
			if (typeof Myntra.PDP.Data.articleTypeId != 'undefined') {
				params += "&articleTypeId=" + Myntra.PDP.Data.articleTypeId;
			}
			params += "&brand=" + brand;
			$.ajax({
				type: "GET",
				url: "/brand_sizing.php",
				dataType: "json",
				data: params,
				success: function(data) {
					if (data) {
						Myntra.Utils.populateComboViaJson(data, $("select.size"), "Select your size");
					}
				},
				beforeSend: getBrandBeforeSend,
				complete: getBrandComplete
			});
		}
	}

}())