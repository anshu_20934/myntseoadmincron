/* Modal JS 
(myntra.modals.js)
------------------------------------ */
$(window).load(function() {

	// Sign Up Modal ---------------------
	$('span.mk-discount-amount').click(function() {
		var modalWrapHeight = $('html').height();
		$('.mk-modal-wrapper').height(modalWrapHeight);
		$('.mk-modal-wrapper').show();
	});
	
	$('span.mk-modal-close').click(function() {
		$('.mk-modal-wrapper').hide();
	});

	// Quicklook Modal ---------------------
	$('a.mk-quick-link').click( function() {
		var modalWrapHeight = $('html').height();
		$('.mk-quicklook-wrapper').height(modalWrapHeight);
		$('.mk-quicklook-wrapper').show();
	});

	$('span.mk-modal-close').click(function() {
		$('.mk-quicklook-wrapper').hide();
	});

	// Add to Cart Modal --------------------
	$('button.mk-add-to-cart').click(function() {
		$('.mk-quicklook-wrapper').hide();
		var modalWrapHeight = $('html').height();
		$('.mk-cart-wrapper').height(modalWrapHeight);
		$('.mk-cart-wrapper').show();
	});

	$('span.mk-modal-close').click(function(){
		$('.mk-cart-wrapper').hide();
	});

	// Order Details Modal ---------------------
	$('.mk-order-number').click(function () {
		var modalWrapHeight = $('html').height();
		$('.mk-order-wrapper').height(modalWrapHeight);
		$('.mk-order-wrapper').show();
		_gaq.push(['_trackEvent', 'mymyntra', 'myorders','order_details_show']);
	});

	$('span.mk-modal-close').click(function(){
		$('.mk-order-wrapper').hide();
		_gaq.push(['_trackEvent', 'mymyntra', 'myorders','order_details_close']);
	});

	// Edit Address Modal ---------------------
	$('em.mk-edit-acct').click(function() {
		var modalWrapHeight = $('html').height();
		$('.mk-edit-acct-wrapper').height(modalWrapHeight);
		$('.mk-edit-acct-wrapper').show();
	});

	$('span.mk-modal-close').click(function(){
		$('.mk-edit-acct-wrapper').hide();	
	});

	// Free Item Modal ---------------------
	$('a.mk-free-view').click( function() {
		var modalWrapHeight = $('html').height();
		$('.mk-free-wrapper').height(modalWrapHeight);
		$('.mk-free-wrapper').show();
		return false;
	});

	$('span.mk-modal-close').click(function() {
		$('.mk-free-wrapper').hide();
	});	
	
	// Add Free Item Modal ---------------------
	$('button.mk-this-item').click(function() {
		$('.mk-free-wrapper').hide();
		var modalWrapHeight = $('html').height();
		$('.mk-awards-wrapper').height(modalWrapHeight);
		$('.mk-awards-wrapper').show();
	});

	$('span.mk-modal-close').click(function(){
		$('.mk-awards-wrapper').hide();
	});

});
