
$(document).ready(function() {
	Myntra.MyGiftCards.initPreview();
});

Myntra.MyGiftCards = (function(){
	var obj = {};
	var giftCardAjaxHelperUrl = http_loc+"/giftCardsAjaxHelper.php?";
	var successPrefix = "<span class='mk-span-success-ico'></span>";
	var errPrefix = "<span class='mk-span-err-ico'></span>";
	
	obj.initPreview = function(){
		$(".mk-gc-preview-link").each(function(){
			$(this).data('lb', Myntra.LightBox('#mk-giftcardpreview-overlay-'+$(this).attr("data-id")));
		});
		
		$(".preview-link").click(function(){
			$(this).data('lb').show();
		});
		
		$(".mk-gc-activate-now").click(function(){
			var id = $(this).attr("data-id");
			var parent = $(this).parent();
			if(typeof id != "undefined" && id != null){
				$.ajax({
				  url: giftCardAjaxHelperUrl+"type=id&mode=activate&id="+id,
				  success: function(data) {
					  var result = $.parseJSON(data);
					  if(typeof result.status != "undefined" && result.status != "failed"){
						  parent.html(successPrefix + " <span class='msg'>" + result.message + "</span>").addClass("success").slideDown('slow');
						  //This should be a success message notification
					  }
					  else{
						  parent.html(errPrefix + " <span class='msg'>" + result.message + "</span>").addClass("err").slideDown('slow');
						  //inputEl.addClass("mk-err");
					  }
				  }});
			}
			else{
				parent.html(errPrefix + " <span class='msg'>Error Occured. Please try again later.</span>").addClass("err").slideDown('slow');
			}
		});
		
	$(".activate-gift-card").click(function(){
		$(".mk-gc-activation-err").html("").removeClass("err").removeClass("success").slideUp('slow');
		$(".mk-gc-activation-error").html("").removeClass("err").removeClass("success").slideUp('slow');
		$(".mk-gc-activation-success").html("").removeClass("err").removeClass("success").slideUp('slow');

		var inputEl = $(".mk-gc-card");
		inputEl.removeClass("mk-err");
		var card=inputEl.val();
		
		var inputEl = $(".mk-gc-pin");
		inputEl.removeClass("mk-err");
		var pin=inputEl.val();

		var inputEl = $(".mk-gc-captcha");
		inputEl.removeClass("mk-err");
		var captcha=inputEl.val();


		if(card == ""){
			$("#card-err.mk-gc-activation-err").html(errPrefix + " <span class='msg'>Please enter a gift card number.</span>").addClass("err").slideDown('slow');
//			inputEl.addClass("mk-err");
			return; 
		}

		if(captcha == ""){
			$("#captcha-err.mk-gc-activation-err").html(errPrefix + " <span class='msg'>Please verify the code above.</span>").addClass("err").slideDown('slow');
			inputEl.addClass("mk-err");
			return; 
		}

		$.ajax({
		  url: giftCardAjaxHelperUrl+"type=pin&mode=activate&pin="+pin+"&captcha="+captcha+"&card="+card,
		  success: function(data) {
			  var result = $.parseJSON(data);
			  if(typeof result.status != "undefined" && result.status != "failed"){
                $(".mk-gc-activation-success").html(successPrefix + " <span class='msg'>" + result.message + "</span>").addClass("success").slideDown('slow');
                $(".mk-gc-card").val("");
                $(".mk-gc-pin").val("");
                $(".mk-gc-captcha").val("");
                $('#captcha').attr("src", https_loc+'/captcha/captcha.php?id=giftCaptcha&rand='+Math.random());
				  //setTimeout(function() {location.reload()}, 2000); // delays 2 secs and then reloads the pge so that cashback information is updated
				  //This should be a success message notification
			  } else {
                if (typeof result.status != "undefined") {
                    if (result.type == "card") {
                        $("#card-err.mk-gc-activation-err")
                            .html(errPrefix + " <span class='msg'>" + result.message + "</span>")
                            .addClass("err").slideDown('slow');
                        $('#captcha').attr("src", https_loc+'/captcha/captcha.php?id=giftCaptcha&rand='+Math.random());
                    } else if (result.type == "captcha") {
                        $("#captcha-err.mk-gc-activation-err")
                            .html(errPrefix + " <span class='msg'>" + result.message + "</span>")
                            .addClass("err").slideDown('slow');
                    } else {
                        $(".mk-gc-activation-error")
                            .html(errPrefix + " <span class='msg'>" + result.message + "</span>")
                            .addClass("err").slideDown('slow');
                        $('#captcha').attr("src", https_loc+'/captcha/captcha.php?id=giftCaptcha&rand='+Math.random());
                    }
                }
			  }
		}});	
	});

		$(".mk-gc-resend-email").click(function(){
			var id = $(this).attr("data-id");
			var parent = $(this).parent();
			if(typeof id != "undefined" && id != null){
				$.ajax({
				  url: giftCardAjaxHelperUrl+"mode=sendemail&id="+id,
				  success: function(data) {
					  var result = $.parseJSON(data);
					  if(typeof result.status != "undefined" && result.status != "failed"){
						  parent.html(successPrefix + " <span class='msg'>" + result.message + "</span>").addClass("success").slideDown('slow');
					  }
					  else{
						  parent.html(errPrefix + " <span class='msg'>" + result.message + "</span>").addClass("err").slideDown('slow');
					  }
				  }});	
			}
			else{
				parent.html(errPrefix + " <span class='msg'>Error Occured. Please try again later.</span>").addClass("err").slideDown('slow');
			}
		});
	}
	
	return obj;
})();

