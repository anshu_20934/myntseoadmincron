$(window).load(function() {
  
  Myntra.WebEngage = Myntra.WebEngage || {};

  if (!Myntra.WebEngage.feedbackEnabled && !Myntra.WebEngage.notificationsEnabled) return;
  
  (function(d){
    var _we = d.createElement('script');
    _we.type = 'text/javascript';
    _we.async = true;
    _we.src = (d.location.protocol == 'https:' ? "//ssl.widgets.webengage.com" : "//cdn.widgets.webengage.com") + "/js/widget/webengage-min-v-3.0.js";
    var _sNode = d.getElementById('_webengage_script_tag');
    _sNode.parentNode.insertBefore(_we, _sNode);
  })(document);

  // Feedback Widget
  Myntra.WebEngage.initFeedback = function() {
    if (!Myntra.WebEngage.feedbackEnabled) return;
    if (typeof(Myntra.WebEngage.regexpJSON) == 'object' && Myntra.WebEngage.regexpJSON[Myntra.WebEngage.category] 
      && window.location.href.search(Myntra.WebEngage.regexpJSON[Myntra.WebEngage.category]) < 0) return;
    webengage.feedback.render({
      defaultFeedbackCategory: Myntra.WebEngage.category,
      showAllFeedbackCategories: false,
      data: {
        Name: Myntra.WebEngage.userFullName,
        Email: Myntra.Data.userEmail,
        LinkedMobile: Myntra.WebEngage.userMobile
      }
    });
  };
  // END Feedback Widget

  // Notifications
  Myntra.WebEngage.initNotifications = function(data) {
    Myntra.Data.UTM = Myntra.Data.UTM || {};
    Myntra.PDP = Myntra.PDP || {};
    Myntra.PDP.Data = Myntra.PDP.Data || {};
    Myntra.Search = Myntra.Search || {};
    Myntra.Search.Data = Myntra.Search.Data || {};
    Myntra.Cart = Myntra.Cart || {};
    Myntra.Cart.Data = data || Myntra.Cart.Data || {};
    //Myntra.Cart.Data = $.extend({}, Myntra.Cart.Data, data);
    data = data || {};
    var query = query || '';
    var styleIdList = styleIdList || '';
    // x = x ? +x : x used below is to ensure consistent numeric type for set values while avoiding NaN
    var ruleData = {
      // User Data
      fullName      : Myntra.WebEngage.userFullName,
      firstName     : Myntra.WebEngage.userFirstName,
      lastName      : Myntra.WebEngage.userLastName,
      email         : Myntra.Data.userEmail,
      mobile        : Myntra.WebEngage.userMobile,
      gender        : Myntra.WebEngage.userGender,
      dateOfBirth   : Myntra.WebEngage.userDOB,
      fbid          : Myntra.Data.userFBId,
      customerid    : Myntra.Data.userHashId,
      cashback      : Myntra.Cart.Data.userCB ? +Myntra.Cart.Data.userCB : Myntra.Cart.Data.userCB,
      couponCount   : data.couponCount ? +data.couponCount : data.couponCount,
      couponExpiry  : data.daysToCouponExpiry ? +data.daysToCouponExpiry : data.daysToCouponExpiry,
      orderCount    : data.orderCount ? +data.orderCount : data.orderCount,
      isLoggedIn    : Myntra.WebEngage.isLoggedIn,
      isFirstTime   : Myntra.WebEngage.isFirstTime,
      isRegistered  : Myntra.WebEngage.isRegistered,
      isReturning   : Myntra.WebEngage.repeatCustomer,
      isRNB   : (Myntra.WebEngage.repeatCustomer == 1)?false:true,
      savedQuantity : Myntra.Cart.Data.savedQuantity,
      // Traffic Data
      host        : http_loc,
      secureHost  : https_loc,
      pageType    : Myntra.WebEngage.category,
      source      : Myntra.Data.UTM.Source,
      medium      : Myntra.Data.UTM.Medium,
      campaign    : Myntra.Data.UTM.Campaign,
      campaignId  : Myntra.Data.UTM.CampaignId,
      octaneUserHash  : Myntra.Data.UTM.OctaneEmail,
      referrer    : document.referrer,
      deviceName  : Myntra.Data.Device.Name,
      deviceType  : Myntra.Data.Device.Type,
      // PDP Data
      productStyleId            : Myntra.PDP.Data.id,
      productDisplayName        : Myntra.PDP.Data.name,
      productCleanURL           : Myntra.PDP.Data.cleanURL,
      productCategory           : Myntra.PDP.Data.category ? Myntra.PDP.Data.category.toLowerCase().replace(/\s/g, '-') : Myntra.PDP.Data.category,
      productSubCategory        : Myntra.PDP.Data.subCategory ? Myntra.PDP.Data.subCategory.toLowerCase().replace(/\s/g, '-') : Myntra.PDP.Data.subCategory,
      productArticleType        : Myntra.PDP.Data.articleType ? Myntra.PDP.Data.articleType.toLowerCase().replace(/\s/g, '-') : Myntra.PDP.Data.articleType,
      productGender             : Myntra.PDP.Data.gender ? Myntra.PDP.Data.gender.toLowerCase().replace(/\s/g, '-') : Myntra.PDP.Data.gender,
      productBrandName          : Myntra.PDP.Data.brand ? Myntra.PDP.Data.brand.toLowerCase().replace(/\s/g, '-') : Myntra.PDP.Data.brand,
      productImage              : Myntra.PDP.Data.image,
      productThumbnail          : Myntra.PDP.Data.searchImage,
      productPrice              : Myntra.PDP.Data.price ? +Myntra.PDP.Data.price : Myntra.PDP.Data.price,
      productDiscount           : Myntra.PDP.Data.discount ? +Myntra.PDP.Data.discount : Myntra.PDP.Data.discount,
      productPriceAfterDiscount : Myntra.PDP.Data.price ? Myntra.PDP.Data.discount ? Myntra.PDP.Data.price - Myntra.PDP.Data.discount : +Myntra.PDP.Data.price : Myntra.PDP.Data.price,
      productInStock            : Myntra.PDP.Data.inStock == 1 ? true : false,
      // Search Data
      navId                 : Myntra.WebEngage.navId, // For Search and PDP
      searchQuery           : Myntra.Search.Data.query,
      searchArticleTypeList : Myntra.Search.Data.searchArticleTypeList,
      searchBrandList       : Myntra.Search.Data.searchBrandList,
      searchCategoryList    : Myntra.Search.Data.searchCategoryList,
      searchSubCategoryList : Myntra.Search.Data.searchSubCategoryList,
      searchGenderList      : Myntra.Search.Data.searchGenderList,
      searchColorList       : Myntra.Search.Data.searchColorList,
      searchTopArticleType  : Myntra.Search.Data.searchTopArticleType,
      searchTopBrand        : Myntra.Search.Data.searchTopBrand,
      searchTopCategory     : Myntra.Search.Data.searchTopCategory,
      searchTopSubCategory  : Myntra.Search.Data.searchTopSubCategory,
      searchTopGender       : Myntra.Search.Data.searchTopGender,
      searchTopColor        : Myntra.Search.Data.searchTopColor,
      searchPriceRangeMin   : Myntra.Search.Data.priceRangeMin ? +Myntra.Search.Data.priceRangeMin : Myntra.Search.Data.priceRangeMin,
      searchPriceRangeMax   : Myntra.Search.Data.priceRangeMax ? +Myntra.Search.Data.priceRangeMax : Myntra.Search.Data.priceRangeMax,
      // Cart Data
      productAdded      : Myntra.Cart.Data.productAdded,
      itemIds           : Myntra.Cart.Data.items ? _.pluck($.parseJSON(Myntra.Cart.Data.items), 'id').join(',') : data.itemIds,
      itemCount         : Myntra.Cart.Data.items ? $.parseJSON(Myntra.Cart.Data.items).length : +data.itemCount,
      totalQuantity     : Myntra.Data.cartTotalQuantity ? +Myntra.Data.cartTotalQuantity : Myntra.Data.cartTotalQuantity,
      totalMRP          : Myntra.Cart.Data.mrp ? +Myntra.Cart.Data.mrp : Myntra.Cart.Data.mrp,
      isGiftOrder       : Myntra.Cart.Data.isGiftOrder,
      giftCharge        : Myntra.Cart.Data.giftCharge ? +Myntra.Cart.Data.giftCharge : Myntra.Cart.Data.giftCharge,
      shippingCharge    : Myntra.Cart.Data.shippingCharge ? +Myntra.Cart.Data.shippingCharge : Myntra.Cart.Data.shippingCharge,
      fullAmount        : Myntra.Cart.Data.amount ? +Myntra.Cart.Data.amount : Myntra.Cart.Data.amount,
      cartLevelDiscount : Myntra.Cart.Data.cartLevelDiscount ? +Myntra.Cart.Data.cartLevelDiscount : Myntra.Cart.Data.cartLevelDiscount,
      itemDiscount      : Myntra.Cart.Data.itemDiscount ? +Myntra.Cart.Data.itemDiscount : Myntra.Cart.Data.itemDiscount,
      couponDiscount    : Myntra.Cart.Data.couponDiscount ? +Myntra.Cart.Data.couponDiscount : Myntra.Cart.Data.couponDiscount,
      cashbackApplied   : Myntra.Cart.Data.cashDiscount ? +Myntra.Cart.Data.cashDiscount : Myntra.Cart.Data.cashDiscount,
      totalSavings      : Myntra.Cart.Data.savings ? +Myntra.Cart.Data.savings : Myntra.Cart.Data.savings,
      totalAmount       : Myntra.Data.cartTotalAmount && Myntra.Data.cartTotalQuantity != "0" ? +Myntra.Data.cartTotalAmount : Myntra.Data.cartTotalQuantity
    };

    webengage.notification.render({
      tokens  : ruleData,
      data    : ruleData,
      ruleData: ruleData
    });
  }
  // END Notifications

  window.webengageWidgetInit = window.webengageWidgetInit || function(){
    webengage.init({
      licenseCode:"~47b6627b"
    }).onReady(function() {
      Myntra.WebEngage.initFeedback();
    });

    if (Myntra.WebEngage.notificationsEnabled) {
      // Do not fire ajax from secure pages. Do not fire ajax if user is logged out and we already have cart data.
      //if (location.protocol == 'https:' || (!Myntra.Data.userEmail && Myntra.Cart && Myntra.Cart.Data)) {
        webengage.onReady(function() {
          Myntra.WebEngage.initNotifications();
        });
      /*} else {
        var url = http_base_url + '/webengage_ajax.php';
        if (Myntra.Data.userEmail && Myntra.Cart && Myntra.Cart.Data) url += '?checkout=1'; // Only get user data, as cart data is available.
        $.ajax({
          url     : url,
          success : function(data) {
                      webengage.onReady(function() {
                        if (data.status != 'SUCCESS') Myntra.WebEngage.initNotifications();
                        else Myntra.WebEngage.initNotifications(data.content);
                      });
                    },
          error   : function() {
                      webengage.onReady(function() {
                        Myntra.WebEngage.initNotifications();
                      });
                    },
          dataType: 'json'
        });
      }*/
    }
  };
});
