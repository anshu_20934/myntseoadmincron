$(function () {
	if (Myntra && Myntra.RecentWidget && Myntra.RecentWidget.init) return;

	(function (context) {
		
		var getItems = function () {
			return $.ajax({
				type:"GET",
				url:http_loc+'/getRecentItems.php',
				dataType:"json"
			});
		};

		var loadWidget = function (content) {
			var widget = $('.mk-recent-viewed');
			widget.html(content).fadeIn(200, function () {
				if ($('.mk-recent-viewed .rs-carousel-runner > li').size() > 5) {
					$('.mk-recent-viewed').carousel({
						itemsPerPage: 5, // number of items to show on each page
						itemsPerTransition: 1, // number of items moved with each transition
						nextPrevActions: true, // whether next and prev links will be included
						pagination: false, // whether pagination links will be included
						onHover: true // whether hover will act like click for next/prev
					});
				}
				if (Myntra.Data.pageName == 'home' && Myntra.Data.mostPopular == 'expanded') {
					widget.find('.icon-collapse-large').click();
				}
			});
			
			/* Expand / Collapse Logic */
			widget.find('h4').click(function (e) {
				var widget = $(this).closest('.mk-recent-viewed'),
					container = widget.children('.rs-carousel-mask').length ? widget.find('.rs-carousel-mask') : widget.find('.rs-carousel-runner, .vertical-line'),
					recTitle = widget.find('.rec-title');
				if (widget.hasClass('collapsed')) {
					widget.removeClass('collapsed');
					container.slideDown();
					recTitle.css('visibility', 'visible');
					widget.find('.icon-expand-large')[0].className = 'icon-collapse-large';
				} else if ($(e.target).hasClass('icon-collapse-large')) {
					widget.addClass('collapsed');
					container.slideUp();
					recTitle.css('visibility', 'hidden');
					widget.find('.icon-collapse-large')[0].className = 'icon-expand-large';
				}
			});
		};

		var getSizes = function () {
			styleids = [];
			
			$('.mk-recent-viewed .inline-add-to-cart-form').find("input[name='productStyleId']").each(function (i, el) {
				styleids.push(el.value);
			});
			
			return $.ajax({
				type: 'GET',
				url: '/myntra/ajax_getsizes.php?ids=' + styleids.join(','),
				dataType: 'json'
			});
		};

		var loadSizes = function (form) {
		    if (form.data('sizes-loaded')) return;
		    var sizeAddBlock = form.parent(),
				sizeCarousel = sizeAddBlock.find('.mk-select-size .rs-carousel-runner'),
		    	styleid = form.find("input[name='productStyleId']").val(),
		        sizes = Myntra.RecentWidget.Data.sizes[styleid],
		        markup = [];
		    if (!sizes) { return; }

		    var count = 0;
		    for (key in sizes) {
		    	count++;
		    	if (count > 1) break;
		    }

		    var key, val, dis;
		    for (key in sizes) {
		        dis = '';
		        val = sizes[key][0];

		        if(count==1) {
		        	form.find("input[name='productSKUID']").val(key);
		        }

		        if (!sizes[key][1]) {
		            continue; // don't show the unavailable sizes
		            dis = ' unavailable';
		        }
		        markup.push(['<button class="btn size-btn rs-carousel-item ', dis, '" value="', key, '">', val, '</button>'].join(''));
		    };

		    if(markup.length == 0) {
		    	 sizeCarousel.closest('.tooltip-content').html('Sold Out').closest('.mk-select-size').removeClass('rs-carousel');
		    } else {
		    	 sizeCarousel.append(markup.join("\n"));
		    }

			// Attach triggers for add to cart
			var _this = form.closest('.inline-add-block'),
				sizeTooltip = _this.find('.mk-select-size'),
				sizeBtns = sizeTooltip.find('.size-btn'),
				buyType = sizeBtns.length ? 'select-size' : sizeTooltip.find('span.selected').length ? 'single-size' : 'sold-out';
			
			if (buyType == 'single-size') {
				_this.find('.icon-buynow').click(function () {
					buy(_this);
				});
			} else if (buyType == 'select-size') {
				sizeBtns.click(function () {
					$(this).addClass('selected');
					buy(_this);
				});
			}
		    form.data('sizes-loaded', true);
		};

		var initSizeTooltip = function () {
			// Show and hide of Buy Now button and Size Select Tooltip
			$('.mynt-layout-new .mk-recent-viewed').on('mouseenter', '.recent-inline', function () {
				$(this).find('.icon-buynow').show();
			}).on('mouseleave', '.recent-inline', function () {
				$(this).find('.icon-buynow').hide();
			}).on('mouseenter', '.icon-buynow, .mk-select-size', function (e) {
				var context  = $(this).closest('.recent-inline'),
					tooltip = context.find('.mk-select-size'),
					fromElem = e.relatedTarget || e.fromElement;
				if (!$(fromElem).closest('.icon-buynow').length && !$(fromElem).closest('.mk-select-size').length) {
					context.find('.quick-look').hide();
					tooltip.fadeIn('fast');
					if (tooltip.hasClass('rs-carousel')) {
						context.find('.icon-buynow').addClass('mk-hover');
						Myntra.Utils.initCarousel(tooltip.parent(), 'calc', 140);
					}
				}
			}).on('mouseleave', '.icon-buynow, .mk-select-size', function (e) {
				var context  = $(this).closest('.recent-inline'),
					tooltip = context.find('.mk-select-size'),
					toElem = e.relatedTarget || e.toElement;
				if (!$(toElem).closest('.icon-buynow').length && !$(toElem).closest('.mk-select-size').length) {
					context.find('.quick-look').show();
					tooltip.fadeOut('fast');
					if (tooltip.hasClass('rs-carousel')) {
						context.find('.icon-buynow').removeClass('mk-hover'); //check for sold out
					}
				}
			});
		};


		var buy = function (addBlock) {
		    //fire add to cart
			var formToSubmit = addBlock.find('.inline-add-to-cart-form'),
				selectedSize = addBlock.find('.selected'),
				quantity = formToSubmit.find("input[name='quantity']").val(),
				sku_ = selectedSize.val(),
				size = selectedSize.text();
				formToSubmit.find("input[name='sizequantity[]']").val(quantity);
				if (sku_) formToSubmit.find("input[name='productSKUID']").val(sku_);
				formToSubmit.find("input[name='selectedsize']").val(size);
                _gaq.push(['_trackEvent','Cart_conversion', Myntra.PDP.gaConversionData.action, Myntra.PDP.gaConversionData.label]);
				_gaq.push(['_trackEvent','buy_now', Myntra.Data.pageName, 'recent_widget']);
				formToSubmit.submit();
		};
		
		var init = function () {
			getItems().success(function (data) {
				if (data.status === 'SUCCESS') { 
					
					loadWidget(data.content);
			
					getSizes().success(function (response) {
						context.Data = Myntra.RecentWidget.Data || {};
						context.Data.sizes = response;
						
						$('.mk-recent-viewed .inline-add-to-cart-form').each(function (i, el) {
							if (Myntra.Data.newLayout) {
								loadSizes($(el));
							} else {
								Myntra.Utils.loadRecentSizes($(el));
							}
						});
						if (Myntra.Data.newLayout) {
							initSizeTooltip();
						}												
					});
				}
			});
		};
		
		context.init = init;
		context.loadSizes = loadSizes;
		context.initSizeTooltip = initSizeTooltip;

	})(Myntra.RecentWidget);
});
