filterProperties["brands"]={
	getURLParam: function(){
		var brandsStr=configArray["brands"].join(":");
		return (brandsStr != "")?"brands="+brandsStr:"";
	},
	processURL: function(str){
		configArray["brands"]=Myntra.Search.getFilterValues(str);
		if (Myntra.Search.Data.optimEnabled) {
			Myntra.Search.Data.initJSON.search.fq.push(('brands_filter_facet:("' + configArray.brands.join('" OR "') + '")').replace('~-', '-').replace('~/', '\/'));
		}
		return true;
	},
	updateUI: function(){
		// unselect all brands
		$(".mk-brands-filters .mk-labelx_check").removeClass("checked").addClass("unchecked");

		// update UI selection for brands in configArray
		for(var brand=0;brand<configArray["brands"].length; brand++){
			var selector=configArray["brands"][brand].toLowerCase().replace(/\W+/g, "") + "-brand-filter";
			$("."+selector).removeClass("unchecked").addClass("checked");
		}
		return true;
	},
	setToDefault: function(){
		configArray["brands"]=[];
		return true;
	},
	resetParams: function(){
		return true;
	},
    updateFilterVisibility: function(filters){
        if(!userTriggered || (filterClickedName != "" && filterClickedName!="brands") || (filterClickedName != "" && filterClickedName=="brands" && configArray["brands"].length == 0)){
            $(".mk-brands-filters .mk-labelx_check").each(function (index,element){
                if(typeof filters.brands_filter_facet == "undefined" || typeof filters.brands_filter_facet[$(element).attr('data-value')] == 'undefined'){
                    $(element).find(".mk-filter-product-count").hide();
                    $(element).addClass('disabled');
                }else{
                    $(element).find(".mk-filter-product-count").show().html("["+filters.brands_filter_facet[$(element).attr('data-value')]+"]")
                    $(element).removeClass("disabled");
                }
            });
        }
    },
    setFilterClickedName: function(){
        filterClickedName = "brands";
	},
	updateClearFilterUI: function(){
		var clearFilterEl=$(".mk-brands-filters").closest("li").find(".mk-clear-filter");
		var clearAllFilterEl=$(".mk-clear-all-filters");
		if(configArray["brands"].length >= 1){
			clearFilterEl.removeClass("mk-hide");
		}
		else{
			clearFilterEl.addClass("mk-hide");
		}
		if(Myntra.Search.Utils.getNoOfFiltersSet()){
			clearAllFilterEl.removeClass("mk-hide");
		}
		else{
			clearAllFilterEl.addClass("mk-hide");
		}
	}
};
filterProperties["articletypes"]={
    getURLParam: function(){
        var atStr=configArray["articletypes"].join(":");
        return (atStr != "")?"articletypes="+atStr:"";
    },
    processURL: function(str){
        configArray["articletypes"]=Myntra.Search.getFilterValues(str);
		if (Myntra.Search.Data.optimEnabled) {
			Myntra.Search.Data.initJSON.search.fq.push(('global_attr_article_type_facet:("' + configArray.articletypes.join('" OR "') + '")').replace('~-', '-').replace('~/', '\/'));
			if (configArray.articletypes.length == 1) {
				Myntra.Search.Data.initJSON.search.facetField = Myntra.Search.Data.initJSON.all_article_attribute[configArray.articletypes[0]];
			}
		}
        return true;
    },
    updateUI: function(){
        // unselect all article types
        $(".mk-articletypes-filters .mk-labelx_check").removeClass("checked").addClass("unchecked");
        // update UI selection for article types in configArray
        for(var articletype=0;articletype<configArray["articletypes"].length; articletype++){
            var selector=configArray["articletypes"][articletype].toLowerCase().replace(/\W+/g, "") + "-at-filter";
            $("."+selector).removeClass("unchecked").addClass("checked");
        }
        return true;
    },
    setToDefault: function(){
        configArray["articletypes"]=[];
        return true;
    },
    resetParams: function(){
        return true;
    },
    updateFilterVisibility: function(filters){
        if(!userTriggered || (filterClickedName != "" && filterClickedName!="articletypes") || (filterClickedName != "" && filterClickedName=="articletypes" && configArray["articletypes"].length == 0)){
            $(".mk-articletypes-filters .mk-labelx_check").each(function (index,element){
                if(typeof filters.global_attr_article_type_facet == "undefined" || typeof filters.global_attr_article_type_facet[$(element).attr('data-value')] == 'undefined'){
                    $(element).find(".mk-filter-product-count").hide();
                    $(element).addClass('disabled');
                }else{
                    $(element).find(".mk-filter-product-count").show().html("["+filters.global_attr_article_type_facet[$(element).attr('data-value')]+"]");
                    $(element).removeClass("disabled");
                }
            });
        }
    },
    setFilterClickedName: function(){
        filterClickedName = "articletypes";
    },
    updateClearFilterUI: function(){
        var clearFilterEl=$(".mk-articletypes-filters").closest("li").find(".mk-clear-filter");
        var clearAllFilterEl=$(".mk-clear-all-filters");
        if(configArray["articletypes"].length >= 1){
            clearFilterEl.removeClass("mk-hide");
        }
        else{
            clearFilterEl.addClass("mk-hide");
        }
        if(Myntra.Search.Utils.getNoOfFiltersSet()){
			clearAllFilterEl.removeClass("mk-hide");
		}
		else{
			clearAllFilterEl.addClass("mk-hide");
		}
    }
};
filterProperties["sizes"]={
	getURLParam: function(){
		var sizesStr=configArray["sizes"].join(":");
		return (sizesStr != "")?"sizes="+sizesStr:"";
	},
	processURL: function(str){
		configArray["sizes"]=Myntra.Search.getFilterValues(str);
		if (Myntra.Search.Data.optimEnabled) {
			Myntra.Search.Data.initJSON.search.fq.push(('sizes_facet:("' + configArray.sizes.join('" OR "') + '")').replace('~-', '-').replace('~/', '\/'));
		}
		return true;
	},
	updateUI: function(){
		// unselect all sizes
		$(".mk-sizes-filters .mk-labelx_check").removeClass("checked").addClass("unchecked");
		
		// update UI selection for sizes in configArray
		for(var size=0;size<configArray["sizes"].length;size++){
			var selector=configArray["sizes"][size].toLowerCase().replace(/\W+/g, "") + "-size-filter";
			$("."+selector).removeClass("unchecked").addClass("checked");
		}
		return true;
	},
	setToDefault: function(){
		configArray["sizes"]=[];
		return true;
	},
	resetParams: function(){
		return true;
	},
    updateFilterVisibility: function(filters){
        if(!userTriggered || (filterClickedName != "" && filterClickedName!="sizes") || (filterClickedName != "" && filterClickedName=="sizes" && configArray["sizes"].length == 0)){
            $(".mk-sizes-filters .unchecked").remove();
            var showSizeFilter=false;
            var sizeScrollPane;
            sizeScrollPane=$(".mk-sizes-filters").data("jsp");
            // Check if only one size available
            var size_count=0;
            for(var i in filters.sizes_facet){
                size_count++;
            }
            if(size_count > 1){
            	if($.inArray("sizes",dynamicFilters) == -1){
            		dynamicFilters.push("sizes");
            	}
                for(var i in filters.sizes_facet){
                    showSizeFilter=true;
                    var checked = "unchecked";
                    if($(".mk-sizes-filters ."+i.toLowerCase().replace(/\W+/g, "")+'-size-filter').length > 0){
                        checked = "checked";
                        $(".mk-sizes-filters ."+i.toLowerCase().replace(/\W+/g, "")+'-size-filter').remove();
                    }
                    var labelHtml = '<label class="mk-labelx_check '+checked+' '+i.toLowerCase().replace(/\W+/g, "")+'-size-filter" data-key="sizes" data-value="'+i+'"><span class="cbx"></span>'+i.toUpperCase().replace(/ /g,"")+'</label>';
                    if(typeof sizeScrollPane != "undefined"){
                        $(".mk-sizes-filters .jspPane").append(labelHtml);
                    }
                    else{
                        $(".mk-sizes-filters").append(labelHtml);
                    }
                }
            }
            if(showSizeFilter){
            	 $(".mk-sizes-filters").parent().parent().show();
                 $(".mk-sizes-filters").height("150");
                 $(".mk-sizes-filters").css({"max-height":"150"});
                 //reinitialise the scroll pane for sizes filter container
                 if(!($(".mk-sizes-filters .jspPane").length)){
                     if($(".mk-sizes-filters").height() >= 150){
                         $(".mk-sizes-filters").jScrollPane();
                     }
                 }
                 else if(typeof sizeScrollPane != "undefined"){
                         sizeScrollPane.reinitialise();
                         if( $(".mk-sizes-filters .jspPane").height() < 150){
                              $(".mk-sizes-filters").height($ (".mk-sizes-filters .jspPane").height()+5);
                         }
                 }
                 $(".mk-sizes-filters").height(Math.min($(".mk-sizes-filters").height(),$(".mk-sizes-filters .jspPane").height()+5));
            }else{
            	if($.inArray("sizes",dynamicFilters) != -1){
            		Myntra.Search.Utils.removeValueFromArray("sizes",dynamicFilters);
           	 	}
                $(".mk-sizes-filters").parent().parent().hide();
            }
            filterProperties["sizes"].updateUI();
        }
    },
    setFilterClickedName: function(){
        filterClickedName = "sizes";
    },
	updateClearFilterUI: function(){
		var clearFilterEl=$(".mk-sizes-filters").closest("li").find(".mk-clear-filter");
		var clearAllFilterEl=$(".mk-clear-all-filters");
		if(configArray["sizes"].length >= 1){
			clearFilterEl.removeClass("mk-hide");
		}
		else{
			clearFilterEl.addClass("mk-hide");		
		}
		if(Myntra.Search.Utils.getNoOfFiltersSet()){
			clearAllFilterEl.removeClass("mk-hide");
		}
		else{
			clearAllFilterEl.addClass("mk-hide");
		}
	}
};
filterProperties["gender"]={
	getURLParam: function(){
		var genderStr=configArray["gender"].join(":");
		return (genderStr != "")?"gender="+genderStr:"";
	},
	processURL: function(str){
		configArray["gender"]=Myntra.Search.getFilterValues(str);
		if (Myntra.Search.Data.optimEnabled) {
			Myntra.Search.Data.initJSON.search.fq.push(('global_attr_gender:("' + configArray.gender.join('" OR "') + '")').replace('~-', '-').replace('~/', '\/'));
		}
		return true;
	},
	updateUI: function(){
		// unselect all gender
		$(".mk-gender-filters .mk-labelx_check").removeClass("checked").addClass("unchecked");
								
		// update UI selection for gender in configArray
		for(var gender=0;gender<configArray["gender"].length;gender++){
			var selector=configArray["gender"][gender].toLowerCase().replace(/\W+/g, "") + "-gender-filter";
			$("."+selector).removeClass("unchecked").addClass("checked");
		}
		return true;
	},
	setToDefault: function(){
		configArray["gender"]=[];
		return true;
	},
	resetParams: function(){
		return true;
	},
    updateFilterVisibility: function(filters){
        if(!userTriggered || (filterClickedName != "" && filterClickedName!="gender") || (filterClickedName != "" && filterClickedName=="gender" && configArray["gender"].length == 0)){
            $(".mk-gender-filters .mk-labelx_check").each(function (index,element){
                if(typeof filters.global_attr_gender != "undefined" && typeof filters.global_attr_gender[$(element).attr('data-value')] == 'undefined'){
                    $(element).addClass('disabled');
                }else{
                    $(element).removeClass("disabled");
                }
            });
        }
    },
    setFilterClickedName: function(){
        filterClickedName = "gender";
    },
	updateClearFilterUI: function(){
		var clearFilterEl=$(".mk-gender-filters").closest("li").find(".mk-clear-filter");
		var clearAllFilterEl=$(".mk-clear-all-filters");
		if(configArray["gender"].length >= 1){
			clearFilterEl.removeClass("mk-hide");
		}
		else{
			clearFilterEl.addClass("mk-hide");
		}
		if(Myntra.Search.Utils.getNoOfFiltersSet()){
			clearAllFilterEl.removeClass("mk-hide");
		}
		else{
			clearAllFilterEl.addClass("mk-hide");
		}
	}
};

filterProperties["offer"]={
	getURLParam: function(){
		var offerStr=configArray["offer"].join(":");
		return (offerStr != "")?"offer="+offerStr:"";
	},
	processURL: function(str){
		configArray["offer"]=Myntra.Search.getFilterValues(str);
		if (Myntra.Search.Data.optimEnabled) {
			Myntra.Search.Data.initJSON.search.fq.push(('dre_offerType:("' + configArray.offer.join('" OR "') + '")').replace('~-', '-').replace('~/', '\/'));
		}
		return true;
	},
	updateUI: function(){
		// unselect all offers
		$(".mk-offer-filters .mk-labelx_check").removeClass("checked").addClass("unchecked");
								
		// update UI selection for offer in configArray
		for(var offer=0;offer<configArray["offer"].length;offer++){
			var selector=configArray["offer"][offer].toLowerCase().replace(/\W+/g, "") + "-offer-filter";
			$("."+selector).removeClass("unchecked").addClass("checked");
		}
		return true;
	},
	setToDefault: function(){
		configArray["offer"]=[];
		return true;
	},
	resetParams: function(){
		return true;
	},
    updateFilterVisibility: function(filters){
        if(!userTriggered || (filterClickedName != "" && filterClickedName!="offer") || (filterClickedName != "" && filterClickedName=="offer" && configArray["offer"].length == 0)){
            $(".mk-offer-filters .mk-labelx_check").each(function (index,element){
                if(typeof filters.dre_offerType != "undefined" && typeof filters.dre_offerType[$(element).attr('data-value')] == 'undefined'){
                    $(element).addClass('disabled');
                }else{
                    $(element).removeClass("disabled");
                }
            });
        }
    },
    setFilterClickedName: function(){
        filterClickedName = "offer";
    },
	updateClearFilterUI: function(){
		var clearFilterEl=$(".mk-offer-filters").closest("li").find(".mk-clear-filter");
		var clearAllFilterEl=$(".mk-clear-all-filters");
		if(configArray["offer"].length >= 1){
			clearFilterEl.removeClass("mk-hide");
		}
		else{
			clearFilterEl.addClass("mk-hide");
		}
		if(Myntra.Search.Utils.getNoOfFiltersSet()){
			clearAllFilterEl.removeClass("mk-hide");
		}
		else{
			clearAllFilterEl.addClass("mk-hide");
		}
	}
};

filterProperties["colour"]={
	getURLParam: function(){
		var colourStr=configArray["colour"].join(":");
		return (colourStr != "")?"colour="+colourStr:"";
	},
	processURL: function(str){
		configArray["colour"]=Myntra.Search.getFilterValues(str);
		if (Myntra.Search.Data.optimEnabled) {
			Myntra.Search.Data.initJSON.search.fq.push(('colour_family_list:("' + configArray.colour.join('" OR "') + '")').replace('~-', '-').replace('~/', '\/'));
		}
		return true;
	},
	updateUI: function(){
		// unselect all colours
		$(".mk-colour-filters .mk-labelx_check").removeClass("checked").addClass("unchecked");
								
		// update UI selection for colours in configArray
		for(var colour=0;colour<configArray["colour"].length;colour++){
			var selector=configArray["colour"][colour].toLowerCase().replace(/\W+/g, "") + "-colour-filter";
			$("."+selector).removeClass("unchecked").addClass("checked");
		}
		return true;
	},
	setToDefault: function(){
		configArray["colour"]=[];
		return true;
	},
	resetParams: function(){
		return true;
	},
    updateFilterVisibility: function(filters){
        if(!userTriggered || (filterClickedName != "" && filterClickedName!="colour") || (filterClickedName != "" && filterClickedName=="colour" && configArray["colour"].length == 0)){
            $(".mk-colour-filters .mk-labelx_check").each(function (index,element){
                if(typeof filters.colour_family_list != "undefined" && typeof filters.colour_family_list[$(element).attr('data-value')] == 'undefined'){
                    $(element).addClass('disabled');
                }else{
                    $(element).removeClass("disabled");
                }
            });
        }
    },
    setFilterClickedName: function(){
        filterClickedName = "colour";
    },
	updateClearFilterUI: function(){
		var clearFilterEl=$(".mk-colour-filters").closest("li").find(".mk-clear-filter");
		var clearAllFilterEl=$(".mk-clear-all-filters");
		if(configArray["colour"].length >= 1){
			clearFilterEl.removeClass("mk-hide");
		}
		else{
			clearFilterEl.addClass("mk-hide");
		}
		if(Myntra.Search.Utils.getNoOfFiltersSet()){
			clearAllFilterEl.removeClass("mk-hide");
		}
		else{
			clearAllFilterEl.addClass("mk-hide");
		}
	}
};

/*
filterProperties["noofitems"]={
	getURLParam: function(){
		var noofitems=parseInt(configArray["noofitems"]);
		return (!isNaN(noofitems))?"noofitems="+noofitems:24;
	},
	processURL: function(str){
		configArray["noofitems"]=str;
		return true;
	},
	updateUI: function(){
		// set noofitems dropdown
		$(".noofitems-select").val(configArray["noofitems"]);
		return true;
	},
	setToDefault: function(){
		configArray["noofitems"]=default_noofitems;
		return true;
	},
	resetParams: function(){
		configArray["page"]=0;
		return true;
	},
    updateFilterVisibility: function(filters){
    },
    setFilterClickedName: function(){
	},
	updateClearFilterUI: function(){
	
	}
};

filterProperties["page"]={
	getURLParam: function(){
		var pagenum=parseInt(configArray["page"])+1;
		return (!isNaN(pagenum))?"page="+pagenum:0;
	},
	processURL: function(str){
		configArray["page"]=parseInt(str)-1;
		if(configArray["page"] == 0){
		    Myntra.Search.InfiniteScroll.pageSetToFirst=true;
		}
		Myntra.Search.InfiniteScroll.pageSetFromHash=true;
		return true;
	},
	updateUI: function(){
		return true;
	},
	setToDefault: function(){
		configArray["page"]=pageDefault;
		Myntra.Search.InfiniteScroll.pageSetToFirst=true;
		Myntra.Search.InfiniteScroll.pageSetFromHash=true;
		// onPageLoad=true;
		return true;
	},
	resetParams: function(){
		return true;
	},
    updateFilterVisibility: function(filters){
    },
    setFilterClickedName: function(){
	},
	updateClearFilterUI: function(){
	
	}
};
*/

filterProperties["sortby"]={
	getURLParam: function(){
		return "sortby="+configArray["sortby"];
	},
	processURL: function(str){
		configArray["sortby"]=str;
		if (Myntra.Search.Data.optimEnabled) {
			Myntra.Search.Data.initJSON.search.sort = Myntra.Search.Data.initJSON.solr_sort_field[str];
			if (Myntra.Search.Data.initJSON.search.curated_query) delete Myntra.Search.Data.initJSON.search.curated_query;
		}
		return true;
	},
	updateUI: function(){
		// set sortyby param
		$(".mk-product-filters a").removeClass("mk-sort-selected");
		var sortParam=configArray["sortby"].toLowerCase();
		if(configArray["sortby"] != ""){
			if(configArray["sortby"] == "PRICEA" || configArray["sortby"] == "PRICED"){
				$(".mk-product-filters .price-sort-filter").addClass("mk-sort-selected");
				if(configArray["sortby"] == "PRICEA"){
					$(".mk-product-filters .mk-price-desc").hide();
					$(".mk-product-filters .mk-price-asc").show();
				}
				else{
					$(".mk-product-filters .mk-price-asc").hide();
					$(".mk-product-filters .mk-price-desc").show();
				}
			}
			else{
				$(".mk-product-filters ."+sortParam+"-sort-filter").addClass("mk-sort-selected");
				$(".mk-product-filters .mk-price-desc").hide();
				$(".mk-product-filters .mk-price-asc").hide();
			}
		}
		return true;
	},
	setToDefault: function(){
		configArray["sortby"]=sortByDefault;
		return true;
	},
	resetParams: function(){
		configArray["page"]=0;
		return true;
	},
    updateFilterVisibility: function(filters){
    },
    setFilterClickedName: function(){
	},
	updateClearFilterUI: function(){
	
	}
};
filterProperties["discountpercentage"]={
	getURLParam: function(){
		var discountStr=configArray["discountpercentage"];
		return (discountStr != "")?"discountpercentage="+discountStr:"";
	},
	processURL: function(str){
		configArray["discountpercentage"]=str;
		if (Myntra.Search.Data.optimEnabled) {
			Myntra.Search.Data.initJSON.search.fq.push(('discount_percentage:[' + str + ' TO *]').replace('~-', '-').replace('~/', '\/'));
		}
		return true;
	},
	updateUI: function(){
		// unselect all checks
		$(".mk-discountpercentage-filters .mk-labelx_check").removeClass("checked").addClass("unchecked");
								
		// update UI selection for price range in configArray
		var selector="mk-"+configArray["discountpercentage"]+"-discountpercentage-filter";
		$("."+selector).removeClass("unchecked").addClass("checked");
		return true;
	},
	setToDefault: function(){
		configArray["discountpercentage"]=[];
		return true;
	},
	resetParams: function(){
		return true;
	},
    updateFilterVisibility: function(filters){
        /*if(!userTriggered || (filterClickedName != "" && filterClickedName!="discountpercentage") || (filterClickedName != "" && filterClickedName=="discountpercentage" && configArray["discountpercentage"].length == 0)){
            $(".mk-discountpercentage-filters .mk-labelx_check").each(function (index,element){
                if(typeof filters.discount_percentage_details.counts == "undefined" || typeof filters.discount_percentage_details.counts[$(element).attr('data-value')] == 'undefined'){
                    $(element).find(".mk-filter-product-count").hide();
                    $(element).addClass('disabled').hide();
                }else{
                    $(element).find(".mk-filter-product-count").show().html("["+filters.discount_percentage_details.counts[$(element).attr('data-value')]+"]")
                    $(element).removeClass("disabled").show();
                }
            });
        }*/
    },
    setFilterClickedName: function(){
        filterClickedName = "discountpercentage";
	},
	updateClearFilterUI: function(){
		var clearFilterEl=$(".mk-discountpercentage-filters").closest("li").find(".mk-clear-filter");
		var clearAllFilterEl=$(".mk-clear-all-filters");
		if(configArray["discountpercentage"].length>0){
			clearFilterEl.removeClass("mk-hide");
		}
		else{
			clearFilterEl.addClass("mk-hide");
		}
		if(Myntra.Search.Utils.getNoOfFiltersSet()){
			clearAllFilterEl.removeClass("mk-hide");
		}
		else{
			clearAllFilterEl.addClass("mk-hide");
		}
	}
};
filterProperties["pricerange"]={
	getURLParam: function(){
		return "pricerange="+configArray["pricerange"].rangeMin+":"+configArray["pricerange"].rangeMax;
	},
	processURL: function(str){
		pricerange_values=Myntra.Search.getFilterValues(str);
		var minVal=parseInt(pricerange_values[0]);
		var maxVal=parseInt(pricerange_values[1]);
		if (!isNaN(minVal) && !isNaN(maxVal) && minVal >= priceRangeMin && minVal <= priceRangeMax && maxVal >= priceRangeMin && maxVal <= priceRangeMax) {
			if (minVal<maxVal) {
				configArray["pricerange"].rangeMin=minVal;
				configArray["pricerange"].rangeMax=maxVal;
				if (Myntra.Search.Data.optimEnabled) {
					Myntra.Search.Data.initJSON.search.fq.push(('discounted_price:[' + minVal + ' TO ' + maxVal + ']').replace('~-', '-').replace('~/', '\/'));
				}
			} else{
				configArray["pricerange"].rangeMin=maxVal;
				configArray["pricerange"].rangeMax=minVal;
				if (Myntra.Search.Data.optimEnabled) {
					Myntra.Search.Data.initJSON.search.fq.push(('discounted_price:[' + maxVal + ' TO ' + minVal + ']').replace('~-', '-').replace('~/', '\/'));
				}
			}
		}
		return true;
	},
	updateUI: function(){
		Myntra.Search.Utils.setPriceSlider(configArray["pricerange"].rangeMin,configArray["pricerange"].rangeMax);
		// unselect all checks
		$(".mk-price-filters .mk-labelx_check").removeClass("checked").addClass("unchecked");
								
		// update UI selection for price range in configArray
		var selector="mk-"+configArray["pricerange"].rangeMin+"-"+configArray["pricerange"].rangeMax+"-pricerange-filter";
		$("."+selector).removeClass("unchecked").addClass("checked");
		return true;
	},
	setToDefault: function(){
		configArray["pricerange"].rangeMin=priceRangeMin;
		configArray["pricerange"].rangeMax=priceRangeMax;
		return true;
	},
	resetParams: function(){
		return true;
	},
    updateFilterVisibility: function(filters){
        if(!userTriggered || (filterClickedName != "" && filterClickedName!="pricerange")){
        	if (Myntra.Search.Data.optimEnabled) {
		    	Myntra.Search.Utils.setPriceSlider(filters.discounted_price.rangeMin,filters.discounted_price.rangeMax);
        	} else {
		    	Myntra.Search.Utils.setPriceSlider(filters.price_range_details.rangeMin,filters.price_range_details.rangeMax);
		    }
        }
    },
    setFilterClickedName: function(){
        filterClickedName = "pricerange";
	},
	updateClearFilterUI: function(){
		var clearFilterEl=$(".mk-pricerange-filters").closest("li").find(".mk-clear-filter");
		var clearAllFilterEl=$(".mk-clear-all-filters");
		if(configArray["pricerange"].rangeMin != priceRangeMin || configArray["pricerange"].rangeMax != priceRangeMax){
			clearFilterEl.removeClass("mk-hide");
		}
		else{
			clearFilterEl.addClass("mk-hide");
			$(".mk-price-filters .mk-labelx_check").removeClass("checked").addClass("unchecked");
		}
		if(Myntra.Search.Utils.getNoOfFiltersSet()){
			clearAllFilterEl.removeClass("mk-hide");
		}
		else{
			clearAllFilterEl.addClass("mk-hide");
		}
	}
};
scrollProperties["scrollPage"]={
    getURLParam: function(){
        // configArray of page holds the page to be loaded, which is usually the next of the current page
    	// When returning from PDP, it should hold the current page, so we subtract 1 from it before going to PDP
    	return "scrollPage=" + (Myntra.Search.Data.optimEnabled 
    		? Myntra.Search.Data.initJSON.search.start + Myntra.Search.Data.initJSON.search.rows
    		: (parseInt(configArray["page"])-1));
    },
    processURL: function(str){
        scrollConfigArray["scrollPage"]=parseInt(str);
    },
    updateUI: function(){
    },
    setToDefault: function(){
    },
    resetParams: function(){
    },
    updateFilterVisibility: function(filters){
    },
    setFilterClickedName: function(){
    }
};
scrollProperties["scrollPos"]={
    getURLParam: function(){
        return "scrollPos="+$(window).scrollTop();
    },
    processURL: function(str){
        scrollConfigArray["scrollPos"]=parseInt(str);
    },
    updateUI: function(){
    },
    setToDefault: function(){
    },
    resetParams: function(){
    },
    updateFilterVisibility: function(filters){
    },
    setFilterClickedName: function(){
    }
};


Myntra.Search.getFilterValues = function(str){
    return str.replace(/-/g, ":").replace(/~:/g, "~-").split(":");   
}

Myntra.Search.InitNoResultsPage = function(){
	// Tie GA Calls to Clicks on display categories in no results page
	$("#no_result a, .no_result_disp_categories a").live("click", function(){
		if($(this).attr('href') != "undefined"){
			_gaq.push(['_trackEvent', 'no_results_page', 'browse', $(this).attr('href')]);
		}
	});
	
}

Myntra.Search.InitInfiniteScroll = function() {
	return true;
};

Myntra.Search.InfiniteScroll = (function(){
	var obj = {};
	
	var prevScrollPos=0,
		prevScrollPos=0,
		curScrollPos=0,
		infScrollCallInProgress=false,
		calltype="searchpagefilters";
	
	obj.pageSetToFirst=false;
	obj.pageSetFromHash=false;
	obj.defaultTopPosition=0;
	
	var productListingUlElem=$('.mk-search-grid>ul'),
		searchPageContentsElem=$("#mk-search-results"),
		moreProductsElem=$(".mk-more-products-link"),
		moreProductsAjaxLoaderElem=$(".more-products-loading-indicator"),
		ajaxLoaderElem=$(".mk-ajax-loader"),
		productsCountElem=$(".mk-product-count"),
		paginationLinksContainerElem=$(".pagination-links"),
		paginationInfoLinksAndSortbyElem=$(".pagination-info, .pagination-links, .sort-by-filters"),
		productsLeftCountElem=$(".products-left-count"),
		moreProductsSectionElem=$(".mk-infscroll-loader"),
		productsTotalCountElem=$(".total-products-count");
	
	obj.timerToDetectScroll = function() {
		if($(window).scrollTop() >= ($(".mk-search-grid").offset().top + $(".mk-search-grid").height() - heightFromLast)){
	       	curScrollPos=$(window).scrollTop();
	    	if(curScrollPos-prevScrollPos > 0){
    			if (Myntra.Search.Data.optimEnabled && !infScrollCallInProgress && (Myntra.Search.Data.initJSON.totalProductsCount > Myntra.Search.Data.initJSON.displayedProductsCount) && this.triggerAutoInfiniteScroll()) {
    				if (Myntra.Search.Data.initJSON.productsQueue.length < Myntra.Search.Data.initJSON.pageSize
    					&& Myntra.Search.Data.initJSON.displayedProductsCount + Myntra.Search.Data.initJSON.productsQueue.length < Myntra.Search.Data.initJSON.totalProductsCount) {
    					this.ajaxSearchServiceCall();
    				} else {
    					this.loadProductsFromQueue();
    				}
    			} else if (!Myntra.Search.Data.optimEnabled && !infScrollCallInProgress && !this.isNextPageOutOfLimit() && this.triggerAutoInfiniteScroll() && !batchProcess) {
	    			this.ajaxSearchCall();
	    		}
	    	}
	    	prevScrollPos=curScrollPos;
	    }
		
		return false;
	}
	
	obj.triggerManualInfiniteScroll = function(){
		if (Myntra.Search.Data.optimEnabled && !infScrollCallInProgress && (Myntra.Search.Data.initJSON.totalProductsCount > Myntra.Search.Data.initJSON.displayedProductsCount)) {
			if (Myntra.Search.Data.initJSON.productsQueue.length < Myntra.Search.Data.initJSON.pageSize
				&& Myntra.Search.Data.initJSON.displayedProductsCount + Myntra.Search.Data.initJSON.productsQueue.length < Myntra.Search.Data.initJSON.totalProductsCount) {
				this.ajaxSearchServiceCall();
			} else {
				this.loadProductsFromQueue();
			}
		} else if(!Myntra.Search.Data.optimEnabled && !infScrollCallInProgress && !this.isNextPageOutOfLimit()){
			this.ajaxSearchCall();
		}
		_gaq.push(['_trackEvent', 'search_page', 'showmoreproducts','manual']);
	}
	
	/*obj.isLimitReached = function(){
		return (current_page > total_pages)?true:false;
	}*/
	
	obj.triggerAutoInfiniteScroll = function(){
		if (Myntra.Search.Data.optimEnabled) current_page = Myntra.Search.Data.initJSON.displayedProductsCount/Myntra.Search.Data.initJSON.pageSize - 1;
		if(current_page<noOfAutoInfiniteScrolls){
			_gaq.push(['_trackEvent', 'search_page', 'showmoreproducts','auto']);
			return true;
		} else {
			return false;
		}
	}
	
	obj.ajaxSearchCall = function(){
		if(!this.pageSetToFirst){
			configArray["page"]=current_page+1;
		}
		if(!this.isNextPageOutOfLimit()){
			this.showLoadingGraphic(this.pageSetToFirst);
			infScrollCallInProgress=true;
			this.pageSetToFirst=false;
			if(Myntra.Data.sizeGroup){
				configArray["size_group"] = [Myntra.Data.sizeGroup];				
			}
			else{
				configArray["size_group"] = [];
			}			
			var querystr=JSON.stringify(configArray);
			Myntra.Search.Utils.ajaxCall.getData(calltype, http_loc + "/myntra/ajax_search.php?nav_id="+nav_id+"&productui="+shownewui+"&showadditionaldetail="+show_additional_product_details+"&prev_page="+encodeURIComponent(window.location.href), querystr, function(data){obj.updateSearchPageUI(data);});
		}
		return false;
	};
	
	obj.ajaxSearchServiceCall = function(){
		if (!this.pageSetToFirst) {
			Myntra.Search.Data.initJSON.search.start = Myntra.Search.Data.initJSON.search.start == 0
				? Myntra.Search.Data.initJSON.products.length
				: Myntra.Search.Data.initJSON.search.start + Myntra.Search.Data.initJSON.search.rows;
			Myntra.Search.Data.initJSON.search.facet = false;
			var queryObj = _.extend({}, Myntra.Search.Data.initJSON.search);
			if (queryObj.curated_query) delete queryObj.curated_query;
		} else {
			Myntra.Search.Data.initJSON.search.facet = true;
			var queryObj = Myntra.Search.Data.initJSON.search;
		}
		if (this.pageSetToFirst || !this.isNextPageOutOfLimit()) {
			this.showLoadingGraphic(this.pageSetToFirst);
			infScrollCallInProgress = true;
			this.pageSetToFirst = false;
			Myntra.Search.Utils.ajaxCall.getData(calltype, http_loc + "/searchws/search/styleids2", queryObj, function(data){obj.updateSearchPageUI(data);});
		}
		return false;
	};

	obj.loadProductsFromQueue = function () {
		this.showLoadingGraphic();
		obj.appendProductsHtml({}, true);
		obj.updateNumberOfProductsCount();
		this.hideLoadingGraphic();
		Myntra.Search.Utils.TriggerLazyLoad();
		Myntra.Utils.showAdminStyleDetails();
	};
	
	obj.isNextPageOutOfLimit = function(){
		return Myntra.Search.Data.optimEnabled
			? Myntra.Search.Data.initJSON.search.start >= Myntra.Search.Data.initJSON.totalProductsCount ? true : false
			: (configArray["page"] >= total_pages) ? true : false;
	};

	obj.updateSearchPageUI = function(data){
		
		if (Myntra.Search.Data.optimEnabled) {
			var result = data.response1 || data;
			Myntra.Search.Data.initJSON.products = result.products;
            if (result.products.length == 0) {
                    result.filters = {"discounted_price":{"rangeMin":0,"rangeMax":99,"breakUps":[]},"discount_percentage":{"count":{}},"sizes_facet":{}};
            }
		} else var result = $.parseJSON(data);
        if (!Myntra.Search.Data.optimEnabled || result.filters) this.updateLeftNavFilter(result.filters);

		this.updateProductCount(result);

		this.setPageCounters(result);
		
		this.hideLoadingGraphic();

		$('.mk-search-contents .mk-sub-text').remove();
	    if(result.query_level == 'FULL'){
			var additional_text_div = '<div class="mk-sub-text mk-help-text">NO MATCHES FOUND / SHOWING RESULTS FOR PARTIAL MATCHES INSTEAD</div>';
			$('.mk-search-contents .mk-product-count').before(additional_text_div);
		}	

		if (current_page == 0 || (this.pageSetFromHash && onPageLoad) || (Myntra.Search.Data.optimEnabled && Myntra.Search.Data.initJSON.displayedProductsCount == 0)) {
			this.replaceProductsHtml(result);
		} else {
			this.appendProductsHtml(result);
		}
		
		this.updateNumberOfProductsCount(result);

		this.pageSetFromHash=false;
		
		this.showProductListing();
		
		if(filterClicked){
			if($(window).scrollTop() > this.defaultTopPosition){
				Myntra.Search.Utils.scrollToTop(this.defaultTopPosition);
			}
			filterClicked=false;
		}
		
		Myntra.Search.Utils.TriggerLazyLoad();
		
		// show the additional details for the styles.
		Myntra.Utils.showAdminStyleDetails();
		
		//set remembered sizes
		Myntra.Data.fpcSizeGroup = result.fpcSizeGroup;

		infScrollCallInProgress=false;
	
		onPageLoad=false;

		if(Myntra.Data.appendedSavedSize&&!parseInt(result.product_count,10)){
			$('.mk-product-filters').find('ul.mk-product-selectors > li').eq(1).show();
			$('.mk-product-filters').find('ul.mk-product-selectors > li').eq(1).find('.mk-filter-wrapper').hide();
		}
		return false;
	};

    obj.updateLeftNavFilter = function(filters){
        this.updateFilterProperties(filters);    
        for(prop in filterProperties){
        	if (!onPageLoad || Myntra.Search.Data.filterParams.indexOf(prop) == -1) {
	            filterProperties[prop].updateFilterVisibility(filters);
	        }
        }
        onPageLoad = false;
        var dataKey="";
        $(".mk-labelx_check.checked").each(function(index,element){
            if(dataKey == "" ){
                dataKey = $(element).attr("data-key"); 
            }
            else if(dataKey != $(element).attr('data-key')){
                dataKey="";
                return false;
            }
        });
        if(dataKey !=""){
            $(".mk-"+dataKey+"-filters"+" .mk-labelx_check").each(function(index,element){
               $(element).removeClass('disabled').find(".mk-filter-product-count").show(); 
            });
        };
        userTriggered=false;
    };

    obj.updateFilterProperties = function(filters){
        var has_article_attr = false;
        for(attr in filters.article_type_attributes){
            has_article_attr = true;
            break;
        }
        if(!has_article_attr){
            $(".article_type_attr").remove();
            for(prop in filterProperties){
                if(prop.indexOf("_article_attr") != -1){
                	if($.inArray(prop,dynamicFilters) != -1){
                	    Myntra.Search.Utils.removeValueFromArray(prop,dynamicFilters);
                	}	
                    delete filterProperties[prop];
                }
            }
        }else if($(".article_type_attr").length == 0){
            for(filter_tmp in filters.article_type_attributes){
                var article_type_attr_value = filter_tmp.replace(/ /g,"_"),
                    isEmptyFilter = true;
                for (prop in filters.article_type_attributes[article_type_attr_value].values) {
                    if (prop.toLowerCase() != 'na') {isEmptyFilter = false; break;}
                }
                if (isEmptyFilter) continue;

                if($.inArray(article_type_attr_value,dynamicFilters) == -1){
                	dynamicFilters.push(article_type_attr_value);
                }
                (function(article_type_attr_value) {
                    configArray[article_type_attr_value]=[];
                    filterProperties[article_type_attr_value]={
                        getURLParam: function(){
                            var ataStr=configArray[article_type_attr_value].join(":");
                            return (ataStr != "")?article_type_attr_value+"="+ataStr:"";
                        },
                        processURL: function(str){
                            configArray[article_type_attr_value]=Myntra.Search.getFilterValues(str);
    						if (Myntra.Search.Data.optimEnabled) {
    							Myntra.Search.Data.initJSON.search.fq.push((article_type_attr_value + ':("' + configArray[article_type_attr_value].join('" OR "') + '")').replace('~-', '-').replace('~/', '\/'));
    						}
                            return true;
                        },
                        updateUI: function(){
                            // unselect all attributes
                            $(".article_type_attr .mk-labelx_check").removeClass("checked").addClass("unchecked");

                            // update UI selection for attributes in configArray
                            for(var attr=0;attr<configArray[article_type_attr_value].length;attr++){
                                var selector=configArray[article_type_attr_value][attr].toLowerCase().replace(/\W+/g, "") + "-ata-filter";
                                $("."+selector).removeClass("unchecked").addClass("checked");
                            }
                            return true;
                        },
                        setToDefault: function(){
                            configArray[article_type_attr_value]=[];
                            return true;
                        },
                        resetParams: function(){
                            configArray["page"]=0;
                            return true;
                        },
                        updateFilterVisibility: function(filters){
                            if((filterClickedName != "" && filterClickedName!=article_type_attr_value) || (filterClickedName != "" && filterClickedName==article_type_attr_value && configArray[article_type_attr_value].length == 0)){
                                $(".mk-"+article_type_attr_value+"-filters .mk-labelx_check").each(function (index,element){
                                    if((typeof filters.article_type_attributes[article_type_attr_value] == 'undefined') || (typeof filters.article_type_attributes[article_type_attr_value].values[$(element).attr('data-value')] == 'undefined')){
                                        $(element).addClass('disabled');
                                    }else{
                                        $(element).removeClass("disabled");
                                    }
                                });
                            }
                        },
                        setFilterClickedName: function(){
                            filterClickedName = article_type_attr_value;
                        },
                        updateClearFilterUI: function(){
                        	var clearFilterEl=$(".mk-"+article_type_attr_value+"-filters").closest("li").find(".mk-clear-filter");
                            if(configArray[article_type_attr_value].length >= 1){
                                clearFilterEl.removeClass("mk-hide");
                            }
                            else{
                                clearFilterEl.addClass("mk-hide");
                            }
                        }
                    };
                }(article_type_attr_value));
                // Append new li for this article type
                var props = filters.article_type_attributes[article_type_attr_value];
                var article_specific_attr_html = ''+
                '<li class="clearfix article_type_attr"><a href="javascript:void(0)" class="mk-a-label label-expand"><span class="mk-label-arrow"></span><span class="mk-f-left"> '+props.title+'</span><span data-key="'+props.title.replace(/ /g,'_')+'_article_attr" class="mk-f-right mk-clear-filter mk-hide">Reset</span></a>'+
                    '<div class="mk-filter-content">'+
                    '<div class="mk-filter-wrapper mk-'+props.title.replace(/ /g,'_')+'_article_attr-filters mk-resizable-box">';
                    for(prop in props.values){
                        if(prop.toLowerCase() != 'na'){
                        article_specific_attr_html += 
                        '<label class="mk-labelx_check unchecked '+prop.toLowerCase().replace(/ /g,"-").replace(/\W+/g,'')+'-ata-filter" data-key="'+props.title.replace(/ /g,"_")+'_article_attr" data-value="'+prop+'"><span class="cbx"></span>'+prop.toLowerCase().toUpperCase()+'</label>';
                        }
                    }
                     article_specific_attr_html += 
                    '</div>'+
                    '</div>'+
                '</li>';	

                $(".mk-product-selectors").append(article_specific_attr_html);
                
                $(".article_type_attr").each(function(){
        			if($(this).find(".mk-resizable-box").height() >= 150){
        				$(this).find(".mk-resizable-box").jScrollPane();
        			}
        		});
                
                filterProperties[article_type_attr_value].updateUI();
            }
        }
    }

	obj.replaceProductsHtml = function(result){
		if (Myntra.Search.Data.optimEnabled) {
			if (!result.products && !result.curated_products) {
				productListingUlElem.html('');
				return;
			}
			if (Myntra.Search.Data.initJSON.search.curated_query && result.curated_products) {
				Myntra.Search.Data.initJSON.productsQueue = result.curated_products.concat(Myntra.Search.Utils.filterOutCuratedProducts(result.products, result.curated_products));
			} else {
				Myntra.Search.Data.initJSON.productsQueue = result.products.slice(0); // shallow copy
			}
			Myntra.Search.Data.initJSON.productsToRender = Myntra.Search.Data.initJSON.productsQueue.splice(0, Myntra.Search.Data.initJSON.pageSize);
			Myntra.Search.Data.initJSON.displayedProductsCount = Myntra.Search.Data.initJSON.productsToRender.length;
			productListingUlElem.html(Myntra.Search.Templates.products(Myntra.Search.Data.initJSON));
		} else {
			productListingUlElem.html($(result.html));
		}
	};
	
	obj.appendProductsHtml = function(result, fromQueue){
		if (Myntra.Search.Data.optimEnabled) {
			if (!fromQueue) {
				if (Myntra.Search.Data.initJSON.search.curated_query && Myntra.Search.Data.initJSON.curatedProductIds) {
					Myntra.Search.Data.initJSON.productsQueue = Myntra.Search.Data.initJSON.productsQueue.concat(Myntra.Search.Utils.filterOutCuratedProducts(result.products));
				} else {
					Myntra.Search.Data.initJSON.productsQueue = Myntra.Search.Data.initJSON.productsQueue.concat(result.products);
				}
			}
			Myntra.Search.Data.initJSON.productsToRender = Myntra.Search.Data.initJSON.productsQueue.splice(0, Myntra.Search.Data.initJSON.pageSize);
			Myntra.Search.Data.initJSON.displayedProductsCount += Myntra.Search.Data.initJSON.productsToRender.length;
			productListingUlElem.append(Myntra.Search.Templates.products(Myntra.Search.Data.initJSON));
		} else {
			productListingUlElem.append($(result.html));
		}
	};
	
	obj.showLoadingGraphic = function(blockUI) {
		if (blockUI) ajaxLoaderElem.show();
		moreProductsElem.hide();
		moreProductsAjaxLoaderElem.show();
	};
	
	obj.hideLoadingGraphic = function() {
		ajaxLoaderElem.hide();
		moreProductsElem.show();
		moreProductsAjaxLoaderElem.hide();
	};
	
	obj.showProductListing = function(){
		searchPageContentsElem.css("visibility","visible");
	};
	
	obj.hideProductListing = function(){
		searchPageContentsElem.css("visibility","hidden");
	};
	
	obj.updateProductCount = function(result, force) {
		if (Myntra.Search.Data.optimEnabled) {
			if (force || Myntra.Search.Data.initJSON.displayedProductsCount == 0) {
				Myntra.Search.Data.initJSON.totalProductsCount = result.totalProductsCount;
				if (result.totalProductsCount) {
					paginationInfoLinksAndSortbyElem.show();
					productsCountElem.html(result.totalProductsCount + ((result.totalProductsCount == 1) ? " Product" : " Products"));
				} else {
					paginationInfoLinksAndSortbyElem.hide();
					productsCountElem.html("No Products");				
				}
			}
		} else {
			if(result.product_count != null){
				paginationInfoLinksAndSortbyElem.show();
				productsCountElem.html(result.product_count + ((result.product_count == 1)?" Product":" Products"));
				// if($(".full-search-message-block")[0]){
					// $(".full-search-message-block:hidden").show();
				// }
			}	
			else{
				paginationInfoLinksAndSortbyElem.hide();
				productsCountElem.html("No Products");
				// if($(".full-search-message-block")[0]){
					// $(".full-search-message-block:visible").hide();
				// }
			}
		}
	};
	
	obj.setPageCounters = function(result){
		current_page=parseInt(result.current_page);
		total_pages=parseInt(result.page_count);
	};
	
	obj.updateNumberOfProductsCount = function(result){
		if (Myntra.Search.Data.optimEnabled) {
			products_left_count = Myntra.Search.Data.initJSON.totalProductsCount - Myntra.Search.Data.initJSON.displayedProductsCount;
		} else {
			var total_products=parseInt(result.product_count);
			products_left_count = total_products - ((current_page+1)*default_noofitems);
		}
		if(products_left_count > 0){
			moreProductsSectionElem.show();
			productsLeftCountElem.html(products_left_count);
			// productsTotalCountElem.html(result.product_count);
		}
		else{
			moreProductsSectionElem.hide();
		}
	};
	
	return obj;
})();

Myntra.Search.RecursiveLoader = (function(){
	var obj = {};
	obj.pagecount;
	obj.pagecount_temp;
	obj.scrollpos;
	obj.counter=0;
	obj.newConfigArray;

	var moreProductsSectionElem=$(".mk-infscroll-loader"),
		productsTotalCountElem=$(".total-products-count"),
		productsLeftCountElem=$(".products-left-count"),
		ajaxLoaderElem=$(".mk-ajax-loader"),
		productListingUlElem=$('.mk-search-grid>ul');
	
	obj.initBatchRequestParams = function(newConfigArray){
		$(window).scrollTop(0);
		batchProcess=true;
		obj.newConfigArray=newConfigArray;
		obj.pagecount=scrollConfigArray["scrollPage"];
		obj.pagecount_temp=scrollConfigArray["scrollPage"];
		obj.scrollpos=scrollConfigArray["scrollPos"];
		if (Myntra.Search.Data.optimEnabled) obj.makeBatchSearchServiceCall();
		else obj.makeBatchRequest();
	}
	
	obj.makeBatchRequest = function(){
		/*
		 * if((obj.pagecount)>4){ obj.setParamsForNextCall(obj.pagecount);
		 * obj.makeBatchAjaxRequest(true); } else{
		 */
			obj.setParamsForNextCall(obj.pagecount);
			if(obj.pagecount>=0){
				obj.makeBatchAjaxRequest(true);
			}
			else{
				obj.makeBatchAjaxRequest(false);
			}
		/* } */
	}
	
	obj.makeBatchAjaxRequest = function(doNextCall){
		var querystr=JSON.stringify(obj.newConfigArray);
		Myntra.Search.Utils.ajaxCall.getData("test", http_loc + "/myntra/ajax_search.php?nav_id="+nav_id+"&productui="+shownewui+"&showadditionaldetail="+show_additional_product_details, querystr, function(data){
			obj.updateSearchPageUI(data);
			obj.counter++;
			if(doNextCall){
				obj.makeBatchRequest();
			}
			else{
				obj.setOriginalConfig();
				batchProcess=false;
				filterClicked=false;
			}
		});
		
		return false; 
	}
	
	obj.makeBatchSearchServiceCall = function() {
		Myntra.Search.Utils.setPageToFirst();
		var queryObj = _.extend({}, Myntra.Search.Data.initJSON.search);
		Myntra.Search.Data.initJSON.search.start = obj.pagecount - queryObj.rows; // offset for next call, because rows get incremented in ajax call
		queryObj.rows = obj.pagecount;
		Myntra.Search.Utils.ajaxCall.getData("test", http_loc + "/searchws/search/styleids2", queryObj, function(data){
			obj.updateSearchPageUI(data);
			obj.setOriginalConfig();
			batchProcess=false;
			filterClicked=false;
		});
		
		return false; 
	}
	
	obj.setParamsForNextCall = function(pagecount){
			obj.pagecount=obj.pagecount-1;
			obj.newConfigArray["noofitems"]=24;
			obj.newConfigArray["page"]=scrollConfigArray["scrollPage"]-(obj.pagecount+1);
	}
	
	obj.setOriginalConfig = function(){
		if (Myntra.Search.Data.optimEnabled) {
			if (Myntra.Search.Data.initJSON.search.start > Myntra.Search.Data.initJSON.totalProductsCount) {
				moreProductsSectionElem.hide();
			}
		} else {
			var next_page=parseInt(scrollConfigArray["scrollPage"]);
			if(next_page>total_pages){
				configArray["page"]=total_pages;
			}
			else{
				configArray["page"]=next_page+1;
			}
			if(total_pages-1 == next_page-1){
				moreProductsSectionElem.hide();
			}			
		}
		scrollConfigArray["scrollProcessed"]=true;
		Myntra.Search.Utils.scrollToTop(scrollConfigArray["scrollPos"]);
		Myntra.Search.InfiniteScroll.pageSetToFirst=false;
		Myntra.Search.InfiniteScroll.pageSetFromHash=false;
	}
	
	obj.updateNumberOfProductsCount = function(result){
		if (Myntra.Search.Data.optimEnabled) {
			products_left_count = Myntra.Search.Data.initJSON.totalProductsCount - Myntra.Search.Data.initJSON.displayedProductsCount;
		} else {
			var total_products=parseInt(result.product_count);
			products_left_count = total_products - ((scrollConfigArray["scrollPage"]+1)*24);
		}		
		if(products_left_count > 0){
			moreProductsSectionElem.show();
			productsLeftCountElem.html(products_left_count);
		}
		else{
			moreProductsSectionElem.hide();
		}
	}
	
	obj.updateSearchPageUI = function(data){
		
		if (Myntra.Search.Data.optimEnabled) {
			var result = data.response1;
			Myntra.Search.Data.initJSON.products = result.products;
            if (result.products.length == 0) {
                    result.filters = {"discounted_price":{"rangeMin":0,"rangeMax":99,"breakUps":[]},"discount_percentage":{"count":{}},"sizes_facet":{}};
            }
		} else var result = $.parseJSON(data);
		
		if (!Myntra.Search.Data.optimEnabled || result.filters) Myntra.Search.InfiniteScroll.updateLeftNavFilter(result.filters);
	
		Myntra.Search.InfiniteScroll.updateProductCount(result, true);

		Myntra.Search.InfiniteScroll.setPageCounters(result);
		
		if (obj.newConfigArray["page"] == 0 || (Myntra.Search.Data.optimEnabled && Myntra.Search.Data.initJSON.displayedProductsCount == 0)) {
			obj.replaceProductsHtml(result);
		} else {
			Myntra.Search.InfiniteScroll.appendProductsHtml(result);
		}
		
		obj.updateNumberOfProductsCount(result);
		
		Myntra.Search.InfiniteScroll.showProductListing();
		
		Myntra.Search.Utils.TriggerLazyLoad();
		
		// show the additional details for the styles.
		Myntra.Utils.showAdminStyleDetails();
		
		infScrollCallInProgress=false;
		
		if (Myntra.Search.Data.stickyFiltersEnabled) {
			Myntra.Search.Utils.stickyFilters.recalc();
		}

		return false;
	}
	
	obj.replaceProductsHtml = function(result) {
		var overflow;
		if (Myntra.Search.Data.optimEnabled) {
			if (!result.products && !result.curated_products) {
				productListingUlElem.html('');
				return;
			}
			if (Myntra.Search.Data.initJSON.search.curated_query && result.curated_products) {
				Myntra.Search.Data.initJSON.productsToRender = result.curated_products.concat(Myntra.Search.Utils.filterOutCuratedProducts(result.products, result.curated_products));
			} else {
				Myntra.Search.Data.initJSON.productsToRender = result.products.slice(0); // shallow copy
			}
			overflow = Myntra.Search.Data.initJSON.productsToRender.length % 24;
			Myntra.Search.Data.initJSON.productsQueue = Myntra.Search.Data.initJSON.productsToRender.splice(-overflow, overflow);
			Myntra.Search.Data.initJSON.displayedProductsCount = Myntra.Search.Data.initJSON.productsToRender.length;
			productListingUlElem.html(Myntra.Search.Templates.products(Myntra.Search.Data.initJSON));
		} else {
			productListingUlElem.html($(result.html));
		}
	};
	
	obj.appendProductsHtml = function(result){
		productListingUlElem.append($(result.html));
	};
	
	obj.showLoadingGraphic = function(){
		ajaxLoaderElem.show();
		moreProductsElem.hide();
		moreProductsAjaxLoaderElem.show();
	};
	
	obj.hideLoadingGraphic = function(){
		ajaxLoaderElem.hide();
		moreProductsElem.show();
		moreProductsAjaxLoaderElem.hide();
	};
	
	obj.showProductListing = function(){
		searchPageContentsElem.css("visibility","visible");
	};
	
	obj.hideProductListing = function(){
		searchPageContentsElem.css("visibility","hidden");
	};	
		
	return obj;
})();

Myntra.Search.Init = function(){
	
    onPageLoad = true;
    Myntra.Search.Data.filterParams = [];
    
	// brands=Rockport/sizes=/gender=/pricerange=100-18099/noofitems=24/sortby=/page=1
	$(window).hashchange( function(event){
		if(!onBeforeUnloadFired){
			filterClicked=true;
			for(prop in filterProperties){
				filterProperties[prop].setToDefault();
				filterProperties[prop].updateUI();
				filterProperties[prop].updateClearFilterUI();
			}
			if (Myntra.Search.Data.optimEnabled) {
				var articleTypes = Myntra.Search.Data.searchArticleTypeList.split(',');
				Myntra.Search.Data.initJSON.search.fq = [];
				Myntra.Search.Data.initJSON.search.facetField = articleTypes.length != 1 ? [] : Myntra.Search.Data.initJSON.all_article_attribute[Myntra.Utils.String.toTitleCase(articleTypes[0].replace('-', ' '))];
				Myntra.Search.Data.initJSON.search.facet = false;
			}
			for(prop in scrollProperties){
				scrollProperties[prop].setToDefault();
				scrollProperties[prop].updateUI();
			}
			
			var hash_str = location.hash,
				query_str = hash_str.substring(hash_str.indexOf("#!")+2, hash_str.length);
			if (query_str != "") {
				Myntra.Search.Utils.setPageToFirst();
				// Recursive (return from PDP)
				if (query_str.indexOf("scrollPos") >= 0 && query_str.indexOf("scrollPage") >= 0 && !scrollConfigArray["scrollProcessed"] && ajaxCallFunc=="InfiniteScroll"){
					var query_options=query_str.replace(/\//g, "|").replace(/~\|/g,"~/").split("|");
					var optionslength=query_options.length;
					for(var option=0;option<optionslength;option++){
						var token=query_options[option];
						var keyvalue=token.split("=");
						if(filterProperties[keyvalue[0]] !== undefined && typeof keyvalue[1] != "undefined" && keyvalue[1] != ""){
							if (Myntra.Search.Data.optimEnabled && keyvalue[0] != 'sortby') Myntra.Search.Data.initJSON.search.facet = true;
							Myntra.Search.Data.filterParams.push(keyvalue[0]);
							filterProperties[keyvalue[0]].processURL(keyvalue[1]);
							filterProperties[keyvalue[0]].updateUI();
							filterProperties[keyvalue[0]].updateClearFilterUI();
						}
						if(scrollProperties[keyvalue[0]] !== undefined && typeof keyvalue[1] != "undefined" && keyvalue[1] != ""){
							scrollProperties[keyvalue[0]].processURL(keyvalue[1]);
							scrollProperties[keyvalue[0]].updateUI();					
						}
					}
					
					//configArray["filteredByUser"] = true;
					var newConfigArray = $.extend(true, {}, configArray);
					onPageLoad=true;
					Myntra.Search.RecursiveLoader.initBatchRequestParams(newConfigArray);
					scrollConfigArray["scrollProcessed"]=true;
				} else {
					var query_options=query_str.replace(/\//g, "|").replace(/~\|/g,"~/").split("|");
					var optionslength=query_options.length;
					for(var option=0;option<optionslength;option++){
						var token=query_options[option];
						var keyvalue=token.split("=");
						if(filterProperties[keyvalue[0]] !== undefined && typeof keyvalue[1] != "undefined" && keyvalue[1] != ""){
							Myntra.Search.Data.filterParams.push(keyvalue[0]);
							filterProperties[keyvalue[0]].processURL(keyvalue[1]);
							filterProperties[keyvalue[0]].updateUI();			
							filterProperties[keyvalue[0]].updateClearFilterUI();
						}						
					}
					
					//configArray["filteredByUser"] = true;
					if (Myntra.Search.Data.optimEnabled) {
						Myntra.Search.Data.initJSON.search.facet = true;
						Myntra.Search[ajaxCallFunc].ajaxSearchServiceCall();	
					} else {
						Myntra.Search[ajaxCallFunc].ajaxSearchCall();
					}
				}		
			} else { // all filters cleared
				configArray["page"]=1;
				filterClicked=false;
				if (!onPageLoad) {
					Myntra.Search.Utils.setPageToFirst();
					for(prop in filterProperties){
						filterProperties[prop].updateUI();
						filterProperties[prop].updateClearFilterUI();
					}
					//configArray["filteredByUser"] = true;
					if (Myntra.Search.Data.optimEnabled) {
						Myntra.Search.Data.initJSON.search.facet = true;
						Myntra.Search[ajaxCallFunc].ajaxSearchServiceCall();	
					} else {
						Myntra.Search[ajaxCallFunc].ajaxSearchCall();
					}
				}
			}
			//onPageLoad=false;
		}
	});

	if(Myntra.Search.Data.optimEnabled) {
		Myntra.Search.InfiniteScroll.showLoadingGraphic(true);
		_.templateSettings['variable'] = 'data';
		Myntra.Search.Templates = Myntra.Search.Templates || {};
		Myntra.Search.Templates.products = _.template($('#search-products-template').html());
		Myntra.Search.Templates.filters = _.template($('#search-filters-template').html());
		var hash_str = location.hash,
			query_str = hash_str.substring(hash_str.indexOf("#!")+2, hash_str.length);
		if (!query_str.length) {
			if (Myntra.Search.Data.initJSON.curated_products) {
				Myntra.Search.Data.initJSON.productsQueue = Myntra.Search.Data.initJSON.curated_products.concat(Myntra.Search.Utils.filterOutCuratedProducts(Myntra.Search.Data.initJSON.products));
			} else {
				Myntra.Search.Data.initJSON.productsQueue = Myntra.Search.Data.initJSON.products.slice(0); // shallow copy
			}
			Myntra.Search.Data.initJSON.productsToRender = Myntra.Search.Data.initJSON.productsQueue.splice(0, Myntra.Search.Data.initJSON.pageSize);
			Myntra.Search.Data.initJSON.displayedProductsCount = Myntra.Search.Data.initJSON.productsToRender.length;
			//$('#mk-search-results > ul').html(Myntra.Search.Templates.products(Myntra.Search.Data.initJSON));
			Myntra.Search.InfiniteScroll.updateNumberOfProductsCount();
		}
		$('.mk-left-nav').html(Myntra.Search.Templates.filters(Myntra.Search.Data.initJSON));
		Myntra.Search.InfiniteScroll.hideLoadingGraphic();
		Myntra.Search.InfiniteScroll.pageSetToFirst = false;
	}
	
	$(window).hashchange();
	
	if($( ".mk-resizable-box" ).length){
		$(".mk-resizable-box").each(function(){
			if($(this).height() >= 150){
				$(this).jScrollPane();
			}
		});
        // We have enabled lazy loading of filter elements on the search page
        // Since they will be loaded after onload we will have to refresh the 
        // jsScrollPane if the height has increased
        $(".mk-resizable-box").each(function(){
            var that = this;
            var lazyHandler = function(){
                $(that).unbind("lazy.change");
                //Let all updates happen so wait for 50 msecs
                setTimeout(
                    function(){
                        if($(that).height() >= 150){
                            $(that).jScrollPane();
                        }
                        $(that).bind("lazy.change", lazyHandler);
                        Myntra.Search.Utils.checkLazyLoadedFilters(that);
                    }, 50);                
            }
            $(this).bind("lazy.change", lazyHandler);
        });
    }
	
	var brandsJScrollPane=$("#mk-brands").data("jsp");
	
	if($( "#mk-brands" ).length){
		var brandsIndex=[];
		var b=0;
		$("#mk-brands .mk-labelx_check").each(function(){
			brandsIndex.push({
				label:$(this).attr("data-value"),
				show:0
			});
		});
		var a=$.ui.autocomplete.prototype._renderItem;
		
		$(".brands-autocomplete-input").autocomplete({
			source:brandsIndex,
            type:"brandsbox",
			appendTo:"#mk-brand-search-results",
			search:function(){
				hideAllBrands();
				brandsJScrollPane.scrollTo(0, 0);
			}
		});

        $("#mk-brands").bind("lazy.change", function(){
            $("#mk-brands").unbind("lazy.change");
            var that = this;
            setTimeout(function(){
                var brandsIndex=[];
                $("#mk-brands .mk-labelx_check").each(function(){
                    brandsIndex.push({
                        label:$(this).attr("data-value"),
                        show:0
                    });
                });

                $(".brands-autocomplete-input").autocomplete({
                    source:brandsIndex,
                    type:"brandsbox",
                    appendTo:"#mk-brand-search-results",
                    search:function(){
                        hideAllBrands();
                        brandsJScrollPane.scrollTo(0, 0);
                    }
                })
                $("#mk-brands").bind("lazy.change", that);
            }, 50);                       
        });
		
		function showAllBrands(){
			$("#mk-brands .mk-labelx_check").show();
		}
		
		function hideAllBrands(){
			$("#mk-brands .mk-labelx_check").hide();
		}
		
		$(".mk-clear-brands-input").hide();
		
		$(".brands-autocomplete-input").bind("click focus keyup blur", function() {
			var bai = $(".brands-autocomplete-input");
			var cbi = bai.siblings(".mk-clear-brands-input");
			if(bai.val() == "" || bai.val() == bai.attr("data-placeholder")) {
				showAllBrands();
				cbi.hide();
			} else { 
				cbi.show();
			}
		});
		
		$(".brands-autocomplete-input").bind("click focus", function() {
			var bai = $(".brands-autocomplete-input");
			if(bai.val() == bai.attr("data-placeholder")) {
				bai.val("");
			}
		});
		
		$(".brands-autocomplete-input").bind("blur", function() {
			var bai = $(".brands-autocomplete-input");
			if(bai.val() == "") {
				bai.val(bai.attr('data-placeholder'));
			}
		});
		
		$(".mk-clear-brands-input").bind("click", function(e) {
			var bai = $(".brands-autocomplete-input");
			var cbi = bai.siblings(".mk-clear-brands-input");
			bai.val("");
			cbi.hide();
			bai.trigger("focus");
			_gaq.push(['_trackEvent', 'search_page_filters', 'brands_auto_complete','clear']);
			e.stopPropagation();
		});
		
	}
	
	if($( ".mk-price-range" ).length){
		// Price Range Slider ----------------------
	    $( ".mk-price-range" ).slider({
	        range: true,
	        min: priceRangeMin,
	        max: priceRangeMax,
	        values: [ priceRangeMin, priceRangeMax ],
	        slide: function( event, ui ) {
	            $( ".mk-amount" ).html( "Rs. " + Myntra.Search.Utils.toCurrencyString(ui.values[ 0 ]) + " - Rs. " + Myntra.Search.Utils.toCurrencyString(ui.values[ 1 ]) );
	        },
			stop: function(event, ui){// This event is triggered when the user
										// stops sliding.
                filterProperties["pricerange"].setFilterClickedName();
				Myntra.Search.Utils.priceRange(ui.values[0], ui.values[1]);
				filterProperties["pricerange"].updateClearFilterUI();
				_gaq.push(['_trackEvent', 'search_page_filters', 'pricerange','slider']);
			}
	    });
	    $( ".mk-amount" ).html( "Rs. " + Myntra.Search.Utils.toCurrencyString($( ".mk-price-range" ).slider( "values", 0 )) +
	        " - Rs. " + Myntra.Search.Utils.toCurrencyString($( ".mk-price-range" ).slider( "values", 1 )) );
	}
	$slider = $( ".mk-price-range" );// Caching slider object
	$amount = $( ".mk-amount" );// Caching amount object
	
    $(".mk-a-label").live('click', function(e){
    	el=$(this);
    	var contentText = el.text();
    	contentText = $.trim(contentText);
		if(el.hasClass("label-expand")){
			el.parent().find(".mk-filter-content").hide("slow", function(){
				el.removeClass("label-expand");
				if (Myntra.Search.Data.stickyFiltersEnabled) {
					Myntra.Search.Utils.stickyFilters.recalc();
				}
			});
			_gaq.push(['_trackEvent', 'search_page_filters', contentText,'collapse']);
		}
		else{
			el.parent().find(".mk-filter-content").show("slow", function(){
				el.addClass("label-expand");
				if (Myntra.Search.Data.stickyFiltersEnabled) {
					Myntra.Search.Utils.stickyFilters.recalc();
				}
			});
			_gaq.push(['_trackEvent', 'search_page_filters', contentText,'expand']);
		}
		return false;
	});
    
	$(".mk-labelx_check:not(.price-range-filter,.discountpercentage-filter)").live('click', function(e){
		if(!$(this).hasClass("disabled")){
			var _this = $(this),
				key= _this.attr("data-key"),
				value= _this.attr("data-value").replace(/\//g,"~/").replace(/-/g,"~-"),
				sizeGroup = _this.attr('data-sizeGroup'),
				check = true,
				sizeCheck = false;
			if(_this.hasClass("unchecked")){
				_this.removeClass("unchecked").addClass("checked");
				if($.inArray(value,configArray[key]) == -1){
					configArray[key].push(value);
				}
			}
			else{
				_this.removeClass("checked").addClass("unchecked");
				check = false;
				if($.inArray(value,configArray[key]) != -1){                    
					configArray[key] = Myntra.Search.Utils.removeValueFromArray(value, configArray[key]);
				}
			}
			//$('.mk-product-sizes').hide();
			if((sizeGroup !== undefined) && (sizeGroup !== false) && (Myntra.Data.showStickySize==='test')){
				$('.mk-product-filters').find('ul.mk-product-selectors > li').eq(1).find('.mk-filter-wrapper').show();
				Myntra.Search.Utils.checkForSize(sizeGroup,check);
				sizeCheck = true;
			}
			else if(Myntra.Data.showStickySize==='control'){
				$('.mk-filter-rem').hide();
			}
			if(key==='sizes'){
				configArray['timeStamp'] = new Date().getTime();
				$('.mk-filter-rem').find('.first').show();
				$('.mk-filter-rem').find('.second').hide();	
			}
			filterProperties[key].updateClearFilterUI();
			filterProperties[key].setFilterClickedName();
			//Myntra.Search.Utils.setPageToFirst();
			Myntra.Search.Utils.setFilterOptionsInUrlHash([key],sizeCheck);
			_gaq.push(['_trackEvent', 'search_page_filters', key, value]);
			userTriggered=true;
		}
		e.preventDefault();
		return false;
	});	
	$(".mk-labelx_check.discountpercentage-filter").live('click', function(e){
		var key=$(this).attr("data-key");
		var value=$(this).attr("data-value");
        if($(this).hasClass("unchecked")){
            $(this).removeClass("unchecked").addClass("checked");
            configArray[key]=value;
        }
        else{
            $(this).removeClass("checked").addClass("unchecked");
            configArray[key]=[];
        }
		filterProperties[key].updateClearFilterUI();
	    filterProperties[key].setFilterClickedName();
		//Myntra.Search.Utils.setPageToFirst();
		Myntra.Search.Utils.setFilterOptionsInUrlHash([key]);
		_gaq.push(['_trackEvent', 'search_page_filters', key, 'click']);
		userTriggered=true;
	});
	$(".mk-labelx_check.price-range-filter").live('click', function(e){
		var key=$(this).attr("data-key");
		var minVal=$(this).attr("data-rangeMin");
		var maxVal=$(this).attr("data-rangeMax");
		if($(this).hasClass("checked")){
			$(this).removeClass("checked").addClass("unchecked");
			Myntra.Search.Utils.priceRange(priceRangeMin, priceRangeMax);
			filterProperties[key].updateClearFilterUI();
		}
		else{
			$(".mk-labelx_check.price-range-filter").removeClass("checked").addClass("unchecked");
			$(this).removeClass("unchecked").addClass("checked");
			Myntra.Search.Utils.priceRange(minVal, maxVal);
			filterProperties[key].updateClearFilterUI();
		}
	    filterProperties[key].setFilterClickedName();
		_gaq.push(['_trackEvent', 'search_page_filters', key, 'click']);
		userTriggered=true;
	});
	
	$(".mk-clear-filter").live('click', function(e){
        filterClickedName = "none";
        userTriggered=true;
		var key=$(this).attr("data-key");
		if (key == 'brands') {
			$(".mk-clear-brands-input").click();
			$(".brands-autocomplete-input").blur();
		}
		if(key=='sizes' && Myntra.Data.showStickySize=='test'){
			$('.mk-filter-rem').stop().show();
			$('.mk-filter-rem .first').show();
			$('.mk-filter-rem .second').hide();
			$('.mk-product-filters').find('ul.mk-product-selectors > li').eq(1).find('.mk-filter-wrapper').show();
		}
		filterProperties[key].setToDefault();
		filterProperties[key].updateClearFilterUI();
		 for(var m=0;m<dynamicFilters.length;m++){
             filterProperties[dynamicFilters[m]].setToDefault();
			 filterProperties[dynamicFilters[m]].updateClearFilterUI();
		 }

		//Myntra.Search.Utils.setPageToFirst();
		Myntra.Search.Utils.setFilterOptionsInUrlHash([key].concat(dynamicFilters));
		_gaq.push(['_trackEvent', 'search_page_filters', 'clear_filter',key]);
		e.stopPropagation();		
	});
	
	$(".mk-clear-all-filters").click(function(e){
        filterClickedName = "none";
        userTriggered=true;
        $(".mk-clear-brands-input").click();
        $(".brands-autocomplete-input").blur();
		for(prop in filterProperties){
			filterProperties[prop].setToDefault();
			filterProperties[prop].updateClearFilterUI();
		}
		//Myntra.Search.Utils.setPageToFirst();
		Myntra.Search.Utils.setFilterOptionsInUrlHash([]);
		_gaq.push(['_trackEvent', 'search_page_filters', 'clear_all_filters']);
		e.stopPropagation();		
	});
	
	$('.mk-product-filters').delegate('.close-rem','click',function(e){
		e.preventDefault();
		$(this).parents('.mk-filter-rem').stop(true).hide(600);
	});
	
	var scrollTimerID;
	
	$(window).scroll(function() {
		if(scrollTimerID){
			clearTimeout(scrollTimerID);
			scrollTimerID=null;
		}
		scrollTimerID=setTimeout(function(){Myntra.Search.InfiniteScroll.timerToDetectScroll();}, 330);
		return false;
	});
	
	$(window).bind("scroll resize", function() {
		Myntra.Search.FilterScroller.timerToDetectScroll($(".mk-product-selectors").offset().top + $(".mk-product-selectors").height());
	});
	
	var ajaxLoaderScrollTimerID;
	var ajaxLoadeInitialOffsetFromTop=$(".mk-ajax-loader-msg").css("top");
	if( ajaxLoadeInitialOffsetFromTop != null){
		 ajaxLoadeInitialOffsetFromTop= ajaxLoadeInitialOffsetFromTop.substring(0, ajaxLoadeInitialOffsetFromTop.indexOf("px"));
		 $(window).scroll(function() {
			Myntra.Search.Utils.AjaxLoaderPositionReset(ajaxLoadeInitialOffsetFromTop);
		});
	}
	
	$(".go-to-top-btn").click(function(){
		Myntra.Search.FilterScroller.hideGoToTop();
		Myntra.Search.Utils.scrollToTop(0);
		_gaq.push(['_trackEvent', 'search_page', 'go_to_top','click']);
	});
	
	Myntra.Search.Utils.TriggerLazyLoad();
		
	Myntra.Search.InfiniteScroll.defaultTopPosition = $(".mk-category-page").offset().top - $('.mk-site-nav>.mk-level-1').outerHeight() - 6;
	
	return true;
};

Myntra.Search.Utils.AjaxLoaderPositionReset = function(initialOffsetFromTop){
	var searchResultsSectionOffset=$(".mk-search-contents").offset().top;
	var searchResultsSectionHeight=searchResultsSectionOffset + $(".mk-search-contents").height();
	var currentLoaderMsgOffsetTop=$(".mk-ajax-loader-msg").css("top");
	
	if(currentLoaderMsgOffsetTop != null){
		currentLoaderMsgOffsetTop=currentLoaderMsgOffsetTop.substring(0,currentLoaderMsgOffsetTop.indexOf("px"))
	}
	
	var newPosition=parseInt(initialOffsetFromTop)+parseInt($(window).scrollTop());
	if( newPosition > initialOffsetFromTop && newPosition < (searchResultsSectionHeight+ searchResultsSectionOffset)){
		$(".mk-ajax-loader-msg").css({top: newPosition});
	}
	else{
		$(".mk-ajax-loader-msg").css({top: initialOffsetFromTop});
	}
}

Myntra.Search.Utils.setPriceSlider = function(min, max){
	var hs=$(".mk-price-range");
	hs.slider('values', 0, min);
	hs.slider('values', 1, max);
	$( ".mk-amount" ).html( "Rs. " + Myntra.Search.Utils.toCurrencyString($( ".mk-price-range" ).slider( "values", 0 )) + " - Rs. " + Myntra.Search.Utils.toCurrencyString($( ".mk-price-range" ).slider( "values", 1 )) );
};
Myntra.Search.Utils.setPriceSliderRange = function(priceRangeMin, priceRangeMax){
    $( ".mk-price-range" ).slider("destroy");
    $( ".mk-price-range" ).slider({
        range: true,
        min: priceRangeMin,
        max: priceRangeMax,
        values: [ priceRangeMin, priceRangeMax ],
        slide: function( event, ui ) {
            $( ".mk-amount" ).html( "Rs. " + Myntra.Search.Utils.toCurrencyString(ui.values[ 0 ]) + " - Rs. " + Myntra.Search.Utils.toCurrencyString(ui.values[ 1 ]) );
        },
        stop: function(event, ui){// This event is triggered when the user
									// stops sliding.
            filterProperties["pricerange"].setFilterClickedName();
            Myntra.Search.Utils.priceRange(ui.values[0], ui.values[1]);
            filterProperties["pricerange"].updateClearFilterUI();
            _gaq.push(['_trackEvent', 'search_page_filters', 'pricerange','slider']);
        }
    });
    $( ".mk-amount" ).html( "Rs. " + Myntra.Search.Utils.toCurrencyString($( ".mk-price-range" ).slider( "values", 0 )) +
        " - Rs. " + Myntra.Search.Utils.toCurrencyString($( ".mk-price-range" ).slider( "values", 1 )) );
};

Myntra.Search.Utils.setPriceSliderToDefault = function(){
	var hs=$( ".mk-price-range" );
	hs.slider('values', 0, priceRangeMin);
	hs.slider('values', 1, priceRangeMax);
	$( ".mk-amount" ).html( "Rs. " + Myntra.Search.Utils.toCurrencyString($( ".mk-price-range" ).slider( "values", 0 )) + " - Rs. " + Myntra.Search.Utils.toCurrencyString($( ".mk-price-range" ).slider( "values", 1 )) );
};

// show page info in title and meta desc if pgage>1
/*
 * Myntra.Search.Utils.pageDrivenTitleMeta = function(currentPage, totalPages){
 * if(currentPage > 1){ document.title = pageTitleNoPage+' | Page
 * '+currentPage+'/'+totalPages;
 * $('meta[name="description"]').attr('content',metaDescNoPage+' Page
 * '+currentPage+' of '+totalPages); } else { document.title = pageTitleNoPage;
 * $('meta[name="description"]').attr('content',metaDescNoPage); } };
 */

// show description for page 1 only
/*
 * Myntra.Search.Utils.toggleDescription = function(currentPage){ var
 * descriptionObj = $('#search_description'); if(descriptionObj.length){
 * if(currentPage > 1){ descriptionObj.hide(); } else { descriptionObj.show(); } } };
 */

Myntra.Search.Utils.gaTrackingForAjaxFilters = function(property, name){
	if(!trackingBooleans[property]){
		// Myntra.GA.gaTrackEventAjax("search_page_filters",name,"");
		_gaq.push(['_trackEvent', 'search_page_filters', name]);
		Myntra.Search.Utils.updateTrackingBooleans(property);		
	}
};

Myntra.Search.Utils.setAllTrackingBooleansToFalse = function(){
	for (var property in trackingBooleans) {
		trackingBooleans[property] = false;
	}
};

Myntra.Search.Utils.updateTrackingBooleans = function(property){
	if(!trackingBooleans[property]){
		trackingBooleans[property]=true;
	}	
};

Myntra.Search.Utils.triggerManualInfiniteScroll = function(){
	Myntra.Search.InfiniteScroll.triggerManualInfiniteScroll();
	return false;
}

Myntra.Search.Utils.ajaxCall = (function(){
	var obj = {};
	
	obj.callList = new Object;
	
	obj.getData = function(type, url, querystr, callback)
	{
		if( this.callList[type] != null )
		{
			this.callList[type].abort();
			this.callList[type] = null;
		}
		if (Myntra.Search.Data.optimEnabled) {
			this.callList[type] = $.ajax(url, {
				type 		: 'POST',
				data		: JSON.stringify([querystr]),
				contentType	: 'application/json',
				dataType	: 'json',
				success		: callback
			});
		} else {
			this.callList[type] = $.get(url, {"query" : querystr}, callback);
		}
	}
	
	return obj;
})();


Myntra.Search.Utils.priceRange = function(minVal, maxVal){
	if(configArray.pricerange.rangeMin != minVal || configArray.pricerange.rangeMax != maxVal ){
		configArray.pricerange.rangeMin=minVal;
		configArray.pricerange.rangeMax=maxVal;
		//Myntra.Search.Utils.setPageToFirst();
		Myntra.Search.Utils.setFilterOptionsInUrlHash(["pricerange"]);
	}
};

Myntra.Search.Utils.setSortBy = function(sortby, el){
	$(".mk-product-sort a").removeClass("mk-sort-selected");
	$(el).addClass("mk-sort-selected");
	if(sortby == "PRICE"){
		sortby=(configArray["sortby"] == "PRICEA")?"PRICED":"PRICEA";
	}
	configArray["sortby"]=sortby;
	//Myntra.Search.Utils.setPageToFirst();
	Myntra.Search.Utils.setFilterOptionsInUrlHash(["sortby", "page"]);
	Myntra.GA.gaTrackPageViewAjax($(el).attr("rel"));
	_gaq.push(['_trackEvent', 'search_page', 'set_sort_by',sortby]);
	return false;
};

Myntra.Search.Utils.setPage = function(page, el){
	configArray.page=page;
	Myntra.Search.Utils.setFilterOptionsInUrlHash(["page"]);
	Myntra.GA.gaTrackPageViewAjax($(el).attr("rel"));
	return false;
};

Myntra.Search.Utils.setPageToFirst = function(){
	Myntra.Search.InfiniteScroll.pageSetToFirst=true;
	configArray.page=0;
	if (Myntra.Search.Data.optimEnabled) {
		Myntra.Search.Data.initJSON.displayedProductsCount = 0;
		Myntra.Search.Data.initJSON.productsQueue = [];
		Myntra.Search.Data.initJSON.search.start = 0;
		if (Myntra.Search.Data.initJSON.search.curated_query) Myntra.Search.Data.initJSON.search.curated_query.start = 0;
	}
};

Myntra.Search.Utils.setPagePrev = function(el){
	var page_num=parseInt(configArray.page)-1;
	if(page_num >= 0 && page_num < total_pages){ 
		Myntra.Search.Utils.setPage(parseInt(configArray.page)-1, el);
	}
	return false;
};

Myntra.Search.Utils.setPageNext = function(el){
	var page_num=parseInt(configArray.page)+1;
	if(page_num >= 0 && page_num < total_pages){
		Myntra.Search.Utils.setPage(parseInt(configArray.page)+1, el);
	}	
	return false;
};

Myntra.Search.Utils.scrollToTop = function(target_top){
	Myntra.Search.FilterScroller.scrollInProgress=true;
	$('html, body').animate({scrollTop:target_top}, 800, function(){
		Myntra.Search.FilterScroller.scrollInProgress=false;
	});
}

Myntra.Search.Utils.removeValueFromArray = function(brand, array){
	var i = array.indexOf(brand);
	if(i != -1) array.splice(i, 1);
	return array;
}

Myntra.Search.Utils.TriggerLazyLoad = function(){
	$(".jquery-lazyload").lazyload({ 
	    effect : "fadeIn",
	    placeholder : cdn_base+"/skin2/images/loader_180x240.gif"
	});
	$(".jquery-lazyload").removeClass("jquery-lazyload");
};

Myntra.Search.Utils.attachPDPRedirectCalls = function() {
	$("#mk-search-results").on('mousedown', '.mk-product', function(e) {
		if(!$(e.target).hasClass("quick-look") && !$(e.target).hasClass("quick-look-icon") 
				&& (!$(e.target).closest('.mk-colour').length || $(e.target).closest('.mk-colour .rs-carousel-item').length) // false for .mk-colour unless target is colour option inside .mk-colour
				&& !$(e.target).closest('.colours-lbl').length) {
			if (e.button == 0 && !e.ctrlKey && !e.shiftKey) {
				filterClicked = true;
				onBeforeUnloadFired = true;
			  	Myntra.Search.Utils.setScrollOptionsInUrlHash(["scrollPage", "scrollPos"]);
			}
			var idx = $(".mk-product").index($(this)),
		  		position = idx+1,
		  		pdpUrl = $(e.target).closest("a").attr("href"),
		  		queryString = "searchQuery=" + configArray["url"] + "&serp=" + position+"&uq="+configArray["uq"]+'#!',
		  		hashIndex = pdpUrl.indexOf('#!');
			_gaq.push(['_trackEvent', 'pdp_click', Myntra.Data.pageName, 'search_results']);
			if(hashIndex === -1){
			  	if(pdpUrl.indexOf("?") != -1){
					$(e.target).closest("a").attr('href', pdpUrl + "&" + queryString);
				}
				else{
					$(e.target).closest("a").attr('href', pdpUrl + "?" + queryString);
				}
			}		  
			return false;
		}
	});
};

Myntra.Search.Utils.setScrollOptionsInUrlHash = function(filter_types){
	
	var current_hash_str=location.hash;
	var current_query_str="";
	var query_hash=new Array();
	var query_obj=new Object();
	if(current_hash_str != ""){
		current_query_str=(current_hash_str).substring((current_hash_str).indexOf("#!")+2, (current_hash_str).length);
		query_hash=current_query_str.replace(/\//g, "|").replace(/~\|/g,"~/").split("|");
	}
	
	if(filter_types.length==0){
		query_hash=[];
	}
	else{
		for(var filteridx=0;filteridx<filter_types.length;filteridx++){
	
			var filter_type=filter_types[filteridx];
			var newstr="";
			var arr_idx=0;
			if(scrollProperties[filter_type] !== undefined ){
				newstr=scrollProperties[filter_type].getURLParam();
				scrollProperties[filter_type].resetParams();
			}
			
			for(var idx=0;idx<query_hash.length;idx++){
				var keyvalsplit=query_hash[idx].split("=");
				if(keyvalsplit[0] == filter_type){
					query_hash.splice(idx,1);
					arr_idx=idx;
				}
			}
					
			if(newstr != ""){
				if(arr_idx==0){
					query_hash.push(newstr);
				}
				else{
					query_hash.splice(arr_idx, 0, newstr);
				}	
			}	
		}
	}
	var query_hash_str="";
	query_hash_str=query_hash.join("|");
	
	// if(filter_types!='page' && filter_types!='sortby,page' &&
	// filter_types!='noofitems,page'){$('#avail-container').hide();}
	
	window.location.hash= "#!" + query_hash_str;
}

// set config array in one place and construct url every time
// brands=Nike/gender=Men/sizes=XL/pricerange=100-500/sortby=DISCOUNT/noofitems=24/page=1
Myntra.Search.Utils.setFilterOptionsInUrlHash = function(filter_types,sizeCheck){
	if(sizeCheck){
		filter_types.push('sizes');	
	}
	var current_hash_str=location.hash;
	var current_query_str="";
	var query_hash=new Array();
	var query_obj=new Object();
	if(current_hash_str != ""){
		current_query_str=(current_hash_str).substring((current_hash_str).indexOf("#!")+2, (current_hash_str).length);
		query_hash=current_query_str.replace(/\//g, "|").replace(/~\|/g,'~/').split("|");
	}
	
	if(filter_types.length==0){
		query_hash=[];
	}
	else{
		for(var filteridx=0;filteridx<filter_types.length;filteridx++){
			
			var filter_type=filter_types[filteridx];
			var newstr="";
			var arr_idx=0;
			if(filterProperties[filter_type] !== undefined ){
				newstr=filterProperties[filter_type].getURLParam();
				filterProperties[filter_type].resetParams();
			}
			
			for(var idx=0;idx<query_hash.length;idx++){
				var keyvalsplit=query_hash[idx].split("=");
				if(keyvalsplit[0] == filter_type){
					query_hash.splice(idx,1);
					arr_idx=idx;
				}
			}
			if(newstr != ""){
				if(arr_idx==0){
					query_hash.push(newstr);
				}
				else{
					query_hash.splice(arr_idx, 0, newstr);
				}	
			}	
		}
	}
	
	var query_hash_str="";
	query_hash_str=query_hash.join("|");
	// if(filter_types!='page' && filter_types!='sortby,page' &&
	// filter_types!='noofitems,page'){$('#avail-container').hide();}
	window.location.hash= "#!" + query_hash_str;
}

//Checks and returns a true if atleast one filter option is selected. false is none of the options is selected
Myntra.Search.Utils.getNoOfFiltersSet = function(){
	var filterSelected=false;
	for(prop in configArray){
		if(typeof configArray[prop] == "object"){
			if(prop == "pricerange" && (configArray["pricerange"].rangeMin != priceRangeMin || configArray["pricerange"].rangeMax != priceRangeMax)){
				filterSelected=true;
				break;
			}
			else{
				if(configArray[prop].length >=1){
					filterSelected=true;
					break;
				}
			}
		}
	}
	return filterSelected;
}

/*Myntra.Search.Utils.selectNoOfItems = function(val, el){
    configArray.noofitems=val;
    Myntra.Search.Utils.setPageToFirst();
    Myntra.Search.Utils.setFilterOptionsInUrlHash(["noofitems", "page"]);
	Myntra.Search.Utils.gaTrackingForAjaxFilters("noofitems","no_of_items_filter");
	$(".noofitems-links").removeClass("selected");
	$(el).addClass("selected");
}
*/
if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function(obj, start) {
	     for (var i = (start || 0), j = this.length; i < j; i++) {
	         if (this[i] === obj) { return i; }
	     }
	     return -1;
	}
}
/*
Myntra.Search.Utils.toCurrencyString=function(num){
	if(!isNaN(num)){
		num=num.toString();
	    return num.replace(/(\d)(?=(\d{3})+\b)/g,',');
	}
	else{
		return num;
	}
}*/

Myntra.Search.Utils.toCurrencyString=function(num){
	var c=0,d='.',t=',';
	var n = num.toString();
	c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

Myntra.Search.FilterScroller = (function(){
	var obj = {};
	
	var prevScrollPos = 0,
		prevWinWidth = 0,
		onRight = $(window).width() > 1150 ? true : false,
		goToTopElem = $(".go-to-top-section"),
		goToTopRight = $('.go-to-top-btn.right');

	$(window).resize( function() {
		onRight = $(window).width() > 1150 ? true : false;		
	});
	
	obj.scrollInProgress=false;
	
	obj.showGoToTop = function(){
		if(!this.scrollInProgress){
			goToTopElem = $(".go-to-top-section");
			goToTopRight = $('.go-to-top-btn.right');
			if(onRight) {
				goToTopElem.hide();
				goToTopRight.css('top', ($(window).height() - 44)/2).show();
			} else {
				goToTopRight.hide();
				goToTopElem.css({
					"position":"fixed",
					"top":0,
					"left":$(".mk-left-nav").offset().left
				}).show();
			}
		}
	}
	
	obj.hideGoToTop = function(){
		goToTopRight.hide();
		goToTopElem.hide().removeAttr("style");
	}
	
	obj.endOfFilterScroll = function(){
		return $(".mk-search-contents").offset().top + $(".mk-search-contents").height() - 100;
	}
	
	obj.timerToDetectScroll = function(filterSectionOffset) {
		var currScrollPos = $(window).scrollTop(),
			currWinWidth = $(window).width();
		
		if (onRight) {
			filterSectionOffset = $(window).height()/2;
		}
		
		if(currScrollPos != prevScrollPos || currWinWidth != prevWinWidth) {
			if(currScrollPos > filterSectionOffset && (onRight || currScrollPos < this.endOfFilterScroll())) {
				this.showGoToTop();
			}
			else{
				this.hideGoToTop();
			}
			prevScrollPos = currScrollPos;
			prevWinWidth = currWinWidth;
		}
	}
	
	return obj;
})();

/* Sticky Filters Begins */
Myntra.Search.Utils.stickyFilters = function() {
	function position(elem, pos, topOffset) {
		topOffset = pos == 'fixed-bottom' ? 'auto' : topOffset || 0;
		bottomOffset = pos == 'fixed-bottom' ? 0 : 'auto';
		elem.removeClass('boundary-top boundary-bottom fixed-top fixed-bottom hung').addClass(pos).css({
			top		:	topOffset,
			bottom	:	bottomOffset
		});
	}
	
	// These variables don't require recalculation every time
	var container = $('.mk-category-page'),
		filters = $('.mk-filters-container .mk-left-nav').addClass('boundary-top'),
		results = $('.mk-results-container .mk-search-contents').addClass('boundary-top');
	
	// These variables initialized here but recalculated every time
	var scrollTop = $(window).scrollTop();

	this.recalc = function() {
		if ($(window).width() < 980) {
			position(filters.add(results), 'boundary-top');
			return;
		}
		
		var	winHeight = $(window).height(),
			scrollTopPrev = scrollTop,
			filtersYOffsetPrev = filtersYOffset,
			filtersYOffset = filters.offset().top,
			filtersHeight = filters.outerHeight(true),
			resultsHeight = results.outerHeight(true),
			shortFilters = filtersHeight <= winHeight,
			shortResults = resultsHeight <= winHeight,
			topBoundary = container.offset().top,
			bottomBoundary = topBoundary + container.height(),
			topFixedOffset = $('.mk-site-nav-wrapper.mk-sticky').outerHeight() || 0,
			filtersBottomPos = bottomBoundary - topBoundary - filtersHeight,
			resultsBottomPos = bottomBoundary - topBoundary - resultsHeight,
			filtersClasses = filters[0].className,
			resultsClasses = results[0].className;
		
		scrollTop = $(window).scrollTop();
		
		var scrollDirection = scrollTopPrev <= scrollTop ? 'down' : 'up', 
			filtersHungPos = filtersYOffset - topBoundary - (scrollTop - scrollTopPrev);
		
		/* For Short Search Results */
		if (shortResults && resultsHeight < filtersHeight) {
			position(filters, 'boundary-top');
			// Case I : From boundary-top to fixed-top as we scroll down
			if (Myntra.Utils.String.contains(resultsClasses, 'boundary-top') && scrollTop > topBoundary) {
				position(results, 'fixed-top', topFixedOffset);
			}
			// Case II : From fixed-top to boundary-bottom as we scroll down
			else if (Myntra.Utils.String.contains(resultsClasses, 'fixed-top', 'boundary-top') && scrollTop >= topBoundary + resultsBottomPos) {
				position(results, 'boundary-bottom', resultsBottomPos);
			}
			// Case III : Reverse of Case I, from fixed-top to boundary-top as we scroll up
			else if (Myntra.Utils.String.contains(resultsClasses, 'fixed-top', 'boundary-bottom') && scrollTop <= topBoundary) {
				position(results, 'boundary-top');
			}
			// Case IV : Reverse of Case II, from boundary-bottom to fixed-top as we scroll up
			else if (Myntra.Utils.String.contains(resultsClasses, 'boundary-bottom') && scrollTop < topBoundary + resultsBottomPos) {
				position(results, 'fixed-top', topFixedOffset);
			}
		}
		
		/* For Short Filters */
		else if (shortFilters) {
			position(results, 'boundary-top');
			// Case I : From boundary-top to fixed-top as we scroll down
			if (Myntra.Utils.String.contains(filtersClasses, 'boundary-top', 'hung', 'fixed-bottom') && scrollTop > topBoundary) {
				position(filters, 'fixed-top', topFixedOffset);
			}
			// Case II : From fixed-top to boundary-bottom as we scroll down
			else if (Myntra.Utils.String.contains(filtersClasses, 'fixed-top', 'boundary-top', 'hung', 'fixed-bottom') && scrollTop >= topBoundary + filtersBottomPos) {
				position(filters, 'boundary-bottom', filtersBottomPos);
			}
			// Case III : Reverse of Case I, from fixed-top to boundary-top as we scroll up
			else if (Myntra.Utils.String.contains(filtersClasses, 'fixed-top', 'boundary-bottom', 'hung', 'fixed-bottom') && scrollTop <= topBoundary) {
				position(filters, 'boundary-top');
			}
			// Case IV : Reverse of Case II, from boundary-bottom to fixed-top as we scroll up
			else if (Myntra.Utils.String.contains(filtersClasses, 'boundary-bottom', 'hung', 'fixed-bottom') && scrollTop < topBoundary + filtersBottomPos) {
				position(filters, 'fixed-top', topFixedOffset);
			}
			// Case V : Corrections in case of erronuous behaviour - load more items
			else if (Myntra.Utils.String.contains(filtersClasses, 'boundary-bottom') && topBoundary + filtersBottomPos > filtersYOffset + filtersHeight) {
				position(filters, 'fixed-top', topFixedOffset);
			}
		}
		
		/* For Long Filters */
		else {
			position(results, 'boundary-top');
			// Case I : From boundary-top to fixed-bottom as we scroll down
			if (Myntra.Utils.String.contains(filtersClasses, 'boundary-top') && scrollTop + winHeight > topBoundary + filtersHeight) {
				position(filters, 'fixed-bottom');
			}
			// Case II : From fixed-top to hung as we scroll down
			else if (Myntra.Utils.String.contains(filtersClasses, 'fixed-top') && scrollDirection == 'down') {
				if (scrollTop + winHeight < topBoundary + filtersHungPos + filtersHeight) {
					position(filters, 'hung', filtersHungPos);
				} else {
					position(filters, 'fixed-bottom');
				}
			}
			// Case III : Similar to Case I, from hung to fixed-bottom as we scroll down
			else if (Myntra.Utils.String.contains(filtersClasses, 'hung') && scrollTop + winHeight >= filtersYOffset + filtersHeight) {
				position(filters, 'fixed-bottom');
			}
			// Case IV : From fixed-bottom to boundary-bottom as we scroll down
			else if (Myntra.Utils.String.contains(filtersClasses, 'fixed-bottom', 'hung', 'boundary-top', 'fixed-top') && filtersYOffset + filtersHeight >= bottomBoundary) {
				position(filters, 'boundary-bottom', filtersBottomPos);
			}
			// Case V : Reverse of Case I, similar to Case IV, from fixed-top to boundary-top as we scroll up
			if (Myntra.Utils.String.contains(filtersClasses, 'fixed-top', 'hung', 'boundary-bottom', 'fixed-bottom') && scrollTop <= topBoundary) {
				position(filters, 'boundary-top');
			}
			// Case VI : Reverse of Case II, similar to Case III, from hung to fixed-top as we scroll up
			else if (Myntra.Utils.String.contains(filtersClasses, 'hung') && scrollTop <= filtersYOffset) {
				position(filters, 'fixed-top', topFixedOffset);
			}
			// Case VII : Reverse of Case III, similar to Case II, from fixed-bottom to hung as we scroll up
			else if (Myntra.Utils.String.contains(filtersClasses, 'fixed-bottom') && scrollDirection == 'up') {
				if (scrollTop > topBoundary + filtersHungPos) {
					position(filters, 'hung', filtersHungPos);
				} else {
					position(filters, 'fixed-top', topFixedOffset);
				}
			}
			// Case VIII : Reverse of Case IV, similar to Cases I & VI, from boundary-bottom to fixed-top as we scroll up
			else if (Myntra.Utils.String.contains(filtersClasses, 'boundary-bottom') && scrollTop < filtersYOffset) {
				position(filters, 'fixed-top', topFixedOffset);
			}
			// Case IX : Corrections in case of erronuous behaviour - expand filters
			if (Myntra.Utils.String.contains(filtersClasses, 'hung', 'fixed-top') && filtersYOffset + filtersHeight >= bottomBoundary) {
				position(filters, 'boundary-bottom', filtersBottomPos);
			}
			// Case X : Corrections in case of erronuous behaviour - load more items
			else if (Myntra.Utils.String.contains(filtersClasses, 'boundary-bottom') && topBoundary + filtersBottomPos > filtersYOffset + filtersHeight) {
				position(filters, 'fixed-bottom');
			}
		}
	}
	return this;
};
/* Sticky Filters Ends */

/* Pass second parameter only if Myntra.Search.Data.initJSON.curatedProductIds is stale */
Myntra.Search.Utils.filterOutCuratedProducts = function(products, curated_products) {
	if (!curated_products && !Myntra.Search.Data.initJSON.curatedProductIds && !Myntra.Search.Data.initJSON.curated_products) return products;
	Myntra.Search.Data.initJSON.curatedProductIds = curated_products // Are curated_products passed?
		? _.pluck(curated_products, 'styleid') // Update initJSON
		: Myntra.Search.Data.initJSON.curatedProductIds || _.pluck(Myntra.Search.Data.initJSON.curated_products, 'styleid'); // If curatedProductIds are not populated, populate them
	return _.reject(products, function(product) {
		return _.contains(Myntra.Search.Data.initJSON.curatedProductIds, product.styleid);
	});
}

/* Filter Preference Starts Here */
Myntra.Search.Utils.updateSavedSizeText = function(sizes){
	for(var j in sizes){
		if(parseFloat(sizes[j],10)<10&&parseFloat(sizes[j],10)%1==0){
			sizes[j] = '0'+ sizes[j];
		}
	}
	sizes.sort();
	$('.mk-product-sizes').find('.mk-savedSize-wrapper').html('');
	for(var i = 0; i < sizes.length; i++){
		if(parseFloat(sizes[i],10)<10&&parseFloat(sizes[i],10)%1==0){
			sizes[i] = sizes[i].substr(1);
		}		
		if(i==2){
			$('.mk-savedSize-wrapper').append('<span class="mk-saved-size">'+sizes[i]+'</span>...');
			break;	
		}
		else if(i<2){
			$('.mk-savedSize-wrapper').append('<span class="mk-saved-size">'+sizes[i]+'</span>, ');
		}
		else if(i==sizes.length-1){
			$('.mk-savedSize-wrapper').append('<span class="mk-saved-size">'+sizes[i]+'</span>');
		}
			
	}	
}

Myntra.Search.Utils.showSavedSizes = function(savedSizes){
	savedSizes = savedSizes.split(':');
	var sizesLength = savedSizes.length,
		singleCon = $('.mk-filter-rem').find('.second .single'),
		mulCon = $('.mk-filter-rem').find('.second .multiple');
	if(sizesLength>1){
		var tempStr;
		for(var i=0; i<sizesLength; i++){
			if(!i){
				tempStr='(';
			}
			if(i==sizesLength-1){
				tempStr+=savedSizes[i];
				tempStr+=')';	
			}
			else{
				tempStr+=savedSizes[i]+', ';
			}
		}
		mulCon.find('span').text(tempStr);
		mulCon.show();
		singleCon.hide();
	}
	else{
		singleCon.find('span').text(savedSizes[0]);
		mulCon.hide();
		singleCon.show();
	}
};

Myntra.Search.Utils.getPreferredSizes = function(){
	var sizeGroupValue = Myntra.Data.fpcSizeGroup;	
	if(sizeGroupValue){		
		var posSizeGroup,posSizes,hashValue;
		hashValue = window.location.hash.substr(2).split('|');
		sizeGroupValue = sizeGroupValue.split('|');
		for(var i in sizeGroupValue){
			if(Myntra.Utils.String.contains(sizeGroupValue[i],Myntra.Data.sizeGroup)){
				posSizeGroup = i;
				break;
			}
		}
		if(posSizeGroup&&sizeGroupValue[posSizeGroup].split('=')[1]){			
			for(var j in hashValue){
				if(Myntra.Utils.String.contains(hashValue[j],'sizes')){
					posSizes = j;
					break;
				}
			}
			sizeGroupValue[posSizeGroup] = sizeGroupValue[posSizeGroup].split('=')[1];
			if(posSizes){				
				hashValue[posSizes] = hashValue[posSizes].split('=')[1];
				var tempArrHash = hashValue[posSizes].split(':'),
					tempArrCookie = sizeGroupValue[posSizeGroup].split(':');
				for(var k in tempArrCookie){
					if(!Myntra.Utils.String.contains(tempArrHash,tempArrCookie[k])){
						tempArrHash.push(tempArrCookie[k]);
					}
					$('.mk-sizes-filters').find('[data-value]="'+tempArrCookie[k]+'"').not('.checked').removeClass('unchecked').addClass('checked');
				}
				//Myntra.Search.Utils.updateSavedSizeText(tempArrHash);				
				hashValue[posSizes] = 'sizes=' + tempArrHash.join(':');
				hashValue = '#!' + hashValue.join('|');
			}
			else{
				//Myntra.Search.Utils.updateSavedSizeText(sizeGroupValue[posSizeGroup].split(':'));				
				if(hashValue.length==1&&hashValue[0]==''){
					hashValue = '#!sizes=' + sizeGroupValue[posSizeGroup];
				}
				else{
					hashValue.push('sizes='+sizeGroupValue[posSizeGroup]);
					hashValue = '#!' + hashValue.join('|');					
				}				
			}
			Myntra.Search.Utils.showSavedSizes(sizeGroupValue[posSizeGroup]);
			window.location.hash = hashValue;
			$('.mk-filter-rem .first').hide();
			$('.mk-filter-rem .second').show();
			//$('.mk-product-sizes').show();
			_gaq.push(['_trackEvent','search_page_visit','1',Myntra.Data.sizeGroup]);
		}
		else{
			$('.mk-filter-rem').stop().show();			
			$('.mk-filter-rem .first').show();
			$('.mk-filter-rem .second').hide();
			//$('.mk-product-sizes').hide();
			_gaq.push(['_trackEvent','search_page_visit','0','']);
		}
	}
	else{
		$('.mk-filter-rem').stop().show();
		$('.mk-filter-rem .first').show();
		$('.mk-filter-rem .second').hide();
		//$('.mk-product-sizes').hide();
		_gaq.push(['_trackEvent','search_page_visit','0','']);
	}
};

Myntra.Search.Utils.validSizeGroup = function(){
	var flag, tempSizeGroup;
	$('#mk-articletypes').find('.mk-labelx_check.checked').each(function(i){
		if(!i){
			tempSizeGroup = $(this).attr('data-sizeGroup');
			flag = true;
		}
		else{
			if(tempSizeGroup!=$(this).attr('data-sizeGroup')){
				flag = false;
			}
		}
	});
	return flag;
};

Myntra.Search.Utils.appendSavedSizes = function(){
	var sizeGroupValue = Myntra.Data.fpcSizeGroup,
		hashValue = window.location.hash.substr(2);
	if(!Myntra.Utils.String.contains(hashValue,'sizes')){
		if(sizeGroupValue){
			var posSizeGroup;
			sizeGroupValue = sizeGroupValue.split('|');
			for(var i in sizeGroupValue){
				if(Myntra.Utils.String.contains(sizeGroupValue[i],Myntra.Data.sizeGroup)){
					posSizeGroup = i;
					break;
				}
			}
			if(posSizeGroup){
				sizeGroupValue[posSizeGroup] = sizeGroupValue[posSizeGroup].split('=')[1];
				if(sizeGroupValue[posSizeGroup]){
					configArray['sizes'] = sizeGroupValue[posSizeGroup].split(':');
					onPageLoad = false;
					configArray['timeStamp'] = new Date().getTime();
					Myntra.Search.Utils.showSavedSizes(sizeGroupValue[posSizeGroup]);
					Myntra.Data.appendedSavedSize = true;
					$('.mk-filter-rem .first').hide();
					$('.mk-filter-rem .second').show();
					_gaq.push(['_trackEvent','search_page_visit','1',Myntra.Data.sizeGroup]);
				}
				else{
					$('.mk-filter-rem').stop().show();
					$('.mk-filter-rem .first').show();
					$('.mk-filter-rem .second').hide();
					Myntra.Data.appendedSavedSize = false;
					_gaq.push(['_trackEvent','search_page_visit','0','']);	
				}					
			}
			else{
				$('.mk-filter-rem').stop().show();
				$('.mk-filter-rem .first').show();
				$('.mk-filter-rem .second').hide();
				Myntra.Data.appendedSavedSize = false;
				_gaq.push(['_trackEvent','search_page_visit','0','']);	
			}						
		}
	}	
};

Myntra.Search.Utils.checkForSize = function(sizeGroup,check){
	if(!Myntra.Search.Utils.validSizeGroup()){
		configArray['sizes']=[];
		Myntra.Data.sizeGroup = '';
		$('.mk-filter-rem').show();
		$('.mk-filter-rem .first').show();
		$('.mk-filter-rem .second').hide();
		//$('.mk-product-sizes').hide();
		Myntra.Data.appendedSavedSize = false;
		_gaq.push(['_trackEvent','search_page_visit','0','']);
	}
	else{
		if(sizeGroup&&check){
			Myntra.Data.sizeGroup = sizeGroup;
			Myntra.Search.Utils.appendSavedSizes();
		}
		else if(!sizeGroup&&!check){
			Myntra.Data.sizeGroup = $('#mk-articletypes').find('.mk-labelx_check.checked:first').attr('data-sizeGroup');
			if(Myntra.Data.sizeGroup){
				Myntra.Search.Utils.appendSavedSizes();	
			}
			else{
				configArray['sizes']=[];
				$('.mk-filter-rem').show();
				$('.mk-filter-rem .first').show();
				$('.mk-filter-rem .second').hide();
				//$('.mk-product-sizes').hide();
				Myntra.Data.appendedSavedSize = false;
				_gaq.push(['_trackEvent','search_page_visit','0','']);	
			}			
		}
		else if((sizeGroup&&!check)&&!Myntra.Search.Utils.validSizeGroup()){
			configArray['sizes']=[];
			Myntra.Data.sizeGroup = '';
			$('.mk-filter-rem').show();
			$('.mk-filter-rem .first').show();
			$('.mk-filter-rem .second').hide();
			//$('.mk-product-sizes').hide();
			Myntra.Data.appendedSavedSize = false;
			_gaq.push(['_trackEvent','search_page_visit','0','']);
		}
		else if((sizeGroup&&!check)&&Myntra.Search.Utils.validSizeGroup()){
			Myntra.Data.sizeGroup = $('#mk-articletypes').find('.mk-labelx_check.checked:first').attr('data-sizeGroup');
			if(Myntra.Data.sizeGroup){
				Myntra.Search.Utils.appendSavedSizes();	
			}
		}			
	}
};

/* Filter Preference Ends Here */


/*Check For Selected Lazy Loaded Filters Start Here*/
Myntra.Search.Utils.checkLazyLoadedFilters = function(obj){
	var dataKey = $(obj).find('.mk-labelx_check').attr('data-key'),
		selectedFilters = configArray[dataKey];	
	if(selectedFilters){
		for(i=0; i<selectedFilters.length; i++){
			$(obj).find('[data-value="'+selectedFilters[i]+'"]').not('.checked').addClass('checked').removeClass('unchecked');
		}
	}	
};
/*Check For Selected Lazy Loaded Filters End Here*/

$(document).ready(function(){
	_gaq.push(['_trackEvent','search_page_visit', window.location.toString(), null, null, true]);
	_gaq.push(['_trackEvent','search_mechanism', Myntra.Search.Data.Seo.searchType, window.location.toString(), Myntra.Search.Data.Seo.searchResultsCount, true]);
	
	// Fashion Story text
	var description = $('.mk-fashion-story .mk-detail');
	if(description.find('p').length > 1) {
		var firstHeight = description.find('p:first-child').width(550).css('margin-bottom', '20px').outerHeight(),
			fullHeight = description.height();
		description.height(firstHeight);
		$('.more-less').toggle(function(){
			description.animate({height:fullHeight + 'px'}, 250);
			$(this).text('Less');
			_gaq.push(['_trackEvent', 'fashionStory', 'read_more']);
		},
		function(){
			description.animate({height:firstHeight}, 250);
			$(this).text('Read More');
			_gaq.push(['_trackEvent', 'fashionStory', 'read_less']);
		});
	} else {$('.more-less').hide();}

	if (noResultsPage) {
		Myntra.Search.InitNoResultsPage();
		return;
	}
	
	Myntra.Search.InitInfiniteScroll();
	ajaxCallFunc="InfiniteScroll";
	if (Myntra.Data.showStickySize==='test') {
		Myntra.Search.Utils.getPreferredSizes();		
	} else if (Myntra.Data.showStickySize==='control') {
		$('.mk-filter-rem').hide();
	}
	Myntra.Search.Init();
	
	// Click Product to go to PDP
	Myntra.Search.Utils.attachPDPRedirectCalls();
	
	// Sticky Filters
	if (Myntra.Search && Myntra.Search.Data && Myntra.Search.Data.stickyFiltersEnabled) {
		Myntra.Search.Utils.stickyFilters = Myntra.Search.Utils.stickyFilters.recalc ? Myntra.Search.Utils.stickyFilters : Myntra.Search.Utils.stickyFilters(); 
		$(window).bind('scroll resize load', Myntra.Search.Utils.stickyFilters.recalc);
	}
	
	// Colour Options
	Myntra.Search.Utils.initColourOptions();
	
	//Style details
	Myntra.Utils.showAdminStyleDetails = function(){
		if(Myntra.Data.showAdminAdditionalStyleDetails == 'y') {
			$('.mk-product-style-details').each( function(){
				if($(this).attr('data-id')){
					var style_id= $(this).attr('data-id').replace('prod_0_style_','');
					var stylediv='<div class="style-details" style="display:none"></div>';
					$(this).removeClass("mk-product-style-details");
					$(this).prepend(stylediv);
					var styleDetailsElem=$(this).children('.style-details');
					if($(styleDetailsElem).length){
						$.get(http_loc+'/myntra/style_details.php',{format:'json',style_id:style_id}, function(data) {
							$(styleDetailsElem).html(data.innerhtml);
							$(styleDetailsElem).show();
						});
					}
				}
			});
		}
	};
	Myntra.Utils.showAdminStyleDetails();
	Myntra.Utils.SizesTooltip.InitTooltip();
});

/* Load Recently Viewed Widget */
if(Myntra.Data.SearchRecentWidgetEnabled){
	$(window).load(function () {
		setTimeout(Myntra.RecentWidget.init, 1000);
	});
}
