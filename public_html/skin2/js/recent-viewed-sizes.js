$(document).ready(function() {
	if (Myntra.Data.newLayout && Myntra.Data.pageName != 'cart') return;

	Myntra.Utils.loadRecentSizes = function(frm) {
	    if (frm.data('sizes-loaded')) { return; }
	    var sizeNode = frm.parent().find('.inline-select'),
	    	sizeAddBlock =frm.parent(),
	    	styleid = frm.find("input[name='productStyleId']").val(),
	        selectedSize = sizeNode.val(),
	        sizes = Myntra.RecentWidget.Data.sizes[styleid],
	        markup = [];
	    if (!sizes) { return; }
	    var count = 0;
	    for (key in sizes) {
	    	count++;
	    }
	
	    var key, val, sel, dis;
	    for (key in sizes) {
	
	        dis = '';
	        sel = key == selectedSize ? ' selected="true"' : '';
	        val = sizes[key][0];
	        if(count==1){
	        	frm.find("input[name='productSKUID']").val(key);
	        }
	
	        if (!sizes[key][1]) {
	            continue; // don't show the unavailable sizes
	            dis = ' disabled="disabled"';
	            val += ' - SOLD OUT';
	        }
	
	        markup.push(['<option value="', key, '"', sel, dis, '>', val, '</option>'].join(''));
	    };
	
	    if(markup.length==0) {
	    	 // markup.push(['<option value="0">SOLD OUT</option>'].join(''));
	    	 sizeNode.hide();
	    	 sizeAddBlock.find('.inline-add').hide();
	    	 sizeAddBlock.append('<button class="btn small-btn primary-btn disabled-btn">SOLD OUT</button>');
	    } else {
	    	 sizeNode.append(markup.join("\n")); //.removeClass('jqTransformHidden').jqTransSelect();
	    	 //sizeAddBlock.find('.inline-add').attr('disabled', 'true').addClass('disabled-btn');
	    }
	    //$('.inline-add-block .jqTransformSelectWrapper div span').addClass('primary-btn btn');
	    $('.inline-add-block').removeClass('mk-hide');
	    frm.data('sizes-loaded', true);
	};
	
	/*$('.mk-inline-size-selector').live('mouseenter',function(){
		$(this).removeClass('mk-inline-size-selector');
	});*/

	/*SIZE SELECT*/
	var sizeInlineSelected = function(sizeDropCont){
		//alert(sizeDropCont.find('.inline-select').html());
		return ((sizeDropCont.find('.inline-select').val()!=0) || !(sizeDropCont.find('.inline-select').length));

	};

    /*
	$('.inline-select').live('change',function(){
		//$(this).closest('.recent-inline').find('.inline-add').trigger('click');
        if ($(this).val() != '0') {
            $(this).closest('.recent-inline').find('.inline-add').removeAttr('disabled').removeClass('disabled-btn');
        }
        else {
            $(this).closest('.recent-inline').find('.inline-add').attr('disabled', 'true').addClass('disabled-btn');
        }
	});
	$('.inline-add-block-size .jqTransformSelectWrapper span').live('mouseenter',function(e){
		var from = e.relatedTarget || e.fromElement;
		if ( !$(from).closest('.jq-drop-down-list').length ) {
			$(this).trigger('click');
		}
	});

	$('.inline-add-block-size .jqTransformSelectWrapper span').live('mouseleave',function(e){
		var to = e.relatedTarget || e.toElement;
		if ( !$(to).closest('.jq-drop-down-list').length ) {
			$('.jq-drop-down-list').hide();
		}
	});

	$('.jq-drop-down-list[data-dd-name=inline-select]').live('mouseleave',function(e){
		var to = e.relatedTarget || e.toElement;
		if ( !$(to).closest('.inline-add-block-size .jqTransformSelectWrapper span').length ) {
			$('.jq-drop-down-list').hide();
		}
	});
    */

	$('.inline-add').live('click',function(){
	    //fire add to cart
	    var sizeDropCont=$(this).closest('.recent-inline');
		var formToSubmit = $(this).parent().find('.inline-add-to-cart-form');
		if(sizeInlineSelected(sizeDropCont)){
			var quantity = formToSubmit.find("input[name='quantity']").val();
			var sku_='';
			var size='';
			if(sizeDropCont.find('.inline-select').length){
				size= sizeDropCont.find('.inline-select option:selected').text();
				sku_=sizeDropCont.find('.inline-select').val();

			}
			else {
				size='Freesize';
			}
			var productStyleId=formToSubmit.find("input[name='productStyleId']").val();
			var articleType= formToSubmit.find("input[name='articleType']").val();
			var pd= {'id':productStyleId,'at':articleType};

			formToSubmit.find("input[name='sizequantity[]']").val(quantity);
			if(sizeDropCont.find('.inline-select').length){
				formToSubmit.find("input[name='productSKUID']").val(sku_);
			}
			formToSubmit.find("input[name='selectedsize']").val(size);
			//AJAX ADD TO CART
			/*commenting to redirect user to cart */
			/*
			var rURL=http_loc+"/mkretrievedataforcart.php?pagetype=productdetail";
			$.ajax({
				type:"POST",
				url:rURL,
				data:formToSubmit.serialize(),
				beforeSend:function(){
					 if(window.location.pathname!='/mkmycart.php' && window.location.pathname!='/mkmycartmultiple_re.php' && window.location.pathname!='/mkmycartmultiple.php')
						 {
						 	$(document).trigger('myntra.addedtocart.showloading');
						 }
				},
				success: function(data){
					if(window.location.pathname!='/mkmycart.php'){
						$(document).trigger('myntra.addedtocart.show',data);
					//	alert('success'+articleType);
						$(document).trigger('myntra.addedtocart.showrecommendation',pd);
						if(Myntra.Data.addToCartStatus){
							Myntra.Header.setCount('bag',Myntra.Header.getCount('bag')+1);
						}
						//focust to selected a size to buy
						sizeDropCont.find('.inline-select').val(0).trigger('click');
						sizeDropCont.find('.jqTransformSelectWrapper span').text('SELECT A SIZE TO BUY');

					}
					else {
						window.location=document.URL;
					}
				},
				complete:function(){
					$(document).trigger('myntra.addedtocart.hide');
				}

	        });
	        */


			var pageName = Myntra.Data.pageName ? Myntra.Data.pageName : formToSubmit.find("input[name='pageName']").val();
			var widgetType = pageName == 'cart' ? 'avail_widget' : 'recently_viewed';
			//_gaq.push(['_trackEvent', widgetType, pageName, sku_]);
			formToSubmit.submit();
			//alert(sku_);
			//Create a new lightbox here for order confirmation;
		}
	});
});
