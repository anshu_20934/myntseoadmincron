/**
 * 
 */

(function(){
	BOOMR = BOOMR || {};
	BOOMR.plugins = BOOMR.plugins || {};
	
	var impl = {
		complete: false,
		
		//Get the latitude and the longitude;
		successFunction : function(position) {
		    var lat = position.coords.latitude;
		    var lng = position.coords.longitude;
		    BOOMR.addVar("latitude", lat);
		    BOOMR.addVar("longitude", lng);
		    
		    impl.complete = true;
			BOOMR.sendBeacon();
		},

		errorFunction : function(){
		    console.log("Could not get location");
		    impl.complete = true;
			BOOMR.sendBeacon();
		},
		
		getCookie : function(c_name){
			var i,x,y,ARRcookies=document.cookie.split(";");
			for (i=0;i<ARRcookies.length;i++){
				x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
				y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
				x=x.replace(/^\s+|\s+$/g,"");
				if (x==c_name){
					return unescape(y);
				}
			}
			return "";
		},

		start: function() {
			//Session Id
			BOOMR.addVar("s_id", Myntra.Data.spyId + "-" + impl.getCookie("microsessid"));
			BOOMR.addVar("c_remember", impl.getCookie("xidC_remember") || "");
			
			// Get Geo-Location, page_type, timestamp
			
			var url = window.document.URL.replace(/^[^\/]*(?:\/[^\/]*){2}/, "");
			
			//Guess the page
			var pageType;
			if(url.match("[A-Z,a-z,-]*/[A-Z,a-z,-]*/[A-Z,a-z,-]*/[0-9]*/buy") != undefined || url.match("^/[0-9][0-9]*$") != undefined)
				pageType = "pdp";
			else if(url == "" || url.match("/$") != undefined 
					|| url.match("/index\.php|html") != undefined 
					|| url.match("/home\.php") != undefined){
				pageType = "home";
			}else if(url.match("/mkmycart\.php") != undefined){
				pageType = "cart";
			}else if(url.match("/mymyntra\.php") != undefined){
				pageType = "mymyntra";
			}else if(url.match("/mkpaymentoptions\.php") != undefined){
				pageType = "payment";
			}else if(url.match("/confirmation\.php") != undefined){
				pageType = "confirmation";
			}else{
				pageType = "search";
			}
			BOOMR.addVar("env", Myntra.Data.configMode);
			BOOMR.addVar("page_type", pageType);
			
			//Add a time-stamp
			BOOMR.addVar("time", (new Date()).getTime());

			impl.complete = true;
			BOOMR.sendBeacon();
			
			return;
			
			/*
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(impl.successFunction, impl.errorFunction);
			}else{
				impl.complete = true;
				BOOMR.sendBeacon();
			}
			*/
		}
	};
	
	BOOMR.plugins.UI = {
		init: function(config) {
			BOOMR.utils.pluginConfig(impl, config, "UI", []);

			BOOMR.subscribe("page_ready", impl.start, null, this);

			return this;
		},

		is_complete: function() {
			return impl.complete;
		}
	};

}());
