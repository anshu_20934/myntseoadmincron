
(function() { "use strict";

Myntra.VtrBag = Myntra.Class.extend({
    init: function(room) {
        this.data = {};
        this.tips = {};
        this.totalAmount = 0;
        this.totalMrp = 0;
        this.totalItems = 0;
        this.root = room.find('.stage .tried-items');
        this.icon= this.root.find('.icon');
        this.count = this.root.find('.count');
        this.hd = this.root.find('.hd');
        this.bd = this.root.find('.bd');
        this.ft = this.root.find('.ft');
        this.hd.on('click', function(e) {
            if (this.icon.hasClass('ico-expand')) {
                this.mouseX = e.pageX;
                this.mouseY = e.pageY;
                this.suspendTips();
                this.bd.slideDown(function() { this.resumeTips(); }.bind(this));
                this.icon.removeClass('ico-expand').addClass('ico-collapse');
            }
            else {
                this.bd.slideUp();
                this.icon.removeClass('ico-collapse').addClass('ico-expand');
            }
        }.bind(this));
        this.bd.slideUp();
        this.icon.removeClass('ico-collapse').addClass('ico-expand');
        this.bd.on('click', 'li', function(e) {
            var el = $(e.currentTarget),
            target = $(e.target),
            styleId = el.data('styleid');

            if (target.hasClass('delete')) {
                this.onRemove(styleId);
            }
            else {
                // show mini pip
            }
        }.bind(this));
    },
    add: function(styleId, fn) {
        this.ajax(styleId, function() {
            this.bd.slideDown();
            this.icon.removeClass('ico-expand').addClass('ico-collapse');
            (typeof fn === 'function') && fn();
        }.bind(this));
    },
    suspendTips: function() {
        for (var styleId in this.tips) {
            this.tips[styleId].suspend();
        }
    },
    resumeTips: function() {
        for (var styleId in this.tips) {
            this.tips[styleId].resume();
        }
        this.bd.find('> li').each(function(i, el) {
            var el = $(el),
            pt = el.offset(),
            xmin = pt.left,
            xmax = xmin + el.outerWidth(),
            ymin = pt.top,
            ymax = ymin + el.outerHeight();
            if (xmin < this.mouseX && this.mouseX < xmax && ymin < this.mouseY && this.mouseY < ymax) {
                var styleId = el.data('styleid'),
                tip = this.tips[styleId];
                tip.show(null, el);
                return true;
            }
        }.bind(this));
    },
    renderItem: function(data) {
        //console.log('[VtrBag.renderItem] data:', data);
        var formatNum = Myntra.Utils.formatNumber,
        markup = [
            '<li data-styleid="', data.styleId, '">',
            '   <span class="delete"></span>',
            '   <div class="title">', data.articleType, '</div>',
            '   <div class="price">Rs. ', formatNum(data.price), '</div>',
            '</li>'
        ].join('');

        var item = $(markup).appendTo(this.bd);
        this.data[data.styleId] = data;
        this.totalAmount += +data.price;
        this.totalItems += 1;
        this.count.html(this.totalItems + ' Items');
        this.root.removeClass('invisible');

        if (data.mrp) {
            item.find('.price').append(' <span class="strike">' + formatNum(data.mrp) + '</span>');
            this.totalMrp += +data.mrp;
        }
        else {
            this.totalMrp += +data.price;
        }

        if (this.totalMrp !== this.totalAmount) {
            this.ft.html('Rs. ' + formatNum(this.totalAmount) + ' <span class="strike">' + formatNum(this.totalMrp) + '</span>');
        }
        else {
            this.ft.html('Rs. ' + formatNum(this.totalAmount));
        }

        var tipNode = $([
            '<div class="myntra-tooltip vtr-tried-item-info">',
            '   <div class="hd">', data.brand, ' ', data.title, '</div>',
            '   <div class="bd">',
            '       <div class="sizes">', this.getAvailableSkus(data.styleId).join(', '), '</div>',
            '   </div>',
            '</div>'
        ].join('')).appendTo('body');
        this.tips[data.styleId] = new Myntra.Tooltip(tipNode, item, {side:'right'});

    },
    remove: function(styleId) {
        var data = this.data[styleId],
        formatNum = Myntra.Utils.formatNumber;
        if (!data) { return; }

        this.bd.find('li[data-styleid=' + data.styleId + ']').remove();
        this.totalAmount -= +data.price;
        this.totalItems -= 1;
        this.count.html(this.totalItems + ' Items');
        
        if (data.mrp) {
            this.totalMrp -= +data.mrp;
        }
        else {
            this.totalMrp -= +data.price;
        }
        
        if (this.totalMrp !== this.totalAmount) {
            this.ft.html('Rs. ' + formatNum(this.totalAmount) + ' <span class="strike">' + formatNum(this.totalMrp) + '</span>');
        }
        else {
            this.ft.html('Rs. ' + formatNum(this.totalAmount));
        }
        
        if (this.totalItems <= 0) {
            this.root.addClass('invisible');
        }
        
        delete this.data[styleId];
        this.tips[styleId].hide();
        delete this.tips[styleId];
    },
    clear: function() {
        this.bd.html('');
        this.ft.html('');
        this.count.html('');
        this.totalItems = 0;
        this.totalAmount = 0;
        this.totalMrp = 0;
        this.data = {};
        this.tips = {};
        this.root.addClass('invisible');
    },
    // this can be overridden in the instances
    onRemove: function(styleId) {
        this.remove(styleId);
    },
    ajax: function(styleIds, fn) {
        var csvIds = $.isArray(styleIds) ? styleIds.join(',') : styleIds;
        $.ajax({
            url: '/myntra/ajax_getsizes.php?ids=' + csvIds + '&output=all',
            dataType: "json",
            context: this,
            success: function(res, status, xhr) {
                if (!res) {
                    throw new Error('[VtrBag.ajax] No response from the Server!');
                }
                if (res.status === 'SUCCESS') {
                    for (var styleId in res.data) {
                        this.renderItem(res.data[styleId]);
                    }
                    (typeof fn === 'function') && fn();
                }
            },
            error: Myntra.Utils.ajaxError('VtrBag')
        });
    },
    getAvailableSkus: function(styleId) {
        var skusAll = this.data[styleId].skus, skus = [], sku;
        for (var i = 0, n = skusAll.length; i < n; i += 1) {
            sku = skusAll[i];
            if (!sku[2]) { continue }
            skus.push(sku[1]);
        }
        return skus;
    },
    getData: function(styleId) {
        return styleId ? this.data[styleId] : this.data;
    }
});

Myntra.VtrRoom = Myntra.Class.extend({
    init: function(id, params, data) {
        //console.log('[VtrRoom.init]', 'params:', params, 'data:', data);
        if (data.errors.length) {
            alert(data.errors.join("\n"));
        }
        this.root = $(id);
        this.params = params;
        this.data = data;
        this.canvases = [];
        this.queue = [];
        this.model = this.root.find('.model');
        this.loadingSpinner = this.root.find('.loading-spinner');
        this.actions = this.root.find('.nav > .actions').on('click', 'li', this.doAction.bind(this));

        this.bg = this.root.find('.bg');
        if (data.bgUrl) {
            this.bg.css('background-image', 'url(' + data.bgUrl + ')');
        }
        
        this.buyBtns = this.root.find('.buy-btns').on('click', 'button', function(e) {
            $(e.target).hasClass('buy') ? this.buyItems() : this.wishItems();
        }.bind(this));
        
        this.shareBtns = this.root.find('.share-btns').on('click', '.share-btn', function(e) {
            e.preventDefault();
            var to = $(e.target).hasClass('facebook') ? 'facebook' : 'twitter';
            this.shareItems(to);
        }.bind(this));

        this.bag = new Myntra.VtrBag(this.root);
        this.bag.onRemove = function(styleId) {
            this.removeItem(styleId);
        }.bind(this);
        this.styleIds = data.styleIds;
        if (this.styleIds.length) {
            this.bag.add(this.styleIds, function() {
                this.buyBtns.show();
                this.shareBtns.show();
            }.bind(this));
        }
        
        this.views = this.root.find('.views').on('click', 'span', function(e) {
            var el = $(e.currentTarget), id = el.data('id');
            myntrack('vtr', 'changeview-' + id, 'click');
            this.ajax('changeview', {id:id});
        }.bind(this));
        $.each(this.data.views, function(i, view) {
            this.views.append('<span data-id="' + view.id + '">View ' + view.name + '</span>');
        }.bind(this));
        this.views.find('span').show().end().find('span[data-id="' + this.data.viewId + '"]').hide();

        this.initSavePane();
        this.initCustomizePane();
        this.renderLayers(data.layers);
        
        // track the FB initialized state
        this.FBInited = false;
        $(document).on('myntra.fb.init.done', function() {
            this.FBInited = true;
        }.bind(this));
    },
    initSavePane: function() {
        this.lookTitle = this.root.find('.look-title');
        this.lookName = this.lookTitle.find('.name');
        this.saveas = this.actions.find('.saveas');
        this.savePane = this.root.find('.vtr-spane').appendTo('body');
        this.saveMsg = this.savePane.find('.success-msg').hide();
        this.saveErr = this.savePane.find('.error-msg').hide();
        this.saveAction = '';
        this.loadingSpinner.add(this.savePane.find('.loading-spinner'));

        this.saveText = this.savePane.find('input[name="lookname"]').on('keypress', function(e) {
            e.which === 13 && !!$.trim(e.target.value) && this.saveBtn.click();
        }.bind(this));
        
        if (this.data.name) {
            this.lookName.text(this.data.name);
            this.lookTitle.show();
            //this.saveas.show();
            //this.saveText.val(this.data.name);
        }
        else {
            this.lookName.text('');
            this.lookTitle.hide();
            //this.saveas.hide();
        }

        this.saveClose = this.savePane.find('.close').on('click', function(e) {
            this.savePane.hide();
            this.saveBtn.show();
            this.saveErr.hide();
            this.saveMsg.hide();
            this.saveText.removeClass('err');
            this.actions.find('.save, .saveas').removeClass('selected');
            if (this.saveTmr) {
                clearTimeout(this.saveTmr); 
                this.saveTmr = null;
            }
        }.bind(this));

        this.saveBtn = this.savePane.find('.save').on('click', function() {
            var title = $.trim(this.saveText.val());
            if (!title) {
                this.saveText.addClass('err').focus();
                this.saveErr.show();
                return false;
            }
            this.save(title);
        }.bind(this));
        
        this.onSave = function(res, xhr) {
            this.saveBtn.hide();
            this.saveErr.hide();
            this.saveMsg.show();
            //this.saveTmr = setTimeout(function() { this.saveClose.click(); }.bind(this), 1000);
        }.bind(this);
    },
    initCustomizePane: function() {
        var list, count;
        this.custPane = this.root.find('.vtr-cpane').appendTo('body');

        list = []; count = 0;
        $.each(this.data.faces, function(i, face) {
            list.push('<li data-id="' + face.id + '"><img src="' + face.thumb + '"></li>');
            count += 1;
        });
        this.custPane.find('.faces .items')
            .html('<ol data-type="face">' + list.join('') + '</ol>')
            .find('ol').css('width', count * 90 - 10);

        list = []; count = 0;
        $.each(this.data.backgrounds, function(i, background) {
            list.push('<li data-id="' + background.id + '"><img src="' + background.thumb + '"></li>');
            count += 1;
        });
        this.custPane.find('.backgrounds .items')
            .html('<ol data-type="background">' + list.join('') + '</ol>')
            .find('ol').css('width', count * 90 - 10);

        this.custClose = this.custPane.find('.close').on('click', function(e) {
            this.custPane.hide();
            this.actions.find('.customize').removeClass('selected');
        }.bind(this));

        this.custPane.find('.items').on('click', 'li', function(e) {
            var el = $(e.currentTarget), id = el.data('id'), type = el.parent().data('type');
            this.ajax('change' + type, {id:id});
            myntrack('vtr', 'change' + type + '-' + id, 'click');
            this.custClose.click();
        }.bind(this));
    },
    getQueryParams: function() {
        return this.params;
    },
    createEmptyCanvas: function() {
        var canvas = document.createElement('canvas');
        canvas.width = 315;
        canvas.height = 580;
        return canvas;
    },
    createCanvas: function() {
        var canvas = this.createEmptyCanvas();
        canvas.style.zIndex = this.canvases.length;
        this.model.append(canvas);
        this.canvases.push(canvas);
        return canvas;
    },
    renderLayers: function(layers) {
        var i = 0, urls = [];

        this.loadingSpinner.show();
        $.each(layers, function(i, layer) {
            urls.push(layer[0]);
            urls.push(layer[1]);
        });

        //var timeStart = +new Date();
        Myntra.Utils.loadImages(urls, function(images) {
            //var timeEnd = +new Date(), timeTook = timeEnd - timeStart;
            //console.log('Time took to load ' + urls.length + ' image urls: ' + timeTook + 'ms');
            var i, j, k, n, canvas, ctx, tmp, cop, canvases = this.canvases;
            
            /*
            // flatten layers
            canvas = this.model.find('canvas');
            if (!canvas.length) {
                canvas = document.createElement('canvas');
                canvas.width = 315;
                canvas.height = 580;
                canvas.style.zIndex = 2;
                this.model.append(canvas);
            }
            else {
                canvas = canvas[0];
            }

            ctx = canvas.getContext('2d');
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            for (i = 0, k = 0, n = images.length; i < n; i += 2, k += 1) {
                if (images[i].hasError) {
                    console.log('Error! Image failed to load. src: ' + images[i].src);
                    continue;
                }
                ctx.drawImage(images[i], 0, 0);

                // if the layer type is not style, don't apply mask
                //console.log('layer:', k, 'type:', layers[k][2]); 
                if (layers[k][2] !== 'style') {
                    console.log('skipped mask layer:', k, 'type:', layers[k][2]); 
                    continue;
                }

                // apply the mask from the above layer, if any
                j = i + 3;
                if (images[j] instanceof Image) {
                    tmp = $(canvas).clone().get(0);
                    tmp.getContext('2d').drawImage(canvas, 0, 0);
                    ctx.clearRect(0, 0, canvas.width, canvas.height);
                    cop = ctx.globalCompositeOperation;
                    ctx.drawImage(images[j], 0, 0);
                    ctx.drawImage(tmp, 0, 0);
                    ctx.globalCompositeOperation = 'xor';
                    ctx.drawImage(images[j], 0, 0);
                    ctx.globalCompositeOperation = cop;
                    tmp = null;
                }
                //console.log('draw: ' + i + ' ' + images[i].src.split('/').pop());
            }
            */

            // create separate canvas per layer and draw the image
            for (k = 0, n = layers.length; k < n; k += 1) {
                i = k * 2; // index of the style image
                canvas = canvases[k] || this.createCanvas();
                canvas.className = '';
                ctx = canvas.getContext('2d');
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                if (images[i].hasError) {
                    console.log('Error! Image failed to load. layer: ' + k + ' src: ' + images[i].src);
                    continue;
                }
                ctx.drawImage(images[i], 0, 0);
                //console.log('layer: ' + k + ' draw:' + images[i].src);
            }

            // hide the rest of the canvases from the pool
            for (n = canvases.length; k < n; k += 1) {
                canvases[k].className = 'hide';
            }

            // apply the masks
            for (k = 1, n = layers.length; k < n; k += 1) {
                i = k * 2 + 1; // index of mask image
                if (images[i].hasError) {
                    console.log('Error! Mask failed to load. layer: ' + k + ' src: ' + images[i].src);
                    continue;
                }
                if (!(images[i] instanceof Image)) {
                    //console.log('No mask for layer: ' + k);
                    continue;
                }
                // apply the mask to ALL the layers below
                for (j = k - 1; j >= 0; j -= 1) {
                    // if the layer type is not style, don't apply mask
                    if (layers[j][2] !== 'style') {
                        //console.log('skipped masking for layer:', j, 'type:', layers[j][2]); 
                        continue;
                    }
                    canvas = canvases[j];
                    ctx = canvas.getContext('2d');
                    tmp = this.createEmptyCanvas();
                    tmp.getContext('2d').drawImage(canvas, 0, 0);
                    ctx.clearRect(0, 0, canvas.width, canvas.height);
                    cop = ctx.globalCompositeOperation;
                    ctx.drawImage(images[i], 0, 0);
                    ctx.drawImage(tmp, 0, 0);
                    ctx.globalCompositeOperation = 'xor';
                    ctx.drawImage(images[i], 0, 0);
                    ctx.globalCompositeOperation = cop;
                    tmp = null;
                    //console.log('applied mask to layer:', j, 'from layer:', k); 
                }
            }
            this.loadingSpinner.hide();
        }.bind(this));
    },
    flattenLayers: function() {
        var canvas = document.getElementById('flattened-canvas'),
        canvases = this.canvases,
        ctx, n, i;

        if (!canvas) {
            canvas = document.createElement('canvas');
            canvas.width = 360;
            canvas.height = 480;
            canvas.id = 'flattened-canvas';
            canvas.style.position = 'absolute';
            canvas.style.top = '-1000px';
            canvas.style.left = '0';
            document.getElementsByTagName('body')[0].appendChild(canvas);
        }

        ctx = canvas.getContext('2d');
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        // bg dimension: 482 x 580 -> crop the left/right to get the 3:4 ratio -> 435 x 580
        if (this.data.bgUrl) {
            var bgImg = new Image();
            bgImg.src = this.data.bgUrl;
            // image, sx, sy, sw, sh, dx, dy, dw, dh
            ctx.drawImage(bgImg, 23, 0, 435, 580, 0, 0, 360, 480);
        }

        // image dimension: 315 x 580 -> scale to fit inside 360 x 480 keeping aspect ratio -> 260 x 480
        // so, render it horizontally centered
        for (i = 0, n = canvases.length; i < n; i += 1) {
            if (canvases[i].className === 'hide') { continue; }
            // canvas, sx, sy, sw, sh, dx, dy, dw, dh
            ctx.drawImage(canvases[i], 0, 0, 315, 580, 50, 0, 260, 480);
        }
    },
    addItem: function(styleId) {
        //console.log('room.addItem', styleId);
        /*
        if (this.isAjaxing) {
            this.queue.push(styleId);
            return;
        }
        */
        this.ajaxAddItem(styleId);
        myntrack('vtr', 'addstyle-' + styleId, 'click');
    },
    ajaxAddItem: function(styleId) {
        if (this.styleIds.indexOf(styleId) !== -1) {
            // style is already added. do nothing
            //alert('This product is already added to the work area');
            Myntra.VtrManager.msg('room', 'style-in-use', styleId);
            return;
        }
        this.ajax('addstyle', {id:styleId}, function(res) {
            // add to the bag, only if the response contains the styleid
            if (res.styleIds.indexOf(styleId) !== -1) {
                this.bag.add(styleId, function() {
                    this.buyBtns.show();
                    this.shareBtns.show();
                    Myntra.VtrManager.msg('room', 'style-added', styleId);
                }.bind(this));
            }
            
            // remove the styles from the bag that are not there in the response.
            // for example, adding a style may remove N incompatible-styles
            $.each(this.styleIds, function(i, styleId) {
                if (res.styleIds.indexOf(styleId) === -1) {
                    this.bag.remove(styleId);
                    Myntra.VtrManager.msg('room', 'style-removed', styleId);
                }
            }.bind(this));

            // store the styleIds from the response to avoid double-add of same style
            this.styleIds = res.styleIds;
        }.bind(this));
    },
    removeItem: function(styleId) {
        //console.log('room.removeItem', styleId);
        var idx = this.styleIds.indexOf(styleId);
        if (idx === -1) { 
            // this should never happen
            return;
        }
        myntrack('vtr', 'removestyle-' + styleId, 'click');
        this.ajax('removestyle', {id:styleId}, function(res) {
            this.bag.remove(styleId);
            this.styleIds = res.styleIds;
            if (!this.styleIds.length) {
                this.buyBtns.hide();
                this.shareBtns.hide();
            }
            Myntra.VtrManager.msg('room', 'style-removed', styleId);
        }.bind(this));
    },
    doAction: function(e) {
        var el = $(e.currentTarget),
        action = el.data('action'),
        pt, x, y, items;

        myntrack('vtr', action, 'click');
        if (action === 'customize') {
            !this.savePane.is(':hidden') && this.saveClose.click();
            pt = el.offset();
            this.custPane.show();
            x = pt.left + el.outerWidth() / 2 - this.custPane.outerWidth() / 2,
            y = pt.top + el.outerHeight();
            this.custPane.css({left:x, top:y});
            el.addClass('selected');
            
            items = this.custPane.find('.faces .items');
            if (!items.data('jsp')) {
                items.jScrollPane();
            }
            
            items = this.custPane.find('.backgrounds .items');
            if (!items.data('jsp')) {
                items.jScrollPane();
            }
        }
        else if (action === 'save' || action === 'saveas') {
            Myntra.Utils.afterLogin(function() {
                // if it is already saved (ie., name won't be null), no need to show the pane
                if (action === 'save' && this.data.name) {
                    this.ajax('save', {title:this.data.name});
                }
                else {
                    !this.custPane.is(':hidden') && this.custClose.click();
                    this.saveAction = action;
                    pt = el.offset();
                    this.saveText.val('');
                    this.savePane.show();
                    x = pt.left + el.outerWidth() / 2 - this.savePane.outerWidth() / 2,
                    y = pt.top + el.outerHeight();
                    this.savePane.css({left:x, top:y});
                    el.addClass('selected');
                }
            }.bind(this))();
        }
        else {
            this.ajax(action);
        }
    },
    buyItems: function() {
        this.flattenLayers();
        var canvas = document.getElementById('flattened-canvas'),
        title = this.data.name || 'Buy the look',
        args = {_invoker:'vtrbuylook', canvas:canvas, title:title, styleIds:this.styleIds, data:this.bag.getData()};
        myntrack('vtr', 'buylook', 'click');
        Myntra.BuyLook.show(args);
    },
    save: Myntra.Utils.afterLogin(function(title) {
        this.ajax(this.saveAction, {title:title}, this.onSave);
    }),
    shareItems: function(to) {
        this.ajax('share', {title:''}, function(res, xhr) {
            //console.log('share URL: ' + res.url);
            var txt, uuid = res.url.split('/').pop();

            myntrack('vtr', 'share-' + to, uuid);
            if (to === 'twitter') {
                txt = "I just created a new look at @myntra's #stylestudio Check it out at";
                var targetUrl = 'http://twitter.com/share?url=' + encodeURIComponent(res.url) + '&text=' + encodeURIComponent(txt);
                window.open(targetUrl);
            }
            else if (to === 'facebook') {
                if (!this.FBInited) {
                    console.log('Error! FB is not yet intialized');
                    return;
                }
                FB.ui({
                    method: 'feed',
                    name: "My Style Studio",
                    link: res.url,
                    picture: '',
                    caption: '',
                    description: "I just created a new look at Myntra's Style Studio. It's simple and fun. Check it out.",
                    message: ''
                });
            }
        }.bind(this));
    },
    ajax: function(action, data, fn) {
        data = data || {};
        $.extend(data, {_action:action, _token:Myntra.Data.token});
        $.ajax({
            url: "/vtr/room.php",
            type: "POST",
            context: this,
            dataType: "json",
            data: data,
            beforeSend: function() {
                this.loadingSpinner.show();
                this.isAjaxing = true;
            },
            success: function(res, status, xhr) {
                this.loadingSpinner.hide();
                if (!res) {
                    throw new Error('[VtrRoom.ajax] No response from the Server!');
                }
                if (res.status === 'SUCCESS') {
                    (typeof data.id !== 'undefined') ? myntrack('vtr', action + '-' + data.id, 'success') : myntrack('vtr', action, 'success');

                    if (res.layers) {
                        this.renderLayers(res.layers);
                    }

                    if (action === 'changebackground' && res.bgUrl) {
                        this.data.bgUrl = res.bgUrl;
                        this.bg.css('background-image', 'url(' + res.bgUrl + ')');
                    }
                    else if (action === 'saveas') {
                        this.data.name = res.name;
                        this.lookName.text(this.data.name);
                        this.lookTitle.show();
                        Myntra.VtrManager.msg('room', 'look-saved');
                        //this.saveas.show();
                    }
                    else if (action === 'new') {
                        this.data.name = '';
                        this.lookName.text('');
                        this.lookTitle.hide();
                        //this.saveas.hide();
                        this.bag.clear();
                        this.buyBtns.hide();
                        this.shareBtns.hide();
                        Myntra.VtrManager.msg('room', 'styles-removed', this.styleIds);
                        this.styleIds = [];
                        this.views.find('span').show().end().find('span[data-id="' + res.viewId + '"]').hide();
                    }
                    else if (action === 'changeview') {
                        this.views.find('span').show().end().find('span[data-id="' + res.viewId + '"]').hide();
                    }
                    
                    if (typeof fn === 'function') {
                        fn(res, xhr);
                    }
                }
                else {
                    (typeof data.id !== 'undefined') ? myntrack('vtr', action + '-' + data.id, 'error') : myntrack('vtr', action, 'error');
                    alert(res.message);
                }
            },
            error: Myntra.Utils.ajaxError('VtrRoom'),
            complete: function(xhr, status) {
                if (status !== 'success') {
                    this.loadingSpinner.hide();
                }
                this.isAjaxing = false;
                /*
                if (action == 'addstyle' && this.queue.length > 0) {
                    var that = this, nextStyleId = this.queue.shift();
                    setTimeout(function() { that.ajaxAddItem(nextStyleId); }, 10);
                }
                */
            }
        });
    }
});

}()); // end of "use strict" wrapper

