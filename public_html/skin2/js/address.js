
$(document).ready(function() {
    var selectedAddress;

    // Address List
    $('.btn-continue').attr('disabled', 'true');
    $(document).bind('myntra.address.list.oninit', function(e, data) {
        if (data.count == 0) {
        	$('.main-content h2, .address-list-wrap, .select-address').hide();
        	$('.new-address-msg').show();
        } else {
            // Gopi: jqTransform is done by the address-list.js
            $('.address-list-wrap .list').jScrollPane();
        }
    });
    $(document).trigger('myntra.address.list.render', {
        _type:'shipping',
        container:'.address-list-wrap',
        isPopup:false
    });
    $(document).bind('myntra.address.list.select', function(e, data) {
        selectedAddress = data;
        $('.btn-continue').removeAttr('disabled');
        _gaq.push(['_trackEvent', 'address', 'selected',window.location.toString()]);
    });
    $('.btn-continue').click(function(e) {
    	_gaq.push(['_trackEvent', 'address', 'continue',window.location.toString()]);
        location.href = 'https://' + location.host + '/mkpaymentoptions.php?address=' + selectedAddress.id;
    });


    // Address Form
    $('.btn-save-continue').attr('disabled', 'true');
    $(document).bind('myntra.address.form.oninit', function(e, data) {
        $('.btn-save-continue').removeAttr('disabled');
    });
    $(document).bind('myntra.address.form.pinerr', function(e, data) {
        $('.btn-save-continue').html('Continue Anyway <span class="proceed-icon"></span>');
    });
    $(document).bind('myntra.address.form.resetpinerr', function(e, data) {
        $('.btn-save-continue').html('Save &amp; Continue <span class="proceed-icon"></span>');
    });
    $(document).trigger('myntra.address.form.render', {
        _type:'shipping',
        container:'.address-form-wrap',
        isPopup:false
    });
    $(document).bind('myntra.address.form.done', function(e, data) {
        location.href = 'https://' + location.host + '/mkpaymentoptions.php?address=' + data.data.id;
    });
    $('.btn-save-continue').click(function(e) {
        $(document).trigger('myntra.address.form.save');
        _gaq.push(['_trackEvent', 'address', 'save_continue',window.location.toString()]);
    });
});
