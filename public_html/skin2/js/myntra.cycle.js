/* Cycle Calls JS 
(myntra.cycle.js)
------------------------------------ */

$(window).load(function() {
	
	// Homepage Cycle Gallery ----------------------
	$('.mk-cycle-wrapper').cycle({
		timeout: 5000,
		pager: '.mk-cycle-nav',
		before: function(currSlideElement, nextSlideElement, options, forwardFlag){
			$(".multiContainerHide").removeClass("multiContainerHide");
		}
	});	

	// Category Feature Cycle ---------------
	$('.mk-banner-cycle').cycle({
		timeout: 9000,
		pager: '.mk-cycle-nav-banner'
	});

	// Also Bought Cycle --------------------
	$('.mk-also-cycle').cycle({
		timeout: 9000,
		pager: '.mk-also-cycle-nav'
	});

	// Shop Looks Cycle -------------------
	$('.mk-looks-cycling').cycle({
		timeout: 9000,
		pager: '.mk-cycle-nav'
	});

});
