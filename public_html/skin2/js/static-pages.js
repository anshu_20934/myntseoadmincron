$(document).ready( function() {
	var floatingContent = $(".mk-static-pages .mk-right-column.fixed"),
		win = $(window);
	
	win.scroll(toggleFloatingContent);
	
	function toggleFloatingContent() {
		var vScroll = win.scrollTop();
		if (vScroll > 420) {
			floatingContent.css({
				position: 'fixed',
				top: '100px'
			});
		} else {
			floatingContent.css({
				position: 'absolute',
				top: '50px'
			});
		}
		if (($(document).height() - vScroll - floatingContent.outerHeight(true)) < 1200) {
			floatingContent.fadeOut();
		}
		else {
			floatingContent.fadeIn();
		}
	}
	
	$(".mk-collapse-control").click( function() {
		$(this).siblings(".mk-collapse-content").slideToggle(toggleFloatingContent);
		return false;
	});
	
	$(".mk-jobs a").click(function () {
		$($(this).attr("href")).click();
	});
	
	$(".mk-sign-up-link").click(function(){
		if(!Myntra.Data.userEmail){
			$(document).trigger('myntra.login.show');
		}
	});
	
	$(".mk-refer-link").click(function(e){
		if(!Myntra.Data.userEmail){
			$(document).trigger('myntra.login.show');
			e.preventDefault();
			return false;
		}
		else{
			return true;
		}
	});
	
	if ($('#returns-hindi').length) {
		var hindiReturnsBox = Myntra.LightBox('#returns-hindi');
	
		$('#returns-hindi-link').click( function() {
			hindiReturnsBox.show();
		});
	}

	/*
	 * For Recently Viewed Items Page
	 */
	if ($('.mk-recently-viewed .mk-recent-viewed').length) {
		var loadURL=http_loc+'/getRecentItems.php?type='+Myntra.Data.pageName;
		$.ajax({
			type:"GET",
			url:loadURL + '&page=recentItems&lsrv='+(window.localStorage ? window.localStorage.getItem('lscache-recently-viewed'):''),
			dataType:"json",
			success: function(data){
				if(data.status==='SUCCESS'){
					//Load the html
					$('.mk-recent-viewed').html(data.content).fadeIn(200,function(){
						//loadRecentSizes();
					}).find('ul.mk-recent-viewed-ul').addClass('mk-cf');
				}
			}
		});
		
		function loadRecentSizes(){
        	Myntra.RecentWidget = Myntra.RecentWidget || {};
        	Myntra.RecentWidget.Data = Myntra.RecentWidget.Data || {};
			styleids=[];
			Myntra.RecentWidget.Data.sizes={};
			$('.inline-add-to-cart-form').find(":hidden[name='productStyleId']").each(function(i, el) {
				styleids.push(el.value);
			});
			$.ajax({
		        type: 'GET',
		        url: '/myntra/ajax_getsizes.php?ids=' + styleids.join(','),
		        dataType: 'json',
		        success: function(resp) {
		            //load in the select   
		        	Myntra.RecentWidget.Data.sizes = resp;
		        	$('.inline-add-to-cart-form').each(function (i, el) {
						if (Myntra.Data.newLayout) {
							Myntra.RecentWidget.loadSizes($(el));
						} else {
							Myntra.Utils.loadRecentSizes($(el));
						}
					});
					if (Myntra.Data.newLayout) {
						Myntra.RecentWidget.initSizeTooltip();
					}
		        }
		    });
		}
	}

	$(".accordion .accord-header").click(function() {
      if($(this).next("div").is(":visible")){
        $(this).next("div").slideUp("slow");
      } else {
        $(".accordion .accord-content").slideUp("slow");
        $(this).next("div").slideToggle("slow");
      }
    });
});
