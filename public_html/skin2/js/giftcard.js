Myntra.GiftCard = (function(){
	var obj={};
	obj.occasionJsonObj;
	var messageCharLimit = 100;
	var occasionMsgs = [];
	var occasionImgs = [];
	
	obj.initLandingPage = function(){
		this.initOccasionCarousel();
		this.initOccasionSelection();
		this.initPreviewLink();
		this.initOnPagePreview();
		this.initGiftCardForm();
		this.initGiftCardBuyNow();
		this.initGiftCardFormValidations();
	}
	
	obj.initOnPagePreview = function(){
		
		//Set these values once on load
		$(".mk-gc-preview-to-name").html($('.mk-recipient-name').val());
		$(".mk-gc-preview-message").html($('.mk-message').val());
		$(".mk-gc-preview-from-name").html($('.mk-sender-name').val());
		$(".mk-gc-preview-amount").html($('.mk-amount').val());

		$('.mk-recipient-name').live('keyup', function() {
			$(".mk-gc-preview-to-name").html($(this).val());
		});
		/*$('.mk-message').live('input', function() {
			$(".mk-gc-preview-message").html($(this).val());
		});*/
		$('.mk-sender-name').live('keyup', function() {
			$(".mk-gc-preview-from-name").html($(this).val());
		});
	}
	
	obj.initGiftCardFormValidations = function(){
		$(".mk-amount").bind("blur, change", function(){Myntra.GiftCard.validateAmount(".mk-amount",".mk-amount-error")});
		$(".mk-sender-name").blur(function(){Myntra.Utils.Validation.nameCheck(".mk-sender-name",".mk-sender-name-error",Myntra.Data.DefaultValues)});
		$(".mk-recipient-name").blur(function(){Myntra.Utils.Validation.nameCheck(".mk-recipient-name",".mk-recipient-name-error",Myntra.Data.DefaultValues)});
		$(".mk-recipient-email").blur(function(){Myntra.Utils.Validation.emailCheck(".mk-recipient-email",".mk-recipient-email-error")});
		//$(".mk-message").blur(Myntra.Utils.Validation.messageCheck);
	}
	
	obj.validateAmount = function(selector, errSelector){
		var el=$(selector);
		var errEl=$(errSelector);
		if(!el || !errEl){
			return false;
		}
		var amountVal = el.val();
	    if (!amountVal) {
	    	errEl.html("Please enter your gift card value").show();
	    	el.addClass("mk-err");
	    	return false;
	    }
	    if(!Myntra.Utils.Validation.isNumber(amountVal)){
	    	errEl.html("Please enter a valid number").show();
	    	el.addClass("mk-err");
	    	return false;
	    }
	    if(amountVal < Myntra.Data.GiftingLowerLimit || amountVal > Myntra.Data.GiftingUpperLimit) {
	    	errEl.html("Please enter a value between Rs. "+ Myntra.Data.GiftingLowerLimit +" and Rs. "+Myntra.Data.GiftingUpperLimit).show();
	    	el.addClass("mk-err");
	    	return false;
	    }
	    errEl.html("").hide();
	    el.removeClass("mk-err");
	    return true;
	}
	
	obj.initOccasionCarousel = function(){
		if($('.mk-occasions ul > li').size()>4){
	   		$('.mk-occasions').carousel({
	   			itemsPerPage: 4, // number of items to show on each page
				itemsPerTransition: 1, // number of items moved with each transition
				noOfRows: 1, // number of rows (see demo)
				nextPrevLinks: true, // whether next and prev links will be included
				pagination: false, // whether pagination links will be included
				speed: 'normal' // animation speed
	   		});
	   	}
	}
	
	obj.initOccasionSelection = function(){
		this.occasionJsonObj = this.convertOccasionJsonObj(Myntra.Data.OccasionJson);

		//populate the occasion messages array
		for(i=1;i<Myntra.Utils.AssocArraySize(Myntra.GiftCard.occasionJsonObj)+1;i++){
			occasionMsgs.push(Myntra.GiftCard.occasionJsonObj[i]["defaultMessage"]);
			occasionMsgs.push(Myntra.GiftCard.occasionJsonObj[i]["previewImage"]);
		}
		
		$(".mk-occasion-thumb").click(function(){
			$(".mk-occasion-thumb").removeClass("mk-occasion-selected");
			$(this).addClass("mk-occasion-selected");
			Myntra.GiftCard.changePropsByOccasion($(this).attr("data-id"));
		});
	}
	
	obj.changePropsByOccasion = function(idx){
		$(".mk-occasion-id").val(parseInt(this.occasionJsonObj[idx]["id"]));
		$(".mk-gc-preview-images").attr("src", cdn_base + "/" + this.occasionJsonObj[idx]["previewImage"]);
		var messageEl = $(".mk-message");
		
		if($.inArray(messageEl.val(), occasionMsgs) != -1){
			$(".mk-gc-preview-message").html(this.occasionJsonObj[idx]["defaultMessage"]);
			messageEl.val(this.occasionJsonObj[idx]["defaultMessage"]);
			messageEl.trigger("change");
		}
	}
	
	obj.convertOccasionJsonObj = function(jsonObj){
		var tempJsonObj=new Object();
		for(i=0;i<jsonObj.length;i++){
			tempJsonObj[jsonObj[i]["id"]] = jsonObj[i];
		}
		return tempJsonObj;
	}
	
	obj.validateGiftCardForm = function(){
		var validAmount = Myntra.GiftCard.validateAmount(".mk-amount",".mk-amount-error");
		var validSenderName = Myntra.Utils.Validation.nameCheck(".mk-sender-name",".mk-sender-name-error",Myntra.Data.DefaultValues);
		var validRecipientName = Myntra.Utils.Validation.nameCheck(".mk-recipient-name",".mk-recipient-name-error",Myntra.Data.DefaultValues);
		var validRecipientEmail = Myntra.Utils.Validation.emailCheck(".mk-recipient-email",".mk-recipient-email-error");
		return (validAmount && validSenderName && validRecipientName && validRecipientEmail);
	}
	
	obj.initGiftCardBuyNow = function(){
		var that = this;
		$(".buy-gift-card").click(function(e){
			e.preventDefault();
			if(!that.validateGiftCardForm()){
				return;
			}
			if (Myntra.Data.userEmail) {
				$(".mk-giftcards-form").submit();
	        }
	        else {
	            $(document).trigger('myntra.login.show', [{onLoginDone: function() {$(".mk-giftcards-form").submit();}}]);
	        }
		});
	}
	
	obj.initPreviewLink = function(){
		$('.mk-giftcard-preview-link').data('lb', Myntra.LightBox('#mk-giftcard-preview'));
		$('.mk-giftcard-preview-link').click(function() {
			$(this).data('lb').show();
			$(".mk-gc-preview-amount").html($('.mk-amount').val());
		});
	}
	
	obj.initGiftCardForm = function(){
		
		$('.mk-gc-switch-to-input').click(function(){
			$('.mk-giftcards-choose,.mk-giftcards-enter,.mk-gc-switch-to-select,.mk-gc-switch-to-input').toggle();			
		});
		
		$('.mk-gc-switch-to-select').click(function(){
			$('.mk-giftcards-choose,.mk-giftcards-enter,.mk-gc-switch-to-input,.mk-gc-switch-to-select').toggle();
			$('.mk-amount-select, .mk-amount').trigger("change");
		});
		
		$('.mk-amount-select').change(function(){
			$('.mk-amount').val($(this).val());
		});		
		
		$('textarea.mk-message').bind("keyup change", function() {
			var charLength = $(this).val().length;
			$('span.mk-message-char-count').html(messageCharLimit-charLength);

			if($(this).val().length > messageCharLimit){
				$('span.mk-message-char-count').html(0);
				$(this).val($(this).val().substring(0,messageCharLimit));
			}
			$(".mk-gc-preview-message").text($(this).val());
		});	
		
		$(".mk-placeholder-inputs").each(function(){
			$(this).focus(function(){
				if($(this).val() == "" || $(this).val() == $(this).attr("data-placeholder")){
					$(this).val("");
				}
				$(this).removeClass("mk-blur");
			}).blur(function(){
				if($(this).val() == ""){
					$(this).val($(this).attr("data-placeholder"));
					if($(this).hasClass("mk-sender-name")){
						$(".mk-gc-preview-from-name").html($(this).attr("data-placeholder"));
					}
					if($(this).hasClass("mk-recipient-name")){
						$(".mk-gc-preview-to-name").html($(this).attr("data-placeholder"));
					}
					//$(this).addClass("mk-blur");
				}
			});
		});
		
		$(".mk-message").focus(function(){
			if($(this).hasClass("mk-blur")){
				$(this).removeClass("mk-blur");
			}
		}).blur(function(){
			if($(this).val() == ""){
				$(".mk-message-clear").trigger("click");
			}	
		});
		
		$(".mk-message-clear").click(function(){
			var idx=parseInt($(".mk-occasion-id").val());
			var messageEl = $(".mk-message");
			$(".mk-gc-preview-message").html(Myntra.GiftCard.occasionJsonObj[idx]["defaultMessage"]);
			messageEl.val(Myntra.GiftCard.occasionJsonObj[idx]["defaultMessage"]);
			messageEl.trigger("change");
		});
	}
	
	obj.loadOccasionPreviewImages = function(){
		for(obj in this.occasionJsonObj){
			var img = new Image();
			img.src=cdn_base+"/"+this.occasionJsonObj[obj].previewImage;
		}
	}
	
	return obj;
})();

$(document).ready(function(){
	Myntra.GiftCard.initLandingPage();
});

$(window).load(function(){
	Myntra.GiftCard.loadOccasionPreviewImages();
});