/* Site top nav interactions */

/* Old layout mini nav */
$(function() {
	if (Myntra.Data.newLayout) return;

	var loadMiniNav = function(hovered) {
	   $.ajax({
	        type:		'GET',
	        url:		http_loc + '/ajaxNavigation.php',
	        dataType:	'html',
	        success:	function(data) {
	        				$('.mk-pdp-carousel-shop-by-text').append(data).find('a.mk-level1').hoverIntent(function(){
	        					$('.mk-level-2').hide();
	        					$('.mk-level-3').hide();
	        					$(this).siblings('.mk-level-2').show();
	        				}, function(){}).end().find('.mk-level-1').hoverIntent(function() {}, function() {
	        					$('.mk-level-2').hide();
		            		}).end().find('.mk-level-2').hoverIntent(function(){}, function(){
		        				$('.mk-level-2').hide();
		        			});
		        		}
		});
	};

	if($(".mk-pdp-carousel-shop-by-text").length){
		$(".mk-pdp-carousel-shop-by-text").hover(function() {
			if (!$('.mk-pdp-carousel-items', this).length) {
				loadMiniNav(true);
			} else {
				$(".mk-pdp-carousel-items").show();
			}
		}, function(){
			$(".mk-pdp-carousel-items").hide();
		});
	}

	$(window).load(function() {
		if($(".mk-pdp-carousel-shop-by-text").length){
			loadMiniNav();
		}
	});
});


/* top nav interactions */
$(function() {
    var timerId;    
    if($('.mk-site-nav-wrapper').length) {
    	var topNav = $('.mk-site-nav-wrapper'),
    		topNavPosition =  topNav.offset().top,
    		bag = $('.mk-utility .bag'),
    		sticky = topNav.add(bag).add('.mk-pdp-carousel-sub-items'),
        	showMenu = $('.mk-showMenu').length;
    }
	
    $('.mynt-layout-new .mk-level-2').hide();

	$('nav').on('mouseenter', 'a.mk-level1', function(){
		$('.mk-persist').removeClass("mk-active");
		
		if(!Myntra.Data.newLayout) {
			$(this).addClass("mk-hover");
    	} else {
			//when you hover over any of menu items attach hover
			$('a.mk-level1.mk-hover').removeClass("mk-hover");
        	$(this).addClass("mk-hover");  
		}
	}).on('mouseleave', 'a.mk-level1', function(){
		$(this).removeClass("mk-hover");
		$('.mk-persist').addClass("mk-active");
		if(Myntra.Data.newLayout) {
			$('.mk-persist').addClass('mk-hover');
		}
	}).find('a.mk-level1').hoverIntent(function(){
		if(!Myntra.Data.newLayout) {
			$('.mk-level-2').hide();
			$('.mk-level-3').hide();
			$(this).siblings('.mk-level-2').show();
		} else {
			//get the name of the menu item
	        var hoveredIndex = $(this).attr('data-index'),
	        	menuItem = $('.mk-level-2.index_' + hoveredIndex);
	       
	        //show the dropdown only if it contains some categories - for example, for sales and brands don't show any dropdown
	        if($(menuItem).length) {
	        	$('.mk-level-2').hide();
	    		$('.mk-level-3').hide();
				
		        //if timer is set clear it when any of the menu item is in focus
		        if(timerId) {
		            clearTimeout(timerId);
		            $(menuItem).show();
		        } else {
		        	$(menuItem).slideDown('fast');
		        }
			} else {
	        	$('.mk-level-2').slideUp('fast');
	        }
	   }
	}, function() {
		if(Myntra.Data.newLayout) {
			var hoveredMenuItem = this,
	        	hoveredIndex = $(this).attr('data-index'),
                menuItem = $('.mk-level-2.index_' + hoveredIndex);
                            
	        if($(menuItem).length) {
                if((showMenu && $(topNav).hasClass('mk-sticky')) || (!$(this).hasClass('mk-hover') && !$('.mk-level1').hasClass('mk-hover'))) {
               	                	
                    timerId = setTimeout(function() {
                        $(menuItem).slideUp({
	                        duration: 'fast', 
	                        easing: 'linear',
	                        complete: function () {
	                        			$(hoveredMenuItem).removeClass("mk-hover");
	                                    timerId = null;
	                                  }
                        })
                    }, 1000);
                } else {
                    setTimeout(function() {
                        var hoveredIndex = $('.mk-persist.mk-hover').attr('data-index');
                        if(hoveredIndex) {
                            var menuItem = $('.mk-level-2.index_' + hoveredIndex);
                            $('.mk-level-2').hide();
                            $(menuItem).show();                        
                        }
                    }, 400);
                }
	       }
		}
	});

	if (Myntra.Data.newLayout) {
		$("a.mk-level1.mk-showMenu").on('showDelayedDropDown', function (event, time) {
	        var hoveredIndex = $("a.mk-level1.mk-active.mk-showMenu").attr('data-index');
            if(hoveredIndex) {
                var menuItem = $('.mk-level-2.index_' + hoveredIndex);
                    
                $('.mk-level-2').hide();
                $('.mk-level-3').hide();
                
                if(!time)
                    time = 3000;
                //set the height of the wrappers and show a dropdownn after a delay
                timerId = setTimeout(function() {
                    if($('.mk-level-2').filter( function() { return $(this).css('display')=='block';}).length == 0)
                    $(menuItem).slideDown('slow'); 
                }, time);
            }
	    });
    
		//change the image on hover
	    $('.mk-site-nav').on('mouseenter', '.mk-level2-category-slideshow, .mk-level2-category-static', function () {
			$('a',this).eq(0).stop(true, true).fadeOut();
			$('a',this).eq(1).stop(true, true).fadeIn();
	    }).on('mouseleave', '.mk-level2-category-slideshow, .mk-level2-category-static', function () {
	    	$('a',this).eq(1).stop(true, true).fadeOut();
			$('a',this).eq(0).stop(true, true).fadeIn();
		});

		$(window).scroll(function() {		
			if (topNavPosition <= $(window).scrollTop()) {
                //slideup (slow) the categories in landing pages
                $('.mk-level-2').filter(function() {
                    return $(this).css('display')=='block';
                }).slideUp('slow');
                
                // onscroll in downward direction - stick the top nav
				sticky.addClass('mk-sticky');
				bag.css('margin-left', -bag.outerWidth());
			} else if (topNavPosition > $(window).scrollTop()) {              
                // onscroll in upward direction - make the top nav relative				
                
                //slidedown (fast) the categories in landing pages
                if($(topNav).hasClass('mk-sticky')) {
                   sticky.removeClass('mk-sticky');
				   bag.attr('style', '');
                   $('a.mk-level1.mk-showMenu').trigger('showDelayedDropDown',200);                   
                }
			}
		});
	} else {
	    $('.mk-level-1').hoverIntent(function() {}, function() {
			$('.mk-level-2').hide();
		});
	}
	
	$('nav').on('mouseenter', '.mk-level-2', function(){
		$('.mk-persist').removeClass("mk-active");
		$(this).siblings('a.mk-level1').addClass("mk-hover");
		
		if(Myntra.Data.newLayout) {
			$('.mk-persist').removeClass('mk-hover');
			var index = $(this).attr('data-index');
			$('a.mk-level1.index_' + index).addClass('mk-hover');
            if(timerId) {
				clearTimeout(timerId);
			}
		}
	}).on('mouseleave', '.mk-level-2', function(){
		if(!Myntra.Data.newLayout)
			$(this).siblings('a.mk-level1').removeClass("mk-hover");
		$('.mk-persist').addClass("mk-active");
		
		if(Myntra.Data.newLayout) {
			$('a.mk-level1').removeClass('mk-hover');
			$('.mk-persist').addClass('mk-hover');
            
            if(!showMenu || $(topNav).hasClass('mk-sticky')) {
                timerId = setTimeout(function() {
                $('.mk-level-2').slideUp({
                duration: 'fast', 
                easing: 'linear',
                complete: function () {
                            timerId = null;
                          }
                    });
                }, 1000);
            } else {
                var hoveredIndex = $('.mk-persist.mk-hover').attr('data-index');
                if(hoveredIndex) {
                    var menuItem = $('.mk-level-2.index_' + hoveredIndex);
                    $('.mk-level-2').hide();
                    $(menuItem).show();
                }
            }
		}
	}).find('.mk-level-2').hoverIntent(function(){}, function() {
		if(!Myntra.Data.newLayout) {
			$('.mk-level-2').hide();
		}
	});
});

Myntra.loadNavBanner = function() {
	if(Myntra.Data.newLayout) {
		if(!$('.mk-site-nav-wrapper').hasClass('mk-sticky')) //if the user has scrolled down then don't show the delayed dropdown
	    	$("a.mk-level1.mk-showMenu").trigger('showDelayedDropDown');
    }
	var banner = $('.nav-banner');
    if (!banner.length) { return }
    var img = new Image();
    img.onload = function() {
        banner.attr('src', this.src).removeAttr('lazy-src');
    };
    img.src = banner.attr('lazy-src');
    delete img;
};

Myntra.Utils.loadLevel2Categories = function () {
	if(Myntra.TopNav && Myntra.TopNav.level2) {
		$.each(Myntra.TopNav.level2, function(selector, value) {			
			$('.mk-level-2.' + selector + ' ul').append($(value));
		});
		Myntra.Utils.loadImageQueue();
	}
};

$(window).load(function() {
    Myntra.loadNavBanner();
    Myntra.Utils.loadLevel2Categories();
});
