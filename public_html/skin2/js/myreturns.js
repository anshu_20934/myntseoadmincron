$(document).ready(function() {

	if(!$(".mk-returns-page").length) return;

	initReturnsTooltip();
	
	$(".mk-return-shipping-details").each(function(){
		showCourierDetailsWindow($(this).attr("data-returnid"), $(this).attr("data-itemid"));
	});
	
	$('.courier-update-div .courier-service').live('change', function() {
		var courier = $(this).val(),
			context = $(this).closest('.courier-update-div');
		if (courier == "other") {
			$(".courier-service-custom-div", context).show();
		} else {
			$(".courier-service-custom-div", context).hide();
		}
	});
	
	var returnItemBox = Myntra.LightBox('#return-item.lightbox');
	$('.return-an-item .return').click( function() {
		if (!$(this).attr('disabled')) {
			returnItemBox.show();
		}
	});
	
});

function initReturnsTooltip(){
	$('.mk-return-number').hover( function(e) {
        var orderid = parseInt($(this).text());

        var tip;
        if (!$(this).data('tip')) {
            tip = $(this).next().appendTo('body');
            $(this).data('tip', tip);
        }
        tip = $(this).data('tip');
        tip.e = e;
        tip.show();
        var pt = $(this).offset(),
            x = pt.left - 110,
            y = Math.max(0, pt.top - tip.height() - 17);
        /*
        var x = Math.max(0, tip.e.pageX),
            y = Math.max(0, tip.e.pageY - tip.height() - 50);
        */
        tip.css({'left': x + 'px', 'top': y + 'px'});

  	},
  	function() {
    	$(this).data('tip').fadeOut('fast');
  	}
	);
	
	var boxes = {};
	$('.mk-returns-page .return-row').click(function(e) {
        var id = 'return-' + $('.mk-return-number', this).text();
        if (!boxes[id]) {
            boxes[id] = Myntra.LightBox('#' + id);
        };
        boxes[id].show();
    });
	
	$(".refund-details .ico").click(function(){
		var ico = $(this),
        pnode = ico.closest(".refund-details"),
        refunddetails = pnode.find(".refund-details-expand");
		finalprice = pnode.find(".final-price");

		if (ico.hasClass('ico-expand')) {
			refunddetails.show();
			ico.removeClass('ico-expand').addClass('ico-collapse');
			finalprice.addClass("final-price-border");
		}
		else if (ico.hasClass('ico-collapse')) {
			refunddetails.hide();
			ico.removeClass('ico-collapse').addClass('ico-expand');
			finalprice.removeClass("final-price-border");
		}
	});
	
}

//[myreturns.tpl]
function showCourierDetailsWindow(returnid, itemid) {
	var context = "#courier_update_div_" + returnid + "_" + itemid;
	if ($(context).css("display") == 'none') {
		var d = new Date();
		$.ajax({
			type: 'POST',
			url: 'mymyntra.php?view=myreturns',
			data: "itemid=" + itemid + "&returnid=" + returnid + "&mode=return-courierdetails-div&date=" + d.getTime(),
			success: function(response) {
				var context = "#courier_update_div_" + returnid + "_" + itemid; 
				$(context).html(response).show();
				closeCourierDetailsWindow(returnid, itemid);
			},
			error: function() {
				$("#courier_update_div_" + returnid + "_" + itemid).html("There seems to be a technical error. Please try again later.");
				$("#courier_update_div_" + returnid + "_" + itemid).show();
			}
		});
	} else {
		closeCourierDetailsWindow(returnid, itemid);
	}
	return true;
}

function closeCourierDetailsWindow(returnid, itemid) {
	var context = "#courier_update_div_" + returnid + "_" + itemid;
	if ($(context).hasClass('updated')) {
		$('input[type="text"], select', context).attr('disabled', 'disabled');
		$('.save-btn, .cancel-btn', context).hide();
		$('.edit-btn', context).show();
	} else {
		$('.cancel-btn', context).hide();
	}
}

function updateTrackingDetails(returnid, itemid) {
	var context = "#courier_update_div_" + returnid + "_" + itemid;
	var courier = $(".courier-service", context).val(),
		trackingNo = $(".tracking-no", context).val(),
		customCourier = false,
		courierErr = $('.courier-service-div .err', context),
		trackingNoErr = $('.tracking-no-div .err', context),
		customCourierErr = $('.courier-service-custom-div .err', context);

	if (courier == "other") {
		courier = $(".courier-service-custom", context).val();
		customCourier = true;
	}

	if (courier == "" && customCourier == false) {
		courierErr.text('Please choose a Courier Service');
		return;
	} else { courierErr.text(''); }

	if (courier == "" && customCourier == true) {
		customCourierErr.text('Please enter a Courier Service');
		return;
	} else { customCourierErr.text(''); }

	if (trackingNo == "") {
		trackingNoErr.text('Please enter a Tracking Number');
		return;
	} else { trackingNoErr.text(''); }

	$("#courierdetails-progress").show();
	var d = new Date();
	$.ajax({
		type: 'POST',
		url: 'mymyntra.php?view=myreturns',
		dataType: 'json',
		data: {
			'courier_service': courier,
			'tracking_no': trackingNo,
			'itemid': itemid,
			'returnid': returnid,
			'mode': 'return-update-courierdetails',
			'date': d.getTime(),
			'_token': Myntra.Data.token
		},
		success: function(response) {
			if (response[0] == 'SUCCESS') {
				result = "Thank you for sharing your return shipment details. We will let you know once we have received your return shipment.";
				$("#return_message_" + returnid).html(result);
				$(context).addClass('updated');
				closeCourierDetailsWindow(returnid, itemid)
				$(".courierdetails-successMsg", context).show();
				_gaq.push(['_trackEvent', 'myreturns', 'enter_tracking_details', 'click']);
			} else {
				$(".courierdetails-errorMsg", context).show();
			}
			$("#courierdetails-progress", context).hide();
		},
		error: function(xmlHttpRequest, textStatus, errorThrown) {
			$(".courierdetails-errorMsg", context).show();
			$("#courierdetails-progress", context).hide();
		}
	});
}

function editCourierDetails(returnid, itemid) {
	var context = "#courier_update_div_" + returnid + "_" + itemid;
	$('.edit-btn', context).hide();
	$('.save-btn, .cancel-btn', context).show();
	$('input[type="text"], select', context).removeAttr('disabled');
}