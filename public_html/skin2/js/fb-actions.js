$(window).load(function() {
    //WISHLIST ACTION
    ////Old code by Myntra. So we can remove it in case not used anywhere else.
    //	$(document).bind('myntra.fb.wishlist.post', function(e,data) {
    //	if(!data.exSocial && social_share_variant=='test'){
    //		FB.api(
    //			'/me/'+facebook_app_name+':wishlist',
    //			'post',
    //			{product:decodeURIComponent(data.href)},
    //			function(response) {
    //				if (!response || response.error) {
    //					return {status:"error",msg:response.error};
    //				} else {
    //					return {status:"success",id:response.id};
    //				}
    //			});
    //	}
    //	else {
    //		return {status:"error",msg:'product excluded from posting to fb'};
    //	}
    //		
    //	});

    //PURCHASE ACTION
    ////Old code by Myntra. So we can remove it in case not used anywhere else.
    //	$(document).bind('myntra.fb.purchase.post', function(e,data) {
    //		if(social_share_variant=='test'){
    //			for(var i=0;i<data.hrefs.length;i++){
    //				FB.api(
    //					'/me/'+facebook_app_name+':buy',
    //					'post',
    //					{product:http_loc+'/'+decodeURIComponent(data.hrefs[i])},
    //					function(response) {
    //						if (!response || response.error) {
    //							return {status:"error",msg:response.error};
    //						} else {
    //							return {status:"success",id:response.id};
    //						}
    //				});
    //			}
    //		}
    //	});

    $('body').delegate('.wishlist-fb-login span,.purchase-fb-login span', 'click', function() {
        var referer = $(this).attr('data-referer');
        Myntra.FBConnect(referer);
        $(this).parent().hide();

    });

    //PAGE LOAD FIRE
    $(document).bind('myntra.fb.init.done', function(e, data) {
        //CHECK IF THE SAVE COOKIE SET, THEN SAVE
        FB.getLoginStatus(function(response) {
            var FBStatus = '',
                FBId = '';
            if (response.status === 'connected') {
                FBStatus = 'authorized';
                FBId = response.userID;
            }

            //bind events
            switch (Myntra.Data.socialAction) {
                case "wishlist":
                    //	login with facebook fire below on page load
                    if (Myntra.Utils.Cookie.get('_sku')) {
                        Myntra.saveItemAsync({
                            sku: Myntra.Utils.Cookie.get('_sku'),
                            qty: 1,
                            url: Myntra.Utils.Cookie.get('_url'),
                            excludeSocial: Myntra.Utils.Cookie.get('_saveitem_ex_social'),
                            isMini: false
                        });
                    }
                    //Old code by Myntra. So we can remove it in case not used anywhere else.
                    //bind succesful fb login
                    //						$(document).bind('myntra.fblogin.auth', function(e, data) {
                    //							FB.getLoginStatus(function(response) {
                    //								if(response.status === 'connected') {
                    //									var isMiniPIP=false;
                    //						   	    	var socialShareUrl=window.location.href;
                    //						   	    	var exclude=false;
                    //						   	    	if(typeof Myntra.Data.MiniPIPsocialExclude != 'undefined'){
                    //						   	    		isMiniPIP=true;
                    //						   	    		socialShareUrl=Myntra.Data.MiniSocialShareUrl;
                    //						   	    		exclude=Myntra.Data.MiniPIPsocialExclude;
                    //						   	    	}
                    //						   	    	else{
                    //						   	    		exclude=Myntra.Data.PDPsocialExclude;
                    //						   	    	}
                    //						   	    	
                    //							    	$(document).trigger('myntra.fb.wishlist.post',{href:socialShareUrl,miniPIP:isMiniPIP,exSocial:exclude});
                    //								}
                    //								
                    //							});
                    //						});
                    break;
                case "cart-wishlist":
                    if (socialPost) {
                        // Kuliza Code start
                        if (kulizaEchoEnabled) {
                            Myntra.FB.showPopupIfRequired('cart_flow', 'false', 'false', 'false', 'false');
                        }
                        // See if below code needs to be removed
                        // Kuliza Code end
                        //Old code by Myntra. So we can remove it in case not used anywhere else.
                        //							if(FBStatus == 'authorized' && FBId != ''){
                        //								$(document).trigger('myntra.fb.wishlist.post',{href:http_loc+'/'+socialPostArray[0],exSocial:false});
                        //							}
                    }
                    break;
                case "purchase":
                    if (socialPost) {
                        // Kuliza Code start
                        if (kulizaEchoEnabled) {
                            Myntra.FB.showPopupIfRequired('checkout_flow', 'false', 'false', 'false', 'false');
                        }
                        // See if below code needs to be removed. See if showPopIfRequired  
                        // doesnot need to be called on under garments checkout. 
                        // Kuliza Code end

                        //Old code by Myntra. So we can remove it in case not used anywhere else.
                        //							if(FBStatus != 'authorized' || FBId == ''){
                        //								if(social_share_variant=='test'){
                        //									$('#share-fb-cont').append('<div class="purchase-fb-login"><em></em><span class="share-yes">SHARE YOUR PURCHASE WITH YOUR FRIENDS</span></div>')
                        //								}
                        //							}else{	
                        //								$(document).trigger('myntra.fb.purchase.post',{hrefs:socialPostArray});
                        //							}
                    }
                    //Old code by Myntra. So we can remove it in case not used anywhere else.
                    //						$(document).bind('myntra.fblogin.auth', function(e, data) {
                    //							FB.getLoginStatus(function(response) {
                    //								if (response.status === 'connected') {
                    //									$(document).trigger('myntra.fb.purchase.post',{hrefs:socialPostArray});
                    //								}
                    //							});
                    //						});

                    break;

            }
        });
    });

});



// Kuliza Code start

//Kuliza Defined Constants
Myntra.FB.og_wishlist = 'wishlist';
Myntra.FB.og_checkout = 'buy';
Myntra.FB.og_like = 'like';

Myntra.FB.echo_base_url = Myntra.Kuliza.echoBaseUrl;
Myntra.FB.post_action_url = Myntra.FB.echo_base_url + 'postAction';
Myntra.FB.get_popup_url = Myntra.FB.echo_base_url + 'getImplicitActionPopup';

Myntra.FB.client_domain = Myntra.Kuliza.clientDomain;
Myntra.FB.client_base_url = Myntra.Kuliza.clientBaseUrl;
Myntra.FB.fb_app_object_type = 'product';

//End of Kuliza Defined constants

Myntra.FB.uid;
Myntra.FB.access_token;


/**
 *  This function is single entry point to initiate echo flow. It can be called in following cases
 *  1) On page load
 *  2) On click of share it button
 *  3) On click of opt out button
 */

Myntra.FB.checkMiniPipFlow = function() {
    if (typeof Myntra.Data.MiniPIPsocialExclude === 'undefined') {
        return 'quicklook-no';
    } else {
        return 'quicklook-yes';
    }
}


Myntra.FB.showPopupIfRequired = function(flow_type, is_opt_out, is_from_opt_out_button, from_popup, from_cancel) {
    FB.getLoginStatus(function(response) {
        var added_class = Myntra.FB.checkMiniPipFlow();
        var dataArray = {
            'domain': Myntra.FB.client_domain,
            'fb_action': '',
            'flow_type': flow_type,
            'added_class': added_class,
            'is_opt_out': is_opt_out,
            'is_from_opt_out_button': is_from_opt_out_button

        };
        if (response.status === 'connected') {
            // the user is logged in and has authenticated your
            // app, and response.authResponse supplies
            // the user's ID, a valid access token, a signed
            // request, and the time the access token 
            // and signed request each expire

            Myntra.FB.uid = response.authResponse.userID;
            Myntra.FB.access_token = response.authResponse.accessToken;

            dataArray.fb_user_id = Myntra.FB.uid;
            dataArray.fb_action = Myntra.FB.getActionFromFlow(flow_type);

        } else if (response.status == 'not_authorized') {
            dataArray.not_connected = 'true';
            dataArray.not_authorized = 'true';

        } else {
            dataArray.not_connected = 'true';
            dataArray.not_authorized = 'false';
        }
        dataArray.from_cancel = from_cancel;
        Myntra.FB.getPermissionsPopup(dataArray, from_popup);
    });
}


/**
 * This function asks the user to login into facebook. Once he is authorized, we show him the
 * relevant popup as required.
 */

Myntra.FB.askForLogin = function(flow_type, not_authorized) {
    FB.login(function(response) {
        // handle the response
        if (response.authResponse) {
            Myntra.FB.showPopupIfRequired(flow_type, "first_time", "false", 'false', 'false');
            return false;
        } else {
            //console.log('User cancelled login or did not fully authorize.');
            return false;
        }
    }, {
        scope: 'user_likes,email,user_checkins,user_interests,publish_actions,user_birthday,user_hometown'
    });
}

/**
 * Get fb action based on flow
 */
Myntra.FB.getActionFromFlow = function(flow_type) {
    var fb_action = '';
    switch (flow_type) {
        case "pdp_flow":
            fb_action = Myntra.FB.og_wishlist;
            break;
        case "cart_flow":
            fb_action = Myntra.FB.og_wishlist;
            break;
        case "checkout_flow":
            fb_action = Myntra.FB.og_checkout;
            break;
        case "like_flow":
            fb_action = Myntra.FB.og_like;
            break;
        default:
    }
    return fb_action;
}


/**
 *  Opt out button click handler. call Showpopupifrequired() with
 *  isoptout = true and is_from_opt_out_button = true. We are using live here because
 *  optout button is added dynamically onto the page when post to fb call succeeds
 */
$('.optout_fb_share').die("click");
$(".optout_fb_share").live("click", function() {
    var flow_type = $(this).attr('flow_type');
    if (flow_type == 'pdp_flow') {
        _gaq.push(['_trackEvent', 'fbapp', 'pdp', 'cust_perm_opt_out']);
    } else if (flow_type == 'checkout_flow') {
        _gaq.push(['_trackEvent', 'fbapp', 'order_conf', 'cust_perm_opt_out']);
    }
    Myntra.FB.showPopupIfRequired(flow_type, 'stop_sharing', 'true', 'false', 'false');
    return false;
});

//        $('.kuliza-social-close-quicklook-yes').die("click");
//        $(".kuliza-social-close-quicklook-yes").live("click",function(){
//            $('.kuliza-social-wrapper-quicklook-yes').hide();
//            $('#minipip-product-guide .mk-save-for-later').show();
//        });
//        
//        $('.kuliza-social-close-quicklook-no').die("click");
//        $(".kuliza-social-close-quicklook-no").live("click",function(){
//            $('.kuliza-social-wrapper-quicklook-no').hide();
//            $('#mk-product-page .mk-save-for-later').show();
//        });

/**
 * Get permissions from db. Response can be of 3 types. 1) User has given permissions. In that case we
 * make call to post on fb. 2) Either he is coming for the first time or he has denied permissions. In that case
 * show a share it button. 3) Request went with optout=true (Optout will be true only
 * if request is going on click of optout button or request is going on click of share button).
 * In that case pop up html is returned and pop up is shown
 *
 */
Myntra.FB.getPermissionsPopup = function(dataArray, from_popup) {
    $.ajax({
        url: Myntra.FB.get_popup_url,
        type: 'GET',
        dataType: "jsonp",
        data: dataArray,
        success: function(response) {
            var added_class = Myntra.FB.checkMiniPipFlow();
            if (response.popup == 'true') {
                // Show pop up
                $('.echo-lightbox-body-' + added_class).html(response.html);
                if (typeof Myntra.Data.socialExcludeText === 'undefined') {
                    //Do Nothing
                } else {
                    $('.exclusion_text').html(Myntra.Data.socialExcludeText);
                }

                if ($('.echo-lightbox-button')) {
                    $('.echo-lightbox-button').addClass('echo-lightbox-button-' + added_class);
                }
                $('.echo-lightbox-button-' + added_class).data('lb').show();

            } else {
                if (response.post_allowed == 'true') {
                    Myntra.FB.postToFb(dataArray['flow_type'], from_popup);
                } else {
                    if (response.post_allowed == 'false' && response.html == 'false') {
                        //Do nothing
                    } else {
                        var extra_html = '';
                        if (dataArray['flow_type'] == 'pdp_flow') {
                            if (added_class == 'quicklook-no') {
                                //$('#mk-product-page .mk-save-for-later').hide();
                            } else {
                                //$('#minipip-product-guide .mk-save-for-later').hide();
                            }
                            //                                            $('.kuliza-social-wrapper-'+added_class).show();
                            //                                            extra_html = '<div class="saved_by_kuliza"><span></span>SAVED IN WISHLIST</div>';
                        }
                        $('.' + added_class).html(response.html);
                    }
                }
            }

        },
        error: function(xhr, textStatus, error) {}
    });
}

/**
 * Get light box id. Each page has a different light box id
 */
Myntra.FB.getLightBoxId = function(flow_type) {
    var light_box_id = "";
    switch (flow_type) {
        case "pdp_flow":
            light_box_id = "echo-pdp-lightbox";
            break;
        case "cart_flow":
            light_box_id = "echo-cart-lightbox";
            break;
        case "checkout_flow":
            light_box_id = "echo-checkout-lightbox";
            break;
        default:
    }
    return light_box_id;
}

/**
 * Posts data on facebook. Loops through array of objects and posts each object on facebook.
 * After ajax call we are not coming from like flow we show opt out button.
 */
Myntra.FB.postToFb = function(flow_type, from_popup) {
    var postonfb_data_array = Myntra.FB.getPostOnFbData(flow_type);
    /*
     *This variable is useful in case of multiple ajax calls. In that case we set UI only on success of first
     * call. On success of subsequent calls we avoid making UI changes.
     */
    var is_ui_set = false;
    for (var j = 0; j < postonfb_data_array.length; j++) {
        $.ajax({
            url: Myntra.FB.post_action_url,
            type: 'GET',
            dataType: "jsonp",
            data: postonfb_data_array[j],
            success: function(response) {
                if (flow_type != 'like_flow' && !is_ui_set) {
                    is_ui_set = true;
                    var light_box_id = Myntra.FB.getLightBoxId(flow_type);

                    var light_box = $('.' + light_box_id);

                    /* Remove few elements from light box div as these elements will be added again in light box
                             div when we initializa optout button again with light box div. Initializaing is done something 
                             like this $('.echo-lightbox-button').data('lb', Myntra.LightBox('#"+light_box_id+"'));
                             On writing this  lightbox.js is called where elements are added in lightbox div
                            */
                    var info_icon = light_box.find('.info-icon');
                    if (info_icon.length) {
                        info_icon.remove();
                    }
                    var close_button = light_box.find('.btn.small-btn.normal-btn.btn-ok');
                    if (close_button.length) {
                        close_button.remove();
                    }

                    var added_class = Myntra.FB.checkMiniPipFlow();
                    var text = '';
                    var extra_html = '';
                    if (from_popup == 'true') {
                        text = 'SHARED WITH YOUR FRIENDS.';
                    } else {
                        text = 'SHARED WITH YOUR FRIENDS.';
                    }

                    if (flow_type == 'pdp_flow') {
                        if (added_class == 'quicklook-no') {
                            //$('#mk-product-page .mk-save-for-later').hide();
                        } else {
                            //$('#minipip-product-guide .mk-save-for-later').hide();
                        }
                        $('.kuliza-social-wrapper-' + added_class).show();
                        if (from_popup == 'true') {
                            //extra_html = '<div class="saved_by_kuliza"><span></span>SAVED IN WISHLIST</div>';
                            text = 'SHARED WITH YOUR FRIENDS.';
                        } else {
                            text = 'SHARED WITH YOUR FRIENDS.';
                        }
                        $('.' + added_class).html(extra_html + "<span class='fb_shareit_icon'></span><span class='saved_and_shared'>" + text + "&nbsp;|&nbsp;</span>" +
                            "<a class='echo-lightbox-button-" + added_class + " saved_and_shared optout_fb_share' href='#' " +
                            "flow_type='" + flow_type + "' >" +
                            "STOP SHARING</a><span class='share_info_icon'></span>");
                    } else if (flow_type == 'checkout_flow') {
                        //Myntra loyalty flow for sharing of product
                        // Myntra.FB.loyaltyFlow('checkout_flow',postonfb_data_array,function(err){

                        // });

                        if (from_popup == 'true') {
                            text = 'THIS PURCHASE HAS BEEN SHARED WITH YOUR FRIENDS.';
                        } else {
                            text = 'THIS PURCHASE HAS BEEN SHARED WITH YOUR FRIENDS.';
                        }
                        $('.' + added_class).html("<div id='more-info' class='spdHappyness'>" +
                            "<h3 class='checkout_padding'>SPREAD THE HAPPINESS</h3>" +
                            "<span class='fb_shareit_icon'></span><span class='saved_and_shared'>" + text + "&nbsp;|&nbsp;</span>" +
                            "<a class='echo-lightbox-button-" + added_class + " saved_and_shared optout_fb_share' href='#' flow_type='" + flow_type + "' >" +
                            "STOP SHARING</a><span class='share_info_icon'></span></div>");

                    } else {
                        $('.' + added_class).each(function(index, value) {
                            var urlVars = Myntra.FB.getUrlVars(window.location.href);
                            if ($(this).attr('product_id') == urlVars.style) {
                                $(this).html("<span class='fb_shareit_icon'></span><span class='saved_and_shared'>" + text + "&nbsp;|&nbsp;</span>" +
                                    "<a class='echo-lightbox-button-" + added_class + " saved_and_shared optout_fb_share' href='#' " +
                                    "flow_type='" + flow_type + "' >" +
                                    "STOP SHARING</a><span class='share_info_icon'></span>");
                            }
                        });

                    }
                    $('.echo-lightbox-button-' + added_class).data('lb', Myntra.LightBox('.' + light_box_id + '-' + added_class));
                }

            },
            error: function(xhr, textStatus, error) {}
        });
    }
}

/**
 * Gets an array of objects. Each object contains posting info of each product
 */
Myntra.FB.getPostOnFbData = function(flow_type) {

    var fb_data_array = new Array();
    var i = 0;
    var current_url;
    var postonfb_data;
    switch (flow_type) {
        case "pdp_flow":
            current_url = window.location.href;
            postonfb_data = Myntra.FB.retrieveDataFromUrl(current_url, 3, flow_type);
            fb_data_array[i++] = postonfb_data;
            break;
        case "cart_flow":
            current_url = socialPostArray[0];
            postonfb_data = Myntra.FB.retrieveDataFromUrl(current_url, 0, flow_type);
            fb_data_array[i++] = postonfb_data;
            break;
        case "checkout_flow":
            for (var j = 0; j < socialPostArray.length; j++) {
                current_url = socialPostArray[j];
                postonfb_data = Myntra.FB.retrieveDataFromUrl(current_url, 0, flow_type);
                fb_data_array[i++] = postonfb_data;
            }
            break;
        case "like_flow":
            current_url = window.location.href;
            postonfb_data = Myntra.FB.retrieveDataFromUrl(current_url, 3, flow_type);
            fb_data_array[i++] = postonfb_data;
            break;
        default:
    }
    return fb_data_array;

}


/**
 * Retrieves all necessary parameters of a product from its url. Start index keeps a reference of
 * that at what index we have which parameter . As this function may be passed full url and in some cases
 * relative url also, so on splitting the url same parameter will be found at different indexes in an array.
 *
 */
Myntra.FB.retrieveDataFromUrl = function(url, start_index, flow_type) {
    var postonfb_data = {
        'category': '',
        'brand': '',
        'object_name': '',
        'object_id': ''

    };

    var current_url = url;
    var url_array = current_url.split('/');
    var url_array_length = url_array.length;

    if (url_array_length > start_index)
        postonfb_data.category = url_array[start_index];

    if (url_array_length > start_index + 1)
        postonfb_data.brand = url_array[start_index + 1];

    if (url_array_length > start_index + 2)
        postonfb_data.object_name = url_array[start_index + 2];

    if (url_array_length > start_index + 3)
        postonfb_data.object_id = url_array[start_index + 3];


    //change object url. Comment this line and uncomment line below this
    //postonfb_data.url = 'http://magento-test.shoppul.se/index.php/coalesce-functioning-on-impatience-t-shirt.html';

    postonfb_data.url = Myntra.FB.getObjectUrlFromUrl(flow_type, current_url);

    //change object image.Comment this line and uncomment line below this
    //postonfb_data.img = 'http://magento-test.shoppul.se/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/c/o/coalesce-functioning-on-impatience-t-shirt-1.jpg';
    if (flow_type == 'checkout_flow') {
        postonfb_data.img = Myntra.FB.getImageUrl(flow_type, postonfb_data.object_id);
    } else {
        postonfb_data.img = Myntra.FB.getImageUrl(flow_type, 'false');
    }

    //quicklook flow
    if (Myntra.FB.checkMiniPipFlow() == 'quicklook-yes') {
        postonfb_data.category = $('#minipip-product-guide h1>a').attr('href').split('/')[3];
        postonfb_data.brand = $('#minipip-product-guide input[name="brand"]').val();
        postonfb_data.object_name = $('#minipip-product-guide input[name="productStyleLabel"]').val();
        postonfb_data.object_id = $('#minipip-product-guide input[name="productStyleId"]').val();
        postonfb_data.img = $('#minipip-product-guide input[name="image"]').val();
        postonfb_data.url = $('#minipip-product-guide h1>a[title="More Details"]').attr('href');
    }

    postonfb_data.echo_token = Myntra.FB.getFbPostId(flow_type, url);

    postonfb_data.domain = Myntra.FB.client_domain;
    var fb_action = Myntra.FB.getActionFromFlow(flow_type);
    postonfb_data.og_action = fb_action;
    postonfb_data.object_type = Myntra.FB.fb_app_object_type;

    //change access token.Comment this line and uncomment line below this
    //postonfb_data.access_token = 'AAADmErEV9FcBACYNnAMrgYf7qzgUb57ZAZAaZBdxoMB7BWkk4Vs2724NnTiIblQ8sifJnI2v3uxRSaZCJYoaQMcGSeOXdPLUZBQ9ALOdHyNmZAgWJQSzk3';
    postonfb_data.access_token = Myntra.FB.access_token;

    postonfb_data.fb_id = Myntra.FB.uid;
    return postonfb_data;
}

/**
 * Get product url. For pdp page product url is same. In cart flow and checkout flow we get only relative urls
 * from a js array so we append base url to it to make our product url.
 * TODO get base url from some config parameter
 */
Myntra.FB.getObjectUrlFromUrl = function(flow_type, url) {
    var object_url = "";
    switch (flow_type) {
        case "pdp_flow":
            object_url = url;
            break;
        case "cart_flow":
            object_url = Myntra.FB.client_base_url + url;
            break;
        case "checkout_flow":
            object_url = Myntra.FB.client_base_url + url;
            break;
        case "like_flow":
            object_url = url;
            break;
        default:
            object_url = "";
    }
    return object_url;
}


/**
 * Get image url. For different flows image url of product is put in different variables.
 * TODO Get image url on check out page
 */
Myntra.FB.getImageUrl = function(flow_type, product_id) {
    var image_url = "";
    switch (flow_type) {
        case "pdp_flow":
            image_url = Myntra.Data.pdp_page_product_image_url;
            break;
        case "cart_flow":
            image_url = Myntra.Data.cart_page_product_image_url;
            break;
        case "checkout_flow":
            if (typeof socialPostStyleDetailsArray === 'undefined') {
                image_url = "";
            } else {
                for (var key in socialPostStyleDetailsArray) {
                    if (key == product_id) {
                        image_url = socialPostStyleDetailsArray[key].default_image_url;
                    }
                }
            }

            break;
        case "like_flow":
            image_url = Myntra.Data.pdp_page_product_image_url;
            break;
        default:
            image_url = "";
    }
    return image_url;
}

/**
 * Get echo token from url. Only in case of pdp page we try to find echo token in url as
 * the product url is same as pdp page url . In all other cases we
 * hard code it as 0
 */
Myntra.FB.getFbPostId = function(flow_type, url) {
    var fb_post_id = "0";
    if (flow_type == 'pdp_flow') {
        if (url.indexOf('fb_action_ids') != -1) {
            var url_vars = Myntra.FB.getUrlVars(url);
            fb_post_id = url_vars['fb_action_ids'];
        }
    }
    return fb_post_id;
}

/**
 * Gets key value pairs for url and puts it in array and returns it
 */
Myntra.FB.getUrlVars = function(url) {
    var vars = {};
    var parts = url.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
        vars[key] = value;
    });
    return vars;
}

Myntra.FB.trackVisit = function() {
    if (typeof pdp_visit_track !== 'undefined') {
        var visit_url = Myntra.FB.echo_base_url + 'visitTracker';
        var visited_id = Myntra.FB.getFbPostId('pdp_flow', window.location.href);
        if (visited_id == 0 || visited_id == '0') {
            return false;
        }
        var visit_data = {
            'echo_token': visited_id
        };
        $.ajax({
            url: visit_url,
            type: 'GET',
            dataType: "jsonp",
            data: visit_data,
            success: function(response) {
                //console.log('here');
            },
            error: function(xhr, textStatus, error) {

            }
        });
    }
}

Myntra.FB.trackVisit();


/**
 * Click handler of our fb like button on pdp page. If the user is logged in and has given permissions
 * for the app then it we update global variables of userid(uid) and access token and make post on fb
 * call sending flow type as like
 *
 */
$("#custom-fb-like-button").click(function() {
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            Myntra.FB.uid = response.authResponse.userID;
            Myntra.FB.access_token = response.authResponse.accessToken;
            Myntra.FB.postToFb('like_flow', 'false');
        } else if (response.status === 'not_authorized') {
            Myntra.FB.login();
        } else {
            Myntra.FB.login();
        }
    });

});

/**
 * Function makes user login into fb and takes permission for the fb app from user. If both login and permissions
 * are done successfully reponse.authResponse object is received
 */
Myntra.FB.login = function() {
    FB.login(
        function(response) {
            if (response.authResponse) {
                Myntra.FB.uid = response.authResponse.userID;
                Myntra.FB.access_token = response.authResponse.accessToken;
                Myntra.FB.postToFb('like_flow', 'false');
            } else {

            }
        }, {
            scope: 'publish_actions,user_birthday,user_interests,user_likes,user_location,user_relationships,email,user_checkins,user_hometown'
        }
    );
}

$('.share_info_icon').live({
    mouseenter: function() {
        $(this).html('<div class="social_flyout_arrow"></div>' + '<div class="social_flyout">' +
            'WE WILL <span class="social_red_text">NOT SHARE</span> INNERWEAR, SWIMWEAR, NIGHTWEAR & GIFT ORDERS' + '</div>');
    },
    mouseleave: function() {
        $(this).html('');
    }
});

// Kuliza Code end

/** 
 * Loyalty flow for FB action
 *
 * fb_action : like,unlike,
 * url: abs_url the user has liked or unliked.
 *
 * TODO : add csrf token
 * Check if the user is allowed to get points
 **/
Myntra.FB.loyaltyFlow = function(fb_flow, data, cb) {
    cb = cb ? cb : function() {};
    if (fb_flow === 'like_flow' || fb_flow === 'checkout_flow') {
        //Like is only applicable for pdp page and certain applicable user. This is implmented in loyaltyUICheck
        if (!Myntra.FB.loyaltyUICheck(fb_flow, data)) {
            cb("Privilege points not applicable.");
            return;
        }

        Myntra.FB.loyaltyLoader('show', fb_flow);
        $.post('/myntra/loyalty.php', {
            fb_flow: fb_flow,
            data: data
        }, function(res, textStatus, jqXHR) {
            if (textStatus !== "success") {
                //TODO : handle error condition here
                Myntra.FB.loyaltyLoader('hide', fb_flow);
                cb("Cannot assign Privilege Points. Try again later");
                return;
            }
            var status = res.status; // Any error while calulating the loyalty points
            if (status === 'ok') {
                var pointsAwarded = parseInt(res.points_awarded);
                Myntra.FB.loyaltyLoader('hide', fb_flow);

                // like_flow is applicable for pdb
                var strMsg = pointsAwarded;
                strMsg += (pointsAwarded > 1) ? " Privilege Points " : " Privilege Point ";
                $('#loyalty-widget').html('<i class="icon-tick"></i><span class="lbl">' + strMsg + '</span>added to your account.');
                cb(null);
            } else {
                //Handle loyalty related error and show it in UI
                Myntra.FB.loyaltyLoader('hide', fb_flow);
                cb("Cannot assign Privilege Points. Try again later");
            }
        });
    }
}

/**
 * Filtering the flow at UI level. Checks are also there in portal as well as services
 * To avoid unnecessary Ajax request. Not fully relying on UI
 * @return {[type]} [description]
 */
Myntra.FB.loyaltyUICheck = function(fb_flow, data) {
    var return_val = false;
    if (fb_flow === 'like_flow') {
        // Flag set in UI as hidden value for checking user allowed to get points for loyalty
        if ($('#canGetLoyaltyPointsForLike').val()) {
            //In case or like data will be url of the page liked
            return_val = true;
            var arr = data.split('/');
            if (arr[2] == 'www.facebook.com') return_val = false; //In case the like button is for Myntra facebook page
        } else {
            return_val = false;
        }
        if (return_val) {
            // Check misuse at UI level for a like button 
            var currentLikeUrl = data.substring(0, data.indexOf('?'));
            if (Myntra.FB.lastLikedUrl && Myntra.FB.lastLikedUrl === currentLikeUrl) {
                return_val = false;
            }
            Myntra.FB.lastLikedUrl = currentLikeUrl; // Store the last liked url to stop misue at UI level
        }
    } else if (fb_flow === 'checkout_flow') {
        return_val = true;
    }
    return return_val;
}

Myntra.FB.loyaltyLoader = function(action, fb_flow) {
    if (fb_flow === 'like_flow') {
        var loyaltyWidget = $('#loyalty-widget');
        if (action === 'show')
            loyaltyWidget.html('<img src="http://myntra.myntassets.com/skin2/images/loader.gif"/> Processing request for Loyalty Points.');
        if (action === 'hide')
            loyaltyWidget.html('');
    }
}