$( function() {
	if(!$(".order-summary").hasClass("mk-gc-order-summary")){
		$('.order-summary .list').jScrollPane();
	}
	if( $('.order-summary .jspVerticalBar').length ) {
		$('order-summary .jspPane').css("margin-left", "7px");
	}
});

// cod mobile verification
$(document).ready(function(){

    var titleDesc = "#title-description";
    var mobileOverlay = "#splash-mobile-verify";
    var cancelVerify = "#cancel-verify";
    var closeVerify = "#close-verify";
    var verifyCode = "#verify-code";
    var validateMobile = "#validate-mobile";
    var resendCode = "#resend-code";

    var codeDiv = "#code-div";
    var successInfoDiv = "#success-div";
    var accountInfoDiv = "#account-error";
    var exceedInfoDiv = "#exceed-error";
    var waitInfoDiv = "#wait-div";
    var codeInfoDiv = "#code-error";
    var resendDiv = ".mob-resend";
    var successResent = "#success-resent";

    var vCode = "input[name='v_code']";
    var token = "input[name='_token']";
    
    // all mobile verification code needs to be inside this
    if($(mobileOverlay).length){
        var overlayObj = Myntra.LightBox(mobileOverlay);
        overlayObj.show();

        // generate verification code
        function generateCode(){
            // show loading
            overlayObj.showLoading();

            //send post mode as generate code, token
            $.ajax({
                type: "POST",
                async: false,
                url: https_loc+"/myntra/ajaxCODMobileVerifier.php",
                data: "&mode=generate-code&_token="+$(token).val(),
                success: function(data) {                    
                    var resp = JSON.parse(data);
                    overlayObj.hideLoading();

                    // hide title description(show only at the start)
                    $(titleDesc).hide();

                    // no token match or missing required params
                    if(!resp.mode && resp.status == 'F'){
                        alert("Unable to serve the request.")

                    // on success, show verify code box, show wait message(for 2 mins), change button 'verify code', show button 'cancel'
                    } else if(resp.status == 'S'){
                        $(codeDiv).show();
                        $(waitInfoDiv).show();
                        $(verifyCode).show();
                        $(cancelVerify).show();

                        $(validateMobile).hide();
                        checkResendOption(resp.max_attempt, resp.num_attempt);

                    // on error, show max attemt exceed info, change button to 'close'
                    } else {
                        $(exceedInfoDiv).show();
                        $(closeVerify).show();

                        closeOnTimeOut = setTimeout(function(){
                            overlayObj.hide();
                        }, 8000);
                    }
                }

            });
        }

        function checkResendOption(maxAttempt, numAttempt){
            // shoe resend option only then when num attempt is lesser than max
            if(numAttempt < maxAttempt){
                $(resendCode).show();
                
            } else {
                $(resendCode).hide();                
            }
            // hide success message
            $(successResent).hide();
        }

        // verify code
        function verifyCodeSentToMobile(){
            // show loading
            overlayObj.showLoading();

            //send post mode as generate code, token
            $.ajax({
                type: "POST",
                async: false,
                url: https_loc+"/myntra/ajaxCODMobileVerifier.php",
                data: "&mode=verify-mobile-code&v_code="+$(vCode).val()+"&_token="+$(token).val(),
                success: function(data) {
                    var resp = JSON.parse(data);
                    overlayObj.hideLoading();

                    // close rend option div
                    $(resendDiv).hide();

                    // no token match or missing required params
                    if(!resp.mode && resp.status == 'F'){
                        alert("Unable to serve the request.")

                    // on success, show success message, show button 'close', close overlay with timeout
                    } else if(resp.status == 'S'){
                        $(successInfoDiv).show();
                        $(closeVerify).show();

                        $(codeDiv).hide();
                        $(waitInfoDiv).hide();
                        $(codeInfoDiv).hide();
                        $(accountInfoDiv).hide();
                        $(verifyCode).hide();
                        $(cancelVerify).hide();                        

                        closeOnTimeOut = setTimeout(function(){
                            overlayObj.hide();
                        }, 8000);

                    // on error, show wrong code message
                    } else if(resp.status == 'F') {
                        $(accountInfoDiv).show();
                        $(codeInfoDiv).hide();
                        $(waitInfoDiv).hide();

                    // on error, show wrong code message
                    } else {
                        $(codeInfoDiv).show();
                        $(accountInfoDiv).hide();
                        $(waitInfoDiv).hide();
                    }
                }
            });
        }
        // click on validate mobile
        $(validateMobile).click(function(){
            generateCode();
        });

        // click on verify code
        $(verifyCode).click(function(){
            verifyCodeSentToMobile();
        });

        // click on resend code -> should trigger validate mobile
        $(resendCode).bind("click",function(){
            generateCode();            
            $(successResent).show();
        });

        // enter on verification code text bix -> should trigger verify code
        $(vCode).keydown(function(event) {
            if (event.keyCode == '13') {
                event.preventDefault();
                $(verifyCode).trigger("click");
            }
        });

        // click on cancel
        $(cancelVerify).click(function(){
            overlayObj.hide();
        });

        // click on close
        $(closeVerify).click(function(){
            overlayObj.hide();
        });
    }
    
});
// cod mobile verification
