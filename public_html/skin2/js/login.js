Myntra.Utils.trackOmnitureLoginAttempt = function() {
	if(typeof(s_analytics) != "undefined") {
		s_analytics.linkTrackVars='events';
		s_analytics.linkTrackEvents='event7';
		s_analytics.events='event7';
		s_analytics.tl(true, 'o', 'Login Popup');
	}
}

Myntra.Utils.trackOmnitureLoginSuccessful = function() {
	if(typeof(s_analytics) != "undefined") {
		s_analytics.linkTrackVars='events';
		s_analytics.linkTrackEvents='event21';
		s_analytics.events='event21';
		s_analytics.tl(true, 'o', 'Login Success');
	}
}

Myntra.Utils.trackOmnitureLoginTimeoutFailure = function() {
    if(typeof(s_analytics) != "undefined") {
        s_analytics.linkTrackVars='events';
        s_analytics.linkTrackEvents='event22';
        s_analytics.events='event22';
        s_analytics.tl(true, 'o', 'Login Timeout Failure');
    }
}

Myntra.xdMessage = function(msg) {
    $(document).trigger('myntra.xdmsg', msg);
};

Myntra.LoginBox = function(id, cfg) {
    cfg = cfg || {};
    var obj = Myntra.LightBox(id, cfg),
        mainDiv = $(id + ' .main-content'),
        fpDiv = $(id + ' .forgot-pass-msg'),
        fpDivEmail = $(id + ' .forgot-pass-msg .email'),
        fbErr = $(id + ' .err-fb'),
        frmLogin = $(id + ' .frm-login'),
        frmSignup = $(id + ' .frm-signup'),
        loginUser = frmLogin.find('input[name="email"]'),
        loginPass = frmLogin.find('input[name="password"]'),
        signupUser = frmSignup.find('input[name="email"]'),
        signupPass = frmSignup.find('input[name="password"]'),
        signupMobile = frmSignup.find('input[name="mobile"]'),
        signupGender = frmSignup.find('input[name="gender"]'),
        signupName = frmSignup.find('input[name="name"]'),
        secureInvoked = frmSignup.find('input[name="secure_invoked"]'),
        secureNewRegister = frmSignup.find('input[name="new_register"]'),
        secureLogin = frmSignup.find('input[name="secure_login"]'),
        signupBirthYear = frmSignup.find('select[name="birth-year"]'),
        loginUserErr = frmLogin.find('.err-user'),
        loginPassErr = frmLogin.find('.err-pass'),
        loginCaptchaErr = frmLogin.find('.err-login-captcha'),
        signupUserErr = frmSignup.find('.err-user'),
        signupPassErr = frmSignup.find('.err-pass'),
        signupMobileErr = frmSignup.find('.err-mobile'),
        signupGenderErr = frmSignup.find('.err-gender'),
        signupNameErr = frmSignup.find('.err-name'),
        signupBirthYearErr = frmSignup.find('.err-birthyear'),
        loginBtn = frmLogin.find('.btn-signin'),
        signupBtn = frmSignup.find('.btn-signup'),
        hdTitle = $(id + ' .hd .h2'),
        title = hdTitle.text(),
        tmrTimeout;
        if(!parseInt(secureNewRegister.val())){
            $('<iframe id="mklogin-iframe" name="mklogin-iframe" src="javascript:void(0)"></iframe')
            .css({'position':'absolute', 'height':'0', 'top':'-100px'})
            .appendTo('body');
            frmLogin.attr('target', 'mklogin-iframe');
            frmSignup.attr('target', 'mklogin-iframe');
        }

    
    var onmessage = function(e) {
        (e.origin === https_loc || e.origin === http_loc) && $(document).trigger('myntra.xdmsg', e.data);
        
    };
    window.addEventListener
        ? window.addEventListener('message', onmessage, false)
        : window.attachEvent
            ? window.attachEvent('onmessage', onmessage)
            : null;

    var hideErrors = function() {
     if($(".loginsignupV2 .placeholder").length==0){
        loginUserErr.html('');
        loginPassErr.html('');
        signupUserErr.html('');
        signupPassErr.html('');
        signupMobileErr.html('');
        signupGenderErr.html('');
        signupNameErr.html('');
        if(signupBirthYearErr.length)   signupBirthYearErr.html('');
        fbErr.html('');
      }
    };
    var validateUser = function(user, errUser) {
        if (!user.val()) {
            errUser.text('Enter your email address');
            return false;
        }
        else if (!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(user.val())) {
            errUser.text('Enter a valid email address');
            return false;
        }
        else {
            errUser.text('');
            return true;
        }
    };

    var validatePass = function(pass, errPass) {
        if (!pass.val()) {
            errPass.text('Enter your password');
            return false;
        }
        else if (pass.val().length > 0 && pass.val().length < 6) {
            errPass.text('Minimum 6 characters required');
            return false;
        }
        else {
            errPass.text('');
            return true;
        }
    };

    var validateMobile = function(mobile, errMobile) {
    	if ( Myntra.Data.phoneNumNotRequired == 'test' &&  Myntra.Data.regV2 == 'test')
    	    return true;
        if (!mobile.val()) {
            errMobile.text('Enter your mobile number');
            return false;
        }
        else if (!/^\d{10}$/.test(mobile.val())) {
            errMobile.text('Enter a valid mobile number');
            return false;
        }
        else {
            errMobile.text('');
            return true;
        }
    };
    
    var validateGender = function(gender, errGender) {
        
        if((!gender.length) || (!errGender.length)){
            errGender.text('');
            return true;
        }
        
        if(!(gender[0].checked || gender[1].checked)){
            errGender.text('Select Gender');
            return false;
        }
        else{
            errGender.text('');
            return true;
        }
        /*if(!gender.length){
        	errGender.text('');
        	return true;
        }
        else if(gender.is(':checked')) {
        	errGender.text('');
        	return true;
            }
        else {
        	errGender.text('Please select gender');
            return false;
        }*/
    };
    
    var validateName = function(name, errName) {
        
        if((!name.length) || (!errName.length)) {
            return true;
        }
        if (!name.val()) {
            errName.text('Enter your name');
            return false;
        }
        else {
            errName.text('');
            return true;
        }
    }

    var validateBirthYear = function(birthYear, errBirthYear) {
        
        if((!birthYear.length) || (!errBirthYear.length)){
            return true;
        }

        if (!birthYear.val() || birthYear.val() == '0000') {
            errBirthYear.text('Required');
            return false;
        }
        else if (!/^\d{4}$/.test(mobile.val())) {
            errBirthYear.text('Invalid Year');
            return false;
        }
        else {
            errBirthYear.text('');
            return true;
        }
    }


    $(document).bind('myntra.xdmsg', function(e, msg) {
        var data = {},
            kvPairs = msg.split(';'),
            errs, err, i, n, kv;
        
        for (i = 0, n = kvPairs.length; i < n; i += 1) {
            kv = kvPairs[i].split('=');
            data[kv[0]] = kv[1];
        }
        
        if (!(data.type === 'signin' || data.type === 'signup')) {
            return;
        }
        
        if (tmrTimeout) {
            clearTimeout(tmrTimeout);
            tmrTimeout = null;
            Myntra.Utils.Cookie.del('_loginerr');
        }

        if (data.status === 'auth-error') {
            Myntra.Utils.Cookie.set('_loginerr', cfg.invoke + '*' + cfg.action + '*authfail');
            _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'auth_error']);
            obj.reloadPage();
        }
        else if (data.status === 'data-error') {
            obj.hideLoading();
            _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'data_error']);
            errs = data.errors.split('~');
            for (i = 0, n = errs.length; i < n; i += 1) {
                err = errs[i];
                if (err === 'email') {
                    val = 'Email address is invalid';
                    cfg.action === 'signup' ? signupUserErr.html(val) : loginUserErr.html(val);
                }
                if (err === 'pass') {
                    val = 'Please enter your password';
                    cfg.action === 'signup' ? signupPassErr.html(val) : loginPassErr.html(val);
                }
                if (err === 'mobile') {
                    val = 'Mobile number is invalid';
                    cfg.action === 'signup' ? signupMobileErr.html(val) : loginMobileErr.html(val);
                }
            }
        }
        else if (data.status === 'success') {
            if(data.signinFrmSignup == '1' ){
                Myntra.Utils.Cookie.set('_mklogin', cfg.invoke + '*' + cfg.action + '*success-signinfrmsignup');
            }
            else{
                Myntra.Utils.Cookie.set('_mklogin', cfg.invoke + '*' + cfg.action + '*success');
            }
            Myntra.Utils.trackOmnitureLoginSuccessful();
            obj.reloadPage();
        }
        else if (data.status === 'error') {
            obj.hideLoading();
            if (data.errorcode === '1') {
                if (cfg.action === 'signup') {
                    signupUserErr.html('Email Address already exists.');
                    _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'failure_username_already_exists']);
                }
                else if (cfg.action === 'signin') {
                    _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'failure_password_invalid']);
                    loginUserErr.html('Email ID and Password do not match.');
                }
            }
            else if (data.errorcode === '2') {
                if (cfg.action === 'signin') {
                    _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'failure_login_not_found']);
                    loginUserErr.html('Email ID and Password do not match.');
                    if(data.showLoginCaptcha === '1'){
                        $('[name="password"]').val("");
                        $('[name="userinputcaptcha"]').val("");
                        $('#captcha').attr('src',(location.protocol==='https:'?'s_':'')+'/captcha/captcha.php?id=loginPageCaptcha');
                    }
                }
            }
            else if(data.errorcode === '3') {
                if (cfg.action === 'signin') {
                    _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'failure_suspended_user']);
                    loginUserErr.html('Email ID and Password do not match.');
                }
            }
            //handle captcha on mutiple login failures
            if(data.showLoginCaptcha === '1'){
                $('#CaptchaBlock').show();
                $('#captcha-login-submit').css("margin-top","-5px");
            }
            else{
                $('#CaptchaBlock').hide();
            }
            if(data.errorcode === '5') {
                if (cfg.action === 'signin') {
                    _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'failure_incorrect_captcha']);
                    loginCaptchaErr.html('Incorrect captcha entered.');
                }
            }

        }
    });

    var fadeSignup = function() {
        if (!$('.reg-page .referral').length && !frmLogin.is('.inFocus')) {
        	hideErrors();
        	frmLogin.animate({'opacity':'1'}, 'slow').addClass('inFocus');
        	frmSignup.animate({'opacity':'0.5'}, 'fast').removeClass('inFocus');
        }
    	if($('.loginsignup').length){
        	hideErrors();
	}
    };

    var fadeLogin = function() {
        if (!$('.reg-page .referral').length && !frmSignup.is('inFocus')) {
        	hideErrors();
        	frmSignup.animate({'opacity':'1'}, 'slow').addClass('inFocus');
        	frmLogin.animate({'opacity':'0.5'}, 'fast').removeClass('inFocus');
        }
    	if($('.loginsignup').length){
        	hideErrors();
	}
    };

    obj.showSignup = function() {
        if($('.loginsignupV2').length){
            $(".signup-content").removeClass("mk-hide");
            $(".login-content").addClass("mk-hide");
           $(".loginsignupV2 .frm-signup input").blur();
        }
        else if(! $('.loginsignup').length){
            fadeLogin();
        }
        else{
            hideErrors();
            $('#navigation ul a#tab1').click();
        }
        frmSignup.find('input').filter(':first').focus();
    };
    obj.showLogin = function() {
        if($('.loginsignupV2').length){
            $(".login-content").removeClass("mk-hide");
            $(".signup-content").addClass("mk-hide");
            $(".loginsignupV2 .frm-login input").blur();
        }
        else if(! $('.loginsignup').length){
            fadeSignup();
        }
        else{
            hideErrors();
          $('#navigation ul a#tab2').click();
        }
        //console.log(frmLogin.find('input').filter(':first').attr('name'));
        frmLogin.find('input').filter(':first').focus();
    };

    if(! $('.loginsignup').length){
	    frmLogin.find('input').focus(fadeSignup);
	    frmLogin.click(fadeSignup);
	    frmSignup.find('input').focus(fadeLogin);
	    frmSignup.click(fadeLogin);
    }
    var handleTimeout = function() {
       tmrTimeout = setTimeout(function() {
            //adding one more data to capture the number of attemts made by the user/set count
            if(Myntra.Data.loginAttemptCount){
                Myntra.Data.loginAttemptCount = parseInt(Myntra.Data.loginAttemptCount)+1;
            }else{
                Myntra.Data.loginAttemptCount = 1;
            }
            Myntra.Utils.Cookie.set('_loginerr', cfg.invoke + '*' + cfg.action + '*timeout*'+Myntra.Data.loginAttemptCount);
            Myntra.Utils.trackOmnitureLoginTimeoutFailure();
            //Fire the beacon call with all required info
             $.ajax({
                type: "GET",
                url: (location.protocol==='https:'?'s_':'')+'ajax_beacon.php?email='+((cfg.action=='signin')?(loginUser.val()+'&type=signin'):(signupUser.val()+'type=signup'))+'&count='+Myntra.Data.loginAttemptCount,
                success :function(){
                    if(parseInt(secureLogin.val()) && Myntra.Data.loginAttemptCount){
                        window.location = https_loc+'/securelogin.php?msg='+encodeURIComponent('status=timeout;type='+cfg.action)+'&referrer='+encodeURIComponent(location.pathname);
                    }else{
                        location.reload();    
                    }
                    
                }
            });
            
        }, Myntra.Data.loginTimeout);
    };

    frmLogin.submit(function(e) {
        var r1 = validateUser(loginUser, loginUserErr),
            r2 = validatePass(loginPass, loginPassErr);
        if (r1 && r2) {
            window.localStorage.removeItem('lscache-beacon:user-data');
            window.localStorage.removeItem('lscache-beacon:user-data-cacheexpiration');
            cfg.action = 'signin';
            obj.showLoading();
            handleTimeout();
        }
        else {
            e.preventDefault();
            return;
        }
    });

    frmSignup.submit(function(e) {
        var r1 = validateUser(signupUser, signupUserErr),
            r2 = validatePass(signupPass, signupPassErr),
            r3 = validateMobile(signupMobile, signupMobileErr),
        	r4 = validateGender(signupGender, signupGenderErr);
            r5 = validateName(signupName, signupNameErr),
            r6 = validateBirthYear(signupBirthYear, signupBirthYearErr);

        if (r1 && r2 && r3 && r4 && r5 && r6) {
            window.localStorage.removeItem('lscache-beacon:user-data');
            window.localStorage.removeItem('lscache-beacon:user-data-cacheexpiration');
            cfg.action = 'signup';
            obj.showLoading();
            handleTimeout();
        }
        else {
            e.preventDefault();
            return;
        }
    });

    // forgot password interaction
    var showForgotPassMsg = function() {
        var btnOk = $(id + ' .btn-forgot-pass-ok');
        $(id + ' .mod').addClass('forgot-pw').css('background', '#fff');
        hdTitle.text('Forgot your password? No Worries!');
        fpDivEmail.html(loginUser.val());
        mainDiv.hide();
	if( Myntra.Data.regV2 == "test"){
	    $("#lb-login").removeClass("loginsignup").addClass("loginsignupV2");
	}
        if($('.loginsignup').length){
            if(Myntra.Data.loginBigBannersEnabled)
            {
                $('.mod-wrapper').css("background-image", "");
            }
        }
        fpDiv.show();
        obj.center();
        if (cfg.isPopup) {
            try { btnOk.focus(); } catch(ex) {}
        }
    };
    var hideForgotPassMsg = function() {
        $(id + ' .mod').removeClass('forgot-pw');
        hdTitle.text(title);
        mainDiv.show();
	if( Myntra.Data.regV2 == "test"){
	    $("#lb-login").removeClass("loginsignup").addClass("loginsignupV2");
	}

        if($('.loginsignup').length){
	    if(Myntra.Data.loginBigBannersEnabled)
            {   
                if($('#navigation .selected').attr('id') == 'tab2'){
                    if(location.protocol==='https:'){
                        var url = Myntra.Data.loginBigBanners[1].replace('http://myntra.myntassets.com/','https://d6kkp3rk1o2kk.cloudfront.net/');
;
                        $('.mod-wrapper').css("background-image", 'url("' + url + '")');
                    }else{
                        $('.mod-wrapper').css("background-image", 'url("' + Myntra.Data.loginBigBanners[1] + '")');
                    }
                }else{
                    if(location.protocol==='https:'){
                        var url = Myntra.Data.loginBigBanners[0].replace('http://myntra.myntassets.com/','https://d6kkp3rk1o2kk.cloudfront.net/');
;
                        $('.mod-wrapper').css("background-image", 'url("' + url + '")');
                    }else{
                        $('.mod-wrapper').css("background-image", 'url("' + Myntra.Data.loginBigBanners[0] + '")');
                    }
                }
            }
        }
        fpDiv.hide();
        obj.center();
    };
    $(id + ' .btn-forgot-pass').click(function(e) {
        if(! $('.loginsignup').length) {
    	   fadeSignup();
        }
        if (validateUser(loginUser, loginUserErr)) {
	        $.ajax({
	            type: "POST",
	            url: (location.protocol==='https:'?'s_':'')+'forgot_password.php',
	            data: "action=FORGOTPASSWORD&username=" + loginUser.val(),
	            beforeSend:function(){
	                obj.showLoading();
	            },
	            success: function(msg){
	                msg = $.trim(msg);
	                if (msg === 'success') {
	                    showForgotPassMsg();
	                }
	                else {
	                    loginUserErr.html("This email is not registered with Myntra.com");
	                }
	            },
	            complete: function(xhr, status) {
	                obj.hideLoading();
	            }
	        });
	        _gaq.push(['_trackEvent', cfg.invoke, 'forgot_password',window.location.toString()]);
        } else {
            e.preventDefault();
        	return;
    	}
    });

    if (cfg.isPopup) {
        $(id + ' .btn-forgot-pass-ok').click(function(e) {
            obj.hide();
            // TODO : Hook callback for after forgot password ok link is clicked
            if (typeof cfg.afterForgotPasswordOk === "function") {
                cfg.afterForgotPasswordOk();
            }
            _gaq.push(['_trackEvent', cfg.invoke,'forgot_passoword_popup', 'continue_shopping']);
        });
    }
    else {
        $(id + ' .btn-forgot-pass-ok').click(function(){
            window.location=http_loc;
        });

    }

    // facebook connect interaction
    $(id + ' .btn-fb-connect').click(function() {
        window.localStorage.removeItem('lscache-beacon:user-data');
        window.localStorage.removeItem('lscache-beacon:user-data-cacheexpiration');
        _gaq.push(['_trackEvent', cfg.invoke, 'fb_connect', 'click']);
        var referer = $(this).attr('data-referer');
        Myntra.FBConnect(referer);
    });
    $(document).bind('myntra.fblogin.auth', function(e, data) {
        obj.showLoading();
    });
    $(document).bind('myntra.fblogin.done', function(e, data) {
    	obj.hideLoading();
        switch (data.status) {
        case 'success':
        	if (cfg.isPopup) { obj.hide(); }
            Myntra.Utils.Cookie.set('_mklogin', cfg.invoke + '*fb_connect*success_' + data.fb_action);
            if(Myntra.Utils.Cookie.get('_sku')){
            	if(typeof Myntra.Data.MiniPIPsocialExclude != 'undefined'){
            		Myntra.Utils.Cookie.set('_saveitem_ex_social',Myntra.Data.MiniPIPsocialExclude);
            	}
            	else {
            		Myntra.Utils.Cookie.set('_saveitem_ex_social',Myntra.Data.PDPsocialExclude);
            	}
            }
            if(typeof(s_analytics) != "undefined") {
            	s_analytics.eVar18='success';
            }
            Myntra.Utils.trackOmnitureLoginSuccessful();
            obj.reloadPage();
            break;
        case 'failure':
            _gaq.push(['_trackEvent', cfg.invoke, 'fb_connect', 'failure']);
            fbErr.html('Connect with Facebook failed.');
            break;
        case 'access-denied':
            _gaq.push(['_trackEvent', cfg.invoke, 'fb_connect', 'access-denied']);
            fbErr.html('Facebook Connect failed. Permission denied.');
        }
    });
    

    obj.reloadPage = function(){
    	if (typeof cfg.onLoginDone === "function") {
            obj.hide();
            Myntra.Data.userEmail = cfg.action === 'signin' ? loginUser.val() : signupUser.val();
    		cfg.onLoginDone();
    	}
/*        else if (location.pathname == '/mkmycartmultiple_re.php'
                || location.pathname == '/mkmycartmultiple.php'
                || location.pathname == '/mkmycart.php') {
            location.href = https_loc + '/checkout-address.php';
        }
*/    	else {
    		location.reload();
    	}
    };

    obj.adjustHeight = function() {
        frmLogin.height(frmSignup.height());
    };

    obj.showErrMsg = function(err) {
        err.action == 'signin' ? loginUserErr.html(err.message) : signupUserErr.html(err.message);
        _gaq.push(['_trackEvent', cfg.invoke, err.action, err.name]);
    };

    // override show method
    var p_show = obj.show;
    obj.show = function() {
    	if ( Myntra.Data.Nudge === "test" &&  Myntra.Data.NudgeShow === "test") {
        	hideNudge();
        }
        hideForgotPassMsg();
        hideErrors();
        
        p_show.call(this);
        //obj.adjustHeight(); // removed for adding Captcha
        try { 
              if((cfg.invoke && cfg.invoke === 'splash') || (cfg.action && cfg.action === 'signup'))    {
                    if(Myntra.Data.rememberUser != '') {
                        obj.showLogin();
                        loginUser.focus();
                    } else {
                        obj.showSignup();
                        signupUser.focus();
                    }
              }
        } catch(ex) {}
    };

    obj.setInvokeType = function(val) {
        cfg.invoke = val;
    };

    obj.setCfg = function(name, value){
    	cfg[name]=value;
    };

    return obj;
};

Myntra.InitLogin = function(id) {
    if (!$(id).length) { return; }
    var cfg = {isModal:false};
    var obj = Myntra.LoginBox(id, cfg);

    $(document).bind('myntra.login.show', function(e, data) {
        var invoke = data && data.invoke ? data.invoke : 'header';
        obj.setInvokeType(invoke);
        //check if showLogin is true and then
        if($('#CaptchaBlock').length && Myntra.Data.showLoginCaptcha){
          $('#captcha').attr('src',(location.protocol==='https:'?'s_':'')+'/captcha/captcha.php?id=loginPageCaptcha');  
        }
        
        $(".loginsignupV2 input").length ? $(".loginsignupV2 input").blur() : '';
        if (data && data.onLoginDone) {
        	obj.setCfg("onLoginDone", data.onLoginDone);
        }
        // When the user explicitly closes the login modal box
        if (data && data.onLoginClose) {
            obj.setCfg("afterClose", data.onLoginClose);
        }

        // Callback after user click on ok after forgot password flow is completed
        if (data && data.afterForgotPasswordOk) {
            obj.setCfg("afterForgotPasswordOk", data.afterForgotPasswordOk);
        }
        if (data && data.afterHide) {
        	obj.setCfg("afterHide", data.afterHide);
        }
        if (data && data.isModal) {
        	obj.setCfg("isModal", data.isModal);
        }

        (data && data.disableCloseButton) ? obj.hideCloseButton() : obj.showCloseButton();
        obj.show();

        Myntra.Utils.trackOmnitureLoginAttempt();
        
        if (data && data.action == 'login') {
        	obj.showLogin();
        }
        else if (data && data.showerr) {
            var act = '';
            if(data.action == 'signin'){
                act = 'login';
                obj.showLogin();
            }else{
                act = 'sign up';
                obj.showSignup();
            }
            data.message = 'Something went wrong. Please try again';
            obj.showErrMsg(data);

        }
        else if(data && data.sessionExpired){
            obj.showLogin();
            
            frmLogin = $(id + ' .frm-login');
            loginUser = frmLogin.find('input[name="email"]');
            loginUser.attr('value', Myntra.Data.userEmail);
        
            data.message =Myntra.Data.secureSessionExpireMessage ? Myntra.Data.secureSessionExpireMessage : 'Your secure session is expired. Login again.';
            obj.showErrMsg(data);
        }else if(data && data.secureSessionExpired){
            obj.showLogin();
            frmLogin = $(id + ' .frm-login');
            loginUser = frmLogin.find('input[name="email"]');
            loginUser.attr('value', Myntra.Data.userEmail);
        
            data.message = Myntra.Data.secureSessionExpireMessage ? Myntra.Data.secureSessionExpireMessage : 'Your secure session is expired. Login again.';
            obj.showErrMsg(data);
        }
        //Myntra.Data.secureSessionExpireMessage
        else {            
            obj.showSignup();
        }
	 if (typeof showRegV2Login != 'undefined' && showRegV2Login == '1'){
	     $(".showLogin").click();
	 }

    });

    // exclude some pages from showing the splash screen
    if (location.pathname == '/register.php' || location.pathname == '/mksystemerror.php' || location.pathname == '/securelogin.php') {
        return true;
    }
    
    //$('.gender-sel').parent().jqTransform();
    var ckLoginErr = Myntra.Utils.Cookie.get('_loginerr');
    if (ckLoginErr) {
        Myntra.Utils.Cookie.del('_loginerr');
        var parts = ckLoginErr.split('*');
        //set Myntra.Data.loginAttemptCount here
        if(parts[3]){
            Myntra.Data.loginAttemptCount = parts[3];
        }
        $(document).trigger('myntra.login.show', {showerr:1, invoke:parts[0], action:parts[1], name:parts[2]});
    }

    if(typeof Myntra.Data.EmailUUID != "undefined" && Myntra.Data.EmailUUID != "" && Myntra.Data.EmailUUID != null && Myntra.Data.userEmail == ""){
    	$(document).trigger('myntra.login.show', [{onLoginDone: function() {
    		var giftCardAjaxHelperUrl = http_loc+"/giftCardsAjaxHelper.php?";
    		$.ajax({
    			url: giftCardAjaxHelperUrl+"type=gc&mode=activate&gc="+Myntra.Data.EmailUUID,
				success: function(data) {
					var result = $.parseJSON(data);
					if(result.status != "failed"){
						Myntra.Utils.Cookie.set('_mkgc', 'success*'+result.message);
					}
					else{
						Myntra.Utils.Cookie.set('_mkgc', 'failed*'+result.message);	
					}
					location.href = '/';
			}});	
    	}}]);
    	Myntra.Utils.trackOmnitureLoginAttempt();
    	Myntra.Utils.Cookie.set('splash', 0, 30);
    } else {  
        if (!Myntra.Utils.Cookie.get('splash')) {
            Myntra.Utils.Cookie.set('splash', $("#lb-login").data("signupsplashpageload")+"" || 1, 30);            
        } else {
            Myntra.Utils.Cookie.set('splash', Math.max(0, parseInt(Myntra.Utils.Cookie.get('splash'))-1), 30);
        }


    	if (Myntra.Utils.Cookie.get('splash') && Myntra.Utils.Cookie.get('splash') === "1") {
            // condition: query param ".nsp" should NOT present AND user is NOT logged in
            if (!Myntra.Utils.getQueryParam('.nsp') && !Myntra.Data.userEmail && $('#loginSplashVariant').val() != 'test') {
                obj.setInvokeType('splash');
        		if (!Myntra.Data.nologin) {
            		setTimeout(function() {
            		    obj.show();
            		    Myntra.Utils.trackOmnitureLoginAttempt();
            		}, parseInt(Myntra.Data.splashDelay));
        		}
            }
            Myntra.Utils.Cookie.set('splash', 0, 30);
        }
    }
    return true;
};

Myntra.InitRegLogin = function(id) {
    if (!$(id).length) { return; }
    var cfg = {isPopup:false,isModal:true};

    var obj = Myntra.LoginBox(id, cfg);
    //$('.gender-sel').parent().jqTransform();
    obj.setInvokeType('reg_page');
    var _ref = Myntra.Utils.getQueryParam('ref');
    var view = Myntra.Utils.getQueryParam('view');
    if (location.pathname === '/register.php' && view) {
        obj.showLogin();
        obj.setCfg("onLoginDone", function() { 
            if(view=='savedcards'){
                location.href = '/mySavedCards.php'; 
            } else if(view=="leaderboard"){
                location.href = '/leaderboard.php?lbId=' + Myntra.Utils.getQueryParam('lbId');
            }
            else{
                location.href = '/mymyntra.php?view=' + view;     
            }
            
            });
    }
    else {
        obj.showSignup();
        obj.setCfg("onLoginDone", function() { location.href = http_loc+_ref?_ref:'' });

        //set the redirection login here
    }
    //obj.adjustHeight();//removed for adding captcha

    var ckLoginErr = Myntra.Utils.Cookie.get('_loginerr');
    if (ckLoginErr) {
        Myntra.Utils.Cookie.del('_loginerr');
        var parts = ckLoginErr.split('*');
        //set Myntra.Data.loginAttemptCount here
        if(parts[3]){
            Myntra.Data.loginAttemptCount = parts[3];
        }
        var err = {showerr:1, invoke:parts[0], action:parts[1], name:parts[2]};
        var act = err.action == 'signin' ? 'login' : 'sign up';
        err.message = 'Something went wrong. Please try again';
        obj.showErrMsg(err);
    }

    return true;
};

$(document).ready(function() {

    $('.loginsignupV2 input').placeholder(); 
    if($('.loginsignup').length){
                $('#lb-login .mod').css("height",'400px');
    }
    if($('.loginsignup').length){
    if(Myntra.Data.loginBigBannersEnabled)
    {
            if(location.protocol==='https:'){
                var url = Myntra.Data.loginBigBanners[0].replace('http://myntra.myntassets.com/','https://d6kkp3rk1o2kk.cloudfront.net/');
                $('.mod-wrapper').css("background-image", 'url("' + url + '")');
            }else{
                $('.mod-wrapper').css("background-image", 'url("' + Myntra.Data.loginBigBanners[0] + '")');
            }
    }
    }
    if($('#reg-login').length){
      Myntra.InitRegLogin('#reg-login');

    }else if($('#lb-login').length){
       Myntra.InitLogin('#lb-login'); 
    } 
    if ($('.loginbox').length) {
        //pre-load the login background image
        var bgimg = $(".loginbox").find("input:hidden[name=loginbg]").val();
        if(bgimg && bgimg != "") {
            new Image().src = bgimg;
        }
    }
    if(Myntra.Utils.Cookie.get('__prompt_login')){
        Myntra.Utils.Cookie.del('__prompt_login');
        Myntra.expiryLoginPrompt();
    }
    if(typeof showLogin != 'undefined'){
            if (showLogin){
                $(document).trigger('myntra.login.show');
                 //$(".frm-login .btn-signin").click();
                 }
    }

    if($('.loginsignup').length){
	    $('#tab2-content').hide();
	    $('#navigation ul a').click(function(){
	      
		$('.loginsignup .frm-login .err-user').html('');
		$('.loginsignup .frm-login .err-pass').html('');
		$('.loginsignup .frm-signup .err-user').html('');
		$('.loginsignup .frm-signup .err-pass').html('');
		$('.loginsignup .frm-signup .err-mobile').html('');
		$('.loginsignup .frm-signup .err-gender').html('');
		$('.loginsignup .frm-signup .err-name').html('');
		if($('.loginsignup .frm-signup .err-birthyear').length){
			   $('.loginsignup .frm-signup .err-birthyear').html('');
		}
		$('.loginsignup .err-fb').html('');

		$('#navigation ul a').removeClass('selected');
		var tabId = $(this).attr('id');
		if(tabId == 'tab1')
		{
			$('#navigation ul a#tab1').addClass("selected");
			$('#tab2-content').hide();
			$('#tab1-content').show();
		    if(Myntra.Data.loginBigBannersEnabled){
			    if(location.protocol==='https:'){
	                        var url = Myntra.Data.loginBigBanners[0].replace('http://myntra.myntassets.com/','https://d6kkp3rk1o2kk.cloudfront.net/');
                    	        $('.mod-wrapper').css("background-image", 'url("' + url + '")');
                	    }else{
                    		$('.mod-wrapper').css("background-image", 'url("' + Myntra.Data.loginBigBanners[0] + '")');
                	    }
		    }
            $('.frm-signup').find('input').filter(':first').focus();
		}else if(tabId == 'tab2')
		{
			$('#navigation ul a#tab2').addClass("selected");
			$('#tab1-content').hide();
			$('#tab2-content').show();
		    if(Myntra.Data.loginBigBannersEnabled){
			   if(location.protocol==='https:'){
                    	   	var url = Myntra.Data.loginBigBanners[1].replace('http://myntra.myntassets.com/','https://d6kkp3rk1o2kk.cloudfront.net/');
                    		$('.mod-wrapper').css("background-image", 'url("' + url + '")');
                	   }else{
                    		$('.mod-wrapper').css("background-image", 'url("' + Myntra.Data.loginBigBanners[1] + '")');
                	   }
		    }
            $('.frm-login').find('input').filter(':first').focus();
		}
	    });
    }

    var show = Myntra.Utils.getQueryParam('show');
    var done = Myntra.Utils.getQueryParam('done');
    if(show =="login"){
	var cfg = {};
	var href = null;
	if (done) {
	    href = decodeURIComponent(done);
	}
	if (href) {
	    cfg.action = 'login';
	    cfg.onLoginDone = function() {
		location.href = href;
	    };
	}else{
	    	cfg.onLoginDone = function() {
		location.href = '/' ;
	    };
	}
    $(document).trigger('myntra.login.show', cfg);
    }
});

//depopulate sign up fields
$(window).load(function() {
    //New login lazy load images on new checkout as well 
    Myntra.Utils.loadImageQueue();
	setTimeout(function() {
		$('.loginbox .frm-signup input[type=text], .loginbox .frm-signup input[type=password]').val('');
	}, 1500);
    $(".fb-facepile iframe").attr("src",Myntra.Data.fbFacepileSrc);
});

//nudge related js
var showNudgeAgain = true;
var hideNudgeAgain = false;
$(window).ready(function(){
    if(Myntra.Data.Nudge === "test" ){
        $(".dockicon").mouseenter(function(e){
            if (Myntra.Data.NudgeOpen  == "0") { 
	            if (showNudgeAgain){
	                _gaq.push(['_trackEvent', "nudge", "mouseenter", '']);
                    $(".nudgeOutter").animate({left:'24px'},700,function(){showNudgeAgain = true});
                    $(".nudgeIconArea .cross").hide();
                    $(".collapsed").removeClass("collapsed");
	                $("#nudge").css("z-index","1010"); 
	                e.stopPropagation();
	                showNudgeAgain = false;
	            }
	        }
        });
        $("#nudge").mouseleave(function(e){
	        if (Myntra.Data.NudgeOpen  == "0"){   
                if (hideNudgeAgain){
		            _gaq.push(['_trackEvent', "nudge", "mouseleave", '']);
	                $(".nudgeOutter").animate({left:'-126px'},700,function(){hideNudgeAgain = true});
	                $(".dockicon").addClass("collapsed");
	                $(".nudgeIconArea .cross").hide();
        	        $("#nudge").css("z-index","1"); 
		            e.stopPropagation();
                    hideNudgeAgain = false;
                }    
	        }	 
	    });
        $(".nudgeIconArea .cross").click(function(event){
            _gaq.push(['_trackEvent', "nudge", "crossclick", '']);
            $(".nudgeOutter").animate({left:'-126px'},700);
	        $(".dockicon").addClass("collapsed");
               //$(".nudgeIconArea .cross").hide();
	        $("#nudge").css("z-index","1");
            event.stopPropagation();
            Myntra.Data.NudgeOpen = "0";
            Myntra.Utils.Cookie.set('nudgeOpen',2);
        });
        $(".nudgeIconArea ,.nudgeOffer,.dockicon").click(function(){
	        _gaq.push(['_trackEvent', "nudge", "nudgeclick", '']);
	        $(".showSignup").click();
            $(document).trigger('myntra.login.show');
        });
        if ( Myntra.Data.Nudge === "test" &&  Myntra.Data.NudgeShow === "test"){
	        Myntra.Data.NudgeOpen = "1";
	        setTimeout(function(){
	            showNudge();
	        },1000);
            setTimeout(function(){
                Myntra.Utils.Cookie.set('nudgeOpen',2);
	            hideNudge();
	        },10000);
	    }
    }
});
function showNudge(){
    var nudgeCookieVal = Myntra.Utils.Cookie.get('nudgeOpen');
    if(nudgeCookieVal != undefined &&  nudgeCookieVal == 2) {
        return;
    } 
    if(nudgeCookieVal == undefined) {
        Myntra.Utils.Cookie.set('nudgeOpen',1);
    } 
    $(".nudgeOutter").animate({left:'24px'},700);
	$(".nudgeIconArea .cross").show();
	$("#nudge").css("z-index","1010");
	$(".dockicon").removeClass("collapsed");
    showNudgeAgain=true;hideNudgeAgain = true;
}
function hideNudge(){
	$(".nudgeOutter").animate({left:'-126px'},700);
    $(".nudgeIconArea .cross").hide();
    $("#nudge").css("z-index","1");
	$(".dockicon").addClass("collapsed");
    Myntra.Data.NudgeOpen  = "0";
    showNudgeAgain = true;hideNudgeAgain = true;
}
//end: nudge related js
Myntra.expiryLoginPrompt = function(){
	$(document).trigger('myntra.login.show', {
        onLoginDone: function() {
    		if(Myntra.Utils.getQueryParam('promptlogin',location.href)){
                window.location=location.protocol+'//'+location.hostname + location.pathname; 
            }
            else {
                location.reload();                
            }
    	}, 
        action:'signin',
        sessionExpired:true,
        isModal:false,
        onLoginClose : function(){
              window.location = http_loc; 
             
        }.bind(this),
            afterForgotPasswordOk : function(){
                window.location = http_loc; 
        }.bind(this)
    });
};

/*START: registration v2 */
$(document).ready(function(){

    $(".showLogin").click(function(){
            $(".login-content").removeClass("mk-hide");
            $(".signup-content").addClass("mk-hide");
	     $(".loginsignupV2 .frm-login input").blur();
        });
    $(".showSignup").click(function(){
            $(".signup-content").removeClass("mk-hide");
            $(".login-content").addClass("mk-hide");
	     $(".loginsignupV2 .frm-signup input").blur();
        });
    $(".skipReturn").click(function(){
            $(".close").click();
        });
    });
/*END: registration v2 */
Myntra.expiryReloadLoginPrompt = function(){
    Myntra.Utils.Cookie.set('__prompt_login', 1, 30);
    location.reload();
}
