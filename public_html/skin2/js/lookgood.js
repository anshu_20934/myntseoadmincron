$(document).ready(function(){
	
Myntra.InitStarNStyle = function(){
	var star_cont='#star-inner-cont';
	$(star_cont+' a').each(function(index){
		var coords=($(this).attr('data-coords')).split(',');
		var w_=coords[2]-coords[0];
		var h_=coords[3]-coords[1];
		$(this).css({width:w_+'px',height:h_+'px',top:coords[1]+'px',left:coords[0]+'px'});
	});
	//$('.star-n-style-details').find('select').jqTransSelect();
	$('.star-n-style-details').find('select').change(function(){
		window.location=$(this).val();
	});
	$('.star-n-style-details ol >li.link').hover(function(){
			var _in = $(this).index()-1;
			$(star_cont+' a').eq(_in).trigger('mouseenter');
		},
		function(){
			var _in = $(this).index()-1;
			$(star_cont+' a').eq(_in).trigger('mouseleave');
		}
	);

	$(star_cont+' a').hover(function(){
		var coords=($(this).attr('data-coords')).split(',');
		$(star_cont).css({background:'#000',opacity:'.6'});
		$(this).css({background:'transparent url('+starImage+') no-repeat -'+coords[0]+'px -'+coords[1]+'px'});
	},
	function(){
		$(star_cont).css({background:'none'});
		$(this).css({background:'none'});
	});
	if($('.star-n-style-details ul>li').length > 3){
		$('.star-n-style-list').jScrollPane();
	}
	
	//GA tracking
	$('.star-n-style-details ol >li.link').click(function(e){
		var _gaParam=$(this).find('a').attr('title');
		_gaq.push(['_trackEvent', 'starntyle', 'sns_lp', _gaParam]);
	});
	$(star_cont+' a').click(function(e){
		var _gaParam=$(this).attr('title');
		_gaq.push(['_trackEvent', 'starntyle', 'sns_lp_img', _gaParam]);
	});
} 
	
	
Myntra.InitLookBook=function(){
	var ol_width=0;
	var lgLD='lg-look-details';

    var loadImages = function(pageNo) {
        var start = pageNo ? pageNo-1 : 0;
        $('.'+lgLD).slice(start, pageNo+2)
            .add($('#lg-look-book li').slice(start, pageNo+2))
                .find('img.unfetched').attr('src', function(i) {
                    return $(this).removeClass('unfetched').data('lazy-src');
                })/*
            .end()
        .end()
            .find('.lg-social-btns.unloaded')
                .each(function(index){
                    $(this)
                        .find('.fb')
                            .html('<fb:like href="' + window.location.href.split("?")[0] + '?utm_source=facebook&utm_medium=look&look=' + $(this).attr("data-look-id") + '" send="false" layout="button_count" width="80" action="like"></fb:like>')
                        .end()
                            .find('.gp')
                                .html('<g:plusone href="' + window.location.href.split("?")[0] + '?utm_source=gplus&utm_medium=look&look=' + $(this).attr("data-look-id") + '" size="medium" count="true"></g:plusone>')
                            .end()
                            .removeClass('unloaded');
                })*/;
    };

    //CHECK OUT THE HEIGHT and init jscrollPane
	$('.lg-look-book').carousel({
		itemsPerPage: 1, // number of items to show on each page
		itemsPerTransition: 1, // number of items moved with each transition
		noOfRows: 1, // number of rows (see demo)
		nextPrevLinks: true, // whether next and prev links will be included
		pagination: true, // whether pagination links will be included
		speed: 'normal', // animation speed
		easing: 'swing',
		insertPagination:function(pagination){
			$(pagination).appendTo($('.all-looks-inner'));
			$(pagination).find('a').each(function(index){
				ol_width=ol_width+$(this).parent().width()+8;
			});
			$('.rs-carousel-pagination').width(ol_width);
			$('.all-looks-inner').jScrollPane();
			return $(pagination);
			},
		after: function(event,data) {$(document).trigger('lg.look.change.complete', [data]);},
		before: function(event,data) {$(document).trigger('lg.look.change.init', [data]);}
	});
	
	if(Myntra.Data.lookId){
		var inToScroll=$('.'+lgLD+'#look-'+Myntra.Data.lookId).index();
		$('.lg-look-book').data('carousel').goToItem(inToScroll);
        loadImages(inToScroll);
	} else {
        loadImages(0);        
    }
	
	$(document).bind('lg.look.change.complete', function(e,data){
		$('.'+lgLD).hide();
		var indexClicked = data.page.index();
		$('.'+lgLD).eq(indexClicked).show();
	});

	$('.rs-carousel-pagination-link').bind('click',function(){
		$('html, body').animate({scrollTop: $('#lg-container').position().top-30}, 800, function() {});
	});
	
	$(document).bind('lg.look.change.init', function(e,data){
		$('.'+lgLD).fadeOut('100');
        var indexClicked = data.page.index();
        loadImages(indexClicked);
	});
	
	$('.style-display-control').click(function(e,data){
		var __state = 0;
		if(data && data.state){
			__state = data.state;
		}
		$(this).find('span').toggleClass('close-state');
		$(this).closest('.'+lgLD).find('.look-styles-overlay').slideToggle('slow');
		$(this).closest('.'+lgLD).find('.look-styles').slideToggle('slow',function(){

			//if($(this).parent().siblings('.style-display-control-top').hasClass('mk-hide') && !__state){
			//	$(this).parent().siblings('.style-display-control-top').removeClass('mk-hide');
			//}
		});
	});
/*	$('.style-display-control-top').click(function(){
		var lookId = $(this)[0].getAttribute('data-index');
		
		$(this).addClass('mk-hide');
		$(this).siblings('.look-styles-cont').find('.style-display-control').trigger('click',{state:1});
		
		_gaq.push(['_trackEvent', 'starnstyle', 'sns_lb', lookId+'_allproducts']);
	
	});
*/
	$('#lg-look-book .rs-carousel-action-next').bind('click',function(){
		_gaq.push(['_trackEvent', 'starnstyle', 'sns_lb', 'next']);
	});
	$('#lg-look-book .rs-carousel-action-prev').bind('click',function(){
		_gaq.push(['_trackEvent', 'starnstyle', 'sns_lb', 'prev']);
	});
	$('.look-styles').bind('click',function(){
		_gaq.push(['_trackEvent', 'starnstyle', 'sns_lb', 'product']);
	});
	$(document).unbind('keydown.lookbook');
	
	var lookKeyBind=function(e){
	
   		if(!$('#quick-look-mod').is(':visible')){
   			if(e.keyCode==39){    	   			
   				$('#lg-look-book .rs-carousel-action-next').trigger('click');
   				
   			}
   			else if(e.keyCode==37){    				
   				$('#lg-look-book .rs-carousel-action-prev').trigger('click');
   			}
   		}
	};
	
   	$(document).bind('keydown.lookbook',lookKeyBind);
	//INIT CAROUSEL FOR STYLE IF IT IS MORE THAN 8
   	$('.look-styles-inner').each(function(index){
   		if($(this).find('ul >li').length > 8) {
   			$(this).css({'width':'688px','float':'left','margin-left':'60px'});
   			$(this).carousel({
   				itemsPerPage: 8, // number of items to show on each page
   				itemsPerTransition: 1, // number of items moved with each transition
   				noOfRows: 1, // number of rows (see demo)
   				nextPrevLinks: true, // whether next and prev links will be included
   				pagination: false, // whether pagination links will be included
   				speed: 'normal', // animation speed
   				easing: 'swing'
   			});
   		$(this).siblings('.look-style-right').css({'float':'right','margin-right':'20px'});
   		}
   		
   	});
   	
   	//USE BY THE LOOK
   	
   	$('.buy-look').click(function(){
   		var _img=$(this).attr('data-img-src');//'http://myntra.myntassets.com/skin2/images/look_main7_360x480.jpg';
   		var _styles=[];
   		$(this).closest('.look-styles').find('ul li').each(function(){
   			_styles.push($(this).attr('data-style-id'));
   		});
   		Myntra.BuyLook.show({photo:_img,styleIds:_styles,title:$(this).data('look-title'),_invoker:'sns_buylook'});
   		$(document).unbind('keydown.lookbook');
   		_gaq.push(['_trackEvent', 'starnstyle', 'sns_lb', 'buynow']);
   	});
   	
   	$(document).bind('buylook.close',function(){
   		$(document).bind('keydown.lookbook',lookKeyBind);
   	});
   	if(Myntra.Data.lookId && Myntra.Data.buy){
   		$('.'+lgLD+'#look-'+Myntra.Data.lookId).find('.style-display-control').trigger('click');
   		$('.'+lgLD+'#look-'+Myntra.Data.lookId).find('.buy-look').trigger('click');
   	}
}

Myntra.InitStarDiary = function(){
	$('.star-diary-carousel').booklet({
		width:980,
		height:480,
		arrows:true,
		hovers:false,
		hoverWidth:0,
		change:trackClick
	}).show();

	function trackClick(event, data){
		if(data.options.movingForward){
			_gaq.push(['_trackEvent', 'starntyle', 'sns_sd', 'next']);
		}
		else {
			_gaq.push(['_trackEvent', 'starntyle', 'sns_sd', 'previous']);
		}
	}
}	



//call depends on the page
switch(lgPage){
	case "lb":
		Myntra.InitLookBook();
		break;
	case "sns":
		Myntra.InitStarNStyle();
		break;
	case "sd":
		Myntra.InitStarDiary();
		break;
	case "st":
		break;

}
});


$(window).load(function(){
    $('#all-looks').find('a').each(function(index){
        $(this).css('background','transparent url('+Myntra.Data.lookThumbs[index]+') no-repeat 0 0');
    });
	
    //LAZY LOAD Script
	if(lgPage=='lb'){
		$.getScript(('https:' == document.location.protocol ? 'https://' : 'http://') + 'apis.google.com/js/plusone.js');
		//loop through looks
		$('.lg-social-btns').each(function(index){
			$(this).find('.fb').html('<fb:like href="' + window.location.href.split("?")[0] + '?utm_source=facebook&utm_medium=look&look=' + $(this).attr("data-look-id") + '" send="false" layout="button_count" width="80" action="like"></fb:like>');
			$(this).find('.gp').html('<g:plusone href="' + window.location.href.split("?")[0] + '?utm_source=gplus&utm_medium=look&look=' + $(this).attr("data-look-id") + '" size="medium" count="true"></g:plusone>');
		});
	}	
});