
/*
 * BuyLook lightbox
 *
 * Usage:
 * Myntra.BuyLook.show(args);
 * Myntra.BuyLook.hide();
 *
 * args: object
 * photo [optional] : image url or canvas object
 * styleIds : array of style ids
 * data [optional] : object of all the style data indexed by style id
 *
 * files:
 * js/buy-look.js
 * css/buy-look.css
 *
 */
(function() { "use strict";

var formatNum = Myntra.Utils.formatNumber,
tplBox = [
'<div id="buy-look" class="lightbox buy-look">',
'   <div class="mod">',
'       <div class="bd">',
'           <div class="photo"></div>',
'           <div class="details">',
'               <h2><span class="lbl">buy the Look  </span><span class="name"></span></h2>',
'               <div class="items-wrap"><ol class="items"></ol></div>',
'               <div class="notes">',
'                   <span class="sel-count"></span>',
'                   <span class="total"></span>',
'               </div>',
'               <div class="err-msg"></div>',
'               <button class="btn primary-btn btn-proceed">Add To BAG</button>',
'               <form method="post" action="/mkretrievedataforcart.php">',
'                   <input type="hidden" name="skuIds">',
'                   <input type="hidden" name="fromBuyLook" value="1">',
'                   <input type="hidden" name="_token" value="', Myntra.Data.token, '">',
'               </form>',
'           </div>',
'       </div>',
'   </div>',
'</div>'
].join(''),

tplItem = [
'<li class="item" data-styleid="{styleId}">',
'   <div class="check">',
'       <input type="checkbox" name="cbox" checked="checked">',
'   </div>',
'   <img src="{image}">',
'   <div class="size"></div>',
'   <p class="price">Rs. {price}</p>',
'   <a href="/{url}" target="_blank" class="title"><em>{brand} </em>{title}</a>',
'</li>'
].join('');

Myntra.BuyLook = {
    init: function() {
    	console.log('init');
    	this.root = $(tplBox).appendTo('body');
        this.btnProceed = this.root.find('.btn-proceed').on('click', $.proxy(function(e) { this.checkout() }, this));
        this.itemsWrap = this.root.find('.items-wrap');
        this.items = this.root.find('.items').on('click', 'li', $.proxy(this.onItemClick, this));
        this.lbox = Myntra.LightBox('#buy-look');
        this.selCount = this.root.find('.sel-count');
        this.total = this.root.find('.total');
        this.errMsg = this.root.find('.err-msg');
        this.form = this.root.find('form');
        this.jsp = null;
        this.data = null;
        this._invoker = 'buylook';
        this._invokerPDPStyle = '';
        this._initalized = true;
        this.totalMrp=null;
        this.noimage = false; 
        
    },
    show: function(args) {
    	!this._initalized && this.init();
        if(args.title){
            this.root.find('h2 > .name').html(' / '+args.title);    
        }
        if(args.noimage){
            this.noimage = true;
            this.root.find('.mod').css('width', 500).find('.photo').hide();
        }
        else{
            this.noimage = false;
        }         
        if (typeof args.canvas !== 'undefined') {
            this.root.find('.mod').css('width', 800).find('.photo')
                .html('<canvas width="360" height="480">')
                .show()
                .find('canvas').get(0).getContext('2d').drawImage(args.canvas, 0, 0)
            ;
        }
        else if (typeof args.photo === 'string') {
            this.root.find('.mod').css('width', 800).find('.photo')
                .html('<img src="' + args.photo + '">').show();
        }

        this.items.html('');
        this.errMsg.html('');
        this.itemsCount = 0;
        this.totalAmount = 0;
        this.totalMrp = 0;
        this.selectedCount = 0;
        this.soldOutCount = 0;
        this._invoker = args._invoker || 'buylook'; // For the GA category
        this._invokerPDPStyle = args._style || '';
        this.lbox.show();
        if (args.data) {
            this.data = args.data;
            this.renderItems();
        } else {
            this.ajax(args.styleIds);
        }
        _gaq.push(['_trackEvent', this._invoker, 'show']);
    	
        //trigger event to handle anything when you close the buy look overlay
    	this.lbox.beforeHide=function(){
    		$(document).trigger('buylook.close');
    	}
        
    },
    hide: function() {
        this.lbox && this.lbox.hide();
        _gaq.push(['_trackEvent', this._invoker, 'hide']);
    },
    renderItems: function() {
        var data, styleId, markup, item;
        for (styleId in this.data) {
            data = this.data[styleId];
            data.price = +data.price;
            markup = tplItem.replace(/\{(.*?)\}/g, function(str, key) {
                if (key in data) {
                    return key === 'price' ? formatNum(data[key]) : data[key];
                }
                return str;
            });
            item = $(markup).appendTo(this.items);
            if (typeof(data.mrp) !== 'undefined') {
                data.mrp = +data.mrp;
                $('<span class="mrp">' + data.mrp + '</span>').appendTo(item.find('.price'));
            }
            item.find('.size').html(this.buildSkusMarkup(styleId));
            if(item.find('.sold').length){
            	//do not increase the count do not sum price
            	item.addClass('disabled unselected');
            	item.find('input[name="cbox"]').prop('checked',0).prop('disabled',1);
            	this.soldOutCount +=1;
              }
              else{
            	this.totalAmount += data.price;
            	this.totalMrp += (data.mrp)?data.mrp:data.price;
            	this.selectedCount += 1;
              }
                this.itemsCount += 1;
            }
        //disable button as all products in the look are sold out
        this.btnProceed.prop('disabled',(this.soldOutCount==this.itemsCount));
        this.updateSelectionInfo();

        if (this.itemsCount > 4) {
            var _h=(this.items.find('li').height()*4)+4;
            this.itemsWrap.css('height',_h+'px');
        	this.jsp = this.itemsWrap.jScrollPane();
            this.itemsWrap.find('.jspTrack').show();
        }
        else
        {	if(this.itemsWrap.find('.jspTrack').length){
        		var _h=(this.items.find('li').height()*this.itemsCount)+this.itemsCount;
        		this.itemsWrap.css({'height':_h+'px'});
        		this.itemsWrap.find('.jspPane').css({'top':'0','width':'100%'});
        		this.itemsWrap.find('.jspTrack').hide();
        	}
        }
        if(this.noimage){
            this.root.find('.mod .bd').css({'min-height':0});
            this.lbox.center();
            this.root.find('.item a.title').css({'width':281});
            this.root.find('.item .size').css({'left':176});
        }else{
            this.root.find('.item a.title').css({'width':215});
            this.root.find('.item .size').css({'left':142});
        }
    
      },
    updateSelectionInfo: function() {
        this.selCount.html('<em>' + this.selectedCount + '</em> of <em>' + this.itemsCount + '</em> items selected');
        this.total.html('TOTAL Rs. ' + formatNum(this.totalAmount)+'<span class="strike"> '+((this.totalMrp!=this.totalAmount)?+""+this.totalMrp:"")+'</span>');
    },
    buildSkusMarkup: function(styleId) {
        var skusAll = this.data[styleId].skus, sku, opts = [];
        for (var i = 0, n = skusAll.length; i < n; i += 1) {
            sku = skusAll[i];
            if (!sku[2]) { continue; }
            if (sku[1].toLowerCase() === 'freesize' || sku[1].toLowerCase() === 'onesize') {
                return '<span class="free" data-value="' + sku[0] + '">'+sku[1]+'</span>';
            }
            opts.push('<option value="' + sku[0] + '">' + sku[1] + '</option>');
        }
        
        if(!opts.length){
        	return '<span class="sold">sold out</span>';	
        }else{
        	return '<span class="icon"></span><select><option value="">SIZE</option>' + opts.join('') + '</select>';
        	
        }
        
    },
    ajax: function(styleIds) {
        var that = this,
        xhr = $.ajax({
            url: '/myntra/ajax_getsizes.php?ids=' + styleIds.join(',') + '&output=all',
            dataType: "json",
            beforeSend: function() {
            	that.lbox.showLoading();
            },
            success: function(res, status, xhr) {
                if (!res) {
                    throw new Error('[BuyLook.ajax] No response from the Server!');
                }
                if (res.status === 'SUCCESS') {
                    that.data = res.data;
                    that.renderItems();
                }
            },
            error: function(xhr, status, msg) {
                if (status === 'parsererror') {
                    alert('[BuyLook.ajax] Invalid JSON!');
                }
            },
            complete: function(xhr, status) {
                that.lbox.hideLoading();
            }
        });
    },
    onItemClick: function(e) {
        var target = $(e.target),
        el = $(e.currentTarget),
        styleId = el.data('styleid'),
        tag = target.prop('tagName'),
        cbox = el.find('input[name="cbox"]'),
        sbox = el.find('select'),
        data = this.data[styleId];

        if (el.hasClass('disabled') || tag !== 'INPUT') {
        	return;
        }
        
        /*
        if (tag === 'SELECT' || tag === 'OPTION') {
            var val = target.attr('value');
            if (val.length) {
                sbox.removeClass('err');
                sbox.prev('.icon').css('display', 'inline-block');
            } else {
                sbox.addClass('err');
                sbox.prev('.icon').css('display', 'none');
            }
            return;
        }
        */

        var checked = cbox.prop('checked');
        if (tag !== 'INPUT') {
            // toggle the checked status of the check box
            checked = !checked;
            cbox.prop('checked', checked);
        }

        if (checked) {
        	el.removeClass('unselected');
        	this.totalAmount += data.price;
        	this.totalMrp += (data.mrp)?data.mrp:data.price;
            this.selectedCount += 1;
            //sbox.length && !sbox.val('').length && sbox.addClass('err');
        } else {
            //update tyhe style of item class
        	el.addClass('unselected');
        	sbox.removeClass('err');
            this.totalAmount -= data.price;
            this.totalMrp -= (data.mrp)?data.mrp:data.price;
            this.selectedCount -= 1;
            //sbox.removeClass('err');
        }
        
        this.updateSelectionInfo();
        this.btnProceed.prop('disabled', (this.selectedCount <= 0));
    },
    checkout: function() {
        var unselectedCount = 0, skuIds = [], msg,soldoutSelected=0;
        this.items.find('li').each(function(i, el) {
        	el = $(el);
        	 if(el.find('.sold').length && !el.hasClass('unselected')){
             	soldoutSelected=1;
             }
            if (el.find('input[name="cbox"]').prop('checked')) {
                var sel = el.find('select');
                if (!sel.length) {
                    skuIds.push(el.find('.free').data('value'));
                    return;
                }
                if (sel.val()) {
                    sel.removeClass('err');
                    skuIds.push(sel.val());
                } else {
                    sel.addClass('err');
                    unselectedCount += 1;
                }
            }
         
           
        });
        if (unselectedCount) {
            msg = 'Please select a size for ' + unselectedCount + (unselectedCount === 1 ? ' item' : ' items');
            this.errMsg.html(msg).show();
        }else if(soldoutSelected){
        	 msg = 'Please unselect sold out items';
             this.errMsg.html(msg).show();
        }        
        else {
            this.form.find('input[name="skuIds"]').val(skuIds.join(',')).end().submit();
            this._invokerPDPStyle?_gaq.push(['_trackEvent', this._invoker, 'add-to-cart',this._invokerPDPStyle]):_gaq.push(['_trackEvent', this._invoker, 'add-to-cart']);
        }
    }
};

}()); // end of "use strict" wrapper

