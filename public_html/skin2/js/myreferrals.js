var counter = 0;
var status = '';
var regcounter = 0;
var resendcounter = 0;
var email_list = '';
var email_ctr = 0;
var email_list_arr = new Array();
var email_list_arr_ctr = 0;
var referralDefault = "Please enter email addresses separated by commas";

$(".invite-friends").live('click', function() {
	counter = 0;
	status = '';
	regcounter = 0;
	resendcounter = 0;
	email_list = '';
	email_ctr = 0;
	email_list_arr = new Array();
	email_list_arr_ctr = 0;
	var inviteForm = $("#invite-form");
	var recipients = $("#recipient_list").val();
	$(".mk-invite-form-message").html("");
	$(".mk-invite-form-message").removeClass("error").removeClass("success");
	if ($.trim(recipients) == '') {
		$(".mk-invite-form-message").html("Please enter email ids").addClass("error");
		return;
	}
	if (($('#invite-form input[name="updateusername"]') == "true" && ($.trim($('#invite-form input[name="username"]').val()) == '') || 
		$.trim($('#invite-form input[name="username"]').val()) == $('#invite-form input[name="username"]').attr("data-placeholder"))) {
		$(".mk-invite-form-message").html("Please enter name").addClass("error");
		return;
	}
	$(".mk-invite-form-loader").show('slow', function(){} );
	//$("#recipient_list").val("");
	var recipientsArr = recipients.split(/[\n\r;,]+/);
	for (i in recipientsArr) {
		recipient = recipientsArr[i].replace(/^\s+|\s+$/g, "");
		//Value += perInviteProgress;
		// empty.
		if (recipient == "") {
			//$("#progressbar").progressbar({ value: Value });
			continue;
		}
		var email;
		if (recipient.search("<") == - 1) {
			// only email present.
			email = recipient;
		}
		else {
			// name and email present.
			email = recipient.substring(recipient.search("<") + 1, recipient.search(">"));
		}
		email_list += email + ',';
		if (email_ctr % 100 == 0 && email_ctr != 0) {
			email_list_arr[email_list_arr_ctr] = email_list;
			email_list = '';
			email_list_arr_ctr++;
		}
		email_ctr++;
	}
	if (email_list != '') email_list_arr[email_list_arr_ctr] = email_list;
	var l = 1;
	var offset = Math.ceil(email_ctr / 100);
	var p_offset = 100 / offset;
	sendEmailInBatches(email_list_arr, p_offset, l);
});
function sendEmailInBatches(email_list_arr, p_offset, l) {
	var formdata = "";
	if($(".mk-invite-form-username-update").val() == "true"){
		formdata="&mode=invite-multiple-friends&email_list=" + email_list_arr[l - 1] + '&_token=' + $('#invite-form input[name="_token"]').val() + '&updateusername=true&username=' + $('#invite-form input[name="username"]').val();
	}
	else{
		formdata="&mode=invite-multiple-friends&email_list=" + email_list_arr[l - 1] + '&_token=' + $('#invite-form input[name="_token"]').val() + '&updateusername=false';
	}
	$.ajax({
		type: "POST",
		url: "mymyntra.php?view=myreferrals",
		data: formdata,
		async: false,
		success: function(data) {
			var msg = jQuery.parseJSON(data);
			
			status = msg.status;
			if(status == "success"){
				counter = counter + msg.sent;
				regcounter = regcounter + msg.already_registered;
				resendcounter = resendcounter + msg.already_sent;

				var prog_status = l * p_offset;
				//alert(prog_status);
				//console.log(prog_status+"==="+p_offset+"====="+l);
				l++;
				var html_status_msg = Math.ceil(prog_status);
				if (html_status_msg > 100) html_status_msg = 100;
				//$("#p_status").html(html_status_msg);
				if (l <= email_list_arr.length) {
					sendEmailInBatches(email_list_arr, p_offset, l);
				}
				else {
					var msg = "Successfully sent " + counter + " invitation(s)!";
					_gaq.push(['_trackEvent', 'my_referrals', 'send_invites', 'success', counter]);
					if (regcounter > 0) {
						msg += "<br/>" + regcounter + " of the above contact(s) have already registered.";
						_gaq.push(['_trackEvent', 'my_referrals', 'send_invites', 'failure_user_registered', regcounter]);
					}

					if (resendcounter > 0) {
						msg += "<br/>" + resendcounter + " of the above contact(s) were sent an email earlier today.";
						_gaq.push(['_trackEvent', 'my_referrals', 'send_invites', 'failure_user_referred_today', resendcounter]);
					}
					$(".mk-invite-form-message").html(msg).removeClass("error").addClass("success");
					$(".mk-invite-form-loader").hide();
					$("#recipient_list").val("");

					if (counter > 0) {
						// refresh the myrefferals page
						/*$.ajax({
							type: "POST",
							url: http_loc + "/myreferrals.php",
							data: "mode=myreferrals",
							success: function(msg) {
								var data = msg.split("#####");
								$("#" + data[1]).html(data[0]);
								if (data[1] == 'myreferrals_tab') {
									// When page loads...
									$(".reff-content").hide(); // Hide all content
									$("ul.right-mytabs li:first").addClass("active"); // Activate
									// first
									// tab
									$(".reff-content:first").show(); // Show first tab
									// content
									toggle_text_all(".toggle-text");
								}
							}
						});*/
					}
				}
			}
			else{
				$(".mk-invite-form-message").html(msg.error).removeClass("success").addClass("error");
			}
		}
	});
}


// Function to share referral link on facebook.
function fb_share_referral(login) {
	_gaq.push(['_trackEvent', 'my_referrals', 'fb_referral_share', 'click']);
	var uri = http_loc + "/register.php?ref=" + login + "&utm_source=MRP&utm_medium=FBpost&utm_campaign=refjoin";
	window.open("http://www.facebook.com/sharer.php?u=" + encodeURIComponent(uri), '', 'width=600,height=400,left=100,top=100');
}

// Share referral link on twitter.
function twitter_share_referral(login, num_coupons, value_coupons) {
	//login = login.replace("@", "%40");
	// login = login.replace("%", "%25");
	_gaq.push(['_trackEvent', 'my_referrals', 'twitter_referral_share', 'click']);
	var longUrl = encodeURIComponent(http_loc + "/register.php/?ref=" + login + "&utm_source=MRP&utm_medium=Twitterpost&utm_campaign=refjoin");
	var shortUrl = longUrl; // in case of error condition
	$.ajax({
		url: http_loc + "/urlshortener.php",
		//this is the php script above
		dataType: "json",
		type: "POST",
		async: false,
		data: {
			url: longUrl
		},
		success: function(data) {
			if (data.status_txt === "OK") {
				shortUrl = data.data.url;
			}
		},
		error: function(xhr, error, message) {
			//no success, fallback to the long url
			shortUrl = longUrl;
		}
	});
	
	num_coupons = (num_coupons == 1)?"a":num_coupons;
 	
 	if(value_coupons.search('%') == -1){
 		value_coupons = value_coupons +' rupees';
 	}

	var tweettext = encodeURIComponent("Hi, You have "+ num_coupons +" 20% discount voucher from @Myntra. All you've to do is register at "+shortUrl+ " #notspam");
	window.open("http://twitter.com/intent/tweet?text="+tweettext);
}

function showUsernameMissingWindow() {
	$("#username_missing_div").css("display", "block");
	$("#UsernameMissingContent").css("display", "block");
	$("#TB_title").css("display", "block");
}

function closeUsernameMissingWindow() {
	$("#username_missing_div").css("display", "none");
	$("#UsernameMissingContent").css("display", "none");
}
function updateUsername() {
	var username = $.trim($("#new_fullname").val());
	if (username == '') {
		alert("please enter full name");
		return;
	}
	$.ajax({
		type: "POST",
		url: http_loc + "/mymyntra.php?view=myreferrals",
		data: "_token=" + Myntra.Data.token + "&mode=update-profile-username&username=" + username,
		success: function(msg) {
			closeUsernameMissingWindow();
			$(".invite-friends").trigger('click');
		}
	});
}

function resendFriendInvite(email) {
	$(".mk-resend-notifications").html("").removeClass("success").removeClass("error");
	$.ajax({
		type: "POST",
		url: "mymyntra.php?view=myreferrals",
		data: "_token=" + Myntra.Data.token + "&mode=invite-friends&email=" + email,
		async: false,
		success: function(msg) {
			msg = $.trim(msg);
			if (msg == "success") {
				_gaq.push(['_trackEvent', 'my_referrals', 'send_invites', 'success']);
				$(".mk-resend-notifications").html("Successfully sent invitation").addClass("success");
			} else if (msg == "custfailure") {
				_gaq.push(['_trackEvent', 'my_referrals', 'send_invites', 'failure_user_registered']);
				$(".mk-resend-notifications").html("Invitee has already registered").addClass("error");
			} else if (msg == "timestampfailure") {
				_gaq.push(['_trackEvent', 'my_referrals', 'send_invites', 'failure_user_referred_today']);
				$(".mk-resend-notifications").html("Email sent earlier today").addClass("error");
			}
			if (msg == "success") {
				// refresh the myrefferals page
				/*$.ajax({
					type: "POST",
					url: http_loc + "/myreferrals.php",
					data: "mode=myreferrals",
					success: function(msg) {
						var data = msg.split("#####");
						$("#" + data[1]).html(data[0]);
						if (data[1] == 'myreferrals') {
							// When page loads...
							$(".reff-content").hide(); // Hide all content
							$("ul.right-mytabs li:first").addClass("active"); // Activate
							// first
							// tab
							$(".reff-content:first").show(); // Show first tab
							// content
						}
					}
				});*/
			}

		}
	});

}

function cleanTextArea() {
 	$("#recipient_list").val('');
}

$(document).ready(function() { 
	if($(".mk-referrals-page").length){
		$(".mk-invite-form-username").bind("click focus", function(){
			 if($(this).val() == "" || $(this).val() == $(this).attr("data-placeholder")){
				 $(this).val('');
			 }
		 });
		 
		 $(".mk-invite-form-username").bind("blur", function(){
			 if($(this).val() == ""){
				 $(this).val($(this).attr("data-placeholder"));
			 }
		 });
		 
		 $(".mk-trigger-plaxo").bind("click", function(e){
			 cleanTextArea();
			 _gaq.push(['_trackEvent', 'my_referrals', 'add_contacts', 'click']);
			 showPlaxoABChooser('recipient_list', '/skin2/my/plaxo.htm');
			 e.preventDefault();
			 return false;
		 });
		 
		 $(".mk-referral-invite-tabs").tabs();
		 
		 $(".resend-invite-link").live("click", function(e){
			 resendFriendInvite($(this).attr("data-referred"));
			 e.preventDefault();
			 return false;
		 });
	}
});
