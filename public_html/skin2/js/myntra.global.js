/* Global JS
(myntra.global.js)
------------------------------------ */

$(document).ready(function() {
	// Coupon ---------------------
	$('span.mk-btn-coupon').toggle(function() {
		$('.mk-coupon-trigger').animate({ left: '+=100'}, 250);
	}, function() {
		$('.mk-coupon-trigger').animate({ left: '-=100'}, 250);
	});

	/*if(Myntra.Data.shoppingFestivalEnabled) {
		var festivalHeader = $("<div />").addClass('mk-festival-header');
		var header = $("<a />").attr('href','/hot-in-december').addClass('mk-festival-header-center');
	    var leftImage = $("<a />").attr('href','/hot-in-december').addClass('mk-festival-header-left');
	    var rightImage = $("<a />").attr('href','/hot-in-december').addClass('mk-festival-header-right');
		//add festival header
	    $(festivalHeader).append($(header));
	    $(festivalHeader).append($(leftImage));
	    $(festivalHeader).append($(rightImage));
		$('.mk-body').prepend($(festivalHeader));
	}*/
});

Myntra.loadLazyElements = function() {
    if( typeof Myntra.Data.lazyElems != "undefined" && Myntra.Data.lazyElems.length > 0){
        Myntra.Data.elementsProcessed = Myntra.Data.elementsProcessed || [];
        for(var index in Myntra.Data.lazyElems){
			var elemdata = Myntra.Data.lazyElems[index];
            // Remove the duplicates out
            if($.inArray(elemdata, Myntra.Data.elementsProcessed) != -1){
                continue;
            }
            Myntra.Data.elementsProcessed.push(elemdata);
            if($.trim(elemdata).length != 0 && typeof(elemdata)=='string'){
                var index = elemdata.indexOf(Myntra.Data.lazySeparator);
                var elem = elemdata.substr(0, index);
    			var elemparent = elemdata.substr(index + Myntra.Data.lazySeparator.length);
                // Add child element and throw the custom change event to notify listerners
                $(elemparent).append(elem).trigger("lazy.change");
            }
		}
	}
};

Myntra.Utils.loadFestivalColumns = function () {
		if(Myntra.Data.shoppingFestivalEnabled && $('.mk-site-nav').length) {
		var leftColumn = $("<div />");
		var rightColumn = $("<div />");	
		
		//add left and right columns 
		$(leftColumn).addClass('mk-left-festival-column');
		$(rightColumn).addClass('mk-right-festival-column');
		if(Myntra.Data.festivalColumnImageLink) {
			$(leftColumn).css('background-image','url('+Myntra.Data.festivalColumnImageLink+ ')');
			$(rightColumn).css('background-image','url('+Myntra.Data.festivalColumnImageLink+ ')');
		}		
		$('.mk-wrapper').css('position','relative').prepend($(leftColumn));
		$('.mk-wrapper').append($(rightColumn));
	}
};

//Myntra.emarkC=null;
$(window).load(function() {
    Myntra.Utils.loadImageQueue();
    Myntra.Utils.loadLoginBg();
    Myntra.loadLazyElements();    

    // GTM load
    if (Myntra.Data.gtmLazyLoadEnabled) {
    	$('body').append('<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-H34B" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>' + 
    		"<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-H34B');</script>");
    }
});
