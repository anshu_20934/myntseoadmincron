
function formatNumber(num) {
    num = num.toFixed(2).split(".")[0];
    if (num.length <= 3) { return num; }
    var m = /(\d+)(\d{3})$/.exec(num);
    var inp = m[1];
    var last = m[2];
    var out = [];
    if (inp.length % 2 === 1) {
        out.push(inp.substr(0,1));
        inp = inp.substr(1);
    }
    var rx = /\d{2}/g;
    while (m = rx.exec(inp)) {
        out.push(m[0]);
    }
    out.push(last);
    return out.join(',');
};

function getCardType(cardNumber){
    cardNumber = cardNumber.toString();
    var tempCardNumber=cardNumber;
    tempCardNumber=cardNumber.replace(/-/g,"");
    tempCardNumber=tempCardNumber.replace(/ /g,"");
    cardNumber=tempCardNumber.valueOf();
    if(cardNumber.match(/\D/)){
        return false ;
    }
    if(cardNumber===null||cardNumber.length<13||cardNumber.search(/[a-zA-Z]+/)!=-1){
        return false;
    }
    returnValue=true;
    if(returnValue){
        var cardIdentifiers={1:{identifier:[4],numlength:[13,14,15,16]},2:{identifier:[51,52,53,54,55],numlength:[16]},3:{identifier:[34,37],numlength:[15]},4:{identifier:[300,301,302,303,304,305,36,38],numlength:[14]},5:{identifier:[502260,504433,5044339,504434,504435,504437,504645,504753,504775,504809,504817,504834,504848,504884,504973,504993,508159,600206,603123,603845,622018,508227,508192,508125,508126],numlength:[16,17,18,19]}};

        var cardType=false;
        for(ruleId in cardIdentifiers){
            var temp=cardIdentifiers[ruleId];
            if($.inArray(cardNumber.length,temp.numlength)>-1){
                for(i=0;i<temp.identifier.length;i++){
                    if(cardNumber.substr(0,(temp.identifier[i]+"").length)==temp.identifier[i]){
                        return ruleId;
                        break;
                    }
                }
                if(ruleId==5){
                    cardType="others";
                }
            }
        }
        if(cardType){
            return cardType;
        }else{
            return false;
        }
    }
    return false;
}

function getCardTypeForFade(cardNumber){
    cardNumber = cardNumber.toString();
    var tempCardNumber=cardNumber;
    tempCardNumber=cardNumber.replace(/-/g,"");
    tempCardNumber=tempCardNumber.replace(/ /g,"");
    cardNumber=tempCardNumber.valueOf();
    if(cardNumber.match(/\D/)){
        return false ;
    }
    if(cardNumber===null||!cardNumber.length||cardNumber.search(/[a-zA-Z]+/)!=-1){
        return false;
    }
    var cardIdentifiers={1:{identifier:[4]},2:{identifier:[51,52,53,54,55]},3:{identifier:[34,37]},4:{identifier:[300,301,302,303,304,305,36,38]},5:{identifier:[502260,504433,5044339,504434,504435,504437,504645,504753,504775,504809,504817,504834,504848,504884,504973,504993,508159,600206,603123,603845,622018,508227,508192,508125,508126]}};
    var cardType=false;
    if(tempCardNumber.length>=4) {
        for(ruleId in cardIdentifiers){
            var temp=cardIdentifiers[ruleId];
            for(i=0;i<temp.identifier.length;i++){
                if(cardNumber.substr(0,(temp.identifier[i]+"").length)==temp.identifier[i]){
                    return ruleId;
                    break;
                }
            }
            if(ruleId==5){
                cardType="others";
            }
        }
        return cardType;
    }
    return false;
}

 function validateCard(cardValue){
         // accept only digits, dashes or spaces
                if (/[^0-9-\s]+/.test(cardValue)){
                     $('.err-card').html("Please enter valid card number");
 //                    $(".pay-btn").attr("disabled","disabled");
                    return false;
                }
                
                var nCheck = 0, nDigit = 0, bEven = false;
                cardValue = cardValue.replace(/\D/g, "");
             
                if(cardValue===null||cardValue.length<13){
                    $('.err-card').html("Please enter valid card number");
 //                   $(".pay-btn").attr("disabled","disabled");
                    return false;
                }
               
                for (var n = cardValue.length - 1; n >= 0; n--) {
                    var cDigit = cardValue.charAt(n),
                        nDigit = parseInt(cDigit, 10);

                    if (bEven) {
                        if ((nDigit *= 2) > 9) nDigit -= 9;
                    }

                    nCheck += nDigit;
                    bEven = !bEven;
                }

                var re = (nCheck % 10) == 0;
        
                if(nCheck==0)
                    re=false;
        
                 if(!re){
                    $('.err-card').html("Entered card number is invalid.");
  //                  $(".pay-btn").attr("disabled","disabled");  
                    return false;
                }

                return true;
    }

function showUserCardType(cardNumber) {
    var cardID = getCardType(cardNumber);
    var cardType = getCardTypeForFade(cardNumber);

    if(citi_discount>0) {
        if(cardID) {
            var bin = cardNumber.substring(0,6);
            if($.inArray(bin,citiCardArray) > -1) {
                var citiDiscount = amountToDiscount*citi_discount/100;
                citiDiscount = Math.round(citiDiscount);
                $(".order-summary .additional-discount").show().find("> .value").text("(-) Rs. " + formatNumber(citiDiscount));
                var finalAmount = total_amount - citiDiscount;
                finalAmount = finalAmount<0?0:finalAmount;
                updateTotal(finalAmount);
                $("#citi_discount_div").html("Congrats! A discount of <span style='color:#d20000'>Rs. " + formatNumber(citiDiscount) +"</span> has been applied");
            } else {
                $(".order-summary .additional-discount").hide();
                updateTotal(total_amount);
                $("#citi_discount_div").html('Avail ' + citi_discount +'% discount by paying with your Citibank <a href="#pay_by_dc" onClick="$(\'#pay_by_dc\').click();return false;">Debit Card</a>');
            }
        } else {
            $(".order-summary .additional-discount").hide();
            updateTotal(total_amount);
            $("#citi_discount_div").html('Avail ' + citi_discount +'% discount by paying with your Citibank <a href="#pay_by_dc" onClick="$(\'#pay_by_dc\').click();return false;">Debit Card</a>');
        }
    }

    if(icici_discount>0) {
        if(cardID) {
            var bin = cardNumber.substring(0,6);
            if($.inArray(bin,iciciCardArray) > -1) {
                var iciciDiscount = amountToDiscount*icici_discount/100;
                iciciDiscount = Math.round(iciciDiscount);
                $(".order-summary .additional-discount").show().find("> .value").text("(-) Rs. " + formatNumber(iciciDiscount));
                var finalAmount = total_amount - iciciDiscount;
                finalAmount = finalAmount<0?0:finalAmount;
                updateTotal(finalAmount);
                $("#icici_discount_div").fadeOut('slow',function(){
                    $("#icici_discount_div").css("background-color","#ffffcc");
                    $("#icici_discount_div").html("Congrats! A discount of <span style='color:#d20000'>Rs. " + formatNumber(iciciDiscount) +"</span> has been applied").fadeIn(3000,function() {
                        $("#icici_discount_div").css("background-color","#ffffff");
                    });
                });
            } else {
                $(".order-summary .additional-discount").hide();
                updateTotal(total_amount);
                $("#icici_discount_div").html(icici_discount_text);
            }
        } else {
            $(".order-summary .additional-discount").hide();
            updateTotal(total_amount);
            $("#icici_discount_div").html(icici_discount_text);
        }
    }

    if(cardType==1){
        //visa
        $(".master").animate({'opacity': '0.3'});
        $(".maestro").animate({'opacity': '0.3'});
        $('#debit_card .ostar').fadeIn();
        $('#debit_card .field-msg').animate({'opacity': '0'});
    } else if(cardType==2){
        //masterCard
        $(".visa").animate({'opacity': '0.3'});
        $(".maestro").animate({'opacity': '0.3'});
        $('#debit_card .ostar').fadeIn();
        $('#debit_card .field-msg').animate({'opacity': '0'});
    } else if(cardType==5){
        //maestro
        $(".visa").animate({'opacity': '0.3'});
        $(".master").animate({'opacity': '0.3'});
        $('#debit_card .ostar').fadeOut();
        $('#debit_card .field-msg').animate({'opacity': '1'});
    } else if(cardType==3 || cardType==4 || cardType=="others"){
        $(".cardType").animate({'opacity': '1'});
        $('#debit_card .ostar').fadeOut();
        $('#debit_card .field-msg').animate({'opacity': '1'});
    } else{
        $(".cardType").animate({'opacity': '1'});
        $('#debit_card .ostar').fadeOut();
        $('#debit_card .field-msg').animate({'opacity': '1'});
    }

    $('.err-card').html("");
  //  $(".pay-btn").removeAttr("disabled");
}


function validatePaymentForm(formID){
    if(formID=='net_banking') {
        if($("#netBanking").val() == '') {
            alert("Please select a valid Netbanking option");
            return false;
        }
    } 
    else if (formID=='credit_card'||formID=='emi') {
        var form = document.getElementById(formID);

        if($('#'+formID).find('input[name="use_saved_card"]').length && $('#'+formID).find('input[name="use_saved_card"]').val() == 'true'){

            if(!$('.card-selected').length){
                alert('please select a card to continue');
                return false;
            }
            else{
                var saved_cvv_code = $('#'+formID).find('.card-selected').closest('li.card').find('input[name="saved_cvv_code"]').val();
                $('#'+formID).find('input[name="saved_cvv_code"]').val('');
                $('#'+formID).find('.card-selected').closest('li.card').find('input[name="saved_cvv_code"]').val(saved_cvv_code);
                $('#'+formID).find('input[name="payment_instrument"]').val($('#'+formID).find('.card-selected').data('key'));
                if((/\D/.test(saved_cvv_code)) || saved_cvv_code.length < 3){
                    alert("Please enter a valid card CVV number");
                    return false;
                }
                else {
                    $('#'+formID).find('input[name="cvv_code"]').val(saved_cvv_code);
                }

            }
        }
        else {
            
            var card_number = form.card_number.value;

                var st =  validateCard(card_number);

               if(!st){
                return false;
               }

            var cardID = getCardType(card_number);
            if(!cardID){
                alert("Please enter a valid credit card number");
                return false;
            }
        
            if(formID=='emi') {
                var bin = card_number.substring(0,6);
                var bankName = form.selectedBank.value;
                if($.inArray(bin,Myntra.Payments.EMICardArray[bankName]) > -1) {
                
                } else {
                    alert("Card number provided is not eligible for EMI. Please pay using any other payment mode or try with a different card number.");
                    return false;
                }
            
                var cvv = form.cvv_code.value;
                if (cvv.length != 3 && bankName == "CITI") {
                    alert("Please enter a valid card CVV number");
                    return false;
                }
            }

            var card_month = form.card_month.value;
            if(card_month == null || card_month == '') {
               alert("Please select card expiry month");
               return false;
            }
            var card_year = form.card_year.value;
            if(card_year == null || card_year == '') {
                alert("Please select card expiry year");
                return false;
            }

            var cvv_code = $('#'+formID).find('.cc-form').find('input[name="cvv_code"]').val();
            /*Clear all the other CVV codes except the selected on*/
            $('#'+formID).find('input[name="cvv_code"]').val('');
            $('#'+formID).find('.cc-form').find('input[name="cvv_code"]').val(cvv_code);
            if((/\D/.test(cvv_code)) || cvv_code.length < 3){
                alert("Please enter a valid card CVV number");
                return false;
            }

            var bill_name = form.bill_name.value;
            if(bill_name==null || bill_name == '' ){
                alert("Please enter the name as it appears on card");
                return false;
            }
            if(Myntra.Data.OrderType != "gifting"){
                var b_firstname = form.b_firstname.value;
                if(b_firstname==null || b_firstname == '' || b_firstname == 'Name'){
                    alert("Please enter a billing name");
                    return false;
                }

                var b_address = form.b_address.value;
                if(b_address==null || b_address == '' || b_address == 'Enter your billing address here'){
                    alert("Please enter a billing address");
                    return false;
                }

                var b_city = form.b_city.value;
                if(b_city==null || b_city == '' || b_city == 'City'){
                    alert("Please enter a billing city");
                    return false;
                }

                var b_state = form.b_state.value;
                if(b_state==null || b_state == '' || b_state == 'State'){
                    alert("Please enter a billing state");
                    return false;
                }

                var b_zipcode = form.b_zipcode.value;
                if(b_zipcode==null || b_zipcode == '' || b_zipcode == 'PinCode' ||(/\D/.test(b_zipcode))){
                    alert("Please enter a valid PinCode/ZipCode");
                    return false;
                }
            }
        }
    } else if (formID=='debit_card') {

        var form = document.getElementById(formID);
        
        if($('#'+formID).find('input[name="use_saved_card"]').length && $('#'+formID).find('input[name="use_saved_card"]').val() == 'true'){
            if(!$('.card-selected').length){
                alert('please select a card to continue');
                return false;
            }
            else{
                var saved_cvv_code = $('#'+formID).find('.card-selected').closest('li.card').find('input[name="saved_cvv_code"]').val();
                $('#'+formID).find('input[name="saved_cvv_code"]').val('');
                $('#'+formID).find('.card-selected').closest('li.card').find('input[name="saved_cvv_code"]').val(saved_cvv_code);
                $('#'+formID).find('input[name="payment_instrument"]').val($('#'+formID).find('.card-selected').data('key'));
                if((/\D/.test(saved_cvv_code)) || saved_cvv_code.length < 3){
                    alert("Please enter a valid card CVV number");
                    return false;
                }
                else {
                    $('#'+formID).find('input[name="cvv_code"]').val(saved_cvv_code);
                }

            }
        }
        else {
            // /console.log('use new form');return false;
           var card_number = form.card_number.value;
           var st =  validateCard(card_number);

               if(!st){
                return false;
               }
               
           var cardID = getCardType(card_number);
           if(!cardID){
                alert("Please enter a valid debit card number");
                return false;
           }

          if(cardID==1 || cardID == 2) {
            var card_month = form.card_month.value;
            if(card_month == null || card_month == '') {
                alert("Please select card expiry month");
                return false;
            }

            var card_year = form.card_year.value;
            if(card_year == null || card_year == '') {
                alert("Please select card expiry year");
                return false;
            }

            var cvv_code = $('#'+formID).find('.cc-form').find('input[name="cvv_code"]').val();
            $('#'+formID).find('input[name="cvv_code"]').val('');
            $('#'+formID).find('.cc-form').find('input[name="cvv_code"]').val(cvv_code);
            if((/\D/.test(cvv_code)) || cvv_code.length < 3){
                alert("Please enter a valid card CVV number");
                return false;
            }
          }

           var bill_name = form.bill_name.value;
           if(bill_name==null || bill_name == '' ){
             alert("Please enter the name as it appears on card");
             return false;
           }
      }

    } 
    else if(formID=='one-click-credit_card'){
        //validate CVVvar cvv_code = form.cvv_code.value;
          var form = document.getElementById(formID);
          var cvv_code = form.cvv_code.value;
            if((/\D/.test(cvv_code)) || cvv_code.length < 3){
                alert("Please enter a valid card CVV number");
                return false;
            }
        
    }
    else if(formID=='one-click-cod'){
        return true;
    }
    else if (formID=='cashcard') {
        return true;
    } else if(formID=='cod') {
        return true;
    }
    return true;
}
//WRAPPER FOR PAYMENT SUBMIT TO TACKLE EXPRESS PAYMENT - NEW USER ADDRESS FIELD VALIDATION

function submitPaymentForm(validate, isOtherCards){
    if(Myntra.Data.promptLogin){
        Myntra.expiryLoginPrompt();
        return false;
    }
	var elem = $('#paymentFormToSubmit').val();
	var form = $('#'+elem);
	if(isOtherCards){
		form.find('input[name="other_cards"]').val('true');
	    copycontactaddress();
	}
	else{
	    form.find('input[name="other_cards"]').val('false');
	}
	
	if(Myntra.Data.pageName=='expresss_payment'){
            if(!hasAddress){
                $(document).trigger('myntra.address.form.save');    
            } else {
                $(document).trigger('express.payment.submit',{hasAddressId:true, validate:validate});
            }
        
        } else {
            return validateAndSubmitPaymentForm(validate);
        }
}

function validateAndSubmitPaymentForm(validate){
    
    var elem = $('#paymentFormToSubmit').val();

    var form = $('#'+elem);

    if(validate){
        if(validatePaymentForm(elem)){

        } else{
            return false;
        }
    }
        
    if(validate){
        _gaq.push(['_trackEvent', 'payment_page', 'proceed_to_pay', elem]);
    } else {
        _gaq.push(['_trackEvent', 'payment_page', 'use_intl_creditcard']);
    }
    if(total_amount>0) {
        var wdh=800;//0.75*screen.width;//800
        var hgt=400;//0.75*screen.height;//400
        var st="location=1,status=1,scrollbars=1,top=150,left=200,width="+wdh+",height="+hgt;
        paymentChildWindow=window.open('',"MyntraPayment",st);

        if(paymentChildWindow!=null){
            var html = '<html><head><title>Myntra Payment</title></head><body><div><center>';
            html = html + '<h2>Processing Payment</h2>';
            html = html + '<img src="https://d6kkp3rk1o2kk.cloudfront.net/images/image_loading.gif">';
            html = html + '<div>Enter your card/bank authentication details in the window once it appears on your screen. <br/>';
            html = html + 'Please do not refresh the page or click the back button of your browser.<br/>';
            html = html + 'You will be redirected back to order confirmation page after the authentication.<br/><br/></div>';
            html = html+ '</center></div></body></html>';
            try {
                paymentChildWindow.document.open();
                paymentChildWindow.document.write(html);
                paymentChildWindow.document.close();
            } catch (err){

            }
            paymentChildWindow.focus();
            if(checkPaymentWindow !=null) checkPaymentWindow = window.clearInterval(checkPaymentWindow);
            checkPaymentWindow = window.setInterval("checkClose()",1000);
            if (window.addEventListener) {
                window.addEventListener('message', receiveMessage, false); 
            } else if (window.attachEvent){
                window.attachEvent('onmessage', receiveMessage);
            }
            $(window).focus(function() {
                paymentChildWindow.focus();
            });
        }
        
        cfg = {"isModal":true,"disableCloseButton":true};
        if(paymentProcessingPopup==null)paymentProcessingPopup = Myntra.LightBox('#splash-payments-processing',cfg);
        paymentProcessingPopup.show();
    }
    form.submit();
    return false;
}
