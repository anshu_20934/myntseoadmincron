$(document).ready(function(){
    
	function rp_val() {
		var newPasswordVal = $("#rp-password").val();
    	if(newPasswordVal == "") {
    		$(".pw-reset .rp-status").html("Enter a valid Password");
    		return false;
    	}
    	else if(newPasswordVal.length < 6) {
    		$(".pw-reset .rp-status").html("Minimum 6 characters required");
    		return false;
    	}
    	else {
    		$(".pw-reset .rp-status").html("");
    		return true;
		}
	}

	function rp_conf_val() {
		var newPasswordVal = $("#rp-password").val();
    	var confirmnewPasswordVal = $("#confirm-password").val();
    	if(newPasswordVal == "" || newPasswordVal.length < 6 || newPasswordVal != confirmnewPasswordVal) {
    		$(".pw-reset .confirm-status").html("Passwords must match");
    		return false;
    	}
    	else {
    		$(".pw-reset .confirm-status").html("");
    		return true;
    	}
	
	}

	$("#rp-password").blur(rp_val);
	$("#confirm-password").blur(rp_conf_val);

	$("#reset-submit").live("click", function() {
		 if(rp_val()&& rp_conf_val()) {
			 $('#reset_form').trigger('submit');
		 }
		 else {return false;}
	});
});
