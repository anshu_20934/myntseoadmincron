
/* Colour Options Begins */
Myntra.Search.Utils.initColourOptions = function(){
	$('#mk-search-results,.lp-section').on('mouseenter', 'li.mk-product .colours-lbl, li.mk-product .mk-colour', function(e) {
		var context = $(this).closest('li.mk-product'), 
			colours = context.find('.mk-colour'),
			fromElem = e.relatedTarget || e.fromElement;
		if (colours.length) {
			if (!$(fromElem).closest('.colours-lbl').length && !$(fromElem).closest('.mk-colour').length) {
				context.trigger('mouseleave').css('z-index', 1);
				context.find('.colours-lbl').addClass('mk-hover');
				colours.fadeIn('fast');
				Myntra.Utils.initCarousel(context, 3);
			}
		}
	}).on('mouseleave', 'li.mk-product .colours-lbl, li.mk-product .mk-colour', function(e) {
		var context = $(this).closest('li.mk-product'), 
			colours = context.find('.mk-colour'),
			toElem = e.relatedTarget || e.toElement;
		if (colours.length) {
			if (!$(toElem).closest('.colours-lbl').length && !$(toElem).closest('.mk-colour').length) {
				context.trigger('mouseenter');
				context.find('.colours-lbl').removeClass('mk-hover');
				colours.fadeOut('fast');
			}
		}
	}).on('click', 'li.mk-product .mk-colour .rs-carousel-item a', function() {
		_gaq.push(['_trackEvent', 'search', 'color_select', ''+$(this).data('id')]);
	});
}
