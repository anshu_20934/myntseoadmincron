
$(document).ready(function() {
    if (!$('.my-profile-page').length) {
        return;
    }
    
  
    $('.prof-btn-fb-connect').click(function() {
//        _gaq.push(['_trackEvent', cfg.invoke, 'fb_connect', 'click']);
        var referer = $(this).attr('data-referer');
        Myntra.FBConnect(referer);
    });    
    
    if($('.pwd-show').length){
    	$('.pro-grp input').attr('disabled','disabled');
        $('.pwd-grp').show();
        $('.pwd-block').hide();
        $('#action_type').val('change-password');
        $('.prof-pass').attr('id','form-pass')
    }
    
    var btn_disable=true;
    $('#edit-pass').click(function(e) {
        e.preventDefault();
        $('.pro-grp input').attr('disabled','disabled');
        $('.pwd-grp').show();
        $('.pwd-block').hide();
        $('#action_type').val('change-password');
        $('#form-profile .action .primary-btn').removeClass('disabled-btn');
        $('.prof-pass').attr('id','form-pass');
        
    });
    $('#cancel-edit').click(function(e) {
        e.preventDefault();
        $('.pro-grp input').removeAttr('disabled');
        $('.pwd-grp').hide();
        $('.pwd-block input,.pro-grp input#login').attr('disabled','disabled');
        $('.pwd-block').show();
        $('#action_type').val('update-profile-data');
        $('.prof-pass').attr('id','form-profile');
        if(btn_disable){
        	$('#form-profile .action .primary-btn').addClass('disabled-btn');
        }
    });


    $('.gender-sel').parent().jqTransform();

    $('.add-new-address').click(function(e) {
        e.preventDefault();
        var conf = {_type:'shipping', _action:'create'};
        $(document).trigger('myntra.address.form.show', conf);
    });

    $(document).bind('myntra.address.list.ondefaultaddress', function(e, address) {
        $('.mk-default-shipping').html([
            '<p class="name">', address.name, '</p>',
            '<p>', address.address, '</p>',
            '<p>', address.locality, '</p>',
            '<p>', address.city, '</p>',
            '<p>', address.state, ' - ', address.pincode, '</p>',
            '<p><label class="mobile">Mobile </label>', address.mobile, '</p>'
        ].join(''));
    });

    $('#name,#mobile').bind('keydown',function(){
    	 $('#form-profile .action .primary-btn').removeClass('disabled-btn');
    	 btn_disable=false;
    })
     
    $('.gender-sel').bind('click',function(){
       	if($(this).attr('data-val') == undefined){
    		$('#form-profile .action .primary-btn').removeClass('disabled-btn');
    		btn_disable=false;
    	}
       });
    
 
	function nameCheck() {
		var nameVal = $("#name").val(),
			err = $("#name").closest(".row").find(".err"),
			nameExp = /^[A-Za-z ]*$/;
		if(!nameExp.test(nameVal)) {
			err.html("Name can contain only letters");
			return false;
		}
    	if(nameVal.length < 2) {
    		err.html("Name must have at least two characters");
    		return false;
    	}
		err.html("");
		return true;
	}

	function mobileCheck() {
		var mobileVal = $("#mobile").val(),
			err = $("#mobile").closest(".row").find(".err"),
			numExp = /^[0-9]*$/;
		if(!numExp.test(mobileVal)) {
			err.html("Mobile number can contain only numbers");
			return false;
		}
    	if(mobileVal.length != 10) {
    		err.html("Mobile number should be 10 digits");
    		return false;
    	}
		err.html("");
		return true;
	}

	function oldPWCheck() {
		var oldPasswordVal = $("#old-password").val(),
			err = $("#old-password").closest(".row").find(".err");
    	if(oldPasswordVal == "") {
    		err.html("Enter a Password");
    		return false;
    	}
    	if(oldPasswordVal.length < 6) {
    		err.html("Old password supplied is not valid");
    		return false;
    	}
		err.html("");
		return true;
	}

	function newPWCheck() {
		var newPasswordVal = $("#new-password").val(),
			err = $("#new-password").closest(".row").find(".err");
    	if(newPasswordVal == "") {
    		err.html("Enter a valid Password");
    		return false;
    	}
    	if(newPasswordVal.length < 6) {
    		err.html("Minimum 6 characters required");
    		return false;
    	}
		err.html("");
		return true;
	}

	function confNewPWCheck() {
		var newPasswordVal = $("#new-password").val(),
    		confirmPasswordVal = $("#confirm-new-password").val(),
			err = $("#confirm-new-password").closest(".row").find(".err");
    	if(confirmPasswordVal == "") {
    		err.html("Enter a valid Password");
    		return false;
    	}
    	if(newPasswordVal != confirmPasswordVal) {
    		err.html("Passwords must match");
    		return false;
    	}
		err.html("");
		return true;
	}

	$("#name").blur(nameCheck);
	$("#mobile").blur(mobileCheck);
	$("#old-password").blur(oldPWCheck);
	$("#new-password").blur(newPWCheck);
	$("#confirm-new-password").blur(confNewPWCheck);

	$("#form-pass").live("submit", function() {
		 if(oldPWCheck() && newPWCheck() && confNewPWCheck()) {
			 return true;
		 }
		 return false;
	});

	$("#form-profile").live("submit", function() {
		 if($('#form-profile .action .primary-btn').hasClass('disabled-btn')){
			 return false;
			 
		 }
		 else if(nameCheck() && mobileCheck()) {
			 return true;
		 }
		 return false;
	});
	if(showSection !=undefined && showSection!=''){
		location.hash="#"+showSection;
	}
    $(document).bind('myntra.address.list.oninit', function(e, data) {
        // Gopi: jqTransform is done by the address-list.js
    });

    $(document).trigger('myntra.address.list.render', {
        _type:'shipping',
        _editdelete:true,
        container:'.address-list-wrap',
        isPopup:false
    });
    
    $(document).bind('myntra.fblogin.done', function(e, data){
    	window.location.href='/mymyntra.php?view=myprofile&show=more-info';
    });
    
    $(document).bind('myntra.fblogin.auth', function(e, data) {
       //show loader ()
    	$('<div class="body-overlay"><img style="margin-top:100px" src="http://myntra.myntassets.com/skin2/images/loader_150x200.gif" /></div>').css({'position':'fixed','left':'0','top':'0','height':'100%','width':'100%','overflow':'hidden','background':'#fff','opacity':'.6','text-align':'center'}).appendTo('body');
    });
    
    $('#more-info-control').toggle(
    	function(){
    	   	$('#more-info-inner').slideUp('fast',function(){
    	   		$('#more-info-control').find('.text').text('SHOW');
    	   		$('#more-info-control').find('.icon').removeClass('open-state');
    	   	});
    	},
    	function(){
    	   	$('#more-info-inner').slideDown('fast',function(){
    			$('#more-info-control').find('.text').text('HIDE THIS');
    			$('#more-info-control').find('.icon').addClass('open-state');
    		});
      	}
    );
    
    $("#form-profile-more").live("submit", function() {
    	
    	var newBrands=[];
    	var newInterests=[];
    	$('#mk-brands .checked').each(function(index,element){
    		newBrands.push($(this).attr('data-id'));
    	});
    	
    	$('#mk-interests .checked').each(function(index,element){
    		newInterests.push($(this).attr('data-id'));
    	});
    	$("input[name='_new_brands']").val(newBrands);
    	$("input[name='_new_interests']").val(newInterests);
    	 return true;
    });
  //FOR BRAND AND INTERESTS
    Myntra.Profile.Init();
    
    //BLOCK SCROLLTO 
    $('.my-block-links button').on('click',function(){
    	$('html, body').animate({scrollTop: $('#'+$(this).val()).position().top}, 800, function() {});
    });
    
    
});//document.ready end



//FOR BRAND AND INTERESTS
Myntra.Profile.Init= function(){
	
	Myntra.Profile.FilterSelect('brands');
	Myntra.Profile.FilterSelect('interests');
}

Myntra.Profile.FilterSelect = function(id) {
	
	$("#mk-"+id).jScrollPane({verticalDragMinHeight:20});
	var brandsJScrollPane=$("#mk-"+id).data("jsp");

	if($( "#mk-"+id ).length){
		var brandsIndex=[];
		var b=0;
		$("#mk-"+id+" .mk-labelx_check").each(function(){
			brandsIndex.push({
				label:$(this).attr("data-value"),
				show:0
			});
		});
		var a=$.ui.autocomplete.prototype._renderItem;
		
		$("."+id+"-autocomplete-input").autocomplete({
			source:brandsIndex,
	        type:id+"box",
			appendTo:"#mk-"+id+"-search-results",
			search:function(){
				hideAllBrands();
				brandsJScrollPane.scrollTo(0, 0);
			}
		});
		
		function getCount(id){
			//get current count here
			console.log('get');
			return $('#mk-'+id).parent().find('.selected-count strong').text();
		}

		function setCount(id,type){
			var i_count=parseInt(getCount(id));
			if(type=='add'){
				i_count=i_count+1;
			}
			else if(type=='sub'){
				i_count=i_count-1;
			}
			$('#mk-'+id).parent().find('.selected-count strong').text(i_count);
		}
		
		function showAllBrands(){
			$("#mk-"+id+" .mk-labelx_check").show();
		}
		
			function hideAllBrands(){
			$("#mk-"+id+" .mk-labelx_check").hide();
		}
		
		$(".mk-clear-"+id+"-input").hide();
		
		$("."+id+"-autocomplete-input").bind("click focus keyup blur", function() {
			var bai = $("."+id+"-autocomplete-input");
			var cbi = bai.siblings(".mk-clear-"+id+"-input");
			if(bai.val() == "" || bai.val() == bai.attr("data-placeholder")) {
				showAllBrands();
				cbi.hide();
			} else { 
				cbi.show();
			}
		});
		
		$("."+id+"-autocomplete-input").bind("click focus", function() {
			var bai = $("."+id+"-autocomplete-input");
			if(bai.val() == bai.attr("data-placeholder")) {
				bai.val("");
			}
		});
		
		$("."+id+"-autocomplete-input").bind("blur", function() {
			var bai = $("."+id+"-autocomplete-input");
			if(bai.val() == "") {
				bai.val(bai.attr('data-placeholder'));
			}
		});
		
		$(".mk-clear-"+id+"-input").bind("click", function(e) {
			var bai = $("."+id+"-autocomplete-input");
			var cbi = bai.siblings(".mk-clear-"+id+"-input");
			bai.val("");
			cbi.hide();
			bai.trigger("focus");
			e.stopPropagation();
		});
	
		$(".mk-"+id+"-filters .mk-labelx_check.checked").live('click',function(e){
			$(this).removeClass("checked").addClass("unchecked");
			setCount(id,'sub');
		});
		
		$(".mk-"+id+"-filters .mk-labelx_check.unchecked").live('click',function(e){
			$(this).removeClass("unchecked").addClass("checked");
			setCount(id,'add');
		});
		
		//Select already exisiting brands.
	}
}


