
Myntra.Footer = {
    init: function() {
        if($('#testimonials-slide').length){
            $('#testimonials-slide').cycle({
                fx:    'fade',
                speed: 350,
                timeout:  12500,
                prev:    '#prev',
                next:    '#next',
                cleartype: false
            });
        }
    }
};

$(document).ready(function() {
    Myntra.Footer.init();
});

if ($('.mk-site-ft .social-block').length) {
	if (Myntra.Data.newLayout) {
		$(window).load(function() {
			$('.mk-site-ft .fb-like-button').html('<fb:like href="https://www.facebook.com/myntra" send="false" layout="button_count" width="80" show_faces="false" action="like"></fb:like>');
			$('.mk-site-ft .tw-tweet-button').html('<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.myntra.com?utm_source=twitter&utm_medium=footer&utm_campaign=tweet" data-via="myntra" data-related="myntra">Tweet</a>');
			$('.mk-site-ft .gp-plusone-button').html('<g:plusone width="90" href="http://www.myntra.com?utm_source=gplus&utm_medium=footer&utm_campaign=share" size="medium" count="true"></g:plusone>');
			$("body").append('<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script><script type="text/javascript" src="//apis.google.com/js/plusone.js"></script>');
		});
	} else {
		$(window).load(function() {
			$('.mk-site-ft .fb-like-button').html('<fb:like href="https://www.facebook.com/myntra" send="false" width="593" show_faces="true" action="like"></fb:like>');
			$('.mk-site-ft .tw-follow-button').html('<a href="https://twitter.com/myntra" class="twitter-follow-button" data-show-count="false" data-size="large">Follow @myntra</a>');
			$('.mk-site-ft .tw-tweet-button').html('<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.myntra.com?utm_source=twitter&utm_medium=footer&utm_campaign=tweet" data-via="myntra" data-size="large" data-related="myntra">Tweet</a>');
			$('.mk-site-ft .gp-plusone-button').html('<g:plusone width="290" annotation="inline" href="http://www.myntra.com?utm_source=gplus&utm_medium=footer&utm_campaign=share"></g:plusone>');
			$("body").append('<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script><script type="text/javascript" src="//apis.google.com/js/plusone.js"></script>');
		});
	}
}