/* Product Page JS
(myntra.product-page.js)
------------------------------------ */

Myntra.PDP = Myntra.PDP || {}; /*modified this from Myntra.PDP = {};*/

$(document).ready(function(){

	_gaq.push(['_trackEvent','pdp_page_visit', window.location.toString(), null, null, true]);
	
	//PRODUCT-DESCRIPTION

	if($('.desc-type-title').size()>1){
		$('.desc-type-title').click(
			function(){
				$('.desc-text').slideUp('fast');
				if($(this).hasClass('open-state')){
					$('.desc-type-title').removeClass('open-state').addClass('close-state');
					$(this).next('li').slideUp('fast',function(){
						$(this).prev('li').removeClass('open-state').addClass('close-state');
					});
				}
				else {
					$('.desc-type-title').removeClass('open-state').addClass('close-state');
					$(this).next('li').slideDown('fast',function(){
						$(this).prev('li').removeClass('close-state').addClass('open-state');
						if (!$(this).next().attr('data-scroll-init')) {
							$(this).jScrollPane().attr('data-scroll-init','1');
							}
					});
				}
		});
		$('.desc-text').addClass("desc-text-styling");
	}
	else {
		$('.desc-type-title').find('span').hide();
		$('.desc-type-title').hide();
		$('.desc-text').css({'max-height':'220px'});
	}

	$('.mk-product-helper .desc-text').each(function() {
		if ($(this).prev().hasClass('open-state') && !$(this).attr('data-scroll-init')) {
			$(this).jScrollPane().attr('data-scroll-init','1');
		}
	});

    //Initializing the discount combo overlay
	Myntra.InitPDPComboOverlay();

	$('#zoom1').click(function(e){
		e.preventDefault();
	});

	$(".mk-product-page .rs-carousel-action-prev").die().live("click", function(event){
		_gaq.push(['_trackEvent', 'pdp', 'prev_image', 'pdp_page']);
		event.stopPropagation();
	});

	$(".mk-product-page .rs-carousel-action-next").die().live("click", function(event){
		_gaq.push(['_trackEvent', 'pdp', 'next_image', 'pdp_page']);
		event.stopPropagation();
	});

	$(".mk-product-page .mk-product-guide .mk-add-to-cart").die().live("click", function(event){
		_gaq.push(['_trackEvent', 'buy_now', Myntra.Data.pageName, zoomLevel]);
		event.stopPropagation();
	});
    $(".mk-express-buy").die().live("click", function(event){
        _gaq.push(['_trackEvent', 'express_buy', Myntra.Data.pageName, zoomLevel]);
        event.stopPropagation();
    });

	$(".mk-product-page .mk-product-guide .mk-save-for-later").die().live("click", function(event){
		_gaq.push(['_trackEvent', 'pdp', 'saveinwishlist', '']);
		event.stopPropagation();
	});

	$('.mk-product-page .mk-product-guide .mk-size').die().live('change',function () {
		_gaq.push(['_trackEvent', 'pdp', 'size_select', $(this).find('option:selected').text()]);
	});

	$('.mk-product-page .mk-product-guide .mk-colour .rs-carousel-item a').die().live('click', function () {
		_gaq.push(['_trackEvent', 'pdp', 'color_select', ''+$(this).data('id')]);
	});

	$('#sizechart-old').die().live("click", function(event){
		_gaq.push(['_trackEvent', 'pdp', 'sizechart', '']);
		event.stopPropagation();
	});

	$('#sizechart-new').die().live("click", function(event){
		_gaq.push(['_trackEvent', 'pdp', 'sizechart', '']);
		event.stopPropagation();
	});

	Myntra.PDP.Utils = {};
	(function(context) {
		context.scrollTo = function(position) {
			Myntra.PDP.GoToTop.scrollInProgress = true;
			$('html, body').animate({scrollTop: position}, 800, function() {
				Myntra.PDP.GoToTop.scrollInProgress = false;
			});
		};
	})(Myntra.PDP.Utils);

	Myntra.PDP.GoToTop = {};
	( function(context) {

		var showIf = $(window).width() > 1150 ? true : false,
			elem = $('.go-to-top-btn.right');

		$(window).resize( function() {
			showIf = $(window).width() > 1150 ? true : false;
		});

		context.scrollInProgress = false;

		var showGoToTop = function(){
			if(!context.scrollInProgress && showIf){
				elem.css('top', ($(window).height() - 44)/2).show();
			}
		};

		var hideGoToTop = function(){
			elem.hide();
		};

		context.detectScroll = function() {
			var showElemBegin = $(window).height()/2,
				currScrollPos = $(window).scrollTop();
			if (currScrollPos > showElemBegin) {
				showGoToTop();
			}
			else {
				hideGoToTop();
			}
		};

		elem.click( function() {
			hideGoToTop();
			Myntra.PDP.Utils.scrollTo(0);
		});

	})(Myntra.PDP.GoToTop);


	$(window).bind("load scroll resize", function() {
		var throttleTimer = null;
		clearTimeout(throttleTimer);
		throttleTimer = setTimeout( function() {Myntra.PDP.GoToTop.detectScroll();}, 250); //so that the method is called 250ms after the last scroll/resize event
	});
	
	/* Colour Selection Carousel Init */
	Myntra.Utils.initCarousel($('.mk-product-page .mk-product-option-cont'));

	// Customer Promises
	$('.mk-free-shipping').data('lb', Myntra.LightBox('#shipping-info'));
	$('.mk-dispatch').data('lb', Myntra.LightBox('#dispatch-info'));
	$('.mk-cod').data('lb', Myntra.LightBox('#cod-info'));
	$('.mk-return').data('lb', Myntra.LightBox('#returns-info'));
	if ($('#emi-info').length) {
		$('.mk-emi').data('lb', Myntra.LightBox('#emi-info'));
	}
	$('.mk-guarantee ul li.link').click(function() {
		$(this).data('lb').show();
	});
	// paroksh
   var jealous = $('#jealous-fit-guide');
   if (jealous.length) {
           jealous.data('lb',Myntra.LightBox('#jealous-fit-guide-info'));
           jealous.click(function(){
           $(this).data('lb').show();
           });
   }
   // paroksh end
        
	if($('.mk-product-disclaimer').length){
		$('.mk-product-disclaimer-link').data('lb', Myntra.LightBox('#product-disclaimer-info'));
		$('.mk-product-disclaimer-link').click(function() {
			$(this).data('lb').show();
		});
	}
	
	// Preselect Size Option
	if ($('#preselectSKU').length) {
		var sku = $('#preselectSKU').val();
		$('#mk-product-page .size-btn[value="' + sku + '"]').click();
	}
	
    Myntra.Utils.SizesTooltip.InitTooltip();

    //GA call for new combo link on PDP
    if($('.goto-combo').length){
    	$('.goto-combo').on('click',function(e){
    		_gaq.push(['_trackEvent', 'combo', 'click', Myntra.Data.pageName, parseInt(Myntra.PDP.Data.discountId)]);
    	});
    	
    }
});

/* 
 * PDP Social Buttons Initialization Begins
 * GT : Removing plusOne, Pinterest, twitter as no longer required in new design
 */
if ($('.mk-share-links #pageURL').length) {
	$(window).load(function() {
		var shareUrl = $('.mk-share-links #pageURL').val(),
			styleid = $('.add-to-cart-form #productStyleId').val();
		
		var popupUrl = $('.mk-share-links .gm a')[0].href + '&su=' + encodeURIComponent(document.title) + '&body=' + encodeURIComponent('Hey!') + '%0D%0D' + encodeURIComponent('I found this on Myntra.com. What do you think about it?') + '%0D%0D' + encodeURIComponent($('.mk-product-guide h1').html()) + '%0D' + encodeURIComponent(shareUrl + '?utm_source=gmail&utm_medium=pdp&utm_campaign=' + styleid) + '%0D%0D' + encodeURIComponent('via www.myntra.com') + '%0D';
		$('.mk-share-links .gm a').click(function() {
			gmail = window.open(popupUrl, 'GMail', 'height=600, width=800, top=50, left=50');
			if (window.focus) gmail.focus();
			return false;
		});
		// $.getScript(('https:' == document.location.protocol ? 'https://' : 'http://') + 'apis.google.com/js/plusone.js');
		// $('.mk-share-links .tw').html('<a href="https://twitter.com/share" class="twitter-share-button" data-via="myntra" data-related="myntra" data-dnt="true" data-url="' + shareUrl + '?utm_source=twitter&utm_medium=pdp&utm_campaign=' + styleid + '">Tweet</a>' +
		// 	'<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>');
		$('.mk-share-links .fb').html('<fb:like href="' + shareUrl + '?utm_source=facebook&utm_medium=pdp&utm_campaign=' + styleid + '" send="false" layout="button_count" width="80" show_faces="true" action="like"></fb:like>');
		// $('.mk-share-links .gp').html('<g:plusone href="' + shareUrl + '?utm_source=gplus&utm_medium=pdp&utm_campaign=' + styleid + '" size="medium" count="true"></g:plusone>');
		// $('.mk-product-media .rs-carousel-runner img').each(function(i) {
		// 	$(this).attr('data-idx', i);
		// 	var ptUrl = 'http://pinterest.com/pin/create/button/?' + 'url=' + encodeURIComponent(shareUrl + '?utm_source=pinterest&utm_medium=pdp&utm_campaign=' + styleid) + '&media=' + encodeURIComponent(this.alt) + '&description=' + encodeURIComponent($('title').html() + ' via @myntra');
		// 	$('.mk-share-links .pt').append('<div id="idx-' + i + '" class="mk-hide"><a class="pin-it-button" href="' + ptUrl + '" count-layout="horizontal" always-show-count="true"><img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a></div>');
		// }).mouseenter(function() {
		// 	$('.mk-share-links .pt div').hide();
		// 	$('.mk-share-links .pt #idx-' + $(this).attr('data-idx')).show();
		// }).first().mouseenter();
		setTimeout(function() {
			$('.mk-share-links .fb iframe').attr('src', function (i, src) {
				return src.replace('&sdk=joey','');
			});
			// $('body').append('<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script><script type="text/javascript" src="//apis.google.com/js/plusone.js"></script>');
		}, 1000);
	});
}
/* PDP Social Buttons Initialization Ends */

/* 
 * PDP / Pincode Widget Interfacing Code Begins
 */
$(function() {
	if ($('.mk-product-sidebar .delivery-network').length) {
		var cfg = {
				styleid:	parseInt($('#mk-product-page .add-to-cart-form #productStyleId')[0].value),
				skuid:		$('#mk-product-page .add-to-cart-form #productSKUID')[0].value
								? $('#mk-product-page .add-to-cart-form #productSKUID')[0].value
								: $('#mk-product-page .flat-size-options .size-btn').not('.unavailable')[0].value
		};

		/* 
		 * Responsible for interfacing between PDP and Pincode Widget
		 * Called on document ready and myntra.pincodewidget.datachange event
		 * isDataChange true -> function triggered from myntra.pincodewidget.datachange event -> FRESH DATA!
		 * isDataChange false -> function triggered on document ready -> if there is any stored data, it is stale
		 */
		function getPincodeSnippet(isDataChange) {
			/* Check localStorage for stored data */
			var json;
			if (localStorage) {
				json = localStorage.getItem('PincodeWidget.ServiceabilityData');
			}
			/* Data Found! */
			if (json) {
				try {
					json = $.parseJSON(json);
				} catch (e) {
					/* Stored Data may be invalid JSON */
					window.localStorage.removeItem('PincodeWidget.ServiceabilityData');
					if (Myntra.Data.userEmail) {
						getFreshData(); // Get data for user's default address
					}
				}
				if (isDataChange) { // Fresh Data! :D Create and insert snippet in sidebar
					var snippet = '<span class="red">' + json.DELIVERY_PROMISE_TIME + '</span> Days Delivery Time'; 
					if ($.inArray('cod', json.SERVICEABLE) > -1) {
						snippet += ' &<br />Cash on Delivery Available';
					}
					snippet += '<br />For PIN Code <span class="red">' + json.pincode + '</span> [<span class="ul">Edit</span>]';
					$('.mk-product-sidebar .delivery-network .changeable').removeClass('ul').html(snippet);
				} else { // Stale Data! :(
					getFreshData(json.pincode); // Use previously stored pincode to get fresh data, as delivery time may vary from item to item
				}
			} else if (Myntra.Data.userEmail) { // No data found! :( 
				getFreshData(); // Get data for user's default address
			}
		}
		
		function getFreshData(pincode) {
			$(window).load(function() { // Fire ajax on window load
				if (!Myntra.PincodeWidget.initialized) {
					Myntra.PincodeWidget = Myntra.PincodeWidget(cfg);
				}
				Myntra.PincodeWidget.fetchAndStoreJSON(pincode);
			});
		}
		
		getPincodeSnippet();
		
		$(window).bind('myntra.pincodewidget.datachange', function() {
			getPincodeSnippet(true); // Ding! Fresh Data just stored!
		});
		
		$('.delivery-network').click(function() {
			if (!Myntra.PincodeWidget.initialized) {
				Myntra.PincodeWidget = Myntra.PincodeWidget(cfg);
			}
			Myntra.PincodeWidget.show();
			_gaq.push(['_trackEvent','PDP', 'Pincode_Widget', 'Click', cfg.styleid]);
		});
	}
});
/* PDP - Pincode Widget Interfacing Code Ends */

/* Load Recently Viewed Widget*/
$(window).load(function () {
	Myntra.RecentWidget.init();
});


$(document).ready(function() {

	var text = $(".supplier-details a").data('tooltiptext');
	if(text != "" && typeof text != "undefined") {
    	$('body').append( '<div id="SuplierInfoTooltip" class="myntra-tooltip"><div></div>' + text + '</div>' );
    }
    var node = $('div#SuplierInfoTooltip');
    if(node.length) {
    	$(".supplier-details a").on("mouseover", function(e) {
    		var yOffset = 220,
			elProp = $(e.target).offset(),
	        top = (elProp.top + 20),
	        left = (elProp.left-110);
	        node.css("top", top+"px").css("left", left+"px");
	        node.fadeIn(300);
    	});

    	$(".supplier-details a").on("mouseout", function(e) {
    		node.fadeOut(300);
    	});
    }
    


	var ctx = $('#fest-pdp-text');
    if (!ctx.length) { return; }

    var lb = Myntra.LightBox('#fest-pdp-popup');
    ctx.on('click', function(e) {
        e.preventDefault();
        lb.show();
    });

});


