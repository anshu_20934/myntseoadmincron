///////////////////////CART///////////////
Myntra.sCombo.ComboScroll = function () {
    $('.cart-widget-box.combo-type-class-defaultitemlist').carousel({
        itemsPerPage: 5, // number of items to show on each page
        itemsPerTransition: 5, // number of items moved with each transition
        noOfRows: 1, // number of rows (see demo)
        nextPrevActions: true, // whether next and prev links will be included
        pagination: false, // whether pagination links will be included
        speed: 'normal', // animation speed
        after: function(event, data){
            var from_index = $('.cart-widget-box.combo-type-class-defaultitemlist').carousel('getPage')*5-4;
            var noOfItems = $('.cart-widget-box.combo-type-class-defaultitemlist').carousel('getNoOfItems');
            var to_index = from_index+4;
            if(to_index>noOfItems){
                to_index = noOfItems;
            }
            $(".pagination-start").html(from_index);
            $(".pagination-end").html(to_index);
        }
   });
}
Myntra.sCombo.FreeComboScroll = function () {
    $('.cart-widget-box.combo-type-class-freeitem').carousel({
        itemsPerPage: 5, // number of items to show on each page
        itemsPerTransition: 5, // number of items moved with each transition
        noOfRows: 1, // number of rows (see demo)
        nextPrevActions: true, // whether next and prev links will be included
        pagination: false, // whether pagination links will be included
        speed: 'normal', // animation speed
	create: function(){
		var noOfItems = $('.cart-widget-box.combo-type-class-freeitem ').carousel('getNoOfItems');
		if(noOfItems<=5){
        	        $(".rs-carousel-action-prev").hide();
                	$(".rs-carousel-action-next").hide();
			$(".combo-type-class-freeitem .cart-widget-items").css("margin","0 auto");
            	}
	},
        after: function(event, data){
            var from_index = $('.cart-widget-box.combo-type-class-freeitem ').carousel('getPage')*5-4;
            var noOfItems = $('.cart-widget-box.combo-type-class-freeitem ').carousel('getNoOfItems');
            var to_index = from_index+4;
            if(to_index>noOfItems){
                to_index = noOfItems;
            }
            $(".pagination-start").html(from_index);
            $(".pagination-end").html(to_index);
        }
   });
}
Myntra.sCombo.tabEvents = function(){
	$("a.item-tab").click(function(event){
		event.preventDefault();
		$(".combo-type-class-freeitem").addClass("hide");
		$(".combo-type-class-defaultitemlist").removeClass("hide");
		Myntra.sCombo.ComboScroll();	
	});
	$("a.free-item-tab").click(function(event){
		event.preventDefault();
		$(".combo-type-class-defaultitemlist").addClass("hide");
		$(".combo-type-class-freeitem").removeClass("hide");
		Myntra.sCombo.FreeComboScroll();
    });
}
///Initialise the magic
Myntra.sCombo.ComboOverlay.Init = function () {
     _gaq.push(['_trackEvent', 'combo_free', 'click', Myntra.Data.pageName, parseInt(Myntra.sCombo.ComboId)]);
    //Add to collection
    Myntra.sCombo.cSetStyles = new Myntra.sCombo.setStyles();
	Myntra.sCombo.cFreeStyles = new Myntra.sCombo.freeStyles();
    Myntra.sCombo.minMore = Myntra.sCombo.min;
    //Check for length of selectedStyles
	for(var k = 0 ; k < 2 ; k++){
		if(k==0) {
			var dataAdded = Myntra.sCombo.products;
			var col = Myntra.sCombo.cSetStyles;
		}
		else if(k==1) {
			var dataAdded = Myntra.sCombo.freeproducts;
			var col = Myntra.sCombo.cFreeStyles;
		}
	    for(var i in dataAdded) {
    	    for(var j in dataAdded[i]){
				// o = already added style in cart
            	var o = dataAdded[i][j];
	            for(var a in Myntra.sCombo.comboSet){
					// loop to get uid
                    if(Myntra.sCombo.comboSet[a].styleid == i ){
                        var styleObj = new Myntra.sCombo.styleModel();
                        styleObj.set({uid: a, styleid: i , skuid: j, quantity: o.quantity});//Also sets initialAttributes that dont change
                        col.add(styleObj);
                        styleObj.setInCart();
                        styleObj.setChecked();
						Myntra.sCombo.UI.checkBoxStyle(styleObj);
						Myntra.sCombo.UI.moveItemToFront(styleObj);
                        styleObj.loadSizes(function(){
                            if(1){//styleObj.isAvailable()){
                                var data = styleObj.get('sizeList');
                                var sizes = data;
                                for (var key in sizes){
                                    if(key==j){
                                        var size = sizes[key][0];
                                        styleObj.set({size: size});
                                        break;
                                    }
                                }
                                Myntra.sCombo.UI.priceMessageContent(true,styleObj.get('uid'),styleObj.get('size'),styleObj.get('quantity'));
								$("#item_combo_lay_"+styleObj.get('uid')+" .main-flip .edit-btn").addClass("hide");
                            }
                        });
                        break;
                    }
        	    }
    	    }
		}
    }
    //adding ends
    Myntra.sCombo.RegisterEvents();
	if (!$.isEmptyObject(Myntra.sCombo.products))
		Myntra.sCombo.ComboScroll();
	else if (!$.isEmptyObject(Myntra.sCombo.freeproducts))
	    Myntra.sCombo.FreeComboScroll();
    //Myntra.sCombo.ComboOverlay.RefreshUI();
    //Myntra.sCombo.InitQuickLook();
}

	Myntra.sCombo.cleanNoQ = function(){
		$('.cart-widget-item').each(function(index,value){
			var uid = $(value).attr('data-uid');
			var styleid = Myntra.sCombo.comboSet[uid].styleid;
			var styleObj = new Myntra.sCombo.styleModel();
			styleObj.set({uid: uid, styleid: styleid });
			styleObj.loadSizes(function(){
				if(!styleObj.isAvailable()) $(value).remove();
			});
		});
	}

