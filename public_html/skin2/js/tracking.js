function trackingOmniture(){
	if(typeof(s_analytics_firesync) != "undefined" && s_analytics_firesync) {
		// Sync call already triggered. Check analytics_data_collection.. 
		return;
	}
	
	// Omniture page call. Using s_analytics as s is getting unset by some piece of code and is not visible here
	if(typeof(s_analytics) != "undefined"){
		s_analytics.t();
	} 
}

function tracking(){
    // GA tracking page load call
	var gaScript = document.createElement('script');
	gaScript.type = 'text/javascript';
	gaScript.async = true;
    gaScript.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(gaScript, s);
	_gaq.push(['_trackEvent','GA.JS', 'load']);
    /*
    setTimeout(function(){
        if (typeof _gat == 'undefined') {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; 
            s.parentNode.insertBefore(ga, s);
            
            _gaq.push(['_trackEvent','GA.JS', 'load']); 
        }
    }, 1000); */
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  	ga('create', 'UA-1752831-15', 'auto');
  	ga('set', '&uid', Myntra.Data.userHashId);
  	ga('send', 'pageview');

}

//GA related calls
Myntra.GA = Myntra.GA || {};

Myntra.GA.gaTrackEventAjax = function(category, action, label){
	_gaq.push(['_trackEvent', category, action, label]);
	return false;
}

Myntra.GA.gaTrackPageViewAjax = function(url){
	_gaq.push(['_trackPageview', url]);
	return false;
}

$(window).load(function(){
	//console.log('trackOmnitue');return false;
	setTimeout(function() { trackingOmniture() }, 0);
    tracking();
});

