//AVAIL
Myntra.emark=null;


function getRRCookie(cname)
{
	var name = Myntra.Data.cookieprefix+""+cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) 
	{
	  var c = ca[i].trim();
	  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
	}
	return "";
}

var R3_COMMON = null;
var R3_ITEM = null;
function availOnLoad() {
	
	// Rich relevance
	$.getScript(window.location.protocol+"//media.richrelevance.com/rrserver/js/1.0/p13n.js",function(){
		
		R3_COMMON = new r3_common();

		
		R3_COMMON.setApiKey('54ed2a6e-d225-11e0-9cab-12313b0349b4');
		
		if(Myntra.Data.cookieprefix != "" ){
			R3_COMMON.setBaseUrl(window.location.protocol+'//integration.richrelevance.com/rrserver/');
		}else{
			R3_COMMON.setBaseUrl(window.location.protocol+'//recs.richrelevance.com/rrserver/');
		}
		
		R3_COMMON.setClickthruServer(window.location.protocol+"//"+window.location.host);
		var rr_sid = getRRCookie("rr_sid")+"";
		R3_COMMON.setSessionId(rr_sid);
		
		if(typeof Myntra.Data.userHashId == "undefined" || Myntra.Data.userHashId == "" ){
			R3_COMMON.setUserId(rr_sid);
		}else{
			R3_COMMON.setUserId(Myntra.Data.userHashId+"");
		}
		
		RR.jsonCallback = function (){
			//console.dir(RR.data.JSON.placements);
			//console.dir("done");
		}; // call back performed, once rr call is executed.


		
		switch (Myntra.Data.pageName) {
		case "home":
			// do nothing 
			break;
		case "pdp":
			R3_ITEM = new r3_item();
			R3_ITEM.setId(Myntra.PDP.Data.id+"");
			R3_ITEM.setName(Myntra.PDP.Data.name+"");
			
			R3_COMMON.addCategoryHintId(Myntra.PDP.Data.articleTypeId+"");

			R3_COMMON.addPlacementType('item_page.Horizontal');

			rr_flush_onload();
			
			r3();
			break;
		case "cart":
			break;
		}
	});
	
	
	$.getScript("http://service.sg.avail.net/2009-02-13/dynamic/54ed2a6e-d225-11e0-9cab-12313b0349b4/emark.js", function() {
		Myntra.emark = new Emark();
		//Myntra.emarkC =new Emark();
				
		switch (Myntra.Data.pageName) {
		case "home":
			//DO NOTHING
			break;
		case "pdp":
			/* similar products*/
			//PAHSE-1-->getRecommendation-Presentation,lgClickedOn, If added to cart call logAddedToCart
			avail_dynamic_param = "append andcat in subtemplate ALL with " + avail_dynamic_param;
			var t_code = Myntra.Utils.getQueryParam("t_code");
			if (t_code != '') {
				Myntra.emark.logClickedOn(curr_style_id, t_code);
			}
			var product = "ProductID:" + curr_style_id;				
			recProductsSimilar= Myntra.emark.getRecommendations("Productpage", [product], [avail_dynamic_param]);
			
			/* cross selling products*/
			if (Myntra.PDP.showExtraRec)
			{
				cross_sell_avail_dynamic_param1 = "append andcat in subtemplate ALL with " + cross_sell_avail_dynamic_param1;
				cross_sell_avail_dynamic_param2 = "append orcat in subtemplate ALL with " + cross_sell_avail_dynamic_param2;
				recProductsMatchWith= Myntra.emark.getRecommendations("Productpage", [product], [cross_sell_avail_dynamic_param1,cross_sell_avail_dynamic_param2]);
			}
			//do the logClickedonHere on PDP page load
			Myntra.emark.commit(function() {
				//do presentation here
				var json = {};
				json['similar'] = {};


               Myntra.Data.recommendationType='avail';
                // Override rect0 output    
                if(Myntra.Data.showMyntraRecommendations =='test'){
                    $.ajax({
                        url: http_loc + '/myntra/ajax_recommendation.php?styleId=' + curr_style_id,
                        async: false,
                        dataType: "json",
                        success: function(data){
                            if(data.length>4){
                                recProductsSimilar.values=data;
                                Myntra.Data.recommendationType='myntra'
                            }
                        }
                    });
                }

                    json['similar']['styleids'] = recProductsSimilar.values;
                    json['similar']['tabid'] = 1;
                    
                if(Myntra.Data.recommendationType != 'myntra')
                {
                    json['similar']['trackingcode'] = recProductsSimilar.trackingcode;
                }
				
				if (Myntra.PDP.showExtraRec)
				{
					json['matchWith'] = {};
					json['matchWith']['styleids'] = recProductsMatchWith.values;
					json['matchWith']['trackingcode'] = recProductsMatchWith.trackingcode;
					json['matchWith']['tabid'] = 2;
					
				}

				/*json['similar'] = {};
				json['similar']['styleids'] = [["42273"], ["42035"], ["42272"], ["40373"], ["40380"],["33726"], ["33732"], ["38411"], ["33738"], ["38402"]];
				json['similar']['trackingcode'] = "3d6913f75ba3262c0276f697ad9c2d5c";
				json['similar']['tabid'] = 1;*/
	/*			json['matchWith'] = {};
				json['matchWith']['styleids'] = [["42273"], ["42035"], ["42272"], ["40373"], ["40380"],["33726"], ["33732"], ["38411"], ["42273"], ["42035"]];
				json['matchWith']['trackingcode'] = "3d6913f75ba3262c0276f697ad9c2d5c";
				json['matchWith']['tabid'] = 2;*/
				
				displayAvail("pdp",json);
			});
			
					
			/* more from this brand */
			if (Myntra.PDP.showExtraRec)
			{
				displayMoreFromThisBrand(gender,article_type,brand,"pdp",ageGroup);
			}

			break;
        /* Gopi: search is not used
		case "search":

			var curUrl = ("'" + window.location + "'").search('#!');
			var pageFilter = -1;
			if (curUrl != -1) {
				pageFilter = ("'" + window.location + "'").search('#!page');
			}
			var phrase = "Phrase:" + query;
			var recProducts = Myntra.emark.getRecommendations("SearchPage", [phrase]);
			Myntra.emark.commit(function() {
				if (curUrl == -1) { //displayAvail(recProducts.values,recProducts.trackingcode);
				}
				else if (pageFilter != -1) { //displayAvail(recProducts.values,recProducts.trackingcode);
				}
			});
			break;
        */
		case "cart":
			if(recommendationsCart =='test')
			{
				if (recProductsInCart =='true')
				{
					avail_dynamic_param1 = "append andcat in subtemplate ALL with " + avail_dynamic_param1;//+",Accessories";
					avail_dynamic_param2 = "append rule in subtemplate ALL with Price >= "+availRecLowerLimit+" and Price <= "+availRecHigherLimit;
					avail_dynamic_param3 = "append orcat in subtemplate ALL with " + avail_dynamic_param_match_with;
					var product = "ProductID:" + productid;				
					var recProducts = Myntra.emark.getRecommendations("CartPageCrossSell",[product],[avail_dynamic_param1,avail_dynamic_param2,avail_dynamic_param3]);
					Myntra.emark.commit(function() {
						var json = {};
						json['similar'] = {};
						json['similar']['styleids'] = recProducts.values;
						json['similar']['trackingcode'] = recProducts.trackingcode;					
						displayAvail("cart",json);
					});
				}
				else
				{
					$('.cart-recommend-block').html('');
				}
			}
			else
			{
				var recProducts = Myntra.emark.getRecommendations("ShoppingCart");
				Myntra.emark.commit(function() {
					var json = {};
					json['similar'] = {};
					json['similar']['styleids'] = recProducts.values;
					json['similar']['trackingcode'] = recProducts.trackingcode;	
				
					/*json['similar'] = {};
					json['similar']['styleids'] = [["42273"], ["42035"], ["42272"], ["40373"], ["40380"]];
					json['similar']['trackingcode'] = "3d6913f75ba3262c0276f697ad9c2d5c";*/
				
					displayAvail("cart",json);
				});
			}
			
			break;
		}

	});
}


function displayAvail(page, json) {
	Myntra.Data.pageName=(page)?page:Myntra.Data.pageName;
	var rec_url = http_loc + "/avail_ajax_recommendation.php",
		data = "json=" + JSON.stringify(json) + "&page=" + Myntra.Data.pageName 
				+ "&styleId=" + curr_style_id + "&recommendationType="+ Myntra.Data.recommendationType;
	$.ajax({
		type: "POST",
		url: rec_url,
		data: data,
		success: function(data) {
			data = $.parseJSON(data);
			if (data != 0){
				switch(Myntra.Data.pageName){
					case "addedtocart":
						$('.recommend-block').html(data);
						if($('.mk-matchmaker-horizontal .rs-carousel-runner > li').size()>4){
							$('.mk-matchmaker-horizontal').carousel({
								itemsPerPage: 4, // number of items to show on each page
								itemsPerTransition: 1, // number of items moved with each transition
								noOfRows: 1, // number of rows (see demo)
								nextPrevLinks: true, // whether next and prev links will be included
								pagination: false, // whether pagination links will be included
								speed: 'normal' // animation speed
							});
						}
						$(document).trigger('myntra.addedtocart.center');
				break;
				case "cart":
					var matchmaker = $('.cart-recommend-block'); 
					if (matchmaker.length){
						for (i in data) {
							matchmaker.hide().html(data[i]).fadeIn(1000,function(){
								if($('.mk-matchmaker-horizontal .rs-carousel-runner > li').size()>5){
									$('.cart-recommend-block .mk-carousel-five').carousel({
										itemsPerPage: 5, // number of items to show on each page
										itemsPerTransition: 5, // number of items moved with each transition
										noOfRows: 1, // number of rows (see demo)
										nextPrevLinks: true, // whether next and prev links will be included
										pagination: false, // whether pagination links will be included
										speed: 'normal', // animation speed
										autoScroll:true										
									});
								}
							});
						}
					}
                    // show the err msg on hover of buy now button unless size is selected
                    $('.cart-recommend-block .inline-add').hover(
                        function() {
                            if ($(this).prev().find('.inline-select').val() == '0') {
                                $(this).next('.err').show();
                            }
                        },
                        function() {
                            $(this).next('.err').hide();
                        }
                    );
					styleids=[];
					$('.inline-add-to-cart-form').find(":hidden[name='productStyleId']").each(function(i, el) {
						styleids.push(el.value);
					});
				   $.ajax({
				        type: 'GET',
				        url: '/myntra/ajax_getsizes.php?ids=' + styleids.join(','),
				        dataType: 'json',
				        beforeSend:function() {
				        },
				        success: function(resp) {
				            //load in the select
				        	Myntra.RecentWidget = Myntra.RecentWidget || {};
				        	Myntra.RecentWidget.Data = Myntra.RecentWidget.Data || {};
				        	Myntra.RecentWidget.Data.sizes=resp;
				        	$('.inline-add-to-cart-form').each(function(e,el){
				        		Myntra.Utils.loadRecentSizes($(el));
				        	});

				        },
				        complete: function(xhr, status) {
				        }
				    });

				break;
				case "pdp":
					/* cross selling tabs display */
					var matchmaker = $('#mk-matchmaker'); 
					if (matchmaker.length){
						for (i in data) {
							$('#tabs-' + i, matchmaker).append(data[i]);
						}
						if (Myntra.PDP.showExtraRec) {
							$(".mk-recommendation-tabs").tabs();
						} else {
							matchmaker.find('.mk-match-cycle').cycle({
								timeout: 9000,
								pager: '.mk-cycle-nav'
							});
						}
						matchmaker.fadeIn(1000);
					}
				break;
				}
			}
			else
			{
				$('.cart-recommend-block').hide()
				$('.recommend-block').hide();
				$('#mk-matchmaker').hide();
			}
			Myntra.Utils.showAdminStyleDetails();
		}
	});

}

function displayMoreFromThisBrand(gender,article_type,brand,page,ageGroup) {
	Myntra.Data.pageName=(page)?page:Myntra.Data.pageName;
	if(ageGroup == "Kids-Unisex")
		gender = "Boys"
	else if (gender == "Unisex")
		gender = "Men";
    var queryStr = "query="+encodeURIComponent('brands_filter_facet:"'+brand+'" AND count_options_availbale:[1 TO *] AND global_attr_article_type:"'+article_type+'" AND global_attr_gender:"'+gender+'"')+ "&page=" + Myntra.Data.pageName;;
	$.ajax({
		type: "POST",
		url: http_loc + "/solr_recommendation.php",
		data:queryStr,
		success: function(data) {			
			var matchmaker = $('#mk-matchmaker'); 
			if (matchmaker.length){
					$('#tabs-3' , matchmaker).append(data);
				}
				$(".mk-recommendation-tabs").tabs();
				matchmaker.fadeIn(1000);
			}		
	});

}
//END AVAIL

$(window).load(function(){

	availOnLoad();


});
