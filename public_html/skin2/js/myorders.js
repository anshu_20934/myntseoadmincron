function getLoyaltyPoints(orderid, itemid, clb) {
    var get_loyalty_points_url = http_loc + "/getLoyaltyPoints.php";
    var data = {
        orderid: orderid,
        itemid: itemid,
        _token: Myntra.Data.token
    };
    $.ajax({
        type: 'POST',
        url: get_loyalty_points_url,
        data: data,
        success: function(res) {
            clb(res);
        }
    });
}


function showShipmentTracking(orderid, shipmentid) {
    var trackingRequestData = $('#track-' + orderid).html();
    Myntra.MsgBox.show('Tracking Details - Order No. ' + orderid, '<img src="http://myntra.myntassets.com/skin2/images/loader_transparent.gif" />');
    $.ajax({
        type: "POST",
        url: http_loc + "/order_shipment_tracking.php",
        data: "requestData=" + encodeURIComponent(trackingRequestData) + "&_token=" + Myntra.Data.token,
        success: function(result) {
            Myntra.Data.TrackBox = Myntra.MsgBox.show("", result);
            if (shipmentid) {
                $('#shipment-' + shipmentid).addClass('expanded').find('.details').show();
            } else {
                $('.order-tracking .shipment').addClass('expanded').find('.details').show();
            }
            if ($('.order-tracking .shipment').length == 1) {
                $('.order-tracking .shipment-header').css('cursor', 'default').find('.expand-ico').hide();
            }
            Myntra.Data.TrackBox.center();
        },
        error: function() {
            Myntra.MsgBox.show('Tracking Details - Order No. ' + orderid, 'There seems to have been an error in fetching the tracking details for your order. Please try again.');
        }
    });
    return false;
}

function fetchChildOrders(orderid) {
    var fetch_child_orders_url = http_loc + "/getChildOrderDetails.php";
    var data = "orderid=" + orderid;
    $.ajax({
        type: 'POST',
        url: fetch_child_orders_url,
        data: data,
        success: function(data) {
            data = $.parseJSON(data);
            $('span#shopfest-cancel-child-orders-msg').text(data.message);
        }

    });

}

$(document).ready(function() {
    //common to orders and returns pages
    
    if ($('.my-orders-page').length || $('.mk-returns-page').length) {

        $('.orderbox .final-price .ico').click(function(e) {
            var ico = $(this),
                pnode = ico.parent().parent(),
                mrp = pnode.find('.mrp'),
                discounts = pnode.find('.discounts');
            finalprice = pnode.find(".final-price");

            if (!mrp.length && !discounts.length) {
                return;
            }
            if (ico.hasClass('ico-expand')) {
                mrp.removeClass('hide');
                discounts.removeClass('hide');
                ico.removeClass('ico-expand').addClass('ico-collapse');
                finalprice.addClass("final-price-border");
            } else if (ico.hasClass('ico-collapse')) {
                mrp.addClass('hide');
                discounts.addClass('hide');
                ico.removeClass('ico-collapse').addClass('ico-expand');
                finalprice.removeClass("final-price-border");
            }
        });
        $('.loyalbox .final-price .ico').click(function(e) {
            var ico = $(this),
                pnode = ico.parent().parent(),
                mrp = pnode.find('.mrp'),
                discounts = pnode.find('.discounts');
            finalprice = pnode.find(".final-price");

            if (!mrp.length && !discounts.length) {
                return;
            }
            if (ico.hasClass('ico-expand')) {
                mrp.removeClass('hide');
                discounts.removeClass('hide');
                ico.removeClass('ico-expand').addClass('ico-collapse');
                finalprice.addClass("final-price-border");
            } else if (ico.hasClass('ico-collapse')) {
                mrp.addClass('hide');
                discounts.addClass('hide');
                ico.removeClass('ico-collapse').addClass('ico-expand');
                finalprice.removeClass("final-price-border");
            }
        });

        $('.orderbox .no-return-why').click(function(e) {
            var $link = $(this);
            //no-return-loyalty-desc
            $link.closest('.details').find('.no-return-loyalty-desc').slideDown();
            $link.hide();
        });

        $('.orderbox .btn-return-only').click(function(e) {
            var btn = $(this),
                key = btn.data('return-key');
            if (!Myntra.Data.orderItems) {
                var json = $('#order-items-json').html();
                try {
                    Myntra.Data.orderItems = $.parseJSON(json);
                } catch (ex) {
                    alert('Order items json is invalid');
                    return;
                }
            }
            if ($('.mk-returns-page').length) {
                var address_markup = $(this).closest('.suborder').find('.shipping-address address').html();
                var shipped_address = $.parseJSON(("{" + $(this).closest('.suborder').find('.shipping-address address').data('address') + "}").replace(/\s+/g, ' ').replace(/([^,{:(\s*)])"+(?![,}(\s*):])/g, '$1'));
                var shipped_zipcode = $(this).closest('.suborder').find('.shipping-address address').data('zipcode');
            } else {
                var address_markup = $(this).closest('.bd').find('.shipping-address address').html();
                var shipped_address = $.parseJSON(("{" + $(this).closest('.bd').find('.shipping-address address').data('address') + "}").replace(/\s+/g, ' ').replace(/([^,{:(\s*)])"+(?![,}(\s*):])/g, '$1'));
                var shipped_zipcode = $(this).closest('.bd').find('.shipping-address address').data('zipcode');
            }
            var url = $(this).closest('li').find('.photo img').attr('src');
            img_url = url.replace(/_images_.*_mini.jpg/, "_images_150_200_mini.jpg");
            var data = Myntra.Data.orderItems[key],
                conf = {
                    cfg: {
                        isModal: true,
                        _type: 'return',
                        img_url: img_url,
                        address_markup: address_markup,
                        key: key,
                        zipcode: shipped_zipcode,
                        address: shipped_address
                    },
                    data: data
                };
            $(document).trigger('myntra.return.show', conf);
        });

        $('.orderbox .btn-fork').click(function(e) {
            var btn = $(this),
                key = btn.data('return-key');
            if (!Myntra.Data.orderItems) {
                var json = $('#order-items-json').html();
                try {
                    Myntra.Data.orderItems = $.parseJSON(json);
                } catch (ex) {
                    alert('Order items json is invalid');
                    return;
                }
            }
            if ($('.mk-returns-page').length) {
                var address_markup = $(this).closest('.suborder').find('.shipping-address address').html();
                var shipped_address = $.parseJSON(("{" + $(this).closest('.suborder').find('.shipping-address address').data('address') + "}").replace(/\s+/g, ' ').replace(/([^,{:(\s*)])"+(?![,}(\s*):])/g, '$1'));
                var shipped_zipcode = $(this).closest('.suborder').find('.shipping-address address').data('zipcode');
            } else {

                var address_markup = $(this).closest('.bd').find('.shipping-address address').html();
                var shipped_address = $.parseJSON(("{" + $(this).closest('.bd').find('.shipping-address address').data('address') + "}").replace(/\s+/g, ' ').replace(/([^,{:(\s*)])"+(?![,}(\s*):])/g, '$1'));
                var shipped_zipcode = $(this).closest('.bd').find('.shipping-address address').data('zipcode');
            }
            var url = $(this).closest('li').find('.photo img').attr('src');
            img_url = url.replace(/_images_.*_mini.jpg/, "_images_150_200_mini.jpg");
            var data = Myntra.Data.orderItems[key],
                conf = {
                    cfg: {
                        isModal: true,
                        _type: 'fork',
                        img_url: img_url,
                        address_markup: address_markup,
                        key: key,
                        zipcode: shipped_zipcode,
                        address: shipped_address
                    },
                    data: data
                };
            $(document).trigger('myntra.fork.show', conf);
        });

        $(document).bind('myntra.return.done', function(e, data) {
            var key = data.orderId + '-' + data.itemId,
                btn = $('.btn-fork[data-return-key="' + key + '"]');
            btn.siblings('.return-placed').removeClass('hide');
            btn.remove();
        });
    }

    if (!$('.my-orders-page').length) {
        return;

    }


    var initDiscountEvents = function(orderid) {
        $(".myorder-discount-submit-" + orderid).click(function(event) {
            if ($(this).parents(".discount-area").find("#discount-tnc-" + orderid).is(':checked')) {
                var lb = Myntra.LightBox('#order-' + orderid);
                var vCode = $(this).parents(".discount-area").find("#voucher-inp-order-" + orderid).val();
                var myRegxp = /^([a-zA-Z0-9_-]+)$/;
                if (myRegxp.test(vCode) == false || vCode.length != 15) {
                    alert("Please enter correct Voucher Code");
                } else {
                    loadDiscountData(orderid, vCode, lb);
                }
            } else {
                alert("Please accept the Terms and Conditions to proceed");
            }
        });
        $(".discount-tnc input:checkbox").click(function(event) {
            var input = $(this)[0];
            $(".myorder-discount-submit-" + orderid).attr("disabled", true);
        });
    };

    var loadDiscountData = function(orderid, vCode, lb, tip, returnItem) {
        /*      var url = "getVoucherData.php?pagetype=myorder&orderid="+orderid;
        if(vCode!=null){
            url = url + "&code=" + vCode;
        }
        $.ajax({
            type: 'GET',
            url: url,
            success: function(data){
                data = jQuery.parseJSON(data);;
                $(".orderbox .bd .discount-area").html(data.main);
                $(".discount-tool-tip-"+orderid).html(data.tooltip);
*/
        if (lb != null) {
            if (returnItem) {
                $("#order-" + orderid + '.orderbox').find('.return-hide').hide().end().find('.return-show').show();
            } else {
                $("#order-" + orderid + '.orderbox').find('.return-show').hide().end().find('.return-hide').show();
            }
            lb.show();
            initDiscountEvents(orderid);
        }
        /*              if(tip!=null && data.tooltip!=null && data.tooltip!=''){
                    $(".discount-tool-tip-"+orderid).css("border-top", "1px solid #E5E5E5");
                    $(".discount-tool-tip-"+orderid).css("padding-top", "5px");
                }

    		},
            error: function(data){
                return false;
            }
		});
*/
    };

    $('.mk-order-number').each(function(el, idx) {
        var ctx = $(this),
            tip = ctx.next();
        Myntra.InitTooltip(tip, ctx, {
            side: 'above'
        });
    });

    var boxes = {};
    $('.my-orders-page tr.order-row').click(function(e) {
        var id = $.trim($('.mk-order-number', this).text()),
            box_id = 'order-' + id;
        if (!boxes[box_id]) {
            boxes[box_id] = Myntra.LightBox('#' + box_id);
        };
        if ($(e.target).is('.order-action .return')) {
            loadDiscountData(id, null, boxes[box_id], null, true);
        } else if (!$(e.target).closest('.order-action').length) {
            loadDiscountData(id, null, boxes[box_id]);
        }
    });

    $('.mk-cancel-order').click(function(e) {
        if (shopFest == '1')
            fetchChildOrders($(this).attr('data-orderid'));
        var id = 'cancel-order-' + $(this).attr('data-orderid');
        $('#reason-validation-div-' + id).css('display', 'none');
        $('#remarks-validation-div-' + id).css('display', 'none');
        $("#msg-box-" + id).css('display', 'none');
        $("#cancel-buttons-" + id).css('display', 'block');
        if (!boxes[id]) {
            boxes[id] = Myntra.LightBox('#' + id);
        };
        boxes[id].show();
    });

    $('.mk-get-points').click(function(e) {
        var id = 'loyalbox-' + $(this).attr('data-orderid');
        if (!boxes[id]) {
            boxes[id] = Myntra.LightBox('#' + id);
        }
        boxes[id].show();
    });

    $('.lp-confirm-get-points').click(function(e) {
        var $this = $(this);
        $this.closest('.lp-confirm-msg').slideUp(500);
        $this.closest('.lp-action').find('.lp-confirm-dialog').slideDown(500);
    });

    $('.lp-get-points-yes').click(function(e) {
        // Call ajax request for service for getting points.
        var $this = $(this),
            orderid_itemid = $this.attr('data-getpoints-key'),
            orderid = orderid_itemid.split("-")[0],
            itemid = orderid_itemid.split("-")[1],
            id = 'loyalbox-' + $this.attr('data-orderid');
        boxes[id].showLoading();
        getLoyaltyPoints(orderid, itemid, function(result) {
            var $loyaltyAwardMsg = $this.closest('.details').find('.loyalty-award-msg');
            if (result && result.status && result.status === 'ok') {
                // $html = $('<span class="icon-success"></span><span> '+result.points_awarded+' Points sucessfully added to your <a href="/mymyntra.php?view=myorders#myloyalty">Check now</a></span>');
                // We need to change the corrosponding message in return exchange screen as well
                $('button[data-return-key="' + orderid_itemid + '"]').removeClass('return-show').hide();
                $('div.no-return-loyalty[data-return-key="' + orderid_itemid + '"]').show();
            } else {
                var $html = typeof(result.markup) != 'undefined' ? result.markup : '<span class="red">There has been a technical glitch, because of which we are unable to process this request.<br/>Please visit "<a href="/mymyntra.php?view=myorders">My Myntra > My Orders</a>" and try again later.</span>';
                $loyaltyAwardMsg.html($html);
            }
            $this.closest('.lp-action-container').hide();
            $loyaltyAwardMsg.show();
            boxes[id].hideLoading();
        });
    });

    $('.lp-get-points-no').click(function(e) {
        var $this = $(this),
            orderid = $this.attr('data-orderid'),
            id = 'loyalbox-' + orderid;
        $this.closest('.lp-confirm-dialog').slideUp();
        $this.closest('.lp-action').find('.lp-confirm-msg').slideDown();
    });

    $('.cancel-submit').click(function() {
        $("#cancel-buttons-" + orderid).css('display', 'none');
        var orderid = $(this).attr('data-orderid');
        var reason = $('#cancel-reason-' + orderid).val();
        var details = $('#details-' + orderid).val();
        var error = false;
        if (reason == '0') {
            $('#reason-validation-div-' + orderid).css('display', 'block');
            error = true;
        }
        if (details == '') {
            $('#remarks-validation-div-' + orderid).css('display', 'block');
            error = true;
        }
        if (error) {
            $("#cancel-buttons-" + orderid).css('display', 'block');
            return false;
        }
        $('#cancel-panel-' + orderid).css('display', 'none');

        var id = 'cancel-order-' + orderid;
        if (!boxes[id]) {
            boxes[id] = Myntra.LightBox('#' + id);
        };
        boxes[id].showLoading();
        $.ajax({
            type: 'POST',
            url: "mymyntra.php",
            data: "_token=" + Myntra.Data.token + "&mode=cancel-order&orderid=" + orderid + "&details=" + details + "&reason=" + reason,
            success: function(data) {
                data = jQuery.parseJSON(data);
                if (data.result == 'success') {
                    var refund_msg = "";
                    if (data.refund_amount > 0) {
                        refund_msg = "<br/> Rs." + data.refund_amount + " have been credited to your cashback account";
                    }
                    $("#msg-box-" + orderid).html("Order has been cancelled successfully" + refund_msg);
                    $("#msg-box-" + orderid).css('display', 'block');
                    $("#order-actions-" + orderid).css('display', 'none');
                } else if (data.result == 'not_allowed') {
                    if (data.msg != '') {
                        $("#msg-box-" + orderid).html(data.msg);
                    } else {
                        $("#msg-box-" + orderid).html("There is an unexpected technical error. Please get in touch with our customer care team");
                    }
                    $("#msg-box-" + orderid).css('display', 'block');
                } else {
                    $("#msg-box-" + orderid).html("There is an unexpected technical error. Please get in touch with our customer care team");
                    $("#msg-box-" + orderid).css('display', 'block');
                }
                boxes[id].hideLoading();
            },
            error: function(data) {
                $("#msg-box-" + orderid).html("There is an unexpected technical error. Please get in touch with our customer care team");
                $("#msg-box-" + orderid).css('display', 'block');
                boxes[id].hideLoading();
                return false;
            }
        });
        return false;
    });

    $('.cancel-no').click(function() {
        var id = 'cancel-order-' + $(this).attr('data-orderid');
        $('.cancel-valid').css('display', 'none');
        boxes[id].hide();
    });

    $('.order-details-link').click(function() {
        var id = 'order-' + ($(this).closest('.orderbox').find('.return-show').hide().end().find('.return-hide').show().end().attr('data-orderid'));
        boxes[id].center();
        return false;
    });

    $('.track-order, .track-shipment').click(function(e) {
        var orderid = $(this).attr('data-orderid') || $(this).closest('.lightbox').attr('data-orderid');
        if ($(e.target).is('.track-shipment')) {
            return showShipmentTracking(orderid, $(e.target).attr('data-shipmentid'));
        } else {
            return showShipmentTracking(orderid);
        }
    });

    $('.order-tracking .shipment-header').live('click.expand', function() {
        if ($(this).closest('.order-tracking').find('.shipment').length > 1) {
            $(this).siblings('.details').slideToggle('fast', function() {
                Myntra.Data.TrackBox.center();
            }).closest('.shipment').toggleClass('expanded');
        }
    });


    //deeplinking changes
    if(order_action && oid){
        if(order_action=='getpoints'){
            $('#order-actions-'+oid).find('.mk-get-points').trigger('click');
        }
        else if(order_action == 'return' || order_action == 'exchange'){
            $('#order-actions-'+oid).find('.return').trigger('click');
        }
        else if(order_action == 'cancel'){
            $('#order-actions-'+oid).find('.mk-cancel-order').trigger('click');
        }
    }
});