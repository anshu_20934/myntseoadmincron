
(function() { "use strict";

/** 
 * Myntra specific Tooltip widget/component
 * Author: gopinath.k@myntra.com
 *
 * Data Types:
 * jobject  : jQuery object
 * selector : the css selector string (used as-in instantiating jQuery objects)
 *
 * Arguments:
 * tip : jobject | selector - can be null, if ctx is specified (mostly with the useTitle option)
 * ctx : jobject | selector - can be null, if tip is specified (same tip is used with multiple contexts dynamically)
 * cfg : object [optional]
 *
 * cfg options:
 * side     : String  - one of [above, below, left, right]
 * useTitle : Boolean - if set, title attribute from the ctx nodes will be show as tooltip
 * nohover  : Boolean - show/hide is NOT based on hover event of the ctx
 * noclick  : Boolean - show/hide is NOT based on click event of the ctx
 * element  : jobject | selector - it should be child of ctx. it is used to calculate the positions
 *
 * Example: show the title attribute as a tooltip
 * var tip = new Myntra.Tooltip(null, $('.list > li > a'), {side:'above', useTitle:true});
 *
 * Example: show a small overlay as a tooltip
 * var tip = new Myntra.Tooltip($('#cash-back-tip', $('.cash-back-tip-ctx'), {side:'below'});
 */
Myntra.Tooltip = Myntra.Base.extend({
    init: function(tip, ctx, cfg) {
        cfg = cfg || {side:'below'};
        this._super();
        this.tip = tip ? $(tip) : $('<div class="myntra-tooltip"/>').appendTo('body');
        this.ctx = $(ctx);
        this.conf(cfg);
        this.tmrHover = null;
        this.tmrAutoHide = null;
        this.isSuspended = false;

        this.insertArrows();
        if (!this.tip.parent().is('body')) {
            this.tip.appendTo('body');
        }

        this.bd = this.tip.find('> .bd');
        if (!this.bd.length) {
            this.bd = $('<div class="bd"/>').appendTo(this.tip);
        }

        if (this.tip.hasClass('feature-tooltip')) {
        	this.insertClose();
        }
        
        this.onCtxClick = function(e) { 
            if (this.isSuspended) { return; }
            this.show(e);
        }.bind(this);

        this.onOutsideClick = function(e) {
            if (this.isSuspended) { return; }
            var el = $(e.target);
            // if the click happened on the tip or ctx, don't hide
            if (el.closest(this.tip).length || el.closest(this.ctx).length) {
                return;
            }
            this.hide();
        }.bind(this);

        if (this.ctx.length && !this.conf('nohover')) {
            if (this.conf('useTitle')) {
                this.backupTitle();
            }
            this.addHoverEvents();
        }
        else if (!this.conf('noclick')) {
            this.ctx.on('click', this.onCtxClick);
        }
    },

    setBody: function(content) {
        this.bd.html(content);
    },

    backupTitle: function() {
        this.ctx.each(function(i, el) {
            var el = $(el), title = el.attr('title');
            if (!title) { return }
            el.attr('data-title', title).removeAttr('title');
        });
    },

    insertArrows: function() {
        if (this.conf('side') === 'below') {
            this.tip.prepend('<div class="arrow a-out arrow-top-outer"></div><div class="arrow a-in arrow-top-inner"></div>');
        }
        else if (this.conf('side') === 'above') {
            this.tip.prepend('<div class="arrow a-out arrow-bottom-outer"></div><div class="arrow a-in arrow-bottom-inner"></div>');
        }
        else if (this.conf('side') === 'left') {
            this.tip.prepend('<div class="arrow a-out arrow-right-outer"></div><div class="arrow a-in arrow-right-inner"></div>');
        }
        else if (this.conf('side') === 'right') {
            this.tip.prepend('<div class="arrow a-out arrow-left-outer"></div><div class="arrow a-in arrow-left-inner"></div>');
        }
        this.arrowOut = this.tip.find('.a-out');
        this.arrowIn = this.tip.find('.a-in');
    },
    
    insertClose : function() {
    	var tip = this;
    	this.tip.append('<div class="close"></div>').on('click', function() {
    		tip.hide();
    	});
    },

    close_tooltip : function(){
    	this.hide();
    },
    
    show: function(e, ctx) {
        if (this.isSuspended) { return; }
        if (!e && !ctx) {
            throw new Error("Tooltip.show: Can't determine the context. Both event and ctx are missing");
        }

        (typeof this.onBeforeShow === 'function') && this.onBeforeShow();
        this.tip.css('top', '-500px');
        this.tip.css('display', 'block');

        if (this.conf('useTitle') && e) {
            this.setBody($(e.currentTarget).data('title'));
        }

        var el = e ? (this.conf('element') ? $(this.conf('element'), e.currentTarget) : $(e.currentTarget)) : ctx,
            ch = el.outerHeight(),
            cw = el.outerWidth(),
            p = el.offset(),
            th = this.tip.outerHeight(),
            tw = this.tip.outerWidth(),
            side = this.conf('side'),
            x, y,
            gap = 10;

        if (side === 'left' || side === 'right') {
            y = Math.round(th / 2) - 12;
            this.arrowOut.css('top', y);
            this.arrowIn.css('top', y + 3);
        }
        else if (side === 'above' || side === 'below') {
            x = Math.round(tw / 2) - 12;
            this.arrowOut.css('left', x);
            this.arrowIn.css('left', x + 3);
        }

        if (side === 'below') {
            p.top = p.top + ch + gap;
            p.left = p.left - tw / 2 + cw / 2;
        }
        else if (side === 'above') {
            p.top = p.top - th - gap;
            p.left = p.left - tw / 2 + cw / 2;
        }
        else if (side === 'left') {
            p.top = p.top - th / 2 + ch / 2;
            p.left = p.left - tw - gap;
        }
        else if (side === 'right') {
            p.top = p.top - th / 2 + ch / 2;
            p.left = p.left + cw + gap;
        }

        this.tip.css('left', p.left + 'px');
        this.tip.css('top', p.top + 'px');
        this.tip.fadeIn();

        if (this.conf('nohover') && !this.conf('noclick')) {
            $(document).on('click', this.onOutsideClick);
        }
    },

    hide: function() {
        if (this.isSuspended) { return; }
        this.tip.fadeOut();
        if (this.conf('nohover') && !this.conf('noclick')) {
            $(document).off('click', this.onOutsideClick);
        }
        if (this.tmrAutoHide) {
            clearTimeout(this.tmrAutoHide);
            this.tmrAutoHide = null;
        }
        (typeof this.onAfterHide === 'function') && this.onAfterHide();
    },

    suspend: function() {
        this.isSuspended = true;
    },

    resume: function() {
        this.isSuspended = false;
    },

    tmpHide: function() {
        this.tip.hide();
    },

    tmpShow: function() {
        this.tip.show();
    },

    addHoverEvents: function() {
        var that = this;
        this.ctx.mouseenter(function(e) {
            if (that.tmrHover) {
                clearTimeout(that.tmrHover);
                that.tmrHover = null;
            }
            that.show(e);
        });
        this.ctx.mouseleave(function(e) {
            that.tmrHover = setTimeout(function() { that.hide() }, 100);
        });
        this.tip.mouseenter(function(e) {
            if (that.tmrHover) {
                clearTimeout(that.tmrHover);
                that.tmrHover = null;
            }
        });
        this.tip.mouseleave(function(e) {
            that.tmrHover = setTimeout(function() { that.hide() }, 100);
        });
    },

    triggerAutoHide: function(timeout) {
        timeout = timeout || 3000;
        this.tmrAutoHide = setTimeout(function() {
            this.hide();
        }.bind(this), timeout);
    }
});


/** 
 * !!! DEPRECATED !!! - use Myntra.Tooltip
 */
var tips = [];
Myntra.InitTooltip = function(tip, ctx, cfg) {
    tips[tips.length] = new Myntra.Tooltip(tip, ctx, cfg);
    /*
    tip = $(tip);
    ctx = $(ctx);
    if (!tip || !ctx) { return; }
    if (tip.length > 1) { return; }

    cfg = cfg || {};
    cfg.side = cfg.side || 'below';

    if (!tip.parent().is('body')) {
        tip.appendTo('body');
    }

    if (cfg.side === 'below') {
        tip.prepend('<div class="arrow a-out arrow-top-outer"></div><div class="arrow a-in arrow-top-inner"></div>');
    }
    else if (cfg.side === 'above') {
        tip.prepend('<div class="arrow a-out arrow-bottom-outer"></div><div class="arrow a-in arrow-bottom-inner"></div>');
    }
    else if (cfg.side === 'left') {
        tip.prepend('<div class="arrow a-out arrow-right-outer"></div><div class="arrow a-in arrow-right-inner"></div>');
    }
    else if (cfg.side === 'right') {
        tip.prepend('<div class="arrow a-out arrow-left-outer"></div><div class="arrow a-in arrow-left-inner"></div>');
    }

    var arrow = tip.find('.arrow'),
        arrowOut = tip.find('.a-out'),
        arrowIn = tip.find('.a-in'),
        x, y, p, th, tw, ch, cw,
        cached = false,
        gap = 10,
        tmrHover = null;

    ctx.mouseenter(function(e) {
        if (tmrHover) {
            clearTimeout(tmrHover);
            tmrHover = null;
        }
        if (!cached) {
            tip.css('top', '-500px');
            tip.css('display', 'block');
            th = tip.outerHeight();
            tw = tip.outerWidth();
            cached = true;
        }

        var el = cfg.element ? $(cfg.element, this) : $(this);
        ch = el.outerHeight();
        cw = el.outerWidth();
        p = el.offset();
        //console.log(tw, th, cw, ch, p);

        if (cfg.side === 'left' || cfg.side === 'right') {
            y = Math.round(th / 2) - 12;
            arrowOut.css('top', y);
            arrowIn.css('top', y + 3);
        }
        else if (cfg.side === 'above' || cfg.side === 'below') {
            x = Math.round(tw / 2) - 12;
            arrowOut.css('left', x);
            arrowIn.css('left', x + 3);
        }

        if (cfg.side === 'below') {
            p.top = p.top + ch + gap;
            p.left = p.left - tw / 2 + cw / 2;
        }
        else if (cfg.side === 'above') {
            p.top = p.top - th - gap;
            p.left = p.left - tw / 2 + cw / 2;
        }
        else if (cfg.side === 'left') {
            p.top = p.top - th / 2 + ch / 2;
            p.left = p.left - tw - gap;
        }
        else if (cfg.side === 'right') {
            p.top = p.top - th / 2 + ch / 2;
            p.left = p.left + cw + gap;
        }

        tip.css('left', p.left + 'px');
        tip.css('top', p.top + 'px');
        tip.fadeIn();
    });
    ctx.mouseleave(function(e) {
        tmrHover = setTimeout(function() { tip.fadeOut() }, 100);
    });
    tip.mouseenter(function(e) {
        if (tmrHover) {
            clearTimeout(tmrHover);
            tmrHover = null;
        }
    });
    tip.mouseleave(function(e) {
        tmrHover = setTimeout(function() { tip.fadeOut() }, 100);
    });
    */
};


/** 
 * !!! DEPRECATED !!! - use Myntra.Tooltip
 */
Myntra.InitTooltipFromTitle = function(ctx, cfg) {
    $(ctx).each(function(i, el) {
        var node = $(el),
            txt = node.attr('title');
        if (!txt) { return; }
        node.removeAttr('title');
        var tip = $('<div class="myntra-tooltip">' + txt + '</div>').appendTo('body');
        Myntra.InitTooltip(tip, node, cfg);
    });
};

}()); // end of "use strict" wrapper

