Myntra.Utils.extend = function(subc, superc, overrides) {
    var F = function(){};
    F.prototype = superc.prototype;
    subc.prototype = new F();
    subc.prototype.constructor = subc;
    subc.superclass = superc.prototype;
    for (var i in overrides) {
        if (!overrides.hasOwnProperty(i)) { continue; }
        subc.prototype[i] = overrides[i];
    }

};

Myntra.Utils.showAdminStyleDetails = function(){
	if(Myntra.Data.showAdminAdditionalStyleDetails == 'y') {
		$('.item').each( function(){
			if($(this).attr('data-id')){
				var style_id= $(this).attr('data-id').replace('prod_0_style_','');
				var styleDetailsElem=$(this).children('.style-details');
				if($(styleDetailsElem).length){
					$.get(http_loc+'/myntra/style_details.php',{format:'json',style_id:style_id}, function(data) {
						$(styleDetailsElem).html(data.innerhtml);
						$(styleDetailsElem).show();
					});
				}
			}
		});
	}

};

Myntra.Utils.getQueryParam = function(name,url) {
    var params = {};
    var qstr = (url)?(url.substring(url.indexOf('?'))):location.search;
    if (!qstr) { return ''; }
    qstr = qstr.replace(/^\?/, '');
    var nvPairs = qstr.split('&');
    for (var i = 0, n = nvPairs.length; i < n; i += 1) {
        var parts = nvPairs[i].split('=');
        params[parts[0]] = parts[1];
    }
    Myntra.Utils.getQueryParam = function(name) {
        return (params[name] || '');
    };
    return (params[name] || '');
};

Myntra.Utils.removeQueryParam = function(name,url) {
   var _url=(url)?url:location.href;
   var qstr = (url)?(url.substring(url.indexOf('?'))):location.search;
   
   if (!qstr) { return _url; }
   qstr = qstr.replace(/^\?/, '');
   var nvPairs = qstr.split('&');
   var nvPairsNew =[];
   for (var i = 0, n = nvPairs.length; i < n; i += 1) {
        var parts = nvPairs[i].split('=');
        if(parts[0] !== name){
            nvPairsNew.push(nvPairs[i]);
        }
    }
   return nvPairsNew.join('&');

};

Myntra.Utils.formatNumber = function(num, suffixPaise) {
    // attempt to convert to Number type
    if (typeof(num) !== 'number') {
        num = +num;
    }
    // and give up if not Number
    if (typeof(num) !== 'number') {
        return '';
    }
    var parts = num.toFixed(2).split("."),
    num = parts[0],
    paise = parts[1];
    if (num.length <= 3) { 
        return suffixPaise ? (num + '.' + paise) : num;
    }
    var m = /(\d+)(\d{3})$/.exec(num);
    var inp = m[1];
    var last = m[2];
    var out = [];
    if (inp.length % 2 === 1) {
        out.push(inp.substr(0,1));
        inp = inp.substr(1);
    }
    var rx = /\d{2}/g;
    while (m = rx.exec(inp)) {
        out.push(m[0]);
    }
    out.push(last);
    return out.join(',') + (suffixPaise ? ('.' + paise) : '');
};


Myntra.Utils.validateEmail = function(email){
	 var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	 return re.test(email);
	
};

Myntra.Utils.loadImageQueue = function(){
    var lazy_images = $('.lazy');
    $.each(lazy_images, function() {
    	$(this).attr("src", $(this).attr("data-lazy-src"));
    	$(this).removeAttr("data-lazy-src");
    	$(this).removeClass("lazy");
    });
};

Myntra.Utils.loadLoginBg = function(){
	var loginBg = $(".mk-login-bg-path").val();
	var boxForBg = null;
	if($("#lb-login .mod").length) {
		boxForBg = $("#lb-login .mod"); 
	} 
	else if($("#reg-login .mod").length) {
		boxForBg = $("#reg-login .mod");
	};
	/*if(boxForBg) {
		if(loginBg) {
			boxForBg.css({'background-image': "url('"+ loginBg +"')"});
		}
		else {
			boxForBg.css({'background': "#ffffff"});
		}
	}*/
};

/*
 * @author: kunalgolani
 * @param context: jQuery object containing the carousel
 * @param _itemsPerPage (optional): number of items to show at once, default: 5; should be 'calc' when carousel items are not all of the same dimensions
 * @param carouselMaskWidth (required only if _itemsPerPage is 'calc'): width of the mask through which the visible items are seen
 * In case of _itemsPerPage == 'calc', the CSS should be such that without initializing the carousel, the carousel runner's width represents the sum of the widths of the carousel items
 * 		- refer recent-viewed-load.js and feature-slides.css for an example of variable width size buttons in a carousel
 */

Myntra.Utils.initCarousel = function (context, _itemsPerPage, carouselMaskWidth) {
	if (context.find('.rs-carousel-mask').length) return;
	
	_itemsPerPage = _itemsPerPage || 5; // default is 5
	
	var _items = $('.rs-carousel-item', context),
        noOfItems = _items.length;
	
	if (_itemsPerPage == 'calc') {
		if (!carouselMaskWidth) return;
		var runnerWidth = context.find('.rs-carousel-runner').width();
		_itemsPerPage = Math.floor(carouselMaskWidth * noOfItems / runnerWidth);
	} else { carouselMaskWidth = null;}
	
	if (noOfItems > _itemsPerPage) {
		$('.rs-carousel', context).carousel({
            mask: null,
            runner: $('.rs-carousel-runner', context),
            items: _items,
			itemsPerPage: _itemsPerPage, // number of items to show on each page
			itemsPerTransition: 1, // number of items moved with each transition
			nextPrevActions: true, // whether next and prev links will be included
			pagination: false, // whether pagination links will be included
			onHover: true, // whether hover will act like click for next/prev
			insertPrevAction: function() {
				return $('.icon-prev', context).removeClass('mk-hide');
			},
			insertNextAction: function() {
				return $('.icon-next', context).removeClass('mk-hide');
			}
		});
		if (carouselMaskWidth) {
			context.find('.rs-carousel-runner').width(runnerWidth); // correcting constant width assumptions made by rs.carousel plugin
		}
	}
}

Myntra.Utils.scrollToPos = function (pos,interval){
	interval=interval||0;
	$('html,body').animate({scrollTop:pos},interval,'easeOutCirc',function(){$(window).scrollTop(pos);});
};

/**
 * This is used to store a composite string (delimited values)
 * basically, it concatenates value to existingValue and returns if the value is either not found in existingValue,
 * or found but unique is set to false
 * Basically the unique flag decides whether to repeat the occurrence of value, if it already exists in existingValue: it should be boolean
 * delimiter is the character (or string) used to separate the values
 * limit: limit the number of entries to this number: most recent
 */
Myntra.Utils.setCompositeData = function (value, existingValue, unique, delimiter, limit) {
	if(typeof unique  === "undefined") {
		var unique = true;
	}
	if(typeof delimiter === "undefined") {
		var delimiter = "|";
	}
	if(typeof limit === "undefined") {
		var limit = -1;
	}
	if(existingValue != null && existingValue !="") {
		var valueArr = existingValue.split(delimiter);
		var found =  (unique && $.inArray(value,valueArr) != -1);
		if(!found || !unique) {
			valueArr.push(value);
			if(limit > 0 && valueArr.length > limit) {
				valueArr = valueArr.reverse();
				valueArr.splice(limit,valueArr.length-limit);
				valueArr = valueArr.reverse();
			}
			value = valueArr.join(delimiter);
		}
	}
	return value;
};

Myntra.Utils.Validation = {} || Myntra.Utils.Validation;

Myntra.Utils.Validation.nameCheck = function(selector, errSelector, isDefaultValuePresent){
	var el=$(selector);
	var errEl=$(errSelector);
	if(!el || !errEl){
		return false;
	}
	var nameVal = el.val(),
		nameExp = /^[A-Za-z ]*$/;
	if (!nameVal) {
		errEl.html("Please enter your name").show();
		el.addClass("mk-err");
		return false;
    }
	if(isDefaultValuePresent && ($(selector).val() == $(selector).attr("data-placeholder"))){
		$(errSelector).html("Please enter a valid name").show();
		return false;
	}
	if(!nameExp.test(nameVal)) {
		errEl.html("Name can contain only letters").show();
		el.addClass("mk-err");
		return false;
	}
	if(nameVal.length < 2) {
		errEl.html("Name must have at least two characters").show();
		el.addClass("mk-err");
		return false;
	}
	errEl.html("").hide();
	el.removeClass("mk-err");
	return true;
}

Myntra.Utils.Validation.emailCheck = function(selector, errSelector) {
	var el=$(selector);
	var errEl=$(errSelector);
	if(!el || !errEl){
		return false;
	}
	var emailVal = el.val(),
		emailExp = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; 
    if (!emailVal) {
    	errEl.html("Please enter your email address").show();
    	el.addClass("mk-err");
    	return false;
    }
    if(el.val() == el.attr("data-placeholder")){
		errEl.html("Please Enter a valid email address").show();
		el.addClass("mk-err");
		return false;
	}
    if(!emailExp.test(emailVal)) {
    	errEl.html("Please enter a valid email address").show();
    	el.addClass("mk-err");
    	return false;
    }
    el.removeClass("mk-err");
    errEl.html("").hide();
    return true;
};

Myntra.Utils.Validation.messageCheck = function(selector) {
	var el=$(selector);
	var errEl=$(errSelector);
	if(!el || !errEl){
		return false;
	}
	var messageVal = el.val(),
		emailExp = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; 
    if (!messageVal) {    	
    	errEl.html("Please enter your message").show();
    	el.addClass("mk-err");
    	return false;
    }    
    errEl.html("").hide();
    el.removeClass("mk-err");
    return true;
};

Myntra.Utils.Validation.isNumber = function(num) {
  return (typeof num == 'string' || typeof num == 'number') && !isNaN(num - 0) && num != null;
};

/*
 * return a throttled function, that will call the underlying function 'fn' _only once_ per 'delay'
 * irrespective of how many times the throttled function is called within the 'delay'
 * author: gopinath.k@myntra.com
 */
Myntra.Utils.throttle = function(fn, delay) {
    var timer = null;
    function inner() {
        var context = this, args = arguments;
        clearTimeout(timer);
        inner.timeout = function() {
            fn.apply(context, args);
            timer = null;
        };
        timer = setTimeout(inner.timeout, delay);
    }
    inner.restart = function() {
        if (timer === null) { return; }
        clearTimeout(timer);
        timer = setTimeout(inner.timeout, delay);
    };
    return inner;
};

/* 
 * generic error handler for ajax
 * author: gopinath.k@myntra.com
 */
Myntra.Utils.ajaxError = function(name) {
    return function(xhr, status, msg) {
        if (status === 'parsererror') {
            alert('[' + name + '.ajax] Invalid JSON!');
        }
    };
};

/*
 * return a function, that will call the underlying 'fn' after the myntra login is done
 * author: gopinath.k@myntra.com
 */
Myntra.Utils.afterLogin = function(fn) {
    return function() {
        var context = this, args = arguments;
        if (!Myntra.Data.userEmail) {
            $(document).trigger('myntra.login.show', {onLoginDone: function(data) {
                fn.apply(context, args);
            }});
        }
        else {
            fn.apply(context, args);
        }
    };
};

Myntra.Utils.AssocArraySize = function(obj) {
    // http://stackoverflow.com/a/6700/11236
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
}

Myntra.Utils.String = Myntra.Utils.String || {};

Myntra.Utils.String.contains = function() {
    if(typeof(arguments[0])=='string'){
    	for (var i = 1; i < arguments.length; i++ ) {
    		if (arguments[0].indexOf(arguments[i]) != -1) return true;
    	}
    }
	return false;
};

/* Replaces multiple whitespaces with single */
Myntra.Utils.String.strip = function(str) {
    if (typeof str != 'string') return str;
    return str.replace(/\s{2,}/g,' ');
};

Myntra.Utils.String.truncate = function(str, noOfChars) {
    if (typeof str != 'string' || str.length <= noOfChars) return str;
    return str.substr(0, noOfChars) + '...';
};

Myntra.Utils.String.toTitleCase = function(str) {
    return str.replace(/\w\S*/g, function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

/*
 * invoke the callback function after all the images are loaded
 * author: gopinath.k@myntra.com
 */
Myntra.Utils.loadImages = function(files, callback) {
    var toLoad = 0, images = [], skipped = 0, i, n,

    // invoke the callback after some delay.
    // if immediately invoked, IE9 behaves weirdly. one of the images (random) is not rendered on the canvas
    done = Myntra.Utils.throttle(callback, 30),

    onload = function() {
        this.hasError = (this.naturalWidth === 0 || this.naturalHeight === 0 || this.complete === false);
        toLoad -= 1;
        toLoad === 0 && done(images);
    },

    onerror = function() {
        this.hasError = true;
        toLoad -= 1;
        toLoad === 0 && done(images);
    };

    for (i = 0, n = files.length; i < n; i += 1) {
        if (!files[i] || (files[i] instanceof Image)) {
            images[i] = files[i];
            skipped += 1;
            continue;
        }
        toLoad += 1;
        //console.log('loop: ' + i + ' toLoad: ' + toLoad);
        images[i] = new Image();
        images[i].onload = onload;
        images[i].onerror = onerror;
        images[i].src = files[i];
    }
    
    // invoke the callback immediately if all files are skipped. (ie., all files are Image instances or empty)
    if (skipped === files.length) {
        done(images);
    }
};


//Function to get length of an object
Myntra.Utils.objectLength = function(obj){
    var count = 0, k;
    for(k in obj){
        if(obj.hasOwnProperty(k)){
            count++;
        }
    }
    return count;
};

Myntra.Utils.TriggerLazyLoad = function(){
    $(".jquery-lazyload").lazyload({ 
        effect : "fadeIn",
        placeholder : cdn_base+"/skin2/images/loader_180x240.gif"
    });
    $(".jquery-lazyload").removeClass("jquery-lazyload");
};

Myntra.Utils.getJpegminiImgSrc = function(key, src) {
    var pos = src.lastIndexOf('.');
    if (pos == -1 || !Myntra.Data.jpegminiconf || !Myntra.Data.jpegminiconf[key]) return src;
    return src.substr(0, pos) + '_mini' + src.substr(pos);
};

/**
 * This is used to populate a Select Box with Json String
 * Json (object form) can be of two formats:
 * optionid1: optiontext1,
 * optionid2: optiontext2
 * or
 * optionid1: [optiontext1 extra1],
 * optionid2: [optiontext2 extra2],
 * jqComboObject:  
 */
Myntra.Utils.populateComboViaJson = function(json, jqSelectObject, defaultOptionText){
       var comboOptionsHtml = "";
       var jobj = (typeof json === "string")? $.parseJSON(json) : json;
       if(typeof defaultOptionText !== 'undefined'){
               comboOptionsHtml = "<option value='-1'>"+defaultOptionText+"</option>";
       }
       for(id in jobj){
               var value = jobj[id];
               var extraData="";
               var optionText;
               if(value && (value instanceof Array)){
                       optionText = value[1];
                       if(value.length > 2) {
                               extraData = value[2];
                       }
               }
               else if(value){
                       optionText = value;
               }
               comboOptionsHtml += "<option data-extra='"+extraData+"' value='"+id+"'>"+optionText+"</option>";
       } 
       jqSelectObject.html(comboOptionsHtml);
}