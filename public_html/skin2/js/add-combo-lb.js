//Construct the addedToCartBox here
Myntra.addedToComboBox = function(id){
	var cfg = {isModal:false};
	var adb = Myntra.LightBox(id,cfg);

	adb.init= function(){
		$('.complete-combo').on('click',function(e){
			_gaq.push(['_trackEvent', 'addedtocombopopup', 'completecombo']);
            Myntra.LightBox.hideAll();
		});
		$('.goto-bag').on('click',function(e){
			_gaq.push(['_trackEvent', 'addedtocombopopup', 'gotobag']);
        });


		$('.close').on('click',function (event) {
			 event.preventDefault();
			_gaq.push(['_trackEvent', 'addedtocombopopup', 'close']);
    		event.stopPropagation();

    	});
	};


	return adb;
};

Myntra.InitAddToCombo = function(){
	//CREATE A LIGHTBOX and SHOW LOADING....
	var tplBox = [
		'<div id="lb-addedtocombo" class="lightbox lb-addedtocombo">',
		' <div class="mod">',
		'  <div class="hd"></div>',
		'  <div class="bd">',
		'	<div class="item-details"><h4></h4><div class="size"></div><div class="price"></div></div>',
		'   <div class="btn-group"></div>',
		'   <div class="combo-status"></div>',	
		' </div></div>',
		'</div>'
	].join('');
	if(!$('#lb-addedtocombo').length){
		$('body').append(tplBox);
	}
	var adb = Myntra.addedToComboBox("#lb-addedtocombo");
	$(document).on('myntra.addedtocombo.show', function(e,data) {
		Myntra.Header.setCount('bag',data.resp.count);
		Myntra.Header.setBagTotal(data.resp.total);
		adb.show();
		adb.showLoading();
		$(document).trigger('myntra.addedtocombo.getcombostatus',data);
		// /adb.loadInfo(data);
	});
	$(document).on('myntra.addedtocombo.showloading', function(e) {
		adb.show();
		adb.showLoading();
	});
	//to get popup message details for combo
	$(document).on('myntra.addedtocombo.getcombostatus',function(e,styleObj){
		
		$.ajax({
			type:"GET",
			url:http_loc+"/comboController.php?mini=1&cid="+styleObj.cid,
			beforeSend:function(){
				adb.showLoading();
			},
			success: function(ret){
				adb.showLoading();
				var lbObj=$('#lb-addedtocombo .mod');
				var retVal = $.parseJSON(ret);
				var retObj = {};
				if(retVal.status =='success'){
					retObj = retVal.data;
					lbObj.find('.hd').html('<h3><span class="icon"></span>ADDED TO BAG AND COMBO</h3>');
					lbObj.find('.item-details h4').html(styleObj.name);
					lbObj.find('.item-details .size').html('<span>SIZE</span> '+styleObj.size + '/ <span>QTY</span>: 1');
					lbObj.find('.item-details .price').html('Rs. '+Myntra.Utils.formatNumber(styleObj.price));
					var _msg ='';
					//Get the proper message //Also use this block to communicate back to the combo page widget
					if(typeof retObj.productsSet.dre_minMore != 'undefined' && retObj.productsSet.dre_minMore > 0){
						//Show the message/status 
						if(retObj.productsSet.dre_buyCount){
							if(retObj.productsSet.dre_percent){
								//Buy X get %	
								_msg ='Buy '+retObj.productsSet.dre_minMore+' more '+retObj.productsSet.dre_placeHolderText+' and get '+retObj.productsSet.dre_percent + '% off on combo';
							}
							else if(retObj.productsSet.dre_amount){
								//Buy X get Rs
								_msg = 'Buy '+retObj.productsSet.dre_minMore+' more '+retObj.productsSet.dre_placeHolderText+' and get '+retObj.productsSet.dre_amount +' off on the combo ';
							}
							else if(retObj.productsSet.dre_freeItems){
								//Buy X get Free
								_msg = 'Buy '+retObj.productsSet.dre_minMore+' more '+retObj.productsSet.dre_placeHolderText+' and get '+retObj.productsSet.dre_itemName +' free';
								//lbObj.find('.combo-status').html('Buy '+retObj.productsSet.dre_minMore+' more '+retObj.productsSet.dre_placeHolderText+'and get '+retObj.productsSet.dre_itemName +' free');		
							}
							else{
								//Buy X get Y
								_msg = 'Buy '+retObj.productsSet.dre_minMore+' more '+retObj.productsSet.dre_placeHolderText+' and get '+retObj.productsSet.dre_count +' more '+retObj.productsSet.dre_placeHolderText+' free';
							}
							
						}
						else if(retObj.productsSet.dre_buyAmount){
							if(retObj.productsSet.dre_percent){
								//Buy Rs get %	
								_msg = 'Buy '+retObj.productsSet.dre_placeHolderText+' for Rs.'+retObj.productsSet.dre_minMore+' more and get '+retObj.productsSet.dre_percent + '% off on combo';
							}
							else if(retObj.productsSet.dre_freeItems){
								_msg = 'Buy '+retObj.productsSet.dre_placeHolderText+' for Rs.'+retObj.productsSet.dre_minMore+' more and get '+retObj.productsSet.dre_itemName +' free';
							}
							else {
								//Buy Rs get Rs
								_msg = 'Buy '+retObj.productsSet.dre_placeHolderText+' for Rs.'+retObj.productsSet.dre_minMore+' more and get '+retObj.productsSet.dre_amount + ' off on combo';
						}

						}

						lbObj.find('.combo-status').html(_msg);				
						var _comboUrl = '/comboController.php?cid='+styleObj.cid;
						if(window.location.pathname == '/comboController.php'){
							 _comboUrl = 'javascript:void(0)';
						}
						lbObj.find('.btn-group').html('<a href="'+Myntra.Data.cartPageUrl+'" class="btn primary-btn goto-bag">Go to bag</a><p>OR</p><a href="'+_comboUrl+'" class="btn normal-btn complete-combo">Complete combo</a>');
					}
					else {
						//will appear here only if combo is complete redirect user to cart
						//lbObj.find('.btn-group').html('<a href="/mkmycart.php" class="btn primary-btn goto-bag">Go to bag</a>');
						if(window.location.pathname!='/comboController.php'){
							window.location=Myntra.Data.cartPageUrl;	
						}
						else {
							lbObj.find('.combo-status').html('Combo Complete');
							lbObj.find('.btn-group').html('<a href="'+Myntra.Data.cartPageUrl+'" class="btn primary-btn goto-bag">Go to bag</a><p>OR</p><a class="btn normal-btn complete-combo">Add More to Combo</a>');
						}
						

					}
					//trigger an event which will be hanlded by the combo-widget
					adb.init();
					retObj.styleObj = styleObj;
					$(document).trigger('myntra.addedtocombo.done',retObj);
					adb.center();
				}else{
					lbObj.find('.combo-status').html('Ooops! something went wrong');
					lbObj.find('.btn-group').html('<a href="'+Myntra.Data.cartPageUrl+'" class="btn primary-btn goto-bag">Go to bag</a><p>OR</p><a class="btn primary-btn complete-combo">Add More to Combo</a>');
					adb.hide();
				}

				


			},
			complete:function(){
				adb.hideLoading();
			}

		});
	});
};


$(window).load(function() {
	
    Myntra.InitAddToCombo();
});

