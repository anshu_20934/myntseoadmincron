Myntra.newTabWidget = (function(){
	function tabWidget (){
		var _this = this;
		var tabLength = 0,
			recentActive = false;

		Myntra.Data.newTabWidget = {};

		_this.init = function(){
			var tabHeader = $('.mk-tabWidget').find('.mk-tabHeader'),
				tabHeaderWidth, unitWidth;
		
			if(Myntra.Utils.Cookie.get('RVC001')){
				tabLength++;
				recentActive = true;
				tabHeader.find('[data-rel="recent"]').removeClass('deactivated').addClass('activated');				
			}
			else{
				tabHeader.find('[data-rel="recent"]').hide();
			}
			if(Myntra.Data.newTabWidget.tabinfo){
				var popularIndex = 0;					
				for(var tempItem in Myntra.Data.newTabWidget.tabinfo){
					tabLength++;
					//Add Tab Header					
					tabHeader.find('.dummyHeader:last').clone().insertBefore('[data-rel="recent"]');
					tabHeader.find('.dummyHeader:last').removeClass('dummyHeader , deactivated').addClass('activated').attr('data-rel','popular-'+popularIndex).find('.text').text(tempItem);
					//Change data-rel-content for tab content
					var tabContent = $('.mk-tabWidget').find('.mk-tabContentWrap');
					tabContent.find('.mk-tabContent').eq(popularIndex+1).attr('data-rel-content','popular-'+popularIndex);
					popularIndex++;
				}
			}
			_this.attachStyles();
			_this.attachEvents();
		};

		_this.attachStyles = function(){
			var tabHeader = $('.mk-tabWidget').find('.mk-tabHeader'),
				tabHeaderWidth, unitWidth;
			tabHeader.find('.dummyHeader').remove();
			$('.mk-tabWidget').find('.dummyWidget').remove();
			$('.mk-tabWidget').show();
			tabHeaderWidth = tabHeader.width();
			unitWidth = (tabHeaderWidth/tabLength)-2;
			tabHeader.find('a').css('width',unitWidth+'px');
			_this.defaultSelectedItem();
		};

		_this.defaultSelectedItem = function(){
			if(!recentActive || Myntra.Data.newTabWidget.abValue == 'control'){
				$('.mk-tabWidget .mk-tabHeader').find('a:first').addClass('active').removeClass('inactive');
				$('.mk-tabWidget').find('.mk-tabContent:first').addClass('active').removeClass('inactive');
			}
			else if(recentActive && Myntra.Data.newTabWidget.abValue == 'test'){
				$('.mk-tabWidget .mk-tabHeader').find('[data-rel="recent"]').addClass('active').removeClass('inactive');
				$('.mk-tabWidget .mk-tabContentWrap').find('[data-rel-content="recent"]').addClass('active').removeClass('inactive').find('.rs-carousel').carousel('goToItem',0);
			}
		};

		_this.attachEvents = function(){
			$('.mk-tabHeader a').on('click',function(evt){
				evt.preventDefault();
				var evtObj = $(evt.target);
				if(evtObj.hasClass('text')){
					evtObj = evtObj.parent();
				}
				if(evtObj.hasClass('activated')&&evtObj.hasClass('inactive')){
					$('.mk-tabHeader a').addClass('active');
					$('.mk-tabContent').addClass('active');
					evtObj.addClass('active').removeClass('inactive');
					$('.mk-tabContentWrap').find('[data-rel-content="'+evtObj.attr('data-rel')+'"]').addClass('active').removeClass('inactive');
					$('.mk-tabContentWrap').find('[data-rel-content="'+evtObj.attr('data-rel')+'"]').find('.rs-carousel').carousel('goToItem',0);
				}
			});
		}
	};
	
	return new tabWidget();
}());
