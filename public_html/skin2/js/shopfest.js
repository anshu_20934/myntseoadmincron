
$(document).ready(function() {
    var tabs, tab, stab, scon;

    var timeLeft = function() {
        var now, endTime, diff, rem, ms, sec, min, hour, str;

        endTime = new Date();
        endTime.setHours(23);
        endTime.setMinutes(59);
        endTime.setSeconds(59);

        now = new Date();
        diff = endTime - now;
        ms = diff;

        rem = ms % 1000;
        ms -= rem;
        sec = ms / 1000;
        ms = rem;

        rem = sec % 60;
        sec -= rem;
        min = sec / 60;
        sec = rem;

        rem = min % 60;
        min -= rem;
        hour = min / 60;
        min = rem;

        str = '';
        if (hour > 0) {
            str += hour + ' ' + (hour === 1 ? 'hour' : 'hrs') + ' ';
        }
        if (min > 0) {
            str += min + ' ' + (min === 1 ? 'minute' : 'minutes') + ' ';
        }
        return str;
    };

    tabs = $('#shopfest > .tabs').on('click', 'li', function(e) {
        tab = $(e.currentTarget);
        if (tab.hasClass('selected')) {
            return;
        }

        // find the selected tab/content
        stab = stab || tabs.find('.selected');
        scon = scon || $('#' + stab.attr('id') + '-content');

        // derive the content div from the tab id
        tcon = $('#' + tab.attr('id') + '-content');

        // switch the selected 
        stab.removeClass('selected');
        scon.removeClass('selected');
        tab.addClass('selected');
        tcon.addClass('selected');

        // cache the selected tab/content
        stab = tab;
        scon = tcon;
		
		$('html,body').animate({'scrollTop': tab.offset().top});
    });

    var topContainer = $('#shopperday-toppers');
    var timeLeftNode = $('#shopperday .timeleft > strong').html(timeLeft());
    var tmrToppers = setInterval(function() {
        $.get('/shopfest.php?_action=toppers').complete(function(xhr, status) {
            if (status === 'parseerror') {
                // invalid json
            }
        }).success(function(data, xhr, status) {
            if (data.status === 'SUCCESS') {
                topContainer.html(data.markup);
                timeLeftNode.html(timeLeft());
            }
            else if (data.status === 'LOGIN') {
                $(document).trigger('myntra.login.show');
            }
        });
    }, 300000);
	
	if(window.location.hash) {
    	var hashValue = window.location.hash;
    	if($('#shopfest .tabs ' + hashValue).length) {
    		$('#shopfest .tabs ' + hashValue).trigger('click');
    	}
    }
});

$(window).load(function() {
	$('html,body').animate({'scrollTop': $('#shopfest > .tabs').offset().top});
});
