/* Product Grid JS 
(myntra.product-grid.js)
------------------------------------ */

$(window).load(function() {

	// Product Grid Banner ---------------
	$('a.mk-img-switch-top').click(function() {
		var IMG1 = $('a.mk-img-switch-top img').attr('alt');
		$('img.mk-banner').attr('src', IMG1);
	});

	$('a.mk-img-switch-bottom').click(function() {
		var IMG2 = $('a.mk-img-switch-bottom img').attr('alt');
		$('img.mk-banner').attr('src', IMG2);
	});

	// Checkboxes (Filters) ---------------
	/*function setupLabel() {
        if ($('.mk-label_check input').length) {
            $('.mk-label_check').each(function(){ 
                $(this).removeClass('mk-c_on');
            });
            $('.mk-label_check input:checked').each(function(){ 
                $(this).parent('label').addClass('mk-c_on');
            });                
        };
       	if ($('.mk-label_radio input').length) {
            $('.mk-label_radio').each(function(){ 
                $(this).removeClass('mk-r_on');
            });
            $('.mk-label_radio input:checked').each(function(){ 
                $(this).parent('label').addClass('mk-r_on');
            });
        };
        if ($('.mk-labelx_check input').length) {
            $('.mk-labelx_check').each(function(){ 
                $(this).removeClass('mk-labelx-c_on');
            });
            $('.mk-labelx_check input:checked').each(function(){ 
                $(this).parent('label').addClass('mk-labelx-c_on');
            });                
        };
    };
        
    $('.mk-label_check, .mk-label_radio, .mk-labelx_check').click(function(){
        setupLabel();
    });
    setupLabel();*/ 

    // Quick Look --------------------
    $(".mk-quicklook a").hover(
        function() {
            $(this).children('span').css('display', 'inline-block');
            $(this).parent('.mk-quicklook').siblings('a.mk-img, div.mk-product-brand').addClass('mk-opacity');
            $(this).parent().removeClass('mk-opacity');
            $(this).parent().siblings('img').addClass('mk-opacity');
        },
        function() {
            $(this).parent('.mk-quicklook').siblings('a.mk-img, div.mk-product-brand').removeClass('mk-opacity');
            $(this).children('span').hide();
            $(this).parent().siblings('img').removeClass('mk-opacity');
        }
    );
});

