
(function() { "use strict";

// check for canvas support
var elem = document.createElement('canvas');
Myntra.isCanvasSupported = !!(elem.getContext && elem.getContext('2d'));

// if no canvas support, show browser-upgrade message and redirect to the home page
if (!Myntra.isCanvasSupported) {
    var lbox = Myntra.LightBox('#lb-nocanvas-msg', {
        isModal: true,
        afterHide: function() {
            location.href = '/';
        }
    });
    if (true || $.browser.ie) {
        $('#lb-nocanvas-msg .ie').show();
    }
    myntrack('vtr', 'nosupport');
    lbox.show();
    return;
}

// show the vtr help screen to the first time visitor
if (!Myntra.Utils.Cookie.get('vtrhelp')) {
    Myntra.Utils.Cookie.set('vtrhelp', 1, 30);
    myntrack('vtr', 'starthelp', 'show');
    $('#vtr-first-run-help').show().on('click', function() { 
        $(this).hide();
        myntrack('vtr', 'starthelp', 'click');
    });
}

// scroll the window such that the main area comes to visible at the top of the viewport
setTimeout(function() {
    var h1 = $('#vtr > h1'),
    posTop = h1.offset().top + h1.outerHeight();
    if (Myntra.Data.newLayout) {
        posTop -= $('.mk-site-nav .mk-level-1').outerHeight() * 2;
    }

    if ($(window).scrollTop() === 0) {
        $('html,body').animate({scrollTop: posTop}, 600);
    }
}, 3000);

Myntra.VtrManager = {
    register: function(name, module) {
        name === 'room' ? this.room = module :
        name === 'products' ? this.products = module : '';
    },
    msg: function(from, message, data) {
        if (from === 'room') {
            if (message === 'style-in-use') {
                this.products && this.products.disableItems(data);
            }
            else if (message === 'style-added') {
                //this.products && this.products.disableItems(data);
            }
            else if (message === 'style-removed' || message === 'styles-removed') {
                this.products && this.products.enableItems(data);
            }
            else if (message === 'look-saved') {
                this.products && this.products.refreshLooks();
            }
        }
        else if (from === 'products') {
            if (message === 'style-selected') {
                this.room && this.room.addItem(data);
            }
            else if (message === 'get-query-params') {
                return this.room ? this.room.getQueryParams() : {};
            }
        }
    }
};

}()); // end of "use strict" wrapper

