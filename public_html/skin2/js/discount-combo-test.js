Myntra.sCombo.Test= {
	// Dont access them directly
	enabled: false,
	detected: false,
	enable: function(){
		Myntra.sCombo.Test.attr.enabled = true;
	},
	disable: function(){
		Myntra.sCombo.Test.attr.enabled = false;
	},
	isEnabled: function(){
		return Myntra.sCombo.Test.enabled;
	},
	exTest: function(){
		
	},
	o: function(cond,err){
		if(!cond) {
			console.log(err);
			Myntra.sCombo.Test.errorDetected=  true;
		}
	}

}

Myntra.sCombo.Test.Cases={
	flatConditional: function(){
		
	},
	freeGift: function(d){
		Myntra.sCombo.Test.errorDetected= false;
		//uid  to styleid
		if(d == undefined) d = 1;
		switch(d){
			case 1:
		var ids = {
			0 : {
				uid : 1, qty : 1, size: 39, skuid : 120232, price : 2599
			}
		};
			break;
			case 2:
        var ids = { 
            0 : {
                uid : 1, qty : 2, size: 39, skuid : 120232, price : 5198
            },
			1 : {
				uid : 0, qty : 3, size: 8, skuid : 165854, price : 5997
			}
        };
            break;
			default: console.log('unknown case'); return false;
		}

		var fprice = 0;

		for(var i in ids){
		var uid = ids[i].uid;
		var qty = ids[i].qty;
		var size= ids[i].size;
		var skuid = ids[i].skuid;
		var price = ids[i].price;
		fprice += price;

		Myntra.sCombo.Test.o($('#item_combo_lay_'+uid +' .checkbox').hasClass('checked'),uid+' not checked');
		Myntra.sCombo.Test.o(!$('#item_combo_lay_'+uid +' .main-flip').hasClass('hide'),uid+' mainflip shown');	
		Myntra.sCombo.Test.o($('#item_combo_lay_'+uid +' .edit-flip').hasClass('hide'),uid+' editflip not hidden');
		Myntra.sCombo.Test.o($('#item_combo_lay_'+uid +' .quantity').val()==qty,uid+' qty value is != '+qty);
		Myntra.sCombo.Test.o($('#item_combo_lay_'+uid +' .cow-size-loader').val()==skuid,uid+' skuid is!='+skuid);
		Myntra.sCombo.Test.o($('#item_combo_lay_'+uid +' .size-text-sub').html()==size,uid+' shown size !='+size);
		Myntra.sCombo.Test.o($('#item_combo_lay_'+uid +' .qnty-text-sub').html()==qty,uid+' shown qty !='+qty);
		}

		Myntra.sCombo.Test.o($('.combo-overlay-total .total').html()==fprice,'final amount != 2599  ');
		Myntra.sCombo.Styles.setCartData();
		Myntra.sCombo.Test.o(Myntra.sCombo.amount_in_combo == fprice, 'final amount != 2599  final amount = '+Myntra.sCombo.amount_in_combo);
//		Myntra.sCombo.Test.o(Myntra.sCombo.Styles.getAllChecked().length== ids.length,'checked length = ? ids.length= ?'+Myntra.sCombo.Styles.getAllChecked().length + ' '+ ids.length);
		if(!Myntra.sCombo.Test.errorDetected) return true;
		else return false;
	}
}

