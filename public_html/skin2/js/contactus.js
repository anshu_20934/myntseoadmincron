$(document).ready(function(){

    // invoke everything for contactus dynamic page only then when issueDeail JSON exists
	if(typeof issueDetail == "undefined"){
        return;
    }

    var issueDetailObjLength = issueDetail.length;

    // regEx
    var emailRegEx = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;

    var textAreaMaxChar = 250;

    // containers DOM and DHTML elements
    var issueLevel_1_Container = '#contactus-issue-level_1';
    var issueLevel_1_SelectBox = 'select#issue_level_1';

    var issueLevel_2_Container = '#contactus-issue-level_2';
    var issueLevel_2_SelectBox = "select#issue_level_2";

    var issueLevel_3_Container = '#contactus-issue-level_3';
    var issueLevel_3_SelectBox = 'select#issue_level_3';

    var issueFieldContainer = '#contactus-issue-fields';
    var errorDiv = '<div class="err"></div>';

    var subjectContainer = "#subject_wrapper";
    var subject = '<label>Subject *</label><input type="text" name="contactus_subject" maxlength="250"/>';

    var email = "input[name='contactus_login']";
    var captchaText = "input[name='captcha_text']";

    var defaultIssueLevel_1 = "select a category";
    var defaultIssueLevel_2 = "select a sub-category";
    var defaultIssueLevel_3 = "select a sub-category";

    // to set when captcha is verified
    var captchaVerifyResultFlag = false;


    // event handler for textarea counter
    $("textarea[name='contactus_detail']").bind('keyup keydown', function() {
        adjustTextCounter($(this));
    });

    // file upload initialization
    $('#contactus_file').MultiFile({
        max: 4,
        accept: 'gif|jpg|png',

        STRING: {
            file: '<em title="Click to remove" onclick="$(this).parent().prev().click()">&nbsp;&nbsp;$file</em>',
            //remove: '<img src="http://www.newui.com/skin2/images/sprite-global.png" height="16" width="16" alt="x"/>',
            remove:'remove',
            selected:'Selected: $file',
            denied:'Invalid file extension $ext!',
            duplicate:'You have already selected:\n$file!'

        },

        onFileRemove: function(element, value, master_element){
            $('#F9-Log').append('<li>onFileRemove - '+value+'</li>')
        },
        afterFileRemove: function(element, value, master_element){
            $('#F9-Log').append('<li>afterFileRemove - '+value+'</li>')
        },
        onFileAppend: function(element, value, master_element){
            $('#F9-Log').append('<li>onFileAppend - '+value+'</li>')
        },
        afterFileAppend: function(element, value, master_element){
            $('#F9-Log').append('<li>afterFileAppend - '+value+'</li>')
        },
        onFileSelect: function(element, value, master_element){
            $('#F9-Log').append('<li>onFileSelect - '+value+'</li>')
        },
        afterFileSelect: function(element, value, master_element){
            $('#F9-Log').append('<li>afterFileSelect - '+value+'</li>')
        }
    });


    // check for issues availability and add issue level 1
    if(issueDetailObjLength > 0){
        constructIssueLevel('level_1');
    }

    // initialize handler for issue level 1 click event
    var selectorLevel_1 = $(issueLevel_1_SelectBox);

    attachHandlerForLevel_1(selectorLevel_1);

    // initialize handler for issue level 1 click event
	function attachHandlerForLevel_1(selectorLevel_1){

        selectorLevel_1.on("change", function(event){

			// remove issue level2 and 3
			destructIssueLevel('level_2');

			// remove issue fields for all level 1, 2 and 3
			destructIssueLevelField('level_1');

            // get issue name and id to contruct issue levels and fields
            issueName = $(issueLevel_1_SelectBox +' option:selected').text();
			issueId = $(issueLevel_1_SelectBox +' option:selected').val();

			// get all level_2 issues for the selected level_1 issue
			constructIssueLevel('level_2', issueName);

			// get all level_1 fields
			constructIssueLevelField("level_1",issueId);

			// get subject selectively for 'other' type of issue
			addSubjectSelectively(issueName);

            // handle issue and fields action
            handleIssueAndFieldAction(selectorLevel_1);

            // initialize handler for issue level 2 click event
			var selectorLevel_2 = $(issueLevel_2_SelectBox);

			attachHandlerForLevel_2(selectorLevel_2);
		});
	}

    // initialize handler for issue level 2 click event
	function attachHandlerForLevel_2(selectorLevel_2){

        selectorLevel_2.on("change", function(event){
			// remove issue level3
			destructIssueLevel('level_3');

			// remove issue fields for level 2 and 3
			destructIssueLevelField('level_2');

            // get issue name and id to contruct issue levels and fields
			issueNameLevel_1 = $(issueLevel_1_SelectBox +' option:selected').text();
			issueNameLevel_2 = $(issueLevel_2_SelectBox +' option:selected').text();
			issueId = $(issueLevel_2_SelectBox +' option:selected').val();

			// get all level_2 issues for the selected level_1 issue
			constructIssueLevel('level_3', issueNameLevel_1, issueNameLevel_2);

			// get level_2 fields
			constructIssueLevelField("level_2",issueId);

            // handle issue and fields action
            handleIssueAndFieldAction(selectorLevel_2);

            // initialize handler for issue level 3 click event
			var selectorLevel_3 = $(issueLevel_3_SelectBox);

			attachHandlerForLevel_3(selectorLevel_3);
		});
	}

    // initialize handler for issue level 3 click event
	function attachHandlerForLevel_3(selectorLevel_3){

        selectorLevel_3.on("change", function(event){

			// remove issue fields for level 3
			destructIssueLevelField('level_3');

            // get issue id to contruct issue fields
			issueId = $(issueLevel_3_SelectBox +' option:selected').val();

			// get level_3 fields
			constructIssueLevelField("level_3",issueId);

            // handle issue and fields action
            handleIssueAndFieldAction(selectorLevel_3);
		});
	}

    // removes an issue level from DOM
	function destructIssueLevel(issueLevel){
		switch(issueLevel){
			case 'level_2'  :   // make level_2 and level_3 containers empty, before remove jqT class
								if($(issueLevel_2_Container).hasClass("jqtransformdone")){
									$(issueLevel_2_Container).removeClass("jqtransformdone");
								}

								if($(issueLevel_3_Container).hasClass("jqtransformdone")){
									$(issueLevel_3_Container).removeClass("jqtransformdone");
								}

								$(issueLevel_2_Container).empty();
								$(issueLevel_3_Container).empty();

								break;

			case 'level_3'  :   // make level_3 container empty
								if($(issueLevel_3_Container).hasClass("jqtransformdone")){
									$(issueLevel_3_Container).removeClass("jqtransformdone");
								}
								$(issueLevel_3_Container).empty();
								break;

		}
	}

    // removes an issue field/s from DOM
	function destructIssueLevelField(issueLevel){

		switch(issueLevel){
			case 'level_1'  :   $(issueFieldContainer).find('*').detach();
								break;

			case 'level_2'  :   $(issueFieldContainer + ' #level_2_field').find('*').detach();
								$(issueFieldContainer + ' #level_3_field').find('*').detach();
								break;

			case 'level_3'  :   $(issueFieldContainer + ' #level_3_field').find('*').detach();
								break;
		}
	}

    // creates an issue Level by adding DOM elements
	function constructIssueLevel(issueLevel, issueNameLevel_1, issueNameLevel_2){

		// initialize issue level select box html
		issueLevelHTML = '';

		switch(issueLevel){

			case "level_1" :    defaultOption = '<option value="" selected="selected">'+defaultIssueLevel_1+'</option>';
								$.each(issueDetail, function(idx, issue){
									if( (issue.issue_level_1 != null && issue.issue_level_1 != "") &&
										(issue.issue_level_2 == null || issue.issue_level_2 == "") &&
										(issue.issue_level_3 == null || issue.issue_level_3 == "")
										){
										issueLevelHTML += '<option value="'+issue.issue_id+'" >'+issue.issue_level_1+'</option>';
									}
								});

								selectBoxAttributes = {'name':'issue_level_1','id':'issue_level_1'};
								issueLevelContainer = issueLevel_1_Container;
								break;

			case "level_2" :    if(issueNameLevel_1 != null && issueNameLevel_1 != ""){
									defaultOption = '<option value="" selected="selected">'+defaultIssueLevel_2+'</option>';
									$.each(issueDetail, function(idx, issue){
										if( (issue.issue_level_1 == issueNameLevel_1) &&
											(issue.issue_level_2 != null && issue.issue_level_2 != "") &&
											(issue.issue_level_3 == null || issue.issue_level_3 == "")
											){
											issueLevelHTML += '<option value="'+issue.issue_id+'" >'+issue.issue_level_2+'</option>';
										}
									});

									selectBoxAttributes = {'name':'issue_level_2','id':'issue_level_2'};
									issueLevelContainer = issueLevel_2_Container;
								}

								break;

			case "level_3" :    if(issueNameLevel_2 != null && issueNameLevel_2 != ""){
									defaultOption = '<option value="" selected="selected">'+defaultIssueLevel_3+'</option>';
									$.each(issueDetail, function(idx, issue){
										if( (issue.issue_level_1 == issueNameLevel_1) &&
											(issue.issue_level_2 == issueNameLevel_2) &&
											(issue.issue_level_3 != null && issue.issue_level_3 != "")
											){
											issueLevelHTML += '<option value="'+issue.issue_id+'" >'+issue.issue_level_3+'</option>';
										}
									});

									selectBoxAttributes = {'name':'issue_level_3','id':'issue_level_3'};
									issueLevelContainer = issueLevel_3_Container;
								}
								break;

		}

		// wrap options by <select> and append it to the container with error div
		if(issueLevelHTML != ''){

			issueLevelHTML = defaultOption + issueLevelHTML;
			$(issueLevelHTML)
				.wrapAll("<select></select>")
				.parent()
				.attr(selectBoxAttributes)
				.appendTo($(issueLevelContainer));

			$(issueLevelContainer).append(errorDiv);

			// initialize jqTransform for all built(dynamically) select box before .on() event
			// because selected elements need to be in DOM before handler works on it
			$(issueLevelContainer).jqTransform();
		}
	}

    // creates an issue field/s by adding DOM elements
	function constructIssueLevelField(issueLevel, issueId){
		issueFieldHTML = '';

		$.each(issueDetail, function(idx, issue){

			if( issue.issue_id == issueId ){

                // need to have check on fields availability(IE gives problem as undefined)
                if(issue.field != false && typeof issue.field != 'undefined'){

                    $.each(issue.field, function(idxField, field){
                        if(field.field_type == 'mandatory'){
                            issueFieldHTML += '<label class="mandatory">'+field.field_label+' *</label>';
                        } else {
                            issueFieldHTML += '<label>'+field.field_label+'</label>';
                        }

                        if(field.maxchar != "0" && field.maxchar != null){
                            issueFieldHTML += '<input class="field" type="text" name="field['+field.issue_field_id+']" maxlength="'+field.maxchar+'"/>';
                        } else {
                            issueFieldHTML += '<input class="field" type="text" name="field['+field.issue_field_id+']"/>';
                        }

                        issueFieldHTML += '<div class="err"></div>';
                    });
                }
			}
		});

		switch(issueLevel){
			case 'level_1'  :   wrapperDivAttributes = {"id":"level_1_field"};
								break;

			case 'level_2'  :   wrapperDivAttributes = {"id":"level_2_field"};
								break;

			case 'level_3'  :   wrapperDivAttributes = {"id":"level_3_field"};
								break;
		}

		if(issueFieldHTML != ''){
			$(issueFieldHTML)
				.wrapAll("<div></div>")
				.parent()
				.attr(wrapperDivAttributes)
				.appendTo($(issueFieldContainer));
		}
	}

    // add subject if issue type is 'other' at level 1
	function addSubjectSelectively(issueName){

		var issueName = issueName.toLowerCase();

		if(issueName == "other" || issueName == "others"){
			// remove the existing subject and add new
			$(subjectContainer).detach();
			$(subject)
				.wrapAll('<div></div>')
				.parent()
				.attr({"id":"subject_wrapper"})
				.insertAfter(issueFieldContainer);

			$(subjectContainer).append(errorDiv)

		} else {
			// remove the existing subject
			$(subjectContainer).detach();
		}
	}

    // handle issue and fields action for a selected issue
    function handleIssueAndFieldAction(selectorLevel){

        $.each(issueDetail, function(idx, issue){

            // run action for selected issue and attach handler for its field/s action
            if(selectorLevel.val() == issue.issue_id){

                var issueAction = $.trim(issue.action);

                if(issueAction != ''){

                    // get compoenents invoke, param and populate of the issue action
                    var issueActionComponents = issueAction.split("#");

                    // get components vaule/s
                    var invoke = issueActionComponents[0].split(":");// invoke[1] is function
                    var param = issueActionComponents[1].split(":");// param[1] is/are param/s
                    var populate = issueActionComponents[2].split(":");// populate[1] is/are field/s need to be populated

                    // check whether invoking function exists(to avoid undefined function and stop executing)
                    // works only if referring function is outside document.ready()
                    if(($.isFunction(window[invoke[1]]))){
                        var callFunc = eval(invoke[1]);
                        callFunc(param[1], populate[1]);
                    }
                }


                // need to have check on fields availability(IE gives problem as undefined)
                if(issue.field != false && typeof issue.field != 'undefined'){

                    // attach handler to invoke field action on focus
                    $.each(issue.field, function(idxField, field){

                        var fieldAction = $.trim(field.action);

                        if(fieldAction != ''){
                            // get compoenents invoke, param and populate of the field action
                            var fieldActionComponents = fieldAction.split("#");

                            // get components vaule/s
                            var invokef = fieldActionComponents[0].split(":");// invoke[1] is function
                            var paramf = fieldActionComponents[1].split(":");// param[1] is/are param/s
                            var populatef = fieldActionComponents[2].split(":");// populate[1] is/are field/s need to be populated

                            // check whether invoking function exists(to avoid undefined function and stop executing)
                            // works only if referring function is outside document.ready()
                            if(($.isFunction(window[invokef[1]]))){
                                var callFunc = eval(invokef[1]);

                                // attach event
                                $('input[name="field\\['+field.issue_field_id+'\\]"]').on("click", function(){
                                    callFunc(paramf[1], populatef[1]);
                                });
                            }
                        }

                    });
                }
            }
        });
    }

	function adjustTextCounter(textAreaObj){

		var TextLength = parseInt(textAreaObj.val().length);

		// adjust text length in textarea
		textAreaObj.val(textAreaObj.val().substring(0,textAreaMaxChar-1));

		var counterDiv = '<div id="counter_div" style="float: right;color:#333;">'+TextLength+'<span style="color:#808080;">&nbsp;/&nbsp;'+textAreaMaxChar+' chars</span></div>';
		$("#counter_div").remove();

		if(TextLength >0){
			textAreaObj.after(counterDiv);
		}
	}

    //##### order overlay events #####
    $("#order-being-processed").click(function(){
        $(this).addClass('active');
        $("#order-completed").removeClass('active');
        $(".order-list-being-processed").show();
        $(".order-list-completed").hide();
    });

    $("#order-completed").click(function(){
        $(this).addClass('active');
        $("#order-being-processed").removeClass('active');
        $(".order-list-being-processed").hide();
        $(".order-list-completed").show();


    });

    // when overlay loaded show completed orders
    $("#order-completed").trigger("click");

    // to show tool-tip for an order
    $('.order-id-overlay').hover(
        function(e) {
            var orderid = parseInt($(this).text());

            var tip;
            if (!$(this).data('tip')) {
                tip = $(this).next().appendTo('body');
                $(this).data('tip', tip);
            }
            tip = $(this).data('tip');
            tip.e = e;

            tip.show();
            var pt = $(this).offset(),
                x = pt.left + (-8),
                y = Math.max(0, pt.top - tip.height() - 30);

            tip.css({'left': x + 'px', 'top': y + 'px'});
        },
        function() {
            $(this).data('tip').hide();
        }
    );

    $('.order-list').delegate('.jqTransformRadio', 'hover', function(e){

		if(e.type=='mouseenter'){
            var orderid = parseInt($(this).parent().next().text());
            var tip;
            if (!$(this).parent().next().data('tip')) {
                tip = $(this).parent().next().next().appendTo('body');
                $(this).parent().next().data('tip', tip);
            }
            tip = $(this).parent().next().data('tip');
            tip.e = e;

            tip.show();
            var pt = $(this).parent().next().offset(),
                x = pt.left + (-8),
                y = Math.max(0, pt.top - tip.height() - 30);

            tip.css({'left': x + 'px', 'top': y + 'px'});

        } else {
		    $(this).parent().next().data('tip').hide();
		}	
    });
  
    //##### order overlay events #####

	//#########validation###########
	// invoke form validation
    $("#contactus_submit").click(function(){
		validateContactUsForm();
	});

    // invoke form validation
	$("input[name='captcha_text']").bind('keydown',function(event) {

		if (event.keyCode == '13') {
			event.preventDefault();
			validateContactUsForm();
		}
	});

    // validates the contactus form on submit
	function validateContactUsForm(){

		var flag = true;

		
		// validate issue levels
		if(flag){
			flag = validateIssueLevel_1();
		}
		if(flag){
			flag = validateIssueLevel_2();
		}
		if(flag){
			flag = validateIssueLevel_3();
		}

		// validate issue mandatory fields
		if(flag){
			flag = validateIssueField();
		}

		// validate subject, if issue level 1 is 'other/others'
		if(flag){
			flag = validateSubject();
		}

        // verify captcha after validating all other form elems
        if(flag){
            verifyCaptcha();
        }

		// captchaVerifyResultFlag is set to true by verifyCaptcha() on authentic captcha
		// which is called(and should be) before the function
		if(flag && captchaVerifyResultFlag){
			//submit the form
			$("form[name='contactus_form']").submit();

		} else {
			return false;
		}
	}

	function validateAgainstRegex(regEx, data){
		return regEx.test(data);
	}

    // validates email
	function validateEmail(){

	    // FGloginCondition is the flag set in ocontactus-dynamic.tpl
        // to check login condition in feature gate
        if(!$(email).length && FGloginCondition == 'true'){// assuming (1 && 0) condition never occurs
            // open login popup lightbox
            $(document).trigger('myntra.login.show');
            return false;

        } else {
            var emailObj = $(email);
            if(!validateAgainstRegex(emailRegEx, emailObj.val())){
                emailObj.next("div.err").text("Enter a valid email address");
                emailObj.focus();
                return false;

            } else {

                emailObj.next("div.err").text("");
                return true;
            }
        }
    }

    // validates level1 issue
	function validateIssueLevel_1(){
		var containerObj = $(issueLevel_1_Container);
        var selectedOptionObj = $(issueLevel_1_SelectBox +' option:selected');
		var invalidIndex = defaultIssueLevel_1;

		if(containerObj.contents().length > 0){
			// no level 1 is selected
			if(selectedOptionObj.text() == invalidIndex){
				containerObj.find(".err").text("Enter a valid issue type");
				return false;

			} else {
				containerObj.find(".err").text("");
				return true;
			}

		} else {
			// level 1 issue should exist
			return false;
		}
	}

    // validates level 2 issue
	function validateIssueLevel_2(){
		var containerObj = $(issueLevel_2_Container);
        var selectedOptionObj = $(issueLevel_2_SelectBox +' option:selected');
		var invalidIndex = defaultIssueLevel_2;

		if(containerObj.contents().length > 0){
			// no level 2 is selected
			if(selectedOptionObj.text() == invalidIndex){
				containerObj.find(".err").text("Enter a valid issue type");
				return false;

			} else {
				containerObj.find(".err").text("");
				return true;
			}

		} else {
			// no level 2 issue
			return true;
		}
	}

    // validates level 3 issue
	function validateIssueLevel_3(){
		var containerObj = $(issueLevel_3_Container);
        var selectedOptionObj = $(issueLevel_3_SelectBox +' option:selected');
		var invalidIndex = defaultIssueLevel_3;

		if(containerObj.contents().length > 0){
			// no level 3 is selected
			if(selectedOptionObj.text() == invalidIndex){
				containerObj.find(".err").text("Enter a valid issue type");
				return false;

			} else {
				containerObj.find(".err").text("");
				return true;
			}

		} else {
			// no level 3 issue
			return true;
		}
	}

    // validates issue field for mandatory
	function validateIssueField(){

		hasError = false;
		var invalidValue = '';

		if($(issueFieldContainer).contents().length > 0){

			$(issueFieldContainer).find('.mandatory').each(function(key,fieldLabel){

				if($.trim($(fieldLabel).next('input').val()) == invalidValue){
					$(fieldLabel).next().next(".err").text("Enter mandatory field marked as *");
					hasError = true;
					return false;// break

				} else {
					$(fieldLabel).next().next(".err").text("");
					hasError = false;
					return true;// continue
				}
			});

			return !hasError;

		} else {
			// on fields for issue/s
			return true;
		}
	}

    // validates subject for mandatory
	function validateSubject(){
		if($(subjectContainer).length){
			if($(subjectContainer).find('input').val() == ''){
				$(subjectContainer).find(".err").text("Enter subject line for your query");
				$(subjectContainer).find("input").focus();
				return false;

			} else {
				$(subjectContainer).find(".err").text("");
				return true;
			}

		} else {
			// there is no subject DOM, no point of validating it
			return true;
		}
	}

    // verifies captcha
	function verifyCaptcha() {
		var captchaTextObj = $(captchaText);
        var token = document.getElementsByName('_token')[0].value;
        makePostAjax(http_loc+'/captcha/captchaVerification.php', 'userInputCaptcha='+captchaTextObj.val()+'&uniqueId=contactUsPage'+'&token='+token, false, false, captchaCallBack);
	}

	function makePostAjax(url, inputData, cache, async, callBack){

		$.ajax({
			type:'POST',
			url: url,
			data: inputData,
			cache: cache,
			async: async,
			success: function(data){callBack(data)}

		});
	}

	function captchaCallBack(result){

		var captchaTextObj = $(captchaText);
		var result = $.trim(result);

		if(result == 'false'){
			captchaTextObj.next("div.err").text("Enter the text you see above");
			captchaTextObj.focus();

			//needed to validate captcha verification on submit
			captchaVerifyResultFlag = false;

		} else {
			captchaTextObj.next("div.err").text("");

			//needed to validate captcha verification on submit
			captchaVerifyResultFlag = true;
		}
	}
	//#########validation###########
});

// NOTE: These following functions need to be outside document.ready()
// otherwise it wont be identified by window[].

/** This is the action invoked on change of issue type and focus on field
  * @param: string param - '' | email
  * @param: string populate -  order-field-id, return-field-id | x | ,y | ''
  * @return : void - shows overlay for order
  */
function orderOverlay(param, populate){

    // initialize light boxes
    var boxes = {};

    // element id to be shown in the light box
    var overlayId = '';

    // check for param validity and mandatory
    if(param == '' || typeof param == 'undefined' || param != 'email'){
        return;

    } else if(param == 'email' && Myntra.Data.userEmail == ''){
        return;

    } else {
        overlayId = 'contactus-order-overlay';
    }

    if(overlayId != ''){

        // transform radio buttons in overlay
        $('#contactus-order-overlay').jqTransform();

        if (!boxes[overlayId]) {
            boxes[overlayId] = Myntra.LightBox('#' + overlayId);
            boxes[overlayId].show({isModal:true});
        }
    }

    // check for populate validity and then attach handler
    if(populate != ''){
        orderOverlayPopulate(populate,boxes[overlayId]);
    }
}

/**
 * attaches handler to populate fields post action
 * @param: string populate - orderfieldId(x),returnfieldId(y) | x | ,y | '' - order of the param is important
 * @param: obj boxes - overlay object
 * @return: void - populates the fields mentioned by populate parameter
 */
function orderOverlayPopulate(populate, boxes){

    populateFields = populate.split(',');

    // attach populate handler
    if((typeof populateFields[0] != 'undefined' && populateFields[0] != '')
            && (typeof populateFields[1] != 'undefined' && populateFields[1] != '')){

        $("input[name='orderid']").off('click').on('click',function(e){

            var orderIdField = $('input[name="field\\['+populateFields[0]+'\\]"]');

            // TODO: need to change for return id later
            var returnIdField = $('input[name="field\\['+populateFields[1]+'\\]"]');

            if(orderIdField.prev().hasClass("mandatory")){
                orderIdField.attr("readonly",true)
            }

            // TODO: need to change for return id later
            if(returnIdField.prev().hasClass("mandatory")){
                returnIdField.attr("readonly",true)
            }

            orderIdField.val($(this).parent().next('div').text());

            // TODO: need to change for return id later
            returnIdField.val($(this).parent().next('div').text());

            boxes.hide();
        });

    } else if((typeof populateFields[0] != 'undefined' && populateFields[0] != '')
                && (typeof populateFields[1] == 'undefined' || populateFields[1] == '')){

        $("input[name='orderid']").off('click').on('click',function(e){

            var orderIdField = $('input[name="field\\['+populateFields[0]+'\\]"]');

            if(orderIdField.prev().hasClass("mandatory")){
                orderIdField.attr("readonly",true)
            }

            orderIdField.val($(this).parent().next('div').text());
            boxes.hide();
        });

    // TODO: need to change for return id later
    } else if((typeof populateFields[0] == 'undefined' || populateFields[0] == '')
                && (typeof populateFields[1] != 'undefined' || populateFields[1] != '')){

        $("input[name='orderid']").off('click').on('click',function(e){

            var returnIdField = $('input[name="field\\['+populateFields[1]+'\\]"]');

            if(returnIdField.prev().hasClass("mandatory")){
                returnIdField.attr("readonly",true)
            }

            returnIdField.val($(this).parent().next('div').text());
            boxes.hide();
        });
    }
}
