Myntra.WizardExchangeShipping = function(cfg, conf) {
    var content, wiz,
        isReset = true,
        sizeSelected = false,
        addresServiceable = false,
        obj = Myntra.WizardStep();
    obj.set('title', 'Address and Size');
    obj.set('ajax_url', '/myntra/ajax_exchange.php');
    obj.init = function() {
        content = obj.get('content');
        wiz = obj.get('wiz');
        //alert("wiz data option is " : wiz.data('option'));
        $([
            '<div class="step-shipping">',
            '   <ul>',
            '       <li>',
            '   	    <div class="label">CONFIRM PICKUP &amp; DELIVERY ADDRESS</div>',
            '           <div class="address"></div>',
            '           <input type="hidden" name="address" value="">',
            '           <div class="mk-address-error mk-hide"><em></em>WE’RE SORRY, EXCHANGE IS NOT OFFERED AT THIS LOCATION.</div>',
            '           <div style="text-decoration:underline;cursor:pointer;" class="choose_address">Choose a Different Address</div>',
            '           <div class="return-this-item"> <div> OR </div> <div style="text-decoration:underline;cursor:pointer;text-transform:uppercase;" class="return-this-link" > Return this item </div></div>',
            '       </li>',
            '       <li>',
            '           <div class="label">Select a Size</div>',
            '   	    <br/><p class="you_ordered"></p>',
            '           <div class="options mk-product-option-cont">',
            ' 	    </div>',
            '       </li>',
            '   </ul>',
            '</div>'
        ].join('')).appendTo(content);
        content.jqTransform();
        wiz.disableNext('Please select a size');

        $('.exchangebox .return-to-fork').show();
        obj.update();

        $(document).bind('myntra.address.list.done.exchange', function(e, data) {
            wiz.data('address', data);
            addresServiceable = true;
            content.find('.mk-address-error').hide();
            content.find('.return-this-item').hide();
            obj.validate();
            var address = wiz.data('address');
            var addrMarkup = [];
            addrMarkup.push('<div class="name">' + address.name + '</div>');
            addrMarkup.push('<div>' + address.address.replace("\n", "<br>") + '</div>');
            address.locality && addrMarkup.push('<div>' + address.locality + '</div>');
            addrMarkup.push('<div>' + address.city + ' - ');
            if (address.pincode != undefined) {
                addrMarkup.push(address.pincode);
            } else {
                addrMarkup.push(address.zipcode);
            }
            addrMarkup.push('</div>');


            addrMarkup.push('<div>' + address.state + '</div>');
            addrMarkup.push('<div><label>Mobile:</label> ' + address.mobile + '</div>');
            content.find('input[name="address"]').val(address);
            content.find('.address').html(addrMarkup.join(''));
        });

        content.find('.address').html(cfg.addrMarkup);

    };

    obj.onAjaxSuccess = function(resp, name) {
        var sizeButtons = " ";
        content.find('.address').html(cfg.address_markup);
        sizeSelected = false;
        wiz.disableNext('Please select a size');
        var itemSkus = resp.sizes;
        //var itemSkus = [{"size":"S","optionid":"289487","sku_id":"296105","is_available":false,"sku_count":0},{"size":"M","optionid":"289488","sku_id":"296104","is_available":true,"sku_count":2},{"size":"L","optionid":"289489","sku_id":"296103","is_available":false,"sku_count":10},{"size":"XL","optionid":"289486","sku_id":"296106","is_available":true,"sku_count":1}];
        //var itemSkus = [["S",false,null],["M",false,null],["L",false,null],["XL",false,null]];
        if (typeof(itemSkus[0].is_available) !== 'undefined') {
            for (var i = 0; i < itemSkus.length; i++) {

                if (itemSkus[i].is_available) {
                    sizeButtons += '<button class="btn size-btn available ' + ((wiz.data("option") == itemSkus[i].size) ? 'size-oredered' : '') + '" data-count="' + itemSkus[i].sku_count + '"  value="' + itemSkus[i].optionid + '" data-sku="' + itemSkus[i].sku_id + '">' + itemSkus[i].size + '</button> ';
                } else {
                    sizeButtons += '<button class="btn size-btn unavailable ' + '" data-count="' + i + '" value="' + itemSkus[i].optionid + '" >' + itemSkus[i].size + '<span class="strike"></span></button>';
                }

            }

            var sizeChart = '';
            if (resp.sizeRepresentationImageUrl === null) {
                if (resp.size_chart === null || resp.size_chart == '') {
                    var sizeChart = '';
                } else {
                    var sizeChart = '<div id="sizechart-old" style="text-decoration:underline;cursor:pointer;" src="' + resp.size_chart + '" data-src="' + resp.size_chart + '" >SIZE CHART</div>';
                }
            } else {
                var sizeChart = '<div id="sizechart-new" style="text-decoration:underline;cursor:pointer;">NOT SURE OF YOUR SIZE?</div>';
            }

            var sizeMarkup = [
                '			    <input type="hidden" name="mk-size" class="mk-size" value="">',
                '			    ' + sizeButtons + '  ',
                '      			<div class="mk-size-guides">' + sizeChart + '</div>',
                '	        		<div class="mk-hide mk-count-message"><em></em>NOTE : ONLY <span>0</span> UNIT(S) LEFT</div>',
                '               <div class="mk-hide mk-same-size-message"><em></em>NOTE : YOU HAVE SELECTED THE SAME SIZE</div>'
            ]
            content.find('.options').html(sizeMarkup.join(' '));
        } else {
            sizeMarkup = '<div class="mk-ooo-error mk-hide" style="display: block;"><em></em>WE’RE SORRY, THIS PRODUCT IS CURRENTLY SOLD OUT.</div>';
            content.find('.options').html(sizeMarkup);
        }

        var wizObj = wiz;
        var sizeConf = {
            curr_style_id: wizObj.data("styleId"),
            allProductOptionDetails: resp.allProductOptionDetails
        }
        $(document).trigger('myntra.exchangesizechart.create', sizeConf);
        //Size Selection and other messaging handle
        //enable  button, show messages and tie it on this js
        content.find('.size-btn.available').on('click', function() {
            if ($(this).hasClass('selected')) {
                return;
            }
            sizeSelected = true;
            obj.validate();
            el = $(this);
            var sizeDropCont = el.closest('.mk-product-option-cont');
            el.siblings('.size-btn').removeClass('selected');
            el.addClass('selected');
            var cntMsg = sizeDropCont.find('.mk-count-message');
            el.siblings('.mk-size').val(el.data('sku'));
            sizeDropCont.find('.add-button-group').removeClass('mk-hide');
            sizeDropCont.find('.notify-cont').addClass('mk-hide');
            wiz.data('skuCount', el.data('count'));
            (el.data('count') <= 3) ? cntMsg.removeClass('mk-hide').find('span').text(el.data('count')) : cntMsg.addClass('mk-hide');
            //this is for exchange
            if (el.hasClass('size-oredered')) {
                //show the warning message message
                content.find('.mk-same-size-message').show();
            } else {
                content.find('.mk-same-size-message').hide();
            }
        });




        //Check whether address is serviceable if not show the rror message and disable next button
        if (!resp.serviceable) {
            addresServiceable = false;
            content.find('.mk-address-error').show();
            content.find('.return-this-item').show();
            obj.validate();
        } else {

            addresServiceable = true;
            content.find('.mk-address-error').hide();
            content.find('.return-this-item').hide();
            obj.validate();
            wiz.data('address', obj.get('address'));
        }

    };

    obj.update = function() {
        $('.exchangebox .close').show();

        if ($('.exchangebox .close').html() === null) {
            var closeMarkup = '<div class="close"></div>';
            $('.exchangebox .mod').append(closeMarkup);
            var close = $('.exchangebox .mod .close');
            var wizObj = obj.get('wiz');
            close.click(function(e) {
                wizObj.hide();
            });
        }

        $('.exchangebox .return-to-fork').show();

        obj.set('zipcode', cfg.zipcode);
        obj.set('address', cfg.address);
        wiz.data('old_option', wiz.data("option"));
        content.find('.you_ordered').html('You ordered : ' + wiz.data("old_option") + ' <br/> WHAT SIZE DO YOU WANT TO EXCHANGE IT FOR?');

        content.find('.return-this-link').click(function() {
            $('.exchangebox .close').click();
            conf.cfg._type = 'return';
            $(document).trigger('myntra.return.show', conf);
        });

        $('.exchangebox .return-to-fork').click(function() {
            $('.exchangebox .close').click();
            $(document).trigger('myntra.fork.show', conf);
        });

        content.find('.choose_address').click(function() {
            wiz.data('ship-opt', $(this).val());
            var conf = {
                _type: 'exchange',
                isModal: true,
                displayName: 'exchange',
                itemid: wiz.data('itemId')
            };
            $(document).trigger('myntra.address.list.show', conf);
        });

        obj.ajax({
            _view: 'size',
            zipcode: obj.get('zipcode'),
            itemId: wiz.data('itemId'),
            styleId: wiz.data('styleId')
        }, 'init');
    };

    obj.reset = function() {};

    obj.next = function() {
        var newsize = content.find('.options .selected');
        wiz.data('option', newsize.html());
        wiz.notify('next');
    };
    obj.validate = function() {
        //validate whether address is serviceable and size is selected
        if (addresServiceable && sizeSelected) {
            wiz.enableNext();
        } else {
            if (!addresServiceable)
                wiz.disableNext('Unfortunately we do not offer exchange at your location.You can still return the item by clicking on the "Return This Item" link.');
            if (!sizeSelected)
                wiz.disableNext('Please select a size');
        }
    }

    return obj;
};



Myntra.WizardExchangeReasonPolicy = function() {
    var content, wiz, agree, items,
        isReset = true,
        reason, details, qty, lqty, skuCount
        obj = Myntra.WizardStep();
    obj.set('title', 'Policy Agreement');
    obj.set('ajax_url', '/myntra/ajax_exchange.php');

    obj.init = function() {
        content = obj.get('content');
        wiz = obj.get('wiz');
        wiz.data('orig_qty', wiz.data('qty'));
        skuCount = wiz.data('skuCount');

        $([
            '<div class="step-agree">',
            '    <div class="label"> Exchange Policy </div>',
            '    <label>Please review our',
            '            <a href="/faqs#returns" target="_blank">Returns &amp; Exchange Policy</a></label>',
            '   <ol class="statements">',
            '       <li>',
            '           <input type="checkbox" name="i-agree" value="1">',
            '           I confirm that the product is unused and with original tags.',
            '       </li>',
            '    </ol>',
            '</div>'
        ].join('')).appendTo(content);

        agree = content.find('input[name="i-agree"]');
        items = content.find('.statements > li');
        agree.click(function() {
            var li = $(this).parent();
            li.next().length && li.next().removeClass('hide');
            obj.validate();
        });
        obj.agreeReset();
        obj.agreeUpdate();

        qty = content.find('select[name="qty"]');
        if (qty) {
            obj.validate();
            qty.change(function() {
                obj.validate();
            });
        }
        reason = content.find('select[name="reason"]');
        if (reason) {
            obj.validate();
            reason.change(function() {
                obj.validate();
            });
        }
        obj.ajax({
            _view: 'reason'
        }, 'init');

    };

    obj.agreeUpdate = function() {
        agree.filter(':not(:checked)').length ? wiz.disableNext('Please agree to the policy to proceed') : wiz.enableNext();

        qty = content.find('select[name="qty"]');
        if (qty) {
            if (wiz.data('qty') > 1 && !qty.val()) {
                wiz.disableNext('Please select a quantity');
            }
        }
        reason = content.find('select[name="reason"]');
        if (reason) {
            if (!reason.val()) {
                wiz.disableNext('Please select a reason for exchanging the item');
            }
        }
    };

    obj.agreeReset = function() {
        agree.removeAttr('checked');
        items.addClass('hide').eq(0).removeClass('hide');
    };


    obj.update = function() {

        $('.exchangebox .return-to-fork').hide();
        agree.filter(':not(:checked)').length ? wiz.disableNext('Please agree to the policy to proceed') : wiz.enableNext();

        qty = content.find('select[name="qty"]');
        if (qty) {
            if (wiz.data('qty') > 1 && !qty.val()) {
                wiz.disableNext('Please select a quantity');
            }
        }
        reason = content.find('select[name="reason"]');
        if (reason) {
            if (!reason.val()) {
                wiz.disableNext('Please select a reason for exchanging the item');
            }
        }

        // update the UI only after a reset
        if (!isReset && (skuCount == wiz.data('skuCount'))) {
            return;
        }

        skuCount = wiz.data('skuCount');
        var val = (wiz.data('qty') < wiz.data('skuCount') ? wiz.data('qty') : wiz.data('skuCount'));

        if (val > 1) {
            var markup = ['<option value="">Select a quantity</option>'];
            for (var i = 1; i <= val; i += 1) {
                markup[markup.length] = '<option value="' + i + '">' + i + '</option>';
            }
            qty.html(markup.join(' '));
            lqty.show();
            qty.parent().show();
        } else {
            qty.html('<option value="1" selected="true">1</option>');
            lqty.hide();
            qty.parent().hide();
        }
        isReset = false;
        wiz = obj.get('wiz');
        $('.exchangebox .exchg_size').html(' & Exchange for Size:<span style="color:black;">' + wiz.data('option') + '</span>');
        obj.validate();
    };

    obj.onAjaxSuccess = function(resp, name) {
        $(resp.markup).appendTo(content);
        wiz.data('amtCredit', resp.amtCredit);
        //wiz.setFootNote('Need help? ' + resp.customerSupportTime + ' on <em>' + resp.customerSupportCall + '</em>');
        wiz.setFootNote('');

        reason = content.find('select[name="reason"]');
        details = content.find('textarea[name="details"]');
        qty = content.find('select[name="qty"]');
        lqty = content.find('.lbl-qty');

        reason.change(function() {
            obj.validate();
        });
        qty.change(function() {
            obj.validate();
        });
        obj.update();
    };

    obj.reset = function() {
        reason.val('');
        details.val('');
        qty.val('');
        isReset = true;
    };

    obj.validate = function() {
        wiz.enableNext();
        reason = content.find('select[name="reason"]');
        if (reason) {
            if (!reason.val()) {
                wiz.disableNext('Please select a reason for exchanging the item');
            }
        }
        qty = content.find('select[name="qty"]');
        if (qty) {
            if (wiz.data('orig_qty') > 1 && !qty.val()) {
                wiz.disableNext('Please select a quantity');
            }
        }
        if (agree.filter(':not(:checked)').length) {
            wiz.disableNext('Please agree to the policy to proceed');
        }
    };

    obj.next = function() {
        wiz.data('reason', reason.val());
        wiz.data('details', details.val());
        wiz.data('qty', qty.val());
        wiz.data('i-agree', 'yes');
        wiz.notify('next');
    };

    return obj;
};

Myntra.WizardExchangeSummary = function(cfg) {
    var content, wiz,
        cacheRefund = {},
        obj = Myntra.WizardStep();

    obj.set('cfg', cfg);
    obj.set('title', 'Summary');
    obj.set('ajax_url', '/myntra/ajax_exchange.php');

    obj.init = function() {
        content = obj.get('content');
        wiz = obj.get('wiz');
        $([
            '<div class="step-summary">',
            '   <div class="product-details"></div>',
            '   <div class="address"></div>',
            '</div>'
        ].join('')).appendTo(content);
        obj.update();
    };

    var num = function(amount) {
        return Myntra.Utils.formatNumber(amount) + '.00';
    }

    obj.update = function() {
        var address = wiz.data('address');
        content = obj.get('content');
        content.find('.product-details').html("<div class='pcode'>Product Code : " + wiz.data('styleId') + " </div> " + wiz.data('styleName') + ", QTY:" + wiz.data('qty') + " <br/> Ordered Size :  " + wiz.data('old_option') + ", Exchanged Size:" + wiz.data('option') + "<br/><div><img src='" + cfg.img_url + "' /></div>");
        var addrMarkup = [];
        addrMarkup.push('<div class="name">' + address.name + '</div>');
        addrMarkup.push('<div>' + address.address.replace("\n", "<br>") + '</div>');
        address.locality && addrMarkup.push('<div>' + address.locality + '</div>');
        addrMarkup.push('<div>' + address.city + ' - ');
        if (address.pincode != undefined) {
            addrMarkup.push(address.pincode);
        } else {
            addrMarkup.push(address.zipcode);
        }
        addrMarkup.push('</div>');
        addrMarkup.push('<div>' + address.state + '</div>');
        addrMarkup.push('<div><label>Mobile:</label> ' + address.mobile + '</div>');

        content.find('.address').html(addrMarkup.join(''));

        //content.find('.address').html('<label> Pickup Address </label>' + address);

    };

    return obj;
};


Myntra.WizardExchangeConfirm = function() {
    var content, wiz,
        obj = Myntra.WizardStep();
    obj.set('title', '');
    obj.set('ajax_url', '/myntra/ajax_exchange.php');
    obj.set('hide_title', 1);

    obj.init = function() {
        content = obj.get('content');
        wiz = obj.get('wiz');
        obj.update();
    };

    obj.reset = function() {
        content.html('');
    };

    obj.onAjaxError = function(resp, name) {
        content.html(resp.markup);
        wiz.notify('error');
    };

    obj.onAjaxSuccess = function(resp, name) {

        //pickup changes
        if (resp && resp.data && resp.data.pickup_excluded) {
            wiz.notify('Exchange is not allowed for this product');
        } else {
            content.html(resp.markup);
            $('.exchangebox .close').hide();
            wiz.notify('done');
            wiz.setTitle('<span class="success-icon"></span> <span>Exchange Request Submitted!</span>');
            $(document).trigger('myntra.return.done', resp.data);
        };
    }

    obj.update = function() {
        var params = $.extend({
            _view: 'confirm'
        }, wiz.data());
        if (params.address.id) {
            //send just the ID else send full address
            params.addressId = params.address.id;
            delete params.address;
        }
        $.extend(params, {
            _token: Myntra.Data.token
        });
        obj.ajax(params, 'next', 'POST');
    };

    return obj;
};


Myntra.WizardExchangeBox = function(conf) {
    var cfg = conf.cfg || {};
    cfg.css = 'exchangebox';

    var id = '#lb-item-exchange',
        obj = Myntra.WizardBox(id, cfg),
        step1 = Myntra.WizardExchangeShipping(cfg, conf),
        step2 = Myntra.WizardExchangeReasonPolicy(),
        step3 = Myntra.WizardExchangeSummary(cfg);
    step4 = Myntra.WizardExchangeConfirm();

    obj.add(step1);
    obj.add(step2);
    obj.add(step3);
    obj.add(step4);

    var p_show = obj.show;
    obj.show = function(conf) {
        p_show.apply(obj, arguments);
        obj.setTitle('Exchange Item');
        obj.setSubTitle(obj.data('styleName') + ', Return Size:<span style="color:black;">' + obj.data('option') + '</span> <span class="exchg_size"></span>');
    };

    return obj;
};


$(document).ready(function() {
    var obj;
    $(document).bind('myntra.exchange.show', function(e, cfg) {
        obj = obj || Myntra.WizardExchangeBox(cfg);
        obj.show(cfg);
    });

});


$(document).ready(function() {
    var lbox;
    $(document).bind('myntra.fork.show', function(e, cfg) {
        lbox = lbox || Myntra.LightBox('#fork-exchange');
        var conf = cfg;
        var choice = {
            return_type: 'return'
        };

        var content = $('.forkbox .bd');
        content.html('');
        var ft = $('.forkbox .ft');
        ft.html('');

        $([
            '<div class="step-fork">',
            '   <div class="fork-choice">',
            '   <p class="how"> What would you like to do?</p>',
            '   <ul>',
            '       <li>',
            '           <input type="radio" name="return-opt" value="return" class="return-opt-ret" />',
            '           <div class="return-type">Return this item</div>',
            '       </li>',
            '       <li>',
            '           <input type="radio" name="return-opt" value="exchange" class="return-opt-exc" />',
            '           <div class="return-type">exchange this item for a different size</div>',
            '       </li>',
            '   </ul>',
            '<div class="qty">',
            '</div>',
            '</div>',
            '<div class="product-details"></div>',
            '</div>'
        ].join('')).appendTo(content);

        /*
        var val = content.data.qty;
        if (val > 1) {
            var markup = ['<select><option value="">Select a quantity</option>'];
            for (var i = 1; i <= val; i += 1) {
                markup[markup.length] = '<option value="' + i + '">' + i + '</option>';
            }
	    markup.push('</select>');
	    qty = content.find('.qty');
            qty.html(markup.join(' '));
        }
        else {
	    qty = content.find('.qty');
            qty.html('Quantity : <select><option value="1" selected="true">1</option></select>');
        }
        isReset = false;

        */

        //pickup changes

        if (conf && conf.data && conf.data.pickup_excluded) {
            content.find('.step-fork ul li:eq(1)').hide();
        } else {
            content.find('.step-fork ul li:eq(1)').show();
        }

        content.find('input[type="radio"]').jqTransRadio();
        content.find('.product-details').html(
            "<div class='pcode'>Product Code : " +
            conf.data.styleId + " </div> " +
            conf.data.styleName +
            ", QTY:" + conf.data.qty +
            " <br/> Ordered Size :  " +
            conf.data.option +
            "<br/><div><img src='" +
            conf.cfg.img_url +
            "' /></div>");

        $('.forkbox .ft').html([
            '<!--span class="note">Need help? Call us on <em>+91-80-43541999</em></span-->',
            '<button class="btn normal-btn btn-ret-type" title="Please choose how you want to return the item" disabled="disabled" data-return-key="' + conf.cfg.key +
            '">Next &raquo;</button>'
        ].join(''));


        $('.forkbox input:radio[name=return-opt]').click(function(e) {
            choice.return_type = $(this).val();
            var btnNext = $('.forkbox .btn-ret-type');
            if (btnNext.is(":disabled")) {
                btnNext.removeAttr('disabled');
                btnNext.attr('title', '');
            }
        });

        //alert($('.lightbox .btn-fork').html);


        $('.forkbox .btn-ret-type').click(function(e) {
            //hide so that user doesnt see this window again /prevent from placing the exchange/return request again
            lbox.hide();
            if (choice.return_type == 'exchange') {
                conf.cfg._type = 'exchange';
                if (conf && conf.data && !conf.data.pickup_excluded)
                    $(document).trigger('myntra.exchange.show', conf);
            } else {
                conf.cfg._type = 'return';                
                $(document).trigger('myntra.return.show', conf);
            }


        });
        //show is moved from top to  bottom so that it is centered in viewport
        lbox.show(conf);
    });

});