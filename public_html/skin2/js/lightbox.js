	
Myntra.LightBox = function(id, cfg) {
    cfg = cfg || {};

    var panel = $(id);
    if (!panel.length) {
        panel = $('#' + id);
        if (!panel.length) { throw new Error("Lightbox root node can't be found with id: " + id); }
    }
        
    // if a template is used in a popup and in a page
    cfg.isPopup = (typeof cfg.isPopup === 'undefined') ? true : cfg.isPopup;
    cfg.isModal = (typeof cfg.isModal === 'undefined') ? false : cfg.isModal;
    cfg.disableCloseButton = (typeof cfg.disableCloseButton === 'undefined') ? false : cfg.isModal;

    // track the no of show/hide so that we do some cleanup/actions at the last hide
    Myntra.LightBox.lbShownCount = Myntra.LightBox.lbShownCount || 0;

    // track the references of the shown lightboxes
    Myntra.LightBox.boxes = Myntra.LightBox.boxes || [];

    // move the panel into immediate child of the body.
    if (cfg.isPopup && !panel.parent().is('body')) {
        $('body').append(panel);
    }

    var obj = Myntra.MkBase(),
        mod = panel.find('.mod'),
        close = panel.find('.close'),
        shim = $('#lightbox-shim'),
        progress = null,
        body = mod.find('.bd');

    if (!shim.length) {
        shim = $('<div class="lightbox-shim" id="lightbox-shim"></div>').appendTo('body');
    }

    if (cfg.isPopup) {
        if (!close.length && !cfg.disableCloseButton) {
            close = $('<div class="close"></div>').appendTo(mod);
            close.click(function(e) {
                //Hooking callback when a user explicitly closes Lightbox. 
                obj.beforeClose();
                obj.hide();
                obj.afterClose();
	        });
        }
        obj.center = function() {
            var dy = Math.round(($(window).height() - mod.outerHeight(false)) / 2);
            if (dy < 20) {
            	dy = 20;
            	mod.css('margin-bottom', dy + 'px');
        	}
            mod.css('margin-top', dy + 'px');
        };
        $(window).resize(function() {
            obj.center();
        });
    }
    else {
        obj.center = function(){};
    }

    panel.click(function(e) {
        if (obj.isModal()) { return; }
        $(e.target).is(panel) && obj.hide();
    });

    obj.getId = function() {
        return panel.attr('id');
    };

    obj.isModal = function() {
        return cfg.isModal;
    };

    obj.isPopup = function() {
        return cfg.isPopup;
    };

    obj.showLoading = function() {
        if (!$(id + ' .loading').length) {
            mod.append('<div class="loading"></div>');
        }
        if (!progress) {
            progress = $(id + ' .loading');
        }
        progress.show();
    };

    obj.hideLoading = function() {
        progress && progress.hide();
    };

    obj.showCloseButton = function() {
        close.show();
    };

    obj.hideCloseButton = function() {
        close.hide();
    };

    obj.beforeShow = function() {
        if (typeof cfg.beforeShow === 'function') {
            cfg.beforeShow.call(obj);
        }
        else if (cfg.evtPrefix) {
            $(document).trigger(cfg.evtPrefix + '.beforeShow');
        }
    };

    obj.afterShow = function() {
        if (typeof cfg.afterShow === 'function') {
            cfg.afterShow.call(obj);
        }
        else if (cfg.evtPrefix) {
            $(document).trigger(cfg.evtPrefix + '.afterShow');
        }
    };

    obj.show = function(a_cfg) {
        var prevLB;
        $.extend(cfg, a_cfg);

        // prevent showing the same popup over and over
        // (for example double clicking a link the triggers showing the same popup 2 times)
        if (Myntra.LightBox.boxes.length && Myntra.LightBox.boxes[Myntra.LightBox.boxes.length - 1] === obj) {
            return;
        }

        // hide the previous box, if exists
        prevLB = Myntra.LightBox.boxes.pop();
        if (prevLB) {
            prevLB.tmpHide();
            Myntra.LightBox.boxes.push(prevLB);
        }

		obj.beforeShow();
        if (Myntra.LightBox.lbShownCount <= 0) {
            Myntra.LightBox.scrollTop = $(window).scrollTop();
            $('html,body,.mk-body').css({'overflow':'hidden', 'margin-right':'5.5px'});
            $('.go-to-top-btn.right').css('right', '37px');
            $(window).scrollTop(Myntra.LightBox.scrollTop);
        }
        panel.fadeIn();
        obj.center();
        shim.show();
		obj.afterShow();

        Myntra.LightBox.lbShownCount += 1;

        // push the last box
        Myntra.LightBox.boxes.push(obj);
	};

    obj.beforeHide = function() {
        if (typeof cfg.beforeHide === 'function') {
            cfg.beforeHide.call(obj);
        }
        else if (cfg.evtPrefix) {
            $(document).trigger(cfg.evtPrefix + '.beforeHide');
        }
    };

    obj.afterHide = function() {
        if (typeof cfg.afterHide === 'function') {
            cfg.afterHide.call(obj);
        }
        else if (cfg.evtPrefix) {
            $(document).trigger(cfg.evtPrefix + '.afterHide');
        }
    };
    // Called before when a user explicitly closes the lightbox
    obj.beforeClose = function() {
        if (typeof cfg.beforeClose === 'function') {
            cfg.beforeClose.call(obj);
        }
        else if (cfg.evtPrefix) {
            $(document).trigger(cfg.evtPrefix + '.beforeClose');
        }
    };
    // Called after when a user explicitly closes the lightbox
    obj.afterClose = function() {
        if (typeof cfg.afterClose === 'function') {
            cfg.afterClose.call(obj);
        }
        else if (cfg.evtPrefix) {
            $(document).trigger(cfg.evtPrefix + '.afterClose');
        }
    };

    obj.hide = function() {
        var prevLB;

		obj.beforeHide();
        if (progress) { progress.hide(); }
        panel.fadeOut(400, function() {
            Myntra.LightBox.lbShownCount -= 1;
            if (Myntra.LightBox.lbShownCount <= 0) {
            	$('.go-to-top-btn.right').css('right', '20px');
            	$('html,body,.mk-body').css({'overflow':'auto','margin-right':'0'});
                $(window).scrollTop(Myntra.LightBox.scrollTop);
            }
        });
        shim.hide();
        obj.afterHide();

        // pop the last box
        Myntra.LightBox.boxes.pop(obj);

        // show the previous box, if exists
        prevLB = Myntra.LightBox.boxes.pop();
        if (prevLB) {
            prevLB.tmpShow();
            Myntra.LightBox.boxes.push(prevLB);
        }
    };

    obj.tmpShow = function() {
        panel.show();
        shim.show();
        obj.center();
	};

    obj.tmpHide = function() {
        panel.hide();
        shim.hide();
    };

    obj.clearPopupContent = function(){
    	body.html('');
    }
    
    if (panel.is('.infobox')) {
    	panel.find('.hd .title').prepend('<span class = "info-icon"></span>');
    	panel.find('.ft').append('<button class="btn small-btn normal-btn btn-ok">Close</button>');
    	panel.find('.ft .btn-ok').click( function() {
    		obj.hide();
    	});
    }

    return obj;
};

// when ESC key pressed, hide only the top most box
$(document).keyup(function(e) {
    if (27 === e.keyCode && Myntra.LightBox.boxes) {
        var n = Myntra.LightBox.boxes.length;
        if (!n) { return }
        var box = Myntra.LightBox.boxes[n - 1];
        if (box && !box.isModal()) {
            box.hide();
        }
    }
});

Myntra.LightBox.hideAll = function() {
    var lb, i;
    for (i = Myntra.LightBox.boxes.length - 1; i >= 0; i--) {
        lb = Myntra.LightBox.boxes[i];
        lb.hide();
    }
    Myntra.LightBox.boxes.length = 0;
};

Myntra.MsgBox = (function(){
    var obj, title, hd, bd;

    var init = function() {
        var h = '';
        h += '<div id="lb-msg-box" class="lightbox">';
        h += '<div class="mod">';
        h += '    <div class="hd">';
        h += '      <h2></h2>';
        h += '  </div>';
        h += '  <div class="bd">';
        h += '  </div>';
        h += '  <div class="ft">';
        h += '      <button class="btn normal-btn small-btn btn-ok">Close</button>';
        h += '  </div>';
        h += '</div>';
        h += '</div>';
        $('body').append(h);
        obj = Myntra.LightBox('#lb-msg-box');
        title = $('#lb-msg-box h2');
        hd = $('#lb-msg-box .hd');
        bd = $('#lb-msg-box .bd');
        $('#lb-msg-box .btn-ok').click(function(e) { obj.hide() });
    };

    return {
        show: function(a_title, content, cfg) {
            if (!obj) { init(); }
            title.text(a_title);
            a_title ? hd.show() : hd.hide();
            bd[0] && (bd[0].innerHTML = content); //Avoid execution of script;
            obj.show();
            if (cfg && cfg.autohide) {
                setTimeout(function() { obj.hide() }, cfg.autohide);
            }
            return obj;
        },

        hide: function() {
            obj.hide();
        }
    };
})();

