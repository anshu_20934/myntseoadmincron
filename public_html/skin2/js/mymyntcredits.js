/** IE < 9 fix for indexOf function **/
if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length >>> 0;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}
(function() { "use strict";

var tplBox = [
'<div id="lb-coupon-transfer" class="lightbox lb-coupon-transfer">',
'   <div class="mod">',
'        <div class="hd">',
'               <h2><span class="lbl">TRANSFER COUPON</span></h2>',
'        </div>',
'       <div class="bd">',
'			<div class="success-msg mk-hide"><span class="icon"></span><span class="msg"></span></div>',
'           <div class="details">',
'              <div class="coupon-block">',
'                 <ul>',
'                 	<li>COUPON:  <span class="coupon-code"></span></li>',
'                 	<li>DESCRIPTION:  <span class="coupon-description"></span></li>',
'                 	<li>EXPIRY:  <span class="coupon-expiry"></span></li>',
'                 </ul>',
'              </div>',
'			   <p>Please enter the email address of the recipient you’d like to transfer this coupon to.</p>',
'			   <p>The coupon will be sent to the recipient via email and will be available on their <br / >My Myntra / Mynt Credits page.</p>',
'              <div class="coupon-form">',
'					<label>EMAIL:  </label><input type="text" name="email" class="email">',
'                   <input type="hidden" name="coupon">',
'					<div class="err-msg"></div>',
'                   <p>Please verify the email address you’ve entered as a coupon transfer cannot be cancelled once initiated.</p>',
'				</div>',
'              <button class="btn primary-btn btn-proceed small-btn">Transfer Coupon</button>',
'              <button class="btn normal-btn btn-cancel small-btn">Close</button>',
'           </div>',
'       </div>',
'   </div>',
'</div>'
].join('');

Myntra.CouponTransfer =  { 
	init: function(){
		this.root = $(tplBox).appendTo('body');
		this.email=this.root.find('input.email');
		this.btnSave=this.root.find('.btn-proceed').on('click', $.proxy(this.validate, this));
		this.btnCancel=this.root.find('.btn-cancel').on('click', $.proxy(this.hide, this));
		this.errMsg=this.root.find('.err-msg');
		this.successMsg=this.root.find('.success-msg');
		this.couponCode=this.root.find('.coupon-code');
		this.couponDesc=this.root.find('.coupon-description');
		this.couponExp=this.root.find('.coupon-expiry');
		this.cBox = Myntra.LightBox('#lb-coupon-transfer');
		this.cBoxMod = this.root.find('.mod');
		this.cBoxHead = this.root.find('.hd');
		this._initialized = true;
        this.targetClicked = '';
        this.couponCodeValue='';
	},

	show: function(couponData){
		!this._initialized && this.init();
		this.targetClicked = couponData.elm;
		this.couponCode.html(couponData.coupon);
		this.couponDesc.html(couponData.desc);
		this.couponExp.html(couponData.exp);
		this.couponCodeValue=couponData.coupon;
		this.successMsg.siblings().show();
		this.cBox.show();
		var that=this;
		this.cBox.beforeHide=function(){
			that.errMsg.html('');
			that.email.val('');
			that.successMsg.find('.msg').text('');
			that.successMsg.hide();
		}
	},
	
	hide : function(){
		this.cBox && this.cBox.hide();
	},
	
	save : function(){
		var that=this;
		$.ajax({
            type: 'POST',
            url:'CouponTransfer.php',
            data: "_token=" + Myntra.Data.token+"&coupon="+that.couponCodeValue+"&email="+this.email.val(),
            beforeSend:function(){
            	that.cBox.showLoading();
			},
            success: function(data){
            	data = jQuery.parseJSON(data);
                that.cBox.hideLoading();
                if(data.status=='success'){
                	console.log('succccees');
                	that.successMsg.siblings().hide();
                	that.successMsg.show().find('.msg').text(data.message);
   	               	setTimeout(function(){
   	               		location.reload();
                		that.cBox.hide();
                	}, 2000);
                	
                }
                else {
                	that.errMsg.text(data.message);
                 }
            }
       });
	},
	validate : function(){
	   if(!this.email.val()){
		   this.errMsg.text('Please enter your email address');
		   return false;
	   }
	   else if (!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(this.email.val())) {
		   this.errMsg.text('Please enter a valid email address');
	   }
	   else{
		   this.save();
	   }
	 
	}

};

Myntra.mymyntra = Myntra.mymyntra ? Myntra.mymyntra : {};

Myntra.mymyntra.MyntCredits = {
	showTabLoading : function(){
		this.$tabContent.html('<div class="loading"></div>');
	},
	init : function(){
		this.$tabContent = $(".mk-mynt-tabs-content"); //store tab content dom
		this.$tabContainer = $(".mk-mynt-tabs");
		this.defaultSection = Myntra.Data.defaultMyntcreditTab ? Myntra.Data.defaultMyntcreditTab : "myprivilege";//By default show myprivilege
		this.initEvents();
		this.activeAjaxReq = null;
		if(window.location.hash){
			var tabName = this.getSectionByHash();
			this.loadTab(tabName);
		}else{
			window.location.hash = this.defaultSection;//If hash is not there change the hash. This will load the default tab automatically
		}
	},
	loadTab :  function(tabName){
		var _this = this;
		//tabName should be mycoupons, mycashback or myprivilege
		if(['myprivilege','mycoupons','mycashback'].indexOf(tabName) == -1){
			return;
		}
		var url = "";
		if (tabName === 'mycoupons') url="/mymyntra.php?view=myprivilege&subview=mycoupons";
		else if (tabName === 'mycashback') url="/mymyntra.php?view=myprivilege&subview=mycashback";
		else if (tabName === 'myprivilege') url="/mymyntra.php?view=myprivilege&subview=myprivilege";
		if(!url) return;
		// var aTabsLi = this.$tabContainer.find('ul.main-subtabs > li');
		this.$tabContainer.find('ul.main-subtabs > li').removeClass('my-mynt-selected-tab');
		this.$tabContainer.find('ul.main-subtabs ul.subtabs').hide();
		this.$tabContainer.find('a[href="#'+tabName+'"]').parent().addClass('my-mynt-selected-tab');
		_this.showTabLoading();
		if(_this.activeAjaxReq) _this.activeAjaxReq.abort();
		_this.activeAjaxReq = $.get(url,function(data, textStatus, jqXHR){
			_this.$tabContent.html(data);
			_this.$tabContainer.find('a[href="#'+tabName+'"]').parent().find('ul.subtabs').show();
			_this.initSection(tabName);
		});
	},
	initSection : function(sectionName){
		if(sectionName == 'mycoupons'){
			var carttooltipcoupon = new Myntra.Tooltip(null, '#coupon-exclusion-tt', {side: 'below', useTitle: true});
		}
		if(sectionName == 'myprivilege'){
			// $('.lp-activation-modal .terms-n-conditions').jScrollPane();
			$('td.timeago').timeago();
		}
	},
	getSectionByHash:function(){
		// section will be #myloyalty-how-it-works, #myloyalty
		var returnVal = this.defaultSection;
		var hashString = window.location.hash;
		if(hashString){
			hashString = hashString.indexOf("-") == -1 ? hashString.substr(1) : hashString.substring(1,hashString.indexOf('-'));
			if(hashString === 'mycoupons') returnVal = 'mycoupons';
			else if(hashString === 'myprivilege') returnVal = 'myprivilege';
			else if(hashString === 'mycashback') returnVal = 'mycashback';
		}
		return returnVal;
	},

	loadRecentActivity : function(start,size,cb){
		var url="/getLoyaltyTransactionHistory.php";
		var	_this = this;
		var data = {start:start,size:size,_token:Myntra.Data.token};
		$.ajax({
            type: 'GET',
            url:url,
            data: data,
            success: function(res){
				cb(null,res);
            }
       });

	},

	initEvents : function(){
		var _this = this;
		$(window).on('hashchange', function() {
		 	var hashString = window.location.hash;
		 	if(hashString === '#mycoupons') _this.loadTab('mycoupons');
		 	else if(hashString === '#myprivilege') _this.loadTab('myprivilege');
		 	else if(hashString === '#mycashback') _this.loadTab('mycashback');
		});

		var $root = $('html, body');
		_this.$tabContainer.on('click','.subtabs a',function(e){
			e.preventDefault();
			// Make a smooth scrolling for the link 
			$root.animate({
		        scrollTop: $( $.attr(this, 'href') ).offset().top
		    }, 900);
		    return false;
		});
		_this.$tabContent.on('click','.mk-mynt-promo-codes h6',function(){
			$(this).parent().find(".mk-coupons-content").slideToggle();
		});
		_this.$tabContent.on('click','.mk-mynt-credits h6',function(){
			$(this).parent().find(".mk-cashback-content").slideToggle();
		});
		_this.$tabContent.on('click','.loyalty-welcome-msg > .close',function(){
			$(this).closest('.loyalty-welcome-msg').slideUp('slow');
		});
		_this.$tabContent.on('click','.see-more-activity',function(){
			var $seeMoreActivity = $(this);
			if(!$seeMoreActivity.find('.see-more-activity-label').is(':visible')){
				return;
			}
            var activityTable = $('.recent-activity-table'),
                $thStart = $('input[name="transactionHistoryStart"]'),
            	start = parseInt($thStart.val()),
                $thSize = $('input[name="transactionHistorySize"]'),
            	size = parseInt($thSize.val()),
                tblHeight = activityTable.outerHeight(true);

            if(!activityTable.parent().hasClass('recent-activity-mask')){
                activityTable.wrap('<div class="recent-activity-mask" style="height:'+tblHeight+'px"/>');
            }
            $seeMoreActivity.find('.see-more-activity-label').hide();
            $seeMoreActivity.find('.showLoading').show();
            _this.loadRecentActivity(start,size,function(error,result){
            	if(error || (result && result.status == 'ERROR')){
            		$seeMoreActivity.html('<span class="error red">Oops !! An error occured while fetching your recent activity. Please try again after some time.</span>');
            		return;
            	}
            	start = start + size;
            	$thStart.val(start);
            	var transHistory = result.content;
            	var showMoreLink = result.showMoreRecentActivity;
            	for (var i = 0; i < transHistory.length; i++){
            		var creditInflow = parseInt(transHistory[i].creditInflow);
            		var creditOutflow = parseInt(transHistory[i].creditOutflow);
            		var stat = creditInflow ? "+" + creditInflow + ' Points': "-" + creditOutflow + ' Points';
	           		// var modifiedOn = parseInt(transHistory[i].modifiedOn);
	           		// modifiedOn = new Date(modifiedOn);
	           		var modifiedOn = transHistory[i].modOn;
            		var description = transHistory[i].description;
            		var $tr = activityTable.find('tr:first-child').clone().appendTo(activityTable);
            		$tr.find('.points-stat').html(stat);
            		$tr.find('.points-activity-desc').html(description);
            		var $dateTd = $tr.find('.points-activity-date');
            		$dateTd.html(modifiedOn);
            		$dateTd.attr('title',transHistory[i].modOnIso);
            		if(transHistory[i].timeAgo){
            			$dateTd.timeago();
            		}
            	};
            	$seeMoreActivity.find('.see-more-activity-label').show();
            	$seeMoreActivity.find('.showLoading').hide();
	            var newHeight = activityTable.outerHeight(true);
	            activityTable.parent().animate({height:newHeight+"px"},{duration:500,complete:function(){
	                if(!showMoreLink){
	                    $seeMoreActivity.html('');
	                    $seeMoreActivity.addClass('see-more-disabled');
	                }    
	            }});
            });

		});
		_this.$tabContent.on('click','.share-coupon button',function(el){
			var datElm=$('#coupon-'+$(this).data('coupon-code')); 
			Myntra.CouponTransfer.show({
				elm:el,
				coupon:$(this).data('coupon-code'),
				desc:datElm.find('td:eq(1)').html(),
				exp:datElm.find('td:eq(2)').html()
			}); 
		});

		_this.$tabContent.on('click','.read-more-tnc',function(el){
			var $clickedLink = $(this),
				$mask = $clickedLink.closest('.loyalty-rules-terms-condition').find('.recent-activity-mask'),
				$tncUl = $mask.find('ul.top-ul');
			// var $tncUl = $('.loyalty-rules-terms-condition ul.top-ul');
			// var oldHeight = $tncUl.outerHeight(true);
			// $tncUl.wrap('<div class="recent-activity-mask" style="height:'+oldHeight+'px"/>');
			// $tncUl.find('li').show();
			var newHeight = $tncUl.outerHeight(true);
			$mask.animate({height:newHeight+"px"},{duration:800,complete:function(){
				$clickedLink.remove();
			}});
		});

		//disabled-btn .terms-n-condition-control input[type="checkbox"]
		_this.$tabContent.on('change','.terms-n-condition-control input[type="checkbox"]',function(){
			var $checkBox = $(this);
			if ($checkBox.is(':checked')) {
        		$('.terms-n-condition-control .agree-btn').removeClass('disabled-btn');
		    } else {
		        $('.terms-n-condition-control .agree-btn').addClass('disabled-btn');
		    }
		});

		_this.$tabContent.on('click','.terms-n-condition-control .agree-btn',function(e){
			var $button = $(this),
				$tncControl = $button.closest('.terms-n-condition-control');
			e.preventDefault();
			if($button.hasClass('disabled-btn')) return;
			// create a ajax call to accept terms and conditions. 
			// We need to reload the whole page in this case as header values needs to be refreshed as well.
			$('.lp-activation-modal-static .loading').show();
			var showTncError = function(){
				$tncControl.html('<span class="error">There is an unexpected technical error. Please reload the page.<br/> If the problem persist, please get in touch with our customer care team</span>');
				$('.lp-activation-modal-static .loading').hide();
			};
			$.post( "/loyaltyAcceptTerms.php", $tncControl.find('form').serialize(),function(data,textStatus,jqXHR){
				if (textStatus !== "success") {
					showTncError();
					return;
				}
				if(typeof(data.status) != 'undefined' && data.status === 'ok'){
					location.reload();
				}else{
					showTncError();
					return;
				}
			});
		});
		_this.$tabContent.on('click','.start-now-button',function(e){
			var termsNConditionModal = $('#mkloyalty-terms-modal');
			if(termsNConditionModal.length > 0){
				e.preventDefault();
				_this.tncBox = Myntra.LightBox('#mkloyalty-terms-modal');
				_this.tncBox.show();
				$('#mkloyalty-terms-modal .terms-n-conditions').jScrollPane();
				termsNConditionModal.on('change','.terms-n-condition-control input[type="checkbox"]',function(){
					var $checkBox = $(this);
					if ($checkBox.is(':checked')) {
		        		$('.terms-n-condition-control .agree-btn').removeClass('disabled-btn');
				    } else {
				        $('.terms-n-condition-control .agree-btn').addClass('disabled-btn');
				    }
				});
				termsNConditionModal.find('.agree-btn').on('click',function(e){
					var $button = $(this),
						$tncControl = $button.closest('.terms-n-condition-control');
					e.preventDefault();
					if($button.hasClass('disabled-btn')) return;
					// create a ajax call to accept terms and conditions. 
					// We need to reload the whole page in this case as header values needs to be refreshed as well.
					Myntra.LightBox('#mkloyalty-terms-modal').showLoading();
					// $('.terms-n-condition-control form').submit();
					var showTncError = function(){
						$tncControl.html('<span class="error">There is an unexpected technical error. Please reload the page.<br/> If the problem persist, please get in touch with our customer care team</span>');
					};
					$.post( "/loyaltyAcceptTerms.php", $tncControl.find('form').serialize(),function(data,textStatus,jqXHR){
						if (textStatus !== "success") {
							showTncError();
							return;
						}
						if(typeof(data.status) != 'undefined' && data.status === 'ok'){
							location.reload();
						}else{
							showTncError();
							return;
						}
					});
				});

			}
		});
	}
};


})();// end of "use strict" wrapper



$(document).ready(function() {

	if(Myntra.Data.loyaltyEnabled && location.search.indexOf('view=myprivilege') != -1){
		// The layout will be different and flow will change in this scenerio.
		// We will have subtabs for Coupons, Cashback and loyalty // Show ajax loader by default
		Myntra.mymyntra.MyntCredits.init();
	}else{
		var carttooltipcoupon = new Myntra.Tooltip(null, '#coupon-exclusion-tt', {side: 'below', useTitle: true});
			if($(".mk-myntcredits-page").length){
				$(".mk-mynt-promo-codes h6").click(function(){
					$(this).parent().find(".mk-coupons-content").slideToggle();
				});
				$(".mk-mynt-credits h6").click(function(){
					$(this).parent().find(".mk-cashback-content").slideToggle();
				});
			}
		 $('.share-coupon button').click(function(el){
			var datElm=$('#coupon-'+$(this).data('coupon-code')); 
			Myntra.CouponTransfer.show({
				elm:el,
				coupon:$(this).data('coupon-code'),
				desc:datElm.find('td:eq(1)').html(),
				exp:datElm.find('td:eq(2)').html()
			}); 
		 });
	}

});

