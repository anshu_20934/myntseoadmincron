
(function() { "use strict";

Myntra.VtrQueryBuilder = Myntra.Class.extend({
    init: function(filters, params) {
        //console.log('[VtrQueryBuilder.init] filters:', filters, 'params:', params);
        this.backups = {};
        this.filters = $.isArray(filters) ? {} : filters;
        this.params = params;
    },
    buildUrl: function() {
        var params = $.extend({}, this.params),
        otherParams = Myntra.VtrManager.msg('products', 'get-query-params'),
        parts = [], name, url;
        $.extend(params, otherParams);
        for (name in this.filters) {
            parts.push(name + ':' + this.filters[name].join('*'));
        }
        params.filters = parts.join('|');
        parts = [];
        for (name in params) {
            parts.push(name + '=' + encodeURIComponent(params[name]));
        }
        var url = parts.join('&');
        //console.log('[QB]', decodeURIComponent(url));
        return url;
    },
    changeSource: function(source) {
        this.backups[this.params.source] = $.extend({}, this.params);
        if (source in this.backups) {
            this.params = $.extend({}, this.backups[source]);
        }
        else {
            this.params.source = source;
            this.params.page = 0;
        }
    },
    hasAnyFilter: function() {
        for (var name in this.filters) {
            return true;
        }
        return false;
    }
});

Myntra.VtrProductListing = Myntra.Class.extend({
    init: function(el, qb, data, onAjaxDone) {
        //console.log('[VtrProductListing.init] data:', data);
        this.pane = $(el).jScrollPane();
        this.qb = qb;
        this.source = this.pane.data('source');
        this.onAjaxDone = onAjaxDone;
        this.loadMoreBtn = this.pane.find('.load-more button');
        this.loadingSpinner = this.pane.find('.load-more .spinner');
        this.data = data;
        this.page = 0;
        this.hasMorePages = this.data.totalPages > 1;
        this.hasMorePages ? this.enableInfiniteScroll() : this.loadMoreBtn.hide();
        this.pane.on('click', 'li', this.onItemClick.bind(this));
        this.loadMoreBtn.on('click', this.ajax.bind(this));
    },
    enableInfiniteScroll: function() {
        this.pane.on('jsp-scroll-y', this.onScroll.bind(this));
    },
    disableInfiniteScroll: function() {
        this.pane.off('jsp-scroll-y');
    },
    /*
    onItemMouseEnter: function(e) {
        $(e.currentTarget).find('.load-look').show();
    },
    onItemMouseLeave: function(e) {
        //$(e.currentTarget).find('.load-look').hide();
    },
    */
    onItemClick: function(e) {
        var el = $(e.currentTarget),
        target = $(e.target),
        styleId = el.data('styleid'),
        uuid = el.data('uuid'),
        shim = el.find('.shim');

        if (target.hasClass('shim')) {
            // do nothing
            return;
        }
        else if (shim.length) {
            e.preventDefault();
            shim.show();
        }
        else if (styleId) {
            e.preventDefault();
            Myntra.VtrManager.msg('products', 'style-selected', styleId);
        }

        // blur the anchor tag, to avoid the dotted focus rectangle
        //target = target.prop('tagName') !== 'A' ? target.closest('a') : target;
        //target.length && target.blur();
    },
    onScroll: function(e, destTop, isAtTop, isAtBottom) {
        if (isAtBottom) {
            myntrack('vtr', 'infinitescroll-trigger', 'data-source-' + this.source);
            this.disableInfiniteScroll();
            this.loadMoreBtn.hide();
            this.ajax();
        }
    },
    ajax: function() {
        this.page += 1;
        this.qb.params.page = this.page; // Note: QB params.page is shared across listings
        $.ajax({
            url: '/vtr/products.php?' + this.qb.buildUrl() + '&_action=nextpage',
            context: this,
            dataType: "json",
            beforeSend: function() {
                this.loadingSpinner.show();
            },
            success: function(res, status, xhr) {
                if (!res) {
                    throw new Error('[VtrProductListing.ajax] No response from the Server!');
                }
                if (res.status === 'SUCCESS') {
                    myntrack('vtr', 'infinitescroll-success', 'data-source-' + this.source);

                    var jsp = this.pane.data('jsp');
                    jsp.getContentPane().find('ol').append(res.markupListing);
                    jsp.reinitialise();

                    if (typeof this.onAjaxDone === 'function') {
                        this.onAjaxDone(this.source, res);
                    }

                    this.hasMorePages = (this.page < res.totalPages - 1);
                    if (this.hasMorePages) {
                        this.enableInfiniteScroll();
                        this.loadMoreBtn.show();
                    }
                }
            },
            error: Myntra.Utils.ajaxError('VtrProductListing'),
            complete: function(xhr, status) {
                this.loadingSpinner.hide();
            }
        });
    },
    start: function(markup, totalPages) {
        this.page = 0;
        this.disableInfiniteScroll();
        this.loadMoreBtn.hide();

        var jsp = this.pane.data('jsp');
        jsp.getContentPane().find('ol').html(markup);
        jsp.reinitialise();
        jsp.scrollToY(0);

        this.hasMorePages = totalPages > 1;
        if (this.hasMorePages) {
            this.enableInfiniteScroll();
            this.loadMoreBtn.show();
        }
    },
    clone: function(source) {
        var jsp = this.pane.data('jsp');
        var el = $('<div class="listing ' + source + '"><ol></ol></div>').attr('data-source', source).insertAfter(this.pane);
        jsp.getContentPane().find('.load-more').clone().appendTo(el);
        var data = $.extend({}, this.data);
        data.totalPages = 1;
        var obj = new Myntra.VtrProductListing(el, this.qb, data);
        return obj;
    },
    show: function() {
        this.pane.show();
    },
    hide: function() {
        this.pane.hide();
    },
    disableItems: function(styleIds) {
        //console.log('[VtrProductListing.disableItems]', styleIds, 'source:', this.source);
        styleIds = $.isArray(styleIds) ? styleIds : [styleIds];
        var jsp = this.pane.data('jsp'), el, shim, i, n, styleId;

        for (i = 0, n = styleIds.length; i < n; i += 1) {
            styleId = styleIds[i];
            el = jsp.getContentPane().find('li[data-styleid="' + styleId + '"]');
            shim = el.find('.shim');
            if (!el.length || shim.length) {
                continue;
            }
            el.addClass('disabled');
            shim = $('<div class="shim in-use">This is already a part of your look</div>').appendTo(el);
        }
    },
    enableItems: function(styleIds) {
        //console.log('[Vtr.ProductListing.enableItems]', styleIds, 'source:', this.source);
        styleIds = $.isArray(styleIds) ? styleIds : [styleIds];
        var jsp = this.pane.data('jsp'), el, shim, i, n, styleId;

        for (i = 0, n = styleIds.length; i < n; i += 1) {
            styleId = styleIds[i];
            el = jsp.getContentPane().find('li[data-styleid="' + styleId + '"]');
            shim = el.find('.shim');
            if (!el.length) {
                continue;
            }
            el.removeClass('disabled');
            shim.length && shim.hasClass('in-use') && shim.remove();
        }
    }
});

Myntra.VtrProducts = Myntra.Class.extend({
    init: function(id, filters, params, data) {
        //console.log('[VtrProducts.init]', 'filters:', filters, 'params:', params, 'data:', data);
        this.root = $(id);
        this.qb = new Myntra.VtrQueryBuilder(filters, params);
        this.loadingSpinner = this.root.find('.loading-spinner');
        
        this.form = this.root.find('.lpane .search-form');
        this.searchBox = this.form.find('input[name="query"]');
        this.searchBtn = this.form.find('button');
        this.searchClose = this.form.find('.close').on('click', this.doDefaultSearch.bind(this));
        this.form.on('submit', this.doSearch.bind(this));

        this.listingItemsCount = this.root.find('.listing-info > .count');
        this.onListingAjaxDone = function(source, data) {
            if (typeof data !== 'undefined') {
                this.listingItemsCount.data(source, +data.totalProducts);
            }

            var count = this.listingItemsCount.data(source);
            //console.log('listing count:', count, 'source:', source);
            if (!count) {
                this.listingItemsCount.html('');
                return;
            }

            if (source === 'search') {
                this.listingItemsCount.html('<strong>' + count + '</strong> Products');
            }
            else if (source === 'recent') {
                this.listingItemsCount.html('<strong>' + count + ' Recently Viewed</strong> Products');
            }
            else if (source === 'wish') {
                this.listingItemsCount.html('<strong>' + count + ' Wishlist</strong> Products');
            }
            else if (source === 'look') {
                this.listingItemsCount.html('<strong>' + count + ' </strong> Saved Looks');
            }
        }.bind(this);

        this.listings = {};
        this.root.find('.listing').each(function(i, el) {
            var source = $(el).data('source');
            this.listings[source] = new Myntra.VtrProductListing(el, this.qb, data, this.onListingAjaxDone);
            this.onListingAjaxDone(source, data);
        }.bind(this));
        this.listing = this.listings[this.qb.params.source];
        
        this.sortWrap = this.root.find('.sorting');
        this.initSorting();

        this.filtersWrap = this.root.find('.filters');
        this.resetFilters = this.root.find('.reset-filters').on('click', this.onResetFiltersClick.bind(this));
        this.initFilters();
        
        this.listingSources = this.root.find('.source');
        this.listingSources.on('click', this.onListingSourceClick.bind(this));
        this.loadSourceLook = Myntra.Utils.afterLogin(function() { this.loadSource('look'); }.bind(this));
        this.loadSourceWish = Myntra.Utils.afterLogin(function() { this.loadSource('wish'); }.bind(this));
    },
    initSorting: function() {
        this.sortMenu = $('#vtr-search-sort-menu').appendTo('body');
        this.sortMenu.on('click', 'li', this.onSortMenuClick.bind(this));
        this.sortBy = this.root.find('.sort-by').on('click', this.onSortByClick.bind(this));
        this.sortSelected = this.root.find('.sort-selected');
        this.sortMenuHide = function(e) {
            var target = $(e.target);
            if (target.is(this.sortBy)) {
                return;
            }
            $(document).off('click', this.sortMenuHide);
            this.sortMenu.hide();
        }.bind(this);
    },
    enableItems: function(styleIds) {
        for (var source in this.listings) {
            if (source === 'look') { continue; }
            this.listings[source].enableItems(styleIds);
        }
    },
    disableItems: function(styleIds) {
        this.listing.disableItems(styleIds);
    },
    onSortByClick: function(e) {
        e.preventDefault();
        var el = $(e.currentTarget),
        pt = el.offset(), x, y;

        $(document).on('click', this.sortMenuHide);
        this.sortMenu.show();
        x = pt.left; // + el.outerWidth() - 13;
        y = pt.top + el.outerHeight() + 3;
        this.sortMenu.css({left:x, top:y});
    },
    onSortMenuClick: function(e) {
        e.preventDefault();
        var target = $(e.target),
            ctarget = $(e.currentTarget),
            value = ctarget.data('value'),
            text = ctarget.text()
        this.qb.params.sort = value;
        this.qb.params.page = 0;
        myntrack('vtr', 'sortby', text);
        this.ajax('sort', text);
    },
    initFilters: function() {
        this.filters = this.root.find('.filter');
        if (!this.filters.length) { return; }
        this.filters
            .removeClass('expanded').addClass('collapsed')
            .find('.hd')
            .on('click', this.onFilterHdClick.bind(this))
            .end()
            .find('.bd')
            .on('click', 'li', this.onFilterBdClick.bind(this))
            .find('> .items').jScrollPane()
            .on('jsp-scroll-y', this.onFilterScroll.bind(this))
            .end()
            .find('.search-box').each(function(i, el) { this.initFilterSearch(el); }.bind(this))
        ;
        this.priceFilter = this.filters.filter('.filter-price');
        this.initPriceSlider();
        this.filters.find('.bd').hide();
        this.expandedFilter = this.filters.first();
        this.expandedFilter.removeClass('collapsed').addClass('expanded').find('.bd').show();
        this.qb.hasAnyFilter() ? this.resetFilters.show() : this.resetFilters.hide();
    },
    destroyFilters: function() {
        if (!this.filters.length) { return; }
        var jsp = this.filters.find('.bd > .items').off('jsp-scroll-y').data('jsp');
        jsp && jsp.destroy();
        this.filters
            .find('.hd').off('click')
            .end()
            .find('.bd').off('click')
            .find('.search-box > .txt').off('keydown')
            .end()
            .find('.search-box > .close').on('click')
        ;
        this.priceSlider.slider('destroy');
    },
    onFilterScroll: function(e, scrollPositionY, isAtBottom, isAtTop) {
        var ctarget = $(e.currentTarget),
        name = ctarget.closest('.filter').data('name');
        //console.log('filter scroll', name, scrollPositionY);
        this.filterProducts.restart();
    },
    initFilterSearch: function(el) {
        var box = $(el),
            txt = box.find('> .txt'),
            close = box.find('> .close'),
            items = box.next('.items'),
            jsp = items.data('jsp'),
            kids = jsp.getContentPane().children();
        txt.on('keydown', Myntra.Utils.throttle(function(e) {
            var text = txt.val();
            if (!text.length || e.which == 27) {
                close.click();
            }
            else {
                close.show();
                kids.each(function(i, li) {
                    var item = $(li),
                        // .attr is used bcoz, .data() will return Number/String based on the value
                        value = item.attr('data-value').toLowerCase();
                    (value.indexOf(text) !== -1) ? item.removeClass('hide') : item.addClass('hide');
                });
            }
            jsp.reinitialise();
        }, 300));
        close.on('click', function(e) {
            close.hide();
            txt.val('');
            kids.removeClass('hide');
            jsp.reinitialise();
        });
    },
    initPriceSlider: function() {
        this.priceSlider = this.priceFilter.find('.slider');
        this.priceSliderValue = this.priceFilter.find('.slider-value');
        var rangeMin = +this.priceSlider.data('min'),
            rangeMax = +this.priceSlider.data('max');
        //console.log('range', rangeMin, rangeMax);
        this.priceSliderValue.html(this.formatPriceRange(rangeMin, rangeMax)); 
        this.priceSlider.slider('destroy');
        this.priceSlider.slider({
            range: true,
            min: rangeMin,
            max: rangeMax,
            values: [rangeMin, rangeMax],
            slide: function(e, ui) {
                this.priceSliderValue.html(this.formatPriceRange(ui.values[0], ui.values[1]));
            }.bind(this),
            stop: function(e, ui) {
                this.qb.filters.price = [ui.values[0], ui.values[1]];
                this.qb.params.page = 0;
                this.priceFilter.find('li').removeClass('ckecked');
                //console.log('QB', this.qb.toString());
                this.filterProducts('price');
            }.bind(this)
        });
    },
    formatPriceRange: function(min, max) {
        return 'Rs. ' + Myntra.Utils.formatNumber(min) + ' - Rs. ' + Myntra.Utils.formatNumber(max);
    },
    onFilterHdClick: function(e) {
        var el = $(e.currentTarget).parent();
        if (el.hasClass('collapsed')) {
            el.removeClass('collapsed').addClass('expanded').find('.bd').show('fast');
            if (this.expandedFilter) {
                this.expandedFilter.removeClass('expanded').addClass('collapsed').find('.bd').hide('fast');
            }
            this.expandedFilter = el;
        }
        else {
            el.removeClass('expanded').addClass('collapsed').find('.bd').hide('fast');
            this.expandedFilter = null;
        }
    },
    onFilterBdClick: function(e) {
        e.preventDefault();
        var target = $(e.target),
            ctarget = $(e.currentTarget),
            name = ctarget.closest('.filter').data('name'),
            value = ctarget.data('value');
        if (ctarget.hasClass('disabled')) {
            return;
        }
        //console.log('name:', name, 'value:', value);
        var filter = (name in this.qb.filters) ? this.qb.filters[name] : [];
        if (name === 'price') {
            var values = value.split('*');
            this.qb.filters.price = values;
            values[0] = +values[0];
            values[1] = +values[1];
            this.priceFilter.find('li').removeClass('checked');
            this.priceSlider.slider('values', 0, values[0]);
            this.priceSlider.slider('values', 1, values[1]);
            this.priceSliderValue.html(this.formatPriceRange(values[0], values[1])); 
            ctarget.addClass('checked');
        }
        else {
            var idx = filter.indexOf(value);
            if (ctarget.hasClass('checked')) {
                if (idx !== -1) {
                    filter.splice(idx, 1);
                }
                ctarget.removeClass('checked');
                myntrack('vtr', 'filter-' + name + '-remove', value);
            }
            else {
                if (idx === -1) {
                    filter.push(value);
                }
                ctarget.addClass('checked');
                myntrack('vtr', 'filter-' + name + '-add', value);
            }
            if (!filter.length) {
                delete this.qb.filters[name];
            }
            else {
                this.qb.filters[name] = filter;
            }
        }
        this.qb.params.page = 0;
        //console.log('QB', this.qb.toString());
        this.filterProducts(name);
    },
    onListingSourceClick: function(e) {
        e.preventDefault();
        var el = $(e.currentTarget).blur(), source = el.data('source');
        source === 'look' ? this.loadSourceLook() : source === 'wish' ? this.loadSourceWish() : this.loadSource(source);
    },
    loadSource: function(source, doAjaxSearch) {
        var exist = (source in this.listings);
        if (source === 'search') {
            this.filtersWrap.slideDown();
            this.form.show();
            this.sortWrap.show();
            this.qb.hasAnyFilter() && this.resetFilters.show();
        }
        else {
            this.filtersWrap.slideUp();
            this.form.hide();
            this.sortWrap.hide();
            this.resetFilters.hide();
        }
        if (!exist) {
            this.listings[source] = this.listing.clone(source);
        }

        if (source !== this.listing.source) {
            this.listingSources.removeClass('selected').filter('[data-source="' + source + '"]').addClass('selected');
            this.qb.changeSource(source);
            this.listing.hide();
            this.listing = this.listings[source];
            this.listing.show();
            this.onListingAjaxDone(source);
        }

        myntrack('vtr', 'change-product-source', source);
        if (!exist || source === 'look') {
            this.ajax('changesource', source);
        }
        else if (source === 'search' && doAjaxSearch) {
            this.ajax('search');
        }
    },
    refreshLooks: function() {
        this.loadSource('look');
    },
    ajaxSearch: function(query) {
        this.qb.params.query = query;
        this.qb.params.page = 0;
        this.qb.filters = {};
        this.ajax('search');
    },
    doSearch: function(e) {
        e.preventDefault();
        var query = this.searchBox.val();
        if (!query) { return; }
        this.ajaxSearch(query);
        this.searchBtn.hide();
        this.searchClose.show();
        myntrack('vtr', 'search', query);
    },
    doDefaultSearch: function() {
        this.searchBox.val('');
        this.ajaxSearch('');
        this.searchBtn.show();
        this.searchClose.hide();
        myntrack('vtr', 'default-search');
    },
    filterProducts: Myntra.Utils.throttle(function(lastAppliedFilterName) {
        this.ajax('filter', lastAppliedFilterName);
        this.resetFilters.show();
    }, 2000),
    onResetFiltersClick: function(e) {
        e.preventDefault();
        this.qb.filters = {};
        this.ajax('resetfilters');
        myntrack('vtr', 'resetfilters', 'click');
    },
    ajax: function() {
        var args = Array.prototype.slice.call(arguments),
        action = args[0];
        $.ajax({
            url: '/vtr/products.php?' + this.qb.buildUrl() + '&_action=' + action,
            context: this,
            dataType: "json",
            beforeSend: function() {
                this.loadingSpinner.show();
            },
            success: function(res, status, xhr) {
                if (!res) {
                    throw new Error('[VtrProducts.ajax] No response from the Server!');
                }
                if (res.status === 'SUCCESS') {
                    var source = action === 'changesource' ? args[1] : 'search';
                    this.listing.start(res.markupListing, res.totalPages);
                    this.onListingAjaxDone(source, res);

                    if (action === 'filter') {
                        this.subsetFilters(args[1], res);
                        this.updatePriceFilterUI(res);
                    }
                    else if (action === 'resetfilters') {
                        this.clearFilters();
                        this.updatePriceFilterUI(res);
                    }
                    else if (action === 'sort') {
                        this.sortSelected.text(args[1]);
                    }
                    else if (action === 'search') {
                        this.destroyFilters();
                        this.root.find('.filters').html(res.markupFilters);
                        this.initFilters();
                    }
                    else if (action === 'changesource' && args[1] === 'search') {
                        this.filtersWrap.html(res.markupFilters);
                        this.initFilters();
                        this.sortWrap.html(res.markupSorting);
                        this.initSorting();
                    }
                }
            },
            error: Myntra.Utils.ajaxError('VtrProducts'),
            complete: function(xhr, status) {
                this.loadingSpinner.hide();
            }
        });
    },
    updatePriceFilterUI: function(res) {
        var dataValues = res.dataFilters.price.values;
        this.priceFilter.find('ul').html(res.markupPriceFilter);
        //this.priceSlider.data('min', dataValues.rangeMin).data('max', dataValues.rangeMax);
        //this.priceSlider.slider('option', 'min', dataValues.rangeMin);
        //this.priceSlider.slider('option', 'max', dataValues.rangeMax);
        //this.initPriceSlider();
    },
    subsetFilters: function(lastAppliedFilterName, res) {
        var name, dataValues, values, val, nodeFilter;
        for (name in res.dataFilters) {
            dataValues = res.dataFilters[name].values;
            if (name === 'price' || name === lastAppliedFilterName) {
                continue;
            }
            // dataValues is a map: val => cnt
            values = [];
            for (val in dataValues) {
                values.push(val);
            }
            nodeFilter = this.filters.filter('.filter-' + name);
            nodeFilter.find('.bd li').each(function(i, el) {
                el = $(el);
                $.inArray(el.data('value'), values) === -1 ? el.addClass('disabled') : el.removeClass('disabled');
            });
        }
    },
    clearFilters: function() {
        this.filters.find('li').removeClass('disabled').removeClass('checked');
        this.resetFilters.hide();
    }
});

}()); // end of "use strict" wrapper

