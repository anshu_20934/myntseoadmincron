//FOR EXPRESS BUY SETTINGS
Myntra.ExpressSettings =  { 
        init: function(){
            this.root = $('#express-block');
            this.enable = $('#express-buy-enable');
            this.url = Myntra.Data.paySerHost;
            this.defaultOption = this.root.find('input[name="default-option"],input[name="express-buy-enable"]').on('change', $.proxy(this.handleSelect, this));
            this.cards = this.root.find("#list-cards"); 
            this.removeCard ='';
            this.setDefault= '';
            this.tplItem = [
                        ' <li class="card {expired}">',
                        '   <div class="mk-f-left details-cont">',
                        '       <div class="exp-msg-expired">THIS CARD HAS EXPIRED</div>',
                        '       <div class="exp-msg-invalid">THIS CARD IS INVALID</div>',
                        '       <span class="card-type {cardType}"></span>',
                        '       <div class="card-details">',
                        '           <span class="card-no">{maskedCardNumber} </span>',
                        '           <span class="card-holder-name">{cardHolderName}</span>',
                        '       </div>',
                        '   </div>',
                        '   <div class="mk-f-right card-options">',
                        '       <div class="default-card">{defaultInstrument}</div>',
                        '       <button class="link-btn remove" data-key="{instrumentId}" data-token="{csrfToken}">remove</button>',
                        '   </div>',    
                        '</li>'
                        ].join('');
            //call list cards
            this.list();
        },
        
        list : function(){
            var that=this,
                pref = {},
                cards = {};
              
            //get user preference here
            if(Myntra.Data.expressbuyEnabled){
                pref.type="list-preferences";
                pref.dataType="json";
                pref.method= "GET";
                pref.url='/payment/userpreference/list/all';
                this.ajax(pref);
            }
                
    
            //also get save cards here
            cards.type="list-cards";
            cards.dataType="json";
            cards.method= "GET";
            cards.url='/paymentInstruments/list';
            this.ajax(cards);
        },

        handleSelect : function(e){
            var target = $(e.target),
                expressSettings= {},
                el = $(e.currentTarget);
                expressSettings.dataType="json";
                expressSettings.contentType ="application/json; charset=utf-8";
                expressSettings.method= "POST";
                expressSettings.url='/userpreference/update';
                if(target.attr('id')=='express-buy-enable'){
                    expressSettings.type="update-preferences-enable";
                    expressSettings.data= JSON.stringify({"preferenceType":"oneclick_enable","preferenceValue":(target.is(':checked')?"true":"false")});
                }
                else {
                    //set the default payemnt method
                    expressSettings.type="update-preferences-default";
                    expressSettings.data= JSON.stringify({"preferenceType":"oneclick_default_method","preferenceValue":(target.val())});
                }
                this.ajax(expressSettings);
        },
        handleClick : function(e){
            var target = $(e.target),
                cardAction= {},
                el = $(e.currentTarget);
                cardAction.dataType="json";
                cardAction.contentType ="application/x-www-form-urlencoded";
                cardAction.method= "POST";
                cardAction.target=target;
                
                if(target.hasClass('remove')){
                    cardAction.url='/paymentInstruments/delete';
                    cardAction.data ='id='+encodeURIComponent(target.data('key'))+'&csrf_token='+encodeURIComponent(target.data('token'));
                    cardAction.type="delete-card";
                }else{
                    var $removeBtn = target.closest('.card-options').find('.remove');
                    cardAction.url='/paymentInstruments/update';
                    cardAction.data ='id='+encodeURIComponent($removeBtn.data('key'))+'&default=true'+'&csrf_token='+encodeURIComponent($removeBtn.data('token'));
                    cardAction.type="update-card";
                }
                this.ajax(cardAction);
        },
        ajax : function(params){
            var that=this;
            //get user preference here
            $.ajax({
                type: params.method,
                url: this.url+params.url,
                data:(params.data)?params.data:'',
                dataType:params.dataType,
                cache:false,
                contentType: (params.contentType)?params.contentType:'application/x-www-form-urlencoded',
                beforeSend : function(){
                    if(params.type !== 'list-preferences'){
                        //show loading bar
                        if(!$('.body-overlay').length){
                            $('<div class="body-overlay"><img style="margin-top:100px" src="https://d6kkp3rk1o2kk.cloudfront.net/skin2/images/loader_150x200.gif" /></div>').css({'position':'fixed','left':'0','top':'0','height':'100%','width':'100%','overflow':'hidden','background':'#fff','opacity':'.6','text-align':'center'}).appendTo('body');   
                        }else{
                            $('.body-overlay').show();
                        }
                    }
                },
                success: function(data,status, xhr){
                    that.setData(data,params);    
                },
                error : function(xhr, status, msg){
                    $('.body-overlay').hide();
                },
                complete: function(xhr, status) {
                    //hide the loader
                    $('.body-overlay').hide();
                    
                }
            });
        },

        setData:function(data,params){ 
            var that=this;
            switch(params.type){
                case "list-preferences":
                    var preference=data;
                    //set preference type and enable/disable option
                    $(preference).each(function(idx, obj){
                        if(obj.preferenceType === 'oneclick_enable'){
                            that.enable.prop('checked',(obj.preferenceValue==='false'?0:1));
                            //set value of cookie to access it across
                        }
                        else if(obj.preferenceType === 'oneclick_default_method'){
                            var prefID='';
                            if(obj.preferenceValue=='creditcard')
                            {
                                prefID='sel_credit_card';
                            }else if(obj.preferenceValue=='creditcard'){
                                prefID='sel_debit_card';
                            }
                            else{
                                prefID='sel_cod';
                            }
                            that.root.find('#'+prefID).prop('checked',1);
                        }
                    });
                    $('.body-overlay').hide();
                break;
                case "update-preferences-default":
                        //do nothing , show a message if required
                        //update cookie 
                        $('.body-overlay').hide();
                break;
                case "update-preferences-enable":
                        Myntra.Utils.Cookie.set('__expressCookie__','set');
                        $('.body-overlay').hide();
                break;
                case "list-cards":
                var cards=data.cardList,markup,str,item,csrfToken=data.csrf_token;
                    this.cards.html('');
                    // Cached the csrf token to Myntra global variable.
                    Myntra.Data = Myntra.Data || {};
                    Myntra.Data.paymentCSRFToken = csrfToken;
                    $(cards).each(function(idx, obj){
                        obj.csrfToken = csrfToken;
                        markup = that.tplItem.replace(/\{(.*?)\}/g, function(str, key) {
                            if (key in obj) {
                                if(key==='defaultInstrument'){
                                    str=(obj[key])?'<div class="selected-block"><span class="selected-icon"></span>default</div><div class="select-default-block mk-hide"><button class="link-btn set-default">set as default</button></div>':'<div class="selected-block mk-hide"><span class="selected-icon"></span>default</div><div class="select-default-block"><button class="link-btn set-default">set as default</button></div>';
                                }else if(key==='cardType'){
                                    str=(obj[key]).toLowerCase();
                                }
                                else if(key==='expired'){
                                    str=(obj[key] )?'expired':'not-expired';
                                    if(!obj[key]){
                                        str+=(obj['inValid'] )?' invalid':' not-invalid';
                                    }else{
                                        str+=' not-invalid';
                                    }
                                }
                                else {
                                    str=obj[key];
                                }
                            }
                            return str;
                        });
                        item = $(markup).appendTo(that.cards);
                    });
                    this.cards.find('li:odd').css({'background':'#fff'});
                    this.removeCard = $('#list-cards .remove').on('click', $.proxy(this.handleClick, this));
                    this.setDefault = $('#list-cards .set-default').on('click', $.proxy(this.handleClick, this));
                    $('.body-overlay').hide();
                break;
                case "update-card":
                        /*cards= {};
                        cards.type="list-cards";
                        cards.dataType="json";
                        cards.method= "GET";
                        cards.url='/admin/paymentInstruments/list';
                        this.ajax(cards);
                        $('.body-overlay').hide();
                        */
                        this.cards.find('.selected-block').hide().siblings().show();
                        this.cards.find('.selected-block').hide();
                        params.target.closest('li').find('.selected-block').show().siblings().hide();
                        //also get save cards here
                break;
                case "delete-card":
                    params.target.closest('li').remove();
                    this.cards.find('li:odd').css({'background':'#fff'});
                    this.cards.find('li:even').css({'background':'#f2f2f2'})
                break;
                case "add-card":
                    
                break;

            }   

        }
    
};

$(document).ready(function(){
    if (!$('.my-profile-page').length) {
        return;
    }
    if(Myntra.Data.promptLogin){
     $(document).trigger('myntra.login.show', {
            invoke: "checkout",
            action: "signin",
            secureSessionExpired: true,
            isModal: true,
            onLoginDone: function() {
                //var url = document.URL.replace(/promptlogin=?\d?/, '');
                //console.log('reload URL: ' + url);
                //window.location = url;
                //Myntra.updateSecureHeaderAfterLogin();
                //!this.loaded && this.load.apply(this, args);
                location.reload();
            }.bind(this),
            onLoginClose : function(){
              window.location = http_loc; 
             
            }.bind(this),
            afterForgotPasswordOk : function(){
                window.location = http_loc; 
            }.bind(this)

        });
     $('.express-block').hide();
     return false;   
    }

    //Init Express buy setting
    Myntra.ExpressSettings.init();
    $('.express-cards .add-card').on('click',function(){
        Myntra.AddCard.show();
    });
  
});

//Handle code to open popoup for adding a new card
//Open a LightBox
//LighBox for Add card

Myntra.AddCard =  { 
    init: function(){
       var tplBox = [
'<div id="lb-add-card" class="lightbox lb-add-card">',
'   <div class="mod">',
'        <div class="hd">',
'               <h2><span class="lbl">ADD A CARD</span></h2>',
'        </div>',
'       <div class="bd">',
'           <div class="success-msg mk-hide"><span class="icon"></span><span class="msg"></span></div>',
'<div class="tabs"><ul>',
'       <li class="selected" id="pay_by_cc"><span class="opt"></span>Credit Card</li>',
'       <li class="" id="pay_by_dc"><span class="opt"></span>Debit Card</li>',
'</ul></div>',
'<div class="tab-content mk-cf">',
'<form style="display: block;" target="MyntraPayment" id="credit_card" method="post" action="">',
' <input type="hidden" value="9" name="address">',
' <input type="hidden" value="9" name="csrf_token">',
'  <input type="hidden" name="pm" value="creditcards">',
'  <input type="hidden" value="default" name="clientCode">',
'  <div class="cc-form">',
'     <div class="accepted-cards">',
'        <span class="cardType visa" style="opacity: 1;">Visa</span>',
'        <span class="cardType master" style="opacity: 1;">Mastercard</span>',
'     </div>',
'     <div class="row">',
'   <label>Card number <em>Without Spaces</em> *</label>',
'   <input type="text" autocomplete="off" name="card_number" maxlength="19" class="long">',
'   <div class="err-msg"></div>',    
'     </div>',
'     <div class="row">',
'   <label class="required">Name on credit card *</label>',
'   <input type="text" class="long" name="bill_name" value="" title="Name on credit card">',
'   <div class="err-msg"></div>',
'     </div>',
'     <div class="row cc-expiry mk-cf">',
'   <div class="col mk-f-left">',
'       <label>Expiry Month *</label>',
'       <select name="card_month">',
'       <option value="">Month</option>',
'       <option value="01"> 1 </option>',
'       <option value="02"> 2 </option>',
'       <option value="03"> 3 </option>',
'       <option value="04"> 4 </option>',
'       <option value="05"> 5 </option>',
'       <option value="06"> 6 </option>',
'       <option value="07"> 7 </option>',
'       <option value="08"> 8 </option>',
'       <option value="09"> 9 </option>',
'       <option value="10"> 10 </option>',
'       <option value="11"> 11 </option>',
'       <option value="12"> 12 </option>',
'       </select>',
'   </div>',
'   <div class="col mk-f-right">',
'       <label>Expiry Year *</label>',
'       <select name="card_year">',
'       <option value="">Year</option>',
'       <option value="13">2013</option>',
'       <option value="14">2014</option>',
'       <option value="15">2015</option>',
'       <option value="16">2016</option>',
'       <option value="17">2017</option>',
'       <option value="18">2018</option>',
'       <option value="19">2019</option>',
'       <option value="20">2020</option>',
'       <option value="21">2021</option>',
'       <option value="22">2022</option>',
'       <option value="23">2023</option>',
'       <option value="24">2024</option>',
'       <option value="25">2025</option>',
'       <option value="26">2026</option>',
'       <option value="27">2027</option>',
'       <option value="28">2028</option>',
'       <option value="29">2029</option>',
'       <option value="30">2030</option>',
'       <option value="31">2031</option>',
'       <option value="32">2032</option>',
'       <option value="33">2033</option>',
'       <option value="34">2034</option>',
'       <option value="35">2035</option>',
'       <option value="36">2036</option>',
'       <option value="37">2037</option>',
'       <option value="38">2038</option>',
'       <option value="39">2039</option>',
'       <option value="40">2040</option>',
'       </select>',
'   </div>',
'   <div class="err-msg"></div>',
'  </div>',
'   <div class="msg-fill-all">* Required fields</div>',
'</div>',
'<div class="cc-form cc-bill-address">',
'   <h3>Credit card billing address</h3>',
'   <div class="msg">Your billing address is used to prevent fraudulent use of your card.<br />Enter the address exactly as it appears on your card statement.<br /><input id="copy-billing-add" type="button" class="btn link-btn" value="Select From Address Book" /></div>',

'   <div class="row">',
'       <label>Name *</label>',
'       <input type="text" class="long" value="" id="cc_b_firstname" name="b_firstname">',
'       <div class="err-msg"></div>', 
'   </div>',
'   <div class="row">',
'       <label>Address *</label>',
'       <textarea rows="3" class="long" id="cc_b_address" name="b_address"></textarea>',
'       <div class="err-msg"></div>',
'   </div>',
'   <div class="row mk-cf">',
'       <div class="col mk-f-left">',
'           <label>City *</label>',
'           <input type="text" class="short" value="" id="cc_b_city" name="b_city">',
'           <div class="err-msg"></div>',
'       </div>',
'       <div class="col mk-f-right">',
'           <label>State *</label>',
'           <input type="text" class="short" value="" id="cc_b_state" name="b_state">',
'           <div class="err-msg"></div>',
'       </div>',
'   </div>',
'   <div class="row">',
'       <label>Pincode *</label>',
'       <input type="text" maxlength="6" class="short" value="" id="cc_b_zipcode" name="b_zipcode">',
'       <div class="err-msg"></div>',
'   </div>',
'   <div class="row cc-bill-country">',
'       <label>Country *</label>',
'       <select name="b_country" id="cc_b_country">',                               
'          <option selected="true" value="IN">India</option>',                              
'       </select>',
'       <div class="err-msg"></div>',
'   </div>',
'   <div class="msg-fill-all">* Required fields</div>',
'</div>',
'</form>',                          

'<form style="display:none" target="MyntraPayment" id="debit_card" method="post" action="">',
'   <input type="hidden" value="9" name="address">',
'   <input type="hidden" value="token" name="csrf_token">',
'   <input type="hidden" name="pm" value="debitcard">',
'   <input type="hidden" value="default" name="clientCode">',
'   <div class="cc-form">',
'      <div class="accepted-cards">',
'         <span class="cardType visa" style="opacity: 1;">Visa</span>',
'         <span class="cardType master" style="opacity: 1;">Mastercard</span>',
'         <span class="cardType maestro" style="opacity: 1;">Maestro</span>',
'      </div>',
'      <div class="row">',
'   <label>Card number <em>Without Spaces</em> *</label>',
'   <input type="text" autocomplete="off" name="card_number" maxlength="19" class="long">',
'   <div class="err-msg"></div>',
'      </div>',
'      <div class="row">',
'   <label class="required">Name on debit card *</label>',
'   <input type="text" class="long" name="bill_name" value="" title="Name on credit card">',
'   <div class="err-msg"></div>',
'      </div>',
'      <div class="row cc-expiry mk-cf">',
'   <div class="col mk-f-left">',
'   <label>Expiry Month <span class="ostar">*</span></label>',
'   <select name="card_month">',
'     <option value="">Month</option>',
'     <option value="01"> 1 </option>',
'     <option value="02"> 2 </option>',
'     <option value="03"> 3 </option>',
'          <option value="04"> 4 </option>',
'     <option value="05"> 5 </option>',
'          <option value="06"> 6 </option>',
'     <option value="07"> 7 </option>',
'     <option value="08"> 8 </option>',
'     <option value="09"> 9 </option>',
'     <option value="10"> 10 </option>',
'     <option value="11"> 11 </option>',
'     <option value="12"> 12 </option>',
'   </select>',
'       </div>',
'      <div class="col mk-f-right">',
'    <label>Expiry Year <span class="ostar">*</span></label>',
'    <select name="card_year">',
'     <option value="">Year</option>',
'     <option value="13">2013</option>',
'         <option value="14">2014</option>',
'     <option value="15">2015</option>',
'     <option value="16">2016</option>',
'     <option value="17">2017</option>',
'     <option value="18">2018</option>',
'     <option value="19">2019</option>',
'     <option value="20">2020</option>',
'     <option value="21">2021</option>',
'     <option value="22">2022</option>',
'     <option value="23">2023</option>',
'     <option value="24">2024</option>',
'     <option value="25">2025</option>',
'     <option value="26">2026</option>',
'     <option value="27">2027</option>',
'     <option value="28">2028</option>',
'     <option value="29">2029</option>',
'     <option value="30">2030</option>',
'     <option value="31">2031</option>',
'     <option value="32">2032</option>',
'     <option value="33">2033</option>',
'     <option value="34">2034</option>',
'     <option value="35">2035</option>',
'     <option value="36">2036</option>',
'     <option value="37">2037</option>',
'     <option value="38">2038</option>',
'     <option value="39">2039</option>',
'     <option value="40">2040</option>',
'   </select>',
'     </div>',
'     <div class="err-msg"></div>',
'   </div>',
'   <div class="msg-fill-all">* Required fields</div>',
'</div>',
'</form>',
'</div>',
'<div class="btn-group"> <button class="small-btn primary-btn btn save-card" >save</button><button class="small-btn btn normal-btn cancel-save" >cancel</button></div>',
'       </div>',
'   </div>',
'</div>',
'<div class="baddress-list-wrap"></div>'
].join('');  
        this.root = $(tplBox).appendTo('body');
        this.cBox = Myntra.LightBox('#lb-add-card');
        this.cBoxMod = this.root.find('.mod');
        this.cBoxHead = this.root.find('.hd');
        this._initialized = true;
        this.tabs = this.root.find('.tabs');
        this.tab = this.root.find('.tabs li').on('click', $.proxy(this.switchtab, this));
        this.tabContent = this.root.find('.tab-content');
        this.creditCardForm = this.root.find('#credit_card');
        this.debitCardForm = this.root.find('#debit_card');
        this.saveBtn = this.root.find('.save-card').on('click', $.proxy(this.validate, this));
        this.cancelBtn = this.root.find('.cancel-save').on('click', $.proxy(this.hide, this));
        this.formIdToValidate = 'credit_card';
        this.cardInput = this.root.find('input[name="card_number"]').on('focus keyup change', $.proxy(this.checkCard, this)).on('focusout' , $.proxy(this.cardvalidate, this));
        this.errMsg = this.root.find('.err-msg');
        this.url = Myntra.Data.paySerHost;
        this.copyAdd=this.root.find('#copy-billing-add').on('click',$.proxy(this.addressBook, this));
        this.successMsg=this.root.find('.success-msg');
        this._addressInit =false;
        this.addressObj='';
        var that=this;
        $(document).bind('myntra.address.list.done', function(e,selectedAddress){
                   //fire one more ajax to this
            that.copyAddress(selectedAddress);
        });

    },
    
    cardvalidate: function(e){
     
        if(e.target==null){
           var target = e;
        }else{
            var target = $(e.target);
        }
        
        cardValue = target.val();
     
         // accept only digits, dashes or spaces
                if (/[^0-9-\s]+/.test(cardValue)){
                	target.siblings('.err-msg').text('Please enter valid card number');
 //                   $(".save-card").attr("disabled","disabled");
                	return false;
                }
                
                 if(cardValue===null||cardValue.length<13){
                    target.siblings('.err-msg').text("Please enter valid card number");
 //                   $(".save-card").attr("disabled","disabled");
                    return false;
                }

                var nCheck = 0, nDigit = 0, bEven = false;
                cardValue = cardValue.replace(/\D/g, "");
             
               
                for (var n = cardValue.length - 1; n >= 0; n--) {
                    var cDigit = cardValue.charAt(n),
                        nDigit = parseInt(cDigit, 10);

                    if (bEven) {
                        if ((nDigit *= 2) > 9) nDigit -= 9;
                    }

                    nCheck += nDigit;
                    bEven = !bEven;
                }

                var re = (nCheck % 10) == 0;
        
                if(nCheck==0)
                	re=false;
        
            if(!re){
                target.siblings('.err-msg').text('Entered card number is invalid');
   //             $(".save-card").attr("disabled","disabled");
                return false;
            }else{
                 target.siblings('.err-msg').text('');
                 return true;
            }
       
    },

    show: function(giftData){
        !this._initialized && this.init();
        this.cBox.show();
        this.cardInput.trigger('focus');
        //this.successMsg.siblings().show();
        this.tabs.css('visibility','visible');
        this.successMsg.hide();
        this.fillCountries(Myntra.Data.countryList);
        this.attachPaymentCSRFToken(Myntra.Data.paymentCSRFToken);
        var that=this;
        this.cBox.beforeHide=function(){
            that.errMsg.text('');
            that.root.find('input[type="text"],select, textarea').val('');
        }
    },

    checkCard :function(e){
        var target = $(e.target),
            cardID = getCardType(target.val()),
            cardType = getCardTypeForFade(target.val());


            //Fading the card Type
            if(cardType==1){
              //visa
                $(".tab-content .master").animate({'opacity': '0.3'});
                $(".tab-content  .maestro").animate({'opacity': '0.3'});
                $('#debit_card .ostar').fadeIn();
                $('#debit_card .field-msg').animate({'opacity': '0'});
            } else if(cardType==2){
                //masterCard
                $(".tab-content .visa").animate({'opacity': '0.3'});
                $(".tab-content .maestro").animate({'opacity': '0.3'});
                $('#debit_card .ostar').fadeIn();
                $('#debit_card .field-msg').animate({'opacity': '0'});
            } else if(cardType==5){
                //maestro
                $(".tab-content .visa").animate({'opacity': '0.3'});
                $(".tab-content .master").animate({'opacity': '0.3'});
                $('#debit_card .ostar').fadeOut();
                $('#debit_card .field-msg').animate({'opacity': '1'});
            } else if(cardType==3 || cardType==4 || cardType=="others"){
                $(".tab-content .cardType").animate({'opacity': '1'});
                $('#debit_card .ostar').fadeOut();
                $('#debit_card .field-msg').animate({'opacity': '1'});
            } else{
                $(".tab-content .cardType").animate({'opacity': '1'});
                $('#debit_card .ostar').fadeOut();
                $('#debit_card .field-msg').animate({'opacity': '1'});
        }
        
        target.siblings('.err-msg').text('');
 //       $(".save-card").removeAttr("disabled");
        
    },

    hide : function(){
        this.errMsg.text('');
        this.cBox && this.cBox.hide();
    },
    
    save : function(dataObj){
        var that=this;
        var formattedData='';
        if(dataObj.type==='cc'){
            formattedData='default=false&csrf_token='+encodeURIComponent(dataObj.csrf_token)+'&cardholder_name='+encodeURIComponent(dataObj.name)+'&card_number='+dataObj.card+'&card_exp_year='+dataObj.year+'&card_exp_month='+dataObj.month+'&billing_name='+dataObj.fName+'&billing_address='+dataObj.address+'&billing_pincode='+dataObj.zipcode+'&billing_city='+dataObj.city+'&billing_state='+dataObj.state+'&billing_country='+dataObj.country;

        }
        else {
            formattedData='default=false&csrf_token='+encodeURIComponent(dataObj.csrf_token)+'&cardholder_name='+encodeURIComponent(dataObj.name)+'&card_number='+dataObj.card+'&card_exp_year='+dataObj.year+'&card_exp_month='+dataObj.month;
        }
        
        // this.cardInput = this.root.find('input[name="card_number"]')

        $.ajax({
                type: 'POST',
                url: this.url+'/paymentInstruments/add',
                data:formattedData,
                dataType:'json',
                contentType:'application/x-www-form-urlencoded',
                beforeSend : function(){
                    that.cBox.showLoading();
                },
                success: function(data,status, xhr){
                    //that.setData(data,params);//    
                    //if success then fire Myntra.Express
                    if(data.status == 'success'){
                        cards= {};
                        cards.type="list-cards";
                        cards.dataType="json";
                        cards.method= "GET";
                        cards.url='/paymentInstruments/list';
                        Myntra.ExpressSettings.ajax(cards);
                    //that.successMsg.siblings().hide();
                        that.tabs.css('visibility','hidden');
                        that.successMsg.show().find('.msg').text('Card Saved');
                        setTimeout(function(){
                            that.cBox.hide();
                        }, 2000);
                    }
                    else {
                        var msg ='';
                        switch(data.error_code){
                            case "CARDPRESENT":
                                msg=data.message;
                            break;
                            case "MAXCARDS":
                                msg=data.message;
                            break;
                            default:
                                msg="Something went wrong. Please try again";
                            break;
                        }
                        $('#'+that.formIdToValidate).find('input[name="card_number"]').siblings('.err-msg').text(msg);
                    }
                },
                error : function(xhr, status, msg){
                    that.cBox.hideLoading();
                },
                complete: function(xhr, status) {
                    that.cBox.hideLoading();
                }
            });
    },
    validate : function(e){  
        this.errMsg.text('');
        var form = $('#'+this.formIdToValidate),
            cardField = form.find('input[name="card_number"]'),
            nameField = form.find('input[name="bill_name"]'),
            monthField =  form.find('select[name="card_month"]'),
            yearField =  form.find('select[name="card_year"]'),
            csrfTokenField = form.find('input[name="csrf_token"]'),
            datObj= {};

            var valid = this.cardvalidate(form.find('input[name="card_number"]'));

            if(!valid){
                return;
            }
            
        if (this.formIdToValidate=='credit_card') {
            var fNameField = form.find('input[name="b_firstname"]'),
                addressField = form.find('textarea[name="b_address"]'),
                cityField = form.find('input[name="b_city"]'),
                stateField = form.find('input[name="b_state"]'),
                zipcodeField = form.find('input[name="b_zipcode"]'),
                countryField =form.find('select[name="b_country"]');
             if( !getCardType(cardField.val()) || !$.trim(nameField.val()) || !$.trim(monthField.val()) || !$.trim(yearField.val()) || !$.trim(fNameField.val()) || !$.trim(addressField.val()) || !$.trim(cityField.val()) || !$.trim(stateField.val()) || !$.trim(zipcodeField.val())){

                    !getCardType(cardField.val()) ? cardField.siblings('.err-msg').text('enter a valid credit card number'):cardField.siblings('.err-msg').text('');
                    !$.trim(nameField.val()) ? nameField.siblings('.err-msg').text('enter the name as it appears on card'):nameField.siblings('.err-msg').text('');
                    !$.trim(monthField.val()) ? monthField.parent().siblings('.err-msg').text('select card expiry monthand year '):monthField.parent().siblings('.err-msg').text('');
                    !$.trim(yearField.val()) ? yearField.parent().siblings('.err-msg').text('select card expiry year'):yearField.parent().siblings('.err-msg').text('');
                    !$.trim(fNameField.val()) ? fNameField.siblings('.err-msg').text('enter a billing name'):fNameField.siblings('.err-msg').text('');
                    !$.trim(addressField.val()) ? addressField.siblings('.err-msg').text('enter a billing address'):addressField.siblings('.err-msg').text('');
                    !$.trim(cityField.val()) ? cityField.siblings('.err-msg').text('enter a billing city'):cityField.siblings('.err-msg').text('');
                    !$.trim(stateField.val()) ? stateField.siblings('.err-msg').text('enter a billing state'):stateField.siblings('.err-msg').text('');
                    !$.trim(zipcodeField.val()) ? zipcodeField.siblings('.err-msg').text('enter a valid PinCode/ZipCode'):zipcodeField.siblings('.err-msg').text('');
             }
            else {
            dataObj = {
                type:'cc',
                name:nameField.val(),
                card:cardField.val(),
                month:monthField.val(),
                year:yearField.val(),
                fName:fNameField.val(),
                address:addressField.val(),
                city:cityField.val(),
                state:stateField.val(),
                zipcode:zipcodeField.val(),
                country:countryField.val(),
                csrf_token : csrfTokenField.val()

            };
            
             this.save(dataObj);
         }


    } else if (this.formIdToValidate=='debit_card') {

      if( !getCardType(cardField.val()) || !$.trim(nameField.val()) || !$.trim(monthField.val()) || !$.trim(yearField.val())){
            !getCardType(cardField.val()) ? cardField.siblings('.err-msg').text(' enter a valid credit card number'):cardField.siblings('.err-msg').text('');
            !$.trim(nameField.val()) ? nameField.siblings('.err-msg').text(' enter the name as it appears on card'):nameField.siblings('.err-msg').text('');
            !$.trim(monthField.val()) ? monthField.parent().siblings('.err-msg').text(' select card expiry month'):monthField.parent().siblings('.err-msg').text('');
            !$.trim(yearField.val()) ? yearField.parent().siblings('.err-msg').text(' select card expiry year'):yearField.parent().siblings('.err-msg').text('');
      }
      else {
         dataObj = {
                type:'dc',
                name:nameField.val(),
                card:cardField.val(),
                month:monthField.val(),
                year:yearField.val(),
                csrf_token : csrfTokenField.val()
            };
            this.save(dataObj);
      }

        } 
    
    },
    switchtab: function(e){
        
        $(".err-msg").text('');
 //       $(".save-card").removeAttr("disabled");

        this.errMsg.text('');
        var target = $(e.target);
        this.tab.removeClass('selected');
        target.addClass('selected');
        if(target.attr('id')==='pay_by_cc'){
            this.creditCardForm.show();
            this.debitCardForm.hide(); 
            this.formIdToValidate = 'credit_card';
        }
        else {
            this.creditCardForm.hide();
            this.debitCardForm.show(); 
            this.formIdToValidate = 'debit_card';
        }

    },
    addressBook : function(e){
      var that=this;
            var cfg = {
                _type:'billing',
                container:'.baddress-list-wrap',
                isPopup:true,
                displayName:'billing',
                hideCreateNewAddress:true
      
            };
            if(!this._addressInit){
               this.addressObj=Myntra.AddressListBox('#lb-address-list-pop',cfg);
                this._addressInit =true; 

            }
            
            this.addressObj.show(cfg);
        //$('#lb-address-list .ft .btn-save').text('continue').addClass('primary-btn');
            
    },

    copyAddress : function(selectedAddr){
       var that=this;
       this.creditCardForm.find('input[name="b_firstname"]').val(selectedAddr.name);
       this.creditCardForm.find('textarea[name="b_address"]').val(selectedAddr.address);
       this.creditCardForm.find('input[name="b_city"]').val(selectedAddr.city);
       this.creditCardForm.find('input[name="b_state"]').val(selectedAddr.state);
       this.creditCardForm.find('input[name="b_zipcode"]').val(selectedAddr.pincode);
     

    },
    fillCountries : function(countriesJSON){
        //iterate through the select button and add to it.
        var  opts = [];
        for(key in countriesJSON){
            opts.push('<option value="' + key + '"'+ ((key=="IN")?"selected":"") +'>' + countriesJSON[key] + '</option>');
        }
        this.creditCardForm.find('select[name="b_country"]').html(opts.join(''));
    },
    attachPaymentCSRFToken : function(csrfToken){
        this.creditCardForm.find('input[name="csrf_token"]').val(csrfToken);
        this.debitCardForm.find('input[name="csrf_token"]').val(csrfToken);
    }
};

