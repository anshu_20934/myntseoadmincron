var zoomLevel="normal";

//zoom code starts here	    
Myntra.PDP.Zoom = (function(){
    	var obj = {};
    	
        var docHeight;
        var docWidth;
        var winWdith;
        var winHeight;
        var iZoomWidth=540;
        var iZoomHeight=720;
        var sZoomWidth=980;
        var sZoomHeight=1306;
        var superZoomMaxHeight = sZoomHeight-($(window).height());
        var scrollPos;
        var noOfImages;
        
    	obj.initZoomClick = function(){
            //This been hidden due to the fix in quick look
            $("#view-prev-szoom").css({"display":"inline"});
            $("#view-next-szoom").css({"display":"inline"});


    		winWidth=$(window).width();
        	winHeight=$(window).height();
        	docHeight=$(document).height();
        	docWidth=$(document).width();
        	noOfImages = $('.mk-product-page .mk-more-views .rs-carousel-item img').length;
        	
        	//Slide up for zoom overlay buy now block
        	$('.zoom-open').live('click', function(){
        		$('.zoom-buy-now-holder').slideUp('fast', function(){
        			$('.zoom-close').removeClass("mk-hide").addClass("mk-show");
            		$('.zoom-open').removeClass("mk-show").addClass("mk-hide");
        		});
        	});
        	
        	//Slide down for zoom overlay buy now block
        	$('.zoom-close').live('click', function(){
        		$('.zoom-buy-now-holder').slideDown('fast', function(){
        			$('.zoom-open').removeClass("mk-hide").addClass("mk-show");
            		$('.zoom-close').removeClass("mk-show").addClass("mk-hide");
        		});
        	});

    		$('.mk-product-page .mk-product-large-image').live('click',function(){
    			//set intermediate zoom size and carousel item to show
    			//$('.izoom-carousel').hide();
    			$('.szoom-carousel').show();
    			scrollPos= $(window).scrollTop();
    	    	Myntra.Utils.scrollToPos(0);
            obj.isOpened = true;
    	    	obj.setSZoomWindow();
    	    	obj.centerImage();
    	    
    		    //get Buttons on top 
	    	var prodGuide = $('#mk-product-page .mk-product-guide');
		    
	    	var container = prodGuide.find('.mk-product-option-cont')
		    	.addClass('zoom-window')
		    	.prepend('<div class="zoom-buy-now-overlay"></div>');
    		
		    container.find('.mk-size-drop')
    			.before(
    				$('<div class="zoom-price-block"></div>')
    				.prepend(
    					prodGuide.find('.discount-message-pdp').clone())
					.prepend(
						prodGuide.find('h3').clone())
				);

		    container.css('min-width', container.width());
		    
		    if (container.find('.mk-sold-out').length) {
		    	$('.zoom-buy-now-overlay, .mk-sold-out', container).wrapAll('<div class="zoom-buy-now-holder"/>');
    		    }
		    else {
		    	container.find('.zoom-buy-now-overlay')
		    		.before('<div class="zoom-hide-block"><div class="zoom-close mk-hide"><span class="ico ico-expand"></span>SIZE OPTIONS</div><div class="zoom-open mk-show"><span class="ico ico-collapse"></span>HIDE THIS</div></div>');
		    	$('.zoom-window .zoom-price-block, .zoom-window .mk-colour, .zoom-window .mk-custom-drop-down, .zoom-window .add-button-group, .zoom-window .notify-cont, .zoom-window .zoom-buy-now-overlay, .zoom-window .mk-sold-out')
		    		.wrapAll('<div class="zoom-buy-now-holder"/>');
    		    }
		    
		    $('.saved-message, .social_action_popup, .kuliza_social_wrapper, .mk-size-guides, #notify-cont, .mk-product-helper, .mk-save-for-later, #sizechart-new, #sizechart-old, .mk-product-disclaimer-container, .mk-color-drop, #jealous-fit-guide')
		    	.addClass('mk-z-hide');
		    

    		    //align button in the middle
		    var left_pos = ($(window).width() - container.width())/2;
		    container.css('left', left_pos + 'px');

            $('.zoom-buy-now-holder').slideUp('slow', function(){
                $('.zoom-close').removeClass("mk-hide").addClass("mk-show");
                $('.zoom-open').removeClass("mk-show").addClass("mk-hide");
            });

    		    
    		    _gaq.push(['_trackEvent', 'pdp', 'product_large_image']);
    		});
		    
    		//show super zoom hide izoom
    		/*$('.izoom-imgs').live('click', function(){
    			$('.izoom-carousel').hide();
    			$('.szoom-carousel').show();
    			scrollPos= $(window).scrollTop();
    	    	Myntra.Utils.scrollToPos(0);
    	    	obj.setSZoomWindow();
    	    	obj.centerImage();
     		
    		});*/
 			
 			//back to intermediate zoom
    		$('.szoom-imgs').click(function(){
    			$('#view-next-'+zoomLevel).trigger('click');
    			//obj.closeZoom();
    			/*$('.szoom-carousel').hide();
    			$('.izoom-carousel').show();
    			obj.setIZoomWindow();*/
    		});
    		
    		//vertical image mouse hover scroll on super zoom
 			$(".szoom li img").mousemove(function(event){
     	    	var yPos=event.pageY;
     			yPos=(yPos*(superZoomMaxHeight/(winHeight)));
     			$(this).css({"top":-yPos,"left":0});
     		});
    		
    		$('.izoom-carousel .close-btn, .szoom-carousel .close-btn, .zoom-overlay').live('click',function(){
    			obj.closeZoom();
    	    });
        	
    		$(window).resize(function(){
    			/*if($('.zoom-container').is(':visible')){
    				obj.setIZoomWindow();
    				//	align button in the middle
    				var left_pos=($(window).width()-$('.zoom-window').width())/2;
    				$('#mk-product-page .mk-product-guide .mk-product-option-cont').css('left',left_pos+'px');
    			}*/
        	});
    	}
    	
    	/*obj.setIZoomWindow = function(){
    		var winWidth=$(window).width();
    		var winHeight=$(window).height();
    		var iZoomWidthTemp=(winHeight/4)*3;
    		iZoomHeight=winHeight;
    		iZoomWidth=iZoomWidthTemp;
    		$(".izoom-carousel").css({"width":iZoomWidthTemp+"px", "height":winHeight+"px","background":"#fff"});
    		$(".zoom-block").css({"width" : iZoomWidthTemp+"px"});
    		$(".zoom-block").css({"left" : ((winWidth - iZoomWidthTemp)/2)+"px"});
    		
    		zoomLevel="izoom";
    		$('.zoom-overlay, .zoom-block').show();
    		if($(".izoom").hasClass("to-be-loaded")){
    			obj.loadIZoomImages();
    		}

	    	$(".izoom-carousel").css({"margin" : "0 auto"});
			$("html").addClass('mk-overflow-hidden');
			$("body").addClass('mk-overflow-hidden');
			$('.izoom-carousel ul').height(iZoomHeight);
			$('.izoom-carousel , .izoom-carousel li, .izoom-carousel .mask').height(iZoomHeight).width(iZoomWidth);
			$('.izoom-carousel').show();
			$('.szoom-carousel').hide();
    	}
    	
    	obj.loadIZoomImages = function(){
    		$('.izoom li.mk-show').each(function(){
    			var img = new Image();
    			var imgSrc = $(this).find('img').attr('data-src');
    			var imgEl = $(this).find('img');
	    		img.onload = function(){
	    			$(imgEl).attr('src',imgSrc);
	    			$('.izoom-carousel .izoom-loader').removeClass("mk-show").addClass("mk-hide");
		    			$('.izoom li.mk-hide').each(function(idx,val){
				    		$(this).find('img').attr('src',($(this).find('img').attr('data-src')));
				    		$(this).find('img').css({height:iZoomHeight,width:iZoomWidth});
				    	});
		    			$(".izoom").removeClass("to-be-loaded");
	    		}
	    		img.src= imgSrc;
	    		$(imgEl).css({height:iZoomHeight,width:iZoomWidth});
    		});
    	}*/
    	
    	obj.loadSZoomImages = function(){
    		$('.szoom li.mk-show').each(function(){
    			var img = new Image();
    			var imgSrc = $(this).find('img').attr('data-src');
    			var imgEl = $(this).find('img');
    			img.onload = function(){
	    			$(imgEl).attr('src',imgSrc);
	    			$('.szoom-carousel .szoom-loader').removeClass("mk-show").addClass("mk-hide");
	    			$('.szoom li.mk-hide').each(function(idx,val){
			    		$(this).find('img').attr('src',($(this).find('img').attr('data-src')));
			    		$(this).find('img').css({height:sZoomHeight,width:sZoomWidth});
			    	});
			    	$(".szoom").removeClass("to-be-loaded");
	    		}
	    		img.src= imgSrc;
	    		$(imgEl).css({height:sZoomHeight,width:sZoomWidth});
    		});
    	}
    	
    	obj.setSZoomWindow = function(){
    		var superZoomHeight=(docHeight>sZoomHeight)?sZoomHeight:docHeight;
    		var winWidth=$(window).width();
        	$(".szoom-carousel").css({"width":sZoomWidth+"px", "height":winHeight+"px", "background":"#fff"});
        	$(".zoom-block").css({"left" : ((winWidth - sZoomWidth)/2)+"px"});
			$(".zoom-block").css({"width" : sZoomWidth+"px"});
			
			zoomLevel="szoom";
			$('.zoom-overlay, .zoom-block').show();
			if($(".szoom").hasClass("to-be-loaded")){
				obj.loadSZoomImages();
			}
			
			$("html").addClass('mk-overflow-hidden');
			$("body").addClass('mk-overflow-hidden');
			
	    	$('.szoom-carousel , .szoom-carousel ul, .szoom-carousel li, .szoom-carousel .mask').height(winHeight);
    	}   
    	
    	obj.closeZoom = function(){
    		Myntra.Utils.scrollToPos(scrollPos);
    		
			//remove all class from 
		$('#mk-product-page .mk-product-guide .mk-product-option-cont')
			.removeClass('zoom-window')
			.removeAttr('style')
				.find('.zoom-buy-now-overlay, .zoom-price-block, .zoom-hide-block').remove()
			.end()
				.find('.zoom-price-block, .mk-colour, .mk-custom-drop-down, .add-button-group, .notify-cont, .zoom-buy-now-overlay').unwrap();

    		
		$('.saved-message, .social_action_popup, .kuliza_social_wrapper, .mk-size-guides, #notify-cont, .mk-product-helper, .mk-save-for-later, #sizechart-new, #sizechart-old, .mk-product-disclaimer-container, .mk-color-drop, #jealous-fit-guide')
			.removeClass('mk-z-hide');
		
		zoomLevel = "normal";
        	$('.zoom-overlay, .zoom-block, .zoom-container, .izoom-carousel, .szoom-carousel').hide();        
			$("html").removeClass('mk-overflow-hidden');
			$("body").removeClass('mk-overflow-hidden');
    		//remove the extra margin added to prevent shifting of content behind the overlay. overflow hidden property for body removes scrollbars
		$("body").css("margin-left", "0px");
        obj.isOpened = false;
    	}
    	
    	obj.centerImage = function(){
        	var yPos=(superZoomMaxHeight/2);
    		$('.zoom-block ul.szoom li img').css({"top":-yPos,"left":0});
        }
    	
    	obj.transitionPDPImage = function(ind){
    		$('.mk-product-page .mk-more-views li:eq('+(ind)+') img').trigger('click');
    	}
    	
    	obj.transitionIZoomImage = function(ind){
    		$('.izoom li').removeClass("mk-show").addClass("mk-hide");
    		$('.izoom li:eq('+ind+')').removeClass("mk-hide").addClass("mk-show");
    	}
    	
    	obj.transitionSZoomImage = function(ind){
    		$('.szoom li').removeClass("mk-show").addClass("mk-hide");
    		$('.szoom li:eq('+ind+')').removeClass("mk-hide").addClass("mk-show");
    	}
    	
    	obj.initPDPThumbnails = function(){
    		// Product Image Switch ---------------------
    		$('.mk-product-page .mk-more-views li a').click(function(e){e.preventDefault();});
    		$('.mk-product-page .mk-more-views li img').click(function() {
    		$('.mk-product-page .mk-more-views li').removeClass('img-selected');
    		$(this).parent().parent().addClass('img-selected');
    		var regular = $(this).attr('alt');
    		var zoom 	= $(this).attr('data-src');
   			$('a.cloud-zoom-cont').attr('href', zoom);
   			$('a.cloud-zoom-cont img').attr('src',regular);
   			
   			var ind=$('.mk-product-page .mk-more-views li.img-selected').index();
   			obj.transitionIZoomImage(ind);
   			obj.transitionSZoomImage(ind);
   		});
   		$('.mk-product-page .mk-more-views img').live("mouseenter", function() {
       		$(this).click();
       	});
    	}
    	
    	obj.initImageTransitions = function(){
    		if($('.mk-product-page .mk-more-views li').size() > 1){
    		//Product Image Switch on clicking next/prev
    		$('#view-next-pdp').bind('click',function(e){
    			var ind=$('.mk-product-page .mk-more-views li.img-selected').index();
    			((ind+1)==noOfImages)?ind=0:ind=ind+1;
    			obj.transitionPDPImage(ind);
    			obj.transitionIZoomImage(ind);
    			obj.transitionSZoomImage(ind);
    			e.stopPropagation();
    		});
    		$('#view-prev-pdp').bind('click',function(e){
    			var ind=$('.mk-product-page .mk-more-views li.img-selected').index();
    			(ind==0)?ind=noOfImages-1:ind=ind-1;
    			obj.transitionPDPImage(ind);
    			obj.transitionIZoomImage(ind);
    			obj.transitionSZoomImage(ind);
    			e.stopPropagation();
    		});
    		$('#view-next-izoom').bind('click',function(e){
    			var ind=$('.izoom li.mk-show').index();
    			((ind+1)==noOfImages)?ind=0:ind=ind+1;
    			obj.transitionPDPImage(ind);
    			obj.transitionIZoomImage(ind);
    			obj.transitionSZoomImage(ind);
    			e.stopPropagation();
    		});
    		$('#view-prev-izoom').bind('click',function(e){
    			var ind=$('.izoom li.mk-show').index();
    			(ind==0)?ind=noOfImages-1:ind=ind-1;    			
    			obj.transitionPDPImage(ind);
    			obj.transitionIZoomImage(ind);
    			obj.transitionSZoomImage(ind);
    			e.stopPropagation();
    		});
    		$('#view-next-szoom').bind('click',function(e){
    			var ind=$('.szoom li.mk-show').index();
    			((ind+1)==noOfImages)?ind=0:ind=ind+1;
    			obj.transitionIZoomImage(ind);
    			obj.transitionSZoomImage(ind);
    			obj.transitionPDPImage(ind);
    			e.stopPropagation();
    		});
    		$('#view-prev-szoom').bind('click',function(e){
    			var ind=$('.szoom li.mk-show').index();
    			(ind==0)?ind=noOfImages-1:ind=ind-1;
    			obj.transitionIZoomImage(ind);
    			obj.transitionSZoomImage(ind);
    			obj.transitionPDPImage(ind);
    			e.stopPropagation();
    		});

    		//keyboard support for next/prev key
    		$(document).unbind('keydown.pdp');
    	   	$(document).bind('keydown.pdp',function(e){
            if (!obj.isOpened) {
                return;
            }
    	   		if(e.keyCode==39){    	   			
    				$('#view-next-'+zoomLevel).trigger('click');
    			}
    			else if(e.keyCode==37){    				
    				$('#view-prev-'+zoomLevel).trigger('click');
    			}
    			else if(e.keyCode == '27'){
    				obj.closeZoom();
			    }
    		});
    	}
    	else{
    		$('#view-next-pdp, #view-prev-pdp, #view-next-izoom, #view-prev-izoom, #view-next-szoom, #view-prev-szoom').hide();	
    	}

};

obj.preloadZoomImages = function(){
    	$('.izoom li, .szoom li').each(function(){
    		var img = new Image();
			var imgSrc = $(this).find('img').attr('data-src');
    		img.src= imgSrc;
    	});
};
    	
return obj;

})();

$(document).ready(function(){
    	Myntra.PDP.Zoom.initZoomClick();
    	Myntra.PDP.Zoom.initPDPThumbnails();
    	Myntra.PDP.Zoom.initImageTransitions();
});

//zoom code ends here
