
$(document).ready(function() {
    var check = $('.main-content .toggle-pass input[type="checkbox"]'),
        pass = $('.main-content .pass'),
        frm = $('.frm-checkout-login'),
        email = frm.find('input[name="email"]'),
        password = frm.find('input[name="password"]'),
        action = frm.find('input[name="_action"]'),
        fpassMsg = $('.fpass-msg'),
        loading = frm.find('.loading');

    check.change(function() {
        if (check.is(':checked')) {
            pass.removeClass('hide');
            _gaq.push(['_trackEvent', 'checkout_login', 'have_an_account', 'checked']);
            password.focus();
        }
        else {
            pass.addClass('hide');
            _gaq.push(['_trackEvent', 'checkout_login', 'have_an_account', 'unchecked']);
        }
    });

    var showErr = function(msg) {
        var errNode = frm.parent().find('.err');
        if (!errNode.length) {
            errNode = $('<div class="err"></div>').insertBefore(frm);
        }
        errNode.html(msg).show();
    };

    var hideErr = function() {
        frm.parent().find('.err').hide();
    };

    $('.btn-continue', frm).click(function(e) {
        if (!email.val().length) {
            e.preventDefault();
            showErr('Please enter the email address');
        }
        else {
            action.val('login');
            console.log('continue click', action.val());
        }
    });

    $('.btn-forgot-pass').click(function(e) {
        if (!email.val().length) {
            e.preventDefault();
            showErr('Please enter the email address');
        }
        else {
            action.val('fpass');
            frm.submit();
        }
    });

    frm.submit(function(e) {
        _gaq.push(['_trackEvent', 'checkout_login', 'form_submit']);
    });

    /*
    frm.submit(function(e) {
        e.preventDefault();
        hideErr();
        $.ajax({
            type: 'POST',
            url: location.href,
            data: frm.serialize(),
            dataType: 'json',
            beforeSend:function() {
                loading.show();
            },
            success: function(resp) {
                if (!resp) {
                    alert('Invalid JSON data returned from the Server');
                    return;
                }
                else {
                    if (resp.status == 'SUCCESS') {
                        if (action.val() === 'fpass') {
                            fpassMsg.find('strong').text(email.val());
                            fpassMsg.removeClass('hide');
                        }
                        else {
                            location.href = resp.redirect;
                        }
                    }
                    else {
                        showErr(resp.errMsg);

                        if (resp.showPassInput) {
                            frm.find('.toggle-pass input').attr('checked', 'true');
                            frm.find('.pass').removeClass('hide');
                        }
                        else {
                            frm.find('.toggle-pass input').removeAttr('checked');
                            frm.find('.pass').addClass('hide');
                        }
                    }
                }
            },
            complete: function(xhr, status) {
                loading.hide();
            }
        });
        _gaq.push(['_trackEvent', 'checkout_login', 'form_submit']);
    });
    */

    $(document).bind('myntra.fblogin.auth', function(e, data) {
        // show the loading
    });
    $(document).bind('myntra.fblogin.done', function(e, data) {
        location.href = 'https://' + location.host + '/checkout-address.php';
    });
    $('.login-page .btn-fb-connect').on('click', function(e) {
        Myntra.FBConnect();
        _gaq.push(['_trackEvent', 'checkout_login', 'fb_connect']);
    });

	//$('.login-page .toggle-pass').jqTransform();

});
