
$(document).ready(function() {
    // After successful login/signup, the page is refreshed.
    // So, cookie is used to trigger the *after* events like GA tracking and showing msg.
    window._gaq = window._gaq || [];
    var ckLogin = Myntra.Utils.Cookie.get('_mklogin');
    var ckGiftCard = Myntra.Utils.Cookie.get('_mkgc');
    var ckGiftCardLogged = Myntra.Utils.Cookie.get('_mkgclogged');
  /*  if (showNewSignUpConfirmation){
          setTimeout(function(){
               $(document).trigger("myntra.login.show");
          },1000);
      }
*/
    if (ckLogin) {
        var parts = ckLogin.split('*');
        if (typeof Myntra.Data.showNewSignUpConfirmation != "undefined" && Myntra.Data.showNewSignUpConfirmation ){
            setTimeout(function(){
                $(document).trigger("myntra.login.show");
                },1000);
	    if(parts[2] == 'success-signinfrmsignup') parts[1] = 'signin';
	    _gaq.push(['_trackEvent', parts[0], parts[1], parts[2]]);
            Myntra.Utils.Cookie.del('_mklogin');
            Myntra.Utils.Cookie.del('_mkgc');
            Myntra.Utils.Cookie.del('_mkgclogged');
                    
            return ;
        }
        if(parts[2] == 'success-signinfrmsignup') parts[1] = 'signin';
        
        Myntra.Utils.Cookie.del('_mklogin');
        _gaq.push(['_trackEvent', parts[0], parts[1], parts[2]]);
        if (parts[1] === 'signup' && location.protocol === 'http:') {
            dataLayer.push({'pageName': 'signup'});
	          var signupAmt = 'Rs. ' + Myntra.Data.nonFBRegCouponValue ;
            var msg = [
                       '<p>You have successfully created a new Myntra account.</p>',
                   	   '<p>You have earned following coupons.</p> '+Myntra.Data.RegCouponSignUpOverLayList+' <p>you can redeem these coupons on your purchase.</p>'
                   	   ];
            var status = '';
            if(ckGiftCard){
            	ckGiftCard = unescape(ckGiftCard);
            	var gcParts = ckGiftCard.split('*');
            	Myntra.Utils.Cookie.del('_mkgc');
            	if(gcParts[0] === 'success'){
            		msg.push('<br/><br/>Gift Card Activated. <span class="red">Rs. '+ gcParts[1] +'</span> has been added to Your <a href="/mymyntra.php?view=mymyntcredits">CashBack</a>');
            		msg.push('<br/><br/> Your Cashback Balance : <span class="red">Rs. ' + gcParts[2] + '</span>');
            		status = 'Congratulations!';
            	}
            	else{
            		msg.push('<br/><br/>' + gcParts[1]);
            		status = 'Sorry!';
            	}
            }
            else{
            	status = 'Congratulations!';
            }
            //Myntra.MsgBox.show(status, msg.join(''), {autohide:5000});
            if(msg.length > 0){
            	Myntra.MsgBox.show(status, msg.join(''), {autohide:5000});
            }
        }
        if (parts[1] === 'signin' && location.protocol === 'http:') {
           var msg=[];
        	 var status='';

           if(ckGiftCard){
        	    ckGiftCard = unescape(ckGiftCard);
             	var gcParts = ckGiftCard.split('*');
             	Myntra.Utils.Cookie.del('_mkgc');
             	if(gcParts[0] === 'success'){
             		msg.push('<br/><br/>Gift Card Activated. <span class="red">Rs. '+ gcParts[1] +'</span> has been added to Your <a href="/mymyntra.php?view=mymyntcredits">CashBack</a>');
             		msg.push('<br/><br/> Your Cashback Balance : <span class="red">Rs. ' + gcParts[2] + '</span>');
             		status = 'Congratulations!';
             	}
             	else{
             		msg.push('<br/><br/>' + gcParts[1]);
             		status = 'Sorry!';
             	}
            }
        	 else{
        		 status = 'Congratulations!';
        	 } 

           if(parts[2] == 'success-signinfrmsignup'){
              msg.push('<br/> It seems you already have an account with us.');
              msg.push('<br/><strong> WE ARE LOGGING YOU INTO YOUR ACCOUNT</strong>');
              status = 'WELCOME BACK!';
           }

        	//Myntra.MsgBox.show(status, msg.join(''), {autohide:5000});
        	if(msg.length > 0){
        		Myntra.MsgBox.show(status, msg.join(''), {autohide:10000});
        	}
        }
        
        // || parts[2] === 'success_signup')
        if (parts[1] === 'fb_connect' && parts[2] === 'success_login' && location.protocol === 'http:') {
        	var msg=[];
       	 	var status='';	
       	 	if(ckGiftCard){
       	 		ckGiftCard = unescape(ckGiftCard);
       	 		var gcParts = ckGiftCard.split('*');
            	Myntra.Utils.Cookie.del('_mkgc');
            	if(gcParts[0] === 'success'){
            		msg.push('<br/><br/>Gift Card Activated. <span class="red">Rs. '+ gcParts[1] +'</span> has been added to Your <a href="/mymyntra.php?view=mymyntcredits">CashBack</a>');
            		msg.push('<br/><br/> Your Cashback Balance : <span class="red">Rs. ' + gcParts[2] + '</span>');
            		status = 'Congratulations!';
            	}
            	else{
            		msg.push('<br/><br/>' + gcParts[1]);
            		status = 'Sorry!';
            	}
       	 	}
       	 	else{
       	 		status = 'Congratulations!';
       	 	} 
       	 	//Myntra.MsgBox.show(status, msg.join(''), {autohide:5000});
       	 	if(msg.length > 0){
       	 		Myntra.MsgBox.show(status, msg.join(''), {autohide:5000});
       	 	}
       }
        
       if (parts[1] === 'fb_connect' && parts[2] === 'success_signup' && location.protocol === 'http:') {
            dataLayer.push({'pageName': 'signup'});
       	   var msg = [
       	   '<p>You have successfully created a new Myntra account.</p>',
       	   '<p>You have earned following coupons.</p> '+Myntra.Data.FBRegCouponSignUpOverLayList+' <p>you can redeem these coupons on your purchase.</p>'
       	   ];
    	   var status = '';
    	   if(ckGiftCard){
    		   ckGiftCard = unescape(ckGiftCard);
               var gcParts = ckGiftCard.split('*');
               Myntra.Utils.Cookie.del('_mkgc');
               if(gcParts[0] === 'success'){
            	   msg.push('<br/><br/>Gift Card Activated. <span class="red">Rs. '+ gcParts[1] +'</span> has been added to Your <a href="/mymyntra.php?view=mymyntcredits">CashBack</a>');
            	   msg.push('<br/><br/> Your Cashback Balance : <span class="red">Rs. ' + gcParts[2] + '</span>');
            	   status = 'Congratulations!';
               }
               else{
            	   msg.push('<br/><br/>' + gcParts[1]);
            	   status = 'Sorry!';
               }
           }
    	   else{
    		   status = 'Congratulations!';
    	   } 
           //Myntra.MsgBox.show(status, msg.join(''), {autohide:5000});
           if(msg.length > 0){
        	   Myntra.MsgBox.show(status, msg.join(''), {autohide:5000});
           }
       } 
    }
    if(ckGiftCardLogged){
    	var msg=[];
    	var status='';
    	ckGiftCardLogged = unescape(ckGiftCardLogged);
    	ckGiftCardLogged = ckGiftCardLogged.replace(/\+/g," ");
    	var gcParts = ckGiftCardLogged.split('*');
     	Myntra.Utils.Cookie.del('_mkgclogged');
     	if(gcParts[0] === 'success'){
     		msg.push('<br/><br/>Gift Card Activated. <span class="red">Rs. '+ gcParts[1] +'</span> has been added to Your <a href="/mymyntra.php?view=mymyntcredits">CashBack</a>');
     		msg.push('<br/><br/> Your Cashback Balance : <span class="red">Rs. ' + gcParts[2] + '</span>');
     		status = 'Congratulations!';
     	}
     	else{
     		msg.push('<br/><br/>' + gcParts[1]);
     		status = 'Sorry!';
     	}     	
     	//Myntra.MsgBox.show(status, msg.join(''), {autohide:5000});
     	if(msg.length){
     		Myntra.MsgBox.show(status, msg.join(''), {autohide:5000});
     	}
    }
    
    Myntra.Utils.Cookie.del('_mklogin');
    Myntra.Utils.Cookie.del('_mkgc');
    Myntra.Utils.Cookie.del('_mkgclogged');
});

