
Myntra.FBInit=function(){
    window.fbAsyncInit = function() {
        FB._https = true;
        FB.init({
            appId      : facebook_app_id,
            channelUrl : (location.protocol == 'https:' ? https_loc + '/s_fbchannel.php' : http_loc + '/fbchannel.php'),
            status     : true,
            cookie     : true,
            xfbml      : true,
            oauth      : true
        });
        $(document).trigger('myntra.fb.init.done');
        
        //Kuliza Code Start
        /** 
        *  To track the likes done by the users 
        *  we will just track the user basic details since we wont have the access token present here.
        */

        //Like Button code for myntra pdp page.
        FB.Event.subscribe('edge.create',
            function(response) {
                // Hooking Loyalty points flow
                // Myntra.FB.loyaltyFlow('like_flow',response); // LP awards on like button click will come in Loyalty v2.
                FB.getLoginStatus(function(response){
                   if(response.status == 'connected') {
                       Myntra.FB.uid = response.authResponse.userID;
                       Myntra.FB.access_token = response.authResponse.accessToken;
                       Myntra.FB.postToFb('like_flow');
                   } 
                });
            }
        );
        //Kuliza Code End
        
        //Show loyalty widget only when the like button is loaded, Commenting LP awards on like button click will come in Loyalty v2.
        // FB.Event.subscribe('xfbml.render', function(response) {
        //       $('#loyalty-widget').show(); // If any loyaly points widget is there show it
        // });
        
        // FB.Event.subscribe('edge.remove',function(url){
        //     //When user unlike a product call the loyalty points flow
        //     Myntra.FB.loyaltyFlow('unlike_flow',url);
        // });
       	
    };
    (function(d){
        var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
        js = d.createElement('script'); js.id = id; js.async = true;
        js.src = "https://connect.facebook.net/en_US/all.js";
        d.getElementsByTagName('head')[0].appendChild(js);
    }(document));
}


Myntra.FBConnect = function(referer) {
    FB.login(function(response) {
        if (response.authResponse) {
            // user successfully connected with FB
            
            $(document).trigger('myntra.fblogin.auth');

            // now do the post actions in our end
            var url = '/myntra/ajax_facebook_login.php';
            if (referer) {
                url += '?ref=' + referer;
            }
            $.post(url,
                {menu_usertype:"C", menu_redirect:"1", menu_filetype:"myhome",_token:Myntra.Data.token},
                function(data){
                    if (data.status == "success") {
                      	data.fb_action = data.fb_action || 'login';
                        $(document).trigger('myntra.fblogin.done', data);
                    }
                    else{
                        $(document).trigger('myntra.fblogin.done', {status:'failure'});
                    }
                },
                "json"
            );
        }
        else {
            $(document).trigger('myntra.fblogin.done', {status:'access-denied'});
        }
    },
    {scope:'user_birthday,user_location,email'}
    );
};


$(window).load(function(){
    Myntra.FBInit();
});

