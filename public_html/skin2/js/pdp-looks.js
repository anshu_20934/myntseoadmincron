
(function() { "use strict";

	var verticalLookWidget = [
			'<div class="mk-right-look-widget">',
			'	<h5></h5>',
			'	<img class="look-image-small" height="128" width:"96" data-src="{path}" src="',cdn_base,'/skin2/images/spacer.gif" />',
			'	<div class="look-items">',
			'    	<ul>',
			'      	</ul>',
			'   </div>',
			'</div>'
		].join(''),

		verticalLookItem = [
	   		'<li class="look-item rs-ca">',
	   		'	<span href="javascript:void(0)" data-href="',http_loc,'/{dre_landing_page_url}" data-styleid="{styleid}" class="quick-look" style="display: none;"></span>', 
			'	<a href="',http_loc,'/{dre_landing_page_url}" title="{stylename}">',
			'  		<div class="mk-product-image">',
   	        '			<img alt="{stylename}" src="{search_image}">',
		    '		</div>',
		    '	</a>',
		    '	<p class="brand-text">{brands_filter_facet}</p>',
		    '	<p class="price-text red">',
		    '		<span class="rupees"> Rs. {discounted_price} </span>',
		   	'	</p>',
		   	'</li>'
        ].join(''),
        
        horizontalLookWidget = [
        	'<div class="pdpv1-looks">',
			'	<h5>COMPLETE THE LOOK</h5>',
			'	<div class="look-pagination"></div>',
			'	<div class="look-carousel rs-carousel">',
			'		<ul class="pdpv1-look-ul rs-carousel-runner">',
			'		</ul>',
			'	</div>',
			'</div>'
        ].join(''),
        
        horizontalLookItem = [
        	'<li class="rs-carousel-item look" data-lookid="{lookId}">',
			'	<div class="look-image">',
			'		<img src="{path}" />',
			'	</div>',
			'	<div class="product-section">',
			'   	<div class="note"><div>Our fashion experts advise you on how to combine clothes & accessories the right way, for you to look stylish & fashionable.</div></div>',
			'	</div>',
			'<div>'
        ].join(''),

        productItem = [
        	'<li class="rs-carousel-item " data-id="{styleid}">',
			'<span href="javascript:void(0)" data-href="',http_loc,'/{dre_landing_page_url}" data-styleid="{styleid}" class="quick-look" style="display: none;"></span>', 
			'	<a href="',http_loc,'/{dre_landing_page_url}" title="{stylename}">',
			'	<div class="mk-product-image">',
			'		<img alt="{stylename}" src="{search_image}">',
			'	</div>',
			'</a>',
			'<div class="mk-product-info">',
			'	<p class="brand-text">{brands_filter_facet}</p>',
			'	<p class="price-text red">Rs. {discounted_price}</p>',
			'</div>',
		'</li>'
        ].join(''),
        formatNum = Myntra.Utils.formatNumber;

	Myntra.PDPLookWidget = {
		init:function(){
			this.lookJSON = {};
			this.options = {};
			this.lookIds = [];
			this.lookToStyleMap = {};
			this.styles = [];
			this.solrObj = {};
			this.solrArray = {};
			this.rightWidget = '';
			this.rightItems = '';
			this.bottomWidget = '';
			this.bottomLooks = '';
			this.bottomLookItems = '';
			this.carouselHeight =0;
			this._init = true;
			this.lookTotal =0;
			this.lookDiscountTotal =0;

		},

		getLookData: function(options){
			
			!this._init && this.init();
			this.options = options || {}
			var that = this;
			$.ajax({
				//url:http_loc+"/resp.json",
				url:http_loc+Myntra.PDP.contentServiceHost+"/relatedsearch?productId="+that.options.styleid,
				beforeSend : function(xhr){
					xhr.setRequestHeader('Authorization', "Basic bW9iaWxlfm1vYmlsZTptb2JpbGU=");	
   					xhr.setRequestHeader('Accept', "application/json");	
				}

			}).done(function (data) {
				if(!data){
					$('.mk-product-sidebar .mk-guarantee').show();
				}else{
					that.lookJSON = data;
					if(that.lookJSON.status && that.lookJSON.status.statusCode == 700 && that.lookJSON.status.totalCount){
						$('.mk-customer-promise.mk-guarantee').show();
						that.createLookToStyleMap();
					}
					else {
						$('.mk-product-sidebar .mk-guarantee').show();
					}
						
				}
				
			});
		},

		createLookToStyleMap : function (){
			var look,styleids =[],lookData,style,styleObj,limit = 0;
			for(look in this.lookJSON.contentEntries){
				if(limit <= 4){
					lookData =  this.lookJSON.contentEntries[look];

					 if(lookData.type === 'look' && lookData.relatedEntities.lookContainsProduct ){
					 	for (var i = 0; i < lookData.relatedEntities.lookContainsProduct["relatedContentEntities"].length; i++) {
						 	styleObj =lookData.relatedEntities.lookContainsProduct["relatedContentEntities"][i];
						 	styleids.push(styleObj.productId)
						 	this.styles.push(styleObj.productId);
					 	}	
					 }
					 else if(lookData.type === 'productCluster' && lookData.relatedEntities.productClusterMember ){
					 	for (var i = 0; i < lookData.relatedEntities.productClusterMember["relatedContentEntities"].length; i++) {
						 	styleObj =lookData.relatedEntities.productClusterMember["relatedContentEntities"][i];
						 	styleids.push(styleObj.productId)
						 	this.styles.push(styleObj.productId);
					 	}	
					 }
					 	
					 this.lookToStyleMap[lookData.id]= styleids;
					 this.lookIds.push(lookData.id);
					 styleids = [];
					 
				}
				limit++;
			}
			this.fetchSolrData();
		},

		fetchSolrData:function(){
			
			var tempStr = this.styles.join(' OR ');
			var that = this;
			var queryObj = [{
				"query": "styleid : ("+tempStr+") AND (count_options_availbale:[0 TO *])",
        		"start": 0,
        		"rows": this.styles.length,
        		"return_docs": true,
        		"facet": false
		       }];
				$.ajax({
						type:'POST',
						url:http_loc + "/searchws/search/styleids2",
						data:JSON.stringify(queryObj),
						dataType:"json",
						cache:false,
						contentType:"application/json"
					}
				).done(function(data){
					that.solrObj = data.response1;
					that.constructSolrObjArray();
					that.createRightWidget();
					that.createBottomWidget();
			});
		},

		constructSolrObjArray:function(){
			
			var styleObj;
			for(var i=0; i < this.solrObj['products'].length;i++){
				styleObj = this.solrObj['products'][i];
				this.solrArray[styleObj.styleid] = styleObj;
				
			}
			
		},

		createRightWidget : function(){
			
			if(this.lookJSON.contentEntries[0].assets && this.lookJSON.contentEntries[0].assets[0].resolutions['96X128']){
				verticalLookWidget = verticalLookWidget.replace('{path}',this.lookJSON.contentEntries[0].assets[0].resolutions['96X128'].domain+this.lookJSON.contentEntries[0].assets[0].resolutions['96X128'].path);	
			}
			this.rightWidget = $(verticalLookWidget).appendTo('.mk-product-sidebar');
			this.rightItems = this.rightWidget.find('.look-items ul');
			if(!this.lookJSON.contentEntries[0].assets || !this.lookJSON.contentEntries[0].assets[0].resolutions['96X128']){
				this.rightWidget.find('h5').text('Pair it with');
				this.rightWidget.find('.look-image-small').remove();
			}
			else{
				this.rightWidget.find('h5').text('Complete the look');
				this.rightWidget.find('.look-image-small').attr('src',this.rightWidget.find('.look-image-small').data('src'));
			}
			this.renderRightWidgetItems();


		},

		createBottomWidget : function(){
			
			this.bottomWidget = $(horizontalLookWidget).appendTo('.pdpv1-look-cont');
			this.bottomLooks = this.bottomWidget.find('.pdpv1-look-ul');
			this.renderBottomWidgetItems();
			if(this.lookJSON.status.totalCount > 1){
				this.initLookCarousel();
			}
			this.initGA();
			this.bottomWidget.show();
		},

		renderRightWidgetItems:function(){	
			var styleid, markup, data,itemid,item;
			//only first look
			var count =1;
			for (styleid in this.lookToStyleMap[this.lookIds[0]]){

				itemid= this.lookToStyleMap[this.lookIds[0]][styleid];
				data = this.solrArray[itemid];
				//markup = verticalLookItem.replace();
				if(!data){
					continue;
				}
				markup = verticalLookItem.replace(/\{(.*?)\}/g, function(str, key) {
                		if (key in data) {
                    		
                    		if(key === 'search_image'){
                    			return data[key].replace('style_search_image','properties').replace('180_240','96_128');
                    		}
                    		else if(key==='discounted_price'){
                    			 return formatNum(data[key]);
                    		}else{
                    			return data[key];	
                    		}
                    		
                		}
                	return str;
            	});
            	item = $(markup).appendTo(this.rightItems);
            	if(data.discount){
            		item.find('.price-text').append(' <span class="strike gray">'+formatNum(data.price)+'</span>');
            		item.append('<p class="price-text red">'+data.dre_discount_label+'</p>');
            	}

            	count++;
            	if(count > 3){
            		return;
            	}
			}
			

		},

		renderBottomWidgetItems:function(){

			var look, markup, lookData,item,itemHtml,limit= 0;
			for(look in this.lookJSON.contentEntries){
				if(limit <= 4){

					this.lookTotal = 0;
					this.lookDiscountTotal = 0;
					itemHtml ='';
					lookData =  this.lookJSON.contentEntries[look];
					if(lookData.assets && lookData.assets[0].resolutions['360X480Xmini']){
						markup = horizontalLookItem.replace('{path}',lookData.assets[0].resolutions['360X480Xmini'].domain+lookData.assets[0].resolutions['360X480Xmini'].path).replace('{lookId}',lookData.id);
					}else{
						markup = horizontalLookItem.replace('{lookId}',lookData.id);
					}
					item = $(markup).appendTo(this.bottomLooks);
					item.find('.product-section').append(this.renderBottomWidgetItemStyles({'lookId':lookData.id,'asset':(lookData.assets && lookData.assets[0].resolutions['360X480Xmini'])?true:false,noofitems:this.lookToStyleMap[lookData.id].length}));
					itemHtml=$('<div class="look-price"><span>TOTAL</span><span class="price red"> Rs. <em>'+formatNum(this.lookDiscountTotal)+'</em> </span>'+(this.lookDiscountTotal<this.lookTotal?'<span class="strike">'+formatNum(this.lookTotal)+'</span>':'')+'<button class="btn primary-btn buy-look">BUY THE LOOK</button></div>');
					if(lookData.assets && lookData.assets[0].resolutions['360X480Xmini']){
						item.find('.look-image').append(itemHtml);
						if(this.lookToStyleMap[lookData.id].length > 6){
							this.initScrollBar(item.find('.product-section'));
						}					
					}else{
						item.addClass('no-image-look');
						itemHtml.appendTo(item).addClass('look-price-center');
						item.find('.buy-look').data('noimage',true);
						item.find('.look-image').remove();
						if(this.lookToStyleMap[lookData.id].length > 5){
							this.initHorizontalCarousel(item.find('.product-section'));
						}
						else if(this.lookToStyleMap[lookData.id].length < 5){
							var totalWidth = 980,itemWidth=980/5,_w;
							_w = itemWidth * this.lookToStyleMap[lookData.id].length;
							item.find('.product-section').css({'width':_w,'margin':'0 auto'});

						}
							
											
					}
					limit++;	
				}	
			}
			this.initBuyLook();
			this.initActions();

		},

		renderBottomWidgetItemStyles : function(lookObj){
			
			var styleid, markup, data,itemid,item;
			
			this.bottomLookItems = $('<ul class="'+(lookObj.asset?'vertical':'horizontal')+' rs-carousel-runner"></ul>');

			for (styleid in this.lookToStyleMap[lookObj.lookId]){
				itemid= this.lookToStyleMap[lookObj.lookId][styleid];
				data = this.solrArray[itemid];
				if(!data){
					continue;
				}
				//markup = verticalLookItem.replace();
				markup = productItem.replace(/\{(.*?)\}/g, function(str, key) {
                		if (key in data) {
                    		//return key === 'price' ? formatNum(data[key]) : data[key];
                    		if(key === 'search_image'){
                    			return data[key].replace('style_search_image','properties').replace('180_240','150_200_mini');
                    		}
                    		else if(key==='discounted_price'){
                    			 return formatNum(data[key]);
                    		}
                    		else{
                    			return data[key];	
                    		}
                    		
                		}
                	return str;
            	});
            	this.lookTotal += data.price;
            	this.lookDiscountTotal += data.discounted_price;
            	item = $(markup).appendTo(this.bottomLookItems);
            	if(data.discount){
            		item.find('.price-text').append(' <span class="strike gray">'+formatNum(data.price)+'</span>');
            		item.find('.mk-product-info').append('<p class="price-text red">'+data.dre_discount_label+'</p>');
            	}

       
			}
			return this.bottomLookItems;
			
		},

		initLookCarousel : function(){
			
			$('.look-carousel').carousel({
				itemsPerPage: 1, // number of items to show on each page
				itemsPerTransition: 1, // number of items moved with each transition
				noOfRows: 1, // number of rows (see demo)
				nextPrevLinks: false, // whether next and prev links will be included
				nextPrevActions:false,
				pagination: true, // whether pagination links will be included
				speed: 'normal', // animation speed
				easing: 'swing',
				insertPagination:function(pagination){
					$(pagination).appendTo($('.look-pagination'));
					return $(pagination);
				},
				after: function(event,data) {
					_gaq.push(['_trackEvent', 'pdp_select_look', data.page.data("lookid"), Myntra.PDP.Data.id]);
					if(data.page.hasClass('no-image-look')){
						$('.look-carousel').animate({height: "370px"}, 500);
							//.({'height':'370px'});
					}
				},
				before: function(event,data){
					if(!data.page.hasClass('no-image-look')){
						$('.look-carousel').animate({height: "100%"}, 500);
					}
				}
			});
	
		},

		initScrollBar : function(elm){
			elm.addClass('vertical');
			elm.jScrollPane();
			
		},

		initHorizontalCarousel : function(elm){
			
			elm.addClass('rs-carousel').css({'width':'950px','padding':'0 15px'});
			elm.carousel({
				itemsPerPage:5, // number of items to show on each page
				itemsPerTransition: 5, // number of items moved with each transition
				noOfRows: 1, // number of rows (see demo)
				nextPrevLinks: true, // whether next and prev links will be included
				pagination: false, // whether pagination links will be included
				speed: 'normal', // animation speed
				easing: 'swing'
			});
		},

		initBuyLook:function(){
			
			var that = this;
			$('.buy-look').click(function(){
		   		var _img=$(this).closest('.look-image').find('img').attr('src');//'http://myntra.myntassets.com/skin2/images/look_main7_360x480.jpg';
		   		var _lookId = $(this).closest('.look').data('lookid');
		   		var noimage = $(this).data('noimage')?true:false;
		   		Myntra.BuyLook.show({photo:_img,styleIds:that.lookToStyleMap[_lookId],_invoker:'pdp_look_details',_style:Myntra.PDP.Data.id,noimage:noimage});
		   		_gaq.push(['_trackEvent', 'pdp_look_details', 'buy_the_look_button', Myntra.PDP.Data.id]);
		   	});
		},

		initGA : function(){
			this.rightWidget.find('.look-items a').on('click',function(e){
				_gaq.push(['_trackEvent', 'pdp_look_sidebar', 'goto_pdp', Myntra.PDP.Data.id]);
				
			});
			this.rightWidget.find('.look-items .quick-look').on('click',function(e){
				_gaq.push(['_trackEvent', 'pdp_look_sidebar', 'quicklook', Myntra.PDP.Data.id]);
				
			});

			this.bottomWidget.find('.pdpv1-look-ul li a').on('click',function(e){
				_gaq.push(['_trackEvent', 'pdp_look_details', 'goto_pdp', Myntra.PDP.Data.id]);
				
			});
			this.bottomWidget.find('.pdpv1-look-ul .quick-look').on('click',function(e){
				_gaq.push(['_trackEvent', 'pdp_look_details', 'quicklook', Myntra.PDP.Data.id]);
				
			});
		},

		initActions:function (){
			var that =this;

			if(this.rightWidget.find('.look-image-small').length){
				this.rightWidget.find('.look-image-small,h5').on('click',function(){
						$('html, body').animate({scrollTop: that.bottomWidget.parent().offset().top-75}, 800, function() {});
				}).on('mouseover',function(){
					that.rightWidget.find('h5').css({'text-decoration':'underline','cursor':'pointer'});
				}).on('mouseout',function(){
					that.rightWidget.find('h5').css({'text-decoration':'none','cursor':'default'});
				});
			}
		}

	}

}());



$(window).load(function(){
	if(Myntra.PDP.PDPV1 && Myntra.PDP.PDPV1Phase2){
			Myntra.PDPLookWidget.getLookData({styleid:Myntra.PDP.Data.id});	
	}
	else if(Myntra.PDP.PDPV1){
		$('.mk-product-sidebar .mk-guarantee').show();
	}
});