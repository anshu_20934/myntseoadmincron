
Myntra.updateSecureHeaderAfterLogin = function() {
    $('#login-trigger').addClass('hide');
    $('#login-welcome').removeClass('hide');
    $('#login-welcome .user-name').html(Myntra.Data.userEmail);
    $('#form-logout').removeClass('hide');
}

Myntra.CheckoutSteps = (function() { "use strict"; return {
    init: function() {
        this.steps = $('#checkout-steps .step');
        if (this.steps.length < 3) return;        
    },

    activate: function(stepNo) {
        if (this.steps.length < 3) return;
        var addressURI = location.protocol+"//"+location.hostname+"/checkout.php#address";
        var paymentURI = location.protocol+"//"+location.hostname+"/checkout.php#payment";
        if (stepNo === 1) {
            this.steps.eq(0).removeClass('done').addClass('active');
            this.steps.eq(1).removeClass('active done');
            this.steps.eq(2).removeClass('active done');
            //remove anchor tags
            if(this.steps.eq(1).find("a"))
                this.steps.eq(1).find("a").children().unwrap();
            if(this.steps.eq(2).find("a"))
                this.steps.eq(2).find("a").children().unwrap();
        }
        else if (stepNo === 2) {
            this.steps.eq(0).removeClass('active').addClass('done');
            this.steps.eq(1).removeClass('done').addClass('active');
            this.steps.eq(2).removeClass('active done');

            //remove anchor tags
            if(this.steps.eq(2).find("a").length>0)
                this.steps.eq(2).find("a").children().unwrap();         

            //add anchor tags for self
            if(this.steps.eq(1).find("a").length==0)
                this.steps.eq(1).children().wrap("<a href=\""+addressURI+"\"/>");
        }
        else if (stepNo === 3) {
            this.steps.eq(0).removeClass('active').addClass('done');
            this.steps.eq(1).removeClass('active').addClass('done');
            this.steps.eq(2).removeClass('done').addClass('active');
            //add anchor tags for address
            if(this.steps.eq(1).find("a").length==0)
                this.steps.eq(1).children().wrap("<a href=\""+addressURI+"\"/>");
            //add anchor tags for payment
            if(!this.steps.eq(2).find("a").length==0)
                this.steps.eq(2).children().wrap("<a href=\""+paymentURI+"\"/>");

        }
    },

    done: function() {
        if (this.steps.length < 3) return;
        this.steps.eq(0).removeClass('active').addClass('done');
        this.steps.eq(1).removeClass('active').addClass('done');
        this.steps.eq(2).removeClass('active').addClass('done');

        if(this.steps.eq(0).find("a").length>0)
            this.steps.eq(0).find("a").children().unwrap();  

        if(this.steps.eq(1).find("a").length>0)
            this.steps.eq(1).find("a").children().unwrap();  

        if(this.steps.eq(2).find("a").length>0)
            this.steps.eq(2).find("a").children().unwrap();  
    }
}}());

$(document).ready(function() {
    Myntra.CheckoutSteps.init();
});


