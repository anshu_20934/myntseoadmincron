define(['backbone',
		'../collections/AddressCollection',
		'text!widgets/Delivery/templates/addressListTemplate.html',
		'./addressLightbox'],
function (Backbone, AddressCollection, AddressListTemplate, AddressLightBoxView) {

	var AddressListView = Backbone.View.extend({

		addressListTemplateHTML: _.template(AddressListTemplate),

		count: 0,

		addressLightbox: null,

		events: {
			'click tr': function (event) {
				event.preventDefault();
				
				var $currentTarget = $(event.currentTarget);
				var currentTargetId = $currentTarget.data('id');

				var selectedRow = $currentTarget.find('td')[0];
				var selectedRowIndex = $currentTarget.data('index');
				var selectedRowHeight = $(selectedRow).height();

                // store the selected addressId in the Data
                Myntra.Data.selectedAddressId = currentTargetId;

				$currentTarget
					.parent()
					.find('a.jqTransformRadio.jqTransformChecked')
					.removeClass('jqTransformChecked')
					.end()
					.find('td.edit a.mk-visible')
					.removeClass('mk-visible').addClass('mk-hide')				
					.end()
					.end()					
					.find('a.jqTransformRadio')
					.addClass('jqTransformChecked')
					.end()
					.find('td.edit a.mk-hide')
					.removeClass('mk-hide').addClass('mk-visible')
			},

			'click tr a.edit-address': function (event) {
	    			_gaq.push(['_trackEvent', 'address', 'edit_address_click']);
				event.preventDefault();

				var $currentTarget = $(event.currentTarget);
				var parentRow = $currentTarget.parentsUntil('tbody')[2];
				var parentRowId = $(parentRow).data('id');

				var selectedModel = AddressCollection.get(parentRowId);	
				
				//for a lightbox specify the title and model data
				var options = {'title': 'EDIT ADDRESS', 'class': 'edit-form-address'};
				options.model = selectedModel.toJSON();
				
				if(!this.addressLightbox) {					
					this.addressLightbox = new AddressLightBoxView(options);					
				} else {
					this.addressLightbox.showLightbox(options);
				}
				event.stopPropagation();
				
			}
		},

		initialize: function (options) {
			
			_.bindAll(this,"render","errorHandler");
			
			AddressCollection.setUrl(options.collectionUrl);
			
			AddressCollection.fetch({
				success: this.render,
				error: this.errorHandler
			});		
		},

		render: function (collection, response, options) {	
			var that = this;

			_.each(collection.toJSON(), function(element, index, list) {
				//element.rowClass = index % 2? 'odd': 'even';
				element.index = that.count++;
				that.$el.append(that.addressListTemplateHTML(element));

			});

			$('.address-list-wrap .list').jScrollPane();
			
			
		},

		renderAddressList: function(collectionData) {
			//this.$el.append(this.addressListTemplateHTML(collectionData));
		},

		errorHandler: function (collection, xhr, options) {
			_gaq.push(['_trackEvent', 'addresslist_view', 'error: ' + collection + " ; " + xhr + " ; " + options]);
		}
	});

	return AddressListView;
});
