define(['backbone',
		'../collections/AddressCollection',
		'text!widgets/common/templates/addressLightbox.html',
		'widgets/common/lightbox/views/lightbox'], function(Backbone, AddressCollection, addressLightboxTemplate, Lightbox) {

		var AddressLightBox = Lightbox.extend({

			'el': 'body',

			'addressLightboxTemplate': _.template(addressLightboxTemplate),

			'events': function() {
				//extend the Lightbox events - close and esc key
				return _.extend({}, Lightbox.prototype.events, {

					'click #lb-address-form .btn-save': function(event) {
	    					_gaq.push(['_trackEvent', 'address', 'address_lightbox_form_btn_save_click']);
						var $addressForm = $('#lb-address-form');
						var serializedFormArray = $addressForm.find('form.addr-form').serializeArray();
					
						if($addressForm.hasClass('new-form-address')) {
							//create a new record
							this.saveAddress(serializedFormArray);

						} else if($addressForm.hasClass('edit-form-address')) {
							//edit the record
							this.editAddress(serializedFormArray);
						}
					},

					'blur .pincode': function (event) {
						var pincodeValue = $(event.currentTarget).val();
						
						//if the pincode is non-numeric and length is less than 6 then return
						if (!/\d{6}/.test(pincodeValue)) {
							return;
						}
						
						var dfd = $.Deferred();

						//get the locality details
						var getLocalityDetails = _.memoize(function(pincode) {
							
							$.ajax('/myntra/s_ajax_pincode_locality.php?pincode=' + pincode, {
								success: function (response) {
									if (response.states.length === 0)
										dfd.reject(response);
									else
										dfd.resolve(response);
								},

								error: function (response) {
									dfd.reject(response);
								}
							});
							return dfd.promise();
						});

						dfd.done(function (response) {

							$('.city').val(response.city).attr('disabled',true);
							$('.state').val(response.state).attr('disabled',true);
							$('.locality').autocomplete('option','source', response.locality);
								
						});

						dfd.fail(function (response) {

							//failed - pincode not found
						});

						getLocalityDetails(pincodeValue);
					},

					'focus .locality': function (event) {
						
					}
				});
			},

			'initialize': function (options) {
				this.render(options);
			},

			'render': function (options) {
				if(!$('#lb-address-form').length) {
					$('body').append(this.addressLightboxTemplate(options||{}));
				} else {
					
					$('body').find('#lightbox-shim').add('#lb-address-form').remove();
					this.events = null;
					$('body').append(this.addressLightboxTemplate(options||{}));
				}

			},

			'getLocalityDetails': function(pincode) {

				var localityDetails = _.memoize(function (pincode) {
					var responseDetails;

					var deferredObject = new $.Deferred();

					$.ajax('/myntra/s_ajax_pincode_locality.php?pincode=' + pincode, {
						success: function (response) {
							deferredObject.resolve();
							responseDetails = response;
							
						}
					});

					deferredObject.done(function () {
						return responseDetails;
					});
					
				});

				return localityDetails(pincode);
			},

			'hideLightbox': function () {
				$('.lightbox-shim').add('.address-form-box').hide();
			},

			'showLightbox': function (options) {

				//if the same lightbox is invoked again, do not alter the dom

				if(options.title == $('.address-form-box .hd .title').html()) {

					$('.lightbox-shim').add('.address-form-box').show();

				} else {

					//re-render the dom to show a new lightbox
					this.render(options);
				}
					
			},

			'saveAddress': function (addressData) {
			},

			'editAddress': function (addressData) {
			}
		});
	
		return AddressLightBox;
});
