define(['backbone'], function(Backbone) {

	var AddressCollection = Backbone.Collection.extend({
		
		collectionVarialbes: ['id', 'address', 'city', 'country', 
							  'defaultAddress', 'email', 'locality',
							  'login', 'mobile', 'name', 'stateCode', 'stateName'],

		getCollectionVariables: function () {
			return this.collectionVarialbes;
		},

		url: function () {
			return this.url;
		},

		parse: function(response) {
			var responseData = response.addressResponse.data;
			var collectionTemp = this;
			return responseData;
		},

		fetch: function (options) {
			options.reset = true;
			return Backbone.Collection.prototype.fetch.call(this, options);
		},

		setUrl: function(url) {
			this.url = url;
		},

		sync: function (method, model, options) {
			options.dataType = "json";
			options.headers = {
				'Accept-Language': 'en-US,en;q=0.8'
			};
			return Backbone.sync(method, model, options);
		},

		initialize: function (options) {
			
		}

	});

	AddressCollection.prototype.setUrl = function(url) {
		this.url = url;
	};
	
	return new AddressCollection();
});