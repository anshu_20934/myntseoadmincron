define(['backbone'], function (Backbone) {

	var LightBox = Backbone.View.extend({

		events: {

			'click .close, .btn-cancel': function (event) {
				//child class to implement the hideLightbox functionality
				this.hideLightbox();
			},

			'keydown': function (event) {
				//close the lightbox if it is an escape key
				if(event.keyCode == 27) {
					this.hideLightbox();
				}
			}
		}
	});

	return LightBox;

});
