define(['backbone',
		//'../collections/AddressCollection',
		'text!widgets/Delivery/templates/addressListTemplate.html'],
		//'./addressLightbox'],
function (Backbone, AddressListTemplate) {

	var AddressListView = Backbone.View.extend({

		addressListTemplateHTML: _.template(AddressListTemplate),

		count: 0,

		addressLightbox: null,

		events: {
			'click tr': function (event) {
				event.preventDefault();
				
				var $currentTarget = $(event.currentTarget);

				if($currentTarget.find('a.jqTransformRadio.jqTransformDisabled').length)
					return false;
				var currentTargetId = $currentTarget.data('id');

				var selectedRow = $currentTarget.find('td')[0];
				var selectedRowIndex = $currentTarget.data('index');
				var selectedRowHeight = $(selectedRow).height();

				$currentTarget
					.parent()
					.find('a.jqTransformRadio.jqTransformChecked')
					.removeClass('jqTransformChecked')
					.end()
					.find('td.address .edit-address .address-serviceability')
					.addClass('hide')
					.end()
					.find('td.address .edit-address .edit-delete-address')
					.addClass('hide')
					.end()
					.find('td.edit a.mk-visible')
					.removeClass('mk-visible').addClass('mk-hide')				
					.end()
					.end()					
					.find('a.jqTransformRadio')
					.addClass('jqTransformChecked')
					.end()
					.find('td.address .edit-address .address-serviceability')
					.removeClass('hide')
					.end()
					.find('td.address .edit-address .edit-delete-address')
					.removeClass('hide')
					.end()
					.find('td.edit a.mk-hide')
					.removeClass('mk-hide').addClass('mk-visible');

				this.$el.find('.edit-address').addClass('hide');

				$currentTarget
					.find('.edit-address').removeClass('hide');
			},

			'click tr a.edit-address': function (event) {
				event.preventDefault();

				var $currentTarget = $(event.currentTarget);
				var parentRow = $currentTarget.parentsUntil('tbody')[2];
				var parentRowId = $(parentRow).data('id');

				var options = {'title': 'EDIT ADDRESS', 'class': 'edit-form-address'};
				event.stopPropagation();
				
			},

			'mouseover .edit-item,.move-item,.delete-item': function (event) {
				$(event.currentTarget).find('.myntra-tooltip').show();
			},

			'mouseout .edit-item, .move-item, .delete-item': function (event) {
				$(event.currentTarget).find('.myntra-tooltip').hide();
			}		
		},	

		initialize: function (options) {
			this.addressList = options.addressList;	
			this.addressId = options.addressId;
			this.render();			
		},

		render: function () {	
			var that = this;
			this.$el.empty();
			var firstAddress;
			
			var sortedList = this.addressList.sort(this.sort_date_desc);
			
			//if a new address is added then it should be first one in the list
			if(this.addressId) {
				
				firstAddress = sortedList[0];
				sortedList = _.sortBy(sortedList.slice(1), function(current) { return !current.defaultAddress });
				sortedList.unshift(firstAddress);

			} else {

				sortedList = _.sortBy(sortedList, function(current) { return !current.defaultAddress });

			}

						
			//iterate over the list of sorted addresses
			_.each(sortedList, function(element, index, list) {
				element.index = that.count++;
				if(index == list.length-1)
					element.lastrow = true;
				that.$el.append(_.unescape(that.addressListTemplateHTML(element)));

			});

			if(sortedList.length == 2) {
				this.$el.find('tr').last().css('border','none');
			}

			var selectedAddress = this.$el.find('tr a.jqTransformChecked');
			if (this.addressId) {
				selectedAddress = this.$el.find('tr[data-id='+this.addressId+'] a.jqTransformRadio');
			}

			if(!selectedAddress.length) {
				selectedAddress = this.$el.find('tr a.jqTransformRadio').not('tr a.jqTransformDisabled').first();
			}
			this.$el.find('tr a.jqTransformRadio').removeClass('jqTransformChecked');
			selectedAddress.addClass('jqTransformChecked')
							.parentsUntil('tr').parent()
							.find('.address .edit-address')
							.css('visibility','visible')
							.find('.address-serviceability')
							.removeClass('hide')
							.end()
							.find('.edit-delete-address')
							.removeClass('hide');

			if($('.address-list-wrap .list').height() == 0) {
				$('.address-content').removeClass('hide');
				$('.address-list-wrap .list').jScrollPane();
				$('.address-content').addClass('hide');
			}
			
			else 
			$('.address-list-wrap .list').jScrollPane();
		},

		sort_date_desc: function (collectionObj1, collectionObj2) {
			return collectionObj2.id - collectionObj1.id;
		}	
		
	});

	return AddressListView;
});