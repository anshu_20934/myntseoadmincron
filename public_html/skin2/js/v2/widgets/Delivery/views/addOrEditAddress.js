define(['backbone',
		'controller',
		'../collections/AddressCollection',
		'text!widgets/Delivery/templates/addOrEditAddress.html'],
		function (Backbone, Controller, AddressCollection, addOrEditAddressTemplate) {

			var AddOrEditAddressView = Backbone.View.extend({

				addOrEditAddressTemplate: _.template(addOrEditAddressTemplate),

				pincodeServicable: true,
				
				
				getPincodeServicable: function () {
					return this.pincodeServicable;
				},

				setPincodeServicable: function(servicable, codServicable) {
					$('.err-pincode-cod-not-available').addClass('hide');
					$('.err-only-cod-available').addClass('hide');
					if(servicable == false && codServicable == true) {
						this.pincodeServicable = codServicable;
						$('.err-only-cod-available').removeClass('hide');
					} else {

						if(servicable) {
							$('.err-pincode-service').addClass('hide');
							$('.btn-continue-addr').removeAttr('disabled');
						} else {
							$('.err-pincode-service').removeClass('hide');
							$('.btn-continue-addr').attr('disabled','disabled');
						}
						if(codServicable === false) {
							$('.err-pincode-cod-not-available').removeClass('hide');
						} else if (!codServicable) {
							$('.err-pincode-cod-not-available').addClass('hide');
						}
						this.pincodeServicable = servicable;
					}
				},

				events: {
					'change .pincode': function (event) {

						var that = this;
						var pincodeValue = $(event.currentTarget).val();
						var city = $('.city');
						var selCity = $('.sel-city');
								
						//if the pincode is non-numeric and length is less than 6 then return
						if (!/\d{6}/.test(pincodeValue)) {
							return;
						}
						
						var dfd = $.Deferred();

						//get the locality details
						var getLocalityDetails = _.memoize(function(pincode) {
							
							$.ajax('/myntra/s_ajax_pincode_locality.php?pincode=' + pincode, {
								success: function (response) {
									if (response.states.length === 0)
										dfd.reject(response);
									else
										dfd.resolve(response);
								},

								error: function (response) {
									dfd.reject(response);
								}
							});
							return dfd.promise();
						});

						dfd.done(function (response) {

							
							that.setPincodeServicable(true);
							var opts = [];
							if(response.cities.length > 1) {
								
								selCity.removeClass('hide');
								selCity.empty();
								$.each(response.cities, function(i, val) {
					                sel = val === response.city ? ' selected ' : '';
					                opts.push('<option value="' + val + '"' + sel + '>' + val + '</option>');
					            });
					            selCity.html(opts.join('')).removeClass('hide');
					            city.addClass('hide').val(selCity.val());
							} else if (response.city.length > 0) {
								city.removeClass('hide');
								selCity.addClass('hide');	
								city.val(response.city).attr('disabled',true);
							}
							$('.locality').val('');
							$('.locality').autocomplete('option', 'source', response.locality);
							$('.locality').autocomplete('close');
							$('.sel-state').val(response.state).attr('disabled',true);
								
						});

						dfd.fail(function (response) {
							//that.setPincodeServicable(false);
							city.removeClass('hide');
							selCity.addClass('hide');	
							city.val(response.city).removeAttr('disabled');
							$('.sel-state').val(response.state).removeAttr('disabled');					
						});

						getLocalityDetails(pincodeValue);
					},

					'click .default-address': function (event) {
						event.stopPropagation();
						$(event.currentTarget).find('.default-address-icon').toggleClass('checked');
					},

					'focus .locality': function (event) {
						$('.locality').autocomplete('search');
					},

					'focus .pincode': function (event) {
						$('.btn-continue-addr').removeAttr('disabled');
					}
				},

				initialize: function (options) {
					this.render(options);
				},

				render: function (options) {
					var that = this;
					var data = {};
					data.model = options.addressObject||{};
					var resultHTML= this.addOrEditAddressTemplate(data);
					that.$el.html(resultHTML);
					that.$el.find('.locality').autocomplete({
			            minLength: 0,
			            source:[]
			        });

			        if(options.addressObject) {
			        	that.$el.find('.pincode').trigger('blur');
				        that.$el.find('.city').attr('disabled', true);
				        that.$el.find('.sel-state').attr('disabled', true);
			    	}
			    	that.$el.find('input').not('[type=hidden]').first().focus();
				}


			});

		return AddOrEditAddressView;
});