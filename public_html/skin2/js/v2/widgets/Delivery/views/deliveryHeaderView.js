define(['backbone',
		'controller',
		'text!widgets/Delivery/templates/deliveryHeader.html'],
		function (Backbone, Controller, deliveryHeaderTemplate) {

			var DeliveryHeaderView = Backbone.View.extend({
				deliveryHeaderTemplate: _.template(deliveryHeaderTemplate),

				/*events: {
					'click a.checkout-wishlist-header': function (event) {
						event.preventDefault();
						Controller.publishNavigationEvent('showWishlist');
					}
				},*/

				initialize: function (options) {
					this.cart = options.cart;
					this.wishlistItemCount = options.wishlistItemCount;
					this.render();
				},

				render: function () {
					var that = this;
					var resultHTML= "";
					var options = {};
					options.cart = this.cart;
					options.wishlistItemCount = this.wishlistItemCount;
					resultHTML = that.deliveryHeaderTemplate(options);
					that.$el.html(resultHTML);
				}
			});

		return DeliveryHeaderView;
});