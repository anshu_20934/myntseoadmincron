define(['backbone',
		'controller',
		'./deliveryHeaderView',
		'./addOrEditAddress',
		'./addresslistview',
		'../collections/AddressCollection',
		'text!widgets/Delivery/templates/addressListContainer.html',
		'text!widgets/common/templates/normalButton.html',
		'text!widgets/common/templates/primaryButton.html'], 
		function (Backbone, Controller, DeliveryHeaderView, AddOrEditAddressView, AddressListView, AddressCollection, addressListContainerTemplate, normalButtonTemplate, continueButtonTemplate) {

	var AddressView = Backbone.View.extend({

		collectionUrl: '',

		addressListContainerTemplate: _.template(addressListContainerTemplate),

		continueButtonTemplate: _.template(continueButtonTemplate),

		normalButtonTemplate: _.template(normalButtonTemplate),

		addressLightbox: null,

		addEditAddressView: null,

		addressListView: null,

		addressObject: [
			{
				'classname': 'name',
				'errmsg': 'ENTER A NAME'
			},

			{
				'classname': 'pincode',
				'errmsg': 'ENTER A VALID PINCODE',
				'validate': function(pincode) {
					if (!/\d{6}/.test(pincode)) {
                		return false;
            		}
            		return true;
				}
			},

			{
				'classname': 'address',
				'errmsg': 'ENTER AN ADDRESS'
			},

			{
				'classname': 'locality',
				'errmsg': 'ENTER A LOCALITY'
			},

			{
				'classname': 'city',
				'errmsg': 'ENTER CITY'
			},

			{
				'classname': 'sel-state',
				'errmsg': 'ENTER STATE'
			},

			{
				'classname': 'mobile',
				'errmsg': 'ENTER A VALID MOBILE NO.',
				'validate': function (mobile) {
					var mobileNumber = mobile;
					if (mobileNumber.charAt(0) == '+' && mobile.length == 13) {
                		return true;                		                		
            		} else if (/\d{12}/.test(mobile)) {
            			return true;
                		
            		} else if (/\d{11}/.test(mobile)) {
            			if(mobileNumber.charAt(0) == '0')
                			return true;
                		else 
                			return false;
            		} else if (/\d{10}/.test(mobile)){
            			return true;
            		}

            		return false;
				}
			}
		],

		getCollectionUrl: function () {
			return this.collectionUrl;
		},

		events: {
			'click .add-new-address' : "showAddNewAddress",
			'click .edit-item': "showEditAddress",
			'click .delete-item': "deleteAddress",
			'click .view-existing-address, .btn-cancel': "viewExistingAddress",
			'click .addressListContainer .btn-continue-addr': "continuePayment",
			'keyup .address-section .add-edit-address-section .addr-form input': 'clearErrorMessages',
			'keyup .address-section .add-edit-address-section .addr-form textarea': 'clearErrorMessages'
		},

		initialize: function (options) {
		
			_.bindAll(this,"render","errorHandler");

			AddressCollection.setUrl(options.collectionUrl);

			$('.widget-overlay').removeClass('hide');

			AddressCollection.fetch({
				//success: this.render,
				error: this.errorHandler
			});
			
			this.listenTo(AddressCollection,"reset",this.render);
		},
		render: function (collection, response, options) {
			$('.widget-overlay').addClass('hide');
			var addressListContainer = this.$el.find('.addressListContainer');
			addressListContainer.empty();
			this.collectionData = collection.toJSON();
			var addressListContainerHTML = this.addressListContainerTemplate();
			var continueButtonHTML = this.continueButtonTemplate({'buttonLabel': 'CONTINUE TO PAYMENT', 'iconType': 'proceed-icon', 'buttonClass': 'btn-continue-addr'});
			var saveAndContinueButtonHTML = this.continueButtonTemplate({'buttonLabel': 'SAVE AND CONTINUE', 'iconType': 'proceed-icon', 'buttonClass': 'btn-edit-continue btn-continue-addr hide'});
			var cancelButtonHTML = this.normalButtonTemplate({'buttonLabel': 'CANCEL', 'buttonClass': 'btn-cancel hide'});
			var resultHTML = addressListContainerHTML + continueButtonHTML + cancelButtonHTML + saveAndContinueButtonHTML;
			
			addressListContainer.append(resultHTML);
			this.showDeliveryHeader();

			//if there are no saved addresses then show add new address
			if(!this.collectionData || this.collectionData.length === 0) {
				this.showAddNewAddress();
			} else {
				this.showAddressList(this.collectionData, response.addressId);
			}

		},

		errorHandler: function (collection, xhr, options) {
			_gaq.push(['_trackEvent', 'delivery_view', 'error: ' + collection + " ; " + xhr + " ; " + options]);
		},

		afterRender: function () {

			//var cartCollection = this.collection.toJSON()[0].cartItemEntry;
			//Controller.publish('bagLoadComplete', cartCollection);
		},

		showDeliveryHeader: function () {

		},
		showDeliveryHeader: function () {
			
			new DeliveryHeaderView({
				el: '.address-section .mk-checkout-header-container',								
			});
		},

		showAddressList: function (addressList, addressId) {
			this.viewExistingAddress();
			if(this.addresListView)
				this.addresListView.undelegateEvents(); 
			this.addressListView = new AddressListView ({
				el: '.address-list-wrap .list table tbody',
				addressList: addressList,
				addressId: addressId
			});
		},

		viewExistingAddress: function (event) {
	    	_gaq.push(['_trackEvent', 'delivery', 'view_existing_address_click']);
	    	$('.locality').autocomplete('close');
							
			this.$el.find('.delivery-bag-header .view-existing').hide();
			this.$el.find('.add-edit-address-section').addClass('hide');
			this.$el.find('.address-list-wrap').show();
			this.$el.find('.delivery-bag-header .select-address').add('.delivery-bag-header .enter-address').show();

			this.$el.find('.addressListContainer .btn-continue').removeClass('hide');
			this.$el.find('.addressListContainer .btn-edit-continue').addClass('hide');
			this.$el.find('.addressListContainer .btn-cancel').addClass('hide');
			$('.btn-continue-addr').removeAttr('disabled');	
		
		},

		showAddNewAddress: function (event) {
	    	_gaq.push(['_trackEvent', 'delivery', 'add_new_address_click']);
			this.$el.find('.address-list-wrap').hide();
			this.$el.find('.add-edit-address-section').removeClass('hide');
			this.$el.find('.delivery-bag-header .select-address').add('.delivery-bag-header .enter-address').hide();
			if(this.collectionData.length > 0) {
				this.$el.find('.delivery-bag-header .view-existing .view-existing-address').text('Choose an address (' + this.collectionData.length + ')');
				this.$el.find('.delivery-bag-header .view-existing').show();
			}

			if(this.addOrEditAddressView)
				this.addOrEditAddressView.undelegateEvents();

			var addressObject = {};
			addressObject.mobile = Myntra.Data.mobile;
			this.addOrEditAddressView = new AddOrEditAddressView({
				el: '.add-edit-address-section',
				addressObject: addressObject
			});
		},

		showEditAddress: function (event) {
	    	_gaq.push(['_trackEvent', 'delivery', 'show_edit_address_click']);		

			this.$el.find('.address-list-wrap').hide();
			this.$el.find('.add-edit-address-section').removeClass('hide');
			this.$el.find('.delivery-bag-header .select-address').add('.delivery-bag-header .enter-address').hide();
			if(this.collectionData.length > 0) {
				this.$el.find('.delivery-bag-header .view-existing .view-existing-address').text('Choose an address (' + this.collectionData.length + ')');
				this.$el.find('.delivery-bag-header .view-existing').show();
			}
			var selectedElement = $(event.currentTarget).parentsUntil('tbody');
			var addressId = $(selectedElement[selectedElement.length-1]).data('id');
			var addressObject = _.where(this.collectionData, {'id': addressId})[0];
			addressObject.id=addressId;

			this.$el.find('.address-list-wrap').hide();
			this.$el.find('.add-edit-address-section').show();
			
			this.$el.find('.addressListContainer .btn-continue').addClass('hide');
			this.$el.find('.addressListContainer .btn-edit-continue').removeClass('hide');
			this.$el.find('.addressListContainer .btn-cancel').removeClass('hide');

			if(this.addOrEditAddressView)
				this.addOrEditAddressView.undelegateEvents();

			this.addOrEditAddressView = new AddOrEditAddressView({
				el: '.add-edit-address-section',
				addressObject: addressObject
			});

			//currentTarget = currentTarget.;

		},

		deleteAddress: function (event) {
	    	_gaq.push(['_trackEvent', 'delivery', 'delete_address_click']);
			var selectedElement = $(event.currentTarget).parentsUntil('tbody');
			var addressId = $(selectedElement[selectedElement.length-1]).data('id');
			AddressCollection.deleteAddress(addressId);
		},

		destroyView: function () {
			this.$el.off();
			this.$el.find('.mk-checkout-header-container *').remove().unbind();
			this.$el.find('.add-edit-address-section *').remove().unbind();
			this.$el.find('.addressListContainer *').remove().unbind();

		},
 		

		continuePayment: function (event) {
			_gaq.push(['_trackEvent', 'delivery', 'btn_continue_click']);

            var cartGAData;
			event.stopPropagation();
			//check whether it is a selected address
			if(this.$el.find('.address-list-wrap').css('display') == 'block') {
				var addressId = this.$el.find('.address-list-wrap tr').has('.jqTransformChecked').data('id');
				//this.undelegateEvents();
				Controller.publishNavigationEvent('selectAddress',addressId);

			} else {
				//if new address or edit address
				var addressEntry = {};
				var container = $('.address-section .add-edit-address-section .addr-form');
				var addressInError = false;
				var errObject = {};
				var errValidation = true;
				container.find('.err').empty();
				addressEntry.name = container.find('.name').val();
				addressEntry.pincode = container.find('.pincode').val();
				addressEntry.address = container.find('.address').val();
				addressEntry.countryCode="IN";
				addressEntry.locality = container.find('.locality').val();
				addressEntry.city = container.find('.sel-city').val() || container.find('.city').val();
				addressEntry.stateCode = container.find('.sel-state').find('option:selected').val() || container.find('.state').val();
				addressEntry.stateName = $.trim(container.find('.sel-state').find('option:selected').text());
				addressEntry.mobile = container.find('.mobile').val();
				addressEntry.defaultAddress = container.find('.default-address .default-address-icon').hasClass('checked');
				for (var addressKey in addressEntry) {
						errObject = _.where(this.addressObject, {'classname': addressKey})[0];
						//extra validation rules in the addressObject
						if(errObject){
							errValidation = errObject.validate? errObject.validate(addressEntry[addressKey]) : true;						
						}

						//check if the address field has some value
						if(addressKey != 'defaultAddress') {
							if(!addressEntry[addressKey] || addressEntry[addressKey].length === 0 || !errValidation) {
								//set the addressInError flag to true, this will not add an entry to the address
								addressInError = true;
								var errObject = _.where(this.addressObject, {'classname': addressKey})[0];
								if(errObject) {
									var errClass = '.err-' + errObject.classname;
									var errMsg = errObject.errmsg;

									container.find(errClass).html(errMsg);
								}
							}
						}
				}
				
				
				//Check pincode validity
				var that = this; 
				var done = function(){
                    var args = [].slice.call(arguments);
					that.addOrEditAddressView.setPincodeServicable.apply(that.addOrEditAddressView, args);

					if(!addressInError && that.addOrEditAddressView.getPincodeServicable()) {
						var addressId = container.find('input[name=id]').val();
						//this.undelegateEvents();

						for (var addressItem in addressEntry) {
							addressEntry[addressItem] = addressEntry[addressItem];
						}
						if(addressId) {
							addressEntry.id = addressId;
							var promise = AddressCollection.updateAddress(addressEntry);
							promise.done(function (response) {
								if (response.status.statusType === 'SUCCESS') {
									Controller.publishNavigationEvent('selectAddress',addressId);
								}
							});
							
						} else {
							var promise = AddressCollection.addNewAddress(addressEntry);
	                        promise.done(function(response) {   
	                            if (response.status.statusType === 'SUCCESS') {
	                                Controller.publishNavigationEvent('selectAddress', response.data[0].id);
	                            }
	                            else {
	                                // TODO: show the error message
	                            }
	                        });
						}
						that.$el.find('.add-edit-address-section').addClass('hide');
						that.$el.find('.addressListContainer').removeClass('hide');
						
						//this.destroyView();				
					}
				};
	            var pXHR = AddressCollection.checkPincodeServicability({'pincode': addressEntry.pincode, 'success': done, 'error': done});


			}
            cartGAData = Myntra.Data.cartGAData;
            if(cartGAData) {
                $.each(cartGAData, function(key,data){
                    var actionNlabel = getGAActionAndLabel(data.category,data.articleType,data.gender,data.brandName); 
                    _gaq.push(['_trackEvent','Checkout_conversion', actionNlabel.action, actionNlabel.label]);
                });
            }
			//$('.widget-overlay').addClass('hide');
			
		},

		clearErrorMessages: function (event) {
			var currentTarget = $(event.currentTarget)
			currentTarget.siblings('.err').empty();
			if(currentTarget.hasClass('pincode')) {
				$('.address-section .add-edit-address-section .addr-form .city').siblings('.err').empty();
			}
		},

		showFooter: function() {

		},
        
	});

	var trackUrl = location.protocol === 'https:' ? https_loc + '/components/s_tracker.php' : http_loc + '/components/tracker.php';
    trackUrl += '?page=checkout_address';
    $.get(trackUrl);

	return function (options) {
		new AddressView({
			el: options.el,
			collectionUrl: options.collectionUrl
		});
	};

});
