define(['backbone'], function(Backbone) {

	var AddressCollection = Backbone.Collection.extend({
		
		url: function () {
			return this.url;
		},

		parse: function(response) {
			var responseData = response.data;
			return responseData;
		},

		setUrl: function(url) {
			this.url = url;
		},

		sync: function (method, model, options) {
			var returnObject;
			var tempUrl;
			options.dataType = "json";
			options.headers = {
				'Accept-Language': 'en-US,en;q=0.8'
			};
			if(method == 'read') {
				tempUrl = this.url;
				this.url = this.url + '/cartserviceability/default?fetchSize=1000';
				returnObject = Backbone.sync(method, model, options);
				this.url = tempUrl;
				return returnObject;
			}
			return Backbone.sync(method, model, options);
		},

		fetch: function (options) {
			options.reset = true;
			return Backbone.Collection.prototype.fetch.call(this, options);
		},

		initialize: function (options) {
			
		},

		updateAddress: function (addressObject) {
			var model = {};
			
			addressObject.userId=Myntra.Data.userEmail;
			addressObject.email=Myntra.Data.userEmail;
			model.addressEntry = addressObject;

			var that = this;
			
			var xhr = $.ajax({
				url: this.url+'/'+addressObject.id,
				type: 'PUT',
				data: JSON.stringify(model.addressEntry),
				contentType: 'application/json',
				headers: { 
        			Accept : "application/json; charset=utf-8",
        			'X-CSRF-TOKEN': Myntra.Data.token,
        			'Accept-Language': 'en-US,en;q=0.8'
    			},
				beforeSend: function () {
					$('.widget-overlay').removeClass('hide');
				},

				success: function(success) {
					//that.reset(that.parse(success));
					that.fetch({});
				},
				error: function (error) {
			 		_gaq.push(['_trackEvent', 'address_collection', 'error: ' + error]);
				},

				complete: function (){
					$('.widget-overlay').addClass('hide');
				}

			});

			return xhr;
		},

		deleteAddress: function (addressId) {
			var that = this;
			var urlAddress = this.url;
			$.ajax({
				url: this.url +'/'+addressId,
				type: 'DELETE',
				headers: { 
        			Accept : "application/json; charset=utf-8", 
        			'X-CSRF-TOKEN': Myntra.Data.token,
        			'Accept-Language': 'en-US,en;q=0.8'   
    			},				

				beforeSend: function () {
					$('.widget-overlay').removeClass('hide');
				},

				success: function(success) {
					that.remove(_.where(that.toJSON(), {'id': addressId}));
					that.reset(that.toJSON());				
				},
				error: function (error) {
			 		_gaq.push(['_trackEvent', 'address_collection', 'error: ' + error]);
				},

				complete: function (){					
					$('.widget-overlay').addClass('hide');
				}

			});
		},

		addNewAddress: function (addressObject) {
			var model = {};
			
			addressObject.userId=Myntra.Data.userEmail;
			addressObject.email=Myntra.Data.userEmail;
			model.addressEntry = addressObject;

			var that = this;
			
			var xhr = $.ajax({
				url: this.url,
				type: 'POST',
				data: JSON.stringify(model.addressEntry),
				headers: { 
        			Accept : "application/json; charset=utf-8",
        			'X-CSRF-TOKEN': Myntra.Data.token,
        			'Accept-Language': 'en-US,en;q=0.8'       
    			},
				contentType: 'application/json',
				
				beforeSend: function () {
					$('.widget-overlay').removeClass('hide');
				},

				success: function(response, status, xhr) {
					that.fetch({'addressId' : response.data[0].id});
				},
				error: function (error) {
			 		_gaq.push(['_trackEvent', 'address_collection', 'error: ' + error]);
				},

				complete: function (){
					$('.widget-overlay').addClass('hide');
				}

			});

            // return the promise
            return xhr;
		},

		checkPincodeServicability: function (pincodeObject) {
				
			return $.ajax({
				url: this.url + '/pincode/'+ pincodeObject.pincode + '/serviceablepaymentmodes',
				type: 'GET', 
				headers: { 
        			Accept : "application/json; charset=utf-8",
        			'X-CSRF-TOKEN': Myntra.Data.token,
        			'Accept-Language': 'en-US,en;q=0.8'        
    			},
				contentType: 'application/json',
							

				success: function(success) {
					if(success.data.codSupported && !success.data.nonCodSupported) {
						pincodeObject.error(false, true)
					} else if(!success.data.nonCodSupported) {
						pincodeObject.error(false);
					} else if(!success.data.codSupported) {
						pincodeObject.success(true, false);
					} else {
						pincodeObject.success(true);
					}

				},
				error: function (error) {
					_gaq.push(['_trackEvent', 'address_collection', 'error: ' + error]);
				},

				complete: function (){
					$('.widget-overlay').addClass('hide');
				}

			});
		}

	});

	AddressCollection.prototype.setUrl = function(url) {
		this.url = url;
	};
	
	return new AddressCollection();
});
