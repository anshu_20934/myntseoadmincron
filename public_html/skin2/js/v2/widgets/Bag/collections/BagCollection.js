define(["underscore","backbone", "controller"],function (_, Backbone, Controller) {


	var BagCollection = Backbone.Collection.extend({

		url : function () {
			return this.url;
		},

		parse: function (response) {
			var responseData = response.data;
			return responseData;
		},

		sync: function (method, model, options) {
			options.dataType = "json";
			return Backbone.sync(method, model, options);
		},

		fetch: function (options) {
			options.reset = true;
			options.headers = {
				'Accept-Language': 'en-US,en;q=0.8'
			};
			return Backbone.Collection.prototype.fetch.call(this, options);
		},

		initialize: function () {

		},

		updateBagItem: function (bagObject) {
			var model = {};
			bagObject.operation = 'UPDATE';
			model.cartItemEntry = bagObject;
			var that = this;
			
			$.ajax({
				url: this.url,
				type: 'PUT',
				data: JSON.stringify(model.cartItemEntry),
				contentType: 'application/json',
				headers : {
					Accept : "application/json; charset=utf-8",
        			'X-CSRF-TOKEN': Myntra.Data.token,
        			'Accept-Language': 'en-US,en;q=0.8'
				},				

				beforeSend: function () {
					$('.widget-overlay').removeClass('hide');
				},

				success: function(success) {
					that.reset(that.parse(success));
				},

				error: function (error) {
			 		_gaq.push(['_trackEvent', 'bag_collection', 'error: ' + error]);
				},

				complete: function (){
					$('.widget-overlay').addClass('hide');
				}

			});
		},

		deleteBagItem: function (bagObject) {
			var model = {};
			bagObject.operation = 'DELETE';
			model.cartItemEntry = bagObject;
			var that = this;
			
			var xhr = $.ajax({
				url: this.url,
				type: 'PUT',
				data: JSON.stringify(model.cartItemEntry),
				contentType: 'application/json',
				headers : {
					Accept : "application/json; charset=utf-8",
        			'X-CSRF-TOKEN': Myntra.Data.token,
        			'Accept-Language': 'en-US,en;q=0.8'
				},

				beforeSend: function () {
					$('.widget-overlay').removeClass('hide');
				},

				success: function(success) {
					that.reset(that.parse(success));
				},
				
				error: function (error) {
			 		_gaq.push(['_trackEvent', 'bag_collection', 'error: ' + error]);
				},

				complete: function (){
					$('.widget-overlay').addClass('hide');
				}

			});

			return xhr;
		},

		addBagItem: function (bagObject) {
			var model = {};
			bagObject.operation = 'ADD';
			model.cartItemEntry = bagObject;
			var that = this;
			
			$.ajax({
				url: this.url,
				type: 'PUT',
				data: JSON.stringify(model.cartItemEntry),
				contentType: 'application/json',
				headers : {
					Accept : "application/json; charset=utf-8",
        			'X-CSRF-TOKEN': Myntra.Data.token,
        			'Accept-Language': 'en-US,en;q=0.8'
				},

				beforeSend: function () {
					$('.widget-overlay').removeClass('hide');
				},

				success: function(success) {
					that.reset(that.parse(success));
				},

				error: function (error) {
			 		_gaq.push(['_trackEvent', 'bag_collection', 'error: ' + error]);
				},

				complete: function () {					
					$('.widget-overlay').addClass('hide');
				}

			});
		},

		applyCoupon: function (coupon) {
			var model = {};
			model.coupon = coupon;
			var that = this;
			
			$.ajax({
				url: this.url + "/applymyntcoupon",
				type: 'PUT',
				data: JSON.stringify(model),
				headers: { 
        			Accept : "application/json; charset=utf-8",
        			'X-CSRF-TOKEN': Myntra.Data.token,
        			'Accept-Language': 'en-US,en;q=0.8'       
    			},
				contentType: 'application/json',
				

				beforeSend: function () {
					$('.widget-overlay').removeClass('hide');
				},

				success: function(success) {
					if (!success.data[0].appliedCoupons.length) {
                        return;
                    }
					if(!(success.data[0].appliedCoupons[0].applicationState.state == "ERROR")) {
						
						that.reset(that.parse(success));
					}

					Controller.publish('couponAppliedStatus', success.data[0].appliedCoupons[0], true);					
				},

				error: function (error) {
					Controller.publish('couponAppliedStatus', {'coupon':coupon, 'applicationState':false});
					_gaq.push(['_trackEvent', 'bag_collection', 'error: applyCoupon', error.statusText]);
				},

				complete: function () {					
					$('.widget-overlay').addClass('hide');
				}

			});
		},

		deleteCoupon: function (couponObject) {
			var that = this;
			
			$.ajax({
				url: this.url + "/applymyntcoupon",
				type: 'DELETE',
				data: JSON.stringify(couponObject),
				headers: { 
        			Accept : "application/json; charset=utf-8",
        			'X-CSRF-TOKEN': Myntra.Data.token,
        			'Accept-Language': 'en-US,en;q=0.8'
    			},
				contentType: 'application/json',
				

				beforeSend: function () {
					$('.widget-overlay').removeClass('hide');
				},

				success: function(success) {
					that.reset(that.parse(success));
				},

				error: function (error) {
					_gaq.push(['_trackEvent', 'bag_collection', 'error: deleteCoupon', error.statusText]);
				},

				complete: function () {					
					$('.widget-overlay').addClass('hide');
				}

			});
		},

		addGiftWrap: function (giftwrapObject) {
			var that = this;
			
			$.ajax({
				url: this.url + "/giftwrap",
				type: 'PUT',
				data: JSON.stringify(giftwrapObject),
				headers: { 
        			Accept : "application/json; charset=utf-8",
        			'X-CSRF-TOKEN': Myntra.Data.token,
        			'Accept-Language': 'en-US,en;q=0.8'      
    			},
				contentType: 'application/json',
				

				beforeSend: function () {
					$('.widget-overlay').removeClass('hide');
				},

				success: function(success) {
					Controller.publish('giftmessageAppliedStatus', success.data[0].giftMessage, true);
					that.reset(that.parse(success));									
				},

				error: function (error) {
					Controller.publish('giftmessageAppliedStatus', null, false);
			 		_gaq.push(['_trackEvent', 'bag_collection', 'error: ' + error]);
				},

				complete: function () {					
					$('.widget-overlay').addClass('hide');
				}

			});	
		},

		removeGiftWrap: function () {
			var that = this;
			
			$.ajax({
				url: this.url + "/giftwrap",
				type: 'DELETE',
				headers: { 
        			Accept : "application/json; charset=utf-8",
        			'X-CSRF-TOKEN': Myntra.Data.token,
        			'Accept-Language': 'en-US,en;q=0.8'       
    			},
				contentType: 'application/json',
				

				beforeSend: function () {
					$('.widget-overlay').removeClass('hide');
				},

				success: function(success) {
					that.reset(that.parse(success));									
				},

				error: function (error) {
			 		_gaq.push(['_trackEvent', 'bag_collection', 'error: ' + error]);
				},

				complete: function () {					
					$('.widget-overlay').addClass('hide');
				}

			});	
		},

		changeFreeGift: function (freeGiftObject) {
			var that = this;

			$.ajax({
				url: this.url + "/changefreegift",
				type: 'PUT',
				data: JSON.stringify(freeGiftObject),
				headers: { 
        			Accept : "application/json; charset=utf-8",
        			'X-CSRF-TOKEN': Myntra.Data.token,
        			'Accept-Language': 'en-US,en;q=0.8'        
    			},
				contentType: 'application/json',
				

				beforeSend: function () {
					$('.widget-overlay').removeClass('hide');
				},

				success: function(success) {
					//Controller.publish('freeGiftChangedStatus', success.data[0].giftMessage, true);
					Controller.publish('freeGiftChangedStatus', 'SUCCESS', true);
					that.reset(that.parse(success));									
				},

				error: function (error) {
					//Controller.publish('freeGiftChangedStatus', null, false);
					Controller.publish('freeGiftChangedStatus', 'ERROR', true);
			 		_gaq.push(['_trackEvent', 'changefreegift', 'error: ' + error]);
				},

				complete: function () {					
					$('.widget-overlay').addClass('hide');
				}

			});	
		},

		moveItemtoWishlist: function (cartObject) {
			var that = this;
			
			var xhr = $.ajax({
				url: this.url + "/movetowishlist",
				type: 'PUT',
				data: JSON.stringify(cartObject),
				headers: { 
        			Accept : "application/json; charset=utf-8",
        			'X-CSRF-TOKEN': Myntra.Data.token,
        			'Accept-Language': 'en-US,en;q=0.8'       
    			},
				contentType: 'application/json',
				

				beforeSend: function () {
					$('.widget-overlay').removeClass('hide');
				},

				success: function(success) {
					that.reset(that.parse(success));									
				},

				error: function (error) {
					_gaq.push(['_trackEvent', 'bag_collection', 'error: ' + error]);
				},

				complete: function () {					
					$('.widget-overlay').addClass('hide');
				}

			});	

			return xhr;
		},
		personaliseBagItem: function (bagObject) {
			bagObject.operation = 'CUSTOMIZE';
			
			var that = this;
			$.ajax({
				url: this.url,
				type: 'PUT',
				data: JSON.stringify(bagObject),
				contentType: 'application/json',
				headers : {
					Accept : "application/json; charset=utf-8",
        			'X-CSRF-TOKEN': Myntra.Data.token,
        			'Accept-Language': 'en-US,en;q=0.8'
				},				

				beforeSend: function () {
					$('.widget-overlay').removeClass('hide');
				},

				success: function(success) {
					//console.log('success');
					that.reset(that.parse(success));
				},

				error: function (error) {
			 		_gaq.push(['_trackEvent', 'bag_collection', 'error: ' + error]);
				},

				complete: function (){
					$('.widget-overlay').addClass('hide');
				}

			});
		},

		removePersonalisation: function (bagObject) {
			bagObject.operation = 'CUSTOMIZE';
			bagObject.doCustomize = false;
			bagObject.customName = '';
			bagObject.customNumber ='';
			var that = this;
			$.ajax({
				url: this.url,
				type: 'PUT',
				data: JSON.stringify(bagObject),
				contentType: 'application/json',
				headers : {
					Accept : "application/json; charset=utf-8",
        			'X-CSRF-TOKEN': Myntra.Data.token,
        			'Accept-Language': 'en-US,en;q=0.8'
				},				

				beforeSend: function () {
					$('.widget-overlay').removeClass('hide');
				},

				success: function(success) {
					that.reset(that.parse(success));

				},

				error: function (error) {
			 		_gaq.push(['_trackEvent', 'bag_collection', 'error: ' + error]);
				},

				complete: function (){
					$('.widget-overlay').addClass('hide');
				}

			});
		}


	});

	BagCollection.prototype.setUrl = function(url) {
		this.url = url;
	};

	return new BagCollection();
});
