define(["underscore","backbone"],function (_, Backbone) {


	var CouponCollection = Backbone.Collection.extend({

		url : function () {
			return this.url;
		},

		parse: function (response) {
			var responseData = response.data;
			return responseData;
		},

		sync: function (method, model, options) {
			options.dataType = "json";
			options.headers = {
				'Accept-Language': 'en-US,en;q=0.8'
			};
			$.ajaxSetup({
				cache: true
			});
			return Backbone.sync(method, model, options);
		},

		fetch: function (options) {
			options.reset = true;
			if (Myntra.Data.fetchCoupons === undefined || Myntra.Data.fetchCoupons === true) {
			    return Backbone.Collection.prototype.fetch.call(this, options);
			} else {
			    options.success({}, {}, {});
			}
		},

		initialize: function () {

		}
		
	});

	CouponCollection.prototype.setUrl = function(url) {
		this.url = url;
	};

	return new CouponCollection();
});
