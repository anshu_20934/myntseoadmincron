define(['backbone',
		'controller',
		'../collections/CouponCollection',
		'../../common/lightbox/views/lightbox',
		'text!../templates/applyCouponLightbox.html'],
		function (Backbone, Controller, CouponCollection, Lightbox, applyCouponLightboxTemplate) {

		var ApplyCouponLightboxView = Lightbox.extend({

			el: 'body',

			applyCouponLightboxTemplate: _.template(applyCouponLightboxTemplate),

			//coupons: null,

			events: function () {
				return  _.extend({}, Lightbox.prototype.events,{
					'click .add-coupon-form-box .coupon-list .row': 'selectCoupon',
					'click .add-coupon-form-box .row .coupon-details-btn': 'showCouponDetails',
					'click .add-coupon-form-box .btn-apply': 'applyCoupon', 
					'couponAppliedStatus': 'showCouponAppliedStatus',
					'keyup .add-coupon-form-box .row.enter-coupon-row .enter-coupon-wrapper': 'changeCouponCode',
					'click .add-coupon-form-box .row.enter-coupon-row .enter-coupon-wrapper': 'changeCouponCode',
					'blur .add-coupon-form-box .row.enter-coupon-row .enter-coupon-wrapper': 'addPlaceHolder',
					'click .add-coupon-form-box .row.enter-coupon-row .clear-coupon': 'clearCouponTextfield'
				});
			},

			initialize: function (options) {

				_.bindAll(this,"render","errorHandler");
					
				CouponCollection.setUrl(options.couponCollectionUrl);

				this.selectedCoupon = options.selectedCoupon;
				this.couponApplicabilityMessage = options.couponApplicabilityMessage;
				CouponCollection.fetch({
				    success: this.render,
				    error: this.errorHandler
				});
			},

			render: function (collection, response, options) {
				this.beforeShow();
				var model = {};
				var resultHTML = "";
                var that = this;
                var taggedAndOpenCoupons;
                if (_.isEmpty(collection)) {
                    taggedAndOpenCoupons = [];
                } else {
                    taggedAndOpenCoupons = _.filter(collection.toJSON() , function(coupon) {
                                                return coupon.displayType == "TAGGED" ||
                                                        coupon.displayType == "OPEN";
                                            });
                }

                model.coupons = taggedAndOpenCoupons;
				model.couponApplicabilityMessage = this.couponApplicabilityMessage;
				
				if(!$('#lb-add-coupon').length) {
					resultHTML = this.applyCouponLightboxTemplate(model||{});
					this.$el.append(resultHTML);
				} else {
					this.$el.find('#lightbox-shim').add('#lb-add-coupon').remove();
					this.events = null;
					this.$el.append(this.applyCouponLightboxTemplate(model||{}));
				}

				this.$el.find('#lightbox-shim').add('#lb-add-coupon').show();

				if(!model.coupons.length) {
					this.$el.find('.addr-form').css('border','none');
				}

                if(_.where(model.coupons, {'applicable': true}).length == 0) {
					$('.add-coupon-form-box .btn-apply').attr('disabled','disabled');
				}

                var promoCoupons = [];
                if (!_.isEmpty(collection) && 
                    Myntra.Data.promoCouponsVariant !== undefined &&
                    Myntra.Data.promoCouponsVariant == "validate") {
                    promoCoupons = _.chain(collection.toJSON())
                                    .filter(function(coupon) {
                                        return  coupon.displayType == "PROMO" && 
                                                coupon.applicable == true;
                                        })
                                    .map(function(coupon) {return coupon.coupon;})
                                    .value();
                } else {
                    promoCoupons = Myntra.Data.promoCoupons.split(",");
                }
                if(promoCoupons.length > 0) { 
                    this.$el.find(".row input[name='coupon-code']").autocomplete({
			            minLength: 0,
			            source: promoCoupons,
                        select: function() {
                            $('.add-coupon-form-box .btn-apply').removeAttr('disabled');
                        }
			        });

                    this.$el.find(".row input[name='coupon-code']").on('focus',function(event){
                        that.$el.find('.mk-placeholder-text').addClass('hide');
                        $(this).autocomplete('search');
                    });
                }
            
				this.$el.find('.addr-form .coupon-list').jScrollPane({autoReinitialise: true});
				this.afterRender(model);
			},

			afterRender: function (model) {
				if(this.selectedCoupon) {

					//check whether the coupon is present in the list
					if(_.where(model.coupons, {'coupon': this.selectedCoupon}).length) {
						this.$el.find('.coupon-list .row .jqTransformChecked').removeClass('jqTransformChecked');
						var selectedCouponRow = this.$el.find(".coupon-list .row[data-couponcode='"+this.selectedCoupon+"']");
						
						selectedCouponRow.find('.jqTransformRadio').addClass('jqTransformChecked');
						//selectedCouponRow.find('.coupon-details-btn').trigger('click');
						this.$el.find('.addr-form .coupon-list').data('jsp').scrollTo(0,selectedCouponRow.data('index') * 40);
					}

					//coupon not found in the list, show it in the input text box
					else {
						var couponTextRow = this.$el.find('.add-coupon-form-box .row.enter-coupon-row');
						this.$el.find('.coupon-list .row .jqTransformChecked').removeClass('jqTransformChecked');
						couponTextRow.find('.enter-coupon-wrapper').trigger('click');
						couponTextRow.find('.complete-icon').css('visibility','visible');
						couponTextRow.find('.clear-coupon').css('visibility','visible');
						this.$el.find(".row input[name='coupon-code']").val(this.selectedCoupon);
						this.$el.find('.row.coupon-info.text-coupon').show();
					}
				}
				
			},

			selectCoupon: function (event) {
				var currentRow = $(event.currentTarget);
				var couponTextRow = this.$el.find('.add-coupon-form-box .row.enter-coupon-row');
				this.$el.find('.add-coupon-form-box .row.enter-coupon-row .enter-coupon-wrapper input[name=coupon-code]').val('');
				this.$el.find('.add-coupon-form-box .row.enter-coupon-row .enter-coupon-wrapper .mk-placeholder-text').removeClass('hide');				
				if(!currentRow.find('.jqTransformDisabled').length) {
					$('.add-coupon-form-box .btn-apply').removeAttr('disabled');
					this.$el.find('.add-coupon-form-box .row.enter-coupon-row input[name="coupon-code"]').val('');
					this.$el.find('.err').addClass('hide');
					currentRow.parent().find('.row .jqTransformChecked').removeClass('jqTransformChecked');
					currentRow.find('.jqTransformRadio').addClass('jqTransformChecked');
				}
				this.$el.find('.row.coupon-info.text-coupon').slideUp();
				couponTextRow.find('.complete-icon').css('visibility','hidden');
				couponTextRow.find('.clear-coupon').css('visibility','hidden');		
			},

			showCouponDetails: function (event) {
				var currentRow = $(event.currentTarget).parents('.row');
				if (currentRow.hasClass('select-coupon')) {
 				    currentRow.next().slideToggle();
				}
 				else {
  				    currentRow = $(event.currentTarget).parentsUntil('.coupon-list');
  				    currentRow.find('.select-coupon')
                                       .next('.coupon-info')
                                       .slideUp()
                                       .end()
                                       .removeClass('select-coupon');

  				    currentRow.next('.coupon-info').slideDown().end().addClass('select-coupon');
  				    this.$el.find('.row.coupon-info.text-coupon').slideUp();
  				}
			},

			applyCoupon: function (event) {
				var nocouponError = $('.add-coupon-form-box .no-coupon-selected');
				var lightboxoverlay = $('.add-coupon-form-box .lightbox-overlay');
				var enteredCouponCode = $('.add-coupon-form-box .row.enter-coupon-row').find("input[name='coupon-code']").val();
				var couponcode = $('.add-coupon-form-box .row').has('.jqTransformChecked').data('couponcode');
				if(! (enteredCouponCode || couponcode)) {
					nocouponError.removeClass('hide');
				} else {
					nocouponError.addClass('hide');
					Controller.publish('applyCoupon', enteredCouponCode || couponcode);
					lightboxoverlay.removeClass('hide');
				}				
			},

			showCouponAppliedStatus: function (event, appliedCoupon) {
				var successContainer = $('.add-coupon-form-box .coupon-applied-successfully').addClass('hide');
				var errorContainer = $('.add-coupon-form-box .coupon-application-failed').addClass('hide');
				var networkErrorContainer = $('.add-coupon-form-box .network-error').addClass('hide');
				var lightboxoverlay = $('.add-coupon-form-box .lightbox-overlay'), 
                    couponApplicableMessage,
                    promoCouponsText,
                    showPromoMessage = true,
                    that = this;
				lightboxoverlay.addClass('hide');

                if(appliedCoupon.applicationState == false) {
                    networkErrorContainer.removeClass('hide');
                    _gaq.push(['_trackEvent', 'Coupons', appliedCoupon.coupon.toUpperCase(), 'error: network']);
                } else if(appliedCoupon.applicationState.state == "ERROR") {
                    _gaq.push(['_trackEvent', 'Coupons', appliedCoupon.coupon.toUpperCase(), 'error: '+appliedCoupon.applicationState.message]);
                    couponApplicableMessage = '<ul>';
                    if(appliedCoupon.couponApplicabilityMessage !== undefined && appliedCoupon.couponApplicabilityMessage[0]) {
                        $.each(appliedCoupon.couponApplicabilityMessage,function(index,message){
                            if(message !== null) {
                                couponApplicableMessage += '<li>'+ message + '</li>';
                            }
                        });
                       
                    } else {
                        if(Myntra.Data.promoCouponsText !== '') {
                            promoCouponsText = Myntra.Data.promoCouponsText.split(",");
                            $.each(promoCouponsText,function(index,message){
                                couponApplicableMessage += '<li class="promo-coupon-text">'+ message + '</li>';
                            });
                        } else {
                            showPromoMessage = false;
                        }
                    }
                    couponApplicableMessage += '</ul>';
                    this.$el.find('.row.coupon-info.text-coupon > ul').remove();
                    if(showPromoMessage) {
                        this.$el.find('.row.coupon-info.text-coupon').append(couponApplicableMessage).show(); 
                    }
				    errorContainer.removeClass('hide');
                   
				} else if (appliedCoupon.applicationState.state == "SUCCESS") {
                    _gaq.push(['_trackEvent', 'Coupons', appliedCoupon.coupon.toUpperCase(), 'success']);
					$('.add-coupon-form-box .addr-form').addClass('hide');
					$('.add-coupon-form-box .btn').addClass('hide');
					successContainer.removeClass('hide');
					var that = this;
					setTimeout(function () {
						that.hideLightbox();
					},2000);
				}
			},

			changeCouponCode: function (event) {
				event.stopPropagation();

				//collapse the coupon info box
				this.$el.find('.coupon-list .coupon-info')
							.slideUp()
							.prev('.select-coupon')
							.removeClass('select-coupon');

				$(event.currentTarget).parentsUntil('.bd').find('.row .jqTransformChecked').removeClass('jqTransformChecked');
				var applyBtn = $('.add-coupon-form-box .btn-apply');
				$(event.currentTarget).find('.mk-placeholder-text').addClass('hide');
				$(event.currentTarget).find('input[name=coupon-code]').trigger('focus');
				this.$el.find('.complete-icon').css('visibility','hidden');
				this.$el.find('.row.coupon-info.text-coupon').slideUp();
				if($(event.currentTarget).find('input[name=coupon-code]').val().length > 0 ) {
					applyBtn.removeAttr('disabled');
					$(event.currentTarget).next('.clear-coupon').css('visibility','visible');					
				} else {
					applyBtn.attr('disabled','disabled');
					$(event.currentTarget).next('.clear-coupon').css('visibility','hidden');										
				}
				var keyCode = (window.event)? event.which : event.keyCode;
				if(keyCode == 13) {
					applyBtn.trigger('click');
					$(event.currentTarget).find('input[name=coupon-code]').trigger('focus');
				}
				return false;
			},

			addPlaceHolder: function (event) {
				if($(event.currentTarget).find('input[name=coupon-code]').val().length == 0 ) 
					$(event.currentTarget).find('.mk-placeholder-text').removeClass('hide');				
			},

			clearCouponTextfield: function(event) {
				event.stopPropagation();
				var couponTextRow = this.$el.find('.add-coupon-form-box .row.enter-coupon-row');
				couponTextRow.find('.complete-icon').css('visibility','hidden');
				couponTextRow.find('.enter-coupon-wrapper input[name=coupon-code]').val('');
				couponTextRow.find('.mk-placeholder-text').removeClass('hide');
				couponTextRow.find('.clear-coupon').css('visibility','hidden');
				this.$el.find('.row.coupon-info.text-coupon').slideUp();
				this.$el.find('.add-coupon-form-box .btn-apply').attr('disabled','disabled');
			},

			beforeShow: function () {
				var scrollTop = $(window).scrollTop();
            	$('html,body,.mk-body').css({'overflow':'hidden', 'margin-right':'5.5px'});
            	$(window).scrollTop(scrollTop);
			},

			beforeHide: function () {
				var scrollTop = $(window).scrollTop();            	
				$('html,body,.mk-body').css({'overflow':'auto','margin-right':'0'});
                $(window).scrollTop(scrollTop);
			},


			errorHandler: function (collection, xhr, options) {
				_gaq.push(['_trackEvent', 'apply_coupon_lightbox_view', 'error: ' + collection + " ; " + xhr + " ; " + options]);
			},

			hideLightbox: function () {
				this.beforeHide();
				this.undelegateEvents();
				this.$el.find('#lightbox-shim').add('#lb-add-coupon').remove();
			}

		});

		return ApplyCouponLightboxView;
});
