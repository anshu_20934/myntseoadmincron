define(['backbone',
		'controller',
		'../models/FreeGiftModel',
		'../../common/lightbox/views/lightbox',
		'text!../templates/freegiftLightbox.html'],
		function (Backbone, Controller, FreeGiftModel, Lightbox, freegiftLightboxTemplate) {

		var FreeGiftLightboxView = Lightbox.extend({

			el: 'body',

			freegiftLightboxTemplate: _.template(freegiftLightboxTemplate),

			events: function () {
				return  _.extend({}, Lightbox.prototype.events,{

					'mouseover .freegift-form-box .mk-product': function(event) {
						$(event.currentTarget).find('.add-to-bag-btn').removeClass('hide');
					},

					'mouseout .freegift-form-box .mk-product': function(event) {
						$(event.currentTarget).find('.add-to-bag-btn').addClass('hide');
					},	

					//'click .freegift-form-box .add-to-bag-btn': 'changeFreeGift',
					'click .freegift-form-box .mk-product': 'changeFreeGift',
					'freeGiftChangedStatus' : 'freeGiftChangedStatusHandler'			
				});
			},

			initialize: function (options) {

				_.bindAll(this,"render","errorHandler");
									
				FreeGiftModel.setUrl(options.freeGiftModelUrl);

				FreeGiftModel.fetch({
					success: this.render,
					error: this.errorHandler
				});
			},

			render: function (collection, response, options) {
				var model=collection.toJSON();
				model = _.toArray(model);
				model = _.sortBy(model, function (current) { return !current.isSelected;});
				if(!$('#lb-freegift').length) {
					resultHTML = this.freegiftLightboxTemplate(model||{});
					this.$el.append(resultHTML);
				} else {
					this.$el.find('#lightbox-shim').add('#lb-freegift').remove();
					this.$el.append(this.freegiftLightboxTemplate(model||{}));
				}
				this.beforeShow();
				this.$el.find('#lightbox-shim').add('#lb-freegift').show();
				$('.freegift-form-box .mk-search-grid').jScrollPane();
				(function () {
					if($('.mk-prod-img img').prop('complete'))
						$('.freegift-form-box .mk-search-grid').jScrollPane();
					else
						setTimeout(arguments.callee,1000)
				})();
					
			},

			changeFreeGift: function (event) {
				var newSkuId = $(event.currentTarget).data('skuid');
				var itemId = $('.freegift-form-box .in-the-bag-text').data('itemid');
				var oldStyleId = $('.freegift-form-box .in-the-bag-text').data('styleid');
				var newStyleId = $(event.currentTarget).data('styleid');
				var data = {'itemId': itemId, 'skuId': newSkuId};
				//$(event).stopPropagation();
				Controller.publish('changeFreeGift', data);
				_gaq.push(['_trackEvent', 'freegift_lightbox_change_gift', 'changing from: ' + oldStyleId + " ; to: " + newStyleId]);



			},


			freeGiftChangedStatusHandler: function (event, data) {
				var successContainer = $('.freegift-form-box .freegift-applied-successfully').addClass('hide');
				var errorContainer = $('.freegift-form-box .freegift-application-failed').addClass('hide');
				var lightboxoverlay = $('.freegift-form-box .lightbox-overlay');
				lightboxoverlay.addClass('hide');
				if(data == "ERROR") {
					errorContainer.removeClass('hide');
				} else if (data == "SUCCESS") {
					$('.freegift-form-box .bd').addClass('hide');
					$('.freegift-form-box .btn').addClass('hide');
					successContainer.removeClass('hide');
					var that = this;
					setTimeout(function () {
						that.hideLightbox();
					},2000);
				}
			},

			errorHandler: function (collection, xhr, options) {
            	_gaq.push(['_trackEvent', 'freegift_lightbox_view', 'error: ' + collection + " ; " + xhr + " ; " + options]);
			},

			beforeShow: function () {
				var scrollTop = $(window).scrollTop();
            	$('html,body,.mk-body').css({'overflow':'hidden', 'margin-right':'5.5px'});
            	$(window).scrollTop(scrollTop);
			},

			beforeHide: function () {
				var scrollTop = $(window).scrollTop();            	
				$('html,body,.mk-body').css({'overflow':'auto','margin-right':'0'});
                $(window).scrollTop(scrollTop);
			},

			hideLightbox: function () {
				this.beforeHide();
				this.undelegateEvents();
				this.$el.find('#lightbox-shim').add('#lb-freegift').remove();
			}

		});

		return FreeGiftLightboxView;
});
