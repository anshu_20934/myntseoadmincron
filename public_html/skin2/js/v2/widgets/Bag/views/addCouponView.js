define(['backbone',
		'controller',
		'./applyCouponLightboxView',
		'text!widgets/Bag/templates/addCoupons.html'],
		function (Backbone, Controller, ApplyCouponLightboxView, addCouponsTemplate) {

			var AddCouponView = Backbone.View.extend( {
				
				addCouponsTemplate: _.template(addCouponsTemplate),

				ApplyCouponView: null,

				coupons: null,

				events: {
					'click .edit-coupon, .apply-coupon': function (event) {
						var view;
						if(this.cart.appliedCoupons.length && this.cart.appliedCoupons[0].applicationState.state=="SUCCESS") { 
							view = new ApplyCouponLightboxView({'couponCollectionUrl': this.couponCollectionUrl, 
																'selectedCoupon': this.cart.appliedCoupons[0].coupon,
																'couponApplicabilityMessage': this.cart.appliedCoupons[0].couponApplicabilityMessage});
						} else {
							view = new ApplyCouponLightboxView({'couponCollectionUrl': this.couponCollectionUrl, 
																'selectedCoupon': null,
																'couponApplicabilityMessage': null});
						}
						Controller.unsubscribe('couponAppliedStatus');
						Controller.subscribe(view,'couponAppliedStatus');
						Myntra.Data.showApplyCouponLB=false;
					},

					'click .delete-coupon': function (event) {
						Controller.publish('deleteCoupon', {'coupon': this.cart.appliedCoupons[0].coupon});
						_gaq.push(['_trackEvent', 'Coupons', this.cart.appliedCoupons[0].coupon.toUpperCase(), 'remove']);
					},

					'click .login-btn': function (event) {
						Controller.showLogin(function() {
							Controller.init(true);
							Myntra.Data.showApplyCouponLB=true;
						});
					},
					'mouseover .edit-coupon,.delete-coupon': 'mouseoverHandler',

					//on mouseout remove the tooltip
					'mouseout .edit-coupon,.delete-coupon': 'mouseoutHandler'
				},

				initialize: function (options) {
					
					this.cart = options.cart;
					this.couponCollectionUrl = options.couponCollectionUrl;
					this.render();
				},

				render: function (options) {
					var that = this;
					var resultHTML= "";
					var data = this.cart;
					resultHTML = that.addCouponsTemplate(this.cart);					
					that.$el.html(resultHTML);
				},

				mouseoverHandler: function (event) {
					$(event.currentTarget).find('.myntra-tooltip').show();
				},

				mouseoutHandler: function (event) {
					$(event.currentTarget).find('.myntra-tooltip').hide();
				}

				
				
			});

		return AddCouponView;			
});
