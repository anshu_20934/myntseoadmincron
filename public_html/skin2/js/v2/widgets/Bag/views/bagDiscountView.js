define(['backbone',
		'text!../templates/bagDiscount.html'],
		function (Backbone, bagDiscountTemplate) {

			var BagDiscountView = Backbone.View.extend( {
				
				bagDiscountTemplate: _.template(bagDiscountTemplate),

				initialize: function (options) {
					this.cart = options.cart;
					this.render();
				},

				render: function () {
					var that = this;
					var resultHTML= "";
					var data = this.cart;
					resultHTML = that.bagDiscountTemplate(this.cart);					
					that.$el.html(resultHTML);
				}
				
			});

		return BagDiscountView;			
});