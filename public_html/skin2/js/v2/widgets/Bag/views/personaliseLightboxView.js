define(['backbone',
		'controller',
		'../../common/lightbox/views/lightbox',
		'text!../templates/personaliseLightbox.html'],
		function (Backbone, Controller, Lightbox, personaliseLightboxTemplate) {

		var PersonaliseLightboxView = Lightbox.extend({

			el: 'body',

			personaliseLightboxTemplate: _.template(personaliseLightboxTemplate),

			personaliseObject: {},

			events: function () {
				return  _.extend({}, Lightbox.prototype.events,{

					'click .btn-proceed': function (event) {
												
						var pname = this.$el.find("#personalise-name");
						var pnumber = this.$el.find("#personalise-number");
						

						if(!pname.val() || pname.val().length > 7 || !/^([a-zA-Z])+$/.test(pname.val())){
							pname.next().css({'visibility':'visible'});
						} else {
							pname.next().css({'visibility':'hidden'});
						}

						//if(!pnumber.val() !(/^([0-9])+$/.test(pnumber.val())) && pnumber.val().length > 2) {\
						if(!pnumber.val() || pnumber.val().length > 2 || !/^([0-9])+$/.test(pnumber.val())){
							pnumber.next().css({'visibility':'visible'});
						} else {
							pnumber.next().css({'visibility':'hidden'});
						}

						if(pname.val() && pnumber.val() && pname.val().length <= 7 && pnumber.val().length <= 2 && /^([a-zA-Z])+$/.test(pname.val()) && /^([0-9])+$/.test(pnumber.val())) {
							this.personaliseObject.customName = pname.val();
							this.personaliseObject.customNumber = pnumber.val();
							this.personaliseObject.itemId = this.options.itemId;
							this.personaliseObject.doCustomize =  this.options.doCustomize;
							Controller.publish('personaliseBagItem', this.personaliseObject, true);	
							this.hideLightbox();						
						}
						
					}

				});

			},

			initialize: function (options) {
				this.render(options)
			},

			render: function (options) {
				if(!$('#lb-personalise').length) {
					resultHTML = this.personaliseLightboxTemplate(options|| {});
					this.$el.append(resultHTML);
				} else {
					this.$el.find('#lightbox-shim').add('#lb-personalise').remove();
					this.events = null;
					this.$el.append(this.personaliseLightboxTemplate(options || {}));
				}
				this.$el.find("#personalise-name").val(options.currentitem[0].customName);
				this.$el.find("#personalise-number").val(options.currentitem[0].customNumber);
				this.beforeShow();
				this.$el.find('#lightbox-shim').add('#lb-personalise').show();
			},

			beforeShow: function () {
				var scrollTop = $(window).scrollTop();
            	$('html,body,.mk-body').css({'overflow':'hidden', 'margin-right':'5.5px'});
            	$(window).scrollTop(scrollTop);
			},

			beforeHide: function () {
				var scrollTop = $(window).scrollTop();            	
				$('html,body,.mk-body').css({'overflow':'auto','margin-right':'0'});
                $(window).scrollTop(scrollTop);
			},			
			
			hideLightbox: function () {
				this.beforeHide();
				this.undelegateEvents();
				this.$el.find('#lightbox-shim').add('#lb-personalise').remove();
			}

		});

		return PersonaliseLightboxView;
});