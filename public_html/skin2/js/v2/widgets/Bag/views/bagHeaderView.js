define(['backbone',
		'controller',
		'text!../templates/bagHeader.html'],
		function (Backbone, Controller, bagHeaderTemplate) {

			var BagHeaderView = Backbone.View.extend({
				bagHeaderTemplate: _.template(bagHeaderTemplate),

				events: {
					'click a.checkout-wishlist-header': function (event) {
						event.preventDefault();
						Controller.publishNavigationEvent('showWishlist');
						//window.location.hash = 'wishlist';
					}
				},

				initialize: function (options) {
					this.cart = options.cart;
					this.wishlistItemCount = options.wishlistItemCount;
					this.render();
				},

				render: function () {
					var that = this;
					var resultHTML= "";
					var options = {};
					options.cart = this.cart||{};
					options.cartItemCount = 0;
					if(options.cart.cartItemEntries.length) {
						options.cartItemCount = _.reduce(options.cart.cartItemEntries, function (sum, cartItemEntry){
							return sum + cartItemEntry.quantity;
						},0);
					}
					options.wishlistItemCount = this.wishlistItemCount;
					resultHTML = that.bagHeaderTemplate(options);
					that.$el.html(resultHTML);
				}
			});

		return BagHeaderView;
});
