define(['backbone',
		'controller',
		'./bagHeaderView',
		'./bagCollectionView',
		'./cartOfferMessageView',
		'./orderTotalSummaryView',
		'./addCouponView',
		'./bagDiscountView',
		'../collections/BagCollection',
		'../collections/MoreShoppingCollection',
		'./gtmCart',
		'./webEngageCart',
		'text!../templates/outOfStockBagHeader.html',
		'text!../templates/orderTotal.html',
		'text!../templates/moreShoppingTemplate.html',
		'text!../templates/emptyBag.html',
		'text!widgets/common/templates/primaryButton.html'],
	
	function (Backbone, Controller, BagHeaderView, BagCollectionView, CartOfferMessageView, OrderTotalSummaryView, AddCouponView, BagDiscountView, BagCollection, MoreShoppingCollection, GTMCart, WebEngageCart, outOfStockBagHeaderTemplate, orderTotalTemplate, moreShoppingTemplate, emptyBagTemplate, placeOrderButtonTemplate) {

	var BagView = Backbone.View.extend({

		outOfStockBagHeaderTemplate: _.template(outOfStockBagHeaderTemplate),

		orderTotalTemplate: _.template(orderTotalTemplate),

		moreShoppingTemplate: _.template(moreShoppingTemplate),

		emptyBagTemplate: _.template(emptyBagTemplate),

		placeOrderButtonTemplate: _.template(placeOrderButtonTemplate),

		cartOfferMessageContainerHTML: "<div class='cart-discount-msg'></div>",

		cartContainerHTML: "<div class='prod-set'></div>",

		orderSummaryContainerHTML: "<div class='order-total-summary'></div>",

		addCouponContainerHTML: "<div class='add-coupon'></div>",

		bagDiscountContainerHTML: "<div class='bag-discount'></div>",

		collection: BagCollection,

		/*containers: {
			
			cartOfferMessageContainerHTML: {
				'class': '.cart-discount-msg',
				'order': 1,
				'method': 'showCartOfferMessage',
				'condition': function () { return collectionData.displayData; }
			},

			cartContainerHTML: {
				'class': '.prod-set',
				'order': 2,
				'method': 'showCartProducts'
			}
		},*/

		events: {

			'click .place-order': function (event) {
			 	_gaq.push(['_trackEvent', 'bag', 'place_order_click']);
			 	event.stopPropagation();
			 	$('.widget-overlay').removeClass('hide');		 	
				this.collection.fetch({
					success: this.checkBeforePlaceOrder, 
					error: this.errorHandler					
				});
			},

			'mouseover .more-shopping': function (event) {
				_gaq.push(['_trackEvent', 'bag', 'more_shopping_click']);
				$(event.currentTarget).find('.myntra-tooltip').toggle();
			},

			'mouseout .more-shopping': function (event) {
				_gaq.push(['_trackEvent', 'bag', 'more_shopping_click']);
				$(event.currentTarget).find('.myntra-tooltip').toggle();
			},

			'wishlistLoadComplete': function(event) {
				this.wishlistItemCount = arguments[1]? arguments[1].wishListItemEntries? _.reduce(arguments[1].wishListItemEntries, function (sum, wishlistItemEntry) { return sum + wishlistItemEntry.quantity },0) : 0 : 0;
				if(this.wishlistItemCount > 0)
					$('.mk-checkout-header .wishlist-item-count').html(this.wishlistItemCount);
			},

			'movetobag': function (event, data) {
				_gaq.push(['_trackEvent', 'bag', 'move_to_bag']);
				this.collection.fetch({
					error: this.errorHandler
				});
			},

			'applyCoupon': function (event, data) {
				_gaq.push(['_trackEvent', 'bag', 'apply_coupon']);
				this.collection.applyCoupon(data);
			},

			'addGiftWrap': function (event, data) {
				console.log('addGiftWrap');
				_gaq.push(['_trackEvent', 'bag', 'add_giftwrap']);
				this.collection.addGiftWrap(data);
			},

			'removeGiftWrap': function (event) {
				_gaq.push(['_trackEvent', 'bag', 'remove_giftwrap']);
				this.collection.removeGiftWrap();
			},

			'deleteCoupon': function (event, data) {
				this.collection.deleteCoupon(data);
			},

			'applyCashback': function (event, data) {
				this.collection.fetch({reset: true});
			},

			'changeFreeGift': function (event, data) {
				this.collection.changeFreeGift(data);
			},

			'freeGiftChangedStatus': function (event, data) {
				this.collection.fetch({
					error: this.errorHandler
				});
			},
			'personaliseBagItem' : function(event,data){
				this.collection.personaliseBagItem(data);
			}
		},

		getCollectionUrl: function () {
			return this.collectionUrl;
		},

		initialize: function (options) {

			_.bindAll(this,"render","errorHandler");

			BagCollection.setUrl(options.collectionUrl);

			this.couponCollectionUrl = options.couponCollectionUrl;

			this.freeGiftModelUrl = options.freeGiftModelUrl;

			this.moreShoppingCollectionUrl = options.moreShoppingCollectionUrl;

			$('.widget-overlay').removeClass('hide');
			BagCollection.fetch({
				//success: this.render,
				error: this.errorHandler			
			});

			GTMCart.bagInit();
			

			this.listenTo(BagCollection,"reset",this.render);

		},

		render: function (collection, response, options) {
			$('.widget-overlay').addClass('hide');
			this.$el.empty();
			var collectionData = collection.toJSON()[0];
			if(collectionData && collectionData.hasOwnProperty('cartItemEntries') && collectionData['cartItemEntries'].length) { 
				//if cart contains some items
				var placeOrderButtonHTML = this.placeOrderButtonTemplate({'buttonLabel': 'PLACE ORDER', 'iconType': 'proceed-icon'});
				var orderTotalHTML = "";
				var resultHTML = "";
				
				if(collectionData.cartItemEntries.length > 1) {
					orderTotalHTML = this.orderTotalTemplate({'placeOrderButton': placeOrderButtonHTML, 'totalPriceWithoutMyntCash': collectionData.totalPriceWithoutMyntCash});
					resultHTML  = orderTotalHTML;
				}
				if(collectionData.displayData) {
					resultHTML += this.cartOfferMessageContainerHTML;
				}

				if(!collectionData.readyForCheckout) {
					resultHTML += this.outOfStockBagHeaderTemplate();
				}

				resultHTML += this.cartContainerHTML;

				if(collectionData.bagDiscount) {
					resultHTML += this.bagDiscountContainerHTML;
				}

				resultHTML += this.orderSummaryContainerHTML +
								this.addCouponContainerHTML;

				orderTotalHTML = this.orderTotalTemplate({'placeOrderButton': placeOrderButtonHTML, 'totalPriceWithoutMyntCash': collectionData.totalPriceWithoutMyntCash, 'className': 'footer'});
				
				resultHTML += orderTotalHTML;

				this.$el.append(resultHTML);

				this.showBagHeader(collectionData, this.wishlistItemCount);

				if(collectionData.displayData) {
					var displayData = collection.toJSON()[0].displayData;
					var slabBasedDisplayData = collection.toJSON()[0].slabBasedDisplayData;
					var cartOfferDisplayData = {
						"displayData": displayData,
						"slabBasedDisplayData": slabBasedDisplayData
					};
					var freeGiftURLBase = null;
					if(typeof displayData.comboQueryString != 'undefined'){
						freeGiftURLBase = location.protocol + "//" + location.hostname + '/' + (location.protocol=='http:' ? '' : 's_');
						freeGiftURLBase += 'getCartComboOverlay.php?json=1&' + displayData.comboQueryString;
					}
					this.showCartOfferMessage(cartOfferDisplayData, freeGiftURLBase);
				}
				//this.showCartProducts(collectionData.cartItemEntry);
				this.showCartProducts(collectionData, collection);
				if(collectionData.bagDiscount) {
					this.showBagDiscountView(collectionData);
				}
				this.showOrderSummary(collectionData);
				this.showAddCouponView(collectionData);
			} else {
				//if cart is empty
				var resultHTML = this.emptyBagTemplate();
				this.$el.append(resultHTML);
				this.showBagHeader(collectionData, this.wishlistItemCount);
			}
			this.afterRender();

		},

		errorHandler: function (collection, xhr, options) {
			_gaq.push(['_trackEvent', 'bag_view', 'error: ' + collection + " ; " + xhr + " ; " + options]);
		},

		showMoreShoppingList: function () {
			var that = this;
			MoreShoppingCollection.setUrl(this.moreShoppingCollectionUrl);

			MoreShoppingCollection.fetch({
				success: function (collection, response, options) {
					var modelData = {};
					modelData.moreShoppingData = collection.toJSON();
					that.$el.find('.order-total .more-shopping').append(that.moreShoppingTemplate(modelData));

				},

				error: function (collection, xhr, options) {
					_gaq.push(['_trackEvent', 'bag_view', 'error: ' + collection + " ; " + xhr + " ; " + options]);
				}				
			});
		},

		showBagHeader: function (cart, wishlistItemCount) {
			if(this.bagHeaderView)
				this.bagHeaderView.undelegateEvents();
			
			this.bagHeaderView = new BagHeaderView({
				el: '.cart-section .mk-checkout-header-container',
				cart: cart,
				wishlistItemCount: wishlistItemCount
			});
		},

		showCartOfferMessage: function(cartOfferMessages, freeGiftURL) {
			if(this.cartOfferMessageView)
				this.cartOfferMessageView.undelegateEvents();
			
			this.cartOfferMessageView = new CartOfferMessageView({
				el: '.cart-discount-msg',
				freeGiftModelUrl: freeGiftURL,
				cartOfferMessages: cartOfferMessages
			});
		},

		showCartProducts: function (cart, collection) {
			if(this.bagCollectionView)
				this.bagCollectionView.undelegateEvents();
			
			this.bagCollectionView = new BagCollectionView({
				el: '.prod-set',
				collection: collection,
				freeGiftModelUrl: this.freeGiftModelUrl,
				cart: cart
			});
		},

		showOrderSummary: function (cart) {
			if(this.orderTotalSummaryView)
				this.orderTotalSummaryView.undelegateEvents();
			
			this.orderTotalSummaryView = new OrderTotalSummaryView({
				el: '.order-total-summary',
				cart: cart,
			});
		},

		showAddCouponView: function (cart) {
			if(this.addCouponView)
				this.addCouponView.undelegateEvents();
			
			this.addCouponView = new AddCouponView({
				el: '.add-coupon',
				couponCollectionUrl: this.couponCollectionUrl,
				cart: cart
			});

			if(Myntra.Data.showApplyCouponLB) {
				this.addCouponView.$el.find('.apply-coupon').trigger('click');
			}

			Myntra.Data.showApplyCouponLB = false;
		},

		showBagDiscountView: function (cart) {
			if(this.bagDiscountView)
				this.bagDiscountView.undelegateEvents();
			
			this.bagDiscountView = new BagDiscountView({
				el: '.bag-discount',
				cart: cart
			});
		},

		afterRender: function () {
			this.showMoreShoppingList();
			var cartCollection = this.collection.toJSON().length? this.collection.toJSON()[0]: [];
			//Myntra.WebEngage.initNotifications(cartCollection);
			WebEngageCart.bagLoadCompleteAction(cartCollection);
			GTMCart.bagLoadComplete(cartCollection);
			Controller.publish('bagLoadComplete', cartCollection);
		},

		checkBeforePlaceOrder: function (collection, response, options) {
			$('.widget-overlay').addClass('hide');
			var readyForCheckout = collection.toJSON()[0].readyForCheckout;
			
			if(!readyForCheckout) {
				//if the cart is not ready for checkout then show the row where there is an issue with product
				var target = $(".prod-set .prod-item[data-productNotAvailable=true]").first();
				
				//scroll to the element which is not available
				if(target.length) {
					$('html, body').animate({
	         			scrollTop: $(target).offset().top
	     			}, 500);
				}			
			} else {
				Controller.publishNavigationEvent('bagPlaceOrder', BagCollection);
			}
		}
		
	});

	return function (options) {
		var freeGiftURLBase = location.protocol + "//" + location.hostname + '/' + (location.protocol=='http:' ? '' : 's_');
		freeGiftURLBase += 'getCartComboOverlay.php';
		var view = new BagView({
			el: options.el,
			collectionUrl: options.collectionUrl,
			couponCollectionUrl: options.couponCollectionUrl,
			freeGiftModelUrl: freeGiftURLBase,
			moreShoppingCollectionUrl: options.moreShoppingCollectionUrl,
		});

		Controller.subscribe(view,'wishlistLoadComplete');
		Controller.subscribe(view,'movetobag');
		Controller.subscribe(view,'applyCoupon');
		Controller.subscribe(view,'addGiftWrap');
		Controller.subscribe(view,'removeGiftWrap');
		Controller.subscribe(view,'deleteCoupon');
		Controller.subscribe(view,'changeFreeGift');
		Controller.subscribe(view,'applyCashback');
		Controller.subscribe(view,'freeGiftChangedStatus');
		Controller.subscribe(view,'personaliseBagItem');
		
        var trackUrl = location.protocol === 'https:' ? https_loc + '/components/s_tracker.php' : http_loc + '/components/tracker.php';
        trackUrl += '?page=cart';
        trackUrl += '&productCountinCart=' + Myntra.Utils.getQueryParam('productCountinCart');
        trackUrl += '&productAdded=' + Myntra.Utils.getQueryParam('productAdded');
        $.get(trackUrl).done(function(data) { $(data).appendTo('body'); });
        
		return view;
	};

});
