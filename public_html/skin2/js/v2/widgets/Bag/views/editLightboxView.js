define(['backbone',
		'../../common/lightbox/views/lightbox',
		'text!../templates/editLightboxTemplate.html',
		'text!../templates/editLightboxQtydropdown.html'],
		function (Backbone, Lightbox, editLightboxTemplate, editLightboxQtydropdownTemplate) {

			var EditLightboxView = Lightbox.extend({
				
				//el: 'body',

				editLightboxTemplate: _.template(editLightboxTemplate),

				editLightboxQtydropdownTemplate: _.template(editLightboxQtydropdownTemplate),

				events: function() {
					//extend the Lightbox events - close and esc key
					return _.extend({}, Lightbox.prototype.events, {
						
						//change in size
						'click .size-btn': function (event) {
							var newSizeBtn = $(event.currentTarget);
							var availableQuantity = parseInt(newSizeBtn.data('count'),10);
							var selectedQuantity = parseInt($('.prod-name .mk-custom-drop-down.qty .qty-section .sel-qty').val()||this.cartItem.selectedSize.availableQuantity, 10);
							var errorMessage = $('.prod-name .mk-custom-drop-down.qty .qty-error-message');
							var errorWarningMessage = $('.prod-name .mk-custom-drop-down.qty .qty-warning-message');
							var qtySection = $('.prod-name .mk-custom-drop-down.qty .qty-section');
							var saveBtn = $('.edit-product .action-buttons .btn-save');

							newSizeBtn.siblings('.size-btn').removeClass('selected');
							newSizeBtn.addClass('selected');
							newSizeBtn.siblings('.mk-size').val(newSizeBtn.val());

							if(newSizeBtn.hasClass('unavailable')) {

								//will change the background color of the button to red
								qtySection.addClass('mk-hide');
								errorMessage.removeClass('mk-hide');
								errorWarningMessage.addClass('mk-hide');
								saveBtn.attr('disabled','disabled');

							}
							else {

								//selecting a new size
								errorMessage.addClass('mk-hide');
								errorWarningMessage.addClass('mk-hide');
								qtySection.removeClass('mk-hide');
								saveBtn.removeAttr('disabled');
								var resultDropdown ="";
								
								if(availableQuantity >= 10) {

									resultDropdown = this.editLightboxQtydropdownTemplate({'numberOfOptions': availableQuantity, 'selectedQuantity': selectedQuantity });
									qtySection.html(resultDropdown);
									errorWarningMessage.addClass('mk-hide');

								} else if(availableQuantity > selectedQuantity) {

									resultDropdown = this.editLightboxQtydropdownTemplate({'numberOfOptions': availableQuantity, 'selectedQuantity': selectedQuantity });
									qtySection.html(resultDropdown);
									errorWarningMessage.html("ONLY " + availableQuantity + " " + (availableQuantity>1? 'UNITS': 'UNIT') + " IN STOCK").removeClass('mk-hide');
								
								} else if(availableQuantity <= selectedQuantity) {

									resultDropdown = this.editLightboxQtydropdownTemplate({'numberOfOptions': availableQuantity, 'selectedQuantity': selectedQuantity });
									qtySection.html(resultDropdown);
									errorWarningMessage.html("ONLY " + availableQuantity + " " + (availableQuantity>1? 'UNITS': 'UNIT') + " IN STOCK").removeClass('mk-hide');
								}

							}
						},

						'click .btn-save': function (event) {
							if($(event.currentTarget).hasClass('free-item')){
								var newSkuId = this.$el.find('.mk-size').val();
								var itemHTML = this.$el.find('.row.edit-product')[0];
								var itemId = $(itemHTML).data('itemid');
								var oldStyleId = this.$el.find('input[name=styleid]').val()
								var newStyleId = this.$el.find('input[name=styleid]').val()
								var data = {'itemId': itemId, 'skuId': newSkuId};
								//$(event).stopPropagation();
								this.collection.changeFreeGift(data);
								this.hideLightbox();

							}else {
								var bagObject = {};
								bagObject.skuId = this.$el.find('.mk-size').val();
								bagObject.quantity = this.$el.find('.sel-qty').length ? parseInt(this.$el.find('.sel-qty').val(), 10) : 1;

								var itemHTML = this.$el.find('.row.edit-product')[0];
								bagObject.itemId = $(itemHTML).data('itemid');
								this.collection.updateBagItem(bagObject);
								this.hideLightbox();
							}
						},

						'keydown .lightbox': function (event) {
							if(event.keyCode == 27) {
								this.hideLightbox();
							}
						}
					});
				},

				initialize: function (options) {
					this.cartItem = options.currentitem[0];
					this.offsetObject = options.offsetObject;
					this.render(this.cartItem);
				},

				render: function (options) {
					//options.offsetObject = this.offsetObject;
					/*if(!$('#lb-edit-cart-item').length) {
						//$('body').append(this.editLightboxTemplate(options||{}));
						this.$el.append(this.editLightboxTemplate(options||{}));
					} else {
					
						//$('body').find('#lightbox-shim').add('#lb-edit-cart-item').remove();
						this.events = null;
						$('body').append(this.editLightboxTemplate(options||{}));
					}*/
					this.$el.append(this.editLightboxTemplate(options||{}));
					this.$el.find('.lightbox').slideDown();
				},

				hideLightbox: function () {					
					this.$el.find('.lightbox').slideUp("normal", function () { 
						$(this).remove();
					});
				}
			});

		return EditLightboxView;

});
