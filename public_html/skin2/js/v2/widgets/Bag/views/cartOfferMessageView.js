define(['backbone',
		'./freegiftLightboxView',
		'text!../templates/cartOfferMessage.html'],
		function (Backbone, FreeGiftLightboxView, cartOfferMessageTemplate) {

			var CartOfferMessageView = Backbone.View.extend( {
				
				cartOfferMessageTemplate: _.template(cartOfferMessageTemplate),

				events: {
					'click .change-free-gift': function (event) {
						var view = new FreeGiftLightboxView({'freeGiftModelUrl': this.freeGiftModelUrl});
					},

					'mouseover .cart-discount-exclusion-tt': function (event) {
						$(event.currentTarget).find('.myntra-tooltip').show();
					},

					'mouseout .cart-discount-exclusion-tt': function (event) {
						$(event.currentTarget).find('.myntra-tooltip').hide();
					}

				},	

				initialize: function (options) {
					this.cartOfferMessages = options.cartOfferMessages;
					this.freeGiftModelUrl = options.freeGiftModelUrl;
					this.render();
				},

				render: function () {
					var that = this;
					var resultHTML= ""; 
					if(_.isArray(this.displayData)) {
						_.each(this.cartItems, function(element, index, list) {
							resultHTML += that.cartOfferMessageTemplate(element);
						});
					} else {
						resultHTML = that.cartOfferMessageTemplate({'cartOfferMessages': this.cartOfferMessages});
					}
					
					that.$el.html(resultHTML);
				}

				
			});

		return CartOfferMessageView;
			
});