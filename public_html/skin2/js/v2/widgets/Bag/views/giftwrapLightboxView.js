define(['backbone',
		'controller',
		'../../common/lightbox/views/lightbox',
		'text!../templates/giftwrapLightbox.html'],
		function (Backbone, Controller, Lightbox, giftwrapLightboxTemplate) {

		var GiftWrapLightboxView = Lightbox.extend({

			el: 'body',

			giftwrapLightboxTemplate: _.template(giftwrapLightboxTemplate),

			//coupons: null,
			giftwrapObject: {},

			events: function () {
				return  _.extend({}, Lightbox.prototype.events,{

					'click .btn-proceed': function (event) {
						
						var sender = this.$el.find("#gift-from");
						var message = this.$el.find("#gift-msg");
						var recipient = this.$el.find("#gift-to");

						if(!sender.val()) {
							sender.next().css({'visibility':'visible'});
						} else {
							sender.next().css({'visibility':'hidden'});
						}

						if(!message.val()) {
							message.next().css({'visibility':'visible'});
						} else {
							message.next().css({'visibility':'hidden'});
						}

						if(!recipient.val()) {
							recipient.next().css({'visibility':'visible'});
						} else {
							recipient.next().css({'visibility':'hidden'});
						}					

						if(sender.val() && message.val() && recipient.val()) {

							this.giftwrapObject.sender = sender.val();
							this.giftwrapObject.message = message.val();
							this.giftwrapObject.recipient = recipient.val();
							Controller.publish('addGiftWrap', this.giftwrapObject, true);
						}						
					},

					'giftmessageAppliedStatus' : function (event, data) {
						if(data) {
							this.$el.find('.giftwrap-container').addClass('hide');
							this.$el.find('.mod').css('min-height', '100px');
							this.$el.find('.success-msg').removeClass('hide');
							var that = this;
							setTimeout(function () { 
								that.hideLightbox();
							}, 2000);
							
						} else {
							this.$el.find('.giftwrap-container').addClass('hide');
							this.$el.find('.error-msg').removeClass('hide');
						}
					}
				});

			},

			initialize: function (options) {
				this.render(options)
			},

			render: function (options) {
				if(!$('#lb-gift-wrap').length) {
					resultHTML = this.giftwrapLightboxTemplate(options|| {});
					this.$el.append(resultHTML);
				} else {
					this.$el.find('#lightbox-shim').add('#lb-gift-wrap').remove();
					this.events = null;
					this.$el.append(this.giftwrapLightboxTemplate(options || {}));
				}
				this.beforeShow();
				this.$el.find('#lightbox-shim').add('#lb-gift-wrap').show();
			},

			beforeShow: function () {
				var scrollTop = $(window).scrollTop();
            	$('html,body,.mk-body').css({'overflow':'hidden', 'margin-right':'5.5px'});
            	$(window).scrollTop(scrollTop);
			},

			beforeHide: function () {
				var scrollTop = $(window).scrollTop();            	
				$('html,body,.mk-body').css({'overflow':'auto','margin-right':'0'});
                $(window).scrollTop(scrollTop);
			},			
			
			hideLightbox: function () {
				this.beforeHide();
				this.undelegateEvents();
				this.$el.find('#lightbox-shim').add('#lb-gift-wrap').remove();
			}

		});

		return GiftWrapLightboxView;
});