define(['backbone',
		'controller',
		'./giftwrapLightboxView',
		'text!../templates/orderTotalSummary.html',
		'text!widgets/common/templates/primaryButton.html'],
		function (Backbone, Controller, GiftWrapLightBoxView, orderTotalSummaryTemplate, placeOrderButtonTemplate) {

			var OrderTotalSummaryView = Backbone.View.extend( {
				orderTotalSummaryTemplate: _.template(orderTotalSummaryTemplate),

				placeOrderButtonTemplate: _.template(placeOrderButtonTemplate),

				events: {
					'click .gift-wrap-checkbox, .edit-giftcard-lbl': function (event) {
						if(!$(event.currentTarget).hasClass('checked')) {
							var view = new GiftWrapLightBoxView(this.giftMessage);
							Controller.subscribe(view, 'giftmessageAppliedStatus');
						} else {
							Controller.publish('removeGiftWrap', null, true);
							this.$el.find('.edit-giftcard').css({'display': 'none'});
						}
					},

					'click .delete-giftcard': function (event) {
						Controller.publish('removeGiftWrap', null, true);
						//this.$el.find('.edit-giftcard').css({'display': 'none'});
					},

					'click .edit-giftcard': function (event) {
						var view = new GiftWrapLightBoxView(this.giftMessage);
						Controller.subscribe(view, 'giftmessageAppliedStatus');
					},

					'mouseover .delivery-charges-span,.vat-charges-span': function (event) {
						$(event.currentTarget).find('.myntra-tooltip').show();
					},

					'mouseout .delivery-charges-span,.vat-charges-span': function (event) {
						$(event.currentTarget).find('.myntra-tooltip').hide();
					},

					'mouseover .edit-giftcard, .delete-giftcard': 'mouseoverHandler',
					'mouseout .edit-giftcard, .delete-giftcard': 'mouseoutHandler'
				},

				initialize: function (options) {
					this.cart = options.cart;
					this.giftMessage = options.cart.giftMessage;
					this.render();
				},

				render: function () {
					var that = this;
					var resultHTML= "";
					var data = this.cart;
					data.placeOrderButton = this.placeOrderButtonTemplate({'buttonLabel': 'PLACE ORDER', 'iconType': 'proceed-icon'});
					resultHTML = that.orderTotalSummaryTemplate(this.cart);
					that.$el.html(resultHTML);
				},

				mouseoverHandler: function (event) {
					$(event.currentTarget).find('.myntra-tooltip').show();
				},

				mouseoutHandler: function (event) {
					$(event.currentTarget).find('.myntra-tooltip').hide();
				}

			});

		return OrderTotalSummaryView;
});
