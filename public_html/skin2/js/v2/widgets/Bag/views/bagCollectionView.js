define(['backbone',
		'controller',
		'./editLightboxView',
		'./freegiftLightboxView',
		'text!../templates/bagProducts.html',
		'text!../templates/productComboMessage.html',
		'./personaliseLightboxView'],
		function (Backbone, Controller, EditLightboxView, FreeGiftLightboxView, bagProductsTemplate, productComboMessageTemplate,PersonaliseLightBoxView) {

		var BagCollectionView = Backbone.View.extend({
				combos: [],

				bagProductsTemplate: _.template(bagProductsTemplate),

				productComboMessageTemplate: _.template(productComboMessageTemplate),

				productComboMessageEnd: "<div class='product-combo' style='height: 10px;'><span class='arrow-up'>&nbsp;</span></div>",

				events: {
					//on mouseover show the tooltips
					'mouseover .edit-item,.move-item,.delete-item, .edit-combo, .coupon-status .coupon-not-applicable-msg': 'mouseoverHandler',

					//on mouseout remove the tooltip
					'mouseout .edit-item, .move-item, .delete-item, .edit-combo, .coupon-status .coupon-not-applicable-msg': 'mouseoutHandler',


					'click .edit-item': function (event) {
						if($(event.currentTarget).hasClass('free-gift-item')) {
							var freeGiftURLBase = location.protocol + "//" + location.hostname + '/' + (location.protocol=='http:' ? '' : 's_');
							freeGiftURLBase += 'getCartComboOverlay.php?json=1&' + $(event.currentTarget).data('freegifturl');
							var view = new FreeGiftLightboxView({'freeGiftModelUrl': freeGiftURLBase });
							//var view = new FreeGiftLightboxView({'freeGiftModelUrl' : '/skin2/js/v2/freegift.json'});
							Controller.subscribe(view, 'freeGiftChangedStatus');
						} else {
							var currentItem = $(event.currentTarget).parentsUntil('.row');
							currentItem = $(currentItem.parent()[0]);
							
							var currentItemId = currentItem.data('itemid');
							currentItemOffset = currentItem.position();

							
							var itemToEdit = _.where(this.cart.cartItemEntries, {'itemId': currentItemId});
							$(currentItem).parent().find('.lightbox').remove();
							new EditLightboxView({
								el: $('#' + $(currentItem).attr('id')),
								collection: this.collection,
								currentitem: itemToEdit
							//	offsetObject: currentItemOffset
							});
						}
					},

					/*'click .edit-item.free-gift-item' : function (event) {
						var view = new FreeGiftLightboxView({'freeGiftModelUrl': this.freeGiftModelUrl});						
					},*/

					'click .delete-item': function (event) {
						var currentItem = $(event.currentTarget).parentsUntil('.row');
						currentItem = $(currentItem.parent()[0]);						
						var currentItemId = currentItem.data('itemid');
						
						this.collection.deleteBagItem({'itemId': currentItemId});
					},

					'click .move-item': function (event) {
						var currentItem = $(event.currentTarget).parentsUntil('.row');
						currentItem = $(currentItem.parent()[0]);	
						var bagObject = {};					
						bagObject.itemId = currentItem.data('itemid');
						var bagItem = _.where(this.cart.cartItemEntries, {'itemId': bagObject.itemId});
						bagObject.skuId = bagItem[0].skuId;
						bagObject.quantity = bagItem[0].quantity;

						function moveItem() {
							var promise = this.collection.moveItemtoWishlist({'itemId': bagObject.itemId});
							promise.done(function () {
                            	Controller.publish('movetowishlist');
                            });
						}

                        if(!Myntra.Data.userEmail) {
                            Controller.showLogin();
                        } else {
                           	moveItem.call(this);					
						}

						
					},

					'click .combo-handler': function (event) {
						var comboId = $(event.currentTarget).data('comboid');
						_gaq.push(['_trackEvent', 'combo', 'click', 'cart', parseInt(comboId)]);
						var comboURL = location.protocol + '//' + location.hostname + '/' + (location.protocol=='http:' ? '' : 's_') + 'comboController.php?cid=' + comboId;
						location.href = comboURL;
					},
					'click .add-personalise': function (event) {
						var currentItem = $(event.currentTarget).parentsUntil('.row');
						currentItem = $(currentItem.parent()[0]);						
						var currentItemId = currentItem.data('itemid');

						var itemToEdit = _.where(this.cart.cartItemEntries, {'itemId': currentItemId});
						var view = new PersonaliseLightBoxView({
							itemId:currentItemId,
							cartElm: $('#' + $(currentItem).attr('id')),
							collection: this.collection,
							currentitem: itemToEdit,
							doCustomize:true
						});
						Controller.subscribe(view, 'personaliseBagItem');
						
					},
					'click .edit-personalise': function (event) {
						var currentItem = $(event.currentTarget).parentsUntil('.row');
						currentItem = $(currentItem.parent()[0]);						
						var currentItemId = currentItem.data('itemid');

						var itemToEdit = _.where(this.cart.cartItemEntries, {'itemId': currentItemId});
						var view = new PersonaliseLightBoxView({
							itemId:currentItemId,
							cartElm: $('#' + $(currentItem).attr('id')),
							collection: this.collection,
							currentitem: itemToEdit,
							doCustomize:true
						});
						Controller.subscribe(view, 'personaliseBagItem');
						
					},
					'click .remove-personalise': function (event) {
						var currentItem = $(event.currentTarget).parentsUntil('.row');
						currentItem = $(currentItem.parent()[0]);						
						var currentItemId = currentItem.data('itemid');
						this.collection.removePersonalisation({itemId:currentItemId});
						
					}
				},


				initialize: function (options) {
					this.cart = options.cart;
					this.cartItems = this.cart.cartItemEntries;
					this.freeGiftModelUrl = options.freeGiftModelUrl;
					this.render();
				},

				render: function () {
					var that = this;
					var resultHTML= "";
					var cartItems = this.cartItems;

					var combolist;
					var comboHTML="";
					var comboMessages = this.cart['setDisplayData'];
					var comboMessage ="";
					that.combos=[];
					var cartOfferMessageExists = cartItems.displayData;

					//if(_.isArray(cartItems)) {

						_.each(cartItems, function(element, index, list) {
                            element.appliedCoupons = that.cart.appliedCoupons;
                            element.couponNotApplicableCartItems = that.cart.couponNotApplicableCartItems;

							
							//check if the item is a combo
							if(element.comboId) {

								//check if the combo is already added to the list
								if(_.indexOf(that.combos, element.comboId) == -1) {

									//add this product comboId to the list
									that.combos.push(element.comboId);

									//find all the items in the list with the same comboId
									combolist = _.where(list, {'comboId': element.comboId});
									
									comboObject = comboMessages[element.comboId];
									comboObject.comboId = element.comboId;
									isComboComplete = comboMessages[element.comboId]['comboComplete'];
									comboHTML = that.productComboMessageTemplate(comboObject);

									//iterate over the list of items in the combo
									_.each(combolist, function (element, index, list) {
                                        element.appliedCoupons = that.cart.appliedCoupons;
                                        element.couponNotApplicableCartItems = that.cart.couponNotApplicableCartItems;

                                        if (location.protocol === "https:") {
                                            element.itemImage = element.itemImage.replace("http://myntra.myntassets.com", "https://d6kkp3rk1o2kk.cloudfront.net");
                                        }
										if(index != list.length-1)
											element.comboItem = "combo-item";
										else {
											element.comboItem = "combo-end";
										}
										comboHTML += that.bagProductsTemplate(element);
									});
									//close the combo
									comboHTML += that.productComboMessageEnd;

									//add combo to the list of all products
									resultHTML += comboHTML;
									
								}
								
							} else {

                                if (location.protocol === "https:") {
                                    element.itemImage = element.itemImage.replace("http://myntra.myntassets.com", "https://d6kkp3rk1o2kk.cloudfront.net");
                                }

                                if(!that.cart.displayData && that.cart.readyForCheckout && index == 0)
                                	element.className = "first-prod";
                                if(index == list.length-1)
                                	element.className = "last-prod";

                                resultHTML += that.bagProductsTemplate(element);
							}

							//if there are more items in the bag, flush\append the html at every 4th iteration
							
							if (index!== 0 && index%4 === 0) {
								that.$el.append(resultHTML);
								resultHTML = "";
							} 
						}); //end of each;
					/*} else {
                      if (location.protocol === "https:") {
                          this.cartItems.itemImage = this.cartItems.itemImage.replace("http://myntra.myntassets.com", "https://d6kkp3rk1o2kk.cloudfront.net");;
                      }
						resultHTML = that.bagProductsTemplate(this.cartItems);
					}*/				
					
					that.$el.append(resultHTML);
				},

				editSize : function (event) {
					var newSizeBtn = $(event.currentTarget);
					var availableQuantity = parseInt(newSizeBtn.data('count'),10);
					var selectedQuantity = parseInt($('.prod-name .mk-custom-drop-down.qty .qty-section .sel-qty').val(), 10);
					var errorMessage = $('.prod-name .mk-custom-drop-down.qty .qty-error-message');
					var errorWarningMessage = $('.prod-name .mk-custom-drop-down.qty .qty-warning-message');
					var qtySection = $('.prod-name .mk-custom-drop-down.qty .qty-section');

					newSizeBtn.siblings('.size-btn').removeClass('selected');
					newSizeBtn.addClass('selected');
					newSizeBtn.siblings('.mk-size').val(newSizeBtn.val());

					if(newSizeBtn.hasClass('unavailable')) {

						//will change the background color of the button to red
						qtySection.addClass('mk-hide');
						errorMessage.removeClass('mk-hide');
						errorWarningMessage.addClass('mk-hide');
					}
					else {

						//selecting a new size
						errorMessage.addClass('mk-hide');
						errorWarningMessage.addClass('mk-hide');
						qtySection.removeClass('mk-hide');
						var resultDropdown ="";
						
						if(availableQuantity >= 9) {

							resultDropdown = this.editLightboxQtydropdownTemplate({'numberOfOptions': 9, 'selectedQuantity': selectedQuantity });
							qtySection.html(resultDropdown);
							errorWarningMessage.addClass('mk-hide');

						} else if(availableQuantity > selectedQuantity) {

							resultDropdown = this.editLightboxQtydropdownTemplate({'numberOfOptions': availableQuantity, 'selectedQuantity': selectedQuantity });
							qtySection.html(resultDropdown);
							errorWarningMessage.html("ONLY " + availableQuantity + " " + (availableQuantity>1? 'UNITS': 'UNIT') + " IN STOCK").removeClass('mk-hide');
						
						} else if(availableQuantity <= selectedQuantity) {

							resultDropdown = this.editLightboxQtydropdownTemplate({'numberOfOptions': availableQuantity, 'selectedQuantity': selectedQuantity });
							qtySection.html(resultDropdown);
							errorWarningMessage.html("ONLY " + availableQuantity + " " + (availableQuantity>1? 'UNITS': 'UNIT') + " IN STOCK").removeClass('mk-hide');
						}

					}
				},

				mouseoverHandler: function (event) {
					$(event.currentTarget).find('.myntra-tooltip').show();
				},

				mouseoutHandler: function (event) {
					$(event.currentTarget).find('.myntra-tooltip').hide();
				},

				saveEdit: function (event) {
					var bagObject = {};
					bagObject.skuId = this.$el.find('.mk-size').val();
					bagObject.quantity = parseInt(this.$el.find('.sel-qty').val(), 10);

					var itemHTML = this.$el.find('.row.edit-product')[0];
					bagObject.itemId = $(itemHTML).data('itemid');
					this.collection.updateBagItem(bagObject);
					this.hideLightbox();
				},

				hideLightbox: function () {
					$('#lb-edit-cart-item').hide();
				}
				
			});

		return BagCollectionView;
			
});
