define(function () {
	

	var bagLoadCompleteAction = function (cartData) {

		if (cartData) {
        	var tempObject = {
				'isGiftOrder': cartData.isGiftOrder,
				'itemIds': _.pluck(cartData.cartItemEntries, 'itemId').join(','),
				'itemCount': cartData.cartItemEntries.length,
				'totalAmount':cartData.totalPrice,
	            'totalQuantity': _.reduce(cartData.cartItemEntries, function (sum, cartItemEntry){
										return sum + cartItemEntry.quantity;
									},0),
	            'mrp':cartData.totalMrp,
	            'cartLevelDiscount': cartData.bagDiscount,
	            'couponDiscount':cartData.totalCouponDiscount,
	            'itemDiscount' :cartData.totalDiscount,
	            'cashDiscount': cartData.totalMyntCashUsage,
	            'shippingCharge':cartData.shippingCharge,
	            'giftCharge':cartData.giftCharge,
	            'amount':cartData.totalMrp+cartData.giftCharge+cartData.shippingCharge,
	            'savings':cartData.totalDiscount+cartData.totalMyntCashUsage+cartData.bagDiscount+cartData.totalCouponDiscount,
	            'productAdded':0
			    
			};

			if (Myntra.WebEngage.notificationsEnabled) {
				Myntra.WebEngage.initNotifications(tempObject);
			}

		}
	}

	return {
		bagLoadCompleteAction: bagLoadCompleteAction
	}
});
