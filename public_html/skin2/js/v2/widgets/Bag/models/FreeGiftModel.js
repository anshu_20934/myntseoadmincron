define(["underscore","backbone"],function (_, Backbone) {


	var FreeGiftModel = Backbone.Model.extend({

		url : function () {
			return this.url;
		},

		parse: function (response) {
			var responseData = response;
			return responseData;
		},

		sync: function (method, model, options) {
			options.dataType = "json";
			options.headers = {
				'Accept-Language': 'en-US,en;q=0.8'
			};
			return Backbone.sync(method, model, options);
		},

		fetch: function (options) {
			options.change = true;
			this.clear();
			return Backbone.Collection.prototype.fetch.call(this, options);
		},

		initialize: function () {

		}
		
	});

	FreeGiftModel.prototype.setUrl = function(url) {
		this.url = url;
	};

	return new FreeGiftModel();
});