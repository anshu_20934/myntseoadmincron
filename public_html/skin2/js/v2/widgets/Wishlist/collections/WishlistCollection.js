define(["underscore","backbone"],function (_, Backbone) {


	var WishlistCollection = Backbone.Collection.extend({

		url : function () {
			return this.url;
		},

		parse: function (response) {
			var responseData = response.data;
			return responseData;
		},

		sync: function (method, model, options) {
			options.dataType = "json";
			return Backbone.sync(method, model, options);
		},

		fetch: function (options) {
			options.reset = true;
			options.headers = {
				'Accept-Language': 'en-US,en;q=0.8'
			};
			return Backbone.Collection.prototype.fetch.call(this, options);
		},

		initialize: function () {
		},

		addWishlistItem: function (wishlistObject) {
			var model = {};
			wishlistObject.operation = 'ADD';
			model.wishListItemEntry = wishlistObject;
			var that = this;
			
			$.ajax({
				url: this.url,
				type: 'PUT',
				data: JSON.stringify(model.wishListItemEntry),
				contentType: 'application/json',
				headers : {
					Accept : "application/json; charset=utf-8",
        			'X-CSRF-TOKEN': Myntra.Data.token,
        			'Accept-Language': 'en-US,en;q=0.8'
				},

				beforeSend: function () {
					$('.widget-overlay').removeClass('hide');
				},

				success: function(success) {
					that.reset(that.parse(success));
				},

				error: function (error) {
			 		_gaq.push(['_trackEvent', 'wishlist_collection', 'error: ' + error]);
				},

				complete: function (){					
					$('.widget-overlay').addClass('hide');
				}

			});
		},

		updateWishlistItem: function (wishlistObject) {
			var model = {};
			wishlistObject.operation = 'UPDATE';
			model.wishListItemEntry = wishlistObject;
			var that = this;
			
			$.ajax({
				url: this.url,
				type: 'PUT',
				data: JSON.stringify(model.wishListItemEntry),
				contentType: 'application/json',
				headers : {
					Accept : "application/json; charset=utf-8",
        			'X-CSRF-TOKEN': Myntra.Data.token,
        			'Accept-Language': 'en-US,en;q=0.8'
				},

				beforeSend: function () {
					$('.widget-overlay').removeClass('hide');
				},

				success: function(success) {
					that.reset(that.parse(success));					
				},
				error: function (error) {
			 		_gaq.push(['_trackEvent', 'wishlist_collection', 'error: ' + error]);
				},

				complete: function (){					
					$('.widget-overlay').addClass('hide');
				}

			});
		},

		deleteWishlistItem: function (wishlistObject) {
			var model = {};
			wishlistObject.operation = 'DELETE';
			model.wishListItemEntry = wishlistObject;
			var that = this;
			
			var xhr = $.ajax({
				url: this.url,
				type: 'PUT',
				data: JSON.stringify(model.wishListItemEntry),
				contentType: 'application/json',
				headers : {
					Accept : "application/json; charset=utf-8",
        			'X-CSRF-TOKEN': Myntra.Data.token,
        			'Accept-Language': 'en-US,en;q=0.8'
				},

				beforeSend: function () {
					$('.widget-overlay').removeClass('hide');
				},

				success: function(success) {
					that.reset(that.parse(success));					
				},
				error: function (error) {
			 		_gaq.push(['_trackEvent', 'wishlist_collection', 'error: ' + error]);
				},

				complete: function (){
					$('.widget-overlay').addClass('hide');
				}

			});

			return xhr;
		},

		moveItemtoBag: function (cartObject) {
			var that = this;
			
			var xhr = $.ajax({
				url: this.url + "/movetocart",
				type: 'PUT',
				data: JSON.stringify(cartObject),
				headers: { 
        			Accept : "application/json; charset=utf-8",
        			'X-CSRF-TOKEN': Myntra.Data.token,
        			'Accept-Language': 'en-US,en;q=0.8'       
    			},
				contentType: 'application/json',
				

				beforeSend: function () {
					$('.widget-overlay').removeClass('hide');
				},

				success: function(success) {
					that.reset(that.parse(success));									
				},

				error: function (error) {
					_gaq.push(['_trackEvent', 'bag_collection', 'error: ' + error]);
				},

				complete: function () {					
					$('.widget-overlay').addClass('hide');
				}

			});	

			return xhr;
		}

	});

	WishlistCollection.prototype.setUrl = function(url) {
		this.url = url;
	};

	return new WishlistCollection();
});
