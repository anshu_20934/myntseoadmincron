({
    baseUrl: "..",
    
    name: "Wishlist/views/app",
    paths: {
        jquery: "empty:",
        underscore: "empty:",
        backbone: "empty:",
        domReady: 'empty:',
        text: "../app/lib/text",
        controller: "empty:",
        widgets: "../widgets"
        
    },
    /*stubModules: ['text'],
    exclude: ['text'],*/
    
    out: "./buildDir/build.js"
})