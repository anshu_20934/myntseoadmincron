define(['backbone',
		'controller',
		'./wishlistHeaderView',
		'./wishlistCollectionView',
		'../collections/WishlistCollection',
		'text!widgets/Wishlist/templates/emptyWishlist.html',
		'text!widgets/common/templates/normalButton.html'],

	function (Backbone, Controller, WishlistHeaderView, WishlistCollectionView, WishlistCollection, emptyWishlistTemplate, gotoBagButtonTemplate) {

	var WishlistView = Backbone.View.extend({

		gotoBagButtonTemplate: _.template(gotoBagButtonTemplate),

		emptyWishlistTemplate: _.template(emptyWishlistTemplate),

		wishlistContainerHTML: "<div class='wishlist-prod-set'></div>",

		collection: WishlistCollection,


		events: {

			'click .btn-gotoBag': function (event) {
				_gaq.push(['_trackEvent', 'wishlist', 'btn_goto_bag_click']);
				Controller.publishNavigationEvent('showBag');
			},

			'click .place-order': function (event) {
				_gaq.push(['_trackEvent', 'wishlist', 'place_order_click']);
				Controller.publishNavigationEvent('bagPlaceOrder', BagCollection);
			},

			'click .more-shopping': function (event) {
				_gaq.push(['_trackEvent', 'wishlist', 'more_shopping_click']);
				$(event.currentTarget).find('.myntra-tooltip').toggle();
			},

			'bagLoadComplete': function (event) {
				_gaq.push(['_trackEvent', 'wishlist', 'bag_load_complete']);
				this.bagItemCount = arguments[1]? arguments[1].cartItemEntries? _.reduce(arguments[1].cartItemEntries, function(sum, cartItemEntry) { return sum + cartItemEntry.quantity;},0) : 0 : 0;
				if(this.bagItemCount > 0)
					$('.mk-checkout-header .bag-item-count').html(this.bagItemCount);		
			},

			'movetowishlist': function (event) {
				_gaq.push(['_trackEvent', 'wishlist', 'move_to_wishlist']);
				this.collection.fetch({
					error: this.errorHandler
				});
			}

		},

		getCollectionUrl: function () {
			return this.collectionUrl;
		},

		initialize: function (options) {
			_.bindAll(this,"render","errorHandler");

			WishlistCollection.setUrl(options.collectionUrl);

			WishlistCollection.fetch({
				//success: this.render,
				error: this.errorHandler
			});
			
			this.listenTo(WishlistCollection,"reset",this.render);

		},

		render: function (collection, response, options) {
			this.$el.empty();
			var collectionData = collection.toJSON()[0];
			if(collectionData && collectionData.hasOwnProperty('wishListItemEntries') && collectionData['wishListItemEntries'].length) {
				
				var gotoBagButtonHTML = this.gotoBagButtonTemplate({'buttonLabel': 'GO TO BAG', 'buttonClass': 'btn-continue btn-gotoBag'});
				var resultHTML = "";
				
				if(collectionData.wishListItemEntries.length > 1) {
					resultHTML = gotoBagButtonHTML;
				}
				
				resultHTML += this.wishlistContainerHTML + gotoBagButtonHTML;

				this.$el.append(resultHTML);
				this.showWishlistHeader(collectionData, this.bagItemCount);
				this.showWishlistProducts(collectionData, collection);
			} else {
				//if wishlist is empty
				var resultHTML = this.emptyWishlistTemplate();
				this.$el.append(resultHTML);
				this.showWishlistHeader(collectionData, this.bagItemCount);
			}
			this.afterRender();
		},

		errorHandler: function (collection, xhr, options) {
			_gaq.push(['_trackEvent', 'wishlist_view', 'error: ' + collection + " ; " + xhr + " ; " + options]);
		},

		showWishlistHeader: function (wishlist, bagItemCount) {
			if(this.wishlistHeaderView)
				this.wishlistHeaderView.undelegateEvents();
			
			this.wishlistHeaderView = new WishlistHeaderView({
				el: '.wishlist-section .mk-checkout-header-container',
				wishlist: wishlist,
				bagItemCount: bagItemCount
			});
		},

		showWishlistProducts: function (cart, collection) {
			if(this.wishlistCollectionView)
				this.wishlistCollectionView.undelegateEvents();
			
			this.wishlistCollectionView = new WishlistCollectionView({
				el: '.wishlist-prod-set',
				collection: collection,
				cart: cart
			});
		},

		/*beforeRender: function () {
			Controller.subscribe(this,'bagLoadComplete');
			Controller.subscribe(this,'movetowishlist');
		},*/

		afterRender: function () {
			
			var wishlistCollection = this.collection.toJSON().length? this.collection.toJSON()[0]: [];;
			Controller.publish('wishlistLoadComplete', wishlistCollection);
		}

		
	});

	return function (options) {
		var view = new WishlistView({
			el: options.el,
			collectionUrl: options.collectionUrl
		});

		Controller.subscribe(view,'bagLoadComplete');
		Controller.subscribe(view,'movetowishlist');

		return view;
	};

});
