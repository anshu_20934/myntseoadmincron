define(['backbone',
		'controller',
		'./editLightboxView',
		'text!widgets/Wishlist/templates/wishlistProducts.html'],
		function (Backbone, Controller, EditLightboxView, wishlistProductsTemplate) {

		var WishlistCollectionView = Backbone.View.extend({

				wishlistProductsTemplate: _.template(wishlistProductsTemplate),

				events: {
					//on mouseover show the tooltips
					'mouseover .edit-item,.move-item,.delete-item': function (event) {
						$(event.currentTarget).find('.myntra-tooltip').show();
					},

					//on mouseout remove the tooltip
					'mouseout .edit-item, .move-item, .delete-item': function (event) {
						$(event.currentTarget).find('.myntra-tooltip').hide();
					},

					'click .delete-item': function (event) {
						var currentItem = $(event.currentTarget).parentsUntil('.row');
						currentItem = $(currentItem.parent()[0]);						
						var currentItemId = currentItem.data('itemid');
						
						this.collection.deleteWishlistItem({'itemId': currentItemId});
					},

					'click .edit-item': function (event) {
						var currentItem = $(event.currentTarget).parentsUntil('.row');
						currentItem = $(currentItem.parent()[0]);
						var currentItemId = currentItem.data('itemid');
						currentItemOffset = currentItem.position();
						var itemToEdit = _.where(this.cart.wishListItemEntries, {'itemId': currentItemId});
						$(currentItem).parent().find('.lightbox').remove();
						new EditLightboxView({
							el: $('#' + $(currentItem).attr('id')),
							collection: this.collection,
							currentitem: itemToEdit
						//	offsetObject: currentItemOffset
						});
					},

					'click .move-to-bag-btn': function (event) {
						var currentItem = $(event.currentTarget).parentsUntil('.row');
						currentItem = $(currentItem.parent()[0]);	
						var wishlistObject = {};					
						wishlistObject.itemId = currentItem.data('itemid');
						var wishlistItem = _.where(this.cart.wishListItemEntries, {'itemId': wishlistObject.itemId});
						wishlistObject.skuId = wishlistItem[0].skuId;
						wishlistObject.quantity = wishlistItem[0].quantity;
						var promise = this.collection.moveItemtoBag({'itemId': wishlistObject.itemId});
						promise.done(function () {
							Controller.publish('movetobag');
						});
						
					}					
				},


				initialize: function (options) {
					this.cart = options.cart;
					this.cartItems = this.cart.wishListItemEntries;
					this.beforeRender();
					this.render();
				},

				render: function () {
					var that = this;
					var resultHTML= "";
					var cartItems = this.cartItems;
					
					_.each(cartItems, function(element, index, list) {
                        if (location.protocol === "https:") {
						    element.itemImage = element.itemImage.replace("http://myntra.myntassets.com", "https://d6kkp3rk1o2kk.cloudfront.net");
                        }

                        if(index == 0)
                        	element.className = "first-prod";
                        if(index == list.length-1)
                           	element.className = "last-prod";
						resultHTML += that.wishlistProductsTemplate(element);
						//if there are more items in the bag, flush\append the html at every 4th iteration
						if (index!== 0 && index%4 === 0) {
							$('.wishlist-prod-set').append(resultHTML);
							resultHTML = "";
						}

					});
					that.$el.append(resultHTML);
					that.afterRender();
				},

				beforeRender: function () {
					
				},

				afterRender: function () {
					
				},

				editSize : function (event) {
					var newSizeBtn = $(event.currentTarget);
					var availableQuantity = parseInt(newSizeBtn.data('count'),10);
					var selectedQuantity = parseInt($('.prod-name .mk-custom-drop-down.qty .qty-section .sel-qty').val(), 10);
					var errorMessage = $('.prod-name .mk-custom-drop-down.qty .qty-error-message');
					var errorWarningMessage = $('.prod-name .mk-custom-drop-down.qty .qty-warning-message');
					var qtySection = $('.prod-name .mk-custom-drop-down.qty .qty-section');

					newSizeBtn.siblings('.size-btn').removeClass('selected');
					newSizeBtn.addClass('selected');
					newSizeBtn.siblings('.mk-size').val(newSizeBtn.val());

					if(newSizeBtn.hasClass('unavailable')) {

						//will change the background color of the button to red
						qtySection.addClass('mk-hide');
						errorMessage.removeClass('mk-hide');
						errorWarningMessage.addClass('mk-hide');
					}
					else {

						//selecting a new size
						errorMessage.addClass('mk-hide');
						errorWarningMessage.addClass('mk-hide');
						qtySection.removeClass('mk-hide');
						var resultDropdown ="";
						if(availableQuantity >= 9) {

							resultDropdown = this.editLightboxQtydropdownTemplate({'numberOfOptions': 9, 'selectedQuantity': selectedQuantity });
							qtySection.html(resultDropdown);
							errorWarningMessage.addClass('mk-hide');

						} else if(availableQuantity > selectedQuantity) {

							resultDropdown = this.editLightboxQtydropdownTemplate({'numberOfOptions': availableQuantity, 'selectedQuantity': selectedQuantity });
							qtySection.html(resultDropdown);
							errorWarningMessage.html("ONLY " + availableQuantity + " " + (availableQuantity>1? 'UNITS': 'UNIT') + " IN STOCK").removeClass('mk-hide');
						} else if(availableQuantity <= selectedQuantity) {

							resultDropdown = this.editLightboxQtydropdownTemplate({'numberOfOptions': availableQuantity, 'selectedQuantity': selectedQuantity });
							qtySection.html(resultDropdown);
							errorWarningMessage.html("ONLY " + availableQuantity + " " + (availableQuantity>1? 'UNITS': 'UNIT') + " IN STOCK").removeClass('mk-hide');
						}

					}
				},

				saveEdit: function (event) {
					var bagObject = {};
					bagObject.skuId = this.$el.find('.mk-size').val();
					bagObject.quantity = parseInt(this.$el.find('.sel-qty').val(), 10);

					var itemHTML = this.$el.find('.row.edit-product')[0];
					bagObject.itemId = $(itemHTML).data('itemid');
					this.collection.updateBagItem(bagObject);
					this.hideLightbox();
				},

				hideLightbox: function () {
					$('#lb-edit-cart-item').hide();
					
				}
			});

		return WishlistCollectionView;
});
