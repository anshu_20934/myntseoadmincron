define(['backbone',
		'controller',
		'text!widgets/Wishlist/templates/wishlistHeader.html'],
		function (Backbone, Controller, wishlistHeaderTemplate) {

			var WishlistHeaderView = Backbone.View.extend( {

				wishlistHeaderTemplate: _.template(wishlistHeaderTemplate),

				events: {
					'click a.checkout-wishlist-header': function (event) {
						event.preventDefault();
						Controller.publishNavigationEvent('showBag');
					}

				},

				initialize: function (options) {
					this.wishlist = options.wishlist;
					this.bagItemCount = options.bagItemCount;
					this.render();
				},

				render: function () {
					var that = this;
					var resultHTML= "";
					var options = {};
					options.wishlist = this.wishlist||{};
					options.wishlistItemCount = 0;
					if(options.wishlist.wishListItemEntries.length) {
						options.wishlistItemCount = _.reduce(options.wishlist.wishListItemEntries, function (sum, wishlistItemEntry){
							return sum + wishlistItemEntry.quantity;
						},0);
					}
					options.bagItemCount = this.bagItemCount;
					resultHTML = that.wishlistHeaderTemplate(options);
					that.$el.html(resultHTML);
				}
			});

		return WishlistHeaderView;
});
