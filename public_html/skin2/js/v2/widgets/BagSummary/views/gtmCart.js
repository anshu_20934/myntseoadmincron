define(function () {
	
	var bagInit = function () {

		dataLayer.push({
	        'pageName': 'newpayment',
	        'source': Myntra.Data.UTM.Source,
	        'medium': Myntra.Data.UTM.Medium,
	        'userEmail': Myntra.Data.userEmail,
	        'userHashId': Myntra.Data.userHashId,
	        'userFBId': Myntra.Data.userFBId,
            'isLoggedIn': Myntra.Data.isLoggedIn,
            'isBuyer': Myntra.Data.isBuyer,
            'deviceName': Myntra.Data.Device.Name,
            'deviceType': Myntra.Data.Device.Type
    	});
	}

	var bagLoadComplete = function (cartData) {

		if (cartData) {
        	var tempObject = {
				'cartIsGiftOrder': cartData.isGiftOrder,
				'cartItems': _.map(cartData.cartItemEntries, function (obj) { return {'id': obj.styleId}; }),
				'cartAmount': cartData.totalPrice,
	            'cartTotalAmount':cartData.subTotalPrice,
	            'cartTotalQuantity': _.reduce(cartData.cartItemEntries, function (sum, cartItemEntry){
										return sum + cartItemEntry.quantity;
									},0),
	            'cartMrp':cartData.totalMrp,
	            'cartCartLevelDiscount': cartData.bagDiscount,
	            'cartCouponDiscount':cartData.totalCouponDiscount,
	            'cartCashDiscount': cartData.totalMyntCashUsage,
	            'cartTotalCashBackAmount':cartData.totalMyntCashUsage,
	            'cartCashBackAmountDisplayOnCart':cartData.totalMyntCashUsage,
	            'cartShippingCharge':cartData.shippingCharge,
	            'cartGiftCharge':cartData.giftCharge,
	            'cartSavings':cartData.totalDiscount,
	            'cartProductAdded':0,
                'event':'ajaxload'

			};

			_.each(cartData.cartItemEntries, function (element, index) {
				tempObject['cartItem' + index] = element.styleId;
			});

			dataLayer.push(tempObject);
 		}
	}

	return {
		bagInit: bagInit,
		bagLoadComplete: bagLoadComplete
	}
});
