define(['backbone', 
        'controller',
        '../collections/BagSummaryCollection', 
        './gtmCart',
        'text!widgets/BagSummary/templates/bagSummaryView.html'], 
        function(Backbone, Controller, BagSummaryCollection, GTMCart, BagSummaryViewTemplate) {
    
    var BagSummaryView = Backbone.View.extend({

        bagSummaryViewTemplate: _.template(BagSummaryViewTemplate),
        
        events: {
           'bagLoadComplete': "render",

           'click .summary-hd': "edit",

           'mouseover .summary-hd': "mouseoverSummary",

           'mouseout .summary-hd': "mouseoutSummary" 

        },

        initialize: function(options) {
            BagSummaryCollection.setUrl(options.collectionUrl);

            BagSummaryCollection.fetch({
                //success: this.render,
                error: this.errorHandler            
            });            
            GTMCart.bagInit();
            this.styleServiceUrl =  options.styleServiceUrl;
            this.listenTo(BagSummaryCollection,"reset",this.render);
   
        },

        
        render: function(collection, response, options) {
            var data = collection.toJSON()[0];
            if(data.cartItemEntries.length==0){ 
                Controller.publishNavigationEvent('showBag');
            }
            for (var i = 0, n = data.cartItemEntries.length; i < n; i += 1) {
                    data.cartItemEntries[i].itemThumbImage = data.cartItemEntries[i].itemImage
                        .replace('_images.jpg', '_images_48_64_mini.jpg');                    
                    if (location.protocol === "https:") {
                        data.cartItemEntries[i].itemThumbImage = data.cartItemEntries[i].itemThumbImage
                            .replace('http://myntra.myntassets.com', 'https://d6kkp3rk1o2kk.cloudfront.net')
                    }
            }
            data.cartItemCount = 0;
            if(data.cartItemEntries.length) {
                data.cartItemCount = _.reduce(data.cartItemEntries, function (sum, cartItemEntry){
                    return sum + cartItemEntry.quantity;
                },0);
            }
            this.$el.html(this.bagSummaryViewTemplate(data));
            Myntra.Data.cartTotalAmount = data.totalPrice;
            this.afterRender(data);
        },        
        afterRender: function (data) {
            GTMCart.bagLoadComplete(data);
            var cartItemIds = [],
                cartGAData = {};
            $.each(data.cartItemEntries,function(k,v){
                if(!v.freeItem) {
                    cartItemIds.push(v.styleId);
                }
            });
            if(cartItemIds.length > 0) {
                $.ajax({
                    url: this.styleServiceUrl,
                    type: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify(cartItemIds)
                }).done(function(result){
                    $.each(result.data,function(key,product){
                        cartGAData[product.id] = {};
                        cartGAData[product.id].articleType = product.articleType &&  product.articleType.typeName;
                        cartGAData[product.id].category = product.masterCategory && product.masterCategory.typeName;
                        cartGAData[product.id].brandName = product.brandName || '';
                        cartGAData[product.id].gender = product.gender || '';
                        Myntra.Data.cartGAData = cartGAData;
                    });
                });
            }
        },
        edit: function(event) {
	        _gaq.push(['_trackEvent', 'bag_summary', 'edit_click']);
            event.preventDefault();
            event.stopPropagation();
            Controller.publishNavigationEvent('showBag');            
        },

        mouseoverSummary:  function (event) {
            $(event.currentTarget).find('.edit').css('text-decoration','underline');
        },

        mouseoutSummary: function (event) {
            $(event.currentTarget).find('.edit').css('text-decoration','none');
        }

    });

    return function(options) {
        var view = new BagSummaryView({
            el: options.el,
            collectionUrl: options.collectionUrl,
            styleServiceUrl: options.styleServiceUrl
        });

        //Controller.subscribe(view,'bagLoadComplete');
        return view;
    };

});

