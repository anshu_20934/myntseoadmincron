define(["underscore","backbone", "controller"],function (_, Backbone, Controller) {


	var BagSummaryCollection = Backbone.Collection.extend({

		url : function () {
			return this.url;
		},

		parse: function (response) {
			var responseData = response.data;
			return responseData;
		},

		sync: function (method, model, options) {
			options.dataType = "json";
			options.headers = {
				'Accept-Language': 'en-US,en;q=0.8'
			};
			return Backbone.sync(method, model, options);
		},

		fetch: function (options) {
			options.reset = true;
			return Backbone.Collection.prototype.fetch.call(this, options);
		},

		initialize: function () {

		}	

	});

	BagSummaryCollection.prototype.setUrl = function(url) {
		this.url = url;
	};

	return new BagSummaryCollection();
});
