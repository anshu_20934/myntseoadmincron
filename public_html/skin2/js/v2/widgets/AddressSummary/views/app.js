
define(['backbone',
        'controller',
        '../models/addressSummaryModel',
        'text!../templates/addressSummaryTemplate.html'], 
        function(Backbone, Controller, AddressSummaryModel, AddressSummaryTemplate) {
    
    var AddressSummaryView = Backbone.View.extend({

        addressSummaryTemplate: _.template(AddressSummaryTemplate),

        model: AddressSummaryModel,

        events: {
           'click .summary-hd': "edit",

           'mouseover .summary-hd': "mouseoverSummary",

           'mouseout .summary-hd': "mouseoutSummary"
        },

        initialize: function(options) {
            if(options['addressObject']) {
                this.render(options['addressObject']);
            } else {
                this.model.setUrl(options.collectionUrl);
                this.model.fetch({
                    error: this.errorHandler
                });
                this.listenTo(this.model,"change", this.render);
            }
        },

        render: function(model) {
            var modelData = model.pincode? model : model.toJSON();
            var resultHTML = this.addressSummaryTemplate(modelData.pincode? modelData: modelData.data[0]);
            this.$el.html(_.unescape(resultHTML));
        },       

        edit: function(e) {
            _gaq.push(['_trackEvent', 'address_summary', 'edit_click']);
            e.preventDefault();
            this.undelegateEvents();
            Controller.publishNavigationEvent('showAddress');            
        },

        mouseoverSummary:  function (event) {
            $(event.currentTarget).find('.edit').css('text-decoration','underline');
        },

        mouseoutSummary: function (event) {
            $(event.currentTarget).find('.edit').css('text-decoration','none');
        }
    });

    return function(options) {
        return new AddressSummaryView(options);
    }

});

