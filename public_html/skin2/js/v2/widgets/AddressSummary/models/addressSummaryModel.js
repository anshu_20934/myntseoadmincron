define(['backbone'], function (Backbone){

	var AddressSummaryModel = Backbone.Model.extend({

		url: function () {
			return this.url;
		},

		parse: function(response) {
			var responseData = response.data;
			return responseData;
		},

		fetch: function (options) {
			options.change = true;
			options.headers = {
				'Accept-Language': 'en-US,en;q=0.8'
			};
			return Backbone.Collection.prototype.fetch.call(this, options);
		},

		setUrl: function(url) {
			this.url = url;
		},

		initialize: function() {
			
		}
	});

	AddressSummaryModel.prototype.setUrl = function(url) {
		this.url = url;
	};

	return new AddressSummaryModel();
});
