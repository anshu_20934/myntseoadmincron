
$(document).ready(function() {
    Myntra.CheckoutSteps.init();
    Myntra.CheckoutSteps.done();
    $('.show-hide-breakup').on('click',function(e){
    	var $el = $(this);
    	$('.loyalty-points-breakup').slideToggle('slow');
    	var tText = ($el.html() === 'Show Calculation') ? 'Hide Calculation' : 'Show Calculation';
    	$el.html(tText);
    });
});

