define(function() {

	var widgetPath = require.configuration.paths.widgets;
	var buildParentDir = require.configuration.baseUrl;
	var version = require.configuration.urlArgs;
	$.ajaxSetup({
  		cache: true
	});
	var widgetContainers = {
		'Bag': {
			el: '.cart-section .bagContainer',
			collectionUrl: Myntra.Data.absolutServiceUrl + '/cart/default',
			couponCollectionUrl: Myntra.Data.absolutServiceUrl + '/myntcoupons',
			freeGiftModelUrl: '/skin2/js/v2/freegift',
			moreShoppingCollectionUrl: Myntra.Data.absolutServiceUrl + '/recommendation/moreshoppingurls/123',
			init: function () {
				return true;
			}

		},
		'Wishlist': {
			el: '.wishlist-section .wishlistContainer',
			collectionUrl: Myntra.Data.absolutServiceUrl + '/wishlist',
			init: function () {
				return Myntra.Data.userEmail? true : false;
			}
		},	
		'Delivery': {
			el: '.address-section',
			collectionUrl: Myntra.Data.absolutServiceUrl + '/address',
			init: function () {
				return false;
			}
		},

		'BagSummary': {
			el: '.cart-summary',
			collectionUrl: Myntra.Data.absolutServiceUrl + '/cart/default',
            styleServiceUrl: 'myntapi/style/style-service/pdpservice/style/pdp/',
			init: function () {
				return false;
			}
		},

		'AddressSummary': {
			el: '#address-summary',
			collectionUrl: Myntra.Data.absolutServiceUrl + '/address/92',
			init: function () {
				return false;
			}
		}
	};

	var showLogin = function (onLoginCallback, action, sessionExpired) {
		var that = this;
		$(document).trigger('myntra.login.show', {
            invoke: "checkout",
            action: action || "signup",
            isModal: true,
            sessionExpired: !!sessionExpired,
            //disableCloseButton: true,
            onLoginDone: function() {
                if(onLoginCallback) {
	                Myntra.updateSecureHeaderAfterLogin();
                	onLoginCallback.call(that);
                }
                else
                    location.reload();
	            if (sessionExpired) location.reload();
	        }
        });
	}

	var init = function (reload) {
if (window.location.hash.slice(1) != "payment"){
		var checkoutPageTrackerUrl = location.protocol === 'https:' ? https_loc + '/s_checkout_page_tracker.php' : http_loc + '/checkout_page_tracker.php';
         var curLocation  = window.location.hash.slice(1)||'cart';
         if (curLocation != "address" && curLocation != "cart")
             curLocation = "cart";
        checkoutPageTrackerUrl += '?page_name=' + curLocation;
        $.get(checkoutPageTrackerUrl);

        _gaq.push(['_trackEvent',curLocation, 'page_view']);
            
}
		function loadModule(modulePath, options, reload) {
			if(!require.defined(modulePath) || reload) {
				var dfd = new $.Deferred();
				require([modulePath], function(module){
					module(options);
					dfd.resolve();
				});
				return dfd.promise();
			}
		}

        if (Myntra.Data.promptLogin) {
            $('.widget-overlay').addClass('hide');
            showLogin(function() {
	                Myntra.Data.promptLogin = false;
	                suppressHashchange = false;
	            		    		
                }, 'signin', true);
            return;
        }

        if(window.location.hash == '#cart' || !window.location.hash || window.location.hash == '#wishlist') {
			//normal flow
			for (var widget in widgetContainers) {

				if(widgetContainers[widget]['init'].call(this)) {
					var widgetOptions = widgetContainers[widget];
					if(Myntra.Data.nocombo == 0)
						loadModuleFromBuildFile(Myntra.WidgetConfig[widget], widget, widgetContainers[widget], reload);
					else 
						loadModule(widgetPath + "/" + widget + "/views/app", widgetContainers[widget], reload);				
				}
			}
			if(window.location.hash == '#wishlist')
				publishNavigationEvent('showWishlist');
			else
				publishNavigationEvent('showBag');

		} else if (window.location.hash == '#address') {
			publishNavigationEvent('bagPlaceOrder');

		} else if (window.location.hash == '#payment') {
			
			if(Myntra.Data.nocombo == 0) {
				loadModuleFromBuildFile(Myntra.WidgetConfig['BagSummary'], 'BagSummary', widgetContainers['BagSummary']);
			}
			else { 
				loadModule(widgetPath + "/" + 'BagSummary' + "/views/app", widgetContainers['BagSummary']);
			}
			
			publishNavigationEvent('selectAddress', Myntra.Data.selectedAddressId);
		}

	};

	var loadModule = function (modulePath, options, reload) {
		if(!require.defined(modulePath) || reload) { 
			require([modulePath], function(module){
				module(options);
			});
		}
		
	};

	var loadModuleFromBuildFile = function(buildFilePath, widget, options, reload) {
			
			if(!require.defined(widget + '/views/app') || reload) { 
				var dfd = new $.Deferred();
				var defer = $.getScript(buildFilePath);

				defer.success(function (script, textStatus, jqXhr) {
					require([widget + "/views/app"], function(module) {
						
						module(options);

					});
					
				});			

				return dfd.promise();
			}
	}

	var controllerEvents = {
		'bagPlaceOrder': function (data) {
            var showHideWidgets = function() {
                if (!Myntra.Data.isSecureCartEnabled && location.pathname === '/cart.php') {
                    location.href = Myntra.Data.checkoutPageUrl + '#address';
                    return;
                }
                $('.cart-section').addClass('hide');
                $('.wishlist-section').addClass('hide');
                $('.grid > .content').removeClass('hide');
                $('.grid > .summary').removeClass('hide');
                $('.address-content').removeClass('hide');
                $('.payment-content').addClass('hide');
                $('.address-summary').addClass('hide');
                $('.cart-summary').removeClass('hide');
                if(Myntra.Data.nocombo == 0) {
                	loadModuleFromBuildFile(Myntra.WidgetConfig.Delivery, "Delivery",  widgetContainers['Delivery']);
					loadModuleFromBuildFile(Myntra.WidgetConfig.BagSummary, "BagSummary",  widgetContainers['BagSummary']);
				} else {
                	loadModule(widgetPath + "/Delivery/views/app", widgetContainers['Delivery']);
                	loadModule(widgetPath + "/BagSummary/views/app", widgetContainers['BagSummary']);
                }
                window.location.hash = 'address';
                $('.widget-overlay').addClass('hide');
                suppressHashchange = true;
                Myntra.CheckoutSteps.activate(2);
            };
            if (!Myntra.Data.userEmail) {
               showLogin(showHideWidgets);
            }
            else {
            	$('.widget-overlay').removeClass('hide');
				showHideWidgets();                
            }
		},

		'showWishlist': function (data) {
            if (!Myntra.Data.isSecureCartEnabled && location.pathname !== '/cart.php') {
                location.href = Myntra.Data.cartPageUrl + '#wishlist';
                return;
            }
			
            if(!widgetContainers['Wishlist']['init'].call(this))
            {
            	showLogin(function () {;
	            	if(Myntra.Data.nocombo == 0) {
                		loadModuleFromBuildFile(Myntra.WidgetConfig.Wishlist, "Wishlist",  widgetContainers['Wishlist']);
					} else {
                		loadModule(widgetPath + "/Wishlist/views/app", widgetContainers['Wishlist']);
                	}
                	$('.cart-section').addClass('hide');
					$('.wishlist-section').removeClass('hide');
            		$('.grid > .content').addClass('hide');
           			$('.grid > .summary').addClass('hide');
                	window.location.hash = 'wishlist';
                	suppressHashchange = true;
            	});
            } else {
            	$('.cart-section').addClass('hide');
				$('.wishlist-section').removeClass('hide');
            	$('.grid > .content').addClass('hide');
           		$('.grid > .summary').addClass('hide');
                window.location.hash = 'wishlist';
                suppressHashchange = true;
            }
            $('#checkout-steps').addClass('hide');
            Myntra.CheckoutSteps.activate(1);
		},

		'showBag': function (data) {
            if (!Myntra.Data.isSecureCartEnabled && location.pathname !== '/cart.php') {
                location.href = Myntra.Data.cartPageUrl;
                return;
            }
            if(window.location.hash == '#payment')
            	window.location.replace(window.location.href.split('#')[0] + '#cart');
            else {
				$('.wishlist-section').addClass('hide');
				$('.cart-section').removeClass('hide');
	            $('.grid > .content').addClass('hide');
	            $('.grid > .summary').addClass('hide');
	            if(window.location.hash == '#wishlist' || !window.location.hash)
	            	suppressHashchange = true;
	            else
	            	suppressHashchange = false;
	            if (window.location.hash) window.location.hash = 'cart';
	            
				$('#checkout-steps').removeClass('hide');
	            Myntra.CheckoutSteps.activate(1);
        	}
		},

		'showAddress': function (data) {
			$('.cart-section').addClass('hide');
			$('.wishlist-section').addClass('hide');
            $('.grid > .content').removeClass('hide');
            $('.grid > .summary').removeClass('hide');
			$('.address-content').removeClass('hide');
			$('.payment-content').addClass('hide');
			$('.address-summary').addClass('hide');
			$('.cart-summary').removeClass('hide');
			if(Myntra.Data.nocombo == 0) {
                loadModuleFromBuildFile(Myntra.WidgetConfig.Delivery, "Delivery",  widgetContainers['Delivery']);
			} else {
            	loadModule(widgetPath + "/Delivery/views/app", widgetContainers['Delivery']);
            }
            window.location.hash = 'address';
            suppressHashchange = true;
            $('#checkout-steps').removeClass('hide');
            Myntra.CheckoutSteps.activate(2);
		},

		'selectAddress': function (addressId) {
			//remove address from the view and show payment
			
			$('.cart-section').addClass('hide');
			$('.wishlist-section').addClass('hide');
            $('.grid > .content').removeClass('hide');
            $('.grid > .summary').removeClass('hide');
			$('.address-content').addClass('hide');
			$('.payment-content').removeClass('hide');
			$('.address-summary').removeClass('hide');
			$('.cart-summary').removeClass('hide');

			if(_.isObject(addressId)) {
				widgetContainers['AddressSummary']['addressObject'] = addressId;							
			} else {
				widgetContainers['AddressSummary'].addressObject = null;
				widgetContainers['AddressSummary'].collectionUrl = Myntra.Data.absolutServiceUrl + '/address/' + addressId;
			}
			//widgetContainers['AddressSummary'].model = addressObject;
			if(Myntra.Data.nocombo == 0)
				loadModuleFromBuildFile(Myntra.WidgetConfig.AddressSummary, "AddressSummary",  widgetContainers['AddressSummary'], true);
			else	
            	loadModule(widgetPath + "/AddressSummary/views/app", widgetContainers['AddressSummary'], true);
            window.location.hash = 'payment';
            suppressHashchange = true;
            $('#checkout-steps').removeClass('hide');
            Myntra.CheckoutSteps.activate(3);
            Myntra.Payments.UI.load(addressId);
		},

		'makePayment': function (data) {
			//remove payment from the view and show confirmation page
		}

		
	};

	var events = {};
	var publishedEvents = {};

	var subscribe = function (subscriber, eventName) {

		//if new event then create an entry in the events map
		if(!events.hasOwnProperty(eventName)) {
			events[eventName] = [];
		}

		//add subscriber to the list
		events[eventName].push(subscriber);

		//event was already published but the module was not loaded
		if(publishedEvents.hasOwnProperty(eventName)) {
			$(subscriber.el).trigger(eventName,publishedEvents[eventName]);
		}

	};

	var unsubscribe = function (eventName) {
		//event if present gets pruned
		if(events.hasOwnProperty(eventName)) {
			events[eventName] = [];
		}
	};

	var publish = function (eventName, data, dontStoreData) {
		//if eventName exists then trigger the event to all subscribers
		if(events[eventName]) {
			for (var i=0; i<events[eventName].length; i++) {
				$(events[eventName][i].el).trigger(eventName, data);
			}
		} 

		if(!dontStoreData)
			publishedEvents[eventName] = data;
	};

	var publishNavigationEvent = function(eventName, data) {
		controllerEvents[eventName].call(this, data);
		if(eventName != 'bagPlaceOrder')
			$(window).scrollTop(0);
	};

	var suppressHashchange = false;
	$(window).on('hashchange', function (event) {

		if(!suppressHashchange) {
			init();
		}
		suppressHashchange = false;
			
	});

	return {
		init: init,
		publishNavigationEvent: publishNavigationEvent,
		subscribe: subscribe,
		unsubscribe: unsubscribe,
		publish: publish,
		showLogin: showLogin
	};

});
