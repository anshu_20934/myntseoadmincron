define (function () {
	
	require.config({
		//enforceDefine: true,
		shim: {
			'backbone': {
				deps: ['underscore'],
				exports: 'Backbone'
			}
		},

		baseUrl: "skin2/js/v2",
		paths: {
			underscore: 'app/lib/underscore',
			//jquery: 'app/lib/jquery',
			backbone: 'app/lib/backbone',
			text: 'app/lib/text',
			//domReady: 'app/lib/domReady',
			//controller: 'app/controller',
			controller: Myntra.WidgetConfig.checkout.replace(/\.js$/, ''),
			widgets: 'widgets'			
		},

		/*paths: {
			underscore: 'empty:',
			jquery: 'empty:',
			backbone: 'empty:',
			text: 'empty:',
			domReady: 'empty:',
			controller: 'app/controller',
			widgets: 'widgets'
		},*/

		waitSeconds: 15
	});

	require.configuration = require.s.contexts._.config;

	require(['controller'],function (controller) {
		controller.init();
	});


});
