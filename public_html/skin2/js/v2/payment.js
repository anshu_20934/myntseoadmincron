/*
 * Payment module
 *
 * The following config are set by the payment service ui
 *
 * Myntra.Payments.isInlineGatewayUp = <boolean>
 * Myntra.Payments.emiEnabled = <boolean> [set from feature-gate]
 * Myntra.Payments.EMICardArray = <object>
 *
 * The following config are set by the portal ui
 *
 * Myntra.Payments.isGiftCardOrder = <boolean>
 * Myntra.Payments.showPayHelpline = <boolean> [set from A/B test]
 * Myntra.Payments.showPayFailOpt = <boolean>  [set from A/B test]
 * Myntra.Payments.payUserCloseOpt = <boolean> [set from A/B test]
 * Myntra.Payments.showPayZipOrders = <boolean> [set from A/B test]
 * Myntra.Payments.codErrorCode = <boolean> [set to TRUE if cod is available]
 *
 */
Myntra.PayModule = (function() { "use strict"; return {
    load: function(addrId, useCashback, cashbackAmount, useLoyaltyPoints, loyaltyPoints) {
        var args = [].slice.call(arguments),
            qparams = {};

        qparams._token = Myntra.Data.token;
        /*
        $('.cart-section').addClass('hide');
        $('.wishlist-section').addClass('hide');
        $('.grid > .content').removeClass('hide');
        $('.grid > .summary').removeClass('hide');
        $('.address-content').addClass('hide');
        $('.payment-content').removeClass('hide');
        $('.address-summary').removeClass('hide');
        $('.cart-summary').removeClass('hide');
        */

        $('#payment-content').html('<div class="loading"></div>');

        if (typeof addrId !== 'undefined') qparams.address = addrId;
        if (typeof useCashback !== 'undefined' && useCashback) {
            qparams.usecashback = useCashback;
            if (useCashback === 'Y' && typeof cashbackAmount !== 'undefined') qparams.cashbackamt = cashbackAmount;
        }
        if (typeof useLoyaltyPoints !== 'undefined' && useLoyaltyPoints) {
            qparams.useloyaltypoints = useLoyaltyPoints;
            if (useLoyaltyPoints === 'Y' && typeof loyaltyPoints !== 'undefined') qparams.loyaltypoints = loyaltyPoints;
        }

        $.ajax({
            url : https_loc + '/components/payment.php',
            data: qparams,
            context: this,
            type: 'POST',
            beforeSend:function(xhr) {
            },
            success: function(data, status, xhr) {
                if (xhr.getResponseHeader('Content-Type') === 'application/json') {
                    if (data.status === 'ERROR_PROMPT_LOGIN') {
                        this.promptLogin.apply(this, args);
                    }
                    else if (data.status === 'ERROR_NO_SHIPPING_ADDRESS') {
                        if (Myntra.Data.userEmail) {
                            /*require(['controller'], function(controller) {
                                controller.publishNavigationEvent('showAddress');
                            });*///commenting to resolve issues with address selection on payment page if logged in as a different user
                            window.location.hash = 'address';
                            location.reload(true);
                        }
                        else {
                            require(['controller'], function(controller) {
                                controller.publishNavigationEvent('showBag');
                            });
                        }
                    }
                    else if (data.status === 'ERROR') {
                        alert(data.message);
                    }
                }
                else {
                    $('#payment-content').html(data);
                    this.init();
                    /* -- this is no more needed. summary widget should not show the cashback amount
                    // if cashback applied/removed, reload the Bag widget
                    if (typeof useCashback !== 'undefined') {
                        require(['controller'], function(controller) {
                            controller.publish('applyCashback');
                        });
                    }
                    */
                }
            },
            complete: function(xhr, status) {
            }
        })

    },

    init: function() {
        if ($('#payment-content .nothing-to-pay').length) {
            this.initEventHandlers();
            this.initLoyaltyPoints();
            return;
        }
        this.initFormSubmit();
        this.initCOD();
        this.initEMI();
        this.initCard();
        this.initLoyaltyPoints();
        this.initNetBank();
        this.initGiftCard();
        this.initEventHandlers();
        this.initTabs();
        this.loaded = true;
        var trackUrl = https_loc + '/components/s_tracker.php';
        trackUrl += '?page=payment';
        $.get(trackUrl);
        var checkoutPageTrackerUrl = location.protocol === 'https:' ? https_loc + '/s_checkout_page_tracker.php' : http_loc + '/checkout_page_tracker.php';
        checkoutPageTrackerUrl += '?page_name=' + window.location.hash.slice(1)||'cart';
        $.get(checkoutPageTrackerUrl);
        _gaq.push(['_trackEvent', 'payment', 'page_view']);
    },

    initTabs: function() {
        this.tabs = $('.pay-block > .tabs > ul > li');
        this.forms = $('.pay-block > .tab-content > form');
        var _this = this;
        this.tabs.on('click', function(e) {
            var id = $(e.currentTarget).attr('id').replace('tab_', '');
            if(Myntra.Payments.paymentCoupon.hasPaymentCoupon){
                /*In case of payments coupon we will show error message*/
                var allowedTabs = Myntra.Payments.paymentCoupon.allowedTabs;
                if($.inArray(id,allowedTabs) === -1){
                    _this.binPromotionBox('wrongMethod');
                    return;
                }
            }
            this.selectTab(id);
        }.bind(this)); 
        this.tabIds = $.map(this.tabs, function(tab, idx) {
            return tab.id.replace('tab_', '');
        });
        // if a tab is already selected AND the (re)loaded UI has the tab, select it
        // else, fallback to the selected tab set from the UI
        $.inArray(this.selectedTabId, this.tabIds) !== -1 ? this.selectTab(this.selectedTabId) : this.tabs.filter('.selected').click();
    },

    selectTab: function(id) {
        this.tabs.removeClass('selected');
        this.forms.hide();
        this.tabs.filter('#tab_' + id).addClass('selected');
        this.forms.filter('#' + id).show();
        this.selectedTabId = id;
        this.afterShowHide();
        if (id === 'emi') {
            var bankName = $('#emi-bank').val(),
                selectedRow = $('#emi .emi-duration#' + bankName + ' .selected');
            selectedRow.length
                ? selectedRow.click()
                : $('#emi .emi-duration#' + bankName + ' tr').first().click();
        }
        else {
            this.showHideEmiCharge(null);
        }
    },

    // Gopi:
    // Lot of dom manipulation and show/hide with backupup/restore
    // Because, the UX design expects *different* ways of showing amounts/total in the header depends on various conditions
    showHideEmiCharge: function(el) {
        var charge, total, hd, subhd, amounts, youpay, subyoupay, csshide;

        hd = $('.payment-content .content-hd');
        subhd = $('.payment-content .content-sub-hd');
        amounts = hd.find('.amounts');
        youpay = hd.find('.you-pay');
        subyoupay = subhd.find('.you-pay');
        //console.log('showHideEmiCharge: ', hd, subhd, amounts, youpay, subyoupay);

        // backup the original values as data attributes
        if (subyoupay.data('orig-total') === undefined) {
            subyoupay.data('orig-total', subyoupay.find('.rupees').html());
        }
        if (amounts.data('orig-hide') === undefined) {
            amounts.data('orig-hide', amounts.hasClass('hide'));
        }
        if (youpay.data('orig-hide') === undefined) {
            youpay.data('orig-hide', youpay.hasClass('hide'));
        }
        if (subyoupay.data('orig-hide') === undefined) {
            subyoupay.data('orig-hide', subyoupay.hasClass('hide'));
        }
        if (subhd.data('orig-hide') === undefined) {
            subhd.data('orig-hide', subhd.hasClass('hide'));
        }

        if (el && el.length) {
            charge = el.find('.fee').html();
            total = el.find('.total').html();
            //console.log('el: ', el, charge, total);
            
            hd.find('.emi-charge').find('.rupees').html(charge).end().removeClass('hide');
            amounts.removeClass('hide').removeClass('two-lines').addClass('three-lines');
            youpay.addClass('hide');
            subyoupay.find('.rupees').html(total);
            subyoupay.removeClass('hide');
            subhd.removeClass('hide');
            //console.log(amounts.data('orig-hide'), youpay.data('orig-hide'), subyoupay.data('orig-hide'), subhd.data('orig-hide'));
        }
        else {
            //console.log('reversing');
            //console.log(amounts.data('orig-hide'), youpay.data('orig-hide'), subyoupay.data('orig-hide'), subhd.data('orig-hide'));
            hd.find('.emi-charge').addClass('hide');

            csshide = amounts.data('orig-hide');
            csshide ? amounts.addClass('hide') : amounts.removeClass('hide');
            amounts.removeClass('three-lines').addClass('two-lines');
            
            csshide = youpay.data('orig-hide');
            csshide ? youpay.addClass('hide') : youpay.removeClass('hide');

            subyoupay.find('.rupees').html(subyoupay.data('orig-total'));
            csshide = subyoupay.data('orig-hide');
            csshide  ? subyoupay.addClass('hide') : subyoupay.removeClass('hide');
            
            csshide = subhd.data('orig-hide');
            csshide ? subhd.addClass('hide') : subhd.removeClass('hide');;
        }
    },

    afterShowHide: function() {
        var cont = $('#payment-content'), gh, ch,
            grid = cont.closest('.grid');

        // the UX vertical tab design has a vertical right border running to the full length of the layout grid.
        // we can't achieve this via just css. so, the height adjustment happens here
        cont.css('height', 'auto');
        gh = grid.height();
        ch = cont.height();
        //console.log('grid height: ' + gh + ' cont height: ' + ch);
        (gh > ch) && cont.css('height', gh);

        // jScrollPane needs the element to be visible (ie., display != none)
        // so, it is initialized here when the element becomes visible
        var alist = this.forms.filter(':visible').find('.card-bill-address-list-container');
        if (alist && alist.is(':visible') && !alist.data('jsp')) {
            alist.jScrollPane();
            alist.find('li').eq(0).click();
        }
        this.forms.filter(':visible').find('input, textarea, select').not('[type=hidden]').first().focus();
    },

    initCOD: function() {
        var img = $('#cod-cap-img');
        if (!img.length) { return; }

        var src = img.data('src'),
            form = $('#cod'),
            change = $('#cod-cap-change'),
            txt = $('#cod-cap-txt'),
            loading = $('#cod-cap-loading'),
            btn = $('#cod-btn-confirm'),
            msg = $('#cod-cap-msg'),
            lbl = $('#cod-cap-lbl'),
            self = this;

        txt.val('');
        change.addClass('hide');
        loading.show();
        img[0].onload = function() {
            loading.hide();
            change.removeClass('hide');
        };
        //setTimeout(function() { img[0].src = src; }, 2000);
        img[0].src = src+"&fc=1";
        change.on('click', function(e) {
            var ts = Math.round(new Date().getTime() / 1000.0),
                rex = /rand=\d+/;
            e.preventDefault();
            change.addClass('hide');
            loading.show();
            msg.removeClass('info success error').html('');
            img[0].src = rex.test(src) ? src.replace(rex, "rand=" + ts) : src + "&rand=" + ts;
            txt.val('');
            lbl.show();
        });

        lbl.on('click', function(e) {
            txt.focus();
        });

        txt.on('focus', function(e) {
            lbl.hide();
        });

        txt.on('blur', function(e) {
            $.trim($(this).val()) ? lbl.hide() : lbl.show();
        });

        this.submitCOD = function() {
            var userInput = $.trim(txt.val());
            if (!userInput) {
                msg.removeClass('info success').addClass('error').html('Please enter the captcha text');
                return;
            }
            if(btn.hasClass('disabled-btn')){
                return;
            }
            $.ajax({
                type: 'POST',
                url : https_loc + '/cod_verification.php',
                data: {condition : 'clickverifycaptcha', userinputcaptcha : userInput},
                beforeSend:function(xhr) {
                    msg.removeClass('error success').addClass('info').html('Verifying...');
                    btn.addClass('disabled-btn');
                    btn.attr('disabled','disabled');
                },
                success: function(data, status, xhr) {
                    if (xhr.getResponseHeader('Content-Type') === 'application/json') {
                        if (data.status === 'ERROR_PROMPT_LOGIN') {
                            self.promptLogin.apply(self,[{'cod':true}]);
                            return;
                        }
                    }
                    if (typeof data === 'string' && data.match(/CaptchaConfirmed/)) {
                        msg.removeClass('error info').addClass('success').html('Code verified. Please wait while your order is being processed.');
                        _gaq.push(['_trackEvent', 'payment_page', 'cod_captcha', 'success']);
                        form[0].submit();
                    }
                    else {
                        msg.removeClass('info success').addClass('error').html('Wrong Code entered');
                        _gaq.push(['_trackEvent', 'payment_page', 'cod_captcha', 'failure']);
                    }
                },
                complete: function(xhr, status) {
                    btn.removeClass('disabled-btn');
                    btn.removeAttr('disabled');
                }
            });
        };
    },

    setBillAddress: function(form, addr) {
        if (addr) {
            form.find('[name="b_firstname"]').val(addr.name);
            form.find('[name="b_address"]').val(addr.address);
            form.find('[name="b_city"]').val(addr.city);
            form.find('[name="b_state"]').val(addr.stateName);
            form.find('[name="b_zipcode"]').val(addr.pincode);
        }
        else {
            form.find('[name="b_firstname"]').val('');
            form.find('[name="b_address"]').val('');
            form.find('[name="b_city"]').val('');
            form.find('[name="b_state"]').val('');
            form.find('[name="b_zipcode"]').val('');
        }
    },

    initLoyaltyPoints: function(){
        var tip = $('.payment-content .calculation-container');
        if(tip.length !== 0 ){
            this.loyaltyTip = new Myntra.Tooltip(tip, '.cashback-loyalty-label', {side:'below'});
        }

        var lpEquivalenTip = $('.lp-equivalent-tooltip');
        if(lpEquivalenTip.length !== 0 ){
            this.lpEquivalenTip = new Myntra.Tooltip(lpEquivalenTip, '.loyal-help-icon', {side:'below'});
        }
    },
    initCard: function() {
        var _this = this;
        $('.use-other-cards').on('click', function(e) {
            e.preventDefault();
            if(Myntra.Payments.paymentCoupon.hasPaymentCoupon){
                _this.binPromotionBox('wrongMethod');
                return;
            }
            var form = $(e.currentTarget).closest('form');
            form.find('input[name="other_cards"]').val('true');
            this.submitForm({form:form}, false);
        }.bind(this));

        $('.use-new-card').on('click', function(e) {
            e.preventDefault();
            var el = $(e.currentTarget).parent();
            el.siblings('.card-details').removeClass('hide');
            el.addClass('hide');
            el.closest('form').find('input[name="use_saved_card"]').val('false');
            this.afterShowHide();
        }.bind(this));

        $('.use-saved-card').on('click', function(e) {
            e.preventDefault();
            var el = $(e.currentTarget).parent();
            el.siblings('.saved-cards').removeClass('hide');
            el.addClass('hide');
            el.closest('form').find('input[name="use_saved_card"]').val('true');
            this.afterShowHide();
        }.bind(this));

        $('.saved-cards .card-listing').on('click', 'li', function(e) {
            var el = $(e.currentTarget);
            if (el.hasClass('expired')) { return; }
            if(el.find('.opt').hasClass('selected')){return;} //Clicking on selected card again should do nothing
            el.parent().find('.opt').removeClass('selected');
            // Clear cvv numbers input 
            el.parent().find('input[name="saved_cvv_code"]').val('');
            el.find('.opt').addClass('selected');
        }.bind(this));

        var tip = $('<div class="myntra-tooltip tip-cvv"><div class="cvv-img"></div></div>').appendTo('body');
        this.cvvTip = new Myntra.Tooltip(tip, '.what-is-cvv', {side:'right'});

        $('.card-bill-address-create').on('click', function(e) {
            e.preventDefault();
            var list = $(e.currentTarget).parent(),
                form = list.siblings('.card-bill-address-form');
            form.show();
            list.hide();
            this.setBillAddress(form);
        }.bind(this));

        $('.card-bill-address-view-existing').on('click', function(e) {
            e.preventDefault();
            var form = $(e.currentTarget).parent(),
                list = form.siblings('.card-bill-address-list'),
                idx = list.find('.opt.selected').parent().data('idx'),
                addr = Myntra.Data.addresses[idx];
            form.hide();
            list.show();
            this.setBillAddress(form, addr);
        }.bind(this));

        $('.copy-ship-address').on('click', function(e) {
            e.preventDefault();
            var form = $(e.currentTarget).parent(), 
                addr = Myntra.Data.shippingAddress;
            this.setBillAddress(form, addr);
        }.bind(this));

        if (!Myntra.Data.addresses) return;
        var i, n = Myntra.Data.addresses.length, addr, markup = '', addrText;
        if (n > 0) {
            markup += '<ul>';
            for (i = 0; i < n; i += 1) {
                addr = Myntra.Data.addresses[i];
                addrText = addr.address;
                if (addr.locality) {
                    addrText += ',<br>' + addr.locality;
                }
                markup += [
                    '<li data-idx="', i, '">',
                    '   <span class="opt"></span>',
                    '   <div class="name">', addr.name, '</div>',
                    '   <div class="addr">', addrText, ',</div>',
                    '   <div class="city">', addr.city, ' ', addr.pincode, ', ', addr.stateName, '</div>',
                    '   <div class="mobile"><strong>Mobile:</strong> ' + addr.mobile, '</div>',
                    '</li>'
                ].join('');
            }
            markup += '<ul>';
            $('.card-bill-address-list-container').html(markup);
            $('.card-bill-address-list').show();
            $('.card-bill-address-form').hide();

            $('.card-bill-address-list-container').on('click', 'li', function(e) {
                var el = $(e.currentTarget),
                    idx = el.data('idx'),
                    addr = Myntra.Data.addresses[idx],
                    form = el.closest('.card-bill-address-list').siblings('.card-bill-address-form');
                el.parent().find('.opt').removeClass('selected');
                el.find('.opt').addClass('selected');
                this.setBillAddress(form, addr);
            }.bind(this));

        }
        else {
            $('.card-bill-address-list').hide();
            $('.card-bill-address-form').show();
        }
    },

    initEMI: function() {
        $('#emi .emi-duration tr').on('click', function(e) {
            var el = $(e.currentTarget);
            if (el.hasClass("emi-disabled")) {
                return;
            }
            el.closest('table').find('.radio-btn').removeClass('selected');
            el.find('.radio-btn').addClass('selected');
	    	$('#emi #emi_bank').val(el.find('.duration').data('duration'));
            this.showHideEmiCharge(el);
        }.bind(this));

        $('#emi-bank').on('change', function(e) {
            var el = $(e.currentTarget),
                tableToShow = $('#' + el.val());
            $('#emi .emi-duration').hide();
            tableToShow.show().find('.radio-btn.selected').click();
        }.bind(this));
    },

    initNetBank: function() {
        var el = $('#netBanking'), bank;
        el.on('change', function(e) {
            bank = el.find('option:selected').text().toLowerCase();
            /*
            if (bank.indexOf('icici') > -1) {
                // apply gateway discount and update order summary
            }
            else {
                // remove gateway discount
            }
            */
        });
    },

    initFormSubmit: function() {
        $(document).on('myntra.paymentui.form.submit',function(e,data){
            var _this = this;
            if(data.type === 'cod'){
                _this.submitCOD();
            }else{
                _this.submitForm(data, true);
            }
        }.bind(this));
        /*$('.pay-block > .tab-content > form').on('submit', function(e) {
            e.preventDefault();
            var form = $(e.currentTarget);

            if (form.attr('id') === 'cod') {
                this.submitCOD();
            }
            else {
                form.find('input[name="other_cards"]').val('false');
                this.submitForm(form, true);
            }
        }.bind(this));*/
    },

    initGiftCard: function() {
        var lb;
        var successPrefix = "<span class='mk-span-success-ico'></span>";
        var errPrefix = "<span class='mk-span-err-ico'></span>";
        var isActivated = false;
        var _this = this;
        $('.have-a-gift-card > a').on('click', function(e) {
            e.preventDefault();
            if(Myntra.Payments.paymentCoupon.hasPaymentCoupon){
                _this.binPromotionBox('wrongMethod');
                return;
            }
            lb = lb || Myntra.LightBox('#mk-payment-gc-activate-popup-content');
            isActivated = false;
            lb.show();
            _gaq.push(['_trackEvent', 'payment_page', 'gift_card', 'click']);
        });

        $('.mk-giftcard-activation-section .btn-ok, .mk-giftcard-activation-section .close').on('click', function(e) {
            lb.hide();
            _gaq.push(['_trackEvent', 'payment_page', 'gift_card', 'close']);
            if (isActivated) {
                Myntra.Payments.UI.load();
            }
        });
    $(".activate-gift-card").click(function(){
        var giftCardAjaxHelperUrl = https_loc+"/giftCardsAjaxHelper.php?";
        var successPrefix = "<span class='mk-span-success-ico'></span>";
        var errPrefix = "<span class='mk-span-err-ico'></span>";

		$(".mk-gc-activation-err").html("").removeClass("err").removeClass("success").slideUp('slow');
                $(".mk-gc-activation-error").html("").removeClass("err").removeClass("success").slideUp('slow');
                $(".mk-gc-activation-success").html("").removeClass("err").removeClass("success").slideUp('slow');


		var inputEl = $(".mk-gc-card");
		inputEl.removeClass("mk-err");
		var card=inputEl.val();
		
		inputEl = $(".mk-gc-pin");
		inputEl.removeClass("mk-err");
		var pin=inputEl.val();

		inputEl = $(".mk-gc-captcha");
		inputEl.removeClass("mk-err");
		var captcha=inputEl.val();


		if(card === ""){
			$("#card-err.mk-gc-activation-err").html(errPrefix + " <span class='msg'>Please enter a gift card number.</span>").addClass("err").slideDown('slow');
			inputEl.addClass("mk-err");
			return; 
		}

		if(captcha === ""){
			$("#captcha-err.mk-gc-activation-err").html(errPrefix + " <span class='msg'>Please verify the code above.</span>").addClass("err").slideDown('slow');
			inputEl.addClass("mk-err");
			return; 
		}

		$.ajax({
		  url: giftCardAjaxHelperUrl+"type=pin&mode=activate&pin="+pin+"&captcha="+captcha+"&card="+card,
		  success: function(data) {
			  var result = $.parseJSON(data);
			  if(typeof result.status != "undefined" && result.status != "failed"){
                    $(".mk-gc-activation-success").html(successPrefix + " <span class='msg'>" + result.message + "</span>").addClass("success").slideDown('slow');
                    $(".mk-gc-card").val("");
                    $(".mk-gc-pin").val("");
                    $(".mk-gc-captcha").val("");
                    $('#captcha').attr("src", https_loc+'/captcha/captcha.php?id=giftCaptcha&rand='+Math.random());
				//  setTimeout(function() {location.reload()}, 2000); // delays 2 secs and then reloads the pge so that cashback information is updated
				  //This should be a success message notification
			  } else {
                if (typeof result.status != "undefined") {
                    if (result.type == "card") {
                        $("#card-err.mk-gc-activation-err")
                            .html(errPrefix + " <span class='msg'>" + result.message + "</span>")
                            .addClass("err").slideDown('slow');
                        $('#captcha').attr("src", https_loc+'/captcha/captcha.php?id=giftCaptcha&rand='+Math.random());
                    } else if (result.type == "captcha") {
                        $("#captcha-err.mk-gc-activation-err")
                            .html(errPrefix + " <span class='msg'>" + result.message + "</span>")
                            .addClass("err").slideDown('slow');
                    } else {
                        $(".mk-gc-activation-error")
                            .html(errPrefix + " <span class='msg'>" + result.message + "</span>")
                            .addClass("err").slideDown('slow');
                        $('#captcha').attr("src", https_loc+'/captcha/captcha.php?id=giftCaptcha&rand='+Math.random());
                    }
                }
			  }
		}});	
	});

    },

    getCardType: function(cardNumber, checkLength) {
        var cardIdentifiers, ruleId, temp, i;

        cardNumber = String(cardNumber);
        cardNumber = cardNumber.replace(/-|\s*/g, "");

        if (!cardNumber || cardNumber.match(/\D/) || cardNumber.length < 3) {
            return false;
        }

        if (checkLength && cardNumber.length < 13) {
            return false;
        }

        cardIdentifiers = {
            '1': {identifier: ['4'], numlength: [13, 14, 15, 16]},
            '2': {identifier: ['51', '52', '53', '54', '55'], numlength: [16]},
            '3': {identifier: ['34', '37'], numlength: [15]},
            '4': {identifier: ['300', '301', '302', '303', '304', '305', '36', '38'], numlength: [14]},
            '5': {identifier: ['502260', '504433', '5044339', '504434', '504435', '504437', '504645', '504753', '504775', 
                '504809', '504817', '504834', '504848', '504884', '504973', '504993', '508159', '600206', '603123', 
                '603845', '622018', '508227', '508192', '508125', '508126'], numlength: [16, 17, 18, 19]}
        };

        for (ruleId in cardIdentifiers) {
            temp = cardIdentifiers[ruleId];
            if (checkLength && $.inArray(cardNumber.length, temp.numlength) === -1) {
                continue;
            }
            for (i = 0; i < temp.identifier.length; i++) {
                if (cardNumber.substr(0, temp.identifier[i].length) === temp.identifier[i]) {
                    return ruleId;
                }
            }
        }

        return "others";
    },

    showUserCardType: function(e) {
        var el = $(e.currentTarget),
            cardType = this.getCardType(el.val());

        //console.log('cardType: ' + cardType);
        if (cardType === '1') {
            el.siblings('.card-type.master').css({'opacity':'0.3'});
            el.siblings('.card-type.maestro').css({'opacity':'0.3'});
        }
        else if (cardType === '2') {
            el.siblings('.card-type.visa').css({'opacity':'0.3'});
            el.siblings('.card-type.maestro').css({'opacity':'0.3'});
        }
        else if (cardType === '5') {
            el.siblings('.card-type.visa').css({'opacity':'0.3'});
            el.siblings('.card-type.master').css({'opacity':'0.3'});
        }
        else {
            el.siblings('.card-type.visa').css({'opacity':'1'});
            el.siblings('.card-type.master').css({'opacity':'1'});
            el.siblings('.card-type.maestro').css({'opacity':'1'});
        }
    },

    validate: function(form) {
        var formID = form.attr('id'),
            cardID, card_number, card_month, card_year,
            bin, bankName, cvv, card,
            bill_name, b_firstname, b_address, b_city, b_state, b_zipcode;

        //console.log('validating form: ' + form[0].id);
        //for (var i = 0, n = form[0].elements.length; i < n; i += 1) {
        //    console.log(form[0].elements[i].name + ' = ' + form[0].elements[i].value);
        //}
        //return false;

        if (formID === 'net_banking' && !$('#netBanking').val().length) {
            alert("Please select a valid Netbanking option");
            return false;
        }
        else if (formID === 'credit_card' || formID === 'emi' || formID === 'debit_card') {
            if (form.find('input[name="use_saved_card"]').val() === 'true') {
                card = form.find('.saved-cards .card .opt.selected');
                if (!card.length){
                    alert('please select a card to continue');
                    return false;
                }
                else {
                    cvv = card.parent().find('input[name="saved_cvv_code"]').val();
                    if((/\D/.test(cvv)) || cvv.length < 3){
                        alert("Please enter a valid card CVV number");
                        return false;
                    }
                    else {
                        form[0].elements["payment_instrument"].value = card.data('key');
                        form[0].elements["cvv_code"].value = cvv;
                    }
                }
            }
            else {
                card_number = form[0].elements["card_number"].value;
                cardID = this.getCardType(card_number, true);
                if (!cardID){
                    alert("Please enter a valid card number");
                    return false;
                }

                if (formID === 'emi') {
                    bin = card_number.substring(0,6);
                    bankName = form[0].elements["selectedBank"].value;
                    if ($.inArray(bin, Myntra.Payments.EMICardArray[bankName]) === -1) {
                        alert(["Card number provided is not eligible for EMI.",
                                "Please pay using any other payment mode or try with a different card number."].join("\n"));
                        return false;
                    }
                
                }

                bill_name = form[0].elements["bill_name"].value;
                if (!bill_name) {
                    alert("Please enter the name as it appears on card");
                    return false;
                }

                if (formID === 'credit_card' || formID === 'emi' || (formID === 'debit_card' && (cardID === '1' || cardID === '2'))) {
                    cvv = form[0].elements["cvv_code"].value;
                    if (/\D/.test(cvv) || cvv.length < 3 || (cvv.length !== 3 && bankName === "CITI")) {
                        alert("Please enter a valid card CVV number");
                        return false;
                    }

                    card_month = form[0].elements["card_month"].value;
                    if (!card_month) {
                       alert("Please select card expiry month");
                       return false;
                    }

                    card_year = form[0].elements["card_year"].value;
                    if (!card_year) {
                        alert("Please select card expiry year");
                        return false;
                    }
                }

                b_firstname = form[0].elements["b_firstname"];
                if (b_firstname && !b_firstname.value) {
                    alert("Please enter a billing name");
                    return false;
                }

                b_address = form[0].elements["b_address"];
                if (b_address && !b_address.value) {
                    alert("Please enter a billing address");
                    return false;
                }

                b_city = form[0].elements["b_city"];
                if (b_city && !b_city.value) {
                    alert("Please enter a billing city");
                    return false;
                }

                b_state = form[0].elements["b_state"];
                if (b_state && !b_state.value) {
                    alert("Please enter a billing state");
                    return false;
                }

                b_zipcode = form[0].elements["b_zipcode"];
                if (b_zipcode && (!b_zipcode.value || /\D/.test(b_zipcode.value))) {
                    alert("Please enter a valid PinCode/ZipCode");
                    return false;
                }
            }
        }
        else if (formID === 'one-click-credit_card') {
            cvv = form[0].elements["cvv_code"].value;
            if (/\D/.test(cvv) || cvv.length < 3) {
                alert("Please enter a valid card CVV number");
                return false;
            }
        }

        //alert('Form validation Success'); return false;
        return true;
    },

    initEventHandlers: function() {
        $(window).on('focus', function() { this.win && this.win.focus(); }.bind(this));
        //$(window).on('message', this.onMessage.bind(this));
        $('#cancel-payment-btn').on('click', this.cancelPayment.bind(this));

        $("#result-payFailOver-err").on("click", ".btn", function(e) {
            var el = $(e.currentTarget);
            if (el.hasClass('codbtn')) {
                _gaq.push(['_trackEvent', 'payment_page', 'failover','cod']);
                this.box.hide();
                this.selectTab('cod');
            }
            else if (el.hasClass('tryagainbtn')) {
                _gaq.push(['_trackEvent', 'payment_page', 'failover', 'tryagain', Myntra.Payments.codErrorCode ? 1 : 0]);		
                this.box.hide();
            }
        }.bind(this));

        $("#result-payUserClose").on("click", ".btn", function(e) {
            var el = $(e.currentTarget);
            if (el.hasClass('codbtn')) {
                _gaq.push(['_trackEvent', 'payment_page', 'userClose','cod']);
                this.box.hide();
                this.selectTab('cod');
            }
            else if (el.hasClass('tryagainbtn')) {
                _gaq.push(['_trackEvent', 'payment_page', 'userClose', 'tryagain', Myntra.Payments.codErrorCode ? 1 : 0]);		
                this.box.hide();
            }
        }.bind(this));

        $('#splash-payments-processing .mod').on('click', '.close', function(e) {
            this.box && this.box.hide();
        }.bind(this));

        $('#payment-use-cashback').on('click', function(e) {
            e.preventDefault();
            var $useLoyaltyPoints = $('#payment-use-loyalty-points'),
            $useCashback = $('#payment-use-cashback'),
                cashBackAmount = $useCashback.data('amount'),
                useCashback = $useCashback.is(':checked') ? 'Y' : 'N',
                addrId = $('#payment-content form')[0].elements['address'].value;
            this.load(addrId,useCashback,cashBackAmount, false, 0);
        }.bind(this));

        /*Loyalty Points start*/
        $('#payment-use-loyalty-points').on('click', function(e) {
            e.preventDefault();
            var $useLoyaltyPoints = $('#payment-use-loyalty-points'),
                loyaltyPoints = $useLoyaltyPoints.data('amount'),
                useLoyaltyPoints = $useLoyaltyPoints.is(':checked') ? 'Y' : 'N',
                addrId = $('#payment-content form')[0].elements['address'].value;
            this.load(addrId,false,0, useLoyaltyPoints, loyaltyPoints);
        }.bind(this));
        /*Loyalty Points ends*/

        /* BIN based promotion starts*/
        $(document).on('myntra.paymentui.cardnum.entered',function(e,data){
            // Check for valid bin based promotion here
            if(Myntra.Payments.paymentCoupon.hasPaymentCoupon){
                if(data.binNum.length < 6) return; //Don't check if value entered by user is less than 6
                this.checkBinPromotion(data.binNum);
            }
        }.bind(this));
        /* BIN based promotion ends*/

        $('#use-another-coupon').on('click',function(e){
            _gaq.push(['_trackEvent', 'payment_page', 'bin_promotion_use_another_coupon']);
            this.gotoBag();
        }.bind(this));

        $('#remove-coupon-conti').on('click',function(e){
            var couponId = $(e.target).attr('data-coupon');
            this.deleteCouponAndContinue({coupon:couponId});
            _gaq.push(['_trackEvent', 'payment_page', 'bin_promotion_deleteCouponAndContinue']);
        }.bind(this));
    },

    /* BIN based promotion check */
    checkBinPromotion : function(binNum,clbk,showLoader){
        var cardID = this.getCardType(binNum, false);
        var _this = this;
        if (!cardID){
            if(clbk) return clbk('wrongCard');
            else return _this.binPromotionBox('wrongCard');
        }
        var promotionCheckUrl = Myntra.Data.absolutServiceUrl + '/coupon/isvalidbin/default?bin='+binNum;
        var currentRequest = $.ajax({
            url: promotionCheckUrl,
            type: 'GET',
            contentType: 'application/json',
            headers : {
                Accept : "application/json; charset=utf-8",
                'X-CSRF-TOKEN': Myntra.Data.token,
                'Accept-Language': 'en-US,en;q=0.8'
            },
            beforeSend : function(){
                if(currentRequest && currentRequest.abort) currentRequest.abort();
                if(showLoader) _this.binPromotionBox('showProcessing');
            },
            success: function(d) {
                if(d.applicability && d.pgCouponApplied){
                    _this.binPromotionBox('hide');
                    if(clbk) return clbk(null); // Call the callback to continue the flow
                    else return true;
                }else{
                    if(clbk) return clbk('wrongCard');
                    else return _this.binPromotionBox('wrongCard');
                }
            },
            error: function (error) {
                _gaq.push(['_trackEvent', 'bag_collection', 'error: ' + error]);
                // Show error message in the modal window
                if(clbk) return clbk('serverError');
                else return _this.binPromotionBox('serverError');
            }

        });
    },

    binPromotionBox : function(type){
        var binErrorBox = Myntra.LightBox('#bin-promotion-box', {isModal:true});
        var $el = $('#bin-promotion-box');
        if(type == 'hide') {
            binErrorBox.hide();
            return;
        }
        $el.find('.removing-coupon').addClass('mk-hide');
        $el.find('.bin-promotion-error').addClass('mk-hide');
        $el.find('.bin-promotion-server-error').addClass('mk-hide');
        $el.find('.checking-bin-promotion').addClass('mk-hide');

        if(type == 'removingCoupon'){
            $el.find('.removing-coupon').removeClass('mk-hide');
        }else if(type == 'showProcessing'){
            $el.find('.checking-bin-promotion').removeClass('mk-hide');
        }else if(type == 'wrongMethod'){
            $el.find('.wrong-card-entered').addClass('mk-hide');
            $el.find('.wrong-method-chosen').removeClass('mk-hide');
            $el.find('.bin-promotion-error').removeClass('mk-hide');
        }else if(type == 'wrongCard'){
            $el.find('.wrong-card-entered').removeClass('mk-hide');
            $el.find('.wrong-method-chosen').addClass('mk-hide');
            $el.find('.bin-promotion-error').removeClass('mk-hide');
        }else if(type == 'serverError'){
            $el.find('.bin-promotion-server-error').removeClass('mk-hide');
        }
        if(!$el.is(":visible")) binErrorBox.show();
    },
    /* BIN based promotion check ends*/

    deleteCouponAndContinue: function (couponObject) {
        var _this = this;
         $.ajax({
            url: Myntra.Data.absolutServiceUrl + "/cart/default/applymyntcoupon",
            type: 'DELETE',
            data: JSON.stringify(couponObject),
            headers: { 
                Accept : "application/json; charset=utf-8",
                'X-CSRF-TOKEN': Myntra.Data.token,
                'Accept-Language': 'en-US,en;q=0.8'
            },
            contentType: 'application/json',
 
            beforeSend: function () {
                _this.binPromotionBox('removingCoupon');
            },

            success: function(success) {
                location.reload();
            },

            error: function (error) {
                _gaq.push(['_trackEvent', 'payment', 'error: deleteCoupon', error.statusText]);
            }

        });
    },


    onMessage: function(e) {
        if (e.originalEvent.origin !== https_loc) {
            return;
        }
        /*
        console.log('onmessage: ' + e.originalEvent.data);
        this.win && this.win.close();
        clearInterval(this.tmr);
        this.tmr = null;
        this.win = null;
        window.location = e.originalEvent.data;
        */
    },

    cancelPayment: function() {
        Myntra.payInstruments && Myntra.payInstruments.initFormSubmit();
        clearInterval(this.tmr);
        this.declineLastOrder(2);
        this.win && this.win.close();
        this.box.hide();
        this.tmr = null;
        this.win = null;
    },

    watchWindow: function() {
        if (this.win && this.win.closed) {
            Myntra.payInstruments && Myntra.payInstruments.initFormSubmit();
            clearInterval(this.tmr);
            this.declineLastOrder(1);
            this.tmr = null;
            this.win = null;
            if ($('#processing-payment').is(':visible')) {
                if (Myntra.Payments.payUserCloseOpt) {
                    $('#processing-payment').hide();
                    $('#result-payUserClose').show();
                }
                else {
                    this.box.hide();
                }
            }
        }
    },

    declineLastOrder: function(reasonCode) {
        $.ajax({
            type: "POST",
            url: https_loc + "/declineLastOrder.php",
            data: "reason_code=" + reasonCode + "&order_type=" + Myntra.Data.OrderType,
            context: this,
            success: function(data) {
                data = $.trim(data);	
                //console.error('declineLastOrder: ' + data);
            },
            complete:function() {
            }
        });
    },

    submitForm: function(data, validate) {
        var form = data.form;
        var totalAmount, markup;
        var _this = this;

        if (validate) {
            if (Myntra.Payments.isInlineGatewayUp && !this.validate(form)) {
                return false;
            }
            _gaq.push(['_trackEvent', 'payment_page', 'proceed_to_pay', form.attr('id')]);
        }
        else {
            _gaq.push(['_trackEvent', 'payment_page', 'use_intl_creditcard']);
        }

        _this.submitFormAfterValidate(form, data.binNum);
        // if(Myntra.Payments.paymentCoupon.hasPaymentCoupon){
        //     _this.checkBinPromotion(data.binNum,function(){
        //         _this.submitFormAfterValidate(form);
        //     },true);
        // }else{
        // }
    },

    submitFormAfterValidate : function(form, binNum){
        var _this = this;
        $('.pay-block > .tab-content > form').off('submit'); //Remove event handler so that it won't trigger again for the payment flow to complete
        var markup = '';
        this.win = window.open('', "MyntraPayment", "location=1,status=1,scrollbars=1,top=150,left=200,width=800,height=400");
        if (this.win) {
            markup = [
                '<html><head><title>Myntra Payment</title></head><body><div><center>',
                '<h2>Processing Payment</h2>',
                '<img src="https://d6kkp3rk1o2kk.cloudfront.net/images/image_loading.gif">',
                '<div>Enter your card/bank authentication details in the window once it appears on your screen. <br/>',
                'Please do not refresh the page or click the back button of your browser.<br/>',
                'You will be redirected back to order confirmation page after the authentication.<br/><br/></div>',
                '</center></div></body></html>'
            ].join('');

            try {
                this.win.document.open();
                this.win.document.write(markup);
                this.win.document.close();
            }
            catch (ex) {
            }
            this.win.focus();
            
            if (this.tmr) {
                clearInterval(this.tmr);
            }
            this.tmr = setInterval(this.watchWindow.bind(this), 1000);
        }

        if (!this.box) {
            this.box = Myntra.LightBox('#splash-payments-processing', {isModal:true, disableCloseButton:true});
        }
        // hide all the sub sections and show the processing mod
        $('#splash-payments-processing .mod').hide();
        $('#processing-payment').show();
        this.box.show();

        if(Myntra.Payments.paymentCoupon.hasPaymentCoupon){
            _this.checkBinPromotion(binNum,function(error){
                if(error){
                    Myntra.payInstruments && Myntra.payInstruments.initFormSubmit();
                    clearInterval(_this.tmr);
                    _this.win && _this.win.close();
                    _this.box.hide();
                    _this.tmr = null;
                    _this.win = null;
                    _this.binPromotionBox('wrongCard');
                    return false;
                }else{
                    form.submit();
                    return true;
                }
            },false);
        }else{
            form.submit();
            return true;
        }
    },

    redirectUserForLoginClose : function(){
        //If secure cart is enable or secure page url is set. Redirect user to myntra home.. Myntra.Data.isSecureCartEnabled .. Myntra.Data.cartPageUrl
        if(Myntra.Data.isSecureCartEnabled){
            window.location = http_loc; // If cart url is not set redirect user to home page
        }else{
            if(Myntra.Data.cartPageUrl){
                window.location = Myntra.Data.cartPageUrl;
            }else{
                window.location = http_loc; // If cart url is not set redirect user to home page
            }
        }
    },

    promptLogin: function() {

        var args = [].slice.call(arguments);
        var self = this;

        $(document).trigger('myntra.login.show', {
            invoke: "checkout",
            action: "signin",
            secureSessionExpired: true,
            isModal: true,
            onLoginDone: function() {
                //var url = document.URL.replace(/promptlogin=?\d?/, '');
                //console.log('reload URL: ' + url);
                //window.location = url;
                //Myntra.updateSecureHeaderAfterLogin();
                //!this.loaded && this.load.apply(this, args);
                if(args.length && typeof args[0] == 'object' && args[0].cod){
                    location.reload(true);
                }else{
                    window.location.hash = 'address';
                    location.reload(true);   
                }
                 
            }.bind(this),
            onLoginClose : function(){
                //When the user explicitly closes the Login Modal box. Redirect user to cart page if cart page url is inscure
                self.redirectUserForLoginClose();
            }.bind(this),
            afterForgotPasswordOk : function(){
                // When the user completes the forgot password flow
                self.redirectUserForLoginClose();
            }.bind(this)
        });
    },

    gotoBag: function() {
        require(['controller'], function(controller) {
            controller.publishNavigationEvent('showBag');
        });
    }
    
    /* -- will be used later
    notify: function(msg, data) {
        //console.log("notify", msg, data);
        if (msg === 'promptlogin') {
            this.promptLogin();
        }
        else if (msg === 'payfailure') {
            $('#processing-payment').hide();
            if (Myntra.Payments.showPayFailOpt) {
                $('#result-payFailOver-err').show();
                $('#splash-payments-processing-bd-pp-payFailOver-err').html(data.markup);
            }
            else {
                $('#result-errPayment').show();
                window.location = data.redirectUrl;
            }
        }
        else if (msg === 'paysuccess') {
            $('#processing-payment').hide();
            $('#result-succPayment').show();
            window.location = data.redirectUrl;
        }
    }
    */

}}()); // end of iify module wrapper


$(document).ready(function() { "use strict";
    $('#login-trigger').on('click', function(e) {
        e.preventDefault();
        $(document).trigger('myntra.login.show', {invoke:"checkout", action:"login"});
    });

    Myntra.Payments = Myntra.Payments || {};
    Myntra.Payments.UI = Object.create(Myntra.PayModule);

    if (Myntra.Data.promptLogin) {
        Myntra.Payments.UI.promptLogin();
    }
});
