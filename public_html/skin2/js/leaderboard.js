$(document).ready(function() {
	
	$("#whereAmI").click(function(){
		$("#whereAmIForm").css("display","block");
	});
	
	$("#whereSearch").click(function(){
		var searchLogin = $("#searchLogin").val();
		var lbId  = $("#lbId").val();
		$(this).attr("disabled", "disabled");
		$.ajax({
	        type: 'POST',
	        url: "leaderboard.php",
	        data: "_token=" + Myntra.Data.token + "&mode=search&lbId=" + lbId+"&searchLogin="+searchLogin,
	        success: function(data){
	            data = jQuery.parseJSON(data);
	            if(data.result == 'success'){
		        	$("#responseDiv").html(data.content);
		        	$(this).attr("disabled", "");
                }else{
		        	$("#responseDiv").html("Something went wrong! Please try again later");
		        	$(this).attr("disabled", "");
                }
	        	
	        }
	    });
	});
	
	
});


function refresh(mode,divId){
	var lbId  = $("#lbId").val();
	$.ajax({
        type: 'POST',
        url: "leaderboard.php",
        data: "_token=" + Myntra.Data.token + "&mode="+mode+"&lbId=" + lbId,
        success: function(data){
        	$("#"+divId).html(data);
        }
    });
}

function refreshMessage(){
	var lbId  = $("#lbId").val();
	$.ajax({
        type: 'POST',
        url: "leaderboard.php",
        data: "_token=" + Myntra.Data.token + "&mode=message&lbId=" + lbId,
        success: function(data){
        	if(data != ""){
        		$("#scrollingText").html(data);
        	}
        }
    });
}



setInterval(function(){refresh('purchases','purchases')}, 120000);
setInterval(function(){refresh('trendingStyles','trendingStyles')}, 120000);
setInterval(function(){refresh('leaderBoard','leaderBoard')}, 120000);

setInterval(refreshMessage, 30000);


