Myntra.PincodeWidget = function(cfg) {

	/* Begin init code */
	var markup = ['<div id="pincode-widget" class="infobox lightbox">',
							'<div class="mod">',
								'<div class="hd">',
									'<div class="title">Delivery Time & <span>CoD</span> Availability</div>',
								'</div>',
								'<div class="bd body-text">',
									'<p class="msg">',
										'Please enter your PIN Code to check delivery time and Cash on Delivery availability.',
									'</p>',
									'<form>',
										'<div class="row"><label>PIN Code:</label> <input type="text" id="pincode" name="pincode" class="small" size="6" maxlength="6" /><div class="err" id="pincode-error">Enter a valid 6 digit PIN Code</div></div>',
										'<button class="primary-btn small-btn btn">Check PIN Code</button>',
									'</form>',
									'<div class="result">',
										'<div class="loading"></div>',
										'<div id="outside-network">',
											'<p><span><span class="icon-error"></span>Unfortunately, we do not ship to your PIN Code!</span></p>',
											'<p>We are continually expanding our delivery network but there are still some parts of India that we cannot deliver to.</p>',
											'<p>We will strive to be able to reach your PIN Code soon!</p>',
										'</div>',
										'<p id="delivery-time"><span>',
											'<span class="icon-success"></span>It\'ll take <em class="red">6-7</em> days for us to deliver this product to your PIN Code.<br />',
											'Actual delivery time may vary depending upon other items in your order.',
										'</span></p>',
										'<p id="cod-available"><span><span class="icon-success"></span>Cash on Delivery is available for your PIN Code<br />for all orders between <em class="red">Rs. 500 - Rs. 10,000</em>.</span></p>',
										'<p id="only-cod-available"><span><span class="icon-alert"></span>ONLY cash on delivery payment option is available for your PIN Code<br />for all orders between <em class="red">Rs. 500 - Rs. 10,000</em>.</span></p>',
										'<p id="cod-unavailable"><span><span class="icon-alert"></span>Cash on delivery payment option is NOT available for your PIN Code.</span></p>',
									'</div>',
								'</div>',
								'<div class="ft"></div>',
							'</div>',
						'</div>'].join('');
	$('body').append(markup);
	
	var obj = Myntra.LightBox('#pincode-widget'),
		codLimitsFetched = false;

	$('#pincode-widget .title .info-icon').remove();
	
	/* Override lightbox's show method to focus on pincode input box */
	var lbShow = obj.show;
	obj.show = function() {
		lbShow();
		$('#pincode-widget #pincode').focus();
	};

	$('#pincode-widget #pincode').focus(function() {
		$(this).val('');
		resetResults();
	});
	
	$('#pincode-widget form').submit(function() {
		resetResults();
		var pincode = $(this).find('#pincode').val(),
			loader = $('#pincode-widget .loading');
		/* Validate Pincode for 6 digit number */
		if (!pincode || !/^\d{6}$/.test(pincode)) {
			$(this).find('#pincode-error').css('visibility', 'visible');
			return false;
		}
		_gaq.push(['_trackEvent','PincodeWidget', pincode, 'CheckPincode', cfg.styleid]);
		loader.fadeIn();
		
		/* Validation successful, sending Ajax Request */
		var request = sendAjax(pincode);
		request.done(function(result) {
			loader.fadeOut();
			if (result.status == 'SUCCESS') {
				setBrowserStorage(result.data);
				showResults(result.data);
			}
		});
		return false;
	});
	/* End init code */
	
	/* Called on Form Submit and in fetchAndStoreJSON, sends ajax request and returns jqxhr object */
	/* When pincode is not give, fetches data for user's default address */
	function sendAjax(pincode) {
		var ajaxUrl = http_loc + '/ajaxPincodeWidget.php?' + getQuryPrameters();
		if (pincode) {
			ajaxUrl += '&pincode=' + pincode;
		}
		
		/* So as not to query Feature Gate with every request */
		if (codLimitsFetched) {
			ajaxUrl += '&gotCod=1';
		}
		
		return $.ajax({
			type: "GET",
			url: ajaxUrl
		});

		function getQuryPrameters() {
			var skuid = $(".flat-size-options .options .mk-size").val() || cfg.skuid;
			var $sizeButton = $(".flat-size-options .options button[value="+skuid+"]");
			$sizeButton = $sizeButton.length ? $sizeButton : $('.mk-custom-drop-down .mk-freesize'); //In case of one size options buttons will not be available
			var payLoad = {
				styleid : cfg.styleid,
				skuid : skuid,
				availableinwarehouses : $sizeButton.data().availableinwarehouses || "",
				leadtime : $sizeButton.data().leadtime || "",
				supplytype : $sizeButton.data().supplytype || ""
			}
			return decodeURIComponent($.param(payLoad));
		}
	}
	
	/* Fetches data for given pincode or user's default shipping address, sends data to setBrowserStorage */
	function fetchAndStoreJSON(pincode) {
		var request = sendAjax(pincode);
		request.done(function(result) {
			if (result.status == 'SUCCESS') {
				setBrowserStorage(result.data);
			}
		});
	}
	
	/* Stores raw serviceability data in localStorage as a JSON string */
	function setBrowserStorage(json) {
		if (!localStorage || !json.SERVICEABLE.length) return; // Unserviceable pincodes' data is not stored
		localStorage.setItem('PincodeWidget.ServiceabilityData', JSON.stringify(json)); // Store in persistent localStorage
		$(window).trigger('myntra.pincodewidget.datachange');
	}
	
	/* Populates the Pincode Widget Infobox */
	function showResults(json) {
		var outsideNetwork = $('#outside-network'),
		deliveryTime = $('#delivery-time'),
		codAvailable = $('#cod-available'),
		onlyCodAvailable = $('#only-cod-available'),
		codUnavailable = $('#cod-unavailable');
	
		/* Insert COD Limits into markup */
		if (!codLimitsFetched && json.codLimits) {
			codAvailable.add(onlyCodAvailable).find('em.red').html(json.codLimits);
			codLimitsFetched = true;
		}
		
		deliveryTime.find('em.red').html(json.DELIVERY_PROMISE_TIME);
		if ($.inArray('cod', json.SERVICEABLE) > -1) {
			deliveryTime.fadeIn();
			if ($.inArray('on', json.SERVICEABLE) > -1) {
				codAvailable.fadeIn();
				_gaq.push(['_trackEvent','PincodeWidget', json.pincode, 'COD_1_OL_1_Days_' + json.DELIVERY_PROMISE_TIME, cfg.styleid]);
			} else {
				onlyCodAvailable.fadeIn();
				_gaq.push(['_trackEvent','PincodeWidget', json.pincode, 'COD_1_OL_0_Days_' + json.DELIVERY_PROMISE_TIME, cfg.styleid]);
			}
		} else {
			if ($.inArray('on', json.SERVICEABLE) > -1) {
				deliveryTime.add(codUnavailable).fadeIn();
				_gaq.push(['_trackEvent','PincodeWidget', json.pincode, 'COD_0_OL_1_Days_' + json.DELIVERY_PROMISE_TIME, cfg.styleid]);
			} else {
				outsideNetwork.fadeIn();
				_gaq.push(['_trackEvent','PincodeWidget', json.pincode, 'COD_0_OL_0', cfg.styleid]);
			}
		}
	}
	
	/* Clears the Pincode Widget Infobox */
	function resetResults() {
		$('#pincode-widget .result > p, #pincode-widget .result > div').hide();
		$('#pincode-widget #pincode-error').css('visibility', 'hidden');
	}
	
	/* Expose relevant methods and variables */
	obj.fetchAndStoreJSON = fetchAndStoreJSON;
	obj.initialized = true;
	return obj;
};
