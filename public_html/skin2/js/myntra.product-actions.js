/* Product actions such as select size, save in wishlist, notify, add to bag; required for PDP and MiniPIP */

var sizeSelect = function(sku, idx, el) {
    el = $(el);
    var sizeDropCont = el.closest('.mk-product-option-cont');

    el.parent().removeClass('error');
    el.siblings('.size-btn').removeClass('selected');
    sizeDropCont.find('.saved-message').hide();
    el.addClass('selected');
    if (el.hasClass('unavailable')) {
        sizeDropCont.find('.add-button-group, .mk-count-message').addClass('mk-hide');
        sizeDropCont.find('.notify-cont').removeClass('mk-hide')
        	.find('.notify-btn-grp').show()
    	.end()
    		.find('.notify-msg').html('');
    } else {
    	var cntMsg = sizeDropCont.find('.mk-count-message');
        el.siblings('.mk-size').val(sku);
        sizeDropCont.find('.add-button-group').removeClass('mk-hide');
        sizeDropCont.find('.notify-cont').addClass('mk-hide');
        (el.data('count') <= 3)
            ? cntMsg.removeClass('mk-hide').find('span').text(el.data('count'))
            : cntMsg.addClass('mk-hide');
    }
};

$(function() {
	/* CLOSE NOTIFY WHEN CANCEL IS CLICKED */
	$('.notify-cancel').live('click', function(e){
		var sizeDropCont = $(this).closest('.mk-product-option-cont');
		e.preventDefault();
		sizeDropCont.find('.add-button-group').removeClass('mk-hide');
		sizeDropCont.find('.notify-cont').addClass('mk-hide');
		//sizeDropCont.find('.mk-size-drop ul li a').removeClass('selected').css('background','#fff');
		//var selectedValue=sizeDropCont.find(".mk-size option[value='0']").text();
		//sizeDropCont.find('.mk-size-drop div span').text(selectedValue);
		sizeDropCont.find('.size-btn').removeClass('selected');
		sizeDropCont.find('.mk-size').val(0);
	});
	
	/* NOTIFY ME EMAIL CLEAR */
	$('.notify-email').live('focus', function() {
		if ($(this).hasClass('empty')) {
			$(this).val('').removeClass('empty');
		}
	}).live('blur', function() {
		if (this.value == '') {
			$(this).addClass('empty').val('Your email address');
			$(this).siblings('.notify-msg').html('');
		}
	});
	
	/* NOTIFY ME EMAIL */
	$('.notify-button').live('click',function(e){
		e.preventDefault();
		var notifyMsg = $(this).closest('.notify-cont').find('.notify-msg').html(''),
			formToSubmit = $(this).closest('.notify-me-form'),
			notifyBtns = $(this).closest('.notify-btn-grp'),
			email = formToSubmit.find("input.notify-email").val();
		if (Myntra.Utils.validateEmail(email)) {
			var sku = $(this).closest('.mk-product-guide').find('.size-btn.selected').val();
			var url = http_loc + "/subs_notification.php";
			formToSubmit.find(":hidden[name='sku']").val(sku);
			$.ajax({
				type: "POST",
				url: url,
				data: formToSubmit.serialize()
			});
			notifyMsg
				.addClass('success')
				.removeClass('error')
				.html('<span class="icon-success"></span> We will notify you when this size is available.');
			notifyBtns.hide();
			setTimeout(function(){
				$('.notify-cancel').trigger('click');
			},4000);
			var pageName = formToSubmit.find(":text[name='pageName']").val();
			var parameterForGA = sku + '-' + email;
			_gaq.push(['_trackEvent', pageName, 'notifyme', parameterForGA]);
		} else {
			notifyMsg
				.removeClass('success')
				.addClass('error')
				.html('Please enter a Valid Email Address');
		}
	});

	/*SIZE SELECT*/
	var sizeSelected = function(sizeDropCont){
		return ((sizeDropCont.find('.mk-size').val()!=0) || (sizeDropCont.find('.mk-freesize').length));
	};
	
	//ADD TO CART BUTTON PDP AND QUICK LOOK	 //COPY THIS FOR SIZECHART AS WELL
	$('.mk-add-to-cart,.mk-express-buy,mk-add-to-combo').live('click',function(){
		var sizeDropCont = $(this).closest('.mk-product-option-cont');
		var formToSubmit = $(this).closest('.mk-product-guide').find('.add-to-cart-form');
		if ($(this).hasClass('mk-express-buy')) {
			formToSubmit.attr('action','/oneclickcart.php');
		} else {
			formToSubmit.attr('action','/mkretrievedataforcart.php?pagetype=productdetail');
		}
		
		if (sizeSelected(sizeDropCont)) {
			var quantity = formToSubmit.find(":hidden[name='quantity']").val();
			var sku_;
			var size;
			if (sizeDropCont.find('.mk-freesize').length) {
				size = sizeDropCont.find('.mk-freesize').text();
			} else {
				size = sizeDropCont.find('.flat-size-options .size-btn.selected').text();
				sku_ = sizeDropCont.find('.mk-size').val();
			}
			var productStyleId = formToSubmit.find(":hidden[name='productStyleId']").val();
			var articleType = formToSubmit.find(":hidden[name='articleType']").val();
			var pd = {'id':productStyleId,'at':articleType};
		

			formToSubmit.find(":hidden[name='sizequantity[]']").val(quantity);
			if (!sizeDropCont.find('.mk-freesize').length) {
				formToSubmit.find(":hidden[name='productSKUID']").val(sku_);
			}
			formToSubmit.find(":hidden[name='selectedsize']").val(size);
			
			//AJAX ADD TO CART USE FOR NEW COMBOS
			
			if($(this).hasClass('mk-add-to-combo')){
				//create a styleObj for the popup
				var styleObj = {
					'sid':productStyleId,
					'size':size,
					'cid' : formToSubmit.find(":hidden[name='comboId']").val(),
					'name':formToSubmit.find(":hidden[name='productStyleLabel']").val(),
					'price':formToSubmit.find(":hidden[name='unitPrice']").val()
				};

				var rURL=http_loc+"/mkretrievedataforcart.php?pagetype=productdetail";
				$.ajax({
					type:"POST",
					url:rURL,
					data:formToSubmit.serialize(),
					beforeSend:function(){
					 	$(document).trigger('myntra.addedtocombo.showloading');
					},
					success: function(data){
                        if (Myntra.PDP && Myntra.PDP.gaConversionData) {
                            _gaq.push(['_trackEvent','Cart_conversion', Myntra.PDP.gaConversionData.action, Myntra.PDP.gaConversionData.label]);
                        }
						styleObj.resp = $.parseJSON(data);
						$(document).trigger('myntra.addedtocombo.show',styleObj);
					}
					

		        });
		    }

		    else{
			//FORM SUBMIT TRIGGER
                if (Myntra.PDP && Myntra.PDP.gaConversionData) {
                    _gaq.push(['_trackEvent','Cart_conversion', Myntra.PDP.gaConversionData.action, Myntra.PDP.gaConversionData.label]);
                }
				formToSubmit.submit();
			}
			//alert(sku_);
			//Create a new lightbox here for order confirmation;
		} else {
			sizeDropCont.find('.flat-size-options .options').addClass('error');
		}
	}).live("mouseenter", function() {
		if (!sizeSelected($(this).closest('.mk-product-option-cont'))) {
			if ($(".zoom-window").length) {
				$(this).closest('.add-button-group').find('.mk-size-error').css('top', '40px').show();
			} else {
				$(this).closest('.add-button-group').find('.mk-size-error').css('top', '-3px').show();
			}	
		}
	}).live("mouseleave", function() {
		$(this).closest('.add-button-group').find('.mk-size-error').hide();
	});

	// Save in Wishlist
	Myntra.saveItemAsync = function(saveObj){
		var rURL = http_loc+"/mksaveditem.php",
			sizeDropCont = saveObj.sizeCont || $('.mk-product-page .mk-product-option-cont');
		$.ajax({
			type: "POST",
			url: rURL,
			data: "actionType=add&productSKUID="+saveObj.sku+"&sizequantity="+saveObj.qty+"&_token="+Myntra.Data.token,
			success: function(data) {
                //Kuliza Code start 
                var kulizaCodeExecuted = 0;
                if(kulizaEchoEnabled){
                    if(typeof Myntra.Data.MiniPIPsocialExclude === 'undefined'){
                        if(typeof Myntra.Data.PDPsocialExclude !== 'undefined'){
                            if(Myntra.Data.PDPsocialExclude == false){
                                kulizaCodeExecuted = 1;
                                Myntra.FB.showPopupIfRequired('pdp_flow', 'false', 'false', 'false', 'false');
                            }
                        }
                    } else {
                        if(Myntra.Data.MiniPIPsocialExclude == false){
                            kulizaCodeExecuted = 1;
                            Myntra.FB.showPopupIfRequired('pdp_flow', 'false', 'false', 'false', 'false');
                        }
                    }
                }
				var addBtnGrp = sizeDropCont.find('.add-button-group');
				if (window.location.pathname!='/mkmycart.php'){
					if(Myntra.Utils.Cookie.get('_sku')){
						Myntra.Utils.Cookie.del('_sku');
						Myntra.Utils.Cookie.del('_url');
						Myntra.Utils.Cookie.del('_saveitem_ex_social');
                    }
                    // get width and position fo save for later and then position the success message
                    var __pos = $('.mk-save-for-later').position();
                    var __width = $('.mk-save-for-later').width();
                    __toppos = __pos.top + 10;
                    __leftpos = __pos.left + __width+15;
                   
                    if(kulizaCodeExecuted == 1){
                        if(typeof Myntra.Data.MiniPIPsocialExclude === 'undefined'){
                            var savedMsg = addBtnGrp.find('.social_enabled_pdp_save');
                        } else {
                            var savedMsg = addBtnGrp.find('.social_enabled_quicklook_save');
                        }
                       
                        if (savedMsg.length){
                            $(savedMsg).show();
                        }
                        if(typeof Myntra.Data.MiniPIPsocialExclude === 'undefined'){
                            $('#mk-product-page .mk-save-for-later').blur();
                            addBtnGrp.append("<div class='saved-message social_enabled_pdp_save'><span></span>SAVED IN WISHLIST</div>");
                            savedMsg = addBtnGrp.find('.social_enabled_pdp_save');
                        } else {                                            //$('#minipip-product-guide .mk-save-for-later').hide();
                            $('#minipip-product-guide .mk-save-for-later').blur();
                            addBtnGrp.append("<div class='saved-message social_enabled_quicklook_save'><span></span>SAVED IN WISHLIST</div>");
                            savedMsg = addBtnGrp.find('.social_enabled_quicklook_save');
                        }
                         if (addBtnGrp.find('.call-to-buy').length > 0) {
                            savedMsg.css({'top': '70px'});
                        }
						
                        savedMsg.css({'top': __toppos+'px','left':__leftpos+'px'});
                        setTimeout(function() {
                                savedMsg.remove();
                        }, 5000);
                    } else {
                        var savedMsg = addBtnGrp.find('.social_enabled_pdp_save');
                        if (savedMsg.length){
                            $(savedMsg).show();
                        }
                        addBtnGrp.append("<div class='saved-message'><span></span>SAVED </div>");
                        savedMsg = addBtnGrp.find('.saved-message');
                        savedMsg.css({'top': __toppos+'px','left':__leftpos+'px'});
                    }
                    setTimeout(function() {
                    	savedMsg.fadeOut('slow');
                    }, 4000);
				}
				else {
					window.location=document.URL;
				}
				if($.trim(data) == 'saved'){
					Myntra.Header.setCount('saved',Myntra.Header.getCount('saved')+1);
				}
                //Kuliza Code end

                //Old code by Myntra. So we can remove it in case not used anywhere else.

//				FB.getLoginStatus(function(response) {
//					if(response.status === 'connected') {
//						$(document).trigger('myntra.fb.wishlist.post',{href:saveObj.url,exSocial:saveObj.excludeSocial,miniPIP:saveObj.isMini});
//					} else {
//						if(saveObj.isMini){ 
//							if(!Myntra.Data.MiniPIPsocialExclude){
//								Myntra.Data.MiniSocialShareUrl=saveObj.url;
//								if(addBtnGrp.find('.wishlist-fb-login').length){
//									addBtnGrp.find('.wishlist-fb-login').show();
//								} else{
//									if(social_share_variant=='test'){
//										addBtnGrp.append('<div class="wishlist-fb-login"><em></em><span class="share-yes">SHARE YOUR WISHLIST</span></div>')
//									}
//								}
//							}
//						}
//						else if(!Myntra.Data.PDPsocialExclude){
//							if(addBtnGrp.find('.wishlist-fb-login').length){
//								addBtnGrp.find('.wishlist-fb-login').show();
//							} else {
//								if(social_share_variant=='test'){
//									addBtnGrp.append('<div class="wishlist-fb-login"><em></em><span class="share-yes">SHARE YOUR WISHLIST</span></div>')
//								}
//							}
//						}			
//					}
//				});
			}
		});
	};

	//SAVE ITEM ACTION
	$('.mk-save-for-later').live('click',function(){
		var sizeDropCont = $(this).closest('.mk-product-option-cont'),
			formToSubmit = $(this).closest('.mk-product-guide').find('.add-to-cart-form'),
			sku_;
		var quantity = formToSubmit.find(":hidden[name='quantity']").val(),
		pUrl = sizeDropCont.parent().find('.mk-more-info').length ? sizeDropCont.parent().find('.mk-more-info').attr('href') : window.location.href;
		if(sizeDropCont.find('.mk-freesize').length){
			sku_=formToSubmit.find(":hidden[name='productSKUID']").val();;
		}
		else{
			sku_= sizeDropCont.find('.mk-size').val();
		}

		if(sizeSelected(sizeDropCont)){
			if(!Myntra.Data.userEmail){
				$(document).trigger('myntra.login.show', {onLoginDone: function() {
                    Myntra.Utils.Cookie.set('_sku',sku_);
                    Myntra.Utils.Cookie.set('_url',pUrl);
                    Myntra.Utils.Cookie.set('_saveitem',1);
                    location.reload();
                },invoke:'wishlist'});
			}
			else {
			//AJAX SAVE FOR LATER
				var isMiniPIP=($(this).hasClass('miniPIP-wishlist'))?true:false;
				var exclude=false;
	   	    	if(typeof Myntra.Data.MiniPIPsocialExclude != 'undefined'){
	   	       		exclude=Myntra.Data.MiniPIPsocialExclude;
	   	    	}
	   	    	else{
	   	    		exclude=Myntra.Data.PDPsocialExclude;
	   	    	}
	   	    	
				Myntra.saveItemAsync({sku:sku_,qty:quantity, url:pUrl,excludeSocial:exclude, sizeCont:sizeDropCont,isMini:isMiniPIP});
			}
		}
		else{
            //sizeDropCont.find('.mk-size-drop .jqTransformSelectOpen').click();
			sizeDropCont.find('.flat-size-options .options').addClass('error');
		}
	}).live("mouseenter", function() {
		if(!sizeSelected($(this).closest('.mk-product-option-cont'))) {

			$(this).closest('.add-button-group').find('.mk-size-error').css('top', ($(this).position().top-2)+'px').show();
		}
	}).live("mouseleave", function() {
		$(this).closest('.add-button-group').find('.mk-size-error').hide();
	});
});


//PDP SIZE TOOL TIP
Myntra.Utils.SizesTooltip = (function(){
	var obj = {};
	var xOffset=0;
	var yOffset=0;
	var text="";
	var top=0;
	var left=0;

	obj.InitTooltip = function(){
		$(".vtooltip").hover(
				function(e){
					text = $(this).attr("data-tooltipText");
		            yOffset = $(this).attr("data-tooltipTop");
					var elProp = $(e.target).offset();
		            top = (elProp.top - yOffset);
		            left = (elProp.left -30);
		            //PORTAL-2712: || replaced by &&
		            if(text != "" && typeof text != "undefined"){
						$('body').append( '<div id="vtooltip" class="myntra-tooltip">' + text + '</div>' );
		            	$('div#vtooltip').css("top", top+"px").css("left", left+"px").fadeIn("fast");
		            }
				},
				function(e){
					$("div#vtooltip").fadeOut("fast").remove();
				}
		);

	};
	return obj;
})();
