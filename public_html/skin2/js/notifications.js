
Myntra.Notifications = (function(){
	function notifications(){
		var _this = this,
			newNotificationCount,
			notificationCount,
			popUpActive = false,
			notificationsLoaded = false,
			notificationData,
			popUpElement = $('#mk-notification-popup'),
			notifTooltip = $('#notif-tooltip'),
			moreText,
			nextViewNumber,
			visibleCount = 0,
			firstClick = false,
			scrollElement,
			unitView = 4,
			markReadArr = [],
			queueMarkRead;

		_this.init = function(){
			if(Myntra.Data.notificationCount==''){
				notificationCount = 0;
				newNotificationCount = 0;
			}
			else{
				var temp = parseInt(Myntra.Data.notificationCount,10);
				if(temp == -1){
					notificationCount = 0;
					newNotificationCount = 0;		
				}
				else{
					notificationCount = 1;
					newNotificationCount = temp; 
				}
			}
			_this.bindEvents();			
			
		};

		_this.bindEvents = function(){
			$('#notificationWrapper').on('mouseenter mouseleave',_this.openPopup);
			$('#mk-notification').on('mouseenter',_this.getNotifications);
			notifTooltip.find('.close').on('click', _this.hideTooltip);
			popUpElement.find('a.moreData').on('click',_this.showMoreData);
			popUpElement.find('a.viewAll').on('click',_this.showAllData);
			$(document).on('click',_this.hideOnOuterClick).on('keyup',_this.hideOnEscPress);
			popUpElement.find('#notifications').on('click',_this.deleteNotification).on('click',_this.fireGACallsForInternalClick);
			setTimeout(_this.hideTooltip, 4000);
		};

		_this.hideTooltip = function() {
			notifTooltip.siblings('.shim').show();
			notifTooltip.animate({'margin-right': -notifTooltip.outerWidth()}, {duration: 1000, complete: function() {
				notifTooltip.hide();
				$('.mk-ntf-last>.mk-hide').fadeIn();
			}});
		};

		_this.fireGACallsForInternalClick = function(evt){
			if($(evt.target).hasClass('ntfLink')){
				_gaq.push(['_trackEvent', 'notifications', 'notification_internal_link', $(evt.target).attr('title')]);				
			}			
		};

		_this.deleteNotification = function(evt){
			if ($(evt.target).hasClass('ntfDelete')) {
				var currElement = $(evt.target).parents('li'),
				notificationId = currElement.attr('data-notification-id');
				notificationCount-=1;
				visibleCount-=1;
				currElement.remove();
				if (!notificationCount) {
					_this.openPopup();
					$('#notificationWrapper').remove();
				} else {
					$('#mk-notification').find('span.newCount').text(notificationCount);
					if(scrollElement) scrollElement.reinitialise();
				}
				$.ajax({
					type: 'POST',
					url: '/notifications/deleteNotification.php',
					dataType: 'json',
					cache: false,
					data: { 'notificationId': notificationId }					
				});
				_gaq.push(['_trackEvent', 'notifications', 'delete_notification', currElement.find('h3').text()]);
			}			
		};

		_this.hideOnEscPress = function(evt){
			if(evt.keyCode == 27 && popUpActive){
				_this.openPopup();
			}
		};

		_this.showAllData = function(evt){
			popUpElement.find('#notifications li.mk-hide').not('.mockElement').removeClass('mk-hide');
			$(evt.target).hide();
			if(scrollElement){
				scrollElement.reinitialise();
			}
			else{
				scrollElement = popUpElement.jScrollPane({
					verticalGutter: 0
				});
				scrollElement = scrollElement.data('jsp');
			}
			_gaq.push(['_trackEvent', 'notifications', 'view_all_notifications']);
		};

		_this.hideOnOuterClick = function(evt){
			if(!$(evt.target).closest('#notificationWrapper').length && popUpActive && !$(evt.target).hasClass('ntfDelete')){
				_this.openPopup();
			}
		}

		_this.showMoreData = function(){
			var lastVisibleIndex = popUpElement.find('#notifications li').index(popUpElement.find('#notifications li.disp:last'))+1;
			markReadArr = [];
			for(var i=lastVisibleIndex; i<(lastVisibleIndex+nextViewNumber); i++){
				popUpElement.find('#notifications li').eq(i).removeClass('mk-hide').addClass('disp');
				visibleCount++;
				if(!notificationData[i-1].read){
					markReadArr.push(notificationData[i-1].id);
				}
			}
			_this.markRead();
			//_this.updateUnreadCount();
			_this.updateFooterLink();
			if(scrollElement){
				scrollElement.reinitialise();
			}
			else{
				scrollElement = popUpElement.jScrollPane({
					verticalGutter: 0
				});
				scrollElement = scrollElement.data('jsp');
			}
			_gaq.push(['_trackEvent', 'notifications', 'more_notifications']);
		};

		_this.markRead = function(){
			if(markReadArr.length){
				var updateCount = parseInt(Myntra.Data.newNotificationCount,10) - markReadArr.length;
				$.ajax({
					type: 'POST',
					url: '/notifications/markRead.php',
					dataType: 'json',
					cache: false,
					data: { 'notificationIds': markReadArr,
							'notificationCount' :  updateCount
					}					
				});
			}
			else{
				queueMarkRead = true;
			}			
		};

		_this.openPopup = function(){
			if(!popUpActive){
				$('#mk-notification-popup').show();
				$('#mk-notification .mk-arrow').show();
				if(!firstClick){
					firstClick = true;
					_this.markRead();
					//_this.updateUnreadCount();
					_this.updateFooterLink();
				}
				popUpActive = true;
				_gaq.push(['_trackEvent', 'notifications', 'notification_open_popup']);
			}
			else{
				$('#mk-notification-popup').hide();
				$('#mk-notification .mk-arrow').hide();
				popUpActive = false;
			}
		};

		_this.updateUnreadCount = function(){
			if(newNotificationCount>unitView){
				newNotificationCount-=unitView;
				if(newNotificationCount>unitView){
					nextViewNumber = unitView;
				}
				else{
					nextViewNumber = newNotificationCount;
				}
				$('#mk-notification').find('span.newCount').show();
			}
			else{
				newNotificationCount = 0;
				nextViewNumber = 0;
				$('#mk-notification').find('span.newCount').hide();
			}
			$('#mk-notification').find('span.newCount').text(newNotificationCount);				
		};

		_this.getNotifications = function(){
			if(!notificationsLoaded){
				notificationsLoaded = true;
				$.ajax({
					type: 'GET',
					url: '/notifications/getNotifications.php',
					dataType: 'json',
					cache: false,
					complete:function(data,status){
						if(status=='success'){
							notificationData = JSON.parse(data.responseText);
							notificationData = notificationData.data;
							if(notificationData){
								_this.prepareNotificationsView();								
							}
							else{
								_this.displayNoNotifications();
								_this.resetNotificationCount();
							}
						}					
					}
				});
			}
		};

		_this.displayNoNotifications = function(){
			popUpElement.find('.preloaderWrapper img').hide();
			popUpElement.find('.preloaderWrapper h1').show();
			popUpElement.css('width','180px');
		};

		_this.resetNotificationCount = function(){
			$.ajax({
				type: 'POST',
				url: '/notifications/resetNotificationsCount.php',
				cache: false
			});
		};

		_this.prepareNotificationsView = function(){
			notificationCount = notificationData.length;
			if(notificationCount){
				for(var i=0; i<notificationCount; i++){
					if(location.protocol == 'https:'){
						notificationData[i].image = notificationData[i].image.replace(cdn_base,secure_cdn_base);
					}
					popUpElement.find('ul li.mockElement').clone().appendTo(popUpElement.find('ul'));
					var currElement = popUpElement.find('ul li.mockElement:last');
					currElement.removeClass('mockElement');
					currElement.find('img').attr('src',notificationData[i].image);
					currElement.find('h3').text(notificationData[i].title);
					currElement.find('p').html(decodeURI(notificationData[i].message));
					currElement.find('p a').addClass('ntfLink').attr('title',notificationData[i].title);
					currElement.attr('data-notification-id',notificationData[i].id);
					if(!notificationData[i].read){
						currElement.addClass('unread');
					}						
					if(i < unitView){
						currElement.removeClass('mk-hide').addClass('disp');
						if(!notificationData[i].read){
							markReadArr.push(notificationData[i].id);
						}
						visibleCount++;						
					}
				}
				popUpElement.find('.mk-ntf-box').show();
				popUpElement.find('.preloaderWrapper').hide();
				moreText = popUpElement.find('a.moreData').text();
				_this.updateFooterLink();
				if(queueMarkRead){
					queueMarkRead = false;
					_this.markRead();
				}
			}	
		};

		_this.updateFooterLink = function(){
			var moreLink = popUpElement.find('a.moreData'),
				allLink = popUpElement.find('a.viewAll');
			if(visibleCount<notificationCount){
				if(nextViewNumber){
					moreLink.text(nextViewNumber+ ' ' + moreText);
					moreLink.show();
					allLink.hide();
				}
				else{
					moreLink.hide();
					allLink.show();
				}
			}
			else if(visibleCount == notificationCount){
				moreLink.hide();
				allLink.hide();
			}

		};	
		
	}

	return new notifications();

})();
$(document).ready(function(){
	if(Myntra.Data.userEmail!='' && Myntra.Data.notificationsEnabled){
		Myntra.Notifications.init();	
	}	
});