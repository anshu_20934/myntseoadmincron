(function ($) {

$.highestZIndex = function(sel){
	var index_highest = 0;
	$(sel).each(function(){
		var index_current = parseInt($(this).css("z-index"), 10);
		if(index_current > index_highest) {
			index_highest = index_current;
		}
	});
	return index_highest;
};

})(jQuery);