$(document).ready(function(){

    //to start with [a-z0-9_] and followed by anychar other than ~@$^
    var MFBtextRegEx = /^[\w]{1}[^\@\$\^\~]*$/;

    var questionWrapper = ".MFBQ_wrapper";

    $(questionWrapper).jqTransform();

    //trigger form submission on click of top button
    $('#MFB_trigger_submit').click(function(){
        $('#MFB_submit').trigger('click');
    });

    //bind all text,textboxes for mouseout event
    $.each(MFB,function(qId,qValue){

        //get all the options of id starting with MFB[qid]
        answerElements = $('[id^="MFB['+qId+']"]');
        
        if(qValue.q_type == 'text' || qValue.q_type == 'textbox'){

            $.each(answerElements,function(key, ele){

                $(ele).bind('blur', function() {

                    //validate content for maxchars and adjust
                    adjustTextInteractively(this,qId,qValue);

                    //validate for special chars
                    if(isValidAgainstRegex(MFBtextRegEx, $(ele).val())){
                        deHighlightMandatoryQuestion(qId);

                    } else {
                        highlightMandatoryQuestion(qId);
                        alert("Starting letter can only be a-z, 0-9 or _. And there should not be ~@$^ used.");
                    }

                });

                $(ele).bind('keydown keyup', function() {
                    adjustTextInteractively(this,qId,qValue);
                });
            });

        } else if(qValue.q_type == 'radio'){            

            $.each(answerElements,function(key, ele){
                $(ele).bind('click', function() {
                    highlightSelectedLabel($(ele));
                });
            });

        } else if(qValue.q_type == 'checkbox'){            

            $.each(answerElements,function(key, ele){
                $(ele).bind('click', function() {
                    if($(ele).attr('checked')){
                        highlightSelectedLabel($(ele));
                    } else {
                        deHighlightSelectedLabel($(ele));
                    }


                });
            });

        }

    });


    //validate MFB form on submission
    $('#MFB_submit').click(function(){

        var hasError=false;

        $.each(MFB,function(qId,qValue){

            //get all the options of id starting with MFB[qid]
            answerElements = $('[id^="MFB['+qId+']"]');
            var local_box_error=true;
            switch(qValue.q_type){

                case 'continuousradio'    :
                case 'radio'    :   if(qValue.mandatory==1){
                                        $.each(answerElements,function(key, ele){
                                            if($(ele).attr('checked')){
                                                local_box_error = false;
                                                deHighlightMandatoryQuestion(qId);                                                
                                                
                                                return false;//break
                                            }
                                        });

                                        if(local_box_error == true){
                                            highlightMandatoryQuestion(qId);
                                            hasError = true;

                                        }
                                    }
                                    break;

                case 'checkbox' :   if(qValue.mandatory==1){
                                        $.each(answerElements,function(key, ele){
                                            if($(ele).attr('checked')){
                                                local_box_error = false;
                                                deHighlightMandatoryQuestion(qId);
                                                //highlightSelectedLabel($(ele));
                                                
                                                return false;//break
                                            }
                                        });

                                        if(local_box_error == true){
                                            highlightMandatoryQuestion(qId);
                                            hasError = true;

                                        }
                                    }
                                    break;

                case 'text'     :   //assuming, always there would only be one textarea for a question
                                    $.each(answerElements,function(key, ele){

                                        if(qValue.mandatory==1){
                                            if(isValidAgainstRegex(MFBtextRegEx, $(ele).val()) && $(ele).val().length <= qValue.maxchars){
                                                local_box_error = false;
                                                deHighlightMandatoryQuestion(qId);
                                                return false;//break
                                            }
                                        } else if($(ele).val() != ''){
                                            if(isValidAgainstRegex(MFBtextRegEx, $(ele).val()) && $(ele).val().length <= qValue.maxchars){
                                                local_box_error = false;
                                                deHighlightMandatoryQuestion(qId);
                                                return false;//break
                                            }
                                        //in case of non-mandatory text on empty don't show error
                                        } else {
                                            local_box_error = false;
                                            return false;//break
                                        }


                                    });

                                    if(local_box_error == true){
                                        highlightMandatoryQuestion(qId);
                                        hasError = true;

                                    }

                                    break;

                case 'textbox'  :   //assuming, always there would only be one textbox for a question
                                    $.each(answerElements,function(key, ele){

                                        if(qValue.mandatory==1){
                                            if(isValidAgainstRegex(MFBtextRegEx, $(ele).val()) && $(ele).val().length <= qValue.maxchars){
                                                local_box_error = false;
                                                deHighlightMandatoryQuestion(qId);
                                                return false;//break
                                            }
                                        } else if($(ele).val() != ''){
                                            if(isValidAgainstRegex(MFBtextRegEx, $(ele).val()) && $(ele).val().length <= qValue.maxchars){
                                                local_box_error = false;
                                                deHighlightMandatoryQuestion(qId);
                                                return false;//break
                                            }
                                        //in case of non-mandatory textbox on empty don't show error
                                        } else {
                                            local_box_error = false;
                                            return false;//break
                                        }


                                    });

                                    if(local_box_error == true){
                                        highlightMandatoryQuestion(qId);
                                        hasError = true;

                                    }

                                    break;

            }
        });

        if(hasError){
            $('.MFBmsg').hide();
            alert("Please select all mandatory questions (marked as *) and check for correct text entered.")
            return false;

        } else {
            $('#MFB_form').trigger('submit');
        }

    });


    /* validates string against regEx
     * @param:(string)regEx
     * @param:(string)data
     * @return:(bool)
     */
    function isValidAgainstRegex(regEx, data){
        return regEx.test(data);
    }

    /* highlight error questions
     * @param:(int)questionId
     */
    function highlightMandatoryQuestion(qId){
        $('#MFBQ_'+qId+'').css({border:'1px solid red',padding:'10px',backgroundColor:'#FFFFFF'});
        $('#MFBQ_'+qId+'').find(".MFBQ").css({color:'#D14747'});
        $('.MFBerrMsg').html('Please answer all the mandatory fields '
                                +'(marked with an *) and take care of character limitations in order to submit your '
                                +'feedback. We appreciate your effort towards '
                                +'improving our service. ').show();
    }

    /* highlight non-error questions
     * @param:(int)questionId
     */
    function deHighlightMandatoryQuestion(qId){
        $('#MFBQ_'+qId+'').css({border:'none',padding:'10px',backgroundColor:'#F2F2F2'});
        $('#MFBQ_'+qId+'').find(".MFBQ").css({color:'#333333'});
    }

    /* highlight label of the selected radio
     * or check box
     */
    function highlightSelectedLabel(eleObj){

        // for radio
        if(eleObj.attr("type") == "radio"){            
            eleObj.parents("ul").find(".selected").removeClass("selected");
            eleObj.parent().next().addClass("selected");
        }

        // for checkbox
        if(eleObj.attr("type") == "checkbox"){
            if(!eleObj.parent().next().hasClass("selected")){
                eleObj.parent().next().addClass("selected");
            }
        }

    }

    /* de highlight a checkbox(required only 
     * for check box since can be checked/unchecked)
     */
    function deHighlightSelectedLabel(eleObj){
        if(eleObj.parent().next().hasClass("selected")){
            eleObj.parent().next().removeClass("selected");
        }
    }

    /* adjsut text(textarea) content for max chars and regEx validation
     * @param:(obj)textEle
     * @param:(int)questionId
     * @param:(obj)questionObj
     */
    function adjustTextInteractively(textEle,qId,qObj){

        var maxChars = parseInt(qObj.maxchars);
        var TextLength = parseInt($(textEle).val().length);
        var charsLeft = maxChars - TextLength;
        var upperLimit = 0;

        if(charsLeft <= 0){

            charsLeft = 0;
            upperLimit = maxChars-1;

            $(textEle).val($(textEle).val().substring(0,upperLimit));
            $(textEle).parents("table").next().text("max "+charsLeft+" chars");

            highlightMandatoryQuestion(qId);
            alert("Number of characters should not exceed "+qObj.maxchars+" characters" );

        } else {
            deHighlightMandatoryQuestion(qId);
            $(textEle).parents("table").next().text("max "+charsLeft+" chars");
        }
    }
});

