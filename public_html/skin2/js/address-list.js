
Myntra.AddressListBox = function(id, cfg) {
    cfg = cfg || {};
    cfg.isPopup = (typeof cfg.isPopup === 'undefined') ? true : cfg.isPopup;
    cfg.isModal = (typeof cfg.isModal === 'undefined') ? true : cfg.isModal;

    cfg.css = cfg.css || '';
    cfg.css += cfg.isPopup ? ' lightbox' : 'lightbox-nopop';

    var isInited = false, selectedAddress, addresses = {}, defAddrId, selAddrId,
        idstr = id.substr(0, 1) === '#' ? id.substr(1) : id,
        markupIsDefault = '<span class="is-default">Default</span>',
        markupSetDefault = '<span class="set-default"><button class="btn link-btn btn-set-default">Set as default</button></span>',
        obj,
        markup = [
        '<div id="' + idstr + '" class="address-list-box ' + cfg.css + '">',
        '<div class="mod">',
        '  <div class="hd">',
        '    <div class="title"></div>',
        '    <div class="subtitle"></div>',
        '  </div>',
        '  <div class="bd">',
        '    <button class="link-btn btn-create"></button>',
        '    <div class="list"></div>',
        '  </div>',
        '  <div class="ft">',
        '    <span class="note"></span>',
        '    <button class="btn normal-btn btn-save">Done</button>',
        '    <button class="btn normal-btn btn-cancel">Cancel</button>',
        '  </div>',
        '</div>',
        '</div>'
    ].join('');
    cfg.container ? $(cfg.container).html(markup) : $('body').append(markup);
    obj = Myntra.LightBox(id, cfg);
    obj.root = $(id);
    obj.set('ajax_url', '/myntra/' + (location.protocol === 'https:' ? 's_' : '') + 'ajax_address.php');

    var hd = $('.mod > .hd', obj.root),
        bd = $('.mod > .bd', obj.root),
        ft = $('.mod > .ft', obj.root),
        title = $('.mod > .hd > .title', obj.root),
        subTitle = $('.mod > .hd > .subtitle', obj.root),
        ftNote = $('.mod > .ft > .note', obj.root),
        btnSave = $('.btn-save', obj.root),
        btnCancel = $('.btn-cancel', obj.root),
        btnCreate = $('.btn-create', obj.root),
        list = $('.mod > .bd > .list', obj.root),
        items;

    obj.setTitle = function(markup) { title.html(markup) };
    obj.setSubTitle = function(markup) { subTitle.html(markup) };
    obj.setBody = function(markup) { bd.html(markup) };
    obj.setFootNote = function(markup) { ftNote.html(markup) };

    obj.enableSave = function() { btnSave.removeAttr('disabled') };
    obj.disableSave = function() { btnSave.attr('disabled', 'disabled') };
    obj.showSave = function() { btnSave.removeClass('hide') };
    obj.hideSave = function() { btnSave.addClass('hide') };
    obj.setSaveText = function(txt) { btnSave.text(txt) };

    if (!cfg.isPopup) {
        hd.remove();
        ft.remove();
        btnCreate.remove();
    }

    obj.buildAddrMarkup = function(address) {
        var tpl = [
        '   <td class="address">',
        '       <strong>{name}</strong>',
        (!cfg._editdelete && address.default_address == 1 ? ' <span class="lbl">(DEFAULT ADDRESS)</span>' : ''),
        '       <br>{address}<br>',
        '       {locality}',
        '       {city} - {pincode}<br>',
        '       {state}<br>',
        '       <span class="lbl">Mobile:</span> {mobile}',
        '   </td>'
        ].join('');

        function htmlEntities(str) {
            return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        }
        var markup = tpl.replace(/\{(.*?)\}/g, function(match, s1) {
            if (s1 == 'address') {
                return htmlEntities(address[s1]).replace('\n', '<br>');
            }
            else if (s1 == 'locality') {
                return htmlEntities(address[s1])? (htmlEntities(address[s1])+'<br>') : '';
            }
            return htmlEntities(address[s1]);
        });

        return markup;
    }

    obj.buildItem = function(address, css) {
        var actMarkup = '',
            defMarkup = '';

        if (+address.default_address) {
            defMarkup = markupIsDefault;
            defAddrId = address.id;
        }
        else {
            defMarkup = markupSetDefault;
        }

        if (cfg._editdelete) {
            actMarkup = [
            '<td class="action">',
            '   <span class="buttons">',
                defMarkup,
            '   <button class="btn link-btn btn-edit">Edit</button>',
            '   <span class="sep">/</span>',
            '   <button class="btn link-btn btn-delete">Remove</button>',
            '   </span>',
            '</td>'
            ].join('');
        }

        var addrMarkup = obj.buildAddrMarkup(address),
            selMarkup = (address.is_servicable && address.id == selAddrId) ? ' checked' : '',
            disMarkup = address.is_servicable ? '' : ' disabled',
            markup = [
        '<tr data-id="' + address.id + '" class="', css, '">',
        '   <td class="opt' + (address.is_servicable ? '' : ' disabled') + '">',
        '       <input type="radio" name="address-sel" value="' + address.id + '"' + disMarkup + selMarkup + '>',
        '   </td>',
            addrMarkup,
            actMarkup,
        '</tr>'
        ].join('');

        if (address.is_servicable && address.id == selAddrId) {
            $(document).trigger('myntra.address.list.select', address);
        }

        return markup;
    };

    obj.buildList = function(ids) {
        var markups = [], count = 0, css,
            i, j, n,
            tip = $('<div class="myntra-tooltip">Address not serviceable</div>').appendTo('body');

        list.html('');
        items = $('<table class="items"></table>').appendTo(list);
        for (j = 0, n = ids.length; j < n; j += 1) {
            i = ids[j];
            count += 1;
            css = count % 2 == 0 ? 'odd' : 'even';
            markups.push(obj.buildItem(addresses[i], css));
        }
        items.html(markups.join(''));
        $('tr', items).click(function(e) { obj.onItemClick(e) });

        items.jqTransform();
        var disabledItems = items.find(':disabled').siblings('.jqTransformRadio');
        Myntra.InitTooltip(tip, disabledItems, {side:'below'}); //, element:'input[type="radio"]'});

        if (disabledItems.length === count) {
            obj.setSubTitle('Sorry! None of these addresses are serviceable.');
            obj.hideSave();
        }
        else {
            obj.setSubTitle('&nbsp;');
            obj.showSave();
        }
    };

    btnCreate.click(function(e) {
        var conf = {_type:cfg._type, _action:'create', isModal:true, itemid: cfg.itemid};
        $(document).trigger('myntra.address.form.show', conf);
    });

    btnSave.click(function(e) {
        obj.hide();
	if(cfg._type == 'return'){
	
        $(document).trigger('myntra.address.list.done.return', selectedAddress);
	}
	else if(cfg._type == 'exchange'){

        $(document).trigger('myntra.address.list.done.exchange', selectedAddress);
	}
	else{

        $(document).trigger('myntra.address.list.done', selectedAddress);
	} 
    });

    btnCancel.click(function(e) {
        obj.hide();
    });

    $(document).bind('myntra.address.form.done', function(e, resp) {
        if (resp._action == 'create') {
            addresses[resp.data.id] = resp.data;
            items ? obj.addItem(resp.data) : obj.buildList([resp.data.id]);
            // if we come from return wizard, just notify back and close
            if (cfg._type == 'return'){
                obj.hide();
                $(document).trigger('myntra.address.list.done.return', resp.data);
            }
	    else if(cfg._type == 'exchange'){
		obj.hide();
                $(document).trigger('myntra.address.list.done.exchange', resp.data);
	    }
            else {
                obj.center();
            }
        }
        else if (resp._action == 'edit') {
            obj.updateItem(resp.data);
        }
    });

    obj.addItem = function(data) {
        var firstItem = items.find('tr').first(),
            css = firstItem.hasClass('odd') ? 'even' : 'odd',
            markup = obj.buildItem(data, css),
            item = $(markup).insertBefore(firstItem);

        item.jqTransform();
        // note: only serviceable address can be added. so, we can safely show the save button
        obj.showSave();
        obj.setSubTitle('&nbsp;');
        item.click(function(e){ obj.onItemClick(e) });
    };

    obj.updateItem = function(data) {
        var markup = obj.buildAddrMarkup(data),
            item = items.find('tr[data-id="' + data.id + '"] > td.address');
        item.replaceWith(markup);
        //item.click(function(e){ obj.onItemClick(e) });
    };

    obj.deleteItem = function(addrId) {
        var item = items.find('tr[data-id="' + addrId + '"]');
        item.animate({opacity:0}, 600, function() {
            // store the pointer to the previous item
            var pitem = item.prev();

            item.remove();
            if (selectedAddress && selectedAddress.id == addrId) {
                selectedAddress = null;
                obj.disableSave();
            }

            // flip the zebra bg for the rest of items
            if (pitem) {
                var cnt = 0;
                while (pitem = pitem.next()) {
                    pitem.hasClass('odd')
                        ? pitem.removeClass('odd').addClass('even')
                        : pitem.removeClass('even').addClass('odd');
                    // to avoid getting into infinite loop
                    cnt += 1;
                    if (cnt > 100) {
                        break;
                    }
                }
            }
        });
    };

    obj.setDefaultAddress = function(addrId) {
        var itemNew = items.find('tr[data-id="' + addrId + '"]'),
            itemOld = items.find('tr[data-id="' + defAddrId + '"]');

        itemNew.find('.set-default').replaceWith(markupIsDefault);
        defAddrId = addrId;
        itemOld.find('.is-default').replaceWith(markupSetDefault);
        $(document).trigger('myntra.address.list.ondefaultaddress', addresses[defAddrId]);
    };

    obj.onItemClick = function(e) {
        var addrId = $(e.currentTarget).attr('data-id'),
            target = $(e.target);
        switch (e.target.nodeName.toLowerCase()) {
            case 'button':
                if (target.hasClass('btn-edit')) {
                    var conf = {_type:cfg._type, _action:'edit', id:addrId};
                    $(document).trigger('myntra.address.form.show', conf);
                }
                else if (target.hasClass('btn-delete')) {
                    var params = {_view:'address-delete', id:addrId, _token:Myntra.Data.token};
                    obj.ajax(params, 'delete', 'POST');
                }
                else if (target.hasClass('btn-set-default')) {
                    var params = {_view:'address-set-default', id:addrId, _token:Myntra.Data.token};
                    obj.ajax(params, 'set-default', 'POST');
                }
                break;
            case 'a':
                if (target.hasClass('jqTransformRadio') && !target.siblings('input:radio').is(':disabled')) {
                    selectedAddress = addresses[addrId];
                    obj.enableSave();
                    $(document).trigger('myntra.address.list.select', selectedAddress);
                }
                break;
            default:
            	$(target).siblings('.opt').find('a.jqTransformRadio').click();
            	break;
        }
    };

    obj.onAjaxSuccess = function(resp, name) {
        if (name === 'init') {
            isInited = true;
            obj.disableSave();
            if(cfg.hideCreateNewAddress){
               btnCreate.remove();
            }
            else {
                cfg._type === 'return' ? btnCreate.text('Create a pickup address') : (cfg._type === 'exchange' ? btnCreate.text('Create an exchange address'):btnCreate.text('Create a shipping address')); 
            }

            //obj.setFootNote('Need help? ' + resp.customerSupportTime + ' on <em>' + resp.customerSupportCall + '</em>');
            obj.setFootNote('');
            if (resp.count > 0) {
                addresses = resp.addresses;
                selAddrId = resp.selAddrId;
                obj.buildList(resp.ids);
                obj.center();
            }
            else {
                list.html('<div class="no-data">There is no address</div>');
            }
            $(document).trigger('myntra.address.list.oninit', {count:resp.count});
        }
        else if (name === 'delete') {
            obj.deleteItem(resp.id);
        }
        else if (name === 'set-default') {
            obj.setDefaultAddress(resp.id);
        }
    };

    var p_show = obj.show;
    obj.show = function(a_cfg) {
        $.extend(cfg, a_cfg);
        if(cfg.displayName){
            obj.setTitle('Select an address for '+ cfg.displayName);
        }
        else {
            obj.setTitle('Select an address for ' + (cfg._type === 'return' ? 'pickup' : 'shipping'));    
        }
        
        obj.setSubTitle('&nbsp;');
        p_show.apply(obj, arguments);
        var params = {_view:'address-list', _type:cfg._type, itemid: cfg.itemid};
        if(cfg.hideCreateNewAddress){
               btnCreate.remove();
        }
        else {
                cfg._type === 'return' ? btnCreate.text('Create a pickup address') : (cfg._type === 'exchange' ? btnCreate.text('Create an exchange address'):btnCreate.text('Create a shipping address')); 
        }

        !isInited && obj.ajax(params, 'init');
    };

    obj.render = function(a_cfg) {
        $.extend(cfg, a_cfg);
        var params = {_view:'address-list', _type:cfg._type};
        !isInited && obj.ajax(params, 'init');
    };

    return obj;
};

$(document).ready(function() {
    var obj,obj_return,obj_exchange;
    $(document).bind('myntra.address.list.show', function(e, cfg) {
	if(cfg._type == 'return'){
	    obj_return = obj_return || Myntra.AddressListBox('#lb-address-list-return', cfg);
            obj_return.show(cfg);
	}
	else if(cfg._type == 'exchange'){
	    obj_exchange = obj_exchange || Myntra.AddressListBox('#lb-address-list-exchange', cfg);
            obj_exchange.show(cfg);
	}
	else{
	    obj = obj || Myntra.AddressListBox('#lb-address-list', cfg);
            obj.show(cfg);
	}
    });
    $(document).bind('myntra.address.list.render', function(e, cfg) {
        obj = obj || Myntra.AddressListBox('#lb-address-list', cfg);
        obj.render(cfg);
    });
});

