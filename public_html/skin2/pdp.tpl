{extends file="layout.tpl"}

{block name=baseHttpLoc}
	<base href="{$http_location}/">
{/block}

{block name=navigation}
	{if $pdpMiniNav && $layoutVariant neq 'test'}		
		{include file="inc/navigation-collapsed.tpl"}
	{else}
		{include file="inc/navigation-default.tpl"}		        
	{/if}
{/block}

{block name=body}
<script>
Myntra.Data.pageName="pdp";
var avail_dynamic_param='{$avail_dynamic_param}';
var cross_sell_avail_dynamic_param1='{$cross_sell_avail_dynamic_param1}';
var cross_sell_avail_dynamic_param2='{$cross_sell_avail_dynamic_param2}';
var curr_style_id = "{$productStyleId}";
var brand = "{$brandname}";
var gender = "{$gender}";
var ageGroup = "{$agegroup}";
var article_type = "{$articleType}";
var allProductOptionDetails={$allProductOptionDetails};
var vatEnabled = {if $vatEnabled}true{else}false{/if};
var vatCouponMessage = "";
var vatThreshold = "";
var showVatMessage = {if $showVatMessage}true{else}false{/if};
if (vatEnabled){

    vatCouponMessage = "{$vatCouponMessage}";
    vatThreshold = "{$vatThreshold}";
}
Myntra.Data.PDPsocialExclude={if $socialExclude}true{else}false{/if};
Myntra.Data.socialAction='wishlist';
Myntra.PDP = Myntra.PDP || {};
Myntra.PDP.showExtraRec = '{$crossSell}' == 'test' ? true : false;
//Kuliza Code start
Myntra.Data.pdp_page_product_image_url = '{$area_icons[0].image}';
//Kuliza Code end
Myntra.PDP.showCouponWidget = {if $pdpdCouponWidgetEnabled}true{else}false{/if};
Myntra.PDP.couponWidgetABTest = '{$pdpCouponWidgetABTest}';
Myntra.PDP.couponWidgetTitleText = '{$pdpCouponWidgetTitleText}';

/*PDPV1 START*/
Myntra.PDP.PDPV1 = {if $pdpv1 eq test}true{else}false{/if};
Myntra.PDP.PDPV1Phase2 = {if $pdpv1_phase2 eq test}true{else}false{/if};
Myntra.PDP.contentServiceHost = '{$contentServiceHost}';

/*PDPv1 END*/

Myntra.PDP.Data = {
    id : "{$productStyleId}",
    vendorId : "{$style_properties.article_number}",
    name : "{$productStyleLabel}",
    brand : "{$style_properties.global_attr_brand}",
    articleType : "{$catalogData.articletype}",
    subCategory : "{$catalogData.sub_category}",
    category : "{$catalogData.master_category}",
    gender : "{$gender}",
    image : "{$style_properties.default_image}",
    searchImage : "{$style_properties.search_image}",
    price : "{$originalprice}",
    discount : "{$discountamount}",
    cleanURL : "{$pageCleanURL}",
    inStock : "{$inStock}",
    discountId : "{$discountId}",
    isStyleDisabled: "{$disableProductStyle}",
    masterCategory: "{$masterCategory}",
    ageGroup : "{$agegroup}",
    articleTypeId: "{$articleTypeId}"
}

//google conversion analytics track events
var gaEventCategory = Myntra.PDP.Data.category ? Myntra.PDP.Data.category : '',
    gaEventArticleType = Myntra.PDP.Data.articleType ? Myntra.PDP.Data.articleType : '',
    gaEventGender = Myntra.PDP.Data.gender ? Myntra.PDP.Data.gender : '',
    gaEventBrand = Myntra.PDP.Data.brand ? Myntra.PDP.Data.brand : '';

</script>
{if $LocationBreadcrumbABTest eq 'test'}		
<div class="mk-pdp-carousel-breadcrumbs mk-location-breadcrumbs {if $pdpv1 eq 'test'} mk-pdpv1-carousel-breadcrumbs{/if}">{$locationBasedBreadCrumb}</div>
{/if}
{if $pdpv1 eq 'test'}<div class="p-code">Product Code: {$productStyleId}</div>{/if}
<section class="mk-site-main">
	<div id="top-right" class="go-to-top-btn right mk-hide"></div>
	<div class="mk-cf">
        {pdprichsnippets macrosObj=$macrosObj}
		<div id="mk-product-page" class="mk-product-page mk-f-left {if $pdpv1 eq 'test'} mk-pdpv1-product-page{/if}" {$rs_itemscope_product_start}>
			<div class="mk-product-shop mk-cf">
				<div class="mk-product-media mk-f-left">
					<div class="mk-more-views rs-carousel mk-f-left"> <!-- Product Thumbs -->
						<ul class="rs-carousel-runner">
							{assign var=i value=0}
	                        {section name="area" loop=$area_icons}
	                     	   <li class="rs-carousel-item {if $i==0}img-selected{/if}"><a title="" href='{myntraimage key="style_1080_1440" src=$area_icons[area].image|replace:"_images":"_images_1080_1440"}' {if $zoomenabled}class="cloud-zoom-gallery" rel="useZoom: 'zoom1'"{/if}>
	                     		   <img width="48" height="64"  title="" src='{if $smarty.section.area.iteration == 1}{myntraimage key="style_48_64" src=$area_icons[area].image|replace:"_images":"_images_48_64"}{else}{$cdn_base}/skin2/images/spacer.gif{/if}'  {if $smarty.section.area.iteration > 1}data-lazy-src='{myntraimage key="style_48_64" src=$area_icons[area].image|replace:"_images":"_images_48_64"}' class="lazy  mk-lazy-white-border"{/if} alt='{myntraimage key="style_360_480" src=$area_icons[area].image|replace:"_images":"_images_360_480"}' data-src='{myntraimage key="style_1080_1440" src=$area_icons[area].image|replace:"_images":"_images_1080_1440"}'>
	                     		   </a>
								</li>
							{assign var=i value=$i+1}
	                        {/section}
						</ul>
					</div><!-- End .more-views -->
					<div class="mk-product-large-image {if $zoomenabled}zoom-enabled{/if}">
						{if $visualtagText}
						<div class="mk-visual-tag-pdp mk-visual-tag-{$visualtagCssClass}"><div>{$visualtagText}</div></div>
						{/if}
						<span class="prev-next-btns mk-hide" id="view-prev-pdp"></span>
						<span class="prev-next-btns mk-hide" id="view-next-pdp"></span>
						<a class="cloud-zoom-cont {if $zoomenabled}cloud-zoom{/if}" id="zoom1" href="{myntraimage key="style_1080_1440" src=$defaultImage|replace:'_images':'_images_1080_1440'}" {if $zoomenabled}rel="position: 'right' , showTitle: false, adjustX:15, adjustY:0,zoomWidth:543, zoomHeight:478" {/if}  alt="{$productDisplayName|escape:'htmlall'}"  ><img {$rs_itemprop_image} width="360" height="480" src="{myntraimage key="style_360_480" src=$defaultImage|replace:'_images':'_images_360_480'}" /></a> <!-- Product Image -->
					</div>
	
					{if $agegroup|lower eq 'adults-unisex'}
						{assign var=genderLinkinParam value="Men:Women"}
					{elseif $agegroup|lower eq 'kids-unisex'}
						{assign var=genderLinkinParam value="Boys:Girls"}
					{else}
						{assign var=genderLinkinParam value=$gender}
					{/if}
			 		<ul class="bread-crumb-bottom mk-f-right" >
			 			<li><a href="{$http_location}/{$articleType|lower|replace:' ':'-'}?f=gender:{$genderLinkinParam|lower}::brands:{$brandname|escape:'url'}#!gender={$genderLinkinParam}|brands={$brandname}" onClick="_gaq.push(['_trackEvent', 'pdp', 'breadcrum', '{$articleType}']);return true;">More {$articleType} FROM <span {$rs_itemprop_brand}>{$brandname}</span></a></li>
			 			<li><a href="{$http_location}/{$brandname|lower|replace:' ':'-'}?f=gender:{$genderLinkinParam|lower}#!gender={$genderLinkinParam}" onClick="_gaq.push(['_trackEvent', 'pdp', 'breadcrum', '{$brandname}']);return true;" class="mk-last">All Products from {$brandname}</a></li>
			 		</ul>

					<div class="mk-share-links mk-f-left">
						{if $pdpSocial}
							<input type="hidden" name="pageURL" id="pageURL" value="{$pageCleanURL}"></input>
							<div class="gm"><a href="https://mail.google.com/mail/?view=cm&ui=2&fs=1&tf=1"></a></div>
		 					<div class="fb"></div>
		 					<div class="tw"></div>
		 					<div class="gp"></div>
		 					<div class="pt"></div>
	 					{/if}
					</div>
				</div> <!-- End .product-media -->

				{*This block is needed here because cashback should be defined outside the pdp-details.tpl file so that the value can be used in the popup*}
				{if $discounttype eq 'rule'}
				    {if $discountamount eq ''}{assign var="discountamount" value="0"}{/if}
					{math equation="(x - d )* y " x=$originalprice d=$discountamount y=0.1  assign="cashback"}
				{else}
				   	{math equation="x * y" x=$originalprice y=0.1  assign="cashback"}
				{/if}
	
				<!-- pdp-details.tpl -->
				{include file="pdp/pdp-details.tpl"}
	
			</div> <!-- End .product-shop -->
			{if $pdpv1 eq 'test'}
			{***CUSTOMER PROMISES DIV HERE/HIDE TILL PHASE 2***}
			<div class="mk-customer-promise mk-guarantee mk-hide">
				<ul>
				{if $brand_logo neq ""}
					<li class="mk-brand-logo"><a href="{$http_location}/{$brandname|lower|replace:' ':'-'}" title="{$brandname}"><img src="{$brand_logo}" width="120" height="70"></a></li>
						{/if}
					<li class="mk-guarantee">100% Original Products</li>
					<li class="mk-free-shipping link">Free Delivery on orders over R<em>s</em>. {$free_shipping_amount}</li>
					<li class="mk-cod link">Cash on Delivery</li>
					<li class="mk-return link">30-Day Returns</li>					
				</ul>
			</div>
				
			<div class="mk-pdpv1-description">
				{if $style_note|trim || $descriptionAndDetails|trim || $materials_care_desc|trim || $size_fit_desc|trim}
					<ul class="mk-pdpv1-product-helper mk-cf">
						<li class="desc-main-title">Product Description</li>
			 				{if $style_note|trim neq ""}
						<li class="desc-sub-title">Style Note</li>
						<li class="desc-sub-text " {$rs_itemprop_description}>{$style_note}</li>
			 				{/if}
			 				{if $descriptionAndDetails|trim neq ""}
						<li class="desc-sub-title ">Product Details</li>
						<li class="desc-sub-text">{$descriptionAndDetails}</li>
			 				{/if}
			 				{if $materials_care_desc|trim neq ""}
						<li class="desc-sub-title ">Material & Care</li>
						<li class="desc-sub-text ">{$materials_care_desc}</li>
			 				{/if}
			 				{if $size_fit_desc|trim neq ""}
						<li class="desc-sub-title">Size & Fit</li>
						<li class="desc-sub-text">{$size_fit_desc}</li>
			 				{/if}
					</ul>
					{/if}
			</div>

			{/if}
	
		</div> <!-- End .product-page -->

		<div class="mk-product-sidebar mk-f-right">
			
			{if !$pdpdCouponWidgetEnabled}
				{if $myntrashoppingfestCoupons and $festPdpText}
            	<div id="fest-pdp-text" class="fest-pdp-text">
                	{$festPdpText}
            	</div>
            	{/if}
            {/if}
            
			<div class="mk-guarantee {if $pdpv1 eq 'test'}mk-hide{/if}">
				<ul>
					{if $brand_logo neq ""}
					<li class="mk-brand-logo"><a href="{$http_location}/{$brandname|lower|replace:' ':'-'}" title="{$brandname}"><img src="{$brand_logo}" width="120" height="70"></a></li>
					{/if}
					<li class="mk-guarantee">100% Original Products</li>
					<li class="mk-free-shipping link">Free Delivery*</li>
					<li class="mk-cod link">Cash on Delivery</li>
					<li class="mk-return link">30-Day Returns</li>					
				</ul>
			</div> <!-- End .guarantee -->
			
			{if $pincodeWidgetVariant eq "test" && !$disableProductStyle && $sizeOptionSKUMapping}
				<div class="delivery-network">
					<div class="truck-icon"></div>
					<div class="changeable ul">
						Check Delivery Time &<br />Cash on Delivery Availability
					</div>
				</div>
			{/if}
			{if $showVatMessage}
                                <div class="vat-message">{$vatMessage}</div>
                        {/if}
			<!--START LOOK BLOCK HERE-->
			<!--div class="mk-right-look-widget">
				<h5>COMPLETE LOOK</h5>
				<img class="look-image-small" src="http://myntra.myntassets.com/images/style/properties/Nike-Men-Black-T-shirt_493fbcbb28986ccf369599082e5f1347_images_96_128_mini.jpg" />
				
				<div class="look-items">
                	<ul>
                		<li class="look-item">  
                			<span href="javascript:void(0)" data-href="http://www.myntra.com/lifestyle/ETC/ETC-Men-Black-Brown-METRO-TOLEDO-Reversible-BELT/130626/buy" data-styleid="130626" data-widget="header_saved" class="quick-look" style="display: none;"></span> 
                			<a href="http://www.myntra.com/lifestyle/ETC/ETC-Men-Black-Brown-METRO-TOLEDO-Reversible-BELT/130626/buy" title="ETC Men Black &amp; Brown METRO TOLEDO Reversible BELT">
                			   	<div class="mk-product-image"> 
                			   	        <img alt="ETC Men Black &amp; Brown METRO TOLEDO Reversible BELT" src="http://myntra.myntassets.com/images/style/properties/ETC-Men-Black-&amp;-Brown-METRO-TOLEDO-Reversible-BELT_31ca906221007d1b81959101c76d9fb0_images_96_128_mini.jpg">
                		        </div>
                		    </a>
                		    <p class="brand-text">ADIDAS</p>
                		    <p class="price-text red">
								Rs.539 <span class="strike gray"> 899.00</span>
							</p>
							<span class="price-text red">
		                		<em>10% OFF *</em>  
		            		</span>
                		    
                		</li>
                		<li class="look-item">  
                			<span href="javascript:void(0)" data-href="http://www.myntra.com/lifestyle/ETC/ETC-Men-Black-Brown-METRO-TOLEDO-Reversible-BELT/130626/buy" data-styleid="130626" data-widget="header_saved" class="quick-look" style="display: none;"></span> 
                			<a href="http://www.myntra.com/lifestyle/ETC/ETC-Men-Black-Brown-METRO-TOLEDO-Reversible-BELT/130626/buy" title="ETC Men Black &amp; Brown METRO TOLEDO Reversible BELT">
                			   	<div class="mk-product-image"> 
                			   	        <img alt="ETC Men Black &amp; Brown METRO TOLEDO Reversible BELT" src="http://myntra.myntassets.com/images/style/properties/ETC-Men-Black-&amp;-Brown-METRO-TOLEDO-Reversible-BELT_31ca906221007d1b81959101c76d9fb0_images_96_128_mini.jpg">
                		        </div>
                		    </a>
                		    <p class="brand-text">ADIDAS</p>
                		    <p class="price-text red">
                		    	<span class="rupees"> Rs.599 </span>
                		   	</p>
                		   	
                		</li>
                		<li class="look-item">  
                			<span href="javascript:void(0)" data-href="http://www.myntra.com/lifestyle/ETC/ETC-Men-Black-Brown-METRO-TOLEDO-Reversible-BELT/130626/buy" data-styleid="130626" data-widget="header_saved" class="quick-look" style="display: none;"></span> 
                			<a href="http://www.myntra.com/lifestyle/ETC/ETC-Men-Black-Brown-METRO-TOLEDO-Reversible-BELT/130626/buy" title="ETC Men Black &amp; Brown METRO TOLEDO Reversible BELT">
                			   	<div class="mk-product-image"> 
                			   	        <img alt="ETC Men Black &amp; Brown METRO TOLEDO Reversible BELT" src="http://myntra.myntassets.com/images/style/properties/ETC-Men-Black-&amp;-Brown-METRO-TOLEDO-Reversible-BELT_31ca906221007d1b81959101c76d9fb0_images_96_128_mini.jpg">
                		        </div>
                		    </a>
                		    <p class="brand-text">ADIDAS</p>
                		    <p class="price-text red">
                		    	<span class="rupees"> Rs.599 </span>
                		   	</p>
                		   	
                		</li>
                
                	</ul>
                </div>
				
			</div>--><!--END LOOK HERE-->
			
		</div> <!-- End .product-sidebar -->
		{include file="pdp/pdp-zoom.tpl"}
	</div>
	{if $pdpv1 eq 'test'}
		<div class="pdpv1-look-cont"></div>
	{/if}
		<!--<div class="pdpv1-looks">
			<h5>LOOK LIKE THIS</h5>
			<ul class="rs-carousel-runner">
				<li class="rs-carousel-item"><ul class="rs-carousel-runner">
					<div class="look-image">
						<img src="http://myntra.myntassets.com/skin2/images/retro_chic_denim_360x480.jpg" />
						<div class="look-price">
							<span>TOTAL</span><span class="price red"> Rs. <em>2,568 </em></span><span class="strike">3668</span>
							<button class="btn primary-btn">BUY THE LOOK</button>
						</div>
					</div>
					<div class="product-section">
						<ul>
							<li class="rs-carousel-item " data-id="prod_0_style_86096" style="">
								<span href="javascript:void(0)" data-href="/jackets/nike/nike-men-blue-jacket/86096/buy" data-styleid="86096" data-widget="recent_widget" class="quick-look" style="display: none;"></span>
								
								<a onclick="_gaq.push(['_trackEvent', 'pdp_click', Myntra.Data.pageName, 'recent_widget']);" href="/jackets/nike/nike-men-blue-jacket/86096/buy" title="Nike Men Blue Jacket">
									<div class="mk-product-image">
										<img alt="Nike Men Blue Jacket" src="http://myntra.myntassets.com/images/style/properties/Nike-Men-Blue-Jacket_ced354b028ba58a1886819deb4da63fd_images_150_200_mini.jpg" id="im_product_img_headerCarouselItems_86096">
									</div>
								</a>
								<div class="mk-product-info">
									<p class="brand-text">Nike</p>
									<p class="price-text red">Rs. 2,577 <span class="strike gray">4,295</span></p>
									<p class="red-text red">(40% OFF)</p>	
								</div>
							</li>
	   							<li class="rs-carousel-item " data-id="prod_0_style_86096" style="">
								<span href="javascript:void(0)" data-href="/jackets/nike/nike-men-blue-jacket/86096/buy" data-styleid="86096" data-widget="recent_widget" class="quick-look" style="display: none;"></span>
								
								<a onclick="_gaq.push(['_trackEvent', 'pdp_click', Myntra.Data.pageName, 'recent_widget']);" href="/jackets/nike/nike-men-blue-jacket/86096/buy" title="Nike Men Blue Jacket">
									<div class="mk-product-image">
										<img alt="Nike Men Blue Jacket" src="http://myntra.myntassets.com/images/style/properties/Nike-Men-Blue-Jacket_ced354b028ba58a1886819deb4da63fd_images_150_200_mini.jpg" id="im_product_img_headerCarouselItems_86096">
									</div>
								</a>
								<div class="mk-product-info">
									<p class="brand-text">Nike</p>
									<p class="price-text red">Rs. 2,577 <span class="strike gray">4,295</span></p>
									<p class="red-text red">(40% OFF)</p>	
								</div>
							</li>
							<li class="rs-carousel-item " data-id="prod_0_style_86096" style="">
								<span href="javascript:void(0)" data-href="/jackets/nike/nike-men-blue-jacket/86096/buy" data-styleid="86096" data-widget="recent_widget" class="quick-look" style="display: none;"></span>
								
								<a onclick="_gaq.push(['_trackEvent', 'pdp_click', Myntra.Data.pageName, 'recent_widget']);" href="/jackets/nike/nike-men-blue-jacket/86096/buy" title="Nike Men Blue Jacket">
									<div class="mk-product-image">
										<img alt="Nike Men Blue Jacket" src="http://myntra.myntassets.com/images/style/properties/Nike-Men-Blue-Jacket_ced354b028ba58a1886819deb4da63fd_images_150_200_mini.jpg" id="im_product_img_headerCarouselItems_86096">
									</div>
								</a>
								<div class="mk-product-info">
									<p class="brand-text">Nike</p>
									<p class="price-text red">Rs. 2,577 <span class="strike gray">4,295</span></p>
									<p class="red-text red">(40% OFF)</p>	
								</div>
							</li>
							<li class="rs-carousel-item " data-id="prod_0_style_86096" style="">
								<span href="javascript:void(0)" data-href="/jackets/nike/nike-men-blue-jacket/86096/buy" data-styleid="86096" data-widget="recent_widget" class="quick-look" style="display: none;"></span>
								
								<a onclick="_gaq.push(['_trackEvent', 'pdp_click', Myntra.Data.pageName, 'recent_widget']);" href="/jackets/nike/nike-men-blue-jacket/86096/buy" title="Nike Men Blue Jacket">
									<div class="mk-product-image">
										<img alt="Nike Men Blue Jacket" src="http://myntra.myntassets.com/images/style/properties/Nike-Men-Blue-Jacket_ced354b028ba58a1886819deb4da63fd_images_150_200_mini.jpg" id="im_product_img_headerCarouselItems_86096">
									</div>
								</a>
								<div class="mk-product-info">
									<p class="brand-text">Nike</p>
									<p class="price-text red">Rs. 2,577 <span class="strike gray">4,295</span></p>
									<p class=" red-text red">(40% OFF)</p>	
								</div>
							</li>
							<li class="rs-carousel-item " data-id="prod_0_style_86096" style="">
								<span href="javascript:void(0)" data-href="/jackets/nike/nike-men-blue-jacket/86096/buy" data-styleid="86096" data-widget="recent_widget" class="quick-look" style="display: none;"></span>
								
								<a onclick="_gaq.push(['_trackEvent', 'pdp_click', Myntra.Data.pageName, 'recent_widget']);" href="/jackets/nike/nike-men-blue-jacket/86096/buy" title="Nike Men Blue Jacket">
									<div class="mk-product-image">
										<img alt="Nike Men Blue Jacket" src="http://myntra.myntassets.com/images/style/properties/Nike-Men-Blue-Jacket_ced354b028ba58a1886819deb4da63fd_images_150_200_mini.jpg" id="im_product_img_headerCarouselItems_86096">
									</div>
								</a>
								<div class="mk-product-info">
									<p class="brand-text">Nike</p>
									<p class="price-text red">Rs. 2,577 <span class="strike gray">4,295</span></p>
									<p class="red-text red">(40% OFF)</p>	
								</div>
							</li>
						</ul>
					</div>
				</li>
			</ul>
		</div> --><!--PDP V! looks ending-->

	<div id="mk-matchmaker" class="mk-matchmaker mk-cf mk-hide">
			{include file="inc/matchmaker-vertical.tpl"}
</div>
	<div class="mk-recent-viewed mk-hide mk-carousel-four mk-carousel-five rs-carousel mk-inline-size-selector"></div>
</section>


{/block}

{block name="lightboxes"}
<div id="shipping-info" class="lightbox infobox">
	<div class="mod">
		<div class="hd">
			<div class="title">Myntra's Shipping Policy</div>
		</div>
		<div class="bd body-text">
		    {if $shippingRate > 0}
			    <p>
			    	We strive to deliver products purchased from Myntra in excellent condition and in the fastest time possible.
                    {if $free_shipping_firstorder}If this is your first order with Myntra, shipping will be completely <span class='mk-din-med'>FREE</span>. Also, for all the subsequent purchases of Rs. {$free_shipping_amount} or more, we will deliver the order to your doorstep free of cost.
                    {else}
                        For all the purchases of Rs. {$free_shipping_amount} or more, we will deliver the order to your doorstep free of cost.
                    {/if}
			        A shipping charge of Rs. {$shippingRate} will be applicable to all orders under Rs. {$free_shipping_amount}.
			    </p>
		    {else}
		        <p>Myntra offers free shipping within India on all products purchased at its website.</p>
		    {/if}
		    <ul>
		        <li>If the order is cancelled, lost or un-delivered to your preferred location, we will refund the complete order amount including any shipping charges.</li>
		        <li>If you cancel part of the order, shipping charges will not be refunded.</li>
		        <li>If you return an order delivered to you, original shipping charges will not be refunded. However if you self-ship your returns, you will be reimbursed based on Myntra's <a href="{$http_location}/faqs#returns" target="_blank">Returns Policy</a>.</li>
		    </ul>
		</div>
		<div class="ft">
			<p>Read our <a href="{$http_location}/faqs#shipping" target="_blank">Shipping & Delivery FAQs</a> to know more.</p>
		</div>
	</div>
</div>

<div id="dispatch-info" class="lightbox infobox">
	<div class="mod">
		<div class="hd">
			<div class="title">24-Hour Dispatch</div>
		</div>
		<div class="bd body-text">
			{$deliveryKnowMoreMsg}
		</div>
		<div class="ft">
			<p>Read our <a href="{$http_location}/faqs#tracking" target="_blank">Order Tracking FAQs</a> and <a href="{$http_location}/faqs#shipping" target="_blank">Shipping & Delivery FAQs</a> to know more.</p>
		</div>
	</div>
</div>

{if $myntrashoppingfestCoupons and $festPdpText}
<div id="fest-pdp-popup" class="lightbox infobox">
	<div class="mod">
		<div class="hd">
			<div class="title">{$festPdpPopupTitle}</div>
		</div>
		<div class="bd body-text">
			{$festPdpPopupContent}
		</div>
	</div>
</div>
{/if}

<!-- paroksh -->
{if $specificAttributeFitInfo[0]}{include file="pdp/fit-guide.tpl" }{/if}

<!-- paroksh ends -->
<div id="cod-info" class="lightbox infobox">
	<div class="mod">
		<div class="hd">
			<div class="title">Cash On Delivery</div>
		</div>
		<div class="bd body-text">
			<p>Myntra offers an option to pay Cash on Delivery (CoD) for your orders.</p>
			<p>The Terms & Conditions for CoD are as follows:</p>
			<ul>
				<li>CoD is available for orders above Rs. {$codMinVal|number_format} and below Rs. {$codMaxVal|number_format}</li>
				<li>Your delivery address should be under
					{if $pincodeWidgetVariant eq "test" && !$disableProductStyle && $sizeOptionSKUMapping}<span class="delivery-network">{/if}
					serviceable network
					{if $pincodeWidgetVariant eq "test" && !$disableProductStyle && $sizeOptionSKUMapping}</span>{/if}
					of our courier partners. Please enter the correct PIN Code to ensure delivery.</li>
				<li>Please pay in cash only and do NOT offer cheque or DD to the courier staff.</li>
				<li>Please do NOT pay for any additional charges, i.e. octroi etc. to courier staff. Your invoice amount is inclusive of all charges.</li>
			</ul>
		</div>
		<div class="ft">
			<p>
				{if $pincodeWidgetVariant eq "test" && !$disableProductStyle && $sizeOptionSKUMapping}
					<span class="delivery-network">Check CoD Availability</span> for your PIN Code. 
				{/if}
				Read our <a href="{$http_location}/faqs#payments" target="_blank">Payments FAQs</a> to know more.</p>
		</div>
	</div>
</div>

<div id="returns-info" class="lightbox infobox">
	<div class="mod">
		<div class="hd">
			<div class="title">30 Day Returns</div>
		</div>
		<div class="bd body-text">
			<p>All products sold on Myntra are covered by our 30 Day Returns Policy.</p>
			<p>In case you have any issues with product quality or sizing, you can return your product within 30 days of the date of delivery.</p>
			<p>On successful processing of returned item(s), you will be refunded the price you paid for the item into your Myntra Cashback account. This can be utilized on any of your subsequent purchases at Myntra.</p>
		</div>
		<div class="ft">
			<p>Read our <a href="{$http_location}/faqs#returns" target="_blank">Returns Policy and FAQs</a> to know more.</p>
		</div>
	</div>
</div>
{if $productDisclaimerText|trim neq ""}
<div id="product-disclaimer-info" class="lightbox infobox">
	<div class="mod">
		<div class="hd">
			<div class="title">{$articleType}</div>
		</div>
		<div class="bd body-text">
			<p>{$productDisclaimerText}</p>
		</div>
		<div class="ft">
		</div>
	</div>
</div>
{/if}

<div id="lb-coupon-widget" class="lightbox">
<div class="mod">
<div class="close"></div>
<div class="loading" style="display: none;"></div>
<div class="no-coupon" style="display: none;">
<div class="message"></div>
</div>
<div class="offer-details" style="display: none;">
    <div style="font-size:20px">GET THIS PRODUCT FOR
    <div class="red"><span style="text-transform:none">Rs. </span><span class="coupon-discounted-price"></span></div>
    </div>
    <br>
    <div style="font-size:14px">WHEN YOU APPLY COUPON CODE <span class="red">"<span class="coupon-code red"></span>"</span> IN YOUR BAG<div>
    <br>
    <div class="line"></div>
    <br><br>
    <div style="font-size:14px; text-align:left;padding-bottom:4px;">DETAILS</div>
    <div class="coupon-details">    
    <div>* Get <span class="coupon-discount"></span> off <span class="is-min-condition hide">on a minimum purchase of Rs. <span class="min-amount"></span></span></div>
    <div><span class="coupon-tnc"></span> </div>
    <div>* Valid till <span class="coupon-validity"></span></div>
    <div><span class="coupon-vat-message"></span></div>    
</div>
</div>
</div>
</div>


{if $emiEnabled}
<div id="emi-info" class="lightbox infobox">
	<div class="mod">
		<div class="hd">
			<div class="title">Interest-Free Emi</div>
		</div>
		<div class="bd body-text">
			<ul>
				<li>Credit card EMI options are available for orders above value of Rs {$icici3MONTHMinEMITotalAmount} and Rs {$icici6MONTHMinEMITotalAmount} for the period of 3 months and 6 months respectively.</li>
				<li>There is no processing charge for 3 months EMI. For 6 months EMI option, there is a flat processing charge of Rs {$icici6MONTHEMICharge}.</li>
				<li>Banks that support EMI option on their credit cards are listed on the payments page. You can see the list of available banks upon selection of EMI (Credit Card) option during checkout process.</li>
				<li>Not all credit cards for the listed banks are eligible for EMI options. Banks have the discretion to approve or reject a EMI transaction basis their respective eligibility criterion and policy. In case, your credit card is not eligible for EMI option, please check with the respective bank.</li>
				<li>Upon successful completion of an order with EMI option, the EMI policy usage and terms and conditions of respective banks will be applicable. Any communication related to an ongoing EMI scheme should be addressed to card issuing bank.</li>
				<li>Upon cancellation of item(s), Myntra will refund back the amount to the Bank. Customers need to check with respective bank to discontinue the EMI option as per the bank's policy.</li>
			</ul>
		</div>
		<div class="ft"></div>
	</div>
</div>
{/if}
{/block}
{block name=footer}
	{if $layoutVariant eq 'test'}
		{include file="inc/footer-new.tpl" megaFooterEnabled=false}
	{else}
		{include file="inc/footer.tpl" megaFooterEnabled=false}
	{/if}
{/block}


{block name=pagejs}
{/block}
{block name=macros}
    {include file="macros/pdp-macros.tpl"}
{/block}
