<div id="mk-payment-gc-activate-popup-content" class="lightbox mk-giftcard-activation-section">
<div class="mod">
        <div class="hd">
            <h2>USE GIFT CARD/VOUCHER</h2>
        </div>

        <div style="background: #E6E6E6;height: 30px;padding-top: 10px;text-transform: none;">
            The amount will be added to your cashback account
        </div>

        <div class="bd clearfix">
            <div style="text-align: left;width: 400px;margin: auto;">
                <div class="mk-gc-instruction" style="text-transform: none;">
                    <div class="mk-gc-inputs" style="padding: 10px;">
                        <div class="input-row" style="width: 200px;margin: auto;">
                            <label class="lbl">GIFT CARD/VOUCHER*</label>
                            <input type="text" name="card" class="mk-gc-card" placeholder="Gift Code/Voucher Number">
                            <div class="mk-gc-activation-err hide" id="card-err"></div>
                            <br><br>
                            <label class=" lbl">PIN</label>
                            <div class="mk-gc-activation-err hide" id="pin-err"></div>
                            <input type="text" name="pin" class="mk-gc-pin" placeholder="PIN"> &nbsp;
                            <label style="font-size: 11px;text-transform: none;font-family:'Arial'">(Enter only if provided)</label>

                            <br><br><br>
                            <div class="lbl">VERIFY*</div>
                            <div style="position:relative">
                                <img src="{$https_location}/captcha/captcha.php?id=giftCaptcha&fc=1"  id="captcha" 
                                onload="
                                    document.getElementById('giftcard-cap-loading').style.display = 'none';
                                "
                                />
                                <span id="giftcard-cap-loading"></span>

                                <input type="text" name="captcha" class="mk-gc-captcha" placeholder="Type in the text above">
                                <div class="mk-gc-activation-err hide" id="captcha-err"></div>
                                <label class="giftcard-captcha-change-text">
                                        <a  href="javascript:void(0)"
                                            onclick="
                                                document.getElementById('giftcard-cap-loading').style.display = 'block';
                                                document.getElementById('captcha').src=
                                                '{$https_location}/captcha/captcha.php?id=giftCaptcha&rand='+Math.random();
                                                document.getElementById('captcha-form').focus();
                                            "
                                            id="change-image" >Change text</a></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mk-gc-activation-success hide"></div>
        <div class="mk-gc-activation-error hide"></div>

        <div style="background: #E6E6E6;height: 1px;"></div>
        <div class="ft">
            <p>
                <button class="activate-gift-card btn primary-btn">Use</button>
                &nbsp; &nbsp;
                <button class="btn normal-btn btn-ok">Close</button>
            </p>
        </div>
</div>
</div>
