{extends file="layout.tpl"}

{block name=navigation}
	{if $pdpMiniNav && $layoutVariant neq 'test'}		
		{include file="inc/navigation-collapsed.tpl"}
	{else}
		{include file="inc/navigation-default.tpl"}		        
	{/if}
{/block}

{block name=body}
{if !$telesales}
	<section class="mk-site-main mk-giftcards">
	<script type="text/javascript">
		Myntra.Data.OccasionJson = {if $occasions_json}{$occasions_json}{else}""{/if};
		Myntra.Data.DefaultValues = {if $defaultvalues}{$defaultvalues}{else}false{/if};
		Myntra.Data.GiftingLowerLimit = {if $giftingLowerLimit}{$giftingLowerLimit}{else}500{/if};
		Myntra.Data.GiftingUpperLimit = {if $giftingUpperLimit}{$giftingUpperLimit}{else}10000{/if};	
	</script>
	<div class="mk-giftcards-header-section">
		<h1>GIFT CARDS</h1>
		<img src="{$cdn_base}/skin2/images/GiftCardMTopNav9.jpg">
	</div>

	<div class="mk-giftcards-left">
		<div class="mk-giftcards-occasions">
			<h2>SELECT A DESIGN</h2>
			<div class="mk-occasions rs-carousel">
				<ul class="rs-carousel-runner">
				{foreach name=occasionsloop item=occ from=$occasions}
					<li class="rs-carousel-item">
						<div class="mk-occasion-title">{$occ->name}</div>
						<img data-id="{$occ->id}" class="mk-occasion-thumb{if $occ->id eq $occasionObj->id} mk-occasion-selected{/if}"  src="{$cdn_base}/{$occ->previewImageThumbnail}"/>
					</li>
				{/foreach}
				</ul>
			</div>	
		</div>
		<div class="mk-giftcards-preview">
			<div class="mk-cf">
				<div class="mk-gp-subtitle mk-f-left"><span>DELIVERY MODE: </span>SEND VIA EMAIL</div>
				<a class="mk-giftcard-preview-link mk-f-right">Preview Gift Card</a>
			</div>
			<div class="mk-giftcards-preview-section">
				<img class="mk-gc-preview-images" src="{$cdn_base}/{$occasionObj->previewImage}"/>
				<div class="mk-gc-onpage-preview">
					<div class="mk-gc-preview-to">Dear <span class="mk-gc-preview-to-name">{$recipient_name}</span>,</div>
					<div class="mk-gc-preview-message">{$message}</div>
					<div class="mk-gc-preview-from">From <br/><span class="mk-gc-preview-from-name">{$sender_name}</span></div>
				</div>
			</div>
		</div>
	</div>

	<div class="mk-giftcards-right">
		<h2>ENTER DETAILS</h2>
		<form class="mk-giftcards-form" action="{$http_location}/mkmygiftcardcart.php" method="post" autocomplete="off">
			<input type="hidden" class="mk-occasion-id" name="occasion_id" value="{$occasion}">
			<input type="hidden" name="quantity" value="1">
			<input type="hidden" name="type" value="0">
			<div class="mk-form-sections mk-giftcards-amount-section mk-cf">
				<div class="mk-giftcards-choose mk-f-left" {if !$defaultvalues}style="display:none"{/if}>
					<label>Choose Value</label>
					<select class="mk-amount-select">
						<option {if $defaultvalues}selected="selected"{/if} value="500"> Rs. 500</option>
						<option value="1000"> Rs. 1000</option>
						<option value="1500"> Rs. 1500</option>
						<option value="2000"> Rs. 2000</option>
						<option value="2500"> Rs. 2500</option>
						<option value="3000"> Rs. 3000</option>
						<option value="3500"> Rs. 3500</option>
						<option value="4000"> Rs. 4000</option>
						<option value="4500"> Rs. 4500</option>
						<option value="5000"> Rs. 5000</option>
						<option value="5500"> Rs. 5500</option>
						<option value="6000"> Rs. 6000</option>
						<option value="6500"> Rs. 6500</option>
						<option value="7000"> Rs. 7000</option>
						<option value="7500"> Rs. 7500</option>
						<option value="8000"> Rs. 8000</option>
						<option value="8500"> Rs. 8500</option>
						<option value="9000"> Rs. 9000</option>
						<option value="9500"> Rs. 9500</option>
						<option value="10000"> Rs. 10000</option>
					</select>
				</div>
				<div class="mk-giftcards-enter mk-f-left" {if $defaultvalues}style="display:none"{/if}>
					<label>Enter Value</label>
					<input type="text" name="amount" class="mk-amount mk-placeholder-inputs {if $defaultvalues}mk-blur{/if}" value="{$amount}" {if $defaultvalues}data-placeholder="{$amount}"{/if}><br/>
					<span>Rs. {$giftingLowerLimit} - Rs. {$giftingUpperLimit}</span>
				</div>
				<a href="javascript:void(0)" class="mk-gc-switch-to-input" {if !$defaultvalues}style="display:none"{/if}>Enter a Value</a>
				<a href="javascript:void(0)" class="mk-gc-switch-to-select" {if $defaultvalues}style="display:none"{/if}>Choose Value</a>	
				<div class="mk-hide mk-amount-error err"></div>
			</div>
			<div class="mk-form-sections">
				<label>Recipient Mail</label>
				<input type="text" name="recipient_email" class="mk-recipient-email mk-placeholder-inputs {if $defaultvalues}mk-blur{/if}" value="{$recipient_email}" {if $defaultvalues}data-placeholder="{$recipient_email}"{/if}>
				<div class="mk-hide mk-recipient-email-error err"></div>
			</div>
			<div class="mk-form-sections">
				<label>Recipient Name <span class="mk-optional">(max. 15 chars.)</span></label>
				<input type="text" name="recipient_name" maxlength="15" class="mk-recipient-name mk-placeholder-inputs {if $defaultvalues}mk-blur{/if}" value="{$recipient_name}" {if $defaultvalues}data-placeholder="{$recipient_name}"{/if}>
				<div class="mk-hide mk-recipient-name-error err"></div>
			</div>
			<div class="mk-form-sections mk-giftcards-message mk-cf">	
				<label>Message <span class="mk-optional">(optional)</span></label>
				<textarea type="textarea" name="message" class="mk-message {if $defaultvalues}mk-blur{/if}" {if $defaultvalues}data-placeholder="{$message}"{/if}>{$message}</textarea>
				<a class="mk-f-left mk-message-clear">back to default</a><span class="mk-f-right mk-message-count"><span class="mk-message-char-count">{$message_chars_left_count}</span> / 100 chars</span>
				<div class="mk-hide mk-cf mk-message-error err"></div>
			</div>
			<div class="mk-form-sections">
				<label>Sender Name <span class="mk-optional">(max. 15 chars.)</span></label>
				<input type="text" name="sender_name" maxlength="15" class="mk-sender-name mk-placeholder-inputs {if $defaultvalues}mk-blur{/if}" value="{$sender_name}" {if $defaultvalues}data-placeholder="{$sender_name}"{/if}>
				<div class="mk-hide mk-sender-name-error err"></div>
				{if !$sender_name|trim && $login}
				<input type="checkbox" name="set_as_profile_name" id="set_as_profile_name" class="mk-set-as-profile-name"/><label class="mk-set-as-profile-label" for="set_as_profile_name">Set as profile name</label>
				{/if} 
			</div>	
			<button class="buy-gift-card btn primary-btn">Buy Gift Card Now <span class="proceed-icon"></span></button>
		</form>	
	</div>
	
	<div class="mk-cf mk-giftcard-faqs">
		<h2>GIFT CARD FAQs</h2>
		<p>How do I purchase a Myntra Gift Card?</p>
		<p class="mk-answer">All you need to do to purchase a Myntra Gift Card is to enter the details of the recipient for whom you need to send the Gift Card and pay for it using your Credit Card/Debit Card/Netbanking/EMI option. The gift card will be sent instantly to the recipient and can be used by him to make purchases on Myntra.</p>
		<p>How do I add a Myntra Gift Card?</p>
		You can add a Gift Card to your account in one of two ways:<br/><br/>
		<ul>
			<li>Once you have received the Gift card email, please click on the 'Add Gift Card' button in the body of the email. You will be taken to the Myntra homepage where you will need to sign-up or login to your account. Once this is done, the Gift card value is instantly transferred to your Cashback account and can be redeemed at checkout.</li>
			<li>You can also add a Gift Card to your account at your My Myntra section. Please go to the tab titled 'Gift Cards' and add a Gift Card. You will need the Gift Code (present in the email) in order to add your Gift Card. A Gift Card can also be added at your Payment Page during Checkout.</li>
		</ul>
		<p class="mk-answer">Please note than Myntra is not responsible if your Gift Card is stolen or used without your knowledge or permission.</p>
		<p>How do I use a Myntra Gift Card?</p>
		<p class="mk-answer">Once the Gift Card value is added to your Cashback account, you can use it to make purchases on Myntra. If the value of the order exceeds your Gift Card value in your Cashback account, you can pay the balance using any mode of payment including Cash on Delivery. If the value is less than the value of the Gift Card, the balance will remain in your Cashback account and can be used later.</p>
		<p>How are Gift Cards delivered?</p>
		<p class="mk-answer">Gift Cards are delivered by email directly to the recipient. A copy of the Gift Card is also sent to the sender. Alternatively, you can take a printout of the Gift Card and give it directly to your recipient.</p>
		<p>What happens if I cannot find the mail that contained a Gift Card?</p>
		<p class="mk-answer">If you have not received the Gift Card, please contact your sender and ask him to resend the Gift Card. The sender can resend the Gift Card email from his My Myntra section on the website. If this does not work as well, please ask him for the Gift Code and add it through your My Myntra section. In case you are still facing problems, please contact our customer care.<br/>Please note than once the Gift Card has been resent, the latest email must be used to add the Gift Card to your account.</p>
		<p>Do Myntra Gift Cards expire?</p>
		<p class="mk-answer">Yes. Myntra Gift Cards expire 1 year from the date of issue.</p>
		<p>How do I cancel a Gift Card?</p>
		<p class="mk-answer">You can cancel a Gift Card if it has not been added or used by your recipient. If you are a sender and would like to cancel a Gift Card, please add the Gift Card to your account. The amount is transferred to your Cashback account and can be used to purchase another Gift Card or products on Myntra. We do not allow refunds of Gift Cards.</p>
		<p>What Payment Methods are available for Gift Cards?</p>
		<p class="mk-answer">You can purchase Gift Cards on Myntra using your Debit Card/Credit Card/Netbanking or EMI.</p>
	</div>
	
	{include file="my/giftcards-preview-template.tpl"}

	</section>
{else}
Access Not Allowed
{/if}
{/block}
