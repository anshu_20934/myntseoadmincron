{include file="inc/doctype.tpl"}
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# {$facebook_app_name}: http://ogp.me/ns/fb/{$facebook_app_name}#">
    {include file="inc/head-meta.tpl"}
    {include file="inc/head-css.tpl"}
    {include file="inc/head-script.tpl"}
    {block name=pagecss}{/block}
</head>

{block name=body}
    <script type="text/javascript">
        var orderid='{$orderid}';
        Myntra.Data.socialAction='purchase';
        Myntra.Data.pageName='confirmation';
        //Exclusion required for gifts sent.
        {if $giftamount > 0} 
            var socialPost = 0;
        {else}
            var socialPost = 1;
        {/if}
        var socialPostArray={$socialArray};
        var socialPostStyleDetailsArray={$socialPostStyleDetailsArray};
    </script>

    <script>
        Myntra.Confirmation = Myntra.Confirmation || [];
        Myntra.Confirmation.Data = {
            orderId : {$orderid},
            shipping : {$shippingCharges},
            shippingCode : '{$userinfo.s_zipcode}',
            shippingCity : '{$userinfo.s_city}',
            paymentOption : '{$paymentoption}',
            couponCode : '{$couponCode}',
            totalAmount : {$amountafterdiscount|string_format:'%d'},
            totalQuantity : {$totalQuantity},
            products : '{$products}',
            productIds : '{$styleIdList}',
            isFirstOrder : {$isFirstOrder},
        };
    </script>



<div class="secure">
    <div class="box-title"><span class="complete-icon" ></span> Congratulations!</div>
    <div class="sub-title">
        <p>Your Order Has Been Placed</p>
        <p>Order No. <a class="c-link" href="/mymyntra.php?view=myorders" target="_blank" >{$orderid}</a></p>
    </div>
    <div class=" box-div">
        <p>
            Your order is being processed and will be dispatched from our warehouse
             within 24 hours. Post shipping you will be recieving an email stating the 
             expected delivery date for your order.
        </p>
       
        <div class="list">
        {* BEGIN: list of items *}
    
        {foreach from=$productsInCart item=product}
            <div class="row prod-item">
                <div class="col2">
                    <div class="prod-name">
                        {if !$product.freeItem && $product.landingpageurl|trim}<a href="{$http_location}/{$product.landingpageurl}" target="_blank">{/if}
                            {$product.productStyleName|truncate:80}
                        {if !$product.freeItem && $product.landingpageurl|trim}</a>{/if}
                    </div>
    
                    <div class="size-qty-wrap">
                        <span class="lbl">Size:</span>
                        {$product.final_size}
                        <span class="sep">/</span>
                        <span class="lbl">Qty:</span> {if $prod.final_quantity}{$product.final_quantity}{else}{$product.quantity}{/if}
                    </div>                    
                </div>
            </div>
        {/foreach}
            
        {* END: list of items *}
    </div>
       
    <div class="sub-title price-div"> Amount Due 
        <span class="detail-text">Rs. {$amountafterdiscount|number_format:0:".":","}</span> at time of delivery
    </div>
    {if $totalLoyaltyPointsAwarded > 0}
	    <div class="lp-box-div" >
	        <div class="lp-logo icon-my-privilege"></div>
	        <div class="lp-green"> You Have Earned {$totalLoyaltyPointsAwarded} points </div>    
	        <div class="lp-text"> for this order. <a class="c-link" href="/mymyntra.php?view=myprivilege#myprivilege" target="_blank" > Know more </a> </div>
	        <div class="lp-foot-msg"> Points will be shown in your account within 30 minutes</div>
	    </div>    
    {/if}
    
    <div class="foot-msg">A confirmation mail has been sent to <span class='detail-text'>{$userinfo.b_email}<span></div>
    
</div>




   
{/block}


{block name="macros"}
    {include file="macros/confirmation-macros.tpl"}
{/block}

