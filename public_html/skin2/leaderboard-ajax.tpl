{if $section eq 'mypurchases'}
	<h4>My purchases</h4>
 	<table border="0" cellspacing="0">
		<col width="125">
		<col width="150">
		<col width="120">
		<tr class="mk-even">
			<td style="text-align: right;">Name:</td>
			<td>{$purchaseSummary.username}</td>
		</tr>
		<tr class="mk-even">
            <td style="text-align: right;">Total Purchase:</td>
            <td>{if $purchaseSummary.total > 0}Rs. {$purchaseSummary.total}{else} Rs. 0.00{/if}</td>
        </tr>
        <tr class="mk-even">
            <td style="text-align: right;">No Of Orders</td>
            <td>{$purchaseSummary.noOfOrders}</td>
        </tr>
        {*<tr class="mk-even">
            <td style="text-align: right;">Rank:</td>
            <td>{$purchaseSummary.rank}</td>*}
        </tr>
 	</table>
{/if}

{if $section eq 'leaderBoard'}
	
	{if !$leaderBoard}
		&nbsp;
	{else}
		<h4>Leader Board</h4> 
	 	<table border="0" cellspacing="0">
			<col width="125">
			<col width="150">
			<col width="120">
	 		<tr class="mk-table-head">
	 			<th> Rank</th>
	 			<th> Name</th>
	 			<th> Total Value</th>
	 		</tr>
	 		{foreach item=row from=$leaderBoard name=leadeerBoardLoop}
	 			<tr class="{cycle name='lb_cycle' values='mk-odd,mk-even'}">
					<td>{$smarty.foreach.leadeerBoardLoop.index + 1}</td>
					<td>{if $row.username|@trim eq '' } Name Not Available {else} {$row.username} {/if} </td>
					<td> {$row.totalAmount}</td> 				
	 			</tr>
	 		{/foreach}
	 	</table>
	 {/if}	
{/if}


{if $section eq 'menLeaderBoard'}
	{if !$menLeaderBoard}
		&nbsp;
	{else}
		<h4>Men's Leader Board</h4> 
	 	<table border="0" cellspacing="0">
			<col width="125">
			<col width="150">
			<col width="120">
	 		<tr class="mk-table-head">
	 			<th> Rank</th>
	 			<th> Name</th>
	 			<th> Total Value</th>
	 		</tr>
	 		{foreach item=row from=$menLeaderBoard name=leadeerBoardLoop}
	 			<tr class="{cycle name='lb_cycle' values='mk-odd,mk-even'}">
					<td>{$smarty.foreach.leadeerBoardLoop.index + 1}</td>
					<td>{if $row.username|@trim eq '' } Name Not Available {else} {$row.username} {/if} </td>
					<td> {$row.totalAmount}</td> 				
	 			</tr>
	 		{/foreach}
	 	</table>
	 {/if}	
{/if}


{if $section eq 'womenLeaderBoard'}
	{if !$womenLeaderBoard}
		&nbsp;
	{else}
		<h4>Women's Leader Board</h4> 
	 	<table border="0" cellspacing="0">
			<col width="125">
			<col width="150">
			<col width="120">
	 		<tr class="mk-table-head">
	 			<th> Rank</th>
	 			<th> Name</th>
	 			<th> Total Value</th>
	 		</tr>
	 		{foreach item=row from=$womenLeaderBoard name=leadeerBoardLoop}
	 			<tr class="{cycle name='lb_cycle' values='mk-odd,mk-even'}">
					<td>{$smarty.foreach.leadeerBoardLoop.index + 1}</td>
					<td>{if $row.username|@trim eq '' } Name Not Available {else} {$row.username} {/if} </td>
					<td> {$row.totalAmount}</td> 				
	 			</tr>
	 		{/foreach}
	 	</table>
	 {/if}	
{/if}



{if $section eq 'trendingStyles'}


{if !$trendingStyles}
	&nbsp;
{else}
	<h4>Trending Styles</h4>
	<table border="0" cellspacing="0">
		<col width="50">
		<col width="400">
		<tr class="mk-table-head">
			<th> &nbsp;</th>
			<th> Style</th>
		</tr>
		
			{foreach item=row from=$trendingStyles}
				<tr>
					<td>
						<a href="{$row.styleId}" target="_blank" style="text-decoration:none;">
							<img src="{$row.imagepath}"  style="vertical-align:middle;"/>
						</a>	
					</td>	
					<td>
						<a href="{$row.styleId}" target="_blank" style="text-decoration:none;"> 
							<span style="padding-left:40px;">{$row.style_name}</span> 
						</a>
					</td>
				</tr>
			{/foreach}
	
	</table>
{/if}
{/if}

{if $section eq 'leaderBoardSeach'}
	<table border="0" cellspacing="0">
		<col width="300">
		<tr class="mk-even red" style="">
			<td style="font-size:14px;"> You are at {$rank} !</td>
		</tr>	
	</table>

{/if}
