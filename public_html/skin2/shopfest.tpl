{extends file="layout.tpl"}

{block name=doctype}{include file="inc/doctype-html5.tpl"}{/block}

{block name=body}
{assign var="imageArray" value=","|explode: $shoppingfestAssuredandBumperPrizes}
<script>
    Myntra.Data.pageName="shopfest";
</script>
<div id="shopfest" class="shopfest">
    <h1> Hot in December / Hot Contests </h1>

    <section class="tabs">
	<ul>

		<li id="shopperday" class="sfbanner" {if $showDailyPrizes == 'false'} style="visibility:hidden;" {/if} >
    			<h2>DAILY PRIZES</h2>
    			<p>Exciting prizes up for grabs!</p><p style="font-size:11px;color:gray;text-transform:uppercase;">{$numberOfWinners}</p>
    			<p class="timeleft" style="text-transform:uppercase;"><span class="clockicon">&nbsp;
</span><strong>10 HOURS 12 MINUTES</strong> FOR CONTEST TO END</p>
    			<span class="opt"></span>
		</li>
		<li class="seperator"></li>
		
		<li id="assuredprize" class="sfbanner center selected" {if $showAssuredPrizes == 'false'} style="visibility:hidden;" {/if} >
    			<h2>ASSURED PRIZES</h2>
    			<p>Win a Samsung Galaxy Tab 2 or Note 2</p>
			<p style="font-size:11px;color:gray;text-transform:uppercase;">	{$winnersAbove50K}</p>
    			<p class="timeleft"><span class="clockicon">&nbsp;
</span><strong>{$daysToGoForMonth}</strong> FOR CONTEST TO END</p>
    			<span class="opt"></span>	
		</li>
		
		<li class="seperator"></li>
		<li id="jackpot" class="sfbanner " {if $showBumperPrizes == 'false'} style="visibility:hidden;"  {/if} >
    			<h2>BUMPER PRIZES </h2>
    			<p>Volkswagen Polo up for grabs every week!<br/></p><p style="font-size:11px;color:gray;text-transform:uppercase;">{$numberOfWeeklyWinners}</p>
    			<p class="timeleft"><span class="clockicon">&nbsp;
</span><strong>{$daysToGoForThisWeek}</strong> FOR CONTEST TO END</p>
    			<span class="opt"></span>
		</li>	
	</ul>
    </section>

    <section id="jackpot-content" class="tab-content jackpot">
        <h2>About the <strong>Bumper Prizes </strong> Contest </h2>
        <p class="about">
	Get lucky and win a Volkswagen Polo every week with a minimum purchase of Rs. {$festLuckydrawMinAmount}.
<br/>	Every incremental purchase of Rs. 1,000 increases your chance of winning.
        </p>

	<div class="note">
	{$BumperPrizeUserMsg}
	</div>

        <div class="prize">
            <img 
            src="{$imageArray.2}">
        </div>
	{if !empty($lastWeekWinners) }

        <div class="jackpot-winners">
        <h3>Winners so far</h3>
                {foreach from=$lastWeekWinners key=key item=item}
                    <span class="index"> Week #{$item['week']} </span><span class="name"> - {$item['name']}</span><br/>
                {/foreach}
        </div>
	{/if}
	<div style="border-top: 1px solid #CCC;margin-bottom:10px;padding-top:10px;">
	<div class="termsandconditions">
	<h2>TERMS AND CONDITIONS</h2>
    {$termsCondBumper}
	</div>
	</div>

    </section>

    <section id="assuredprize-content" class="tab-content assured selected">
        <h2>ABOUT THE ASSURED PRIZES</h2>
        <p class="about">
	    Shop for at least Rs. 50,000 in December and get assured prizes.
	    <br/>Win a Samsung Galaxy Tab 2 by purchasing over Rs. 50,000 (or) a Samsung Galaxy Note 2 by purchasing above Rs. 1,00,000.<br/>
        </p>
        <div class="note">
	    <div class="line-container" style="padding:0px 80px 40px 80px;width: 300px;display: inline-block;float: left;margin-left:20px;">

	    	<div style="text-transform: none;font-size: 12px;font-weight: normal;font-family:Arial,sans-serif;">{$moreToWinFor50}	</div>
	    	{if $login}
	    	<div class="v-line"> <span> Rs.0 </span></div>
	    	<div class="current-spending" style="left:{$leftBag1};"><span> {$myBag} </span></div>
	    	<div class="h-line" style="width:284px;"></div>
	    	<div class="v-line"> <span> Rs.50,000 </span></div>
	    	{/if}
	    </div>
            <p style="float:left;margin-top:115px;"> {$filler} </p>
	    <div class="line-container" style="padding: 0px 80px 40px 80px;width: 300px;display: inline-block;float: left;margin-left:20px;">
	    	<div style="text-transform: none;font-size: 12px;font-weight: normal; font-family:Arial,sans-serif;">{$moreToWinFor100}	</div>
	    	{if $login}
	    	<div class="v-line" style=''> <span> Rs.0 </span></div>
	    	<div class="current-spending" style="left:{$leftBag2};"><span> {$myBag} </span></div>
	    	<div class="h-line " style="width:284px;"></div>
	    	<div class="v-line "> <span> Rs.1,00,000 </span></div>
	    	{/if}
	    </div>

	    <br/>
        </div>
	<div style="style=padding-bottom:20px;border-bottom:solid #808080 1px;margin-bottom:20px;">
        <img class="prizes" alt="" src="{$imageArray.0}" style="border-right:1px solid #808080;display:inline-block;">
        <img class="prizes" alt="" src="{$imageArray.1}">
	</div>

	{if !empty($winners50K) }
	<div class="jackpot-winners">
	<h3>Winners so far</h3>
	<ol>

	{if !empty($winners50K) }
			<li><span class="index"> Samsung Galaxy Tab 2 </span><span class="name"> - {$winners50K} winners </span></li>
	{/if}
	{if !empty($winners1Lakh) }
			<li><span class="index"> Samsung Galaxy Note 2 </span><span class="name"> - {$winners1Lakh} winners </span></li>
	{/if}
	</ol>
	</div>
	{/if}
	<div class="termsandconditions">
	<h2>TERMS AND CONDITIONS</h2>
    {$termsCondAssured}
	</div>
    </section>

    <section id="shopperday-content" class="tab-content toppers">
        <h2>About <strong> The Daily Prizes </strong> Contest</h2>
        <p class="about">
	   {$dw_ui_msg}
<br/>
        </p>

<br/>
        <div id="shopperday-toppers">
            {include file="shopfest/toppers.tpl"}
        </div>

        <div id="yesterday-toppers">
            {include file="shopfest/winners.tpl"}
        </div>
	
	<div style="border-top:solid 1px #ccc;margin-bottom:10px;padding-top:10px;">

	<div class="termsandconditions">
	<h2>TERMS AND CONDITIONS</h2>
    {$termsCondDaily}
	</div>
	</div>

    </section>

</div>
{/block}

