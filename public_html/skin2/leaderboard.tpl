{extends file="layout.tpl"}

{block name=pagejs}
	<script src="/skin2/js/leaderboard.js"></script>
{/block}
{block name=pagecss}
	<link href="skin2/css/my.css" rel="stylesheet" type="text/css">
	<style type="text/css">
		#scrollingText div.scrollableArea p {
				display: block;
				float: left;
				margin: 0;
				padding: 0px 0px 20px 20px;
				white-space: nowrap;
			}
	</style>
{/block}

{block name=body}
<input type="hidden" id="lbId" name="lbId" value="{$lbId}" />
<br/>
 <div class="mymyntra mk-account-pages my-orders-page">
 	
 	<h2>{$pageTitle}</h2>
 	
 	<div style="margin:20px;" class="red"> 
 		{$tickerMessage}
 	</div>

    <div id ="banner" style="width:800px; margin:0 auto;" >
        <a href= "{$bannerHref}"> <img src = "{$bannerSrc}"> </a>
    </div>

 	<div id="purchases">
            {if $login}
	 		{assign var=section value=mypurchases}
	 		{include file="leaderboard-ajax.tpl"}
            {/if}
 	</div>

    {if $flashQualifyMessage}
        <h4 style="color:red"> {$flashQualifyMessage} </h4>
    {/if}

    {if $flashQualifiedMessage}
        <h4 style="color:green">{$flashQualifiedMessage}</h4>
    {/if}
    	 	
 	<br/> <br/>
 	<div>
		{if $leaderBoard}
		<div style="width:50%;float:left;">
		        <div id="leaderBoard">
		        {assign var=section value=leaderBoard}
		        {include file="leaderboard-ajax.tpl"}
		        </div>
		</div>
		{/if}

		{if $menLeaderBoard}
		<div style="width:50%;float:left;">
		        <div id="menLeaderBoard">
		        {assign var=section value=menLeaderBoard}
		        {include file="leaderboard-ajax.tpl"}
		        </div>
		</div>
		{/if}

		{if womenLeaderBoard}
		<div style="width:50%;float:left;">
		        <div id="womenLeaderBoard">
		        {assign var=section value=womenLeaderBoard}
		        {include file="leaderboard-ajax.tpl"}
		        </div>
		</div>
		{/if}

 		<!--div style="width:50%;float:left;" id="trendingStyles">
 				{assign var=section value=trendingStyles}
	 			{include file="leaderboard-ajax.tpl"}
 		</div-->
 		<div  style="clear:both;"></div>
 	</div>
 	<br/><br/>
 	
 </div>
{/block}
