<!DOCTYPE html>
<html>
<head>
    {include file="inc/head-meta.tpl"}
    {include file="inc/head-css.tpl"}
    {include file="inc/head-script.tpl"}
    {block name=pagecss}{/block}
</head>
<body class="{$_MAB_css_classes} {if $smarty.server.HTTPS}secure{/if}">
    {include file="inc/gtm.tpl"}
    <div class="mk-body{if $layoutVariant eq 'test'} mynt-layout-new{/if}">
	{if $promoBannerEnabled}
    	{include file="inc/fest-promo-secure.tpl"}
    {/if}
    <div class="mk-wrapper">
       	{if $layoutVariant eq 'test'}
       		{include file="inc/header-new.tpl" checkoutHeader=true}
       	{else}
	        {include file="inc/header-checkout.tpl"}
        {/if}
        {block name=body}{/block}
        {block name=footer}
        	{if $layoutVariant eq 'test'}
        		{include file="inc/footer-new.tpl" megaFooterEnabled=false}
        	{else}
        		{include file="inc/footer.tpl" megaFooterEnabled=false}
        	{/if}
        {/block}
    </div>
    </div>
    {block name=lightboxes}{/block}
    {if $currentPage neq "/securelogin.php"}
        {include file="inc/modal-signup.tpl"}
    {/if}
    {include file="inc/modal-combo-overlay.tpl"}
	{include file="inc/body-css.tpl"}
    {include file="inc/body-script.tpl"}
    {block name=pagejs}{/block}
    {block name=macros}{/block}
</body>
</html>
