{include file="inc/doctype.tpl"}
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# {$facebook_app_name}: http://ogp.me/ns/fb/{$facebook_app_name}#">
    {include file="inc/head-meta.tpl"}
    {include file="inc/head-css.tpl"}
    {include file="inc/head-script.tpl"}
    {block name=pagecss}{/block}
</head>
<body>
    {if !$isGTMLazyLoadEnabled}
        {include file="inc/gtm.tpl"}
    {/if}
    <div class="mk-body{if $layoutVariant eq 'test'} mynt-layout-new{/if}">
    {$nodeheader}
    <div class="mk-wrapper">
        {block name=body}{/block}
        {include file="inc/touts.tpl"}
        {block name=searchdescription}{/block}
    </div>
    {$nodefooter}
    </div>
    {block name=lightboxes}{/block}
    {if not $login and $currentPage neq "/register.php"}
        {include file="inc/modal-signup.tpl"}
    {/if}
    {if $login and $showNewSignUpConfirmation}
        {include file="inc/modal-signupconfirmation.tpl"}
    {/if}
    {include file="inc/modal-combo-overlay.tpl"}
    {include file="inc/modal-quick-look.tpl"}
    {include file="inc/body-css.tpl"}
    {include file="inc/body-script.tpl"}
    {block name=pagejs}{/block}
    {block name=macros}{/block}
</body>
</html>
