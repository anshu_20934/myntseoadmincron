{extends file="layout.tpl"}
{block name=pagecss}
 	<style>
 	{literal}
 	#star-container {
 			background:transparent url('{/literal}{$starNStyleImage}{literal}') no-repeat 0 0;
 	}
 	{/literal}
 	</style>
{/block}
{block name=navigation}
	{if $layoutVariant eq 'test'}
		{include file="inc/navigation-default.tpl"}
	{else}
		{include file="inc/navigation-collapsed.tpl"}
	{/if}
{/block}
{block name=body}

<div class="mk-star-n-style">
	{if $starNStyleError}
	<div class="lg-not-found">
		<p class="title">OOPS - WE COULDN'T FIND ANY MATCHES!</p>
		<p>The Edition you are trying to view is either removed or doesnt exist	</p>
	</div>
	{else}
	<div class="star-n-style-details">
		<h1 class="mk-hide">{$starNStyleName}</h1>
		
		<div class="star-n-style-list">
			<span>STARRING</span>
			{if $starNStyleList|count > 1}
			<select>
				<option>{$starNStyleName}</option>
				{foreach name=starNStyle key=key item=starNStyle from=$starNStyleList}
				{if $starNStyle[0] neq $starNStyleID}
					<option value="{$http_location}/starnstyle-{$starNStyle[1]|replace:' ':'-'|lower}-{$starNStyle[0]}">{$starNStyle[1]}</option>
				{/if}
				{/foreach}
			</select>
			{else}
			<span class="star-name">{$starNStyleName}</span>
			{/if}
		</div>
		
		
		<ol>
			<li class="title">IN THIS ISSUE:</li>
			{foreach name=imageConfig key=key item=imageConfig from=$starNStyleImageConfig}
				<li class="link">{if $imageConfig@first}{else} / {/if}<a href="{$imageConfig->target_page}" {if $imageConfig->new_window}target="_blank"{/if} title="{$imageConfig->name|escape}">{$imageConfig->name}</a></li>
			{/foreach}
		</ol>
		
	</div>
	<div id="star-container">
		<div id="star-inner-cont">
		{foreach name=imageConfig key=key item=imageConfig from=$starNStyleImageConfig}
			<a href="{$imageConfig->target_page}" class="map-block" data-coords="{$imageConfig->image_coords_x1|string_format:'%d'},{$imageConfig->image_coords_y1|string_format:'%d'},{$imageConfig->image_coords_x2|string_format:'%d'},{$imageConfig->image_coords_y2|string_format:'%d'}" {if $imageConfig->new_window}target="_blank"{/if} title="{$imageConfig->name|escape}"></a>
		{/foreach}
		</div>
	</div>
	{/if}
</div>

{/block}

{block name=pagejs}
 <script>
	var lgPage='sns';
	var starImage='{$starNStyleImage}';
</script>

{/block}
