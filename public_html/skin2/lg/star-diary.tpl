{extends file="layout.tpl"}

{block name=navigation}
	{if $layoutVariant eq 'test'}
		{include file="inc/navigation-default.tpl"}
	{else}
		{include file="inc/navigation-collapsed.tpl"}
	{/if}
{/block}
{block name=body}

<div class="mk-star-diary">
	{if $starDiaryError}
	<div class="lg-not-found">
		<p class="title">OOPS - WE COULDN'T FIND ANY MATCHES!</p>
		<p>The Edition you are trying to view is either removed or doesnt exist	</p>
	</div>
	{else}
	<div id="style-diray">
		<h1 >{$starNStyleName}'s DIARY</h1>
		<div class="rs-carousel star-diary-carousel mk-hide">
		{foreach name=page key=key item=page from=$starDiary}
		<div class="mk-f-left diary-note">
			<h3>{$page->name}</h3>
			<p>{$page->note}</p>
		</div>
		<div class="mk-f-right diary-image">
			<img src="{if $page@first}{$page->image}{else}{$cdn_base}/skin2/images/spacer.gif{/if}" {if $page@first}{else}class="lazy"  data-lazy-src="{$page->image}"{/if}  alt="{$page->name} "/>
		</div>
		{/foreach}
		</div>
	</div>
	{/if}
</div>

{/block}

{block name=pagejs}
    {literal}
	<script>
	var lgPage='sd';
</script>
{/literal}
{/block}
