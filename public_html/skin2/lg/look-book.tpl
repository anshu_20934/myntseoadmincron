{extends file="layout.tpl"}

{block name=navigation}
	{if $layoutVariant eq 'test'}
		{include file="inc/navigation-default.tpl"}
	{else}
		{include file="inc/navigation-collapsed.tpl"}
	{/if}
{/block}

{block name=body}
<div id="lg-container">
{if $lookNotFound}
	<div class="lg-not-found">
		<p class="title">OOPS - WE COULDN'T FIND ANY MATCHES!</p>
		<p>The Look Book you are trying to view is either removed or doesnt exist	</p>
	</div>
{else}
	<h1>{$lookBookName}</h1>
	<div class="lg-look-book-details">
	{foreach name=lookDetail key=key item=lookDetail from=$lookDetails}
		<div class="lg-look-details {if $lookDetail.styleData|count > 0}scroll{/if}" id="look-{$lookDetail.id}">
			<div class="look-detail {if $lookDetail.isRightAligned}left-align{else}right-align{/if} {if $lookDetail.isTextWhite}white{/if}">
				<!--<span class="cur-page">LOOK <em>{$smarty.foreach.lookDetail.iteration}</em> / {$totalLooks}</span>-->
				<h3>{$lookDetail.name}</h3>
				<p class="desc">{$lookDetail.description}</p>
			</div>
			{if $lookDetail.styleData|count > 0}
			
			<!--<span data-index='{$smarty.foreach.lookDetail.iteration}' class="style-display-control-top {if $lookDetail.isRightAligned}buy-look-right-align{else}buy-look-left-align{/if} {if $lookDetail.isTextWhite}buy-look-white{else}buy-look-black{/if}">SHOP THIS LOOK</span>-->
			
			{if !empty($lookDetail.link_text) and !empty($lookDetail.link_url)}
			
				<a href="{$lookDetail.link_url}" class="look-detail look-detail-hyperlink look-hyperlink {if $lookDetail.isRightAligned}hyperlink-right-align{else}hyperlink-left-align{/if} {if $lookDetail.isTextWhite}buy-look-white{/if}" onClick="_gaq.push(['_trackEvent', 'startnstyle', 'sns_lb', '{$smarty.foreach.lookDetail.iteration|cat:"_custom_url"}']);return true;" >{$lookDetail.link_text}</a>
			  
			{/if} 
			
			<div class="look-styles-cont">
				<span class="style-display-control"><span class="icon close-state"></span><em>{$lookDetail.styleData|count}</em> ITEMS IN THIS LOOK</span>
				<div class="look-styles-overlay mk-hide"></div>
				<div class="look-styles mk-hide">
					<div class="look-styles-inner rs-carousel">
						<ul class="rs-carousel-runner">
						 	{foreach name=widgetdataloop item=widgetdata from=$lookDetail.styleData}
			    			 	{if $widgetdata.product neq '' && $widgetdata.product neq ' '}
									<li data-style-id="{$widgetdata.styleid}" class="rs-carousel-item">
										{if $quicklookenabled}
											<span class="quick-look" data-widget="{$ga_component_name}" data-styleid="{$widgetdata.styleid}" data-href="{$http_location}/{$widgetdata.landingpageurl}" href="javascript:void(0)"></span>				
										{/if}
										<img class="unfetched" src="{$cdn_base}/skin2/images/spacer.gif" data-lazy-src="{myntraimage key="style_81_108" src=$widgetdata.search_image|replace:'_images_180_240':'_images_81_108'|replace:'_images_240_320':'_images_81_108'|replace:'style_search_image':'properties'}" alt="{$widgetdata.product}" />
										<div class="mk-product-brand">
											<em>{$widgetdata.global_attr_brand}</em>
										</div>
										<p class="price-text red">
											{$rupeesymbol}
											{if $widgetdata.discount > 0}
												{$widgetdata.discounted_price|number_format:0:".":","}<span class="strike gray"> {$widgetdata.price|number_format:2:".":","}</span>
											{else}
												{$widgetdata.price|number_format:0:".":","}
											{/if}
										</p>
										{if $widgetdata.discount_label}
											<p class="price-text red-text red">{include file="string:{$widgetdata.discount_label}"}</p>
										{/if}
									</li>
							 	{/if}
						 	{/foreach}
						</ul>
					</div>
					<div class="look-style-right">
						<div class="price-block">TOTAL <span class="red">{$rupeesymbol} {$lookDetail.lookDiscountedTotal}</span> {if $lookDetail.lookDiscountedTotal neq $lookDetail.lookTotal}<span class="strike gray">{$lookDetail.lookTotal}</span>{/if}</div>
						<button class="btn primary-btn buy-look" data-img-src="{$buyImageArray[$smarty.foreach.lookDetail.index]}" data-look-title="{$lookDetail.name|escape}">BUY THE LOOK</button>
						<div class="lg-social-btns unloaded" data-look-id={$lookDetail.id}>
							<div class="fb" ></div>	
	 						<div class="gp"></div>
						</div>
					</div>
				</div>
			</div>
			{/if}
		
		</div>
	{/foreach}
	</div>
	<div class="lg-look-book rs-carousel" id="lg-look-book">

		<ul class="rs-carousel-runner">
			{foreach name=lookImage key=key item=lookImage from=$lookImages}
			<li class="rs-carousel-item"><img src="{$cdn_base}/skin2/images/spacer.gif" class="unfetched" data-lazy-src="{$lookImage}" /></li>
			{/foreach}
		</ul>
	</div>
	<h1 class="more-looks-text-heading">MORE LOOKS</h1>
	<div id="all-looks">
		<div class="all-looks-inner"></div>
	</div>
	
{/if}
</div>


{/block}

{block name=pagejs}

<script>
	var lgPage='lb';
	Myntra.Data.lookThumbs={$lookThumbnails};
	Myntra.Data.lookId={$lookId};
	Myntra.Data.buy = {$buy};
</script>

{/block}
