{if $shopingFestFG}
    <div class="msg">
        You could win exciting prizes such as Galaxy Note and Volkswagen Polo.<br>
        Know more about our <a href="{$http_location}/shopfest.php" title="Myntra Shop Fest">Hot in December Contest</a>.
    </div>
{/if}
{if $confirmationConfigMsg}
		 <div id="special-coupons" class="info-block">	    	 	
	    	<p class="body-txt">
	    	
				<em >{$confirmationConfigMsg}</em><br />
			</p>
		</div>
{/if}
{if $fest_coupons}
<div id="special-coupons" class="info-block">

                {if $coupon_percent }

                        <h3>YOU HAVE WON {if $fest_coupons|count > 1}SOME{else}A{/if} CASHBACK COUPON{if $fest_coupons|count > 1}S{/if}</h3>
                        <p class="body-txt">

                                        <em >You have won a cashback coupon worth Rs. {$coupon_value}. <br />This coupon can be used for a {$coupon_percent}% off up to a maximum discount of Rs. {$coupon_value}</em>.<br />
                                </p>


                {else}

                        <h3>YOU HAVE WON {$fest_coupons|count} COUPON{if $fest_coupons|count > 1}S{/if}</h3>
                        <p class="body-txt">

                                        <em >You have won {$fest_coupons|count} coupons worth Rs. {$coupon_value} each</em><br />
                                </p>

                        {/if}

            {* we do not need this for mothers day fest
                        <p class="body-txt">
                        You can use these coupons on your future purchases at Myntra<br />
                                                                <span class="or">OR</span>
                                        <a class="share-link" href="{$http_location}/mymyntra.php?view=mymyntcredits#top">TRANSFER THEM TO YOUR FRIENDS</a>
                </p>
            *}

                                <div class="mk-mynt-promo-codes mk-active-coupons">
                                <div class="mk-coupons-content">
                                <table>
                                        <col width="120" />
                                        <col width="290" />
                                        <col width="190" />
                                        <tr class="mk-table-head">
                                                <th>Coupon</th>
                                                <th class="left">Description</th>
                                                <th>Exp Date</th>
                                        </tr>
                                        {section name=prod_num loop=$fest_coupons}
                                        <tr class="{cycle name='active_cycle' values='mk-odd,mk-even'}">
                                                <td>{$fest_coupons[prod_num].coupon|upper}</td>
                                                <td class="left">
                                                {if $fest_coupons[prod_num].couponType eq "absolute"}
                                <span class="rupees">Rs. {$fest_coupons[prod_num].MRPAmount|round}</span> off
                           {elseif $fest_coupons[prod_num].couponType eq "percentage"}
                           	{$fest_coupons[prod_num].MRPpercentage}% off
                           {elseif $fest_coupons[prod_num].couponType eq "dual"}
                           	{$fest_coupons[prod_num].MRPpercentage}% off upto
                           	<span class="rupees">Rs. {$fest_coupons[prod_num].MRPAmount|round}</span>
                        	{/if}
                       	{if $fest_coupons[prod_num].minimum neq '' && $fest_coupons[prod_num].minimum gt 0}
                           	on a minimum purchase of <span class="rupees">Rs. {$fest_coupons[prod_num].minimum|round}</span> 
                       	{/if}
						</td>
						<td>{$fest_coupons[prod_num].expire|date_format:"%e %b, %Y"}
                       	({math equation= "ceil(x/86400) - ceil(y/86400) + 1" x=$fest_coupons[prod_num].expire y=$today assign="valid_days"}{$valid_days}
                       	{if $valid_days eq 1}day{else}days{/if} to expiry)</td>
					</tr> 
					{/section}
				</table>
				</div>
			</div>
		</div>
{/if}

