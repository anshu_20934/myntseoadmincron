{if $amountafterdiscount}
	{assign var=totalAmount value=$amountafterdiscount}
{/if}
{if $ordersubtotal}
	{assign var=amount value=$ordersubtotal}
{/if}
{if $totalcashdiscount}
	{assign var=cashdiscount value=$totalcashdiscount}
{/if}
{$rs="<span class=rupees>Rs. </span>"}
<div class="order-summary mk-gc-order-summary">
    <h2>Order Summary</h2>

	<ul class="amount">
        {if $paybyoption eq 'emi' && giftCardConfirmationPage}
	        <li class="emi-charges">
	            <div class="name">EMI Processing Fee</div>
	            {if $emi_charge > 0}
	                <div class="value rupees red">{$rs} {$emi_charge|round|number_format:0:".":","}</div>
	            {else}
	                <div class="value red">Free</div>
	            {/if}
	        </li>
	    {else}
	    	<li class="emi-charges hide">
	            <div class="name">EMI Processing Fee</div>
	            {if $emi_charge > 0}
	                <div class="value rupees red">{$rs} {$emi_charge|round|number_format:0:".":","}</div>
	            {else}
	                <div class="value red">Free</div>
	            {/if}
        	</li>    
        {/if}
        
        <li class="order-total">
            <div class="name">Gift Card Value</div>
            <div class="value red">
            	{$rs} {$amount}
            </div>
            {if isset($eossSavings) && $eossSavings gt 0}
            	DISCOUNT <div class="value discount red">{$rs} {$eossSavings|round|number_format:0:".":","}</div>
            {/if}
        </li>
        {if $cashdiscount neq 0}
            <li class="cash-discount">
                <div class="name">Cashback</div>
                <div class="value discount red">(-) {$rs} {$cashdiscount|round|number_format:0:".":","}</div>
            </li>
        {/if}
        <li class="grand-total">
        	<div class="name">You {if $giftCardPaymentPage}Pay{else}Paid{/if}</div>
	    	<div class="you-pay value red">{$rs} {$totalAmount|round|number_format:0:".":","}</div>
        </li>
    </ul>

    {*<div class="you-pay total-amount red rupees">
        {$rs} {$totalAmount|round|number_format:0:".":","}
    </div>*}

    <div class="list">
    	<div class="mk-summary-gc">
    		<div class="mk-summary-gc-preview">Gift Card Preview {if !$giftCardConfirmationPage}<a href="{$http_location}/giftcards">edit</a>{/if}</div>
    		<div class="mk-summary-gc-thumbnail">
    			<img class="mk-summary-gc-thumb-img" src="{if $smarty.server.HTTPS}{$secure_cdn_base}{else}{$cdn_base}{/if}/{$giftCardOccasion->previewImageThumbnail}">
    		</div>
    		<div class="mk-summary-gc-delivery-mode">Delivery Mode: <span>Send Via Email</span></div>	
    	</div>
    </div>
</div>

