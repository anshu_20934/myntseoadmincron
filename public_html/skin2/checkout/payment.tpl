{extends file="layout-checkout.tpl"}

{block name=body}
<script>
Myntra.Data.pageName="payment";
Myntra.Data.OrderType="normal";
Myntra.Data.countryList = {if $countriesJson}{$countriesJson}{else}''{/if};
Myntra.Payments = Myntra.Payments || {};
Myntra.Payments.showPayHelpline = '{$payHelpline}' == 'test' ? true : false;
Myntra.Payments.showPayFailOpt= '{$payFailOpt}' == 'test' ? true : false;
Myntra.Payments.payUserCloseOpt= '{$payUserCloseOpt}' == 'test' ? true : false;
Myntra.Payments.showPayZipOrders= '{$payUserCloseOpt}' == 'test' ? true : false;
Myntra.Payments.codErrorCode= '{$codErrorCode}' == '0' ? true : false;
Myntra.Payments.isGiftCardOrder = false;

var ispayUserCloseOpt = '{$payUserCloseOpt}';
var isGiftCardOrder = 0;
var hasAddress = 1;
{if $paymentPageUserTimeLimit}
Myntra.Payments.paymentPageUserTimeLimit= '{$paymentPageUserTimeLimit}';
{/if}
{if !$customeragent}
    var telesalesUser = true;
{else}
    var telesalesUser = false;
{/if}
</script>
<div class="checkout payment-page mk-cf">
    <h1>Payment Options</h1>
	<h3>100% Safe & Secure Shopping / Pay with Confidence</h3>
	{if $payHelpline == 'test'}
		<div class="pp-helpline gray">
	    	<div>NEED HELP?</div>
	    	<div>
	    		<span class="icon-phone"></span>
	    		<span id="phone-no">{$customerSupportCall}</span>
	    	</div>
	    </div>
	{/if}
    {* mainContent *}
    <div class="main-content">
    	{if $payFailOpt == 'control'}
		    {include file="checkout/paymentError.tpl"}
		{/if}

        {if $citi_discount > 0}
            <div id="citi_discount_div"> Avail {$citi_discount}% discount by paying with your Citibank <a href="#pay_by_dc" onClick="$('#pay_by_dc').click();return false;">Debit Card</a></div>
        {/if}

        {if $icici_discount > 0}
            <div id="icici_discount_div"> Avail {$icici_discount}% discount on payment through <img alt="ICICI" src="{$secure_cdn_base}/skin1/images/ICICI.png"> (credit card, debit card or netbanking)</div>
        {/if}

        {* pay button is used in top and bottom. so, capture is used here *}
        {capture name="pay_button"}
        <div class="pay-btn-wrap">
            <a href="#paybtn" {if $totalAmount eq 0} onclick="return submitPaymentForm(false);" {elseif $is_icici_gateway_up} onclick="return submitPaymentForm(true);" {else} onclick="return submitPaymentForm(false);" {/if} class="btn primary-btn pay-btn">
                 {if $totalAmount eq 0}
                    Confirm Order
                 {elseif $is_icici_gateway_up}
                    {if $paymentType == "cod" }
                        Confirm Order
                    {else}
                        Pay Now
                    {/if}
                 {else}
                    Proceed to Payment
                 {/if}
            </a>
        </div>
        {/capture}

        {*$smarty.capture.pay_button*}

        <div class="coupon-wrap mk-cf">
            {include file="cart/coupon_widget.tpl"}
        </div>
        
        {include file="giftcards/giftcards-activation-widget-placeholder.tpl"}

        {*PyamentBlock*}
        {if $paymentServiceEnabled}
        	{$paymentPageUI}
        {else}
        <div class="pay-block">
            <!--PORTAL-667 Configurable Order of payment options-->
            {if $totalAmount eq 0}
                {assign var="paymentType" value="credit_card"}
            {else}
                {assign var="paymentType" value="$defaultPaymentType"}
	            <h2>Select a payment mode</h2>
            {/if}

            <input type=hidden id="paymentFormToSubmit" value="{$paymentType}">
            <input type=hidden id="codErrorCode" value="{$codErrorCode}">
            <input type=hidden id="addressPincode" value="{$address.pincode}">

            <input type=hidden id="customer-shipping-name" value="{$address.name}">
            <textarea style="display:none" id="customer-shipping-address">{$address.address}</textarea>
            <input type=hidden id="customer-shipping-locality" value="{$address.locality}">
            <input type=hidden id="customer-shipping-city" value="{$address.city}">
            <input type=hidden id="customer-shipping-state" value="{$address.state}">
            <input type=hidden id="customer-shipping-statename" value="{$address.statename}">
            <input type=hidden id="customer-shipping-country" value="{$address.country}">
            <input type=hidden id="customer-shipping-countryname" value="{$address.countryname}">
            <input type=hidden id="customer-shipping-pincode" value="{$address.pincode}">
            <input type=hidden id="customer-shipping-mobile" value="{$address.mobile}">

            {if $totalAmount eq 0}
                <form action="{$https_location}/{$paymentphp}" method="post" id="credit_card">
	                <input type="hidden" name='s_option' value ='{$logisticId}'/>
    	            <!--delivery preference-->
    	            <input type="hidden" name="shipment_preferences" value="shipped_together" >
    	            <!--delivery preference-->
    	            <input type="hidden" name="is_shipping_cost_set" value="{$s_city}">
    	            <input type="hidden" name="checksumkey" value="{$checksumkey}">
    	            <input type="hidden" name="address" value="{$address.id}">
    	            <input type="hidden" value="creditcards" name="pm" />
	            	<div class="nothing-to-pay">
	            		You have no outstanding amount to pay.<br />
	            		Click 'Confirm Order' to proceed.
	            	</div>
                </form>
            {else}
	            {*Tabs*}
	           {* {if $EMIenabled}
	            	<span class="opt-new"></span>
	            {/if}*}
	            	
	            <div class="tabs">
	                <ul>
                        {foreach from=$payment_options item=pmt_option}
                            {if $pmt_option eq "credit_card"}
                                <li id="pay_by_cc" {if $defaultPaymentType eq "credit_card"} class="selected" {/if}>
                                    <span class="opt"></span>Credit Card</li>
                            {elseif $pmt_option eq "emi"}
                                <li id="pay_by_emi" {if $defaultPaymentType eq "emi"} class="selected" {/if}>
                                    <span class="opt"></span>EMI<br />(Credit Card)</li>                            
                            {elseif $pmt_option eq "debit_card"}
                                <li id="pay_by_dc" {if $defaultPaymentType eq "debit_card"} class="selected" {/if}>
                                    <span class="opt"></span>Debit Card</li>
                            {elseif $pmt_option eq "net_banking"}
                                <li id="pay_by_nb" {if $defaultPaymentType eq "net_banking"} class="selected" {/if}>
                                    <span class="opt"></span>Net Banking</li>
                            {elseif $pmt_option eq "cod"}
                                <li id="pay_by_cod" {if $defaultPaymentType eq "cod"}  class="selected" {/if}>
                                    <span class="opt"></span>Cash on Delivery</li>
                            {elseif $pmt_option eq "pay_by_phone"}
                                <li id="pay_by_phone" {if $defaultPaymentType eq "pay_by_phone"}  class="selected" {/if}>
                                    <span class="opt"></span>Phone</li>
                            {/if}
                        {/foreach}
	                </ul>
	            </div>
	            {*Tabs@End*}

	            {*Tabs content*}
	            <div class="tab-content mk-cf">
	                {foreach from=$payment_options item=pmt_option}
	                    {if $pmt_option eq "credit_card"}
	                        {include file="cart/form_pmt_creditcard.tpl"}
                        {elseif $pmt_option eq "emi"}
                               {include file="cart/form_pmt_emi.tpl"}	                        
	                    {elseif $pmt_option eq "debit_card"}
	                        {include file="cart/form_pmt_debitcard.tpl"}
	                    {elseif $pmt_option eq "net_banking"}
	                        {include file="cart/form_pmt_netbanking.tpl"}
	                    {elseif $pmt_option eq "cod"}
	                        {if $codWindow eq "displayCaptchaScreen"}
	                            {include file="cart/form_pmt_cod.tpl"}
	                        {else}
	                            {include file="cart/form_pmt_mobile_verify.tpl"}
	                        {/if}
	                    {elseif $pmt_option eq "pay_by_phone"}
	                        {include file="cart/form_pmt_pbp.tpl"}
	                    {/if}
	                {/foreach}
	            </div>
	            {*Tabs content@End*}
            {/if}
        </div>
        
        

        {$smarty.capture.pay_button}        
        {*PyamentBlock@End*}
        {/if}
        {if $payZipOrders == 'test' and $payZipOrdersFlag}
			{include file="checkout/pincode_city_orders.tpl"}
        {/if}
    </div>
    {*mainContent@End*}
	
    {include file="checkout/summary.tpl" flow="payment_page"}

{*    <div id="myOnPageContent" style="display:none;">
        <table cellpadding="0" cellspacing="0" width="100%" class="my-c-table" style="margin-top:10px;border:1px solid #ccc;">
            <tbody>
                <tr>
                    <th>Select</th>
                    <th>Coupon code</th>
                    <th>coupon description</th>
                    <th>Expires on</th>
                    <!-- <th>Discount on Cart Contents</th> -->
                </tr>
                {section name=prod_num loop=$rest_coupons}
                    <tr class="even">
                        <td><input name="select" type="radio" onClick="javascript:redeemCoupon2('{$rest_coupons[prod_num].coupon}');"> </td>
                        <td>{$rest_coupons[prod_num].coupon}</td>
                        <td>{$rest_coupons[prod_num].description}</td>
                        <td class="td-cr">{$rest_coupons[prod_num].expire|date_format:$config.Appearance.date_format}</td>
                        <!-- <td class="td-last td-rs">170.00</td> -->
                    </tr>
                {/section}
            </tbody>
        </table>
    </div>
*}

</div>
{/block}

{block name=lightboxes}
<!-- commenting it out so that not to use pull method mobile verification
<div id="splash-mobile-verify" class="lightbox">
<div class="mod">
    <div class="hd">
        <h2>VALIDATE MOBILE NUMBER</h2>
    </div>
    <div class="bd clearfix">
        <div class="verify-sms">
            Please send an SMS within 10 minutes from your mobile number (<span id='userMobile'>{$userMobile}</span>) with the text:<br>
            <strong>VERIFY</strong><br>
            to this number:<br>
            <strong>0-92824-24444</strong>
        </div>
        <span class="mnote">* Standard SMS charges apply</span>

        Once you send the SMS, your number will be validated in the next 120 seconds.<br>
        In case you want to validate another number, please go to your My Myntra Profile to update your number before trying again.<br>
        <button class="btn normal-btn ok-btn" type="button">OK</button>
    </div>
</div>
</div>
-->
<div id="coupon-info" class="lightbox infobox">
	<div class="mod">
		<div class="hd">
			<div class="title">How do I Apply a Coupon?</div>
		</div>
		<div class="bd body-text">
			<p></p>
			<ol>{if $condNoDiscountCouponFG eq 1 || $styleExclusionFG eq 1}
					{assign var="message" value=$couponExcludedProductsMsg}
					<li>{$message}</li>
					<li>Most coupons can be applied only when the value of the products in your bag is above a minimum value. The minimum value is calculated after applying all discounts.</li>
				{else}
					<li>Most coupons can be applied only when the value of the products in your bag is above a minimum value. The minimum value is calculated after applying all discounts.</li>
				{/if}
			</ol>
		</div>
		<div class="ft">
			<p></p>
		</div>
	</div>
</div>
{* Do not change id of any div below it is used in orderProcessing.tpl *}
<div id="splash-payments-processing" class="lightbox">
	<div class="mod" id="processing-payment">
	    <div class="hd" id="splash-payments-processing-hd">
	        <h2>Processing Payment</h2>
	    </div>
	    <div class="bd clearfix" id="splash-payments-processing-bd">
	    	<center><img src="{$secure_cdn_base}/images/image_loading.gif"></center>
	        <div class="processing-payments" id="splash-payments-processing-bd-pp">
	            Enter your card/bank authentication details in the new window once it appears on your screen. <br/>
				Please do not refresh the page or click the back button of your browser.<br/>
				You will be redirected back to order confirmation page after the authentication. It might take a few seconds.<br/><br/>
	        </div>		

	        <center id="cancel-payment-btn"><button class="action-btn cancel-payment-btn" type="button">Cancel Payment</button></center>
	    </div>
	</div>
	<div class="mod mk-hide" id="result-succPayment">	 
    	<div class="hd" id="splash-payments-processing-hd-succPayment">
    		<h2>Payment Successful</h2>
        </div>
        <div class="bd clearfix" id="splash-payments-processing-bd-succPayment">
        	<center><img src="{$secure_cdn_base}/images/image_loading.gif"></center>
    		<div class="processing-payments bd" id="splash-payments-processing-bd-pp-succPayment">    
    			Please wait while redirecting       
       		</div>    
       	</div>    
    </div>
    <div class="mod mk-hide" id="result-errPayment">	 
    	<div class="hd" id="splash-payments-processing-hd-errPayment">
    		<h2>Payment Failure</h2>
        </div>
        <div class="bd clearfix" id="splash-payments-processing-bd-errPayment">
        	<center><img src="{$secure_cdn_base}/images/image_loading.gif"></center>
    		<div class="processing-payments bd" id="splash-payments-processing-bd-pp-errPayment">    
    			Please wait while redirecting       
    		</div>
        </div>        
    </div>
    
{if $payFailOpt == 'test'}
	<div class="mod mk-hide" id="result-payFailOver-err">	 
    	<div class="hd" id="splash-payments-processing-hd-payFailOver-err">
    		<h2>Payment Failed</h2>
        </div>
        <div class="bd clearfix" id="splash-payments-processing-bd-payFailOver-err">
    		<div class="processing-payments bd" id="splash-payments-processing-bd-pp-payFailOver-err">
    		</div>    		      
        </div>        
    </div>
{/if}

{if $payUserCloseOpt == 'test'}
	<div class="mod mk-hide" id="result-payUserClose">	 
    	<div class="hd" id="splash-payments-processing-hd-payUserClose">
    		<h2>Payment Cancelled</h2>
        </div>
        <div class="bd clearfix" id="splash-payments-processing-bd-payUserClose">
    		<div class="processing-payments bd" id="splash-payments-processing-bd-pp-payUserClose"><center>	 
    			You have cancelled your payment. You can try to pay again {if $codErrorCode eq 0}or alternatively pay by COD{/if}</center>
	    		<div class="pay-btn-wrap inline-btns">
				{if $codErrorCode eq 0}
				   	<a class="btn primary-btn codbtn">
		    		Pay by COD
		     		</a>
		     	{/if}
		     	<a class="btn primary-btn tryagainbtn">
		     		Try Again
		    	 </a>			    
		 		</div>
		 		<div class="close"></div>	  		
    		</div>    		      
        </div>        
    </div>
{/if}
</div>

{include file="giftcards/giftcards-activation-widget.tpl"}
{/block}

{block name=pagejs}
<script type="text/javascript">

	Myntra.Payments = Myntra.Payments || {};
	Myntra.Payments.amount = '{$rupeesymbol} {$totalAmount|round|number_format:0:".":","}';
	var total_amount = 	{$totalAmount};
	var shipping_countrycode = "{$address.country|escape:'html'}";
	var codErrorCode = {if $codErrorCode}{$codErrorCode}{else}""{/if};
    if(codErrorCode==2){
        _gaq.push(['_trackEvent', 'payment_page', 'cod_unserviceable_zipcode', $('#addressPincode').val()]);
    }
	{if $onlineErrorMessage } var onlinePaymentEnabled = 0; {else} var onlinePaymentEnabled = 1; {/if}
	var codTabClicked = false;
	{literal}
	var toggle_text_elements = {};
	{/literal}
	var loginid = "{$loginid}";
	var citi_discount = {$citi_discount};
	var icici_discount = {$icici_discount};
	var icici_discount_text = "Avail {$icici_discount}% discount on payment through <img alt='ICICI' src='{$secure_cdn_base}/skin1/images/ICICI.png'> (credit card, debit card or netbanking)";
	var oneClickCod = {$oneClickCod};
	{if $is_icici_gateway_up}
	var is_icici_gateway_up = true;
	{else}
	var is_icici_gateway_up = false;
	{/if}
	{if $shippingRate neq 0}
		{assign var=amountToDiscount value=$totalAmount-$shippingRate}
		var amountToDiscount = {$amountToDiscount};
	{else}
		var amountToDiscount = {$totalAmount};
	{/if}

	{if $citi_discount>0}
		var	citiCardArray = new Array();
		{foreach from=$citi_discount_bins item=array_item key=id}
			citiCardArray.push('{$array_item}');
		{/foreach}
	{/if}

	{if $icici_discount>0}
		var iciciCardArray = new Array();
		{foreach from=$icici_discount_bins item=array_item key=id}
			iciciCardArray.push('{$array_item}');
		{/foreach}
	{/if}


	{if !$paymentServiceEnabled}
		var emiEnabled = false;
		{if $EMIenabled}
			emiEnabled = true;
		
			Myntra = Myntra || {};
			Myntra.Payments = Myntra.Payments || {};
			Myntra.Payments.EMICardArray = {};
			{foreach from=$EMIBanksAvailable item=emibank key=bank}
				(function() {
					var binArray = new Array();
					{foreach from=$emibank.Bins item=array_item key=id}
						binArray.push('{$array_item}');
			{/foreach}
					Myntra.Payments.EMICardArray['{$bank}'] = binArray;
				})();
			{/foreach}
		{/if}
	{/if}


	var mobileVerificationSubmitCount = 0;
	var codCharge = {$cod_charge};
	var orderSummaryDislplayed=false;
	{if $cashcoupons.MRPAmount>0}
		var cashBackAmount = {$cashcoupons.MRPAmount};
	{else}
		var cashBackAmount = 0;
	{/if}

	var userMobileNumber = "{$userMobile}";
	var paymentChildWindow;
	var checkPaymentWindow;
	var paymentProcessingPopup;
	$('.your-coupons .tt-ctx').each(function(el, i) {
		var ctx = $(this), 
			tip = ctx.children('.tt-tip');
		Myntra.InitTooltip(tip, ctx,{ldelim}side:'right'{rdelim});
	});
	{*$('a.coupon-info-link').each(function(el,i){ldelim}$(this).data('lb', Myntra.LightBox('#coupon-info'));{rdelim});
	$('a.coupon-info-link').each(function(el,i){ldelim}$(this).click(function(){ldelim}$(this).data('lb').show();{rdelim}){rdelim});*}
	
	$('a.coupon-info-link').data('lb', Myntra.LightBox('#coupon-info'));
	$('a.coupon-info-link').click(function(){ldelim}$(this).data('lb').show();{rdelim});
	
</script>
{/block}
{block name=macros}
    {include file="macros/cart-macros.tpl"}
{/block}
