{extends file="layout-checkout.tpl"}

{block name=lightboxes}
	{include file="inc/modal-quick-look.tpl"}
	{if not $login}
		{include file="inc/modal-signup.tpl"}
	{/if}

       <div id="cashback-info" class="lightbox infobox">
	 <div class="mod">{$cashBackInfo}</div>
       </div>

{/block}


{block name=body}
<script>
Myntra.Data.pageName="cart";
Myntra.Data.socialAction='cart-wishlist';
var socialPost = {$socialPost};
var socialPostArray={$socialArray};
var socialPostStyleDetailsArray={$socialPostStyleDetailsArray};
{if $recommendationsCart == 'test'}
	{if $recProductsInCart == 'true'}
		var avail_dynamic_param1 = '{$gender}';
		var availRecLowerLimit = {$availRecLowerLimit};
		var availRecHigherLimit = {$availRecHigherLimit};
		var dre_cart_minMore = 999999;
		{if $dre_cart_minMore}
			dre_cart_minMore = {$dre_cart_minMore};
		{/if}
		var cartRecommendationsAmount = {$cartRecommendationsAmount};
		var dre_cart_maxMore = dre_cart_minMore + (dre_cart_minMore * cartRecommendationsAmount/100);
		if(dre_cart_minMore < availRecHigherLimit)
		{
			availRecLowerLimit = dre_cart_minMore;
			availRecHigherLimit = dre_cart_maxMore;
		}
		var cartRecommendationsHighLimit = {$cartRecommendationsHighLimit};
		if(availRecHigherLimit < cartRecommendationsHighLimit)
			availRecHigherLimit = cartRecommendationsHighLimit;
		var productid = {$productid};
		var avail_dynamic_param_match_with ='{$avail_dynamic_param_match_with}';
	{/if}
{/if}	
Myntra.Cart = Myntra.Cart || {};
Myntra.Cart.showRec = '{$recommendationsCart}' == 'test' ? true : false;


var recommendationsCart = '{$recommendationsCart}';
var recProductsInCart = '{$recProductsInCart}';
</script>
{$rs="<span class=rupees>Rs. </span>"}
<div class="checkout cart-page">
	<div class="cart-list">
		{if $totalItems eq 0 && !$login}
			<div class="cart-sign-in-msg">ALREADY HAVE A MYNTRA ACCOUNT?<br/>
				<div class="cart-sign-in-sub-msg">PLEASE <span class="btn-login">LOGIN</span> TO VIEW SHOPPING BAG & WISHLIST ITEMS
				THAT YOU MAY HAVE ADDED EARLIER.</div>
			</div>
		{/if}
		<h2 id="bag">My Shopping Bag
			{if $totalItems gt 0}
				<span class="l-gray">/</span> <span class="gray">{$totalItems} {if $totalItems eq 1}Item{else}Items{/if}</span>
			{/if}
		</h2>
		{if $totalItems gt 0}
			{if $myntraShopFestCoupons}
				<div class="cart-discount-msg">
					{if $cartOfferMessage1}
						<span style="font-size:14px">{$cartOfferMessage1}</span><br/>
					{/if}
					{if $cartOfferMessage2}
						<span style="font-size:12px">{$cartOfferMessage2}</span>
					{/if}
				</div>
			{/if}			
			
			{capture name="cart_discount_msg"}
			{if $cartDiscountMessage }
				<div class="cart-discount-msg">
					{include file="string:{$cartDiscountMessage}"}						
					{if $cartDiscountExclusionTT}
					&nbsp; &nbsp; <span id="cart-discount-exclusion-tt" style="color:#808080;text-decoration:underline;" title='{include file="string:{$cartDiscountExclusionTT}"}' >
						Conditions Apply</span>
					{/if}
				</div>
			{/if}
			{if $cartSlabDiscountMessage }
				<div class="cart-discount-msg">
					{assign dre_cart_percent "$dre_cart_slab_percent"}
					{assign dre_cart_amount "$dre_cart_slab_amount"}
					{assign dre_cart_minMore "$dre_cart_slab_minMore"}					
					{assign dre_cart_buyAmount "$dre_cart_slab_buyAmount"}
					{assign dre_cart_buyCount "$dre_cart_slab_buyCount"}
					{assign dre_cart_discountId "$dre_cart_slab_discountId"}
					{assign dre_cart_freeItemsMaxWorth "$dre_cart_slab_freeItemsMaxWorth"}
					{assign dre_cart_itemName "$dre_cart_slab_itemName"}
					{assign dre_cart_itemPrice "$dre_cart_slab_itemPrice"}
									
					{include file="string:{$cartSlabDiscountMessage}"}
					
				</div>
			{/if}
			{/capture}
			{$smarty.capture.cart_discount_msg}
			{$has_error = ($checkInStock and ($items_out_of_stock or $items_zero_quantity or $product_intotal_out_of_stock))}
	
			{capture name=cart_total}
				<div class="total">
					Total <span class="rupees red">{$rs} {$totalAmount|round|number_format:0:".":","}</span>
					{if $totalAmount lt $amount}
						<span class="strike gray mrp-total">{$amount|round|number_format:0:".":","}</span>
					{/if}
				</div>
				{if isset($eossSavings) && $eossSavings gt 0 && $eossSavings eq $savings}
					<div class="savings">Item Discount <span class="rupees red">{$rs} {$eossSavings|round|number_format:0:".":","}</span></div>
				{elseif isset($cartLevelDiscount) && $cartLevelDiscount gt 0 && $cartLevelDiscount eq $savings}
					<div class="savings">Bag Discount <span class="rupees red">{$rs} {$cartLevelDiscount|round|number_format:0:".":","}</span></div>
				{elseif isset($myntCashUsage) && $myntCashUsage gt 0 && $myntCashUsage eq $savings}
					<div class="savings">Cashback <span class="rupees red">{$rs} {$myntCashUsage|round|number_format:0:".":","}</span></div>
				{elseif isset($couponDiscount) && $couponDiscount gt 0 && $couponDiscount eq $savings}
					<div class="savings">Coupon <span class="rupees red">{$rs} {$couponDiscount|round|number_format:0:".":","}</span></div>
				{elseif $savings gt $eossSavings}
					<div class="savings-hdr">
						<span class="ico ico-expand"></span>
						<span class="savings">Savings <span class="rupees">{$rs} {$savings|round|number_format:0:".":","}</span></span>
					</div>
					<div class="savings-details hide">
						{if $eossSavings gt 0}<div class="eossDis">Item Discount <span>{$rs} {$eossSavings|round|number_format:0:".":","}</span></div>{/if}
						{if $cartLevelDiscount gt 0}<div class="eossDis">Bag Discount <span>{$rs} {$cartLevelDiscount|round|number_format:0:".":","}</span></div>{/if}
						{if $myntCashUsage gt 0}<div class="cashback">Cashback <span>{$rs} {$myntCashUsage|round|number_format:0:".":","}</span></div>{/if}
						{if $couponDiscount gt 0}<div class="coupon">Coupon <span>{$rs} {$couponDiscount|round|number_format:0:".":","}</span></div>{/if}
					</div>
				{/if}
			{/capture}
	
			<div class="row header first">
				<div class="col1 count">
					<a class="btn normal-btn btn-continue-shopping" href="/" onclick="_gaq.push(['_trackEvent', 'cart', 'continue_shopping']);return true;">Continue Shopping</a>
				</div>
				<div class="col2 action">
					<div class="gray">Apply your <span class="red">Cashback & Coupons</span> on the Payment Page</div>
					{if $styleExclusionFG}
						<h4 class="coupon-app-info"> COUPONS ARE NOT APPLICABLE ON 
						<span id="coupon-exclusion-tt" style="color:#808080;text-decoration:underline;" 
						title='{$couponExclusionTT}' >
						SOME PRODUCTS</span></h4>
					{elseif $condNoDiscountCouponFG }
						<h4 class="coupon-app-info"> COUPONS CAN BE APPLIED <span class="red">ONLY</span> ON <span class="red">NON-DISCOUNTED</span> ITEMS</h4>
					{/if}
				</div>
				<div class="col3">
					<button class="btn primary-btn btn-place-order" onclick="_gaq.push(['_trackEvent', 'cart', 'place_order']);s_analytics.linkTrackEvents=s_analytics.events='scCheckout';s_analytics.tl(this,'o','Checkout Initiated');return true;"
							{if $has_error}disabled="disabled"{/if}>Checkout Securely <span class="proceed-icon"></span></button>
					{* {if $totalAmount gt $cashBackThreshold && $myntraShopFestCoupons}
						<a href="/" class="red earn-cashback">Earn {$rs} {$totalAmount|round|number_format:0:".":","} cashback </a>
					{/if} *}

					{$smarty.capture.cart_total}
					
				</div>
				{if (($giftWrapEnabled || $isGiftOrder) && $totalItems gt 0)&&!$telesales}
					<div class="gift-holder">				
						<div class="gift-chkbox-div"> 
							<input type='checkbox' name="gift-wrap-select" class="gift-wrap-cart gift-wrap-select" {if $isGiftOrder}checked="true" {/if}/> Gift wrap this order for <em>Rs.</em> {$giftingCharges} 
{* <span class="red">NEW</span> *}
							<span {if $isGiftOrder}data-gift-to="{$giftTo}" data-gift-from="{$giftFrom}" data-gift-msg="{$giftMessage|escape}" class="gift-data gift-msg-edit" {else}class="mk-hide gift-msg-edit"{/if}>EDIT MESSAGE</span>
														 
						</div>
						<p class="gift-wrap-info">
							Cash on Delivery not available for gift orders
						</p>		
					</div>				
				{/if}
			</div>
	
			{* BEGIN: success messages *}
			{if $pname}
				<div class="success page-success">
					<span class="success-icon"></span> Added to bag: <strong>{$pname}</strong>
				</div>
			{/if}
			{* END: success messages *}
	
			{* BEGIN: error messages *}
			{if $redirectSource eq 'mi'}
				<div class="err page-error">
					It seems your cart details have been modified during the checkout process.<br>
					Please confirm the details below and proceed.
				</div>
			{/if}
	
			{if ($checkInStock) and ($items_out_of_stock or $items_zero_quantity or $product_intotal_out_of_stock)}
				{if $items_zero_quantity}
					<div class="err page-error">
						Zero units have been ordered for some items. Please enter a larger number or click 'Remove'.
					</div>
				{elseif $product_intotal_out_of_stock}
					<div class="err page-error">
						We're sorry, we don't have enough stock to process your entire order - Please review the items.
					</div>
				{else}
					<div class="err page-error">
						<span class="error-icon"></span>Some items in your bag are out of stock - Please review them to place your order.
					</div>
				{/if}
			{/if}
			{* END: error messages *}
	
			{* BEGIN: list of items *}
			{* cart_view should be complete|summary. base on this the tpl will render the markup *}
			{$cart_view="complete"}
			{include file="checkout/cart-details.tpl"}
			{* END: list of items *}
	
			<div class="shipping-item mk-cf">
				<div class="col1">&nbsp;</div>
				{if $shippingCharge > 0}<div class="col2">Buy for <span class="rupees">{$rs}</span> {$free_shipping_amount} or more and make your shipping <span class="discount">FREE</span>!</div>{/if}
				<div class="col3">Shipping {if $shippingCharge > 0}Fee <span class="rupees red">{$rs} {$shippingCharge|round|number_format:0:".":","}</span>{else}<span class="rupees red">FREE</span>{/if}
					{if ($giftWrapEnabled && $isGiftOrder) && $totalItems gt 0}
						<div class="gift-wrap-charges">Gift wrap charge <span class="rupees red">{$rs} {$giftingCharges|round|number_format:0:".":","}</span></div>
					{/if}
				</div>
			</div>
				
			<div class="total-row mk-cf">
				<div class="col1">&nbsp;</div>
				<div class="col2">&nbsp;</div>
				<div class="col3">{$smarty.capture.cart_total}</div>
			</div>

			<div class="row header">
				<div class="col1 count">
					<a class="btn normal-btn btn-continue-shopping" href="/" onclick="_gaq.push(['_trackEvent', 'cart', 'continue_shopping']);return true;">Continue Shopping</a>
				</div>
				<div class="col2 action">
					&nbsp;
				</div>
				<div class="col3">
					<button class="btn primary-btn btn-place-order" onclick="_gaq.push(['_trackEvent', 'cart', 'place_order']);s_analytics.linkTrackEvents=s_analytics.events='scCheckout';s_analytics.tl(this,'o','Checkout Initiated');return true;"
							{if $has_error}disabled="disabled"{/if}>Checkout Securely <span class="proceed-icon"></span></button>
				</div>
			</div>
		{else}
			<div class="empty-cart">
				<div class="err">Your Shopping Bag is empty</div>
				<div class="action">
					<a href="/" class="btn normal-btn btn-continue-shopping">Continue Shopping</a>
				</div>
			</div>
		{/if}
	</div>	
	{if $recommendationsCart == 'test'}
		<br/>
		{$smarty.capture.cart_discount_msg}
 		<div class="cart-recommend-block">
			<img src="http://myntra.myntassets.com/skin2/images/loader_150x200.gif" alt="loading" class="loader">
		</div>
	{/if}
	<div class="save-list">
		<h2 id="wishlist">My Wishlist
			{if $savedItemCount gt 0}
				<span class="l-gray">/</span> <span class="gray">{$savedItemCount} {if $savedItemCount eq 1}Item{else}Items{/if}</span>
		   	{/if}
	   	</h2>
	   	<h3 class="gray">Your Personal List of Items that you would like to Buy from Myntra</h3>
		{if $login && $savedItemCount gt 0}
			{include file="checkout/saved-details.tpl"}
		{else}
			<div class="empty-saved">
				<div class="err">
					Your wishlist is empty
				</div>
				{if $totalItems gt 0 && !$login}
					<div class="cart-sign-in-sub-msg">Please <span class="btn-login">Login</span> to view your Wishlist Items that you may have added earlier.</div>
				{/if}
			</div>
		{/if}
	</div>

</div>

	<!-- SHopping Cart recommendation -->
	{if $recommendationsCart != 'test'}
	<div class="cart-recommend-block">
		<img src="http://myntra.myntassets.com/skin2/images/loader_150x200.gif" alt="loading" class="loader">
	</div>
	{/if}

{/block}

{block name=pagejs}
{/block}

{block name=macros}
    {include file="macros/cart-macros.tpl"}
{/block}
