{if $loyaltyEnabled && $totalLoyaltyPointsAwarded}
<div class="loyalty-block">
    <div class="loyalty-logo">
      <span class="icon-my-privilege"></span>
    </div>
    <div class="loyalty-award-heading">You have earned {$totalLoyaltyPointsAwarded} Point{if $totalLoyaltyPointsAwarded gt 1}s{/if}</div>
    <div class="loyalty-award-msg">
        for this order. <a onclick="_gaq.push(['_trackEvent', 'confirmation', 'check_privilege_points']);return true;" href="{$http_location}/mymyntra.php?view=myprivilege">Know more</a>
    </div>
    <div class="loyalty-award-info gray">{$loyaltyDisclamer}</div>
    {* commenting for now 
    <div class="loyalty-points-breakup clearfix hide">
        <div class="breakup-big first-block">
            <div class="breakup-round-container gray fright">10 %</div>
            <div class="breakup-cal-desc fright">
                or 10 points for every Rs. 100 (as per your level)
            </div>
        </div>
        <div class="breakup-small">
            <div class="breakup-operator">x</div>
        </div>
        <div class="breakup-big ">
            <div class="breakup-round-container gray breakup-round-main">
                Rs. 
            </div>
            <div class="breakup-cal-main">
                <table>
                    <tr>
                        <td class="b-captions">Bag total</td>
                        <td class="b-values"></td>
                    </tr>
                    <tr>
                        <td class="b-captions">Coupon discount</td>
                        <td class="b-values"></td>
                    </tr>
                    <tr>
                        <td class="b-captions">Privilege Points</td>
                        <td class="b-values"></td>
                    </tr>
                    <tr>
                        <td class="b-captions">Delivery Charges</td>
                        <td class="b-values"></td>
                    </tr>
                    <tr>
                        <td class="b-captions">Vat Collected</td>
                        <td class="b-values"></td>
                    </tr>
                    <tr>
                        <td class="b-captions">Gift Wrap</td>
                        <td class="b-values"></td>
                    </tr>
                    <tr class="b-total">
                        <td class="b-captions">Total</td>
                        <td class="b-values"></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="breakup-small">
            <div class="breakup-operator">=</div>
        </div>
        <div class="breakup-big">
            <div class="breakup-round-container loyal-points gray fleft"> points</div>
            <div class="breakup-cal-desc fleft">
                added to your pending<br/>points
            </div>
        </div>
    </div>
    <a class="show-hide-breakup">Show Calculation</a>
    *}
</div>
{/if}