{extends file="layout-checkout.tpl"}

{block name=body}
<script>
Myntra.Data.pageName="shipping_address";
</script>
<div class="checkout address-page mk-cf">
    <h1>Shipping Address</h1>

    <div class="main-content">
        <h2>Choose an Address</h2>
        <div class="address-list-wrap">
        </div>

        <div class="page-action select-address">
            <button class="btn primary-btn btn-continue">Continue <span class="proceed-icon"></span></button>
        </div>

        <h2>Or create a new address</h2>
        <div class="new-address-msg mk-hide">Enter a shipping address</div>
        <div class="address-form-wrap"></div>
        <div class="info-msg">
        	<div>* Mandatory Fields</div>
        	<div class="grey">This Address will be saved in your Address Book</div>
		</div>
        <div class="page-action">
            <button class="btn primary-btn btn-save-continue">Save &amp; Continue <span class="proceed-icon"></span></button>
        </div>
    </div>

    {include file="checkout/summary.tpl"}
</div>

{/block}
{block name=macros}
    {include file="macros/cart-macros.tpl"}
{/block}
