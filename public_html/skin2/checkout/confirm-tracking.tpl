
    {if $fireSyncGACall}
	<script language="JavaScript" src="https://ssl.google-analytics.com/ga.js"></script>
    {/if}
        <script type="text/javascript">
        try {ldelim}
        	//var pageTracker = _gat._getTracker("{$ga_acc}");
        	_gaq.push(['_setCustomVar',
        	//pageTracker._setCustomVar(
                    1,
                    "Already-Bought",
                    "Yes",
                    1]);

        	{*if $payment_method neq 'chq'*}
				_gaq.push(['_addTrans',
				"{$orderid}",                                     // Order ID
				"{$userinfo.s_firstname} {$userinfo.s_lastname}", //cust name
				"{$amountafterdiscount|string_format:'%.2f'}",	  // Total
				"{$vat}",                                     	  // Tax
				"{$shippingRate}",                                // Shipping
				"{$userinfo.s_city}",                             // City
				"{$userinfo.s_state}",                            // State
				"{$userinfo.s_country}"]                              // Country
				);
			{*/if*}

			{assign var='loopIndex' value=0}
        	{assign var='netTotalPrice' value=0}
        	{assign var='productidDelimString' value=''}
        	{assign var='productTypeDelimString' value=''}
			{assign var='totalQuantity' value=0}

			{***products detail for my things conversion***}
			var myThingsProducts = new Array();

        	{foreach name=outer item=product from=$productsInCart}
        		{foreach key=key item=item from=$product}
        			{if $key eq 'productId'}
        			{assign var='productId' value=$item}
        			{assign var='productidDelimString' value=$productidDelimString|cat:$productId|cat:"|"}
        			{/if}

        			{if $key eq 'unitPrice'}
            		{assign var='unitPrice' value=$item}
            		{/if}

        			{if $key eq 'productStyleName'}
        			{assign var='productStyleName' value=$item}
        			{/if}

        			{if $key eq 'quantity'}
        			{assign var='quantity' value=$item}
        			{assign var='totalQuantity' value=$quantity+$totalQuantity}
        			{/if}

        			{if $key eq 'discount'}
                    {assign var='discount' value=$item}
                    {/if}

                    {if $key eq 'coupon_discount'}
                    {assign var='coupon_discount' value=$item}
                    {/if}

        			{if $key eq 'productTypeLabel'}
        			{assign var='productTypeLabel' value=$item}
        			{assign var='productTypeDelimString' value=$productTypeDelimString|cat:$productTypeLabel|cat:"|"}
        			{/if}

        		  	{if $key eq 'productCatLabel'}
					{assign var='productCatLabel' value=$item}
					{/if}

        			{if $key eq 'totalPrice'}
        			{assign var='totalPrice' value=$item}
        			{assign var='netTotalPrice' value=$netTotalPrice+$totalPrice}
					{/if}

					{***added for tyroo conversion tracking***}
					{if $key eq 'productPrice'}
						{assign var='productPrice' value=$item}
						{*assign var='itemString' value=":prod:$productPrice:qty:$quantity"*}
						{*assign var='lineItemString' value=$lineItemString|cat:$itemString*}
        			{/if}
        			{***tyroo till here***}

        		{/foreach}
				{assign var='itemString' value=":prod:$productPrice:qty:$quantity"}
				{assign var='lineItemString' value=$lineItemString|cat:$itemString}
        		{assign var='totalDiscountOnItems' value=$coupon_discount}
        		{assign var='discountPerItem' value=$totalDiscountOnItems/$quantity}
        		{assign var='unitPriceAfterDiscount' value=$unitPrice-$discountPerItem}



				{*if $payment_method neq 'chq'*}
					_gaq.push(['_addItem',
					"{$orderid}",			// Order ID
					"{$productId}",         //pid
					"{$productStyleName}",  //style
					"{$productCatLabel}",   //articleType|Brand
					"{$unitPriceAfterDiscount}",        //unit price
					"{$quantity}"           // Quantity
					]);
				{*/if*}

				{***products detail for my things conversion	***}
				myThingsProducts[{$loopIndex}] = {ldelim}id: "{$productId}",price:"{$productPrice|number_format:'2':'.':''}",qty:"{$quantity}"{rdelim};

				{assign var='loopIndex' value=$loopIndex+1}
            {/foreach}

            	{***tyroo conversion tracking***}
                    {*****MOD: not sending (p,q) tuples anymore****}
			{*if $lineItemString|strlen > 40*}
				{assign var='lineItemString' value=":prod:$amountafterdiscount:qty:1"}
			{*/if*}

            {*if $payment_method neq 'chq'*}
				_gaq.push(['_trackTrans']);

				{if $recordGACall}
					{literal}
                        _gaq.push(function(){
                            var newdiv = document.createElement('img');
                            var newGASrc= https_loc+"/baecon/{/literal}{$orderid}{literal}";
                            newdiv.setAttribute("src",newGASrc);
                            newdiv.setAttribute("width","1");
                            newdiv.setAttribute("height","1");
                            newdiv.setAttribute("alt","");
                            document.appendChild(newdiv);
                        });
                    {/literal}
				{/if}

            // Yahoo Conversion Tracking
            /*window.ysm_customData = new Object();
            window.ysm_customData.conversion = "transId={$orderid},currency=INR,amount={$amountafterdiscount|string_format:'%.2f'}";
            var ysm_accountid  = "1GJC30EFMMKEBN6R4ASQ2SBNQCG";
            document.write("<SCR" + "IPT language='JavaScript' type='text/javascript' "
            + "SRC=https://" + "srv2.wa.marketingsolutions.yahoo.com" + "/script/ScriptServlet" + "?aid=" + ysm_accountid
            + "></SCR" + "IPT>");*/

            {*/if*}
        {rdelim} catch(err) {ldelim}{rdelim}
        
      
		</script>

    <!--retarget pixel tracking-->
    {if $retargetPixel.confirmHttps}
        <script type="text/javascript">
        Myntra.Data.confirmHttps='{$retargetPixel.confirmHttps}';
        </script>
        <span id="retargetSpan" style="display:none;"></span>
    {/if}
