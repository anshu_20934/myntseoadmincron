<script type="text/javascript">
    var orderid='{$orderid}';
    Myntra.Data.socialAction='purchase';
    Myntra.Data.pageName='confirmation';
    //Exclusion required for gifts sent.
    {if $giftamount > 0} 
        var socialPost = 0;
    {else}
        var socialPost = 1;
    {/if}
    var socialPostArray={$socialArray};
    var socialPostStyleDetailsArray={$socialPostStyleDetailsArray};
</script>

<script>
    Myntra.Confirmation = Myntra.Confirmation || [];
    Myntra.Confirmation.Data = {
        orderId : {$orderid},
        shipping : {$shippingCharges},
        shippingCode : '{$userinfo.s_zipcode}',
        shippingCity : '{$userinfo.s_city}',
        paymentOption : '{$paymentoption}',
        couponCode : '{$couponCode}',
        totalAmount : {$amountafterdiscount|string_format:'%d'},
        totalQuantity : {$totalQuantity},
        products : '{$products}',
        productIds : '{$styleIdList}',
        isFirstOrder : {$isFirstOrder},
        orderSociomanticString : '{$orderSociomanticString}',
        orderNanigansString : '{$orderNanigansString}',
        CartProductId1 : '{$CartProductId1}',
        CartProductAmount1 : '{$CartProductAmount1}',
        CartProductQuantity1 : '{$CartProductQuantity1}',
        CartProductId2 : '{$CartProductId2}',
        CartProductAmount2 : '{$CartProductAmount2}',
        CartProductQuantity2 : '{$CartProductQuantity2}',
        CartProductId3 : '{$CartProductId3}',
        CartProductAmount3 : '{$CartProductAmount3}',
        CartProductQuantity3 : '{$CartProductQuantity3}',
        CartProductId4 : '{$CartProductId4}',
        CartProductAmount4 : '{$CartProductAmount4}',
        CartProductQuantity4 : '{$CartProductQuantity4}',
        CartProductId5 : '{$CartProductId5}',
        CartProductAmount5 : '{$CartProductAmount5}',
        CartProductQuantity5 : '{$CartProductQuantity5}',
    };
  
    var productIds = Myntra.Confirmation.Data.productIds.split(',');
</script>

<div class="confirm-msg">
    {if $message}
    <p class="message">{$message}</p>
    {else}
    ---------------------
    <span class="txt"><a href="{$http_location}/mymyntra.php?view=myorders" title="Track your orders at My Myntra" 
            onclick="_gaq.push(['_trackEvent', 'confirmation', 'mymyntra']);return true;">Track</a> your orders at My Myntra
    </span>
    --------------------
    {/if}
</div>

<div class="confirm-box-wrapper">
    <div class="confirm-box">
        <div class="congrats"><span class="icon-success-large"></span> Congratulations</div>
        <div class="msg-order-no">Your order has been placed<br>Order No. <a href="{$http_location}/mymyntra.php?view=myorders" title="My Myntra" onClick="_gaq.push(['_trackEvent', 'confirmation', 'mymyntra']);return true;"><span class="order-number">{$orderid}</span></a></div>
        <div class="google-play-button">
            <div class="play-img">    
                <a href="https://play.google.com/store/apps/details?id=com.myntra.android&referrer=utm_campaign%3DOrganic-App-Install%26utm_source%3DWebsite%26utm_medium%3DOrder-Confirmation%26utm_content%3DDownload-App"><img alt="Myntraapp on Google Play" src="https://developer.android.com/images/brand/en_app_rgb_wo_45.png" /></a>
            </div>
            <p class="play-msg">NOW YOU CAN ALSO TRACK YOUR ORDER ON THE MYNTRA ANDROID APP</p>
        </div> 
        <!--Kuliza Code start -->
            {include file="socialAction.tpl" echo_flow="checkout" quicklook="no" popupdiv="true"}
        <!--Kuliza Code end -->
        
        <div class="msg delivery-date">Your order is currently being processed. As the product(s) in your order are shipped, you will receive an email stating the expected delivery date for your product(s).</div>
        {include file="checkout/confirm-loyalty.tpl"}
         <div class="amount-paymode">
            <span class="amount">
                {if $paybyoption eq "cod"}
                    <strong>Amount Due</strong>
                    <span class="rupees">{$rupeesymbol} {$finalAmountTobePaid|number_format:0:".":","}</span>
                    at time of delivery
                {else}
                    Amount Paid
                    <span class="rupees">{$rupeesymbol} {$amountafterdiscount|number_format:0:".":","}</span>
                {/if}
            </span>
            <span class="paymode">
                Payment Mode 
                <strong>
    	        {if $paybyoption eq 'cod'}
    	        	Cash On Delivery
    	        {elseif $paybyoption eq 'netbanking'}
    	        	Net-Banking
    	        {elseif $paybyoption eq 'debitcard'}
    	        	Debit Card
    	        {elseif $paybyoption eq 'creditcards'}
    	        	Credit Card
    	        {elseif $paybyoption eq 'emi'}
    	        	EMI (Credit Card)
                {elseif $paybyoption eq 'cashback'}
                    Cashback
    	        {/if}
                </strong>
            </span>
        </div>

        <div class="title">Items</div>
        <ol class="order-items">
    		{foreach from=$productsInCart item=product}
            <li>
                <strong>
                {if !$product.freeItem && $product.landingpageurl|trim}<a href="{$http_location}/{$product.landingpageurl}" target="_blank">{/if}
                    {$product.productStyleName|truncate:80}
                {if !$product.freeItem && $product.landingpageurl|trim}</a>{/if}
                </strong>

                <span class="sep">/</span>
                Size <strong>{$product.final_size}</strong>

                <span class="sep">/</span>
                Qty <strong>{if $prod.final_quantity}{$product.final_quantity}{else}{$product.quantity}{/if}</strong>
            </li>
            {/foreach}
        </ol>

        <div class="title">Address</div>
        <div class="shipping-address">
            <div class="name">{$userinfo.s_firstname} {$userinfo.s_lastname}</div>
            <div class="address">
                {$userinfo.s_address|unescape: "html"|nl2br},</br>
                {if $userinfo.s_locality}{$userinfo.s_locality|unescape: "html"},<br>{/if}
            </div>
            <div class="city">
                {$userinfo.s_city|unescape: "html"} - {$userinfo.s_zipcode|unescape: "html"},
            </div>
            <div class="state">{$userinfo.s_state|unescape: "html"}</div>
            <div class="mobile"><span class="lbl">Mobile:</span> {$userinfo.mobile|unescape: "html"}</div>
        </div>
        <div class="msg confirm-email">A confirmation has been sent to <strong>{$userinfo.b_email}</strong></div>
        <div class="msg confirm-email"><strong>Note :We do not demand your banking and credit card details verbally or telephonically. Please do not divulge your details to fraudsters and imposters falsely claiming to be calling on Myntra.com's behalf.</strong></div>
        <div id="share-fb-cont"></div>
    </div>
</div>
{if $shopingFestFG || $confirmationConfigMsg || $fest_coupons}
<div class="confirm-box-wrapper">
    <div class="confirm-box confirm-box-special">
        {include file="checkout/confirm-coupons.tpl"}
    </div>
</div>        
{/if}
<a href="{$http_location}" title="Continue Shopping" class="btn primary-btn btn-continue" 
    onclick="_gaq.push(['_trackEvent', 'confirmation', 'continue_shopping']);return true;">Continue Shopping</a>

<script type="text/javascript">
    var pdpServiceUrl = '{$pdpServiceUrl}';
    var cartItemIds =  Myntra.Confirmation.Data.productIds.split(',');
    var cartGAData = {};
</script>

<script language="JavaScript" src="https://media.richrelevance.com/rrserver/js/1.0/p13n.js"></script>
<script type="text/javascript">
   // rich relevance instrumentation
   
   function getRRCookie(cname)
	{
		if(localStorage){
			var ls_rr_sid = localStorage.getItem('lscache-rr_sid'); // geting from local storage
			
			if( ls_rr_sid ){
				return ls_rr_sid;
			}
		}
				
		var name = Myntra.Data.cookieprefix+""+cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) 
		{
		  var c = ca[i].trim();
		  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
		}
		return "";
	}
	
	var R3_COMMON = null;
	var R3_ITEM = null;
   
  	
		R3_COMMON = new r3_common();

		
		R3_COMMON.setApiKey('54ed2a6e-d225-11e0-9cab-12313b0349b4');
		
		if(Myntra.Data.cookieprefix != "" ){
			R3_COMMON.setBaseUrl(window.location.protocol+'//integration.richrelevance.com/rrserver/');
		}else{
			R3_COMMON.setBaseUrl(window.location.protocol+'//recs.richrelevance.com/rrserver/');
		}
		
		R3_COMMON.setClickthruServer(window.location.protocol+"//"+window.location.host);
		var rr_sid = getRRCookie("rr_sid")+"";
		R3_COMMON.setSessionId(rr_sid);
		
		if(typeof Myntra.Data.userHashId == "undefined" || Myntra.Data.userHashId == "" ){
			R3_COMMON.setUserId(rr_sid);
		}else{
			R3_COMMON.setUserId(Myntra.Data.userHashId+"");
		}
		
		RR.jsonCallback = function (){
			//console.dir(RR.data.JSON.placements);
			//console.dir("done");
		}; // call back performed, once rr call is executed.

		var R3_PURCHASED = new r3_purchased();
				
		R3_PURCHASED.setOrderNumber(Myntra.Confirmation.Data.orderId+'');
		
		try{
			var products = JSON.parse(Myntra.Confirmation.Data.products);
		}catch(err){
			return; // unable to parse the items
		}
		
		
		products.forEach(function(k,v){			
			R3_PURCHASED.addItemIdPriceQuantity(k.id+'', k.price+'', k.quantity+'');		
		});
				
		
		rr_flush_onload();
		
		r3();
		
	
</script>

{include file="checkout/confirm-tracking.tpl"}
{include file="checkout/confirm-mobile-verify.tpl"}
{include file="macros/confirmation-macros.tpl"}

