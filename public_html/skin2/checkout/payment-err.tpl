
<div class="content-hd">
    Payment
</div>
<div class="content-bd">
    <div class="error">
        {if $errorMessage}
            {$errorMessage}
        {else}
            We're sorry, but something went wrong.<br>
            Please try again. If the problem continues, please try after some time.
        {/if}
    </div>
</div>
