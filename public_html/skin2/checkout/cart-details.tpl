{assign var=numrows value=0}
{foreach name=combo item=productsSet key=discountId from=$productsInCart}
        <div class="prod-set">
        {assign var=isCombo value=false}
        {if $discountId gt 0 }
            {assign var=isCombo value=true}
            {if $productsSet.dre_minMore gt 0}
                {assign var=isConditionMet value=false}
            {else}
                {assign var=isConditionMet value=true}
            {/if}
			<div class="combo">
			{if $cart_view eq "complete"}
            <div class="combo-header-main row header">
			 <div class="col1" style="width:194px;">&nbsp;</div>
			 <div class="col2">
					<div class="discount-msg">{if $isConditionMet}{$productsSet.icon}&nbsp;{/if}{include file="string:{$productsSet.label_text}"}</div>
					<div class="discount-amt">YOU PAY <span class="combo-savings-msg">{$rs}{math equation="a-b" a=$productsSet.comboTotal b=$productsSet.comboSavings assign="comboActualTotal"}{$comboActualTotal|round|number_format:0:".":","}</span> / YOU SAVE <span class="combo-savings-msg">{$rs}{$productsSet.comboSavings|round|number_format:0:".":","}</span></div>
					<a class="combo-completion-btn btn normal-btn" isConditionMet="{$isConditionMet}"
                    	ctype="{if $productsSet.dre_buyCount>0}c{else}a{/if}"
                    	min="{if $productsSet.dre_buyCount>0}{$productsSet.dre_buyCount}{else}{$productsSet.dre_buyAmount}{/if}"
                    	discountId="{$discountId}">
                    	{if $isConditionMet}
                        	Add More to Combo!
                    	{else}
                        	Complete Combo Now!
                    	{/if}
                	</a>
       				<div class="combo-completion-msg red">{if $isConditionMet}Combo Complete!{else}{if $productsSet.dre_buyAmount>0}{$rs} {/if}{$productsSet.dre_minMore} more to go!{/if}</div>
				</div>
			<div class="col3" style="width:194px;">&nbsp;</div>
		</div>
			{/if}
        {/if}

        {*Combo Loop starts here*}
        {foreach name=outer item=product key=itemid from=$productsSet.productSet}
            {assign var=numrows value=$numrows+1}
            <div class="row prod-item" id="product_view_{$itemid}">
                {if $cart_view eq "complete"}
                    <div class="col1">
                        {if !$product.freeItem && $product.landingpageurl|trim}
						<a href="{$http_location}/{$product.landingpageurl}" target="_blank" onclick="_gaq.push(['_trackEvent',{if $flow}'{$flow}' , 'pdp_click', Myntra.Data.pageName,{else}'pdp_click', Myntra.Data.pageName, 'cart_image'{/if}]);">
						{/if}
                        		<img src="{myntraimage key="style_96_128" src=$product.cartImagePath}" alt="image of {$product.productStyleName}">
                        {if !$product.freeItem && $product.landingpageurl|trim}
                        	</a>
						{/if}
                    </div>
                {/if}

                <div class="col2">
                    {if $cart_view eq "complete"}
                        <form id="cartform_{$itemid}" method="post" action="modifycart.php" class="form-cart-item">
                            <input type="hidden" name="_token" value="{$USER_TOKEN}">
                            <input type="hidden" name="cartitem" value="{$itemid}">
                            <input type="hidden" name="styleid" value="{$product.productStyleId}">
                            <input type="hidden" name="update" value="1">
                    {/if}
                    {if $cart_view eq "complete"}
                        <div class="prod-code">Product Code: {$product.productStyleId}</div>
                    {/if}

                    <div class="prod-name">
                    {if $cart_view eq "complete"}
                        {if !$product.freeItem && $product.landingpageurl|trim}
						<a href="{$http_location}/{$product.landingpageurl}" onclick="_gaq.push(['_trackEvent',{if $flow}'{$flow}' ,'pdp_click', Myntra.Data.pageName{else}'pdp_click', Myntra.Data.pageName, 'cart_title'{/if}]);" target="_blank">{$product.productStyleName}</a>
						{else}
							<span class="free-prod-name">{$product.productStyleName}</span>
						{/if}
                    {else}
                        {if !$product.freeItem && $product.landingpageurl|trim}
						<a href="{$http_location}/{$product.landingpageurl}" onclick="_gaq.push(['_trackEvent',{if $flow}'{$flow}' ,'pdp_click', Myntra.Data.pageName{else}'pdp_click', Myntra.Data.pageName, 'cart_title'{/if}]);" target="_blank">{$product.productStyleName|truncate:80}</a>
						{else}
                        	<span class="free-prod-name">{$product.productStyleName|truncate:80}</span>
						{/if}
                    {/if}
                    </div>

                    <div class="size-qty-wrap">
                        {if $cart_view eq "complete"}
                            <span class="mk-custom-drop-down size">
                                <label>Size:</label>
                                {if $product.sizenames && $product.sizequantities}
                                    {if $product.sizenames|@count gt 0}
                                    	{if $product.flattenSizeOptions} 
											{assign var=selectedSize value=$product.sizenameunified} 
										{else} 
											{assign var=selectedSize value=$product.sizename} 
										{/if}
										<!-- We only show the selected size in the combo initially. Other sizes are loaded by ajax -->	
                                        {if $product.freeItem}
											<span class="mk-freesize">{$selectedSize}</span>
										{else}
                                        	<select class="sel-size" name="{$itemid}size">
                                        		<option value="{$product.skuid}" selected>{$selectedSize}</option>
                                            </select>
										{/if}
                                    {/if}
                                {else}
                                    NA
                                {/if}
                            </span>
                            <span class="mk-custom-drop-down qty">
                                {assign var=sku value=$product.productStyleId|cat:'-'|cat:$product.sizename}
                                {assign var=cssqty value=""}
                                {if ($checkInStock) and ($product_availability[$itemid] lt $product.quantity
                                        or $product.quantity eq 0 or $total_consolidated_sale[$sku].stockout eq true)}
                                    {assign var=cssqty value="qty-err"}
                                {/if}
                                {$maxQty=min($cartItemMaxQty,$sku_info[$itemid].availableItems)}
                                <label>Qty:</label>
                                {if $maxQty gt 0 && $product.freeItem neq 1}
                                <select class="sel-qty" name="{$itemid}qty">
									{for $i=1 to $maxQty}
                                    	<option value="{$i}"{if $i eq $product.quantity} selected{/if}>{$i}</option>
                                    {/for}
	                                {if $product.quantity > $maxQty }
                                        <option disabled selected>{$product.quantity}</option>
	                                {/if}
                                </select>
                                {else}
									<span class="mk-freesize">{$product.quantity}</span>
									<!-- Kundan: on changing item size, it was not reflecting in case of Out of stock items: maxQty will be equal to 0 -->
									<input type="hidden" name="{$itemid}qty" value="{$product.quantity}"/>
                                {/if}
                            </span>
                        {else}
                            <span class="lbl">Size:</span>
                            {if $product.flattenSizeOptions} {$product.sizenameunified} {else} {$product.sizename} {/if}
                            <span class="sep">/</span>
                            <span class="lbl">Qty:</span> {$product.sizequantity}
                        {/if}
                    </div>

                    {if $cart_view eq "complete"}
                    </form>
                    <div class="err item-error">
                        {if ($checkInStock) and $product.quantity eq 0 }
                            Zero unit(s) ordered
                        {elseif ($checkInStock)  and ($product_availability[$itemid] lte 0)}
                            Sold Out {if $product.freeItem neq 1}-<button class="link-btn move-to-wishlist item-err">Move Item to Wishlist</button>or<button class="link-btn cart-remove-item item-err">Remove</button>to continue {/if}
                        {elseif ($checkInStock) and $product_availability[$itemid] lt $product.quantity}
                            Only {$product_availability[$itemid]} unit(s) in stock {if $product.freeItem neq 1}- Change Quantity to continue{/if}
                        {/if}
                    </div>
                    {/if}
                </div>

                <div class="col3">
					{assign var="couponApplicable" value="COUPON APPLICABLE"}
		           	{if $styleExclusionFG eq 1 && !$product.couponApplicable}
						{assign var="couponApplicable" value="COUPON NOT APPLICABLE"}
	    	        {elseif $condNoDiscountCouponFG eq 1 && $product.isDiscounted}
							{assign var="couponApplicable" value="COUPON NOT APPLICABLE"}
	            	{/if}
	            			
                    {if $product.productPrice eq 0}
                    	{if $cart_view eq "complete"}<div class="discount"></div>{/if}
                        <div class="amount red">FREE</div>
                        {if $cart_view neq "complete"}<div class="discount"></div>{/if}
                    {else}
						{if (isset($product.pricePerItemAfterDiscount) && $product.pricePerItemAfterDiscount gte 0) || $product.freeItem eq 1}
					
							{if $cart_view neq "complete"}
								<div class="amount red">
									{if $product.pricePerItemAfterDiscount eq 0}FREE{else}{$rs} {math equation="a*b" a=$product.pricePerItemAfterDiscount b=$product.quantity assign="priceAfterDiscount"}{$priceAfterDiscount|round|number_format:0:".":","}{/if}
									<span class="strike rupees gray">{math equation="a*b" a=$product.productPrice b=$product.quantity assign="netPrice"}{$netPrice|round|number_format:0:".":","}</span>{if $product.quantity neq $product.discountQuantity}<span class="red"> ({$product.discountQuantity} FREE) </span>{/if}
								</div>
	                            <div class="discount red">
									<span> {include file="string:{$product.discountDisplayText}"}</span>
								</div>
							{else}
	                            <div class="discount">
	                                <span class="strike rupees gray">{$rs} {math equation="a*b" a=$product.productPrice b=$product.quantity assign="netPrice"}{$netPrice|round|number_format:0:".":","}</span>
								{if $product.quantity neq $product.discountQuantity}
                                    <span class="red"> ({$product.discountQuantity} FREE) </span>
								{/if}
									<span> {include file="string:{$product.discountDisplayText}"}</span>
								</div>
                            	<div class="amount red">{if $product.pricePerItemAfterDiscount eq 0}FREE{else}{$rs} {math equation="a*b" a=$product.pricePerItemAfterDiscount b=$product.quantity assign="priceAfterDiscount"}{$priceAfterDiscount|round|number_format:0:".":","}{/if}</div>
							{/if}
                    	{else}
	                    	{if $cart_view eq "complete"}
	                    		<div class="discount">
	                    			{if $styleExclusionFG eq 0 && $condNoDiscountCouponFG eq 1 && !($product.isDiscounted)}
	            						<span class="gray">{$couponApplicable}</span>
	            					{/if}
								</div>	            					        		
	                    	{/if}
	                    			 	
                            <div class="amount red">{$rs} {$product.productPrice*$product.quantity-$product.discountAmount|round|number_format:0:".":","}</div>
 	                        {if $cart_view neq "complete"}
	                    		<div class="discount">
	                    			{if $styleExclusionFG eq 0 && $condNoDiscountCouponFG eq 1 && !($product.isDiscounted)}
	            						<span class="gray">{$couponApplicable}</span>
	            					{/if}
	            				</div>
	                        {/if}
                        {/if}
	                    {*{if $cart_view eq "complete"}
	                        {if $product.discountDisplayText neq ''}
	                            <div class="tool-text">
	                                {include file="string:{$product.discountDisplayText}"}
	                            </div>
	                        {elseif $cashback_gateway_status eq 'on' && $enableCBOndiscountedProducts eq 'true'}
	                            <div class="cashback-info {if $cashback_displayOnCartPage neq '1'}hide{/if}">
	                                Cashback:
	                                {$rs}{math equation="(w-x) * z" w=$product.productPrice x=$product.discountAmount
	                                    y=$product.quantity z=0.10 assign="cashback"}
	                                {$cashback|round|number_format:0:".":","} per item
	                            </div>
	                        {elseif $cashback_gateway_status eq 'on'
	                                && $enableCBOndiscountedProducts eq 'false'
	                                && $product.discountAmount eq 0}
	                            <div class="cashback-info {if $cashback_displayOnCartPage neq '1'}hide{/if}">
	                                Cashback:
	                                {$rs}{math equation="(w-x) * z" w=$product.productPrice
	                                    x=$product.discountAmount y=$product.quantity
	                                    z=0.10 assign="cashback"}
	                                {$cashback|round|number_format:0:".":","} per item
	                            </div>
	                        {/if}*}
						{if $styleExclusionFG eq 1}	                        
	                        <div class="couponApplicability">
    	                    	<span>{$couponApplicable}</span>
        	                </div>
                        {/if}
	                        
                        {if $cart_view eq "complete" && $product.freeItem neq 1}
                            <div class="action">
                                <form method="post" action="/mksaveditem.php" class="form-save-item">
                                    <input type="hidden" name="actionType" value="addFromCart">
                                    <input type="hidden" name="itemId" value="{$itemid}">
                                    <input type="hidden" name="redirectTo" value="cart">
                                    <input type="hidden" name="_token" value="{$USER_TOKEN}">
				    				<input type="hidden" name="productId" value="{$product.productStyleId}">
                                    <button type="submit" class="mk-move-to-wishlist link-btn"
										onclick="_gaq.push(['_trackEvent', 'cart', 'save_item']);"> Move to Wishlist</button>
                                </form>
                               	<span class="sep">/</span>
                                <form method="get" action="/modifycart.php" class="form-remove-item">
                                    <input type="hidden" name="remove" value="true">
                                    <input type="hidden" name="itemid" value="{$itemid}">
                                    <input type="hidden" name="_token" value="{$USER_TOKEN}">
                                    <button type="submit" class="mk-remove link-btn"
                                        onclick="_gaq.push(['_trackEvent', 'cart', 'remove_item'])">Remove</button>
                                </form>
                            </div>
						{else}
							{if $freeChoiceDiscountMessageItemLevel != '' && $product.freeItem eq 1 && $product.discountComboId eq 0 && $product.isDiscountRuleApplied eq true}
			    				<div class="action">
                        			{include file="string:{$freeChoiceDiscountMessageItemLevel}"}
			    				</div>
                    		{/if}
                        {/if}
                                                
                    {/if}
                </div>
            </div>
        {/foreach}

        {if $isCombo}
			</div>
        {/if}

        </div>
{/foreach}

<script>
    Myntra.Cart = Myntra.Cart || {};
    Myntra.Cart.Data = {ldelim}
        isGiftOrder : {$isGiftOrder},
        items : '{$_cartItems}', 
        amount : {$amount},
        totalAmount : {$totalAmount},
        totalQuantity : {$totalQuantity},
        mrp : {$mrp},
        couponDiscount : {$couponDiscount},
        cashDiscount : {$cashdiscount},
        totalCashBackAmount : {$totalCashBackAmount},
        cashBackAmountDisplayOnCart : {$cashBackAmountDisplayOnCart},
        shippingCharge : {$shippingCharge},
        giftCharge : {$giftCharge},
        savings : {$savings},
        cartLevelDiscount : {$cartLevelDiscount},
        itemDiscount : {$eossSavings},
{if isset($productAdded)}
        productAdded : {$productAdded}
{else}
        productAdded : 0
{/if}
    {rdelim};
</script>
