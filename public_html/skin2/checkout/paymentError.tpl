	{if $transaction_status && $transaction_status eq "EMI"}
		 <div class="access-block" style="text-align:center;">
		       <div class="access-error"><span class="wrong-large-icon" style="vertical-align: middle; margin-top: 3px;"></span> OOPS - YOUR TRANSACTION DIDN'T GO THROUGH!</div>
		       		<div class="access-error-msg">
		                <div style="line-height:18px;font-family: arial;font-size: 13px;">
		                <span style="color: #d14747">Card number provided is not eligible for EMI.</span> <br>
		                <p>                
		                Please check if the card information you entered is correct and try again.<br/>				
		                </p>
		                {if $codErrorCode eq 0}
							<br>
							Alternatively, you can also opt to pay by cash on delivery.
		                {/if}
		                <p style="padding:0px;margin:5 0px;text-align: justify;color: #999999;">
		                In case you continue to face problems, please call our customer support {if $customerSupportCall} at {$customerSupportCall}{/if}.
		                </p>
		                </div>
		              </div>
		            </div>
		        {elseif $transaction_status && !$vbvfail}
		            <div class="access-block" style="text-align:center;">
		                <div class="access-error"><span class="wrong-large-icon" style="vertical-align: middle; margin-top: 3px;"></span> OOPS - YOUR TRANSACTION DIDN’T GO THROUGH!</div>
		                <div class="access-error-msg">
		                <div style="line-height:18px;font-family: arial;font-size: 13px;">
		                The reason could be <span style="color: #d14747">invalid card/netbanking details.</span> <br>
		                <p>
		                Please check if the card/bank information you entered is correct and try again.<br/>
				In case your account has been debited, the amount will be rolled back within 24 hours.
		                </p>
		                {if $codErrorCode eq 0}
					<br>
					Alternatively, you can also opt to pay by cash on delivery.
		                {/if}
		                <p style="padding:0px;margin:5 0px;text-align: justify;color: #999999;">
		                In case you continue to face problems, please call our customer support {if $customerSupportCall} at {$customerSupportCall}{/if}.
		                </p>
		                </div>
		                </div>
		            </div>
		        {elseif $vbvfail}
		            <div class="access-block" style="text-align:center;">
		            <div class="access-error"><span class="wrong-large-icon" style="vertical-align: middle; margin-top: 3px;"></span> OOPS - YOUR TRANSACTION DIDN’T GO THROUGH!</div>
		            	<div class="access-error-msg">
		                <div style="line-height:18px;font-family: arial;font-size: 13px;">
		                The reason could be <span style="color: #d14747">failed secure verification of your card.</span> <br>
		                	<p>
			                	Please check if the card information you entered is correct and try again.<br/>
			                	{if $bankVBVLink}
			                    		In case you are not registered for secure verification, you may do so by following the instructions <a href="{$bankVBVLink}" onClick="_gaq.push(['_trackEvent', 'payment_page', '3dregistration', '{$bankName}']);return true;" target="_blank">here</a>.<br>
			                	{else}
			                    		In case you are not registered for secure verification, please contact your Bank.<br>
			                	{/if}
						In case your account has been debited, the amount will be rolled back within 24 hours.
		                	</p>
		                	{if $codErrorCode eq 0}
		                    		<br>
		                    		Alternatively, you can also opt to pay by cash on delivery.
		                	{/if}
		                	<br>
		                	<p style="padding:0px;margin:5 0px;text-align: justify;color: #999999;">
		                		In case you continue to face problems, please call our customer support {if $customerSupportCall} at {$customerSupportCall} {/if}.
		                	</p>
		                </div>
		                </div>
		            </div>
	{/if}
	{if $payFailOpt == 'test' && ($transaction_status || $vbvfail)}
	<div class="pay-btn-wrap inline-btns">
			{if $codErrorCode eq 0}
			   	<a class="btn primary-btn codbtn">
	    		Pay by COD
	     		</a>
	     	{/if}
	     	<a class="btn primary-btn tryagainbtn">
	     		Try Again
	    	 </a>
		    
	 </div>	
	{/if}

