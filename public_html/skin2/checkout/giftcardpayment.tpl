{extends file="layout-checkout.tpl"}

{block name=body}
<script>
Myntra.Data.pageName="payment";
Myntra.Data.OrderType="gifting";
Myntra.Payments = Myntra.Payments || {};
Myntra.Payments.isGiftCardOrder = true;
var isGiftCardOrder = 1;
var hasAddress = '{$hasAddress}';
</script>
<div class="checkout payment-page mk-cf giftcard-payment">
    <h1>Payment Options</h1>
	<h3>100% Safe & Secure Shopping / Pay with Confidence</h3>
    {* mainContent *}
    <div class="main-content">
    	{if $transaction_status && $transaction_status eq "EMI"}
    		<div class="access-block" style="text-align:center;">
                <div class="access-error"><span class="wrong-large-icon" style="vertical-align: middle; margin-top: 3px;"></span> OOPS - YOUR TRANSACTION DIDN’T GO THROUGH!</div>
                <div style="line-height:18px;font-family: arial;font-size: 13px;">
                <span style="color: #d14747">Card number provided is not eligible for EMI.</span> <br>
                <p>                
                Please check if the card information you entered is correct and try again.<br/>				
                </p>
                <p style="padding:0px;margin:5 0px;text-align: justify;color: #999999;">
                In case you continue to face problems, please call our customer support {if $customerSupportCall} at {$customerSupportCall}{/if}.
                </p>
                </div>
            </div>
        {elseif $transaction_status && !$vbvfail}
            <div class="access-block" style="text-align:center;">
                <div class="access-error"><span class="wrong-large-icon" style="vertical-align: middle; margin-top: 3px;"></span> OOPS - YOUR TRANSACTION DIDN’T GO THROUGH!</div>
                <div style="line-height:18px;font-family: arial;font-size: 13px;">
                The reason could be <span style="color: #d14747">invalid card/netbanking details.</span> <br>
                <p>
                Please check if the card/bank information you entered is correct and try again.<br/>
		In case your account has been debited, the amount will be rolled back within 24 hours.
                </p>
                <p style="padding:0px;margin:5 0px;text-align: justify;color: #999999;">
                In case you continue to face problems, please call our customer support {if $customerSupportCall} at {$customerSupportCall}{/if}.
                </p>
                </div>
            </div>
        {elseif $vbvfail}
            <div class="access-block" style="text-align:center;">
            <div class="access-error"><span class="wrong-large-icon" style="vertical-align: middle; margin-top: 3px;"></span> OOPS - YOUR TRANSACTION DIDN’T GO THROUGH!</div>
                <div style="line-height:18px;font-family: arial;font-size: 13px;">
                The reason could be <span style="color: #d14747">failed secure verification of your card.</span> <br>
                	<p>
	                	Please check if the card information you entered is correct and try again.<br/>
	                	{if $bankVBVLink}
	                    		In case you are not registered for secure verification, you may do so by following the instructions <a href="{$bankVBVLink}" onClick="_gaq.push(['_trackEvent', 'payment_page', '3dregistration', '{$bankName}']);return true;" target="_blank">here</a>.<br>
	                	{else}
	                    		In case you are not registered for secure verification, please contact your Bank.<br>
	                	{/if}
				In case your account has been debited, the amount will be rolled back within 24 hours.
                	</p>
                	<br>
                	<p style="padding:0px;margin:5 0px;text-align: justify;color: #999999;">
                		In case you continue to face problems, please call our customer support {if $customerSupportCall} at {$customerSupportCall} {/if}.
                	</p>
                </div>
            </div>
        {/if}

        {if $citi_discount > 0}
            <div id="citi_discount_div"> Avail {$citi_discount}% discount by paying with your Citibank <a href="#pay_by_dc" onClick="$('#pay_by_dc').click();return false;">Debit Card</a></div>
        {/if}

        {if $icici_discount > 0}
            <div id="icici_discount_div"> Avail {$icici_discount}% discount on payment through <img alt="ICICI" src="{$secure_cdn_base}/skin1/images/ICICI.png"> (credit card, debit card or netbanking)</div>
        {/if}

        {* pay button is used in top and bottom. so, capture is used here *}
        {capture name="pay_button"}
        <div class="pay-btn-wrap">
            <a href="#paybtn" {if $totalAmount eq 0} onclick="return submitPaymentForm(false);" {elseif $is_icici_gateway_up} onclick="return submitPaymentForm(true);" {else} onclick="return submitPaymentForm(false);" {/if} class="btn primary-btn pay-btn">
                 {if $totalAmount eq 0}
                    Confirm Order
                 {elseif $is_icici_gateway_up}
                    {if $paymentType == "cod" }
                        Confirm Order
                    {else}
                        Pay Now
                    {/if}
                 {else}
                    Proceed to Payment
                 {/if}
            </a>
        </div>
        {/capture}

        {*$smarty.capture.pay_button*}

        <div class="coupon-wrap mk-cf">
            {include file="cart/giftcardcoupon_widget.tpl"}
        </div>
        
        {include file="giftcards/giftcards-activation-widget-placeholder.tpl"}

        {*PyamentBlock*}
        {if $paymentServiceEnabled}
            {if $prompt_login != 1}
            	{$paymentPageUI}
            {/if}
        {else}
        <div class="pay-block">
            <!--PORTAL-667 Configurable Order of payment options-->
            {if $totalAmount eq 0}
                {assign var="paymentType" value="credit_card"}
            {else}
                {assign var="paymentType" value="$defaultPaymentType"}
	            <h2>Select a payment mode</h2>
            {/if}

            <input type=hidden id="paymentFormToSubmit" value="{$paymentType}">
            <input type=hidden id="codErrorCode" value="{$codErrorCode}">
            <input type=hidden id="addressPincode" value="{$address.pincode}">

            <input type=hidden id="customer-shipping-name" value="{$address.name}">
            <textarea style="display:none" id="customer-shipping-address">{$address.address}</textarea>
            <input type=hidden id="customer-shipping-locality" value="{$address.locality}">
            <input type=hidden id="customer-shipping-city" value="{$address.city}">
            <input type=hidden id="customer-shipping-state" value="{$address.state}">
            <input type=hidden id="customer-shipping-statename" value="{$address.statename}">
            <input type=hidden id="customer-shipping-country" value="{$address.country}">
            <input type=hidden id="customer-shipping-countryname" value="{$address.countryname}">
            <input type=hidden id="customer-shipping-pincode" value="{$address.pincode}">
            <input type=hidden id="customer-shipping-mobile" value="{$address.mobile}">

            {if $totalAmount eq 0}
                <form action="{$https_location}/{$paymentphp}" method="post" id="credit_card">
                	{if $giftCardPaymentPage}
                		<input type="hidden" name='order_type' value='gifting'/>
                	{/if}
	                <input type="hidden" name='s_option' value ='{$logisticId}'/>
    	            <!--delivery preference-->
    	            <input type="hidden" name="shipment_preferences" value="shipped_together" >
    	            <!--delivery preference-->
    	            <input type="hidden" name="is_shipping_cost_set" value="{$s_city}">
    	            <input type="hidden" name="checksumkey" value="{$checksumkey}">
    	            <input type="hidden" name="address" value="{$address.id}">
    	            <input type="hidden" value="creditcards" name="pm" />
	            	<div class="nothing-to-pay">
	            		You have no outstanding amount to pay.<br />
	            		Click 'Confirm Order' to proceed.
	            	</div>
                </form>
            {else}
	            {*Tabs*}
	           {* {if $emiEnabled}
	            	<span class="opt-new"></span>
	            {/if}*}
	            <div class="tabs">
	                <ul>
                        {foreach from=$payment_options item=pmt_option}
                            {if $pmt_option eq "credit_card"}
                                <li id="pay_by_cc" {if $defaultPaymentType eq "credit_card"} class="selected" {/if}>
                                    <span class="opt"></span>Credit Card</li>
                            {elseif $pmt_option eq "emi"}
                                <li id="pay_by_emi" {if $defaultPaymentType eq "emi"} class="selected" {/if}>
                                    <span class="opt"></span>EMI<br />(Credit Card)</li>                            
                            {elseif $pmt_option eq "debit_card"}
                                <li id="pay_by_dc" {if $defaultPaymentType eq "debit_card"} class="selected" {/if}>
                                    <span class="opt"></span>Debit Card</li>
                            {elseif $pmt_option eq "net_banking"}
                                <li id="pay_by_nb" {if $defaultPaymentType eq "net_banking"} class="selected" {/if}>
                                    <span class="opt"></span>Net Banking</li>
                            {elseif $pmt_option eq "cod"}
                                <li id="pay_by_cod" {if $defaultPaymentType eq "cod"}  class="selected" {/if}>
                                    <span class="opt"></span>Cash on Delivery</li>
                            {elseif $pmt_option eq "pay_by_phone"}
                                <li id="pay_by_phone" {if $defaultPaymentType eq "pay_by_phone"}  class="selected" {/if}>
                                    <span class="opt"></span>Phone</li>
                            {/if}
                        {/foreach}
	                </ul>
	            </div>
	            {*Tabs@End*}

	            {*Tabs content*}
	            <div class="tab-content mk-cf">
	                {foreach from=$payment_options item=pmt_option}
	                    {if $pmt_option eq "credit_card"}
	                        {include file="cart/form_pmt_creditcard.tpl"}
                        {elseif $pmt_option eq "emi"}
                               {include file="cart/form_pmt_emi.tpl"}	                        
	                    {elseif $pmt_option eq "debit_card"}
	                        {include file="cart/form_pmt_debitcard.tpl"}
	                    {elseif $pmt_option eq "net_banking"}
	                        {include file="cart/form_pmt_netbanking.tpl"}
	                    {elseif $pmt_option eq "cod"}
	                        {if $codWindow eq "displayCaptchaScreen"}
	                            {include file="cart/form_pmt_cod.tpl"}
	                        {else}
	                            {include file="cart/form_pmt_mobile_verify.tpl"}
	                        {/if}
	                    {elseif $pmt_option eq "pay_by_phone"}
	                        {include file="cart/form_pmt_pbp.tpl"}
	                    {/if}
	                {/foreach}
	            </div>
	            {*Tabs content@End*}
            {/if}
        </div>

        {$smarty.capture.pay_button}
        {*PyamentBlock@End*}
        {/if}
    </div>
    {*mainContent@End*}

    {include file="checkout/giftcardsummary.tpl"}

{*    <div id="myOnPageContent" style="display:none;">
        <table cellpadding="0" cellspacing="0" width="100%" class="my-c-table" style="margin-top:10px;border:1px solid #ccc;">
            <tbody>
                <tr>
                    <th>Select</th>
                    <th>Coupon code</th>
                    <th>coupon description</th>
                    <th>Expires on</th>
                    <!-- <th>Discount on Cart Contents</th> -->
                </tr>
                {section name=prod_num loop=$rest_coupons}
                    <tr class="even">
                        <td><input name="select" type="radio" onClick="javascript:redeemCoupon2('{$rest_coupons[prod_num].coupon}');"> </td>
                        <td>{$rest_coupons[prod_num].coupon}</td>
                        <td>{$rest_coupons[prod_num].description}</td>
                        <td class="td-cr">{$rest_coupons[prod_num].expire|date_format:$config.Appearance.date_format}</td>
                        <!-- <td class="td-last td-rs">170.00</td> -->
                    </tr>
                {/section}
            </tbody>
        </table>
    </div>
*}

</div>
{/block}

{block name=lightboxes}
<!-- commenting it out so that not to use pull method mobile verification
<div id="splash-mobile-verify" class="lightbox">
<div class="mod">
    <div class="hd">
        <h2>VALIDATE MOBILE NUMBER</h2>
    </div>
    <div class="bd clearfix">
        <div class="verify-sms">
            Please send an SMS within 10 minutes from your mobile number (<span id='userMobile'>{$userMobile}</span>) with the text:<br>
            <strong>VERIFY</strong><br>
            to this number:<br>
            <strong>0-92824-24444</strong>
        </div>
        <span class="mnote">* Standard SMS charges apply</span>

        Once you send the SMS, your number will be validated in the next 120 seconds.<br>
        In case you want to validate another number, please go to your My Myntra Profile to update your number before trying again.<br>
        <button class="btn normal-btn ok-btn" type="button">OK</button>
    </div>
</div>
</div>
-->

{* Do not change id of any div below it is used in orderProcessing.tpl *}
<div id="splash-payments-processing" class="lightbox">
<div class="mod">
    <div class="hd" id="splash-payments-processing-hd">
        <h2>Processing Payment</h2>
    </div>
    <div class="bd clearfix" id="splash-payments-processing-bd">
    	<center><img src="{$secure_cdn_base}/images/image_loading.gif"></center>
        <div class="processing-payments" id="splash-payments-processing-bd-pp">
            Enter your card/bank authentication details in the new window once it appears on your screen. <br/>
			Please do not refresh the page or click the back button of your browser.<br/>
			You will be redirected back to order confirmation page after the authentication. It might take a few seconds.<br/><br/>
        </div>

        <center id="cancel-payment-btn"><button class="action-btn cancel-payment-btn" type="button">Cancel Payment</button></center>
    </div>
</div>
</div>

{include file="my/giftcards-preview-template.tpl"}

{include file="giftcards/giftcards-activation-widget.tpl"}
{/block}

{block name=pagejs}
<script type="text/javascript">
	Myntra.Payments = Myntra.Payments || {};
	Myntra.Payments.amount = '{$rupeesymbol} {$totalAmount|round|number_format:0:".":","}';
	var total_amount = 	{$totalAmount};
	var iciciMinEMITotalAmount = "{$iciciMinEMITotalAmount}";
	var shipping_countrycode = "{$address.country|escape:'html'}";
	var codErrorCode = {if $codErrorCode}{$codErrorCode}{else}0{/if};
	{if $onlineErrorMessage } var onlinePaymentEnabled = 0; {else} var onlinePaymentEnabled = 1; {/if}
	var codTabClicked = false;
	{literal}
	var toggle_text_elements = {};
	{/literal}
	var loginid = "{$loginid}";
	var citi_discount = {$citi_discount};
	var icici_discount = {$icici_discount};
	var icici_discount_text = "Avail {$icici_discount}% discount on payment through <img alt='ICICI' src='{$secure_cdn_base}/skin1/images/ICICI.png'> (credit card, debit card or netbanking)";
	var oneClickCod = {$oneClickCod};
	{if $is_icici_gateway_up}
	var is_icici_gateway_up = true;
	{else}
	var is_icici_gateway_up = false;
	{/if}
	{if $shippingRate neq 0}
		{assign var=amountToDiscount value=$totalAmount-$shippingRate}
		var amountToDiscount = {$amountToDiscount};
	{else}
		var amountToDiscount = {$totalAmount};
	{/if}

	{if $citi_discount>0}
		var	citiCardArray = new Array();
		{foreach from=$citi_discount_bins item=array_item key=id}
			citiCardArray.push('{$array_item}');
		{/foreach}
	{/if}

	{if $icici_discount>0}
		var iciciCardArray = new Array();
		{foreach from=$icici_discount_bins item=array_item key=id}
			iciciCardArray.push('{$array_item}');
		{/foreach}
	{/if}

	{if !$paymentServiceEnabled}
	var emiEnabled = false;
	{if $EMIenabled}
		emiEnabled = true;
		
		Myntra = Myntra || {};
		Myntra.Payments = Myntra.Payments || {};
		Myntra.Payments.EMICardArray = {};
		{foreach from=$EMIBanksAvailable item=emibank key=bank}
			(function() {
				var binArray = new Array();
				{foreach from=$emibank.Bins item=array_item key=id}
					binArray.push('{$array_item}');
		{/foreach}
				Myntra.Payments.EMICardArray['{$bank}'] = binArray;
			})();
		{/foreach}
	{/if}
	{/if}
	var mobileVerificationSubmitCount = 0;
	var codCharge = {$cod_charge};
	var orderSummaryDislplayed=false;
	{if $cashcoupons.MRPAmount>0}
		var cashBackAmount = {$cashcoupons.MRPAmount};
	{else}
		var cashBackAmount = 0;
	{/if}

	var userMobileNumber = "{$userMobile}";
	var paymentChildWindow;
	var checkPaymentWindow;
	var paymentProcessingPopup;
	
	$('.your-coupons .tt-ctx').each(function(el, i) {
		var ctx = $(this), 
			tip = ctx.children('.tt-tip');
		Myntra.InitTooltip(tip, ctx,{ldelim}side:'right'{rdelim});
	});
</script>
{/block}

