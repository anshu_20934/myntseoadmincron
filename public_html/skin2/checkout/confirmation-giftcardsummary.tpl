{$rs="<span class=rupees>Rs. </span>"}
<div class="order-summary">
    <h2>Order Summary</h2>
    
	 <div class="total-amount">
	    <span class="red">{$rs} {$amountafterdiscount|round|number_format:0:".":","}</span>
		{if $amountafterdiscount gt 0}
	        <br />
	        {if $paybyoption eq 'cod'}
	        	Cash on Delivery
	        {elseif $paybyoption eq 'netbanking'}
	        	Net-Banking
	        {elseif $paybyoption eq 'debitcard'}
	        	Debit Card
	        {elseif $paybyoption eq 'creditcards'}
	        	Credit Card
	        {elseif $paybyoption eq 'emi'}
	        	EMI (Credit Card)
	        {/if}
	    {/if}
    </div>
	
    <ul class="amount">
        {if $paybyoption eq 'emi'}
	        <li class="emi-charges">
	            <div class="name">EMI Processing Fee</div>
	            {if $emi_charge > 0}
	                <div class="value rupees red">{$rs} {$emi_charge|round|number_format:0:".":","}</div>
	            {else}
	                <div class="value red">Free</div>
	            {/if}
	        </li>
        {/if}
        <li class="order-total">
            <div class="name">Shopping Bag Total</div>
            <div class="value red">
            	{$rs} {math equation="a-b+c" a=$grandTotal b=$eossSavings c=$shippingRate assign=subTotal}{$subTotal|round|number_format:0:".":","}
	            {if isset($eossSavings) && $eossSavings gt 0}
	            	{math equation="d+e" d=$grandTotal e=$shippingRate assign=total}
	            	<span class="strike gray">{$total|round|number_format:0:".":","}</span>
	            {/if}
            </div>
            {if isset($eossSavings) && $eossSavings gt 0}
            	 <div class="value discount red">DISCOUNT {$rs} {$eossSavings|round|number_format:0:".":","}</div>
            {/if}
        </li>
        {if $cartLevelDiscount neq 0}
            <li class="coupon-discount">
                <div class="name">Bag Discount</div>
                <div class="value discount red">(-) {$rs} {$cartLevelDiscount|round|number_format:0:".":","}</div>
            </li>
        {/if}
        {if $codCharges && $codCharges gt 0}
        <li class="cod-charges">
            <div class="name">COD Charges</div>
            <div class="value">{$rs} $codCharges</div>
        </li>
        {/if}
        {if $coupon_discount neq 0}
            <li class="coupon-discount">
                <div class="name">Coupon</div>
                <div class="value discount red">(-) {$rs} {$coupon_discount|round|number_format:0:".":","}</div>
            </li>
        {/if}
        {if $cashCouponCode && $totalcashdiscount gt 0}
            <li class="cash-discount">
                <div class="name">Cashback</div>
                <div class="value discount red">(-) {$rs} {$totalcashdiscount|round|number_format:0:".":","}</div>
            </li>
        {/if}
        <li class="additional-discount hide">
            <div class="name">Additional Discounts</div>
            <div class="value discount red">(-) {$rs} 0.00</div>
        </li>
        <li class="grand-total">
            <div class="name">You {if $paybyoption eq 'cod'}Pay{else}Paid{/if}</div>
	    <div class="you-pay value red">{$rs} {$amountafterdiscount|round|number_format:0:".":","}</div>
        </li>
    </ul>
</div>

