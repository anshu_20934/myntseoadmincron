{foreach $savedItems as $discountId => $productsSet name=combo}
        <div class="prod-set">
        {assign var=isCombo value=false}
        {if $discountId gt 0 }
            {assign var=isCombo value=true}
            {if $productsSet.dre_minMore gt 0}
                {assign var=isConditionMet value=false}
            {else}
                {assign var=isConditionMet value=true}
            {/if}
			<div class="combo">
				<div class="combo-header-main row header">
                    <div class="discount-msg">{include file="string:{$productsSet.label_text}"}</div>
                    <div class="discount-amt">YOU PAY {$rs} <span class="combo-savings-msg">{math equation="a-b" a=$productsSet.comboTotal b=$productsSet.comboSavings assign="comboActualTotal"}{$comboActualTotal|round|number_format:0:".":","}</span> / YOU SAVE {$rs}<span class="combo-savings-msg">{$productsSet.comboSavings|round|number_format:0:".":","}</span></div>
                    <!--<a class="combo-completion-btn btn normal-btn" isConditionMet="{$isConditionMet}"
                        ctype="{if $productsSet.dre_buyCount>0}c{else}a{/if}"
                        min="{if $productsSet.dre_buyCount>0}{$productsSet.dre_buyCount}{else}{$productsSet.dre_buyAmount}{/if}"
                        discountId="{$discountId}">
                        {if $isConditionMet}
                            Edit Combo!
                        {else}
                            Complete Combo Now!
                        {/if}
                    </a>-->
                    <div class="combo-completion-msg">
			{*{if $isConditionMet}*}
			Buy before the discount expires&nbsp;{*else}
			Move to bag to get discount
			{/if*}
		    </div>
            </div>
		{/if}

        {*Combo Loop starts here*}
        {foreach $productsSet.productSet as $itemid => $product name=item}    
	        <div class="row prod-item {if $smarty.foreach.item.first}first{/if}">
	            <div class="col1">
		{if !$product.freeItem && $product.landingpageurl|trim}
	                <a href="{$http_location}/{$product.landingpageurl}" target="_blank" onclick="_gaq.push(['_trackEvent','pdp_click', Myntra.Data.pageName, 'saved_image']);">
		{/if}
	                    <img src="{$product.cartImagePath}" alt="image of {$product.productStyleName}">
                    <!--Kuliza Code start -->   
                    {if $smarty.foreach.item.last}
                        <script>
                            Myntra.Data.cart_page_product_image_url = "{$product.cartImagePath}";
                        </script>
                    {/if}
                    <!--Kuliza Code end -->
		{if !$product.freeItem && $product.landingpageurl|trim}
	                </a>
		{/if}
	            </div>
	            <div class="col2">
	                <form id="cartform_{$itemid}" method="post" action="/mksaveditem.php" class="form-saved-item">
	                    <input type="hidden" name="_token" value="{$USER_TOKEN}">
	                    <input type="hidden" name="itemId" value="{$itemid}">
	                    <input type="hidden" name="styleid" value="{$product.productStyleId}">
	                    <input type="hidden" name="redirectTo" value="cart">
	                    <input type="hidden" name="actionType" value="1">
	
	                <div class="prod-code">Product Code: {$product.productStyleId}</div>
	                <div class="prod-name">
		{if !$product.freeItem && $product.landingpageurl|trim}
	                    <a href="{$http_location}/{$product.landingpageurl}" onclick="_gaq.push(['_trackEvent','pdp_click', Myntra.Data.pageName, 'saved_title']);" target="_blank" >{$product.productStyleName}</a>
		{else}
			<span>{$product.productStyleName}</span>
		{/if}
	                </div>

	                <div class="size-qty-wrap">
	                    <span class="mk-custom-drop-down size">
	                        <label>Size:</label>
	                        {if $product.sizenames && $product.sizequantities}
	                            {if $product.sizenames|@count gt 0}
	                                {if $product.flattenSizeOptions}
	                                    {assign var=productsizenames value=$product.sizenamesunified}
	                                {else}
	                                    {assign var=productsizenames value=$product.sizenames}
	                                {/if}
				{if $product.freeItem}
                                <span class="mk-freesize">{$sizename}</span>
                                {else}
	                                <select class="sel-size" name="{$itemid}size">
	                                {assign var=i value=0}
	                                {foreach from=$productsizenames item=sizename}
	                                    <option value="{$product.skuid}"{if $sizename eq $selectedSize} selected{/if}>{$sizename}</option>
	                                    {assign var=i value=$i+1}
	                                {/foreach}
	                                </select>
	                            {/if}
                            {/if}
	                        {else}
	                            NA
	                        {/if}
	                    </span>
	                    <span class="mk-custom-drop-down qty">
	                        <label>Qty:</label>
	                        {assign var=sku value=$product.productStyleId|cat:'-'|cat:$product.sizename}
	                        {assign var=cssqty value=""}
	                        {if ($checkInStock) and ($product_availability[$itemid] lt $product.quantity
	                                or $product.quantity eq 0 or $total_consolidated_sale[$sku].stockout eq true)}
	                            {assign var=cssqty value="qty-err"}
	                        {/if}
			{if $product.freeItem}
				<span class="mk-freesize">{$product.quantity}</span>
			{else}	
	                        <select class="sel-qty" name="{$itemid}qty" {if $product.productPrice eq 0}disabled="true"{/if}>
	                            {for $i=1 to 10}
	                            <option value="{$i}"{if $i eq $product.quantity} selected{/if}>{$i}</option>
	                            {/for}
	                        </select>
			{/if}
	                    </span>
	                </div>
	                <div class="err item-error">
	                    {if ($checkInStock) and $product.quantity eq 0 }
	                        Zero unit(s) ordered
	                    {elseif ($checkInStock) and $product_availability[$itemid] lte 0}
	                        Sold Out
	                    {elseif ($checkInStock) and $product_availability[$itemid] lt $product.quantity}
	                        Only {$product_availability[$itemid]} units in stock
	                    {/if}
	                </div>
	                </form>
	            </div>
	            <div class="col3">
                <!--Kuliza Code start -->
                {if $smarty.foreach.item.last}
                    {include file="socialAction.tpl" echo_flow="cart" quicklook="no" product_id="{$product.productStyleId}" popupdiv="true"}                
                {/if}
                <!--Kuliza Code end -->
                {if $product.productPrice eq 0 || $product.freeItem}
	                    <div class="amount">FREE</div>
	                {else}
                    {if isset($product.pricePerItemAfterDiscount) && $product.pricePerItemAfterDiscount gte 0}
	                        <div class="discount">
                            <span class="strike rupees gray">{$rs} {$product.productPrice*$product.quantity|round|number_format:0:".":","}</span>
							{if $product.quantity neq $product.discountQuantity}
								<span class="red"> ({$product.discountQuantity} FREE) </span>
							{/if}
								<span> {include file="string:{$product.discountDisplayText}"}</span>
	                        </div>
                        <div class="amount red">{if $product.pricePerItemAfterDiscount eq 0}FREE{else}{$rs} {math equation="a*b" a=$product.pricePerItemAfterDiscount b=$product.quantity assign="priceAfterDiscount"}{$priceAfterDiscount|round|number_format:0:".":","}{/if}</div>
	                    {else}
	                        <div class="amount red">
	                            {$rs} {$product.productPrice*$product.quantity-$product.discountAmount|round|number_format:0:".":","}
	                        </div>
	                    {/if}
	                    <div class="action">
	                        <form method="post" action="/mksaveditem.php" class="move-to-cart">
	                            <input type="hidden" name="actionType" value="moveToCart">
	                            <input type="hidden" name="itemId" value="{$itemid}">
	                            <input type="hidden" name="redirectTo" value="cart">
	                            <input type="hidden" name="_token" value="{$USER_TOKEN}">
	                            <button type="submit" class="mk-move-saved-item-to-bag link-btn"
	                                onclick="_gaq.push(['_trackEvent', 'cart', 'move_saved_item_to_bag'])">Move to bag</button>
	                        </form>
	                        <span class="sep">/</span>
	                        <form method="POST" action="/mksaveditem.php" class="saved-remove-item">
	                            <input type="hidden" name="actionType" value="delete">
	                            <input type="hidden" name="itemId" value="{$itemid}">
	                            <input type="hidden" name="redirectTo" value="cart">
	                            <input type="hidden" name="_token" value="{$USER_TOKEN}">
	                            <button type="submit" class="mk-remove-saved-item link-btn"
	                                onclick="_gaq.push(['_trackEvent', 'cart', 'remove_saved_item'])">Remove</button>
	                        </form>
	                    </div>
	                {/if}
	            </div>
	        </div>
        {/foreach}

        {if $isCombo}
			</div>
        {/if}	        
{/foreach}
