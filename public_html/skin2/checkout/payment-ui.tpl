
<div class="content-hd">
    <div class="amounts two-lines {if !$cashbackApplied and !$loyaltyPointsUsed}hide{/if}">
        <div class="cart-total">Total <span class="rupees">Rs. {$cartTotal|number_format:0:".":","}</span></div>
        <div class="cashback {if !$cashbackApplied and !$loyaltyPointsUsed}hide{/if}">
            <span class="cashback-loyalty-label {if $cashbackApplied and $loyaltyPointsUsed}underlined-linked{/if}">{if $cashbackApplied}Cashback {if $loyaltyPointsUsed}& {/if}{/if}{if $loyaltyPointsUsed}Points {/if}</span><span class="rupees deduction">- Rs. {$totalDeduction|number_format:0:".":","}</span>
            {if $cashbackApplied and $loyaltyPointsUsed}
            <div class="myntra-tooltip calculation-container hide">
                <div class="loyalty-calulation-summary clearfix">
                    <div class="calculation-summary-span">
                        <span class="calculation-cashback">Rs. {$cashbackApplied|number_format:0:".":","}</span>
                        <span class="calculation-loyalty">Rs. {$loyaltyPointsUsedRupees|number_format:0:".":","}</span>
                    </div>
                    <div class="calculation-summary-label">
                        <label>Cashback</label>
                        <label>Points</label>
                    </div>
                </div>
                <div class="loyalty-calculation-total clearfix">
                    <span>Rs. {$totalDeduction|number_format:0:".":","}</span>
                    <label>Total</label>
                </div>
            </div>
            {/if}
        </div>
        <div class="emi-charge hide">
            EMI Processing Fee <span class="rupees">Rs. 0</span>
        </div>
    </div>
    <span class="you-pay {if $cashbackApplied or $loyaltyPointsUsed}hide{/if}">You Pay <span class="rupees">Rs. {$totalAmount|number_format:0:".":","}</span></span>
    <h3>Payment</h3>
</div>
<div class="content-sub-hd {if !$cashbackApplied and !$cashbackBalance and !($loyaltyEnabled && $showLoyaltyBlock)}hide{/if}">
    <span class="you-pay {if !$cashbackApplied and !$loyaltyPointsUsed}hide{/if}">You Pay <span class="rupees">Rs. {$totalAmount|number_format:0:".":","}</span></span>
    {if $loyaltyEnabled and $showLoyaltyBlock}
    <div class="mynt-block">
        <input type="checkbox" id="payment-use-loyalty-points" data-amount="{$totalActiveLoyaltyPoints}" name="useloyaltypoints" value="1" {if $loyaltyPointsUsed}checked{/if} 
            {if !$isLoyaltyPointsAboveThreshold || !$totalActiveLoyaltyPoints}disabled="disabled"{/if}>
        {if $loyaltyPointsUsed or ($totalActiveLoyaltyPoints and $isLoyaltyPointsAboveThreshold)}
                <span class="lbl">{if $loyaltyPointsUsed}Points Redeemed{else}Redeem Points{/if}</span> 
                <span class="balance">
                    {if $loyaltyPointsUsed}
                           / Remaining Balance <span class="{if $activeLoyaltyPointsBalance > 0}rupees{else}balance{/if}">Rs. {$activeLoyaltyPointsBalanceRupees|number_format:0:".":","}</span>
                    {else}
                        <span class="rupees"> / Rs. {$totalActiveLoyaltyPointsRupees|number_format:0:".":","}</span>&nbsp;<a class="loyal-help-icon">[?]</a>
                        <div class="myntra-tooltip hide lp-equivalent-tooltip">
                            {$lpEquivalentPoints} Point{if $lpEquivalentPoints > 1}s{/if} = <span class="rupees">Rs. {$lpEquivalentRupees|number_format:0:".":","}</span> 
                        </div>    
                    {/if}
                </span>
            {elseif $totalActiveLoyaltyPoints and !$isLoyaltyPointsAboveThreshold}
                <span class="balance">Points / {$totalActiveLoyaltyPoints} / </span><span class="lbl loyal-lbl">You need minimum {$loyaltyPointsThreshold} points to use them as cash</span>
            {elseif !$totalActiveLoyaltyPoints}
                <span class="balance">Points / {$totalActiveLoyaltyPoints} / </span><span class="lbl loyal-lbl">Complete this purchase to earn {$loyaltyPointsEarnAfterPurchase} points</span>
        {/if}
    </div>
    {/if}  
    {if $cashbackApplied or $cashbackBalance}
    <div class="mynt-block">
        <input type="checkbox" id="payment-use-cashback" data-amount="{$cashbackBalance}" name="usecashback" value="1" {if $cashbackApplied}checked{/if}>
        <span class="lbl">{if $cashbackApplied}Cashback Used{else}Use Cashback{/if}</span> 
        <span class="balance"> / {if $cashbackApplied}Remaining Balance {/if}<span class="{if $cashbackBalance >0}rupees{else}balance{/if}">Rs. {$cashbackBalance|number_format:0:".":","}</span></span>
    </div>
    {if $totalAmount eq 0 and !$cashbackApplied}  
    <div class="myntra-tooltip hide cashback-not-usable-tooltip clearfix">
        <div class="cashback-not-usable-tooltip-icon"><span class="info-icon"></span></div>
        <div class="cashback-not-usable-tooltip-msg">
            <p>Points will be used first for making payments</p>
            <p>To use cashback, unselect Points & use cashback</p>
        </div>
    </div>  
    {/if}  
    {/if}
</div>

{if $hasPaymentCoupon}
<div class="content-coupon-hd">
    <div class="payment-promotion-box">
      <div class="payment-promotion-label">OFFER</div>
      <div class="payment-promotion-msg">{$appliedCoupon->description}</div>
    </div>
</div>
{/if}

<div class="content-bd">
    {$paymentMarkup}
</div>
{if $hasPaymentCoupon}
<div id="bin-promotion-box" class="lightbox">        
    <div class="mod bin-promotion-error">
        <div class="hd">
            <h2 class="title">Offer Information</h2>
        </div>
        <div class="bd clearfix">
                <div class="red wrong-card-entered">OFFER “{$appliedCoupon->coupon}” IS NOT VALID ON THE CARD YOU HAVE ENTERED</div>
                <div class="red wrong-method-chosen mk-hide">OFFER “{$appliedCoupon->coupon}” WILL NOT BE VALID IF YOU CHOOSE THIS PAYMENT METHOD</div>
                <div class="wrong-card-msg">
                    <div class="wrong-card-entered">Using this card will remove the offer</div>
                    <!-- <div class="wrong-method-chosen mk-hide">Using this payment method will remove the offer</div> -->
                    <div>Total payable amount will increase by <span class="rupees">Rs. {$couponDiscount|number_format:0:".":","}</span> (Coupon Discount)</div>
                </div>

                <div class="confirm-msg">Do you wish to remove this coupon ? </div>
                <center>
                    <div class="offer-action-button-container">
                        <button id="use-another-coupon" class="btn primary-btn" type="button">Use another coupon</button>
                        <div>in your shopping bag</div>
                    </div>
                    <div class="offer-action-button-container">
                        <button id="remove-coupon-conti" data-coupon="{$appliedCoupon->coupon}" class="btn normal-btn" type="button">Remove & continue</button>
                        <div>without this offer</div>
                    </div>
                </center>
        </div>
    </div>

    <div class="mod checking-bin-promotion mk-hide">
        <div class="hd">
            <h2>Checking</h2>
        </div>    
        <div class="bd clearfix">
            <center><img src="{$secure_cdn_base}/images/image_loading.gif"></center>
            <div class="checking-paymentoffer">
                Please wait. Checking the credit/debit card entered for valid promotion.
            </div>
        </div> 
    </div>

    <div class="mod removing-coupon mk-hide">
        <div class="hd">
            <h2>Removing Coupon</h2>
        </div>    
        <div class="bd clearfix">
            <center><img src="{$secure_cdn_base}/images/image_loading.gif"></center>
            <div class="checking-paymentoffer">
                Please wait, while removing coupon from your cart.
            </div>
        </div> 
    </div>
    <div class="mod mk-hide bin-promotion-server-error">
        <div class="hd">
            <h2>Error while processing</h2>
        </div>
        <div class="bd clearfix">
            <div class="processing-payments bd">
                An error occured while processing the request. Please try again.
            </div>
        </div>
    </div>
</div>

{/if}

<div id="splash-payments-processing" class="lightbox">
	<div class="mod" id="processing-payment">
	    <div class="hd" id="splash-payments-processing-hd">
	        <h2>Processing Payment</h2>
	    </div>
	    <div class="bd clearfix" id="splash-payments-processing-bd">
	    	<center><img src="{$secure_cdn_base}/images/image_loading.gif"></center>
	        <div class="processing-payments" id="splash-payments-processing-bd-pp">
	            Enter your card/bank authentication details in the new window once it appears on your screen. <br/>
				Please do not refresh the page or click the back button of your browser.<br/>
				You will be redirected back to order confirmation page after the authentication. It might take a few seconds.<br/><br/>
	        </div>
	        <center><button id="cancel-payment-btn" class="btn primary-btn" type="button">Cancel Payment</button></center>
	    </div>
	</div>
    
    <div class="mod mk-hide" id="result-succPayment">
    	<div class="hd" id="splash-payments-processing-hd-succPayment">
    		<h2>Payment Successfull</h2>
        </div>
        <div class="bd clearfix" id="splash-payments-processing-bd-succPayment">
        	<center><img src="{$secure_cdn_base}/images/image_loading.gif"></center>
    		<div class="processing-payments bd" id="splash-payments-processing-bd-pp-succPayment">
    			Please wait while redirecting
       		</div>
       	</div>
    </div>

    <div class="mod mk-hide" id="result-errPayment">
    	<div class="hd" id="splash-payments-processing-hd-errPayment">
    		<h2>Payment Failure</h2>
        </div>
        <div class="bd clearfix" id="splash-payments-processing-bd-errPayment">
        	<center><img src="{$secure_cdn_base}/images/image_loading.gif"></center>
    		<div class="processing-payments bd" id="splash-payments-processing-bd-pp-errPayment">
    			Please wait while redirecting
    		</div>
        </div>
    </div>

	<div class="mod mk-hide" id="result-payFailOver-err">
    	<div class="hd" id="splash-payments-processing-hd-payFailOver-err">
    		<h2>Payment Failed</h2>
        </div>
        <div class="bd clearfix" id="splash-payments-processing-bd-payFailOver-err">
    		<div class="processing-payments bd" id="splash-payments-processing-bd-pp-payFailOver-err"></div>
        </div>
    </div>

	<div class="mod mk-hide" id="result-payUserClose">
    	<div class="hd" id="splash-payments-processing-hd-payUserClose">
    		<h2>Payment Cancelled</h2>
        </div>
        <div class="bd clearfix" id="splash-payments-processing-bd-payUserClose">
    		<div class="processing-payments bd" id="splash-payments-processing-bd-pp-payUserClose">
                <center>You have cancelled your payment. 
                You can try to pay again {if $codErrorCode eq '0'}or alternatively pay by COD{/if}</center>
	    		<div class="pay-btn-wrap inline-btns">
                    {if $codErrorCode eq '0'}
				   	<a class="btn primary-btn codbtn">Pay by COD</a>
                    {/if}
                    <a class="btn primary-btn tryagainbtn">Try Again</a>
		 		</div>
		 		<div class="close"></div>
    		</div>
        </div>
    </div>
</div>

{include file="giftcards/giftcards-activation-widget.tpl"}

<script>
    Myntra.Data.userEmail = "{$login}";
    Myntra.Data.OrderType = "normal";
    Myntra.Data.citiDiscount = {$citiDiscount};
    Myntra.Data.iciciDiscount = {$iciciDiscount};
    Myntra.Data.shippingAddress = {$shippingAddressJSON};
    Myntra.Data.addresses = {$addressesJSON};

    Myntra.Payments = Myntra.Payments || {ldelim}{rdelim};
    Myntra.Payments.isGiftCardOrder = false;
    Myntra.Payments.showPayHelpline = '{$payHelpline}' == 'test' ? true : false;
    Myntra.Payments.showPayFailOpt = '{$payFailOpt}' == 'test' ? true : false;
    Myntra.Payments.payUserCloseOpt = '{$payUserCloseOpt}' == 'test' ? true : false;
    Myntra.Payments.showPayZipOrders = '{$payUserCloseOpt}' == 'test' ? true : false;
    Myntra.Payments.codErrorCode = {if $codErrorCode eq '0'}true{else}false{/if};


    Myntra.Payments.paymentCoupon = {ldelim}
        hasPaymentCoupon : {if $hasPaymentCoupon}true{else}false{/if},
        allowedTabs : [
        {foreach from=$paymentCouponAllowedOptions item=ao name=allowedOptions}
            '{$ao}'{if !$smarty.foreach.allowedOptions.last},{/if}
        {/foreach} 
        ]
    {rdelim};

    Myntra.Cart = Myntra.Cart || {};
    Myntra.Cart.Data = {ldelim}
        isGiftOrder : {$isGiftOrder},
        items : '{$_cartItemsJson}', 
        amount : {$amount},
        totalAmount : {$totalAmount},
        totalQuantity : {$totalQuantity},
        mrp : {$mrp},
        couponDiscount : {$couponDiscount},
        cashDiscount : {$cashdiscount},
        totalCashBackAmount : {$totalCashBackAmount},
        cashBackAmountDisplayOnCart : {$cashBackAmountDisplayOnCart},
        shippingCharge : {$shippingCharge},
        giftCharge : {$giftCharge},
        savings : {$savings},
        cartLevelDiscount : {$cartLevelDiscount},
        curEvent: "ajaxload",
        {if isset($productAdded)}
                productAdded : {$productAdded},
        {else}
                productAdded : 0,
        {/if}
        {rdelim};

</script>
{*include file="macros/cart-macros.tpl"*}
