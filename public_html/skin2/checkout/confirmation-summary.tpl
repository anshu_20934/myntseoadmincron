{$rs="<span class=rupees>Rs. </span>"}
<div class="order-summary">
    <h2>Order Summary</h2>
    
	 <div class="total-amount">
	    <span class="red">{$rs} {$amountafterdiscount|round|number_format:0:".":","}</span>
		{if $amountafterdiscount gt 0}
	        <br />
	        {if $paybyoption eq 'cod'}
	        	Cash on Delivery
	        {elseif $paybyoption eq 'netbanking'}
	        	Net-Banking
	        {elseif $paybyoption eq 'debitcard'}
	        	Debit Card
	        {elseif $paybyoption eq 'creditcards'}
	        	Credit Card
	        {elseif $paybyoption eq 'emi'}
	        	EMI (Credit Card)
	        {/if}
	    {/if}
    </div>

    {if $userinfo}
    <div class="shipping-address">
        <span class="lbl">Shipping Address</span>
        <address>
            <strong>{$userinfo.s_firstname} {$userinfo.s_lastname}</strong><br>
            {$userinfo.s_address|nl2br}</br>
            {if $userinfo.s_locality}{$userinfo.s_locality}<br>{/if}
            {$userinfo.s_city} - {$userinfo.s_zipcode}, {$userinfo.s_state}<br>
            <span class="lbl">Mobile:</span> {$userinfo.mobile}
        </address>
    </div>
    {/if}
	
	{assign "total" 0}
	{foreach from=$productsInCart item=prod}
		{if $prod.final_quantity}
			{assign total $total+$prod.final_quantity}
		{else}
			{assign total $total+$prod.quantity}
		{/if}
	{/foreach}
	
    <div class="total-items">
        <span class="lbl">{$total} Item(s)</span>
    </div>

    <div class="list">
        {* BEGIN: list of items *}

		{foreach from=$productsInCart item=product}
        	<div class="row prod-item">
        		<div class="col2">
        			<div class="prod-name">
        				{if !$product.freeItem && $product.landingpageurl|trim}<a href="{$http_location}/{$product.landingpageurl}" target="_blank">{/if}
							{$product.productStyleName|truncate:80}
						{if !$product.freeItem && $product.landingpageurl|trim}</a>{/if}
                    </div>

                    <div class="size-qty-wrap">
                        <span class="lbl">Size:</span>
                       	{$product.final_size}
                        <span class="sep">/</span>
                        <span class="lbl">Qty:</span> {if $prod.final_quantity}{$product.final_quantity}{else}{$product.quantity}{/if}
					</div>
	                <div class="col3">
	                	{if $product.totalPrice eq 0 || $product.total_amount eq 0}
	                        <div class="amount red">FREE</div>
	                        <div class="discount red"></div>
	                    {else}
                            <div class="amount red">
	                            {if $product.discount neq 0}
					{math equation="a+b" a=$product.total_amount|floor b=$product.cart_discount_split_on_ratio|floor assign=total_w_itemd_wo_cartd}
					{$rs} {$total_w_itemd_wo_cartd|round|number_format:0:".":","}
	                            	<span class="strike gray">{$product.actual_product_price*$product.quantity|round|number_format:0:".":","}</span>
					{if $product.quantity neq $product.discount_quantity}<span class="red"> ({$product.discount_quantity} FREE) </span>{/if}
				    {else}
					{$rs} {$product.actual_product_price*$product.quantity|round|number_format:0:".":","}
	                            {/if}
                            </div>
                            <div class="discount red">
		                        {if $product.discount neq 0}
		                                {include file="string:{$product.discount_display_text}"}
		                        {/if}
							</div>
	                    {/if}
    	            </div>
        	    </div>
        	</div>
        {/foreach}
        	
        {* END: list of items *}
    </div>

    <ul class="amount">
        <li class="shipping-charges">
            <div class="name">Shipping</div>
            {if $shippingRate > 0}
                <div class="value">{$rs} {$shippingRate|round|number_format:0:".":","}</div>
            {else}
                <div class="value red">Free</div>
            {/if}
        </li>
        {if $paybyoption eq 'emi'}
	        <li class="emi-charges">
	            <div class="name">EMI Processing Fee</div>
	            {if $emi_charge > 0}
	                <div class="value rupees red">{$rs} {$emi_charge|round|number_format:0:".":","}</div>
	            {else}
	                <div class="value red">Free</div>
	            {/if}
	        </li>
        {/if}
	{if $giftamount > 0}
	        <li class="shipping-charges">
	            <div class="name">Gift Charges</div>
	            <div class="value rupees red">{$rs} {$giftamount|round|number_format:0:".":","}</div>
	        </li>
        {/if}
        <li class="order-total">
            <div class="name">Shopping Bag Total</div>
            <div class="value red">
            	{$rs} {math equation="a-b+c+d" a=$grandTotal b=$eossSavings c=$shippingRate d=$giftamount assign=subTotal}{$subTotal|round|number_format:0:".":","}
	            {if isset($eossSavings) && $eossSavings gt 0}
	            	{math equation="d+e+f" d=$grandTotal e=$shippingRate f=$giftamount assign=total}
	            	<span class="strike gray">{$total|round|number_format:0:".":","}</span>
	            {/if}
            </div>
            {if isset($eossSavings) && $eossSavings gt 0}
            	 <div class="value discount red">DISCOUNT {$rs} {$eossSavings|round|number_format:0:".":","}</div>
            {/if}
        </li>
        {if $cartLevelDiscount neq 0}
            <li class="coupon-discount">
                <div class="name">Bag Discount</div>
                <div class="value discount red">(-) {$rs} {$cartLevelDiscount|round|number_format:0:".":","}</div>
            </li>
        {/if}
        {if $codCharges && $codCharges gt 0}
        <li class="cod-charges">
            <div class="name">COD Charges</div>
            <div class="value">{$rs} $codCharges</div>
        </li>
        {/if}
        {if $coupon_discount neq 0}
            <li class="coupon-discount">
                <div class="name">Coupon</div>
                <div class="value discount red">(-) {$rs} {$coupon_discount|round|number_format:0:".":","}</div>
            </li>
        {/if}
        {if $myntCashUsage && $myntCashUsage gt 0}
            <li class="cash-discount">
                <div class="name">Cashback</div>
                <div class="value discount red">(-) {$rs} {$myntCashUsage|round|number_format:0:".":","}</div>
            </li>
        {/if}
        <li class="additional-discount hide">
            <div class="name">Additional Discounts</div>
            <div class="value discount red">(-) {$rs} 0.00</div>
        </li>
        <li class="grand-total">
            <div class="name">You {if $paybyoption eq 'cod'}Pay{else}Paid{/if}</div>
	    <div class="you-pay value red">{$rs} {$amountafterdiscount|round|number_format:0:".":","}</div>
        </li>
    </ul>
</div>

