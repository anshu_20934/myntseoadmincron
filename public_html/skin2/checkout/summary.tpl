
{$rs="<span class=rupees>Rs. </span>"}
<div class="order-summary  {if $isGiftOrder}gift-order-summary{/if}">
    {if $isGiftOrder}
    	<span class="gift-icon"></span>
		<div class="gift-holder">				
			<div class="gift-chkbox-div"> 
				<input type='checkbox' name="gift-wrap-select" class="gift-wrap-payment gift-wrap-select" {if $isGiftOrder}checked="true" {/if}/> <span class="gift-title">This is a gift</span>
				<span {if $isGiftOrder}data-gift-to="{$giftTo}" data-gift-from="{$giftFrom}" data-gift-msg="{$giftMessage|escape}" class="gift-data gift-msg-edit" {else}class="mk-hide gift-msg-edit"{/if}>EDIT MESSAGE</span>
				<p class="gift-wrap-info">Cash on Delivery not available for gift orders</p>
			</div>
		</div>		
	{/if}
    
    <h2>Order Summary</h2>

    <div class="you-pay value total-amount red rupees" data-original-value="{$rs} {$totalAmount|round|number_format:0:'.':','}">
        {$rs} {$totalAmount|round|number_format:0:".":","}
    </div>

    {if !$expressCheckout && $address }
    <div class="shipping-address">
        <span class="lbl">Shipping Address</span>
        <a href="{$https_location}/checkout-address.php?change=1" onClick="_gaq.push(['_trackEvent',{if $flow}'{$flow}' , 'address_edit'{else} 'address', 'edit'{/if},window.location.toString()]);return true;">Add / Edit</a>
        <address>
            <strong>{$address.name}</strong><br>
            {$address.address|nl2br}</br>
            {if $address.locality}{$address.locality}<br>{/if}
            <span class="zipcode-city-state" style="padding:5px;">
            {$address.city} - {$address.pincode}, {$address.statename}</span><br>            
            <span class="lbl">Mobile:</span> {$address.mobile}
        </address>
    </div>
	{/if}
    <div class="total-items">
        <span class="lbl">{$totalItems} Item(s)</span>
       {if !$expressCheckout} <a href="{$http_location}/mkmycart.php" onClick="_gaq.push(['_trackEvent',{if $flow}'{$flow}' , 'order_summary_edit'{else} 'order_summary', 'edit'{/if},window.location.toString()]);return true;">Edit</a>{/if}
    </div>

    <div class="list">
        {* BEGIN: list of items *}
        {* cart_view should be complete|summary. base on this the tpl will render the markup *}
        {$cart_view="summary"}
        {include file="checkout/cart-details.tpl"}
        {* END: list of items *}
    </div>

    <ul class="amount">
        <li class="shipping-charges">
            <div class="name">Shipping</div>
            {if $shippingCharge > 0}
                <div class="value">{$rs} {$shippingCharge|round|number_format:0:".":","}</div>
            {else}
                <div class="value red">Free</div>
            {/if}
        </li>
        {if $giftCharge > 0}
        <li class="shipping-charges">
            <div class="name">Gift Charge</div>
            <div class="value red">{$rs} {$giftCharge|round|number_format:0:".":","}</div>
        </li>
        {/if}
         <li class="emi-charges hide">
            <div class="name">EMI Processing Fee</div>
            <div class="value rupees red">{if $emi_charge > 0}
                {$rs} {$emi_charge|round|number_format:0:".":","}
            {else}
                FREE
            {/if}</div>
        </li>
        <li class="order-total">
            <div class="name">Shopping Bag Total</div>
            <div class="value red">
            	{$rs} {math equation="a-b" a=$amount b=$eossSavings assign=subTotal}{$subTotal|round|number_format:0:".":","}
            	{if isset($eossSavings) && $eossSavings gt 0}
            		<span class="strike gray">{$amount|round|number_format:0:".":","}</span>
            	{/if}
            </div>
            {if isset($eossSavings) && $eossSavings gt 0}
            	 <div class="value discount red">DISCOUNT {$rs} {$eossSavings|round|number_format:0:".":","}</div>
            {/if}
        </li>
        <li class="cod-charges hide">
            <div class="name">COD Charges</div>
            <div class="value">{$rs} 0.00</div>
        </li>
		{if $cartLevelDiscount neq 0}
            <li class="coupon-discount">
                <div class="name">Bag Discount</div>
                <div class="value discount red">(-) {$rs} {$cartLevelDiscount|round|number_format:0:".":","}</div>
            </li>
		{/if}
        {if $couponDiscount neq 0}
            <li class="coupon-discount">
                <div class="name">Coupon</div>
                <div class="value discount red">(-) {$rs} {$couponDiscount|round|number_format:0:".":","}</div>
                <div class="coupon-app-amnt"> your coupon has been applied on</div>
                <div class="coupon-app-amnt"><span class="red">{$couponAppliedItemsCount} item{if $couponAppliedItemsCount gt 1}s{/if}</span> worth <span class="red">{$rs} {$couponAppliedSubtotal|round|number_format:0:".":","}</span></div>
                <div><a class="coupon-info-link" href="#">Know More</a></div>
               		
            </li>
        {/if}
        {if $myntCashUsage neq 0}
            <li class="cash-discount">
                <div class="name">Cashback</div>
                <div class="value discount red">(-) {$rs} {$myntCashUsage|round|number_format:0:".":","}</div>
            </li>
        {/if}
        <li class="additional-discount hide">
            <div class="name">Additional Discounts</div>
            <div class="value discount red">(-) {$rs} 0.00</div>
        </li>
        {if $loyaltyPointsUsageInCash neq 0}
            <li class="loyalty-points-usage-cash">
                <div class="name">Points Redeemed</div>
                <div class="value discount red">(-) {$rs} {$loyaltyPointsUsageInCash|round|number_format:0:".":","}</div>
            </li>
        {/if}
        <li class="grand-total">
            <div class="name">You Pay</div>
            <div class="you-pay value red rupees" >{$rs} {$totalAmount|round|number_format:0:".":","}</div>
        </li>
    </ul>
</div>

