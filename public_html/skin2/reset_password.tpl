{extends file="layout.tpl"}

{block name=body}
	<div class="pw-reset">
		<h1>Reset Your Password</h1>
		{if $link_status eq 'true'}
			<form action="{$https_location}/reset_password.php?key={$reset_key}" method="post" id="reset_form">
				<input type="hidden" value="C" name="usertype">
				<input type="hidden" value="1" name="redirect">
				<input type="hidden" value="login" name="mode">
				<input type="hidden" value="home" name="filetype">
				<input type="hidden" name="reset_key" value="{$reset_key}" />
				<input type="hidden" name="method" value="change_password" />
				<label>New Password</label>
				<input type="password" name="password" id="rp-password" />
				<div class="err rp-status status" ></div>
				<label>Retype New Password</label>
				<input type="password" name="confirm_pwd" id="confirm-password" />
				<div class="err confirm-status status" ></div>
				<hr />
				<button id="reset-submit" type="submit" class="btn primary-btn btn-signup">Change Password</button>
			</form>
		{else}
			<div class="error">{$error_msg}</div>
		{/if}
	</div>
{/block}