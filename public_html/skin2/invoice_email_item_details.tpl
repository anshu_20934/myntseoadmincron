<table cellspacing=0 cellpadding=0 width=99%>
    <tr>
        <td><hr style="border: 1px solid black;"/></td>
    </tr>
    <tr>
    <td>
    {assign var="counter" value="1"}
    {foreach from=$order_details item=product name=item_loop}
    <table cellspacing=0 cellpadding=0 width=99% class="printtable">
        <tr>
            {assign var="skuid" value=$product.sku_id }
            <td colspan=7><b>{$counter}.  {$sku_code_mapping.$skuid} - {$product.productStyleName}  ({$product.article_number})</b></td>
        </tr>
        <tr><td colspan=7>&nbsp;</td></tr>
        
        <tr>
            <td width=5%>&nbsp;</td>
            {if $invoiceType != 'finance'}
	            <td align=center width=10%>Size</td>
	            <td align=center width=10%>Qty</td>
	            <td align=center width=10%>Unit Price</td>
	            <td align=center width=10%>Discount</td>
                    <td align=center width=10%>Coupon</td>
{if isset($product.govt_tax_rate) && $show_govt_tax}
                    <td align=center width=10%>VAT/CST</td>
                    <td align=center width=10%>Tax Amount</td>
{/if}
	            <td align=right width=10%>Sub Total</td>
	            {if $display_for eq 'email' and $product.dispatch_date}
	            	<td align=right width=25%>Expected Dispatch Date</td>
	            {/if}
	        {else}
	        	<td align=center width=13%>Size</td>
            	<td align=center width=13%>Quantity</td>
	            <td align=center width=13%>Tax Rate</td>
	            <td align=center width=13%>Base Price</td>
	            <td align=center width=13%>Taxes</td>
	            <td align=right width=13%>Sub Total</td>
            {/if}
        </tr>
        <tr><td>&nbsp;</td><td colspan=8><hr style="border:1px dotted #AAAAAA;"/></td></tr>
        <tr>
            <td width=10%>&nbsp;</td>
            {if $product.final_size_replaced &&  $product.final_size ne $product.final_size_replaced }
                <td align=center>{$product.final_size} ({$product.final_size_replaced})</td>
            {else}
                <td align=center>{$product.final_size}</td>
            {/if}
            <td align=center>{$product.final_quantity}</td>
            {if $invoiceType != 'finance'}
            	<td align=center>Rs. {$product.unitPrice}</td>
	            <td align=center>Rs. {$product.per_item_discount}</td>
                    <td align=center>Rs. {$product.coupon_discount}</td>
{if isset($product.govt_tax_rate) && $show_govt_tax}
                    <td align=center>{$product.govt_tax_rate}%</td>
                    <td align=center>Rs. {$product.govt_tax_amount}</td>
{/if}
	            <td align=right>Rs. {$product.total_amount - $product.coupon_discount}</td>
	            {if $display_for eq 'email' and $product.dispatch_date}
	            	<td align=right>{$product.dispatch_date}</td>
	            {/if}
            {else}
	            <td align=center>{$product.govt_tax_rate}% </td>
	            <td align=center>Rs. {$product.base_price}</td>
	            <td align=center>Rs. {$product.vatamount_finance}</td>
	            <td align=right>Rs. {$product.total_amount_finance}</td>
            {/if}
        </tr>
        <tr><td colspan=9>&nbsp;</td></tr>
        {assign var=counter value=$counter+1} 
    </table>
    {/foreach}
    </td>
    </tr>
    <tr>
        <td><hr style="border: 1px solid black;"/></td>
    </tr>
</table>
 <div style="clear:both;"></div>
{if $invoiceType != 'finance'}
	{if $amount_details}
	    <div style="width:99%;  line-height: 2em; text-align:left; page-break-before: auto;">
	        <div style="float:left; width: 55%">&nbsp;</div>
	        <div style="float:left; width: 27%; text-align:left;">Shipment Value</div>
	        <div style="float:left; width: 18%; text-align:right;"><b>Rs. {$amount_details.discounted_sub_total}</b></div>
	        <div style="clear:both;"></div>

	        {if $amount_details.pg_discount ne 0 }
	            <div style="float:left; width: 55%">&nbsp;</div>
	            <div style="float:left; width: 27%; text-align:left;">Payment Gateway Discount</div>
	            <div style="float:left; width: 18%; text-align:right;"><b>(-) Rs. {$amount_details.pg_discount}</b></div>
	            <div style="clear:both;"></div>
	        {/if}
	        {if $amount_details.cash_redeemed ne 0 }
	            <div style="float:left; width: 55%">&nbsp;</div>
	            <div style="float:left; width: 27%; text-align:left;">Cashback Used</div>
	            <div style="float:left; width: 18%; text-align:right;"><b>(-) Rs. {$amount_details.cash_redeemed}</b></div>
	            <div style="clear:both;"></div>
	        {/if}
                {if $amount_details.loyalty_credit ne 0 }
	            <div style="float:left; width: 55%">&nbsp;</div>
	            <div style="float:left; width: 27%; text-align:left;">Loyalty Credit</div>
	            <div style="float:left; width: 18%; text-align:right;"><b>(-) Rs. {$amount_details.loyalty_credit}</b></div>
	            <div style="clear:both;"></div>
	        {/if}
	        {if $amount_details.gift_charges ne 0 }
	            <div style="float:left; width: 55%">&nbsp;</div>
	            <div style="float:left; width: 27%; text-align:left;">Gift Charges</div>
	            <div style="float:left; width: 18%; text-align:right;"><b>Rs. {$amount_details.gift_charges}</b></div>
	            <div style="clear:both;"></div>
	        {/if}
	        
	        
	        {if $amount_details.cod ne 0 }
	            <div style="float:left; width: 55%">&nbsp;</div>
	            <div style="float:left; width: 27%; text-align:left;">COD Charges</div>
	            <div style="float:left; width: 18%; text-align:right;"><b>Rs. {$amount_details.cod}</b></div>
	            <div style="clear:both;"></div>
	        {/if}
	        {if $amount_details.payment_surcharge ne 0 }
	            <div style="float:left; width: 55%">&nbsp;</div>
	            <div style="float:left; width: 27%; text-align:left;">EMI Charges</div>
	            <div style="float:left; width: 18%; text-align:right;"><b>Rs. {$amount_details.payment_surcharge}</b></div>
	            <div style="clear:both;"></div>
	        {/if}
	        {if $amount_details.refundable_amount && $amount_details.cashback ne 0 }
	            <div style="float:left; width: 55%">&nbsp;</div>
	            <div style="float:left; width: 27%; text-align:left;">Cashback Credit Revert</div>
	            <div style="float:left; width: 18%; text-align:right;"><b>(-) Rs. {$amount_details.cashback}</b></div>
	            <div style="clear:both;"></div>
	        {/if}
	        {if $amount_details.difference_refund ne 0 }
	            <div style="float:left; width: 55%">&nbsp;</div>
	            <div style="float:left; width: 27%; text-align:left;">Price mismatch refund</div>
	            <div style="float:left; width: 18%; text-align:right;"><b>(-) Rs. {$amount_details.difference_refund}</b></div>
	            <div style="clear:both;"></div>
	        {/if}
            {if $amount_details.tax ne 0 }
	            <div style="float:left; width: 55%">&nbsp;</div>
	            <div style="float:left; width: 27%; text-align:left;">VAT Collected</div>
	            <div style="float:left; width: 18%; text-align:right;"><b>Rs. {$amount_details.tax}</b></div>
	            <div style="clear:both;"></div>
	        {/if}

		    <div style="float:left; width: 55%">&nbsp;</div>
		    <div style="float:left; width: 27%; text-align:left;">Shipping</div>
		    <div style="float:left; width: 18%; text-align:right;"><b>{if $amount_details.shipping_cost ne 0 }Rs. {$amount_details.shipping_cost}{else}FREE{/if}</b></div>
		    <div style="clear:both;"></div>

            {if $amount_details.gift_card_amount ne 0 }
                <div style="float:left; width: 55%">&nbsp;</div>
                <div style="float:left; width: 27%; text-align:left;">Gift Card Amount</div>
                <div style="float:left; width: 18%; text-align:right;"><b>Rs. {$amount_details.gift_card_amount}</b></div>
                <div style="clear:both;"></div>
            {/if}
	    
		    {if $amount_details.refundable_amount}
		        <div style="float:left; width: 55%; margin-top: 10px;">&nbsp;</div>
		        <div style="float:left; width: 45%; margin-top: 10px;">
		            <div style="padding: 10px 0px; border-top: solid 0.15em; border-bottom: 0.15em solid;">
		            {if $amount_details.refundable_amount < 0}
		            	<div style="width:50%; float:left;">Amount Debited from Cash-back account</div>
		            	<div style="text-align:right; float:right; width:50%;"><b>Rs. {$amount_details.refundable_amount*-1}</b></div>
		            {else}
		                <div style="width:50%; float:left;">Refund Amount</div>
		                <div style="text-align:right; float:right; width:50%;"><b>Rs. {$amount_details.refundable_amount}</b></div>
		            {/if}
		                <div style="clear:both"></div>
		            </div>
		        </div>
		        <div style="clear:both"></div>
		    {else}
		        {if $amount_details.payment_method eq 'cod' }
		            <div style="float:left; width: 55%; margin-top: 10px;">
		                {if $display_for eq 'invoice'}
			                <div style="margin-right:25px; padding:10px; float:right; border: solid 0.15em; width: 150px; font-size: 14px;">
			                    <b>CASH ON DELIVERY</b>
			                </div>
			            {else}&nbsp;{/if}
		            </div>
		            <div style="float:left; width: 45%; margin-top: 10px;">
		                <div style="padding: 10px 0px; border-top: solid 0.15em; border-bottom: 0.15em solid;">
		                    <div style="width:50%; float:left;">Amount to be paid</div>
		                    <div style="text-align:right; float:right; width:50%;">
		                        {if $display_for eq 'invoice'}
		                        <strong style="font-size: 20px;">Rs. {$amount_details.payable_amount}</strong>
		                        {else}
		                        <strong style="font-size: 17px;">Rs. {$amount_details.payable_amount}</strong>
		                        {/if}
		                    </div>
		                    <div style="clear:both;"></div>
		                </div>
		           </div>
		           <div style="clear:both"></div>
		        {else}
		            <div style="float:left; width: 55%; margin-top: 10px;">
		                {if $display_for eq 'invoice'}
			                <div style="margin-right:25px; padding:10px 20px; float:right; border: solid 0.15em; width: 80px; font-size: 14px;">
			                    <b>PREPAID</b>
			                </div>
		                {else}&nbsp;
		                {/if}
		            </div>
		            <div style="float:left; width: 45%; margin-top: 10px;">
		                <div style="padding: 10px 0px; border-top: solid 0.15em; border-bottom: 0.15em solid;">
		                    <div style="width:50%; float:left;">Amount paid</div>
		                    <div style="text-align:right; float:right; width:50%;">
		                        {if $display_for eq 'invoice'}
		                        <strong style="font-size: 20px;">Rs. {$amount_details.payable_amount}</strong>
		                        {else}
		                        <strong style="font-size: 16px;">Rs. {$amount_details.payable_amount}</strong>
		                        {/if}
		                    </div>
		                    <div style="clear:both"></div>
		                </div>
		           </div>
		           <div style="clear:both"></div>
		       {/if}
		    {/if}
	    </div>
	{/if}
	
	 <div style="clear:both"></div>
	{if $other_shipment_items}
		<div id="other_shipments">
		    <div style="height: 100px;">&nbsp;</div>
		    <strong style="font-size: 16px;">Items not part of this shipment</strong>
		    <table id="other_shipments" cellspacing=0 cellpadding=0 width=99% class="printtable">
		        <tr><td colspan=4><hr style="border: 1px dotted #aaaaaa;"/></td></tr>
		        <tr><td width=50%>Product Description</td><td width=15%>Size</td><td width=15%>Quantity</td><td width=20%>Status</td></tr>
		        <tr><td colspan=4><hr style="border: 1px dotted #aaaaaa;"/></td></tr>
		        {assign var="counter" value="1"}
		        {foreach from=$other_shipment_items item=product}
		            <tr>
		                <td style="line-height: 1.2em;">{$counter}. {$product.productStyleName}</td>
		                <td style="line-height: 1.2em;">
		                    {if $product.final_size_replaced &&  $product.final_size ne $product.final_size_replaced }
		                        {$product.final_size} ({$product.final_size_replaced})
		                    {else}
		                        {$product.final_size}
		                    {/if}
		                </td>
		                <td style="line-height: 1.2em;">{$product.quantity}</td>
		                <td style="line-height: 1.2em;">{if $product.item_status eq 'S' or $product.item_status eq 'D'}Shipped{else}Pending{/if}</td>
		            </tr>
		            {assign var=counter value=$counter+1} 
		        {/foreach}
		        <tr><td colspan=4><hr style="border: 1px dotted #aaaaaa;"/></td></tr>
		        <tr><td colspan=4 align=center style="color: #757575; font-size: 12px;">Pending or Shipped items will be delivered to you separately.</td></tr>
		    </table>
		</div>
		<div style="clear:both;"></div>
	{else}
	    {if $display_for eq 'invoice'}
	        <div style="height: 80px;">&nbsp;</div>
	    {else}
	        <div style="height: 30px;">&nbsp;</div>
	    {/if}
	{/if}
{else}
	<div style="float:left; width: 45%; margin-top: 10px; padding-left:400;">
	    <div style="padding: 10px 0px; border-top: solid 0.15em; border-bottom: 0.15em solid;">
	        <div style="width:50%; float:left;">Grand Total</div>
	        <div style="text-align:right; float:right; width:50%;">
	            <strong style="font-size: 20px;">Rs. {$grand_total}</strong>
	        </div>
	        <div style="clear:both"></div>
	    </div>
   </div>
   <div style="clear:both"></div>
{/if}
