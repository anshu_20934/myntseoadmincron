<header class="mk-site-hd mk-cf">
    <script>
        {if $usern != "" || $passwd != ""}
            var showLogin = true;
        {/if}
    </script>
    {if $telesales}
     <script>
        var telesales = true;
     </script>

     <div style="color: #fff;background-color: #333333 ;text-transform:none;padding:18px 0px;">Telesales User: <b>{$telesalesUser}</b></div>
     {else}
       <script>
        var telesales = false;
       </script>
    {/if}

   	<div class="customer-promises mk-cf">
   	    <span class="mk-f-left">{$newUiTopCpsMessage}</span>
   	    <span class="mk-f-right">
   	        {if !$contactUsLink}
   	            HELPLINE <em>{$customerSupportCall}</em>
   	        {else}
   	            {$contactUsLink}{" / "}{$myOrdersLink}
            {/if}
   	    </span>
   	</div>
	<h1 class="mk-f-left">
		<a href="/"><img src="{$cdn_base}/skin2/images/logo.png" alt="Myntra" title="Online Shopping store for Branded Shoes, Clothing, Accessories & Fashion wear in India." /></a>
	</h1>
	<form class="mk-search-form mk-cf mk-f-right clearfix" action="" method="" onsubmit="return menuSearchFormSubmit(this);">
		<input type="text" value="" autocomplete="off" class="mk-site-search mk-placeholder mk-f-left" id="search_query" />
		<span class="mk-placeholder-text">{$searchBoxText}</span>
		<button type="submit" name="" class="mk-site-search-btn mk-f-left"></button>
	</form>
    {$pn = $smarty.server.REQUEST_URI|regex_replace:"/\?.*$/":""}
	{if $login}
	    <p class="mk-welcome welcome-logout mk-f-left">Welcome, {if $userfirstname neq ""}{$userfirstname|truncate:15}{else}{$login|truncate:15}{/if}&nbsp;<span id="pipe">|</span>&nbsp;{$promoHeaderLoggedinText}
	        <span id="header-logout-link" class="mk-sign-out">Logout</span>
	    </p>
	    <form id="header-logout-form" method="post" action="{$http_location}/include/login.php">
	        <input type="hidden" name="mode" value="logout">
	        <input type="hidden" name="_token" value="{$USER_TOKEN}">
	    </form>
	{elseif $pn eq "/register.php"}
		<p class="mk-welcome mk-f-left link-btn right" >&nbsp</p>
	{else}
		<a class="mk-welcome mk-f-left btn small-btn primary-btn btn-login right">Login / Sign Up</a>{$promoHeaderText}
		<input type="hidden" id="loginSplashVariant" value="{$loginSplashVariant}" />
	{/if}
	<ul class="mk-utility mk-f-right">
		{if $login}
			{*if $lbId gt 0*}
			{if 1 eq 2}
				<li class="mk-header">
					<em class="mk-mymyntra" style="text-decoration:none;cursor:auto;">Happy Hour</em>
					<span class="mk-count">
						<a href="{$http_location}/leaderboard.php?lbId={$lbId}">Leader Board</a>
					</span>
				</li>
			{/if}
			<li class="{if !$mymyntra}mk-header-hover{/if}"><a href="{$http_location}/mymyntra.php">
				<em class="mk-mymyntra" data-id="mymyntra" data-show="true" data-load="true">My Myntra
					<div class="mk-arrow mk-hide"><span></span></div>
				</em>
				{if $myntCashDetails.balance > 0}<span class="mk-count rupees">{$rupeesymbol} {$myntCashDetails.balance|round|number_format:0:".":","}</span>
				{else}<span class="mk-count">{$myMyntraCredits.couponsCount} {if $myMyntraCredits.couponsCount eq 1}Coupon {else}Coupons{/if}</span>
				{/if}
			</a></li>
		{/if}
		{if $showRecent && !$recentViewedCount eq 0}
			<li class="{if !$mymyntra}mk-header-hover{/if}"><a href="{$http_location}/recently-viewed">
				<em class="mk-recent " data-id="recent" data-show="{if $recentViewedCount eq 0}false{else}true{/if}" data-load="true">Recently Viewed
					<div class="mk-arrow mk-hide"><span></span></div>
				</em>
				<span class="mk-count">[<strong>{$recentViewedCount}</strong>]</span>
			</a></li>
		{/if}
		{if $login}
			<li class="{if !$mymyntra}mk-header-hover{/if}"><a href="{$cartPageUrl}#wishlist">
				<em class="mk-saved " data-id="saved" data-show="{if $savedItemCount eq 0}false{else}true{/if}" data-load="true">My Wishlist
					<div class="mk-arrow mk-hide"><span></span></div>
				</em>
				<span class="mk-count">
					{if !$savedItemCount eq 0}[<strong>{$savedItemCount}</strong>]
					{else}Empty{/if}
				</span>
			</a></li>
		{/if}
		<li class="{if !$mymyntra}mk-header-hover{/if}"><a href="{$cartPageUrl}">
			<div class="bag-cont">
				<em class="mk-bag " data-id="bag" data-show="{if $cartTotalQuantity eq 0}false{else}true{/if}" data-load="true">My Bag
					<div class="mk-arrow mk-hide"><span></span></div>
				</em>
				<span class="mk-count">
					{if !$cartTotalQuantity eq 0}
						<span class="rupees red">{$rupeesymbol} {$totalAmountCart|round|number_format:0:".":","}</span>
						<span>[<strong>{$cartTotalQuantity}</strong>]</span>
					{else}
						Empty
					{/if}
				</span>
			</div>
			<span class="bag-icon"></span>
		</a></li>
	</ul>
</header>

