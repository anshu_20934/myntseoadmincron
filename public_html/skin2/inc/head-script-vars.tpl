var _gaq = _gaq || [];
        var pluginUrl ='//www.google-analytics.com/plugins/ga/inpage_linkid.js';
        _gaq.push(['_require', 'inpage_linkid', pluginUrl]);
        _gaq.push(['_setAccount', '{$ga_acc}']);
        _gaq.push(['_setDomainName', '{$cookiedomain}']);
        var http_loc = '{$http_location}';
        var https_loc = '{$https_location}';
        var http_base_url = location.protocol === "https:" ? https_loc : http_loc;
        var cdn_base = '{$cdn_base}';
        var secure_cdn_base = '{$secure_cdn_base}';
        var cdn_base_url = location.protocol === "https:" ? secure_cdn_base : cdn_base;
        var cdn_base = location.protocol === "https:" ? secure_cdn_base : cdn_base;
        var facebook_app_id = '{$facebook_app_id}';
        var facebook_app_name='{$facebook_app_name}';
        var fbFacepileSrc = "https://www.facebook.com/plugins/facepile.php?app_id={$facebook_app_id}";
        var s_account= '{$analytics_account}';
        var social_share_variant='{$socialShareVariant}';
        {literal}
        var Myntra = Myntra || {};
        Myntra.Data = Myntra.Data || {};
        Myntra.Cart = Myntra.Cart || {};
        Myntra.Cart.Data = Myntra.Cart.Data || {};
        {/literal}
        Myntra.Data.showNewSignUpConfirmation = '{$showNewSignUpConfirmation}';
	Myntra.Data.fbFacepileSrc = "https://www.facebook.com/plugins/facepile.php?app_id={$facebook_app_id}";
	Myntra.Data.splashDelay = "{$splashDelay}";
	if (Myntra.Data.splashDelay="")
		Myntra.Data.splashDelay = "0";
Myntra.Data.NudgeOpen="0";	
    Myntra.Data.nologin = "{$nologin}"; 
        Myntra.Data.userEmail = "{$login}";
        Myntra.Data.userHashId = "{$AVAIL_USER_ID_MD5}";
        Myntra.Data.userFBId ="{$facebook_uid}";
        Myntra.Data.socialAction = 'wishlist';
        Myntra.Data.socialExcludeText = "{$socialPermissionCategoryExcludeText}";
        Myntra.Data.nonFBRegCouponValue = "{$mrp_nonfbCouponValue}";
        Myntra.Data.fbRegCouponValue = "{$mrp_fbCouponValue}";
        Myntra.Data.showLogin = "{$showLogin}";
Myntra.Data.phoneNumNotRequired = "{$phoneNumNotRequired}";
Myntra.Data.regV2 = "{$loginSignupV2ABTest}";
        Myntra.Data.RegCouponSignUpOverLayList = "{$regCouponSignUpOverLayList}";
        Myntra.Data.FBRegCouponSignUpOverLayList = "{$fBregCouponSignUpOverLayList}";
	Myntra.Data.Nudge = "{$nudgeABTest}";
	Myntra.Data.NudgeShow = "{$nudgeShowABTest}";
        Myntra.Data.token = "{$USER_TOKEN}";
	Myntra.Data.regV2 = "{$loginSignupV2ABTest}";
        Myntra.Data.showAutoSuggest = "{$enableAutoSuggest}";
        Myntra.Data.showAdminAdditionalStyleDetails = "{$showAdminAdditionalStyleDetails}";   	
		Myntra.Data.cookiedomain = "{$cookiedomain}";
	    Myntra.Data.cookieprefix = "{$cookieprefix}";
        Myntra.Data.loginTimeout = {if $loginTimeout}{$loginTimeout*1000}{else}15000{/if};
        Myntra.Data.enableGuestLogin = {if $enableGuestLogin}1{else}0{/if};
        Myntra.Data.configMode = "{$configMode}";
        Myntra.Data.enableSpy = {if $enableSpy}1{else}0{/if};
        Myntra.Data.spyId = {if $enableSpy}"{$spyId}"{else}undefined{/if};
        Myntra.Data.pageName="static";
        Myntra.Data.lazyElems = new Array();
        Myntra.Data.lazySeparator = "#!#";
        Myntra.Data.ServerCheck = "{$nmServerName}";
        Myntra.Data.EmailUUID = {if $email_uuid}"{$email_uuid}"{else}""{/if};
        Myntra.Data.shoppingFestivalEnabled = {if $shoppingFestivalEnabled}true{else}false{/if};
        Myntra.Data.festivalColumnImageLink = "{$festivalColumnImageLink}";
        Myntra.Data.notificationsEnabled = "{$notificationsEnabled}";
        Myntra.Data.notificationCount = "{$notificationCount}";
        Myntra.Data.newNotificationCount = "{$newNotificationCount}";
        Myntra.Data.gtmLazyLoadEnabled = {if $isGTMLazyLoadEnabled}true{else}false{/if};

        Myntra.Data.exchangesEnabled = {if $exchanges_enabled eq "true"}true{else}false{/if};
        Myntra.Data.loginBigBannersEnabled = {if $loginBigBannersEnabled eq 1}true {else}false{/if};
        Myntra.Data.loginBigBanners ={if $loginBigBannersEnabled eq 1} Array("{$newLoginBanners[0]['image_url']}","{$newLoginBanners[1]['image_url']}") {else} null {/if};
    Myntra.Data.loginSignupAmt = "{$signupAmt}";

        Myntra.Data.UTM = Myntra.Data.UTM || {};
        Myntra.Data.UTM.Source = "{$utm_source|escape:'javascript'}";
        Myntra.Data.UTM.Medium = "{$utm_medium|escape:'javascript'}";
        Myntra.Data.UTM.Campaign = "{$utm_campaignName|escape:'javascript'}";
        Myntra.Data.UTM.CampaignId = "{$utm_campaignId|escape:'javascript'}";
        Myntra.Data.UTM.OctaneEmail = "{$utm_octaneEmail|escape:'javascript'}";
        Myntra.Data.Device = Myntra.Data.Device || {};
        Myntra.Data.Device.isTablet = {$isTablet};
        Myntra.Data.Device.isMobile = {$isMobile};
        Myntra.Data.Device.Type = "{$deviceType}";
        Myntra.Data.Device.Name = "{$deviceName}";
        
        Myntra.Cart.Data.totalAmount = '{$totalAmountCart}';
        Myntra.Cart.Data.totalQuantity = '{$cartTotalQuantity}';
        Myntra.Cart.Data.savedQuantity = '{$savedItemCount}';
        Myntra.Cart.Data.userCB = '{$myntUserCB}';
        
        {if $layoutVariant eq 'test'}
            Myntra.Data.newLayout = true;
        {else}
            Myntra.Data.newLayout = false;
        {/if}
        {if isset($prompt_login)}
            Myntra.Data.promptLogin={$prompt_login};
        {/if}
        Myntra.Data.cartPageUrl = '{$cartPageUrl}';
        Myntra.Data.loyaltyEnabled = '{$loyaltyEnabled}';
        Myntra.Data.defaultMyntcreditTab = '{$defaultMyntcreditTab}';
		Myntra.Data.cartTotalAmount =  '{$cart_total_amount}';
		Myntra.Data.cartTotalQuantity = '{$cart_total_quantity}';
        Myntra.Data.isLoggedIn = '{if $login}1{else}0{/if}';
        Myntra.Data.isBuyer = '{if $returningCustomer}1{else}0{/if}';
        Myntra.Data.isBuyerCookie = '{if $returningCustomerCookie}1{else}0{/if}';
        var dataLayer = dataLayer || [];
    Myntra.Data.showLoginCaptcha = {if $showLoginCaptcha}1{else}0{/if};
	Myntra.Data.rememberUser = '{$remember_user}';

        Myntra.Data.SearchRecentWidgetEnabled = {if $searchRecentWidgetEnabled}true{else}false{/if};
