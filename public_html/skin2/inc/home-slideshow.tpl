
<div class="mk-marquee{if $newHpBanners} new-hp-banner-wrapper{/if}">
	<div class="mk-cycle-wrapper {if $slideShowBanners}mk-banner-wrapper{/if}">
	{foreach item=slideShowImage from=$slideShowBanners }	
		<li>
			<a {if $slideShowImage.target_url|strpos:"http" === 0}
				href="{$slideShowImage.target_url}" target="_blank"
			{else}
				href="{$slideShowImage.target_url}"
			{/if} onClick="_gaq.push(['_trackEvent', 'slide_show', '{$slideShowImage.target_url}', 'click']);return true;">
				<img alt="{$slideShowImage.alt_text}"
					{if $slideShowImage@first}
						src="{myntraimage key="slideshow" src=$slideShowImage.image_url}"
					{else}
						src="{$cdn_base}/skin2/images/spacer.gif" class="lazy"
						data-lazy-src="{myntraimage key="slideshow" src=$slideShowImage.image_url}"
					{/if}
				/>
			</a>
			{if $slideShowImage.multiclick eq 1}
				{if $slideShowImage.group_id}
					{$slideShowImage.image_id = "{$slideShowImage.image_id}-{$slideShowImage.group_id}"}
				{/if}
				<div class="{if $slideShowImage@first}multiContainer{else}multiContainer multiContainerHide{/if}" id="multi{$slideShowImage.image_id}">
					{foreach name=multiClickData item=multiClickData from=$multiBannerData.{$slideShowImage.image_id}}
						<a href="{$multiClickData.target_url}" title="{$multiClickData.title}" style="left:{$multiClickData.coordinates.0}px; top:{$multiClickData.coordinates.1}px; width:{$multiClickData.width}px; height:{$multiClickData.height}px;">&nbsp;</a>
					{/foreach}
				</div>			
			{/if}
		</li>
	{/foreach}
	</div> <!-- End .cycle-wrapper --> 

	<div class="mk-cycle-nav"></div> <!-- End .cycle-nav -->

</div><!-- End .marquee -->
