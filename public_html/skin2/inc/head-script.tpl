{if $mymyntra}
    {assign var="curCDN" value=$secure_cdn_base scope="root"}
{else}
    {assign var="curCDN" value=$cdn_base scope="root"}
{/if}
    <script>
        {include file="inc/head-script-vars.tpl"}
        {literal}
        // tracking wrapper: args are same as GA _trackevent 
        function myntrack(category, action, opt_label, opt_value, opt_noninteraction) {
            var args = Array.prototype.slice.call(arguments);
            if (args.length < 3) { return; }
            args.unshift('_trackEvent');
            _gaq.push(args);
        }
        {/literal}
    </script>
	<!--[if lt IE 9]>
        {if $smarty.server.HTTPS}
		<script src="{$secure_cdn_base}/skin2/js/html5shiv.js.jgz"></script>
        {else}
		<script src="{$cdn_base}/skin2/js/html5shiv.js.jgz"></script>
        {/if}
	<![endif]-->

    {*
    {if $smarty.get.log4js}
    <script src="/skin2/js/log4javascript.js"></script>
    <script>
        var log = log4javascript.getDefaultLogger();
        log.debug("BEGIN Logging");
    </script>
    {else}
    <script>
        {literal}
        var log = {
            trace:function(){},
            debug:function(){},
            info:function(){},
            warn:function(){},
            error:function(){},
            fatal:function(){},
        };
        {/literal}
    </script>
    {/if}
    *}

    {include file="inc/newrelic-header.tpl"}
    {if $tracelytics_header}
        {$tracelytics_header}
    {/if}
    <script>
        var trackNode = ((document.getElementsByTagName('head') || [null])[0] || 
                        document.getElementsByTagName('script')[0].parentNode); 
    </script>
    
    <!-- Kuliza Code Start : To make kulizaechoenabled available on all page -->
    <script>
        {if $isKulizaEchoEnabled}
            var kulizaEchoEnabled = 1;
        {else}
            var kulizaEchoEnabled = 0;
        {/if}
        {literal}
            Myntra.Kuliza = {};
        {/literal}
        Myntra.Kuliza.echoBaseUrl = '{$kulizaEchoBaseUrl}';
        Myntra.Kuliza.clientDomain = '{$kulizaClientDomain}';
        Myntra.Kuliza.clientBaseUrl = '{$kulizaClientBaseUrl}';
        
    </script>
    <!-- Kuliza Code End -->
