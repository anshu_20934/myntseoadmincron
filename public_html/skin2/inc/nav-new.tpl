<section class="mk-site-nav-wrapper">
	<nav class="mk-site-nav">
		<div class="mk-level-1 mk-f-left">
			<ul>
				<li>
                    <a href="/"><span class="home-icon"></span></a>
                </li>
 			    {foreach key=indexA item=itemsA from=$topNavigationWidget name=topNavigationWidgetLoop}
					<li>
						<a class="mk-level1 mk-level1-link index_{$itemsA.display_order} {if $nav_selection_arr[$itemsA.id]}mk-active mk-persist mk-hover{/if} {if $expandMenu == 'true'}mk-showMenu{/if}" 
							data-index="{$itemsA.display_order}"
							{if $itemsA.link_style} style="{$itemsA.link_style}" {/if}
							href="{$itemsA.link_url}?nav_id={$itemsA.id}&src=tn" {if $itemsA.link_url|strpos:"http" === 0}rel="nofollow" target="_blank"{/if} 
							onClick="_gaq.push(['_trackEvent', 'megamenu', Myntra.Data.pageName, '{$itemsA.link_url}']);return true;">{$itemsA.link_name}</a>
					</li>
    			{/foreach}
			</ul>
		</div>
		{if $login}
		<script type="text/javascript">
			Myntra.TopNav = Myntra.TopNav || {};
			Myntra.TopNav.level2 = {};
		</script>
		{/if}
	    {foreach key=indexA item=itemsA from=$topNavigationWidget name=topNavigationWidgetLoop}
	        {if $itemsA.child|count gt 0}
	            <div class="mk-level-2 mk-f-left index_{$itemsA.display_order} mk-hide" data-index="{$itemsA.display_order}">
                    {assign var='hasImages' value=false}
                    {if $itemsA.child[$itemsA.child|key].link_name|stristr:'<img'}
                    	{assign var='hasImages' value=true}
                    {/if}
	            	<ul{if $hasImages} class="images"{/if}>
	                    {assign var=i value=0}
	                    {assign var="level2Categories" value = '<li>
		                    {foreach key=indexB item=itemsB from=$itemsA.child}
		                    	{if $i%11}{assign var=i value=$i+1}<div class="filler"></div>{if $i%11 eq 10}{assign var=i value=$i+1}<div class="filler"></div>{/if}{if $i%11 eq 0}</li><li>{/if}{/if}
		                    	<a class="mk-level2 mk-level2-category {if $nav_selection_arr[$itemsB.id]}mk-active{/if}" 
		                    		href="{$itemsB.link_url}?nav_id={$itemsB.id}&src=tn" {if $itemsB.link_url|strpos:"http" === 0}rel="nofollow" target="_blank"{/if} 
		                    		{if $itemsB.link_style} style="{$itemsB.link_style}" {/if} onClick="_gaq.push([\'_trackEvent\', \'megamenu\', Myntra.Data.pageName, \'{$itemsB.link_url|escape}\']);return true;">
		                            {if $itemsA.link_name eq "Men" or $itemsA.link_name eq "Women"}
		                            	{$itemsA.link_name}\'s {$itemsB.link_name} 
		                            {else}
		                                 {$itemsB.link_name}
		                            {/if}
		                        </a>
		                        {assign var=i value=$i+1}
		                        {if $i%11 eq 0}</li><li>{/if}
		                        {foreach key=indexC item=itemsC from=$itemsB.child}
		                            <a class="mk-level2 {if $nav_selection_arr[$itemsC.id]}mk-active{/if}" href="{$itemsC.link_url}?nav_id={$itemsC.id}&src=tn" {if $itemsC.link_style} style="{$itemsC.link_style}" {/if} onClick="_gaq.push([\'_trackEvent\', \'megamenu\', Myntra.Data.pageName, \'{$itemsC.link_url}\']);return true;">{$itemsC.link_name}</a>
		                            {assign var=i value=$i+1}
		                            {if $i%11 eq 0 && !($itemsB@last && $itemsC@last)}</li><li>{/if}
		                        {/foreach}                    
		                    {/foreach}
	                    </li>
	                    {if $navbarSlideShowImages[$itemsA.link_name]}
	                   		<li class="mk-level2-category-slideshow">
	                    		<a href="{$navbarSlideShowImages[$itemsA.link_name][0].target_url}" onClick="_gaq.push([\'_trackEvent\', \'megamenu\', Myntra.Data.pageName, \'{$navbarSlideShowImages[$itemsA.link_name][0].image_url|replace:$curBaseCDN:$curCDN}\']);return true;" ><img alt="{$navbarSlideShowImages[$itemsA.link_name][0].alt_text}" class="lazy" data-lazy-src="{myntraimage key="topnav_img" src=$navbarSlideShowImages[$itemsA.link_name][0].image_url|replace:$curBaseCDN:$curCDN}"/></a>
	                    		<a style="display: none;" href="{$navbarSlideShowImages[$itemsA.link_name][1].target_url}" onClick="_gaq.push([\'_trackEvent\', \'megamenu\', Myntra.Data.pageName, \'{$navbarSlideShowImages[$itemsA.link_name][1].image_url|replace:$curBaseCDN:$curCDN}\']);return true;"><img alt="{$navbarSlideShowImages[$itemsA.link_name][1].alt_text}" class="lazy" data-lazy-src="{myntraimage key="topnav_img" src=$navbarSlideShowImages[$itemsA.link_name][1].image_url|replace:$curBaseCDN:$curCDN}" /></a>
	                    	</li> 	
	                    {elseif $navbarStaticImages[$itemsA.link_name]}
	                    	<li class="mk-level2-category-static" {if isset($navbarStaticImages[$itemsA.link_name][2])}style="margin-left:0"{/if}>
	                    		<a href="{$navbarStaticImages[$itemsA.link_name][0].target_url}" onClick="_gaq.push([\'_trackEvent\', \'megamenu\', Myntra.Data.pageName, \'{$navbarStaticImages[$itemsA.link_name][0].image_url|replace:$curBaseCDN:$curCDN}\']);return true;"><img alt="{$navbarStaticImages[$itemsA.link_name][0].alt_text}" class="lazy" data-lazy-src="{myntraimage key="topnav_img" src=$navbarStaticImages[$itemsA.link_name][0].image_url|replace:$curBaseCDN:$curCDN}"/></a>
	                    		<a style="display: none;" href="{$navbarStaticImages[$itemsA.link_name][1].target_url}" onClick="_gaq.push([\'_trackEvent\', \'megamenu\', Myntra.Data.pageName, \'{$navbarStaticImages[$itemsA.link_name][1].image_url|replace:$curBaseCDN:$curCDN}\']);return true;"><img alt="{$navbarStaticImages[$itemsA.link_name][1].alt_text}" class="lazy" data-lazy-src="{myntraimage key="topnav_img" src=$navbarStaticImages[$itemsA.link_name][1].image_url|replace:$curBaseCDN:$curCDN}" /></a>
	                    	</li>
	                    	{if $navbarStaticImages[$itemsA.link_name][2]}
	                    		<li class="mk-level2-category-static">
	                    			<a href="{$navbarStaticImages[$itemsA.link_name][2].target_url}" onClick="_gaq.push([\'_trackEvent\', \'megamenu\', Myntra.Data.pageName, \'{$navbarStaticImages[$itemsA.link_name][2].image_url|replace:$curBaseCDN:$curCDN}\']);return true;"><img alt="{$navbarStaticImages[$itemsA.link_name][2].alt_text}" class="lazy" data-lazy-src="{myntraimage key="topnav_img" src=$navbarStaticImages[$itemsA.link_name][2].image_url|replace:$curBaseCDN:$curCDN}" /></a>
	                    			<a style="display: none;" href="{$navbarStaticImages[$itemsA.link_name][3].target_url}" onClick="_gaq.push([\'_trackEvent\', \'megamenu\', Myntra.Data.pageName, \'{$navbarStaticImages[$itemsA.link_name][3].image_url|replace:$curBaseCDN:$curCDN}\']);return true;"><img alt="{$navbarStaticImages[$itemsA.link_name][3].alt_text}" class="lazy" data-lazy-src="{myntraimage key="topnav_img" src=$navbarStaticImages[$itemsA.link_name][3].image_url|replace:$curBaseCDN:$curCDN}" /></a>
	                    		</li>
	                    	{/if}                    	
	                    {/if}'}
	                
	                    {if $login}
							<script type="text/javascript">
								Myntra.TopNav.level2['index_{$itemsA.display_order}'] = '{include file="string:$level2Categories"|regex_replace:"/[\r\t\n]/":""|escape: 'quotes'}';
							</script>
						{else}
							{include file="string:$level2Categories"}
						{/if}
	            	</ul>
	            </div>
	        {/if}
		{/foreach}
	</nav>
</section>
<div class="mk-clear"></div>
