<div class="mod">
    <div class="mod-wrapper">
        <div class="bd mk-cf">
            <div class="main-content mk-cf">
    <div class="innerShell">
        <div class="topHeading">
          <span class="signup-content"> Already have an account?<span class="blue showLogin"> Login </span></span>
          <span class="mk-hide login-content"> New to Myntra? <span class="blue showSignup"> Create a new account </span></span>
        </div>
        <div class="formHeading">
            <span class="signup-content">Create a new account</span>
            <span class="mk-hide login-content">Login to my account</span>
        </div>
        <div class="facebookLoginArea">
           <button type="button" class="btn normal-btn btn-fb-connect" data-referer="{$mrp_referer}">
               <span class="ico-fb-small"></span> Connect with Facebook
           </button>
	   <div class="fb-facepile">
		<iframe  scrolling="no" frameborder="0" style="border:none;overflow:hidden; width:200px;" allowTransparency="true"></iframe>  
	   </div>
          <div class="err err-fb"></div>                                                                                                            
        </div>
        <div class="loginSeparator">
            <div class="or">OR</div>
            <div class="dotted"></div>
        </div>
        <div class="myntraLoginSignup">
		
        <form method="post" class="frm-signup signup-content" action="{if $secureLoginEnabled}{$https_location}{else}{$http_location}{/if}/mklogin.php">
            <div class="err-top"></div>
		        
		    <input placeholder="Enter email address" type="text" name="email" value=""/>
		    <div class="err err-user"></div>
		    <input type="password" name="password" placeholder="Password" value="" autocomplete="off"/>
		    <div class="err err-pass"></div>
		  {if $phoneNumNotRequired neq "test"}
		    <input type="text" name="mobile" maxlength="10" value="" placeholder="Mobile Number"/>
		    <div class="err err-mobile"></div>
		  {/if}
		    <div class="gender-section">
		        <label for="menu-gender" class="gender_label">Gender</label>
		        <input class="gender-sel" type="radio" name="gender" value="M" ><span class="name">Male</span>
		        <input class="gender-sel" type="radio" name="gender" value="F" ><span class="name">Female</span>
		    </div>
		    <div class="err err-gender"></div>
		    <input type="hidden" name="_token" value="{$USER_TOKEN}" />
            <input type="hidden" name="mode" value="signup" />
            <input type="hidden" name="menu_usertype" value="C" />
            <input type="hidden" name="mrp_referer" id="mrp_referer" value="{$mrp_referer}" />
            <input type="hidden" name="secure_invoked" value="{if isset($is_secure)}1{else}0{/if}" />
            <input type="hidden" name="secure_login" value="{if $secureLoginEnabled}1{else}0{/if}" />
            <input type="hidden" name="new_register" value="{if isset($newRegister)}1{else}0{/if}" />
            <div class="signup-button">
                <button type="submit" class="btn primary-btn btn-signup">CREATE MY ACCOUNT</button>
            </div>
        </form>
		
        <form method="post" class="mk-hide login-content frm-login" action="{if $secureLoginEnabled}{$https_location}{else}{$http_location}{/if}/mklogin.php">
                        <div class="err-top"></div> 
                        <input type="text" name="email" placeholder="Enter email address" value="{$usern}"/>
                        <div class="err err-user"></div>                  
                        <input placeholder="Password" type="password" name="password" value="{$passwd}" autocomplete="off"/>
                        <div class="err err-pass"></div>
                        
                        <!--div class="signin-button">
                	   <button type="submit" class="btn primary-btn btn-signin">Login</button>
                        </div-->
                        <!--Adding captcha start-->
                            {if $showLoginCaptchaFG}
                                {if $showLoginCaptcha}
                                <div id="CaptchaBlock" class="span-12 left captcha-block" style="display: block;">
                                {else}
                                <div id="CaptchaBlock" class="span-12 left captcha-block" style="display: none;">
                                {/if}
                                    <div class="clearfix captcha-details">
                                        <div class="p5 corners black-bg4 span-7 left">
                                            {if $showLoginCaptcha}
                                            <img id="captcha" src="/captcha/captcha.php?id=loginPageCaptcha&rand={rand()}"/>
                                            {else}
                                            <img id="captcha"/>
                                            {/if}
                                        </div>
                                    </div>
                                    <div class="clearfix mt10 captcha-entry">
                                        <label class="login-captcha-change-text">
                                            <a href="javascript:void(0)" onclick="document.getElementById('captcha').src='/captcha/captcha.php?id=loginPageCaptcha&rand='+Math.random();document.getElementById('captcha-form').focus();"id="change-image" >Change text</a>
                                        </label>
                                        <input type="text" name="userinputcaptcha" id="captcha-form" placeholder="Type text as in image above"/>
                                        {*<div id="captcha-loading" class="captcha-loading" style="display:none;"></div>*}
                                    </div>
                                    <div class="err err-login-captcha"></div>
                                </div>
                            {/if}

                            {if $showLoginCaptchaFG}
                                {if $showLoginCaptcha}
                                <div class="submit-buttons" id="captcha-login-submit" style="margin-top: -5px;">
                                {else}
                                <div class="submit-buttons" id="captcha-login-submit">
                                {/if}
                            {else}
                                <div class="submit-buttons">
                            {/if}
                                    <div class="signin-button">
                                        <button type="submit" class="btn primary-btn btn-signin">Login</button>
                                    </div>
                                </div>
                        <!--Adding captcha end-->



                        <div class="btn-forgot-pass blue">Forgot password?</div> 
                        <input type="hidden" name="_token" value="{$USER_TOKEN}" />
                        <input type="hidden" name="mode" value="signin" />
                        <input type="hidden" name="menu_usertype" value="C" />
                        <input type="hidden" name="secure_invoked" value="{if isset($is_secure)}1{else}0{/if}" />
                        <input type="hidden" name="secure_login" value="{if $secureLoginEnabled}1{else}0{/if}" />
                        <input type="hidden" name="new_register" value="{if isset($newRegister)}1{else}0{/if}" />
        </form>

        </div>  
    </div>
    <div class="outter-content">
    <div class="signup-content signup-values">
        <div class="{$imageFlag}">
            <div class="top-heading">
                Create an account to
            </div> 
            <ul>
                {foreach from=$signUpPoints key=index item=value}
                {assign var='curIndex' value=$index+1} 
                <li class="{if $signUpPoints|@count neq $curIndex}engravedUnderLine{/if}">
                    <div class="{$value['imageClass']}"></div> 
                    <div class="heading">{$value["heading"]}</div>
                    <div class="subtext">{$value["subtext"]}</div>
                </li>
                {if $signUpPoints|@count neq $curIndex}<li class="whiteLine"></li>     {/if}
                {/foreach}
            </ul>
        </div>
    </div>
    <div class="login-content login-values mk-hide">
        <div class="top-heading engravedUnderLine">
            Login to
        </div>
	<div style="margin-bottom:10px;" class="whiteLine"></div>

        <ul>
            {foreach from=$loginPoints item=value}
            <li><div class="tick"></div>{$value}</li>
            {/foreach}
        </ul>

    </div>
   </div>
    </div>
		<div class="forgot-pass-msg" style="display:none">
			<h1>NO WORRIES!</h1>
			<p class="ico-email">&nbsp;</p>
			<p class="reset"><span class="tick-ico"> &nbsp; &nbsp; &nbsp; </span> An email with a link to reset your password has been sent to :</p>
			<p class="email"></p>
			<p class="gray">Please check your email and click the password reset link</p>
			<button type="button" class="btn btn-forgot-pass-ok" >OK</button>
		</div>
     </div>
   </div>
</div>
<input type="hidden" name="loginbg" value="{if isset($is_secure)}{$loginbg|replace :'http://myntra.myntassets.com':'https://d6kkp3rk1o2kk.cloudfront.net'}{else}{$loginbg}{/if}" class="mk-login-bg-path" />


