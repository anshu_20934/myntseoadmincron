<script type="text/javascript">
	Myntra.Data.enableMostPopular = "{$enableMostPopular}";
	{if $mostPopularVariant == 'control' || !$enableMostPopular}
		Myntra.Data.mostPopular = 'collapsed';
	{else}
		Myntra.Data.mostPopular = 'expanded';
	{/if} 
</script>
{if $enableMostPopular}
	<div class="mk-most-popular mk-hide mk-carousel-four mk-carousel-five rs-carousel mk-inline-size-selector">
		<div class="top-seller-pagination" class="" style="float:left; margin-right: 10px; margin-top: 5px;"></div>
		<div class="category"></div>		
		<div class="rs-carousel-mask">
			<ul class="rs-carousel-runner mk-most-popular-ul" >
			</ul>
		</div>
	</div>
{/if}