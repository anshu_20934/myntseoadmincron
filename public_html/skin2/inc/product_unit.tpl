{foreach name=widgetdataloop item=widgetdata from=$products}
    {assign var=mod_val value=$widgetdata@index%4}    
    <li class="mk-product mk-product-style-details {$mod_val} {if $mod_val eq '3'}mk-last{/if} {if $mod_val eq '0'}mk-first{/if}" data-id="prod_{$widgetdata.id}">
    	{if $quicklookenabled}
			<span class="quick-look" data-widget="search_results" data-styleid="{$widgetdata.styleid}" data-href="/{$widgetdata.landingpageurl}"></span>
		{/if}

		<a style="display:block;" class="clearfix" href="{if $widgetdata.landingpageurl}/{$widgetdata.landingpageurl}{/if}{if $nav_id}?nav_id={$nav_id}{/if}">
			{if $widgetdata.visual_tag}
                 {assign var="visualtag" value=":"|explode:$widgetdata.visual_tag}
                 <div class="mk-visual-tag mk-visual-tag-{$visualtag[1]|lower}"><div>{$visualtag[0]|lower}</div></div>
            {/if}
			<div class="mk-prod-img">
				<img {if $smarty.foreach.widgetdataloop.index gt 3}class="jquery-lazyload" original="{myntraimage key="style_180_240" src=$widgetdata.searchimagepath}" {/if} src="{if $smarty.foreach.widgetdataloop.index gt 3}{$cdn_base}/skin2/images/loader_180x240.gif{else}{myntraimage key="style_180_240" src=$widgetdata.searchimagepath}{/if}" alt="{$widgetdata.product}">
			</div>
			<div class="mk-prod-info">
				{if $widgetdata.generic_type neq 'design'}
			        <div class="availability" data-sizes="{if $widgetdata.sizes neq '' && $widgetdata.sizes neq ' '}Sizes: {$widgetdata.sizes|replace:",":", "}{else}SOLD OUT{/if}">
			        	<div class="tooltip-arrow"><div class="tooltip-arrow-inner"></div></div>
			        	<div class="tooltip-content"></div>
			        </div>
		        {/if}
	            <span class="mk-prod-name"><span class="mk-prod-brand-name">{$widgetdata.brands_filter_facet}</span> {$widgetdata.product|strip|truncate:53}</span>
	            <span class="mk-prod-price red">{$rupeesymbol}{if $widgetdata.discount > 0}{$widgetdata.discounted_price|number_format:0:".":","} <span class="strike gray">{$widgetdata.price|number_format:0:".":","}</span>{else}{$widgetdata.price|number_format:0:".":","}{/if}</span>
	            {if $widgetdata.discount_label}
		            <span class="mk-discount red">
		                {include file="string:{$widgetdata.discount_label}"}  
		            </span>
		        {/if}
	        </div>
		</a>

		{if $colourOptionsVariant == 'test' && $widgetdata.colourVariantsData|count gt 0}
			<div class="colours-lbl">+{$widgetdata.colourVariantsData|count} Colour{if $widgetdata.colourVariantsData|count gt 1}s{/if}</div>
		{/if}

		{if $colourOptionsVariant == 'test' && $widgetdata.colourVariantsData|count gt 0}
			<div class="mk-colour mk-hide">
	        	<div class="tooltip-arrow"><div class="tooltip-arrow-inner"></div></div>
	        	<div class="tooltip-content rs-carousel">
				    <ul class="rs-carousel-runner mk-cf">
				    	{foreach from=$widgetdata.colourVariantsData key=key item=val}
					        <li class="rs-carousel-item">
					            <a href="/{$val.landing_page_url}{if $nav_id}?nav_id={$nav_id}{/if}" data-id="{$key}">
						            {assign "title" "{$val.global_attr_base_colour}{if $val.global_attr_colour1 neq 'NA' && $val.global_attr_colour1 neq ''} / {$val.global_attr_colour1}{/if}"}
					            	<img class="jquery-lazyload" src="{$cdn_base}/skin2/images/loader_180x240.gif" original="{myntraimage key='style_48_64' src=$val.search_image|replace:'_images_180_240':'_images_48_64'|replace:'/style_search_image/':'/properties/'}" height="52" width="39" title="{$title}" alt="{$title}" />
					            </a>
					        </li>
				        {/foreach}
				    </ul>
				    <span class="icon-prev mk-hide"></span>
				    <span class="icon-next mk-hide"></span>
				</div>
			</div>
		{/if}
		
	</li>
{/foreach}
