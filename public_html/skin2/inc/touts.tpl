{if $storyboards@count}
	<ul class="mk-feature-page-touts mk-clear">
		{assign var=i value=1}
		{foreach name=storyboardItem key=key item=storyboardItem from=$storyboards}
			<li {if $i mod 5 == 0}class="mk-last"{/if}>
				<a href="{$http_location}/{$storyboardItem.target_url}" title="{$storyboardItem.title}" onClick="_gaq.push(['_trackEvent', 'story_board', '{$storyboardItem.title}', '{$storyboardItem.target_url}']);return true;">
					<img src='{$cdn_base}/skin2/images/spacer.gif' data-lazy-src="{myntraimage key="story_board" src=$storyboardItem.image_url}" alt="{$storyboardItem.alt_text}" class="lazy"/>
					<h3>{$storyboardItem.title} </h3>
					<p>{$storyboardItem.description} </p> 
				</a>
			</li>
			{assign var=i value=$i+1}
		{/foreach}
	</ul> <!-- End .category-page-touts --> 
{/if}