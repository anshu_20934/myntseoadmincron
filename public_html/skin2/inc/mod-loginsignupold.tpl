{assign var="referermail" value=$referer|replace:' ':''}

{*assign var="signUpMsg" value="And get Rs 1000 worth of coupons" *}
{*assign var="connectFbMsg" value="AND GET RS. 1,000 WORTH OF COUPONS THE FIRST TIME YOU CONNECT WITH FACEBOOK" *}

<div class="mod {if $referermail}referral{/if}">
	<div class="hd">
		{if $referermail}
			<div class="h2">Mynt Club - Myntra Rewards & Loyalty Program</div>
			{*<div class="h4">Welcome to India's largest Online Fashion Store</div>*}
		{else}
			{*<div class="h2">Welcome to India's largest Online Fashion Store</div>*}
		{/if}
	</div>
	<div class="bd mk-cf">
		<div class="main-content mk-cf">
			{if $referermail}
				<div class="referermail">
					Congratulations!<br />
					You have been referred by
					<div class="mail">{$referermail}</div>
					To be a part of Mynt Club.
				</div>
			{else}
				{if $newSignupAB == 'test'}
				<div class="connect-wth-facebook-new-signup">
					<button type="button" class="btn btn-new-signup normal-btn btn-fb-connect" data-referer="{$mrp_referer}">
				{else}
				<div class="connect-with-facebook">
					<button type="button" class="btn normal-btn btn-fb-connect" data-referer="{$mrp_referer}">
				{/if}
						<span class="ico-fb-small"></span> Connect with Facebook
					</button>
					<div>{$connectFbMsg}</div>
	
					{if $newSignupAB == 'test'}
					<div class="err-new-signup err-fb"></div>
					{else}
					<div class="err err-fb"></div>
					{/if}
				
				</div>
			{/if}
			<div class="left-panel">
				<form method="post" class="frm-signup" action="{if $secureLoginEnabled}{$https_location}{else}{$http_location}{/if}/mklogin.php">
					<div class="h3">
						{if $referermail} Sign Up through Email 
						{else} New to Myntra? Sign Up 
						{/if}
					</div>
					<div class="h4">{$signUpMsg}</div>

					{if $newSignupAB == 'test'}
					<div class="err-top-new-signup"></div>
					{else}
					<div class="err-top"></div>
					{/if}

					{if $newSignupAB == 'test'}
					
						<label class="new-signup-label">EMAIL ADDRESS*</label>
						<input type="text" name="email" class="new-signup" value="" id="signup-email-label">
						<div class="err err-new-signup err-user"></div>					
					
					{else}

						<label>EMAIL ADDRESS</label>
						<input type="text" name="email" value=""/>
						<div class="err err-user"></div>
					{/if}
					
					{if $newSignupAB == 'test'}
						
						<label class="new-signup-label">PASSWORD*</label>
						<input type="password" name="password" autocomplete="off" class="new-signup" value="" id="signup-pass-label">
						<div class="err err-new-signup err-pass"></div>
					{else}
						
						<label>PASSWORD</label>
						<input type="password" name="password" autocomplete="off" value="" />
						<div class="err err-pass"></div>
					{/if}
					
					{if $newSignupAB == 'test'}
					
						<label class="new-signup-label">YOUR MOBILE NUMBER* (10 DIGIT)</label>
						<input type="text" name="mobile" class="new-signup" maxlength="10" value="" id="signup-mobile-label" >
						<div class="err err-new-signup err-mobile"></div>
					{else}
					
						<label>YOUR MOBILE NUMBER (10 DIGIT)</label>
						<input type="text" name="mobile" maxlength="10" value="" />
						<div class="err err-mobile"></div>
					{/if}

					{if $newSignupAB == 'test' && $display_name }
											
						{if $is_name_optional}
							<label class="new-signup-label">NAME (OPTIONAL)</label>
							<input type="text" class="new-signup" name="name" value="" id="signup-name-label" >
	           			{else}
	           				<label class="new-signup-label">NAME*</label>
							<input type="text" class="new-signup" name="name" value="" id="signup-name-label" >
	           			{/if}
	           			
	           			{if $is_name_optional}
	           				<div class="err err-new-signup"></div>
	           			{else}
	           				<div class="err err-new-signup err-name"></div>
	           			{/if}
           			{/if}


					
					{if $newSignupAB == 'test'  && $display_birth_year}
						
						{if $showgender eq 'gender'}
						<div class="mk-f-left-signup mk-custom-drop-down row">
	                    	<select name="gender">
	                    		<option for="menu_gender" selected="false" class="gender_label" value="">GENDER*</option>
	                    		<option value="M">MALE</option>
	                    		<option value="F">FEMALE</option>
	                    	</select>	
	                  	</div>
	                  	{/if}

	                  	<div class="mk-f-right-signup mk-custom-drop-down prof-year row">
	                  	<select name="birth-year" class="sel-year">
	                  			{if $is_birth_year_optional}
	                   			<option value="0000">BIRTH YEAR</option>
	                   			{else}
	                   			<option value="0000">BIRTH YEAR*</option>
	                   			{/if}
	                	  		{section name=year loop={$birth_year_range_high + 1} max = $birth_year_range step=-1}
	               	    		<option value="{$smarty.section.year.index}" {if $userProfileData.DOB|date_format:'%Y' eq $smarty.section.year.index}selected{/if}>{$smarty.section.year.index}</option>
	                   			{/section}
							</select>
						</div>
					
						<div class = "err"></div>
						<div class="err err-gender err-left"></div>

						{if $is_birth_year_optional}
						<div class=" err err-right optional-right">OPTIONAL</div>
						{else}
							<div class=" err err-birthyear err-right"></div>
						{/if}

					{else}
						{if $showgender eq 'gender'}
							<div class="gender-section">
								<label for="menu-gender" class="gender_label">GENDER</label>
								<input class="gender-sel" type="radio" name="gender" value="M" ><span class="name">Male</span>
		                    	<input class="gender-sel" type="radio" name="gender" value="F" ><span class="name">Female</span>
		                    	<div class="err err-gender"></div>
                  			</div>
                  		{/if}

					{/if}

					<button type="submit" class="btn primary-btn btn-signup">Sign Up</button>

					<input type="hidden" name="_token" value="{$USER_TOKEN}" />
					<input type="hidden" name="mode" value="signup" />
					<input type="hidden" name="menu_usertype" value="C" />
					<input type="hidden" name="mrp_referer" id="mrp_referer" value="{$mrp_referer}" />
					<input type="hidden" name="secure_invoked" value="{if isset($is_secure)}1{else}0{/if}" />
					<input type="hidden" name="secure_login" value="{if $secureLoginEnabled}1{else}0{/if}" />
					<input type="hidden" name="new_register" value="{if isset($newRegister)}1{else}0{/if}" />
				</form>
			</div>
			<div class="right-panel">
				{if $referermail}
					<form class="frm-login">
						<div class="h3">Sign Up through Facebook</div>
						<div class="h4">And Get <span class="rupees discount">{$rupeesymbol}{$mrp_fbCouponValue}</span> worth of Mynt Credits</div>
						<div class="connect-with-facebook">
							<button type="button" class="btn normal-btn btn-fb-connect" data-referer="{$mrp_referer}">
								<span class="ico-fb-small"></span> Connect with Facebook
							</button>
							<div class="err err-fb"></div>
						</div>
					</form>
				{else}
					<form method="post" class="frm-login" action="{if $secureLoginEnabled}{$https_location}{else}{$http_location}{/if}/mklogin.php">
						<div class="h3">Existing User? Login</div>
						<div class="h4">View your Bag / Wishlist & Track your Orders</div>
						
						{if $newSignupAB == 'test'}
						<div class="err-top-new-signup"></div>
						{else}
						<div class="err-top"></div>
						{/if}

						{if $newSignupAB == 'test'}
						
							<label class="new-signup-label">EMAIL ADDRESS</label>
							<input type="text" name="email" class="new-signup" value="{$usern}" id="signup-email-label">
							<div class="err err-new-signup err-user"></div>					
						
						{else}

							<label>EMAIL ADDRESS</label>
							<input type="text" name="email" value="{$usern}"/>
							<div class="err err-user"></div>
						{/if}
						
						{if $newSignupAB == 'test'}
							
							<label class="new-signup-label">PASSWORD</label>
							<input type="password" name="password" class="new-signup" autocomplete="off" value="{$passwd}" id="signup-pass-label">
							<div class="err err-new-signup err-pass"></div>
						{else}
							
							<label>PASSWORD</label>
							<input type="password" name="password" autocomplete="off" value="{$passwd}" />
							<div class="err err-pass"></div>
						{/if}
	
						<button type="submit" class="btn primary-btn btn-signin">Login</button>
						<button type="button" class="btn link-btn btn-forgot-pass">Forgot your password?</button>
						<input type="hidden" name="_token" value="{$USER_TOKEN}" />
						<input type="hidden" name="mode" value="signin" />
						<input type="hidden" name="menu_usertype" value="C" />
						<input type="hidden" name="secure_invoked" value="{if isset($is_secure)}1{else}0{/if}" />
						<input type="hidden" name="secure_login" value="{if $secureLoginEnabled}1{else}0{/if}" />
						<input type="hidden" name="new_register" value="{if isset($newRegister)}1{else}0{/if}" />
					</form>
				{/if}
			</div>
		</div>
		<div class="forgot-pass-msg">
			<p>An email with a link to reset your password has been sent to :</p>
			<p class="email"></p>
			<p>Hope to see you back soon!</p>
			<button type="button" class="btn normal-btn btn-forgot-pass-ok">Continue Shopping</button>
		</div>
	</div>
</div>
<!-- Login background coming from PHP now -->
<input type="hidden" name="loginbg" value="{if isset($is_secure)}{$loginbg|replace :'http://myntra.myntassets.com':'https://d6kkp3rk1o2kk.cloudfront.net'}{else}{$loginbg}{/if}" class="mk-login-bg-path" />

