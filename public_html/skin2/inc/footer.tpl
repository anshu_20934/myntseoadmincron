<footer class="mk-site-ft mk-cf">
<div class="mynt-footer">
	<hr class="main" />
    <div class="footer-content">
    	{if $megaFooterEnabled}
        <div class="mynt-links-block">
            <div class="mynt-footer-links">
                <ul>
                    <li><strong>SHOP</strong></li>
                    {foreach key=indexA item=itemsA from=$topNavigationWidget name=topNavigationWidgetLoop}
						<li><a href="{$itemsA.link_url}?nav_id={$itemsA.id}" {if $itemsA.link_name|lower eq "style blog"}rel="nofollow" target="_blank"{/if}>{$itemsA.link_name}</a></li>
					{/foreach}
					{if $giftingEnabled}
					<li><a href="{$http_location}/giftcards">Gift Cards</a></li>
					{/if}
					<li><a href="{$http_location}/sales">Sale</a></li>

                    <li><strong class="mt20">ACCOUNT</strong></li>
                    {if not $login}
                    	<li><a class="btn-login foot-login">Login / Sign Up</a></li>
                    {else}
	                    <li><a href="{$http_location}/mymyntra.php" onClick="_gaq.push(['_trackEvent', 'mymyntra', 'footer', 'click']);return true;">My Myntra</a></li>
	                    <li><a href="{$http_location}/mymyntra.php?view=myprofile#more-info" onClick="_gaq.push(['_trackEvent', 'myprofile', 'footer', 'click']);return true;">My Profile</a></li>
                    {/if}
                    <li><a href="{$http_location}/mkmycart.php" onClick="_gaq.push(['_trackEvent', 'bag', 'footer', 'click']);return true;">My Shopping Bag</a></li>
                    <li><a href="{$http_location}/mkmycart.php#wishlist" onClick="_gaq.push(['_trackEvent', 'wishlist', 'footer', 'click']);return true;">My Wishlist</a></li>
				</ul>
                <ul>
                    <li><strong class="mt20">CUSTOMER SERVICE</strong></li>
                    <li><a href="{$http_location}/faqs#top" onClick="_gaq.push(['_trackEvent', 'footer', 'faqs', 'click']);return true;">FAQs</a></li>
                    <li><a href="{$http_location}/faqs#shipping" onClick="_gaq.push(['_trackEvent', 'footer', 'shippingpolicy', 'click']);return true;">Shipping</a></li>
                    <li><a href="{$http_location}/faqs#cancel" onClick="_gaq.push(['_trackEvent', 'footer', 'cancelpolicy', 'click']);return true;">Cancellation</a></li>
                    <li><a href="{$http_location}/faqs#returns" onClick="_gaq.push(['_trackEvent', 'footer', 'returnspolicy', 'click']);return true;">Returns</a></li>
                    <li><a href="{$http_location}/privacypolicy#top" onClick="_gaq.push(['_trackEvent', 'footer', 'privacypolicy', 'click']);return true;">Privacy</a></li>
                    <li><a href="{$http_location}/termsofuse#top" onClick="_gaq.push(['_trackEvent', 'footer', 'termsofuse', 'click']);return true;">Terms of Use</a></li>

                    <li><strong>MYNTRA</strong></li>
                    <li><a href="{$http_location}/aboutus#top" onClick="_gaq.push(['_trackEvent', 'footer', 'aboutus', 'click']);return true;">About Us</a></li>
                    <li><a href="{$http_location}/careers#top" onClick="_gaq.push(['_trackEvent', 'footer', 'careers', 'click']);return true;">Work With Us</a></li>
                    <li><a href="{$http_location}/contactus#top" onClick="_gaq.push(['_trackEvent', 'footer', 'contactus', 'click']);return true;">Contact Us</a></li>
                    <li><a href="http://blog.myntra.com/" onClick="_gaq.push(['_trackEvent', 'footer', 'corporateblog', 'click']);return true;" target="_blank">Corporate Blog</a></li>
                </ul>
            </div>
            <div class="mynt-footer-promos">
                <div class="promo-block">
                    <div class="promo ">
                       {$totalNumStyles} STYLES<br>
                        {$totalNumBrands} BRANDS
                        <span class="promo-small-text">AND COUNTING!</span>
                    </div>
                    <div class="promo-leaf" >
                        We have the largest collection of the latest fashion and lifestyle products in India!
                    </div>
                </div>

                <div class="promo-block">
                    <div class="pipe">
                        <div class="certified-icon">&nbsp;</div>
                    </div>
                    <div class="promo-leaf">
                        All our products are brand new and 100% original.
                    </div>
                </div>
				{*<div class="promo-block">
                    <div class="pipe promo-link">
                        <a href="http://stylemynt.com" target="_blank" onClick="_gaq.push(['_trackEvent', 'footer', 'stylemynt', 'click']);return true;">STYLEMYNT</a>
                    </div>
                    <div class="promo-leaf" style="padding-top: 5px;">
                        Visit our blog for the latest global and local fashion news, views, trends, styles, and tips.
                    </div>
                </div>*}
            </div>
        </div>
        <hr />
    	{/if}
        <div class="support-block">
            	{if $megaFooterEnabled}
            		<div class="testimonials">
			            <div class="foot-left-arrow" id="prev">&nbsp;</div>
			            <div class="testimonials-slide left">
			                <strong>A DELIGHTED CUSTOMER SAYS...</strong>
			                {include file="inc/footer-testimonials.tpl"}
			            </div>
			            <div class="foot-right-arrow" id="next">&nbsp;</div>
		            </div>
	            {/if}
        	  	<div class="contact-block">
        	        {if !$contactUsLink || !$megaFooterEnabled}
                        NEED HELP? GOT FEEDBACK?<br />
                        <span>{$customerSupportCall}</span>
                        <a href="mailto:support@myntra.com" class="maillink" onClick="_gaq.push(['_trackEvent', 'footer', 'mailink', 'click']);return true;">support@myntra.com</a>
                    {else}
                        {$contactUsLink}
                    {/if}
                    <div>{$community}</div>
	            	{if $megaFooterEnabled}
						Social
	                    <div class="social-buttons">
	                        <a href="http://www.facebook.com/myntra" target="_blank" class="fb" onClick="_gaq.push(['_trackEvent', 'footer', 'fb-myntra', 'click']);return true;">&nbsp;</a>
	                        <a href="http://twitter.com/myntra" target="_blank" class="tw" onClick="_gaq.push(['_trackEvent', 'footer', 'twitter-myntra', 'click']);return true;">&nbsp;</a>
	                        <a href="http://pinterest.com/myntra/" target="_blank" class="pt" onClick="_gaq.push(['_trackEvent', 'footer', 'pinterest-myntra', 'click']);return true;">&nbsp;</a>
	                        <a href="http://www.youtube.com/user/myntradotcom" target="_blank" class="yt" onClick="_gaq.push(['_trackEvent', 'footer', 'yt-myntra', 'click']);return true;">&nbsp;</a>
	                        <a href="https://plus.google.com/109282217040005996404/posts" target="_blank" class="gp" onClick="_gaq.push(['_trackEvent', 'footer', 'gp-myntra', 'click']);return true;">&nbsp;</a>
	                    </div>
                	{/if}
                 </div>
        </div>
    	{if $megaFooterEnabled}
	        <div class="social-block">
    	        <strong>DO YOU<span class="love-icon">&nbsp;</span>MYNTRA?</strong>
	            <div class="love-myntra">
					<div class="fb-like-button"></div>
	            </div>
	            <div class="social-links-block">
					<div class="tw-tweet-button inline-block"></div>
					<div class="tw-follow-button inline-block"></div>
					<div class="gp-plusone-button"></div>
                 </div>
	        </div>
    	{/if}
		{if $megaFooterEnabled}
        {*<div class="news-block hide">
            <div class="mynt-news">
                <strong>SIGN UP FOR MYNTRA NEWSLETTER</strong>
                <input type="text" value="Your Email Address" name="yourmail" class="newsletter">
                <button class="action-btn btn-signin" type="submit">SUBMIT</button>
                *}{*<button class="action-btn btn-signin" style="width:85px;margin-left:8px;" type="submit">WOMEN</button>*}{*
            </div>
            <div class="style-mynt">
                <strong>VISIT <a href="http://stylemynt.com" target="_blank">STYLEMYNT</a>, OUR STYLE BLOG...</strong>
                <span class="style-text">For the latest global and local fashion news, views, trends, styles, and tips.</span>
            </div>
        </div>*}
        {if $mostPopularWidget}
        <div class="mynt-tags">
        {foreach name=tagtype item=fixedorDynamic from=$mostPopularWidget}
	        	{foreach name=uniquetag item=tag from=$fixedorDynamic}
	        	    {if $smarty.foreach.uniquetag.last}
	        	    <a href="{$http_location}/{$tag.link_url}" onClick="_gaq.push(['_trackEvent', 'footer', '{$tag.link_url}', 'click']);return true;">{$tag.link_name}</a>
	        	    {else}
	            	<a href="{$http_location}/{$tag.link_url}" onClick="_gaq.push(['_trackEvent', 'footer', '{$tag.link_url}', 'click']);return true;">{$tag.link_name}</a>,
	            	 {/if}
	            {/foreach}
        {/foreach}
        </div>
        {/if}

        <div class="mynt-text">
            <div class="mynt-foot-title">ABOUT MYNTRA</div>
            Myntra.com is India’s largest online fashion and lifestyle store for men, women, and kids.<br>
            Shop online from the latest collections of apparel, footwear and accessories, featuring the best brands.<br>
            We are committed to delivering the best online shopping experience imaginable.<br>
        </div>
		{/if}

        <div class="payment-block {if $emiEnabled}emi{/if}">
             <div class="payment">
                <div class="payment-title">PAY SECURELY</div>
                <div class="payment-icons"></div>
            </div>
           <div class="poweredby">
                <div class="payment-title">POWERED BY</div>
                <div class="powered-icons"></div>
           </div>

        </div>

        <div class="mynt-copy-right">
            &copy; myntra.com / All rights reserved
        </div>
        {if $showMobileSiteLink}
            <div><a href="#" onclick="javascript:document.location.href='{$http_location}/udvo?redirecturl='+document.location">View Mobile Site</a></div>
        {/if}
    </div>
</div>

<div id="fb-root"></div>

<!-- @Footer-->


</footer>
