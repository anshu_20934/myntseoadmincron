<div class="mk-product-filters">			
	<ul class="mk-product-sort">  <!-- Note: different option? -->
		<li class="mk-title"> Sort:</li>
		<li class="mk-sort-popular">&nbsp;&nbsp;<a class="popular-sort-filter {if $sort_param_in_url eq 'POPULAR' || $sort_param_in_url eq ''}mk-sort-selected{/if}" onclick="return Myntra.Search.Utils.setSortBy('POPULAR', this);">Popularity</a>&nbsp;&nbsp;/</li>
		<li class="mk-sort-discount">&nbsp;&nbsp;<a class="discount-sort-filter {if $sort_param_in_url eq 'DISCOUNT'}mk-sort-selected{/if}" onclick="return Myntra.Search.Utils.setSortBy('DISCOUNT', this);">Discount</a>&nbsp;&nbsp;/</li>
		<li class="mk-sort-whats-new">&nbsp;&nbsp;<a class="recency-sort-filter {if $sort_param_in_url eq 'RECENCY'}mk-sort-selected{/if}" onclick="return Myntra.Search.Utils.setSortBy('RECENCY', this);">What's New</a>&nbsp;&nbsp;/</li>
		<li class="mk-sort-price">&nbsp;&nbsp;<a class="price-sort-filter {if $sort_param_in_url eq 'PRICEA' || $sort_param_in_url eq 'PRICED'}mk-sort-selected{/if}" onclick="return Myntra.Search.Utils.setSortBy('PRICE', this);">Price <span class="mk-price-asc" {if $sort_param_in_url eq 'PRICEA'}style="display:inline-block"{else}style="display:none;"{/if}></span><span class="mk-price-desc" {if $sort_param_in_url eq 'PRICED'}style="display:inline-block"{else}style="display:none;"{/if}></span></a></li>
	</ul> <!-- End .product-sort -->
</div>
