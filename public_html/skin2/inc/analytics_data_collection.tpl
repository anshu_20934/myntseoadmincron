{*if $analyticsStatus}
<script language="JavaScript" type="text/javascript">
var omnistat = {if $analytics_json}{$analytics_json}{else}''{/if};
s.pageName=omnistat.pageName;
s.server="";
s.channel=omnistat.channel;
s.pageType="";

/* Conversion Variables */
s.campaign=omnistat.campaign;
s.state="";
s.zip="";
s.events=omnistat.events;
s.products=omnistat.products;
s.purchaseID=omnistat.purchaseID;

for (var i in omnistat) {
	if (/(prop|eVar)\d+/.test(i)) {
		s[i] = omnistat[i];
	}
}

s.eVar1="";
s.eVar2="";

// Setting s to s_analytics global variable as s is getting unset by some peice of code b/w here 
// and afterOnLoad function call in custom.js
var s_analytics = s;
var s_analytics_firesync = {if $fireSyncOmnitureCall}true{else}false{/if};
if(s_analytics_firesync) {
	s_analytics.t();
}
</script>
<!--<noscript><img src="http://myntra.d1.sc.omtrdc.net/b/ss/{$analytics_account}/1/H.24.2--NS/0"
height="1" width="1" border="0" alt="" /></noscript>-->
{/if*}
