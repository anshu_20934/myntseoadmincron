{*assign var="loginSignupV2ABTest" value="test" *}
{if $loginSignupV2ABTest == 'test'}
	<div id="lb-login" data-signupsplashpageload="{$SignUpSplashPageLoad}" class="lightbox loginbox loginsignupV2">
		{include file="inc/mod-loginsignupv2.tpl"}
{else}
    {if $loginSignupABTest == 'test'}
	    <div id="lb-login" data-signupsplashpageload="{$SignUpSplashPageLoad}" class="lightbox loginbox loginsignup">
	        {include file="inc/mod-loginsignupnew.tpl"}
    {else}
	    <div id="lb-login" data-signupsplashpageload="{$SignUpSplashPageLoad}" class="lightbox loginbox loginsignupold">
	        {include file="inc/mod-loginsignupold.tpl"}
    {/if}
{/if}
</div>

