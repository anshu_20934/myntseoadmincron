<div class="mk-festival-header">
	{if $festHeaderImage_Center}
		<a href="{$festHeaderImageURL}" class="mk-festival-header-center">
			<img class="lazy" data-lazy-src="{$festHeaderImage_Center}" /> <!-- http://myntra.myntassets.com/skin2/images/MyntraShoppingFestival_topbanner_center.png -->
		</a>
	{/if}
	{if $festHeaderImage_Left}
		<a href="{$festHeaderImageURL}" class="mk-festival-header-left">
			<img class="lazy" data-lazy-src="{$festHeaderImage_Left}" />
		</a>
	{/if}
	{if $festHeaderImage_Right}
		<a href="{$festHeaderImageURL}" class="mk-festival-header-right">
			<img class="lazy" data-lazy-src="{$festHeaderImage_Right}" />
		</a>
	{/if}
</div>