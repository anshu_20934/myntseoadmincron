<form method="post" class="addr-form">
    <input type="hidden" name="_token" value="{$USER_TOKEN}">
    <input type="hidden" name="id" value="{$account.neftAccountId}">    
    
    <div class="row">
        <label>Your Name (As per bank records)<strong>*</strong></label>
        <input type="text" name="account-name" class="name" value="{$account.accountName}">
        <p class="err err-account-name"></p>        
    </div>

    <div class="row">
        <label>Name of the Bank <strong>*</strong></label>
        <input type="text" name="bank-name" class="name" value="{$account.bankName}">
        <p class="err err-bank-name"></p>        
    </div>

    <div class="row">
        <label>Name of the Branch<strong>*</strong></label>
        <input type="text" name="branch" class="name" value="{$account.branch}">
        <p class="err err-branch"></p>        
    </div>

    <div class="row">
        <label>Bank IFSC Code <strong>*</strong></label>
        <input type="text" name="ifsc" class="name" maxlength="11" value="{$account.ifscCode}">
        <p class="err err-ifsc"></p>
    </div>

    <div class="row">
        <label>Your Account Number <strong>*</strong></label>        
        <input type="text" name="account-number" class="name" maxlength="20" value="{$account.accountNumber}">
        <p class="err err-account-number"></p>
    </div>

    <div class="row">
        <label>Re-enter Account Number <strong>*</strong></label>        
        <input type="password" name="re-account-number" class="name" maxlength="20" value="{$account.accountNumber}">
        <p class="err err-re-account-number"></p>
    </div>

    <div class="row">
        <label>Your Account Type <strong>*</strong></label>
        <select name="account-type" class="state">      
            <option value="CURRENT">Current</option>
            <option value="SAVINGS" selected>Savings</option>
        </select>
        <p class="err err-account-type"></p>
    </div>
</form>