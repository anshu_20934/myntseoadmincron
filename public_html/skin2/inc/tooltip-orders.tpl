<?php 
////////////// Myntra ///////////////
// ToolTip - Orders 
// CSS in account-pages.css
/////////////////////////////////////
?> 

<div class="mk-order-tip mk-hide">
	<h4> Order No. 1234567 </h4> 
	<ul>
		<li>Lanvin Leather and Suede Sneaker</li>
		<li>Maison Sude Low-Top Sneakers</li>
		<li class="mk-last">Jimmy Choo Suede Sneakers</li>
	</ul>
	<h4> Order Total: RS.12896 </h4>

	<div class="mk-tip-arrow"><img src="/skin2/images/border-tooltip.png" /></div>
</div>