<div class="mk-free-wrapper mk-hide">
	<div class="mk-free-modal">
		<span class="mk-modal-close"> X </span>
		<div class="mk-free-item">
			<div class="mk-product-shop">
				<div class="mk-product-media mk-f-left">
					<div class="mk-product-large-image">
						<img src="/skin2/images/fpo/free-item-large.jpg" />
					</div>

					<div class="mk-more-views">
						<div class="mk-wrap">
							<div class="mk-sub-wrap">
								<img src="/skin2/images/fpo/free-item-1.jpg" alt="/skin2/images/fpo/fpo-large-product.jpg" title="/skin2/images/fpo/fpo-zoom.jpg"/>
								<img src="/skin2/images/fpo/free-item-2.jpg" alt="/skin2/images/fpo/fpo-large-view2.jpg" title="/skin2/images/fpo/fpo-large-view2.jpg"/>
								<img src="/skin2/images/fpo/free-item-3.jpg" alt="/skin2/images/fpo/fpo-large-view3.jpg" title="/skin2/images/fpo/fpo-large-view3.jpg"/>
							</div><!--End .sub-wrap -->
						</div>
					</div>
				<div class="mk-product-guide mk-f-right">
					<h5>Product Code: 173506</h5> <!-- Product Code -->
					<h1>APC Striped Sweater</h1>  <!-- Product Name -->
					<p class="mk-discount"> MRP RS 11995.00 (10% Off) </p> <!-- Product Discount --> 
					<h3> RS 10796 </h3>  <!-- Product Price -->

					<ul class="mk-size">
						<li><a href="">S</a></li>
						<li><a href="" class="mk-active">M</a></li>
						<li><a href="">L</a></li>
						<li><a href="" class="mk-last">XL</a></li>
					</ul>

					<div class="mk-size-guides">
						<a href="">Size Guide</a>
						<a href="">Size Request</a>
					</div> 

					<div class="mk-color-selector"> <!-- Color Selector --> 
						<a href class="mk-active"> <img src="/skin2/images/fpo/fpo-black.png" alt="Black &amp; White"/></a> 
						<a href> <img src="/skin2/images/fpo/fpo-blue.png" alt="Blue &amp; White"/></a>
						<a href> <img src="/skin2/images/fpo/fpo-grey.png" alt="Grey &amp; White"/></a>

						<h6> Black & White </h6> 
					</div>

					<button class="mk-add-to-cart"> Add to Bag </button> 
					<button class="mk-save-for-later"> Save in Wishlist </button>
				
				 	<div class="mk-quicklook-reviews">

				 		<img src="/skin2/images/btn-starred.png" alt="" />
				 		<img src="/skin2/images/btn-starred.png" alt="" />
				 		<img src="/skin2/images/btn-starred.png" alt="" />
				 		<img src="/skin2/images/btn-stars.png" alt="" />
				 		<img src="/skin2/images/btn-stars.png" alt="" />

						<h2> 32 Reviews </h2>

						<a href=""> + Add Your Review </a>

				 	</div> <!-- End .quicklook-reviews -->
				</div> <!-- End .product-guide -->
			</div> <!-- End .product-shop --> 
			<div class="mk-product-extras">
				<div class="mk-product-description">
					<h2> Cashmere Crewneck Long Sleeve Sweater in Bar Stripe.</h2> 
					<h4> Available in Black/Grey. Imported. Cashmere. Dry Clean.</h4> 

					<div class="mk-detail">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequ. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt id est. </p>
					</div> 
				</div> <!-- End. product-description --> 
				<div class="mk-last">
					<a href=""> View Full Product Details</a>
				</div>
			</div> <!-- End .product-extras -->

				</div>
			</div>
		</div>
	</div>
</div>