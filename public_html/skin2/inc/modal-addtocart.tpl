<?php 
////////////// Myntra ///////////////
// Modal - Add to Cart 
/////////////////////////////////////
?> 

<div class="mk-cart-wrapper mk-hide">
	<div class="mk-cart-modal">
		<span class="mk-modal-close">X</span>
		<div class="mk-cart-top mk-clear">
			<img src="/skin2/images/fpo/fpo-large-product.jpg" class="mk-large mk-f-left"/>

			<div class="mk-cart-product-info">
				<div class="mk-product-guide mk-f-left">
					<h5>Product Code: 173506</h5> <!-- Product Code -->
					<h1>APC Striped Sweater</h1>  <!-- Product Name -->
					<p class="mk-discount"> MRP RS 11995.00 (10% Off) </p> <!-- Product Discount --> 
					<h3> RS 10796 </h3>  <!-- Product Price -->

					<p> Size: XL / Color: Black &amp; White </p>
					<div class="mk-item-options">
						<a href="">Change</a>
						<a href="">Remove</a>
					</div>
				</div> <!-- End .product-guide -->
			</div> <!-- End .cart-product-info -->

			<div class="mk-cart-items mk-f-right">
				<p> Your Bag: </p>
				<h3> 3 Items </h3>
				<p> Total: </p>
				<h3> RS 12396 </h3> 
				
				<button class="mk-checkout-now"> Checkout </button>	
			</div> <!-- End .cart-items -->
		</div> <!-- End .cart-top -->
			
		<div class="mk-cart-bottom">
			<h2> Customers Also Bought </h2>
			<div class="mk-also-cycle-nav"></div>
			<div class="mk-also-cycle">
				<div class="mk-also">
					<a href=""><img src="/skin2/images/fpo/match-side1-1.jpg">
						<div class="mk-brand"><div><em>Belstaff</em></div></div></a>
					<a href=""><img src="/skin2/images/fpo/match-side1-2.jpg">
						<div class="mk-brand"><div><em>Marni</em></div></div></a>
					<a href=""><img src="/skin2/images/fpo/match-side1-3.jpg">
						<div class="mk-brand"><div><em>H &amp; M</em></div></div></a>
					<a href=""><img src="/skin2/images/fpo/match-side1-4.jpg">
						<div class="mk-brand"><div><em>Billy Reid</em></div></div></a>
				</div>
				<div class="mk-also">
					<a href=""><img src="/skin2/images/fpo/match-side2-1.jpg">
						<div class="mk-brand"><div><em>Marc</em></div></div></a>
					<a href=""><img src="/skin2/images/fpo/match-side2-2.jpg">
						<div class="mk-brand"><div><em>Smedley</em></div></div></a>
					<a href=""><img src="/skin2/images/fpo/match-side2-3.jpg">
						<div class="mk-brand"><div><em>Smedley</em></div></div></a>
					<a href=""><img src="/skin2/images/fpo/match-side2-4.jpg">
						<div class="mk-brand"><div><em>Levi's</em></div></div></a>
				</div>
			</div>
		</div> <!-- End .cart-bottom -->
	</div>
</div>