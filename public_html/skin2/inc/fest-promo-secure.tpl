<div class="mk-festival-header">	
	{if $festHeaderImage_Center_Secure}
		<a href="{$festHeaderImageURL}" class="mk-festival-header-center">
			<img class="lazy" data-lazy-src="{$festHeaderImage_Center_Secure}" />
		</a>
	{/if}
	{if $festHeaderImage_Left_Secure}
		<a href="{$festHeaderImageURL}" class="mk-festival-header-left">
			<img class="lazy" data-lazy-src="{$festHeaderImage_Left_Secure}" />
		</a>
	{/if}
	{if $festHeaderImage_Right_Secure}
		<a href="{$festHeaderImageURL}" class="mk-festival-header-right">
			<img class="lazy" data-lazy-src="{$festHeaderImage_Right_Secure}" />
		</a>
	{/if}
</div>