{if $isWebEngageFeedbackEnabled || $isWebEngageNotificationsEnabled}
  {literal}
    <script id="_webengage_script_tag" type="text/javascript">
      
      Myntra.WebEngage = Myntra.WebEngage || {};

      Myntra.WebEngage.feedbackEnabled = {/literal}{if $isWebEngageFeedbackEnabled}true{else}false{/if}{literal};
      Myntra.WebEngage.notificationsEnabled = {/literal}{if $isWebEngageNotificationsEnabled}true{else}false{/if}{literal};
      Myntra.WebEngage.category = {/literal}'{$pageName}'{literal};
      Myntra.WebEngage.userFullName = {/literal}'{if $userFirstName || $userLastName}{$userFirstName} {$userLastName}{else}{/if}'{literal};
      Myntra.WebEngage.userFirstName = {/literal}'{$userFirstName}'{literal};
      Myntra.WebEngage.userLastName = {/literal}'{$userLastName}'{literal};
      Myntra.WebEngage.userMobile = '';
      Myntra.WebEngage.userGender = {/literal}'{$userGender}'{literal};
      Myntra.WebEngage.userDOB = {/literal}'{$userDOB}'{literal};
      Myntra.WebEngage.navId = {/literal}'{$navId}'{literal};
      Myntra.WebEngage.utmCampaign = {/literal}'{$utm_campaign}'{literal};
      Myntra.WebEngage.isLoggedIn = '{/literal}{if $login}1{else}0{/if}{literal}';
      Myntra.WebEngage.isFirstTime = '{/literal}{if !$showLogin}1{else}0{/if}{literal}';
      Myntra.WebEngage.isRegistered = '{/literal}{if $showLogin}1{else}0{/if}{literal}';
      Myntra.WebEngage.repeatCustomer = '{/literal}{if $returningCustomer}1{else}0{/if}{literal}';      
      try {
        Myntra.WebEngage.regexpJSON = {/literal}{if $webEngageFeedbackRegexpJSON}{$webEngageFeedbackRegexpJSON}{else}''{/if}{literal};
      } catch (e) {
        Myntra.WebEngage.regexpJSON = '';
      }
    
    </script>
  {/literal}
{/if}
