<div class="mk-recommendation-tabs">
	{if $crossSell == 'test'}
		<ul class="mk-mynt-nav mk-cf">
			<li><a href="#tabs-1">Also Popular </a></li>
			<li><a href="#tabs-2">Match With</a></li>
			<li><a href="#tabs-3">More from this brand </a></li>
		</ul> 
		<div id="tabs-1">		
		</div>
		<div id="tabs-2">		
		</div>
		<div id="tabs-3">		
		</div>
	{else} {* Show the old UI *}
		<h3> You May Also Want To Consider</h3>
		<span class="mk-cycle-nav"> </span>
		<div class="mk-match-cycle" id="tabs-1"></div>
	{/if}
</div>
