{if $expressCheckout}
	<div class="mk-header-exp-payment-page">
		<ol>
			<li>
				SAVE YOUR PAYMENT DETAILS FOR A FASTER & EASIER CHECKOUT!
			</li>
			<li>
				<span class="exp-icon"></span><span>PAYMENT SETTINGS</span>
			</li>
		</ol>
	</div>
{else}	
	{$pn = $smarty.server.REQUEST_URI|regex_replace:"/\?.*$/":""}
	{* TODO: Find a better way to set the selected/completed states of the _steps_ *}
	{$selCart = (
	    $pn eq "/mkmycart.php" or
	    $pn eq "/mkmycartmultiple.php" or
	    $pn eq "/mkmycartmultiple_re.php")}
	{$selLogin = (
	    $pn eq "/checkout-login.php")}
	{$selAddr = (
	    $pn eq "/checkout-address.php" or
	    $pn eq "/mkcustomeraddress.php")}
	{$selPay = (
	    $pn eq "/mkpaymentoptions.php" or
	    $pn eq "/mkpaymentoptions_re.php")}
	{$tickCart = (
	    $pn eq "/checkout-login.php" or
	    $pn eq "/checkout-address.php" or
	    $pn eq "/mkcustomeraddress.php" or
	    $pn eq "/mkpaymentoptions.php" or
	    $pn eq "/mkpaymentoptions_re.php" or
	    $pn eq "/confirmation.php")}
	{$tickLogin = (
	    $pn eq "/checkout-address.php" or
	    $pn eq "/mkcustomeraddress.php" or
	    $pn eq "/mkpaymentoptions.php" or
	    $pn eq "/mkpaymentoptions_re.php" or
	    $pn eq "/confirmation.php" or
				$login)}
	{$tickAddr = (
	    $pn eq "/mkpaymentoptions.php" or
	    $pn eq "/mkpaymentoptions_re.php" or
	    $pn eq "/confirmation.php" or
				$selectedaddressid gt 0)}
	{$tickPay = (
	    $pn eq "/confirmation.php")}
	{$i=1}
	{if !$giftCardPaymentPage}
	<ul class="steps">
	    <li class="{if $selCart}selected{/if} {if $tickCart}completed{/if}">
	<span class="ico"></span>
	{if $tickPay}
		{$i}. shopping bag
	{else}
		<a href="{$http_location}/mkmycart.php" onClick="_gaq.push(['_trackEvent',{if $selPay} 'payment_page'{else} 'checkoutflow'{/if},'header', 'shoppingbag']);return true;">{$i}. shopping bag</a>
	{/if}
	    </li>
	    {$i=$i+1}
	    {if $enableGuestLogin}
	<li class="{if $selLogin}selected{/if} {if $tickLogin}completed{/if}">
	    <span class="ico"></span>
	    {$i}. email
	</li>
	{$i=$i+1}
	    {/if}
	    <li class="{if $selAddr}selected{/if} {if $tickAddr}completed{/if}">
	<span class="ico"></span>
	{if $tickPay}
	    {$i}. shipping
	{else}
	    <a href="{$https_location}/checkout-address.php?change=1" onClick="_gaq.push(['_trackEvent',{if $selPay} 'payment_page' {else} 'checkoutflow'{/if},'header', 'shipping']);return true;">{$i}. shipping</a>
	{/if}
	    </li>
	    {$i=$i+1}
	    <li class="{if $selPay}selected{/if} {if $tickPay}completed{/if}">
	<span class="ico"></span>
	{$i}. payment
	    </li>
	</ul>
	{else}
	<div class="mk-header-gc-payment-page">Myntra Gift Cards / Checkout</div>
	{/if}
{/if}