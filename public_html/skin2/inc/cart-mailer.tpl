<!-- Begin Cart HTML data -->

{if $productsInCart|@count > 0}
{foreach name=outer item=product key=itemid from=$productsInCart}
		
		<table width="580" cellspacing="0" cellpadding="0" border="0" align="center" style="border-bottom:1px solid #e2e2e2;">
							<tr>
								<td width="135" align="center" style="text-align:center; font-size:12px;">
									<a target="_blank" href="http://www.myntra.com/{$product.landingpageurl}?utm_source=triggered&utm_medium=mailer&utm_campaign=abn_cart&omni_camp_startdate={$campaignDate}>" style="color:#a61a71; text-decoration:none; outline:none;"><img  src="{$product.cartImagePath}" alt="{$product.productStyleName}"title="" width="100" height="133" style="display:block; margin-left:17px;"/></a>
							</td>
								
								<td width="280" align="center" style="text-align:center; font-size:12px; line-height:20px;">
									<div style="color:#959594;">PRODUCT CODE: {$product.productStyleId}</div>
									<div style="font-weight:bold;">{$product.productStyleName}</div>
																		
									<span>SIZE</span>
										{if $product.sizenames && $product.sizequantities}
											{if $product.sizenames|@count gt 0}
												{if $product.flattenSizeOptions} 
													{assign var=selectedSize value=$product.sizenameunified} 
												{else} 
													{assign var=selectedSize value=$product.sizename} 
												{/if}
												
												<span style="padding-left:5px; padding-right:5px;"><input type="text" name="size" value="{$selectedSize}" readonly="readonly" style="width:50px; text-align:center;"/></span>
											{/if}
										{else}
											<span style="padding-left:5px; padding-right:5px;"><input type="text" name="size" value="NA" readonly="readonly" style="width:50px; text-align:center;"/></span>
									
										{/if}
									
									<span>QTY</span>
									<span style="padding-left:5px; padding-right:5px;"><input type="text" name="size" value="{$product.quantity}" readonly="readonly" style="width:50px; text-align:center;"/></span>
								</td>
								
								<td width="165" align="right" style="text-align:right; font-size:12px;">
									<div style="color:#808080;">{if $product.discountAmount gt 0}<span style="text-decoration:line-through">Rs. {math equation="a*b" a=$product.productPrice b=$product.quantity assign="netPrice"}{$netPrice|round|number_format:0:".":","}</span>
								<span  style="color:#d14747;">{*$product.discountDisplayText*}</span>{/if}</div>
									<div style="color:#d14747;">Rs. {$product.productPrice*$product.quantity-$product.discountAmount|round|number_format:0:".":","}</div>
								</td>
							</tr>
		</table>
		
{/foreach}
<table width="580" cellspacing="0" cellpadding="0" border="0" align="center" style="border-bottom:1px solid #e2e2e2;">
	<tr>
		<td width="580" align="right" style="text-align:right; font-size:16px; padding-top:10px; padding-bottom:10px;">
			<span>TOTAL</span> 
			<span style="color:#d14747;">Rs. {$totalAmount|round|number_format:0:".":","}</span>
			{if $totalAmount lt $amount}
			<span style="color:#808080; text-decoration:line-through;">{$amount|round|number_format:0:".":","}</span>
			{/if}
				{if $itemDiscount gt 0}
					<br/><span style="font-size:12px;">ITEM DISCOUNT</span> 
					<span style="font-size:12px; color:#d14747">Rs. {$itemDiscount}</span>
				{/if}
						{if $cartLevelDiscount gt 0}<br/><span style="font-size:12px;">BAG DISCOUNT</span> <span style="font-size:12px; color:#d14747">Rs. {$cartLevelDiscount|round|number_format:0:".":","}</span>{/if}
						{if $cashdiscount gt 0}<br/><span style="font-size:12px;">CASHBACK</span > <span style="font-size:12px; color:#d14747">Rs. {$cashdiscount|round|number_format:0:".":","}</span></div>{/if}
						{if $couponDiscount gt 0}<br/><span style="font-size:12px;">COUPON </span ><span style="font-size:12px; color:#d14747">Rs. {$couponDiscount|round|number_format:0:".":","}</span></div>{/if}
		</td>
	</tr>
</table>

{/if}

<!-- End Cart HTML Data -->