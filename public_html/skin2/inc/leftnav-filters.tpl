		{*assign var="minNonLazyFilters" value="7"*}
		<div class="go-to-top-section" style="display:none;">
			<span class="go-to-top-arrow"></span>
			<button type="submit" class="go-to-top-btn btn normal-btn">GO TO TOP</button>
		</div>

		<div id="top-right" class="go-to-top-btn right mk-hide"></div>

		<div class="mk-product-filters">
			<form>
				<span class="mk-clear-all-filters mk-f-right mk-hide">Reset All Filters</span>
				<ul class="mk-product-selectors mk-clear">
					{if $article_type_filters}
					<li class="clearfix"><a href="javascript:void(0)" class="mk-a-label label-expand"><span class="mk-label-arrow"></span><span class="mk-f-left"> Categories</span><span data-key="articletypes" class="mk-f-right mk-clear-filter mk-hide">Reset</span></a>
						<div class="mk-filter-content">
						<div id="mk-articletypes" class="mk-filter-wrapper mk-articletypes-filters mk-resizable-box">
                            {foreach $filters->articleTypes as $atype}
                                {if $atype@iteration < $minNonLazyFilters}
									<a href="/{$atype.url}?src=lnc"
                                            {if $atype.noindex} rel="noindex" {/if}
                                            {if $nofollowForSearchFilterLinks} rel="nofollow" {/if}
                                            class="mk-labelx_check unchecked clearfix {$atype.css}"
                                            data-key="articletypes"
                                            data-value="{$atype.name}"
                                            data-sizeGroup="{$atype.sizeGroup}">
                                        <span class="cbx"></span>
                                        <span class="mk-filter-display">{$atype.name|capitalize} 
                                            <span class="mk-filter-product-count">[{$atype.count}]</span>
                                        </span>
                                    </a>
								{/if}
								{if !$optimEnabled && $atype@iteration == $minNonLazyFilters}
									<script>
								{/if}
								{if !$optimEnabled && $atype@iteration >= $minNonLazyFilters}
									Myntra.Data.lazyElems.push('<a href="/{$atype.url}" {if $atype.noindex} rel="noindex" {/if} {if $nofollowForSearchFilterLinks} rel="nofollow" {/if} class="mk-labelx_check unchecked clearfix {$atype.css}" data-key="articletypes" data-value="{$atype.name}" data-sizeGroup="{$atype.sizeGroup}"> <span class="cbx"></span> <span class="mk-filter-display">{$atype.name|capitalize} <span class="mk-filter-product-count">[{$atype.count}]</span> </span> </a>' + Myntra.Data.lazySeparator + '.mk-articletypes-filters .jspPane');
								{/if}
                                {if !$optimEnabled && $atype@iteration >= $minNonLazyFilters && $atype@last}
                                    </script>
                                {/if}
							{/foreach}
						</div>
						</div>
					</li>
					{/if}
					
					<li class="clearfix" style="{if $sizes_filter && $sizes_filter_count gt 1}display:block{else}display:none{/if}"><a href="javascript:void(0)" class="mk-a-label label-expand"><span class="mk-label-arrow"></span><span class="mk-f-left"> Size</span><span data-key="sizes" class="mk-f-right mk-clear-filter mk-hide">Reset</span></a>
						<div class="mk-filter-content">
						{if $showStickySize == 'test'}
						<div class="mk-filter-rem clearfix">
							<a class="close-rem" href="javascript:void(0);">X</a>
							<div class="first">
								<h4>USE THESE FILTERS TO FIND YOUR SIZE!</h4>
								<p>We'll remember your size when you are shopping at Myntra again.</p>
							</div>
							<div class="second">
								<h4 class="single">WE REMEMBERED YOUR SIZE : <span></span></h4>
								<h4 class="multiple">WE REMEMBERED YOUR SIZES : <span></span></h4>
								<p>We've filtered products based on sizes that you'd selected on your last visit to Myntra.</p>
							</div>
						</div>
						{/if}
						<div class="mk-filter-wrapper mk-sizes-filters clearfix mk-resizable-box">
							{foreach key=size item=count from=$faceted_search_results_new->sizes_facet}
								<label class="mk-labelx_check unchecked clearfix {$size|lower|trim|regex_replace:'/\W+/':''}-size-filter" data-key="sizes" data-value="{$size}"><span class="cbx"></span>{$size|upper|replace:" ":""}</label>
							{/foreach}
						</div>
						</div>
					</li>
					{if $brands_search_results && $brands_search_results_count gt 1}
					<li class="clearfix"><a href="javascript:void(0)" class="mk-a-label label-expand"><span class="mk-label-arrow"></span><span class="mk-f-left"> Brand</span><span data-key="brands" class="mk-f-right mk-clear-filter mk-hide">Reset</span></a>
						<div class="mk-filter-content">
							<input class="ui-autocomplete-input brands-autocomplete-input" value="SEARCH BRANDS" data-placeholder="SEARCH BRANDS" type="text" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true">
							<span class="mk-clear-brands-input"></span>
		    				<div id="mk-brand-search-results" class="mk-filter-wrapper mk-brands-filters" style="display:none">
		    				</div>
		    				<div id="mk-brands" class="mk-filter-wrapper mk-brands-filters ui-widget-content mk-resizable-box">
							{foreach key=brand item=count from=$faceted_search_results_new->brands_filter_facet name="brands"}
								{if $smarty.foreach.brands.iteration < $minNonLazyFilters}
								<a href="/{$brand|lower|trim|replace:' ':'-'}?src=lnb" class="mk-labelx_check unchecked clearfix {$brand|lower|trim|regex_replace:'/\W+/':''}-brand-filter" data-key="brands" data-value="{$brand}"><span class="cbx"></span><span class="mk-filter-display">{$brand} <span class="mk-filter-product-count">[{$count}]</span></span></a>
								{/if}
								{if !$optimEnabled && $smarty.foreach.brands.iteration == $minNonLazyFilters}
									<script>
								{/if}
								{if !$optimEnabled && $smarty.foreach.brands.iteration >= $minNonLazyFilters}
                                    Myntra.Data.lazyElems.push('<a href="/{$brand|lower|trim|replace:' ':'-'}?src=lnb" class="mk-labelx_check unchecked clearfix {$brand|lower|trim|regex_replace:'/\W+/':''}-brand-filter" data-key="brands" data-value="{$brand}"><span class="cbx"></span><span class="mk-filter-display">{$brand} <span class="mk-filter-product-count">[{$count}]</span></span></a>' + Myntra.Data.lazySeparator + '.mk-brands-filters .jspPane');
                                {/if}
                            {/foreach}
                            {if !$optimEnabled && $smarty.foreach.brands.iteration >= $minNonLazyFilters}
                                </script>
                            {/if}
							</div>
						</div>
					</li>
					{/if}
					{if $offer_type_count gt 0}
					<li class="clearfix"><a href="javascript:void(0)" class="mk-a-label label-expand"><span class="mk-label-arrow"></span><span class="mk-f-left"> Offer</span><span data-key="offer" class="mk-f-right mk-clear-filter mk-hide">Reset</span></a>
						<div class="mk-filter-content">
						<div class="mk-filter-wrapper mk-offer-filters">
				            {foreach item=offer from=$offer_type key=k}
				            <label class="mk-labelx_check unchecked clearfix {$k|lower|trim|regex_replace:'/\W+/':''}-offer-filter" data-key="offer" data-value="{$k}">
				            	<span class="cbx"></span>
				            	<span class="mk-filter-display">{$k|lower|capitalize}</span></label>
						    {/foreach}
						</div>
						</div>
					</li>
					{/if}					
					{if $discountPercentageBreakupCount gt 1}
					<li class="clearfix"><a href="javascript:void(0)" class="mk-a-label label-expand"><span class="mk-label-arrow"></span><span class="mk-f-left"> Discount</span><span data-key="discountpercentage" class="mk-f-right mk-clear-filter mk-hide">Reset</span></a>
						<div class="mk-filter-content">
						<div class="mk-filter-wrapper mk-discountpercentage-filters">
			            {if $discountPercentageBreakup}
				            {foreach item=discountpercengate from=$discountPercentageBreakup key=k}
							<label class="mk-labelx_check unchecked clearfix discountpercentage-filter mk-{$k}-discountpercentage-filter" data-key="discountpercentage" data-value="{$k}">
                            	<span class="rbtn"></span> {$k} % AND ABOVE
                        	</label>
						    {/foreach}
                        {/if}    
						</div>
						</div>
					</li>
					{/if}					
					{if $rangeMin neq $rangeMax}
					<li class="clearfix"><a href="javascript:void(0)" class="mk-a-label label-expand"><span class="mk-label-arrow"></span><span class="mk-f-left"> Price</span><span data-key="pricerange" class="mk-f-right mk-clear-filter mk-hide">Reset</span></a>
						<div class="mk-filter-content">
						<div class="mk-filter-wrapper mk-price-filters">
			            {if $priceBreakup}
				            {foreach item=range from=$priceBreakup}
							<label class="mk-labelx_check unchecked clearfix price-range-filter mk-{$range.rangeMin}-{$range.rangeMax}-pricerange-filter" data-key="pricerange" data-rangeMin="{$range.rangeMin}" data-rangeMax="{$range.rangeMax}">
                            	<span class="rbtn"></span>Rs. {$range.rangeMin|number_format:0:".":","} - Rs. {$range.rangeMax|number_format:0:".":","}
                        	</label>
						    {/foreach}
                        {/if}    
						</div>
						<div class="mk-filter-wrapper mk-pricerange-filters">
							<div class="mk-price-range" data-rangeMin="{$rangeMin}" data-rangeMax="{$rangeMax}"></div>
							<div class="mk-amount"></div>
						</div>	
						</div>
					</li>
					{/if}					
					{if $fit_filters_arr && $fit_filters_arr_count gt 1}
					<li class="clearfix"><a href="javascript:void(0)" class="mk-a-label label-expand"><span class="mk-label-arrow"></span><span class="mk-f-left"> Gender</span><span data-key="gender" class="mk-f-right mk-clear-filter mk-hide">Reset</span></a>
						<div class="mk-filter-content">
						<div class="mk-filter-wrapper mk-gender-filters">
						{foreach key=fit item=count from=$fit_filters_arr}
						<label class="mk-labelx_check unchecked clearfix {$fit|lower|trim|regex_replace:'/\W+/':''}-gender-filter" data-key="gender" data-value="{$fit}"><span class="cbx"></span><span class="mk-filter-display">{if $fit|lower eq "boy" }Boys{elseif $fit|lower eq "girl"}Girls{else}{$fit|lower|capitalize}{/if}</span></label>
						{/foreach}
						</div>
						</div>
					</li>
					{/if}
					{if $colour_arr && $colour_arr_count gt 1}
					<li class="clearfix"><a href="javascript:void(0)" class="mk-a-label label-expand"><span class="mk-label-arrow"></span><span class="mk-f-left"> Colour</span><span data-key="colour" class="mk-f-right mk-clear-filter mk-hide">Reset</span></a>
						<div class="mk-filter-content">
						<div class="mk-filter-wrapper mk-colour-filters mk-resizable-box">
						{foreach key=colour item=count from=$colour_arr}
						<label class="mk-labelx_check unchecked clearfix {$colour|lower|trim|regex_replace:'/\W+/':''}-colour-filter {$bgPosition}" data-key="colour" data-value="{$colour}" data-swatch="{$colorSwatchMapping[$colour]}"><span class="cbx"></span><span class="mk-color-swatch" style="background-position:-783px -{math equation='((x-1) * 11) + (x*2)' x=$colorSwatchMapping[$colour]}px"></span><span class="mk-filter-display">{$colour|lower|capitalize}</span></label>
						{/foreach}
						</div>
						</div>
					</li>
					{/if}
					{if $article_type_attributes}
					{foreach key=attribute item=props from=$article_type_attributes}
					<li class="clearfix article_type_attr"><a href="javascript:void(0)" class="mk-a-label label-expand"><span class="mk-label-arrow"></span><span class="mk-f-left"> {$props.title}</span><span data-key="{$props.title|replace:' ':'_'}_article_attr" class="mk-f-right mk-clear-filter mk-hide">Reset</span></a>
						<div class="mk-filter-content">
						<div class="mk-filter-wrapper mk-{$props.title|replace:' ':'_'}_article_attr-filters">
						{foreach key=prop item=count from=$props.values}
							{if $prop neq 'na'}
							<label class="mk-labelx_check unchecked clearfix {$prop|lower|replace:' ':'-'|regex_replace:'/\W+/':''}-ata-filter" data-key="{$props.title|replace:' ':'_'}_article_attr" data-value="{$prop}"><span class="cbx"></span><span class="mk-filter-display">{$prop|lower|capitalize}</span></label>
							{/if}
						{/foreach}
						</div>
						</div>
					</li>	
					{/foreach}					
					{/if}
				</ul> <!-- End .product-selectors --> 
			</form>
		</div> <!-- End .product-filters -->
