<header class="mk-site-hd mk-cf mk-checkout-hd">
    {if $telesales}
    <script>
         var telesales = true;
     </script>

        <div style="color: #fff;background-color: #333333 ;text-transform:none;padding:18px 0px;">Telesales User: <b>{$telesalesUser}</b></div>
    {else}
    <script>
        var telesales = false;
    </script>
    {/if}
   	<div class="customer-promises mk-cf"><span class="mk-f-left">{$newUiTopCpsMessage}</span>
   		<span class="mk-f-right">{if $payHelpline == 'test'}<span id="contact">{$contactUsLink}</span>{else}HELPLINE <em>{$customerSupportCall}</em>{/if}</span>
   	</div>
    <div class="logo">
        <a href="/">
        {if $smarty.server.HTTPS}
        	<img src="{$secure_cdn_base}/skin2/images/logo.png" alt="Myntra" title="Online Shopping store for Branded Shoes, Clothing, Accessories & Fashion wear in India.">
        {else}
        	<img src="{$cdn_base}/skin2/images/logo.png" alt="Myntra" title="Online Shopping store for Branded Shoes, Clothing, Accessories & Fashion wear in India.">
        {/if}
        </a>
        {include file="inc/header-checkout-steps.tpl" scope="parent"}
    </div>
    <p class="mk-f-right checkout-promise">
        <span class="lock-icon"></span>&nbsp;256-bit ssl secure checkout
    </p>
    {if $login}
	    <p class="mk-welcome welcome-logout">Welcome, {if $userfirstname neq ""}{$userfirstname|truncate:15}{else}{$login|truncate:15}{/if}
	        <span id="header-logout-link" class="mk-sign-out">Logout</span>
	    </p>
	    <form id="header-logout-form" method="post" action="/include/login.php">
	        <input type="hidden" name="mode" value="logout">
	        <input type="hidden" name="_token" value="{$USER_TOKEN}">
	    </form>
    {elseif $selCart}
	    <p class="mk-welcome mk-f-left btn small-btn primary-btn btn-login right">Login / Sign Up</p>
    {/if}
</header>

