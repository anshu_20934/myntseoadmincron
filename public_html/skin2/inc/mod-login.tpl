{assign var="referermail" value=$referer|replace:' ':''}

{*assign var="signUpMsg" value="And get Rs 1000 worth of coupons" *}
{*assign var="connectFbMsg" value="AND GET RS. 1,000 WORTH OF COUPONS THE FIRST TIME YOU CONNECT WITH FACEBOOK" *}

<div class="mod">
    <div class="mod-wrapper">
	<div class="hd">
				<div id="navigation">
				<ul>
				<li style="float:left;" >
					<a href="javascript:void(0);" id="tab1" class="selected"><span>New to Myntra? Sign Up</span></a>
				</li>

				<li style="float:left;"  >
					<a href="javascript:void(0);" id="tab2"><span>Returning User? Login</span></a>
				</li>
				</ul>
				<div class="clear"></div>
			</div>
	</div>
	<div class="bd mk-cf">
		<div class="main-content mk-cf">
			<div id="tab1-content">
				<div class="left-panel">
                {if ! loginBigBannersEnabled}
				<img class="lazy" data-lazy-src="{$newLoginBanners[0]['image_url']}" />
                {/if}
				<div class="signup">Sign up and get </div>
				<div class="signup_amt"> {$signupAmtMsg} </div>
				<div class="coupons">{$signupCouponsMsg}</div>

				</div>
				<div class="right-panel">
				<form method="post" class="frm-signup" action="{if $secureLoginEnabled}{$https_location}{else}{$http_location}{/if}/mklogin.php">
					<div class="err-top"></div>


						<label>EMAIL ADDRESS<span>* </span></label>
						<input type="text" name="email" value=""/>
						<div class="err err-user"></div>
					
										
						<label>PASSWORD<span>* </span></label>
						<input type="password" name="password" autocomplete="off" value="" />
						<div class="err err-pass"></div>
					
				
						<label>MOBILE NUMBER<span>* </span> </label>
						<input type="text" name="mobile" maxlength="10" value="" />
						<div class="err err-mobile"></div>
							<div class="gender-section">
								<label for="menu-gender" class="gender_label">GENDER</label>
								<input class="gender-sel" type="radio" name="gender" value="M" ><span class="name">Male</span>
		                    	<input class="gender-sel" type="radio" name="gender" value="F" ><span class="name">Female</span>
		                    	<div class="err err-gender"></div>
                  			</div>
    					<input type="hidden" name="_token" value="{$USER_TOKEN}" />
					<input type="hidden" name="mode" value="signup" />
					<input type="hidden" name="menu_usertype" value="C" />
					<input type="hidden" name="mrp_referer" id="mrp_referer" value="{$mrp_referer}" />
					<input type="hidden" name="secure_invoked" value="{if isset($is_secure)}1{else}0{/if}" />
					<input type="hidden" name="secure_login" value="{if $secureLoginEnabled}1{else}0{/if}" />
					<input type="hidden" name="new_register" value="{if isset($newRegister)}1{else}0{/if}" />
    				<div class="submit-buttons">
                    <div class="connect-with-facebook">
					    <button type="button" class="btn normal-btn btn-fb-connect" data-referer="{$mrp_referer}">
						<span class="ico-fb-small"></span> Sign up with Facebook
					    </button>
					    <div class="err err-fb"></div>
				    </div>
                    <div class="signup-button">
					    <div class="or"> OR </div>
					    <button type="submit" class="btn primary-btn btn-signup">Sign Up</button>
                    </div>
                    </div>
				</form>
				</div>
			</div>
			<div id="tab2-content">
				<div class="left-panel">
                    {if ! loginBigBannersEnabled}
					<img class="lazy" data-lazy-src="{$newLoginBanners[1]['image_url']}" />
                    {/if}
					<div class="coupons login-to"> LOGIN TO
					<ul>
						<li><span class="ico"> &nbsp; </span>{$loginMsg1}</li>
						<li><span class="ico"> &nbsp; </span>{$loginMsg2}</li>
						<li><span class="ico"> &nbsp; </span>{$loginMsg3}</li>
						<li><span class="ico"> &nbsp; </span>{$loginMsg4}</li>
					</ul>
				</div>
				</div>
					<div class="right-panel">
					<form method="post" class="frm-login" action="{if $secureLoginEnabled}{$https_location}{else}{$http_location}{/if}/mklogin.php">
						
						<div class="err-top"></div>
							
                       					 <label>EMAIL ADDRESS<span>* </span></label>
							<input type="text" name="email" value="{$usern}"/>
							<div class="err err-user"></div>
						
						
							<label>PASSWORD<span>* </span></label>
							<label class="btn-forgot-pass">Forgot password?</label>
							<input type="password" name="password" autocomplete="off" value="{$passwd}" />
							<div class="err err-pass"></div>
	    
                            <div class="submit-buttons">
            					<div class="connect-with-facebook">
			        			<button type="button" class="btn normal-btn btn-fb-connect" data-referer="{$mrp_referer}">
					    		<span class="ico-fb-small"></span> Login with Facebook
						        </button>
							    <div class="err err-fb"></div>
					            </div>

                                <div class="signin-button">
					            <div class="or"> OR </div>
						        <button type="submit" class="btn primary-btn btn-signin">Login</button>
                                </div>
                            </div>
					
	
						<input type="hidden" name="_token" value="{$USER_TOKEN}" />
						<input type="hidden" name="mode" value="signin" />
						<input type="hidden" name="menu_usertype" value="C" />
						<input type="hidden" name="secure_invoked" value="{if isset($is_secure)}1{else}0{/if}" />
						<input type="hidden" name="secure_login" value="{if $secureLoginEnabled}1{else}0{/if}" />
						<input type="hidden" name="new_register" value="{if isset($newRegister)}1{else}0{/if}" />
					</form>
					</div>
			</div>
		</div>
		<div class="forgot-pass-msg">
			<h1>NO WORRIES!</h1>
			<p class="ico-email">&nbsp;</p>
			<p class="reset"><span class="tick-ico"> &nbsp; &nbsp; &nbsp; </span> An email with a link to reset your password has been sent to :</p>
			<p class="email"></p>
			<p class="gray">Please check your email and click the password reset link</p>
			<button type="button" class="btn btn-forgot-pass-ok" >OK</button>
		</div>
	</div>
	</div>
</div>
<!-- Login background coming from PHP now -->
<input type="hidden" name="loginbg" value="{if isset($is_secure)}{$loginbg|replace :'http://myntra.myntassets.com':'https://d6kkp3rk1o2kk.cloudfront.net'}{else}{$loginbg}{/if}" class="mk-login-bg-path" />

