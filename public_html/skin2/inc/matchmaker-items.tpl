	{assign var=i value=0}
	{foreach name=widgetdataloop item=widgetdata from=$avail_recommendation.data}
			{if $widgetdata.product neq '' && $widgetdata.product neq ' '}
				{if $i eq 0}<div class="mk-match">{elseif $i mod 5 eq 0 }</div><div class="mk-match">{/if}
				<div class="mk-cycle-inner" data-id="prod_{$widgetdata.id}">
					{if $quicklookenabled}
						<span data-widget="avail_widget" data-styleid="{$widgetdata.styleid}" data-href="{$http_location}/{$widgetdata.landingpageurl}?t_code={$tracking_code}&previous_style_id={$previousProductStyleId}&previous_recommendation_type={$previousRecommendationType}&previous_recommendation_styleids={$previousRecommendationStyleIds}" class="quick-look {if $page eq 'addedtocart'}mk-hide{/if}" ></span>
					{/if}
					<a title="{$widgetdata.product}" href="{$http_location}/{$widgetdata.landingpageurl}?t_code={$tracking_code}&previous_style_id={$previousProductStyleId}&previous_recommendation_type={$previousRecommendationType}&previous_recommendation_styleids={$previousRecommendationStyleIds}&src=pp" onclick="_gaq.push(['_trackEvent','pdp_click', Myntra.Data.pageName, '{$tabName}']);s_crossSell();"> {*send in the tabname for ga *}
						{if $widgetdata.visual_tag}
			                 {assign var="visualtag" value=":"|explode:$widgetdata.visual_tag}
			                 <div class="mk-visual-tag mk-visual-tag-{$visualtag[1]|lower}"><div>{$visualtag[0]|lower}</div></div>
			            {/if}
						<img src='{myntraimage key="style_150_200" src=$widgetdata.search_image|replace:"_images_180_240":"_images_150_200"|replace:"_images_240_320":"_images_150_200"|replace:"style_search_image":"properties"}' alt="{$widgetdata.product}">
					</a>
					<div class="product-info">
						{if $widgetdata.generic_type neq 'design'}
					        <div class="availability" data-sizes="{if $widgetdata.sizes neq '' && $widgetdata.sizes neq ' '}Sizes: {$widgetdata.sizes|replace:",":", "}{else}SOLD OUT{/if}">
					        	<div class="tooltip-arrow"><div class="tooltip-arrow-inner"></div></div>
					        	<div class="tooltip-content"></div>
					        </div>
				        {/if}
						<p class="brand-text">{$widgetdata.global_attr_brand|truncate:20}</p>
						<p class="price-text red">{$rupeesymbol}{if $widgetdata.discount > 0}{$widgetdata.discounted_price|number_format:0:".":","} <span class="strike gray">{$widgetdata.price|number_format:0:".":","}</span>{else}{$widgetdata.price|number_format:0:".":","}{/if}</p>
						<p class="price-text red-text red">{include file="string:{$widgetdata.discount_label}"}  </p>
					</div>
				</div>
			{if $smarty.foreach.widgetdataloop.last}</div>{/if}
		{/if}
	{assign var=i value=$i+1}
	{/foreach}
	
