 	<ul class="rs-carousel-runner">{assign var=i value=0}
	{foreach name=widgetdataloop item=widgetdata from=$avail_recommendation.data}
			{if $widgetdata.product neq '' && $widgetdata.product neq ' '}
					<li data-id="prod_{$widgetdata.id}"  class="rs-carousel-item {if $cartpage}recent-inline recent-inline-load{/if}">
					{if $quicklookenabled && $cartpage}
						<span class="quick-look" data-widget="avail_widget" data-styleid="{$widgetdata.styleid}" data-href="{$http_location}/{$widgetdata.landingpageurl}" href="javascript:void(0)"></span>
					{/if}
					<a title="{$widgetdata.product}" href="{$http_location}/{$widgetdata.landingpageurl}?t_code={$tracking_code}&previous_style_id={$previousProductStyleId}&previous_recommendation_type={$previousRecommendationType}&previous_recommendation_styleids={$previousRecommendationStyleIds}" onclick="_gaq.push(['_trackEvent','pdp_click', Myntra.Data.pageName, 'avail_widget']);s_crossSell();">
						<div class="mk-product-image">
							<img src='{myntraimage key="style_150_200" src=$widgetdata.search_image|replace:"_images_180_240":"_images_150_200"|replace:"_images_240_320":"_images_150_200"|replace:"style_search_image":"properties"}' alt="{$widgetdata.product}">
						</div>
					</a>
					<div class="product-info">
						{if $widgetdata.generic_type neq 'design'}
					        <div class="availability" data-sizes="{if $widgetdata.sizes neq '' && $widgetdata.sizes neq ' '}Sizes: {$widgetdata.sizes|replace:",":", "}{else}SOLD OUT{/if}">
					        	<div class="tooltip-arrow"><div class="tooltip-arrow-inner"></div></div>
					        	<div class="tooltip-content"></div>
					        </div>
			        	{/if}
						<p class="brand-text">{$widgetdata.global_attr_brand|truncate:20}</p>
						<p class="price-text red">{$rupeesymbol}{if $widgetdata.discount > 0}{$widgetdata.discounted_price|number_format:0:".":","} <span class="strike gray">{$widgetdata.price|number_format:0:".":","}</span>{else}{$widgetdata.price|number_format:0:".":","}{/if}</p>
						<p class="price-text red-text red">
							{include file="string:{$widgetdata.discount_label}"}  </p>
					</div>
					{if $cartpage}
					<div class="inline-add-block">
						{if $widgetdata.sizes_facet eq 'Freesize'}
						{assign var=size_selected value=$widgetdata.sizes_facet}
							<button class=" inline-add btn primary-btn small-btn" onclick="_gaq.push(['_trackEvent','buy_now', Myntra.Data.pageName, 'avail_widget']);">BUY NOW</button>
						{else}
							<div class="inline-add-block-size ">
								<select class="inline-select" name="inline-select">
									<option value="0">SELECT SIZE TO BUY</option>
								</select>
							</div>
							<button class=" inline-add btn primary-btn small-btn" onclick="_gaq.push(['_trackEvent','buy_now', Myntra.Data.pageName, 'avail_widget']);">BUY NOW</button>
						{/if}
				<form action="mkretrievedataforcart.php?pagetype=productdetail" class="inline-add-to-cart-form" method="post" class="clearfix">
	                   	<input type="hidden" name="_token" value="{$USER_TOKEN}" />
	                    <input type="hidden" name="productStyleId" value="{$widgetdata.styleid}">
	                	<input type="hidden" value="1" name="quantity">
		                <input type="hidden" class="sizename" value="" name="sizename[]">
		                <input type="hidden" value="1" name="sizequantity[]">
		                <input type="hidden" name="selectedsize" value="">
		                <input type="hidden" name="pageName" value="{if $cartpage}cart{else}addtocartpopup{/if}">
		                <input type="hidden" name="widgetType" value="recommedation_widget">
		                <input type="hidden" name="productSKUID" class="productSKUID" value="{if $size_selected}{$size_selected}{/if}">
		                <input type="hidden" name="productStyleLabel"  value="{$widgetdata.stylename}">
		                <input type="hidden" name="brand" value="{$widgetdata.global_attr_brand}">
		                <input type="hidden" name="articleType" value="{$widgetdata.global_attr_article_type}">

	           </form>
			</div>

					{/if}
				</li>

		{/if}
	{assign var=i value=$i+1}
	{/foreach}
	</ul>
