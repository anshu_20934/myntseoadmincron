
<form method="post" class="addr-form">
    <input type="hidden" name="_token" value="{$USER_TOKEN}">
    <input type="hidden" name="id" value="{$addr.id}">
    <input type="hidden" name="country" value="IN">
    <div class="row">
        <label>Pin Code <strong>*</strong></label>
        {*<p class="note">Please enter correct pin code to ensure delivery.</p>*}
        <input type="text" name="pincode" class="pincode" maxlength="6" value="{$addr.pincode}">
        <p class="err err-pincode"></p>
        <input type="hidden" name="pinerr" class="pinerr" value="0">
    </div>
    <div class="row">
        <label>Name <strong>*</strong></label>
        <input type="text" name="name" class="name" value="{$addr.name}">
        <p class="err err-name"></p>
        <div class="copyname">
            <input name="copy-name" class="copy-name" type="checkbox" checked="true"> Save as your name
        </div>
    </div>
    <div class="row yourname">
        <label>Your Name  <strong>*</strong></label>
        <input type="text" name="yourname" class="yourname" value="{$addr.yourname}">
        <p class="err err-yourname"></p>
    </div>
    <div class="row">
        <label>Address  <strong>*</strong></label>
        <textarea name="address" class="address" rows="3">{$addr.address}</textarea>
        <p class="err err-address"></p>
    </div>
    <div class="row">
        <label>Locality / Town {if $abLocality eq 'yes'}<strong>*</strong>{/if}</label>
        <input type="text" name="locality" class="locality" value="{$addr.locality}">
        <p class="err err-locality"></p>
        <input type="hidden" name="selected-locality" value="">
    </div>
    <div class="row">
        <div class="col">
            <label>City / District<strong>*</strong></label>
            <input type="text" name="city" class="city" value="{$addr.city}">
            <select name="sel-city" class="sel-city hide"></select>
            <p class="err err-city"></p>
        </div>
        <div class="col">
            <label>State <strong>*</strong></label>
            <input type="text" name="state-txt" class="state-txt hide" value="{$addr.state}">
            <select name="state" class="state">
                <option value="">--Select State--</option>
                {foreach item="state" from=$states}
                <option value="{$state.code}"{if $state.code eq $addr.state} selected{/if}>{$state.state}</option>
                {/foreach}
            </select>
            <p class="err err-state"></p>
        </div>
    </div>
    <div class="row">
        <label>Mobile No <strong>*</strong></label>
        {*<p class="note">Please do not prefix your mobile number with country code or '0'</p>*}
        <input type="text" name="mobile" class="mobile" maxlength="10" value="{$addr.mobile}">
        <p class="err err-mobile"></p>
    </div>
    <div class="pin-warn">
        <span class="ico"></span>
        <p></p>
    </div>
</form>

