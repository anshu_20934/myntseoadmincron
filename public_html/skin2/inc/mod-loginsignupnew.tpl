{assign var="referermail" value=$referer|replace:' ':''}

{*assign var="signUpMsg" value="And get Rs 1000 worth of coupons" *}
{*assign var="connectFbMsg" value="AND GET RS. 1,000 WORTH OF COUPONS THE FIRST TIME YOU CONNECT WITH FACEBOOK" *}

<div class="mod">
    <div class="mod-wrapper">
	<div class="hd">
				<div id="navigation">
				<ul>
				<li style="float:left;" >
					<a href="javascript:void(0);" id="tab1" class="selected"><span>New to Myntra?</span></a>
				</li>

				<li style="float:left;"  >
					<a href="javascript:void(0);" id="tab2"><span>Have an Account? Login</span></a>
				</li>
				</ul>
				<div class="clear"></div>
			</div>
	</div>
	<div class="bd mk-cf">
		<div class="main-content mk-cf">
			<div id="tab1-content">
				<div class="left-panel">
				 {if !$loginBigBannersEnabled && $newLoginBanners}
				<img class="lazy" data-lazy-src="{if $smarty.server.HTTPS}{$newLoginBanners[0]['image_url']|replace : 'http://myntra.myntassets.com':'https://d6kkp3rk1o2kk.cloudfront.net'}{else}{$newLoginBanners[0]['image_url']}{/if}" src="{if $smarty.server.HTTPS}{$secure_cdn_base}{else}{$cdn_base}{/if}/skin2/images/loader_180x240.gif"/>
		        {/if}
				<div class="signup">Create an account &</div>
				<div class="signup_amt">GET {$signupAmtMsg} </div>
				<div class="coupons">{$signupCouponsMsg}</div>

				</div>
				<div class="right-panel">
				<form method="post" class="frm-signup" action="{if $secureLoginEnabled}{$https_location}{else}{$http_location}{/if}/mklogin.php">
					<div class="err-top"></div>


						<label>EMAIL ADDRESS</label>
						<input type="text" name="email" value=""/>
						<div class="err err-user"></div>
					
										
						<label>PASSWORD</label>
						<input type="password" name="password" value="" />
						<div class="err err-pass"></div>
					
				
						<label>MOBILE NUMBER</label>
						<input type="text" name="mobile" maxlength="10" value="" />
						<div class="err err-mobile"></div>
							<div class="gender-section">
								<label for="menu-gender" class="gender_label">GENDER</label>
								<input class="gender-sel" type="radio" name="gender" value="M" ><span class="name">Male</span>
		                    	<input class="gender-sel" type="radio" name="gender" value="F" ><span class="name">Female</span>
		                    	<div class="err err-gender"></div>
                  			</div>
    					<input type="hidden" name="_token" value="{$USER_TOKEN}" />
					<input type="hidden" name="mode" value="signup" />
					<input type="hidden" name="menu_usertype" value="C" />
					<input type="hidden" name="mrp_referer" id="mrp_referer" value="{$mrp_referer}" />
					<input type="hidden" name="secure_invoked" value="{if isset($is_secure)}1{else}0{/if}" />
					<input type="hidden" name="secure_login" value="{if $secureLoginEnabled}1{else}0{/if}" />
					<input type="hidden" name="new_register" value="{if isset($newRegister)}1{else}0{/if}" />
    				<div class="submit-buttons">
                    <div class="connect-with-facebook">
					    <button type="button" class="btn normal-btn btn-fb-connect" data-referer="{$mrp_referer}">
						<span class="ico-fb-small"></span>Connect with Facebook
					    </button>
					    <div class="err err-fb"></div>
				    </div>
                    <div class="signup-button">
					    <div class="or"> OR </div>
					    <button type="submit" class="btn primary-btn btn-signup">Create My Account</button>
                    </div>
                    </div>
				</form>
				</div>
			</div>
			<div id="tab2-content">
				<div class="left-panel">
    				{if !$loginBigBannersEnabled && $newLoginBanners}
					<img class="lazy" data-lazy-src="{if $smarty.server.HTTPS}{$newLoginBanners[1]['image_url']|replace : 'http://myntra.myntassets.com':'https://d6kkp3rk1o2kk.cloudfront.net'}{else}{$newLoginBanners[1]['image_url']}{/if}" src="{if $smarty.server.HTTPS}{$secure_cdn_base}{else}{$cdn_base}{/if}/skin2/images/loader_180x240.gif"/>
                    {/if}
					<div class="coupons login-to"> LOGIN TO
					<ul>
						<li><span class="ico"> &nbsp; </span>{$loginMsg1}</li>
						<li><span class="ico"> &nbsp; </span>{$loginMsg2}</li>
						<li><span class="ico"> &nbsp; </span>{$loginMsg3}</li>
						<li><span class="ico"> &nbsp; </span>{$loginMsg4}</li>
					</ul>
				</div>
				</div>
					<div class="right-panel">
					<form method="post" class="frm-login" action="{if $secureLoginEnabled}{$https_location}{else}{$http_location}{/if}/mklogin.php">
						
						<div class="err-top"></div>
							
                       					 <label>EMAIL ADDRESS<span>* </span></label>
							<input type="text" name="email" value="{$usern}"/>
							<div class="err err-user"></div>
						
						
							<label>PASSWORD<span>* </span></label>
							<label class="btn-forgot-pass">Forgot password?</label>
							<input type="password" name="password" value="{$passwd}" />
							<div class="err err-pass"></div>
							
							{if $showLoginCaptchaFG}
								{if $showLoginCaptcha}
									<div id="CaptchaBlock" class="span-12 left captcha-block" style="display: block;">
								{else}
									<div id="CaptchaBlock" class="span-12 left captcha-block" style="display: none;">
								{/if}
									<div class="clearfix captcha-details">
										<div class="p5 corners black-bg4 span-7 left">
											<img   id="captcha" />
										</div>
									</div>
									<div class="clearfix mt10 captcha-entry">
										<label>Type text as in image above</label> 

										<label class="login-captcha-change-text"><a href="javascript:void(0)" onclick="document.getElementById('captcha').src='{$https_location}/captcha/captcha.php?id=loginPageCaptcha&rand='+Math.random();document.getElementById('captcha-form').focus();"id="change-image" >Change text</a><label>
											
										<input type="text" name="userinputcaptcha" id="captcha-form" />
										{*<div id="captcha-loading" class="captcha-loading" style="display:none;"></div>*}
									</div>
									<div class="err err-login-captcha"></div>
								</div>
							{/if}

							{if $showLoginCaptchaFG}
								{if $showLoginCaptcha}
                            		<div class="submit-buttons" id="captcha-login-submit" style="margin-top: -5px;">
                            	{else}
                            		<div class="submit-buttons" id="captcha-login-submit">
                            	{/if}
                            {else}
                            	<div class="submit-buttons">
                            {/if}
            					<div class="connect-with-facebook">
			        			<button type="button" class="btn normal-btn btn-fb-connect" data-referer="{$mrp_referer}">
					    		<span class="ico-fb-small"></span> Login with Facebook
						        </button>
							    <div class="err err-fb"></div>
					            </div>

                                <div class="signin-button">
					            <div class="or"> OR </div>
						        <button type="submit" class="btn primary-btn btn-signin">Login</button>
                                </div>
                            </div>
					
	
						<input type="hidden" name="_token" value="{$USER_TOKEN}" />
						<input type="hidden" name="mode" value="signin" />
						<input type="hidden" name="menu_usertype" value="C" />
						<input type="hidden" name="secure_invoked" value="{if isset($is_secure)}1{else}0{/if}" />
						<input type="hidden" name="secure_login" value="{if $secureLoginEnabled}1{else}0{/if}" />
						<input type="hidden" name="new_register" value="{if isset($newRegister)}1{else}0{/if}" />
					</form>
					</div>
			</div>
		</div>
		<div class="forgot-pass-msg">
			<h1>NO WORRIES!</h1>
			<p class="ico-email">&nbsp;</p>
			<p class="reset"><span class="tick-ico"> &nbsp; &nbsp; &nbsp; </span> An email with a link to reset your password has been sent to :</p>
			<p class="email"></p>
			<p class="gray">Please check your email and click the password reset link</p>
			<button type="button" class="btn btn-forgot-pass-ok" >OK</button>
		</div>
	</div>
	</div>
</div>
<!-- Login background coming from PHP now -->
<input type="hidden" name="loginbg" value="{if isset($is_secure)}{$loginbg|replace :'http://myntra.myntassets.com':'https://d6kkp3rk1o2kk.cloudfront.net'}{else}{$loginbg}{/if}" class="mk-login-bg-path" />

