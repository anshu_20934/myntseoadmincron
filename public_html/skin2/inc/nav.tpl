<h1>hi</h1>
<section class="mk-site-nav-wrapper">
	<nav class="mk-site-nav">
		<div class="mk-level-1 mk-f-left">
			<ul>
    {foreach key=indexA item=itemsA from=$topNavigationWidget name=topNavigationWidgetLoop}
				<li>
					<a class="mk-level1 mk-level1-link {if !$pdpMiniNav && $nav_selection_arr[$itemsA.id]}mk-active mk-persist{/if} {if $itemsA.child|count gt 0}mk-nav-bg-arrow{/if}" href="{$itemsA.link_url}?nav_id={$itemsA.id}" {if $itemsA.link_url|strpos:"http" === 0}rel="nofollow" target="_blank"{/if} {if $itemsA.link_style} style="{$itemsA.link_style}" {/if} onClick="_gaq.push(['_trackEvent', 'megamenu', Myntra.Data.pageName, '{$itemsA.link_url}']);return true;">{$itemsA.link_name}</a>
					{if $itemsA.link_name|lower|trim eq "hot in december"}<span class="icon-new-large"></span>{/if}
					{if $itemsA.child|count gt 0}
					<div class="mk-level-2 mk-f-left {if !$pdpMiniNav && $nav_selection_arr[$itemsA.id]}mk-show-strict{else}mk-hide{/if}">
						<ul>
							<li>
							{assign var=i value=0}
	                        {foreach key=indexB item=itemsB from=$itemsA.child}
								<a class="mk-level2 mk-level2-category {if !$pdpMiniNav && $nav_selection_arr[$itemsB.id]}mk-active{/if}" href="{$itemsB.link_url}?nav_id={$itemsB.id}" {if $itemsB.link_url|strpos:"http" === 0}rel="nofollow" target="_blank"{/if} {if $itemsB.link_style} style="{$itemsB.link_style}" {/if} onClick="_gaq.push(['_trackEvent', 'megamenu', Myntra.Data.pageName, '{$itemsB.link_url|escape}']);return true;">
									{if $itemsA.link_name eq "Men" or $itemsA.link_name eq "Women"}
										{$itemsA.link_name}'s {$itemsB.link_name}
									{else}
										{$itemsB.link_name}
									{/if}
								</a>
								{assign var=i value=$i+1}
								{if $i%8 eq '0'}</li><li>{/if}
								{foreach key=indexC item=itemsC from=$itemsB.child}
									<a class="mk-level2 {if !$pdpMiniNav && $nav_selection_arr[$itemsC.id]}mk-active{/if}" href="{$itemsC.link_url}?nav_id={$itemsC.id}" {if $itemsC.link_style} style="{$itemsC.link_style}" {/if} onClick="_gaq.push(['_trackEvent', 'megamenu', Myntra.Data.pageName, '{$itemsC.link_url}']);return true;">{$itemsC.link_name}</a>
									{assign var=i value=$i+1}
									{if $i%8 eq '0'}</li><li>{/if}
								{/foreach}
	                        {/foreach}
                        	</li>
                       </ul>
                    </div>
                    {/if}
				</li>
    {/foreach}
			</ul>
		</div>
		{if !$pdpMiniNav && $bannerImageProperties.image_url}
			<div class="mk-search-promo-banner mk-f-left" style="display: block;">
				<a href="{$http_location}{$bannerImageProperties.target_url}" onClick="_gaq.push(['_trackEvent', 'banner', Myntra.Data.pageName, '{$bannerImageProperties.target_url}']);return true;">
					<img src="{myntraimage key='search_banner' src=$bannerImageProperties.image_url}" alt="{$bannerImageProperties.alt_text}"/>
				</a>
			</div>
		{elseif !$pdpMiniNav && ($navflag || $topNavBannerProperties.image_url)}
			{*if $login}
				<div class="mk-mymyntra-dash mk-f-right" style="display: block;">
					<ul class="gray">
						{$smarty.capture.myMyntraDash}
					</ul>
				</div>
			{else*}
				<div class="mk-free-shipping-cb mk-f-right" style="display: block;">
					<a href="{$http_location}{$topNavBannerProperties.target_url}" onClick="_gaq.push(['_trackEvent', 'banner', Myntra.Data.pageName, '{$topNavBannerProperties.target_url}']);return true;">
						<img class="nav-banner" lazy-src="{myntraimage key='topnav' src=$topNavBannerProperties.image_url}" alt="{$topNavBannerProperties.alt_text}"/>
					</a>
				</div>
			{*/if*}
		{/if}
	</nav>
</section>
<div class="mk-clear"></div>
