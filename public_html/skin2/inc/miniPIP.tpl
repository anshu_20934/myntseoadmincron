<script>
var vatEnabled = {if $vatEnabled}true{else}false{/if};
var vatCouponMessage = "";
var vatThreshold = "";
var showVatMessage = {if $showVatMessage}true{else}false{/if};
if (vatEnabled){

    var vatCouponMessage = "{$vatCouponMessage}";
    var vatThreshold = "{$vatThreshold}";
}
var styleId = "{$productStyleId}";
var originalprice = "{$originalprice}";
</script>

<div style="display:none" id="minipip-data" data-mini-exclude={if $socialExclude}true{else}false{/if} data-socialaction='wishlist'></div>
<div class="mk-more-views rs-carousel mk-f-left"> <!-- Product Thumbs -->
	<ul class="rs-carousel-runner">
	{assign var=i value=0}
    {section name="area" loop=$area_icons}
    <li class="rs-carousel-item {if $i==0}img-selected{/if}"><a title="" href='javascript:void(0)'>
    	<img width="48" height="64"  title="" src='{if $smarty.section.area.iteration == 1}{myntraimage key="style_48_64" src=$area_icons[area].image|replace:"_images":"_images_48_64"}{else}{$cdn_base}/skin2/images/spacer.gif{/if}'  {if $smarty.section.area.iteration > 1}data-lazy-src='{myntraimage key="style_48_64" src=$area_icons[area].image|replace:"_images":"_images_48_64"}' class="lazy mk-lazy-white-border"{/if} alt='{myntraimage key="style_360_480" src=$area_icons[area].image|replace:"_images":"_images_360_480"}' data-src='{myntraimage key="style_360_480" src=$area_icons[area].image|replace:"_images":"_images_360_480"}'></a>
	</li>
	{assign var=i value=$i+1}
    {/section}
	</ul>
</div><!-- End .more-views -->
<div class="mk-product-large-image">
	{if $visualtagText}
	<div class="mk-visual-tag-pdp mk-visual-tag-{$visualtagCssClass}"><div>{$visualtagText}</div></div>
	{/if}
	<span class="prev-next-btns" id="view-prev-minipip"></span>
	<span class="prev-next-btns" id="view-next-minipip"></span>
	<a href="javascript:void(0)" alt="{$productDisplayName|escape:'htmlall'}"  ><img width="360" height="480" src="{myntraimage key="style_360_480" src=$defaultImage|replace:'_images':'_images_360_480'}" /></a> <!-- Product Image -->
</div>
<div id="minipip-product-guide" class="mk-product-guide mk-f-right">

						<h5>Product Code: {$productStyleId}</h5> <!-- Product Code -->
						<h1>{if $fromcart neq 1}
							<a href="" title="More Details" onclick="_gaq.push(['_trackEvent', 'pdp_click', Myntra.Data.pageName, 'quicklook_view_details']);">
							{/if}
								{$productDisplayName}
							{if $fromcart neq 1}</a>{/if}
						</h1>  <!-- Product Name -->
						<h3 class="red">
							Rs. {if $discountamount > 0}
								{math equation="x - d" x=$originalprice d=$discountamount assign="finalamount"}{$finalamount|round|number_format:0:".":","} <span class="strike gray">{$originalprice|round|number_format:0:".":","}</span>
							{else}
								{$originalprice|round|number_format:0:".":","}
							{/if}
                            {if $quickLookCouponWidgetEnabled}
                            <span class='ql-coupon-offer-launcher' style="display:inline"><span class='coupon-quick-look-icon'><span class="offer-arrow"></span></span><span class="coupon-quick-look-text" style="vertical-align: middle;display:inline-block;padding-bottom:4px">{$quickLookCouponWidgetText}</span></span>{/if}
						</h3>  <!-- Product Price -->

					{if $discountlabeltext}
						<p class="mk-discount"> {include file="string:{$discountlabeltext}"} </p> <!-- Product Discount -->
					{/if}

					{if !$disableProductStyle}


						<div class="mk-product-option-cont">
						{if $pdpColorGrouping && $colourSelectVariant == 'test' && $relatedColorStyleDetailsArray}
							<div class="mk-colour rs-carousel">
								<div class="lbl">{$relatedColorStyleDetailsArray|count} More Colour{if $relatedColorStyleDetailsArray|count gt 1}s{/if}:</div>
							    <ul class="rs-carousel-runner mk-cf">
							    	{foreach from=$relatedColorStyleDetailsArray key=key item=val}
								        <li class="rs-carousel-item">
								            <a href="{$http_location}/{$val.url}{if $nav_id}?nav_id={$nav_id}{/if}" data-id="{$key}">
									            {assign "title" "{$val.base_color}{if $val.color1 neq 'NA' && $val.color1 neq ''} / {$val.color1}{/if}"}
								            	<img src="{myntraimage key='style_48_64' src=$val.image|replace:'_images_180_240':'_images_48_64'|replace:'/style_search_image/':'/properties/'}" height="52" width="39" title="{$title}" alt="{$title}" />
								            </a>
								        </li>
							        {/foreach}
							    </ul>
							    <span class="icon-prev mk-hide"></span>
							    <span class="icon-next mk-hide"></span>
							</div>
						{/if}

                        <div id="quicklook-coupon-widget" style="display:none;position:relative;">
                                <div class="close">CLOSE</div>
                                <div class="loading" style="display: none;"></div>
                                <div class="no-coupon" style="display: none;">
                                    <div class="message"></div>
                                </div>
                                
                                <div class="offer-details" style="display: none;">
                                    <div style="font-size:16px;text-align:center">GET THIS PRODUCT FOR
                                        <div class="red"><span style="text-transform:none">Rs. </span><span class="coupon-discounted-price"></span></div>
                                    </div>                                    
                                    <div style="font-size: 12px;text-align: center;">WHEN YOU APPLY COUPON CODE <span class="red">"<span class="coupon-code red"></span>"</span> IN YOUR BAG</div>
                                    <div class="line"></div>        
                                    <div style="font-size:12px; text-align:left;padding-bottom:4px;">DETAILS</div>
                                    <div class="coupon-details">  
                                    	<div>* Get <span class="coupon-discount"></span> off <span class="is-min-condition hide">on a minimum purchase of Rs. <span class="min-amount"></span></span></div>
                                    	<div><span class="coupon-tnc"></span> </div>
                                        <div>* Valid till <span class="coupon-validity"></span></div>
                                        <div><span class="coupon-vat-message"></span></div>    
                                    </div>
                        </div>
</div>
						{if $sizeOptionSKUMapping}
						<div class="mk-custom-drop-down mk-size-drop mk-size-drop-pdp">
						{if $allSizeOptionSKUMapping|count == 1}
							{foreach from=$allSizeOptionSKUMapping key=key item=val}
							{assign var=sku value=$key}
								<div class="mk-freesize" data-count="{$val[2]}">{$val[0]}</div>
								{if $val[2] <= 3}<div class="mk-count-message">Only <span>{$val[2]}</span> left in stock</div>{/if}
							{/foreach}

						{else}
                            <div class="flat-size-options">
                                <div class="lbl">Select a Size:</div>
                                <div class="options">
                                    <input type="hidden" name="mk-size" class="mk-size" value="">
                                    {$j=1}
                                    {foreach $allSizeOptionSKUMapping as $key => $val}
                                        {if $val[1]}
                                        <button class="btn size-btn {if $renderTooltip}vtooltip{/if}"
                                            data-count="{$val[2]}" 
                                            value="{$key}" 
                                            onclick="sizeSelect({$key},{$j},this)" 
                                            data-tooltipText="{$sizeOptionsTooltipHtmlArray[$key]}" 
                                            data-tooltipTop="{$tooltipTop}"
                                            {foreach $val[3] as $key1 => $val1}
                                        		data-{$key1}="{$val1}" 
                                            {/foreach}
                                            >{$val[0]}</button>
                                        {else if $notifymefeatureon && $notifyMeVariant == 'test'}
                                        <button class="btn size-btn unavailable {if $renderTooltip}vtooltip{/if}" 
                                        	title="Sold out - click to request size"
                                            data-count="{$val[2]}" 
                                            value="{$key}" 
                                            onclick="sizeSelect({$key},{$j},this)" 
                                            data-tooltipText="{$sizeOptionsTooltipHtmlArray[$key]}" 
                                            data-tooltipTop="{$tooltipTop}"
											{foreach $val[3] as $key1 => $val1}
                                        		data-{$key1}="{$val1}" 
                                            {/foreach}
                                            >{$val[0]}
                                            <span class="strike"></span></button>
                                        {/if}
                                        {$j=$j+1}
                                    {/foreach}
                                </div>
                            </div>

							<div class="mk-hide mk-count-message">Only <span>0</span> left in stock</div>
						{/if}
						</div>

							<div class="add-button-group">
								{if $buynowdisabled}
				 					<button class="mk-add-to-cart"> SOLD OUT </button>
				 				{else}
									{if $fromcart eq 1}
										<button class="quick-look-combo-btn btn primary-btn"> Add to Combo </button>
									{else}
				 						<button class="mk-add-to-cart btn primary-btn {if $show_combo && $checkout eq 'test'}mk-add-to-combo{/if}" onclick="_gaq.push(['_trackEvent','buy_now', Myntra.Data.pageName, 'quicklook']);">{$buynowlabel} <span class="proceed-icon"></span></button>
				 						{if $show_combo && $checkout eq 'test'}
											<span class="combo-lbl">AND ADD TO COMBO</span>
					  					{/if}
				 						{if $expressBuyEnabled}<button class="mk-express-buy link-btn">{$expressBuyLabel}</button>{/if}
						            {if $showtelesalesNumber && !$buynowdisabled && $telesalesNumber|trim}
						                <div class="call-to-buy">Or call <em>{$telesalesNumber}</em> to buy</div>
						            {/if}
			 						{if $wishlistenabled && $wishlistVariant == 'test'}
					 					{*<div class="or">or</div>*}
                                        <button class="mk-save-for-later link-btn miniPIP-wishlist">{if not $login}Login to {/if}Save in Wishlist</button>
			 						{/if}

				 					{/if}
								<span class="mk-hide mk-size-error">Select A Size</span>
								{/if}
				 			</div>

				 			{if $SoldByVendorText}
								<div id="vendorDetails">
									<div><span>Sold by</span><span>{$SoldByVendorText}</span></div>
									{if $ShowSuppliedByPartner}
									<div class="supplier-details"><span>Supplied by</span><span>Partner<a data-tooltiptext="{$SuppliedByPartnerTooltip}"> [?] </a></span></div>
									{/if}
								</div>
							{/if}

                                                  <!-- Kuliza Code-->     
                                                  <!-- with sarcity of data on quicklook we are deriving data from few input fields namely productStyleId,
                                                       productStyleName,brand,and image along with the produt page url derived from the anchor tags (h1>a[title="More Details"]).
                                                       please be cautious while changing this fields and ofcourse,if any changes need to be done,check fb-action.js in function
                                                       retrieve_data_from_url() for more details.
                                                  -->
                                                        {include file="socialAction.tpl" echo_flow="pdp" quicklook="yes" popupdiv="true"}    
				 		 <!-- End Kuliza Code-->
                                                        
				 			{if $notifymefeatureon && $notifyMeVariant == 'test'}
				 			<div class="mk-hide notify-cont">
					 			<div id="notify-cont" class="mk-hide notify-cont">
					 				<p>This size is currently out of stock <br />Do you want to be notified when it is available?</p>
					 				<form class="notify-me-form" id="notify-pdp">
					 					<input type="text" name="notifymeEmail" value="{if $login}{$login}{else}Your email address{/if}" class="notify-email {if !$login}empty{/if}" />
					 					<input type="hidden" name="sku" value="" class="notify-sku" />
					 					<input type="hidden" name="pageName" value="minipip">
					 					<input type="hidden" name="_token" value="{$USER_TOKEN}">
						 				<div class="notify-msg"></div>
					 					<div class="notify-btn-grp">
						 					<button class="notify-button btn normal-btn small-btn" >Notify Me</button>
							 				<a href="#" class="notify-cancel" onClick="_gaq.push(['_trackEvent', 'pdp', 'notifyme', 'cancel']);return true;">CANCEL</a>
						 				</div>
					 				</form>
					 			</div>
				 			{/if}
							{else}
						<div class="mk-sold-out">This product is currently sold out.</div>
							{/if}
							</div>
							{else}
						<div class="mk-sold-out">This product is currently sold out.</div>
				{/if}
				{if $fromcart neq 1}
				<a href="" title="More Details" class="mk-more-info" onclick="_gaq.push(['_trackEvent', 'pdp_click', Myntra.Data.pageName, 'quicklook_view_details']);">VIEW FULL PRODUCT DETAILS</a>
				{/if}
					<form action="/mkretrievedataforcart.php?pagetype=productdetail" class="add-to-cart-form" method="post" class="clearfix" id="hiddenFormMiniPIP">
	                   	<input type="hidden" name="_token" value="{$USER_TOKEN}" />
	                    <input type="hidden" name="productStyleId" value="{$productStyleId}">
						{if $fromcart eq 1}
						<input type="hidden" value="{$combouid}" name="combouid">
						{/if}
						<input type="hidden" name="unitPrice" value="{if $discountprice}{$discountprice}{else}{$originalprice}{/if}">
	                	<input type="hidden" value="1" name="quantity">
		                <input type="hidden" class="sizename" value="" name="sizename[]">
		                <input type="hidden" value="1" name="sizequantity[]">
		                <input type="hidden" name="selectedsize" value="">
		                <input type="hidden" name="productSKUID" class="productSKUID" value="{$sku}">
		                <input type="hidden" name="productStyleLabel"  value="{$productDisplayName}">
		                <input type="hidden" name="brand" value="{$brandname}">
		                <input type="hidden" name="articleType" value="{$articleType}">
                                <!-- Kuliza Code Start -->
                                <input type="hidden" name="image" value="{$defaultImage|replace:'_images':'_images_360_480'}">
                                <!-- Kuliza Code End -->
		                {**AVAIL*}
						<input type="hidden" name="t_code"  id="t_code" value="{$t_code}">
						{**END AVAIL*}
						{if $show_combo && $checkout eq 'test' }
                        	<input type="hidden" name="comboProduct" value="1">
                        	<input type="hidden" name="comboId" value="{$discountId}">
                        {/if}
	                 </form>

			{if $fromcart eq 1}
				<div style="margin-top:200px;">
				   &laquo;&nbsp;<a href="#" title="Back To Combo" id="back-to-combo-actn-btn">BACK TO COMBO</a>
				</div>
			{/if}

 			</div>

	</div>
<script>
var gaEventCategory = "{$category}",
    gaEventArticleType = "{$articleType}",
    gaEventGender = "{$gender}",
    gaEventBrand = "{$brand}";
    Myntra.PDP = Myntra.PDP || {};
$(document).ready(function(){

	var text = $(".supplier-details a").data('tooltiptext');
	if(text != "" && typeof text != "undefined") {
    	$('body').append( '<div id="SuplierInfoTooltip" class="myntra-tooltip"><div></div>' + text + '</div>' );
    }
    var node = $('div#SuplierInfoTooltip');
    if(node.length) {
    	$(".supplier-details a").on("mouseover", function(e) {
    		var yOffset = 220,
			elProp = $(e.target).offset(),
	        top = (elProp.top + 20),
	        left = (elProp.left-110);
	        node.css("top", top+"px").css("left", left+"px");
	        node.fadeIn(300);
    	});

    	$(".supplier-details a").on("mouseout", function(e) {
    		node.fadeOut(300);
    	});
    }

    var actionNlabel = getGAActionAndLabel(gaEventCategory,gaEventArticleType,gaEventGender,gaEventBrand);

    Myntra.PDP.gaConversionData = {
        action: actionNlabel.action,
        label: actionNlabel.label
    };
    _gaq.push(['_trackEvent','PDP_conversion', actionNlabel.action, actionNlabel.label]);
});

</script>
