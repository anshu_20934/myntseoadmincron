<footer class="mk-site-ft mk-cf">
<div class="mynt-footer{if !$megaFooterEnabled} mini-ft{/if}">
	{if $megaFooterEnabled}<hr class="main" />{/if}
    <div class="footer-content">
    	{if $megaFooterEnabled}
        <div class="mynt-links-block">
            <div class="mynt-footer-links">
                <ul>
                    <li class="gray">Shop - </li>
                    {foreach key=indexA item=itemsA from=$topNavigationWidget name=topNavigationWidgetLoop}
						<li>
							{if $itemsA.link_name|lower neq "style zone"}
								{if $smarty.foreach.topNavigationWidgetLoop.index gt 0}<span class="l-gray"> / </span>{/if}
								<a href="{$itemsA.link_url}?nav_id={$itemsA.id}&src=ft" {if $itemsA.link_name|lower eq "style blog"}rel="nofollow" target="_blank"{/if}>{$itemsA.link_name}</a>
							{/if}
						</li>
					{/foreach}
					{if $giftingEnabled}
						<li><span class="l-gray"> / </span><a href="/giftcards">Gift Cards</a></li>
					{/if}
				</ul>
                <ul>
                    <li class="gray">Style Zone - </li>
					<li>
						<a href="http://www.stylemynt.com" target="_blank" rel="nofollow">Style Mynt</a>
					</li>
					<li>
						<span class="l-gray"> / </span>
						<a href="/shop/lookgood-helpline">Look Good Helpline</a>
					</li>
				</ul>
                <ul>
                    <li class="gray">Help - </li>
                    <li><a href="/faqs" onClick="_gaq.push(['_trackEvent', 'footer', 'faqs', 'click']);return true;">FAQs</a></li>
                    <li><span class="l-gray"> / </span><a href="/faqs#shipping" onClick="_gaq.push(['_trackEvent', 'footer', 'shippingpolicy', 'click']);return true;">Shipping</a></li>
                    <li><span class="l-gray"> / </span><a href="/faqs#cancel" onClick="_gaq.push(['_trackEvent', 'footer', 'cancelpolicy', 'click']);return true;">Cancellation</a></li>
                    <li><span class="l-gray"> / </span><a href="/faqs#returns" onClick="_gaq.push(['_trackEvent', 'footer', 'returnspolicy', 'click']);return true;">Returns</a></li>
				</ul>
				<ul>
                    <li class="gray">Account - </li>
                    {if not $login}
                    	<li><a class="btn-login foot-login">Login / Sign Up</a><span class="l-gray"> / </span></li>
                    {/if}
                    <li><a href="/mymyntra.php" onClick="_gaq.push(['_trackEvent', 'mymyntra', 'footer', 'click']);return true;">My Myntra</a><span class="l-gray"> / </span></li>
                    {if $login}
	                    <li><a href="/mymyntra.php?view=myprofile#more-info" onClick="_gaq.push(['_trackEvent', 'myprofile', 'footer', 'click']);return true;">My Profile</a><span class="l-gray"> / </span></li>
                    {/if}
                    <li><a href="/mkmycart.php" onClick="_gaq.push(['_trackEvent', 'bag', 'footer', 'click']);return true;">My Shopping Bag</a></li>
                    {if $login}
	                    <li><span class="l-gray"> / </span><a href="/mkmycart.php#wishlist" onClick="_gaq.push(['_trackEvent', 'wishlist', 'footer', 'click']);return true;">My Wishlist</a></li>
                    {/if}
				</ul>
				<ul>
                    <li class="gray">Myntra - </li>
                    <li><a href="/aboutus" onClick="_gaq.push(['_trackEvent', 'footer', 'aboutus', 'click']);return true;">About Us</a></li>
                    <li><span class="l-gray"> / </span><a href="/careers" onClick="_gaq.push(['_trackEvent', 'footer', 'careers', 'click']);return true;">Work With Us</a></li>
                    <li><span class="l-gray"> / </span><a href="http://blog.myntra.com/" onClick="_gaq.push(['_trackEvent', 'footer', 'corporateblog', 'click']);return true;" target="_blank">Corporate Blog</a></li>
                    <li><span class="l-gray"> / </span><a href="/security/whitehat" onClick="_gaq.push(['_trackEvent', 'footer', 'whitehat', 'click']);return true;" target="_blank">Responsible Disclosure Policy</a></li>
                </ul>
            </div>
       	  	<div class="contact-block">
                <strong>Need Help?</strong>&nbsp;&nbsp;&nbsp;<a class="btn small-btn normal-btn" href="/contactus">Contact Us</a>
				{if $megaFooterEnabled}
                    <div class="social-buttons">
						Follow Us&nbsp;&nbsp;
                        <a href="http://www.facebook.com/myntra" target="_blank" class="fb" onClick="_gaq.push(['_trackEvent', 'footer', 'fb-myntra', 'click']);return true;">&nbsp;</a>
                        <a href="http://twitter.com/myntra" target="_blank" class="tw" onClick="_gaq.push(['_trackEvent', 'footer', 'twitter-myntra', 'click']);return true;">&nbsp;</a>
                        <a href="http://pinterest.com/myntra/" target="_blank" class="pt" onClick="_gaq.push(['_trackEvent', 'footer', 'pinterest-myntra', 'click']);return true;">&nbsp;</a>
                        <a href="http://www.youtube.com/user/myntradotcom" target="_blank" class="yt" onClick="_gaq.push(['_trackEvent', 'footer', 'yt-myntra', 'click']);return true;">&nbsp;</a>
                        <a href="https://plus.google.com/109282217040005996404/posts" target="_blank" class="gp" onClick="_gaq.push(['_trackEvent', 'footer', 'gp-myntra', 'click']);return true;">&nbsp;</a>
                    </div>
	            <div class="social-block">
					<div class="fb-like-button"></div>
					<div class="tw-tweet-button inline-block"></div>
					<div class="gp-plusone-button"></div>
                 </div>
               	{/if}
            </div>
		</div>
		<div>            
            <div class="mynt-footer-promos mk-cf">
                <div class="promo-block mk-f-left">
                    <div class="promo ">
                    	{$totalNumStyles} Styles<br>
                        {$totalNumBrands} Brands
                        <span class="promo-small-text">And Counting!</span>
                    </div>
                    <div class="promo-leaf" >
                        We have the largest collection of the latest fashion and lifestyle products in India!
                    </div>
                </div>

                <div class="promo-block mk-f-left">
                    <div class="pipe">
                        <div class="certified-icon">&nbsp;</div>
                    </div>
                    <div class="promo-leaf">
                        All our products are brand new and 100% original, and are checked for quality before shipping.
                    </div>
                </div>

                <div class="promo-block mk-f-left mk-last">
                    <div class="promo ">
                    	Free 30-day<br>
                    	Returns
                        <span class="promo-small-text">Right from your doorstep</span>
                    </div>
                    <div class="promo-leaf" >
                        Our easy and simple returns process is sure to delight you.
                    </div>
                </div>
            </div>
        </div>
    	{/if}
		{if $megaFooterEnabled}
	        <div class="mynt-text">
	            <div class="mynt-foot-title">ABOUT MYNTRA.COM</div>
	            Myntra.com is India's largest online fashion and lifestyle store for men, women, and kids.<br>
	            Shop online from the latest collections of apparel, footwear and accessories, featuring the best brands.<br>
	            We are committed to delivering the best online shopping experience imaginable.<br>
	        </div>

	        {if $mostPopularWidget}
		        <div class="mynt-tags">
			        {foreach name=tagtype item=fixedorDynamic from=$mostPopularWidget}
			        	{foreach name=uniquetag item=tag from=$fixedorDynamic}
		        	    	<a href="/{$tag.link_url}?src=ft" onClick="_gaq.push(['_trackEvent', 'footer', '{$tag.link_url}', 'click']);return true;">{$tag.link_name}</a>
			        	    {if !$fixedorDynamic@last || !$tag@last}, {/if}
			            {/foreach}
			        {/foreach}
		        </div>
	        {/if}
		{/if}

        <div class="payment-block {if $emiEnabled}emi{/if}">
            <div class="payment">
            	<div class="payment-icons"></div>
           	</div>
			{if !$megaFooterEnabled}<div class="contact gray">
				Need Help?<br />
				<span class="d-gray">{$customerSupportCall}</span><span class="l-gray"> / </span>
				<a href="mailto:support@myntra.com" class="maillink red" onClick="_gaq.push(['_trackEvent', 'footer', 'mailink', 'click']);return true;">support@myntra.com</a>
			</div>{/if}
        </div>

        <div class="mynt-copy-right">
            &copy; myntra.com<span class="l-gray"> / </span>All rights reserved<span class="l-gray"> / </span><a href="/privacypolicy" onClick="_gaq.push(['_trackEvent', 'footer', 'privacypolicy', 'click']);return true;">Our Privacy Policy</a><span class="l-gray"> / </span><a href="/termsofuse" onClick="_gaq.push(['_trackEvent', 'footer', 'termsofuse', 'click']);return true;">Our Terms of Use</a>
        </div>
        {if $showMobileSiteLink}
            <div><a href="#" onclick="javascript:document.location.href='{$http_location}/udvo?redirecturl='+document.location">View Mobile Site</a></div>
        {/if}
    </div>
</div>

<div id="fb-root"></div>

<!-- @Footer-->


</footer>
