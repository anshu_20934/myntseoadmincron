<header class="mk-site-hd mk-cf{if $checkoutHeader} mk-checkout-hd{/if}">
   <script>
           {if $usern != "" || $passwd != ""}
                 var showLogin = true;
           {/if}
           </script>
{if $telesales}
     <script>
       var telesales = true;
     </script>
     <div style="color: #fff;background-color: #333333 ;text-transform:none;padding:18px 0px;">Telesales User: <b>{$telesalesUser}</b></div>
{else}
     <script>
        var telesales = false;
     </script>
{/if}
                                                      
 	<div class="customer-promises mk-cf">
	    <div class="mk-f-left">
   	        {if $contactUsLink && !$checkoutHeader}
   	            {$contactUsLink}{" / "}{$myOrdersLink}
   	        {else}
   	            Need Help? <em>{$customerSupportCall}</em>
            {/if}
   	    </div>
   	    <div class="mk-f-right mk-cf" id="notificationWrapper">
			{if $notificationsEnabled && $notificationCount > 0}
				<a href='javascript:void(0);' id='mk-notification'>
					{if $notificationCount}
						<span class='newCount'>{$notificationCount}</span>
					{/if}
					<div class="mk-arrow"><span></span></div>					
				</a>
				{if $newNotificationCount > 0}
					<div class="myntra-tooltip feature-tooltip" id="notif-tooltip">
						{$newNotificationCount} new offer{if $newNotificationCount neq 1}s{/if} for you<span class="close"></span>
						<div class="arrow-right-inner arrow"></div>					
					</div>
					<div class="shim"></div>
				{/if}
				<div id="mk-notification-popup">
					<div class="preloaderWrapper">
							<img src="{if $smarty.server.HTTPS}{$secure_cdn_base}{else}{$cdn_base}{/if}/skin2/images/loader_180x240.gif" alt="Loading" />
							<h1>No Notifications Exist</h1>
					</div>
					<div class="mk-ntf-box">
						<h1>NOTIFICATIONS</h1>
						<div class='mk-ntf-category'>
							<h2>OFFERS</h2>
							<ul id='notifications'>
								<li class='mk-hide clear mockElement'>
									<img src='{if $smarty.server.HTTPS}{$secure_cdn_base}{else}{$cdn_base}{/if}/skin2/images/spacer.gif' alt='Image'/>
									<h3></h3>
									<p></p>
									<a href='javascript:void(0);' title='Delete' class='ntfDelete'>X</a>
								</li>								
							</ul>
							<a href='javascript:void(0);' class='viewAll blueLink'>View All</a>
							<a href='javascript:void(0);' class='moreData blueLink'>more</a>
						</div>
					</div>
				</div>
			{/if}
		</div>
   	    <div class="mk-f-right{if $notificationsEnabled && $notificationCount > -1} mk-ntf-last{/if}">
 			{$pn = $smarty.server.REQUEST_URI|regex_replace:"/\?.*$/":""}
 	   		{if $login}
	   	    	{if !$checkoutHeader}<span{if $notificationsEnabled && $newNotificationCount > 0} class="mk-hide"{/if}>
	   	    		{$promoHeaderLoggedinText} <div class="icon-arrow-right"></div>
	   	    	</span>{/if}
	   	    	Welcome, <span class="d-gray">{if $userfirstname neq ""}{$userfirstname|truncate:50}{else}{$login|truncate:50}{/if}</span>!
	   	    	<a id="header-logout-link" class="mk-sign-out d-gray">Logout</a>
			    <form id="header-logout-form" method="post" action="{if isset($is_secure)}{$https_location}{else}{$http_location}{/if}/include/login.php">
			        <input type="hidden" name="mode" value="logout">
			        <input type="hidden" name="_token" value="{$USER_TOKEN}">
			    </form>
			{elseif $pn neq "/register.php" && $pn neq "/securelogin.php"}
				{if !$checkoutHeader}{$promoHeaderText} <div class="icon-arrow-right"></div>{/if}
				<a class="btn-login d-gray">Login / Sign Up</a>
				<input type="hidden" id="loginSplashVariant" value="{$loginSplashVariant}" />
			{/if}			
		</div>		
   	</div>
	
	<h1 class="col">
		<a href="/">
			<img src="{if $smarty.server.HTTPS}{$secure_cdn_base}{else}{$cdn_base}{/if}/skin2/images/MyntraLogo.png"
				alt="Myntra" height="52" title="Online Shopping store for Branded Shoes, Clothing, Accessories & Fashion wear in India." />
		</a>
	</h1>
	
	{if $checkoutHeader}
        {include file="inc/header-checkout-steps.tpl"}
	    <div class="checkout-promises">
    	    <div class="mk-promises gray">{$newUiTopCpsMessage}</div>
	        <div class="mk-secure"><span class="lock-icon"></span>&nbsp;256-bit ssl secure checkout</div>
	    </div>

    {else}
		<form class="mk-search-form mk-cf col" action="" method="" onsubmit="return menuSearchFormSubmit(this);">
			<input type="text" value="" autocomplete="off" class="mk-site-search mk-placeholder mk-f-left" id="search_query" />
			<span class="mk-placeholder-text">{$searchBoxText}</span>
			<button type="submit" name="" class="mk-site-search-btn mk-f-left"></button>
		</form>
		
		<ul class="mk-utility col">
			{if $showRecent && !$recentViewedCount eq 0}
				<li class="{if !$mymyntra}mk-header-hover{/if}"><a href="{$http_location}/recently-viewed">
					<em class="mk-recent " data-id="recent" data-show="{if $recentViewedCount eq 0}false{else}true{/if}" data-load="true">Recently Viewed
						<div class="mk-arrow mk-hide"><span></span></div>
					</em>
				</a></li>
			{/if}
			{if $login}
				{*if $lbId gt 0*}
				{if 1 eq 2}
					<li class="mk-header">
						<em class="mk-mymyntra" style="text-decoration:none;cursor:auto;">Happy Hour</em>
						<span class="mk-count">
							<a href="{$http_location}/leaderboard.php?lbId={$lbId}">Leader Board</a>
						</span>
					</li>
				{/if}
			{/if}
			<li class="{if !$mymyntra}mk-header-hover{/if}"><a href="{$http_location}/mymyntra.php"{if !$login} class="btn-login"{/if}>
				<em class="mk-mymyntra" data-id="mymyntra" data-show="{if $login}true{else}false{/if}" data-load="true">My Myntra
					<div class="mk-arrow mk-hide"><span></span></div>
				</em>
				{if $login}
				    {if $enableHeaderMRPData}
						{if  $myntCashDetails.balance > 0}<span class="mk-count rupees">{$rupeesymbol} {$myntCashDetails.balance|round|number_format:0:".":","}</span>
						{elseif $loyaltyEnabled and $loyaltyPointsHeaderDetails.tNcAccepted and $loyaltyPointsHeaderDetails.activePointsBalance > 0}<span class="mk-count">{$loyaltyPointsHeaderDetails.activePointsBalance} {if $loyaltyPointsHeaderDetails.activePointsBalance eq 1}PT {else}PTS{/if}</span>
						{else}<span class="mk-count">{$myMyntraCredits.couponsCount} {if $myMyntraCredits.couponsCount eq 1}Coupon {else}Coupons{/if}</span>
						{/if}
					{else}
						<span class="mk-count">&nbsp</span>
					{/if}
				{else}<span class="mk-count">Login</span>
				{/if}
			</a></li>
			<li class="{if !$mymyntra}mk-header-hover{/if}"><a href="{$cartPageUrl}#wishlist"{if !$login} class="btn-login"{/if}>
				<em class="mk-saved " data-id="saved" data-show="{if $savedItemCount eq 0}false{else}true{/if}" data-load="true">My Wishlist
					<div class="mk-arrow mk-hide"><span></span></div>
				</em>
				<span class="mk-count">
					{if !$savedItemCount eq 0}[<strong>{$savedItemCount}</strong>]
					{elseif $login}Empty
					{else}Login
					{/if}
				</span>
			</a></li>
			<li class="{if !$mymyntra}mk-header-hover{/if} bag"><a href="{$cartPageUrl}">
				<div class="bag-cont">
					<em class="mk-bag " data-id="bag" data-show="{if $cartTotalQuantity eq 0}false{else}true{/if}" data-load="true">My Bag
						<div class="mk-arrow mk-hide"><span></span></div>
					</em>
					<span class="mk-count">
						{if !$cartTotalQuantity eq 0}
							<span class="rupees red">{$rupeesymbol} {$totalAmountCart|round|number_format:0:".":","}</span>
							<i class="mk-hide"> Empty</i>
							<span><strong>{$cartTotalQuantity}</strong></span>
						{else}
							<span class="rupees red mk-hide"></span>
							<i> Empty</i><strong>{$cartTotalQuantity}</strong>
						{/if}
					</span>
				</div>
				<span class="bag-icon"></span>
			</a></li>
		</ul>
	{/if}	
</header>