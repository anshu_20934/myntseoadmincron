<script type="text/javascript">
    var gaSuffix = "{$productTypeLabel}_{$brandname}";
</script>
<div class="product-detail-block last right">
    {if $fromcart eq 1}
        <div class="clearfix back-to-combo-offer">
            <a href="javascript:void(0)" id="back-to-combo-actn-btn">
                &laquo; Back to Special Combo Offer details </a>
        </div>
    {/if}
    <div class="mt10 product-detail-block-left left">
        <h1 class="product-title">{$productDisplayName}</h1>
        <div class="pricing-info mt10">
            {if $discountamount > 0}
                <span class="dprice"><span>Rs.</span> {if $discountamount > 0}{math equation="x - d" x=$originalprice d=$discountamount assign="finalamount"}{$finalamount|round|number_format:0:".":","}{else}{$originalprice|round|number_format:0:".":","}{/if}</span>
                <span class="oprice strike"> {$originalprice|round|number_format:0:".":","}</span>
            {else}
                <span class="dprice"><span>Rs.</span> {$originalprice|round|number_format:0:".":","}</span>	
            {/if}
        </div>
        {if !$disableProductStyle}
            {if $discountlabeltext}
                <div class="clearfix mt5 discount-block" style="border:0px none">
                    <div class="pdp-discount-align pdp-sploff pdp-trigger" style="clear:none;">
                  {include file="string:{$discountlabeltext}"} </div>
                </div>
            {else}
                <div class="add-pricing-info mb10">
                    <div class="hide" style={if $cashback_gateway_status eq 'on' && $enableCBOndiscountedProducts eq 'true' && $cashback_displayOnPDPPage eq '1'}"display:block;"{elseif $cashback_gateway_status eq 'on' && $enableCBOndiscountedProducts eq 'false' && ($discountamount eq '' || $discountamount eq 0) && $cashback_displayOnPDPPage eq '1'}"display:block;"{else}"display:none;"{/if}>
                        And get Rs.
                        <span class="bold"> {$cashback|round|string_format:"%.2f"}</span> (<span class="bold">10%</span>) Cashback
                    </div>
                </div>
            {/if}
            {if $sizeOptionSKUMapping}	
            <div class="clearfix flat-size-options quick-look-flat-size-options">
                <span>Select Size: </span><br/>
                <div class="size-options" id="size-options">
                    {assign var='j' value=1}
                    {foreach from=$sizeOptionSKUMapping key=key item=val}
                        {if $renderTooltip}
                            <a href="javascript:void(0)" class="action-btn pdp-action-btn vtooltip" rel="{$sizeOptionsTooltipHtmlArray[$key]}" data-tooltipTop="{$tooltipTop}" onclick="Myntra.MiniPDP.sizeSelect({$key},{$j},this)">{$val}</a>
                        {else}
                            <a href="javascript:void(0)" class="action-btn pdp-action-btn" rel="" onclick="Myntra.MiniPDP.sizeSelect({$key},{$j},this)">{$val}</a>
                        {/if}
                        {assign var='j' value=$j+1}
                    {/foreach}
                    <div class="size-options-error-msg quick-look-size-options-error-msg clear" style="display:none;">Please select a size.</div>
                </div>
            </div>
            {/if}

       

            <div class="clearfix mt10 buy-now-block"> 
                <span class="left">
                {if $fromcart eq 1}
                    {literal}<a class="quick-look-combo-btn orange-bg cart-btn corners span-6 left clearfix">Add To Combo &raquo;</a>{/literal}
                {else}
                    {if $buynowdisabled}
                        <a class="btn-disabled corners span-6 left clearfix">Buy now</a>
                    {else}
                        {literal}<a class="orange-bg quick-look-cart-btn cart-btn corners span-6 left clearfix">Buy now &raquo;</a>{/literal}
                    {/if}
                {/if}
                {if $buynowdisabled}
                    <div class="out-of-stock-msg clearfix">Currently this product is sold out.</div>
                {/if}
                </span>
                {if $showtelesalesNumber neq 'control' && !$buynowdisabled && $fromcart neq 1}
                    <span>or <b style="color:#ff9412">call {$telesalesNumber}</b> <br> to place an order</span>
                {/if}
            </div>
            {if !$buynowdisabled && $fromcart neq 1}
            <div class="view-more-details clearfix">
                <div class="span-6">
                    <span>or view</span>
                    <a class="more-details action-btn">MORE DETAILS</a>
                </div>
            </div>
            {/if}
        {else}
            <div class="out-of-stock-msg clearfix">Currently this product is sold out.</div>
        {/if}
    </div>
</div>
