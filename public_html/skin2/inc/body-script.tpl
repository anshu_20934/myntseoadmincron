 {if !$smarty.server.HTTPS}
    <script> 
        window['__myx_session__'] = {$userData};
        window['__myx_features__'] = {$features};
        window['__myx_kvpairs__'] = {$kvpairs};
        window['__myx_ab__'] = {$googleJsonData};
    </script> 
    <script src="{$nodescript}"></script>
{/if}

{foreach from=$jsFiles item=href}
    <script src="{$href}"></script>
{/foreach}
 
{if !$smarty.server.HTTPS}
    <script>
        {literal}
            if (window.components) {
                window.components.header && window.components.header({el:$('.header-cnt')[0],pagename:Myntra.Data.pageName});
                window.components.login && window.components.login({ login: __myx_session__.login, isRegistered: __myx_session__.isRegistered});
                if (__myx_session__.loginEnabled) {
                    if (__myx_session__.isRegistered) {
                        $('.header-cnt .banner .shortcuts a[href="/register.php"]').attr('href', '#signup').text('Create Account');
                        $('.header-cnt .header .actions a.big.fill[href="/register.php"]').attr('href', '#signin').html(function (i, html) {
                            return html.replace('Create Account', 'Login');
                        });
                    } else {
                        $('.header-cnt .banner .shortcuts a[href="/register.php"]').attr('href', '#signin').text('Login');
                        $('.header-cnt .header .actions a.big.fill[href="/register.php"]').attr('href', '#signup').html(function (i, html) {
                            return html.replace('Login', 'Create Account');
                        });       
                    }
                }
            } else {
                window.Header && window.Header({el:$('.header-cnt')[0],pagename:Myntra.Data.pageName});
            }
        {/literal}
    </script>
{/if}
{if $analyticsStatus}
	{include file="inc/analytics_data_collection.tpl"}
{/if}

<script>
    {if $lt_new_session eq "true"}
    {/if}
    {if $_MAB_GA_calls}
        {$_MAB_GA_calls}
    {/if}
    _gaq.push(['_trackPageview']);
    _gaq.push(['_trackPageLoadTime']);

</script>


<!--[if lt IE 8]>
	<script src="/skin2/js/json2.js"></script>
<![endif]-->

{include file="inc/newrelic-footer.tpl"}

{if $tracelytics_footer}
    {$tracelytics_footer}
{/if}

{include file="inc/webengage.tpl"}

{foreach from=$prefetchFiles item=prefetchFile}
    {$prefetchFile}
{/foreach}
