<div class="mk-pdp-carousel-sub-items">
	{include file="header/item-carousel.tpl"}
</div>
<div class="mk-pdp-carousel-shop-by mk-cf">
	<div class="mk-pdp-carousel-shop-by-text mk-f-left">
		<span class="pdp-shop-by-btn">shop by<a href="#" class="jqTransformSelectOpen"></a></span>
       	{*include file="pdp/mini-nav.tpl"*}
   	</div>
	<div class="mk-pdp-carousel-breadcrumbs mk-f-left">
	{if $pageTypePdp == 'pdp'}
		<a href="{$http_location}">HOME</a> <span class='sep'>/</span>
	{/if}
	{$navString}
	{*foreach key=navitemidx item=navitem from=$nav_selection_arr_url}
    		{if $navitem@first}<a href="{$http_location}">HOME</a>{/if}
    		/ <a href="{$navitem}">{$navitemidx}</a>
    	{/foreach*}
    </div>
</div>