<?php 
////////////// Myntra ///////////////
// Product-Filters
/////////////////////////////////////
?>
	


		<div class="mk-product-filters">
			<form>
		            <label class="mk-label_check" for="checkbox-01"><input name="features" id="checkbox-01" value="new" type="checkbox" checked /> New Arrivals</label><span class="mk-count">65</span>
		            <label class="mk-label_check" for="checkbox-02"><input name="features" id="checkbox-02" value="sale" type="checkbox" /> Sale</label><span class="mk-count">65</span>
		            <label class="mk-label_check" for="checkbox-03"><input name="features" id="checkbox-03" value="top" type="checkbox" /> Top Rated</label><span class="mk-count">88</span>
		            <label class="mk-label_check" for="checkbox-04"><input name="features" id="checkbox-04" value="exclusives" type="checkbox" /> Exclusives</label><span class="mk-count">88</span>
				
				<ul class="mk-product-sort mk-f-right">  <!-- Note: different option? -->
					<li class="mk-title"> Sort: </li>
					<li class="mk-price"> <a href="#">Price</a></li>
					<li class="mk-popularity"> <a href="#">Popularity</a></li>
					<li class="mk-brand"> <a href="#">Brand</a></li>
					<li class="mk-color"> <a href="#">Color</a></li>
				</ul> <!-- End .product-sort -->

				<ul class="mk-product-selectors">
					<li><a href="" class="mk-a-label">Size <span class="mk-label-arrow"></sapn></a>
						<div class="mk-filter-wrapper mk-hide">
							<label class="mk-labelx_check" for="size-1"><input name="features" id="size-1" value="1" type="checkbox" checked />S</label>
							<label class="mk-labelx_check" for="size-2"><input name="features" id="size-2" value="2" type="checkbox"  />M</label>
							<label class="mk-labelx_check" for="size-3"><input name="features" id="size-3" value="3" type="checkbox"  />L</label>
							<label class="mk-labelx_check" for="size-4"><input name="features" id="size-4" value="4" type="checkbox"  />XL</label>
						</div>
					</li>
					<li><a href="" class="mk-a-label">Brand  <span class="mk-label-arrow"></sapn> </a>
						<div class="mk-filter-wrapper mk-hide">
							<label class="mk-labelx_check" for="brand-1"><input name="features" id="brand-1" value="1" type="checkbox" checked />Brand 1</label>
							<label class="mk-labelx_check" for="brand-2"><input name="features" id="brand-2" value="2" type="checkbox"  />Brand 2</label>
							<label class="mk-labelx_check" for="brand-3"><input name="features" id="brand-3" value="3" type="checkbox"  />Brand 3</label>
							<label class="mk-labelx_check" for="brand-4"><input name="features" id="brand-4" value="4" type="checkbox"  />Brand 4</label>
						</div>	
					</li>
					<li><a href="" class="mk-a-label">Price  <span class="mk-label-arrow"></sapn></a>
						<div class="mk-filter-wrapper mk-hide">
							<div class="mk-price-range"></div>
							<input type="text" class="mk-amount" />
						</div>	
					</li>
					<li><a href="" class="mk-a-label">Color  <span class="mk-label-arrow"></sapn></a>
						<div class="mk-filter-wrapper mk-hide">
							<label class="mk-labelx_check" for="color-1">
								<input name="features" id="color-1" value="1" type="checkbox" checked />
									<img src="/skin2/images/color-white.png" class="mk-color"/>
									White
							</label>
							<label class="mk-labelx_check" for="color-2">
								<input name="features" id="color-2" value="2" type="checkbox"  />
									<img src="/skin2/images/color-beige.png" class="mk-color"/>
									Beige
							</label>
							<label class="mk-labelx_check" for="color-3">
								<input name="features" id="color-3" value="3" type="checkbox"  />
									<img src="/skin2/images/color-orange.png" class="mk-color"/>
									Orange
							</label>
							<label class="mk-labelx_check" for="color-4">
								<input name="features" id="color-4" value="4" type="checkbox"  />
									<img src="/skin2/images/color-periwinkle.png" class="mk-color"/>
									Periwinkle
							</label>
						</div>	
					</li>
					<li><a href="" class="mk-a-label">Occasion  <span class="mk-label-arrow"></sapn></a>
					</li>
					<li class="mk-last"><a href="" class="mk-a-label">Style  <span class="mk-label-arrow"></sapn> </a>
					</li>					
				</ul> <!-- End .product-selectors --> 
			</form>
		</div> <!-- End .product-filters -->