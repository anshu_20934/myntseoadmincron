<div id="nudge">
<div class="nudgeOutter">
    <div class="nudgeOffer">
        <div class="lightText">{$nudgeMessage[0]}</div>
        <div class="highlightedText">{$nudgeMessage[1]}</div>
    </div>
    <div class="nudgeIconArea">
        <div class="cross">
        
        </div>
    </div>
</div>
<div class="dockicon collapsed">
 <div class="nudgeStar"></div>
</div>
</div>

