
    <meta charset="utf-8">
    <meta name="google-signin-clientid" content="787245060481-4padi0c9g2l1pnbie52fmpfdpo43209i.apps.googleusercontent.com">
    <meta name="google-signin-cookiepolicy" content="single_host_origin">
    <meta name="google-signin-scope" content="https://www.googleapis.com/auth/plus.me email">
    <meta name="google-signin-prompt" content="select_account">

    {* Note: This flag is set in /myntra/search.php to stop the bots indexing improper list pages *}

    {if isset($seoStrategy)}
        <!-- using new seo -->
        {$seoStrategy->getBaseWidget()}
    {else}
        <meta name="description" content="{$metaDescription|escape}">
        <meta name="keywords" content="{$metaKeywords|escape}">
        <title>{if $pageTitle ne ''}{$pageTitle}{elseif $pageHeader ne ''}{$pageHeader}{else}Myntra{/if}</title>
    {/if}

    {if $noindex eq 2 or $noRobotIndexing or $metaTagNoindex}
    <meta name="robots" content="noindex, nofollow">
    {/if}

    {if $smarty.server.HTTPS}
        <link rel="dns-prefetch" href="{$secure_cdn_base}">
        <link rel="dns-prefetch" href="https://www.googletagmanager.com">
        <link rel="shortcut icon" href="{$secure_cdn_base}/skin1/icons/favicon.ico">
    {else}
        <link rel="dns-prefetch" href="{$cdn_base}">
        <link rel="dns-prefetch" href="http://www.googletagmanager.com">
        <link rel="shortcut icon" href="{$cdn_base}/skin1/icons/favicon.ico">
    {/if}
    {* SOCIAL *}
    <link href="https://plus.google.com/109282217040005996404" rel="publisher" />
    <meta property="fb:admins" content="520074227">
    <meta property="fb:app_id" content="{$facebook_app_id}"> 
    <meta property="og:site_name" content="Myntra">
    <meta property="og:type" content="{$facebook_app_object_name}">

    {block name=baseHttpLoc}{/block}

    {if isset($seoStrategy)}
        {$ogWidget = $seoStrategy->getOGWidget()}
        {$ogWidget}
        <meta name="twitter:site"     content="@myntra">
        {$seoStrategy->getTwitterWidget()}
    {/if}
    {if !isset($ogWidget)}
        {if $pageTypePdp}
            <meta property="og:title" content="{$pageTitle|trim}">
            <meta property="og:url" content="{$pageCleanURL}">
            <meta property="og:image" content="{$defaultImage|replace:'_images':'_images_360_480'}">
            <meta property="og:description" content="{$fb_meta_decription|escape}">
            <link rel="image_src" href="{$defaultImage|replace:'_images':'_images_96_128'}">
        {elseif $lookBookName}
            <meta property="og:title" content="{$lookOGTags.lookName}">
            <meta property="og:url" content="{$lookOGTags.lookURL}">
            <meta property="og:image" content="{$lookOGTags.lookImage}">
            <meta property="og:description" content="{$lookOGTags.lookDescription}">
        {else}
            <meta property="og:type" content="website">
            <meta property="og:title" content="{$pageTitle|trim}">
            <meta property="og:url" content="http://www.myntra.com">
            <meta property="og:image" content="{$cdn_base}/skin2/images/logo.png">
            <link rel="image_src" href="{$cdn_base}/skin2/images/logo.png">
        {/if}
    {/if}


    {* end of SOCIAL *}
    
    {if $pageTypeSearch}
        {if $showCanonicalTag}
        <link rel="canonical" href="{$canonicalPageUrl}">
        {/if}
        {if $previousLinkUrl }
        <link rel="prev" href="{$previousLinkUrl}"> 
        {/if}
        {if $nextLinkUrl}
        <link rel="next" href="{$nextLinkUrl}">
        {/if}
    {elseif $canonicalPageUrl}
        <link rel="canonical" href="{$canonicalPageUrl}">
    {/if}
