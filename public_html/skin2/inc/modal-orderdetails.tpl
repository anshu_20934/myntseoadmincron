<?php 
////////////// Myntra ///////////////
// Modal - Order Details
/////////////////////////////////////
?> 


<div class="mk-order-wrapper mk-hide">
	<div class="mk-order-modal">
		<span class="mk-modal-close">X</span>

		<h1> Order#1234567</h1>
		<h6> Placed on 15 Nov, 2011. Estimated Arrival Date 30 Nov, 2011</h6>

		<div class="mk-order-item">
			<div class="mk-product-details mk-f-right">
				<h5>Product Code: 173506</h5> <!-- Product Code -->
				<h1>Lanvin Leather and Suede Sneaker</h1>  <!-- Product Name -->
				<p class="mk-discount"> MRP RS 11995.00 (10% Off) </p> <!-- Product Discount --> 
				<h3> RS 10796 </h3>  <!-- Product Price -->
				<p> Size: XL / Color: Brown &amp; White </p>
			</div> <!-- End .product-details -->
			<img src="/skin2/images/fpo/fpo-order-items-1.jpg" alt="" />
		</div>
		<div class="mk-order-item">
			<div class="mk-product-details mk-f-right">
				<h5>Product Code: 173506</h5> <!-- Product Code -->
				<h1>Maison Suede Low-Top Sneakers</h1>  <!-- Product Name -->
				<p class="mk-discount"> MRP RS 11995.00 (10% Off) </p> <!-- Product Discount --> 
				<h3> RS 10796 </h3>  <!-- Product Price -->
				<p> Size: XL / Color: Blue &amp; Black </p>
			</div> <!-- End .product-details -->
			<img src="/skin2/images/fpo/fpo-order-items-2.jpg" alt="" />

		</div>
		<div class="mk-order-item">
			<div class="mk-product-details mk-f-right">
				<h5>Product Code: 173506</h5> <!-- Product Code -->
				<h1>Jimmy Choo Suede Sneakers</h1>  <!-- Product Name -->
				<p class="mk-discount"> MRP RS 11995.00 (10% Off) </p> <!-- Product Discount --> 
				<h3> RS 10796 </h3>  <!-- Product Price -->
				<p> Size: XL / Color: Black </p>
			</div> <!-- End .product-details -->
			<img src="/skin2/images/fpo/fpo-order-items-3.jpg" alt="" />
		</div>

		<div class="mk-order-totals">
			<p>Order Total:</p>
			<h4>RS12896</h4>
			<p>Est. Shipping & Handling<p>
			<h4>RS 0</h4>
			<p>Discount<p>
			<h1>RS 500 Mynt Club Bonus</h1>
			<p>Total</p>
			<h4>RS 12396</h4>
		</div>
	</div> <!-- End .order-modal -->
</div> <!-- End .order-wrapper --> 

