{if $layoutVariant eq 'test'}
	<div class="mk-pdp-carousel-sub-items">
		{include file="header/item-carousel.tpl"}
	</div>
{/if}

<div class="mk-carousel-items">
	{if $layoutVariant eq 'test'}
		{include file="inc/nav-new.tpl"}
	{else}
		{include file="inc/nav.tpl"}
		{include file="header/item-carousel.tpl"}
	{/if}
</div>