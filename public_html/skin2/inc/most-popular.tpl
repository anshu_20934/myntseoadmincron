{assign var=wi value=0}

	{assign var=i value=1}
	{foreach name=widgetdataloop item=widgetdata from=$widget.data}
		{if $widgetdata.product|trim neq ''}
		 	{if $recent_viewed_count >= 5 || $i <= 5}
				<li data-id="prod_{$widgetdata.id}" class="rs-carousel-item {if $i <= $recent_viewed_count}recent-inline recent-inline-load{/if}">
			
					{if $quicklookenabled}
						<span class="quick-look" data-widget="{if $ga_component_name}{$ga_component_name}{else}most_popular_page{/if}" data-styleid="{$widgetdata.styleid}" data-href="{$http_location}/{$widgetdata.landingpageurl}" href="javascript:void(0)"></span>
					{/if}
			
					<a title="{$widgetdata.product}" href="{$http_location}/{$widgetdata.landingpageurl}" onclick="_gaq.push(['_trackEvent', 'pdp_click', Myntra.Data.pageName, '{if $ga_component_name}{$ga_component_name}{else}most_popular_page{/if}']);">
						<div class="mk-product-image">
							<img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src='{myntraimage key="style_150_200" src=$widgetdata.search_image|replace:"_images_180_240":"_images_150_200"|replace:"_images_240_320":"_images_150_200"|replace:"style_search_image":"properties"}' alt="{$widgetdata.product}" />
						</div>
					</a>
					
					<p class="brand-text">{$widgetdata.global_attr_brand|truncate:20}</p>
					<p class="price-text red">{$rupeesymbol}{if $widgetdata.discount > 0}{$widgetdata.discounted_price|number_format:0:".":","} <span class="strike gray">{$widgetdata.price|number_format:0:".":","}</span>{else}{$widgetdata.price|number_format:0:".":","}{/if}</p>
					{if $widgetdata.discount_label}<p class="price-text red-text red">{include file="string:{$widgetdata.discount_label}"}</p>{/if}

					{if $i <= $recent_viewed_count}
						<div class="inline-add-block">
							<button class="inline-add icon-buynow mk-hide" onclick="_gaq.push(['_trackEvent','buy_now', Myntra.Data.pageName, '{if $ga_component_name}{$ga_component_name}{else}most_popular_page{/if}']);"></button>
			
							{if $widgetdata.sizes_facet eq 'Freesize'}
								{assign var=size_selected value=$widgetdata.sizes_facet}
							{/if}
							
							<form action="/mkretrievedataforcart.php?pagetype=productdetail" class="inline-add-to-cart-form" method="post" class="clearfix">
								   	<input type="hidden" name="_token" value="{$USER_TOKEN}" />
									<input type="hidden" name="productStyleId" value="{$widgetdata.styleid}">
									<input type="hidden" value="1" name="quantity">
									<input type="hidden" class="sizename" value="" name="sizename[]">
									<input type="hidden" value="1" name="sizequantity[]">
									<input type="hidden" name="selectedsize" value="">
									<input type="hidden" name="pageName" value="pdp">
									<input type="hidden" name="widgetType" value="recently_viewed">
									<input type="hidden" name="productSKUID" class="productSKUID" value="{if $size_selected}{$size_selected}{/if}">
									<input type="hidden" name="productStyleLabel"  value="{$widgetdata.stylename}">
									<input type="hidden" name="brand" value="{$widgetdata.global_attr_brand}">
									<input type="hidden" name="articleType" value="{$widgetdata.global_attr_article_type}">
							</form>
						</div>
		   				{if  $recent_viewed_count < 5 && $i <= 5}{assign var=wi value=$wi+190}{/if}

	   				{elseif $widgetdata.generic_type neq 'design'}
						<div class="availability" data-sizes="{if $widgetdata.sizes neq '' && $widgetdata.sizes neq ' '}Sizes: {$widgetdata.sizes|replace:",":", "}{else}SOLD OUT{/if}">
							<div class="tooltip-arrow"><div class="tooltip-arrow-inner"></div></div>
							<div class="tooltip-content"></div>
						</div>
					{/if}
				</li>
			{/if}
		{/if}
		{assign var=i value=$i+1}
	{/foreach}


{if $wi >0}
	{assign var=pos value=$wi+15}
	<span class="vertical-line"></span>
{/if}
