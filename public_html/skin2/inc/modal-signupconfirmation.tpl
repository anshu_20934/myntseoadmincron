<div id="lb-login" class="lightbox loginbox loginsignupV2">
    <div class="mod">
        <div class="innerShell">
            <div class="confMainHeading"><div class="red-tick"></div>Congratulations</div>
            <div class="confSubText">Your account has been successfully created</div>
            <div class="arrow"></div>
            <div class="confHeading">So, What would you like to shop today?</div>
            <div class="confMessages">
                {foreach from=$confMessages key=index item=value }
                <a href='{$value["href"]}'>
                <div class="circleMessage" style="background-color:{$value['color']}">
                    {$value['message']}
                </div>
                </a>
                {/foreach}
            </div>
        </div>
        <div class="outter-content">
            <div class="myntraWaterMark"></div>
	    <div class="signupCoupon">{$signupOffersTextDesktop}</div>
            <div class="blue skipReturn">Skip & Return to myntra <span class="right-arrow"></span></div>
        </div>
    </div>
</div>
<input type="hidden" name="loginbg" value="{if isset($is_secure)}{$loginbg|replace :'http://myntra.myntassets.com':'https://d6kkp3rk1o2kk.cloudfront.net'}{else}{$loginbg}{/if}" class="mk-login-bg-path" />

