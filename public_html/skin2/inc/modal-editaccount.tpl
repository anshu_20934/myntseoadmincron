<?php 
////////////// Myntra ///////////////
// Modal - Edit Account
/////////////////////////////////////
?> 

<div class="mk-edit-acct-wrapper mk-hide">
	<div class="mk-edit-acct-modal">
	<span class="mk-modal-close">X</span>
		<h1> Edit Address </h1> 
		<h3> *Required Field</h3>

		<form>
			<label>Name this Address*</label>
			<input type="text" />
			<div class="mk-f-right mk-cf">
				<label>Last Name</label>
				<input type="text" class="mk-half-size"/>
			</div>
			<label>First Name</label>
			<input type="text" class="mk-half-size"/>
			<label>Enter Shipping Address*</label>
			<input type="text" />
			<input type="text" />
			<div class="mk-f-right mk-cf">
				<label>State</label>
				<input type="text" class="mk-half-size"/>
			</div>
			<label>City</label>
			<input type="text" class="mk-half-size"/>
			<label class="mk-clear">Country</label>
			<input type="text" />
			<div class="mk-f-right mk-cf">	
				<label>Mobile*</label>
				<input type="text" class="mk-half-size"/>
			</div>
			<label>Pin Code*</label>
			<input type="text" class="mk-half-size"/>	

			<label class="mk-label_check"><input type="checkbox" /> Same as Billing Address</label>  <label class="mk-label_check mk-form-check mk-f-right"><input type="checkbox" /> Save this Address for Future Purchases</label>
			
			<div class="mk-form-submit">
            	<input type="submit" value="continue" class="mk-edit-submit"/> 
            </div>
        </form>
	</div><!-- End .edit-acct-modal -->
</div> <!-- End .edit-acct-wrapper -->