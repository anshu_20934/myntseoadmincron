<script type="text/javascript">
        Myntra.Data.newTabWidget = true;
</script>
<section class="mk-tabWidget">
        <div class="mk-tabHeader clearfix">
		<a href="#" title="" data-rel="popular" class="deactivated inactive dummyHeader"><span class="text"></span><span class="activeArrow">&nbsp;</span></a>
		<a href="#" title="" data-rel="recent" class="deactivated inactive"><span class="text"></span><span class="activeArrow">&nbsp;</span>Recently Viewed</a>				
	</div>
	<div class="mk-tabContentWrap">
		<div class="mk-tabContent inactive dummyWidget" data-rel-content="popular">{include file="inc/most-popular-widget.tpl"}</div>
		<div class="mk-tabContent inactive" data-rel-content="recent"><div class="mk-recent-viewed mk-hide mk-carousel-four mk-carousel-five rs-carousel mk-inline-size-selector"></div></div>	
	</div>        	
</section>   
