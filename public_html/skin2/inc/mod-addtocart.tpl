	<script>
	Myntra.Data.addToCartStatus={if $status}{$status}{else}0{/if};
	</script>
	<div class="confirm-block product-guide">
        <div class="mk-f-left add-cart-product-info ">
        	<div class="product-info-inner">
            <p class="confirm-message"><span></span>{$message}</p>
            <h5>Product Code: {$productStyleId}</h5>
            <h2>{$productName}</h2>
            <div class="mk-price-block">
                <span class="price-info">{$rupeesymbol} {if $discountamount > 0}{math equation="x - d" x=$originalprice d=$discountamount assign="finalamount"}{$finalamount|round|number_format:0:".":","}{else}{$originalprice|round|number_format:0:".":","}{/if}</span>
               {if discountlabeltext} {if $discountamount > 0}<p class="discount-info-block">{$rupeesymbol} <span class="strike">{$originalprice}</span>  {include file="string:{$discountlabeltext}"}{/if}</p> {/if}           
            </div>
            <p class="size-info">Size:&nbsp;<span>{$sizeName}</span></p>
            <!-- <a href="#" class="remove-item">REMOVE</a> -->
            </div>
        </div>
        <div class="mk-f-left cart-info">
            <span class="item-count-info">{$itemsInCart} item(s)</span>
            <p class="bag-info">IN YOUR BAG</p>
            <p class="total-price">TOTAL: Rs {$totalAmount}</p>
            <a href="/mkmycart.php" class="btn primary-btn checkout-btn" onClick="_gaq.push(['_trackEvent', 'addtocartpopup', 'checkout']);return true;">CHECKOUT</a>
            <div class="or">OR</div>
            <a class="btn normal-btn continue-btn" href="/">CONTINUE SHOPPING</a>
        </div>
    </div>
    <div class="recommend-block">
    
    <img src="{$cdn_base}/skin2/images/loader_150x200.gif" alt="loading" class="loader"/>
             
    </div>