<!-- Recent Items -->
{assign var=wi value=0}
<h4><span class="recViewText">Recently Viewed</span><span class="rec-title">You may also consider</span></h4>
<ul class="rs-carousel-runner mk-recent-viewed-ul">
	{assign var=i value=1}
	{foreach name=widgetdataloop item=widgetdata from=$widget.data}
		{if $widgetdata.product|trim neq ''}
		{assign var=size_selected value=''}
		 	{if $recent_viewed_count >= 5 || $i <= 5}
				<li data-id="prod_{$widgetdata.id}" class="rs-carousel-item {if $i <= $recent_viewed_count}recent-inline{/if}">
			
					{if $quicklookenabled}
						<span class="quick-look" data-widget="{if $ga_component_name}{$ga_component_name}{else}recent_viewed_page{/if}" data-styleid="{$widgetdata.styleid}" data-href="/{$widgetdata.landingpageurl}" href="javascript:void(0)"></span>
					{/if}
			
					<a title="{$widgetdata.product}" href="/{$widgetdata.landingpageurl}?src=rv" onclick="_gaq.push(['_trackEvent', 'pdp_click', Myntra.Data.pageName, '{if $ga_component_name}{$ga_component_name}{else}recent_viewed_page{/if}']);">
						{if $widgetdata.visual_tag}
			                 {assign var="visualtag" value=":"|explode:$widgetdata.visual_tag}
			                 <div class="mk-visual-tag mk-visual-tag-{$visualtag[1]|lower}"><div>{$visualtag[0]|lower}</div></div>
			            {/if}
						<div class="mk-product-image">
							<img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src='{myntraimage key="style_150_200" src=$widgetdata.search_image|replace:"_images_180_240":"_images_150_200"|replace:"_images_240_320":"_images_150_200"|replace:"style_search_image":"properties"}' alt="{$widgetdata.product}" />
						</div>
					</a>
					<div class="mk-product-info">
						<p class="brand-text">{$widgetdata.global_attr_brand|truncate:20}</p>
						<p class="price-text red">{$rupeesymbol}{if $widgetdata.discount > 0}{$widgetdata.discounted_price|number_format:0:".":","} <span class="strike gray">{$widgetdata.price|number_format:0:".":","}</span>{else}{$widgetdata.price|number_format:0:".":","}{/if}</p>
						{if $widgetdata.discount_label}<p class="price-text red-text red">{include file="string:{$widgetdata.discount_label}"}</p>{/if}
	
						{if $i > $recent_viewed_count && $widgetdata.generic_type neq 'design'}
							<div class="availability" data-sizes="{if $widgetdata.sizes neq '' && $widgetdata.sizes neq ' '}Sizes: {$widgetdata.sizes|replace:",":", "}{else}SOLD OUT{/if}">
								<div class="tooltip-arrow"><div class="tooltip-arrow-inner"></div></div>
								<div class="tooltip-content"></div>
							</div>
						{/if}
					</div>
	   				{if $i <= $recent_viewed_count}
						<div class="inline-add-block">
							<button class="inline-add icon-buynow mk-hide"></button>
			
							{if $widgetdata.sizes_facet|lower eq 'freesize' || $widgetdata.sizes_facet|lower eq 'onesize'}
								{assign var=size_selected value=$widgetdata.sizes_facet}
							{/if}
							
							<form action="/mkretrievedataforcart.php?pagetype=productdetail" class="inline-add-to-cart-form" method="post" class="clearfix">
								   	<input type="hidden" name="_token" value="{$USER_TOKEN}" />
									<input type="hidden" name="productStyleId" value="{$widgetdata.styleid}">
									<input type="hidden" value="1" name="quantity">
									<input type="hidden" class="sizename" value="" name="sizename[]">
									<input type="hidden" value="1" name="sizequantity[]">
									<input type="hidden" name="selectedsize" value="">
									<input type="hidden" name="pageName" value="pdp">
									<input type="hidden" name="widgetType" value="recently_viewed">
									<input type="hidden" name="productSKUID" class="productSKUID" value="">
									<input type="hidden" name="productStyleLabel" value="{$widgetdata.stylename}">
									<input type="hidden" name="brand" value="{$widgetdata.global_attr_brand}">
									<input type="hidden" name="articleType" value="{$widgetdata.global_attr_article_type}">
							</form>
							<div class="mk-select-size rs-carousel mk-hide">
					        	<div class="tooltip-arrow"><div class="tooltip-arrow-inner"></div></div>
					        	<div class="tooltip-content">
					        		{if $size_selected}
						        		<span class="selected">{$size_selected}</span>
									{else}
										Select a size to buy:
									    <div class="rs-carousel-runner mk-cf">
									    </div>
								    {/if}
								    <span class="icon-prev mk-hide"></span>
								    <span class="icon-next mk-hide"></span>
								</div>
							</div>
						</div>
		   				{if  $recent_viewed_count < 5 && $i <= 5}{assign var=wi value=$wi+190}{/if}
					{/if}
				</li>
			{/if}
		{/if}
		{assign var=i value=$i+1}
	{/foreach}
</ul>
{if $wi > 0}
	{assign var=pos value=$wi}
	<span class="vertical-line"></span>
{/if}
<script type="text/javascript">
	Myntra.Data.recentlyViewedNumber = {$recent_viewed_count};
</script>

<style>
	.rec-title{literal}{{/literal}left:{$wi+20}px;position:absolute; {if $recent_viewed_count > 4}display:none;{/if} *line-height:15px{literal}}{/literal}
	.vertical-line {literal}{{/literal}left:{$pos}px;{literal}}{/literal}
</style>
