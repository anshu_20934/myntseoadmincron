	{**Check for Carousel initiate accordingly**}
	<div class="lp-section ">
		<h4><div></div><a href="/{$url}">{$title}</a></h4>
		{if $sectiondescription}
		<p class="section-desc mynt-text">
				{$sectiondescription}
		</p>
		{/if}
	<div class=" lp-section-inner mk-carousel-four mk-carousel-five {if $is_carousel && $no_of_products > 5} lp-section-carousel{/if}">
		<ul class="{if $is_carousel} rs-carousel-runner {/if} lp-product-section-ul">
		{foreach name=widgetdataloop item=widgetdata from=$widget.data}
		    {assign var=mod_val value=$widgetdata@index%5}    
			{if $widgetdata.product|trim neq ''}
	

					 <li class="mk-product mk-product-style-details  lp-section-item {if $is_carousel} rs-carousel-item {/if} {if !$is_carousel && $mod_val eq '4'}mk-last{/if} {if !$is_carousel &&  $mod_val eq '0' }mk-first{/if}" data-id="prod_{$widgetdata.id}">
    	{if $quicklookenabled}
			<span class="quick-look" data-widget="search_results" data-styleid="{$widgetdata.styleid}" data-href="{$http_location}/{$widgetdata.landingpageurl}"></span>
		{/if}

		<a style="display:block;" class="clearfix" href="{if $widgetdata.landingpageurl}{$http_location}/{$widgetdata.landingpageurl}{/if}{if $nav_id}?nav_id={$nav_id}{/if}">
			<div class="mk-prod-img">
					<img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src='{myntraimage key="style_150_200" src=$widgetdata.search_image|replace:"_images_180_240":"_images_150_200"|replace:"_images_240_320":"_images_150_200"|replace:"style_search_image":"properties"}' alt="{$widgetdata.product}" />
			</div>
			<div class="mk-prod-info">
				{if $widgetdata.generic_type neq 'design'}
			        <div class="availability" data-sizes="{if $widgetdata.sizes neq '' && $widgetdata.sizes neq ' '}Sizes: {$widgetdata.sizes|replace:",":", "}{else}SOLD OUT{/if}">
			        	<div class="tooltip-arrow"><div class="tooltip-arrow-inner"></div></div>
			        	<div class="tooltip-content"></div>
			        </div>
		        {/if}
	            <span class="mk-prod-name"><span class="mk-prod-brand-name">{$widgetdata.brands_filter_facet}</span> {$widgetdata.product|strip|truncate:53}</span>
	            <span class="mk-prod-price red">{$rupeesymbol}{if $widgetdata.discount > 0}{$widgetdata.discounted_price|number_format:0:".":","} <span class="strike gray">{$widgetdata.price|number_format:0:".":","}</span>{else}{$widgetdata.price|number_format:0:".":","}{/if}</span>
	            {if $widgetdata.discount_label}
		            <span class="mk-discount red">
		                {include file="string:{$widgetdata.discount_label}"}  
		            </span>
		        {/if}
	        </div>
		</a>

		{if $colourOptionsVariant == 'test' && $widgetdata.colourVariantsData|count gt 0}
			<div class="colours-lbl">+{$widgetdata.colourVariantsData|count} Colour{if $widgetdata.colourVariantsData|count gt 1}s{/if}</div>
		{/if}

		{if $colourOptionsVariant == 'test' && $widgetdata.colourVariantsData|count gt 0}
			<div class="mk-colour rs-carousel mk-hide">
	        	<div class="tooltip-arrow"><div class="tooltip-arrow-inner"></div></div>
	        	<div class="tooltip-content">
				    <ul class="rs-carousel-runner mk-cf">
				    	{foreach from=$widgetdata.colourVariantsData key=key item=val}
					        <li class="rs-carousel-item">
					            <a href="{$http_location}/{$val.landing_page_url}{if $nav_id}?nav_id={$nav_id}{/if}" data-id="{$key}">
						            {assign "title" "{$val.global_attr_base_colour}{if $val.global_attr_colour1 neq 'NA' && $val.global_attr_colour1 neq ''} / {$val.global_attr_colour1}{/if}"}
					            	<img src="{$val.search_image|replace:'_images_180_240':'_images_48_64'|replace:'/style_search_image/':'/properties/'}" height="48" width="39" title="{$title}" alt="{$title}" />
					            </a>
					        </li>
				        {/foreach}
				    </ul>
				    <span class="icon-prev mk-hide"></span>
				    <span class="icon-next mk-hide"></span>
				</div>
			</div>
		{/if}
		
	</li>
			{/if}
			
		{/foreach}
		</ul>
	</div>
</div>

