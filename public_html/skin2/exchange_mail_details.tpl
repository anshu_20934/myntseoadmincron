<table cellspacing=0 cellpadding=0 width=99%>
	<caption><b>Shipment Details</b></caption>
    <tr>
        <td><hr style="border: 1px solid black;"/></td>
    </tr>
    <table cellspacing=0 cellpadding=5 width=100% float=right>
        <tr>
			<td><b>Exchange Order ID</b></td>
            <td>{$exchangeid}</td>
        </tr>
        {if $courier}
	        <tr>
	            <td><b>Courier Service Provider</b></td>
	            <td>{$courier}</td>
	        </tr>
	    {/if}
	    {if $tracking}
	        <tr>
	            <td><b>Courier Tracking Number</b></td>
	            <td>{$tracking}</td>
	        </tr>
	    {/if}
	    {if $deliveryStaff_name}
	        <tr>
	            <td><b>Delivery Agent Name</b></td>
	            <td>{$deliveryStaff_name}</td>
	        </tr>
	    {/if}
	    {if $deliveryStaff_mobile}
	        <tr>
	            <td><b>Delivery Agent Mobile number</b></td>
	            <td>{$deliveryStaff_mobile}</td>
	        </tr>
	    {/if}
        </tr>
        <tr>
            <td><b>Exchange Address</b></td>
            <td>{$address}</td>
        </tr>
        <tr>
            <td><b>Exchange Product</b></td>
            <td>{$productDesc}</td>
        </tr>
        <tr>
            <td><b>Original Size: </b>{$originalSize}</td>
            <td><b>New Size: </b>{$newSize}</td>
        </tr>
        {if $quantity > 1}
	        <tr>
	            <td><b>Quantity Exchanged</b></td>
	            <td>{$quantity}</td>
	        </tr>
	    {/if}
    </table>
    <tr>
        <td><hr style="border: 1px solid black;"/></td>
    </tr>
</table>
