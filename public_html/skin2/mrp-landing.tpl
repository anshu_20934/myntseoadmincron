{extends file="layout.tpl"}

{block name=body}
{if $styleExclusionFG}
	<div class="mk-one-column">
		<section class="mk-site-main">
			<div class="mk-static-pages">
			<h1 id="top">Mynt Club - Rewards and Loyalty Program</h1>
			<div class="mk-mynt-content mk-cf">
				<div class="mk-mrp-page">
					<div class="mk-mrp-blocks clearfix">
						{if !$login}
						<div class="mk-mrp-block mk-mrp-block-left">
							<div class="mk-mrp-head-text">Sign up & Earn</div>
							<div class="mk-mrp-sub-text">Sign up now and get <span class="mk-price-spl"> Rs.{$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_numCoupons*$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_mrpAmount}* </span>credited to your Mynt Club account, all for your shopping delight.</div>
							<a href="javascript:void(0)" class="mk-sign-up-link btn primary-btn">sign up</a>
						</div>
						{/if}
						<div class="mk-mrp-block mk-mrp-block-right" {if $login}style="float:none;margin:0 auto;"{/if}>
							<div class="mk-mrp-head-text">Refer & Earn</div>
							<div class="mk-mrp-sub-text">Refer your friends, and earn <span class="mk-price-spl">Rs.{$mrpCouponConfiguration.mrp_refRegistration_numCoupons*$mrpCouponConfiguration.mrp_refRegistration_mrpAmount}* </span>for every friend who registers, and <span class="mk-price-spl"> Rs.{$mrpCouponConfiguration.mrp_refFirstPurchase_numCoupons*$mrpCouponConfiguration.mrp_refFirstPurchase_mrpAmount}* </span>	on the first purchase of each of your friends.</div>
							<a href="{$http_location}/mymyntra.php?view=myreferrals" class="mk-refer-link btn primary-btn">refer friends & family</a>
						</div>
					</div>
					<div class="mk-cf mk-mrp-faqs">
					    <h6>How do I become part of Mynt Club?</h6>
						<div class="mk-bordered">To earn the benefits of Mynt Club, all you need to do is register with Myntra.com. If you're registered with Myntra, then you're already a member of the Mynt Club. Get set to enjoy all the benefits of the program.</div>
						<h6>Sign up and earn - benefits</h6>
        				<div>Register with Myntra to instantly earn Rs {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_numCoupons*$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_mrpAmount}. This will be added to your Mynt Club account as {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_textNumCoupons} credit of Rs {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_mrpAmount} , valid for {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_validity} days, on purchases of coupon-applicable items worth Rs {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_minCartValue|number_format:0:'':','} and above. Alternatively, you could register with Myntra using "Connect with Facebook" option, and instantly earn Rs.{$mrpCouponConfiguration.mrp_firstLogin_fbreg_numCoupons*$mrpCouponConfiguration.mrp_firstLogin_fbreg_mrpAmount}. This will be added to your Mynt Club account as {$mrpCouponConfiguration.mrp_firstLogin_fbreg_textNumCoupons} credit of Rs {$mrpCouponConfiguration.mrp_firstLogin_fbreg_mrpAmount} , valid for {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_validity} days and on purchases of coupon-applicable items worth Rs {$mrpCouponConfiguration.mrp_firstLogin_fbreg_minCartValue|number_format:0:'':','} and above.  <br>
						Please note that you would be getting the benefit based on the method you use to sign up on Myntra for the first time.</div> 
						<h6>Refer and Earn</h6>
						<div class="mk-bordered">Refer your friends and earn more credits! You earn Rs. {$mrpCouponConfiguration.mrp_refRegistration_numCoupons*$mrpCouponConfiguration.mrp_refRegistration_mrpAmount} when each of your referral registers with Myntra. This is added to your Mynt Club account as {$mrpCouponConfiguration.mrp_refRegistration_textNumCoupons} credits of Rs.{$mrpCouponConfiguration.mrp_refRegistration_mrpAmount}, valid for {$mrpCouponConfiguration.mrp_refRegistration_validity} days. You can redeem this against any purchase of coupon-applicable items worth Rs.{$mrpCouponConfiguration.mrp_refRegistration_minCartValue|number_format:0:'':','} and above.<br>
           				You also earn Rs.{$mrpCouponConfiguration.mrp_refFirstPurchase_numCoupons*$mrpCouponConfiguration.mrp_refFirstPurchase_mrpAmount|number_format:0:'':','} when your referred friends make their first purchase. This is added to your Mynt Club account as {$mrpCouponConfiguration.mrp_refFirstPurchase_textNumCoupons} credits of Rs. {$mrpCouponConfiguration.mrp_refFirstPurchase_mrpAmount} each, valid for {$mrpCouponConfiguration.mrp_refFirstPurchase_validity} days. You can redeem these against purchases of coupon-applicable items worth Rs. {$mrpCouponConfiguration.mrp_refFirstPurchase_minCartValue|number_format:0:'':','} and above.</div>
					    <h6>How can I refer my friends to join the Mynt Club?</h6>
	    				<div class="bordered">Sign in, and go to "My Referrals" tab under "My Myntra" to send out invitations in several easy ways:
        				<ul class="mk-refer-friends-content">
            				<li>1) Select contacts from your email address book
                			<ul class="mk-bullets">
                    			<li>Click on "Add your contacts from" to choose email addresses of friends from your Gmail, Yahoo mail, AOL mail or Outlook accounts.</li>
                    			<li>Follow the steps to select the contacts you wish to invite. You can also manually type in email addresses, separating each one with a comma.</li>
                			</ul>
            				</li>
            				<li>2) Send invites on Facebook - Click on the Facebook icon, follow the steps to invite your friends through message or wall post.</li>
            				<li>3) Send invites on Twitter - Post your invitation as a tweet by clicking the Twitter icon.</li>
            				<li>4) Distribute your invitation link through your blog, social network, instant messengers, website by pasting your referral link along with a personalised message for your friends.</li>
        				</ul>
    					</div>
        				<h6>How do I view my Mynt Club credits?</h6>
						<div class="mk-bordered">Your Mynt Club credits comprise of Sign up and referral bonuses. You can view these in the <a href="mymyntra.php?view=mymyntcredits" target="_blank">Mynt Credits</a> tab of your My Myntra account. Remember to redeem your credits before they expire!</div>
						<h6>How can I redeem my Mynt Club credits</h6>
						<div class="mk-bordered">You will be required to enter a valid mobile number before you can start redeeming your credits. You mobile number will be automatically verified if it is not registered against another account. Please note that we currently support only Indian mobile numbers on our verification process. At checkout, you can choose to apply any of the following benefits -
							<ul>
								<li>1) Sign up or referral credits</li>
								<li>2) Any other discount voucher from Myntra.com</li>
							</ul>
						</div>    
    			        <h6>I have not received my Mynt credits. What should I do?</h6>
				        <div class="mk-bordered">Check your spam folder and add no-reply@myntra.com and support@myntra.com to your address book to ensure delivery of all mails from Mynt Club. If the above did not work, write to us at support@myntra.com or call our Customer Care on {$customerSupportCall} anytime of the day for assistance.</div>
				        <h6>REDEEMING CASHBACK</h6>
						<div class="mk-bordered">You can use the Cashbank amount anytime you wish at the payment page after processing the order. If your order value is greater than the
						Cashback amount, you can make the balance payment through various payment options that we offer. If your order value is lesser than the amount
						of credits in your Cashback account, Cashback credit equal to the value of your order would be utliized for the purchase.</div>
					</div>
				</div>
			</div>	
		</div>
	</section>
	</div>
{elseif $condNoDiscountCouponFG}
	<div class="mk-one-column">
		<section class="mk-site-main">
			<div class="mk-static-pages">
			<h1 id="top">Mynt Club - Rewards and Loyalty Program</h1>
			<div class="mk-mynt-content mk-cf">
				<div class="mk-mrp-page">
					<div class="mk-mrp-blocks clearfix">
						{if !$login}
						<div class="mk-mrp-block mk-mrp-block-left">
							<div class="mk-mrp-head-text">Sign up & Earn</div>
							<div class="mk-mrp-sub-text">Sign up now and get <span class="mk-price-spl"> Rs.{$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_numCoupons*$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_mrpAmount}* </span>credited to your Mynt Club account, all for your shopping delight.</div>
							<a href="javascript:void(0)" class="mk-sign-up-link btn primary-btn">sign up</a>
						</div>
						{/if}
						<div class="mk-mrp-block mk-mrp-block-right" {if $login}style="float:none;margin:0 auto;"{/if}>
							<div class="mk-mrp-head-text">Refer & Earn</div>
							<div class="mk-mrp-sub-text">Refer your friends, and earn <span class="mk-price-spl">Rs.{$mrpCouponConfiguration.mrp_refRegistration_numCoupons*$mrpCouponConfiguration.mrp_refRegistration_mrpAmount}* </span>for every friend who registers, and <span class="mk-price-spl"> Rs.{$mrpCouponConfiguration.mrp_refFirstPurchase_numCoupons*$mrpCouponConfiguration.mrp_refFirstPurchase_mrpAmount}* </span>	on the first purchase of each of your friends.</div>
							<a href="{$http_location}/mymyntra.php?view=myreferrals" class="mk-refer-link btn primary-btn">refer friends & family</a>
						</div>
					</div>
					<div class="mk-cf mk-mrp-faqs">
					    <h6>How do I become part of Mynt Club?</h6>
						<div class="mk-bordered">To earn the benefits of Mynt Club, all you need to do is register with Myntra.com. If you're registered with Myntra, then you're already a member of the Mynt Club. Get set to enjoy all the benefits of the program.</div>
						<h6>Sign up and earn - benefits</h6>
        				<div>Register with Myntra to instantly earn Rs {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_numCoupons*$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_mrpAmount}. This will be added to your Mynt Club account as {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_textNumCoupons} credit of Rs {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_mrpAmount} , valid for {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_validity} days, on purchases of non-discounted items worth Rs {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_minCartValue|number_format:0:'':','} and above. Alternatively, you could register with Myntra using "Connect with Facebook" option, and instantly earn Rs.{$mrpCouponConfiguration.mrp_firstLogin_fbreg_numCoupons*$mrpCouponConfiguration.mrp_firstLogin_fbreg_mrpAmount}. This will be added to your Mynt Club account as {$mrpCouponConfiguration.mrp_firstLogin_fbreg_textNumCoupons} credit of Rs {$mrpCouponConfiguration.mrp_firstLogin_fbreg_mrpAmount} , valid for {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_validity} days and on purchases of non-discounted items worth Rs {$mrpCouponConfiguration.mrp_firstLogin_fbreg_minCartValue|number_format:0:'':','} and above.  <br>
						Please note that you would be getting the benefit based on the method you use to sign up on Myntra for the first time.</div> 
						<h6>Refer and Earn</h6>
						<div class="mk-bordered">Refer your friends and earn more credits! You earn Rs. {$mrpCouponConfiguration.mrp_refRegistration_numCoupons*$mrpCouponConfiguration.mrp_refRegistration_mrpAmount} when each of your referral registers with Myntra. This is added to your Mynt Club account as {$mrpCouponConfiguration.mrp_refRegistration_textNumCoupons} credits of Rs.{$mrpCouponConfiguration.mrp_refRegistration_mrpAmount}, valid for {$mrpCouponConfiguration.mrp_refRegistration_validity} days. You can redeem this against any purchase of non-discounted items worth Rs.{$mrpCouponConfiguration.mrp_refRegistration_minCartValue|number_format:0:'':','} and above.<br>
           				You also earn Rs.{$mrpCouponConfiguration.mrp_refFirstPurchase_numCoupons*$mrpCouponConfiguration.mrp_refFirstPurchase_mrpAmount|number_format:0:'':','} when your referred friends make their first purchase. This is added to your Mynt Club account as {$mrpCouponConfiguration.mrp_refFirstPurchase_textNumCoupons} credits of Rs. {$mrpCouponConfiguration.mrp_refFirstPurchase_mrpAmount} each, valid for {$mrpCouponConfiguration.mrp_refFirstPurchase_validity} days. You can redeem these against purchases of non-discounted items worth Rs. {$mrpCouponConfiguration.mrp_refFirstPurchase_minCartValue|number_format:0:'':','} and above.</div>
					    <h6>How can I refer my friends to join the Mynt Club?</h6>
	    				<div class="bordered">Sign in, and go to "My Referrals" tab under "My Myntra" to send out invitations in several easy ways:
        				<ul class="mk-refer-friends-content">
            				<li>1) Select contacts from your email address book
                			<ul class="mk-bullets">
                    			<li>Click on "Add your contacts from" to choose email addresses of friends from your Gmail, Yahoo mail, AOL mail or Outlook accounts.</li>
                    			<li>Follow the steps to select the contacts you wish to invite. You can also manually type in email addresses, separating each one with a comma.</li>
                			</ul>
            				</li>
            				<li>2) Send invites on Facebook - Click on the Facebook icon, follow the steps to invite your friends through message or wall post.</li>
            				<li>3) Send invites on Twitter - Post your invitation as a tweet by clicking the Twitter icon.</li>
            				<li>4) Distribute your invitation link through your blog, social network, instant messengers, website by pasting your referral link along with a personalised message for your friends.</li>
        				</ul>
    					</div>
        				<h6>How do I view my Mynt Club credits?</h6>
						<div class="mk-bordered">Your Mynt Club credits comprise of Sign up and referral bonuses. You can view these in the <a href="mymyntra.php?view=mymyntcredits" target="_blank">Mynt Credits</a> tab of your My Myntra account. Remember to redeem your credits before they expire!</div>
						<h6>How can I redeem my Mynt Club credits</h6>
						<div class="mk-bordered">You will be required to enter a valid mobile number before you can start redeeming your credits. You mobile number will be automatically verified if it is not registered against another account. Please note that we currently support only Indian mobile numbers on our verification process. At checkout, you can choose to apply any of the following benefits -
							<ul>
								<li>1) Sign up or referral credits</li>
								<li>2) Any other discount voucher from Myntra.com</li>
							</ul>
						</div>    
    			        <h6>I have not received my Mynt credits. What should I do?</h6>
				        <div class="mk-bordered">Check your spam folder and add no-reply@myntra.com and support@myntra.com to your address book to ensure delivery of all mails from Mynt Club. If the above did not work, write to us at support@myntra.com or call our Customer Care on {$customerSupportCall} anytime of the day for assistance.</div>
				        <h6>REDEEMING CASHBACK</h6>
						<div class="mk-bordered">You can use the Cashbank amount anytime you wish at the payment page after processing the order. If your order value is greater than the
						Cashback amount, you can make the balance payment through various payment options that we offer. If your order value is lesser than the amount
						of credits in your Cashback account, Cashback credit equal to the value of your order would be utliized for the purchase.</div>
					</div>
				</div>
			</div>	
		</div>
	</section>
	</div>
{else}
	<div class="mk-one-column">
		<section class="mk-site-main">
			<div class="mk-static-pages">
			<h1 id="top">Mynt Club - Rewards and Loyalty Program</h1>
			<div class="mk-mynt-content mk-cf">
				<div class="mk-mrp-page">
					<div class="mk-mrp-blocks clearfix">
						{if !$login}
						<div class="mk-mrp-block mk-mrp-block-left">
							<div class="mk-mrp-head-text">Sign up & Earn</div>
							<div class="mk-mrp-sub-text">Sign up now and get <span class="mk-price-spl"> Rs.{$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_numCoupons*$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_mrpAmount}* </span>credited to your Mynt Club account, all for your shopping delight.</div>
							<a href="javascript:void(0)" class="mk-sign-up-link btn primary-btn">sign up</a>
						</div>
						{/if}
						<div class="mk-mrp-block mk-mrp-block-right" {if $login}style="float:none;margin:0 auto;"{/if}>
							<div class="mk-mrp-head-text">Refer & Earn</div>
							<div class="mk-mrp-sub-text">Refer your friends, and earn <span class="mk-price-spl">Rs.{$mrpCouponConfiguration.mrp_refRegistration_numCoupons*$mrpCouponConfiguration.mrp_refRegistration_mrpAmount}* </span>for every friend who registers, and <span class="mk-price-spl"> Rs.{$mrpCouponConfiguration.mrp_refFirstPurchase_numCoupons*$mrpCouponConfiguration.mrp_refFirstPurchase_mrpAmount}* </span>	on the first purchase of each of your friends.</div>
							<a href="{$http_location}/mymyntra.php?view=myreferrals" class="mk-refer-link btn primary-btn">refer friends & family</a>
						</div>
					</div>
					<div class="mk-cf mk-mrp-faqs">
					    <h6>How do I become part of Mynt Club?</h6>
						<div class="mk-bordered">To earn the benefits of Mynt Club, all you need to do is register with Myntra.com. If you're registered with Myntra, then you're already a member of the Mynt Club. Get set to enjoy all the benefits of the program.</div>
						<h6>Sign up and earn - benefits</h6>
        				<div>Register with Myntra to instantly earn Rs {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_numCoupons*$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_mrpAmount}. This will be added to your Mynt Club account as {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_textNumCoupons} credit of Rs {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_mrpAmount} , valid for {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_validity} days, on purchases of Rs {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_minCartValue|number_format:0:'':','} and above. Alternatively, you could register with Myntra using "Connect with Facebook" option, and instantly earn Rs.{$mrpCouponConfiguration.mrp_firstLogin_fbreg_numCoupons*$mrpCouponConfiguration.mrp_firstLogin_fbreg_mrpAmount}. This will be added to your Mynt Club account as {$mrpCouponConfiguration.mrp_firstLogin_fbreg_textNumCoupons} credits of Rs {$mrpCouponConfiguration.mrp_firstLogin_fbreg_mrpAmount} each, valid for {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_validity} days and on purchases of Rs {$mrpCouponConfiguration.mrp_firstLogin_fbreg_minCartValue|number_format:0:'':','} and above.  <br>
						Please note that you would be getting the benefit based on the method you use to sign up on Myntra for the first time.</div> 
						<h6>Refer and Earn</h6>
						<div class="mk-bordered">Refer your friends and earn more credits! You earn Rs. {$mrpCouponConfiguration.mrp_refRegistration_numCoupons*$mrpCouponConfiguration.mrp_refRegistration_mrpAmount} when each of your referral registers with Myntra. This is added to your Mynt Club account as {$mrpCouponConfiguration.mrp_refRegistration_textNumCoupons} credits of Rs.{$mrpCouponConfiguration.mrp_refRegistration_mrpAmount}, valid for {$mrpCouponConfiguration.mrp_refRegistration_validity} days. You can redeem this against any purchase of Rs.{$mrpCouponConfiguration.mrp_refRegistration_minCartValue|number_format:0:'':','} and above.<br>
           				You also earn Rs.{$mrpCouponConfiguration.mrp_refFirstPurchase_numCoupons*$mrpCouponConfiguration.mrp_refFirstPurchase_mrpAmount|number_format:0:'':','} when your referred friends make their first purchase. This is added to your Mynt Club account as {$mrpCouponConfiguration.mrp_refFirstPurchase_textNumCoupons} credits of Rs. {$mrpCouponConfiguration.mrp_refFirstPurchase_mrpAmount} each, valid for {$mrpCouponConfiguration.mrp_refFirstPurchase_validity} days. You can redeem these against purchases of Rs. {$mrpCouponConfiguration.mrp_refFirstPurchase_minCartValue|number_format:0:'':','} and above.</div>
					    <h6>How can I refer my friends to join the Mynt Club?</h6>
	    				<div class="bordered">Sign in, and go to "My Referrals" tab under "My Myntra" to send out invitations in several easy ways:
        				<ul class="mk-refer-friends-content">
            				<li>1) Select contacts from your email address book
                			<ul class="mk-bullets">
                    			<li>Click on "Add your contacts from" to choose email addresses of friends from your Gmail, Yahoo mail, AOL mail or Outlook accounts.</li>
                    			<li>Follow the steps to select the contacts you wish to invite. You can also manually type in email addresses, separating each one with a comma.</li>
                			</ul>
            				</li>
            				<li>2) Send invites on Facebook - Click on the Facebook icon, follow the steps to invite your friends through message or wall post.</li>
            				<li>3) Send invites on Twitter - Post your invitation as a tweet by clicking the Twitter icon.</li>
            				<li>4) Distribute your invitation link through your blog, social network, instant messengers, website by pasting your referral link along with a personalised message for your friends.</li>
        				</ul>
    					</div>
        				<h6>How do I view my Mynt Club credits?</h6>
						<div class="mk-bordered">Your Mynt Club credits comprise of Sign up and referral bonuses. You can view these in the <a href="mymyntra.php?view=mymyntcredits" target="_blank">Mynt Credits</a> tab of your My Myntra account. Remember to redeem your credits before they expire!</div>
						<h6>How can I redeem my Mynt Club credits</h6>
						<div class="mk-bordered">You will be required to verify your mobile number before you can start redeeming your credits. On initiating the verification process, you will be sent a code via SMS. Please enter this code on the website to complete the verification. Please note that we currently support only Indian mobile numbers on our verification process. At checkout, you can choose to apply any of the following benefits -
							<ul>
								<li>1) Sign up or referral credits</li>
								<li>2) Any other discount voucher from Myntra.com</li>
							</ul>
						</div>    
    			        <h6>I have not received my Mynt credits. What should I do?</h6>
				        <div class="mk-bordered">Check your spam folder and add no-reply@myntra.com and support@myntra.com to your address book to ensure delivery of all mails from Mynt Club. If the above did not work, write to us at support@myntra.com or call our Customer Care on {$customerSupportCall} anytime of the day for assistance.</div>
				        <h6>REDEEMING CASHBACK</h6>
						<div class="mk-bordered">You can use the Cashbank amount anytime you wish at the payment page after processing the order. If your order value is greater than the
						Cashback amount, you can make the balance payment through various payment options that we offer. If your order value is lesser than the amount
						of credits in your Cashback account, Cashback credit equal to the value of your order would be utliized for the purchase.</div>
					</div>
				</div>
			</div>	
		</div>
	</section>
	</div>
	
{/if}

{/block}

