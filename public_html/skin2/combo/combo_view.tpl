{extends file="layout.tpl"}
{block name=body}
<!-- Header -->
<script>
    Myntra.Combo = Myntra.Combo || {};
    Myntra.Combo.isConditionMet = {$isConditionMet|@json_encode};
    Myntra.Combo.selectedStyles = {$selectedStyles|@json_encode};
    Myntra.Combo.min = parseInt({$min}+0.5);
    Myntra.Combo.ctype = "{$ctype}";
    Myntra.Combo.cartSavings = parseInt({$comboSavings}+0.5);
    Myntra.Combo.cartTotal = parseInt({$comboTotal}+0.5);
    Myntra.Combo.products = {$products|@json_encode};
    Myntra.Combo.solrProducts = {$dataMap|@json_encode};
    Myntra.Combo.selectedStyleProductMap = {$selectedStyleProductMap|@json_encode};
    Myntra.Combo.isAmount = {if $productsSet.dre_buyAmount>0} true; {else} false; {/if}
    Myntra.Combo.ComboId = {if $comboId}{$comboId}{else}''{/if};
    Myntra.Combo.freeItem = Object();
    Myntra.Combo.discountedCount = parseInt({$productsSet.dre_count});
    Myntra.Combo.comboTotal = {$comboTotal};
    Myntra.Combo.itemName = "{$dre_itemName|escape:'quotes'}";
    Myntra.Combo.productsSet = {$productsSet|@json_encode};
    Myntra.Combo.itemPrice = '{$dre_itemPrice}';
    Myntra.Combo.freeItemDetails = {$freeItemDetails|@json_encode};
    {literal}
    if(Myntra.Combo.itemName != undefined && Myntra.Combo.itemPrice != undefined){
        Myntra.Combo.freeItem = { name : Myntra.Combo.itemName , price : Myntra.Combo.itemPrice};
    }
    {/literal}
</script>
<section class="mk-site-main">
    <div class="mk-combo-header">
        <h1>Combo Offer</h1>
    </div>

    <div class="mk-combo-box-cont">
        <div class="mk-combo-box" id="mk-combo-box">
            <div class="combo-message">{include file="string:{$displayText}"}</div>
            <div class="progress-cont">
                <div class="progress-cont-inner">
                    <div class="combo-progress">

                    </div>
                    <div class="combo-mini-details">
                   
                    </div>

                </div>

                 <a class="btn small-btn normal-btn" id="goto-bag" href="{$cartPageUrl}">go to bag</a><span class="open box-control"></span>
                 <span class="min-more-message"></span>
            </div>
            <div class="combo-items-outer">
              <div class="combo-items rs-carousel">
                <ul class="rs-carousel-runner">
  
                </ul>
              </div>
            </div>
        </div>
    </div>
    
    <div class="mk-product-filters"> 
        <span class="product-count">Choose from {$data|@count} products <em>&nbsp;|&nbsp;</em> </span>           
        <ul class="mk-product-sort">  <!-- Note: different option? -->
            <li class="mk-title"> Sort:</li>
            <li class="mk-sort-popular">&nbsp;&nbsp;<a data-val='POPULAR' href="javascript:void(0);"  class="popular-sort-filter mk-sort-selected">Popularity</a>&nbsp;&nbsp;/</li>
                  
            <li class="mk-sort-whats-new">&nbsp;&nbsp;<a data-val='RECENCY' href="javascript:void(0);" class="recency-sort-filter" >What's New</a>&nbsp;&nbsp;/</li>

            <li class="mk-sort-price">&nbsp;&nbsp;<a data-val= "PRICEA" href="javascript:void(0);" class="price-sort-filter">Price <span style="display:none" class="mk-price-asc"></span><span style="display: none" class="mk-price-desc"></span></a></li>
        </ul> <!-- End .product-sort -->
    </div>
 <script>
     var uiParams = null;
     var cid = {$cid};
 </script>

    <div class="mk-combo-grid mk-search-grid megalist" id="mk-combo-grid">
        <ul class="mk-cf">
            {*include file="combo/combo_items.tpl"*}
       </ul>
    </div>
</section>


{/block}
