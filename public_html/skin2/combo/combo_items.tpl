 <script>
     var uiParams = {$uiParamsJSON};
 </script>
{*foreach name=widgetdataloop item=widgetdata from=$data}
    {assign var=mod_val value=$widgetdata@index%5}    
    <li class="mk-product mk-product-style-details {$mod_val} {if $mod_val eq '5'}mk-last{/if} {if $mod_val eq '0'}mk-first{/if}" data-id="prod_{$widgetdata.styleid}" id="prod_{$widgetdata.styleid}">
        
        <span class="quick-look " data-widget="search_results" data-styleid="{$widgetdata.styleid}" data-href="{$http_location}/{$widgetdata.landingpageurl}"></span>

        
        <a style="display:block;" class="clearfix" href="{if $widgetdata.landingpageurl}{$http_location}/{$widgetdata.landingpageurl}{/if}">
            <div class="mk-prod-img">
                <img {if $smarty.foreach.widgetdataloop.index gt 4}class="jquery-lazyload" original="{myntraimage key="style_180_240" src=$widgetdata.search_image}" {/if} src="{if $smarty.foreach.widgetdataloop.index gt 4}{$cdn_base}/skin2/images/loader_180x240.gif{else}{myntraimage key="style_180_240" src=$widgetdata.search_image}{/if}" alt="{$widgetdata.product}">
            </div>
            <div class="mk-prod-info">
                {if $widgetdata.generic_type neq 'design'}
                    <div class="availability" data-sizes="{if $widgetdata.sizes neq '' && $widgetdata.sizes neq ' '}Sizes: {$widgetdata.sizes|replace:",":", "}{else}SOLD OUT{/if}">
                        <div class="tooltip-arrow"><div class="tooltip-arrow-inner"></div></div>
                        <div class="tooltip-content"></div>
                    </div>
                {/if}
                <span class="mk-prod-name"><span class="mk-prod-brand-name">{$widgetdata.brands_filter_facet}</span> {$widgetdata.product|strip|truncate:53}</span>
                <span class="mk-prod-price red">{$rupeesymbol}{if $widgetdata.discount > 0}{$widgetdata.discounted_price|number_format:0:".":","} <span class="strike gray">{$widgetdata.price|number_format:0:".":","}</span>{else}{$widgetdata.price|number_format:0:".":","}{/if}</span>
                {if $displayText}
                    <span class="mk-discount red">
                        {include file="string:{$widgetdata.discount_label}"}  
                    </span>
                {/if}
            </div>
        </a>

        {if $colourOptionsVariant == 'test' && $widgetdata.colourVariantsData|count gt 0}
            <div class="colours-lbl">+{$widgetdata.colourVariantsData|count} Colour{if $widgetdata.colourVariantsData|count gt 1}s{/if}</div>
        {/if}

        {if $colourOptionsVariant == 'test' && $widgetdata.colourVariantsData|count gt 0}
            <div class="mk-colour rs-carousel mk-hide">
                <div class="tooltip-arrow"><div class="tooltip-arrow-inner"></div></div>
                <div class="tooltip-content">
                    <ul class="rs-carousel-runner mk-cf">
                        {foreach from=$widgetdata.colourVariantsData key=key item=val}
                            <li class="rs-carousel-item">
                                <a href="{$http_location}/{$val.url}{if $nav_id}?nav_id={$nav_id}{/if}" data-id="{$key}">
                                    {assign "title" "{$val.base_color}{if $val.color1 neq 'NA' && $val.color1 neq ''} / {$val.color1}{/if}"}
                                    <img class="jquery-lazyload" src="{$cdn_base}/skin2/images/loader_180x240.gif" original="{myntraimage key='style_48_64' src=$val.image|replace:'_images_180_240':'_images_48_64'|replace:'/style_search_image/':'/properties/'}" height="52" width="39" title="{$title}" alt="{$title}" />
                                </a>
                            </li>
                        {/foreach}
                    </ul>
                    <span class="icon-prev mk-hide"></span>
                    <span class="icon-next mk-hide"></span>
                </div>
            </div>
        {/if}
       <span class="combo-flag {if $widgetdata.styleid|in_array:$selectedStyles}{else}mk-hide{/if}"></span>  
    </li>
{/foreach*}