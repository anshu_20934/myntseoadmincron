<li class="col">
	<h4><a href="/mymyntra.php?view=myprofile">My Profile</a></h4>
	{if $dashValues.profile.firstname|trim}
		{$dashValues.profile.firstname} {$dashValues.profile.lastname}<br />
	{/if}
	<span class="email">{$dashValues.profile.login|truncate:24}</span><br />
	Mobile: {$dashValues.profile.mobile}<br />
	{if !$dashValues.profile.firstname|trim}
		<br />
	{/if}
	<a href="/mymyntra.php?view=myprofile">Add / Edit Profile</a>
</li>
<li class="col">
	<h4><a href="/mymyntra.php?view=myorders">My Orders</a></h4>
	{if $dashValues.counts.pending_orders eq 0 && $dashValues.counts.completed_orders eq 0}
		You haven't placed any Orders yet<br />
		Start Shopping Now!
	{else}
		{$dashValues.counts.pending_orders} Pending<br />
		{$dashValues.counts.completed_orders} Completed<br />
		<br />
		<a href="/mymyntra.php?view=myorders">Track an Order</a>
	{/if}
</li>
<li class="col">
	<h4><a href="/mymyntra.php?view=mymyntcredits">Mynt Credits</a></h4>
	{if $dashValues.myntCashDetails.balance eq 0 && $dashValues.credits.couponsCount eq 0 && $loyaltyPointsHeaderDetails.activePointsBalance eq 0}
		You can Earn Coupons through our Rewards and Loyalty Program<br />
		<a href="/mrp_landing.php">Learn More</a>
	{else}
		<span class="rupees">{$rupeesymbol} {$dashValues.myntCashDetails.balance|number_format:2 }</span> Cashback<br />
		{if $loyaltyEnabled && $loyaltyPointsHeaderDetails.tNcAccepted}
			<span class="loyalty-dash-info"><a href="/mymyntra.php?view=myprivilege">{$loyaltyPointsHeaderDetails.activePointsBalance} Points</a>
				<span class="icon-new-large new-loyal-flag-icon"></span>
			</span>
			<br/>
		{/if}
		{$dashValues.credits.couponsCount} Active Coupons<br />
		{if !$loyaltyEnabled || !$loyaltyPointsHeaderDetails.tNcAccepted}<br/>{/if}
		{if $dashValues.credits.expiring.count eq 0 || $dashValues.credits.expiring.count eq ''}
			<a href="/mrp_landing.php">Earn more Coupons!</a>
		{else}
			<a href="/mymyntra.php?view=mymyntcredits">
				{$dashValues.credits.expiring.count}
				{if $dashValues.credits.expiring.count gt 1} 
					Coupons Expire
				{else} 
					Coupon Expires
				{/if}
				{if $dashValues.credits.expiring.expdays lt 1}
					Today!
				{elseif $dashValues.credits.expiring.expdays lt 2}
					Tomorrow!
				{else}
					in {$dashValues.credits.expiring.expdays} days!
				{/if}
			</a>
		{/if}

	{/if}
</li>
<li class="col">
	<h4><a href="/mymyntra.php?view=myreferrals">My Referrals</a></h4>
	{if $dashValues.referrals.open eq 0 && $dashValues.referrals.registered eq 0 && $dashValues.referrals.converted eq 0}
		You haven't referred Anyone yet<br /><br />
		<a href="/mymyntra.php?view=myreferrals">Start Referring Now!</a>
	{else}
		{$dashValues.referrals.open} Open Invites<br />
		{$dashValues.referrals.registered} Registered<br />
		{$dashValues.referrals.converted} Placed First Order<br />
		<a href="/mymyntra.php?view=myreferrals">Refer More Friends!</a>
	{/if}
</li>
<li class="col no-border">
	<h4><a href="/mymyntra.php?view=myreturns">My Returns</a></h4>
	{if $dashValues.counts.pending_returns eq 0 && $dashValues.counts.completed_returns eq 0}
		You haven't placed any Returns Requests yet<br />
	{else}
		{$dashValues.counts.pending_returns} Pending<br />
		{$dashValues.counts.completed_returns} Completed<br />
	{/if}
	{if !$dashValues.counts.completed_orders eq 0}
		<br />
		<a href="/mymyntra.php?view=myreturns">Return an Item</a>
	{/if}
</li>
