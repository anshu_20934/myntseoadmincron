{if !$telesales && !$mymyntra}
<!-- My Myntra Container  -->
<div class="mk-mymyntra-items mk-saved-wrapper mk-hide rs-carousel"><ul class="gray"></ul></div>

<!-- Saved Items Container  -->
<div class="mk-saved-items mk-saved-wrapper mk-hide rs-carousel"><ul class="rs-carousel-runner"></ul></div>

<!-- Recent Items --> 
<div class="mk-recent-items mk-saved-wrapper mk-hide rs-carousel"><ul class="rs-carousel-runner"></ul></div>

<!-- Bag Items --> 
<div class="mk-bag-items mk-saved-wrapper mk-hide rs-carousel"><ul class="rs-carousel-runner"></ul></div>
{/if}
