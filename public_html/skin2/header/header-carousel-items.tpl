<!-- RECENT VIEWED -->	
{if isset($recentWidget)}
	{assign var=ga_component_name value="header_recent"}
	{foreach name=widgetdataloop item=widgetdata from=$widgetData.data}
	{if $widgetdata.product neq '' && $widgetdata.product neq ' '}
	<li data-id="prod_{$widgetdata.id}"  class="rs-carousel-item">
		{if $quicklookenabled}
			<span class="quick-look" data-widget="{$ga_component_name}" data-styleid="{$widgetdata.styleid}" data-href="/{$widgetdata.landingpageurl}" href="javascript:void(0)"></span>				
		{/if}
		<a title="{$widgetdata.product}" href="/{$widgetdata.landingpageurl}" onClick="_gaq.push(['_trackEvent','pdp_click', Myntra.Data.pageName, '{$ga_component_name}']); return true;">
				<div class="mk-product-image">
					<img  src="{myntraimage key="style_81_108" src=$widgetdata.search_image|replace:'_images_180_240':'_images_81_108'|replace:'_images_240_320':'_images_81_108'|replace:'style_search_image':'properties'}" alt="{$widgetdata.product}" />
					<div class="mk-product-brand">
						<em>{$widgetdata.global_attr_brand}</em>
					</div>
				</div>
					
			</a>
				<p class="brand-text">{$widgetdata.global_attr_brand|truncate:20}</p>
				<p class="price-text red"><span class="rupees">{$rupeesymbol}{if $widgetdata.discount > 0}{$widgetdata.discounted_price|number_format:0:".":","} <span class="strike gray">{$widgetdata.price|number_format:0:".":","}</span>{else}{$widgetdata.price|number_format:0:".":","}{/if}</span></p>
				{if $widgetdata.discount_label}<p class="price-text red-text red">{include file="string:{$widgetdata.discount_label}"}  </p>{/if}
		</li>
	
	{/if}
	{/foreach}
{else}
<!-- SAVED AND CART HERE -->
	{if isset($savedWidget)}
		{assign var=ga_component_name value="header_saved"}
	{else}
		{assign var=ga_component_name value="header_bag"}
	{/if}	
	{foreach name=bagItem item=bagItem from=$bagItems}
	{if !$bagItem.freeItem }
	<li data-id="prod_{$bagItem.productId}" class="rs-carousel-item">
		{if $quicklookenabled}
			<span class="quick-look" data-widget="{$ga_component_name}" data-styleid="{$bagItem.productStyleId}" data-href="/{$bagItem.landingpageurl}" href="javascript:void(0)"></span>				
		{/if}
		<a title="{$widgetdata.product}" href="/{$bagItem.landingpageurl}" onClick="_gaq.push(['_trackEvent','pdp_click', Myntra.Data.pageName, '{$ga_component_name}']); return true;">
				<div class="mk-product-image">
					<img src="{myntraimage key="style_81_108" src=$bagItem.cartImagePath|replace:'_images_96_128':'_images_81_108'}" alt="{$bagItem.productStyleName}" />
					<div class="mk-product-brand">
						<em>{$bagItem.brand}</em>
					</div>
				</div>
					
			</a>
				<p class="brand-text">{$bagItem.brand|truncate:20}</p>
				<p class="price-text red">
                    {if $bagItem.freeItem } FREE
                    {else}
                    <span class="rupees">{$rupeesymbol}
					{if $bagItem.discountAmount > 0}
						{math equation="(p - d)" p=$bagItem.totalProductPrice d=$bagItem.discountAmount assign=discounted_price }{$discounted_price|number_format:0:".":","}
						<span class="strike gray">{$bagItem.totalProductPrice|number_format:0:".":","}</span>
					{else}
						{$bagItem.totalProductPrice|number_format:0:".":","}
					{/if}
                    {/if}
				</span></p>
				<p class="price-text">
					{if $bagItem.sizename neq 'freesize'} <label>SIZE:</label>{/if} {if $bagItem.sizenameunified}{$bagItem.sizenameunified}{else}{$bagItem.sizename}{/if}{if $bagItem.sizequantity > 1} <label>/ QTY:</label> {$bagItem.sizequantity}{/if}
				</p>
						
		</li>
	{/if}
	{/foreach}
{/if}
