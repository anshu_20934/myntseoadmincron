
        <div class="winners">
            <span>PRIZES</span>
            <ol>
                {foreach $prizes as $item}
                <li>
                    <span> {$item.label}</span>
                    <img src="{$item.image}">
                    <span> {$item.name}</span>
                </li>
                {/foreach}
            </ol>
            {if !empty($winner_names)}
            <div class="jackpot-winners">
            <h3>Yesterday's Winners</h3>
                {foreach from=$winner_names key=key item=item}
                    <span class="index"> #{$key} </span><span class="name"> - {$item.name}</span><br/>
                {/foreach}
            </div>
            {/if}
        </div>
