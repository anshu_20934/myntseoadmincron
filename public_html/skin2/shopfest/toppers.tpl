
        {if $login}
			<div class="note">
			   {$a} {$userTopMsgForPrize}
			</div>
        {/if}
		<div class="stats">
            <table class="top-all" rules="groups">
                <thead>
                    <tr>
                        <td colspan="2" align="center">Today's Fiery Five (till now)</td>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$topAll key=key item=item}
                    <tr>
			{if $item.name}
						{if $item.login == $login}
							<td class="name"> #{$key +1}. <strong>ME</strong></td>
						{else}
							<td class="name"> #{$key +1}. {$item.name}</td>
						{/if}
		                        
                        <td class="amount">Rs. {$item.amount}</td>
			{else}
                        <td class="name">#{$key +1}. NO TAKERS YET</td>
                        <td class="amount"></td>
			{/if}
                    </tr>
                    {/foreach}
                    <tr class="filler">
                        <td colspan="2">&nbsp;</td>
                    </tr>
                </tbody>
				{if $login}
					<tfoot>
						<tr>
							<td class="name">Me</td>
							{if $userTopAmountAll}
								<td class="amount">Rs. {$userTopAmountAll}</td>
							{else}
								<td class="amount">Rs. 0 </td>
							{/if}
						</tr>
					</tfoot>
				{/if}
            </table>

            <table class="top-women" rules="groups">
                <thead>
                    <tr>
                        <td colspan="2" align="center">Today's Hot Shopper <br/>Women's Wear (till now)</td>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$topWomen key=key item=item}
                    <tr>
						{if $item.name}
									{if $item.login == $login}
										<td class="name"> #{$key +1}. <strong>ME</strong></td>
									{else}
										<td class="name"> #{$key +1}. {$item.name}</td>
									{/if}
			                        <td class="amount">Rs. {$item.amount}</td>
						{else}
			                        <td class="name">#{$key +1}. NO TAKERS YET</td>
			                        <td class="amount"></td>
						{/if}
                    </tr>
                    {/foreach}
                </tbody>
				{if $login}
					<tfoot>
						<tr>
							<td class="name">Me</td>
							{if $userTopAmountWomen}
								<td class="amount">Rs. {$userTopAmountWomen}</td>
							{else}
								<td class="amount">Rs. 0 </td>
							{/if}
						</tr>
					</tfoot>
				{/if}
            </table>

            <table class="top-men" rules="groups">
                <thead>
                    <tr>
                        <td colspan="2" align="center">Today's Hot Shopper <br/>Men's Wear (till now)</td>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$topMen key=key item=item}
                    <tr>
						{if $item.name}
									{if $item.login == $login}
										<td class="name"> #{$key +1}. <strong>ME</strong></td>
									{else}
										<td class="name"> #{$key +1}. {$item.name}</td>
									{/if}
			                        <td class="amount">Rs. {$item.amount}</td>
						{else}
			                        <td class="name">#{$key +1}. NO TAKERS YET</td>
			                        <td class="amount"></td>
						{/if}
                    </tr>
                    {/foreach}
                </tbody>
				{if $login}
					<tfoot>
						<tr>
							<td class="name">Me</td>
							{if $userTopAmountMen}
								<td class="amount">Rs. {$userTopAmountMen}</td>
							{else}
								<td class="amount">Rs. 0</td>
							{/if}
						</tr>
					</tfoot>
				{/if}
            </table>
        </div>
