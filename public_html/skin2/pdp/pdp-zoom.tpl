<div class="zoom-overlay mk-hide"></div>
<div class="zoom-block mk-hide">
{*<div class="zoom-container rs-carousel izoom-carousel">
<div class="close-btn"><span class="close-icon"></span></div>
<ul class="rs-carousel-runner izoom to-be-loaded">
{assign var=i value=0}
 {section name="area" loop=$area_icons}
   <li class="rs-carousel-item {if $smarty.section.area.first}mk-show{else}mk-hide{/if}">
	   <img  class="izoom-imgs" src='{$cdn_base}/skin2/images/spacer.gif'  data-src='{myntraimage key="style_1080_1440" src=$area_icons[area].image|replace:"_images":"_images_1080_1440"}' />
   </li>
{assign var=i value=$i+1}
{/section}
</ul>
<img class="izoom-loader mk-show mk-zoom-loaders" width="35" height="29" src="{$cdn_base}/skin2/images/loader_transparent.gif">
<span class="prev-next-btns" id="view-prev-izoom"></span>
<span class="prev-next-btns" id="view-next-izoom"></span>
</div>*}
<div class="zoom-container rs-carousel szoom-carousel">
<div class="close-btn"><span class="close-icon"></span></div>
<ul class="rs-carousel-runner szoom to-be-loaded">
{assign var=i value=0}
 {section name="area" loop=$area_icons}
   <li class="rs-carousel-item {if $smarty.section.area.first}mk-show{else}mk-hide{/if}">
	   <img class="szoom-imgs" src='{$cdn_base}/skin2/images/spacer.gif'  data-src='{myntraimage key="style_1080_1440" src=$area_icons[area].image|replace:"_images":"_images_1080_1440"}' />
   </li>
{assign var=i value=$i+1}
{/section}
</ul>
<img class="szoom-loader mk-show mk-zoom-loaders" width="35" height="29" src="{$cdn_base}/skin2/images/loader_transparent.gif">
<span class="prev-next-btns" id="view-prev-szoom"></span>
<span class="prev-next-btns" id="view-next-szoom"></span>
</div>
</div>