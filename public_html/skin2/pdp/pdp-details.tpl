           <div class="mk-product-guide mk-f-right {if $pdpv1 neq 'test'} mk-pdpv1-product-guide{/if}" >
				<div id="mk-filler" class="mk-cf">
					<div class="mk-zoom-hide">
						{if $layoutVariant eq 'test'}
							<div class="mk-pdp-carousel-breadcrumbs" {$rs_itemprop_breadcrumb}>
							{if $LocationBreadcrumbABTest neq 'test'}		
								{$navString}
							{/if}
						    </div>
						    <h1 {$rs_itemprop_name}>{$productDisplayName}</h1>  <!-- Product Name -->
						    {if $pdpv1 neq 'test'}<h5>Product Code: {$productStyleId}</h5>{/if} <!-- Product Code -->
					    {else}
					    	{if $pdpv1 neq 'test'}<h5>Product Code: {$productStyleId}</h5>{/if} <!-- Product Code -->
					    	<h1 {$rs_itemprop_name}>{$productDisplayName}</h1>  <!-- Product Name -->
					    {/if}						
						
                        <div class="price-offer-line">
                            <h3 class="red" style="display:inline;" {$rs_itemscope_offer_start}>
                                {$rs_itemprop_currency}
                                {$rs_itemprop_price}
                                Rs.
                                {if $discountamount > 0}
                                    {math equation="x - d" x=$originalprice d=$discountamount assign="finalamount"}{$finalamount|round|number_format:0:".":","}{if $showVatMessage}<span class="vat-asterisk">*</span>{/if} <span class="strike gray">{$originalprice|round|number_format:0:".":","}</span>
                                {else}
                                    {$originalprice|round|number_format:0:".":","}
                                {/if}
                            </h3>  <!-- Product Price -->
                                <span class='coupon-offer-launcher'><span class='coupon-pdp-icon'></span><span class="coupon-pdp-text" style="vertical-align: middle;display:inline-block;padding-bottom:4px">{$pdpCouponWidgetTitleText}</span></span>
                        </div>

					{if !$disableProductStyle}

						<!-- DISCOUNT SECTION HERE -->
						{if $discountlabeltext}
							{if $show_combo}
								<div class="clearfix combo-pdp-block">
							{/if}
							<div class="discount-message-pdp">
								{if $show_combo}
									<span class="discount-message">{include file="string:{$discounttooltiptext}"}</span>
									<br />

								{else}
									<p class="mk-discount">
									{if $discountamount > 0}
										
									{/if}
									{include file="string:{$discountlabeltext}"} </p>
								{/if}
							</div>
							{if $show_combo && $checkout eq 'control'}
								<a class="combo-overlay-btn btn normal-btn"  style="height:25px" data-styleid="{$productStyleId}">CREATE COMBO NOW!</a>
							{elseif $show_combo && $checkout eq 'test'}
								<a class="goto-combo" href="{$http_location}/comboController.php?cid={$discountId}">MORE COMBO OPTIONS</a>
							{/if}

						{if $show_combo}
						</div>
						{/if}
        		    	{/if}
        				<!-- END DISCOUNT HERE-->
                        {if $loyaltyFactor > 1}
                            <div class="loyalty-message">
                                <span class="loyalty-info">PRIVILEGE POINTS - <em>GET {$loyaltyFactor}X POINTS</em> FOR THIS ITEM</span>
                            </div>
                        {/if}	
        				<div class="mk-product-option-cont" >
						{if $pdpColorGrouping && $colourSelectVariant == 'test' && $relatedColorStyleDetailsArray}
							<div class="mk-colour rs-carousel">
								<div class="lbl">{$relatedColorStyleDetailsArray|count} More Colour{if $relatedColorStyleDetailsArray|count gt 1}s{/if}:</div>
							    <ul class="rs-carousel-runner mk-cf">
							    	{foreach from=$relatedColorStyleDetailsArray key=key item=val}
								        <li class="rs-carousel-item">
								            <a href="{$http_location}/{$val.url}{if $nav_id}?nav_id={$nav_id}{/if}" data-id="{$key}">
									            {assign "title" "{$val.base_color}{if $val.color1 neq 'NA' && $val.color1 neq ''} / {$val.color1}{/if}"}
								            	<img src="{myntraimage key='style_48_64' src=$val.image|replace:'_images_180_240':'_images_48_64'|replace:'/style_search_image/':'/properties/'}" height="52" width="39" title="{$title}" alt="{$title}" />
								            </a>
								        </li>
							        {/foreach}
							    </ul>
							    <span class="icon-prev mk-hide"></span>
							    <span class="icon-next mk-hide"></span>
							</div>
						{/if}
						{if $sizeOptionSKUMapping}
						<div class="mk-custom-drop-down mk-size-drop mk-size-drop-pdp">
						{if $allSizeOptionSKUMapping|count == 1}
							{foreach from=$allSizeOptionSKUMapping key=key item=val}
							{assign var=sku value=$key}
								<div class="mk-freesize" data-count="{$val[2]}"
									{foreach $val[3] as $key1 => $val1}
                                		data-{$key1}="{$val1}" 
                                    {/foreach}
								>{$val[0]}</div>
								{if $val[2] <= 3}<div class="mk-count-message">Only <span>{$val[2]}</span> left in stock</div>{/if}
							{/foreach}
							{if $dynamicSizeChart eq 'true' && $sizeRepresentationImageUrl neq ''}
								<button id="sizechart-new" class="link-btn">
									{if $masterCategoryId == 10}
										FIND MY SIZE
									{else}
										NOT SURE OF YOUR SIZE?
									{/if}
								</button>
							{elseif $size_chart_image_path neq ''}
								<button id="sizechart-old"  class="link-btn" data-src="{myntraimage key="size_chart" src=$size_chart_image_path}">Size Chart</button>
							{/if}

                                                        <!-- paroksh  jealous fit info-->
                                                        {if $specificAttributeFitInfo[0]}
                                                        <button id="jealous-fit-guide" class="link-btn">{$brandname} HIP FIT GUIDE</button>
                                                        {/if}
                                                        <!-- end paroksh -->

							{if $productDisclaimerTitle|trim neq ""}
							<div class="mk-product-disclaimer-container" {if $sizeRepresentationImageUrl eq '' && $size_chart_image_path eq ''}style="margin-top:15px"{/if}>
								<div class="mk-product-disclaimer">
									{$productDisclaimerTitle} {if $productDisclaimerText|trim neq ""}<a href="javascript:void(0)" class="mk-product-disclaimer-link link">Know More</a>{/if}
								</div>
							</div>
							{/if}
						{else}
                            <div class="flat-size-options">
                                <div class="lbl">Select a Size</div>
                                <div class="options">
                                    <input type="hidden" name="mk-size" class="mk-size" value="" />
                                    {if $preselectSKU}
	                                    <input type="hidden" name="preselectSKU" id="preselectSKU" value="{$preselectSKU}" />
                                    {/if}	
                                    {$j=1}
                                    {foreach $allSizeOptionSKUMapping as $key => $val}
                                        {if $val[1]}
                                        <button class="btn size-btn {if $renderTooltip}vtooltip{/if}"
                                            data-count="{$val[2]}" 
                                            value="{$key}" 
                                            onclick="sizeSelect({$key},{$j},this)" 
                                            data-tooltipText="{$sizeOptionsTooltipHtmlArray[$key]}" 
                                            data-tooltipTop="{$tooltipTop}" 
                                        	{foreach $val[3] as $key1 => $val1}
                                        		data-{$key1}="{$val1}" 
                                            {/foreach}
                                            >{$val[0]}</button>
                                        {else if $notifymefeatureon && $notifyMeVariant == 'test'}
                                        <button class="btn size-btn unavailable {if $renderTooltip}vtooltip{/if}" 
                                        	title="Sold out - click to request size"
                                            data-count="{$val[2]}" 
                                            value="{$key}" 
                                            onclick="sizeSelect({$key},{$j},this)" 
                                            data-tooltipText="{$sizeOptionsTooltipHtmlArray[$key]}" 
                                            data-tooltipTop="{$tooltipTop}" 
											{foreach $val[3] as $key1 => $val1}
                                        		data-{$key1}="{$val1}" 
                                            {/foreach}
                                            >{$val[0]}
                                            <span class="strike"></span></button>
                                        {/if}
                                        {$j=$j+1}
                                    {/foreach}
                                </div>
                            </div>

							<div class="mk-size-guides">
							{if $dynamicSizeChart eq 'true' && $sizeRepresentationImageUrl neq ''}
							<div id="sizechart-new" class="link-btn">
								<span class="sizechart-icon"></span>
								<span class="sizechart-link-text">
								{if $masterCategoryId == 10}
									FIND MY SIZE
								{else}
									NOT SURE OF YOUR SIZE?
								{/if}
								</span>
							</div>
							{elseif $size_chart_image_path neq ''}
								<button id="sizechart-old"  class="link-btn" data-src="{myntraimage key="size_chart" src=$size_chart_image_path}">Size Chart</button>
							{/if}
							</div>
                                                         <!-- paroksh  jealous fit info-->
                                                        {if $specificAttributeFitInfo[0]}
                                                        <button id="jealous-fit-guide" class="link-btn">{$brandname} HIP FIT GUIDE</button>
                                                        {/if}
                                                        <!-- end paroksh -->
                                                        
							<div class="mk-hide mk-count-message">Only <span>0</span> left in stock</div>
							{if $productDisclaimerTitle|trim neq ""}
							<div class="mk-product-disclaimer-container">
							<div class="mk-product-disclaimer">
								{$productDisclaimerTitle} {if $productDisclaimerText|trim neq ""}<a href="javascript:void(0)" class="mk-product-disclaimer-link link">Know More</a>{/if}
							</div>
							</div>
							{/if}
						{/if}

						</div>

							<div id="add-button-group" class="add-button-group">
								{if $buynowdisabled}
					 				<button class="mk-add-to-cart"> SOLD OUT </button>
									<div class="mk-size-guides">
										{if $dynamicSizeChart eq 'true' && $sizeRepresentationImageUrl neq ''}
											<button id="sizechart-new" class="link-btn">
												{if $masterCategoryId == 10}
													FIND MY SIZE
												{else}
													NOT SURE OF YOUR SIZE?
												{/if}
											</button>
										{elseif $size_chart_image_path neq ''}
											<button id="sizechart-old"  class="link-btn" data-src="{myntraimage key="size_chart" src=$size_chart_image_path}">Size Chart</button>
										{/if}
									</div>
                                     <!-- paroksh  jealous fit info-->
                                    {if $specificAttributeFitInfo[0]}
	                                    <button id="jealous-fit-guide" class="link-btn">{$brandname} HIP FIT GUIDE</button>
                                    {/if}
                                    <!-- end paroksh -->
				 				{else}
				 						<button class="mk-add-to-cart btn primary-btn {if $show_combo && $checkout eq 'test'}mk-add-to-combo{/if}">{$buynowlabel} <span class="proceed-icon"></span></button>
				 					{if $show_combo && $checkout eq 'test'}
										<span class="combo-lbl">AND ADD TO COMBO</span>
					  				{/if}
					 				{if $expressBuyEnabled}<button class="mk-express-buy link-btn">{$expressBuyLabel}</button>{/if}
						            {if $showtelesalesNumber && !$buynowdisabled && $telesalesNumber|trim}
						                <div class="call-to-buy">Or call <em>{$telesalesNumber}</em> to buy</div>
						            {/if}
					 				{if $wishlistenabled && $wishlistVariant == 'test'}
						 				<button class="mk-save-for-later link-btn">{if not $login}Login to {/if}Save in Wishlist</button>
					 				{/if}
									<span class="mk-hide mk-size-error">Select A Size</span>
                                                                        <!--Kuliza Code start -->
                                                                        {include file="socialAction.tpl" echo_flow="pdp" quicklook="no" popupdiv="true"}
                                                                        <script type="text/javascript">
                                                                            var pdp_visit_track = true;    
                                                                        </script>    
                                                                        <!--Kuliza Code end -->
				 				{/if}
				 			</div>

				 			{if $notifymefeatureon && $notifyMeVariant == 'test'}
					 			<div id="notify-cont" class="mk-hide notify-cont">
					 				<p>This size is currently out of stock <br />Do you want to be notified when it is available?</p>
					 				<form class="notify-me-form" id="notify-pdp">
					 					<input type="text" name="notifymeEmail" value="{if $login}{$login}{else}Your email address{/if}" class="notify-email {if !$login}empty{/if}" />
					 					<input type="hidden" name="sku" value="" class="notify-sku" />
					 					<input type="hidden" name="pageName" value="pdp">
					 					<input type="hidden" name="_token" value="{$USER_TOKEN}">
				 						<div class="notify-msg"></div>
					 					<div class="notify-btn-grp">
						 					<button class="notify-button btn normal-btn small-btn">Notify Me</button>
							 				<a href="#" class="notify-cancel" onClick="_gaq.push(['_trackEvent', 'pdp', 'notifyme', 'cancel']);return true;">CANCEL</a>
					 					</div>
					 				</form>
					 			</div>
				 			{/if}
					{else}
						<div class="mk-size-guides">
							{if $dynamicSizeChart eq 'true' && $sizeRepresentationImageUrl neq ''}
								<button id="sizechart-new" class="link-btn">
									{if $masterCategoryId == 10}
										FIND MY SIZE
									{else}
										NOT SURE OF YOUR SIZE?
									{/if}
								</button>
							{elseif $size_chart_image_path neq ''}
								<button id="sizechart-old"  class="link-btn" data-src="{myntraimage key="size_chart" src=$size_chart_image_path}">Size Chart</button>
							{/if}
						</div>
                         <!-- paroksh  jealous fit info-->
                        {if $specificAttributeFitInfo[0]}
                            <button id="jealous-fit-guide" class="link-btn">{$brandname} HIP FIT GUIDE</button>
                        {/if}
                        <!-- end paroksh -->
						{if $productDisclaimerTitle|trim neq ""}
							<div class="mk-product-disclaimer-container">
							<div class="mk-product-disclaimer">
								{$productDisclaimerTitle} {if $productDisclaimerText|trim neq ""}<a href="javascript:void(0)" class="mk-product-disclaimer-link link">Know More</a>{/if}
							</div>
							</div>
						{/if}
						<div class="mk-sold-out">This product is currently sold out</div>
					{/if}
					</div>
				{else}
					<div class="mk-size-guides">
						{if $dynamicSizeChart eq 'true' && $sizeRepresentationImageUrl neq ''}
							<button id="sizechart-new" class="link-btn">
								{if $masterCategoryId == 10}
									FIND MY SIZE
								{else}
									NOT SURE OF YOUR SIZE?
								{/if}
							</button>
						{elseif $size_chart_image_path neq ''}
							<button id="sizechart-old"  class="link-btn" data-src="{myntraimage key="size_chart" src=$size_chart_image_path}">Size Chart</button>
						{/if}
					</div>
                     <!-- paroksh  jealous fit info-->
                    {if $specificAttributeFitInfo[0]}
                        <button id="jealous-fit-guide" class="link-btn">{$brandname} HIP FIT GUIDE</button>
                    {/if}
                    <!-- end paroksh -->
					{if $productDisclaimerTitle|trim neq ""}
						<div class="mk-product-disclaimer">
							{$productDisclaimerTitle} {if $productDisclaimerText|trim neq ""}<a href="javascript:void(0)" class="mk-product-disclaimer-link link">Know More</a>{/if}
						</div>
					{/if}
					<div class="mk-sold-out">This product is currently sold out</div>
				{/if}
				</div>
			</div>
			{if $pdpv1 neq 'test'}

				{assign var='fl' value=0}
				{if $style_note|trim || $descriptionAndDetails|trim || $materials_care_desc|trim || $size_fit_desc|trim}
				<ul class="mk-product-helper mk-cf">
					<li class="desc-main-title">Product Description</li>
			 		{if $style_note|trim neq ""}
			 			<li class="desc-type-title open-state"><span class="icon"></span>Style Note</li>
			 			<li class="desc-text " {$rs_itemprop_description}>{$style_note}</li>
			 			{assign var='fl' value=1}
			 		{/if}
			 		{if $descriptionAndDetails|trim neq ""}
			 			<li class="desc-type-title {if $fl eq 0}open-state{else}close-state{/if}"><span class="icon"></span>Product Details</li>
			 			<li class="desc-text {if $fl eq 1}mk-hide{/if}">{$descriptionAndDetails}</li>
			 			{assign var=fl value=1}
			 		{/if}
			 		{if $materials_care_desc|trim neq ""}
			 			<li class="desc-type-title {if $fl eq 0}open-state{else}close-state{/if}"><span class="icon"></span>Material & Care</li>
			 			<li class="desc-text  {if $fl eq 1}mk-hide{/if}">{$materials_care_desc}</li>
			 			{assign var=fl value=1}
			 		{/if}
			 		{if $size_fit_desc|trim neq ""}
			 			<li class="desc-type-title {if $fl eq 0}open-state{else}close-state{/if}"><span class="icon"></span>Size & Fit</li>
			 			<li class="desc-text  {if $fl eq 1}mk-hide{/if}">{$size_fit_desc}</li>
			 		{/if}
			 	</ul>
				{/if}
				
			{/if}


					{if $SoldByVendorText}
						<div id="vendorDetails">
							<div><span>Sold by</span><span>{$SoldByVendorText}</span></div>
							{if $ShowSuppliedByPartner}
							<div class="supplier-details"><span>Supplied by</span><span>Partner<a data-tooltiptext="{$SuppliedByPartnerTooltip}"> [?] </a></span></div>
							{/if}
						</div>
					{/if}
					
						
					

			 		<!-- FORM HERE -->

                    <form action="/mkretrievedataforcart.php?pagetype=productdetail" id="hiddenForm" method="post" class="add-to-cart-form" >
                    	<input type="hidden" name="_token" value="{$USER_TOKEN}" />
                        <input type="hidden" id="count" value="{$i}">
                        <input type="hidden" name="productStyleId" id="productStyleId" value="{$productStyleId}">
                        <input type="hidden" name="uploadedImagePath" id="imagepath" value="">
                        <input type="hidden" name="unitPrice" value="{if $discountprice}{$discountprice}{else}{$originalprice}{/if}">
                        <input type="hidden" name="pagetype" value="productdetail">
                        <input type="hidden" name="totalOptions" id="totalOptions" value="{$Options}">
                        <input type="hidden" name="productName" id="productName" value="{$designer[6]}">
                        <input type="hidden" name="productTypeName" id="productTypeName" value="{$productTypeLabel}">
                        <input type="hidden" name="productStyleLabel" value="{$productDisplayName}">
                        <input type="hidden" name="productTypeId" id="productTypeId" value="{$productTypeId}">
                        <input type="hidden" name="gender" value="{$gender}">
                        <input type="hidden" name="brand" value="{$brandname}">
						<input type="hidden" name="articleType" value="{$articleType}">

                        <input type="hidden" name="referrer" value="productDetail">
                        <input type="hidden" name="referredTo"  id= "referredTo" value="">
                        <input type="hidden" name="quantityMenu" id="quantityMenu" size="3" maxlength="4" value="1">
                        <input type="hidden" name="productSKUID" id="productSKUID" value="{$sku}" >
                        <input type="hidden" name="selectedsize" value="">
                        <input type="hidden" name="quantity"  id="quantity" value="1">
                        {if $show_combo && $checkout eq 'test' }
                        	<input type="hidden" name="comboProduct" value="1">
                        	<input type="hidden" name="comboId" value="{$discountId}">
                        {/if}
						{**AVAIL*}
						<input type="hidden" name="uqs"  id="uqs" value="{$uqs}">
						<input type="hidden" name="uq"  id="uq" value="{$uq}">
						<input type="hidden" name="t_code"  id="t_code" value="{$t_code}">
						{**END AVAIL*}
                        {if $productOptions}
                            {section name=options loop=$productOptions}
                            <input type="hidden" name="option{$smarty.section.options.index}name"  id="option{$smarty.section.options.index}name"  value="{$productOptions[options][0]}">
                            <input type="hidden" name="option{$smarty.section.options.index}value" id="option{$smarty.section.options.index}value" value="">
                            <input type="hidden" name="option{$smarty.section.options.index}id" id="option{$smarty.section.options.index}id"  value="" >
                            {/section}
                        {/if}

                        {assign var='i' value=1}
                        {if $sizeOptions}
                            {foreach from=$sizeOptions key=key item=val}
                                <input type="hidden" name="sizequantity[]" id="sizequantity{$i}" class="sizequantity">
                                <input type="hidden" name="sizename[]" id="sizename{$i}" value="{$key}">
                                {assign var='i' value=$i+1}
                            {/foreach}
                        {else}
                            <input type="hidden" name="sizequantity[]"  class="sizequantity">
                            <input type="hidden" name="sizename[]" id="sizename1" value="NA">
                        {/if}
                    </form>

                   <form action="mksaveditem.php?actionType=add"  method="post" class="save-for-later-form" >


                   </form>
		<!-- END FORM -->

			</div> <!-- End .product-guide -->
