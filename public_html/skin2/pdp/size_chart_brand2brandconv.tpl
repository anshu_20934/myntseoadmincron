<div class="tab left hide brand-size" data-tabid="1">
    <div class="mk-clear brand-size-title-container">
        <div class="brand-size-title tooltip">
            Get the closest fit in <b>{$style_attributes.brand} {$style_attributes.article_type_name}</b> based on <b>Brand that you wear</b>
        </div>
        <div class="brand-size-title-arrow"></div>
        <div class="brand-size-title-arrow-inner"></div>
    </div>
    <div class="brand-size-selectors">
        <div class="brand-size-brand-selector">
            <select name="brand" class="brand fit"></select>
        </div>
        <div class="brand-size-size-selector hide">
            <select name="size" class="size fit"></select>
        </div>
    </div>
    <div class="mk-clear hide suggested" >
        <div class="best-fit-size-text"></div>
        <div class="suggested-sizes mk-clear"></div>
        <div class="size-unavailable-text-container">
            <div class="size-unavailable-text hide">Sorry! Size is not available</div>
        </div>
        <div class="measurement-diff-text hide"></div>
        <div class="select-size-div hide">
            <button class="btn primary-btn select-size">Select this Size</button>
        </div>
    </div>    
</div>
