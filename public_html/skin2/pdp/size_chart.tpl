	<script>
			Myntra.Data.articleType='{$style_attributes.article_type_name}';
			Myntra.Data.articleTypeId ={$style_attributes.global_attr_article_type};
			Myntra.Data.ageGroup='{$style_attributes.age_group}';
			Myntra.Data.sizeMeasurementsObj = {$sizeOptionMeasurements};
			Myntra.Data.styleSizeAttributes = {$styleSizeAttributes};
			Myntra.Data.brand = '{$style_attributes.brand}';
			//PORTAL-2116: Kundan: on click of size chart, track the style-ids in cookies
			//the cookie constants need to be saved in a variable so that they can be used in sizechart.js
			{if $trackSizeChartClicks}
				Myntra.Data.sizechartCookieConstants = {$cookieConstants|@json_encode};
			{/if}
			var sizeImage = '{$sizeRepresentationImageUrl}';
			if(sizeImage.substr(0,4).toLowerCase() != 'http') 
				Myntra.Data.sizeImg='{myntraimage key="size_chart" src=$cdn_base|cat:"/"|cat:$sizeRepresentationImageUrl}';
			else 
				Myntra.Data.sizeImg='{myntraimage key="size_chart" src=$sizeRepresentationImageUrl}';
				
			$(document).ready(function(){
				{if $measurementGuideText}
				$(".tab-btn").bind("click",function(){
					$(".tab-btn").toggleClass("link-tab"); 
					$(".tab-btn").toggleClass("active-tab");
					var tabid = $(this).attr("data-tabid");
					$(".tab-btn").removeClass("active-tab").addClass("link-tab");
					$(".tab-btn[data-tabid="+tabid+"]").removeClass("link-tab").addClass("active-tab"); 
					$(".tab").hide();
					$(".tab[data-tabid="+tabid+"]").show();
					var maxHeightCSS = parseInt($('.measure-guide').css('max-height'));
					var scrollHeight = $('.measure-guide').prop('scrollHeight');
					//console.log("maxCSSHeight: ", maxHeightCSS, " scrollHeight: ",scrollHeight);
					if(tabid == 1 && (scrollHeight > maxHeightCSS)) {
						$(".measure-guide").jScrollPane();
					};
				});
				{/if}
				$(".best-fit-size-text").html("<H3>Select Brand & Size to get the best fit in "+Myntra.Data.brand+" "+Myntra.Data.articleType+" "+Myntra.Data.ageGroup+"</H3>");
				$("div.fit-comparison-title").html("<H3>Select Brand & Size to compare fitment with "+Myntra.Data.brand+" "+Myntra.Data.articleType+"</H3>");
			});
		</script>
    	<div class="hd">
    		<h2>{$style_attributes.brand} {$style_attributes.article_type_name}</h2>
    	</div>
    	
   		<div class="bd mk-cf">
   		<div>
			<!-- Portal-2053: Kundan: this should appear in tabs now. -->  
			<div id="tab-list" class="mk-f-left lft-cont">
				<!-- The tab buttons to switch -->
				<div class="tab-btns">
					<ul>
						<li data-tabid="-1" class="disabled-tab"></li>
						<li data-tabid="0" class="active-tab tab-btn {if $isMobile} default{/if}">Size Options</li>
                        {if !$isMobile && $masterCategoryId == 10}
						<li data-tabid="1" class="link-tab tab-btn default">Find My Size</li>
                        {/if}
						{if $measurementGuideText}
						<li data-tabid="2" class="link-tab tab-btn">Measurement Guide</li>
						{/if}
						<li data-tabid="-1" class="disabled-tab"></li>
					</ul>
				</div>
				<!-- Begin 1st tab -->
				<div class="tab left" data-tabid="0"> <!-- 1st Tab: activated by default -->
						<div class="size-scrollable-box clearfix">
							{**REPEAT START**}
							{assign var='flag' value=$sizearraylength-1}
							{section name=sizearray loop=$sizearraylength-1}
							{if  $smarty.section.sizearray.index > 0}
								{if $totalMappedSizes <= 5}<div class="rowCont mk-clear ">{/if}
								{if $smarty.section.sizearray.index == 1}
									<span class="size-label bold mk-f-left">{$sizeOptionsLabel[$smarty.section.sizearray.index]}</span>
								{else}
									<span class="size-label mk-f-left mk-clear mk-cf">{$sizeOptionsLabel[$smarty.section.sizearray.index]}</span>
								{/if}
								<div id="size-scrollable_{$smarty.section.sizearray.index}" class="size-scrollable rs-carousel mk-f-left {if $smarty.section.sizearray.index == 1}size-scroll-btns bold size-val{else}size-scroll-equi{/if}">
									<ul class="rs-carousel-runner {if $smarty.section.sizearray.index == 1}size-op {else} equi-sizes{/if} size-items left " id="{if $smarty.section.sizearray.index == 1}size-select{/if}">
									{foreach name=sizesArray key=key item=sizesArrayItem from=$sizeOptionsArray }
										<li data-value="{$sizesArrayItem[0]}" {if $smarty.foreach.sizesArray.last}style="border:none"{/if} class="item rs-carousel-item {if $sizesArrayItem[$flag] eq 'false' && $smarty.section.sizearray.index == 1}size-inactive{else}size-available{/if}">{$sizesArrayItem[$smarty.section.sizearray.index]}{if $sizesArrayItem[$flag] eq 'false' && $smarty.section.sizearray.index == 1}<span class="strike-line"></span>{/if}</li>
									{/foreach}
									</ul>
								</div>
								{if $totalMappedSizes <= 5}</div>{/if}
							{/if}
						{/section}
						{**REPEAT END**}
						<span class="out-of-stock mk-clear">Out Of Stock</span>
						</div>
						<div id="sub-btn" class="mk-cf">
							<span class="sub-btn btn primary-btn">{$buynowlabel} <span class="proceed-icon"></span></span>
						</div>
				
					<form class="re-form-block" id="sizeForm-{$styleid}" action="/mkretrievedataforcart.php?pagetype=productdetail&amp;lead=size_chart" method="post">
						<input type="hidden" name="_token" value="{$USER_TOKEN}" />
	                	<input type="hidden" value="{$styleid}" name="productStyleId">
	                	<input type="hidden" value="1" name="quantity">
	                	<input type="hidden" class="sizename" value="" name="sizename[]">
	                	<input type="hidden" value="1" name="sizequantity[]">
	                	<input type="hidden" name="selectedsize" value="">
	              		<input type="hidden" name="productSKUID" class="productSKUID" value="">
	              		<input type="hidden" name="productStyleLabel"  value="{$productDisplayName}">
	              		<input type="hidden" name="brand" value="{$style_attributes.brand}">
	            	</form>
                  {if $style_attributes.brand_logo neq ''}
                  <div class="brand-logo-cont">
                     <img class="brand-logo" src="{$style_attributes.brand_logo}" />
                  </div>
		 		  {/if}
				</div> <!-- End of 1st Tab -->
		  		<!-- Begin 2nd-tab for brand to brand conversion -->
                {if !$isMobile && $masterCategoryId == 10}
		  		{include file="pdp/size_chart_brand2brandconv.tpl"}
                {/if}
                <!-- End for brand to brand conversion tab -->
		  		<!-- If measurementGuideText is not defined, then don't show any more tab -->
				{if $measurementGuideText}
		 		<!-- Begin 3rd-tab for measurements -->
		 		<div class="tab left hide" data-tabid="2">
			 		<div class="measure-guide" >
						{$measurementGuideText}					
					</div>
					{if $measurementNote}
					<div class="ft">
						{$measurementNote}
					</div>
					{/if}
                  {if $style_attributes.brand_logo neq ''}
                  <div class="brand-logo-cont">
                     <img class="brand-logo" src="{$style_attributes.brand_logo}" />
                  </div>
		 		  {/if}
				</div> <!-- end of 3rd tab -->
		 		{/if}
		  </div> <!-- End tabs -->
		  <div class="mk-f-right rgt-cont size-measurements" id="measurement-box">
			<span class="size-image main-image" /></span>
		  </div>
		  </div>
		  <div class="clearfix">&nbsp;</div>
		  {if $measurementToleranceText}
			<div class="measurement-type-text">				
			{$measurementToleranceText}	
			{if $measurementMasterCategory}All measurements shown are of the {$measurementMasterCategory}. {/if}
			</div>			
		  {/if}			  
		</div>	
