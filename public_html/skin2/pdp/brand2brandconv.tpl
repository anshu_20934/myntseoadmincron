<!DOCTYPE html>
<html>
<head>
    {include file="inc/head-meta.tpl"}
    {include file="inc/head-css.tpl"}
    {include file="inc/head-script.tpl"}
    {assign var=megaFooterEnabled value=false}
    {block name=pagecss}{/block}
</head>
<body class="{$_MAB_css_classes} {if $smarty.server.HTTPS}secure{/if}">
    <div class="mk-body">
	    <div class="mk-wrapper">
    	    {block name=body}{/block}
    	</div>
    </div>
    {block name=lightboxes}{/block}
	{include file="inc/body-css.tpl"}
    {include file="inc/body-script.tpl"}
    {block name=pagejs}{/block}
</body>
</html>
{block name=pagecss}
{literal}
<style type="text/css">
	label {display:inline;}
	table tr td {margin: 50px; border: 1px solid lightgray; vertical-align: top}
	.bestfit {background-color: lightgray;}
	.measurementsdiff {font-size: 0.7em; font-family:verdana, arial; text-transform: none; }
	li {list-style-type: square;}
	li ul li {list-style-type: circle;}
</style>
{/literal}
{/block}
{block name=body}
<H1>This is Brand to Brand Size Conversion Page</H1>
<div id="brand2brand">
    <div class="mod">
        <div class="hd">
            <div class="title">Compare Sizes Across Brands</div> <!-- brand name here -->
        </div>
        <div class="bd body-text">
        	<form name="" method="POST" action="brand_sizing.php">
 				<table>
 					<tbody>
 						<tr> <td colspan="2"><H2>I'm looking for:</H3></td> </tr>
 						<tr> 
 							<td colspan="2">
 								<span>
 									<label for="ageGroup">Age-Group:</label>
        							<select name="ageGroup" class="age-group fit">
        								<option value="-1">--Select Age-Group--</option>
        								{foreach from=$ageGroups item=eachAgeGroup}
        								<option>{$eachAgeGroup}</option>
        								{/foreach}
        							</select>
        						</span>
        						<span>
        							<label for="articleType">Article-Type:</label>
        							<select name="articleType" class="article-type fit"></select>			
        						</span>
							</td> 
						</tr>
						<tr>
							<td>
								<div>
        							<H3>The Brand-Size that fits me:</H3>
        							<select name="brand" class="brand fit"></select>
        							<select name="size" class="size fit"></select>
        							<br/>
        							<textarea rows="2" cols="6" style="width:200px; height:100px" name="measurements" class="measurements fit"></textarea>
        						</div>				
							</td>
							<td>
								<div>
        							<H3>The brand I want to buy:</H3>
        							<select name="desiredbrand" class="brand desired"></select>
        							<select name="desiredsize" class="size desired"></select>
        							<br/>
        							<textarea rows="2" cols="6" style="width:200px; height:100px" name="desiredmeasurements" class="measurements desired"></textarea>
        						</div>			
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div class="measurementsdiff"></div>
							</td>
						</tr>
						
 					</tbody>
 				</table>
             </form>           
        </div>
    </div>
</div>
{/block}
{block name="pagejs"}
<script language="javascript">
{literal}
$(document).ready(function(){
	$("select.age-group").change(function(){
		var selectedAgeGroup = $("option:selected",this).val();
		if(selectedAgeGroup!= -1){
			populateComboViaAjax("fetchArticleTypes",$("select.article-type"),"--Select Article Type--",selectedAgeGroup);
		}
	});

	$("select.article-type").change(function(){
		var selectedAgeGroup = $("select.age-group option:selected").val();
		var selectedArticleType = $("option:selected",this).val();
		if(selectedAgeGroup != -1 && selectedArticleType != -1){
			populateComboViaAjax("fetchBrands",$("select.brand"),"--Select Brand--",selectedAgeGroup,selectedArticleType);
		}
	});

	var measurementDiffs = {}, targetSizes= {};
	$("select.brand").change(function(){
		var selectedAgeGroup = $("select.age-group option:selected").val();
		var selectedArticleType = $("select.article-type option:selected").val();
		var selectedBrand = $("option:selected",this).val();
		var selectedSize = $("select.size.fit option:selected").val();
		if(selectedAgeGroup != -1 && selectedArticleType != -1){
			if($(this).hasClass("desired") && selectedSize!=-1){
				populateComboViaAjax("fetchSizes",$("select.size.desired"),"--Select Size--",selectedAgeGroup,selectedArticleType,selectedBrand);
			}
			else {
				populateComboViaAjax("fetchSizes",$("select.size.fit"),"--Select Size--",selectedAgeGroup,selectedArticleType,selectedBrand);
			}
		}
	});

	$("select.size").change(function(){
		var selectedValue = $("option:selected",this).val();
		if(selectedValue != -1){
			var selectedSizeData = $("option:selected",this).attr("data-extra");
			var textAreaSelector = $(this).hasClass("desired") ? $("textarea.measurements.desired") : $("textarea.measurements.fit");
			textAreaSelector.val(selectedSizeData);
			if($(this).hasClass("desired")){
				console.log(measurementDiffs["diffs"][selectedValue]);
			}
		}
	});

	$("select").not(".size").change(function(){
		var selector = $(this).hasClass("desired") ? "textarea.measurements.desired" : "textarea.measurements"; 
		$(selector).val("");
	});
	
	var populateComboViaAjax = function(actionType, jqComboSelector,defaultOptionText, ageGroup, articleTypeId, brand){
		var params = "actionType="+actionType+"&ageGroup="+ageGroup;
		if(typeof articleTypeId != 'undefined'){
			params += "&articleTypeId="+articleTypeId;
			if(typeof brand != 'undefined'){
				params += "&brand="+brand;
			}
		}		
		$.ajax({
			type:"POST",
			url:"/brand_sizing.php",
			data:params,
			beforeSend:function(){
				$("select").attr("disabled","disabled");
			},
			success: function(data){
				if(data) {
					populateComboViaJson(data, jqComboSelector,defaultOptionText);	
				}
			},
			complete: function(){
				$("select").removeAttr("disabled");
				if(jqComboSelector.hasClass("desired") && jqComboSelector.hasClass("size")){
					populateTargetSizes(jqComboSelector);
					populateMeasurementDiffs();
					displayMeasurementDiffs();
				}
			},
		});
	}; 
	
	var populateComboViaJson = function(json, comboSelector, defaultOptionText){
		var jobj = (typeof json === "string")? $.parseJSON(json) : json;
		var comboOptionsHtml = "<option value='-1'>"+defaultOptionText+"</option>";
		for(id in jobj){
			var value = jobj[id];
			var extraData="";
			var optionText;
			if(value && (value instanceof Array)){
				optionText = value[1];
				if(value.length > 2) {
					extraData = value[2];
				}
			}
			else if(value){
				optionText = value;
			}
			comboOptionsHtml = comboOptionsHtml + "<option data-extra='"+extraData+"' value='"+id+"'>"+optionText+"</option>";
		} 
		comboSelector.html(comboOptionsHtml);
	};

	var populateTargetSizes = function(jqSelect){
		targetSizes = {};
		$("option",jqSelect).each(function(){
			targetSizes[$(this).val()] = $(this).text();
		});
	};

	var populateMeasurementDiffs = function(){
		measurementDiffs = {};
		var selectedSizeMeasurements = $("select.size.fit option:selected").attr("data-extra");
		var minSSD = 10000;
		var minSSDSize = -1;
		$("select.size.desired option").each(function(){
			var selectedSizeValue = $(this).val();
			if(selectedSizeValue != -1){
				measurementDiffs[selectedSizeValue] = compareMeasurements(selectedSizeMeasurements, $(this).attr("data-extra"));
				//find the minimum SSD & Index
				if(measurementDiffs[selectedSizeValue]["ssd"] < minSSD ){
					 minSSDSize = selectedSizeValue;
					 minSSD = measurementDiffs[selectedSizeValue]["ssd"];
				}  
			}
		});
		measurementDiffs = {"best-fit-size":minSSDSize,"diffs":measurementDiffs}; 
	};
	
	var compareMeasurements = function(mjson1, mjson2){
		var mjobj1 = JSON.parse(mjson1);
		var mjobj2 = JSON.parse(mjson2);
		var measurementsToCompare = {};
		var ssd = 0;//represents sum of squared differences
		for(measurement in mjobj1){
			if(typeof mjobj2[measurement] !== 'undefined'){
				//now check if their type is flat and their units are the same
				if(mjobj2[measurement]["type"] === "flat" && mjobj1[measurement]["type"] === "flat" 
					&& mjobj1[measurement]["value"]["unit"] === mjobj2[measurement]["value"]["unit"]){
					var value1 = parseFloat(mjobj1[measurement]["value"]["value"]);
					var value2 = parseFloat(mjobj2[measurement]["value"]["value"]);
					var diff = value2-value1;
					ssd += diff*diff;
					measurementsToCompare[measurement] = [value1,value2,diff,mjobj2[measurement]["value"]["unit"]];	
				} 
			}
		}
		measurementsToCompare["ssd"] = parseFloat(Math.sqrt(ssd).toFixed(2));
		/*for(index in measurementsToCompare){
			if(index != 'ssd'){
				console.log("Measurement: "+index+"; src:"+measurementsToCompare[index][0]+"; target:"+measurementsToCompare[index][1]+"; difference: "+measurementsToCompare[index][2].toFixed(2)+" "+measurementsToCompare[index][3]);
			}
			else {
				console.log("ssd="+ssd);
			}
		}*/
		return measurementsToCompare;
	};

	var displayMeasurementDiffs = function(){
		var bestFitSizeId = measurementDiffs["best-fit-size"];
		var bestFitSize = targetSizes[bestFitSizeId];
		var diffs = measurementDiffs["diffs"];
		console.log(diffs);
		var diffMsg = "";//"Best Match:"+bestFitSize+"<br>";
		for(sizeValue in diffs){
			var diff = diffs[sizeValue];
			diffMsg += (sizeValue !== bestFitSizeId)? "<li>":"<li class='bestfit'>";
			diffMsg += "<ul>"+targetSizes[sizeValue]+" in "+$("select.brand.desired option:selected").text();
			for(measurement in diff){
				if(measurement != "ssd"){
					var measurementValue = diff[measurement];
					var moreOrLess = (measurementValue[2] > 0)?"more":"less";
					var diffEachMeasureMsg = measurement+": "+" measures "+Math.abs(measurementValue[2]).toFixed(2)+" "+measurementValue[3]+" "+moreOrLess+" than "+$("select.brand.fit option:selected").text();
					diffMsg += "<li>"+diffEachMeasureMsg+"</li>";		
				}
			}
			diffMsg += "</ul></li>";
		}
		$("div.measurementsdiff").html(diffMsg);
	};
});	
{/literal}
</script>
{/block}