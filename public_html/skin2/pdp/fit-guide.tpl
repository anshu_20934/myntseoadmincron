{* paroksh... this has to be made generic..so that it can be apply to other brands too.. *}
<div id="jealous-fit-guide-info" class="lightbox fitguidebox">
    <div class="mod">
        <div class="hd">
            <div class="title">{$specificAttributeFitInfo[1]['brand_name']}</div> <!-- brand name here -->
            <div class="tag">{$specificAttributeFitInfo[1]['guide']}</div>
        </div>
        <div class="bd body-text">
            <div class="fit-info lft-cont">
                <div class="jealous-fitguide-info">{$specificAttributeFitInfo[1]['size_count']}</div>
                <div class="jealous-fitguide-info">{$specificAttributeFitInfo[1]['info']}</div>
                <div class= "jealous-fitguide-table">
                    <table class="fit-table">
                    {$cnt =1}
                    {foreach $specificAttributeFitInfo[1]['table'] as $key => $value}
                        {if $cnt==1} 
                            <tr class="first">
                        {elseif $cnt%2==1}
                            <tr class="even">
                        {else}
                            <tr>
                        {/if}
                            {foreach $value as $key_n => $value_n}
                                <td>{$value_n[0]}<br/><span>{$value_n[1]}</span></td>
                            {/foreach}

                        </tr>
                        {$cnt = $cnt + 1}
                    {/foreach}
                    
                    </table>
                </div>
                <div class="mk-f-left">
                    <ul>
                        <li>HIP - Widest part of the body below the waist</li>
                        <li>WAIST - Slimmest part of the natural waistline, above navel and below ribcage</li>
                    </ul>
                </div>

                <div class="footer-text">
                {if $brand_logo neq ""}
                    <a href="{$http_location}/{$brandname|lower|replace:' ':'-'}" title="{$brandname}"><img src="{$brand_logo}" width="120" height="70"></a>
                {/if}
                </div>
                
                 <div class="footer-text">
                    {$specificAttributeFitInfo[1]['footer-text']}
                </div>
                
            </div>

            <div class="hip-fit rgt-cont">
            <img src="http://myntra.myntassets.com/skin2/images/hip-fit.png" />
            </div>
            
        </div>
        <div class="ft"> </div>
        
    </div>
</div>

