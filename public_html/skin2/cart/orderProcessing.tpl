<html>
<head>
<title>Processing Myntra Order</title>
</head>
<body>    
    {assign var=content value={include file="checkout/paymentError.tpl"}}
    {assign var=errContent value=$content|regex_replace:"/[\r\t\n]/":" "}
    <center><img class="test" src="{$secure_cdn_base}/images/image_loading.gif"></center>
    <br>
    <center><b>Processing Payments... </b></center>
    {literal}
        <script type="text/javascript">
            var parentWindow = window.opener;
            var winHTTPLocation = {/literal}'{$http_location}';{literal}
            var winHTTPSLocation = {/literal}'{$https_location}';{literal}
            var redirectLocation = {/literal}'{$redirect_url}';{literal}
            var message = {/literal}'{$redirect_message}';{literal}            
            var index =-1;
            var isGiftCardOrder = {/literal}'{$isGiftCardOrder}';{literal}
            var isSuccOrder = {/literal}'{$isSuccessOrder}';{literal}
            var ispayFailOpt = {/literal}'{$payFailOpt}';{literal}
            var errContent = {/literal}'{$errContent}';{literal}
            var ispayUserCloseOpt = {/literal}'{$payUserCloseOpt}';{literal}
            var isMobile = {/literal}'{$isMobile}';{literal}
        
            if(parentWindow!=null) {
                //check if parent window is myntra window if yes close this window and open confirmation page in parent window else open it in this window itself
                try {
                    var parentDomain = parentWindow.document.domain.toLowerCase();
                    index = winHTTPLocation.toLowerCase().indexOf(parentDomain);
                } catch (err) {
                }

                if(index==-1) {
                    index = winHTTPSLocation.toLowerCase().indexOf(parentDomain);
                }

                if(index==-1) {
                    //parent window has some other domain open confirmation page in this window it self
                    window.location.href = redirectLocation;
                } else {
                        if(isGiftCardOrder == '1') {
                            var parentDialogHeader = parentWindow.document.getElementById('splash-payments-processing-hd');
                            if(parentDialogHeader!=null) {
                                parentDialogHeader.innerHTML = "<h2>"+message+"<h2>";
                            }
                    
                            var parentDialogBody = parentWindow.document.getElementById('splash-payments-processing-bd-pp');
                            if(parentDialogBody!=null) {
                                parentDialogBody.innerHTML = "<center>Please wait while redirecting</center>";
                            }
        
                            var parentCancelButton = parentWindow.document.getElementById('cancel-payment-btn');
                            if(parentCancelButton!=null) {
                                parentCancelButton.innerHTML="";
                            }

                            parentWindow.location.href = redirectLocation;
                        }
                        else if(isSuccOrder)
                        {
                            parentWindow.location.href = redirectLocation;                           
                            parentWindow.document.getElementById('processing-payment').style.display='none';
                            parentWindow.document.getElementById('result-succPayment').style.display='block'; 
                        }
                        else if(ispayFailOpt == 'control' || isMobile)// Code for "test" mode for ispayFailOpt AB test is not working on mobile browsers
                                                                      // hence we are falling back to "control" mode for this AB test on mobile browsers
                        {
                            parentWindow.location.href = redirectLocation;  
                            parentWindow.document.getElementById('processing-payment').style.display='none';
                            parentWindow.document.getElementById('result-errPayment').style.display='block';
                        }
                        else if(ispayFailOpt == 'test')
                        {
                            var lightBoxBody = parentWindow.document.getElementById('splash-payments-processing-bd-pp-payFailOver-err');
                            if(lightBoxBody!=null) {
                                lightBoxBody.innerHTML = '<center >'+errContent+'</center>';
                            }
                            parentWindow.document.getElementById('processing-payment').style.display='none';
                            parentWindow.document.getElementById('result-payFailOver-err').style.display='block';
                            var mobileErrorDiv = parentWindow.document.getElementById('error-div');
                            if(mobileErrorDiv!=null){
                                mobileErrorDiv.innerHTML = '<center >'+errContent+'</center>';
                                var codTab = parentWindow.document.getElementById('pay_by_cod');
                                if (codTab!=null){
                                    codTab.click();
                                }
                            }
                        }
                    //close this window and open the confirmation page in parent window.
                    //Do not focus to parent window on mobile devices as it closes the parent window instead of the child.
                    if(!isMobile){
                        parentWindow.focus();
                    }
                    window.close()
                }                
            } else {
                window.location.href = redirectLocation;
            }
        </script>
    {/literal}
</body>
</html>
