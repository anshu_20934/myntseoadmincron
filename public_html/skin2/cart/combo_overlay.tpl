<!-- Header -->
<script>
    Myntra.Combo.isConditionMet = {$isConditionMet|@json_encode};
    Myntra.Combo.selectedStyles = {$selectedStyles|@json_encode};
    Myntra.Combo.min = parseInt({$min}+0.5);
    Myntra.Combo.ctype = "{$ctype}";
    Myntra.Combo.cartSavings = parseInt({$comboSavings}+0.5);
    Myntra.Combo.cartTotal = parseInt({$comboTotal}+0.5);
    Myntra.Combo.products = {$products|@json_encode};
    Myntra.Combo.solrProducts = {$dataMap|@json_encode};
    Myntra.Combo.isAmount = {if $productsSet.dre_buyAmount>0} true; {else} false; {/if}
    Myntra.Combo.ComboId = {$comboId};
    Myntra.Combo.freeItem = Object();
    Myntra.Combo.discountedCount = parseInt({$productsSet.dre_count});
    Myntra.Combo.itemName = "{$dre_itemName|escape:'quotes'}";
    Myntra.Combo.itemPrice = '{$dre_itemPrice}';
    {literal}
    if(Myntra.Combo.itemName != undefined && Myntra.Combo.itemPrice != undefined){
        Myntra.Combo.freeItem = { name : Myntra.Combo.itemName , price : Myntra.Combo.itemPrice };
    }
    {/literal}
</script>
<div class="acombo">
<!-- Combo Message -->
<div class="combo-header-main" >
	{include file="string:{$displayText}"}
</div>

<!-- Conditional Message -->
<div class="combo-header-sub" >
	<div class="completion-message">{if $isConditionMet}<span class="tick-small-icon">&nbsp;</span> Combo complete! <em>Modify Combo</em> {else}<span class="warning-small-icon">&nbsp;</span> Combo incomplete. <em>{if $productsSet.dre_buyAmount}{$rs}{/if}{$min}{if $productsSet.dre_buyAmount}/-{/if} more to go!</em> {/if}</div>
	<!--<a>CLEAR COMBO</a>-->
</div>

<!-- Pagination Message -->
<div class="pagination-count">
	<span>Showing <span class="pagination-start">1</span>-<span class="pagination-end">{if $data|@count gt 5}5{else}{$data|@count}{/if}</span> of {$data|@count} products</span>
</div>

<!-- Widget -->
{include file="cart/combo_overlay_widget.tpl"}

<!-- Action Buttons -->
<div class="action-buttons">
	<form method="post"  action="mkretrievedataforcart.php?pagetype=productdetail&lead=recently_viewed" id="form-{$widgetdata.styleid}" class="re-form-block">
	    <input type="hidden" name="_token" value="{$USER_TOKEN}" />
	    <button class="add-to-cart btn primary-btn combo-add-to-cart-button" disabled="true" >ADD TO BAG</button>
		&nbsp; &nbsp; <a class="combo-overlay-cancel" href="javascript:void(0)">CANCEL</a>
	</form>
</div>
<!-- Total and Savings -->
<div class="combo-overlay-total">
    <div>YOU PAY <span class="rupees">Rs. <span class="total">{math equation="a-b" a=$comboTotal b=$comboSavings assign="comboActualTotal"}{$comboActualTotal|round|number_format:0:".":","}</span></span></div>
    <div class="savings-msg" {if $dre_itemPrice>0}style="display:none;"{/if}>YOU SAVE <span class="rupees">Rs. <span class="savings"> 
    {if isset($comboSavings) && $comboSavings>0}
    {$comboSavings|round|number_format:0:".":","}
    {else}
    0
    {/if}
    </span></span></div>
    {if $isConditionMet neq true}
        <div class="message" style="padding:5px 0px;">{if $dre_itemPrice>0}{else}-{/if} Complete the combo to save!</div>
    {/if}	
</div>
</div>
