<form action="{$https_location}/{$paymentphp}" id="debit_card" method="post" target="MyntraPayment"
	{if $totalAmount eq 0}
		style="display:none"
	{elseif $defaultPaymentType eq "debit_card"}
		style="display: block;"
	{else}
		style="display:none"
	{/if}
>
    {if $customeragent}
	{if $giftCardPaymentPage}
		<input type="hidden" name='order_type' value='gifting'/>
    {/if}
	<input type="hidden" name='s_option' value ='{$logisticId}'/>
	<!--delivery preference-->
	<input type="hidden" name="shipment_preferences" value="shipped_together" >
	<!--delivery preference-->
	<input type="hidden" name="is_shipping_cost_set" value="{$s_city}">
	<input type="hidden" name="checksumkey" value="{$checksumkey}">
	<input type="hidden" name="address" value="{$address.id}">
	<input type="hidden" value="debitcard" name="pm" />
	{if $onlineErrorMessage}
        {$onlineErrorMessage}
    {else}
		{if $is_icici_gateway_up}
			{if $creditDebitDownBankListStr}
				<div class="warning-text-border">
		            <p><div class="warning-icon"></div>{$creditDebitDownBankListStr}</p>
				</div>
			{/if}
		    <div class="cc-form">
		        <div class="accepted-cards">
		            <span class="cardType visa">Visa</span>
		            <span class="cardType master">Mastercard</span>
		            <span class="cardType maestro">Maestro</span>
		        </div>
		
				<div class="row">
					<label>Card number <em>Without Spaces</em> *</label>
					<input type="text" class="long" maxlength="19" name="card_number" autocomplete="off" onkeyup="showUserCardType(document.getElementById('debit_card').card_number.value);" onfocus="showUserCardType(document.getElementById('debit_card').card_number.value);" onchange="showUserCardType(document.getElementById('debit_card').card_number.value);">
				</div>
		
				<div class="row">
					<label class="required">Name on debit card *</label>
					<input type="text" title="Name on credit card" value="" name="bill_name" class="long">
				</div>
				
				<div class="field-msg"><span class="info-icon"></span>You do not need to enter Expiry and CVV if they are not mentioned on your Debit Card</div>
				
				<div class="row cc-expiry mk-cf">
		            <div class="col mk-f-left">
					    <label>Expiry Month <span class="ostar">*</span></label>
						<select name="card_month">
							<option value="">Month</option>
							<option value="01"> 1 </option>
							<option value="02"> 2 </option>
							<option value="03"> 3 </option>
							<option value="04"> 4 </option>
							<option value="05"> 5 </option>
							<option value="06"> 6 </option>
							<option value="07"> 7 </option>
							<option value="08"> 8 </option>
							<option value="09"> 9 </option>
							<option value="10"> 10 </option>
							<option value="11"> 11 </option>
							<option value="12"> 12 </option>
						</select>
		            </div>
		            <div class="col mk-f-right">
					    <label>Expiry Year <span class="ostar">*</span></label>
						<select name="card_year">
							<option value="">Year</option>
							<option value="12">2012</option>
							<option value="13">2013</option>
							<option value="14">2014</option>
							<option value="15">2015</option>
							<option value="16">2016</option>
							<option value="17">2017</option>
							<option value="18">2018</option>
							<option value="19">2019</option>
							<option value="20">2020</option>
							<option value="21">2021</option>
							<option value="22">2022</option>
							<option value="23">2023</option>
							<option value="24">2024</option>
							<option value="25">2025</option>
							<option value="26">2026</option>
							<option value="27">2027</option>
							<option value="28">2028</option>
							<option value="29">2029</option>
							<option value="30">2030</option>
							<option value="31">2031</option>
							<option value="32">2032</option>
							<option value="33">2033</option>
							<option value="34">2034</option>
							<option value="35">2035</option>
							<option value="36">2036</option>
							<option value="37">2037</option>
							<option value="38">2038</option>
							<option value="39">2039</option>
							<option value="40">2040</option>
						</select>
					</div>
				</div>
			
				<div class="row">
					<label>CVV Code <span class="ostar">*</span></label>
					<input type="password" id="cvv" cardtypefieldname="card_type" title="Card Verification Value Code"
		                    maxlength="4" size="4" value="" name="cvv_code" autocomplete="off">
		            <a href="#whatiscvv" class="what-is-cvv dc-cvv">What is a cvv code?</a>
		            <div class="myntra-tooltip tt-dc-cvv">
		                <div class="cvv-img"></div>
		            </div>
				</div>
			
		        <div class="msg-fill-all">
		            * Required fields
		        </div>
		    </div>
		{else}
			<span>
	            Please click on 'Proceed to Payment' to proceed with your order.
	            <br/><br/>
	            We will redirect you to our payment gateway partner to complete the payment process.
	        </span>
		{/if}
	{/if}
        {else}
            {$telesalesmsg}
        {/if}
</form>
