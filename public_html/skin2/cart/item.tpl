{foreach key=key item=item from=$product}
				{if $key eq 'productId'}
					{assign var='productId' value=$item}
				{/if}
				{if $key eq 'producTypeId'}
					{assign var='producTypeId' value=$item}
				{/if}
				{if $key eq 'productStyleId'}
					{assign var='productStyleId' value=$item}
				{/if}
				{if $key eq 'productStyleName'}
					{assign var='productStyleName' value=$item}
				{/if}
				{if $key eq 'productPrice'}
					{assign var='productPrice' value=$item}
				{/if}
				{if $key eq 'unitPrice'}
                    {assign var='unitPrice' value=$item}
                {/if}
				{if $key eq 'quantity'}
					{assign var='quantity' value=$item}
				{/if}
				{if $key eq 'actualPriceOfProduct'}
					{assign var='actualPriceOfProduct' value=$item}
				{/if}
				{if $key eq 'productTypeLabel'}
					{assign var='productTypeLabel' value=$item}
				{/if}
				{if $key eq 'totalPrice'}
					{assign var='totalPrice' value=$item}
				{/if}
				{if $key eq 'sizenames'}
					{assign var='sizenames' value=$item}
				{/if}
				{if $key eq 'sizequantities'}
					{assign var='sizevalues' value=$item}
				{/if}
				{if $key eq 'cartImagePath'}
					<!--to show name and number for jerseys added by arun-->
					{include file="affiliatetemplates/reebok_ipl/cricket_jersey_cart_images.tpl" assign='cartImagePath'}
					<!--to show name and number for jerseys added by arun-->
					{* assign var='cartImagePath' value=$item *}
				{/if}
				{if $key eq 'discount'}
					{assign var='discount' value=$item}
				{/if}
				{if $key eq 'productStyleType'}
					{assign var='productStyleType' value=$item}
				{/if}
				{if $key eq 'description'}
					{assign var='description' value=$item}
				{/if}
				{if $key eq 'totalCustomizedAreas'}
					{assign var='totalCustomizedAreas' value=$item}
				{/if}
				{if $key eq 'custominfo'}
                    {assign var='custominfo' value=$item.areas}
                {/if}

			{/foreach}
                    <form id="cartform_{$itemid}" method="post" action="modifycart.php" style="margin-bottom:0px;margin-top:0px">
                                <tr id="product_view_{$itemid}">
                                <td class="tdimg">

                                                             {* image *}
                                       <img src="{$cartImagePath}" alt="">
                                                            {* preview *}
                                       <p>

                                               <span>
                                                 <span class="gallery_{$itemid}">
                                                   {assign var='counter' value=0}
                                                    {foreach from=$custominfo item=area}
                                                            {if $counter eq 0 }
                                                            <a href="{$area.area_image_final}" id="link_{$itemid}">Preview</a>
                                                            {else}
                                                            <a href="{$area.area_image_final}" id="link_{$itemid}"></a>
                                                            {/if}
                                                            {assign var='counter' value=$counter+1}
                                                    {/foreach}
                                               </span>
                                               <script type="text/javascript">
                                                   {literal}${/literal}('.gallery_{$itemid} a').lightBox();
                                               </script>
                                               </span>
                                       </p>


                                </td>
                                <td class="prodesc">

                                <b>Type:</b> {$productTypeLabel} <br />
                                       <b>Style:</b>  {$productStyleName} <br />
                                       {if $product_custom_display_forcart.$productId.text && $product_custom_display_forcart.$productId.num}
                                       <b>Name:</b> {$product_custom_display_forcart.$productId.text}  Number :{$product_custom_display_forcart.$productId.num}
                                       {/if}
                                <td>Rs {$unitPrice|string_format:"%.2f"}/-</td>
                                {if $sizenames && $sizevalues}
                                <td>
		                            <ul>
                                        {assign var=i value=0}
                                        {foreach from=$sizenames item=sizename}
                                            {if $sizevalues[$i] }
											<li>&nbsp;{$sizename}</li>
											{/if}
											{assign var=i value=$i+1}
										{/foreach}
                                    </ul>
                                </td>
                                <td>
                                    		                                    <ul>
                                    {assign var=i value=1}
                                    {foreach from=$sizevalues item=sizevalue}
                                        {if $sizevalue}
                                        <li>{$sizevalue}</li>
                                        {/if}
                                        {assign var=i value=$i+1}
								    {/foreach}
                                    </ul>
                                </td>
                                   {else}
                                        <td>
                                           NA
                                        </td>
                                        <td >
                                           {$quantity}
                                        </td>
                                    {/if}
                                <td>
                                <p>Rs {$totalPrice|string_format:"%.2f"}/-</p>
                                </td>
                                <td>
                                   <span class="modifybtn"><a href="javascript:void(0);" onclick="$('#product_view_{$itemid}').hide(); $('#product_modify_{$itemid}').show();">Modify Qty <img src="{$cdn_base}/skin1/images/small-arrow.png" alt=""style="border:0px"></a></span>
                                   <span class="removebtn"><a  href="{$http_location}/modifycart.php?remove=true&itemid={$itemid}" onclick="return confirmDelete('{$productStyleName|escape:"quotes"}', '{$itemid}', '{$quantity}','{$productTypeLabel}');">Remove <img src="{$cdn_base}/skin1/images/cart-removebtn.png" alt=""style="border:0px;float:none; vertical-align:middle;_position:absolute;"></a></span>
                                </td>
                                </tr>

                                {* modify code *}

                                <tr id="product_modify_{$itemid}" style="display:none;" class="modify">
                                <td  class="tdimg">

                                                             {* image *}
                                       <img src="{$cartImagePath}" alt="">
                                </td>
                                <td class="prodesc">

                                <b>Type:</b> {$productTypeLabel} <br />
                                       <b>Style:</b>  {$productStyleName} <br />
                                       {if $product_custom_display_forcart.$productId.text && $product_custom_display_forcart.$productId.num}
                                       <b>Name:</b> {$product_custom_display_forcart.$productId.text}  Number :{$product_custom_display_forcart.$productId.num}
                                       {/if}
                                <td>Rs {$unitPrice|string_format:"%.2f"}/-</td>
                                {if $sizenames && $sizevalues}
                                <td>
		                            <ul>
                                        {assign var=i value=0}
                                        {foreach from=$sizenames item=sizename}
											<li>&nbsp;{$sizename}</li>
											{assign var=i value=$i+1}
										{/foreach}
                                    </ul>
                                </td>
                                <td>
                                    		                               <ul>
                                                                    {assign var=i value=1}
                                                                    {foreach from=$sizevalues item=sizevalue}
                                                                        <li><input type="text" name="{$itemid}sizequantity[]" id="{$itemid}sizequantity{$i}" value="{$sizevalue}" /></li>
                                                                        {assign var=i value=$i+1}
                                								    {/foreach}
                                                                    </ul>
                                                                    <input type="hidden" name="{$itemid}count" id="{$itemid}count" value="{$i}" />
								                                                   <input id="{$itemid}qty"  name="{$itemid}qty" type="hidden" class="quantity"  value="{$quantity}" maxlength="4" tabindex="{$tabindex}" />
                                </td>
                                   {else}
                                        <td>
                                           NA
                                        </td>
                                        <td >
                                           <input id="{$itemid}qty"  name="{$itemid}qty" type="text" class="quantity"  value="{$quantity}" maxlength="4"  />
                                        </td>
                                    {/if}
                                <td>
                                <p>Rs {$totalPrice|string_format:"%.2f"}/-</p>
                                </td>
                                <td>
                                   <span class="savebtn"><a style="cursor:pointer"  onclick=" updateTotalQuantity({$itemid},this);">Save <img src="{$cdn_base}/skin1/images/save-arrow.jpg" alt=""style="border:0px"></a></span>
                                   <span class="removebtn"><a href="{$http_location}/modifycart.php?remove=true&itemid={$itemid}" onclick="return confirmDelete('{$productStyleName|escape:"quotes"}', '{$itemid}', '{$quantity}','{$productTypeLabel}');">Remove <img src="{$cdn_base}/skin1/images/cart-removebtn.png" alt=""style="border:0px;float:right;_position:absolute;"></a></span>
                                </td>
                                </tr>




                                                                  <input type="hidden" name="cartitem" id="cartitem" value="{$itemid}" />
                                                                  <input type="hidden" name="update" value="update"  />

	   			                                              </form>
                                                      
