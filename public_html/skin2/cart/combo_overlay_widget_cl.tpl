{if $combotype != 'freeitem'}
<script>
    lists_image_srcs = new Object();
</script>
{/if}
    <div class="combo-type-class-{$combotype} cart-widget-box mk-carousel-four clearfix rs-carousel">
    	<ul class="cart-widget-items rs-carousel-runner">
		{foreach name=widgetdata_outerloop item=widgetdata from=$data key=uid}
		{if $widgetdata.product neq '' && $widgetdata.product neq ' '}
			<li id="item_combo_lay_{$uid}" class="cart-widget-item rs-carousel-item" 
				data-uid = "{$uid}" data-combotype="{$combotype}">
				<div class="cart-widget-item-container">
				
					<!-- Selection Box -->
 	            	<div class="cart-widget-checkbox">
	                    <a class="checkbox mk-labelx_check unchecked" >
							<span class="cbx"></span>
						</a>
					</div>

<div class="main-flip">
	<!-- Quick View -->
	{if $quicklookenabled && $combotype != 'freeitem'}
	<span class="combo-quick-look quick-look" data-widget="combo_overlay" data-uid="{$uid}" data-styleid="{$widgetdata.styleid}" href="javascript:void(0)"></span>
	{/if}

	<!-- Product Image -->
	<div class="mk-product-image">
		<img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src="{$widgetdata.search_image}" alt="{$widgetdata.product}"/>
		<div class="mk-product-brand">
			<em>{$widgetdata.global_attr_brand}</em>
		</div>
	</div>
	<!-- Brand Info -->
	<p class="brand-text">{$widgetdata.global_attr_brand|truncate:20}</p>
                                    
	<!-- Pricing Data -->
	<div class="price-message">
		<span class="price-text main">{$rupeesymbol}{$widgetdata.price|number_format:0:".":","}</span>
		<span class="size-text-hdr hide"> / SIZE: <span class="size-text-sub"></span></span>
		<span class="qnty-text-hdr hide"> / QTY: <span class="qnty-text-sub"></span></span>     
	</div>

	<a class="edit-btn hide" href="javascript:void(0)">Edit Details</a>
</div>

<div class="edit-flip hide">
	<!-- Size sdhfg -->
	<div class="size-box hide">
		<p class="size-msg err">&nbsp;</p>
		<div class="size-info-box" skuid="" qty="">&nbsp;</div>
		<div class="size-info mk-custom-drop-down">
			<span style="font-weight:normal; text-align: center; display: block;">SIZE: </span>
			<select class="cow-size-loader" name="{$widgetdata.styleid}_cow_size" >
				<option value="-">-</option>
			</select>
		</div>
	</div>

	<!-- Quantity -->
	<div class="qty-box">
		<p class="qty-msg err">&nbsp;</p>
		<div class="quantity-info mk-custom-drop-down hide">
			<span style="font-weight:normal; text-align: center; display: block;">QTY: </span>
			<select name="{$widgetdata.styleid}_cow_qty" class="quantity filter-select cart-options quantity-box">
			{for $i=1 to 10}
	            <option value="{$i}"{if $i eq $products[$widgetdata.styleid][$widgetskuid].quantity} selected{/if}>{$i}</option>
            {/for}
            </select>
		</div>
	</div>
	<!-- Save and Cancel Buttons -->
	<div class="buttons">
    	<a class="save-btn btn primary-btn">Ok</a>
		<a class="cancel-btn btn normal-btn">Cancel</a>
	</div>

</div>

				</div>
			</li>
		{/if}
		{/foreach}
		</ul>
	</div>

