
<script>
	lists_image_srcs = new Object();
</script>
    <div class="cart-widget-box mk-carousel-four clearfix rs-carousel">
            <ul class="cart-widget-items rs-carousel-runner">
                {assign var=i value=1}
                {foreach name=widgetdata_outerloop item=widgetdata from=$data}
                {if $selectedSKUs[$widgetdata.styleid]}
                    {foreach name=widgetdataloop item=widgetskuid from=$selectedSKUs[$widgetdata.styleid]}
                    {if $widgetdata.product neq '' && $widgetdata.product neq ' '}
                        <li data-id="prod_{$widgetdata.id}" class="cart-widget-item item_{$widgetdata.styleid} rs-carousel-item">
                        <div class="cart-widget-item-container cart-widget-item-box-pre-added">
	                        <div class="cart-widget-item-container cart-combo-widget-image">
	                            
								<!-- Selection Box -->
	                            <div class="cart-widget-checkbox">
                 					<a class="checkbox mk-labelx_check unchecked" data-styleid="{$widgetdata.styleid}" data-skuid="{$widgetskuid}" data-fromcart="1" data-productid="{$products[$widgetdata.styleid][$widgetskuid].productid}" disabled="disabled">
										<span class="cbx"></span>
									</a>							    
								</div>

								<div class="main-flip">	
								
		                            <!-- Product Image -->
		                            <div class="mk-product-image ">
		                                <img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src="{myntraimage key="style_180_240" src=$widgetdata.search_image}" alt="{$widgetdata.product}"/>
		                                <div class="mk-product-brand">
		                                    <em>{$widgetdata.global_attr_brand}</em>
		                                </div>
		                            </div>
		
									<!-- Brand Info --> 
	                            	<p class="brand-text">{$widgetdata.global_attr_brand|truncate:20}</p>
		                            
									<!-- Pricing Data -->
		                            <div class="price-message">
										<span class="price-text main">{$rupeesymbol}{$widgetdata.price|number_format:0:".":","}</span>
										<span class="size-text-hdr"> / SIZE: <span class="size-text-sub"></span></span>
										<span class="qnty-text-hdr"> / QTY: <span class="qnty-text-sub"></span></span>
		                            </div>
									
									<a class="edit-btn hide" style="display:none;" href="javascript:void(0)">Edit Details</a>
								</div>
								
								<div class="edit-flip hide">
		                            <!-- Size sdhfg -->
									<div class="size-box size-selection-row-{$widgetdata.styleid} hide">
										<p class="size-msg err">&nbsp;</p>
		                        	<div class="size-info-box">&nbsp;</div>    
						<div class="size-info mk-custom-drop-down">
		                                	<span style="font-weight:normal; text-align: center; display: block;">SIZE: </span>
		                                	<select class="cow-size-loader" id="cow-size-select-{$widgetdata.styleid}" data-skuid="{$widgetskuid}" 
								name="{$widgetdata.styleid}_cow_size" >
		                                    	<option value="-">-</option>
		                                	</select>
		                            	</div>
									</div>
		                           
		                            <!-- Quantity -->
									<div class="qty-box">
									<p class="qty-msg err">&nbsp;</p>
		                            <div class="quantity-info mk-custom-drop-down quantity-selection-row-{$widgetdata.styleid} hide">
		                                <span style="font-weight:normal; text-align: center; display: block;">QTY: </span>
		                                <select id="{$widgetdata.styleid}_cow_qty"  name="{$widgetdata.styleid}_cow_qty" data-skuid="{$widgetskuid}"
		                                    data-styleid="{$widgetdata.styleid}" class="quantity filter-select cart-options quantity-box">
	                                        {for $i=1 to 10}
	                                        	<option value="{$i}"{if $i eq $products[$widgetdata.styleid][$widgetskuid].quantity} selected{/if}>{$i}</option>
	                                    	{/for} 	
										</select>
		                            </div>
									</div>
	
									<!-- Save and Cancel Buttons -->
									<div class="buttons">
										<a class="save-btn btn primary-btn">Ok</a>
										<a class="cancel-btn btn normal-btn">Cancel</a>
									</div>

								</div>
								
	                        </div>
						</div>		
                    </li>
                    {/if}
                    {/foreach}
                {else}
                    {if $widgetdata.product neq '' && $widgetdata.product neq ' '}
                        <li data-id="prod_{$widgetdata.id}" class="cart-widget-item item_{$widgetdata.styleid} rs-carousel-item">
	                        <div class="cart-widget-item-container cart-combo-widget-image">
	                            <!-- Selection Box -->
	                            <div class="cart-widget-checkbox">
                 					<a class="checkbox mk-labelx_check unchecked" data-styleid="{$widgetdata.styleid}" data-skuid="{$widgetskuid}" data-fromcart="1" data-productid="{$products[$widgetdata.styleid][$widgetskuid].productid}">
										<span class="cbx"></span>
									</a>							    
								</div>
								<div class="main-flip">	
								
		                            <!-- Quick View -->
		                            {if $quicklookenabled}
		                            <span class="combo-quick-look quick-look" data-widget="combo_overlay" data-styleid="{$widgetdata.styleid}" href="javascript:void(0)"></span>
		                            {/if}
		
		                            <!-- Product Image -->
		                            <div class="mk-product-image ">
		                                <img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" {if $smarty.foreach.widgetdata_outerloop.index gt 5} src="http://myntra.myntassets.com/skin2/images/spacer.gif" data-lazy-src="{myntraimage key="style_180_240" src=$widgetdata.search_image}" class="lazy"{else}src="{myntraimage key="style_180_240" src=$widgetdata.search_image}" {/if}alt="{$widgetdata.product}"/>
		                                <div class="mk-product-brand">
		                                    <em>{$widgetdata.global_attr_brand}</em>
		                                </div>
		                            </div>
		
									<!-- Brand Info --> 
	                            	<p class="brand-text">{$widgetdata.global_attr_brand|truncate:20}</p>
	                            
									<!-- Pricing Data -->
		                            <div class="price-message">
										<span class="price-text main">{$rupeesymbol}{$widgetdata.price|number_format:0:".":","}</span>
										<span class="size-text-hdr hide"> / SIZE: <span class="size-text-sub"></span></span>
										<span class="qnty-text-hdr hide"> / QTY: <span class="qnty-text-sub"></span></span>
		                            </div>
									
									<a class="edit-btn hide" href="javascript:void(0)">Edit Details</a>
								</div>								
					<div class="flip-wrapper">
							<!--<div class="flip-dummy">&nbsp;</div>		-->
								<div class="edit-flip hide">
		                            <!-- Size sdhfg -->
									<div class="size-box size-selection-row-{$widgetdata.styleid} hide">
										<p class="size-msg err">&nbsp;</p>
					  	<div class="size-info mk-custom-drop-down">
		                                	<div style="font-weight:normal; text-align: center; display: block;">SELECT SIZE</div>
							<div class="size-info-box hide">Freesize</div>
		                                	<select class="cow-size-loader" id="cow-size-select-{$widgetdata.styleid}" data-skuid="{$widgetskuid}" 
								 name="{$widgetdata.styleid}_cow_size" >
		                                    	<option value="-">-</option>
		                                	</select>
		                            	</div>
									</div>
		                            <!-- Quantity -->
									<div class="size-box">
									<p class="qty-msg err">&nbsp;</p>
		                            <div class="quantity-info mk-custom-drop-down quantity-selection-row-{$widgetdata.styleid} hide">
		                              	<div style="font-weight:normal; text-align: center; display: block;">SELECT QUANTITY</div>
		                                <select id="{$widgetdata.styleid}_cow_qty"  name="{$widgetdata.styleid}_cow_qty" data-skuid="{$widgetskuid}"
		                                    data-styleid="{$widgetdata.styleid}" class="quantity filter-select cart-options quantity-box">
	                                        {for $i=1 to 10}
	                                        	<option value="{$i}"{if $i eq $products[$widgetdata.styleid][$widgetskuid].quantity} selected{/if}>{$i}</option>
	                                    	{/for} 	
										</select>
		                            </div>
		                            </div>
		                            <!-- Save and Cancel Buttons -->
									<div class="buttons">
										<a class="save-btn btn primary-btn" disabled='disabled'>Ok</a>
										<a class="cancel-btn btn normal-btn">Cancel</a>
									</div>
								</div>	
						</div>
							</div>	
	                    </li>                                                
					{/if}                    
                {/if}
                {assign var=i value=$i+1}
                {/foreach}
            </ul>
</div>



