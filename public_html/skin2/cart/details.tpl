									<h1>YOUR ORDER DETAILS</h1>
								<div class="clearall" style="font-size:0px;height:8px;">&nbsp;</div>
									<table cellpadding="0" cellspacing="0" width="470" border="0" class="cart-final-items" style="width:580px">
										<tr class="tableheader">
											<th class="leftb">PRODUCT DESCRIPTION</th>
											<th>UNIT PRICE</th>
											<th>SIZE</th>
											<th>QTY</th>
											<th>[VAT Inclusive]</th>
											<th>TOTAL</th>											
										</tr>
                                        {foreach name=outer item=product key=itemid from=$productsInCart}
                                            {foreach key=key item=item from=$product}
                                                    {if $key eq 'productId'}
                                                        {assign var='productId' value=$item}
                                                    {/if}
                                                    {if $key eq 'producTypeId'}
                                                        {assign var='producTypeId' value=$item}
                                                    {/if}
                                                    {if $key eq 'productStyleId'}
                                                        {assign var='productStyleId' value=$item}
                                                    {/if}
                                                    {if $key eq 'productStyleName'}
                                                        {assign var='productStyleName' value=$item}
                                                    {/if}
                                                    {if $key eq 'productPrice'}
                                                        {assign var='productPrice' value=$item}
                                                    {/if}
                                                    {if $key eq 'totalProductPrice'}
                                                        {assign var='totalProductPrice' value=$item}
                                                    {/if}
                                                    {if $key eq 'unitPrice'}
                                                        {assign var='unitPrice' value=$item}
                                                    {/if}
                                                    {if $key eq 'quantity'}
                                                        {assign var='quantity' value=$item}
                                                    {/if}
                                                    {if $key eq 'actualPriceOfProduct'}
                                                        {assign var='actualPriceOfProduct' value=$item}
                                                    {/if}
                                                    {if $key eq 'productTypeLabel'}
                                                        {assign var='productTypeLabel' value=$item}
                                                    {/if}
                                                    {if $key eq 'totalPrice'}
                                                        {assign var='totalPrice' value=$item}
                                                    {/if}
                                                    {if $key eq 'sizenames'}
                                                        {assign var='sizenames' value=$item}
                                                    {/if}
                                                    {if $key eq 'sizequantities'}
                                                        {assign var='sizevalues' value=$item}
                                                    {/if}
                                                    {if $key eq 'cartImagePath'}
                                                        <!--to show name and number for jerseys added by arun-->
                                                        {include file="affiliatetemplates/reebok_ipl/cricket_jersey_cart_images.tpl" assign='cartImagePath'}
                                                        <!--to show name and number for jerseys added by arun-->
                                                        {* assign var='cartImagePath' value=$item *}
                                                    {/if}
                                                    {if $key eq 'discount'}
                                                        {assign var='discount' value=$item}
                                                    {/if}
                                                    {if $key eq 'productStyleType'}
                                                        {assign var='productStyleType' value=$item}
                                                    {/if}
                                                    {if $key eq 'description'}
                                                        {assign var='description' value=$item}
                                                    {/if}
                                                    {if $key eq 'totalCustomizedAreas'}
                                                        {assign var='totalCustomizedAreas' value=$item}
                                                    {/if}
                                                    {if $key eq 'unitVat'}
                                                        {assign var='unitVat' value=$item}
                                                    {/if}
                                                    {if $key eq 'flattenSizeOptions'}
                                                    	{assign var='flattenSizeOptions' value=$item}
                                                    {/if}
                                                    {if $key eq 'sizenamesunified'}
                                                    	{assign var='sizenamesunified' value=$item}
                                                    {/if}
													 {if $key eq 'cashdiscount'}
                                                    	{assign var='cashdiscount' value=$item}
                                                    {/if}
													 {if $key eq 'cashCouponCode'}
                                                    	{assign var='cashCouponCode' value=$item}
                                                    {/if}
                                                {/foreach}
                                                <tr>
                                                    <td class="prodec" width="185">Type: {$productTypeLabel} <br>Style: {$productStyleName}</td>
                                                    <td class="uprice">Rs {$productPrice|string_format:"%.2f"}</td>
                                                    <td class="cartsize">
                                                    {if $sizenames}
												                                                     <ul>
												                                                     {if $flattenSizeOptions}
                                                                                            {assign var=i value=0}
                                                                                            {foreach from=$sizenamesunified item=sizename}
                                                                                                {if $sizevalues[$i] }
                                                    											<li>&nbsp;{$sizename}</li>
                                                    											{/if}
                                                    											{assign var=i value=$i+1}
                                                    										{/foreach}
                                                    										{else}
                                                    										{assign var=i value=0}
                                                                                            {foreach from=$sizenames item=sizename}
                                                                                                {if $sizevalues[$i] }
                                                    											<li>&nbsp;{$sizename}</li>
                                                    											{/if}
                                                    											{assign var=i value=$i+1}
                                                    										{/foreach}
                                                    										{/if}
                                                    										</ul>
                                                       {else}
                                                       <ul>
                                                       <li>
                                                          NA
                                                       </li>
                                                       </ul>
                                                       {/if}

                                                    </td>
                                                    <td class="cartqty">
                                                     <ul>
                                                      {if $sizevalues}
                                    {assign var=i value=1}
                                    {foreach from=$sizevalues item=sizevalue}
                                        {if $sizevalue}
                                        <li>{$sizevalue}</li>
                                        {/if}
                                        {assign var=i value=$i+1}
								    {/foreach}
                                    </ul>  {else}
                                                       <ul>
                                                       <li>
                                                          {$quantity}
                                                       </li>
                                                       </ul>
                                                       {/if}

                                                    </td>

                                                    <td class="vat">[Rs {math equation="x * y" x=$unitVat y=$quantity format="%.2f" }]</td>
													<td class="carttotal">Rs <span>{$totalProductPrice|string_format:"%.2f"}</span></td>
                                                </tr>

                                        {/foreach}



										<tr> 
										<td> &nbsp;</td> 
										<td colspan="5">
										<table cellspacing="0" cellpadding="0" border="0" class="price_details" width="100%">
										<tr>
											<td class="pricelist">Amount:</td>
											<td class="pricelisttotal">Rs<span>{$amount|string_format:"%.2f"}</span></td>
										</tr>
										{if $specialOfferDiscount && $specialOfferDiscount gt 0}
										<tr>
											<td class="pricelist">Special offer discount:</td>
											<td class="pricelisttotal">Rs<span>{$specialOfferDiscount|string_format:"%.2f"}</span></td>
										</tr>
										{/if}
										{if $coupon_discount && $coupon_discount gt 0}
										<tr>
											<td class="pricelist">Discount:</td>
											<td class="pricelisttotal">Rs<span>{$coupon_discount|string_format:"%.2f"}</span></td>
										</tr>
										{/if}
										{if $cashRedeemed && $cashRedeemed gt 0}
										<tr>
											<td class="pricelist">Mynt Credits:</td>
											<td class="pricelisttotal">Rs<span>{$cashRedeemed|round|string_format:"%.2f"}</span></td>
										</tr>
										{/if}
										{if $pg_discount && $pg_discount gt 0}
										<tr>
											<td class="pricelist">Payments Discount:</td>
											<td class="pricelisttotal">Rs<span>{$pg_discount|round|string_format:"%.2f"}</span></td>
										</tr>
										{/if}
										<tr>
											<td class="pricelist">Shipping cost:</td>
											<td class="pricelisttotal">Rs<span>{$shippingRate|string_format:"%.2f"}</span></td>
										</tr>
										{if $giftamount && $giftamount gt 0}
										<tr>
											<td class="pricelist">Gift wrapping cost:</td>
											<td class="pricelisttotal">Rs<span id="gift_charges">{$giftamount|string_format:"%.2f"}</span></td>
										</tr>
										{/if}
										{if $emi_charge && $emi_charge gt 0}
										<tr>
											<td class="pricelist">EMI Charges:</td>
											<td class="pricelisttotal">Rs<span id="emi_charges">{$emi_charge|string_format:"%.2f"}</span></td>
										</tr>
										{/if}
										{if $codCharge && $codCharge gt 0}
										<tr>
											<td class="pricelist">COD Charge:</td>
											<td class="pricelisttotal">Rs<span>{$codCharge|round|string_format:"%.2f"}</span></td>
										</tr>
										{/if}
										</table> </td> </tr>

									</table>

									<div class="total-block" style="_height:30px;">
										<div class="cart-total-text"><p>TOTAL</p></div>
										<div class="cart-total-text1"><p>Final Amount to be paid</p></div>
										<div class="cart-total-price"><p>Rs <span id="total_charges">{$totalAmount|string_format:"%.2f"}</span></p></div>
									</div>
