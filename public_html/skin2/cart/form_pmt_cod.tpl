<form action="{$https_location}/{$paymentphp}" id="cod" method="post"
	{if $totalAmount eq 0}
		style="display:none"
	{elseif $defaultPaymentType eq "cod"}
		style="display: block;"
	{else}
		style="display:none"
	{/if}
>
	{if $giftCardPaymentPage}
    	<input type="hidden" name='order_type' value='gifting'/>
   	{/if}
	<input type="hidden" name='s_option' value ='{$logisticId}'/>
	<!--delivery preference-->
	<input type="hidden" name="shipment_preferences" value="shipped_together" >
	<!--delivery preference-->
	<input type="hidden" name="is_shipping_cost_set" value="{$s_city}">
	<input type="hidden" name="checksumkey" value="{$checksumkey}">
	<input type="hidden" name="address" value="{$address.id}">
	<input type="hidden" value="cod" name="pm" />
	<span id="codMessage">
		{if $codErrorMessage}
			{$codErrorMessage}
		{else}
			<div class="cod-page-notifications error hide"></div>
			<div id="codCaptchaBlock" class="span-12 left captcha-block">
				<div class="notification-success"></div>
				<div class="notification-error"></div>

				<div class="clearfix captcha-details">
					<input type="hidden" name="mobile" value="{$address.mobile}" id="cod-mobile"/>
					<div class="p5 corners black-bg4 span-7 left">
						<img src="{$https_location}/captcha/captcha.php?id=codVerificationPage" id="captcha" />
					</div>
					<a href="javascript:void(0)" onclick="document.getElementById('captcha').src='{$https_location}/captcha/captcha.php?id=codVerificationPage&rand='+Math.random();document.getElementById('captcha-form').focus();_gaq.push(['_trackEvent', 'cod_verification', 'captcha_Refresh']);"
						id="change-image" class="no-decoration-link left">Change text</a>
				</div>
				<div class="clearfix mt10 captcha-entry">
					<label>Type text as in image above</label>
					<input type="text" name="userinputcaptcha" id="captcha-form" />
					<div id="cod-captcha-loading" class="captcha-loading" style="display:none;"></div>
				</div>
				<div id="cod-captcha-message" class="captcha-message hide"></div>
			</div>
			<div>Cash on Delivery option allows you to pay cash against the delivery at your shipping address. <br/>
			{if $cod_charge > 0}
				You are being charged an additional Rs. {$cod_charge} as Cash on Delivery convenience charge. <br />
				You could choose to pay using <a href="{$https_location}/mkpaymentoptions.php#pay_by_dc" onClick="$('#pay_by_dc').click();_gaq.push(['_trackEvent', 'payment_page', 'cod_avoid_charge','debit_card']);return false;">debit card</a>, <a href="{$https_location}/mkpaymentoptions.php#pay_by_cc" onClick="$('#pay_by_cc').click();_gaq.push(['_trackEvent', 'payment_page', 'cod_avoid_charge','credit_card']);return false;">credit card</a> or <a href="{$https_location}/mkpaymentoptions.php#pay_by_nb" onClick="$('#pay_by_nb').click();_gaq.push(['_trackEvent', 'payment_page', 'cod_avoid_charge','netbanking']);return false;">netbanking</a> to avoid this charge.
			{/if}
			<br/>
			</div>
			{if $cashback_gateway_status eq 'on' && $cashback_tobe_given && $cashbackearnedcreditsEnabled eq '1'}
	            <div>
	                <p>
	                    Please note that all purchases made through the Cash on Delivery option will receive their Cashback after <b>30 days</b>.
	                </p>
	
	                <p>
	                    Online purchases will receive their Cashback within an <b>hour</b> after the order has been placed.
	                </p>
	            </div>
            {/if}
			<a href="javascript:void(0)" class="next-button btn primary-btn verify-captcha" id="cod-verify-captcha" onClick="verifyCaptcha()">Confirm order</a>
        {/if}
	</span>
</form>
