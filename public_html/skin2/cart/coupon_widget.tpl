	{if $cashback_gateway_status eq 'on'}
	    <div class="coupons-widget mk-f-left" id="cart_cash_coupon_options">
    		<h2>{if $myntCashdivid eq 'success'}<span class="tick-large-icon"></span>{elseif $myntCashdivid eq 'error'}<span class="wrong-large-icon"></span>{/if}CASHBACK</h2>
			{if $myntCashdivid eq 'error'}
				{if $myntCashUsageMessage}
					<div class="err-div">
						<span class="{$divid} mt10 err err-msg" id="discount_coupon_message">
							{$myntCashUsageMessage}
						</span>
						<a class="close">Dismiss</a>
					</div>
				{/if}
			{/if}
    		{assign var=total_amout_to_be_paid value=$totalAmount+$giftamount+$myntCashUsage}
			{if $myntCashUsageMessage && $myntCashdivid eq 'success'}
				<div class="cashback-message" id="discount_cashcoupon_message">
						{$myntCashUsageMessage}
				</div>
			{/if}
			<form action="{if $expressCheckout}expressbuy.php{else}mkpaymentoptions.php{/if}{if $addressID&&$addressID>0 }?address={$addressID}{/if}" method="post" id="myntCashForm" name="myntCashForm" class="{if $myntCashUsageMessage && $myntCashdivid eq 'success' } hide {/if}" onsubmit="return redeemMyntCash();">
		    	<div class="coupon-block">
		        	<div class="cashback-message">
						{if $myntCashDetails.balance eq 0} You do not have any cashback balance
						{else} You can redeem <span class="rupees discount">Rs. {if $myntCashDetails.balance lte $totalAmount}{$myntCashDetails.balance|number_format:2}{else}{$totalAmount|number_format:2}{/if}</span> 
							{if $myntCashDetails.balance gt $totalAmount}of your <span class="rupees discount"> Rs. {$myntCashDetails.balance|number_format:2} </span> cashback{/if}
						{/if}
	                </div>
	        		<input type="hidden" value="{$myntCashDetails.balance}" id="userCashAmount" class="cb-input" name="userAmount">
	        		<input type="hidden" value="{$cashcoupons.coupon}" id="cashcoupon" class="cb-input" name="cashcoupon">
	        		 <input type="hidden" value="use" id="useMyntCash" class="cb-input" name="useMyntCash">
	        		<input type="hidden" value="{$appliedCouponCode}" id="c-code" class="cb-input" name="c-code">
			     </div>
				<a id="redeemMyntCashButton" class="cashback-btn btn normal-btn small-btn" {if $myntCashDetails.balance gt 0}{else}disabled="disabled"{/if} onclick="javascript:redeemMyntCash();">Redeem</a>
     		</form>
			{if $myntCashdivid eq 'success'}
				<a onclick="javascript:removeMyntCash();" class="cashback-btn btn normal-btn small-btn">Remove</a>
			{/if}
		</div>
	{/if}



    <div class="coupons-widget mk-f-right" id="cart_coupon_options">
    
    {if $condApplyOnAllCoupon eq 1}
    	{assign var=nonDiscountedItemCount value="1"}
    	{assign var=couponApplicableItemCount value="1"}    
    {/if}
    
    {if ($enableSpecialCoupon eq 1 || $noStyleExclusionForUserCoupon eq 1 || $noStyleExclusionForSomeGroups eq 1)}
    	{assign var=couponApplicableItemCount value="1"}    
    {/if}
	
    <h2>
	    {if $divid eq 'success'}
    		<span class="tick-large-icon"></span>
    	{elseif (($styleExclusionFG eq 1 && $couponApplicableItemCount eq 0) || 
    				($condNoDiscountCouponFG eq 1 && $nonDiscountedItemCount eq 0)) 
    			&&  $myntraLoginSuffix eq 0} 
    		<span class="warning-large-icon"></span>
    	{elseif $divid eq 'error'}<span class="wrong-large-icon"></span>
    	{/if}
    	COUPONS
    </h2>

	{if $expressCheckout}
		{assign var=action value="expressbuy.php"}
	{else}
		{assign var=action value="mkpaymentoptions.php"}
	{/if}
	
	{if ($styleExclusionFG eq 1 && $couponApplicableItemCount eq 0) ||
			($condNoDiscountCouponFG eq 1 && $nonDiscountedItemCount eq 0)}
		{assign var=couponMsgSwitch value="0"}	
	{else }
		{assign var=couponMsgSwitch value="1"}
	{/if}
	
	{if $myntraLoginSuffix eq 1}
		{assign var=couponMsgSwitch value="1"}
	{/if}
	
	
		{if $couponMessage && $divid eq 'success' && $couponMsgSwitch eq 1}
			<div class="coupon-msg" id="discount_coupon_message">
					{$couponMessage}
				<div><a class="coupon-info-link" href="#">Know More</a></div>
			</div>
		{elseif $styleExclusionFG eq 1 && $couponApplicableItemCount eq 0 &&  $myntraLoginSuffix eq 0}
			<div class="coupon-msg " id="discount_coupon_message" >
	                			sorry, coupons can be applied<span class="red"> only</span> on <span class="red">coupon-applicable </span >items.	                			
			</div>			
		{elseif $condNoDiscountCouponFG eq 1 && $nonDiscountedItemCount eq 0 && $myntraLoginSuffix eq 0}
			<div class="coupon-msg " id="discount_coupon_message" >
	                			sorry, coupons can be applied<span class="red"> only</span> on <span class="red">non-discounted </span >items.
	                			
	                			<div class="coupon-all-disc-leaf">
	                					ALL ITEMS IN YOUR BAG ARE DISCOUNTED
	                					<div><a class="coupon-info-link" href="#">Know More</a></div>
	                			</div>	            				
			</div>
					
		{/if}
		{*<form action="{$action} {if $addressID&&$addressID>0 }?address={$addressID}{/if}" method="post" id="couponform" name="couponform" class="{if $couponMessage && $divid eq 'success' } hide {/if}  {if (($styleExclusionFG eq 1 && $couponApplicableItemCount eq 0) || ($condNoDiscountCouponFG eq 1 && $nonDiscountedItemCount eq 0)) &&  $myntraLoginSuffix eq 0} hide {/if}">*}
		<form action="{$action}{if $addressID&&$addressID>0 }?address={$addressID}{/if}" 
			  method="post" 
			  id="couponform" 
			  name="couponform" 
			  class="{if $couponMessage && $divid eq 'success' } hide {/if}  {if (($styleExclusionFG eq 1 && $couponApplicableItemCount eq 0) || ($condNoDiscountCouponFG eq 1 && $nonDiscountedItemCount eq 0)) &&  $myntraLoginSuffix eq 0} hide {/if}">
	        <input type="hidden" name="removecouponcode" id="removecouponcode" class="coupon-code">
	        <input type="hidden" value="" id="useMyntCash" class="cb-input" name="useMyntCash">
	        <input type="hidden" value="{$appliedCashbackCode}" id="code" class="cb-input" name="code">
	        <input type="hidden" value="{$myntCashDetails.balance}" id="userAmount" class="cb-input" name="userAmount">
	        {*Other discount coupons*}
	        <div class="coupon-redeem">
			    {if $divid eq 'error' && $couponMsgSwitch eq 1}
        			{if $couponMessage}
	        			<div class="err-div">
	            			<div class="coupon-msg error-msg" id="discount_coupon_message">
	                			{$couponMessage}
	            			</div>
	            			<a class="coupon-info-link" href="#">Know More</a>
	            			<a class="close">Dismiss</a>
            			</div>
        			{/if}
        		     		
    			{/if}
	            <div class="code-enter">
	                <input type="hidden" name="couponcode" id="couponcode" class="coupon-code">
	                <span class="placeholder">Enter a Coupon Code</span>
	                <input type="text" name="othercouponcode" id="othercouponcode" autocomplete="off" class="coupon-code">
	            </div>
	        {*Other discount coupons@End*}
	
	        {if $rest_coupons}
	                {if $showVerifyMobileMsg && $mobile_status neq 'V'}
	                    {assign var=mobileverificationrequired value=1}
	                    <div id="discount_coupon_message" class="message vmessage mt10" >
	                        <a href="javascript:void(0)" id="showSMSVerification" onclick="_gaq.push(['_trackEvent', 'coupon_widget', 'verify_now', 'click']);">Verify your mobile to use your coupons</a>
	                    </div>
	                {/if}
	                <div class="coupon-block">
	                    {if $mobileverificationrequired neq 1}
	                    	<div class="or">OR</div>
		                    <div class="your-coupons">
		                        <a class="link-btn">Choose from my coupons [{$rest_coupons|@count}]</a>
	                            <div class="select-coupons mk-hide">
	                            	<span class="close"></span>
	                            	<h6>Select a Coupon</h6>
		                            <ul>
		                                {section name=prod_num loop=$rest_coupons}
			                                {if $rest_coupons[prod_num].coupon neq ""}
			                                    <li class="mk-cf tt-ctx {if $rest_coupons[prod_num].disabled}disabled{/if}">          
			                                        <div class="select_this_coupon mk-f-left">
			                                        	<a href="#" onclick="redeemCoupon2('{$rest_coupons[prod_num].coupon}');"></a>
			                                        </div>
			                                        <div class="inline-block mk-f-left">
				                                        <div class="coupontext {if $rest_coupons[prod_num].disabled}gray{/if}"><span class="rupees {if !$rest_coupons[prod_num].disabled}red{/if}">{$rest_coupons[prod_num].couponTypeDescription}</span>&nbsp;&nbsp;[Exp: {$rest_coupons[prod_num].expire|date_format:"%d %b, %Y"}]</div>
				                                        <div class="coupon-val gray">{if $rest_coupons[prod_num].minimum && $rest_coupons[prod_num].minimum neq 0 && $rest_coupons[prod_num].minimum neq '0'}Minimum Purchase <span class="rupees {if $rest_coupons[prod_num].disabled}gray{else}d-gray{/if}">{$rupeesymbol} {$rest_coupons[prod_num].minimum|number_format:0:".":","}</span>{/if}</div>
			                                        </div>
			                                        
			                                     {if $styleExclusionFG eq 1 }			                                     
			                                     	{math equation="x-y" x=$rest_coupons[prod_num].minimum y=$couponApplicableItemAmount assign="couponRestAmount"}
			                                     	{if $couponRestAmount > 0}
				                                     	<div class="gray myntra-tooltip tt-tip" >
				                                     		You need to have <span class="red">coupon-applicable</span> items worth <span class="rupeed red">{$rupeesymbol} {$rest_coupons[prod_num].minimum|number_format:0:".":","}</span> or more in your bag to apply this coupon
				                                        </div>
			                                        {/if}
			                                     {elseif $condNoDiscountCouponFG eq 1 }
			                                     
			                                     	{if $condApplyOnAllCoupon eq 1 && $rest_coupons[prod_num].isApplyOnAllCoupon eq 1}
			                                     	
			                                     	{else}
			                                     					                                     
				                                     	{math equation="x-y" x=$rest_coupons[prod_num].minimum y=$nonDiscountedItemAmount assign="couponRestAmount"}
				                                     	{if $couponRestAmount > 0}
				                                     	<div class="gray myntra-tooltip tt-tip" >
				                                     		You need to have <span class="red">non-discounted</span> items worth <span class="rupeed red">{$rupeesymbol} {$rest_coupons[prod_num].minimum|number_format:0:".":","}</span> or more in your bag to apply this coupon
				                                        </div>
				                                        {/if}
				                                     {/if}				                                     
			                                     {/if}  
			                                    </li>
			                                {/if}
		                                {/section}
		                            </ul>
	                            </div>
		                    </div>
						{/if}
	                </div>
	        {/if}
			</div>
			<a onclick="javascript:redeemCoupon();" class="coupon-apply-btn cashback-btn btn normal-btn small-btn" disabled="disabled">Apply</a>	
	    </form>
		<div id="smsVerification" class="mk-hide lightbox verifybox small-lightbox">
			<div class="mod">
				<div class="hd"><h2 class="title">Mobile Verification</h2></div>
				<div class="bd">
					<form action="{if $expressCheckout}expressbuy.php{else}mkpaymentoptions.php{/if}{if $addressID&&$addressID>0 }?address={$addressID}{/if}" method="post" id="change-profile" name="change-profile">
			            <input type="hidden" name="mode" value="update-profile-data" />
			            <input type="hidden" name="login" id="login" value="{$userProfileData.login}" />
						<input type="hidden" id="_token" name="_token" value="{$USER_TOKEN}" />
			            <div class="v-code-note">
			            	<div id="v-code-note-message" {if $verified_for_another eq 1 || $mobile_status eq 'E'} class="error" {/if} >
			            		{if $verified_for_another eq 1}
			            			Your mobile number is unverified because it has been registered against another user. Please enter a valid mobile number to use your coupons.
							    {elseif $userProfileData.mobile eq ''}
				                    Please provide a contact
				                    number on which we can reach you for any issues related to your
				                    transactions. This number is also required to be verified for
				                    usage of any use-specific coupons that you may have been issued
				                    through Myntra.
			                    {elseif $userProfileData.mobile neq "" && $mobile_status neq "V" && $mobile_status neq "E" && $verified_for_another eq 0}
				                    Please verify your
				                    specified contact number to be able to use any user-specific
				                    coupons that you may have been issued through Myntra. We may use
				                    this number to contact you for issues related to any of your
				                    transactions on Myntra.
			                	{elseif $mobile_status eq 'E'}
			                    	You have exceeded the maximum no. of attempts to verify this mobile number. If you have still not received the verification code or are facing any other difficulties, please provide an alternate number and retry. Or, try again after 6 hours.
			                    {/if}
								{if $mrp_skipMobileVerification neq 1 && $mobile_status neq 'V'}
			                    <p>
			                        In case you do not receive your verification code from Myntra, please call us at our Customer Support number at <strong>080-43541999</strong> anytime and get your number verified instantly.
			                    </p>
			                    {/if}
			                </div>
			            </div>
			            <div class="v-code">
			                <div class="row"><label>Name :</label> <span>{$userProfileData.firstname} {$userProfileData.lastname}</span></div>
			                <div class="row"><label>Email :</label> <span class="email">{$userProfileData.login}</span></div>
			                <div class="row"><label>Mobile  :</label> <span><input type="text" id="mobile" name="mobile" value="{$userProfileData.mobile}" class="profile-mobile" maxlength="10" > <div class="verify-loading mk-hide" id="mobile-verify-loading" style="*position:relative;*top:-25px"></div></span></div>
			                {if $mrp_skipMobileVerification neq 1}<input type="button" value="Verify Mobile Now" id="verify-mobile" class="verify-mobile-cart-page btn small-btn primary-btn">
			                {elseif $showVerifyMobileMsg && $mobile_status neq 'V'}<input type="button" value="Verify Mobile Now" id="verify-mobile" class="verify-mobile-cart-page btn small-btn primary-btn">
			                {else}{if $verified_for_another eq 1}<input type="button" value="Edit Mobile" id="verify-mobile" class="verify-mobile-cart-page btn small-btn normal-btn">{/if}{/if}
			                <div id="mobile-error" class="captcha-message error hide"></div>
			            </div>
					</form>
					<div class="captcha-block hide" id="mobile-captcha-block">
						<fieldset>
						    <legend>Type the characters you see in the picture</legend>
				            <form id="mobile-captchaform" method="post" action="">
				                <div class="mk-cf captcha-details">
				                    <div class="p5 corners black-bg4 span-7">
				                        <img src="{$secure_cdn_base}/skin1/images/spacer.gif" id="mobile-captcha" />
				                    </div>
				                    <a href="javascript:void(0)" onclick="document.getElementById('mobile-captcha').src='{$http_location}/captcha/captcha.php?id=paymentpage&rand='+Math.random();document.getElementById('mobile-captcha-form').focus();" id="change-image" class="no-decoration-link">Change text</a>
				                </div>
				
				                <div class="mk-cf mt10 captcha-entry">
				                    <input type="text" name="mobile-userinputcaptcha" id="mobile-captcha-form" />
				                    <input type="hidden" name="mobile-login" class="btn-captcha" id="mobile-login" value="{$userProfileData.login}" >
				                    <a href="javascript:void(0)" class="next-button btn primary-btn small-btn verify-captcha" id="mobile-verify-captcha" style="*position:relative;*top:-15px" onClick="verifyMobileCaptcha()">Confirm</a>
				                    <div id="mobile-captcha-loading" class="captcha-loading mk-hide"></div>
				                </div>
				                <div class="captcha-message hide" id="mobile-captcha-message"></div>
				            </form>
				        </fieldset>
			        </div>
		
				    <form action="{if $expressCheckout}expressbuy.php{else}mkpaymentoptions.php{/if}{if $addressID&&$addressID>0 }?address={$addressID}{/if}" method="post" id="verify-mobile-code" name="verify-mobile-code" class="mt20 mb10 hide">
				        <input type="hidden" name="mode" value="verify-mobile-code">
				        <div class="four-dig-code">
				        <p id="mobile-message"></p>
				            <div class="row">
				            	<label style="*position:relative;*top:-4px">Verification Code : </label>
				            	<span style="*display:inline-block"><input id="mobile-code" name="mobile-code" size="4" style="*position:relative;*top:4px"></span>
			            	</div>
				            <div class="verify-loading" id="code-verify-loading" style="display:none; margin-right: 100px;"></div>
				            <input type="button" value="Submit" id="submit-code" class="submit-code btn primary-btn small-btn">
				        </div>
				        <div class="four-dig-code-note">
				           <p id="mobile-verification-error">If you have not received your verification code, please wait for atleast 2 minutes before pressing submit button</p>
				        </div>
				    </form>
				</div>
			</div>
		</div>
		
		{if $divid eq 'success'}
			<a onclick="javascript:removeCoupon();" class="cashback-btn btn normal-btn small-btn">Remove</a>
		{/if}			
	</div>

