<form action="{$https_location}/{$paymentphp}" id="cod" method="post"
	{if $totalAmount eq 0} 
		style="display:none" 
	{elseif $defaultPaymentType eq "cod"} 
		style="display: block;"
	{else}
		style="display:none"	 
	{/if}
>
	{if $giftCardPaymentPage}
		<input type="hidden" name='order_type' value='gifting'/>
	{/if}
	<input type="hidden" name='s_option' value ='{$logisticId}'/>
	<!--delivery preference-->
	<input type="hidden" name="shipment_preferences" value="shipped_together" >
	<!--delivery preference-->								
	<input type="hidden" name="is_shipping_cost_set" value="{$s_city}">
	<input type="hidden" name="checksumkey" value="{$checksumkey}">
	<input type="hidden" name="address" value="{$address.id}">					
	<input type="hidden" value="cod" name="pm" />
	<div id="codMessage">
		{if $codErrorMessage}
			{$codErrorMessage}
		{else}
			<div>Cash on Delivery option allows you to pay cash against the delivery at your shipping address. </div>
			<div class="mobile-verify">
	            <div class="mynt-success hide">
	                <span class="success-icon">&nbsp;</span> <strong>MOBILE VALIDATED</strong><br>
	                <span class="mynt-msg">Your number is validated. You can now place cash on delivery orders on Myntra.</span>
	            </div>
	            <div class="warning">
	                <span class="warning-icon">&nbsp;</span> <strong>MOBILE VALIDATION REQUIRED</strong><br>
	                <span class="mynt-msg">You need to validate your mobile number in order to place a cash on delivery order.</span>
	            </div>
	            
	            <div class="num-error hide">
	                <span class="error-icon">&nbsp;</span> <strong>MOBILE VALIDATION ERROR</strong><br>
	                <span class="mynt-msg">
	                    Your number has already been validated against another My Myntra account.<br>
                        Please edit your number:
	                </span>
	            </div>

                <div class="mobile-number">
                    <div class="your-number">Mobile: <strong><span id='userMobile'>{$userMobile}</span></strong> <button class="link-btn num-edit-btn" id="edit-btn-mobile-verify" type="button">Edit</button></div>
                    <div class="edit-your-number hide">Mobile: <strong><input type="text" class="mobile-number" value="{$userMobile}" maxlength="10"></strong> <button class="btn primary-btn small-btn save-btn" type="button">SAVE</button> &nbsp;&nbsp; <button class="btn normal-btn small-btn cancel-btn" type="button">CANCEL</button>
                </div>
                <div class="verify-block">
                    <button class="btn small-btn normal-btn mb20 mynt-verify-btn" type="button" id="mynt-verify-btn">VALIDATE NOW</button><br>
                    In case you've just tried validating your number, please wait for a few minutes before trying again.
                </div>
			</div>
		{/if}
	</div>
    {if !$codErrorMessage}<button id="confirmCodOrderBtn" class="btn primary-btn right" disabled="disabled" type="submit">CONFIRM ORDER</button></div>{/if}
</form>

