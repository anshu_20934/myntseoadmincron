{extends file="layout-checkout.tpl"}
{block name=body}
    <script type="text/javascript">
        var orderid='{$orderid}';
        var giftCardId='{$giftCardId}';
    </script>
<div class="checkout confirm-page mk-cf giftcard-confirm-page">
    <h1>Confirmation</h1>

    {* mainContent *}
    <div class="main-content">
		{if $message}
			<p class="message">
				{$message}
			</p>
		{else}
			<p class="success-msg">
				<span class="success-icon"></span>Your order has been placed successfully!
			</p>
		{/if}
		<div class="order-details">
			<p><em>Order No. </em><span class="order-number">{$orderid}</span></p>
			<div>	
				<p class="body-txt">YOUR GIFT CARD HAS BEEN EMAILED{if $giftCard["recipientEmail"] neq ""} TO {$giftCard["recipientEmail"]}{/if}.</p>
			</div>
			<p>WE'VE ALSO EMAILED THE GIFT CARD TO YOU, FOR YOUR REFERENCE, AT:{if $giftCard["senderEmail"] neq ""} {$giftCard["senderEmail"]}{/if}.</p>
			<p>View your gift card details on your <a href="{$http_location}/mymyntra.php?view=mygiftcards" title="My Myntra" onClick="_gaq.push(['_trackEvent', 'confirmation', 'mymyntra']);return true;">My Myntra</a> page.</p>
		</div>
	    {*<p style={if $cashback_gateway_status eq 'on' && $cashback_tobe_given neq '' && $cashbackearnedcreditsEnabled eq '1'}"display:block;"{else}"display:none;"{/if}>{if $paybyoption == "cod"}You will receive a cashback of Rs. <b>{$cashback_tobe_given}</b> in your MyMyntra account after 30 days.{else}You will receive a cashback of Rs. <b>{$cashback_tobe_given}</b> in your MyMyntra account within the next hour.{/if}</p>*}

		<div id="more-info">
			<h3>HELP US KNOW YOU BETTER</h3>
			<p>
			This will help us recommend you products and send you mailers that are more relevant.<br />
			We care about your privacy and we will not share your personal data with third party.
			</p>
			<a href="{$http_location}/mymyntra.php#more-info" title="tell us about yourself now">TELL US ABOUT YOURSELF NOW</a>
			
	    </div>
	    <a href="{$http_location}" title="Continue Shopping" class="btn normal-btn" onClick="_gaq.push(['_trackEvent', 'confirmation', 'continue_shopping']);return true;">Continue Shopping</a>
	</div>
	{*mainContent@End*}
	{include file="checkout/giftcardsummary.tpl"}
</div>
{/block}

{block name=lightboxes}
    <!--cod mobile verification for confirmation.tpl-->
   {if $verify_mobile_for_COD eq 'true'}
   <div id="splash-mobile-verify" class="lightbox">
       <div class="mod">
           <div class="hd">
               <h2>VALIDATE MOBILE NUMBER</h2>
           </div>
           <div class="mob-description-section">
               <span class="mob-info" id="title-description">Your order has been successfully placed. Please verify your contact number to
                   help us process your order immediately.</span>
           </div>

           <div class="bd clearfix">
               <input type="hidden" name="_token" value="{$USER_TOKEN}"/>

               <div class="mob-row">
                   <label>Mobile : +91 - <strong>{$mobileToverify}</strong></label>
               </div>

               <div class="mob-row hide" id="code-div">
                   <label>Verify Code : </label>
                   <input type="text" value="" maxlength="4" size="4" class="mobile error" name="v_code">

                   <div class="mob-resend secure">
                       <div id="success-resent"><span class="tick-small-icon"></span><em>Code Resent</em></div>
                       <button class="btn primary-btn btn-save" id="resend-code" style="padding:0px 5px;height:26px;">Resend</button>
                   </div>

               </div>

               <div class="mob-info-section hide" id="success-div">
                   <span class="mob-info success">Your number is validated. Your order is being processed and will be dispatched from our warehouse in 24 hours.</span>
               </div>

               <div class="mob-info-section hide" id="account-error">
                   <span class="mob-info error">Your number has already been validated against another My Myntra account.</span>
               </div>

               <div class="mob-info-section hide" id="exceed-error">
                   <span class="mob-info error">You have exceeded the maximum number of attempts to verify this mobile number.
                       We will call you shortly to confirm your order.</span>
               </div>

               <div class="mob-info-section hide" id="wait-div">
                   <span class="mob-info">Please enter the verification code sent to {$mobileToverify}.
                       If you have not received your verification code, please wait for 2 minutes before clicking on the Resend link above.</span>
               </div>

               <div class="mob-info-section hide" id="code-error">
                   <span class="mob-info error">Please enter the valid verification code sent to your mobile.</span>
               </div>

               <div class="mob-action-section">
                   <button class="btn primary-btn btn-save" id="validate-mobile">Validate Mobile</button>
                   <button class="btn primary-btn btn-save hide" id="verify-code">Verify</button>
                   <button class="btn normal-btn btn-cancel hide" id="cancel-verify">Cancel</button>
                   <button class="btn normal-btn btn-cancel hide" id="close-verify">Close</button>
               </div>
           </div>
           <div class="ft">
               {if $contactUsLink}
                   <span class="note">{$contactUsLink}</em></span>
               {/if}
           </div>
       </div>
   </div>
   {/if}
   <!--cod mobile verification for confirmation.tpl-->
{/block}
