<form action="{$https_location}/{$paymentphp}" id="phone_payment" method="post"
	{if $totalAmount eq 0}
		style="display:none"
	{elseif $defaultPaymentType eq "pay_by_phone"}
		style="display: block;"
	{else}
		style="display:none"
	{/if}
>
	{if $giftCardPaymentPage}
		<input type="hidden" name='order_type' value='gifting'/>
	{/if}
	<input type="hidden" name='s_option' value ='{$logisticId}' />
	<!--delivery preference-->
	<input type="hidden" name="shipment_preferences" value="shipped_together" />
	<!--delivery preference-->
	<input type="hidden" name="is_shipping_cost_set" value="{$s_city}" />
	<input type="hidden" name="checksumkey" value="{$checksumkey}" />
	<input type="hidden" name="address" value="{$address.id}" />
	<input type="hidden" value="ivr" name="pm" />
	{if $onlineErrorMessage}
        {$onlineErrorMessage}
    {else}
		<div class="cod-page-notifications">
			<div align="center"><img src="{$secure_cdn_base}/skin1/images/phone-icon3.jpg"></div>
			<span class="pb-phone">Call up <span style="color:red">{$pay_by_phone_number}</span> to pay by phone.</span>
			<span>You could make a secure payment using Credit card or Debit card over phone.</span>
			<input type="button" value="Proceed to Confirm" id="phone-order-confirm" class="pay-by-phone-confirm next-button verify-captcha btn primary-btn">
		</div>
    {/if}
</form>
