<div style="border: 1px solid gray;">
	<div style="font-family: arial, sans-serif;  padding: 10px">
		<P style="text-transform: capitalize">Dear {$customerName},</P>
		We noticed that there are few items waiting for your attention in your shopping cart. <BR/>
		{if $discountUpto gt 0}
		We are running <i>upto {$discountUpto}%</i> discount on select products until stock lasts and this may be the best time to buy those items.<BR/> 
		{/if}		
		So hurry up and grab your products on Myntra!<BR/><BR/>
		
		You can <a href="http://www.myntra.com/mkmycart.php?utm_source=rem&utm_medium=abn_cart&utm_campaign={$campaignDate}">Click on this link</a> to visit your cart and complete your transaction.<BR/><BR/>
		<table style="width: 100%;border: 1px solid lightgray">
			<thead style="background-color: rgb(224, 224, 224);font-size: 110%; font-weight: bold;">
				<tr>
					<td colspan="2" style="padding: 10px">Shopping Cart Items</td>
					<td style="padding: 10px">Size</td>
					<td style="padding: 10px; text-align: right">Quantity</td>
					<td style="padding: 10px">Price</td>
				</tr>
			</thead>
			<tbody>
				{if $productsInCart|@count > 0}
				{foreach name=outer item=product key=itemid from=$productsInCart}
				<tr style="">
					<td style="border: 1px solid lightgray; width: 60px">
						<div>
							{assign var='productStyleId' value=$product.productStyleId}
							{assign var='item' value=$product.cartImagePath}
							{include file="affiliatetemplates/reebok_ipl/cricket_jersey_cart_images.tpl" assign='cartImagePath'}
							<img src="{if $cartImagePath|trim neq ' '}{$cartImagePath}{else}{$product.cartImagePath}{/if}" alt="{$product.productStyleName}">
						</div>
					</td>
					<td style="border: 1px solid lightgray;">
						<b>{if $product.is_customizable eq 0 and $product.totalCustomizedAreas eq 0 and $product.landingpageurl neq ''}
							<a href="{$http_location}/{$product.landingpageurl}">{$product.productStyleName}</a>{else}{$product.productStyleName}
							{/if}
						<br/><br/>
						Rs {$product.productPrice|string_format:"%.2f"}/-</b>
				           {if $product.discountDisplayText neq ''}
				           <div>{$product.discountDisplayText}</div>
				           {/if}
					</td>
					<td style="text-align:center; border: 1px solid lightgray; width: 100px">
					{assign var=breakloop value=false}
					{if $product.sizenames && $product.sizequantities}
						{if $product.sizenames|@count>1}
						{assign var=i value=0}
							{if $product.flattenSizeOptions}
								{assign var=productsizenames value=$product.sizenamesunified}
							{else}
								{assign var=productsizenames value=$product.sizenames}
							{/if}
							{foreach from=$productsizenames item=sizename}
								{if $product.sizequantities[$i] neq NULL and $breakloop eq false}
									{assign var=breakloop value=true}
									{assign var=selectedSize value=$sizename}
									{if $sizename eq 'XS'}Extra-Small
									{elseif $sizename eq 'S'}Small
									{elseif $sizename eq 'M'}Medium
									{elseif $sizename eq 'L'}Large
									{elseif $sizename eq 'XL'}X-Large
									{elseif $sizename eq 'XXL'}XX-Large
									{elseif $sizename eq 'XXXL'}XXX-Large
									{else}{$sizename}
									{/if}
								{/if}
								{assign var=i value=$i+1}
							{/foreach}
						{else}
							{$product.sizenames[0]}
						{/if}
					{else}
						NA
					{/if}
					</td>
					<td style="text-align: right; padding-right: 8px; border: 1px solid lightgray; width: 60px">
						{assign var=sku value=$product.productStyleId|cat:'-'|cat:$selectedSize}
						{$product.quantity}
						        {if ($checkInStock) and $product.quantity eq 0 }
						        <span style="color:#ff0000;">Zero unit(s) ordered</span>
						        {elseif ($checkInStock) and $product_availability[$itemid] lte 0 }
						        <span style="color:#ff0000;">Sold Out</span>
						        {elseif ($checkInStock) and $product.totalCustomizedAreas gt 0 and $total_consolidated_sale[$sku].total gt $product_availability[$itemid]}
						        <span style="color:#FF8040;">Only {$product_availability[$itemid]} units in stock</span>
						        {elseif ($checkInStock) and $product_availability[$itemid] lt $product.quantity }
						        <span style="color:#FF8040;">Only {$product_availability[$itemid]} units in stock</span>
						        {/if}
					</td>
				    <td style="text-align: right; border: 1px solid lightgray; padding-right: 8px; width: 90px">
				         <span>
				                 Rs {if $coupondiscount!=0}{$product.productPrice*$product.quantity-$product.discountAmount}{else}{$product.productPrice*$product.quantity-$product.discountAmount}{/if}
				         </span>
						<!-- span class="original-price strike">Rs {$product.totalPrice}</span  -->
				    </td>
				</tr>
				{/foreach}
				{/if}
			</tbody>
		</table>
		<br/>
		<div align="right">
			<a href="http://www.myntra.com/mkmycart.php?utm_source=rem&utm_medium=abn_cart&utm_campaign={$campaignDate}"
			 style="text-decoration: none; padding: 6px 40px; background-color: orange; font-size: 25px; font-family: arial; color: white; -moz-border-radius: 30px; -webkit-border-radius: 30px;-moz-linear-gradient(center bottom , #FF9009 22%, #FFAA44 81%);-moz-box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.2);">
			<b>Buy Now</b></a>
		</div>
		<br/>
		<BR/>
		Our Customer Champions are available 24 hours all 7 days of the week and will be glad to help you with completing this order or answer any other questions that you may have. 
		<BR/><BR/>
		Regards,<BR/>
		Team Myntra
		<BR/> 
	</div>
</div>