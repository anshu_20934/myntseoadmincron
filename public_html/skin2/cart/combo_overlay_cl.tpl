<!-- Header -->
<script>
    Myntra.sCombo.isConditionMet = {$isConditionMet|@json_encode};
    Myntra.sCombo.selectedStyles = {$selectedStyles|@json_encode};
	Myntra.sCombo.selectedStyleProductMap = {$selectedStyleProductMap|@json_encode};
    Myntra.sCombo.min = parseInt({$min}+0.5);
    Myntra.sCombo.ctype = "{$ctype}";
    Myntra.sCombo.cartSavings = parseInt({$comboSavings}+0.5);
    Myntra.sCombo.cartTotal = parseInt({$comboTotal}+0.5);
    Myntra.sCombo.products = {$products|@json_encode};
	Myntra.sCombo.freeproducts = {$freeproducts|@json_encode};
    Myntra.sCombo.solrProducts = {$dataMap|@json_encode};
	Myntra.sCombo.comboSet = {$comboSet|@json_encode};
    Myntra.sCombo.isAmount = {if $productsSet.dre_buyAmount>0} true; {else} false; {/if}
    Myntra.sCombo.ComboId = {$comboId};
    Myntra.sCombo.freeItems = new Object();
	Myntra.sCombo.getPercent = '{$dre_percent}';
	Myntra.sCombo.getAmount = '{$dre_amount}';
	Myntra.sCombo.getCount = '{$dre_count}';
	{if $freeItemsSolrResult|@count }
		Myntra.sCombo.freeItems = {$freeItemsSolrResult|@json_encode};
	{/if}
</script>

<div class="scombo">
<!-- Combo Message -->
<div class="combo-header-main" >
	{include file="string:{$displayText}"}
</div>


<!-- Conditional Message -->
<div class="combo-header-sub" >
	<div class="completion-message">{if $isConditionMet}<span class="tick-small-icon">&nbsp;</span> Combo complete! <em>Modify Combo</em> {else}<span class="warning-small-icon">&nbsp;</span> Combo incomplete. <em>{if $productsSet.dre_buyAmount}{$rs}{/if}{$min}{if $productsSet.dre_buyAmount}/-{/if} more to go!</em> {/if}</div>
	<!--<a>CLEAR COMBO</a>-->
</div>

<!-- Pagination Message -->
<div class="pagination-count">
	{if $data|@count}
	<span>Showing <span class="pagination-start">1</span>-<span class="pagination-end">{if $data|@count gt 5}5{else}{$data|@count}{/if}</span> of {$data|@count} products</span>
	{else if $freeItemsSolrResult|@count}
	<span>Showing <span class="pagination-start">1</span>-<span class="pagination-end">{if $data|@count gt 5}5{else}{$freeItemsSolrResult|@count}{/if}</span> of {$freeItemsSolrResult|@count} products</span>
	{/if}
</div>
{if $freeItemsSolrResult|@count  && $data|@count}
<div class="mk-combo-tabs" style="height:32px;">
    <ul class="mk-mynt-nav">
        <li><a href="#" class="item-tab">Item List</a></li>
        <li><a href="#" class="free-item-tab">Free Item</a></li>
    </ul>
</div>
<!-- Widget -->
<div id="tabs-1">
	{include file="cart/combo_overlay_widget_cl.tpl" combotype='defaultitemlist'}
</div>
<div id="tabs-2">
<!-- Free Items -->
	{include file="cart/combo_overlay_widget_cl.tpl" data=$freeItemsSolrResult combotype='freeitem'}
<!-- Free Items end -->
</div>
{elseif $data|@count}
	{include file="cart/combo_overlay_widget_cl.tpl" combotype='defaultitemlist'}
{elseif $freeItemsSolrResult|@count}
	{include file="cart/combo_overlay_widget_cl.tpl" combotype='freeitem' data=$freeItemsSolrResult}
{/if}
<!-- Action Buttons -->
<div class="action-buttons">
	<form method="post"  action="mkretrievedataforcart.php?pagetype=productdetail&lead=recently_viewed" id="form-{$widgetdata.styleid}" class="re-form-block">
	    <input type="hidden" name="_token" value="{$USER_TOKEN}" />
	    <span class="add-to-cart btn primary-btn combo-add-to-cart-button" disabled="true" >ADD TO BAG</span>
		&nbsp; &nbsp; <a class="combo-overlay-cancel" href="javascript:void(0)">CANCEL</a>
	</form>
</div>
<!-- Total and Savings -->
<div class="combo-overlay-total">
    <div>YOU PAY <span class="rupees">Rs. <span class="total">{math equation="a-b" a=$comboTotal b=$comboSavings assign="comboActualTotal"}{$comboActualTotal|round|number_format:0:".":","}</span></span></div>
    <div class="savings-msg">YOU SAVE <span class="rupees">Rs. <span class="savings"> 
    {if isset($comboSavings) && $comboSavings>0}
    {$comboSavings|round|number_format:0:".":","}
    {else}
    0
    {/if}
    </span></span></div>
    {if $isConditionMet neq true}
        <div class="message" style="padding:5px 0px;">
			{if !$freeItemsSolrResult|@count }
				- {/if}Complete the combo to save!</div>
    {/if}	
</div>
</div>

