<form action="{$https_location}/{$paymentphp}" method="post" id="emi" target="MyntraPayment"
	{if $totalAmount eq 0}
		style="display:none"
	{elseif $defaultPaymentType eq "emi"}
		style="display: block;"
	{else}
		style="display:none"
	{/if}
>
    {if $customeragent}
	{if $giftCardPaymentPage}
		<input type="hidden" name='order_type' value='gifting'/>
	{/if}
	<input type="hidden" name='s_option' value ='{$logisticId}'/>
	<!--delivery preference-->
	<input type="hidden" name="shipment_preferences" value="shipped_together" >
	<!--delivery preference-->
	<input type="hidden" name="is_shipping_cost_set" value="{$s_city}">
	<input type="hidden" name="checksumkey" value="{$checksumkey}">
	<input type="hidden" name="address" value="{$address.id}">
	<input type="hidden" value="emi" name="pm" />
	<input type="hidden" value="first time" name="emi_bank" id="emi_bank" />
    {if $onlineErrorMessage}
       	{$onlineErrorMessage}
    {else}
    	{if $EMIenabled}
			{if $EMIEligibleGatewayUp}
				
			<div class="cc-form">
			        
			        <div class="emi-options">
			        
					        <div class="row emi-bank">
					            <label>Which Bank is your Credit Card from? *</label>
					            <select id="emi-bank" name="selectedBank">
						            {foreach from=$EMIBanksAvailable item=emibank key=bank} 
	            						<option value="{$bank}">{$emibank.BankName|upper}</option>
								{/foreach}                    
					            </select>
					        </div>
			
			        	<h3>EMI Options</h3>
			        		<p class="gray">Select an EMI option that is most convenient for you</p>
						{foreach from=$EMIBanksAvailable item=emibank key=bank name="emiLoop"}
                            <div class="emi-duration {if $smarty.foreach.emiLoop.index != 0}mk-hide{/if}" id="{$bank}">
			        		<table border="0" cellspacing="0">
			        			<colgroup>
				        			<col width="30" />
				        			<col width="100" />
				        			<col width="100" />
				        			<col width="120" />
				        			<col width="100" />
			        			</colgroup>
			        			<tbody>
				        			<tr>
				        				<th></th>
				        				<th>Duration</th>
				        				<th>Installment</th>
				        				<th>Processing Fee</th>
				        				<th>Total</th>
				        			</tr>
                                        {assign var="unselectedFlag" value=true}
                                        {foreach from=$emibank.Durations item=attributes key=months}
                                        	<tr class="{cycle name='emi_cycle' values='odd,even'} {if !$attributes.Enabled} emi-disabled {/if}">
                                           	 	<td><div class="radio-btn {if $attributes.Enabled && $unselectedFlag} {assign var="unselectedFlag" value=false} selected {/if}"></div></td>
                                           	 	<td class="duration" data-duration="{$bank|lower}{$attributes.Installments|lower}emi">{$attributes.Installments} Months</td>
                                           	 	{if $attributes.Enabled}
	                                            	<td class="rupees">{$rupeesymbol} {$attributes.PerMonthEMI|number_format:0:".":","}</td>
    	                                        	<td class="rupees fee">{if $attributes.Charge gt 0}{$rupeesymbol} {$attributes.Charge|number_format:0:".":","}{else}FREE{/if}</td>
        	                                    	<td class="rupees red total">{$rupeesymbol} {$attributes.TotEMIAmount|number_format:0:".":","}</td>
                            	    {else}
        	                                    	<td class="msg gray" colspan="3">
        	                                    		Applicable above order value <span class="rupees">{$rupeesymbol}</span> {$attributes.MinAmount}
        	                                    	</td>
        	                                    {/if}
                	                </tr>
                                        {/foreach}
			        			</tbody>
			        		</table>
			        	</div>
                        {/foreach}
		        </div>
		        
		        <h3>Card Details</h3>
		
		        <div class="accepted-cards">
		            <span class="cardType visa">Visa</span>
		            <span class="cardType master">Mastercard</span>
		        </div>
		
				<div class="row">
					<label>Card number <em>Without Spaces</em> *</label>
					<input type="text" class="long" maxlength="19" name="card_number" autocomplete="off" onkeyup="showUserCardType(document.getElementById('emi').card_number.value);" onfocus="showUserCardType(document.getElementById('emi').card_number.value);" onchange="showUserCardType(document.getElementById('emi').card_number.value);">
				</div>
		
				<div class="row">
					<label class="required">Name on credit card *</label>
					<input type="text" title="Name on credit card" value="" name="bill_name" class="long">
				</div>
		
				<div class="row cc-expiry mk-cf">
		            <div class="col mk-f-left">
					    <label>Expiry Month *</label>
						<select name="card_month">
							<option value="">Month</option>
							<option value="01"> 1 </option>
							<option value="02"> 2 </option>
							<option value="03"> 3 </option>
							<option value="04"> 4 </option>
							<option value="05"> 5 </option>
							<option value="06"> 6 </option>
							<option value="07"> 7 </option>
							<option value="08"> 8 </option>
							<option value="09"> 9 </option>
							<option value="10"> 10 </option>
							<option value="11"> 11 </option>
							<option value="12"> 12 </option>
						</select>
		            </div>
		            <div class="col mk-f-right">
					    <label>Expiry Year *</label>
						<select name="card_year">
							<option value="">Year</option>
							<option value="12">2012</option>
							<option value="13">2013</option>
							<option value="14">2014</option>
							<option value="15">2015</option>
							<option value="16">2016</option>
							<option value="17">2017</option>
							<option value="18">2018</option>
							<option value="19">2019</option>
							<option value="20">2020</option>
							<option value="21">2021</option>
							<option value="22">2022</option>
							<option value="23">2023</option>
							<option value="24">2024</option>
							<option value="25">2025</option>
							<option value="26">2026</option>
							<option value="27">2027</option>
							<option value="28">2028</option>
							<option value="29">2029</option>
							<option value="30">2030</option>
							<option value="31">2031</option>
							<option value="32">2032</option>
							<option value="33">2033</option>
							<option value="34">2034</option>
							<option value="35">2035</option>
							<option value="36">2036</option>
							<option value="37">2037</option>
							<option value="38">2038</option>
							<option value="39">2039</option>
							<option value="40">2040</option>
						</select>
					</div>
				</div>
		
				<div class="row">
					<label>CVV Code *</label>
					<input type="password" cardtypefieldname="card_type" title="Card Verification Value Code"
		                    id="cvv" maxlength="4" size="4" value="" name="cvv_code" autocomplete="off">
		            <a href="#whatiscvv" class="what-is-cvv cc-cvv">What is a cvv code?</a>
				</div>
		
		        <div class="msg-fill-all">
		            * Required fields
		        </div>
			</div>
			{if !$giftCardPaymentPage}
			<div class="cc-form cc-bill-address">
		        <h3>Credit Card Billing Address</h3>
		
		        <div class="msg">
		            Your billing address is used to prevent fraudulent use of your card.
		            <br>
		            Enter the address exactly as it appears on your card statement.
		            <br>
		            <input type="button" value="Copy my shipping address" onclick="copycontactaddress(true);" class="btn link-btn">
		        </div>
		
		        <div class="row">
		            <label>Name *</label>
		            <input type="text" name="b_firstname" id="cc_b_firstname" value="" class="long">
		        </div>
		
		        <div class="row">
		            <label>Address *</label>
		            <textarea name="b_address" id="cc_b_address" class="long" rows="3"></textarea>
		        </div>
		
		        <div class="row mk-cf">
		            <div class="col mk-f-left">
		                <label>City *</label>
		                <input type="text" name="b_city" id="cc_b_city" value="" class="short">
		            </div>
		            <div class="col mk-f-right">
		                <label>State *</label>
		                <input type="text" name="b_state" id="cc_b_state" value="" class="short">
		            </div>
		        </div>
		
		        <div class="row">
		            <label>Pincode *</label>
		            <input type="text" name="b_zipcode" id="cc_b_zipcode" value="" class="short" maxlength="6">
		        </div>
		
		        <div class="row cc-bill-country">
		            <label>Country *</label>
		            <select id="cc_b_country" name="b_country">
		                {foreach key=countrycode item=countryname from=$countries}
		                    <option value="{$countrycode}" {if $countrycode eq 'IN'}selected="true"{/if}>{$countryname}</option>
		                {/foreach}
		            </select>
		        </div>
		
		        <div class="msg-fill-all">
		            * Required fields
		        </div>
			</div>
			{/if}
		{else}
			<span>
					<div class="warning-text-border">
			            <p><span class="warning-icon"></span>{$EMIGatewayDownListErrorMsg}</p>
			        </div>
	        </span>
		{/if}
	{else}
			Credit card EMI options are available only for orders above value of <span class="red">{$rupeesymbol} {$minAmountforEMI}</span>.
		<br/>Please select another payment option.
	{/if}
    {/if}
    {else}
    {$telesalesmsg}
    {/if}
</form>
