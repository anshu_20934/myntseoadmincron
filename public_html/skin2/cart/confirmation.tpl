{extends file="layout-checkout.tpl"}
{block name=body}
    <script type="text/javascript">
        var orderid='{$orderid}';
        Myntra.Data.socialAction='purchase';
        Myntra.Data.pageName='confirmation';
        //Exclusion required for gifts sent.
        {if $giftamount > 0} 
            var socialPost = 0;
        {else}
            var socialPost = 1;
        {/if}
        var socialPostArray={$socialArray};
        var socialPostStyleDetailsArray={$socialPostStyleDetailsArray};
    </script>

    <script>
        Myntra.Confirmation = Myntra.Confirmation || [];
        Myntra.Confirmation.Data = {
            orderId : {$orderid},
            shipping : {$shippingCharges},
            shippingCode : '{$userinfo.s_zipcode}',
            shippingCity : '{$userinfo.s_city}',
            paymentOption : '{$paymentoption}',
            couponCode : '{$couponCode}',
            totalAmount : {$amountafterdiscount|string_format:'%d'},
            totalQuantity : {$totalQuantity},
            products : '{$products}',
            productIds : '{$styleIdList}',
            isFirstOrder : {$isFirstOrder},
            orderSociomanticString : '{$orderSociomanticString}',
            orderNanigansString : '{$orderNanigansString}',
            CartProductId1 : '{$CartProductId1}',
            CartProductAmount1 : '{$CartProductAmount1}',
            CartProductQuantity1 : '{$CartProductQuantity1}',
            CartProductId2 : '{$CartProductId2}',
            CartProductAmount2 : '{$CartProductAmount2}',
            CartProductQuantity2 : '{$CartProductQuantity2}',
            CartProductId3 : '{$CartProductId3}',
            CartProductAmount3 : '{$CartProductAmount3}',
            CartProductQuantity3 : '{$CartProductQuantity3}',
            CartProductId4 : '{$CartProductId4}',
            CartProductAmount4 : '{$CartProductAmount4}',
            CartProductQuantity4 : '{$CartProductQuantity4}',
            CartProductId5 : '{$CartProductId5}',
            CartProductAmount5 : '{$CartProductAmount5}',
            CartProductQuantity5 : '{$CartProductQuantity5}',
        };
    </script>

<div class="checkout confirm-page mk-cf">
    <h1>Confirmation</h1>

    {* mainContent *}
    <div class="main-content">
		{if $message}
			<p class="message">
				{$message}
			</p>
		{else}
			<p class="success-msg">
				<span class="success-icon"></span>Your order has been placed successfully!
			</p>
		{/if}
		<div class="order-details">
			<p><em>Order No. </em><span class="order-number">{$orderid}</span></p>
			<p>You can Track / Return this order from your <a href="{$http_location}/mymyntra.php?view=myorders" title="My Myntra" onClick="_gaq.push(['_trackEvent', 'confirmation', 'mymyntra']);return true;">My Myntra</a> page.</p>
		</div>
		{if $paybyoption == "cod"}
			<div>
				<p class="body-txt">You have chosen to pay cash on delivery. Please have <em>{$rupeesymbol} {$amountafterdiscount|round|number_format:0:".":","}</em> ready in cash at the shipping address to pay for this order on delivery.</p>
			</div>
		{/if}
		<div>	
			<p class="body-txt">Your order is being processed and will be dispatched from our warehouse within 24 hours. Post shipping you will be receiving an email stating the expected delivery date for your Order.</p>
			<p class="body-txt">A confirmation email has been sent to <span class="email">{$userinfo.b_email}</span></p>
		</div>
                <!--Kuliza Code start -->
                {if !$telesales}
                    {include file="socialAction.tpl" echo_flow="checkout" quicklook="no" popupdiv="true"}
                {/if}
                <!--Kuliza Code end -->
		{if $shopingFestFG}
			<p class="body-txt">You could win exciting prizes such as Galaxy Note and Volkswagen Polo.<br /> 
Know more about our <a href="{$http_location}/shopfest.php" title="Myntra Shop Fest">Hot in December Contest</a>.</p>
		{/if}
		{if $confirmationConfigMsg}
			 <div id="special-coupons" class="info-block">	    	 	
		    	<p class="body-txt">
		    	
					<em >{$confirmationConfigMsg}</em><br />
				</p>
			</div>
		{/if}
		{if $fest_coupons}
	    <div id="special-coupons" class="info-block">
	    	
	    	{if $coupon_percent }
	    	
		    	<h3>YOU HAVE WON A CASHBACK COUPON{if $fest_coupons|count > 1}S{/if}</h3>
		    	<p class="body-txt">
		    	
					<em >You have won a cashback coupon worth Rs. {$coupon_value}. <br />This coupon can be used for a {$coupon_percent}% off up to a maximum discount of Rs. {$coupon_value}.</em><br />
				</p>
	    	
	    	
	    	{else}
	    	
		    	<h3>YOU HAVE WON {$fest_coupons|count} COUPON{if $fest_coupons|count > 1}S{/if}</h3>
		    	<p class="body-txt">
		    	
					<em >You have won {$fest_coupons|count} coupons worth Rs. {$coupon_value} each</em><br />
				</p>
			
			{/if}
			
			{* <p class="body-txt">
			You can use these coupons on your future purchases at Myntra<br />
								<span class="or">OR</span>
					<a class="share-link" href="{$http_location}/mymyntra.php?view=mymyntcredits#top">TRANSFER THEM TO YOUR FRIENDS</a>
	    	</p> *}
	    	
				<div class="mk-mynt-promo-codes mk-active-coupons">
				<div class="mk-coupons-content">
				<table>
					<col width="120" />
					<col width="290" />
					<col width="190" />
					<tr class="mk-table-head"> 
						<th>Coupon</th>
						<th class="left">Description</th>
						<th>Exp Date</th>
					</tr>
					{section name=prod_num loop=$fest_coupons}
					<tr class="{cycle name='active_cycle' values='mk-odd,mk-even'}">
						<td>{$fest_coupons[prod_num].coupon|upper}</td>
						<td class="left">
						{if $fest_coupons[prod_num].couponType eq "absolute"}
                           	<span class="rupees">Rs. {$fest_coupons[prod_num].MRPAmount|round}</span> off
                           {elseif $fest_coupons[prod_num].couponType eq "percentage"}
                           	{$fest_coupons[prod_num].MRPpercentage}% off
                           {elseif $fest_coupons[prod_num].couponType eq "dual"}
                           	{$fest_coupons[prod_num].MRPpercentage}% off upto
                           	<span class="rupees">Rs. {$fest_coupons[prod_num].MRPAmount|round}</span>
                        	{/if}
                       	{if $fest_coupons[prod_num].minimum neq '' && $fest_coupons[prod_num].minimum gt 0}
                           	on a minimum purchase of <span class="rupees">Rs. {$fest_coupons[prod_num].minimum|round}</span>
                       	{/if}
						</td>
						<td>{$fest_coupons[prod_num].expire|date_format:"%e %b, %Y"}
                       	({math equation= "ceil(x/86400) - ceil(y/86400) + 1" x=$fest_coupons[prod_num].expire y=$today assign="valid_days"}{$valid_days}
                       	{if $valid_days eq 1}day{else}days{/if} to expiry)</td>
					</tr> 
					{/section}
				</table>
				</div>
			</div>
		</div>
		{/if}
		{*<p style={if $cashback_gateway_status eq 'on' && $cashback_tobe_given neq '' && $cashbackearnedcreditsEnabled eq '1'}"display:block;"{else}"display:none;"{/if}>{if $paybyoption == "cod"}You will receive a cashback of Rs. <b>{$cashback_tobe_given}</b> in your MyMyntra account after 30 days.{else}You will receive a cashback of Rs. <b>{$cashback_tobe_given}</b> in your MyMyntra account within the next hour.{/if}</p>*}
		
		<div id="more-info" class="info-block">
			<h3>HELP US KNOW YOU BETTER</h3>
			<p>
			This will help us recommend you products and send you mailers that are more relevant.<br />
			We care about your privacy and we will not share your personal data with third party.
			</p>
			<a href="{$http_location}/mymyntra.php?view=myprofile#more-info" title="Tell us about yourself now">TELL US ABOUT YOURSELF NOW</a>
			
	    </div>
	    <a href="{$http_location}" title="Continue Shopping" class="btn normal-btn" onClick="_gaq.push(['_trackEvent', 'confirmation', 'continue_shopping']);return true;">Continue Shopping</a>
	</div>
	{*mainContent@End*}
	{include file="checkout/confirmation-summary.tpl"}
</div>
    {if $fireSyncGACall}
	<script language="JavaScript" src="https://ssl.google-analytics.com/ga.js"></script>
    {/if}
        <script type="text/javascript">
        try {ldelim}
        	//var pageTracker = _gat._getTracker("{$ga_acc}");
        	_gaq.push(['_setCustomVar',
        	//pageTracker._setCustomVar(
                    1,
                    "Already-Bought",
                    "Yes",
                    1]);

        	{*if $payment_method neq 'chq'*}
				_gaq.push(['_addTrans',
				"{$orderid}",                                     // Order ID
				"",
				"{$amountafterdiscount|string_format:'%.2f'}",	  // Total
				"{$vat}",                                     	  // Tax
				"{$shippingCharges}",                                // Shipping
				"{$userinfo.s_city}",                             // City
				"{$userinfo.s_state}",                            // State
				"{$userinfo.s_country}"]                              // Country
				);
			{*/if*}

			{assign var='loopIndex' value=0}
        	{assign var='netTotalPrice' value=0}
        	{assign var='productidDelimString' value=''}
        	{assign var='productTypeDelimString' value=''}
			{assign var='totalQuantity' value=0}

			{***products detail for my things conversion***}
			var myThingsProducts = new Array();

        	{foreach name=outer item=product from=$productsInCart}
        		{foreach key=key item=item from=$product}
        			{if $key eq 'productId'}
        			{assign var='productId' value=$item}
        			{assign var='productidDelimString' value=$productidDelimString|cat:$productId|cat:"|"}
        			{/if}

        			{if $key eq 'unitPrice'}
            		{assign var='unitPrice' value=$item}
            		{/if}

        			{if $key eq 'productStyleName'}
        			{assign var='productStyleName' value=$item}
        			{/if}

        			{if $key eq 'quantity'}
        			{assign var='quantity' value=$item}
        			{assign var='totalQuantity' value=$quantity+$totalQuantity}
        			{/if}

        			{if $key eq 'discount'}
                    {assign var='discount' value=$item}
                    {/if}

                    {if $key eq 'coupon_discount'}
                    {assign var='coupon_discount' value=$item}
                    {/if}

        			{if $key eq 'productTypeLabel'}
        			{assign var='productTypeLabel' value=$item}
        			{assign var='productTypeDelimString' value=$productTypeDelimString|cat:$productTypeLabel|cat:"|"}
        			{/if}

        		  	{if $key eq 'productCatLabel'}
					{assign var='productCatLabel' value=$item}
					{/if}

        			{if $key eq 'totalPrice'}
        			{assign var='totalPrice' value=$item}
        			{assign var='netTotalPrice' value=$netTotalPrice+$totalPrice}
					{/if}

					{***added for tyroo conversion tracking***}
					{if $key eq 'productPrice'}
						{assign var='productPrice' value=$item}
						{*assign var='itemString' value=":prod:$productPrice:qty:$quantity"*}
						{*assign var='lineItemString' value=$lineItemString|cat:$itemString*}
        			{/if}
        			{***tyroo till here***}

        		{/foreach}
				{assign var='itemString' value=":prod:$productPrice:qty:$quantity"}
				{assign var='lineItemString' value=$lineItemString|cat:$itemString}
        		{assign var='totalDiscountOnItems' value=$coupon_discount}
        		{assign var='discountPerItem' value=$totalDiscountOnItems/$quantity}
        		{assign var='unitPriceAfterDiscount' value=$unitPrice-$discountPerItem}



				{*if $payment_method neq 'chq'*}
					_gaq.push(['_addItem',
					"{$orderid}",			// Order ID
					"{$productId}",         //pid
					"{$productStyleName}",  //style
					"{$productCatLabel}",   //articleType|Brand
					"{$unitPriceAfterDiscount}",        //unit price
					"{$quantity}"           // Quantity
					]);
				{*/if*}

				{***products detail for my things conversion	***}
				myThingsProducts[{$loopIndex}] = {ldelim}id: "{$productId}",price:"{$productPrice|number_format:'2':'.':''}",qty:"{$quantity}"{rdelim};

				{assign var='loopIndex' value=$loopIndex+1}
            {/foreach}

            	{***tyroo conversion tracking***}
                    {*****MOD: not sending (p,q) tuples anymore****}
			{*if $lineItemString|strlen > 40*}
				{assign var='lineItemString' value=":prod:$amountafterdiscount:qty:1"}
			{*/if*}

            {*if $payment_method neq 'chq'*}
				_gaq.push(['_trackTrans']);

				{if $recordGACall}
					{literal}
                        _gaq.push(function(){
                            var newdiv = document.createElement('img');
                            var newGASrc= https_loc+"/baecon/{/literal}{$orderid}{literal}";
                            newdiv.setAttribute("src",newGASrc);
                            newdiv.setAttribute("width","1");
                            newdiv.setAttribute("height","1");
                            newdiv.setAttribute("alt","");
                            document.appendChild(newdiv);
                        });
                    {/literal}
				{/if}

            // Yahoo Conversion Tracking
            /*window.ysm_customData = new Object();
            window.ysm_customData.conversion = "transId={$orderid},currency=INR,amount={$amountafterdiscount|string_format:'%.2f'}";
            var ysm_accountid  = "1GJC30EFMMKEBN6R4ASQ2SBNQCG";
            document.write("<SCR" + "IPT language='JavaScript' type='text/javascript' "
            + "SRC=https://" + "srv2.wa.marketingsolutions.yahoo.com" + "/script/ScriptServlet" + "?aid=" + ysm_accountid
            + "></SCR" + "IPT>");*/

            {*/if*}
        {rdelim} catch(err) {ldelim}{rdelim}
        
      
		</script>

    <!--retarget pixel tracking-->
    {if $retargetPixel.confirmHttps}
        <script type="text/javascript">
        Myntra.Data.confirmHttps='{$retargetPixel.confirmHttps}';
        </script>
        <span id="retargetSpan" style="display:none;"></span>
    {/if}
{/block}

{block name=lightboxes}
    <!--cod mobile verification for confirmation.tpl-->
   {if $verify_mobile_for_COD eq 'true'}
   <div id="splash-mobile-verify" class="lightbox">
       <div class="mod">
           <div class="hd">
               <h2>VALIDATE MOBILE NUMBER</h2>
           </div>
           <div class="mob-description-section">
               <span class="mob-info" id="title-description">Your order has been successfully placed. Please verify your contact number to
                   help us process your order immediately.</span>
           </div>

           <div class="bd clearfix">
               <input type="hidden" name="_token" value="{$USER_TOKEN}"/>

               <div class="mob-row">
                   <label>Mobile : +91 - <strong>{$mobileToverify}</strong></label>
               </div>

               <div class="mob-row hide" id="code-div">
                   <label>Verify Code : </label>
                   <input type="text" value="" maxlength="4" size="4" class="mobile error" name="v_code">

                   <div class="mob-resend secure">
                       <div id="success-resent"><span class="tick-small-icon"></span><em>Code Resent</em></div>
                       <button class="btn primary-btn btn-save" id="resend-code" style="padding:0px 5px;height:26px;">Resend</button>
                   </div>

               </div>

               <div class="mob-info-section hide" id="success-div">
                   <span class="mob-info success">Your number is validated. Your order is being processed and will be dispatched from our warehouse in 24 hours.</span>
               </div>

               <div class="mob-info-section hide" id="account-error">
                   <span class="mob-info error">Your number has already been validated against another My Myntra account.</span>
               </div>

               <div class="mob-info-section hide" id="exceed-error">
                   <span class="mob-info error">You have exceeded the maximum number of attempts to verify this mobile number.
                       We will call you shortly to confirm your order.</span>
               </div>

               <div class="mob-info-section hide" id="wait-div">
                   <span class="mob-info">Please enter the verification code sent to {$mobileToverify}.
                       If you have not received your verification code, please wait for 2 minutes before clicking on the Resend link above.</span>
               </div>

               <div class="mob-info-section hide" id="code-error">
                   <span class="mob-info error">Please enter the valid verification code sent to your mobile.</span>
               </div>

               <div class="mob-action-section">
                   <button class="btn primary-btn btn-save" id="validate-mobile">Validate Mobile</button>
                   <button class="btn primary-btn btn-save hide" id="verify-code">Verify</button>
                   <button class="btn normal-btn btn-cancel hide" id="cancel-verify">Cancel</button>
                   <button class="btn normal-btn btn-cancel hide" id="close-verify">Close</button>
               </div>
           </div>
           <div class="ft">
               {if $contactUsLink}
                   <span class="note">{$contactUsLink}</em></span>
               {/if}
           </div>
       </div>
   </div>
   {/if}
   <!--cod mobile verification for confirmation.tpl-->
   
   
   
   
<script language="JavaScript" src="https://media.richrelevance.com/rrserver/js/1.0/p13n.js"></script>
<script type="text/javascript">
   // rich relevance instrumentation
   
   function getRRCookie(cname)
	{
		if(localStorage){
			var ls_rr_sid = localStorage.getItem('lscache-rr_sid'); // geting from local storage
			
			if( ls_rr_sid ){
				return ls_rr_sid;
			}
		}
				
		var name = Myntra.Data.cookieprefix+""+cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) 
		{
		  var c = ca[i].trim();
		  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
		}
		return "";
	}
	
	var R3_COMMON = null;
	var R3_ITEM = null;
   
  	
		R3_COMMON = new r3_common();

		
		R3_COMMON.setApiKey('54ed2a6e-d225-11e0-9cab-12313b0349b4');
		
		if(Myntra.Data.cookieprefix != "" ){
			R3_COMMON.setBaseUrl(window.location.protocol+'//integration.richrelevance.com/rrserver/');
		}else{
			R3_COMMON.setBaseUrl(window.location.protocol+'//recs.richrelevance.com/rrserver/');
		}
		
		R3_COMMON.setClickthruServer(window.location.protocol+"//"+window.location.host);
		var rr_sid = getRRCookie("rr_sid")+"";
		R3_COMMON.setSessionId(rr_sid);
		
		if(typeof Myntra.Data.userHashId == "undefined" || Myntra.Data.userHashId == "" ){
			R3_COMMON.setUserId(rr_sid);
		}else{
			R3_COMMON.setUserId(Myntra.Data.userHashId+"");
		}
		
		RR.jsonCallback = function (){
			//console.dir(RR.data.JSON.placements);
			//console.dir("done");
		}; // call back performed, once rr call is executed.

		var R3_PURCHASED = new r3_purchased();
				
		R3_PURCHASED.setOrderNumber(Myntra.Confirmation.Data.orderId+'');
		
		try{
			var products = JSON.parse(Myntra.Confirmation.Data.products);
		}catch(err){
			return; // unable to parse the items
		}
		
		
		products.forEach(function(k,v){			
			R3_PURCHASED.addItemIdPriceQuantity(k.id+'', k.price+'', k.quantity+'');		
		});
				
		

		rr_flush_onload();
		
		r3();
		
	
</script>
   
   
{/block}


{block name="macros"}
    {include file="macros/confirmation-macros.tpl"}
{/block}
