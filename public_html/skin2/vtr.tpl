{extends file="layout.tpl"}

{block name=navigation}
    {if $layoutVariant eq 'test'}
		{include file="inc/navigation-default.tpl"}
	{else}
		{include file="inc/navigation-collapsed.tpl"}
	{/if}
{/block}

{block name=body}
<script>
    Myntra.Data.pageName="vtr";
</script>
<div id="vtr" class="vtr">
    <h1>Style Studio <sup>BETA</sup></h1>
    {$markupProducts}
    {$markupRoom}
    <div style="clear:both"></div>
    <div id="vtr-first-run-help"></div>
</div>
{/block}

{block name=lightboxes}
<div class="lightbox" id="lb-nocanvas-msg">
    <div class="mod">
        <div class="hd">Your Browser needs a makeover!</div>
        <div class="bd">
            <p>Please upgrade your browser <span class="ie highlight">(Internet Explorer)</span> to access Style Studio</p>
            <p class="ie"><a class="btn normal-btn" href="http://www.microsoft.com/india/windows/ie/IE9.aspx">Download Latest Version</a></p>
            <p>We hope to see you again really soon!</p>
            <p>Alternatively you can also use:</p>
            <ul>
               <li>Google Chrome 10+</li>
               <li>Mozilla Firefox 4+</li>
               <li>Mac Safari 5.1+</li>
               <li>Opera 10+</li>
            </ul>
        </div>
    </div>
</div>
{/block}

{block name=pagejs}
<script>
    {$scriptProducts}
    {$scriptRoom}
</script>
{/block}

