{extends file="layout.tpl"}

{block name=navigation}
	{if $layoutVariant eq 'test'}
		{include file="inc/navigation-default.tpl"}
	{else}
		{include file="inc/navigation-collapsed.tpl"}
	{/if}
{/block}

{block name=body}
	<script>
		var pageName = '{$landingPage}';
	</script>
	<div class="mk-one-column mk-landing-page fashion-trends">
		<h1>Fashion Trends</h1>
		<section class="mk-site-main">
			<div class="trends-carousel rs-carousel" id="trends-carousel">
				<ul class="rs-carousel-runner">
					{foreach key=key item=image from=$carouselImages}
						<li class="rs-carousel-item"><a href="{$image.target_url}">
							<img src="{if $image@first}{$image.image_url}{else}{$cdn_base}/skin2/images/spacer.gif{/if}" {if $image@first}{else}class="lazy" data-lazy-src="{$image.image_url}"{/if} />
						</a></li>
					{/foreach}
				</ul>
			</div>
			<div id="thumbs">
				<div class="thumbs-inner"></div>
			</div>
		</section>
	</div>
{/block}

{block name=pagejs}
	<script>
		Myntra.Data.trendsThumbs={$thumbImages};
	</script>
{/block}
