<div class="order-tracking">
    <div class="header">
    	<div class="title">TRACKING DETAILS - Order No. {$trackingData.orderid}</div>
    	<div class="sub-title gray">
    		Order Placed On : {$trackingData.placedDate|date_format:"%d %b, %Y"} /
    		{$trackingData.itemCount} {if $trackingData.itemCount eq 1}Item{else}Items{/if}
    		{if $trackingData.shipments|@count gt 1}/ Multiple Shipments{/if}
    	</div>
    </div>
    {foreach $trackingData.shipments as $orderid => $order}
		<div class="shipment" id="shipment-{$orderid}">
		    <div class="shipment-header">
		    	<h4 class="gray">
			    	{if $order.shipmentNo gt 0}Shipment #{$order.shipmentNo}{else}{if $order.statusMsg|upper eq 'PROCESSED'}Processed{else}Being Processed{/if}{/if}
			    	({$order.itemCount} {if $order.itemCount eq 1}Item{else}Items{/if})
			    	{if $order.statusMsg|upper eq 'SHIPPED' || $order.statusMsg|upper eq 'DELIVERED'}<span class="gray">/</span> {$order.statusMsg}{/if}
		    	</h4>
		    	{foreach $order.items as $item}
		    		<div class="item">
		    			{$item.name} <span class="gray">/ Size:</span> {$item.size} <span class="gray">/ Qty:</span> {$item.qty}
		    		</div>
	    		{/foreach}
	    		<div class="expand-ico"></div>
		    </div>
		    {if is_array($order.trackingDetails) && count($order.trackingDetails) > 0}
	    <div class="details">
				    <div class="mk-cf">
				    	<div class="mk-f-left">Courier : 
					    	{if $order.trackingWebsite|trim && $order.trackingWebsite|trim neq "#"}<a href="{$order.trackingWebsite}" target="_blank" title="Track Status on {$order.courier}'s Website">{/if}
				    		{$order.courier}
					    	{if $order.trackingWebsite|trim && $order.trackingWebsite|trim neq "#"}</a>{/if}
			    		</div> 
				    	<div class="mk-f-right">Tracking No. : {$order.trackingNo}</div>
			    	</div>
	        <table width="100%">
		        <col width="180">
				        <col width="400">
		        <col width="200">
                <tr class="mk-table-head">
                    <th>DATE & TIME</td>
                    <th class="left">REMARKS</td>
                    <th>LOCATION</td>
                </tr>
			            {section name=rows loop=$order.trackingDetails}
	                <tr class="{cycle values="mk-even,mk-odd"}">
			                    <td>{$order.trackingDetails[rows].scanDate|date_format:"%d %b, %Y %I:%M %p"}</td>
		                    	<td class="left">{$order.trackingDetails[rows].remarks}</td>
		                    	<td>{$order.trackingDetails[rows].scannedlocation}</td>
	                </tr>
	            {/section}
	       </table>
    	</div>
		    {else if $order.trackingNo}
		    	<div class="details">
				    <div>Courier : 
				    	{if $order.trackingWebsite|trim && $order.trackingWebsite|trim neq "#"}<a href="{$order.trackingWebsite}" target="_blank" title="Track Status on {$order.courier}'s Website">{/if}
				    	{$order.courier}
				    	{if $order.trackingWebsite|trim && $order.trackingWebsite|trim neq "#"}</a>{/if}
			    	</div>
			    	<div>Tracking No. : {$order.trackingNo}</div>
				    {if $order.showTrackingStatus eq 'unsupported'}
					    <div class="body-txt">This Courier is currently not supported by our order tracking system.<br />
				    		You can track this shipment at the 
				    			{if $order.trackingWebsite|trim && $order.trackingWebsite|trim neq "#"}<a href="{$order.trackingWebsite}" target="_blank">{/if}
				    			Courier's Website
				    			{if $order.trackingWebsite|trim && $order.trackingWebsite|trim neq "#"}</a>{/if}.
			    		</div>
				    {else if $order.showTrackingStatus eq 'purged'}
				    	<div class="gray">
				        	Tracking Details for Orders Delivered more than 30 days ago are not available.
			        	</div>	        	
    {else}
					    <div class="body-txt">Your shipment details haven't yet been entered into the order tracking system.<br />
				    		Please check back within 24 hours to view the tracking details for this shipment.
			    		</div>
    {/if}
		    	</div>
		    {else}
		    	<div class="details gray">We will update the tracking details for {if $order.itemCount eq 1}this item{else}these items{/if} over here once {if $order.itemCount eq 1}it is{else}they are{/if} shipped.</div>
		    {/if}
	    </div>
    {/foreach}
    <div class="footer">Need help? {$customerSupportTime} at {$customerSupportCall}</div>
</div>
