{extends file="layout.tpl"}

{block name=body}

	<div class="mk-one-column">
		<section class="mk-site-main">
			<div class="mk-static-pages">
				<h1 id="top"> Myntra <span class="sep">/</span> About </h1>
				<ul class="mk-mynt-nav">
					<li><a href class="active">About</a></li>
					<li><a href="careers">Careers</a></li>
					<li><a href="contactus">Contact</a></li>
				</ul>
				<hr />
				<div class="mk-mynt-content mk-cf">
					<div class="mk-centre-column">
						<h2>ABOUT MYNTRA.COM</h2>
						<p>Myntra.com is a one stop shop for all your fashion and lifestyle needs. Being India's largest e-commerce store for fashion and lifestyle products, Myntra.com aims at providing a hassle free and enjoyable shopping experience to shoppers across the country with the widest range of brands and products on its portal. The brand is making a conscious effort to bring the power of fashion to shoppers with an array of the latest and trendiest products available in the country.</p>
						
						<p><strong>Value proposition</strong></p>
						<p>Myntra.com's value proposition revolves around giving consumers the power and ease of purchasing fashion and lifestyle products online. Offerings such as the largest in-season product catalogue, 100% authentic products, cash on delivery, EMI facility and 30 day return policy make Myntra.com, the preferred shopping destination in the country. To make online shopping easier for you, a dedicated customer connect team is on standby to answer your queries 24x7.</p>

						<p><strong>Brands</strong></p>
						<p>Myntra.com understands its shoppers' needs and caters to them with choice of apparel, accessories, cosmetics and footwear from over 500 leading Indian and international brands. Prominent brands include Adidas, Nike, Puma, Catwalk, Inc 5, United Colors of Benetton, FCUK, Timberland, Avirate,  FabIndia and Biba to name a few. You can also shop from some recently introduced labels such as - Roadster, Sher Singh, Dressberry, Kook N Keech and ETC.</p>

						<p><strong>Recognitions</strong></p>
						<ul>
							<li>Awarded <strong>'Fashion eRetailer of the Year 2013'</strong> by Franchise India – Indian eRetail Awards</li>
							<li>Awarded <strong>'Best E-commerce Website for 2012'</strong> by IAMAI – India Digital Awards</li>
							<li>Awarded <strong>'Images Most Admired Retailer of the Year: Non–Store Retail'</strong> for 2012 by Images Group</li>
							<li>Awarded <strong>'Best E-commerce Partner of the year 2011-12'</strong> by Puma India</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
	</div>

{/block}
