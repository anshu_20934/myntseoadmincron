{extends file="layout.tpl"}

{block name=body}

	<div class="mk-one-column">
		<section class="mk-site-main">
			<div class="mk-static-pages">
				<h1 id="top"> Myntra <span class="sep">/</span> Contact </h1>
				<ul class="mk-mynt-nav">
					<li><a href="aboutus">About</a></li>
					<li><a href="careers">Careers</a></li>
					<li><a href class="active">Contact</a></li>
				</ul>
				<hr />
				<div class="mk-mynt-content mk-cf">
					<div class="mk-left-column">
						<h2>How Can We Help You?</h2>
						<p>For order-related enquiries, email us at <a href="mailto:support@myntra.com">support@myntra.com</a> with your order number and contact details in the Subject Line and we will respond within 3 working days</p>
						<p>If you have already done so and are not satisfied with the response from Customer Connect, please email us at <a href="mailto:cclead@myntra.com" class="no-decoration-link">cclead@myntra.com</a> with your Ticket Number</p>
						<p>In case your concern still remains unresolved, please drop an email at <a href="mailto:ccmanager@myntra.com" class="no-decoration-link">ccmanager@myntra.com</a></p>
						<p>Or you can dial our <strong>24/7 Helpline number: {if $customerSupportCall}{$customerSupportCall}{/if}</strong></p>
						<strong>Other Enquiries</strong>
						<ul>
							<li>Corporate Sales: <a href="mailto:sales@myntra.com">sales@myntra.com</a></li>
							<li>Business Development and Partnerships: <a href="mailto:partners@myntra.com">partners@myntra.com</a></li>
							<li>Press Relations: <a href="mailto:pr@myntra.com">pr@myntra.com</a></li>
						</ul>
						<p><strong>Returns Processing Facility</strong> (to be used when shipping back returns)<br />
						ATTN: The Logistics Manager<br />
						The Returns Desk<br />
						Vector E-Commerce Pvt Ltd.<br />

						No 36/5, 27th Main,<br />
						HSR layout, Sector 2,<br />
						Somasandra Playa,<br />
						Near KEB Power station,<br />
						Haralukunte Post,<br />
						Bangalore 560102 </p>
						<p><strong>Bangalore Office</strong><br />
						Myntra.com<br />
						Maruthi Chamber,<br />
						Annex Building,Rupena Agrahara,<br />
						Hosur Road,<br />
						Bangalore - 560068<br />
						Phone: 080-67996666<br />
						<!--a href="http://cdn.myntra.com/skin1/pdf/Myntra_Route_Map.pdf" target="_blank"> <img src="http://cdn.myntra.com/skin1/myntra_images/pdf_22_22.png" alt="route map" />Download Route Map</a--></p>
					</div>
					<div class="mk-right-column fixed">
						<h5>We Are Only A Phone Call Away</h5>
						<h2>{if $customerSupportCall}{$customerSupportCall}{/if}</h2>
						<h5>24 Hours a Day / 7 Days a Week</h5>
						<h5>Or Drop us a Mail at:</h5>
						<h3><a href="mailto:support@myntra.com">support@myntra.com</a></h3>
					</div>
				</div>
			</div>
		</section>
	</div>

{/block}
