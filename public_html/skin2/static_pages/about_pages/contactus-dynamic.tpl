{extends file="layout.tpl"}

{block name=body}
{if !$telesales}
	<div class="mk-one-column">
		<section class="mk-site-main">
			<div class="mk-static-pages">
				<h1> Myntra <span class="sep">/</span> Contact </h1>
				<ul class="mk-mynt-nav">
					<li><a href="aboutus">About</a></li>
					<li><a href="careers">Careers</a></li>
					<li><a href class="active">Contact</a></li>
				</ul>
				<hr />
				<div class="mk-mynt-content mk-cf">
					<div class="mk-centre-column">
					
					    <!--contactus block-->
					    <div class="contactus-block">
					        {if $error_message}<div class='contactus-error-message err'><span class='error-icon'></span>&nbsp;{$error_message}</div><br/><br/>{/if}
                            {if $message}<div class='contactus-success-message success'><span class='icon'></span>&nbsp;{$message}</div><br/><br/>{/if}

                            <h3><a href="mymyntra.php?view=myorders" style="color: grey" onClick="_gaq.push(['_trackEvent', 'ContactUsPage', 'myorders', 'myorders']);return true;">CLICK HERE</a> TO TRACK, RETURN OR CANCEL YOUR ORDERS</h3>
					        <h3>OR</h3>
					        <h3>SMS your Order ID to 9227507512 to get an instantaneous update on your Order status, E.g. Myntra &lt;Order ID&gt;</h3>
					        <br/><br/>
                            <h6>To get Personalised Style advice and Makeovers contact our MyStylist helpline:</br>080-43541999 and press 4</h6>
                            <br/><br/>
					        <h6>Contact us using the form below. We will respond within 24 hours</h6>					        

					        {if $login eq '' && $login_condition eq 'true'}
                                <h4>Please login to submit your query</h4>
					            <h4><div class="btn normal-btn small-btn add-new-address btn-login">Login</div></h4>			             
					            <div class="form-shim"></div>
                            {/if}

                                <form action="{$http_location}/contactus" method="post" enctype="multipart/form-data" name="contactus_form" >
                                    <input type="hidden" name="timestamp" value="{$time}">
                                    <input type="hidden" name="_token" value="{$USER_TOKEN}"/>

                                    <div class="err"></div>

                                    <label>How can we help you? *</label>
                                    <div id="contactus-issue-level_1" class="contactus-issue-levels mk-custom-drop-down"></div>
                                    <div id="contactus-issue-level_2" class="contactus-issue-levels mk-custom-drop-down"></div>
                                    <div id="contactus-issue-level_3" class="contactus-issue-levels mk-custom-drop-down"></div>

                                    <div id="contactus-issue-fields"></div>

                                    <label>Tell Us More About Your Query</label>
                                    <textarea name="contactus_detail">{if $error_message}{$smarty.post.contactus_detail}{/if}</textarea>
                                    <div class="err"></div>

                                    <label class="image-suggestion-label">Attach Screenshot/Image</label>
                                    <span class="image-suggestion">(Images Only - png, jpg, gif. At max 4 images, total size 2MB.)</span>
                                    <input type="file" name="contactus_file[]" id="contactus_file">
                                    <br/>

                                    <label>Authenticate Yourself</label>
                                    <img class="captcha-image" src="{$http_location}/captcha/captcha.php?id=contactUsPage&rand={$random}" id="captcha" />
                                    <a class="captcha-change-text" href="javascript:void(0)" onclick="document.getElementById('captcha').src='{$http_location}/captcha/captcha.php?id=contactUsPage&rand='+Math.random();document.getElementById('captcha_text').focus();" id="change-image" >Change text</a>
                                    <div style="clear:both"></div>

                                    <label>Type the text you see in the image above</label>
                                    <input class="field" type="text" name="captcha_text" id="captcha_text"/>
                                    <div class="err"></div>
                                </form>

                                <div class="button-div">
                                    <div class="btn primary-btn" id="contactus_submit">Submit</div>                                                                        
                                </div>                       

                            <div class="mail-div">
                                <p style="width:25%">
                                    <span>CORPORATE SALES</span><br/>
                                    <a href="mailto:sales@myntra.com">sales@myntra.com</a>
                                </p>
                                <p style="width:40%">
                                    <span>BUSINESS DEVELOPMENT & PARTNERSHIPS</span><br/>
                                    <a href="mailto:partners@myntra.com">partners@myntra.com</a>
                                </p>
                                <p style="width:25%">
                                    <span>PRESS RELATIONS</span><br/>
                                    <a href="mailto:pr@myntra.com">pr@myntra.com</a>
                                </p>
                            </div>

                            {if $customerSupportCall}
                            <div class="callus-div">
                                <h5>We Are Only A Phone Call Away</h5>
                                <h3>Call Us at {$customerSupportCall}</h3>
                                <h5>24 Hours a Day</h5>
                                <h5>7 Days a Week</h5>
                            </div>
                            <hr>
                            {/if}                            

                            <div class="address-div">
                                <p>
                                    <span>Returns Processing Facility</span><br />
                                    ATTN: The Logistics Manager<br />
                                    The Returns Desk<br />
                                    Vector E-Commerce Pvt Ltd.<br />
                                    No 36/5, 27th Main,<br />
                                    HSR layout, Sector 2,<br />
                                    Somasandra Playa,<br />
                                    Near KEB Power station,<br />
                                    Haralukunte Post,<br />
                                    Bangalore 560102 
                                 </p>
                                <hr>
                                <p>                                    
                                    <span>Bangalore Office</span><br />
                                    Myntra.com<br />
                                    Maruthi Chamber,<br />
                                    Annex Building,Rupena Agrahara,<br />
                                    Hosur Road,<br />
                                    Bangalore - 560068<br />
                                    Phone: 080-67996666<br />
            						<!--a href="http://cdn.myntra.com/skin1/pdf/Myntra_Route_Map.pdf" target="_blank"> <img src="http://cdn.myntra.com/skin1/myntra_images/pdf_22_22.png" alt="route map" />Download Route Map</a-->
                                </p>
                            </div>
					    </div>
					    <!--contactus block-->
					    
					</div>					
				</div>
			</div>
		</section>
	</div>
       {else}
     Access Not Allowed

{/if}

{/block}

{block name=lightboxes}

    {if $login ne ''}
    
        <div id="contactus-order-overlay" class="lightbox">
            <div class="mod">
                <div class="hd">
                    <h2 class="title">Select an Order</h2>
                </div>
                <div class="bd">
                    <ul class="mk-mynt-nav">
                        <li><a href="javascript:void(0);" id="order-being-processed">Being processed</a></li>
                        <li><a href="javascript:void(0);" id="order-completed">Completed</a></li>
                    </ul>

                    <div class="order-list">
                        <table border="0" cellspacing="0" class="order-list-being-processed">
                            <col width="125">
                            <col width="150">
                            <col width="120">
                            <col width="160">
                            <tr class="order-list-table-head">
                                <th>Order No.</th>
                                <th>Date Placed </th>
                                <th>Total</th>
                                <th>Delivered / Items</th>
                            </tr>
                            {foreach key=grouporderid name="ordersLoop" item=ordermap from=$orders}
                            {if $ordermap.status eq 'PENDING'}
                            <tr class="{if $smarty.foreach.ordersLoop.index % 2 == 0}order-list-even{else}order-list-odd{/if}">
                                <td>
                                    <input type="radio" name="orderid">
                                    <div class="order-id-overlay">{$grouporderid}</div>
                                    <div class="order-list-tip mk-hide">
                                        <h4>Items in this order :</h4>
                                        <ul>
                                            {foreach from=$ordermap.titles item=title name=title}
                                            <li{if $smarty.foreach.title.last} class="last"{/if}>{$title}</li>
                                            {/foreach}
                                        </ul>
                                        {if $ordermap.shippeddate}
                                            <h4>Order Shipped On : {$ordermap.shippeddate}</h4>
                                        {elseif $ordermap.queueddate}
                                            <h4>Order Processed On : {$ordermap.queueddate}</h4>
                                        {/if}                                        
                                        <div class="order-list-tip-arrow"><img src="{$cdn_base}/skin1/images/border-tooltip.png"></div>
                                    </div>
                                </td>
                                <td>{$ordermap.orders[0].date|date_format:"%d %b, %Y"}</td>
                                <td><span class="rupees">{$rupeesymbol} {$ordermap.total}</span></td>
                                <td>{$ordermap.delivered} / {$ordermap.itemsCount}</td>
                            </tr>
                            {/if}
                            {foreachelse}
                                <tr class="{if $smarty.foreach.ordersLoop.index % 2 == 0}order-list-even{else}order-list-odd{/if}">
                                    <td>There are no pending orders to display</td>
                                </tr>
                            {/foreach}
                        </table>

                        <table border="0" cellspacing="0" class="order-list-completed">
                            <col width="125">
                            <col width="150">
                            <col width="120">
                            <col width="160">
                            <tr class="order-list-table-head">
                                <th>Order No.</th>
                                <th>Date Placed </th>
                                <th>Total</th>
                                <th>Items</th>
                            </tr>
                            {foreach key=grouporderid name="ordersLoop2" item=ordermap from=$orders}
                            {assign var=baseorder value=$ordermap.orders[0]}
                            {if $ordermap.status eq 'COMPLETED'}
                            <tr class="{if $smarty.foreach.ordersLoop2.index % 2 == 0}order-list-even{else}order-list-odd{/if}">
                                <td>
                                    <input type="radio" name="orderid">
                                    <div class="order-id-overlay">{$grouporderid}</div>
                                    <div class="order-list-tip mk-hide">
                                        <h4>Items in this order :</h4>
                                        <ul>
                                            {foreach from=$ordermap.titles item=title name=title}
                                            <li{if $smarty.foreach.title.last} class="last"{/if}>{$title}</li>
                                            {/foreach}
                                        </ul>
                                        {if $ordermap.delivereddate}
                                            <h4>{$ordermap.delivereddate}</h4>
                                        {/if}                                        
                                        <div class="order-list-tip-arrow"><img src="{$cdn_base}/skin1/images/border-tooltip.png"></div>
                                    </div>
                                </td>
                                <td>{$ordermap.orders[0].date|date_format:"%d %b, %Y"}</td>
                                <td><span class="rupees">{$rupeesymbol} {$ordermap.total}</span></td>
                                <td>{$ordermap.itemsCount}</td>
                            </tr>
                            {/if}
                            {foreachelse}
                                <tr class="{if $smarty.foreach.ordersLoop2.index % 2 == 0}order-list-even{else}order-list-odd{/if}">
                                    <td>There are no completed orders to display</td>
                                </tr>
                            {/foreach}
                        </table>
                    </div>
                </div>

                <div class="ft">
                    {if $customerSupportCall}<span class="note">Need Help? Call Us On {$customerSupportCall}</span>{/if}
                </div>
            </div>
        </div>

    {else}

        <div id="contactus-order-overlay" class="lightbox">
            <div class="mod">
                <div class="hd">
                    <h2 class="title">Login/Sign Up</h2>
                </div>
                <div class="bd">
                    <p>Please login by clicking following link to select an order</p>
                    <p class="mk-welcome link-btn btn-login right" style="text-align:center;">Login / Sign Up</p>
                </div>
            </div>
        </div>
    {/if}
    
{/block}

{block name=pagejs}
{literal}
    <script type="text/javascript">
        // contactus issue data
	    var issueDetail = JSON.parse('{/literal}{$issue_detail}{literal}');

	    // feature gate value for login customer or not
	    var FGloginCondition = '{/literal}{$login_condition}{literal}';	    	    
	</script>
{/literal}
{/block}
