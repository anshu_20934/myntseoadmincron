{extends file="layout.tpl"}

{block name=body}

{literal}
<style type="text/css">
html {
    text-transform: none;
}
b {
    font-weight: bold;
}
.policy_desc {
	font-family:Arial, Helvetica, sans-serif;
	font-size:14px;
	color:#ffffff;
}

#mobile .coupon,#desktop .coupon {
	font-family:Arial, Helvetica, sans-serif;
	font-size:18px;
	color:#000000;
	border:1px dotted #000000;
	background:#f0dc87;
	}
	
.coupon_desc {
	font-family:Arial, Helvetica, sans-serif;
	font-size:13px;
	color:#000000;
	border-bottom:1px dotted #cccccc;
}

</style>
{/literal}

<body>

<table id="desktop" width="775" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr><td>
        
        <table width="775" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr><td align="center"><table width="480" border="0" cellspacing="0" cellpadding="0" align="center"><tr><td height="40" align="center" class="coupon">LEVEL 5 TREASURE HUNT CODE: <b>IKNOWMYNTRA</b></td></tr></table></td></tr>
            <tr><td height="40" align="center" class="coupon_desc"><b>Go back &amp; enter the above code</b> to clear Level 5 in the Treasure Hunt</td></tr>
            <tr><td height="20" style="font-size:10px; ">&nbsp;</td></tr>
            <tr><td align="center"><img src="http://myntramailer.s3.amazonaws.com/march2014/1395668756treasure-hunt.jpg" alt="TREASURE HUNT" title="TREASURE HUNT" style="border: none;display:block; color:#2295b2; font-size:30px;"/></td></tr>
        </table>
        
        <table width="775" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
            	<td align="center"><img src="http://myntramailer.s3.amazonaws.com/march2014/139566869501.jpg" alt="" title="" style="border: none;display:block; color:#2295b2; font-size:15px;"/></td>
              	<td valign="top" bgcolor="#77b1a6">
                
               		<table border="0" cellspacing="0" cellpadding="0" align="center">
                        <tr><td align="center"><a href="http://www.myntra.com/faqs#returns" target="_blank"><img src="http://myntramailer.s3.amazonaws.com/march2014/1395668755return.jpg" alt="30 DAY RETURN" title="30 DAY RETURN" style="border: none;display:block; color:#ffffff; font-size:20px;"/></a></td></tr>
                        <tr><td height="100" align="center"  class="policy_desc">Myntra.com's Return and Exchange Policy offers you the option to return or exchange items within 30 days of the receipt.</td></tr>
                    </table>
                
              	</td>
                <td align="center"><img src="http://myntramailer.s3.amazonaws.com/march2014/139566869602.jpg" alt="" title="" style="border: none;display:block; color:#2295b2; font-size:15px;"/></td>
                <td valign="top" bgcolor="#293643">
                
               		<table border="0" cellspacing="0" cellpadding="0" align="center">
                        <tr><td align="center"><a href="http://www.myntra.com/faqs#payments" target="_blank"><img src="http://myntramailer.s3.amazonaws.com/march2014/1395668753cod.jpg" alt="COD" title="COD" style="border: none;display:block; color:#ffffff; font-size:20px;"/></a></td></tr>
                        <tr><td height="136" align="center" class="policy_desc">Pay for your order with cash at the time of delivery.<br/><br/>You can place a COD order for purchases up to<br/>Rs. 20,000</td></tr>
                    </table>
                
              	</td>
                <td align="center"><img src="http://myntramailer.s3.amazonaws.com/march2014/139566869603.jpg" alt="" title="" style="border: none;display:block; color:#2295b2; font-size:15px;"/></td>
            </tr>
        </table>
        
        <table width="775" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
            	<td align="center"><img src="http://myntramailer.s3.amazonaws.com/march2014/139566869704.jpg" alt="" title="" style="border: none;display:block; color:#2295b2; font-size:15px;"/></td>
              	<td valign="top" bgcolor="#af131f">
                
               		<table border="0" cellspacing="0" cellpadding="0" align="center">
                        <tr><td align="center"><a href="http://www.myntra.com/mymyntra.php?view=myorders" target="_blank"><img src="http://myntramailer.s3.amazonaws.com/march2014/1395668755track.jpg" alt="TRACK ORDER" title="TRACK ORDER" style="border: none;display:block; color:#ffffff; font-size:20px;"/></a></td></tr>
                        <tr><td height="107" align="center" class="policy_desc">check the status of your order at any time here. It may take up to 24 hours for the order status to be updated.</td></tr>
                    </table>
                
              	</td>
                <td align="center"><img src="http://myntramailer.s3.amazonaws.com/march2014/139566869805.jpg" alt="" title="" style="border: none;display:block; color:#2295b2; font-size:15px;"/></td>
            </tr>
        </table>
        
        <table width="775" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
            	<td align="center"><img src="http://myntramailer.s3.amazonaws.com/march2014/139566869806.jpg" alt="" title="" style="border: none;display:block; color:#2295b2; font-size:15px;"/></td>
              	<td valign="top" bgcolor="#c7aa25">
                
               		<table border="0" cellspacing="0" cellpadding="0" align="center">
                        <tr><td align="center"><a href="http://www.myntra.com/giftcards" target="_blank"><img src="http://myntramailer.s3.amazonaws.com/march2014/1395668753gift.jpg" alt="GIFT CARDS" title="GIFT CARDS" style="border: none;display:block; color:#ffffff; font-size:20px;"/></a></td></tr>
                        <tr><td height="122" align="center"  class="policy_desc">Myntra gift cards ware available from value Rs. 500 to Rs. 10000 which is emailed to the recipient instantly.</td></tr>
                    </table>
                
              	</td>
                <td align="center"><img src="http://myntramailer.s3.amazonaws.com/march2014/139566869907.jpg" alt="" title="" style="border: none;display:block; color:#2295b2; font-size:15px;"/></td>
                <td valign="top" bgcolor="#000000">
                
               		<table border="0" cellspacing="0" cellpadding="0" align="center">
                        <tr><td align="center"><a href="http://www.myntra.com/myprivilege" target="_blank"><img src="http://myntramailer.s3.amazonaws.com/march2014/1395668754privilege.jpg" alt="MY PRIVILEGE" title="MY PRIVILEGE" style="border: none;display:block; color:#ffffff; font-size:20px;"/></a></td></tr>
                        <tr><td height="117" align="center" class="policy_desc">Members for the<br/>My Privilege loyalty program earn points with every purchase.<br/>The points can be redeemed on future transactions.</td></tr>
                    </table>
                
              	</td>
                <td align="center"><img src="http://myntramailer.s3.amazonaws.com/march2014/139566869908.jpg" alt="" title="" style="border: none;display:block; color:#2295b2; font-size:15px;"/></td>
            </tr>
        </table>
        
        <table width="775" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr><td align="center"><img src="http://myntramailer.s3.amazonaws.com/march2014/139566870009.jpg" alt="" title="" style="border: none;display:block; color:#2295b2; font-size:15px;"/></td></tr>
        </table>
        
   	</td></tr>
</table>	
{/block}
