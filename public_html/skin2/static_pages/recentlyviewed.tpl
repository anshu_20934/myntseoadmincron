{extends file="layout.tpl"}

{block name=body}
<script>
Myntra.Data.pageName="recently_viewed_page";
</script>
	{assign var=ga_component_name value="recent_viewed_page"}
	<div class="mk-one-column mk-site-main mk-recently-viewed mk-static-pages">
		<h1>Recently Viewed</h1>
		<div class="mk-recent-viewed mk-carousel-five mk-carousel-four"></div>
	</div>
{/block}
