{extends file="layout.tpl"}

{block name=body}
{* Followin template given by marketing. Just putting it as it is *}
{literal}
<style>
html {
    text-transform: none;
}
b {
    font-weight: bold;
}
td.big {
    padding: 7px;
}
/* - - - - - HOTMAIL FIXES*/
.ReadMsgBody {
width: 100%;
}.
ExternalClasas {
width: 100%;
}

.policy_desc {
    font-family:Arial, Helvetica, sans-serif;
    font-size:14px;
    color:#ffffff;
}

#mobile .coupon,#desktop .coupon {
    font-family:Arial, Helvetica, sans-serif;
    font-size:18px;
    color:#000000;
    border:1px dotted #ff0000;
    background:#f0dc87;
    }

.coupon_desc {
    font-family:Arial, Helvetica, sans-serif;
    font-size:13px;
    color:#ffffff;
}

@media screen and (max-width: 540px), screen and (max-device-width: 540px) {
body { -webkit-text-size-adjust: none;}
div[id=desktop] {
display:none !important;
width:0px !important;
overflow:hidden !important;
}
div[id=mobile] {
display:block !important;
width:100% !important;
height:auto !important;
max-height:inherit !important;
overflow:visible !important;
 } 
table[id=body]{
width:100% !important; display:block !important;
}

.accord-header{cursor:pointer; font-weight:bold; padding-bottom:10px;}
.accord-content {display: none; padding-bottom:15px; padding-left:20px; }

.bold{
    font-size:18px;
    font-weight:bold;
    margin:0px; 
    }
    
.main-head{
    font-size:26px;
    font-weight:bold;
    }

.steps {
    -webkit-border-radius: 8px;
    -moz-border-radius: 8px;
    border-radius: 8px;
    background-color:#ffffff;
    padding:15px 20px;
}
.circle {
    -webkit-border-radius: 17px;
    -moz-border-radius: 8px;
    border-radius: 35px;
    background-color:#333333;
    width:35px;
    height:35px;
    color:#cccccc;
    text-align:center;
    font-size:15px;
    font-weight:bold;
    line-height:35px;
}


</style>

{/literal}

<script type="text/javascript" >

function init() {
document.getElementById('expandablePanel-term').style.display = 'none';
}

function toggleDiv(divid,img,u,d) {
var obj=document.getElementById(divid),img=document.getElementById(img);
if (obj){
obj.style.display=obj.style.display!='none'?'none':'block';
if (img){
img.src=obj.style.display=='none'?u:d;
}
}
}

</script>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $(".accordion .accord-header").click(function() {
      if($(this).next("div").is(":visible")){
        $(this).next("div").slideUp("slow");
      } else {
        $(".accordion .accord-content").slideUp("slow");
      }
    });
  });
</script>

</head>

<body onLoad="init();">


<!-- Mobile Version -->
<div id="mobile" style="width: 0px; display: none; max-height: 0px; overflow: hidden">
  <table width="540" height="65" cellspacing="0" cellpadding="0" border="0" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333;">
    <tr>
        <td><img src="http://myntramailer.s3.amazonaws.com/january2014/1389251682myprivilege-mob-head.jpg" alt="MYPrivilege" title="MYPrivilege" style="border:0px; display:block; font-family:arial,helvetica,serif;font-size:20px; color:#f00c6b;"></td>
    </tr>
    <tr>
        <td height="100" style="border-bottom:1px solid #cccccc;">
        We at <b>Myntra.com</b> are focused on providing you with a perfect customer experience. We now give you all the more reason to keep coming back to your favourite online shopping destination with our exclusive loyalty  program -  <br/><b>'My Privilege'</b>.
        </td>
    </tr>
    
    
    <tr><td height="25" style="font-size:5px;">&nbsp;</td></tr>
    
    
    <tr>
        <td>
          <table width="540" cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#eeeeee">
            <tr>
                <td>
                
                    <table width="450" cellspacing="0" cellpadding="0" border="0" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333;">
                        <tr><td height="10">&nbsp;</td></tr>
                        <tr><td align="center" class="main-head">HOW IT WORKS</td></tr>
                        <tr><td height="35" align="center" valign="top">Get started with 3 simple steps</td></tr>
                    </table>
                    
                  <table width="450" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tr><td class="steps">
                        
                        <table width="410" cellspacing="0" cellpadding="0" border="0" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333;">
                            <tr>
                                <td width="35"><div class="circle">1</div></td>
                                <td width="35">&nbsp;</td>
                                <td width="75"><img src="http://myntramailer.s3.amazonaws.com/october2013/1382103061shop.png" alt="" title="" style="border:0px; display:block; font-family:arial,helvetica,serif;font-size:20px; color:#e80084;"></td>
                                <td><span class="bold">SHOP</span><br/>on myntra.com</td>
                            </tr>
                        </table>
                            
                        </td></tr>
                        <tr><td height="5" style="font-size:5px;">&nbsp;</td></tr>
                        <tr><td class="steps">
                        
                        <table width="410" cellspacing="0" cellpadding="0" border="0" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333;">
                            <tr>
                                <td width="35"><div class="circle">2</div></td>
                                <td width="35">&nbsp;</td>
                                <td width="75"><img src="http://myntramailer.s3.amazonaws.com/october2013/1382351105earn.png" alt="" title="" style="border:0px; display:block; font-family:arial,helvetica,serif;font-size:20px; color:#e80084;"></td>
                                <td class="bold">EARN POINTS</td>
                            </tr>
                        </table>
                            
                        </td></tr>
                        <tr><td height="5" style="font-size:5px;">&nbsp;</td></tr>
                        <tr><td class="steps">
                        
                        <table width="410" cellspacing="0" cellpadding="0" border="0" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333;">
                            <tr>
                                <td width="35"><div class="circle">3</div></td>
                                <td width="35">&nbsp;</td>
                                <td width="75"><img src="http://myntramailer.s3.amazonaws.com/october2013/1382092473myntra.png" alt="" title="" style="border:0px; display:block; font-family:arial,helvetica,serif;font-size:20px; color:#e80084;"></td>
                                <td><span class="bold">USE POINTS TO SHOP MORE</span><br/>1 point = Rs 0.50</td>
                            </tr>
                        </table>
                            
                        </td></tr>
                        <tr><td height="30" style="font-size:5px;">&nbsp;</td></tr>
                  </table>
                
                </td>
            </tr>
            </table>
        </td>
    </tr>
    <tr><td height="70" align="center" class="main-head">HOW TO EARN POINTS</td></tr>
    <tr><td>
    
    <table width="540" cellspacing="15" cellpadding="0" border="0" align="center" style="border:1px solid #cccccc;">
        <tr><td height="25"><b>What should I do?</b> : SHOP</td></tr>
        <tr><td height="25"><b>Benefits</b> : EARN POINTS AS PER YOUR MEMBERSHIP LEVEL</td></tr>
    </table>
    
    </td></tr>
    
    <tr><td>
    
    <table width="540" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td width="175" valign="top">
        
            <table width="175" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr><td align="center" height="140"><img src="http://myntramailer.s3.amazonaws.com/october2013/1381500372SILVER.jpg" width="100" alt="" title="" style="border:0px; display:block; font-family:arial,helvetica,serif;font-size:20px; color:#e80084;"></td></tr>
                <tr><td align="center" valign="top"><b>6 points</b><br/>for every Rs. 100<br/>worth of purchase</td></tr>
            </table>
        
        </td>
        <td width="20">&nbsp;</td>
        <td width="175" valign="top">
        
            <table width="175" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr><td align="center" height="140"><img src="http://myntramailer.s3.amazonaws.com/october2013/1381500372GOLD.jpg" width="100" alt="" title="" style="border:0px; display:block; font-family:arial,helvetica,serif;font-size:20px; color:#e80084;"></td></tr>
                <tr><td align="center" valign="top"><b>9 points</b><br/>for every Rs. 100<br/>worth of purchase</td></tr>
            </table>
        
        </td>
        <td width="20">&nbsp;</td>
        <td width="175" valign="top">
        
        
            <table width="175" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr><td align="center" height="140"><img src="http://myntramailer.s3.amazonaws.com/october2013/1381500372PLATINUM.jpg" width="100" alt="" title="" style="border:0px; display:block; font-family:arial,helvetica,serif;font-size:20px; color:#e80084;"></td></tr>
                <tr><td height="65" align="center" valign="top"><b>12 points</b><br/>for every Rs. 100<br/>worth of purchase.</td></tr>
                <tr><td height="25" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333; border-top:1px solid #cccccc;"><b>Free Shipping on all orders</b></td></tr>
            </table>
            
        
        </td>
    </tr>
    </table>
    
    </td></tr>
    
    <tr><td height="15" style="font-size:5px;border-top:1px solid #cccccc;">&nbsp;</td></tr>
                
                <tr><td height="70" align="center" style="font-size:18px;">
                    <b>Keep shopping and be rewarded for it!<br/>It does not get better than that, does it?</b>
                </td></tr>
                <tr><td height="25" align="center" style="font-size:15px;">
                    <a href="http://www.myntra.com"><img src="http://myntramailer.s3.amazonaws.com/october2013/1382103394start-shopping.png" alt="START SHOPPING NOW" title="START SHOPPING NOW" style="border:0px; display:block; font-family:arial,helvetica,serif;font-size:20px; color:#e4650d;"></a>
                </td></tr>
                <tr><td height="25" style="font-size:5px;">&nbsp;</td></tr>
    
    

</table>

</div>










<div id="desktop">
<table width="775" cellspacing="0" cellpadding="0" border="0" align="center">
    <tr>
      <td>
      
            
        <table width="775" height="65" cellspacing="0" cellpadding="0" border="0" align="center">
        <tr>
            <td><img src="http://myntramailer.s3.amazonaws.com/january2014/1389086621head.jpg" alt="MYPrivilege" title="MYPrivilege" style="border:0px; display:block; font-family:arial,helvetica,serif;font-size:20px; color:#f00c6b;"></td>
        </tr>
        </table>
            
            
            
            
            
        <table width="775" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333;">
                <tr><td height="75" align="center" style="border-bottom:1px solid #cccccc;">
                We at <b>Myntra.com</b> are focused on providing you with a perfect customer experience. We now give you all the more reason to keep coming back to your favourite online shopping destination with our exclusive loyalty  program -  <b>'My Privilege'</b>.
          </td></tr>
                
                <tr><td height="25" style="font-size:5px;">&nbsp;</td></tr>
                
                <tr><td align="center" style="font-size:24px; font-weight:bold;">HOW IT WORKS</td></tr>
                <tr><td height="50" align="center" valign="top">Get started with 3 simple steps</td></tr>
                
                
                <tr><td>
                
                    <table width="775" border="0" cellspacing="0" cellpadding="0" align="center">
                        <tr>
                            <td width="35">&nbsp;</td>
                            <td width="200" valign="top">
                            
                                <table width="200" border="0" cellspacing="0" cellpadding="0" align="center">
                                    <tr><td height="50" align="center"><img src="http://myntramailer.s3.amazonaws.com/october2013/1382103061shop.png" alt="" title="" style="border:0px; display:block; font-family:arial,helvetica,serif;font-size:20px; color:#e80084;"></td></tr>
                                    <tr><td height="30" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#333333;">SHOP</td></tr>
                                    <tr><td align="center" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333;">on <b> Myntra.com</b></td></tr>
                                </table>
                            
                            </td>
                            <td width="20" align="center"><img src="http://myntramailer.s3.amazonaws.com/october2013/1382092742right.png" alt="" title="" style="border:0px; display:block; font-family:arial,helvetica,serif;font-size:20px; color:#e80084;"></td>
                            <td width="200" valign="top">
                            
                                <table width="200" border="0" cellspacing="0" cellpadding="0" align="center">
                                    <tr><td height="50"  align="center"><img src="http://myntramailer.s3.amazonaws.com/october2013/1382351105earn.png" alt="" title="" style="border:0px; display:block; font-family:arial,helvetica,serif;font-size:20px; color:#e80084;"></td></tr>
                                    <tr><td height="30" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#333333;">EARN POINTS</td></tr>
                                    <tr><td align="center" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333;"></td></tr>
                                </table>
                            
                            </td>
                            <td width="20" align="center"><img src="http://myntramailer.s3.amazonaws.com/october2013/1382092742right.png" alt="" title="" style="border:0px; display:block; font-family:arial,helvetica,serif;font-size:20px; color:#e80084;"></td>
                            <td width="200" valign="top">
                            
                            
                                <table width="200" border="0" cellspacing="0" cellpadding="0" align="center">
                                    <tr><td height="50"  align="center"><img src="http://myntramailer.s3.amazonaws.com/october2013/1382092473myntra.png" alt="" title="" style="border:0px; display:block; font-family:arial,helvetica,serif;font-size:20px; color:#e80084;"></td></tr>
                                    <tr><td height="30" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#333333;">USE POINTS TO SHOP MORE</td></tr>
                                    <tr><td align="center" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333;"><b>1 point = Rs 0.50</b></td></tr>
                                </table>
                                
                            
                            </td>
                            <td width="35">&nbsp;</td>
                        </tr>
                    </table>
                
                </td></tr>
                
                
                <tr><td height="25" style="font-size:5px;">&nbsp;</td></tr>
                
                
          <tr><td height="70" align="center" style="font-size:24px; font-weight:bold; border-top:1px solid #cccccc;">HOW TO EARN POINTS</td></tr>
                <tr><td height="40" align="center" valign="top">
                
                    <table width="600" cellspacing="0" cellpadding="7" border="0" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; border:1px solid #cccccc; border-collapse:collapse;">
                        <tr>
                          <td class="big" width="200" style="border:1px solid #cccccc; border-right:none;"><b>What should I do ?</b></td>
                          <td class="big" align="right" style="border:1px solid #cccccc; border-left:none;"><b>Benefits</b></td>
                      </tr>
                      <tr>
                          <td class="big" style="border:1px solid #cccccc;border-right:none;">SHOP</td>
                          <td class="big" align="right" style="border:1px solid #cccccc; border-left:none;">EARN POINTS AS PER YOUR MEMBERSHIP LEVEL</td>
                      </tr>
                    </table>
                
                </td></tr>
                
                
                <tr><td height="25" style="font-size:5px;">&nbsp;</td></tr>
                
                
          <tr><td>
                
                    <table width="775" border="0" cellspacing="0" cellpadding="0" align="center">
                        <tr>
                            <td width="35">&nbsp;</td>
                            <td width="200" valign="top">
                            
                                <table width="200" border="0" cellspacing="0" cellpadding="0" align="center">
                                    <tr><td align="center" height="140"><img src="http://myntramailer.s3.amazonaws.com/october2013/1381500372SILVER.jpg" width="120" alt="" title="" style="border:0px; display:block; font-family:arial,helvetica,serif;font-size:20px; color:#e80084;"></td></tr>
                                    <tr><td align="center" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333;"><b>6 points</b> for every Rs. 100<br/>worth of purchase</td></tr>
                                </table>
                            
                            </td>
                            <td width="20">&nbsp;</td>
                            <td width="200" valign="top">
                            
                                <table width="200" border="0" cellspacing="0" cellpadding="0" align="center">
                                    <tr><td align="center" height="140"><img src="http://myntramailer.s3.amazonaws.com/october2013/1381500372GOLD.jpg" width="120" alt="" title="" style="border:0px; display:block; font-family:arial,helvetica,serif;font-size:20px; color:#e80084;"></td></tr>
                                    <tr><td align="center" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333;"><b>9 points</b> for every Rs. 100<br/>worth of purchase</td></tr>
                                </table>
                            
                            </td>
                            <td width="20">&nbsp;</td>
                            <td width="200" valign="top">
                            
                            
                                <table width="200" border="0" cellspacing="0" cellpadding="0" align="center">
                                    <tr><td align="center" height="140"><img src="http://myntramailer.s3.amazonaws.com/october2013/1381500372PLATINUM.jpg" width="120" alt="" title="" style="border:0px; display:block; font-family:arial,helvetica,serif;font-size:20px; color:#e80084;"></td></tr>
                                    <tr><td height="35" align="center" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333;"><b>12 points</b> for every Rs. 100<br/>worth of purchase.</td></tr>
                                    <tr><td height="25" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333; border-top:1px solid #cccccc;"><b>Free Shipping on all orders</b></td></tr>
                                </table>
                                
                            
                            </td>
                            <td width="35">&nbsp;</td>
                        </tr>
                    </table>
                
                </td></tr>
                
                
                <tr><td height="25" style="font-size:5px;">&nbsp;</td></tr>
                <tr><td height="15" style="font-size:5px;border-top:1px solid #cccccc;">&nbsp;</td></tr>
                
                <tr><td height="40" align="center" style="font-size:15px;">
                    <b>Keep shopping and be rewarded for it! It does not get better than that, does it?</b>
                </td></tr>
                <tr><td height="25" align="center" style="font-size:15px;">
                    <a href="http://www.myntra.com"><img src="http://myntramailer.s3.amazonaws.com/october2013/1382103394start-shopping.png" alt="START SHOPPING NOW" title="START SHOPPING NOW" style="border:0px; display:block; font-family:arial,helvetica,serif;font-size:20px; color:#e4650d;"></a>
                </td></tr>
                <tr><td height="25" style="font-size:5px;">&nbsp;</td></tr>
                </table>
            </td>
    </tr>
    </table>
    
</div>

<table width="775" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333;">
                <tr><td height="70" align="center" style="font-size:24px; font-weight:bold; border-top:1px solid #cccccc;">FAQs</td></tr>

                
                <tr><td>
                
                
                <div class="accordion">
                  <div class="accord-header">01. What is My Privilege?</div>
                  <div class="accord-content">My Privilege is the Loyalty Program offered by Myntra to its customers. Members of this program will earn points with every purchase. They can redeem the  points earned on future transactions at Myntra.com</div>
                  
                  <div class="accord-header">02. How do I become a member of My Privilege?</div>
                  <div class="accord-content">You can become a member of My Privilege by registering at Myntra.com. All existing customers are given a free membership.</div>
                  
                  <div class="accord-header">03. What are the benefits that I receive?</div>
                  <div class="accord-content">As a part of the My Privilege Program, you will stand a chance to  avail  numerous offers and privileges apart from points that can be used to buy products available  on Myntra. Every order will provide points  which can be used later for redemption.</div>
                  
                  <div class="accord-header">04. How much  is each point worth?</div>
                  <div class="accord-content">Each point is worth Rs. 0.50.</div>
                  
                  <div class="accord-header">05. I am a Silver/Gold/Platinum member. What does that mean?</div>
                  <div class="accord-content">
                  
                    On the basis of various levels of engagement that you'll have with Myntra, your status will be constantly evaluated and will be classified into one of the above-mentioned tiers. Please see the details below for more information on the categorisation.<br/><br/>
                    
                        
                      <table width="700" cellspacing="0" cellpadding="7" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; border:1px solid #cccccc; border-collapse:collapse;">
                          <tr>
                            <td bgcolor="#f2f2f2" style="border:1px solid #cccccc; ">Benefits</td>
                            <td bgcolor="#f2f2f2" style="border:1px solid #cccccc; ">Silver</td>
                            <td bgcolor="#f2f2f2" style="border:1px solid #cccccc; ">Gold</td>
                            <td bgcolor="#f2f2f2" style="border:1px solid #cccccc; ">Platinum</td>
                        </tr>
                        <tr>
                            <td bgcolor="#f2f2f2" style="border:1px solid #cccccc;">Reward Points</td>
                            <td style="border:1px solid #cccccc;">6 points for Rs. 100</td>
                            <td style="border:1px solid #cccccc;">9 points for Rs. 100</td>
                            <td style="border:1px solid #cccccc;">12 points for Rs. 100</td>
                        </tr>
                        <tr>
                            <td bgcolor="#f2f2f2" style="border:1px solid #cccccc;">Redemption</td>
                            <td style="border:1px solid #cccccc;">Minimum of 200 points required for redemption</td>
                            <td style="border:1px solid #cccccc;">Minimum of 200 points required for redemption</td>
                            <td style="border:1px solid #cccccc;">Minimum of 200 points required for redemption</td>
                        </tr>
                        <tr>
                            <td bgcolor="#f2f2f2" style="border:1px solid #cccccc;">Qualification Criteria</td>
                            <td style="border:1px solid #cccccc;">Default Membership</td>
                            <td style="border:1px solid #cccccc;">Shop for Rs. 3000-7999 or more in 6 months</td>
                            <td style="border:1px solid #cccccc;">Shop for Rs. 8000 or more in 6 months</td>
                        </tr>
                        <tr>
                            <td bgcolor="#f2f2f2" style="border:1px solid #cccccc;">Re-qualification Criteria</td>
                            <td style="border:1px solid #cccccc;">No-downgrade</td>
                            <td style="border:1px solid #cccccc;">Shop for min Rs.3000 in a year</td>
                            <td style="border:1px solid #cccccc;">Shop for min Rs.5000 in a year</td>
                        </tr>
                      </table>
                  
                  </div>
                  
                  <div class="accord-header">06. Is there an annual fee for this program?</div>
                  <div class="accord-content">There is no annual fee for the program.</div>
                  
                  <div class="accord-header">07. Can I leave My Privilege program?</div>
                  <div class="accord-content">No. All Myntra customers are a part of the program.</div>
                  
                  <div class="accord-header">08. How do I earn points?</div>
                  <div class="accord-content">As a member of the program, you earn points for every successful transaction that you make. Initially awarded points will be in a 'Pending' state, which will become 'Active' once you accept the order/ item. Accepting an item means that you have verified the product and agreed that you will not return the product.</div>
                  
                  <div class="accord-header">09. I am not able to  receive points on placing on order. Why?</div>
                  <div class="accord-content">If you haven't received any points for the order placed, please contact us at <a href="http://www.myntra.com/contactus">http://www.myntra.com/contactus</a> or call our customer care at 080-43541999. We will make sure that any discrepancy is resolved at the earliest.</div>
                  
                  <div class="accord-header">10. Why are points being shown in 'Pending' state?</div>
                  <div class="accord-content">The points will be activated only after you have successfully accepted the order and have not returned part or whole of the order in 30 days from the date of order delivery.</div>
                  
                  <div class="accord-header">11. How can I convert Pending points to Active points?</div>
                  <div class="accord-content">To activate, you have to accept the order and click the 'Get Points' link against the order id. for which the points are in the 'PENDING' state. Alternatively, the points will get activated in 30 days  from the date of delivery. Once you've claimed points you will not be able to return the product.</div>
                  
                  <div class="accord-header">12. Where and how can I redeem my points?</div>
                  <div class="accord-content">
                  
                    Once you have a minimum of 200 active points, you can redeem them on purchases at Myntra.com. Each point has a value of Rs. 0.50. Points can be redeemed at the checkout page by selecting the "Redeem Points" option there.<br/><br/>
                     Illustration:<br/>
                     You have 3000 active points  in your account. You are about to purchase items worth Rs. 1200. You can use 2400 points to make the purchase.<br/><br/>
                     You have 3000 active points in your account. You are about to purchase items worth Rs. 3000. You can use 3000 points to pay for Rs.1500. You may use any of the payment options to pay the remaining Rs.1500.
                  
                  </div>
                  
                  <div class="accord-header">13. I am unable to  redeem points. Why?</div>
                  <div class="accord-content">You need a minimum of 200 points to be able to redeem them. Please check your account to see if you have the required points. If you have more than 200 points and  are still not able to redeem the points, please contact the customer care.</div>
                  
                  <div class="accord-header">14. I have multiple accounts at Myntra. Can I club points in my multiple accounts?</div>
                  <div class="accord-content">No, you can't club points present in multiple accounts. To enjoy the full benefits of the program, we advise you to use a single account to make all your transactions.</div>
                  
                  <div class="accord-header">15. Will my points expire?</div>
                  <div class="accord-content">Yes. Points will expire in 12 calendar months since the month of their issue. So, all points issued in October, 2013 will expire on October, 2014</div>
                  
                  <div class="accord-header">16. My pending points got deducted. Why?</div>
                  <div class="accord-content">
                  
                    There are 2 cases in which pending points could be deducted<br/>
                        a. If you return the order or a part of it, points will get deducted accordingly<br/>
                        b. If you cancel the product or reject the order, the points issued for that order will be debited from your account.
                  
                  </div>
                  
                  <div class="accord-header">17. How can I get upgraded to a Gold/ Platinum membership?</div>
                  <div class="accord-content">
                  
                    If you haven't made any transactions with Myntra or have only made transactions worth less than Rs. 3000 in the last 6 months, you will be a Silver Member<br/><br/>
                    
                    <table width="700" cellspacing="0" cellpadding="7" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; border:1px solid #cccccc; border-collapse:collapse;">
                          <tr>
                            <td bgcolor="#f2f2f2" style="border:1px solid #cccccc; ">&nbsp;</td>
                            <td bgcolor="#f2f2f2" style="border:1px solid #cccccc; ">Silver</td>
                            <td bgcolor="#f2f2f2" style="border:1px solid #cccccc; ">Gold</td>
                            <td bgcolor="#f2f2f2" style="border:1px solid #cccccc; ">Platinum</td>
                        </tr>
                        <tr>
                            <td bgcolor="#f2f2f2" style="border:1px solid #cccccc;">Qualification Criteria</td>
                            <td style="border:1px solid #cccccc;">Default Membership</td>
                            <td style="border:1px solid #cccccc;">Shop for Rs. 3000-7999 or more in 6 months</td>
                            <td style="border:1px solid #cccccc;">Shop for Rs. 8000 or more in 6 months</td>
                        </tr>
                      </table>
                  
                  </div>
                  
                  <div class="accord-header">18. Will I get downgraded from my current membership? How can I avoid it?</div>
                  <div class="accord-content">
                  
                  If you successfully satisfy the following conditions depending on the membership level you are in, there will be no membership downgrade.<br/>
                    Eg: If you're a Gold Member, you need to shop for Rs. 3000 in a year. If you're a Platinum member in Oct, 2013, you need to make purchases worth Rs. 5000 in a year's period to remain in the same tier.<br/><br/>
                    
                    <table width="700" cellspacing="0" cellpadding="7" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; border:1px solid #cccccc; border-collapse:collapse;">
                          <tr>
                            <td bgcolor="#f2f2f2" style="border:1px solid #cccccc; ">&nbsp;</td>
                            <td bgcolor="#f2f2f2" style="border:1px solid #cccccc; ">Silver</td>
                            <td bgcolor="#f2f2f2" style="border:1px solid #cccccc; ">Gold</td>
                            <td bgcolor="#f2f2f2" style="border:1px solid #cccccc; ">Platinum</td>
                        </tr>
                        <tr>
                            <td bgcolor="#f2f2f2" style="border:1px solid #cccccc;">Re-qualification Criteria</td>
                            <td style="border:1px solid #cccccc;">Default Membership</td>
                            <td style="border:1px solid #cccccc;">Shop for min Rs.3000 in a year</td>
                            <td style="border:1px solid #cccccc;">Shop for min Rs.5000 in a year</td>
                        </tr>
                      </table>
                  
                  </div>
                  
                  <div class="accord-header">19. I have made all the required purchases, but I haven't been upgraded yet. Why?</div>
                  <div class="accord-content">
                  
                  You will be upgraded only if you have accepted all the orders that will enable the upgrade. You can check the status in the Loyalty Tab of your My Myntra Account.<br/>
                     If you have successfully met all the criteria and you're still facing the issue, kindly contact our customer care.<br/>
                  </div>
                  
                  <div class="accord-header">20. Can I purchase a gift-card using points?</div>
                  <div class="accord-content">No. We are not offering this feature at this point in time.</div>
                  
                  <div class="accord-header">21. Will I get points for the amount I have paid by redeeming points?</div>
                  <div class="accord-content">You will not receive any points for redeeming past points. However, if you've made only a part-payment using points, you'll get points for the payment made using other payment modes, including cash-back.</div>
                  
                  <div class="accord-header">22. Will I get points for buying a gift-card?</div>
                  <div class="accord-content">No. You will not get points for buying gift cards.</div>
                  
                  <div class="accord-header">23. Will I get points for shipping-charges and gift-packing charges?</div>
                  <div class="accord-content">No. You will not get points for shipping-charges and gift-packing charges.</div>
                  
                </div>
                
                
                
                
                
                </td></tr>
                
                <tr><td height="25" style="font-size:5px;">&nbsp;</td></tr>
                
                <tr><td height="70" align="center" style="font-size:24px; font-weight:bold; border-top:1px solid #cccccc;">Program Terms and Conditions</td></tr>

                <tr><td>
                
                <table width="775" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333;">
                        <tr><td height="25" style="font-size:5px;">&nbsp;</td></tr>
                
                
                        <tr><td>
                        <b>1.ELIGIBILITY, MEMBERSHIP, ACCRUAL</b><br/>
                        1.1. These terms and conditions are operational only in India and open to participation of all the registered members, resident of India of myntra.com, over and above the age of 18 years..<br/><br/>
                        1.2. The program may be extended to other geographies and other audiences at Myntra's discretion.<br/><br/>
                        1.3. Membership will be offered to the  My Privilege<br/><br/>
                        </td></tr>
                 </table>
                
                <div id="expandableArea-term">
                <div id="expandablePanel-term">
                    <table width="775" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333;">
                

                
                <tr><td>
                        1.4. New Customers will gain membership to the program on registration. All existing customers are given a free membership to the program.<br/><br/>
                        1.5. Membership will be identified by a single registered email id confirmed by the registered member at time of first login on the Myntra's webpage.<br/><br/>
                        1.6. Two email id's cannot be combined under a single membership.<br/><br/>
                        1.7. Membership will be activated for existing and new members/customers as follows:<br/>
                            i. For existing customers: Activated by default.<br/>
                            ii. For new customers/member - on registering with Myntra.com.<br/><br/>
                        
                        1.8.Membership is valid for life until such time as myntra.com decided to terminate this loyalty programme, with or without notice to its members. Accumulation of points shall cease immediately on termination of this programme, but points already accrued shall be redeemable for 60 days.<br/><br/>
                        1.9.Once membership is activated, the member starts earning points whenever they purchase products on Myntra.com.<br/><br/>
                        1.9.1. Points will be at a certain percent of the purchase value specified by Myntra.<br/><br/>
                        1.9.2. The percent may vary by product, brand etc at the discretion of Myntra.<br/><br/>
                        1.9.3. Myntra points are not legal tender, have no monetary value and cannot be bartered or sold for cash.<br/><br/>
                        1.9.4. Points can be redeemed for products on Myntra.com or any other products specified by Myntra at an exchange rate of 1 point - Rs. 0.50.<br/><br/>
                        1.9.5. All points have the same exchange value.<br/><br/>
                        1.9.6. This exchange rate can be changed from time to time at Myntra's discretion.<br/><br/>
                        1.9.7. Points will be valid for a 12 month period on a rolling month (end of month) basis.<br/><br/>
                        1.9.8. Points will be credited to the member's account immediately after the successful acceptance of the order.<br/><br/>
                        1.9.9. Fractional points will be rounded off to the next highest integer. Example: if total points is greater than 99, but less than 100, it shall be added as 100 points.<br/><br/><br/>
                
                <b>2.TIER SYSTEM FOR MEMBERS/CUSTOMERS</b><br/><br/>
                    Members/customers will be slotted into tiers based on total amount purchased on Myntra<br/><br/>
                    2.1.1. New customers will enter into the <b> Silver Membership(BASE TIER) </b> of the program.<br/><br/>
                    2.1.2. If a customer purchases worth Rs. 3000 (total net bill value after all discounts) in a six month period he/she shall be upgraded to the <b>Gold Membership (MIDDLE TIER)</b>.<br/><br/>
                    2.1.3. If a customer purchases worth Rs. 8000 in a six month period he/she will upgrade to the <b>Platinum Membership (TOP TIER)</b>.<br/><br/>
                    2.1.4. Tiers will be branded with names - and the thresholds may change from time to time at Myntra's discretion<br/><br/>
                    2.1.5. Existing customers as of (October 25 2013) may be invited to join the program directly into a tier based on the total net bill value of purchases in the six  months before program launch<br/><br/>
                    2.1.5.1. Rs. 3000 in 6 months - Gold Membership <br/><br/>
                    2.1.5.2. Rs. 8000 in 6 months - Platinum Membership<br/><br/>
                    2.1.6. Points earned increase with tier at a specified percent value.<br/><br/>
                    2.1.7. Once a tier is attained it can be retained for 12 months.<br/><br/>
                    2.1.8. At the end of 12 months, member tiers will be revalidated based on prevailing revalidation criteria.<br/><br/>
                    2.1.9. Members not fulfilling revalidation criteria will be downgraded to one tier lower than the current tier.<br/><br/>
                    2.1.10. Tier will be upgraded the moment the member attains a higher threshold based on 6 months purchase.<br/><br/>
                
                
                </td></tr>

                
                
                <tr><td height="25" style="font-size:5px;">&nbsp;</td></tr>
                
                
                <tr><td>
                
                <b>3.GENERAL TERMS AND CONDITIONS</b><br/><br/>
                    3.1. Each member is responsible for remaining knowledgeable about the Myntra Program Terms and Conditions and the number of points in his or her account.<br/><br/>
                    3.2. Myntra will send correspondence to active members to advise them of matters of interest, including notification of Myntra Program changes and Points Updates.<br/><br/>
                    3.3. Myntra will not be liable or responsible for correspondence lost or delayed in the mail/e-mail.<br/><br/>
                    3.4. Myntra reserves the right to refuse, amend, vary or cancel membership of any Member without assigning any reason and without prior notification.<br/><br/>
                    3.5. Any change in the name, address, or other information relating to the Member must be notified to Myntravia the Helpdesk/email by the Member, as soon as possible at <a href="http://www.myntra.com/contactus">http://www.myntra.com/contactus</a> or call at +91-80-43541999.<br/><br/>
                    3.6. Myntra reserves the right to add,modify,delete or otherwise change the Terms and Conditions without any approval, prior notice or reference to the Member.<br/><br/>
                    3.7. In the event of dispute in connection with Myntra Program and the interpretation of Terms and Conditions, Myntra's decision shall be final and binding.<br/><br/>
                    3.8. This Policy and these terms shall be read in conjunction with the standard legal policies of Myntra.com, including its Privacy policy.<br/><br/>
                
                
                </td></tr>
                    </table>
                </div>
            
            <div id="button_to_expand-term" style="padding-left:30px;">
            <a href="javascript:toggleDiv('expandablePanel-term','i2','http://myntramailer.s3.amazonaws.com/october2013/1382101559more.png','http://myntramailer.s3.amazonaws.com/october2013/1382101559less.png');" ><img id="i2" src = "http://myntramailer.s3.amazonaws.com/october2013/1382101559more.png"></a>
            </div>
            </div>
                
                
                
                
                </td></tr>
                
                
                

                
            </table>
{/block}
