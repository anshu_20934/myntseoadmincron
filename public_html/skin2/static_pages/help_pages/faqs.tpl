{extends file="layout.tpl"}

{block name=body}

	<div class="mk-one-column">
		<section class="mk-site-main">
			<div class="mk-static-pages mk-faqs">
				<h1 id="top"> Help <span class="sep">/</span> Policies & FAQs </h1>
				<ul class="mk-mynt-nav">
					<li><a href class="active">Policies & FAQs</a></li>
					<li><a href="privacypolicy">Privacy</a></li>
					<li><a href="termsofuse">Terms of Use</a></li>
				</ul>
				<hr />
				<div class="mk-mynt-content mk-cf">
					<div class="mk-left-column">

						<h3 id="website" class="no-border">Website</h3>
						<div>
							<p class="mk-collapse-control"><strong>How do I determine the correct size for me?</strong></p>
							<div class="mk-collapse-content">
								<p>You can view the sizing details for a product by clicking on the 'Not sure about your size?' link on the product page located above the 'Buy Now' button. This displays the size chart for the product and helps you decide which size fits you best.</p>
								<p>You can always return the product if the size you selected does not fit you properly. You can review our returns policy <a href="/faqs#returns">here</a>.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>What can I do if the product is not available in my size?</strong></p>
							<div class="mk-collapse-content">
								<p>If a product is not available in your size, you can choose to be notified when it comes back in stock. To create a notification, please go to the product details page, choose your size, and provide your email address in the text box below the size display. We'll send you an email as soon as the item is back in stock. Please note that sometimes we may not be able to procure the item in your size. In such cases you will not receive a notification from us.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>How can I find out if Myntra.com delivers to my PIN Code?</strong></p>
							<div class="mk-collapse-content">
								<p>You can find out if Myntra.com delivers to your PIN Code using the courier serviceability tool located on the right side on all product detail pages. Please enter your PIN code into the tool and choose your mode of payment to get this information.</p>
								<p>The PIN codes serviced by us are frequently updated, so if we do not deliver to your PIN code today, please come back and check to see if this has changed.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>How long does it take for an order to be delivered to me?</strong></p>
							<div class="mk-collapse-content">
								<p>We deliver most of our orders within 7 days from the order date. You can find out how long we will need to deliver your order to your specific pincode using the courier serviceability tool located on the right side on all product detail pages.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>How are orders on Myntra.com delivered to me?</strong></p>
							<div class="mk-collapse-content">
								<p>All orders placed on Myntra.com are dispatched through our own courier service - Myntra.com Logistics, or through other courier partners such as Blue Dart, Quantium, etc. We do not use Speed Post to deliver orders placed on Myntra.com</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>Does Myntra.com deliver products outside India?</strong></p>
							<div class="mk-collapse-content">
								<p>No. At this point, Myntra.com delivers products only within India.</p>
							</div>
						</div>
						<h3 id="coupons" class="no-border">Coupons and Cashback</h3>
						<div>
							<p class="mk-collapse-control"><strong>How do I apply a coupon?</strong></p>
							<div class="mk-collapse-content">
								<p>You can apply a coupon on cart page before you place an order. The complete list of your unused and valid coupons is available under the 'Mynt Credits' header in your 'My Myntra' section.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>What is cashback? How do I redeem it?</strong></p>
							<div class="mk-collapse-content">
								<p>Cashback is your store credit with Myntra.com that can be used to pay for purchases on Myntra.com. You get cashback when you return items to Myntra.com or when you receive Myntra.com Gift Cards. You can redeem your cashback on the Payments page.</p>
								<p>You can check your cashback balance in your 'My Myntra' section.</p>
							</div>
						</div>
						<h3 id="payments" class="no-border">Payments</h3>
						<div>
							<p class="mk-collapse-control"><strong>How can I pay for my order at Myntra.com?</strong></p>
							<div class="mk-collapse-content">
								<p>We support the following payment options at Myntra.com:</p>
								<ul>
								 <li> Cash On Delivery (available in select pincodes)</li>     <li> Credit Card</li>     <li> Debit Card</li>     <li> Net banking</li>     			</ul>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>How does the COD (Cash on Delivery) payment option work?</strong></p>
							<div class="mk-collapse-content">
								<p>Cash on Delivery allows you to pay for your order with cash at the time of delivery. Myntra.com offers a Cash on Delivery option for all orders between Rs. {$codMinVal} and Rs. {$codMaxVal} for most delivery addresses. To place an order and pay for it using Cash on Delivery (COD), please select the 'Cash On Delivery' option on the payment page.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>Why can't I see the COD option on my payment page?</strong></p>
							<div class="mk-collapse-content">
								<p>If you do not see a COD option on your payment page, this may be due to one of the following reasons:</p>
								<ol>
								 <li>Your order value may be below Rs. {$codMinVal} or over Rs. {$codMaxVal}.</li>
								 <li>Our courier partners may not support a Cash on Delivery option to your delivery address.</li>
								 <li>You may have placed another order using the COD option which is pending delivery. If the amount of this order when added to your current order exceeds Rs. {$codMaxVal}, then the COD option will be disabled.</li>
								</ol>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>What should I do if my payment fails?</strong></p>
							<div class="mk-collapse-content">
								<p>Please retry making the payment after ensuring that the information entered is accurate, including all account details, billing addresses and passwords. If your payment still fails, you can use the Cash on Delivery(COD) payment option on the payment page to place your order.</p>
								<p>If your payment is debited from your account after a payment failure, it will be credited back within 7-10 business days, after we receive a confirmation from the bank.</p>
							</div>
						</div>
						{if $showVatFaq}
						<div>
							<p class="mk-collapse-control"><strong> I am being charged a VAT amount on my order. What is VAT?</strong></p>
							<div class="mk-collapse-content">
								<p>Value Added Tax (VAT) is an indirect tax under the Indian taxation system which is levied, based on consumption, by state governments.  VAT will have to borne/paid by the ultimate consumer of the product.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>How is the VAT amount decided?</strong></p>
							<div class="mk-collapse-content">
								<p>Following rules will govern whether or not an additional VAT will be applicable on the products purchased by you:</p>
								<ol>
									<li>Products which are sold at a net discount of {$vatThreshold}% or above, VAT is not included in the price and an additional VAT will be collected over and above the discounted price.</li>
									<li>Products which are sold at Maximum Retail Price or at a net discount less than {$vatThreshold}%, VAT is included in the price and no additional VAT will be charged.Net discount is essentially the product price less of product discount and coupon discount, if any.</li>
								</ol>
								{$vatFaqMessage}
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>If I return the purchased product will the VAT amount charged be refunded?</strong></p>
							<div class="mk-collapse-content">
								<p>Yes. If you return the product the applicable VAT amount will also be refunded to your Myntra cashback account.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>If I cancel my order will the VAT amount charged be refunded?</strong></p>
							<div class="mk-collapse-content">
								<p> Yes. If you cancel the order, the applicable VAT amount will also be refunded to your Myntra cashback account.</p>
							</div>
						</div>
						{/if}
						<h3 id="shipping" class="no-border">Order Tracking, Shipping and Delivery</h3>
						<div class="action-block">
							<h3>Know Where your Order Is!</h3>
							<p>You can Track your Orders easily</p>
							<a href="mymyntra.php?view=myorders" class="btn small-btn normal-btn">Go to My Myntra / Orders</a>
						</div>
						
						<h4>Myntra.com's Shipping Policy</h4>
					    {if $shippingRate > 0}
                            <p>
                                We strive to deliver products purchased from Myntra in excellent condition and in the fastest time possible.
                                {if $free_shipping_firstorder}If this is your first order with Myntra, shipping will be completely <span class='mk-din-med'>FREE</span>. Also, for all the subsequent purchases of Rs. {$free_shipping_amount}<sup>*</sup> or more, we will deliver the order to your doorstep free of cost.
                                {else}
                                    For all the purchases of Rs. {$free_shipping_amount}<sup>*</sup> or more, we will deliver the order to your doorstep free of cost.
                                {/if}
                                A shipping charge of Rs. {$shippingRate} will be applicable to all orders under Rs. {$free_shipping_amount}<sup>*</sup>.
                            </p>
					    {else}
					        <p>Myntra offers free shipping within India on all products purchased at its website.</p>
					    {/if}
					    <ul>
					        <li>If the order is cancelled, lost or un-delivered to your preferred location, we will refund the complete order amount including any shipping charges.</li>
					        <li>If you cancel part of the order, shipping charges will not be refunded.</li>
					        <li>If you return an order delivered to you, original shipping charges will not be refunded. However if you self-ship your returns, you will be reimbursed based on Myntra.com's <a href="faqs#returns">Returns Policy</a>.</li>
					    </ul>
					    <p><sup>* </sup>Order value is calculated after applying discounts.</p>
					    {if $shippingRate > 0}
							<div>
								<p class="mk-collapse-control"><strong>What are the shipping charges on Myntra products?</strong></p>
								<div class="mk-collapse-content">
								    <p>
								    	Myntra offers free shipping within India on all products if your total order amount is Rs. {$free_shipping_amount}<sup>*</sup>
								    	or more. Otherwise Rs. {$shippingRate} will be levied as Shipping Charges.
								    	   <br><br><sup>* </sup>Order value is calculated after applying discounts.
								    </p>
								</div>
							</div>
						{/if}						
						<div>
							<p class="mk-collapse-control"><strong>How do I check the status of my order?</strong></p>
							<div class="mk-collapse-content">
								<p>You can check the status of your order at any time at <a href="/mymyntra.php?view=myorders">this link</a>. This page also has a courier tracking number that can be used to track the status of the order at our courier partner's website. It may take up to 24 hours for the order status to be updated on our courier partner's website.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>How can I check if Myntra.com delivers to my PIN Code?</strong></p>
							<div class="mk-collapse-content">
								<p>You can find out if Myntra.com delivers to your PIN Code using the courier serviceability tool located on the right side on all product detail pages. Please enter your PIN code into the tool and choose your mode of payment to get this information.</p> 
								<p>The PIN codes serviced by us are frequently updated, so if we do not deliver to your PIN code today, please come back and check to see if this has changed.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>How long does it take for an order to be delivered to me?</strong></p>
							<div class="mk-collapse-content">
								<p>We deliver most of our orders within 7 days from the order date. You can find out how long we will need to deliver your order to your specific pincode using the courier serviceability tool located on the right side on all product detail pages.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>How are orders placed on Myntra.com delivered to me?</strong></p>
							<div class="mk-collapse-content">
								<p>All orders placed on Myntra.com are dispatched through our own courier service - Myntra.com Logistics, or through other courier partners such as Blue Dart, Quantium, etc. We do not use Speed Post to deliver orders placed on Myntra.com</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>Does Myntra.com deliver products outside India?</strong></p>
							<div class="mk-collapse-content">
								<p>No. At this point, Myntra.com delivers products only within India.</p>
							</div>
						</div>

						{if $nextDayDeliveryEnabled}
						<!-- Next day delivery starts -->
						<div>
							<p class="mk-collapse-control"><strong>How can I get my order delivered faster?</strong></p>
							<div class="mk-collapse-content">
								<p>We offer two faster delivery options to ensure guaranteed and faster delivery of your order: Same Day Delivery and Next Day Delivery.</p>
								<p>If you place an order by choosing the Same Day Delivery option before {$sameDayDeliveryCutOffTime} then we guarantee the delivery of your order by end of same business day. If you place an order by choosing the Next Day Delivery option before {$nextDayDeliveryCutOffTime} then we guarantee the delivery of your order by end of next business day.</p>
								<p>These services are offered for selected cities and items. If the Same Day Delivery and the Next Day Delivery options are available for your order, you will see these option displayed when you select the delivery address for your order.</p>
								
								<p><strong>Available Shipping Options</strong></p>
								<table class="faq-table">
									<thead>
										<tr>
									        <th>Shipping Option</th>
									        <th>Availability</th>
									        <th>Charges</th>
										</tr>
									</thead>
									<tbody>
										<tr>
								            <td class="caption">
								            	<strong>Same Day Delivery</strong>
												<div>Order placed before {$sameDayDeliveryCutOffTime} today will be delivered by end of same business day.</div>
											</td>
								            <td>Available for orders {if $chargeCartLimit > 0 }above Rs. {$chargeCartLimit}{/if} in Bangalore, Delhi, Faridabad, Ghaziabad, Greater Noida, Gurgaon,and Noida. </td>
								            <td>Rs. {$sameDayDeliveryCharges}</td>
								        </tr>

								        <tr>
								            <td class="caption">
								            	<strong>Next Day Delivery</strong>
												<div>Order placed before {$nextDayDeliveryCutOffTime} today will be delivered by end of next business day.</div>
											</td>
								            <td>Available for orders {if $chargeCartLimit > 0 }above Rs. {$chargeCartLimit}{/if} in Ahmedabad, Bangalore, Chennai, Delhi, Faridabad, Ghaziabad, Greater Noida, Gurgaon, Hyderabad, Mumbai, Noida and Pune.</td>
								            <td>Rs. {$nextDayDeliveryCharges}</td>
								        </tr>
								        <tr>
								            <td class="caption">
								            	<strong>Standard Delivery</strong>
												<div>Delivery typically in 2 to 7 days. Please check the product details page for pincode-specific times.</div>
											</td>
								            <td>All serviceable locations.</td>
								            <td>
								            {if $shippingRate > 0}
								            	FREE for orders above Rs. {$chargeCartLimit}. Rs. {$shippingRate} per order below Rs. {$chargeCartLimit}
								           	{else}
								           		FREE
								           	{/if}
								           	</td>
								        </tr>
									</tbody>
								</table>
							</div>
						</div>



						<div>
							<p class="mk-collapse-control"><strong>What if my Same Day Delivery/Next Day Delivery order is not delivered by the next business day?</strong></p>
							<div class="mk-collapse-content">
								<p>You will receive a full refund of Same Day Delivery/Next Day Delivery charges to your Myntra cashback account if we are not able to attempt a delivery to you within the promised date. </p>
							</div>
						</div>

						<div>
							<p class="mk-collapse-control"><strong>What is next business day?</strong></p>
							<div class="mk-collapse-content">
								<p>Business day is ‘working day’ of a week when we do deliveries. We deliver only on business days that include Monday through Saturday. We remain closed on Sunday and gazetted or statutory holidays. The holiday can vary from state to state. Any Next Day Delivery order placed on a Saturday or day before a holiday will be delivered on the next business day.</p>
								<p><strong>List of holidays across India:</strong></p>
								<table class="faq-table">
									<thead>
										<tr>
									        <th>Name of the holiday</th>
									        <th>Date</th>
										</tr>
									</thead>
									<tbody>
								    <tr>
										<td>New Year's Day</td>
										<td>1-Jan-2014</td>
									</tr>
									<tr>	
										<td>Makar Sankranti and Milad-un-Nabi</td>
										<td>14-Jan-2014</td>
									</tr>
									<tr>	
										<td>Bihu Festival</td>
										<td>15-Jan-2014</td>
									</tr>
									<tr>	
										<td>Uttarayana Punya kala Sankranti Festival</td>
										<td>15-Jan-2014</td>
									</tr>
									<tr>	
										<td>Republic Day</td>
										<td>26-Jan-2014</td>
									</tr>
									<tr>	
										<td>Maha Shivaratri</td>
										<td>27-Feb-2014</td>
									</tr>
									<tr>	
										<td>Holi</td>
										<td>17-Mar-2014</td>
									</tr>
									<tr>	
										<td>Chandramana Ugadi</td>
										<td>31-Mar-2014</td>
									</tr>
									<tr>	
										<td>Ram Navami </td>
										<td>8-Apr-2014</td>
									</tr>
									<tr>	
										<td>Tamil New year's day and Dr B R Ambedkar's Birthday </td>
										<td>14-Apr-2014</td>
									</tr>
									<tr>	
										<td>May Day</td>
										<td>1-May-2014</td>
									</tr>
									<tr>	
										<td>Rath Yatra</td>
										<td>29-Jun-2014</td>
									</tr>
									<tr>	
										<td>Ramzan</td>
										<td>29-Jul-2014</td>
									</tr>
									<tr>	
										<td>Independence Day</td>
										<td>15-Aug-2014</td>
									</tr>
									<tr>	
										<td>Gopak Kala / Dahi Handi</td>
										<td>18-Aug-2014</td>
									</tr>
									<tr>	
										<td>Janamastami </td>
										<td>18-Aug-2014</td>
									</tr>
									<tr>	
										<td>Varasiddhi Vinayaka Vrata</td>
										<td>29-Aug-2014</td>
									</tr>
									<tr>	
										<td>Anand Chathurthi</td>
										<td>8-Sep-2014</td>
									</tr>
									<tr>	
										<td>Biswakarma Pooja</td>
										<td>17-Sep-2014</td>
									</tr>
									<tr>	
										<td>Mahathma Gandhi Jayanthi </td>
										<td>2-Oct-2014</td>
									</tr>
									<tr>	
										<td>Maha Navami Ayudapooja </td>
										<td>3-Oct-2014</td>
									</tr>
									<tr>	
										<td>Vijayadasami </td>
										<td>4-Oct-2014</td>
									</tr>
									<tr>	
										<td>Naraka Chaturdashi </td>
										<td>22-Oct-2014</td>
									</tr>
									<tr>	
										<td>Kali Pooja</td>
										<td>23-Oct-2014</td>
									</tr>
									<tr>	
										<td>Balipadyami Deepavali </td>
										<td>24-Oct-2014</td>
									</tr>
									<tr>	
										<td>Bhaidhunj</td>
										<td>25-Oct-2014</td>
									</tr>
									<tr>	
										<td>Kannada Rajyotsava </td>
										<td>1-Nov-2014</td>
									</tr>
									<tr>	
										<td>Christmas </td>
										<td>25-Dec-2014</td>
									</tr>
									</tbody>
								</table>

							</div>
						</div>

						<!-- Next day delivery ends -->

						{/if}





						<h3 id="cancel" class="no-border">Cancellations and Modifications</h3>
						<h4>Myntra's Cancellation Policy</h4>
					    <p>
					    	You can cancel an order until it has been processed in our warehouse by going to the 'My Myntra' section of the website. This includes items purchased on sale. Any amount you paid will be credited back into your Myntra.com account in the form of Cashback.
					    </p>
						<p>For specific queries, please read the FAQs below.</p> 						
						<div>
							<p class="mk-collapse-control"><strong>Can I modify the shipping address of my order after it has been placed?</strong></p>
							<div class="mk-collapse-content">
								<p>Yes. You can modify the shipping address of your order before we have processed (shipped) it, by contacting our customer support team at {$customerSupportCall}.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>I just cancelled my order. When will I receive my refund?</strong></p>
							<div class="mk-collapse-content">
								<p>If you have selected Cash on Delivery, there is no amount to refund because you haven't paid for your order. For payments made using a Credit Card, Debit Card, or Net Banking, you will receive a refund in your Myntra.com Cashback account immediately after your order has been cancelled. Any coupons used are also reinstated it to your account.</p>
								<p>If you have made your payment using a Credit Card, Debit Card, or net banking and would like a refund to your bank account, please get in touch with our customer support team.</p>
							</div>
						</div>
						<h3 id="returns" class="no-border">Returns</h3>
						<div class="action-block">
							<h3>Want to Return an Item?</h3>
							<p>Returning Items is Easy - Just use our Returns Wizard!</p>
							<a href="mymyntra.php?view=myreturns" class="btn small-btn normal-btn">Go to My Myntra / Returns</a>
						</div>
						
						<h4>Myntra.com's 30 Day Returns & Exchanges Policy ( <a href="#returns-hindi" id="returns-hindi-link">हिंदी मे</a> )</h4>
						<p>Myntra.com's Return and Exchange Policy offers you the option to return or exchange items purchased on Myntra.com within 30 days of receipt. In case of returns, we will credit the amount you paid for the products as cashback into your Myntra cashback account. If you choose to exchange the item for reason of mismatch of size or receipt of a defective item, you will be provided with a replacement of the item, free of cost. However an exchange is subject to availability of the item in our stock.</p>
						<p> The following EXCEPTIONS and RULES apply to this Policy: </p>
				    	<ol>

						<li>Certain innerwear, cosmetics, socks, deodorants, perfumes, fashion accessories, hair accessories cannot be exchanged or returned. The list of such items includes:</li>
                                                 <ul>
						 <li>Briefs, Shape wear bottoms or any lingerie set that includes a brief.</li>
						 <li>Anklets, Bangles, Earrings, Bracelets, Brooches, Earrings, Pendants, Necklaces and Rings.</li>
						 <li>Mittens, Socks and Wrist-Bands.</li>
						 </ul>
						 <li>Due to their intimate nature, we handle returns and exchanges for innerwear, sleepwear and lingerie items differently:</li>
						 <ul>
						 	<li>Only self-ship return allowed for such items, the pickup facility will not be available.</li>
						 	<li>These items cannot be exchanged.</li>
						 </ul>
						 <li>Fine jewelry products must be returned within 15 days from the day of delivery, To return your fine jewelry purchase, please call customer care at {$customerSupportCall}.</li>
							<li>All items to be returned or exchanged must be <strong>unused and in their original condition</strong> with all original tags and packaging intact (for e.g. shoes must be packed in the original shoe box as well and returned), and should not be broken or tampered with.</li>
							<li>Only size exchanges are allowed. Items can be exchanged for a similar size or a different size.</li>
							<li>Exchanges are allowed only if your address is serviceable for an Exchange by our logistics team.</li>
							<li>In case you had purchased an item which has a free gift/offer associated with it and you wish to return the item,maximum of Refunds/Free item MRP will be debited until the satisfactory receipt of all free gift(s)/offer item(s) that are shipped along with it.</li>
							<li>If you choose to self-ship your returns, kindly pack the items securely to prevent any loss or damage during transit. For all self-shipped returns, we recommend you use a reliable courier service.</li>
							<li>Once your return is received by us, you will be reimbursed Rs 100/- towards shipping costs per order/parcel, if self-shipped. This is subject to your return having met the requirements of this Policy.</li>
						</ol>
						<p>For specific queries, please read the FAQs below.</p>
						<div>
							<p class="mk-collapse-control"><strong>What is Myntra.com's 30 day return and exchange policy? How does it work?</strong></p>
							<div class="mk-collapse-content">
								<p>Myntra.com's 30 day returns and exchange policy gives you an option to return or exchange items purchased on Myntra.com for any reason within 30 days of receipt of the item.  We only ask that you don't use the product and preserve its original condition, tags, and packaging. You are welcome to try on a product but please take adequate measure to preserve its condition.</p>
								<p>There are two ways to return the product to us. In most locations we offer a free pick up service for your return. You will see a pickup option when you submit a return request.  If we don't offer a pick up at your location, or if you prefer, you can ship an item back to us. In such cases, we will credit your cashback account with Rs.100 to cover your cost of shipping the product back to us.</p>
								<p>Once we pick up or receive your return, we will do a quality check of the product at our end and if the product passes our quality check we will credit the amount you paid for the products as cashback into your Myntra.com account. If you wish to receive a refund instead, please send an email with the following details from your registered email address to <a href="mailto:payment@myntra.com">payment@myntra.com</a>. If the picked up product does not pass the quality check we shall return it back to you.</p>
									<ul>
									<li>Order Number</li><li>Bank Name</li><li>Account Holder Name</li><li>Bank Account Number</li><li>Branch Address</li><li>IFSC Code </li>
									</ul>
								<p>You can exchange an item purchased from Myntra.com for a new size or the original size for no extra charge. However all exchanges are subject to stock availability and subject to your address being serviceable for an exchange. If you choose to exchange an item, our delivery representative will deliver the new item to you and simultaneously pick up the original item from you. Please note that we are only able to offer size exchanges. If you wish to exchange your item for an alternative product, we suggest that you return it to obtain a cash-back refund and purchase the new item separately.</p>
								<p>The following products sold on Myntra.com are exceptions to this policy:</p>
								<ul>
								<li>Certain innerwear, cosmetics, socks, deodorants, perfumes, fashion accessories, hair accessories cannot be exchanged or returned. The list of items where returns or exchanges is not supported includes:</li>
								<ul>
								<li>Bra, Vests, Briefs, Trunks and Lingerie.</li>
								<li>Anklets, Bangles, Earrings, Bracelets, Brooches, Earrings, Pendants, Necklaces and Rings.</li>
								<li>Mittens, Socks and Wrist-Bands.</li>
								</ul>
								<li>Fine jewelry products must be returned within 15 days of delivery.</li>
								</ul>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>How do I return a product to Myntra.com?</strong></p>
							<div class="mk-collapse-content">
								<p>To return a product to Myntra.com, please follow these steps:</p>
								<ol>
									<li>Create a Returns Request online by going to the <a href="/mymyntra.php?view=myreturns">My Returns</a> section of Myntra.com. You can return almost all products purchased from Myntra.com within 30 days of receiving the product. Follow the screens that come up after clicking on 'Return a Product' button. Please make a note of the Return ID that that we generate at the end of the process.</li>
									<li>Fill out a returns form: You can use the returns form that was sent with the order or download and print one at the link available by clicking on your return ID. If you don't have the returns form or can't access a printer, please write the following information on a sheet of paper and use that sheet as the returns form:
										<ul>
										<li>Order No</li>
										<li>Return Id</li>
										</ul>
									</li>
									<li>Keep the item ready for pick up or ship it to us: You can choose to schedule a pick-up of your order or self-ship your return to us. We offer pick-ups of your return in select locations - this option will be available to you when you submit your return online.
										<ul>
										<li><b>Pick-up:</b> If you choose to schedule a pick-up, please put the product with accompanying tag and a filled out returns form in a packet. Leave the packet ready and open to expedite the return pickup. Our staff will initially examine the product at the time of pickup and a further quality check of the product will be conducted at our Distribution Center. Once your product has been approved by our quality staff we will credit your account with cashback.</li>
										<li><b>Self-ship:</b> If you choose to self-ship, please pack the product in its original package with accompanying tags and a filled out returns form. Kindly address the package to the address of the delivery center closest to you. We have listed out the addresses of the delivery centers in another section on this page.</li>
										</ul>
									</li>
								</ol>
								<p>We will send you a confirmation email as we receive the shipment at our warehouse. At any time, you can track the status of your return request by going to the <a href="/mymyntra.php">My Myntra</a> section.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>How do I place an exchange request on Myntra.com?</strong></p>
							<div class="mk-collapse-content">
								<p>If you would like to exchange some products purchased from Myntra.com, please follow these steps:</p>
								<ol>
									<li>Create an exchange request online. You can exchange any product purchased from Myntra.com within 30 days of delivery by going to the <a href="/mymyntra.php">My Myntra</a> section. Please select the item you wish to exchange. If your address is serviceable for exchange you will be able to proceed and generate an exchange id. Please note down your exchange id for future reference.</li>
									<li>Fill out a returns form for the original product that you are exchanging. You can use the returns form that was sent with the original order. If you don't have a returns form, please write the following information on a sheet of paper and use this sheet as the returns form.
										<ul>
										<li>Original Order Id</li>
										<li>Exchange Order Id</li>
										</ul>
									</li>
									<li>Place the product in a packet but do not seal it. In the packet, please include the product along with the tags that it came with and a completed returns form.</li>
									<li>Hand over the original product to our delivery staff and receive the exchange item from him. Please ensure that you have the original item available with you at the same address which has been selected for delivery of the exchange item.</li>
								</ol>
								<p>At any time, you can track the status of your exchange requests by going to the <a href="/mymyntra.php">My Myntra</a> section.</p>
							</div>
						</div>						
						<div>
							<p class="mk-collapse-control"><strong>How long would it take me to receive the refund for my return?</strong></p>
							<div class="mk-collapse-content">
								<p>It can take up to 15 days for you to receive your refund. We take up to 2 days to process the refund once we receive the product at our returns processing facility. Therefore the return time really depends on the time it takes the courier company to deliver the packet to the returns processing facilty.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>How do I return multiple products from a single order?</strong></p>
							<div class="mk-collapse-content">
								<p>If you are returning multiple products from a single order, you will receive a separate Return ID and email for each item. However, you can ship all the products delivered in a single order in a single shipment. Please include the Return IDs for all the products in a single return form.
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>Does Myntra.com pick up the product I want to return from my location?</strong></p>
							<div class="mk-collapse-content">
								<p>Currently we pickup returns only in select PIN Codes. If your delivery location is serviceable, you will be able to see and select the pickup option when you queue your return online.</p>
								<p>We will pick up the return within 72 hours of your placing the request. Please keep the return shipment ready.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>I would like to Self-Ship the return myself to Myntra.com. How can I do this?</strong></p>
							<div class="mk-collapse-content">
								<p>If your pincode is not covered by our pickup service, you will need to self-ship the return item by courier. If we do not offer a return pick up service in your area or you choose to ship the return to us, please send the return package and the returns form inside to the nearest delivery centre. For all self-shipped returns, you will be reimbursed Rs. 100 towards your shipping costs. This is subject to your returns being inspected and successfully processed upon receipt at our end.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>Where should I send my return?</strong></p>
							<div class="mk-collapse-content">
								<p>You can send the return to any one of the following addresses depending on your location:</p>
							  <ul>
							   <li>
							   <strong>The Returns Desk</strong><br><strong>
							   Vector Ecommerce Pvt Ltd.</strong>.<br>
								No 36/5, 27th Main,<br/>
								HSR layout, Sector 2,<br/>
								Somasandra Playa,<br/>
								Near KEB Power station,<br/>
								Haralukunte Post,<br/>
								Bangalore 560102 
							   </li>   
							   <li>
							   <strong>Vector E-Commerce Pvt Ltd.</strong><br>
							   Dev Niwas, K-2 Block,<br>
							   Khasra No. 819,<br>
							   Mahipalpur,<br>
							   Delhi-110037
							   </li>   
							   <li>
							   <strong>Vector E-commerce Pvt. Ltd.</strong><br>
							   Plot no: 22, 1st floor,<br>
							   Banerjee Para Road,<br>
							   West Putiary,<br>
							   Kolkata- 700041
							   </li>   
							   <li>
							   <strong>Vector E-Commerce Pvt Ltd.</strong><br>
							   Acropolis,<br>
							   Lower Ground floor. CTS NO 1386<br>
							   Church Road, Marol Andheri ( East )<br>
							   Mumbai 400 059
							   </li>  
							  </ul>
							</div>
						</div>
						<h3 id="mynt-club" class="no-border">Registration and Referral Program</h3>
						<div>
							<p class="mk-collapse-control"><strong>What are the benefits when I create an account?</strong></p>
							<div class="mk-collapse-content">
														
								<p>When you create an account with Myntra.com, you will instantly earn following set of coupons.								
								</p>
								<ol>
								{foreach from=$couponConfig item=aCoupon}
									
									{if $aCoupon.coupon_type eq "absolute"}
									<li>Rs. {$aCoupon.amount_discount} off on purchase of Rs. {$aCoupon.minimum_purchase} or more.</li>
									{elseif $aCoupon.coupon_type eq "percentage"}
									<li>{$aCoupon.percent_discount}% off on purchase of Rs. {$aCoupon.minimum_purchase} or more.</li>
									{elseif $aCoupon.coupon_type eq "dual"}
									<li>{$aCoupon.percent_discount}% off upto Rs. {$aCoupon.amount_discount} on purchase of Rs. {$aCoupon.minimum_purchase} or more.</li>
									{/if}
								{/foreach}
								</ol>
								
								 							
								<p>Alternatively, you could register with Myntra.com using the 'Connect with Facebook' option, and instantly earn following set of coupons.</p>
								
								<ol>
								{foreach from=$fbCouponConfig item=aCoupon}
									
									{if $aCoupon.coupon_type eq "absolute"}
									<li>Rs. {$aCoupon.amount_discount} off on purchase of Rs. {$aCoupon.minimum_purchase} or more.</li>
									{elseif $aCoupon.coupon_type eq "percentage"}
									<li>{$aCoupon.percent_discount}% off on purchase of Rs. {$aCoupon.minimum_purchase} or more.</li>
									{elseif $aCoupon.coupon_type eq "dual"}
									<li>{$aCoupon.percent_discount}% off upto Rs. {$aCoupon.amount_discount} on purchase of Rs. {$aCoupon.minimum_purchase} or more.</li>
									{/if}
								{/foreach}
								</ol>
								
								<p>Please note that you will receive the sign up benefit based on the method you use to sign up on Myntra.com for the first time.
								This will be added to your My Myntra section.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>How can I refer my friends?</strong></p>
							<div class="mk-collapse-content">
								<p>If you would like to refer your friends, please follow these steps:</p>
								<ol>
									<li>Go to the My Referrals tab in your My Myntra section.</li>     
									<li>You have the option to import the email addresses of friends from your Gmail, Yahoo! Mail, AOL Mail or Outlook accounts. Choose your email account and follow the steps to select the contacts you wish to invite. You can also manually type in email addresses, separating each one with a comma. We will invite your friends to join Myntra.com on your behalf by sending them an email with a link inviting them to register.</li>     
									<li>Alternatively, you can click on the icons below the page to refer your friends through Facebook and twitter. You can also distribute your invitation link through your blog, social network, instant messengers and your website by pasting your referral link along with a personalized message for your friends and followers.</li>     
									<li>Do note that your contacts would have to create accounts on Myntra.com using the link shared by you or through the link in the referral email. The referral benefit would not be credited to you if your referred contacts are already registered on Myntra.com</li>    
								</ol>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>What benefits do I receive if I refer friends to Myntra.com?</strong></p>
							<div class="mk-collapse-content">
								<p>You earn Rs.{$refregValue} when each of your referral registers with Myntra.com. This is added to your My Myntra account as {$mrpCouponConfiguration.mrp_refRegistration_textNumCoupons} coupon of Rs.{$mrpCouponConfiguration.mrp_refRegistration_mrpAmount}, valid for {$mrpCouponConfiguration.mrp_refRegistration_validity} days. You can redeem this against any purchase of Rs.{$mrpCouponConfiguration.mrp_refRegistration_minCartValue|number_format:0:'':','} and above.</p>
								<p>You also earn Rs.{$refpurValue} when your referred friends make their first purchase. This is added to your My Myntra account as {$mrpCouponConfiguration.mrp_refFirstPurchase_textNumCoupons} coupons of Rs.{$mrpCouponConfiguration.mrp_refFirstPurchase_mrpAmount} each, valid for {$mrpCouponConfiguration.mrp_refFirstPurchase_validity} days. You can redeem these against purchases of Rs.{$mrpCouponConfiguration.mrp_refFirstPurchase_minCartValue|number_format:0:'':','} and above.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>How can I redeem my coupons?</strong></p>
							<div class="mk-collapse-content">
								<p>You will be required to verify your mobile number before you can start redeeming your credits. On initiating the verification process, you would need to register using your mobile number. Do note that you can only create one account with your mobile number to receive your registration and referral benefits.</p>
							</div>
						</div>
                                                <h3 id="zipdial-sms" class="no-border"> SMS Referral Program </h3>
                                                <h4> CAMPAIGN IS CURRENTLY SUSPENDED </h4>
                                                <div>
							<p class="mk-collapse-control"><strong>How does SMS referral program work </strong></p>
							<div class="mk-collapse-content">
								<p>Referrer gets 10 loyalty points for every new friend referred. Points are credited when the new user calls on the number provided by the referrer( provided to the referrer through Myntra campaign). Points are credited within 24 hours of the call. Points are not credited if myntra account already exists with the phone number</p>
							</div>
						</div>
                                                <div>
							<p class="mk-collapse-control"><strong>What does the new users get </strong></p>
							<div class="mk-collapse-content">
								<p>Referral get Rs 100 cash back into the account after registration with Myntra using the phone number the referral used to give the miss call. Cashback is credited within 24 hours of the registration. Cashback is not credited if myntra account already exists with the phone number of the friend.</p>
							</div>
						</div>
                                                <div>
							<p class="mk-collapse-control"><strong>What else? </strong></p>
							<div class="mk-collapse-content">
								<p>Myntra.com reserves the right to amend/modify/cancel/update or withdraw the campaign without notice.</p>
							</div>
						</div>

						<h3 id="gift-cards" class="no-border">Gift Cards</h3>
						<div>
							<p class="mk-collapse-control"><strong>How do I purchase a Myntra.com Gift Card?</strong></p>
							<div class="mk-collapse-content">
								<p>To purchase a Myntra.com Gift Card, please go to our Gift Card page and enter the required information. We will email the gift card to the recipient instantly. The recipient can redeem the gift card and use the amount to pay for his or her purchase on Myntra.com</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>I just received a Myntra.com Gift Card. How do I use it?</strong></p>
							<div class="mk-collapse-content">
								<p>To use a Gift Card, click on the 'Add Gift Card' button in the body of the email. You will be taken to the Myntra.com website where you will need to sign up or login to your account. Once this is done, the Gift card value is instantly added to your Cashback account and can be used for any Myntra.com purchase.</p>
								<p>You can also add a Gift Card to your Myntra.com Cashback account by going to Gift cards section in the 'My Myntra' section of the website. You can also redeem a gift card while you are making a purchase. Simply enter the gift card code in the payment options tab. </p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>What should I do if I have not received the mail that contained a Gift Card?</strong></p>
							<div class="mk-collapse-content">
								<p>If you have not received the Gift Card, please contact the sender and ask him to resend the Gift Card. The sender can resend the Gift Card email from the Gift Card section on the Myntra.com website. </p>
								<p>Please note than once the Gift Card has been resent, the most recent email must be used to add the Gift Card to your account.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>Do Myntra.com Gift Cards expire?</strong></p>
							<div class="mk-collapse-content">
								<p>Yes. Unless redeemed, Myntra.com Gift Cards expire one year from the date of issue. Once the Gift Card has been redeemed into the recipient's cashback account, the cashback never expires.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>How do I cancel a Gift Card?</strong></p>
							<div class="mk-collapse-content">
								<p>You can cancel a Gift Card if it has not been redeemed by your recipient. If the Gift Card has not been redeemed and you would like to cancel it, please add the Gift Card to your own account. The amount will be transferred to your Cashback account and can be used to purchase products on Myntra.com. Gift card amount that is transferred to the customer’s cash back account cannot be used to buy another gift card </p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>What Payment Methods are available for Gift Cards?</strong></p>
							<div class="mk-collapse-content">
								<p>You can purchase Gift Cards using any online payment method (i.e Credit Card, Debit Card and Netbanking)</p>
							</div>
						</div>
						<h3 id="gift-wrap" class="no-border">Gift Wrapping</h3>
						<div>
							<p class="mk-collapse-control"><strong>Can I use Myntra.com to send gifts to others?</strong></p>
							<div class="mk-collapse-content">
								<p>Yes, you can use Myntra.com to send gifts to others. For an additional charge of Rs. 25, you can choose to gift wrap your order by selecting the option in your shopping bag. Your gift will also include a personalized message from you.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>What payment methods can I use to pay for a gift order?</strong></p>
							<div class="mk-collapse-content">
								<p>You can use any payment method that Myntra.com supports, except Cash on Delivery. You can also use your Cashback balance to pay for the order.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>Will the pricing, discount and payment information be displayed on the package sent to the recipient</strong></p>
							<div class="mk-collapse-content">
								<p>No. The external packaging slip/invoice will not include any pricing, discount, or payment information. However, the original product tag with the MRP will be intact on the products inside the package.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>Can I apply a coupon or discount on the gift wrapping charge?</strong></p>
							<div class="mk-collapse-content">
								<p>No. Discounts and coupons are not applicable on the gift wrap charge.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>Who can return the gift?</strong></p>
							<div class="mk-collapse-content">
								<p>Both the sender and the recipient can put in a request to return the gift item. A sender can return an item by visiting the My Orders tab in his My Myntra section. The gift recipient can call our customer support to return a gift item. The refund amount will be credited to the sender's Cashback account.</p>
								<p>Please note that when you return an item, the gift wrap charges are not credited back.</p>
							</div>
						</div>
					
						{if $loyaltyEnabled}
						<h3 id="my-privilege" class="no-border">Myntra Points</h3>
						    {if isset($loyaltyDisableMessage) && trim($loyaltyDisableMessage)!==''}
   								 <h4>{$loyaltyDisableMessage}</h4>
    						{/if}

						<div>
							<p class="mk-collapse-control"><strong>What is My Privilege?</strong></p>
							<div class="mk-collapse-content">
							<p>
							My Privilege is the Loyalty Program offered by Myntra to its customers. Members of this program will earn points with every purchase. They can redeem the points earned on future transactions at Myntra.com
							</p>
							</div>
						</div>

						<div>
							<p class="mk-collapse-control"><strong>How do I become a member of My Privilege?</strong></p>
							<div class="mk-collapse-content">
							<p>
							You can become a member of My Privilege by registering at Myntra.com. All existing customers are given a free membership.
							</p>
							</div>
						</div>

						<div>
							<p class="mk-collapse-control"><strong>What are the benefits that I receive?</strong></p>
							<div class="mk-collapse-content">
							<p>
							As a part of the My Privilege Program, you will stand a chance to avail numerous offers and privileges apart from points that can be used to buy products available on Myntra. Every order will provide points which can be used later for redemption.
							</p>
							</div>
						</div>

						<div>
							<p class="mk-collapse-control"><strong>How much is each point worth?</strong></p>
							<div class="mk-collapse-content">
							<p>
							Each point is worth 50 paise.
							</p>
							</div>
						</div>


			<div>
							<p class="mk-collapse-control"><strong>I am a Silver/Gold/Platinum member. What does that mean?</strong></p>
							<div class="mk-collapse-content">
								<p>On the basis of various levels of engagement that you'll have with Myntra, your status will be constantly evaluated and will be classified into one of the above-mentioned tiers. Please see the details below for more information on the categorisation.</p>
								<table class="faq-table">
									<thead>
										<tr>
				                            <th>Benefits</th>
				                            <th>Silver</th>
				                            <th>Gold</th>
				                            <th>Platinum</th>
										</tr>
									</thead>
						            <tbody>
						                        
						                        <tr>
						                            <td class="caption">Reward Points</td>
						                            <td>6 points for Rs. 100</td>
						                            <td>9 points for Rs. 100</td>
						                            <td>12 points for Rs. 100</td>
						                        </tr>
						                        <tr>
						                            <td class="caption">Redemption</td>
						                            <td>Minimum of 200 points required for redemption</td>
						                            <td>Minimum of 200 points required for redemption</td>
						                            <td>Minimum of 200 points required for redemption</td>
						                        </tr>
						                        <tr>
						                            <td class="caption">Qualification Criteria</td>
						                            <td>Default Membership</td>
						                            <td>Shop for Rs. 3000-7999 or more in 6 months</td>
						                            <td>Shop for Rs. 8000 or more in 6 months</td>
						                        </tr>
						                        <tr>
						                            <td class="caption">Re-qualification Criteria</td>
						                            <td>No-downgrade</td>
						                            <td>Shop for min Rs.3000 in a year</td>
						                            <td>Shop for min Rs.5000 in a year</td>
						                        </tr>
						                      </tbody></table>
							</div>
						</div>

						<div>
							<p class="mk-collapse-control"><strong>Is there an annual fee for this program?</strong></p>
							<div class="mk-collapse-content">
							<p>
							No. There is no annual fee for the program.
							</p>
							</div>
						</div>

						<div>
							<p class="mk-collapse-control"><strong>Can I leave My Privilege program?</strong></p>
							<div class="mk-collapse-content">
							<p>
							No. All Myntra customers are a part of the program.
							</p>
							</div>
						</div>

						<div>
							<p class="mk-collapse-control"><strong>How do I earn points?</strong></p>
							<div class="mk-collapse-content">
							<p>
							As a member of the program, you earn points for every successful transaction that you make. Initially, awarded points will be in a 'Pending' state, which will become 'Active' once you accept the order/ item. Accepting an item means that you have verified the product and agreed that you will not return the product.
							</p>
							</div>
						</div>

						<div>
							<p class="mk-collapse-control"><strong>I am not able to receive points on placing on order. Why?</strong></p>
							<div class="mk-collapse-content">
							<p>
							Please verify that you have accepted the Terms and Conditions of My Privilege and become a member of this program. If you are already a member of My Privilege, please reach us through email/ contact us at <a href="http://www.myntra.com/contactus">http://www.myntra.com/contactus</a> or call our customer care at 080-43541999. We will make sure that any discrepancy is resolved at the earliest.
							</p>
							</div>
						</div>

						<div>
							<p class="mk-collapse-control"><strong>Why are points being shown in 'Pending' state?</strong></p>
							<div class="mk-collapse-content">
							<p>
							The points will be activated only after you have successfully accepted the order and have not returned part or whole of the order in 30 days from the date of order delivery
							</p>
							</div>
						</div>

						<div>
							<p class="mk-collapse-control"><strong>How can I convert Pending points to Active points?</strong></p>
							<div class="mk-collapse-content">
							<p>
							To activate, you have to accept the order and click the 'Get Points' link against the order id. for which the points are in the 'PENDING' state. Alternatively, the points will get activated in 30 days from the date of delivery. Once you've claimed the points you will not be able to return the product.
							</p>
							</div>
						</div>


						<div>
							<p class="mk-collapse-control"><strong>Where and how can I redeem my points?</strong></p>
							<div class="mk-collapse-content">
							<p>
								Once you have a minimum of 200 active points, you can redeem them on purchases at Myntra.com. Each point has a value of Rs. 0.50. Points can be redeemed at the checkout page by selecting the "Redeem Points" option.
							</p>
							<p>
							Illustration:<br/>
							You have 3000 active points in your account. You are about to purchase items worth Rs. 1,200. You can use 2400 points to make the purchase.
							</p>
							<p>
								You have 3000 active points in your account. You are about to purchase items worth Rs. 3000. You can use 3000 points to pay for Rs.1,500. You may use any of the payment options to pay the remaining Rs.1500.
							</p>
							</div>
						</div>

						<div>
							<p class="mk-collapse-control"><strong>I am unable to redeem points. Why?</strong></p>
							<div class="mk-collapse-content">
							<p>
								You need a minimum of 200 points to be able to redeem them. Please check your account to see if you have the required points. If you have more than 200 points and are still not able to redeem them, please contact the customer care.
							</p>
							</div>
						</div>


						<div>
							<p class="mk-collapse-control"><strong>I have multiple accounts at Myntra. Can I club points in my multiple accounts?</strong></p>
							<div class="mk-collapse-content">
							<p>
								No, you can't club points present in multiple accounts. To enjoy the full benefits of the program, we advise you to use a single account to make all your transactions.
							</p>
							</div>
						</div>

						<div>
							<p class="mk-collapse-control"><strong>Will my points expire?</strong></p>
							<div class="mk-collapse-content">
							<p>
								Yes. Points will expire in 12 calendar months since the month of their issue. So, all points issued in October 2013 will expire on October 2014
							</p>
							</div>
						</div>


						<div>
							<p class="mk-collapse-control"><strong>My pending points got deducted. Why?</strong></p>
							<div class="mk-collapse-content">
							<p>There are 2 cases in which pending points could be deducted</p>
							<p>	a. If you return the order or a part of it, points will get deducted accordingly</p>
							<p> b. If you cancel the product or reject the order, the points issued for that order will be debited from your account</p>
							</div>
						</div>

						<div>
							<p class="mk-collapse-control"><strong>How can I get upgraded to a Gold/ Platinum membership?</strong></p>
							<div class="mk-collapse-content">
								<p>If you haven't made any transactions with Myntra or have only made transactions worth less than Rs. 3000 in the last 6 months, you will be a Silver Member</p>
								<table class="faq-table">
									<thead>
						                        <tr>
						                            <th> </th>
						                            <th>Silver</th>
						                            <th>Gold</th>
						                            <th>Platinum</th>
						                        </tr>
						            </thead>            
			                      <tbody>
			                        <tr>
			                            <td class="caption">Qualification Criteria</td>
			                            <td>Default Membership</td>
			                            <td>Shop for Rs. 3000-7999 or more in 6 months</td>
			                            <td>Shop for Rs. 8000 or more in 6 months</td>
			                        </tr>
			                      </tbody>
						        </table>
							</div>
						</div>



						<div>
							<p class="mk-collapse-control"><strong>Will I get downgraded from my current membership? How can I avoid it?</strong></p>
							<div class="mk-collapse-content">
							<p>If you successfully satisfy the following conditions, depending on the membership level you are in, there will be no membership downgrade.<p>
							<p>Eg: If you're a Gold Member, you need to shop for Rs. 3000 in a year. If you become a Platinum member in October 2013, you need to make purchases worth Rs. 5000 in a year's period to remain in the same tier.</p>
									<table class="faq-table">
						                <thead>  
						                  <tr>
						                    <th> </th>
						                    <th>Silver</th>
						                    <th>Gold</th>
						                    <th>Platinum</th>
						                </tr>
						            	</thead>
						                  <tbody>
						                <tr>
						                    <td class="caption">Re-qualification Criteria</td>
						                    <td>Default Membership</td>
						                    <td>Shop for minimum of Rs.3000 in a year since qualification</td>
						                    <td>Shop for minimum of Rs.5000 in a year since qualification</td>
						                </tr>
						              </tbody>
						            </table>
							</div>
						</div>


						<div>
							<p class="mk-collapse-control"><strong>I have made all the required purchases, but my membership has not been upgraded yet. Why?</strong></p>
							<div class="mk-collapse-content">
							<p> 
								You will be upgraded only if you have accepted all the orders that will enable the upgrade. You can check the status in the Loyalty Tab of your My Myntra Account.
							</p>
							<p>
								If you have successfully met all the criteria and you're still facing the issue, kindly contact our customer care.
							</p>
							</div>
						</div>

						<div>
							<p class="mk-collapse-control"><strong>Can I purchase a gift card using points?</strong></p>
							<div class="mk-collapse-content">
							<p> 
								No. We are not offering this feature at this point in time.
							</p>
							</div>
						</div>

						<div>
							<p class="mk-collapse-control"><strong>Will I get points for the amount I have paid by redeeming points?</strong></p>
							<div class="mk-collapse-content">
							<p> 
								You will not receive any points for redeeming past points. However, if you've made only a part-payment using points, you'll get points for the payment made using other payment modes, including cash-back.
							</p>
							</div>
						</div>

						<div>
							<p class="mk-collapse-control"><strong>Will I get points for buying a gift card?</strong></p>
							<div class="mk-collapse-content">
							<p> 
							No. You will not get points for buying gift cards.
							</p>
							</div>
						</div>

						<div>
							<p class="mk-collapse-control"><strong>Will I get points for Shipping charges and Gift Packing charges?</strong></p>
							<div class="mk-collapse-content">
							<p>
								No. You will not get points for Shipping charges and Gift Packing charges.
							</p>
							</div>
						</div>

						{/if}

						<h3 id="guest-checkout" class="no-border">Placing order using guest login</h3>
						<div>
							<p>
								<ul>
									<li>
										Myntra creates a new user account, if the account with the email address doesn’t exist.
									</li>
									<li>
										In case of returns, we will credit the amount you paid for the products as cashback into your Myntra cashback account.
									</li>
								</ul>
							</p>
						</div>

					{* commented drop-offs section --Achal Sharma
					
							<h3 id="drop-offs" class="no-border">Drop Offs</h3>
						<div>
							<p class="mk-collapse-control"><strong>Can I drop-off my returns at certain locations for Myntra.com to pick-up?</strong></p>
							<div class="mk-collapse-content">
								<p>Myntra.com has just launched a pilot drop-off facility for all customers in Thane, Kalyan and Bhiwandi. Customers from these locations who would like to return their products can visit the centres listed below and send their returns to Myntra.com</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>Do I need to pay a fee for using this service?</strong></p>
							<div class="mk-collapse-content">
								<p>No. This service is completely free of charge. Your return will be received by Myntra.com within 3 business days of your drop-off. After we receive your return, you will receive Rs.100 in the form of a cashback in your Myntra.com. This amount is in addition to the cashback you will receive for the value of your returned product.</p>
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>How do I queue a return at a drop-off centre?</strong></p>
							<div class="mk-collapse-content">
								<p>Please follow this process when you visit our centre:</p>
									<ul>
									 <li>Create a self-ship return id at Myntra.com <a href="mymyntra.php?view=myreturns" target="_blank">here</a></li>
									 <li>Visit any of the centres listed below with your return product.</li>
									 <li>Provide your return id to the service representative and handover the package.</li>
									 <li>Take the acknowledgement receipt from the service representative and save it for future reference.</li>
									</ul> 
							</div>
						</div>
						<div>
							<p class="mk-collapse-control"><strong>Where are the drop-off centres located?</strong></p>
							<div class="mk-collapse-content">
								<p>The drop-off centres are located at the following addresses:</p>
								<ul>
								<li><strong>Thane</strong><br />
								 Quantium Mail Logistics Solutions (I) Pvt Ltd,<br />
								 Shop No.2, Ground Floor,<br />
								 Shanti Niwas,<br />
								 Village Chandani,<br />
								 Thane East - 400603
								</li>
								<li><strong>Kalyan</strong><br />
								 Quantium Mail Logistics Solutions (I) Pvt Ltd,<br />
								 S/001, Ground Floor,<br />
								 Kalyan Nagari Rachana Co-Op Housing Society Ltd., Sangle Wadi,<br />
								 Kalyan West, Thane - 421301
								</li>
								<li><strong>Bhiwandi</strong><br />
								 Quantium Mail Logistics Solutions (I) Pvt Ltd,<br />
								 Survey No.131/1, Shop No.14,<br />
								 Shivkrupa Society, Rahanal,<br />
								 Taluka, Bhiwandi, Mumbai - 421302
								</li>
								</ul>
							</div>
						</div>
					</div>

					 commented drop-offs section --Achal Sharma  *}

					<div class="mk-right-column fixed">
						<ul>
							<li><a href="#website">Website</a></li>
							<li><a href="#coupons">Coupons and Cashback</a></li>
							<li><a href="#payments">Payments</a></li>
							<li><a href="#shipping">Order Tracking, Shipping and Delivery</a></li>
							<li><a href="#cancel">Cancellations and Modifications</a></li>
							<li><a href="#returns">Returns</a></li>
							<li><a href="#mynt-club">Registration and Referral Program</a></li>
                                                        <li><a href="#zipdial-sms">SMS Referral Program</a></li>
							<li><a href="#gift-cards">Gift Cards</a></li>                                
							<li><a href="#gift-wrap">Gift Wrapping</a></li>
							{if $loyaltyEnabled}
							<li><a href="#my-privilege">Myntra Points</a></li>
							{/if}
					{* commented drop-offs section --Achal Sharma
							<li><a href="#drop-offs">Drop Offs</a></li>  
					commented drop-offs section --Achal Sharma *}
			    	    </ul>
						<h4><a href="">Go to Top</a></h4>
					</div>
				</div>
			</div>
		</section>
	</div>

{/block}

{block name=lightboxes}
	<div id="returns-hindi" class="lightbox infobox">
		<div class="mod">
			<div class="hd"><div class="title">Myntra वापसी नीति</div></div>
			<div class="bd body-text">
				<span style="margin-left : -10px"><img src="http://myntra.myntassets.com/skin2/images/returnspolicy_hindi.png" /></span>
			</div>
			<div class="ft">
			</div>
		</div>
	</div>
{/block}
