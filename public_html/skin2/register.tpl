
{extends file="layout.tpl"}

{block name=body}
<script>
    var pageName="register";
</script>
<div class="reg-page">
{if $loginSignupV2ABTest == 'test'}
<div id="reg-login" class="lightbox loginbox loginsignupV2">
{include file="inc/mod-loginsignupv2.tpl"}
{else}
    {if $loginSignupABTest == 'test'}
    	<div id="reg-login" class="lightbox loginbox loginsignup">
        {include file="inc/mod-loginsignupnew.tpl"}
    {else}
    <div id="reg-login" class="lightbox loginbox loginsignupold">
        {include file="inc/mod-loginsignupold.tpl"}
    {/if}
{/if} 
    </div>
</div>
{/block}

{block name="pagejs"}
<script>
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    document.write('<img src="http://ad.doubleclick.net/activity;src=2994863;type=pagev820;cat=regis144;ord=' + a + '?" width="1" height="1" alt=""/>');
</script>
<noscript>
    <img src="http://ad.doubleclick.net/activity;src=2994863;type=pagev820;cat=regis144;ord=1?" width="1" height="1" alt="">
</noscript>
{/block}

