<html>
<head>
    {include file="inc/head-meta.tpl"}
    {include file="inc/head-css.tpl"}
    {include file="inc/head-script.tpl"}
    <script src="http://myntra.myntassets.com/skin2/js/common-1346325650.js.jgz" type="text/javascript"></script
</head>
<body>
    {literal}
    <style type="text/css">
        html {
            background: none;
            border: none;
            maring: 0;
            padding: 0;
            color: #333333;
            width: 100%;
        }
        #wrapper {
            margin: 20px auto;
            width: 90%;
        }
        
        .label {
            margin: 10px 0 10px 30px;
            width: 150px;
        }
        .value {
            margin: 10px 30px 10px 10px;
            width: 100px;
            text-align: left;
        }
        
        .most-selling {
            border: 1px solid #777777;
            margin: 20px 0px;
            padding: 10px 5px;   
        }
        
        .most-selling-blinker {
            text-align:center;
            width: 98%;
        }
        
        .most-selling-text {
            margin: 20px 0px;
        }
        
        .most-selling-text li {
            margin: 10px 0px 0px 0px;
            border-bottom: 1px solid #dfdfdf;
        }
        
        .green { color: green; font-size: 20px;}
        
        .top-buyers {
            width: 40%;
            margin: 40px 0px 20px 0px;
        }
        
        .top-buyers-list {
            padding: 10px;
            border: 1px solid #777777;
        }
        
        .top-buyers-list li {
            margin: 20px 5px 0px 5px;
            border-bottom: 1px solid #dfdfdf;
        }
        
        .top-buyers-list li div.user{
            width: 65%;
        }
        
        .top-buyers-list li div.amount{
            width: 25%;
        }
        
        .top-buyers-list li div.orders{
            width: 10%;
        }
        
        .purchases {
            width: 50%;
            margin: 40px 0px 20px 0px;
        }
        
        .purchases-list {
            padding: 10px;
            border: 1px solid #777777;
            height: 1300px;
            overflow:hidden;
        }
        
        .purchases-list li {
            margin: 0px 5px 0px 5px;
            border-bottom: 1px solid #dfdfdf;
            height: 130px;
        }
        
        .purchases-list li .display {
            width: 25%;
        }
        
        .purchases-list li .title {
            margin-top: 30px;
            width: 70%;
            height: 80px;
            color: #D14747;
        }
        
        .purchases-list li .who {
            margin-top: 30px;
            width: 25%;
            height: 80px;
            margin-left: 10px;
        }
        
        #sales-image {
            position: absolute;
            top: 0px;
            left: 0px;
            opacity: 0.85;
        }
        
        #sales-image img { width: 1300px; }
        
        .summarysection {
            border: 1px solid #dfdfdf;
            font-size: 20px;
        }
        
    </style>
    <script type="text/javascript">
        function fetchRevenueSummary() {
	        $.get("myntrasalestracker.php?action=total_revenue",
	            function(response) {
	                $("#total_revenue").html(response);
	            }
	        );
	        $.get("myntrasalestracker.php?action=items_count",
                function(response) {
                    $("#total_items").html(response);
                }
            );
            $.get("myntrasalestracker.php?action=orders_count",
                function(response) {
                    $("#total_orders").html(response);
                }
            );
            $.get("myntrasalestracker.php?action=users_count",
                function(response) {
                    $("#total_users").html(response);
                }
            );
        }
        
        function fetchTopBuyers() {
            $.get("myntrasalestracker.php?action=top_5_buyers&first5KUser="+$("#first5KUser").val(),
                function(response) {
                    var topBuyers = response.topBuyers;
                    var html = '';
                    for(var i in topBuyers) {
                        var userData = topBuyers[i];
                        html += '<li>';
                        html += '<div class="user mk-f-left">'+userData.username+'</div>';
                        html += '<div class="amount mk-f-left red">Rs. '+userData.totalAmount+'</div>';
                        html += '<div class="order mk-f-right">'+userData.orders+'</div>';
                        html += '<div class="clearfix"></div>';
                        html += '</li>';
                    }
                    $("#topBuyers").html(html);
                    if($("#first5KUser").val() == '') {
                        $("#first5KUser").val(response.first5KUser);
                    }
                }
            );
        }
        
        function fetchDressBerryTopBuyers() {
            $.get("myntrasalestracker.php?action=fetchDressBerryTopBuyers",
                function(response) {
                    var topBuyers = response.topBuyers;
                    var html = '';
                    for(var i in topBuyers) {
                        var userData = topBuyers[i];
                        html += '<li>';
                        html += '<div class="user mk-f-left">'+userData.username+'</div>';
                        html += '<div class="amount mk-f-left red">Rs. '+userData.totalAmount+'</div>';
                        html += '<div class="order mk-f-right">'+userData.orders+'</div>';
                        html += '<div class="clearfix"></div>';
                        html += '</li>';
                    }
                    $("#topDressBerryBuyers").html(html);
                }
            );
        }
        
        function fetchRoadsterTopBuyers() {
            $.get("myntrasalestracker.php?action=fetchRoadsterTopBuyers",
                function(response) {
                    var topBuyers = response.topBuyers;
                    var html = '';
                    for(var i in topBuyers) {
                        var userData = topBuyers[i];
                        html += '<li>';
                        html += '<div class="user mk-f-left">'+userData.username+'</div>';
                        html += '<div class="amount mk-f-left red">Rs. '+userData.totalAmount+'</div>';
                        html += '<div class="order mk-f-right">'+userData.orders+'</div>';
                        html += '<div class="clearfix"></div>';
                        html += '</li>';
                    }
                    $("#topRoadsterBuyers").html(html);
                }
            );
        }
        
        function fetchSherSinghTopBuyers() {
            $.get("myntrasalestracker.php?action=fetchSherSinghTopBuyers",
                function(response) {
                    var topBuyers = response.topBuyers;
                    var html = '';
                    for(var i in topBuyers) {
                        var userData = topBuyers[i];
                        html += '<li>';
                        html += '<div class="user mk-f-left">'+userData.username+'</div>';
                        html += '<div class="amount mk-f-left red">Rs. '+userData.totalAmount+'</div>';
                        html += '<div class="order mk-f-right">'+userData.orders+'</div>';
                        html += '<div class="clearfix"></div>';
                        html += '</li>';
                    }
                    $("#topSherSinghBuyers").html(html);
                }
            );
        }
        
        function fetchItemsTopBuyers() {
            $.get("myntrasalestracker.php?action=fetchItemsTopBuyers",
                function(response) {
                    var topBuyers = response.topBuyers;
                    var html = '';
                    for(var i in topBuyers) {
                        var userData = topBuyers[i];
                        html += '<li>';
                        html += '<div class="user mk-f-left">'+userData.username+'</div>';
                        html += '<div class="amount mk-f-left red">'+userData.totalItems+' Item(s)</div>';
                        html += '<div class="order mk-f-right">'+userData.orders+' Order(s)</div>';
                        html += '<div class="clearfix"></div>';
                        html += '</li>';
                    }
                    $("#topItemsBuyers").html(html);
                }
            );
        }
        
        function fetchFirst20minTopBuyers() {
            $.get("myntrasalestracker.php?action=first20minTopBuyers",
                function(response) {
                    var topBuyers = response.topBuyers;
                    var html = '';
                    for(var i in topBuyers) {
                        var userData = topBuyers[i];
                        html += '<li>';
                        html += '<div class="user mk-f-left">'+userData.username+'</div>';
                        html += '<div class="amount mk-f-left red">Rs. '+userData.totalAmount+'</div>';
                        html += '<div class="order mk-f-right">'+userData.orders+'</div>';
                        html += '<div class="clearfix"></div>';
                        html += '</li>';
                    }
                    $("#first20minTopBuyers").html(html);
                }
            );
        }
        
        function fetchNext20minTopBuyers() {
            $.get("myntrasalestracker.php?action=next20minTopBuyers",
                function(response) {
                    var topBuyers = response.topBuyers;
                    var html = '';
                    for(var i in topBuyers) {
                        var userData = topBuyers[i];
                        html += '<li>';
                        html += '<div class="user mk-f-left">'+userData.username+'</div>';
                        html += '<div class="amount mk-f-left red">Rs. '+userData.totalAmount+'</div>';
                        html += '<div class="order mk-f-right">'+userData.orders+'</div>';
                        html += '<div class="clearfix"></div>';
                        html += '</li>';
                    }
                    $("#next20minTopBuyers").html(html);
                }
            );
        }
        
        function fetchLast20minTopBuyers() {
            $.get("myntrasalestracker.php?action=last20minTopBuyers",
                function(response) {
                    var topBuyers = response.topBuyers;
                    var html = '';
                    for(var i in topBuyers) {
                        var userData = topBuyers[i];
                        html += '<li>';
                        html += '<div class="user mk-f-left">'+userData.username+'</div>';
                        html += '<div class="amount mk-f-left red">Rs. '+userData.totalAmount+'</div>';
                        html += '<div class="order mk-f-right">'+userData.orders+'</div>';
                        html += '<div class="clearfix"></div>';
                        html += '</li>';
                    }
                    $("#last20minTopBuyers").html(html);
                }
            );
        }
        
        var fetch5KTimerId;
        function fetch5Kin15minsPurchasers() {
            $.get("myntrasalestracker.php?action=5kin15mins",
                function(response) {
                    var purchasers = response.buyers;
                    var html = '';
                    for(var i in purchasers) {
                        var userData = purchasers[i];
                        html += '<li>';
                        html += '<div class="user mk-f-left">'+userData.username+'</div>';
                        html += '<div class="amount mk-f-left red">Rs. '+userData.totalAmount+'</div>';
                        html += '<div class="order mk-f-right">'+userData.orders+'</div>';
                        html += '<div class="clearfix"></div>';
                        html += '</li>';
                    }
                    $("#fast5KBuyers").html(html);
                    if(response.timeover) {
                        clearInterval(fetch5KTimerId);
                        var html = '';
                        html += '<li><div class="user">'+response.winner1+'</div></li>';
                        html += '<li><div class="user">'+response.winner2+'</div></li>';
                        html += '<li><div class="user">'+response.winner3+'</div></li>';
                        $("#fast5KBuyersWinners").html(html);
                    }
                }
            );
        }
        
        var fetch10KTimerId;
        function fetch15Kin30minsPurchasers() {
            $.get("myntrasalestracker.php?action=15kin30mins",
                function(response) {
                    var purchasers = response;
                    var html = '';
                    for(var i in purchasers) {
                        var userData = purchasers[i];
                        html += '<li>';
                        html += '<div class="user mk-f-left">'+userData.username+'</div>';
                        html += '<div class="amount mk-f-left red">Rs. '+userData.totalAmount+'</div>';
                        html += '<div class="order mk-f-right">'+userData.orders+'</div>';
                        html += '<div class="clearfix"></div>';
                        html += '</li>';
                        $("#fast15KBuyers").html(html);
                    }
                }
            );
        }
        
        function fetchMostSellingSku() {
            $.get("myntrasalestracker.php?action=trending",
                function(response) {
                    var html = '';
                    for(var key in response) {
                        var product = response[key];
                        html += '<li>';
                        html += '<div class="mk-f-left" style="width: 20%; text-align:center; color: #F7931E;">'+key+'</div>';
                        html += '<div class="mk-f-left imagebox" style="width:25%;"><img src="'+product.imagepath+'"></div>';
                        html += '<div class="mk-f-left red" style="width: 53%; text-align:center; margin: 30px 0px 0px 5px;">'+product.style_name+'</div>';
                        html += '<div class="clearfix"></div>';
                        html += '</li>';
                    }
                    $(".most-selling-text").html(html);
                }
            );
        }
        
        var tickerInitialized = false;
        var tickerTimerId ;
        function fetchLatestPurchases() {
            $.get("myntrasalestracker.php?action=latest_purchases&lastitemid="+$("#lastScannedItemId").val(),
                function(response) {
                    var lastItemId = '';
                    for(var i in response) {
                        var product = response[i];
                        var html = '';
                        html += '<li><a href="http://www.myntra.com/'+product.style_id+'">';
                        html += '<div class="mk-f-left display"><img src="'+product.imagepath+'"></div>';
                        html += '<div class="mk-f-left title">'+product.stylename+'</div>';
                        //html += '<div class="mk-f-left who">'+product.user+'</div>';
                        html += '<div class="clearfix"></div>';
                        html += '</a></li>';
                        $("#ticker").append(html);
                        lastItemId = product.itemid;
                    }      
                    if(lastItemId != '') {
                        $("#lastScannedItemId").val(lastItemId);
                    }     
                    if($("#ticker").children().length > 10 && tickerInitialized == false) {
                        tickerTimerId = setInterval(function(){ tick () }, 4000);
                        tickerInitialized = true;
                    } else if($("#ticker").children().length <= 10 && tickerTimerId) {
                        clearInterval(tickerTimerId);
                        tickerInitialized = false;
                        tickerTimerId = null;
                    }
                }
            );
        }
        
        function tick(){
            $('#ticker li:first').slideUp( function () { $(this).appendTo($('#ticker')).remove(); });
        }
        
        function tickTrending(){
            var first = $('.most-selling-text li:first').html();
            $('.most-selling-text li:first').slideUp( function () { $(this).remove(); });
            $('.most-selling-text').append(first);
        }
        
        var visible = true;
        /*
        var imageTogglerTimerId;
        function toggleBackgroundImage() {
            if(visible) {
                visible = false;
                $("#sales-image").fadeOut(3000, function () { $("#sales-image").css("opacity", "0.85"); });
                clearInterval(imageTogglerTimerId);
                imageTogglerTimerId = setInterval(toggleBackgroundImage, 180000);
            } else {
                visible = true;
                $("#sales-image").fadeIn(3000, function () { $("#sales-image").css("opacity", "0.85"); });
                clearInterval(imageTogglerTimerId);
                imageTogglerTimerId = setInterval(toggleBackgroundImage, 10000);
            }
        }
        */
        var fastestBuyerTimerId;
        function fetchFirst5KUser() {
             $.get("myntrasalestracker.php?action=fastestBuyer",
                function(response) {
                    if(response != '') {
                        clearInterval(fastestBuyerTimerId);
                        fastestBuyerTimerId = null;
                    }
                }
             );
        }
        
        $(document).ready(function(){
            fetchRevenueSummary();
            fetchTopBuyers();
            fetchMostSellingSku();
            //fetchLatestPurchases();
            fetchFirst20minTopBuyers();
            fetchNext20minTopBuyers();
            fetchLast20minTopBuyers();
            fetchDressBerryTopBuyers();
            fetchRoadsterTopBuyers();
            fetchSherSinghTopBuyers();
            fetchItemsTopBuyers();
            setInterval(fetchRevenueSummary, 30000);
            setInterval(fetchTopBuyers, 60000);
            setInterval(fetchMostSellingSku, 90000);
            setInterval(fetchFirst20minTopBuyers, 90000);
            setInterval(fetchNext20minTopBuyers, 90000);
            setInterval(fetchLast20minTopBuyers, 90000);
            setInterval(fetchDressBerryTopBuyers, 100000);
            setInterval(fetchRoadsterTopBuyers, 110000);
            setInterval(fetchSherSinghTopBuyers, 120000);
            setInterval(fetchItemsTopBuyers, 130000);
            //setInterval(fetchLatestPurchases, 10000);
            //fetch5KTimerId = setInterval(fetch5Kin15minsPurchasers, 60000);
            //fetch10KTimerId = setInterval(fetch15Kin30minsPurchasers, 60000);
            //fastestBuyerTimerId = setInterval(fetchFirst5KUser, 60000);
            //imageTogglerTimerId = setInterval(toggleBackgroundImage, 180000);
            setInterval(function(){ tickTrending () }, 4000);
        });
        
    </script>
    {/literal}
    <div id="wrapper">
        <input type=hidden name="lastScannedItemId" id="lastScannedItemId" value="">
        <input type=hidden name="first5KUser" id="first5KUser" value="">
        <h1 class="mk-f-left">
            <a href="/"><img src="{$cdn_base}/skin2/images/logo.png" alt="Myntra" title="Online Shopping store for Branded Shoes, Clothing, Accessories & Fashion wear in India." /></a>
        </h1>
        <div class="mk-f-right clearfix summarysection">
            <div class="mk-f-left"><div class="mk-f-left label">Total Orders:</div><div id="total_orders" class="mk-f-left value red">&nbsp;</div></div>
            <div class="mk-f-left"><div class="mk-f-left label">Total Items:</div><div id="total_items" class="mk-f-left value red">&nbsp;</div></div>
            <div class="clearfix"></div>
            <div class="mk-f-left"><div class="mk-f-left label">Total Revenue:</div><div id="total_revenue" class="mk-f-left value red">&nbsp;</div></div>
            <div class="mk-f-left"><div class="mk-f-left label">Total Users:</div><div id="total_users" class="mk-f-left value red">&nbsp;</div></div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        
        <div class="mk-f-left purchases">
            <!--
            <h3 class="green"><blink>Current Purchases</blink></h3>
            <ul id="ticker" class="purchases-list">
			</ul>
			-->
			<h3 class="green"><blink>Top 5 Buyers</blink></h3>
            <ul class="top-buyers-list" id="topBuyers">
            </ul>
            <br><br>
			<h3 class="green"><blink>First 20 mins - Top 5 buyers</blink></h3>
            <ul class="top-buyers-list" id="first20minTopBuyers">
            </ul>
            <br><br>
            <h3 class="green"><blink>Next 20 mins - Top 5 buyers</blink></h3>
            <ul class="top-buyers-list" id="next20minTopBuyers">
            </ul>
            <br><br>
            <h3 class="green"><blink>Last 20 mins - Top 5 buyers</blink></h3>
            <ul class="top-buyers-list" id="last20minTopBuyers">
            </ul>
            <br><br>
            <h3 class="green"><blink>Top buyers - DressBerry - 45 mins</blink></h3>
            <ul class="top-buyers-list" id="topDressBerryBuyers">
			</ul>   
			<br><br>         
            <h3 class="green"><blink>Top buyers - Roadster - 45 mins</blink></h3>
            <ul class="top-buyers-list" id="topRoadsterBuyers">
			</ul>   
			<br><br>         
            <h3 class="green"><blink>Top buyers - Sher Singh - 45 mins</blink></h3>
            <ul class="top-buyers-list" id="topSherSinghBuyers">
			</ul>   
			<br><br>         
            <h3 class="green"><blink>Top buyers - No of Items - 45 mins</blink></h3>
            <ul class="top-buyers-list" id="topItemsBuyers">
			</ul>
        </div>
        <div class="mk-f-right top-buyers">
            <div class="most-selling red">
                 <blink class="most-selling-blinker mk-f-left green" style="border-bottom: 1px solid #dfdfdf;">TOP 10 TRENDING</blink>
                <div class="clearfix"></div>
                <ul class="most-selling-text">
                </ul>
                <blink class="most-selling-blinker mk-f-left green">TOP 10 TRENDING</blink>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</body>
</html>