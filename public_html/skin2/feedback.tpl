{include file="inc/doctype.tpl"}
<html>
    <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# {$facebook_app_name}: http://ogp.me/ns/fb/{$facebook_app_name}#">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        {include file="inc/head-meta.tpl"}
        {include file="inc/head-css.tpl"}
        {include file="inc/head-script.tpl"}
        {block name=pagecss}{/block}

        <style type="text/css">
            .mk-wrapper {
                width: 100%;
            }
            .MFB_wrapper_outer {
                width: 100% !important;
            }
            .feedback-block .feedback-form {
                width: 100%;
            }
            .MFBQ_wrapper.jqtransformdone {
                max-width: 600px;
                margin: 0 auto 20px !important;
            }
            ul.MFB_option_wrapper {
                width: 100%;
            }
            ul.MFB_option_wrapper li {
                width: 100%;
                margin-right: 0 !important;
            }
            table.jqTransformTextarea {
                width: 100%;
            }
            table.jqTransformTextarea tr > td:nth-child(2) {
                width: 100% !important;
            }
            table.jqTransformTextarea tr > td:nth-child(2) > div {
                width: 100% !important;
            }
            table.jqTransformTextarea tr > td:nth-child(2) > div textarea {
                width: 100% !important;
            }
        </style>
    </head>
    <body>
        {if !$isGTMLazyLoadEnabled}
            {include file="inc/gtm.tpl"}
        {/if}
        <div class="mk-body{if $layoutVariant eq 'test'} mynt-layout-new{/if}">
            <div class="mk-wrapper">
                <section class="mk-site-main">
                    <div class="feedback-block mk-cf">
                        <table cellspacing="0" cellpadding="0" style="width: 100%;">
                            <tr>
                                <td style="text-align: left; padding: 10px 20px 0">
                                    <a href="http://www.myntra.com/">
                                        <img src="http://myntra.myntassets.com/myx/images/myntra.41f8417dd06ef6309718fee381004886ea6171fc.png"  />
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <hr />
                        <div class="feedback-form">
                            {include file="feedbackForm.tpl"}
                        </div>
                    </div>
                </section>
                {include file="inc/touts.tpl"}
                {block name=searchdescription}{/block}
            </div>
        </div>
        {block name=lightboxes}{/block}
        {if not $login and $currentPage neq "/register.php"}
            {include file="inc/modal-signup.tpl"}
        {/if}
        {if $login and $showNewSignUpConfirmation}
            {include file="inc/modal-signupconfirmation.tpl"}
        {/if}
        {include file="inc/modal-combo-overlay.tpl"}
        {include file="inc/modal-quick-look.tpl"}
        {include file="inc/body-css.tpl"}
        {include file="inc/body-script.tpl"}
        
        <script type="text/javascript">
            var MFB = $.parseJSON('{$MFB_JSON}');
        </script>
    </body>
</html>