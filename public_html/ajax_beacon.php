<?php
require_once 'auth.php';
include_once($xcart_dir."/Profiler/Profiler.php");
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";
include_once "$xcart_dir/include/class/abtest/MABTest.php";

$loginlog = $logger_manager->getLogger('MYNTRALOGINLOGGER');
$userEmail = filter_input(INPUT_GET, 'email', FILTER_SANITIZE_STRING);
$count = filter_input(INPUT_GET, 'count', FILTER_SANITIZE_NUMBER_INT);
$type = filter_input(INPUT_GET, 'type', FILTER_SANITIZE_STRING);
global $_MABTestObject;

$secureLoginFeatureGate=\FeatureGateKeyValuePairs::getBoolean('secure.login');
$secureLoginABTest=$_MABTestObject->getUserSegment("secureSignin");
if($secureLoginABTest == 'secure' && $secureLoginFeatureGate){
	$secureLoginEnabled = 1; 
}
else{
	$secureLoginEnabled = 0; 
}
if($type == 'signin'){
	if($secureLoginEnabled){
		Profiler::increment("secure-signin-timeout-error");
	}
	else{
		Profiler::increment("signin-timeout-error");	
	}
	
}else if($type == 'signup'){
	if($secureLoginEnabled){
		Profiler::increment("secure-signup-timeout-error");
	}
	else{
		Profiler::increment("signup-timeout-error");
	}

}

$loginlog->debug('Email ==>'.$userEmail.'   AttemptNo==>'.$count.'    type==>'.$type .'     UserAgent==>'.$_SERVER['HTTP_USER_AGENT'].'       IP==>'.$_SERVER["REMOTE_ADDR"].'    Secure==>'.$secureLoginEnabled);

$response = array('status' => 'SUCCESS', 'message'=>'logged');
header('Content-Type:application/json');
echo json_encode($response);
exit;

