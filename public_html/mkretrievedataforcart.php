<?php
require "./auth.php";
include_once("./checkOnlineUser.php");
if( isOnlineUser() )
{
        include_once("./mkretrievedataforcart_re.php");
        exit;
}
require_once("class.imageconverter.php");
include_once("./include/func/func.mkcore.php");
include_once("./include/func/func.mkmisc.php");
include_once("./include/func/func.mkspecialoffer.php");
include_once("./include/func/func.randnum.php");
include_once("./include/func/func.mkimage.php");

$chosenProductConfiguration = $XCART_SESSION_VARS['chosenProductConfiguration'];
$masterCustomizationArray = $XCART_SESSION_VARS['masterCustomizationArray'];

if($chosenProductConfiguration["defaultCustomizationAreaId"]){
$defaultCustId = $chosenProductConfiguration["defaultCustomizationAreaId"];
}
if($chosenProductConfiguration["defaultCustomizationId"]){
$defaultCustId = $chosenProductConfiguration["defaultCustomizationId"];
}
//Count number of  customization area
// Only Tshirt case is having two customization area later it will extend as the case would extend    
$custAreaCount = 0;
$j=0;
foreach($chosenProductConfiguration[productCustomizationAreas] as $key=>$value)
{
	 if($masterCustomizationArray[$value[0]]['pasteimagepath'] != "" || $masterCustomizationArray[$value[0]]['customizedtext'] != "" || $masterCustomizationArray["paste_image_".$j] != "")
	 {
        $custAreaCount = $custAreaCount + 1;
     }
     else 
     {
           //$custAreaCount = 0;	
     }
     $j++;
}

      
//$custAreaCount = count($masterCustomizationArray);

if(isset($chosenProductConfiguration['productId']) && !empty($chosenProductConfiguration['productId']))
{
     $defaultCustomizationAreaImageDetail = func_load_product_customized_area_rel_details($chosenProductConfiguration['productId'], $defaultCustId);
	 $ImagePath =   $defaultCustomizationAreaImageDetail[0]['image_portal_T'];
}
if($chosenProductConfiguration['productId'] == "")
{  
	//generate the final images for customization area
	generateFinalImages($masterCustomizationArray,$chosenProductConfiguration);
	x_session_register('masterCustomizationArray');	
    $masterCustomizationArray;
    if($masterCustomizationArray[$defaultCustId]['pasteimagepath']  || $masterCustomizationArray[$defaultCustId]['customizedtext'] )
	 	$ImagePath =   $masterCustomizationArray[$defaultCustId][$defaultCustId]['finalcustomizeThumbimage'];
    else
    {
        $curcustomizationareaid = $chosenProductConfiguration['currentCustomizationAreaId'];
        $ImagePath =   $masterCustomizationArray[$curcustomizationareaid][$curcustomizationareaid]['finalcustomizeThumbimage'];
    }
}

//Load type of product type
$productStyleId =         $chosenProductConfiguration['productStyleId'];
$productStyleTable = $sql_tbl["mk_product_style"];
$queryString = func_query_first("select a.styletype as type,price  from $productStyleTable a where a.id='$productStyleId'");
$productStyleType = $queryString['type'];  
######## Modified for Ibibo #########
$actualPriceOfProduct =   $queryString['price'] ;
$productId =              $chosenProductConfiguration['productId'];
$productTypeId = 		  $chosenProductConfiguration['productTypeId'];
$productStyleName =       $chosenProductConfiguration['productStyleLabel'];
$quantity =               $chosenProductConfiguration['quantity']; 
$productPrice =           $chosenProductConfiguration['unitPrice'];  
$productTypeLabel =       $chosenProductConfiguration['productTypeLabel'];
$designImagePath =        $ImagePath;// $chosenProductConfiguration['uploadedImagePath'];
$optionsArray =           $chosenProductConfiguration['optionsArray'];
$sizenamesArray =		  $chosenProductConfiguration['sizename'];
$sizequantityArray =	  $chosenProductConfiguration['sizequantity'];
$totalPrice =       $quantity *   $productPrice;
$optionsNameArray = array_keys($optionsArray);
$optionsValueArray = array_values($optionsArray);
$optionsId= $chosenProductConfiguration['optionsId'];
$template_id = $chosenProductConfiguration['template_id'];
$addTo = $_GET['at'];
// if product Id is empty means new product is being added to the cart
if (empty($productId)){
    //generate a unique id for the created product
    //TODO - to be changed later a good unique id generator program.
	$len = 4;
	$time = time();
	$timefirstdigit = substr($time, 0, 3);
	$timelastdigit = substr($time, -3);
    $productId = $timefirstdigit.get_rand_number($len).$timelastdigit;
}
$productTypeData = func_query_first("SELECT location, vat FROM $sql_tbl[mk_product_type] WHERE id='$productTypeId'");
$discount = $quantity * func_get_special_offer_discount_for_product($productId);

$productDetail =   array('productId' => $productId,'producTypeId' => $productTypeId
,'productStyleId' => $productStyleId,'productStyleName' => $productStyleName
,'quantity'=> $quantity ,'productPrice'=>$productPrice,'productTypeLabel' => $productTypeLabel
, 'totalPrice'=>$totalPrice,'optionNames'=>$optionsNameArray,'optionValues'=>$optionsValueArray
,'designImagePath'=>$designImagePath,'discount'=>$discount,'optionsId'=>$optionsId
,'defaultCustomizationId'=>$defaultCustId,'custAreaCount'=>$custAreaCount,
'productStyleType'=>$productStyleType,'sizenames'=>$sizenamesArray,'sizequantities'=>$sizequantityArray,
'actualPriceOfProduct' => $actualPriceOfProduct, 'operation_location' => $productTypeData['location'],
'vatRate'=>$productTypeData['vat'],'template_id'=>$template_id);


if(!empty($chosenProductConfiguration['promotion_id'])){
	if(($promotions = verify_promotion_tracking_id($chosenProductConfiguration['promotion_id'])) !== false ){
		$productDetail['promotion'] = $promotions;
	}
}

(!empty($chosenProductConfiguration['yahootpl'])) ? $productDetail['yahootpl']=$chosenProductConfiguration['yahootpl'] : '' ;

#declare an array
$productids = array();   

if(!x_session_is_registered('productsInCart'))
{
    
    x_session_register('productsInCart'); 
      
}
if(!x_session_is_registered('productids'))
{
    //$productids = array();   
    x_session_register($productids);  
}
$productsInCart = $XCART_SESSION_VARS['productsInCart'];
//commented to fix#1251 - dont overwrite. See below for details
//$productsInCart[$productId] = $productDetail;

/**
 * Added by VenuGopal Annamaneni
 * Fix for - https://node9.cvsdude.com/bugz/xmukesh/show_bug.cgi?id=1251
 * Comments : Product in the cart is being overwritten if another order for the same product is placed
 * 
 * Fix : Instead of copying $productDetail in $productsInCart[$productId],  check
 * if $productId is already there and modify the cart such that it has both old & new details 
 */
 
if( $productsInCart[$productId] ) // product already there in cart
{
	$oldProductInCart = $productsInCart[$productId];
	$productDetail['totalPrice'] += $oldProductInCart['totalPrice'];
	$productDetail['quantity'] += $oldProductInCart['quantity'];
	$productDetail['discount'] = $quantity * func_get_special_offer_discount_for_product($productId);
	//merge or add sizequantities
	for($i=0;$i<count($oldProductInCart['sizequantities']);$i++)
	{
		$productDetail['sizequantities'][$i] = intval($productDetail['sizequantities'][$i]) + intval($oldProductInCart['sizequantities'][$i]);
	}
}
$productsInCart[$productId] = $productDetail;
 /* Fix #1251- Ends Here  */

if(!x_session_is_registered('grandTotal'))
{
    
    x_session_register('grandTotal'); 
      
}  

x_session_register('productsInCart');  
$productids = $XCART_SESSION_VARS['productids'];


if(!in_array($productId, $productids)){
	$productids[] = $productId;
	x_session_register('productids');
}




$affiliateInfo = $XCART_SESSION_VARS['affiliateInfo'];
if(empty($affiliateInfo[0]['affiliate_id']) )
{
	$cartArray['cart'] = $productsInCart;
	$cartArray['products'] = $productids;
	$cartitems = serialize($cartArray['cart']);
	$cartproducts = serialize($cartArray['products']);
	$uuid = stripslashes(getMyntraCookie('MYNTRA_SHOPPING_ID'));
	if($uuid=="")
	{
		$uuid = mysql_result(mysql_query('Select UUID()'),0);
		$expiryDate= time() + $max_cart_validity_time;
		setMyntraCookie("MYNTRA_SHOPPING_ID",$uuid,$expiryDate,'/',$cookiedomain);
		setMyntraCookie("MYNTRA_UNQ_COOKIE_ID",substr($uuid,1,20),$expiryDate,'/',$cookiedomain);
	}
	store_cart_products($uuid,$login,$productsInCart,$productids);
}
#################################################################
for($i =  0; $i < count($productids) ;  $i++)
{
    if($productids[$i] ==$productId  )
    {
        $exist = true;
        break;
    }
}
if($exist != true){    
	$productids[] = $productId;
	x_session_register('productids');
}

$pagetype = $_GET['pagetype'];

header("location:mkmycartmultiple.php?at=".$addTo."&pagetype=".$pagetype);
?>
