<?php
include_once("./auth.php");
include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalClient.php";
require_once $xcart_dir."/include/class/class.mymyntra.php";
include_once "$xcart_dir/include/func/func.utilities.php";

if ($telesales){
    echo "";exit;
}

if(empty($login)){
	$resp = array(
		'status'  => 'ERROR_PROMPT_LOGIN',
		'content' => ""
    );
	header('Content-Type: application/json');
	echo json_encode($resp);
	exit;
}
$orderid = filter_input(INPUT_POST, 'orderid', FILTER_SANITIZE_NUMBER_INT);
$itemid = filter_input(INPUT_POST, 'itemid', FILTER_SANITIZE_NUMBER_INT);
$_token = filter_input(INPUT_POST, '_token', FILTER_SANITIZE_STRING);
$resp = array();

if (!checkCSRF($_token)){
	$resp = array('status' => 'ERROR','content' => '');
	echo json_encode($resp);
	exit;
}
$mymyntra = new MyMyntra(mysql_real_escape_string($login));
$result = $mymyntra->confirmGetLoyaltyPoints($login,$itemid, $orderid);
if(!empty($result['status']) && $result['status']['statusType']== 'SUCCESS'){
	$resp = array(
	    'status' => 'ok',
	    'content' => ""
	);
}else{
	$resp = array('status' => 'ERROR','content' => '');
	$errorlog->error("Get Loyalty points Error !!! Result : $result");
}

header('Content-Type: application/json');
echo json_encode($resp);
exit;