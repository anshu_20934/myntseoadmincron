<?php
return;//to fix x5 login problem
use enums\pageconfig\PageConfigSectionConstants;

use enums\pageconfig\PageConfigLocationConstants;

use pageconfig\manager\CachedBannerManager;

use abtest\MABTest;

global $xcart_dir;
include_once($xcart_dir."/include/class/widget/class.widget.php");
require_once("$xcart_dir/include/solr/solrProducts.php");

// Top Navigation data
$widget = new Widget(WidgetTopNavigationV3::$WIDGETNAME);
$widget->setDataToSmarty($smarty);

	function getGlobalBrands(){
		$solrProducts = new SolrProducts();

		$global_brands = getBrandsFromQuery('*:*');

		return $global_brands;
	}
	
	function getBrandsFromQuery($query, $ranges = array(0 => array("rangeText" => "A - D", "solrQuery" => "brands_filter_facet:[A TO E] OR brands_filter_facet:[a TO e] OR brands_filter_facet:[0 TO 9]"),
						1 => array("rangeText" => "E - L", "solrQuery" => "brands_filter_facet:[E TO M] OR brands_filter_facet:[e TO m]"),
						2 => array("rangeText" => "M - P", "solrQuery" => "brands_filter_facet:[M TO Q] OR brands_filter_facet:[m TO q]"),
						3 => array("rangeText" => "Q - Z", "solrQuery" => "brands_filter_facet:[Q TO *] OR brands_filter_facet:[q TO *]")
						)){
							
		$dontShowOOSProducts = FeatureGateKeyValuePairs::getFeatureGateValueForKey("dontShowOOSProducts");
		if((!empty($dontShowOOSProducts) && $dontShowOOSProducts == 'true'))
			$query = $query. " count_options_availbale:[1 TO *]";
										
		$solrProducts = new SolrProducts();
		$brandsPresent = array();
		foreach ($ranges as $key=>$value){
			$tmpRes = $solrProducts->multipleFacetedSearch($query . " AND (" . $value['solrQuery'] .")" ,array("brands_filter_facet"),0,0,1,"index")->facet_fields;
			$tmpArr = get_object_vars($tmpRes->brands_filter_facet);
			uksort($tmpArr,strcasecmp);
			$brandsPresent[$key]['result']['brands_filter_facet'] = $tmpArr;
			$brandsPresent[$key]['range'] = $value['rangeText'];
			$brandsPresent[$key]['count'] = count((array)$brandsPresent[$key]['result']['brands_filter_facet']);
			
		}
		return $brandsPresent;
	}



$global_brands = json_decode($xcache->fetch($xcart_http_host . '-' . "global_brands"), true);
if($global_brands == NULL || $nocache == 1){
    $global_brands = getGlobalBrands();
    if($nocache != 1){//Do not store in case nocache param is set
   		$xcache->store($xcart_http_host . '-' . "global_brands", json_encode($global_brands));
    }
}

//var_dump($layoutVariant);
$layoutVariant = MABTest::getInstance()->getUserSegment('NewLayout');
if($layoutVariant == 'test') {
	//get navbar categories
	$data = $widget->getData();
	
	//array to store navbarimage links
	$navbarSlideShowImages = array();
	$navbarStaticImages = array();
	foreach ($data as &$value){
		//fetch the slideshow images from TopNav
		$navbarSlideShowImages[$value["link_name"]] = CachedBannerManager::getCachedBanners(PageConfigLocationConstants::TopNavImages, PageConfigSectionConstants::SlideshowImage, $value["link_name"]);
		$navbarStaticImages[$value["link_name"]] = CachedBannerManager::getCachedBanners(PageConfigLocationConstants::TopNavImages, PageConfigSectionConstants::StaticImage, $value["link_name"]);
	}
	
	$smarty->assign("navbarSlideShowImages", $navbarSlideShowImages);
	$smarty->assign("navbarStaticImages", $navbarStaticImages);
	
}

$smarty->assign("global_brands", $global_brands->brands_filter_facet);


