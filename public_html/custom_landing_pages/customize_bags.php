<?php
include_once("../auth.php");
include_once("../include/func/func.mkcore.php");
include_once("../include/func/func.stylegroups.php");

//load all product types for bags group
$group_id = 13;//bags groupid
$product_types = load_product_types_forgroup($group_id);

$selected_type_name = ($_GET['type'])? $_GET['type']:'bags';
$selected_type_name = ($selected_type_name == 'bags')? 'laptop-bags-backpacks': $selected_type_name;
$selected_type_name = strtolower(str_replace('-',' ',$selected_type_name));
$default_type_info = func_query_first("select id as type_id,default_style_id,name,description,type_h1,type_title,type_keywords,type_metadesc from mk_product_type where lower(name)='{$selected_type_name}'");
$selected_type_id = $default_type_info['type_id'];
$selected_type_description = $default_type_info['description'];
$selected_type_h1 = $default_type_info['type_h1'];
$selected_type_title = $default_type_info['type_title'];
$selected_type_keywords = $default_type_info['type_keywords'];
$selected_type_metadesc = $default_type_info['type_metadesc'];
$default_style_id_for_type = $default_type_info['default_style_id'];

//load groupids based on selected type
$groupids = load_groups_for_producttype($selected_type_id);//separated by '_'

//load styles and style group info based on selected type's groupids
if(!empty($groupids)){
	$groupids = str_replace("_",",",$groupids);	
	$style_group_info = load_styles_group_info_forgroups($groupids);	
	$style_group_info_multilevel = array();
	foreach($style_group_info as $key=>$val){
		$style_group_info_multilevel[$val['style_group_name']][] = $val;	
	}
}

//load all customization areas information for the default style id
$style_cust_info = load_custareas_forstyle($default_style_id_for_type);
foreach($style_cust_info as $key=>$val){
	if($val['isprimary']==1){
		$default_area_image = $val['image_l']; 
		break;
	}		
}

//breadcrumb
$breadCrumb=array(
				array('reference'=>"$http_location","title"=>"home"),
                array('reference'=>"mkcreateproduct.php",'title'=>'create product'),
				array('reference'=>'#','title'=>'<div style="float:left;"><h1 style="font-size:13px;font-weight:normal;margin-top:0px;">'.$selected_type_name.'</h1></div>')
			);
$smarty->assign("breadCrumb",$breadCrumb);
$smarty->assign("product_types",$product_types);
$smarty->assign("selected_type_id",$selected_type_id);
$smarty->assign("selected_type_name",$selected_type_name);
$smarty->assign("selected_type_desc",$selected_type_description);
$smarty->assign("pageTitle",$selected_type_title);
$smarty->assign("metaKeywords",$selected_type_keywords);
$smarty->assign("metaDescription",$selected_type_metadesc);
$smarty->assign("selected_type_h1",$selected_type_h1);
$smarty->assign("default_style_id",$default_style_id_for_type);
$smarty->assign("style_group_info",$style_group_info_multilevel);
$smarty->assign("style_cust_info",$style_cust_info);
$smarty->assign("default_area_image",$default_area_image);
func_display("custom_landing_skins/customize_bags.tpl", $smarty);
?>