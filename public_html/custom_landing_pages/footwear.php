<?php
include_once("../auth.php");

// Inisialize the tracker
include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
$tracker = new BaseTracker(BaseTracker::LANDING_PAGE);

// Update the tracker
$tracker->fireRequest();

func_display("custom_landing_pages/footwear.tpl", $smarty);
?>