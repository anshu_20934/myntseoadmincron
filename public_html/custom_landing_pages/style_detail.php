<?php
include_once("../auth.php");
include_once("../include/func/func.mkcore.php");

$styleid = $_GET['style_id'];

//load style description
$query = "select description from mk_product_style where id='{$styleid}'";
$style_description = func_query_first_cell($query);

//load all customization areas information for the default style id
$query = "select style_id,name as custarea_name,isprimary,image_t,image_l 
			from mk_style_customization_area where style_id='$styleid'";
$style_cust_info = func_query($query);

$other_cust_areas = '';
if(!empty($style_cust_info)){
	foreach($style_cust_info as $key=>$val){
		if($val['isprimary']==1){
			$default_area_image = $val['image_l'];
			$other_cust_areas .= "<li class='selected-thumb'><a href='{$http_location}/{$val['image_l']}'><img src='{$http_location}/{$val['image_t']}' width='32'></a></li>";
							
		} else {
			$other_cust_areas .= "<li><a href='{$http_location}/{$val['image_l']}'><img src='{$http_location}/{$val['image_t']}' width='32'></a></li>";
		
		}
			
	}	
}

$cust_image = "{$http_location}/{$default_area_image}";

//return cust image,cust areas and style description   
echo "$cust_image##$other_cust_areas##$style_description";
?>