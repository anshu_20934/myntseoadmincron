<?php
include_once("../auth.php");
include_once($xcart_dir."/include/class/widget/headerMenu.php");

// Inisialize the tracker
include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
$tracker = new BaseTracker(BaseTracker::LANDING_PAGE);

// Update the tracker
$tracker->fireRequest();

//Most Popular tags
$widget = new Widget(WidgetMostPopular::$WIDGETNAME);
$widget->setDataToSmarty($smarty);

func_display("custom_landing_pages/paris-calling-contest.tpl", $smarty);
?>