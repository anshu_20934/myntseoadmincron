<?php
include_once($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");
include_once($xcart_dir."/include/func/func.mkcore.php");
/*
 * PORTAL-2415:Kundan: this file has been majorly changed
 * We had 2 Size unification Systems: old and new. 
 * The old one existed from eternity and the new one was primarily written by Kumar Anand
 * The old unification is grossly inadequate and lots of times, misleading too: showing wrongly mapped sizes. 
 * We have completely gotten rid of non-brand specific unification.
 * Pre this ticket, we were getting ordering of sizes based on old size unification (size_unification_mapping table)
 * This is changed now. We get ordering based on the table mk_all_size_order
 * This table is going to be removed sometime soon.  
 * This is done as a part of PORTAL-2415
*/  
class Size_unification{
	
	public static function cmpSortOrder($a, $b) {
	    if ($a['sortorder'] == $b['sortorder']) {
	        return 0;
	    }
	    return ($a['sortorder'] < $b['sortorder']) ? -1 : 1;
	}
	
	/*
	 * To get the unified sizes for a styleid
	 * Todo : Refine the category related call
	 */
	public static function getUnifiedSizeByStyleId($styleid, $datatype='string', $getalloptions=false, $callApi=true, $returndata=''){
		$checkInStock=FeatureGateKeyValuePairs::getBoolean('checkInStock',true);
		$fetchall = (!$checkInStock || $getalloptions);
		$sizeOptions = func_load_product_options($styleid, 0, $fetchall, true, $callApi);
		return  self::getUnifiedSizeForOptions($sizeOptions, $styleid, $datatype, $returndata);
	}
	
	private static function getUnifiedSizeForOptionsNew($sizeOptions,$styleid,$datatype='string',$returndata='')
	{
		if(empty($sizeOptions) || empty($sizeOptions[0])) {
			return "";
		}
		$options = implodeArrayOfAssocs($sizeOptions, "id");
		$query = "	select val.size_value as unifiedSize,
				  	opt.id as id,opt.value as originalSize,opt.AllSizes as allsize
				  	from mk_product_options as opt
				  	left join size_chart_scale_value as val
				  	on val.id = unified_size_value_id
					where opt.id in ($options)
					order by intra_scale_size_order asc";
		$unifiedSizeResults=func_query($query);
		
		$unifiedSizes = "";
		$unifiedsizeArray = array();
		$allSizeArray =array();
		for($i=0;!empty($unifiedSizeResults[$i]);$i++) {
			 if(empty($unifiedSizeResults[$i]["unifiedSize"])) {	
			 	//Kundan: PORTAL-2677: but now, make unification strict. If any size options is not-unified, then return blank
			 	return "";			
			 	/* 
			 	//If Unified Size Options is blank,then get the original size from size-options
			 	$id = $unifiedSizeResults[$i][id];
				$query = "select value,id from mk_product_options where id =$id and name in ('Size','Kids Size');";
				//print_r($query);
				$sizeResult=func_query($query);
				if(empty($sizeResult[0][value])) {
					continue;
				}
				else {
					if($isFirstSizeOption == false) {
						$unifiedSizes = $unifiedSizes.",";
					} 
					else {
						$isFirstSizeOption = false;
					}
					$unifiedSizes = $unifiedSizes.$sizeResult[0][value];
					$unifiedsizeArray[$unifiedSizeResults[$i][originalSize]] = $sizeResult[0][value];
					$allSizeArray[$unifiedSizeResults[$i][originalSize]] =$unifiedSizeResults[$i][allsize];
				} */
			}
			else {
				if($i > 0) {
					$unifiedSizes .= ",";
				}
				$unifiedSizes = $unifiedSizes.$unifiedSizeResults[$i]["unifiedSize"];
				$originalSize = $unifiedSizeResults[$i]["originalSize"];
				$unifiedsizeArray[$originalSize] = $unifiedSizeResults[$i]["unifiedSize"];
				$allSizeArray[$originalSize] = $unifiedSizeResults[$i]["allsize"];
			}
		}
		
		if($returndata == "tooltip"){
			return $allSizeArray;
		}
		if($datatype == "array"){
			return $unifiedsizeArray;
		}
		return $unifiedSizes;
	}
	public static function getSizeGroup($styleid)
	{
		$query = "select groupName from mk_style_properties as sty,mk_articletype_size_group_mapping as map
		where style_id = $styleid and map.article_type=global_attr_article_type";
		$result=func_query($query,true);
		$value = $result[0][groupName];
		return $value;
	}
	public static function getUnifiedSizeForOptions($sizeOptions, $styleid, $datatype, $returndata='') {
		$sizeOptionsAfterNewUnification = self::getUnifiedSizeForOptionsNew($sizeOptions,$styleid,$datatype,$returndata);
		//Kundan: if new size unification results in partially unified sizes, then we should assume they've not got unified
		if(!empty($sizeOptionsAfterNewUnification)) {
			return $sizeOptionsAfterNewUnification;
		}
		if(empty($sizeOptions)){
			return ($datatype==="array")? array(): "";
		}	
		//Kundan: No tool tip if style is not unified as per new unification.
		if($returndata === 'tooltip'){
			return "";
		}
		$unsortedSizes = array();
		//Kundan: remove old unification. Just add support for sorting of sizes based on mk_all_size_order
		$allSizesInClause="";
		foreach($sizeOptions as $eachSize){
			$sizeValue = $eachSize["value"];
			$unsortedSizes[$sizeValue] = $sizeValue;
			$allSizesInClause .= "'".$sizeValue."',";
		}
		$allSizesInClause = substr($allSizesInClause, 0, strlen($allSizesInClause)-1);
		
		//$sql = "select @rownum:=@rownum+1 as sortorder,o.stringsize as size from mk_all_size_order o,(SELECT @rownum:=0) r where stringsize in ($allSizesInClause) order by intorder ";
		//$sortedSizes = getResultsetAsPrimaryKeyValuePair($sql, "size","sortorder");
		
		$sql = "select stringsize as sizekey,stringsize as sizevalue from mk_all_size_order where binary stringsize in ($allSizesInClause) order by intorder";
		$sortedSizes = getResultsetAsPrimaryKeyValuePair($sql, "sizekey","sizevalue");
		if(sizeof($sortedSizes) < sizeof($unsortedSizes)){
			//this is possible if one or more size options are missing in mk_all_size_order
			$sortedSizes = $unsortedSizes;
			//in this case, we don't want to miss any size option to be displayed to user.
			//even if the ordering is incorrect
		}
		if($datatype == "array"){
			return $sortedSizes;
		}else {
			return implode(",", array_keys($sortedSizes));
		}
	}
	
	/*
	 * Update mapping in solr on Add/Save/Delete a size mapping
	 */
	public static function updateMappingInSolr($size, $gender){
		
		$sql="select style_id from mk_style_properties where global_attr_gender='$gender' and style_id in (select style from mk_product_options where value='$size')";
		//$sql = "SELECT style FROM mk_product_options where value='$size'";
	    
		$styleids = func_query($sql);
		
		if(!empty($styleids)){
			$solrProducts = new SolrProducts();
			$solrProducts->addStylesToIndex($styleids);
		}

	}
	
	public static function ifSizesUnified($sizeOptions){
		if(!empty($sizeOptions)){
			foreach($sizeOptions as $key => $value){
				$key=strtoupper(preg_replace('/\s+/', '', $key));
				if($key == $value){
					return false;
				}
			}
		}else{
			return false;
		}
		return true;
	}
	
	public static function getSizeScale($styleid){
		if(empty($styleid))return null;
		$query="select value from mk_product_options where style=$styleid limit 1";
		$value=func_query_first_cell($query);
		$value=preg_replace("/[^A-Z]/","",strtoupper($value));
		return $value;
	}
	
	public static function getSizeScaleFromOption($sizeOption){
		if(empty($sizeOption))return null;	
		$value=preg_replace("/[^A-Z]/","",strtoupper($sizeOption));
		return $value;
	}
	
	public function getToolTipSizes($sizename,$gender) {
		return $this->_tooltipSizeMapping[$sizename][$gender];
	}
	
	public function getIndianSize($sizename,$gender) {
		return $this->_sizeOptionsMapping[$sizename][$gender];
	}
	
	public function ifSizeOptionUnificationSizes($productTypeLabel, $brand_name, $sizes){
		
		if(!empty($sizes)){
			if(strpos($sizes,"US") === 0){
				$sizeSpecUsed="US";
			}
			else if(strpos($sizes,"UK") === 0){
				$sizeSpecUsed="UK";
			}
			else if(strpos($sizes,"EURO") === 0){
				$sizeSpecUsed="EURO";
			}		
			else{
				$sizeSpecUsed="INDIAN";
			}
		}
		else{
			return false;
		}
		if(strpos($sizes,".") || $sizeSpecUsed=="INDIAN"){
			return false;
		}
		
		if($productTypeLabel == "Shoe" || $productTypeLabel == "Sandals" || $productTypeLabel == "Slippers" || $productTypeLabel == "Formal Shoes" || $productTypeLabel == "Sports Shoes" || $productTypeLabel == "Casual Shoes"){
			if($brand_name != "Crocs" && $brand_name != "Newfeel" && $brand_name != "Decathlon"){
				return true;
			}
		}
		return false;
	}
	
	public function ifSizeOptionUnification($productTypeLabel, $brand_name){
		
		if($productTypeLabel == "Shoe" || $productTypeLabel == "Sandals" || $productTypeLabel == "Slippers" || $productTypeLabel == "Formal Shoes" || $productTypeLabel == "Sports Shoes" || $productTypeLabel == "Casual Shoes"){
			if($brand_name != "Crocs" && $brand_name != "Newfeel" && $brand_name != "Decathlon"){
				return true;
			}
		}
		return false;
	}
}
?>
