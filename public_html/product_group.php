<?php
require "./auth.php";
// What group was asked?
$group_id = $_GET['id'];

// Featured stores ..
$featuredstores = func_query("select * from mk_featured_stores order by display_order limit 3");

// Requested group ..
$group = func_select_first("mk_product_group", array('id' => $group_id));

// Product types in group ..  
//product type 3 means it will display all the styles of perticular producttypegroup
if($group['type']==3)
	$product_types = func_query("SELECT a.id,a.name,a.product_type,b.image_path AS image_t,a.price,a.description FROM mk_product_style a,mk_style_images b WHERE a.styletype='P' AND a.is_active=1 AND a.id=b.styleid AND b.type='T' AND  a.product_type IN (SELECT pt.id  FROM mk_product_type pt  WHERE pt.product_type_groupid = {$group_id} AND pt.is_active = '1' AND pt.type = 'P' )") ;
else
	$product_types = func_query("select pt.*, ps.name as style_name from mk_product_type pt, mk_product_style ps  where pt.product_type_groupid = {$group_id} and pt.is_active = '1' and pt.type = 'P' and pt.default_style_id = ps.id") ;


$selectedPageIndex = $XCART_SESSION_VARS['selectedPageIndex'];
// Bad idea .. Not good for Google .. 
if ($selectedPageIndex == 3) {
    $prev = array('reference' => "{$http_location}/mkcreateproduct.php", 'title' => 'create product');
} else if ($selectedPageIndex == 4) {
    $shopid = strip_tags($_GET['shopid']);
    $shopid = sanitize_int($shopid);
    if (isset($shopid)) {
        $smarty->assign("qryStringKey", 'shopid');
        $smarty->assign("shopid", $shopid);
        $smarty->assign("qryStringValue", $shopid);
    }
    $prev = array('reference' => "{$http_location}/mkstartsellinghome.php", 'title' => 'start selling');
    
}


// Breadcrumb
$breadCrumb = array(
    array('reference' => "$http_location", "title" => "home"),
    $prev,
    array('title' => '<div style="float:left;"><h1 style="font-size:11px;margin-top:0px;">' . $group['name'] . '</h1></div>')
);

$smarty->assign ("metaDescription",$group['seo_description']);
$smarty->assign ("pageTitle",$group['seo_title']);
$smarty->assign ("metaKeywords",$group['seo_keywords']);

$smarty->assign("group", $group);
$smarty->assign("selectedIndex", $selectedPageIndex);
$smarty->assign("breadCrumb", $breadCrumb);

$smarty->assign("product_types", $product_types);
$smarty->assign("featuredstores", $featuredstores);

func_display("product_group/view.tpl", $smarty);

?>
