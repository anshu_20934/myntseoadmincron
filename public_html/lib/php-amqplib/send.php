<?php

require_once( 'amqp.inc');

$connection = new AMQPConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();



$msg = new AMQPMessage('Hello World!');
$channel->basic_publish($msg, '', 'sampleQueue');

echo " [x] Sent 'Hello World!'\n";

$channel->close();
$connection->close();

?>
