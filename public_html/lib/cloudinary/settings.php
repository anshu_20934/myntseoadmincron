<?php

\Cloudinary::config(array(
    "cloud_name" => CloudinaryConfig::$cloudName,
    "api_key" => CloudinaryConfig::$apiKey,
    "api_secret" => CloudinaryConfig::$apiSecret
));

?>
