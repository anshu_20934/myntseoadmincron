<?php
include_once("./auth.php");
include_once("$xcart_dir/utils/http/HttpClient.class.php");

$method = $_POST['method'];
$data = $_POST['data'];
$url = $_POST["url"];
$user = $_POST["user"];
$weblog->debug("asyncGetPostHandler Request $url $method ".$data);
if($method === "POST"){
	$response = HttpClient::quickPost($url,$data,$user,'');
}
$weblog->debug("asyncGetPostHandler Response".$response);
exit;