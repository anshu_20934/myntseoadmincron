<?php
//************************************************************ /
// INCLUDE ALL FILES REQUIRED                                  /
//************************************************************ /
include_once("./auth.php");
include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
include_once($xcart_dir."/include/func/func.mkcore.php");
require_once($xcart_dir."/include/func/func.utilities.php");
require_once($xcart_dir."/include/class/widget/class.static.functions.widget.php");
include_once($xcart_dir."/include/class/search/UserInterestCalculator.php");
include_once($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");
include_once($xcart_dir."/include/class/notify/class.notify.php");
$publickey  = "6Le_oroSAAAAANGCD-foIE8DPo6zwcU1FaW3GiIX";
$privatekey = "6Le_oroSAAAAAKuArW6iPxhOlk0QVJMwWOTGVQnt";
include_once($xcart_dir."/include/func/func.utilities.php");
require_once($xcart_dir."/tracking/trackingUtil.php");
require_once($xcart_dir."/modules/discount/DiscountEngine.php");
require_once("$xcart_dir/include/class/search/SearchProducts.php");
include_once($xcart_dir."/Cache/Cache.php");
require_once("$xcart_dir/socialInit.php");
include_once "$xcart_dir/modules/vat/VatCalculator.php";
use abtest\MABTest;
use style\builder\CachedStyleBuilder;
use style\builder\StyleBuilder;
use revenue\payments\service\PaymentServiceInterface;

$tracker = new BaseTracker(BaseTracker::MINI_PIP);

$shownewui= $_MABTestObject->getUserSegment("productui");
$productuiABparam=$shownewui;
/**EXPRESS BUY****/
$expressBuyEnabled = false;
if(!empty($login)){
	$expressBuyEnabled = PaymentServiceInterface::showExpressBuyForUser($login);
}
$smarty->assign('expressBuyEnabled',$expressBuyEnabled);	
/******EXPRESS BUY****/

$smarty->assign("shownewui", $shownewui);
$smarty->assign("productuiABparam", $productuiABparam);

$pdpColorGrouping = FeatureGateKeyValuePairs::getBoolean('pdp.colorgrouping.enabled');
$smarty->assign("pdpColorGrouping", $pdpColorGrouping);

if(isset($HTTP_GET_VARS["id"]) ) {
	$productStyleId=filter_input(INPUT_GET,"id", FILTER_SANITIZE_NUMBER_INT);
}

if(isset($HTTP_GET_VARS["fc"])) {
    $fromCart = (int)$HTTP_GET_VARS["fc"];
    $smarty->assign("fromcart", $fromCart);
	$smarty->assign("combouid", $combouid);
}

$productStyleId=sanitize_int($productStyleId);

$cachedStyleBuilderObj=CachedStyleBuilder::getInstance();
$styleObj=$cachedStyleBuilderObj->getStyleObject($productStyleId);

//color group styles selection
$relatedStylesDetailsByGroupId = $styleObj->getStyleIdDetailsForColorGroup();
$relatedColorStyleDetailsArray = array();
if (!empty($relatedStylesDetailsByGroupId)){
	foreach ($relatedStylesDetailsByGroupId as $value)
	{	if ($value["styleid"] != $productStyleId){
			
			$articletype = $value['global_attr_article_type'];
			$brandname = $value['global_attr_brand'];
			$stylename = $value['stylename'];
			$styleid = $value['styleid'];
			$landingpageurl = construct_style_url($articletype,$brandname,$stylename,$styleid);
			$relatedColorStyleDetailsArray[$styleid] = array(
				"base_color" => $value['global_attr_base_colour'],
				"color1" => $value['global_attr_colour1'],
				"url" => $landingpageurl,
				"image" => $value['search_image']
			);
		}
		else
		{
			$smarty->assign('styleBaseColor',$value['global_attr_base_colour']);
			$smarty->assign('styleColor1',$value['global_attr_colour1']);
		}
	}
}

if(!empty($relatedColorStyleDetailsArray)){
	$smarty->assign('relatedColorStyleDetailsArray',$relatedColorStyleDetailsArray);
}



// validate productStyleid

if($styleObj == NULL){
	$XCART_SESSION_VARS['errormessage']="Requested page was not found on this server";
    func_header_location("mksystemerror.php");
    exit;
}
$style_properties= $styleObj->getStyleProperties();
$smarty->assign("style_properties",array("0"=>$style_properties));
$isStyle = true;

//visual tag
$producttags = explode('|',$style_properties["product_tags"]);
$producttags[1] = trim($producttags[1]);
if (!empty($producttags[1])) {
	$producttags = explode(':', $producttags[1]);
	$producttags[0] = strtolower(trim($producttags[0]));
	$producttags[1] = strtolower(trim($producttags[1]));
	if(!empty($producttags[0]) && !empty($producttags[1])) {
		$smarty->assign('visualtagText', $producttags[0]);
		$smarty->assign('visualtagCssClass', $producttags[1]);
	}	
}

// ***************************************************************  /
// Start Filling Smarty data to load view		                    /
// ***************************************************************  /

$productStyleDetails=$styleObj->getProductStyleDetails();

$styleiddetail=$styleObj->getStyleIdDetails();

$productTypeId=strip_tags($styleiddetail["product_type"]);
$productTypeLabel=strip_tags($styleiddetail["name"]);

$currentPageURL=currentPageURL();
$prettyURL=strrpos($currentPageURL,'?');
if(!empty($prettyURL)){
	$pageCleanURL=	substr($currentPageURL,0,strrpos($currentPageURL,'?'));
}
else {
	$pageCleanURL=$currentPageURL;
}
$smarty->assign("pageURL",$pageCleanURL);

//NEW SIZE CHART
$dynamicSizeChart=FeatureGateKeyValuePairs::getFeatureGateValueForKey('sizechart.dynamicSizeChart');
$smarty->assign("dynamicSizeChart",$dynamicSizeChart);

// load size chart image
//encode single quotes to %27 if any
$size_chart_image_path = str_replace("'", "%27", $productStyleDetails["size_chart_image"]);
$isCustomizable = $productStyleDetails["is_customizable"];
$smarty->assign("size_chart_image_path",$size_chart_image_path);

$enableAllStyles=FeatureGateKeyValuePairs::getBoolean('enableAllStyles');
if(!$enableAllStyles){
	$row_disableStyle = $styleObj->getProductStyleCount();
}
$disableProductStyle = false;

$myntraUser = false;
$pos = strpos($login, "@myntra.com");
if($pos !== false)
	$myntraUser = true;

$mplstyles = explode(",", trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('mpl.styles')));
if(empty($row_disableStyle) &&(!$enableAllStyles) || ( in_array($productStyleId, $mplstyles) && !$myntraUser ) ) {
	$disableProductStyle = true;
	$disableMessage =  	"This product is currently out-of-stock. ";
	$smarty->assign("disableProductStyle",$disableProductStyle);
	$smarty->assign("disableMessage",$disableMessage);
}

//AB test for new checkout flow /Used for new Combo page as well
$smarty->assign("checkout", MABTest::getInstance()->getUserSegment('checkout'));


$productStyleLabel = $productStyleDetails["label"];
$descriptionAndDetails = $productStyleDetails["description"];
// Check if there is any discount applied.
$originalprice = $productStyleDetails["price"];

$smarty->assign("originalprice", $originalprice);
$smarty->assign("descriptionAndDetails",$descriptionAndDetails);
$smarty->assign("productTypeType",$productStyleDetails['type']);
$smarty->assign("productTypeLabel",$productTypeLabel);
$smarty->assign("productStyleDetails",$productStyleDetails);
$smarty->assign("productStyleLabel",$productStyleLabel);
$smarty->assign("productStyleId",$productStyleId);
$smarty->assign("productTypeId",$productTypeId);

// Load all product options
$allProductOptionDetails=$styleObj->getAllProductOptions();
$productOptionDetails=$styleObj->getActiveProductOptionsData();
$inactiveProductOptionDetails=$styleObj->getInactiveProductOptionsData();
$productOptionNameValues=$styleObj->getProductOptionNameValues();
$discountedprice = $originalprice;

$discountData= $styleObj->getDiscountData();
$discountType = $discountData->discountType;
$discountAmount = $discountData->discountAmount;

foreach($discountData->discountToolTipText->params as $k=>$v) {
    $smarty->assign("dre_".$k, $v);
}
foreach($discountData->displayText->params as $k=>$v) {
    $smarty->assign("dre_".$k, $v);
}

$discountSetStyles = $discountData->discountToolTipText->relatedProducts;

if($discountSetStyles!=null && count($discountSetStyles)>0 ) {
    $smarty->assign("show_combo", true);
}else{
    $smarty->assign("show_combo", false);
}

$smarty->assign("discountamount", $discountAmount);
$smarty->assign("discountId", $discountData->discountId);
$smarty->assign("discounttype", $discountType);
$smarty->assign("discountlabeltext", $_rst[$discountData->displayText->id]);
$smarty->assign("discounttooltiptext", $_rst[$discountData->discountToolTipText->id]);


$finalprice  = ($discountedprice>0) ? $discountedprice : $originalprice;

$defaultImage = $style_properties['default_image'];
$areanames = array('default', 'top', 'front', 'back', 'right', 'left', 'bottom');

$area_icons =  array();

foreach($areanames as $singlearea) {
	if(!empty($style_properties["$singlearea" . "_image"]) && $style_properties["$singlearea" . "_image"]!="$cdn_base/") {
		$singleareaproperties = array();
		$imagepath = $style_properties["$singlearea" . "_image"];
			
		$singleareaproperties["image"] = $imagepath;
		$singleareaproperties["name"] = $singlearea;
		
		// With new rectangular images in places, area icons are 48x64 pixels.
		$singleareaproperties["area_icon"] = str_replace('_images.', '_images_48_64.', $imagepath);
			
		$area_icons[] = $singleareaproperties;
	}
}
	
$smarty->assign("defaultImage",$defaultImage);
$smarty->assign("area_icons", $area_icons);
$smarty->assign("pageTitle", $style_properties['product_display_name']." | Myntra");
$smarty->assign("productDisplayName", $style_properties['product_display_name']);
$smarty->assign("objectType","product");

// For Facebook metaDescription.
$meta = 'Buy ' . $style_properties['product_display_name'] . ' online at Rs. ' . $finalprice . ' . ';
$meta.= ' In stock and ready to ship. All India Free Shipping and Cash On Delivery available';

// Fetching related products to show on right column		
if(!empty($style_properties['product_tags'])) {
	$producttags= explode(",",$style_properties['product_tags']);
}

if(!empty($producttags)) {

	$keywords = array();
	foreach($producttags as $t) {
		$keywords[] = trim($t);
	}
		
	$keywords [] = 'sportswear';
	$keywords = array_merge($keywords, explode(" ",$style_properties['product_display_name']));
			
	$smarty->assign("metaKeywords", implode(",", $keywords));
		
	$meta = 'Buy ' . $style_properties['product_display_name'].' online at Rs. ';
			
	if($discountedprice>0) {
		$meta.= "$discountedprice";
	} else {
		$meta.= "$originalprice";
	}
}

$buynowdisabled=false;
if(empty($productOptionDetails)) {
	$buynowdisabled=true;
	$smarty->assign("buynowdisabled", true);
	$meta.= '. In stock and ready to ship. All India Free Shipping and Cash On Delivery available';
}else{
	$buynowdisabled=false;
	$smarty->assign("buynowdisabled", false);
}

$smarty->assign("metaDescription", $meta);


if($isStyle && !empty($productStyleId)) {
	//UserInterestCalculator::trackAction($productStyleId, UserInterestCalculator::PRODUCT_VIEW);
}
	
$smarty->assign("isreturninguser", ($expiry_time-time())/(30*24*60*60)<=14?"returning":"new");

$sizeSpecUsed="";

$flattenSizeOptions=false;
$gender=$style_properties["global_attr_gender"];
$brand_name=$style_properties["global_attr_brand"];

$brand_filters=$styleObj->getBrandData();
if(!empty($brand_filters['logo'])) {
$smarty->assign("brand_logo", $brand_filters['logo']);
}
$smarty->assign("brandname", $brand_name);
if(empty($gender) || $gender == "NA"){
	$gender="Men";
}

//Retrieving data from mk_catalogue_classification for the product
$catalogData = $styleObj->getCatalogueClassificationData();
$articleType = $catalogData["articletype"];
$masterCategory = $catalogData["master_category"];
$stylePropertiesdata = $styleObj->getStyleProperties();
$sizeRepresentationImageUrl=$stylePropertiesdata["size_representation_image_url"];

$sizeOptions=$styleObj->getSizeUnificationDataForAvailableOptions();
$allSizeOptions=$styleObj->getSizeUnificationDataForAllOptions();
// not a db call
$flattenSizeOptions = Size_unification::ifSizesUnified($sizeOptions);
$flattenSizeOptionsAll = Size_unification::ifSizesUnified($allSizeOptions);
$allSizeKeys=array_keys($allSizeOptions);

foreach ($inactiveProductOptionDetails as $row){
	$unavailabeOptions[$row['value']]=$row;
	if($flattenSizeOptionsAll){
		$unavailabeOptions[$row['value']]['unified']=$allSizeOptions[$row['value']];
	}
}

$sizeOptionsTooltip=$styleObj->getSizeOptionsTooltipData();

$sizeOptionSKUMapping = array();
$sizeOptionsTooltipSKUMapping = array();
$allSizeOptionSKUMapping =array();
$allSizeOptionsTooltipSKUMapping = array();
$ShowSuppliedByPartner = false;
foreach ($sizeOptions as $key=>$value) {
	foreach ($productOptionDetails as $options) {
		if($options['value'] == $key) {
			$sizeOptionSKUMapping[$options['sku_id']] = $value;
			$sizeOptionsTooltipSKUMapping[$options['sku_id']] = $sizeOptionsTooltip[$key];
		}
	}
}

foreach ($allSizeOptions as $key=>$value) {
	foreach ($allProductOptionDetails as $options) {
		if($options['value'] == $key) {
			$isAvailable = ($options['sku_count'] > 0 && !empty($options['is_active']));
			$allSizeOptionSKUMapping[$options['sku_id']] = array($value,$isAvailable,$options['sku_count'],array(
					'availableInWarehouses'  => $options['availableInWarehouses'],
				    'leadTime' => $options['leadTime'],
				    'supplyType' => $options['supplyType']
				));
			$allSizeOptionsTooltipSKUMapping[$options['sku_id']] = $sizeOptionsTooltip[$key];
			$ShowSuppliedByPartner = ($options['supplyType'] === 'JUST_IN_TIME' ? true : $ShowSuppliedByPartner);
		}
	}
}


$sizeOptionsTooltipHtmlArray=array();

if($flattenSizeOptions){
	foreach ($allSizeOptionSKUMapping as $idx => $sizeopt){
		$sizeopt_breakdown=explode(",", $allSizeOptionsTooltipSKUMapping[$idx]);
		$str="";
			$str="<table>";
			foreach($sizeopt_breakdown as $sizeopt_breakdown_idx=>$sizeopt_breakdown_val){
				$str .="<tr>";
				if((strpos($sizeopt_breakdown_val, "/") === false) && (strpos($sizeopt_breakdown_val, "-") === false)){
					$alpha_val=ereg_replace("[^A-Z]", "", $sizeopt_breakdown_val);
					$numeric_val=ereg_replace("[^0-9.]", "", $sizeopt_breakdown_val);
					if(!empty($alpha_val)){
						$str .= "<td>" . ($alpha_val == 'UK'?'UK/IN':$alpha_val) . "</td>";
					}
					if(!empty($numeric_val)){
						$str .= "<td>" . $numeric_val . "</td>";
					}
				}
				else{
					$str .= "<td>" . $sizeopt_breakdown_val . "</td>";
				}
				$str .="</tr>";
			}
			$str .= "</table>";
		$sizeOptionsTooltipHtmlArray[$idx]=$str;
		$tooltipTop=count($sizeopt_breakdown) * 25 + 25;
		$smarty->assign("tooltipTop",$tooltipTop);
	}
}

$sizeScale;
if($flattenSizeOptions)
	$sizeScale=Size_unification::getSizeScaleFromOption($allSizeKeys[0]);
$smarty->assign("sizeScale",$sizeScale);
$smarty->assign("unavailableOptions",$unavailabeOptions);

//Featuregates for sizechart and tooltip
$tooltipFlag=FeatureGateKeyValuePairs::getBoolean('PDPShowTooltip');
$PDPShowTooltip=$flattenSizeOptions && $tooltipFlag;
$renderTooltip=$PDPShowTooltip;	
$renderSizeChart=!$PDPShowTooltip;

//retargeting tracking code
$retargetPixel = retargetingPixelOnType($productTypeId);
if(!empty($retargetPixel)){
    $smarty->assign("retargetPixel",$retargetPixel);
}    


$socialExcludeArray=addSocialExcludeToCache();
$socialExclude=checkSocialExclude($style_properties["global_attr_article_type"],$socialExcludeArray);
$smarty->assign('socialExclude',$socialExclude);

$smarty->assign("sizeOptionSKUMapping",$sizeOptionSKUMapping);
$smarty->assign("ShowSuppliedByPartner",$ShowSuppliedByPartner);
$smarty->assign("allSizeOptionSKUMapping",$allSizeOptionSKUMapping);
$smarty->assign("renderTooltip",$renderTooltip);
$smarty->assign("renderSizeChart",$renderSizeChart);
$smarty->assign("sizeSpecUsed",$sizeSpecUsed);
$smarty->assign("gender",$gender);
$smarty->assign("sizeOptionsMapping",$sizeOptionsMapping);
$smarty->assign("sizeOptions",$sizeOptions);
$smarty->assign("sizeOptionsTooltipHtmlArray",$sizeOptionsTooltipHtmlArray);
$smarty->assign("productOptions",$productOptionNameValues);
$smarty->assign("flattenSizeOptions",$flattenSizeOptions);
$smarty->assign("notifymefeatureon",FeatureGateKeyValuePairs::getBoolean(Notifications::$_CUSTOMER_INSTOCK_NOTIFYME.'.enable')==true);
$smarty->assign('articleType',$articleType);
$smarty->assign("notifymefeatureon",FeatureGateKeyValuePairs::getBoolean(Notifications::$_CUSTOMER_INSTOCK_NOTIFYME.'.enable')==true);
//FOR EXPRESS BUY BUTTON TEXT
$smarty->assign('expressBuyLabel', WidgetKeyValuePairs::getWidgetValueForKey("expressbuy.label"));

if(!is_null($tracker->pdpData)){
	$tracker->pdpData->setStyleId($productStyleId);
	$tracker->pdpData->setBrand($brand_name);
	$tracker->pdpData->setArticleType($articleType);
}
//AVAIL
$avail_dynamic_param = "$gender,$masterCategory";
$smarty->assign("avail_dynamic_param",$avail_dynamic_param);
if(!empty($_GET['t_code'])){
$smarty->assign("t_code",filter_input(INPUT_GET, 't_code', FILTER_SANITIZE_STRING));
}

$vatEnabled = FeatureGateKeyValuePairs::getBoolean('vat.enabled');
if($vatEnabled) {
    $vatThreshold = VatCalculator::getVatOnStyle($brand_name, $productStyleDetails["product_type"], $catalogData["article_id"]);
    if($discountPercent >= $vatThreshold) {
        $vatMessage = FeatureGateKeyValuePairs::getFeatureGateValueForKey('vat.pdp.message');
        $smarty->assign("showVatMessage", true);
        $smarty->assign("vatMessage", $vatMessage);
    }
    $vatCouponMessage = FeatureGateKeyValuePairs::getFeatureGateValueForKey('vat.pdpcoupon.message');
    $smarty->assign("vatThreshold",$vatThreshold);
    $smarty->assign("vatCouponMessage", $vatCouponMessage);
    $smarty->assign("vatEnabled",$vatEnabled);
}

$tracker->fireRequest();
$smarty->assign("brand",$brand_name);
$smarty->assign("category",$masterCategory);
$smarty->assign("SoldByVendorText",WidgetKeyValuePairs::getWidgetValueForKey("SoldBy_Vendor_Text"));
$smarty->assign("SuppliedByPartnerTooltip",WidgetKeyValuePairs::getWidgetValueForKey("SuppliedBy_Partner_Tooltip"));
if($skin=='skin2'){
echo trim($smarty->fetch("inc/miniPIP.tpl"));
}
else{
echo trim(func_display("product_new_tpl/miniPIP.tpl", $smarty));
}

