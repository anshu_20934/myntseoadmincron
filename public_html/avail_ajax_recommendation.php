<?php
define("QUICK_START", 1);
use abtest\MABTest;
include_once("./auth.php");
include_once "$xcart_dir/include/class/widget/class.widget.skulist.php";
include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");

$page = filter_input(INPUT_POST, 'page', FILTER_SANITIZE_STRING);
$styleId = filter_input(INPUT_POST, 'styleId', FILTER_SANITIZE_NUMBER_INT);
$recommendationType = filter_input(INPUT_POST, 'recommendationType', FILTER_SANITIZE_STRING);

$json = filter_input(INPUT_POST, 'json', FILTER_SANITIZE_STRING);

$json = json_decode(html_entity_decode($json,ENT_COMPAT), true);

if(empty($json['similar']['styleids'])){
	echo 0;
	exit;
}

switch($page)
{
	case "home":
		$avail_title="";
		break;
	case "pdp" :
		$avail_title="Other Customers Recommend";
		break;
	case "search":
		$avail_title="Other Customers Recommend";
	    break;
	case "cart":
		$avail_title="You Might Also Be Interested In";
		break;
	default:
		$avail_title="You May Also Like";
		break;

}

$returnArr = array();

$crossSellabtest = MABTest::getInstance()->getUserSegment('crossSell');

foreach ($json as $k=>$set) {
	global $shownewui;
	$styles_to_show = array();
	foreach ($set['styleids'] as $style) {
		$styles_to_show[] = $style[0];
	}
	
	if ($crossSellabtest == "test")
	{
		if($page=='pdp'){
			if($set['tabid'] == 1)
			{
				$styles_to_show = array_slice($styles_to_show,0,5);
				$tracker = new BaseTracker(BaseTracker::RECO_WIDGET);
				if($k=='similar'){
					if(!is_null($tracker->recommendationData)){
						$tracker->recommendationData->setStyleId($styleId);
						$tracker->recommendationData->setPage($page);
						$tracker->recommendationData->setRecommendationType($recommendationType);
						$tracker->recommendationData->setRecommendationStyleIds($styles_to_show);
					}

					$tracker->fireRequest();

					$previousProductStyleId = $styleId;
					$previousRecommendationType = $recommendationType;
					$previousRecommendationStyleIds = implode(",",$styles_to_show);
				}

			}
			elseif ($set['tabid'] == 2)
				$styles_to_show = array_slice($styles_to_show,-5);
		}
	}

	$tabName = $set['tabid'] == 2 ? 'cross_sell_widget' : 'recommend_widget';  
	
	
	$styleWidget = new WidgetSKUList('avail recommendation', $styles_to_show);
	$widgetData = $styleWidget->getData();	
	$smarty->assign('page',$page);
	$smarty->assign('tracking_code',$set['trackingcode']);
	$smarty->assign("avail_recommendation",$widgetData);
	$smarty->assign("avail_title",$avail_title);
	$smarty->assign("tabName", $tabName);
	
	$smarty->assign("previousProductStyleId",rawurlencode($previousProductStyleId));
	$smarty->assign("previousRecommendationType",rawurlencode($previousRecommendationType));
	$smarty->assign("previousRecommendationStyleIds",rawurlencode($previousRecommendationStyleIds));
	
	if($page=='addedtocart'){
		$div_content=$smarty->fetch("inc/matchmaker-horizontal.tpl", $smarty);
	}
	elseif($page=='cart'){
		$smarty->assign('cartpage','true');
		$div_content=$smarty->fetch("inc/matchmaker-horizontal.tpl", $smarty);
	}
	else{
		$div_content = $smarty->fetch("inc/matchmaker-items.tpl", $smarty);
	}	
	$returnArr[$set['tabid']] = $div_content;
}
echo json_encode($returnArr);
?>