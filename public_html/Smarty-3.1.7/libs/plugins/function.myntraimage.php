<?php

/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     function.myntraimage.php
 * Type:     function
 * Name:     myntraimage
 * Purpose:  convert normal image to myntraimage
 * -------------------------------------------------------------
 */

use imageUtils\dao\JpegminiDBAdapter;
use imageUtils\Jpegmini;
function smarty_function_myntraimage($params, &$smarty)
{
    global $weblog;
    $key = $params["key"];
    $src = $params["src"];
    $doc_root=$_SERVER['DOCUMENT_ROOT'];
//  $weblog->debug("myntraimage : Fetching myntraimage with key : $key and src : $src");

    require_once "$doc_root/auth.php";
    require_once "$doc_root/include/class/cache/XCache.php";

	global $weblog;
//	$weblog->debug("myntraimage : Key to fetch is : $key");
	$adapter = new imageUtils\dao\JpegminiDBAdapter();
	$config =  $adapter->getConfig();
    
    if(!empty($config[$key])){
        $enabled = $config[$key];
        if($enabled){
            $src = Jpegmini::getCompressedImagePathFromOriginalPath($src);
        }
    }
//  $weblog->debug("myntraimage : Returning $src for key:$key and src:$src");
    return $src;
    

}
?>
