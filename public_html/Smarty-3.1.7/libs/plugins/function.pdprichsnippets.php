<?php

/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     function.myntraimage.php
 * Type:     function
 * Name:     myntraimage
 * Purpose:  convert normal image to myntraimage
 * -------------------------------------------------------------
 */

use richsnippets\PDPRichSnippets;

function smarty_function_pdprichsnippets($params, &$smarty)
{
    $richSnippets = new PDPRichSnippets($params['macrosObj']);

    $rs_itemscope_product_start='';
    $rs_itemprop_brand='';
    $rs_itemprop_name='';
    $rs_itemprop_image='';
    $rs_itemprop_breadcrumb='';
    $rs_itemprop_description='';
    $rs_itemprop_currency='';
    $rs_itemprop_price='';
    $rs_itemscope_offer_start='';

    if($richSnippets->getIsEnabled()){
        $rs_itemscope_product_start='itemscope itemtype="http://data-vocabulary.org/Product"';
        $rs_itemprop_brand='itemprop="brand"';
        $rs_itemprop_name='itemprop="name"';
        $rs_itemprop_image='itemprop="image"';
        $rs_itemprop_breadcrumb='itemprop="breadcrumb"';
        $rs_itemprop_description='itemprop="description"';
        $rs_itemprop_currency='<meta itemprop="currency" content="INR" />';
        $rs_itemprop_price='<meta itemprop="price" content="'.$richSnippets->getDisplayFinalPrice().'"/>';
        $rs_itemscope_offer_start='itemprop="offerDetails" itemscope itemtype="http://data-vocabulary.org/Offer"';
        }
    $smarty->assign('rs_itemscope_product_start',$rs_itemscope_product_start);
    $smarty->assign('rs_itemprop_brand',$rs_itemprop_brand);
    $smarty->assign('rs_itemprop_name',$rs_itemprop_name);
    $smarty->assign('rs_itemprop_image',$rs_itemprop_image);
    $smarty->assign('rs_itemprop_breadcrumb',$rs_itemprop_breadcrumb);
    $smarty->assign('rs_itemprop_description',$rs_itemprop_description);
    $smarty->assign('rs_itemprop_currency',$rs_itemprop_currency);
    $smarty->assign('rs_itemprop_price',$rs_itemprop_price);
    $smarty->assign('rs_itemscope_offer_start',$rs_itemscope_offer_start);
    
}
?>
