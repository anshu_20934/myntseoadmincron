<?php
require_once("auth.php");
include_once("$xcart_dir/include/class/mcart/class.MCart.php");
include_once("$xcart_dir/include/class/mcart/class.MCartFactory.php");

$mcartFactory = new MCartFactory();
$cart = $mcartFactory->getCurrentUserCart();
if($_POST['mode'] == 'removeGift'){
	$cart->setIsGiftOrder(false);
	$cart->setGiftCharge(0);
	//$cart->setGiftMessage("");
	$cart->setAsChanged();
	$cart->refresh(false,true,true,false);
	MCartUtils::persistCart($cart);
	echo json_encode(array("status"=>"success","message"=>"normal order")); exit;
}
else if($_POST['mode'] == 'selectGift'){
	$cart->setIsGiftOrder(true);
	$cart->setGiftCharge(0);
	$cart->setAsChanged();
	MCartUtils::persistCart($cart);
	echo json_encode(array("status"=>"success","message"=>"gift order")); exit;
}
else{
	if($cart !=null && $_POST['giftTo'] != null && $_POST['giftMessage'] != null && $_POST['giftFrom'] != null){
        $giftTo = mysql_real_escape_string(filter_input(INPUT_POST, 'giftTo', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW));
        $giftFrom = mysql_real_escape_string(filter_input(INPUT_POST, 'giftFrom', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW));
        $giftMessage = mysql_real_escape_string(filter_input(INPUT_POST, 'giftMessage', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW));
		$giftMessage = substr($giftTo,0,60)."___".substr($giftFrom,0,60)."___".substr($giftMessage,0,200);
		$giftingCharges = WidgetKeyValuePairs::getInteger('giftwrap.charges');
		$cart->setIsGiftOrder(true);
		$cart->setGiftCharge($giftingCharges);
		$cart->setGiftMessage($giftMessage);
		$cart->setAsChanged();
		$cart->refresh(true,false,false,false);
		MCartUtils::persistCart($cart);
		echo json_encode(array("status"=>"success","message"=>"gift order")); exit;	
	}	
}
echo json_encode(array("stauts"=>"failed","message"=>"something went wrong please try again")); exit;
