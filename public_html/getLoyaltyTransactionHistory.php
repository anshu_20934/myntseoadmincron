<?php 
include_once("./auth.php");
include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalClient.php";
if ($telesales){
    echo "";exit;
}
if(empty($login)){
	$resp = array(
		'status'  => 'ERROR_PROMPT_LOGIN',
		'content' => ""
    );
	header('Content-Type: application/json');
	echo json_encode($resp);
	exit;
}


$start = filter_input(INPUT_GET, 'start', FILTER_SANITIZE_NUMBER_INT);
$size = filter_input(INPUT_GET, 'size', FILTER_SANITIZE_NUMBER_INT);

$transactionHistory = LoyaltyPointsPortalClient::getTransactionHistory($login,LoyaltyPointsPortalClient::CONSOLIDATED,$start,$size + 1);
$resp = array();
if(!empty($transactionHistory['status']) && $transactionHistory['status']['statusType']== 'SUCCESS'){
	$recentActivity = $transactionHistory['consolidatedTransactionHistory'];
	$showMoreRecentActivity = false;
	if(count($recentActivity) > $size){
		//We need to show show more link and remove the last element from array
		$showMoreRecentActivity = true;
		unset($recentActivity[count($recentActivity)-1]);	
	}http://www.myntra.com/look-book/12?look=298#
	$fiveDaysAgo = time() - (5 * 24 * 60 * 60);
	for ($i=0; $i < count($recentActivity); $i++) { 
		$recentActivity[$i]['timeAgo'] = $recentActivity[$i]['modifiedOn']/1000 > $fiveDaysAgo ? true : false;
		$recentActivity[$i]['modOnIso'] = date("c",$recentActivity[$i]['modifiedOn']/1000);
		$recentActivity[$i]['modOn'] = date('F d, Y',$recentActivity[$i]['modifiedOn']/1000);
		$recentActivity[$i]['showRow'] = (strtolower($recentActivity[$i]['accountType']) === 'inactivepoints' && $recentActivity[$i]['businesProcess'] === 'POINTS_ACTIVATED' && $recentActivity[$i]['creditOutflow'] > 0) ? false : true;
		$recentActivity[$i]['description'] = (strtolower($recentActivity[$i]['accountType']) === 'activepoints' && $recentActivity[$i]['businesProcess'] === 'POINTS_ACTIVATED' && $recentActivity[$i]['creditInflow'] > 0) ? 'Converting Pending points to Active points' : $recentActivity[$i]['description'];
	}
	$resp = array(
	            	'status'  => 'SUCCESS',
	            	'content' => $recentActivity,
	            	'showMoreRecentActivity' => $showMoreRecentActivity
	        	);
}else{
	$resp = array('status' => 'ERROR','content' => '');
}

header('Content-Type: application/json');
echo json_encode($resp);
exit;