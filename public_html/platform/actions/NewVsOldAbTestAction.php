<?php
namespace actions;

use base\BaseAction;

class NewVsOldAbTestAction extends BaseAction{
	public function run($params){
                global $errorlog;
		$configMap=$this->getController()->getMABTestConfigMap();
		foreach($GLOBALS as $key=>$value){
			global $$key;
		}
		
		foreach($XCART_SESSION_VARS as $key=>$value){
			global $$key;
		}
		
		$GLOBALS['skin'] = $configMap['skin'];
		$key = $this->getController()->getPageName();
		$actionId=$this->getId();
		if(!empty($actionId)){
			$key.=":$actionId";
		}
		$phpFile=$configMap[$key]['php'];
		if(empty($phpFile)){
			$errorlog->error("NewVsOldAbTestAction:php file config not found for page".$this->getController()->getPageName());
		}
		$GLOBALS['minPageName']=$this->getController()->getPageName();
		chdir(\HostConfig::$documentRoot);
		include_once("./include/cssjs.min.php");
		include_once "./$phpFile";
	}
}
