<?php
namespace web\filters;

/**
* Interface for FilterChain
*/
interface IFilterChain{

	/**
	* Method to execute the next available filter in the chain
	* Each filter will invoke this API to hand over the execution to the next 
	* filter. Any filter in the chain may also stop the filter chain at any point
	* by not invoking this API and returning from its doFilter API
	*/
	public function doFilter($route);

	/** 
	* Method to add a filter in the filter chain
	* @param $filter the IFilter object to be added to filter chain
	*/
	public function addFilter($filter);
}
