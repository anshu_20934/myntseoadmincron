<?php
namespace web\filters;

/**
* Interface for Request/Response Filters to be fed in the request routing layer 
**/
interface IFilter{

	/**
	* Method to execute the filter on the current route
	**/
	public function doFilter($filterchain, $route);

}
?>
