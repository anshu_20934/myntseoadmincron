<?php

namespace web\filters\impl;
use web\filters\IFilter;

class MobileAppFilter implements IFilter{

    private static $instance = null;
    private $log = null;

    private function __construct(){
    }

    public static function getInstance(){
        global $weblog;
        if(self::$instance == null){
            self::$instance = new MobileAppFilter();
            self::$instance->log = $weblog;
        }
        return self::$instance;
    }

    public function doFilter($filterchain, $route){
        global $isMobileApp,$mobileAppClient,$_SERVER;
        //Initialize default values here
        $isMobileApp = false;
        $mobileAppClient['client'] = "";
        $mobileAppClient['version'] = "";
        $mobileAppClient['type'] = "";
        $mobileAppClient['app_version'] = "";

        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        $myntraAppFliterKeys = array('MyntraAndroid','MyntraIphone','MyntraWindowsPhone', 'MyntraRetailAndroid', 'MyntraRetailiPhone', 'MyntraRetailiPad', 'MyntraRetailWindowsPhone');
        $pos = 0;
        foreach ($myntraAppFliterKeys as $strMobApp) {
            $pos = strpos($userAgent, $strMobApp);
            if($pos !== false){
                $isMobileApp = true;
                $mobileAppClient['client'] = strtolower($strMobApp);
                break;
            }
        }
        if($isMobileApp){
            //Parse the header for myntra app version and 
            $temp = substr($userAgent,$pos); // This will hold information regarding myntra mobile app version etc
            preg_match('/\/(.*)(\s|\Z)/U',$temp,$matches);
            $mobileAppClient['app_version'] = isset($matches[1]) ? (string)$matches[1] : '';

            //Check for anyother meta data if its present
            preg_match('/\((.*)\)/U',$temp,$matches2);
            if(isset($matches2[1])){
                $aOtherInfo = explode(',',$matches2[1]);
                $mobileAppClient['type'] = strtolower($aOtherInfo[0]); //Mobile, tablet
            }
        }
        $filterchain->doFilter();//Invoke the next filter
    }

}
