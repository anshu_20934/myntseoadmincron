<?php
namespace web\filters\impl;
use web\filters\IFilterChain;

/*
* Filter chain implementation
* @author amitrana
*/
class FilterChain implements IFilterChain{

	private $filters;
	private $curFilterPos = 0;
	private $logger;

	public function __construct(){
		global $weblog;
		$this->filters = array();
		$this->logger = $weblog;
	}

	/*
	* Adds a filter in the current filter chain
	* @param $filter IFilter object to be added to filter chain
	*/
	public function addFilter($filter){
		$this->logger->debug("Adding filter:".get_class($filter));
		$this->filters[] = $filter;
	}

	/*
	* Method to execute the next available filter in the filter chain
	* @param $route Route object on which filter is to be applied
	*/
	public function doFilter($route){
		if(empty($this->filters) || $this->curFilterPos == sizeof($this->filters)){
			$this->logger->debug("Either there are no filters or all filters have been executed hence returning");
			return;
		}
		$this->curFilterPos++;
		$this->logger->debug("Executing the next filter ".get_class($this->filters[$this->curFilterPos - 1]));
		$this->filters[$this->curFilterPos -1]->doFilter($this, $route);
	}
}
