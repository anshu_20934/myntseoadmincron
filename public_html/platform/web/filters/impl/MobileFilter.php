<?php

namespace web\filters\impl;
use web\filters\IFilter;
use web\utils\DeviceDetector; 
use web\utils\MobileConstants; 
include_once \HostConfig::$documentRoot."/include/class/class.feature_gate.keyvaluepair.php";

class MobileFilter implements IFilter{

    const WEBSITE_VIEW_KEY = "website-view";
    const WEBSITE_DEFAULT_OVERRIDE_KEY = "view-override";
    private static $pageActionMapping = 
        array(
             'home'=>array('enabled'=>true),
             'pdp'=>array('enabled'=>true),
             'search'=>array('enabled'=>true),
             'landing'=>array('enabled'=>true),
             'ajax-search'=>array('enabled'=>true),
             'checkout-cart'=>array('enabled'=>true),
             'checkout-address'=>array('enabled'=>true),
             'customeraddress'=>array('enabled'=>true),
             'ajaxAddress'=>array('enabled'=>true),
             'payment'=>array('enabled'=>true),
             'confirmation'=>array('enabled'=>true),
             'expressbuy'=>array('enabled'=>true),
             'orderinsert'=>array('enabled'=>true),
        	 'mksaveditem'=>array('enabled'=>true)
        );

    private static $instance = null;
    private $filteredIPList = array();
    private $log = null;

    private function __construct(){
        $this->filteredIPList = \FeatureGateKeyValuePairs::getStrArray('mobile.filteredIPs');
    }

    public static function getInstance(){
        global $weblog;
        if(self::$instance == null){
            self::$instance = new MobileFilter();
            self::$instance->log = $weblog;
        }
        return self::$instance;
    }

    /**
    * Apply the Mobile filter to change the route in case the mobile filter is applicable
    * @param $filterchain IFilterChain object to chain the filters
    * @param $route Route object
    **/
    public function doFilter($filterchain, $route){
        $view = $this->getView($route);
        $this->log->debug("MobileFilter : Retrived view as $view");
        if($view == MobileConstants::DESKTOP_VIEW){
            $filterchain->doFilter();//Invoke the next filter
            $this->log->debug("MobileFilter : View is desktop so passing to next filter in filer chain");
            return;
        }
        global $skin;
        $skin = MobileConstants::MOBILE_SKIN;
        $this->log->debug("MobileFilter : Showing the mobile view and setting the skin as '$skin'");
    }


    private function getView($route){
        $viewOverride = filter_input(INPUT_GET, 'viewOverride', FILTER_SANITIZE_STRING);
        if($viewOverride === MobileConstants::MOBILE_VIEW){
            return MobileConstants::MOBILE_VIEW;
        }
        $ip = get_user_ip();
        if(!empty($this->filteredIPList) && !in_array($ip, $this->filteredIPList)){
            $this->log->debug("MobileFilter : Returning desktop view as IP is not in filtered IP list");
            return MobileConstants::DESKTOP_VIEW;
        }
        if(\FeatureGateKeyValuePairs::getBoolean('mobile.getViewFromStorage', false)){
            $view = $this->getViewFromStorage();
            if(!empty($view)){
                $this->log->debug("MobileFilter : Returing view '$view' as retrieved from storage");
                return $view;
            }
        }
        $mapping = self::$pageActionMapping[_CONTROLLER_];
        if(empty($mapping) || $mapping['enabled'] == false){
            if(defined('_CONTROLLER_')){
                $this->setView(MobileConstants::DESKTOP_VIEW, _CONTROLLER_);
                $this->log->debug("MobileFilter :: Returning desktop view as mobile view is not enabled for given controller : "._CONTROLLER_);
            }else{
                $this->log->debug("MobileFilter :: Returning desktop view as no controller is set ");
            }
            return MobileConstants::DESKTOP_VIEW;
        }
        
        $isMobile = DeviceDetector::getInstance()->isMobile();
        if(!$isMobile){
            $this->setView(MobileConstants::DESKTOP_VIEW, _CONTROLLER_);
            $this->log->debug("MobileFilter : Returning desktop view as devide is not mobile");
            return MobileConstants::DESKTOP_VIEW;
        }
        if($this->isDefaultOverrideEnabled()){
            $this->log->debug("MobileFilter : Returning desktop view as default override is enabled");
            global $showMobileSiteLink;
            $showMobileSiteLink = true;
            return MobileConstants::DESKTOP_VIEW;
        }
        $this->setView(MobileConstants::MOBILE_VIEW, _CONTROLLER_);
        $this->log->debug("MobileFilter : Returning mobile view as device is mobile and is enabled for given controller :"._CONTROLLER_);
        return MobileConstants::MOBILE_VIEW;
    }
    
    /**
    * Abstracting the retrieval of view from internal storage
    * Currently we are storing the view in user's session
    * @return string value of the currently stored user's view else null
    */
    private function getViewFromStorage(){
        global $XCART_SESSION_VARS;
        $routeSuffix = '';
        if(defined('_CONTROLLER_')){
            $routeSuffix = '-'._CONTROLLER_;
        }
        if(empty($XCART_SESSION_VARS[self::WEBSITE_VIEW_KEY.$routeSuffix])){
            return null;
        }
        return $XCART_SESSION_VARS[self::WEBSITE_VIEW_KEY.$routeSuffix];
    }

    /**
    * Exposed externally to enable overriding of view from other pieces of code
    */
    private function setView($view, $routeSuffix=""){
        global $GLOBALS,$XCART_SESSION_VARS;
        if(empty($routeSuffix) && defined('_CONTROLLER_')){
            $routeSuffix = _CONTROLLER_;
        }
        if(!empty($routeSuffix)){
            $routeSuffix = "-$routeSuffix";
        }
        $GLOBALS[self::WEBSITE_VIEW_KEY.$routeSuffix] = $view;
        $XCART_SESSION_VARS[self::WEBSITE_VIEW_KEY.$routeSuffix] = $view;
        x_session_register(self::WEBSITE_VIEW_KEY.$routeSuffix, $view);
    }

    /**
    * Exposed externally to enable overriding of view from other pieces of code
    */
    public function setDefaultViewOverride($override=false){
        global $GLOBALS,$XCART_SESSION_VARS;
        $this->log->debug("MobileFilter : Setting default view override flag as $override : ".($override?"true":"false"));
        if($override === true){
            $override = true;
        }else{
            $override = false;
        }
        $this->log->debug("MobileFilter : Setting default view override flag as ".($override?"true":"false"));
        $GLOBALS[self::WEBSITE_DEFAULT_OVERRIDE_KEY] = $override;
        $XCART_SESSION_VARS[self::WEBSITE_DEFAULT_OVERRIDE_KEY] = $override;
        x_session_register(self::WEBSITE_DEFAULT_OVERRIDE_KEY, $override);
    }

    private function isDefaultOverrideEnabled(){
        global $XCART_SESSION_VARS;
        if(!empty($XCART_SESSION_VARS[self::WEBSITE_DEFAULT_OVERRIDE_KEY]) && $XCART_SESSION_VARS[self::WEBSITE_DEFAULT_OVERRIDE_KEY] == true){
            $this->log->debug("MobileFilter : Default override is enabled");
            return true;
        }
        $this->log->debug("MobileFilter : Default override is disabled");
        return false;
    }
}
