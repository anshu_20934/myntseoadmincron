<?php

namespace web\utils;

/**
* Interface for device detector
**/
interface IDeviceDetector{

    /** 
    * Method to detect if the current user agent is a mobile browser
    * @return boolean true if the user agent is a mobile browser else false
    **/
    public function isMobile();

    /** 
    * Method to detect if the current user agent is in a table device
    * @return boolean true if the user agent is in a table device else false
    **/
    public function isTablet();
}
