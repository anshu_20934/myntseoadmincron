<?php
namespace web\utils;

class MobileConstants{
        const DESKTOP_VIEW = "desktop";
        const MOBILE_VIEW = "mobile";
        const MOBILE_SKIN = "skinmobile";
}
