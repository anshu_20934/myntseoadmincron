<?php
namespace web\utils;

class ChannelType{
	const MOBILE = "mobile";
	const DESKTOP = "desktop";
	const TABLET = "tablet";
	const TELE_SALES = "telesales";
	const MOBILE_APP = "mobile_app";
	/*
	 * @params $channelName is name of the channel to get const (enum)
	 *  if strtolower($channelName) == "mobile" returns ChannelType::MOBILE
	 *  if strtolower($channelName) == "tablet" returns ChannelType::TABLET
	 *  if strtolower($channelName) == "telesales" returns ChannelType::TELE_SALES
	 *  else returns ChannelType::DESKTOP
	 */
	public static function getChannel($channelName){
		if(strtolower($channelName) == "mobile"){
			return self::MOBILE;
		}elseif(strtolower($channelName) == "tablet"){
			return self::TABLET;
		}elseif(strtolower($channelName) == "telesales"){
			return self::TELE_SALES;
		}elseif(strtolower($channelName) == "mobile_app"){
			return self::MOBILE_APP;
		}else{
			return self::DESKTOP;
		}
	}	
	
}

