<?php
namespace web\utils;
use web\utils\IDeviceDetector;
use mobile\dao\MobileUAWhitelistDBAdapter;

include_once(\HostConfig::$documentRoot."/platform/web/utils/Mobile_Detect.php");

/**
* Device detection wrapper over the unlying plugin
*/
class DeviceDetector implements IDeviceDetector{

    private $pluginInstance = null;
    private static $instance = null;
    const ALL_DEVICES = "all";
    const ALL_BROWSERS = "all";

    private function __construct(){
        $this->pluginInstance = new \Mobile_Detect();
        $this->mobileUAWhitelistDBAdapter = new MobileUAWhitelistDBAdapter();
    }

    private function isEnabled(){
        $adapter = $this->mobileUAWhitelistDBAdapter;
        $data = $adapter->getWhitelist();
        if(empty($data)){
            return true;
        }
        foreach($data as $devices=>$browsers){
            $devices = explode(',', $devices);
            $browsers = explode(',', $browsers);
            foreach($devices as $device){
                if($device == self::ALL_DEVICES || $this->pluginInstance->is($device)){
                    foreach($browsers as $browser){
                        if($browser == self::ALL_BROWSERS || $this->pluginInstance->is($browser)){
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static function getInstance(){
        if(self::$instance == null){
            self::$instance = new DeviceDetector();
        }
        return self::$instance;
    }
    private function checkBotInUA($userAgent,$botCombinedString){
        $UAarray = explode("|",$botCombinedString);
        $UAMatched = false;
        foreach ($UAarray as $UASubStr){
            if (stripos($userAgent,$UASubStr)!==false){
                $UAMatched = true;
                break;
            }
        }
        return $UAMatched;
    }
    public function isWebBot($userAgent,$botCombinedString){
         self::$instance->pluginInstance->setUserAgent($userAgent);
         return $this->checkBotInUA( self::$instance->pluginInstance->getUserAgent(),$botCombinedString);
    }
    private function checkGTMRestrictedUA($userAgent,$UACombinedString){
        $UAarray = explode("&",$UACombinedString);
        $UAMatched = true;
        foreach ($UAarray as $UASubStr){
            if (strpos($userAgent,$UASubStr)===false){
                $UAMatched = false;
                break;
            }
        }
        return $UAMatched; 
        //if ((strpos($userAgent,"Android 2.3")!==false)&&(strpos($userAgent,"AppleWebKit")!==false)&&(strpos($userAgent,"(KHTML, like Gecko) Version/4.0 Mobile Safari")!==false))
         //   return true;
        //else 
         //   return false;
    }
    
    public function isGTMRestricted($userAgent,$UACombinedString){
         self::$instance->pluginInstance->setUserAgent($userAgent); 
         return $this->checkGTMRestrictedUA( self::$instance->pluginInstance->getUserAgent(),$UACombinedString);
    }
    
    public function isMobile(){
        if(self::$instance->isEnabled()){
            return self::$instance->pluginInstance->isMobile();
        }
        return false;
    }

    public function isTablet(){
        if(self::$instance->isEnabled()){
            return self::$instance->pluginInstance->isTablet();
        }
        return false;
    }

    public function isMobileApp(){
        if(self::$instance->isEnabled()){
            return self::$instance->pluginInstance->isMobileApp();
        }
        return false;
    }



//not currently in use but could be used 
//to get customized devices based on useragent
    // public function getMobileDevice(){
    //     if(self::$instance->isEnabled()){
    //         return self::$instance->pluginInstance->getMobileDevice();
    //     }
    //     return null;
    // }

    public function getDevice() {
        if(self::$instance->isEnabled()){
            $userAgent = self::$instance->pluginInstance->getUserAgent();
            $devices = array_merge(
                self::$instance->pluginInstance->getPhoneDevices(),
                self::$instance->pluginInstance->getTabletDevices()
            );

            foreach($devices as $name => $regex) {
                $regex = str_replace('/', '\/', $regex);
                if(preg_match('/'.$regex.'/is', $userAgent, $match)) {
                    return $name." ".$match[0];
                }
            }
        }
        return null;
    }
}
