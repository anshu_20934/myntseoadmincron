<?php
require_once '../../env/Host.config.php';
define('_CONTROLLER_','confirmation');
include_once ("../../auth.php");
MClassLoader::addToClassPath(\HostConfig::$documentRoot."/platform/");

use base\BaseController;
use actions\NewVsOldAbTestAction;

class ConfirmationPageController extends BaseController{
	
	public function actionIndex(){
		foreach($GLOBALS as $key=>$value){
			global $$key;
		}
		
		foreach($XCART_SESSION_VARS as $key=>$value){
			global $$key;
		}
		$GLOBALS['minPageName'] = $this->getPageName();
		chdir(\HostConfig::$documentRoot);
		include_once("./include/cssjs.min.php");
		include_once "./confirmation.php";
	}

	public function getMABTestName(){
		return 'newVsOld';
	}
	
	public function getPageName(){
		return 'confirmation';
	}
}


$aboutUs=new ConfirmationPageController();
$aboutUs->actionIndex();
