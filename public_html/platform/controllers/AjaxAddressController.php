<?php
require_once '../../env/Host.config.php';
define('_CONTROLLER_','ajaxAddress');
include_once ("../../auth.php");
MClassLoader::addToClassPath(\HostConfig::$documentRoot."/platform/");

use base\BaseController;
use actions\NewVsOldAbTestAction;

class AjaxAddressController extends BaseController{
	
	public function actionIndex(){
		foreach($GLOBALS as $key=>$value){
			global $$key;
		}
		
		foreach($XCART_SESSION_VARS as $key=>$value){
			global $$key;
		}
		chdir(\HostConfig::$documentRoot);
		include_once("./include/cssjs.min.php");
		include_once ("./myntra/s_ajax_address.php");
	}

	public function getMABTestName(){
		return 'newVsOld';
	}
	
	public function getPageName(){
		return 'ajaxAddress';
	}
}


$address=new AjaxAddressController();
$address->actionIndex();
