<?php
$xcart_dir=$_SERVER['DOCUMENT_ROOT'];
define('_CONTROLLER_','checkout-cart');
require_once "$xcart_dir/env/Host.config.php";
include_once ("$xcart_dir/auth.php");
MClassLoader::addToClassPath(\HostConfig::$documentRoot."/platform/");

use base\BaseController;
use actions\NewVsOldAbTestAction;

class CartPageController extends BaseController{
    
    public function actionIndex(){
        foreach($GLOBALS as $key=>$value){
            global $$key;
        }
        
        foreach($XCART_SESSION_VARS as $key=>$value){
            global $$key;
        }
        global $cartPageUrl,$isMobile;
        //isNewCheckout and cartPageUrl and isMobile are declared in auth.php
        if($isNewCheckout && $cartPageUrl && !$isMobile) {
                $redirectCartPageUrl = str_replace('actionname=view_cart&','',$_SERVER['QUERY_STRING']);
                $redirectCartPageUrl = str_replace('actionname=view_cart','',$redirectCartPageUrl);
                if($redirectCartPageUrl != '' ) 
                        $redirectCartPageUrl = $cartPageUrl.'?'.$redirectCartPageUrl;
                else
                        $redirectCartPageUrl = $cartPageUrl;
                header("Location: $redirectCartPageUrl");
                exit;
        }
        $GLOBALS['minPageName']=$this->getPageName();
        chdir(\HostConfig::$documentRoot);
        include_once("./include/cssjs.min.php");
        include_once "./mkmycart.php";
    }

    public function getMABTestName(){
        return 'newVsOld';
    }
    
    public function getPageName(){
        return 'mkmycart';
    }
    
    public function actionAddToCart(){
        $actionViewer= new NewVsOldAbTestAction($this, "add_to_cart");
        $param=array('abconfig-key'=>"add_to_cart");
        $actionViewer->run($params);
    }
}

$aboutUs=new CartPageController();
if($actionname==='add_to_cart'){
    $aboutUs->actionAddToCart();
}else{
    $aboutUs->actionIndex();
}

