<?php
require_once '../../env/Host.config.php';
define('_CONTROLLER_','mksaveditem');
include_once ("../../auth.php");
MClassLoader::addToClassPath(\HostConfig::$documentRoot."/platform/");
use base\BaseController;
use actions\NewVsOldAbTestAction;

class MkSavedItemController extends BaseController{
	public function actionIndex(){
		foreach($GLOBALS as $key=>$value){
			global $$key;
		}
		foreach($XCART_SESSION_VARS as $key=>$value){
			global $$key;
		}
		$GLOBALS['minPageName']='mksaveditem';
		chdir(\HostConfig::$documentRoot);
		include_once("./include/cssjs.min.php");
		include_once "./mksaveditem.php";
	}

	public function getMABTestName(){
		return 'newVsOld';
	}
	
	public function getPageName(){
		return 'mksaveditem';
	}
}


$aboutUs=new MkSavedItemController();
$aboutUs->actionIndex();
