<?php
require_once '../../env/Host.config.php';
define('_CONTROLLER_','widget');
include_once ("../../auth.php");
MClassLoader::addToClassPath(\HostConfig::$documentRoot."/platform/");

use base\BaseController;
use widget\WidgetFactory;
use widget\enums\WidgetName;

class WidgetController extends BaseController{
	protected $pname;
	public function actionRender(){
		$widgetName = $_GET['widget_name'];
		$widget = WidgetFactory::createWidget(WidgetName::$$widgetName);
		$this->pname = $_GET['lpname'];
		$widget->render();
	}

	public function getPageName(){
		return 'widget';
	}
}


$widgetController=new WidgetController();
$widgetController->actionRender();
