<?php
require_once '../../env/Host.config.php';
define('_CONTROLLER_','notification');
include_once ("../../auth.php");
MClassLoader::addToClassPath(\HostConfig::$documentRoot."/platform/");

use base\BaseController;
use notification\Notification;
use notification\manager\NotificationManager;

class NotificationController extends BaseController{
	
	public function actionAdd(){
		global $login;
	        global $XCART_SESSION_VARS;
		$mobile = $XCART_SESSION_VARS['mobile'];
		// Construct notification object
		$notification = new Notification($login,$mobile,$_POST['notification_page'],$_POST['notification_type'],$_POST['notification_message']);
		// Save notification object
		$notificationManager = new NotificationManager();
		$notificationManager->saveNotification($notification);

	}

	public function getPageName(){
		return 'ajaxnotification';
	}
}


$notification=new NotificationController();
$notification->actionAdd();
