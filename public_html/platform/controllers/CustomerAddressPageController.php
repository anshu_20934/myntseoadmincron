<?php
$xcart_dir=$_SERVER['DOCUMENT_ROOT'];
define('_CONTROLLER_','customeraddress');
require_once "$xcart_dir/env/Host.config.php";
include_once ("$xcart_dir/auth.php");
MClassLoader::addToClassPath(\HostConfig::$documentRoot."/platform/");

use base\BaseController;
use actions\NewVsOldAbTestAction;

class CustomerAddressPageController extends BaseController{
	
    public function actionIndex(){
        foreach($GLOBALS as $key=>$value){
        global $$key;
        }
        
        foreach($XCART_SESSION_VARS as $key=>$value){
            global $$key;
        }
        $GLOBALS['minPageName']='checkout-address';
        chdir(\HostConfig::$documentRoot);
        include_once("./include/cssjs.min.php");
        include_once ("./checkout-address.php");
        
    }

	public function getMABTestName(){
		return 'newVsOld';
	}
	
	public function getPageName(){
		return 'mkcustomeraddress';
	}
	
}

$aboutUs=new CustomerAddressPageController();
$aboutUs->actionIndex();


