<?php
use oneclick\OneClickBuy;

require_once '../../env/Host.config.php';
define('_CONTROLLER_','oneclick');
include_once ("../../auth.php");
MClassLoader::addToClassPath(\HostConfig::$documentRoot."/platform/");

use base\BaseController;
//use oneclick;

class OneClickController extends BaseController{
	
	public function actionBuy(){
		foreach($GLOBALS as $key=>$value){
			global $$key;
		}
		
		foreach($XCART_SESSION_VARS as $key=>$value){
			global $$key;
		}
		
		chdir(\HostConfig::$documentRoot);
		include_once("./include/cssjs.min.php");
		
		$oneClickBuyProcessor = new OneClickBuy();
		$oneClickBuyProcessor->setSkuId($_POST['productSKUID']);
		$oneClickBuyProcessor->processOneClickBuy();	
	
	}

	public function getPageName(){
		return 'oneclick';
	}
}


$aboutUs=new OneClickController();
$aboutUs->actionBuy();
?>