<?php
ini_set('display_errors',1);
require_once '../../env/Host.config.php';
define('_CONTROLLER_','aboutus');
include_once "../../start.php";
include_once \HostConfig::$documentRoot.'/auth.php';

MClassLoader::addToClassPath("$xcart_dir/platform/");
use base\BaseController;
use actions\NewVsOldAbTestAction;

class AboutUsController extends BaseController{
	
	public function actionIndex(){
		$actionViewer= new NewVsOldAbTestAction($this, "");
		$actionViewer->run($params);
	}

	public function getMABTestName(){
		return 'newVsOld';
	}
	
	public function getPageName(){
		return 'aboutus';
	}
}


$aboutUs=new AboutUsController();
$aboutUs->actionIndex();
