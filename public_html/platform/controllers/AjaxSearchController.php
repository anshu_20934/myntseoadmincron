<?php
require_once '../../env/Host.config.php';
define('_CONTROLLER_','ajax-search');
include_once ("../../auth.php");
MClassLoader::addToClassPath(\HostConfig::$documentRoot."/platform/");

use base\BaseController;
use actions\NewVsOldAbTestAction;

class AjaxSearchController extends BaseController{
	
	public function actionIndex(){
		foreach($GLOBALS as $key=>$value){
			global $$key;
		}
		
		foreach($XCART_SESSION_VARS as $key=>$value){
			global $$key;
		}
		$GLOBALS['minPageName']='ajax-search';
		chdir(\HostConfig::$documentRoot);
		include_once "./myntra/ajax_search.php";
	}

	public function getMABTestName(){
		return 'newVsOld';
	}
	
	public function getPageName(){
		return 'ajax-search';
	}
}

$ajaxSearch=new AjaxSearchController();
$ajaxSearch->actionIndex();
