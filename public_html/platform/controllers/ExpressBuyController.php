<?php
require_once '../../env/Host.config.php';
define('_CONTROLLER_','expressbuy');
include_once ("../../auth.php");
MClassLoader::addToClassPath(\HostConfig::$documentRoot."/platform/");
use base\BaseController;
use actions\NewVsOldAbTestAction;
class ExpressBuyController extends BaseController{

    public function actionIndex(){
        foreach($GLOBALS as $key=>$value){
            global $$key;
        }
        foreach($XCART_SESSION_VARS as $key=>$value){
            global $$key;
        }
        $GLOBALS['minPageName'] = $this->getPageName();
        chdir(\HostConfig::$documentRoot);
        include_once("./include/cssjs.min.php");
        
        include_once "./expressbuy.php";
    }

    public function getMABTestName(){
        return 'newVsOld';
    }

    public function getPageName(){
        return 'expressbuy';
    }
}


$expressBuy=new ExpressBuyController();
$expressBuy->actionIndex();

