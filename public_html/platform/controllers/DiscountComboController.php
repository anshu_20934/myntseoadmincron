<?php
$xcart_dir=$_SERVER['DOCUMENT_ROOT'];
define('_CONTROLLER_',1);
require_once "$xcart_dir/env/Host.config.php";
include_once ("$xcart_dir/auth.php");
MClassLoader::addToClassPath(\HostConfig::$documentRoot."/platform/");

use base\BaseController;
use actions\NewVsOldAbTestAction;

class DiscountComboController extends BaseController{

    public function actionIndex($page){
        $actionViewer= new NewVsOldAbTestAction($this, $page);
        $actionViewer->run($params);
    }

    public function getMABTestName(){
        return 'newVsOld';
    }

    public function getPageName(){
        return 'discombo';
    }
}

$aboutUs=new DiscountComboController();
$aboutUs->actionIndex($actionname);
