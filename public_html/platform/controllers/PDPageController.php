<?php
$xcart_dir=$_SERVER['DOCUMENT_ROOT'];
define('_CONTROLLER_','pdp');
require_once "$xcart_dir/env/Host.config.php";
include_once ("$xcart_dir/auth.php");
MClassLoader::addToClassPath(\HostConfig::$documentRoot."/platform/");

use base\BaseController;
use actions\NewVsOldAbTestAction;

class PDPageController extends BaseController{
	
	public function actionIndex(){
		foreach($GLOBALS as $key=>$value){
			global $$key;
		}
		
		foreach($XCART_SESSION_VARS as $key=>$value){
			global $$key;
        }
		$GLOBALS['minPageName']='pdp';
		chdir(\HostConfig::$documentRoot);
		include_once("./include/cssjs.min.php");
		include_once "./pdp.php";
	}

	public function getMABTestName(){
		return 'newVsOld';
	}
	
	public function getPageName(){
		return 'pdp';
	}
}


$aboutUs=new PDPageController();
$aboutUs->actionIndex(); 
