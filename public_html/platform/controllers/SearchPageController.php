<?php
$xcart_dir=$_SERVER['DOCUMENT_ROOT'];
define('_CONTROLLER_','search');
define('LAZY_LOAD_DB', 1);
require_once "$xcart_dir/env/Host.config.php";
include_once ("$xcart_dir/auth.php");
include_once "$xcart_dir/include/class/instrumentation/BaseTracker.php";
require_once "$xcart_dir/include/class/class.Search_Synonym_Key.php";
include_once "$xcart_dir/utils/request/class.RequestUtils.php";

MClassLoader::addToClassPath(\HostConfig::$documentRoot."/platform/");

use base\BaseController;
use actions\NewVsOldAbTestAction;

class SearchPageController extends BaseController{
	
	protected $extensions=array("\.html","\.htm","\.png","\.jpeg","\.jpg","\.ttf","\.gif");
	
	public function init(){
		$this->tracker = new BaseTracker(BaseTracker::SEARCH_PAGE);
		$GLOBALS['tracker']=$this->tracker;
		parent::init();
	}

	public function actionIndex(){

        $requestUri = $_SERVER['REQUEST_URI'];
        $requestUri = urldecode($requestUri);

        //saveLastSearchPage($requestUri);

        $qpos = strpos($requestUri,"?");
        $request = $requestUri;
        if ($qpos !== false) {
           $request = substr($requestUri, 0, $qpos);
        }
        
        $regex = '/(' .implode('|', $this->extensions) .')/i';
		if(preg_match ($regex,$request))
		{
			$this->actionPageNotFound();
			exit;
		}


		$parts = explode('/', $request);
        // the first element will be empty to we get rid of it
        array_shift($parts);
        for($i = 0, $n = count($parts); $i < $n; $i += 1) {
            $parts[$i] = filter_var($parts[$i], FILTER_SANITIZE_STRING);
        }

		$ct=count($parts);
		$parts[0] = strtolower($parts[0]);

        // url encoded (%2F) forward slash is not handled properly in Apache (it will throw 404)
        // though it can be fixed by the AllowEncodedSlashes directive, it will not solve our case
        // because slash is used as a separator of the multipart query data
        // for example, if the search query is "3/4 pants", $parts[0] = "3" $parts[1] = "4 pants"
        // so, it is replaced by double underscores from the client side and reversed here
		$parts[0] = str_replace('__', '/', $parts[0]);

		if(!(\RequestUtils::isParametrizedSearchPage($parts[1]) || \RequestUtils::isLandingPage($parts[0])))
		{
			$synonymArray = \SearchSynonymKey::getAllSearchSynonymValuePairs();
			$synonymFlag = false;
			$paramWithSpace = str_replace("-", " ", $parts[0]);
			$words = explode("-",$parts[0]);
			for($numofWord=1;$numofWord<=sizeof($words);$numofWord++)
			{
				for($pos1stWord=0;$pos1stWord<=sizeof($words)-$numofWord;$pos1stWord++)
				{
					$tempStr = "";
					for($k=$pos1stWord;$k<=$pos1stWord+$numofWord-1;$k++)
					{
						$tempStr=$tempStr." ".$words[$k];
					}
					$tempStr = trim($tempStr);
					if(!empty($synonymArray[$tempStr]))
					{
						//Check if word is not already a substring
						//i.e. if all word of corrected word are already in search string just don't do anything
						if(strpos(" ",$paramWithSpace)===true){
							$tempArray =  explode(" ",$synonymArray[$tempStr]);
							$checkFlag = true;
							for($i=0;$i<sizeof($tempArray);$i++)
							{
								if(strpos($paramWithSpace,$tempArray[$i])=== false)
								{
									$checkFlag = false;
									continue;
								}
							}
							if($checkFlag ==  true)
							{
								continue;
							}
						}
						$paramWithSpace = str_replace($tempStr, $synonymArray[$tempStr], $paramWithSpace);
						$synonymFlag = true;
					}
				}
			}

			if($synonymFlag == true)
			{
				$_GET['synonymQuery'] = str_replace("-", " ", $parts[0]);
			}
			$parts[0] = str_replace(" ", "-", $paramWithSpace);
		}
		$_GET['query']=str_replace(" ","-",$parts[0]);


        // @paranoids
        // sanitized query data is decoded here for solr that don't know html entities
        // it should *never* be used in templates/javascript/fe stuff
        // TODO: add a method to set the query data in the SearchProducts class
        $_GET['query_decoded'] = html_entity_decode($_GET['query'], ENT_QUOTES);
		$narrow_by_u='';
		$i_query=explode("-",$_GET['query_decoded']);
	
		foreach($i_query as $key=>$value)
		{
			$narrow_by_u .=",".$value;
		}
		if(!empty($narrow_by_u))
		$_GET['narrow_by']=$narrow_by_u;
		if($ct == 3)
		{
			if(!empty($parts[1]) && $parts[1] != 'all')
			$_GET['sortby']=$parts[1];
			if(!empty($parts[2]) && $parts[2] != 'all')
			$_GET['page']=$parts[2];
		}
		else if($ct == 2)
		{
			if(!empty($parts[1]) && $parts[1] != 'all')
			$_GET['page']=$parts[1];
		}

		if(!empty($parts[1]) && \RequestUtils::isParametrizedSearchPage($parts[1])){
			$_GET[\RequestUtils::$REQUEST_TYPE]=RequestType::$PARAMETRIZED;
		}else if(\RequestUtils::isLandingPage($parts[0])){
			$_GET[\RequestUtils::$REQUEST_TYPE]=RequestType::$LANDING_PAGE;
		}else{
			if($ct !=  3 && $ct !=2){
				if(!empty($parts[1]))
				{
					$parts[1] = intval($parts[1]);
					if($parts[1]<=0 || $parts[1]>10000)
					{
						$parts[1] =1;
					}
					$_GET['page']=$parts[1];
				}
			}
			$_GET[\RequestUtils::$REQUEST_TYPE]=RequestType::$NORMAL_SEARCH;
		}
		//include_once "$xcart_dir/myntra/search.php";
		
		foreach($GLOBALS as $key=>$value){
			global $$key;
		}
		
		foreach($XCART_SESSION_VARS as $key=>$value){
			global $$key;
		}
		$GLOBALS['minPageName']='search';
		chdir(\HostConfig::$documentRoot);		
		include_once("./include/cssjs.min.php");
		include_once "./myntra/search.php";
	}

	public function getMABTestName(){
		return 'newVsOld';
	}
	
	public function getPageName(){
		return 'solrRequestHandler';
	}
}


$aboutUs=new SearchPageController();
$aboutUs->actionIndex(); 
