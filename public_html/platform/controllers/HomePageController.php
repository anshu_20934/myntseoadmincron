<?php
require_once '../../env/Host.config.php';
define('_CONTROLLER_','home');
include_once ("../../auth.php");
MClassLoader::addToClassPath(\HostConfig::$documentRoot."/platform/");

use base\BaseController;
use actions\NewVsOldAbTestAction;

class HomePageController extends BaseController{
	
	public function actionIndex(){
		foreach($GLOBALS as $key=>$value){
			global $$key;
		}
		
		foreach($XCART_SESSION_VARS as $key=>$value){
			global $$key;
		}
		$GLOBALS['minPageName']='home';
		chdir(\HostConfig::$documentRoot);
		include_once("./include/cssjs.min.php");
		include_once "./index2.php";
	}

	public function getMABTestName(){
		return 'newVsOld';
	}
	
	public function getPageName(){
		return 'homepage';
	}
}


$aboutUs=new HomePageController();
$aboutUs->actionIndex();
