<?php

/**
* Route object of framework to contain the actual controller and action required for 
* routing the request
**/
class Route{

	private $controller;
	private $action;
	private $requestURI;

	public function __contruct($controller, $action, $requestURI){
		$this->controller = $controller;
		$this->action = $action;
		$this->requestURI = $requestURI;
	}

	public function getController(){
		return $this->$controller;
	}

	public function setController($controller){
		$this->controller = $controller;
	}

	public function getAction(){
		return $this->$action;
	}

	public function setAction(){
		$this->action = $action;
	}

	public function getRequestURI(){
		return $this->requestURI;
	}

	public function setRequestURI(){
		$this->requestURI = $requestURI;
	}
}
