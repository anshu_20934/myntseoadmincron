<?php
namespace base;

/**
 * BaseAction class
 * @author yogesh
 *
 */
abstract class BaseAction {
	
	private $_id;
	private $_controller;
	
	public function __construct(BaseController $controller,$id)
	{
		$this->_controller=$controller;
		$this->_id=$id;
	}
	
	/**
	* @return CController the controller who owns this action.
	*/
	public function getController()
	{
		return $this->_controller;
	}
	
	/**
	 * @return string id of this action
	 */
	public function getId()
	{
		return $this->_id;
	}
	
	/**
	 * 
	 * @param unknown_type $params
	 */
	public abstract function run($params);
	
}