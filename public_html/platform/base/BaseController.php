<?php
namespace base;

use abtest\MABTest;

abstract class BaseController{
	
	private $configMap;
	
	private $underMABTest = false;
	
	protected $tracker;
	
	public function __construct(){
		$this->init();
	}
	
	/**
	 * Initializer method
	 */
	public function init(){
		if($this->getMABTestName()!=false){
			$mabTestObj = MABTest::getInstance();
			$config=$mabTestObj->getConfigValue($this->getMABTestName());
			$this->configMap = $config;
			$this->underMABTest=true;
		}
	}
	
	/**
	 * Returns AB test config map,
	 * returns empty id controller is not associated with any test
	 */
	public function getMABTestConfigMap(){
		return $this->configMap;
	}
	
	/**
	* Returns AB test config map,
	* returns configuration for the page
	*/
	public function getMABTestPageConfigMap(){
		return $this->configMap[$this->getPageName()];
	}
	
	/**
	 * Need to be overridden,
	 * if controller is associated with abtest,
	 * Based on this test name config map is fetched
	 * @return boolean
	 */
	public function getMABTestName(){
		return false;
	}
	
	/**
	 * Returns true if abtest is associated controller
	 */
	public function isUnderMABTest(){
		return $this->underMABTest;
	}
	
	abstract public function getPageName();
	
	public function actionPageNotFound(){
		$request = $_SERVER['REDIRECT_URI'];
        $request = urldecode($request);

		header('HTTP/1.1: 404 OK');
		
		/*
		$errordocument = '<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
		<html>
		<head>
		    <title>404 Not Found</title>
		</head>
		<body>
		    <h1>Not Found</h1>
		    <p>The requested thing '.$request.' was not found on this server.</p>
		    <hr />
		    <address>www.myntra.com</address>
		</body>
		</html>';
		
		echo $errordocument;
		*/
		
		//custom error page for 404 in case the search page in not found, example: myntra.com/abfg.html
		
		global $XCART_SESSION_VARS;
		$XCART_SESSION_VARS['errormessage'] = "<div style=\"align: left\">Sorry, it appears the page you were looking for no longer exists or has been moved.</br>Your visit is important to us.</br> Please click <a href=\"/\">here</a> to check the exciting offers we have for you. </div>";
		
		func_header_location('../../mksystemerror.php');
		
		exit;
	}
}
