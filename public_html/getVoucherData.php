<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include_once("./auth.php");
if($_SERVER['HTTP_REFERER'] == NULL || stripos($_SERVER['HTTP_REFERER'],'http://'.$xcart_http_host.'/mymyntra.php') != 0 ) {
//        || stripos($_SERVER['HTTP_REFERER'],'https://'.$xcart_http_host.'/mymyntra.php') != 0 ){
    $retValue = array();
    $retValue['main'] = '';
    $retValue['tooltip'] = '';
    echo json_encode($retValue);
    die();
}

try{
include_once ("$xcart_dir/modules/discount/DiscountEngine.php");

$code = $_GET['code'];
$orderId = $_GET['orderid'];

if($code==null){
    $code = 0;
}
//echo $code . " " . $orderid;

$shownewui= $_MABTestObject->getUserSegment("productui");

$voucherData = DiscountEngine::redeemVoucher($orderId, $code);

//echo "<PRE>".print_r($voucherData, true)."</PRE>";
//die();

if(isset($voucherData) && $voucherData!=null)
{
    foreach($voucherData->params as $k=>$v) {
        $smarty->assign('dre_'.$k,$v);
    }
	$smarty->assign('image_url',stripslashes($voucherData->image_url));
    global $_rst;
    //echo $_rst[$voucherData->id];

    $smarty->assign("displayText", $_rst[$voucherData->id]);
    $smarty->assign("tooltipText", $_rst[$voucherData->tooltip_id]);
    $smarty->assign("orderid", $orderId);
    $smarty->assign("isActive", $voucherData->isActive);
    $smarty->assign("code", $code);
    if(isset($voucherData->error))
        $smarty->assign("error", $_rst[$voucherData->error]);
    else
        $smarty->assign("error", '');

   	if($skin === "skin2") {
		$mainDiv = func_display("my/discount.tpl", $smarty, false);
        $toolTipDiv = func_display("my/discount-tooltip.tpl", $smarty, false);
	} else {
    	$mainDiv = func_display("mymyntra/myorders-discount.tpl", $smarty, false); 
    	$toolTipDiv = func_display("mymyntra/myorders-discount-tooltip.tpl", $smarty, false);
    }
}
else {
    $mainDiv = ""; 
    $toolTipDiv = "";
}
$retValue = array();
$retValue['main'] = $mainDiv;
$retValue['tooltip'] = $toolTipDiv;

echo json_encode($retValue);
}catch(Exception $e) {
	echo 'Caught exception: ',  $e->getMessage(), "\n";
}
?>
