<?php
require_once 'auth.php';
include_once($xcart_dir."/include/class/widget/class.widget.php");

$pdpMiniNav = FeatureGateKeyValuePairs::getBoolean('pdp.mininav.enabled');
$smarty->assign("pdpMiniNav", $pdpMiniNav);

// Top Navigation data
$widget = new Widget(WidgetTopNavigationV3::$WIDGETNAME);
$widget->setDataToSmarty($smarty);
$nav_content=$smarty->fetch("pdp/mini-nav.tpl", $smarty);
echo $nav_content;