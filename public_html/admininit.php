<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: init.php,v 1.31.2.8 2006/06/29 10:53:45 svowl Exp $
#
# X-Cart initialization
#
/* setting the default time zone to Indian Continent */
//putenv("TZ=Asia/Calcutta");
date_default_timezone_set("Asia/Calcutta");

if (!defined('XCART_START')) { header("Location: index.php"); die("Access denied"); }
@include_once ($xcart_dir."/prepare.php");
@include_once ($xcart_dir."/include/func/func.core.php");
@include_once ($xcart_dir."/include/logging.php");
@include_once ($xcart_dir."/include/class/cache/XCache.php");
require_once ($xcart_dir."/include/func/func.db.php");
require_once ($xcart_dir."/include/func/func.files.php");
@require_once $xcart_dir."/config.php";
@require_once ($xcart_dir."/AnalyticsBase.php");
# Instances needed globally .. (Do not add stuff that's not really needed globally)
$xcache         = new XCache();
#
# Include functions
#

include_once($xcart_dir."/include/bench.php");
include_once($xcart_dir."/include/func/func.mail.php");

//Myntra Package Loader
include_once ("$xcart_dir/include/class/MClassLoader.php");
	//add class paths here
MClassLoader::addToClassPath("$xcart_dir/include/class/");
MClassLoader::addToClassPath("$xcart_dir/modules/");
MClassLoader::addToClassPath(\HostConfig::$documentRoot."/env/");
MClassLoader::addToClassPath(\HostConfig::$documentRoot."/platform/");
MClassLoader::addToClassPath(\HostConfig::$documentRoot."/admin/class/");
require_once $xcart_dir."/databaseinit.php";
@include_once ($xcart_dir."/resources/strings/old/en.php");

if (!@is_readable($xcart_dir."/config.php")) {
	echo "Can't read config!";
	exit;
}

$file_temp_dir = $var_dirs["tmp"];

$secure_pages_list = array(
	"/mkcustomeraddress.php",
	"/mkpaymentoptions.php",
	"/cod_verification.php",
	"/mkCODverification.php",
	"/mkupdatechecksum.php",
	"/mkorderinsert.php",
	"/mkshippingcalculate_re.php",
	"/icici/iciciresponse.php",
	"/icici/ajax_bintest.php",
	"/mkorderBook.php",
	"/captcha/captcha.php",
	"/mymyntra.php",
	"/mobile_verification.php",
	"/mkphoneOrder.php",
	"/mklogin.php",
	"/myntra/ajax_pincode_locality.php",
	"/declineLastOrder.php",
	"/confirmation.php"
);

#
#Define Cart expiry time to be 7 days
#
$max_cart_validity_time=604800;


#
# HTTP & HTTPS locations
#
$http_location = "http://$xcart_http_host".$xcart_web_dir;
$https_location = "https://$xcart_https_host".$xcart_web_dir;

#
# Fix broken path for some hostings
#
$current_location = $HTTPS ? $https_location : $http_location;
$_tmp = parse_url($current_location);
$xcart_web_dir = empty($_tmp["path"]) ? "" : $_tmp["path"];

if ($HTTPS_RELAY) {

	# Fix wrong PHP_SELF for HTTPS relay
	$_tmp = parse_url($http_location);
	if (empty($_tmp['path'])) {
		$PHP_SELF = $xcart_web_dir.$PHP_SELF;

	} else {
		$PHP_SELF = $xcart_web_dir.preg_replace("/^".preg_quote($_tmp['path'], "/")."/", "", $PHP_SELF);
	}

	$HTTP_SERVER_VARS['PHP_SELF'] = $PHP_SELF;

}

$_tmp = parse_url($https_location);
$xcart_https_host = $_tmp["host"];
unset($_tmp);
$_tmp = parse_url($http_location);
$xcart_http_host = $_tmp["host"];
unset($_tmp);


#
# Create URL
#
$php_url = array("url" => "http".($HTTPS=="on"?"s://".$xcart_https_host:"://".$xcart_http_host).$PHP_SELF, "query_string" => $QUERY_STRING);

#
# Check internal temporary directories
#
$var_dirs_rules = array (
	"cache" => array (
		".htaccess" => "Deny from all\n<files \"*.js\">\nAllow from all\n</files>"
	),
	"tmp" => array (
		".htaccess" => "Deny from all"
	),
	"templates_c" => array (
		".htaccess" => "Deny from all"
	),
	"upgrade" => array (
		".htaccess" => "Deny from all"
	),
	"log" => array (
		".htaccess" => "Deny from all"
	)
);

foreach ($var_dirs as $k=>$v) {
	if (!file_exists($v) || !is_dir($v)) {
		@unlink($v);
		@func_mkdir($v);
	}

	if (!is_writable($v) || !is_dir($v)) {
		echo "Can't write data to the temporary directory: <b>".$v."</b>.<br />Please check if it exists, and have writable permissions.";
		exit;
	}

	foreach ($var_dirs_rules[$k] as $f=>$c) {
		if (file_exists($v."/".$f))
			continue;

		if ($__fp = @fopen($v."/".$f, "w")) {
			@fwrite($__fp, $c);
			@fclose($__fp);
		}
	}
}

#
# Create Smarty object
#
if (!@include $xcart_dir."/smarty.php") {
    echo "Can't launch template engine!";
    exit;
}

#
# Init miscellaneous vars
#
$smarty->assign("skin_config",$skin_config_file);
$mail_smarty->assign("skin_config",$skin_config_file);

$smarty->assign("facebook_app_id",$facebook_app_id);
$smarty->assign("http_location",$http_location);
$mail_smarty->assign("http_location",$http_location);
$smarty->assign("https_location",$https_location);
$mail_smarty->assign("https_location",$https_location);
$smarty->assign("xcart_web_dir",$xcart_web_dir);
$smarty->assign("current_location",$current_location);
$smarty->assign("php_url",$php_url);

foreach ($var_dirs_web as $k=>$v) {
	$var_dirs_web[$k] = $current_location.$v;
}

$smarty->assign_by_ref("var_dirs_web", $var_dirs_web);

$xcart_catalogs = array (
    "admin" => $current_location.DIR_ADMIN,
    "customer" => $current_location.DIR_CUSTOMER,
    "provider" => $current_location.DIR_PROVIDER,
    "partner" => $current_location.DIR_PARTNER
);

$xcart_catalogs_secure = array (
    "admin" => $https_location.DIR_ADMIN,
    "customer" => $https_location.DIR_CUSTOMER,
    "provider" => $https_location.DIR_PROVIDER,
    "partner"=>$https_location.DIR_PARTNER
);


$smarty->assign("catalogs", $xcart_catalogs);
$smarty->assign("catalogs_secure", $xcart_catalogs_secure);
$mail_smarty->assign("catalogs", $xcart_catalogs);
$mail_smarty->assign("catalogs_secure", $xcart_catalogs_secure);

#
# Files directories
#
$files_dir_name = $xcart_dir.$files_dir;
$files_http_location = $http_location.$files_webdir;
$smarty->assign("files_location",$files_dir_name);

$templates_repository = $xcart_dir.$templates_repository_dir;



#
# Connect to database.
# We check whether the constant 'REPORTS_CONNECTION' is defined. If yes, we open the reportsro connection i.e., to secondary slave
# Otherwise, we create a ro connection , i.e. to primary slave 
# In case ro connection fails we fallback to using rw connection parameters for ro connection
# Connect to rw connection at the last to enable mysql_* functions use this connection by default
#
if (defined("REPORTS_CONNECTION")){
	$ro_connection = db_connect(DBConfig::$reportsro['host'], DBConfig::$reportsro['user'], DBConfig::$reportsro['password']);
	if (db_select_db(DBConfig::$reportsro['db'], $ro_connection ) == false){
		echo "reportsro connection cannot be established \n";
		exit;
	}
}
else {
	$ro_connection = db_connect($sql_ro['host'], $sql_ro['user'], $sql_ro['password']);
	if(db_select_db($sql_ro['db'], $ro_connection ) == false){
		$ro_connection = $rw_connection;
	}
}
$rw_connection = db_connect($sql_rw['host'], $sql_rw['user'], $sql_rw['password']);
        if(db_select_db($sql_rw['db'], $rw_connection ) == false){
		header('HTTP/1.1 500 Internal Server Error');
                @mail(EmailListsConfig::$dbConnectionFailure, 'CRITICAL : Portal database not accessible - RW', 'Failed while initializing read write db connections');
                include($xcart_dir."/maintenance.html");
                exit;
        }

if (!defined('QUICK_START')) {
	#
	# Search engine bots & spiders identificator
	#
	@include_once($xcart_dir."/include/bots.php");
	
	@include_once($xcart_dir."/include/blowfish.php");

	#
	# Start Blowfish class
	#
	$blowfish = new ctBlowfish();
}

#
# Prepare session
#
@include_once $xcart_dir."/include/sessions.php";


/**
 * Assigning token to users.
 * To prevent CSRF attacks
 * One copy will be stored in session and another copy will be sent along with all html forms in hidden field _token.
 * Session USER_TOKEN value will be compared against the token value coming from form before saving any changes through form
 */
if(!defined(USER_TOKEN)){
	if(empty($XCART_SESSION_VARS['USER_TOKEN'])){
		$user_token_characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWZYZ";
		$user_token_string;
		for ($p = 0; $p < 10; $p++) {
			$user_token_string .= $user_token_characters[mt_rand(0, strlen($user_token_characters))];
		}
		$user_token = md5($user_token_string);
	}else{
		$user_token = $XCART_SESSION_VARS['USER_TOKEN'];
	}

	define(USER_TOKEN,$user_token);
	$smarty->assign("USER_TOKEN",$user_token);
	if(!x_session_is_registered('USER_TOKEN')){
		x_session_register("USER_TOKEN",$user_token);
	}
	$XCART_SESSION_VARS['USER_TOKEN']=$user_token;
	unset($user_token);
	unset($user_token_string);
	unset($user_token_characters);
}

if (!defined('QUICK_START')) {
	// Search engine bots & spiders identificator. 
	// Now that the a session is defined, we can register is_robot and robot variables defined above in bots.php
	x_session_register("is_robot");
	x_session_register("robot");
}


if (!defined("QUICK_START")) {
	#
	# Define default user profile fields
	#
	$default_user_profile_fields = array(
		"firstname" => array("avail"=>"Y","required"=>"Y"),
		"lastname"  => array("avail"=>"Y","required"=>"Y"),
		"b_address"   => array("avail"=>"Y","required"=>"N"),
		"b_address_2" => array("avail"=>"Y","required"=>"N"),
		"b_city"      => array("avail"=>"Y","required"=>"N"),
		"b_county"    => array("avail"=>"Y","required"=>"N"),
		"b_state"     => array("avail"=>"Y","required"=>"N"),
		"b_country"   => array("avail"=>"Y","required"=>"N"),
		"b_zipcode"   => array("avail"=>"Y","required"=>"N"),
		"s_address"   => array("avail"=>"Y","required"=>"N"),
		"s_address_2" => array("avail"=>"Y","required"=>"N"),
		"s_city"      => array("avail"=>"Y","required"=>"N"),
		"s_county"    => array("avail"=>"Y","required"=>"N"),
		"s_state"     => array("avail"=>"Y","required"=>"N"),
		"s_country"   => array("avail"=>"Y","required"=>"N"),
		"s_zipcode"   => array("avail"=>"Y","required"=>"N"),
		"phone"       => array("avail"=>"Y","required"=>"N"),
		"mobile"      => array("avail"=>"Y","required"=>"N"),
		"email"       => array("avail"=>"N","required"=>"N"),
		"fax"         => array("avail"=>"N","required"=>"N"),
		"url"         => array("avail"=>"N","required"=>"N"),
		"image"       => array("avail"=>"Y","required"=>"N"),
		"about_me"       => array("avail"=>"Y","required"=>"N"),
		"interests"       => array("avail"=>"Y","required"=>"N"),
		"mylinks"       => array("avail"=>"Y","required"=>"N")
	);

	#
	# Define default contact us fields
	#
	$default_contact_us_fields = array(
		"department"  => array("avail"=>"Y","required"=>"Y"),
		"username"    => array("avail"=>"Y","required"=>"Y"),
		"title"       => array("avail"=>"Y","required"=>"Y"),
		"firstname"   => array("avail"=>"Y","required"=>"Y"),
		"lastname"    => array("avail"=>"Y","required"=>"Y"),
		"company"     => array("avail"=>"Y","required"=>"N"),
		"b_address"   => array("avail"=>"Y","required"=>"Y"),
		"b_address_2" => array("avail"=>"Y","required"=>"N"),
		"b_city"      => array("avail"=>"Y","required"=>"Y"),
		"b_county"    => array("avail"=>"Y","required"=>"Y"),
		"b_state"     => array("avail"=>"Y","required"=>"Y"),
		"b_country"   => array("avail"=>"Y","required"=>"Y"),
		"b_zipcode"   => array("avail"=>"Y","required"=>"Y"),
		"phone"       => array("avail"=>"Y","required"=>"Y"),
		"mobile"       => array("avail"=>"Y","required"=>"Y"),
		"email"       => array("avail"=>"N","required"=>"N"),
		"fax"         => array("avail"=>"Y","required"=>"N"),
		"url"         => array("avail"=>"Y","required"=>"N")
	);

	if ($config["General"]["use_counties"] != "Y") {
		#
		# Disable county usage
		#
		$default_user_profile_fields["b_county"]["avail"] = "N";
		$default_user_profile_fields["b_county"]["required"] = "N";
		$default_user_profile_fields["s_county"]["avail"] = "N";
		$default_user_profile_fields["s_county"]["required"] = "N";
		$default_contact_us_fields["b_county"]["avail"] = "N";
		$default_contact_us_fields["b_county"]["required"] = "N";
	}

	$taxes_units = array(
		"ST"  => "lbl_subtotal",
		"DST" => "lbl_discounted_subtotal",
		"SH"  => "lbl_shipping_cost"
	);

	#
	# Unserialize & Assign Right-to-Left languages
	#
	if ($config["r2l_languages"])
		$config["r2l_languages"] = unserialize ($config["r2l_languages"]);

	#
	# IP addresses
	#
	$smarty->assign("PROXY_IP",$PROXY_IP);
	$smarty->assign("CLIENT_IP",$CLIENT_IP);
	$smarty->assign("REMOTE_ADDR",$REMOTE_ADDR);
	$mail_smarty->assign("PROXY_IP",$PROXY_IP);
	$mail_smarty->assign("CLIENT_IP",$CLIENT_IP);
	$mail_smarty->assign("REMOTE_ADDR",$REMOTE_ADDR);

}
#
# Read Modules and put in into $active_modules
#
$import_specification = array();
$active_modules = func_data_cache_get("modules");

$addons = array();
$body_onload = "";
$tbl_demo_data = $tbl_keys = array();
if ($active_modules) {
	foreach ($active_modules as $active_module => $tmp) {
		if (file_exists($xcart_dir."/modules/".$active_module."/config.php"))
			include $xcart_dir."/modules/".$active_module."/config.php";

		if (file_exists($xcart_dir."/modules/".$active_module."/func.php"))
			include $xcart_dir."/modules/".$active_module."/func.php";
	}
}


$smarty->assign_by_ref("active_modules", $active_modules);
$mail_smarty->assign_by_ref("active_modules", $active_modules);


# Read config variables from Database
# This variables are used inside php scripts, not in smarty templates
#
define('CONFIG_CACHE', 'config_cache');
$config = $xcache->fetch(CONFIG_CACHE);
if ($config == NULL) {
	$result = db_query("SELECT name, value, category FROM $sql_tbl[config] WHERE type != 'separator'");
	$config = array();
	if ($result) {
		while ($row = db_fetch_row($result)) {
			if (!empty($row[2]))
			$config[$row[2]][$row[0]] = $row[1];
			else
			$config[$row[0]] = $row[1];
		}
	}
	db_free_result($result);
	$xcache->store(CONFIG_CACHE, $config);
}


if (!defined("QUICK_START")) {
	
	if(!empty($config["Appearance"]["timezone_offset"]))
		$config["Appearance"]["timezone_offset"] = intval($config["Appearance"]["timezone_offset"])*3600;
	if (empty($config["Appearance"]["thumbnail_width"]))
		$config["Appearance"]["thumbnail_width"] = 0;
	if (empty($config["Appearance"]["date_format"]))
		$config["Appearance"]["date_format"] = "%d-%m-%Y";
	if(empty($config["Appearance"]["datetime_format"]))
		$config["Appearance"]["datetime_format"] = $config["Appearance"]["date_format"]." ".$config["Appearance"]["time_format"];
		
	
	#
	# Assign config array to smarty
	#
	$smarty->assign("config",$config);
	$mail_smarty->assign("config",$config);

	#
	# Assign Smarty delimiters
	#
	$smarty->assign("ldelim","{");
	$mail_smarty->assign("ldelim","{");
	$smarty->assign("rdelim","}");
	$mail_smarty->assign("rdelim","}");

	if ((isset($HTTP_GET_VARS["delimiter"])  && $HTTP_GET_VARS["delimiter"]=="tab")
	||  (isset($HTTP_POST_VARS["delimiter"]) && $HTTP_POST_VARS["delimiter"]=="tab"))
		$delimiter = "\t";
}

#
# Init modules
#
if (is_array($active_modules)) {
	foreach ($active_modules as $__k=>$__v) {
		if (file_exists($xcart_dir."/modules/".$__k."/init.php"))
			include $xcart_dir."/modules/".$__k."/init.php";
	}
}

#
#
# Remember visitor for a long time period
#
$remember_user = true;

#
# Time period for which user info should be stored (days)
#
$remember_user_days = 30;



if (x_session_is_registered("fb_uid")) {
        $smarty->assign("facebook_uid",$XCART_SESSION_VARS['fb_uid']);
}
if (x_session_is_registered("userfirstname")) {
        $smarty->assign("userfirstname",$XCART_SESSION_VARS['userfirstname']);
}
$smarty->assign("xsessionname",$XCART_SESSION_NAME);

if(isset($_SERVER['HTTPS'])){
	
	if(!in_array($_SERVER['SCRIPT_NAME'], $secure_pages_list)){
		// Page need to be redirected back to http		
		func_header_location($http_location.$_SERVER['REQUEST_URI'],false);
	}
}


# Include size options unification global mapping array
include_once($xcart_dir."/size_unification.php");
# Include top nav class


require_once __DIR__.'/Predis/Autoloader.php';
Predis\Autoloader::register();

$courierServiceabilityVersion = FeatureGateKeyValuePairs::getFeatureGateValueForKey('lms.courierserviceability.version', 'old');

/*
function fatalErrorHandler() {
	# Getting last error
	$error = error_get_last();

	# Checking if last error is a fatal error
	if(($error['type'] === E_ERROR) || ($error['type'] === E_USER_ERROR)) {
		# Here we handle the error, displaying HTML, logging, ...
		echo 'Sorry, a serious error has occured in ' . $error['file'];
	}
}

# Registering shutdown function
register_shutdown_function('fatalErrorHandler');
*/
$ccm_sql = "select * from ccm where state=1 and end_date>=current_date and start_date<=current_date order by id desc";
$ccm_values = func_query($ccm_sql);
$ccmdata= "";
foreach ($ccm_values as $data) {
    $ccmdata = $ccmdata. "&#10029; " . $data[category].":".$data[sub_category].":".$data[message].".&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
}
$smarty->assign('ccmdata', $ccmdata);
?>
