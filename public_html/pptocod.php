<?php
include_once("./auth.php");
include_once("./include/func/func.mkcore.php");

// get orderid after validating the telesales request for order placement
if(empty($_POST['telesales'])){
    if(empty($_GET['adminchangepptocod']) || empty($_GET['orderid'])) {
        echo "Wrong Call for COD conversion";
        exit;
    }
}else{
    if(empty($_POST['orderid'])) {
        echo "Wrong Call for COD conversion";
        exit;
    }
    $orderid = mysql_real_escape_string(filter_input(INPUT_POST,"orderid", FILTER_SANITIZE_NUMBER_INT));
}

// get login id for order
$codloginid = func_query_first_cell("select login from xcart_orders where orderid='$orderid'");

// set confirm mode for telesales
$confirmmode = 'pptocodqueued';
if($_POST['telesales'] == "telesalesdirectcod"){
    $confirmmode = 'telesalesdirectcod';
}else if($_POST['telesales'] == "telesalesconverttocod"){
    $confirmmode = 'telesalesconverttocod';
}

// redirect to COD verification page
$url = "$http_location/cod_verification.php?adminconfirmed=true&confirmmode=$confirmmode&codloginid=$codloginid&codorderid=$orderid";
header("Location: $url");
exit;
?>