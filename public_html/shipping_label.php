<?php

require "./auth.php";
include_once("$xcart_dir/include/func/func.db.php");
include_once("$xcart_dir/modules/apiclient/CourierApiClient.php");
 
//$order_ids = array("2172066,2170969,2150213,2150213");
$order_ids = array($_GET['orderid']);

$order_details = getOrderDetails($order_ids);
global $baseurl;

$output = "";

$order_arr = array_unique(explode(",", $order_ids[0]));
$courServiceNotAvailable = array();

foreach ($order_arr as $order_id) {
    foreach ($order_details as $order_detail) {
        if ($order_detail['orderid'] == $order_id) {
            if ($order_detail['courier_service'] != null && $order_detail['courier_service'] != "") {
                $s_zipcode = $order_detail['s_zipcode'];
                $trking_codes = explode(",", $order_detail['tracking']);
                $trking_code = end($trking_codes);
                $order_id = $order_detail['orderid'];
                $cod = $order_detail['cod'];
                $f_name = $order_detail['firstname'];
                $l_name = $order_detail['lastname'];
                $address = $order_detail['s_address'];
                $city = $order_detail['s_city'];
                $state = $order_detail['state'];
                $order_addr_mobile = $order_detail['order_addr_mobile'];
                $cust_mobile = $order_detail['cust_mobile'];
                $courier_code = $order_detail['courier_service'];
                $items_in_shipment = $order_detail['itemquantity'];
                $dispatch_date = date('F j, Y', time());
            	$data = PincodeApiClient::getAreaDetailForPincode($order_detail['courier_service'], trim($order_detail['s_zipcode']));
			    $city_area_code = "";
			    if (!$data) {
			        $city_area_code = $order_detail['s_city'];
			    } else {
			        $city_area_code = $data[0][cityCode] . "/" . $data[0][areaCode];
			    }
                $payment_method = $order_detail['payment_method'];
                
                $phone_numbers = $order_addr_mobile;

                if ($cust_mobile != $order_addr_mobile) {
                    $cust_mobile = ", " . $cust_mobile;
                } else {
                    $cust_mobile = "";
                }

                if ($trking_code != null) {
                    //$trk_img_byte = save_bar_code_image($trking_code);
                }
                $shipping_label_type = $_GET['type'];
                if($shipping_label_type == 'special'){
                    $result = "<html xmlns='http://www.w3.org/1999/xhtml'>
                        <head>
                            <title>.</title>
                            <meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />
                        </head>
                        <body>
                            <div style='width:380px;text-align:center;padding : 3px' >
                            <div style='border-bottom: 1px solid  black;text-align:left; padding : 3px 0'>
                            <div style='float:left;'><img src='./images/site-horizontal/myntra_bw.png' width='157px' height='42px'></div>
                            <div style='clear:both'>
                            <span style='font-size:17px;font-family:arial;color:#000000;clear:both'>Pincode</span>
                            <span style='font-size:17px;font-family:arial bold;font-weight:bold;color:#000000;clear:both'>$city_area_code ($s_zipcode)</span>";
                    $result.= "</div>
                    </div>
                    <div style='border-bottom: 1px solid black;padding: 12px 0'>";

                    $result .= "
                    <div style='font-size:12px;font-family:arial bold;font-weight:bold;color:#000000'>$order_id ($items_in_shipment)</div>
                    <div><img src='{$baseurl}/include/class/class.barcode.php?code=$order_id&text=0&type=.png&height=80' width='300px' height='50px' alt='trk_code'></div>";
                    $result.="</div> <div style='border-bottom: 1px solid black;text-align:left;padding : 15px 0'>";
                    $result.= "</div>
                        <div style='text-align:left;padding: 10px 0'>
                        <div style='font-size:25px;font-family:arial bold;font-weight:bold;color:#000000;text-align:center'>$order_id</div>
                        </div>
                        </body>
                        </html> <br>  ";
                } else {
                    $result = "<html xmlns='http://www.w3.org/1999/xhtml'>
                        <head>
                            <title>.</title>
                            <meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />
                        </head>
                        <body>
                            <div style='width:380px;text-align:center;padding : 3px' >
                            <div style='border-bottom: 1px solid  black;text-align:left; padding : 3px 0'>
                            <div style='float:left;'><img src='./images/site-horizontal/myntra_bw.png' width='157px' height='42px'></div>
                            <div style='font-size:21px;font-family:arial bold;font-weight:bold;color:#000000;float:right;'>$courier_code</div>
                            <div style='clear:both'>
                            <span style='font-size:17px;font-family:arial;color:#000000;clear:both'>Ship To</span>
                            <span style='font-size:17px;font-family:arial bold;font-weight:bold;color:#000000;clear:both'>$city_area_code ($s_zipcode)</span>";
                    if ($payment_method == "COD" || $payment_method == "cod") {
                        $result .= "<span style='line-height: 4px;font-size:21px;font-family:arial bold;color:#000000;float:right;'>COD </span>";
                    }
                    $result.= "</div>
                    </div>
                    <div style='border-bottom: 1px solid black;text-align:left;padding:8px 0'>
                    <div style='font-size:15px;font-family:arial bold;font-weight:bold;color:#000000;text-transform:capitalize'>$f_name $l_name</div>
                    <div style='font-size:15px;font-family:arial;color:#000000;text-transform:capitalize'>$address</div>
                    <div style='font-size:15px;font-family:arial;color:#000000;text-transform:capitalize'>$city $state $s_zipcode</div>
                    <div><span style='font-size:15px;font-family:arial bold;font-weight:bold;color:#000000'>Phone : $order_addr_mobile</span>
                         <span style='font-size:15px;font-family:arial;color:#000000'>$cust_mobile</span> </div>
                    </div>
                    <div style='border-bottom: 1px solid black;padding: 12px 0'>";
                    
                    if ($trking_code != null) {
                        $result .= "
                        <div style='font-size:12px;font-family:arial bold;font-weight:bold;color:#000000'>$trking_code</div>
                        <div><img src='{$baseurl}/include/class/class.barcode.php?code=$trking_code&text=0&type=.png&height=80' width='300px' height='50px' alt='trk_code'></div>";
                    } else {
                        $result .= "<div style='font-size:20px;font-family:arial bold;font-weight:bold;color:#000000'>Tracking Number</div>
                        <div style='font-size:18px;font-family:arial bold;color:#000000'>Not Available</div>";
                    }
                    $result.="</div> <div style='border-bottom: 1px solid black;text-align:left;padding : 15px 0'>";
                    if ($payment_method == "COD" || $payment_method == "cod") {
                        $cod = number_format($cod, 2, ".", '');
                        $result .= "<div><span style='font-size:18px;font-family:arial bold;font-weight:bold;color:#000000'>COD Amount Rs.$cod</span>
                        <span style='font-size:10px;font-family:arial;color:#000000'>(Inclusive of all charges and taxes)</span></div>";
                    } else {
                        $result .= "<div><span style='font-size:18px;font-family:arial bold;color:#000000'>Prepaid</span>
                        <span style='font-size:12px;font-family:arial;color:#000000'>(No Amount due)</span></div>";
                    }
                    $result.= "</div>
                    <div style='text-align:left;padding: 10px 0'>
                    <div style='font-size:15px;font-family:arial bold;font-weight:bold;color:#000000'>Order No. $group_id</div>
                    <div style='font-size:15px;font-family:arial bold;font-weight:bold;color:#000000'>Shipment No. $order_id ($items_in_shipment)</div>			
                    <div style='font-size:12px;font-family:arial;color:#000000'>Processed on : $dispatch_date</div>                            
                    </div>
                    <div style='font-size:12px;font-family:arial,sans-serif bold;font-weight:bold;color:black;padding:5px 0'>Free Shipping | 30 Day Returns | Pay Cash on Delivery</div>
                    </div>
                    </body>
                    </html> <br>  ";
                }
                $output .= $result;
            }else{
               array_push($courServiceNotAvailable,$order_detail['orderid']);
            }
        }
    }
}

if(count($courServiceNotAvailable) == 0){
    echo $output;
}else{
    $comma_separated = implode(",", $courServiceNotAvailable);
    echo " Please assign courier operators/remove these orders : ".$comma_separated;
}

function getOrderDetails($order_ids) {
    global $weblog;
    $order_id_to_pass_for_sql = implode(",", $order_ids);
    $sql_to_get_order_details = "SELECT xo.orderid,xo.s_zipcode,xo.tracking,xo.invoiceid,xo.shippeddate, xo.group_id,
            upper(xo.courier_service) as courier_service,xo.s_address,xo.s_city,xo.mobile AS order_addr_mobile ,xc.mobile AS cust_mobile,
            xo.total+xo.shipping_cost+xo.cod AS cod ,xo.s_firstname as firstname,xo.s_lastname as lastname, xo.payment_method,
            sum(xod.amount) as itemquantity FROM xcart_orders xo, xcart_customers xc, xcart_order_details xod WHERE xo.orderid IN 
            ($order_id_to_pass_for_sql) AND xc.login = xo.login and xod.orderid = xo.orderid group by orderid";
    $data = func_query($sql_to_get_order_details);
    return $data;
}

function save_bar_code_image($bar_code_value) {
    global $baseurl;
    global $weblog;
    $weblog->info("barcode info =" . $bar_code_value);
    $url = "{$baseurl}/include/class/class.barcode.php?code=$bar_code_value&text=0&type=.png&height=80";
    $img_data = file_get_contents($url);
    return chunk_split(base64_encode($img_data));
}
?>


