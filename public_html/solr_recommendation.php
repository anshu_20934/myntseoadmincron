<?php
define("QUICK_START", 1);
include_once("./auth.php");

require_once($xcart_dir."/include/class/search/SearchProducts.php");
include_once($xcart_dir."/include/class/widget/class.widget.keyvaluepair.php");

$page = filter_input(INPUT_POST, 'page', FILTER_SANITIZE_STRING);
$query = filter_input(INPUT_POST, 'query', FILTER_SANITIZE_STRING);
$query = html_entity_decode($query,ENT_COMPAT);

$getStatsInstance=false;
$ajaxSearchProducts = new SearchProducts($query,null,$getStatsInstance);
$ajaxSearchProducts->getAjaxQueryResult($query,"", null, 0,5);	

switch($page)
{
	case "home":
		$avail_title="";
		break;
	case "pdp" :
		$avail_title="Other Customers Recommend";
		break;
	case "search":
		$avail_title="Other Customers Recommend";
		break;
	case "cart":
		$avail_title="You Might Also Be Interested In";
		break;
	default:
		$avail_title="You May Also Like";
		break;

}


global $shownewui;

$widgetData = array();
/* make a solr query */
$widgetData['data'] = $ajaxSearchProducts->products;
$smarty->assign('page',"pdp");
$smarty->assign("avail_recommendation",$widgetData);
$smarty->assign("avail_title",$avail_title);
$smarty->assign("tabName", 'brand_reco_widget');

$div_content = $smarty->fetch("inc/matchmaker-items.tpl", $smarty);

echo $div_content;
