<?php
use enums\pageconfig\PageConfigSectionConstants;
use enums\pageconfig\PageConfigLocationConstants;
use pageconfig\manager\PageConfigManager;
require_once "auth.php";
require_once 'myntra/navhighlighter.php';
use seo\SeoStrategy;
use seo\SeoPageType;
use macros\PageMacros;
use landingpage\manager\LPManager;

$landingPage = strtolower(filter_input(INPUT_GET,"page", FILTER_SANITIZE_STRING));

global $skin;

$pageName = 'landing';
include_once $xcart_dir."/webengage.php";
include_once $xcart_dir."/gtm.php";
 
$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setContextualPageData(AnalyticsBase::LANDING_PAGE, $landingPage);
if(!empty($nav_id)) {
	$analyticsObj->setHeaderLinkVars($landingPage);
}
$analyticsObj->setMerchandisingBrowseCategoryData($landingPage);
$analyticsObj->setTemplateVars($smarty);

//Macros
$macros = new PageMacros('STATIC');
$macros->url = currentPageURLWithStripedQueryStringsAndPort();
$macros->page_type = SeoPageType::STATIC_PAGE;
$macros->page_name = $landingPage;

// If the URL through which the page is accessed is not in lower case, add a meta tag for canonical redirect to lower case URL
$queryStrIndex = strpos($_SERVER['REQUEST_URI'], '?');
$uri = $queryStrIndex ? substr($_SERVER['REQUEST_URI'], 0, $queryStrIndex) : $_SERVER['REQUEST_URI'];

if($queryStrIndex) {
    $smarty->assign('canonicalPageUrl', $http_location . strtolower($uri));
}
if ($uri != strtolower($uri)) {
	$smarty->assign('canonicalPageUrl', $http_location . strtolower($uri));
}


$seoEnabled=FeatureGateKeyValuePairs::getBoolean('SeoV3Enabled');
if($seoEnabled){
    try {
		$handle = Profiler::startTiming("seov3_load");
        $seoStrategy = new SeoStrategy($macros);
        Profiler::endTiming($handle);
        $smarty->assign("seoStrategy",$seoStrategy);
    } catch (Exception $e) {
        $seolog->error($staticPage." :: ".$e->getMessage());
    }
}

if ($landingPage == 'fashiontrends') {
	$mgr = new PageConfigManager(PageConfigLocationConstants::FashionTrends, PageConfigSectionConstants::SlideshowImage);
	$carouselImages = $mgr->getCachedImages();
	unset($mgr);
	
	$mgr = new PageConfigManager(PageConfigLocationConstants::FashionTrends, PageConfigSectionConstants::StaticImage);
	$thumbImages = $mgr->getCachedImages();
	unset($mgr);
	
	// Breadcrumbs
	$nav_arr = array('Style Zone'=>'/stylezone', 'Fashion Trends' => '/fashion-trends');
	$smarty->assign("nav_selection_arr_url", $nav_arr);
	
	$smarty->assign('carouselImages',$carouselImages);
	$smarty->assign('thumbImages',json_encode($thumbImages));
	$smarty->assign('landingPage',$landingPage);
	func_display("fashion-trends.tpl", $smarty);
} else {
	$mgr = new PageConfigManager(PageConfigLocationConstants::LandingPage, PageConfigSectionConstants::SlideshowImage,$landingPage);
	$slideShowBanners = $mgr->getCachedImages();
	$multiBannerData = $mgr->getCachedMultiClickData($slideShowBanners);
	unset($mgr);
	
	$mgr = new PageConfigManager(PageConfigLocationConstants::LandingPage, PageConfigSectionConstants::StaticImage,$landingPage);
	$staticBanners = $mgr->getCachedImages();
	unset($mgr);

	// Feature gate for banner cycle interval
	$smarty->assign("bannerCycleInterval", FeatureGateKeyValuePairs::getInteger("landingpages.bannerCycle.interval", 5000));

	if($landingPage == 'stylezone') {
		// Breadcrumbs
		$nav_arr = array('Style Zone'=>'/stylezone');
		$smarty->assign("nav_selection_arr_url", $nav_arr);
	}
	
	if($layoutVariant == 'test') {
		$topNavExpandArray = FeatureGateKeyValuePairs::getStrArray('topnav.persistentcategories', true);
		if(in_array(strtolower($landingPage),array_map('strtolower',$topNavExpandArray)))
			$smarty->assign('expandMenu',TRUE);
		else	
			$smarty->assign('expandMenu',FALSE);
	}

	$lpmanager = new LPManager();
    $lpdetails = $lpmanager->getDetailsByLPNameCached($landingPage);

	$smarty->assign('isTablet', true);//Showing tablet view by default for landing pages. Only applicable for mobile view
	$smarty->assign('slideShowBanners',$slideShowBanners);
	$smarty->assign('multiBannerData',$multiBannerData);
	$smarty->assign('staticBanners',$staticBanners);
	$smarty->assign('landingPage',$landingPage);
	$smarty->assign('lpdetails',$lpdetails);
	func_display("landing-page.tpl", $smarty);
}
