<?php 

use style\builder\CachedStyleBuilder;
use style\builder\StyleBuilder;

function addSocialExcludeToCache(){
	global $xcache;
	$social_article_type_exclude = $xcache->fetchAndStore(function(){
		$resultArray=db_query("select id from mk_catalogue_classification where enable_social_sharing=false",true);
		$finalResult=array();
		while($result = db_fetch_array($resultArray)){
			$finalResult[] = $result['id'];
		}
		return $finalResult;},
	array(),
	'social_article_type_exclude_list');
	return $social_article_type_exclude;
}


function checkSocialExclude($at,$sc){
	return in_array($at,$sc);
}

function getFormattedArrayForSocial($styleIds){
	$resultArray = array();
	$cachedStyleBuilderObj=CachedStyleBuilder::getInstance();
	foreach($styleIds as $style){
		$styleObj=$cachedStyleBuilderObj->getStyleObject($style);
		$style_properties= $styleObj->getStyleProperties();
		$catalogData = $styleObj->getCatalogueClassificationData();
	//	echo "<PRE>";print_r($style_properties);
		//echo "\n";print_r($catalogData);
		//check for exclusions
		$socialExcludeArray=addSocialExcludeToCache();
		$excludeFlag=checkSocialExclude($style_properties["global_attr_article_type"],$socialExcludeArray);
		if(!$excludeFlag){
			$resultArray[]=construct_style_url($catalogData["articletype"],$style_properties["global_attr_brand"],$style_properties["product_display_name"],$style);
		}
	}
	//Fetch array for all styles to post to facebook,get only that we need to post. Exclude social exclude items here
	return $resultArray;
}

function getStyleDataArrayForSocial($styleIds){
	$resultArray					=	array();
	$cachedStyleBuilderObj			=	CachedStyleBuilder::getInstance();
	$socialExcludeArray				=	addSocialExcludeToCache();

	foreach($styleIds as $style)
	{
		// Retrive Style Object
		$styleObj					=	$cachedStyleBuilderObj->getStyleObject($style);
		//get Style Information
		$style_properties			=	$styleObj->getStyleProperties();
		$catalogData				=	$styleObj->getCatalogueClassificationData();
		$excludeFlag				=	checkSocialExclude($style_properties["global_attr_article_type"],$socialExcludeArray);

		if(!$excludeFlag)
		{
			$styleDataURLs							=	array();
			$styleDataURLs['style_url']				=	construct_style_url($catalogData["articletype"],$style_properties["global_attr_brand"],$style_properties["product_display_name"],$style);
			$styleDataURLs['default_image_url']		=	$style_properties["default_image"];
			$resultArray[$style] 					=	$styleDataURLs;
		}
	}
	return $resultArray;
}

