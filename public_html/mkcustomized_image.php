<?php
include_once("./include/func/func.randnum.php");
function generateCustomizedImage($pastepath,$print_img,$new_x,$new_y,$IMG_WIDTH,$IMG_HEIGHT,$CUST_WIDTH,$CUST_HEIGHT){
	$print_img = $_SERVER['DOCUMENT_ROOT'].'/'.$print_img;
	$info = getimagesize($print_img,$info);
	if(!empty($info)){
		switch($info['mime']){
			case 'image/jpeg' : $image = imagecreatefromjpeg($print_img);
								break;
			case 'image/png' : $image = imagecreatefrompng($print_img);
								break;
			case 'image/gif' : $image = imagecreatefromgif($print_img);
								break;
		}
	}else{
		$image  = imagecreatefromjpeg($print_img);
	}
	
	imagepolygon($image,
		                 array (
		                       $CUST_WIDTH, $CUST_HEIGHT,
		                       $CUST_WIDTH, $CUST_HEIGHT+$IMG_HEIGHT,
		                       $CUST_WIDTH+$IMG_WIDTH, $CUST_HEIGHT+$IMG_HEIGHT,
		                       $CUST_WIDTH+$IMG_WIDTH, $CUST_HEIGHT
		                 ),
		                 4,
		                 IMG_COLOR_STYLED);
	
	$dirPath = $_SERVER['DOCUMENT_ROOT']."/images/customized/customized_design_images/L/finalcustomizeddesign";
	$pasteimagepath = $pastepath;
	
	list($width_orig, $height_orig) = getimagesize($pasteimagepath);
	    	$ratio_orig = $width_orig/$height_orig;
	        $widthMax = max($IMG_WIDTH,$width_orig);
	        $heightMax = max($IMG_HEIGHT,$height_orig);
			 //add the original uploaded image path in the session
	        
	        $widthinCA;//width of the image pasted in customization area
	        $heightinCA;//height of the image pasted in customization area
	        if (($width_orig >$IMG_WIDTH) && ($width_orig >= $height_orig)){
	            $widthinCA =$IMG_WIDTH;
	            $heightinCA = $widthinCA/$ratio_orig;
	            if ($heightinCA >$IMG_HEIGHT){
	            	$heightinCA =$IMG_HEIGHT;
	            	$widthinCA = $heightinCA * $ratio_orig;
	            }
	        }elseif (($height_orig >$IMG_HEIGHT) && ($height_orig >= $width_orig)){
	            $heightinCA =$IMG_HEIGHT;
	            $widthinCA = $ratio_orig * $heightinCA;
	            if ($widthinCA >$IMG_WIDTH){
	            	$widthinCA =$IMG_WIDTH;
	            	$heightinCA = $widthinCA/$ratio_orig;
	            }
	        }elseif (($width_orig <=$IMG_WIDTH) && ($width_orig >= $height_orig)){
	            $widthinCA = $width_orig;
	            $heightinCA = $widthinCA/$ratio_orig;
	            if ($heightinCA >$IMG_HEIGHT){
	            	$heightinCA =$IMG_HEIGHT;
	            	$widthinCA = $heightinCA * $ratio_orig;
	            }
	        }elseif (($height_orig <=$IMG_HEIGHT) && ($height_orig >= $width_orig)){
	            $heightinCA = $height_orig;
	            $widthinCA = $ratio_orig * $heightinCA;
	            if ($widthinCA >$IMG_WIDTH){
	            	$widthinCA =$IMG_WIDTH;
	            	$heightinCA = $widthinCA/$ratio_orig;
	            }
	        }
	
	
	    $pasteImage = imagecreatefromjpeg($pasteimagepath);
	    $dx = ($IMG_WIDTH - $widthinCA)/2;
	    $dy = ($IMG_HEIGHT- $heightinCA)/2;
	
	     imagecopyresampled($image, $pasteImage, $new_x+$dx,$new_y+$dy, 0, 0, $widthinCA, $heightinCA, $widthinCA, $heightinCA);
	     $temp = time().get_rand_id(4).".jpg"; ;
	     $tempCustImageFile = $dirPath.$temp;
		 imagejpeg($image,$tempCustImageFile);
		 return $temp;
}
?>