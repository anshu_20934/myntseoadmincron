<?php
include_once($xcart_dir."/include/func/func.returns.php");
include_once("$xcart_dir/include/func/func.courier.php");
include_once("$xcart_dir/include/func/func.utilities.php");
require_once "$xcart_dir/myordershelper.php";

$errors = array();
global $userToken;

if($return_enabled == "internal") {
	$pos = strpos($login, "@myntra.com");
	if($pos === false)
		$return_enabled = "false";
	else
		$return_enabled = "true";
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$mode = $_POST["mode"];
	if(!empty($mode)){
		switch($mode){
			case "return-update-courierdetails":
			{
	    		if(checkCSRF($_POST["_token"], "myreturns")){
	    			$returnid = filter_input(INPUT_POST,'returnid',FILTER_SANITIZE_NUMBER_INT);
	    			$itemid = filter_input(INPUT_POST,'itemid',FILTER_SANITIZE_NUMBER_INT);
	    			$tracking_no = mysql_real_escape_string(filter_input(INPUT_POST,'tracking_no',FILTER_SANITIZE_STRING));
	    			$courier_service = mysql_real_escape_string(filter_input(INPUT_POST,'courier_service',FILTER_SANITIZE_STRING));
	    			if($courier_service == "other") {
	    				$courier_service = filter_input(INPUT_POST,"courier_service_custom",FILTER_SANITIZE_STRING);
	    			}
	    			updateCourierDetailsForReturn($returnid, $itemid, $courier_service, $tracking_no,$login);
	    			$response = array("SUCCESS",get_courier_display_name($courier_service),$tracking_no);
	    			echo json_encode($response);
	    		}else{
	    			func_header_location($http_location."/mymyntra.php",false);
	    			exit;
	    		}
	    		exit;
	    	}
			case "return-courierdetails-div":
	    	{
	    		$returnid = filter_input(INPUT_POST,'returnid',FILTER_SANITIZE_NUMBER_INT);
	    		$itemid = filter_input(INPUT_POST,'itemid',FILTER_SANITIZE_NUMBER_INT);
	    		$return_details = format_return_request_fields($returnid);
	    		$couriers = get_active_courier_partners();
				$smarty->assign("couriers", $couriers);
				$smarty->assign("returnid", $returnid);
				$smarty->assign("return_details", $return_details);
				$smarty->assign("itemid", $itemid);
	    		$html=func_display("my/returns_courier_update.tpl",$smarty,false);
	            echo $html;
	    		exit;
	    	}
		}
	}
}

function getContentForMyReturnsTabNew($completed){
	global $mymyntra, $smarty;
    $userProfileData = $mymyntra->getUserProfileData();
	$returns = $mymyntra->getUserReturns($userProfileData['login'],$completed);
	$couriers = get_active_courier_partners();
	$smarty->assign("returns", $returns);
	$smarty->assign("couriers", $couriers);
}

getContentForMyReturnsTabNew(null);

$callNumber = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
$callPeriod = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportTime');
$smarty->assign("MYNTRA_CC_PHONE",$callNumber.' ('.$callPeriod.')');
$smarty->assign("mode",$mode);
$smarty->assign("view",$view);
$smarty->assign("login",$login);
$smarty->assign("userToken",$userToken);
$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setContextualPageData(AnalyticsBase::MYMYNTRA);
$analyticsObj->setTemplateVars($smarty);

$tracker->fireRequest();
        
$smarty->display("my/returns.tpl");
