<?php
include_once("auth.php");
// Online user -  Not a RPOS user, Not an Affiliate, Not a Brand store user

function isOnlineUser()
{
	//If it is shubhadeeps login then dont make it online user
	global $XCART_SESSION_VARS;
	if($XCART_SESSION_VARS['login']== 'subhadeep.saha@myntra.com')
		return false;
	else
		return !(isRPOSUser() || isBrandShopUser());
}

function isRPOSUser()
{
	//check for RPOS user
	global $XCART_SESSION_VARS;
	$userType = $XCART_SESSION_VARS['login_type'];
	if($XCART_SESSION_VARS['identifiers'][$userType]['account_type'] == "P2" || $XCART_SESSION_VARS['identifiers'][$userType]['account_type'] == "SM")
        	return true;
	else
		return false;
}
function isBrandShopUser()
{
	//check for Brandshop user
    //all subdomians are brandshops
	global $XCART_SESSION_VARS,$domain_position;
    //if( (isset($XCART_SESSION_VARS['orgId']) && !empty($XCART_SESSION_VARS['orgId'])) || ($domain_position === false) )
	if( isset($XCART_SESSION_VARS['orgId']) && !empty($XCART_SESSION_VARS['orgId']))
		return true;
	else
		return false;
}

function isAffiliateUser()
{
	//check for affiliate user
	global $XCART_SESSION_VARS;
	if( isset($XCART_SESSION_VARS["affiliateInfo"]) && !empty($XCART_SESSION_VARS["affiliateInfo"]) )
		return true;
	else
		return false;
}

?>
