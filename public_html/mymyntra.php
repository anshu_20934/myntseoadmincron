
<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/auth.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/include/func/func.utilities.php';
include_once("$xcart_dir/xframe_header.php");
//Check whether User account is suspended , if yes then invalidate the session and redirect to home page
$esc_login = mysql_real_escape_string($login);
$userStatus = func_query_first_cell("SELECT status FROM $sql_tbl[customers] WHERE login='$esc_login'");
if($userStatus == 'N'){
        clearSessionVarsOnLogout(true);
        func_header_location($http_location,false);
}

$defaultView = FeatureGateKeyValuePairs::getFeatureGateValueForKey('myMyntra.defaultView');
if (isset($_GET['view']))
	        $curView = $_GET['view'];
else
	        $curView = $defaultView;
if (empty($login) ) {
	$queryString = $_SERVER['QUERY_STRING'] 
						? isset($_GET['view']) 
							? filter_var($_SERVER['QUERY_STRING'], FILTER_SANITIZE_STRING)
							: filter_var($_SERVER['QUERY_STRING'], FILTER_SANITIZE_STRING) . "&view=default"
						: "view=default";
    echo "<meta http-equiv=\"Refresh\" content=\"0;URL=$http_location/register.php?".$queryString."\" >";
    exit;
}
if (empty($login)){
	$showPage = false;
}
else{
	$showPage = true;
}
$smarty->assign("showPage",$showPage);



include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
$tracker = new BaseTracker(BaseTracker::MYMYNTRA);
global $mymyntra;
//$mymyntra= true;
$smarty->assign("mymyntra",$mymyntra);
$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setContextualPageData(AnalyticsBase::MYMYNTRA);
$analyticsObj->setTemplateVars($smarty);
//EXPRESS BUY SETTING TAB
$PCIDSSCompliant = \FeatureGateKeyValuePairs::getBoolean('payments.service.PCI-DSS-Compliant') || in_array($login, \FeatureGateKeyValuePairs::getStrArray('payments.savedcards.override'));
$smarty->assign('PCIDSSCompliant',$PCIDSSCompliant);
$showMyPrivilegeTab = \FeatureGateKeyValuePairs::getBoolean('showMyPrivilegeTab');
$smarty->assign('showMyPrivilegeTab',$showMyPrivilegeTab);

if ($skin == 'skin2') {
    global $telesales;
    $smarty->assign("notAllowedMsg","Access not allowed");
    if ($telesales){
        $smarty->assign("customerUser",false);
    }
    else{
        $smarty->assign("customerUser",true);
    }

	// only when no mode is specified, we can display the appropriate mymyntra page
	if (!in_array($mode,array("verify-mobile-code","generate-code","check-mobile-user","cancel-order"))){
        $pageName = 'mymyntra';
        include_once $xcart_dir."/webengage.php";
        include_once $xcart_dir."/gtm.php";

		$defaultView = FeatureGateKeyValuePairs::getFeatureGateValueForKey('myMyntra.defaultView');
		$view = isset($_GET['view'])
					? filter_input(INPUT_GET,'view',FILTER_SANITIZE_STRING) 
					: $defaultView;
		$view = (!in_array($view, array('mymyntcredits','myorders','myreferrals','myreturns','myprofile','mygiftcards','myprivilege'))) 
					? $defaultView 
					: $view;
    	$smarty->assign('view', $view);
    	if ($view == 'mymyntcredits') {
        	require_once "$xcart_dir/mymyntcredits.php";
    	}else if($view == 'myprivilege'){
            require_once "$xcart_dir/myprivilege.php";
        }
	    else if ($view == 'myorders') {
    	    require_once "$xcart_dir/myorders.php";
	    }
	    else if ($view == 'myreferrals'){
    		require_once "$xcart_dir/myreferrals.php";
    	}
		else if ($view == 'myreturns'){
    		require_once "$xcart_dir/myreturns.php";
	    }
    	else if ($view == 'myprofile') {
    		require_once "$xcart_dir/myprofile.php";
    	}
		else if ($view == 'mygiftcards') {
            require_once "$xcart_dir/mygiftcards.php";
        }
        else if ($view == 'myloyalty') {
    		require_once "$xcart_dir/myloyalty.php";
    	}
		exit;
	}
}
use geo\ZipDAO;
include_once $xcart_dir."/include/func/func.mkcore.php";
include_once $xcart_dir."/include/class/class.mymyntra.php";
include_once "$xcart_dir/modules/coupon/mrp/SmsVerifier.php";
include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
include_once("$xcart_dir/include/func/func.courier.php");
include_once($xcart_dir."/include/func/func.returns.php");

$smsVerifier = SmsVerifier::getInstance();
$couponAdapter = CouponAdapter::getInstance();
$return_enabled = FeatureGateKeyValuePairs::getFeatureGateValueForKey('returnenabled');
$exchanges_enabled = FeatureGateKeyValuePairs::getBoolean('exchanges.enabled');
$smarty->assign("exchanges_enabled",$exchanges_enabled);
error_log("***************** mymyntra.php***********************");
error_log($exchanges_enabled);
error_log("****************************************");
if($return_enabled == "internal") {
	$pos = strpos($login, "@myntra.com");
	if($pos === false)
		$return_enabled = "false";
	else
		$return_enabled = "true";
}

$errors = array();
$mymyntra = new MyMyntra(mysql_real_escape_string($login));
if ($mode == "update-address" || $mode == "add-address") {
    $userData = func_select_first('xcart_customers', array('login' => $login));
    $firstname = $userData['firstname'];
    $lastname = $userData['lastname'];
    $promptForName = empty($firstname); // Only firstname is madatory

    $mode = filter_input(INPUT_POST,"mode",FILTER_SANITIZE_STRING);
    $id = filter_input(INPUT_POST,'id', FILTER_SANITIZE_NUMBER_INT);
    $name =filter_input(INPUT_POST,'name',FILTER_SANITIZE_STRING);
    $yourname = filter_input(INPUT_POST,'your-name',FILTER_SANITIZE_STRING);
    $locality = filter_input(INPUT_POST,'locality',FILTER_SANITIZE_STRING);
    $selectedLocality = filter_input(INPUT_POST, 'selected-locality', FILTER_SANITIZE_STRING);
    $city = filter_input(INPUT_POST,'city',FILTER_SANITIZE_STRING);
    $country = filter_input(INPUT_POST,'country',FILTER_SANITIZE_STRING);
    $state = ($country == "IN") ? filter_input(INPUT_POST,'state',FILTER_SANITIZE_STRING) : filter_input(INPUT_POST,'state-text',FILTER_SANITIZE_STRING);
    $pincode = mysql_real_escape_string(filter_input(INPUT_POST,'pincode',FILTER_SANITIZE_NUMBER_INT));
    $email = mysql_real_escape_string(filter_input(INPUT_POST,'email',FILTER_SANITIZE_STRING));
    $mobile = mysql_real_escape_string(filter_input(INPUT_POST,'mobile',FILTER_SANITIZE_NUMBER_INT));
    $copyRecipientName = filter_input(INPUT_POST,'copy-as-acc-name',FILTER_SANITIZE_STRING);

    $address = $_POST['address'];
    $address = preg_replace(array('/&(\w+|#\d+);/', '/[ \t]+/', '/(\n|\r|\r\n)+/'), array('', ' ', "\n"), $address);
    $address = filter_var($address, FILTER_SANITIZE_STRING);

    // strip out non allowed chars from the string inputs
    $rexChars = '/[^\w\d\s#\^&\*\(\)\-\+\{\}\[\]\\\|;:\'"\/\.>,<@]+/';
    foreach (array('name', 'yourname', 'address', 'locality', 'selectedLocality', 'city', 'state') as $var) {
        $$var = iconv("UTF-8", "ASCII//TRANSLIT", $$var);
        $$var = preg_replace($rexChars, '', $$var);
    }

    foreach (array('name', 'address', 'city', 'state', 'pincode', 'email', 'mobile') as $var) {
        if (empty($_POST[$var]))
            $errors[$var] = 'This field is required';
        else if (empty($$var))
            $errors[$var] = 'Please enter valid input';
    }

    if (strlen($address) > 500) {
        $errors['address'] = 'Address can not exceed more than 500 characters';
    }

    if ($abLocality == 'yes') {
        if (empty($_POST['locality']))
            $errors['locality'] = 'This field is required';
        else if (empty($locality))
            $errors['locality'] = 'Please enter valid input';
    }

    if (!empty($promtForName)) {
        if (empty($_POST['yourname']))
            $errors['yourname'] = 'This field is required';
        else if (empty($yourname))
            $errors['yourname'] = 'Please enter valid input';
    }

    if (!empty($country) && $country != 'IN') {
        $errors['country'] = 'Please choose India';
    }

    $pinerr = (int)filter_input(INPUT_POST, 'pinerr', FILTER_SANITIZE_NUMBER_INT);
    $bitPinErrState = empty($pinerr) ? 0 : 1;
    $bitPinErrLocality = (!empty($locality) && ($locality == $selectedLocality)) ? 0 : 1;
    $errmask = "b'" . $bitPinErrLocality . $bitPinErrState . "'";

    if ($mymyntra->login && $name && $address && $city && $country && $state && $pincode && $email && $mobile) {
        $data = array(
            'name' => $name,
            'address' => $address,
            'locality' => $locality,
            'city' => $city,
            'state' => $state,
            'country' => $country,
            'pincode' => $pincode,
            'email' => $email,
            'mobile' => $mobile,
            'phone' => '',
            'datecreated' => date("Y-m-d H:i:s"),
        	'errmask' => array("value"=>$errmask, "type"=>"binary")
        );
    }
}

function getContentForMyReferralsTab(){
	global $couponAdapter, $mymyntra, $smarty;
	// Open invites.
	$userProfileData = $mymyntra->getUserProfileData();
	$open_invites = $couponAdapter->getOpenInvitesForReferer($userProfileData['login']);
	$smarty->assign("open_invites", $open_invites);
	$smarty->assign("num_open_invites", count($open_invites));
	//echo count($open_invites);

	// Accepted invites.
	$accepted_invites = $couponAdapter->getAcceptedInvitesForReferer($userProfileData['login']);
	$smarty->assign("accepted_invites", $accepted_invites);
	$smarty->assign("num_accepted_invites", count($accepted_invites));
	// Active invites
	$active_invites = $couponAdapter->getActiveInvitesForReferer($userProfileData['login']);
	$smarty->assign("active_invites", $active_invites);
	$smarty->assign("num_active_invites", count($active_invites));
	$smarty->assign("userProfileData", $userProfileData);
	$smarty->assign("today", time());

	global $mrpCouponConfiguration;
    // amount earnable due to referral registration.
	$smarty->assign("mrpAmount",$mrpCouponConfiguration['mrp_refRegistration_numCoupons'] * $mrpCouponConfiguration['mrp_refRegistration_mrpAmount'] );
    // amount earnable by referral first purchase.
	$smarty->assign("refTotalValue", $mrpCouponConfiguration['mrp_refFirstPurchase_numCoupons'] * $mrpCouponConfiguration['mrp_refFirstPurchase_mrpAmount']) ;
	$smarty->assign("num_coupons", $mrpCouponConfiguration['mrp_firstLogin_nonfbreg_numCoupons']);
	$smarty->assign("value_coupons", $mrpCouponConfiguration['mrp_firstLogin_nonfbreg_mrpAmount']);

}

function getContentForMyOrdersTab()	{

	global $mymyntra, $smarty, $return_enabled;
    $orders = $mymyntra->getUserOrderHistory(true, -1); // We need to fetch item details as well, so passing 'true' as parameter.
	$smarty->assign("orders", $orders);
	$smarty->assign("returnenabled",$return_enabled);
	$smarty->assign("exchanges_enabled",$exchanges_enabled);
	$smarty->assign("type", "pending");
	$smarty->assign("message","");
	$userProfileData = $mymyntra->getUserProfileData();
	$smarty->assign("userProfileData", $userProfileData);

    $orderItems = array();
    $orderTotals = array();
    foreach ($orders as $orderId => $sorders) {
        $amounts = array('total' => 0, 'subtotal' => 0, 'discount' => 0, 'coupon_discount' => 0, 'cash_redeemed' => 0,
                'pg_discount' => 0, 'shipping_cost' => 0,'emi_charge' => 0);
        $subOrders = $sorders['orders'];
        foreach ($subOrders as $subOrder) {
            foreach ($subOrder['items'] as $item) {
                $subItemId = $item['itemid'];
                $subOrderId = $item['orderid'];
                $orderItems["$subOrderId-$subItemId"] = array(
                    'baseOrderId' => $orderId,
                    'orderId' => $subOrderId,
                    'itemId' => $subItemId,
                    'styleName' => $item['stylename'],
                    'styleId' => $item['product_style'],
                    'option' => $item['option'],
                    'amtPaid' => (float)$item['final_price_paid'],
                    'qty' => $item['quantity']
                );
            }
            $amounts['total'] += $subOrder['total']+$subOrder['payment_surcharge'];
            $amounts['subtotal'] += $subOrder['subtotal'];
            $amounts['discount'] += $subOrder['discount'];
	    $amounts['cart_discount'] += $subOrder['cart_discount'];
            $amounts['coupon_discount'] += $subOrder['coupon_discount'];
            $amounts['cash_redeemed'] += $subOrder['cash_redeemed'];
            $amounts['pg_discount'] += $subOrder['pg_discount'];
            $amounts['shipping_cost'] += $subOrder['shipping_cost'];
            $amounts['emi_charge'] += $subOrder['payment_surcharge'];
        }
        $orderTotals[$orderId] = $amounts;
    }
    $orderItemsJson = json_encode($orderItems);
    $smarty->assign('orderItemsJson', $orderItemsJson);
    $smarty->assign("orderTotals", $orderTotals);
}

function getContentForMyProfileTab() {

	global $mymyntra, $smarty, $smsVerifier;
	$countries = func_get_countries();
	//TODO Right now states drop down functionality is supported only for Indian states. Should be extended for other countries.
	$country = "IN";
	$states = func_get_states($country);
	$userProfileData = $mymyntra->getUserProfileData();
	$addresses = $mymyntra->getUserAddresses();
	// Mobile verification status.
	$mobile_status = $smsVerifier->getStatus($userProfileData['mobile'], $userProfileData['login']);

    // Show verify button.
    $verified_for_another = 0;
    $verifiedLogin = $smsVerifier->getVerifiedLoginFor($userProfileData['mobile']);
    if ($verifiedLogin != null && $verifiedLogin != $userProfileData['login']) {
    	$verified_for_another = 1;
    }

    global $skipMobileVerification;
    if($skipMobileVerification){
    	$smsVerifier->skipVerification($userProfileData['mobile'], $userProfileData['login']);
    	$mobile_status = $smsVerifier->getStatus($userProfileData['mobile'], $userProfileData['login']);
    }

	$smarty->assign("verified_for_another", $verified_for_another);
	$smarty->assign("login",$login);
	$smarty->assign("userProfileData", $userProfileData);
	$smarty->assign("countries", $countries);
	$smarty->assign("states", $states);
	$smarty->assign("mobile_status", $mobile_status);
	$smarty->assign("addresses", $addresses);
	$smarty->assign("mobile", $userProfileData['mobile']);

}

function getContentForMyMyntCreditsTab(){

	global $mymyntra, $couponAdapter, $smarty,$sql_tbl;
    $userProfileData = $mymyntra->getUserProfileData();
    // Cashback coupon.
    $cashbackCoupon = $couponAdapter->getCashbackCoupon($userProfileData['login']);
    $cashback_coupon = (empty($cashbackCoupon)) ? '' : $cashbackCoupon['coupon'];
	$cashback_balance = (empty($cashbackCoupon)) ? '' : $cashbackCoupon['MRPAmount'];
	$smarty->assign("cashback_coupon", $cashback_coupon);
	$smarty->assign("cashback_balance", $cashback_balance);
	// Cashback transactions.
	$cashback_transactions = $couponAdapter->getCashbackTransactions($userProfileData['login']);
	$smarty->assign("cashback_transactions", $cashback_transactions);
	// My Coupons.
	$my_coupons = $couponAdapter->getCouponsForUserInMyMyntra($userProfileData['login']);
	$today = strtotime(date("d F Y", time()));
	$active_coupons=array();
    $expired_coupons=array();
    $used_locked_coupons=array();
    $displayPRVFaqs = 'false';
    foreach($my_coupons as $key=>$value)
    {
    	if($value['expire'] < $today && $value['status'] != 'U')	{
        	$expired_coupons[]=$value;
        }
        else if(($value['isInfinite']) && ($value['times_used'] <= $value['maxUsageByUser']) || ($value['isInfinite'] ||(($value['times_used'] + $value['times_locked']) < $value['maxUsageByUser'])))
        {
        	$active_coupons[]=$value;
        	// check if any active coupon belongs to group "PRV"
        	// portal: 1423
        	if($value['groupName'] == "PRV")	{
        		$displayPRVFaqs = 'true';
        	}
        }
        else
        {
        	$used_result = array();
            if ($value['code'] != null && $value['code'] != "") { 
                $used_result=func_query("SELECT date, orderid, login, status
                        FROM " . $sql_tbl['orders'] . "
                        WHERE coupon = '".$value['code']."'
                        AND status in ('PP','C', 'Q', 'PV', 'OH', 'PD','SH','WP') order by date desc;");
            }
                if(!empty($used_result[0]))
            	   $value['used_on']=$used_result[0]['date'];
                $used_locked_coupons[]=$value;
            
       	}
    }
    $smarty->assign("my_coupons", $my_coupons);
    $smarty->assign("active_coupons", $active_coupons);
    $smarty->assign("expired_coupons", $expired_coupons);
    $smarty->assign("used_locked_coupons", $used_locked_coupons);
    $smarty->assign("today", $today);
	// setting expiry date for cashback coupons to 1/1/2037.
	$cashbackDay = mktime(0,0,0,1,1,2037);
	$smarty->assign("cashbackexpirydate",$cashbackDay);
	$smarty->assign("userProfileData", $userProfileData);
	$smarty->assign("displayPRVFaqs", $displayPRVFaqs);
}

function getContentForMyReturnsTab($completed){
	global $mymyntra, $smarty;
    $userProfileData = $mymyntra->getUserProfileData();
	$returns = $mymyntra->getUserReturns($userProfileData['login'],$completed);
	$couriers = get_active_courier_partners();
	$smarty->assign("returns", $returns);
	$smarty->assign("couriers", $couriers);
}


function generateInvitationEmails($email_list, $login,  $multipleEmails = false){
	global $couponAdapter, $mrpCouponConfiguration, $xcart_dir;
	$ret_msg='';
	if(empty($email_list))
    {
    	$ret_msg['status']="emptylist";
        exit;
    }

	$row = func_query_first("SELECT firstname, lastname FROM xcart_customers WHERE login = '$login';");
    $name = $row['firstname'] . " " . $row['lastname'];
    $name = (empty($row['firstname']) ? $login : $name);
	$login_tw = str_replace("@", "%40", $login);

	//pick up additional information about the coupons to pass it to mailers
	$num_coupons = $mrpCouponConfiguration['mrp_firstLogin_nonfbreg_numCoupons'];
    $each_coupon_value = $mrpCouponConfiguration['mrp_firstLogin_nonfbreg_mrpAmount'];
    $validity = $mrpCouponConfiguration['mrp_firstLogin_nonfbreg_validity'];
    $minCartValue = $mrpCouponConfiguration['mrp_firstLogin_nonfbreg_minCartValue'];
    $mrp_amount = $num_coupons*$each_coupon_value;

    include_once "$xcart_dir/modules/coupon/database/MRPDataAdapter.php";
    include_once "$xcart_dir/modules/coupon/mrp/exception/MrpCustomerNotFoundException.php";
    require_once "$xcart_dir/include/func/func.mail.php";
    require_once "$xcart_dir/include/func/func.db.php";

    $mrpDataAdapter = MRPDataAdapter::getInstance();

    $sent=0;
    $already_registered=0;
    $already_sent=0;
    $invalid_email=0;
    $emails=explode(',',$email_list);

    foreach($emails as $email)
    {
    	if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email))
    	{
            if(!$multipleEmails){
            	echo "failure";
            	exit;
            }
            $invalid_email++;
            continue;
		}

		try {
        	$customer = $mrpDataAdapter->fetchCustomer($email);

            //already registered
            if(!$multipleEmails){
            	echo "custfailure";
                exit;
            }
            $already_registered++;
            continue;
        }
        catch (MrpCustomerNotFoundException $e) {

            // Send the invitation email.
            $template = "mrp_referral_invitation";
            global $http_location;
            $args =
            	array (
                	"REFERER"          =>  $login,
                    "NAME-REFERER"     =>  $name,
                    "MRP_AMOUNT"	   => $mrp_amount,
                    "INVITATIONLINK"   =>  $http_location."/register.php?ref=".$login_tw."&utm_source=MRP_mailer&utm_medium=referral_invitation&utm_campaign=accept_invitation",
            		"NUM_COUPONS"	=> $num_coupons,
        			"EACH_COUPON_VALUE" => $each_coupon_value,
            		"VALIDITY"		=> $validity,
        			"MIN_PURCHASE_AMOUNT" => $minCartValue
                );

			$subject_args=array ("NAME-REFERER"     =>  $name, "MRP_AMOUNT" => $mrp_amount);
			$retval = $couponAdapter->addNewReferral($login, $email);

            if($retval)	{
            	sendMessageDynamicSubject($template, $args, $email, "MRP",$subject_args);
            	if(!$multipleEmails)	{
            		echo "success";
            		exit;
            	}

				// Open invites.
                $sent++;
			}	else	{
				if(!$multipleEmails)	{
					echo "timestampfailure";
					exit;
				}
				$already_sent++;
			}
        } // end of catch block

	} // end of foreach block

	$ret_msg['status']="success";
	$ret_msg['sent']=$sent;
	$ret_msg['already_registered']=$already_registered;
	$ret_msg['already_sent']=$already_sent;
	$ret_msg['invalid_email']=$invalid_email;

	return $ret_msg;
}

if (!empty($errors)) {
    $tmp = array('mode' => $mode, 'id' => $id, 'errors' => $errors, 'post' => $_POST);
    $json = json_encode($tmp);
    $smarty->assign('errorsJson', $json);
}
$actionResult = array();
global $weblog;

switch ($mode){
    case "update-address" :
    	{
    		if(checkCSRF($_POST["_token"], "mymyntra", "update-address")){
                if(empty($errors)){
                    if (!empty($id) && !empty($data)){
    				    $data['id'] = $id;
                        $actionResult = $mymyntra->updateAddress($data);
                    }
                }else{
                    $actionResult[MyMyntra::$ERROR] = "Please enter valid inputs";
                }
    		}else{
    			func_header_location($http_location."/mymyntra.php",false);
    			exit;
    		}
    		break;
    	}
    case "add-address" :
        {
        	if(checkCSRF($_POST["_token"], "mymyntra", "add-address")){
                if (empty($errors)) {
                    if (!empty($data)){
                        $actionResult = $mymyntra->addAddress($data);
                    }
                }else{
                    $actionResult[MyMyntra::$ERROR] = "Please enter valid inputs";
                }
        		if ( empty($actionResult[MyMyntra::$ERROR]) ) {
        			if ( $copyRecipientName ) {
        				$yourname = $name;
        			}
        			$namearr = MyMyntra::splitFirstLastName($yourname);
        			$firstname = $namearr[0];
        			$lastname = $namearr[1];
        			if(!empty($firstname)){
        				$data = array ( 'firstname' => $firstname, 'lastname' => $lastname );
        				$actionResult = array_merge($actionResult, $mymyntra->updateProfile($data));
        			}
        		}
        	}else{
        		func_header_location($http_location."/mymyntra.php",false);
        		exit;
        	}
            break;
        }
    case "delete-address" :
        {
        	if(checkCSRF($_POST["_token"], "mymyntra", "delete-address")){
        		$id = mysql_real_escape_string(filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT));
        		if(!empty($id)){
        			$actionResult = $mymyntra->deleteAddress($id);
        		}
        	}else{
        		func_header_location($http_location."/mymyntra.php",false);
        		exit;
        	}
            break;
        }
    case "set-default-address" :
        {
        	if(checkCSRF($_POST["_token"], "mymyntra", "set-default-address")){
        		$id = mysql_real_escape_string(filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT));
        		if(!empty($id)){
        			$actionResult = $mymyntra->setDefaultAddress($id);
        		}
        	}else{
        		func_header_location($http_location."/mymyntra.php",false);
        		exit;
        	}
            break;
        }
    case "change-password" :
        {
        	if(checkCSRF($_POST["_token"], "mymyntra", "change-password")){
        		$old_password = $_POST['old-password'];
        		$new_password = $_POST['new-password'];
        		$confirm_new_password = $_POST['confirm-new-password'];
        		if( $new_password == $confirm_new_password ){
        			$actionResult = $mymyntra->changePassword($old_password, $new_password);
        		} else {
        			$actionResult = array(MyMyntra::$ERROR=> "Supplied new password and confirm new password fields do not match");
        		}
        	}else{
        		func_header_location($http_location."/mymyntra.php",false);
        		exit;
        	}
            break;
        }
    case "update-profile-data" :
        {
        	if(checkCSRF($_POST["_token"], "mymyntra", "update-profile-data")){
        		$mobile = mysql_real_escape_string(filter_input(INPUT_POST,'mobile',FILTER_SANITIZE_NUMBER_INT));
        		$name = mysql_real_escape_string(filter_input(INPUT_POST,'name',FILTER_SANITIZE_STRING));
        		$namearr = MyMyntra::splitFirstLastName($name);
        		if(!empty($mobile) && !empty($namearr)){
        			$mobile = substr($mobile, -10);
        			$data = array ('mobile' => $mobile,'firstname' => $namearr[0], 'lastname' => $namearr[1] );
        			$prev_mobile=func_query_first_cell("select mobile from xcart_customers where login='".$login."'");
        			$actionResult = $mymyntra->updateProfile($data);

        			global $skipMobileVerification;
        			if($mobile != $prev_mobile)	{
        				$smsVerifier->addNewMapping($mobile,$login);
        				if($skipMobileVerification){
        					$smsVerifier->skipVerification($mobile,$login);
        				}
        			}
        		} else {
        			$actionResult[MyMyntra::$ERROR] = "Incorrect profile data supplied";
        		}
        	}else{
        		func_header_location($http_location."/mymyntra.php",false);
        		exit;
        	}
            break;
        }
	case "update-profile-username" :
		{
			if(checkCSRF($_POST["_token"], "mymyntra", "update-profile-username")){
				$name = mysql_real_escape_string(filter_input(INPUT_POST,'username',FILTER_SANITIZE_STRING));
				$namearr = MyMyntra::splitFirstLastName($name);
				if(!empty($namearr)){
					$data = array ('firstname' => $namearr[0], 'lastname' => $namearr[1] );
					$actionResult = $mymyntra->updateProfile($data);


				} else {
					$actionResult[MyMyntra::$ERROR] = "Incorrect profile data supplied";
				}
			}else{
				func_header_location($http_location."/mymyntra.php",false);
				exit;
			}
			exit;

			break;

		}
    case "verify-mobile-code" :
        {
        	if(checkCSRF($_POST["_token"], "mymyntra", "verify-mobile-code")){
        		$userProfileData = $mymyntra->getUserProfileData();
        		$code = mysql_real_escape_string(filter_input(INPUT_POST,'mobile-code',FILTER_SANITIZE_STRING));
        		if (!empty($code)) {
        			$ret = $smsVerifier->verifyMobileCode($userProfileData['mobile'], $code, $userProfileData['login']);

        			if ($ret) {
        				$verStatus = $smsVerifier->verifyMapping($userProfileData['mobile'], $userProfileData['login']);
        				//$smsVerifier->clearMobileCodeMapping($userProfileData['mobile']);

        				if ($verStatus) {

        					$actionResult[MyMyntra::$MESSAGE] = "Mobile number successfully verified.";
        				}
        				else {
        					$actionResult[MyMyntra::$ERROR] = "The mobile number is registered with Myntra for another customer.";
        				}
        			}
        			else {

        				$num_attempts = $smsVerifier->getAttempts($userProfileData['mobile'], $userProfileData['login']);
        				if($num_attempts == 4) {
        					$smsVerifier->lockMobileLogin($userProfileData['mobile'], $userProfileData['login']);
        					$smarty->assign("mobile_status", "E");
        				}

        				//$smsVerifier->clearMobileCodeMapping($userProfileData['mobile']);
        				$actionResult[MyMyntra::$ERROR] = "Mobile code verification failed.";
        			}
        		}
        	}else{
        		func_header_location($http_location."/mymyntra.php",false);
        		exit;
        	}
            break;
        }
    case "generate-code" :
        {
        	if(checkCSRF($_POST["_token"], "mymyntra", "generate-code")){
        		$code = rand(1000, 9999);
        		$retval = $smsVerifier->addMobileCodeMapping(mysql_real_escape_string(filter_input(INPUT_POST,'mobile',FILTER_SANITIZE_NUMBER_INT)), $code, $login);

        		if($retval == "showverifybtn")	{
        			include_once "$xcart_dir/include/func/func.sms_alerts.php";
        			func_send_sms_cellnext(filter_input(INPUT_POST,'mobile',FILTER_SANITIZE_NUMBER_INT), "Your verification code is $code.");
        			echo "success";
        		} else if ($retval == "verifyFailed")	{
        			echo "verifyFailed";
        		} else {
        			echo "showCaptcha";
        		}
        	}else{
        		func_header_location($http_location."/mymyntra.php",false);
        		exit;
        	}
        	exit;
            break;
        }
    case "check-mobile-user" :
    	{
    		if(checkCSRF($_POST["_token"], "mymyntra", "check-mobile-user")){

    			if(!empty($_POST['mobile'])) {
    				$retval = $smsVerifier->getVerifiedLoginFor(filter_input(INPUT_POST,'mobile',FILTER_SANITIZE_NUMBER_INT));
    				if($retval === null || strcasecmp($retval,$login) ==0) {
    					$mobile = mysql_real_escape_string(filter_input(INPUT_POST,'mobile',FILTER_SANITIZE_NUMBER_INT));
    					$mobile = substr($mobile, -10);
    					$data = array ('mobile' => $mobile);
    					$prev_mobile=func_query_first_cell("select mobile from xcart_customers where login='".$login."'");
    					$actionResult = $mymyntra->updateProfile($data);
    					global $skipMobileVerification;
    					if($mobile != $prev_mobile) {
    						$smsVerifier->addNewMapping($mobile,$login);
    						if($skipMobileVerification){
    							$smsVerifier->skipVerification($mobile,$login);
    							echo "skipVerify";
    							exit; break;
    						}
    					}

    					$num_attempts = $smsVerifier->getAttempts($mobile,$login);
    					if($num_attempts>=4) {
    						$smsVerifier->lockMobileLogin($mobile,$login);
    						echo "exceed";
    					}  else {
    						echo "sendSMS";
    					}
    				} else {
    					echo "verified";
    				}
    			} else {
    				echo "fail";
    			}
    		}else{
				func_header_location($http_location."/mymyntra.php",false);
    			exit;
    		}
    		exit;
    		break;
    	}
    case "invite-friends" :
        {
        	if(checkCSRF($_POST["_token"], "mymyntra", "invite-friends")){
        		$email = mysql_real_escape_string(filter_input(INPUT_POST,'email',FILTER_SANITIZE_STRING));
        		generateInvitationEmails($email,$login);
        	}else{
        		func_header_location($http_location."/mymyntra.php",false);
        		exit;
        	}
            break;
        }
    case "invite-multiple-friends" :
        {
        	if(checkCSRF($_POST["_token"], "mymyntra", "invite-multiple-friends")){
        		$email_list = mysql_real_escape_string(filter_input(INPUT_POST,'email_list',FILTER_SANITIZE_STRING));
        		$ret_msg = generateInvitationEmails($email_list,$login,true);
        		echo json_encode($ret_msg);
        	}else{
        		func_header_location($http_location."/mymyntra.php",false);
        		exit;
        	}
            exit;
        }

    case "myprofile":
        {
			getContentForMyProfileTab();
            $html=func_display("mymyntra/myprofile.tpl",$smarty,false);
            echo $html."#####myprofile_tab";
            exit;

        }
	case "myorders":
        {
        	getContentForMyOrdersTab();
            $html=func_display("mymyntra/myorders.tpl",$smarty,false);
            echo $html."#####myorders_tab";
            exit;
        }
    case "mymyntcredits":
        {
			getContentForMyMyntCreditsTab();
            $html=func_display("mymyntra/mycoupons.tpl",$smarty,false);
            echo $html."#####mymyntcredits_tab";
            exit;
        }
    case "myreferrals":
        {
            getContentForMyReferralsTab();
            $html=func_display("mymyntra/myreferrals.tpl",$smarty,false);
            echo $html."#####myreferrals_tab";
            exit;
        }
    case "myreturns":
    	{
			getContentForMyReturnsTab(null);
			$smarty->assign("mode",$mode);
			$callNumber = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
			$callPeriod = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportTime');
			$smarty->assign("MYNTRA_CC_PHONE",$callNumber.' ('.$callPeriod.')');
			$smarty->assign("mode",$mode);
			$html=func_display("mymyntra/myreturns.tpl",$smarty,false);
            echo $html."#####myreturns_tab";
            exit;
    	}
    case "return-update-courierdetails":
    	{
    		if(checkCSRF($_POST["_token"], "mymyntra", "return-update-courierdetails")){
    			$returnid = filter_input(INPUT_POST,'returnid',FILTER_SANITIZE_NUMBER_INT);
    			$itemid = filter_input(INPUT_POST,'itemid',FILTER_SANITIZE_NUMBER_INT);
    			$tracking_no = mysql_real_escape_string(filter_input(INPUT_POST,'tracking_no',FILTER_SANITIZE_STRING));
    			$courier_service = mysql_real_escape_string(filter_input(INPUT_POST,'courier_service',FILTER_SANITIZE_STRING));
    			if($courier_service == "other") {
    				$courier_service = filter_input(INPUT_POST,"courier_service_custom",FILTER_SANITIZE_STRING);
    			}
    			updateCourierDetailsForReturn($returnid, $itemid, $courier_service, $tracking_no,$login);
    			$response = array("SUCCESS",get_courier_display_name($courier_service),$tracking_no);
    			echo json_encode($response);
    		}else{
    			func_header_location($http_location."/mymyntra.php",false);
    			exit;
    		}
    		exit;
    }
	case "return-courierdetails-div":
    	{
            if(checkCSRF($_POST["_token"], "mymyntra", "return-courierdetails-div")){
    	       $returnid = filter_input(INPUT_POST,'returnid',FILTER_SANITIZE_NUMBER_INT);
    	       $itemid = filter_input(INPUT_POST,'itemid',FILTER_SANITIZE_NUMBER_INT);
    	       $couriers = get_active_courier_partners();
	           $smarty->assign("couriers", $couriers);
	           $smarty->assign("returnid", $returnid);
	           $smarty->assign("itemid", $itemid);
               $html=func_display("mymyntra/returns_courier_update.tpl",$smarty,false);
               echo $html;
    		   exit;
            }
            else{
                func_header_location($http_location."/mymyntra.php",false);
                exit;
            }
    	}
    	
    	case "cancel-order":{
            if(checkCSRF($_POST["_token"], "mymyntra", "cancel-order")){
    			$orderid = filter_input(INPUT_POST,'orderid',FILTER_SANITIZE_NUMBER_INT);
    			$details = filter_input(INPUT_POST,'details',FILTER_SANITIZE_STRING);
    			$cancel_reason = filter_input(INPUT_POST,'reason',FILTER_SANITIZE_STRING);
    			$valid = isValidUserOrder($login,$orderid);
    			$order_details = func_query_first("select status from xcart_orders where orderid = $orderid");
    			if(!($order_details['status'] == 'Q'||$order_details['status'] == 'WP'||$order_details['status'] == 'OH')){
    				echo json_encode(array('result'=>'not_allowed','msg'=>"Order has been already processed or cancelled, Please get in touch with our customer care team."));
    				exit;
    			}
    			if($valid){
    				$cancel_args = array(
    					'cancellation_type' 			=> 'CCR',
    					'reason'						=> $cancel_reason,
    					'orderid2fullcancelordermap'	=> array($orderid=>true),
    					'generateGoodWillCoupons'		=> false,
    					'is_oh_cancellation'			=> false
    				);
    				$cancellation_response =  func_change_order_status($orderid, 'F',$login,'Online cancellation initiated by user with comment :: <br/>'.$details,null,true,null,$cancel_args);
    				
    			}else{
    				$out = array('result'=>'failure');
    				echo json_encode($out);
    				exit;
    			}
    			if($cancellation_response[$orderid]['status'] == 'SUCCESS'){
    				$out = array('result'=>'success','refund_amount'=>$cancellation_response[$orderid]['refund_amount']);	
                                    global $telesales,$telesalesUser;
                                    if ($telesales){
                                        $audit_cancellation = array();
                                        $audit_cancellation["order_id"]=$orderid;
                                        $audit_cancellation["user_id"]=$login;
                                        $audit_cancellation["agent_id"]=$telesalesUser;
                                        $audit_cancellation["action"]="cancellation";
                                        func_array2insertWithTypeCheck("Cancellation_log",$audit_cancellation);
                                        
                                    }
    			}else{
    				$out = array('result'=>'failure');
    			}
    			
    			echo json_encode($out);
    			exit;
            }
            else{
                func_header_location($http_location."/mymyntra.php",false);
                exit;
            }
		}	
    	
}

$available_views = array("myprofile", "mypendingorders", "mymyntcredits", "myreferrals","mycompletedorders");
$view=filter_input(INPUT_GET,'view',FILTER_SANITIZE_STRING);
if(empty($view))$view="myprofile";
if($return_enabled == "true") {
	$available_views[] = "mypendingreturns";
	$available_views[] = "mycompletedreturns";
}
if( !in_array($view, $available_views) ){
	//$view="myprofile";
}
global $weblog;

switch($view)
{
    case "myprofile":
        {
        	getContentForMyProfileTab();
            break;
        }
    case "myorders":
        {
        	getContentForMyOrdersTab();
            break;
        }
    case "mymyntcredits":
        {
			getContentForMyMyntCreditsTab();
            break;
        }
    case "myreferrals":
        {
			getContentForMyReferralsTab();
			break;
        }
    case "mypendingreturns":
    	{
    		getContentForMyReturnsTab(null);
    		break;
    	}
	case "mycompletedreturns":
    	{
    		getContentForMyReturnsTab(null);
    		break;
    	}
}
$error = $actionResult[MyMyntra::$ERROR];
$message = $actionResult[MyMyntra::$MESSAGE];


$breadCrumb=array(
        array('reference'=>'home.php','title'=>'home'),
        array('reference'=>'mymyntra.php','title'=>'My Myntra'),
        );
$smarty->assign("view",$view);
$smarty->assign("error",$error);
$smarty->assign("message",$message);
$smarty->assign("login",$login);
$smarty->assign("returnenabled",$return_enabled);
$smarty->assign("exchanges_enabled",$exchanges_enabled);
$smarty->assign("breadCrumb",$breadCrumb);
$smarty->assign("counts",$mymyntra->getOrderAndReturnsCounts($login));

const STATEZIPS_CACHEKEY = "statezips_xcache";
global $xcache ;
if($xcache==null){
	$xcache = new XCache();
}
$stateZipsJson = $xcache->fetchAndStore(function() {
	require_once "geoinit.php";
	$zipDAO = new ZipDAO();
	$stateZips = $zipDAO->getAllStateLevelZips();
	return json_encode($stateZips);
}, array(), STATEZIPS_CACHEKEY);
$smarty->assign("stateZipsJson",$stateZipsJson);

$tracker->fireRequest();
func_display("mymyntra.tpl", $smarty);
?>
