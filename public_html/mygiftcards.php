<?php
header("Location: mymyntra.php?view=myprofile");
exit;

require_once($xcart_dir."/modules/giftcards/GiftCardsHelper.php");

$gco_bought=GiftCardsHelper::getGiftCardOrdersByEmail($login);
$gc_received=GiftCardsHelper::getActivatedGiftCardsByEmail($login);

$gc_received_obj=array();

foreach($gc_received as $key => $value){
	if($value->status == 'Activated'){
		$gc_received_obj[]=$value;
	}
}

$smarty->assign("gco_bought", $gco_bought);
$smarty->assign("gc_received", $gc_received_obj);

$smarty->display("my/giftcards.tpl");

