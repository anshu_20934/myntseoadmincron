<?php

require_once("auth.php");
include_once("$xcart_dir/modules/amqpclients/producer/AMQPMessageProducer.php");
$client = AMQPMessageProducer::getInstance();
$sent_messages = $client->resendMessages();

if($sent_messages && $sent_messages > 0){
	include_once($xcart_dir."/include/class/class.mail.multiprovidermail.php");
	
	$mail_details = array(
        //"header"=>"Content-Type: text/plain",
      	    "from_email"=>'admin@myntra.com',
	        "from_name"=>'OMS Admin',
	        "to" => 'engg_erp_oms@myntra.com',
	        "mail_type" => MailType::CRITICAL_TXN,
	        "subject"=> "Failed messages queue cron",
	        "content"=> "queued $sent_messages messages"
	);
        $multiPartymailer = new \MultiProviderMailer($mailDetails, $customKeywords);
        $flag = $multiPartymailer->sendMail();
}

?>

