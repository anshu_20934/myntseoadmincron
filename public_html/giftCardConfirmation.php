<?php
chdir(dirname(__FILE__));
include_once("./auth.php");
include_once("$xcart_dir/include/class/class.orders.php");
include_once("$xcart_dir/include/func/func.mk_orderbook.php");
include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
include_once \HostConfig::$documentRoot."/include/class/class.mymyntra.php";
include_once \HostConfig::$documentRoot."/modules/coupon/mrp/SmsVerifier.php";
include_once($xcart_dir."/modules/giftcards/GiftCardsHelper.php");
global $telesales;
if ($telesales){
    echo "Access not allowed";exit; 
}
//unset($XCART_SESSION_VARS['selected_address']);

//security checks
if(empty($_GET) || empty($orderid)) {
	func_header_location($http_location,false);
}

if(empty($login)) {
	//empty login
	func_header_location($http_location,false);
}

$pageName = 'confirmation';
include_once $xcart_dir."/webengage.php";

$mymyntra = new MyMyntra(mysql_real_escape_string($login));
$customer = $mymyntra->getBasicUserProfileData($login);

$orderid = mysql_real_escape_string($orderid);
$order_result = \GiftCardsHelper::getGiftCardOrderById($orderid);
$order_result_temp = $order_result;
$order_result=objectToArray($order_result);

//Triggering email for gift card
$giftCard = $order_result["giftCard"];
$smarty->assign("giftCard",$giftCard);
$giftCardId = $order_result["giftCard"]["id"];
//\GiftCardsHelper::sendEmailForGiftCard($giftCardId);

//$order_result = func_query("SELECT * FROM " . $sql_tbl['orders'] . " WHERE group_id='". $orderid ."'");
$orderLogin = $order_result['login'];
$status = $order_result['status'];
$status = "Q";
$paymnetlogs = func_query("SELECT payment_option FROM mk_payments_log WHERE orderid='". $orderid ."'");
$paybyoption = $paymnetlogs[0]['payment_option'];
if(strcasecmp($login, $orderLogin) != 0) {
	//login not matched with user who placed the order
	func_header_location($http_location,false);
}

//security check ends
if($status=="Q" || $status=="WP" || $status=="SH" || $status=="OH" || $status=="DL") {
		$ordersubtotal = $order_result['subTotal'];
		$cashdiscount = $order_result['cashRedeemed'];			
		$codCharge = $order_result['codCharge'];	
		$orderTotal = $order_result['total'];
		//$coupondiscount += $order['coupon_discount'];
		$coupondiscount = 0;
		$productDiscount = $order_result['discount'];
		$cartDiscount = $order_result['cartDiscount'];
		$pg_discount = $order_result['paymentGatewayDiscount'];
		$emi_charge = $order_result['paymentSurcharge'];
		//$shippingCharges += $order['shipping_cost'];
		$shippingCharges = 0;
		$amountafterdiscount = $order_result['total'] + $order_result['codCharge'] + $order_result['paymentSurcharge'];	
	//$productsInCart = func_create_array_products_of_order($orderids, null);	
	//$highestPriceArticleType = getHighestPriceArticleType($productsInCart);
	
	//$couponCode = $order_result[0]['coupon'];
	$couponCode = "";
	$cashCouponCode = $order_result['cashCouponCode'];
	$paymentoption = $order_result['paymentMethod'];
	
	$cashback_gateway_status = FeatureGateKeyValuePairs::getFeatureGateValueForKey('cashbackenabled');
	$enable_cashback_discounted_products = FeatureGateKeyValuePairs::getFeatureGateValueForKey('enableCBOnDiscountedProducts');
	
	$adapter = CouponAdapter::getInstance();
	$total = $orderTotal + $cashdiscount + $cartDiscount- $shippingCharges; 
	$cashBackToBeCredited = $adapter->getCashBackToBeCreditedForOrderid($cashback_gateway_status, $enable_cashback_discounted_products, $login, $total, $orderid);
	
	$fireSyncGACall = FeatureGateKeyValuePairs::getBoolean("confirmation.fireSyncGACall",false);
	$recordGACall = FeatureGateKeyValuePairs::getBoolean("confirmation.recordGACall",false);
	
	$giftCard = $order_result_temp->giftCard;
	$smarty->assign("gc",$giftCard);
	
	$giftCardId=$giftCard->giftCardMessage->giftCardOccasion->id;
	$giftCardOccasion=\GiftCardsHelper::getOccasionById($giftCardId);
	$smarty->assign("giftCardOccasion", $giftCardOccasion);
	$smarty->assign("gc_occasion", $giftCardOccasion);
	$smarty->assign("gc_message", $giftCard->giftCardMessage);
	
	$smarty->assign("giftCardPaymentPage", true);
	$smarty->assign("orderid", $orderid);
	$smarty->assign("ordersubtotal", $ordersubtotal);
	$smarty->assign("amountafterdiscount", $amountafterdiscount);
	$smarty->assign("highestPriceArticleType",$highestPriceArticleType);
	$smarty->assign("userinfo", $customer);
	$smarty->assign("paymentoption", $paymentoption);
	$smarty->assign("paybyoption",$paybyoption);
	$smarty->assign("cashCouponCode",$cashCouponCode);
	$smarty->assign("totalcashdiscount",$cashdiscount);			
	$smarty->assign("grandTotal",$ordersubtotal);
	$smarty->assign("eossSavings", $productDiscount);
	$smarty->assign("cartLevelDiscount", $cartDiscount);
	$smarty->assign("totalDiscount",number_format($productDiscount+$coupondiscount+$pg_discount+$cartDiscount,2,".",''));
	//$smarty->assign("shippingRate",$order_result[0]['shipping_cost']);
	$smarty->assign("shippingRate",0);
	$smarty->assign("coupon_discount",$coupondiscount);
	//$smarty->assign("giftamount",$order_result[0]['gift_charges']);
	$smarty->assign("giftamount",0);
	$smarty->assign("codCharges",$codCharge);
	$smarty->assign("productsInCart", $productsInCart);	
	$smarty->assign("emi_charge",$emi_charge);
	$smarty->assign("delivery_date",$delivery_date);
	$smarty->assign("random_integer",rand());//To avoid expensive rand in smarty, assign from here
	$smarty->assign("giftCardConfirmationPage", true);
	if(!empty($cashBackToBeCredited)) 
		$smarty->assign("cashback_tobe_given",$cashBackToBeCredited);
		
	$smarty->assign("fireSyncGACall",$fireSyncGACall);
	$smarty->assign("recordGACall",$recordGACall);

	$fireSyncOmnitureCall = 'false';
	$fireSyncOmnitureCallFG = FeatureGateKeyValuePairs::getFeatureGateValueForKey("confirmation.fireSyncOmnitureCall");
	$fireSyncOmnitureCallFG = false;
	if($fireSyncOmnitureCallFG=='true') {
		$fireSyncOmnitureCall=true;
	} else {
		$fireSyncOmnitureCall=false;
	}
	$fireSyncOmnitureCall=false;
	$smarty->assign("fireSyncOmnitureCall",$fireSyncOmnitureCall);
	
	/*$analyticsObj = AnalyticsBase::getInstance();
	$analyticsObj->setPurchasedEventVars($productsInCart, $paymentoption, $orderid, $totalItemRevenueOmnitureTrack, $cashbackRedeemedOmnitureTrack);
	$analyticsObj->setContextualPageData(AnalyticsBase::CONFIRMATION);
	$analyticsObj->setTemplateVars($smarty);*/
	func_display("cart/giftcardconfirmation.tpl", $smarty);
} else {
	//order status not proper to show confirmation page
	func_header_location($http_location,false);
}

function objectToArray($d) {
	if (is_object($d)) {
		// Gets the properties of the given object
		// with get_object_vars function
		$d = get_object_vars($d);
	}

	if (is_array($d)) {
		/*
		* Return array converted to object
		* Using __FUNCTION__ (Magic constant)
		* for recursive call
		*/
		return array_map(__FUNCTION__, $d);
	}
	else {
		// Return array
		return $d;
	}
}
?>
