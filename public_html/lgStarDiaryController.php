<?php
require_once "auth.php";
require_once 'myntra/navhighlighter.php';
require_once($xcart_dir."/modules/lg/LookGoodAPI.php");

$pageName = 'landing';
include_once $xcart_dir."/webengage.php";
include_once $xcart_dir."/gtm.php";

$id = filter_input(INPUT_GET,"id", FILTER_SANITIZE_STRING);

$starDiaryDetails=LookGoodAPI::getStarDiary($id);

if(empty($starDiaryDetails)){
	$smarty->assign("starDiaryError","error");
}
$currentPageURL=currentPageURL();
$prettyURL=strrpos($currentPageURL,'?');
if(!empty($prettyURL)){
	$pageCleanURL=substr($currentPageURL,0,strrpos($currentPageURL,'?'));
}
else {
	$pageCleanURL=$currentPageURL;
}

$starDiary=$starDiaryDetails->starNStyleDiary;

$nav_arr=array('STAR \'N\' STYLE'=>'/starnstyle',$starDiaryDetails->name.'\'s Diary' => $pageCleanURL);
$smarty->assign("nav_selection_arr_url", $nav_arr);

$smarty->assign("starNStyleName",$starDiaryDetails->name);
$smarty->assign('starDiary',$starDiary);
$smarty->assign("id",$id);
func_display("lg/star-diary.tpl", $smarty);
