<?php
##########################################################################
## Author	: Shantanu Bhadoria											##
## Date 	: 14 Oct'2008												##
## Copyright Myntra Designs												##
##																		##
## Myntra SOAP Server:-													##
## this file specifies/includes any classes for which a SOAP API		##
## has to be Provided. The Restrictions on functions for that class 	##
## need to be specified in the corresponding wsdl file					##
##########################################################################

require '../include/class/class.SOAP.backsync.php';

ini_set('soap.wsdl_cache_enabled', '0'); // disabling WSDL cache
$server = new SoapServer('WSDL/soap.backsync.wsdl');

$server->setClass('SOAP_backsync');
$server->handle();

?>
