<?php
#############################################################################
# Soap client Infrastructure												#
# Author	: Shantanu Bhadoria												#
# Date		: 15 Oct' 2008													#
# This Soap Client achieves basic authentication and transmission of data	#
# to and from the main myntra system in the form of a challenge/requests 	#
# system. 																	#
#############################################################################


class SOAP_CLIENT{
	function create_connection($WSDL){
		ini_set('soap.wsdl_cache_enabled', '0');
		
		try{
			$client = new SoapClient($WSDL,array(
				'trace' 		=> 1,
				'exceptions'	=> 0)
			);
		}catch (SoapFault $exception){
			echo $exception;
		}
		return $client;
	}
}
/*
ini_set('soap.wsdl_cache_enabled', '0');

try{
	$client = new SoapClient('http://208.109.88.113/server/WSDL/soap.createorder.wsdl',array(
		'trace' 		=> 1,
		'exceptions'	=> 0)
	);
}catch (SoapFault $exception){
	echo $exception;
}

$return = $client->createorder(array('abc' => 'main message'));

print "<PRE>";
print_r ($return);
print "\n\nLast Request Header:\n";
print_r ($client->__getLastRequestHeaders());
print "\n\nLast Request:\n";
print_r ($client->__getLastRequest());
print "\n\nLast Response Header:\n";
print_r ($client->__getLastResponseHeaders());
print "\n\nLast response:\n";
print_r ($client->__getLastResponse());

print "</PRE>";
 */
?>

