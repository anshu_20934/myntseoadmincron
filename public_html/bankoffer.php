<?php

require_once "./auth.php";

ini_set("display_errors", '0');
include_once("./include/func/func.mkcore.php");
include_once("./include/func/func.content.php");
include_once("./include/func/func.search.php");
include_once("./include/solr/solrProducts.php");
include_once("./include/class/cache/XCache.php");
include_once("./include/solr/solrQueryBuilder.php");
include_once("./include/solr/solrUtils.php");
include_once("./include/class/home/HomePage.php");
include_once("./include/class/widget/class.pagemapping.widget.php");
include_once("./include/class/widget/class.widget.php");
include_once("./include/func/func_seo.php");
include_once "./include/class/class.recent_viewed_content.php";
require_once("$xcart_dir/include/phpab.php");

$productuiABSwitch = new phpab("productui", false); // set the name of the a/b test
$productuiABSwitch->set_ga_slot('2');

$testVar=ABTestingVariables::getTestVariables('productui');
foreach ($testVar['variations'] as $variation){
	$productuiABSwitch->add_variation_with_probability($variation['name'],'',$variation['p_prob']);
}

$shownewui= $productuiABSwitch->get_user_segment()==='newui';

$smarty->assign("shownewui", $shownewui);

$__RECENT_HISTORY_DCONTENT->setGACategory("hp_strip");
$__RECENT_HISTORY_DCONTENT->genContent();
$__DCONTENT_MANAGER->startCapture();

$model = new HomePage();

$smarty->assign("homepageBannerType", "sliding");
//$images = $model->get_images("new");
$images=array();
//$images[0]['id']="197";
//$images[0]['image'] = "$cdn_base/images/banners/1309776556-50-myntra-3.jpg";
$images[0]['image'] = "$cdn_base/skin1/images/icici-bank2a.jpg";
//$images[0]['banner_type'] = "new";
$images[0]['url'] = "/bankoffer\" onClick=\"return false;\"";
$images[0]['anchor'] = "Get additional 10% discount on payment through ICICI.";
$images[0]['alt'] = "Get additional 10% discount on payment through ICICI.";
//$images[0]['display_order'] = "1";

//print_r($images);exit;
$smarty->assign("images", $images);


foreach($properties['sections'] as $section ){
    $sections [$section['order']] = func_display($section['value'], $smarty, false);
}

$smarty->assign("sections", $sections);


// Get the widget for Most Popular Tag.
// This data is being used in /skin1/myntra/myntra_home.tpl
$widget = new Widget(WidgetMostPopular::$WIDGETNAME);
$widget->setDataToSmarty($smarty);

$homepageWidgets = new WidgetMapping();
$homepageWidgetData = $homepageWidgets->getWidgets(WidgetMapping::$HOMEPAGE);

if(!empty($homepageWidgetData)) {
	$smarty->assign("scrollableWidgets", $homepageWidgetData);
}
$smarty->assign("hideSaleBlock", true);
$home_page_content = func_display("myntra/myntra_home_content.tpl", $smarty, false);
$model->set_home_page($home_page_content);
$smarty->assign("home_page_content", $home_page_content);

$home_page = func_display("myntra/myntra_home.tpl", $smarty, false);
echo $home_page;exit;
?>
