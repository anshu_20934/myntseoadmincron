<?php
require "./auth.php";

if($_GET['action'] == 'reterive')
{
	$ordertable = "";	
	$cdate = time();
	if($_GET['param'] == '7'){
		$pastdate = $cdate-(6*23*60*60);
		$all = false;
	}elseif($_GET['param'] == '1M'){
		$pastdate = $cdate-(30*23*60*60);
		$all = false;
	}elseif($_GET['param'] == '3M'){
		$pastdate = $cdate-(90*23*60*60);
		$all = false;
	}elseif($_GET['param'] == '1Y'){
		$pastdate = $cdate-(365*23*60*60);
		$all = false;
	}elseif($_GET['param'] == 'all'){
		$all = true;
	}elseif($_GET['param'] == '0'){
		$select = "nodata";
	}

	if(($select != "nodata") && ($all == true || $all == false)){

		#
		#Query to load the record of referral history
		#
		$query =	"SELECT a.orderid as orderid, a.status as status, (a.ref_money) as ref_money, 
					 FROM_UNIXTIME(b.date,'%d-%m-%Y') as date , c.firstname , c.lastname ,c.login
					 FROM $sql_tbl[referral_money] a INNER JOIN $sql_tbl[orders] b  ON a.orderid=b.orderid 
					 INNER JOIN  $sql_tbl[customers] as c ON c.login = b.login 
					 WHERE a.login='$login' ";
		
		if($all == false){
			$query .= " AND (b.date>='$pastdate' AND b.date<='$cdate')";			
		}
		$result = func_query($query);
		$sqllog->debug("File Name::>"."ajax_referalhistory.php"."####"."##SQL Query::>".$query);
		if($result)
		{
				$ordertable .= "<div>";
				$ordertable .= "<table cellpadding='2' class='table'>";
				$ordertable .= "<tr class='font_bold'>";
				$ordertable .= "<td class='align_right'>Customer Name</td>";
				$ordertable .= "<td class='align_right'>Date</td>";
				$ordertable .= "<td class='align_right'>Commission Earned</td>";
				$ordertable .= "<td class='align_right'>Commission Used</td>";
				//$ordertable .= "<td class='align_right'>Amount</td>";
				$ordertable .= "</tr>";

				for($i=0; $i<count($result); $i++)
				{
					$arr = $result[$i];
					$ordertable .= "<tr>";
					$ordertable .=	"<td class='align_right'><a style='cursor:pointer;text-decoration:none;' title='$arr[login]'>".$arr['firstname']." ".$arr['lastname']."</a>
					</td>";
					$ordertable .="<td class='align_right'>$arr[date]</td>";
					if($arr['status'] == "C") 
					    $ordertable .="<td class='align_right' style='color:#34C120;'>".abs($arr[ref_money])."</td>";
					else
					     $ordertable .="<td class='align_right' >-</td>"; 
					if($arr['status'] == "D") 
					   $ordertable .="<td class='align_right' style='color:#D51335;'>".abs($arr[ref_money])."</td>";
					else 
					   $ordertable .="<td class='align_right' >-</td>";    
					//$ordertable .="<td class='align_right'>".abs($arr[ref_money])."</td>";
					$ordertable .= "</tr>";
				}
			
			   $sql = "SELECT sum(ref_money) as tot_ref_amt FROM $sql_tbl[referral_money] a
				WHERE a.login='$login' ";
		       $res = func_query_first($sql);
		       $total_referral_amount = number_format($res['tot_ref_amt'],2,".",'');
		       $smarty->assign("total_referral_amount", number_format($total_referral_amount,2,".",''));
			
				$ordertable .="<tr class='font_bold'>";
				$ordertable .="<td class='align_right'>&nbsp;</td>";
				$ordertable .="<td class='align_left'>&nbsp;</td>";
				$ordertable .="<td class='align_right'>&nbsp;</td>";
				$ordertable .="<td class='align_right'>&nbsp;</td>";
				$ordertable .="</tr>";
				$ordertable .="<tr class='font_bold'>";
				$ordertable .="<td class='align_right'>&nbsp;</td>";
				$ordertable .="<td class='align_left'>&nbsp;</td>";
				$ordertable .="<td class='align_right' colspan=2 style='color:#34C120;'><strong>Total referral money earned: Rs. $total_referral_amount </strong></td>";
				$ordertable .="</tr>";
				$ordertable .="</table>";
				$ordertable .="</div>";
				echo $ordertable;
		}
	}
	else{
		echo $ordertable = '0';
	}
}
?>