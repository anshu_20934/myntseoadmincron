<?php
include_once("./auth.php");
if ($skin == 'skin2') {
    require_once "$xcart_dir/checkout-address.php";
    exit;
}

use geo\ZipDAO;

include_once("$xcart_dir/include/func/func.mk_orderbook.php");
include_once("$xcart_dir/include/func/func.utilities.php");
include_once("$xcart_dir/include/class/class.mymyntra.php");
require_once $xcart_dir."/include/func/func.courier.php";

if(empty($_SERVER['HTTPS'])){
	//Page to be accessed only over http redirect him to https
	 func_header_location($https_location .$_SERVER['REQUEST_URI'],false);
}

if($_SERVER['HTTP_HOST'] != $xcart_https_host){
	//redirect if someone opened with any other url
	func_header_location($https_location . $_SERVER['REQUEST_URI'],false);
}


if(empty($login)){
    // If user is not logged in send him to login/register page ..
    func_header_location($http_location."/register.php",false);
}
function getState($state){
        $result = func_select_first('xcart_states', array('code' => $state));
        return empty($result['state'])?$state:$result['state'];
}
function getCountry($country){
        $result = func_select_first('xcart_languages', array('name' => 'country_'.$country,'topic' => 'Countries'));
        return $result['value'];
}

$esc_login = mysql_real_escape_string($login);


$userData = func_select_first('xcart_customers', array('login' => $login));
$firstname = $userData['firstname'];
$lastname = $userData['lastname'];
$promptForName = empty($firstname); // Only firstname is madatory

$name = '';
$yourname = '';
$errors = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add-address'])&&checkCSRF($_POST["_token"],"mkcustomeraddress")) {
    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    $yourname = filter_input(INPUT_POST, 'yourname', FILTER_SANITIZE_STRING);
    $locality = filter_input(INPUT_POST, 'locality', FILTER_SANITIZE_STRING);
    $selectedLocality = filter_input(INPUT_POST, 'selected-locality', FILTER_SANITIZE_STRING);
    $city = filter_input(INPUT_POST, 'city', FILTER_SANITIZE_STRING);
    $country = filter_input(INPUT_POST, 'country', FILTER_SANITIZE_STRING);
    $state = filter_input(INPUT_POST, 'state', FILTER_SANITIZE_STRING);
    $pincode = filter_input(INPUT_POST, 'pincode', FILTER_SANITIZE_NUMBER_INT);
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    $mobile = filter_input(INPUT_POST, 'mobile', FILTER_SANITIZE_NUMBER_INT);

    $address = $_POST['address'];
    $address = preg_replace(array('/&(\w+|#\d+);/', '/[ \t]+/', '/(\n|\r|\r\n)+/'), array('', ' ', "\n"), $address);
    $address = filter_var($address, FILTER_SANITIZE_STRING);

    // strip out non allowed chars from the string inputs
    $rexChars = '/[^\w\d\s#\^&\*\(\)\-\+\{\}\[\]\\\|;:\'"\/\.>,<@]+/';
    foreach (array('name', 'yourname', 'address', 'locality', 'selectedLocality', 'city', 'state') as $var) {
        $$var = iconv("UTF-8", "ASCII//TRANSLIT", $$var);
        $$var = preg_replace($rexChars, '', $$var);
    }

    foreach (array('name', 'address', 'city', 'state', 'pincode', 'email', 'mobile') as $var) {
        if (empty($_POST[$var]))
            $errors[$var] = 'This field is required';
        else if (empty($$var))
            $errors[$var] = 'Please enter valid input';
    }

    if (strlen($address) > 500) {
        $errors['address'] = 'Address can not exceed more than 500 characters';
    }

    if ($abLocality == 'yes') {
        if (empty($_POST['locality']))
            $errors['locality'] = 'This field is required';
        else if (empty($locality))
            $errors['locality'] = 'Please enter valid input';
    }

    $bitPinErrLocality = (!empty($locality) && ($locality == $selectedLocality)) ? 0 : 1;

    if (!empty($promtForName)) {
        if (empty($_POST['yourname']))
            $errors['yourname'] = 'This field is required';
        else if (empty($yourname))
            $errors['yourname'] = 'Please enter valid input';
    }

    if (!empty($country) && $country != 'IN') {
        $errors['country'] = 'Please choose India';
    }

    if (!empty($pincode) && !is_zipcode_servicable_delivery($pincode)) {
        $errors['pincode'] = 'Currently shipment is not possible in this pincode area. We are in the process of expanding our network and apologize for the inconvenience.';
    }
}
$smarty->assign('errors', $errors);


$pinerr = (int)filter_input(INPUT_POST, 'pinerr', FILTER_SANITIZE_NUMBER_INT);
$bitPinErrState = empty($pinerr) ? 0 : 1;

$copyAsAccountName = $_POST['copy-as-acc-name'];
if($copyAsAccountName){
    $yourname_input = mysql_real_escape_string($name);
} else{
    $yourname_input = mysql_real_escape_string($yourname);
}
if(!empty($yourname_input)&&checkCSRF($_REQUEST["_token"], "mkcustomeraddress")){
    $namearr = MyMyntra::splitFirstLastName($yourname_input);
    db_query("update  xcart_customers set firstname = '$namearr[0]', lastname='$namearr[1]' where login ='$login'");
}

if (!empty($errors)) {
    // keep the form showing with error messages. No redirection to payment page

} else if (!empty($name) && !empty($address) && !empty($locality) && !empty($city)
        && !empty($country) && !empty($state) && !empty($pincode) && !empty($email) && !empty($mobile)) {
    $errmask = "b'" . $bitPinErrLocality . $bitPinErrState . "'";
    if(func_query_first_cell("select count(*) from mk_customer_address where login = '$esc_login'")>0){
    	$data = array(
	        'login' => $login,
	        'name' => $name,
	        'address' => $address,
	        'locality' => $locality,
	        'city' => $city,
	        'state' => $state,
	        'country' => $country,
	        'pincode' => $pincode,
	        'email' => $email,
	        'mobile' => $mobile,
	        'datecreated' => date("Y-m-d H:i:s"),
        	'errmask' => array("value"=>$errmask, "type"=>"binary")
    	);
    } else {
    	$data = array(
	        'login' => $login,
    		'default_address' => 1,
	        'name' => $name,
	        'address' => $address,
	        'locality' => $locality,
	        'city' => $city,
	        'state' => $state,
	        'country' => $country,
	        'pincode' => $pincode,
	        'mobile' => $mobile,
	        'email' => $email,
	        'datecreated' => date("Y-m-d H:i:s"),
        	'errmask' =>  array("value"=>$errmask, "type"=>"binary")
    	);
    }

    $id = func_array2insertWithTypeCheck('mk_customer_address', $data);

    func_header_location($https_location . "/mkpaymentoptions.php?address=".$id."#shipping-section",false);

} else if (isset($_GET['remove'])) {
	if(checkCSRF($_REQUEST["_token"]," mkcustomeraddress")){
		$id = mysql_real_escape_string($_GET['id']);
		$result = func_select_first('mk_customer_address', array('id' => $id));
		if ($result['login'] == $login) {
			db_query("delete from mk_customer_address where id ={$id}");
		}
	}
    func_header_location($https_location . "/mkcustomeraddress.php?change=true",false);
}  else if (isset($_POST['address']) && !$promptForName) {
	if(checkCSRF($_POST["_token"], "mkcustomeraddress")){
		/**
		 *  Only if a default address is checked do we update the row ..
		 *
		 */
		$address = mysql_real_escape_string($_POST['address']);
		$default_address = mysql_real_escape_string($_POST['default_address']);
		if ($default_address) {
			db_query("update  mk_customer_address set default_address = 0 where login ='{$login}'");
			db_query("update  mk_customer_address set default_address = 1 where id ='{$default_address}'");
		}
	}
    func_header_location($https_location . "/mkpaymentoptions.php?address=".$address."#shipping-section",false);

} else if(!isset($_GET['change']) && !$promptForName) {
    // If user has a default address and has a name stored in db.. move on to payment options page where he does have a option of coming back and changing the address ..
    $address = func_select_first('mk_customer_address', array('login' => $login, 'default_address' => 1));
    if(!empty($address) && $address['country'] == 'IN' && is_zipcode_servicable_delivery($address['pincode'])){
        func_header_location($https_location . "/mkpaymentoptions.php?address=".$address['id']."#shipping-section",false);
    } else {
    	$address = func_select_first('mk_customer_address', array('login' => $login),0,1,array('datecreated' => 'desc'));
    	if(!empty($address) && $address['country'] == 'IN' && is_zipcode_servicable_delivery($address['pincode'])){
        	func_header_location($https_location . "/mkpaymentoptions.php?address=".$address['id']."#shipping-section",false);
    	}
    }
} else if(isset($_POST['addname']) && !$promptForName){
    // In case user has submitted the form just to add his name then after updating the user's name we need to move him to mkpaymentoptions page
	func_header_location($https_location . "/mkpaymentoptions.php#shipping-section",false);
}

$addresses_all = func_select_query('mk_customer_address', array('login' => $login), 0, 9, array('datecreated' => 'desc'));
$addresses = array();
foreach($addresses_all as $key => $address){
    if ($address['country'] != 'IN' || !is_zipcode_servicable_delivery($address['pincode'])) {
        continue;
    }
    $addresses[] = $address;
    if (count($addresses) >= 3) break;
}

$country = isset($addresses[0]) ? $addresses[0]['country'] : "";
$city = isset($addresses[0]) ?  $addresses[0]['state'] : "";
$state = isset($addresses[0]) ?  $addresses[0]['city'] : "";
foreach($addresses as $key=>$address){
        $state=getState($address['state']);
        $addresses[$key]['state_code']=$address['state'];
        $addresses[$key]['state']=$state;
        //$country=getCountry($address['country']);
        //Passing country code instead of name because drop down expects code to match selected
        $country=$address['country'];
        $addresses[$key]['country_code']=$address['country'];
        $addresses[$key]['country']=getCountry($country);
}

//if ($country == "") {
//PORTAL-385: When entering a new Shipping Address from UI in Payment Page, the Default Country
// was earlier the same as the country for the last created/modified address.Now we want it to be India always
    $country = "IN";
//}

$countries = array(); //func_get_countries();
$states = func_get_states($country);
$username = $lastname != "" ? $firstname . " " . $lastname : $firstname;
$smarty->assign("username", $username);
$smarty->assign("promptForName", $promptForName);
$smarty->assign("addresses", $addresses);
$smarty->assign("default", $addresses[0]);
$smarty->assign("countries", $countries);
$smarty->assign("country", $country);
$smarty->assign("states", $states);
$smarty->assign("userData", $userData);

if (empty($name)) $name = $username;
$smarty->assign('name', $name);

const STATEZIPS_CACHEKEY = "statezips_xcache";
global $xcache ;
if($xcache==null){
	$xcache = new XCache();
}

$stateZipsJson = $xcache->fetchAndStore(function() {
	require_once "geoinit.php";
	$zipDAO = new ZipDAO();
	$stateZips = $zipDAO->getAllStateLevelZips();
	return json_encode($stateZips);
}, array(), STATEZIPS_CACHEKEY);

$smarty->assign("stateZipsJson",$stateZipsJson);

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setContextualPageData(AnalyticsBase::CHECKOUT_SHIPPING);
$analyticsObj->setTemplateVars($smarty);

func_display("cart/address.tpl", $smarty);
