<?php

// Function to convert number to words ex: 33 -> thirty three
$words = array('0'=> '' ,'1'=> 'one' ,'2'=> 'two' ,'3' => 'three','4' => 'four','5' => 'five','6' => 'six','7' => 'seven','8' => 'eight','9' => 'nine','10' => 'ten','11' => 'eleven','12' => 'twelve','13' => 'thirteen','14' => 'fouteen','15' => 'fifteen','16' => 'sixteen','17' => 'seventeen','18' => 'eighteen','19' => 'nineteen','20' => 'twenty','30' => 'thirty','40' => 'fourty','50' => 'fifty','60' => 'sixty','70' => 'seventy','80' => 'eighty','90' => 'ninty','100' => 'hundred &','1000' => 'thousand','100000' => 'lakh','10000000' => 'crore');
function no_to_words($no)
{    global $words;
    if($no == 0)
        return ' ';
    else {           $novalue='';$highno=$no;$remainno=0;$value=100;$value1=1000;       
            while($no>=100)    {
                if(($value <= $no) &&($no  < $value1))    {
                $novalue=$words["$value"];
                $highno = (int)($no/$value);
                $remainno = $no % $value;
                break;
                }
                $value= $value1;
                $value1 = $value * 100;
            }       
          if(array_key_exists("$highno",$words))
              return $words["$highno"]." ".$novalue." ".no_to_words($remainno);
          else {
             $unit=$highno%10;
             $ten =(int)($highno/10)*10;            
             return $words["$ten"]." ".$words["$unit"]." ".$novalue." ".no_to_words($remainno);
           }
    }
}

function convert_coupon_numbers_to_words(&$mrpCouponConfiguration){

	$value = $mrpCouponConfiguration[mrp_firstLogin_nonfbreg_numCoupons];
	switch ($value){
		case 1:
			$mrpCouponConfiguration['mrp_firstLogin_nonfbreg_textNumCoupons'] = 'a';			
			break;
		default:
			$mrpCouponConfiguration['mrp_firstLogin_nonfbreg_textNumCoupons'] = no_to_words($value); 
			break;	
	}
	
	$value = $mrpCouponConfiguration['mrp_firstLogin_fbreg_numCoupons'];
	switch ($value){
		case 1:
			$mrpCouponConfiguration['mrp_firstLogin_fbreg_textNumCoupons'] = 'a';			
			break;
		default:
			$mrpCouponConfiguration['mrp_firstLogin_fbreg_textNumCoupons'] = no_to_words($value); 
			break;	
	}
	
	$value = $mrpCouponConfiguration['mrp_refFirstPurchase_numCoupons'];
	switch ($value){
		case 1:
			$mrpCouponConfiguration['mrp_refFirstPurchase_textNumCoupons'] = 'a';			
			break;
		default:
			$mrpCouponConfiguration['mrp_refFirstPurchase_textNumCoupons'] = no_to_words($value); 
			break;	
	}
	
	$value = $mrpCouponConfiguration['mrp_refRegistration_numCoupons'];
	switch ($value){
		case 1:
			$mrpCouponConfiguration['mrp_refRegistration_textNumCoupons'] = 'a';			
			break;
		default:
			$mrpCouponConfiguration['mrp_refRegistration_textNumCoupons'] = no_to_words($value); 
			break;	
	}
}

?>