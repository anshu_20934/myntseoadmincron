<?php
chdir(dirname(__FILE__));
require_once '../auth.php';

/*if($configMode=="local"||$configMode=="test"||$configMode=="release"){
	$SECURE_SECRET = "4DD4E4AF122706E9579040B956677B73";
} else {*/
	$SECURE_SECRET = "5A0FEEDF45C6B00190DF81EB6E8A149E";
//}

/**
* This method is for sorting the fields and creating an MD5 secure hash.
*
* @param fields is a map of all the incoming hey-value pairs from the VPC
* @param buf is the hash being returned for comparison to the incoming hash
*/
function hashAllFields($fields = array()) {
	global $SECURE_SECRET;
	// create a list and sort it
	ksort($fields);
	//print_r($fields);exit;
	$buf=$SECURE_SECRET;
	// iterate through the list and add the remaining field values	
	foreach ($fields as $key=>$value){
		if(isset($value)) $buf = $buf.$value;
	}	
	
	$md5String = md5($buf);
	
	return $md5String;
    
} // end hashAllFields()

//  ----------------------------------------------------------------------------

/**
* This method is for creating a URL query string.
*
* @param buf is the inital URL for appending the encoded fields to
* @param fields is the input parameters from the order page
*/
// Method for creating a URL query string
function appendQueryFields($buf,$fields=array()) {        
	// move through the list and create a series of URL key/value pairs
	$toImplode = array();
	foreach ($fields as $fieldName=>$fieldValue) {
		if(!empty($fieldValue)) {
			$toImplode[] = urlencode($fieldName)."=".urlencode($fieldValue);			
		}		
	} 
	$buf = $buf."?".implode("&",$toImplode);
	return $buf;    
} // appendQueryFields()

//  ----------------------------------------------------------------------------

//  ----------------------------------------------------------------------------

/*
 * This method takes a data String and returns a predefined value if empty
 * If data Sting is null, returns string "No Value Returned", else returns input
 *
 * @param in String containing the data String
 * @return String containing the output String
 */
function null2unknown($in) {
	if (isset($in)) {
		return $in;
	} else {
		return "No Value Returned";
	}
} // null2unknown()

//  ----------------------------------------------------------------------------

/*
 * This function uses the returned status code retrieved from the Digital
 * Response and returns an appropriate description for the code
 *
 * @param vResponseCode String containing the vpc_TxnResponseCode
 * @return description String containing the appropriate description
 */
function getResponseDescription($vResponseCode) {
	$result = "";
	// check if a single digit response code
	if (strlen($vResponseCode) == 1) {
		switch ($vResponseCode) {
			case '0' : $result = "Transaction Successful"; break;
			case '1' : $result = "Unknown Error"; break;
			case '2' : $result = "Bank Declined Transaction"; break;
			case '3' : $result = "No Reply from Bank"; break;
			case '4' : $result = "Expired Card"; break;
			case '5' : $result = "Insufficient Funds"; break;
			case '6' : $result = "Error Communicating with Bank"; break;
			case '7' : $result = "Payment Server System Error"; break;
			case '8' : $result = "Transaction Type Not Supported"; break;
			case '9' : $result = "Bank declined transaction (Do not contact Bank)"; break;
			case 'A' : $result = "Transaction Aborted"; break;
			case 'C' : $result = "Transaction Cancelled"; break;
			case 'D' : $result = "Deferred transaction has been received and is awaiting processing"; break;
			case 'F' : $result = "3D Secure Authentication failed"; break;
			case 'I' : $result = "Card Security Code verification failed"; break;
			case 'L' : $result = "Shopping Transaction Locked (Please try the transaction again later)"; break;
			case 'N' : $result = "Cardholder is not enrolled in Authentication Scheme"; break;
			case 'P' : $result = "Transaction has been received by the Payment Adaptor and is being processed"; break;
			case 'R' : $result = "Transaction was not processed - Reached limit of retry attempts allowed"; break;
			case 'S' : $result = "Duplicate SessionID (OrderInfo)"; break;
			case 'T' : $result = "Address Verification Failed"; break;
			case 'U' : $result = "Card Security Code Failed"; break;
			case 'V' : $result = "Address Verification and Card Security Code Failed"; break;
			case '?' : $result = "Transaction status is unknown"; break;
			default  : $result = "Unable to be determined";
		}
		return $result;
	} else {
		return "No Value Returned";
	}
} // getResponseDescription()

//  ----------------------------------------------------------------------------

/**
 * This function uses the QSI AVS Result Code retrieved from the Digital
 * Receipt and returns an appropriate description for this code.
 *
 * @param vAVSResultCode String containing the vpc_AVSResultCode
 * @return description String containing the appropriate description
 */
function displayAVSResponse($vAVSResultCode) {
	$result = "";
	if (isset($vAVSResultCode)) {
		if (strcasecmp($vAVSResultCode,"Unsupported")==0 || strcasecmp($vAVSResultCode,"No Value Returned")==0) {
			$result = "AVS not supported or there was no AVS data provided";
		} else {                
			switch ($vAVSResultCode){
				case 'A' : $result = "Address match only"; break;
				case 'B' : $result = "Street address matches, but postal code could not be verified because of incompatible formats."; break;
				case 'C' : $result = "Neither the street address nor the postal code could be verified because of incompatible formats."; break;
				case 'D' : $result = "Visa Only. Street addresses and postal code match.";break;
				case 'E' : $result = "Address and ZIP/postal code not provided"; break;
				case 'F' : $result = "Visa Only, UK-issued cards. Acquirer sent both street address and postal code; both match.";break;
				case 'G' : $result = "Issuer does not participate in AVS (international transaction)"; break;
				case 'I' : $result = "Visa Only. Address information not verified for international transaction.";break;
				case 'M' : $result = "Visa Only. Street addresses and postal codes match."; break;
				case 'N' : $result = "Address and ZIP/postal code not matched"; break;
				case '0' : $result = "AVS not requested"; break;
				case 'P' : $result = "Visa Only. Postal code matches, but street address not verified because of incompatible formats."; break;
				case 'R' : $result = "Issuer system is unavailable"; break;
				case 'S' : $result = "Service not supported or address not verified (international transaction)"; break;
				case 'U' : $result = "Address unavailable or not verified"; break;
				case 'W' : $result = "9 digit ZIP/postal code matched, Address not Matched"; break;
				case 'X' : $result = "Exact match - address and 9 digit ZIP/postal code"; break;
				case 'Y' : $result = "Exact match - address and 5 digit ZIP/postal code"; break;
				case 'Z' : $result = "5 digit ZIP/postal code matched, Address not Matched"; break;
				default  : $result = "Unable to be determined";
			}
		}
	} else {
		$result = "null response";
	}
	return $result;
}

//  ----------------------------------------------------------------------------

/**
 * This function uses the QSI CSC Result Code retrieved from the Digital
 * Receipt and returns an appropriate description for this code.
 *
 * @param vCSCResultCode String containing the vpc_CSCResultCode
 * @return description String containing the appropriate description
 */
function displayCSCResponse($vCSCResultCode) {
	$result = "";
	if (isset($vCSCResultCode)) {

		if ((strcasecmp($vCSCResultCode, "Unsupported")==0)  || (strcasecmp($vCSCResultCode,"No Value Returned")==0)) {
			$result = "CSC not supported or there was no CSC data provided";
		} else {
			switch ($vCSCResultCode){
				case 'M' : $result = "Exact code match"; break;
				case 'S' : $result = "Merchant has indicated that CSC is not present on the card (MOTO situation)"; break;
				case 'P' : $result = "Code not processed"; break;
				case 'U' : $result = "Card issuer is not registered and/or certified"; break;
				case 'N' : $result = "Code invalid or not matched"; break;
				default  : $result = "Unable to be determined";
			}
		}

	} else {
		$result = "null response";
	}
	return $result;
}

//  ----------------------------------------------------------------------------

/**
 * This method uses the 3DS verStatus retrieved from the
 * Response and returns an appropriate description for this code.
 *
 * @param vpc_VerStatus String containing the status code
 * @return description String containing the appropriate description
 */
function getStatusDescription($vStatus) {
	$result = "";
	if (isset($vStatus)) {

		if ( (strcasecmp($vStatus, "Unsupported")==0)  || (strcasecmp($vStatus,"No Value Returned")==0)) {
			$result = "3DS not supported or there was no 3DS data provided";
		} else {
			switch ($vStatus){
				case 'Y'  : $result = "The cardholder was successfully authenticated."; break;
				case 'E'  : $result = "The cardholder is not enrolled."; break;
				case 'N'  : $result = "The cardholder was not verified."; break;
				case 'U'  : $result = "The cardholder's Issuer was unable to authenticate due to some system error at the Issuer."; break;
				case 'F'  : $result = "There was an error in the format of the request from the merchant."; break;
				case 'A'  : $result = "Authentication of your Merchant ID and Password to the ACS Directory Failed."; break;
				case 'D'  : $result = "Error communicating with the Directory Server."; break;
				case 'C'  : $result = "The card type is not supported for authentication."; break;
				case 'S'  : $result = "The signature on the response received from the Issuer could not be validated."; break;
				case 'P'  : $result = "Error parsing input from Issuer."; break;
				case 'I'  : $result = "Internal Payment Server system error."; break;
				default   : $result = "Unable to be determined"; break;
			}
		}
	} else {
		$result = "null response";
	}
	return $result;
}
?>