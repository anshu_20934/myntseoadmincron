{include file="macros/base-macros.tpl"}
<script>
    dataLayer.push({ldelim}
        'noResultsPage': Myntra.Search.Data.noResultsPage,
        'navId': Myntra.Search.Data.navId,
        'priceRangeMax': Myntra.Search.Data.priceRangeMax,
        'priceRangeMin': Myntra.Search.Data.priceRangeMin,
        'listGroup': Myntra.Search.Data.listGroup,
        'searchCategoryList': Myntra.Search.Data.searchCategoryList,
        'searchTopCategory': Myntra.Search.Data.searchTopCategory,
        'searchSubCategoryList': Myntra.Search.Data.searchSubCategoryList,
        'searchTopSubCategory': Myntra.Search.Data.searchTopSubCategory,
        'searchBrandList': Myntra.Search.Data.searchBrandList,
        'searchTopBrand': Myntra.Search.Data.searchTopBrand,
        'searchGenderList': Myntra.Search.Data.searchGenderList,
        'searchTopGender': Myntra.Search.Data.searchTopGender,
        'searchArticleTypeList': Myntra.Search.Data.searchArticleTypeList,
        'searchTopArticleType': Myntra.Search.Data.searchTopArticleType,
        'searchColorList': Myntra.Search.Data.searchColorList,
        'searchTopColor': Myntra.Search.Data.searchTopColor,
    {rdelim});

    $(document).ready(function(){
        var actionNlabel = getGAActionAndLabel(gaEventCategory,gaEventArticleType,gaEventGender,gaEventBrand);
        _gaq.push(['_trackEvent','Search_conversion', actionNlabel.action, actionNlabel.label, 0, true]);
    });
</script>
