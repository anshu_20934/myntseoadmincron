{include file="macros/base-macros.tpl"}
<script>
    if (!!Myntra.Cart.Data) {
        var items = JSON.parse(Myntra.Cart.Data.items);
        var styleIds = [];

        for (var i=0, len=items.length; i<len; ++i) {
            var item = items[i];
            styleIds.push(item.id);
        }
        var styleIdList = styleIds.join();
        var temp = {ldelim}
            'cartIsGiftOrder':Myntra.Cart.Data.isGiftOrder,
            'cartItems':Myntra.Cart.Data.items,
            'cartItemIds' : styleIdList,
    {foreach from=$_cart key=idx item=item}
            'cartItem{$idx}' : {$item.id},
    {/foreach} 
            'cartAmount':Myntra.Cart.Data.amount,
            'cartTotalAmount':Myntra.Cart.Data.totalAmount,
            'cartTotalQuantity':Myntra.Cart.Data.totalQuantity,
            'cartMrp':Myntra.Cart.Data.mrp,
            'cartCartLevelDiscount':Myntra.Cart.Data.cartLevelDiscount,
            'cartCouponDiscount':Myntra.Cart.Data.couponDiscount,
            'cartCashDiscount':Myntra.Cart.Data.cashDiscount,
            'cartTotalCashBackAmount':Myntra.Cart.Data.totalCashBackAmount,
            'cartCashBackAmountDisplayOnCart':Myntra.Cart.Data.cashBackAmountDisplayOnCart,
            'cartShippingCharge':Myntra.Cart.Data.shippingCharge,
            'cartGiftCharge':Myntra.Cart.Data.giftCharge,
            'cartSavings':Myntra.Cart.Data.savings,
            'cartProductAdded':Myntra.Cart.Data.productAdded,
        {rdelim};
         if (!!Myntra.Cart.Data.curEvent){
            temp['event']= Myntra.Cart.Data.curEvent;
        }
        dataLayer.push(temp);
    }
    

</script>
