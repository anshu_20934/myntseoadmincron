$(document).ready(function() {
    $(document).bind('myntra.login.show', function(e, data) {
        //Generate custon events for mobile app 
        var loginEvent = new CustomEvent("myntra.mobileapp.login.show", data);
        document.dispatchEvent(loginEvent);
    });
});