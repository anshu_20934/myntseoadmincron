<?php
/*
 * Created on 28-Jun-2011
 *
 * Send abandoned cart mailers to logged in users who added/modified their carts but didn't go to payments page
 * i.e. for users for which no order was created
 * This should be sent by cron-job at 4am IST everyday
 * FeatureGateParameter: how many days looking behind should it track the abandoned cart?
 */
 include_once "auth.php";
 
 $today = time();
 $year = date("Y", $today);
 $month = date("m", $today);
 $day = date("d", $today);
 $numDays = FeatureGateKeyValuePairs::getInteger("AbandonedCartMailer.NumDays", 1);
 //Start from 12am of numDays before today
 $startFrom = mktime(0,0,0,$month, $day-$numDays, $year);
 //End upto 12am of the next day after startFrom
 $endUpto = $startFrom + 24*3600;
 //Look for abandoned carts only in the interval: startFrom upto endUpto
 $abandoned_cart_sql = "select mk_shopping_cart.login, firstname, lastname, from_unixtime(last_modified) as CartLastModifiedDate, cart_data "
." from mk_shopping_cart inner join xcart_customers on mk_shopping_cart.login = xcart_customers.login"
." where length(cart_data) > 150"
." and (last_modified is not null)" 
." and last_modified between $startFrom and $endUpto"
." and mk_shopping_cart.login is not null"
." and mk_shopping_cart.login != ''"
." and mk_shopping_cart.login not in ("
."	select login from xcart_orders where date > $startFrom"
.")";
$rset = db_query($abandoned_cart_sql);

$smarty->assign("campaignDate", date("Ymd",$today));
$smarty->assign("discountUpto", FeatureGateKeyValuePairs::getInteger("AbandonedCartMailer.DiscUpto", 10));
$abandonedCartExists = false;

while ($row = mysql_fetch_assoc($rset)) {
	$login = $row["login"];
	$lastModified = $row["CartLastModifiedDate"];
	$cartData = unserialize(stripslashes($row["cart_data"]));
	$productsInCart = $cartData["cart"];
	$smarty->assign("productsInCart",$productsInCart);
	$smarty->assign("login",$login);
	$smarty->assign("lastModified",$lastModified);
	$smarty->assign("customerName",getCustomerName($row));
	
	$mailHtmlBody = $smarty->fetch("cart/emailcart.tpl", $smarty);
	$subject = "There are some items in your cart waiting to be bought at Myntra.com";
	$flag = sendMessageInlineBody($mailHtmlBody, "You have items on your cart", $login);
	echo "Mail Status to: ".$login.";flag:".$flag."<BR>";
	if(!$abandonedCartExists)
		$abandonedCartExists = true;
}
if(!$abandonedCartExists){
	echo "There are no abandoned carts for logged-in users between ". date("r",$startFrom). " and ".date("r",$endUpto)." for whom orders were not generated after ".date("r",$startFrom);
}	

function getCustomerName($row) {
	$customerName = "";
	if(!empty($row["firstname"])) {
		$customerName = $row["firstname"];
	}
	if(!empty($row["lastname"])) {
		$customerName .= " ".$row["lastname"];
	}
	if(empty($customerName)) {
		//$email = explode("@",$row["login"]);
		$customerName = "Customer";//$email[0];
	} 
	return $customerName;
}
?>
