USE `myntra`;
DROP TABLE IF EXISTS `mk_product_group`;

CREATE TABLE `mk_product_group` (
  `id` int(11) NOT NULL default '0',
  `name` varchar(255) default NULL,
  `label` varchar(255) default NULL,
  `description` text,
  `default_style_id` int(11) default NULL,
  `type` int(11) default NULL,
  `url` varchar(2048) default NULL,
  `image` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `mk_product_group` */

insert into `mk_product_group` (`id`,`name`,`label`,`description`,`default_style_id`,`type`,`url`,`image`) values (1,'Apparels','Apparels','Garments fall in this category',3,1,NULL,'skin1/mkimages/product_groups/apparel.jpg');
insert into `mk_product_group` (`id`,`name`,`label`,`description`,`default_style_id`,`type`,`url`,`image`) values (2,'Print On','Print On','Products made of ceramic',707,2,'printon','skin1/mkimages/product_groups/print-on.jpg');
insert into `mk_product_group` (`id`,`name`,`label`,`description`,`default_style_id`,`type`,`url`,`image`) values (3,'Mugs & Drink ware','Mugs & Drink ware','Mugs & Drink ware',14,1,NULL,'skin1/mkimages/product_groups/mug.jpg');
insert into `mk_product_group` (`id`,`name`,`label`,`description`,`default_style_id`,`type`,`url`,`image`) values (4,'Calendar & Diaries     ','Calendar & Diaries   ','Calendar & Diaries',30,1,NULL,'skin1/mkimages/product_groups/calendar_diaries.jpg');
insert into `mk_product_group` (`id`,`name`,`label`,`description`,`default_style_id`,`type`,`url`,`image`) values (5,'Writing Instruments   ','Writing Instruments  ','Writing Instruments  ',827,2,'brandedpens','skin1/mkimages/product_groups/writing_instruments.jpg');
insert into `mk_product_group` (`id`,`name`,`label`,`description`,`default_style_id`,`type`,`url`,`image`) values (6,'Cards & Posters','Cards & Posters','Cards & Posters',390,1,NULL,'skin1/mkimages/product_groups/cards_posters.jpg');
insert into `mk_product_group` (`id`,`name`,`label`,`description`,`default_style_id`,`type`,`url`,`image`) values (7,'Fun Stuff        ','Fun Stuff    ','Fun Stuff    ',437,1,NULL,'skin1/mkimages/product_groups/fun_stuff.jpg');
insert into `mk_product_group` (`id`,`name`,`label`,`description`,`default_style_id`,`type`,`url`,`image`) values (8,'Desktop Products','Desktop Products','Desktop Products',411,1,NULL,'skin1/mkimages/product_groups/desktop_products.jpg');
insert into `mk_product_group` (`id`,`name`,`label`,`description`,`default_style_id`,`type`,`url`,`image`) values (9,'Keyrings','Keyrings','Keyrings',20,1,NULL,'skin1/mkimages/product_groups/apparel.jpg');
insert into `mk_product_group` (`id`,`name`,`label`,`description`,`default_style_id`,`type`,`url`,`image`) values (10,'Men\'s Accessories','Men\'s Accessories','Men\'s Accessories',738,1,NULL,'skin1/mkimages/product_groups/keyrings.jpg');
insert into `mk_product_group` (`id`,`name`,`label`,`description`,`default_style_id`,`type`,`url`,`image`) values (11,'Women\'s Accessories','Women\'s Accessories','Women\'s Accessories',109,1,NULL,'skin1/mkimages/product_groups/women_accessories.jpg');
insert into `mk_product_group` (`id`,`name`,`label`,`description`,`default_style_id`,`type`,`url`,`image`) values (12,'Watches','Watches','Watches',86,1,NULL,'skin1/mkimages/product_groups/watch.jpg');