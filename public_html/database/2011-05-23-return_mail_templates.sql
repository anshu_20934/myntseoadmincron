delete from mk_email_notification where name in ('return_details_table', 'return_confirmation_self', 'return_confirmation_pickup', 'return_process_success', 'return_process_failed','return_confirmation_self','return_update_reminder','return_recieved','return_reshipped');

insert into mk_email_notification(name, subject, body) values('return_details_table', '',
'<table border=1 cellspacing=0 cellpadding=0 style="width:100%;">
<thead style="background-color:#E0E0E0;">
<th width="34%" style="padding: 10px 5px;" align=left>Product</th>
<th width="34%" style="padding: 10px 5px;" align=left>Price</th>
<th width="5%" style="padding: 10px 5px;" align=left>Discount</th>
<th width="5%" style="padding: 10px 5px;" align=left>Size</th>
<th width="10%" style="padding: 10px 5px;" align=left>Quantity</th>
<th width="10%" style="padding: 10px 5px;" align=right>Total</th>
</thead>
<tbody>[ITEM_DETAILS]</tbody>
</table>');

insert into mk_email_notification(name, subject, body) values('return_confirmation_self', 'Confirmation of returns request – Return ID. [RETURN_ID]',
'Hi [USER], <br/><br/>
We have received your request for return of the following item(s). Please note your Return ID. [RETURN_ID] for your reference. You are requested to quote this on any communication related to this return. <br/><br/>
Here are the details of your Return No. [RETURN_ID], for your reference - <br/><br/> 
[RETURN_DETAILS]
<br/>
<b>Steps to return your product:</b><br/>
<p style="padding-left: 10px;">1. Fill out the Returns Form shipped with your order. In case you have misplaced the form, you can download the same from <a href="[MY_RETURNS_URL]">My Returns</a> section under My Myntra.</p>
<p style="padding-left: 10px;">2. Fill out the required details and sign the form.</p>
<p style="padding-left: 10px;">3. Re-pack the product you want to return, along with the above returns form and order invoice, making sure to include all accompanying material the item was originally delivered to you with.</p>
<p style="padding-left: 10px;">4. Download and print address label from My Return Requests section on My Myntra.</p>
    <span style="padding-left: 10px;"><b>No-printer option:</b> Please inscribe the following address on the consignment:</span><br/>
    <p style="padding-left: 30px;">[MYNTRA_ADDRESS]</p>
</p>
<p style="padding-left: 10px;">5. Ship the package and retain the receipt (docket no.) till we have confirmed receipt of the consignment.
Please update the shipment tracking details of your returns consignment on the My Returns section of My Myntra as this would enable us to track your consignment and make sure it reaches us.</p>
<p>This mail is for your records only and you are requested not to send a response to this auto-generated mail. You can track the status of your return requests under the <a href="[MY_RETURNS_URL]">My Returns</a> section in My Myntra. In case of any further queries, please contact our Customer Care at [MYNTRA_CC_PHONE]</p>
<br/>
Regards,<br/>
Myntra Customer Care');

insert into mk_email_notification(name, subject, body) values('return_confirmation_pickup', 'Confirmation of returns request – Return No. [RETURN_ID]',
'Hi [USER], <br/><br/>
We have received your request for return of the following items. Please note your Return No. [RETURN_ID]. You are requested to quote this on any communication related to this return.<br/><br/>
Here are the details of your Return No. [RETURN_ID], for your reference - <br/>
[RETURN_DETAILS]
<br/>
<b>Steps to return your product:</b><br/>
<p style="padding-left: 10px;">1. Open the <a href="[MY_RETURNS_URL]">My Returns</a> section under My Myntra.</p>
<p style="padding-left: 10px;">2. Click the “Download returns form”.</p>
<p style="padding-left: 10px;">3. Print out the downloaded form, fill out the required details and sign the form.<br/>
    <span style="padding-left: 15px;">No-printer option: Please note the following particulars on a sheet of a paper to serve as a returns form:</span><br/>
    <span style="padding-left: 15px;">a. Order No</span><br/>
    <span style="padding-left: 15px;">b. Return No</span><br/>
    <span style="padding-left: 15px;">c. Customer Login</span><br/>
    <span style="padding-left: 15px;">d. Request Date</span><br/>
</p>
<p style="padding-left: 10px;">4. Re-pack the product you want to return making sure to include all accompanying material the item was originally delivered to you with.</p>
<p style="padding-left: 10px;">5. Paste the returns form on the reverse side of package such that the entire form is visible.</p>
<p style="padding-left: 10px;">6. Download and print address label from My Return Requests section on My Myntra.<br/>
    <span style="padding-left: 10px;">No-printer option: Please inscribe the following address on the consignment:</span><br/>
    <p style="padding-left: 30px;">[MYNTRA_ADDRESS]</p>
</p>
<p style="padding-left: 10px;">7. Please hand over the consignment to our logistics partner, when they visit your doorstep for pickup. Please retain the receipt (docket no.) till we have confirmed receipt of the consignment.</p>
<p>This mail is for your records only and you are requested not to send a response to this auto-generated mail. You can track the status of your return requests under the <a href="[MY_RETURNS_URL]">My Returns</a> section in My Myntra. In case of any further queries, please contact our Customer Care at [MYNTRA_CC_PHONE]</p>
<br/>
Regards,<br/>
Myntra Customer Care');


insert into mk_email_notification(name, subject, body) values('return_update_reminder', 'Reminder to update returns tracking details (Returns ID: [RETURN_ID])',
'Hi [USER], <br/><br/>
You had placed a request for the return of the below product on [RETURN_CREATED_DATE].We would like to remind you to update the details of the same on the My Returns page on My Myntra. We would want to reiterate the importance of doing so, as it would enable us to track your returns consignment and ensure that it reaches us safely.<br/><br/>
[RETURN_DETAILS]
<br/><br/>
<p>We would like to inform you that your returns request would be deemed as lapsed or cancelled in case you do not update the tracking details, or if we do not receive your returns within 30 days of your request date.</p>
<br/><br/>
<p>Thanks in advance for your cooperation.</p><br/><br/> 
<p>This mail is for your records only and you are requested not to send a response to this auto-generated mail. You can track the status of your return requests under the <a href="[MY_RETURNS_URL]">My Returns</a> section in My Myntra.</p>
Regards,<br/>
Myntra Customer Care');


insert into mk_email_notification(name, subject, body) values('return_recieved', 'Returns ID: [RETURN_ID] - received at our returns processing facility',
'Hi [USER], <br/><br/>
You had placed a request for the return of the below product on [RETURN_CREATED_DATE], and we would like to confirm that we have received it at our returns processing facility. <br/><br/>
Here are the details of your Return No. [RETURN_ID], for your reference - <br/><br/>
[RETURN_DETAILS]
<br/><br/>
<p>The processing of your returns would usually take less than 2 working days. In case you do not hear back from us within this time frame, please contact Customer Care at [MYNTRA_CC_PHONE].</p> 
<p>This mail is for your records only and you are requested not to send a response to this auto-generated mail. You can track the status of your return requests under the <a href="[MY_RETURNS_URL]">My Returns</a> section in My Myntra.</p>
Regards,<br/>
Myntra Customer Care');



insert into mk_email_notification(name, subject, body) values('return_process_success', 'Return ID: [RETURN_ID] Processed Successfully',
'Hi [USER], <br/><br/>
<p>We are pleased to inform you that your returned item has been accepted by us.</p>
Here are the details of refund for your Return No. [RETURN_ID], for your reference -
<br/> <br/>
[RETURN_DETAILS]
<div style="height:1px;"><hr style="border:1px dotted #AAAAAA;"></div>
*The cashback benefit you received on this purchase would be reversed on your cashback account. In case the balance on your cashback account is insufficient for a complete reversal of the amount credited for the purchase of this product, the remaining amount would be deducted from the amount refunded on this return.
<br/><br/>
<p>The above refund amount has been credited to your <a href="[MYNT_CREDITS_URL]">My Myntra Cashback account</a>, and is valid on purchase of any product from Myntra.</p>
<p>This mail is for your records only and you are requested not to send a response to this auto-generated mail. You can track the status of your return requests under the <a href="[MY_RETURNS_URL]">My Returns</a> section in My Myntra. In case of any further queries, please contact our Customer Care at [MYNTRA_CC_PHONE].</p>
Regards,<br/>
Myntra Customer Care');

insert into mk_email_notification(name, subject, body) values('return_process_failed', 'Returns ID: [RETURN_ID] - Rejected by QA',
'Hi [USER], <br/><br/>
We are sorry to inform you that your returned item has not been accepted by our quality team.<br/>
Here are the details of your Return No. [RETURN_ID], for your reference - <br/><br/>
[RETURN_DETAILS]
<br/><br/>
<p>We would be arranging for reshipment of your returned item within the next 2 working days.</p> 
<p>This mail is for your records only and you are requested not to send a response to this auto-generated mail. You can track the status of your return requests under the <a href="[MY_RETURNS_URL]">My Returns</a> section in My Myntra. In case of any further queries, please contact our Customer Care at [MYNTRA_CC_PHONE].</p>
Regards,<br/>
Myntra Customer Care');


insert into mk_email_notification(name, subject, body) values('return_reshipped', 'Returns ID: [RETURN_ID] - Rejected returns have been reshipped back to you',
'Hi [USER], <br/><br/>
We are sorry to inform you that your returned item has not been accepted by our quality team. After confirming the same with you, we have now reshipped your returned product back to you through [RESHIP_COURIER_INFO] vide tracking no. [RESHIP_TRACKING]. <br/><br/>
Here are the details of your Return No. [RETURN_ID], for your reference - <br/><br/>
[RETURN_DETAILS]
<br/><br/>
<p>You should be receiving the reshipment of your returned item within the next 7-10 working days. In case you do not, please contact our Customer Care.</p> 
<p>This mail is for your records only and you are requested not to send a response to this auto-generated mail. You can track the status of your return requests under the <a href="[MY_RETURNS_URL]">My Returns</a> section in My Myntra. In case of any further queries, please contact our Customer Care at [MYNTRA_CC_PHONE].</p>
Regards,<br/>
Myntra Customer Care');


