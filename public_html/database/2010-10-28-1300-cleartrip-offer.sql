INSERT INTO mk_email_notification VALUES(NULL,
'cleartrip_offer',
'Your Cleartrip voucher from Myntra.com',
'Dear Customer,
<br/>
<br/>
Thanks for shopping on Myntra.com
<br/>
<br/>
Your transaction value being Rs.500 and/or above, we\'re pleased to offer you a
discount coupon of Rs.500 that can be redeemed on any round trip flight bookings on
Cleartrip.com!
<br/><br/>
<b>How to use</b>
<br/><br/>
1. Select a round trip flight to a destination and travel date of your choice
<br/><br/>
2. Enter the coupon code CTMY500 before payment
<br/><br/>
3. In case you have any issues while redeeming the coupon, please call
Cleartrip customer care at +91 22 41300300 (7AM to 10 PM IST) or 1 800 270
1000 (toll free from non BSNL/MTNL networks within India)
<br/><br/>
<b>Terms and conditions</b>
<br/><br/>
1. Valid for Bookings on Cleartrip.com from 28 Oct 2010 to 31 Dec 2010
<br/><br/>
2. Dates of travel are open
<br/><br/>
Once again, thanks and have a great festive season!
<br/><br/>
Regards,
<br/><br/>
Team Myntra
<br/><br/>
<b>*Please remember</b>
<br/><br/>
� You can avail the Offer on all modes of payment available on www.cleartrip.com
<br/><br/>
� To avail of Cash back you need to enter coupon code CTMY500 before payment during the
booking process. If correct coupon code is not entered then you will not receive any discount.
<br/><br/>
� If you wish to book your travel on Phone, call 022 41 300 300 between 7.30am to 9.30pm &
announce coupon code CTMY500 to Cleartrip customer service representative. Avail your cashback
& proceed to pay the net amount on the IVR.
<br/><br/>
� You can avail the offer only once for Flights. If you transact more than once you can still book
but will not get any Cash back on your 2nd Flight transaction.
<br/><br/>
� The Cash back offer is valid per booking. In case you book more than 1 adult/ segment/ ticket in
a single booking, you will still receive Cash back on per booking basis only.
<br/><br/>
� In case you cancel the booking (in part or full) the entire Cash back availed for the said booking
will be deducted from the refund amount.
<br/><br/>
� This offer cannot be clubbed with any other Cash back offer on www.cleartrip.com .
<br/><br/>
� Cleartrip reserves the right to change or withdraw the offer, anytime at the sole discretion of the
Management.
<br/><br/>
� The terms and conditions of this offer are subject to change without any prior notice.
<br/><br/>
� Disputes are subject to Mumbai jurisdiction.
<br/><br/>', NULL, NULL);