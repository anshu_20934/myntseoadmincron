insert into mk_order_action_reasons (action, reason, reason_display_name, email_contents) values ('order_cancel', 'OOSC', 'Out Of Stock Cancellation', 'We regret to inform you that we are unable to service your order and would have to cancel it due to an overwhelming demand, which has exhausted our stocks.');

insert into mk_order_action_reasons (action, reason, reason_display_name, email_contents) values ('order_cancel', 'OPIS', 'Online Payment Issue', 'We regret to inform you that due to an issue with realization of your online payment, we would not be able to process your order further and are therefore cancelling it. We would request you to try placing an order on Myntra using an alternate form of payment from the one you had used for this order.');

insert into mk_order_action_reasons (action, reason, reason_display_name, email_contents) values ('order_cancel', 'NCNV', 'COD NC NV', 'We tried reaching you a number of times for confirming your COD order, but were unable to make contact. As per our process, we would not be able to process your order further and are therefore cancelling it. Please accept our apologies for any inconvenience caused. If you wish to continue with this order, kindly place the order again and do provide us your contact details where we may contact you in case of any requirement.');

insert into mk_order_action_reasons (action, reason, reason_display_name, email_contents) values ('order_cancel', 'CODNR', 'COD VErified NR', 'As per your request, we are going ahead with the cancellation of your order.');

insert into mk_order_action_reasons (action, reason, reason_display_name, email_contents) values ('order_cancel', 'CCR', 'Customer Request', 'Thank you for contacting Myntra Support. As per your request, we are cancelling your Order No. [ORDER_ID].');

