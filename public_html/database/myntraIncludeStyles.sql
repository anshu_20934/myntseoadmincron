CREATE TABLE `mk_coupon_included_style_ids` (
  `id` int(31) NOT NULL AUTO_INCREMENT,
  `coupon_group` varchar(50) NOT NULL,
  `style_id` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT '2013-07-22 01:22:56',
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `coupon_group` (`coupon_group`,`style_id`),
  KEY `indx_coupongroup` (`coupon_group`),
  KEY `indx_styleid` (`style_id`)
) 
