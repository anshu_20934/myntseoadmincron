-- MySQL dump 10.13  Distrib 5.1.54, for redhat-linux-gnu (x86_64)
--
-- Host: 10.130.227.27    Database: myntra
-- ------------------------------------------------------
-- Server version	5.5.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mk_widget_top_nav_v3`
--

DROP TABLE IF EXISTS `mk_widget_top_nav_v3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mk_widget_top_nav_v3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `link_name` varchar(50) NOT NULL,
  `link_url` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=416 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mk_widget_top_nav_v3`
--

LOCK TABLES `mk_widget_top_nav_v3` WRITE;
/*!40000 ALTER TABLE `mk_widget_top_nav_v3` DISABLE KEYS */;
INSERT INTO `mk_widget_top_nav_v3` VALUES (5,-1,1,'Men','/men'),(6,5,2,'Clothing','/men-clothing'),(7,6,1,'Casual Shirts','/men-casual-shirts'),(9,6,2,'Ethnic Clothing','/men-ethnic-wear'),(10,6,3,'Formal Shirts','/men-formal-shirts'),(11,6,4,'Innerwear & Sleepwear','/men-underwear'),(12,6,5,'Jackets & Blazers','/men-jacket-blazers'),(13,6,6,'Jeans','/men-jeans'),(14,6,8,'Sweaters & Sweatshirts','/men-sweatshirts'),(15,6,10,'Tracksuits','/men-tracksuits'),(16,6,9,'Tees & Polos','/men-tshirts'),(17,5,1,'Footwear','/men-footwear'),(18,17,1,'Casual Shoes','/men-footwear-casual-shoes'),(19,17,2,'Formal Shoes','/men-footwear-formal-shoes'),(20,17,3,'Sports Shoes','/men-footwear-sports-shoes'),(22,17,4,'Sandals','/men-footwear-sandals'),(23,17,5,'Flip Flops','/men-footwear-flip-flops'),(24,5,3,'Accessories','/men-accessories'),(25,24,1,'Backpacks and Bags','/men-bags-backpacks'),(26,24,2,'Belts & Suspenders','/men-belts '),(27,24,3,'Caps & Hats','/men-caps'),(28,24,4,'Fragrances','/men-perfumes'),(30,24,5,'Gift Sets','/men-set'),(31,24,6,'Scarves & Mufflers','/men-scarves'),(32,24,7,'Socks','/men-socks'),(34,-1,2,'Women','/women'),(35,34,2,'Clothing','/womens-clothing'),(36,34,1,'Footwear','/women-footwear'),(37,34,3,'Accessories','/women-accessories'),(38,35,1,'Dresses','/women-dresses'),(40,35,2,'Ethnic Clothing','/women-ethnic-wear'),(46,35,3,'Innerwears & Sleepwear','/women-lingerie'),(47,35,4,'Jackets','/women-jackets'),(48,35,5,'Jeans','/women-Jeans'),(49,35,6,'Jeggings & Leggings','/women-leggings'),(50,35,7,'Shorts','/women-shorts'),(51,35,8,'Skirts','/women-skirts'),(60,37,1,'Bags & Backpacks','/women-bags'),(61,37,2,'Belts','/women-belts'),(62,37,3,'Caps & Hats','/women-caps'),(63,37,4,'Fragrances','/women-perfumes'),(64,37,5,'Gift Sets','/women-set'),(67,37,6,'Scarves, Stoles & Mufflers','/women-scarves'),(69,37,7,'Socks','/women-socks'),(71,37,8,'Sunglasses','/women-sunglasses'),(146,36,3,'Sandals','/women-footwear-sandals'),(147,36,1,'Casual Shoes','/women-footwear-casual-shoes'),(150,36,2,'Sports Shoes','/women-footwear-sports-shoes'),(153,36,4,'Flip Flops','/women-footwear-flip-flops'),(172,-1,3,'Kids','/kids'),(173,172,1,'Footwear','/kids-footwear'),(174,173,1,'Shoes','/kids-shoes'),(176,173,2,'Sandals','/kids-sandals'),(177,173,3,'Flip-flops','/kids-flip-flops'),(179,172,2,'For Boys','/boy'),(180,179,1,'Accessories','/boy-accessories'),(181,179,2,'Bottoms','/boy-bottoms'),(183,179,3,'Sweaters & Jackets','/boy-sweaters'),(190,179,4,'Tees & Tops','/boy-tshirts'),(3,-1,6,'Style Blog','http://www.stylemynt.com/'),(2,-1,4,'Brands','/brands'),(395,6,7,'Shorts & 3/4 Pants','/men-shorts'),(1,-1,5,'Sale','/sales'),(400,6,11,'Trousers','/men-trousers'),(401,24,8,'Sunglasses','/men-sunglasses'),(402,24,9,'Ties & Cufflinks','/men-ties'),(403,24,10,'Wallets','/men-wallets'),(404,24,11,'Watches','/men-watches'),(405,35,9,'Sweaters & Sweatshirts','/women-sweatshirts'),(406,35,10,'Tops','/women-tops'),(407,35,11,'Tracksuits','/women-tracksuits'),(409,37,9,'Wallets & Purses','/wome-wallets'),(410,37,10,'Watches','/women-watches'),(411,172,3,'For Girls','/girls'),(412,411,1,'Accessories','/girl-accessories'),(413,411,2,'Bottoms','/girl-bottoms'),(414,411,3,'Sweaters & Jackets','/girl-sweaters'),(415,411,4,'Tees & Tops','/girl-tshirts');
/*!40000 ALTER TABLE `mk_widget_top_nav_v3` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-04-11 13:55:51
