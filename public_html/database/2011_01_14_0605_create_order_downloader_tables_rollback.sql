TRUNCATE TABLE `mk_order_downloader_runs`;
DROP TABLE `mk_order_downloader_runs`;

TRUNCATE TABLE `mk_order_downloader_schedules`;
DROP TABLE `mk_order_downloader_schedules`;

TRUNCATE TABLE `mk_editors`;
DROP TABLE `mk_editors`;

TRUNCATE TABLE `download_images_status`;
DROP TABLE `download_images_status`;

TRUNCATE TABLE `mk_assignee_editors`;
DROP TABLE `mk_assignee_editors`;
