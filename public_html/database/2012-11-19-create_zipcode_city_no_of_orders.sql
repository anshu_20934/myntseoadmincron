drop table if exists zip_city_orders;
create table zip_city_orders(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,location varchar(64) NOT NULL, location_type varchar(6) NOT NULL, no_of_orders int(20) NOT NULL DEFAULT 0);
