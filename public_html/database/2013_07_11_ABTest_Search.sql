insert into mk_abtesting_tests (name, ga_slot, seg_algo,source_type) values ('styleRevenueAdjustedNew', '0', 'abtest\\algos\\impl\\RandomSegmentationAlgo', 'tpl');
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('test1', (select id from mk_abtesting_tests where name ='styleRevenueAdjustedNew'), 0);
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('test2', (select id from mk_abtesting_tests where name ='styleRevenueAdjustedNew'), 0);
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('test3', (select id from mk_abtesting_tests where name ='styleRevenueAdjustedNew'), 0);
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('test4', (select id from mk_abtesting_tests where name ='styleRevenueAdjustedNew'), 0);
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('test5', (select id from mk_abtesting_tests where name ='styleRevenueAdjustedNew'), 100);
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('control', (select id from mk_abtesting_tests where name ='styleRevenueAdjustedNew'), 0);

