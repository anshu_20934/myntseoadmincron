use myntra;
insert into mk_email_notification (name,subject,body) values('gateway_confirm_order','Myntra Order Confirmation [ORDERID]','Dear [USER],
<br/>
<p>Thank you for placing an order with Myntra.com. Your order details are following:<br>

	<table style="width:95%">
		<tr style="background-color:#d9d9d9;">
			<th width="55%">Product - size(addon)</th>
			<th width="5%">Quantity</th>
			<th width="20%">Unit Price</th>
			<th width="20%">Total</th>
		</tr>
		[PRODUCTDETAIL]
		<tr><td colspan="4"><hr/></td></tr>
		<tr>
			<td colspan="3"	 align="right">Subtotal</td>
			<td align="right">[SUBTOTAL]</td>
		</tr>
		<tr>
			<td colspan="3"	 align="right">Discount</td>
			<td align="right">[DISCOUNT]</td>
		</tr>
		<tr>
			<td colspan="3"	 align="right">Tax</td>
			<td align="right">[TAX]</td>
		</tr>
		<tr>
			<td colspan="3"	 align="right">Shipping</td>
			<td align="right">[SHIPPINGCHARGE]</td>
		</tr>
		<tr>
			<td colspan="3"	 align="right">Gift Wrapping Charges</td>
			<td align="right">[GIFTWRAPPERCHARGE]</td>
		</tr>
		<tr><td colspan="4"><hr/></td></tr>
		<tr style="background-color:#ffffcc;">
			<td colspan="3"	 align="right"><b>Grand Total</b></td>
			<td align="right"><b>[GRANDTOTAL]</b></td>
		</tr>
		<tr><td colspan="4"><hr/></td></tr>
	</table>
</p>	
<br/>
<p>Your payment for the product(s) has been approved and your order ID is <b>[ORDERID]</b>. You can track your order at myntra.com using your login credentials. You can also download the Invoice from your order history.<br/></p>
<p>Your order is currently being processed. [DELIVERY_DATE] Once your order is shipped, you will receive a mail from us giving the tracking details for the order.</p>
<br/>
<p>Thanks</p>
<p>Myntra Customer Support</p>
<br/><br/><br/>
<p style="color:#0000ff">
	<i>------------------------------------------------------------------------------------------------------------------------------------------<br>
	This mail is intended only for the person or entity to which it is addressed and may contain confidential and/or privileged
	information. Any review, retransmission, dissemination or other use of, or taking of any action in reliance upon,
	this information by persons or entities other than the intended recipient is prohibited.
	If you received this in error, please contact the sender and delete the material from any system.
	</i>
</p>');

insert into mk_email_notification (name,subject,body) values('check_confirm_order','Myntra Order Confirmation [ORDERID]','Dear [USER],
<br/>
<p>Thank you for placing an order with Myntra.com. Your order details are following:<br>

	<table style="width:95%">
		<tr style="background-color:#d9d9d9;">
			<th width="55%">Product - size(addon)</th>
			<th width="5%">Quantity</th>
			<th width="20%">Unit Price</th>
			<th width="20%">Total</th>
		</tr>
		[PRODUCTDETAIL]
		<tr><td colspan="4"><hr/></td></tr>
		<tr>
			<td colspan="3"	 align="right">Subtotal</td>
			<td align="right">[SUBTOTAL]</td>
		</tr>
		<tr>
			<td colspan="3"	 align="right">Discount</td>
			<td align="right">[DISCOUNT]</td>
		</tr>
		<tr>
			<td colspan="3"	 align="right">Tax</td>
			<td align="right">[TAX]</td>
		</tr>
		<tr>
			<td colspan="3"	 align="right">Shipping</td>
			<td align="right">[SHIPPINGCHARGE]</td>
		</tr>
		<tr>
			<td colspan="3"	 align="right">Gift Wrapping Charges</td>
			<td align="right">[GIFTWRAPPERCHARGE]</td>
		</tr>
		<tr><td colspan="4"><hr/></td></tr>
		<tr style="background-color:#ffffcc;">
			<td colspan="3"	 align="right"><b>Grand Total</b></td>
			<td align="right"><b>[GRANDTOTAL]</b></td>
		</tr>
		<tr><td colspan="4"><hr/></td></tr>
	</table>
<p>	
<br/>
<p>This is to confirm that we have received your order [ORDERID]. You have opted to make the payment by check. Please print the invoice that was generated at the time you placed the order and mail it to us with your check at the following address .<br/>
	<b>Myntra Customer Service</b><br/>
	<b>Myntra Designs Pvt Ltd,<br/>
	#1546/47, 19th Main,<br/>
	Sector 1, HSR Layout,<br/>
	Bangalore - 560102</b>
</p>
<p>You can access your invoice by logging into your Myntra account and checking your order history here. Please do not forget to mention the order id behind the check too. If you have any questions, please send an email to support@myntra.com using your order number as reference.</p>
<p>We will start processing your order as soon as we receive your check and it will ship within 2 business days of receiving your payment. Thanks for shopping at myntra.com.</p>
<br/>
<p>Thanks</p>
<p>Myntra Customer Support</p>
<br/><br/><br/>
<p style="color:#0000ff">
	<i>------------------------------------------------------------------------------------------------------------------------------------------<br>
	This mail is intended only for the person or entity to which it is addressed and may contain confidential and/or privileged
	information. Any review, retransmission, dissemination or other use of, or taking of any action in reliance upon,
	this information by persons or entities other than the intended recipient is prohibited.
	If you received this in error, please contact the sender and delete the material from any system.
	</i>
</p>');

update mk_email_notification set body='Dear [TRANSFER_TO], <BR><BR>Congratulations! <B>[TRANSFER_FROM]</B> has rewarded you <B>[POINTS] points</B> for the <B>[REWARD_NAME] award</B> with the following message: <BR><BR><B>[MEMO]</B><BR><BR>The points have been credited into your Personal Reward Account and can be used to redeem any gift available at the redemption portal.<BR><BR><BR>Sincerely<BR><BR>HR Department<BR><BR><BR><BR><a href="http://www.myntra.com/modules/organization/viewecard.php?toname=[TRANSFER_TO]&fromname=[TRANSFER_FROM]&points=[POINTS]&type=[REWARD_NAME]&memo=[URL_ENCODED_MEMO]">Click here to view a larger image of your ecard</a><BR><BR><BR><BR><img src="http://www.myntra.com/modules/organization/viewecard.php?toname=[TRANSFER_TO]&fromname=[TRANSFER_FROM]&points=[POINTS]&type=[REWARD_NAME]&mode=email&memo=[URL_ENCODED_MEMO]" alt="View Your E-card" /> <BR><BR>' where name='RRemployeePointTransfer';