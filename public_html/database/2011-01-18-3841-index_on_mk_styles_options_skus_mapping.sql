/*Adding indexes to make picklist query fast*/
create index style_mapping_index on mk_styles_options_skus_mapping(style_id);

create index option_mapping_index on mk_styles_options_skus_mapping(option_id);