use myntra;

CREATE TABLE `mk_issuer_promoportal` (
`orgid` smallint unsigned NOT NULL,                  
`issuercode` smallint(4) zerofill unsigned NOT NULL,                  
`issuername` varchar(50) NULL,                              
PRIMARY KEY  (`issuercode`,`orgid`)                                              
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

insert into mk_issuer_promoportal (orgid,issuercode,issuername) values('24','5013','icici');
insert into mk_issuer_promoportal (orgid,issuercode,issuername) values('26','5014','icici-imint');
drop table mk_issuer_promoportal;

CREATE TABLE `mk_issuer_category_product_map` (              
`id` int(11) NOT NULL auto_increment,
`issuercode` smallint(4) unsigned default NULL,
`categoryid` tinyint(2) zerofill unsigned default NULL,                       
`productid` int(11) default NULL,                          
`active` enum('Y','N') default 'N',                      
PRIMARY KEY  (`id`),
INDEX(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

alter table mk_issuer_category_style_map change categoryid categoryid tinyint(2) zerofill unsigned default null;
alter table mk_voucher_issuer_category add(browse enum('Y','N') default 'N',customize enum('Y','N') default 'N');
insert into mk_voucher_issuer_category (issuercode,categoryid,active) values('5014','01','Y');
update mk_voucher_issuer_category set browse='Y', customize='Y' where issuercode=5014 and categoryid=01;
update mk_voucher_issuer_category set customize='Y' where issuercode=5013;

insert into mk_email_notification (name,subject,body) values('gift_order_imint_m2i','Your order on Myntra for merchandise','<p>[DATE]</p>Dear Customer,
<br/>
<p>We have placed your order with Order ID [ORDID] and will ship the ordered merchandise to the address you have given us.
The merchandise will be shipped by [SHIPDATE] and you can expect to receive it at your doorstep by [RECEIVEDATE].
You will be sent an e-mail with the shipment tracking number as soon as we have shipped the product.</P><p>Customer email Id : <b>[EMAIL]</b></p>
<P>If you have any questions or need any clarification, please feel free to contact us at any of our Customer Care phone numbers below<br/>
[PHONE]<br/>or to write to us at support@myntra.com.</p><br/><p>Sincerely,<br/>Myntra Customer Support</p>');
insert into xcart_discount_coupons values ("5014011234567890","0","100.00","percent","0","0","0.00","200","N","1","1294120318","A","myntraProvider","N","N","N","Y","0","0.00","202","0","0");

insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5014','01','1256208136','Y');
insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5014','01','1253460206','Y');
insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5014','01','1259818273','Y');
insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5014','01','1259545339','Y');
insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5014','01','1256368400','Y');
insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5014','01','1256657459','Y');
insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5014','01','1268183029','Y');
insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5014','01','1267531261','Y');

insert into mk_issuer_category_style_map (issuercode,categoryid,styleid,active) values ('5014','01','981','Y');
insert into mk_issuer_category_style_map (issuercode,categoryid,styleid,active) values ('5014','01','985','Y');
insert into mk_issuer_category_style_map (issuercode,categoryid,styleid,active) values ('5014','01','986','Y');
