insert into mk_org_accounts (login,subdomain,created_date,activate_date,status,org_header_path,accountname,relationshipmgr,domain,org_ini_file,auth_level,strike_off_enable,landingpage,coupon_on_registration)
values('arun.kumar@myntra.com','keane',unix_timestamp(now()),unix_timestamp(now()),1,'../private_interface/keane/capgemini.jpg','keane','arun','.com','../modules/organization/ini/ini_keane.ini',2,'Y','orgIndex.php','N');

insert into mk_org_email (orgid,email_template) values ((select id from mk_org_accounts where subdomain='keane'),'orgkeaneregistration');

insert into mk_email_notification (name,subject,body) values('orgkeaneregistration','Your Keane merchandize store account is created ','Dear [USER],<br><p>Your Keane account has been created. You can login to your account using following details: </p><br><p>Brand Shop URL : http://keane.myntra.com </p><p>login: [LOGIN]</p><p>Password: [PASSWORD]</p><p>For any help or questions regarding your account or products, you can get in touch with us at support@myntra.com</p><p>Yours Sincerely<br/>Myntra Customer Service<br/>www.myntra.com<br/></p>');


insert into mk_org_accounts (login,subdomain,created_date,activate_date,status,org_header_path,accountname,relationshipmgr,domain,org_ini_file,auth_level,strike_off_enable,landingpage,coupon_on_registration)
values('arun.kumar@myntra.com','oracle',unix_timestamp(now()),unix_timestamp(now()),1,'../private_interface/oracle/capgemini.jpg','oracle','arun','.com','../modules/organization/ini/ini_oracle.ini',2,'Y','orgIndex.php','N');

insert into mk_org_email (orgid,email_template) values ((select id from mk_org_accounts where subdomain='oracle'),'orgoracleregistration');

insert into mk_email_notification (name,subject,body) values('orgoracleregistration','Your Oracle merchandize store account is created ','Dear [USER],<br><p>Your Oracle account has been created. You can login to your account using following details: </p><br><p>Brand Shop URL : http://oracle.myntra.com </p><p>login: [LOGIN]</p><p>Password: [PASSWORD]</p><p>For any help or questions regarding your account or products, you can get in touch with us at support@myntra.com</p><p>Yours Sincerely<br/>Myntra Customer Service<br/>www.myntra.com<br/></p>');


insert into mk_org_accounts (login,subdomain,created_date,activate_date,status,org_header_path,accountname,relationshipmgr,domain,org_ini_file,auth_level,strike_off_enable,landingpage,coupon_on_registration)
values('arun.kumar@myntra.com','sasken',unix_timestamp(now()),unix_timestamp(now()),1,'../private_interface/sasken/capgemini.jpg','sasken','arun','.com','../modules/organization/ini/ini_sasken.ini',2,'Y','orgIndex.php','N');

insert into mk_org_email (orgid,email_template) values ((select id from mk_org_accounts where subdomain='sasken'),'orgsaskenregistration');

insert into mk_email_notification (name,subject,body) values('orgsaskenregistration','Your Sasken merchandize store account is created ','Dear [USER],<br><p>Your Sasken account has been created. You can login to your account using following details: </p><br><p>Brand Shop URL : http://sasken.myntra.com </p><p>login: [LOGIN]</p><p>Password: [PASSWORD]</p><p>For any help or questions regarding your account or products, you can get in touch with us at support@myntra.com</p><p>Yours Sincerely<br/>Myntra Customer Service<br/>www.myntra.com<br/></p>');

