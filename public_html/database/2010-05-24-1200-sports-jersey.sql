/*1*/
use myntra;

/*2.creating jersey group-need to put defaultstyleid*/
insert into mk_product_group (id,name,label,title,description,default_style_id,`type`,url,image,display_order)
values('14','Jerseys','Jerseys','Myntra Jerseys','Jerseys description','1114','2','bags','skin1/mkimages/product_groups/bags.jpg','14');

/*3*/
update mk_product_group set is_active='N' where id='14';


/*4*/
update mk_product_type set description='The cricket fever is truly on! Myntra offers personalized official Indian Team Jersey and IPL Jerseys for all enthusiastic cricket fans. In collaboration with the biggest sports brands, we bring to you a gorgeous collection of official Indian Team Jersey and IPL Jerseys. So, get your favorite cricket jersey and personalize it with your name and number. Get started now, ship internationally!', type_h1='Indian Team Jersey, IPL Jerseys', type_title='Indian Team Jersey | Official IPL Cricket Jerseys', type_keywords='Indian Team Jersey, IPL jersey, IPL jerseys, India jersey, India odi jersey, Chennai Super Kings Jersey, Bangalore Royal Challengers jersey, Mumbai Indians Jersey, Delhi Daredevils Jersey, Deccan Chargers Jersey, Rajasthan Royals jersey, Kings XI Punjab jersey, Kolkata Knight Riders Jersey, cricket jersey', type_metadesc='Buy Indian Team Jersey and IPL Jerseys personalized with your own name and number. Official jerseys ship internationally within 3 days.' where id='219';
update mk_product_type set description='Celebrate the biggest sporting spectacle with official FIFA soccer jerseys and English Premier League football jerseys. Myntra in partnership with Adidas brings to you 2010 FIFA World Cup jerseys. A great collection of authentic and personalized football jerseys for soccer fanatics. Pick your favorite soccer jersey and personalize it with your name and number. Support your team now!', type_h1='Football Jerseys, 2010 FIFA World Cup Soccer Jerseys', type_title='Football Jerseys | Buy 2010 FIFA World Cup Soccer Jerseys online in India', type_keywords='football jerseys,soccer jerseys,world cup jerseys,football team jersey,custom football jersey,football t-shirts,adidas soccer jersey,soccer t-shirts,fifa jerseys,adidas football jersey,fifa 2010 jerseys', type_metadesc='Football Jerseys: Buy Official 2010 FIFA World Cup Soccer Jerseys. Custom print jerseys with your name and number. International shipping available.' where id='227';




/* 1)update option price for style online*/
update mk_product_options set price = 2999 where style='1130';

/* 2)insert addon price for style and its options online*/
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1130','2866','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1130','2867','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1130','2868','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1130','2869','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1130','2870','customized',499,1);


/* 1)update option price for style online*/
update mk_product_options set price = 2999 where style='1131';

/* 2)insert addon price for style and its options online*/
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1131','2871','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1131','2872','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1131','2873','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1131','2874','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1131','2875','customized',499,1);


/* 1)update option price for style online*/
update mk_product_options set price = 2999 where style='1132';

/* 2)insert addon price for style and its options online*/
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1132','2876','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1132','2877','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1132','2878','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1132','2879','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1132','2880','customized',499,1);


/* 1)update option price for style online*/
update mk_product_options set price = 2999 where style='1133';

/* 2)insert addon price for style and its options online*/
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1133','2881','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1133','2882','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1133','2883','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1133','2884','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1133','2885','customized',499,1);


/* 1)update option price for style online*/
update mk_product_options set price = 2999 where style='1134';

/* 2)insert addon price for style and its options online*/
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1134','2886','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1134','2887','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1134','2888','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1134','2889','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1134','2890','customized',499,1);

###1135==================;
/* 1)update option price for style online*/
update mk_product_options set price = 1699 where style='1135';

/* 2)insert addon price for style and its options online*/
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1135','2893','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1135','2891','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1135','2892','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1135','2894','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1135','2895','customized',499,1);


