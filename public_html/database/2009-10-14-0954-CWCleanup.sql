use myntra;

delete from mk_svaccount_transactions where transaction_reference_type like 'RR%';
delete from mk_svaccounts where account_type in ('ICR','PEER','MANAGER') and account_id in (select svaccount_id from mk_customer_svaccounts);
delete from mk_customer_svaccounts;
delete from mk_external_employee where employer_org_id=25;
delete from xcart_customers where orgid=25 and login like '%@cwgoindia.com';
delete from mk_org_user_map where orgid=25 and login like '%@cwgoindia.com';
update mk_svaccounts set balance=100000 where account_type='MASTER';

