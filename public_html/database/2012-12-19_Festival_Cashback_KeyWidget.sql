insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) values ('cash_back_info','<div class="hd">
			<div class="title"><span class="info-icon"></span>Cash On Delivery</div>
		</div>
		<div class="bd body-text">
			<p>Myntra offers an option to pay Cash on Delivery (CoD) for your orders.</p>
			<p>The Terms &amp; Conditions for CoD are as follows:</p>
			<ul>
				<li>CoD is available for orders above Rs. 500 and below Rs. 10,000,000</li>
				<li>Your delivery address should be under
					<span class="delivery-network">					serviceable network
					</span>					of our courier partners. Please enter the correct PIN Code to ensure delivery.</li>
				<li>Please pay in cash only and do NOT offer cheque or DD to the courier staff.</li>
				<li>Please do NOT pay for any additional charges, i.e. octroi etc. to courier staff. Your invoice amount is inclusive of all charges.</li>
			</ul>
		</div>
		<div class="ft">
			<p>
									<span class="delivery-network">Check CoD Availability</span> for your PIN Code. 
								Read our <a href="http://dev.myntra.com/faqs#payments" target="_blank">Payments FAQs</a> to know more.</p>
		 ','Cash Back Info');
