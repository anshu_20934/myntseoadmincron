-- PORTAL-667: payment options order made configurable as feature-gate
insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values
('PaymentOptionsOrder','credit_card, debit_card, net_banking, cod','The comma-separated order of payment options to appear in payments page: the payment options are: credit_card, debit_card, net_banking, cod');