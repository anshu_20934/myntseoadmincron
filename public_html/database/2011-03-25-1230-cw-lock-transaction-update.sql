insert into mk_rules (name,description,path) values ('RRPeerLockedTransferMaxYearlyAmount','Haximum amount you can transfer to peer per year in a locked transaction','ws.rewardsService.RRRules');
insert into mk_rules (name,description,path) values ('RRPeerLockedTransferMaxCount',"Maximum number of times a transfer can occur between two accounts in a locked transaction",'ws.rewardsService.RRRules'); 

insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRLockTransferEmployeePoints'),(select id from mk_rules where name='RRPeerLockedTransferMaxYearlyAmount'));
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRLockTransferEmployeePoints'),(select id from mk_rules where name='RRPeerLockedTransferMaxCount'));

delete from mk_activity_rule_mapping where activity_id=(select id from mk_activities where name='RRClearLockedTransaction') and rule_id=(select id from mk_rules where name='RRPeerTransferMaxYearlyAmount');
delete from mk_activity_rule_mapping where activity_id=(select id from mk_activities where name='RRClearLockedTransaction') and rule_id=(select id from mk_rules where name='RRPeerTransferMaxCount');