
CREATE TABLE mk_zipcode_courier_support_mapping (
courier			varchar(10) NOT NULL,
zipcode			varchar(64) NOT NULL,
pickup_supported	int(1) default 0,
cod_supported		int(1) default 0,
enabled			int(1) default 1,

PRIMARY KEY(courier, zipcode)
);

