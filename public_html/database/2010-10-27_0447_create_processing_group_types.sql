CREATE TABLE `mk_processing_group_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `processing_group_id` int(11) NOT NULL,
  `product_type_id` int(11) NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `processing_group_id` (`processing_group_id`),
  KEY `product_type_id` (`product_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
