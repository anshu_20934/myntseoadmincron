ALTER TABLE mk_style_visibility_info
ADD (   style_real_revenue INT(11) DEFAULT 0,
        style_store1 INT(11) DEFAULT 0,
        style_store2 INT(11) DEFAULT 0,
        style_store3 INT(11) DEFAULT 0,
        style_store4 INT(11) DEFAULT 0,
        style_store5 INT(11) DEFAULT 0
);

ALTER TABLE mk_style_visibility_info_backup
ADD (   style_real_revenue INT(11) DEFAULT 0,
        style_store1 INT(11) DEFAULT 0,
        style_store2 INT(11) DEFAULT 0,
        style_store3 INT(11) DEFAULT 0,
        style_store4 INT(11) DEFAULT 0,
        style_store5 INT(11) DEFAULT 0
);

