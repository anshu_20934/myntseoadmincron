CREATE TABLE `mk_registration_class` (
  `channel` varchar(20) NOT NULL DEFAULT '',
  `registration_class` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`channel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `mk_registration_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` varchar(20) DEFAULT NULL,
  `execution_order` tinyint(2) DEFAULT NULL,
  `action` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

CREATE TABLE `mk_registration_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` varchar(20) NOT NULL DEFAULT 'ON',
  `start_date` int(11) NOT NULL DEFAULT 0,
  `end_date` int(11) NOT NULL DEFAULT 0,
  `registration_email_template` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;