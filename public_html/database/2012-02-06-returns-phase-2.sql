alter table xcart_returns add column (delivery_credit decimal(10,2));

alter table xcart_returns change itembarcode itembarcode varchar(255);

alter table mk_returns_comments_log add column(image_url varchar(500));

update xcart_returns set delivery_credit = 100 where return_mode = 'self';

insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('return.disable.category','22,136','article ids of styles for which returns is disabled');

insert into mk_roles (name,type,description,allowed_actions) values ('Customer Service TL','CST','Customer Service TL role','ordermanagement,productmanagement,producttypemanagement,reportmanagement,productstylemanagement');

alter table mk_old_returns_tracking change currstate currstate enum('RTOQ','RTOCAL1','RTOCAL2','RTOCAL3','RTOC','RTORS','RTORSC','RTORF','RTOAI','RTQ','RTCSD','RTPI','RTPTU','RTR','RTQP','RTQF','RTCC','RTQPA','RTQPNA','RTRF','RTAI','RTRS','RTRSC','END','CMMNT','RTCL','RTOCL','RTOPREQ') not null;

alter table mk_old_returns_tracking add column (ud_status varchar(50));