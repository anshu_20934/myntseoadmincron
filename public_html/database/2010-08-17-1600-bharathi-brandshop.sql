use myntra;

/*new brandshop*/
insert into mk_org_accounts (login,subdomain,created_date,activate_date,status,org_header_path,accountname,relationshipmgr,domain,org_ini_file,org_css_file,brand_shop_dir,auth_level,strike_off_enable)
values('arun.kumar@myntra.com','bharathi-axa',unix_timestamp(now()),unix_timestamp(now()),1,'../private_interface/bharathi-axa/header.jpg','bharathi-axa','arun','.com','../modules/organization/ini/ini_bharathi.ini','../skin1/modules/organization/css/css_bharathi.css','/bharathi-axa-store-template',1,'N');


/*master account for bharathi-axa*/
insert into mk_org_masteraccount_mapping (orgid,master_account_id) values('31','1397');
insert into mk_svaccounts (account_id,balance,active,account_type,currency_type) values('1397',5000,1,'MASTER','CUR_INR');
insert into mk_customer_svaccounts (customer_login,svaccount_id) values ('rewards@bharti-axagi.co.in',1397);

/*c&w master modification*/
insert into mk_customer_svaccounts (customer_login,svaccount_id) values ('rewards@cwgoindia.com',88);

/*shipping consignment capacity for bharthi for cricket jerseys*/
insert into mk_org_product_type_map (product_type_id,orgid,consignment_capacity) values (219,31,0);


/*svaccount types for bharathi*/
insert into mk_svaccount_types (type_code,description,org_id,is_admin) values ('ICR','Individual Contributor Reward','31',0);
insert into mk_svaccount_types (type_code,description,org_id,is_admin) values ('MANAGER','PRODIGY','31',0);
insert into mk_svaccount_types (type_code,description,org_id,is_admin) values ('MASTER','Administrative Master Account','31',1);
insert into mk_svaccount_types (type_code,description,org_id,is_admin) values ('PEER','HANDSHAKE','31',0);

/*admin employee*/
insert into xcart_customers (login,usertype,password,firstname,lastname,email,status,activity,termcondition,account_type,orgid) values('rewards@bharti-axagi.co.in','C','B-e50c09ffe6cd7c0b24f08288f49c038894df56388df173c7','bhartirewards','admin','rewards@bharti-axagi.co.in','Y','Y',1,'OG',31);
insert into mk_org_user_map (orgid,login,active,usetype,employee_id) values (31,'rewards@bharti-axagi.co.in',1,'OG',99998);
insert into mk_external_employee (employee_id,email,date_of_joining,level,is_admin,dept_name,manager_employee_id,employment_status,date_of_resignation,date_of_exit,employer_org_id) values (99998,'rewards@bharti-axagi.co.in',0,'IC','Y','HR',0,'A',0,0,31);

/*RRNewEmployee new template(generic)*/
update mk_email_notification set body='Dear [EMPLOYEE_NAME], <BR><BR>Your account has been created in the [COMAPNY_NAME] R&R portal. <BR>Please see below for your credentials for portal login. You are requested to change your password after first login.<BR><BR>System URL: [URL] <BR><BR><b>Login: [LOGIN]</b><BR><BR>Password: [PASSWORD]<BR><BR>Points would be allocated to your rewards accounts within an hour of receiving this email. <BR><BR>We look forward to your active participation. Happy Gifting! <BR><BR>Sincerely<BR><BR>

HR Department<BR><BR>' where name='RRNewEmployee';
