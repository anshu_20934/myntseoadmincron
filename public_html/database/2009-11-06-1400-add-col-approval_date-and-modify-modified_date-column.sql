use myntra;

ALTER TABLE xcart_products ADD approval_date INT(20) AFTER add_date;

UPDATE xcart_products SET approval_date = add_date+3600;

UPDATE xcart_products SET modified_date = FROM_UNIXTIME(approval_date);