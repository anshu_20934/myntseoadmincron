/*1*/
use myntra;

/*2*/
alter table mk_product_group add column is_customizable enum('Y','N') default 'N';

/*3*/
update mk_product_group set is_customizable='Y';
update mk_product_group set is_customizable='N' where id='14';

/* 4)update option price for style online*/
update mk_product_options set price = 2995 where style='1136';

/* 5)insert addon price for style and its options online*/
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1136','2898','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1136','2899','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1136','2900','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1136','2901','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1136','2902','customized',499,1);

/* 6)update option price for style online*/
update mk_product_options set price = 1495 where style='1137';

/* 7)insert addon price for style and its options online*/
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1137','2903','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1137','2904','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1137','2905','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1137','2906','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1137','2907','customized',499,1);
