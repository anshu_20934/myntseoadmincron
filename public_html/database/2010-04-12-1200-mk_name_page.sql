use myntra;
CREATE TABLE `mk_name_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `style_id` int(10) DEFAULT NULL,
  `product_type_id` int(10) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `small_font_image` varchar(1000) DEFAULT NULL,
  `large_font_image` varchar(1000) DEFAULT NULL,
  `small_style_image` varchar(1000) DEFAULT NULL,
  `large_style_image` varchar(1000) DEFAULT NULL,
  `small_back_image` varchar(1000) DEFAULT NULL,
  `large_back_image` varchar(1000) DEFAULT NULL,
  `row_sequence` int(10) DEFAULT NULL,
  `width_small_font` int(4) DEFAULT NULL,
  `width_large_font` int(4) DEFAULT NULL,
  `coordinates_of_small_image` varchar(100) DEFAULT NULL,
  `coordinates_of_large_image` varchar(100) DEFAULT NULL,
  `price` int(10) DEFAULT NULL,
  `product_name` varchar(500) DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL,
  `height_small_font` int(10) DEFAULT NULL,
  `height_large_font` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;