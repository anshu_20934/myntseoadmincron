alter table mk_style_properties
add `catalog_date` int(11) DEFAULT NULL;

update mk_style_properties msp, mk_product_style mps
set msp.catalog_date = msp.add_date
where msp.style_id = mps.id and mps.styletype = 'P';

alter table mk_style_properties
add index (`catalog_date`);
