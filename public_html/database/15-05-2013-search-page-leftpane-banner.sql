insert into page_config_image_rule(location_type,section,width,height,min,max,step,size,require_page_location) values ('search','slideshow',188,180,0,1,1,512000,1);
update page_config_image_rule set `min` = 0 where `location_type` = 'landing' and `section` = 'slideshow';
update page_config_image_rule set `height` = -1 where `location_type` = 'landing' and `section` = 'static';
