CREATE TABLE `mk_icici_payments_log` (
  `id` int(31) NOT NULL AUTO_INCREMENT,
  `orderid` int(20) NOT NULL,
  `login` varchar(128) NOT NULL,
  `status` varchar(6),
  `eci`	int(6),
  `cavv` varchar(128),
  `purchaseamount` int(32),
  `currency` int(6),
  `xid` varchar(128),
  `response_id` int(32),
  `response_md` int(32),
  `veres_status` varchar(6),
  `message_hash` text,
  `mpi_error_code` int(11),
  `pares_status` varchar(6),
  `pares_verified` varchar(6),
  `time_insert` int(11),
  `gateway_response_code` int(6),
  `gateway_response_message` text,
  `gateway_transaction_id` varchar(32),
  `epg_trasaction_id` varchar(32),
  `gateway_auth_id_code` varchar(32),
  `gateway_rrn` varchar(32),
  `gateway_cvv_response_code` varchar(6),
  `gateway_fdms_score` varchar(32),
  `gateway_fdms_result` varchar(32),  
  `gateway_cookie` text,  
  `response` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

alter  table mk_icici_payments_log add index `orderid` (orderid);
alter  table mk_icici_payments_log add index `login` (login);