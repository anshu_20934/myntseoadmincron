ALTER TABLE xcart_order_details
    DROP COLUMN coupon_code,
    DROP COLUMN coupon_discount_product;

ALTER TABLE `mk_coupon_group`
	DROP COLUMN `channel`, 
	DROP COLUMN `timeCreated`, 
 	DROP COLUMN `creator`;

-- Coupon logins (usage) table.
ALTER TABLE `xcart_discount_coupons_login`
	DROP COLUMN `times_locked`,
	DROP COLUMN `subtotal`,
	DROP COLUMN `discount`;

-- Coupons table.
ALTER TABLE `xcart_discount_coupons`
	DROP COLUMN `groupName`,
	DROP COLUMN `isInfinite`,
	DROP COLUMN `maxUsagePerCart`,
	DROP COLUMN `subtotal`,
	DROP COLUMN `discountOffered`,
  	DROP COLUMN `maxUsageByUser`,
  	DROP COLUMN `isInfinitePerUser`,
	DROP COLUMN `maxAmount`,
  	DROP COLUMN `users`,
  	DROP COLUMN `excludeUsers`,
  	DROP COLUMN `paymentMethod`,
  	DROP COLUMN `timeCreated`,
	DROP COLUMN `comments`,
  	DROP COLUMN `couponType`,
  	DROP COLUMN `MRPAmount`,
  	DROP COLUMN `MRPpercentage`,
  	DROP COLUMN `freeShipping`,
	DROP COLUMN `excludeProductTypeIds`,
	DROP COLUMN `excludeStyleIds`,
	DROP COLUMN `catStyleIds`,
	DROP COLUMN `excludeCatStyleIds`,
	DROP COLUMN `excludeCategoryIds`,
	DROP COLUMN `excludeSKUs`,
	DROP COLUMN `productTypeIds`,
        DROP COLUMN `styleIds`,
        DROP COLUMN `categoryIds`,
        DROP COLUMN `SKUs`;

DELETE FROM mk_coupon_group WHERE groupname = 'FeedbackCoupons';
DELETE FROM mk_coupon_group WHERE groupname = 'OldCoupons';
