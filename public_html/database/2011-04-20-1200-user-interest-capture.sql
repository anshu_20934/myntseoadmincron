CREATE TABLE mk_style_transient_data
(
	style_id INT(11) NOT NULL,
	date INT(11) DEFAULT 0,
	action VARCHAR(255),
	count INT(11) DEFAULT 0,
	INDEX(date)
);

CREATE TABLE mk_style_aggregated_transients
(
	style_id INT(11) NOT NULL,
	action VARCHAR(255),
	aggregated_count INT(11) DEFAULT 0,
	UNIQUE INDEX(style_id, action)
);

CREATE TABLE mk_style_todays_transients
(
	style_id INT(11) NOT NULL,
	action VARCHAR(255),
	todays_count INT(11) DEFAULT 0,
	UNIQUE INDEX(style_id, action)
);

CREATE TABLE mk_transients_config
(
	action VARCHAR(255) NOT NULL,
	weightage INT(4) DEFAULT 1,
	cutoff_days INT(4) DEFAULT 15
);

insert into mk_transients_config (action, weightage, cutoff_days) values("ProductView", 1, 15);
insert into mk_transients_config (action, weightage, cutoff_days) values("AddToCart", 5, 30);
insert into mk_transients_config (action, weightage, cutoff_days) values("Purchase", 15, 90);
	
