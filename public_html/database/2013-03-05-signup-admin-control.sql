insert into mk_widget_key_value_pairs(`key`,`value`,`description`) values('birth_year_range_low', '1950', 'lower value of birth year range in signup');

insert into mk_widget_key_value_pairs(`key`,`value`,`description`) values('birth_year_range_high', '2013', 'high value of birth year range in signup');

insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('display_name', 'true', 'enable/disable name field in signup');

insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('display_birth_year', 'true', 'enable/disable birth year field in signup');

insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('is_name_optional', 'true', 'optional name field in signup');

insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) value('is_birth_year_optional', 'true', 'optional birth year field in signup');



