CREATE TABLE `mk_sem_promo` (
  `id` int auto_increment NOT NULL,
  `identifier` varchar(150) NOT NULL,
  `promo_content` varchar(250) NOT NULL,
  `promo_url` varchar(250) NOT NULL,
  `promo_created_date` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY(`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
alter table mk_sem_promo add constraint unique(identifier);

