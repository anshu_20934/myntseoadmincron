ALTER TABLE mk_mobile_code
	ADD column num_attempts tinyint(1) DEFAULT 0,
	ADD column cap_attempts tinyint(1) DEFAULT 0;
