DROP TABLE IF EXISTS `mk_coupon_mobile_assoc`;
CREATE TABLE `mk_coupon_mobile_assoc` (
  `id` int(11) NOT NULL auto_increment,
  `mobile_number` varchar(14) DEFAULT NULL,
  `campaign_id` int(5) default 1,
  `login` varchar(255) default NULL,
  `coupon` varchar(11) default NULL,
  PRIMARY KEY  (`id`),
  FOREIGN KEY(`login`) REFERENCES xcart_customers(`login`), 
  FOREIGN KEY(`coupon`) REFERENCES xcart_discount_coupons(`coupon`),
  UNIQUE INDEX(`mobile_number`, `campaign_id`)
) ENGINE=InnoDb DEFAULT CHARSET=latin1;


alter table xcart_customers add column reg_coupon_gen_key varchar(50) default NULL;

insert into mk_email_notification (name, subject, body) values('generete_registration_coupon_sms', '[FIRST_NAME], Welcome to Myntra',
'Dear [FIRST_NAME],<br><br>

Thank you for registering with Myntra.com, India''s largest online store for personalized products and merchandise. <br><br> 

Your user name is [USER_NAME] and as a first time user at Myntra, you are eligible for a <b>FREE personalised mug!</b> <br>
To avail your free mug coupon click on the following link.<br><a href=''http://www.myntra.com/senCoupon.php?email=[EMAIL_ENCRYPTED]'' target=''_blank''>http://www.myntra.com/senCoupon.php?email=[EMAIL_ENCRYPTED]</a><br><br><u>How to redeem</u>:<br><ol><li>Click on the Free Mug link in this mail</li><li>Personalize your white ceramic mug with a photograph, text or slogan and add to cart</li><li>At check out, enter the voucher code and click on ''Redeem''</li></ol><br><br>

 <a href=''http://www.myntra.com/create-my/White%20Ceramic%20Mug/14'' target=''_blank''><b>Click here to claim your free mug.</b></a><br><br><u>T&amp;C:</u><br>
 <ol><li>The voucher is valid only on purchase of White Ceramic Mug</li><li>Offer valid till 15 days from date of registration<br></li></ol><br><br>

Cheers,<br><br>

Team Myntra<br>
<br><br>

');