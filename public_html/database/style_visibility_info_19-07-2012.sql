alter table mk_style_visibility_info
add `style_sold_count` int(11) DEFAULT 0,
add `style_added_to_cart_count` int(11) DEFAULT 0,
add `style_pdp_view_count` int(11) DEFAULT 0,
add `style_revenue` int(11) DEFAULT 0,
add index (`style_id`);

alter table mk_style_visibility_info_backup
add `style_sold_count` int(11) DEFAULT 0,
add `style_added_to_cart_count` int(11) DEFAULT 0,
add `style_pdp_view_count` int(11) DEFAULT 0,
add `style_revenue` int(11) DEFAULT 0,
add index (`style_id`);
