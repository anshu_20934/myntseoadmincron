drop table if exists cash_on_delivery_dups;
drop table if exists cash_on_delivery_info_dump;
create table cash_on_delivery_info_dump as (select login_id,contact_number from cash_on_delivery_info where order_id<=1679255 and order_confirmed='Y');
alter table cash_on_delivery_info_dump add index `contact_number`(contact_number);
alter table cash_on_delivery_info_dump add index `login_id` (login_id);
create table cash_on_delivery_dups as (select c1.login_id as login1, c2.login_id as login2, c1.contact_number from cash_on_delivery_info_dump c1, cash_on_delivery_info_dump c2 where c1.contact_number = c2.contact_number and c1.login_id != c2.login_id);
alter table cash_on_delivery_dups add index `login1` (login1);