insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) VALUES ('automated.captcha.verification.user', 'testing@myntra.com', 'email used in automated testing');
insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) VALUES ('automated.captcha.verification.captchacode', 'testing', 'captcha to verify for QA Automation scripts');
