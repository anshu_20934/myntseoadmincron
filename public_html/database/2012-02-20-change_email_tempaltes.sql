delete from mk_email_notification where name in ('orderprocessedandhanddelivered', 'orderprocessedandhanddelivered_part', 'shipped_order_details_table', 'processing_order_details_table', 'order_shipped_confirmation', 'order_cancel', 'order_cancel_oos', 'item_cancel', 'items_cancel', 'order_shipped_confirmation_part');

insert into mk_email_notification (name, subject, body) values ('orderprocessedandhanddelivered_part', 'Some items in your Order have been Shipped! Order No. [ORDER_ID]', 
'Dear [USER],<br>
<p>Your shipment with Order ID [ORDER_ID] - Shipment ID [SHIPMENT_ID] has been processed and dispatched from our warehouse through <b>Myntra Logistics</b>. You will receive your product by <b>[DELIVERY_DATE]</b>.</p>
<p>Shipment Tracking Number - [TRACKING_NUMBER].</p>
<p>Here are the details of items Shipped via Shipment ID [SHIPMENT_ID], for your reference -</p>[SHIPMENT_DETAILS]<br>
<p>At Myntra, we make every possible effort to ensure that you receive your product on time. Please help us to deliver on time by considering the following:
<ul style="list-style-type:decimal;">
<li>Do make your self available to receive the shipment on specified delivery date.</li>
<li>If case you are unavailable, do make arrangements to have the shipment received by someone on your behalf at your delivery address.</li>
<li>If you have placed a COD order, do keep the due amount ready and at hand to pay our delivery representative when you receive the shipment.</li>
</ul>
We would like to thank you for buying from www.myntra.com. We hope you enjoyed the experience and we look forward to having you back!
</p>
<p>In case of any further queries, please get in touch with us at [MYNTRA_CC_PHONE].</p>
Regards,<br/>
Myntra Customer Connect<br/>
India\'s Largest Online Fashion Store
<br/><br/>[DECLARATION]<br/>
</div>');

insert into mk_email_notification (name, subject, body) values ('orderprocessedandhanddelivered', 'Your Order with Myntra has been Shipped! Order No. [ORDER_ID]',
'Dear [USER],<br>
<p>Your shipment with Order ID [ORDER_ID] - Shipment ID [SHIPMENT_ID] has been processed and dispatched from our warehouse through <b>Myntra Logistics</b>. You will receive your product by <b>[DELIVERY_DATE]</b>.</p>
<p>Shipment Tracking Number - [TRACKING_NUMBER].</p>
<p>Here are the details of your order, for your reference -</p>[SHIPMENT_DETAILS]<br>
<p>At Myntra, we make every possible effort to ensure that you receive your product on time. Please help us to deliver on time by considering the following:
<ul style="list-style-type:decimal;">
<li>Do make your self available to receive the order on specified delivery date.</li>
<li>If case you are unavailable, do make arrangements to have the order received by someone on your behalf at your delivery address.</li>
<li>If you have placed a COD order, do keep the due amount ready and at hand to pay our delivery representative when you receive the order.</li>
</ul>We would like to thank you for buying from www.myntra.com. We hope you enjoyed the experience and we look forward to having you back!
</p>
<p>In case of any further queries, please get in touch with us at [MYNTRA_CC_PHONE].</p>
Regards,<br/>
Myntra Customer Connect<br/>
India\'s Largest Online Fashion Store
<br/><br/>[DECLARATION]<br/>
</div>');


insert into mk_email_notification (name, subject, body) values ('order_shipped_confirmation', 'Your Order with Myntra has been Shipped! Order No. [ORDER_ID]',
'Dear [USER],<br>
<p>Your shipment with Order ID [ORDER_ID] - Shipment ID [SHIPMENT_ID] has been processed and dispatched from our warehouse. You will receive your product by <b>[DELIVERY_DATE]</b>.</p>
<p>Here are your tracking details:<br>
Courier Partner: [COURIER_SERVICE] 
Tracking Code: [TRACKING_NUMBER]
</p>
<p>You can track the shipment progress of your order in the My Orders section of My Myntra, or directly on the shipping vendor\'s website at [COURIER_WEBSITE]. Please note that it may take upto 24 hours before your order tracking number reflects on our partner website and at the MyOrders section of My Myntra.</p>
<p>Here are the details of your order, for your reference -</p>[SHIPMENT_DETAILS]<br>
<p>At Myntra, we make every possible effort to ensure that you receive your product on time. Please help us to deliver on time by considering the following:
<ul style="list-style-type:decimal;">
<li>Do make your self available to receive the order on specified delivery date.</li>
<li>If case you are unavailable, do make arrangements to have the order received by someone on your behalf at your delivery address.</li>
<li>If you have placed a COD order, do keep the due amount ready and at hand to pay our delivery representative when you receive the order.</li>
</ul>We would like to thank you for buying from www.myntra.com. We hope you enjoyed the experience and we look forward to having you back!
</p>
<p>In case of any further queries, please get in touch with us at [MYNTRA_CC_PHONE].</p>
Regards,<br/>
Myntra Customer Connect<br/>
India\'s Largest Online Fashion Store
<br/><br/>[DECLARATION]<br/>
</div>');


insert into mk_email_notification (name, subject, body) values ('order_shipped_confirmation_part', 'Some items in your Order have been Shipped! Order No. [ORDER_ID]',
'Dear [USER],<br>
<p>Your shipment with Order ID [ORDER_ID] - Shipment ID [SHIPMENT_ID] has been processed and dispatched from our warehouse. You will receive your product by <b>[DELIVERY_DATE]</b>.</p>
<p>Here are your tracking details:<br>
Courier Partner: [COURIER_SERVICE] 
Tracking Code: [TRACKING_NUMBER]
</p>
<p>You can track the shipment progress of your order in the My Orders section of My Myntra, or directly on the shipping vendor\'s website at [COURIER_WEBSITE]. Please note that it may take upto 24 hours before your order tracking number reflects on our partner website and at the MyOrders section of My Myntra.</p>
<p>Here are the details of items Shipped via Shipment ID [SHIPMENT_ID], for your reference -</p>[SHIPMENT_DETAILS]<br>
<p>At Myntra, we make every possible effort to ensure that you receive your product on time. Please help us to deliver on time by considering the following:
<ul style="list-style-type:decimal;">
<li>Do make your self available to receive the order on specified delivery date.</li>
<li>If case you are unavailable, do make arrangements to have the order received by someone on your behalf at your delivery address.</li>
<li>If you have placed a COD order, do keep the due amount ready and at hand to pay our delivery representative when you receive the order.</li>
</ul>We would like to thank you for buying from www.myntra.com. We hope you enjoyed the experience and we look forward to having you back!
</p>
<p>In case of any further queries, please get in touch with us at [MYNTRA_CC_PHONE].</p>
Regards,<br/>
Myntra Customer Connect<br/>
India\'s Largest Online Fashion Store
<br/><br/>[DECLARATION]<br/>
</div>');

insert into mk_email_notification (name, subject, body) values ('order_cancel', 'Cancelled Order No. [ORDER_ID]!',
'Hello [USER], <br/><br/>
[REASON_TEXT] <br><br>
Please find the details of your Order No. [ORDER_ID] below:<br/><br/>
[ORDER_DETAILS]
<br/><br/>
<p>[GOODWILL_COUPON_MSG] [REFUND_MSG] [COUPON_USAGE_TEXT] [CAHBACK_DEDUCT_MSG]</p>
<p>
We want you to know we are honoured to have you as our esteemed customer and we will do our best to meet your needs. Your continued patronage and suggestions are a vital part of our growth, and for that we are most grateful to you.
</p>
<p>For any clarification, kindly contact us at [MYNTRA_CC_PHONE].</p><br>
Regards,<br/>
Myntra Customer Connect');

insert into mk_email_notification (name, subject, body) values ('order_cancel_oos', 'Cancelled Order No. [ORDER_ID]!',
'Hello [USER], <br/><br/>
[REASON_TEXT] <br><br>
Please find the details of your Order No. [ORDER_ID] below:<br/><br/>
[ORDER_DETAILS]
<br/><br/>
<p>[GOODWILL_COUPON_MSG] [REFUND_MSG] [COUPON_USAGE_TEXT] [CAHBACK_DEDUCT_MSG]</p>
<p>You, our customer, are our focus and your experience takes precedence over everything else. It is the key principle of Myntra.com and we would like to reassure you that we would continue to remain invested in offering the best shopping experience for Fashion and Lifestyle products. We hope that you continue to shop with us and treat this as an aberration to the quality standards we strive to achieve.
</p>
<p>For any clarification, kindly contact us at [MYNTRA_CC_PHONE].</p><br>
Regards,<br>
Myntra Customer Connect');

insert into mk_email_notification (name, subject, body) values ('items_cancel', 'Cancelled Items in Order No. [ORDER_ID]!',
'Hello [USER], <br/><br/>
[REASON_TEXT]<br/><br/>
Here are the details of cancelled items, for your reference:
<br/><br/>
[ITEM_DETAILS]
<br/>
<p>[GOODWILL_COUPON_MSG] [REFUND_MSG] [COUPON_USAGE_TEXT] [CAHBACK_DEDUCT_MSG]</p>
<p>We want you to know we are honoured to have you as our esteemed customer and we will do our best to meet your needs. Your continued patronage and suggestions are a vital part of our growth, and for that we are most grateful to you.</p>
<p>For any clarification, kindly contact us at [MYNTRA_CC_PHONE].</p>
<br/>
Regards,<br/>
Myntra Customer Connect');
