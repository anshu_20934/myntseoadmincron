
update mk_product_group set description = 'Express yourself with fine custom t-shirts and custom apparel. Myntra brings to you a choice selection of round neck and polo t-shirts. Create your own t-shirt by uploading your photo, design or text.' where id = 1;
update mk_product_group set description = 'Take a sip of your favorite drink in our custom mugs and drinkware. Myntra offers a beautiful collection of mugs, coasters, plates, sippers and wine glasses. You can personalize them with a photo, design or text of your choice.' where id = 3;
update mk_product_group set description = 'Keep a track of time and events in style with our custom calendars and diaries. Myntra adds color to your life with its lively and adorable collection of calendars and diaries. Personalize them by adding your favorite pictures, designs or text.' where id = 4;
update mk_product_group set description = 'Create beautiful memories with custom cards and posters. Myntra has put together an exquisite collection of greeting cards and posters that can be personalized with your photo, design or text. Our posters come with a glossy finish.' where id = 6;
update mk_product_group set description = 'Look at the whole new fun side of gifting with our custom fun stuff. Our fun collection includes custom jigsaws, teddy bear, money banks and chocolate wrappers. Test your creativity and personalize them with your very own designs.' where id = 7;
update mk_product_group set description = 'Enjoy the experience of creating your own desktop products at Myntra. A great collection of custom desktop products that includes mousepads, wooden plaques and wooden photo frames. Upload your photo, design or text to personalize them.' where id = 8;
update mk_product_group set description = 'Up your style quotient with our custom keychains. Myntra offers a wide range of cute and adorable keychains. Personalize your keychain with your favorite design, text or image.' where id = 9;
update mk_product_group set description = 'Look classy and elegant with our custom men\'s accessories. Myntra offers custom metallic address books and metallic cigarette cases with excellent finish. You can personalize them with photos or designs of your choice.' where id = 10;
update mk_product_group set description = 'Here is a collection that will make every woman go gaga. Myntra proudly brings to you an exquisite collection of custom women\'s accessories that includes pendants, jewelery boxes and metallic compact mirrors. You can personalize them with designs or photos of your choice.' where id = 11;
update mk_product_group set description = 'Look at the whole new fun side of gifting with our custom fun stuff. Our fun collection includes custom jigsaws, teddy bear, money banks and chocolate wrappers. Test your creativity and personalize them with your very own designs.' where id = 12;


alter table mk_product_group add column title varchar(255) after label;


update mk_product_group set title = 'Apparels - Personalized T-Shirts, Sweatshirts' where id = 1;
update mk_product_group set title = 'Mugs & Drinkwares - Photo Mugs, Beer Mugs' where id = 3;
update mk_product_group set title = 'Calendars and Diaries' where id = 4;
update mk_product_group set title = 'Cards & Posters' where id = 6;
update mk_product_group set title = 'Fun Stuff, Funny Gifts' where id = 7;
update mk_product_group set title = 'Desktop Products' where id = 8;
update mk_product_group set title = 'Keychains, Keyrings' where id = 9;
update mk_product_group set title = 'Men\'s Accessories' where id = 10;
update mk_product_group set title = 'Women\'s Accessories' where id = 11;
update mk_product_group set title = 'Watches, Photo Watches' where id = 12;
