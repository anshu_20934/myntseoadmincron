insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) 
values('payments.login.always-prompt','false','true enables login expiry for all');

insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) 
values('payments.login.savedcard-prompt','false','true enables login expiry for all user who has atleast one saved card');

insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) 
values('payments.login.cashback-prompt','false','true enables login expiry for all users who has cachback > 0');

insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) 
values('sessionInactiveThreshold','1800','set the time in seconds');

insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) 
values('secure.login','true','enable/diasble secure login. Disabling this will disable the oneclick/saved card features as well ');  