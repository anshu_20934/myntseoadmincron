alter table `mk_product_style` add column `is_customizable` tinyint (1)  DEFAULT '1' NULL;
alter table `xcart_order_details` add column `is_customizable` tinyint (1)  DEFAULT '1' NULL;
alter table `mk_product_style` add column `items_sold` int (11)  DEFAULT '0' NULL ;
update `mk_product_group` set `id`='15' where `id`='0';
alter table `mk_product_group` change `id` `id` int (11)   NOT NULL AUTO_INCREMENT ,drop primary key,  add primary key (`id` );
DROP TABLE IF EXISTS `mk_filter_group`;

CREATE TABLE `mk_filter_group` (
  `id` int(11) NOT NULL auto_increment,
  `group_name` varchar(255) default NULL,
  `group_label` varchar(255) default NULL,
  `description` text,
  `config` text,
  `display_order` int(11) default NULL,
  `is_active` tinyint(1) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `mk_filter_mapping` */

DROP TABLE IF EXISTS `mk_filter_mapping`;

CREATE TABLE `mk_filter_mapping` (
  `id` int(11) NOT NULL auto_increment,
  `filter_id` int(11) default NULL,
  `filter_map_id` int(11) default NULL,
  `is_active` tinyint(1) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `mk_filters` */

DROP TABLE IF EXISTS `mk_filters`;

CREATE TABLE `mk_filters` (
  `id` int(11) NOT NULL auto_increment,
  `filter_name` varchar(255) default NULL,
  `filter_url` varchar(255) default NULL,
  `filter_group_id` int(11) NOT NULL,
  `logo` varchar(255) default NULL,
  `logo_url` varchar(255) default NULL,
  `top_banner` varchar(255) default NULL,
  `top_banner_url` varchar(255) default NULL,
  `about_filter` text,
  `is_active` tinyint(1) default NULL,
  `narrow_by` varchar(255) default '1',
  `page_title` varchar(255) default NULL,
  `h1_title` varchar(255) default NULL,
  `meta_keywords` text,
  `meta_description` text,
  `top_banner_alt_tag` varchar(255) default NULL,
  `logo_alt_tag` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `mk_generic_homepage_details` */

DROP TABLE IF EXISTS `mk_generic_homepage_details`;

CREATE TABLE `mk_generic_homepage_details` (
  `id` int(11) NOT NULL auto_increment,
  `page_title` varchar(255) default NULL,
  `h1_title` varchar(255) default NULL,
  `top_banner` varchar(255) default NULL,
  `top_banner_url` varchar(255) default NULL,
  `meta_keywords` text,
  `meta_description` text,
  `left_panel_filter_groups` varchar(255) default NULL,
  `bottom_text_heading` varchar(255) default NULL,
  `bottom_text` text,
  `page_name` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `mk_style_properties` */

DROP TABLE IF EXISTS `mk_style_properties`;

CREATE TABLE `mk_style_properties` (
  `id` int(11) NOT NULL auto_increment,
  `style_id` int(11) NOT NULL,
  `product_sku_id` int(11) default NULL,
  `product_tags` text,
  `product_display_name` varchar(255) default NULL,
  `variant_name` varchar(255) default NULL,
  `discount_type` varchar(255) default NULL,
  `discount_value` double(10,2) default NULL,
  `product_information` text,
  `default_image` varchar(255) default NULL,
  `front_image` varchar(255) default NULL,
  `left_image` varchar(255) default NULL,
  `right_image` varchar(255) default NULL,
  `back_image` varchar(255) default NULL,
  `top_image` varchar(255) default NULL,
  `search_image` varchar(255) default NULL,
  `search_zoom_image` varchar(255) default NULL,
  `filters` text,
  `user_rating` int(11) default '0',
  `user_rate` int(11) default '0',
  `myntra_rating` int(11) default '0',
  `add_date` int(11) default NULL,
  `modified_date` int(11) default NULL,
  `target_url` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `mk_mapping_generic`;
CREATE TABLE `mk_mapping_generic` (
  `generic_type` varchar(5) NOT NULL,
  `table_name` varchar(20) NOT NULL,
  `generic_type_pk_column` varchar(20) DEFAULT NULL,
  `description` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`generic_type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO mk_mapping_generic (generic_type, table_name, generic_type_pk_column, description) values ('PTG', 'mk_product_group', 'id', 'Product Type Group');
INSERT INTO mk_mapping_generic (generic_type, table_name, generic_type_pk_column, description) values ('PT', 'mk_product_type', 'id', 'Product Type');
INSERT INTO mk_mapping_generic (generic_type, table_name, generic_type_pk_column, description) values ('PSG', 'mk_style_group', 'style_group_id', 'Product Style Group');
INSERT INTO mk_mapping_generic (generic_type, table_name, generic_type_pk_column, description) values ('PS', 'mk_product_style', 'id', 'Product Style');
INSERT INTO mk_mapping_generic (generic_type, table_name, generic_type_pk_column, description) values ('PRD', 'xcart_products', 'productid', 'Design or Product');
INSERT INTO mk_mapping_generic (generic_type, table_name, generic_type_pk_column, description) values ('DSGR', 'mk_designer', 'customerid', 'Designer');
INSERT INTO mk_mapping_generic (generic_type, table_name, generic_type_pk_column, description) values ('SHOP', 'mk_shop', 'shopid', 'SHOP');
DROP TABLE IF EXISTS `mk_rating_generic`;
CREATE TABLE `mk_rating_generic` (
  `vote_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `remote_ip` varchar(15) DEFAULT NULL,
  `sessionid` varchar(200) DEFAULT NULL,
  `generic_type` varchar(20) NOT NULL,
  `generic_type_id` int(11) NOT NULL,
  `vote_value` int(1) DEFAULT NULL,
  `user_type` int(1) DEFAULT '0',
  `vote_date` int(11) DEFAULT NULL,
  PRIMARY KEY (`vote_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `mk_review_generic`;
CREATE TABLE `mk_review_generic` (
  `vote_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `remote_ip` varchar(15) DEFAULT NULL,
  `generic_type` varchar(20) NOT NULL,
  `generic_type_id` int(11) NOT NULL,
  `review` text,
  `rating_vote_id` int(11) DEFAULT NULL,
  `review_date` int(11) DEFAULT NULL,
  PRIMARY KEY (`vote_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `mk_left_panel_search_link`;
CREATE TABLE `mk_left_panel_search_link` (
	`link_id` int(11) NOT NULL AUTO_INCREMENT,
	`link_name` varchar(50) NOT NULL,
	`link` varchar(255) NOT NULL,
	`link_description` varchar(255) DEFAULT NULL,
	`is_active` tinyint(1) default NULL,
	`link_position` int(11) NOT NULL,
	PRIMARY KEY (`link_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `mk_best_selling_product_style`;
CREATE TABLE `mk_best_selling_product_style` (
       `product_style_id` int(11) NOT NULL,
       PRIMARY KEY (`product_style_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;