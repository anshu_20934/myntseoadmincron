insert into mk_feature_gate_key_value_pairs (`key`,`value`,`description`) values('myntra.loyalty.enabled','true','Feature gate for loyalty. If true, Loyalty will be enabled else disabled.');
insert into mk_feature_gate_key_value_pairs (`key`,`value`,`description`) values('showMyPrivilegeTab','true','Feature gate for to show my privilege tab instead of referral.');
insert into mk_feature_gate_key_value_pairs (`key`,`value`,`description`) values('mymyntra.myntcredit.defaultTab','mycoupons','mycoupons,myprivilege,mycashback');

insert into mk_widget_key_value_pairs(`key`, `value`, `description`) values('loyalty.label','Loyalty','Label to be used throughout the site for loyalty points');
insert into mk_widget_key_value_pairs(`key`, `value`, `description`) values('loyalty.confirmpage.disclaimer','These will get reflected after 30 minutes','Disclaimer Message to be shown in confirmpage');
insert into mk_widget_key_value_pairs(`key`, `value`, `description`) values('loyalty.mymyntra.activitytable.disclaimer','Updated after every 30 minutes','Disclaimer Message to be shown in mymyntra page for recent activity table');

insert into mk_widget_key_value_pairs(`key`, `value`, `description`) values('loyalty.users','gaurav.tiwari@myntra.com,rgv@test.com','Comma seperated value of email address. If @myntra.com => enable for all login with domain @myntra.com. If value is "all" loyalty is enabled for all user. If blank loyalty is disabled. ');
insert into mk_widget_key_value_pairs(`key`, `value`, `description`) values('loyalty.mymyntra.terms','[
{"label" : "1.ELIGIBILITY, MEMBERSHIP, ACCRUAL",
    "terms"  : [
        {"term" : "1.1. These terms and conditions are operational only in India and open to participation of all the registered members, resident of India of myntra.com, over and above the age of 18 years."},
        {"term" : "1.2. The program may be extended to other geographies and other audiences at Myntra\'s discretion."},
        {"term" : "1.3. Membership will be offered to the  MY Privilege(program brand name)"},
        {"term" : "1.4. Program membership will be confirmed only when the registered member clicks the \'Accept Program Membership\' button on the webpage."},
        {"term" : "1.5. Membership will be identified by a single registered email id confirmed by the registered member at time of first login on the Myntra\'s webpage."},
        {"term" : "1.6. Two email id\'s cannot be combined under a single membership."},
        {"term" : "1.7. Membership will be activated for existing and new members/customers as follows:",
            "subterms" :[
                            {"subterm" : "i. To an existing Myntra member who already has a registered id with Myntra - on acceptance of program membership."},
                            {"subterm" : "ii. For new customers/member - on acceptance of program membership."}
                        ]
        },
        {"term" : "1.8. Membership is valid for life until such time as myntra.com decided to terminate this loyalty programme, with or without notice to its members. Accumulation of points shall cease immediately on termination of this programme, but points already accrued shall be redeemable for 60 days."},
        {"term" : "1.9. Once membership is activated, the member starts earning points whenever they purchase products on Myntra.com.",
            "terms" : [
                    {"term" : "1.9.1. Points will be at a certain percent of the purchase value specified by Myntra."},
                    {"term" : "1.9.2. The percent may vary by product, brand etc at the discretion of Myntra."},
                    {"term" : "1.9.3. Myntra points are not legal tender, have no monetary value and cannot be bartered or sold for cash."},
                    {"term" : "1.9.4. Points can be redeemed for products on Myntra.com or any other products specified by Myntra at an exchange rate of 2 points being equivalent to INR1."},
                    {"term" : "1.9.5. All points have the same exchange value."},
                    {"term" : "1.9.6. This exchange rate can be changed from time to time at Myntra\'s discretion."},
                    {"term" : "1.9.7. Points will be valid for a 12 month period on a rolling month (end of month) basis."},
                    {"term" : "1.9.8. Points will be credited to the member\'s account immediately after the successful acceptance of the order."},
                    {"term" : "1.9.9. Fractional points will be rounded off to the nearest integer. Example: if total points are 99.4,it shall be added as 99 points, 99.5 and above shall be added as 100 points."}
                ]
        }
    ]
},

{"label" : "2. TIER SYSTEM FOR MEMBERS/CUSTOMERS",
    "terms"  : [
        {"term" : "Members/customers will be slotted into tiers based on total amount purchased on Myntra",
            "terms" : [
                {"term": "2.1.1. New customers will enter into the BASE TIER of the program."},
                {"term": "2.1.2. If a customer purchases worth Rs. 3000 (total net bill value after all discounts) in a six month period he/she shall be upgraded to the MIDDLE TIER."},
                {"term": "2.1.3. If a customer purchases worth Rs. 8000 in a six month period he/she will upgrade to the TOP TIER."},
                {"term": "2.1.4. Tiers will be branded with names - and the thresholds may change from time to time at Myntra\'s discretion"},
                {"term": "2.1.5. Existing customers as of (September 15\'2013) may be invited to join the program directly into a tier based on the total net bill value of purchases in the six  months before program launch",
                    "terms" : [
                        {"term" : "2.1.5.1. Rs. {#tierOnePurchaseValue} in 3 months - Tier 1"},
                        {"term" : "2.1.5.2. Rs. {#tierTwoPurchaseValue} in 3 months - Tier 2"},
                        {"term" : "2.1.5.2. Rs. {#tierThreePurchaseValue} in 3 months - Tier 3"}
                    ]   
                },
                {"term" : "2.1.6. Points earned increase with tier at a specified percent value."},
                {"term" : "2.1.7. Once a tier is attained it can be retained for 12 months."},
                {"term" : "2.1.8. At the end of 12 months, member tiers will be revalidated based on prevailing revalidation criteria."},
                {"term" : "2.1.9. Members not fulfilling revalidation criteria will be downgraded to one tier lower than the current tier."},
                {"term" : "2.1.10. Tier will be upgraded the moment the member attains a higher threshold based on 6 months purchase."}
            ]   
        }
    ]
}, 

{"label" : "3. GENERAL TERMS AND CONDITIONS",
    "terms"  : [
            {"term": "3.1. Each member is responsible for remaining knowledgeable about the Myntra Program Terms and Conditions and the number of points in his or her account."},
            {"term": "3.2. Myntra will send correspondence to active members to advise them of matters of interest, including notification of Myntra Program changes and Points Updates."},
            {"term": "3.3. Myntra will not be liable or responsible for correspondence lost or delayed in the mail/e-mail."},
            {"term": "3.4. Myntra reserves the right to refuse, amend, vary or cancel membership of any Member without assigning any reason and without prior notification."},
            {"term": "3.5. Any change in the name, address, or other information relating to the Member must be notified to Myntravia the Helpdesk/email by the Member, as soon as possible at <a href=\'mailto:support@myntra.com\'>support@myntra.com</a> or call at {#customerSupportCall} 24 Hours a Day / 7 Days a Week."},
            {"term": "3.6. Myntra reserves the right to add,modify,delete or otherwise change the Terms and Conditions without any approval, prior notice or reference to the Member."},
            {"term": "3.7. In the event of dispute in connection with Myntra Program and the interpretation of Terms and Conditions, Myntra\'s decision shall be final and binding."},
            {"term": "3.8. This Policy and these terms shall be read in conjunction with the standard legal policies of Myntra.com, including its Privacy policy."}
    ]
}
]','Terms and Conditions for loyalty mymyntra page');
insert into mk_widget_key_value_pairs(`key`, `value`, `description`) values('loyalty.mymyntra.banner_img_url','http://myntra.myntassets.com/skin2/images/loyalty_main.png','Banner Image url for loyalty mymyntra page');
insert into mk_widget_key_value_pairs(`key`, `value`, `description`) values('loyalty.mymyntra.loyalty_description','Fashion is rewarding at Myntra. A loyalty program is designed especially for shopping lovers because more you shop, more you earn.<br/> Use these points as cash to shop great products at Myntra. Happy shopping.','Loyalty description text just below the banner image.');
insert into mk_widget_key_value_pairs(`key`, `value`, `description`) values('loyalty.mymyntra.how_it_works.block1','{"heading":"Shop,Share,Like Products","description":"on Myntra.com. More you do,<br/>better it is"}','Loyalty Mymyntra How it works block 1. Heading and description');
insert into mk_widget_key_value_pairs(`key`, `value`, `description`) values('loyalty.mymyntra.how_it_works.block2','{"heading":"Earn points","description":"Upgrade your level to earn<br/>more points."}','Loyalty Mymyntra How it works block 2. Heading and description');
insert into mk_widget_key_value_pairs(`key`, `value`, `description`) values('loyalty.mymyntra.how_it_works.block3','{"heading":"Shop, share, like products","description":"Points convert to cash after<br/>30-days return period is over"}','Loyalty Mymyntra How it works block 3. Heading and description');



