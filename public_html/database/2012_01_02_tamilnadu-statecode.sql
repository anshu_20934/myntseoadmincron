-- Portal-1557: Kundan: although iso code for tamil nadu is TN, in our DB it is consistently TK. so changing that in Geo Database
update geo.states set iso_code = 'IN-TK' where iso_code = 'IN-TN';