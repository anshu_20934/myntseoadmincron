use myntra;
/*1*/
insert into `mk_text_font_color` (color) values('#e1e1e1');

/*2*/
update mk_customization_orientation set textFontColors='1',textColorDefault='1' where id=1606;
update mk_customization_orientation set textFontColors='140',textColorDefault='140' where id=1599;
update mk_customization_orientation set textFontColors='140',textColorDefault='140' where id=1601;
update mk_customization_orientation set textFontColors='140',textColorDefault='140' where id=1604;

/*3*/
alter table mk_product_type add column type_h1 text default null;
