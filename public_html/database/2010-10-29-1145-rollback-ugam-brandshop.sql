alter table mk_org_accounts drop column landingpage;
delete from mk_org_email where orgid=(select id from mk_org_accounts where subdomain='ugam');
delete from mk_email_notification where name='orgugamregistration';
delete from mk_org_accounts where subdomain='ugam';
