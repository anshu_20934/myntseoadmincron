
CREATE TABLE IF NOT EXISTS mk_shipment_tracking(
	id			INTEGER(20) AUTO_INCREMENT NOT NULL UNIQUE,
	ref_id			INTEGER(20) UNSIGNED NOT NULL,
	ref_type  		ENUM('o','i') NOT NULL DEFAULT 'o',
	shipment_type		ENUM('s', 'r', 'rs') NOT NULL DEFAULT 's',		
	tracking_no		BIGINT UNSIGNED NOT NULL,
	return_tracking_no	BIGINT UNSIGNED,
	courier_operator	VARCHAR(15) NOT NULL,
	delivery_status		ENUM('DL', 'RT', 'FIT', 'RIT') NOT NULL DEFAULT 'FIT',
	failed_attempts		INTEGER UNSIGNED NOT NULL DEFAULT 0,
	pickup_date		DATETIME NOT NULL,
	delivery_date 		DATETIME						
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE mk_shipment_tracking ADD UNIQUE INDEX (courier_operator, tracking_no); 
ALTER TABLE mk_shipment_tracking ADD UNIQUE INDEX ref_entity_index (ref_id, ref_type);

CREATE TABLE IF NOT EXISTS mk_shipment_tracking_detail(
	detail_id		BIGINT UNSIGNED AUTO_INCREMENT NOT NULL UNIQUE,
	id 			INTEGER(20) NOT NULL,
	location		VARCHAR(80) NOT NULL,
	action_date		DATETIME NOT NULL,
	activity_type		ENUM('AT', 'OT') NOT NULL DEFAULT 'OT',
	movement	 	ENUM('f','b') NOT NULL DEFAULT 'f',				
	remark			VARCHAR(250) NOT NULL			
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE mk_shipment_tracking_detail ADD FOREIGN KEY (id) REFERENCES mk_shipment_tracking(id) ON DELETE CASCADE;
