/*query*/
use myntra;

insert into mk_fonts (id,name,image_name,ttf_name,display_order) values ('153','comic','comic.png','comic.ttf','1');

delete from mk_text_font_detail where orientation_id=1664 and font_id=150;
delete from mk_text_font_detail where orientation_id=1664 and font_id=151;

delete from mk_text_font_detail where orientation_id=1665 and font_id=150;
delete from mk_text_font_detail where orientation_id=1665 and font_id=151;

delete from mk_text_font_detail where orientation_id=1666 and font_id=150;
delete from mk_text_font_detail where orientation_id=1666 and font_id=151;

delete from mk_text_font_detail where orientation_id=1667 and font_id=150;
delete from mk_text_font_detail where orientation_id=1667 and font_id=151;

delete from mk_text_font_detail where orientation_id=1668 and font_id=150;
delete from mk_text_font_detail where orientation_id=1668 and font_id=151;

delete from mk_text_font_detail where orientation_id=1669 and font_id=150;
delete from mk_text_font_detail where orientation_id=1669 and font_id=151;

/*4.insert into mk_text_font_detail*/
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1664','150','0.75','12','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1664','153','0.75','12','N','Y');


insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1665','150','0.75','12','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1665','153','0.75','12','N','Y');


insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1666','150','0.75','12','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1666','153','0.75','12','N','Y');


insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1667','150','0.75','12','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1667','153','0.75','12','N','Y');


insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1668','150','0.75','12','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1668','153','0.75','12','N','Y');


insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1669','150','0.75','12','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1669','153','0.75','12','N','Y');
