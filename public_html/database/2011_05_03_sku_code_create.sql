ALTER TABLE mk_skus ADD (
        `description` longtext,
        `last_modified_on` datetime,
        `brand_id` int(11),
        `article_type_id` int(11),
        `created_by` varchar(50),
        `created_on` datetime,
	`vendor_article_no` varchar(45),
	`vendor_article_name` varchar(45),
	`remarks` varchar(255),
	`size` varchar(10)
);

ALTER TABLE mk_attribute_type_values ADD COLUMN attribute_code VARCHAR(10) DEFAULT NULL;
