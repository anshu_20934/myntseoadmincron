CREATE TABLE `MRP_cellnext_stats` (
 `mobile` VARCHAR(32) NOT NULL,
 
 `login` VARCHAR(128) NOT NULL,
  
 `captured_lock_on` INT(11) NOT NULL,
  PRIMARY KEY (`mobile`)
);

ALTER TABLE `mk_mobile_mapping`
ADD COLUMN `cod_verification_status` VARCHAR(1) DEFAULT '1';

insert into mk_feature_gate_key_value_pairs(`key`, `value`, `description`) values('codMobileVerification.enable', 'true', 'false removes the mobile verification for cod customers.');

