/*1*/
use myntra;


/*2*/
update mk_product_group set url='laptop-bags-backpacks' where id='13';

/*3*/
alter table mk_product_type change column type_h1 type_h1 varchar(250) default null;
alter table mk_product_type add column type_title varchar(250) default null, add column type_keywords varchar(250) default null,add column type_metadesc text default null;

/*4*/
update mk_product_type set type_title='Laptop Bags & Backpacks |Nike Bags | Buy Laptop Bags Online in India', type_keywords='laptop bags, laptop bags india, buy laptop bags online,backpacks, backpacks india, Nike bags', type_metadesc='Buy personalized Laptop Bags or Backpacks with text of your choice embroidered. Starting price Rs. 899. Ships internationally within 4 days.' where id='224';
update mk_product_type set type_title='Travel Bags | Luggage Bags | Buy Personalized Travel Bags Online in India', type_keywords='travel bags, buy travel bags online, travel bags india, luggage bags, luggage bags india', type_metadesc='Buy personalized Travel Bags with text of your choice embroidered. Starting price Rs. 799. Luggage bags ship internationally within 4 days.' where id='226';




