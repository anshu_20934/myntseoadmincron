ALTER TABLE `discount_free_item`  add is_active tinyint(1) DEFAULT '1';
alter table discount_free_item  ADD INDEX is_active (is_active);
ALTER TABLE `discount_free_item_history`  add is_active tinyint(1) DEFAULT '1';
alter table discount_free_item_history  ADD INDEX is_active (is_active);
