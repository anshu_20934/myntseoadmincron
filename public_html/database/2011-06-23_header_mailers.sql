insert into  mk_email_notification 
 (name, subject, body) 
 values
 ("header","", '<div style="border:1px solid gray;">
  <div style="height:75px; background-color:#FFFFFF;margin:0px; padding:0px;">
	<div style="float:left; padding-left: 10px;"><a href="http://www.myntra.com/" target="_blank"><img src="http://myntramailer.s3.amazonaws.com/myntra-logo.jpg" alt="Myntra" title="Myntra" width="215" height="75" style="line-height:0px;border:0px;color:#FFF;font-family:arial,helvetica,serif;font-size:12px;"></a></div>
	<div style="float: right; margin-top:6px;">
	
	<span style="float: right;  font-size 11px; font-family: arial; margin: 6px 0px 10px 0px; padding: 0px 5px;"><strong style="display: block; font-size: 12px; color: #4d4d4d; line-height: 17px;">[CASHBACK_MESSAGE]<br>FREE SHIPPING<br>CASH ON DELIVERY
</strong></span>
	<span style="float: right;">
	<a href="http://www.myntra.com" alt="refer and win" title="refer and win" width="215" height="75"  target="_blank"><img src="http://cdn.myntra.com/skin1/images/referandwin-header.jpg" style="line-height:0px;border:0px;color:#FFF;font-family:arial,helvetica,serif;font-size:12px;"/></a></span>
	</div>
	<div style="clear: both;"></div>
  </div>
  <div style="height: 35px; background-color: rgb(103, 103, 103); margin: 0pt 0pt 0px;">
    <ul style="margin:0px;float:left;color:white;font-family:Arial,Helvetica,sans-serif;font-size:12px;list-style:none outside none;padding:0px;line-height:15px;">
      <li style="border-right:1px solid #8a8a8a; float:left;margin:0px;"><a href="http://www.myntra.com/cricket-jerseys" target="_blank" style="color: rgb(255, 255, 255); text-decoration: none; display: block; padding: 8px 15px;">Sports Jerseys</a></li>
      <li style="border-right:1px solid #8a8a8a; float:left;margin:0px;"><a href="http://www.myntra.com/tshirts" target="_blank" style="color: rgb(255, 255, 255); text-decoration: none; display: block; padding: 8px 15px;">Tshirts</a></li>
      <li style="border-right:1px solid #8a8a8a; float:left;margin:0px;"><a href="http://www.myntra.com/sports-footwear" target="_blank" style="color: rgb(255, 255, 255); text-decoration: none; display: block; padding: 8px 10px;">Sports Footwear</a></li>
      <li style="border-right:1px solid #8a8a8a; float:left;margin:0px;"><a href="http://www.myntra.com/casual-footwear" target="_blank" style="color: rgb(255, 255, 255); text-decoration: none; display: block; padding: 8px 15px;">Casual Footwear</a></li>
      <li style="border-right:1px solid #8a8a8a; float:left;margin:0px;"><a href="http://www.myntra.com/brands" target="_blank" style="color: rgb(255, 255, 255); text-decoration: none; display: block; padding: 8px 15px;">Brands</a></li>
      <li style="float: left;margin:0px;"><a href="http://www.myntra.com/accessories" target="_blank" style="color: rgb(255, 255, 255); text-decoration: none; display: block; padding: 8px 20px;">Accessories</a></li>
    </ul>
  </div>
 </div>');
 
update mk_email_notification set body='Hi [USER], <br/><br/>
This is to confirm that your order with Myntra has been successfully placed!<br/>Your Order No. is [ORDER_ID]. Please use this for future reference.<br/><br/>
[ORDER_DETAILS]
<br/>
<p>Your order is under process and we will be sending a separate email once your order has been shipped.</p><br>
The delivery address is:<br>
[DELIVERY_ADDRESS]
<br><br>[CASHBACK_SUMMARY]
<p>Your experience at Myntra.com is important to us. If you have any queries or suggestions, please email us at <a href="mailto:support@myntra.com">support@myntra.com</a>, or call us at [MYNTRA_CC_PHONE]. We look forward to hearing from you!</p>
<br/>
Regards,<br/>
Myntra.com'
where name='order_confirmation';

insert into  mk_email_notification 
 (name, subject, body) 
 values
 ("footer", "",
'<div style="margin-top:0px;background-color:#9b9b9b;border:0px solid red; height:30px; "> 
    <div style="_margin-left:10px; padding:3px 8px 2px 8px;height:32px; font-family:arial,helvetica,serif; font-size:12px; position:absolute;">
      <span style="float:left;width:370px; margin-top: 5px;color:#FFFFFF;"> <a style="color:#FFFFFF;" href="mailto:support@myntra.com">support@myntra.com</a> | Call [MYNTRA_PHONE] | Connect with us</span> <span style="float:left; width:100px"><a href="http://www.facebook.com/pages/Myntracom/6466648220?v=wall" target="_blank" /><img style="cursor: pointer;" src="http://cdn.myntra.com/skin1/images/facebook-icon.jpg" border="none"/></a>
	<a href="http://twitter.com/myntradotcom" target="_blank" /> <img style="cursor: pointer;" src="http://cdn.myntra.com/skin1/images/twitter-icon.jpg" border="none"/></a>
	  </span>
      <span style="float:right;width:100px; margin-top: 5px; margin-right: 10px;"><a style="color:#FFFFFF;" href="www.myntra.com">www.myntra.com</a></span>
      
    </div>
  </div>
  <div style="clean:both;font-size:0px;line-height:0px;">&nbsp;</div>
  <p style="font-family:arial,helvetica,serif; font-size:11px; color:#1f1f1f;">Offer valid on Myntra.com only. Myntra logo is a registered trademark of www.myntra.com, India. Check out our Privacy Policy and User Agreement. Unsubscribe from this newsletter. Forgot your password? Need help or have any question? Contact us at Customer Support. <br>
    &copy; 2011. All Right Reserved. www.myntra.com</p>
</div>');
