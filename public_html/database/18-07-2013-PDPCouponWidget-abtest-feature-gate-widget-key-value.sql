
/**AB TEST**/
insert into mk_abtesting_tests (name,ga_slot,seg_algo,source_type)
values('PDPCouponWidget','1','abtest\\algos\\impl\\RandomSegmentationAlgo','tpl');


insert into mk_abtesting_variations (name,ab_test_id,percent_probability) 
values('test',(select id from mk_abtesting_tests where name ='PDPCouponWidget'),0);
insert into mk_abtesting_variations (name,ab_test_id,percent_probability) 
values('control',(select id from mk_abtesting_tests where name ='PDPCouponWidget'),100);

/**FEATURE GATE**/

insert into mk_feature_gate_key_value_pairs(`key`, `value`, `description`) 
	values('PDPCouponWidget.enable', 0, 'Comma seperated list of coupons');

/**WIDGET KEY VALUE **/

insert into mk_widget_key_value_pairs(`key`, `value`, `description`) 
	values('specialCouponsForPDPWidgetDisplay', '', 'Comma seperated list of coupons');
insert into mk_widget_key_value_pairs(`key`, `value`, `description`) 
	values('pdpCouponWidgetMessage', 'get Additional 20% off on all products', 'Footer message on the coupon widget');