alter table landing_page 
    add column is_autosuggest tinyint(1) DEFAULT 1,
    add column alt_text varchar(500);
