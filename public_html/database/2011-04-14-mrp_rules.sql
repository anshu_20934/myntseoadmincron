-- Rules table.
CREATE TABLE `mk_mrp_rules` (
  `ruleid` INT(11) NOT NULL,
  `name` VARCHAR(50),
  `isActive` TINYINT(1),
  `isRootLevel` TINYINT(1),
  `childRules` VARCHAR(255),
  `applicableOn` VARCHAR(50),
  `isPrimitive` TINYINT(1),
  `fieldToCompare` VARCHAR(32),
  `operatorToCompare` VARCHAR(32),
  `valueToCompare` VARCHAR(32),
  `successAction` VARCHAR(32),
  `failureAction` VARCHAR(32),
  `nonPrimitiveFunction` VARCHAR(32),
  `isSynchronous` TINYINT(1),
  PRIMARY KEY (`ruleid`));

-- Rule execution log
CREATE TABLE `mk_mrp_log` (
  `ruleid` INT(11) NOT NULL,
  `rulename` VARCHAR(50),
  `timestamp` INT(11),
  `appliedOn` VARCHAR(128),
  `result` TINYINT(1),
  `coupon` VARCHAR(50),
  `couponUser` VARCHAR(128));

-- Table to record per user coupon codes.
CREATE TABLE `mk_mrp_coupons_mapping` (
  `login` VARCHAR(128) NOT NULL,
  `coupon` VARCHAR(50));

-- Tracking referrals.
CREATE TABLE `mk_referral_log` (
  `referer` VARCHAR(128) NOT NULL,
  `referred` VARCHAR(128) NOT NULL,
  `dateReferred` INT(11) NOT NULL,
  `dateJoined` INT(11),
  `dateFirstPurchase` INT(11),
  `success` TINYINT(1) NOT NULL DEFAULT 0);

-- SMS verification table.
CREATE TABLE `mk_mobile_mapping` (
  `mobile` VARCHAR(32) NOT NULL,
  `login` VARCHAR(128) NOT NULL,
  `status` VARCHAR(1) NOT NULL,
  `date_verified` INT(11) NOT NULL,
  PRIMARY KEY (`mobile`, `login`));

-- mobile verification code table.
CREATE TABLE `mk_mobile_code` (
  `mobile` VARCHAR(32) NOT NULL,
  `code` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`mobile`));

-- Populate data in the SMS verification table.

-- Populate rules.
