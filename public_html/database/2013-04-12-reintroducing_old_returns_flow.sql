insert into mk_return_status_details (code,display_name,mymyntra_status,mymyntra_msg,end_state,cust_end_state) values 
('CPDC','CC Approved Return at DC','ACCEPTED','Your return has been accepted by us. You will be issued a refund on this item within 24 hours.',0,0),
('CFDC','CC Declined Returns at DC','REJECTED','Your return request has been declined as it did not meet our <a href="[GUIDE_LINES_LINK]" onclick="_gaq.push([\'_trackEvent\', \'myreturns\', \'view_returns_policy\', \'click\']);" target="_blank">Returns Policy</a>. We will contact you soon regarding this.',0,0),
('RQCP','CC Approved Return','ACCEPTED','Your return has been accepted by us. You will be issued a refund on this item within 24 hours.',0,0),
('RQCF','CC Declined Returns','REJECTED','Your return request has been declined as it did not meet our <a href="[GUIDE_LINES_LINK]" onclick="_gaq.push([\'_trackEvent\', \'myreturns\', \'view_returns_policy\', \'click\']);" target="_blank">Returns Policy</a>. We will contact you soon regarding this.',0,0);

insert into mk_return_state_transitions (from_state,to_state,description,sla,admin_access,roles_allowed) values 
('RQCF','RRRS','Reshipping rejected returns',24,0,'OP,AD'),
('RQCF','RRJ','Disposed rejected returns',24,0,'OP,AD'),
('RQCP','RIS','Restocking returned item',24,0,'OP,AD'),
('RQSF','RQCP','Return CC review pass',24,0,'CS,AD'),
('RQSF','RQCF','Return CC review fail',24,0,'CS,AD'),
('RJDC','CPDC','CC Approved Return at DC',24,0,'OP,AD'), 
('RJDC','CFDC','CC Declined Returns at DC',24,0,'OP,AD'), 
('CPDC','RRC','Returns Received at WH',24,0,'OP,AD'), 
('CFDC','RRRS','Reshipping rejected returns',24,0,'OP,AD');


delete from mk_return_state_transitions where from_state = 'RQSF' and to_state in ('RRRS','RRJ');
delete from mk_return_state_transitions where from_state = 'RJDC' and to_state in ('RRC','RRRS');
update mk_return_state_transitions set from_state = 'RQCF' where from_state = 'RQSF' and to_state = 'RIS';

update mk_courier_service set return_supported = 1 where code  = 'BD';
