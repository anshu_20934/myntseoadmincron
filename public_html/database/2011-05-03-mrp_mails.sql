-- First login mail.
INSERT INTO `mk_email_notification`
  (name, subject, body)
VALUES
  ('mrp_first_login', 'Welcome to Myntra.com', '<html>
        <body>
                <p>Hello [USER],</p>
                <p>Welcome to the ever-growing Myntra family!<br>
                We assure you of finding the best sports and casual lifestyle products, across a wide range of brands, on <a href="http://www.myntra.com?utm_source=welcomemailer&utm_medium=mailerOnline&utm_campaign=MRP">Myntra.com</a>.</p>
                <p>To start your journey, we have credited your account with Rs. [MRP_AMOUNT], valid for the next [VALIDITY] days, to redeem against your first purchase.
                <a href="http://www.myntra.com/mymyntra.php" target="_blank">Click here</a> to view your coupons and start your exciting journey as a Myntra customer!</p>
                <p>You can even invite your friends to join the bandwagon to earn more rewards!</p>
                <p>So, what are you waiting for?<br>
                Click on the icons below to start inviting your friends on Facebook and Twitter.<br>
                <a href="http://www.facebook.com/sharer.php?u=http://www.myntra.com/register.php?ref=[REFERER]" target="_blank">
                        <img src="http://cdn.myntra.com/skin1/myntra_images/facebook.png" alt="Invite friends on Facebook." border="0"></a>
                <a href="http://twitter.com/home?status=I loved shopping at Myntra.com... Join me! http://www.myntra.com/register.php?ref=[REFERER_TW]" target="_blank">
                    <img src="http://cdn.myntra.com/skin1/myntra_images/twitter.png" alt="Invite followers from Twitter." border="0"></a></p>
                <p>Shop more, invite more and save more!</p>
                <p>In case of any queries, do feel free to get in touch with our 24X7 customer care at +91-80-43541999.</p>
                <p>Thanks!<br>
                Team Myntra</p>
        </body>
</html>');

-- Referral successful mail.
INSERT INTO `mk_email_notification`
  (name, subject, body)
VALUES
  ('mrp_referral_successful', 'Rs. 1000 credited to your Myntra account', '<html>
        <body>
                <p>Hello [REFERER],</p>
                <p>Your referred friend, [REFERRED] has made their first purchase on <a href="http://www.myntra.com?utm_source=MRP&utm_medium=Onlinemailer&utm_campaign=welcomemail">Myntra.com</a>!</p>
                <p>To convey our gratitude, we have credited your account with Rs. [MRP_AMOUNT]/-<br>
                Use the credits to shop more and save even more!<br>
                <a href="http://www.facebook.com/sharer.php?u=http://www.myntra.com/register.php?ref=[REFERER]" target="_blank">
                    <img src="http://cdn.myntra.com/skin1/myntra_images/facebook.png" alt="Invite friends on Facebook."></a>
                <a href="http://twitter.com/home?status=I loved shopping at Myntra.com... Join me! http://www.myntra.com/register.php?ref=[REFERER_TW]" target="_blank">
                    <img src="http://cdn.myntra.com/skin1/myntra_images/twitter.png" alt="Invite followers from Twitter."></a></p>
                <p>In case of any queries, do feel free to get in touch with our 24X7 customer care at +91-80-43541999.</p>
                <p>Thanks!<br>
                Team Myntra.</p>
        </body>
</html>');

-- Referral invitation mail.
INSERT INTO `mk_email_notification`
  (name, subject, body)
VALUES
  ('mrp_referral_invitation', 'Invitation to join Myntra.com', '<html>
        <body>
                <p>Hi,</p>
                <p>I have registered with <a href="http://www.myntra.com/home.php?utm_source=MRP&utm_medium=refmailer&utm_campaign=myntra">Myntra.com</a>, my online destination for fab sports and casual branded products. If you love brands, then join me by just clicking on this link <a href="http://www.myntra.com/register.php?ref=[REFERER]&utm_source=MRP&utm_medium=refmailer&utm_campaign=refmailerjoin">www.myntra.com/register.php?ref=[REFERER]</a></p>
                <p>On registration, you would receive a Rs. 500 gift coupon that you can use on your first purchase.</p>
                <p>I\'m sure you will enjoy browsing and buying from the excellent product catalog on Myntra as much as I did!</p>
                <p>Cheers,<br>
                [NAME-REFERER]</p>
        </body>
</html>');
