alter table xcart_returns add validated_reason varchar(255) default NULL;

alter table mk_returns_reasons add customer_visible boolean NOT NULL default true;

update mk_returns_reasons set displayname='Product quality issue - manufacturing defect - customer used', customer_visible=0 where code='PMD';

insert into mk_returns_reasons (code, displayname, display_order, customer_visible) values ('PMDU', 'Product quality issue - manufacturing defect - customer unused', 8, 0);
