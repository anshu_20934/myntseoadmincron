
ALTER TABLE  mk_feedback_questions ADD COLUMN details INT DEFAULT 0 ;
UPDATE mk_feedback_questions SET details = 1, question = 'It was quite easy to browse and locate the product on website.' WHERE question = 'Did you find it easy to locate the product and also place your order?';
UPDATE mk_feedback_questions SET details = 1, question = 'The online payment process was smooth (applicable for online payment transaction).' WHERE question = 'Did you have any difficulty in completing the payment transaction (applicable for debit or credit card transaction) ?';
UPDATE mk_feedback_questions SET details = 1, question = 'Received a prompt response and resolution, whenever contacted Myntra Customer Care team.' WHERE question = 'Did you get a prompt response and resolution, whenever you had contacted our CS ?';
UPDATE mk_feedback_questions SET details = 1, question = 'Product was delivered as per the committed delivery time' WHERE question = 'Did you receive the product, as per the committed delivery time?';
UPDATE mk_feedback_questions SET details = 1, question = 'Product was delivered as per the expected standards' WHERE question = 'Did you receive the product, as per the expected standards?';
UPDATE mk_feedback_questions SET question = 'Would consider shopping with Myntra again' WHERE question = 'Would you consider shopping with Myntra again?';
UPDATE mk_feedback_questions SET question = 'Bought this product as a gift for someone' WHERE question = 'Did you buy the product as a gift for someone else?';



UPDATE mk_email_notification
SET body = 'User [USER] has sent following feedback:<br><br>
<ol>
<li>
It was quite easy to browse and locate the product on website.<span style="color:red;">[ANS1]</span>
</li>
<li>
 The online payment process was smooth (applicable for online payment transaction). <span style="color:red;">[ANS2]</span>
</li>
<li>
Received a prompt response and resolution, whenever contacted Myntra Customer Care team. <span style="color:red;">[ANS3]</span>
</li>
<li>
 Product was delivered as per the committed delivery time. <span style="color:red;">[ANS4]</span>
</li>
<li>
Product was delivered as per the expected standards. <span style="color:red;">[ANS5]</span>
</li>
<li>
Would consider shopping with Myntra again <span style="color:red;">[ANS6]</span>
</li>
<li>
Bought this product as a gift for someone<span style="color:red;">[ANS7]</span>
</li>
<li>
Do you have any feedback or suggestions for the myntra.com? If yes, please describe below.<br>
<span style="color:red;">[ANS8]</span>
</li>
</ol>'
WHERE NAME = 'feedback_survey';


