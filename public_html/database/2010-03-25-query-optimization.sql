alter table mk_home_featured_products add column image_url varchar(1024);

update xcart_products p , mk_home_featured_products h
set image_url = concat("http://images.myntra.com/" , SUBSTRING(image_portal_t, 3))
where h.product_id = p.productid ;


alter table mk_product_tag_map add index `tagid_index` (tagid);
alter table mk_seo add index `pageurl_idx` (pageurl);
alter table mk_tag_product_type_count add index `product_type_id` (product_type_id);
alter  table xcart_customers add index `email_idx` (email);

/* Following queries are not urgent - */

alter table xcart_sessions_data engine = innodb;
alter table xcart_order_details engine = innodb;
alter table xcart_customers engine = innodb;