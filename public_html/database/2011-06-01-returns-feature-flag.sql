delete from `mk_widget_key_value_pairs` where `key` = 'returnenabled';

insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('returnenabled','false','(LOWERCASE)true/false to enable/disable 365 days feature');
