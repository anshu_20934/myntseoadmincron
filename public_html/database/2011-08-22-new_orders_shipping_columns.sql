ALTER TABLE xcart_orders add column shippeddate int(11), add column delivereddate int(11);

update xcart_orders set shippeddate = completeddate, delivereddate = completeddate where payment_method != 'cod' and status = 'C'; 

UPDATE xcart_orders o LEFT JOIN xcart_order_details od ON o.orderid = od.orderid SET o.shippeddate = od.sent_date, o.delivereddate = o.completeddate WHERE o.payment_method = 'cod' and o.status in ('SH', 'C');
