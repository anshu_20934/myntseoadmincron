UPDATE mk_email_notification SET body='<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
<tr>
	<td valign="top" align="center" style=" font-family:arial, helvetica, sans-serif">
		<table width="620" cellspacing="0" cellpadding="0" border="0" align="center">
		<tr>
			<td valign="top" background="http://myntramailer.s3.amazonaws.com/february2013/1359975582border-shadow.jpg" align="center">
				<table width="580" cellspacing="0" cellpadding="0" border="0" align="center">
				<tr>
					<td>
						<table width="580" height="75" cellspacing="0" cellpadding="0" border="0" align="center">
						<tr>
							<td width="220" valign="top">
							  <a href="http://www.myntra.com/?utm_source=marketing&utm_medium=mailer&utm_campaign=welcome-coupon-06052013" target="_blank"><img src="http://myntramailer.s3.amazonaws.com/february2012/1329901955myntra-logo.jpg" alt="MYNTRA.COM" title="MYNTRA.COM" width="200" height="70" style="border:0px;color:#f00c6b;font-family:arial,helvetica,serif;font-size:24px;"></a>
							</td>
							<td width="360">
								<table width="350" cellspacing="0" cellpadding="0" border="0" align="center" style="color: #838383; font-family:Arial,serif;font-size:10px; line-height:20px; text-align:center; text-transform:uppercase;">
								<tr>
									<td>&bull; Free Shipping on orders above Rs. [SHIPPING_THRESHOLD]</td>
									<td>&bull; Cash On Delivery</td>
									
								</tr>
								</table>
								<table width="240" cellspacing="0" cellpadding="0" border="0" align="center" style="color: #838383; font-family:Arial,serif;font-size:10px; line-height:20px; text-align:center; text-transform:uppercase;">
								<tr>
									<td>&bull; 24-Hour Dispatch</td>
									<td>&bull; Free 30-Day Returns</td>
								</tr>
								</table>
							</td>
						</tr>
						</table>
						
						
						<table width="580" height="29" cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#e6e6e6" style="font-family:Arial,serif; font-size:14px; font-weight: bold; border-bottom:1px solid #cccccc; color:#0a0a0a;">
                            <tr>
                                    <td width="6">&nbsp;</td>
                                    <td width="81" align="center" style="color:#000000;"><a href="http://www.myntra.com/men?utm_source=marketing&utm_medium=mailer&utm_campaign=welcome-coupon-06052013" target="_blank" style="color:#0a0a0a; text-decoration:none;">MEN</a></td>
                                    <td width="105" align="center"><a href="http://www.myntra.com/women?utm_source=marketing&utm_medium=mailer&utm_campaign=welcome-coupon-06052013" target="_blank" style="color:#0a0a0a; text-decoration:none;">WOMEN</a></td>
                                    <td width="84" align="center"><a href="http://www.myntra.com/kids-landing?utm_source=marketing&utm_medium=mailer&utm_campaign=welcome-coupon-06052013" target="_blank" style="color:#0a0a0a; text-decoration:none;">KIDS</a></td>
                                    <td width="108" align="center"><a href="http://www.myntra.com/brands?utm_source=marketing&utm_medium=mailer&utm_campaign=welcome-coupon-06052013" target="_blank" style="color:#0a0a0a; text-decoration:none;">BRANDS</a></td>
                                    <td width="86" align="center"><a href="http://www.myntra.com/sales?utm_source=marketing&utm_medium=mailer&utm_campaign=welcome-coupon-06052013" target="_blank" style="color:#0a0a0a; text-decoration:none;">SALE</a></td>
									<td width="104" align="center"><a href="http://www.myntra.com/100-days-of-summer?utm_source=marketing&utm_medium=mailer&utm_campaign=welcome-coupon-06052013" target="_blank" style="color:#0a0a0a; text-decoration:none;">TRENDS</a></td>
                                    <td width="6">&nbsp;</td>
                            </tr>
						</table>
						
					  
					  
						<table width="580" cellspacing="0" cellpadding="0" border="0" align="center"><tr><td height="5" style="font-size:5px;">&nbsp;</td></tr></table>
						
						
						<table width="580" border="0" cellspacing="0" cellpadding="0" align="center">
							<tr><td>
							<a href="http://www.myntra.com?utm_source=marketing&utm_medium=mailer&utm_campaign=welcome-coupon-06052013" target="_blank" style="text-decoration:none;">
								<div style="display:block; width:572px; height:222px; border:4px solid #f1f1f1;">
									<div style="display:block; width:572px; height:40px;">&nbsp;</div>
										<div style="display:block; width:572px; height:50px; font-size:50px; text-align:center;">
											<span style="color:#9f1f63;">W</span><span style="color:#f9a01b;">E</span><span style="color:#7aad37;">L</span><span style="color:#00b0df;">C</span><span style="color:#da1d89;">O</span><span style="color:#7aad37;">M</span><span style="color:#f47321;">E</span>
										</div>
										
										<div style="display:block; width:572px; font-size:18px; text-align:center;line-height:40px; color:#a2a4a6;">WE ARE PLEASED TO HAVE YOU AT MYNTRA.COM</div>
										
										<div style="display:block; width:572px; font-size:18px; font-style:italic; text-align:center;color:#ffffff;"><span style="width: 300px; background:#61aedd; height: 60px; line-height:60px;padding:10px 20px; ">India\'s Largest Online Fashion Store</span></div>
								</div>
							</a>
							</td></tr>
						</table>
						
						
						<table width="580" cellspacing="0" cellpadding="0" border="0" align="center"><tr><td height="10" style="font-size:5px;">&nbsp;</td></tr></table>
						
						
						<table width="580" border="0" cellspacing="0" cellpadding="0" align="center">
							<tr>
								<td width="235">
								
									<table width="235" border="4" cellspacing="0" cellpadding="10" align="center" style="border-collapse:collapse;">
										<tr><td style="border:4px solid #f1f1f1;">
											
											<table width="210" border="0" cellspacing="0" cellpadding="0" align="center">
												<tr><td height="10" style="font-size:2px;">&nbsp;</td></tr>
												<tr><td height="30" align="center">[COUPON_MSG]
												</td></tr>
												<tr><td height="40" valign="bottom" align="center"><a href="http://www.myntra.com?utm_source=marketing&utm_medium=mailer&utm_campaign=welcome-coupon-06052013" target="_blank" style="text-decoration:none;"><span style="background:#000000; font-size:12px; color:#ffffff; padding:5px 15px;border-radius:4px 4px 4px 4px">SHOP NOW</span></a></td></tr>
												<tr><td height="10" style="font-size:2px;">&nbsp;</td></tr>
											</table>
											
										</td></tr>
										<tr><td style="border:4px solid #f1f1f1;">
										
											<table width="210" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial,serif;font-size:12px; line-height:17px; color:#333333;">
												<tr>
													<td height="60"  valign="bottom"><b style="font-size:18px; color:#333333;">You got the following Coupon:<br/><br/></td>
												</tr>
												
												
												[COUPON_DETAILS]
												
											</table>
											
											
											<table width="210" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial,serif;font-size:12px; line-height:17px; color:#333333;">
												<tr>
													<td width="25" valign="top"><img src="http://myntramailer.s3.amazonaws.com/april2013/1366805548bullet.jpg" width="20" height="20" border="0" alt="" title="" style="border: none; display:block;"/></td>
													<td height="45"  valign="top">To access your Coupons go to <a href="http://www.myntra.com/mymyntra.php?view=mymyntcredits&?utm_source=marketing&utm_medium=mailer&utm_campaign=welcome-coupon-06052013" target="_blank" style="text-decoration:none; color:#f47321;">Your Myntra Account.</a></td>
												</tr>
												<tr>
													<td width="25" valign="top"><img src="http://myntramailer.s3.amazonaws.com/april2013/1366805548bullet.jpg" width="20" height="20" border="0" alt="" title="" style="border: none; display:block;"/></td>
													<td>Coupons are valid only for the next 15 Days.</td>
												</tr>
											</table>
										
										</td></tr>
									</table>
									
								</td>
								
								<td width="15">&nbsp;</td>
								
								<td width="325" valign="top">
								
										<table width="325" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial,serif;font-size:16px; line-height:20px; color:#333333;">
											<tr><td>
											
												Dear Customer,<br/><br/>
												At Myntra, we offer the latest fashion from a range of 500+ global brands.<br/><br/>
												We aim to provide you with an easy, convenient and hassle free shopping experience with:

											
											</td></tr>
											<tr><td height="20">&nbsp;</td></tr>
											<tr><td>
												
												<table width="325" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial,serif;font-size:16px; font-weight:bold; color:#333333;">
												<tr>
													<td width="25" valign="top"><img src="http://myntramailer.s3.amazonaws.com/april2013/1366805548bullet.jpg" width="20" height="20" border="0" alt="" title="" style="border: none; display:block;"/></td>
													<td height="25"  valign="top">Free Shipping Over Rs. [SHIPPING_THRESHOLD]</td>
												</tr>
												<tr>
													<td width="25" valign="top"><img src="http://myntramailer.s3.amazonaws.com/april2013/1366805548bullet.jpg" width="20" height="20" border="0" alt="" title="" style="border: none; display:block;"/></td>
													<td height="25"  valign="top">Cash On Delivery</td>
												</tr>
												<tr>
													<td width="25" valign="top"><img src="http://myntramailer.s3.amazonaws.com/april2013/1366805548bullet.jpg" width="20" height="20" border="0" alt="" title="" style="border: none; display:block;"/></td>
													<td height="25"  valign="top">Free 30-Day Returns</td>
												</tr>
												<tr>
													<td width="30" valign="top"><img src="http://myntramailer.s3.amazonaws.com/april2013/1366805548bullet.jpg" width="20" height="20" border="0" alt="" title="" style="border: none; display:block;"/></td>
													<td>24-Hour Dispatch</td>
												</tr>
												</table>
											
											</td></tr>
											<tr><td height="50">&nbsp;</td></tr>
											<tr><td style="font-family:Arial,serif;font-size:13px; font-style:italic; line-height:17px; color:#333333;">
											
												We look forward to seeing you. <br/>
												Warm regards, <br/>
												The Myntra Team<br/>
												support@myntra.com <br/>
												+91-80-43541999
											
											</td></tr>
										</table>
								
								</td>
							</tr>
						</table>
						
						
						
						
						
						
						
						<table width="580" cellspacing="0" cellpadding="0" border="0" align="center"><tr><td height="10" style="font-size:5px;">&nbsp;</td></tr></table>
					  
					  
					  
						
					
				  </td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>

<div width="100%" align="center" style="font-family:Arial, Helvetica, sans-serif">
	<table width="620" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial,serif;font-size:10px; color:#818181;">
		<tr><td valign="top"><img src="http://myntramailer.s3.amazonaws.com/march2012/1332137601border-bottom.jpg" alt="" title="" width="620" height="25"/></td></tr>
		<tr><td align="center"><a href="mailto:support@myntra.com" style="font-size: 11px;text-decoration:none;color:#3e9dc5;" target="_blank">support@myntra.com</a> | Call +91-80-43541999</td></tr>
		<tr><td align="center" height="35">
			Connect with us &nbsp; &nbsp;
			<a href="http://www.facebook.com/myntra" style=" outline:none" target="_blank"><img src="http://myntramailer.s3.amazonaws.com/february2012/1329296349facebook.jpg" width="17" height="17" border="none" style=""></a>
			<a href="http://www.twitter.com/myntra" style="outline:none;padding-left: 1px;" target="_blank"><img src="http://myntramailer.s3.amazonaws.com/february2012/1329296350twitter.jpg" width="17" height="17" border="none"></a>
		</td></tr>
		<tr><td align="center">
			Offer valid on Myntra.com only. Myntra logo is a registered trademark of 
			<a href="http://www.myntra.com?utm_source=marketing&utm_medium=mailer&utm_campaign=welcome-coupon-06052013" style="text-decoration: none;color:#3e9dc5;" target="_blank">www.myntra.com</a>, India.<br>
			Check out our <a href="http://www.myntra.com/privacy_policy.php?utm_source=marketing&utm_medium=mailer&utm_campaign=welcome-coupon-06052013" style="text-decoration: none;color:#3e9dc5;" target="_blank">Privacy Policy</a> &copy; 2013. All Right Reserved. <a href="http://www.myntra.com?utm_source=marketing&utm_medium=mailer&utm_campaign=welcome-coupon-06052013" style="text-decoration: none;color:#3e9dc5;" target="_blank">www.myntra.com</a>
		</td></tr>
	</table>
</div>' 
WHERE name in ('mrp_first_login', 'mrp_fbfirst_login');