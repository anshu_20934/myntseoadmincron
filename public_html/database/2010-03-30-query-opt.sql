 alter table xcart_products add quantity_sold int;
 

 alter table mk_product_tag_map add column tagname varchar(255) after tagid;
 alter table mk_product_tag_map add column is_valid int not null default 0 ;
 alter table mk_product_tag_map add column products_available int not null default 0 ;

 alter table mk_designer add column average_rating float;
 alter table mk_designer add column approved_products int;

update mk_designer d , (
select item_id,  AVG(v.rating_value) as `avg` from mk_votes v group by item_id
)  r
set
d. average_rating = r.avg where d.id = r.item_id;

update mk_designer d , (
select d.id, count(*) as count from mk_designer d , xcart_products p where p.designer = d.id and p.statusid = 1008 group by d.id
)  r
set
d. approved_products = r.count where d.id = r.id;


 alter table mk_shop add column average_rating float;
 alter table mk_shop add column approved_products int;


 update mk_shop s,
(select mk_shop_votes.shopname, AVG(mk_shop_votes.rating_value) as avg_rating from mk_shop_votes group by mk_shop_votes.shopname) v
set s.average_rating = v.avg_rating where s.name = v.shopname;

update mk_shop s,
(SELECT shopid, count(mk_shop_product.productid) as products FROM mk_shop_product
                                                INNER JOIN xcart_products
                                                ON xcart_products.productid = mk_shop_product.productid where statusid = 1008 group by shopid) t
set s.approved_products = t.products where t.shopid = s.shopid;

