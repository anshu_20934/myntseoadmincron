use myntra;

CREATE TABLE `imint_m2i_orders` (              
`orderid` int(11) NOT NULL auto_increment,
`promocode` varchar(200) not null,
`productid` int(11) not NULL,                       
`productname` varchar(200) not NULL,
`productprice` decimal(10,2) not NULL,                          
`email` varchar(250) not null,
`firstname` varchar(100) default null,                      
`lastname` varchar(100) default null,
`shipaddress` text default null,
`country` varchar(150) default null,
`state` varchar(150) default null,
`city` varchar(150) default null,
`phone` varchar(150) default null,
`mobile` varchar(150) default null,
`orderdate` int(11) not NULL,                       
PRIMARY KEY  (`orderid`),
INDEX(`orderid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

alter table imint_m2i_orders add column digital enum('Y','N') default 'N' after mobile;
alter table imint_m2i_orders add column zipcode varchar(15) after city;