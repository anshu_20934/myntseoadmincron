use myntra;

/*1*/
update mk_product_group set title='Personalized Bags, Backpacks, Travel Bags, Laptop Bags, College Bags and School Bags',description='Myntra redefines space and comfort with its wide array of personalized bags and backpacks. A fabulous and trendy collection of bags that includes personalized laptop bags, travel bags, school bags, gym bags and more. Made from high-quality material, our personalized backpacks are tough and extremely convenient to carry. You can personalize your bag with name or text embroidered.',is_active='Y' where id='13';

/*2*/
insert into `mk_text_font_color` (color) values('#839e7a');

/*3*/
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='141',textColorDefault='141' where id=1600;
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='1',textColorDefault='1' where id=1605;

/*4*/
update mk_product_type set type_h1='Laptop Bags and Backpacks' where id='224';
