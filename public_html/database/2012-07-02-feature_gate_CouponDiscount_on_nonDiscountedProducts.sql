
--
-- Database: `myntra`
--

-- --------------------------------------------------------

--
-- Table structure for table `mk_feature_gate_key_value_pairs`
--

INSERT INTO `mk_feature_gate_key_value_pairs` (`id`, `key`, `value`, `description`) VALUES
(239, 'condNoDiscountCoupon', 'true', 'true : to apply coupon on only non-discounted products ');
