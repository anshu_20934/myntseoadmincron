--
-- Table structure for table `cashback_account`
--

CREATE TABLE IF NOT EXISTS `cashback_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(128) NOT NULL,
  `account_type` smallint(2) NOT NULL,
  `balance` float NOT NULL,
  `cashback_coupon` varchar(50) DEFAULT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `version` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`,`account_type`),
  KEY `account_type` (`account_type`),
  KEY `login_2` (`login`)
) ;




--
-- Table structure for table `cashback_transaction_log`
--

CREATE TABLE IF NOT EXISTS `cashback_transaction_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cashback_account_id` int(11) NOT NULL,
  `item_type` varchar(20) NOT NULL,
  `item_id` int(20) NOT NULL DEFAULT '0',
  `business_process` varchar(20) NOT NULL,
  `credit_inflow` decimal(12,2) NOT NULL DEFAULT '0.00',
  `credit_outflow` decimal(12,2) NOT NULL DEFAULT '0.00',
  `balance` decimal(12,2) NOT NULL DEFAULT '0.00',
  `modified_by` varchar(128) NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `descripion` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `business_process` (`business_process`),
  KEY `coupon_account_id` (`cashback_account_id`),
  KEY `cashback_account_id` (`cashback_account_id`)
);




--
-- Database: `myntra`
--

-- --------------------------------------------------------

--
-- Table structure for table `cashback_business_process_lookup`
--

CREATE TABLE IF NOT EXISTS `cashback_business_process_lookup` (
  `business_process` varchar(30) NOT NULL,
  `Code` varchar(20) NOT NULL,
  `transaction_type` varchar(10) NOT NULL,
  `Bucket` varchar(20) NOT NULL,
  `Action` text NOT NULL,
  KEY `Code` (`Code`)
);




--
-- Table structure for table `cashback_account_type`
--

CREATE TABLE IF NOT EXISTS `cashback_account_type` (
  `id` smallint(2) NOT NULL,
  `name` varchar(40) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
);


ALTER TABLE  `xcart_order_details` ADD  `store_credit_usage` DECIMAL( 12, 2 ) NOT NULL AFTER  `cash_redeemed` ,
ADD  `earned_credit_usage` DECIMAL( 12, 2 ) NOT NULL DEFAULT 0.0 AFTER  `store_credit_usage`;

ALTER TABLE  `xcart_orders` ADD  `store_credit_usage` DECIMAL( 12, 2 ) NOT NULL AFTER  `cash_redeemed` ,
ADD  `earned_credit_usage` DECIMAL( 12, 2 ) NOT NULL DEFAULT 0.0 AFTER  `store_credit_usage`;

ALTER TABLE  `gift_cards_orders` ADD  `store_credit_usage` DECIMAL( 12, 2 ) NOT NULL DEFAULT 0.0 AFTER  `cash_redeemed` ,
ADD  `earned_credit_usage` DECIMAL( 12, 2 ) NOT NULL DEFAULT 0.0 AFTER  `store_credit_usage`;


--
-- Dumping data for table `cashback_business_process_lookup`
--

INSERT INTO `cashback_business_process_lookup` (`business_process`, `Code`, `transaction_type`, `Bucket`, `Action`) VALUES
('Earned Credit', 'EARNED_CREDIT', 'Credit', 'Earned Credit', 'When customer purchases an item at MRP. Online purchase receives it directly. For COD purchases credit is made 30 days hence'),
('Full Cancellation', 'FULL_CANCELLATION', 'Credit', 'Store Credit', 'The value of the products, split item-wise is stored as separate line items'),
('Partial Cancellation', 'PARTIAL_CANCELLATION', 'Credit', 'Store Credit', 'The value of the products, split item-wise is stored as separate line items'),
('Return', 'RETURN_PRODUCT', 'Credit', 'Store Credit', 'The value of the product returned is stored item-wise as separate line items\r\n'),
('Return Shipping', 'RETURN_SHIPPING', 'Credit', 'Earned Credit', 'The value of the shipping is credited to the customer for each item returned (Currently Rs. 100)'),
('RTO', 'RTO', 'Credit', 'Store Credit', 'For all online orders, the value paid by the customer is credited to the customer''s account'),
('Refund', 'REFUND', 'Debit', 'Store Credit', 'The amount debited from the Cashback Account and paid to the customer as a bank account refund'),
('Usage', 'CASHBACK_USED', 'Debit', 'Store/Earned Credit', 'When the customer uses a cashback from his account, the code creates a number for that transaction and increments it for each transaction\r\n'),
('Return of Usage  ', 'CASHBACK_RETURN', 'Credit', 'Store/Earned Credit', 'When the customer has used a cashback for a transaction and the transaction is cancelled, returned or RTO-ed, the cashback used for the transaction is refunded back to the same bucket as the original transaction ID code'),
('Cashback Reversal ', 'CASHBACK_REVERSAL', 'Debit', 'Earned Credit', 'reversal of earned credit, in the cae of online payment failure. '),
('Mrp mismatch debit', 'MRP_MISMATCH', 'Debit', 'Store Credit', 'debiting the mrp mismatch cashback credit, that was credited in case of mrp mismatch with actual product pricetag and cataloge price. Debiting is done when there is a order cancelation in this case.'),
('giftcard', 'GIFT_CARD', 'Credit', 'Store Credit', 'crediting gift card value into users cashback account. Store credit.'),
('GOOD_WILL', 'GOOD_WILL', 'Credit', 'Earned Credit', 'Credit given to users through manual process on admin interface.'),
('Dispute on delivery', 'DELIVERY_DISPUTE_REFUND', 'CREDIT', 'Earned Credit', 'Dispute on delivery'),
('Courier asking for extra money', 'INVOICE_MISMATCH', 'CREDIT', 'Earned Credit', 'Courier asking for extra money / Invoice mismatch'),
('Missing items / articles in pa', 'MISSING_ITEM', 'CREDIT', 'Earned Credit', 'Missing items / articles in packet'),
('Product mismatch', 'PRODUCT_MISMATCH', 'CREDIT', 'Earned Credit', 'Product mismatch'),
('Self-ship delivery dispute', 'SELF_SHIP_DELIVERY_DISPUTE', 'CREDIT', 'Earned Credit', 'Self-ship delivery dispute'),
('Extra courier charges refund', 'EXTRA_COURIER_CHARGES_REFUND', 'CREDIT', 'Earned Credit', 'Extra courier charges refund');


--
-- Dumping data for table `cashback_account_type`
--

INSERT INTO `cashback_account_type` (`id`, `name`, `description`) VALUES
(1, 'earned credit', 'cash back given as part of good will'),
(2, 'store credit', ' cash back given at the time of return or cancellation of order.');

INSERT INTO `mk_widget_key_value_pairs` ( `key`, `value`, `description`) VALUES
( 'myntCashAdminSpecialUsers', 'rgv,test,admin', 'comma separated list of cashback admin special users with out any inflow limit.    '),
( 'myntCashAdminInflowLimits', '100,1000,5000', 'comma seperated list of credit limits for cashback in flown through admin for each admin user. format : day-limit,week-limit,month-limit,    '),
('myntCashAdminInflowLimitsPerCustomer', '10,1000,5000', 'comma seperated list of credit limits for cashback in flown through admin for each customer . format : day-limit,week-limit,month-limit,     ');


INSERT INTO `myntra`.`mk_order_action_reasons` ( `action`, `reason`, `reason_display_name`, `email_contents`, `parent_reason`, `is_internal_reason`) VALUES ( 'oh_disposition', 'MCNA', 'cashback/myntCash not available in customers account', NULL, NULL, '1');

INSERT INTO `myntra`.`mk_order_action_reasons` ( `action`, `reason`, `reason_display_name`, `email_contents`, `parent_reason`, `is_internal_reason`) VALUES ( 'oh_disposition', 'MCSD', 'cashback/myntCash service unavailable at the time of order placement.', NULL, NULL, '1');


INSERT INTO `cancellation_codes` ( `cancel_type`, `cancel_type_desc`, `cancel_reason`, `cancel_reason_desc`, `email_content`, `last_updated_time`, `cancellation_mode`, `is_active`) VALUES
('CCC', 'system cancelation', 'MCFAIL', 'cashback usage failure', 'We regret you to inform that, there is no enough cashback in your Mynt Credits account at the , hence we are cancelling the order.', NULL, 'FULL_CANCELLATION', 1),
('CCC', 'CC Cancellation', 'MCFAIL', 'cashback usage failure', 'We regret you to inform that, there is no enough cashback in your Mynt Credits account at the , hence we are cancelling the order.', NULL, 'FULL_ORDER_CANCELLATION', 1);

INSERT INTO `order_oh_reasons_master` ( `reason_code`, `reason_desc`, `is_manual_change`, `last_updated_time`) VALUES
( 'MCSD', 'cashback debit failure : service down', 1, NULL),
( 'MCNA', 'cashback debit failure : balance not sufficent', 1, NULL);


ALTER TABLE `mk_cashback_log`
    ADD id INT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE;

