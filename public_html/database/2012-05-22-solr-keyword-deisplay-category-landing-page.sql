
alter table landing_page
add category varchar(100) ;

INSERT INTO `myntra`.`mk_feature_gate_key_value_pairs` (`key`, `value`, `description`) VALUES ('categoryLandingPage', '{\"brands_filter_facet\":\"Brand\",\"global_attr_article_type_facet\":\"Article Type\",\"global_attr_base_colour\":\"Colour\",\"global_attr_gender\":\"Gender\"}', 'json of solr search keyword mapped to String name');



INSERT INTO `myntra`.`mk_feature_gate_key_value_pairs` (`key`, `value`, `description`) VALUES ('categoryLandingOrder', '{\"Brand\":5,\"Article Type\":4,\"Colour\":3,\"Gender\":2}', 'json of solr search keyword string name for autosuggest  mapped to order');

