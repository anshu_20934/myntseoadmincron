alter table mk_style_properties add column tex_color_image varchar(255) after default_image;


create table style_groups ( id int(11) not null auto_increment,style_id int(11),group_id int(11),group_type varchar(45),pattern_name varchar(245), primary key (id),
key style_id_fk (style_id),KEY group_id_ck (group_id),key style_id_group_type_ck (style_id,group_type), key group_type_ck(group_type),key  pattern_name_ck(pattern_name));
