delete from mk_solr_sprod_priority where name like 'global_attr%';
insert into mk_solr_sprod_priority (name,priority,is_active,label,display_order) values ('global_attr_article_type',10,1,'Article Type Filter',0);
insert into mk_solr_sprod_priority (name,priority,is_active,label,display_order) values ('display_category',10,1,'Display Category Filter',0);
insert into mk_solr_sprod_priority (name,priority,is_active,label,display_order) values ('global_attr_master_category',10,1,'Master Category Filter',0);
insert into mk_solr_sprod_priority (name,priority,is_active,label,display_order) values ('global_attr_sub_category',10,1,'Sub Category Filter',0);

insert into mk_solr_sprod_priority (name,priority,is_active,label,display_order) values ('global_attr_gender',15,1,'Gender Filter',0);
insert into mk_solr_sprod_priority (name,priority,is_active,label,display_order) values ('global_attr_age_group',10,1,'Gender Filter',0);
insert into mk_solr_sprod_priority (name,priority,is_active,label,display_order, do_phonetic_search) values ('global_attr_brand',15,1,'Brand Filter',0, 1);
insert into mk_solr_sprod_priority (name,priority,is_active,label,display_order) values ('global_attr_base_colour',5,1,'Colour Filter',0);

update mk_solr_sprod_priority set is_active=0 where name in ('categories_filter','product','brands_filter','sports_filter','fit_filter','keywords');


