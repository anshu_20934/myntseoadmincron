CREATE TABLE `mk_tracking_history` (
  `id` int auto_increment NOT NULL,
  `utm_source` varchar(250) NOT NULL,  
  `utm_medium` varchar(250) NULL,  
  `event_type` varchar(50) NOT NULL,
  `event_value` varchar(100) NULL,
  PRIMARY KEY (`id`),
  KEY(`utm_medium`),key(`event_type`),key(`utm_source`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
alter table mk_tracking_history add column track_time int not null;