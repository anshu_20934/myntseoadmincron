CREATE TABLE `mk_widget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_name` varchar(50) NOT NULL,
  `widget_type` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `mk_widget_sku_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_id` int(11) NOT NULL,
  `sku` varchar(20) NOT NULL,
  `display_order` int(11),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `mk_widget_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_id` int(11) NOT NULL,
  `tag` varchar(50) NOT NULL,
  `limit` int(11) DEFAULT 5,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `mk_widget_top_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11),
  `display_order` int(11),
  `link_name` varchar(50) NOT NULL,
  `link_url` varchar(100),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `mk_widget_most_popular` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link_name` varchar(50),
  `link_url` varchar(100),
  `is_permanent` tinyint(1) default 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `mk_widget_page_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_id` int(11),
  `page_name` varchar(50),
  `display_order` int(11),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `mk_widget_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(100),
  `url` varchar(100),
  `anchor` varchar(100),
  `alt` varchar(100),
  `display_order` int(11),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
