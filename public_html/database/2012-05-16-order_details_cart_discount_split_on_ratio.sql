ALTER TABLE xcart_orders ADD COLUMN `cart_discount` DECIMAL(12,2) NOT NULL DEFAULT '0.00'  AFTER `discount` ;
ALTER TABLE xcart_order_details CHANGE COLUMN `cart_discount_split_on_ratio` `cart_discount_split_on_ratio` DECIMAL(12,2) NOT NULL DEFAULT '0.00'  ;
UPDATE xcart_order_details SET cart_discount_split_on_ratio=0;
