-- Add new columns to coupons table.
ALTER TABLE `xcart_discount_coupons`
  ADD COLUMN `showInMyMyntra` TINYINT(1) DEFAULT 1,
  ADD COLUMN `description` VARCHAR(1024),
  ADD COLUMN `lastEdited` INT(11) NOT NULL,
  ADD COLUMN `lastEditedBy` VARCHAR(32) DEFAULT NULL;

-- Add new column to groups table.
ALTER TABLE `mk_coupon_group`
  ADD COLUMN `lastEdited` INT(11),
  ADD COLUMN `lastEditedBy` VARCHAR(32) DEFAULT NULL,
  ADD COLUMN `active_count` INT(20) DEFAULT 0;

-- Fill data in `lastEdited` columns.
UPDATE `xcart_discount_coupons`
   SET lastEdited = timeCreated;

UPDATE `mk_coupon_group`
   SET lastEdited = timeCreated;

-- Fill data in active_count.
UPDATE mk_coupon_group a 
   SET a.active_count = (SELECT count(*) FROM xcart_discount_coupons b WHERE b.groupname = a.groupname AND b.status='A');

-- Description field population.

