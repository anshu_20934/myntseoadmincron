/*1.insert new fonts(if required)*/
insert into mk_fonts (name,image_name,ttf_name,display_order) values ('times_italic','times_italic.png','times_italic.ttf','1');

/*2.insert into mk_text_font_detail*/
update mk_text_font_detail set font_id=(select id from mk_fonts where name='times_italic') where orientation_id in (1691,1692,1693,1694,1695,1696,1697,1699,1700,1800,1801,1802);
