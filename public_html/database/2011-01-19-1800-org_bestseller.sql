alter table mk_org_style_map add column
(
	`bestseller` varchar(1) NOT NULL DEFAULT 'N',
	`closeout` varchar(1) NOT NULL DEFAULT 'N',
	`whatsnew` varchar(1) NOT NULL DEFAULT 'N'
);

alter table mk_org_products_map add column
(
	`bestseller` varchar(1) NOT NULL DEFAULT 'N',
	`closeout` varchar(1) NOT NULL DEFAULT 'N',
	`whatsnew` varchar(1) NOT NULL DEFAULT 'N'
)