INSERT INTO page_config_image_rule (location_type,section,width,height,min,max,step,size,require_page_location) VALUES ("new-home-features","slideshow",980,480,1,10,1,512000,false);
INSERT INTO page_config_image_rule (location_type,section,width,height,min,max,step,size,require_page_location) VALUES ("new-home-features","static",320,490,1,2,1,256000,false);

INSERT INTO page_config_image_rule (location_type,section,width,height,min,max,step,size,require_page_location) VALUES ("new-home-subbanners","slideshow",320,240,4,8,4,128000,false);
INSERT INTO page_config_image_rule (location_type,section,width,height,min,max,step,size,require_page_location) VALUES ("new-home-subbanners","static",320,240,3,15,3,128000,false);
