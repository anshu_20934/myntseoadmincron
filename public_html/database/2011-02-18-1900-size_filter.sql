insert into mk_filter_group (group_name, group_label,display_order,is_active) values ('chest_size','chest_size',8,1);
insert into mk_filter_group (group_name, group_label,display_order,is_active) values ('shoe_size','shoe_size',8,1);
insert into mk_filter_group (group_name, group_label,display_order,is_active) values ('waist_size','waist_size',8,1);

insert into mk_filters(filter_name, filter_group_id) values ('XXS', (select id from mk_filter_group where group_name = 'chest_size'));
insert into mk_filters(filter_name, filter_group_id) values ('XS', (select id from mk_filter_group where group_name = 'chest_size'));
insert into mk_filters(filter_name, filter_group_id) values ('S', (select id from mk_filter_group where group_name = 'chest_size'));
insert into mk_filters(filter_name, filter_group_id) values ('M', (select id from mk_filter_group where group_name = 'chest_size'));
insert into mk_filters(filter_name, filter_group_id) values ('L', (select id from mk_filter_group where group_name = 'chest_size'));
insert into mk_filters(filter_name, filter_group_id) values ('XL', (select id from mk_filter_group where group_name = 'chest_size'));
insert into mk_filters(filter_name, filter_group_id) values ('XXL', (select id from mk_filter_group where group_name = 'chest_size'));
insert into mk_filters(filter_name, filter_group_id) values ('XXXL', (select id from mk_filter_group where group_name = 'chest_size'));


insert into mk_filters(filter_name, filter_group_id) values ('4', (select id from mk_filter_group where group_name = 'shoe_size'));
insert into mk_filters(filter_name, filter_group_id) values ('5', (select id from mk_filter_group where group_name = 'shoe_size'));
insert into mk_filters(filter_name, filter_group_id) values ('6', (select id from mk_filter_group where group_name = 'shoe_size'));
insert into mk_filters(filter_name, filter_group_id) values ('7', (select id from mk_filter_group where group_name = 'shoe_size'));
insert into mk_filters(filter_name, filter_group_id) values ('8', (select id from mk_filter_group where group_name = 'shoe_size'));
insert into mk_filters(filter_name, filter_group_id) values ('9', (select id from mk_filter_group where group_name = 'shoe_size'));
insert into mk_filters(filter_name, filter_group_id) values ('10', (select id from mk_filter_group where group_name = 'shoe_size'));
insert into mk_filters(filter_name, filter_group_id) values ('11', (select id from mk_filter_group where group_name = 'shoe_size'));
insert into mk_filters(filter_name, filter_group_id) values ('12', (select id from mk_filter_group where group_name = 'shoe_size'));

insert into mk_filters(filter_name, filter_group_id) values ('24', (select id from mk_filter_group where group_name = 'waist_size'));
insert into mk_filters(filter_name, filter_group_id) values ('26', (select id from mk_filter_group where group_name = 'waist_size'));
insert into mk_filters(filter_name, filter_group_id) values ('28', (select id from mk_filter_group where group_name = 'waist_size'));
insert into mk_filters(filter_name, filter_group_id) values ('30', (select id from mk_filter_group where group_name = 'waist_size'));
insert into mk_filters(filter_name, filter_group_id) values ('32', (select id from mk_filter_group where group_name = 'waist_size'));
insert into mk_filters(filter_name, filter_group_id) values ('34', (select id from mk_filter_group where group_name = 'waist_size'));
insert into mk_filters(filter_name, filter_group_id) values ('36', (select id from mk_filter_group where group_name = 'waist_size'));
insert into mk_filters(filter_name, filter_group_id) values ('38', (select id from mk_filter_group where group_name = 'waist_size'));
insert into mk_filters(filter_name, filter_group_id) values ('40', (select id from mk_filter_group where group_name = 'waist_size'));
insert into mk_filters(filter_name, filter_group_id) values ('42', (select id from mk_filter_group where group_name = 'waist_size'));
