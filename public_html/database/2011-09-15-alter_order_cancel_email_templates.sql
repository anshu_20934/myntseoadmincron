update mk_email_notification set body = 'Hello [USER], <br/><br/>
[REASON_TEXT] <br><br>
Please find the details of your Order No. [ORDER_ID] below:<br/><br/>
[ORDER_DETAILS]
<br/><br/>
<p>[GOODWILL_COUPON_MSG] [REFUND_MSG] [COUPON_USAGE_TEXT]</p>
<p>
We want you to know we are honoured to have you as our esteemed customer and we will do our best to meet your needs. Your continued patronage and suggestions are a vital part of our growth, and for that we are most grateful to you.
</p>
<br/>
<p>For any clarification, kindly contact us at [MYNTRA_CC_PHONE].</p><br>
Regards,<br/>
Team Myntra' where name = 'order_cancel';

update mk_email_notification set body = 'Hello [USER], <br/><br/>
[REASON_TEXT] <br><br>
Please find the details of your Order No. [ORDER_ID] below:<br/><br/>
[ORDER_DETAILS]
<br/><br/>
<p>[GOODWILL_COUPON_MSG] [REFUND_MSG] [COUPON_USAGE_TEXT]</p>
<p>You, our customer, are our focus and your experience takes precedence over everything else. It is the key principle of Myntra.com and we would like to reassure you that we would continue to remain invested in offering the best shopping experience for Fashion and Lifestyle products. We hope that you continue to shop with us and treat this as an aberration to the quality standards we strive to achieve.
</p>
<br/>
<p>For any clarification, kindly contact us at [MYNTRA_CC_PHONE].</p><br>
Regards,<br>
Team Myntra' where name = 'order_cancel_oos';

insert into mk_email_notification (name, body) values ('order_cancel_gw_coupon', 'We would like to make it up by issuing a special coupon code(s) <b>[COUPON_CODES]</b>  exclusively for you to redeem any product on Myntra.com for Rs <b>[COUPON_AMOUNT]</b> absolutely <b>free</b>. Kindly consider this product and voucher as a token of our appreciation for shopping at Myntra.com. We are glad that you shopped with us and truly wish that this experience hasn\'t upset you. This coupon will be valid till <b>[EXPIRY_DATE]</b>.'); 
