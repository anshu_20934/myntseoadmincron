update mk_email_notification set body='<div style="border: 1px solid gray; padding: 6px">
Hi [USER], <br/>
<p>We hope that you are delighted with the product(s) for the Order No. [ORDER_ID] which we delivered to you on [DELIVERY_DATE].</p>
As a constant endeavour to improve our services, we would request you to share with us some feedback on your shopping experience with Myntra.<br/><br/>
As a simple gesture of thanks, we will e-mail you a coupon as soon as you share your feedback.<br/><br/>
[ORDER_MFB_FORM]<br/>[REDIRECT_INFO]<br/>
<p>For any further clarification, kindly contact us at [MYNTRA_CC_PHONE].</p>

Regards,<br/>
Myntra Customer Connect<br/>
India''s Largest Online Fashion Store
<br/><br/>[DECLARATION]<br/>
</div>' where name='voc_order';

update mk_email_notification set body='Dear [USER],<br><p>Thanks for providing your valuable feedback.</p><br><p>As a token of appreciation here is your discount coupon code <b>[COUPON_CODE]</b>. Please use the coupon on your next purchase at myntra.</p><p>This coupon is valid for a month from now.</p><p>Enjoy even more shopping at Myntra!</p><br><p>-Team Myntra</p>' where name='sendfeedbackcoupon';

insert into mk_feature_gate_key_value_pairs (`key`,value,description) values('MFB.Order.Transition','true','true:will record the transitional order feedback instance and conditionally sends the coupon , false:sends the new coupon to all order feedback instances');