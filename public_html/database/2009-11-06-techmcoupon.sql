use myntra;

update mk_email_notification set body='
Dear [USER],<br>
<p>Your Techmahindra Brand Shop account has been created. You can access your account using following login details: </p>
<br>
<p>Brand Shop URL : www.myntra.com/techmahindra </p>
<p>login: [LOGIN]</p>
<p>Password: [PASSWORD]</p>

<p>At this store, you can purchase your company merchandize like t-shirt, mugs, caps, key-chains and many other products.
You can create your own design by adding photo/your company logo/text on any product. <br/>
You can also order in bulk for your team/group for different team events. If you have a specific requirement, you
can get in touch with us at sales@myntra.com and we will do our best to work out right solution for you at pre-approved
prices for Techmahindra.</p>

<p>For any help or questions regarding your account or products, you can get in touch with us at techsupport@myntra.com</p>

<p>Yours Sincerely<br/>
Myntra Customer Service<br/>
www.myntra.com<br/>
</p>
' where name='orgtechmregistration';
