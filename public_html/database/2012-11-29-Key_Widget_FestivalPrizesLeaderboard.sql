INSERT INTO `myntra`.`mk_widget_key_value_pairs`
(`key`,
`value`,
`description`)
VALUES
(
'shoppingfest_dailyPrizes',
'',
'7 images to show as daily prizes'
);

INSERT INTO `myntra`.`mk_widget_key_value_pairs`
(`key`,
`value`,
`description`)
VALUES
(
'shoppingfest_assuredandbumperprizes',
'',
'first two as assured and the third for bumper prize'
);

INSERT INTO `myntra`.`mk_widget_key_value_pairs`
(`key`,
`value`,
`description`)
VALUES
(
'leaderBoardId',
'',
'set the leaderboard id to 2 for enabling leader board, if blank then leaderboard is disabled'
);


