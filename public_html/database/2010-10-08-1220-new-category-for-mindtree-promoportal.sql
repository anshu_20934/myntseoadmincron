/*add new category*/
insert into mk_voucher_issuer_category (issuercode,categoryid,category_name,active,browse,customize) values('5013','01',null,'Y','Y','N');

/*add styles to category*/
insert into mk_issuer_category_style_map (issuercode,categoryid,styleid,active) values('5013','01','1334','Y');
insert into mk_issuer_category_style_map (issuercode,categoryid,styleid,active) values('5013','01','1335','Y');
insert into mk_issuer_category_style_map (issuercode,categoryid,styleid,active) values('5013','01','1336','Y');
insert into mk_issuer_category_style_map (issuercode,categoryid,styleid,active) values('5013','01','1337','Y');
insert into mk_issuer_category_style_map (issuercode,categoryid,styleid,active) values('5013','01','1338','Y');
insert into mk_issuer_category_style_map (issuercode,categoryid,styleid,active) values('5013','01','1339','Y');

/*add products to category*/
insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5014','01','1280310202','Y');
insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5014','01','1280611886','Y');
insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5014','01','1282089400','Y');
insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5014','01','1282525793','Y');
insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5014','01','1284821122','Y');
insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5014','01','1286204531','Y');

/*a demo coupon(can be used multiple times) for the category products(for all styles of a product type)*/
insert into xcart_discount_coupons (coupon,groupid,discount,coupon_type,minimum,times,per_user,times_used,expire,status,provider,producttypeid) values ('5013011234567890','0','100.00','percent','0.00','100','N','0',unix_timestamp(now())+86400*365,'A','myntraProvider','261');

