-- Table to log the transactions on the cashback coupon.
CREATE TABLE `mk_cashback_log` (
  `login` VARCHAR(128) NOT NULL,
  `coupon` VARCHAR(50) NOT NULL,
  `orderid` INT(20),
  `subtotal` DECIMAL(12, 2),
  `date` INT(11),
  `txn_type` VARCHAR(1),
  `amount` DECIMAL(12, 2),
  `balance` DECIMAL(12, 2));
