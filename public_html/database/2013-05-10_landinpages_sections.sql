DROP TABLE IF EXISTS landingpage_product_section;
DROP TABLE IF EXISTS landingpage_metadata;

CREATE  TABLE IF NOT EXISTS `landingpage_metadata` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NULL ,
  `description` TEXT NULL ,
  `image` VARCHAR(255) NULL ,
  `lp_name` VARCHAR(255) NULL ,
  PRIMARY KEY (`id`),
  UNIQUE (`lp_name`) )
;

CREATE  TABLE IF NOT EXISTS `landingpage_product_section` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `lp_id` INT NOT NULL ,
  `title` VARCHAR(255) NOT NULL ,
  `target_url` VARCHAR(255) NOT NULL ,
  `url` VARCHAR(255) NOT NULL ,
  `description` TEXT NULL ,
  `no_of_products` INT NOT NULL ,
  `is_carousel` TINYINT(1) NULL DEFAULT 0 ,
  `sort_order` INT NOT NULL DEFAULT 0 ,
  `created_by` VARCHAR(255) NULL ,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `last_modified_on` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_landingpage_metadat_product_section` (`lp_id` ASC) ,
  CONSTRAINT `fk_landingpage_metadat_product_section`
    FOREIGN KEY (`lp_id` )
    REFERENCES `landingpage_metadata` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;


alter table landingpage_metadata add column subtitle text;

