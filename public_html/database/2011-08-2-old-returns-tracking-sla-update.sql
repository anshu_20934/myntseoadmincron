                           
alter table mk_old_returns_tracking_sla modify column prevstate ENUM('RTOQ','RTOCAL1', 'RTOCAL2', 'RTOCAL3','RTOC','RTORS', 'RTORSC', 'RTORF', 'RTOAI', 'RTQ','RTCSD','RTPI', 'RTPTU', 'RTR','RTQP','RTQF','RTCC','RTQPA','RTQPNA','RTRF','RTAI','RTRS','RTRSC', 'END', 'CMMNT', 'UDQ', 'UDC1', 'UDC2', 'UDC3', 'UDCC', 'UDLS') NOT NULL;
alter table mk_old_returns_tracking_sla modify column nextstate	ENUM('RTOQ','RTOCAL1', 'RTOCAL2', 'RTOCAL3','RTOC','RTORS', 'RTORSC', 'RTORF', 'RTOAI', 'RTQ','RTCSD','RTPI', 'RTPTU', 'RTR','RTQP','RTQF','RTCC','RTQPA','RTQPNA','RTRF','RTAI','RTRS','RTRSC', 'END', 'CMMNT', 'UDQ', 'UDC1', 'UDC2', 'UDC3', 'UDCC', 'UDLS') NOT NULL;

delete from mk_old_returns_tracking_sla where prevstate = 'RTOQ';

INSERT INTO mk_old_returns_tracking_sla VALUES ('RTOQ', 'RTOCAL1', 360);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTOCAL1', 'RTOCAL2', 360);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTOCAL1', 'RTOC', 360);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTOCAL1', 'RTORS', 360);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTOCAL2', 'RTOCAL3', 360);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTOCAL2', 'RTOC', 360);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTOCAL2', 'RTORS', 360);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTOCAL3', 'RTOC', 360);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTOCAL3', 'RTORS', 360);
