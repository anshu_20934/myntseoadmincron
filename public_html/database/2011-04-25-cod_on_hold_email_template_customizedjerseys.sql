insert into mk_email_notification(name, subject, body) values('cod_manual_verification', 'COD Order to be manually verified : Order #[ORDERID] ([CUSTOMERCAREMESSAGE])', 'Call and Verify this COD order
<br/>
<p>
Check for : <b>[CUSTOMERCAREMESSAGE]</b><br/>
Order    : [ORDERID] <br/>
Total    : [GRANDTOTAL] <br/>
Mobile No: [LASTMOBILE] <br/><br/>
Name     : [USER] <br/>
LoginID  : [LOGINID] <br/>
<br/><br/>
</p>

<p>These are the order details placed by [USER] :<br>
	<table style="width:95%">
		<tr style="background-color:#d9d9d9;">
			<th width="45%">Product - size(addon)</th>
			<th width="5%">Quantity</th>
			<th width="15%">Unit Price (INR)</th>
			<th width="20%">designer markup (INR)</th>
			<th width="15%">Total (INR)</th>
		</tr>
		[PRODUCTDETAIL]
		<tr><td colspan="5"><hr/></td></tr>
		<tr>
			<td colspan="4"	 align="right">Subtotal</td>
			<td align="right">[SUBTOTAL]</td>
		</tr>
		<tr>
			<td colspan="4"	 align="right">Discount</td>
			<td align="right">[DISCOUNT]</td>
		</tr>
		<tr>
			<td colspan="4"	 align="right">Tax</td>
			<td align="right">[TAX]</td>
		</tr>
		<tr>
			<td colspan="4"	 align="right">Shipping</td>
			<td align="right">[SHIPPINGCHARGE]</td>
		</tr>
		<tr>
			<td colspan="4"	 align="right">Gift Wrapping Charges</td>
			<td align="right">[GIFTWRAPPERCHARGE]</td>
		</tr>
		<tr><td colspan="5"><hr/></td></tr>
		<tr style="background-color:#ffffcc;">
			<td colspan="4"	 align="right"><b>Grand Total (Rs)</b></td>
			<td align="right"><b>[GRANDTOTAL]</b></td>
		</tr>
		<tr><td colspan="5"><hr/></td></tr>
	</table>
</p>');