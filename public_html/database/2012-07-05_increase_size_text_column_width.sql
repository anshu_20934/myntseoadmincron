alter table size_chart_scale modify scale_name varchar(64);
alter table size_chart_scale modify human_readable_scale_name varchar(64);
alter table size_chart_scale modify backward_compat_scale_name varchar(64);
update size_chart_scale set human_readable_scale_name = 'Innerwear T-Shirt Size' where human_readable_scale_name = 'Innerwear T-Shirt Si'
