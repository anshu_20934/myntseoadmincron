DROP TABLE IF EXISTS `mk_ad_affiliates`;

CREATE TABLE IF NOT EXISTS `mk_ad_affiliates` (
    `tagname`       VARCHAR(10) NOT NULL,
    `status`        CHAR(1) DEFAULT 'A',
    `since`         INT(11),
    `until`         INT(11),
    `trackspan`     INT(11) DEFAULT 2592000,
    `description`   VARCHAR(100)
)
ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='for ad affiliates like tyroo, dgm etc';

INSERT INTO mk_ad_affiliates (tagname) VALUES ('aff');
INSERT INTO mk_ad_affiliates (tagname) VALUES ('aff-dgm');
INSERT INTO mk_ad_affiliates (tagname) VALUES ('aff-ibibo');
