-- MySQL dump 10.13  Distrib 5.1.41, for Win32 (ia32)
--
-- Host: localhost    Database: myntra_new
-- ------------------------------------------------------
-- Server version	5.1.41

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gift_cards`
--

DROP TABLE IF EXISTS `gift_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gift_cards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gift_message_id` int(11) DEFAULT NULL,
  `gift_card_type` int(11) DEFAULT NULL,
  `pin` varchar(10) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `sender_email` varchar(128) DEFAULT NULL,
  `recipient_email` varchar(128) DEFAULT NULL,
  `sender_name` varchar(255) DEFAULT NULL,
  `recipient_name` varchar(255) DEFAULT NULL,
  `amount` decimal(12,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `delivered_date` int(11) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_message_id` (`gift_message_id`),
  CONSTRAINT `fk_message_id` FOREIGN KEY (`gift_message_id`) REFERENCES `gift_cards_messages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gift_cards`
--

LOCK TABLES `gift_cards` WRITE;
/*!40000 ALTER TABLE `gift_cards` DISABLE KEYS */;
INSERT INTO `gift_cards` VALUES (1,1,0,'GIFTxyzabc',0,'soaprajchanged@gmail.com','soaprajchanged@gmail.com',NULL,NULL,'15003.00',1,2012,'2012-08-02 08:39:45','2012-08-02 08:39:45',NULL),(2,2,1,'GIFTxyza2c',0,'soapraj@gmail.com','soapraj@gmail.com',NULL,NULL,'5005.00',1,2012,'2012-07-04 11:16:39','2012-07-04 11:16:39','soapraj@gmail.com'),(5,4,1,'ASDF1234OP',1,'soapraj@gmail.com','soapraj@gmail.com',NULL,NULL,'500.97',1,2012,'2012-07-09 09:10:53','2012-07-09 09:10:53',NULL),(6,5,1,'ASDF1234OP',1,'soapraj@gmail.com','soapraj@gmail.com',NULL,NULL,'500.97',1,2012,'2012-07-10 08:39:52','2012-07-10 08:39:52',NULL),(7,7,1,'GIFTtester',0,'soapraj@gmail.com','soapraj@gmail.com',NULL,NULL,'5003.00',1,NULL,'2012-07-24 18:22:38','2012-07-24 18:22:38',NULL),(8,8,1,'GIFTtester',0,'soapraj@gmail.com','soapraj@gmail.com',NULL,NULL,'5003.00',1,NULL,'2012-07-24 18:25:19','2012-07-24 18:25:19',NULL),(9,9,0,'GIFTtester',0,'raghunandan.b@myntra.com','john@myntra.com','JT Myntra','John Myntra','500.00',1,NULL,'2012-07-27 11:15:00','2012-07-27 11:15:00','raghunandan.b@myntra.com'),(10,10,0,'GIFTtester',0,'raghunandan.b@myntra.com','john@myntra.com','JT Myntra','John Myntra','500.00',1,NULL,'2012-07-27 16:16:10','2012-07-27 16:16:10','raghunandan.b@myntra.com'),(11,11,0,'GIFTtester',0,'raghunandan.b@myntra.com','john@myntra.com','JT Myntra','John Myntra','500.00',1,NULL,'2012-07-28 08:04:45','2012-07-28 08:04:45','raghunandan.b@myntra.com'),(12,12,0,'GIFTtester',0,'raghunandan.b@myntra.com','john@myntra.com','JT Myntra','John Myntra','1500.00',1,NULL,'2012-07-30 08:31:27','2012-07-30 08:31:27','raghunandan.b@myntra.com'),(20,19,0,'GIFTq49KvM',0,'raghunandan.b@myntra.com','test@gmail.com','JT Myntra','Narendra','1600.00',1,NULL,'2012-07-30 10:14:23','2012-07-30 10:14:23',NULL),(21,1,0,'GIFTxyzabc',0,'soaprajchanged@gmail.com','soaprajchanged@gmail.com','Hello Test','Test Hello','15003.00',1,2012,'2012-08-03 05:26:32','2012-08-03 05:26:32','testxyz@myntra.com'),(22,24,0,'GIFTkdMZiw',0,'testxyz@myntra.com','stest@gmail.com','World Hello','Hello World','500.00',1,NULL,'2012-08-01 11:16:55','2012-08-01 11:16:55',NULL),(23,25,0,'GIFTU9cgU6',0,'raghunandan.b@myntra.com','soapraj@gmail.com','adashdsahfuihe','asdifhbsaifdh','500.00',1,NULL,'2012-08-03 05:46:44','2012-08-03 05:46:44',NULL);
/*!40000 ALTER TABLE `gift_cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gift_card_occasions`
--

DROP TABLE IF EXISTS `gift_card_occasions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gift_card_occasions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `subject` text,
  `header` text,
  `body` text,
  `footer` text,
  `preview_image` varchar(255) DEFAULT NULL,
  `preview_image_thumbnail` varchar(255) DEFAULT NULL,
  `email_preview_image` varchar(255) DEFAULT NULL,
  `default_message` text,
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gift_card_occasions`
--

LOCK TABLES `gift_card_occasions` WRITE;
/*!40000 ALTER TABLE `gift_card_occasions` DISABLE KEYS */;
INSERT INTO `gift_card_occasions` VALUES (1,'Birthday','Mail Subject 1','','Mail Body 1','','http://myntra.myntassets.com/skin2/images/giftcards/GiftCard_Birthday.png','http://myntra.myntassets.com/skin2/images/giftcards/GiftCard_Thumb_Birthday.png',NULL,'Many more happy returns of the day','2012-07-04 11:16:39','2012-07-04 11:16:39','soapraj@gmail.com'),(2,'Anniversary','Mail Subject 2','','Mail Body 2','','http://myntra.myntassets.com/skin2/images/giftcards/GiftCard_Anniversary.png','http://myntra.myntassets.com/skin2/images/giftcards/GiftCard_Thumb_Anniversary.png',NULL,'Many more happy returns of the anniversary day','2012-07-04 11:16:39','2012-07-04 11:16:39','soapraj@gmail.com'),(3,'BestWishes','Mail Subject 3','','Mail Body 3','','http://myntra.myntassets.com/skin2/images/giftcards/GiftCard_BestWishes.png','http://myntra.myntassets.com/skin2/images/giftcards/GiftCard_Thumb_BestWishes.png',NULL,'Many more happy returns of the anniversary day 2',NULL,NULL,NULL),(4,'Congrats','Mail Subject 4','','Mail Body 4','','http://myntra.myntassets.com/skin2/images/giftcards/GiftCard_Congrats.png','http://myntra.myntassets.com/skin2/images/giftcards/GiftCard_Thumb_Congrats.png','','Congratulations','2012-07-24 06:54:04','2012-07-24 06:54:04','unknown@myntra.com');
/*!40000 ALTER TABLE `gift_card_occasions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gift_cards_messages`
--

DROP TABLE IF EXISTS `gift_cards_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gift_cards_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(255) DEFAULT NULL,
  `occasion_id` int(11) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mk_occasion_id` (`occasion_id`),
  CONSTRAINT `mk_occasion_id` FOREIGN KEY (`occasion_id`) REFERENCES `gift_card_occasions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gift_cards_messages`
--

LOCK TABLES `gift_cards_messages` WRITE;
/*!40000 ALTER TABLE `gift_cards_messages` DISABLE KEYS */;
INSERT INTO `gift_cards_messages` VALUES (1,'Hello World! 1 soaprajchanged',1,'2012-08-03 05:26:32','2012-08-03 05:26:32','testxyz@myntra.com'),(2,'Hello World! 2',2,'2012-07-04 11:16:39','2012-07-04 11:16:39','soapraj@gmail.com'),(4,'This is a test message',2,NULL,NULL,NULL),(5,'This is a test message',1,NULL,NULL,NULL),(6,'This is a test message',2,NULL,NULL,NULL),(7,'Hello World! 1',NULL,'2012-07-24 18:22:38','2012-07-24 18:22:38',NULL),(8,'Hello World! 1',1,'2012-07-24 18:25:19','2012-07-24 18:25:19',NULL),(9,'Here&#39;s to a future as bright as you are!',1,'2012-07-27 11:14:59','2012-07-27 11:14:59','raghunandan.b@myntra.com'),(10,'Here&#39;s to a future as bright as you are!',1,'2012-07-27 16:16:10','2012-07-27 16:16:10','raghunandan.b@myntra.com'),(11,'Here&#39;s to a future as bright as you are!',1,'2012-07-28 08:04:45','2012-07-28 08:04:45','raghunandan.b@myntra.com'),(12,'Here&#39;s to a future as bright as you are!',1,'2012-07-30 08:31:26','2012-07-30 08:31:26','raghunandan.b@myntra.com'),(19,'Test Message',1,'2012-07-30 10:14:23','2012-07-30 10:14:23',NULL),(23,'Here&#39;s to a future as bright as you are!',1,'2012-08-01 10:41:41','2012-08-01 10:41:41',NULL),(24,'Here&#39;s to a future as bright as you are!',1,'2012-08-01 11:16:55','2012-08-01 11:16:55',NULL),(25,'sdfahsfbajbfsbhb',3,'2012-08-03 05:46:44','2012-08-03 05:46:44',NULL);
/*!40000 ALTER TABLE `gift_cards_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gift_cards_orders`
--

DROP TABLE IF EXISTS `gift_cards_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gift_cards_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gift_card_id` int(11) NOT NULL,
  `final_amount` decimal(12,2) NOT NULL,
  `status` int(1) DEFAULT NULL,
  `mk_payments_log_id` int(31) NOT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gift_cards_orders`
--

LOCK TABLES `gift_cards_orders` WRITE;
/*!40000 ALTER TABLE `gift_cards_orders` DISABLE KEYS */;
INSERT INTO `gift_cards_orders` VALUES (1,20,'1600.00',0,123,'2012-07-30 10:14:23','2012-07-30 10:14:23',NULL),(2,21,'500.00',0,123,'2012-08-01 10:41:41','2012-08-01 10:41:41',NULL),(3,22,'500.00',0,123,'2012-08-01 11:16:55','2012-08-01 11:16:55',NULL),(4,23,'500.00',0,123,'2012-08-03 05:46:44','2012-08-03 05:46:44',NULL);
/*!40000 ALTER TABLE `gift_cards_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gift_card_logs`
--

DROP TABLE IF EXISTS `gift_card_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gift_card_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gift_card_id` int(11) DEFAULT NULL,
  `activated_by` varchar(128) DEFAULT NULL,
  `activated_on` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gift_card_id` (`gift_card_id`),
  CONSTRAINT `fk_gift_card_id` FOREIGN KEY (`gift_card_id`) REFERENCES `gift_cards` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gift_card_logs`
--

LOCK TABLES `gift_card_logs` WRITE;
/*!40000 ALTER TABLE `gift_card_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `gift_card_logs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-08-07 11:58:00
