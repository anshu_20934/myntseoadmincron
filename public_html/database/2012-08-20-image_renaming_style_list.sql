create table seo_image_renaming_details(
styleId int(11),
masterCategory varchar(255),
gender varchar(255),
renamingFinished int(1) default 0,
renameTime int(11),
PRIMARY KEY (styleId),
KEY masterCategory_idx (masterCategory),
KEY gender_idx (gender)
);
