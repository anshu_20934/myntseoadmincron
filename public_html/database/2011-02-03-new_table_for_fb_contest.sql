CREATE TABLE `mk_facebook_app_contest1` (
  `fb_uid` varchar(20) NOT NULL,
  `fb_last_publish` int(11) default 0,
  `won` int(2) default 0,
  PRIMARY KEY (`fb_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `mk_facebook_app_contest1_friends_joined` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `fb_uid` varchar(20) NOT NULL,
  `fb_friend_uid` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

alter  table mk_facebook_app_contest1_friends_joined add index `fb_uid` (fb_uid);