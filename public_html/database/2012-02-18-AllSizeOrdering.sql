-- MySQL dump 10.13  Distrib 5.1.50, for Win32 (ia32)
--
-- Host: localhost    Database: myntra
-- ------------------------------------------------------
-- Server version	5.1.50-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mk_all_size_order`
--

DROP TABLE IF EXISTS `mk_all_size_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mk_all_size_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `StringSize` varchar(45) DEFAULT NULL,
  `intorder` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_UNIQUE` (`intorder`),
  UNIQUE KEY `StringSize_UNIQUE` (`StringSize`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mk_all_size_order`
--

LOCK TABLES `mk_all_size_order` WRITE;
/*!40000 ALTER TABLE `mk_all_size_order` DISABLE KEYS */;
INSERT INTO `mk_all_size_order` VALUES (1,'0','1'),(2,'0.5','2'),(3,'1','3'),(4,'1.5','4'),(5,'2','5'),(6,'2.5','6'),(7,'3','7'),(8,'3.5','8'),(9,'4','9'),(10,'4.5','10'),(11,'5','11'),(12,'6','12'),(13,'6.5','13'),(14,'7','14'),(15,'7.5','15'),(16,'8','16'),(17,'8.5','17'),(18,'9','18'),(19,'9.5','19'),(20,'10','20'),(21,'10.5','21'),(22,'11','22'),(23,'11.5','23'),(24,'12','24'),(25,'12.5','25'),(26,'13','26'),(27,'13.5','27'),(28,'14','28'),(29,'14.5','29'),(30,'15','30'),(31,'15.5','31'),(32,'16','32'),(33,'16.5','33'),(34,'17','34'),(35,'17.5','35'),(36,'18','36'),(37,'18.5','37'),(38,'19','38'),(39,'19.5','39'),(40,'20','40'),(42,'XXS','100'),(43,'XS','101'),(44,'S','102'),(45,'M','103'),(46,'L','104'),(47,'XL','105'),(48,'XXL','106'),(49,'XXXL','107'),(50,'EURO32','61'),(51,'EURO33','62'),(52,'EURO34','63'),(53,'EURO35','64'),(54,'EURO36','65');
/*!40000 ALTER TABLE `mk_all_size_order` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-02-19  5:04:50
