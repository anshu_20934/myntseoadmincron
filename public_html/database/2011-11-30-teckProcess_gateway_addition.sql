INSERT INTO mk_netbanking_gateways (id, name,weight)
VALUES (4,'TekProcess',30);

INSERT INTO mk_netbanking_banks (id, name, is_popular)
VALUES (39, 'Development Credit Bank', 0),
       (40, 'Dhanlaxmi Bank' , 0),
       (41, 'Indian Bank', 0);

INSERT INTO mk_netbanking_mapping (bank_id, gateway_id, gateway_code, is_active)
VALUES(1,4,50,1),
(7,4,340,1),
(8,4,310,1),
(9,4,310,1),
(10,4,240,1),
(12,4,440,1),
(13,4,120,1),
(15,4,270,1),
(2,4,300,1),
(3,4,10,1),
(16,4,520,1),
(17,4,420,1),
(21,4,140,1),
(28,4,180,1),
(29,4,450,1),
(34,4,620,1),
(35,4,190,1),
(37,4,200,1),
(38,4,130,1),
(39,4,540,1),
(40,4,370,1),
(41,4,490,1);
