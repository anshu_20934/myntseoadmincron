update mk_org_accounts set brand_shop_dir='/ugam-store-template' where subdomain='ugam';

update mk_email_notification set body='Dear [USER],<br><p>Your defiance Brand Shop account has been created. You can login to your account using following details: </p><br><p>Brand Shop URL : www.defiancebrandstore.com </p><p>login: [LOGIN]</p><p>Password: [PASSWORD]</p><p>At this brand store, you can purchase merchandize like t-shirts, mugs, caps, key-chains and many other products at discounted price exclusively for employees of defiance. You can also personalize these products with your photographs, your company logo or name or message. You can also order merchandize in bulk for your team for different events. For such requirements you can get in touch with us at sales@myntra.com and we will do our best to work out right solution for you at pre-approved prices for defiance.</p><p>For any help or questions regarding your account or products, you can get in touch with us defiance-support@myntra.com</p><p>Yours Sincerely<br/>Myntra Customer Service<br/>www.myntra.com<br/></p>' where name='orgdefianceregistration';

update mk_email_notification set body='<p>Hi Ugamite !</p><br/> 
<p>Thank you for registering on the Ugam My Shoppe.</p><br/> 
<p>Your details are as follows �<br/>
Username: [LOGIN]<br/>
Password: [PASSWORD]<br/><br/> 
Online gift voucher code <b>[COUPON]</b><br/><br/> 
Terms and Conditions:<br/>
1) The Online gift voucher code is valid for only 30 days after registration.<br/>
2) You can redeem your Online gift voucher only once.<br/><br/> 
Happy Shopping !</p>' where name='orgugamregistration';

alter table mk_org_accounts add column coupon_on_registration enum('Y','N') default 'N';
update mk_org_accounts set coupon_on_registration='Y' where id=35;