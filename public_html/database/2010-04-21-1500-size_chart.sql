use myntra;
/*queries for raglan */
insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('392',' 14 - 1/2','21','6','5','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('393','14 - 1/2','21','6 �','5 - �','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('394','16 - 1/2','22 �','7','5 - �','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('1239','17 - 1/2','22 �','8','6','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2250','18 - 1/2','24','9','7','','1');

/*select id from mk_product_options where value='XXL' and style='234'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('396',' 14 - 1/2','21','6','5','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('397','14 - 1/2','21','6 �','5 - �','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('398','16 - 1/2','22 �','7','5 - �','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('399','17 - 1/2','22 �','8','6','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('1232','18 - 1/2','24','9','7','','1');

/*select id from mk_product_options where value='XXL' and style='235'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2298',' 14 - 1/2','21','6','5','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2299','14 - 1/2','21','6 �','5 - �','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2300','16 - 1/2','22 �','7','5 - �','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2301','17 - 1/2','22 �','8','6','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2302','18 - 1/2','24','9','7','','1');

/*select id from mk_product_options where value='XXL' and style='977'; does not have entry in mk_product_options table*/

/* ==================================queries for womens_ringer.csv =======================================================*/
insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('384',' 14 - 1/2','21','4','4','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('385','15 - 1/2','22','4 �','4','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('386','16 - 1/2','23','5','5','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('387','17 - 1/2','23','5','5','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('1238','18 - 1/2','24','5 - �','5','','1');

/*select id from mk_product_options where value='XXL' and style='232'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('388',' 14 - 1/2','21','4','4','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('389','15 - 1/2','22','4 �','4','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('390','16 - 1/2','23','5','5','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('391','17 - 1/2','23','5','5','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('1237','18 - 1/2','24','5 - �','5','','1');

/*select id from mk_product_options where value='XXL' and style='233'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2245',' 14 - 1/2','21','4','4','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2246','15 - 1/2','22','4 �','4','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2247','16 - 1/2','23','5','5','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2248','17 - 1/2','23','5','5','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2249','18 - 1/2','24','5 - �','5','','1');

/*select id from mk_product_options where value='XXL' and style='968'; does not have entry in mk_product_options table*/

/*==============================queries for women_round_neck .csv=============================================================*/
insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('21',' 14 - 1/2','20','4 �','5','12','1');

/*select id from mk_product_options where value='S' and style='7'; does not have entry in mk_product_options table*/

/*select id from mk_product_options where value='M' and style='7'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('231','17 - 1/2','22.5','6',' 5-1/2','15','1');

/*select id from mk_product_options where value='XL' and style='7'; does not have entry in mk_product_options table*/

/*select id from mk_product_options where value='XXL' and style='7'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('24',' 14 - 1/2','20','4 �','5','12','1');

/*select id from mk_product_options where value='S' and style='8'; does not have entry in mk_product_options table*/

/*select id from mk_product_options where value='M' and style='8'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2308','17 - 1/2','22.5','6',' 5-1/2','15','1');

/*select id from mk_product_options where value='XL' and style='8'; does not have entry in mk_product_options table*/

/*select id from mk_product_options where value='XXL' and style='8'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('828',' 14 - 1/2','20','4 �','5','12','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('829','15 - 1/2','21.5','5','5','13','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('830','16 - 1/2','22','5 �',' 5-1/2','14','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('831','17 - 1/2','22.5','6',' 5-1/2','15','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('832','18 - 1/2','23','6 �','6','16','1');

/*select id from mk_product_options where value='XXL' and style='421'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('833',' 14 - 1/2','20','4 �','5','12','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('834','15 - 1/2','21.5','5','5','13','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('835','16 - 1/2','22','5 �',' 5-1/2','14','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('836','17 - 1/2','22.5','6',' 5-1/2','15','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('837','18 - 1/2','23','6 �','6','16','1');

/*select id from mk_product_options where value='XXL' and style='425'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('843',' 14 - 1/2','20','4 �','5','12','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('844','15 - 1/2','21.5','5','5','13','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('845','16 - 1/2','22','5 �',' 5-1/2','14','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('846','17 - 1/2','22.5','6',' 5-1/2','15','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('847','18 - 1/2','23','6 �','6','16','1');

/*select id from mk_product_options where value='XXL' and style='427'; does not have entry in mk_product_options table*/
/* ======================================queries for womens_polo.csv ======================================================== */
insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('376',' 14 - 1/2','21.5','4 �','5','12','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2050','15 - 1/2','22.5','5','5','13.5','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2051','16 - 1/2','23','5 �',' 5-1/2','14','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2052','17 - 1/2','23','6',' 5-1/2','15','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2053','17.5','24.5','6 �','6','16','1');

/*select id from mk_product_options where value='XXL' and style='230'; does not have entry in mk_product_options table*/

/*select id from mk_product_options where value='XS' and style='231'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('382','15 - 1/2','22.5','5','5','13.5','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('383','16 - 1/2','23','5 �',' 5-1/2','14','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('802','17 - 1/2','23','6',' 5-1/2','15','1');

/*select id from mk_product_options where value='XL' and style='231'; does not have entry in mk_product_options table*/

/*select id from mk_product_options where value='XXL' and style='231'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2212',' 14 - 1/2','21.5','4 �','5','12','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2213','15 - 1/2','22.5','5','5','13.5','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2214','16 - 1/2','23','5 �',' 5-1/2','14','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2215','17 - 1/2','23','6',' 5-1/2','15','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2216','17.5','24.5','6 �','6','16','1');

/*select id from mk_product_options where value='XXL' and style='962'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2217',' 14 - 1/2','21.5','4 �','5','12','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2218','15 - 1/2','22.5','5','5','13.5','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2219','16 - 1/2','23','5 �',' 5-1/2','14','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2220','17 - 1/2','23','6',' 5-1/2','15','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2221','17.5','24.5','6 �','6','16','1');

/*select id from mk_product_options where value='XXL' and style='963'; does not have entry in mk_product_options table*/

/* ============================================queries for mens_polo.csv ============================================*/
insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('352','19','25','8','6','17','1');

/*select id from mk_product_options where value='M' and style='223'; does not have entry in mk_product_options table*/

/*select id from mk_product_options where value='L' and style='223'; does not have entry in mk_product_options table*/

/*select id from mk_product_options where value='XL' and style='223'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2039','23','29','9 �','8','21','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('372','19','25','8','6','17','1');

/*select id from mk_product_options where value='M' and style='229'; does not have entry in mk_product_options table*/

/*select id from mk_product_options where value='L' and style='229'; does not have entry in mk_product_options table*/

/*select id from mk_product_options where value='XL' and style='229'; does not have entry in mk_product_options table*/

/*select id from mk_product_options where value='XXL' and style='229'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2227','19','25','8','6','17','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2228','20','26','8','6','18','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2229','21','27','8 �','6 �','20','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2230','22','28','9','7','20','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2231','23','29','9 �','8','21','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2232','19','25','8','6','17','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2233','20','26','8','6','18','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2234','21','27','8 �','6 �','20','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2235','22','28','9','7','20','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2236','23','29','9 �','8','21','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2237','19','25','8','6','17','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2238','20','26','8','6','18','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2239','21','27','8 �','6 �','20','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2240','22','28','9','7','20','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2241','23','29','9 �','8','21','1');
/* ============================================= mens_round_neck.csv ===================================================*/
insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('5','19','25','8','6','17','1');

/*select id from mk_product_options where value='M' and style='3'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2191','21','27','8 �','6 �','19','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2190','22','28','9','7','20','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2189','23','29','9','8','21','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('15','19','25','8','6','17','1');

/*select id from mk_product_options where value='M' and style='5'; does not have entry in mk_product_options table*/

/*select id from mk_product_options where value='L' and style='5'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2244','22','28','9','7','20','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2689','23','29','9','8','21','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('808','19','25','8','6','17','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('809','20','26','8','6','18','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('810','21','27','8 �','6 �','19','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('811','22','28','9','7','20','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('812','23','29','9','8','21','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('813','19','25','8','6','17','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2056','20','26','8','6','18','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2304','21','27','8 �','6 �','19','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2305','22','28','9','7','20','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2306','23','29','9','8','21','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('818','19','25','8','6','17','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('819','20','26','8','6','18','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('821','21','27','8 �','6 �','19','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('822','22','28','9','7','20','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2307','23','29','9','8','21','1');
/* =============================== mens_round_neck_ringer.csv===========================================================*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('356','19','25','8','6','','1');

/*select id from mk_product_options where value='M' and style='224'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('1234','21','27','9','6 �','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2045','22','28','9','7','','1');

/*select id from mk_product_options where value='XXL' and style='224'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('360','19','25','8','6','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('1235','20','26','8','6','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2058','21','27','9','6 �','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2293','22','28','9','7','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2294','23','29','10','7 �','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2288','19','25','8','6','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2289','20','26','8','6','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2290','21','27','9','6 �','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2291','22','28','9','7','','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2292','23','29','10','7 �','','1');
/* ======================================== mens_round_neck_ranglan.csv =============================================================*/
insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('364','19','25','13','6','','1');

/*select id from mk_product_options where value='M' and style='226'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2295','21','27','15','7','','1');

/*select id from mk_product_options where value='XL' and style='226'; does not have entry in mk_product_options table*/

/*select id from mk_product_options where value='XXL' and style='226'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('368','19','25','13','6','','1');

/*select id from mk_product_options where value='M' and style='228'; does not have entry in mk_product_options table*/

/*select id from mk_product_options where value='L' and style='228'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2311','22','28','16','7�','','1');

/*select id from mk_product_options where value='XXL' and style='228'; does not have entry in mk_product_options table*/
/* ================================================ kids_round_neck.csv ========================================*/
/*select id from mk_product_options where value='2-4yrs' and style='67'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('158','14 �','18','14','4 �','5 - 3/8 ','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('159','15 �','19','14 �','5 - �','5 - �','1');

/*select id from mk_product_options where value='8-10yrs' and style='67'; does not have entry in mk_product_options table*/

/*select id from mk_product_options where value='10-12yrs' and style='67'; does not have entry in mk_product_options table*/

/*select id from mk_product_options where value='2-4yrs' and style='282'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('551','14 �','18','14','4 �','5 - 3/8 ','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('552','15 �','19','14 �','5 - �','5 - �','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('553','16 �','21','15 �','6','6','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('560','17 �','23','16 �','7 �','6 �','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('561','13 �','17','13','4','5','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('562','14 �','18','14','4 �','5 - 3/8 ','1');

/*select id from mk_product_options where value='6-8yrs' and style='284'; does not have entry in mk_product_options table*/

/*select id from mk_product_options where value='8-10yrs' and style='284'; does not have entry in mk_product_options table*/

/*select id from mk_product_options where value='10-12yrs' and style='284'; does not have entry in mk_product_options table*/

/*select id from mk_product_options where value='2-4yrs' and style='285'; does not have entry in mk_product_options table*/

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('567','14 �','18','14','4 �','5 - 3/8 ','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('568','15 �','19','14 �','5 - �','5 - �','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('569','16 �','21','15 �','6','6','1');

insert into mk_product_options_details(product_option_id,chest,full_length,sleeve_l,sleeve_w,shoulder,is_active) values('2287','17 �','23','16 �','7 �','6 �','1');