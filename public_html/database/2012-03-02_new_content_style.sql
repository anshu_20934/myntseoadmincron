ALTER TABLE mk_style_properties
ADD (style_note text default null,
materials_care_desc text default null,
size_fit_desc text default null,
comments text default null,
weight int default 0);