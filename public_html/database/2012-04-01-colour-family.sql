ALTER TABLE mk_attribute_type ENGINE = InnoDB;
ALTER TABLE mk_attribute_type_values ENGINE = InnoDB;

DROP TABLE IF EXISTS `attribute_type_family_member`;
DROP TABLE IF EXISTS `attribute_type_family`;

CREATE TABLE `attribute_type_family` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_attribute_type` int NOT NULL,
  `family_name` varchar(50) NOT NULL,
  `swatch_position` int NOT NULL,
  `created_on` int DEFAULT NULL,
  `created_by` varchar(128) DEFAULT NULL,
  `updated_on` int DEFAULT NULL,
  `updated_by` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`id_attribute_type`) REFERENCES `mk_attribute_type`(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `attribute_type_family_member` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_attribute_type_family` int NOT NULL,
  `id_attribute_type_value` int NOT NULL,
  `created_on` int DEFAULT NULL,
  `created_by` varchar(128) DEFAULT NULL,
  `updated_on` int DEFAULT NULL,
  `updated_by` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`id_attribute_type_family`) REFERENCES `attribute_type_family`(`id`),
  FOREIGN KEY (`id_attribute_type_value`) REFERENCES `mk_attribute_type_values`(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



