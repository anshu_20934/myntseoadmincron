update mk_email_notification
set body = '
<div style="width: 550px; height: 330px;padding-left: 27px;" align="LEFT"><div style="margin-top: 15px;width: 550px;  font-family: georgia; font-size: 14px;  color: #000;">Hi !<br><br>Thank you for visiting <a href="http://www.myntra.com?utm_source=MRP_mailer&utm_medium=welcome_non_fb&utm_campaign=mlrwelcome-mynt-club" style="color:#0084BA;text-decoration:none;"target="_blank">Myntra.com</a>, India\'s largest online fashion store. We offer the latest and authentic products from over 300 brands. We provide <b>FREE SHIPPING</b>*, with an option to pay <b>CASH ON DELIVERY</b>. And if you don\'t like what you have ordered, we offer a hassle-free <b>30 DAY RETURN POLICY</b>.<br><br>As a token of our appreciation, we are providing <b>[NUM_COUPONS] coupons of Rs. [EACH_COUPON_VALUE]</b> valid on a minimum purchase of Rs. [MIN_PURCHASE_AMOUNT]. These coupons are present in your <a href="http://www.myntra.com/mymyntra.php?view=mymyntcredits&utm_source=MRP_mailer&utm_medium=welcome_non_fb&utm_campaign=mlrwelcome-mynt-club" style="color:#0084BA" target="_blank">Mynt Credits account</a> and are valid only for 7 days. <br><br>Visit <a href="http://www.myntra.com?utm_source=MRP_mailer&utm_medium=welcome_non_fb&utm_campaign=mlrwelcome-mynt-club" style="color:#0084BA;text-decoration:none;"target="_blank">Myntra.com</a> and enjoy the experience of shopping on India\'s largest online fashion store <br><br> Regards, <br> Team Myntra<br><a href="http://www.myntra.com?utm_source=MRP_mailer&utm_medium=welcome_non_fb&utm_campaign=mlrwelcome-mynt-club" style="color:#000;text-decoration:none;" target="_blank">www.myntra.com</a><br><span style="font-size: 12px;color: gray;">Helpline: +91-80-43541999</span></div></div></div>
'
where name='mrp_first_login';


update mk_email_notification
set body = '
<div style="width: 550px; height: 330px;padding-left: 27px;" align="LEFT"><div style="margin-top: 15px;width: 550px;  font-family: georgia; font-size: 14px;  color: #000;">Hi !<br><br>Thank you for visiting <a href="http://www.myntra.com?utm_source=MRP_mailer&utm_medium=welcome_fb&utm_campaign=mlrwelcome-mynt-club" style="color:#0084BA;text-decoration:none;"target="_blank">Myntra.com</a>, India\'s largest online fashion store. We offer the latest and authentic products from over 300 brands. We provide <b>FREE SHIPPING</b>*, with an option to pay <b>CASH ON DELIVERY</b>. And if you don\'t like what you have ordered, we offer a hassle-free <b>30 DAY RETURN POLICY</b>.<br><br>As a token of our appreciation, we are providing <b>[NUM_COUPONS] coupons of Rs. [EACH_COUPON_VALUE]</b> valid on a minimum purchase of Rs. [MIN_PURCHASE_AMOUNT]. These coupons are present in your <a href="http://www.myntra.com/mymyntra.php?view=mymyntcredits&utm_source=MRP_mailer&utm_medium=welcome_fb&utm_campaign=mlrwelcome-mynt-club" style="color:#0084BA" target="_blank">Mynt Credits account</a> and are valid only for 7 days. <br><br>Visit <a href="http://www.myntra.com?utm_source=MRP_mailer&utm_medium=welcome_fb&utm_campaign=mlrwelcome-mynt-club" style="color:#0084BA;text-decoration:none;"target="_blank">Myntra.com</a> and enjoy the experience of shopping on India\'s largest online fashion store <br><br> Regards, <br> Team Myntra<br><a href="http://www.myntra.com?utm_source=MRP_mailer&utm_medium=welcome_fb&utm_campaign=mlrwelcome-mynt-club" style="color:#000;text-decoration:none;" target="_blank">www.myntra.com</a><br><span style="font-size: 12px;color: gray;">Helpline: +91-80-43541999</span></div></div></div>
'
where name='mrp_fbfirst_login';

update mk_email_notification
set body = '
<div style="width:615px">
<div style="height:90px;background-color:#ffffff;margin:0px;padding:0px">
<div style="float:left;padding-left:10px;padding-top:12px">
<a href="http://www.myntra.com/?utm_source=MRP_mailer&utm_medium=mailer&utm_campaign=myntra_logo" target="_blank">
<img src="http://myntramailer.s3.amazonaws.com/february2012/1329901955myntra-logo.jpg" alt="Myntra" title="Myntra" width="215" height="75" style="line-height:0px;border:0px;color:#fff;font-family:arial,helvetica,serif;font-size:12px"></a></div><div style="float:right;margin-top:5px;height:80px;width:250px;margin-right:15px">
<div style="float:left;width:90px;height:70px">
<a href="http://www.myntra.com?utm_source=MRP_mailer&utm_medium=mailer&utm_campaign=original" alt="100% Original" title="100% Original" target="_blank">
<img src="http://myntramailer.s3.amazonaws.com/january2012/1326188875100original.jpg" height="79" width="90" style="line-height:0px;border:0px;color:#fff;font-family:arial,helvetica,serif;font-size:12px"></a>
</div>
<div style="width:140px;height:80px;float:right;font-family:georgia,arial,serif;margin:5px 0px 10px 0px;padding:0px 5px;display:block;font-size:14px;color:#4d4d4d;line-height:22px"> FREE SHIPPING<br> 30 DAY RETURNS<br> CASH ON DELIVERY
</div>
</div>
</div>

                            <div style="height:33px;background-color:rgb(103,103,103);margin:15pt 0pt 0px">
                                <ul style="margin:0px;float:left;color:white;letter-spacing:2px;font-family:georgia,times New Roman,serif;font-size:14px;list-style:none outside none;padding:0px;line-height:15px">
                                    <li style="border-right:1px solid #8a8a8a;float:left;margin:0px"><a href="http://www.myntra.com/men?utm_source=mlr&amp;utm_medium=welcome-mynt-club&amp;utm_campaign=mlrwelcome-mynt-club" style="color:#fff;text-decoration:none;display:block;padding:8px 15px" target="_blank">MEN</a></li>
                                    <li style="border-right:1px solid #8a8a8a;float:left;margin:0px"><a href="http://www.myntra.com/women?utm_source=mlr&amp;utm_medium=welcome-mynt-club&amp;utm_campaign=mlrwelcome-mynt-club" style="color:#fff;text-decoration:none;display:block;padding:8px 15px" target="_blank">WOMEN</a></li>
                                    <li style="border-right:1px solid #8a8a8a;float:left;margin:0px"><a href="http://www.myntra.com/kids?utm_source=mlr&amp;utm_medium=welcome-mynt-club&amp;utm_campaign=mlrwelcome-mynt-club" style="color:#fff;text-decoration:none;display:block;padding:8px 10px" target="_blank">KIDS</a></li>
                                    <li style="border-right:1px solid #8a8a8a;float:left;margin:0px"><a href="http://www.myntra.com/brands?utm_source=mlr&amp;utm_medium=welcome-mynt-club&amp;utm_campaign=mlrwelcome-mynt-club" style="color:#fff;text-decoration:none;display:block;padding:8px 15px" target="_blank">BRANDS</a></li>
                                    <li style="border-right:1px solid #8a8a8a;float:left;margin:0px"><a href="http://www.myntra.com/sales?utm_source=mlr&amp;utm_medium=welcome-mynt-club&amp;utm_campaign=mlrwelcome-mynt-club" style="color:#fff;text-decoration:none;display:block;padding:8px 15px" target="_blank">SALE</a></li>
                                    <li style="float:left;margin:0px"><a href="http://www.stylemynt.com?utm_source=mlr&amp;utm_medium=welcome-mynt-club&amp;utm_campaign=mlrwelcome-mynt-club" style="color:#fff;text-decoration:none;display:block;padding:8px 20px" target="_blank">STYLE BLOG</a></li>
                                </ul>
                            </div>
</div>
'
where name='mrp_header';


update mk_email_notification
set body = '
<div style="width:615px;height:120px;background-color:#f2f2f2">
                                <img src="http://myntramailer.s3.amazonaws.com/february2012/1329296349footer-border.jpg" alt="" title="" width="615" height="9">
                                <div style="margin-left:5px;padding-top:1px;margin-top:5px;width:450px;float:left;height:30px;color:#666666">
                                    <a href="mailto:support@myntra.com" style="font-family:georgia;font-size:12px;text-decoration:none;color:#0084ba" target="_blank">support@myntra.com</a> | <span style="font-family:georgia;font-size:12px;text-decoration:none;color:#0084ba">Call +91-80-43541999</span> | <span style="font-family:georgia;font-size:12px;text-decoration:none;color:#0084ba">Connect with us</span>
                                </div>
                                <div style="width:150px;float:right;height:30px;color:#666666" align="left">
                                    <a href="http://www.facebook.com/myntra" style="outline:none" target="_blank"><img src="http://myntramailer.s3.amazonaws.com/february2012/1329296349facebook.jpg" width="31" height="30" border="none"></a>
                                    <a href="http://www.twitter.com/myntra" style="outline:none;padding-left:3px" target="_blank"><img src="http://myntramailer.s3.amazonaws.com/february2012/1329296350twitter.jpg" width="31" height="30" border="none"></a>
                                    <a href="https://plus.google.com/109282217040005996404/posts" style="outline:none;padding-left:3px" target="_blank"><img src="http://myntramailer.s3.amazonaws.com/february2012/1329296349googleplus.jpg" width="31" height="30" border="none"></a>
                                    <a href="http://www.youtube.com/user/myntradotcom" style="outline:none;padding-left:3px" target="_blank"><img src="http://myntramailer.s3.amazonaws.com/february2012/1329296350youtube.jpg" width="31" height="30" border="none"></a>
                                </div>
                                <div style="margin-left:5px;padding-top:1px;margin-top:5px;width:600px;float:left;height:50px;color:#666666;font-family:arial;font-size:10px">
                                     Offer valid on <a href="http://www.myntra.com?utm_source=MRP_mailer&amp;utm_medium=mailer&amp;utm_campaign=myntra_logo" style="font-family:georgia;color:#0084ba" target="_blank">Myntra.com</a> only. Myntra logo is a registered trademark of <a href="http://www.myntra.com?utm_source=MRP_mailer&amp;utm_medium=mailer&amp;utm_campaign=myntra_logo" style="font-family:georgia;color:#0084ba" target="_blank">www.myntra.com</a>, India. Check out our <a href="http://www.myntra.com/privacy_policy.php?utm_source=MRP_mailer&amp;utm_medium=mailer&amp;utm_campaign=myntra_pp" style="font-family:georgia;color:#0084ba" target="_blank">Privacy Policy</a>
                                    Š 2011. All Right Reserved. <a href="http://www.myntra.com?utm_source=mlr&utm_medium=welcome-mynt-club&utm_campaign=mlrwelcome-mynt-club" style="font-family:georgia;color:#0084ba" target="_blank">www.myntra.com</a>
                                </div>
                            </div>
'
where name='mrp_footer';
