CREATE TABLE `mk_skus_logs` (
  `skuid` int(11) DEFAULT NULL,
  `log` varchar(255) DEFAULT NULL,
  `lastmodified` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `mk_skus` ADD COLUMN `inv_count` INT NULL DEFAULT '0'  , ADD COLUMN `thrs_count`  INT NULL DEFAULT '0'  , ADD COLUMN `lastuser`  VARCHAR(65) NULL DEFAULT NULL  , ADD COLUMN `admin_disabled`  TINYINT(1) NULL DEFAULT false  ;