CREATE TABLE `mk_widget_key_value_pairs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(30),
  `value` varchar(120),
  `description` varchar(255),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) values('customerSupportCall','Call +91-80-43541901/03/19','Customer Support Call Message');
insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) values('customerSupportTime','(24 hours 7 days a week)','Customer Support Timings');