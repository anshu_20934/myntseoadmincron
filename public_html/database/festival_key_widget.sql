insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) values ('fest_month_start_date','2012-12-1','given date in YYYY-MM-DD format');

insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) values ('fest_month_end_date','2012-12-31','given date in YYYY-MM-DD format');

insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) values ('fest_week_start_day',6,'put 0 for sunday,1 for monday,2 for tues.....6 for saturday');

insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) values ('fest_week_end_day',5,'put 0 for sunday,1 for monday,2 for tues.....6 for saturday');

insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) values ('fest_leader_board_id',2,'leader board id reflects id in leader_board table corresponding to leader board being used');

insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) values ('fest_cache_invalidate_time',300,'set the time afte which cache should become invalid');

insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) values ('fest_topNbuyers',5,'gives value of N in top N buyers');

insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) values ('fest_topNCategoryBuyers',1,'gives value of N in top N category buyers');

insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) values ('fest_lucky_draw_increment_amount',1000,'incremental amount on which extra each ticket will be added');

insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) values ('fest_lucky_draw_min_amount',5000,'minimum amount to qualify for the first lucky draw ticket');

insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) values ('fest_prizes_semicolon_separated','Samsung Galaxy S Duos;iPod Nano;iPod Nano;Samsung DV100 Camera;Samsung DV100 Camera','semi-colon separated prizes for the respective positions');

insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) values ('fest_prizes_men_semicolon_separated','Fossil watch','semi-colon separated prizes for the respective positions');

insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) values ('fest_prizes_women_semicolon_separated','Gold pendant','semi-colon separated prizes for the respective positions');

insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) values ('fest_shop_more_to_win_msg','SHOP FOR Rs. $mindiff MORE AND GET A CHANCE TO WIN $prize','Message to be displayed when I need to shop more and win some prize');

insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) values ('fest_top_in_all_cat_msg','You are the WINNER in all categories','message to be displayed when the user is the topper in all categories');

insert into `mk_feature_gate_key_value_pairs` (`key`, `value`, `description`) values ('myntrashoppingfestleaderboard',true,'Enabling/Disabling the leader board functionality for Myntra Shopping Festival');

insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) values ('fest_daily_winners_ui_msg','Be one of the top 5 shoppers on Myntra and qualify for the Feiry Five prizes such as <strong>Samsung Galaxy S Duos</strong>, <strong>iPod Nano</strong>, and <strong>Samsung Digital camera</strong>.Also be the top shopper in Men\'s products for the day and win a Men\'s <strong>Fossil watch </strong> or be the top shopper in Women\'s products for the day and win a Women\'s <strong>Gitanjali Gold Chain and Pendant</strong>.','Enabling/Disabling the leader board functionality for Myntra Shopping Festival');


