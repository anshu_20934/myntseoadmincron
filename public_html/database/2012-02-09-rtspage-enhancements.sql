drop table if exists ready_to_ship_batch;

create table ready_to_ship_batch(
id int not null AUTO_INCREMENT,
orderids text,
created_by varchar(255) not null,
created_time datetime not null,
last_updated_time datetime,
PRIMARY KEY (id)
)ENGINE=InnoDb;
