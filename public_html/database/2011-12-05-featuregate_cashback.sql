insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('cashback.displayOnPDPPage','true','false removes cashback related infromation from PDP page');

insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('cashback.displayOnHomePage','true','false removes cashback related infromation from home page');

insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('cashback.displayOnCartPage','true','false removes cashback related infromation from cart page');

insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values ('cashback.displayOnSearchPage','true','false removes cashback related infromation from Search page');