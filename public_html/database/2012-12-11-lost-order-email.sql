insert into mk_email_notification(name,
	subject,body) values('order_lost','Order No. [ORDER_ID] Lost in transit','
	Dear [USER],<br><br>
	We are sorry to inform you that the shipment containing one or more products below that you had ordered has been lost on its way to you.<br><br>
	Please find the details of the item(s) below:<br>
	[ITEM_DETAILS]
	<br>
	<p>We’ve made all attempts possible to track down the package but have not been able to find it. Since this was the last unit of the product in our stock, we are unable to ship a replacement to you. [GOODWILL_COUPON_MSG] [REFUND_MSG] [COUPON_USAGE_TEXT]</p>
	<br/><br/>

<p>We assure you that this is an exceptional situation and apologize for any inconvenience this may have caused you. We hope you will give us another chance to serve you.<br>

If you have any further questions, please do not hesitate to contact our customer care champions by using the web form at 
www.myntra.com/contactus or call us anytime at +91-80-43541999.</p><br/>

Warm Regards,<br/>

Customer Connect Team,<br/>
Myntra Designs Pvt. Ltd. <br/><br/>
	');
