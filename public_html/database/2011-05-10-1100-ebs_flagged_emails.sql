insert into mk_email_notification (name, subject, body) values('ebs_flagged_on_hold','[ORDER_ID] flagged by EBS', 'The following order has been flagged by EBS and put on ON HOLD.<br/>
<table>
	<tr><td>OrderID</td><td>[ORDER_ID]</td></tr>
	<tr><td>Payment ID</td><td>[GATEWAY_ID]</td></tr>
	<tr><td>Order Date</td><td>[ORDER_DATE]</td></tr>
	<tr><td>Order Amount</td><td>[ORDER_AMOUNT]</td></tr>
	<tr><td>Customer login</td><td>[CUSTOMER]</td></tr>
	<tr><td>Customer name</td><td>[CUSTOMER_NAME]</td></tr>
	<tr><td>Customer phone</td><td>[CUSTOMER_PHONE]</td></tr>
</table>');

insert into mk_email_notification (name, subject, body) values('ebs_flagged_rejection','Myntra Order [ORDER_ID] Cancellation','Dear [FIRST_NAME],<br/>This is in reference to your order no. <a href="http://www.myntra.com/mymyntra.php">[ORDER_ID]</a> placed on [ORDER_DATE].<br/><br/>The bank had put your online payment transaction on hold. In order to proceed with the transaction, the bank required some extra documents from you. To get these documents and get the transaction approved, we tried calling you a few times. Unfortunately, we have not been able to reach you and hence regret to inform that your order has been cancelled. <br/><br/>However, if you would like to proceed with your order, you could place a Cash on Delivery order through our website (Myntra.com) or you could contact us at ([MYNTRA_CC_PHONE]) and we can help you expedite this process.<br/><br/>We sincerely regret the inconvenience this has caused to you. We value your patronage and the time you took to shop with us. We would like you to further continue shopping at Myntra.com.<br/><br/>Regards,<br/>Myntra Customer Support<br/>');
