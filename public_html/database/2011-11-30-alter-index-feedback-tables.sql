alter table mk_feedbacks add column created_date int, add column modified_date int, add column modifiedby varchar(150), add column emailto text default null;
alter table mk_feedbacks change id feedbackid int auto_increment;

alter table mk_feedback_questions change question question text, drop column details, add column mandatory tinyint;
create index mk_feedback_questions_feedbackid on mk_feedback_questions(feedbackid);
alter table mk_feedback_questions change id questionid int auto_increment;

alter table mk_feedback_options change id optionid int auto_increment, drop column option_img, drop column weightage, add column maxchars smallint;
create index mk_feedback_options_questionid on mk_feedback_options(questionid);

rename table mk_order_feedback to mk_feedback_instances;
alter table mk_feedback_instances change id instanceid int auto_increment, modify feedback_type int not null after instanceid, add column feedback_reference int not null after feedback_type, add column sent_date int after feedback_reference, add column customer_email varchar(200) not null, add column authentic_key varchar(250);
alter table mk_feedback_instances change recieved_date received_date int default null;
create index mk_feedback_instances_feedback_type on mk_feedback_instances(feedback_type);
create index mk_feedback_instances_feedback_reference on mk_feedback_instances(feedback_reference);

rename table mk_feedback_data to mk_feedback_instance_data;
alter table mk_feedback_instance_data change id dataid int auto_increment, change feedback_id feedback_instance_id int not null, drop column response_details;

alter table mk_feedbacks add column survey_email text default null, add column report_email text default null, add column selected_order_FB tinyint default 0, add column selected_SR_FB tinyint default 0, add column active tinyint default 1;

alter  table mk_feedback_questions add column active tinyint default 1;
alter  table mk_feedback_options add column active tinyint default 1;