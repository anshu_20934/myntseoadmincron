use myntra;
/*1.new table for text font color*/
CREATE TABLE `mk_text_font_color` (                
`id` int(11) unsigned NOT NULL auto_increment,     
`color` varchar(20) default NULL,                
PRIMARY KEY  (`id`),  
KEY `id` (`id`)                 
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

/*2.insert into color table*/
insert into `mk_text_font_color` (color) values('#000000');
insert into `mk_text_font_color` (color) values('#008080');
insert into `mk_text_font_color` (color) values('#00ffff');
insert into `mk_text_font_color` (color) values('#32cd32');
insert into `mk_text_font_color` (color) values('#556b2f');
insert into `mk_text_font_color` (color) values('#6b8e23');
insert into `mk_text_font_color` (color) values('#7fff00');
insert into `mk_text_font_color` (color) values('#87ceeb');
insert into `mk_text_font_color` (color) values('#90ee90');
insert into `mk_text_font_color` (color) values('#af2a2a');
insert into `mk_text_font_color` (color) values('#b0e0e6');
insert into `mk_text_font_color` (color) values('#cd5c5c');
insert into `mk_text_font_color` (color) values('#d8bfd8');
insert into `mk_text_font_color` (color) values('#e0ffff');
insert into `mk_text_font_color` (color) values('#f0fff0');
insert into `mk_text_font_color` (color) values('#f8f8ff');
insert into `mk_text_font_color` (color) values('#ff00ff');
insert into `mk_text_font_color` (color) values('#ffa07a');
insert into `mk_text_font_color` (color) values('#ffe4b5');
insert into `mk_text_font_color` (color) values('#fffacd');
insert into `mk_text_font_color` (color) values('#000080');
insert into `mk_text_font_color` (color) values('#008B8B');
insert into `mk_text_font_color` (color) values('#191970');
insert into `mk_text_font_color` (color) values('#40E0D0');
insert into `mk_text_font_color` (color) values('#5F9EA0');
insert into `mk_text_font_color` (color) values('#708090');
insert into `mk_text_font_color` (color) values('#7FFFD4');
insert into `mk_text_font_color` (color) values('#87CEFA');
insert into `mk_text_font_color` (color) values('#9370D8');
insert into `mk_text_font_color` (color) values('#A9A9A9');
insert into `mk_text_font_color` (color) values('#B22222');
insert into `mk_text_font_color` (color) values('#CD853F');
insert into `mk_text_font_color` (color) values('#DA70D6');
insert into `mk_text_font_color` (color) values('#E6E6FA');
insert into `mk_text_font_color` (color) values('#F0FFFF');
insert into `mk_text_font_color` (color) values('#FA8072');
insert into `mk_text_font_color` (color) values('#FF1493');
insert into `mk_text_font_color` (color) values('#FFA500');
insert into `mk_text_font_color` (color) values('#FFE4C4');
insert into `mk_text_font_color` (color) values('#FFFAF0');
insert into `mk_text_font_color` (color) values('#00008B');
insert into `mk_text_font_color` (color) values('#00BFFF');
insert into `mk_text_font_color` (color) values('#20B2AA');
insert into `mk_text_font_color` (color) values('#4169E1');
insert into `mk_text_font_color` (color) values('#6495ED');
insert into `mk_text_font_color` (color) values('#708090');
insert into `mk_text_font_color` (color) values('#800000');
insert into `mk_text_font_color` (color) values('#8A2BE2');
insert into `mk_text_font_color` (color) values('#9400D3');
insert into `mk_text_font_color` (color) values('#A9A9A9');
insert into `mk_text_font_color` (color) values('#B8860B');
insert into `mk_text_font_color` (color) values('#D2691E');
insert into `mk_text_font_color` (color) values('#DAA520');
insert into `mk_text_font_color` (color) values('#E9967A');
insert into `mk_text_font_color` (color) values('#F4A460');
insert into `mk_text_font_color` (color) values('#FAEBD7');
insert into `mk_text_font_color` (color) values('#FF4500');
insert into `mk_text_font_color` (color) values('#FFB6C1');
insert into `mk_text_font_color` (color) values('#FFE4E1');
insert into `mk_text_font_color` (color) values('#FFFAFA');
insert into `mk_text_font_color` (color) values('#0000CD');
insert into `mk_text_font_color` (color) values('#00CED1');
insert into `mk_text_font_color` (color) values('#228B22');
insert into `mk_text_font_color` (color) values('#4682B4');
insert into `mk_text_font_color` (color) values('#66CDAA');
insert into `mk_text_font_color` (color) values('#778899');
insert into `mk_text_font_color` (color) values('#800080');
insert into `mk_text_font_color` (color) values('#8B0000');
insert into `mk_text_font_color` (color) values('#98FB98');
insert into `mk_text_font_color` (color) values('#ADD8E6');
insert into `mk_text_font_color` (color) values('#BA55D3');
insert into `mk_text_font_color` (color) values('#D2B48C');
insert into `mk_text_font_color` (color) values('#DC143C');
insert into `mk_text_font_color` (color) values('#EE82EE');
insert into `mk_text_font_color` (color) values('#F5DEB3');
insert into `mk_text_font_color` (color) values('#FAF0E6');
insert into `mk_text_font_color` (color) values('#FF6347');
insert into `mk_text_font_color` (color) values('#FFC0CB');
insert into `mk_text_font_color` (color) values('#FFEFD5');
insert into `mk_text_font_color` (color) values('#FFFF00');
insert into `mk_text_font_color` (color) values('#0000FF');
insert into `mk_text_font_color` (color) values('#00FF00');
insert into `mk_text_font_color` (color) values('#2E8B57');
insert into `mk_text_font_color` (color) values('#483D8B');
insert into `mk_text_font_color` (color) values('#696969');
insert into `mk_text_font_color` (color) values('#778899');
insert into `mk_text_font_color` (color) values('#808000');
insert into `mk_text_font_color` (color) values('#8B008B');
insert into `mk_text_font_color` (color) values('#9932CC');
insert into `mk_text_font_color` (color) values('#ADFF2F');
insert into `mk_text_font_color` (color) values('#BC8F8F');
insert into `mk_text_font_color` (color) values('#D3D3D3');
insert into `mk_text_font_color` (color) values('#DCDCDC');
insert into `mk_text_font_color` (color) values('#F08080');
insert into `mk_text_font_color` (color) values('#F5F5DC');
insert into `mk_text_font_color` (color) values('#FDF5E6');
insert into `mk_text_font_color` (color) values('#FF69B4');
insert into `mk_text_font_color` (color) values('#FFD700');
insert into `mk_text_font_color` (color) values('#FFF0F5');
insert into `mk_text_font_color` (color) values('#FFFFE0');
insert into `mk_text_font_color` (color) values('#006400');
insert into `mk_text_font_color` (color) values('#00FF7F');
insert into `mk_text_font_color` (color) values('#2F4F4F');
insert into `mk_text_font_color` (color) values('#48D1CC');
insert into `mk_text_font_color` (color) values('#696969');
insert into `mk_text_font_color` (color) values('#7B68EE');
insert into `mk_text_font_color` (color) values('#808080');
insert into `mk_text_font_color` (color) values('#8B4513');
insert into `mk_text_font_color` (color) values('#9ACD32');
insert into `mk_text_font_color` (color) values('#AFEEEE');
insert into `mk_text_font_color` (color) values('#BDB76B');
insert into `mk_text_font_color` (color) values('#D3D3D3');
insert into `mk_text_font_color` (color) values('#DDA0DD');
insert into `mk_text_font_color` (color) values('#F0E68C');
insert into `mk_text_font_color` (color) values('#F5F5F5');
insert into `mk_text_font_color` (color) values('#FF0000');
insert into `mk_text_font_color` (color) values('#FF7F50');
insert into `mk_text_font_color` (color) values('#FFDAB9');
insert into `mk_text_font_color` (color) values('#FFF5EE');
insert into `mk_text_font_color` (color) values('#FFFFF0');
insert into `mk_text_font_color` (color) values('#008000');
insert into `mk_text_font_color` (color) values('#00FFFF');
insert into `mk_text_font_color` (color) values('#2F4F4F');
insert into `mk_text_font_color` (color) values('#4B0082');
insert into `mk_text_font_color` (color) values('#6A5ACD');
insert into `mk_text_font_color` (color) values('#7CFC00');
insert into `mk_text_font_color` (color) values('#8FBC8F');
insert into `mk_text_font_color` (color) values('#A0522D');
insert into `mk_text_font_color` (color) values('#B0C4DE');
insert into `mk_text_font_color` (color) values('#C0C0C0');
insert into `mk_text_font_color` (color) values('#D87093');
insert into `mk_text_font_color` (color) values('#DEB887');
insert into `mk_text_font_color` (color) values('#F0F8FF');
insert into `mk_text_font_color` (color) values('#F5FFFA');
insert into `mk_text_font_color` (color) values('#FF00FF');
insert into `mk_text_font_color` (color) values('#FF8C00');
insert into `mk_text_font_color` (color) values('#FFDEAD');
insert into `mk_text_font_color` (color) values('#FFF8DC');
insert into `mk_text_font_color` (color) values('#FFFFFF');

/*3.new table for text font type and size*/
CREATE TABLE `mk_text_font_detail` (                
`orientation_id` int(11) unsigned NOT NULL,     
`font_id` int(11) default NULL, 
`font_size` float(6,2) default NULL,
`maxchars` int default NULL,
`font_default` enum('Y','N') default 'N',
`size_default` enum('Y','N') default 'N',
PRIMARY KEY  (`orientation_id`,`font_id`,`font_size`)    
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

/*4.insert into mk_style_images tp get 75*75 for all styles*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1114','images/style/carea/T/laptop-yellow-1114.png','B','Y');
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1129','images/style/carea/T/gym-1129.jpg','B','Y');
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1121','images/style/carea/T/laptop-blue-1121.png','B','Y');
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1120','images/style/carea/T/laptop-green-1120.png','B','Y');
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1122','images/style/carea/T/laptop-red-1122.png','B','Y');
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1124','images/style/carea/T/nike-yellow-1124.png','B','Y');
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1127','images/style/carea/T/nike-black-1127.png','B','Y');
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1115','images/style/carea/T/premium-black-1115.png','B','Y');
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1128','images/style/carea/T/travel-1128.png','B','Y');

/*5.update for bags styles orientations*/
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='80',textColorDefault='80' where id=1598;
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='107',textColorDefault='107' where id=1599;
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='101',textColorDefault='101' where id=1600;
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='107',textColorDefault='107' where id=1601;
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='116',textColorDefault='116' where id=1602;
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='80',textColorDefault='80' where id=1603;
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='107',textColorDefault='107' where id=1604;
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='95',textColorDefault='95' where id=1605;
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='107',textColorDefault='107' where id=1606;

/*insert new fonts*/
insert into mk_fonts (id,name,image_name,ttf_name,display_order) values ('150','neonsign','neonsign.png','neonsign.ttf','1');
insert into mk_fonts (id,name,image_name,ttf_name,display_order) values ('151','leopold','leopold.png','leopold.ttf','2');
insert into mk_fonts (id,name,image_name,ttf_name,display_order) values ('152','brush','brush.png','brush.ttf','3');

/*6.insert into mk_text_font_detail*/
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1598','150','0.5','15','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1598','151','0.5','15','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1598','151','0.75','15','N','N');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1598','152','0.5','15','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1598','152','0.75','15','N','N');

insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1599','150','0.5','15','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1599','151','0.5','15','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1599','151','0.75','15','N','N');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1599','152','0.5','15','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1599','152','0.75','15','N','N');

insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1600','150','0.5','15','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1600','151','0.5','15','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1600','151','0.75','15','N','N');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1600','152','0.5','15','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1600','152','0.75','15','N','N');

insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1601','150','0.5','15','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1601','151','0.5','15','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1601','151','0.75','15','N','N');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1601','152','0.5','15','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1601','152','0.75','15','N','N');

insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1602','150','0.5','15','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1602','151','0.5','15','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1602','151','0.75','15','N','N');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1602','152','0.5','15','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1602','152','0.75','15','N','N');

insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1603','150','0.5','15','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1603','151','0.5','15','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1603','151','0.75','15','N','N');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1603','152','0.5','15','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1603','152','0.75','15','N','N');

insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1604','150','0.5','15','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1604','151','0.5','15','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1604','151','0.75','15','N','N');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1604','152','0.5','15','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1604','152','0.75','15','N','N');

insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1605','150','0.5','15','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1605','151','0.5','15','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1605','151','0.75','15','N','N');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1605','152','0.5','15','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1605','152','0.75','15','N','N');

insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1606','150','0.5','15','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1606','151','0.5','15','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1606','151','0.75','15','N','N');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1606','152','0.5','15','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1606','152','0.75','15','N','N');
