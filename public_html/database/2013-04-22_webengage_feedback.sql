insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('WebEngage.feedback.enabledPages', 'home,landing,pdp,search,cart,checkout,confirmation,mymyntra,static,other','Comma separated strings. Available pages: home,landing,pdp,search,cart,checkout,confirmation,mymyntra,static,other');

insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('WebEngage.feedback.regexpJSON', '{}','JSON of regexps for matching against page URL. Feedback widget will be shown only if the URL matches the regexp for the category. E.g. {pdp:/buy/i, search:/^((?!nike).)*$/i}');

insert into mk_abtesting_tests (name,ga_slot,seg_algo,source_type, enabled)
values('WebEngageFeedback',0,'abtest\\algos\\impl\\RandomSegmentationAlgo','tpl', true);

insert into mk_abtesting_variations (name,ab_test_id,percent_probability) 
values('test',(select id from mk_abtesting_tests where name='WebEngageFeedback'),0);
insert into mk_abtesting_variations (name,ab_test_id,percent_probability) 
values('control',(select id from mk_abtesting_tests where name='WebEngageFeedback'),100);