
insert into mk_return_status_details (code,display_name,mymyntra_status,mymyntra_msg,end_state,cust_end_state,display_order) values
('RQCP','CC Approved Return','ACCEPTED','Your return has been accepted by us. You will be issued a refund on this item within 24 hours.',0,0,5),
('RQCF','CC Declined Returns','REJECTED','Your return request has been declined as it did not meet our <a href="[GUIDE_LINES_LINK]" onclick="_gaq.push([\'_trackEvent\', \'myreturns\', \'view_returns_policy\', \'click\']);" target="_blank">Returns Policy</a>. We will contact you soon regarding this.',0,0,5);

update mk_return_status_details set mymyntra_status = 'RECEIVED',mymyntra_msg = 'We have received your return on [RECEIVED_DATE]. It will be processed by [PROCESSING_PROMISE_DATE].' where code = 'RQSF';

delete from mk_return_state_transitions where from_state = 'RQSF';

insert into mk_return_state_transitions (from_state,to_state,description,sla,admin_access,roles_allowed) values 
('RQCP','RFI','Return Refund Inititated',24,0,'CS,OP,AD'),
('RQCF','RRRS','Reshipping rejected returns',24,0,'OP,AD'),
('RQCF','RRJ','Disposed rejected returns',24,0,'OP,AD'),
('RQSF','RQCP','Return CC review pass',24,0,'CS,AD'),
('RQSF','RQCF','Return CC review fail',24,0,'CS,AD');

alter table xcart_returns add column ccreviewpassdate int(11);
alter table xcart_returns add column ccreviewfaildate int(11);
