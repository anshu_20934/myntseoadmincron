CREATE TABLE TelesalesSignup_log(
    id INTEGER(11) AUTO_INCREMENT UNIQUE KEY,
    userName VARCHAR(128) NOT NULL,
    telesalesUserName VARCHAR(128) NOT NULL,
    mobile VARCHAR(128) NOT NULL
);
