use myntra;
DROP TABLE IF EXISTS `mk_rpos_walkin_details`;
CREATE TABLE `mk_rpos_walkin_details` (
  `id` int(11) NOT NULL auto_increment,
  `walkin_time` int(11) default NULL,
  `login` varchar(255) default NULL,
  `storeid` int(11) default NULL,
  `is_valid` smallint(1) default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

alter table `xcart_orders` add column `payment_method_pos` varchar (255)   NULL;
alter table `xcart_order_details` add column `type_fulfillment_pos` varchar (255)   NULL;
DROP TABLE IF EXISTS `mk_pos_employee`;

CREATE TABLE `mk_pos_employee` (
  `emp_id` int(11) NOT NULL auto_increment,
  `emp_firstname` varchar(255) default NULL,
  `emp_lastname` varchar(255) default NULL,
  `store_id` int(11) default NULL,
  `is_active` tinyint(1) default '1',
  PRIMARY KEY  (`emp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `mk_pos_employee_attendance`;

CREATE TABLE `mk_pos_employee_attendance` (
  `emp_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `start_shift` int(11) default NULL,
  `end_shift` int(11) default NULL,
  `status` varchar(255) default NULL,
  PRIMARY KEY  (`emp_id`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `mk_pos_deposits`;

CREATE TABLE `mk_pos_deposits` (
  `id` int(11) NOT NULL auto_increment,
  `method` varchar(255) default NULL,
  `added_date` int(11) default NULL,
  `store_id` int(11) default NULL,
  `login_id` varchar(255) default NULL,
  `amount` decimal(12,2) default NULL,
  `status` tinyint(2) default NULL,
  `deposited_by` varchar(255) default NULL,
  `comments` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;