CREATE TABLE `mk_order_item_option_quantity` (
  `itemid` int(11) NOT NULL,
  `optionid` int(11) NOT NULL,
  `quantity` int(11) default '0',
  KEY `mk_order_item_option_quantity_itemid_index` (`itemid`),
  KEY `mk_order_item_option_quantity_option_index` (`optionid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;