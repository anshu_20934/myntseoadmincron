CREATE TABLE `mk_customer_address` (
  `id` INT(8) NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(255) NOT NULL,
  `default_address` INT(11) NOT NULL DEFAULT '0',
  `name` VARCHAR(255) NOT NULL,
  `address` TEXT NOT NULL,
  `city` VARCHAR(255) NOT NULL,
  `state` VARCHAR(255) NOT NULL,
  `country` VARCHAR(255) NOT NULL,
  `pincode` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `mobile` VARCHAR(48) NOT NULL,
  `phone` VARCHAR(48) NOT NULL,
  `datecreated` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MYISAM AUTO_INCREMENT=80 DEFAULT CHARSET=latin1


INSERT INTO mk_customer_address(SELECT NULL, login, 1,  CONCAT(s_firstname, " ", s_lastname), s_address, s_city, s_state, s_country, s_zipcode, email, mobile, phone, NOW() FROM xcart_customers
WHERE
s_firstname IS NOT NULL AND TRIM(s_firstname) != '' AND
s_lastname IS NOT NULL AND TRIM(s_lastname) != '' AND
s_address IS NOT NULL AND TRIM(s_address) != '' AND
s_city IS NOT NULL AND TRIM(s_city) != '' AND
s_state IS NOT NULL AND TRIM(s_state) != ''  AND
s_country IS NOT NULL AND s_country != '' AND
s_zipcode IS NOT NULL AND TRIM(s_zipcode) != '' AND
email IS NOT NULL AND TRIM(email) != '' AND
(mobile IS NOT NULL AND TRIM(mobile) != '' OR
phone IS NOT NULL AND TRIM(phone) != ''));

