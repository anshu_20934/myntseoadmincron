drop table if exists order_oh_reasons_master;

create table order_oh_reasons_master(
id int not null AUTO_INCREMENT,
reason_code varchar(5) not null,
reason_desc varchar(255) not null,
is_manual_change tinyint(2),
last_updated_time datetime,
PRIMARY KEY (id),
Unique Key occ_unq_key(reason_code)
)ENGINE=InnoDb;


insert into order_oh_reasons_master (reason_code,reason_desc,is_manual_change) values
('ASR','Awaiting Stock Replenishment',1),
('FRI','Undergoing Fraud Investigation',1),
('CVP','COD - RTO verification pending',0);

alter table xcart_orders add column (on_hold_reason_id_fk int);

alter table mk_order_action_reasons add column (parent_reason varchar(100));
alter table mk_order_action_reasons add column (is_internal_reason tinyint(2) not null default 0);
insert into mk_order_action_reasons (action,reason,reason_display_name,parent_reason,is_internal_reason) values
('oh_disposition','CQ','Contacted - Queue',null,0),
('oh_disposition','CR','Contacted - Reject',null,0),
('oh_disposition','NR','Not contactable',null,0),
('oh_disposition','AV','Correct Address Verified','CQ',0),
('oh_disposition','NAV','Incorrect Address - New address Verified','CQ',0),
('oh_disposition','IPO','Incorrect Product Ordered','CR',0),
('oh_disposition','CNU','Coupon Not Used','CR',0),
('oh_disposition','DO','Duplicate Order','CR',0),
('oh_disposition','NOP','New Order Placed','CR',0),
('oh_disposition','COS','Cm Out of Station','CR',0),
('oh_disposition','IA','Incorrect Address','CR',0),
('oh_disposition','INN','Invalid Number','NR',0),
('oh_disposition','WRN','Wrong Number','NR',0),
('oh_disposition','BUSY','Busy','NR',0),
('oh_disposition','NRC','Not Reachable','NR',0),
('oh_disposition','NRS','No Response','NR',0),
('oh_disposition','MSO','Switched off','NR',0),
('oh_disposition','SPM','State-PIN code mismatch',null,1),
('oh_disposition','NC','New customer with no completed orders (no orders in DL/C)',null,1),
('oh_disposition','LNRTO','One of last n orders is RTO',null,1),
('oh_disposition','OHRTO','One of last n orders is put on hold via cod oh tracker and resulted in RTO',null,1),
('oh_disposition','OHF','Last order called from this tracker + rejected',null,1),
('oh_disposition','NINC','Last n orders incomplete',null,1),
('oh_disposition','OH','On Hold',null,1);


drop table if exists cod_oh_orders_dispostions;
drop table if exists cod_oh_orders_dispostions_log;
create table cod_oh_orders_dispostions(
id int not null AUTO_INCREMENT,
orderid int not null,
disposition varchar(50),
reason varchar(100),
comment text,
created_by varchar(100),
trial_number int not null default 0,
created_time datetime,
PRIMARY KEY (id),
Unique Key cod_oh_disp_unq_key(orderid)
)ENGINE=InnoDb;


create table cod_oh_orders_dispostions_log(
id int not null AUTO_INCREMENT,
refid int not null,
orderid int not null,
disposition varchar(50),
reason varchar(100),
created_by varchar(100),
comment text,
trial_number int not null default 0,
created_time datetime,
logged_time datetime,
PRIMARY KEY (id)
)ENGINE=InnoDb;

insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('conditionalCodOnHold','true','true or false');
insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('conditionalCodOnHold.maxNoOfTrials','3','Number of trials to contact an oh customer');
insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('conditionalCodOnHold.RTOCheckToDisableCOD','true','true or false');
insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('conditionalCodOnHold.CustomerRTOLimit','3','No of RTOs allowed per user to have cod option');
insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('conditionalCodOnHold.TrackedAndRTOLimit','2','No of RTOs allowed after tracker calling per user to have cod option');

insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('condCODOH.pincode','true','true or false');
insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('condCODOH.nocompleteorders','false','true or false');
insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('condCODOH.anyoflastnordersrto','true','true or false');
insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('condCODOH.lastnordersincomplete','true','true or false');
insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('condCODOH.anyoflastnordersohtrackedrto','true','true or false');
insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('condCODOH.trackedandrejected','true','true or false');
insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('condCODOH.trackedandrejected.nooforders','2','No of Orders among which one is oh tracked and rejected');
insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('condCODOH.anyoflastnordersrto.nooforders','2','No of Orders among which one is rto');
insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('condCODOH.lastnordersincomp.nooforders','2','No of Incomplete Orders');
insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('condCODOH.trackedandrto.nooforders','2','No of Orders among which one is tracked and rtoed');

update cancellation_codes set email_content = "We regret to inform you that your order has been rejected due to a mismatch between the PIN code and state provided in your mailing address. The details of the cancelled order (Order No. [ORDER_ID]) are as below:" where cancel_type  = 'CCC' and cancel_reason = 'ADDPINMM';

update cancellation_codes set email_content = "We regret to inform you that your order has been rejected owing to our inability to resolve this to a physical mailing address. We understand that in cases our understanding may be incorrect. If you believe this is the case, we apologize for the inconvenience and request you to call our Customer Care to place this order again. The details of this order (Order No. [ORDER_ID]) are as below:" where cancel_type  = 'CCC' and cancel_reason = 'INVADD';


alter table cancellation_codes add column (cancellation_mode varchar(100));
update  cancellation_codes set cancellation_mode = 'PART_CANCELLATION',last_updated_time = now() where is_part_cancellation = 1;
update  cancellation_codes set cancellation_mode = 'FULL_CANCELLATION',last_updated_time = now() where is_part_cancellation = 0;

insert into cancellation_codes (cancel_type,cancel_type_desc,cancel_reason,cancel_reason_desc,email_content,cancellation_mode) values 
('NR','Not Reachable','NR','All NR types',"We tried reaching you a number of times for confirming your COD order, but were unable to make contact. As per our process, we would not be able to process your order further and are therefore cancelling it. Please accept our apologies for any inconvenience caused. If you wish to continue with this order, kindly place the order again and do provide us your contact details where we may contact you in case of any requirement.Please find below the details of the cancelled order (Order No. [ORDER_ID]):",'OH_CANCELLATION'),
('CR','Customer Reject','IPO','Incorrect Product Ordered',"As per your request, We are going ahead with the cancellation of your Order No. [ORDER_ID], the details of which are given below:",'OH_CANCELLATION'),
('CR','Customer Reject','CNU','Coupon Not Used',"As per your request, We are going ahead with the cancellation of your Order No. [ORDER_ID], the details of which are given below:",'OH_CANCELLATION'),
('CR','Customer Reject','DO','Duplicate Order',"As per your request, We are going ahead with the cancellation of your Order No. [ORDER_ID], the details of which are given below:",'OH_CANCELLATION'),
('CR','Customer Reject','NOP','New Order Placed',"As per your request, We are going ahead with the cancellation of your Order No. [ORDER_ID], the details of which are given below:",'OH_CANCELLATION'),
('CR','Customer Reject','COS','Cm Out of Station',"As per your request, We are going ahead with the cancellation of your Order No. [ORDER_ID], the details of which are given below:",'OH_CANCELLATION'),
('CR','Customer Reject','IA','Incorrect Address',"We regret to inform you that your order has been rejected owing to our inability to resolve this to a physical mailing address. We understand that in cases our understanding may be incorrect. If you believe this is the case, we apologize for the inconvenience and request you to call our Customer Care to place this order again. The details of this order (Order No. [ORDER_ID]) are as below:",'OH_CANCELLATION');

insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('CODOH_CALLING_MAIL_1','true','true or false');
insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('CODOH_CALLING_MAIL_2','true','true or false');
insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('CODOH_CALLING_MAIL_3','true','true or false');

delete from mk_email_notification where name in ('CODOH_CALLING_MAIL_1','CODOH_CALLING_MAIL_2','CODOH_CALLING_MAIL_3');

insert into mk_email_notification(name, subject, body) values('CODOH_CALLING_MAIL_1', 'Regarding confirmation of your order: [ORDER_ID] on Myntra - We are trying to reach you!',
'Dear [USER],
<p>Greetings from Myntra!</p><br/>
<p>Your order: [ORDER_ID] that was placed on [ORDER_DATE] needs to be confirmed before we can proceed with processing it further.</p><br/>
We tried contacting you on your mobile number provided in your shipping address but did not get a response. We will attempt to call you again later today or early tomorrow and we request you to accept our call.</p><br/>
Alternatively, you can call Myntra Customer Connect at [MYNTRA_CC_PHONE] to enable us to process your order.</p><br/>
Please accept our sincere apologies for the inconvenience caused. We value your association with Myntra and thank you for your support and co-operation.</p><br/>
<br/>
Regards,<br/>
Myntra Customer Care');

insert into mk_email_notification(name, subject, body) values('CODOH_CALLING_MAIL_2', 'Regarding confirmation of your order: [ORDER_ID] on Myntra - We are trying to reach you!',
'Dear [USER],
<p>Greetings from Myntra!</p>
<p>With regards to confirmation of your order: [ORDER_ID] placed on [ORDER_DATE], we attempted to call you again but were unable to reach you.<br/>
Please do make yourself available or contact us at [MYNTRA_CC_PHONE] within the next 24 hours to confirm your shipping address and contact information, failing which your order may be canceled.</p>
<p>We value your association with Myntra and thank you for your support and co-operation.</p>
<br/>
Regards,<br/>
Myntra Customer Care');

insert into mk_email_notification(name, subject, body) values('CODOH_CALLING_MAIL_3', 'Your order: [ORDER_ID] on Myntra has been cancelled',
'Dear [USER], 
<p>Greetings from Myntra!</p>
<p>We regret to inform you that we tried reaching you now for the third and final time, regarding verification of your order: [ORDER_ID] (placed on [ORDER_DATE]), but were unable to reach you.</p>
As per our process, your order has now been cancelled.</p>
<p>We value your association with Myntra and thank you for your support and co-operation.</p>
<br/>
Regards,<br/>
Myntra Customer Care');




