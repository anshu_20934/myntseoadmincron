update mk_email_notification set body='Dear [USER],
<br/>
<p>Thank you for placing an order with Myntra.com. Your order details are following:<br>

	<table style="width:95%">
		<tr style="background-color:#d9d9d9;">
			<th width="45%">Product - size(addon)</th>
			<th width="5%">Quantity</th>
			<th width="15%">Unit Price (INR)</th>
			<th width="20%">designer markup (INR)</th>
			<th width="15%">Total (INR)</th>
		</tr>
		[PRODUCTDETAIL]
		<tr><td colspan="5"><hr/></td></tr>
		<tr>
			<td colspan="4"	 align="right">Subtotal</td>
			<td align="right">[SUBTOTAL]</td>
		</tr>
		<tr>
			<td colspan="4"	 align="right">Discount</td>
			<td align="right">[DISCOUNT]</td>
		</tr>
		<tr>
			<td colspan="4"	 align="right">Tax</td>
			<td align="right">[TAX]</td>
		</tr>
		<tr>
			<td colspan="4"	 align="right">Shipping</td>
			<td align="right">[SHIPPINGCHARGE]</td>
		</tr>
		<tr>
			<td colspan="4"	 align="right">Gift Wrapping Charges</td>
			<td align="right">[GIFTWRAPPERCHARGE]</td>
		</tr>
		<tr><td colspan="5"><hr/></td></tr>
		<tr style="background-color:#ffffcc;">
			<td colspan="4"	 align="right"><b>Grand Total (Rs)</b></td>
			<td align="right"><b>[GRANDTOTAL]</b></td>
		</tr>
		<tr><td colspan="5"><hr/></td></tr>
	</table>
</p>	
<br/>
<p>This is to confirm that we have received your order and your order ID is <b>[ORDERID]</b>. You have opted Cash on Delivery mode of payment. <br/><br/>

Consider the following:<br/>
1. Please have enough ready cash at delivery address to make the payment.<br/>
2. For COD option, we accept only Cash. Please do NOT offer a cheque or dd to the courier staff.<br/>
3. Please do NOT pay for any additional charges i.e. octroi etc. to courier staff. Your invoice amount is inclusive of all charges.<br/>
4. <b>Cancelling your COD order : </b>Please do so within 10 hours of receiving this email, post which it will processed and shipped. <br/>
&nbsp;&nbsp;&nbsp;
   If you want to cancel the order, please get in touch with Customer Care at +91-80-43541901/03/19 (Mon-Sat: 9:00AM to 9:00PM IST) or email at support@myntra.com.<br/>

<br/>
Your order is currently being processed. It will be delivered at your doorstep by [DELIVERY_DATE]. <br/>
Once your order is shipped, you will receive a mail from us giving the tracking details for the order.<br/><br/>

<p>Thanks</p>
<p>Myntra Customer Support</p>
<br/><br/><br/>
<p style="color:#0000ff">
	<i>------------------------------------------------------------------------------------------------------------------------------------------<br>
	This mail is intended only for the person or entity to which it is addressed and may contain confidential and/or privileged
	information. Any review, retransmission, dissemination or other use of, or taking of any action in reliance upon,
	this information by persons or entities other than the intended recipient is prohibited.
	If you received this in error, please contact the sender and delete the material from any system.
	</i>
</p>'
where name='cod_confirm_order' and messageid>0;