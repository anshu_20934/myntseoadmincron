insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('homepage.bannerCycle.interval', 5000, 'Interval (in milliseconds) between banner image transition on homepage.');

insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('landingpages.bannerCycle.interval', 5000, 'Interval (in milliseconds) between banner image transition on landing pages.');
