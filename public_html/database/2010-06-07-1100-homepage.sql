CREATE TABLE `mk_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `url` varchar(2048) DEFAULT NULL,
  `image` varchar(2048) DEFAULT NULL,
  `banner_set` varchar(255) DEFAULT NULL,
  `alt` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `enabled` int(11) DEFAULT NULL,
  `datecreated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

CREATE TABLE `mk_testimonial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(2048) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `enabled` datetime DEFAULT NULL,
  `datecreated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `mk_tabs` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(255) DEFAULT NULL,
 `display_order` int(11) DEFAULT NULL,
 `enabled` int(11) DEFAULT NULL,
 `datecreated` datetime DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

CREATE TABLE `mk_tab_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tab_id` varchar(255) DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `alt` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `enabled` int(11) DEFAULT NULL,
  `datecreated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 ;

CREATE TABLE `mk_config` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `property` varchar(255) DEFAULT NULL,
 `value` text DEFAULT NULL,
 `enabled` int(11) DEFAULT NULL,
 `datecreated` datetime DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
