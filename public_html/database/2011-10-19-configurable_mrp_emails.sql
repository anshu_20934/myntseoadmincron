update mk_email_notification set subject='Rs. [MRP_AMOUNT] credited to your Myntra account' where name='mrp_ref_registration';
update mk_email_notification set subject='Rs. [MRP_AMOUNT] credited to your Myntra account' where name='mrp_referral_successful';
update mk_email_notification set subject='[NAME-REFERER] has gifted you Rs. [MRP_AMOUNT]* to shop.' where name='mrp_referral_invitation';

update mk_email_notification set body='<div style="width:615px; height: 300px;">
  <div style="float:left;margin-top:0px; padding:0px;">
 <a href="http://www.myntra.com" target="_blank" style="outline:none">
 <img src="http://myntramailer.s3.amazonaws.com/online/left-banner-refferal-reg-mailer.jpg" width="314" height="325" alt="mynt club | Rewards and loyalty program" title="mynt club | Rewards and loyalty program" style="text-decoration:none;border:none;font-family:Arial,Helvetica,sans-serif;color:#666;font-size:12px;"/>
 </a>
  </div>
    <div style="float:right;margin-top:0px; padding:0px;">
 <a href="http://www.myntra.com" target="_blank" style="outline:none">
 <img src="http://myntramailer.s3.amazonaws.com/online/right-banner-refferal-reg-mailer.jpg" width="288" height="231" alt="Congratulations! Rs [MRP_AMOUNT] credited to your Mynt Club account!" title="Congratulations! Rs [MRP_AMOUNT] credited to your Mynt Club account!" style="text-decoration:none;border:none;font-family:Arial,Helvetica,sans-serif;color:#666;font-size:12px;"/>
 </a>
  </div>
      <div style="width: 240px;float:right;margin-right:45px; margin-top:5px;padding:0px;line-height:20px; background-color:#EBE7E6;border:0px solid red; height:70px; ">
 <span style="float:left;width:230px; margin-top: 5px; margin-left: 5px;color:color; font-family: Arial,Helvetica,sans-serif; font-size: 14px; ">[REFERRED] has accepted<br>your invitation and registered on <br> Myntra.com. <a href="[MRPLANDINGPAGE]">Know more</a>.</span> 
  </div>
   </div>
    <div style="margin-top:0px; padding:0px; width: 615px;height: 136px;">
 <a href="[REFERANDEARNURL]" target="_blank" style="outline:none">
 <img src="http://myntramailer.s3.amazonaws.com/online/bottom-banner-refferal-reg-mailer.jpg" width="615" height="102" alt="Get Rs [REF_AMOUNT] on the first purchase of each of your friends! Refer and earn now!" title="Get Rs 1000 on the first purchase of each of your friends! Refer and earn now!" style="text-decoration:none;border:none;font-family:Arial,Helvetica,sans-serif;color:#666;font-size:12px;"/>
 </a>
 <img src="http://www.email-tracking-with-google-analytics.com/track/t4741.gif" border=0>
 </div>' where name='mrp_ref_registration';

 update mk_email_notification set body='<div style="width:615px; height: 325px;">
  <div style="float:left;margin-top:0px; padding:0px;">
 <a href="http://www.myntra.com" target="_blank" style="outline:none">
 <img src="http://myntramailer.s3.amazonaws.com/online/left-banner-first-purchase-mailer.jpg" width="314" height="325" alt="mynt club | Rewards and loyalty program" title="mynt club | Rewards and loyalty program" style="text-decoration:none;border:none;font-family:Arial,Helvetica,sans-serif;color:#666;font-size:12px;"/>
 </a>
  </div>
    <div style="float:right;margin-top:0px; padding:0px;">
 <a href="http://www.myntra.com" target="_blank" style="outline:none">
 <img src="http://myntramailer.s3.amazonaws.com/online/right-banner-first-purchase-mailer.jpg" width="290" height="231" alt="Congratulations! Rs [MRP_AMOUNT] credited to your Mynt Club account!" title="Congratulations! Rs [MRP_AMOUNT] credited to your Mynt Club account!" style="text-decoration:none;border:none;font-family:Arial,Helvetica,sans-serif;color:#666;font-size:12px;"/>
 </a>
  </div>
      <div style="width: 240px;float:right;margin-right:45px; margin-top:5px;padding:0px;line-height:20px; background-color:#EBE7E6;border:0px solid red; height:70px; ">
 <span style="float:left;width:230px; margin-top: 5px; margin-left: 5px;color:color; font-family: Arial,Helvetica,sans-serif; font-size: 14px; ">Your friend, [REFERRED], has made first purchase on Myntra.com<br><a href="[MRPLANDINGPAGE]">Know more</a>.</span> 
  </div>
   </div>
    <div style="margin-top:0px; padding:0px; width: 615px;height: 102px;">
 <a href="[REFERANDEARNURL]" target="_blank" style="outline:none">
 <img src="http://myntramailer.s3.amazonaws.com/online/bottom-banner-first-purchase-mailer.jpg" width="615" alt="Invite more friends to earn more! Refer and earn now!" title="Invite more friends to earn more! Refer and earn now!" style="text-decoration:none;border:none;font-family:Arial,Helvetica,sans-serif;color:#666;font-size:12px;"/>
 </a>
 <img src="http://www.email-tracking-with-google-analytics.com/track/t4740.gif" border=0>
</div>' where name='mrp_referral_successful';

update mk_email_notification set body='<div style="border:1px solid gray;width:615px;">
<div style="margin-top:0px; padding:0px;">
 <a href="[MRPLANDINGPAGE]" target="_blank" style="outline:none">
 <img src="http://myntramailer.s3.amazonaws.com/online/mynt-club-welcome-banner-nonfb.jpg" width="615" height="453" alt="You are now a privileged member of the Mynt Club. You have received a credit of Rs [MRP_AMOUNT] for your first purchase! Click here to learn more." title="You are now a privileged member of the Mynt Club. You have received a credit of Rs [MRP_AMOUNT] for your first purchase! Click here to learn more." style="text-decoration:none;border:none;font-family:Arial,Helvetica,sans-serif;color:#666;font-size:12px;"/>
 </a>
  </div>
 <table background="http://myntramailer.s3.amazonaws.com/july2011/ymyntra-background.jpg" height="180" width="615">
 <tbody><tr><td><div style="margin-top:0px;height:200px;width:600px;line-height:0px;font-family:Arial,Helvetica,sans-serif;color:#000;font-size:15px;list-style-type:circle; list-style-align:center;" align="center" background="http://myntramailer.s3.amazonaws.com/july2011/ymyntra-background.jpg">
 <div style="padding-top:40px;line-height:27px"> 
 <img src="http://myntramailer.s3.amazonaws.com/july2011/bullet-style.jpg" width="10" height="11" alt="" title="" /><strong> Largest in-season catalog</strong> of sports and casual wear<br>
 <img src="http://myntramailer.s3.amazonaws.com/july2011/bullet-style.jpg" width="10" height="11" alt="" title="" /><strong> More than 50 brands of</strong> footwear, apparel &amp; accessories<br>
 <img src="http://myntramailer.s3.amazonaws.com/july2011/bullet-style.jpg" width="10" height="11" alt="" title="" /><strong> Free shipping</strong> throughout the country<br>
  <img src="http://myntramailer.s3.amazonaws.com/july2011/bullet-style.jpg" width="10" height="11" alt="" title="" /> Customer friendly<strong> Returns policy</strong><br>
 <img src="http://myntramailer.s3.amazonaws.com/july2011/bullet-style.jpg" width="10" height="11" alt="" title="" /> Pay<strong> Cash on Delivery</strong><br>
 <img src="http://myntramailer.s3.amazonaws.com/july2011/bullet-style.jpg" width="10" height="11" alt="" title="" /> 24x7<strong> Customer support</strong><br>
 </div>
 </div>
 </td></tr></tbody></table>
   <div style="margin-top:5px; padding:0px;">
 <a href="[REFERANDEARNURL]" target="_blank" style="outline:none">
 <img src="http://myntramailer.s3.amazonaws.com/online/mynt-club-welcome-banner2.jpg" width="615" height="155" alt="Refer and earn more. Get Rs [REG_AMOUNT] for every friend who registers, and Rs [REF_AMOUNT] on the first time each of them buys on Myntra! Click here to learn more" title="Refer and earn more. Get Rs [REG_AMOUNT] for every friend who registers, and Rs [REF_AMOUNT] on the first time each of them buys on Myntra! Click here to learn more" style="text-decoration:none;border:none;font-family:Arial,Helvetica,sans-serif;color:#666;font-size:12px;"/>
 </a>
 </div>
  <div style="margin-top:0px; padding:0px;">
 <img src="http://myntramailer.s3.amazonaws.com/online/mynt-club-welcome-banner-nonfb3.jpg" width="615" height="74" alt="mynt club | Rewards and loyalty program" title="mynt club | Rewards and loyalty program" style="text-decoration:none;border:none;font-family:Arial,Helvetica,sans-serif;color:#666;font-size:12px;"/>
 <img src="http://www.email-tracking-with-google-analytics.com/track/t4738.gif" border=0>
 </div>' where name='mrp_first_login';
 
 update mk_email_notification set body='<div style="border:1px solid gray;width:615px;">
  <div style="margin-top:0px; padding:0px;">
 <a href="[MRPLANDINGPAGE]" target="_blank" style="outline:none">
 <img src="http://myntramailer.s3.amazonaws.com/online/mynt-club-welcome-banner.jpg" width="615" height="452" alt="You are now a privileged member of the Mynt Club. You have received a credit of Rs [MRP_AMOUNT] for your first purchase! Click here to learn more." title="You are now a privileged member of the Mynt Club. You have received a credit of Rs [MRP_AMOUNT] for your first purchase! Click here to learn more." style="text-decoration:none;border:none;font-family:Arial,Helvetica,sans-serif;color:#666;font-size:12px;"/>
 </a>
  </div>
  <table background="http://myntramailer.s3.amazonaws.com/july2011/ymyntra-background.jpg" height="180" width="615">
 <tbody><tr><td><div style="margin-top:0px;height:200px;width:600px;line-height:0px;font-family:Arial,Helvetica,sans-serif;color:#000;font-size:15px;list-style-type:circle; list-style-align:center;" align="center" background="http://myntramailer.s3.amazonaws.com/july2011/ymyntra-background.jpg">
 <div style="padding-top:40px;line-height:27px"> 
 <img src="http://myntramailer.s3.amazonaws.com/july2011/bullet-style.jpg" width="10" height="11" alt="" title="" /><strong> Largest in-season catalog</strong> of sports and casual wear<br>
 <img src="http://myntramailer.s3.amazonaws.com/july2011/bullet-style.jpg" width="10" height="11" alt="" title="" /><strong> More than 50 brands of</strong> footwear, apparel &amp; accessories<br>
 <img src="http://myntramailer.s3.amazonaws.com/july2011/bullet-style.jpg" width="10" height="11" alt="" title="" /><strong> Free shipping</strong> throughout the country<br>
  <img src="http://myntramailer.s3.amazonaws.com/july2011/bullet-style.jpg" width="10" height="11" alt="" title="" /> Customer friendly<strong> Returns policy</strong><br>
 <img src="http://myntramailer.s3.amazonaws.com/july2011/bullet-style.jpg" width="10" height="11" alt="" title="" /> Pay<strong> Cash on Delivery</strong><br>
 <img src="http://myntramailer.s3.amazonaws.com/july2011/bullet-style.jpg" width="10" height="11" alt="" title="" /> 24x7<strong> Customer support</strong><br>
 </div>
 </div>
 </td></tr></tbody></table>
   <div style="margin-top:5px; padding:0px;">
 <a href="[REFERANDEARNURL]" target="_blank" style="outline:none">
 <img src="http://myntramailer.s3.amazonaws.com/online/mynt-club-welcome-banner2.jpg" width="615" height="155" alt="Refer and earn more. Get Rs [REG_AMOUNT] for every friend who registers, and Rs [REF_AMOUNT] on the first time each of them buys on Myntra! Click here to learn more" title="Refer and earn more. Get Rs [REG_AMOUNT] for every friend who registers, and Rs [REF_AMOUNT] on the first time each of them buys on Myntra! Click here to learn more" style="text-decoration:none;border:none;font-family:Arial,Helvetica,sans-serif;color:#666;font-size:12px;"/>
 </a>
  </div>
    <div style="margin-top:0px; padding:0px;">
 <img src="http://myntramailer.s3.amazonaws.com/online/mynt-club-welcome-banner3.jpg" width="615" height="75" alt="mynt club | Rewards and loyalty program" title="mynt club | Rewards and loyalty program" style="text-decoration:none;border:none;font-family:Arial,Helvetica,sans-serif;color:#666;font-size:12px;"/>
<img src="http://www.email-tracking-with-google-analytics.com/track/t4739.gif" border=0>
 </div>' where name='mrp_fbfirst_login';
 
update mk_email_notification set body='<div style="border:1px solid gray;width:615px;">
<div style="width:615px; height: 365px;">
  <div style="margin:0px; padding:0px;line-height:0px; width: 316px; float:left; height:343px;">
	<a href="[INVITATIONLINK]" style="text-decoration:none; color: #a40067; outline: none;" target="_blank">
	<img src="http://myntramailer.s3.amazonaws.com/july2011/invitation-mailer-leftbanner.jpg" width="322" height="362" alt="Mynt Club at Myntra.com" title="Mynt Club at Myntra.com" style="text-decoration:none;border:none;font-family:Arial,Helvetica,sans-serif;color:#666;font-size:12px;"/>
	</a>
	</div>
<div style="line-height:0px; width: 293px; height:55px; float:right; ">
	<img src="http://myntramailer.s3.amazonaws.com/july2011/invitation-mailer-upper-bubble.jpg" width="293" height="20" alt="Accept Invitation" title="Accept Invitation" style="text-decoration:none;border:none;font-family:Arial,Helvetica,sans-serif;color:#666;font-size:12px;"/>
	</div>
  <div style="width: 259px; height:20px; float:right; font-family:Arial,Helvetica,sans-serif;color:#000;font-size:16px; margin-top: 0px;">
	
	<span style="font-family:Arial,Helvetica,sans-serif;color:#000;font-size:14px;"><a href="[INVITATIONLINK]" style="text-decoration:none; color: #000;" target="_blank"> Your friend, [NAME-REFERER] has invited you to join Myntra.com</a></span>
  </div>
  <div style="width: 259px; height:170px; float: right; padding-top: 25px;">
	<p style="font-family:Arial narrow,Helvetica,sans-serif;font-size:18px; color: #917f7d; font-weight: bold; line-height: 0px;">
	<a href="[INVITATIONLINK]" style="text-decoration:none; color: #917f7d;" target="_blank">
	BECOME A MEMBER NOW!</a></p>
	
	<p style="font-family:Arial narrow,Helvetica,sans-serif;font-size:30px; color: #a40067; font-weight: bold; margin-top:30px; line-height: 35px;">
	<a href="[INVITATIONLINK]" style="text-decoration:none; color: #a40067;" target="_blank">
	EARN RS. [MRP_AMOUNT]* AS SOON AS YOU SIGN UP</a></p>
	
	</div>
  <div style="width: 259px; height:50px; float: right; ">
	<div style="width: 160px; height:25px;;background-color: #b90147; color: #ffffff; font-size: 15px; -moz-border-radius: 5px; -webkit-border-radius: 5px;" align="center">
	<span style="padding-top:15px; line-height: 25px;">
	<a href="[INVITATIONLINK]" style="text-decoration:none; color: #ffffff;" target="_blank">ACCEPT INVITATION</a></span></div>
	</div>
 
</div>
<div style="width: 615px;margin-top:12px;">
<table background="http://myntramailer.s3.amazonaws.com/july2011/ymyntra-background1.jpg" height="180" width="600">
 <tbody><tr><td><div style="margin-top:0px;height:200px;width:600px;line-height:0px;font-family:Arial,Helvetica,sans-serif;color:#000;font-size:15px;list-style-type:circle; list-style-align:center;" align="center" background="http://myntramailer.s3.amazonaws.com/july2011/ymyntra-background.jpg">
 <div style="padding-top:10px; padding-bottom:10px;font-family:Arial narrow,Helvetica,sans-serif;font-size:20px; color: #ff4c1b; font-weight: bold; margin-top:0px; line-height: 15px; ">
	
	WHY MYNTRA?</div> <BR>
 <div style="padding-top:0px;line-height:27px"> 
   <img src="http://myntramailer.s3.amazonaws.com/july2011/bullet-style.jpg" width="10" height="11" alt="" title="" /><strong> Largest in-season catalog</strong> of fashion and lifestyle wear<br>
 <img src="http://myntramailer.s3.amazonaws.com/july2011/bullet-style.jpg" width="10" height="11" alt="" title="" /><strong> More than 50 brands of</strong> footwear, apparel &amp; accessories<br>
 <img src="http://myntramailer.s3.amazonaws.com/july2011/bullet-style.jpg" width="10" height="11" alt="" title="" /> <strong>Free shipping</strong> throughout the country<br>
  <img src="http://myntramailer.s3.amazonaws.com/july2011/bullet-style.jpg" width="10" height="11" alt="" title="" /> Customer friendly<strong> Returns policy</strong><br>
 <img src="http://myntramailer.s3.amazonaws.com/july2011/bullet-style.jpg" width="10" height="11" alt="" title="" /> Pay<strong> Cash on Delivery</strong><br>
 <img src="http://myntramailer.s3.amazonaws.com/july2011/bullet-style.jpg" width="10" height="11" alt="" title="" /> 24x7<strong> Customer support</strong><br>
 </div>
 </div>
 </td></tr></tbody></table></div>
 <div style="margin-top:0px; width:615px;line-height:0px;font-family:Arial,Helvetica,sans-serif;color:#322725;font-size:12px;" >
<img src="http://myntramailer.s3.amazonaws.com/july2011/invitation-mailer-brands-banner.jpg" width="615" height="57" alt="Brands at Myntra.com: Nike, Adidas, Puma, Reebok, Wrangler, Lee and many more.." title="Brands at Myntra.com: Nike, Adidas, Puma, Reebok, Wrangler, Lee and many more.." style="text-decoration:none;border:none;font-family:Arial,Helvetica,sans-serif;color:#666;font-size:12px;"/>
</div>
<img src="http://www.email-tracking-with-google-analytics.com/track/t4737.gif" border=0>
</div>' where name='mrp_referral_invitation';

alter table mk_feature_gate_key_value_pairs modify column value varchar(1000);

update mk_email_notification set body='<html>
<body>
<p>Hi,</p>
<p>Thanks for taking time to complete our survey. As a token of appreciation, please accept a gift voucher of Rs.[MRP_AMOUNT] from us.<br>
	Your voucher code is <b>[COUPONCODE]</b>. This voucher is valid for [VALIDITY] days from the date of issue, and on purchases of Rs.[MIN_AMOUNT] or above.
</p>
<p>
	<b>How to use your voucher � </b><br>
	<ul>
	<li>Select products, add to cart</li>
	<li>At check out, enter the voucher code and redeem</li>
	<li>Pay the balance using your credit/debit cards or internet banking</li>	
</p>
<p>
In case of any queries, please feel free to call us any time of the day at +91 80 4354 1999 or write to support@myntra.com
</p>
<p>Regards<br>
Myntra</p>
</body>
</html>' where name='custsurvey0611';