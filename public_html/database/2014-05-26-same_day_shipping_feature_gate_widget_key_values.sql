insert into mk_feature_gate_key_value_pairs (`key`,`value`,`description`) values('myntra.sameDayDelivery.enabled','false','Feature gate for Same Day Delivery.');
insert into mk_widget_key_value_pairs(`key`, `value`, `description`) values('shipping.charges.sameDayDelivery','150.0','Same Day Delivery Shipping charge.');
insert into mk_widget_key_value_pairs(`key`, `value`, `description`) values('shipping.cutOffTime.sameDayDelivery','11','Same Day Delivery cutOffTime. Shown in UI only.');
insert into mk_abtesting_tests (name,ga_slot,seg_algo,source_type) values('sameDayDelivery','1','abtest\\algos\\impl\\RandomSegmentationAlgo','tpl');
insert into mk_abtesting_variations (ab_test_id,name,percent_probability) values((select id from mk_abtesting_tests where name='sameDayDelivery'),'test',0);
insert into mk_abtesting_variations (ab_test_id,name,percent_probability) values((select id from mk_abtesting_tests where name='sameDayDelivery'),'control',100);
