

insert into mk_abtesting_tests (name,ga_slot,seg_algo,source_type) values('LocationBreadcrumbABTest','0','abtest\\algos\\impl\\RandomSegmentationAlgo','tpl');
insert into mk_abtesting_variations (ab_test_id,name,percent_probability) values((select id from mk_abtesting_tests where name='LocationBreadcrumbABTest'),'test',50);
insert into mk_abtesting_variations (ab_test_id,name,percent_probability) values((select id from mk_abtesting_tests where name='LocationBreadcrumbABTest'),'control',50);

insert into mk_widget_key_value_pairs(`key`,`value`,`description`) values('searchBreadCrumbRuleInJson','[\"h\",\"mc\",\"g-mc\",\"at\",\"b-at\",\"pn\"]','h=>home, mc=>masterCategory, g=>gender, sc=>sub category, at=>article type, b=>brand, pn=>productName/page title');
insert into mk_widget_key_value_pairs(`key`,`value`,`description`) values('pdpBreadCrumbRuleInJson','[\"h\",\"mc\",\"g-mc\",\"at\",\"b-at\",\"pn\"]','h=>home, mc=>masterCategory, g=>gender, sc=>sub category, at=>article type, b=>brand, pn=>productName/page title');
insert into mk_widget_key_value_pairs(`key`,`value`,`description`) values('breadCrumbSynonymList','{\"apparel\": \"Clothing\", \"men\": \"Men\'s\"}',' synonym list in json for breadcrumbs');

