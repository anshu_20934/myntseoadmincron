drop table if exists mk_style_visibility_info;
create table mk_style_visibility_info( style_id INT, pvv1 DECIMAL(10,3), pvv2 DECIMAL(10,3), pvv3 DECIMAL(10,3), sum_list_count INT, new varchar(10), replenished varchar(10));
drop table if exists mk_style_visibility_info_backup;
create table mk_style_visibility_info_backup( style_id INT, pvv1 DECIMAL(10,3), pvv2 DECIMAL(10,3), pvv3 DECIMAL(10,3), sum_list_count INT, new varchar(10), replenished varchar(10));


CREATE INDEX id_index_style_vis_value ON
mk_style_visibility_info(style_id);

alter table mk_style_visibility_info charset=utf8;
