DROP TABLE IF EXISTS `photoshoot_style_size_issue_rule`;

CREATE TABLE `photoshoot_style_size_issue_rule`(
  `id` int NOT NULL AUTO_INCREMENT,
  `id_attribute_type_values_fk_brand` int NOT NULL,
  `id_catalogue_classification_fk` int NOT NULL,
  `id_attribute_type_values_fk_gender` int NOT NULL DEFAULT 0,
  `id_attribute_type_values_fk_age_group` int NOT NULL DEFAULT 0,
  `id_attribute_type_values_fk_usage` int NOT NULL DEFAULT 0,
  `size_option_1` varchar(32) NOT NULL,
  `size_option_2` varchar(32) DEFAULT NULL,
  `size_option_3` varchar(32) DEFAULT NULL,
  `created_on` int DEFAULT NULL,
  `created_by` varchar(128) DEFAULT NULL,
  `updated_on` int DEFAULT NULL,
  `updated_by` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `size_issue_rule` (`id_attribute_type_values_fk_brand`, `id_catalogue_classification_fk`, `id_attribute_type_values_fk_gender`, `id_attribute_type_values_fk_age_group`, `id_attribute_type_values_fk_usage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;