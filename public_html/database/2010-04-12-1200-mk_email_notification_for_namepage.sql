use myntra;
insert into mk_email_notification (name,subject,body) values
('namepage','[RECEIVERNAME], I found your page on Myntra',"Hi [RECEIVERNAME],
 <p>How does the idea of wearing official Team India Jersey with your name on it or writing with a 
classy Sheaffer pen with your name imprinted sound to you? Sounds great, isn't it?</p> <br>
 <p>Now, all these memorable personalized products are right at your fingertips as Myntra has created a page just for you 
 that has all your favorite products with your name on them. Don't believe me?! 
 Check out the link www.myntra.com/[RECEIVERNAME]/userprofile/  to see for yourself.</p><br>
 <p>
 All these gorgeous products can be yours in just 3 simple steps. It takes just 2-4 business days for Myntra to deliver your products anywhere in
 India and 7-10 business days when shipped internationally. Indulge now! </p>
 <br/>
 <p>Sincerely</p>
 <p>[SENDERNAME]</p>");

