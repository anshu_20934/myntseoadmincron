alter table cod_oh_orders_dispostions add index idx_cod_oh_disp USING hash (disposition);
alter table cod_oh_orders_dispostions add index idx_cod_oh_reason USING hash (reason);

alter table mk_order_action_reasons add index idx_ord_act_reason USING hash (reason);

update xcart_order_details xod inner join xcart_returns xr on xod.itemid = xr.itemid inner join xcart_orders xo on xod.orderid = xo.orderid
set xod.item_status = (case when (xr.reshippeddate is not null or xr.receiveddate is null) then (case when xo.status in ('C','DL') then 'D' else 'S' end) when xr.receiveddate is not null then 'RT' else 'FIX' end);