-- MySQL dump 10.13  Distrib 5.1.54, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: myntra
-- ------------------------------------------------------
-- Server version	5.1.54

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mk_widget_top_nav`
--

DROP TABLE IF EXISTS `mk_widget_top_nav`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mk_widget_top_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `link_name` varchar(50) NOT NULL,
  `link_url` varchar(100) DEFAULT NULL,
  `display_category` varchar(255) DEFAULT '',
  `styleid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=583 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mk_widget_top_nav`
--

LOCK TABLES `mk_widget_top_nav` WRITE;
/*!40000 ALTER TABLE `mk_widget_top_nav` DISABLE KEYS */;
INSERT INTO `mk_widget_top_nav` VALUES (4,-1,1,'FOR HIM','/mens','',NULL),(34,4,2,'WHAT\'S NEW','','accessories',2477),(176,34,1,'Fastrack Watches','/mens-accessories-fastrack-watches#!sortby=RECENCY','',NULL),(224,34,2,'Nike Bags','/mens-accessories-nike-bags#!sortby=RECENCY','',NULL),(225,34,3,'Reid & Taylor Wallets','/mens-accessories-reid-and-taylor-wallets#!sortby=RECENCY','',NULL),(257,34,4,'Reid & Taylor Ties','/mens-accessories-reid-and-taylor-ties#!sortby=RECENCY','',NULL),(575,568,2,'Adidas','/adidas','undefined',0),(576,568,3,'Nike','/nike','undefined',0),(572,567,3,'Reebok','/reebok','undefined',0),(573,567,4,'Nike','/nike','undefined',0),(574,567,5,'Adidas','/adidas','undefined',0),(569,567,1,'Puma','/puma-tshirt','undefined',0),(570,568,1,'Puma','/puma-tshirt','undefined',0),(399,4,1,'BRANDS','/tshirt-brands','accessories',2477),(400,399,1,'Fastrack','/mens-accessories-fastrack','',NULL),(401,399,2,'Wildcraft','/mens-accessories-wildcraft','',NULL),(402,-1,2,'FOR HER','/womens','',NULL),(403,402,1,'BRANDS','/sports-footwear-brands','accessories',0),(404,-1,3,'KIDS','/kids','',NULL),(405,404,1,'BRANDS','/casual-footwear-brands','',6089),(406,403,1,'Nike','/nike-sports-shoes','',NULL),(407,403,2,'Puma','/puma-sports-shoes','',NULL),(408,403,3,'Adidas','/adidas-sports-footwear','',NULL),(409,403,4,'Asics','/asics-shoes','',NULL),(410,403,5,'Reebok','/reebok-sports-footwear','',NULL),(411,403,6,'Decathlon','/decathlon-sports-shoes','',NULL),(412,405,1,'Converse','/converse-shoes','',NULL),(413,405,2,'Puma','/puma-casual-footwear','',NULL),(414,405,3,'Lee Cooper','/lee-cooper-shoes','',NULL),(415,405,4,'Numero Uno','/numero-uno-footwear','',NULL),(416,405,5,'Adidas','/adidas-casual-footwear','',NULL),(417,405,6,'Skechers','/skechers-shoes','',NULL),(418,405,7,'ID','/id-shoes','',NULL),(419,405,8,'Reebok','/reebok-casual-footwear','',NULL),(420,405,9,'Catwalk','/catwalk-footwear','',NULL),(421,405,10,'Carlton London','/carlton-london-footwear','',NULL),(422,405,11,'Red Tape','/red-tape-shoes','',NULL),(568,4,4,'WHAT\'S NEW','','footwear',6090),(567,4,3,'BRANDS','','footwear',6090),(426,403,7,'Fila','/fila-sports-shoes','',NULL),(427,405,12,'Fila','/fila-casual-footwear','',NULL),(428,403,8,'Lotto','/lotto-sports-shoes','',NULL),(432,405,13,'Crocs','/crocs','',NULL),(571,567,2,'Puma','/puma-tshirt','undefined',0),(458,399,3,'Reid & Taylor','/mens-accessories-reid-and-taylor','',NULL),(459,399,4,'Carrera','/mens-accessories-carrera','',NULL),(460,399,5,'ESPIRIT','/mens-accessories-espirit','',NULL),(504,405,14,'Spalding','/spalding-footwear','',NULL),(509,405,15,'Rockport','/rockport-footwear','',NULL),(582,568,5,'lotto','/lotto','undefined',0),(577,568,4,'Adidas','/adidas','undefined',0),(578,4,5,'BRANDS','','formalwear',0),(579,4,6,'WHAT\'S NEW','','formalwear',6092),(580,578,1,'Puma','/puma-tshirt','undefined',0),(581,579,1,'Adidas','/adidas','undefined',0),(552,405,16,'Newfeel','/newfeel-footwear','',NULL),(562,402,2,'WHAT\'S NEW','','accessories',6089),(563,562,1,'Reecok','/reebok','undefined',0),(564,562,2,'Nike','/nike','undefined',0),(565,404,2,'WHAT\'S NEW','','',6090),(566,565,1,'Puma','/puma','undefined',0);
/*!40000 ALTER TABLE `mk_widget_top_nav` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-10-19  2:08:53
