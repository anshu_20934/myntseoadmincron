insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('WebEngage.notifications.enabled', 1, '1 - enabled, 0 - disabled');

insert into mk_abtesting_tests (name,ga_slot,seg_algo,source_type, enabled)
values('WebEngageNotifications',0,'abtest\\algos\\impl\\RandomSegmentationAlgo','tpl', true);

insert into mk_abtesting_variations (name,ab_test_id,percent_probability) 
values('test',(select id from mk_abtesting_tests where name='WebEngageNotifications'),0);
insert into mk_abtesting_variations (name,ab_test_id,percent_probability) 
values('control',(select id from mk_abtesting_tests where name='WebEngageNotifications'),100);