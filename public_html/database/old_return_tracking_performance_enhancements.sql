create index MK_OLD_RETURN_TRACKING_queued_date ON mk_old_returns_tracking(queued_date);

create index MK_OLD_RETURN_TRACKING_return_type ON mk_old_returns_tracking(returntype);

create index MK_OLD_RETURN_TRACKING_currstate ON mk_old_returns_tracking(currstate);

create index MK_OLD_RETURN_ACTIVITY_activity_code ON mk_old_returns_tracking_activity(activitycode);

create index MK_OLD_RETURN_ACTIVITY_recorddate ON mk_old_returns_tracking_activity(recorddate);

create index MK_OLD_RETURN_TRACKING_UD_currstate ON mk_old_returns_tracking_ud(currstate);

create index MK_OLD_RETURN_TRACKING_UD_queued_date ON mk_old_returns_tracking_ud(queued_date);

create index MK_OLD_RETURN_ACTIVITY_UD_activity_code ON mk_old_returns_tracking_ud_activity(activitycode);

create index MK_OLD_RETURN_ACTIVITY_UD_recorddate ON mk_old_returns_tracking_ud_activity(recorddate);

ALTER TABLE `mk_old_returns_tracking_ud`  ADD COLUMN `queued_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `itembarcode`;

UPDATE mk_old_returns_tracking_ud A, mk_old_returns_tracking_ud_activity B SET queued_date = recorddate
WHERE A.id = B.udid AND B.activitycode = 'UDQ';


