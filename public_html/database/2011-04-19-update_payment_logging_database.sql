ALTER TABLE mk_payments_log MODIFY payment_option  varchar(32);

ALTER TABLE mk_payments_log MODIFY payment_gateway_name varchar(64) default null;