delete from mk_email_notification where name = 'order_split';

insert into mk_email_notification(name, subject, body) values('order_split', 'Your order with Myntra has been split! Order No. [ORDER_ID]',
'Hi [USER], <br/><br/>
[SPLIT_REASON_TEXT]<br/><br/>
Here are the details of Order No. [ORDER_ID], for your reference -
<br/><br/>
[ORDER_DETAILS]
<br/><br/>
Here are the details of Order No. [NEW_ORDER_ID], for your reference -
<br/><br/>
[NEW_ORDER_DETAILS]
<br/><br/>
<p>In case of any further queries, please get in touch with us at [MYNTRA_CC_PHONE].</p>
<br/>
Regards,<br/>
Myntra Customer Care');
