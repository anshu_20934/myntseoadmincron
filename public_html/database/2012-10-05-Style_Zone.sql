ALTER TABLE mk_widget_top_nav_v3 MODIFY link_name TEXT;

UPDATE mk_widget_top_nav_v3 SET link_name = 'Style Zone', link_url = '/stylezone' WHERE link_name = 'Style Blog';

INSERT INTO mk_widget_top_nav_v3 ( parent_id, display_order, link_name, link_url ) VALUES ((SELECT id FROM mk_widget_top_nav_v3 w2 WHERE w2.link_name = 'Style Zone'), 1, '<img src="http://myntra.myntassets.com/skin2/images/StyleZoneMenu_StyleStudio_final.jpg" height="160" width="188" />', '/stylestudio');

INSERT INTO mk_widget_top_nav_v3 ( parent_id, display_order, link_name, link_url ) VALUES ((SELECT id FROM mk_widget_top_nav_v3 w2 WHERE w2.link_name = 'Style Zone'), 2, '<img src="http://myntra.myntassets.com/skin2/images/StyleZoneMenu_SnS_final.jpg" height="160" width="188" />', '/starnstyle');

INSERT INTO mk_widget_top_nav_v3 ( parent_id, display_order, link_name, link_url ) VALUES ((SELECT id FROM mk_widget_top_nav_v3 w2 WHERE w2.link_name = 'Style Zone'), 3, '<img src="http://myntra.myntassets.com/skin2/images/StyleZoneMenu_StyleMynt_final.jpg" height="160" width="188" />', 'http://www.stylemynt.com');

INSERT INTO mk_widget_top_nav_v3 ( parent_id, display_order, link_name, link_url ) VALUES ((SELECT id FROM mk_widget_top_nav_v3 w2 WHERE w2.link_name = 'Style Zone'), 4, '<img src="http://myntra.myntassets.com/skin2/images/StyleZoneMenu_FashionTrends_final.jpg" height="160" width="188" />', '/fashion-trends');