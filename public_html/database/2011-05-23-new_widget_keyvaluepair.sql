
ALTER TABLE `mk_widget_key_value_pairs` modify `value` text;

insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) VALUES ('returnPickupCharges', '99.00', 'Returns Processing Charges');
insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) VALUES ('returnAddress', 'The Logistics Manager,<br>Myntra Designs Pvt Ltd.,<br>7th Mile, Krishna Reddy Industrial Area, Kudlu Gate,<br>Opp Macaulay High School, Behind Andhra Bank ATM,<br>Bangalore 560 068<br>080-4902-3100<br>', 'Address for sending returns');
