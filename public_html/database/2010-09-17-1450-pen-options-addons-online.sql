use myntra;

update mk_product_options set price=null where id in(3286,3287,3288);

insert into mk_product_addons(style_id,product_option_id,name,price,description,type,is_active,default_option) values(1099,2798,'Fountain Pen',0,'Fountain pen','Fountain Pen',1,'Y');
insert into mk_product_addons(style_id,product_option_id,name,price,description,type,is_active,default_option) values(1100,2795,'Roller Pen',0,'Roller pen','Roller Pen',1,'Y');
insert into mk_product_addons(style_id,product_option_id,name,price,description,type,is_active,default_option) values(1101,2799,'Fountain Pen',0,'Fountain pen','Fountain Pen',1,'Y');
insert into mk_product_addons(style_id,product_option_id,name,price,description,type,is_active,default_option) values(1102,2796,'Roller Pen',0,'Roller pen','Roller Pen',1,'Y');
insert into mk_product_addons(style_id,product_option_id,name,price,description,type,is_active,default_option) values(1103,2800,'Fountain Pen',0,'Fountain pen','Fountain Pen',1,'Y');
insert into mk_product_addons(style_id,product_option_id,name,price,description,type,is_active,default_option) values(1104,2797,'Roller Pen',0,'Roller pen','Roller Pen',1,'Y');
insert into mk_product_addons(style_id,product_option_id,name,price,description,type,is_active,default_option) values(1105,2807,'Roller Pen',0,'Roller pen','Roller Pen',1,'Y');
insert into mk_product_addons(style_id,product_option_id,name,price,description,type,is_active,default_option) values(1106,2808,'Fountain Pen',0,'Fountain pen','Fountain Pen',1,'Y');
insert into mk_product_addons(style_id,product_option_id,name,price,description,type,is_active,default_option) values(1190,3286,'Ball Pen',0,'Ball pen','Ball Pen',1,'Y');
insert into mk_product_addons(style_id,product_option_id,name,price,description,type,is_active,default_option) values(1191,3288,'Ball Pen',0,'Ball pen','Ball Pen',1,'Y');
insert into mk_product_addons(style_id,product_option_id,name,price,description,type,is_active,default_option) values(1192,3287,'Ball Pen',0,'Ball pen','Ball Pen',1,'Y');