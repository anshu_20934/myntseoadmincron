use myntra;

/*1233*/
/* 1)update option price for style online*/
update mk_product_options set price = 2999 where style='1233';

/* 2)insert addon price for style and its options online*/
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1233','3096','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1233','3097','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1233','3098','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1233','3099','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1233','3100','customized',499,1);

/* 3)insert option price for style pos [rpos]*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('3096','41',2999);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3097','41',2999);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3098','41',2999);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3099','41',2999);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3100','41',2999);

/* 4)insert option price for style pos [kiosk]*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('3096','39',2999);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3097','39',2999);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3098','39',2999);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3099','39',2999);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3100','39',2999);


/*1280*/
/* 1)update option price for style online*/
update mk_product_options set price = 399 where style='1280';

/* 2)insert addon price for style and its options online*/
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1280','3141','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1280','3144','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1280','3146','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1280','3148','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1280','3150','customized',399,1);

/* 3)insert option price for style pos [rpos]*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('3141','41',399);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3144','41',399);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3146','41',399);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3148','41',399);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3150','41',399);

/* 4)insert option price for style pos [kiosk]*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('3141','39',399);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3144','39',399);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3146','39',399);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3148','39',399);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3150','39',399);


/*1278*/
/* 1)update option price for style online*/
update mk_product_options set price = 2995 where style='1278';

/* 2)insert addon price for style and its options online*/
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1278','3126','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1278','3128','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1278','3129','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1278','3131','customized',499,1);

/* 3)insert option price for style pos [rpos]*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('3126','41',2995);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3128','41',2995);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3129','41',2995);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3131','41',2995);

/* 4)insert option price for style pos [kiosk]*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('3126','39',2995);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3128','39',2995);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3129','39',2995);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3131','39',2995);


/*1279*/
/* 1)update option price for style online*/
update mk_product_options set price = 2995 where style='1279';

/* 2)insert addon price for style and its options online*/
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1279','3133','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1279','3135','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1279','3137','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1279','3139','customized',499,1);

/* 3)insert option price for style pos [rpos]*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('3133','41',2995);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3135','41',2995);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3137','41',2995);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3139','41',2995);

/* 4)insert option price for style pos [kiosk]*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('3133','39',2995);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3135','39',2995);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3137','39',2995);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3139','39',2995);


/*1287*/
/* 1)update option price for style online*/
update mk_product_options set price = 2995 where style='1287';

/* 2)insert addon price for style and its options online*/
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1287','3181','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1287','3182','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1287','3183','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1287','3184','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1287','3185','customized',499,1);

/* 3)insert option price for style pos [rpos]*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('3181','41',2995);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3182','41',2995);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3183','41',2995);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3184','41',2995);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3185','41',2995);

/* 4)insert option price for style pos [kiosk]*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('3181','39',2995);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3182','39',2995);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3183','39',2995);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3184','39',2995);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3185','39',2995);
