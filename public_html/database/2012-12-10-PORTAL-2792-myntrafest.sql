
INSERT INTO `mk_widget_key_value_pairs` ( `key`, `value`, `description`) VALUES
('myntraFestCouponPercentDiscount', '50', 'percentage discount to be given as MYNTRAFEST coupons');



INSERT INTO `mk_feature_gate_key_value_pairs` ( `key`, `value`, `description`) VALUES
( 'myntraFestSLabCoupons', 'false', 'true to switch on slab based coupon generation, otherwise false');


INSERT INTO `mk_email_notification` ( `name`, `subject`, `body`, `from_name`, `from_designation`, `enabled`) VALUES
( 'myntra_fest_coupon_share_template_dual', '[FROM] has shared a coupon with you. ', '<font color="#222222" face="Arial"><span style="font-size: 15px; white-space: pre-wrap;">Greetings from Myntra.com!\n\nYou have received a cashback coupon ( [COUPON_CODE] ) from [FROM]. This coupon offers you a [COUPON_PERCENT]% discount on your next purchase on Myntra with a maximum discount amount of Rs. [COUPON_AMOUNT].\n\nThis coupon can be used towards your purchase of over 500 fashion and lifestyle brands on Myntra.com.\n\nYou can view this coupon in your My Myntra section by going to  http://www.myntra.com/mymyntra.php?view=mymyntcredits . You would need to sign up or sign on Myntra using your email address. This coupon is valid until [COUPON_EXP].\n\nWe hope you enjoy shopping at Myntra.com.\n\nThanks\nMyntra.com Team\nIndia''s Largest Fashion Store\n</span></font><div style="color: rgb(0, 0, 0); font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px; "><br></div>', NULL, NULL, 1);



