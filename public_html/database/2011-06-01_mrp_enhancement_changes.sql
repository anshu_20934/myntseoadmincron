/*alter table mk_coupon_group add column grouptype enum('cash','noncash') default 'noncash' after groupname;*/
alter table xcart_orders add column cash_redeemed decimal(12,2) default 0.00;
alter table xcart_order_details add column cash_redeemed decimal(12,2) default 0.00;
alter table xcart_orders add column cash_coupon_code varchar(50) default NULL;
alter table xcart_order_details add column cash_coupon_code varchar(50) default NULL;

insert into `mk_mrp_rules` (`ruleid`,`name`,`isActive`,`isRootLevel`,`childRules`,`applicableOn`,`isPrimitive`,`fieldToCompare`,`operatorToCompare`,`valueToCompare`,`successAction`,`failureAction`,`nonPrimitiveFunction`,`isSynchronous`) values ( '1','first_login','1','1','2','customer','1','',NULL,NULL,'first_login_action','first_login_failure',NULL,1);
insert into `mk_mrp_rules` (`ruleid`,`name`,`isActive`,`isRootLevel`,`childRules`,`applicableOn`,`isPrimitive`,`fieldToCompare`,`operatorToCompare`,`valueToCompare`,`successAction`,`failureAction`,`nonPrimitiveFunction`,`isSynchronous`) values ( '2','first_login_child','1','0',NULL,'customer','1','first_login','equals','last_login','',NULL,NULL,0);
insert into `mk_mrp_rules` (`ruleid`,`name`,`isActive`,`isRootLevel`,`childRules`,`applicableOn`,`isPrimitive`,`fieldToCompare`,`operatorToCompare`,`valueToCompare`,`successAction`,`failureAction`,`nonPrimitiveFunction`,`isSynchronous`) values ( '3','referral','1','1','4','order','0','',NULL,NULL,'referral_action','referral_failure',NULL,0);
insert into `mk_mrp_rules` (`ruleid`,`name`,`isActive`,`isRootLevel`,`childRules`,`applicableOn`,`isPrimitive`,`fieldToCompare`,`operatorToCompare`,`valueToCompare`,`successAction`,`failureAction`,`nonPrimitiveFunction`,`isSynchronous`) values ( '4','referral_child','1','0',NULL,'order','0',NULL,NULL,NULL,'',NULL,'verify_referral','0');
insert into `mk_mrp_rules` (`ruleid`,`name`,`isActive`,`isRootLevel`,`childRules`,`applicableOn`,`isPrimitive`,`fieldToCompare`,`operatorToCompare`,`valueToCompare`,`successAction`,`failureAction`,`nonPrimitiveFunction`,`isSynchronous`) values ( '5','first_fb_login','1','1','6','customer','1','',NULL,NULL,'first_fblogin_action','first_login_failure',NULL,1);
insert into `mk_mrp_rules` (`ruleid`,`name`,`isActive`,`isRootLevel`,`childRules`,`applicableOn`,`isPrimitive`,`fieldToCompare`,`operatorToCompare`,`valueToCompare`,`successAction`,`failureAction`,`nonPrimitiveFunction`,`isSynchronous`) values ( '6','first_fb_login_child','1','0',NULL,'customer','1','first_login','equals','last_login','',NULL,NULL,0);


-- Referral successful mail.
INSERT INTO `mk_email_notification`
  (name, subject, body)
VALUES
  ('mrp_ref_registration', 'Rs. 250 credited to your Myntra account', '<html>
        <body>
                <p>Hello [REFERER],</p>
                <p>Your referred friend, [REFERRED] has registered on <a href="http://www.myntra.com?utm_source=MRP&utm_medium=Onlinemailer&utm_campaign=welcomemail">Myntra.com</a>!</p>
                <p>To convey our gratitude, we have credited your account with Rs. [MRP_AMOUNT]/-<br>
                Use the credits to shop more and save even more!<br>
                <a href="http://www.facebook.com/sharer.php?u=http://www.myntra.com/register.php?ref=[REFERER]" target="_blank">
                    <img src="http://cdn.myntra.com/skin1/myntra_images/facebook.png" alt="Invite friends on Facebook."></a>
                <a href="http://twitter.com/home?status=I loved shopping at Myntra.com... Join me! http://www.myntra.com/register.php?ref=[REFERER_TW]" target="_blank">
                    <img src="http://cdn.myntra.com/skin1/myntra_images/twitter.png" alt="Invite followers from Twitter."></a></p>
                <p>In case of any queries, do feel free to get in touch with our 24X7 customer care at +91-80-43541999.</p>
                <p>Thanks!<br>
                Team Myntra.</p>
        </body>
</html>');

-- mk_referral_log table
ALTER TABLE mk_referral_log 
	ADD COLUMN regcompleteCoupon VARCHAR(50),
	ADD COLUMN ordqueuedCoupon VARCHAR(50), 
	ADD column invitesSent TINYINT;
	
