insert into mk_feature_gate_key_value_pairs(`key`,`value`,description) values('shopfest.showAssuredPrizes','true','Shopping Festival show assured prizes');

insert into mk_feature_gate_key_value_pairs(`key`,`value`,description) values('shopfest.showDailyPrizes','false','Shopping Festival show daily prizes');

insert into mk_feature_gate_key_value_pairs(`key`,`value`,description) values('shopfest.showBumperPrizes','false','Shopping Festival show bumper prizes');
