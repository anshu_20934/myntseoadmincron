use myntra;

/*1232*/
/* 1)update option price for style online*/
update mk_product_options set price = 2999 where style='1232';

/* 2)insert addon price for style and its options online*/
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1232','3091','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1232','3092','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1232','3093','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1232','3094','customized',499,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1232','3095','customized',499,1);

/* 3)insert option price for style pos [rpos]*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('3091','41',2999);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3092','41',2999);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3093','41',2999);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3094','41',2999);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3095','41',2999);

/* 4)insert option price for style pos [kiosk]*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('3091','39',2999);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3092','39',2999);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3093','39',2999);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3094','39',2999);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3095','39',2999);
