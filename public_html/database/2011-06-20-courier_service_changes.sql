update mk_courier_service set enable_status = 1 where code = 'HD';

update mk_courier_service set reports_template = 'orderid as Reference, CONCAT(s_firstname," ",s_lastname) as "name", s_address as "address", s_city as "city", ifNULL((select state from xcart_states where code=s_state and country_code=s_country),s_state) as "state", s_zipcode as pin, mobile as "tel no", mobile as "mob no", qtyInOrder as pieces, "Item" as "description"' where code = 'AR';
