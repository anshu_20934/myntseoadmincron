alter table mk_payments_log add column bin_number int(6) default null;
alter table mk_payments_log add column ip_address varchar(255) default null;


alter table mk_payments_log add index  `payment_gateway_name` (payment_gateway_name);
alter table mk_payments_log add index  `time_insert` (time_insert);
alter table mk_payments_log add index  `is_complete` (is_complete);


CREATE TABLE `mk_axis_payments_log` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `orderid` int(20) NOT NULL,
  `login` varchar(128),
  `response_code` varchar(32),
  `amount` int(12),
  `locale` varchar(5),
  `batch_no` varchar(8),
  `command` varchar(16),
  `message` varchar(255),
  `version` varchar(8),
  `cardtype` varchar(2),
  `orderinfo` varchar(34),
  `receipt_no` varchar(12),
  `merchant_id` varchar(16),
  `merchant_txn_ref` varchar(40),
  `authorize_id` varchar(6),
  `transaction_no` varchar(19),
  `acq_response_code` varchar(3),
  `csc_result_code` varchar(11),
  `csc_request_code` varchar(11),
  `acq_csc_resp_code` varchar(1),
  `trans_type_3ds` varchar(3),
  `ver_status_3ds` varchar(1),
  `ver_token_3ds` varchar(28),
  `ver_security_level_3ds` varchar(2),
  `enrolled_3ds` varchar(1),
  `xid` varchar(28),  
  `eci`	int(2),
  `status_3ds` varchar(1),
  `avs_request_code` varchar(11),
  `avs_result_code` varchar(11),
  `avs_acq_response_code` varchar(1),
  `response_code_description` varchar(255),
  `csc_response_description` varchar(255),
  `status_3ds_description` varchar(255),
  `avs_result_code_description` varchar(255),
  `response` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

alter  table mk_axis_payments_log add index `orderid` (orderid);
alter  table mk_axis_payments_log add index `login` (login);
