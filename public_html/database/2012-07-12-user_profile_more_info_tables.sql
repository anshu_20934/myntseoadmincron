create  table customer_personna (
login varchar(128) PRIMARY KEY,
 relationship_status varchar(30),
 employment varchar(50),
 education varchar(50),
 update_from_fb int(1) not null default 1
 ) ENGINE=InnoDB;

create table interests_list (
    `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `interest_name` varchar(128) NOT NULL,
    `created_on` int(11) DEFAULT NULL,
    `modified_on` int(11) DEFAULT NULL
)ENGINE=InnoDB;

CREATE TABLE `user_brand_preferences` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(128) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `created_on` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
)ENGINE=InnoDB;

CREATE TABLE `user_brand_preferences_history` (
  `id` int(20) DEFAULT NULL,
  `login` varchar(128) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `created_on` int(11) DEFAULT NULL,
  `revision_number` int(11) DEFAULT NULL,
  `revision_type` int(1) DEFAULT NULL
) ENGINE=InnoDB;

CREATE TABLE `user_interests` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(128) NOT NULL,
  `interest_id` int(11) NOT NULL,
  `created_on` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
)ENGINE=InnoDB;

CREATE TABLE `user_interests_history` (
  `id` int(20) DEFAULT NULL,
  `login` varchar(128) NOT NULL,
  `interest_id` int(11) NOT NULL,
  `created_on` int(11) DEFAULT NULL,
  `revision_number` int(11) DEFAULT NULL,
  `revision_type` int(1) DEFAULT NULL
)ENGINE=InnoDB;

ALTER TABLE user_brand_preferences ADD INDEX login USING BTREE (login ASC) ;
ALTER TABLE user_interests ADD INDEX login USING BTREE (login ASC) ;

insert into interests_list (interest_name,created_on,modified_on) values('Runner',unix_timestamp(),unix_timestamp());
insert into interests_list (interest_name,created_on,modified_on) values('Sports – player',unix_timestamp(),unix_timestamp());
insert into interests_list (interest_name,created_on,modified_on) values('Sports - watcher',unix_timestamp(),unix_timestamp());
insert into interests_list (interest_name,created_on,modified_on) values('Movie Buff',unix_timestamp(),unix_timestamp());
insert into interests_list (interest_name,created_on,modified_on) values('Outdoorsy',unix_timestamp(),unix_timestamp());
insert into interests_list (interest_name,created_on,modified_on) values('Traveler',unix_timestamp(),unix_timestamp());
insert into interests_list (interest_name,created_on,modified_on) values('Cook',unix_timestamp(),unix_timestamp());
insert into interests_list (interest_name,created_on,modified_on) values('Blogger',unix_timestamp(),unix_timestamp());
insert into interests_list (interest_name,created_on,modified_on) values('Reader',unix_timestamp(),unix_timestamp());
insert into interests_list (interest_name,created_on,modified_on) values('Swimmer',unix_timestamp(),unix_timestamp());
insert into interests_list (interest_name,created_on,modified_on) values('Home Decorator',unix_timestamp(),unix_timestamp());
insert into interests_list (interest_name,created_on,modified_on) values('Party Animal',unix_timestamp(),unix_timestamp());
insert into interests_list (interest_name,created_on,modified_on) values('Ethnic Dresser',unix_timestamp(),unix_timestamp());
insert into interests_list (interest_name,created_on,modified_on) values('Gym Addict',unix_timestamp(),unix_timestamp());
insert into interests_list (interest_name,created_on,modified_on) values('Foodie',unix_timestamp(),unix_timestamp());
insert into interests_list (interest_name,created_on,modified_on) values('Music Lover',unix_timestamp(),unix_timestamp());
insert into interests_list (interest_name,created_on,modified_on) values('Just like to hang out',unix_timestamp(),unix_timestamp());