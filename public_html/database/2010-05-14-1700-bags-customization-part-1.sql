/*1*/
use myntra;

/*2*/
alter table mk_product_group add column is_active enum('Y','N') default 'N';

/*3*/
update mk_product_group set is_active='Y';

/*4.creating bag group-need to put defaultstyleid*/
insert into mk_product_group (id,name,label,title,description,default_style_id,`type`,url,image,display_order)
values('13','Bags','Bags','Myntra bags','Bags description','1114','2','bags','skin1/mkimages/product_groups/bags.jpg','13');

/*5*/
update mk_product_group set is_active='N' where id='13';


/*6.alter for all style's all orientations*/
alter table mk_customization_orientation add column textLimitation tinyint(1) not null default 0,add column textLimitationTpl varchar(250) null, add column textFontColors varchar(250) not null default 'ALL',add column textColorDefault smallint(1) not null default 1; 


