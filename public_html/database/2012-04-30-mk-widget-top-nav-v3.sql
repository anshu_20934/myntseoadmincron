DROP TABLE IF EXISTS `mk_widget_top_nav_v3`;
CREATE TABLE `mk_widget_top_nav_v3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11),
  `display_order` int(11),
  `link_name` varchar(50) NOT NULL,
  `link_url` varchar(100),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
