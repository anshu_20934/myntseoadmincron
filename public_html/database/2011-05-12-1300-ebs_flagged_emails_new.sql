delete from mk_email_notification where name = 'ebs_flagged_rejection';
delete from mk_email_notification where name = 'ebs_flagged_queued';
delete from mk_email_notification where name = 'ebs_flagged_rejected';

insert into mk_email_notification (name, subject, body) values('ebs_flagged_rejection','Myntra Order [ORDER_ID] Cancellation','Dear [FIRST_NAME],<br/>This is in reference to your order no. [ORDER_ID]</a> placed on [ORDER_DATE].<br/><br/>[ORDER_DETAILS]<br/>The bank had put your online payment transaction on hold. In order to proceed with the transaction, the bank required some extra documents from you. To get these documents and get the transaction approved, we tried calling you a few times. Unfortunately, we have not been able to reach you and hence regret to inform that your order has been cancelled. <br/><br/>However, if you would like to proceed with your order, you could place a Cash on Delivery order through our website (Myntra.com) or you could contact us at ([MYNTRA_CC_PHONE]) and we can help you expedite this process.<br/><br/>We sincerely regret the inconvenience this has caused to you. We value your patronage and the time you took to shop with us. We would like you to further continue shopping at Myntra.com.<br/><br/>Regards,<br/>Myntra Customer Support<br/>');

insert into mk_email_notification (name, subject, body) values('ebs_flagged_queued','[ORDER_ID] flagged by EBS -- Queued', 'The following order was flagged by EBS and was put on ON HOLD. This order has been queued as the same is unflagged on EBS.<br/>
<table>
	<tr><td>OrderID</td><td>[ORDER_ID]</td></tr>
	<tr><td>Payment ID</td><td>[GATEWAY_ID]</td></tr>
	<tr><td>Order Date</td><td>[ORDER_DATE]</td></tr>
	<tr><td>Queued Date</td><td>[QUEUED_DATE]</td></tr>
	<tr><td>Order Amount</td><td>[ORDER_AMOUNT]</td></tr>
	<tr><td>Customer login</td><td>[CUSTOMER]</td></tr>
	<tr><td>Customer name</td><td>[CUSTOMER_NAME]</td></tr>
	<tr><td>Customer phone</td><td>[CUSTOMER_PHONE]</td></tr>
</table>');

insert into mk_email_notification (name, subject, body) values('ebs_flagged_rejected','[ORDER_ID] flagged by EBS -- Rejected', 'The following order was flagged by EBS and was put on ON HOLD. This order has been rejected as the same is still flagged on EBS.<br/>
<table>
	<tr><td>OrderID</td><td>[ORDER_ID]</td></tr>
	<tr><td>Payment ID</td><td>[GATEWAY_ID]</td></tr>
	<tr><td>Order Date</td><td>[ORDER_DATE]</td></tr>
	<tr><td>Rejection Date</td><td>[REJECTION_DATE]</td></tr>
	<tr><td>Order Amount</td><td>[ORDER_AMOUNT]</td></tr>
	<tr><td>Customer login</td><td>[CUSTOMER]</td></tr>
	<tr><td>Customer name</td><td>[CUSTOMER_NAME]</td></tr>
	<tr><td>Customer phone</td><td>[CUSTOMER_PHONE]</td></tr>
</table>');
