update mk_courier_service set reports_template= '"" as "AIRWAYBILL NUMBER", orderid as "SHIPPERS REFERENCE NUMBER",CONCAT(s_title," ",s_firstname) as "CONSIGNEE CONTACT NAME", CONCAT(s_firstname," ",s_lastname) as "CONSIGNEE NAME",mid(s_address,1,29) as "CONSIGNEE ADDRESS LINE 1",mid(s_address,30,29) as "CONSIGNEE ADDRESS LINE 2", concat(mid(s_address,59,10),",",s_city,",",s_state,",",s_country) as "CONSIGNEE ADDRESS LINE 3","" as "CONSIGNEE ADDRESS LINE 4","" as "CONSIGNEE AREA", s_zipcode as "CONSIGNEE PINCODE", mobile as "CONSIGNEE MOBILE NUMBER","IXP" as PRODUCT, qtyInOrder as PIECE, 0.5 as WEIGHT,total as "DECLARED VALUE","LIFESTYLE PRODUCTS" as "CONTENT DESCRIPTION"' where code = 'FX';


update mk_courier_service set reports_template= '"BLR" as Origin_Mail_Centre_Code, "" as Destination_Mail_Centre_Code, s_city as Destination_City, orderid as Reference, from_unixtime(date, "%d.%m.%Y") as Order_Date, date_format(now(), "%d.%m.%Y") as Picking_Date, 10932 as Customer_Code, 10932 as AccountNo, 
(SELECT CASE WHEN payment_method = "cod" THEN concat("COD",": ", total) ELSE "NON-COD" END) as Bag_No, "" as Master_Con_Note, "" as Tracking_No, qtyInOrder as Quantity, "0.5" as Weight, (SELECT CASE WHEN payment_method = "cod" THEN concat("COD:",total + shipping_cost + gift_charges + cod) ELSE "NON-COD" END) as Special_Delivery_Instruction, "Return to Origin" as Instruction_On_Undelivery, CONCAT(firstname, " ", lastname) as Receiver_Name, mobile as Receiver_Contact_No, s_address as Receiver_Area, s_city as Receiver_City, s_zipcode as Receiver_Zip' where code = 'QS';



