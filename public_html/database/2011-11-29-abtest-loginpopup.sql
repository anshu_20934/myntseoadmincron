insert into mk_abtesting_tests (name,ga_slot,seg_algo,source_type) values('loginpopup','2','abtest\\algos\\impl\\RandomSegmentationAlgo','tpl');
insert into mk_abtesting_variations (ab_test_id,name,percent_probability) values((select id from mk_abtesting_tests where name='loginpopup'),'splash',50);
insert into mk_abtesting_variations (ab_test_id,name,percent_probability) values((select id from mk_abtesting_tests where name='loginpopup'),'control',50);

