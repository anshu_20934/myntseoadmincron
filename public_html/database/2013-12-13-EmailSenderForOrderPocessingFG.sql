insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) 
values('UseLocalSmtpForOrderProcessingMails','false','if true, Order processing mail sender uses MailType->NON_CRITICAL_TXN. if false, it uses CRITICAL_TXN');
