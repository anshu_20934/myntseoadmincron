alter table leader_board_ticker_messages add column title text;
alter table leader_board_ticker_messages add column banner_url varchar(255);
alter table leader_board_ticker_messages add column banner_href varchar(255);
alter table leader_board add column refresh_duration int(11);
alter table leader_board add column type varchar(255);
alter table leader_board add column qualifying_amount int(11);
alter table leader_board add column qualifying_statement varchar(255);
alter table leader_board add column post_qualifying_statement varchar(255);
