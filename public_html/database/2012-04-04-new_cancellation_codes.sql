
delete from cancellation_codes where cancel_type = 'IC';

insert into cancellation_codes (id, cancel_type, cancel_type_desc, cancel_reason, cancel_reason_desc, email_content, last_updated_time, cancellation_mode) values (39, 'CCC','CC Cancellation', 'OOSC','OOS Cancellation', 'We regret to inform you that some of the items ordered by you are not in stock right now. Unfortunately, we were at a very low level of inventory for these items which went out of stock during the time your order was being processed. As a result, we have cancelled these items from your order.', now(), 'ITEM_CANCELLATION');

insert into cancellation_codes (id, cancel_type, cancel_type_desc, cancel_reason, cancel_reason_desc, email_content, last_updated_time, cancellation_mode) values (40, 'CCC','CC Cancellation', 'PMM','Product Mismatch', 'We regret to inform you that some of the items ordered by you are not in stock right now. Unfortunately, we were at a very low level of inventory for these items which went out of stock during the time your order was being processed. As a result, we have cancelled these items from your order.', now(), 'ITEM_CANCELLATION');

insert into cancellation_codes (id, cancel_type, cancel_type_desc, cancel_reason, cancel_reason_desc, email_content, last_updated_time, cancellation_mode) values (41, 'CCR','Customer Request', 'ISOC','Incorrect size ordered', 'Thank you for contacting Myntra Customer Care. We have processed your request for modification of your order. As requested by you, we have cancelled some items in your order.', now(), 'ITEM_CANCELLATION');

insert into cancellation_codes (id, cancel_type, cancel_type_desc, cancel_reason, cancel_reason_desc, email_content, last_updated_time, cancellation_mode) values (42, 'CCR','Customer Request', 'PNRA','Product not required anymore', 'Thank you for contacting Myntra Customer Care. We have processed your request for modification of your order. As requested by you, we have cancelled some items in your order.', now(), 'ITEM_CANCELLATION');

insert into cancellation_codes (id, cancel_type, cancel_type_desc, cancel_reason, cancel_reason_desc, email_content, last_updated_time, cancellation_mode) values (43, 'CCR','Customer Request', 'CIOC','Cash Issue', 'Thank you for contacting Myntra Customer Care. We have processed your request for modification of your order. As requested by you, we have cancelled some items in your order.', now(), 'ITEM_CANCELLATION');

insert into cancellation_codes (id, cancel_type, cancel_type_desc, cancel_reason, cancel_reason_desc, email_content, last_updated_time, cancellation_mode) values (44, 'CCR','Customer Request', 'OBM','Ordered By Mistake', 'Thank you for contacting Myntra Customer Care. We have processed your request for modification of your order. As requested by you, we have cancelled some items in your order.', now(), 'ITEM_CANCELLATION');

insert into cancellation_codes (id, cancel_type, cancel_type_desc, cancel_reason, cancel_reason_desc, email_content, last_updated_time, cancellation_mode) values (45, 'CCR','Customer Request', 'CIOC','Wants to change style/color', 'Thank you for contacting Myntra Customer Care. We have processed your request for modification of your order. As requested by you, we have cancelled some items in your order.', now(), 'ITEM_CANCELLATION');


