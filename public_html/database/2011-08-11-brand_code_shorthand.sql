UPDATE mk_skus SET CODE = CONCAT('WWW',CODE)
WHERE brand_id = 1086;

UPDATE mk_skus SET CODE = CONCAT('W',CODE)
WHERE brand_id = 1091;

UPDATE mk_attribute_type_values SET attribute_code='WWAC'
WHERE id=1091;

UPDATE mk_attribute_type_values SET attribute_code='WWWW'
WHERE id=1086;
