insert into `mk_widget_key_value_pairs` (`key`,`value`,`description`) VALUES ('fest_cancel_message','any 100% cashback coupons generated as a part of this order will be cancelled','This message will be displayed when user will try to cancel the order');
insert into `mk_widget_key_value_pairs` (`key`,`value`,`description`) VALUES ('fest_cancel_orders','Order Id(s) $orderids will be cancelled','Displays all the order ids that will be canceled on cancelling a given order during december fest');

