USE `myntra`;
/*Table structure for table `mk_comments` */

DROP TABLE IF EXISTS `mk_comments`;

CREATE TABLE `mk_comments` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `comment` text,
  `submited_date` int(11) default NULL,
  `status` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


