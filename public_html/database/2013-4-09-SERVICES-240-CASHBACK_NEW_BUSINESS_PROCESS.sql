INSERT INTO `cashback_business_process_lookup` (
`business_process` ,
`Code` ,
`transaction_type` ,
`Bucket` ,
`Action`
)
VALUES (
'Fraud Reversal',  'FRAUD_REVERSAL',  'Debit',  'Store/Earned Credit',  'debiting cashback in case of fraudulant behaviour of user cashback accounts.'
);


#ALTER TABLE  `cashback_transaction_log` CHANGE  `business_process`  `business_process` VARCHAR( 30 ) NOT NULL;
