use myntra;
CREATE TABLE `mk_category_search` (
  `key_id` int(11) NOT NULL ,
  `url` varchar(2000) default NULL,
   keyword varchar(300),
  PRIMARY KEY  (`key_id`)
) ;
update mk_stats_search set is_valid='P' where key_id in (2959,2704,3723,3724,867687,
2706,867683,2857,3726,867686,3727,2874,3728,867684,2819,2856,3730,867685,867688,2859,28830,2891,2705,204142,38673,137931);

update mk_stats_search set is_valid='N' where key_id='155967';

insert into mk_category_search (key_id,url,keyword) values ('2704','buy/Mug','mug');
insert into mk_category_search (key_id,url,keyword) values ('2705','buy/Mug','mugs');
insert into mk_category_search (key_id,url,keyword) values ('2706','buy/Poster','poster');
insert into mk_category_search (key_id,url,keyword) values ('2819','buy/Watch','watch');
insert into mk_category_search (key_id,url,keyword) values ('2856','buy/Pendant','pendant');
insert into mk_category_search (key_id,url,keyword) values ('2857','buy/Keychain','keychain');
insert into mk_category_search (key_id,url,keyword) values ('2891','buy/T-Shirt','tshirt');
insert into mk_category_search (key_id,url,keyword) values ('2959','buy/T-Shirt','t-shirt');
insert into mk_category_search (key_id,url,keyword) values ('3723','buy/Coaster','coaster');
insert into mk_category_search (key_id,url,keyword) values ('3724','buy/Mousepad','mousepad');
insert into mk_category_search (key_id,url,keyword) values ('3726','buy/Jigsaw','jigsaw');
insert into mk_category_search (key_id,url,keyword) values ('3730','buy/Sweatshirt','sweatshirt');
insert into mk_category_search (key_id,url,keyword) values ('38673','buy/T-Shirt','shirt');
insert into mk_category_search (key_id,url,keyword) values ('204142','buy/T-Shirt','tee shirt');
insert into mk_category_search (key_id,url,keyword) values ('867686','buy/Jigsaw','jigsaw');
insert into mk_category_search (key_id,url,keyword) values ('867687','buy/Greeting Card','greeting card');
insert into mk_category_search (key_id,url,keyword) values ('867683','buy/Plate','plate');
insert into mk_category_search (key_id,url,keyword) values ('3728','buy/Plate','plate');
insert into mk_category_search (key_id,url,keyword) values ('137931','buy/Plate','plates');