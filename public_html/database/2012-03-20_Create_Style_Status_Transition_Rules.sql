DROP TABLE IF EXISTS `style_status_transition_rules`;
CREATE TABLE `style_status_transition_rules` (
  `id` int NOT NULL AUTO_INCREMENT,
  `style_status_code_from` char(3) NOT NULL,
  `style_status_name_from` varchar(50) NOT NULL,
  `style_status_code_to` char(3) NOT NULL,
  `style_status_name_to` varchar(50) NOT NULL,
  `created_on` int DEFAULT NULL,
  `created_by` varchar(128) DEFAULT NULL,
  `updated_on` int DEFAULT NULL,
  `updated_by` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `style_status_transition_rules`
(`style_status_code_from`, `style_status_name_from`, `style_status_code_to`, `style_status_name_to`) VALUES
('D', 'Draft', 'RBQ', 'Ready for BIS QC'),
('D', 'Draft', 'SC', 'Style Cancelled'),
('RBQ', 'Ready for BIS QC', 'RC', 'Ready for Content'),
('RBQ', 'Ready for BIS QC', 'SC', 'Style Cancelled'),
('RC', 'Ready for Content', 'D', 'Draft'),
('RC', 'Ready for Content', 'CC', 'Content Complete'),
('RC', 'Ready for Content', 'SC', 'Style Cancelled'),
('CC', 'Content Complete', 'CQC', 'Content QC Complete'),
('CQC', 'Content QC Complete', 'IUC', 'Image Upload Complete'),
('CQC', 'Content QC Complete', 'SC', 'Style Cancelled'),
('IUC', 'Image Upload Complete', 'P', 'Active'),
('IUC', 'Image Upload Complete', 'SC', 'Style Cancelled'),
('A', 'Inactive', 'P', 'Active'),
('A', 'Inactive', 'D', 'Draft'),
('P', 'Active', 'A', 'Inactive'),
('P', 'Active', 'SC', 'Style Cancelled');