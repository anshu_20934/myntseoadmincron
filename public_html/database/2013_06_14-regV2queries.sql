

insert into mk_abtesting_tests (name, ga_slot, seg_algo,source_type) values ('nudge', '0', 'abtest\\algos\\impl\\RandomSegmentationAlgo', 'tpl');
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('test', (select id from mk_abtesting_tests where name ='nudge'), 100);
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('control', (select id from mk_abtesting_tests where name ='nudge'), 0);

insert into mk_abtesting_tests (name, ga_slot, seg_algo,source_type) values ('nudgeShow', '0', 'abtest\\algos\\impl\\RandomSegmentationAlgo', 'tpl');
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('test', (select id from mk_abtesting_tests where name ='nudgeShow'), 100);
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('control', (select id from mk_abtesting_tests where name ='nudgeShow'), 0);


insert into mk_abtesting_tests (name, ga_slot, seg_algo,source_type) values ('regV2', '0', 'abtest\\algos\\impl\\RandomSegmentationAlgo', 'tpl');
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('test', (select id from mk_abtesting_tests where name ='regV2'), 100);
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('control', (select id from mk_abtesting_tests where name ='regV2'), 0);
 
insert into mk_abtesting_tests (name, ga_slot, seg_algo,source_type) values ('regV2PhoneRequired', '0', 'abtest\\algos\\impl\\RandomSegmentationAlgo', 'tpl');
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('test', (select id from mk_abtesting_tests where name ='regV2PhoneRequired'), 100);
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('control', (select id from mk_abtesting_tests where name ='regV2PhoneRequired'), 0);


insert into mk_widget_key_value_pairs (`key`, `value`) values ('signupPoints', '[{"heading":"GET 20% OFF","subtext":"on your first purchase", "imageClass":"bagOff"},{"heading":"MANAGE YOUR WISHLIST","subtext":"Save and share products you like", "imageClass":"heartWishlist"}]');
insert into mk_widget_key_value_pairs (`key`, `value`) values ('confMessages', '[{"message":"new arrivals","color":"#FF8787","href":"\/new-arrivals"},{"message":"on sale","color":"#FFADD7","href":"\/sale"},{"message":"Trends","color":"#6DD9f3","href":"\/100-days-of-summer"},{"message":"500+ Brands","color":"#B8D45d","href":"\/brands"}]');
insert into mk_widget_key_value_pairs (`key`, `value`) values ('loginPoints', '["Apply coupons & cashback","Place orders easily","Track past orders","Manage WishList"]');
insert into mk_widget_key_value_pairs (`key`, `value`) values ('signupPoints2', '[{"heading":"Track your orders","subtext":"Get upto data information about your order\'s location", "imageClass":"indiaMap"}]');
insert into mk_widget_key_value_pairs (`key`, `value`) values ('nudgePoint', '["Create account &","Get 20% off"]');
insert into mk_widget_key_value_pairs (`key`, `value`) values ('showRegV2Confirm', 'show');
insert into mk_widget_key_value_pairs (`key`, `value`) values ('splashDelay',"0");
