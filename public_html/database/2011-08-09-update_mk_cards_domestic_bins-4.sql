insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('402566','ICICI Bank Ltd','Domestic','Onus','','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('437367','The Lakshmi Vilas Bank Ltd','Domestic','Offus','','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('437579','Indusind Bank Limited','Domestic','Offus','','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('457274','ICICI Bank','Domestic','Onus','Credit','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('461148','Bank of Baroda','Domestic','Offus','Debit','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('466059','Idbi Bank Ltd','Domestic','Offus','Debit','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('468556','Development Credit Bank Ltd','Domestic','Offus','Debit','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('468557','Development Credit Bank Ltd','Domestic','Offus','Debit','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('468560','Corporation Bank','Domestic','Offus','Debit','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('470689','City Union Bank Ltd','Domestic','Offus','','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('493749','Standard Chartered Bank','Domestic','Offus','Credit','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('511619','HSBC','Domestic','Offus','CREDIT','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('512967','HDFC Bank Limited','Domestic','Offus','Debit','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('512968','HDFC Bank Limited','Domestic','Offus','Debit','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('523157','Federal Bank Limited,The','Domestic','Offus','Credit','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('523988','Standard Chartered Bank','Domestic','Offus','Debit ','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('523990','Standard Chartered Bank','Domestic','Offus','Credit','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('524240','Axis Bank Limited','Domestic','Offus','Credit','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('525319','South Indian Bank Ltd,The','Domestic','Offus','Credit','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('532729','ICICI Bank Limited','Domestic','Onus','Credit','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('532731','Axis Bank Limited','Domestic','Offus','Debit','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('532734','Axis Bank Limited','Domestic','Offus','Credit','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('533102','Axis Bank Limited','Domestic','Offus','Debit','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('533112','ICICI Bank Limited','Domestic','Onus','Credit','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('548026','Federal Bank Limited,The','Domestic','Offus','Debit','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('548245','Bank of Baroda','Domestic','Offus','Credit','');
insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values ('548568','Indusind Bank Limited','Domestic','Offus','Credit','');

update mk_cards_domestic_bins set card_bank_code='SBI',card_bank_registration_link='https://secure4.arcot.com/vpas/sbi_vbvisa/enroll/index.jsp?locale=en_US&bankid=6897' where UPPER(bank_name) like "SBI%" OR UPPER(bank_name) like "STATE BANK OF INDIA%";

update mk_cards_domestic_bins set card_bank_code='ABN',card_bank_registration_link='http://www.abnamro.co.in/consumer/creditcard/secureCode.html' where UPPER(bank_name) like "ABN%";

update mk_cards_domestic_bins set card_bank_code='AXIS',card_bank_registration_link='https://cardsecurity.enstage.com/ACSWeb/EnrollWeb/AxisBank/main/index.jsp' where UPPER(bank_name) like "AXIS%";

update mk_cards_domestic_bins set card_bank_code='HDFC',card_bank_registration_link='http://www.hdfcbank.com/campaign/visa/visa.htm' where UPPER(bank_name) like "HDFC%";

update mk_cards_domestic_bins set card_bank_code='CITI',card_bank_registration_link='https://www.citibank.co.in/ssjsps/ssindex.do' where UPPER(bank_name) like "CITI%";

update mk_cards_domestic_bins set card_bank_code='HSBC',card_bank_registration_link='http://www.hsbc.co.in/1/2/personal/credit-cards/secure-online-payment-services' where UPPER(bank_name) like "HSBC%";

update mk_cards_domestic_bins set card_bank_code='IDBI',card_bank_registration_link='https://secureonline.idbibank.com/ACSWeb/EnrollWeb/IDBIBank/main/index.jsp' where UPPER(bank_name) like "IDBI%";

update mk_cards_domestic_bins set card_bank_code='ICICI',card_bank_registration_link='https://www.3dsecure.icicibank.com/ACSWeb/EnrollWeb/ICICIBank/main/index.jsp' where UPPER(bank_name) like "ICICI%";

update mk_cards_domestic_bins set card_bank_code='KARUR',card_bank_registration_link='https://cardsecurity.enstage.com/ACSWeb/EnrollWeb/KVB/main/vbv.jsp' where UPPER(bank_name) like "KARUR%";

update mk_cards_domestic_bins set card_bank_code='JK',card_bank_registration_link='https://cardsecurity.enstage.com/ACSWeb/EnrollWeb/JKBank/main/index.jsp' where UPPER(bank_name) like "JAMMU%" OR UPPER(bank_name) like "J&K%";

update mk_cards_domestic_bins set card_bank_code='PNB',card_bank_registration_link='https://pnb.electracard.com/pnb/enrollment/enroll_welcome.jsp' where UPPER(bank_name) like "PUNJAB%" OR UPPER(bank_name) like "PNB%";

update mk_cards_domestic_bins set card_bank_code='CANARA',card_bank_registration_link='https://cardsecurity.enstage.com/ACSWeb/EnrollWeb/CanaraBank/main/index.jsp' where UPPER(bank_name) like "CANARA%";

update mk_cards_domestic_bins set card_bank_code='VIJAYA',card_bank_registration_link='https://cardsecurity.enstage.com/ACSWeb/EnrollWeb/VijayaBank/main/index.jsp' where UPPER(bank_name) like "VIJAYA%";

update mk_cards_domestic_bins set card_bank_code='CENTRAL',card_bank_registration_link='https://cbi.electracard.com/cbi/enrollment/enroll_welcome.jsp' where UPPER(bank_name) like "CENTRAL%";

update mk_cards_domestic_bins set card_bank_code='INDIAN',card_bank_registration_link='https://cardsecurity.enstage.com/ACSWeb/EnrollWeb/IndianBank/main/index.jsp' where UPPER(bank_name) like "INDIAN%";

update mk_cards_domestic_bins set card_bank_code='SC',card_bank_registration_link='http://www.standardchartered.co.in/personal/credit-cards/en/3d_secure.html' where UPPER(bank_name) like "STANDARD%";

update mk_cards_domestic_bins set card_bank_code='DB',card_bank_registration_link='https://www.dbindia.in/DeutscheBank/enrollment/card_type_options.jsp' where UPPER(bank_name) like "DEUTSCHE%";

update mk_cards_domestic_bins set card_bank_code='CORP',card_bank_registration_link='https://corpbank.electracard.com/corpbank/enrollment/enroll_welcome.jsp' where UPPER(bank_name) like "CORPORATION%";

update mk_cards_domestic_bins set card_bank_code='FEDERAL',card_bank_registration_link='https://cardsecurity.enstage.com/ACSWeb/EnrollWeb/FederalBank/main/index.jsp' where UPPER(bank_name) like "FEDERAL%";