alter table mk_style_properties add column discount_rule varchar(255) default NULL;

alter table xcart_order_details 
add column applied_discount_rule int(11) default NULL,
add column applied_discount_algoid varchar(32) default NULL,
add column applied_discount_data text default NULL;

CREATE TABLE `mk_discount_rules` (
  `ruleid` int(11) NOT NULL AUTO_INCREMENT,
  `ruletype` int(11) NOT  NULL DEFAULT 0, 
  `display_name` varchar(255) NOT NULL,
  `algoid` varchar(128) NOT NULL,
  `data` text DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT 1,
  `start_date` int(11) NOT NULL,
  `end_date` int(11) NOT NULL,
  `display_text` text DEFAULT NULL,
  `terms` text DEFAULT NULL,
  `brand` varchar(128) DEFAULT NULL,
  `articletype` varchar(128) DEFAULT NULL,
  `fashion` varchar(128) DEFAULT NULL,
  `exceptionstyles` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ruleid`),
  INDEX `algoid_idx` (`algoid`)
) ENGINE=InnoDB CHARSET=latin1;
