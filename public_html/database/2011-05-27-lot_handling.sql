CREATE TABLE `mk_lot_details` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `lot_id` varchar(45) DEFAULT NULL,

  `skuid` int(11) DEFAULT NULL,

  `quantity` int(11) DEFAULT '0',

  `price` float DEFAULT '0',

  `quantity_sold` int(11) DEFAULT '0',

  `lastmodified` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (`id`),

  KEY `mk_lot_details_index_lot_sku` (`lot_id`,`skuid`),

  KEY `mk_lot_details_index_sku` (`skuid`)

);


alter table  mk_inventory_movement_reasons add column action_mode_manual tinyint(1) default '0';
update mk_inventory_movement_reasons set action_type='I' where action_type in ('MI','OCAI','ICAI');
update mk_inventory_movement_reasons set action_type='R' where action_type in ('MR');
update mk_inventory_movement_reasons set action_type='D' where action_type in ('MD','OPAD','CIA');
update mk_inventory_movement_reasons set action_type='N' where action_type not in ('I','D','R');

update mk_inventory_movement_reasons set action_mode_manual=1 where code in ('NEWP','RJQA','NPRC','INJB','OTHI','SHRI','ISOP','RETV',
'ISJB','REIO','OTHD','MAND','MANE','MANT','MCAD','MCAE','SINV','RJOP','IC','CIA');


insert into mk_inv_vendor_master(name,code) values('Default','MTR');

