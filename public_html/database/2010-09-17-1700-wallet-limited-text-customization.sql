use myntra;

/*1.insert into color table(if new color has to be added)*/
insert into `mk_text_font_color` (color) values('#a4872e');
insert into `mk_text_font_color` (color) values('#b6bdb9');

/*1199*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1199','images/style/carea/T/1199-Premium_Mens_Wallet_Popper_Black_Dazzler.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='145',textColorDefault='145' where id=1686;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1686','1','0.25','14','Y','Y');

/*1200*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1200','images/style/carea/T/1200-Premium_Mens_Wallet_Popper_Dark_Brown.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='144',textColorDefault='144' where id=1687;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1687','1','0.25','14','Y','Y');

/*1201*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1201','images/style/carea/T/1201-Premium_Mens_Wallet_Black_Dazzler.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='145',textColorDefault='145' where id=1688;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1688','1','0.25','14','Y','Y');

/*1202*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1202','images/style/carea/T/1202-Premium_Mens_Wallet_Dark_Brown_Delight.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='144',textColorDefault='144' where id=1689;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1689','1','0.25','14','Y','Y');

/*1203*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1203','images/style/carea/T/1203-Classic_Mens_Wallet_Dark_Brown.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='144',textColorDefault='144' where id=1691;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1691','1','0.25','14','Y','Y');

/*1204*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1204','images/style/carea/T/1204-Classic_Mens_Wallet_Black.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='145',textColorDefault='145' where id=1692;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1692','1','0.25','14','Y','Y');

/*1205*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1205','images/style/carea/T/1205-Premium_Ladies_Wallet-Chocolate_Brown.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='144',textColorDefault='144' where id=1693;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1693','1','0.25','14','Y','Y');

/*1206*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1206','images/style/carea/T/1206-Premium_Ladies_Wallet_Black_Beauty.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='145',textColorDefault='145' where id=1694;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1694','1','0.25','14','Y','Y');

/*1207*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1207','images/style/carea/T/1207-Premium_Ladies_Wallet_Tanned_Exotica.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='144',textColorDefault='144' where id=1695;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1695','1','0.25','14','Y','Y');

/*1208*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1208','images/style/carea/T/1208-Premium_Ladies_Wallet_Popper_Black_Dazzler.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='145',textColorDefault='145' where id=1696;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1696','1','0.25','14','Y','Y');

/*1209*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1209','images/style/carea/T/1209-Premium_Ladies_Wallet_Popper_Dark_Brown.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='144',textColorDefault='144' where id=1697;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1697','1','0.25','14','Y','Y');

/*1210*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1210','images/style/carea/T/1210-Premium_Ladies_Wallet_Red_Sizzler.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='144',textColorDefault='144' where id=1698;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1698','1','0.25','14','Y','Y');

/*1211*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1211','images/style/carea/T/1211-Classic_Ladies_Wallet_Dark_Brown.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='144',textColorDefault='144' where id=1699;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1699','1','0.25','14','Y','Y');

/*1212*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1212','images/style/carea/T/1212-Classic_Ladies_Wallet_Tan.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='144',textColorDefault='144' where id=1700;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1700','1','0.25','14','Y','Y');

/*1213*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1213','images/style/carea/T/1213-Pocket_Business_Card_Holder.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='145',textColorDefault='145' where id=1701;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1701','1','0.20','10','Y','Y');

/*1214*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1214','images/style/carea/T/1214-Leather_Pocket_Business_Card_Holder.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='145',textColorDefault='145' where id=1702;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1702','1','0.20','10','Y','Y');

/*1215*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1215','images/style/carea/T/1215-Leather_Pocket_Business_Card_Holder.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='145',textColorDefault='145' where id=1703;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1703','1','0.20','10','Y','Y');

/*1216*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1216','images/style/carea/T/1216-Leather_Pocket_Business_Card_Holder.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='145',textColorDefault='145' where id=1704;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1704','1','0.20','10','Y','Y');

/*1217*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1217','images/style/carea/T/1217-Key_Fob_1.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='145',textColorDefault='145' where id=1705;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1705','1','0.20','8','Y','Y');


/*1218*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1218','images/style/carea/T/1218Key_Fob_2.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='145',textColorDefault='145' where id=1706;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1706','1','0.20','8','Y','Y');

/*1219*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1219','images/style/carea/T/1219-Key_Fob_3.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='145',textColorDefault='145' where id=1707;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1707','1','0.50','2','Y','Y');

/*1307*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1307','images/style/carea/T/1307-Passport_Holder-Black.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='145',textColorDefault='145' where id=1800;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1800','1','0.25','14','Y','Y');

/*1308*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1308','images/style/carea/T/1308-Passport_Holder-Tan_Brown.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='144',textColorDefault='144' where id=1801;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1801','1','0.25','14','Y','Y');

/*1309*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1309','images/style/carea/T/1309-Passport_Holder-Dark_Brown.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='144',textColorDefault='144' where id=1802;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1802','1','0.25','14','Y','Y');

/*1310*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1310','images/style/carea/T/1310-Calendar_Keychain.jpg','B','Y');
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='145',textColorDefault='145' where id=1803;
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1803','1','0.20','10','Y','Y');
