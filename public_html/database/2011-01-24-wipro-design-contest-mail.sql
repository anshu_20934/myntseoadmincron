insert into mk_email_notification (name,subject,body) values('wiproinvitefrdtoseedesign','Check out my design',"Hi [FRIEND]
<p>
I have created a new design for a [DESIGN_TYPE] and published it on Myntra.com. Please <a href='[URL_OF_DESIGN]'>click here</a> to view and rate my design.  
</p>
<p>[YOUR_NAME]</p>");