alter table xcart_orders modify column courier_service varchar(10);

alter table mk_courier_service modify column code varchar(10);

alter table mk_zipcode_courier_support_mapping modify column courier varchar(10);

insert into mk_courier_service (code,courier_service,website,enable_status,reports_template,orderby,order_completed_mail_template,return_supported) 
select 'ML-DEL','Myntra Logistics Delhi',website,enable_status,reports_template,orderby,order_completed_mail_template,return_supported from mk_courier_service where code = 'ML';

update mk_courier_service set code='ML-BLR',courier_service = 'Myntra Logistics Bangalore' where code = 'ML';

update xcart_orders set courier_service = 'ML-BLR' where courier_service = 'ML';

delete from mk_zipcode_courier_support_mapping where courier = 'ML';