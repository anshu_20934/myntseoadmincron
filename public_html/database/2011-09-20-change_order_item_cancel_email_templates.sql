update mk_email_notification set body = 'Hello [USER], <br/><br/>
[REASON_TEXT] <br><br>
Please find the details of your Order No. [ORDER_ID] below:<br/><br/>
[ORDER_DETAILS]
<br/><br/>
<p>[GOODWILL_COUPON_MSG] [REFUND_MSG] [COUPON_USAGE_TEXT] [CAHBACK_DEDUCT_MSG]</p>
<p>You, our customer, are our focus and your experience takes precedence over everything else. It is the key principle of Myntra.com and we would like to reassure you that we would continue to remain invested in offering the best shopping experience for Fashion and Lifestyle products. We hope that you continue to shop with us and treat this as an aberration to the quality standards we strive to achieve.
</p>
<p>For any clarification, kindly contact us at [MYNTRA_CC_PHONE].</p><br>
Regards,<br>
Team Myntra' where name = 'order_cancel_oos';

update mk_email_notification set body = 'Hello [USER], <br/><br/>
[REASON_TEXT] <br><br>
Please find the details of your Order No. [ORDER_ID] below:<br/><br/>
[ORDER_DETAILS]
<br/><br/>
<p>[GOODWILL_COUPON_MSG] [REFUND_MSG] [COUPON_USAGE_TEXT] [CAHBACK_DEDUCT_MSG]</p>
<p>
We want you to know we are honoured to have you as our esteemed customer and we will do our best to meet your needs. Your continued patronage and suggestions are a vital part of our growth, and for that we are most grateful to you.
</p>
<p>For any clarification, kindly contact us at [MYNTRA_CC_PHONE].</p><br>
Regards,<br/>
Team Myntra' where name = 'order_cancel';

update mk_email_notification set body = 'Hello [USER], <br/><br/>
[CANCEL_REASON_TEXT]<br/><br/>
Updated order details:
<br/><br/>
[ORDER_DETAILS]
<br/><br/>
Your cancelled item details:
<br/><br/>
[CANCELLED_ORDER_DETAILS]
<br/>
<p>[GOODWILL_COUPON_MSG] [REFUND_MSG] [COUPON_USAGE_TEXT] [CAHBACK_DEDUCT_MSG]</p>
<p>We want you to know we are honoured to have you as our esteemed customer and we will do our best to meet your needs. Your continued patronage and suggestions are a vital part of our growth, and for that we are most grateful to you.</p>
<p>For any clarification, kindly contact us at [MYNTRA_CC_PHONE].</p>
<br/>
Regards,<br/>
Team Myntra ' where name = 'item_cancel';
