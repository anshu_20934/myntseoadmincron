CREATE TABLE `size_unification_mapping` (
  `id` int(20) AUTO_INCREMENT NOT NULL UNIQUE,
  `gender` varchar(20) NOT NULL,
  `size` varchar(20) NOT NULL,
  `articletype` varchar(50) DEFAULT NULL,
  `articletype_brand` varchar(100) DEFAULT NULL,
  `styleid` int(15) DEFAULT NULL,
  `unifiedsize` varchar(20) NOT NULL
);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK1', 1);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US2', 1);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO32', 1);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK1.5', 1.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US2.5', 1.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO32.5', 1.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK2', 2);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US3', 2);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO33', 2);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK2.5', 2.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US3.5', 2.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO34', 2.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK3', 3);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US4', 3);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO34.5', 3);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK3.5', 3.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US4.5', 3.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO35', 3.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK4', 4);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US5', 4);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO36', 4);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK4.5', 4.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US5.5', 4.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO36.5', 4.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK5', 5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US6', 5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO37.5', 5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK5.5', 5.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US6.5', 5.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO38.5', 5.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK6', 6);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US7', 6);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO39', 6);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK6.5', 6.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US7.5', 6.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO40', 6.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK7', 7);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US8', 7);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO40.5', 7);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK7.5', 7.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US8.5', 7.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO41', 7.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK8', 8);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US9', 8);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO42', 8);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK8.5', 8.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US9.5', 8.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO42.5', 8.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK9', 9);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US10', 9);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO43', 9);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK9.5', 9.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US10.5', 9.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO44', 9.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK10', 10);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US11', 10);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO44.5', 10);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK10.5', 10.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US11.5', 10.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO45', 10.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK11', 11);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US12', 11);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO45.5', 11);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK11.5', 11.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US12.5', 11.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO46', 11.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK12', 12);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US13', 12);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO47', 12);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK12.5', 12.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US13.5', 12.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO48', 12.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK13', 13);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US14', 13);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO48.5', 13);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK13.5', 13.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US14.5', 13.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO49', 13.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK14', 14);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US15', 14);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO50', 14);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK14.5', 14.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US15.5', 14.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO51', 14.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK15', 15);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US16', 15);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO52', 15);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK15.5', 15.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US16.5', 15.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO53', 15.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK16', 16);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US17', 16);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO53.5', 16);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK16.5', 16.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US17.5', 16.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO54', 16.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK17', 17);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US18', 17);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO55', 17);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK17.5', 17.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US18.5', 17.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO56', 17.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK18', 18);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US19', 18);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO56.5', 18);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK18.5', 18.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US19.5', 18.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO57', 18.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK19', 19);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US20', 19);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO58', 19);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK20', 20);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US21', 20);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO58.5', 20);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'UK21', 21);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'US22', 21);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Men', 'EURO59', 21);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK1', 1);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US2', 1);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO32', 1);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK1.5', 1.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US2.5', 1.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO32.5', 1.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK2', 2);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US3', 2);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO33', 2);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK2.5', 2.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US3.5', 2.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO34', 2.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK3', 3);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US4', 3);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO34.5', 3);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK3.5', 3.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US4.5', 3.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO35', 3.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK4', 4);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US5', 4);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO36', 4);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK4.5', 4.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US5.5', 4.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO36.5', 4.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK5', 5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US6', 5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO37.5', 5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK5.5', 5.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US6.5', 5.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO38.5', 5.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK6', 6);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US7', 6);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO39', 6);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK6.5', 6.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US7.5', 6.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO40', 6.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK7', 7);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US8', 7);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO40.5', 7);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK7.5', 7.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US8.5', 7.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO41', 7.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK8', 8);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US9', 8);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO42', 8);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK8.5', 8.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US9.5', 8.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO42.5', 8.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK9', 9);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US10', 9);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO43', 9);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK9.5', 9.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US10.5', 9.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO44', 9.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK10', 10);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US11', 10);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO44.5', 10);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK10.5', 10.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US11.5', 10.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO45', 10.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK11', 11);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US12', 11);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO45.5', 11);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK11.5', 11.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US12.5', 11.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO46', 11.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK12', 12);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US13', 12);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO47', 12);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK12.5', 12.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US13.5', 12.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO48', 12.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK13', 13);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US14', 13);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO48.5', 13);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK13.5', 13.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US14.5', 13.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO49', 13.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK14', 14);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US15', 14);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO50', 14);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK14.5', 14.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US15.5', 14.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO51', 14.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK15', 15);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US16', 15);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO52', 15);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK15.5', 15.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US16.5', 15.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO53', 15.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK16', 16);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US17', 16);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO53.5', 16);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK16.5', 16.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US17.5', 16.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO54', 16.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK17', 17);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US18', 17);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO55', 17);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK17.5', 17.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US18.5', 17.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO56', 17.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK18', 18);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US19', 18);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO56.5', 18);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK18.5', 18.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US19.5', 18.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO57', 18.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK19', 19);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US20', 19);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO58', 19);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK20', 20);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US21', 20);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO58.5', 20);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'UK21', 21);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'US22', 21);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Unisex', 'EURO59', 21);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK1', 1);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US3.5', 1);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO33', 1);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK1.5', 1.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US4', 1.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO34', 1.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK2', 2);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US4.5', 2);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO34.5', 2);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK2.5', 2.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US5', 2.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO35', 2.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK3', 3);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US5.5', 3);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO35.5', 3);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK3.5', 3.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US6', 3.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO36', 3.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK4', 4);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US6.5', 4);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO37', 4);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK4.5', 4.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US7', 4.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO37.5', 4.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK5', 5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US7.5', 5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO38', 5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK5.5', 5.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US8', 5.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO38.5', 5.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK6', 6);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US8.5', 6);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO39', 6);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK6.5', 6.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US9', 6.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO40', 6.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK7', 7);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US9.5', 7);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO40.5', 7);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK7.5', 7.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US10', 7.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO41', 7.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK8', 8);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US10.5', 8);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO42', 8);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK8.5', 8.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US11', 8.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO42.5', 8.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK9', 9);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US11.5', 9);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO43', 9);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK9.5', 9.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US12', 9.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO44', 9.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK10', 10);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US12.5', 10);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO44.5', 10);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK10.5', 10.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US13', 10.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO45', 10.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK11', 11);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US13.5', 11);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO46', 11);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK11.5', 11.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US14', 11.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO47', 11.5);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK12', 12);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US14.5', 12);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO48', 12);

insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'UK12.5', 12.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'US15', 12.5);
insert into size_unification_mapping (gender, size, unifiedsize) values ('Women', 'EURO48.5', 12.5);