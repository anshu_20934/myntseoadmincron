update mk_email_notification set body = 'Hello [USER], <br/><br/>
[CANCEL_REASON_TEXT]<br/><br/>
Updated order details:
<br/><br/>
[ORDER_DETAILS]
<br/><br/>
Your cancelled item details:
<br/><br/>
[CANCELLED_ORDER_DETAILS]
<br/>
<p>[REFUND_MSG]</p>
<p>We want you to know we are honoured to have you as our esteemed customer and we will do our best to meet your needs. Your continued patronage and suggestions are a vital part of our growth, and for that we are most grateful to you.</p>
<p>For any clarification, kindly contact us at [MYNTRA_CC_PHONE].</p>
<br/>
Regards,<br/>
Team Myntra' where name = 'item_cancel';


update mk_email_notification set body = 'Hello [USER], <br/><br/>
We regret to inform you that we are unable to service your order and would have to cancel it due to an overwhelming response, which has exhausted our stocks. Please find the details of your Order No. [ORDER_ID] below:<br/><br/>
[ORDER_DETAILS]
<br/><br/>
[REFUND_MSG]
[COUPON_USAGE_TEXT]
<p>We want you to know we are honoured to have you as our esteemed customer and we will do our best to meet your needs. Your continued patronage and suggestions are a vital part of our growth, and for that we are most grateful to you.</p>
<p>For any clarification, kindly contact us at [MYNTRA_CC_PHONE].</p>
<br/>
Regards,<br/>
Team Myntra' where name = 'order_cancel_ccc';

update mk_email_notification set body = 'Hello [USER], <br/><br/>
Thank you for contacting Myntra Support. As per your request, we are cancelling your Order No. [ORDER_ID]. Please find below details of the same:<br/><br/>
[ORDER_DETAILS]
<br/><br/>
[REFUND_MSG]
[COUPON_USAGE_TEXT]
<p>We want you to know we are honoured to have you as our esteemed customer and we will do our best to meet your needs. Your continued patronage and suggestions are a vital part of our growth, and for that we are most grateful to you.</p>
<p>For any clarification, kindly contact us at [MYNTRA_CC_PHONE].</p>
<br/>
Regards,<br/>
Team Myntra' where name = 'order_cancel_ccr';
