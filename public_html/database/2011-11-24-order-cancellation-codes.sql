drop table if exists cancellation_codes;

create table cancellation_codes(
id int not null AUTO_INCREMENT,
cancel_type varchar(10) not null,
cancel_type_desc varchar(40) not null,
cancel_reason varchar(10) not null,
cancel_reason_desc varchar(40) not null,
is_part_cancellation tinyint(2) not null default 0,
email_content text,
last_updated_time datetime,
PRIMARY KEY (id),
Unique Key occ_unq_key(cancel_type,cancel_reason)
)ENGINE=InnoDb;

insert into cancellation_codes(cancel_type,cancel_type_desc,cancel_reason,cancel_reason_desc,last_updated_time,is_part_cancellation) values 
('CCC','CC Cancellation','OOSC','OOS Cancellation',now(),0),
('CCC','CC Cancellation','OPIS','Online Payment Issue',now(),0),
('CCC','CC Cancellation','NCNV','COD NC NV',now(),0),
('CCC','CC Cancellation','CODNR','COD Verified NR',now(),0),
('CCC','CC Cancellation','RTO','Returned To Origin',now(),0),
('CCC','CC Cancellation','RTOCR','Returned To Origin',now(),0),
('CCC','CC Cancellation','INVADD','COD Invalid Address',now(),0),
('CCC','CC Cancellation','ADDPINMM','Address Pincode Mismatch',now(),0),
('CCR','Customer Request','DDC','Delayed Delivery Cancellation',now(),0),
('CCR','Customer Request','ISOC','Incorrect size ordered',now(),0),
('CCR','Customer Request','DOC','Duplicate order cancelled',now(),0),
('CCR','Customer Request','PNRA','Product not required anymore / Not interested',now(),0),
('CCR','Customer Request','CIOC','Cash Issue- Cancel Order',now(),0),
('CCR','Customer Request','OBM','Ordered by mistake',now(),0),
('CCR','Customer Request','FO','Fraud Order',now(),0),
('CCR','Customer Request','WTCS','Wants to change style/color',now(),0),
('IC','Item Cancellation','OOS','Item out of Stock',now(),1),
('IC','Item Cancellation','CR','Customer Request',now(),1);

update cancellation_codes cc,mk_order_action_reasons moar set cc.email_content = moar.email_contents where cc.cancel_type= 'CCR' and moar.reason = 'CCR' and moar.action = 'order_cancel';

update cancellation_codes cc,mk_order_action_reasons moar set cc.email_content = moar.email_contents where cc.cancel_type= 'CCC' and moar.reason = cc.cancel_reason and moar.action = 'order_cancel';

update cancellation_codes cc,mk_order_action_reasons moar set cc.email_content = moar.email_contents where cc.cancel_type= 'IC' and moar.reason = cc.cancel_reason and moar.action = 'item_cancel';
 
 
delete from mk_order_action_reasons where action in ('order_cancel','item_cancel');

alter table xcart_orders add column (cancellation_code int);
alter table xcart_order_details add column (cancellation_code int); 
