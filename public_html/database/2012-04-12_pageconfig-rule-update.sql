alter table page_config_image_rule add require_page_location tinyint default 0 comment 'whether this kind of page requires page-location';
update page_config_image_rule set require_page_location = 1 where location_type in ('landing','search','fashion-story');
