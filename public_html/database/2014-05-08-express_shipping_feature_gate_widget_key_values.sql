insert into mk_feature_gate_key_value_pairs (`key`,`value`,`description`) values('myntra.nextDayDelivery.enabled','false','Feature gate for Next Day Delivery.');
insert into mk_widget_key_value_pairs(`key`, `value`, `description`) values('shipping.charges.nextDayDelivery','79.0','Next Day Delivery Shipping charge.');
insert into mk_widget_key_value_pairs(`key`, `value`, `description`) values('shipping.cutOffTime.nextDayDelivery','20','Next Day Delivery cutOffTime. Shown in UI only.');
insert into mk_abtesting_tests (name,ga_slot,seg_algo,source_type) values('nextDayDelivery','1','abtest\\algos\\impl\\RandomSegmentationAlgo','tpl');
insert into mk_abtesting_variations (ab_test_id,name,percent_probability) values((select id from mk_abtesting_tests where name='nextDayDelivery'),'test',0);
insert into mk_abtesting_variations (ab_test_id,name,percent_probability) values((select id from mk_abtesting_tests where name='nextDayDelivery'),'control',100);