insert into mk_voucher_issuer_category (issuercode,categoryid,category_name,active,browse,customize) values('5013','02',null,'Y','Y','N');
insert into mk_voucher_issuer_category (issuercode,categoryid,category_name,active,browse,customize) values('5013','03',null,'Y','Y','N');

insert into mk_issuer_category_style_map (issuercode,categoryid,styleid,active) values ('5013','02','1732','Y');
insert into mk_issuer_category_style_map (issuercode,categoryid,styleid,active) values ('5013','02','1733','Y');
insert into mk_issuer_category_style_map (issuercode,categoryid,styleid,active) values ('5013','02','1734','Y');
insert into mk_issuer_category_style_map (issuercode,categoryid,styleid,active) values ('5013','02','1735','Y');
insert into mk_issuer_category_style_map (issuercode,categoryid,styleid,active) values ('5013','02','1736','Y');

insert into mk_issuer_category_style_map (issuercode,categoryid,styleid,active) values ('5013','03','1740','Y');
insert into mk_issuer_category_style_map (issuercode,categoryid,styleid,active) values ('5013','03','1741','Y');
insert into mk_issuer_category_style_map (issuercode,categoryid,styleid,active) values ('5013','03','1743','Y');
insert into mk_issuer_category_style_map (issuercode,categoryid,styleid,active) values ('5013','03','1745','Y');
insert into mk_issuer_category_style_map (issuercode,categoryid,styleid,active) values ('5013','03','1747','Y');

insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5013','02','1291898898','Y');
insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5013','02','1291475993','Y');
insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5013','02','1292719075','Y');
insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5013','02','1291150151','Y');
insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5013','02','1296809316','Y');

insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5013','03','1294884418','Y');
insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5013','03','1296658503','Y');
insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5013','03','1290368577','Y');
insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5013','03','1298477652','Y');
insert into mk_issuer_category_product_map (issuercode,categoryid,productid,active) values ('5013','03','1299420718','Y');
