CREATE  TABLE IF NOT EXISTS `shopping_fest_leader_board` (    `id` INT NOT NULL AUTO_INCREMENT ,    `login` VARCHAR(100) NOT NULL ,    `category` INT(2) NOT NULL ,    `name` VARCHAR(100) NOT NULL ,    `amount` VARCHAR(45) NOT NULL ,    `order_count` INT NOT NULL ,    `date` DATE NOT NULL,    PRIMARY KEY (`id`) ,    INDEX `IDX_SFLB_DATE` (`date` ASC) ,    INDEX `IDX_SFLB_Login` (`login` ASC) );

