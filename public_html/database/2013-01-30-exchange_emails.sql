insert into mk_email_notification(name, subject, body) values('exchange_confirmation', 'Your Myntra.com exchange request confirmation',
'Hi [USER], <br/><br/>
We have received your request to exchange the product below. Our delivery representative will arrive at your premises by [DELIVERY_DATE] to deliver the replacement product and pick up the original product from you. Please follow the steps described in this email and keep the original product ready for our representative to pick up.<br/><br/>
[SHIPMENT_DETAILS]
<br/>
<b>Step 1: Fill out a returns form</b> for the original product that you are exchanging. You can use the returns form that was sent with the original order or download and print one from <a href="[MY_RETURNS_URL]">My Returns</a> section under My Myntra. If you don\'t have a returns form or can\'t access a printer, please write the following information on a sheet of paper and use that sheet as the returns form.
<ul>
<li>Original Order ID</li>
<li>Exchange Order ID</li>
</ul>
</br>
<b>Step 2: Pack the product.</b> In the packet, please include the product along with the tags that it came with and a completed returns form.
<br><br>
At any time, you can track the status of your exchange request at <a href="[MY_ORDERS_URL]">My Orders</a> section of My Myntra.
<br><br>
<p>Thank you for shopping at Myntra.com. Feel free to contact us at http://www.myntra.com/contactus or by calling us at [MYNTRA_CC_PHONE] if you have any questions or need further assistance.</p>
<br/>
Regards,<br/>
Myntra Customer Care');


insert into mk_email_notification(name, subject, body) values('exchange_shipped', 'Your exchange order has been dispatched from the warehouse',
'Hi [USER], <br/><br/>
We would like to inform you that your Myntra.com exchange order has been dispatched from our warehouse. Our delivery representative will arrive at your premises by [DELIVERY_DATE] to deliver the replacement product and pick up the original product from you.<br/><br/>
[SHIPMENT_DETAILS]
At any time, you can track the status of your exchange request at <a href="[MY_ORDERS_URL]">My Orders</a> section of My Myntra.
<br><br>
<p>Feel free to contact us at http://www.myntra.com/contactus or by calling us at [MYNTRA_CC_PHONE] if you have any questions or need further assistance.</p>
<br/>
Regards,<br/>
Myntra Customer Care');


insert into mk_email_notification(name, subject, body) values('exchange_out_for_delivery', 'Your Myntra.com exchange order is out for delivery',
'Hi [USER], <br/><br/>
We would like to inform you that your Myntra.com exchange order is out for delivery. You will receive your exchange order by the end of the day. Our delivery representative will deliver the replacement product to you and simultaneously pick up the original product from you.<br/><br/>
[SHIPMENT_DETAILS]
<p>
At any time, you can track the status of your exchange request at <a href="[MY_ORDERS_URL]">My Orders</a> section of My Myntra.
<br><br>
<p>Feel free to contact us at http://www.myntra.com/contactus or by calling us at [MYNTRA_CC_PHONE] if you have any questions or need further assistance.</p>
<br/>
Regards,<br/>
Myntra Customer Care');


insert into mk_email_notification(name, subject, body) values('exchange_successful_delivery', 'Your Myntra.com exchange order has been delivered',
'Hi [USER], <br/><br/>
Your Myntra.com exchange order has been delivered.<br/><br/>
[SHIPMENT_DETAILS]
<p>Feel free to contact us at http://www.myntra.com/contactus or by calling us at [MYNTRA_CC_PHONE] if you have any questions or need further assistance.</p>
<br/>
<p>We hope you had an enjoyable shopping experience on Myntra.com.<br/><br/>

See you back on Myntra.com.<br/>
Regards,<br/>
Myntra Customer Care');


insert into mk_email_notification(name, subject, body) values('exchange_failed_delivery', 'We attempted to deliver your Myntra.com exchange order',
'Hi [USER], <br/><br/>
We are sorry we were unable to deliver your exchange order on [DELIVERY_DATE]. We will attempt to deliver the order again tomorrow or on the next working day.<br/><br/>
[SHIPMENT_DETAILS]
<p>Feel free to contact us at http://www.myntra.com/contactus or by calling us at [MYNTRA_CC_PHONE] if you have any questions or need further assistance.</p>
<br/>

Regards,<br/>
Myntra Customer Care');
