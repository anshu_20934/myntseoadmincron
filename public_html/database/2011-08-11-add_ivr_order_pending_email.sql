insert into mk_email_notification (name,subject,body) values ('ivr_order_pending','Your order with Myntra.com is Pending Payment! Order No. [ORDER_ID]','Dear [USER], <br/><br/>
Your order with Myntra is Pending Payment. Please call up [IVR_NUMBER] within 24 hours and quote order number [ORDER_ID] to complete the order.<br/><br/>
[ORDER_DETAILS]
<br/>
<br>[CASHBACK_SUMMARY]
<p>Your experience at Myntra.com is important to us. If you have any queries or suggestions, please email us at <a href="mailto:support@myntra.com">support@myntra.com</a>, or call us at [MYNTRA_CC_PHONE]. We look forward to hearing from you!</p>
<br/>
Regards,<br/>
Myntra.com');