/*
SQLyog - Free MySQL GUI v5.19
Host - 5.0.27-community-nt : Database - myntra
*********************************************************************
Server version : 5.0.27-community-nt
*/
use myntra;

/*Table structure for table `mk_fonts` */

DROP TABLE IF EXISTS `mk_fonts`;

CREATE TABLE `mk_fonts` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(64) default NULL,
  `image_name` varchar(64) default NULL,
  `ttf_name` varchar(255) default NULL,
  `display_order` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mk_fonts` */

insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (1,'arial','arial.png','arial.ttf',1);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (2,'arial_narrow','arial_narrow.png','arial_narrow.ttf',2);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (3,'bookman_old_style','bookman_old_style.png','bookman_old_style.ttf',3);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (4,'century','century.png','century.ttf',4);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (5,'georgia','georgia.png','georgia.ttf',5);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (6,'impact','impact.png','impact.ttf',6);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (7,'verdana','verdana.png','verdana.ttf',7);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (8,'symbol','symbol.png','symbol.ttf',8);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (9,'webdings','webdings.png','webdings.ttf',9);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (10,'bamboo','bamboo.png','bamboo.ttf',10);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (11,'hathor','hathor.png','hathor.ttf',11);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (12,'bailey','bailey.png','bailey.ttf',12);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (13,'barbatric','barbatric.png','barbatric.ttf',13);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (14,'hassle','hassle.png','hassle.ttf',14);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (15,'barred_out','barred_out.png','barred_out.ttf',15);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (16,'xtrusion','xtrusion.png','xtrusion.ttf',16);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (17,'hearts','hearts.png','hearts.ttf',17);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (18,'badaboom','badaboom.png','badaboom.ttf',18);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (19,'xerox_malfunction','xerox_malfunction.png','xerox_malfunction.ttf',19);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (20,'charming_font','charming_font.png','charming_font.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (21,'cut_above_rest','cut_above_rest.png','cut_above_rest.ttf',21);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (22,'abduction2002','abduction2002.png','abduction2002.ttf',22);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (23,'armor_piercing','armor_piercing.png','armor_piercing.ttf',23);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (24,'Arrobatherapy','Arrobatherapy.png','Arrobatherapy.ttf',24);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (25,'beat_my_guest','beat_my_guest.png','beat_my_guest.ttf',25);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (26,'BeautySchoolDropout','BeautySchoolDropout.png','BeautySchoolDropout.ttf',26);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (27,'betsy_flanagan','betsy_flanagan.png','betsy_flanagan.ttf',27);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (28,'BoobToobOpen','BoobToobOpen.png','BoobToobOpen.ttf',28);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (29,'chinese_takeaway','chinese_takeaway.png','chinese_takeaway.ttf',29);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (30,'coffee_tin_mf','coffee_tin_mf.png','coffee_tin_mf.ttf',30);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (31,'ComputerAmok','ComputerAmok.png','ComputerAmok.ttf',31);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (32,'debonair_inline_nf','debonair_inline_nf.png','debonair_inline_nf.ttf',32);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (33,'disco_dork','disco_dork.png','disco_dork.ttf',33);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (34,'doktor_terror','doktor_terror.png','doktor_terror.ttf',34);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (35,'Japperneese','Japperneese.png','Japperneese.ttf',35);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (36,'jawbreaker_hard_BRK','jawbreaker_hard_BRK.png','jawbreaker_hard_BRK.ttf',36);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (37,'kelt_caps_freehand','kelt_caps_freehand.png','kelt_caps_freehand.ttf',37);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (38,'Kleptocracy','Kleptocracy.png','Kleptocracy.ttf',38);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (39,'labyrinth_NF','labyrinth_NF.png','labyrinth_NF.ttf',39);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (40,'Vegetable','Vegetable.png','Vegetable.ttf',59);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (41,'Vampiress','Vampiress.png','Vampiress.ttf',58);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (42,'unsteady_oversteer','unsteady_oversteer.png','unsteady_oversteer.ttf',57);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (43,'underground_NF','underground_NF.png','underground_NF.ttf',56);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (44,'Undercover','Undercover.png','Undercover.ttf',55);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (45,'tooney_noodle_NF','tooney_noodle_NF.png','tooney_noodle_NF.ttf',54);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (46,'tantrum_tongue','tantrum_tongue.png','tantrum_tongue.ttf',53);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (47,'shake_that_booty','shake_that_booty.png','shake_that_booty.ttf',52);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (48,'sarcastic_BRK','sarcastic_BRK.png','sarcastic_BRK.ttf',51);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (49,'Radio','Radio.png','Radio.ttf',50);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (50,'QuakeShake','QuakeShake.png','QuakeShake.ttf',49);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (52,'Lewinsky','Lewinsky.png','Lewinsky.ttf',40);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (53,'Luciferius','Luciferius.png','Luciferius.ttf',41);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (54,'Maranallo','Maranallo.png','Maranallo.ttf',42);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (55,'massive_headache','massive_headache.png','massive_headache.ttf',43);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (56,'mystic_prophet','mystic_prophet.png','mystic_prophet.ttf',44);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (57,'Nickodemus-Extremus','Nickodemus-Extremus.png','Nickodemus-Extremus.ttf',45);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (58,'obloquy_solid_BRK','obloquy_solid_BRK.png','obloquy_solid_BRK.ttf',46);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (59,'paint_peel_MF_initials','paint_peel_MF_initials.png','paint_peel_MF_initials.ttf',47);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (60,'words_of_love','words_of_love.png','words_of_love.ttf',60);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (105,'obloquyo','obloquyo.png','obloquyo.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (106,'OCEAVMI_','OCEAVMI_.png','OCEAVMI_.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (107,'oilage','oilage.png','oilage.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (108,'oliver__','oliver__.png','oliver__.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (109,'OogieBoogie','OogieBoogie.png','OogieBoogie.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (110,'osakasans','osakasans.png','osakasans.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (111,'Pacmania','Pacmania.png','Pacmania.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (112,'PAINPMI_','PAINPMI_.png','PAINPMI_.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (113,'PEACE___','PEACE___.png','PEACE___.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (114,'PHATGUY_','PHATGUY_.png','PHATGUY_.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (115,'PIECB___','PIECB___.png','PIECB___.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (116,'PINEM___','PINEM___.png','PINEM___.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (117,'pizzabot','pizzabot.png','pizzabot.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (118,'plain_germanica','plain_germanica.png','plain_germanica.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (119,'PLANETNS','PLANETNS.png','PLANETNS.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (120,'Playtoy','Playtoy.png','Playtoy.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (121,'Pokemon','Pokemon.png','Pokemon.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (122,'Portcullion','Portcullion.png','Portcullion.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (123,'PSAUDI5','PSAUDI5.png','PSAUDI5.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (124,'QUAKE','QUAKE.png','QUAKE.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (125,'queasy','queasy.png','queasy.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (126,'ransom','ransom.png','ransom.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (127,'RCMP2','RCMP2.png','RCMP2.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (128,'reg','reg.png','reg.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (129,'RHUBARBP','RHUBARBP.png','RHUBARBP.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (130,'Ripple_0','Ripple_0.png','Ripple_0.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (131,'ROCKYAOE','ROCKYAOE.png','ROCKYAOE.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (132,'SAMARN__','SAMARN__.png','SAMARN__.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (133,'sarcasti','sarcasti.png','sarcasti.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (134,'SATURN__','SATURN__.png','SATURN__.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (135,'Scood___','Scood___.png','Scood___.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (136,'SEVEMFBR','SEVEMFBR.png','SEVEMFBR.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (137,'SF_Balloons','SF_Balloons.png','SF_Balloons.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (138,'SF_Espresso_Shack_Bold','SF_Espresso_Shack_Bold.png','SF_Espresso_Shack_Bold.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (139,'SF_Pale_Bottom','SF_Pale_Bottom.png','SF_Pale_Bottom.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (140,'Shadowed_Germanica','Shadowed_Germanica.png','Shadowed_Germanica.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (141,'SHAKETHA','SHAKETHA.png','SHAKETHA.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (142,'SHANLN__','SHANLN__.png','SHANLN__.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (143,'She-Creature','She-Creature.png','She-Creature.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (144,'Simpsonfont','Simpsonfont.png','Simpsonfont.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (145,'SNAKV___','SNAKV___.png','SNAKV___.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (146,'SpitShine','SpitShine.png','SpitShine.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (147,'Starjedi','Starjedi.png','Starjedi.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (148,'Symbnerv','Symbnerv.png','Symbnerv.ttf',20);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (150,'neonsign','neonsign.png','neonsign.ttf',1);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (151,'leopold','leopold.png','leopold.ttf',2);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (152,'brush','brush.png','brush.ttf',3);
insert into `mk_fonts` (`id`,`name`,`image_name`,`ttf_name`,`display_order`) values (153,'comic','comic.png','comic.ttf',1);