update mk_sr_categories set category_type="order";
update mk_sr_categories set category_type="non-order" where SR_category in ('Coupon','Product Information','Customer Information','VOC');

alter table mk_sr_subcategories drop index SR_subcategory_UNIQUE;

alter table mk_order_sr change compensation_amount compensation_value text default null;
alter table mk_order_sr add column compensation_type varchar(200) default null after compensation;
alter table mk_order_sr add column refund decimal(10,2) default null after customer_email;

alter table mk_sr_tracker add column modified_date int(11) default null after due_date, add column updatedby varchar(100) default null after assignee;
alter table mk_sr_tracker add column sr_active enum('Y','N') default 'Y';
alter table mk_sr_tracker add column deletedby varchar(100) default null;

alter table mk_sr_categories add column cat_active enum('Y','N') default 'Y';
alter table mk_sr_subcategories add column subcat_active enum('Y','N') default 'Y';
alter table mk_sr_priority add column priority_active enum('Y','N') default 'Y';
alter table mk_sr_status add column status_active enum('Y','N') default 'Y';