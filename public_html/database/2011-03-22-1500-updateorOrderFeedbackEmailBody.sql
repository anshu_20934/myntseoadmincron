update mk_email_notification set body='<b>Order Details</b>
<ul>
<li>
Order ID: <a href="[ORDERIDURL]">[ORDERID]</a>
</li>
<li>
Order Date: [ORDERDATE]
</li>
<li>
Shipped Date: [SHIPDATE]
</li>
<li>
Shipping Promise: [SHIPPROMISE]
</li>
<li>
Products Ordered: [PRODUCTS]
</li>
<li>
City, State & Country: [CITY], [STATE] & [COUNTRY]
</li>
<li>
Gift Option: [GIFTOPTION]
</li>
<li>
Payment Method: [PAYMENTMETHOD]
</li>
<li>
Courier Provider: [COURIERPROVIDER]
</li>
</ul>
<br/>
<br/>
User [USER] has sent following feedback:<br><br>
<ol>
<li>
It was quite easy to browse and locate the product on website.<span style="color:red;">[ANS1]</span>
</li>
<li>
 The online payment process was smooth (applicable for online payment transaction). <span style="color:red;">[ANS2]</span>
</li>
<li>
Received a prompt response and resolution, whenever contacted Myntra Customer Care team. <span style="color:red;">[ANS3]</span>
</li>
<li>
 Product was delivered as per the committed delivery time. <span style="color:red;">[ANS4]</span>
</li>
<li>
Product was delivered as per the expected standards. <span style="color:red;">[ANS5]</span>
</li>
<li>
Would consider shopping with Myntra again <span style="color:red;">[ANS6]</span>
</li>
<li>
Bought this product as a gift for someone<span style="color:red;">[ANS7]</span>
</li>
<li>
Do you have any feedback or suggestions for the myntra.com? If yes, please describe below.<br>
<span style="color:red;">[ANS8]</span>
</li>
</ol>' where name = 'feedback_survey';
