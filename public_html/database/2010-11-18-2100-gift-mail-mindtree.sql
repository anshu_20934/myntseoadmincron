insert into mk_email_notification (name,subject,body) values('gift_order_mindtree','Our Birthday Wishes & Your Order has been placed','</p>Dear [USER],
<br/>
<p>We wish you a Happy Birthday once again. We thank you for choosing to redeem your birthday gift.
Your Order ID is [ORDID]. Your order has been placed successfully. Your gift will be shipped to the address provided on the site.
You will receive an email with the shipment tracking number once the product has been shipped out. [RECEIVEDATE].
<br/>On any issues you can call us on [PHONE] or write to us at support@myntra.com.</p><br/><p>Thanks,<br/>MindTree team</p>');
