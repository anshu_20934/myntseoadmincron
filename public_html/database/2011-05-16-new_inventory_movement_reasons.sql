insert into mk_inventory_movement_reasons (text, action_type, inv_update, wh_update, is_for_system, code) values ('Item Cancelled', 'ICAI', 1, 0, 1, 'IC');

insert into mk_inventory_movement_reasons (text, action_type, inv_update, wh_update, is_for_system, code) values ('Cancelled Item Added', 'CIA', 1, 0, 1, 'CIA');
