
alter table daily_winners add COLUMN `login` varchar(100) NOT NULL;

alter table weekly_winners add COLUMN `login` varchar(100) NOT NULL;

alter table user_tickets add COLUMN `login` varchar(100) NOT NULL;

alter table user_tickets drop column `week`;

alter table user_tickets add column `start_date` date NOT NULL;

alter table user_tickets add column `end_date` date NOT NULL;



