create table recommendation_rules_output_temp(select * from recommendation_rules_output);
create table recommendation_rules_input_temp(select * from recommendation_rules_input);
create table recommendation_rules_mapping_temp(select * from recommendation_rules_mapping);


update recommendation_rules_input set global_attr_gender = "Men" where id=55;
update recommendation_rules_input set global_attr_gender = "Men" where id=138;
update recommendation_rules_input set global_attr_gender = "Men" where id=218;
update recommendation_rules_input set global_attr_gender = "Men" where id=14;
update recommendation_rules_input set global_attr_gender = "Men" where id=66;


insert into recommendation_rules_input(global_attr_article_type, global_attr_sub_category, global_attr_master_category, global_attr_gender) values(321, 320, 212, "Men");
insert into recommendation_rules_input(global_attr_article_type, global_attr_sub_category, global_attr_master_category, global_attr_gender) values(323, 320, 212, "Men");
insert into recommendation_rules_input(global_attr_article_type, global_attr_sub_category, global_attr_master_category, global_attr_gender) values(327, 320, 212, "Men");
insert into recommendation_rules_input(global_attr_article_type, global_attr_sub_category, global_attr_master_category, global_attr_gender) values(325, 213, 212, "Men");
insert into recommendation_rules_input(global_attr_article_type, global_attr_sub_category, global_attr_master_category, global_attr_gender) values(364, 355, 212, "Men");
insert into recommendation_rules_input(global_attr_article_type, global_attr_sub_category, global_attr_master_category, global_attr_gender) values(338, 330, 212, "Men");


insert into recommendation_rules_output(output_rule) values ('[{"article_type" : "Divan Set"}]');
insert into recommendation_rules_output(output_rule) values ('[{"article_type" : "Curtain"}]');
insert into recommendation_rules_output(output_rule) values ('[{"article_type" : "Vases"}]');
insert into recommendation_rules_output(output_rule) values ('[{"article_type" : "Bath Mats"}]');
insert into recommendation_rules_output(output_rule) values ('[{"article_type" : "Ornaments"}]');
insert into recommendation_rules_output(output_rule) values ('[{"article_type" : "Bathroom Accessories"}]');

DELETE FROM recommendation_rules_mapping WHERE input_rule_id=282;
DELETE FROM recommendation_rules_mapping WHERE input_rule_id=283;
DELETE FROM recommendation_rules_mapping WHERE input_rule_id=284;
DELETE FROM recommendation_rules_mapping WHERE input_rule_id=285;
DELETE FROM recommendation_rules_mapping WHERE input_rule_id=286;
DELETE FROM recommendation_rules_mapping WHERE input_rule_id=55;
DELETE FROM recommendation_rules_mapping WHERE input_rule_id=239;
DELETE FROM recommendation_rules_mapping WHERE input_rule_id=138;
DELETE FROM recommendation_rules_mapping WHERE input_rule_id=287;
DELETE FROM recommendation_rules_mapping WHERE input_rule_id=218;
DELETE FROM recommendation_rules_mapping WHERE input_rule_id=14;
DELETE FROM recommendation_rules_mapping WHERE input_rule_id=66;


insert into recommendation_rules_mapping(input_rule_id, output_rule_id) values (282, 96) ;
insert into recommendation_rules_mapping(input_rule_id, output_rule_id) values (283, 97) ;
insert into recommendation_rules_mapping(input_rule_id, output_rule_id) values (284, 98) ;
insert into recommendation_rules_mapping(input_rule_id, output_rule_id) values (285, 16) ;
insert into recommendation_rules_mapping(input_rule_id, output_rule_id) values (286, 97) ;
insert into recommendation_rules_mapping(input_rule_id, output_rule_id) values (55, 99) ;
insert into recommendation_rules_mapping(input_rule_id, output_rule_id) values (239, 93) ;
insert into recommendation_rules_mapping(input_rule_id, output_rule_id) values (239, 101) ;
insert into recommendation_rules_mapping(input_rule_id, output_rule_id) values (138, 100) ;
insert into recommendation_rules_mapping(input_rule_id, output_rule_id) values (287, 98) ;
insert into recommendation_rules_mapping(input_rule_id, output_rule_id) values (218, 16) ;
insert into recommendation_rules_mapping(input_rule_id, output_rule_id) values (14, 70) ;
insert into recommendation_rules_mapping(input_rule_id, output_rule_id) values (66, 16) ;


