CREATE TABLE `mk_payments_log` (
  `id` int(31) NOT NULL AUTO_INCREMENT,
  `orderid` int(20) NOT NULL,
  `login` varchar(128) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `payment_gateway_name` varchar(64) NOT NULL,
  `payment_option` varchar(8) NOT NULL,
  `payment_issuer` varchar(32),
  `is_inline` tinyint(1),
  `time_insert` int(11),
  `time_return` int(11),
  `time_transaction` int(11),
  `is_tampered` tinyint(1),
  `is_complete` tinyint(1),
  `is_flagged` tinyint(1),
  `response_code` varchar(16),
  `response_message` text,  
  `bank_transaction_id` varchar(32),
  `gateway_payment_id` varchar(32),  
  `completed_via` varchar(32),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

alter  table mk_payments_log add index `orderid` (orderid);
alter  table mk_payments_log add index `login` (login);

