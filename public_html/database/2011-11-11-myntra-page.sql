drop table if exists parameterized_page;
CREATE TABLE `parameterized_page` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `page_key` varchar(32) NOT NULL,
  `parameter_string` varchar(5000) NOT NULL,
  `parameter_hash_key` varchar(32) NOT NULL,
  `page_desc` varchar(50) NOT NULL,
  PRIMARY KEY (id),
  KEY `parameter_hash_idx` (`parameter_hash_key`)
);

drop table if exists landing_page;
CREATE TABLE `landing_page` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `page_url` varchar(32) NOT NULL,
  `parameterized_page_id` int(20) NOT NULL,		
  PRIMARY KEY (id),
  KEY `page_url_idx` (`page_url`)
);


