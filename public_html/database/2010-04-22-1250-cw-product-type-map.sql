use myntra;

drop table if exists mk_org_product_type_map;

CREATE TABLE `mk_org_product_type_map` (                             
  `product_type_id` int(11) unsigned not null,                                     
  `orgid` int(11) unsigned not null,                           
  `consignment_capacity` decimal(4,2) default null,
  PRIMARY KEY  (`product_type_id`,`orgid`)                                                                                              
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC; 

insert into mk_org_product_type_map (product_type_id,orgid,consignment_capacity) values ('219','25',0.00);