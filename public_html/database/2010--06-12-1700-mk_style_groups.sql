use myntra;
alter table mk_style_group add column url varchar(2000);
alter table mk_style_group add column image_path varchar(2000);
alter table mk_style_group add column description varchar(5000);
alter table mk_style_group add column title varchar(2000);