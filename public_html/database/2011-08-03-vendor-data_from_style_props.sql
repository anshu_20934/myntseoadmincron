UPDATE mk_skus t4 SET vendor_article_no =
( SELECT vendor_article_number_value FROM
	(
	SELECT t1.id sku_id, t3.article_number vendor_article_number_value 
	FROM mk_skus t1,mk_styles_options_skus_mapping t2, mk_style_properties t3
	WHERE t3.style_id = t2.style_id
	AND t2.sku_id = t1.id
	AND t1.id != 3923
	) AS nested
	WHERE sku_id = t4.id
)
WHERE t4.vendor_article_no IS NULL;


UPDATE mk_skus t4 SET vendor_article_name =
( SELECT vendor_article_name_value FROM
	(
	SELECT t1.id sku_id, t3.product_display_name vendor_article_name_value 
	FROM mk_skus t1,mk_styles_options_skus_mapping t2, mk_style_properties t3
	WHERE t3.style_id = t2.style_id
	AND t2.sku_id = t1.id
	AND t1.id != 3923
	) AS nested
	WHERE sku_id = t4.id
)
WHERE t4.vendor_article_name IS NULL;
