update mk_inventory_movement_reasons set text = 'Manual JIT Enable', code = 'MJE' where code = 'JITE';

update mk_inventory_movement_reasons set text = 'Manual JIT Disable', code = 'MJD' where code = 'JITD';

insert into mk_inventory_movement_reasons (text, action_type, inv_update, wh_update, is_for_system, code, action_mode_manual) values ('Auto Enable JIT', 'N', 0, 0, 1, 'AJE', 0);

insert into mk_inventory_movement_reasons (text, action_type, inv_update, wh_update, is_for_system, code, action_mode_manual) values ('Auto Disable JIT', 'N', 0, 0, 1, 'AJD', 0);

