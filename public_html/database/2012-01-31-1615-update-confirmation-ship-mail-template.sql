/*order confirmation mail*/
update mk_email_notification set body='<div style="border: 1px solid gray; padding: 6px">
Dear [USER], <br/>
<p>This is to confirm that your order with Myntra.com has been successfully placed! Your Order No. is <b>[ORDER_ID]</b>. Please use this for future reference.</p>
<p>Your order is being processed now and we will ship/dispatch it from our warehouse within the next 24 hours. Your order will be delivered by <b>[DELIVERY_DATE]</b>.
We will confirm the delivery date to you in the shipping confirmation email you will receive once the order is shipped.</p>
<br/>[ORDER_DETAILS]<br/>
The delivery address is:<br>[DELIVERY_ADDRESS]<br><br>
[CASHBACK_SUMMARY]
<p>We would like to thank you for buying from www.myntra.com. We hope you enjoyed the experience and we look forward to having you back!</p>
<p><strong>Please note that changes to orders cannot be made once the order is shipped. In case of any further queries, please get in touch with us at [MYNTRA_CC_PHONE].</strong></p>
Regards,<br/>
Myntra Customer Connect<br/>
India\'s Largest Online Fashion Store
<br/><br/>[DECLARATION]<br/>
</div>' where name='order_confirmation';
/*order confirmation mail*/


/*order ship mail third party logistic*/
update mk_email_notification set body='<div style="border: 1px solid gray; padding: 6px">
Hi [USER], <br/>
<p>Your order with Myntra has been processed and shipped/dispatched from our warehouse. You will receive your product by <b>[DELIVERY_DATE]</b>.</p>
<p>Here are the tracking details:<br/>
Courier Partner:  [COURIER_SERVICE]<br/>
Tracking Code: [TRACKING_NUMBER]
</p>
<p>You can track the shipment progress of your order in the <a href="http://www.myntra.com/mymyntra.php">My Orders</a> section of My Myntra,
or directly on the shipping vendor\'s website at - [COURIER_WEBSITE].
Please note that it may take upto 24 hours before your order tracking number reflects on our partner website and at the MyOrders section of My Myntra.</p>
<p>Here are the details of Order No. [ORDER_ID], for your reference -</p>[ORDER_DETAILS]<br>
<p>At Myntra, we make every possible effort to ensure that you receive your product on time. Please help us to deliver on time by considering the following:
<ul style="list-style-type:decimal;">
<li>Do make yourself available to receive the order on and before the specified delivery date</li>
<li>In case you are unavailable, do make arrangements to have the order received by someone on your behalf at your delivery address.</li>
<li>If you have placed a COD order, do keep the amount due ready and at hand to pay our delivery representative while you receive the order.</li>
</ul>
We would like to thank you for buying from www.myntra.com. We hope you enjoyed the experience and we look forward to having you back!
</p>
<p>In case of any further queries, please get in touch with us at [MYNTRA_CC_PHONE].</p>
Regards,<br/>
Myntra Customer Connect<br/>
India\'s Largest Online Fashion Store
<br/><br/>[DECLARATION]<br/>
</div>' where name='order_shipped_confirmation';
/*order ship mail third party logistic*/



/*order ship mail myntra logistic*/
update mk_email_notification set body='<div style="border: 1px solid gray; padding: 6px">
Dear [USER],<br>
<p>Your order with Order ID [ORDER_ID] has been processed and shipped/dispatched from our warehouse through <b>Myntra Logistics</b>. You will receive your product by <b>[DELIVERY_DATE]</b>.</p>
<p>Here are the details of Order No. [ORDER_ID], for your reference -</p>[ORDER_DETAILS]<br>
<p>At Myntra, we make every possible effort to ensure that you receive your product on time. Please help us to deliver on time by considering the following:
<ul style="list-style-type:decimal;">
<li>Do make yourself available to receive the order on and before the specified delivery date</li>
<li>In case you are unavailable, do make arrangements to have the order received by someone on your behalf at your delivery address.</li>
<li>If you have placed a COD order, do keep the amount due ready and at hand to pay our delivery representative while you receive the order.</li>
</ul>
We would like to thank you for buying from www.myntra.com. We hope you enjoyed the experience and we look forward to having you back!
</p>
<p>In case of any further queries, please get in touch with us at [MYNTRA_CC_PHONE].</p>
Regards,<br/>
Myntra Customer Connect<br/>
India\'s Largest Online Fashion Store
<br/><br/>[DECLARATION]<br/>
</div>' where name='orderprocessedandhanddelivered';
/*order ship mail myntra logistic*/