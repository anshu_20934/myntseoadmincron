insert into mk_source_types (type_name, active) values('Team India Store','1');
insert into mk_sources (
	source_type_id,location_id,source_code,source_name,
	source_address,source_sync_username,source_sync_password,
	source_manager_email,active,synchronizable )
values((select type_id from mk_source_types where type_name='Team India Store'),'14','CTS-TEAM-INDIA-STORE','Cognizant Associates',
	'Myntra 19th Main HSR Layout','myntra','bd0e6915956aed7b1ae3f9a274a63502',
	'Sajid.Hussain@cognizant.com','1','1');
update brand_store_details set source_id=(select source_id from mk_sources where source_code='CTS-TEAM-INDIA-STORE') where username='CTS-TEAM-INDIA-STORE';
