insert into mk_widget_key_value_pairs(`key`, `value`,`description`) values ('shipping.charges.amount', '49', 'Shipping amount to be charged on a cart'); 
insert into mk_widget_key_value_pairs(`key`, `value`,`description`) values ('shipping.charges.cartlimit', '500', 'Shipping amount to be charged on cart with amount less than this value'); 
