CREATE TABLE cms_catalog_timer
(
    style_id int not null,
    login varchar(255) not null,
    role varchar(255) not null,
    start int(11),
    end int(11),
    time_taken int(11),
    article_type_id int(11),
    article_type varchar(255)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
