update mk_return_state_transitions set to_state = 'RRRS', description='Reshipping rejected returns' where from_state='RJDC' and to_state='CFDC';

update mk_return_state_transitions set to_state = 'RRC', description='Returns Received at WH' where from_state='RJDC' and to_state='CPDC';

delete from mk_return_state_transitions where from_state in ('CPDC', 'CFDC');

delete from mk_return_status_details where code in ('CPDC', 'CFDC');

update mk_return_state_transitions set to_state='RIS', description='Restocking returned item' where from_state='RQP' and to_state='RFI';

update mk_return_state_transitions set to_state='RIS', description='Restocking returned item' where from_state='RQSP' and to_state='RFI';

update mk_return_state_transitions set to_state='RIS', description='Restocking returned item' where from_state='RQSF' and to_state='RQCP';

delete from mk_return_state_transitions where to_state in ('RQCP', 'RFI')  or from_state in ('RQCP', 'RFI');

delete from mk_return_status_details where code in ('RQCP', 'RFI', 'RQCF');

update mk_return_state_transitions set from_state = 'RQSF' where from_state = 'RQCF' and to_state = 'RRRS';

update mk_return_state_transitions set from_state = 'RQSF' where from_state = 'RQCF' and to_state = 'RRJ';

delete from mk_return_state_transitions where from_state = 'RQSF' and to_state = 'RQCF';

alter table xcart_returns add refundeddate int(11) DEFAULT NULL;
