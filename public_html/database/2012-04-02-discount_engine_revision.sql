ALTER TABLE mk_widget_key_value_pairs ADD UNIQUE (`key`);
ALTER TABLE mk_abtesting_tests  ADD UNIQUE (`name`);
ALTER TABLE mk_abtesting_variations  ADD UNIQUE (ab_test_id,`name`);
alter table mk_widget_key_value_pairs modify `key` varchar(45);

insert into mk_widget_key_value_pairs(`key`, `value`, `description`) values ('discountEngine.change.revisionNumber', '1', 'Discount Engine Change revision number');