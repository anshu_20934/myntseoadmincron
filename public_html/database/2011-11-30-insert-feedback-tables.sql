insert into mk_feedbacks (name,created_date,emailto) values ('voc_order', unix_timestamp(now()),'arun.kumar@myntra.com');
insert into mk_feedbacks (name,created_date,emailto) values ('voc_SR', unix_timestamp(now()),'arun.kumar@myntra.com');

update mk_feedbacks set selected_SR_FB=1, survey_email='SR_feedback@myntra.com' where name='voc_SR';
update mk_feedbacks set selected_order_FB=1, survey_email='alrt_userfeedback@myntra.com' where name='voc_order';


insert into mk_feedback_questions (feedbackid,question,question_type,display_order,mandatory) values ((select feedbackid from mk_feedbacks where name='voc_order'),'Please rate your overall satisfaction with Myntra.com based on your experience with this order?','radio',1,1);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Dissatisfied',0,5,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Dissatisfied',0,4,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Neutral',0,3,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Satisfied',0,2,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Satisfied',0,1,null);

insert into mk_feedback_questions (feedbackid,question,question_type,display_order,mandatory) values ((select feedbackid from mk_feedbacks where name='voc_order'),'Based on your experience, how likely are you to recommend Myntra.com to your friends?','radio',2,1);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Unlikely',0,5,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Unlikely',0,4,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Neutral',0,3,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Likely',0,2,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Likely',0,1,null);

insert into mk_feedback_questions (feedbackid,question,question_type,display_order,mandatory) values ((select feedbackid from mk_feedbacks where name='voc_order'),'How easy was it for you to find what you were looking for?','radio',3,1);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Difficult',0,5,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Difficult',0,4,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Neutral',0,3,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Easy',0,2,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Easy',0,1,null);

insert into mk_feedback_questions (feedbackid,question,question_type,display_order,mandatory) values ((select feedbackid from mk_feedbacks where name='voc_order'),'Please rate how happy you were with the range of items (Catalog) available at Myntra.com?','radio',4,1);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Dissatisfied',0,5,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Dissatisfied',0,4,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Neutral',0,3,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Satisfied',0,2,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Satisfied',0,1,null);

insert into mk_feedback_questions (feedbackid,question,question_type,display_order,mandatory) values ((select feedbackid from mk_feedbacks where name='voc_order'),'Please rate your checkout (Cart and Payment Process) experience at Myntra?','radio',5,1);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Difficult',0,5,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Difficult',0,4,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Neutral',0,3,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Good',0,2,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Good',0,1,null);

insert into mk_feedback_questions (feedbackid,question,question_type,display_order,mandatory) values ((select feedbackid from mk_feedbacks where name='voc_order'),'If you contacted our Customer Support team, please rate your satisfaction?','radio',6,1);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Dissatisfied',0,5,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Dissatisfied',0,4,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Neutral',0,3,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Satisfied',0,2,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Satisfied',0,1,null);

insert into mk_feedback_questions (feedbackid,question,question_type,display_order,mandatory) values ((select feedbackid from mk_feedbacks where name='voc_order'),'How satisfied were you with the time taken to deliver your order from the time you placed it?','radio',7,1);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Dissatisfied',0,5,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Dissatisfied',0,4,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Neutral',0,3,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Satisfied',0,2,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Satisfied',0,1,null);

insert into mk_feedback_questions (feedbackid,question,question_type,display_order,mandatory) values ((select feedbackid from mk_feedbacks where name='voc_order'),'Were you happy with the quality of the product delivered to you?','radio',8,1);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Unhappy',0,5,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Unhappy',0,4,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Neutral',0,3,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Happy',0,2,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Happy',0,1,null);

insert into mk_feedback_questions (feedbackid,question,question_type,display_order,mandatory) values ((select feedbackid from mk_feedbacks where name='voc_order'),'Do you have any feedback for Myntra.com? If yes, please describe below:','text',9,0);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),null,0,1,250);

insert into mk_feedback_questions (feedbackid,question,question_type,display_order,mandatory) values ((select feedbackid from mk_feedbacks where name='voc_SR'),'Please rate your satisfaction with the quality of service you received from Myntra.com on this particular issue:','radio',1,1);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Dissatisfied',0,5,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Dissatisfied',0,4,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Neutral',0,3,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Satisfied',0,2,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Satisfied',0,1,null);

insert into mk_feedback_questions (feedbackid,question,question_type,display_order,mandatory) values ((select feedbackid from mk_feedbacks where name='voc_SR'),'As a customer, how much effort did you need to put into solving this issue?','radio',2,1);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very High',0,5,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'High',0,4,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Moderate',0,3,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Low',0,2,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Low',0,1,null);

insert into mk_feedback_questions (feedbackid,question,question_type,display_order,mandatory) values ((select feedbackid from mk_feedbacks where name='voc_SR'),'Please rate your satisfaction with the Customer Support Representative.','radio',3,1);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Dissatisfied',0,5,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Dissatisfied',0,4,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Neutral',0,3,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Satisfied',0,2,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Satisfied',0,1,null);

insert into mk_feedback_questions (feedbackid,question,question_type,display_order,mandatory) values ((select feedbackid from mk_feedbacks where name='voc_SR'),'Was the Customer Support Representative knowledgable about the issue?','radio',4,1);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Yes',0,1,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'No',0,2,null);

insert into mk_feedback_questions (feedbackid,question,question_type,display_order,mandatory) values ((select feedbackid from mk_feedbacks where name='voc_SR'),'Did the Customer Support Representative treat you as a Valued Customer?','radio',5,1);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Yes',0,1,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'No',0,2,null);

insert into mk_feedback_questions (feedbackid,question,question_type,display_order,mandatory) values ((select feedbackid from mk_feedbacks where name='voc_SR'),'Did the Customer Support Representative take ownership and communicate progress while helping you resolve the issue?','radio',6,1);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Yes',0,1,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'No',0,2,null);

insert into mk_feedback_questions (feedbackid,question,question_type,display_order,mandatory) values ((select feedbackid from mk_feedbacks where name='voc_SR'),'How satisfied were you with the solution provided to you by the Customer Support Representative?','radio',7,1);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Dissatisfied',0,5,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Dissatisfied',0,4,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Neutral',0,3,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Satisfied',0,2,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Satisfied',0,1,null);

insert into mk_feedback_questions (feedbackid,question,question_type,display_order,mandatory) values ((select feedbackid from mk_feedbacks where name='voc_SR'),'How satisfied were you with the amount of time it took to resolve this issue?','radio',8,1);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Dissatisfied',0,5,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Dissatisfied',0,4,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Neutral',0,3,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Satisfied',0,2,null);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),'Very Satisfied',0,1,null);

insert into mk_feedback_questions (feedbackid,question,question_type,display_order,mandatory) values ((select feedbackid from mk_feedbacks where name='voc_SR'),'What can Myntra do to improve the quality of service, the interaction with support and the progress?','text',9,0);
insert into mk_feedback_options (questionid,optionname,opt_default,display_order,maxchars) values ((select max(questionid) from mk_feedback_questions),null,0,1,250);

update mk_email_notification set name="voc_order" , body='<br/>
Hi [USER], <br/><br/>
We hope that you are delighted with the product(s) for the Order No. [ORDER_ID] which we delivered to you on [DELIVERY_DATE].<br/></br>
As a constant endeavour to improve our services, we would request you to share with us some feedback on your shopping experience with Myntra.<br/><br/>
As a simple gesture of thanks, we will e-mail you a Rs 100 coupon as soon as you share your feedback.<br/><br/>
[ORDER_MFB_FORM]<br/>[REDIRECT_INFO]<br/>
<p>For any further clarification, kindly contact us at [MYNTRA_CC_PHONE].</p>

Regards,<br/>
Myntra Customer Care<br/><br/>[DECLARATION]' where name='feedback_request';

insert into mk_email_notification (name,subject,body) values('voc_SR','Request for Feedback for your interaction with Myntra Customer Service','<br/>
Hi [USER], <br/><br/>
We notice that you spoke to our Customer Support on [SR_CREATE_DATE]. Our records indicate that this was successfully resolved on [SR_CLOSED_DATE].<br/><br/>
As a constant endeavour to improve our services, we would request you to share with us some feedback on your customer service experience with Myntra.<br/><br/>
[SR_MFB_FORM]<br/>[REDIRECT_INFO]<br/>
<p>For any further clarification, kindly contact us at [MYNTRA_CC_PHONE].</p>

Regards,<br/>
Myntra Customer Care<br/><br/>[DECLARATION]');

update mk_email_notification set name="voc_order_survey" , subject='[USER] has sent feedback for order : [ORDERID]', body='<br/>
<div style="width:100%;height:auto;background-color:#eee;">
	<div style="width:auto;height:auto;padding:10px">
	<span><strong>Order Details:</strong><span>
		<ul style="list-style-type:none;">
			<li style="margin-bottom:10px;">
				<span>Customer Name :</span>
				<label>[USER]</label>
			</li>
			<li style="margin-bottom:10px;">
				<span>Order Id :</span>
				<label>[ORDERID]</label>
			</li>					
			<li style="margin-bottom:10px;">
				<span>Order Date :</span>
				<label>[DATE]</label>
			</li>
			<li style="margin-bottom:10px;">
				<span>Ship Date :</span>
				<label>[SHIP_DATE]</label>
			</li>
			<li style="margin-bottom:10px;">
				<span>Delivery Date :</span>
				<label>[DELIVERY_DATE]</label>
			</li>
			<li style="margin-bottom:10px;">
				<span>Payment Method :</span>
				<label>[PAYMENT_METHOD]</label>
			</li>
			<li style="margin-bottom:10px;">
				<span>Courier :</span>
				<label>[LOGISTIC]</label>
			</li>
		</ul>
	</div>
</div>
<br/>
[ORDER_MFB_SURVEY]<br/>
Regards,<br/>
Myntra Feedback Engine<br/><br/>' where name='feedback_survey';

insert into mk_email_notification (name,subject,body) values("voc_SR_survey" , '[USER] has sent feedback for SR : [SRID]', '<br/>
<div style="width:100%;height:auto;background-color:#eee;">
	<div style="width:auto;height:auto;padding:10px">
	<span><strong>SR Details:</strong><span>
		<ul style="list-style-type:none;">
			<li style="margin-bottom:10px;">
				<span>Customer Name :</span>
				<label>[USER]</label>
			</li>
			<li style="margin-bottom:10px;">
				<span>SR Id :</span>
				<label>[SRID]</label>
			</li>					
			<li style="margin-bottom:10px;">
				<span>SR Category :</span>
				<label>[CATEGORY]</label>
			</li>			
			<li style="margin-bottom:10px;">
				<span>SR Created Date :</span>
				<label>[SR_CREATE_DATE]</label>
			</li>
			<li style="margin-bottom:10px;">
				<span>Created By :</span>
				<label>[CREATEDBY]</label>
			</li>
			<li style="margin-bottom:10px;">
				<span>SR Closed Date :</span>
				<label>[SR_CLOSE_DATE]</label>
			</li>
			<li style="margin-bottom:10px;">
				<span>Closed By :</span>
				<label>[CLOSEDBY]</label>
			</li>			
		</ul>
	</div>
</div>
<br/>
[SR_MFB_SURVEY]<br/>
Regards,<br/>
Myntra Feedback Engine<br/><br/>');

update mk_email_notification set body='Dear [USER],<br><br>
<p>Your order with Order ID [ORDER_ID] has been processed and shipped out through <b>Myntra Logistics</b>. You should be receiving your product within the next 2 working days.</p>
<p>Here are the details of Order No. [ORDER_ID], for your reference -</p>
[ORDER_DETAILS]
<br>
<p>We would like to thank you for buying from <a href="www.myntra.com">www.myntra.com</a>. We hope you enjoyed the experience and <b>we look forward to having you back!</b></p>
<p>In case of any further queries, please get in touch with us at [MYNTRA_CC_PHONE].</p>
<br>
Regards,<br>
Myntra Customer Care' where name='orderprocessedandhanddelivered';

update mk_email_notification set body='Hi [USER], <br/><br/>
Your order with Myntra has been processed and shipped. Here are the tracking details:<br/><br/>
Courier Partner:  [COURIER_SERVICE]<br/>
Tracking Code: [TRACKING_NUMBER]<br/>
<br/>
You can track the shipment progress of your order in the <a href="http://www.myntra.com/mymyntra.php">My Orders</a> section of My Myntra, 
or directly on the shipping vendor\'s website at - [COURIER_WEBSITE]. 
Please note that it may take few hours before your order tracking details are updated and displayed here.<br/><br/>

Here are the details of Order No. [ORDER_ID], for your reference -
<br/><br/>
[ORDER_DETAILS]
<br/>
<p>In case of any further queries, please get in touch with us at [MYNTRA_CC_PHONE].</p>
<br/>
Regards,<br/>
Myntra Customer Care' where name='order_shipped_confirmation';
