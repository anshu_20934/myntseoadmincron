

insert into leader_board values (2,"leaderboard for Dec fest",unix_timestamp('2012-12-1 00:00:00'),unix_timestamp('2012-12-1 16:00:00'),unix_timestamp('2012-12-18 13:30:00'),unix_timestamp('2012-11-18 17:00:00'),1,"raghu",unix_timestamp('2012-11-18'));

CREATE TABLE `daily_winners`(
id int(11) NOT NULL AUTO_INCREMENT,
name varchar(200) NOT NULL,
`date` date NOT NULL,
amount decimal(12,2) NOT NULL DEFAULT '0.00',
position TINYINT NOT NULL DEFAULT '0',
category VARCHAR(5) NOT NULL DEFAULT 'O',
 PRIMARY KEY (`id`) 
) ENGINE=InnoDB;


CREATE TABLE `weekly_winners`(
id int(11) NOT NULL AUTO_INCREMENT,
week TINYINT NOT NULL DEFAULT '1',
name varchar(200) NOT NULL,
amount decimal(12,2) NOT NULL DEFAULT '0.00',
 PRIMARY KEY (`id`) 
) ENGINE=InnoDB;



CREATE TABLE `user_tickets`(
id int(11) NOT NULL AUTO_INCREMENT,
week TINYINT NOT NULL DEFAULT '1',
name varchar(200) NOT NULL,
tickets int(11) NOT NULL DEFAULT 0,
 PRIMARY KEY (`id`) 
) ENGINE=InnoDB;




