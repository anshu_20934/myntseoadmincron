CREATE TABLE mk_queue_retry_data (
  id int(11) NOT NULL AUTO_INCREMENT,
  queue_name varchar(256) not null,
  format varchar(50) not null,
  props varchar(512),
  data text not null,
  root_element varchar(256),
  status varchar(10),
  created_on int(11),
  PRIMARY KEY (id),
  KEY idx_qrd_status (status),
  KEY idx_qrd_date (created_on)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


