alter table mk_payments_log add column amountPaid varchar(20) null;
alter table mk_payments_log add column amountToBePaid varchar(20) null;
alter table mk_payments_log add column gatewayResponse text null;
