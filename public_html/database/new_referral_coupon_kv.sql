INSERT INTO `mk_feature_gate_key_value_pairs` ( `key`, `value`, `description`) VALUES
('mrp.firstLogin.fbreg.mrpPercentage', '25', 'percentage discount for mrp firstLogin fbreg');

INSERT INTO `mk_feature_gate_key_value_pairs` ( `key`, `value`, `description`) VALUES
('mrp.refFirstPurchase.couponType', 'dual', 'coupon type for mrp firstLogin fbreg');


INSERT INTO `mk_feature_gate_key_value_pairs` ( `key`, `value`, `description`) VALUES
('mrp.refRegistration.mrpPercentage', '25', 'percentage discount for mrp firstLogin fbreg');

INSERT INTO `mk_feature_gate_key_value_pairs` ( `key`, `value`, `description`) VALUES
('mrp.refRegistration.couponType', 'dual', 'coupon type for mrp firstLogin fbreg');
