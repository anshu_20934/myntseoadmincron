UPDATE mk_style_properties SET product_tags = REPLACE(product_tags, ', ', ',') WHERE product_tags LIKE '%, %';
UPDATE mk_style_properties SET product_tags = REPLACE(product_tags, ' ,', ',') WHERE product_tags LIKE '% ,%';
