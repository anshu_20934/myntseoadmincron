alter table mk_attribute_type add column is_filter TINYINT(1) NOT NULL default 0;
update  mk_attribute_type set is_filter=0 where 1=1;
update  mk_attribute_type set is_filter=1 where id in(82,85,86,90,95,94,43,46,47,52,51,56,20,70,112,113);