-- MySQL dump 10.13  Distrib 5.1.41, for Win32 (ia32)
--
-- Host: localhost    Database: myntra_new
-- ------------------------------------------------------
-- Server version	5.1.41

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gift_cards`
--

DROP TABLE IF EXISTS `gift_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gift_cards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gift_message_id` int(11) DEFAULT NULL,
  `gift_card_type` int(11) DEFAULT NULL,
  `pin` varchar(32) NOT NULL,
  `email_link_uuid` varchar(255) NOT NULL,
  `status` int(11) DEFAULT '0',
  `sender_email` varchar(128) DEFAULT NULL,
  `recipient_email` varchar(128) DEFAULT NULL,
  `sender_name` varchar(255) DEFAULT NULL,
  `recipient_name` varchar(255) DEFAULT NULL,
  `amount` decimal(12,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `delivered_date` int(11) DEFAULT NULL,
  `activated_date` int(11) DEFAULT NULL,
  `email_last_sent` int(11) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_message_id` (`gift_message_id`),
  CONSTRAINT `fk_message_id` FOREIGN KEY (`gift_message_id`) REFERENCES `gift_cards_messages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gift_cards`
--

LOCK TABLES `gift_cards` WRITE;
/*!40000 ALTER TABLE `gift_cards` DISABLE KEYS */;
/*!40000 ALTER TABLE `gift_cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gift_cards_orders`
--

DROP TABLE IF EXISTS `gift_cards_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gift_cards_orders` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `orderid` int(20) NOT NULL,
  `gift_card_id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `sub_total` decimal(12,2) NOT NULL,
  `total` decimal(12,2) NOT NULL DEFAULT '0.00',
  `discount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `cod_charge` decimal(12,2) NOT NULL DEFAULT '0.00',
  `payment_surcharge` decimal(12,2) NOT NULL DEFAULT '0.00',
  `status` int(1) DEFAULT NULL,
  `mk_payments_log_id` int(31) NOT NULL,
  `payment_option` varchar(64) NOT NULL,
  `cash_redeemed` decimal(12,2) NOT NULL DEFAULT '0.00',
  `cash_coupon_code` varchar(50) DEFAULT NULL,
  `pg_discount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `cart_discount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gco_orderid` (`orderid`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gift_cards_orders`
--

LOCK TABLES `gift_cards_orders` WRITE;
/*!40000 ALTER TABLE `gift_cards_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `gift_cards_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gift_card_logs`
--

DROP TABLE IF EXISTS `gift_card_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gift_card_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gift_card_id` int(11) DEFAULT NULL,
  `activated_by` varchar(128) DEFAULT NULL,
  `activated_on` int(11) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gift_card_id` (`gift_card_id`),
  CONSTRAINT `fk_gift_card_id` FOREIGN KEY (`gift_card_id`) REFERENCES `gift_cards` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gift_card_logs`
--

LOCK TABLES `gift_card_logs` WRITE;
/*!40000 ALTER TABLE `gift_card_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `gift_card_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gift_card_occasions`
--

DROP TABLE IF EXISTS `gift_card_occasions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gift_card_occasions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `subject` text,
  `header` text,
  `body` text,
  `footer` text,
  `preview_image` varchar(255) DEFAULT NULL,
  `preview_image_thumbnail` varchar(255) DEFAULT NULL,
  `email_preview_image` varchar(255) DEFAULT NULL,
  `default_message` text,
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gift_card_occasions`
--

LOCK TABLES `gift_card_occasions` WRITE;
/*!40000 ALTER TABLE `gift_card_occasions` DISABLE KEYS */;
INSERT INTO `gift_card_occasions` VALUES (1,'Birthday','Mail Subject 1','','Mail Body 1','','skin2/images/GiftCard_HappyBirthday_v1.png','skin2/images/GiftCard_Thumb_HappyBirthday_115x70_v1.png',NULL,'Celebrating birthdays can never go out of fashion. Have a great one!','2012-07-04 11:16:39','2012-07-04 11:16:39','soapraj@gmail.com'),(2,'Anniversary','Mail Subject 2','','Mail Body 2','','skin2/images/GiftCard_HappyAnniversary_v1.png','skin2/images/GiftCard_Thumb_HappyAnniversary_115x70_v1.png',NULL,'Our love is like blue jeans and sneakers: just perfect!','2012-07-04 11:16:39','2012-07-04 11:16:39','soapraj@gmail.com'),(3,'Best Wishes','Mail Subject 3','','Mail Body 3','','skin2/images/GiftCard_BestWishes_v1.png','skin2/images/GiftCard_Thumb_BestWishes_115x70_v1.png',NULL,'Here\'s a stylish little something for you. All the best!',NULL,NULL,NULL),(4,'Congrats','Mail Subject 4','','Mail Body 4','','skin2/images/GiftCard_Congrats_v1.png','skin2/images/GiftCard_Thumb_Congrats_115x70_v1.png','','You\'ve achieved success in true style. Great going!','2012-07-24 06:54:04','2012-07-24 06:54:04','unknown@myntra.com');
/*!40000 ALTER TABLE `gift_card_occasions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gift_cards_messages`
--

DROP TABLE IF EXISTS `gift_cards_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gift_cards_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(255) DEFAULT NULL,
  `occasion_id` int(11) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mk_occasion_id` (`occasion_id`),
  CONSTRAINT `mk_occasion_id` FOREIGN KEY (`occasion_id`) REFERENCES `gift_card_occasions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gift_cards_messages`
--

LOCK TABLES `gift_cards_messages` WRITE;
/*!40000 ALTER TABLE `gift_cards_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `gift_cards_messages` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-09-21 16:29:53
