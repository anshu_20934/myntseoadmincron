CREATE TABLE `mk_order_downloader_runs` (
  `id` int(11) NOT NULL auto_increment,
  `run_number` int(11) NOT NULL,
  `description` varchar(200) default NULL,
  `status` varchar(20) NOT NULL default 'started',
  `run_time` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `mk_order_downloader_schedules` (
  `id` int(11) NOT NULL auto_increment,
  `scheduled_time` time NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `mk_editors` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(30) NOT NULL,
  `description` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `download_images_status` (
  `id` int(11) NOT NULL auto_increment,
  `item_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `status` varchar(10) default 'created',
  `run_number` int(11) NOT NULL,
  `assignee` varchar(45) NOT NULL,
  `editor_name` varchar(45) NOT NULL,
  `processing_group` varchar(45) NOT NULL,
  `product_style_name` varchar(45) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `item_id` (`item_id`),
  KEY `order_id` (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `mk_assignee_editors` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `assignee_id` INT(11) NOT NULL,
  `editor_id` INT(11) NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MYISAM DEFAULT CHARSET=latin1;

