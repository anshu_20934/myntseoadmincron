alter table xcart_orders add column cashback_processed tinyint(1) default 0;

-- update existing xcart_order rows
update xcart_orders set cashback_processed=1;

alter table xcart_orders add index `cb_processed_idx` (cashback_processed);
alter table xcart_discount_coupons add index `times_used_idx` (times_used), add index `times_locked_idx` (times_locked);
alter table xcart_discount_coupons_login add index `times_used_idx` (times_used), add index `times_locked_idx` (times_locked);