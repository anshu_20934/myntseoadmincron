insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) values('brand-article-id', '[
    {
        "brand_id": "183",
        "article_id": "70",
        "info": {
            "brand_name": "Jealous 21",
            "guide": "HIP FIT GUIDE",
            "size_count": "3 HIP SIZES FOR EVERY WAIST SIZE",
            "info": "KNOW YOUR SIZE USING JEALOUS 21 OOMPHOMETER",
            "table": 
			[
                [
                    [
                        "HIP FIT",
                        ""
                    ],
                    [
                        "GUIDE",
                        ""
                    ],
                    [
                        "EXAMPLE",
                        ""
                    ]
                ],
                [
                    [
                        "HOTTIE",
                        ""
                    ],
                    [
                        "WAIST + 8\' = HIP",
                        "A FIT THAT FLATTERS THE SLEEK BODY"
                    ],
                    [
                        "28\' + 8\' = 36\'",
                        ""
                    ]
                ],
                [
                    [
                        "HOURGLASS",
                        ""
                    ],
                    [
                        "WAIST + 10\' = HIP",
                        "A GREAT FIT FOR MODERATE FIGURES"
                    ],
                    [
                        "28\' + 10\' = 38\'",
                        ""
                    ]
                ],
                [
                    [
                        "BOOTILICIOUS",
                        ""
                    ],
                    [
                        "WAIST + 11\' = HIP",
                        "A FIT THAT FLATTERS THE CURVY HIPS"
                    ],
                    [
                        "28\' + 11\' = 39\'",
                        ""
                    ]
                ]
            ],
			"footer-text":"To shop your fit, search for \\\"Hottie\\\"/\\\"Hourglass\\\"/\\\"Bootilicious\\\""
        }
    }
] ', 'brand_id : id of jealous 21(183) for now article_id = id of jeans (70)');