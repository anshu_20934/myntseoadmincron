insert into mk_cards_domestic_bins (bin, bank_name, type, icici_onus, product_type, product_id) values (457623,'BANK OF INDIA','Domestic','Offus','Credit',''),
(403250,'SBI CARDS AND PAYMENT SERVICES PTE LTD','Domestic','Offus','Credit',''),
(457704,'HDFC BANK LIMITED','Domestic','Offus','Credit',''),
(498726,'INDUSIND BANK LIMITED','Domestic','Offus','Credit',''),
(529557,'IDBI Bank Ltd.','Domestic','Offus','Debit','STANDARD'),
(402780,'Bank of India','Domestic','Offus','Debit','CLASSIC'),
(457624,'Bank of India','Domestic','Offus','Debit','PLATINUM'),
(401822,'The Thane Janata Sahakari Bank Ltd.','Domestic','Offus','Debit','CLASSIC'),
(401823,'The Thane Janata Sahakari Bank Ltd.','Domestic','Offus','Debit','PLATINUM'),
(457553,'Axis Bank Limited','Domestic','Offus','Debit','PLATINUM'),
(457580,'Axis Bank Limited','Domestic','Offus','Debit','CORPORATE T CARD'),
(457581,'Axis Bank Limited','Domestic','Offus','Debit','CORPORATE T CARD'),
(457582,'Axis Bank Limited','Domestic','Offus','Debit','CORPORATE T CARD'),
(457583,'Axis Bank Limited','Domestic','Offus','Debit','CORPORATE T CARD'),
(457584,'Axis Bank Limited','Domestic','Offus','Debit','CORPORATE T CARD'),
(457585,'Axis Bank Limited','Domestic','Offus','Debit','CORPORATE T CARD'),
(457586,'Axis Bank Limited','Domestic','Offus','Debit','CORPORATE T CARD'),
(457587,'Axis Bank Limited','Domestic','Offus','Debit','CORPORATE T CARD'),
(457588,'Axis Bank Limited','Domestic','Offus','Debit','CORPORATE T CARD'),
(457741,'Punjab and Maharashtra Co-Operative Bank Ltd.','Domestic','Offus','Debit','CLASSIC'),
(457742,'Punjab and Maharashtra Co-Operative Bank Ltd.','Domestic','Offus','Debit','PREMIER'),
(457743,'Punjab and Maharashtra Co-Operative Bank Ltd.','Domestic','Offus','Debit','PLATINUM'),
(459169,'State Bank of India','Domestic','Offus','Debit','PREMIER'),
(459170,'State Bank of India','Domestic','Offus','Debit','PLATINUM'),
(459179,'State Bank of India','Domestic','Offus','Debit','PREMIER'),
(459180,'State Bank of India','Domestic','Offus','Debit','PLATINUM');

update mk_cards_domestic_bins set card_bank_code='SBI',card_bank_registration_link='https://secure4.arcot.com/vpas/sbi_vbvisa/enroll/index.jsp?locale=en_US&bankid=6897' where UPPER(bank_name) like "SBI%" OR UPPER(bank_name) like "STATE BANK OF INDIA%";

update mk_cards_domestic_bins set card_bank_code='ABN',card_bank_registration_link='http://www.abnamro.co.in/consumer/creditcard/secureCode.html' where UPPER(bank_name) like "ABN%";

update mk_cards_domestic_bins set card_bank_code='AXIS',card_bank_registration_link='https://cardsecurity.enstage.com/ACSWeb/EnrollWeb/AxisBank/main/index.jsp' where UPPER(bank_name) like "AXIS%";

update mk_cards_domestic_bins set card_bank_code='HDFC',card_bank_registration_link='http://www.hdfcbank.com/campaign/visa/visa.htm' where UPPER(bank_name) like "HDFC%";

update mk_cards_domestic_bins set card_bank_code='CITI',card_bank_registration_link='https://www.citibank.co.in/ssjsps/ssindex.do' where UPPER(bank_name) like "CITI%";

update mk_cards_domestic_bins set card_bank_code='HSBC',card_bank_registration_link='http://www.hsbc.co.in/1/2/personal/credit-cards/secure-online-payment-services' where UPPER(bank_name) like "HSBC%";

update mk_cards_domestic_bins set card_bank_code='IDBI',card_bank_registration_link='https://secureonline.idbibank.com/ACSWeb/EnrollWeb/IDBIBank/main/index.jsp' where UPPER(bank_name) like "IDBI%";

update mk_cards_domestic_bins set card_bank_code='ICICI',card_bank_registration_link='https://www.3dsecure.icicibank.com/ACSWeb/EnrollWeb/ICICIBank/main/index.jsp' where UPPER(bank_name) like "ICICI%";

update mk_cards_domestic_bins set card_bank_code='KARUR',card_bank_registration_link='https://cardsecurity.enstage.com/ACSWeb/EnrollWeb/KVB/main/vbv.jsp' where UPPER(bank_name) like "KARUR%";

update mk_cards_domestic_bins set card_bank_code='JK',card_bank_registration_link='https://cardsecurity.enstage.com/ACSWeb/EnrollWeb/JKBank/main/index.jsp' where UPPER(bank_name) like "JAMMU%" OR UPPER(bank_name) like "J&K%";

update mk_cards_domestic_bins set card_bank_code='PNB',card_bank_registration_link='https://pnb.electracard.com/pnb/enrollment/enroll_welcome.jsp' where UPPER(bank_name) like "PUNJAB%" OR UPPER(bank_name) like "PNB%";

update mk_cards_domestic_bins set card_bank_code='CANARA',card_bank_registration_link='https://cardsecurity.enstage.com/ACSWeb/EnrollWeb/CanaraBank/main/index.jsp' where UPPER(bank_name) like "CANARA%";

update mk_cards_domestic_bins set card_bank_code='VIJAYA',card_bank_registration_link='https://cardsecurity.enstage.com/ACSWeb/EnrollWeb/VijayaBank/main/index.jsp' where UPPER(bank_name) like "VIJAYA%";

update mk_cards_domestic_bins set card_bank_code='CENTRAL',card_bank_registration_link='https://cbi.electracard.com/cbi/enrollment/enroll_welcome.jsp' where UPPER(bank_name) like "CENTRAL%";

update mk_cards_domestic_bins set card_bank_code='INDIAN',card_bank_registration_link='https://cardsecurity.enstage.com/ACSWeb/EnrollWeb/IndianBank/main/index.jsp' where UPPER(bank_name) like "INDIAN%";

update mk_cards_domestic_bins set card_bank_code='SC',card_bank_registration_link='http://www.standardchartered.co.in/personal/credit-cards/en/3d_secure.html' where UPPER(bank_name) like "STANDARD%";

update mk_cards_domestic_bins set card_bank_code='DB',card_bank_registration_link='https://www.dbindia.in/DeutscheBank/enrollment/card_type_options.jsp' where UPPER(bank_name) like "DEUTSCHE%";

update mk_cards_domestic_bins set card_bank_code='CORP',card_bank_registration_link='https://corpbank.electracard.com/corpbank/enrollment/enroll_welcome.jsp' where UPPER(bank_name) like "CORPORATION%";

update mk_cards_domestic_bins set card_bank_code='FEDERAL',card_bank_registration_link='https://cardsecurity.enstage.com/ACSWeb/EnrollWeb/FederalBank/main/index.jsp' where UPPER(bank_name) like "FEDERAL%";
