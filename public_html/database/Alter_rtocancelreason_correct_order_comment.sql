ALTER TABLE `mk_old_returns_tracking`  CHANGE COLUMN `rtocancelreason` `rtocancelreason` ENUM('Customer Request','Customer Unreachable', 'Direct online order cancellation') NULL DEFAULT NULL AFTER `returnreason`;

update mk_ordercommentslog 
SET commenttype = 'RT processing', commenttitle = 'Order in RT'
where commenttype = 'RTO processing' 
and commenttitle = 'Order in';