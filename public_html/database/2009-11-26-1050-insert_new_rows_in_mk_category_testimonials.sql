use myntra;
insert into mk_category_testimonials(cat_id,name,content,is_publish) values(152, 'Priyanka, Delhi', "I created a personalized 2010 desktop calendar with a photo of my kids. The quality of the calendar is amazing and it's the perfect way to start the day on a great note!", 1);
insert into mk_category_testimonials(cat_id,name,content,is_publish) values(152, 'Rajan, Cochin', "I have been very pleased with the ease with which I could create my wall calendar on Myntra. It was so simple and the best part was I could add my personal touch it. It was unlike the normal calendars I am used to seeing and is a wonderful product.", 1);
insert into mk_category_testimonials(cat_id,name,content,is_publish) values(152, 'Bharathi, New Delhi', "The mug calendar is indeed unique! It has the 2010 calendar printed on it and the mug can be personalized with with a photo as well!", 1);

delete from mk_category_testimonials where cat_id=154;
delete from mk_category_testimonials where cat_id=155;
insert into mk_category_testimonials(cat_id,name,content,is_publish) values(154, 'Bharathi, New Delhi', "The mug calendar is indeed unique! It has the 2010 calendar printed on it and the mug can be personalized with with a photo as well!", 1);
insert into mk_category_testimonials(cat_id,name,content,is_publish) values(154, 'Dheeraj, Pune', "The greeting card cum calendar is practical yet beautiful. It's a novel product which can be personalized and is an ideal choice for gifting.", 1);
insert into mk_category_testimonials(cat_id,name,content,is_publish) values(154, 'Kavitha, Chandigarh', "The set of pocket calendars are compact and the best part is it can be personalized with a photo or design of your choice!", 1);

insert into mk_category_testimonials(cat_id,name,content,is_publish) values(155, 'Renuka, Chennai', "The wall calendar from Myntra is the best way way to start off the year 2010. It is large, beautifully designed and I had it personalized with my photo.", 1);
insert into mk_category_testimonials(cat_id,name,content,is_publish) values(155, 'Praful, Mumbai ', "I created a fabulous 2010 Wall Calendar. There is a collection of design templates to choose from and you can give it your personal touch by adding your own photo or design to it.", 1);
insert into mk_category_testimonials(cat_id,name,content,is_publish) values(155, 'Rajan, Cochin', "I have been very pleased with the ease with which I could create my wall calendar on Myntra. It was so simple and the best part was I could add my personal touch it. It was unlike the normal calendars I am used to seeing and is a wonderful product.", 1);

