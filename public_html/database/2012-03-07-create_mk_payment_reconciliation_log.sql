CREATE TABLE `mk_payment_reconciliation_log` (
`orderid` int(20) NOT NULL,
`captured_amount` decimal(12,2) NOT NULL DEFAULT '0.00',
`refund` decimal(12,2) NOT NULL DEFAULT '0.00',
`chargeback` decimal(12,2) NOT NULL DEFAULT '0.00',
`tdr` decimal(12,2) NOT NULL DEFAULT '0.00',
`service_tax` decimal(12,2) NOT NULL DEFAULT '0.00',
`payment_gateway_name` varchar(64) DEFAULT NULL,
`captured_date` int(11) NOT NULL DEFAULT '0',
`update_time` int(11) NOT NULL DEFAULT '0',
`insert_time` int(11) NOT NULL DEFAULT '0',
`update_by` varchar(128) NOT NULL DEFAULT '',
PRIMARY KEY (`orderid`),
KEY `update_time` (`update_time`),
KEY `insert_time` (`insert_time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;