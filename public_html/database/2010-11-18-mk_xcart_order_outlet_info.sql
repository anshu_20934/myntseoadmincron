CREATE TABLE mk_xcart_order_outlet_info (                                                        
     id int(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',                                     
     orderid int(20) NOT NULL COMMENT 'FK to xcart_orders',                                        
     ref_id varchar(128) DEFAULT NULL COMMENT 'Reference number',                                  
     outlet_code varchar(128) DEFAULT NULL COMMENT 'Code of outlet where order was placed',
     outlet_name varchar(512) DEFAULT NULL COMMENT 'Name of outlet where order was placed',           
     outlet_region varchar(256) DEFAULT NULL COMMENT 'Region where outlet lies',
     outlet_state varchar(256) DEFAULT NULL COMMENT 'State where outlet lies', 
     outlet_city varchar(256) DEFAULT NULL COMMENT 'City where outlet lies',                    
     area_mgr varchar(256) DEFAULT NULL COMMENT 'Area manager details',                         
     area_mgr_email varchar(256) DEFAULT NULL COMMENT 'Area manager email',                         
     event_name varchar(512) DEFAULT NULL COMMENT 'Event/campaign during which the order was placed',  
     PRIMARY KEY (id),                                                                             
     KEY orderid (orderid)                                                                       
) ENGINE=InnoDB DEFAULT CHARSET=latin1;