UPDATE xcart_orders xo 
SET xo.status = 'D'
WHERE xo.status IN ('Q','WP','OH')
AND xo.coupon IN 
( SELECT coupon 
FROM xcart_discount_coupons dc
WHERE dc.coupon = xo.coupon
AND dc.coupon LIKE 'survey%' AND dc.times_used >1 );

UPDATE xcart_order_details xod
SET item_status = 'UA'
WHERE xod.orderid IN
(       
        SELECT orderid  FROM xcart_orders a, xcart_discount_coupons b
        WHERE a.coupon=b.coupon AND b.coupon LIKE 'survey%' AND b.times_used >1 AND a.status IN ('Q','WP','OH')
);
