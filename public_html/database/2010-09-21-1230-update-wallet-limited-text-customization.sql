use myntra;

/*1.insert into color table(if new color has to be added)*/
insert into `mk_text_font_color` (color) values('#656066');
insert into `mk_text_font_color` (color) values('#d59e0f');

/*new font*/
insert into mk_fonts (id,name,image_name,ttf_name,display_order) values ('154','MTCORSVA','MTCORSVA.png','MTCORSVA.ttf','1');
insert into mk_fonts (id,name,image_name,ttf_name,display_order) values ('155','times','times.png','times.ttf','1');

/*1199*/
update mk_text_font_detail set font_id='155'where orientation_id=1686 and font_id=1;

/*1200*/
update mk_text_font_detail set font_id='155'where orientation_id=1687 and font_id=1;

/*1201*/
update mk_text_font_detail set font_id='155'where orientation_id=1688 and font_id=1;

/*1202*/
update mk_text_font_detail set font_id='155'where orientation_id=1689 and font_id=1;

/*1203*/
update mk_text_font_detail set font_id='155'where orientation_id=1691 and font_id=1;

/*1204*/
update mk_text_font_detail set font_id='155'where orientation_id=1692 and font_id=1;

/*1205*/
update mk_text_font_detail set font_id='155'where orientation_id=1693 and font_id=1;

/*1206*/
update mk_text_font_detail set font_id='155'where orientation_id=1694 and font_id=1;

/*1207*/
update mk_text_font_detail set font_id='155'where orientation_id=1695 and font_id=1;

/*1208*/
update mk_text_font_detail set font_id='155'where orientation_id=1696 and font_id=1;

/*1209*/
update mk_text_font_detail set font_id='155'where orientation_id=1697 and font_id=1;

/*1210*/
update mk_text_font_detail set font_id='155'where orientation_id=1698 and font_id=1;

/*1211*/
update mk_text_font_detail set font_id='155'where orientation_id=1699 and font_id=1;

/*1212*/
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='147',textColorDefault='147' where id=1700;
update mk_text_font_detail set font_id='155'where orientation_id=1700 and font_id=1;

/*1213*/
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='146',textColorDefault='146' where id=1701;
update mk_text_font_detail set font_id='154',font_size='0.25' where orientation_id=1701 and font_id=1;

/*1214*/
update mk_text_font_detail set font_id='154',font_size='0.25' where orientation_id=1702 and font_id=1;

/*1215*/
update mk_text_font_detail set font_id='154',font_size='0.25' where orientation_id=1703 and font_id=1;

/*1216*/
update mk_text_font_detail set font_id='154',font_size='0.25' where orientation_id=1704 and font_id=1;

/*1217*/
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='146',textColorDefault='146' where id=1705;
update mk_text_font_detail set font_id='154',font_size='0.25' where orientation_id=1705 and font_id=1;

/*1218*/
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='146',textColorDefault='146' where id=1706;
update mk_text_font_detail set font_id='154',font_size='0.25' where orientation_id=1706 and font_id=1;

/*1219*/
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='146',textColorDefault='146' where id=1707;
update mk_text_font_detail set font_id='154',font_size='0.50' where orientation_id=1707 and font_id=1;

/*1307*/
update mk_text_font_detail set font_id='155'where orientation_id=1800 and font_id=1;

/*1308*/
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='147',textColorDefault='147' where id=1801;
update mk_text_font_detail set font_id='155'where orientation_id=1801 and font_id=1;

/*1309*/
update mk_text_font_detail set font_id='155'where orientation_id=1802 and font_id=1;

/*1310*/
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='146',textColorDefault='146' where id=1803;
update mk_text_font_detail set font_id='154',font_size='0.25' where orientation_id=1803 and font_id=1;
