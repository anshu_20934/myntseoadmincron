use myntra;
update mk_email_notification set body="Hi [RECEIVERNAME],
 <p>How does the idea of wearing official Team India Jersey with your name on it or writing with a 
classy Sheaffer pen with your name imprinted sound to you? Sounds great, isn't it?</p>
 <p>Now, all these memorable personalized products are right at your fingertips as Myntra has created a page just for you 
 that has all your favorite products with your name on them. 
 Check out the link <a target='_blank' href='http://www.myntra.com/[RECEIVERNAME]/name/'>www.myntra.com/[RECEIVERNAME]/name/</a>  to see for yourself.</p>
 <p>
 All these gorgeous products can be yours in just 3 simple steps. It takes just 2-4 business days for Myntra to deliver your products anywhere in
 India and 7-10 business days when shipped internationally. Indulge now! </p>
 <p>-[SENDERNAME]</p>",subject="Found something special for you!" where name='namepage' ;

