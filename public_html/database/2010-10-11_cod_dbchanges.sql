/* query to  add cod_enables column  for product style, It will be used  for cod */
alter table mk_product_style add column cod_enabled char(1) default 'Y'; 

/* set default value to yes('Y') for mk_product_type cod_enabled column */
update mk_product_type set cod_enabled = 'Y';

/* new table to support cash-on-delivery */
create table cash_on_delivery_info ( order_id int(20) NOT NULL PRIMARY KEY , login_id VARCHAR(128),  verification_code int(6), contact_number varchar(15), shopping_cookie varchar(50), create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP) TYPE="innodb" ;

/* add  new payment date column to xcart_orders */

alter table xcart_orders add column cod_payment_date TIMESTAMP ;

/* new column for paystatus in xcart_orders table */
alter  table xcart_orders add column cod_pay_status varchar(20);