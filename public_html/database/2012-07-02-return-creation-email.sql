update mk_email_notification set body = 'Hi [USER], <br/><br/>
We have received your request for return of the following items. Please note your Return No. [RETURN_ID]. You are requested to quote this on any communication related to this return. Our delivery boy will be reaching out to you in the next 72 hours for Pickup of your Return Order.<br/><br/>
Here are the details of your Return No. [RETURN_ID], for your reference - <br/><br/> 
[RETURN_DETAILS]
<br/>
<b>Steps to return your product:</b><br/>
<ul style="list-style-type:decimal;">
	<li>Open the My Returns section under My Myntra.</li>
	<li>Click the Download returns form.</li>
	<li>Print out the downloaded form, fill out the required details and sign the form.<br/>
		<b>No-printer option:</b> Please note the following particulars on a sheet of a paper to serve as a returns form: <br/>
		<ul style="list-style-type:lower-alpha;">
			<li>Order No</li>
			<li>Return No</li>
			<li>Customer Login</li>
			<li>Request Date</li>			
		</ul> 
	</li>
	<li>Re-pack the product you want to return making sure to include all accompanying material the item was originally delivered to you with.</li>
	<li>Paste the returns form on the reverse side of package such that the entire form is visible.</li>
	<li>Download and print address label from My Return Requests section on My Myntra. <br/>
		<b>No-printer option:</b> Please inscribe the following address on the consignment:
		<p>[MYNTRA_ADDRESS]</p>
	</li>
	<li>Please hand over the consignment to our logistics partner, when they visit your doorstep for pickup. Please retain the receipt (docket no.) till we have confirmed receipt of the consignment.</li>
</ul>
<p>This mail is for your records only and you are requested not to send a response to this auto-generated mail. You can track the status of your return requests under the <a href="[MY_RETURNS_URL]" onclick="_gaq.push([\'_trackEvent\', \'returns_mailer\', \'view_my_returns\', \'returns_conf_mail\']);">My Returns</a> section in My Myntra. In case of any further queries, please contact our Customer Care at [MYNTRA_CC_PHONE]</p>
<br/>
Regards,<br/>
Myntra Customer Care' where name = 'return_confirmation_pickup';
