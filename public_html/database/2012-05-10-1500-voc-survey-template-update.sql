update mk_email_notification set body='<br/>
<div style="width:100%;height:auto;background-color:#eee;">
        <div style="width:auto;height:auto;padding:10px">
        <span><strong>Order Details:</strong><span>
                <ul style="list-style-type:none;">
                        <li style="margin-bottom:10px;">
                                <span>Customer Name :</span>
                                <label>[USER]</label>
                        </li>
                        <li style="margin-bottom:10px;">
                                <span>Order Id :</span>
                                <label>[ORDERID]</label>
                        </li>
                        <li style="margin-bottom:10px;">
                                <span>Order Date :</span>
                                <label>[DATE]</label>
                        </li>
                        <li style="margin-bottom:10px;">
                                <span>Ship Date :</span>
                                <label>[SHIP_DATE]</label>
                        </li>
                        <li style="margin-bottom:10px;">
                                <span>Delivery Date :</span>
                                <label>[DELIVERY_DATE]</label>
                        </li>
                        <li style="margin-bottom:10px;">
                                <span>Payment Method :</span>
                                <label>[PAYMENT_METHOD]</label>
                        </li>
                        <li style="margin-bottom:10px;">
                                <span>Courier :</span>
                                <label>[LOGISTIC]</label>
                        </li>
						<li style="margin-bottom:10px;">
                                <span>Ship City :</span>
                                <label>[SHIPCITY]</label>
                        </li>
						<li style="margin-bottom:10px;">
                                <span>Ship Zipcode :</span>
                                <label>[SHIPCITYZIP]</label>
                        </li>

                </ul>
                <div style="margin-top:10px">[ORDER_PRODUCT_AMOUNT_DETAIL_HTML]</div>
        </div>
</div>
<br/>
[ORDER_MFB_SURVEY]<br/>
Regards,<br/>
Myntra Feedback Engine<br/><br/>' where name='voc_order_survey';