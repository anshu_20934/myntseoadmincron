CREATE TABLE mk_api_users (
id                      int(5) auto_increment,
userid                  varchar(255) NOT NULL,
password                varchar(255) NOT NULL,
username                varchar(255) NOT NULL,
role                    varchar(255),
status                  int(1) NOT NULL default 1,
PRIMARY KEY(id)
);


insert into mk_api_users (userid, password, username, role) VALUES ('apiadmin', 'B-b5b508490d384be7241b47b3baba7da36f4dd4178cb5e50c', 'API Admin', 'SuperUser');
