alter table mk_stats_search add column page_promotion text null;
update mk_stats_search set page_promotion='<div style="border:0px solid red;width:980px;">
			 <div style="width: 980px; height: 200px;">
			 <div style="margin-top:0px; padding:0px;">
			<span style="margin-top: 10px; width: 330px; float: left; padding-left: 0px; font-family:Arial,Helvetica,sans-serif; color: black;font-size:13px; text-align: justify;"><strong style="font-size:16px;"> Canvas Shoes:</strong><br>There is a canvas shoe for everyone. The simple design of these canvas shoes makes for great creative expression. Go preppy in solid colors or classic checks or show your rebellious side with pop art.<br>
			<br>Canvas shoes are a classic staple for your wardrobe, easy to maintain and high on fashion quotient. Choose from a wide range of brands - Converse, Puma, Adidas, Numero Uno, Lee Cooper, Lotto & Fila and find the one that represents you. </span>
				<div style="float:right;"><img src="http://cdn.myntra.com/skin1/images/marketing/canvas-shoes-banner.jpg " width="620" height="200" alt="Canvas Collection" title="Canvas Collection" style="text-decoration:none;border:none;font-family:Arial,Helvetica,sans-serif;color:#666;font-size:12px;"/>
			  </div>
			  </div>
			  </div>
			  <div style="clear:both"></div>
			<div style="margin-top:25px; padding:0px; width: 980px; height: 130px;">
				<a href="http://staging.myntra.com/converse-canvas-shoes"><img src="http://cdn.myntra.com/skin1/images/marketing/converse-canvas.jpg" width="193" height="130" alt="Converse canvas shoes" title="Converse canvas shoes" style="border: none;"/></a>
				<a href="http://staging.myntra.com/adidas-canvas-shoes"><img src="http://cdn.myntra.com/skin1/images/marketing/adidas-canvas.jpg" width="193" height="130" alt="Adidas canvas shoes" title="Adidas canvas shoes" style="border: none;"/></a>
				<a href="http://staging.myntra.com/puma-canvas-shoes"><img src="http://cdn.myntra.com/skin1/images/marketing/puma-canvas.jpg" width="193" height="130" alt="Puma canvas shoes" title="Puma canvas shoes" style="border: none;"/></a>
				<a href="http://staging.myntra.com/numero-uno-canvas-shoes"><img src="http://cdn.myntra.com/skin1/images/marketing/numero-uno-canvas.jpg" width="193" height="130" alt="Numero Uno canvas shoes" title="Numero Uno canvas shoes" style="border: none;"/></a>
				<a href="http://staging.myntra.com/fila-canvas-shoes"><img src="http://cdn.myntra.com/skin1/images/marketing/fila-canvas.jpg" width="193" height="130" alt="Fila canvas shoes" title="Fila canvas shoes" style="border: none;"/></a>
				</div>
			</div>' where keyword = 'canvas shoes';
