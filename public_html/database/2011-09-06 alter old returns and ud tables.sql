ALTER TABLE `mk_old_returns_tracking`  CHANGE COLUMN `returnreason` `returnreason` VARCHAR(100) NULL DEFAULT NULL AFTER `returntype`;

ALTER TABLE `mk_old_returns_tracking_ud`  CHANGE COLUMN `reason` `reason` VARCHAR(100) NULL DEFAULT NULL AFTER `attemptcount`;