
CREATE TABLE IF NOT EXISTS `myntra_festival_slabs` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `lowerBound` INT(11) NOT NULL ,
  `noOfCoupons` INT(11) NOT NULL ,
  `CouponValue` INT(11) NOT NULL ,
  `CouponMinPurchaseValue` INT(11) NOT NULL ,
  `isActive` TINYINT(1)  NOT NULL DEFAULT 1 ,
  `updatedBy` VARCHAR(50) NOT NULL ,
  `createdOn` TIMESTAMP NOT NULL ,
  `updatedOn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`id`) ,
  INDEX `isActive` (`isActive` ASC) 
);

CREATE TABLE IF NOT EXISTS `myntra_fest_order_coupon_map` (
  `order_id` int(11) NOT NULL,
  `coupon_code` varchar(50) NOT NULL,
  KEY `coupon_code` (`coupon_code`),
  KEY `order_id` (`order_id`)
);


cREATE TABLE IF NOT EXISTS `myntra_fest_coupon_share_log` (
  `from` varchar(128) NOT NULL,
  `to` varchar(128) NOT NULL,
  `couponCode` varchar(50) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `couponCode` (`couponCode`)
) 

CREATE TABLE IF NOT EXISTS `myntra_fest_cron_timestamps` (
  `name` varchar(30) NOT NULL,
  `updatedOn` int(11) NOT NULL
) 
