
-- A/B test for new order creation/confirm flow
insert into mk_abtesting_tests (name,ga_slot,seg_algo,source_type) values('orderplacement','1','abtest\\algos\\impl\\RandomSegmentationAlgo','tpl');
insert into mk_abtesting_variations (ab_test_id,name,percent_probability) values((select id from mk_abtesting_tests where name='orderplacement'),'test',50);
insert into mk_abtesting_variations (ab_test_id,name,percent_probability) values((select id from mk_abtesting_tests where name='orderplacement'),'control',50);
