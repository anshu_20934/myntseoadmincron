INSERT INTO `myntra`.`mk_feature_gate_key_value_pairs` (`key`, `value`, `description`)
VALUES ('boostedSearch.enabled', 'true', 'true/false ');



insert into mk_abtesting_tests (name,ga_slot,seg_algo,source_type) 
values('boostedSearch','5','abtest\\algos\\impl\\RandomSegmentationAlgo','tpl');
insert into mk_abtesting_variations (name,ab_test_id,percent_probability) 
values('enabled',(select id from mk_abtesting_tests where name ='boostedSearch'),60);
insert into mk_abtesting_variations (name,ab_test_id,percent_probability)
values('control',(select id from mk_abtesting_tests where name ='boostedSearch'),40);