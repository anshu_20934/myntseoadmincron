


update mk_returns_reasons set displayname = 'Fitment issue - Too large' where code='PLS';
update mk_returns_reasons set displayname = 'Fitment issue - Too small' where code='PSS';
update mk_returns_reasons set displayname = 'Product quality issue - material' where code='PQM';
update mk_returns_reasons set displayname = 'Product quality issue - manufacturing defect' where code='PMD';
update mk_returns_reasons set displayname = 'Wrong Item Shipped' where code='WIS';
update mk_returns_reasons set displayname = 'Product not as displayed on website' where code='PNW';
update mk_returns_reasons set displayname = 'Wrong size shipped' where code='ISS';


update mk_return_status_details set mymyntra_status = 'RECEIVED',mymyntra_msg = 'We have received your return on [RECEIVED_DATE]. It will be processed by [PROCESSING_PROMISE_DATE].' where code = 'RQSF';
update mk_return_status_details set mymyntra_status = 'ACCEPTED',mymyntra_msg = 'Your return has been accepted by us. You will be issued a refund on this item within 24 hours.' where code = 'RQCP';
update mk_return_status_details set mymyntra_status = 'REJECTED',mymyntra_msg = 'Your return request has been declined as it did not meet our <a href="[GUIDE_LINES_LINK]" onclick="_gaq.push([\'_trackEvent\', \'myreturns\', \'view_returns_policy\', \'click\']);" target="_blank">Returns Policy</a>. We will contact you soon regarding this.' where code = 'RQCF';
update mk_return_status_details set mymyntra_msg = 'We will arrange a pickup of your return within 1-2 working days, between 2-7 pm.' where code = 'RRQP';
update mk_return_status_details set mymyntra_msg = 'We will arrange a pickup of your return within [PICKUP_PROMISE_DATE], between 2-7 pm.' where code = 'RPI';
update mk_return_status_details set mymyntra_msg = 'We will arrange a pickup of your return within [RPI_2_PROMISE_DATE], between 2-7 pm.' where code = 'RPI2';
update mk_return_status_details set mymyntra_msg = 'We are awaiting the update of your return shipment details. If you have already shipped your return, please update the details here. This will allow us to track their delivery to our Returns Processing Facility.' where code = 'RRQS';
update mk_return_status_details set mymyntra_msg = 'We have been unable to pickup your return on our first attempt. Please call our Customer Care at [CUST_CARE] in case you would like to have the pickup re-attempted or if you would like to ship the return yourself.' where code = 'RPF';
update mk_return_status_details set mymyntra_msg = 'Your return has been picked up by [COURIER_INFO] with Tracking Number: [TRACKING_NO]. In case of any discrepancy, contact our Customer Care at [CUST_CARE].' where code = 'RPU';
update mk_return_status_details set mymyntra_msg = 'Thank you for sharing your return shipment details. We will let you know once we have received your return shipment.' where code = 'RDU';
update mk_return_status_details set mymyntra_msg = 'We have received your return on [RECEIVED_DATE]. It will be processed by [PROCESSING_PROMISE_DATE].' where code = 'RRC';
update mk_return_status_details set mymyntra_msg = 'We have received your return on [RECEIVED_DATE]. It will be processed by [PROCESSING_PROMISE_DATE].' where code = 'RQP';
update mk_return_status_details set mymyntra_msg = 'We have received your return on [RECEIVED_DATE]. It will be processed by [PROCESSING_PROMISE_DATE].' where code = 'RQF';
update mk_return_status_details set mymyntra_msg = 'Your return has been accepted by us. You will be issued a refund for this item within 24 hours.' where code = 'RQSP';
update mk_return_status_details set mymyntra_msg = 'Your return request has been declined as it did not meet our <a href="[GUIDE_LINES_LINK]" onclick="_gaq.push([\'_trackEvent\', \'myreturns\', \'view_returns_policy\', \'click\']);" target="_blank">Returns Policy</a>. We will contact you soon regarding this.' where code = 'RQSF';
update mk_return_status_details set mymyntra_msg = 'Your return request has been declined as it did not meet our <a href="[GUIDE_LINES_LINK]" onclick="_gaq.push([\'_trackEvent\', \'myreturns\', \'view_returns_policy\', \'click\']);" target="_blank">Returns Policy</a>. The item will be shipped back to you.' where code = 'RRRS';
update mk_return_status_details set mymyntra_msg = 'Your return request has been declined as it did not meet our <a href="[GUIDE_LINES_LINK]" onclick="_gaq.push([\'_trackEvent\', \'myreturns\', \'view_returns_policy\', \'click\']);" target="_blank">Returns Policy</a>. The item has been shipped back to you through [RESHIP_COURIER_INFO] with tracking number: [RESHIP_TRACKING] on [RESHIP_DATE]' where code = 'RRS';
update mk_return_status_details set mymyntra_msg = 'Your return request has been declined as it did not meet our <a href="[GUIDE_LINES_LINK]" onclick="_gaq.push([\'_trackEvent\', \'myreturns\', \'view_returns_policy\', \'click\']);" target="_blank">Returns Policy</a>. The item has been delivered back to you through [RESHIP_COURIER_INFO] with tracking number: [RESHIP_TRACKING] on [REDELIVER_DATE]' where code = 'RSD';
update mk_return_status_details set mymyntra_msg = 'Your return request has been declined as it did not meet our <a href="[GUIDE_LINES_LINK]" onclick="_gaq.push([\'_trackEvent\', \'myreturns\', \'view_returns_policy\', \'click\']);" target="_blank">Returns Policy</a>. As confirmed with you, your return will be disposed of by us.' where code = 'RRJ';
update mk_return_status_details set mymyntra_msg = 'Your return request has been declined as it did not meet our <a href="[GUIDE_LINES_LINK]" onclick="_gaq.push([\'_trackEvent\', \'myreturns\', \'view_returns_policy\', \'click\']);" target="_blank">Returns Policy</a>. As confirmed with you, your return will be disposed of by us.' where code = 'RJS';
update mk_return_status_details set mymyntra_msg = 'We will arrange a pickup of your return within 1-2 working days, between 2-7 pm.' where code = 'RPUQ2';

