insert into mk_email_notification(name, subject, body) values ('order_cancel', 'Cancelled Order No. [ORDER_ID]',
'Hello [USER], <br/><br/>
[REASON_TEXT] Please find the details of your Order No. [ORDER_ID] below:<br/><br/>
[ORDER_DETAILS]
<br/><br/>
[REFUND_MSG]
[COUPON_USAGE_TEXT]
[EXTRA_COUPON_TEXT]
<p>We want you to know we are honoured to have you as our esteemed customer and we will do our best to meet your needs. Your continued patronage and suggestions are a vital part of our growth, and for that we are most grateful to you.</p>
<p>For any clarification, kindly contact us at [MYNTRA_CC_PHONE].</p>
<br/>
Regards,<br/>
Team Myntra');


delete from mk_email_notification where name = 'order_cancel_ccc';
delete from mk_email_notification where name = 'order_cancel_ccr';

update mk_email_notification set body = 'Hello [USER], <br/><br/>
[CANCEL_REASON_TEXT]<br/><br/>
Updated order details:
<br/><br/>
[ORDER_DETAILS]
<br/><br/>
Your cancelled item details:
<br/><br/>
[CANCELLED_ORDER_DETAILS]
<br/>
[REFUND_MSG]
[COUPON_USAGE_TEXT]
[EXTRA_COUPON_TEXT]
<p>We want you to know we are honoured to have you as our esteemed customer and we will do our best to meet your needs. Your continued patronage and suggestions are a vital part of our growth, and for that we are most grateful to you.</p>
<p>For any clarification, kindly contact us at [MYNTRA_CC_PHONE].</p>
<br/>
Regards,<br/>
Team Myntra' where name = 'item_cancel';
