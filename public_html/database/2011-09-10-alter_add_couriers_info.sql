update mk_courier_service set reports_template = ' "" as Origin_Mail_Centre_Code, "" as Destination_Mail_Centre_Code, s_city as Destination_City, orderid as Reference, from_unixtime(date, "%d.%m.%Y") as Order_Date, from_unixtime(date, "%d.%m.%Y") as Picking_Date, 10932 as Customer_Code, 10932 as AccountNo, 
(SELECT CASE WHEN payment_method = "cod" THEN concat("COD",": ", total) ELSE "NON-COD" END) as Bag_No, "" as Master_Con_Note, "" as Tracking_No, qtyInOrder as Quantity, "1" as Weight, (SELECT CASE WHEN payment_method = "cod" THEN "COD" ELSE "NON-COD" END) as Special_Delivery_Instruction, "Return to Origin" as Instruction_On_Undelivery, CONCAT(firstname, " ", lastname) as Receiver_Name, phone as Receiver_Contact_No, s_address as Receiver_Area, s_city as Receiver_City, s_zipcode as Receiver_Zip' where code = 'QS';

update mk_courier_service set enable_status = 0 where code in ('DD', 'FF', 'RB', 'NK', 'UP', 'VK'); 

ALTER TABLE mk_courier_service drop column return_supported, add column return_supported tinyint(1) default 0;

update mk_courier_service set return_supported = 1 where code = 'ML';
