drop table if exists notification;
create table notification(
  id int not null auto_increment primary key,
  user varchar(128) not null,
  page varchar(100) not null,
  type varchar(100) not null,
  message varchar(2000) not null,
  created_on timestamp not null default now(),
  updated_on timestamp
);

insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('paymentPageUserTimeLimit','3','time in minutes after which cc will get notification about user spending more time on payment page');
