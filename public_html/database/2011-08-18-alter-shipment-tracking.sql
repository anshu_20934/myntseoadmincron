ALTER TABLE `mk_shipment_tracking_detail`  CHANGE COLUMN `location` `location` VARCHAR(150) NOT NULL AFTER `id`;

ALTER TABLE `mk_shipment_tracking_detail`  CHANGE COLUMN `activity_type` `activity_type` ENUM('AT','OT','DL','UD','IT','RT','RD','UN') NOT NULL DEFAULT 'OT' AFTER `action_date`,  ADD COLUMN `external_tracking_code` VARCHAR(10) NULL DEFAULT NULL AFTER `activity_type`;

ALTER TABLE `mk_shipment_tracking`  CHANGE COLUMN `tracking_no` `tracking_no` VARCHAR(255) NOT NULL AFTER `shipment_type`,  CHANGE COLUMN `return_tracking_no` `return_tracking_no` VARCHAR(255) NULL DEFAULT NULL AFTER `tracking_no`;

ALTER TABLE `mk_shipment_tracking`  CHANGE COLUMN `pickup_date` `pickup_date` DATETIME NULL AFTER `failed_attempts`,  CHANGE COLUMN `delivery_date` `delivery_date` DATETIME NULL AFTER `pickup_date`;