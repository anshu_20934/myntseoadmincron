INSERT INTO `myntra`.`mk_feature_gate_key_value_pairs` (`key`, `value`, `description`) VALUES ('UnisexRevenueAsFemale', '10', 'int 0-100 Percentage value - unisex revenue will be damped by that factor as female revenue');

INSERT INTO `myntra`.`mk_feature_gate_key_value_pairs` (`key`, `value`, `description`) VALUES ('MaleRevenueAsFemale', '80', 'int 0-100 Percentage value - male revenue will be damped by that factor as female revenue');

INSERT INTO `myntra`.`mk_feature_gate_key_value_pairs` (`key`, `value`, `description`) VALUES ('RatioForGenderSpecificRevenue', '3', 'int if female count is x times more then sorting based on female revenue and vice versa');

