DROP TABLE IF EXISTS cart_order_mapping;
CREATE TABLE cart_order_mapping (
  cartId varchar(50) NOT NULL,
  orderId int(20) NOT NULL default 0,
  cart_data mediumtext,
  order_date timestamp NOT NULL default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (cartId)
);
