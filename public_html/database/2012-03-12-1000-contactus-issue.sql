CREATE TABLE `contactus_issue` (
  `issue_id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_level_1` varchar(200) not null, 
  `issue_level_2` varchar(200), 
  `issue_level_3` varchar(200), 
  `is_active` tinyint default 1 not null,
  PRIMARY KEY (`issue_id`),
  KEY `issue_level_1` (`issue_level_1`) USING HASH,
  KEY `issue_level_2` (`issue_level_2`) USING HASH,
  KEY `issue_level_3` (`issue_level_3`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='all the issues related to contact us';

CREATE TABLE `contactus_issue_field` (
  `issue_field_id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) not null,
  `field_type` enum('mandatory','optional') default 'optional' not null,
  `field_label` varchar(100),
  `maxchar` smallint default null,
  `is_active` tinyint default 1 not null,
  PRIMARY KEY (`issue_field_id`),
  KEY `issue_id` (`issue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='all the fields relaetd to a contact us issue';

CREATE TABLE `contactus_issue_instance` (
  `instance_id` int(11) NOT NULL auto_increment,    
  `issue_subject` varchar(250),
  `issue_detail` text,
  `customer_email` varchar(200) not null,
  `response_time` int(11) not null,
  PRIMARY KEY (`instance_id`),
  KEY `customer_email` (`customer_email`) USING HASH,
  KEY `response_time` (`response_time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='captures contactus issue instance detail';

CREATE TABLE `contactus_issue_instance_field` (
  `instance_id` int(11) NOT NULL,    
  `issue_field_id` int(11) NOT NULL,
  `issue_field_value` text, 
  PRIMARY KEY `instance_id_issue_field_id` (`instance_id`, `issue_field_id`),
  KEY `instance_id` (`instance_id`), 
  KEY `issue_field_id` (`issue_field_id`)  
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='captures contactus issue instance field detail';

insert into mk_feature_gate_key_value_pairs (`key`,value,description) values('Contactus.Message','YOUR QUERY HAS BEEN SUBMITTED. WE\'LL RESPOND TO YOU WITHIN 24 HOURS.','This is the message we show on successful submission of contactus form');
insert into mk_feature_gate_key_value_pairs (`key`,value,description) values('Contactus.Form','false','true:will make the form visible, false:vice-varsa');
insert into mk_feature_gate_key_value_pairs (`key`,value,description) values('Contactus.Login','true','true:will make user login mandatory, false:login is not mandatory');

alter table contactus_issue_instance add column leaf_issue_id int default null after instance_id;