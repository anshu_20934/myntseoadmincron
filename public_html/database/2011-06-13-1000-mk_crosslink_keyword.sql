CREATE TABLE `mk_crosslink_keyword` (
  `id` int auto_increment NOT NULL,
  `crosslink_keyword` varchar(250) NOT NULL,  
  `is_active` tinyint NOT NULL default 1,
  PRIMARY KEY (`id`),
  KEY(`crosslink_keyword`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;


