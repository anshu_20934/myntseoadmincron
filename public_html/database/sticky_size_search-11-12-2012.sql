CREATE TABLE `sticky_size_search` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(128) NOT NULL,
  `size_group` varchar(20) NOT NULL,
  `sizes_csv` text,
  PRIMARY KEY (`id`),
  INDEX (`login`),
  INDEX (`size_group`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


insert into mk_abtesting_tests (name, ga_slot, seg_algo,source_type) values ('stickySize', '0', 'abtest\\algos\\impl\\RandomSegmentationAlgo', 'tpl');
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('test', (select id from mk_abtesting_tests where name ='stickySize'), 100);
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('control', (select id from mk_abtesting_tests where name ='stickySize'), 0);
