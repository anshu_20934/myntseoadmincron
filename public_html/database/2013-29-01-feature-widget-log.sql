CREATE TABLE `mk_feature_gate_key_value_pairs_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(45) DEFAULT NULL,
  `value` mediumtext,
  `description` varchar(255) DEFAULT NULL,
  `time_id` varchar(20) DEFAULT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `operation_type` varchar(10) DEFAULT NULL,

  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ;

CREATE TABLE `mk_widget_key_value_pairs_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(45) DEFAULT NULL,
  `value` mediumtext,
  `description` varchar(255) DEFAULT NULL,
  `time_id` varchar(20) DEFAULT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `operation_type` varchar(10) DEFAULT NULL,

  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ;

