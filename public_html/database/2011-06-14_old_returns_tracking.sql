
CREATE TABLE IF NOT EXISTS mk_old_returns_tracking_ud (
	id              BIGINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
	orderid         BIGINT UNSIGNED NOT NULL UNIQUE,
	iscod		TINYINT UNSIGNED NOT NULL DEFAULT 0,
	courier         VARCHAR(30),
	trackingno      VARCHAR(80),
	currstate       ENUM('UDQ','UDC1','UDC2','UDC3','UDCC','UDLS', 'END') NOT NULL,
	attemptcount	TINYINT UNSIGNED NOT NULL DEFAULT 0,
	reason		VARCHAR(80),	
	custname        VARCHAR(60) NOT NULL,
        pickupaddress   TEXT NOT NULL,
        pickupcity      VARCHAR(80) NOT NULL,
        pickupstate     VARCHAR(80) NOT NULL,
        pickupcountry   VARCHAR(80) NOT NULL,
        pickupzipcode   VARCHAR(10) NOT NULL,
        phonenumber     VARCHAR(15) NOT NULL,
	resolution	ENUM('RJ', 'RA')
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS mk_old_returns_tracking_ud_activity (
	udid	        BIGINT UNSIGNED NOT NULL,
        activitycode    VARCHAR(10) NOT NULL,
        activitydate    TIMESTAMP DEFAULT 0,
        recorddate      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        adminuser       VARCHAR(40) NOT NULL,
        remark          TEXT,
	cc_disposition	ENUM('NRB','NRS','BSY', 'CM'),
	cc_resolution	ENUM('RJ', 'RA'),		
        time_since_last_activity INTEGER UNSIGNED DEFAULT 0,
        sla_time        INTEGER UNSIGNED DEFAULT 0
) ENGINE = InnoDB;

ALTER TABLE mk_old_returns_tracking_ud_activity ADD FOREIGN KEY mk_old_returns_tracking_ud_udid (udid) REFERENCES mk_old_returns_tracking_ud(id) ON DELETE CASCADE;

CREATE TABLE IF NOT EXISTS mk_old_returns_tracking(
	id		BIGINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
	orderid		BIGINT UNSIGNED NOT NULL UNIQUE,
	iscod		TINYINT UNSIGNED NOT NULL DEFAULT 0,
	returntype	ENUM('RT', 'RTO') NOT NULL DEFAULT 'RT',
	returnreason	ENUM('Product Quality Issue', 'Fitment Issue', 'Product Not As Displayed', 'Wrong Product Shipped', 'Wrong Product Ordered'),
	rtocancelreason	ENUM('Customer Request', 'Customer Unreachable'),
	currstate	ENUM('RTOQ','RTOC','RTORS', 'RTORSC', 'RTORF', 'RTOAI', 'RTQ','RTCSD','RTPI', 'RTPTU', 'RTR','RTQP','RTQF','RTCC','RTQPA','RTQPNA','RTRF','RTAI','RTRS','RTRSC', 'END', 'CMMNT') NOT NULL,
	shipmenttype	ENUM('SS', 'MS') NOT NULL DEFAULT 'MS',
	courier		VARCHAR(30),
	trackingno	VARCHAR(80),
	revcourier	VARCHAR(30),
	revtrackingno	VARCHAR(80),
	custname	VARCHAR(60) NOT NULL,
	pickupaddress	TEXT NOT NULL,
	pickupcity	VARCHAR(80) NOT NULL,
	pickupstate	VARCHAR(80) NOT NULL,
	pickupcountry	VARCHAR(80) NOT NULL,
	pickupzipcode	VARCHAR(10) NOT NULL,
	phonenumber	VARCHAR(15) NOT NULL,
	pickupdate	TIMESTAMP DEFAULT 0,
	couponcode	VARCHAR(25),
	couponvalue	INT UNSIGNED NOT NULL DEFAULT 0,
	item_val_rejected INT UNSIGNED NOT NULL DEFAULT 0,
	item_val_restocked INT UNSIGNED NOT NULL DEFAULT 0				
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS mk_old_returns_tracking_sla(
	prevstate 	ENUM('RTOQ','RTOC','RTORS', 'RTORSC', 'RTORF', 'RTOAI', 'RTQ','RTCSD','RTPI', 'RTPTU', 'RTR','RTQP','RTQF','RTCC','RTQPA','RTQPNA','RTRF','RTAI','RTRS','RTRSC', 'END', 'CMMNT', 'UDQ', 'UDC1', 'UDC2', 'UDC3', 'UDCC', 'UDLS'),
	nextstate	ENUM('RTOQ','RTOC','RTORS', 'RTORSC', 'RTORF', 'RTOAI', 'RTQ','RTCSD','RTPI', 'RTPTU', 'RTR','RTQP','RTQF','RTCC','RTQPA','RTQPNA','RTRF','RTAI','RTRS','RTRSC', 'END', 'CMMNT', 'UDQ', 'UDC1', 'UDC2', 'UDC3', 'UDCC', 'UDLS'),
	maxtime		INTEGER UNSIGNED NOT NULL
) ENGINE = InnoDB;

ALTER TABLE mk_old_returns_tracking_sla ADD PRIMARY KEY (prevstate, nextstate);

CREATE TABLE IF NOT EXISTS mk_old_returns_tracking_activity(
	returnid	BIGINT UNSIGNED NOT NULL,
	activitycode	VARCHAR(10) NOT NULL,
	activity	VARCHAR(40) NOT NULL,
	activitydate	TIMESTAMP DEFAULT 0,
	recorddate	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	adminuser	VARCHAR(40) NOT NULL,
	remark		TEXT,
	time_since_last_activity INTEGER UNSIGNED DEFAULT 0,
	sla_time	INTEGER UNSIGNED DEFAULT 0	
) ENGINE = InnoDB;

ALTER TABLE mk_old_returns_tracking_activity ADD FOREIGN KEY mk_old_returns_tracking_returnid (returnid) REFERENCES mk_old_returns_tracking(id) ON DELETE CASCADE;
ALTER TABLE mk_old_returns_tracking_activity ADD UNIQUE KEY (returnid,activity);

INSERT INTO mk_old_returns_tracking_sla VALUES ('RTQ', 'RTPI', 720);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTQ', 'RTCSD', 2880);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTPI', 'RTPTU', 1440);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTPTU', 'RTR', 5760);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTCSD', 'RTR', 5760);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTR', 'RTQP', 240);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTR', 'RTQF', 240);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTQP', 'RTQPA', 60);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTQP', 'RTQPNA', 60);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTQF', 'RTCC', 120);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTQPA', 'RTRF', 120);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTQPNA', 'RTCC', 120);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTCC', 'RTRS', 1440);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTRS', 'RTRSC', 5760);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTQPA', 'RTAI', 240);

INSERT INTO mk_old_returns_tracking_sla VALUES ('RTOQ', 'RTOC', 120);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTOQ', 'RTORS', 120);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTOC', 'RTORF', 120);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTORS', 'RTORSC', 5760);
INSERT INTO mk_old_returns_tracking_sla VALUES ('RTOC', 'RTOAI', 240);

INSERT INTO mk_old_returns_tracking_sla VALUES ('UDQ', 'UDC1', 360);
INSERT INTO mk_old_returns_tracking_sla VALUES ('UDC1', 'UDC2', 360);
INSERT INTO mk_old_returns_tracking_sla VALUES ('UDC1', 'UDCC', 360);
INSERT INTO mk_old_returns_tracking_sla VALUES ('UDC2', 'UDC3', 360);
INSERT INTO mk_old_returns_tracking_sla VALUES ('UDC2', 'UDCC', 360);
INSERT INTO mk_old_returns_tracking_sla VALUES ('UDC3', 'UDCC', 360);
INSERT INTO mk_old_returns_tracking_sla VALUES ('UDCC', 'UDLS', 120);
