CREATE TABLE `mk_left_panel_search_link` (
	`link_id` int(11) NOT NULL AUTO_INCREMENT,
	`link_name` varchar(50) NOT NULL,
	`link` varchar(255) NOT NULL,
	`link_description` varchar(255) DEFAULT NULL,
	`is_active` tinyint(1) default NULL,
	`link_position` int(11) NOT NULL,
	PRIMARY KEY (`link_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;