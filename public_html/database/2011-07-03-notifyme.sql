
drop table if exists mk_notifications;
drop table if exists mk_notification_subs;
drop table if exists mk_notification_subs_instock;
drop table if exists mk_notification_subs_instock_unavailable;




CREATE TABLE  `mk_notifications` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(45) DEFAULT NULL,
 `type` int default 0,
 UNIQUE KEY `key_name_unique` (`name`),
 PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

CREATE TABLE `mk_notification_subs` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `notif_id` int(11) NOT NULL ,
 `email` varchar(45) DEFAULT NULL,
 `mobile` varchar(20) DEFAULT NULL,
 `notified` tinyint default 0,
 `lastmodified` timestamp default current_timestamp,
 PRIMARY KEY(id),
 KEY `key_notifid_email` (notif_id,email)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

CREATE TABLE  `mk_notification_subs_instock` (
 `notif_subs_id` int(11) NOT NULL ,
 `sku` int(11) not NULL,
 KEY `key_notif_subs_id_sku` (notif_subs_id,sku)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

CREATE TABLE  `mk_notification_subs_instock_unavailable` (
 `notif_subs_id` int(11) NOT NULL ,
 `style` int(11) not NULL,
 `size` varchar(20) not NULL,
 KEY `notif_subs_id_style_size` (notif_subs_id,style,size)
) ENGINE=INNODB DEFAULT CHARSET=latin1;


alter table mk_feature_gate_key_value_pairs modify  `key` VARCHAR(45);
insert into mk_notifications (name,type) values('customernotifyme.instock',2);


CREATE TABLE IF NOT EXISTS `myntra_notification_message_details` (
  `message_details_id` BIGINT(12) NOT NULL AUTO_INCREMENT,
  `sent` TINYINT DEFAULT '0',
  `queuedtime` DATETIME DEFAULT NULL,
  `dispatchtime` DATETIME DEFAULT NULL,
  `comment` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`message_details_id`)
);

CREATE TABLE IF NOT EXISTS `myntra_notification_template_details` (
  `template_details_id` BIGINT(12) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(64) NOT NULL,
  `type` TINYINT NOT NULL,
  `body` TEXT NOT NULL,
  `created_by` VARCHAR(128),
  `created_on` DATETIME ,
  `last_modified` DATETIME,
  `comment` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`template_details_id`),
  UNIQUE KEY `name` (`name`)
);

CREATE TABLE IF NOT EXISTS `myntra_notification_sms_details` (
  `sms_details_id` BIGINT(12),
  `mode` TINYINT NOT NULL,
  `sendto` VARCHAR(2048),
  `body` TEXT,
  `template_id` BIGINT(12) DEFAULT NULL,
  PRIMARY KEY (`sms_details_id`),
  CONSTRAINT `sms_details_message_details_id`FOREIGN KEY (`sms_details_id`) REFERENCES myntra_notification_message_details(`message_details_id`),
  CONSTRAINT `sms_details_template_details_id`FOREIGN KEY (`template_id`) REFERENCES myntra_notification_template_details(`template_details_id`)
);

CREATE TABLE IF NOT EXISTS `myntra_notification_email_details` (
  `email_details_id` BIGINT(12),
  `mode` TINYINT NOT NULL,
  `sendto` VARCHAR(2048),
  `cc` VARCHAR(2048),
  `bcc` VARCHAR(2048),
  `sender` VARCHAR(128) NOT NULL,
  `subject` VARCHAR(1024) DEFAULT NULL,
  `mimetype` VARCHAR(128) NOT NULL,
  `body` TEXT,
  `template_id` BIGINT(12) DEFAULT NULL,
   PRIMARY KEY (`email_details_id`),
  CONSTRAINT `email_details_message_details_id`FOREIGN KEY (`email_details_id`) REFERENCES myntra_notification_message_details(`message_details_id`),
  CONSTRAINT `email_details_template_details_id`FOREIGN KEY (`template_id`) REFERENCES myntra_notification_template_details(`template_details_id`)
); 

