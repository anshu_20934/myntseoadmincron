create table org_emp_promo_map(
	empid varchar(50),
	orgid int,
	promocode varchar(255),
	PRIMARY KEY  (`empid`,`orgid`),
	index (`empid`)
)ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='latin1_swedish_ci';


update xcart_customers set account_type='OG', orgid=24 ,email='arun.kumar@myntra.com',title='Mr',gender='M', DOB='1990-01-01', firstname='arun', lastname='kumar',ship2diff='N', termcondition=1, status='A' where login='arun.kumar@myntra.com';
insert into mk_org_user_map (orgid,login,active,usetype,employee_id) values(24,'arun.kumar@myntra.com',1,'OG','dummy01');
insert into org_emp_promo_map (empid,orgid,promocode) values('dummy01',24,'5013011234567890');


insert into xcart_discount_coupons (coupon,groupid,discount,coupon_type,minimum,times,per_user,times_used,expire,status,provider,producttypeid) values ('5013010732122174','0','100.00','percent','0.00','1','N','0',unix_timestamp(now())+86400*120,'A','myntraProvider','261');
insert into xcart_discount_coupons (coupon,groupid,discount,coupon_type,minimum,times,per_user,times_used,expire,status,provider,producttypeid) values ('5013011196796586','0','100.00','percent','0.00','1','N','0',unix_timestamp(now())+86400*120,'A','myntraProvider','261');
insert into xcart_discount_coupons (coupon,groupid,discount,coupon_type,minimum,times,per_user,times_used,expire,status,provider,producttypeid) values ('5013011160796554','0','100.00','percent','0.00','1','N','0',unix_timestamp(now())+86400*120,'A','myntraProvider','261');
insert into xcart_discount_coupons (coupon,groupid,discount,coupon_type,minimum,times,per_user,times_used,expire,status,provider,producttypeid) values ('5013011625307047','0','100.00','percent','0.00','1','N','0',unix_timestamp(now())+86400*120,'A','myntraProvider','261');
insert into xcart_discount_coupons (coupon,groupid,discount,coupon_type,minimum,times,per_user,times_used,expire,status,provider,producttypeid) values ('5013011924830855','0','100.00','percent','0.00','1','N','0',unix_timestamp(now())+86400*120,'A','myntraProvider','261');




insert into xcart_customers (login,usertype,password,gender,DOB,title,firstname,lastname,s_firstname,s_lastname,s_address,s_city,s_state,s_country,s_zipcode,email,mobile,first_login,last_login,status,termcondition,account_type,orgid)
values('SAMUEL_NAVNEETH@MINDTREE.COM','C','B-ea26f46706f489322cfa7dae1e70e56d','M','1979-11-16','Mr','Samuel S','Navneeth','Samuel S','Navneeth','#10, 9th cross,16th Main','bangalore','KA','IN','560029','SAMUEL_NAVNEETH@MINDTREE.COM','9740156156',unix_timestamp(now()),unix_timestamp(now()),'A',1,'OG',24);
insert into mk_org_user_map (orgid,login,active,usetype,employee_id) values(24,'SAMUEL_NAVNEETH@MINDTREE.COM',1,'OG','1011293');
insert into org_emp_promo_map (empid,orgid,promocode) values('1011293',24,'5013010732122174');

insert into xcart_customers (login,usertype,password,gender,DOB,title,firstname,lastname,s_firstname,s_lastname,s_address,s_city,s_state,s_country,s_zipcode,email,mobile,first_login,last_login,status,termcondition,account_type,orgid)
values('Sharmila_Prakash@mindtree.com','C','B-ea26f46706f489322cfa7dae1e70e56d','F','1980-09-20','Ms','Sharmila','Prakash','Sharmila','Prakash','102, 2nd Cross, 4th Phase - JP Nagar, Dollars Colony','bangalore','KA','IN','560078','Sharmila_Prakash@mindtree.com','',unix_timestamp(now()),unix_timestamp(now()),'A',1,'OG',24);
insert into mk_org_user_map (orgid,login,active,usetype,employee_id) values(24,'Sharmila_Prakash@mindtree.com',1,'OG','1001337');
insert into org_emp_promo_map (empid,orgid,promocode) values('1001337',24,'5013011196796586');

insert into xcart_customers (login,usertype,password,gender,DOB,title,firstname,lastname,s_firstname,s_lastname,s_address,s_city,s_state,s_country,s_zipcode,email,mobile,first_login,last_login,status,termcondition,account_type,orgid)
values('Sindhu_Subhashini@mindtree.com','C','B-ea26f46706f489322cfa7dae1e70e56d','F','1975-03-23','Ms','Sindhu','S','Sindhu','S','A-10 Srusti Apartements','bangalore','KA','IN','560037','Sindhu_Subhashini@mindtree.com','',unix_timestamp(now()),unix_timestamp(now()),'A',1,'OG',24);
insert into mk_org_user_map (orgid,login,active,usetype,employee_id) values(24,'Sindhu_Subhashini@mindtree.com',1,'OG','1003665');
insert into org_emp_promo_map (empid,orgid,promocode) values('1003665',24,'5013011160796554');

insert into xcart_customers (login,usertype,password,gender,DOB,title,firstname,lastname,s_firstname,s_lastname,s_address,s_city,s_state,s_country,s_zipcode,email,mobile,first_login,last_login,status,termcondition,account_type,orgid)
values('Babuji_Abraham@mindtree.com','C','B-ea26f46706f489322cfa7dae1e70e56d','M','1964-05-26','Mr','Babuji Philip','Abraham','Babuji Philip','Abraham','','','','IN','','Babuji_Abraham@mindtree.com','',unix_timestamp(now()),unix_timestamp(now()),'A',1,'OG',24);
insert into mk_org_user_map (orgid,login,active,usetype,employee_id) values(24,'Babuji_Abraham@mindtree.com',1,'OG','1000033');
insert into org_emp_promo_map (empid,orgid,promocode) values('1000033',24,'5013011625307047');

