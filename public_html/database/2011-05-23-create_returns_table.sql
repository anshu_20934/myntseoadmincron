DROP TABLE IF EXISTS xcart_returns;

CREATE TABLE xcart_returns (
  returnid int(11) NOT NULL AUTO_INCREMENT,
  orderid int(20) NOT NULL,
  itemid varchar(255) NOT NULL,
  skucode varchar(255) NOT NULL,
  quantity int(5) NOT NULL,
  sizeoption varchar(32) NOT NULL,
  status varchar(64) NOT NULL,
  login varchar(255) NOT NULL,
  name varchar(32) DEFAULT NULL,
  address text,
  city varchar(64) DEFAULT NULL,
  state varchar(32) DEFAULT NULL,
  country varchar(32) DEFAULT NULL,
  zipcode varchar(32) DEFAULT NULL,
  mobile varchar(32) DEFAULT NULL,
  phone varchar(32) DEFAULT NULL,
  email varchar(255) DEFAULT NULL,
  return_mode varchar(32) NOT NULL,
  courier_service varchar(255) DEFAULT NULL,
  tracking_no varchar(255) DEFAULT NULL,
  reship_courier varchar(255) DEFAULT NULL,
  reship_tracking_no varchar(255) DEFAULT NULL,
  reason varchar(255) DEFAULT NULL,
  description text,
  pickup_charges decimal(12,2) DEFAULT '0.00',
  qafail_reason varchar(255) DEFAULT NULL,
  couponcode varchar(64) DEFAULT NULL,
  refundamount decimal(12,2) DEFAULT NULL,
  createddate int(11) NOT NULL,
  sentfortrackingdate int(11) DEFAULT NULL,
  pickupinitdate int(11) DEFAULT NULL,
  pickupdate int(11) DEFAULT NULL,
  receiveddate int(11) DEFAULT NULL,
  delivereddate int(11) DEFAULT NULL,
  qapassdate int(11) DEFAULT NULL,
  restockeddate int(11) DEFAULT NULL,
  rejecteddate int(11) DEFAULT NULL,
  qafaildate int(11) DEFAULT NULL,
  reshippeddate int(11) DEFAULT NULL,
  redelivereddate int(11) DEFAULT NULL,
  itembarcode varchar(20) DEFAULT NULL,
  createdby varchar(255) NOT NULL,
  cancelleddate int(11) DEFAULT NULL,
  cancel_reason text,
  trackingupdatetime int(11) DEFAULT NULL,
  PRIMARY KEY (returnid),
  KEY rt_login_idx (login),
  KEY rt_orderid_idx (orderid),
  KEY rt_itemid_idx (itemid),
  KEY rt_createddate_idx (createddate),
  KEY rt_receiveddate_idx (receiveddate),
  KEY rt_qapassdate_idx (qapassdate),
  KEY rt_qafaildate_idx (qafaildate),
  KEY rt_status_idx (status)
) ENGINE=MyISAM AUTO_INCREMENT=10000 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;


DROP TABLE IF EXISTS mk_returns_reasons;

CREATE TABLE mk_returns_reasons (
  id int(5) NOT NULL AUTO_INCREMENT,
  code varchar(5) NOT NULL,
  displayname varchar(255) NOT NULL,
  description varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

insert into mk_returns_reasons (code, displayname) values ('PLS', 'Fitment issue - Too Large');
insert into mk_returns_reasons (code, displayname) values ('PSS', 'Fitment issue - Too Small');
insert into mk_returns_reasons (code, displayname) values ('PQM', 'Product Quality Issue - Material');
insert into mk_returns_reasons (code, displayname) values ('PMD', 'Product Quality Issue - Manufacturing Defect');
insert into mk_returns_reasons (code, displayname) values ('WIS', 'Wrong Item Shipped');
insert into mk_returns_reasons (code, displayname) values ('PNW', 'Product not as Displayed on Website');
insert into mk_returns_reasons (code, displayname) values ('ISS', 'Wrong Size Shipped');


DROP TABLE IF EXISTS mk_returns_comments_log;

create table mk_returns_comments_log (
    commentid             int(11) auto_increment,
    returnid              int(11) NOT NULL,
    commenttype           varchar(255),
    commenttitle          varchar(255),
    addedby               varchar(255),
    description           text,
    adddate               int(11) NOT NULL,

    PRIMARY KEY (commentid)
);


drop table if exists mk_return_status_details;

CREATE TABLE mk_return_status_details (
  id int(5) NOT NULL AUTO_INCREMENT,
  code varchar(5) DEFAULT NULL,
  display_name varchar(255) NOT NULL,
  mymyntra_msg varchar(500) NOT NULL,
  comments_template varchar(255) DEFAULT NULL,
  end_state tinyint(1) DEFAULT '0',
  cust_end_state tinyint(1) DEFAULT '0',
  mymyntra_status varchar(100) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

insert into mk_return_status_details (code,display_name,mymyntra_status,mymyntra_msg,end_state,cust_end_state) values
('RRD','Return Request Declined','DECLINED','Your return request has been declined.',1,1),
('RRQP','Returns Pickup Request Queued','QUEUED','We will arrange a pickup of this return within 1-2 working days, between 2-7 pm.',0,0),
('RRQS','Returns SelfShip Request Queued','QUEUED','We are awaiting the update of your return shipment details. If you have already shipped your return, please update the details here. This will allow us to track their delivery to our Returns Processing Facility.',0,0),
('RPI','Return Pickup Initiated','QUEUED','We will arrange a pickup of your return within [PICKUP_PROMISE_DATE], between 2-7 pm.',0,0),
('RPF','Return Pickup Failed','QUEUED','We have been unable to pickup your return on our first attempt. Please call our Customer Care at [CUST_CARE] in case you would like to have the pickup re-attempted or if you would like to ship the return yourself.',0,0),
('RPI2','Return Pickup Initiated (attempt 2)','QUEUED','We will arrange a pickup of your return within [RPI_2_PROMISE_DATE], between 2-7 pm.',0,0),
('RPU','Return Picked Up','IN TRANSIT','Your return has been picked up by [COURIER_INFO] with Tracking Number: [TRACKING_NO]. In case of discrepancy, contact our Customer Care at [CUST_CARE].',0,0),
('RDU','Returns Details Updated','IN TRANSIT','Thank you for sharing your return shipment details. We will let you know once we have received your return shipment.',0,0),
('RRC','Return Received','RECEIVED','We have received your return on [RECEIVED_DATE]. It will be processed by [PROCESSING_PROMISE_DATE].',0,0),
('RQP','Return QA Pass','RECEIVED','We have received your return on [RECEIVED_DATE]. It will be processed by [PROCESSING_PROMISE_DATE].',0,0),
('RQF','Return QA Fail','RECEIVED','We have received your return on [RECEIVED_DATE]. It will be processed by [PROCESSING_PROMISE_DATE].',0,0),
('RQSP','Return QA Supervisor Pass','ACCEPTED','Your return has been accepted by us. You will be issued a refund on this item within 24 hours.',0,0),
('RQSF','Return QA Supervisor Fail','REJECTED','Your return request has been declined as it did not meet our <a href="[GUIDE_LINES_LINK]" onclick="_gaq.push([\'_trackEvent\', \'myreturns\', \'view_returns_policy\', \'click\']);" target="_blank">Returns Policy</a>. We will contact you soon regarding this.',0,0),
('RRRS','Rejected Returns to be ReShipped','REJECTED','Your return request request has been declined as it did not meet our <a href="[GUIDE_LINES_LINK]" onclick="_gaq.push([\'_trackEvent\', \'myreturns\', \'view_returns_policy\', \'click\']);" target="_blank">Returns Policy</a>. The item will be shipped back to you.',0,0),
('RRJ','Rejected Returns - to be disposed','REJECTED','Your return request has been declined as it did not meet our <a href="[GUIDE_LINES_LINK]" onclick="_gaq.push([\'_trackEvent\', \'myreturns\', \'view_returns_policy\', \'click\']);" target="_blank">Returns Policy</a>. As confirmed with you, your return will be disposed of by us.',0,1),
('RRS','Returns ReShipped','REJECTED','Your return request has been declined as it did not meet our <a href="[GUIDE_LINES_LINK]" onclick="_gaq.push([\'_trackEvent\', \'myreturns\', \'view_returns_policy\', \'click\']);" target="_blank">Returns Policy</a>. The item has been shipped back to you through [RESHIP_COURIER_INFO] with tracking number: [RESHIP_TRACKING] on [RESHIP_DATE]',0,1),
('RSD','ReShipped return Delivered','REJECTED','Your return request has been declined as it did not meet our <a href="[GUIDE_LINES_LINK]" target="_blank">Returns Policy</a>. The item has been delivered back to you through [RESHIP_COURIER_INFO] with tracking number: [RESHIP_TRACKING] on [REDELIVER_DATE]',1,1),
('RFI','ReFund Issued','ACCEPTED','A refund amount of Rs [REFUND_AMOUNT] has been credited to your Cashback account on [REFUND_DATE].',0,1),
('RIS','Return Item restocked','ACCEPTED','A refund amount of Rs [REFUND_AMOUNT] has been credited to your Cashback account on [REFUND_DATE].',1,1),
('RJS','Restocked after disposed','REJECTED','Your return request has been declined as it did not meet our <a href="[GUIDE_LINES_LINK]" onclick="_gaq.push([\'_trackEvent\', \'myreturns\', \'view_returns_policy\', \'click\']);" target="_blank">Returns Policy</a>. As confirmed with you, your return will be disposed of by us.',1,1),
('RPUQ2','Returns Pickup Queued (2nd time)','QUEUED','We will arrange a pickup of this return within 1-2 working days, between 2-7pm. ',0,0);


drop table if exists mk_return_state_transitions;

CREATE TABLE mk_return_state_transitions (
  id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  from_state varchar(64) NOT NULL,
  to_state varchar(64) NOT NULL,
  description varchar(255) DEFAULT NULL,
  sla int(10) DEFAULT NULL,
  admin_access tinyint(1) DEFAULT NULL,
  roles_allowed varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY id (id)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

insert into mk_return_state_transitions (from_state,to_state,description,sla,admin_access,roles_allowed) values
('RRQP','RDU','Initiated Pickup',24,0,'OP,AD'),
('RRQP','RPI','Pickup Initiated',24,0,'OP,AD'),
('RRQS','RDU','Return details updates',24,0,'OP,AD'),
('RRQS','RRC','Recieved returns',24,0,'OP,AD'),
('RPI','RPU','Confirming pickup',24,0,'OP,AD'),
('RPI','RDU','Return details updates',24,0,'OP,CS,AD'),
('RPI','RRD','Declining Return',24,0,'CS,AD'),
('RPI','RRC','Recieved returns',24,0,'OP,AD'),
('RPI','RPF','return pickup failed',24,0,'OP,AD'),
('RPF','RPUQ2','Re Queueing return',24,0,'CS,AD'),
('RPF','RRD','Declining Return',24,0,'CS,AD'),
('RPF','RRQS','Marking as self ship',24,0,'CS,AD'),
('RPUQ2','RPI2','Retrying return pickup',24,0,'OP,AD'),
('RPUQ2','RDU','Return Recieved',24,0,'OP,AD'),
('RPI2','RRD','Cancelling Return',24,0,'CS,AD'),
('RPI2','RRQS','Marking as self ship',24,0,'CS,AD'),
('RPI2','RPU','Confirming pickup',24,0,'OP,AD'),
('RPU','RRC','Recieved returns',24,0,'OP,AD'),
('RPU','RDU','Return details updates',24,0,'OP,CS,AD'),
('RDU','RRC','Recieved updates',24,0,'OP,AD'),
('RRC','RQP','Marking items as QA pass',24,0,'OP,AD'),
('RRC','RQF','Marking items as QA fail',24,0,'OP,AD'),
('RQP','RFI','Return refund initiated',24,0,'CS,OP,AD'),
('RQF','RQSP','Marking items as QA pass by supervisor',24,0,'OP,AD'),
('RQF','RQSF','Marking items as QA fail by supervisor',24,0,'OP,AD'),
('RQSP','RFI','Return refund initiated',24,0,'AD'),
('RQSF','RRRS','Reshipping rejected returns',24,0,'CS,AD'),
('RQSF','RRJ','Disposed rejected returns',24,0,'CS,AD'),
('RRRS','RRS','Rejected Returns Reshipped',24,1,'OP,AD'),
('RRJ','RJS','Restocking Returned Item',24,0,'IT,AD'),
('RRS','RSD','Reshipped Return Delivered',24,0,'CS,AD'),
('RFI','RIS','Restocking returned item',24,0,'OP,AD');


update mk_feature_gate_key_value_pairs set value = 'true' where `key` = 'returnenabled';

update mk_courier_service set reports_template = 'orderid as Refno, "" as Company, CONCAT(s_firstname," ",s_lastname) as "Name", mid(replace(s_address,"\r\n"," "),1,29) as Address1,mid(replace(s_address,"\r\n"," "),30,29) as Address2, concat(mid(replace(s_address,"\r\n"," "),59,10),",",s_city,",",s_state,",",s_country) as Address3,s_zipcode as Pincode,phone as Phone, mobile as Mobile' where code = 'BD';

insert into mk_feature_gate_key_value_pairs (`key`,`value`,description) values('returnMinDeliveryAmount','200','Minimum amount below which qa failure makes no sense');

alter table mk_courier_service add column (return_pickup_order int);
alter table mk_courier_service add column (pickup_reports_template text);

update mk_courier_service set return_pickup_order = 1 where code = 'ML-BLR';
update mk_courier_service set return_pickup_order = 2 where code = 'ML-DEL';
update mk_courier_service set return_pickup_order = 3 where code = 'BD';
update mk_courier_service set return_pickup_order = 4 where code = 'DH';
update mk_courier_service set return_pickup_order = 5 where code = 'QS';
update mk_courier_service set return_pickup_order = 6 where code = 'FX';
update mk_courier_service set return_pickup_order = 7 where code = 'AR';

update mk_courier_service set pickup_reports_template = '@rownum:=@rownum+1 as "Sl. No",now() as "Current date", createdBy as "Champion name",returnid as "Return #",name as "Cm Name",mobile as "Ph #",concat(address," ",city, " ",state, " ",country, " " ,zipcode) as "Pickup address"," " as "Pickup Date"," " as "Pickup Time"," " as "Additional Remarks"," " as "Pickup Status"," " as "Pickup Time"," " as "Pickup Failure Reason"," " as "Logistics Remarks/Actual Pickup Date"," " as "AWB #"';