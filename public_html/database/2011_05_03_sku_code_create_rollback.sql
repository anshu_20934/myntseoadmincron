ALTER TABLE mk_skus DROP COLUMN `description`;
ALTER TABLE mk_skus DROP COLUMN `brand_id`;
ALTER TABLE mk_skus DROP COLUMN `last_modified_on`;
ALTER TABLE mk_skus DROP COLUMN `article_type_id`;
ALTER TABLE mk_skus DROP COLUMN `created_by`;
ALTER TABLE mk_skus DROP COLUMN `created_on`;
ALTER TABLE mk_skus DROP COLUMN `vendor_article_no`;
ALTER TABLE mk_skus DROP COLUMN `vendor_article_name`;
ALTER TABLE mk_skus DROP COLUMN `remarks`;
ALTER TABLE mk_skus DROP COLUMN `size`;


ALTER TABLE mk_attribute_type_values DROP COLUMN `attribute_code`;
