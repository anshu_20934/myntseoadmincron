
insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) VALUES ('cancelOrderCouponCount', '2', 'No of coupons to be given to user when order is cancelled automatically');

insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) VALUES ('cancelOrderCouponMrp', '500', 'Mrp of coupon to be given to user as good will gesture on order cancellation');

insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) VALUES ('cancelOrderCouponMinPurchase', '1000', 'Minimum purchase amount on coupon to be given as good will gesture on order cancellation');

insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) VALUES ('cancelOrderCouponExpiryDays', '15', 'No of days for the coupon to expire given as good will gesture on order cancellation');

