DELETE FROM `mk_email_notification`
  WHERE name IN
  ('gateway_confirm_order_ptg_qualified', 
   'gateway_confirm_order_ptg_not_qualified', 
   'check_confirm_order_ptg_qualified', 
   'check_confirm_order_ptg_not_qualified', 
   'cod_confirm_order_ptg_qualified', 
   'cod_confirm_order_ptg_not_qualified');
