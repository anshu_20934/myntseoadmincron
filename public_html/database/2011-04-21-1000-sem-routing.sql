CREATE TABLE `mk_sem_routing` (
  `id` int auto_increment NOT NULL,
  `url` varchar(250) NOT NULL,
  `landing_url` varchar(250) NOT NULL,  
  `created_date` int NOT NULL,
  `is_active` tinyint NOT NULL default 1,
  PRIMARY KEY (`id`),
  KEY(`url`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
alter table mk_sem_routing add constraint unique(url);

