create table exchange_order_mappings (
  exchange_orderid int(20) NOT NULL AUTO_INCREMENT, 
  releaseid int(20), 
  itemid int(11), 
  returnid int(11),
  PRIMARY KEY (exchange_orderid),
  Index (exchange_orderid, releaseid, itemid, returnid)
) ENGINE=MyISAM AUTO_INCREMENT=10000 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
