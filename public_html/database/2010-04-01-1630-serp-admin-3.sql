ALTER TABLE  `serp_sets` ADD  `avg_conversion` FLOAT( 0 ) NOT NULL AFTER  `avg_revenue`;
ALTER TABLE  `serp_keywords` ADD  `isFuzzy` BIT( 0 ) NOT NULL AFTER  `urls`;
ALTER TABLE  `serp_keywords` ADD  `serp1` INT NOT NULL AFTER `serp`;

