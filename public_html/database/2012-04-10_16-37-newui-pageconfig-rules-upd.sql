INSERT INTO page_config_image_rule (location_type,section,width,height,min,max,step,size) VALUES 
('topnav-default','static',792,180,0,1,1,307200);

update page_config_image_rule set step = 3 where location_type = 'home' and section = 'static';

update page_config_image_rule set max = 1, min=0 where location_type = 'login' and section = 'static';

update page_config_image_rule set min=0 where location_type = 'search' and section = 'static';

update page_config_image_rule set step = 3, min=3 where location_type='landing' and section='static' ;

update page_config_image_rule set min = 1 where location_type='topnav-default' and section='static' ;