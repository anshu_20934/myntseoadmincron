use myntra;

/*845*/
/*link online addons to new option*/
update mk_product_addons set product_option_id=2806 where id=58 and style_id='845' and product_option_id=1930;
update mk_product_addons set product_option_id=2806 where id=59 and style_id='845' and product_option_id=1930;
update mk_product_addons set product_option_id=2806 where id=60 and style_id='845' and product_option_id=1930;

/*update pos option by new online option*/
update mk_product_options_pos set id=2806 where id=1930 and shop_master_id=39;

/*insert option for RPOS*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('2806','41',0);

/*insert addons for RPOS*/
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('58','41',0);
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('59','41',200);
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('60','41',400);


/*841*/
/*link online addons to new option*/
update mk_product_addons set product_option_id=2805 where id=51 and style_id='841' and product_option_id=1919;

/*update pos option by new online option*/
update mk_product_options_pos set id=2805 where id=1919 and shop_master_id=39;

/*insert option for RPOS*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('2805','41',0);

/*insert addons for RPOS*/
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('51','41',0);

/*846*/
/*insert option for RPOS*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('1937','41',0);

/*insert addons for RPOS*/
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('61','41',0);
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('62','41',500);
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('63','41',1000);


/*844*/
/*insert option for RPOS*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('1929','41',0);

/*insert addons for RPOS*/
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('55','41',0);
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('56','41',1000);
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('57','41',4000);


/*840*/
/*link online addons to new option*/
update mk_product_addons set product_option_id=2804 where id=50 and style_id='840' and product_option_id=1917;

/*update pos option by new online option*/
update mk_product_options_pos set id=2804 where id=1917 and shop_master_id=39;

/*insert option for RPOS*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('2804','41',0);

/*insert addons for RPOS*/
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('50','41',0);

/*839*/
/*link online addons to new option*/
update mk_product_addons set product_option_id=2803 where id=49 and style_id='839' and product_option_id=1918;

/*update pos option by new online option*/
update mk_product_options_pos set id=2803 where id=1918 and shop_master_id=39;

/*insert option for RPOS*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('2803','41',0);

/*insert addons for RPOS*/
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('49','41',0);


/*838*/
/*link online addons to new option*/
update mk_product_addons set product_option_id=2802 where id=48 and style_id='838' and product_option_id=1915;

/*update pos option by new online option*/
update mk_product_options_pos set id=2802 where id=1915 and shop_master_id=39;

/*insert option for RPOS*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('2802','41',0);

/*insert addons for RPOS*/
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('48','41',0);


/*837*/
/*link online addons to new option*/
update mk_product_addons set product_option_id=2801 where id=47 and style_id='837' and product_option_id=1916;

/*update pos option by new online option*/
update mk_product_options_pos set id=2801 where id=1916 and shop_master_id=39;

/*insert option for RPOS*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('2801','41',0);

/*insert addons for RPOS*/
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('47','41',0);

/*827*/
/*insert option for RPOS*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('1909','41',0);

/*insert addons for RPOS*/
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('33','41',0);


/*828*/
/*insert option for RPOS*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('1897','41',0);

/*insert addons for RPOS*/
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('35','41',0);


/*836*/
/*insert option for RPOS*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('1914','41',0);

/*insert addons for RPOS*/
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('46','41',0);


/*835*/
/*link online addons to new option*/
update mk_product_addons set product_option_id=2794 where id=44 and style_id='835' and product_option_id=1912;
update mk_product_addons set product_option_id=2794 where id=45 and style_id='835' and product_option_id=1912;
update mk_product_addons set product_option_id=2794 where id=54 and style_id='835' and product_option_id=1912;

/*update pos option by new online option*/
update mk_product_options_pos set id=2794 where id=1912 and shop_master_id=39;

/*insert option for RPOS*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('2794','41',0);

/*insert addons for RPOS*/
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('44','41',655);
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('45','41',0);
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('54','41',455);


/*834*/
/*link online addons to new option*/
update mk_product_addons set product_option_id=2792 where id=42 and style_id='834' and product_option_id=1911;
update mk_product_addons set product_option_id=2792 where id=43 and style_id='834' and product_option_id=1911;
update mk_product_addons set product_option_id=2792 where id=53 and style_id='834' and product_option_id=1911;

/*update pos option by new online option*/
update mk_product_options_pos set id=2792 where id=1911 and shop_master_id=39;

/*insert option for RPOS*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('2792','41',0);

/*insert addons for RPOS*/
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('42','41',1300);
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('43','41',0);
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('53','41',900);

/*833*/
/*link online addons to new option*/
update mk_product_addons set product_option_id=2791 where id=41 and style_id='833' and product_option_id=1905;

/*update pos option by new online option*/
update mk_product_options_pos set id=2791 where id=1905 and shop_master_id=39;

/*insert option for RPOS*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('2791','41',0);

/*insert addons for RPOS*/
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('41','41',0);


/*829*/
/*link online addons to new option*/
update mk_product_addons set product_option_id=2788 where id=36 and style_id='829' and product_option_id=1910;

/*update pos option by new online option*/
update mk_product_options_pos set id=2788 where id=1910 and shop_master_id=39;

/*insert option for RPOS*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('2788','41',0);

/*insert addons for RPOS*/
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('36','41',0);

/*830*/
/*link online addons to new option*/
update mk_product_addons set product_option_id=2789 where id=37 and style_id='830' and product_option_id=1908;

/*update pos option by new online option*/
update mk_product_options_pos set id=2789 where id=1908 and shop_master_id=39;

/*insert option for RPOS*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('2789','41',0);

/*insert addons for RPOS*/
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('37','41',0);

/*831*/
/*link online addons to new option*/
update mk_product_addons set product_option_id=2790 where id=38 and style_id='831' and product_option_id=1903;

/*update pos option by new online option*/
update mk_product_options_pos set id=2790 where id=1903 and shop_master_id=39;

/*insert option for RPOS*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('2790','41',0);

/*insert addons for RPOS*/
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('38','41',0);

/*832*/
/*link online addons to new option*/
update mk_product_addons set product_option_id=2793 where id=39 and style_id='832' and product_option_id=1913;
update mk_product_addons set product_option_id=2793 where id=40 and style_id='832' and product_option_id=1913;
update mk_product_addons set product_option_id=2793 where id=52 and style_id='832' and product_option_id=1913;

/*update pos option by new online option*/
update mk_product_options_pos set id=2793 where id=1913 and shop_master_id=39;

/*insert option for RPOS*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('2793','41',0);

/*insert addons for RPOS*/
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('39','41',800);
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('40','41',350);
insert into mk_product_addons_posprice (id,shop_master_id,price) values ('52','41',0);
/*run*/
