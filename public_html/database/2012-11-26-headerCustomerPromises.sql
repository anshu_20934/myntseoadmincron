UPDATE `myntra`.`mk_widget_key_value_pairs`
SET
value = '100% Original Products &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Free Shipping on Orders over Rs. system_free_shipping_amount &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Cash on Delivery &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 30-Day Returns',
description = 'Top Header Message : Customer Promises (please do not change the string system_free_shipping_amount)'
WHERE mk_widget_key_value_pairs.key = 'newui.topCpsMessage';
