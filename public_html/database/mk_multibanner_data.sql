-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.1.50-community - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2013-01-30 16:13:58
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table myntra.mk_multibanner_data
CREATE TABLE IF NOT EXISTS mk_multibanner_data (
  image_id bigint(20) NOT NULL,
  target_url varchar(255) NOT NULL,
  coordinates varchar(255) NOT NULL,
  title varchar(255) DEFAULT NULL,
  UNIQUE KEY clustered (image_id,coordinates)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE page_config_image_reference add multiclick tinyint(1);
