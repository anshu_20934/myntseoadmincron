use myntra;

update mk_product_type set type_h1='School Bags, College Bags with Your Text or Photo',description='Finding the right School Bag can be quite a challenge. Well, no longer as we bring to you a fabulous collection of personalized School Bags and College Bags, specifically designed for students. Buy School Bags that rank high on utility and comfort with the availabity of multiple pockets and compartments. Personalize your School bag with text or photo.', type_title='School Bags | College Bags |Buy Personalized School Bags Online in India', type_keywords='school bags, college bags, buy school bags, buy college bags, buy school bags india', type_metadesc='Buy personalized School Bags with text or photo of your choice embroidered. Starting price Rs. 599. Ships internationally within 4 days.' where id='234';

/*1.insert into color table(if new color has to be added)*/
insert into `mk_text_font_color` (color) values('#ad3481');

/*2.insert into mk_style_images tp get 75*75 for all styles(for new style to be added)*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1144','images/style/carea/T/kids-pink-1144.png','B','Y');
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1145','images/style/carea/T/kids-yellow-1145.png','B','Y');

/*3.update for bags styles orientations*/
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='80',textColorDefault='80' where id=1629;
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='143',textColorDefault='143' where id=1628;

/*4.insert into mk_text_font_detail*/
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1628','150','0.5','12','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1628','151','0.75','12','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1628','152','0.75','12','N','Y');

insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1629','150','0.5','12','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1629','151','0.75','12','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1629','152','0.75','12','N','Y');
