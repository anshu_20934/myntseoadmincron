drop table if exists mk_style_recommendation;
drop table if exists mk_style_recommendation_backup;
create table mk_style_recommendation(
  `style_id` int not null primary key,
  `recommended_styles` varchar(2000) not null,
  `created_by` VARCHAR(255) NULL ,
  `created_on` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `last_modified_on` datetime DEFAULT NULL
);

create table mk_style_recommendation_backup(
  `style_id` int not null primary key,
  `recommended_styles` varchar(2000) not null,
  `created_by` VARCHAR(255) NULL ,
  `created_on` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `last_modified_on` datetime DEFAULT NULL
);

insert into mk_abtesting_tests (name, ga_slot, seg_algo,source_type) 
	values ('showMyntraRecommendations', '0', 'abtest\\algos\\impl\\RandomSegmentationAlgo', 'tpl');

insert into mk_abtesting_variations (ab_test_id,name,percent_probability) 
	values((select id from mk_abtesting_tests where name='showMyntraRecommendations'),'test',0);
insert into mk_abtesting_variations (ab_test_id,name,percent_probability) 
	values((select id from mk_abtesting_tests where name='showMyntraRecommendations'),'control',100);