delete from `lg_look_book`;
INSERT INTO `lg_look_book` (id,name,summary,description,is_active) VALUES (1,"Kalki's STYLE BOOK","Kalki's STYLE BOOK","Kalki's STYLE BOOK",1);

delete from `lg_look`;
INSERT INTO `lg_look` (id,name,summary,description,is_active,image_look,image_buy_look,is_right_aligned,is_text_white) VALUES (1,'Playful Pleats','Playful Pleats',"Kalki pulls off the brogues and pleated top-shorts look gracefully, showing off her long shapely legs. Pleats add playfulness as well as that girlie charm, doesn't she look spunky?",1,'http://myntra.myntassets.com/skin2/images/kalki_playful_pleats_new_980x540.jpg','http://myntra.myntassets.com/skin2/images/kalki_playful_pleats_new_360x480.jpg',1,0);
INSERT INTO `lg_look` (id,name,summary,description,is_active,image_look,image_buy_look,is_right_aligned,is_text_white) VALUES (2,'Country Belle','Country Belle','Taking minimalism to the next level, combining it with grace that comes naturally to her, Kalki puts on a short denim dress with wedge heels, perfect for a lazy Sunday brunch!',1,'http://myntra.myntassets.com/skin2/images/kalki_country_belle_new_980x540.jpg','http://myntra.myntassets.com/skin2/images/kalki_country_belle_new_360x480.jpg',1,0);
INSERT INTO `lg_look` (id,name,summary,description,is_active,image_look,image_buy_look,is_right_aligned,is_text_white) VALUES (3,'Pretty in Pastels','Pretty in Pastels','Pastel on pastel is a trend that Kalki showcased very effectively through this look, the gorgeous colours and those dreamy eyes, very demure!',1,'http://myntra.myntassets.com/skin2/images/kalki_pretty_pastel_new_980x540.jpg','http://myntra.myntassets.com/skin2/images/kalki_pretty_pastel_new_360x480.jpg',1,0);
INSERT INTO `lg_look` (id,name,summary,description,is_active,image_look,image_buy_look,is_right_aligned,is_text_white) VALUES (4,'Luxe layering','Luxe layering',"What's the easiest way to enhance the appeal of an outfit? A jacket in an eye-catching colour! With a pair of black jeggings and a grey top to neutralise. This one is so simple, it's brilliant!",1,' http://myntra.myntassets.com/skin2/images/kalki_luxe_layering_new_980x540.jpg',' http://myntra.myntassets.com/skin2/images/kalki_luxe_layering_new_360x480.jpg',1,0);
INSERT INTO `lg_look` (id,name,summary,description,is_active,image_look,image_buy_look,is_right_aligned,is_text_white) VALUES (5,'Daytime Diva','Daytime Diva','Refashioning  a multi-coloured kurta into a dress and teaming it with funky slippers is something only Kalki can do with oodles of oomph! This one is our favourite.',1,' http://myntra.myntassets.com/skin2/images/kalki_daytime_diva_new_980x540.jpg',' http://myntra.myntassets.com/skin2/images/kalki_daytime_diva_new_360x480.jpg',1,0);
INSERT INTO `lg_look` (id,name,summary,description,is_active,image_look,image_buy_look,is_right_aligned,is_text_white) VALUES (6,'Boho Girl','Boho Girl','You would have never thought that a strappy top could look good with a pair of printed Patiala bottoms! But that was until Kalki wore it. What an explosion of colour and style!',1,' http://myntra.myntassets.com/skin2/images/kalki_boho_girl_new_980x540.jpg',' http://myntra.myntassets.com/skin2/images/kalki_boho_girl_new_360x480.jpg',1,0);

delete from `lg_look_book_lg_look_map`;
INSERT INTO `lg_look_book_lg_look_map` (fk_lg_look_book_id,fk_lg_look_id) VALUES (1,1);
INSERT INTO `lg_look_book_lg_look_map` (fk_lg_look_book_id,fk_lg_look_id) VALUES (1,2);
INSERT INTO `lg_look_book_lg_look_map` (fk_lg_look_book_id,fk_lg_look_id) VALUES (1,3);
INSERT INTO `lg_look_book_lg_look_map` (fk_lg_look_book_id,fk_lg_look_id) VALUES (1,4);
INSERT INTO `lg_look_book_lg_look_map` (fk_lg_look_book_id,fk_lg_look_id) VALUES (1,5);
INSERT INTO `lg_look_book_lg_look_map` (fk_lg_look_book_id,fk_lg_look_id) VALUES (1,6);


delete from `lg_look_styles`;
INSERT INTO `lg_look_styles` VALUES (1,1,34583);
INSERT INTO `lg_look_styles` VALUES (2,1,19031);

INSERT INTO `lg_look_styles` VALUES (3,2,57038);
INSERT INTO `lg_look_styles` VALUES (4,2,45322);

INSERT INTO `lg_look_styles` VALUES (5,3,57066);
INSERT INTO `lg_look_styles` VALUES (6,3,45320);

INSERT INTO `lg_look_styles` VALUES (7,4,57171);
INSERT INTO `lg_look_styles` VALUES (8,4,52524);

INSERT INTO `lg_look_styles` VALUES (9,5,56960);
INSERT INTO `lg_look_styles` VALUES (10,5,51220);
INSERT INTO `lg_look_styles` VALUES (11,5,30707);



INSERT INTO `lg_star_n_style` (id,name,description,image,is_active,fk_lg_look_book_id,created_on) VALUES (1,'KALKI KOECHLIN','Styled by the renowned Aki Narula,Other star is sure to inspire you with her fashionista looks, her very own picks from our catalogue, and a diary featuring her trysts with fashion over the years.','http://myntra.myntassets.com/skin2/images/starnstyle_kalki_v2.jpg',1,1,2012);

INSERT INTO `lg_star_n_style_image_config` VALUES (1,"Stylemynt",1,626,516,740,626,'http://stylemynt.com/category/star-n-style-2/',1);
INSERT INTO `lg_star_n_style_image_config` VALUES (2,"Kalki's Picks",1,510,160,750,333,'/allpicks',1);
INSERT INTO `lg_star_n_style_image_config` VALUES (3,"Star Diary",1,553,661,750,844,'/style-diary-kalki-1',0);
INSERT INTO `lg_star_n_style_image_config` VALUES (4,"Kalki's Style Book",1,0,335,240,632,'/look-book/kalki-style-book-1',0);


INSERT INTO `lg_star_n_style_diary` VALUES (1,"Red Carpet Accolades for Kalki Koechlin",1,'http://myntra.myntassets.com/skin2/images/kalki_diary_2.jpg','Being a Bollywood celebrity does not necessarily mean that you comprehend fashion. Kalki Koechlin, on the other hand, knows how to pick outfits that are both stylish and that fit her requirements to a nicety. Take her red carpet avatars for instance - she has sported everything from satin gowns coupled with chunky jewellery to shorter sleeveless dresses with many colours, layers and even waistbands. Feast your eyes on the montage of images of Kalki doing what she does best on the red carpet - win hearts. Say it like Kalki - be bold, unapologetic and stay true to the self while incorporating trendy elements into your look.');
INSERT INTO `lg_star_n_style_diary` VALUES (2,"Take a leaf out of Kalki's Style Diary",1,'http://myntra.myntassets.com/skin2/images/kalki_diary_1.jpg','Surprisingly, Kalki Koechlin, the style diva who looks gorgeous in her Diors and Guccis, would prefer to wear pyjamas all the time (if only wearing them for public appearances was considered fashionable). Though a big fan of the casual look (gunjee-jeans-flat sandals), she loves wearing saris too, as she demonstrated on her visit to Myntra\'s office in a black and grey cotton sari from Dhaka. And who can forget the lovely Sabyasachi lehenga-sari she wore at Cannes 2012! From scarves and jewellery, to hats, she believes in accessorising to add that "something extra" to an ensemble. A salute to the woman who pulls off both Indian and Western ensembles with equal panache.');
