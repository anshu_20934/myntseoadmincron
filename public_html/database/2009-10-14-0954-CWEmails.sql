use myntra;

insert into mk_email_notification(name,
	subject,body) values('RROrderPlaced','Your Order has been placed','
	Dear [FIRST_NAME],<br><br>
	Your order with <font color="#ff0000"><b>Order ID #[ORDER_ID]</font></b> has been
	placed successfully. We will ship your order to the shipping
	address you have given us.<p>
	You will receive an email with the shipment tracking number once we have shipped the product. <br/><br/>
	[DELIVERY_DATE]<br/><br/></p>
	Thanks,<br>
	Myntra Customer Support<br><br>
	');



update mk_email_notification set body='

Dear [EMPLOYEE_NAME], <BR><BR>

Your account has been created in the Cable and Wireless R&R portal. <BR>
Please see below for your credentials for portal login. You are requested to change your password after first login.<BR><BR>

System URL: http://cw-galaxy.myntra.com <BR><BR>

<b>Login: [LOGIN]</b><BR><BR>

Password: [PASSWORD]<BR><BR>

Look forward to your active participation. Happy Gifting! <BR><BR>

Sincerely<BR><BR>

HR Department<BR><BR>
' where name='RRNewEmployee';


