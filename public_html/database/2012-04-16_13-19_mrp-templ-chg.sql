update mk_email_notification
set body = '
<div style="width:550px;padding-left:27px" align="LEFT">
    
    <div style="margin-top:30px;width:580px;font-family:georgia;font-size:16px;color:#000">
      Hello!<br />   It&#39;s great to see that you&#39;ve started your online shopping journey at<br /> <a href="http://www.myntra.com?utm_source=MRP_mailer&amp;utm_medium=welcome_non_fb&amp;utm_campaign=mlrwelcome-mynt-club" style="color:#000;text-decoration:none" target="_blank">Myntra.com</a>!In fact we&#39;re so excited to have you that we&#39;ve provided you,<br /> in your <a href="http://www.myntra.com/mymyntra.php?view=mymyntcredits&amp;utm_source=MRP_mailer&amp;utm_medium=welcome_non_fb&amp;utm_campaign=mlrwelcome-mynt-club" style="color:#0084ba" target="_blank">Mynt Credits account</a>,<br /><br />
      <div style="font-family:georgia;margin-top:5px;padding-top:1px;padding-left:5px;height:23px;width:500px;background-color:#cf0900;color:#fff;font-size:16px;margin-bottom:15px">[NUM_COUPONS] COUPONS OF Rs. [EACH_COUPON_VALUE] EACH VALID FOR THE NEXT [VALIDITY] DAYS,</div>
      on a minimum purchase of Rs.[MIN_PURCHASE_AMOUNT].<br /><br />So what are you waiting for? <a href="http://www.myntra.com?utm_source=MRP_mailer&amp;utm_medium=welcome_non_fb&amp;utm_campaign=mlrwelcome-mynt-club" style="color:#0084ba" target="_blank">Shop Now</a> from India&#39;s Largest Online <br />Fashion Store !<br /> <br /> Regards, <br /> Team Myntra<br /><a href="http://www.myntra.com?utm_source=MRP_mailer&amp;utm_medium=welcome_non_fb&amp;utm_campaign=mlrwelcome-mynt-club" style="color:#000;text-decoration:none" target="_blank">www.myntra.com</a> 
    </div>
</div>
'
where name='mrp_first_login';


update mk_email_notification
set body = '
<div style="width:550px;height:290px;padding-left:27px" align="LEFT">
    <div style="margin-top:30px;width:580px;font-family:georgia;font-size:16px;color:#000">
      Hello!<br />
      It&#39;s great to see that you&#39;ve started your online shopping journey at<br /> <a href="http://www.myntra.com?utm_source=MRP_mailer&amp;utm_medium=welcome_fb&amp;utm_campaign=mlrwelcome-mynt-club" style="color:#000;text-decoration:none" target="_blank">Myntra.com</a>!
      In fact we&#39;re so excited to have you that we&#39;ve provided you,<br /> in your <a href="http://www.myntra.com/mymyntra.php?view=mymyntcredits&amp;utm_source=MRP_mailer&amp;utm_medium=welcome_fb&amp;utm_campaign=mlrwelcome-mynt-club" style="color:#0084ba" target="_blank">Mynt Credits account</a>,<br /><br />
      <div style="font-family:georgia;margin-top:5px;padding-top:1px;padding-left:5px;height:23px;width:500px;background-color:#cf0900;color:#fff;font-size:16px;margin-bottom:15px">[NUM_COUPONS] COUPONS OF Rs. [EACH_COUPON_VALUE] EACH VALID FOR THE NEXT [VALIDITY] DAYS,</div>
      on a minimum purchase of Rs.[MIN_PURCHASE_AMOUNT].<br /><br />So what are you waiting for? 
      <a href="http://www.myntra.com?utm_source=MRP_mailer&amp;utm_medium=welcome_fb&amp;utm_campaign=mlrwelcome-mynt-club" style="color:#0084ba" target="_blank">Shop Now</a> from India&#39;s Largest Online <br />Fashion Store !<br /> 
      <br /> Regards, <br /> Team Myntra<br />
      <a href="http://www.myntra.com?utm_source=MRP_mailer&amp;utm_medium=welcome_fb&amp;utm_campaign=mlrwelcome-mynt-club" style="color:#000;text-decoration:none" target="_blank">www.myntra.com</a> 
    </div>
</div>
</div>
'
where name='mrp_fbfirst_login';
