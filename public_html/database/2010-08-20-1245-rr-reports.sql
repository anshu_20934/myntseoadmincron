use myntra;

/*query*/
CREATE TABLE `mk_org_reports_map` (
  `orgid` tinyint NOT NULL,
  `reportid` int NOT NULL,
  PRIMARY KEY  (`orgid`,`reportid`)  
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

insert into mk_reports (name,createddate,groupid,query,sumquery,mailto,report_type) 
values("Bharti Orders",unix_timestamp(now()),15,"select concat(xc.firstname,' ',xc.lastname) as 'customer name',tr.transaction_reference_number as orderid,
tr.transaction_reference_amount as value,tr.balance,tr.transaction_date as 'order date'
from mk_svaccount_transactions tr,mk_customer_svaccounts cs,xcart_customers xc
where tr.transaction_reference_type like '%RR-ORDER%' and xc.login=cs.customer_login and tr.account_id=cs.svaccount_id and xc.orgid=31",'','arun.kumar@myntra.com','Q');

insert into mk_org_reports_map (orgid,reportid) values (25,158);
insert into mk_org_reports_map (orgid,reportid) values (25,159);
insert into mk_org_reports_map (orgid,reportid) values (31,260);

update mk_reports set mailto='arun.kumar@myntra.com' where id in (158,159);

update mk_reports set query="select concat(xc.firstname,' ',xc.lastname) as 'customer name',tr.transaction_reference_number as orderid,
tr.transaction_reference_amount as value,tr.balance,tr.transaction_date as 'order date'
from mk_svaccount_transactions tr,mk_customer_svaccounts cs,xcart_customers xc
where tr.transaction_reference_type like '%RR-ORDER%' and xc.login=cs.customer_login and tr.account_id=cs.svaccount_id and xc.orgid=25" where id=159;