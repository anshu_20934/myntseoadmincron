DROP TABLE IF EXISTS `mk_user_roles_name`;

CREATE TABLE `mk_user_roles_name` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(31) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `mk_permission_name`;

CREATE TABLE `mk_permission_name` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `permission_name` varchar(31) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `mk_user_to_roles`;

CREATE TABLE `mk_user_to_roles` (
  `login` varchar(128) NOT NULL ,
  `rid` int(15) NOT NULL,
  PRIMARY KEY (`login`,`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

alter  table mk_user_to_roles add index `login` (login);

DROP TABLE IF EXISTS `mk_roles_to_permission`;

CREATE TABLE `mk_roles_to_permission` (
  `rid` int(15) NOT NULL ,
  `pid` int(15) NOT NULL,
  PRIMARY KEY (`rid`,`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

alter  table mk_roles_to_permission add index `rid` (rid);