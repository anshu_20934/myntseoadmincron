insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('googleTagManager.enabed','true','Fire async calls to Google Tag Manager');
insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('googleTagManager.containerId','GTM-98JK','Container id of Myntra for Google Tag Manager');
insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('tags.affiliates.migratedToGTM','false','affiliate tags migration status');
