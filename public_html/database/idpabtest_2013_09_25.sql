insert into mk_abtesting_tests (name, ga_slot, seg_algo,source_type) values ('idptest', '0', 'abtest\\algos\\impl\\RandomSegmentationAlgo', 'tpl');
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('test', (select id from mk_abtesting_tests where name ='idptest'), 0);
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('control', (select id from mk_abtesting_tests where name ='idptest'), 100);


insert into mk_abtesting_tests (name, ga_slot, seg_algo,source_type) values ('checkout_flow_java', '0', 'abtest\\algos\\impl\\RandomSegmentationAlgo', 'tpl');
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('java', (select id from mk_abtesting_tests where name ='checkout_flow_java'), 0);
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('control', (select id from mk_abtesting_tests where name ='checkout_flow_java'), 100);



