insert into mk_org_accounts (login,subdomain,created_date,activate_date,status,org_header_path,accountname,relationshipmgr,domain,org_ini_file,auth_level,strike_off_enable,landingpage,coupon_on_registration)
values('arun.kumar@myntra.com','canvasm',unix_timestamp(now()),unix_timestamp(now()),1,'../private_interface/canvasm/capgemini.jpg','canvasm','arun','.com','../modules/organization/ini/ini_canvasm.ini',2,'Y','orgIndex.php','N');

insert into mk_org_email (orgid,email_template) values ((select id from mk_org_accounts where subdomain='canvasm'),'orgmahindracanvasmregistration');

insert into mk_email_notification (name,subject,body) values('orgmahindracanvasmregistration','Your CanvasM merchandize store account is created ','Dear [USER],<br><p>Your CanvasM account has been created. You can login to your account using following details: </p><br><p>Brand Shop URL : http://canvasm.myntra.com </p><p>login: [LOGIN]</p><p>Password: [PASSWORD]</p><p>For any help or questions regarding your account or products, you can get in touch with us at support@myntra.com</p><p>Yours Sincerely<br/>Myntra Customer Service<br/>www.myntra.com<br/></p>');
