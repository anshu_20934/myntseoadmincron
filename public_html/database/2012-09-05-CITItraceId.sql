CREATE  TABLE IF NOT EXISTS `citi_trace_transactionid` (
  `traceid` INT(6) NOT NULL AUTO_INCREMENT ,
  `transactionid` varchar(32) NOT NULL ,
  PRIMARY KEY (`traceid`),
  KEY `transactionid` (`transactionid`)
)ENGINE = InnoDB;
