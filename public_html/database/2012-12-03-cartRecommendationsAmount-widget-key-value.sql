insert into mk_widget_key_value_pairs (`key`,`value`,description) values('cartRecommendationsAmount','20','Recommendations on the cart page will be shown in this range');
insert into mk_widget_key_value_pairs (`key`,`value`,description) values('cartRecommendationsHighLimit','500','Maximum value for Recommendations on the cart');

insert into mk_widget_key_value_pairs (`key`,`value`,description) values('cartRecommendationsMapping','{"Footwear": ["Socks"],"Shirts": ["Belts","Wallets","Ties"],"Tshirts": ["Deos"]}','Recommendations on the cart page will be shown based on this mapping');
