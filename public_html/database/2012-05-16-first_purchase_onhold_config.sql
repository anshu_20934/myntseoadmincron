insert into mk_widget_key_value_pairs (`key`, `value`, `description`) values ('codOH.firstpurchase.states', '', 'First Orders by customer from these states should go to on hold');

update order_oh_reasons_master set reason_desc = ' COD Verification Pending' where reason_code = 'CVP';

insert into mk_order_action_reasons (action, reason, reason_display_name, email_contents, parent_reason, is_internal_reason) values ('oh_disposition', 'FCPS', 'First COD Purchase from States', NULL, NULL, 1);

insert into mk_widget_key_value_pairs (`key`,`value`,description) values('conditionalCodOnHold.maxNoOfTrials','3','Number of trials to contact an oh customer');
insert into mk_widget_key_value_pairs (`key`,`value`,description) values('conditionalCodOnHold.CustomerRTOLimit','3','No of RTOs allowed per user to have cod option');
insert into mk_widget_key_value_pairs (`key`,`value`,description) values('conditionalCodOnHold.TrackedAndRTOLimit','2','No of RTOs allowed after tracker calling per user to have cod option');

insert into mk_widget_key_value_pairs (`key`,`value`,description) values('condCODOH.pincode','true','true or false');
insert into mk_widget_key_value_pairs (`key`,`value`,description) values('condCODOH.nocompleteorders','false','true or false');
insert into mk_widget_key_value_pairs (`key`,`value`,description) values('condCODOH.anyoflastnordersrto','true','true or false');
insert into mk_widget_key_value_pairs (`key`,`value`,description) values('condCODOH.lastnordersincomplete','true','true or false');
insert into mk_widget_key_value_pairs (`key`,`value`,description) values('condCODOH.anyoflastnordersohtrackedrto','true','true or false');
insert into mk_widget_key_value_pairs (`key`,`value`,description) values('condCODOH.trackedandrejected','true','true or false');
insert into mk_widget_key_value_pairs (`key`,`value`,description) values('condCODOH.trackedandrejected.nooforders','2','No of Orders among which one is oh tracked and rejected');
insert into mk_widget_key_value_pairs (`key`,`value`,description) values('condCODOH.anyoflastnordersrto.nooforders','2','No of Orders among which one is rto');
insert into mk_widget_key_value_pairs (`key`,`value`,description) values('condCODOH.lastnordersincomp.nooforders','2','No of Incomplete Orders');
insert into mk_widget_key_value_pairs (`key`,`value`,description) values('condCODOH.trackedandrto.nooforders','2','No of Orders among which one is tracked and rtoed');

delete from mk_feature_gate_key_value_pairs where `key` = 'conditionalCodOnHold.maxNoOfTrials';
delete from mk_feature_gate_key_value_pairs where `key` = 'conditionalCodOnHold.CustomerRTOLimit';
delete from mk_feature_gate_key_value_pairs where `key` = 'conditionalCodOnHold.TrackedAndRTOLimit';
delete from mk_feature_gate_key_value_pairs where `key` = 'condCODOH.pincode';
delete from mk_feature_gate_key_value_pairs where `key` = 'condCODOH.nocompleteorders';
delete from mk_feature_gate_key_value_pairs where `key` = 'condCODOH.anyoflastnordersrto';
delete from mk_feature_gate_key_value_pairs where `key` = 'condCODOH.lastnordersincomplete';
delete from mk_feature_gate_key_value_pairs where `key` = 'condCODOH.anyoflastnordersohtrackedrto';
delete from mk_feature_gate_key_value_pairs where `key` = 'condCODOH.trackedandrejected';
delete from mk_feature_gate_key_value_pairs where `key` = 'condCODOH.trackedandrejected.nooforders';
delete from mk_feature_gate_key_value_pairs where `key` = 'condCODOH.anyoflastnordersrto.nooforders';
delete from mk_feature_gate_key_value_pairs where `key` = 'condCODOH.lastnordersincomp.nooforders';
delete from mk_feature_gate_key_value_pairs where `key` = 'condCODOH.trackedandrto.nooforders';
