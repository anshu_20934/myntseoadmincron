ALTER TABLE `mk_courier_service`  ADD COLUMN `color_code` VARCHAR(7) NULL AFTER `return_supported`;

update mk_courier_service set color_code='#800000' where code='DD';
update mk_courier_service set color_code='#2ECCFA' where code='BD';
update mk_courier_service set color_code='#ff0000' where code='FF';
update mk_courier_service set color_code='#40FF00' where code='ML';
update mk_courier_service set color_code='#ffA500' where code='RB';
update mk_courier_service set color_code='#808000' where code='AR';
update mk_courier_service set color_code='#008000' where code='NK';
update mk_courier_service set color_code='#800080' where code='UP';
update mk_courier_service set color_code='#ff00ff' where code='VK';
update mk_courier_service set color_code='#008080' where code='SP';
update mk_courier_service set color_code='#FFFF00' where code='DH';
update mk_courier_service set color_code='#00ffff' where code='QS';
update mk_courier_service set color_code='#0000ff' where code='FX';


UPDATE `mk_courier_service` 
SET `reports_template`='orderid as orderId, title as title, s_firstname as firstName, s_lastname as lastName, s_address as address, s_city as city, s_state as state, s_country as country, s_zipcode as zipcode, mobile as mobile, s_email as email, if(payment_method = \'cod\', \'true\',\'false\') as cod, if(payment_method = \'cod\',(total + shipping_cost + gift_charges + cod),null) as codAmount, b_mobile as custMobile' 
WHERE `code`='ML' LIMIT 1;

