
/*index on completeddate for xcart_orders */
CREATE INDEX `completed_date_index` USING BTREE ON `xcart_orders` (`completeddate`);