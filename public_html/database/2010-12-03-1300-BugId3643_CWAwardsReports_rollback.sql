update mk_reports set 
query = "select concat(c1.firstname,' ',c1.lastname) as receiving_colleague,concat(c2.firstname,' ',c2.lastname) as gifting_colleague,

t2.description,tr.credit,tr.balance, tr.transaction_info as memo, tr.transaction_date as date 

from mk_svaccount_transactions tr, mk_customer_svaccounts cs1,mk_customer_svaccounts cs2, 

xcart_customers c1,xcart_customers c2,mk_svaccounts a2,mk_svaccount_types t2

where tr.transaction_reference_type like 'RR-ACCOUNT' and tr.account_id!=88 and tr.transaction_reference_number!=88 

and cs1.svaccount_id=tr.account_id and cs2.svaccount_id=tr.transaction_reference_number 

and c1.login=cs1.customer_login and c2.login=cs2.customer_login and tr.debit=0 and 

a2.account_id=cs2.svaccount_id and t2.type_code=a2.account_type

order by tr.transaction_date desc"
where name = 'CW Awards';