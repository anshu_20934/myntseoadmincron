delete from mk_email_notification where name in ('order_cancel_ccr', 'order_cancel_ccc');

insert into mk_email_notification(name, subject, body) values('order_cancel_ccr', 'Cancelled Order No. [ORDER_ID]',
'Hello [USER], <br/><br/>
Thank you for contacting Myntra Support. As per your request, we are cancelling your Order No. [ORDER_ID]. Please find below details of the same:<br/><br/>
[ORDER_DETAILS]
<br/><br/>
<p>The refund amount of Rs [REFUND_AMOUNT] has been credited to your Myntra Club cashback account and you can view the same by <a href="[URL]">clicking here</a>.</p>
[COUPON_USAGE_TEXT]
<p>We want you to know we are honoured to have you as our esteemed customer and we will do our best to meet your needs. Your continued patronage and suggestions are a vital part of our growth, and for that we are most grateful to you.</p>
<p>For any clarification, kindly contact us at [MYNTRA_CC_PHONE].</p>
<br/>
Regards,<br/>
Team Myntra'
);

insert into mk_email_notification(name, subject, body) values('order_cancel_ccc', 'Cancelled Order No. [ORDER_ID]',
'Hello [USER], <br/><br/>
We regret to inform you that we are unable to service your order and would have to cancel it due to an overwhelming response, which has exhausted our stocks. Please find the details of your Order No. [ORDER_ID] below:<br/><br/>
[ORDER_DETAILS]
<br/><br/>
<p>The refund amount of Rs [REFUND_AMOUNT] has been credited to your Myntra Club cashback account and you can view the same by <a href="[URL]">clicking here</a>.</p>
[COUPON_USAGE_TEXT]
<p>We want you to know we are honoured to have you as our esteemed customer and we will do our best to meet your needs. Your continued patronage and suggestions are a vital part of our growth, and for that we are most grateful to you.</p>
<p>For any clarification, kindly contact us at [MYNTRA_CC_PHONE].</p>
<br/>
Regards,<br/>
Team Myntra'
);

