alter table `mk_style_properties` add column `global_attr_article_type` varchar (255)   NULL , add column `global_attr_sub_category` varchar (255)   NULL  after `global_attr_article_type`, add column `global_attr_master_category` varchar (255)   NULL  after `global_attr_sub_category`, add column `global_attr_brand` varchar (255)   NULL  after `global_attr_master_category`, add column `global_attr_age_group` varchar (255)   NULL  after `global_attr_brand`, add column `global_attr_gender` varchar (255)   NULL  after `global_attr_age_group`, add column `global_attr_base_colour` varchar (255)   NULL  after `global_attr_gender`, add column `global_attr_colour1` varchar (255)   NULL  after `global_attr_base_colour`, add column `global_attr_colour2` varchar (255)   NULL  after `global_attr_colour1`, add column `global_attr_fashion_type` varchar (255)   NULL  after `global_attr_colour2`, add column `global_attr_season` varchar (255)   NULL  after `global_attr_fashion_type`, add column `global_attr_year` varchar (255)   NULL  after `global_attr_season`, add column `global_attr_usage` varchar (255)   NULL  after `global_attr_year`;
alter table `mk_style_properties` add column `global_attr_catalog_add_date` int (11)  NULL ;
CREATE TABLE `mk_catalogue_classification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typename` varchar(255) DEFAULT NULL,
  `typecode` varchar(55) DEFAULT NULL,
  `parent1` int(11) DEFAULT NULL,
  `parent2` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
insert into `mk_catalogue_classification` (`id`,`typename`,`typecode`,`parent1`,`parent2`,`is_active`) values (1,'Accessories','',-1,-1,1);
insert into `mk_catalogue_classification` (`id`,`typename`,`typecode`,`parent1`,`parent2`,`is_active`) values (2,'Bags',NULL,1,-1,1);
insert into `mk_catalogue_classification` (`id`,`typename`,`typecode`,`parent1`,`parent2`,`is_active`) values (3,'Bagpack','BAPK',2,1,1);
insert into `mk_catalogue_classification` (`id`,`typename`,`typecode`,`parent1`,`parent2`,`is_active`) values (4,'Bags','BAGS',2,1,1);
insert into `mk_catalogue_classification` (`id`,`typename`,`typecode`,`parent1`,`parent2`,`is_active`) values (5,'Clutch','CLTH',2,1,1);

CREATE TABLE `mk_attribute_type_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_type` varchar(255) DEFAULT NULL,
  `attribute_value` varchar(255) DEFAULT NULL,
  `applicable_set` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

insert into `mk_attribute_type_values` (`id`,`attribute_type`,`attribute_value`,`applicable_set`,`is_active`) values (1,'Brand','Reebok',NULL,1);
insert into `mk_attribute_type_values` (`id`,`attribute_type`,`attribute_value`,`applicable_set`,`is_active`) values (2,'Brand','Puma',NULL,1);
insert into `mk_attribute_type_values` (`id`,`attribute_type`,`attribute_value`,`applicable_set`,`is_active`) values (3,'Age Group','Adult-Men',NULL,1);
insert into `mk_attribute_type_values` (`id`,`attribute_type`,`attribute_value`,`applicable_set`,`is_active`) values (4,'Age Group','Adult-Women',NULL,1);
insert into `mk_attribute_type_values` (`id`,`attribute_type`,`attribute_value`,`applicable_set`,`is_active`) values (5,'Gender','Men',NULL,1);
insert into `mk_attribute_type_values` (`id`,`attribute_type`,`attribute_value`,`applicable_set`,`is_active`) values (6,'Gender','Women',NULL,1);
insert into `mk_attribute_type_values` (`id`,`attribute_type`,`attribute_value`,`applicable_set`,`is_active`) values (7,'Colour','Red',NULL,1);
insert into `mk_attribute_type_values` (`id`,`attribute_type`,`attribute_value`,`applicable_set`,`is_active`) values (8,'Colour','Blue',NULL,1);
insert into `mk_attribute_type_values` (`id`,`attribute_type`,`attribute_value`,`applicable_set`,`is_active`) values (9,'Colour','Green',NULL,1);
insert into `mk_attribute_type_values` (`id`,`attribute_type`,`attribute_value`,`applicable_set`,`is_active`) values (14,'Colour','Black',NULL,1);
insert into `mk_attribute_type_values` (`id`,`attribute_type`,`attribute_value`,`applicable_set`,`is_active`) values (15,'Fashion Type','Core',NULL,NULL);


CREATE TABLE `mk_attribute_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_type` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
insert into `mk_attribute_type` (`id`,`attribute_type`,`is_active`) values (1,'Brand',1);
insert into `mk_attribute_type` (`id`,`attribute_type`,`is_active`) values (2,'Age Group',1);
insert into `mk_attribute_type` (`id`,`attribute_type`,`is_active`) values (3,'Gender',1);
insert into `mk_attribute_type` (`id`,`attribute_type`,`is_active`) values (4,'Colour',1);
insert into `mk_attribute_type` (`id`,`attribute_type`,`is_active`) values (7,'Fashion Type',1);
insert into `mk_attribute_type` (`id`,`attribute_type`,`is_active`) values (8,'Season',1);
insert into `mk_attribute_type` (`id`,`attribute_type`,`is_active`) values (9,'Year',1);
insert into `mk_attribute_type` (`id`,`attribute_type`,`is_active`) values (10,'Usage',1);
