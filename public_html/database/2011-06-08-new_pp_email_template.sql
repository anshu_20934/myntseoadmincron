delete from mk_email_notification where name = 'pre_purchase';

insert into mk_email_notification(name, subject, body) values('pre_purchase', 'Your Incomplete Order at Myntra.com',
'Dear [USER], <br/>
<p>We noticed that your Order No [ORDER_ID] did not go through successfully. We regret the inconvenience caused and would like to understand the problem faced by you during this incidence.</p>
<p> You can <a href="[URL]">Click this link</a> to try again and complete your transaction.</p>
<p>Our Customer Champions are available 24 hours all 7 days of the week and will be glad to help you with completing this order or answer any other questions that you may have. You can reach our Customer Champion at [MYNTRA_CC_PHONE] or support@myntra.com</p>
<p>We thank you for trying out Myntra.com and assure you that we will do everything possible to ensure you have good experience.</p>
Regards,<br/>
Myntra Customer Care
');
