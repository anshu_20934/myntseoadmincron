ALTER TABLE `xcart_discount_coupons`
  DROP COLUMN `showInMyMyntra`,
  DROP COLUMN `description`,
  DROP COLUMN `lastEdited`,
  DROP COLUMN `lastEditedBy`;

ALTER TABLE `mk_coupon_group`
  DROP COLUMN `lastEdited`,
  DROP COLUMN `lastEditedBy`
  DROP COLUMN `active_count`;
