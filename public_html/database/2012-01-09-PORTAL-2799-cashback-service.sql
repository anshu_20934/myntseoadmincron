
-- --------------------------------------------------------

--
-- Table structure for table `cashback_account`
--

CREATE TABLE IF NOT EXISTS `cashback_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(128) NOT NULL,
  `account_type` smallint(2) NOT NULL,
  `balance` float NOT NULL,
  `cashback_coupon` varchar(50) DEFAULT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `login` (`login`,`account_type`),
  KEY `account_type` (`account_type`),
  KEY `login_2` (`login`)
)  ;


--
-- Table structure for table `cashback_transaction_log`
--

CREATE TABLE IF NOT EXISTS `cashback_transaction_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cashback_account_id` int(11) NOT NULL,
  `item_type` varchar(20) NOT NULL,
  `item_id` int(20) DEFAULT NULL,
  `business_process` varchar(20) NOT NULL,
  `credit_inflow` decimal(12,2) DEFAULT NULL,
  `credit_outflow` decimal(12,2) DEFAULT NULL,
  `balance` decimal(12,2) NOT NULL DEFAULT '0.00',
  `modified_by` varchar(128) NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `descripion` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `business_process` (`business_process`),
  KEY `coupon_account_id` (`cashback_account_id`),
  KEY `cashback_account_id` (`cashback_account_id`)
);




--
-- Database: `myntra`
--

-- --------------------------------------------------------

--
-- Table structure for table `cashback_business_process_lookup`
--

CREATE TABLE IF NOT EXISTS `cashback_business_process_lookup` (
  `business_process` varchar(30) NOT NULL,
  `Code` varchar(20) NOT NULL,
  `transaction_type` varchar(10) NOT NULL,
  `Bucket` varchar(20) NOT NULL,
  `Action` text NOT NULL,
  KEY `Code` (`Code`)
);




--
-- Table structure for table `cashback_account_type`
--

CREATE TABLE IF NOT EXISTS `cashback_account_type` (
  `id` smallint(2) NOT NULL,
  `name` varchar(40) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
);


ALTER TABLE  `xcart_order_details` ADD  `store_credit_usage` DECIMAL( 12, 2 ) NOT NULL AFTER  `cash_redeemed` ,
ADD  `earned_credit_usage` DECIMAL( 12, 2 ) NOT NULL AFTER  `store_credit_usage`;

ALTER TABLE  `xcart_orders` ADD  `store_credit_usage` DECIMAL( 12, 2 ) NOT NULL AFTER  `cash_redeemed` ,
ADD  `earned_credit_usage` DECIMAL( 12, 2 ) NOT NULL AFTER  `store_credit_usage`;

ALTER TABLE  ` gift_cards_orders` ADD  `store_credit_usage` DECIMAL( 12, 2 ) NOT NULL AFTER  `cash_redeemed` ,
ADD  `earned_credit_usage` DECIMAL( 12, 2 ) NOT NULL AFTER  `store_credit_usage`;
