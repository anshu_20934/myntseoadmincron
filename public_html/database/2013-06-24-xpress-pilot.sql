CREATE TABLE xpress_capacity (
  id int(5) NOT NULL AUTO_INCREMENT,
  zipcode int(5) NOT NULL,
  record_date varchar(100) NOT NULL,
  no_of_orders int(11),
  start_hour int(5), 
  end_hour int(5),
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `order_additional_info` (
  `id` INT(20) NOT NULL AUTO_INCREMENT,
  `order_id_fk` INT(20) NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_on` datetime DEFAUlT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oai_unq_key` (`order_id_fk`,`key`),
  KEY `order_id_key` (`order_id_fk`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;


--xpressshipping.capacities widget
--xpressshipping.hourly.percentage   widget
--xpressshipping.enabled    key
--priorityshipping.enabled    key
--xpressshipping.pickingtime  key
--xpressshipping.qctime     key

insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('priorityshipping.enabled','false','feature gate for priority orders');


