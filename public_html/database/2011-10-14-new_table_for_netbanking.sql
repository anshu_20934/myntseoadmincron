drop table if EXISTS mk_netbanking_banks;
CREATE TABLE `mk_netbanking_banks` (
`id` int(31) NOT NULL AUTO_INCREMENT,
`name` varchar(255) NOT NULL,
`is_popular` int(1) NOT NULL default 0,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

drop table if EXISTS mk_netbanking_gateways;
CREATE TABLE `mk_netbanking_gateways` (
`id` int(31) NOT NULL AUTO_INCREMENT,
`name` varchar(127) NOT NULL,
`weight` int(3) NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

drop table if EXISTS mk_netbanking_mapping;
CREATE TABLE `mk_netbanking_mapping` (
`id` int(31) NOT NULL AUTO_INCREMENT,
`bank_id` int(31) NOT NULL,
`gateway_id` int(31) NOT NULL,
`gateway_code` varchar(31) NOT NULL,
`is_active` int(1) NOT NULL default 0,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

alter table mk_netbanking_mapping add index `bank_id` (bank_id);

drop table if EXISTS mk_netbanking_logs;
CREATE TABLE `mk_netbanking_logs` (
`id` int(31) NOT NULL AUTO_INCREMENT,
`login` VARCHAR(128) NOT NULL,
`time_modify` int(11) NOT NULL,
`action` VARCHAR(31) NOT NULL,
`bank_modified` VARCHAR(255) DEFAULT NULL,
`gateway_modified` VARCHAR(127) DEFAULT NULL,
`extra_info` VARCHAR(255) DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

alter table mk_netbanking_logs add index `login` (login);
alter table mk_netbanking_logs add index `time_modify` (time_modify);

insert into mk_netbanking_banks (`id`,`name`,`is_popular`) values (1,'Axis Bank',1),(2,'HDFC Bank',1),(3,'ICICI Bank',1),
															 (4,'State Bank of India',1),(5,'CitiBank',1),(6,'Andhra Bank',0),
															 (7,'Bank of Bahrain & Kuwait',0),(8,'Bank of Baroda Corporate Accounts',0),(9,'Bank of Baroda Retail Accounts',0),
															 (10,'Bank of India',0),(11,'Bank of Maharashtra',0),(12,'City Union Bank',0),
															 (13,'Corporation Bank',0),(14,'Deutsche Bank',0),(15,'Federal Bank',0),
															 (16,'IDBI Bank',0),(17,'Indian Overseas Bank',0),(18,'IndusInd Bank',0),
															 (19,'ING Vysya Bank',0),(20,'Jammu & Kashmir Bank',0),(21,'Karnataka Bank',0),
															 (22,'Karur Vysya Bank',0),(23,'Kotak Mahindra Bank',0),(24,'Lakshmi Vilas Bank NetBanking',0),
															 (25,'Oriental Bank of Commerce',0),(26,'Punjab National Bank Corporate Accounts',0),(27,'Punjab National Bank Retail Accounts',0),
															 (28,'South Indian Bank',0),(29,'Standard Chartered Bank',0),(30,'State Bank of Hyderabad',0),
															 (31,'State Bank of Mysore',0),(32,'State Bank of Travancore',0),(33,'Syndicate Bank',0),
															 (34,'Tamilnad Mercantile Bank',0),(35,'Union Bank of India',0),(36,'United Bank of India',0),
															 (37,'Vijaya Bank',0),(38,'YES Bank',0);
															 
															 
insert into mk_netbanking_gateways (`id`,`name`,`weight`) values (1,'CCAvenue',50),(2,'EBS',30),(3,'Atom',20);

insert into mk_netbanking_mapping (`bank_id`,`gateway_id`,`gateway_code`,`is_active`) values (1,1,'UTI_N',1),(2,1,'HDEB_N',1),(3,1,'IDEB_N',1),
																						(4,1,'SBI_N',1),(5,1,'CBIBAN_N',1),(6,1,'AND_N',1),
																						(7,1,'BBK_N',1),(8,1,'BOBCO_N',1),(9,1,'BOB_N',1),
																						(10,1,'BOI_N',1),(11,1,'BOM_N',1),(12,1,'CITIUB_N',1),
																						(13,1,'COP_N',1),(14,1,'DEUNB_N',1),(15,1,'FDEB_N',1),
																						(16,1,'IDBI_N',1),(17,1,'IOB_N',1),(18,1,'NIIB_N',1),
																						(19,1,'ING_N',1),(20,1,'JKB_N',1),(21,1,'KTKB_N',1),
																						(22,1,'KVB_N',1),(23,1,'NKMB_N',1),(24,1,'LVB_N',1),
																						(25,1,'OBC_N',1),(26,1,'PNBCO_N',1),(27,1,'NPNB_N',1),
																						(28,1,'SIB_N',1),(29,1,'SCB_N',1),(30,1,'SBH_N',1),
																						(31,1,'SBM_N',1),(32,1,'SBT_N',1),(33,1,'SYNBK_N',1),
																						(34,1,'TNMB_N',1),(35,1,'UNI_N',1),(36,1,'UBI_N',1),
																						(37,1,'VJYA_N',1),(38,1,'YES_N',1),
																						(1,2,'1004',1), (2,2,'1007',1),(3,2,'1016',1),
																						(4,2,'1032',1), (5,2,'1127',1),(15,2,'1029',1),
																						(20,2,'1015',1),(30,2,'1034',1);
																						
insert into mk_roles (`name`,`type`,`description`) values ('Payments', 'PA', 'This role allows you to control payment gateways');