ALTER TABLE xcart_returns add column warehouseid int(5) not null, add index warehouseid_idx (warehouseid);

update xcart_returns set warehouseid = 1;
