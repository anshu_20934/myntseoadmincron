update mk_email_notification set subject = 'We need to verify important details about your Myntra.com order' where name = 'cod_manual_verification';

update mk_email_notification set body = '<div style="border: 1px solid gray; padding: 6px">Hi [USER], <br/>
<p>Thank you for placing an order on Myntra.com. Before we can process your order, we need to verify a few important details about your order. We will call you on your registered phone number +91 [USER_PHONE_NO]. This call will take less than five minutes and is necessary before we can ship your order. If you prefer, you can call us at your convenience at [MYNTRA_CC_PHONE].</p>
<p>Order ID: [ORDER_ID]</p>
[ORDER_DETAILS]<br/><br/>
Shipped To: <br><br>
[DELIVERY_ADDRESS]
<p>We look forward to talking with you soon</p>
Thanks,<br>
Myntra.com Team<br>
India\'s Largest Online Fashion Store<br><br>[DECLARATION]</font><br>
</div>' where name = 'cod_manual_verification';

insert into mk_email_notification (name, subject, body) values ('cod_manual_verified', 'Your Myntra.com order is confirmed',
'<div style="border: 1px solid gray; padding: 6px">
Hi [USER],<br/>
<p>Thank you for verifying details of your Myntra.com order. We will deliver your order by [DELIVERY_DATE]. Once your order is shipped, we will send you an email with the courier tracking information.</p>
<p>Order ID: [ORDER_ID]</p>
[ORDER_DETAILS]<br/><br/>
Shipped To: <br><br>
[DELIVERY_ADDRESS]
<p>You can track or manage your order at any time by going to [MYORDERS_URL]. Please feel free to contact us at [CONTACTUS_URL] if you have any questions or need further assistance.</p>
<p>We hope you enjoyed shopping at Myntra.com</p>
Thanks,<br>
Myntra.com Team<br>
India\'s Largest Online Fashion Store<br><br>[DECLARATION]</font><br>
</div>');

insert into mk_email_notification (name, subject, body) values ('cod_attempt_failure', 'We need to verify important details about your Myntra.com order', '<div style="border: 1px solid gray; padding: 6px">Hi [USER], <br/>
<p>Thank you for placing an order on Myntra.com. Before we can process your order, we need to verify a few important details about your order. We have tried calling you on your registered phone number +91 [USER_PHONE_NO] but are unable to reach you. We will call you again by the end of today or tomorrow morning to verify your order. This call will take less than five minutes and is necessary before we can ship your order. If you prefer, you can call us at your convenience at [MYNTRA_CC_PHONE].</p>
<p>Order ID: [ORDER_ID]</p>
[ORDER_DETAILS]<br/><br/>
Shipped To: <br><br>
[DELIVERY_ADDRESS]
<p>We look forward to talking with you soon</p>
Thanks,<br>
Myntra.com Team<br>
India\'s Largest Online Fashion Store<br><br>[DECLARATION]</font><br>
</div>');

