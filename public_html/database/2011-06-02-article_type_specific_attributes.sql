
DROP TABLE IF EXISTS mk_style_article_type_attribute_values;
CREATE  TABLE mk_style_article_type_attribute_values (
  id INT(11) NOT NULL AUTO_INCREMENT ,
  product_style_id INT(11) NOT NULL,
  article_type_attribute_value_id INT(11) NOT NULL ,
  created_on INT(11) NULL ,
  created_by VARCHAR(128) NULL ,
  updated_on INT(11) NULL ,
  updated_by VARCHAR(128) NULL ,
  is_active TINYINT(1) NULL ,
  PRIMARY KEY (id)
) ;


ALTER TABLE mk_attribute_type 
	ADD COLUMN catalogue_classification_id INT(11) NULL DEFAULT NULL  AFTER id , 
	ADD COLUMN display_order INT(2) NULL  AFTER is_active , 
	ADD COLUMN created_on INT(11) NULL  AFTER display_order , 
	ADD COLUMN created_by VARCHAR(128)  AFTER created_on , 
	ADD COLUMN updated_on INT(11) NULL  AFTER created_by , 
	ADD COLUMN updated_by VARCHAR(128) NULL  AFTER updated_on;

ALTER TABLE mk_attribute_type_values 
	ADD COLUMN attribute_type_id INT(11) NULL  AFTER attribute_code , 
	ADD COLUMN created_on INT(11) NULL  AFTER attribute_code , 
	ADD COLUMN created_by VARCHAR(128) NULL  AFTER created_on , 
	ADD COLUMN updated_on INT(11) NULL  AFTER created_by , 
	ADD COLUMN updated_by VARCHAR(128) NULL  AFTER updated_on;

update mk_attribute_type_values set attribute_type_id=(select id from mk_attribute_type where mk_attribute_type.attribute_type=mk_attribute_type_values.attribute_type);

ALTER TABLE mk_style_properties 
	ADD COLUMN article_specific_attributes VARCHAR(255) NULL  AFTER global_attr_catalog_add_date , 
	ADD COLUMN display_categories VARCHAR(255) NULL  AFTER article_specific_attributes ;
