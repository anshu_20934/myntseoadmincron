CREATE TABLE `mk_sr_tracker` (
  `SR_id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `sub_category_id` int(10) unsigned NOT NULL,
  `priority` tinyint(3) unsigned NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `created_date` int(11) DEFAULT NULL,
  `due_date` int(11) DEFAULT NULL,
  `closed_date` int(11) DEFAULT NULL,
  `TAT` int(11) DEFAULT NULL,
  `reporter` varchar(100) NOT NULL,
  `assignee` varchar(100) DEFAULT NULL,
  `resolver` varchar(100) DEFAULT NULL,
  `copyto` text,
  `create_summary` text NOT NULL,
  `close_summary` text,
  `notification_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`SR_id`),
  KEY `category_id` (`category_id`),
  KEY `sub_category_id` (`sub_category_id`),
  KEY `priority` (`priority`),
  KEY `status` (`status`),
  KEY `created_date` (`created_date`),
  KEY `due_date` (`due_date`),
  KEY `closed_date` (`closed_date`),
  KEY `reporter` (`reporter`),
  KEY `assignee` (`assignee`),
  KEY `resolver` (`resolver`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='internal service request tracker';


CREATE TABLE `mk_sr_comments` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `SR_id` int(11) NOT NULL,
  `comment` text,
  `comment_by` varchar(100) DEFAULT NULL,
  `comment_date` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `SR_id` (`SR_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='add all comments for SR tickets';


CREATE TABLE `mk_sr_status` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `SR_status` varchar(30) NOT NULL,
  `status_description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `SR_status_UNIQUE` (`SR_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='service request status';


CREATE TABLE `mk_sr_priority` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `SR_priority` varchar(5) NOT NULL,
  `priority_description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `SR_priority_UNIQUE` (`SR_priority`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='service request priorities';


CREATE TABLE `mk_sr_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SR_category` varchar(100) NOT NULL,
  `category_description` text,
  `owners` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `SR_category_UNIQUE` (`SR_category`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='service request categories';


CREATE TABLE `mk_sr_subcategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SR_category_id` int(11) NOT NULL,
  `SR_subcategory` varchar(200) NOT NULL,
  `subcategory_description` text,
  `owners` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `SR_subcategory_UNIQUE` (`SR_subcategory`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='service request sub-categories';



CREATE TABLE `mk_order_sr` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `SR_id` int(11) zerofill NOT NULL,
  `order_id` int(11) NOT NULL,
  `items` text,
  `customer_email` varchar(100) DEFAULT NULL,
  `compensation` enum('Y','N') NOT NULL DEFAULT 'N',
  `compensation_amount` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='service requests related to orders';



CREATE TABLE `mk_non_order_sr` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `SR_id` int(11) zerofill NOT NULL,
  `customer_email` varchar(100) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_email` (`customer_email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='service requests which are not related to orders';


alter table `mk_sr_categories` add column SLA_in_sec int(11) unsigned default null;
alter table `mk_sr_subcategories` add column SLA_in_sec int(11) unsigned default null;
alter table mk_sr_categories drop index SR_category_UNIQUE;
alter table mk_sr_categories add column category_type varchar(30) default null;


insert into mk_sr_status (SR_status,status_description) values ('open','opens an issue');
insert into mk_sr_status (SR_status,status_description) values ('close','closes an issue');


insert into mk_sr_priority (SR_priority,priority_description) values ('P0','P0 issue');
insert into mk_sr_priority (SR_priority,priority_description) values ('P1','P1 issue');
insert into mk_sr_priority (SR_priority,priority_description) values ('P2','P2 issue');
insert into mk_sr_priority (SR_priority,priority_description) values ('P3','P3 issue');
insert into mk_sr_priority (SR_priority,priority_description) values ('P4','P4 issue');
insert into mk_sr_priority (SR_priority,priority_description) values ('P5','P5 issue');


insert into mk_sr_categories (SR_category,category_description,category_type) values('Delivery','','order');
insert into mk_sr_categories (SR_category,category_description,category_type) values('Return','','order');
insert into mk_sr_categories (SR_category,category_description,category_type) values('Payment SR','','order');
insert into mk_sr_categories (SR_category,category_description,category_type) values('Coupon','','non-order');
insert into mk_sr_categories (SR_category,category_description,category_type) values('Product Information','','non-order');
insert into mk_sr_categories (SR_category,category_description,category_type) values('Customer Information','','non-order');
insert into mk_sr_categories (SR_category,category_description,category_type) values('VOC','','non-order');

insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(1,'Delayed delivery','Customer has not recevied delivery after delivery assurance date, Multiple issue like invalid tracking number, no tracking number etc also needs to be registered here');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(1,'Delivery VOC','Complaint regarding delivery boy issue or any other issue during delivery which impacts customer  experience . Example CM called to collect product, cm asked for extra money, change issue');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(1,'Delivery - Payment Issue','customer asked for amount more then the bill or delivery boy did not collect money for COD order');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(1,'Missing component','Main product missing during delivery. Do not add partial orders under this category. Only if you observe that the product is shipped but not recevied it will be raised as an SR for OOS or JIT inform customer and cancel no need to raise SR');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(1,'Free Gift Missing','Free gift not recevied by Customer');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(1,'Packaging issue','Product not recevied in correct condition or issue with packaging');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(1,'Pin code issue','If a pin code is not available for delivery or not available for COD pin code delivery');

insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(2,'Wrong Delivery - Different Product','Customer wants to return the product as wrong item receviued');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(2,'Wrong Delivery - Wrong SKU','Customer wants to return the product as wrong SKU receviued');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(2,'Defective Product','Customer wants to return the product as it is defective');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(2,'Damaged goods delivery','Customer wants to return the product as it is damaged');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(2,'Inferior products','Customer wants to return as not happy with product quality');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(2,'Wrong size ','Customer wants to return the product as size incorrect');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(2,'Recollection pick up delay/failed','Customer has called to escalate pick up did not happen');

insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(3,'Payment processed but order not queued','Customer has not rcvd the order and on we find the payment is processed but order not queued due to payment capture issue');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(3,'Amount debited but transaction failed','Customer payment is debited but transaction incomplete');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(3,'Amount debited twice','Customer claims amount is debited twice');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(3,'Return refund not processed','Customer claims refund is not processed');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(3,'OOS refund not processed','Customer claims refund is not for OOS/JIT processed');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(3,'Partial refund not processed','Customer claims refund is not for OOS/JIT PARTIAL order');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(3,'Refunded credited but not recevied by customer','Customer calls to complaint refund not recevied even though he has confirmation number');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(3,'Other refund pending','Miscllns - Coupon ');

insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(4,'Coupon not working','CM calls to claim coupon not working');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(4,'coupon not recevied','CM calls to claim coupon is not recevied');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(4,'Coupon validity  issue','CM has issue with validity of coupon');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(4,'Mynt coupon issue','CM says Mynt credit not recevied');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(4,'Coupon request Goodwill','CM wants a coupon without reason');

insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(5,'Product Components, Characteristics','Customer wants info about product not listed on site');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(5,'Warranty details','Customer wants to know about warranty ');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(5,'Usage guidance','Customer wants to know how to use product');

insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(6,'Fake Customer','Fake customer monitored on admin');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(6,'Chargeback Request - Fraud Alert','Fraud alert recevied from bank');

insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(7,'Complaint against SC/Myntra employee','');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(7,'Complaint against Myntra process','');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(7,'Complaint about product packaging','');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(7,'Complaint about promotional scheme','');
insert into mk_sr_subcategories (SR_category_id,SR_subcategory,subcategory_description) values(7,'Complaint about website','');

