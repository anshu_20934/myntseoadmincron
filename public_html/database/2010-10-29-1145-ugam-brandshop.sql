alter table mk_org_accounts add column landingpage varchar(250) null;

update mk_org_accounts set landingpage='orghomeproducts.php' where subdomain='defiance';

insert into mk_org_accounts (login,subdomain,created_date,activate_date,status,org_header_path,accountname,relationshipmgr,domain,org_ini_file,org_css_file,brand_shop_dir,auth_level,strike_off_enable,landingpage)
values('arun.kumar@myntra.com','ugam',unix_timestamp(now()),unix_timestamp(now()),1,'../private_interface/ugam/ugam.jpg','ugam','arun','.com','../modules/organization/ini/ini_ugam.ini','../skin1/modules/organization/css/css_ugam.css','/defiance-store-template',2,'Y','orghomeproducts.php');

insert into mk_org_email (orgid,email_template) values ((select id from mk_org_accounts where subdomain='ugam'),'orgugamregistration');

insert into mk_email_notification (name,subject,body) values('orgugamregistration','Your Ugam merchandize store account is created ','Dear [USER],<br><p>Your ugam Brand Shop account has been created. You can login to your account using following details: </p><br><p>Brand Shop URL : www.myntra.com/account/ugam/35 </p><p>login: [LOGIN]</p><p>Password: [PASSWORD]</p><p>At this brand store, you can purchase merchandize like t-shirts, mugs, caps, key-chains and many other products at discounted price exclusively for employees of ugam. You can also personalize these products with your photographs, your company logo or name or message. You can also order merchandize in bulk for your team for different events. For such requirements you can get in touch with us at sales@myntra.com and we will do our best to work out right solution for you at pre-approved prices for ugam.</p><p>For any help or questions regarding your account or products, you can get in touch with us ugam-support@myntra.com</p><p>Yours Sincerely<br/>Myntra Customer Service<br/>www.myntra.com<br/></p>');
