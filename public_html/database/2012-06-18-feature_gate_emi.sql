update mk_feature_gate_key_value_pairs set value="credit_card, emi, debit_card, net_banking, cod" where `key`="PaymentOptionsOrder";

insert into mk_feature_gate_key_value_pairs(`key`,`value`,description) values('payments.iciciEMIBIN','402368,407651,407659,420580,437551,444341,447746,447747,447758,462986,470573,517637,517653,517719,523951,524193,524376,525996,540282,545207,547467,471861,407439,418212,471863,471860,436560,411146,524178,414767,416644,436389,416643,436388,416645,436390,416646,543705,402874,412903,412904,412905,419607,429344,454198,456398,457036,462270,462271,462272,462273,466269,466271,523988,523990,540460,540461,540711,543186,544438,547359,549124,549132,553160,554374,554375,554378,554623','EMI Bins for ICICI Gateway');

insert into mk_feature_gate_key_value_pairs(`key`,`value`,description) values('payments.icici6MONTHEMICharge','500','EMI Charge for ICICI 6 months emi');

insert into mk_feature_gate_key_value_pairs(`key`,`value`,description) values('payments.icici3MONTHEMICharge','0','EMI Charge for ICICI 3 months emi');

insert into mk_feature_gate_key_value_pairs(`key`,`value`,description) values('payments.emiBank','Axis Bank, ICICI Bank, Kotak Mahindra Bank, Standard Charted Bank','Comma seprated values for supported banks');

insert into mk_feature_gate_key_value_pairs(`key`,`value`,description) values('payments.iciciMinEMITotalAmount','1500','Minimum cart amount for emi option to be available.');
