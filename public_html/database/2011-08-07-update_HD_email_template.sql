update mk_email_notification set body = 'Dear [USER],<br><br>
<p>Your order with Order ID [ORDER_ID] has been processed and Hand Delivered.</p> 
<p>We would like to thank you for using www.myntra.com to buy your customized product.</p> 
<p>We hope you enjoyed the experience and we would love 
to have you back whenever 
you want to buy a unique product.</p><br>
Thanks,<br><br>
Myntra Customer Support<br><br>
------------------------------------------------------------------------------------------------------------------------------------------<br>
<p>This mail is intended only for the person or entity to which it is addressed and may contain confidential and/or privileged <br>
information. Any review, retransmission, dissemination or other use of, or taking of any  action in reliance upon, <br>
this information by persons or entities other than  the intended recipient is prohibited. <br>
If you received this in error, please contact the sender and delete the material from any system.</p>' where name = 'orderprocessedandhanddelivered';
