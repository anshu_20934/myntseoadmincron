 CREATE TABLE `ccm` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `message` varchar(250) NOT NULL,
  `category` varchar(25) NOT NULL,
  `sub_category` varchar(25) DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `state` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
