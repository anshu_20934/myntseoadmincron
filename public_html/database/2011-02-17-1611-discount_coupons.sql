update xcart_languages set value = 'Add' where name = 'lbl_add_coupon';


insert into xcart_languages (code, name, value,topic) values ('US', 'lbl_coupon_type_dual', '% off Upto', 'Labels');


update xcart_languages set value='Apply coupon on' where name = 'lbl_coupon_apply_to';


update xcart_languages set value='Select categories...' where name = 'lbl_coupon_apply_category';


update xcart_languages set value='Select product styles belonging to categories...' where name = 'lbl_coupon_apply_product';


ALTER TABLE `xcart_order_details`
  ADD COLUMN `coupon_code` varchar(50) DEFAULT NULL,
  ADD COLUMN `coupon_discount_product` decimal(12,2) NOT NULL DEFAULT '0.00';


-- Coupon groups table.
ALTER TABLE `mk_coupon_group`
	ADD COLUMN `channel` VARCHAR(50) DEFAULT '', 
	ADD COLUMN `timeCreated` int(11) NOT NULL, 
 	ADD COLUMN `creator` VARCHAR(50) NOT NULL DEFAULT '';

-- Coupon logins (usage) table.
ALTER TABLE `xcart_discount_coupons_login`
	ADD COLUMN `times_locked` int(11) NOT NULL DEFAULT '0',
	ADD COLUMN `subtotal` decimal(32,2) NOT NULL DEFAULT '0.00',
	ADD COLUMN `discount` decimal(32,2) NOT NULL DEFAULT '0.00';

-- Coupons table.
ALTER TABLE `xcart_discount_coupons`
	ADD COLUMN `groupName` VARCHAR(50) DEFAULT NULL,
	ADD COLUMN `isInfinite` tinyint(1) NOT NULL DEFAULT '0',
	ADD COLUMN `maxUsagePerCart` int(11) NOT NULL DEFAULT '-1',
	ADD COLUMN `subtotal` decimal(32,2) NOT NULL DEFAULT '0.00',
	ADD COLUMN `discountOffered` decimal(32,2) NOT NULL DEFAULT '0.00',
  	ADD COLUMN `maxUsageByUser` int(11) NOT NULL DEFAULT '0',
  	ADD COLUMN `isInfinitePerUser` tinyint(1) NOT NULL DEFAULT '0',
	ADD COLUMN `maxAmount` decimal(12,2) NOT NULL DEFAULT '0.00',
  	ADD COLUMN `users` varchar(2048) DEFAULT NULL,
  	ADD COLUMN `excludeUsers` varchar(2048) DEFAULT NULL,
  	ADD COLUMN `paymentMethod` varchar(50) DEFAULT NULL,
  	ADD COLUMN `timeCreated` int(11) NOT NULL DEFAULT '0',
	ADD COLUMN `comments` varchar(256) DEFAULT NULL,
  	ADD COLUMN `couponType` varchar(32) NOT NULL DEFAULT '',
  	ADD COLUMN `MRPAmount` decimal(12,2) DEFAULT '0.00',
  	ADD COLUMN `MRPpercentage` decimal(12,2) DEFAULT '0.00',
  	ADD COLUMN `freeShipping` tinyint(1) NOT NULL DEFAULT '0',
	ADD COLUMN `excludeProductTypeIds` varchar(2048) DEFAULT NULL,
	ADD COLUMN `excludeStyleIds` varchar(2048) DEFAULT NULL,
	ADD COLUMN `catStyleIds` varchar(2048) DEFAULT NULL,
	ADD COLUMN `excludeCatStyleIds` varchar(2048) DEFAULT NULL,
	ADD COLUMN `excludeCategoryIds` varchar(2048) DEFAULT NULL,
	ADD COLUMN `excludeSKUs` varchar(2048) DEFAULT NULL,
	ADD COLUMN `productTypeIds` varchar(2048) DEFAULT NULL,
        ADD COLUMN `styleIds` varchar(2048) DEFAULT NULL,
        ADD COLUMN `categoryIds` varchar(2048) DEFAULT NULL,
        ADD COLUMN `SKUs` varchar(2048) DEFAULT NULL;

-- Accomodate data in the new coupon columns.

-- Update groupName from groupid.
UPDATE xcart_discount_coupons c
   SET c.groupName = (SELECT groupname FROM mk_coupon_group g WHERE g.groupid = c.groupid)
 WHERE c.groupid IS NOT NULL AND c.groupid > 0;

-- Set the couponType from coupon_type
UPDATE xcart_discount_coupons SET couponType = coupon_type WHERE coupon_type = 'absolute';
UPDATE xcart_discount_coupons SET couponType = 'percentage' WHERE coupon_type = 'percent';

-- Set MRPAmount and MRPpercentage from discount
UPDATE xcart_discount_coupons SET MRPAmount = discount WHERE coupon_type = 'absolute';
UPDATE xcart_discount_coupons SET MRPpercentage = discount WHERE coupon_type = 'percent';

-- Set freeShipping from freeship
UPDATE xcart_discount_coupons SET freeShipping = 1 WHERE freeship = 'Y';
UPDATE xcart_discount_coupons SET freeShipping = 0 WHERE freeship = 'N';

-- Set maxUsageByUser from per_user
UPDATE xcart_discount_coupons SET maxUsageByUser = 1 WHERE per_user = 'Y';

-- Set SKUs from productid
UPDATE xcart_discount_coupons
   SET SKUs = productid
 WHERE productid IS NOT NULL AND productid != 0;

-- Set categoryIds from categoryid
UPDATE xcart_discount_coupons
   SET categoryIds = categoryid
 WHERE categoryid IS NOT NULL AND categoryid != 0;

-- Set styleIds from styleid
UPDATE xcart_discount_coupons
   SET styleIds = styleid
 WHERE styleid IS NOT NULL AND styleid != 0;

-- Set productTypeIds from producttypeid
UPDATE xcart_discount_coupons
   SET productTypeIds = producttypeid
 WHERE producttypeid IS NOT NULL AND producttypeid != 0;

-- Add a group for existing standalong coupons.
INSERT INTO `mk_coupon_group`
  (groupname, channel, timeCreated, creator, active, count)
VALUES
  ('OldCoupons', 'online', unix_timestamp(now()), 'myntraprovider', 'A', (SELECT count(*) FROM xcart_discount_coupons WHERE groupid IS NULL OR groupid = 0));

-- Set groupName as OldCoupons for old standalone coupons.
UPDATE `xcart_discount_coupons`
   SET groupName = 'OldCoupons'
 WHERE groupid IS NULL OR groupid = 0;

-- Create a group for feedback coupons.
INSERT INTO `mk_coupon_group`
  (groupname, channel, timeCreated, creator, active, count)
VALUES
  ('FeedbackCoupons', 'online', unix_timestamp(now()), 'myntraprovider', 'A', 0);

