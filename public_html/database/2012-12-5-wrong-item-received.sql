insert into mk_returns_reasons (code, displayname, display_order, customer_visible) values ('WIR', 'Wrong Item Received / No Item Received', 10, 0);

update mk_return_status_details set end_state=1 where code='RRJ';
