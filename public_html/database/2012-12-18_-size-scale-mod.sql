-- delete unused sizes from size-chart-scale-value
delete from size_chart_scale_value where id not in (select sizevalueid from mk_size_unification_rules union select sizeValueId from mk_size_representation_rule);

alter table size_chart_scale add constraint unq_name_at unique(scale_name,id_catalogue_classification_fk);
alter table size_chart_scale_value add constraint unq_name_at unique(size_value,id_size_chart_scale_fk);
alter table size_chart_scale_value add constraint unq_scale_order unique(id_size_chart_scale_fk ,intra_scale_size_order);

-- drop this useless column. It will take huge amount of time, so please run it in off-peak-time only
alter table mk_product_options drop newSize;

-- we no longer keep non-unified size measurements in mk_size_unification_rules. Since US9 is already mapped to UK8, define measurements for UK8 only, not US9 
delete from  mk_size_representation_rule where sizeValueId not in (select unifiedSizeValueId from mk_size_unification_rules);
