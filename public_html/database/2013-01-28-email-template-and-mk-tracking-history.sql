update mk_email_notification set body= '<div style="border: 1px solid gray; padding: 6px">
Hi [USER], <br/>
<p>We hope that you are delighted with the product(s) for the Order No. [ORDER_ID] which we delivered to you on [DELIVERY_DATE].</p>
As a constant endeavour to improve our services, we would request you to share with us some feedback on your shopping experience with Myntra.<br/><br/>
[ORDER_MFB_FORM]<br/>[REDIRECT_INFO]<br/>
<p>For any further clarification, kindly contact us at [MYNTRA_CC_PHONE].</p>

Regards,<br/>
Myntra Customer Connect<br/>
India\'s Largest Online Fashion Store
<br/><br/>[DECLARATION]<br/>
</div>'
where name='voc_order';

update mk_tracking_history set event_value=(select max(instanceid)+1 from mk_feedback_instance where feedback_id=7) where event_type='transitional_feedback_instance_id';
