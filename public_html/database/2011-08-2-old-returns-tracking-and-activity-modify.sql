
alter table mk_old_returns_tracking modify column currstate ENUM('RTOQ','RTOCAL1', 'RTOCAL2', 'RTOCAL3','RTOC','RTORS', 'RTORSC', 'RTORF', 'RTOAI', 'RTQ','RTCSD','RTPI', 'RTPTU', 'RTR','RTQP','RTQF','RTCC','RTQPA','RTQPNA','RTRF','RTAI','RTRS','RTRSC', 'END', 'CMMNT', 'RTCL', 'RTOCL') NOT NULL;

alter table mk_old_returns_tracking_activity add column cc_disposition	ENUM('NRB','NRS','BSY', 'CM');

alter table mk_old_returns_tracking_activity add column cc_resolution	ENUM('RJ', 'RA');
