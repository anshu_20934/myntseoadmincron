-- PORTAL-252: Kundan: now track when was the cart last modified, 
-- so as to be able to send mailers to users who abandoned their carts without proceeding for payment
alter table mk_shopping_cart add column (last_modified int(11) default null);
alter table mk_shopping_cart add index indx_last_modified (last_modified);
alter table mk_shopping_cart add index indx_login (login);