
update mk_email_notification set body = '<div style="border: 1px solid gray; padding: 6px">
Hi [USER], <br/>
<p>Your order with Myntra is now out for delivery. You will receive your product in the next 10 hours</b>.</p>
<p>Here are the tracking details:<br/>
Courier Partner:  [COURIER_SERVICE]<br/>
Tracking Code: [TRACKING_NUMBER]<br/>
Delivery Address: [DELIVERY_ADDRESS]
</p>
<p>You can track the shipment progress of your order in the <a href="http://www.myntra.com/mymyntra.php">My Orders</a> section of My
Myntra.To see tracking details, select your order in My Orders and click on the tracking number link.</p>
<p>Here are the details of Order No. [ORDER_ID] - Shipment No. [SHIPMENT_ID], for your reference-</p>
[ORDER_DETAILS]<br>
<p>At Myntra, we make every possible effort to ensure that you receive your product on time. Please help us to deliver on time by considering the following:
<ul style="list-style-type:decimal;">
<li>Do make your self available to receive the order in the next 10 hours.</li>
<li>In case you are unavailable, do make arrangements to have the order received by someone on your behalf at your delivery address.</li>
</ul>
We would like to thank you for buying from <a href="www.myntra.com">www.myntra.com</a>. We hope you enjoyed the experience and we look forward to having you back!
</p>
<p>In case of any further queries, please get in touch with us at [MYNTRA_PHONE].</p>
Regards,<br/>
Myntra Customer Connect<br/>
India\'s Largest Online Fashion Store
<br/><br/>[DECLARATION]<br/>
</div>' where name = 'out_for_delivery';



insert into mk_email_notification(name,subject,body) values ('out_for_delivery_cod','Your order with Myntra is out for delivery! Order No.[ORDER_ID]' ,
'<div style="border: 1px solid gray; padding: 6px">
Hi [USER], <br/>
<p>Your order with Myntra is now out for delivery. You will receive your product in the next 10 hours</b>.</p>
<p>Here are the tracking details:<br/>
Courier Partner:  [COURIER_SERVICE]<br/>
Tracking Code: [TRACKING_NUMBER]<br/>
Delivery Address: [DELIVERY_ADDRESS]
</p>
<p>You can track the shipment progress of your order in the <a href="http://www.myntra.com/mymyntra.php">My Orders</a> section of My
Myntra.To see tracking details, select your order in My Orders and click on the tracking number link.</p>
<p>Here are the details of Order No. [ORDER_ID] - Shipment No. [SHIPMENT_ID], for your reference-</p>
[ORDER_DETAILS]<br>
<p>Please keep the COD amount of Rs. [ORDER_AMOUNT] ready to pay our delivery representative while you receive the order.</p>
<p>At Myntra, we make every possible effort to ensure that you receive your product on time. Please help us to deliver on time by considering
the following:
<ul style="list-style-type:decimal;">
<li>Do make your self available to receive the order in the next 10 hours.</li>
<li>In case you are unavailable, do make arrangements to have the order received by someone on your behalf at your delivery address.</li>
</ul>
We would like to thank you for buying from <a href="www.myntra.com">www.myntra.com</a>. We hope you enjoyed the experience and we look forward to having you back!
</p>
<p>In case of any further queries, please get in touch with us at [MYNTRA_PHONE].</p>
Regards,<br/>
Myntra Customer Connect<br/>
India\'s Largest Online Fashion Store
<br/><br/>[DECLARATION]<br/>
</div>');








