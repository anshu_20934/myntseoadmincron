CREATE TABLE if not exists `mk_abtesting_tests` (

  `id` INT NOT NULL AUTO_INCREMENT ,

  `name` VARCHAR(45) NULL ,

  `enabled` TINYINT(1) NULL DEFAULT 0 ,

  PRIMARY KEY (`id`) );


CREATE TABLE if not exists `mk_abtesting_variations` (

  `id` INT NOT NULL AUTO_INCREMENT ,

  `ab_test_id` INT NULL ,

  `name` VARCHAR(45) NULL ,

  `percent_probability` INT NULL DEFAULT 0 ,

  PRIMARY KEY (`id`) );
  
  
  
  insert into mk_abtesting_tests (id,name,enabled) values (1,'sortorder',1);
  insert into mk_abtesting_variations (ab_test_id,name,percent_probability) values (1,'sortbyrecency',50);

