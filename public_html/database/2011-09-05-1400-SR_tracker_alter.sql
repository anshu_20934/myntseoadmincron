alter table mk_sr_priority add column `priority_default` enum('Y','N') default 'N';
update mk_sr_priority set priority_default='Y' where SR_priority='P2';

alter table mk_sr_tracker add column customer_SR_id varchar(10) unique default null after SR_id;
alter table mk_order_sr change refund refund enum('Y','N') default 'N', add column refund_type varchar(200) null after refund, add column refund_value text null after refund_type;