insert into mk_order_action_reasons (action, reason, reason_display_name, email_contents, parent_reason, is_internal_reason) values ('oh_disposition','ISO','Incorrect size Ordered', NULL,'CR',0);

insert into mk_order_action_reasons (action, reason, reason_display_name, email_contents, parent_reason, is_internal_reason) values ('oh_disposition','ICO','Incorrect color Ordered', NULL,'CR',0);


insert into cancellation_codes (cancel_type,cancel_type_desc,cancel_reason,cancel_reason_desc,email_content,cancellation_mode) values ('CR', 'Customer Reject', 'ISO', 'Incorrect size Ordered', 'As per your request, We are going ahead with the cancellation of your Order No. [ORDER_ID], the details of which are given below:', 'OH_CANCELLATION');

insert into cancellation_codes (cancel_type,cancel_type_desc,cancel_reason,cancel_reason_desc,email_content,cancellation_mode) values ('CR', 'Customer Reject', 'ICO', 'Incorrect color Ordered', 'As per your request, We are going ahead with the cancellation of your Order No. [ORDER_ID], the details of which are given below:', 'OH_CANCELLATION');

