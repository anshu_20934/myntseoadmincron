CREATE TABLE `mk_mapping_generic` (
  `generic_type` varchar(5) NOT NULL,
  `table_name` varchar(20) NOT NULL,
  `generic_type_pk_column` varchar(20) DEFAULT NULL,
  `description` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`generic_type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO mk_mapping_generic (generic_type, table_name, generic_type_pk_column, description) values ('PTG', 'mk_product_group', 'id', 'Product Type Group');
INSERT INTO mk_mapping_generic (generic_type, table_name, generic_type_pk_column, description) values ('PT', 'mk_product_type', 'id', 'Product Type');
INSERT INTO mk_mapping_generic (generic_type, table_name, generic_type_pk_column, description) values ('PSG', 'mk_style_group', 'style_group_id', 'Product Style Group');
INSERT INTO mk_mapping_generic (generic_type, table_name, generic_type_pk_column, description) values ('PS', 'mk_product_style', 'id', 'Product Style');
INSERT INTO mk_mapping_generic (generic_type, table_name, generic_type_pk_column, description) values ('PRD', 'xcart_products', 'productid', 'Design or Product');
INSERT INTO mk_mapping_generic (generic_type, table_name, generic_type_pk_column, description) values ('DSGR', 'mk_designer', 'customerid', 'Designer');
INSERT INTO mk_mapping_generic (generic_type, table_name, generic_type_pk_column, description) values ('SHOP', 'mk_shop', 'shopid', 'SHOP');

CREATE TABLE `mk_rating_generic` (
  `vote_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `remote_ip` varchar(15) DEFAULT NULL,
  `sessionid` varchar(200) DEFAULT NULL,
  `generic_type` varchar(20) NOT NULL,
  `generic_type_id` int(11) NOT NULL,
  `vote_value` int(1) DEFAULT NULL,
  `user_type` int(1) DEFAULT '0',
  `vote_date` int(11) DEFAULT NULL,
  PRIMARY KEY (`vote_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `mk_review_generic` (
  `vote_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `remote_ip` varchar(15) DEFAULT NULL,
  `generic_type` varchar(20) NOT NULL,
  `generic_type_id` int(11) NOT NULL,
  `review` text,
  `rating_vote_id` int(11) DEFAULT NULL,
  `review_date` int(11) DEFAULT NULL,
  PRIMARY KEY (`vote_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
