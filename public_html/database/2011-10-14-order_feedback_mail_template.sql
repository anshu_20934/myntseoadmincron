update mk_email_notification set body='Hi [USER], <br/><br/>
We hope that you have received the product(s) for the Order No. [ORDER_ID] which we shipped to you on [SHIPPING_DATE].<br/>
As a constant endeavour to improve our services, we would request you to share with us some feedback on your shopping experience with Myntra.<br/><br/>
<a href="[FEEDBACK_URL]">Click here to go to the feedback form</a>
<br/><br/>
As a simple gesture of thanks, we will e-mail you a Rs 100 coupon as soon as you share your feedback.
<br/><br/>
Here are the details of Order No. [ORDER_ID], for your reference -
<br/><br/>
[ORDER_DETAILS]
<br/>
<p>For any further clarification, kindly contact us at [MYNTRA_CC_PHONE].</p>
<br/>
Regards,<br/>
Myntra Customer Care' where name='feedback_request';