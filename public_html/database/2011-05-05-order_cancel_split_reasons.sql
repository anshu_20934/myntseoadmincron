
DROP TABLE mk_order_action_reasons;

CREATE TABLE mk_order_action_reasons (
id int auto_increment NOT NULL,
action  varchar(100) NOT NULL,
reason  varchar(100) NOT NULL,
reason_display_name varchar(100) NOT NULL,
email_contents text,
PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

insert into mk_order_action_reasons (action, reason, reason_display_name, email_contents) values ('item_cancel', 'OOS', 'Items Out of Stock', 'We regret to inform you that we are unable to service your entire order and would have to cancel some of the items due to an overwhelming response, which has exhausted our stocks. Please find the details of the updated order and item(s) cancelled from the original Order No. [ORDER_ID]');

insert into mk_order_action_reasons (action, reason, reason_display_name, email_contents) values ('item_cancel', 'CR', 'Customer Request', 'Thank you for contacting Myntra Support. As per your request, we are cancelling a part of your order. Please find below details of the updated order and item(s) cancelled from the original Order No. [ORDER_ID]');

insert into mk_order_action_reasons (action, reason, reason_display_name, email_contents) values ('order_split', 'CR', 'Customer Request', 'As requested by you, your order has been split up such that it can be shipped to different destination addresses specified by you. Your original Order No. [ORDER_ID] with Myntra has now been split into two orders with Order Nos. [ORDER_ID] and [NEW_ORDER_ID]. You may track the order consignments individually from My Myntra.');

insert into mk_order_action_reasons (action, reason, reason_display_name, email_contents) values ('order_split', 'OOS', 'Items Out of Stock', 'Due to an unforeseen delay in processing some of your order items, we would be shipping your order to you in parts. Your original Order No. [ORDER_ID] with Myntra has now been split into two orders with Order Nos. [ORDER_ID] and [NEW_ORDER_ID]. You may track the order consignments individually from My Myntra.');

insert into mk_order_action_reasons (action, reason, reason_display_name, email_contents) values ('order_split', 'DSL', 'Different Shipping Location', 'Orders on Myntra may be shipped to you from different locations depending on item availability. This is done in the best way possible to minimize time to deliver products to your doorstep.<br/>Due to the above reason, your original Order No. [ORDER_ID] with Myntra has now been split into two orders with Order Nos. [ORDER_ID] and [NEW_ORDER_ID]. You may track the order consignments individually from My Myntra.');
