update mk_email_notification set subject = 'Your order with Myntra has been shipped! Order No. [ORDER_ID]', body = 'Dear [USER],<br><br>
<p>Your order with Order ID [ORDER_ID] has been processed and shipped out through <b>Myntra Logistics</b>. You should be receiving your product within the next 2 working days.</p>
<p>Here are the details of Order No. [ORDER_ID], for your reference -</p>
[ORDER_DETAILS]
<br>
<p>We would like to thank you for buying from <a href="www.myntra.com">www.myntra.com</a>. We hope you enjoyed the experience and <b>we look forward to having you back!</b></p>
<p>We will be sending a separate mailer requesting for your feedback. In case of any further queries, please get in touch with us at [MYNTRA_CC_PHONE].</p>
<br>
Regards,<br>
Myntra Customer Care' where name = 'orderprocessedandhanddelivered';
