insert into mk_feature_gate_key_value_pairs(`key`, `value`, `description`) values ('curatedSearch.enabled', 'false', 'Enable or Disable Curated Search');

insert into mk_abtesting_tests (name, ga_slot, seg_algo,source_type) values ('CuratedSearch', '0', 'abtest\\algos\\impl\\RandomSegmentationAlgo', 'tpl');
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('enabled', (select id from mk_abtesting_tests where name ='CuratedSearch'), 50);
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('control', (select id from mk_abtesting_tests where name ='CuratedSearch'), 50);
