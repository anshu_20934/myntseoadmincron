alter table xcart_orders change parent_orderid group_id int(20), add index warehouseid_idx (warehouseid);

update xcart_orders set warehouseid = 1 WHERE warehouseid is NULL;

update xcart_orders set group_id = orderid where group_id is NULL;
