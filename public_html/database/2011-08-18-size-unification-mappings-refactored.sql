drop table if exists size_unification_mapping;

CREATE TABLE `size_unification_mapping` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `gender` varchar(20) NOT NULL,
  `size` varchar(20) NOT NULL,
  `articletype` varchar(50) DEFAULT NULL,
  `brand` varchar(50) DEFAULT NULL,
  `articletype_brand` varchar(100) DEFAULT NULL,
  `styleid` int(15) DEFAULT NULL,
  `unifiedsize` varchar(20) NOT NULL,
  `filter_order` int(11) DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=726 DEFAULT CHARSET=latin1;

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK1,US2,EURO32',1);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK1,US3.5,EURO32',1);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK1,US2,EURO32',1);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK1.5,US2.5,EURO32.5',1.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK1.5,US4,EURO34',1.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK1.5,US2.5,EURO34',1.5);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK2,US3,EURO33',2);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK2,US4.5,EURO34.5',2);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK2,US3,EURO34.5',2);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK2.5,US3.5,EURO34',2.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK2.5,US5,EURO35',2.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK2.5,US5,EURO35',2.5);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK3,US4,EURO34.5',3);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK3,US5.5,EURO35.5',3);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK3,US4,EURO35.5',3);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK3.5,US4.5,EURO35',3.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK3.5,US6,EURO36',3.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK3.5,US4.5,EURO36',3.5);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK4,US5,EURO36',4);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK4,US6.5,EURO37',4);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK4,US5,EURO37',4);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK4.5,US5.5,EURO36.5',4.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK4.5,US7,EURO37.5',4.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK4.5,US5.5,EURO37.5',4.5);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK5,US6,EURO37.5',5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK5,US7.5,EURO38',5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK5,US6,EURO38',5);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK5.5,US6.5,EURO38.5',5.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK5.5,US8,EURO38.5',5.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK5.5,US6.5,EURO38.5',5.5);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK6,US7,EURO39',6);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK6,US8.5,EURO39',6);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK6,US7,EURO39',6);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK6.5,US7.5,EURO40',6.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK6.5,US9,EURO40',6.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK6.5,US7.5,EURO40',6.5);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK7,US8,EURO40.5',7);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK7,US9.5,EURO40.5',7);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK7,US8,EURO40.5',7);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK7.5,US8.5,EURO41',7.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK7.5,US10,EURO41',7.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK7.5,US8.5,EURO41',7.5);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK8,US9,EURO42',8);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK8,US10.5,EURO42',8);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK8,US9,EURO42',8);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK8.5,US9.5,EURO42.5',8.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK8.5,US11,EURO42.5',8.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK8.5,US9.5,EURO42.5',8.5);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK9,US10,EURO43',9);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK9,US11.5,EURO43',9);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK9,US10,EURO43',9);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK9.5,US10.5,EURO44',9.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK9.5,US12,EURO44',9.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK9.5,US10.5,EURO44',9.5);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK10,US11,EURO44.5',10);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK10,US12.5,EURO44.5',10);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK10,US11,EURO44.5',10);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK10.5,US11.5,EURO45',10.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK10.5,US13,EURO45',10.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK10.5,US11.5,EURO45',10.5);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK11,US12,EURO45.5',11);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK11,US13.5,EURO46',11);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK11,US12,EURO45.5',11);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK11.5,US12.5,EURO46',11.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK11.5,US14,EURO47',11.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK11.5,US12.5,EURO46',11.5);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK12,US13,EURO47',12);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK12,US14.5,EURO48',12);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK12,US13,EURO47',12);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK12.5,US13.5,EURO48',12.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Women','UK12.5,US15,EURO48.5',12.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK12.5,US13.5,EURO48',12.5);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK13,US14,EURO48.5',13);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK13,US14,EURO48.5',13);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK13.5,US14.5,EURO49',13.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK13.5,US14.5,EURO49',13.5);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK14,US15,EURO50',14);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK14,US15,EURO50',14);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK14.5,US15.5,EURO51',14.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK14.5,US15.5,EURO51',14.5);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK15,US16,EURO52',15);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK15,US16,EURO52',15);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK15.5,US16.5,EURO53',15.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK15.5,US16.5,EURO53',15.5);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK16,US17,EURO53.5',16);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK16,US17,EURO53.5',16);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK16.5,US17.5,EURO54',16.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK16.5,US17.5,EURO54',16.5);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK17,US18,EURO55',17);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK17,US18,EURO55',17);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK17.5,US18.5,EURO56',17.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK17.5,US18.5,EURO56',17.5);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK18,US19,EURO56.5',18);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK18,US19,EURO56.5',18);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK18.5,US19.5,EURO57',18.5);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK18.5,US19.5,EURO57',18.5);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK19,US20,EURO58',19);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK19,US20,EURO58',19);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK20,US21,EURO58.5',20);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK20,US21,EURO58.5',20);

insert into size_unification_mapping (gender,size,unifiedsize) values ('Men','UK21,US22,EURO59',21);
insert into size_unification_mapping (gender,size,unifiedsize) values ('Unisex','UK21,US22,EURO59',21);

insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values ( 'Women','C10/11','Sandals','Crocs','Sandals|Crocs',NULL,'9.5','16');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values ('Women','C12','Sandals','Crocs','Sandals|Crocs',NULL,'11','3');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values ('Women','C12/13','Sandals','Crocs','Sandals|Crocs',NULL,'11.5','4');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values ('Women','C4','Sandals','Crocs','Sandals|Crocs',NULL,'3','6');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values ('Women','C4/5','Sandals','Crocs','Sandals|Crocs',NULL,'3.5','7');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','C6','Sandals','Crocs','Sandals|Crocs',NULL,'5','9');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','C6/7','Sandals','Crocs','Sandals|Crocs',NULL,'5.5','10');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','C8/9','Sandals','Crocs','Sandals|Crocs',NULL,'7.5','13');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M10','Sandals','Crocs','Sandals|Crocs',NULL,'9','15');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M10-11/W12-13','Sandals','Crocs','Sandals|Crocs',NULL,'10','2');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M10/W12','Sandals','Crocs','Sandals|Crocs',NULL,'10','2');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M11','Sandals','Crocs','Sandals|Crocs',NULL,'11','3');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M4/W6','Sandals','Crocs','Sandals|Crocs',NULL,'4','8');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M5/W7','Sandals','Crocs','Sandals|Crocs',NULL,'5','9');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M6-7/W8-9','Sandals','Crocs','Sandals|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M6/W8','Sandals','Crocs','Sandals|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M7','Sandals','Crocs','Sandals|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M7/W9','Sandals','Crocs','Sandals|Crocs',NULL,'7','12');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M8','Sandals','Crocs','Sandals|Crocs',NULL,'7','12');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M8-9/W10-11','Sandals','Crocs','Sandals|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M8/W10','Sandals','Crocs','Sandals|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M9','Sandals','Crocs','Sandals|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M9/W11','Sandals','Crocs','Sandals|Crocs',NULL,'9','15');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','W4','Sandals','Crocs','Sandals|Crocs',NULL,'1','1');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','W5','Sandals','Crocs','Sandals|Crocs',NULL,'2','5');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','W6','Sandals','Crocs','Sandals|Crocs',NULL,'3','6');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','W7','Sandals','Crocs','Sandals|Crocs',NULL,'4','8');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','C10/11','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'9.5','16');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','C12','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'11','3');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','C12/13','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'11.5','4');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','C4','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'3','6');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','C4/5','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'3.5','7');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','C6','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'5','9');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','C6/7','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'5.5','10');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','C8/9','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'7.5','13');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M10','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'9','15');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M10-11/W12-13','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'10','2');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M10/W12','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'10','2');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M11','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'11','3');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M4/W6','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'4','8');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M5/W7','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'5','9');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M6-7/W8-9','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M6/W8','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M7','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M7/W9','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'7','12');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M8','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'7','12');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M8-9/W10-11','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M8/W10','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M9','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M9/W11','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'9','15');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','W4','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'1','1');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','W5','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'2','5');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','W6','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'3','6');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','W7','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'4','8');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','C10/11','Slippers','Crocs','Slippers|Crocs',NULL,'9.5','16');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','C12','Slippers','Crocs','Slippers|Crocs',NULL,'11','3');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','C12/13','Slippers','Crocs','Slippers|Crocs',NULL,'11.5','4');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','C4','Slippers','Crocs','Slippers|Crocs',NULL,'3','6');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','C4/5','Slippers','Crocs','Slippers|Crocs',NULL,'3.5','7');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','C6','Slippers','Crocs','Slippers|Crocs',NULL,'5','9');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','C6/7','Slippers','Crocs','Slippers|Crocs',NULL,'5.5','10');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','C8/9','Slippers','Crocs','Slippers|Crocs',NULL,'7.5','13');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M10','Slippers','Crocs','Slippers|Crocs',NULL,'9','15');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M10-11/W12-13','Slippers','Crocs','Slippers|Crocs',NULL,'10','2');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M10/W12','Slippers','Crocs','Slippers|Crocs',NULL,'10','2');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M11','Slippers','Crocs','Slippers|Crocs',NULL,'11','3');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M4/W6','Slippers','Crocs','Slippers|Crocs',NULL,'4','8');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M5/W7','Slippers','Crocs','Slippers|Crocs',NULL,'5','9');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M6-7/W8-9','Slippers','Crocs','Slippers|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M6/W8','Slippers','Crocs','Slippers|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M7','Slippers','Crocs','Slippers|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M7/W9','Slippers','Crocs','Slippers|Crocs',NULL,'7','12');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M8','Slippers','Crocs','Slippers|Crocs',NULL,'7','12');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M8-9/W10-11','Slippers','Crocs','Slippers|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M8/W10','Slippers','Crocs','Slippers|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M9','Slippers','Crocs','Slippers|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','M9/W11','Slippers','Crocs','Slippers|Crocs',NULL,'9','15');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','W4','Slippers','Crocs','Slippers|Crocs',NULL,'1','1');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','W5','Slippers','Crocs','Slippers|Crocs',NULL,'2','5');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','W6','Slippers','Crocs','Slippers|Crocs',NULL,'3','6');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Women','W7','Slippers','Crocs','Slippers|Crocs',NULL,'4','8');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C10/11','Sandals','Crocs','Sandals|Crocs',NULL,'9.5','16');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C12','Sandals','Crocs','Sandals|Crocs',NULL,'11','3');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C12/13','Sandals','Crocs','Sandals|Crocs',NULL,'11.5','4');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C4','Sandals','Crocs','Sandals|Crocs',NULL,'3','6');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C4/5','Sandals','Crocs','Sandals|Crocs',NULL,'3.5','7');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C6','Sandals','Crocs','Sandals|Crocs',NULL,'5','9');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C6/7','Sandals','Crocs','Sandals|Crocs',NULL,'5.5','10');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C8/9','Sandals','Crocs','Sandals|Crocs',NULL,'7.5','13');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M10','Sandals','Crocs','Sandals|Crocs',NULL,'9','15');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M10-11/W12-13','Sandals','Crocs','Sandals|Crocs',NULL,'10','2');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M10/W12','Sandals','Crocs','Sandals|Crocs',NULL,'10','2');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M11','Sandals','Crocs','Sandals|Crocs',NULL,'11','3');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M4/W6','Sandals','Crocs','Sandals|Crocs',NULL,'4','8');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M5/W7','Sandals','Crocs','Sandals|Crocs',NULL,'5','9');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M6-7/W8-9','Sandals','Crocs','Sandals|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M6/W8','Sandals','Crocs','Sandals|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M7','Sandals','Crocs','Sandals|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M7/W9','Sandals','Crocs','Sandals|Crocs',NULL,'7','12');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M8','Sandals','Crocs','Sandals|Crocs',NULL,'7','12');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M8-9/W10-11','Sandals','Crocs','Sandals|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M8/W10','Sandals','Crocs','Sandals|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M9','Sandals','Crocs','Sandals|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M9/W11','Sandals','Crocs','Sandals|Crocs',NULL,'9','15');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','W4','Sandals','Crocs','Sandals|Crocs',NULL,'1','1');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','W5','Sandals','Crocs','Sandals|Crocs',NULL,'2','5');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','W6','Sandals','Crocs','Sandals|Crocs',NULL,'3','6');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','W7','Sandals','Crocs','Sandals|Crocs',NULL,'4','8');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C10/11','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'9.5','16');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C12','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'11','3');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C12/13','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'11.5','4');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C4','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'3','6');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C4/5','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'3.5','7');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C6','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'5','9');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C6/7','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'5.5','10');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C8/9','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'7.5','13');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M10','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'9','15');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M10-11/W12-13','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'10','2');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M10/W12','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'10','2');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M11','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'11','3');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M4/W6','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'4','8');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M5/W7','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'5','9');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M6-7/W8-9','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M6/W8','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M7','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M7/W9','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'7','12');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M8','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'7','12');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M8-9/W10-11','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M8/W10','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M9','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M9/W11','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'9','15');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','W4','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'1','1');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','W5','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'2','5');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','W6','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'3','6');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','W7','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'4','8');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C10/11','Slippers','Crocs','Slippers|Crocs',NULL,'9.5','16');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C12','Slippers','Crocs','Slippers|Crocs',NULL,'11','3');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C12/13','Slippers','Crocs','Slippers|Crocs',NULL,'11.5','4');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C4','Slippers','Crocs','Slippers|Crocs',NULL,'3','6');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C4/5','Slippers','Crocs','Slippers|Crocs',NULL,'3.5','7');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C6','Slippers','Crocs','Slippers|Crocs',NULL,'5','9');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C6/7','Slippers','Crocs','Slippers|Crocs',NULL,'5.5','10');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','C8/9','Slippers','Crocs','Slippers|Crocs',NULL,'7.5','13');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M10','Slippers','Crocs','Slippers|Crocs',NULL,'9','15');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M10-11/W12-13','Slippers','Crocs','Slippers|Crocs',NULL,'10','2');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M10/W12','Slippers','Crocs','Slippers|Crocs',NULL,'10','2');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M11','Slippers','Crocs','Slippers|Crocs',NULL,'11','3');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M4/W6','Slippers','Crocs','Slippers|Crocs',NULL,'4','8');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M5/W7','Slippers','Crocs','Slippers|Crocs',NULL,'5','9');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M6-7/W8-9','Slippers','Crocs','Slippers|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M6/W8','Slippers','Crocs','Slippers|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M7','Slippers','Crocs','Slippers|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M7/W9','Slippers','Crocs','Slippers|Crocs',NULL,'7','12');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M8','Slippers','Crocs','Slippers|Crocs',NULL,'7','12');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M8-9/W10-11','Slippers','Crocs','Slippers|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M8/W10','Slippers','Crocs','Slippers|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M9','Slippers','Crocs','Slippers|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','M9/W11','Slippers','Crocs','Slippers|Crocs',NULL,'9','15');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','W4','Slippers','Crocs','Slippers|Crocs',NULL,'1','1');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','W5','Slippers','Crocs','Slippers|Crocs',NULL,'2','5');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','W6','Slippers','Crocs','Slippers|Crocs',NULL,'3','6');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Men','W7','Slippers','Crocs','Slippers|Crocs',NULL,'4','8');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C10/11','Sandals','Crocs','Sandals|Crocs',NULL,'9.5','16');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C12','Sandals','Crocs','Sandals|Crocs',NULL,'11','3');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C12/13','Sandals','Crocs','Sandals|Crocs',NULL,'11.5','4');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C4','Sandals','Crocs','Sandals|Crocs',NULL,'3','6');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C4/5','Sandals','Crocs','Sandals|Crocs',NULL,'3.5','7');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C6','Sandals','Crocs','Sandals|Crocs',NULL,'5','9');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C6/7','Sandals','Crocs','Sandals|Crocs',NULL,'5.5','10');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C8/9','Sandals','Crocs','Sandals|Crocs',NULL,'7.5','13');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M10','Sandals','Crocs','Sandals|Crocs',NULL,'9','15');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M10-11/W12-13','Sandals','Crocs','Sandals|Crocs',NULL,'10','2');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M10/W12','Sandals','Crocs','Sandals|Crocs',NULL,'10','2');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M11','Sandals','Crocs','Sandals|Crocs',NULL,'11','3');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M4/W6','Sandals','Crocs','Sandals|Crocs',NULL,'4','8');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M5/W7','Sandals','Crocs','Sandals|Crocs',NULL,'5','9');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M6-7/W8-9','Sandals','Crocs','Sandals|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M6/W8','Sandals','Crocs','Sandals|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M7','Sandals','Crocs','Sandals|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M7/W9','Sandals','Crocs','Sandals|Crocs',NULL,'7','12');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M8','Sandals','Crocs','Sandals|Crocs',NULL,'7','12');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M8-9/W10-11','Sandals','Crocs','Sandals|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M8/W10','Sandals','Crocs','Sandals|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M9','Sandals','Crocs','Sandals|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M9/W11','Sandals','Crocs','Sandals|Crocs',NULL,'9','15');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','W4','Sandals','Crocs','Sandals|Crocs',NULL,'1','1');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','W5','Sandals','Crocs','Sandals|Crocs',NULL,'2','5');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','W6','Sandals','Crocs','Sandals|Crocs',NULL,'3','6');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','W7','Sandals','Crocs','Sandals|Crocs',NULL,'4','8');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C10/11','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'9.5','16');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C12','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'11','3');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C12/13','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'11.5','4');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C4','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'3','6');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C4/5','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'3.5','7');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C6','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'5','9');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C6/7','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'5.5','10');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C8/9','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'7.5','13');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M10','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'9','15');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M10-11/W12-13','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'10','2');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M10/W12','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'10','2');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M11','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'11','3');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M4/W6','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'4','8');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M5/W7','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'5','9');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M6-7/W8-9','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M6/W8','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M7','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M7/W9','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'7','12');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M8','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'7','12');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M8-9/W10-11','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M8/W10','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M9','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M9/W11','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'9','15');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','W4','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'1','1');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','W5','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'2','5');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','W6','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'3','6');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','W7','Casual Shoes','Crocs','Casual Shoes|Crocs',NULL,'4','8');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C10/11','Slippers','Crocs','Slippers|Crocs',NULL,'9.5','16');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C12','Slippers','Crocs','Slippers|Crocs',NULL,'11','3');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C12/13','Slippers','Crocs','Slippers|Crocs',NULL,'11.5','4');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C4','Slippers','Crocs','Slippers|Crocs',NULL,'3','6');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C4/5','Slippers','Crocs','Slippers|Crocs',NULL,'3.5','7');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C6','Slippers','Crocs','Slippers|Crocs',NULL,'5','9');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C6/7','Slippers','Crocs','Slippers|Crocs',NULL,'5.5','10');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','C8/9','Slippers','Crocs','Slippers|Crocs',NULL,'7.5','13');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M10','Slippers','Crocs','Slippers|Crocs',NULL,'9','15');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M10-11/W12-13','Slippers','Crocs','Slippers|Crocs',NULL,'10','2');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M10/W12','Slippers','Crocs','Slippers|Crocs',NULL,'10','2');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M11','Slippers','Crocs','Slippers|Crocs',NULL,'11','3');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M4/W6','Slippers','Crocs','Slippers|Crocs',NULL,'4','8');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M5/W7','Slippers','Crocs','Slippers|Crocs',NULL,'5','9');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M6-7/W8-9','Slippers','Crocs','Slippers|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M6/W8','Slippers','Crocs','Slippers|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M7','Slippers','Crocs','Slippers|Crocs',NULL,'6','11');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M7/W9','Slippers','Crocs','Slippers|Crocs',NULL,'7','12');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M8','Slippers','Crocs','Slippers|Crocs',NULL,'7','12');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M8-9/W10-11','Slippers','Crocs','Slippers|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M8/W10','Slippers','Crocs','Slippers|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M9','Slippers','Crocs','Slippers|Crocs',NULL,'8','14');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','M9/W11','Slippers','Crocs','Slippers|Crocs',NULL,'9','15');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','W4','Slippers','Crocs','Slippers|Crocs',NULL,'1','1');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','W5','Slippers','Crocs','Slippers|Crocs',NULL,'2','5');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','W6','Slippers','Crocs','Slippers|Crocs',NULL,'3','6');
insert into size_unification_mapping(gender,size,articletype,brand,articletype_brand,styleid,unifiedsize,filter_order) values('Unisex','W7','Slippers','Crocs','Slippers|Crocs',NULL,'4','8');