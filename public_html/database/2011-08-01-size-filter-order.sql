/*=================================================================
	Changes for the size filter ordering : Lohiya : Start
=================================================================*/
/*Update article type column*/
update size_unification_mapping set articletype=SUBSTRING_INDEX(articletype_brand,'|',1) where articletype_brand is not null;

/*Procedure to create columns, it will first check for the columns and if exists then it will drop them*/
drop procedure if exists schema_change_alter_table;

delimiter ';;'
create procedure schema_change_alter_table() begin

 /* delete columns if they exist */
 if exists (select * from information_schema.columns where table_name = 'size_unification_mapping' and column_name = 'brand') then
  ALTER TABLE size_unification_mapping DROP COLUMN brand;
 end if;
 if exists (select * from information_schema.columns where table_name = 'size_unification_mapping' and column_name = 'filter_order') then
  ALTER TABLE size_unification_mapping DROP COLUMN filter_order;
 end if;
 
 /* add columns */
ALTER TABLE size_unification_mapping ADD COLUMN brand VARCHAR(50)  AFTER articletype;
ALTER TABLE size_unification_mapping ADD COLUMN filter_order INT(11) DEFAULT 0  AFTER unifiedsize;
  
end;;

delimiter ';'
call schema_change_alter_table();

drop procedure if exists schema_change_alter_table;

/*Procedure ends*/

/*Update brand column*/
update size_unification_mapping set brand=SUBSTRING_INDEX(articletype_brand,'|',-1) where articletype_brand is not null;

set @num := 0, @type := '', @unified :='';

UPDATE size_unification_mapping AS a
    RIGHT JOIN (
        select  id , @num := if(@type = articletype, if(@unified = concat(articletype,unifiedsize),@num ,@num + 1 ), 1) as filter_order, @type := articletype as dummy, @unified := concat(articletype,unifiedsize) as dummy1 
	from size_unification_mapping where articletype is not null order by articletype,unifiedsize
    ) AS m 
      ON 
	m.id = a.id
SET
    a.filter_order = m.filter_order;

