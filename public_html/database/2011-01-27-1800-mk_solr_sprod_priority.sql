CREATE TABLE `mk_solr_sprod_priority` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `priority` int(11) default NULL,
  `display_order` int(11) default NULL,
  `is_active` tinyint(1) default NULL,
  `label` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

insert into `mk_solr_sprod_priority` (`id`,`name`,`priority`,`display_order`,`is_active`,`label`) values (1,'categories_filter',9,1,1,'Categories Filter');
insert into `mk_solr_sprod_priority` (`id`,`name`,`priority`,`display_order`,`is_active`,`label`) values (2,'stylename',5,2,1,'Style Name');
insert into `mk_solr_sprod_priority` (`id`,`name`,`priority`,`display_order`,`is_active`,`label`) values (3,'product',2,0,1,'Product');
insert into `mk_solr_sprod_priority` (`id`,`name`,`priority`,`display_order`,`is_active`,`label`) values (4,'brands_filter',10,0,1,'Brands Filter');
insert into `mk_solr_sprod_priority` (`id`,`name`,`priority`,`display_order`,`is_active`,`label`) values (5,'sports_filter',8,0,1,'Sports Filter');
insert into `mk_solr_sprod_priority` (`id`,`name`,`priority`,`display_order`,`is_active`,`label`) values (7,'fit_filter',6,0,1,'Fit Filter');
insert into `mk_solr_sprod_priority` (`id`,`name`,`priority`,`display_order`,`is_active`,`label`) values (8,'team_filter',4,0,1,'Team Filter');
insert into `mk_solr_sprod_priority` (`id`,`name`,`priority`,`display_order`,`is_active`,`label`) values (10,'teamsleagues_filter',3,0,1,'Team/Leagues Flter');
insert into `mk_solr_sprod_priority` (`id`,`name`,`priority`,`display_order`,`is_active`,`label`) values (11,'product_type_group',11,0,1,'Product Type Group');
insert into `mk_solr_sprod_priority` (`id`,`name`,`priority`,`display_order`,`is_active`,`label`) values (12,'descr',1,0,1,'Description');
insert into `mk_solr_sprod_priority` (`id`,`name`,`priority`,`display_order`,`is_active`,`label`) values (13,'keywords',5,0,1,'Keywords');
