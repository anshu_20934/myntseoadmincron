INSERT INTO `page_config_image_rule` (`location_type`, `section`, `width`, `height`, `min`, `max`, `step`, `size`, `require_page_location`, `parent_rule_id`)
VALUES
	('new-home-page', 'slideshow', 980, 459, 1, 10, 1, 512000, 0, NULL),
	('new-home-page', 'static', 320, 490, 1, 2, 1, 256000, 0, NULL);

