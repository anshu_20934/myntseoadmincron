-- default value of 1/july/2011 is used for timeReminderEmailSent.
alter table xcart_discount_coupons add column timeReminderEmailSent int(11) default 1309478400; 
alter table xcart_discount_coupons add index `timeReminderEmailSent_idx` (timeReminderEmailSent);
alter table xcart_discount_coupons add index `expire_idx` (expire);

insert into  mk_email_notification 
 (name, subject, body) 
 values
 ("mrp_coupon_expiry_reminder","Your Mynt Credits are about to expire! Shop now to redeem them", '<div style="margin:0px; padding:0px;line-height:0px;">
	<a href="http://www.myntra.com?utm_source=MRP_mailer&utm_medium=coupon_expiry_reminder&utm_campaign=shop_now" target="_blank" style="outline:none">
	<img src="http://myntramailer.s3.amazonaws.com/july2011/invitation-mailer-main-banner.jpg" width="615" height="331" alt="Shop now at Myntra.com" title="Shop now at Myntra.com" style="text-decoration:none;border:none;font-family:Arial,Helvetica,sans-serif;color:#666;font-size:12px;"/>
	</a>
  </div>
  	
	<div style="margin-top:10px; width: 615px;:0px; padding:0px;font-family:Arial,Helvetica,sans-serif;color:#322725;font-size:10px;" align="center">
	<table border="1" bordercolor="#7f7f7f" style="background-color:#FFFFFF; font-size:13px;" width="552" cellpadding="4" cellspacing="0" >
	[DISPLAY_COUPON_TABLE]
	</table>
</div>
<div style="margin-top:20px; width: 587px;margin-right:20px;line-height:0px;font-family:Arial,Helvetica,sans-serif;color:#322725;font-size:12px;" align="right">
<a href="http://www.myntra.com/mymyntra.php?view=mymyntcredits&utm_source=MRP_mailer&utm_medium=coupon_expiry_reminder&utm_campaign=view_my_credits" style="color:#7f1f5e;" target="_blank">View my Mynt credits >>></a>
</div>
<div style="margin-top:20px; width:615px;line-height:0px;font-family:Arial,Helvetica,sans-serif;color:#322725;font-size:12px;" >
<img src="http://myntramailer.s3.amazonaws.com/july2011/invitation-mailer-bottom-banner.jpg" width="615" height="43" alt="" title="" style="text-decoration:none;border:none;font-family:Arial,Helvetica,sans-serif;color:#666;font-size:12px;"/>
</div>');