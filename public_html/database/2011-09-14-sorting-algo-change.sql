DROP TABLE IF EXISTS mk_sort_field;
CREATE TABLE mk_sort_field (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(100),
  order_by ENUM('asc','desc'),
  created_on INT(11) NULL,
  created_by VARCHAR(128) NULL,
  updated_on INT(11) NULL,
  updated_by VARCHAR(128) NULL,
  is_active TINYINT(1) DEFAULT 1,
  sort_order INT(11) DEFAULT 0,
  PRIMARY KEY (id)
);


DROP TABLE IF EXISTS mk_sort_field_value_weightage;
CREATE TABLE mk_sort_field_value_weightage (
  id INT(11) NOT NULL AUTO_INCREMENT,
  sort_field_id INT(11) NOT NULL,
  field_value VARCHAR(100),
  weightage INT(11),
  created_on INT(11) NULL,
  created_by VARCHAR(128) NULL,
  updated_on INT(11) NULL,
  updated_by VARCHAR(128) NULL,
  is_active TINYINT(1) DEFAULT 1,
  PRIMARY KEY (id)
);

insert into mk_sort_field (name,order_by,is_active,sort_order) values ('year_fashion_season','desc',1,1);
insert into mk_sort_field (name,order_by,is_active,sort_order) values ('year_fashion','desc',1,2);
insert into mk_sort_field (name,order_by,is_active,sort_order) values ('availability','desc',1,3);

insert into mk_sort_field_value_weightage (sort_field_id ,field_value , weightage, is_active ) values (1,'2011_Fashion_Spring',99,1);
insert into mk_sort_field_value_weightage (sort_field_id ,field_value , weightage, is_active ) values (1,'2011_Fashion_Summer',99,1);
insert into mk_sort_field_value_weightage (sort_field_id ,field_value , weightage, is_active ) values (2,'2010_Fashion',98,1);
insert into mk_sort_field_value_weightage (sort_field_id ,field_value , weightage, is_active ) values (2,'2009_Fashion',97,1);

insert into mk_sort_field_value_weightage (sort_field_id ,field_value , weightage, is_active ) values (3,'0',0,1);
insert into mk_sort_field_value_weightage (sort_field_id ,field_value , weightage, is_active ) values (3,'1',80,1);
insert into mk_sort_field_value_weightage (sort_field_id ,field_value , weightage, is_active ) values (3,'2',90,1);
insert into mk_sort_field_value_weightage (sort_field_id ,field_value , weightage, is_active ) values (3,'3',100,1);
insert into mk_sort_field_value_weightage (sort_field_id ,field_value , weightage, is_active ) values (3,'4',100,1);
