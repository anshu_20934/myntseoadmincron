alter table mk_widget_top_nav add column display_category varchar(255) default "";
alter table mk_widget_top_nav add column styleid int(11);
alter table mk_attribute_type_values add column is_featured int(1) default 0;
alter table mk_attribute_type_values add column is_latest int(1) default 0;
insert into mk_widget_key_value_pairs (`key`, value) values('megaMenuMensTitle', 'FOR HIM');
insert into mk_widget_key_value_pairs (`key`, value) values('megaMenuWomensTitle', 'FOR HER');
insert into mk_widget_key_value_pairs (`key`, value) values('megaMenuKidsTitle', 'KIDS');


