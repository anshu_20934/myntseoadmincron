/*
 * New UI: page configurators and banner upload on admin side need images to be uploaded and saved
 * 5 new tables were added for the same. This is the drop table script for the same.
 * Author: Kundan Burnwal
 */
drop table if exists page_config_image_reference;
drop table if exists page_config_image_rule;