
insert into `mk_registration_class` (`channel`, `registration_class`) values('FA','FacebookAppRegistration');
insert into `mk_registration_class` (`channel`, `registration_class`) values('FB','FacebookRegistration');


insert into `mk_registration_emails` (`channel`, `start_date`, `end_date`, `registration_email_template`) values('FA','1296536852','1356930452','facebookappregistation');


insert into `mk_registration_actions` (`channel`, `execution_order`, `action`) values('FA','2','sendEmail');
insert into `mk_registration_actions` (`channel`, `execution_order`, `action`) values('FA','1','saveToDB');
insert into `mk_registration_actions` (`channel`, `execution_order`, `action`) values('FB','1','saveToDB');
insert into `mk_registration_actions` (`channel`, `execution_order`, `action`) values('FB','2','sendPasswordResetEmail');
insert into `mk_registration_actions` (`channel`, `execution_order`, `action`) values('FB','3','autoLogin');