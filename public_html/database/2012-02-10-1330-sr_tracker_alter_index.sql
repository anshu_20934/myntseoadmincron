alter table mk_sr_tracker add column SR_department varchar(50) not null after `status`, add column SR_channel varchar(50) not null after SR_department;
create index SR_department_HASH using hash on mk_sr_tracker(SR_department);
create index SR_channel_HASH using hash on mk_sr_tracker(SR_channel);

alter table mk_sr_tracker add column callbacktime int null after SR_channel;
create index callbacktime on mk_sr_tracker(callbacktime);

create unique index code_UNIQUE on mk_courier_service(code);