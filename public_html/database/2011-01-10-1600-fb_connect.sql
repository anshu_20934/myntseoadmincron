/*Table structure for table `mk_facebook_user` */

DROP TABLE IF EXISTS `mk_facebook_user`;

CREATE TABLE `mk_facebook_user` (
  `fb_uid` varchar(20) NOT NULL,
  `myntra_login` varchar(128) NOT NULL,
  `fb_name` varchar(255) default NULL,
  `fb_first_name` varchar(255) default NULL,
  `fb_last_name` varchar(255) default NULL,
  `fb_birthday` date default NULL,
  `fb_about_me` text default NULL,
  `fb_email` varchar(128) default NULL,
  `fb_first_login` int(11) default 0,
  `fb_last_login` int(11) default 0,
  `fb_session_key` text default NULL,
  `fb_mobile_phone` varchar(20) default NULL,
  `fb_address_street` varchar(255) default NULL,
  `fb_address_city` varchar(128) default NULL,
  `fb_address_state` varchar(128) default NULL,
  `fb_address_country` varchar(128) default NULL,
  `fb_address_zip` varchar(16) default NULL,
  `fb_gender` varchar(16) default NULL,
  `fb_hometown` varchar(255) default NULL,
  `fb_location` varchar(255) default NULL,
  `fb_relationship_status` varchar(255) default NULL,
  `fb_likes` text default NULL,
  `fb_movies` text default NULL,
  `fb_music` text default NULL,
  `fb_books` text default NULL,
  `fb_interests` text default NULL,
  `fb_quotes` text default NULL,
  `fb_tv` text default NULL,
  `fb_activities` text default NULL,
  `fb_family`	text default NULL,
  `fb_friends`	text default NULL,
  PRIMARY KEY  (`fb_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

alter  table mk_facebook_user add index `myntra_login` (myntra_login);


DROP TABLE IF EXISTS `mk_facebook_family`;
