alter table mk_style_visibility_info add `style_revenue_adjusted` int(11) DEFAULT 0;
alter table mk_style_visibility_info_backup add `style_revenue_adjusted` int(11) DEFAULT 0;

insert into mk_abtesting_tests (name, ga_slot, seg_algo,source_type) values ('styleRevenueAdjusted', '0', 'abtest\\algos\\impl\\RandomSegmentationAlgo', 'tpl');
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('test', (select id from mk_abtesting_tests where name ='styleRevenueAdjusted'), 0);
insert into mk_abtesting_variations (name, ab_test_id, percent_probability) values ('control', (select id from mk_abtesting_tests where name ='styleRevenueAdjusted'), 100);