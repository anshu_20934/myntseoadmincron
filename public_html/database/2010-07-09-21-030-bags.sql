/*query*/
use myntra;

/*1.insert into mk_style_images tp get 75*75 for all styles(for new style to be added)*/
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1183','images/style/carea/T/pre-school-bag-1183.png','B','Y');
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1182','images/style/carea/T/pre-school-bag-1182.png','B','Y');
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1184','images/style/carea/T/lunch-box-bag-1184.png','B','Y');
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1185','images/style/carea/T/lunch-box-bag-1185.png','B','Y');
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1187','images/style/carea/T/tote-bag-1187.png','B','Y');
insert into mk_style_images (styleid,image_path,`type`,isdefault) values('1188','images/style/carea/T/travel-bag-1188.png','B','Y');

/*3.update for bags styles orientations*/
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='98',textColorDefault='98' where id=1665;
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='45',textColorDefault='45' where id=1664;
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='139',textColorDefault='139' where id=1666;
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='139',textColorDefault='139' where id=1667;
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='139',textColorDefault='139' where id=1668;
update mk_customization_orientation set textLimitation='1',textLimitationTpl='customize_text_limitation.tpl',textFontColors='139',textColorDefault='139' where id=1669;

/*4.insert into mk_text_font_detail*/
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1664','150','0.5','12','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1664','151','0.75','12','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1664','152','0.75','12','N','Y');

insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1665','150','0.5','12','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1665','151','0.75','12','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1665','152','0.75','12','N','Y');

insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1666','150','0.5','12','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1666','151','0.75','12','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1666','152','0.75','12','N','Y');

insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1667','150','0.5','12','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1667','151','0.75','12','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1667','152','0.75','12','N','Y');

insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1668','150','0.5','12','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1668','151','0.75','12','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1668','152','0.75','12','N','Y');

insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1669','150','0.5','12','Y','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1669','151','0.75','12','N','Y');
insert into mk_text_font_detail(orientation_id,font_id,font_size,maxchars,font_default,size_default) values('1669','152','0.75','12','N','Y');
