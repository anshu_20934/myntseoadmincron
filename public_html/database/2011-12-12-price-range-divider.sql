insert into mk_abtesting_tests (name,ga_slot,seg_algo,source_type) values('PriceRange','5','abtest\\algos\\impl\\RandomSegmentationAlgo','tpl');

insert into mk_abtesting_variations (name,ab_test_id,percent_probability) values('v1',(select id from mk_abtesting_tests where name ='PriceRange'),60);
insert into mk_abtesting_variations (name,ab_test_id,percent_probability) values('control',(select id from mk_abtesting_tests where name ='PriceRange'),40);