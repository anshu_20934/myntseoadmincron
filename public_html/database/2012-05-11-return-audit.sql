create table mk_return_transit_details(id int(11) NOT NULL AUTO_INCREMENT, returnid int(11) not null,from_status varchar(20),to_status varchar(20) not null,audit_time int(11) not null,created_by varchar(50),PRIMARY KEY (id),KEY rt_aud_returnid_idx (returnid),KEY rt_aud_date_idx (audit_time));


alter table xcart_returns add column (is_refunded tinyint(1) not null default 0);

alter table mk_return_status_details add index ret_stat_code(code); 

update xcart_returns set is_refunded = 1 where qapassdate is not null and qapassdate != 0;




