drop table if exists daily_cod_oh_orders_breakup;
create table daily_cod_oh_orders_breakup(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, order_date date default NULL, start_hour int(2) NOT NULL, end_hour int(2) NOT NULL, no_of_orders int(11) NOT NULL DEFAULT 0);

insert into mk_order_action_reasons (action,reason,reason_display_name,parent_reason,is_internal_reason) values ('oh_disposition', 'NPV', 'Incorrect Phone No - New Phone No Updated', 'CQ', 0);

insert into mk_order_action_reasons (action,reason,reason_display_name,parent_reason,is_internal_reason) values ('oh_disposition', 'NAPV', 'Incorrect Address & Phone - New Address & Phone Updated', 'CQ', 0);

insert into mk_order_action_reasons (action,reason,reason_display_name,parent_reason,is_internal_reason) values ('oh_disposition', 'CB', 'Call Back', 'CQ', 0);

insert into mk_order_action_reasons (action,reason,reason_display_name,parent_reason,is_internal_reason) values ('oh_disposition', 'TAN', 'Try Another Number', 'CQ', 0);

insert into mk_order_action_reasons (action,reason,reason_display_name,parent_reason,is_internal_reason) values ('oh_disposition', 'ISO', 'Incorrect Size Ordered', 'CR', 0);

insert into mk_order_action_reasons (action,reason,reason_display_name,parent_reason,is_internal_reason) values ('oh_disposition', 'ICO', 'Incorrect Color Ordered', 'CR', 0);


insert into mk_api_users values (2,'minacs','B-e6444f458e99de2c41b114e62b232bf07a1b354be29a8c32','Aditya Birla Minacs','SuperUser',1);
insert into mk_api_users values (3,'Myntra','Myntra123','Myntra Portal','SuperUser',1);

alter table mk_api_users add column source varchar(255) default NULL, add column target varchar(255) default null;

update mk_api_users set source='myntra-erp', target = 'myntra-portal' where id = 1;
update mk_api_users set source='minacs', target = 'myntra-portal' where id = 2;
update mk_api_users set source='myntra-portal', target = 'minacs' where id = 3;

