insert into mk_abtesting_tests (name,ga_slot,seg_algo,source_type) values('abinfinitescroll','3','abtest\\algos\\impl\\RandomSegmentationAlgo','tpl');
insert into mk_abtesting_variations (ab_test_id,name,percent_probability) values((select id from mk_abtesting_tests where name='abinfinitescroll'),'infinitescroll',50);
insert into mk_abtesting_variations (ab_test_id,name,percent_probability) values((select id from mk_abtesting_tests where name='abinfinitescroll'),'control',50);
insert into mk_widget_key_value_pairs(`key`, `value`) values('noOfAutoInfiniteScrolls', 4);

