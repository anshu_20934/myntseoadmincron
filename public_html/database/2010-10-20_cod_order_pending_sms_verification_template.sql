insert into mk_email_notification(name, subject, body) values( 'cashondeliveryconfirm', 'Your mobile verification for Cash On Delivery order', 'Dear [USER],
<br/><br/>
<p>Please ignore this email if you have already verified your mobile number to place the order.
<br/><br/>
In case you were not able to enter your verification code or did not receive an sms, please follow this link:</p>
<br/>
<a href="[REDIRECT_LINK]">Click here to verify or resend sms</a>
<br/>

<p>Thanks</p>
<p>Myntra Customer Support</p>
<br/><br/><br/>
<p style="color:#0000ff">
	<i>------------------------------------------------------------------------------------------------------------------------------------------<br>
	This mail is intended only for the person or entity to which it is addressed and may contain confidential and/or privileged
	information. Any review, retransmission, dissemination or other use of, or taking of any action in reliance upon,
	this information by persons or entities other than the intended recipient is prohibited.
	If you received this in error, please contact the sender and delete the material from any system.
	</i>
</p>');