/* to add the index on the style id column */
create index style_id_index on mk_style_properties(style_id);

ALTER TABLE `download_images_status` CHANGE `product_style_name` `product_style_name` VARCHAR(100);
