alter table xcart_orders add index `cod_idx` (cod_pay_status, payment_method);
alter table xcart_order_details add index `product_type_idx` (product_type);
alter table mk_pos_order_details add index `order_idx` (orderid);

