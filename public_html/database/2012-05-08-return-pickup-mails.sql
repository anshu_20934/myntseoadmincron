insert into mk_email_notification(name, subject, body) values('out_for_pickup', 'Out for pickup of your Myntra returns! Return No. [RETURN_ID]',
'<div style="border: 1px solid gray; padding: 6px">
Hi [USER], <br/>
<p>Your return request has been processed by Myntra and our agent is out for pick-up of your items. Your items will be picked-up in the next 12 hours. Here are the tracking details.</p>
<p>Here are the tracking details:<br/>
Return Tracking Code:  [RETURN_ID]<br/>
Pick-up Address: [PICKUP_ADDRESS]<br/>
Phone Number: [PHONE_NUMBER]
</p>
<p>You can track the shipment progress of your return in the <a href="http://www.myntra.com/mymyntra.php">My Returns</a> section of My Myntra.</p>
<p>Here are the details of Return No. [RETURN_ID], for your reference -</p>[RETURN_DETAILS]<br>
<p>At Myntra, we make every possible effort to ensure that your product is picked-up on time. Please help us by considering the following:
<ul style="list-style-type:decimal;">
<li>Do make your self available to send the pick-up in the next 12 hours.</li>
<li> If case you are unavailable, do make arrangements to have the pick-up sent by someone on your behalf at your pick-up address.</li>
</ul>
We would like to thank you for buying from <a href="www.myntra.com">www.myntra.com</a>. We hope you enjoyed the experience and we look forward to having you back!
</p>
<p>In case of any further queries, please get in touch with us at [MYNTRA_PHONE].</p>
Regards,<br/>
Myntra Customer Connect<br/>
India\'s Largest Online Fashion Store
<br/><br/>[DECLARATION]<br/>
</div>');

insert into mk_email_notification(name, subject, body) values('successful_pickup', 'Your returns with Myntra has been picked up! Return No. [RETURN_ID]',
'<div style="border: 1px solid gray; padding: 6px">Hi [USER], <br/>
 <p>Your order with Myntra has been successfully picked-up. Here are the tracking details of this order:<br/></p>
Return Tracking Code:  [RETURN_ID]<br/>
Pick-up Address: [PICKUP_ADDRESS]<br/>
Phone Number: [PHONE_NUMBER]
</p>
<p>You can track the shipment progress of your return in the <a href="http://www.myntra.com/mymyntra.php">My Returns</a> section of My Myntra.</p>
<p>Here are the details of Return No. [RETURN_ID], for your reference -</p>[RETURN_DETAILS]<br>
<p>We would like to thank you for buying from www.myntra.com. We hope you enjoyed the experience and we look forward to having you back!</p>
<p>In case of any further queries, please get in touch with us at [MYNTRA_PHONE].</p>
Regards,<br/>
Myntra Customer Connect<br/>
India\'s Largest Online Fashion Store
<br/><br/>[DECLARATION]<br/></div>');

insert into mk_email_notification(name, subject, body) values('failed_pickup', 'We attempted to pickup your returns with Myntra! Return No. [RETURN_ID]',
'<div style="border: 1px solid gray; padding: 6px">Hi [USER], <br/>
 <p>We attempted to pick-up your product on [PICKUP_DATE] and could not reach you. Our customer service representative will get in touch with you shortly regarding this pick-up. Here are the tracking details:<br/></p>
Return Tracking Code:  [RETURN_ID]<br/>
Pick-up Address: [PICKUP_ADDRESS]<br/>
Phone Number: [PHONE_NUMBER]
</p>
<p>Here are the details of Return No. [RETURN_ID], for your reference -</p>[RETURN_DETAILS]<br>
<p>In case of any further queries, please get in touch with us at [MYNTRA_PHONE].</p>
Regards,<br/>
Myntra Customer Connect<br/>
India\'s Largest Online Fashion Store
<br/><br/>[DECLARATION]<br/></div>');


