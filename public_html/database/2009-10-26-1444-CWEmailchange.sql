use myntra;

update  mk_email_notification set body='

Dear [EMPLOYEE_NAME], <BR><BR>

Your account has been created in the Cable and Wireless R&R portal. <BR>
Please see below for your credentials for portal login. You are requested to change your password after first login.<BR><BR>

System URL: http://cw-galaxy.myntra.com <BR><BR>

<b>Login: [LOGIN]</b><BR><BR>

Password: [PASSWORD]<BR><BR>

Points would be allocated to your rewards accounts within an hour of receiving this email. <BR><BR>
We look forward to your active participation. Happy Gifting! <BR><BR>

Sincerely<BR><BR>

HR Department<BR><BR>
' where name='RRNewEmployee';
