alter table cancellation_codes add column (is_active tinyint(1) not null default 1);

update cancellation_codes set is_active = 0 where cancellation_mode = 'ITEM_CANCELLATION' and cancel_type = 'CCR' and cancel_reason not in  ('OOSC','PMM');
update cancellation_codes set is_active = 0 where cancellation_mode = 'FULL_ORDER_CANCELLATION' and cancel_type = 'CCR' and cancel_reason in ('CIOC','DOC','FO');


insert into cancellation_codes values (null,'CCR','Customer Request','OTHR','Others','Thank you for contacting Myntra Support. As per your request, we are cancelling your Order No. [ORDER_ID].',null,'FULL_ORDER_CANCELLATION',1);


