use myntra;

Alter table mk_customization_orientation modify start_x float(11,2) ;
Alter table mk_customization_orientation modify start_y float(11,2) ;
Alter table mk_customization_orientation modify size_x float(11,2) ;
Alter table mk_customization_orientation modify size_y float(11,2) ;

