ALTER TABLE mk_courier_service ADD COLUMN return_supported varchar(1) NOT NULL default 'N';

update mk_courier_service set return_supported = 'Y' where code in ('BD', 'HD');
