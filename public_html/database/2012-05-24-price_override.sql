insert into mk_email_notification(name,
	subject,body) values('price_override','Price Mismatch Refund for Order ID [ORDER_ID]','
	Dear [USER],<br><br>
	We have spent the last three and a half years building Myntra.com as a customer-centric company by always putting you first. While processing your Order No. [ORDER_ID], we noticed that the price published on our website for the following item(s) is higher than what is reflected on the price tag. This has compromised your shopping experience at Myntra.com and we apologize.<br><br>
	Please find the details of the item(s) below:<br>
	[ITEM_DETAILS]
	<p>We would like to make it up to you by refunding Rs. [REFUND_AMOUNT] into your Myntra cash-back account. You can use the cash-back to shop at our website anytime you choose to.
        [MYNT_CREDIT_URL]<br/><br/>
	You, our customer, are our focus and your experience takes precedence over everything else. It is the key principle of Myntra.com and we would like to reassure you that we would continue to remain invested in offering the best shopping experience for Fashion and Lifestyle products. We hope that you continue to shop with us and treat this as an aberration to the quality standards we strive to achieve. <br>
	For any clarification, kindly contact us at +91-80-43541999 (Call us).</p><br/>
	Regards,<br/>
	Myntra Customer Connect<br/><br/>
	');
