insert into mk_abtesting_tests(name,seg_algo,source_type,enabled) values('couponAutoApplyTest','abtest\\algos\\impl\\RandomSegmentationAlgo','tpl','1');
insert into mk_abtesting_variations (ab_test_id,name,percent_probability) values ((select id from mk_abtesting_tests where name='couponAutoApplyTest'),'apply',75);
insert into mk_abtesting_variations (ab_test_id,name,percent_probability) values ((select id from mk_abtesting_tests where name='couponAutoApplyTest'),'control',25);
