DROP TABLE IF EXISTS `user_group_map`;
DROP TABLE IF EXISTS `user_group`; 
DROP TABLE IF EXISTS `user_notification_map`;
DROP TABLE IF EXISTS `user_notification`;

CREATE TABLE `user_notification` (
      `id` bigint(20) NOT NULL AUTO_INCREMENT,
      `title` varchar(45) DEFAULT NULL,
      `message` varchar(255) DEFAULT NULL,
      `image` varchar(255) DEFAULT NULL,
      `start_date` datetime NOT NULL,
      `end_date` datetime NOT NULL,
      `created_by` VARCHAR(255) NULL ,
      `created_on` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      `last_modified_on` datetime DEFAULT NULL,
      PRIMARY KEY (`id`)
);

CREATE  TABLE `user_notification_map` (
      `id` bigint(20) NOT NULL AUTO_INCREMENT ,
      `user_notification_id` bigint(20) NOT NULL ,
      `user_id` VARCHAR(255) NOT NULL ,
      `read` tinyint(1) NOT NULL DEFAULT 0,
      `created_by` VARCHAR(255) DEFAULT NULL ,
      `created_on` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      `last_modified_on` datetime DEFAULT NULL,
      PRIMARY KEY (`id`) ,
      INDEX `fk_user_notification_map_1_idx` (`user_notification_id` ASC) ,
      INDEX `fk_user_notification_map_2_idx` (`user_notification_id` ,`user_id`) ,
      CONSTRAINT `fk_user_notification_map_1` FOREIGN KEY (`user_notification_id` ) REFERENCES `user_notification` (`id` )
);

CREATE TABLE `user_group` (
      `id` bigint(20) NOT NULL AUTO_INCREMENT,
      `title` varchar(45) DEFAULT NULL,
      `type` varchar(10) DEFAULT NULL,
      `description` varchar(255) DEFAULT NULL,
      `created_by` VARCHAR(255) NULL ,
      `created_on` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      `last_modified_on` datetime DEFAULT NULL,
      PRIMARY KEY (`id`)
);


CREATE TABLE `user_group_map` (
      `id` bigint(20) NOT NULL AUTO_INCREMENT,
      `user_group_id` bigint(20) NOT NULL,
      `user_id` varchar(255) NOT NULL,
      `created_by` VARCHAR(255) NULL ,
      `created_on` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      `last_modified_on` datetime DEFAULT NULL,
      PRIMARY KEY (`id`),
      KEY `fk_user_group_map_1_idx` (`user_group_id`),
      CONSTRAINT `fk_user_group_map_1` FOREIGN KEY (`user_group_id`) REFERENCES `user_group` (`id`)
);
