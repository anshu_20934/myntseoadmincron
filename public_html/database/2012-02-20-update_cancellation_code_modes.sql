alter table cancellation_codes drop column is_part_cancellation, drop key occ_unq_key;

update cancellation_codes set cancellation_mode = 'FULL_ORDER_CANCELLATION' where cancellation_mode = 'FULL_CANCELLATION';

delete from cancellation_codes where cancel_type = 'CCC' and cancel_reason = 'RTO';
delete from cancellation_codes where cancel_type = 'CCC' and cancel_reason = 'RTOCR';

update cancellation_codes set cancellation_mode = 'ITEM_CANCELLATION' where cancellation_mode = 'PART_CANCELLATION';

insert into cancellation_codes (cancel_type, cancel_type_desc, cancel_reason, cancel_reason_desc, email_content, last_updated_time, cancellation_mode) values ('CCC','CC Cancellation', 'OOSC','OOS Cancellation', 'We have spent the last three and a half years building Myntra.com as a customer-centric company by always putting you first. Due to overwhelming demand, we have had delays in completing some items of your order number [ORDER_ID] owing to stock issues. This has compromised your shopping experience at Myntra.com and we apologize.', now(), 'PART_ORDER_CANCELLATION');

insert into cancellation_codes (cancel_type, cancel_type_desc, cancel_reason, cancel_reason_desc, email_content, last_updated_time, cancellation_mode) values ('CCR','Customer Request', 'DDC','Delayed Delivery Cancellation', 'Thank you for contacting Myntra Support. As per your request, we are cancelling items from your Order No. [ORDER_ID].', now(), 'PART_ORDER_CANCELLATION');

insert into cancellation_codes (cancel_type, cancel_type_desc, cancel_reason, cancel_reason_desc, email_content, last_updated_time, cancellation_mode) values ('CCR','Customer Request', 'ISOC','Incorrect size ordered', 'Thank you for contacting Myntra Support. As per your request, we are cancelling items from your Order No. [ORDER_ID].', now(), 'PART_ORDER_CANCELLATION');

insert into cancellation_codes (cancel_type, cancel_type_desc, cancel_reason, cancel_reason_desc, email_content, last_updated_time, cancellation_mode) values ('CCR','Customer Request', 'DOC','Duplicate Order', 'Thank you for contacting Myntra Support. As per your request, we are cancelling items from your Order No. [ORDER_ID].', now(), 'PART_ORDER_CANCELLATION');

insert into cancellation_codes (cancel_type, cancel_type_desc, cancel_reason, cancel_reason_desc, email_content, last_updated_time, cancellation_mode) values ('CCR','Customer Request', 'PNRA','Product not required anymore', 'Thank you for contacting Myntra Support. As per your request, we are cancelling items from your Order No. [ORDER_ID].', now(), 'PART_ORDER_CANCELLATION');

insert into cancellation_codes (cancel_type, cancel_type_desc, cancel_reason, cancel_reason_desc, email_content, last_updated_time, cancellation_mode) values ('CCR','Customer Request', 'CIOC','Cash Issue', 'Thank you for contacting Myntra Support. As per your request, we are cancelling items from your Order No. [ORDER_ID].', now(), 'PART_ORDER_CANCELLATION');

insert into cancellation_codes (cancel_type, cancel_type_desc, cancel_reason, cancel_reason_desc, email_content, last_updated_time, cancellation_mode) values ('CCR','Customer Request', 'OBM', 'Ordered by mistake','Thank you for contacting Myntra Support. As per your request, we are cancelling items from your Order No. [ORDER_ID].', now(), 'PART_ORDER_CANCELLATION');

insert into cancellation_codes (cancel_type, cancel_type_desc, cancel_reason, cancel_reason_desc, email_content, last_updated_time, cancellation_mode) values ('CCR','Customer Request', 'FO','Fraud Order', 'Thank you for contacting Myntra Support. As per your request, we are cancelling items from your Order No. [ORDER_ID].', now(), 'PART_ORDER_CANCELLATION');

insert into cancellation_codes (cancel_type, cancel_type_desc, cancel_reason, cancel_reason_desc, email_content, last_updated_time, cancellation_mode) values ('CCR','Customer Request', 'WTCS','Wants to change style/color', 'Thank you for contacting Myntra Support. As per your request, we are cancelling items from your Order No. [ORDER_ID].', now(), 'PART_ORDER_CANCELLATION');


