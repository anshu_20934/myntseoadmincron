-- MySQL dump 10.13  Distrib 5.1.50, for Win32 (ia32)
--
-- Host: localhost    Database: myntra
-- ------------------------------------------------------
-- Server version	5.1.50-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `search_synonym_table`
--

DROP TABLE IF EXISTS `search_synonym_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_synonym_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `original` varchar(45) DEFAULT NULL,
  `corrected` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `original_UNIQUE` (`original`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_synonym_table`
--

LOCK TABLES `search_synonym_table` WRITE;
/*!40000 ALTER TABLE `search_synonym_table` DISABLE KEYS */;
INSERT INTO `search_synonym_table` VALUES (20,'ladies','womens'),(21,'niike','nike'),(22,'fast track','fastrack'),(23,'leecooper','lee cooper'),(24,'wood land','woodland'),(25,'rbk','reebok'),(26,'flipflops','flip flops'),(27,'football boots','football shoes'),(28,'under garments','underwear'),(29,'under wear','underwear'),(30,'play boy','playboy'),(31,'leecoper','lee cooper'),(32,'forceindia','force india'),(33,'balck','black'),(34,'waistcoat','waist coat'),(35,'sun glasses','sunglasses'),(36,'spadling','spalding'),(37,'wotch','watch'),(38,'newbalance','new balance'),(39,'jwelery','jewellery'),(40,'jewelery','jewellery'),(41,'hush pupies','hush puppies'),(42,'gucchi','gucci'),(43,'foot ball','ootball'),(44,'basket ball','basketball'),(45,'suspemders','suspenders'),(46,'chappals','sandals'),(47,'sportsbra','sports bra'),(48,'deodorant','deo'),(49,'soccer','football'),(50,'satyapaul','satya paul'),(51,'salwar kameez dupatta','salwar suit'),(52,'skd','salwar suit'),(53,'new feel','newfeel'),(54,'mtv','m tv'),(55,'half pant','shorts'),(56,'half trouser','shorts'),(57,'goggle','sunglasses'),(58,'shooes','shoes'),(59,'shoos','shoes');
/*!40000 ALTER TABLE `search_synonym_table` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-02-22 18:47:35
