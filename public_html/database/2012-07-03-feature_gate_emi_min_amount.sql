delete from mk_feature_gate_key_value_pairs where `key` = 'payments.iciciMinEMITotalAmount';

insert into mk_feature_gate_key_value_pairs(`key`,`value`,description) values('payments.icici6MONTHMinEMITotalAmount','500','EMI Charge for ICICI 6 months emi');

insert into mk_feature_gate_key_value_pairs(`key`,`value`,description) values('payments.icici3MONTHMinEMITotalAmount','0','EMI Charge for ICICI 3 months emi');