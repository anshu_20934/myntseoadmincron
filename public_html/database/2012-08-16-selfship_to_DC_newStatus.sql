insert into mk_return_status_details (code,display_name,mymyntra_status,mymyntra_msg,end_state,cust_end_state) values 
('RADC','Returns Accepted at DC','ACCEPTED','Your returns have been accepted by us. You will be issued a refund on this item within 24 hours.',0,0), 
('RJDC','Returns Rejected at DC','REJECTED','Your returns request has been declined as it did not meet  our <a href="[GUIDE_LINES_LINK]" onclick="_gaq.push([\'_trackEvent\', \'myreturns\', \'view_returns_policy\', \'click\']);" target="_blank">Returns Policy</a>. The item will be shipped back to you.',0,0),
('CPDC','CC Approved Return at DC','ACCEPTED','Your return has been accepted by us. You will be issued a refund on this item within 24 hours.',0,0),
('CFDC','CC Declined Returns at DC','REJECTED','Your return request has been declined as it did not meet our <a href="[GUIDE_LINES_LINK]" onclick="_gaq.push([\'_trackEvent\', \'myreturns\', \'view_returns_policy\', \'click\']);" target="_blank">Returns Policy</a>. We will contact you soon regarding this.',0,0);

insert into mk_widget_key_value_pairs (`key`, `value`, description) values
('selfship.dccodes','[{"code":"AND","name":"Andheri"},{"code":"TLG","name":"Tollygunge"},{"code":"MHP","name":"Mahipalpur"}]','Delivery Centers where Self-shipped returns are accepted.');
