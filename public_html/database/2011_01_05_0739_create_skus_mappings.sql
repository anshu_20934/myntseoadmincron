CREATE TABLE `mk_skus` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(250) DEFAULT NULL,
 `code` varchar(20) DEFAULT NULL,
 `enabled` tinyint(1) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

CREATE TABLE `mk_styles_options_skus_mapping` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `style_id` INT(11) NOT NULL,
 `option_id` INT(11) DEFAULT NULL,
 `sku_id` INT(11) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

