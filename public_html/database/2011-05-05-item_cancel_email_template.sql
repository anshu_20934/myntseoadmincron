delete from mk_email_notification where name = 'item_cancel';

insert into mk_email_notification(name, subject, body) values('item_cancel', 'Cancelled Items from Order No. [ORDER_ID].', 
'Hello [USER], <br/><br/>
[CANCEL_REASON_TEXT]<br/><br/>
Updated order details:
<br/><br/>
[ORDER_DETAILS]
<br/><br/>
Your cancelled item details:
<br/><br/>
[CANCELLED_ORDER_DETAILS]
<br/>
<p>The refund amount of Rs [REFUND_AMOUNT] has been credited to your Myntra Club cashback account and you can view the same by <a href="[URL]">clicking here</a></p>
<p>We want you to know we are honoured to have you as our esteemed customer and we will do our best to meet your needs. Your continued patronage and suggestions are a vital part of our growth, and for that we are most grateful to you.</p>
<p>For any clarification, kindly contact us at [MYNTRA_CC_PHONE].</p>
<br/>
Regards,<br/>
Team Myntra'
);

