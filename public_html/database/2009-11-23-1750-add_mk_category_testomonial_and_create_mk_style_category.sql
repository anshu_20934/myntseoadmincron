use myntra;
DROP TABLE IF EXISTS `mk_category_testimonials`;
CREATE TABLE  `mk_category_testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `content` varchar(800) DEFAULT NULL,
  `is_publish` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `mk_style_category`;
CREATE TABLE  `mk_style_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catid` int(10) DEFAULT NULL,
  `styleid` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

insert into mk_category_testimonials(cat_id,name,content,is_publish) values(152, 'Rahul Sharma, Mysore', "There is a wide range of calendars with cool designs and various personalization options. So I could create a calendar just the way I wanted. Thanks to Myntra for a superb product.", 1);
insert into mk_category_testimonials(cat_id,name,content,is_publish) values(152, 'Praful, Mumbai', "I created a fabulous 2010 Wall Calendar. There is a collection of design templates to choose from and you can give it your personal touch by adding your own photo or design to it.", 1);
insert into mk_category_testimonials(cat_id,name,content,is_publish) values(152, 'Kavitha, Chandigarh', "The set of pocket calendars are compact and the best part is it can be personalized with a photo or design of your choice!", 1);
/* for desktop calendar	*/

insert into mk_category_testimonials(cat_id,name,content,is_publish) values(153, 'Priyanka, Delhi', "I created a personalized 2010 desktop calendar with a photo of my kids. The quality of the calendar is amazing and it's the perfect way to start the day on a great note!", 1);
insert into mk_category_testimonials(cat_id,name,content,is_publish) values(153, 'Rahul Sharma, Mysore', "There is a wide range of calendars with cool designs and various personalization options. So I could create a calendar just the way I wanted. Thanks to Myntra for a superb product.", 1);
insert into mk_category_testimonials(cat_id,name,content,is_publish) values(153, 'Manisha, Bangalore', "I love your wooden desktop calendar! It's truly unique and I could personalize it as well!", 1);

/* for wall calendar*/
insert into mk_category_testimonials(cat_id,name,content,is_publish) values(154, 'Renuka, Chennai', "The wall calendar from Myntra is the best way way to start off the year 2010. It is large, beautifully designed and I had it personalized with my photo.", 1);
insert into mk_category_testimonials(cat_id,name,content,is_publish) values(154, 'Praful, Mumbai ', "I created a fabulous 2010 Wall Calendar. There is a collection of design templates to choose from and you can give it your personal touch by adding your own photo or design to it.", 1);
insert into mk_category_testimonials(cat_id,name,content,is_publish) values(154, 'Rajan, Cochin', "I have been very pleased with the ease with which I could create my wall calendar on Myntra. It was so simple and the best part was I could add my personal touch it. It was unlike the normal calendars I am used to seeing and is a wonderful product.", 1);


/* for Personal Calendar */
insert into mk_category_testimonials(cat_id,name,content,is_publish) values(155, 'Bharathi, New Delhi', "The mug calendar is indeed unique! It has the 2010 calendar printed on it and the mug can be personalized with with a photo as well!", 1);
insert into mk_category_testimonials(cat_id,name,content,is_publish) values(155, 'Dheeraj, Pune', "The greeting card cum calendar is practical yet beautiful. It's a novel product which can be personalized and is an ideal choice for gifting.", 1);
insert into mk_category_testimonials(cat_id,name,content,is_publish) values(155, 'Kavitha, Chandigarh', "The set of pocket calendars are compact and the best part is it can be personalized with a photo or design of your choice!", 1);

insert into mk_style_category(catid,styleid) values(153,939);
insert into mk_style_category(catid,styleid) values(153,938);
insert into mk_style_category(catid,styleid) values(153,30);
insert into mk_style_category(catid,styleid) values(153,863);
insert into mk_style_category(catid,styleid) values(153,943);
insert into mk_style_category(catid,styleid) values(153,278);


insert into mk_style_category(catid,styleid) values(154,937);
insert into mk_style_category(catid,styleid) values(154,940);
insert into mk_style_category(catid,styleid) values(154,941);
insert into mk_style_category(catid,styleid) values(154,942);


insert into mk_style_category(catid,styleid) values(155,40);
insert into mk_style_category(catid,styleid) values(155,936);
