insert into mk_abtesting_tests (name,ga_slot,seg_algo,source_type, enabled)
values('homeRecentlyViewed','','abtest\\algos\\impl\\RandomSegmentationAlgo','tpl', true);

insert into mk_abtesting_variations (name,ab_test_id,percent_probability) 
values('control',(select id from mk_abtesting_tests where name='homeRecentlyViewed'),100);
insert into mk_abtesting_variations (name,ab_test_id,percent_probability) 
values('test',(select id from mk_abtesting_tests where name='homeRecentlyViewed'),0);
