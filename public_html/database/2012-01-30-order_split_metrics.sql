CREATE TABLE mk_shipping_cost_metrics (
	id 			int(5) NOT NULL AUTO_INCREMENT,
	warehouse_id 		int(5) NOT NULL,
	state_zipcode_prefix 	int(2) NOT NULL,
	shipping_cost 		decimal(2,2) default 0.00,
	delivery_time		decimal(2,2) default 0.00,

	PRIMARY KEY (id)

);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 11, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 11, 0.00 ,0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 12, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 12, 0.00 ,0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 13, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 13, 0.00 ,0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 14, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 14, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 15, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 15, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 16, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 16, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 17, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 17, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 18, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 18, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 19, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 19, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 20, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 20, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 21, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 21, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 22, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 22, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 23, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 23, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 24, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 24, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 25, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 25, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 26, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 26, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 27, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 27, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 28, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 28, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 30, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 30, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 31, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 31, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 32, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 32, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 33, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 33, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 34, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 34, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 36, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 36, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 37, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 37, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 38, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 38, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 39, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 39, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 40, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 40, 1.00, 1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 41, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 41, 1.00, 1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 42, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 42, 1.00, 1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 43, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 43, 1.00, 1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 44, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 44, 1.00, 1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 45, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 45, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 46, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 46, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 47, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 47, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 48, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 48, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 49, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 49, 0.00, 0.00);


INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 50, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 50, 1.00 ,1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 51, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 51, 1.00 ,1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 52, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 52, 1.00 ,1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 53, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 53, 1.00 ,1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 56, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 56, 1.00 ,1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 57, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 57, 1.00 ,1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 58, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 58, 1.00 ,1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 59, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 59, 1.00 ,1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 60, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 60, 1.00 ,1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 61, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 61, 1.00 ,1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 62, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 62, 1.00 ,1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 63, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 63, 1.00 ,1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 64, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 64, 1.00 ,1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 67, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 67, 1.00 ,1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 68, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 68, 1.00 ,1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 69, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 69, 1.00 ,1.00);


INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 70, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 70, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 71, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 71, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 72, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 72, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 73, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 73, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 74, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 74, 0.00, 0.00);


INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 75, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 75, 1.00 ,1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 76, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 76, 1.00 ,1.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 77, 0.00, 0.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 77, 1.00 ,1.00);


INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 78, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 78, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 79, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 79, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 80, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 80, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 81, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 81, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 82, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 82, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 83, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 83, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 84, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 84, 0.00, 0.00);

INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (1, 85, 1.00, 1.00);
INSERT into mk_shipping_cost_metrics (warehouse_id, state_zipcode_prefix, shipping_cost, delivery_time) VALUES (2, 85, 0.00, 0.00);

