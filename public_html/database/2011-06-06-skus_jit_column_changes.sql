ALTER TABLE mk_skus add column jit_sourced int(1) default 0;

INSERT INTO mk_inventory_movement_reasons (text, action_type, inv_update, wh_update, is_for_system, code, action_mode_manual) values ('Enable JIT Sourced', 'N', 0, 0, 0, 'JITE', 1);

INSERT INTO mk_inventory_movement_reasons (text, action_type, inv_update, wh_update, is_for_system, code, action_mode_manual) values ('Disable JIT Sourced', 'N', 0, 0, 0, 'JITD', 1);

