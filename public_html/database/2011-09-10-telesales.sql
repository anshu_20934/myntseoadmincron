
DROP TABLE IF EXISTS telesales_logs;
create table telesales_logs (orderid int,cclogin varchar(45),action varchar(10),lastmodified timestamp default CURRENT_TIMESTAMP, key icall_orderid_index (orderid),key icall_cclogin_index (cclogin));


insert into mk_abtesting_tests (name,ga_slot,seg_algo,source_type) values('telesales','','abtest\\algos\\impl\\RandomSegmentationAlgo','tpl');
insert into mk_abtesting_variations (name,ab_test_id,percent_probability) values('shownumber',(select id from mk_abtesting_tests where name ='telesales'),60);
insert into mk_abtesting_variations (name,ab_test_id,percent_probability) values('control',(select id from mk_abtesting_tests where name ='telesales'),40);
insert into mk_roles(name,type,description,allowed_actions) values ('Telesales Customer Service','TCS','Role for Telesales Customer Service','telesalescustomerservice');


