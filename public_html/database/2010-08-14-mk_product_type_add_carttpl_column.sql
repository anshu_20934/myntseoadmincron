use myntra
alter table mk_product_type add column cart_display_template varchar(200) DEFAULT 'add_to_cart.tpl' ;
update mk_product_type set cart_display_template = 'add_to_cart_select.tpl' where id = 141;
alter table mk_product_options modify column price double(10, 2) ;
