-- 3/4 length pants
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (210,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(210,'{default:default}','skin1/myntra_images/sizechart/sizechart_capris_v5.png');

-- booties
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (202,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(202,'{default:default}','skin1/myntra_images/sizechart/sizecharts_feet_v1.png');


-- boxers
update mk_style_image_rules set ageGroup = null where article_type_id = 139;

-- bra
update mk_style_image_rules set ageGroup = null where article_type_id = 153;

-- briefs kids
insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(137,'{default:default}','skin1/myntra_images/sizechart/sizecharts_feet_v1.png');

-- camisoles
insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(141,'{default:default}','skin1/myntra_images/sizechart/Innerwear-Vest.png');

-- churidar already
-- coats
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (81,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(81,'{default:default}','skin1/myntra_images/sizechart/sizechart_jacket_v4.png');

-- harem pants
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (256,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(256,'{default:default}','skin1/myntra_images/sizechart/sizechart_formal-trouser_v5.png');

-- hygene panties
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (274,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(274,'{default:default}','skin1/myntra_images/sizechart/Briefs.png');

-- jeggings
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (249,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(249,'{default:default}','skin1/myntra_images/sizechart/sizechart_leggings_chudidar_v1.png');

-- leggings
insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(142,'{default:default}','skin1/myntra_images/sizechart/Lingerie-Set.png');

-- lounge shorts 
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (197,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(197,'{default:default}','skin1/myntra_images/sizechart/sizechart_shorts_v5.png');

-- maternity bra

insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (361,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(361,'{default:default}','skin1/myntra_images/sizechart/Bras.png');

-- maternity briefs
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (362,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(362,'{default:default}','skin1/myntra_images/sizechart/Panties.png');

-- padded briefs
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (357,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(357,'{default:default}','skin1/myntra_images/sizechart/Panties.png');

-- patiyala
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (184,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(184,'{default:default}','skin1/myntra_images/sizechart/sizechart_formal-trouser_v5.png');

-- perfume panties
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (276,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(276,'{default:default}','skin1/myntra_images/sizechart/Panties.png');

-- period panties
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (275,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(275,'{default:default}','skin1/myntra_images/sizechart/Panties.png');

-- rain jacket
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (271,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(271,'{default:default}','skin1/myntra_images/sizechart/sizechart_jacket_v4.png');

-- salwar
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (255,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(255,'{default:default}','skin1/myntra_images/sizechart/sizechart_formal-trouser_v5.png');

-- shrug
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (273,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(273,'{default:default}','skin1/myntra_images/sizechart/sizechart_jacket_v4.png');

-- sports bra
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (151,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(151,'{default:default}','skin1/myntra_images/sizechart/Bras.png');

-- sports sandals
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (283,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(283,'{default:default}','skin1/myntra_images/sizechart/sizecharts_feet_v1.png'); 

-- girls stockings
insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(145,'{default:default}','skin1/myntra_images/sizechart/sizechart_leggings_chudidar_v1.png');

-- swimwear
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (75,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(75,'{default:default}','skin1/myntra_images/sizechart/Briefs.png');

-- thermal bottoms
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (354,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(354,'{default:default}','skin1/myntra_images/sizechart/sizechart_trackpants_v5.png');

-- trunk
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (307,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(307,'{default:default}','skin1/myntra_images/sizechart/Briefs.png');

-- waistcoat
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (133,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(133,'{default:default}','skin1/myntra_images/sizechart/sizechart_jacket_v4.png');

-- windbreaker
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (270,'default','default',1);

insert into mk_style_image_rules (article_type_id,attribute_string,image_url) values(270,'{default:default}','skin1/myntra_images/sizechart/sizechart_jacket_v4.png');

-- innerwear t-shirts

insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (193,'Sleeve','Short Sleeve',1);

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (193,'{Sleeve:Long Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_fullsleeve_v1.png','Adults-Men');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (193,'{Sleeve:Long Sleeve}','skin1/myntra_images/sizechart/sizechart_women_tshirt_fullsleeve_v1.png','Adults-Women');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (193,'{Sleeve:Long Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_fullsleeve_v1.png','Kids-Boys');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (193,'{Sleeve:Long Sleeve}','skin1/myntra_images/sizechart/sizechart_women_tshirt_fullsleeve_v1.png','Kids-Girls');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (193,'{Sleeve:Long Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_fullsleeve_v1.png','Kids-Unisex');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (193,'{Sleeve:Short Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_halfsleeve_v1.png','Adults-Men');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (193,'{Sleeve:Short Sleeve}','skin1/myntra_images/sizechart/sizechart_women_tshirt_halfsleeve_v1.png','Adults-Women');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (193,'{Sleeve:Short Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_halfsleeve_v1.png','Kids-Boys');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (193,'{Sleeve:Short Sleeve}','skin1/myntra_images/sizechart/sizechart_women_tshirt_halfsleeve_v1.png','Kids-Girls');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (193,'{Sleeve:Short Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_halfsleeve_v1.png','Kids-Unisex');


INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (193,'{Sleeve:Long Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_fullsleeve_v1.png','Adults-Unisex');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (193,'{Sleeve:Short Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_halfsleeve_v1.png','Adults-Unisex');


-- lounge top
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (282,'Sleeve','Short Sleeve',1);

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (282,'{Sleeve:3/4th Sleeve}','skin1/myntra_images/sizechart/sizechart_women_3quarter_top_v5.png','Adults-Women');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (282,'{Sleeve:Long Sleeve}','skin1/myntra_images/sizechart/sizechart_women_full_top_v5.png','Adults-Women');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (282,'{Sleeve:Short Sleeve}','skin1/myntra_images/sizechart/sizechart_women_cap_top_v5.png','Adults-Women');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (282,'{Sleeve:Sleeveless}','skin1/myntra_images/sizechart/sizechart_women_sleeveless_top_v5.png','Adults-Women');


-- maternity sleepwear
insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (363,'Sleeve','Short Sleeve',1);

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (363,'{Sleeve:3/4th Sleeve}','skin1/myntra_images/sizechart/sizechart_women_kurta_dress_3quartersleeve_v1.png','Adults-Women');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (363,'{Sleeve:Long Sleeve}','skin1/myntra_images/sizechart/sizechart_women_kurta_dress_fullsleeve_v1.png','Adults-Women');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (363,'{Sleeve:Short Sleeve}','skin1/myntra_images/sizechart/sizechart_women_kurta_dress_halfsleeve_v1.png','Adults-Women');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (363,'{Sleeve:Sleeveless}','skin1/myntra_images/sizechart/sizechart_women_kurta_dress_sleeveless_v1.png','Adults-Women');

-- nightdress

insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (201,'Sleeve','Short Sleeve',1);


INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (201,'{Sleeve:3/4th Sleeve}','skin1/myntra_images/sizechart/sizechart_women_kurta_dress_3quartersleeve_v1.png','Adults-Women');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (201,'{Sleeve:Long Sleeve}','skin1/myntra_images/sizechart/sizechart_women_kurta_dress_fullsleeve_v1.png','Adults-Women');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (201,'{Sleeve:Short Sleeve}','skin1/myntra_images/sizechart/sizechart_women_kurta_dress_halfsleeve_v1.png','Adults-Women');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (201,'{Sleeve:Sleeveless}','skin1/myntra_images/sizechart/sizechart_women_kurta_dress_sleeveless_v1.png','Adults-Women');

-- thermal tops

insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (353,'Sleeve','Short Sleeve',1);

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (353,'{Sleeve:Long Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_fullsleeve_v1.png','Adults-Men');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (353,'{Sleeve:Long Sleeve}','skin1/myntra_images/sizechart/sizechart_women_tshirt_fullsleeve_v1.png','Adults-Women');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (353,'{Sleeve:Long Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_fullsleeve_v1.png','Kids-Boys');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (353,'{Sleeve:Long Sleeve}','skin1/myntra_images/sizechart/sizechart_women_tshirt_fullsleeve_v1.png','Kids-Girls');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (353,'{Sleeve:Long Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_fullsleeve_v1.png','Kids-Unisex');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (353,'{Sleeve:Short Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_halfsleeve_v1.png','Adults-Men');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (353,'{Sleeve:Short Sleeve}','skin1/myntra_images/sizechart/sizechart_women_tshirt_halfsleeve_v1.png','Adults-Women');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (353,'{Sleeve:Short Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_halfsleeve_v1.png','Kids-Boys');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (353,'{Sleeve:Short Sleeve}','skin1/myntra_images/sizechart/sizechart_women_tshirt_halfsleeve_v1.png','Kids-Girls');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (353,'{Sleeve:Short Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_halfsleeve_v1.png','Kids-Unisex');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (353,'{Sleeve:Long Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_fullsleeve_v1.png','Adults-Unisex');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (353,'{Sleeve:Short Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_halfsleeve_v1.png','Adults-Unisex');

-- tshirt combo pack

insert into mk_style_image_fields (article_type_id,attribute_field,defaultValue,internalOrder) values (179,'Sleeve','Short Sleeve',1);

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (179,'{Sleeve:Long Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_fullsleeve_v1.png','Adults-Men');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (179,'{Sleeve:Long Sleeve}','skin1/myntra_images/sizechart/sizechart_women_tshirt_fullsleeve_v1.png','Adults-Women');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (179,'{Sleeve:Long Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_fullsleeve_v1.png','Kids-Boys');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (179,'{Sleeve:Long Sleeve}','skin1/myntra_images/sizechart/sizechart_women_tshirt_fullsleeve_v1.png','Kids-Girls');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (179,'{Sleeve:Long Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_fullsleeve_v1.png','Kids-Unisex');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (179,'{Sleeve:Short Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_halfsleeve_v1.png','Adults-Men');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (179,'{Sleeve:Short Sleeve}','skin1/myntra_images/sizechart/sizechart_women_tshirt_halfsleeve_v1.png','Adults-Women');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (179,'{Sleeve:Short Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_halfsleeve_v1.png','Kids-Boys');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (179,'{Sleeve:Short Sleeve}','skin1/myntra_images/sizechart/sizechart_women_tshirt_halfsleeve_v1.png','Kids-Girls');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (179,'{Sleeve:Short Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_halfsleeve_v1.png','Kids-Unisex');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (179,'{Sleeve:Long Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_fullsleeve_v1.png','Adults-Unisex');

INSERT INTO mk_style_image_rules (article_type_id,attribute_string,image_url,ageGroup) VALUES (179,'{Sleeve:Short Sleeve}','skin1/myntra_images/sizechart/sizechart_men_tshirts_halfsleeve_v1.png','Adults-Unisex');


