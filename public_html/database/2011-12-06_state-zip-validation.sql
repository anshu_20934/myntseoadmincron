-- Portal-1436: Kundan: Add a field in address table to flag the rows which have some or the other forms of validation errors
-- At present only the Least Significant Bit of the errmask field is used to store if there's a validation error in zip-code corresponding to state selection
-- In order to select the error-mask, we should do: select errmask+0 from mk_customer_address [<where clause>]. If we don't add +0 it shows 30 (don't know why!)
-- In future, more validation errors can be saved in different bit positions
alter table mk_customer_address add (errmask bit(8) default b'0' not null);
