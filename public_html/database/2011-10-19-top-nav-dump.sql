-- MySQL dump 10.13  Distrib 5.1.54, for redhat-linux-gnu (x86_64)
--
-- Host: myntra.cmyzvjiswfdq.ap-southeast-1.rds.amazonaws.com    Database: myntra_20110627
-- ------------------------------------------------------
-- Server version	5.1.57-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mk_widget_top_nav`
--

DROP TABLE IF EXISTS `mk_widget_top_nav`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mk_widget_top_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `link_name` varchar(50) NOT NULL,
  `link_url` varchar(100) DEFAULT NULL,
  `display_category` varchar(255) DEFAULT '',
  `styleid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=647 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mk_widget_top_nav`
--

LOCK TABLES `mk_widget_top_nav` WRITE;
/*!40000 ALTER TABLE `mk_widget_top_nav` DISABLE KEYS */;
INSERT INTO `mk_widget_top_nav` VALUES (4,-1,1,'FOR HIM','/mens','',NULL),(34,4,2,'WHAT\'S NEW','','accessories',6565),(176,34,1,'Fastrack Watches','/mens-accessories-fastrack-watches#!sortby=RECENCY','',NULL),(224,34,2,'Nike Bags','/mens-accessories-nike-bags#!sortby=RECENCY','',NULL),(225,34,3,'Reid & Taylor Wallets','/mens-accessories-reid-and-taylor-wallets#!sortby=RECENCY','',NULL),(257,34,4,'Reid & Taylor Ties','/mens-accessories-reid-and-taylor-ties#!sortby=RECENCY','',NULL),(575,568,2,'Lee Cooper Shoes','/mens-footwear-lee-cooper','undefined',0),(576,568,3,'Asics','/mens-footwear-asics','undefined',0),(572,567,4,'Adidas','/mens-footwear-adidas','undefined',0),(573,567,3,'FILA','/mens-footwear-FILA','undefined',0),(574,567,5,'Reebok','/mens-footwear-reebok','undefined',0),(569,567,2,' Puma','/mens-footwear-puma','undefined',0),(570,568,1,'Timberland Shoes','/mens-footwear-timberland','undefined',0),(399,4,1,'BRANDS','/tshirt-brands','accessories',10368),(400,399,2,'Fastrack','/mens-accessories-fastrack','',NULL),(401,399,1,'Wildcraft','/mens-accessories-wildcraft','',NULL),(402,-1,2,'FOR HER','/womens','',NULL),(403,402,1,'BRANDS','/sports-footwear-brands','accessories',10137),(404,-1,3,'KIDS','/kids','',NULL),(405,404,1,'BRANDS','/casual-footwear-brands','',13387),(407,403,1,'Puma','/womens-accessories-puma','',NULL),(408,403,2,'Adidas','/womens-accessories-adidas','',NULL),(625,4,10,'WHAT\'S NEW','','casual wear',7277),(410,403,3,'Reebok','/womens-accessories-reebok','',NULL),(411,403,4,'Decathlon','/womens-accessories-decathlon','',NULL),(634,405,9,'Gini and Jony','/kids-gini-and-jony','undefined',0),(633,631,2,'Nike','/womens-footwear-nike','undefined',0),(415,405,1,'Puma','/kids-puma','',NULL),(416,405,2,'Adidas','/kids-adidas','',NULL),(417,405,3,'Disney','/kids-disney','',NULL),(418,405,4,'Doodle','/kids-doodle','',NULL),(420,405,5,'Ant','/kids-ant','',NULL),(421,405,6,'Levis Kids','/kids-levis-kids','',NULL),(422,405,7,'Palm Tree','/kids-palm-tree','',NULL),(568,4,4,'WHAT\'S NEW','','footwear',1649),(567,4,3,'BRANDS','','footwear',1649),(624,609,6,'Flying Machine','/mens-casual-wear-flying-machine','undefined',0),(432,405,8,'Crocs','/kids-crocs','',NULL),(571,567,1,'Nike','/mens-footwear-nike','undefined',0),(458,399,4,'Reid & Taylor','/mens-accessories-reid-and-taylor','',NULL),(459,399,3,'Carrera','/mens-accessories-carrera','',NULL),(460,399,5,'ESPIRIT','/mens-accessories-espirit','',NULL),(631,402,7,'BRANDS','','footwear',0),(582,568,5,'Numero Uno','/mens-footwear-numero-uno','undefined',0),(577,568,4,'New Balance','/mens-footwear-new-balance','undefined',0),(578,4,5,'BRANDS','','formal wear',11163),(579,4,6,'WHAT\'S NEW','','formal wear',11163),(580,578,1,'Reid & Taylor','/mens-formal-wear-reid-and-taylor','undefined',0),(581,579,1,'Genesis','/mens-formal-wear-genesis','undefined',0),(632,631,1,'Puma','/womens-footwear-puma','undefined',0),(562,402,2,'WHAT\'S NEW','','accessories',10137),(563,562,1,'ESPRIT','/womens-accessories-esprit','undefined',0),(564,562,2,'Carrera','/womens-accessories-carrera','undefined',0),(565,404,2,'WHAT\'S NEW','','',13387),(566,565,4,'Ant','/kids-ant','undefined',0),(609,4,7,'BRANDS','','casual wear',7277),(584,567,6,'Clarks','/mens-footwear-clarks','undefined',0),(585,567,7,'Red Tape','/mens-footwear-red-tape','undefined',0),(586,402,3,'BRANDS','','ethnic wear',12111),(587,402,4,'WHAT\'S NEW','','ethnic wear',12111),(588,586,1,'W','/womens-ethnic-wear-w','undefined',0),(589,586,2,'Vishudh','/womens-ethnic-wear-vishudh','undefined',0),(590,586,3,'AURELIA','/womens-ethnic-wear-aurelia','undefined',0),(591,586,4,'Mother Earth','/womens-ethnic-wear-mother-earth','undefined',0),(592,587,1,'W','/womens-ethnic-wear-w','undefined',0),(593,587,2,'Vishudh','/womens-ethnic-wear-vishudh','undefined',0),(594,587,3,'AURELIA','/womens-ethnic-wear-aurelia','undefined',0),(595,587,4,'Mother Earth','/womens-ethnic-wear-mother-earth','undefined',0),(596,402,5,'BRANDS','','casual wear',12126),(597,402,6,'WHAT\'S NEW','','casual wear',12126),(598,596,1,'UCB','/womens-casual-wear-united-colors-of-benetton','undefined',0),(599,596,2,'Jealous 21','/womens-casual-wear-jealous-21','undefined',0),(600,578,2,'Mark Taylor','/mens-formal-wear-mark-taylor','undefined',0),(601,596,3,'Lee','/womens-casual-wear-lee','undefined',0),(602,596,4,'Wrangler','/womens-casual-wear-wrangler','undefined',0),(603,578,3,'Indigo Nation','/mens-formal-wear-indigo-nation','undefined',0),(604,596,5,'SCULLERS FOR HER','/womens-casual-wear-scullers-for-her','undefined',0),(605,597,1,'UCB','/womens-casual-wear-united-colors-of-benetton','undefined',0),(606,578,4,'John Miller','/mens-formal-wear-john-miller','undefined',0),(607,597,2,'Inkfruit','/womens-casual-wear-inkfruit','undefined',0),(608,597,3,'Flying Machine','/womens-casual-wear-flying-machine','undefined',0),(610,609,1,'United Colors of Benetton','/mens-casual-wear-united-colors-of-benetton','undefined',0),(611,609,2,'Wrangler','/mens-casual-wear-wrangler','undefined',0),(612,609,3,'Lee','/mens-casual-wear-lee','undefined',0),(613,4,8,'BRANDS','','sports wear',13113),(614,4,9,'WHAT\'S NEW','','sports wear',13113),(615,613,1,'Puma','/mens-sports-wear-puma','undefined',0),(616,613,2,'Nike','/mens-sports-wear-nike','undefined',0),(617,613,3,'Adidas','/mens-sports-wear-adidas','undefined',0),(618,613,4,'Reebok','/mens-sports-wear-reebok','undefined',0),(619,614,1,'Nike','/mens-sports-wear-nike','undefined',0),(620,609,4,'Indigo Nation','/mens-casual-wear-indigo-nation','undefined',0),(621,614,2,'Adidas','/mens-sports-wear-adidas','undefined',0),(623,609,5,'Spykar','/mens-casual-wear-spykar','undefined',0),(626,625,1,'Classic Polo','/mens-casual-wear-classic-polo','undefined',0),(627,403,5,'ESPRIT','/womens-accessories-esprit','undefined',0),(628,625,2,'Indian Terrain','/mens-casual-wear-indian-terrain','undefined',0),(629,403,6,'Carrera','/womens-accessories-carrera','undefined',0),(630,562,3,'Nike','/womens-accessories-nike','undefined',0),(635,631,3,'Reebok','/womens-footwear-reebok','undefined',0),(636,565,1,'Timberland','/kids-timberland','undefined',0),(637,631,4,'Adidas','/womens-footwear-adidas','undefined',0),(638,565,2,'Palm Tree','/kids-palm-tree','undefined',0),(639,565,3,'Gini and Jony','/kids-gini-and-jony','undefined',0),(640,402,8,'WHAT\'S NEW','','footwear',0),(641,402,9,'BRANDS','','sports-wear',0),(642,641,1,'Puma','/womens-sports-wear-puma','undefined',0),(643,641,2,'Adidas','/womens-sports-wear-adidas','undefined',0),(644,641,3,'Reebok','/womens-sports-wear-reebok','undefined',0),(645,641,4,'Nike','/womens-sports-wear-nike','undefined',0),(646,402,10,'WHAT\'S NEW','','sports-wear',0);
/*!40000 ALTER TABLE `mk_widget_top_nav` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-10-20  9:11:05
