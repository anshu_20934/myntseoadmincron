delete from mk_email_notification where name='cognizantBackDatedEmails';
insert into mk_email_notification (name,subject,body) values
('cognizantBackDatedEmails','Order Reconfirmation',"Dear [ASSOCIATE_NAME],\n<p>Your order has been placed successfully.<br/>The order number is [ORDER_ID]. The expected date of delivery is latest by 22nd Feb 2011.\nThe details of the order are<br/><br/>
<h3> Order Confirmation </h3>
<div>
<span style='font-weight:bold;'>Associate Information</span>
</div>
<table width='100%'>
<tr><td width='30%'><span style='font-size:10pt'>Associate Name</span></td> <td><span style='font-size:10pt;color:#420420'>[ASSOCIATE_NAME]</span></td></tr>
<tr><td width='30%'> <span style='font-size:10pt'>Associate Email</span></td> <td><span style='font-size:10pt;color:#420420'>[ASSOCIATE_EMAIL]</span></td></tr>
<tr><td width='30%'> <span style='font-size:10pt'>Associate Id</span></td> <td><span style='font-size:10pt;color:#420420'>[ASSOCIATE_ID]</span></td></tr>
<tr><td width='30%'> <span style='font-size:10pt'>Associate Mobile</span></td> <td><span style='font-size:10pt;color:#420420'>[ASSOCIATE_MOBILE]</span></td></tr>
</table><br/>[PRODUCT_DETAIL_TABLE]<br/>Total Product Price : Rs. [TOTAL_PRODUCT_PRICE]/-<br/>Total Discount : Rs. [TOTAL_DISCOUNT]/-<br/>Amount to be paid : Rs. [AMOUNT_TO_BE_PAID]/-<br/>
<b>Terms & Conditions</b>
<li>Associates are advised that any purchase made or any activity undertaken hereunder shall purely be a voluntary action on the part of the associate. Cognizant shall not be liable for any purchases made or information disclosed by the associates through this campaign or related portal. This campaign shall be subject to any terms and conditions notified by Cognizant.</li>
<li>This program is currently open only for associates in India during the course of this program. This program would not be applicable for India Associates who are currently onsite.</li>
<li>All deductions would be done in February month's India payroll of the concerned associate and would not be done in installments. It is the associate's responsibility to ensure that the purchases are within his / her Net Salary.</li>
<li>It is the responsibility of the individual associates to track their deductions in the payroll.</li>
<li>All deductions are made on the basis of inputs received from Program Partners (Nike). In case of any discrepancy in amounts or merchandise, Cognizant will not be responsible for the same.</li>
<br/><br/>
<b>In case of any discrepancy, request you to write to support@myntra.com.</b>");