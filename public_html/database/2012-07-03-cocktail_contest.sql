USE myntra;

CREATE TABLE mk_cocktail_contest (
	orderid int NOT NULL,
	login varchar(255),
	PRIMARY KEY (orderid),
	CONSTRAINT fk_PerOrders FOREIGN KEY (orderid)
	REFERENCES xcart_orders(orderid)
);

INSERT INTO mk_widget_key_value_pairs (`key`, `value`, description) VALUES ('promo.cocktail.enabled','1','Cocktail Contest Switch');
INSERT INTO mk_widget_key_value_pairs (`key`, `value`, description) VALUES ('promo.cocktail.imgUrl','https://d6kkp3rk1o2kk.cloudfront.net/skin2/images/Cocktail.jpg','URL for uploaded banner image 564px X 120px');
INSERT INTO mk_widget_key_value_pairs (`key`, `value`, description) VALUES ('promo.cocktail.mainMsg','Participate in the <a href="/cocktail">Myntra.com - Cocktail contest</a><br />and stand a chance to meet the "Cocktail" star - Diana Penty!','Main message for contest');
INSERT INTO mk_widget_key_value_pairs (`key`, `value`, description) VALUES ('promo.cocktail.subMsg','Also win exciting movie merchandise daily!','Sub Message for contest');
INSERT INTO mk_widget_key_value_pairs (`key`, `value`, description) VALUES ('promo.cocktail.thanksMsg','Thank you for participating in the<br /><a href="/cocktail">Myntra.com - Cocktail contest</a>','Thank You Message for contest');
INSERT INTO mk_widget_key_value_pairs (`key`, `value`, description) VALUES ('promo.cocktail.winMsg','You will receive an email from us if you are a lucky winner!','Winner Message for contest');
INSERT INTO mk_widget_key_value_pairs (`key`, `value`, description) VALUES ('promo.cocktail.confMsg','You have confirmed your participation in the<br /><a href="/cocktail">Myntra.com - Cocktail contest</a> with a previous order','Already confirmed message for contest');
INSERT INTO mk_widget_key_value_pairs (`key`, `value`, description) VALUES ('promo.cocktail.confSubMsg','This Order will add to your chances of winning.','Already confirmed sub-message for contest');

