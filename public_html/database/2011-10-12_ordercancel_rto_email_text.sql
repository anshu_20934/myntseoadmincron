delete from mk_order_action_reasons where action = 'order_cancel' and reason = 'RTO';

insert into mk_order_action_reasons (action, reason, reason_display_name, email_contents) values ('order_cancel', 'RTO', 'Return To Origin', 'We regret to inform you that our shipping partner was unable to reach you at the shipping address and/or contact number provided by you and therefore could not make the delivery and cancelling the order.');

insert into mk_order_action_reasons (action, reason, reason_display_name, email_contents) values ('order_cancel', 'RTOCR', 'Returned To Origin', 'We regret to inform you that our shipping partner was unable to reach you at the shipping address and/or contact number provided by you and therefore could not make the delivery. On our consequent conversation with you, and as per your request, we are processing the cancellation of the order [ORDER_ID]');

