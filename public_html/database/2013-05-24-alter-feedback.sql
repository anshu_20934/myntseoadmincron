/*alter sqls for feedback*/
alter table mk_feedback add unique key name_UNIQUE(name);
alter table mk_feedback add column title text after name;
alter table mk_feedback_instance add column reference_type varchar(100) after feedback_reference;

/*update sr feedback mail*/
update mk_email_notification set subject='Request for feedback on Myntra Customer Service', body='<div style="border: 1px solid gray; padding: 6px">
Hi [USER], <br/><br/>
Thank you for contacting Myntra customer service. Hope your concern has been adequately addressed. Please take a few minutes of your valuable time and tell us more about your experience :<br/><br/>
[SR_MFB_FORM]<br/>[REDIRECT_INFO]<br/>
<p>Thanks for your participation!</p>
Sincerely,<br/>
The Myntra Team
<br/><br/>[DECLARATION]<br/>
</div>' where name='voc_SR';

/*
insert into mk_email_notification(name,subject,body,enabled) values('voc_order_may2013','Request your feedback for website experience',
'<div style="border: 1px solid gray; padding: 6px">
Hi [USER], <br/><br/>
Please share your feedback on your order placement experience by filling the short survey at the end of this email.<br/>
Thank you for shopping with Myntra.com. Please take a few minutes of your valuable time and tell us more about your experience.<br/><br/>
[MFB_FORM]<br/>
[REDIRECT_INFO]<br/>
<p>For any further clarification, kindly contact us at [MYNTRA_CC_PHONE].</p>Regards,
<br/>Myntra Customer Connect<br/>
India\'s Largest Online Fashion Store<br/><br/>
[DECLARATION]<br/>
</div>',1);


insert into mk_email_notification(name,subject,body,enabled) values('MFB_survey','Survey for [FEEDBACK_TITLE]',
'<br/>
User Email : [USER]<br/>
Feedback Reference Id: [REFERENCE_ID]<br/>
[MFB_SURVEY]<br/>
Regards,<br/>
Myntra Feedback Engine<br/><br/>',1);
*/

/*website experience feedback*/
insert into mk_feedback(name,title,created_date,emailto,survey_email,active) values('order','Please share feedback on Myntra Website',unix_timestamp(),'arun.kumar@myntra.com','arun.kumar@myntra.com',1);


insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='order'),'Based on your experience on our website, how likely are you to recommend Myntra to your friends and family:', 'radio',1,1,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='order'),'How satisfied were you with your experience on Myntra website?', 'radio',2,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='order'),'How satisfied were you with the selection of products available on our website?', 'radio',3,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='order'),'How satisfied were you with the product descriptions and imagery used on our website?', 'radio',4,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='order'),'How easy was it for you to find the product you were looking for?', 'radio',5,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='order'),'How easy was it for you to pay for and complete your order?', 'radio',6,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='order'),'Based on your experience on our website, how likely are you to buy with us again?', 'radio',7,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='order'),'Please tell us if you have any other feedback on our website:', 'text',8,0,1);


insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience on our website, how likely are you to recommend Myntra to your friends and family:'),'0<br/>Very unlikely',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience on our website, how likely are you to recommend Myntra to your friends and family:'),'1',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience on our website, how likely are you to recommend Myntra to your friends and family:'),'2',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience on our website, how likely are you to recommend Myntra to your friends and family:'),'3',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience on our website, how likely are you to recommend Myntra to your friends and family:'),'4',0,5,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience on our website, how likely are you to recommend Myntra to your friends and family:'),'5',0,6,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience on our website, how likely are you to recommend Myntra to your friends and family:'),'6',0,7,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience on our website, how likely are you to recommend Myntra to your friends and family:'),'7',0,8,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience on our website, how likely are you to recommend Myntra to your friends and family:'),'8',0,9,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience on our website, how likely are you to recommend Myntra to your friends and family:'),'9',0,10,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience on our website, how likely are you to recommend Myntra to your friends and family:'),'10<br/>Extremely likely',0,11,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with your experience on Myntra website?'),'Very dissatisfied',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with your experience on Myntra website?'),'Dissatisfied',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with your experience on Myntra website?'),'Neither satisfied nor dissatisfied',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with your experience on Myntra website?'),'Satisfied',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with your experience on Myntra website?'),'Very satisfied',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the selection of products available on our website?'),'Very dissatisfied',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the selection of products available on our website?'),'Dissatisfied',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the selection of products available on our website?'),'Neither satisfied nor dissatisfied',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the selection of products available on our website?'),'Satisfied',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the selection of products available on our website?'),'Very satisfied',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the product descriptions and imagery used on our website?'),'Very dissatisfied',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the product descriptions and imagery used on our website?'),'Dissatisfied',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the product descriptions and imagery used on our website?'),'Neither satisfied nor dissatisfied',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the product descriptions and imagery used on our website?'),'Satisfied',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the product descriptions and imagery used on our website?'),'Very satisfied',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How easy was it for you to find the product you were looking for?'),'Very difficult',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How easy was it for you to find the product you were looking for?'),'Difficult',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How easy was it for you to find the product you were looking for?'),'Neither easy nor difficult',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How easy was it for you to find the product you were looking for?'),'Easy',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How easy was it for you to find the product you were looking for?'),'Very easy',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How easy was it for you to pay for and complete your order?'),'Very difficult',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How easy was it for you to pay for and complete your order?'),'Difficult',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How easy was it for you to pay for and complete your order?'),'Neither easy nor difficult',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How easy was it for you to pay for and complete your order?'),'Easy',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How easy was it for you to pay for and complete your order?'),'Very easy',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience on our website, how likely are you to buy with us again?'),'Very unlikely',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience on our website, how likely are you to buy with us again?'),'Unlikely',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience on our website, how likely are you to buy with us again?'),'Not sure',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience on our website, how likely are you to buy with us again?'),'Likely',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience on our website, how likely are you to buy with us again?'),'Very likely',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Please tell us if you have any other feedback on our website:'),null,0,1,500,1);


/*delivery feedback*/
insert into mk_feedback(name,title,created_date,emailto,survey_email,active) values('shipment','Please share feedback on your delivery experience',unix_timestamp(),'arun.kumar@myntra.com','arun.kumar@myntra.com',1);


insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='shipment'),'Based on your experience with Myntra order delivery, how likely you are to recommend Myntra to your friends and family:', 'radio',1,1,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='shipment'),'How satisfied were you with your order delivery from Myntra.com?', 'radio',2,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='shipment'),'How satisfied were you with the time it took for us to deliver your order?', 'radio',3,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='shipment'),'How clear was the communication regarding order delivery time?', 'radio',4,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='shipment'),'How did the delivery time compare with your expectations?', 'radio',5,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='shipment'),'How did you like the packaging of your order?', 'radio',6,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='shipment'),'How satisfied were you with the condition of the product(s) we delivered to you?', 'radio',7,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='shipment'),'Based on your experience with Myntra order delivery, how likely are you to buy with us again?', 'radio',8,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='shipment'),'Please tell us if you have any other feedback on order delivery by Myntra:', 'text',9,0,1);


insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra order delivery, how likely you are to recommend Myntra to your friends and family:'),'0<br/>Very unlikely',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra order delivery, how likely you are to recommend Myntra to your friends and family:'),'1',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra order delivery, how likely you are to recommend Myntra to your friends and family:'),'2',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra order delivery, how likely you are to recommend Myntra to your friends and family:'),'3',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra order delivery, how likely you are to recommend Myntra to your friends and family:'),'4',0,5,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra order delivery, how likely you are to recommend Myntra to your friends and family:'),'5',0,6,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra order delivery, how likely you are to recommend Myntra to your friends and family:'),'6',0,7,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra order delivery, how likely you are to recommend Myntra to your friends and family:'),'7',0,8,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra order delivery, how likely you are to recommend Myntra to your friends and family:'),'8',0,9,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra order delivery, how likely you are to recommend Myntra to your friends and family:'),'9',0,10,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra order delivery, how likely you are to recommend Myntra to your friends and family:'),'10<br/>Extremely likely',0,11,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with your order delivery from Myntra.com?'),'Very dissatisfied',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with your order delivery from Myntra.com?'),'Dissatisfied',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with your order delivery from Myntra.com?'),'Neither satisfied nor dissatisfied',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with your order delivery from Myntra.com?'),'Satisfied',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with your order delivery from Myntra.com?'),'Very satisfied',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the time it took for us to deliver your order?'),'Very dissatisfied',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the time it took for us to deliver your order?'),'Dissatisfied',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the time it took for us to deliver your order?'),'Neither satisfied nor dissatisfied',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the time it took for us to deliver your order?'),'Satisfied',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the time it took for us to deliver your order?'),'Very satisfied',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How clear was the communication regarding order delivery time?'),'Very confusing',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How clear was the communication regarding order delivery time?'),'Confusing',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How clear was the communication regarding order delivery time?'),'Neither clear nor confusing',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How clear was the communication regarding order delivery time?'),'Clear',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How clear was the communication regarding order delivery time?'),'Very clear',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How did the delivery time compare with your expectations?'),'Significantly missed expectations',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How did the delivery time compare with your expectations?'),'Did not meet expectations',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How did the delivery time compare with your expectations?'),'Met expectations',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How did the delivery time compare with your expectations?'),'Exceeded expectations',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How did the delivery time compare with your expectations?'),'Significantly exceeded expectations',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How did you like the packaging of your order?'),'Hated it',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How did you like the packaging of your order?'),'Disliked it',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How did you like the packaging of your order?'),'Neither liked nor disliked it',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How did you like the packaging of your order?'),'Liked it',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How did you like the packaging of your order?'),'Loved it',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the condition of the product(s) we delivered to you?'),'Very dissatisfied',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the condition of the product(s) we delivered to you?'),'Dissatisfied',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the condition of the product(s) we delivered to you?'),'Neither satisfied nor dissatisfied',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the condition of the product(s) we delivered to you?'),'Satisfied',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the condition of the product(s) we delivered to you?'),'Very satisfied',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra order delivery, how likely are you to buy with us'),'Very unlikely',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra order delivery, how likely are you to buy with us'),'Unlikely',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra order delivery, how likely are you to buy with us'),'Not sure',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra order delivery, how likely are you to buy with us'),'Likely',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra order delivery, how likely are you to buy with us'),'Very likely',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Please tell us if you have any other feedback on order delivery by Myntra:'),null,0,1,500,1);


/*retun feedback*/
insert into mk_feedback(name,title,created_date,emailto,survey_email,active) values('return','Please share feedback on your returns experience',unix_timestamp(),'arun.kumar@myntra.com','arun.kumar@myntra.com',1);


insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='return'),'Based on your experience with returns on Myntra, how likely you are to recommend Myntra to your friends and family:', 'radio',1,1,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='return'),'How satisfied were you with the returns experience on Myntra.com?', 'radio',2,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='return'),'How do you like Myntra\'s return policy?', 'radio',3,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='return'),'How easy was it to request for return of your product?', 'radio',4,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='return'),'How easy was it to physically return your product to us?', 'radio',5,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='return'),'How did the refund time compare with your expectations?', 'radio',6,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='return'),'Based on your experience with returns on Myntra, how likely are you to buy with us again?', 'radio',7,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='return'),'Please tell us if you have any other feedback on your returns experience:', 'text',8,0,1);


insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with returns on Myntra, how likely you are to recommend Myntra to your friends and family:'),'0<br/>Very unlikely',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with returns on Myntra, how likely you are to recommend Myntra to your friends and family:'),'1',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with returns on Myntra, how likely you are to recommend Myntra to your friends and family:'),'2',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with returns on Myntra, how likely you are to recommend Myntra to your friends and family:'),'3',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with returns on Myntra, how likely you are to recommend Myntra to your friends and family:'),'4',0,5,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with returns on Myntra, how likely you are to recommend Myntra to your friends and family:'),'5',0,6,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with returns on Myntra, how likely you are to recommend Myntra to your friends and family:'),'6',0,7,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with returns on Myntra, how likely you are to recommend Myntra to your friends and family:'),'7',0,8,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with returns on Myntra, how likely you are to recommend Myntra to your friends and family:'),'8',0,9,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with returns on Myntra, how likely you are to recommend Myntra to your friends and family:'),'9',0,10,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with returns on Myntra, how likely you are to recommend Myntra to your friends and family:'),'10<br/>Extremely likely',0,11,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the returns experience on Myntra.com?'),'Very dissatisfied',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the returns experience on Myntra.com?'),'Dissatisfied',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the returns experience on Myntra.com?'),'Neither satisfied nor dissatisfied',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the returns experience on Myntra.com?'),'Satisfied',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the returns experience on Myntra.com?'),'Very satisfied',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How do you like Myntra\'s return policy?'),'Hated it',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How do you like Myntra\'s return policy?'),'Disliked it',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How do you like Myntra\'s return policy?'),'Neither liked nor disliked it',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How do you like Myntra\'s return policy?'),'Liked it',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How do you like Myntra\'s return policy?'),'Loved it',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How easy was it to request for return of your product?'),'Very difficult',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How easy was it to request for return of your product?'),'Difficult',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How easy was it to request for return of your product?'),'Neither easy nor difficult',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How easy was it to request for return of your product?'),'Easy',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How easy was it to request for return of your product?'),'Very easy',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How easy was it to physically return your product to us?'),'Very difficult',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How easy was it to physically return your product to us?'),'Difficult',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How easy was it to physically return your product to us?'),'Neither easy nor difficult',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How easy was it to physically return your product to us?'),'Easy',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How easy was it to physically return your product to us?'),'Very easy',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How did the refund time compare with your expectations?'),'Significantly missed expectations',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How did the refund time compare with your expectations?'),'Did not meet expectations',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How did the refund time compare with your expectations?'),'Met expectations',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How did the refund time compare with your expectations?'),'Exceeded expectations',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How did the refund time compare with your expectations?'),'Significantly exceeded expectations',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with returns on Myntra, how likely are you to buy with us again'),'Very unlikely',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with returns on Myntra, how likely are you to buy with us again'),'Unlikely',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with returns on Myntra, how likely are you to buy with us again'),'Not sure',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with returns on Myntra, how likely are you to buy with us again'),'Likely',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with returns on Myntra, how likely are you to buy with us again'),'Very likely',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Please tell us if you have any other feedback on your returns experience:'),null,0,1,500,1);


/*sr feedback*/
insert into mk_feedback(name,title,created_date,emailto,survey_email,active) values('SR','Myntra customer service feedback',unix_timestamp(),'arun.kumar@myntra.com','arun.kumar@myntra.com',1);


insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='SR'),'Based on your experience with Myntra customer service, how likely you are to recommend Myntra to your friends and family:', 'radio',1,1,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='SR'),'How satisfied were you with the way Myntra solved your problem?', 'radio',2,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='SR'),'How satisfied were you with the solution we provided to your problem?', 'radio',3,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='SR'),'How did our issue resolution time compare with your expectations?', 'radio',4,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='SR'),'How clear was our communication with you through the issue resolution process?', 'radio',5,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='SR'),'Do you agree that we treated you as a valued customer through the issue resolution process?', 'radio',6,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='SR'),'Based on your experience with Myntra customer service, how likely are you to shop with us again?', 'radio',7,0,1);
insert into mk_feedback_question(feedbackid,question,question_type,display_order,mandatory,active) values((select feedbackid from mk_feedback where name='SR'),'Please tell us if you have any other feedback on your experience with Myntra Customer Service:', 'text',8,0,1);


insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra customer service, how likely you are to recommend Myntra to your friends and family:'),'0<br/>Very unlikely',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra customer service, how likely you are to recommend Myntra to your friends and family:'),'1',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra customer service, how likely you are to recommend Myntra to your friends and family:'),'2',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra customer service, how likely you are to recommend Myntra to your friends and family:'),'3',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra customer service, how likely you are to recommend Myntra to your friends and family:'),'4',0,5,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra customer service, how likely you are to recommend Myntra to your friends and family:'),'5',0,6,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra customer service, how likely you are to recommend Myntra to your friends and family:'),'6',0,7,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra customer service, how likely you are to recommend Myntra to your friends and family:'),'7',0,8,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra customer service, how likely you are to recommend Myntra to your friends and family:'),'8',0,9,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra customer service, how likely you are to recommend Myntra to your friends and family:'),'9',0,10,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra customer service, how likely you are to recommend Myntra to your friends and family:'),'10<br/>Extremely likely',0,11,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the way Myntra solved your problem?'),'Very dissatisfied',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the way Myntra solved your problem?'),'Dissatisfied',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the way Myntra solved your problem?'),'Neither satisfied nor dissatisfied',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the way Myntra solved your problem?'),'Satisfied',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the way Myntra solved your problem?'),'Very satisfied',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the solution we provided to your problem?'),'Very dissatisfied',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the solution we provided to your problem?'),'Dissatisfied',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the solution we provided to your problem?'),'Neither satisfied nor dissatisfied',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the solution we provided to your problem?'),'Satisfied',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How satisfied were you with the solution we provided to your problem?'),'Very satisfied',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How did our issue resolution time compare with your expectations?'),'Significantly missed expectations',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How did our issue resolution time compare with your expectations?'),'Did not meet expectations',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How did our issue resolution time compare with your expectations?'),'Met expectations',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How did our issue resolution time compare with your expectations?'),'Exceeded expectations',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How did our issue resolution time compare with your expectations?'),'Significantly exceeded expectations',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How clear was our communication with you through the issue resolution process?'),'Very confusing',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How clear was our communication with you through the issue resolution process?'),'Confusing',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How clear was our communication with you through the issue resolution process?'),'Neither clear nor confusing',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How clear was our communication with you through the issue resolution process?'),'Clear',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='How clear was our communication with you through the issue resolution process?'),'Very clear',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Do you agree that we treated you as a valued customer through the issue resolution process?'),'Strongly disagree',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Do you agree that we treated you as a valued customer through the issue resolution process?'),'Disagree',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Do you agree that we treated you as a valued customer through the issue resolution process?'),'Neither agree or disagree',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Do you agree that we treated you as a valued customer through the issue resolution process?'),'Agree',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Do you agree that we treated you as a valued customer through the issue resolution process?'),'Strongly agree',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra customer service, how likely are you to shop with us again?'),'Very unlikely',0,1,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra customer service, how likely are you to shop with us again?'),'Unlikely',0,2,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra customer service, how likely are you to shop with us again?'),'Not sure',0,3,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra customer service, how likely are you to shop with us again?'),'Likely',0,4,0,1);
insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Based on your experience with Myntra customer service, how likely are you to shop with us again?'),'Very likely',0,5,0,1);

insert into mk_feedback_option(questionid,option_value,option_default,display_order,maxchars,active) values((select questionid from mk_feedback_question where question='Please tell us if you have any other feedback on your experience with Myntra Customer Service:'),null,0,1,500,1);
/*==========*/
