alter table mk_mobile_code add column id int primary key auto_increment first;
create index mobile on mk_mobile_code(mobile);
create index login on mk_mobile_code(login);
create index mobile_login on mk_mobile_code(mobile,login);