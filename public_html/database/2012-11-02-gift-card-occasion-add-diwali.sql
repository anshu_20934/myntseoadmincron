INSERT INTO `gift_card_occasions`
(`name`,`subject`,`header`,`body`,`footer`,`preview_image`,`preview_image_thumbnail`,`email_preview_image`,`default_message`,`created_on`,`updated_on`,`updated_by`, 'display_order') VALUES
('Happy Diwali', 'Test', '', 'Mail Body 5', '', 'skin2/images/MyntraGiftCard_HappyDiwali.jpg', 'skin2/images/MyntraGiftCard_HappyDiwali_Thumb.jpg', '', 'May this festival of lights bring a sparkle to your wardrobe!', '2012-10-30 16:14:55', '2012-10-30 16:14:55', '', 1);

alter table gift_card_occasions add column display_order int(3) after name;

update gift_card_occasions set display_order=1 where id=1;
update gift_card_occasions set display_order=5 where id=2;
update gift_card_occasions set display_order=3 where id=3;
update gift_card_occasions set display_order=4 where id=4;
update gift_card_occasions set display_order=2 where id=5;

update gift_card_occasions set preview_image='skin2/images/MyntraGiftCard_HappyBirthday.jpg' where id=1;
update gift_card_occasions set preview_image_thumbnail='skin2/images/MyntraGiftCard_HappyBirthday_Thumb.jpg' where id=1;

update gift_card_occasions set preview_image='skin2/images/MyntraGiftCard_HappyAnniversary.jpg' where id=2;
update gift_card_occasions set preview_image_thumbnail='skin2/images/MyntraGiftCard_HappyAnniversary_Thumb.jpg' where id=2;

update gift_card_occasions set preview_image='skin2/images/MyntraGiftCard_BestWishes.jpg' where id=3;
update gift_card_occasions set preview_image_thumbnail='skin2/images/MyntraGiftCard_BestWishes_Thumb.jpg' where id=3;

update gift_card_occasions set preview_image='skin2/images/MyntraGiftCard_Congratulations.jpg' where id=4;
update gift_card_occasions set preview_image_thumbnail='skin2/images/MyntraGiftCard_Congratulations_Thumb.jpg' where id=4;