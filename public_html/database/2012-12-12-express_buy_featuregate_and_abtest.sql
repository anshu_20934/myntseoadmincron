insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('payments.expressbuy.enabled','false','true enables express buy feature');
insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('payments.service.PCI-DSS-Compliant','false','true enables express buy feature');

insert into mk_abtesting_tests (name,ga_slot,seg_algo,source_type)
values('ExpressBuyABTest','1','abtest\\algos\\impl\\RandomSegmentationAlgo','tpl');


insert into mk_abtesting_variations (name,ab_test_id,percent_probability) 
values('test',(select id from mk_abtesting_tests where name ='ExpressBuyABTest'),100);
insert into mk_abtesting_variations (name,ab_test_id,percent_probability) 
values('control',(select id from mk_abtesting_tests where name ='ExpressBuyABTest'),0);
