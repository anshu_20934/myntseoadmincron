
alter table mk_config add column `order` int after  `value`;

alter table mk_name_page add column parent_id int after  id;
alter table mk_name_page add column alignment varchar(8) after  parent_id;
alter table mk_name_page add column icon_style_image varchar(100) after  large_font_image;
alter table mk_name_page add column sprite_width_large int  after  coordinates_of_large_image;
alter table mk_name_page add column alt varchar(255) ;
alter table mk_name_page add column title varchar(255) ;
alter table mk_name_page add column additional varchar(255) ;



alter table mk_template_orientation_map add column image_allowed tinyint(4);
alter table mk_template_orientation_map add column image_cust_allowed tinyint(4);
alter table mk_template_orientation_map add column text_allowed tinyint(4);
alter table mk_template_orientation_map add column text_cust_allowed tinyint(4);