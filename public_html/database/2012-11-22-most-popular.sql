drop table if exists mk_widget_mostpopular;
CREATE TABLE `mk_widget_mostpopular` (   
    `id` int(11) NOT NULL AUTO_INCREMENT,   
    `parent_id` int(11) DEFAULT NULL,   
    `display_order` int(11) DEFAULT NULL,   
    `link_name` text,   `link_url` varchar(100) DEFAULT NULL,   
    PRIMARY KEY (`id`) 
) ENGINE=InnoDB AUTO_INCREMENT=473 DEFAULT CHARSET=latin1;
