alter table mk_tracking_history drop index idx_mk_tracking_history_event_value_int, drop column event_value_int;

alter table mk_tracking_history change event_value event_value int;
alter table mk_tracking_history add column event_value_extra varchar(200) default null after event_value;

