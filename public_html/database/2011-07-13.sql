/*=========================================================
	Changes for the filter ordering : Lohiya : Start
=========================================================*/

/*Procedure to create columns, it will first check for the columns and if exists then it will drop them*/
drop procedure if exists schema_change_alter_table;

delimiter ';;'
create procedure schema_change_alter_table() begin

 /* delete columns if they exist */
 if exists (select * from information_schema.columns where table_name = 'mk_catalogue_classification' and column_name = 'filter_order') then
  ALTER TABLE mk_catalogue_classification DROP COLUMN filter_order;
 end if;
 if exists (select * from information_schema.columns where table_name = 'mk_attribute_type_values' and column_name = 'filter_order') then
  ALTER TABLE mk_attribute_type_values DROP COLUMN filter_order;
 end if;
 
 /* add columns */
ALTER TABLE mk_catalogue_classification ADD COLUMN filter_order INT(11) DEFAULT 0  AFTER is_active;
ALTER TABLE mk_attribute_type_values ADD COLUMN filter_order INT(11) DEFAULT 0  AFTER attribute_type_id;
  
end;;

delimiter ';'
call schema_change_alter_table();

drop procedure if exists schema_change_alter_table;

/*Procedure ends*/

set @i=0;
update mk_catalogue_classification set filter_order = @i:= @i+1  where parent1 !=-1 and parent2 != -1 order by typename asc;

set @num := 0, @type := '';

UPDATE mk_attribute_type_values AS a
    LEFT JOIN (
        select  id , @num := if(@type = attribute_type_id, @num + 1, 1) as filter_order, @type := attribute_type_id as dummy 
	from mk_attribute_type_values order by attribute_type_id
    ) AS m 
      ON 
	m.id = a.id
SET
    a.filter_order = m.filter_order;


DROP TABLE IF EXISTS mk_display_categories;
CREATE TABLE mk_display_categories (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(100),
  category_rule TEXT,
  created_on INT(11) NULL,
  created_by VARCHAR(128) NULL,
  updated_on INT(11) NULL,
  updated_by VARCHAR(128) NULL,
  is_active TINYINT(1) DEFAULT 1,
  filter_order INT(11) DEFAULT 0,
  PRIMARY KEY (id)
);

truncate table mk_display_categories;
insert into mk_display_categories (name,filter_order) values
('Accessories',1), 
('Casual wear',2), 
('Casual Shoes',3),
('Sandals',4),
('Formal Shoes',5),
('Jerseys',6),
('Flipflops',7),
('Sports Shoes',8),
('T-Shirt',9);

/*=========================================================
	Changes for the filter ordering : Lohiya : End
=========================================================*/
