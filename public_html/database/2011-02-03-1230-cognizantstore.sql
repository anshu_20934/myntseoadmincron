CREATE TABLE `brand_store_locations` (
  `store_id` INT(11)  NOT NULL,
  `location_code` varchar(255)  NOT NULL,
  `s_address` varchar(255)  NOT NULL,
  `s_city` varchar(255)  NOT NULL,
  `s_pincode` varchar(255)  NOT NULL,
  `s_state` varchar(255)  NOT NULL,
  `s_country` varchar(255)  DEFAULT 'IN'
)
ENGINE = MyISAM;


insert into mk_email_notification (name,subject,body) values('cognizantStoreOrderConfirmation','Your order has been placed',"Dear [USER_NAME],<br/><p>Your order has been placed successfully.<br/>The order number is [ORDER_ID]. The expected date of delivery is [DELIVERY_DATE].<br/>The details of the order are :-<br/><br/>[ORDER_CONFIM_CONTENT]<br/><br/><b>Terms & Conditions</b><li>The participation in this purchase program is completely Voluntary.</li><li>This program is currently open only for associates in India during the course of this program. This program would not be applicable for India Associates who are currently onsite.</li><li>All deductions would be done in February month\'s India payroll of the concerned associate and would not be done in installments.   It is the associate�s responsibility to ensure that the purchases are within his / her Net Salary.</li><li>It is the responsibility of the individual associates to track their deductions in the payroll.</li><li>All deductions are made on the basis of inputs received from Program Partners (Nike). In case of any discrepancy in amounts or merchandise, Cognizant will not be responsible for the same.</li><br/><br/>Thank you for shopping on Myntra!<br/>www.myntra.com<br/><br/>* For any queries/suggestions, please call 9986059687.</p>");


insert into brand_store_details (username,passwd,store_name,store_code,store_email,contact_person,
s_city,s_pincode,s_address,s_state,s_mobilenumber,s_phonenumber,
brand_store_id,retail_partner_name,retail_partner_contact_telephone,
source_id,source_code,operations_location)
values('CTS-TEAM-INDIA-STORE','bd0e6915956aed7b1ae3f9a274a63502','Cognizant Associates','CTS-TEAM-INDIA-STORE','Sajid.Hussain@cognizant.com','Sajid Hussain',
'Bangalore','560102','Myntra 19th Main HSR Layout','KA','9986059687','91-80-43541901',
'1','','',
'1','CTS-TEAM-INDIA-STORE','1');

insert into brand_store_locations (store_id,location_code,s_address,s_city,s_pincode,s_state,s_country) values((select store_id from brand_store_details where username = 'CTS-TEAM-INDIA-STORE'), 'CTS Bangalore', '#1, 1/1 to 1/5, Silver Jubilee Block, Behind Unity Building Complex, 3rd Cross, Mission Road', 'Bangalore', '560027', 'KA', 'IN');
insert into brand_store_locations (store_id,location_code,s_address,s_city,s_pincode,s_state,s_country) values((select store_id from brand_store_details where username = 'CTS-TEAM-INDIA-STORE'), 'CTS Chennai', '#5/535, Old Mahabalipuram Road, Okkiam -Thoraipakkam', 'Chennai', '600096', 'TK', 'IN');
insert into brand_store_locations (store_id,location_code,s_address,s_city,s_pincode,s_state,s_country) values((select store_id from brand_store_details where username = 'CTS-TEAM-INDIA-STORE'), 'CTS Coimbatore', 'Mountain View Campus, STPI - IT Park, Kumaraguru College of Technology', 'Coimbatore', '641006', 'TK', 'IN');
insert into brand_store_locations (store_id,location_code,s_address,s_city,s_pincode,s_state,s_country) values((select store_id from brand_store_details where username = 'CTS-TEAM-INDIA-STORE'), 'CTS Cochin', 'Plot No. 1, Cochin Special Economic Zone, 3rd floor, Technopolis, Kakkanad', 'Cochin', '682037', 'KL', 'IN');
insert into brand_store_locations (store_id,location_code,s_address,s_city,s_pincode,s_state,s_country) values((select store_id from brand_store_details where username = 'CTS-TEAM-INDIA-STORE'), 'CTS Kolkata', 'Technocomplex, Plot GN-34/3, Sector-V, Saltlake Electronic Complex', 'Kolkata', '700019', 'WB', 'IN');
insert into brand_store_locations (store_id,location_code,s_address,s_city,s_pincode,s_state,s_country) values((select store_id from brand_store_details where username = 'CTS-TEAM-INDIA-STORE'), 'CTS Mumbai', '13th, Kensington SEZ,Hiranandani Business Park, Powai', 'Mumbai', '400076', 'MH', 'IN');
insert into brand_store_locations (store_id,location_code,s_address,s_city,s_pincode,s_state,s_country) values((select store_id from brand_store_details where username = 'CTS-TEAM-INDIA-STORE'), 'CTS Gurgaon', 'Signature Towers - RX - 508, 15th Floor Tower B,South City-1', 'Gurgaon', '122022', 'HR', 'IN');
insert into brand_store_locations (store_id,location_code,s_address,s_city,s_pincode,s_state,s_country) values((select store_id from brand_store_details where username = 'CTS-TEAM-INDIA-STORE'), 'CTS Hyderabad', '3rd Floor, H Building, Vanenburg IT Park, Software Units Layout, Madhapur', 'Hyderabad', '500081', 'AP', 'IN');
insert into brand_store_locations (store_id,location_code,s_address,s_city,s_pincode,s_state,s_country) values((select store_id from brand_store_details where username = 'CTS-TEAM-INDIA-STORE'), 'CTS Pune', 'Plot # 26, Rajiv Gandhi Infotech Park, MIDC, Hinjawadi', 'Pune', '411057', 'MH', 'IN');
