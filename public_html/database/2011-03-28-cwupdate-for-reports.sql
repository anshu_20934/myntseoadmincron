delete from mk_org_reports_map where orgid=25;

insert into mk_reports(name,createddate,groupid,query,mailto,report_type) values('CW Awards','1256638728','15',"select concat(c1.firstname,' ',c1.lastname) as receiving_colleague,concat(c2.firstname,'' '',c2.lastname) as gifting_colleague,

t2.description,tr.credit,tr.balance, tr.transaction_info as memo, tr.transaction_date as date 

from mk_svaccount_transactions tr, mk_customer_svaccounts cs1,mk_customer_svaccounts cs2, 

xcart_customers c1,xcart_customers c2,mk_svaccounts a2,mk_svaccount_types t2

where tr.transaction_reference_type like 'RR-ACCOUNT' and tr.account_id!=88 and tr.transaction_reference_number!=88 

and cs1.svaccount_id=tr.account_id and cs2.svaccount_id=tr.transaction_reference_number 

and c1.login=cs1.customer_login and c2.login=cs2.customer_login and tr.debit=0 and 

a2.account_id=cs2.svaccount_id and t2.type_code=a2.account_type

and t2.org_id = 25 and c2.orgid = 25

order by tr.transaction_date desc",'arun.kumar@myntra.com','Q');

insert into mk_reports(name,createddate,groupid,query,mailto,report_type) values('CW Orders','1256644703','15',"select concat(xc.firstname,' ',xc.lastname) as 'customer name',tr.transaction_reference_number as orderid,
tr.transaction_reference_amount as value,tr.balance,tr.transaction_date as 'order date'
from mk_svaccount_transactions tr,mk_customer_svaccounts cs,xcart_customers xc
where tr.transaction_reference_type like '%RR-ORDER%' and xc.login=cs.customer_login and tr.account_id=cs.svaccount_id and xc.orgid=25",'arun.kumar@myntra.com','Q');


insert into mk_org_reports_map (orgid,reportid) values ('25',(select id from mk_reports where name='CW Awards'));
insert into mk_org_reports_map (orgid,reportid) values ('25',(select id from mk_reports where name='CW Orders'));