insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('personalization.name.maxlen', 10,'max number of characters allowed for the custom name');
insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('personalization.number.maxlen', 2,'max number of characters allowed for the custom number');
insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('personalization.enabled', false,'the enable switch');
