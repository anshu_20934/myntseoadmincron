use myntra;

/*992*/
/* 1)update option price for style online*/
update mk_product_options set price = 895 where style='992';

/* 2)insert addon price for style and its options online*/
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('992','2352','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('992','2353','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('992','2354','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('992','2355','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('992','2356','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('992','2402','customized',399,1);


/* 3)insert option price for style pos*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('2352','39',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2353','39',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2354','39',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2355','39',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2356','39',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2402','39',895);

insert into mk_product_options_pos (id,shop_master_id,price) values ('2352','41',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2353','41',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2354','41',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2355','41',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2356','41',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2402','41',895);

insert into mk_product_options_pos (id,shop_master_id,price) values ('2352','47',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2353','47',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2354','47',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2355','47',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2356','47',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2402','47',895);

insert into mk_product_options_pos (id,shop_master_id,price) values ('2352','48',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2353','48',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2354','48',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2355','48',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2356','48',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2402','48',895);

/*993*/
/* 1)update option price for style online*/
update mk_product_options set price = 1495 where style='993';

/* 2)insert addon price for style and its options online*/
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('993','2359','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('993','2360','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('993','2361','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('993','2362','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('993','2363','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('993','2839','customized',399,1);


/* 3)insert option price for style pos*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('2359','39',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2360','39',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2361','39',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2362','39',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2363','39',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2839','39',1495);

insert into mk_product_options_pos (id,shop_master_id,price) values ('2359','41',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2360','41',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2361','41',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2362','41',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2363','41',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2839','41',1495);

insert into mk_product_options_pos (id,shop_master_id,price) values ('2359','47',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2360','47',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2361','47',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2362','47',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2363','47',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2839','47',1495);

insert into mk_product_options_pos (id,shop_master_id,price) values ('2359','48',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2360','48',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2361','48',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2362','48',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2363','48',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2839','48',1495);

/*994*/
/* 1)update option price for style online*/
update mk_product_options set price = 2295 where style='994';

/* 2)insert addon price for style and its options online*/
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('994','2364','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('994','2365','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('994','2366','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('994','2367','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('994','2368','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('994','2782','customized',399,1);


/* 3)insert option price for style pos*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('2364','39',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2365','39',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2366','39',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2367','39',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2368','39',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2782','39',2295);

insert into mk_product_options_pos (id,shop_master_id,price) values ('2364','41',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2365','41',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2366','41',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2367','41',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2368','41',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2782','41',2295);

insert into mk_product_options_pos (id,shop_master_id,price) values ('2364','47',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2365','47',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2366','47',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2367','47',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2368','47',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2782','47',2295);

insert into mk_product_options_pos (id,shop_master_id,price) values ('2364','48',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2365','48',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2366','48',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2367','48',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2368','48',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2782','48',2295);

/*1163*/
/* 1)update option price for style online*/
update mk_product_options set price = 895 where style='1163';

/* 2)insert addon price for style and its options online*/
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1163','2989','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1163','2990','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1163','2991','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1163','2992','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1163','2993','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1163','2994','customized',399,1);


/* 3)insert option price for style pos*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('2989','39',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2990','39',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2991','39',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2992','39',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2993','39',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2994','39',895);

insert into mk_product_options_pos (id,shop_master_id,price) values ('2989','41',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2990','41',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2991','41',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2992','41',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2993','41',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2994','41',895);

insert into mk_product_options_pos (id,shop_master_id,price) values ('2989','47',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2990','47',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2991','47',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2992','47',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2993','47',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2994','47',895);

insert into mk_product_options_pos (id,shop_master_id,price) values ('2989','48',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2990','48',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2991','48',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2992','48',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2993','48',895);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2994','48',895);

/*1164*/
/* 1)update option price for style online*/
update mk_product_options set price = 1495 where style='1164';

/* 2)insert addon price for style and its options online*/
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1164','2995','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1164','2996','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1164','2997','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1164','2998','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1164','2999','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1164','3000','customized',399,1);


/* 3)insert option price for style pos*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('2995','39',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2996','39',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2997','39',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2998','39',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2999','39',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3000','39',1495);

insert into mk_product_options_pos (id,shop_master_id,price) values ('2995','41',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2996','41',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2997','41',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2998','41',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2999','41',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3000','41',1495);

insert into mk_product_options_pos (id,shop_master_id,price) values ('2995','47',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2996','47',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2997','47',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2998','47',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2999','47',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3000','47',1495);

insert into mk_product_options_pos (id,shop_master_id,price) values ('2995','48',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2996','48',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2997','48',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2998','48',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('2999','48',1495);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3000','48',1495);

/*1165*/
/* 1)update option price for style online*/
update mk_product_options set price = 2295 where style='1165';

/* 2)insert addon price for style and its options online*/
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1165','3001','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1165','3002','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1165','3003','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1165','3004','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1165','3005','customized',399,1);
insert into mk_product_addons (style_id,product_option_id,name,price,is_active) values ('1165','3006','customized',399,1);


/* 3)insert option price for style pos*/
insert into mk_product_options_pos (id,shop_master_id,price) values ('3001','39',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3002','39',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3003','39',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3004','39',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3005','39',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3006','39',2295);

insert into mk_product_options_pos (id,shop_master_id,price) values ('3001','41',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3002','41',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3003','41',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3004','41',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3005','41',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3006','41',2295);

insert into mk_product_options_pos (id,shop_master_id,price) values ('3001','47',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3002','47',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3003','47',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3004','47',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3005','47',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3006','47',2295);

insert into mk_product_options_pos (id,shop_master_id,price) values ('3001','48',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3002','48',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3003','48',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3004','48',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3005','48',2295);
insert into mk_product_options_pos (id,shop_master_id,price) values ('3006','48',2295);
