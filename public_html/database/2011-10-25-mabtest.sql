alter table mk_abtesting_tests add column (ga_slot int(11) default null, filters varchar(255),seg_algo varchar(100),source_type varchar(45),completed tinyint(1) default 0, key (completed),key(name));
alter table mk_abtesting_variations add column (inline_html text,final_variant tinyint(1) default 0);
create table mk_abtest_audit_logs (id int(11) auto_increment,abtestid int(11),login varchar(100),lastmodified timestamp default current_timestamp,comment text,details text,jsondump text,
PRIMARY KEY(id),KEY audit_abtestf_key(abtestid),KEY audit_time(lastmodified),KEY audit_abtest_time(abtestid,lastmodified));

update mk_abtesting_tests set seg_algo='abtest\\algos\\impl\\RandomSegmentationAlgo', source_type='tpl';

insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('mabtest.testmode.enable','false','true enables test mode for MABTests');


insert into mk_abtesting_variations (ab_test_id,name) values ((select id from mk_abtesting_tests where name='sortorder'),'control' ); 
insert into mk_abtesting_variations (ab_test_id,name) values ((select id from mk_abtesting_tests where name='productui'),'control' ); 
insert into mk_abtesting_variations (ab_test_id,name) values ((select id from mk_abtesting_tests where name='megamenuabtest'),'control' ); 

