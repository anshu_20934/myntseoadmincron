CREATE TABLE mk_inv_vendor_master (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(100) DEFAULT NULL,
  code varchar(45) DEFAULT NULL, 
  PRIMARY KEY (id), 
  KEY mk_inv_vendor_master_code_index (code) 
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;


CREATE TABLE `mk_inventory_movement_reasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(100) DEFAULT NULL,
  `action_type` char(4) DEFAULT NULL,
  `inv_update` tinyint(1) DEFAULT NULL,
  `wh_update` tinyint(1) DEFAULT NULL,
  `is_for_system` tinyint(4) DEFAULT '0',
  `code` char(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mk_inventory_movement_reasons_index_code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

ALTER TABLE `mk_skus` ADD COLUMN `wh_count` int(11) DEFAULT '0' ;


CREATE TABLE `mk_skus_inv_manual_mod_logs` (
  `sku_log_id` int(11) DEFAULT NULL,
  `lot_id` varchar(45) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `mk_skus_inv_order_logs` (
  `sku_log_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `mk_skus_logs` 
 ADD COLUMN `id` int(11) NOT NULL AUTO_INCREMENT,
 ADD COLUMN `reasonid` int(11) DEFAULT NULL,
 ADD COLUMN `change_by` varchar(45) DEFAULT NULL,
 ADD COLUMN  `value` varchar(25) DEFAULT NULL,
 ADD COLUMN `extra_info` varchar(255) DEFAULT NULL,
 ADD INDEX `mk_skus_logs_index_skuid` (`skuid`,`reasonid`),
 ADD INDEX  `mk_skus_logs_index_reasonid` (`reasonid`),
 ADD INDEX  `mk_skus_logs_index_timestamp` (`lastmodified`),
 ADD PRIMARY KEY (`id`) ;




ALTER TABLE `mk_skus` ADD INDEX `mk_skus_code_index` (`code` ASC) ;
ALTER TABLE `mk_skus_inv_manual_mod_logs` 
ADD INDEX `mk_skus_inv_manual_mod_logs_index_sku_log_id` (`sku_log_id` ASC) 
, ADD INDEX `mk_skus_inv_manual_mod_logs_index_vendor_id` (`vendor_id` ASC) ;


ALTER TABLE `mk_skus_inv_order_logs` 
ADD INDEX `mk_skus_inv_order_logs_index_sku_logs_id` (`sku_log_id` ASC) 
, ADD INDEX `mk_skus_inv_order_logs_index_order_id` (`order_id` ASC) ;




update mk_skus set wh_count=inv_count ;

INSERT INTO `mk_inventory_movement_reasons` VALUES (1,'New Purchase','MI',1,1,0,'NEWP'),(2,'Rejects from QA','MI',1,1,0,'RJQA'),(3,'NP Returns from Customer','MI',1,1,0,'NPRC'),(4,'Inwards from Jobwork','MI',1,1,0,'INJB'),(5,'Others','MI',1,1,0,'OTHI'),(6,'Shrinkage','MD',1,1,0,'SHRI'),(7,'Issues to Ops','MD',0,1,0,'ISOP'),(8,'Return to vendor','MD',1,1,0,'RETV'),(9,'Issues for Jobwork','MD',1,1,0,'ISJB'),(10,'Re-issue to Ops','MD',1,1,0,'REIO'),(11,'Others','MD',1,1,0,'OTHD'),(12,'Order Placement','OPAD',1,0,1,'ODPL'),(13,'Order Cancel','OCAI',1,0,1,'ODCL'),(14,'Out of Stock Auto Disable','ADI',0,0,1,'OSAD'),(15,'In Stock Auto Enabled','AEN',0,0,1,'ISAE'),(16,'Manual Disable','MDI',0,0,0,'MAND'),(17,'Manual Enable','MEN',0,0,0,'MANE'),(18,'Manual Threshold Change','MTC',0,0,0,'MANT'),(19,'Manual Change Out of Stock Auto Disable','MCAD',0,0,1,'MCAD'),(20,'Manual Change In Stock Auto Enabled','MCAE',0,0,1,'MCAE');





insert into mk_inv_vendor_master(name,code) values('Adidas India','A001');
insert into mk_inv_vendor_master(name,code) values('ADPAC Industries','A002');
insert into mk_inv_vendor_master(name,code) values('Almats Branding Solutions Pvt Ltd','A003');
insert into mk_inv_vendor_master(name,code) values('AMIGO SPORT PVT LTD','A006');
insert into mk_inv_vendor_master(name,code) values('Anandha Knit Garments( B2C)','A004');
insert into mk_inv_vendor_master(name,code) values('ARYAN TRADING CORPORATIN ( B2C )','A008');
insert into mk_inv_vendor_master(name,code) values('VF Arvind Brands Pvt Ltd (B2C)','A007');
insert into mk_inv_vendor_master(name,code) values('Aquarian -B2C','A005');
insert into mk_inv_vendor_master(name,code) values('Batliboi Limited','B001');
insert into mk_inv_vendor_master(name,code) values('Bindurao Design Pvt.  Ltd','B002');
insert into mk_inv_vendor_master(name,code) values('Blue Ocean Technologies','B003');
insert into mk_inv_vendor_master(name,code) values('Bootsmall','B005');
insert into mk_inv_vendor_master(name,code) values('Brigade Prints','B004');
insert into mk_inv_vendor_master(name,code) values('Chandan Radio Electronics','C001');
insert into mk_inv_vendor_master(name,code) values('Carlton Overseas Pvt Ltd','C002');
insert into mk_inv_vendor_master(name,code) values('Catwalk Worldwide Pvt Ltd','C003');
insert into mk_inv_vendor_master(name,code) values('Darpan Enterprises(B2C)','D001');
insert into mk_inv_vendor_master(name,code) values('Decathlon Sports India Pvt Ltd','D002');
insert into mk_inv_vendor_master(name,code) values('Dilip Electronics (B2C)','D003');
insert into mk_inv_vendor_master(name,code) values('Dream Creators -B2C','D004');
insert into mk_inv_vendor_master(name,code) values('Ediots','E001');
insert into mk_inv_vendor_master(name,code) values('Finger Prints Fashions Pvt Ltd.','F001');
insert into mk_inv_vendor_master(name,code) values('GLOBAL TRENDZ LIMITED','G001');
insert into mk_inv_vendor_master(name,code) values('Hasbro Clothing Pvt Ltd','H006');
insert into mk_inv_vendor_master(name,code) values('Helios Packaging','H001');
insert into mk_inv_vendor_master(name,code) values('HF MetalArt Pvt Ltd','H002');
insert into mk_inv_vendor_master(name,code) values('Hindpac Industries','H003');
insert into mk_inv_vendor_master(name,code) values('Holosolv Marketing','H004');
insert into mk_inv_vendor_master(name,code) values('Hourglass Essentials Private Limited','H005');
insert into mk_inv_vendor_master(name,code) values('IImages Clothing Co. B2C','I001');
insert into mk_inv_vendor_master(name,code) values('Immpressions','I002');
insert into mk_inv_vendor_master(name,code) values('Impress India','I003');
insert into mk_inv_vendor_master(name,code) values('Impress Printal','I004');
insert into mk_inv_vendor_master(name,code) values('Interlace Knits (B2C)','I005');
insert into mk_inv_vendor_master(name,code) values('Jain Enterprises','J001');
insert into mk_inv_vendor_master(name,code) values('Jalan Brothers Pvt Ltd','J002');
insert into mk_inv_vendor_master(name,code) values('Janpan Mannequin Company','J003');
insert into mk_inv_vendor_master(name,code) values('Jazz- The Fashion Stop( Adidas)','J004');
insert into mk_inv_vendor_master(name,code) values('J.K.Traders','J005');
insert into mk_inv_vendor_master(name,code) values('J.N.Arora & Co. Pvt. Ltd','J006');
insert into mk_inv_vendor_master(name,code) values('JoyIfashion Enterprises','J007');
insert into mk_inv_vendor_master(name,code) values('KALPANA KRAFT','K002');
insert into mk_inv_vendor_master(name,code) values('Karizma (Cr)','K001');
insert into mk_inv_vendor_master(name,code) values('1 Kar Air NCC','K003');
insert into mk_inv_vendor_master(name,code) values('Manipal Press Ltd','M001');
insert into mk_inv_vendor_master(name,code) values('Matrix Sports Marketing Pvt. Ltd.','M002');
insert into mk_inv_vendor_master(name,code) values('Maxheap Technologies Private Limited','M003');
insert into mk_inv_vendor_master(name,code) values('Mohan Impressions Pvt Ltd(B2c)','M004');
insert into mk_inv_vendor_master(name,code) values('M and B Footwear Pvt  Ltd','M005');
insert into mk_inv_vendor_master(name,code) values('Navrang Caps (B2C)','N001');
insert into mk_inv_vendor_master(name,code) values('Nike India Pvt Ltd','N002');
insert into mk_inv_vendor_master(name,code) values('Nikhileshwari Designs Pvt Ltd','N003');
insert into mk_inv_vendor_master(name,code) values('Niranjan Graphics (B2C)','N004');
insert into mk_inv_vendor_master(name,code) values('Noble Enterprises','N005');
insert into mk_inv_vendor_master(name,code) values('Peacock Enterprises (B2C)','P001');
insert into mk_inv_vendor_master(name,code) values('Pleasantime Products','P002');
insert into mk_inv_vendor_master(name,code) values('Premium Overseas','P003');
insert into mk_inv_vendor_master(name,code) values('Prestige Compu- Wares','P004');
insert into mk_inv_vendor_master(name,code) values('Print Express','P005');
insert into mk_inv_vendor_master(name,code) values('Print Impressions(B2C)','P006');
insert into mk_inv_vendor_master(name,code) values('Printo Document Services Pvt Ltd','P007');
insert into mk_inv_vendor_master(name,code) values('Proton Plus(B2C)','P008');
insert into mk_inv_vendor_master(name,code) values('PUMA Sports India Pvt Ltd','P009');
insert into mk_inv_vendor_master(name,code) values('Puthur InfoTech Pvt Ltd','P010');
insert into mk_inv_vendor_master(name,code) values('Ramya Reprographic Pvt Ltd(B2C)','R001');
insert into mk_inv_vendor_master(name,code) values('Ravi Agencies(B2C)','R002');
insert into mk_inv_vendor_master(name,code) values('Reebok India Company','R003');
insert into mk_inv_vendor_master(name,code) values('Reebok Store','R004');
insert into mk_inv_vendor_master(name,code) values('Ritech Peripherals','R005');
insert into mk_inv_vendor_master(name,code) values('RJR Professional','R006');
insert into mk_inv_vendor_master(name,code) values('Rohin Industries (B2C)','R007');
insert into mk_inv_vendor_master(name,code) values('Royal Trading Company (B2C)','R008');
insert into mk_inv_vendor_master(name,code) values('Sagar Fab International (B2C)','S001');
insert into mk_inv_vendor_master(name,code) values('Sambhav Trading Co','S002');
insert into mk_inv_vendor_master(name,code) values('Samsonite South Asia Pvt Ltd','S003');
insert into mk_inv_vendor_master(name,code) values('SGS India Pvt. Ltd','S004');
insert into mk_inv_vendor_master(name,code) values('Shah Clock Agencies(B2C)','S005');
insert into mk_inv_vendor_master(name,code) values('Shah Marketing ( B2C)','S006');
insert into mk_inv_vendor_master(name,code) values('Shah Time Agencies (B2C)','S007');
insert into mk_inv_vendor_master(name,code) values('Shree Screens(B2C)','S008');
insert into mk_inv_vendor_master(name,code) values('Shrushti Fashions(B2C)','S009');
insert into mk_inv_vendor_master(name,code) values('Singapore-India Trade Resources','S010');
insert into mk_inv_vendor_master(name,code) values('Sparkle Innovations (B2C)','S011');
insert into mk_inv_vendor_master(name,code) values('Spinks India Pvt Ltd','S012');
insert into mk_inv_vendor_master(name,code) values('Sports Life Style Pvt. Ltd.','S001');
insert into mk_inv_vendor_master(name,code) values('Spring Orion Fashion (B2C)','S013');
insert into mk_inv_vendor_master(name,code) values('Sri Nisarga(B2C)','S014');
insert into mk_inv_vendor_master(name,code) values('Srinivas Fine Art (P) Ltd','S015');
insert into mk_inv_vendor_master(name,code) values('Srinivas Fine Arts P Ltd  (B2C)','S016');
insert into mk_inv_vendor_master(name,code) values('Sunrise Infotech(B2C)','S017');
insert into mk_inv_vendor_master(name,code) values('Surabhi Solutions(B2C)','S018');
insert into mk_inv_vendor_master(name,code) values('Team Work Apparels Pvt Ltd','T001');
insert into mk_inv_vendor_master(name,code) values('Tech Air Corporation','T002');
insert into mk_inv_vendor_master(name,code) values('TEC Solutions Inc.','T003');
insert into mk_inv_vendor_master(name,code) values('The Edge(B2C)','T004');
insert into mk_inv_vendor_master(name,code) values('THE VINOD JUNCTION (B2c)','T005');
insert into mk_inv_vendor_master(name,code) values('TMD Packaging Private Limited','T006');
insert into mk_inv_vendor_master(name,code) values('Total Wardrobe Solutions','T007'); 
insert into mk_inv_vendor_master(name,code) values('URBAN RETAIL DIVISION ( B2C )','U001');  
