use myntra;

insert into mk_courier_service values (NULL, 'FD', 'Fedex', '', 1, '', 25, 'order_shipped_confirmation', 1, '', 5, '');


update mk_courier_service set pickup_reports_template = 'name as "Contact Name", "Fedex" as "Company Name", address as "Address Line 1", "" as "Address Line 2", "" as "Remarks to courier", city as "City", state as "State", zipcode as "Postal Code", mobile as "Phone Number", "" as "Ready Time", "" as "Closing Time", "" as "Package Weight", returnid as "Reference Number"' where code = 'FD';   