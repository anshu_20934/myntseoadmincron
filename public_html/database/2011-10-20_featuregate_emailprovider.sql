insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('DefaultEmailProvider','myntra','Which Mail Provider to use by default: options: ses, myntra');
insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('BackupEmailProvider','ses','Which Mail Provider to use by if default one does not accept mail: options: ses, myntra');

insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('Mailers.CriticalTxn','ses,myntra','Mail service provider names for sending critical transactional Mails: comma separated string');
insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('Mailers.NonCriticalTxn','myntra','Mail service provider names for sending non-critical transactional Mails: comma separated string');
insert into mk_feature_gate_key_value_pairs(`key`,`value`,`description`) values('Mailers.Promotional','myntra','Mail service provider names for sending promotional Mails: comma separated string');

