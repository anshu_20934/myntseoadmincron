CREATE TABLE IF NOT EXISTS `analytics_keywords` (
  `keyword` varchar(512) NOT NULL,
  `medium` varchar(255) NOT NULL,
  `visitCount` int(11) NOT NULL,
  `transactionCount` int(11) NOT NULL,
  `itemCount` int(11) NOT NULL,
  `itemRevenue` float NOT NULL,
  `totalRevenue` float NOT NULL,
  KEY `keyword` (`keyword`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `serp_keywords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setid` int(11) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `urls` varchar(1024) NOT NULL,
  `serp` varchar(64) NOT NULL,
  `r_serp` varchar(99) DEFAULT NULL,
  `m_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `setid` (`setid`),
  KEY `keyword` (`keyword`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1000 ;

CREATE TABLE IF NOT EXISTS `serp_sets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `avg_serp` float DEFAULT NULL,
  `avg_visits` float NOT NULL,
  `avg_revenue` float NOT NULL,
  `keywordCount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1000 ;
