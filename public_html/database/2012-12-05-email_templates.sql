
update mk_email_notification set subject = 'Your Myntra.com Confirmation',body='<div style="border: 1px solid gray; padding: 6px">Hi [USER],&nbsp;<br/><br/>
Thank you for shopping at Myntra.com!<br/><br/>
We will deliver your order in the next 7 days. Once your order is shipped, we will send you an email with the courier tracking information and a confirmed delivery date.&nbsp;<br/><br/>
Order ID :<b> [ORDER_ID]</b><br/><br/>
[ORDER_DETAILS]<br/>
Shipped to:<br/>[DELIVERY_ADDRESS]<br/><br/>
[CASHBACK_SUMMARY]
<p>You can track or manage your order at any time by going to <a href="http://www.myntra.com/mymyntra.php?view=myorders">My Orders</a>. Please feel free to contact us at <a href="http://www.myntra.com/contactus">Contact Us</a> should you have any questions or need further assistance.</p>
We hope you enjoyed shopping at Myntra.com.<br/><br/>Thanks.<br/>
Myntra.com Team<br/>
India \'s Largest Online Fashion Store<br/><br/>[DECLARATION]<br/></div>' where name='order_confirmation';

update mk_email_notification set subject= 'Your order has been dispatched from the warehouse', body='Hi [USER],<br/><br/>
We would like to inform you that part of your Myntra.com order has been dispatched from our warehouse. You will receive this part of your order by&nbsp;<b>[DELIVERY_DATE]</b>.<br/><br/>
<b>Shipment Details</b><br/>
Order ID : [ORDER_ID]<br/>
Shipment ID : [SHIPMENT_ID]<br/>
Courier Service Provider : [COURIER_SERVICE]<br/>
Tracking Number : [TRACKING_NUMBER]<br/>
You can track your shipment at any time by going to&nbsp;<a href="http://www.myntra.com/mymyntra.php?view=myorders">My Orders</a>.<br/>
You can also track your order on [COURIER_SERVICE] \'s tracking page. It may take up to 24 hours for the tracking information to be available on their website.&nbsp;<br/><br/>
[SHIPMENT_DETAILS]<br/><br/>
Please feel free to contact us at&nbsp;<a href='http://www.myntra.com/contactus'>Contact Us</a>&nbsp;should you have any questions or need further assistance.
<br/>Regards,
<br/>Myntra.com Team<br/>
India's Largest Online Fashion Store<br/>
<br/>[DECLARATION]<br/><br/><br/><br/><br/>" 
where name='order_shipped_confirmation';


insert into mk_email_notification(name,
	subject,body) values('order_shipped_confirmation_part_first','Part of your order has been dispatched from the warehouse',"<div>Hi [USER],</div>
	<br/>
	<div>We would like to inform you that your Myntra.com order has been dispatched from our warehouse. You will receive your order by <b>[DELIVERY_DATE]</b>.</div>
	<br/>
	<div><b>Shipment Details</b></div>
	<div><br/></div><div>Order ID : [ORDER_ID]</div>
	<div>Shipment ID : [SHIPMENT_ID]</div>
	<div>Courier Service Provider : [COURIER_SERVICE]</div>
	<div>Tracking Number : &nbsp;[TRACKING_NUMBER]</div>
	<div><br/></div>
	<div>You can track your shipment anytime by going to&nbsp;<a href='http://www.myntra.com/mymyntra.php?view=myorders'>My Orders</a>.</div>
	<div><br/>You can also track your order on [COURIER_SERVICE] 's website at [COURIER_WEBSITE]. It may take up to 24 hours for the tracking information to be available on their website.<br/><br/></div>
	<div><b>Items included in the Shipment</b></div>
	<div><br/></div><div>[SHIPMENT_DETAILS]</div><br/>
	<div><br/></div><div>Please feel free to contact us at <a href='http://www.myntra.com/contactus'>Contact Us</a> should you have any questions or need further assistance.</div><div><br/></div>
	<div>Regards,</div><div>Myntra.com Team</div><div>India's Largest Online Fashion Store&nbsp;</div><div><br/></div><div><br/></div><div>[DECLARATION]</div>");
	
update mk_email_notification set subject= 'Part of your order has been dispatched from the warehouse', body="Hi [USER],<br/><br/>
We would like to inform you that part of your Myntra.com order has been dispatched from our warehouse. You will receive this part of your order by&nbsp;<b>[DELIVERY_DATE]</b>.<br/><br/>
<b>Shipment Details</b><br/>
Order ID : [ORDER_ID]<br/>
Shipment ID : [SHIPMENT_ID]<br/>
Courier Service Provider : [COURIER_SERVICE]<br/>
Tracking Number : [TRACKING_NUMBER]<br/>
You can track your shipment at any time by going to&nbsp;<a href='http://www.myntra.com/mymyntra.php?view=myorders'>My Orders</a>.
<br/>You can also track your order on [COURIER_SERVICE] 's tracking page. It may take up to 24 hours for the tracking information to be available on their website.&nbsp;<br/>
[SHIPMENT_DETAILS]<br/><br/>
With this all items of your order have been shipped from our warehouse.<br/>
Please feel free to contact us at&nbsp;<a href='http://www.myntra.com/contactus'>Contact Us</a>&nbsp;should you have any questions or need further assistance.<br/>
<br/>Regards,
<br/>Myntra.com Team<br/>
India's Largest Online Fashion Store<br/>
<br/>[DECLARATION]<br/><br/><br/><br/><br/>" 
where name='order_shipped_confirmation_part';




insert into mk_email_notification(name,
	subject,body) values('orderprocessedandhanddelivered_part_first','Part of your order has been dispatched from the warehouse',"Hi [USER],<br/><br/>
We would like to inform you that part of your Myntra.com order has been dispatched from our warehouse. You will receive this part of your order by&nbsp;<b>[DELIVERY_DATE]</b>.<br/><br/>
<b>Shipment Details</b><br/>
Order ID : [ORDER_ID]<br/>
Shipment ID : [SHIPMENT_ID]<br/>
Courier Service Provider : [COURIER_SERVICE]<br/>
Tracking Number : [TRACKING_NUMBER]<br/>
<br/>You can track your shipment at any time by going to&nbsp;<a href='http://www.myntra.com/mymyntra.php?view=myorders'>My Orders</a>.
<div><b>Items included in the Shipment</b></div>
<div>[SHIPMENT_DETAILS] &nbsp;</div>
<br/>
We will inform you when the remaining items are dispatched from our warehouse in a separate mail.&nbsp;<br/><br/>
Please feel free to contact us at&nbsp;<a href='http://www.myntra.com/contactus'>Contact Us</a>&nbsp;should you have any questions or need further assistance.<br/>
<br/>Regards,
<br/>Myntra.com Team<br/>
India's Largest Online Fashion Store<br/>
<br/>[DECLARATION]<br/><br/><br/><br/><br/>");
	
update mk_email_notification set subject= 'Part of your order has been dispatched from the warehouse', body="Hi [USER],<br/><br/>
We would like to inform you that part of your Myntra.com order has been dispatched from our warehouse. You will receive this part of your order by&nbsp;<b>[DELIVERY_DATE]</b>.<br/><br/>
<b>Shipment Details</b><br/>
Order ID : [ORDER_ID]<br/>
Shipment ID : [SHIPMENT_ID]<br/>
Courier Service Provider : [COURIER_SERVICE]<br/>
Tracking Number : [TRACKING_NUMBER]<br/>
<br/>You can track your shipment at any time by going to&nbsp;<a href='http://www.myntra.com/mymyntra.php?view=myorders'>My Orders</a>.
<div><b>Items included in the Shipment</b></div>
<div>[SHIPMENT_DETAILS] &nbsp;</div>

With this all items of your order have been shipped from our warehouse.<br/>
Please feel free to contact us at&nbsp;<a href='http://www.myntra.com/contactus'>Contact Us</a>&nbsp;should you have any questions or need further assistance.<br/>
<br/>Regards,
<br/>Myntra.com Team<br/>
India's Largest Online Fashion Store<br/>
<br/>[DECLARATION]<br/><br/><br/><br/><br/>" 
where name='orderprocessedandhanddelivered_part';


update mk_email_notification set subject= 'Your order has been dispatched from the warehouse', body="Hi [USER],<br/><br/>
We would like to inform you that part of your Myntra.com order has been dispatched from our warehouse. You will receive this part of your order by&nbsp;<b>[DELIVERY_DATE]</b>.<br/><br/>
<b>Shipment Details</b><br/>
<br/>Order ID : [ORDER_ID]<br/>
Shipment ID : [SHIPMENT_ID]<br/>
Courier Service Provider : [COURIER_SERVICE]
<span class='Apple-tab-span' style='white-space: pre;'>	</span><br/>Tracking Number : [TRACKING_NUMBER]<br/>
<br/>You can track your shipment at any time by going to&nbsp;<a href='http://www.myntra.com/mymyntra.php?view=myorders'>My Orders</a>.
<div><b>Items included in the Shipment</b></div>
<div>[SHIPMENT_DETAILS] &nbsp;</div>
<br/>
Please feel free to contact us at&nbsp;<a href='http://www.myntra.com/contactus'>Contact Us</a>&nbsp;should you have any questions or need further assistance.<br/>
<br/>Regards,
<br/>Myntra.com Team<br/>
India's Largest Online Fashion Store<br/>
<br/>[DECLARATION]<br/><br/><br/><br/><br/>" 
where name='orderprocessedandhanddelivered';