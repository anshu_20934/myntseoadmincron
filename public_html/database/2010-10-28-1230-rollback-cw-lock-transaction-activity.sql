delete from mk_email_notification where name='RRLockedTransaction';
update mk_email_notification set body='Dear [TRANSFER_TO], <br/>[NOTE]<br/>Congratulations! <B>[TRANSFER_FROM]</B> has rewarded you <B>[POINTS] points</B> for the <B>[REWARD_NAME] award</B> with the following message: <BR><B>[MEMO]</B><BR>[MGR_NAME] [MGR_MEMO]<br/>The points have been credited into your Personal Reward Account and can be used to redeem any gift available at the redemption portal.<BR><BR><BR>Sincerely<BR><BR>HR Department<BR><BR><BR><BR><a href="http://www.myntra.com/modules/organization/viewecard.php?toname=[TRANSFER_TO]&fromname=[TRANSFER_FROM]&points=[POINTS]&type=[REWARD_NAME]&memo=[URL_ENCODED_MEMO]">Click here to view a larger image of your ecard</a><BR><BR><BR><BR><img src="http://www.myntra.com/modules/organization/viewecard.php?toname=[TRANSFER_TO]&fromname=[TRANSFER_FROM]&points=[POINTS]&type=[REWARD_NAME]&mode=email&memo=[URL_ENCODED_MEMO]" alt="View Your E-card" /> <BR><BR>' where name='RRemployeePointTransfer';
delete from mk_org_business_constants where name='TRANSACTION_LOCK_PERIOD_IN_DAYS';
drop table mk_locked_svtransactions;

delete from mk_activity_params where activity_id=(select id from mk_activities where name='RRLockTransferEmployeePoints');
delete from mk_activity_rule_mapping where activity_id=(select id from mk_activities where name='RRLockTransferEmployeePoints');
delete from mk_activity_operation_mapping where activity_id=(select id from mk_activities where name='RRLockTransferEmployeePoints');
delete from mk_activities where name='RRLockTransferEmployeePoints';
delete from mk_operations where name='RRLockTransferEmployeePointsByAccountType';

delete from mk_activity_params where activity_id=(select id from mk_activities where name='RRClearLockedTransaction');
delete from mk_activity_rule_mapping where activity_id=(select id from mk_activities where name='RRClearLockedTransaction');
delete from mk_activity_operation_mapping where activity_id=(select id from mk_activities where name='RRClearLockedTransaction');
delete from mk_activities where name='RRClearLockedTransaction';
delete from mk_operations where name in ('RRClearLockedTransaction','RRListLockedTransaction');
delete from mk_rules where name in ('RRCheckClearTransactionLock','RRCheckClearRewardeeManager');

delete from mk_activity_params where activity_id=(select id from mk_activities where name='RRSearchListLockedTransaction');
delete from mk_activity_operation_mapping where activity_id=(select id from mk_activities where name='RRSearchListLockedTransaction');
delete from mk_activities where name='RRSearchListLockedTransaction';


delete from mk_activity_params where activity_id=(select id from mk_activities where name='RRSearchListLockedTransactionByManager');
delete from mk_activity_operation_mapping where activity_id=(select id from mk_activities where name='RRSearchListLockedTransactionByManager');
delete from mk_activities where name='RRSearchListLockedTransactionByManager';
delete from mk_operations where name in ('RRListLockedTransactionByManager');
