insert into mk_email_notification (name,subject,body) values ('RRLockedTransaction','Award Confirmation','Dear [REWARDER], <BR>[CONTENT]<BR/>[MGR_NAME][MGR_MEMO]<BR>[NOTE]<br/><BR><BR>Sincerely<BR>HR Department');
update mk_email_notification set body='Dear [TRANSFER_TO], <br/>[NOTE]<br/>Congratulations! <B>[TRANSFER_FROM]</B> has rewarded you <B>[POINTS] points</B> for the <B>[REWARD_NAME] award</B> with the following message: <BR><B>[MEMO]</B><BR>[MGR_NAME] [MGR_MEMO]<br/>The points have been credited into your Personal Reward Account and can be used to redeem any gift available at the redemption portal.<BR><BR><BR>Sincerely<BR><BR>HR Department<BR><BR><BR><BR><a href="http://www.myntra.com/modules/organization/viewecard.php?toname=[TRANSFER_TO]&fromname=[TRANSFER_FROM]&points=[POINTS]&type=[REWARD_NAME]&memo=[URL_ENCODED_MEMO]">Click here to view a larger image of your ecard</a><BR><BR><BR><BR><img src="http://www.myntra.com/modules/organization/viewecard.php?toname=[TRANSFER_TO]&fromname=[TRANSFER_FROM]&points=[POINTS]&type=[REWARD_NAME]&mode=email&memo=[URL_ENCODED_MEMO]" alt="View Your E-card" /> <BR><BR>' where name='RRemployeePointTransfer';
insert into mk_org_business_constants(name,category,value,orgid) values('TRANSACTION_LOCK_PERIOD_IN_DAYS','RR',7,25);

create table mk_locked_svtransactions(
	transaction_id int(10) unsigned NOT NULL auto_increment,
	rewarder_emp_login varchar(255),
	rewarder_emp_account_type varchar(20),
	rewardee_emp_login varchar(255),
	rewardee_emp_account_type varchar(20),
	transaction_info text,
	locked_amount decimal(12,2) default NULL,
	transaction_date timestamp NULL default CURRENT_TIMESTAMP,
	system_transaction_clear_date timestamp null,
	transaction_clear_info text,
	transaction_status varchar(20) not null default "locked",
	orgid int(10) unsigned default NULL,
	PRIMARY KEY  (`transaction_id`),
	index (`transaction_id`)
)ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='latin1_swedish_ci';


insert into mk_activities (name,description,tplname) values ('RRLockTransferEmployeePoints','makes a locked transaction (for furthur approval by rewardee\'s manager) in case of handshake if rewardee has a manager','grant_reward_points.tpl');

insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRLockTransferEmployeePoints'),1);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRLockTransferEmployeePoints'),2);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRLockTransferEmployeePoints'),3);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRLockTransferEmployeePoints'),4);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRLockTransferEmployeePoints'),5);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRLockTransferEmployeePoints'),7);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRLockTransferEmployeePoints'),8);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRLockTransferEmployeePoints'),9);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRLockTransferEmployeePoints'),11);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRLockTransferEmployeePoints'),12);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRLockTransferEmployeePoints'),13);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRLockTransferEmployeePoints'),14);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRLockTransferEmployeePoints'),15);

insert into mk_activity_params (activity_id,param_name,required) values((select id from mk_activities where name='RRLockTransferEmployeePoints'),'amount',1);
insert into mk_activity_params (activity_id,param_name,required) values((select id from mk_activities where name='RRLockTransferEmployeePoints'),'burn_account_type',1);
insert into mk_activity_params (activity_id,param_name,required) values((select id from mk_activities where name='RRLockTransferEmployeePoints'),'burn_employee_login',1);
insert into mk_activity_params (activity_id,param_name,required) values((select id from mk_activities where name='RRLockTransferEmployeePoints'),'earn_account_type',1);
insert into mk_activity_params (activity_id,param_name,required) values((select id from mk_activities where name='RRLockTransferEmployeePoints'),'earn_employee_login',1);
insert into mk_activity_params (activity_id,param_name,required) values((select id from mk_activities where name='RRLockTransferEmployeePoints'),'memo',1);
insert into mk_activity_params (activity_id,param_name,required) values((select id from mk_activities where name='RRLockTransferEmployeePoints'),'orgid',1);

insert into mk_operations (name,description,path) values ('RRLockTransferEmployeePointsByAccountType','Lock the points transferred by debiting the rewarder account and recording the transaction','ws.rewardsService.RROperations');

insert into mk_activity_operation_mapping (activity_id,operation_id,sequence) values ((select id from mk_activities where name='RRLockTransferEmployeePoints'),(select id from mk_operations where name='RRLockTransferEmployeePointsByAccountType'),0);
insert into mk_activity_operation_mapping (activity_id,operation_id,sequence) values ((select id from mk_activities where name='RRLockTransferEmployeePoints'),3,1);
insert into mk_activity_operation_mapping (activity_id,operation_id,sequence) values ((select id from mk_activities where name='RRLockTransferEmployeePoints'),4,1);
insert into mk_activity_operation_mapping (activity_id,operation_id,sequence) values ((select id from mk_activities where name='RRLockTransferEmployeePoints'),6,1);



insert into mk_activities (name,description,tplname) values ('RRClearLockedTransaction','clears a locked transaction by approving or rejecting by a manager','manage_locked_transaction.tpl');

insert into mk_rules (name,description,path) values ('RRCheckClearTransactionLock','check for authenticity of the parameters(may get tampered) while clearing a locked transaction','ws.rewardsService.RRRules');
insert into mk_rules (name,description,path) values ('RRCheckClearRewardeeManager',"authenticates the rewardee's manager while clearing a locked transaction",'ws.rewardsService.RRRules'); 

insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRClearLockedTransaction'),1);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRClearLockedTransaction'),3);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRClearLockedTransaction'),4);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRClearLockedTransaction'),5);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRClearLockedTransaction'),7);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRClearLockedTransaction'),9);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRClearLockedTransaction'),11);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRClearLockedTransaction'),12);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRClearLockedTransaction'),13);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRClearLockedTransaction'),14);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRClearLockedTransaction'),15);
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRClearLockedTransaction'),(select id from mk_rules where name='RRCheckClearTransactionLock'));
insert into mk_activity_rule_mapping (activity_id,rule_id) values ((select id from mk_activities where name='RRClearLockedTransaction'),(select id from mk_rules where name='RRCheckClearRewardeeManager')); 

insert into mk_activity_params (activity_id,param_name,required) values((select id from mk_activities where name='RRClearLockedTransaction'),'lock_transaction_id',1);
insert into mk_activity_params (activity_id,param_name,required) values((select id from mk_activities where name='RRClearLockedTransaction'),'burn_employee_login',1);
insert into mk_activity_params (activity_id,param_name,required) values((select id from mk_activities where name='RRClearLockedTransaction'),'earn_employee_login',1);
insert into mk_activity_params (activity_id,param_name,required) values((select id from mk_activities where name='RRClearLockedTransaction'),'burn_account_type',1);
insert into mk_activity_params (activity_id,param_name,required) values((select id from mk_activities where name='RRClearLockedTransaction'),'earn_account_type',1);
insert into mk_activity_params (activity_id,param_name,required) values((select id from mk_activities where name='RRClearLockedTransaction'),'amount',1);
insert into mk_activity_params (activity_id,param_name,required) values((select id from mk_activities where name='RRClearLockedTransaction'),'memo',1);
insert into mk_activity_params (activity_id,param_name,required) values((select id from mk_activities where name='RRClearLockedTransaction'),'mgr_memo',1);
insert into mk_activity_params (activity_id,param_name,required) values((select id from mk_activities where name='RRClearLockedTransaction'),'lock_transaction_status',1);
insert into mk_activity_params (activity_id,param_name,required) values((select id from mk_activities where name='RRClearLockedTransaction'),'orgid',1);
insert into mk_activity_params (activity_id,param_name,required) values((select id from mk_activities where name='RRClearLockedTransaction'),'customer_login',1); 

insert into mk_operations (name,description,path) values ('RRClearLockedTransaction','clear a locked handshake transaction by approving(credit rewarder and handshake) or rejecting(credit rewarder)','ws.rewardsService.RROperations');
insert into mk_operations (name,description,path) values ('RRListLockedTransaction','list all locked transactions','ws.rewardsService.RROperations');

insert into mk_activity_operation_mapping (activity_id,operation_id,sequence) values ((select id from mk_activities where name='RRClearLockedTransaction'),(select id from mk_operations where name='RRClearLockedTransaction'),0);
insert into mk_activity_operation_mapping (activity_id,operation_id,sequence) values ((select id from mk_activities where name='RRClearLockedTransaction'),3,1);
insert into mk_activity_operation_mapping (activity_id,operation_id,sequence) values ((select id from mk_activities where name='RRClearLockedTransaction'),6,1);



insert into mk_activities (name,description,tplname) values ('RRSearchListLockedTransaction','lists all locked transactions based on criteria','manage_locked_transaction.tpl');

insert into mk_activity_params (activity_id,param_name,required) values((select id from mk_activities where name='RRSearchListLockedTransaction'),'orgid',1);

insert into mk_activity_operation_mapping (activity_id,operation_id,sequence) values ((select id from mk_activities where name='RRSearchListLockedTransaction'),(select id from mk_operations where name='RRListLockedTransaction'),1);
insert into mk_activity_operation_mapping (activity_id,operation_id,sequence) values ((select id from mk_activities where name='RRSearchListLockedTransaction'),3,1);
insert into mk_activity_operation_mapping (activity_id,operation_id,sequence) values ((select id from mk_activities where name='RRSearchListLockedTransaction'),6,1);



insert into mk_activities (name,description,tplname) values ('RRSearchListLockedTransactionByManager',"lists all locked transactions based on criteria for rewardee's manager",'manage_locked_transaction.tpl');

insert into mk_activity_params (activity_id,param_name,required) values((select id from mk_activities where name='RRSearchListLockedTransactionByManager'),'orgid',1);
insert into mk_activity_params (activity_id,param_name,required) values((select id from mk_activities where name='RRSearchListLockedTransactionByManager'),'customer_login',1);

insert into mk_operations (name,description,path) values ('RRListLockedTransactionByManager',"list all locked transactions according to rewardee's manager",'ws.rewardsService.RROperations');

insert into mk_activity_operation_mapping (activity_id,operation_id,sequence) values ((select id from mk_activities where name='RRSearchListLockedTransactionByManager'),(select id from mk_operations where name='RRListLockedTransactionByManager'),1);
insert into mk_activity_operation_mapping (activity_id,operation_id,sequence) values ((select id from mk_activities where name='RRSearchListLockedTransactionByManager'),3,1);
insert into mk_activity_operation_mapping (activity_id,operation_id,sequence) values ((select id from mk_activities where name='RRSearchListLockedTransactionByManager'),6,1);
