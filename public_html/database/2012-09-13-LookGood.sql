SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';



DROP TABLE IF EXISTS `lg_style_tip_styles`;
DROP TABLE IF EXISTS `lg_star_n_style_lg_style_tips_map`;
DROP TABLE IF EXISTS `lg_style_tip`;
DROP TABLE IF EXISTS `lg_star_n_style_image_config`;
DROP TABLE IF EXISTS `lg_star_n_style_diary`;
DROP TABLE IF EXISTS `lg_star_n_style`;
DROP TABLE IF EXISTS `lg_look_styles`;
DROP TABLE IF EXISTS `lg_look_book_lg_look_map`;
DROP TABLE IF EXISTS `lg_look`;
DROP TABLE IF EXISTS `lg_look_book`;


-- -----------------------------------------------------
-- Table `lg_look_book`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lg_look_book` ;
CREATE  TABLE IF NOT EXISTS `lg_look_book` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NULL ,
  `summary` TEXT NULL ,
  `description` TEXT NULL ,
  `is_active` TINYINT NULL DEFAULT 0 ,
  `created_on` INT(11) NULL ,
  `updated_on` INT(11) NULL ,
  `updated_by` VARCHAR(255) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `lg_look`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lg_look` ;

CREATE  TABLE IF NOT EXISTS `lg_look` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NULL ,
  `summary` TEXT NULL ,
  `description` TEXT NULL ,
  `is_active` TINYINT NULL DEFAULT 0 ,
  `is_right_aligned` TINYINT NULL DEFAULT 0 ,
  `is_text_white`  TINYINT NULL DEFAULT 0 ,
  `image_look` VARCHAR(255) NULL ,
  `image_buy_look` VARCHAR(255) NULL ,
  `similar_styles_url` VARCHAR(255) NULL ,
  `is_purchasable` TINYINT NULL DEFAULT 1 ,
  `created_on` INT(11) NULL ,
  `updated_on` INT(11) NULL ,
  `updated_by` VARCHAR(255) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `lg_look_book_lg_look_map`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lg_look_book_lg_look_map` ;

CREATE  TABLE IF NOT EXISTS `lg_look_book_lg_look_map` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `fk_lg_look_book_id` INT NULL ,
  `fk_lg_look_id` INT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_lg_look_book_look_lg_look_id` (`fk_lg_look_id` ASC) ,
  INDEX `fk_lg_look_book_look_lg_look_book_id` (`fk_lg_look_book_id` ASC) ,
  CONSTRAINT `fk_lg_look_book_look_lg_look_id`
    FOREIGN KEY (`fk_lg_look_id` )
    REFERENCES `lg_look` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_look_book_look_lg_look_book_id`
    FOREIGN KEY (`fk_lg_look_book_id` )
    REFERENCES `lg_look_book` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lg_style_tip`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lg_style_tip` ;

CREATE  TABLE IF NOT EXISTS `lg_style_tip` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NULL ,
  `description` TEXT NULL ,
  `image` VARCHAR(255) NULL ,
  `is_active` TINYINT NULL DEFAULT 0 ,
  `created_on` INT(11) NULL ,
  `updated_on` INT(11) NULL ,
  `updated_by` VARCHAR(255) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lg_star_n_style`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lg_star_n_style` ;

CREATE  TABLE IF NOT EXISTS `lg_star_n_style` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NULL ,
  `description` TEXT NULL ,
  `image` VARCHAR(255) NULL ,
  `is_active` TINYINT NULL DEFAULT 0 ,
  `start_date` INT(11) NULL ,
  `end_date` INT(11) NULL ,
  `created_on` INT(11) NULL ,
  `updated_on` INT(11) NULL ,
  `updated_by` VARCHAR(255) NULL ,
  `fk_lg_look_book_id` INT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_lg_star_n_style_lg_look_book_id` (`fk_lg_look_book_id` ASC) ,
  CONSTRAINT `fk_lg_star_n_style_lg_look_book_id`
    FOREIGN KEY (`fk_lg_look_book_id` )
    REFERENCES `lg_look_book` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lg_star_n_style_image_config`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lg_star_n_style_image_config` ;

CREATE  TABLE IF NOT EXISTS `lg_star_n_style_image_config` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NULL ,
  `fk_lg_star_n_style_id` INT NULL ,
  `image_coords_x1` INT NULL ,
  `image_coords_y1` INT NULL ,
  `image_coords_x2` INT NULL ,
  `image_coords_y2` INT NULL ,
  `target_page` VARCHAR(255) NULL ,
  `new_window` TINYINT NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_lg_star_n_style_image_config_lg_star_n_style_id` (`fk_lg_star_n_style_id` ASC) ,
  CONSTRAINT `fk_lg_star_n_style_image_config_lg_star_n_style_id`
    FOREIGN KEY (`fk_lg_star_n_style_id` )
    REFERENCES `lg_star_n_style` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lg_star_n_style_diary`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lg_star_n_style_diary` ;

CREATE  TABLE IF NOT EXISTS `lg_star_n_style_diary` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NULL ,
  `fk_lg_star_n_style_id` INT NULL ,
  `image` VARCHAR(255) NULL ,
  `note` TEXT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_lg_star_n_style_diary_lg_star_n_style_id` (`fk_lg_star_n_style_id` ASC) ,
  CONSTRAINT `fk_lg_star_n_style_diary_lg_star_n_style_id`
    FOREIGN KEY (`fk_lg_star_n_style_id` )
    REFERENCES `lg_star_n_style` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lg_star_n_style_lg_style_tips_map`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lg_star_n_style_lg_style_tips_map` ;

CREATE  TABLE IF NOT EXISTS `lg_star_n_style_lg_style_tips_map` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `fk_star_n_style_id` INT NULL ,
  `fk_style_tip_id` INT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_lg_star_n_style_tips_map_lg_star_n_style_id` (`fk_star_n_style_id` ASC) ,
  INDEX `fk_lg_star_n_style_tips_map_lg_style_tip_id` (`fk_style_tip_id` ASC) ,
  CONSTRAINT `fk_lg_star_n_style_tips_map_lg_star_n_style_id`
    FOREIGN KEY (`fk_star_n_style_id` )
    REFERENCES `lg_star_n_style` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_star_n_style_tips_map_lg_style_tip_id`
    FOREIGN KEY (`fk_style_tip_id` )
    REFERENCES `lg_style_tip` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lg_look_styles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lg_look_styles` ;

CREATE  TABLE IF NOT EXISTS `lg_look_styles` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `fk_lg_look_id` INT NULL ,
  `style_id` INT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_lg_look_lg_look_styles_id` (`fk_lg_look_id` ASC) ,
  CONSTRAINT `fk_lg_look_lg_look_styles_id`
    FOREIGN KEY (`fk_lg_look_id` )
    REFERENCES `lg_look` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lg_style_tip_styles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lg_style_tip_styles` ;

CREATE  TABLE IF NOT EXISTS `lg_style_tip_styles` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `fk_lg_style_tip_id` INT NULL ,
  `style_id` INT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_lg_style_tip_styles_lg_style_tip_id` (`fk_lg_style_tip_id` ASC) ,
  CONSTRAINT `fk_lg_style_tip_styles_lg_style_tip_id`
    FOREIGN KEY (`fk_lg_style_tip_id` )
    REFERENCES `lg_style_tip` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
