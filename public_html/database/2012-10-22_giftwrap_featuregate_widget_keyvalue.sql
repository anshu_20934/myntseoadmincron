insert into mk_feature_gate_key_value_pairs(`key`,`value`,description) values('giftwrap.enabled',true,'giftwrap enable/disable');
insert into mk_widget_key_value_pairs(`key`, `value`, `description`) values('giftwrap.charges','30','amount to be charged for gift wrapping');
