-- PORTAL-252: Kundan: now track when was the cart last modified, 
-- so as to be able to send mailers to users who abandoned their carts without proceeding for payment
-- Corresponding Rollback Script
alter table mk_shopping_cart drop column last_modified;