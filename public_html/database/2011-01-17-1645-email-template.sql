UPDATE mk_email_notification SET body = 'Dear [FIRST_NAME],<br><br>

Welcome to Myntra, the one-stop online destination for your Sportswear and Lifestyle merchandise needs! <br>
Shop for your favourite brands like Reebok, Nike, Puma, Geonaute and many more. <br><br>

Your user id: [USER_NAME]<br>

For any queries, write to us on <a href="mailto:support@myntra.com">support@myntra.com</a> or call our <a href="http://www.myntra.com/contact_us.php">Customer Care team</a>. <br><br>

Enjoy shopping online at Myntra!<br><br>

Team Myntra<br>
<br><br>' WHERE NAME = 'generete_registration_coupon_sms';

UPDATE mk_email_notification SET body = 'Dear [FIRST_NAME],<br><br>

Welcome to Myntra, the one-stop online destination for your Sportswear and Lifestyle merchandise needs! <br>
Shop for your favourite brands like Reebok, Nike, Puma, Geonaute and many more. <br><br>

Your user id: [USER_NAME]<br>

For any queries, write to us on <a href="mailto:support@myntra.com">support@myntra.com</a> or call <a href="http://www.myntra.com/contact_us.php">Customer Care team</a>. <br><br>

Enjoy shopping online at Myntra!<br><br>

Team Myntra<br>
<br><br>' WHERE NAME = 'newuserregistration_withfreecoupon';