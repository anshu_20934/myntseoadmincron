
insert into mk_abtesting_tests (name,ga_slot,seg_algo,source_type) values('orderplacement','1','abtest\\algos\\impl\\RandomSegmentationAlgo','tpl');
insert into mk_abtesting_variations (ab_test_id,name,percent_probability) values((select id from mk_abtesting_tests where name='orderplacement'),'test',0);
insert into mk_abtesting_variations (ab_test_id,name,percent_probability) values((select id from mk_abtesting_tests where name='orderplacement'),'control',100);

alter table order_oh_reasons_master add column subreason_code varchar(10) default null, add column subreason_desc varchar(255) default null, drop index occ_unq_key, add unique index reason_subreason_unq_key (reason_code, subreason_code);

insert into order_oh_reasons_master (id, reason_code, reason_desc, subreason_code, subreason_desc, is_manual_change, last_updated_time) VALUES (11, 'CVP', 'COD Verification Pending', 'NC', 'New customer with no completed orders (no orders in DL/C)', 0, NULL);
insert into order_oh_reasons_master (id, reason_code, reason_desc, subreason_code, subreason_desc, is_manual_change, last_updated_time) VALUES (12, 'CVP', 'COD Verification Pending', 'LNRTO', 'One of last n orders is RTO', 0, NULL);
insert into order_oh_reasons_master (id, reason_code, reason_desc, subreason_code, subreason_desc, is_manual_change, last_updated_time) VALUES (13, 'CVP', 'COD Verification Pending', 'OHRTO', 'One of last n orders is put on hold via cod oh tracker and resulted in RTO', 0, NULL);
insert into order_oh_reasons_master (id, reason_code, reason_desc, subreason_code, subreason_desc, is_manual_change, last_updated_time) VALUES (14, 'CVP', 'COD Verification Pending', 'OHF', 'Last order called from this tracker + rejected', 0, NULL);
insert into order_oh_reasons_master (id, reason_code, reason_desc, subreason_code, subreason_desc, is_manual_change, last_updated_time) VALUES (15, 'CVP', 'COD Verification Pending', 'NINC', 'Last n orders incomplete', 0, NULL);
insert into order_oh_reasons_master (id, reason_code, reason_desc, subreason_code, subreason_desc, is_manual_change, last_updated_time) VALUES (16, 'CVP', 'COD Verification Pending', 'FCPS', 'First COD Purchase from States', 0, NULL);
insert into order_oh_reasons_master (id, reason_code, reason_desc, subreason_code, subreason_desc, is_manual_change, last_updated_time) VALUES (17, 'CVP', 'COD Verification Pending', 'ZPPM', 'First COD Purchase from States', 0, NULL);
insert into order_oh_reasons_master (id, reason_code, reason_desc, subreason_code, subreason_desc, is_manual_change, last_updated_time) VALUES (18, 'CVP', 'COD Verification Pending', 'SPM', 'State-PIN code mismatch', 0, NULL);
insert into order_oh_reasons_master (id, reason_code, reason_desc, subreason_code, subreason_desc, is_manual_change, last_updated_time) VALUES (19, 'CVP', 'COD Verification Pending', 'SAL', 'On Hold for suspicious address string length', 0, NULL);
insert into order_oh_reasons_master (id, reason_code, reason_desc, subreason_code, subreason_desc, is_manual_change, last_updated_time) VALUES (20, 'CVP', 'COD Verification Pending', 'ALL', 'All cod order On Hold', 0, NULL);

insert into order_oh_reasons_master (id, reason_code, reason_desc, subreason_code, subreason_desc, is_manual_change, last_updated_time) VALUES (21, 'FRI', 'Undergoing Fraud Investigation', 'PGF', 'Payment Gateway Flagged', 0, NULL);
insert into order_oh_reasons_master (id, reason_code, reason_desc, subreason_code, subreason_desc, is_manual_change, last_updated_time) VALUES (22, 'FRI', 'Undergoing Fraud Investigation', 'INO', 'International Card Used', 0, NULL);
insert into order_oh_reasons_master (id, reason_code, reason_desc, subreason_code, subreason_desc, is_manual_change, last_updated_time) VALUES (23, 'FRI', 'Undergoing Fraud Investigation', 'MISC', 'User fraud check required', 1, NULL);

update order_oh_reasons_master set reason_code = 'FRI', reason_desc = 'Undergoing Fraud Investigation', subreason_code = 'UAPM', subreason_desc = 'mismatch in amount paid and item total', is_manual_change = 1 where id = 2;
update order_oh_reasons_master set reason_code = 'PPOH', reason_desc = 'Payments Processed PP to OH', is_manual_change = 1 where id = 5;
update order_oh_reasons_master set reason_code = 'CDE', reason_desc = 'Cashback Debit Error', subreason_code = 'CNA', subreason_desc = 'cashback/myntCash not available in customers account', is_manual_change = 1 where id = 6;
update order_oh_reasons_master set reason_code = 'CDE', reason_desc = 'Cashback Debit Error', subreason_code = 'CSD', subreason_desc = 'cashback/myntCash service unavailable at the time of order placement', is_manual_change = 1 where id = 7;
update order_oh_reasons_master set reason_code = 'TEL', reason_desc = 'Telesales order verification', is_manual_change = 1 where id = 8;
update order_oh_reasons_master set reason_code = 'FRI', reason_desc = 'Undergoing Fraud Investigation', subreason_code = 'FIDS', subreason_desc = 'Free item present in different shipment', is_manual_change = 0 where id = 10;

CREATE TABLE `xpress_capacity` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `zipcode` int(5) NOT NULL,
  `record_date` date NOT NULL,
  `no_of_orders` int(11) DEFAULT NULL,
  `start_hour` int(5) DEFAULT NULL,
  `end_hour` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `order_additional_info` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `order_id_fk` int(20) NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oai_unq_key` (`order_id_fk`,`key`),
  KEY `order_id_key` (`order_id_fk`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

alter table xcart_orders modify status status varchar(5); 
