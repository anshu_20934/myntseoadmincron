delete from mk_email_notification where name in ( 'pre_purchase', 'order_confirmation', 'order_shipped_confirmation', 'feedback_request', 'cancelled_order_feedback_request', 'cod_mobile_verification', 'order_details_table');

insert into mk_email_notification(name, subject, body) values('order_details_table', '', 
'<table border=0 cellspacing=0 cellpadding=0 style="width:100%;">
<thead style="background-color:#E0E0E0;">
<th width="34%" style="padding: 10px 5px;" align=left>Product</th>
<th width="15%" style="padding: 10px 5px;" align=left>Actual Price</th>
<th width="10%" style="padding: 10px 5px;" align=left>Discount</th>
<th width="5%" style="padding: 10px 5px;" align=left>Size</th>
<th width="10%" style="padding: 10px 5px;" align=left>Quantity</th>
<th width="10%" style="padding: 10px 5px;" align=right>Total</th>
<th width="15%" style="padding: 10px 5px;" align=left>[Vat Inclusive]</th>
<th width="1%" style="padding: 10px 5px;">&nbsp;</th>
</thead>
<tbody>[PRODUCT_DETAILS]</tbody>
</table>');

insert into mk_email_notification(name, subject, body) values('pre_purchase', 'Complete your purchase at Myntra.com', 
'Hi [USER], <br/><br/>
<p>We noticed that your purchase at Myntra was not completed and thought we\'d check and see if we can help. If you had any trouble buying, please try one of the following options:</p>
<br/>
<p>
  1. <a href="http://www.myntra.com/mkmycart.php?at=c&pagetype=productdetail&utm_source=reminder&utm_medium=mail&utm_campaign=ppreminder">Click this link</a> to try purchasing again.<br/>
  2. Or if you\'d like to talk to us, kindly call us at [MYNTRA_CC_PHONE]. Do mention your order id - [ORDER_ID].
</p>
<br/>
Your experience at Myntra is important to us. If you have any queries or suggestions, please e-mail us at support@myntra.com, or call us at [MYNTRA_CC_PHONE]. We look forward to hear from you!
<br/>
Regards,<br/>
Myntra Customer Care
');

insert into mk_email_notification(name, subject, body) values('order_confirmation', 'Your order with Myntra is successfully placed! Order No. [ORDER_ID]',
'Hi [USER], <br/><br/>
This is to confirm that your order with Myntra has been successfully placed!<br/>Your Order No. is [ORDER_ID]. Please use this for future reference.<br/><br/>
[ORDER_DETAILS]
<br/>
<p>Your order is under process and shall be delivered to you by [DELIVERY_DATE].</p>
<p>You can track your order status at <a href="http://www.myntra.com/mymyntra.php">My Myntra</a> by logging in with your account details. In case of any queries kindly contact us at [MYNTRA_CC_PHONE].</p>
<br/>
Regards,<br/>
Myntra Customer Care');

insert into mk_email_notification(name, subject, body) values('order_shipped_confirmation', 'Your order with Myntra has been shipped! Order No. [ORDER_ID]',
'Hi [USER], <br/><br/>
Your order with Myntra has been processed and shipped. Here are the tracking details:<br/><br/>
Courier Partner:  [COURIER_SERVICE]<br/>
Tracking Code: [TRACKING_NUMBER]<br/>
<br/>
You can track your order online at [COURIER_WEBSITE] using the given tracking code. It may take few hours before your order is updated and displayed by the courier partner. <br/><br/>
Here are the details of Order No. [ORDER_ID], for your reference -
<br/><br/>
[ORDER_DETAILS]
<br/>
<p>We will be sending a separate mailer requesting for your feedback. In case of any further queries, please get in touch with us at [MYNTRA_CC_PHONE].</p>
<br/>
Regards,<br/>
Myntra Customer Care');

insert into mk_email_notification(name, subject, body) values('feedback_request', 'Request for your feedback - Order No. [ORDER_ID]',
'Hi [USER], <br/><br/>
We hope that you have received the product(s) for the Order No. [ORDER_ID] which we shipped to you on [SHIPPING_DATE].<br/>
As a constant endeavour to improve our service, we would request you to share with us - your shopping experience with Myntra.<br/><br/>
<a href="[FEEDBACK_URL]">Click here to go to the feedback form</a>
<br/><br/>
As a simple gesture of thanks, we will e-mail you a Rs 100 coupon as soon as you send your feedback.
<br/><br/>
Here are the details of Order No. [ORDER_ID], for your reference -
<br/><br/>
[ORDER_DETAILS]
<br/>
<p>For any further clarification, kindly contact us at [MYNTRA_CC_PHONE].</p>
<br/>
Regards,<br/>
Myntra Customer Care');

insert into mk_email_notification(name, subject, body) values('cancelled_order_feedback_request', 'Feedback on your cancelled Order No. [ORDER_ID]',
'Hi [USER], <br/><br/>
We are sorry to note that your order has been cancelled. This is of high importance to us and we would like to improve shortfalls on our front, if any. Please provide your valuable feedback regarding this concern.<br/><br/>
<a href="">Click here to submit feedback</a><br/><br/>
As a simple gesture of thanks, we will e-mail you a Rs 100 gift coupon, once you submit your feedback.<br/><br/>
Here are the details of cancelled Order No. [ORDER_ID], for your reference -
<br/><br/>
[ORDER_DETAILS]
<br/>
<p>For any further clarification kindly contact us at [MYNTRA_CC_PHONE]. We hope to have you shopping with us again.</p>
<br/>
Regards,<br/>
Myntra Customer Care');

insert into mk_email_notification(name, subject, body) values('cod_mobile_verification', 'Mobile verification for Order [ORDER_ID]',
'Hi [USER], <br/><br/>
<p>Please ignore this email if you have already verified your mobile number for order [ORDER_ID].</p><br/>
<p>In case you were not able to verify your mobile phone, please follow this link :<br/>
<a href="http://www.myntra.com/mkCODverification.php?verify=email&orderid=[ORDER_ID]&loginid=[CUSTOMER_LOGIN]&code=[CUSTOMER_LOGIN_HASH]">Click here to verify your mobile phone</a></p>
<br/><br/>
<p>In case of any other queries, please get in touch with us at [MYNTRA_CC_PHONE].</p>
<br/>
Regards,<br/>
Myntra Customer Care');
