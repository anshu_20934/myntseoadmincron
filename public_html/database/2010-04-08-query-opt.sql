
alter table xcart_products drop key idx_fulldescr;
alter table xcart_products drop key idx_keywords;
alter table xcart_products drop key idx_product;
alter table xcart_products drop key idx_descr;

alter table xcart_products engine = innodb;
alter table mk_product_type engine = innodb;
alter table mk_product_style engine = innodb;
alter table mk_designer engine = innodb;
alter table xcart_customers engine = innodb;