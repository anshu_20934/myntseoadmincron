/* Adding fields to discount table*/
alter table discount add column `discount_funding` varchar(255) default null;
alter table discount add column `funding_percentage` decimal(5,2) default null;
alter table discount add column `funding_tax` tinyint(1) default null;
alter table discount add column `funding_basis` varchar(255) default null;
alter table discount add column `discount_limit` decimal(5,2) default null;

/* Updating corresponding history table too*/
alter table discount_history add column `discount_funding` varchar(255) default null;
alter table discount_history add column `funding_percentage` decimal(5,2) default null;
alter table discount_history add column `funding_tax` tinyint(1) default null;
alter table discount_history add column `funding_basis` varchar(255) default null;
alter table discount_history add column `discount_limit` decimal(5,2) default null;

