<?php
require_once 'auth.php';
require_once $xcart_dir.'/include/func/func.order.php';

$styleid = filter_input(INPUT_GET, 'styleid', FILTER_SANITIZE_NUMBER_INT);
$skuid = filter_input(INPUT_GET, 'skuid', FILTER_SANITIZE_NUMBER_INT);
$pincode = filter_input(INPUT_GET, 'pincode', FILTER_SANITIZE_NUMBER_INT);
$fetchedCodLimits = filter_input(INPUT_GET, 'gotCod', FILTER_SANITIZE_NUMBER_INT);
$availableInWarehouses = filter_input(INPUT_GET, 'availableinwarehouses', FILTER_SANITIZE_STRING);
$leadTime = filter_input(INPUT_GET, 'leadtime', FILTER_SANITIZE_NUMBER_INT);
$supplyType = filter_input(INPUT_GET, 'supplytype', FILTER_SANITIZE_STRING);

if(empty($pincode)) {
	$sql = "SELECT pincode FROM mk_customer_address WHERE login = '". mysql_real_escape_string($XCART_SESSION_VARS['login']) ."' and default_address = 1;";
	$pincode = func_query_first($sql, true);
	$pincode = $pincode['pincode'];
}

if(empty($pincode)) {
	$response = array('status' => 'ERROR', 'message' => 'Pincode not found!');
	header('Content-Type:application/json');
	echo json_encode($response);
	exit;
}
// Fetching the first available SKU for a given style
// $skuid = func_query_first_cell("select sku_id from mk_product_options po, mk_styles_options_skus_mapping som where po.id = som.option_id and po.is_active = 1 and po.style = $styleid", true);
$warehouses = explode(",", $availableInWarehouses);
$returnArray = fetchProductServiceabilityAndTat($styleid, $skuid, $pincode, $warehouses, $supplyType, $leadTime);
$returnArray['pincode'] = $pincode;

if ($fetchedCodLimits != 1 && $_GET['pincode']) {
	$codRange = trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('cod.limit.range'));
	$codRangeArray = explode("-", $codRange);
	$returnArray['codLimits'] = "Rs. ".number_format($codRangeArray[0])." - Rs. ".number_format($codRangeArray[1]);
}

$response = array('status' => 'SUCCESS', 'data' => $returnArray);
header('Content-Type:application/json');
echo json_encode($response);
exit;

