<?php
require_once("../authAPI.php");
ob_clean();
Header('Content-type: text/xml');
function getXMLToReturn($orderID,$login,$cust_mobile,$amountToTake,$errorCode,$errorMsg) {
	$newsXML = new SimpleXMLElement("<order_id_details></order_id_details>");
	$newsXML->addChild('trans_id',$orderID);
	$newsXML->addChild('customer_id',$login);
	$newsXML->addChild('caller_id',$cust_mobile);
	$newsXML->addChild('amount',$amountToTake);
	$newsXML->addChild('error_code',$errorCode);
	$newsXML->addChild('error_msg',$errorMsg);
	return $newsXML->asXML();
}


$errorCode = 700;
$errorMessage = "Unauthorised usage";

if ($REQUEST_METHOD == "GET"){
	
	$orderId = $HTTP_GET_VARS['cN'];	
	$orderId = trim($orderId);
	
	if(empty($orderId)){
		// return error 300 Booking Id is empty null
		$errorCode = 300;
		$errorMessage = "Booking Id is empty";
	} else if(is_numeric($orderId)) {
		$orderId = mysql_escape_string($orderId);
		$order_details = func_query_first("SELECT * FROM " . $sql_tbl['orders'] . " WHERE orderid='". $orderId ."'");
		if(!empty($order_details)) {
			//its a valid order id proceed to see its state.
			$order_state = $order_details['status'];
			if($order_state =='PP'||$order_state =='D') {				
				$amountToBePaid = $order_details['subtotal'] - $order_details['coupon_discount'] - $order_details['cash_redeemed'] + $order_details['cod'] + $order_details['payment_surcharge'] + $order_details['shipping_cost'] - $order_details['discount'] - $order_details['cart_discount'] - $order_details['pg_discount'];
				$amountToBePaid = number_format($amountToBePaid, 2, ".", '');
				$amountToBePaid = $amountToBePaid < 0 ? 0 : $amountToBePaid;
				if($amountToBePaid>0) {
					//return 100 success
					$errorCode = 100;
					$errorMessage = "Success";
					$customer_login = $order_details['login'];
					$customer_mobile = $order_details['mobile'];					
				} else {
					//error condition return error 700 i.e Error has occured: "Amount to be collected is zero"
					$errorCode = 700;
					$errorMessage = "Amount to be collected is zero";
				}
			} else {
				// order is already been queued.
				//error condition return error 700 i.e Error has occured: "Order is already placed with the system"
				$errorCode = 700;
				$errorMessage = "Order is already placed or declined with the system, generate a new order";
			}			
		} else {
			//error condition return error 200 i.e no transaction found for the mentioned booking ID
			$errorCode = 200;
			$errorMessage = "No transaction found for the mentioned booking ID";
		}		
	} else {
		//error condition return error 700 i.e Error has occured: "Invalid booking id entered, booking id should be numeric only"
		$errorCode = 700;
		$errorMessage = "Invalid booking id entered, booking id should be numeric only";
	}
} else {
	//error condition return error 700 i.e Error has occured: "Invalid Access"
	$errorCode = 700;
	$errorMessage = "Invalid Access";
}
$xmlToReturn=null;
if($errorCode==100){
	$xmlToReturn = getXMLToReturn($orderId,$customer_login,$customer_mobile,$amountToBePaid,$errorCode,$errorMessage);
} else {
	$xmlToReturn = getXMLToReturn("null","null","null","null",$errorCode,$errorMessage);	
}
echo $xmlToReturn;

?>