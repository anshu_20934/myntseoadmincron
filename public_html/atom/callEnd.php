<?php
require_once("../authAPI.php");
include_once("$xcart_dir/include/class/class.orders.php");
include_once ("$xcart_dir/modules/coupon/database/CouponAdapter.php");
include_once("$xcart_dir/include/func/func.mail.php");
include_once("$xcart_dir/include/func/func.sms_alerts.php");
include_once("$xcart_dir/include/func/func.mk_orderbook.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/include/func/func.mkcore.php");

include_once "$xcart_dir/modules/myntCash/MyntCashTransaction.php";
include_once "$xcart_dir/modules/myntCash/MyntCashService.php";


function getXMLToReturn($orderID,$login,$cust_mobile,$errorCode,$errorMsg) {
	$newsXML = new SimpleXMLElement("<order_post_details></order_post_details>");
	$newsXML->addChild('trans_id',$orderID);
	$newsXML->addChild('customer_id',$login);
	$newsXML->addChild('response_id',$cust_mobile);
	$newsXML->addChild('error_code',$errorCode);
	$newsXML->addChild('error_msg',$errorMsg);
	return $newsXML->asXML();
}

$errorCode = 700;
$errorMessage = "Unauthorised usage";

if ($REQUEST_METHOD == "GET"){
	$toLog = array();
	$toLog['time_return'] = time();
	$orderId = $HTTP_GET_VARS['cN'];	
	$orderId = trim($orderId);
	$txnId = $HTTP_GET_VARS['TxnID'];
	$txnId = trim($txnId);
	$toLog['bank_transaction_id'] = mysql_escape_string($txnId);
	$txnDateTime = $HTTP_GET_VARS['TxnDateTime'];
	$txnDateTime = trim($txnDateTime);
	$toLog['time_transaction'] = mysql_escape_string(strtotime($txnDateTime));
	$amountPaid = $HTTP_GET_VARS['Amount'];
	$amountPaid = trim($amountPaid);
	$toLog['amountPaid'] = mysql_escape_string($amountPaid);
	$pgId = $HTTP_GET_VARS['PaymentGatewayID'];
	$pgId = trim($pgId);
	$toLog['gateway_payment_id'] = mysql_escape_string($pgId);
	$rrn = $HTTP_GET_VARS['RRN'];
	$rrn = trim($rrn);
	$txnStatus = $HTTP_GET_VARS['TxnStatus'];
	$txnStatus = trim($txnStatus);
	$toLog['response_message'] = mysql_escape_string($txnStatus);
	$authCode = $HTTP_GET_VARS['AuthCode'];
	$authCode = trim($authCode);
	$toLog['response_code'] = mysql_escape_string($authCode);
	$toLog['is_tampered'] = 0;
	$toLog['is_complete'] = 0;
	$toLog['completed_via'] = "IVR";
	
	if(empty($orderId)){
		// return error 300 � Booking Id is empty � null
		$errorCode = 300;
		$errorMessage = "Booking Id is empty";
	} else if (empty($txnId)){
		$errorCode = 301;
		$errorMessage = "Transaction ID is empty";
	} /*else if (empty($txnDateTime)) {
		$errorCode = 302;
		$errorMessage = "Transaction Date Time is empty";
	} */else if(empty($amountPaid)){
		$errorCode = 303;
		$errorMessage = "Amount is empty";
	} /*else if(empty($pgId)){
		$errorCode = 304;
		$errorMessage = "Payment Gateway ID is empty";
	} else if(empty($rrn)){
		$errorCode = 305;
		$errorMessage = "RRN is empty";
	} else if(empty($authCode)){
		$errorCode = 306;
		$errorMessage = "Authorization Code is empty";
	} */else if(empty($txnStatus)){
		$errorCode = 307;
		$errorMessage = "Transaction Status is empty";
	} else if(!is_numeric($orderId)){
			$errorCode = 700;
			$errorMessage = "Invalid booking id entered, booking id should be numeric only";
	} else {
		//All well start processing		

		$adapter = CouponAdapter::getInstance();
		
		$orderId = mysql_escape_string($orderId);
		$order_details = func_query_first("SELECT * FROM " . $sql_tbl['orders'] . " WHERE orderid='". $orderId ."'");
		if(!empty($order_details)) {
			$order_state = $order_details['status'];
			if($order_state =='PP') {				
				$amountToBePaid = $order_details['subtotal'] - $order_details['coupon_discount'] - $order_details['cash_redeemed'] + $order_details['cod'] + $order_details['payment_surcharge'] + $order_details['shipping_cost'] - $order_details['discount'] - $order_details['cart_discount'] - $order_details['pg_discount'];
				$amountToBePaid = number_format($amountToBePaid, 2, ".", '');
				$amountToBePaid = $amountToBePaid < 0 ? 0 : $amountToBePaid;
				$toLog['amountToBePaid'] = $amountToBePaid;
				if($amountToBePaid>0) {
					if($txnStatus=="OK"){
						if($amountPaid > $amountToBePaid-1) {
							$session_id = func_query_first_cell("select sessionid from mk_temp_order where orderid='$orderId'");
							if(!empty($session_id)){								
								$couponCode = $order_details['couponcode'];
								$userName = $order_details['login'];
								$customer_mobile = $order_details['mobile'];
								$cashCouponCode=$order_details['cash_coupon_code'];
								$cashdiscount=$order_details['cash_redeemed'];	
								$customerFName = $order_details['firstname'];
								$customerMobileNo = $order_details['issues_contact_number'];	
								$productsInCart = func_create_array_products_of_order($orderId,null);	    
							    // Update the coupon usage, if it hasn't already been updated for this order.
								if(!empty($couponCode)) {							    									    
							    	$adapter->updateUsageByUser($couponCode,
								                                $userName,
								                                $order_details['subtotal'],
								                                $order_details['coupon_discount'],
								                                $orderId);
					
				                	// Unlock the coupon.
				                	$adapter->unlockCouponForUser($couponCode, $userName);
								}
								
								/*if(!empty($cashCouponCode) && !empty($cashdiscount)) {	                            	
									$subtotal_after_discount=$order_details['subtotal']-$order_details['coupon_discount'];
	                            	$adapter->updateUsageByUser($cashCouponCode,
	                                                        $userName,
	                                                        $subtotal_after_discount,
	                                                        $cashdiscount,
	                                                        $orderId);
	
					                // Unlock the coupon.
	                				$adapter->unlockCouponForUser($cashCouponCode, $userName);
	                        	}*/
	                        	
	                        	if(!empty($order_details['cash_redeemed'])){
	                        		$trs = new MyntCashTransaction($login,MyntCashItemTypes::ORDER, $orderid, MyntCashBusinessProcesses::CASHBACK_USED, 0, 0,$order_details['cash_redeemed'],  "Usage on order no. $orderId");
	                        		$myntCashUsageResponse = MyntCashService::debitMyntCashForOrder($trs,$orderid);
	                        	}
								
								
								$toLog['is_complete'] = 1;
								//Put the order to queued state.
								$orderstatus = "Q";					
								$invoiceid = func_generate_invoice_id($orderId, $orderstatus, $session_id);											
								$msg = "Dear ".$customerFName.",Your order:".$orderId." has been placed successfully. Thank you www.myntra.com" ;			
	        					func_send_sms($customerMobileNo,$msg); 
	        					func_order_confirm_mail("gateway",$productsInCart,$order_details,$customerFName,$userName,$orderId,null,$order_details);
								
								
								// AS part of new wms.. the order should be moved immediately to WP state as there is not going to 
								// be any manual assignment.
								// also move item_status to assign it to OPS Manager which is going to be id 1.
				            	$weblog->info("Move order to WP state. Current Status - $orderstatus.");
								$order_query = "UPDATE $sql_tbl[orders] SET status ='WP' WHERE orderid = $orderId";
								db_query($order_query);
								$order_detail_query = "UPDATE $sql_tbl[order_details] SET item_status = 'A', assignee=1, assignment_time = ".time()." where orderid = $orderId";
								db_query($order_detail_query);
								func_addComment_status($orderid, "WP", "Q", "Auto moving order to WP state from Q", "AUTOSYSTEM");

					        	// All successfully done
					        	// check if this order needs to be split.
					        	$weblog->info("OrderWHManager: start");
							    $whIdToOrderId = OrderWHManager::assignWarehouseForOrder($orderId, $order_details);
							    OrderWHManager::updateInventoryAfterSplit($orderId, $whIdToOrderId, "Order created from $orderId and moved to WP state", "AUTOSYSTEM");
								$weblog->info("OrderWHManager: end");
								
								$errorCode = 100;
								$errorMessage = "Success";
							} else {
								$errorCode = 600;
								$errorMessage = "Error has occured: No session id found corresponding to orderid";
							}
						} else {
							$toLog['is_tampered'] = 1;
							$errorCode = 700;
							$customerFName = $order_details['firstname'];
							$customerMobileNo = $order_details['issues_contact_number'];
							$errorMessage = "Wrong amount is paid by the customer, correct amount is : $amountToBePaid, his order will be rejected";
							$mail_detail = array(
                        		"to"=>'alrt_ebs_fraud_attempt@myntra.com',
                        		"subject"=>"Urgent! Possible user fraud for Orderid:$orderId",
                        		"content"=>"Based on payment gateway verification a suspect order has been placed by $customerFName  $customerMobileNo, orderID = $orderId. 
                        		This order has been declined and will not be processed. 
                        		Further Details are: 
                        		Payment Gateway: ATOM 
                        		Payment Option Selected: IVR 
                        		Amount to be paid: $amountToBePaid 
                        		Amount as returned by Gateway: $amountPaid                        				 
                        		Order ID  = ". $orderId,
                        		"from_name"=>'Myntra Admin',
                        		"from_email"=>'admin@myntra.com',
                        		"header"=>'',
                    		);
    						send_mail_on_domain_check($mail_detail);
							$sql = "UPDATE ".$sql_tbl['orders']." SET status ='F'  WHERE orderid = '".intval($orderid)."' ";  ### Update the table in case fraud happened in EBS gateway
							db_query($sql);
						}
					} else if($txnStatus=="FL" || $txnStatus=="NONE" || $txnStatus=="TO" || $txnStatus=="EA" || $txnStatus=="MR") {
						//transaction failed
						$errorCode = 100;
						$errorMessage = "Success";
						$sql = "UPDATE ".$sql_tbl['orders']." SET tf_status ='TF' WHERE orderid = '".intval($orderId)."' ";  ### Update the table in case transaction get failed 
						db_query($sql);
					} else {
						$errorCode = 700;
						$errorMessage = "Invalid transaction status";
					}
				} else {
					$errorCode = 700;
					$errorMessage = "Amount to be collected is zero";
				}
				//Log enteries here
				func_array2update('mk_payments_log', $toLog,"orderid=$orderId");				
			} else {
				$errorCode = 700;
				$errorMessage = "Order is already placed with the system";
			}
		} else {
			//error condition return error 200 i.e no transaction found for the mentioned booking ID
			$errorCode = 200;
			$errorMessage = "No transaction found for the mentioned booking ID";
		}
	}	
} else {
	//error condition return error 700 i.e Error has occured: "Invalid Access"
	$errorCode = 700;
	$errorMessage = "Invalid Access";
}
$xmlToReturn=null;
if($errorCode==100){
	$xmlToReturn=getXMLToReturn($orderId,$userName,$customer_mobile,$errorCode,$errorMessage);
} else {
	$xmlToReturn=getXMLToReturn("null","null","null",$errorCode,$errorMessage);
}
ob_clean();
Header('Content-type: text/xml');
echo $xmlToReturn;
?>
