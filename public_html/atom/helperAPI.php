<?php 
chdir(dirname(__FILE__));
require_once '../auth.php';
$atomURL = "http://203.114.240.77/paynetz/epi/fts";
$atomLogin = "15";
$atomPassword = "Test@123";
$prodID = "NSE";
$currency = "INR";
$txnsamnt = "0"; //Transaction Service Charge Amount. Charged by the merchant.
$clientCode = urlencode(base64_encode("myntra"));
$custacc = "0253143165113216";
$bankMapping = array("atomBank"=>"6528");
function initiateAtomNetBankingTransaction($orderid,$amount,$bankName){
	global $atomURL,$atomLogin,$atomPassword, $prodID, $currency, $txnsamnt, $clientCode, $custacc, $bankMapping;
	$ch = curl_init();
	$today = date("d/m/Y H:i:s");
	$bankID = $bankMapping[$bankName];
	$data = array("login"=>$atomLogin,
					"pass"=>$atomPassword,
					"ttype"=>"NBFundTransfer", 
					"prodid"=>$prodID,
					"amt"=>$amount,
					"txncurr" => $currency,
					"txnscamt" => $txnsamnt,
					"clientcode" => $clientCode,
					"txnid" => $orderid,
					"date" => $today,
					"custacc" => $custacc
					);
					
	if(!empty($bankID)) {
		$data["bankid"] = $bankID;
	}
	
	curl_setopt($ch, CURLOPT_URL,$atomURL);
    curl_setopt($ch, CURLOPT_FAILONERROR,1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_TIMEOUT, 5);
	
	$retValue = curl_exec($ch); 
	//echo "return xml is $retValue\n";
	curl_close($ch);
	try {
		$retValArray = new SimpleXMLElement($retValue);
		$responseObject = $retValArray->MERCHANT->RESPONSE;
		$toReturn = array();
		$toReturn["url"] = (string)$responseObject->url;
		foreach ($responseObject->param as $object) 		
		{
			$key = (string)$object['name'];
			$toReturn[$key] = (string)$object;			
		}
		
		return $toReturn;
	} catch (Exception $e) {
		return false;
	}	
}

$testData = initiateAtomNetBankingTransaction("1234567","113.23","atomBank"); 
//print_r($testData);
//exit;
if(!empty($testData)) {
?>
	<body onload="javascript: submitForm();">
		<center><img src="<?php echo $secure_cdn_base; ?>/images/image_loading.gif"></center>
		<br>
		<center><b>Please wait ... Redirecting to Bank Page</b></center>
		<form name="paymentformforatom" method="get" action="<?php echo $testData['url']; ?>" >		
			<?php foreach ($testData as $key=>$value) { 
				if($key != "url") {
					$value = urldecode($value);
			?>				
				<input type="hidden" name="<?php echo $key ?>" value="<?php echo $value ?>"/>
			<?php 
				}
			}
			?>
		</form>	
	
		<script type="text/javascript">
			 function submitForm() {
				 document.paymentformforatom.submit();
			 }
		</script>
	</body>		
<?php 	
}
?>