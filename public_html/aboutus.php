<?php
require "./auth.php";

if ($skin == 'skin2') {
    header('Location: aboutus#top');
    exit;
} 

// Omniture Tracking, old UI
$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setStaticPageVars(AnalyticsBase::ABOUT_US);
$analyticsObj->setTemplateVars($smarty);

func_display("aboutus.tpl",$smarty);
?>