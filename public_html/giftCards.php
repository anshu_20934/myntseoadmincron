<?php
use giftcard\GiftCardMessage;
use giftcard\GiftCardOccasion;
use giftcard\GiftCard;
use giftcard\manager\MCartGiftCard;
use giftcard\manager\MCartGiftCardItem;
use enums\cart\CartContext;
include_once("./auth.php");
require_once($xcart_dir."/modules/giftcards/GiftCardsHelper.php");
include_once "$xcart_dir/include/class/class.mymyntra.php";
include_once "$xcart_dir/include/class/mcart/class.MCart.php";
include_once "$xcart_dir/include/class/mcart/class.MCartUtils.php";
use seo\SeoStrategy;
use seo\SeoPageType;
use macros\PageMacros;
global $telesales;
#if ($telesales){
#    echo "Access not allowed";exit; 
#}
if(!$giftingEnabled){
	include_once $xcart_dir."/include/func/func.core.php";
	func_header_location($https_location,false);	
}

$pageName = 'cart';
include_once $xcart_dir."/webengage.php";

$occasions=GiftCardsHelper::getAllOccasions();
/* Custom Sorting function to sort the occasions by displayOrder */
function cmpDisplayOrderForGiftCardOccasions($a, $b){
	 if ($a->displayOrder == $b->displayOrder) {
        return 0;
    }
    return ($a->displayOrder < $b->displayOrder) ? -1 : 1;
}

usort($occasions, "cmpDisplayOrderForGiftCardOccasions");

$login = $XCART_SESSION_VARS['login'];

$recipient_name = "Recipient Name";
$recipient_email = "someone@example.com";
$sender_name = "Sender Name";
$message = "";
$amount = 500;
$defaultvalues=true;
if(!empty($login)){
	$mymyntra = new MyMyntra(mysql_real_escape_string($login));
	$userProfileData = $mymyntra->getBasicUserProfileData($login);
	$mcartFactory= new \MCartFactory();
	$myCart= $mcartFactory->getCurrentUserCart(true, CartContext::GiftCardContext);
	$products=$myCart->getProducts();
	if(!empty($products)){
		$giftcard=$products["giftcard"];
		$recipient_name = $giftcard->giftCardDetails->recipientName;
		$recipient_email = $giftcard->giftCardDetails->recipientEmail;
		$sender_name = $giftcard->giftCardDetails->senderName;
		if(empty($sender_name)){
			$sender_name = $userProfileData["firstname"] . " " . $userProfileData["lastname"];	
		}
		$sender_email = $login;		
		$message = $giftcard->giftCardDetails->giftCardMessage->message;		
		$occasion = $giftcard->giftCardDetails->giftCardMessage->giftCardOccasion->id;
		$amount = $giftcard->giftCardDetails->amount;
		$defaultvalues=false;
	}
	else{
		$occasionDisplayOrder = 1;
	}
}
else{
	$occasionDisplayOrder = 1;
}

$occasionObj = null;
foreach($occasions as $occkey => $occval){
	if($occval->displayOrder == $occasionDisplayOrder || $occval->id == $occasion){
		$occasionObj = $occval;
		$occasion = $occval->id;
		break;
	}
}
if(empty($message)){
	$message = $occasionObj->defaultMessage;
}
$message_chars_left_count = 100 - strlen($message);

//Macros
$macros = new PageMacros('STATIC');
$macros->url = currentPageURLWithStripedQueryStringsAndPort();
$macros->page_type = SeoPageType::STATIC_PAGE;
$macros->page_name = 'Gift Cards';

$seoEnabled=FeatureGateKeyValuePairs::getBoolean('SeoV3Enabled');
if($seoEnabled){
    try {
		$handle = Profiler::startTiming("seov3_load");
        $seoStrategy = new SeoStrategy($macros);
        Profiler::endTiming($handle);
        $smarty->assign("seoStrategy",$seoStrategy);
    } catch (Exception $e) {
        $seolog->error($staticPage." :: ".$e->getMessage());
    }
}

$smarty->assign("message_chars_left_count", $message_chars_left_count);
$smarty->assign("sender_name", substr($sender_name, 0, 15));
$smarty->assign("sender_email", $sender_email);
$smarty->assign("recipient_name", substr($recipient_name, 0, 15));
$smarty->assign("recipient_email", $recipient_email);
$smarty->assign("message", $message);
$smarty->assign("occasion", $occasion);
$smarty->assign("amount", $amount);
$smarty->assign("occasionObj", $occasionObj);
$smarty->assign("gc_occasion", $occasionObj);


$type = 0;
$quantity = 1;
$giftCard = new GiftCard();
$giftCard->setAmount($amount);
$giftCard->setQuantity($quantity);
$giftCard->setType($type);
$giftCard->setSenderEmail($login);
$giftCard->setRecipientEmail($recipient_email);
$giftCard->setSenderName($sender_name);
$giftCard->setRecipientName($recipient_name);
$giftCardMessage = new GiftCardMessage();
$giftCardMessage->setMessage($message);
$giftCardOccasion = new GiftCardOccasion();
$giftCardOccasion->setId($occasion);
$giftCardMessage->setGiftCardOccasion($giftCardOccasion);
$giftCard->setGiftCardMessage($giftCardMessage);

$smarty->assign("gc", $giftCard);
$smarty->assign("defaultvalues", $defaultvalues);

$smarty->assign("pdpMiniNav",true);
$smarty->assign("occasions",$occasions);
$smarty->assign("occasions_json",json_encode($occasions));

$smarty->display("giftcards/giftcardslandingpage.tpl");

?>
