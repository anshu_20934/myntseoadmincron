<?php
$excludedArticleTypes = WidgetKeyValuePairs::getWidgetValueForKey('cart-tooltip-for-coupon');
$smarty->assign("couponExclusionTT", $excludedArticleTypes);
// My Coupons.
$my_coupons = $couponAdapter->getCouponsForUserInMyMyntra($userProfileData['login']);
$today = strtotime(date("Y-m-d H:i:s"));
$active_coupons=array();
$expired_coupons=array();
$used_locked_coupons=array();
$displayPRVFaqs = 'false';
foreach($my_coupons as $key=>$value){
    if(ceil($value['expire']/86400) < ceil($today/86400)-1 && $value['status'] != 'U'){
        $expired_coupons[]=$value;
    }
    else if($value['status']=='A' && (($value['isInfinite']) && ($value['times_used'] <= $value['maxUsageByUser']) || ($value['isInfinite'] ||(($value['times_used'] + $value['times_locked']) < $value['maxUsageByUser'])))){
		$active_coupons[]=$value;
        // check if any active coupon belongs to group "PRV"
        // portal: 1423
        if($value['groupName'] == "PRV")	{
        	$displayPRVFaqs = 'true';
        }
    }
    else{
        $used_result = array();
        if($value['coupon'] != null && $value['coupon'] != "") {
    	   $used_result=func_query("SELECT date, orderid, login, status
                     FROM " . $sql_tbl['orders'] . "
                        WHERE coupon = '".$value['coupon']."'
                        AND status in ('PP','C', 'Q', 'PV', 'OH', 'PD','SH','WP') order by date desc;");
        }
            if(!empty($used_result[0]))
        	   $value['used_on']=$used_result[0]['date'];
		      $used_locked_coupons[]=$value;
        
	}
}

$condNoDiscountCouponFG = FeatureGateKeyValuePairs::getBoolean('condNoDiscountCoupon');
$smarty->assign('condNoDiscountCouponFG', $condNoDiscountCouponFG);

$styleExclusionFG = FeatureGateKeyValuePairs::getBoolean('styleExclusion');
$smarty->assign('styleExclusionFG', $styleExclusionFG);

$smarty->assign("my_coupons", $my_coupons);
$smarty->assign("active_coupons", $active_coupons);
$smarty->assign("expired_coupons", $expired_coupons);
$smarty->assign("used_locked_coupons", $used_locked_coupons);
$smarty->assign("today", $today);
// setting expiry date for cashback coupons to 1/1/2037.
$cashbackDay = mktime(0,0,0,1,1,2037);
$smarty->assign("cashbackexpirydate",$cashbackDay);
$smarty->assign("userProfileData", $userProfileData);
$smarty->assign("displayPRVFaqs", $displayPRVFaqs);

$smarty->assign("view",$view);
$smarty->assign("login",$login);

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setContextualPageData(AnalyticsBase::MYMYNTRA);
$analyticsObj->setTemplateVars($smarty);

$tracker->fireRequest();

$myntraShopFestCoupons = FeatureGateKeyValuePairs::getBoolean("myntrashoppingfestCoupons");
$smarty->assign('myntraShopFestCoupons', $myntraShopFestCoupons);
$shopFestCouponGroupName = WidgetKeyValuePairs::getWidgetValueForKey("myntraFestCouponGroupName");
$smarty->assign("shopFestCouponGroupName",$shopFestCouponGroupName);