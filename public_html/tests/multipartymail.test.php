<?php
require_once '../auth.php';
require_once $xcart_dir.'/include/class/class.mail.multiprovidermail.php';

$email_details['to']="parveen.sharma@myntra.com";//kundan.burnwal@gmail.com";
$email_details['subject']="Recovery of your Myntra account password";
$email_details['content']= "Dear Customer,<br><br>".
							"You requested help with your Myntra account password.".
							"Please click the link below to set your new password.<br><br>".
							"<b><a href='http://www.myntra.com/reset_password.php?key=a748f0996cfaeb9d09d7f02e97fc17bf'>http://www.myntra.com/reset_password.php?key=a748f0996cfaeb9d09d7f02e97fc17bf </a></b> <br><br>". 
							"Please ignore this email if it wasn't you who requested help with your password . Your current password will remain unchanged.<br><br>".
							"Myntra Customer Support<br><a href='http://www.myntra.com'>www.myntra.com</a><br>";
$email_details['from_email']="no-reply@myntra.com";
$email_details['from_name']="Myntra no-reply";


/*$mail_details = array( 
						"to" => "kundan.burnwal@myntra.com",
						"subject" => "Order Amount Discrepancy: $orderid",
						"content" => "This order is put on HOLD dude discrepancy in order total and item amount calculation.",
						"mail_type" => MailType::CRITICAL_TXN
					);
*/	$multiPartymailer = new MultiProviderMailer($email_details);
	echo "Sent mail:".$multiPartymailer->sendMail();
	
	