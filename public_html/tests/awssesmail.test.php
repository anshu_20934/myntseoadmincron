<?php
require_once "../auth.php";
require_once "$xcart_dir/include/class/class.mail.amazonsesmailer.php";
$email_details['to']="kundan.burnwal@gmail.com";
$email_details['subject']="Recovery of your Myntra account password";
$email_details['content']= "Dear Customer,<br><br>".
							"You requested help with your Myntra account password.".
							"Please click the link below to set your new password.<br><br>".
							"<b><a href='http://www.myntra.com/reset_password.php?key=a748f0996cfaeb9d09d7f02e97fc17bf'>http://www.myntra.com/reset_password.php?key=a748f0996cfaeb9d09d7f02e97fc17bf </a></b> <br><br>". 
							"Please ignore this email if it wasn't you who requested help with your password . Your current password will remain unchanged.<br><br>".
							"Myntra Customer Support<br><a href='http://www.myntra.com'>www.myntra.com</a><br>";
$email_details['from_email']="no-reply@myntra.com";
$email_details['from_name']="Myntra no-reply";

try {	
$mailer = new AmazonSESMailer();
print_r($email_details);
/*
echo "verifying from_email: ";
$mailer->verifyEmail($email_details['from_email']);*/
/*echo "daily sent quota:";
print_r($mailer->getDailySendQuota());
*/
$start = time();
$ret = $mailer->sendMail($email_details);
$end = time();

echo "Mail sent:".$ret."Time taken:".($end-$start)."\n";

} catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
    echo $e->getTraceAsString();
}
?>
