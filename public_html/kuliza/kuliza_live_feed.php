<?php 
include_once("../env/Minify.skin2.config.php");
$jsMin = MinifyConfig_skin2::$conf["groups"]["kuliza"]["mingz_js"];
$cssMin = MinifyConfig_skin2::$conf["groups"]["kuliza"]["mingz_css"];
?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	<title>Myntra Social Feed</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<meta http-equiv="Content-Script-Type" content="text/javascript"/>
        <meta name="description" content="Online Shopping Store in India for Branded Shoes, Clothing, Accessories for Men and Women. Cash on Delivery, Free Shipping and 30 day returns"/>
        <meta name="keywords" content="Online Shopping India, Casual Shoes, Clothing, Accessories, Fashion wear, Sporting Goods, Branded apparel, T-shirt, Footwear, watches, kurtas, kurtis, formal wear, shirts, pants, trousers"/>
        <meta name="author" content="Ravenclaw"/>
        <meta property="og:title" content="Myntra Social feed. See what’s trending on the social network at Myntra.com"/>
        <meta property="og:description" content="Online Shopping Store in India for Branded Shoes, Clothing, Accessories for Men and Women. Cash on Delivery, Free Shipping and 30 day returns"/>
        <meta property="og:url" content="http://www.myntra.com"/>
        <meta property="og:image" content=""/>
        <link href="http://myntra.myntassets.com/<?=$cssMin?>" rel="stylesheet"/>
        <link rel="shortcut icon" href="http://myntra.myntassets.com/skin1/icons/favicon.ico">
        <script>
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-1752831-1']);
            _gaq.push(['_setDomainName', '.myntra.com']);
        </script>
</head>
<body>
    <div id="scrollToTop">&nbsp;</div>
	<div id="wrapper"> 
            <div id="header">
		<div class="headerBlk">
			<a href="" class="logo">&nbsp;</a>
<!--			<ul class="hdNav">
				<li><span class="fBold">2137</span>People are shopping right now</li>
				<li><span class="fBold">298</span>Orders in last 24 hrs</li>
			</ul>-->
		</div><!-- headerBlk -->
		<div class="sortingBlk">
			<div class="fLeft"><a id="live_feed" href="" class="header_links active">LATEST UPDATES</a>  /  <a class="header_links" id="popular" href="">POPULAR</a></div><!-- fLeft -->
			<div class="fRight"><a class="header_links" id="my_activity" href="">MY ACTIVITY</a>  /  <a class="header_links" id="my_location" href=""> MY LOCATION</a></div><!-- fRight -->
		</div><!-- sortingBlk -->
		<div class="filtersBlk">
			<ul class="filters">
				<li>FILTERS:</li>
<!--				<li class="filter_"><span><a href="">&nbsp;</a>Dressberry</span></li>
				<li><span><a href="">&nbsp;</a>Bangalore</span></li>-->
			</ul>
			<a href="" class="clearAll" >clear All</a>
		</div><!-- filtersBlk -->
		<div class="shadowBlk">
		</div>
            </div>
            <div class="refreshBtn">
               <span class="refreshTxt">Refresh</span><span class="refreshImg">&nbsp;</span>
            </div>
		<div class="productsGridWrap">
                    <div class="noFeed">
                    </div>
                    <ul class="productList">
                    </ul>
                    <div class="scrollLoader">
                        <img src="http://myntra.myntassets.com/images/kuliza/loading.gif"/>
                        <input type="hidden" name="limit" id="limit" value="12"/>
                        <input type="hidden" name="offset" id="offset" value="0"/>
                        <input type="hidden" name="last_timestamp" id="last_timestamp" value=""/>
                        <input type="hidden" name="feed_type" id="feed_type" value="live"/>
                        <input type="hidden" name="city_filter_text" id="city_filter_text" value=""/>
                        <input type="hidden" name="brand_filter_text" id="brand_filter_text" value=""/>
                        <input type="hidden" name="fb_user_id" id="fb_user_id" value=""/>
                        <input type="hidden" name="access_token" id="access_token" value=""/>
                    </div>
		</div><!-- productsGridWrap -->
		<div class="feedComplete">No more feed to show.</div>
	</div><!-- mainWrap -->
        <div class="fbCommentsPopup">
            <a href="#" class="close closePopup">
                &nbsp;
            </a>
		<div class="fbCommentsWrap">
			<div class="fbProduct">
			</div><!--fbProduct -->
			<div class="fbComments">
			</div><!--fbComments -->
			<div class="commentLoader">
				<img src="http://myntra.myntassets.com/images/kuliza/loading.gif"/>
				<div class="commentLoaderText">Loading Comments</div>
			</div>
		</div><!-- fbCommentsWrap -->
	</div><!-- fbCommentsPopup -->
	<div class="overlay"></div>
        <div class="overlayLoader">
            <img src="http://myntra.myntassets.com/images/kuliza/loading.gif"/>
        </div> 
        <div id="fb-root"></div>

    <script language="javascript" type="text/javascript" src="http://myntra.myntassets.com/<?=$jsMin?>"></script>    
    <script language="javascript" type="text/javascript" src="https://myntra-echo.kuliza.com/js/custom.js"></script>
    <script>
        $(window).load(function(){
            // GA tracking page load call
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        });
    </script>
</body>
</html>
