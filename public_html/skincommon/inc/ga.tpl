{literal}
<script type="text/javascript">
var cartGAData = {};
$(document).ready(function(){
    $.ajax({
        url: pdpServiceUrl,
        type: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        data: JSON.stringify(cartItemIds)
    }).done(function(result){
        $.each(result.data,function(key,product){
            cartGAData[product.id] = {};
            cartGAData[product.id].articleType = product.articleType &&  product.articleType.typeName;
            cartGAData[product.id].category = product.masterCategory && product.masterCategory.typeName;
            cartGAData[product.id].brandName = product.brandName || '';
            cartGAData[product.id].gender = product.gender || '';
        }); 
        for(var pid in cartGAData) {
            var actionNlabel = getGAActionAndLabel(cartGAData[pid].category, cartGAData[pid].articleType, cartGAData[pid].gender, cartGAData[pid].brandName);
            _gaq.push([ 
                '_trackEvent',
                'Transaction_conversion', 
                actionNlabel.action.replace(/-/g,' ').toLowerCase(), 
                actionNlabel.label.replace(/-/g,' ').toLowerCase()
            ]);        
        } 
    });
});

</script>
{/literal}

