{include file="macros/base-macros.tpl"}
<script>
    dataLayer.push({ldelim}
        'transactionId' : Myntra.Confirmation.Data.orderId,
        'transactionTotal' : Myntra.Confirmation.Data.totalAmount,
        'transactionShipping' : Myntra.Confirmation.Data.shipping,
        'transactionShippingZipcode' : Myntra.Confirmation.Data.shippingCode,
        'transactionShippingCity' : Myntra.Confirmation.Data.shippingCity,
        'transactionPaymentType': Myntra.Confirmation.Data.paymentOption,
        'transactionPromoCode' : Myntra.Confirmation.Data.couponCode,
        'transactionQuantity' : Myntra.Confirmation.Data.totalQuantity,
        'isFirstOrder' : Myntra.Confirmation.Data.isFirstOrder,
        'orderSociomanticString' : Myntra.Confirmation.Data.orderSociomanticString,
        'orderNanigansString' : Myntra.Confirmation.Data.orderNanigansString,
        'CartProductId1' : Myntra.Confirmation.Data.CartProductId1,
        'CartProductAmount1' : Myntra.Confirmation.Data.CartProductAmount1,
        'CartProductQuantity1' : Myntra.Confirmation.Data.CartProductQuantity1,
        'CartProductId2' : Myntra.Confirmation.Data.CartProductId2,
        'CartProductAmount2' : Myntra.Confirmation.Data.CartProductAmount2,
        'CartProductQuantity2' : Myntra.Confirmation.Data.CartProductQuantity2,
        'CartProductId3' : Myntra.Confirmation.Data.CartProductId3,
        'CartProductAmount3' : Myntra.Confirmation.Data.CartProductAmount3,
        'CartProductQuantity3' : Myntra.Confirmation.Data.CartProductQuantity3,
        'CartProductId4' : Myntra.Confirmation.Data.CartProductId4,
        'CartProductAmount4' : Myntra.Confirmation.Data.CartProductAmount4,
        'CartProductQuantity4' : Myntra.Confirmation.Data.CartProductQuantity4,
        'CartProductId5' : Myntra.Confirmation.Data.CartProductId5,
        'CartProductAmount5' : Myntra.Confirmation.Data.CartProductAmount5,
        'CartProductQuantity5' : Myntra.Confirmation.Data.CartProductQuantity5,
    {foreach from=$productsInCart key=idx item=item}
            'transactionItem{$idx}' : {$item.productStyleId},
    {/foreach}
        'transactionProducts'   :Myntra.Confirmation.Data.products,
        'transactionProductIds' :Myntra.Confirmation.Data.productIds,
    {rdelim});
</script>
