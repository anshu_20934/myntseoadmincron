{include file="macros/base-macros.tpl"}


<script>
    dataLayer.push({ldelim}
        'productStyleId':Myntra.PDP.Data.id,
        'productVendorId':Myntra.PDP.Data.vendorId,
        'productDisplayName':Myntra.PDP.Data.name,
        'productBrandName':Myntra.PDP.Data.brand,
        'productArticleType':Myntra.PDP.Data.articleType,
        'productSubCategory':Myntra.PDP.Data.subCategory,
        'productCategory':Myntra.PDP.Data.category,
        'productSmallImage':Myntra.PDP.Data.searchImage,
        'productBigImage':Myntra.PDP.Data.image,
        'productPrice':Myntra.PDP.Data.price,
        'productDiscount':Myntra.PDP.Data.discount,
        'productPriceAfterDiscount' : Myntra.PDP.Data.price-Myntra.PDP.Data.discount,
        'productCleanURL':Myntra.PDP.Data.cleanURL,
        'productInStock':Myntra.PDP.Data.inStock,
    {rdelim});

$(document).ready(function(){
    var actionNlabel = getGAActionAndLabel(gaEventCategory,gaEventArticleType,gaEventGender,gaEventBrand);

    Myntra.PDP.gaConversionData = {
        action: actionNlabel.action,
        label: actionNlabel.label
    };
    _gaq.push(['_trackEvent','PDP_conversion', actionNlabel.action, actionNlabel.label, 0, true]);
});

</script>
