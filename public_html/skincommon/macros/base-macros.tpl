<script>
    dataLayer.push({ldelim}
        'pageName': Myntra.Data.pageName,
        'source': Myntra.Data.UTM.Source,
        'medium': Myntra.Data.UTM.Medium,
        'campaign': Myntra.Data.UTM.Campaign,
        'campaignId': Myntra.Data.UTM.CampaignId,
        'octaneUserHash': Myntra.Data.UTM.OctaneEmail,
        'userEmail': Myntra.Data.userEmail,
        'userHashId': Myntra.Data.userHashId,
        'userFBId': Myntra.Data.userFBId,
        'deviceName' : Myntra.Data.Device.Name,
        'deviceType' : Myntra.Data.Device.Type,
        'isLoggedIn' : Myntra.Data.isLoggedIn,
        'isBuyer' : Myntra.Data.isBuyer,
        'isBuyerCookie' : Myntra.Data.isBuyerCookie
    {rdelim});
</script>
