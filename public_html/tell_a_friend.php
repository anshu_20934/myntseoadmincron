<?php
require "./auth.php";
include_once("./include/func/func.mail.php");

//sending mail to freind for product recommendation
$producturl =  $_SERVER['HTTP_REFERER'] ;
$name =  'Myntra'; //$HTTP_POST_VARS["send_name"];
$from =  'admin@myntra.com';//$HTTP_POST_VARS["send_from"];
$to   =   $HTTP_POST_VARS["send_to"];

if ($to && $from && $name){
	$purl = "<a href='".$producturl."' >$producturl</a>" ;
	$template = "sendtofriend";
	$args =	array( "PRODUCT_URL" =>$purl,
                   "NAME"  => ucfirst($name)
			);
   sendMessage($template, $args,$to,$from);
   $top_message["content"] = func_get_langvar_by_name("txt_recommendation_sent");
   echo "Your recommendation has been sent!";
}
?>