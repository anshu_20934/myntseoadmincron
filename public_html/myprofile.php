<?php
require_once "$xcart_dir/include/func/func.mkcore.php";
require_once "$xcart_dir/include/func/func.utilities.php";
require_once "$xcart_dir/include/class/class.mymyntra.php";
require_once "$xcart_dir/modules/coupon/mrp/SmsVerifier.php";

$errors = array();
$successMsg = '';
$mymyntra = new MyMyntra(mysql_real_escape_string($login));
$smsVerifier = SmsVerifier::getInstance();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $url = $http_location ."/mymyntra.php?view=myprofile";

    if (!checkCSRF($_POST["_token"], "myprofile")) {
        //$errors[] = 'Invalid Access';
        $errStr = 'Invalid Access';
    }
    else {
        $action = $_POST['_action'];
        if ($action == 'update-profile-data') {
            $mobile = filter_input(INPUT_POST,'mobile',FILTER_SANITIZE_NUMBER_INT);
            $name = filter_input(INPUT_POST,'name',FILTER_SANITIZE_STRING);
            $gender = filter_input(INPUT_POST,'gender-sel',FILTER_SANITIZE_STRING);
            $namearr = MyMyntra::splitFirstLastName($name);
            if (!empty($mobile) && !empty($namearr)){
                $mobile = substr($mobile, -10);
                $data = array (
                    'mobile' => mysql_real_escape_string($mobile),
                    'firstname' => mysql_real_escape_string($namearr[0]),
                    'lastname' => mysql_real_escape_string($namearr[1]),
                	'gender' => mysql_real_escape_string($gender)
                );
                $prev_mobile = func_query_first_cell("select mobile from xcart_customers where login='$login'");
                $actionResult = $mymyntra->updateProfile($data);
                if (isset($actionResult[MyMyntra::$ERROR])) {
                    //$errors[] = $actionResult[MyMyntra::$ERROR];
                    $errStr = $actionResult[MyMyntra::$ERROR];
                }
                else {
                    $successMsg = 'Your contact information has been updated successfully';
                    //global $skipMobileVerification;
                    if ($mobile != $prev_mobile)	{
                        $smsVerifier->addNewMapping($mobile,$login);
                        if($skipMobileVerification){
                            $smsVerifier->skipVerification($mobile,$login);
                        }
                    }
                }
            }
            else {
                //$errors[] = 'Incorrect profile data supplied';
                $errStr = 'Incorrect profile data supplied';
            }
        }
        else if ($action == 'change-password') {
            $old_password = $_POST['old-password'];
            $new_password = $_POST['new-password'];
            $confirm_new_password = $_POST['confirm-new-password'];
            if (empty($old_password) || empty($new_password) || empty($confirm_new_password)) {
                //$errors[] = 'Please enter all the fields';
                $errStr =  'Please enter all the fields';
            }
            else if ($new_password === $confirm_new_password) {
                $actionResult = $mymyntra->changePassword($old_password, $new_password);
                if (isset($actionResult[MyMyntra::$ERROR])) {
                    //$errors[] = $actionResult[MyMyntra::$ERROR];
                    $errStr= $actionResult[MyMyntra::$ERROR];
                }
                else {
                    fireAsync(HostConfig::$portalIDPSessionHost, 'idp/session/clearCache/'.urlencode($login));
                    $successMsg = 'Your password got changed successfully';
                }
            }
            else {
                //$errors[] = 'Supplied new password and confirm new password fields do not match';
                $errStr =  'Supplied new password and confirm new password fields do not match';
            }
            
           
        }
        //MORE INFO UPDATE START
        else if($action == 'update-more-info'){
        	//DO MOR INFO UPDATE HERE
        	$brands=array();
        	$currentBrands=array();
        	$delArray=array();
            $insertArray=array();   
            $interests=array();
        	$currentInterests=array();
        	$delInterstArray=array();
            $insertInterestArray=array();         
        	$DOB = filter_input(INPUT_POST,'sel-year',FILTER_SANITIZE_STRING).'-'.filter_input(INPUT_POST,'sel-month',FILTER_SANITIZE_STRING).'-'.filter_input(INPUT_POST,'sel-date',FILTER_SANITIZE_STRING);
            $relationship = filter_input(INPUT_POST,'sel-rel-status',FILTER_SANITIZE_STRING);
            $employment = filter_input(INPUT_POST,'sel-employment',FILTER_SANITIZE_STRING);
            $education = filter_input(INPUT_POST,'sel-education',FILTER_SANITIZE_STRING);
            
            $brandString=filter_input(INPUT_POST,'_new_brands');
            $interestString=filter_input(INPUT_POST,'_new_interests');
          
			if(!empty($brandString)){
            	$brands =explode(',',filter_input(INPUT_POST,'_new_brands'));
			}
       		if(!empty($interestString)){
            	$interests =explode(',',filter_input(INPUT_POST,'_new_interests'));
			}
			
            $data = array (
                    'DOB' => mysql_real_escape_string($DOB)
            );

            $actionResult = $mymyntra->updateProfile($data);
            //UPDATE PERSONNA TABLE
            $data =array('relationship_status' => mysql_real_escape_string($relationship),
                    'employment' => mysql_real_escape_string($employment),
            		'education' => mysql_real_escape_string($education),
            		'update_from_fb' => 0);
            $userExists=func_query_first_cell("select count(*) from customer_personna where login='$login'");
            if($userExists == 0){
				$data['login']=mysql_real_escape_string($login);
				$personnaresult = $mymyntra->insertPersonna($data);
			}
			else {
				$personnaresult = $mymyntra->updatePersonna($data);
			}
			$currentBrands=$mymyntra->getUserBrandPreferences();
			$currentInterests=$mymyntra->getUserInterests();
            $delArray=array_diff($currentBrands,$brands);//these brands needs to be inserted to history table with flag deleted,and delete from brand preference
    		$insertArray=array_diff($brands,$currentBrands);//these are new entries to both tables
    		$delInterstArray=array_diff($currentInterests,$interests);//these brands needs to be inserted to history table with flag deleted,and delete from brand preference
    		$insertInterestArray=array_diff($interests,$currentInterests);//these are new entries to both tables
    		$revno = $mymyntra->getRevisionNumber();
    		//echo "<PRE>";print_r($delArray);print_r($insertArray);exit;
    		if(!empty($insertArray)){
    			$insertBrands = $mymyntra->insertBrandPreferences($insertArray, $revno);
    		}
    		if(!empty($delArray)){
    			$deleteBrands = $mymyntra->deleteBrandPreferences($delArray, $revno);  
    		}
    		  		
    		if(!empty($insertInterestArray)){
    			$insertInterests = $mymyntra->insertInterests($insertInterestArray, $revno);
    		}
    		if(!empty($delInterstArray)){
    			$deleteInterests = $mymyntra->deleteInterests($delInterstArray, $revno);  
    		}
    		
    		
            if (isset($actionResult[MyMyntra::$ERROR])) {
                //$errors[] = $actionResult[MyMyntra::$ERROR];
                $errStr = $actionResult[MyMyntra::$ERROR];
            }
            else {
                $successMsg = 'Profile has been updated successfully';
            }
       	
        }
        //MORE INFO END ABOVE
    }
    if (!empty($errStr))
        $url = $url . "&error=".$errStr;
    if (!empty($successMsg))
         $url = $url . "&message=".$successMsg;
    func_header_location($url,false);
}

$userProfileData = $mymyntra->getUserProfileData();
$userPersonnaData = $mymyntra->getUserPersonnaData();
$userProfileData['name'] = trim($userProfileData['firstname'] . ' ' . $userProfileData['lastname']);
$addresses = $mymyntra->getUserAddresses();
$defaultAddress = count($addresses) ? $addresses[0] : array();
foreach ($addresses as $address) {
    if (!empty($address['default_address'])) {
        $defaultAddress = $address;
        break;
    }
}

// Mobile verification status.
$mobile_status = $smsVerifier->getStatus($userProfileData['mobile'], $userProfileData['login']);

if ($skipMobileVerification) {
    $smsVerifier->skipVerification($userProfileData['mobile'], $userProfileData['login']);
    $mobile_status = $smsVerifier->getStatus($userProfileData['mobile'], $userProfileData['login']);
}

//GET USER PREFERENCES AND INTERESTS FROM DB
$months=array('JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC');
$relationship_status=array('single','married');
$educationArr=array('still studying','higher secondary certificate','bachelors','masters','PhD');
$employmentArr=array('self employed','work for private company','work for public company','retired','student','services');

//BRANDS 
$userBrands=$mymyntra->getUserBrandPreferences();
$brandsResults=array();
$brandsResults = $mymyntra->addBrandsToCache();

foreach($brandsResults as $key=>$brands){
	if(in_array($key,$userBrands)){
		$brandsResults[$key][1]=1;
	}
	else {
		$brandsResults[$key][1]=0;
	}
}

//INTERESTS

$userInterests=$mymyntra->getUserInterests();
$interestsResults=array();
$interestsResults = $mymyntra->addInterestsToCache();

foreach($interestsResults as $key=>$interests){
	if(in_array($key,$userInterests)){
		$interestsResults[$key][1]=1;
	}
	else {
		$interestsResults[$key][1]=0;
	}
}
///echo "<PRE>";print_r($userInterests);exit;
$getMsg = filter_input(INPUT_GET,'message',FILTER_SANITIZE_STRING);
$getErr = filter_input(INPUT_GET,'error',FILTER_SANITIZE_STRING);
if (!empty($getErr))
    $errors[]=$getErr;
if (!empty($getMsg)){
    $successMsg=$getMsg;
}
$smarty->assign('existingBrands',json_encode($userBrands));
$smarty->assign('userBrands',$userBrands);
$smarty->assign('brandsResults',$brandsResults);
$smarty->assign('existingInterests',json_encode($userInterests));
$smarty->assign('userInterests',$userInterests);
$smarty->assign('interestsResults',$interestsResults);
$smarty->assign("months", $months);
$smarty->assign("relationship_status", $relationship_status);
$smarty->assign("education", $educationArr);
$smarty->assign("employment", $employmentArr);
$smarty->assign("paySerHost",HostConfig::$paymentServiceHost);
$smarty->assign("errors", $errors);
$smarty->assign("successMsg", $successMsg);
$smarty->assign("addresses", $addresses);
$smarty->assign("defaultAddress", $defaultAddress);
$smarty->assign("userProfileData", $userProfileData);
$smarty->assign("userPersonnaData", $userPersonnaData);
$smarty->assign("mobile_status", $mobile_status);
$smarty->assign("showSection",filter_input(INPUT_GET,'show',FILTER_SANITIZE_STRING));
$smarty->display("my/profile.tpl");

