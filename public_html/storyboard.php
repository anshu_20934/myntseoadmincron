<?php
use enums\pageconfig\PageConfigSectionConstants;
use enums\pageconfig\PageConfigLocationConstants;
use pageconfig\manager\CachedBannerManager;
require_once "auth.php";

$storyboards = CachedBannerManager::getCachedBanners(PageConfigLocationConstants::StoryBoard, PageConfigSectionConstants::StaticImage, "");
//echo "<PRE>";print_r($storyboards);

$smarty->assign('storyboards',$storyboards);
