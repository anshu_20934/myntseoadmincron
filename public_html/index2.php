<?php
use enums\pageconfig\PageConfigSectionConstants;
use enums\pageconfig\PageConfigLocationConstants;
use pageconfig\manager\CachedBannerManager;
use web\utils\MobileConstants;
use web\utils\DeviceDetector;
use abtest\MABTest;
use seo\SeoStrategy;
use seo\SeoPageType;
use macros\PageMacros;

require_once "auth.php";
require_once "$xcart_dir/include/class/instrumentation/BaseTracker.php";
include_once("$xcart_dir/include/class/class.feature_gate.keyvaluepair.php");
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");
$tracker = new BaseTracker(BaseTracker::HOME_PAGE);

$pageName = 'home';
include_once $xcart_dir."/webengage.php";
include_once $xcart_dir."/gtm.php";

$email_uuid = filter_input(INPUT_GET,'gc',FILTER_SANITIZE_STRING);
$smarty->assign("email_uuid", $email_uuid);

if(!empty($login) && !empty($email_uuid)){
	require_once($xcart_dir."/modules/giftcards/GiftCardsHelper.php");
	include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
	
	$result = \GiftCardsHelper::getGiftCardInfoForActivation("gc", $email_uuid, null, null);
	if($result->status != "failed"){
		$giftCardOrder = $result;
		$giftCard = $giftCardOrder->giftCard;
		//$returnval=$couponAdapter->creditCashback($login, $giftCard->amount,  "Cashback credit for giftcard from " . $giftCard->senderName . " (" . $giftCard->senderEmail . ")", $giftCardOrder->orderId);
		
		$id=$giftCard->id;
		$amount=$giftCard->amount;
		$giftCardLog = new stdClass();		
		$giftCardLog->activatedBy = $login;
		$giftCardLog->giftCardId = $id;
		$giftCardLog->activatedOn = time();
		$activationResponse=\GiftCardsHelper::logGiftCardActivation($giftCardLog);
		
		if(!empty($activationResponse) && $activationResponse->status === "success"){
			$trs = new MyntCashTransaction($login, MyntCashItemTypes::ORDER, $giftCardOrder->orderId,  MyntCashBusinessProcesses::GIFT_CARD , 0,$giftCard->amount,0 , "ix Cashback credit for giftcard from " . $giftCard->senderName . " (" . $giftCard->senderEmail . ")");
			MyntCashService::creditToUserMyntCashAccount($trs);
			
			//Reset the mymyntcredits if the user has activated a giftcard when he lands on to homepage
			$myMyntraCredits = $couponAdapter->getTotalCredits($login);
			
			/* fetch balance from MyntCashService */
			$myntCashDetails = MyntCashService::getUserMyntCashDetails($login);
			$smarty->assign("myntCashDetails",$myntCashDetails);
			
			$smarty->assign('myMyntraCredits',$myMyntraCredits);
			$cashback=$myntCashDetails["balance"];
		}
		
		
		
		if($retval->status != "failed"){
			setMyntraCookie("_mkgclogged","success" . "*" . $amount . "*" . $cashback);
		}
		else{
			setMyntraCookie("_mkgclogged","failed" . "*" . $retval->message);
		}	
	}
	else{
		setMyntraCookie("_mkgclogged","failed" . "*" . $result->message);
	}
}

$macros = new PageMacros('HOME');
$macros->url = currentPageURLWithStripedQueryStringsAndPort();
$macros->page_type = SeoPageType::STATIC_PAGE;
$macros->page_name = 'Home';

$seoEnabled=FeatureGateKeyValuePairs::getBoolean('SeoV3Enabled');
if($seoEnabled){
    try {
		$handle = Profiler::startTiming("seov3_load");
        $seoStrategy = new SeoStrategy($macros);
        Profiler::endTiming($handle);
        $smarty->assign("seoStrategy",$seoStrategy);
    } catch (Exception $e) {
        $seolog->error("home :: ".$e->getMessage());
    }
}

//Mobile homepage tooltip text - mobile.home_tooltip_text
if($skin==MobileConstants::MOBILE_SKIN){
        $homeTooltipText=WidgetKeyValuePairs::getWidgetValueForKey("mobile.home_tooltip_text");
        if($homeTooltipText != NULL){
                $smarty->assign("homeTooltipText", trim($homeTooltipText));
        }   
        else{   
                $smarty->assign("homeTooltipText", "Discover more products here");
        }
        $homeTooltipDuration=WidgetKeyValuePairs::getWidgetValueForKey("mobile.home_tooltip_duration");

        if($homeTooltipDuration != NULL){
                $smarty->assign("homeTooltipDuration", intval($homeTooltipDuration));
        }
        else{
                $smarty->assign("homeTooltipDuration", 5000);
        }

        $smarty->assign("homepageLayout", true);
}

//AB Test for Recently Viewed
$smarty->assign("recentVariant", MABTest::getInstance()->getUserSegment('homeRecentlyViewed'));

$smarty->assign("enableMostPopular" , FeatureGateKeyValuePairs::getBoolean("enableMostPopular"));

//AB Test for Most Populer
$smarty->assign("mostPopularVariant", MABTest::getInstance()->getUserSegment('homeMostPopularWidget'));

// Feature gate for banner cycle interval
$smarty->assign("bannerCycleInterval", FeatureGateKeyValuePairs::getInteger("homepage.bannerCycle.interval", 5000));

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setContextualPageData(AnalyticsBase::HOME_PAGE);
$analyticsObj->setTemplateVars($smarty);

if($skin==MobileConstants::MOBILE_SKIN && !DeviceDetector::isTablet()){
    $page=PageConfigLocationConstants::MobileHomePage;
}else{
    $page=PageConfigLocationConstants::HomePage;
}
$smarty->assign("fluid", true);//Setting fluid width for mobile home page
$smarty->assign("showSearchInHeader", true);//Show serch box in header

//mobile ads data section
if( DeviceDetector::isMobile() ) {
	$mobileDiscountAdString= WidgetKeyValuePairs::getWidgetValueForKey("mobile.header.promotion.data");
	$mobileDiscountAds=json_decode($mobileDiscountAdString,true,512);
	$mobileHomePageAds = $mobileDiscountAds['home']['offers'];
	$smarty->assign("is_mobile_home","true");
	$smarty->assign("mobile_promotions",$mobileHomePageAds);
}
//mobile ads data section ends.

if (($skin==MobileConstants::MOBILE_SKIN && !DeviceDetector::isTablet())|| $layoutVariant!='test') {
	$slideShowBanners = CachedBannerManager::getCachedBanners($page, PageConfigSectionConstants::SlideshowImage, "");
	$staticBanners = CachedBannerManager::getCachedBanners($page, PageConfigSectionConstants::StaticImage, "");
	$smarty->assign('slideShowBanners',$slideShowBanners);
	$smarty->assign('staticBanners',$staticBanners);
	func_display("index.tpl", $smarty);
} else {

	$newHpBanners = FeatureGateKeyValuePairs::getBoolean('newhpbanners.enabled', false);
	if($newHpBanners){
		$page = PageConfigLocationConstants::NewHomePage;		
	}else{
		$page = PageConfigLocationConstants::NewHomeFeatures;
	}
	$slideShowBanners = CachedBannerManager::getCachedBanners($page, PageConfigSectionConstants::SlideshowImage, "");
	$saleBanners = CachedBannerManager::getCachedBanners($page, PageConfigSectionConstants::StaticImage, "");
	$subBanners = CachedBannerManager::getCachedBanners(PageConfigLocationConstants::NewHomeSubBanners, PageConfigSectionConstants::SlideshowImage, "");
	$staticBanners = CachedBannerManager::getCachedBanners(PageConfigLocationConstants::NewHomeSubBanners, PageConfigSectionConstants::StaticImage, "");
	$multiBannerData = CachedBannerManager::getMultiBannerData($slideShowBanners,$page, PageConfigSectionConstants::SlideshowImage, "");
	$smarty->assign('slideShowBanners',$slideShowBanners);
	$smarty->assign('saleBanners',$saleBanners);
	$smarty->assign('subBanners',$subBanners);
	$smarty->assign('staticBanners',$staticBanners);
    if ($skin==MobileConstants::MOBILE_SKIN)
	    $smarty->display('index.tpl', $smarty);
    else{
    	$smarty->assign('multiBannerData',$multiBannerData);
    	$smarty->display('index2.tpl', $smarty);
    }
}
$tracker->fireRequest();
