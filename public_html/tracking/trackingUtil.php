<?php
/**
 * User: Arun Kumar K
 * Date: 8 Jun, 2011
 * Time: 6:57:20 PM
 */
include_once("$xcart_dir/include/func/func.utilities.php");

/** check one among the products is tshirt or shoe
 * @param:(array)$products | (string)$typeid
 * @return:(bool)$flag
 */
function retargetingPixelOnType($productType,$arrayType=null){
    //all type ids which need to be tracked
    $retargetTypeIds = array(2,288,289);


    //all product type ids and pixel map for which retarget pixel tracking is needed
    $retargetPixelTypeMap = array(
                            "retarget"=>array(
                                            "http"=>array(
                                                        $retargetTypeIds[0]=>"<img src='//a.tribalfusion.com/i.cid?c=397723&d=30&page=landingPage' width='1' height='1' border='0'>",//not mentioning http/s will automatically take care of
                                                        $retargetTypeIds[1]=>"<img src='//a.tribalfusion.com/i.cid?c=398403&d=30&page=landingPage' width='1' height='1' border='0'>",//pixel for shoe
                                                        $retargetTypeIds[2]=>"<img src='//a.tribalfusion.com/i.cid?c=397723&d=30&page=landingPage' width='1' height='1' border='0'>",//pixel for t-shirt
                                                    ),
                                            "https"=>array(
                                                        $retargetTypeIds[0]=>"<img src='//a.tribalfusion.com/i.cid?c=397723&d=30&page=landingPage' width='1' height='1' border='0'>",
                                                        $retargetTypeIds[1]=>"<img src='//a.tribalfusion.com/i.cid?c=398403&d=30&page=landingPage' width='1' height='1' border='0'>",
                                                        $retargetTypeIds[2]=>"<img src='//a.tribalfusion.com/i.cid?c=397723&d=30&page=landingPage' width='1' height='1' border='0'>",
                                                    ),
                                            ),

                            "confirm"=>array(
                                            "http"=>array(
                                                        $retargetTypeIds[0]=>"<img src='http://a.tribalfusion.com/ti.ad?client=397723&ev=1' width='1' height='1' border='0'>",//pixel in confirm page for t-shirt
                                                        $retargetTypeIds[1]=>"<img src='http://a.tribalfusion.com/ti.ad?client=398403&ev=1' width='1' height='1' border='0'>",//for shoe
                                                        $retargetTypeIds[2]=>"<img src='http://a.tribalfusion.com/ti.ad?client=397723&ev=1' width='1' height='1' border='0'>",
                                                    ),
                                            "https"=>array(
                                                        $retargetTypeIds[0]=>"<img src='https://s.tribalfusion.com/ti.ad?client=397723&ev=1' width='1' height='1' border='0'>",
                                                        $retargetTypeIds[1]=>"<img src='https://s.tribalfusion.com/ti.ad?client=398403&ev=1' width='1' height='1' border='0'>",
                                                        $retargetTypeIds[2]=>"<img src='https://s.tribalfusion.com/ti.ad?client=397723&ev=1' width='1' height='1' border='0'>",
                                                    ),
                                            )
                            );

    $existingTypes = array();
    $retargetPixel = '';
    
    //check one among the products is tshirt or shoe
    if(is_array($productType)){

        switch($arrayType){
            //productsInCart structure
            case 'PID': $typeKey = 'producTypeId';
                        break;

            //product List structure(search page)
            case 'PL':  $typeKey = 'typeid';
                        break;
            
            default:    $typeKey = 'typeid';
                        break;
        }

        foreach($productType as $idx=>$product){
            if(in_array($product[$typeKey],$retargetTypeIds)){
                if(!in_array($product[$typeKey],$existingTypes)){
                    $existingTypes[] = $product[$typeKey];    
                }
            }
        }

    } else {//input is typeid
        if(in_array($productType,$retargetTypeIds)){
            $existingTypes[] = $productType;
        }
    }

    //build pixel string
    $retargetHttp = array();
    $retargetHttps = array();
    $confirmHttp = array();
    $confirmHttps = array();
    
    if(!empty($existingTypes)){
        foreach($existingTypes as $idx=>$typeId){
            //to avoid duplication of <img> tag  for different types
            if(!in_array($retargetPixelTypeMap['retarget']['http'][$typeId],$retargetHttp)){                
                $retargetHttp[] = $retargetPixelTypeMap['retarget']['http'][$typeId];
            }
            if(!in_array($retargetPixelTypeMap['retarget']['https'][$typeId],$retargetHttps)){
                $retargetHttps[] = $retargetPixelTypeMap['retarget']['https'][$typeId];
            }
            if(!in_array($retargetPixelTypeMap['confirm']['http'][$typeId],$confirmHttp)){
                $confirmHttp[] = $retargetPixelTypeMap['confirm']['http'][$typeId];
            }
            if(!in_array($retargetPixelTypeMap['confirm']['https'][$typeId],$confirmHttps)){
                $confirmHttps[] = $retargetPixelTypeMap['confirm']['https'][$typeId];
            }
        }
    }

    $retargetPixel['retargetHttp'] = implode('',$retargetHttp);
    $retargetPixel['retargetHttps'] = implode('',$retargetHttps);
    $retargetPixel['confirmHttp'] = implode('',$confirmHttp);
    $retargetPixel['confirmHttps'] = implode('',$confirmHttps);
    return $retargetPixel; 
}

/* tracks utm_source and utm_medium against different event types(order etc)
 * @param:(array)$eventInfo -- this array takes array key as table(mk_tracking_history) column names
 * @param:(string)$condition
 * @return:void
 */
function utmTrackForEvent($eventInfo=array(),$condition=NULL){

    $utm_data = getUnserializedCookieContent('utm_track_1'); 
    if (is_array($utm_data) && !empty($utm_data)) {
        $cookieContent['utm_source'] = $utm_data['utm_source'];
        $cookieContent['utm_medium'] = $utm_data['utm_medium'];
    }

    //no utm source or medium set consider it as direct traffic
    if(empty($cookieContent)){
        $cookieContent['utm_source'] = $cookieContent['utm_medium'] = 'direct';
    }
    //track time
    $eventTime = array('track_time'=>time());

    if(!empty($eventInfo) && is_array($eventInfo)){
        //insert data
        if($condition == NULL){
            $eventInfo += $cookieContent+$eventTime;
            func_array2insert('mk_tracking_history',sanitizeDBValues($eventInfo));

        } else {//update data
            $eventInfo += $cookieContent+$eventTime;
            func_array2update('mk_tracking_history', sanitizeDBValues($eventInfo),$condition);
        }
    }
}
?>
