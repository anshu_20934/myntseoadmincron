<?php
$lt_timeout_cookie_name = 'lt_timeout';
$lt_session_cookie_name = 'lt_session';
$lt_new_session = "false";
$lt_timeout_cookie_content = getMyntraCookie($lt_timeout_cookie_name); 
$lt_session_cookie_content = getMyntraCookie($lt_session_cookie_name);
if(!isset($lt_timeout_cookie_content) || !isset($lt_session_cookie_content)){
	setMyntraCookie($lt_timeout_cookie_name, '1', time() + 60*30, "/",$cookiedomain); // Add timout cookie with 30 mins timeout
	setMyntraCookie($lt_session_cookie_name, '1', 0, "/",$cookiedomain); // Add a session based cookie
	$lt_new_session = "true";
}else{ // Refresh the timout cookie's expires time
	setMyntraCookie($lt_timeout_cookie_name, '1', time() + 60*30, "/",$cookiedomain);
}

$smarty->assign("lt_new_session",$lt_new_session);

if($lt_new_session){
	
	if(!empty($XCART_SESSION_VARS['fb_uid'])){
	    $smarty->assign("lt_fb_connected", "Yes");
    }
	
    if(!empty($XCART_SESSION_VARS['totalQuantity'])){
	    $smarty->assign("lt_cart_status", "Filled");
    }else{
        $smarty->assign("lt_cart_status", "Empty");
    }
    
    if(!empty($login)){
	    $smarty->assign("lt_login_status", "Logged In");
    }else{
        $smarty->assign("lt_login_status", "Not Logged In");
    }
 
}
$enableHeaderMRPData = FeatureGateKeyValuePairs::getBoolean("enableHeaderMRPData", true);
$smarty->assign('enableHeaderMRPData', $enableHeaderMRPData);
if($login && $enableHeaderMRPData){
	if($loyaltyEnabled){
		include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalClient.php";
		include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalHelper.php";
	    LoyaltyPointsPortalHelper::setLoyaltyPointsHeaderDetails($login); // This will put smarty variable 
	}	
	$currentTime = time();
	$headerCacheThreshold = FeatureGateKeyValuePairs::getInteger('headerCacheThreshold', 15*60);
	$lastHeaderCacheTime = $XCART_SESSION_VARS['lastHeaderCacheTime'] ? $XCART_SESSION_VARS['lastHeaderCacheTime'] : $currentTime;
	$headerCacheThresholdBreached = ($currentTime - $lastHeaderCacheTime) > $headerCacheThreshold;

	if($headerCacheThresholdBreached || empty($XCART_SESSION_VARS['mrpData'])){
		// Adding appropriate header message for mrp.
		include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
		include_once "$xcart_dir/modules/myntCash/MyntCashService.php";
		include_once "$xcart_dir/modules/myntCash/MyntCashRawService.php";

		$couponAdapter = CouponAdapter::getInstance();
		$myMyntraCredits = $couponAdapter->getTotalCredits($login);

		/* fetch balance from MyntCashService  */
		/* fetching it from raw service to avoid load on remote service */
		$myntCashDetails = MyntCashRawService::getBalance($login);
		$myMyntraCredits["cashback"] = $myntCashDetails["balance"]; // this to support unseen usage of $myMyntraCredits["cashback"].

		//export these variables to session 
		$mrpData = array();
		$mrpData["myntCashDetails"] = $myntCashDetails;
		$mrpData["myMyntraCredits"] = $myMyntraCredits;
		$mrpData["loyaltyPointsDetails"] = $loyaltyPointsDetails;
		x_session_register("mrpData", $mrpData);
	}else{
	    $mrpData = $XCART_SESSION_VARS['mrpData'];
	    $myntCashDetails = $mrpData["myntCashDetails"];
	    $myMyntraCredits = $mrpData["myMyntraCredits"];
	    $loyaltyPointsDetails = $mrpData["loyaltyPointsDetails"];
	}


	$smarty->assign("myntCashDetails",$myntCashDetails);
	$smarty->assign('myMyntraCredits',$myMyntraCredits);
	$smarty->assign('myntUserCB', $myntCashDetails["balance"]);

    if($headerCacheThresholdBreached){
        $lastHeaderCacheTime = time();
        x_session_register("lastHeaderCacheTime", $lastHeaderCacheTime);
    }

}

?>