<?php
	require "./auth.php";
	include_once("./include/func/func.sms_alerts.php");
	include_once("./include/func/func.user.php");

	//global $smarty;
	global $XCART_SESSION_VARS;
	$hashKey = $_GET['email'];
	if($hashKey != NULL){
		x_session_unregister("loginNameInCouponGeneration");
		$loginQuery = "SELECT login, b_firstname FROM $sql_tbl[customers] WHERE reg_coupon_gen_key = '$hashKey'";
		$loginQueryResult = db_query($loginQuery);
		$row = db_fetch_array($loginQueryResult);
		$login = $row['login'];
		$firstName = $row['b_firstname'];
		x_session_register("loginNameInCouponGeneration", $login);
	}	
	
	if(!isset($_POST['method'])){
		$login = $XCART_SESSION_VARS[loginNameInCouponGeneration];
		if(isset($login)){
			
			
			$couponLoginQuery = "SELECT mobile_number, coupon FROM $sql_tbl[coupon_mobile] WHERE login = '$login'";
			$couponLoginQueryResult = db_query($couponLoginQuery);
			$row = db_fetch_array($couponLoginQueryResult);
			if($row == null || $row[coupon] == null){
				$status = "couponNotGenerated";
				$mobileNumberInTextBox = "+91";
				//$smarty->assign("status", "couponNotGenerated");	
			}else{
				$couponCode = $row['coupon'];
				$mobileNumberInTextBox = $row[mobile_number];
				$couponUsageQuery = "select times_used, times_locked, expire from $sql_tbl[discount_coupons] where coupon = '$couponCode'";
				$couponUsageQueryResult = db_query($couponUsageQuery);
				$row = db_fetch_array($couponUsageQueryResult);
				$times_used = $row[times_used];
				$times_locked = $row[times_locked];
				$expire = $row[expire];
				if($times_used > 0 || $times_locked > 0){
					$status = "alreadyUsed";
					//$smarty->assign("status", "alreadyUsed");
				}
				else if($expire < time()){
					$status = "expired";
					//$smarty->assign("status", "expired");
				}else{
					$status = "alreadyGenerated";
					//$smarty->assign("status", "alreadyGenerated");
				}
			}
		}
		else{ 
			$status = "invalidURL";
			//$smarty->assign("status", "invalidURL");
		}
	}
	if(isset($_POST['method']) && $_POST['method']== 'genereateNewCoupon_couponNotGenerated'){
		$login = $XCART_SESSION_VARS[loginNameInCouponGeneration];
		$mobileNumber = $_POST['mobileFromForm'];
		
		$mobileNumberStatus = checkExistingMobileNumbers($mobileNumber, $login);
		if($mobileNumberStatus != "valid"){
			$status = "mobileNumberUsed";
		}
		else{
			$camapaignCode = 1; //for free mug campaign, for others will have to be changed.
			$couponMobileQuery = "SELECT coupon, login FROM $sql_tbl[coupon_mobile] WHERE mobile_number = '$mobileNumber' and campaign_id = '$camapaignCode'";
			$couponMobileQueryResult = db_query($couponMobileQuery);
			$row = db_fetch_array($couponMobileQueryResult);
			if($row == null || $row[coupon] == null){
				generateNewCoupon($mobileNumber, $login);
				$status = "smsSent";
			}
			else{
				$status = "mobileNumberUsed";
			}
		}
	}
		
	if(isset($_POST['method']) && $_POST['method']== 'genereateNewCoupon_alreadyGenerated'){
		$login = $XCART_SESSION_VARS[loginNameInCouponGeneration];
		$mobileNumber = $_POST['mobileFromForm'];
		$mobileNumberStatus = checkExistingMobileNumbers($mobileNumber, $login);
		if($mobileNumberStatus != "valid"){
			$status = "mobileNumberUsed";
		}
		else{
			$camapaignCode = 1; //for free mug campaign, for others will have to be changed.
			$couponMobileQuery = "SELECT coupon, login FROM $sql_tbl[coupon_mobile] WHERE mobile_number = '$mobileNumber' and campaign_id = '$camapaignCode'";
			$couponMobileQueryResult = db_query($couponMobileQuery);
			$row = db_fetch_array($couponMobileQueryResult);
			if($row[login] == null){
				$couponLoginQuery = "SELECT coupon FROM $sql_tbl[coupon_mobile] WHERE login = '$login'";
				$couponLoginQueryResult = db_query($couponLoginQuery);
				$row = db_fetch_array($couponLoginQueryResult);
				if($row != null && $row[coupon] != null){
					$oldCouponCode = $row[coupon];
					invalidateOldCoupon($oldCouponCode);
					generateNewCoupon($mobileNumber, $XCART_SESSION_VARS[loginNameInCouponGeneration]);
					$status = "smsSent";
				}
			}
			if($row[login] != null && $row[login] != $XCART_SESSION_VARS[loginNameInCouponGeneration]){
				$status = "mobileNumberUsed";
			}
			else if($row[login] != null && $row[login] == $XCART_SESSION_VARS[loginNameInCouponGeneration]){

				$oldCouponCode = $row[coupon];
				invalidateOldCoupon($oldCouponCode);
				generateNewCoupon($mobileNumber, $XCART_SESSION_VARS[loginNameInCouponGeneration]);

				$status = "smsSent";

			}
		}
	}
	
	if(isset($_POST['method']) && $_POST['method']== 'resendCoupon'){
		$login = $XCART_SESSION_VARS[loginNameInCouponGeneration];
		$resendCouponQuery = "select coupon, mobile_number from $sql_tbl[coupon_mobile] where login = '$XCART_SESSION_VARS[loginNameInCouponGeneration]'";
		$resendCouponQueryResult = db_query($resendCouponQuery);
		$row = db_fetch_array($resendCouponQueryResult);
		$couponCode = $row['coupon'];
		$mobileNumber = $row['mobile_number'];
		$status = "smsSent";
		
		func_send_sms_cellnext($mobileNumber, "Your free mug coupon code is : " . $couponCode . " - Myntra");
	}
	
	if(isset($_POST['method']) && $_POST['method']== 'continueWithShopping'){
		
	}
	
	function invalidateOldCoupon($oldCouponCode){
		global $sql_tbl, $sqllog;
		$invalidateOldCouponQuery = "update $sql_tbl[discount_coupons] set times_used = 500, expire = 0 where coupon = '$oldCouponCode'";
	 	db_query($invalidateOldCouponQuery);
		$sqllog->debug("Query ".$invalidateOldCouponQuery."  Filename: reister.php");
	}
	
	function generateNewCoupon($mobileNumber, $login){
		global $sql_tbl, $sqllog;
		
		
		$styleid=14;
		$discount=100;
		$maximum=249;
		$couponCode = func_free_coupon_on_registration($login,$styleid, $discount,$maximum);
		$campaignId = 1;
		
		$deleteOldCouponQuery = "DELETE FROM $sql_tbl[coupon_mobile] where login = '$login'";
		db_query($deleteOldCouponQuery);
		$sqllog->debug("Query ".$deleteOldCouponQuery."  Filename: reister.php");
		
		$insertCouponMobileQuery="INSERT INTO $sql_tbl[coupon_mobile] (mobile_number, login, coupon, campaign_id) VALUES ('$mobileNumber','$login','$couponCode', '$campaignId')";
		db_query($insertCouponMobileQuery);
		$sqllog->debug("Query ".$insertCouponMobileQuery."  Filename: reister.php");
		
		func_send_sms_cellnext($mobileNumber, "Your free mug coupon code is : " . $couponCode . " -Myntra");
		
	}
	
	function checkExistingMobileNumbers($mobileNumber, $login){
		if(strlen($mobileNumber) > 10){
			//to take care of numbers appended with 0 or +91
			$mobileNumber = substr($mobileNumber, strlen($mobileNumber) - 10, strlen($mobileNumber));
		}
		global $sql_tbl;
		$existingMobileNumberCheckQuery = "select login from $sql_tbl[customers] where mobile like '%$mobileNumber%'";
		$existingMobileNumberCheckQueryResult = db_query($existingMobileNumberCheckQuery);
		$row = db_fetch_array($existingMobileNumberCheckQueryResult);
		if($row == null || $row['login'] == null){
			return "valid";
		}
		else {
			return "invalid";
		}
		/* have added this so that we can map the same customer with same mobile, but currently we are doing a blind check
		if($row != null){
			$existingLogin = $row['login'];
			if($existingLogin == $login){
				return "valid";
			}
			else{
				return "invalid";
			}
			
		}*/
	}

	$smarty->assign("firstName", $firstName);
	$smarty->assign("mobileNumberInTextBox", $mobileNumberInTextBox);
	$smarty->assign("couponCode", $couponCode);
	$smarty->assign("login", $login);
	$smarty->assign("status",$status);
	//func_display("apoorva_sample.tpl", $smarty);
	func_display("senCoupon.tpl", $smarty);
	
//	echo "MobileNumber" .$_POST['mobileFromForm'];
	
	
	//styleid of free style ie currently mug is free so style id of free mug 
/*	$styleid=14;
	$discount=100;
	$maximum=249;
	$couponcode = func_free_coupon_on_registration($uname,$styleid, $discount,$maximum);
	
	echo $couponcode;
	
	$coupon_details=$couponcode;
	$mailto = $uname;
	$from = "MyntraAdmin";
	$args = array( "FIRST_NAME" => $profile_values['firstname'],
                                      "USER_NAME" => $uname,
                                      "COUPON_DETAILS" => $coupon_details);

	 


	//added by apoorva for free mug
	$defaultMobileNumber = 9845823705;
	//                   db_exec("INSERT INTO $sql_tbl[coupon_mobile] (mobile_number, login, coupon) VALUES (?,?,?)",
	//                array ((integer)$defaultMobileNumber,$uname,$couponcode));
	global $sqllog;
	$query="INSERT INTO $sql_tbl[coupon_mobile] (mobile_number, login, coupon) VALUES ($defaultMobileNumber,'$uname','$couponcode')";
	//db_exec($query);
	db_query($query);
	$sqllog->debug("Query ".$query."  Filename: reister.php");
	
	*/
?>