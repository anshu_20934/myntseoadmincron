<?
use abtest\MABTest;
require_once 'auth.php';
include_once($xcart_dir."/Profiler/Profiler.php");
include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
include_once "$xcart_dir/modules/coupon/CouponDiscountCalculator.php";
include_once "$xcart_dir/include/class/mcart/class.MCart.php";
include_once "$xcart_dir/include/class/mcart/class.MCartItem.php";
include_once "$xcart_dir/modules/discount/DiscountEngine.php";
include_once "$xcart_dir/modules/coupon/exception/CouponAmountException.php";
include_once "$xcart_dir/modules/coupon/exception/CouponCountException.php";
include_once "$xcart_dir/modules/coupon/CouponValidator.php";
use style\builder\CachedStyleBuilder;



$styleid = filter_input(INPUT_GET, 'styleid', FILTER_SANITIZE_NUMBER_INT);
$login = $XCART_SESSION_VARS['login'];
$channel = $XCART_SESSION_VARS['channel'];

$cachedStyleBuilderObj=CachedStyleBuilder::getInstance();
$styleObj=$cachedStyleBuilderObj->getStyleObject($styleid);
$styleDetails = $styleObj->getProductStyleDetails();
$price = $styleDetails['price'];
if (null == $styleObj) {
	echo json_encode(array('status'=>'nocoupon','message' => "invalid style"));
	exit;
} else {
    $discountData= $styleObj->getDiscountData();
}
$discountedPrice = ceil($price - $discountData->discountAmount);


$couponAdapter = CouponAdapter::getInstance();
$validator = CouponValidator::getInstance();
$calculator = CouponDiscountCalculator::getInstance();
$specialCouponsForPDP = (string)WidgetKeyValuePairs::getWidgetValueForKey('specialCouponsForPDPWidgetDisplay');
$pdpCouponWidgetMessage =(string)WidgetKeyValuePairs::getWidgetValueForKey('pdpCouponWidgetMessage');
$pdpCouponWidgetNoOfferMessage =(string)WidgetKeyValuePairs::getWidgetValueForKey('pdpCouponWidgetNoOfferMessage');
$loginOnlyCouponGroup=(string)WidgetKeyValuePairs::getWidgetValueForKey('loginOnlyCouponGroup');
$loginForBestCouponUrlDesk=(string)WidgetKeyValuePairs::getWidgetValueForKey('loginForBestCouponUrlDesk');
$loginForBestCouponUrl=(string)WidgetKeyValuePairs::getWidgetValueForKey('loginForBestCouponUrl');

$data =array();
$dataset = array();
$active_coupons = array();
// My Coupons.

if (!empty($login)) {
    $active_coupons = $couponAdapter->getAllActiveCouponsForUserInMyMyntra($login);
}


if(!empty($specialCouponsForPDP)){
	$specialCouponsForPDP = explode(',', $specialCouponsForPDP);
	$active_coupons = array_unique(array_merge($active_coupons,$specialCouponsForPDP), SORT_REGULAR);
}



if(!empty($active_coupons)){
	foreach($active_coupons as $active_coupon){
        $couponDiscountedPrice = null;
		try {
            $coupon = $couponAdapter->fetchCoupon($active_coupon);
            if (!$validator->isCouponValid($coupon)) continue;
            /**
             * Since coupon modules are aligned to work on cart objects, making do 
             * with a dummy one.
             */
            $tempCart = new MCart();
            $tempCart->setLogin($login); 
            $tempCart->addProduct(new MCartNPItem($styleid, MCartNPItem::getDefaultSkuID($styleid), 1), false, false);

            /*escape the exceptions for min amount and count*/
                
	            try {
				    $calculator->applyCouponOnCart($coupon, $tempCart);
	            } catch (CouponAmountException $cae) {	            	
	            } catch (CouponCountException $cce) {
	            }
	           
            
			$type = $coupon->getType();
			$minimum = $coupon->getMinimum();
			$minimumCount = $coupon->getMinimumCount();
			$enddate = $coupon->getEndDate();
			$datearray = getdate($enddate);
            		$groupname = $coupon->getGroupName();

			$couponExtraTNC = "";
			if($type == 'percentage'){
				$coupondiscount  = (int)$coupon->getPercentDiscount().'%';
			}else if ($type == 'max_percentage_dual')
			{
				$coupondiscount  = '*'.(int)$coupon->getPercentDiscount().'%';
				$couponExtraTNC = "* *upto maximum ".$coupon->getAbsoluteDiscount()."% total discount on this product ";
			}else if ($coupon->getAbsoluteDiscount() > 0) {
				$coupondiscount =$coupon->getAbsoluteDiscount();
                if ($minimum > 0 && $discountedPrice < $minimum) {
                    $coupondiscount = ceil($coupon->getAbsoluteDiscount()*($discountedPrice/$minimum));
                    $couponDiscountedPrice = $discountedPrice - $coupondiscount;
                }
			}
			
			//$tooltip = '<p>GET '.$coupondiscount.' off on '.($minimum>0?'a minimum purchase of Rs'.$minimum:'this product').'.</p><br/>';
			//$tooltip = $tooltip.'<p>Valid till '..'</p>';
            if (empty($couponDiscountedPrice)) {
            	$couponDiscountedPrice = $calculator->getCouponDiscountedPrice($coupon,$discountedPrice,$price);
            }
            
			//continue if discounted price is greater or equal 
           	if(ceil($couponDiscountedPrice) >= $discountedPrice){
            	continue;
           	}
            
            
			$dataset[]=array(
								'coupon' => $active_coupon,
								'price' => ceil($couponDiscountedPrice),
								'tooltip'=> $tooltip,
								'type' => $type,
								'minimum'=> round($minimum),
								'minimumCount'=> $minimumCount,
								'coupondiscount' => $coupondiscount,
								'enddate' => 'midnight '.$datearray['mday'].getDateOrdinal($datearray['mday']).' '.$datearray['month'].', '.$datearray['year'],
                                				'originalPrice' => $price,
								'couponExtraTNC'=>$couponExtraTNC,
                        				        'groupName' =>$groupname
							);

		} catch(Exception $e) {
			// ignore :: the coupon is not applicable on this product

		}
	}
}

usort($dataset, pricesort);

function pricesort($a,$b)
{
   return ($a['price'] == $b['price'])? 0 :(($a['price'] < $b['price']) ? -1 : 1);
}

function getDateOrdinal($number)
{
	$suffix = array('th','st','nd','rd','th','th','th','th','th','th');
	if (($number %100) >= 11 && ($number%100) <= 13)
   		$abbreviation = 'th';
	else
	$abbreviation = $suffix[$number % 10];
	return $abbreviation;
}

if(!empty($active_coupons) || !empty($specialCouponsForPDP)){
	//PASS only one coupon for now
	$data['coupons'] = $dataset;
	//$data['coupons'] = $dataset; //commented - enable this later when you need to display all coupons
}

$desktop_channel = web\utils\ChannelType::getChannel('desktop'); 
$mobile_channel = web\utils\ChannelType::getChannel('mobile'); 

if(empty($dataset)){
	echo json_encode(array('status'=>'nocoupon','message' => $pdpCouponWidgetNoOfferMessage));
	exit;
}
else if (MABTest::getInstance()->getUserSegment("loginOnlyCoupon")==="test") {
     $best_coupon = $dataset[0];

     $status = 'success';
     $message = $pdpCouponWidgetMessage;

     if (empty($login) && $best_coupon['groupName']==$loginOnlyCouponGroup) {
        if ($channel==$desktop_channel) {
                $status = 'nocoupon';
                $message = $loginForBestCouponUrlDesk;
        }
        else if ($channel==$mobile_channel) {
                $status = 'nocoupon';
                $message = $loginForBestCouponUrl;
        }
     }
}
else {
        $status = 'success';
        $message = $pdpCouponWidgetMessage;
}
echo json_encode(array('status'=>$status,'data'=>$data,'message' => $message,'numberOfCoupons' => count($dataset)));
exit;
