<?php

chdir(dirname(__FILE__));
include_once("./auth.php");
include_once("$xcart_dir/include/class/class.orders.php");
include_once("$xcart_dir/include/func/func.mail.php");
include_once("$xcart_dir/include/func/func.sms_alerts.php");
include_once("$xcart_dir/include/func/func.mk_orderbook.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/include/func/func.mkcore.php");
include_once("./include/class/mcart/class.MCart.php");

if(empty($_GET) || empty($orderid)) {
	func_header_location($https_location . "/mkpaymentoptions.php",false);
}
$redirectToInCaseOfReload = $http_location;
$orderid=mysql_real_escape_string($orderid);
$order_result = func_query("SELECT * FROM " . $sql_tbl['orders'] . " WHERE orderid='". $orderid ."'");
$paymentoption = $order_result[0]['payment_method'];
$prev_status = $order_result[0]['status'];
$couponCode = $order_result[0]['coupon'];
$cashCouponCode = $order_result[0]['cash_coupon_code'];
$cashdiscount = $order_result[0]['cash_redeemed'];
$codCharge = $order_result[0]['cod'];
$orderLogin = $order_result[0]['login'];
$isPaymentDataCorrect = false;
$amountToBePaid = $order_result[0]['subtotal'] - $order_result[0]['coupon_discount'] - $order_result[0]['cash_redeemed'] + $order_result[0]['cod'] + $order_result[0]['shipping_cost'] + $order_result[0]['payment_surcharge'] - $order_result[0]['discount'] - $order_result[0]['cart_discount'] - $order_result[0]['pg_discount'];
$amountToBePaid = $amountToBePaid < 0 ? 0 : $amountToBePaid;

if($prev_status!='PP' && $paymentoption !='ivr'){
	func_header_location($https_location . "/mkpaymentoptions.php",false);
}


if(empty($login)){
    $errormessage = "Ooops! There was an error in order processing and hence your order could not be placed. If you have been charged, the charged amount would be refunded back to your banking or credit card account within 2 working days. Please contact support@myntra.com for more details.";
    $XCART_SESSION_VARS['errormessage'] = $errormessage;
	$toLog = array();
	$toLog['time_return'] = time();		
	$toLog['is_tampered'] = 0;		
	$toLog['is_complete'] = 0;
	$toLog['amountToBePaid'] = $amountToBePaid;	
	$toLog['response_message'] = "Login was empty.";	
	$toLog['completed_via'] = 'Auto';
    func_array2update('mk_payments_log', $toLog,"orderid=$orderid");
    func_header_location($redirectToInCaseOfReload,false);
}

//all checks proper till now, check for user id and orderid mapping.
if(strcasecmp($login, $orderLogin) != 0) {
	$errormessage = "We are sorry, You are trying to access wrong order id.";
    $XCART_SESSION_VARS['errormessage'] = $errormessage;
	$toLog = array();
	$toLog['time_return'] = time();		
	$toLog['is_tampered'] = 0;		
	$toLog['is_complete'] = 0;
	$toLog['amountToBePaid'] = $amountToBePaid;	
	$toLog['response_message'] = "We are sorry, You are trying to access wrong order id.";	
	$toLog['completed_via'] = 'Auto';
    func_array2update('mk_payments_log', $toLog,"orderid=$orderid");
    func_header_location($redirectToInCaseOfReload,false);
}

//start displaying order details now
$mcartFactory= new MCartFactory();
$myCart= $mcartFactory->getCurrentUserCart();
if($myCart!=null) {
	$productsInCart = MCartUtils::convertCartToArray($myCart);
	$smarty->assign("orderid",$orderid);
	$smarty->assign("amount",$order_result[0]['subtotal']);
	$smarty->assign("specialOfferDiscount",$order_result[0]['discount']);
	$smarty->assign("shippingRate",$order_result[0]['shipping_cost']);
	$smarty->assign("cashRedeemed",$order_result[0]['cash_redeemed']);
	$smarty->assign("coupon_discount",$order_result[0]['coupon_discount']);
	$smarty->assign("totalAmount",$amountToBePaid);
	$smarty->assign("giftamount",$order_result[0]['gift_charges']);
	$smarty->assign("codCharge",$order_result[0]['cod']);
	$smarty->assign("pg_discount",$order_result[0]['pg_discount']);
	$smarty->assign("emi_charge",$order_result[0]['payment_surcharge']);
	$smarty->assign("productsInCart", $productsInCart);
	
	$payByPhoneNumber = WidgetKeyValuePairs::getWidgetValueForKey('payByPhoneNumber');
	if($payByPhoneNumber == NULL) {
		$payByPhoneNumber = $customerSupportCall;
	}
	
	$smarty->assign("pay_by_phone_number", $payByPhoneNumber);
	
	$customerFName = $order_result[0]['firstname'];
	$customerMobileNo = $order_result[0]['issues_contact_number'];
	$msg =  "Dear ".$customerFName.",Your order: ".$orderid." is pending payment. Please call $payByPhoneNumber within 24 hrs to complete the order.";
	
	$productsInCart = func_create_array_products_of_order($orderid,$productsInCart);
	func_order_confirm_mail("ivr",$productsInCart,$order_result[0],$customerFName,$login,$orderid,null,$order_result[0],$payByPhoneNumber);
	func_send_sms($customerMobileNo,$msg);
	
	##################################################################################
	#Remove purchased products from cookies and database cart persistence.			 #
	##################################################################################
	MCartUtils::deleteCart($myCart);
	x_session_unregister('productsInCart'); 
	x_session_unregister('productids'); 
	x_session_unregister('cart'); 
	x_session_unregister('coupondiscount');
	x_session_unregister('couponCode');
	x_session_unregister('cashdiscount');
	x_session_unregister('cashCouponCode');
	x_session_unregister('userCashAmount');
	x_session_unregister('productDiscount');
	x_session_unregister('masterCustomizationArrayD');
	x_session_unregister('offers'); 
	x_session_unregister('totaloffers'); 
	x_session_unregister('totaldiscountonoffer'); 
	x_session_unregister('OrderTotal');
	x_session_unregister('giftdata');
	x_session_unregister('totalQuantity');
	x_session_unregister('issuer_voucher_code');//unregistering brandstore voucher code
	x_session_unregister('order_id'); //unregistering the order id
	$uuid = stripslashes(getMyntraCookie('MYNTRA_SHOPPING_ID'));
	setMyntraCookie("MYNTRA_UNQ_COOKIE_ID",substr($uuid,1,20),time() - 3600,'/',$cookiedomain);
	if (x_session_is_registered("chosenProductConfiguration")) x_session_unregister("chosenProductConfiguration");
	if (x_session_is_registered("masterCustomizationArray")) x_session_unregister("masterCustomizationArray");
	if (x_session_is_registered("masterCustomizationArrayD")) x_session_unregister("masterCustomizationArrayD");
	 
	x_session_register("OrderProcessCompleted",1);
	
	func_display("cart/payByPhoneConfirmation.tpl", $smarty);
} else {
	func_header_location($http_locations,false); //redirect to homepage
}
?>