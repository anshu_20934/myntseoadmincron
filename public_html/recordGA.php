<?php

$xcart_dir = realpath(dirname(__FILE__));
include_once($xcart_dir.'/logger.php');

$confirmationlog = $logger_manager->getLogger('MYNTRACONFIRMATIONLOGGER');

$orderid = filter_input(INPUT_GET,"orderid", FILTER_SANITIZE_NUMBER_INT);

if($orderid !== false) {
	$confirmationlog->debug("Orderid:" . $orderid);
}

?>