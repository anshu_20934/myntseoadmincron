<?php
require_once "auth.php";
require_once 'myntra/navhighlighter.php';
require_once($xcart_dir."/modules/lg/LookGoodAPI.php");

$pageName = 'landing';
include_once $xcart_dir."/webengage.php";
include_once $xcart_dir."/gtm.php";

$id = filter_input(INPUT_GET,"id", FILTER_SANITIZE_NUMBER_INT);
$lookId = filter_input(INPUT_GET,"look", FILTER_SANITIZE_NUMBER_INT);
$buy = filter_input(INPUT_GET,"buy", FILTER_SANITIZE_NUMBER_INT);

if(empty($lookId)){
	$lookId=0;
}
if(empty($buy)){
	$buy=0;
}

$notfound=0;

if(empty($id)){
	
	header("HTTP/1.0 404 Not Found");
	$notfound=1;
	$smarty->assign('lookNotFound',$notfound);
}

$lookBook=LookGoodAPI::getLookBook($id);
if(empty($lookBook)){
	header("HTTP/1.0 404 Not Found");
	$notfound=1;
	$smarty->assign('lookNotFound',$notfound);
}

$looks=$lookBook->looks;

//print_r($looks);exit;
$lookImageArray=array();
$buyImageArray=array();
$lookDetails=array();
$thumbnails=array();

$currentPageURL=currentPageURL();
$prettyURL=strrpos($currentPageURL,'?');

if(!empty($prettyURL)){
    $pageCleanURL=substr($currentPageURL,0,strrpos($currentPageURL,'?'));
    $smarty->assign('canonicalPageUrl', $pageCleanURL);
}
else {
	$pageCleanURL=$currentPageURL;
}

$_GET['include_oos'] = 1;
foreach($looks as $look){
	$lookTotal=0;$lookDiscountedTotal=0;
	$lookImageArray[]=$look->image_look;
	$buyImageArray[]=$look->image_buy_look;
	$thumbnails[]=str_replace('_980x540.jpg','_98x54.jpg',$look->image_look);
	$styles_to_show=array();
	foreach($look->lookStyles as $lookStyles){
		$styles_to_show[]=$lookStyles->style_id;
	}
	$styleWidget=new WidgetSKUList($look->name, $styles_to_show);
	$widgetData = $styleWidget->getData();
	foreach($widgetData['data'] as $key=>$style){
		$lookTotal=$lookTotal+$style['price'];
		$lookDiscountedTotal=$lookDiscountedTotal+$style['discounted_price'];
		//strip the brand name from it
		$pos = stripos($style["product"], $style["brands_filter_facet"]);
        $style["product"] = trim(substr($style["product"], $pos+strlen($style["brands_filter_facet"]), strlen($style["product"])));
        //echo $style["product"];
        
	}
	$lookDetails[]=array(
		'id'                 =>$look->id,
		'name'               =>$look->name,
		'description'        =>$look->description,
		'isPurchasable'      =>$look->is_purchasable,
		'styleData'          =>$widgetData['data'],
		'lookTotal'          =>$lookTotal,
		'lookDiscountedTotal'=>$lookDiscountedTotal,
		'isRightAligned'     =>$look->is_right_aligned,
		'isTextWhite'        =>$look->is_text_white,
		'similarStylesURL'   =>$look->similar_styles_url,
		'link_text'			 =>$look->link_text,
		'link_url'			 =>$look->link_url
	);
	if($look->id == $lookId){
			//setOgTypeValues from here based on the lookId
			$lookOGTags=array(
				'lookImage' => $look->image_buy_look,
				'lookName' => $look->name,
				'lookURL' => $pageCleanURL."?look=".$look->id,
				'lookDescription' => $look->description
			);
	}
}

if(empty($lookId)){
	//setOgTypeValues from here default
	$lookOGTags=array(
		'lookImage' => $buyImageArray[0],
		'lookName' => $lookDetails[0]['name'],
		'lookURL' => $pageCleanURL."?look=".$lookDetails[0]['id'],
		'lookDescription' => $lookDetails[0]['description']
	);
}

//Create BreadCrumb
$nav_arr=array('STAR \'N\' STYLE'=>'/starnstyle',$lookBook->name => $pageCleanURL);
$smarty->assign("nav_selection_arr_url", $nav_arr);


$smarty->assign('lookOGTags',$lookOGTags);
$smarty->assign('lookId',$lookId);
$smarty->assign('buy',$buy);
$smarty->assign('lookImages',$lookImageArray);
$smarty->assign('buyPopupImages',json_encode($buyImageArray));
$smarty->assign('buyImageArray',$buyImageArray);
$smarty->assign('lookThumbnails',json_encode($thumbnails));
$smarty->assign('lookDetails',$lookDetails);
$smarty->assign('lookBookName',$lookBook->name);
$smarty->assign('totalLooks',count($looks));
///exit;

func_display("lg/look-book.tpl", $smarty);
