<?php

require_once "./auth.php";
include_once "include/func/func.returns.php";

$start_time = time();
$max_return_id = func_query_first_cell("select max(returnid) from xcart_returns",true);

$return_statuses = get_all_return_statuses();
$return_statuses_map = array();
foreach($return_statuses as $return_status){
	$return_statuses_map["Remarks (".$return_status['display_name'].")"] = $return_status['code']; 
}

for($i=10000;$i<$max_return_id;$i=$i+1){
	
	$return_comments = func_query("select l.*,r.createdby,r.return_mode,r.createddate from mk_returns_comments_log l,xcart_returns r where r.returnid =  l.returnid and r.returnid = $i order by commentid");
	$return_comment_map = array();
	$return_comment_title_map = array();
	foreach ($return_comments as $return_comment){
		$return_comment_map[$return_comment['commentid']] = $return_statuses_map[$return_comment['commenttitle']];
	}
	$prev_comment_id = false;
	$from_status = null;
	foreach ($return_comments as $return_comment){
		if($prev_comment_id){
			$from_status = $return_comment_map[$prev_comment_id];
		}
		$prev_comment_id = $return_comment['commentid'];
		$to_status = $return_statuses_map[$return_comment['commenttitle']];
		if($from_status == null && $to_status != 'RRQP' && $to_status!= 'RRQS'){
			if($return_comment['return_mode'] == 'self'){
				$sql = "insert into mk_return_transit_details values (NULL,$i,NULL,'RRQS',$return_comment[createddate],'$return_comment[createdby]')";
				$from_status = 'RRQS';	
			}else{
				$sql = "insert into mk_return_transit_details values (NULL,$i,NULL,'RRQP',$return_comment[createddate],'$return_comment[createdby]')";
				$from_status = 'RRQP';
			}
			func_query($sql);
		}
		$sql = "insert into mk_return_transit_details values (NULL,$i,'$from_status','$to_status',$return_comment[adddate],'$return_comment[addedby]')";
		func_query($sql);
	}
}

$end_time = time();

echo $end_time-$start_time;


?>
