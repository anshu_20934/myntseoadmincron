<?php
/*
 * Created on Aug 28, 2012
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

 if($_SERVER['REMOTE_ADDR'] != '182.71.248.194' && $_SERVER['REMOTE_ADDR'] != '61.12.18.54')
 	exit;
 
 include_once("auth.php");
 include_once($xcart_dir."/include/class/widget/class.widget.keyvaluepair.php");
 
 $startTime = '2013-02-18 00:00:00';//WidgetKeyValuePairs::getWidgetValueForKey('employeehappyhour.starttime');
 $endTime =  '2013-02-19 12:00:00';//WidgetKeyValuePairs::getWidgetValueForKey('employeehappyhour.endtime');
  
 $action = $_GET['action'];
 
 if($action == 'total_revenue') {
 	$total = func_query_first_cell("Select sum(total+cod+cash_redeemed) as finalAmount from xcart_orders where date >= unix_timestamp('$startTime') and date < unix_timestamp('$endTime')  and status in ('OH','Q','WP','SH','DL','C')", true);
 	$total = number_format($total, 2, ".", ",");
 	header('Content-type: text/x-json');
	print json_encode($total);
	exit;
 } else if ($action == 'items_count') {
 	$total = func_query_first_cell("Select sum(od.amount) as totalItems from xcart_orders o, xcart_order_details od where od.orderid = o.orderid and o.date >= unix_timestamp('$startTime') and o.date < unix_timestamp('$endTime') and o.login like '%@myntra.com' and o.status in ('OH','Q','WP','SH','DL','C')", true);
 	header('Content-type: text/x-json');
	print json_encode($total);
	exit;
 } else if ($action == 'orders_count') {
 	$total = func_query_first_cell("Select count(*) as orders from xcart_orders where date >= unix_timestamp('$startTime') and date < unix_timestamp('$endTime')  and status in ('OH','Q','WP','SH','DL','C')", true);
 	header('Content-type: text/x-json');
	print json_encode($total);
	exit;
 } else if ($action == 'users_count') {
 	$total = func_query_first_cell("Select count(distinct login) as users from xcart_orders where date >= unix_timestamp('$startTime') and date < unix_timestamp('$endTime')  and status in ('OH','Q','WP','SH','DL','C')", true);
 	header('Content-type: text/x-json');
	print json_encode($total);
	exit;
 } else if ($action == 'top_5_buyers') {
 	$results = func_query("select o.login, concat(c.firstname,' ',c.lastname) as username, sum(total+cod+cash_redeemed) as totalAmount, count(distinct orderid) as orders from xcart_orders o use index(order_date), xcart_customers c where o.login = c.login and date >= unix_timestamp('$startTime') and date < unix_timestamp('$endTime') and o.status in ('OH','Q','WP','SH','DL','C') group by o.login order by totalAmount desc limit 5", true);
 	foreach($results as $index=>$result) {
 		$results[$index]['totalAmount'] = number_format($result['totalAmount'], 2, ".", ",");
 	}
 	$response['topBuyers'] = $results;
 	
 	header('Content-type: text/x-json');
	print json_encode($response);
	exit;
 } else if ($action == 'most_selling') {
 	$products = func_query("select sp.product_display_name as style_name, sp.default_image from mk_style_properties sp, (Select sku_id, sum(od.amount) as totalItems from xcart_orders o, xcart_order_details od, mk_order_item_option_quantity oq, mk_styles_options_skus_mapping som where od.orderid = o.orderid and od.itemid = oq.itemid and oq.optionid = som.option_id and o.date >= unix_timestamp('$startTime') and o.date < unix_timestamp('$endTime') and  o.status in ('OH','Q','WP','SH','DL','C') group by sku_id order by totalItems desc limit 3) b, mk_styles_options_skus_mapping som where som.sku_id = b.sku_id and som.style_id = sp.style_id", true);
 	foreach($products as $index=>$product) {
 		$products[$index]['imagepath'] = str_replace("_images", "_images_96_128", $product['default_image']);
 	}
 	header('Content-type: text/x-json');
	print json_encode($products);
	exit;
 } else if ($action == 'latest_purchases') {
 	$lastItemId = $_GET['lastitemid'];
 	if(empty($lastItemId)){
 		$lastItemId = func_query_first_cell("Select min(itemid) from xcart_orders o, xcart_order_details od where o.orderid = od.orderid and o.date >= greatest(unix_timestamp(DATE_SUB(now(), INTERVAL 5 MINUTE)), unix_timestamp('$startTime')) and o.date <= unix_timestamp('$endTime') and o.status in ('OH','Q','WP','SH','DL','C')", true);
 	}
 	if($lastItemId && !empty($lastItemId)) {
 		$products = func_query("select som.style_id, od.itemid, concat(c.firstname,' ',c.lastname) as user, sp.product_display_name as stylename, sp.default_image from xcart_orders o, xcart_order_details od, mk_order_item_option_quantity oq, mk_styles_options_skus_mapping som, mk_style_properties sp, xcart_customers c where od.orderid = o.orderid and od.itemid = oq.itemid and oq.optionid = som.option_id and od.product_style = sp.style_id and o.login = c.login and o.date >= unix_timestamp('$startTime') and o.date <= unix_timestamp('$endTime') and o.status in ('OH','Q','WP','SH','DL','C') and od.itemid > $lastItemId limit 5", true);
 	}
 	
 	foreach($products as $index=>$product) {
 		$products[$index]['imagepath'] = str_replace("_images", "_images_96_128", $product['default_image']);
 	}
 	
	header('Content-type: text/x-json');
	print json_encode($products);
	exit;	 	
 } else if ($action == 'trending') {
 	$style_articletype_rows = func_query("Select sp.style_id, mcc.typename from xcart_orders o, xcart_order_details od, mk_style_properties sp, mk_catalogue_classification mcc where o.orderid = od.orderid and od.product_style = sp.style_id and sp.global_attr_article_type = mcc.id and o.date >= unix_timestamp('$startTime') and o.date < unix_timestamp('$endTime') and o.status in ('OH','Q','WP','SH','DL','C')", true);
 	if($style_articletype_rows) {
 		foreach($style_articletype_rows as $row){
 			$style2ArticleTypeMapping[$row['style_id']] = $row['typename'];
 		}
 	}

	$articleType2BestSellingStyleMapping = array();
 	$counter = 0;
 	$products = func_query("select sp.default_image, sp.style_id, sp.product_display_name as style_name, b.totalItems from mk_style_properties sp, (Select product_style, sum(od.amount) as totalItems from xcart_orders o, xcart_order_details od where od.orderid = o.orderid and o.date >= unix_timestamp('$startTime') and o.date < unix_timestamp('$endTime') and o.status in ('OH','Q','WP','SH','DL','C') group by od.product_style order by totalItems desc) b where b.product_style = sp.style_id", true);
 	if($products) {
 		foreach ( $products as $index=>$product) {
       		$product['imagepath'] = str_replace("_images", "_images_96_128", $product['default_image']);
       		if(!isset($articleType2BestSellingStyleMapping[$style2ArticleTypeMapping[$product['style_id']]])) {
       			$articleType2BestSellingStyleMapping[$style2ArticleTypeMapping[$product['style_id']]] = $product;
       			$counter += 1;
       		}
       		if($counter === 10) {
       			break;
       		}
		}
 	}
 	
 	header('Content-type: text/x-json');
	print json_encode($articleType2BestSellingStyleMapping);
	exit;
 }  else if ($action == 'fastestBuyer') {
 	$fastest5KUser = trim(WidgetKeyValuePairs::getWidgetValueForKey('shopping.hour.winner.5K.myntra'));
 	if(!$fastest5KUser || empty($fastest5KUser)) {
	 	$userPurchases = func_query("select login, sum(total+cod+cash_redeemed) as purchaseAmount, min(orderid) as minOrderId, count(*) as noOfOrders from xcart_orders where date >= unix_timestamp('$startTime') and date < unix_timestamp('$endTime') and status in ('OH','Q','WP','SH','DL','C') group by login having purchaseAmount >= 5000 limit 20",true);
	 	$loginWithMultipleOrders = array();
	 	$fastest5KUser = '';
	 	$minOrderId = 999999999;
	 	if($userPurchases) {
	 		if(count($userPurchases) == 1) {
	 			$fastest5KUser = $userPurchases[0]['login'];
	 		} else {
			 	foreach($userPurchases as $purchaseInfo) {
			 		if($purchaseInfo['noOfOrders'] == 1) {
			 			if(intval($purchaseInfo['orderid']) < $minOrderId) {
			 				$fastest5KUser = $purchaseInfo['login'];
			 				$minOrderId = intval($purchaseInfo['orderid']);
			 			}
			 		} else {
			 			$loginWithMultipleOrders[] = $purchaseInfo['login'];
			 		}
			 	}
	 		}
	 		
	 		if(!empty($loginWithMultipleOrders)) {
	 			$userOrders = func_query("Select login, orderid, (total+cod+cash_redeemed) as total from xcart_orders where login in ('".implode($loginWithMultipleOrders, "','")."') and date >= unix_timestamp('$startTime') and date < unix_timestamp('$endTime') and status in ('OH','Q','WP','SH','DL','C')", true);
				$prevUser = '';
				$foundUserOrder = false;
				foreach($userOrders as $order) {
					if($foundUserOrder && $prevUser == $order['login'])
						continue;
						
					if($prevUser != $order['login']) {
						$total = 0;
						$prevUser = $order['login'];
					}
					$total += $order['total'];
					if($total >= 5000) {
						if(intval($order['orderid']) < $minOrderId) {
			 				$fastest5KUser = $order['login'];
			 				$minOrderId = intval($order['orderid']);
			 			}
						$foundUserOrder = true;
					}
				} 			
	 		}
	 	}
	 	if($fastest5KUser != '') {
	 		WidgetKeyValuePairs::setWidgetValueForKey(null, 'shopping.hour.winner.5K.myntra', $fastest5KUser, '');
	 	}
 	}
 	header('Content-type: text/x-json');
	print json_encode($fastest5KUser);
 	exit;
 } else if ($action == '5kin15mins') {
 	$results = func_query("select unix_timestamp('$startTime' + INTERVAL 15 MINUTE) as elapsingtime, o.login, concat(c.firstname,' ',c.lastname) as username, sum(total+cod+cash_redeemed) as totalAmount, count(distinct orderid) as orders from xcart_orders o use index(order_date), xcart_customers c where o.login = c.login and date >= unix_timestamp('$startTime') and date <= unix_timestamp('$startTime' + INTERVAL 15 MINUTE) and o.status in ('OH','Q','WP','SH','DL','C') group by o.login having totalAmount > 5000 order by totalAmount desc", true);
 	foreach($results as $index=>$result) {
 		$results[$index]['totalAmount'] = number_format($result['totalAmount'], 2, ".", ",");
 	}
 	
 	$response = array('buyers'=>$results);
 	
 	if(time() > $results[0]['elapsingtime'] && count($results) > 3) {
 		$winner1 = rand(0, count($results));
 		$winner2 = $winner1;
 		while($winner2 == $winner1)
 			$winner2 = rand(0, count($results));
 		
 		$winner3 = rand(0, count($results));
 		while($winner3 == $winner1 || $winner3 == $winner2) {
 			$winner3 = rand(0, count($results));
 		}
 		$response['winner1'] = $results[$winner1]['username'];
 		$response['winner2'] = $results[$winner2]['username'];
 		$response['winner3'] = $results[$winner3]['username'];
 		$response['timeover'] = true;
 	} else {
 		$response['timeover'] = false;
 	}
 	
 	header('Content-type: text/x-json');
	print json_encode($response);
	exit;
 } else if ($action == '15kin30mins') {
 	$results = func_query("select o.login, concat(c.firstname,' ',c.lastname) as username, sum(total+cod+cash_redeemed) as totalAmount, count(distinct orderid) as orders from xcart_orders o use index(order_date), xcart_customers c where o.login = c.login and date >= unix_timestamp('$startTime') and date <= unix_timestamp('$startTime' + INTERVAL 30 MINUTE) and o.status in ('OH','Q','WP','SH','DL','C') group by o.login having totalAmount > 15000 order by totalAmount desc", true);
 	foreach($results as $index=>$result) {
 		$results[$index]['totalAmount'] = number_format($result['totalAmount'], 2, ".", ",");
 	}
 	
 	header('Content-type: text/x-json');
	print json_encode($results);
	exit;
 } else if ($action == 'first20minTopBuyers') {
 	$results = func_query("select o.login, concat(c.firstname,' ',c.lastname) as username, sum(total+cod+cash_redeemed) as totalAmount, count(distinct orderid) as orders from xcart_orders o use index(order_date), xcart_customers c where o.login = c.login and date >= unix_timestamp('$startTime') and date < unix_timestamp('$startTime' + INTERVAL 20 MINUTE) and o.status in ('OH','Q','WP','SH','DL','C') group by o.login order by totalAmount desc limit 5", true);
 	foreach($results as $index=>$result) {
 		$results[$index]['totalAmount'] = number_format($result['totalAmount'], 2, ".", ",");
 	}
 	$response['topBuyers'] = $results;
 	
 	header('Content-type: text/x-json');
 	print json_encode($response);
 	exit;
 } else if ($action == 'next20minTopBuyers') {
 	$results = func_query("select o.login, concat(c.firstname,' ',c.lastname) as username, sum(total+cod+cash_redeemed) as totalAmount, count(distinct orderid) as orders from xcart_orders o use index(order_date), xcart_customers c where o.login = c.login and date >= unix_timestamp('$startTime' + INTERVAL 20 MINUTE) and date < unix_timestamp('$startTime' + INTERVAL 40 MINUTE) and o.status in ('OH','Q','WP','SH','DL','C') group by o.login order by totalAmount desc limit 5", true);
 	foreach($results as $index=>$result) {
 		$results[$index]['totalAmount'] = number_format($result['totalAmount'], 2, ".", ",");
 	}
 	$response['topBuyers'] = $results;
 	
 	header('Content-type: text/x-json');
 	print json_encode($response);
 	exit;
 } else if ($action == 'last20minTopBuyers') {
 	$results = func_query("select o.login, concat(c.firstname,' ',c.lastname) as username, sum(total+cod+cash_redeemed) as totalAmount, count(distinct orderid) as orders from xcart_orders o use index(order_date), xcart_customers c where o.login = c.login and date >= unix_timestamp('$endTime' - INTERVAL 20 MINUTE) and date < unix_timestamp('$endTime') and o.status in ('OH','Q','WP','SH','DL','C') group by o.login order by totalAmount desc limit 5", true);
 	foreach($results as $index=>$result) {
 		$results[$index]['totalAmount'] = number_format($result['totalAmount'], 2, ".", ",");
 	}
 	$response['topBuyers'] = $results;
 	
 	header('Content-type: text/x-json');
 	print json_encode($response);
 	exit;
 } else if ($action == 'fetchDressBerryTopBuyers') {
 	$results = func_query("select b.login, concat(c.firstname,' ',c.lastname) as username, sum(b.total+b.cod+b.cash_redeemed) as totalAmount, count(distinct b.orderid) as orders from (select o.orderid, o.login, o.total, o.cash_redeemed, o.cod from xcart_orders o use index(order_date), xcart_order_details od, mk_style_properties sp where o.orderid = od.orderid and od.product_style = sp.style_id and sp.global_attr_brand = 'DressBerry' and o.date >= unix_timestamp('$startTime') and o.date < unix_timestamp('$startTime' + INTERVAL 45 MINUTE) and o.status in ('OH','Q','WP','SH','DL','C') group by o.orderid) b LEFT JOIN xcart_customers c on b.login = c.login group by c.login order by totalAmount desc limit 5");
 	foreach($results as $index=>$result) {
 		$results[$index]['totalAmount'] = number_format($result['totalAmount'], 2, ".", ",");
 	}
 	$response['topBuyers'] = $results;
 	
 	header('Content-type: text/x-json');
 	print json_encode($response);
 	exit;
 } else if ($action == 'fetchRoadsterTopBuyers') {
 	$results = func_query("select b.login, concat(c.firstname,' ',c.lastname) as username, sum(b.total+b.cod+b.cash_redeemed) as totalAmount, count(distinct b.orderid) as orders from (select o.orderid, o.login, o.total, o.cash_redeemed, o.cod from xcart_orders o use index(order_date), xcart_order_details od, mk_style_properties sp where o.orderid = od.orderid and od.product_style = sp.style_id and sp.global_attr_brand = 'Roadster' and o.date >= unix_timestamp('$startTime') and o.date < unix_timestamp('$startTime' + INTERVAL 45 MINUTE) and o.status in ('OH','Q','WP','SH','DL','C') group by o.orderid) b LEFT JOIN xcart_customers c on b.login = c.login group by c.login order by totalAmount desc limit 5");
 	foreach($results as $index=>$result) {
 		$results[$index]['totalAmount'] = number_format($result['totalAmount'], 2, ".", ",");
 	}
 	$response['topBuyers'] = $results;
 	
 	header('Content-type: text/x-json');
 	print json_encode($response);
 	exit;
 } else if ($action == 'fetchSherSinghTopBuyers') {
 	$results = func_query("select b.login, concat(c.firstname,' ',c.lastname) as username, sum(b.total+b.cod+b.cash_redeemed) as totalAmount, count(distinct b.orderid) as orders from (select o.orderid, o.login, o.total, o.cash_redeemed, o.cod from xcart_orders o use index(order_date), xcart_order_details od, mk_style_properties sp where o.orderid = od.orderid and od.product_style = sp.style_id and sp.global_attr_brand = 'Sher Singh' and o.date >= unix_timestamp('$startTime') and o.date < unix_timestamp('$startTime' + INTERVAL 45 MINUTE) and o.status in ('OH','Q','WP','SH','DL','C') group by o.orderid) b LEFT JOIN xcart_customers c on b.login = c.login group by c.login  order by totalAmount desc limit 5");
 	foreach($results as $index=>$result) {
 		$results[$index]['totalAmount'] = number_format($result['totalAmount'], 2, ".", ",");
 	}
 	$response['topBuyers'] = $results;
 	
 	header('Content-type: text/x-json');
 	print json_encode($response);
 	exit;
 }else if ($action == 'fetchItemsTopBuyers') {
 	$results = func_query("select o.login, concat(c.firstname,' ',c.lastname) as username, sum(od.amount) as totalItems, count(distinct o.orderid) as orders from xcart_orders o use index(order_date), xcart_customers c, xcart_order_details od where o.login = c.login and o.orderid = od.orderid and date >= unix_timestamp('$startTime') and date < unix_timestamp('$startTime' + INTERVAL 45 MINUTE) and o.status in ('OH','Q','WP','SH','DL','C') group by o.login order by totalItems desc limit 5", true);
 	$response['topBuyers'] = $results;
 	
 	header('Content-type: text/x-json');
 	print json_encode($response);
 	exit;
 }

 $smarty->display("salestracker/overallsummary.tpl");
 
?>
