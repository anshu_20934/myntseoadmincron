<?php
use giftcard\GiftCardMessage;
use giftcard\GiftCardOccasion;
use giftcard\GiftCard;
use giftcard\manager\MCartGiftCard;
use giftcard\manager\MCartGiftCardItem;
use enums\revenue\payments\Gateways;
use revenue\payments\gateway\ICICIBankGateway;
use enums\cart\CartContext;
use revenue\payments\service\PaymentServiceInterface;

include_once("./auth.php");
include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
$tracker = new BaseTracker(BaseTracker::PAYMENT_PAGE);

include_once "$xcart_dir/include/func/func.mkcore.php";
include_once "$xcart_dir/include/func/func.randnum.php";
include_once "$xcart_dir/include/func/func.mk_orderbook.php";
include_once "$xcart_dir/include/class/class.mymyntra.php";
include_once "$xcart_dir/include/func/func.refer.php";
include_once "$xcart_dir/include/class/class.orders.php";
include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
include_once "$xcart_dir/modules/coupon/CouponValidator.php";
include_once "$xcart_dir/include/class/mcart/class.MCart.php";
include_once "$xcart_dir/include/class/mcart/class.MCartUtils.php";
include_once "$xcart_dir/modules/coupon/exception/CouponNotFoundException.php";
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";
include_once "$xcart_dir/modules/discount/DiscountEngine.php";
include_once "$xcart_dir/modules/coupon/CouponDiscountCalculator.php";
include_once "$xcart_dir/modules/coupon/CashCouponCalculator.php";
include_once "$xcart_dir/include/class/PaymentGatewayDiscount.php";
include_once "$xcart_dir/include/class/netbankingHelper.php";
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";
require_once "$xcart_dir/include/func/func.courier.php";
include_once "$xcart_dir/include/func/func.loginhelper.php";
require_once($xcart_dir."/modules/giftcards/GiftCardsHelper.php");

if(!$giftingEnabled){
	include_once $xcart_dir."/include/func/func.core.php";
	func_header_location($https_location,false);	
}

$pageName = 'checkout';
include_once $xcart_dir."/webengage.php";

$login = $XCART_SESSION_VARS['login'];
if(empty($login)){
	//Page to be accessed only if logged in else redirect to giftards page
	func_header_location($http_location."/giftCards.php",false);
}

//Check whether User account is suspended , if yes then invalidate the session and redirect to home page
$userStatus = func_query_first_cell("SELECT status FROM $sql_tbl[customers] WHERE login='$login'");
if($userStatus == 'N'){
    clearSessionVarsOnLogout(true);
    func_header_location($http_location,false);
}


if(empty($_SERVER['HTTPS'])){
	//Page to be accessed only over http redirect him to https
	 func_header_location($https_location .$_SERVER['REQUEST_URI'],false);
}

if($_SERVER['HTTP_HOST'] != $xcart_https_host){
	//redirect if someone opened with any other url
	func_header_location($https_location . $_SERVER['REQUEST_URI'],false);
}

if(isset($_GET['promptlogin'])){
    setMyntraCookie('__prompt_login',"1");
}

if(!empty($_GET['address'])){
	$address=mysql_real_escape_string($address);
	x_session_register("lastAddressUsedID", $address);
	$address = func_select_first('mk_customer_address', array('id' => $address));
} elseif(!empty($lastAddressUsed)){
	x_session_register("lastAddressUsedID", $lastAddressUsed);
	$address = func_select_first('mk_customer_address', array('id' => $lastAddressUsed));
} else {
    $address = func_select_first('mk_customer_address', array('login' => $login , 'default_address' => 1));
}
if (!empty($address) && ($address['country'] != 'IN' || !is_zipcode_servicable($address['pincode']))) {
    $address = array();
}
if(!empty($address)){
	$result = func_select_first('xcart_states', array('code' => $address['state']));
	$address['statename']= empty($result['state'])?$address['state']:$result['state'];;
	$result = func_select_first('xcart_languages', array('name' => 'country_'.$address['country'],'topic' => 'Countries'));
	$address['countryname']= $result['value'];
}

if(empty($address)) {
	$smarty->assign("addressPresent", false);
}

/*
// If we don't find a address or user is trying to pass some other ID, kick the user back to address page ..
if(empty($address) || strcasecmp($login,$address['login']) !=0) {
	func_header_location($https_location . "/mkcustomeraddress.php?gc=true",false);
}
*/

$couponAdapter = CouponAdapter::getInstance();

//function to replace last occurance of a search in a string
function stringLastReplace($search, $replace, $subject)
{
    $pos = strrpos($subject, $search);

    if($pos === false)
    {
        return $subject;
    }
    else
    {
        return substr_replace($subject, $replace, $pos, strlen($search));
    }
}


$mymyntra = new MyMyntra(mysql_real_escape_string($login));
$userProfileData = $mymyntra->getUserProfileData();
 
// Instance of the coupon database liaison.
$adapter = CouponAdapter::getInstance();
$mcartFactory= new \MCartFactory();
$myCart= $mcartFactory->getCurrentUserCart(true, CartContext::GiftCardContext);
//$adapter->unlockCouponForUser('CASHBACKuz9lxek7', $login);

if($myCart!=null)
	$cartItems=$myCart->getProducts();
if( $myCart==null || empty($cartItems) )
{
	//stale cart , cart is made empty from abother tab/window - cannot proceed. Rebuild the cart again
	func_header_location($http_location."/mkmygiftcardcart.php?at=c&pagetype=productdetail&src=po",false);
}

$giftCard=$cartItems["giftcard"];
$smarty->assign("gc",$giftCard->giftCardDetails);
$smarty->assign("giftCardDetails",$giftCard->giftCardDetails);
$giftCardId=$giftCard->giftCardDetails->giftCardMessage->giftCardOccasion->id;
$giftCardOccasion=GiftCardsHelper::getOccasionById($giftCardId);
$smarty->assign("giftCardOccasion", $giftCardOccasion);
$smarty->assign("gc_occasion", $giftCardOccasion);
$smarty->assign("gc_message", $giftCard->giftCardDetails->giftCardMessage);
$smarty->assign("giftCardPaymentPage", true);

/*$giftCard = new GiftCard();
//$giftCard->setAmount(filter_input(INPUT_POST, 'amount', FILTER_SANITIZE_STRING));
$giftCard->setAmount(500);
//echo filter_input(INPUT_POST, 'amount', FILTER_SANITIZE_STRING);exit;
$giftCard->setQuantity(1);
$giftCard->setType(0);
$giftCard->setSenderEmail($login);
$giftCard->setRecipientEmail(filter_input(INPUT_POST, 'recipient_email', FILTER_SANITIZE_STRING));
$giftCard->setSenderName(filter_input(INPUT_POST, 'sender_name', FILTER_SANITIZE_STRING));
$giftCard->setRecipientName(filter_input(INPUT_POST, 'recipient_name', FILTER_SANITIZE_STRING));
$giftCardMessage = new GiftCardMessage();
$giftCardMessage->setMessage(filter_input(INPUT_POST, 'message', FILTER_SANITIZE_STRING));
$giftCardOccasion = new GiftCardOccasion();
$giftCardOccasion->setId(filter_input(INPUT_POST, 'occasion_id', FILTER_SANITIZE_STRING));
$giftCardMessage->setGiftCardOccasion($giftCardOccasion);
$giftCard->setGiftCardMessage($giftCardMessage);
//$giftCardDetails=json_encode($giftCard);
//$mcartItem= new MCartGiftCardItem($giftCard->getAmount(), 1);
$mcartItem= new MCartGiftCardItem(500, 1);
$mcartItem->setGiftCardDetails($giftCard);
//var_dump($giftCard);exit;

$myCart->addProduct($mcartItem, false, false);
*/

//echo "<PRE>";
//var_dump($myCart);exit;

// Coupons widget.
// Cash Coupons are deprecated
//$cashcoupons = $adapter->getMyntraCreditsCoupon($XCART_SESSION_VARS["login"]);

$calcCouponDiscount = false;
$showCouponMessage = false;
$calcCashCouponDiscount = false;
$cashCouponMessage = false;
//coupone code logic
if (!empty($HTTP_POST_VARS["couponcode"])) {
	$couponCode = $HTTP_POST_VARS["couponcode"];
	$myCart->addCouponCode($couponCode, MCart::$__COUPON_TYPE_DEFAULT);
}/*else if(!empty($HTTP_POST_VARS["cashcoupon"]) && $HTTP_POST_VARS["cashcoupon"]=='removecoupon'){
	$cashCouponCodeArrayRem=array_keys($myCart->getAllCouponCodes(MCart::$__COUPON_TYPE_CASHBACK));
	$myCart->removeCoupon($cashCouponCodeArrayRem[0], MCart::$__COUPON_TYPE_CASHBACK);
	$smarty->assign("appliedCashbackCode", "");
}
else if(!empty($HTTP_POST_VARS["cashcoupon"])){
	$cashCouponCode = $HTTP_POST_VARS["cashcoupon"];
	$userCashAmount = $HTTP_POST_VARS["userAmount"];
	$myCart->addCouponCode($cashCouponCode, MCart::$__COUPON_TYPE_CASHBACK,array('userCashAmount'=>$userCashAmount));
}*/
else if(!empty($HTTP_POST_VARS["removecouponcode"]) && $HTTP_POST_VARS["removecouponcode"]=='remove')
{
	$couponCodeArrayRem=array_keys($myCart->getAllCouponCodes(MCart::$__COUPON_TYPE_DEFAULT));
	$myCart->removeCoupon($couponCodeArrayRem[0], MCart::$__COUPON_TYPE_DEFAULT);
	$smarty->assign("appliedCouponCode", "");
}else if(!empty($HTTP_POST_VARS["useMyntCash"]) && $HTTP_POST_VARS["useMyntCash"]=='use'){
	//$cashCouponCode = $HTTP_POST_VARS["cashcoupon"];
	$userCashAmount = $HTTP_POST_VARS["userAmount"];
	$myCart->enableMyntCashUsage($userCashAmount);
	//$myCart->addCouponCode($cashCouponCode, MCart::$__COUPON_TYPE_CASHBACK,array('userCashAmount'=>$userCashAmount));
}else if(!empty($HTTP_POST_VARS["useMyntCash"]) && $HTTP_POST_VARS["useMyntCash"]=='remove'){
	$myCart->removeMyntCashUsage();
	//$cashCouponCodeArrayRem=array_keys($myCart->getAllCouponCodes(MCart::$__COUPON_TYPE_CASHBACK));
	//$myCart->removeCoupon($cashCouponCodeArrayRem[0], MCart::$__COUPON_TYPE_CASHBACK);
	//$smarty->assign("appliedCashbackCode", "");
}else{
	//$myCart->addCouponCode($cashcoupons[0]['coupon'], MCart::$__COUPON_TYPE_CASHBACK,array('userCashAmount'=>$cashcoupons[0]['MRPAmount']));
	//$smarty->assign("appliedCashbackCode", "");
	 $giftcardCashbackEnabled = FeatureGateKeyValuePairs::getBoolean('giftcardCashback.enabled');
	 if(!$giftcardCashbackEnabled){
         	$smarty->assign('cashback_gateway_status', "off");
	        $myCart->removeMyntCashUsage();
	 }
	 //apply cashback/myntcash by default
	 else if($myntCashDetails["balance"]>0){
                $myCart->enableMyntCashUsage($myntCashDetails["balance"]);
         }
}

$myCart->refresh(true,true,true,false);

//echo "<PRE>";var_dump($myCart);exit;

if(!$myCart->isReadyForCheckout()){
	MCartUtils::persistCart($myCart);
	//To redirect if giftcard not ready for purchase
	//func_header_location($https_location."/mkmycartmultiple.php?at=c&pagetype=productdetail&src=po",false);
}

$cashCouponCodeArray=array_keys($myCart->getAllCouponCodes(MCart::$__COUPON_TYPE_CASHBACK));
//Currently We have just one coupon for each type
$cashCouponCode=$cashCouponCodeArray[0];
/*	
if(!empty($cashCouponCode)){
	$showCashCouponMessage=true;
	$smarty->assign("appliedCashbackCode", $cashCouponCode);
	$applicationCode = $myCart->getCouponApplicationCode($cashCouponCode, MCart::$__COUPON_TYPE_CASHBACK);
	if($applicationCode===MCart::$__COUPON_APP_CODE_SUCCESS){
		$cashdiscount = number_format($myCart->getCashDiscount(), 2, ".", ',');
		$smarty->assign("cashdiscount", $cashdiscount);
		$abs_discount=$cashcoupons[0]['MRPAmount'];
		if($cashdiscount <= $abs_discount)
		{
			$cash_left=$abs_discount - $cashdiscount;
			$cashCouponDesc= "Use your Myntra credits. Current balance: Rs $cash_left ";
		}
		$cashCouponMessage = "You have redeemed <em>Rs. $cashdiscount</em> of your <em>Rs. ".number_format($cashcoupons[0]['MRPAmount'],2)."</em> cashback";
		$smarty->assign("cashdivid", "success");
	}else{
		$cashCouponMessage = $myCart->getCouponApplicationErrorMessage($cashCouponCode,  MCart::$__COUPON_TYPE_CASHBACK);
		$myCart->removeCoupon($cashCouponCode, MCart::$__COUPON_TYPE_CASHBACK);
		$smarty->assign("cashdivid", "error");
	}
}*/

//echo "<pre/>";var_dump($myCart);exit;
MCartUtils::persistCart($myCart);
x_session_unregister("LAST_CART_UPDATE_TIME");
unset($XCART_SESSION_VARS["LAST_CART_UPDATE_TIME"]);
$lastUpdatedTime=$myCart->getLastContentUpdatedTime();
x_session_register('LAST_CART_UPDATE_TIME',$lastUpdatedTime);

$smarty->assign("cashcoupons", $cashcoupons[0]);
$additionalCharges = number_format($myCart->getAdditionalCharges(), 2, ".", '');
$totalMRP = number_format($myCart->getMrp(), 2, ".", '');
$vat = number_format($myCart->getVat(), 2, ".", '');
$totalQuantity = number_format($myCart->getItemQuantity(), 0);
$couponDiscount = number_format($myCart->getDiscount(), 2, ".", '');
$cashDiscount = number_format($myCart->getCashDiscount(), 2, ".", '');
$myntCashUsage = number_format($myCart->getTotalMyntCashUsage(), 2, ".", '');
$productDiscount = number_format($myCart->getDiscountAmount(), 2, ".", '');
$shippingCharge = number_format($myCart->getShippingCharge(), 2, ".",'');
$cartDiscount = number_format($myCart->getCartLevelDiscount(), 2, ".", '');
$totalAmount = $totalMRP + $additionalCharges - $productDiscount - $couponDiscount - $myntCashUsage + $shippingCharge - $cartDiscount;

$coupondiscount = $couponDiscount;
$cashdiscount = $cashDiscount;
$mrp = $totalMRP;
$amount = $totalMRP + $additionalCharges;
$cashback_tobe_given=$myCart->getCashbackToGivenOnCart();
$nonDiscountedItemAmount=$myCart->getNonDiscountedItemsSubtotal();
$nonDiscountedItemCount=$myCart->getNonDiscountedItemCount();
$couponAppliedItemsCount=$myCart->getCouponAppliedItemCount();
$couponAppliedSubtotal=$myCart->getCouponAppliedSubtotal();

/*if($showCouponMessage) {
	$smarty->assign("couponMessage", $couponMessage);
}*/
if($showCashCouponMessage) {
	$smarty->assign("cashCouponMessage", $cashCouponMessage);
}
/*if(!empty($cashback_tobe_given))
        $smarty->assign("cashback_tobe_given",$cashback_tobe_given);
*/
$smarty->assign("cashCouponDesc", $cashCouponDesc);

/* fetch balance from MyntCashService */
$myntCashDetails = MyntCashService::getUserMyntCashDetails($login);
$smarty->assign("myntCashDetails",$myntCashDetails);

if($myCart->isMyntCashUsed() && $myCart->getTotalMyntCashUsage() >0){
	$showCashCouponMessage=true;
	//$smarty->assign("appliedCashbackCode", $cashCouponCode);
	//$applicationCode = $myCart->getCouponApplicationCode($cashCouponCode, MCart::$__COUPON_TYPE_CASHBACK);

	$myntCashUsage = number_format($myCart->getTotalMyntCashUsage(), 2, ".", ',');
	$smarty->assign("myntCashUsage", $myntCashUsage);
	/*$abs_discount=$cashcoupons[0]['MRPAmount'];
		if($cashdiscount <= $abs_discount)
		{
	$cash_left=$abs_discount - $cashdiscount;
	$cashCouponDesc= "Use your Myntra credits. Current balance: Rs $cash_left ";
	}*/
	$myntCashUsageMessage = "You have redeemed <em>Rs. $myntCashUsage</em> of your <em>Rs. ".number_format($myntCashDetails['balance'],2)."</em> cashback";
	$smarty->assign("myntCashdivid", "success");
	$smarty->assign("myntCashUsageMessage", $myntCashUsageMessage);

}


if( $totalAmount < 1) { //less than 1re payment is not accepted by banks to making the amount to zero in case final amount falls below 1re.
	$totalAmount = 0;
}

$totalAmount = round($totalAmount);
$vat = ($vat < 0 ? 0 : $vat);
/*global $enable_cashback_discounted_products;

if(!empty($enable_cashback_discounted_products)){
	if($enable_cashback_discounted_products == "true")	{
		$totalCashBackAmount = $totalAmount;
	} else {
		$totalCashBackAmount = $myCart->getTotalCashBackAmount();
	}
} else {
	$totalCashBackAmount = $myCart->getTotalCashBackAmount();
}
*/
if($totalCashBackAmount < 0)	{
	$totalCashBackAmount = 0;
}

//end recalculation of price.

//get countries list
/*$returnCountry = func_get_countries();
// load shipment options
$shippingoptions = func_load_shipping_options();

$coupon = null;
$couponCode = $XCART_SESSION_VARS['couponCode'];
if(!empty($couponCode)) {
    try {
        $coupon = $couponAdapter->fetchCoupon($couponCode);
    }
    catch (CouponNotFoundException $e) {
        $coupon = null;
    }
}

$logisticId = $shippingoptions[0]['code'] ;
*/
//$shippingRate = func_shipping_rate_re($country,$state, $shippingCity, $productsInCart, $productids, $coupon,$logisticId, null ,$amount);
//$shippingTotalAmount = number_format($shippingRate['totalshippingamount'],2,".",'');
//$box_count = $shippingRate['box_count'];
$smarty->assign("shippingRate", $shippingCharge);
//$smarty->assign("box_count",$box_count);

$amountForCitiDiscount = $totalAmount + $coupondiscount;
$smarty->assign("amountForCitiDiscount",$amountForCitiDiscount);
$smarty->assign("amount",$amount);
$smarty->assign("vat",$vat);
$smarty->assign("mrp",$mrp);
$smarty->assign("couponCode",$couponCode);
$smarty->assign("coupondiscount",$coupondiscount);
$smarty->assign("couponAppliedItemsCount", $couponAppliedItemsCount);
$smarty->assign("couponAppliedSubtotal", $couponAppliedSubtotal);
$smarty->assign("cashdiscount",$cashdiscount);
$smarty->assign("coupondiscounttodisplay",$coupondiscounttodisplay);
$smarty->assign("couponpercent", $couponpercent);
$smarty->assign("totalAmount",$totalAmount);
$smarty->assign("totalQuantity",$totalQuantity);
$smarty->assign("productDiscount",$productDiscount);
$smarty->assign("cartLevelDiscount",$cartDiscount);
$smarty->assign("totalCashBackAmount", $totalCashBackAmount);
$smarty->assign("nonDiscountedItemAmount", $nonDiscountedItemAmount);
$smarty->assign("nonDiscountedItemCount", $nonDiscountedItemCount);
/*
$condNoDiscountCouponFG = FeatureGateKeyValuePairs::getBoolean('condNoDiscountCoupon');
$smarty->assign('condNoDiscountCouponFG', $condNoDiscountCouponFG);
*/
//PORTAL-667: Kundan: made order of online payments configurable via admin interface
$defaultPaymentOrder = array("credit_card", "emi", "debit_card", "net_banking", "cod","pay_by_phone");
$payment_options = FeatureGateKeyValuePairs::getStrArray("GiftingPaymentOptionsOrder", $defaultPaymentOrder);
$smarty->assign("payment_options",$payment_options);
//Kundan: the selected payment option will always be the first payment option
$smarty->assign("defaultPaymentType", $payment_options[0]);

//$onlineServiceable = func_check_prepaid_serviceability($zipcode);
$onlineServiceable = true;
if(!$onlineServiceable) {
	$onlineErrorMessage = "Prepaid delivery is not available for your Pincode.";
	$smarty->assign("onlineErrorMessage",$onlineErrorMessage);
}

$smarty->assign("loginid",$login);
/*$smarty->assign("logisticId", $logisticId);

if($shippingCity!=null){
	$smarty->assign("s_city",1);
} else {
	$smarty->assign("s_city",0);
}*/

$checksumkey = uniqid();
$smarty->assign("checksumkey", $checksumkey);

// For PTG specific campaign.
global $system_ptg_campaign;
if ($system_ptg_campaign) {
    if (!empty($XCART_SESSION_VARS["qualified_for_ptg_campaign"])) {
        $smarty->assign("ptg_campaign_qualified", true);
        global $system_ptg_campaign_threshold;
        $smarty->assign("ptg_threshold", $system_ptg_campaign_threshold);
    }
}

if(isset($_GET['transaction_status'])&& ($_GET['transaction_status'] == 'EMI' ||$_GET['transaction_status'] == 'B' || $_GET['transaction_status'] == 'N')) $smarty->assign("transaction_status",$_GET['transaction_status']);

if(!empty($_GET['vbv'])&&$_GET['vbv'] =='F' ) {
	$smarty->assign("vbvfail",true);
} else {
	$smarty->assign("vbvfail",false);
}

$bankVBVLink = null;

$paymentServiceEnabled = PaymentServiceInterface::getPaymentServiceEnabledForUser($login);
if($paymentServiceEnabled) {
	$weblog->info("Payment service used for login $login as per featuregate");
}


if(!empty($_GET['bank_name'])) {
	$bank_name = trim(strtoupper($_GET['bank_name']));
	$bankVBVLink = func_query_first_cell("select card_bank_registration_link from mk_cards_domestic_bins where card_bank_code='$bank_name' limit 1");
}

if(!empty($bankVBVLink)){
	$smarty->assign("bankVBVLink", $bankVBVLink);
	$smarty->assign("bankName", $bank_name);
} else {
	$smarty->assign("bankVBVLink", false);
}

$icici_gateway_up = FeatureGateKeyValuePairs::getBoolean('payments.iciciPG');
$axis_gateway_up =  FeatureGateKeyValuePairs::getBoolean('payments.axisPG');
$icici_gateway_available = FeatureGateKeyValuePairs::getBoolean('payments.isICICIPGAvailable');
$axis_gateway_available = FeatureGateKeyValuePairs::getBoolean('payments.isAxisPGAvailable');
$citi_gateway_up = FeatureGateKeyValuePairs::getBoolean('payments.citiPG');
$citi_gateway_available = FeatureGateKeyValuePairs::getBoolean('payments.isCITIPGAvailable');
$smarty->assign("is_icici_gateway_up", (($icici_gateway_up&&$icici_gateway_available)||($axis_gateway_up&&$axis_gateway_available)));

$citi_discount = FeatureGateKeyValuePairs::getInteger('payments.citiBankCardDiscount');
//additional discount for citibank
if($citi_discount>0&&$amountForCitiDiscount>1000) {
	$smarty->assign("citi_discount", $citi_discount);
} else {
	$smarty->assign("citi_discount", 0);
}

$icici_discount = FeatureGateKeyValuePairs::getInteger('payments.iciciBankCardDiscount');
//additional discount for citibank
if($icici_discount>0) {
	$smarty->assign("icici_discount", $icici_discount);
} else {
	$smarty->assign("icici_discount", 0);
}

if(isset($XCART_SESSION_VARS["oneClickCODElegible"])){
	$checkCODElegible = $XCART_SESSION_VARS["oneClickCODElegible"];
} else {
	$checkCODPaid = func_query_first_cell("select login from xcart_orders where login='$login' and payment_method='cod' and (cod_pay_status='paidtobluedart' or cod_pay_status='paid') limit 1");
    x_session_unregister("oneClickCODElegible");
    if(!empty($checkCODPaid)){
    	x_session_register("oneClickCODElegible","true");
    } else {
    	x_session_register("oneClickCODElegible","false");
    }
    $checkCODElegible = $XCART_SESSION_VARS["oneClickCODElegible"];
}

$oneClickCODEnabled = FeatureGateKeyValuePairs::getBoolean('oneClickCOD');
$codErrorCode = 0;
$codErrorMessage = '';
if($oneClickCODEnabled&&$checkCODElegible == "true" && $codErrorCode == 0) {
	$smarty->assign("oneClickCod", 1);
} else {
       $smarty->assign("oneClickCod", 0);
}
if($codErrorCode == 0 && !$onlineServiceable) {
	$smarty->assign("oneClickCod", 1);
}

//get countries list
$returnCountry = func_get_countries();
$smarty->assign("countries", $returnCountry);
$smarty->assign("countriesJson",json_encode($returnCountry));

$codCharge = FeatureGateKeyValuePairs::getInteger('codCharge');
//additional discount for citibank
if($codCharge>0) {
	$smarty->assign("cod_charge", $codCharge);
} else {
	$smarty->assign("cod_charge", 0);
}

// SMS verification overlay.
$login = $XCART_SESSION_VARS["login"];
$query = "SELECT * FROM xcart_customers WHERE login = '$login';";
$userProfileData = func_query_first($query);
$smarty->assign("userProfileData", $userProfileData);

// Mobile verification status.
//$mobile_status = $smsVerifier->getStatus($userProfileData['mobile'], $userProfileData['login']);
global $skipMobileVerification;
if($mobile_status != 'V' && $skipMobileVerification){
	//$smsVerifier->skipVerification($userProfileData['mobile'], $userProfileData['login']);
	//$mobile_status = $smsVerifier->getStatus($userProfileData['mobile'], $userProfileData['login']);
}

// Show verify button.
$verified_for_another = 0;
//$verifiedLogin = $smsVerifier->getVerifiedLoginFor($userProfileData['mobile']);
if ($verifiedLogin !== null && $verifiedLogin != $userProfileData['login']) {
    $verified_for_another = 1;
}
$smarty->assign("verified_for_another", $verified_for_another);
$smarty->assign("mobile_status", $mobile_status);
$smarty->assign("userMobile",$userProfileData['mobile']);

$payByPhoneNumber = WidgetKeyValuePairs::getWidgetValueForKey('payByPhoneNumber');
if($payByPhoneNumber != NULL) {
	$smarty->assign("pay_by_phone_number", $payByPhoneNumber);
} else {
	//fall back should not happen
	$smarty->assign("pay_by_phone_number", $customerSupportCall);
}

$citiDiscountBins = PaymentGatewayDiscount::$cardsBin['CITI'];
$iciciDiscountBins = PaymentGatewayDiscount::$cardsBin['ICICI'];
$smarty->assign("citi_discount_bins", $citiDiscountBins);
$smarty->assign("icici_discount_bins", $iciciDiscountBins);


$bank_list_text = NetBankingHelper::getNetBankingSelectBoxText();
$smarty->assign("bank_list_text",$bank_list_text);

$netbankingDownBankList = trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('payments.netBankingDownBanks'));
$creditDebitDownBankList = trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('payments.creditDebitDownBanks'));

if(!empty($netbankingDownBankList)) {
	$netbankingDownBankListStr= stringLastReplace(',',' and',$netbankingDownBankList);
	$netbankingDownBankListStr .= " are currently facing technical difficulties and are currently unavailable.<br/>";
	$netbankingDownBankListStr .= "Try using some other bank, or ";
	if($codErrorCode > 0){
		$netbankingDownBankListStr .= "try again later";
	} else {
		$netbankingDownBankListStr .= "buy with Cash on Delivery";
	}
	$smarty->assign("netbankingDownBankListStr", $netbankingDownBankListStr);
}

if(!empty($creditDebitDownBankList)) {
	$creditDebitDownBankListStr = stringLastReplace(',',' and ',$creditDebitDownBankList);
	$creditDebitDownBankListStr .= " are currently facing technical difficulties.<br/>";
	$creditDebitDownBankListStr .= "Please pay using a card from a different bank, or ";
	if($codErrorCode > 0) {
		$creditDebitDownBankListStr .= "try again later";
	} else {
		$creditDebitDownBankListStr .= "choose Cash on Delivery";
	}
	$smarty->assign("creditDebitDownBankListStr", $creditDebitDownBankListStr);
}


///////////////////////////////////////////////////////////////////////////////////////////////////

$GatewayUp = array();
$GatewayUp['ICICI']= (($icici_gateway_up&&$icici_gateway_available));
$GatewayUp['CITI']= (($citi_gateway_up&&$citi_gateway_available));


$EMIBanks = \FeatureGateKeyValuePairs::getFeatureGateValueForKey('Payments.EMIBanks');
$EMIBanks = json_decode($EMIBanks, true);
$EMIBanksAvailable = array();
$EMIenabled = false;
$minAmountforEMI = PHP_INT_MAX;

$EMIEligibleGatewayUp = false;
$EMIEligibleGatewayDownList = array();

foreach ($EMIBanks as $key => $value) {
	$bankArray = array();
	$bankData = \FeatureGateKeyValuePairs::getFeatureGateValueForKey('Payments.EMIBanks.'.$value);
	$bankData = json_decode($bankData, true);
	$bankArray['BankName'] = $bankData['BankName'];
	$bankArray['Up'] = $GatewayUp[$value];
	$bankArray['Enabled'] = false;
	$bankArray['Durations'] = array();
	
	foreach ($bankData[Durations] as $option => $duration) {
		$durationData = \FeatureGateKeyValuePairs::getFeatureGateValueForKey('Payments.EMIBanks.'.$value.'.'.$duration);
		$durationData = json_decode($durationData,true);
		$bankArray['Durations'][$durationData['Installments'].'MonthsEMI']['Charge'] = $durationData['Charge'];
		$bankArray['Durations'][$durationData['Installments'].'MonthsEMI']['Installments'] = $durationData['Installments'];
		$bankArray['Durations'][$durationData['Installments'].'MonthsEMI']['MinAmount'] = $durationData['MinAmount'];
		
		if ($minAmountforEMI > $durationData['MinAmount'] ) {$minAmountforEMI = $durationData['MinAmount'];}
				
		$bankArray['Durations'][$durationData['Installments'].'MonthsEMI']['TotEMIAmount'] = $totalAmount+$durationData['Charge'];
		$bankArray['Durations'][$durationData['Installments'].'MonthsEMI']['PerMonthEMI'] = round(($totalAmount+$durationData['Charge'])/$durationData['Installments']);
		if ($totalAmount >= $durationData['MinAmount']) {
			$bankArray['Durations'][$durationData['Installments'].'MonthsEMI']['Enabled'] = true;
			$bankArray['Enabled'] = true;
			$EMIenabled = true;
		}
		else {
			$bankArray['Durations'][$durationData['Installments'].'MonthsEMI']['Enabled'] = false;
		}
	}
	
	$bankBins = \FeatureGateKeyValuePairs::getStrArray('Payments.EMIBanks.'.$value.'.Bins');
	$bankArray['Bins'] = $bankBins;
	
	if ($bankArray['Enabled']) {
		if ($bankArray['Up']){
			$EMIEligibleGatewayUp = true;
		}
		else{
			$EMIEligibleGatewayDownList[] = $bankArray['BankName'];
		}
	}
	
	if ($bankArray['Enabled'] && $bankArray['Up']){
		$EMIBanksAvailable[$value]= $bankArray;
	}
};

$EMIGatewayDownListErrorMsg = "";

if (sizeof($EMIEligibleGatewayDownList) > 0){
	if (sizeof($EMIEligibleGatewayDownList) == 1){
		$EMIGatewayDownListErrorMsg .="Currently ".$EMIEligibleGatewayDownList[0]." is facing technical difficulties. Please "; 
		if($codErrorCode > 0){
			$EMIGatewayDownListErrorMsg .= "try again later.";
		} else {
			$EMIGatewayDownListErrorMsg .= "buy with Cash on Delivery.";
		}
	}
	else{
		$BanksString = "";
		foreach ($EMIEligibleGatewayDownList as $value) {
			$BanksString .= " ".$value.","; 	
		}
		$BanksString = rtrim($BanksString, ',');
		print_r($BanksString);
		$EMIGatewayDownListErrorMsg .="Currently".$BanksString." are facing technical difficulties. Please ";

		if($codErrorCode > 0){
			$EMIGatewayDownListErrorMsg .= "try again later.";
		} else {
			$EMIGatewayDownListErrorMsg .= "buy with Cash on Delivery.";
		}
	
	}
}
$smarty->assign('EMIBanksAvailable', $EMIBanksAvailable);
$smarty->assign('EMIenabled', $EMIenabled);
$smarty->assign('EMIEligibleGatewayUp', $EMIEligibleGatewayUp);
$smarty->assign('EMIEligibleGatewayDownList', $EMIEligibleGatewayDownList);
$smarty->assign('minAmountforEMI', $minAmountforEMI);
$smarty->assign('EMIGatewayDownListErrorMsg', $EMIGatewayDownListErrorMsg);



///////////////////////////////////////////////////////////////////////////////////////////////////

//The php to which payment form is to be submitted is set here
$smarty->assign('paymentphp','mkorderinsert.php');
//$smarty->assign('isGiftingPaymentPage','mkgiftcardorderinsert.php');

//echo "$totalAmount $iciciMinEMITotalAmount";exit;
$smarty->assign("is_icici_emi_gateway_up", (($icici_gateway_up&&$icici_gateway_available)));

$smarty->assign("paymentServiceEnabled", $paymentServiceEnabled);
global $errorlog;
if($paymentServiceEnabled) {
	$xid = $_COOKIE[HostConfig::$cookieprefix . 'xid'];
	$serviceRequestParams = "totalAmount=". urlencode($totalAmount);
	$serviceRequestParams .= "&addressid=" . urlencode($address['id']);
	if($oneClickCOD) {
		$serviceRequestParams .= "&oneClickCOD=1";
	}
	$serviceRequestParams .= "&addressname=" . urlencode($address['name']);
	$serviceRequestParams .= "&addressaddress=" . urlencode($address['address']);
	$serviceRequestParams .= "&addresscity=" . urlencode($address['city']);
	$serviceRequestParams .= "&addressstatename=" . urlencode($address['statename']);
	$serviceRequestParams .= "&addresspincode=" . urlencode($address['pincode']);
	$serviceRequestParams .= "&addresscountry=" . urlencode($address['country']);
	$serviceRequestParams .= "&clientCode=giftcard";
	$serviceRequestParams .= "&cartContext=".CartContext::GiftCardContext;
	$serviceRequestParams .= "&coderrorcode=" . urlencode($codErrorCode);
	$serviceRequestParams .= "&login=" . urlencode($login);
	$serviceRequestParams .= "&xid=" . urlencode($xid);
	$profile = HostConfig::$httpHost;
	$serviceRequestParams .= "&profile=" . urlencode($profile);
	
	if(!empty($onlineErrorMessage)) {
		$serviceRequestParams .= "&onlineErrorMessage=". urlencode($onlineErrorMessage);
	}

	if(!empty($codErrorMessage)) {
		$serviceRequestParams .= "&codErrorMessage=".urlencode($codErrorMessage);
	}

	$url = HostConfig::$paymentServiceInternalHost.'/payment?'. $serviceRequestParams;

	//echo "$url<br/>";
	$restRequest = new RestRequest($url, 'GET');

	$headers = array();
	array_push($headers, 'Accept: text/html');
	array_push($headers,'Content-Type: application/json');

	$restRequest ->setHttpHeaders($headers);
	$restRequest ->execute();
	$responseInfo = $restRequest ->getResponseInfo();
	$responseBody = $restRequest ->getResponseBody();

	//print_r($responseInfo);
	//exit;
	if(!$responseBody || empty($responseBody) || $responseInfo['http_code'] != 200) {
		$weblog->error("Payment options could not be loaded.");
		$errorlog->error("Payment options could not be loaded in gift cards checkout.");
	}

	$smarty->assign("paymentPageUI", $responseBody);

}

$tracker->fireRequest();

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setContextualPageData(AnalyticsBase::PAYMENT_PAGE);
$analyticsObj->setPaymentEventVars();
$analyticsObj->setTemplateVars($smarty);
if (isset($XCART_SESSION_VARS['telesalesAgent'])){
            $smarty->assign("customeragent", false);
            $smarty->assign("telesalesmsg","This payment option is not available for telesales agents placing order on behalf of customers. Please use COD to place order.");
 }
 else{
            $smarty->assign("customeragent", true);
 }

$smarty->assign("telesalesmsg","Gift card option is not available for telesales agent placeing order on behalf of customer.");
if($skin === "skin2") {
    //require_once "$xcart_dir/cart_details.php";
	$smarty->display("checkout/giftcardpayment.tpl");
} else {
    func_display("cart/inlinepayment.tpl",$smarty);
}
?>
