<?php

/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     function.myntra_link.php
 * Type:     function
 * Name:     myntra_link
 * Purpose:  convert normal link to myntralink
 * -------------------------------------------------------------
 */

function smarty_function_myntralink($params, &$smarty)
{
	$parameters_array = $params;

	ksort($parameters_array);

	$values = array_values($parameters_array);
	if(count($parameters_array)==1){
		return strtolower(str_replace(array(","," "), "-", $values[0]));
	}
	else {
		/* URL generation order
		 * brand>gender>team name>sport/usage>article type
		 * */

		$url_desc='';
		// Brand
		$url_desc .= str_replace(array(","," "), "-", $parameters_array['brands_filter_facet']);
		// Gender
		if(!empty($url_desc) && !empty($parameters_array['global_attr_gender'])){
			$url_desc .= "-";
		}
		$url_desc .= str_replace(array(","," "), "-", $parameters_array['global_attr_gender']);

		// Display Category
		if(!empty($url_desc) && !empty($parameters_array['display_category_facet'])){
			$url_desc .= "-";
		}
		$url_desc .= str_replace(array(","," "), "-", $parameters_array['display_category_facet']);

		// Team Name
		if(!empty($url_desc) && !empty($parameters_array['team_filter_facet'])){
			$url_desc .= "-";
		}
		$url_desc .= str_replace(array(","," "), "-", $parameters_array['team_filter_facet']);
		// Usage
		if(!empty($url_desc) && !empty($parameters_array['global_attr_usage'])){
			$url_desc .= "-";
		}
		$url_desc .= str_replace(array(","," "), "-", $parameters_array['global_attr_usage']);

		// Article Type
		if(!empty($url_desc) && !empty($parameters_array['global_attr_article_type_facet'])){
			$url_desc .= "-";
		}
		$url_desc .= str_replace(array(","," "), "-", $parameters_array['global_attr_article_type_facet']);

		if(empty($url_desc)){
			$url_desc = str_replace(array(","," "), "-", implode("-", $parameters_array));
		}

		return strtolower($url_desc);
	}
}

?>
