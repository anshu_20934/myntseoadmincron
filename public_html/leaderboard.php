<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/auth.php';
include_once "$xcart_dir/include/func/func.order.php";
//include_once "$xcart_dir/include/class/leaderboard/LeaderBoard.php";

use leaderboard\LeaderBoard;

/*
if (empty($login)) {
    $queryString = $_SERVER['QUERY_STRING'] 
                        ? isset($_GET['view'])
                            ? $_SERVER['QUERY_STRING']
                            : $_SERVER['QUERY_STRING'] . "&view=default"
                        : "view=default";
    echo "<meta http-equiv=\"Refresh\" content=\"0;URL=$http_location/register.php?".$queryString."\" >";
    exit;
}
*/
$leaderBoardObject = new LeaderBoard();
$lbId = $_REQUEST['lbId'];
$lbId = mysql_real_escape_string($lbId);
$leaderBoardMetaData = $leaderBoardObject->getLeaderBoardMetaData($lbId);

if (empty($lbId) or empty($leaderBoardMetaData))
    header( "Location: ". $http_location );

$currentTime = time();
if (($currentTime < $leaderBoardMetaData['display_start_time']) or ($currentTime > $leaderBoardMetaData['display_end_time']))
    header( "Location: ". $http_location );

$exists = false;
if (!empty($login)) {
    $exists = true;    
    if($leaderBoardMetaData['rule'] == "list") {
        $exists = func_query_first("select login from leader_board_users where lb_fk_id = '$lbId' and login = '$login'",true);
    }
}

if(!$exists || !$_REQUEST['lbId'] || !isInteger($_REQUEST['lbId'])){
    if (empty($login)) {
        $queryString = $_SERVER['QUERY_STRING'] 
                        ? isset($_GET['view'])
                            ? $_SERVER['QUERY_STRING']
                            : $_SERVER['QUERY_STRING']
                        : "view=default";
        echo "<meta http-equiv=\"Refresh\" content=\"0;URL=$http_location/register.php?".$queryString."&view=leaderboard\" >";
    }else{
        echo "<meta http-equiv=\"Refresh\" content=\"0;URL=$http_location/mymyntra.php?view=myprofile \" >";
    }
    exit;
}

$mode = $_POST['mode'];

if($mode == 'purchases'){
    $purchaseSummary = $leaderBoardObject->getUserPurchasesForLeaderBoard($login,$lbId);
    $smarty->assign("purchaseSummary", $purchaseSummary);
    
    $smarty->assign("section", "mypurchases");
    $html=func_display("leaderboard-ajax.tpl",$smarty,false);
    echo $html;
    exit;
}else if($mode == 'trendingStyles'){
    $trendingStyles = $leaderBoardObject->getTrendingStylesForLeaderBoard($lbId);
    $smarty->assign("trendingStyles",$trendingStyles);
    $smarty->assign("section","trendingStyles");
    $html=func_display("leaderboard-ajax.tpl",$smarty,false);
    echo $html;
    exit;
}else if($mode == 'leaderBoard'){
    $leaderBoard = $leaderBoardObject->getTopNBuyersForLeaderBoard($lbId);
    $smarty->assign("leaderBoard",$leaderBoard);
    $smarty->assign("section","leaderBoard");
    $html=func_display("leaderboard-ajax.tpl",$smarty,false);
    echo $html;
    exit;
}else if($mode == 'search'){
    $smarty->assign("section","leaderBoardSeach");
    $html=func_display("leaderboard-ajax.tpl",$smarty,false);
    echo json_encode(array("result" => "success", "content" => $html));
    exit;
}else if($mode == 'message'){
    echo $leaderBoardObject->getTickerMessageForLeaderBoard($lbId);
    exit;
}else{

    if ($leaderBoardMetaData['type'] == 'OVERALL') {
        $leaderBoard = $leaderBoardObject->getTopNBuyersForLeaderBoard($lbId);
        $smarty->assign("leaderBoard",$leaderBoard);
    }

    if ($leaderBoardMetaData['type'] == 'MEN' or $leaderBoardMetaData['type'] == 'BOTH') {
        $menLeaderBoard = $leaderBoardObject->getTopNMenBuyersForLeaderBoard($lbId);
        $smarty->assign("menLeaderBoard",$menLeaderBoard);
    }

    if ($leaderBoardMetaData['type'] == 'WOMEN' or $leaderBoardMetaData['type'] == 'BOTH') {
        $womenLeaderBoard = $leaderBoardObject->getTopNWomenBuyersForLeaderBoard($lbId);
        $smarty->assign("womenLeaderBoard",$womenLeaderBoard);        
    }

    $trendingStyles = $leaderBoardObject->getTrendingStylesForLeaderBoard($lbId);
    $smarty->assign("trendingStyles",$trendingStyles);
    
    $purchaseSummary = $leaderBoardObject->getUserPurchasesForLeaderBoard($login, $lbId);
    $smarty->assign("purchaseSummary", $purchaseSummary);


    if (!empty($leaderBoardMetaData['qualifying_amount'])) {
        $amountToQualify = (int)$leaderBoardMetaData['qualifying_amount'] - (int)$purchaseSummary['total'];
        if ($amountToQualify > 0) {
            $diff = $amountToQualify;
            $flashQualifyMessage = $leaderBoardMetaData['qualifying_statement'];
            $replacedString = str_replace("$", $diff, $flashQualifyMessage);
            $smarty->assign("flashQualifyMessage", $replacedString);
        }
        else {
            $flashQualifiedMessage = $leaderBoardMetaData['post_qualifying_statement'];
            $smarty->assign("flashQualifiedMessage", $flashQualifiedMessage);

        }
    }

    $pageParams    = $leaderBoardObject->getAllConfigurableFields($lbId);
    
    $pageTitle     = empty($pageTitle) ? 'Welcome to Myntra Shopping Festival!!' : $pageParams['title'];
    $smarty->assign( 'pageTitle', $pageTitle );
    
    $tickerMessage = empty($pageParams['message']) ? "" : $pageParams['message'];
    $smarty->assign( 'tickerMessage', $tickerMessage );
    
    $bannerHref    = empty($pageParams['banner_href']) ? "" : $pageParams['banner_href'];
    $smarty->assign( 'bannerHref', $bannerHref );
    
    $bannerSrc     = empty($pageParams['banner_url']) ? "" : $pageParams['banner_url'];
    $smarty->assign( 'bannerSrc', $bannerSrc );

    $smarty->assign("lbId",$lbId);
    $smarty->display("leaderboard.tpl");    
}

?>
