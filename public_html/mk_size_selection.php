<?php
include_once($xcart_dir."/include/func/func.mkcore.php");
include_once($xcart_dir."/include/func/func.mkmisc.php");
require_once "./auth.php";

define("PRODUCT_QUANTITY",50);
$productid=$HTTP_GET_VARS['productid'];
$typeid=$HTTP_GET_VARS['typeid'];
$typename=$HTTP_GET_VARS['typename'];
$styleid=$HTTP_GET_VARS['styleid'];
$stylename=$HTTP_GET_VARS['stylename'];
$unitprice=$HTTP_GET_VARS['unitprice'];

#
#Registering chosenProductConfiguration and masterCustomizationArray
#
$chosenProductConfiguration=array();
if (x_session_is_registered("chosenProductConfiguration"))
	x_session_unregister("chosenProductConfiguration");

#
#Load default customization area
#
//echo "SELECT id, name FROM ".$sql_tbl['mk_style_customization_area']." WHERE style_id='".$styleid."' AND isprimary=1";
$defaultCustId=func_query_first_cell("SELECT id, name FROM ".$sql_tbl['mk_style_customization_area']." WHERE style_id='".$styleid."' AND isprimary=1");
#
#Load product customization areas
#
$productCustomizationAreas=func_get_all_customization_area_for_product_style($styleid);

$chosenProductConfiguration['productStyleId'] = $styleid;
$chosenProductConfiguration['productStyleLabel'] = $stylename;
$chosenProductConfiguration['productId'] = $productid;
$chosenProductConfiguration['customizationAreaId'] = $defaultCustId;
$chosenProductConfiguration['currentCustomizationAreaId'] = $defaultCustId;
$chosenProductConfiguration["defaultCustomizationId"] = $defaultCustId;
$chosenProductConfiguration['productTypeId']=$typeid;
$chosenProductConfiguration['productTypeLabel'] = $typename;
$chosenProductConfiguration['unitPrice'] = $unitprice;

x_session_register("chosenProductConfiguration");
#
#Load size options values
#
$sql = "SELECT value FROM $sql_tbl[mk_product_options] WHERE style='$styleid' ORDER BY id";
$result = db_query($sql);
$num_rows = db_num_rows($result);
if($num_rows>0){
	$styleoptions = array();
	$j=0;
	while($row = db_fetch_array($result)){
		$options[$j] = $row['value'];
		$j++;
	}
	$styleoptions['styles']=$options;
}else{
	$options=0;
}

#
#Loop for product quantity
#
$quantity_array=array();
for($i=0;$i<=PRODUCT_QUANTITY;$i++){
	$quantity_array[]=$i;
}
$heading = ($typename=='T-Shirt')?"Please Select size and quantity":"Please Select quantity";
$smarty->assign("heading",$heading);

if($typename=="Mithai" || $typename=="Ooty"){
	$flag= "direct_add_to_cart";
	echo $flag;
}else{
	$smarty->assign("stylename",$stylename);
	$smarty->assign("quantity_array",$quantity_array);
	$smarty->assign("styleoptions",$options);
}
func_display("mk_size_selection.tpl", $smarty);

?>