<?php
include_once("auth.php");
include_once("include/class/customization/CustomizationHelper.php");
try
{
	//$text = $HTTP_GET_VARS["text"]; // Deprecated use of $HTTP_GET_VARS..... Using $_GET
	
	$text = $_GET["text"];
	
	$text = trim($text);
	$windowid = $HTTP_GET_VARS["windowid"];
	$thisChosenProductConfiguration = $XCART_SESSION_VARS["chosenProductConfigurationEx"][$windowid];
	$thisCustomizationArray = $XCART_SESSION_VARS["customizationArrayEx"][$windowid];
	$thisMasterCustomizationArray = $XCART_SESSION_VARS["masterCustomizationArrayEx"][$windowid];
	
	$params = array("custArray"=>$thisCustomizationArray,"masterCustomizationArray"=>$thisMasterCustomizationArray,"chosenProductConfiguration"=>$thisChosenProductConfiguration,"globalDataArray"=>$globalDataArray);
	$customizationHelper = new CustomizationHelper($params );
	$customizationHelper->updateTextInfoInCustArea($text);
	
	$XCART_SESSION_VARS["customizationArrayEx"][$windowid] = $customizationHelper->getCustomizationArray();
	$isTextCustAllowed = $customizationHelper->isTextCustomizationAllowedForOrientation();
	$objectid = $customizationHelper->getTextObjectIDForCurrentOrientation();

	echo "success#$isTextCustAllowed#$text#$objectid";
}
catch(Exception $ex)
{
	echo "<pre>";print_r($ex);
}
?>
