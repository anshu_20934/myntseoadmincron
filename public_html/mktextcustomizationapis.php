<?php
include_once("./include/TempImages.php");

######## Place text ######
function func_put_text_on_generated_image($fontbold,$fontcolor,$fontitalics,$fontsize,$fonttype,$fonttext,$textdx,$textdy,&$custarray,&$smarty,$bgCustOrientationDetails){
	global $mk_fonts, $customizationAreaId, $isJPG;
	global $isPreviewPage,$orientationId,$chosenProductConfiguration;;
	global $xcart_dir;
	global $weblog;

	// creating transparent image for text
	$textImage = imagecreatetruecolor($custarray[$orientationId]['w'],$custarray[$orientationId]['h']);
	imagesavealpha($textImage,true);
	imagealphablending($textImage,false);
	$transparentColor = imagecolorallocatealpha($textImage, 100, 100, 200, 127);
	imagefill($textImage, 0, 0, $transparentColor);

	######### For background customization image ###
	$bgtextImage = imagecreatetruecolor($bgCustOrientationDetails['bgsize_x'],$bgCustOrientationDetails['bgsize_y']);
	imagesavealpha($bgtextImage,true);
	imagealphablending($bgtextImage,false);
	$transparentColor = imagecolorallocatealpha($bgtextImage, 100, 100, 200, 127);
	imagefill($bgtextImage, 0, 0, $transparentColor);
	## end

	#### creating transparent image for text
	if(!empty($custarray['common_generated_img'])){
		if ($isJPG){
			$baseImage = imagecreatefromjpeg($custarray['common_generated_img']);
		}else{
			$baseImage = imagecreatefrompng($custarray['common_generated_img']);
		}
	}else{
		if ($isJPG){
			$baseImage = imagecreatefromjpeg($custarray[$orientationId]['generatedimagenotext']);
			if(!empty($custarray['generatedimagenotextbg']))
			$baseBgImage = imagecreatefromjpeg($custarray['generatedimagenotextbg']);
		}else{
			$baseImage = imagecreatefrompng($custarray[$orientationId]['generatedimagenotext']);
			if(!empty($custarray['generatedimagenotextbg']))
			$baseBgImage = imagecreatefromjpeg($custarray['generatedimagenotextbg']);
		}
	}

	##   Previous code
	/*if ($isJPG){
	$baseImage = imagecreatefromjpeg($custarray['generatedimagenotext']);
	if(!empty($custarray['generatedimagenotextbg']))
	$baseBgImage = imagecreatefromjpeg($custarray['generatedimagenotextbg']);
	}else{
	$baseImage = imagecreatefrompng($custarray['generatedimagenotext']);
	if(!empty($custarray['generatedimagenotextbg']))
	$baseBgImage = imagecreatefrompng($custarray['generatedimagenotextbg']);
	}*/

	$fonttypename = $fonttype;
	if((($pos=strpos($fontcolor, "#")) !== false))
	{
		$fontcolor = $fontcolor;
		$textfontcolor = $fontcolor;
	}
	else
	{
		$fontcolor = $fontcolor;
		$textfontcolor = dechex($fontcolor);
	}
	$fontcolor = html2rgb($fontcolor);
	list($rgb, $r, $g, $b) = split('[(,)]', $fontcolor);

	$fontcolor = imagecolorallocate($baseImage, $r, $g, $b); //TODO change the color to the recieved color
	if(!empty($baseBgImage))
	$fontcolor = imagecolorallocate($baseBgImage, $r, $g, $b); //For cust Bg Image
	if (strpos($fonttype,"ttf")) $isfonttypepathset = true;

	if (!$isfonttypepathset)
	$fonttype =  $xcart_dir.'/fonts/'.$mk_fonts[$fonttype];
	// Add the text on the image
	// get the width of the text to be pasted.
	if (empty($custarray[$orientationId]['customizedtext'])){
		$customizedText = array();
		$customizedText['fonttype'] = $fonttype;
		$customizedText['fontsize'] = $fontsize;
		$customizedText['fonttext'] = $fonttext;
		$customizedText['fontcolor'] = $fontcolor;
		$customizedText['textfontcolor'] = $textfontcolor;
		$customizedText['fonttypename'] = $fonttypename;
		$customizedText['textX'] = 0;
		$customizedText['textY'] = 0;
	}
	$customizedText = $custarray[$orientationId]['customizedtext'];
	// print_R($customizedText);
	//exit;
	//put the text on the customized area image
	imageTextWrapped($textImage, $customizedText['textX'] + $textdx, $customizedText['textY'] + $textdy, $custarray[$orientationId]['w'], $fonttype, $fontcolor, $fonttext, $fontsize,'c');
	if(!empty($baseBgImage)){
		imageTextWrapped($bgtextImage, $customizedText['textX'] + $textdx, $customizedText['textY'] + $textdy, $bgCustOrientationDetails['bgsize_x'],  $fonttype, $fontcolor, $fonttext, $fontsize,'c');
	}


	// store the text attributes in customized session array
	$customizedText['fonttype'] = $fonttype;
	$customizedText['fontsize'] = $fontsize;
	$customizedText['fonttext'] = $fonttext;
	$customizedText['fonttypename'] = $fonttypename;
	$customizedText['fontcolor'] = $fontcolor;
	$customizedText['textfontcolor'] = $textfontcolor;
	$customizedText['textX'] += $textdx;
	$customizedText['textY'] += $textdy;

	//fit the customized area image back on the base product image
	paste_customized_area_image($custarray[$orientationId],$baseImage,$textImage);
	paste_customized_bg_image($bgCustOrientationDetails,$baseBgImage,$bgtextImage);
	$len = 4;
	if ($isJPG){
		$tempCustTextFile = "./images/customized/custtext".time().get_rand_id($len).".jpg";
		if(!imagejpeg($baseImage,$tempCustTextFile))
			$weblog->info("unable to create $tempCustTextFile in mktextcustomizationapis.php");
		## For customize bg image
		if(!empty($baseBgImage)){
			$tempBgCustTextFile = "./images/customized/bgimages/custtext".time().get_rand_id($len).".jpg";
			if(!imagejpeg($baseBgImage,$tempBgCustTextFile))
			   $weblog->info("unable to create $tempBgCustTextFile in mktextcustomizationapis.php");
		}
	}else{
		$tempCustTextFile = "./images/customized/custtext".time().get_rand_id($len).".png";
		if(!imagepng($baseImage,$tempCustTextFile))
			$weblog->info("unable to create $tempCustTextFile in mktextcustomizationapis.php");
		## For customize bg image
		if(!empty($baseBgImage)){
			$tempBgCustTextFile = "./images/customized/bgimages/custtext".time().get_rand_id($len).".png";
			if(!imagepng($baseBgImage,$tempBgCustTextFile))
			    $weblog->info("unable to create $tempBgCustTextFile in mktextcustomizationapis.php");
		}
	}

	addToTempImagesList($tempCustTextFile);
	if(!empty($tempBgCustTextFile)){
		addToTempImagesList($tempBgCustTextFile);
	}
	$custarray[$orientationId]['customizedtext'] = $customizedText;


	$custarray[$orientationId]['generatedimage'] = $tempCustTextFile;
	$custarray['common_generated_final_image'] = $tempCustTextFile; //a ddeed later for multiple customization
	//$custarray['common_generated_img'] =   $tempCustTextFile;
	if(!empty($tempBgCustTextFile)){
		$custarray['generatedbgimage'] = $tempBgCustTextFile;
	}
	$smarty->assign("customizedImage",$tempCustTextFile);
	if($isPreviewPage == "1")
	{
		func_display("mkcreatecustomizeimageforpreview.tpl",$smarty);
	}
	else
	{
		func_display("mkcreatecustomizeimage.tpl",$smarty);
	}
	return;
}
/***************** New function for text customization ********************/
function func_put_text_on_generated_commonimage($fontbold,$fontcolor,$fontitalics,$fontsize,$fonttype,$fonttext,$textdx,$textdy,&$custarray,&$smarty,$orientationId,&$common_txt_image){
	global $mk_fonts, $customizationAreaId, $isJPG;
	global $isPreviewPage,$chosenProductConfiguration;;
	global $xcart_dir;
	global $weblog;

   	// creating transparent image for text
	$textImage = imagecreatetruecolor($custarray[$orientationId]['w'],$custarray[$orientationId]['h']);
	imagesavealpha($textImage,true);
	imagealphablending($textImage,false);
	$transparentColor = imagecolorallocatealpha($textImage, 100, 100, 200, 127);
	imagefill($textImage, 0, 0, $transparentColor);

	#### creating transparent image for text
	if(!empty($common_txt_image)){
		if ($isJPG){
			$baseImage = imagecreatefromjpeg($common_txt_image);
		}else{
			$baseImage = imagecreatefrompng($common_txt_image);
		}
	}else{
		if ($isJPG){
			$baseImage = imagecreatefromjpeg($custarray[$orientationId]['generatedimagenotext']);
			if(!empty($custarray['generatedimagenotextbg']))
			$baseBgImage = imagecreatefromjpeg($custarray['generatedimagenotextbg']);
		}else{
			$baseImage = imagecreatefrompng($custarray[$orientationId]['generatedimagenotext']);
			if(!empty($custarray['generatedimagenotextbg']))
			$baseBgImage = imagecreatefromjpeg($custarray['generatedimagenotextbg']);
		}
	}

	
	$fonttypename = $fonttype;
	if((($pos=strpos($fontcolor, "#")) !== false))
	{
		$fontcolor = $fontcolor;
		$textfontcolor = $fontcolor;
	}
	else
	{
		$fontcolor = $fontcolor;
		$textfontcolor = dechex($fontcolor);
	}
	$fontcolor = html2rgb($fontcolor);
	list($rgb, $r, $g, $b) = split('[(,)]', $fontcolor);

	//$fontcolor = imagecolorallocate($baseImage, $r, $g, $b); //TODO change the color to the recieved color
	$fontcolor = hexdec(str_pad(dechex($r), 2, 0, STR_PAD_LEFT).str_pad(dechex($g), 2, 0, STR_PAD_LEFT).str_pad(dechex($b), 2, 0, STR_PAD_LEFT));
	if (strpos($fonttype,"ttf")) $isfonttypepathset = true;

	if (!$isfonttypepathset)
	$fonttype =  $xcart_dir.'/fonts/'.$mk_fonts[$fonttype];

	// Add the text on the image
	// get the width of the text to be pasted.
	if (empty($custarray[$orientationId]['customizedtext'])){
		$customizedText = array();
		$customizedText['fonttype'] = $fonttype;
		$customizedText['fontsize'] = $fontsize;
		$customizedText['fonttext'] = $fonttext;
		$customizedText['fontcolor'] = $fontcolor;
		$customizedText['textfontcolor'] = $textfontcolor;
		$customizedText['fonttypename'] = $fonttypename;
		$customizedText['textX'] = 0;
		$customizedText['textY'] = 0;
	}else{
		$customizedText = $custarray[$orientationId]['customizedtext'];
	}
	
	//put the text on the customized area image
	imageTextWrapped($textImage, $customizedText['textX'] + $textdx, $customizedText['textY'] + $textdy, $custarray[$orientationId]['w'], $fonttype, $fontcolor, $fonttext, $fontsize,'c');
	

	// store the text attributes in customized session array
	$customizedText['fonttype'] = $fonttype;
	$customizedText['fontsize'] = $fontsize;
	$customizedText['fonttext'] = $fonttext;
	$customizedText['fonttypename'] = $fonttypename;
	$customizedText['fontcolor'] = $fontcolor;
	$customizedText['textfontcolor'] = $textfontcolor;
	$customizedText['textX'] += $textdx;
	$customizedText['textY'] += $textdy;
	
	//fit the customized area image back on the base product image
	paste_customized_area_image($custarray[$orientationId],$baseImage,$textImage);
	
	$len = 4;
	if ($isJPG){
		$tempCustTextFile = "./images/customized/custtext".time().get_rand_id($len).".jpg";
		if(!imagejpeg($baseImage,$tempCustTextFile))
			$weblog->info("unable to create $tempCustTextFile in mktextcustomizationapis.php");
		## For customize bg image
		if(!empty($baseBgImage)){
			$tempBgCustTextFile = "./images/customized/bgimages/custtext".time().get_rand_id($len).".jpg";
			if(!imagejpeg($baseBgImage,$tempBgCustTextFile))
			   $weblog->info("unable to create $tempBgCustTextFile in mktextcustomizationapis.php");
		}
	}else{
		$tempCustTextFile = "./images/customized/custtext".time().get_rand_id($len).".png";
		if(!imagepng($baseImage,$tempCustTextFile))
			$weblog->info("unable to create $tempCustTextFile in mktextcustomizationapis.php");
		## For customize bg image
		if(!empty($baseBgImage)){
			$tempBgCustTextFile = "./images/customized/bgimages/custtext".time().get_rand_id($len).".png";
			if(!imagepng($baseBgImage,$tempBgCustTextFile))
			   $weblog->info("unable to create $tempBgCustTextFile in mktextcustomizationapis.php");
		}
	}

	addToTempImagesList( $tempCustTextFile);
	if(!empty($tempBgCustTextFile)){
		addToTempImagesList($tempBgCustTextFile);
	}
	$custarray[$orientationId]['customizedtext'] = $customizedText;

	$custarray[$orientationId]['generatedimage'] = $tempCustTextFile;

	if(!empty($tempBgCustTextFile)){
		$custarray['generatedbgimage'] = $tempBgCustTextFile;
	}
	$custarray['common_generated_final_image'] = $tempCustTextFile;
	$common_txt_image = $tempCustTextFile;
	return  $tempCustTextFile;

}

function paste_customized_area_image($custarray,&$baseimage,$caimage){
	imagecopyresampled($baseimage,$caimage,$custarray['x'],$custarray['y'],0,0,$custarray['w'],$custarray['h'],$custarray['w'],$custarray['h']);
}
function paste_customized_bg_image($orientationDetail,&$bgTxtImage,$caimage){
	if(!empty($bgTxtImage)){
		imagecopyresampled($bgTxtImage,$caimage,$orientationDetail['bgstart_x'],$orientationDetail['bgstart_y'],0,0,$orientationDetail['bgsize_x'],$orientationDetail['bgsize_y'],$orientationDetail['bgsize_x'],$orientationDetail['bgsize_y']);
	}

}

?>
