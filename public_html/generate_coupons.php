<?php

include_once("./auth.php");
include_once("$xcart_dir/modules/coupon/database/CouponAdapter.php");

$couponAdapter = CouponAdapter::getInstance();


// 250
$startDate = strtotime('01 April 2011');
$endDate = strtotime('31 December 2011');
$groupName = 'CSAT';
$type = 'absolute';
$amount = '250.00';
$percent = '';
$minimum = 500.00;
$noOfVouchers = 700;

if (!$couponAdapter->existsCouponGroup($groupName)) {
    $couponAdapter->createCouponGroup(
        $groupName, 'online', time(), 'myntraprovider');
}

for ($i = 0; $i < $noOfVouchers; $i++) {
    $couponAdapter->generateSingleCouponForUser('CSAT250', 5, $groupName, $startDate, $endDate, '', $type, $amount, $percent, $minimum);
}

echo "created CSAT250\n";


// 500
$startDate = strtotime('01 April 2011');
$endDate = strtotime('31 December 2011');
$groupName = 'CSAT';
$type = 'absolute';
$amount = '500.00';
$percent = '';
$minimum = 1000.00;
$noOfVouchers = 460;

for ($i = 0; $i < $noOfVouchers; $i++) {
    $couponAdapter->generateSingleCouponForUser('CSAT500', 5, $groupName, $startDate, $endDate, '', $type, $amount, $percent, $minimum);
}

echo "created CSAT500\n";


// 1000
$startDate = strtotime('01 April 2011');
$endDate = strtotime('31 December 2011');
$groupName = 'CSAT';
$type = 'absolute';
$amount = '1000.00';
$percent = '';
$minimum = 2000.00;
$noOfVouchers = 170;

for ($i = 0; $i < $noOfVouchers; $i++) {
    $couponAdapter->generateSingleCouponForUser('CSAT1000', 4, $groupName, $startDate, $endDate, '', $type, $amount, $percent, $minimum);
}

echo "created CSAT1000\n";


// 2500
$startDate = strtotime('01 April 2011');
$endDate = strtotime('31 December 2011');
$groupName = 'CSAT';
$type = 'absolute';
$amount = '2500.00';
$percent = '';
$minimum = 5000.00;
$noOfVouchers = 20;

for ($i = 0; $i < $noOfVouchers; $i++) {
    $couponAdapter->generateSingleCouponForUser('CSAT2500', 4, $groupName, $startDate, $endDate, '', $type, $amount, $percent, $minimum);
}

echo "created CSAT2500\n";

?>
