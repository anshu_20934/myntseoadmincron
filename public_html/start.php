<?php

define('XCART_START', 1);
define ('DIR_CUSTOMER', '');
$xcart_dir = __DIR__;

//since SSL termination is happening at LB need to check this
//nginx will set this variable while proxy forwarding to haproxy
if ($_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
	$_SERVER['HTTPS'] = 'on';
}

// configurations
$use_sessions_type = 3;
$XCART_SESSION_NAME = "xid";
$current_area = "C";
require_once $xcart_dir."/env.php";

// is this XHR request
$isXHR = isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest';

// class auto loader and shutdown hook
require_once $xcart_dir."/include/class/MClassLoader.php";
MClassLoader::addToClassPath($xcart_dir."/include/class/");
MClassLoader::addToClassPath($xcart_dir."/modules/");
MClassLoader::addToClassPath($xcart_dir."/platform/");
require_once $xcart_dir."/include/class/ShutdownHook.php";


require_once $xcart_dir."/include/class/cache/XCache.php";
require_once $xcart_dir."/Cache/Cache.php";
$xcache = new XCache();

require_once $xcart_dir."/include/func/func.utils.php";
require_once $xcart_dir."/include/func/func.core.php";
require_once $xcart_dir."/include/func/func.db.php";
require_once $xcart_dir."/databaseinit.php";
require_once $xcart_dir."/include/class/widget/class.widget.keyvaluepair.php";
require_once $xcart_dir."/include/class/class.feature_gate.keyvaluepair.php";

require_once $xcart_dir."/include/sessions.php";
require_once $xcart_dir."/AnalyticsBase.php";


/**
 * Assigning token to users.
 * To prevent CSRF attacks
 * One copy will be stored in session and another copy will be sent along with all html forms in hidden field _token.
 * Session USER_TOKEN value will be compared against the token value coming from form before saving any changes through form
 */
if (!defined(USER_TOKEN)) {
	if (empty($XCART_SESSION_VARS['USER_TOKEN'])) {
		$user_token_characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWZYZ";
		$user_token_string;
		for ($p = 0; $p < 10; $p++) {
			$user_token_string .= $user_token_characters[mt_rand(0, strlen($user_token_characters))];
		}
		$user_token = md5($user_token_string);
    }
    else {
		$user_token = $XCART_SESSION_VARS['USER_TOKEN'];
	}

	define(USER_TOKEN, $user_token);
	if (!x_session_is_registered('USER_TOKEN')){
		x_session_register("USER_TOKEN", $user_token);
	}
	$XCART_SESSION_VARS['USER_TOKEN'] = $user_token;
	unset($user_token);
	unset($user_token_string);
	unset($user_token_characters);
}

use abtest\MABTest;
$abTests = array();
$abTests['homeRecentlyViewed'] = MABTest::getInstance()->getUserSegment('homeRecentlyViewed');
$abTests['PincodeWidget'] = MABTest::getInstance()->getUserSegment('PincodeWidget');
$abTests['checkout'] = MABTest::getInstance()->getUserSegment('checkout');

$skin = "skin2";
$filterChain = new web\filters\impl\FilterChain();
$filterChain->addfilter(web\filters\impl\MobileFilter::getInstance());
$filterChain->doFilter();

require_once $xcart_dir."/smarty_new.php";
$smarty->assign('isXHR', $isXHR);
$smarty->assign('USER_TOKEN', $XCART_SESSION_VARS['USER_TOKEN']);
$smarty->assign("rupeesymbol","Rs. ");
$smarty->assign("http_location", "http://".HostConfig::$httpHost);
$smarty->assign("https_location", "https://".HostConfig::$httpsHost);
$smarty->assign("cdn_base", HostConfig::$cdnBase);
$smarty->assign("secure_cdn_base", HostConfig::$secureCdnBase);

require_once $xcart_dir."/include/check_useraccount.php";
require_once $xcart_dir."/include/cssjs.min.php";

$newrelicFilteredAgents = array("KTXN");
$filter = false;
foreach ($newrelicFilteredAgents as $agent) {
    if (strpos($_SERVER["HTTP_USER_AGENT"], $agent) !== FALSE){
        $filter = true;
        break;
    }
}
if (extension_loaded('newrelic') && $filter != true) {
	$newrelic_header = newrelic_get_browser_timing_header();
	$newrelic_footer = newrelic_get_browser_timing_footer();
}
if (extension_loaded('oboe') && $filter != true) {
    $tracelytics_header = oboe_get_rum_header();
    $tracelytics_footer = oboe_get_rum_footer();
}

/*
header('Content-Type: text/plain');
$incFiles = get_included_files();
echo "login: "; var_dump($login);
echo "A/B Tests: "; print_r($abTests);
echo "Session: "; print_r($XCART_SESSION_VARS);
echo "Included Files: "; print_r($incFiles);
*/
