<?php
use order\exception\CartTamperedException;
use order\exception\EmptyCartException;
use order\exception\NoSuitableGatewayException;
use order\exception\OrderCreationFailedException;
use order\exception\UserNotLoggedInException;
use enums\cart\CartContext;
use giftcard\manager\MCartGiftCard;
use giftcard\manager\MCartGiftCardItem;
use order\dataobject\builder\OrderDOBuilder;

use giftcard\manager\GiftCardOrderManager;

//Kundan: We no longer use mkorderinsert_re.php: all of that goes inside here, and nested classes
//include_once("$xcart_dir/mkorderinsert_re.php");

if(file_exists("./auth.php")) {
	include_once("./auth.php");
} else if (file_exists("../auth.php")) {
	include_once("../auth.php"); // happens for secure pages
} 
include_once "$xcart_dir/include/class/mcart/class.MCart.php";
include_once "$xcart_dir/include/class/mcart/class.MCartUtils.php";

//Get the cart for which Order needs to be created
$mcartFactory = new \MCartFactory();
$cart = $mcartFactory->getCurrentUserCart(true, CartContext::GiftCardContext);
//NOTE: Removed Logging to order.log

//var_dump($cart);exit;

//Create Order Data Object Builder from which Order DO and all child DOs get created 
//$orderDOBuilder = new GiftCardOrderDOBuilder();

$orderDOBuilder = new OrderDOBuilder();

global $http_location, $https_location, $weblog, $errlog, $XCART_SESSION_VARS;
global $errorlog;
//Build OrderDO from the cart
try {
	if($cart == null) {
		throw new EmptyCartException("Cart Empty while checkout: may be due to concurrent activity");
	}
	$orderDO = $orderDOBuilder->build($cart);
} catch (UserNotLoggedInException $exc) {
	redirectAndCloseOverlayPaymentWindow($http_location);
} catch (EmptyCartException $exc) {
	redirectAndCloseOverlayPaymentWindow("$http_location/mkmygiftcardcart.php");
} catch (Exception $e) {
	$weblog->info("Redirecting back to the payment page. Reason: Exception Occured:".$e);
	redirectAndCloseOverlayPaymentWindow($https_location . "/mkgiftcardpaymentoptions.php?transaction_status=N");
}

//Get an instance of OrderManager, which will do the top level business transaction
$orderManager = new GiftCardOrderManager($orderDO);

//Stage-1: Create order by doing checkout
try {
	$orderManager->checkout();
} catch (UserNotLoggedInException $unli) {
	//User's session somehow lost
	$errorlog->error("Redirecting back to home page.Reason: ".$unli);
	$weblog->error("Redirecting back to home page.Reason: ".$unli);
	redirectAndCloseOverlayPaymentWindow($http_location);
} catch (EmptyCartException $ece) {
	//stale cart , cart is made empty from another tab/window - cannot proceed. Rebuild the cart again
	$weblog->info("Redirecting back to home page.Reason: ".$unli);
	redirectAndCloseOverlayPaymentWindow("$http_location/mkmygiftcardcart.php");
} catch (CartTamperedException $cte) {
	//Cart was possibly tampered. Rebuild the cart again
	$errorlog->error("Redirecting to cart page: Reason: cart was possibly tampered: ".$cte);
	$weblog->error("Redirecting to cart page: Reason: cart was possibly tampered: ".$cte);
	redirectAndCloseOverlayPaymentWindow("$http_location/mkmygiftcardcart.php");
} catch (NoShippingAddressException $exc) {
	//redirectAndCloseOverlayPaymentWindow($https_location . "/mkcustomeraddress.php?gc=true");
} catch(CouponException $cpexc) {
	//NOTE: Removed x_session_register: redirectedToCart:  
	$weblog->info("Redirecting back to the payment page. Reason: ".$cpexc->getMessage());
	redirectAndCloseOverlayPaymentWindow($https_location . "/mkgiftcardpaymentoptions.php");
} catch(OrderCreationFailedException $ordex) {
	$errorlog->error("Order creation failed: ".$ordex->getDO());
	$weblog->error("Order creation failed: ".$ordex->getDO());
	$XCART_SESSION_VARS['errormessage'] = "Your Transaction failed. We request you to kindly re-try the transaction.";
	redirectAndCloseOverlayPaymentWindow($https_location . "/mkgiftcardpaymentoptions.php?transaction_status=N");
} catch (NoSuitableGatewayException $e) {
	global $weblog;
	$weblog->info("Redirecting back to the payment page. Reason: No suitable gateway found for calculating Payment gateway Discount. payment dataobject:".$e->toString());
	$paymentDO = $orderManager->orderDO->getPaymentDO();
	$emi_bank = $paymentDO->getEMIBank();
	if(!empty($emi_bank)) {
		redirectAndCloseOverlayPaymentWindow($https_location . "/mkgiftcardpaymentoptions.php?transaction_status=EMI");
	} else {
		redirectAndCloseOverlayPaymentWindow($https_location . "/mkgiftcardpaymentoptions.php?transaction_status=N");
	}
} catch (Exception $e) {
	$weblog->info("Redirecting back to the payment page. Reason: Exception Occured:" . $e);
	redirectAndCloseOverlayPaymentWindow($https_location . "/mkgiftcardpaymentoptions.php?transaction_status=N");
}

//Stage-2: Attempt to pay for this order
try {
	$form = $orderManager->getPaymentGatewayForm();
	//add a parameter here for the gifting order to be placed
} catch (NoSuitableGatewayException $e) {
	global $weblog;
	$weblog->info("Redirecting back to the payment page. Reason: No suitable gateway found for payment dataobject:".$e->toString());
	redirectAndCloseOverlayPaymentWindow($https_location . "/mkgiftcardpaymentoptions.php?transaction_status=N");
	//func_header_location($https_location . "/mkgiftcardpaymentoptions.php?transaction_status=N",false);
} catch (Exception $e) {
	$weblog->info("Redirecting back to the payment page. Reason: Exception Occured:".$e);
	redirectAndCloseOverlayPaymentWindow($https_location . "/mkgiftcardpaymentoptions.php?transaction_status=N");
}
?>

<body onload="javascript: submitForm()">

<?php echo $form; 
?>

	<script type="text/javascript">
	 function submitForm(){
	     document.subFrm.submit(); 
	 }
	 </script>
</body>
