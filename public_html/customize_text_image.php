<?php
include_once($xcart_dir."/include/func/func.mkimage.php");
include_once($xcart_dir."/mkcustomized_image.php");
ini_set("max_execution_time", 360000);
include_once("$xcart_dir/include/class/mcart/class.MCart.php");
//ini_set('display_errors',1);

/********************************************************Function for createDesigns ****************************************/
function createDesign($product_type_id,$product_style_id,$title,$template_name,$tags,$template_image,$font_size,$font_type,$font_color,$start_x,$start_y,$width,$height,$angle,$max_character_length){

	// Create a 300x100 image
	$im    = imagecreatetruecolor($width,$height);
	$white = imagecolorallocate($im, 255, 255, 255);
	$black = imagecolorallocate($im, 0, 0, 0);

	// Make the background red
	imagefilledrectangle($im, 0, 0, $width, $height, $white);

	// Draw the text 'PHP Manual' using font size 13
	$tb = imagettfbbox($font_size, $angle, $font_type, $title);

	/*
	note added by chander
	//$td is an array that can be used for alignments
	Array
	(
	[0] => 0 // lower left X coordinate
	[1] => -1 // lower left Y coordinate
	[2] => 198 // lower right X coordinate
	[3] => -1 // lower right Y coordinate
	[4] => 198 // upper right X coordinate
	[5] => -20 // upper right Y coordinate
	[6] => 0 // upper left X coordinate
	[7] => -20 // upper left Y coordinate
	)
	*/

	$x = ceil(($width - $tb[2]) / 2); // lower left X coordinate for text
	$y = ceil(($height -($tb[1]+$tb[7]))/2 ) ;
	imagettftext($im, $font_size, $angle, $x,$y , $black, $font_type, $title);

	$temp_txt_path =  $_SERVER['DOCUMENT_ROOT']."/images/tmp/".time().get_rand_id($len).".jpg";
	imagejpeg($im,$temp_txt_path);
	$print_img = $template_image;
	$new_x =$start_x;
	$new_y = $start_y;
	$IMG_WIDTH = $width;
	$IMG_HEIGHT = $height;
	$CUST_WIDTH = $start_x;
	$CUST_HEIGHT = $start_y;
	$image_url = $temp_txt_path;
	$nameofthedesign = $template_name."-".$title ;

	$generatedImage = generateCustomizedImage($image_url,$print_img,$new_x,$new_y,$IMG_WIDTH,$IMG_HEIGHT,$CUST_WIDTH,$CUST_HEIGHT);
	//unlink($temp_txt_path);

	/***product publish****/
	$product_id = PublishDesign($nameofthedesign,$tags,$product_type_id,$product_style_id,$generatedImage,$title);

	if(!empty($product_id)){
		return $product_id;
	}else{
		return 0;
	}
}
/*********************************************************End of Function createDesigns ***********************************/

function PublishDesign($nameofthedesign,$tags,$product_type_id,$product_style_id,$imgname,$text){
	//$login = 'arun.kumar@myntra.com';
	//$address = "bangalore";
	//db_query("insert into mk_designer (customerid,payment_address, agreed_t_c) values ('$login','$address','1')");
	$productId=MCartUtils::genProductId();
	$productCode = "MK".$productId;
	$designName = $nameofthedesign;
	$provider = $login;
	$distribution = "";
	$fulldescription = $nameofthedesign;
	$status = 1011;
	$filterKeywords =$tags;
	$isCustomizable = 0;
	$productTypeId =$product_type_id;
	$productStyleId =$product_style_id;
	$expirationDate ="";
	$designer ="myntra designs" ;
	$contestId =0;
	$adddate = time();
	$results = db_query("select price from mk_product_style where id='".$product_style_id."' and product_type='".$product_type_id."' ");
	$row=mysql_fetch_array($results);
	$listPrice = $row['price'];
	$sourceImagePath = "./images/customized/customized_design_images/L/finalcustomizeddesign".$imgname;
	$targetImagePath = "./images/customized/customized_design_images/T/finalcustomizeddesign".$imgname;
	$resizeWidth = 160;
	$resizeHeight = 160;
	resizeImageToDimension($_SERVER['DOCUMENT_ROOT'].'/'.$sourceImagePath,$_SERVER['DOCUMENT_ROOT'].'/'.$targetImagePath,$resizeWidth,$resizeHeight);
	$portal_imagePath = $targetImagePath;

	db_query("insert into xcart_products (productid,productcode,product,provider,distribution,list_price,fulldescr,keywords,designer,is_customizable,expiration_date,statusid,product_type_id,product_style_id,contestid, add_date,image_portal_t, is_publish,markup) values ('$productId','$productCode','$designName','$provider','$distribution','$listPrice','$fulldescription','$filterKeywords','$designer','$isCustomizable','$expirationDate','$status','$productTypeId','$productStyleId','$contestId', $adddate,'$portal_imagePath', 0,'$markup')");

	$image ="" ;
	$imagePath = $sourceImagePath ;
	$imageSize = filesize($imagePath);
	$fileName = $imagePath;
	$alt = "";
	$md5 = md5($imagePath);
	$time = time();

	db_query("insert into  xcart_images_T (id,image,image_path,image_size,filename,alt,md5,date) values ('$productId','$image','$imagePath','$imageSize','$fileName','$alt','$md5',$time)");

	$results = db_query("select id from mk_style_customization_area where style_id='".$productStyleId."' and isprimary='1' ");
	$row=db_fetch_array($results);
	$area_id = $row['id'];
	$resizeWidth = 800;
	$resizeHeight = 800;
	$targetImagePath = "./images/customized/custtemp".$imgname;
	resizeImageToDimension($_SERVER['DOCUMENT_ROOT'].'/'.$sourceImagePath,$_SERVER['DOCUMENT_ROOT'].'/'.$targetImagePath,$resizeWidth,$resizeHeight);
	$area_image_final =$targetImagePath;
	$templateid = 0;
	//insert into database
	db_query("insert into  mk_xcart_product_customized_area_image_rel (product_id,style_id,area_id,area_image_final,templateid) values ('$productId','$productStyleId','$area_id','$area_image_final','$templateid')");

	$results = db_query("select id from mk_customization_orientation where area_id='".$area_id."'  ");
	$row=db_fetch_array($results);
	$orientation_id = $row['id'];
	$image_name = $sourceImagePath;
	$image_x=0;
	$image_y = 0;
	$image_width =0;
	$image_height = 0;
	//$text =0;
	$portalPath = $sourceImagePath;
	$newImageWithTime = get_rand_id(4).stristr($portalPath, 'f');
	$targetImagePath ="./images/popup_image/pi_$newImageWithTime";
	$resizeWidth = 300;
	$resizeHeight = 300;
	resizeImageToDimension($_SERVER['DOCUMENT_ROOT'].'/'.$sourceImagePath,$_SERVER['DOCUMENT_ROOT'].'/'.$targetImagePath,$resizeWidth,$resizeHeight);
	$pop_up_image_path = $targetImagePath ;

	db_query("insert into  mk_xcart_product_customized_area_rel (product_id,style_id,area_id,orientation_id,image_name,image_x,image_y,image_width,image_height,text,popup_image_path,thumb_image,orientation_final_image_display,orientation_final_image_print) values ('$productId','$productStyleId','$area_id','$orientation_id','$image_name','$image_x','$image_y','$image_width','$image_height','$text','$pop_up_image_path','$image_name','$image_name','$image_name')");

	return $productId;
}

?>