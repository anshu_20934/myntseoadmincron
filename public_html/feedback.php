<?php
require "./auth.php";
include_once(\HostConfig::$documentRoot."/modules/coupon/database/CouponAdapter.php");
include_once(\HostConfig::$documentRoot."/include/class/class.mail.multiprovidermail.php");
include_once(\HostConfig::$documentRoot."/modules/serviceRequest/ServiceRequest.php");
include_once(\HostConfig::$documentRoot."/include/func/func.order.php");

use feedback\instance\MFBInstance;
use feedback\instance\MFBOrderInstance;
use feedback\manage\MFB;

//domain name for links in feedback survey mail
$httpLocation = "http://www.myntra.com/";

//set the url to which response to be redirected
$formURL = $httpLocation."feedback.php";

//get the feedback instance
$MFBInstanceObj = new MFBInstance();

//params get or post
$MFBInstanceId = ($_POST['MFBInstanceId']) ? $_POST['MFBInstanceId'] : $_GET['MFBInstanceId'];
$MFBInstanceAuthKey = ($_POST['MFBInstanceAuthKey']) ? $_POST['MFBInstanceAuthKey'] : $_GET['MFBInstanceAuthKey'];
// NPS option value
$MFB_NPS = ($_POST['MFB_NPS']) ? $_POST['MFB_NPS'] : $_GET['MFB_NPS'];

//show MFB form pre-populated(handle redirection from email)
if(empty($_POST['MFB_response'])){
    
    //validate post parameters for instance and key
    if(!empty($MFBInstanceId) && !empty($MFBInstanceAuthKey)){

        //validate of auth key against instance id
        if($MFBInstanceObj->isValidMFB($MFBInstanceId, $MFBInstanceAuthKey) && !$MFBInstanceObj->isNpsCaptured($MFBInstanceId)){

            //get instance detail
            $MFBInstance = $MFBInstanceObj->getMFBInstances(array("instanceid"=>$MFBInstanceId));
            
            //get MFB object
            $MFB = new MFB();
            $MFBInfo = $MFB->getMFB(array("feedbackid"=>$MFBInstance[0]['feedback_id']));
            
            //wrap MFB HTML questionnaire with <form>
            //and hidden fields required to send with order FB mail
            $formHead = $MFBInfo[0]['title'];
            
            $submitButtonProperties = array("value"=>"Submit Feedback","type"=>"button");
            $selectedQuestionOptionArray = $_POST['MFB'];
            
            if(empty($MFB_NPS)){
            	
				$MFBActiveQuestionnaire = $MFB->prePopulateMFBQuestionnaire ( $MFBInstance [0] ['feedback_id'], $selectedQuestionOptionArray );
				$MFB_HTML = $MFBInstanceObj->wrapMFBHTMLQuestionnaire ( $MFBActiveQuestionnaire ['questionnaireHTML'], $formURL, $MFBInstance [0] ['instanceid'], $MFBInstance [0] ['authentic_key'], $formHead, $setResponse = 1, $submitButtonProperties );
				// show user confirmation message
				// $userConfirmMsg = $formHead;
								
            //NPS handling
            } else {
					
				// capture NPS MFB instance data(feedback response) into DB
				$isInstanceDataAdded = $MFBInstanceObj->addMFBInstanceDataForNps ( $MFBInstance [0] ['instanceid'], $MFB_NPS );
				
				// capture recevied date of NPS
				if ($isInstanceDataAdded) {
					// update MFB instance data with received date so that to restrict
					// further submission(page refresh) of feedback
					$MFBInstanceObj->setMFBInstance ( $MFBInstance [0] ['instanceid'] );
				}
				
				$MFBActiveQuestionnaire = $MFB->prePopulateMFBQuestionnaire ( $MFBInstance [0] ['feedback_id'], $selectedQuestionOptionArray, true );
				$MFB_HTML = $MFBInstanceObj->wrapMFBHTMLQuestionnaire ( $MFBActiveQuestionnaire ['questionnaireHTML'], $formURL, $MFBInstance [0] ['instanceid'], $MFBInstance [0] ['authentic_key'], $formHead, $setResponse = 1, $submitButtonProperties, true );
				// show user confirmation message
				//$userConfirmMsg = 'Thank you for rating us. If you have a moment we\'d welcome some feedback.<br/><br/>' . 'If you are <b>' . $MFBInstance [0] ['customer_email'] . '</b>., and if you have completed all ' . 'mandatory questions in your feedback,' . '<br/>please click<br/><input type="button" name="MFB_trigger_submit" id="MFB_trigger_submit" value="Submit Feedback" class="btn primary-btn"/>';
				$userConfirmMsg = 'Thank you for rating us. If you have a moment we\'d welcome some feedback.<br/>';
            }            
                        
            $smarty->assign("msg",$userConfirmMsg);

            //show pre-populated form
            $smarty->assign("MFB_HTML",$MFB_HTML);

            //initialize client side MFB obj
            $smarty->assign("MFB_JSON",$MFBActiveQuestionnaire['questionnaireJSON']);

        } else {
        	$msg = "We apologize to you that this feedback form is not active any more. ". 
        			"We appreciate your interest in helping us improve our service.<br/>".
        					"Thank You for shopping with us.";         
            $smarty->assign("error_msg",$msg);                        
        }
    } else {
        $msg="You have already submitted your feedback and your feedback is under process.";
        $smarty->assign("error_msg",$msg);
    }

}

//MFB response given by the user
else if(is_numeric($_POST['MFB_response']) && $_POST['MFB_response'] == 1){

    //validate post parameters for instance and key
    if(!empty($MFBInstanceId) && !empty($MFBInstanceAuthKey)){

        //validate of auth key against instance id
        if($MFBInstanceObj->isValidMFB($MFBInstanceId, $MFBInstanceAuthKey)){  

        	// we need to handle both scenarios: prior NPS and post NPS changes
        	
        	// NPS response
        	if(is_numeric($_POST['MFB_NPS_response']) && $_POST['MFB_NPS_response'] == 1){
        		
        		// because by this time we had already captured NPS question and received_date would not be null
        		if($MFBInstanceObj->isInstanceDataNotCaptured($MFBInstanceId)){
        			//get instance detail
        			$MFBInstance = $MFBInstanceObj->getMFBInstances(array("instanceid"=>$MFBInstanceId));
        			//get the MFB for question type, survey email etc..
        			$MFB = new MFB();
        			$MFBInfo = $MFB->getMFB(array("feedbackid"=>$MFBInstance[0]['feedback_id']));
        			 
        			//get all posted answers
        			$selectedQuestionOptionArray = $_POST['MFB'];
        			 
        			$MFBquesion = $MFB->validateMFBQuestionnaire($MFBInstance[0]['feedback_id'], $selectedQuestionOptionArray, true);
        			 
        			//if all mandatory questions are answered
        			if($MFBquesion === true){
        				 
        				//insert all MFB instance data(feedback response) into DB
        				// except NPS answer selected by the customer(as we would have captured it earlier)
        				$isInstanceDataAdded = $MFBInstanceObj->addMFBInstanceData($MFBInstance[0]['instanceid'], $selectedQuestionOptionArray, true);
        				        				
        				$msg="<div style='font-size:20px;font-weight:bold;'>Thank you for your feedback!</div>"
        						."<br/>We hope to incorporate most of your suggestions in order to provide a better"
        						." experience to all our customers.";
        				$smarty->assign("msg",$msg);
        				 
        				//if no mandatory question is answered
        			} else {
        				 
        				//to show the MFB pre-populated form
        				$MFBActiveQuestionnaire = $MFB->prePopulateMFBQuestionnaire($MFBInstance[0] ['feedback_id'], $selectedQuestionOptionArray, true );
        				// TODO can highlight a invalid question
        				 
        				// wrap MFB HTML questionnaire with <form>
        				// and hidden fields required to send with order FB mail
        				$formHead = $MFBInfo [0] ['title'];
        				 
        				$submitButtonProperties = array (
        						"value" => "Submit Feedback",
        						"type" => "button"
        				);
        				$MFB_HTML = $MFBInstanceObj->wrapMFBHTMLQuestionnaire ( $MFBActiveQuestionnaire ['questionnaireHTML'], $formURL, $MFBInstance [0] ['instanceid'], $MFBInstance [0] ['authentic_key'], $formHead, $setResponse = 1, $submitButtonProperties, true );
        				 
        				// show pre-populated form
        				$smarty->assign ( "MFB_HTML", $MFB_HTML );
        				 
        				// initialize client side MFB obj
        				$smarty->assign ( "MFB_JSON", $MFBActiveQuestionnaire ['questionnaireJSON'] );
        				 
        				// show error message
        				$msg = "Please answer all the mandatory fields (marked with an *) in order to submit your feedback. " . "We appreciate your effort towards improving our service.";
        				 
        				$smarty->assign("error_msg",$msg);
        				 
        			}
        		} else {
        			
        			$msg = "We apologize to you that this feedback form is not active any more. ". 
        			"We appreciate your interest in helping us improve our service.<br/>".
        					"Thank You for shopping with us.";
        			$smarty->assign("error_msg",$msg);
        		}
        		
        	// prior(NPS) MFB submission handling	
        	} else {
        		//get instance detail
        		$MFBInstance = $MFBInstanceObj->getMFBInstances(array("instanceid"=>$MFBInstanceId));
        		
        		if(empty($MFBInstance[0]['received_date'])){
        			//get the MFB for question type, survey email etc..
        			$MFB = new MFB();
        			$MFBInfo = $MFB->getMFB(array("feedbackid"=>$MFBInstance[0]['feedback_id']));
        			
        			//get all posted answers
        			$selectedQuestionOptionArray = $_POST['MFB'];
        			
        			$MFBquesion = $MFB->validateMFBQuestionnaire($MFBInstance[0]['feedback_id'], $selectedQuestionOptionArray);
        			
        			//if all mandatory questions are answered
        			if($MFBquesion === true){
        			
        				//insert all MFB instance data(feedback response) into DB        				
        				$isInstanceDataAdded = $MFBInstanceObj->addMFBInstanceData($MFBInstance[0]['instanceid'], $selectedQuestionOptionArray);
        				
        				if($isInstanceDataAdded){
        					//update MFB instance data with received date so that to restrict
        				 	//further submission(page refresh) of feedback
        				 	$MFBInstanceObj->setMFBInstance($MFBInstance[0]['instanceid']);
        				 }
        				 
        				$msg="<div style='font-size:20px;font-weight:bold;'>Thank you for your feedback!</div>"
        						."<br/>We hope to incorporate most of your suggestions in order to provide a better"
        						." experience to all our customers.";
        				$smarty->assign("msg",$msg);
        			
        				//if no mandatory question is answered
        			} else {
        			
        				//to show the MFB pre-populated form
        				$MFBActiveQuestionnaire = $MFB->prePopulateMFBQuestionnaire($MFBInstance[0] ['feedback_id'], $selectedQuestionOptionArray );
        				// TODO can highlight a invalid question
        					
        				// wrap MFB HTML questionnaire with <form>
        				// and hidden fields required to send with order FB mail
        				$formHead = $MFBInfo [0] ['title'];
        					
        				$submitButtonProperties = array (
        						"value" => "Submit Feedback",
        						"type" => "button"
        				);
        				$MFB_HTML = $MFBInstanceObj->wrapMFBHTMLQuestionnaire ( $MFBActiveQuestionnaire ['questionnaireHTML'], $formURL, $MFBInstance [0] ['instanceid'], $MFBInstance [0] ['authentic_key'], $formHead, $setResponse = 1, $submitButtonProperties );
        					
        				// show pre-populated form
        				$smarty->assign ( "MFB_HTML", $MFB_HTML );
        					
        				// initialize client side MFB obj
        				$smarty->assign ( "MFB_JSON", $MFBActiveQuestionnaire ['questionnaireJSON'] );
        					
        				// show error message
        				$msg = "Please answer all the mandatory fields (marked with an *) in order to submit your feedback. " . "We appreciate your effort towards improving our service.";
        			
        				$smarty->assign("error_msg",$msg);
        			
        			}
        				
        		} else {
        			$msg = "We apologize to you that this feedback form is not active any more. ". 
        			"We appreciate your interest in helping us improve our service.<br/>".
        					"Thank You for shopping with us.";
        			$smarty->assign("error_msg",$msg);
        		}        		
        	}

        //if feedback given already for this reference type
        } else {
            $msg = "We apologize to you that this feedback form is not active any more. ". 
        			"We appreciate your interest in helping us improve our service.<br/>".
        					"Thank You for shopping with us.";
            $smarty->assign("error_msg",$msg);
        }

    //if feedback given already for this reference type
    } else {
        $msg="You have already submitted your feedback and your feedback is under process.";
        $smarty->assign("error_msg",$msg);
    }
    
}
//direct url hit message(show message for un-authenticated access)
else{

    $msg="We are sorry to tell you that this is an un-authenticated request and is not being processed.";
    $smarty->assign("error_msg",$msg);  
}

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setStaticPageVars(AnalyticsBase::ORDER_FEEDBACK);
$analyticsObj->setTemplateVars($smarty);

if($skin === "skin2") {
    func_display("feedback.tpl",$smarty);
} else {
    func_display("customer/mkfeedback.tpl",$smarty);
}
?>