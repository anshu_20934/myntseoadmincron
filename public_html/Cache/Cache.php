<?php
/**
 *
 */
include_once($xcart_dir."/Profiler/Profiler.php");
include_once($xcart_dir."/env/Cache.config.php");
include_once($xcart_dir."/Cache/Redis.php");

class Cache extends Redis {
	private $group;
	private $cachedKeyValues;
        private static $latency = array();

	public function __construct($group, $keys) {
		global $weblog,$errorlog;
		try {
			Cache::init();
			$this->group=$group.":".self::$cacheVersion;
			if(self::$cachingEnabled && !empty(self::$redisRO)) {
				$cachedKeys = array();
                                foreach ($keys as $k) {
                                    $cachedKeys[] = $this->constructKey($k);
                                }
				if(!empty($cachedKeys)) {
                                        $handle = Profiler::startTiming("redis_mget");
					$cachedValues = self::$redisRO->mget($cachedKeys);
                                        Profiler::endTiming($handle);
				} else {
					$cachedValues = array();
				}
				$keyValues = array();
				foreach ($cachedKeys as $key=>$val) {
					$keyValues[$val] = $cachedValues[$key];
				}
				$this->cachedKeyValues = $keyValues;
			}

		} catch (Exception $e) {
			$weblog->error($e);
			$errorlog->error($e);
			Profiler::increment("redis_exception");
		}
	}

	private function constructKey($key) {
		$key = $this->group.":".$key;
		return $key;
	}
	
	/**
	 * Gets the value for a key from the redis Cache
	 * key => key of the cache
	 **/
	public function getValue($key) {
		$handle_get = Profiler::startTiming("redis_get");
		$key = $this->constructKey($key);
		// check if key is in cachedKeys
		$cachedVal = $this->cachedKeyValues[$key];
		if(!empty($cachedVal)){
			$val = unserialize($cachedVal);
                        Profiler::endTiming($handle_get);
		}
		return $val;

	}
	
	/**
	 *  Gets the value for the key and sets the value for the key if it doesnt exist, by calling the callback function
	 *  function -> the callback function that needs to be called in case of value not yet set
	 *  params -> array of parameters for the callback function if any
	 *  key -> key for which value needs to be obtained from cache
	 *  expiry -> Time To Live (TTL) in seconds in cache for the key
	 *  	      the time interval in milliseconds after which the cache for the key gets invalidated
	 **/
	public function get($function, $params, $key, $expiry=43200) {
		$handle_get_miss = Profiler::startTiming("redis_get_miss");
		$val = $this->getValue($key);
		if(empty($val)){
			// Compute value from user defined function
			$val = call_user_func_array($function, $params);
			$this->setVal($key,$val,$expiry);
			Profiler::endTiming($handle_get_miss);
		}
		return $val;
	}

	/**
	 * Sets the value for the cache on the given key
	 * key -> the key for which the value needs to be set
	 * value -> the value that needs to be set for the key
	 * expiry -> Time To Live (TTL) in seconds in cache for the key
	 *  	      the time interval in milliseconds after which the cache for the key gets invalidated
	 **/
	public function setVal($key, $value, $expiry=43200) {
		global $weblog, $errorlog;
		$key = $this->constructKey($key);
		$handle_set = Profiler::startTiming("redis_set");
		try {
			if(self::$cachingEnabled && self::connectedToRW()) {
				self::$redisRW->set($key, serialize($value));
				self::$redisRW->expire($key, $expiry);
			}
		} catch (Exception $e) {
			$weblog->error($e);
			$errorlog->error($e);
			Profiler::increment("redis_exception");
		}
                Profiler::endTiming($handle_set);
	}
	
	public function invalidate() {
            global $weblog, $errorlog;
            $handle = Profiler::startTiming("redis_invalidate");
		try {
			global $weblog;
			$weblog->info($this->group.":cache-invalidated");
			if(self::$cachingEnabled && self::connectedToRW() && !empty($this->cachedKeyValues)) {
				self::$redisRW->del(array_keys($this->cachedKeyValues));
			}
		} catch (Exception $e) {
			$weblog->error($e);
			$errorlog->error($e);
			Profiler::increment("redis_exception");
		}
		Profiler::endTiming($handle);
	}
        
        public static function trackLatency($start_time)
        {
            array_push(self::$latency, microtime(true) - $start_time);
        }

        public static function getLatencies()
        {
            return self::$latency;
        }
}

?>
