<?php
/**
 *
 */
class CacheQueue {
	private $group;
	private $redis;
	private $enabled;

	public function __construct($group) {
		global $weblog , $errorlog;
		try {
			$this->group=$group;
			$redisConfig = FeatureGateKeyValuePairs::getFeatureGateValueForKey("cache-queue-redis-config");
			$this->enabled = FeatureGateKeyValuePairs::getBoolean("cache-queue-enabled");
			if($this->enabled)
			{
				if(!empty($redisConfig)) {
					$hosts = json_decode($redisConfig, true);
					$redis = new Predis\Client($hosts);
				} else {
					$redis = new Predis\Client();
				}
				$this->redis = $redis;
			}
		} catch (Exception $e) {
			$weblog->error($e);
			$errorlog->error($e);
		}
	}

	
	public function rpush($key, $data) {
		global $weblog, $errorlog;
		if($this->enabled)
		{
			$key = $this->group.":".$key;
			try {
				if(!empty($this->redis)) {
					$len  = $this->redis->rpush($key, serialize($data));
					return $len;
				}
			} catch (Exception $e) {
				$weblog->error($e);
				$errorlog->error($e);
			}
		}
		return false;
	}
	
	public function lpop($key) {
		global $weblog, $errorlog;
		if($this->enabled)
		{
			$key = $this->group.":".$key;
			try {
				if(!empty($this->redis)) {
					$values  = $this->redis->lpop($key);
					return $values;
				}
			} catch (Exception $e) {
				$weblog->error($e);
				$errorlog->error($e);
			}
		}
		return false;
	}
	
	public function llen($key) {
		global $weblog, $errorlog;
		if($this->enabled)
		{
			$key = $this->group.":".$key;
			try {
				if(!empty($this->redis)) {
					$len  = $this->redis->llen($key);
					return $len;
				}
			} catch (Exception $e) {
				$weblog->error($e);
				$errorlog->error($e);
			}
		}
		return false;
	}
}
?>
