<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Redis
 *
 * @author sudhir
 */
include_once($xcart_dir."/Profiler/Profiler.php");
include_once($xcart_dir."/env/Cache.config.php");

class Redis {
    	
        private static $initialized = FALSE;
        protected static $redisRO;
        protected static $redisRW;
        protected static $seoRedisRO;
        protected static $seoRedisRW;
        protected static $cachingEnabled;
	protected static $cacheVersion;

        public static function init()
	{
		if(!self::$initialized)
		{
			self::$initialized = TRUE;
			self::$cacheVersion = CacheConfig::$version;
			self::$cachingEnabled = CacheConfig::$enabled;
			if(self::$cachingEnabled) {
                                $handle = Profiler::startTiming("redis_connect");
				self::$redisRO = Redis::getRedisConnection();
                                Profiler::endTiming($handle);
			}
		}
	}
        
        public static function connectedToRW()
        {
                if(self::$cachingEnabled)
                {
                    if(empty(self::$redisRW)) {
                        $handle = Profiler::startTiming("redis_connect-rw");
                        self::$redisRW = Redis::getRedisConnection(true);
                        Profiler::endTiming($handle);
                    }
                }
                return !empty(self::$redisRW);
        }
        
        public static function getRedisConnection($rw=false,$config=false)
	{
                $redisConfig = "";
                if($rw)
                {
                    $redisConfig = CacheConfig::$redisRW;
                }
                else
                {
                    $redisConfig = CacheConfig::$redisRO;
                }

                if($config) {
                    $redisConfig = $config;
                }
                
		if(!empty($redisConfig)) {
                        $hosts = json_decode($redisConfig, true);
                        $factory = null;
                        if(!empty(CacheConfig::$factory)) {
                            $factory = json_decode(CacheConfig::$factory, true);
                        }
                        $redis = new Predis\Client($hosts, $factory);
		} else {
			$redis = new Predis\Client();
		}
		global $weblog;
		$weblog->debug("Connected to redis $redisConfig");
		return $redis;
	}

    public static function getRedisRO(){
        return self::$redisRO;
    }
    public static function getRedisRW(){
        Redis::connectedToRW();
        return self::$redisRW;
    }
    
    public static function getFooterRedisRW() {
        return self::getRedisConnection(true,CacheConfig::$redisFooterRW);    
    }
    public static function getSeoRedisRO(){
        return self::getRedisConnection(true,CacheConfig::$seoRedisRO);
    }
    
    public static function getSeoRedisRW(){
        return self::getRedisConnection(true,CacheConfig::$seoRedisRW);
    }
}

?>
