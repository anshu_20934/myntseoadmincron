<?php
include_once($xcart_dir."/Profiler/Profiler.php");
include_once($xcart_dir."/Cache/Redis.php");
/**
 * TableCache
 *
 * @author sudhir
 */
class TableCache extends Redis {    
    private $dataFunction;
    private $rowFunction;
    private $dataParams;
    private $prefix;
    // this timeout is in seconds.
    private $lock_timeout = 300;
    private $bucket_size = 512;
    
    public static function getTableCache($tableName, $tableKey)
    {
        return new TableCache(
            function($tableName, $tableKey){
                $table = array();
                $data = func_query("select * from $tableName", true);
                foreach ($data as $row) {
                    $table[$row[$tableKey]] = $row;
                }
                return $table;
            }, 
            function($tableName, $tableKey, $key){
                $rs = func_query("select * from $tableName where $tableKey='$key'", true);
                if(count($rs) > 0){
                    return $rs[0];
                }
                return NULL;
            }, 
            array($tableName, $tableKey), 
            $tableName."_".$tableKey);
    }
    
    /**
     * 
     * @param type $dataSource Should be a function that returns entire table 
     * to be cached in the form of key value pairs
     */
    public function __construct($dataFunction, $rowFunction, $dataParams, $prefix) {
        TableCache::init();
        $this->dataFunction = $dataFunction;
        $this->dataParams = $dataParams;
        $this->rowFunction = $rowFunction;
        $this->prefix = $prefix;
    }
    
    private function getBucket($key)
    {
        $hash = crc32("$key");
        $bucket = abs($hash) % $this->bucket_size;
        return $this->prefix.":".$bucket;
    }
    
    private function populateCache()
    {
        $profilerHandle = Profiler::startTiming("table-cache-populate");
        if($this->obtainLock())
        {
            Profiler::increment("table-cache-populate-lock-success");
            $this->setPopulated(false);
            
            $table_data = call_user_func_array($this->dataFunction, $this->dataParams);
            foreach ($table_data as $key => $value) {
                self::$redisRW->hset($this->getBucket($key), $key, serialize($value));
            }
            $this->setPopulated(true);
            
            $this->releaseLock();
            Profiler::endTiming($profilerHandle);
            return true;
        }
        else
        {
            Profiler::increment("table-cache-populate-lock-fail");
            return false;
        }
    }
    
    private function tryGet($key) 
    {
        $val = self::$redisRO->hget($this->getBucket($key), $key);
        if(!empty ($val))
        {
            return unserialize($val);
        }
        return NULL;
    }

    // to be called after obtainLock is successful
    private function setPopulated($flag)
    {
        if($flag)
        {
            self::$redisRW->hset($this->prefix, "::populated", 1);
        }
        else
        {
            self::$redisRW->hdel($this->prefix, "::populated");
        }
    }

    private function isPopulated()
    {
        $populated = self::$redisRO->hget($this->prefix, "::populated");
        return $populated == 1;
    }
    
    private function fallback($key)
    {
        $profilerMissHandle = Profiler::startTiming("table-cache-get-fallback");
        $val = call_user_func_array($this->rowFunction, array_merge($this->dataParams, array($key)));
        Profiler::endTiming($profilerMissHandle);
        return $val;
    }

    public function refresh($key)
    {
        try
        {
            $value = $this->fallback($key);
            self::connectedToRW();
            if($value == null)
            {
                self::$redisRW->hdel($this->getBucket($key), $key);
            }
            else
            {
                self::$redisRW->hset($this->getBucket($key), $key, serialize($value));
            }
        }
        catch (Exception $ex)
        {
            global $weblog, $errorlog;
            $weblog->error($ex->getFile().":".$ex->getMessage());
            $errorlog->error($ex->getFile().":".$ex->getMessage());
            Profiler::increment("table-cache-exception");
        }
    }
    
    public function get($key) 
    {
        try
        {
            $profilerHandle = Profiler::startTiming("table-cache-get");
            $val = $this->tryGet($key);        
            if($val == NULL)
            {
                if(!$this->isPopulated())
                {
                    if($this->populateCache())
                    {
                        $val = $this->tryGet($key);
                    }
                    else
                    {
                        $val = $this->fallback($key);
                    }
                }
            }
            else
            {
                Profiler::increment("table-cache-hit");
            }
            Profiler::endTiming($profilerHandle);
        }
        catch (Exception $ex)
        {
            global $weblog,$errorlog;
            $weblog->error($ex->getFile().":".$ex->getMessage());
            $errorlog->error($ex->getFile().":".$ex->getMessage());
            Profiler::increment("table-cache-exception");
            $val = $this->fallback($key);
        }
        return $val;
    }

    // this is called only after ensuring that isPopulated() returns false
    private function obtainLock() 
    {
        $profilerHandle = Profiler::startTiming("table-cache-lock-obtain");
        $current_time = microtime(true);
        
        $lock_name = $this->getLockName();
        $aquired = 0;
        try
        {
            // check lock in local cache
            $last_lock_timestamp = self::$redisRO->get($lock_name);
            
            if($last_lock_timestamp == NULL || floatval($last_lock_timestamp) < $current_time)
            {
                self::connectedToRW();
                // try to set lock atomically
                $aquired = self::$redisRW->setnx($lock_name, $current_time + $this->lock_timeout);
                
                if($aquired == 0)
                {
                    // initial attempt to lock failed 
                    // lock could be expired one
                    $last_lock_timestamp = self::$redisRW->get($lock_name);
                    if($last_lock_timestamp == NULL || floatval($last_lock_timestamp) < $current_time)
                    {
                        $oldValue = self::$redisRW->getset($lock_name, $current_time + $this->lock_timeout);
                        $aquired = ($oldValue == $last_lock_timestamp);
                    }
                }
                if($aquired == 1)
                {
                    // put default expiry on lock in case we fail before releasing lock
                    self::$redisRW->expire($lock_name, $this->lock_timeout);
                }
            }
        }
        catch (Exception $ex)
        {
            global $weblog, $errorlog;
            $weblog->error($ex->getFile().":".$ex->getMessage());
            $errorlog->error($ex->getFile().":".$ex->getMessage());
            Profiler::increment("table-cache-exception");
        }
        Profiler::endTiming($profilerHandle);
        return ($aquired == 1);
    }
    
    private function getLockName() 
    {
        return $lock_name = "tableCache::lock::".$this->prefix;
    }
    
    private function releaseLock() 
    {    
        // release does not actually delete the lock. 
        // If we delete the lock as soon as cache is populated, 
        // then other threads which are in contention with populator thread 
        // would be able to get the lock and start populating the cache.
        self::$redisRW->expire($this->getLockName(), $this->lock_timeout);
    }
    
}
?>

