<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class LeaderBoardCacheKeys {
    public static $prefix = "leaderboard:";
    public static $trending_styles = "trending_styles";
    public static $top10Buyers = "top10Buyers";
    public static $top10MenBuyers = "top10MenBuyers ";
    public static $top10WomenBuyers= "top10WomenBuyers ";
    public static $meta = "meta";
    public static function keySet() {
        return array(self::$trending_styles, self::$top10Buyers, self::$top10MenBuyers, self::$top10WomenBuyers);
    }
}
class ComboCacheKeys{
    public static $prefix = "combo:";
    public static $allRelated = "related";
    public static function keySet() {
        return array(self::$allRelated);
    }
}

class CatalogueMetaCacheKeys {
    public static $prefix = "cat-meta:";
    public static $allBrands = "brands";
    public static $allGender = "gender";
    public static $allMasterCategory = "masterCategory";
    public static $allSubCategory = "subCategory";
    public static $allArticleTypes = "articleTypes";
    public static function keySet() {
        return array(self::$allBrands, self::$allGender, self::$allMasterCategory, self::$allSubCategory, self::$allArticleTypes);
    }
}

class PDPCacheKeys {
    public static $prefix = "pdp:";
    public static $styleObject = "styleObject";
    public static function keySet() {
        return array(self::$styleObject);
    }
}

class SpecialCouponCacheKeys {
    public static $prefix = "myntraFestCouponSlabs";
    public static $myntraFestSlabDetails = "myntraFestSlabDetails";
    public static function keySet() {
        return array(self::$myntraFestSlabDetails);
    }    
}

class CourierApiCacheKeys {
    public static $prefix = "LMSCouriers:";
    
    public static $courierDetails = "CourierDetails";
    public static $allCouriers = "AllCouriers";
    public static $activeCouriers = "ActiveCouriers";
    
    public static function keySet() {
        return array(self::$courierDetails, self::$allCouriers, self::$activeCouriers);
    }        
}

class LMSPincodeCacheKeys {
    public static $prefix = "LMS-Pincode:";
    public static $pincodeCourierMap = "PincodeCourierMap";
    public static function keySet() {
        return array(self::$pincodeCourierMap);
    }        
}

class TATCacheKeys {
    public static $prefix = "LMS-TAT";
    
    public static function getKey($whId, $zipcode, $paymentMethod, $courier,$serviceType='NORMAL') {
        return "tat-".($whId?$whId:"A")."-".$zipcode."-".($paymentMethod?$paymentMethod:"A").($courier?"-".$courier:"")."-".$serviceType;
    }        
    
    public static function keySet($whId, $zipcode, $paymentMethod, $courier,$serviceType='NORMAL') {
        return array(self::getKey($whId, $zipcode, $paymentMethod, $courier,$serviceType));
    }
    
    public static function getTatKey($whId, $zipcode, $courier=false, $serviceType='NORMAL') {
    	return "tat-".$whId."-".$zipcode."-".($courier?$courier:"A")."-".$serviceType;
    }
    
}

class ExpressTATCacheKeys {
	public static $prefix = "expresstat";

	public static function getKey($whId, $zipcode, $paymentMethod, $courier) {
		return "expresstat-".($whId?$whId:"A")."-".$zipcode."-".($paymentMethod?$paymentMethod:"A").($courier?"-".$courier:"");
	}

	public static function keySet($whId, $zipcode, $paymentMethod, $courier) {
		return array(self::getKey($whId, $zipcode, $paymentMethod, $courier));
	}
}

class SEOAdminCacheKeys {
    public static $prefix = "seoadminnew:";        
}

?>
