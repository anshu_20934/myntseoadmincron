<?php

include_once($xcart_dir."/Cache/Redis.php");
/**
 * Managed TableCache
 *
 * @author anjana
 */
class ManagedTableCache extends Redis {    
    private $dataFunction;
    private $rowFunction;
    private $dataParams;
    private $prefix;
    private $bucket_size = 512; 
  
    /**
     * 
     * @param type $dataSource Should be a function that returns entire table 
     * to be cached in the form of key value pairs
     */
    public function __construct($dataFunction, $prefix, $expiry) {
        ManagedTableCache::init();
        $this->dataFunction = $dataFunction;        
        $this->prefix = $prefix;        
        $this->expiry = $expiry;
        $this->seoRedisConnection = Redis::getSeoRedisRW();
    }   

    private function getBucket($key)
    {
        $hash = crc32("$key");
        $bucket = abs($hash) % $this->bucket_size;
        return $this->prefix.":".$bucket;
    }

    public function populateCache()
    {  
            $table_data = call_user_func_array($this->dataFunction, array());
            foreach ($table_data as $key => $value) {
                //self::$redisRW->hset($this->getBucket($key), $key, serialize($value));
                $key = $this->prefix.$key;
var_dump($key);
echo "\n";
var_dump($value);
echo "\n";
                $this->seoRedisConnection->set($key, serialize($value));
                //ADD expiry
                $this->seoRedisConnection->expire($key,$this->expiry);
            }
            return true;                
        
    }

    /* Get the Data */
    public function getMultiple($key1, $key2, $key3) 
    {
        $key1 =  $this->prefix.$key1;
        $key2 =  $this->prefix.$key2;
        $key3 =  $this->prefix.$key3;
        $val = self::$redisRO->mget($key1, $key2, $key3);
        if(!empty ($val)){
            $data = array();
            foreach ($val as $row){
                array_push($data, unserialize($row));
            }
            return $data;
        }        
        return NULL;
    }   
   
    public function get($key) {
        $value = self::$redisRO->get($this->prefix.$key);
        if(!empty($value)) {
            return $value;
        }
        return NULL;
    } 
}
?>

