<?php
require_once "./auth.php";
include_once ("./include/class/instrumentation/BaseTracker.php");
$tracker = new BaseTracker(BaseTracker::HOME_PAGE);

ini_set("display_errors", '0');
include_once("./include/func/func.mkcore.php");
include_once("./include/func/func.search.php");
include_once("./include/solr/solrProducts.php");
include_once("./include/class/cache/XCache.php");
include_once("./include/solr/solrQueryBuilder.php");
include_once("./include/solr/solrUtils.php");
include_once("./include/class/home/HomePage.php");
include_once("./include/class/class.recent_viewed_content.php");
include_once("./include/class/widget/class.pagemapping.widget.php");
include_once("./include/class/widget/class.widget.php");
include_once("./include/func/func_seo.php");
include_once($xcart_dir."/myntra/setShowStyleDetails.php");

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setContextualPageData(AnalyticsBase::HOME_PAGE);
$analyticsObj->setTemplateVars($smarty);

$shownewui= $_MABTestObject->getUserSegment("productui");
$productuiABparam=$shownewui;

$smarty->assign("shownewui", $shownewui);
$smarty->assign("productuiABparam", $productuiABparam);

include_once "./include/class/class.recent_viewed_content.php";
$__RECENT_HISTORY_DCONTENT->setGACategory("hp_strip");
$__RECENT_HISTORY_DCONTENT->genContent();
$__DCONTENT_MANAGER->startCapture();

$filetobeloaded = "index.php";

$cached_seo_data = $xcache->fetchAndStore(function($filetobeloaded) {
	$cached_seo_data = array();
	$cached_seo_data['pageTitle'] = getTitleTag($filetobeloaded);
	$cached_seo_data['metaDescription'] = getMetaDescription($filetobeloaded);
	$cached_seo_data['metaKeywords'] = getMetaKeywords($filetobeloaded);
	$cached_seo_data['seoExtraContents'] = getExtraContents($filetobeloaded);		
	return $cached_seo_data;
}, array($filetobeloaded), "indexSEO:".$filetobeloaded.":cached_seo_data",1800);
	
$pageTitle = $cached_seo_data['pageTitle'];
$metaDescription = $cached_seo_data['metaDescription'];
$metaKeywords = $cached_seo_data['metaKeywords'];
$seoExtraContents = $cached_seo_data['seoExtraContents'];


$smarty->assign("external_content", $seoExtraContents['external_content']);
$smarty->assign("viewcomment", $seoExtraContents['comments']);
$smarty->assign("rssfeeds", $seoExtraContents['rssfeeds']);
$smarty->assign("h1tag", $seoExtraContents['h1tag']);
$smarty->assign("h2tag", $seoExtraContents['h2tag']);
$smarty->assign("relatedkeywords", $seoExtraContents['relatedkeywords']);
$smarty->assign("imagesource", $seoExtraContents['imagesource']);
$smarty->assign("pageTitle", $pageTitle);
$smarty->assign("metaDescription", $metaDescription);
$smarty->assign("metaKeywords", $metaKeywords);

if (x_session_is_registered("chosenProductConfiguration"))
    x_session_unregister("chosenProductConfiguration");

if (x_session_is_registered("masterCustomizationArray"))
    x_session_unregister("masterCustomizationArray");


$smarty->assign("pagename", $pagename = "myhome");
$filetype = base64_decode($HTTP_GET_VARS['filetype']);
$smarty->assign("login_event", $filetype);

$selectedIndex = '0';
$smarty->assign("selectedIndex", $selectedIndex);
$smarty->assign("login_status", $HTTP_GET_VARS['login_status']);
$smarty->assign("logout_status", $HTTP_GET_VARS['logout_status']);
$smarty->assign("location", $location);

$model = new HomePage();
if($nocache != 1){
    $cache = $model->get_home_page($productuiABparam);

    if ($cache != NULL ) {
    	$tracker->fireRequest();
        echo $cache;
        return;
    }
}



$homepageBannerType = WidgetKeyValuePairs::getWidgetValueForKey('homepageBannerType');
$images=array();
if($homepageBannerType == "static"){
	$images = $model->get_images("new");
	$smarty->assign("homepageBannerType", "static");
}
else{
	$images = $model->get_images("old");
	$smarty->assign("homepageBannerType", "sliding");
}

$smarty->assign("images", $images);

if ($XCART_SESSION_VARS['logout_event']) {
    $smarty->assign("logout_event", $XCART_SESSION_VARS['logout_event']);
    x_session_unregister("logout_event");
}
$modelImagePath = "model" . rand(1, 10) . ".jpg";
$smarty->assign("modelImage", $modelImagePath);

#
#Load referral id if it exist
#
$refid = func_query_first("SELECT id FROM $sql_tbl[mk_referral] WHERE status='1'");

$smarty->assign("refid", $refid[id]);
$smarty->assign("quickRegRequired", 'Y');

$affiliateInfo = $XCART_SESSION_VARS['affiliateInfo'];
$headerflag = $affiliateInfo[0]['headerflag'];
$smarty->assign("headerflag", $headerflag);
if ($headerflag) {
    $XCART_SESSION_VARS['affiliatecarttemplate'] = '';
    $XCART_SESSION_VARS['affiliateleftpaneltemplate'] = '';
    $XCART_SESSION_VARS['affiliateInfo'][0]['themeid'] = '';
    //   $affiliateInfo[0]['themeid']='';
    $smarty->assign("themeid", "");
}

$smarty->assign("pageHeader", "Myntra - Your Style Factory");

$smarty->assign("logintype", "customer");


$linkin_team = (!empty($_GET['team']) && array_key_exists($_GET['team'],$jersey_style_ids))? $_GET['team'] :'mi';
$smarty->assign("linkin_team", $linkin_team);

$homepageWidgets = new WidgetMapping();
$homepageWidgetData = $homepageWidgets->getWidgets(WidgetMapping::$HOMEPAGE);

if(!empty($homepageWidgetData)) {
	$smarty->assign("scrollableWidgets", $homepageWidgetData);
}
global $skin;
if($skin === "skin2") {
	$home_page_content = $smarty->fetch("index.tpl");
	$model->set_home_page($home_page_content, $productuiABparam);
}
else {
	$home_page_content = func_display("myntra/myntra_home_content.tpl", $smarty, false);
	$model->set_home_page($home_page_content, $productuiABparam);
	$smarty->assign("home_page_content", $home_page_content);
	$home_page = func_display("myntra/myntra_home.tpl", $smarty, false);
}

$tracker->fireRequest();

echo $home_page;
?>