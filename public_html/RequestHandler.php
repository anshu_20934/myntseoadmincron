<?php
	include_once "authAPI.php";
	require_once "$xcart_dir/modules/RestAPI/RestUtils.php";
	require_once "$xcart_dir/modules/RestAPI/PermissionUtils.php";
	require_once "$xcart_dir/modules/RestAPI/CommonActions.php";
	ob_clean();

	//sample request params
	// Headers: 
	// Authorization: Basic YXBpYWRtaW46bTFudHJhUjBja2V0MTMhIw==
	// Get params: handler=Skus&otherparams
	/* 
	 *POST xml:    
	 *<?xml version="1.0" encoding="utf-8" standalone="yes"?>
	 *<portalApiRequest><handler>Skus</handler><data>any parameters</data></portalApiRequest>
	 *
	 */
	$uniqueId = uniqid();
	$request = RestUtils::createRequestObject($uniqueId);
	$authStatus = CommonActions::authenticateUser($request);
		
	if($authStatus) {
		$handler = $request->getRequestHandler();
		$portalapilog->info("ApiRequestHandler: Perform action for handler (ID: $uniqueId) - ". $handler);
		$portalapilog->info("cookies in request (ID: $uniqueId) - ".print_r($_COOKIE, true));
		if($handler && $handler != '') {
			include_once ($xcart_dir."/modules/RestAPI/".$handler.'Actions.php');
			$actionsClass = $handler.'Actions';
			$instance = new $actionsClass;
			$responseObject = $instance->handleRequest($request);
		} else {
			$responseObject = array();
			$responseObject['status'] = array();
			$responseObject['status']['statusCode'] = 700;
			$responseObject['status']['statusMessage'] = "No matching handler found for this request";
			$responseObject['status']['statusType'] = "FAILURE";
		}
	}
	
	if($request->getHttpAccept()=="json"){
		$portalapilog->info("ApiRequestHandler: Handler returned response (ID: $uniqueId) - ".print_r($responseObject, true));
	}else{
		$portalapilog->info("ApiRequestHandler: Handler returned response (ID: $uniqueId) - ".SimpleParser::arrayToXml($responseObject, 'PortalApiResponse'));
	}	
	ob_clean();
	if($request->getHttpAccept()=="json"){
		RestUtils::sendResponse(200, json_encode($responseObject), 'application/json');
	}else{
		RestUtils::sendResponse(200, SimpleParser::arrayToXml($responseObject, 'PortalApiResponse'), 'text/xml');
	}
?>
