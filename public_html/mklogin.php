<?php

use abtest\MABTest;

require_once "./auth.php";
include_once($xcart_dir."/Profiler/Profiler.php");
include_once "$xcart_dir/include/func/func.loginhelper.php";
include_once(HostConfig::$documentRoot."/include/class/instrumentation/BaseTracker.php");
include_once("$xcart_dir/include/class/class.mymyntra.php");
include_once("$xcart_dir/include/func/func.core.php");
include_once("$xcart_dir/include/func/func.utilities.php");

$tracker = new BaseTracker(BaseTracker::SIGNIN);
$loginStartTime	 = microtime(true);
$loginlog = $logger_manager->getLogger('MYNTRALOGINLOGGER');

require_once("$xcart_dir/include/sessionstore/session_store_wrapper.php");
x_load('crypt','user');

function sendResponse() {
    
    global $msg, $https_location, $http_location,$secureInvoked,$newRegister,$mode,$redirectUrl,$XCART_SESSION_VARS,$showLoginCaptchaFG,$ip_table_ttl,$ip_attempt_limit,$clientIPAddr;
    
    if($showLoginCaptchaFG and $mode == 'signin' and $msg['status'] == 'error'){
        
        if($msg['errorcode'] == '2')
            setLoginCaptchaInfo($clientIPAddr, false, $ip_table_ttl);
        else
            setLoginCaptchaInfo($clientIPAddr, true, $ip_table_ttl);

        $showLoginCaptcha = showLoginCaptcha($clientIPAddr, $ip_attempt_limit);
        
        if($showLoginCaptcha){
            Profiler::increment("captcha-shown-to-user"); 
            $msg['showLoginCaptcha'] = '1';
        }
        else{ 
            $msg['showLoginCaptcha'] = '0';
        }
    }
    

    $msg_txt = '';
    foreach ($msg as $key => $val) {
        if ($msg_txt) $msg_txt .= ';';
        $msg_txt .= $key . '=' . $val;
    }
    $msg_txt_enc = rawurlencode($msg_txt);

    if($newRegister){
        if($msg['status'] == 'success'){
            Profiler::increment("new-register-".$mode."-success");
            if(x_session_is_registered("secureloginreferrer")){
                x_session_unregister("secureloginreferrer");
             }
                
            func_header_location($http_location.$redirectUrl,false);
            exit;

        }else{
            Profiler::increment("new-register-".$mode."-fail");
            Profiler::increment("new-register-".$mode."-fail-".$msg['status']."-code-".(!empty($msg['errorcode'])?$msg['errorcode']:0));
            func_header_location($https_location .'/securelogin.php?msg='.$msg_txt_enc);
            exit;
        }
        exit;
    }
    else{
        $locBase = $secureInvoked ? $https_location : $http_location;
        
echo <<<HTML
<!DOCTYPE html>
<html>
    <head></head>
    <body>
    <script>
    if (typeof top.postMessage !== 'undefined') {
        top.postMessage("{$msg_txt}", "{$http_location}");
        top.postMessage("{$msg_txt}", "{$https_location}");
    }
    else {
        location.href = "{$locBase}/xd.php?msg={$msg_txt_enc}";
    }
    console.log("mklogin", "{$msg_txt}");
    </script>
    </body>
</html>
HTML;
    exit;
    }
}

$mode = filter_input(INPUT_POST, 'mode', FILTER_SANITIZE_STRING);
$user = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
$pass = $_POST['password'];
$mobile = filter_input(INPUT_POST, 'mobile', FILTER_SANITIZE_NUMBER_INT);
$gender = filter_input(INPUT_POST, 'gender', FILTER_SANITIZE_STRING);
$newRegister = filter_input(INPUT_POST, 'new_register', FILTER_SANITIZE_NUMBER_INT);
$redirectUrl = $XCART_SESSION_VARS['secureloginreferrer'];

$tracker->loginData->setUserEmail($user);

/* --------------------- AB Test and other fields for New Signup --------------------------------*/

$newSignupAB = MABTest::getInstance()->getUserSegment('newSignupAB');

$display_name = FeatureGateKeyValuePairs::getBoolean('display_name');
$display_birth_year = FeatureGateKeyValuePairs::getBoolean('display_birth_year');
$is_name_optional = FeatureGateKeyValuePairs::getBoolean('is_name_optional');
$is_birth_year_optional = FeatureGateKeyValuePairs::getBoolean('is_birth_year_optional');

if($newSignupAB == 'test'){

    //FETCHING FIRST NAME, LAST NAME AND YEAR OF BIRTH WHILE SIGNUP
    if($display_name){
            $user_name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            $firstName = "";
            $lastName = "";
            $nameArr = MyMyntra::splitFirstLastName($user_name);
            if(!empty($nameArr))    $firstName = $nameArr[0];
            if($nameArr[1]) $lastName = $nameArr[1];
    }
    if($display_birth_year){
            $DOB = filter_input(INPUT_POST,'birth-year',FILTER_SANITIZE_STRING);
            if(!empty($DOB)) $DOB = $DOB.'-00-00' ;
    }
}
/*------------------------------------------------------------------------------------------------*/

$secureInvoked = filter_input(INPUT_POST, 'secure_invoked', FILTER_SANITIZE_NUMBER_INT);

$msg = array();
$msg['type'] = $mode;

if (!checkCSRF($_POST["_token"], "mklogin")) {
    $msg['status'] = 'auth-error';
    $loginEndTime = microtime(true);
    Profiler::increment("auth-error-token");
    $loginlog->debug("auth-error : expected token = ". $XCART_SESSION_VARS['USER_TOKEN']. " does not match input ".$_POST["_token"] . " for XCARTSESSID = md5($XCARTSESSID) where login = $user, mode = $mode in time :". ($loginEndTime - $loginStartTime));
    sendResponse();
}

$errors = array();
if (empty($user)) $errors[] = 'email';
if (empty($pass)) $errors[] = 'pass';
//if ($mode === 'signup' && empty($mobile)) $errors[] = 'mobile';
if ($mode === 'signup'){
    if($newSignupAB == 'true'){
        if($display_name  and !$is_name_optional and empty($user_name)) $errors[] = 'name';
        if($display_birth_year and !$is_birth_year_optional and empty($DOB)) $errors[] = 'birth_year';
    }
}

if (!empty($errors)) {
    $msg['status'] = 'data-error';
    $msg['errors'] = implode('~', $errors);
    $loginEndTime = microtime(true);
    Profiler::increment("data-error-empty-input");
    $loginlog->debug("data-error : empty input data : XCARTSESSID = md5($XCARTSESSID) where login = $user, mode = $mode , mobile = $mobile in time :". ($loginEndTime - $loginStartTime));
    sendResponse();
}

//check if user is already logged in and this is session expiry login//
$userLoggedIn = $XCART_SESSION_VARS['login'];

//$login = $user;
$login_type = 'C';
$_curtime = time();

$userAuthManager = new user\UserAuthenticationManager();

if ($mode == 'signin') {
	
    $tracker->loginData->setAction("signin");

    if($showLoginCaptcha){
        if(filter_input(INPUT_POST, 'userinputcaptcha', FILTER_SANITIZE_STRING) != $XCART_SESSION_VARS['captchasAtMyntra']["loginPageCaptcha"]){
            Profiler::increment("incorrect-captcha-entered");
            $msg['status'] = 'error';
            $msg['errorcode'] = '5';
            sendResponse();
        }
        else{
             Profiler::increment("correct-captcha-entered");
             unset($XCART_SESSION_VARS['captchasAtMyntra']["loginPageCaptcha"]);
        }
    }
	
    try{
		$user = $userAuthManager->performUserSignIn($user, $pass ,user\User::USER_TYPE_CUSTOMER);
		$msg['status'] = 'success';
        /*CLEAR ALL SESSION VARABLE RELATED TO LOGIN ONLY*/
        if(!empty($userLoggedIn)){
          clearSessionVarsOnLogout();
        }    
        /*END CLEAR VARS*/
		$account_created_date = $user->getFirstLoginTime();
		
		$account_type = $user->getUserType();
		$userfirstname = $user->getFirstName();
        $userlastname = $user->getLastName();
		$mobile = $user->getMobile();
		$login = $user->getLogin();
        $DOB = $user->getDOB();
        change_sessionid();
        $gender = $user->getGender();
		$tracker->loginData->setFinalResult(instrumentation\LoginPTracker::SUCCESS);
    
	}catch(user\exception\UserDoesNotExistsException $ex){
		$tracker->loginData->setFinalResult(instrumentation\LoginPTracker::USER_DOESNT_EXISTS_FAILURE);
        $msg['status'] = 'error';
        $msg['errorcode'] = 1;
        $loginEndTime = microtime(true);
        Profiler::increment("user-does-not-exist-error");
        $loginlog->debug("user-does-not-exist-error : input data : XCARTSESSID = md5($XCARTSESSID) where login = $user, mode = $mode in time :". ($loginEndTime - $loginStartTime));
        sendResponse();
	}catch(user\exception\PasswordDoesNotMatchException $ex){
		$tracker->loginData->setFinalResult(instrumentation\LoginPTracker::PASSWORD_FAILURE);
        $msg['status'] = 'error';
        $msg['errorcode'] = 2;
        $loginEndTime = microtime(true);
        Profiler::increment("password-mismatch-error");
        $loginlog->debug("password-mismatch-error : input data : XCARTSESSID = md5($XCARTSESSID) where login = $user, mode = $mode in time :". ($loginEndTime - $loginStartTime));
        sendResponse();
    }catch(user\exception\AccountSuspendedException $ex){
        $tracker->loginData->setFinalResult(instrumentation\LoginPTracker::USER_SUSPENDED_FAILURE);
        $msg['status'] = 'error';
        $msg['errorcode'] = 3;
        $loginEndTime = microtime(true);
        Profiler::increment("user-suspended-error");
        $loginlog->debug("user-suspended-error : input data : XCARTSESSID = md5($XCARTSESSID) where login = $user, mode = $mode in time :". ($loginEndTime - $loginStartTime));
        sendResponse();
    }
    $tracker->fireRequest();
}
else if ($mode == 'signup') {
    try{

        $checkUserCredential = $userAuthManager->loginCredentialCheck($user, $pass ,user\User::USER_TYPE_CUSTOMER);
    
    }catch(user\exception\PasswordDoesNotMatchException $ex){
        $tracker->loginData->setFinalResult(instrumentation\LoginPTracker::USER_ALREADY_EXISTS_FAILURE);
        $msg['status'] = 'error';
        $msg['errorcode'] = 1;
        $loginEndTime = microtime(true);
        Profiler::increment("password-mismatch-error");
        $loginlog->debug("password-mismatch-error : register user exists and wrong password : input data : XCARTSESSID = md5($XCARTSESSID) where login = $user, mode = $mode in time :". ($loginEndTime - $loginStartTime));
        sendResponse();
    }

    if($checkUserCredential){
        try{
            $user = $userAuthManager->performUserSignIn($user, $pass ,user\User::USER_TYPE_CUSTOMER);
            $msg['status'] = 'success';
            $msg['signinFrmSignup'] = '1';
            /*CLEAR ALL SESSION VARABLE RELATED TO LOGIN ONLY*/
            if(!empty($userLoggedIn)){
              clearSessionVarsOnLogout();
            }    
            /*END CLEAR VARS*/
            $account_created_date = $user->getFirstLoginTime();
            
            $account_type = $user->getUserType();
            $userfirstname = $user->getFirstName();
            $userlastname = $user->getLastName();
            $mobile = $user->getMobile();
            $login = $user->getLogin();
            $DOB = $user->getDOB();
            $gender = $user->getGender();
            change_sessionid();
            $tracker->loginData->setAction("signin");
            $tracker->loginData->setFinalResult(instrumentation\LoginPTracker::SUCCESS);
        }catch(user\exception\UserDoesNotExistsException $ex){
            $tracker->loginData->setFinalResult(instrumentation\LoginPTracker::USER_DOESNT_EXISTS_FAILURE);
            $msg['status'] = 'error';
            $msg['errorcode'] = 1;
            $loginEndTime = microtime(true);
            Profiler::increment("user-does-not-exist-error");
            $loginlog->debug("user-does-not-exist-error : input data : XCARTSESSID = md5($XCARTSESSID) where login = $user, mode = $mode in time :". ($loginEndTime - $loginStartTime));
            sendResponse();
        }catch(user\exception\PasswordDoesNotMatchException $ex){
            $tracker->loginData->setFinalResult(instrumentation\LoginPTracker::PASSWORD_FAILURE);
            $msg['status'] = 'error';
            $msg['errorcode'] = 2;
            $loginEndTime = microtime(true);
            Profiler::increment("password-mismatch-error");
            $loginlog->debug("password-mismatch-error : input data : XCARTSESSID = md5($XCARTSESSID) where login = $user, mode = $mode in time :". ($loginEndTime - $loginStartTime));
            sendResponse();
        }catch(user\exception\AccountSuspendedException $ex){
            $tracker->loginData->setFinalResult(instrumentation\LoginPTracker::USER_SUSPENDED_FAILURE);
            $msg['status'] = 'error';
            $msg['errorcode'] = 3;
            $loginEndTime = microtime(true);
            Profiler::increment("user-suspended-error");
            $loginlog->debug("user-suspended-error : input data : XCARTSESSID = md5($XCARTSESSID) where login = $user, mode = $mode in time :". ($loginEndTime - $loginStartTime));
            sendResponse();
        }
	}
    else {
        $tracker->loginData->setAction("signup");
    	$user = new user\User($user,user\User::USER_TYPE_CUSTOMER);

        $mrp_referer = filter_input(INPUT_POST, 'mrp_referer', FILTER_SANITIZE_STRING);
        $profile_values = array();
	    $profile_values['email'] = $user->getLogin();
	    $profile_values['login'] = $user->getLogin();
	    $profile_values['mobile'] = $mobile;
        $profile_values['usertype'] = $login_type;
	    $profile_values['password'] = $pass;
	    $profile_values['referer'] = $mrp_referer;
	
    /* ADDITIONAL FIELDS WHILE SIGNUP */
    
    if(!empty($firstName))
        $profile_values['firstName'] = $firstName;
    if(!empty($lastName))
        $profile_values['lastName'] = $lastName;
    if(!empty($DOB))
        $profile_values['DOB'] = mysql_real_escape_string($DOB); 
    
    /*-----------------------------------*/

    if(!empty($gender)){
		$profile_values['gender'] = $gender;
        $tracker->loginData->setUserGender($gender);
	}

	try{
		$user = $userAuthManager->performUserRegistration($profile_values);
		$account_created_date = $user->getFirstLoginTime();
        /*CLEAR ALL SESSION VARABLE RELATED TO LOGIN . SESSION EXPIRY*/
        if(!empty($userLoggedIn)){
          clearSessionVarsOnLogout();
        }
        $msg['status'] = 'success';
        $userfirstname = '';
        change_sessionid();
	$login=$user->getLogin();
        $tracker->loginData->setFinalResult(instrumentation\LoginPTracker::SUCCESS);

	    }catch(user\exception\UserAlreadyExistsException $ex){
		  $tracker->loginData->setFinalResult(instrumentation\LoginPTracker::USER_ALREADY_EXISTS_FAILURE);
		  $msg['status'] = 'error';
		  $msg['errorcode'] = 1;
		  $loginEndTime = microtime(true);
            Profiler::increment("user-already-exists-error");
		  //$loginlog->debug("user-already-exists-error : input data : XCARTSESSID = md5($XCARTSESSID) where login = $user, mode = $mode , mobile = $mobile in time :". ($loginEndTime - $loginStartTime));
		  sendResponse();
	   }
	   $tracker->fireRequest();
    }
}
setSessionVarsOnLogin();
$loginEndTime = microtime(true);
Profiler::increment("success");
$loginlog->debug("success : input data : XCARTSESSID = md5($XCARTSESSID) where login = {$user->getLogin()}, mode = $mode , mobile = $mobile in time :". ($loginEndTime - $loginStartTime));
sendResponse();
?>
