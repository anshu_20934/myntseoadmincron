<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: register.php,v 1.7 2006/01/11 06:55:57 mclap Exp $
#
require_once "./auth.php";

use abtest\MABTest;

include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
$tracker = new BaseTracker(BaseTracker::REGISTRATION);

require_once $xcart_dir."/include/categories.php";
include_once("./include/func/func.mkcore.php");
include_once ("$xcart_dir/include/class/mcart/class.MCart.php");
$productsInCart = $XCART_SESSION_VARS["productsInCart"];
$login = $XCART_SESSION_VARS['login'];

$pageName = 'other';
include_once $xcart_dir."/webengage.php";
include_once $xcart_dir."/gtm.php";

/** Setting login background image */
$loginSignupABTest = MABTest::getInstance()->getUserSegment('loginSignupABTest');
$smarty->assign("loginSignupABTest",$loginSignupABTest);

/**
 * Use cases
 * Use case 1 : If user is logged in cart has products .. redirect to shipping address
 * Use case 2 : User just logged in, and no products in cart go to home.
 */


#
# Where to forward <form action
#
$smarty->assign("register_script_name",(($config["Security"]["use_https_login"]=="Y")?$xcart_catalogs_secure['customer']."/":"")."register.php");

$action= strip_tags($_GET['action']);
$action= sanitize_paranoid_string($action);

if (empty($mode) and !empty($login))
	$mode = "update";

include_once("./include/func/func.mail.php");
require $xcart_dir."/include/register.php";

$_GET['ref'] = str_replace('[AT]', '@', $_GET['ref']);

if($login){
	if (x_session_is_registered("referer_url")) {
		 $toRedirect = $XCART_SESSION_VARS['referer_url'];
                x_session_unregister("referer_url");
                //If the user reached register page from reset password page. Then upon login redirect him to homepage.
                if(strpos($toRedirect, "reset_password") === FALSE){
                    func_header_location($toRedirect, false);
                }
                else{
                    func_header_location($http_location."/index.php", false);
                }
	} else {
		func_header_location($http_location."/index.php", false);
	}


}

$referer = $_GET['ref'];
if($referer == $login)	{
	$redirectToHome = true;
}

$mcartFactory= new MCartFactory();
$myCart= $mcartFactory->getCurrentUserCart();

if($login && ($myCart!=null && $myCart->getItemQuantity()>0)){
    if (x_session_is_registered("referer_url"))
        x_session_unregister("referer_url");
		if($redirectToHome)	{
			func_header_location($http_location."/index.php", false);
		} else {
			func_header_location($https_location."/mkcustomeraddress.php?xid=".$XCARTSESSID, false);
		}
} else if($login) {
        if (x_session_is_registered("referer_url"))
        x_session_unregister("referer_url");
        func_header_location($http_location."/index.php", false);
}

$smarty->assign("login",$login);
$smarty->assign("mode",$mode);

$smarty->assign("success",$_GET['success']);
if($_GET['regerror'] == "IC"){
	if (x_session_is_registered("reg_username_entered")){
		$smarty->assign("reg_username_entered",$XCART_SESSION_VARS['reg_username_entered']);
        x_session_unregister("reg_username_entered");
	}
	if (x_session_is_registered("reg_firstname_entered")){
		$smarty->assign("reg_firstname_entered",$XCART_SESSION_VARS['reg_firstname_entered']);
        x_session_unregister("reg_firstname_entered");
	}
	if (x_session_is_registered("reg_lastname_entered")){
		$smarty->assign("reg_lastname_entered",$XCART_SESSION_VARS['reg_lastname_entered']);
        x_session_unregister("reg_lastname_entered");
	}
}
$smarty->assign("reg_error",$_GET['regerror']);
$smarty->assign("message",$_GET['message']);

# Assign the current location line
$smarty->assign("location", $location);
include_once("./include/func/func.mkcore.php");

# Assign the current location line
$smarty->assign("location", $location);

$myAcc= strip_tags($_GET['myAcc']);
$myAcc= sanitize_paranoid_string($myAcc);
$smarty->assign("myAcc", $myAcc);
$smarty->assign("checkout", $_GET['checkout']);
$smarty->assign("termandcondition",$userinfo['termcondition']);

if(!empty($_GET['redirecturl']))
{
	  $str = $_GET['redirecturl'];
	  $referer = $str ;
     $smarty->assign("referer", $referer);
}

if($_GET["frmcheckout"] == "true")
{
	func_header_location($https_location."/mkpaymentoptions.php?update=SUCCESS&xid=".$XCARTSESSID, false);
}

require_once($xcart_dir."/include/fb/facebook_login.php");
$facebook_login = new Facebook_Login();
$facebook = $facebook_login->facebook;
$fb_uid = $facebook->getUser();
if ($fb_uid) {
  try {
    $fb_me = $facebook->api('/me');
  } catch (FacebookApiException $e) {
    error_log($e);
  }
}

$smarty->assign("fb_login_url", $facebook->getLoginUrl());
$smarty->assign("fb_appId", $facebook->getAppId());
$smarty->assign("fb_username_found", '0');
$smarty->assign("fb_new_user", '0');
$smarty->assign("fb_logged",'0');
if($fb_me) {
	$smarty->assign("fb_logged",'1');
	$smarty->assign("reg_firstname_entered", $fb_me["first_name"]);
	$smarty->assign("reg_lastname_entered", $fb_me["last_name"]);
	$fb_email = $fb_me["email"];
	$smarty->assign("reg_username_entered", $fb_email);
	$fb_uid = $facebook->getUser();
	if(func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[customers] WHERE login='$fb_email'") > 0) {
		if(func_query_first_cell("SELECT COUNT(*) FROM mk_facebook_user WHERE fb_uid='$fb_uid'") > 0) {
			//user already present and connected with fb we will update the user info now
			$smarty->assign("fb_username_found", '2');
		} else {
			//user is not present in the fb database show him pop dialog and make a entry in the db
			$smarty->assign("fb_username_found", '1');
		}
	} else {
		//user not registered make a entry in to the facebook database and show him the password entering form
		$smarty->assign("fb_new_user", '1');
	}

	$facebook_login->insertOrUpdateFaceBookDB($fb_me);

}

$mrp_referer = mysql_real_escape_string($_GET['ref']);
if(!empty($mrp_referer)){
    $query = "SELECT firstname, lastname FROM $sql_tbl[customers] WHERE login='$mrp_referer';";
    $result = db_query($query);
    $row = db_fetch_array($result);
}

if(empty($row)) {
	$mrp_referer = "";
} else {
	$refName = $mrp_referer;
	if($row['firstname'] != '') {
		$refName = $row['firstname'];
		$refName .= ($row['lastname'] != '')? " ".$row['lastname']: " ";
	}
}

$smarty->assign("ref_name",$refName);
$smarty->assign("mrp_referer", $mrp_referer);

global $mrpCouponConfiguration, $mrp_fbCouponValue, $mrp_nonfbCouponValue;
$extraCredit = $mrp_fbCouponValue - $mrp_nonfbCouponValue;
$smarty->assign("extraCredit", $extraCredit);

$smarty->assign("mrp_refRegCouponValue",$mrpCouponConfiguration['mrp_refRegistration_numCoupons']*$mrpCouponConfiguration['mrp_refRegistration_mrpAmount']);
$smarty->assign("mrp_refPurCouponValue",$mrpCouponConfiguration['mrp_refFirstPurchase_numCoupons']*$mrpCouponConfiguration['mrp_refFirstPurchase_mrpAmount']);

$tracker->fireRequest();

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setContextualPageData(AnalyticsBase::REGISTER);
$analyticsObj->setTemplateVars($smarty);

$newSignupAB = MABTest::getInstance()->getUserSegment('newSignupAB');

if($loginSignupABTest == 'test'){
    $done = "" ;
    $query_string = $_SERVER['QUERY_STRING'];
    if($query_string){
        $done = "&" . $query_string;
    }
    $doneUrl = $XCART_SESSION_VARS['referer_url'];
    if(strpos($doneUrl, "reset_password") === FALSE){
	    if($doneUrl)
	    {
			$done = "&done=" . urlencode($doneUrl) ;
	    }
	    if($_GET["view"])
	    {
	    	$doneStr = "$http_location/mymyntra.php?view=" . $_GET["view"];
			// $done = "&done=" . urlencode("$http_location/mymyntra.php?view=" . $_GET["view"]) ;
			if(isset($_GET['subview'])){
				$subview = filter_input(INPUT_GET,'subview',FILTER_SANITIZE_STRING);
				$subview = (in_array($subview, array('mycashback','myloyalty','mycoupons'))) 
					? $subview 
					: "";
				if($subview){
					$doneStr = $doneStr . "#" . $subview;
				}
			}
			$done = "&done=" . urlencode($doneStr) ;
	    }
    } 
    //stopping redirection as it was causing performance issue - contact naren/abhinav to understand more
    //func_header_location("$http_location/index.php?show=login". $done , false);
}
$smarty->assign('canonicalPageUrl', "$http_location/register.php");
if($skin === "skin2") {
    $smarty->assign("referer", str_replace('[AT]', '@', $referer));
    $smarty->assign("newSignupAB", $newSignupAB);
	$smarty->display("register.tpl");
} else {
    if ($abLoginPopup == 'splash') {
        func_display("customer/mkregisterme_B.tpl",$smarty);
    }
    else {
        func_display("customer/mkregisterme.tpl",$smarty);
    }
}

