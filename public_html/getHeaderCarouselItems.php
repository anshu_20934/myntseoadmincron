<?php 
define("QUICK_START", 1);
include_once("./auth.php");
include_once "$xcart_dir/include/class/widget/class.widget.skulist.php";
include_once ("$xcart_dir/include/class/mcart/class.MCart.php");
use enums\saveditems\SavedItemActionType;
use saveditems\action\SavedItemAddAction;
use saveditems\action\SavedItemAction;

$type=filter_input(INPUT_GET, 'type', FILTER_SANITIZE_STRING);
$style_ids=array();
if($type!=''){
  switch($type)
   {
	case "saved":
		$action = new SavedItemAction();
		$savedItems=$action->getProductsInSaved();
		$savedArray = array();
		foreach ($savedItems as $itemid => $item) {
  			 $savedArray[$itemid] = MCartUtils::convertCartItemToArray($item);
		}
		$smarty->assign('savedWidget',true);
		$smarty->assign('bagItems', $savedArray);
			
		break;
	case "bag":
		//get bag items style id
   		$mcartFactory= new MCartFactory();
		$mcart=$mcartFactory->getCurrentUserCart();
		$productArray=array();
		if($mcart!=null){
			$mcart->refresh(false,false,false,false);
			$productArray = MCartUtils::convertCartToArray($mcart);
		}
		$smarty->assign('bagWidget',true);
		$smarty->assign('bagItems', $productArray);
		break;
	case "recent":
		$recent_views_cookie_content = getMyntraCookie('RVC001');
		if(!empty($recent_views_cookie_content))
		{
			$recent_views=explode(',',$recent_views_cookie_content);
		}
		$style_ids=array_unique($recent_views);
		$widgetData=getWidget($style_ids);
		$smarty->assign('recentWidget',true);
		$smarty->assign('widgetData',$widgetData);
		break;
   }
   
   if(!empty($style_ids) || !empty($productArray) || !empty($savedArray)){
  		$div_content=$smarty->fetch("header/header-carousel-items.tpl", $smarty);
  		if (!empty($div_content)){
  		 	$resp = array(
            	'status'  => 'SUCCESS',
            	'content' => $div_content
        	);
  		}
  		else {
  			$resp = array(
            	'status'  => 'ERROR',
            	'content' => ''
        	);
  		}
   		
   	}
   	else{
   		$resp = array(
            	'status'  => 'ERROR',
            	'content' => ''
        	);
   	}

}
else {
	$resp = array(
       	'status'  => 'ERROR',
       	'content' => ''
    	);
}
echo json_encode($resp);

function getWidget($styleids){
		$styleWidget=new WidgetSKUList('headerCarouselItems',$styleids);
		$widgetFormattedData = $styleWidget->getData();
		return $widgetFormattedData;
}	
