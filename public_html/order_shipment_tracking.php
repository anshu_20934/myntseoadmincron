<?php

require "auth.php";
include_once("$xcart_dir/include/func/func.db.php");
include_once("$xcart_dir/include/func/func.mkcore.php");
include_once("$xcart_dir/include/func/func.courier.php");


if(checkCSRF($_POST["_token"], "mymyntra", "get-shipment-tracking") === false){
	header("HTTP/1.1 403 Forbidden");
	exit;
}

$orders = array();

$requestData = json_decode($_POST['requestData'], true);

$result = func_query("select orderid, courier_service, tracking from xcart_orders where orderid in (" . implode(',', array_keys($requestData['shipments'])) . ");");
foreach ($result as $row) {
	$orders[$row['orderid']] = $row;
	unset($orders[$row['orderid']]['orderid']);
}
unset($result);

foreach ($requestData['shipments'] as $orderid => &$shipment) {
	$shippeddate = func_query_first_cell("select shippeddate from xcart_orders where orderid=".$orderid, true);
	$requestData['shipments'][$orderid]['courier'] = get_courier_display_name($orders[$orderid]['courier_service']);
	$requestData['shipments'][$orderid]['trackingNo'] = $orders[$orderid]['tracking'];
	if($shippeddate && $shippeddate != 0) // shippeddate is either NULL or 0 for WP orders
		$requestData['shipments'][$orderid]['trackingDetails'] = getOrderTrackingDetails($orderid, true, $orders[$orderid]['courier_service'], $orders[$orderid]['tracking']);
}

$smarty->assign("trackingData",$requestData);
$smarty->assign("customerSupportCall",  WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall'));
$smarty->assign("customerSupportTime",  WidgetKeyValuePairs::getWidgetValueForKey('customerSupportTime'));

$html = func_display("shipment_tracking_details.tpl", $smarty, false);

echo $html;

?>
