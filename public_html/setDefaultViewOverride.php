<?php

include_once("auth.php");
use web\filters\impl\MobileFilter;

$override = filter_input(INPUT_GET, 'override', FILTER_SANITIZE_STRING);
$redirecturl = filter_input(INPUT_GET, 'redirecturl', FILTER_SANITIZE_URL);

if($override == "true"){
    $override = true;
}else{
    $override = false;
}
MobileFilter::getInstance()->setDefaultViewOverride($override);

if($override === true){
	setMyntraCookie("dvo", 1, 0, "/", $cookiedomain, false, true);
} else {
	deleteMyntraCookie('dvo');
}

header("HTTP/1.1 307 Moved Temporarily");
if(!empty($redirecturl) && strpos(parse_url($redirecturl, PHP_URL_HOST), '.myntra.com') !== false){
    header("Location: $redirecturl");
}else{
    header("Location: ".HostConfig::$baseUrl);
}
?>
