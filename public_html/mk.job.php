<?php
/**
*	File to handle the various background jobs in the system. This file will be invoked using a cron .
*	
*/


define("REPORTS_CONNECTION",true);
require_once "./auth.php";
require_once "$xcart_dir/include/func/func.mail.php";
global $reportlog;

$time=time();	
$reportlog->info("schedular program invloked at ". $time);

//Get all active jobs
$jobquery ="select job_id,name,scriptname,methodname,retrydelaytime,retrycounts,notificationemailaddress,delayinterval from job where isactive=1";

$jobresult = db_query($jobquery);
//print_r($jobresult);
if($jobresult)
{
	$reportlog->debug("Active jobs found");


				while ($row = db_fetch_array($jobresult)) 
				{
						$qry_job_start_time ="select start_time,FROM_UNIXTIME(start_time,'%d/%m/%Y %H:%i:%s') as stime  from jobstatus where job_id= ".$row['job_id']." and start_time=(select max(start_time) from jobstatus a where a.job_id=".$row['job_id'].") and  ".$time ."- end_time > ".$row['delayinterval'];
						
						$res_job_start_time = db_query($qry_job_start_time);
						$row_job_startime = db_fetch_array($res_job_start_time);



						$reportlog->debug("Checking if job ".$row['name'] ." is scheduled to run ");

//echo("Checking if job ".$row['name'] ." is scheduled to run ");
						if($row_job_startime['start_time'])
						{
								$reportlog->debug("starting job ".$row['name']);
								echo("starting job ".$row['name']);	
								$count=0;
								$scriptname=$row['scriptname'];
								$methodname= $row['methodname'];
								require_once($scriptname);
								$notificationemailaddress=$row['notificationemailaddress'];
								$func =  new ReflectionFunction($methodname);
								$starttime=time();
								$executetime = $func->invoke($row_job_startime['start_time'] );
								$endtime=time();
								$uniqueprimaryid="JOB_".uniqid();
								// if else to handle failure of a job end time will be '0' in faliure case
								if($executetime){
									$reportlog->info('job '.$row['name']. 'execution successful ');
								$runstatus="INSERT INTO jobstatus(id,job_id,start_time,end_time,retries) values ('$uniqueprimaryid','$row[job_id]','$executetime','$endtime','0')";
								}
								else{
									$reportlog->info('job '.$row['name']. 'execution failed ');
								$runstatus="INSERT INTO jobstatus(id,job_id,start_time,retries) values ('$uniqueprimaryid','$row[job_id]','$starttime','0')";
								}

								$flag = db_query($runstatus);
								  
								//Re running the job in case of faliures. We will run the job for the number of times retry count is set and add delay for the as per settings


								while($count<=$row['retrycounts'] && !$executetime)
								{
										$reportlog->info(' Retrying executing job '.$row['name']);
										$starttime=time();
										$executetime = $func->invoke($row_job_startime['start_time']);
										$endtime=time();
										
									    // if else to handle failure of a job end time will be '0' in faliure case
									    if($executetime){
									    $rerunstatus="update jobstatus set job_id='$row[job_id]',start_time='$executetime',end_time='$endtime',retries='$count' where id ='$uniqueprimaryid'";
									    }
									    else{
										   $rerunstatus="update jobstatus set job_id='$row[job_id]',start_time='$starttime',retries='$count' where id ='$uniqueprimaryid'";
									    }

									   $flag = db_query($rerunstatus);
									   sleep($row['retrydelaytime']);
									 $count++;
								}
								if(!$executetime)
								{
								//send mail to admin that job could  not be run successfully
										

								  $template = "backgroundtasks";
								  $mailto = "mgmt@myntra.com";
								  $from = "MyntraAdmin";
								  $args = array("TASK_NAME" => $row['name'],
											 "EXECUTION_TIME" => $row_job_startime['stime'],											 
											  );
								  $reportlog->info('Sending mail for failed task '.$row['name']);
								 
     			   				  //$flag= sendMessage($template, $args,$mailto,$from);
								 
                                  
								}

						}
						else
						{
							// simply log a message and exit.
							$reportlog->info('job '.$row['name']. 'is not scheduled to run  ');
						

						}
					
					}

}
else
{
	$reportlog->info("No active jobs in the system");

}
$reportlog->info("schedular program completes at ". time());


				
?>

