<?php
use FeatureGateKeyValuePairs;

global $pageName;

if ($skin != 'skin2') {
	return;
}

if (in_array($pageName, array("cart","checkout","confirmation"), true)) {
	$smarty->assign('isGTMLazyLoadEnabled', false);
	return;
}

$enabledPages = array();
$enabledPages = FeatureGateKeyValuePairs::getStrArray('gtm.lazyLoadPages');
$smarty->assign('isGTMLazyLoadEnabled', in_array($pageName, $enabledPages, true));

?>