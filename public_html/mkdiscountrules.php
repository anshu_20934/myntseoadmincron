<?php
  // get the discount coupon details according to the coupon code.
  // return error msg if the coupon code is invalid or has expired.

  // case 1
  // order subtotal equals or exceed the amount specified

  //case 2
  //cart contains the products specified
  // **** discount applied once per order
  // **** disocunt applied to each item

  //case 3
  //cart contains products belonging to a caategory/subcategory
  // **** discount applied once
  // **** discount applied to each product from the category
  // **** discount applied to each product title from category

function check_coupon_validity($couponCodeDetails){
	global $login,$couponCode,$grandTotal;
	
    if (empty($couponCodeDetails)){
        $errorMessageCoupon = "";
        return $errorMessageCoupon;
    }
    //check for the coupon validity and usage
    $couponExpirationDate = $couponCodeDetails[8];
    if ($couponExpirationDate < time())
	{
		//check time with Unix Epoch
        $errorMessageCoupon = "The coupon has expired.";
        return $errorMessageCoupon;
    }

    //check for the coupon startdate
    $couponStartDate = $couponCodeDetails[21];
    if ($couponStartDate > time()){
        $after = date('jS M, Y',$couponStartDate);
        $before = date('jS M, Y',$couponExpirationDate);
        $errorMessageCoupon = "The coupon is only valid between $after and $before.";
        return $errorMessageCoupon;
    }
    
    // for peruser check
    if($couponCodeDetails[6] == 'Y')
    {
        
	 
		$couponUsed = db_query("SELECT times_used FROM xcart_discount_coupons_login WHERE login='".$login."' AND coupon='".$couponCode."'");
		$row = db_fetch_array($couponUsed);
		if ($row['times_used']  > 0)
		{
			// need to put the logic for peruser check
			$errorMessageCoupon = "Your coupon usage has expired.";
			return $errorMessageCoupon;
		}
			
       
    }
    else 
    {
    	if ($couponCodeDetails[7] >= $couponCodeDetails[5])
		{
			$errorMessageCoupon = "The coupon usage has expired.";
		    return $errorMessageCoupon;
		}
    	elseif ($couponCodeDetails[7]+$couponCodeDetails[20] >= $couponCodeDetails[5]){
			$errorMessageCoupon = "The coupon has been locked due to a possible incomplete order. please wait for 9 hours for coupon to be released and then try again.";
		    return $errorMessageCoupon;
		}
    	
    }

	/*if ($couponCodeDetails[7] >= $couponCodeDetails[5]){ // need to put the logic for peruser check
        $errorMessageCoupon = "This coupon has already been redeemed. Please provide valid coupon code";
        return $errorMessageCoupon;
    }*/
    return "";
}

function calculate_discount_for_cart($productsInCart,$productids,$couponCodeDetails,$grandTotal){
	global $amountOf99corner;
	global $weblog;
	$weblog->info(" PRODUCT TYPE >  >>>>>>>>>> ".$couponCodeDetails[18]);
	$weblog->info(" PRODUCT STYLE >  >>>>>>>>>> ".$couponCodeDetails[16]);
	$grandTotal = (!empty($amountOf99corner)) ? ($grandTotal - $amountOf99corner) :  $grandTotal;

    if ($couponCodeDetails[2] == '0' && $couponCodeDetails[3] == '0' && $couponCodeDetails[16] == '0'&& $couponCodeDetails[18] == '0' && $couponCodeDetails[19] == '0'){
    	//case 1 order subtotal equals or exceed the amount specified
		$weblog->info(" CASE =====1");
        return calculate_order_subtotal_discount($productsInCart,$productids,$couponCodeDetails,$grandTotal);
    }
    elseif ($couponCodeDetails[2] != '0' && $couponCodeDetails[3] == '0'){
    	//case 2 cart contains the products specified
				$weblog->info(" CASE =====2");

        return calculate_product_only_discount($productsInCart,$productids,$couponCodeDetails);
    }
    elseif ($couponCodeDetails[2] == '0' && $couponCodeDetails[3] != '0'){
    	//case 3 cart contains products from specific category/subcategory.
				$weblog->info(" CASE =====3");

		return calculate_category_only_discount($productsInCart,$productids,$couponCodeDetails);
    }
	elseif (!empty($couponCodeDetails[16])){
		//case 3 cart contains products from specific styleid.
				$weblog->info(" CASE =====4");

		return calculate_order_style_discount($productsInCart,$productids,$couponCodeDetails,$grandTotal);
    }
   elseif ($couponCodeDetails[16]  == 0 && $couponCodeDetails[18] != '0'){
   	   //case 3 cart contains products from specific typeid and no particular style defined.
	   		$weblog->info(" CASE =====5");

		return calculate_order_type_discount($productsInCart,$productids,$couponCodeDetails);
    }
    elseif (!empty($couponCodeDetails[19])){
        //case 3 promotional offer
				$weblog->info(" CASE =====6");

        return calculate_discount_for_promotion($productsInCart,$productids,$couponCodeDetails);
    }
    else
	{
		 	 return 0;
    }

}

function calculate_order_subtotal_discount($productsInCart,$productids,$couponCodeDetails,$grandTotal){
    //TODO check for the max discount that can be availed.
    if ($grandTotal < $couponCodeDetails[4])
        return 0;
    //check for maximum if maximum defined and total greater than max then offer percent discount only on maximum amount
   if (!empty($couponCodeDetails[17])&&$couponCodeDetails[17]!=0&&$grandTotal > $couponCodeDetails[17]&&$couponCodeDetails[1] == 'percent')
        return (($couponCodeDetails[17] * $couponCodeDetails[0])/100);
    if ($couponCodeDetails[1] == 'absolute'){
        return $couponCodeDetails[0];
    }elseif($couponCodeDetails[1] == 'percent'){
        return (($grandTotal * $couponCodeDetails[0])/100);
    }
}

function calculate_order_style_discount($productsInCart,$productids,$couponCodeDetails,$grandTotal)
{
	$discountamount =0;
if ($grandTotal < $couponCodeDetails[4])
                return 0;	
if (!empty($couponCodeDetails[17])&&$couponCodeDetails[17]!=0&&$grandTotal > $couponCodeDetails[17]&&$couponCodeDetails[1] == 'percent')
        return (($couponCodeDetails[17] * $couponCodeDetails[0])/100);
    if ($couponCodeDetails[1] == 'absolute'){
        return $couponCodeDetails[0];
    }
	
	foreach($productids AS $key=>$value)
	{
		
			$productDetail =   $productsInCart[$value];
			$grandTotal = $productDetail['productPrice']*$productDetail['quantity'];
			
		  if($couponCodeDetails[16] == $productDetail['productStyleId'])
		  {
		  	//print_r($grandTotal);
		  
		  	//no discount if total for this style is less than minimum order amount
		  	if ($grandTotal < $couponCodeDetails[4])
                return 0;
            //if maximum defined and total for this style greater than maximum then offer discount only for maximum     
		  	if (!empty($couponCodeDetails[17])&&$couponCodeDetails[17]!=0&&$grandTotal > $couponCodeDetails[17]){
		  	if ($couponCodeDetails[1] == 'absolute'){
					$discountamount = $discountamount+$couponCodeDetails[0];
				}elseif($couponCodeDetails[1] == 'percent'){
					$discountamount = $discountamount+(($couponCodeDetails[17] * $couponCodeDetails[0])/100);
						
				}
		  	}
		  	//if maximum not defined or total for this style < maximum then give discount on total for this style
		  	elseif(($couponCodeDetails[17]==0)||(!empty($couponCodeDetails[17])&&$grandTotal < $couponCodeDetails[17])){
				if ($couponCodeDetails[1] == 'absolute'){
					$discountamount = $discountamount+$couponCodeDetails[0];
				}elseif($couponCodeDetails[1] == 'percent'){
					$discountamount = $discountamount+(($grandTotal * $couponCodeDetails[0])/100);
						
				}
		  	}
		  }
		  /*else
		  {
			  if ($couponCodeDetails[1] == 'absolute'){
					$discountamount += $couponCodeDetails[0];
				}elseif($couponCodeDetails[1] == 'percent'){
					$discountamount += (($grandTotal * $couponCodeDetails[0])/100);
				}
		  }*/
		
	}
 //exit;
      return $discountamount;

    
}
function calculate_order_type_discount($productsInCart,$productids,$couponCodeDetails)
{
	$discountamount =0;
	$finalgrandtotal=0;
	global $weblog;
	
	
	foreach($productids AS $key=>$value)
	{
		
			$productDetail =   $productsInCart[$value];
			$grandTotal = $productDetail['productPrice']*$productDetail['quantity'];
			$typeid = $productDetail['producTypeId'];
			$weblog->info("tyeeeppee id >>>>> ".$typeid);
		    if($couponCodeDetails[18] == $typeid)
		    {
		  	  $finalgrandtotal=$finalgrandtotal+$grandTotal;
		  	
		     }
		  
		
	}
	// THIS IMPLIES THAT CART DOES NOT CONTAIN THE PRODUCTS OF SPECIFIED PRODUCT TYPE
	if($finalgrandtotal ==0 ) return 0;

	$weblog->info("finalgrandtotal >>>>> ".$finalgrandtotal);

	$weblog->info("max >>>>> ".$couponCodeDetails[17]);

   //no discount if total for this type is less than minimum order amount
		  	if ($finalgrandtotal < $couponCodeDetails[4])
                return 0;
            //if maximum defined and total for this type greater than maximum then offer discount only for maximum     
		  	if ((!empty( $couponCodeDetails[17] )&& $couponCodeDetails[17]!=0  )&&  $finalgrandtotal > $couponCodeDetails[17]){
		 		$weblog->info("INSIDEEEE >>>>>>>>>11 ");

				if ($couponCodeDetails[1] == 'absolute'){
						$discountamount = $couponCodeDetails[0];
					}elseif($couponCodeDetails[1] == 'percent'){
						$discountamount = (($couponCodeDetails[17] * $couponCodeDetails[0])/100);
							
					}
		  	}
		  	//if maximum not defined or total for this type < maximum then give discount on total for this type
		  	elseif(($couponCodeDetails[17]==0)||(($couponCodeDetails[17]!=0) && $finalgrandtotal < $couponCodeDetails[17])){
				$weblog->info("INSIDEEEE >>>>>>>>>22 ");

				if ($couponCodeDetails[1] == 'absolute'){
					$discountamount = $couponCodeDetails[0];
				}elseif($couponCodeDetails[1] == 'percent'){
					$discountamount = (($finalgrandtotal * $couponCodeDetails[0])/100);
						
				}
		  	}

      return $discountamount;

    
}
function calculate_product_only_discount($productsInCart,$productids,$couponCodeDetails){
    if (in_array($couponCodeDetails[2],$productids)){
        // TODO need to clarify the funda behind product title. Right now calculating on the basis of quantity.
        if ($couponCodeDetails[13] == 'Y'){
            //discount needs to be applied only once per order
            if ($couponCodeDetails[1] == 'absolute')
                return $couponCodeDetails[0];
            elseif ($couponCodeDetails[1] == 'percent')
                return (($productsInCart[$couponCodeDetails[2]]['productPrice'] * $couponCodeDetails[0])/100);
        }elseif ($couponCodeDetails[13] == 'N'){
            //discount needs to be applied per product item
            $count = $productsInCart[$couponCodeDetails[2]]['quantity'];
            if ($couponCodeDetails[1] == 'absolute')
                return ($couponCodeDetails[0] * $count);
            elseif ($couponCodeDetails[1] == 'percent')
                return ((($productsInCart[$couponCodeDetails[2]]['productPrice'] * $couponCodeDetails[0])/100) * $count);
        }
    }
    // no matching product found in cart. return zero discount
    return 0;
}

function calculate_category_only_discount($productsInCart,$productids,$couponCodeDetails){
    // check if the category chosen is recursive, if so get all the categories and subcategories
	//print_r($couponCodeDetails);
	  if ($couponCodeDetails[11] == 'Y'){
        $categoryArray = func_get_all_subcategories_for_category($couponCodeDetails[3]);
    }else{

        $categoryArray = array($couponCodeDetails[3]);
    }
    // get categories for all products in the cart
	  //  print_r($productids);

    $cartCategories = get_categories_for_cart($productids);
    $discount = 0.0;
    for($i=0;$i<count($cartCategories);$i++){
        if (in_array($cartCategories[$i][0],$categoryArray)){
            //discount is valid for product category

            if ($couponCodeDetails[12] == 'Y' && $couponCodeDetails[13]=='Y'){
                //discount is applied once per order
                if ($couponCodeDetails[1] == 'absolute'){
                    $discount +=  ($couponCodeDetails[0]);
                }elseif ($couponCodeDetails[1] == 'percent'){

                    $discount +=  ($couponCodeDetails[0] *$productsInCart[$cartCategories[$i][1]]['productPrice'])/100;
                }
            }elseif($couponCodeDetails[12] == 'N' && $couponCodeDetails[13]=='Y'){
                if ($couponCodeDetails[1] == 'absolute'){
                    $discount +=  ($couponCodeDetails[0]);
                }elseif ($couponCodeDetails[1] == 'percent'){
                    $discount +=  ($couponCodeDetails[0] *$productsInCart[$cartCategories[$i][1]]['productPrice'])/100;
                }
            }elseif($couponCodeDetails[12] == 'N' && $couponCodeDetails[13]=='N'){
                if ($couponCodeDetails[1] == 'absolute'){
                    $discount +=  ($productsInCart[$cartCategories[$i][1]]['quantity'] * $couponCodeDetails[0]);
                }elseif ($couponCodeDetails[1] == 'percent'){
                    $discount +=  ($productsInCart[$cartCategories[$i][1]]['quantity'] * $couponCodeDetails[0] *$productsInCart[$cartCategories[$i][1]]['unitPrice'])/100;
                }
            }
        }
    }
    return $discount;
}

function get_categories_for_cart($productids){
    $productIdsString = implode(',',$productids);
    return func_get_categories_for_products($productIdsString);
}
function calculate_discount_for_promotion($productsInCart,$productids,$couponCodeDetails){

	$num_of_prd_for_dis = 0;
	$discountamount = 0;
	$productPriceAmt = 0;
	foreach($productids as $_k => $_product){
		if(!empty($productsInCart[$_product]['promotion']) && is_valid_promotion($productsInCart[$_product]['promotion']['promotion_id'])){
			$num_of_prd_for_dis += $productsInCart[$_product]['quantity'];
			$productPriceAmt += $productsInCart[$_product]['productPrice'] * $productsInCart[$_product]['quantity'];
		}
	}
	if ($couponCodeDetails[1] == 'absolute'){
					$discountamount = $num_of_prd_for_dis * $couponCodeDetails[0];
	}elseif($couponCodeDetails[1] == 'percent'){
					$discountamount = (($productPriceAmt * $couponCodeDetails[0])/100);
					
	}
	
	return $discountamount;
	
}
function is_valid_promotion(){
	return false;
}

?>
