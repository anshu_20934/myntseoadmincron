<?php
include_once("./auth.php");

include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
$tracker = new BaseTracker(BaseTracker::ADD_TO_CART);

include_once("./include/func/func.randnum.php");
include_once("./include/func/func.mkmisc.php");
include_once("./include/func/func.mkspecialoffer.php");
include_once("./include/class/mcart/class.MCart.php");
include_once("./include/func/func.utilities.php");

include_once ($xcart_dir."/include/class/search/UserInterestCalculator.php");

require_once("$xcart_dir/include/class/search/SearchProducts.php");
include_once ("$xcart_dir/modules/discount/DiscountEngine.php");
include_once("$xcart_dir/utils/Helper.php");
include_once($xcart_dir."/myntra/setShowStyleDetails.php");
include_once(HostConfig::$documentRoot."/include/func/func.utilities.php");

use mcart\exception\ProductAlreadyInCartException;

$cartData = array();
$fromComboOverlay = false;
$newCheckoutCombo = false;
if(isset($_POST['comboProduct']) && $_POST['comboProduct'] == 1) {
    $newCheckoutCombo = true;
}



if(isset($_POST['fromComboOverlay']) && $_POST['fromComboOverlay'] == 1) {
    $fromComboOverlay = true;
    if(isset($_POST['cartPostData'])){
        $cartPostData = json_decode($_POST['cartPostData']);
    }
    foreach($cartPostData as $key=>$c){
        $cartDataTmp = array();
        $cartDataTmp['skuid'] =         	$c->skuid;
        $cartDataTmp['quantity'] =          abs($c->quantity);
        $cartDataTmp['sizequantityArray'] =	  array(0=>abs($cartDataTmp['quantity']));
	$cartDataTmp['itemAddedByDiscountEngine'] = $c->itemAddedByDiscountEngine;
        $cartData[] = $cartDataTmp;
    }
} else if (isset($_POST['fromBuyLook'])) {
    $skuIdsRaw = explode(',', $_POST['skuIds']);
    $cartData = array();
    foreach ($skuIdsRaw as $skuIdRaw) {
        $skuId = filter_var($skuIdRaw, FILTER_SANITIZE_NUMBER_INT);
        if (empty($skuId)) continue;
        $cartData[] = array(
            'skuid' => $skuId,
            'quantity' => 1,
            'sizequantityArray' => array(1)
        );
    }
} else {
    //Load product type,style and product id details
    $cartData[0]['skuid'] =         		  filter_input(INPUT_POST,"productSKUID", FILTER_SANITIZE_NUMBER_INT);
    $cartData[0]['quantity'] =               mysql_real_escape_string(filter_input(INPUT_POST,"quantity", FILTER_SANITIZE_NUMBER_INT));
    $cartData[0]['sizequantityArray'] =	  $_POST['sizequantity'];
    $cartData[0]['lead'] =	  				  filter_input(INPUT_GET,'lead', FILTER_SANITIZE_STRING);
    $cartData[0]['uq'] =                     filter_input(INPUT_POST,'uq', FILTER_SANITIZE_STRING);
    $cartData[0]['uqs'] =                    filter_input(INPUT_POST,'uqs', FILTER_SANITIZE_STRING);
    $cartData[0]['t_code'] =                 filter_input(INPUT_POST,'t_code', FILTER_SANITIZE_STRING);
}



$mcartFactory= new MCartFactory();
$mcart= $mcartFactory->getCurrentUserCart(true);

foreach($cartData as $key=>$cart){

    $skuid =         		  $cart['skuid'];
    $quantity =               $cart['quantity'];
    $sizequantityArray =	  $cart['sizequantityArray'];
    $lead =	  				  $cart['lead'];
    $uq =                     $cart['uq'];
    $uqs =                    $cart['uqs'];
    $t_code =                 $cart['t_code'];

    $is_customizable = 0;//      $thisChosenProductConfiguration['is_customizable'];
    $selectedQuantity;
    for($i=0;$i<count($sizequantityArray);$i++){
        if(!empty($sizequantityArray[$i])){
            $selectedQuantity = $sizequantityArray[$i];
            break;
        }
    }

    $selectedQuantity=mysql_real_escape_string($selectedQuantity);

    if($selectedQuantity=='') $selectedQuantity = "1";
    
    $mcartItem= new MCartNPItem(MCartUtils::genProductId(),$skuid,$selectedQuantity);
    $mcartItem->itemAddedByDiscountEngine = $cart['itemAddedByDiscountEngine']?$cart['itemAddedByDiscountEngine']:false;
    if(checkCSRF($_POST["_token"], "mkretrievedataforcart_re")){
    	try {
    		$mcart->addProduct($mcartItem);
    	} catch (ProductAlreadyInCartException $e) {
    		$status=0;
    	}
    }

    $productStyleId = $mcartItem->getStyleId();

    $pagetype = $_GET['pagetype'];
    $addTo = 'c';

    if(!empty($productStyleId))
    {
        UserInterestCalculator::trackAction($productStyleId, UserInterestCalculator::ADD_TO_CART);
    }
    if(!is_null($tracker->addToCartData))
    {
        $tracker->addToCartData->setCartItem(MCartUtils::convertCartItemToArray($mcartItem));
    }
    $tracker->fireRequest();
}
//new cart creation cookie is set when a new cart is created. 
//This is looked up when cart page is loaded to fire a ga call if a new cart was created.
if($mcart->isCartNew()){
	setMyntraCookie("ncc",$mcart->getCartCreateTimestamp());
}
MCartUtils::persistCart($mcart);

if($skin=='skin2' && !$fromComboOverlay){
	if(!isset($status)){
		$status=1;
	}
			//GET ALL DETAILS RELATED TO CART TO SHOW In CONFIRM BOX
	$smarty->assign('productStyleId',$productStyleId);
	$smarty->assign('sizeName',filter_input(INPUT_POST,'selectedsize',FILTER_SANITIZE_STRING));
	$discountData=DiscountEngine::getDataForAStyle('pdp', $productStyleId);
	//echo "<PRE>";print_r($discountData);
	$discountType = $discountData->discountType;
	$discountAmount = $discountData->discountAmount;
	$discountSetStyles = $discountData->discountToolTipText->relatedProducts;
	foreach($discountData->discountToolTipText->params as $k=>$v) {
    	$smarty->assign("dre_".$k, $v);
	}
	foreach($discountData->displayText->params as $k=>$v) {
    	$smarty->assign("dre_".$k, $v);
	}
	if($discountSetStyles!=null && count($discountSetStyles)>0 ) {
    	$smarty->assign("show_combo", true);
	}else{
    	$smarty->assign("show_combo", false);
	}
	$smarty->assign("discountamount", $discountAmount);
	$smarty->assign("discountId", $discountData->discountId);
	$smarty->assign("discounttype", $discountType);
	$smarty->assign("discountlabeltext", $_rst[$discountData->displayText->id]);
	$smarty->assign("discounttooltiptext", $_rst[$discountData->discountToolTipText->id]);
	$originalprice =$mcartItem->getProductPrice();
	$discountedprice = $originalprice;
	$finalprice  = ($discountedprice>0) ? $discountedprice : $originalprice;
	
	$smarty->assign("originalprice",$originalprice);
	$productName=filter_input(INPUT_POST,'productStyleLabel',FILTER_SANITIZE_STRING);
	$smarty->assign('productName',$productName);
	$smarty->assign('message','ADDED TO BAG');
	$smarty->assign('status',$status);
	$smarty->assign('itemsInCart',$mcart->getItemQuantity());
	$smarty->assign('totalAmount',$mcart->getCartSubtotal());
	$avail_params = array();
    $avail_session_id_content = $_COOKIE['__avail_session__'];
	if(!empty($avail_session_id_content)){
		$avail_session_id=$avail_session_id_content;
	}
	$avail_params[] = array("method"=>"logAddedToCart","params"=>array("ProductID"=>$productStyleId,"SessionID"=>$avail_session_id));
	
	
	//No remove option in the overlay
	if(!empty($_GET['product'])){
		$productsCount = filter_input(INPUT_GET,'product', FILTER_SANITIZE_STRING);
	}
	if(!empty($_GET['uq'])){
		$smarty->assign("uq",filter_input(INPUT_GET,'uq', FILTER_SANITIZE_STRING));
	}
	if(!empty($_GET['uqs'])){
		$smarty->assign("uqs",filter_input(INPUT_GET,'uqs', FILTER_SANITIZE_STRING));
	}
	if(!empty($_GET['t_code'])){
		$smarty->assign("t_code",filter_input(INPUT_GET,'t_code', FILTER_SANITIZE_STRING));
	}
    //availswitchoff    
	//$url = Helper::getPropertyValue('recoServiceUrl');
	//post_async(HostConfig::$selfHostURL."/asyncGetPostHandler.php",array("url"=>$url,"method"=>"POST","data"=>json_encode($avail_params),"user"=>$user));
	//END AVAIL
    if($newCheckoutCombo){
        echo json_encode(array("status"=>"success",
            "message" => "NEW COMBO ADDED TO CART, time to get combo details here",
            "total"=>$mcart->getCartSubtotal(),
            "count"=>$mcart->getItemQuantity()
            ));
    }
    else{
	   header("location:$cartPageUrl?pname=".urlencode($productName) . "&productCountinCart=". (count($mcart->getProducts()) -1) . "&productAdded=$productStyleId");
    }
}
else {
	if(!defined("CART_PROXY")){
    	if($fromComboOverlay){
        	echo "Success";
    	}
        else{
       		header("location:mkmycartmultiple_re.php?at=".$addTo."&pagetype=".$pagetype.'&windowid='.$windowid.'&product='.(count($mcart->getProducts()) -1).'&lead='.$lead.'&av_id_added='.$productStyleId.'&uq='.$uq.'&uqs='.$uqs.'&t_code='.$t_code);
    	}

    }
}
?>
