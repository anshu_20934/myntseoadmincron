<?php
################ LOGGERS INITIALIZED  #################

define('MYNTRA_HOME_DIR',$xcart_dir);
define('LOG4PHP_DIR', MYNTRA_HOME_DIR."/log4php");
define('LOG4PHP_CONFIGURATION', MYNTRA_HOME_DIR."/myntralogger.properties");
define('LOG4PHP_CONFIGURATOR_CLASS',LOG4PHP_DIR."/LoggerPropertyConfigurator");

include_once(LOG4PHP_DIR . '/LoggerManager.php');

global $logger_manager, $weblog, $errorlog, $sqllog, $debuglog, $apilog, $portalapilog, $solrlog, $maillog, $fraudOrderLog, $reportlog, $sessionlog, $drelog, $seolog;

$logger_manager     = new LoggerManager();
$weblog             = $logger_manager->getLogger('MYNTRAWEBLOGGER');
$errorlog           = $logger_manager->getLogger('MYNTRAERRORLOGGER');
$sqllog             = $logger_manager->getLogger('MYNTRASQLLOGGER');
$debuglog           = $logger_manager->getLogger('MYNTRADEBUGLOGGER');
$apilog             = $logger_manager->getLogger('WMSAPILOGGER');
$portalapilog		= $logger_manager->getLogger('PORTALAPILOGGER');
$solrlog            = $logger_manager->getLogger('SOLRLOGGER');
$maillog 			= $logger_manager->getLogger('MAILLOGGER');
$fraudOrderLog 		= $logger_manager->getLogger('FRAUDORDERLOGGER');
$reportlog 			= LoggerManager::getLogger('REPORTLOGGER');
$sessionlog         = $logger_manager->getLogger('SESSIONSERVICELOGGER');
$drelog             = $logger_manager->getLogger('DRELOGGER');
$authLogger         = $logger_manager->getLogger('MYNTRAAUTHLOGGER');
$paymentLogger      = $logger_manager->getLogger('MYNTRAPAYMENTLOGGER');
$seolog             = $logger_manager->getLogger('MYNTRASEOLOGGER');

## usage ###

## $errorlog SHOULD ONLY BE USED FOR ERROR LOGS !!!
## $errorlog->error($errormsg);

## For all info and debug log will go into #####
## $weblog->info($msg);
## $weblog->debug($msg);
## For ALL SQL LOG
## $sqllog->debug($sql);

###################################################################
?>
