<?php
require "./auth.php";
include_once $xcart_dir."/include/func/func.returns.php";
include_once $xcart_dir."/include/class/class.mymyntra.php";
require_once $xcart_dir."/include/func/func.core.php";
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");

if($_SERVER['REQUEST_METHOD'] == 'POST') {
	$action = $_POST['action'];
	
	if($action == 'newaddress') {
		$name = mysql_real_escape_string($_POST['name']);
		$address = mysql_real_escape_string($_POST['address']);
		$city = mysql_real_escape_string($_POST['city']);
		$state = mysql_real_escape_string($_POST['state']);
		$country = mysql_real_escape_string($_POST['country']);
		$zipcode = mysql_real_escape_string($_POST['zipcode']);
		$mobile = mysql_real_escape_string($_POST['mobile']);
		$email = mysql_real_escape_string($_POST['email']);
		$phone = mysql_real_escape_string($_POST['phone']);
		$defaultAddId = mysql_real_escape_string($_POST['defaultAddId']);
		
		if($defaultAddId && $defaultAddId != "") {
			$update_array = array('default_address'=>0);
			func_array2update("mk_customer_address", $update_array, 'id='.$defaultAddId);
		}
		
		$insertdata = array('login'=>$XCART_SESSION_VARS['login'], 'default_address'=>true, 'name'=>$name, 'address'=>$address, 'city'=>$city, 'state'=>$state, 'country'=>$country,
							'pincode'=>$zipcode, 'email'=>$email, 'mobile'=>$mobile, 'phone'=>$phone, 'datecreated'=>date("Y-m-d H:i:s"));
		$new_add_id = func_array2insert("mk_customer_address", $insertdata);
		
		$data = array("SUCCESS", $new_add_id);
	}
	
	header('Content-type: text/x-json');
	print json_encode($data);
} else {

	$orderid = (int)$_GET['orderid'];
	$login = mysql_real_escape_string($login);
	if(empty($login) || empty($orderid)){
	    // If user is not logged in send him to login/register page ..
	    func_header_location($http_location."/register.php");
	}
	
	$itemid = (int)$_GET['itemid'];
	
	$item_return_sql = "Select returnid from $sql_tbl[returns] where itemid = '$itemid'";
	$return_req = func_query_first($item_return_sql);
	
	if(!$return_req) {
		$mymyntra = new MyMyntra($login);
		
		$itemsql = "SELECT a.*, a.completion_time AS completion_time, d.name AS stylename, f.login, " .
		           "f.status as status, f.queueddate AS queueddate, " .
		           "a.quantity_breakup, a.assignment_time AS assignment_time, f.payment_method as paymentmethod, " .
		           "prd.image_portal_t as image_portal_t, sp.default_image " .
		           "FROM ((((xcart_order_details a JOIN mk_product_style d ON a.product_style=d.id) " .
		           "LEFT JOIN mk_style_properties sp on d.id=sp.style_id) " .
		           "JOIN xcart_products prd on a.productid=prd.productid) " .
		           "LEFT JOIN xcart_orders f ON a.orderid=f.orderid) where a.item_status != 'IC' AND f.orderid = '$orderid' AND a.itemid = '$itemid'";
		
		$singleitem = func_query_first($itemsql);
		if(!empty($singleitem['default_image'])) {
			// Rectangular thumbnails
			$singleitem['default_image'] = str_replace("_images", "_images_96_128", $singleitem['default_image']);
		}
		
		$selected_option = $_GET['item_option'];
		
		$optionsQty = $mymyntra->getBreakUpQuantityArray($singleitem['quantity_breakup']);
		foreach ($optionsQty as $option) {
			if($option['option'] == $selected_option) {
				$singleitem['option'] = $option['option'];
				$singleitem['size'] = $option['size'];
				$singleitem['quantity'] = $option['quantity'];
				break;
			}
		}
		
		$pickup_charges = WidgetKeyValuePairs::getWidgetValueForKey('returnPickupCharges');
		$selfDeliveryCredit = WidgetKeyValuePairs::getWidgetValueForKey('returnSelfDeliveryCredit');
		
		$selected_address_id = intval($_GET['address']);
		$weblog->info("Selected addressId - $selected_address_id");
		
		$address_sql = "Select * from mk_customer_address where login='$login' order by default_address DESC";
		$addresses = func_query($address_sql);
		
		$selectedAddress = false;
		$defaultAddress = false;
		foreach ($addresses as $index=>$address) {
			$addresses[$index]['state_display'] = func_get_state($address['state'], $address['country']);
			$addresses[$index]['country_display'] = func_get_country($address['country']);
			$couriers = func_check_pickup_availability($address['pincode'], $singleitem['product_style']);
			if($couriers) {
				$addresses[$index]['pickup'] = true;
			}
			
			if($selected_address_id != 0 && $selected_address_id == $address['id'] ) {
				$selectedAddress = $addresses[$index];
			} 
			
			if($address['default_address'] == 1) {
				$defaultAddress = $addresses[$index];
			}
		}
		
		if($selectedAddress === false) {
			$selectedAddress = $defaultAddress;
			$selected_address_id = $defaultAddress['id'];
		}
		
		$return_mode = $_GET['returnmode'];
		if(!$return_mode || $return_mode == "")
			$return_mode = "self";
		
		
		$countries = func_get_countries();
		$india_states = func_get_states('IN');
		
		$breadCrumb=array(
			array('reference'=>'home.php','title'=>'home'),
			array('reference'=>'mymyntra.php','title'=>'My Myntra'),
			array('reference'=>'mkreturn.php','title'=>'Return Item'),
		);
		
		$return_reasons = get_all_return_reasons(true);
		
		x_session_register('new_return_request', true);
		
		$smarty->assign("action", $action);
		$smarty->assign("orderid", $orderid);
		$smarty->assign("order", $order);
		$smarty->assign("singleitem", $singleitem);
		$smarty->assign("breadCrumb",$breadCrumb);
		$smarty->assign("states", $india_states);
		$smarty->assign("countries", $countries);
		$smarty->assign("pickupcharges", $pickup_charges);
		$smarty->assign("selfDeliveryCredit", $selfDeliveryCredit);
		$smarty->assign("selectedAddressId", $selected_address_id);
		$smarty->assign("defaultAddressId", $defaultAddress['id']);
		$smarty->assign("selectedAddress", $selectedAddress);
		$smarty->assign("returnMode", $return_mode);
		$smarty->assign("allAddresses", $addresses);
		$smarty->assign("reasons", $return_reasons);
	} else {
		$smarty->assign("return_id", $return_req['returnid']);
	}
	func_display("mkreturn.tpl", $smarty);
}
?>