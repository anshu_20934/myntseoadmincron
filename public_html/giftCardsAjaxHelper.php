<?php

include_once("./auth.php");
include_once (dirname(__FILE__)."/../RestAPI/RestRequest.php");
include_once $xcart_dir."/include/func/func.mail.php";
require_once($xcart_dir."/modules/giftcards/GiftCardsHelper.php");
include_once "$xcart_dir/include/class/class.mymyntra.php";
include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
include_once $xcart_dir."/include/func/func.db.php";


global $telesales;
if ($telesales){
    exit; 
}
$type = filter_input(INPUT_GET,'type',FILTER_SANITIZE_STRING);
$mode = filter_input(INPUT_GET,'mode',FILTER_SANITIZE_STRING);
$id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_STRING);
$orderId = filter_input(INPUT_GET,'orderId',FILTER_SANITIZE_STRING);
$comments = filter_input(INPUT_GET,'comments',FILTER_SANITIZE_STRING);
$email_uuid = filter_input(INPUT_GET,'gc',FILTER_SANITIZE_STRING);
$pin = filter_input(INPUT_GET,'pin',FILTER_SANITIZE_STRING);
$card = filter_input(INPUT_GET,'card',FILTER_SANITIZE_STRING);
$login_to_activate_on = filter_input(INPUT_GET,'login_to_activate',FILTER_SANITIZE_STRING);
$is_admin = filter_input(INPUT_GET,'is_admin',FILTER_SANITIZE_STRING);
$adminUser = filter_input(INPUT_GET,'adminuser',FILTER_SANITIZE_STRING);
$cardNumber = filter_input(INPUT_POST,'cardNumber',FILTER_SANITIZE_STRING);
$cardPin = filter_input(INPUT_POST,'pinNumber',FILTER_SANITIZE_STRING);
$postType = filter_input(INPUT_POST,'type',FILTER_SANITIZE_STRING);

if(!empty($login_to_activate_on)){
	$login = $login_to_activate_on;
}

if ($postType == "balance"){
	$result = \GiftCardsHelper::getGiftCardBalance($postType, null, $cardPin, null, $cardNumber);
	echo json_encode($result);
	exit;
}

$couponAdapter = CouponAdapter::getInstance();
if($mode == "activate"){
	if($type == "id"){
		$result = \GiftCardsHelper::getGiftCardInfoForActivation($type, null, null, $id);		
	}
	elseif ($type == "gc"){
		$result = \GiftCardsHelper::getGiftCardInfoForActivation($type, $email_uuid, null, null);
	}
	elseif ($type == "pin"){
        if(filter_input(INPUT_GET, 'captcha', FILTER_SANITIZE_STRING) != 
            $XCART_SESSION_VARS['captchasAtMyntra']["giftCaptcha"]) {
            $msg = array();
            $msg['type'] = "captcha";
            $msg['status'] = "failed";
            $msg['message'] = "WRONG CODE ENTERED";
            echo json_encode($msg);
            exit;
        } else {
            $XCART_SESSION_VARS['captchasAtMyntra']["giftCaptcha"] = "";
        }
		$result = \GiftCardsHelper::getGiftCardInfoForActivation($type, null, $pin, null, $card);
	}

	if($result->status != "failed"){
        if (!empty($result->giftCard)) {
		    $giftCardOrder = $result;
            $giftCard = $giftCardOrder->giftCard;
            
            $activationResponse = logGiftCardActivation($login,$giftCard->id, $type, $giftCard->amount, $giftCard->amount);
            
            if(!empty($activationResponse) && $activationResponse->status === "success"){
	    		$trs = new MyntCashTransaction($login, 
	                                            MyntCashItemTypes::ORDER,
	                                            $giftCardOrder->orderId,
	                                            MyntCashBusinessProcesses::GIFT_CARD,
	                                            0,
	                                            $giftCard->amount,
	                                            0, 
	                                            "Cashback credit for giftcard from " . $giftCard->senderName . " (" . $giftCard->senderEmail . ")");
	
	            MyntCashService::creditToUserMyntCashAccount($trs);
	            $myntCashBalance = MyntCashService::getUserMyntCashDetails($login);
	           
	            $commentArrayToInsert = Array ("orderid" => $orderId,
	                    "commenttype" => "Automated",
	                    "commenttitle" => "Gift Card Activated",
	                    "commentaddedby" => $adminUser,
	                    "description" => "The Gift card id ".$id." is activated: ".$comments,
	                    "commentdate" => time());
	            func_array2insert("mk_ordercommentslog", $commentArrayToInsert);
            }
            echo json_encode($activationResponse);
            exit;
            
        } else if (!empty($result->giftBig)) {
		    $giftBigTransaction = $result;
    		$trs = new MyntCashTransaction($login,
                                            MyntCashItemTypes::GIFT_VOUCHER,
                                            $giftBigTransaction->giftBig->transactionId,
                                            MyntCashBusinessProcesses::GIFT_BIG,
                                            0,
                                            $giftBigTransaction->giftBig->amount,
                                            0,
                                            "Cashback credit for the voucher: ".MyntCashBusinessProcesses::GIFT_BIG);
            MyntCashService::creditToUserMyntCashAccount($trs);
            $result = new stdClass();
            $result->status = "success";
            $result->message = "Gift Voucher Activated. <br/>Amount added to Your <a href='/mymyntra.php?view=mymyntcredits#mycashback'>CashBack</a>";
            echo json_encode($result);
        }
	}
	else{
		echo json_encode($result);
		exit;
	}
}
elseif ($mode == "sendemail"){
	if(!empty($is_admin) && $is_admin === "true"){
		$result = \GiftCardsHelper::sendEmailForGiftCard($id, 'true');	
	}
	else{
		$result = \GiftCardsHelper::sendEmailForGiftCard($id);
	}
	if($result->status != "failed"){
		$commentArrayToInsert = Array ("orderid" => $orderId,
				"commenttype" => "Automated",
				"commenttitle" => "Mail Sent",
				"commentaddedby" => $adminUser,
				"description" => "The Mail is sent for Gift card id ".$id.": ".$comments,
				"commentdate" => time());
		func_array2insert("mk_ordercommentslog", $commentArrayToInsert);
	}
	echo json_encode($result);
	exit;		
}
elseif ($mode == "ordercomplete"){
	$result = \GiftCardsHelper::setGiftCardOrderComplete($id);
	if($result->status != "failed"){
		$commentArrayToInsert = Array ("orderid" => $orderId,
				"commenttype" => "Automated",
				"commenttitle" => "Gift Order Completed",
				"commentaddedby" => $adminUser,
				"description" => "The Order is completed for Gift card id ".$id.": ".$comments,
				"commentdate" => time());
		func_array2insert("mk_ordercommentslog", $commentArrayToInsert);
	}
	echo json_encode($result);
	exit;
}
else{
	$result = new stdClass();
	$result->status = "failed";
	$result->message = "Error occured. Please try again later.";
	echo json_encode($result);
	exit;
}

function logGiftCardActivation($login, $id, $type, $amount, $cashback){	
	$result =new stdClass();
	$giftCardLog = new stdClass();
	$giftCardLog->activatedBy = $login;
	$giftCardLog->giftCardId = $id;
	$giftCardLog->activatedOn = time();
	$retval=\GiftCardsHelper::logGiftCardActivation($giftCardLog);
	
	$response = "";
	if(!empty($retval) && $retval->status != "failed"
			&& $retval->status == "success"
					&& !empty($retval->giftCardLogId)
	){
		if($type == "pin"){
			$result->status = "success";
			$result->message = "Gift Card Activated. <br/>Gift Amount added to Your <a href='/mymyntra.php?view=mymyntcredits#mycashback'>CashBack</a>";
			$response =$result;
	
		}
		elseif($type == "id"){
			$result->status = "success";
			$result->message = "Activated on ". date('%d %F, %Y',$giftCardLog->activatedOn) . "<br/>Gift Amount added to Your <a href='/mymyntra.php?view=mymyntcredits#mycashback'>CashBack</a>"
					. "<br/> Your Cashback Balance : Rs. " . $cashback;
			$response = $result;
	
		}
		else{
			$result->status = "success";
			$result->message = $amount . "*" . $cashback;
			$response = $result;
		}
	}
	else{
		$response = $retval;
	}
	
	return $response;
	
}


?>
