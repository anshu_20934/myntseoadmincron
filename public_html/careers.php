<?php
require "./auth.php";
if ($skin == 'skin2') {
    header('Location: careers#top');
    exit;
}

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setStaticPageVars(AnalyticsBase::CAREERS);
$analyticsObj->setTemplateVars($smarty);

func_display("careers.tpl",$smarty);
?>