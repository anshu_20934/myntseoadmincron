<?php

require_once './admin/auth.php';
require_once "$xcart_dir/modules/RestAPI/RestUtils.php";
require_once "$xcart_dir/modules/RestAPI/PermissionUtils.php";
require_once "$xcart_dir/modules/Registration/registration_module.php";

$data = RestUtils::processRequest();

switch($data->getMethod())
{
	case 'get':
		// retrieve the data
		$params = $data->getRequestVars();
		if(!isset($params['username'])){
			RestUtils::sendResponse(501);
		}
		
		$username = $params['username'];
		$password = $params['password'];
		
		try{
			$userData = PermissionUtils::authenticateUser($username, $password);
		} catch(InvalidArgumentException $e){
			RestUtils::sendResponse(404);
		}
		
		if($userData==null){
			RestUtils::sendResponse(401);
		}
		
		$toReturn = array();
		$toReturn["user_data"] = $userData;
		$permissions = PermissionUtils::getUserPermission($username);
		$toReturn["permissions"] = $permissions;
		//$permissions = PermissionUtils::getUserRoleName($username);
		
		if($data->getHttpAccept() == 'json')
		{
			RestUtils::sendResponse(200, json_encode($toReturn), 'application/json');
		}
		else if ($data->getHttpAccept() == 'xml')
		{	
			RestUtils::sendResponse(501);
		}

		break;
		
	case 'post':
		require_once $xcart_dir."/include/security.php";
		try {
			$permissions = PermissionUtils::getPermissionsFromRids($data->getData());
			$comma_separated = implode(",", $permissions);
		} catch(InvalidArgumentException $e){
			RestUtils::sendResponse(404);
		}
		RestUtils::sendResponse(200, json_encode($comma_separated), 'application/json');
		
		break;
	// etc, etc, etc...
}

?>