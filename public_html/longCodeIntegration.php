<?php
include_once("./auth.php");
include_once "$xcart_dir/modules/coupon/mrp/SmsVerifier.php";
include_once "$xcart_dir/include/func/func.order.php";
include_once "$xcart_dir/modules/apiclient/TripApiClient.php";
include_once "$xcart_dir/include/func/func.sms_alerts.php";

$sender = $_GET["Sender"];
$text = trim($_GET["Message_Text"]);
$timeRecieved = $_GET["Time_Stamp"];
//$operator =  $_GET["Operator"];
//$circle =  $_GET["Circle"];
//$vmn =  $_GET["To"];

if (empty($sender) || (strlen($sender) < 10))
        die("Error in sender's mobile field.");

if (empty($text))
        die("Text Field is empty");

switch(strtolower($text)) {
	case "verify":
		$mobile = substr($sender, -10);
		$arr = explode(" ",$timeRecieved);
		$date = explode("-",$arr[0]);
		$time = explode(":",$arr[1]);
		$timestamp = mktime($time[0],$time[1],$time[2],$date[1],$date[2],$date[0]);
		
		/*global $weblog;
		$weblog->info(basename(__FILE__).": Details Follow:");
		$weblog->info(basename(__FILE__).$mobile);
		$weblog->info(basename(__FILE__).$text);
		$weblog->info(basename(__FILE__).$timeRecieved);*/
		
		/*
		$weblog->info(basename(__FILE__).$operator);
		$weblog->info(basename(__FILE__).$circle);
		$weblog->info(basename(__FILE__).$vmn);
		$weblog->info(basename(__FILE__).": Details eneded");
		*/
		$smsVerifier = SmsVerifier::getInstance();
		$smsVerifier->updateViaCellNext($mobile, $timestamp);
		break;
	default:
		$orderid = $text;
		$mobile = substr($sender, -10);
		$weblog->info("Update order as delivered - $orderid , $sender");
        if(isInteger($orderid)) {
			$response = TripApiClient::updateLocalDeliveryTripStatus($orderid,$mobile);
			if(strtolower($response['status']) == 'success') {
				//func_change_order_status($orderid, 'DL', $mobile, 'Marking Order Delivered via mobile interface', "", false);
				func_addComment_order($orderid, $mobile, 'Automated', 'Mobile Update', 'Marking Order Delivered via mobile interface');
			}else if(strtolower($response['status']) == 'failure'){
				$weblog->info("Sending delivery (orderId:$orderid) update failure SMS to TDA. Msg: ". $response['reason']);
				func_send_sms($mobile, $response['reason']);
			}
			$weblog->info("Update Trip Status Response - ". print_r($response, true));
		}else
			die("Text Field is not correct");
		
}

?>
