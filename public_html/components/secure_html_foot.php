<?php
$analyticsOmnitureStatus = FeatureGateKeyValuePairs::getBoolean('omniture.enabled');
$smarty->assign("analyticsStatus", $analyticsOmnitureStatus);
$smarty->assign("analytics_account", AnalyticsConfig::$analyticsAccount);

$analyticsScript = $analyticsOmnitureStatus && empty($noInpageAnalytics) ? $smarty->fetch('inc/analytics_data_collection.tpl') : '';
?>

<?php foreach($smarty->getTemplateVars('jsFiles') as $url) { ?>
<script src="<?php echo $url ?>"></script>
<?php } ?>
<!--[if lt IE 8]>
    <script src="<?php echo HostConfig::$secureCdnBase ?>/skin2/js/json2.js"></script>
<![endif]-->

<script>
<?php echo abtest\MABTest::getInstance()->getGACallsString() ?>

_gaq.push(['_trackPageview']);
_gaq.push(['_trackPageLoadTime']);
</script>

<?php
echo $analyticsScript;
echo $newrelic_footer;
echo $tracelytics_footer;
?>
