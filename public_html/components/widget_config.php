<?php

$widgetConfig = array();
$widgetsConfFile = $xcart_dir.'/env/widgets.json';
if (!file_exists($widgetsConfFile)) {
    $errorlog->error("Checkout: Widgets conf file: '$widgetsConfFile' is missing");
}
else {
    $jsonContent = file_get_contents($widgetsConfFile);
    $jsonConfig = json_decode($jsonContent, true);

    $cdnBase = (isset($_SERVER['HTTPS'])) ? HostConfig::$secureCdnBase: HostConfig::$cdnBase;
    $minBase = (isset($_SERVER['HTTPS'])) ? 'https://'.HostConfig::$httpsHost : 'http://'.HostConfig::$httpHost;

    foreach ($jsonConfig as $name => $widget) {
        if (HostConfig::$iscdn) {
            $path = $widget['zip'];
            $widgetConfig[$name] = "$cdnBase/$path";
        }
        else {
            $path = $widget['min'];
            $widgetConfig[$name] = "$minBase/$path";
        }
    }
}

$widgetConfigJson = json_encode($widgetConfig);
//header('Content-Type:text/plain'); print_r($widgetConfig); exit;

