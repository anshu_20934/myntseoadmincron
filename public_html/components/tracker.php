<?php

require_once __DIR__.'/../auth.php';

$page = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_STRING);
$analyticsObj = AnalyticsBase::getInstance();

if ($page == 'payment') {
    $analyticsObj->setContextualPageData(AnalyticsBase::PAYMENT_PAGE);
    $analyticsObj->setPaymentEventVars();
}
else if ($page == 'cart') {
    $analyticsObj->setContextualPageData(AnalyticsBase::CART_PAGE);
    $productCountinCart = filter_input(INPUT_GET, 'productCountinCart', FILTER_SANITIZE_STRING);
    $productAdded = filter_input(INPUT_GET, 'productAdded', FILTER_SANITIZE_STRING);
    if (!empty($productAdded)) {
        $analyticsObj->setCartEventVars($productCountinCart, $productAdded);
        $smarty->assign('productAdded', $productAdded);
    }
}
else if ($page == 'address') {
    $analyticsObj->setContextualPageData(AnalyticsBase::CHECKOUT_SHIPPING);
    $analyticsObj->setChooseShippingAddressEventVars();
}
else {
    exit;
}

$analyticsObj->setTemplateVars($smarty);
$analyticsOmnitureStatus = FeatureGateKeyValuePairs::getBoolean('omniture.enabled');
$smarty->assign("analyticsStatus", $analyticsOmnitureStatus);
$smarty->assign("analytics_account", AnalyticsConfig::$analyticsAccount);

if ($analyticsOmnitureStatus) {
    $smarty->assign('fireSyncOmnitureCall', true);
    $smarty->display('inc/analytics_data_collection.tpl');
}


