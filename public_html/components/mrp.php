<?php

require_once $xcart_dir."/modules/coupon/MRPConstants.php";

const COUPONCONFIGURATION_CACHEKEY = "couponconfiguration_xcache";
$mrpCouponConfiguration = $xcache->fetchAndStore(function() {
global $mrpDefaultCouponConfiguration;
$couponConfiguration['mrp_firstLogin_nonfbreg_numCoupons'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.nonfbreg.numCoupons", $mrpDefaultCouponConfiguration['mrp_firstLogin_nonfbreg_numCoupons']);
$couponConfiguration['mrp_firstLogin_nonfbreg_mrpAmount'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.nonfbreg.mrpAmount", $mrpDefaultCouponConfiguration['mrp_firstLogin_nonfbreg_mrpAmount']);
$couponConfiguration['mrp_firstLogin_nonfbreg_validity'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.nonfbreg.validity", $mrpDefaultCouponConfiguration['mrp_firstLogin_nonfbreg_validity']);
$couponConfiguration['mrp_firstLogin_nonfbreg_minCartValue'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.nonfbreg.minCartValue", $mrpDefaultCouponConfiguration['mrp_firstLogin_nonfbreg_minCartValue']);

$couponConfiguration['mrp_firstLogin_fbreg_numCoupons'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.fbreg.numCoupons", $mrpDefaultCouponConfiguration['mrp_firstLogin_fbreg_numCoupons']);
$couponConfiguration['mrp_firstLogin_fbreg_mrpAmount'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.fbreg.mrpAmount", $mrpDefaultCouponConfiguration['mrp_firstLogin_fbreg_mrpAmount']);
$couponConfiguration['mrp_firstLogin_fbreg_validity'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.fbreg.validity", $mrpDefaultCouponConfiguration['mrp_firstLogin_fbreg_validity']);
$couponConfiguration['mrp_firstLogin_fbreg_minCartValue'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.fbreg.minCartValue", $mrpDefaultCouponConfiguration['mrp_firstLogin_fbreg_minCartValue']);


$couponConfiguration['mrp_refFirstPurchase_numCoupons'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.numCoupons", $mrpDefaultCouponConfiguration['mrp_refFirstPurchase_numCoupons']);
$couponConfiguration['mrp_refFirstPurchase_mrpAmount'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.mrpAmount", $mrpDefaultCouponConfiguration['mrp_refFirstPurchase_mrpAmount']);
$couponConfiguration['mrp_refFirstPurchase_validity'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.validity", $mrpDefaultCouponConfiguration['mrp_refFirstPurchase_validity']);
$couponConfiguration['mrp_refFirstPurchase_minCartValue'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.minCartValue", $mrpDefaultCouponConfiguration['mrp_refFirstPurchase_minCartValue']);


$couponConfiguration['mrp_refRegistration_numCoupons'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.numCoupons", $mrpDefaultCouponConfiguration['mrp_refRegistration_numCoupons']);
$couponConfiguration['mrp_refRegistration_mrpAmount'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.mrpAmount", $mrpDefaultCouponConfiguration['mrp_refRegistration_mrpAmount']);
$couponConfiguration['mrp_refRegistration_validity'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.validity", $mrpDefaultCouponConfiguration['mrp_refRegistration_validity']);
$couponConfiguration['mrp_refRegistration_minCartValue'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.minCartValue", $mrpDefaultCouponConfiguration['mrp_refRegistration_minCartValue']);

$couponConfiguration['cust_survey_validity'] =
	FeatureGateKeyValuePairs::getInteger("mrp.cust.survey.validity", $mrpDefaultCouponConfiguration['cust_survey_validity']);
$couponConfiguration['cust_survey_mrpAmount'] =
	FeatureGateKeyValuePairs::getInteger("mrp.cust.survey.mrpAmount", $mrpDefaultCouponConfiguration['cust_survey_mrpAmount']);
$couponConfiguration['cust_survey_minCartValue'] =
	FeatureGateKeyValuePairs::getInteger("mrp.cust.survey.minCartValue", $mrpDefaultCouponConfiguration['cust_survey_minCartValue']);

	return $couponConfiguration;
}, array(), COUPONCONFIGURATION_CACHEKEY);

// Amount earnable by mrp program - to be displayed on the menu strip
$menuDisplayRefAmount = $mrpCouponConfiguration['mrp_refFirstPurchase_numCoupons'] * $mrpCouponConfiguration['mrp_refFirstPurchase_mrpAmount']
  	 			+ $mrpCouponConfiguration['mrp_refRegistration_numCoupons'] * $mrpCouponConfiguration['mrp_refRegistration_mrpAmount'];
$mrp_fbCouponValue = $mrpCouponConfiguration['mrp_firstLogin_fbreg_numCoupons'] * $mrpCouponConfiguration['mrp_firstLogin_fbreg_mrpAmount'];
$mrp_nonfbCouponValue = $mrpCouponConfiguration['mrp_firstLogin_nonfbreg_numCoupons'] * $mrpCouponConfiguration['mrp_firstLogin_nonfbreg_mrpAmount'];

if ($smarty) {
    $smarty->assign("mrp_fbCouponValue",$mrp_fbCouponValue);
    $smarty->assign("mrp_nonfbCouponValue",$mrp_nonfbCouponValue);
    $smarty->assign("mrp_nonfbNumCoupons",$mrpCouponConfiguration['mrp_firstLogin_nonfbreg_numCoupons']);
    $smarty->assign("mrp_nonfbMrpAmount",$mrpCouponConfiguration['mrp_firstLogin_nonfbreg_mrpAmount']);
}

