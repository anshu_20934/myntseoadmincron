<?php
include_once "$xcart_dir/include/func/func.loginhelper.php";
include_once $xcart_dir.'/modules/RestAPI/RestRequest.php';

$customerSupportCall = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
$customerSupportTime = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportTime');
$newCheckoutTopCpsMessage = WidgetKeyValuePairs::getWidgetValueForKey('newCheckout.topCpsMessage');
if($newCheckoutTopCpsMessage != NULL) {
    $newCheckoutTopCpsMessage = str_replace("system_free_shipping_amount", $system_free_shipping_amount, $newCheckoutTopCpsMessage);
}


?>
<header class="page-header">
<?php
   global $telesales;
   global $telesalesUser;
   if( $telesales){
?>
<script>
   var telesales = true;
</script>
   <div style="color: #fff;background-color: #333333 ;text-transform:none;padding:18px 0px;">Telesales User: <b><?php echo $telesalesUser;?></b></div>
<?php
    }
?>

    <div class="user-info">
        <span class="signin-status">
            <a href="#login" class="login <?php if (isGuestCheckout() || !empty($login)) echo 'hide' ?>" id="login-trigger">Login / Sign Up</a>
            <span id="login-welcome" class="welcome <?php if (empty($login)) echo 'hide' ?>">
                Welcome, <strong class="user-name">
                <?php echo $XCART_SESSION_VARS['userfirstname'] ? $XCART_SESSION_VARS['userfirstname'] : $login ?></strong>!
            </span>
            <form id="form-logout" method="post" class="form-logout <?php if (empty($login)) echo 'hide' ?>"
                    action="https://<?php echo HostConfig::$httpsHost ?>/include/login.php">
                <input type="hidden" name="mode" value="logout">
                <input type="hidden" name="_token" value="<?php echo $XCART_SESSION_VARS['USER_TOKEN'] ?>">
                <button type="submit" class="link-btn">Logout</button>
            </form>
        </span>
        <span class="promise"><span class="icon-lock"></span> 256-Bit Secure Checkout</span>
        <span class="promise-msg"><?php echo $newCheckoutTopCpsMessage ?><span>
    </div>
    <div class="details clearfix">
        <div class="help-info">
            <span class="lbl">Need Help?</span>
            <span class="phone-number"><span class="icon-phone"></span> <?php echo $customerSupportCall ?></span>
        </div>
        <a class="logo" href="http://<?php echo HostConfig::$httpHost ?>" alt="Myntra Logo">
            <img src="<?php echo $cdnBase ?>/skin2/images/MyntraLogo.png"></a>
        <ol class="checkout-steps" id="checkout-steps">
            <li class="step step1 active"><a href="<?php echo $cartPageUrl ?>"><span class="ico"></span><div class="txt">Bag</div></a></li>
            <li class="divider">-----------</li>
            <li class="step step2"><span class="ico"></span><div class="txt">Delivery</div></li>
            <li class="divider">-----------</li>
            <li class="step step3"><span class="ico"></span><div class="txt">Payment</div></li>
        </ol>
    </div>
</header>
<?php 
   if(isGuestCheckout()) {
    x_session_unregister('glogin');
    $url = HostConfig::$portalIDPHost.'signout';
    $restRequest = new RestRequest($url, 'POST');
    $headers = array();
    array_push($headers, 'x-csrf-token: '.$XCART_SESSION_VARS['USER_TOKEN']);
    array_push($headers , 'Cookie: '.HostConfig::$cookieprefix .'xid='.$_COOKIE[HostConfig::$cookieprefix . 'xid']);
    $restRequest->setHttpHeaders($headers);

    $restRequest->execute();
    }
?>
