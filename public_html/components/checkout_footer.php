<?php
$cc_email = 'support@myntra.com';
$cc_phone = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall', '+91-80-43541999');
?>
<footer class="page-footer">
    <div class="help-info">
        <span class="lbl">Need Help?</span>
        <span class="phone-number-email">
        <span class="phone-number"><?php echo $cc_phone ?></span> / 
            <a class="email" href="mailto:<?php echo $cc_email ?>"><?php echo $cc_email ?></a>
        </span>
        <span class="icon-payment-options"></span>
    </div>
    <div class="copyright">
        Copyright &copy; myntra.com / All Rights Reserved /
        <a href="http://www.myntra.com/privacypolicy#top">Our Privacy Policy</a> /
        <a href="http://www.myntra.com/termsofuse#top">Our Terms of Use</a>
    </div>
</footer>
