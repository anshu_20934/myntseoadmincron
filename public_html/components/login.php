<?php
require_once $xcart_dir."/components/mrp.php";

$_GET['ref'] = str_replace('[AT]', '@', $_GET['ref']);
$referer = filter_input(INPUT_GET, 'ref', FILTER_VALIDATE_EMAIL);

$mrp_referer = '';
if (!empty($referer)) {
    $query = "SELECT firstname, lastname FROM $sql_tbl[customers] WHERE login='$mrp_referer';";
    $result = db_query($query);
    $row = db_fetch_array($result);
    if (!empty($row)) {
        $mrp_referer = $referer;
        /*
        $refName = $mrp_referer;
        if ($row['firstname'] != '') {
            $refName = $row['firstname'];
            $refName .= ($row['lastname'] != '')? " ".$row['lastname']: " ";
        }
        */
    }
}


$smarty->assign("mrp_referer", $mrp_referer);
$smarty->assign("connectFbMsg", WidgetKeyValuePairs::getWidgetValueForKey("connect_fb_message"));
$smarty->assign("signUpMsg", WidgetKeyValuePairs::getWidgetValueForKey("signup_message"));

$secureLoginEnabled = FeatureGateKeyValuePairs::getBoolean('secure.login');
$smarty->assign('secureLoginEnabled', $secureLoginEnabled);
if (isset($_SERVER['HTTPS']) && $secureLoginEnabled) {
	$smarty->assign('is_secure', true);
}

$loginTimeout = WidgetKeyValuePairs::getInteger("loginTimeout", 30) * 1000;
$smarty->display('inc/modal-signup.tpl');
?>

<script>
    Myntra.Data.loginTimeout = <?php echo $loginTimeout ?>;
    Myntra.Data.nonFBRegCouponValue = "<?php echo $mrp_nonfbCouponValue ?>";
    Myntra.Data.fbRegCouponValue = "<?php echo $mrp_fbCouponValue ?>";
</script>


