<?php

$curlVerboseEnabled = FeatureGateKeyValuePairs::getBoolean('curlverbose.enabled', false);

function curlInit($url, $headers = array(), $cookieString = '') {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_COOKIE, $cookieString);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

    return $ch;
}

function curlExec($ch) {
    global $weblog, $errorlog, $curlVerboseEnabled;

    if ($curlVerboseEnabled) {
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        $verbose = fopen('php://temp', 'rw+');
        curl_setopt($ch, CURLOPT_STDERR, $verbose);
    }

    $result = curl_exec($ch);
    $error = curl_error($ch);
    $info = curl_getinfo($ch);

    if ($curlVerboseEnabled) {
        !rewind($verbose);
        $verboseLog = stream_get_contents($verbose);
        if (!empty($error) || empty($result) || $info['http_code'] != 200) {
            $errorlog->error("REST Request:\n" . $verboseLog);
            $errorlog->error("REST Response Body:\n" . $result);
        }
        else {
            $weblog->debug("REST Request:\n" . $verboseLog);
        }
    }

    return array($result, $error, $info);
}

function curlSetVerbPut($ch, $requestBody) {
    $requestLength = strlen($requestBody);

    $fh = fopen('php://memory', 'rw');
    fwrite($fh, $requestBody);
    rewind($fh);

    curl_setopt($ch, CURLOPT_INFILE, $fh);
    curl_setopt($ch, CURLOPT_INFILESIZE, $requestLength);
    curl_setopt($ch, CURLOPT_PUT, 1);
}

function curlSetVerbDelete($ch, $requestBody = false) {
 	if($requestBody != false){
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
    }
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
}

