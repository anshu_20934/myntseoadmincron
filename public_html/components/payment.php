<?php
require_once __DIR__.'/../auth.php';
require_once $xcart_dir.'/components/curl.php';
use enums\cart\CartContext;
use abtest\MABTest;
use revenue\payments\service\PaymentServiceInterface;
include_once ("$xcart_dir/securesessioncheck.php");

$isPromptLogin = isSecureSessionExpired();
/*
$session_data = getSessionBySessionId($sessionId);
$session = unserialize($session_data['data']);
echo('<pre>Session: '); print_r($session); echo('</pre>'); exit;
*/


// Prompt Login
// -------------------------------------------------------------------------------------
if ($isPromptLogin) {
    if ($isXHR) {
        header('Content-Type: application/json');
        $response = array('status' => 'ERROR_PROMPT_LOGIN', 'message' => '');
        echo json_encode($response);
    }
    else {
        $smarty->assign('isPromptLogin', $isPromptLogin);
        $smarty->display('checkout/prompt-login.tpl');
    }
    exit;
}


// Cookies
// -------------------------------------------------------------------------------------
$xid = $_COOKIE[HostConfig::$cookieprefix . 'xid']; //Sent to payment service as parameter
$cookieString = '';
$cookieNames = array('xid', 'sxid','mab');
foreach ($cookieNames as $key) {
    $name = HostConfig::$cookieprefix . $key;
    $value = $_COOKIE[$name];
    if ($cookieString) {
        $cookieString .= ';';
    }
    $cookieString .= $name . '=' . addslashes(rawurlencode($value));
}



// Cart Total
// -------------------------------------------------------------------------------------
$totalAmount = 0;
$cashbackApplied = 0;
$useCashback = '';
$useLoyaltyPoints='';
if (isset($_REQUEST['usecashback'])) {
    if ($_POST["_token"] !== $XCART_SESSION_VARS['USER_TOKEN']) {
        header('Content-Type: application/json');
        $response = array('status' => 'ERROR', 'message' => 'Invalid access');
        echo json_encode($response);
        exit;
    }
    $useCashback = filter_var($_REQUEST['usecashback'], FILTER_SANITIZE_STRING);
}

if (isset($_REQUEST['useloyaltypoints'])) {
	if ($_POST["_token"] !== $XCART_SESSION_VARS['USER_TOKEN']) {
		header('Content-Type: application/json');
		$response = array('status' => 'ERROR', 'message' => 'Invalid access');
		echo json_encode($response);
		exit;
	}
	$useLoyaltyPoints = filter_var($_REQUEST['useloyaltypoints'], FILTER_SANITIZE_STRING);
}

$url = HostConfig::$portalAbsolutServiceInternalHost . '/cart/default';
$headers = array('Accept: application/json', 'Accept-Language: en-US');
if ($useCashback == 'Y' || $useCashback == 'N') {
    $url .= '/applymyntcredit';
    $headers[] = 'Content-Type:application/json';
    $headers[] = 'X-CSRF-TOKEN:' . $XCART_SESSION_VARS['USER_TOKEN'];
}else if ($useLoyaltyPoints == 'Y' || $useLoyaltyPoints == 'N'){
	$url .= '/applyloyaltypoints';
	$headers[] = 'Content-Type:application/json';
	$headers[] = 'X-CSRF-TOKEN:' . $XCART_SESSION_VARS['USER_TOKEN'];
}


// query param to do real time inventory check
$url .= '?rt=true';
$ch = curlInit($url, $headers, $cookieString);
$requestBody = '';
if ($useCashback == 'Y') {
    $cashbackAmount = 100; //filter_var($_REQUEST['cashbackamt'], FILTER_SANITIZE_NUMBER);
    $requestData = array("useAmount" => $cashbackAmount);
    $requestBody = json_encode($requestData);
    curlSetVerbPut($ch, $requestBody);
}
else if ($useCashback == 'N') {
    curlSetVerbDelete($ch);
}else if ($useLoyaltyPoints == 'Y') {
    $points = 10; //filter_var($_REQUEST['cashbackamt'], FILTER_SANITIZE_NUMBER);
    $requestData = array("usePoints" => $points);
    $requestBody = json_encode($requestData);
    curlSetVerbPut($ch, $requestBody);
}
else if ($useLoyaltyPoints == 'N') {
    curlSetVerbDelete($ch);
}


list($result, $error, $info) = curlExec($ch);
curl_close($ch);

if (!empty($error) || empty($result) || $info['http_code'] != 200) {
    $errorlog->error("Cart Service Error! $error url: $url status: " . $info['http_code'] . " response: " . strip_tags($result));
    $smarty->display('checkout/payment-err.tpl');
    exit;
}
$response = json_decode($result);
//$weblog->debug('Cart Service Response: ' . print_r($response, true));
if ($response->status->statusType != 'SUCCESS') {
    $errorlog->error("Cart Service Error! url: $url msg: {$response->status->statusMessage} code: {$response->status->statusCode}");
    $smarty->display('checkout/payment-err.tpl');
    exit;
}
else if (empty($response->data[0]->readyForCheckout)) {
    $errorMessage = <<<HTML
An item in your shopping bag has gone out of stock.
Please <a href="#editbag" onclick="Myntra.Payments.UI.gotoBag()">go to your bag</a>, remove the item, and proceed to checkout.
HTML;
    $smarty->assign('errorMessage', $errorMessage);
    $smarty->display('checkout/payment-err.tpl');
    exit;
}

$isGiftOrder = $response->data[0]->isGiftOrder;
$totalAmount = $response->data[0]->totalPrice;
$cashbackApplied = $response->data[0]->totalMyntCashUsage;
$cartTotal = $response->data[0]->totalPriceWithoutMyntCash;

$isGiftOrder  = intval($response->data[0]->isGiftOrder);
$amount =$response->data[0]->totalMrp;
$totalQuantity  = sizeof($response->data[0]->cartItemEntries);
$mrp =$response->data[0]->totalMrp;
$couponDiscount = $response->data[0]->totalCouponDiscount;
$cashBackAmount =$response->data[0]->totalMyntCashUsage;
$totalCashBackAmount = $response->data[0]->totalMyntCashUsage;
$cashBackAmountDisplayOnCart= $response->data[0]->totalMyntCashUsage;
$shippingCharge = ($response->data[0]->shippingCharge);
$giftCharge = $response->data[0]->giftCharge;
$cartLevelDiscount = $response->data[0]->totalDiscount;
$savings = -intval($mrp) + intval($totalAmount);
$_cart = array();
$allCart = $response->data[0]->cartItemEntries;

// BIN based coupon 
$appliedCoupons = $response->data[0]->appliedCoupons;
$appliedCoupon = empty($appliedCoupons) ? array() : $appliedCoupons[0];
$hasPaymentCoupon = !empty($appliedCoupon) ? ($appliedCoupon->groupType == 'pg' ? true : false) : false;
$paymentCouponAllowedOptions = array('credit_card','debit_card');
$smarty->assign('hasPaymentCoupon',$hasPaymentCoupon);
$smarty->assign('paymentCouponAllowedOptions',$paymentCouponAllowedOptions);

for ($i=0; $i < sizeof($allCart); $i++ ){
    $_item = array();
    $_item['id'] = $response->data[0]->cartItemEntries[$i]->styleId;
    array_push($_cart, $_item);
}
$smarty->assign("_cartItemsJson", json_encode($_cart));
$smarty->assign("_cart", $_cart);
$smarty->assign("isGiftOrder",$isGiftOrder);
$smarty->assign("amount",$amount);
$smarty->assign("totalQuantity",$totalQuantity);
$smarty->assign("mrp",$mrp);
$smarty->assign("couponDiscount",$couponDiscount);
$smarty->assign("cashdiscount",$totalCashBackAmount);
$smarty->assign("totalCashBackAmount",$totalCashBackAmount);
$smarty->assign("cashBackAmountDisplayOnCart",$cashBackAmountDisplayOnCart);
$smarty->assign("shippingCharge",$shippingCharge);
$smarty->assign("giftCharge",$giftCharge);
$smarty->assign("savings",$savings);
$smarty->assign("cartLevelDiscount",$cartLevelDiscount);
$smarty->assign('totalAmount', $totalAmount);
$smarty->assign('cashbackApplied', $cashbackApplied);
$smarty->assign('cartTotal', $cartTotal);

$smarty->assign('totalAmount', $totalAmount);
$smarty->assign('cashbackApplied', $cashbackApplied);
$smarty->assign('cartTotal', $cartTotal);
$smarty->assign('appliedCoupon',$appliedCoupon);


// Loyalty 
// -------------------------------------------
include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalClient.php";

function hcf($a, $b) {
    if ($b > $a) return hcf($b, $a);
    if ($b == 0) return $a;
    return hcf($b, $a % $b);
}
$totalDeduction =  $cashbackApplied ? $cashbackApplied : 0;
// $loyaltyEnabled is initialized from auth.php from Feature gate and put into smarty
if($loyaltyEnabled){
    
    $loyaltyPointsUsed = $response->data[0]->loyaltyPointsUsed;
    $loyaltyPointsEarnAfterPurchase = $response->data[0]->loyaltyPointsAwarded;
    $loyaltyPointsEarnAfterPurchase = ($loyaltyPointsEarnAfterPurchase < 0 ) ? 0 : round($loyaltyPointsEarnAfterPurchase);

    $lpD = LoyaltyPointsPortalClient::getAccountInfo($login);
    $tNCAccpedForLoyalty = $lpD["tNcAccepted"];
    $loyaltyPointsThreshold = $lpD["thresholdForUsage"];
    $loyaltyPointsConversionFactor =  $lpD["conversionFactor"];
    $totalActiveLoyaltyPoints = $lpD["activePointsBalance"]; // Total active loyalty points of the customer
    $totalActiveLoyaltyPointsRupees =  $lpD["cashEqualantAmount"];
    $activeLoyaltyPointsBalance = $lpD["activePointsBalance"] - $loyaltyPointsUsed;
    $activeLoyaltyPointsBalance = ($loyaltyPointsBalance < 0)?0:$activeLoyaltyPointsBalance;
    $activeLoyaltyPointsBalanceRupees = $activeLoyaltyPointsBalance * $loyaltyPointsConversionFactor;

    $isLoyaltyPointsAboveThreshold = false;
    $showLoyaltyBlock = false;

    $loyaltyPointsUsedRupees = $loyaltyPointsUsed * $loyaltyPointsConversionFactor;

    if(($tNCAccpedForLoyalty || $tNCAccpedForLoyalty == "true") && ($loyaltyPointsUsed || $totalActiveLoyaltyPointsRupees || $loyaltyPointsEarnAfterPurchase)){
        $showLoyaltyBlock = true; //Flag to toggle loyaltyBlock to show or not to show
    }


    if($loyaltyPointsThreshold <= $totalActiveLoyaltyPoints){
        $isLoyaltyPointsAboveThreshold = true; // In case where customer has not reached the threshold to use loyaltyPoints
    }



    $totalDeduction += $loyaltyPointsUsedRupees;
    // For showing tooltip : 2 LP = Rs . 1 or 1 LP = Rs. 2
    $lpEquivalentPoints = 1;
    $lpEquivalentRupees = $loyaltyPointsConversionFactor;
    if($lpEquivalentRupees < 1){
        $lpEquivalentPoints = intval($lpEquivalentPoints * 1000);
        $lpEquivalentRupees = intval($lpEquivalentRupees * 1000);
        $tHCF = hcf($lpEquivalentPoints,$lpEquivalentRupees);
        $lpEquivalentPoints = $lpEquivalentPoints/$tHCF;
        $lpEquivalentRupees = $lpEquivalentRupees/$tHCF;
    }
    $smarty->assign('lpEquivalentPoints',$lpEquivalentPoints);
    $smarty->assign('lpEquivalentRupees',$lpEquivalentRupees);
    $smarty->assign('showLoyaltyBlock',$showLoyaltyBlock);
    $smarty->assign('loyaltyPointsThreshold', $loyaltyPointsThreshold);
    $smarty->assign('totalActiveLoyaltyPoints',$totalActiveLoyaltyPoints);
    $smarty->assign('totalActiveLoyaltyPointsRupees',$totalActiveLoyaltyPointsRupees);
    $smarty->assign('loyaltyPointsUsed', $loyaltyPointsUsed);
    $smarty->assign('loyaltyPointsBalance', $loyaltyPointsBalance);
    $smarty->assign('loyaltyPointsEarnAfterPurchase', $loyaltyPointsEarnAfterPurchase);
    $smarty->assign('loyaltyPointsUsedRupees', $loyaltyPointsUsedRupees);
    $smarty->assign('isLoyaltyPointsAboveThreshold',$isLoyaltyPointsAboveThreshold);
    $smarty->assign('activeLoyaltyPointsBalance',$activeLoyaltyPointsBalance); 
    $smarty->assign('activeLoyaltyPointsBalanceRupees',$activeLoyaltyPointsBalanceRupees);
}
$smarty->assign('totalDeduction', $totalDeduction);


// set the cart last updated time in the session to avoid "cart tampered" error from OMS
$XCART_SESSION_VARS['LAST_CART_UPDATE_TIME'] = $response->data[0]->lastContentUpdated;



// Cashback
// -------------------------------------------------------------------------------------
$cashbackBalance = 0;
$url = HostConfig::$portalAbsolutServiceInternalHost . '/myntcredits';
$headers = array('Accept: application/json', 'Accept-Language: en-US');
$ch = curlInit($url, $headers, $cookieString);
list($result, $error, $info) = curlExec($ch);
curl_close($ch);
if (!empty($error) || empty($result) || $info['http_code'] != 200) {
    $errorlog->error("Myntcredits Service Error! $error url: $url status: " . $info['http_code'] . " response: " . strip_tags($result));
    $smarty->display('checkout/payment-err.tpl');
    exit;
}
$response = json_decode($result);
//$weblog->debug('Myntcredits Service Response: ' . print_r($response, true));
if ($response->status->statusType != 'SUCCESS') {
    $errorlog->error("Myntcredits Service Error! url: $url msg: {$response->status->statusMessage} code: {$response->status->statusCode}");
    $smarty->display('checkout/payment-err.tpl');
    exit;
}
$cashbackBalance = $response->data[0]->balance - $cashbackApplied;
$cashbackBalance = $cashbackBalance < 0 ? 0 : round($cashbackBalance);
$smarty->assign('cashbackBalance', $cashbackBalance);


// Shipping Address
// -------------------------------------------------------------------------------------
$addressId = filter_var($_REQUEST['address'], FILTER_SANITIZE_NUMBER_INT);
if (empty($addressId)) {
    $addressId = $XCART_SESSION_VARS['payment_address'];
}
if (empty($addressId)) {
    header('Content-Type: application/json');
    $response = array('status' => 'ERROR_NO_SHIPPING_ADDRESS');
    echo json_encode($response);
    exit;
}
$XCART_SESSION_VARS['payment_address'] = $addressId;

$url = HostConfig::$portalAbsolutServiceInternalHost . '/address/cartserviceability/default?fetchSize=50';
$headers = array('Accept: application/json', 'Accept-Language: en-US');
$ch = curlInit($url, $headers, $cookieString);
list($result, $error, $info) = curlExec($ch);
curl_close($ch);
if (!empty($error) || empty($result) || $info['http_code'] != 200) {
    $errorlog->error("Address Service Error! $error url: $url status: " . $info['http_code'] . " response: " . strip_tags($result));
}
$response = json_decode($result);
//$weblog->debug('Address Service Response: ' . print_r($response, true));
if ($response->status->statusType != 'SUCCESS') {
    $errorlog->error("Address Service Error! url: $url msg: {$response->status->statusMessage} code: {$response->status->statusCode}");
    $smarty->display('checkout/payment-err.tpl');
    exit;
}
$addresses = $response->data;

// move the default address to the top
for ($i = 1, $n = count($addresses); $i < $n; $i += 1) {
    if ($addresses[$i]->defaultAddress) {
        $tmp = $addresses[0];
        $addresses[0] = $addresses[$i];
        $addresses[$i] = $tmp;
        break;
    }
}
//$arr = array_map(function($a) { return $a->id . ':' . $a->defaultAddress; }, $addresses);
//$weblog->debug('Sorted Addresses: ' . print_r($arr, true));

$shippingAddress = null;
foreach ($addresses as $address) {
    if ($address->id == $addressId) {
        $shippingAddress = $address;
    }
}
if (empty($shippingAddress)) {
    $errorlog->error("Shipping Address Id: $addressId not found in the customer's address list");
    header('Content-Type: application/json');
    $response = array('status' => 'ERROR_NO_SHIPPING_ADDRESS');
    echo json_encode($response);
    //$smarty->assign('errorMessage', 'Please create or select a shipping address');
    //$smarty->display('checkout/payment-err.tpl');
    exit;
}
//$weblog->debug('Shipping Address: ' . print_r($shippingAddress, true));



// Online/COD Serviceablity
// -------------------------------------------------------------------------------------
$codErrorCode = $shippingAddress->codAvailabilityEntry->errorCode;
$onlineServiceable = $shippingAddress->addressServiceabilityEntry->nonCODServiceability == 'SERVICEABLE';
$onlineErrorMessage = $onlineServiceable ? "" : "Prepaid delivery is not available for your Pincode.";
/*
if (!empty($codErrorMessage)) {
}
*/
// Initializing codErrorMessage to empty
$codErrorMessage = '';
// COD Error messages are changed in the new checkout flow
// Making gift order as non cod always.
if ($isGiftOrder) {
    //$codErrorMessage = 'Çash on Delivery not available on gift orders. You may pay for this order using any other payment modes.';
    $codErrorMessage = 'We do not offer cash on delivery for gift orders. Please pay using a different payment mode.';
}else if (!empty($codErrorCode)) {
    $codErrorMessage = $shippingAddress->codAvailabilityEntry->errorMessage;
    /*
    $customerSupportCall = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
    $customerSupportTime = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportTime');
    $codErrorMessage .= "<br/><br/>";
    $codErrorMessage .= "<b>You may pay for this order using any of our other payment modes.</b><br/><br/>";
    $codErrorMessage .= <<<HTML
    Have a query? Our Customer Champions are available ($customerSupportTime) 
    and will be glad to help you with completing this order or answer any other questions that you may have.
    You can reach our Customer Champion at $customerSupportCall
    or <a href="mailto:support@myntra.com" target="_blank">support@myntra.com</a>.
HTML;
    */
}


// One click COD eligibility
// -------------------------------------------------------------------------------------
if (isset($XCART_SESSION_VARS["oneClickCODElegible"])) {
    $checkCODElegible = $XCART_SESSION_VARS["oneClickCODElegible"];
}
else {
    $sql = "select login from xcart_orders where login='$login' and payment_method='cod' 
        and (cod_pay_status='paidtobluedart' or cod_pay_status='paid') limit 1";
    $row = func_query_first_cell($sql);
    $checkCODElegible = empty($row) ? "false" : "true";
    $XCART_SESSION_VARS["oneClickCODElegible"] = $checkCODElegible;
}

$oneClickCODEnabled = FeatureGateKeyValuePairs::getBoolean('oneClickCOD');
$oneClickCOD = ($oneClickCODEnabled && $checkCODElegible == "true" && $codErrorCode == 0);

if ($codErrorCode == 0 && !$onlineServiceable) {
    $oneClickCOD = true;
}


// Payment UI
// -------------------------------------------------------------------------------------
$paymentMarkup = '';
$clientCode = 'portalnew';
global $telesales;
if ($telesales)
    $clientCode='telesalen';
$cartContext = CartContext::DefaultContext;
$serviceRequestParams = "totalAmount=". urlencode($totalAmount);
$serviceRequestParams .= "&clientCode=". urlencode($clientCode);
$serviceRequestParams .= "&cartContext=". urlencode($cartContext);
$serviceRequestParams .= "&addressid=" . urlencode($shippingAddress->id);
$serviceRequestParams .= "&addressname=" . urlencode($shippingAddress->name);
$serviceRequestParams .= "&addressaddress=" . urlencode($shippingAddress->address);
$serviceRequestParams .= "&addresscity=" . urlencode($shippingAddress->city);
$serviceRequestParams .= "&addressstatename=" . urlencode($shippingAddress->stateName);
$serviceRequestParams .= "&addresspincode=" . urlencode($shippingAddress->pincode);
$serviceRequestParams .= "&addresscountry=" . urlencode($shippingAddress->countryName);
$serviceRequestParams .= "&coderrorcode=" . urlencode($codErrorCode);
$serviceRequestParams .= "&login=" . urlencode($login);
$serviceRequestParams .= "&xid=" . urlencode($xid);

if($hasPaymentCoupon){
    $serviceRequestParams .= "&defaultPaymentType=credit_card";
}

if (!empty($oneClickCOD)) {
    $serviceRequestParams .= "&oneClickCOD=1";
}
if (!empty($onlineErrorMessage)) {
    $serviceRequestParams .= "&onlineErrorMessage=" . urlencode($onlineErrorMessage);
}
if (!empty($codErrorMessage)) {
    $serviceRequestParams .= "&codErrorMessage=".urlencode($codErrorMessage);
}
if (!empty($isGiftOrder)) {
    $serviceRequestParams .= "&giftWrapEnabled=1";
}
$profile = HostConfig::$httpHost;
$serviceRequestParams .= "&profile=" . urlencode($profile);

$url = HostConfig::$paymentServiceInternalHost.'/payment?'. $serviceRequestParams;
$headers = array('Accept: text/html', 'Content-Type: application/json');
$ch = curlInit($url, $headers);
list($result, $error, $info) = curlExec($ch);
curl_close($ch);
if (!empty($error) || empty($result) || $info['http_code'] != 200) {
    $errorlog->error("Payment Service Error! $error url: $url status: " . $info['http_code'] . " response: " . strip_tags($result));
    $smarty->display('checkout/payment-err.tpl');
    exit;
}
else {
    $paymentMarkup = $result;
}

$smarty->assign('citiDiscount', FeatureGateKeyValuePairs::getInteger('payments.citiBankCardDiscount'));
$smarty->assign('iciciDiscount', FeatureGateKeyValuePairs::getInteger('payments.iciciBankCardDiscount'));
$smarty->assign("payHelpline", MABTest::getInstance()->getUserSegment('payHelpline'));
$smarty->assign("payFailOpt", MABTest::getInstance()->getUserSegment('payFailOpt'));
$smarty->assign("payZipOrders", MABTest::getInstance()->getUserSegment('payZipOrders'));
$smarty->assign("payUserCloseOpt", MABTest::getInstance()->getUserSegment('payUserCloseOpt'));
$smarty->assign('codErrorCode', $codErrorCode);
//$smarty->assign('clientCode', $clientCode);
$smarty->assign('paymentMarkup', $paymentMarkup);
$smarty->assign('shippingAddressJSON', json_encode($shippingAddress));
$smarty->assign('addressesJSON', json_encode($addresses));
$smarty->display('checkout/payment-ui.tpl');

