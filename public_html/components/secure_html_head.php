<?php
if (!empty($_REQUEST['nocombo']) || !empty($_COOKIE['nocombo'])) {
    HostConfig::$nocombo = '1';
}
if (isset($_SERVER['HTTPS'])) {
    $cdnBase = HostConfig::$secureCdnBase;
    $httpBase = 'https://' . HostConfig::$httpsHost;
    $fontBase = $httpBase;
}
else {
    $cdnBase = HostConfig::$cdnBase;
    $httpBase = 'http://' . HostConfig::$httpHost;
    $fontBase = $cdnBase;
}
//Kuliza Code Start
$isKulizaEchoEnabled = FeatureGateKeyValuePairs::getBoolean('kulizaecho.enabled', true);
$smarty->assign('isKulizaEchoEnabled', $isKulizaEchoEnabled);
$socialPermissionCategoryExcludeText = WidgetKeyValuePairs::getWidgetValueForKey("socialPermissionCategoryExcludeText");
$smarty->assign('socialPermissionCategoryExcludeText', $socialPermissionCategoryExcludeText);
$smarty->assign('kulizaEchoBaseUrl', HostConfig::$kulizaEchoBaseUrl);
$smarty->assign('kulizaClientDomain', HostConfig::$kulizaClientDomain);
$smarty->assign('kulizaClientBaseUrl', HostConfig::$kulizaClientBaseUrl);
//Kuliza Code End

?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="language" content="en">
<title>Secure Checkout - Myntra</title>
<style type="text/css">
@font-face {
    font-family: 'din-med';
        src: url('<?php echo $fontBase ?>/skin2/fonts/1D41FF_1_0.eot');
        src: url('<?php echo $fontBase ?>/skin2/fonts/1D41FF_1_0.eot?#iefix') format('embedded-opentype'),
             url('<?php echo $fontBase ?>/skin2/fonts/1D41FF_1_0.woff') format('woff'),
             url('<?php echo $fontBase ?>/skin2/fonts/1D41FF_1_0.ttf') format('truetype');
}
</style>

<?php foreach($smarty->getTemplateVars('cssFiles') as $url) { ?>
<link type="text/css" rel="stylesheet" href="<?php echo $url ?>">
<?php } ?>

<?php $smarty->display('inc/head-script.tpl'); ?>

<?php if ($widgetConfigJson): ?>
<script>
Myntra.WidgetConfig = <?php echo $widgetConfigJson ?>;
</script>
<?php endif ?>

