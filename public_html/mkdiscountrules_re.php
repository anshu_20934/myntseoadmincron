<?php
  // get the discount coupon details according to the coupon code.
  // return error msg if the coupon code is invalid or has expired.

  // case 1
  // order subtotal equals or exceed the amount specified

  //case 2
  //cart contains the products specified
  // **** discount applied once per order
  // **** disocunt applied to each item

  //case 3
  //cart contains products belonging to a caategory/subcategory
  // **** discount applied once
  // **** discount applied to each product from the category
  // **** discount applied to each product title from category


/*this function is written by vaibhav to support multiple styles for a given coupon */
function styleIdCheckInCoupon($styleIdsString, $styleId) {

        $styleArr = explode(",", trim($styleIdsString, " "));
        if(in_array($styleId, $styleArr)) {
                return 1;
        } else {
                return 0;
        }

}

function check_coupon_validity($couponCodeDetails,$amount=0){
	global $login,$couponCode,$grandTotal;
	global $weblog;
	$weblog->info("inside function check_coupon_validity login $login couponCode $couponCode grandTotal $grandTotal");
	
    if (empty($couponCodeDetails)){
        $errorMessageCoupon = "No active coupon found matching your coupon code.";
	$weblog->info("inside function check_coupon_validity checking for empty login $login couponCode $couponCode error $errorMessageCoupon");
	
        return $errorMessageCoupon;
    }
    //check for the coupon validity and usage
    $oldExpireTime = $couponCodeDetails[8];
    $intExpireTime=date('d F Y',$oldExpireTime);
	$newExpireTime=strtotime($intExpireTime);
    if($newExpireTime!=$oldExpireTime) {
		$intExpireTime=date('d F Y',$newExpireTime+24*60*60);
		$newExpireTime=strtotime($intExpireTime);
	}
	else $newExpireTime=$oldExpireTime;
    
    $couponExpirationDate = $newExpireTime;
    if ($couponExpirationDate < time())
	{
		//check time with Unix Epoch
        $errorMessageCoupon = "The coupon has expired.";
	$weblog->info("inside function check_coupon_validity checking for expiration login $login couponCode $couponCode error $errorMessageCoupon");
        return $errorMessageCoupon;
    }

    //check for the coupon startdate
    $couponStartDate = $couponCodeDetails[21];
    if ($couponStartDate > time()){
        $after = date('jS M, Y',$couponStartDate);
        $before = date('jS M, Y',$couponExpirationDate);
        $errorMessageCoupon = "The coupon is only valid between $after and $before.";
	$weblog->info("inside function check_coupon_validity checking for startdate login $login couponCode $couponCode error $errorMessageCoupon");
        return $errorMessageCoupon;
    }
    //check cart amount is greater than minimum amount of coupon
	if(!empty($amount))
	{
		if($amount < $couponCodeDetails[4] )
		{
			$errorMessageCoupon = "Your coupon can be redeemed only for cart value above Rs $couponCodeDetails[4]";
			$weblog->info("inside function check_coupon_validity checking for cart value login $login couponCode $couponCode error $errorMessageCoupon");
		        return $errorMessageCoupon;
		}
	}
    // for peruser check
    if($couponCodeDetails[6] == 'Y')
    {

	 
		$couponUsed = db_query("SELECT times_used FROM xcart_discount_coupons_login WHERE login='".$login."' AND coupon='".$couponCode."'");
		$row = db_fetch_array($couponUsed);
		if ($row['times_used']  > 0)
		{
			// need to put the logic for peruser check
			$errorMessageCoupon = "Your coupon usage has expired.";
			$weblog->info("inside function check_coupon_validity checking for times_used login $login couponCode $couponCode error $errorMessageCoupon");
			return $errorMessageCoupon;
		}
		if ($couponCodeDetails[20]  > 0)
		{
			// need to put the logic for peruser check
			$errorMessageCoupon = "The coupon has been locked due to a possible incomplete order. please wait for 9 hours for coupon to be released and then try again.";
			$weblog->info("inside function check_coupon_validity checking for times_used and times_locked login $login couponCode $couponCode error $errorMessageCoupon");
			return $errorMessageCoupon;
		}
			
       
    }
    else 
    {
	$weblog->info("couponName : $couponCode times $couponCodeDetails[5] times_used $couponCodeDetails[7] times_locked $couponCodeDetails[20]");
    	if ($couponCodeDetails[7] >= $couponCodeDetails[5])
		{
			$errorMessageCoupon = "The coupon usage has expired.";
			$weblog->info("inside function check_coupon_validity login $login couponCode $couponCode error $errorMessageCoupon");
		    return $errorMessageCoupon;
		}
    	elseif ($couponCodeDetails[7] + $couponCodeDetails[20] >= $couponCodeDetails[5]){
			$errorMessageCoupon = "The coupon has been locked due to a possible incomplete order. please wait for 9 hours for coupon to be released and then try again.";
			$weblog->info("inside function check_coupon_validity login $login couponCode $couponCode error $errorMessageCoupon");
		    return $errorMessageCoupon;
		}
    	
    }

	/*if ($couponCodeDetails[7] >= $couponCodeDetails[5]){ // need to put the logic for peruser check
        $errorMessageCoupon = "This coupon has already been redeemed. Please provide valid coupon code";
        return $errorMessageCoupon;
    }*/
    return "";
}

function calculate_discount_for_cart($productsInCart,$productids,$couponCodeDetails,$grandTotal){
	global $amountOf99corner;
	global $weblog;
	$weblog->info(" PRODUCT TYPE >  >>>>>>>>>> ".$couponCodeDetails[18]);
	$weblog->info(" PRODUCT STYLE >  >>>>>>>>>> ".$couponCodeDetails[16]);
	$grandTotal = (!empty($amountOf99corner)) ? ($grandTotal - $amountOf99corner) :  $grandTotal;

    if ($couponCodeDetails[2] == '0' && $couponCodeDetails[3] == '0' && $couponCodeDetails[16] == '0'&& $couponCodeDetails[18] == '0' && $couponCodeDetails[19] == '0'){
    	//case 1 order subtotal equals or exceed the amount specified
		$weblog->info(" CASE =====1");
        return calculate_order_subtotal_discount($productsInCart,$productids,$couponCodeDetails,$grandTotal);
    }
    elseif ($couponCodeDetails[2] != '0' && $couponCodeDetails[3] == '0'){
    	//case 2 cart contains the products specified
				$weblog->info(" CASE =====2");

        return calculate_product_only_discount($productsInCart,$productids,$couponCodeDetails);
    }
    elseif ($couponCodeDetails[2] == '0' && $couponCodeDetails[3] != '0'){
    	//case 3 cart contains products from specific category/subcategory.
				$weblog->info(" CASE =====3");

		return calculate_category_only_discount($productsInCart,$productids,$couponCodeDetails);
    }
	elseif (!empty($couponCodeDetails[16])){
		//case 3 cart contains products from specific styleid.
				$weblog->info(" CASE =====4");

		return calculate_order_style_discount($productsInCart,$productids,$couponCodeDetails,$grandTotal);
    }
   elseif ($couponCodeDetails[16]  == '0' && $couponCodeDetails[18] != '0'){
   	   //case 3 cart contains products from specific typeid and no particular style defined.
	   		$weblog->info(" CASE =====5");

		return calculate_order_type_discount($productsInCart,$productids,$couponCodeDetails);
    }
    elseif (!empty($couponCodeDetails[19])){
        //case 3 promotional offer
				$weblog->info(" CASE =====6");

        return calculate_discount_for_promotion($productsInCart,$productids,$couponCodeDetails);
    }
    else
	{
		 	 return 0;
    }

}

function calculate_order_subtotal_discount($productsInCart,$productids,$couponCodeDetails,$grandTotal){

    //TODO check for the max discount that can be availed.
    if ($grandTotal < $couponCodeDetails[4])
        return 0;
    //check for maximum if maximum defined and total greater than max then offer percent discount only on maximum amount
   if (!empty($couponCodeDetails[17])&&$couponCodeDetails[17]!=0&&$grandTotal > $couponCodeDetails[17]&&$couponCodeDetails[1] == 'percent')
        return (($couponCodeDetails[17] * $couponCodeDetails[0])/100);
    if ($couponCodeDetails[1] == 'absolute'){
        return $couponCodeDetails[0];
    }elseif($couponCodeDetails[1] == 'percent'){
        return (($grandTotal * $couponCodeDetails[0])/100);
    }
}

function calculate_order_style_discount($productsInCart,$productids,$couponCodeDetails,$grandTotal)
{
	$discountAmount  = 0;
	if ($grandTotal < $couponCodeDetails[4])
		return 0;	
	if (!empty($couponCodeDetails[17])&&$couponCodeDetails[17]!=0&&$grandTotal > $couponCodeDetails[17]&&$couponCodeDetails[1] == 'percent' && empty($couponCodeDetails[16]))
		return (($couponCodeDetails[17] * $couponCodeDetails[0])/100);
    if ($couponCodeDetails[1] == 'absolute')
	{
        return $couponCodeDetails[0];
    }
	foreach($productsInCart AS $key=>$value)
	{	
			$productDetail =   $value;
			$subTotal = $productDetail['productPrice']*$productDetail['quantity'];
		  	if(styleIdCheckInCoupon($couponCodeDetails[16],$productDetail['productStyleId']))
		  	{
				//to check if already discout is given for 1 style 
				if(!empty($discountAmount ))break;
				//no discount if total for this style is less than minimum order amount
				if ($subTotal < $couponCodeDetails[4])
                	$discountAmount = $discountAmount + 0;
				//if maximum defined and total for this style greater than maximum then offer discount only for maximum     
				if (!empty($couponCodeDetails[17])&&$couponCodeDetails[17]!=0 && $subTotal >= $couponCodeDetails[17])
				{
					if ($couponCodeDetails[1] == 'absolute')
					{
						$discountAmount = $discountAmount + $couponCodeDetails[0];
					}
					elseif($couponCodeDetails[1] == 'percent')
					{
						$discountAmount = $discountAmount + (($couponCodeDetails[17] * $couponCodeDetails[0])/100);
					}
		  		}
		  		//if maximum not defined or total for this style < maximum then give discount on total for this style
		  		elseif(($couponCodeDetails[17]==0)||(!empty($couponCodeDetails[17])&&$subTotal < $couponCodeDetails[17]))
				{
					if ($couponCodeDetails[1] == 'absolute')
					{
						$discountAmount = $discountAmount+$couponCodeDetails[0];
					}
					elseif($couponCodeDetails[1] == 'percent')
					{
						$discountAmount = $discountAmount+(($grandTotal * $couponCodeDetails[0])/100);	
					}
		  		}
			}
	}
	return $discountAmount;
}
function calculate_order_type_discount($productsInCart,$productids,$couponCodeDetails)
{
	$discountamount =0;
	$finalgrandtotal=0;
	global $weblog;
	
	
	foreach($productsInCart AS $key=>$value)
	{
			$productDetail =  $value;
			$grandTotal = $productDetail['productPrice']*$productDetail['quantity'];
			$typeid = $productDetail['producTypeId'];
			$weblog->info("tyeeeppee id >>>>> ".$typeid);
		    if($couponCodeDetails[18] == $typeid)
		    {
		  	  $finalgrandtotal=$finalgrandtotal+$grandTotal;
		  	
		     }
		  
		
	}
	// THIS IMPLIES THAT CART DOES NOT CONTAIN THE PRODUCTS OF SPECIFIED PRODUCT TYPE
	if($finalgrandtotal ==0 ) return 0;

	$weblog->info("finalgrandtotal >>>>> ".$finalgrandtotal);

	$weblog->info("max >>>>> ".$couponCodeDetails[17]);

   //no discount if total for this type is less than minimum order amount
		  	if ($finalgrandtotal < $couponCodeDetails[4])
                return 0;
            //if maximum defined and total for this type greater than maximum then offer discount only for maximum     
		  	if ((!empty( $couponCodeDetails[17] )&& $couponCodeDetails[17]!=0  )&&  $finalgrandtotal > $couponCodeDetails[17]){
		 		$weblog->info("INSIDEEEE >>>>>>>>>11 ");

				if ($couponCodeDetails[1] == 'absolute'){
						$discountamount = $couponCodeDetails[0];
					}elseif($couponCodeDetails[1] == 'percent'){
						$discountamount = (($couponCodeDetails[17] * $couponCodeDetails[0])/100);
							
					}
		  	}
		  	//if maximum not defined or total for this type < maximum then give discount on total for this type
		  	elseif(($couponCodeDetails[17]==0)||(($couponCodeDetails[17]!=0) && $finalgrandtotal < $couponCodeDetails[17])){
				$weblog->info("INSIDEEEE >>>>>>>>>22 ");

				if ($couponCodeDetails[1] == 'absolute'){
					$discountamount = $couponCodeDetails[0];
				}elseif($couponCodeDetails[1] == 'percent'){
					$discountamount = (($finalgrandtotal * $couponCodeDetails[0])/100);
						
				}
		  	}

      return $discountamount;

    
}
function calculate_product_only_discount($productsInCart,$productids,$couponCodeDetails)
{
	$totalDiscount = 0;
	foreach ( $productsInCart as $key=>$value )
	{
		$productid = $value["productId"];
		if( $productid == $couponCodeDetails[2])
		{
			if ($couponCodeDetails[13] == 'Y')
			{
				$count = $productsInCart[$couponCodeDetails[2]]['quantity'];
				if( $couponCodeDetails[1] == 'absolute' ) 
					$totalDiscount += $couponCodeDetails[0];
				elseif ($couponCodeDetails[1] == 'percent')
					$totalDiscount += ($value['productPrice'] * $couponCodeDetails[0])/100;
			}
			elseif( $couponCodeDetails[13] == 'N' )
			{
				$count = $productsInCart[$couponCodeDetails[2]]['quantity'];
				if( $couponCodeDetails[1] == 'absolute' )
					$totalDiscount += ($couponCodeDetails[0] * $count);
				else
					$totalDiscount += ($value['productPrice'] * $couponCodeDetails[0] * $count)/100;
			}
		}
	}
	return $totalDiscount;
}

function calculate_category_only_discount($productsInCart,$productids,$couponCodeDetails)
{
	// check if the category chosen is recursive, if so get all the categories and subcategories
	//print_r($couponCodeDetails);
	if ($couponCodeDetails[11] == 'Y')
	{
		$categoryArray = func_get_all_subcategories_for_category($couponCodeDetails[3]);
    }
	else
	{
		$categoryArray = array($couponCodeDetails[3]);
	}

    $cartCategories = get_categories_for_cart($productsInCart);
    $discount = 0.0;
    for($i=0;$i<count($cartCategories);$i++)
	{
        if (in_array($cartCategories[$i][0],$categoryArray))
		{
            //discount is valid for product category
			if ($couponCodeDetails[12] == 'Y' && $couponCodeDetails[13]=='Y')
			{
				//discount is applied once per order
                if ($couponCodeDetails[1] == 'absolute')
				{
                    $discount +=  ($couponCodeDetails[0]);
                }
				elseif ($couponCodeDetails[1] == 'percent')
				{
					foreach($productsInCart as $key=>$value)
					{
						$productid = $value["productId"];
						if( $productid == $cartCategories[$i][1] )
							$discount +=  ($couponCodeDetails[0] * $value['productPrice'])/100;
					}
                }
			}
			elseif($couponCodeDetails[12] == 'N' && $couponCodeDetails[13]=='Y')
			{
				if ($couponCodeDetails[1] == 'absolute')
				{
					$discount +=  ($couponCodeDetails[0]);
				}
				elseif ($couponCodeDetails[1] == 'percent')
				{
					foreach($productsInCart as $key=>$value)
					{
						$productid = $value["productId"];
						if( $productid == $cartCategories[$i][1] )
							$discount +=  ($couponCodeDetails[0] * $value['productPrice'])/100;
					}
                }
            }
			elseif($couponCodeDetails[12] == 'N' && $couponCodeDetails[13]=='N')
			{
                if ($couponCodeDetails[1] == 'absolute')
				{	
					foreach($productsInCart as $key=>$value)
					{
						$productid = $value["productId"];
						if( $productid == $cartCategories[$i][1] )
                    		$discount +=  ($value['quantity'] * $couponCodeDetails[0]);
					}
                }
				elseif ($couponCodeDetails[1] == 'percent')
				{
					foreach($productsInCart as $key=>$value)
					{
						$productid = $value["productId"];
						if( $productid == $cartCategories[$i][1] )
                    		$discount +=  ($value['quantity'] * $couponCodeDetails[0] * $value['unitPrice'])/100;
					}
                }
            }
        }
    }
    return $discount;
}

function get_categories_for_cart($productsInCart)
{
	$productids = array();
	foreach($productsInCart as $key=>$value)
	{
		$productids = $value["productId"];
	}
	$productids = array_unique($productids);
    $productIdsString = implode(',',$productids);
    return func_get_categories_for_products($productIdsString);
}
function calculate_discount_for_promotion($productsInCart,$productids,$couponCodeDetails){

	$num_of_prd_for_dis = 0;
	$discountamount = 0;
	$productPriceAmt = 0;
	foreach($productids as $_k => $_product)
	foreach($productsInCart as $key=>$value)
	{
		if(!empty($value['promotion']) && is_valid_promotion($value['promotion']['promotion_id']))
		{
			$num_of_prd_for_dis += $value['quantity'];
			$productPriceAmt += $value['productPrice'] * $value['quantity'];
		}
	}
	if ($couponCodeDetails[1] == 'absolute'){
					$discountamount = $num_of_prd_for_dis * $couponCodeDetails[0];
	}elseif($couponCodeDetails[1] == 'percent'){
					$discountamount = (($productPriceAmt * $couponCodeDetails[0])/100);
					
	}
	
	return $discountamount;
	
}
function is_valid_promotion(){
	return false;
}

?>
