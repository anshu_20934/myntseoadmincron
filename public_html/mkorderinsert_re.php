<?php
if(file_exists("./auth.php")) {
	include_once("./auth.php");
} else if (file_exists("../auth.php")) {
	include_once("../auth.php"); // happens for secure pages
} 

include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
$tracker = new BaseTracker(BaseTracker::ORDER_INSERT);
$tracker->fireRequest();			

include_once("$xcart_dir/include/func/func.mk_orderbook.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/include/func/func.mkcore.php");
include_once("$xcart_dir/include/func/func.mkspecialoffer.php");
include_once("$xcart_dir/mkstatesarray.php");

include_once ("$xcart_dir/modules/coupon/database/CouponAdapter.php");
include_once ("$xcart_dir/modules/coupon/CouponValidator.php");

include_once ("$xcart_dir/include/class/search/UserInterestCalculator.php");
include_once ("$xcart_dir/include/class/class.feature_gate.keyvaluepair.php");
include_once ("$xcart_dir/include/class/PaymentGatewayDiscount.php");
require_once ("$xcart_dir/icici/ajax_bintest.php");
include_once ("$xcart_dir/include/class/mcart/class.MCart.php");
require_once ("$xcart_dir/axis/helperAPI.php");
include_once ("$xcart_dir/include/class/netbankingHelper.php");

require_once ("$xcart_dir/libfuncs.php3");
x_load('crypt','mail','user');

function determineCardType($number) {
	// Based on code from here 
	// http://mel-green.com/2008/11/determine-credit-card-type-with-javascript/		
	if(preg_match('/^4/', $number)) {
		return 'VISA';
	} elseif(preg_match('/^5[1-5]/', $number)) {
		return 'MC';
	} elseif(preg_match('/^3(?:0[0-5]|[68])/', $number)) {
		return 'DINERS';
	} else {
		return 'MAEST';
	}
}

function getNetBankingGateway($gateways) {
	//expecting a array having name,weight and bankcode
	if(!is_array($gateways)) return null;
	$arrayElement = array();
	$weightarray = array();
	foreach($gateways as $gateway){
		$weightarray[] = $gateway['weight'];
		$arrayElement[] = array('name' =>$gateway['name'],'gateway_code' => $gateway['gateway_code'] );
	}
	return selectRandomArrayElement($arrayElement,$weightarray);
}


function selectRandomArrayElement($arrayElemets,$weight) {
	if(!is_array($arrayElemets) || !is_array($weight)) return null;
	
	$weightSum = array();
	$weightSum[0]=$weight[0];
	$weightLenght = count($weight);
	for($i=1;$i<$weightLenght;$i++) {
		$weightSum[$i] = $weightSum[$i-1] + $weight[$i];	
	}	
	$k=mt_rand(0, $weightSum[$weightLenght-1]);
	
	for ($i = 0; $k > $weightSum[$i]; $i++) ;
	
	return $arrayElemets[$i];
}



//+++++++++++++++ Order Log File for debugging ++++++++++++++//
$orderLogFile = $var_dirs['log']."/orders.log";


// capture the session dump in the beginning
$shouldLog = true;
$ORDERLOG = fopen($orderLogFile,"a+");
if (!$ORDERLOG) 
	$shouldLog = false;
if ($shouldLog)
{
	fputs($ORDERLOG,"++++++++++++++++++++++++STARTING ORDER PROCESSING FOR".$login." at ".date("F j, Y, g:i a")."++++++++++++++++\n");
	$sessionDumpStr = "";
	ob_start();
	var_dump($XCART_SESSION_VARS);
	$sessionDumpStr = ob_get_contents();
	ob_end_clean();
	fputs($ORDERLOG,"\tSession ID - ".$XCARTSESSID."\n");
	fputs($ORDERLOG,"\tSession Dump - ".$sessionDumpStr."\n");
}
if(empty($login))
{
	//header("location:mkMyAccount.php");
	$errormessage = "We do not have your login due to session timeout. We apologize for the inconvenience. Your items are still in the cart. Please try checking out again from your cart";
	 x_session_register("errormessage");

	if ($shouldLog) fputs($ORDERLOG,"**************Surprisingly session has been lost. Its a pity, but it has happened. Refer to session dump above form more details.\n");

	if ($shouldLog) fclose($ORDERLOG);
	
	func_header_location($http_location,false);
	exit;
}
else
{
	$mcartFactory= new MCartFactory();
	$myCart= $mcartFactory->getCurrentUserCart();
	if($myCart!=null){
		$productArray=MCartUtils::convertCartToArray($myCart);
		$productids = $myCart->getProductIdsInCart();
	}
	$productsInCart = $productArray;
	if( empty($productsInCart) )
	{
		//stale cart , cart is made empty from another tab/window - cannot proceed. Rebuild the cart again
		func_header_location("$http_location/mkmycart.php?at=c&pagetype=productdetail&src=po");
	}

	if($XCART_SESSION_VARS['LAST_CART_UPDATE_TIME']!=$myCart->getLastContentUpdatedTime()){
		$myCart->setLastContentUpdatedTime(time());
		MCartUtils::persistCart($myCart);
		func_header_location("$http_location/mkmycart.php?at=c&pagetype=productdetail&src=mi");
	}

	$couponCode = $XCART_SESSION_VARS['couponCode'];
	$cashCouponCode = $XCART_SESSION_VARS['cashCouponCode'];
	$userName = $XCART_SESSION_VARS['login'];
	$coupon = null;

	if(!empty($couponCode) || !empty($cashCouponCode))
	{
		$adapter = CouponAdapter::getInstance();
		$validator = CouponValidator::getInstance();
	}
	if(!empty($couponCode))
	{
	    $couponMessage = "";

	    // Find whether the coupon is still valid for the user.
	    $isValid = false;
		try {
            $coupon = $adapter->fetchCoupon($couponCode);
            try{
            	$isValid = $validator->isCouponValidForUser($coupon, $userName);
            }catch (CouponLockedException $ex){
            	$unblocked=findAndCancelPPOrderBlockingCoupon( $login,"DISCOUNT",$couponCode);
            	if($unblocked){
            		$coupon = $adapter->fetchCoupon($couponCode);
            		$isValid = $validator->isCouponValidForUser($coupon, $userName);
            	}else{
            		throw $ex;
            	}
            }catch (CouponLockedForUserException $ex){
            	$unblocked=findAndCancelPPOrderBlockingCoupon( $login,"DISCOUNT",$couponCode);
            	if($unblocked){
            		$coupon = $adapter->fetchCoupon($couponCode);
            		$isValid = $validator->isCouponValidForUser($coupon, $userName);
            	}else{
            		throw $ex;
            	}
            }
		}
		catch (CouponException $ex) {
		    $couponMessage = $ex->getMessage();
		    $couponMessage = "<font color=\"red\">$couponMessage</font>";
		}
	    
		//
		// If coupon is invalid, then clean up the coupon 
		// and redirect back to the cart page.
		//
		if (!$isValid) {
		    // XXX: Must find a way to notify the user.
		    // Currently, we are displaying the couponMessage.
		    $redirectedToCart = true;
		    x_session_register("redirectedToCart");
		    $weblog->info(
		    	"Redirecting back to the payment page. Reason: $couponMessage");
		    func_header_location($https_location . "/mkpaymentoptions.php",false);
		}
	}
	if(!empty($cashCouponCode))
        {
            $couponMessage = "";

            // Find whether the coupon is still valid for the user.
            $isValid = false;
                try {
                    $coupon = $adapter->fetchCoupon($cashCouponCode);
                    try{
                    	$isValid = $validator->isCouponValidForUser($coupon, $userName);
                    }catch(CouponLockedException $ex){
                    	$unblocked=findAndCancelPPOrderBlockingCoupon( $login,"CASH",$cashCouponCode);
                    	if($unblocked){
                    		$coupon = $adapter->fetchCoupon($cashCouponCode);
                    		$isValid = $validator->isCouponValidForUser($coupon, $userName);
                    	}else{
                    		throw $ex;
                    	}
                    }catch (CouponLockedForUserException $ex){
                    	$unblocked=findAndCancelPPOrderBlockingCoupon( $login,"CASH",$cashCouponCode);
                    	if($unblocked){
                    		$coupon = $adapter->fetchCoupon($cashCouponCode);
                    		$isValid = $validator->isCouponValidForUser($coupon, $userName);
                    	}else{
                    		throw $ex;
                    	}
                    }
                }
                catch (CouponException $ex) {
                    $couponMessage = $ex->getMessage();
                    $couponMessage = "<font color=\"red\">$couponMessage</font>";
                }

                //
                // If coupon is invalid, then clean up the coupon
                // and redirect back to the cart page.
                //
                if (!$isValid) {
                    // XXX: Must find a way to notify the user.
                    // Currently, we are displaying the couponMessage.
                    $redirectedToCart = true;
                    x_session_register("redirectedToCart");
                    $weblog->info(
                        "Redirecting back to the payment page. Reason: $couponMessage");
                    func_header_location($https_location . "/mkpaymentoptions.php",false);
                }
        }

	 
    $address = mysql_real_escape_string($_POST['address']);
    if(empty($address)) {
        $check = func_select_first('mk_customer_address', array('login' => $login,'default_address'=>1));
        if(!empty($check)){
            $address=$check['id'];
        } else {
	    	$address = func_select_first('mk_customer_address', array('login' => $login),0,1,array('datecreated' => 'desc'));
	    	if(!empty($address)) {
	        	$address=$address['id'];
	    	} else {        	
	            //no address present take him to address page
	            func_header_location($https_location . "/mkcustomeraddress.php?change=true",false);
	        }	    	
    	}     
    }
    $address = func_select_first('mk_customer_address', array('id' => $address));

	$s_country = $address['country'] ;
	$s_state = $address['state'] ;
	$s_city = $address['city'] ;
	$s_locality = $address['locality'] ;
    $s_zipcode = strtolower($address['pincode']);
	$s_email=$address['email'];
	
    	//validate shipping address
	if($s_country=="" || $s_state=="" || $s_city=="")
	{

		$errormessage = "Shipping address missing due to session time out. Please try checkout out your cart again. We apologize for the inconvenience.";
		x_session_register("errormessage");

		if ($shouldLog) fputs($ORDERLOG,"**************The shipping address has not been recorded. The session is probably lost.  Refer to session dump above form more details.\n");

		if ($shouldLog) 
			fclose($ORDERLOG);
	  	header("Location:mksystemerror.php");
		exit;
	}

	$shippingoptions = func_load_shipping_options();
	$cod_charge = 0;
	if($_POST['pm'] == "cod")
	{
		$totalAmount = $XCART_SESSION_VARS["totalAmount"];
		$codElegiblity = verifyCODElegiblityForUser($login, $address['mobile'],$address['country'], $totalAmount, strtolower($address['pincode']), $productsInCart, $productids);
		$codErrorCode = $codElegiblity['errorCode'];
		
		if($codErrorCode!=0) {
			//user not eligible for cod anymore	as conditions not satisfied.
			func_header_location($https_location . "/mkpaymentoptions.php?address=".$address['id'],false);		
		}
		$codCharge = FeatureGateKeyValuePairs::getInteger('codCharge');
		//additional discount for citibank
		if($codCharge>0) {
			$cod_charge = $codCharge;
		}
    }
	else {
		$logisticId = mysql_real_escape_string($_POST["s_option"]);//$shippingoptions[0]['code'] ;
	}
    $amount = $XCART_SESSION_VARS["amount"];
    $coupondiscount = $XCART_SESSION_VARS["coupondiscount"];
    $cart_amount = $amount;

	$shippingRate = func_shipping_rate_re($s_country,$s_state,$s_city,$productsInCart, $productids, $coupon, $logisticId,$s_zipcode, $cart_amount);
	// restore values from session
	$thisShippingRate = $shippingRate['totalshippingamount'];
	$box_count = $shippingRate['box_count'];
	
	$typearray = array();
	foreach($productsInCart AS $key=>$value)
	{
		$productDetail =   $value;
		$typeid = $productDetail['producTypeId'];

		###### Check for free shipping status
		if(array_key_exists($typeid, $typearray))
		{
			$typearray[$typeid] =  $typearray[$typeid] + $productDetail['quantity'];
		}
		else
		{
			$typearray[$typeid] = $productDetail['quantity'];
		}
	}
	$box_count=0;
	foreach($typearray AS $type=>$quantity)
	{
		$cquery = func_query_first("SELECT consignment_capacity FROM mk_shipping_prod_type WHERE product_type_id='".$type."'");
		$climit=$cquery[consignment_capacity];
		$totalunits = ceil($quantity/$climit);
		$box_count+=$totalunits;
	}

	$gift_charge_amount = 0;
	if(x_session_is_registered("giftdata")) {
		x_session_unregister("giftdata");
	}
	
	if($_POST['asgift'])
	{
		$giftdata = array();
		x_session_register("giftdata");
		$giftdata['asgift'] = "Y";
		$giftdata['gmessage'] = $_POST['gmessage'];
		$giftdata['giftpack'] = "Y";
		$giftdata['giftamount'] = 20;//irrespective of boxcount gift charge is 20rs/order 
		$XCART_SESSION_VARS['giftdata'] = $giftdata;		
	}

	$text = "";
	$checksumkey = uniqid();
	$weblog->info(">>>>>> vat value ".$_POST['hidevat']);
	if(!empty($HTTP_POST_VARS['ibibophotodiscount']))
	{
		$weblog->info(" Ibibo photo discount   ".$HTTP_POST_VARS['ibibophotodiscount']);
		$ibibophotodiscount =  $HTTP_POST_VARS['ibibophotodiscount'];
	}
	foreach($_POST AS $key=>$value)
	{
		if($key == 'totalamount')
		{
			$fieldvalue = strip_tags($thisTotalAmount);
			$fieldvalue = sanitize_float($fieldvalue);
		}
		elseif ($key == 'totalquantity')
		{
			$fieldvalue = strip_tags($value);
			$fieldvalue = sanitize_int($fieldvalue);
		}
		elseif ($key == 'shippingrate')
		{
			$fieldvalue = strip_tags($thisShippingRate);
			$fieldvalue = sanitize_float($fieldvalue);
		}
		elseif ($key == 'Amount')
		{
			$fieldvalue = strip_tags($thisAmount);
			$fieldvalue = sanitize_float($fieldvalue);
		}
		elseif ($key == 'hideref')
		{
			$fieldvalue = strip_tags($value);
			$fieldvalue = sanitize_float($fieldvalue);
		}
		elseif ($key == 'hidevat')
		{
			$fieldvalue = strip_tags($thisVat);
			$fieldvalue = sanitize_float($fieldvalue);
		}
		elseif ($key == 'hidetotamount')
		{
			$fieldvalue = strip_tags($value);
			$fieldvalue = sanitize_float($fieldvalue);
		}
		elseif ($key == 's_address')
		{
			$saddress = str_ireplace("\r\n"," ", $value);
			$fieldvalue = mysql_escape_string($saddress);
		}
		elseif ($key == 'b_address')
		{
			$baddress = str_ireplace("\r\n"," ", $value);
			$fieldvalue = mysql_escape_string($baddress);
		}
		elseif($key == 's_option')
		{
			$fieldvalue = $value;
		}
		elseif($key == 's_email' || $key == 'b_email')
		{
			$fieldvalue = $value;
		}
		else 
		{
			$fieldvalue = strip_tags($value);
			$fieldvalue = sanitize_paranoid_string($fieldvalue);
		}
		if($text == "")
		{
			$text .= "$key=>".$fieldvalue;
		}
		else
		{
			$text .= "||$key=>".$fieldvalue;
		}

	}
		
	if($_POST['billtoship']) $ship2diff = 'Y';
		else $ship2diff = 'N';

	$text = $text."||ship2diff=>".$ship2diff;

	$session_id = $XCARTSESSID;
	#load payment option
	$paybyoption = $_POST['pm'];
        


	
	
	$coupondiscount = $XCART_SESSION_VARS['coupondiscount'];
	$cashdiscount = $XCART_SESSION_VARS['cashdiscount'];
	$couponCode = $XCART_SESSION_VARS['couponCode'];
	$couponpercent = $XCART_SESSION_VARS['couponpercent'];
	// orignal code
	$tempOrderQuery = "INSERT INTO mk_temp_order(sessionid, parameter,checksumkey) VALUES('$session_id', '$text','$checksumkey')";

	if ($shouldLog) 
		fputs($ORDERLOG,"Inserting order into temp order table. Query is - ".$tempOrderQuery."\n");
	db_query($tempOrderQuery);

	// #1496:remove parameter from where clause and add checksumkey
	$retrieveTempOrderQuery = "SELECT orderid FROM mk_temp_order WHERE sessionid='$session_id' AND checksumkey='$checksumkey'";

	if ($shouldLog) 
		fputs($ORDERLOG,"Retrieving orderId from mk_temp_order. Query is - ".$retrieveTempOrderQuery."\n");

	$tempresult = db_query($retrieveTempOrderQuery);
	$row = db_fetch_array($tempresult);
	$Order_Id = $row['orderid'];
	if ($shouldLog) 
		fputs($ORDERLOG,"Retrieved orderId from mk_temp_order. Order Id is - ".$Order_Id."\n");

	############ START ENTER ORDER DETAILS IN THE SYSTEM IN PREPROCESSED STATE########

	//Database tables
	global $state_array;
	$tb_order_details = $sql_tbl['order_details'];
	$tb_products = $sql_tbl['products'];
	$tb_order_options= $sql_tbl['mk_product_options_order_details_rel'];
	$tb_order_customization_detail = $sql_tbl['mk_xcart_order_details_customization_rel'];
	$tb_order_image_order_detail =   $sql_tbl['mk_xcart_order_customized_area_image_rel'];
	$tb_orders = $sql_tbl["orders"];
	$tb_giftcerts =  $sql_tbl["giftcerts"];
	$tb_customer =  $sql_tbl["customers"];
	$tb_temporder =  $sql_tbl["temp_order"];
	$tb_wishlist = $sql_tbl['wishlist'];
	$tb_product_options=$sql_tbl['mk_product_options'];
	$tb_mk_order_item_option_quantity=$sql_tbl['mk_order_item_option_quantity'];
	$orderid = $Order_Id;
	//for testing in staging becasue orderid should be unique else ccavenue will give duplicate orderid error
	//$orderid=$orderid."123";
	$giftdata = $XCART_SESSION_VARS['giftdata'];

	#
	#load gift message information
	#
	if($giftdata['asgift'] == 'Y')
	{
		$giftstatus = $giftdata['asgift'];
		$giftmessage = addslashes($giftdata['gmessage']);
		$giftpack = $giftdata['giftpack'];
		$giftamount = $giftdata['giftamount'];
	}
	else
	{
		$giftstatus = "N";
		$giftpack = "N";
		$giftamount = 0;
	}

	#Call function to update and return the array of billing and shipping address

	$weblog->info("Called func_update_billing_shipping_address for updating the billing and shipping address") ;
	$customer = func_update_billing_shipping_address($login, $orderid, $session_id);
	$weblog->info("func_update_billing_shipping_address return an array of billing and shipping address");

	//Check if product exist or not
	//If not insert a product
	//Call function to insert not published product
	foreach($productsInCart as $key=>$productDetail)
	{
		$xcart_ipt_160 ='';
		$xcart_ti = '';
		$is_published = $productDetail["is_published"];
		$productId = $productDetail["productId"];
		$styleId = $productDetail["productStyleId"];
		$typeId = $productDetail["producTypeId"];
		$listPrice = $productDetail["productPrice"];
		$templateid = empty($productDetail["template_id"])?0:$productDetail["template_id"];
		$addonid = empty($productDetail["addon_id"])?0:$productDetail["addon_id"];
		$time = time();

		//on refresh - shouldnot enter again - so check against the DB also
		$query = "select count(*) from xcart_products where productid=$productId";
		$res = db_query($query);
		$row = db_fetch_row($res);

		if( $row[0] ==  1)
			$is_published = 1;
		
		if( !$is_published )
		{
	        //if non customizable product
       		if( empty($xcart_ipt_160) && empty($xcart_ti) && isset($productDetail['is_customizable']) &&  $productDetail['is_customizable'] == '0')
			{
				$res=func_query_first("select * from mk_style_properties where style_id=$styleId");
				$def_image=$res['default_image'];
				$thumb_image=str_replace("_images","_images_96_128",$def_image);
				$image_portal_thumbnail_160=str_replace("_images","_images_96_128",$def_image);
				$xcart_ipt_160 = $image_portal_thumbnail_160;
				$xcart_ti = $thumb_image;
			}
            //if non customizable product
                    if( empty($xcart_ipt_160) && empty($xcart_ti) && isset($productDetail['is_customizable']) &&  $productDetail['is_customizable'] == '0')
					{
                        $res=func_query_first("select * from mk_style_properties where style_id=$styleId");
                        $def_image=$res['default_image'];
                        $thumb_image=str_replace("_images","_images_96_128",$def_image);
                        $image_portal_thumbnail_160=str_replace("_images","_images_96_128",$def_image);
                        $xcart_ipt_160 = $image_portal_thumbnail_160;
						$xcart_ti = $thumb_image;
					}
			//Now insert into xcart_products 
			$query = "insert into xcart_products
					(productid,productcode,provider,list_price,add_date,forsale,designer,statusid,product_type_id,product_style_id,descr,fulldescr,
					image_portal_t,is_publish,image_portal_thumbnail_160)
					values
					($productId,'MK$productId','$login',$listPrice,$time,'Y',0,1009,$typeId,$styleId,'','',
					'$xcart_ti',0,'$xcart_ipt_160')";
			db_query($query);
		}
	}

	//Computing the final amount to be paid
	#########################################################


	$discountRatioSum = 0;
	$productTypeQuantityArray = array();
	foreach($productsInCart AS $key=>$value)
	{
		$productDetail = $value;
		#
		#Find the sum of discount ratio
		#
		if($productDetail['discount'] == 0)
			$discountRatioSum += $productDetail['totalPrice'];
		$subtotal = $subtotal + $productDetail['totalPrice'];
		$totalQuantity +=  $productDetail['quantity'];

		/***************************************/
		#
		#Create an array for quantities of product type 
		#
		if(array_key_exists($productDetail['producTypeId'], $productTypeQuantityArray))
		{
			$productTypeQuantityArray[$productDetail[producTypeId]]['qty'] = $productTypeQuantityArray[$productDetail[producTypeId]]['qty'] + $productDetail['quantity'];
			$productTypeQuantityArray[$productDetail[producTypeId]]['stylecount'] = $productTypeQuantityArray[$productDetail[producTypeId]]['stylecount'] + 1;   
		}
		else
		{
			$productTypeQuantityArray[$productDetail[producTypeId]]['qty'] = $productDetail['quantity'];
			$productTypeQuantityArray[$productDetail[producTypeId]]['stylecount'] = 1;
		}
	}
	
	####################### start insert order details into database #################################
			
	$time =time();
	$firstname = mysql_escape_string($address['name']);
	$lastname = "";

	$shipping_option_name = $customer['s_option'];
	$saddress = mysql_escape_string(str_ireplace("\r\n"," ", $address['address']));
	$scountry = ($address['country'] == 'In')?'India':$customer['s_country'];
	$scity = mysql_escape_string($address['city']);
	$s_state = mysql_escape_string($address['state']);
	$s_zipcode = $address['pincode'];
	$phone = $address['phone'];
	$mobile =$address['mobile'];
    // Below fields are not available any more ..
	$baddress = mysql_escape_string($customer['b_address']);
	$bcountry = mysql_escape_string(($customer['b_country'] == 'In')?'India':$customer['b_country']);
	$bcity = mysql_escape_string($customer['b_city']);
	$b_state = mysql_escape_string($customer['b_state']);
	$b_zipcode = mysql_real_escape_string($customer['b_zipcode']);

    $channel="web"; // default channel to web
	$orderstatus = "PP";
	// Due to legacy this code has been hacked, If this code is removed .. reports will break
    // Basically $paybyoption has two possible values on and chq
    
    if($paybyoption == 'otherpayments' && $_POST['otherpaymentstype'] == 'checkdd' ){
        $paymentOption = 'chq';
    }
    elseif ($paybyoption == "cod") {
    	// Pravin - COD Feature. Setting payment option to 'cod'. In xcart_orders, such orders will store payment_method as 'cod'
    	$paymentOption = 'cod';
    	// In case of Cash on Delivery option, the initial order status should be set to PV which implies "Pending Verification"
		$orderstatus = "PV"; 
		// After SMS Verification is done, this status should be changed to 'OH' (on hold for 12 hours) 
    }
    elseif ($paybyoption == "NP") {
        $paymentOption = 'NP';
    } elseif ($paybyoption == "ivr") {
    	$paymentOption = 'ivr';
    	$channel="ivr"; //change channel to ivr in case of pay by phone
    }
    else {
        $paymentOption = 'on'; 
    }

	#################################################################################
	# Section to add custom source code and source id for orders from users with 	#
	# account type as PU								#
	#################################################################################

	$query = "select source_id,source_code from mk_sources where source_manager_email = '$login' and EXISTS(select * from xcart_customers where login='$login' and (account_type='PU' or account_type='P2'))";
	$source_data=func_query_first($query);
	if(array_key_exists('source_id',$source_data)){
		$user_source_id 	= $source_data['source_id'];
		$user_source_code	= $source_data['source_code'];
	}elseif($fromrelianceip == 1 ){
		$user_source_id		= 249;
		$user_source_code	= 'RLN0014';
	}else{
		$user_source_id		= $order_source_id;
		$user_source_code	= $order_source_code;
	}
	#########################
	# End of Section	#
	#########################

    //issue_contact_number is never get posted other than 'cod' case then customer mobile number is taken
    if(!empty($HTTP_POST_VARS['issues_contact_number'])){
        $issues_contact_number = mysql_real_escape_string($HTTP_POST_VARS['issues_contact_number']);
    } else {
        $issues_contact_number = $address['mobile'];
    }

	$shipment_preferences = mysql_real_escape_string($HTTP_POST_VARS['shipment_preferences']);
	
	$mrp = $XCART_SESSION_VARS["mrp"];
	// mrp is equal to vat + amount ( back calculated in cart page )
	$vat = $XCART_SESSION_VARS["vat"];
	$amount = $XCART_SESSION_VARS["amount"];
	if( isset($XCART_SESSION_VARS['coupondiscount']) )
	{
		$coupondiscount = $XCART_SESSION_VARS['coupondiscount'];
		$couponCode = $XCART_SESSION_VARS['couponCode'];
		$couponpercent = $XCART_SESSION_VARS['couponpercent'];
	}
	else
	{
		$coupondiscount  = 0;$couponCode = "";$couponpercent = "";
	}
	if(!empty($XCART_SESSION_VARS['cashdiscount']))
	{
		$cash_coupon_code=$XCART_SESSION_VARS['cashCouponCode'];
		$cashredeemed=$XCART_SESSION_VARS['cashdiscount'];
	}
	else
	{
		$cash_coupon_code="";
		$cashredeemed=0;
	}
	if( isset($XCART_SESSION_VARS['productDiscount']) )
	{
		$productDiscount = $XCART_SESSION_VARS['productDiscount'];
	}
	else
	{
		$productDiscount = 0.0;
	}
	
	$cod_payment_status='';
	
	$pg_discount = 0; // field to use for payment gateway discounts like citi card discount / online discounts / anyother payment page discount
	$final_total_amount = $amount - $coupondiscount + $cod_charge + $thisShippingRate + $giftamount - $cashredeemed - $productDiscount; // used to pass on to ccavenue
	$amountWithProductDiscount = $amount - $productDiscount; //use to divide payment gateway discount (pg_discount) propotionally in for xcart_order_details table.      
	$subtotal = $amount;
	$total = $subtotal-$coupondiscount - $cashredeemed - $productDiscount;
	$total = $total < 0 ? 0 : $total;
	if( $paybyoption == 'debitcard' || $paybyoption == 'creditcards' ) {
        $ccNumber = $HTTP_POST_VARS['card_number'];
        $ccNumber = str_replace('-', '', $ccNumber);    // baseline single quote
		$ccNumber = str_replace(' ', '', $ccNumber);    // baseline single quote
		$bin = substr($ccNumber, 0,6);
		$pg_discount = PaymentGatewayDiscount::getDiscount($total, $amountWithProductDiscount,$bin);
	} elseif($paybyoption == 'netbanking') {
		$activeBanks = NetBankingHelper::getActiveBanksList();
		$bank_id = $_POST['netBankingCards'];
		$bankName = $activeBanks[$bank_id]['name'];
		if(stripos($bankName, "icici") !== false) { //icici bank
			$icici_discount = FeatureGateKeyValuePairs::getInteger('payments.iciciBankCardDiscount');
			if($icici_discount>0) {				
				$pg_discount = $total*$icici_discount/100;
				$pg_discount = round($pg_discount, 2);			
			}
		}
	}
	
	$final_total_amount = $final_total_amount - $pg_discount;
	$total = $total - $pg_discount;
	$final_total_amount = number_format($final_total_amount, 2, ".", '');
    $total = number_format($total, 2, ".", '');
	$final_total_amount = $final_total_amount < 0 ? 0 : $final_total_amount;
    $total = $total < 0 ? 0 : $total;

    /* Pravin Cleanup: All useless fields being set to 0, so that insert query does not break. */ 
    if(empty($affiliateId))
		$affiliateId = 0;
    if(empty($paymentOptionId))
		$paymentOptionId =0;
    if(empty($refferalAmt))
		$refferalAmt = 0;
    if(empty($shopid))
		$shopid=0;
    if(empty($ammaGUID))
		$ammaGUID = 0;
    if(empty($affdiscount))
		$affdiscount =0 ;
    if(empty($afforderid))
		$afforderid =0 ;
    if(empty($orgId))
		$orgId =0 ;
	/* End of useless data reset */
		
	 
	$query = "insert into $tb_orders
				(orderid, login,membership,total,giftcert_discount,giftcert_ids,subtotal, discount,coupon,
				coupon_discount,shippingid,tracking,shipping_cost,tax,taxes_applied,date,status,payment_method,
				flag,details,customer_notes,notes,extra,customer,title,firstname,lastname,company,
				b_title,b_firstname,b_lastname,b_address,b_city,b_state,b_country,b_zipcode,b_mobile,b_email,
				s_title,s_firstname,s_lastname,s_address,s_locality,s_city,s_state,s_country,s_zipcode,s_email,
				phone,fax,url,email,paymentid, mobile,gift_status,gift_pack,ref_discount, gift_charges, 
				affiliateid, cod,shopid,guid,aff_discount,aff_orderid,orgid,issues_contact_number,shipment_preferences,additional_info,order_name,source_id, 
				cod_payment_date, cod_pay_status,cash_redeemed,cash_coupon_code,pg_discount,channel, request_server, group_id)
				values 
				('$orderid', '$login','','$total',0.0,'','$subtotal','$productDiscount','$couponCode',
				'$coupondiscount',0,'','$thisShippingRate','$vat','$taxApplied','$time','$orderstatus','$paymentOption',
				'N','','$notes','$giftmessage','','$login','".$customer['title']."','".$firstname."','".$lastname."','".$customer['company']."',
				'".$customer['b_title']."','".$customer['b_firstname']."','".$customer['b_lastname']."','".$baddress."','".$bcity."','".$b_state."','".$bcountry."','".$b_zipcode."','{$mobile}','{$b_email}',
                '".$customer['s_title']."','".$firstname."','".$lastname."','".$saddress."','".$s_locality."','".$scity."','".$s_state."','".$s_country."','".$s_zipcode."','{$s_email}',
				'".$phone."','".$customer['fax']."','".$customer['url']."','".$loginEmail."','$paymentOptionId', '".$mobile."','".$giftstatus."','".$giftpack."','".$refferalAmt."','".$giftamount."', 
				'".$affiliateId."', '".$cod_charge."','".$shopid."','".$ammaGUID."','".$affdiscount."','".$afforderid."','".$orgId."','".$issues_contact_number."','".$shipment_preferences."','".$additional_info."','".$user_source_code.'-'.$orderid."','".$user_source_id."', 
				NULL, NULL ,'$cashredeemed','$cash_coupon_code','$pg_discount','$channel', '$server_name', $orderid)";   // Pravin - COD columns added.. (1) cod_payment_date, (2) cod_pay_status

	//++++++++ log order query +++++++++
	if ($shouldLog) fputs($ORDERLOG,"INSERTING INTO ORDER TABLE - Query is :\n\t".$query."\n");
		$weblog->info("ICICI PROMO CODE check for orderid {$orderid}  in file ".__FILE__." in line ".__LINE__." query => {$query}") ;
	db_query($query);
	
	// PTG campaign specific code.
	global $system_ptg_campaign;
	if ($system_ptg_campaign && (!empty($XCART_SESSION_VARS["qualified_for_ptg_campaign"]) || !empty($XCART_SESSION_VARS["qualified_for_surprise_ptg_campaign"]))) {
	    $query = 
	    	"INSERT INTO mk_ordercommentslog 
	    	    (orderid, commenttype, commenttitle, commentaddedby, description, commentdate) 
	    	 VALUES
	    	    ('$orderid', 'Order Change', 'Free Watch', 'Admin', 'Add a free Fastrack watch while shipping.', " . time() . ");";
	    
	    db_query($query);
	    $commentid = db_insert_id();
	    
	    $query = "UPDATE " . $tb_orders . " SET changerequest_status = 'Y' WHERE orderid = '$orderid';";
	    db_query($query);
	}

	if(!empty($couponCode)) {
		# Update the number of times the coupon has been used once the order is placed
			$adapter->lockCouponForUser($couponCode, $userName);
	}
	if(!empty($cashCouponCode)) {
                # Update the number of times the coupon has been used once the order is placed
                        $adapter->lockCouponForUser($cashCouponCode, $userName);
        }
	
	$orderId = $orderid;  //What is this ????
	$productid ;
	$product ='';
	$totalQty = 0;
	$productTypeNames ='';

	foreach($productsInCart as $key=>$value)
	{
			$productDetail = $value;

			// Fetch product detail for the product
			$productid =$productDetail['productId'];

			//makeup quntitybreakup string for product
			$qcount = count($productDetail['sizenames']);
			$quantity_breakup = "";
			for($i=0;$i<$qcount;$i++)
			{
				if($i!=$qcount-1)
					$quantity_breakup = $quantity_breakup.$productDetail['sizenames'][$i].":".$productDetail['sizequantities'][$i].",";
				else
					$quantity_breakup = $quantity_breakup.$productDetail['sizenames'][$i].":".$productDetail['sizequantities'][$i];
			}
			#
			#shipping rate for the city
			#
			$logisticId = $HTTP_POST_VARS['s_option'];
			#
			#create an array of products for each elements 
			#
			$productInCart = array();
			$productInCart[] = $productDetail;
			$productid_array = array();
			 
			$shipping_city = strtolower($scity);
			$shippingRateArray = func_shipping_rate_re($scountry,$s_state, $shipping_city, $productInCart, $productid_array, $coupon, $logisticId,$s_zipcode, $cart_amount);
			
			$shippingRate = $shippingRateArray['shiprate'];
			 
			$consignmentCapacity = func_query_first_cell("SELECT consignment_capacity FROM $sql_tbl[shipping_prod_type] WHERE product_type_id='".$productDetail[producTypeId]."'");
			$totalunits = ceil($productTypeQuantityArray[$productDetail[producTypeId]]['qty']/$consignmentCapacity);
			$shippingTotalCost = ($totalunits *  $shippingRate)/$productTypeQuantityArray[$productDetail[producTypeId]]['stylecount'];
             //added by nishith
            if(empty($thisShippingRate)){
                $shippingTaxAmount=0;
                $shippingAmount=0;
            }else{
                $shippingTaxAmount = ($shippingTotalCost * ($vatRate/100));
                $shippingAmount = $shippingTotalCost - $shippingTaxAmount;
			}

			$vatRate = $productDetail["vatRate"];
			$actual_product_price  = $productDetail["actualPriceOfProduct"];
			$cash_redeemed  = $productDetail["cashdiscount"];
			
			$vatAmount = number_format($productDetail["unitVat"] * $productDetail["quantity"],2,".",'');
			$totalAmount = number_format(($productDetail["productPrice"] * $productDetail["quantity"]) - $productDetail['discountAmount'],2,".",'');
			$pg_discount_product = number_format(($totalAmount*$pg_discount)/$amountWithProductDiscount,2,".",'');
			$productPrice = $productDetail["productPrice"];
			$is_customizable = isset($productDetail["is_customizable"])?$productDetail["is_customizable"]:1;
			$coupon_discount = $productDetail['couponDiscount'];
			$cash_coupon_code=$XCART_SESSION_VARS['cashCouponCode'];
			$discountOnProduct  = ($productDetail['discountAmount'] != 0) ? $productDetail['discountAmount'] : $productDetail["systemDiscount"];
			$discountQuantity = $productDetail['discountQuantity'];
			$cartDiscountSplitOnRatio = $productDetail['cartDiscount'];
			$product_discount_rev_id = $productDetail['discountRuleRevId'];
			$product_discount_ruleid = $productDetail['discountRuleId'];
			$product_is_discountable = $productDetail['isDiscounted'];
			$product_is_returnable = $productDetail['isReturnable'];
			
			$styleId = $productDetail["productStyleId"];
			
			$jit_purchase = 0;
			
			$skuid = $productDetail["skuid"];
			$sku_details = SkuApiClient::getSkuDetailsWithInvCountsForIds($skuid);
			if($sku_details[0]['invCount'] <= 0 && $sku_details[0]['jitSourced']){
				$jit_purchase = 1;
			}
			
			
			/** Pravin Cleanup - resetting useless date */
			if(empty($promotion_code)) 
				$promotion_code = 0;
				
			$style_template = $productDetail['template_id'];
			
			if(empty($style_template)) 
				$style_template = 0;
				
			if(empty($product_discount_ruleid)) 
				$product_discount_ruleid = "null";
				
			if(empty($product_discount_algoid)) { 
				$product_discount_algoid = "null";
			} else { 
				$product_discount_algoid = "'$product_discount_algoid'";
			}
				
			if(empty($product_discount_data)) { 
				$product_discount_data = "null";
			} else {
				$product_discount_data = "'$product_discount_data'";
			} 				
				
			/* End reset data for query correctness */
			
			//Insert data in xcart_order_detail
			$query ="insert into $tb_order_details (
						orderid,productid,price,amount,provider,product,product_style,product_type, discount, discount_quantity, cart_discount_split_on_ratio,
						actual_product_price,quantity_breakup, location, item_status, tax_rate, taxamount, shipping_amount, 
						shipping_tax, total_amount,promotion_id,style_template,is_customizable,coupon_code,
						coupon_discount_product,cash_redeemed,cash_coupon_code,discount_rule_id,discount_rule_rev_id,
						is_discounted, is_returnable,  jit_purchase,pg_discount, product_options, extra_data 
					) values (
						'".$orderId."','".$productid."','".$productPrice."','".$productDetail['quantity']."','".$designer."','".$product."','".$productDetail['productStyleId']."','".$productDetail['producTypeId']."', '".$discountOnProduct."','".$discountQuantity."','".$cartDiscountSplitOnRatio."',
						'".$actual_product_price."', '".$quantity_breakup."','".$productDetail['operation_location']."', 'UA', '".$vatRate."', '".$vatAmount."', '".$shippingAmount."', 
						'".$shippingTaxAmount."', '".$totalAmount."','".$promotion_code."','".$style_template."','".$is_customizable."','".$couponCode."',
						'".$coupon_discount."','".$cash_redeemed."','".$cash_coupon_code."','".$product_discount_ruleid."','".$product_discount_rev_id."',
						'".$product_is_discountable."','".$product_is_returnable."','$jit_purchase','$pg_discount_product', '', '' 
					)";
			db_query($query);
			$last_id_rs = func_query("select last_insert_id() as id");
			$item_id=$last_id_rs[0]['id'];
			$totalQty = $totalQty + $productDetail['quantity'];
			
			$productOptionIds = func_query_first("SELECT option_id from mk_styles_options_skus_mapping where sku_id=".$skuid);
			$item_option_id = $productOptionIds['option_id']; 
			$item_size_quanity = $productDetail['quantity'];
			$sql_query="insert into $tb_mk_order_item_option_quantity (itemid,optionid,quantity) values($item_id,$item_option_id,$item_size_quanity)";
			db_query($sql_query);			
	}
		  
	$UpdatesQtyquery ="UPDATE $tb_orders  SET qtyInOrder=$totalQty WHERE orderid = '".$orderId."'";
	db_query($UpdatesQtyquery);
	if(x_session_is_registered("orderID")){
		x_session_unregister("orderID");
	}
	x_session_register("orderID", $orderId);
			
    ############ END ENTER ORDER DETAILS IN THE SYSTEM IN PREPROCESSED STATE #########
   
    $Merchant_Id = "M_xmukesh_5506" ;//This id(also User Id)  available at "Generate Working Key" of "Settings & Options"
    $Amount = $final_total_amount;//$_POST['Amount'];//your script should substitute the amount in the quotes provided here

    $userType = $XCART_SESSION_VARS['login_type'];        
			
    if($paybyoption == 'cod') {
        $formAction = $https_location."/mkorderBook.php?type=cod&orderid=".$Order_Id ."&".$XCART_SESSION_NAME."=".$session_id;
        $redirectmessage ="Please wait... while we process your order";
        $query = "update xcart_orders set cod_pay_status='pending' where orderid = '$orderId'";
        db_query($query);

    } else if ($paybyoption == "ivr") {
    	$formAction = $https_location."/mkphoneOrder.php?orderid=".$Order_Id;
    	$redirectmessage = "Please wait... while we process your order";
    } else if($Amount == 0 || ($paybyoption == 'otherpayments' && $_POST['otherpaymentstype'] == 'checkdd' )){
        $formAction = $https_location."/mkorderBook.php?type=aff&orderid=".$Order_Id."&".$XCART_SESSION_NAME."=".$session_id;
        $redirectmessage = "Please wait... while we process your order";

    }else{
        $formAction ="https://www.ccavenue.com/shopzone/cc_details.jsp";
        $redirectmessage = "Please wait... while redirecting to ccavenue. It may take some time";
    }

    //passing channel(utm_source and utm_medium) info for order in payment redirect url
    $cookieContent = getUnserializedCookieContent('utm_track_1');

    $Redirect_Url = $https_location."/mkorderBook.php?orderid=".$Order_Id."&".$XCART_SESSION_NAME."=".$session_id."&utm_source_confirm=".$cookieContent['utm_source']."&utm_medium_confirm=".$cookieContent['utm_medium'];

    //your redirect URL where your customer will be redirected after authorisation from CCAvenue

    $WorkingKey = "bj7xvl403fy6qlfth4n1m7sjdgyzwy28";//put in the 32 bit alphanumeric key in the quotes provided here.Please note that get this key ,login to your CCAvenue merchant account and visit the "Generate Working Key" section at the "Settings & Options" page.
    $ebs_secret_key = "b9e5685ab6439dcaa55013a27f44decf";	 // Your Secret Key
    $ebs_account_id = 6047;
    $ebs_mode ="LIVE";

    if($configMode=="local"||$configMode=="test"||$configMode=="release"){
        if($configMode=="release"){
            $Checksum = getCheckSum($Merchant_Id,$Amount,"RELE_".$Order_Id ,$Redirect_Url,$WorkingKey);
            $ebs_string = "$ebs_secret_key|$ebs_account_id|$Amount|RELE_$Order_Id|$Redirect_Url|$ebs_mode";
        } else {
            $Checksum = getCheckSum($Merchant_Id,$Amount,"ALPHA".$Order_Id ,$Redirect_Url,$WorkingKey);
            $ebs_string = "$ebs_secret_key|$ebs_account_id|$Amount|ALPHA$Order_Id|$Redirect_Url|$ebs_mode";
        }
        $ebs_secure_hash = md5($ebs_string);
    }
    else {
        $Checksum = getCheckSum($Merchant_Id,$Amount,$Order_Id ,$Redirect_Url,$WorkingKey);
        $ebs_string = "$ebs_secret_key|$ebs_account_id|$Amount|$Order_Id|$Redirect_Url|$ebs_mode";
        $ebs_secure_hash = md5($ebs_string);
    }
		
		//*******************************************************

		
			
			
		if(!empty($_POST['b_firstname'])) {
			$billing_cust_fname= strip_tags($_POST['b_firstname']);		
			$billing_cust_fname= sanitize_paranoid_string($billing_cust_fname);
			$billing_cust_name = $billing_cust_fname;	
		} else {
			$billing_cust_name = sanitize_paranoid_string($address['name']);			
		}
		
		if(!empty($_POST['b_lastname'])) {
			$billing_cust_lname= strip_tags($_POST['b_lastname']);	
			$billing_cust_lname= sanitize_paranoid_string($billing_cust_lname);	
			if(!empty($billing_cust_fname)) $billing_cust_name = $billing_cust_fname." ".$billing_cust_lname;
			else $billing_cust_name = $billing_cust_lname;	
		} else {
			$billing_cust_name = sanitize_paranoid_string($address['name']);	
		}
		
		if(!empty($_POST['b_address'])&&$_POST['b_address']!="Enter your billing address here") {
			$billing_cust_address = str_ireplace("\r\n"," ", $_POST['b_address']);				
		} else {
			$billing_cust_address = str_ireplace("\r\n"," ", $address['address']);				
		}
		$billing_cust_address = sanitize_paranoid_string($billing_cust_address);

		$billing_cust_address = htmlspecialchars($billing_cust_address, ENT_QUOTES);
		
		
		if(!empty($_POST['b_country'])) {
			$billing_cust_country= strip_tags($_POST['b_country']);			
		} else {
			$billing_cust_country=strip_tags($address['country']);
		}
		
		$billing_cust_country= sanitize_paranoid_string($billing_cust_country);

		if(!empty($_POST['b_city'])&&$_POST['b_city']!="City") {
			$billing_cust_city= strip_tags($_POST['b_city']);				
		} else {
			$billing_cust_city= strip_tags($address['city']);
		}
		
		$billing_cust_city= sanitize_paranoid_string($billing_cust_city);
		
		if(!empty($_POST['b_state'])&&$_POST['b_state']!="State") {
			$billing_cust_state=strip_tags($_POST['b_state']);				
		} else {
			$billing_cust_state=strip_tags($address['state']);
			$billing_cust_state=sanitize_paranoid_string($state_array[$billing_cust_state]);
		}
		
		$billing_cust_state=sanitize_paranoid_string($billing_cust_state);
		
		$billing_cust_tel= strip_tags($mobile);
		$billing_cust_tel= sanitize_paranoid_string($billing_cust_tel);

		if(!empty($_POST['b_email'])) {
			$billing_cust_email= strip_tags($_POST['b_email']);				
		} else {
			$billing_cust_email= strip_tags($address['email']);
		}
		
		$billing_cust_email= fsanitize_email($billing_cust_email);
		
		if(!empty($_POST['b_zipcode']) && $_POST['b_zipcode']!="PinCode" ) {
			$billing_zip_code= strip_tags($_POST['b_zipcode']);				
		} else {
			$billing_zip_code=strip_tags($address['pincode']);
		}
		
		$billing_zip_code= sanitize_paranoid_string($billing_zip_code);
						
		$delivery_cust_name = sanitize_paranoid_string($address['name']);

		$delivery_cust_address = str_ireplace("\r\n"," ", $address['address']);
		$delivery_cust_address = sanitize_paranoid_string($delivery_cust_address);
		$delivery_cust_address = htmlspecialchars($delivery_cust_address, ENT_QUOTES);

		$delivery_cust_country=strip_tags($address['country']);
		$delivery_cust_country=sanitize_paranoid_string($delivery_cust_country);

		$delivery_cust_city=strip_tags($address['city']);
		$delivery_cust_city=sanitize_paranoid_string($delivery_cust_city);

		$delivery_cust_state=strip_tags($address['state']);
		$delivery_cust_state=sanitize_paranoid_string($state_array[$delivery_cust_state]);

		$delivery_cust_tel=strip_tags($address['mobile']);
		$delivery_cust_tel=sanitize_paranoid_string($delivery_cust_tel);

		$delivery_zip_code=strip_tags($address['pincode']);
		$delivery_zip_code=sanitize_paranoid_string($delivery_zip_code);
		
		
		if ($shouldLog){
			fputs($ORDERLOG,"Sending order [".$Order_Id."] to CC Avenue. \n");
			fclose($ORDERLOG);
		}
		//CCAvenue identifies full country name, we should send country name, not country code. Eg. India for IN
		$returnCountry = func_get_countries();
		$billing_cust_country_code= $billing_cust_country;
		$billing_cust_country = $returnCountry[$billing_cust_country];
		$shipping_cust_country =$returnCountry[$delivery_cust_country];

        // Payment method chosen
        $payment_method = $_POST['pm'];
        $gateway = "ccavenue";
        $vpcURL = "https://migs.mastercard.com.au/vpcpay";
        if($payment_method == 'netbanking') {
            $gateway = "ccavenue";
            $bank_id = $_POST['netBankingCards'];
            $netbanking_gateways = NetBankingHelper::getActiveGatewaysListForBank($bank_id);
            $gatewaySelected = getNetBankingGateway($netbanking_gateways);             
            if(empty($gatewaySelected)) {
            	//no supporting gateway for the bank found redirecting him to payment page if the bank is show in the payment page bank list its time to clear xcache.
            	$weblog->info("Redirecting back to the payment page. Reason: No suitable gateway found for $bankName, id= $bank_id");
            	func_header_location($https_location . "/mkpaymentoptions.php?transaction_status=N",false);
            }
            $gateway = strtolower($gatewaySelected['name']);
            $gateway_code = strtoupper($gatewaySelected['gateway_code']);    

            if($gateway=='tekprocess') {
            	require_once "$xcart_dir/include/tekprocess/getcheck.php";
            	//echo "$gateway_code";
            	$tekprocessFromToSubmit = TekProcessPaymentProcess::getFormToSubmit($Order_Id, $Amount, $gateway_code);           	
            	if(empty($tekprocessFromToSubmit)) {
            		//if fail try to get another gateway except tekprocess
            		if(count($netbanking_gateways)>1) {
            			//we have more than 1 gateway available try to hit other gateway other than tekprocess
            			$new_netbanking_gateways = array();  
            			foreach($netbanking_gateways as $gateway) {
            				if($gateway['name']!="tekprocess") {
            					$new_netbanking_gateways[] = $gateway; 
            				}
            			}          		
            			$gatewaySelected = getNetBankingGateway($new_netbanking_gateways);
            			$gateway = strtolower($gatewaySelected['name']);
            			$gateway_code = strtoupper($gatewaySelected['gateway_code']);            			            			
            		} else {
            			//only supporting gateway was tekprocess and its not available, we may want to switch off the the bank from payment lists.
            			$weblog->info("Redirecting back to the payment page. Reason: No suitable gateway found for $bankName, id=$bank_id");
            			func_header_location($https_location . "/mkpaymentoptions.php?transaction_status=N",false);
            		}            		 
            	}
            }
            
        } else if( $payment_method == 'debitcard' || $payment_method == 'creditcards' ) {	        
        	$cardIssuer = determineCardType($ccNumber);
        	$bankName = getIssuingBank($bin);
        	$icici_gateway_up = FeatureGateKeyValuePairs::getBoolean('payments.iciciPG');
        	$axis_gateway_up =  FeatureGateKeyValuePairs::getBoolean('payments.axisPG');
        	$icici_gateway_available = FeatureGateKeyValuePairs::getBoolean('payments.isICICIPGAvailable');
        	$axis_gateway_available = FeatureGateKeyValuePairs::getBoolean('payments.isAxisPGAvailable');
        	     	
        	if($cardIssuer=='VISA' || $cardIssuer=='MC') {
        		//choose the payment gateway depending on gateway availablity and other factors
        		$gatewayName = array();
        		$weights = array();
        		$gateway_cutoff_transaction = FeatureGateKeyValuePairs::getInteger('payments.inlinePGCutOffTrans',50);
        		$lastHour = time() - 3600;
        		if($bankName&&$icici_gateway_up&&$icici_gateway_available) {
        			//find the number of PP transaction for ICICI Gateway in last one hour        			
        			$iciciTimeOutCount = func_query_first_cell("select count(*) from mk_payments_log where payment_gateway_name='icici' and is_complete is null and time_insert>$lastHour");
        			if($gateway_cutoff_transaction>$iciciTimeOutCount) {
        				$gatewayName[] = "icici";
        				$weights[]= FeatureGateKeyValuePairs::getInteger('payments.icicPGWeight', 0);
        			}
        		}	       		
        		
        		$processInternationalTransactionInline = FeatureGateKeyValuePairs::getBoolean('payments.inlineInternationalTransaction',false);
        		
        		if(($bankName||$processInternationalTransactionInline)&&$axis_gateway_up&&$axis_gateway_available) {
        			$axisTimeOutCount = func_query_first_cell("select count(*) from mk_payments_log where payment_gateway_name='axis' and is_complete is null and time_insert>$lastHour");
        			if($gateway_cutoff_transaction>$axisTimeOutCount) {
        				$gatewayName[] = "axis";
        				$weights[]= FeatureGateKeyValuePairs::getInteger('payments.axisPGWeight', 0);
        			}
        		}    
        		     		
        		$gateway = selectRandomArrayElement($gatewayName, $weights);
        		
        		if(empty($gateway)) {
        			if(($bankName||$processInternationalTransactionInline)&&$axis_gateway_up&&$axis_gateway_available) {
        				$gateway = "axis"; //fall back to axis in case no other gateway is available
        			} elseif($bankName&&$icici_gateway_up&&$icici_gateway_available) {
        				$gateway = "icici"; //fall back to icici in case no other gateway is available
        			} else {
        				$gateway = "ccavenue";//fall back to ccavenue in case no other gateway is available
        			}
        		}
        		
        	} else {
        		//its always to icici if is it able to process it i.e it is in its bin list
        		if($bankName && $icici_gateway_up &&$icici_gateway_available) {
					$gateway = "icici";				
				} else {
					$gateway = "ccavenue";	
				}
        	}
        	        	
        	if($gateway=="axis") {
        		$axisBankParameter = array();
				$axisBankParameter['vpc_Amount'] = $Amount * 100;
        		$expiryMonth = $HTTP_POST_VARS['card_month'];
				$expiryYear = $HTTP_POST_VARS['card_year'];
				$expiryDate =$expiryYear.$expiryMonth; 
				if(empty($expiryDate)) {
					$expiryDate = "4912";
				}
				$axisBankParameter['vpc_CardExp'] = $expiryDate;
				$axisBankParameter['vpc_CardNum'] = $ccNumber;//5123456789012346/4557012345678902
				
        		$cardCVV = $HTTP_POST_VARS['cvv_code'];
				if(!empty($cardCVV)){
					$axisBankParameter['vpc_CardSecurityCode'] = $cardCVV; //optional
				}
				
				$axisBankParameter['vpc_Command'] = "pay";
				$axisBankParameter['vpc_Locale'] = "en";
				/*if($configMode=="local"||$configMode=="test"||$configMode=="release"){
					$axisBankParameter['vpc_Merchant'] = "TESTMYNTRA1";
					$axisBankParameter['vpc_AccessCode'] = "8A240123";
				} else {*/
					$axisBankParameter['vpc_Merchant'] = "MYNTRA1";
					$axisBankParameter['vpc_AccessCode'] = "6D6F7CD9";
				//}
				
				if($configMode=="local"||$configMode=="test"||$configMode=="release"){
					if($configMode=="release"){
						$axisBankParameter['vpc_MerchTxnRef'] = "RELE_".Order_Id; //orderid
					} else {
						$axisBankParameter['vpc_MerchTxnRef'] = "ALPHA".$Order_Id; //orderid
					}
				} else {
					$axisBankParameter['vpc_MerchTxnRef'] = $Order_Id; //orderid
				}
				$axisBankParameter['vpc_OrderInfo'] = $Order_Id;
				$axisBankParameter['vpc_ReturnURL'] = $Redirect_Url;
				$axisBankParameter['vpc_Version'] = "1";
				if($cardIssuer=='VISA'){
					$axisBankParameter['vpc_card'] = "Visa";
				} else {
					$axisBankParameter['vpc_card'] = "Mastercard"; //Visa/Mastercard/Amex
				}
				
				$axisBankParameter['vpc_gateway'] = "ssl"; //ssl/threeDSecure
				$axisBankParameter['vpc_AVS_Street01']=substr($billing_cust_address,0,128);
				$axisBankParameter['vpc_AVS_City']=substr($billing_cust_city,0,128);
				$axisBankParameter['vpc_AVS_StateProv']=substr($billing_cust_state,0,128);
				$axisBankParameter['vpc_AVS_PostCode'] = substr($billing_zip_code,0,9);
				
				$billing_ISO3country = getISO3CountryCode($billing_cust_country_code);
				if(empty($billing_ISO3country)) {
					$billing_ISO3country = "IND";
				}
				$axisBankParameter['vpc_AVS_Country']=$billing_ISO3country;
				$secureHash = hashAllFields($axisBankParameter);
				$axisBankParameter['vpc_SecureHash'] = $secureHash;
        	}
        	
        	if($gateway=="icici") {
        		$expiryMonth = $HTTP_POST_VARS['card_month'];
				$expiryYear = $HTTP_POST_VARS['card_year'];
				$expiryDate =$expiryMonth.$expiryYear; 
				if(empty($expiryDate)) {
					$expiryDate = "1249";
				}
	        	$holderName = $HTTP_POST_VARS['holderName'];
				$cardType = "CREDI";
				$cardCVV = $HTTP_POST_VARS['cvv_code'];
				if(empty($cardCVV)){
					$cardCVV = "";
				}
				$toEncrypt = $ccNumber . ";" . $expiryDate . ";". $cardCVV . ";" . $holderName.";" . $cardIssuer. ";" . $cardType. ";";
				$encryptedtext = text_crypt($toEncrypt);
					
				//delete the cookie
				setMyntraCookie("myntra_secure", "", time()-3600,'/',$cookiedomain);
				//set the cookie
				$time = 0; //If set to 0, or omitted, the cookie will expire at the end of the session (when the browser closes)
				setMyntraCookie('myntra_secure', $encryptedtext,$time,'/',$cookiedomain);
				$purchaseAmount = $Amount * 100;
				$displayAmount = "INR".$Amount;
				$accept_hdr = $_SERVER["HTTP_ACCEPT"];
				$user_agent = $_SERVER["HTTP_USER_AGENT"];
        	}
        	
			if($configMode=="local"||$configMode=="test"||$configMode=="release") {
				if($ccNumber=="4012888888881881"||$ccNumber=="4123450131001381") {
					$bankName = "ICICI TEST CARDS";
				}
			}
        } else if($payment_method == 'cashcard') {
            $gateway = "ccavenue";
        } else if($payment_method=='cod') {
        	$gateway = null;
        } else if($payment_method=='ivr') {
        	$gateway = "atom";
        }

        $toLog = array();
		$toLog['orderid'] = $Order_Id;
		$toLog['login'] = $login;
		$toLog['amount'] = $Amount;
		$toLog['user_agent'] = mysql_real_escape_string($_SERVER['HTTP_USER_AGENT']);
		if(!empty($gateway)){
			$toLog['payment_gateway_name'] = $gateway;
		}	

		if(!empty($payment_method)){
			$toLog['payment_option'] = $payment_method;
		}	
		
		if(($payment_method =='netbanking')){
			$toLog['payment_issuer'] = $bankName;			
		} elseif(!empty($cardIssuer)){
			$toLog['payment_issuer'] = $cardIssuer;
		} elseif(($payment_method =='otherpayments')&&(!empty($_POST['otherpaymentstype']))){
			$toLog['payment_issuer'] = $_POST['otherpaymentstype'];
		}	
		
		if($gateway=='icici'||$gateway=='axis'){
			$toLog['is_inline'] = 1;
		} else {
			$toLog['is_inline'] = 0;
		}
		
		if(!empty($bankName)) {
			$toLog['card_bank_name'] = $bankName;
		}
		
		if(!empty($bin)) {
			$toLog['bin_number'] = $bin;
		}
		
		$toLog['ip_address'] = mysql_escape_string($CLIENT_IP); // client ip set in prepare.php by looking at various things
		
		$toLog['time_insert'] = time();

		func_array2insert('mk_payments_log', $toLog);
		
		if($configMode=="local"||$configMode=="test"||$configMode=="release") {
			if($configMode=="release"){
				$Order_Id = "RELE_".$Order_Id;
				$iciciMerchantID= "M00000000002585";
			} else {
				$Order_Id = "ALPHA".$Order_Id;
				$iciciMerchantID= "M00000000003433";
			}
			
		} else {
			$iciciMerchantID= "M00000000005357";
		}
		
		$fraudMatch = func_query_first("select * from (
				select o.orderid,o.total,sum(((od.actual_product_price- od.cart_discount_split_on_ratio)* od.amount) - od.discount - od.coupon_discount_product - od.cash_redeemed - od.pg_discount) total_oi
				from xcart_order_details od,xcart_orders o 
				where o.orderid=od.orderid and  od.item_status!='IC' and od.orderid='$orderId') t
				where abs(abs(t.total)-abs(t.total_oi))>1");
		if(!empty($fraudMatch)){
			global $fraudOrderLog;
			$fraudOrderLog->debug("MKORDERINSERT : ORDER:$orderId Detected as fraud ");
			if($myCart!==null){
				$fraudOrderLog->debug("MKORDERINSERT : ORDER:$orderId CARTDUMP{".serialize($myCart)."}");
			}else{
				$fraudOrderLog->debug("MKORDERINSERT : ORDER:$orderid CARTDUMP{EMPTY}");
			}
			$fraudOrderLog->debug("MKORDERINSERT : ORDER:$orderId SESSIONDUMP{".serialize($XCART_SESSION_VARS)."} ");
		}
		?>
<body onload="javascript: submitForm()">
<?php
$netbanking_value='';
if(($payment_method =='netbanking')){
$netbanking_value=$gateway_code;
if ($Amount > 0 ) $formAction="https://www.ccavenue.com/servlet/new_txn.PaymentIntegration";
}
?>
<input type='hidden' id="gateway_value" name='gateway_value' value="<?=$gateway?>" />

<?php if($gateway=='ebs' && $Amount > 0){?>
<form name="paymentformforebs" action="https://secure.ebs.in/pg/ma/sale/pay/" method="POST">
<center><img src="<?php echo $secure_cdn_base; ?>/images/image_loading.gif"></center>
<br>
<center><b>Redirecting to EBS</b></center>
<input type='hidden' name="account_id" value="<?php echo $ebs_account_id; ?>">
<!--<input type='hidden' name="account_id" value='5880'>-->
<input type='hidden' name="return_url"  size="60" value="<?php echo $Redirect_Url.'&DR={DR}'; ?>" />
<input type='hidden' name="mode" value="<?php echo $ebs_mode; ?>">
<input name="reference_no" value="<?php echo $Order_Id; ?>" type='hidden'/>
<input name="amount" type="hidden" value="<?php echo $Amount; ?>"/>
<input type="hidden" name="secure_hash" value="<?php echo $ebs_secure_hash; ?>" />
<?php if(isset($gateway_code)){ ?>
<input type="hidden" name="payment_option" value="<?php echo $gateway_code; ?>" />
<?php } ?>
<input name="description" type="hidden" value="detail of the sale"/>
<!--billing address-->
<input name="name" type="hidden" maxlength="255" value="<?php echo $billing_cust_name; ?>" />
<input name="address" type="hidden" value="<?php echo $billing_cust_address; ?>" />
<input name="city" type="hidden" value="<?php echo $billing_cust_city; ?>"/>
<input name="state" type="hidden" value="<?php echo $billing_cust_state; ?>"/>
<input name="postal_code" type="hidden" value="<?php echo $billing_zip_code; ?>"/>
<input name="country" type='hidden' value="<?php echo $billing_cust_country_code; ?>">
<input name="email" type="hidden" value="<?php echo $billing_cust_email; ?>"/>
<input name="phone" type="hidden" maxlength="20" value="<?php echo $billing_cust_tel; ?>"/>

<!--Shipping Address-->
<input name="ship_name" type="hidden" value="<?php echo $delivery_cust_name; ?>"/>
<input name="ship_address" type="hidden" value="<?php echo $delivery_cust_address; ?>"/> 
<input name="ship_city" type="hidden" value="<?php echo $delivery_cust_city; ?>"/>
<input name="ship_state" type="hidden" value="<?php echo $delivery_cust_state; ?>"/>
<input name="ship_postal_code" type="hidden" value="<?php echo $delivery_zip_code; ?>"/> 
<input name="ship_country" type='hidden' value="<?php echo $delivery_cust_country; ?>">
<input name="ship_phone" type="hidden" value="<?php echo $delivery_cust_tel; ?>"/>
<input type="hidden" name="<?php echo($XCART_SESSION_NAME); ?>" value="<?php echo($XCARTSESSID); ?>">
</form>
<?php } else if($gateway=='icici' && $Amount > 0){?>
<form name="paymentformforicici" method="post" action="https://3dsecure.payseal.com/MultiMPI/from_icici_merchant.jsp" >	
	<center><img src="<?php echo $secure_cdn_base; ?>/images/image_loading.gif"></center>
	<br>
	<center><b>Redirecting to Verify by Visa / Master Card Secure</b></center>
	<input type="hidden"  name="pan" value="<?php echo $ccNumber; ?>"/>
	<input type="hidden" name="expirydate" value="<?php echo $expiryDate; ?>">
	<input type="hidden" name="purchaseAmount"  value="<?php echo $purchaseAmount; ?>">
	<input type="hidden" name="displayAmount"  value="<?php echo $displayAmount; ?>">
	<input type="hidden" name="shoppingContext"  value="<?php echo $Order_Id; ?>">	
	<input type="hidden" name="merchantID"  value="<?php echo $iciciMerchantID?>">
	<input type="hidden" name="currencyVal"  value="356">
	<input type="hidden" name="exponent"  value="2">
	<input type="hidden" name="orderdesc"  value="Myntra Purchase">
	<input type="hidden" name="deviceCategory" value="0">
	<input type="hidden" name="acceptHdr" value="<?php echo $accept_hdr; ?>">
	<input type="hidden" name="agentHdr" value="<?php echo $user_agent; ?>">
</form>
<?php } else if($gateway=='axis' && $Amount > 0){?>
<form name="paymentformforaxis" method="post" action="<?php echo $vpcURL;?>" >
<center><img src="<?php echo $secure_cdn_base; ?>/images/image_loading.gif"></center>
<br>
<center><b>Redirecting to Verify by Visa / Master Card Secure</b></center>
<?php
	foreach($axisBankParameter as $key=>$value) {
?>
		<input type="hidden" name="<?php echo $key; ?>" value="<?php echo $value; ?>" />
<?php	 
	}
?>
</form>
<?php } else if($gateway=='tekprocess' && $Amount > 0){?>
<center><img src="<?php echo $secure_cdn_base; ?>/images/image_loading.gif"></center>
<br>
<center><b>Redirecting to Bank Site for Payment Processing</b></center>
<?php echo $tekprocessFromToSubmit;?>
<?php }else{ ?>

<form name="paymentform" action="<?php echo $formAction; ?>" method="POST">
<center><img src="<?php echo $secure_cdn_base; ?>/images/image_loading.gif"></center>
<br>
<center><b><?php echo $redirectmessage; ?></b></center>
<!--	<form name="paymentform" action="<?php echo $Redirect_Url; ?>" method="POST">-->
	<input type="hidden" name="Order_Id" value="<?php echo $Order_Id; ?>"> 
	<input type="hidden" name="Amount" value="<?php echo $Amount; ?>"> 
	<input type="hidden" name="Merchant_Id" value="M_xmukesh_5506"> 
	<input type="hidden" name="Redirect_Url" value="<?php echo $Redirect_Url; ?>">
	<input type="hidden" name="Checksum" value="<?php echo $Checksum; ?>"> 
	<!--?php if(($payment_method =='netbanking')&&(!empty($_POST['netBankingCards']))){ ?-->
	<input type="hidden" name="billing_cust_name" value="<?php echo $billing_cust_name; ?>"> 
	<input type="hidden" name="billing_cust_address" value="<?php echo $billing_cust_address; ?>"> 
	<input type="hidden" name="billing_cust_country" value="<?php echo $billing_cust_country; ?>"> 
	<input type="hidden" name="billing_cust_city" value="<?php echo $billing_cust_city; ?>"> 
	<input type="hidden" name="billing_cust_tel" value="<?php echo $billing_cust_tel; ?>"> 
	<input type="hidden" name="billing_cust_email" value="<?php echo $billing_cust_email; ?>"> 
	<input type="hidden" name="billing_cust_state" value="<?php echo $billing_cust_state; ?>"> 
	<input type="hidden" name="billing_zip_code" value="<?php echo $billing_zip_code; ?>">
	<!-- ?php } ?-->
	<input	type="hidden" name="delivery_cust_name"	value="<?php echo $delivery_cust_name; ?>">
	<input type="hidden" name="delivery_cust_address" value="<?php echo $delivery_cust_address; ?>"> 
	<input type="hidden" name="delivery_cust_state" value="<?php echo $delivery_cust_state; ?>">
	<input type="hidden" name="delivery_cust_city" value="<?php echo $delivery_cust_city; ?>"> 
	<input type="hidden" name="delivery_cust_tel" value="<?php echo $delivery_cust_tel; ?>"> 
	<input type="hidden" name="delivery_cust_country" value="<?php echo $shipping_cust_country; ?>"> 
	<input type="hidden" name="delivery_zip_code" value="<?php echo $delivery_zip_code; ?>"> 
	<?php 
		if(!empty($netbanking_value)){
	?>
	<input type="hidden" name="cardOption" value="netBanking">
	<input type="hidden" name="netBankingCards" value="<?php echo $netbanking_value ;?>">
	<?php } ?>
	<input	type="hidden" name="<?php echo($XCART_SESSION_NAME); ?>" value="<?php echo($XCARTSESSID); ?>">
</form>

<?php }?>

<script type="text/javascript">
	 function submitForm(){
     var amount = '<?= $Amount?>'; 
	 if(amount > 0 && document.getElementById("gateway_value").value=='ebs')
		document.paymentformforebs.submit();
	 else if(amount > 0 && document.getElementById("gateway_value").value=='icici')
		 document.paymentformforicici.submit();
	 else if(amount > 0 && document.getElementById("gateway_value").value=='axis')
		 document.paymentformforaxis.submit();
	 else if(amount > 0 && document.getElementById("gateway_value").value=='tekprocess');
	 else document.paymentform.submit();
	 }
	 </script>

		<?php
}//End varify checksum*/

?>
</body>
