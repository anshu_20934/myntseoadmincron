<?php
require_once "./auth.php";
include_once($xcart_dir."/include/func/func.mkcore.php");
include_once($xcart_dir."/include/func/func.core.php");
include_once($xcart_dir."/include/func/func.mkmisc.php");
include_once($xcart_dir."/include/func/func.mkspecialoffer.php");
include_once("./include/class/mcart/class.MCart.php");

use mcart\exception\ProductAlreadyInCartException;

$chosenProductConfiguration = $XCART_SESSION_VARS['chosenProductConfiguration'];
$defaultCustId = $chosenProductConfiguration["defaultCustomizationId"];
$defaultCustomizationAreaImageDetail = func_load_product_customized_area_rel_details($chosenProductConfiguration['productId'], $defaultCustId);
$productStyleId =$chosenProductConfiguration['productStyleId'];
$quantity =$chosenProductConfiguration['quantity'];
$size_qty_array = $chosenProductConfiguration['size_qty_array'];
$productId=$chosenProductConfiguration['productId'];
if(is_array($size_qty_array)){
	$quantity = array_sum($size_qty_array);
	$sizenamesArray = array_keys($size_qty_array);
	$sizequantityArray = array_values($size_qty_array);
}elseif (is_array($HTTP_POST_VARS['productqty'])){
	$quantity = array_sum($HTTP_POST_VARS['productqty']);
	$sizenamesArray = array_keys($HTTP_POST_VARS['productqty']);
	$sizequantityArray = array_values($HTTP_POST_VARS['productqty']);
}else{
	$quantity = ($HTTP_POST_VARS['productqty'])?$HTTP_POST_VARS['productqty']:$HTTP_GET_VARS['productqty'];
}
$sk_selectedSize;
$sk_selectedQuantity;
foreach($size_qty_array as $sk_size=>$sk_quantity){
	if(!empty($sk_quantity)){
		$sk_selectedSize=$sk_size;
		$sk_selectedQuantity=$sk_quantity;
	}
}

$skuid = filter_input(INPUT_POST,"productSKUID", FILTER_SANITIZE_NUMBER_INT);


$mcartFactory= new MCartFactory();
$mcart= $mcartFactory->getCurrentUserCart(true);
$mcartItem= new MCartPItem($chosenProductConfiguration['productId'],$skuid,$sk_selectedQuantity);

try {
	$mcart->addProduct($mcartItem);
	MCartUtils::persistCart($mcart);
} catch (ProductAlreadyInCartException $ex) {
	
}


if(!x_session_is_registered('grandTotal')){
    x_session_register('grandTotal');
}  

header("location:".$http_location."/mkmycartmultiple.php?pagetype=productdetail");
?>