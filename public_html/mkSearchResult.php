<?php
define("NUMBER_VARS", "posted_data[price_min],posted_data[price_max],posted_data[avail_min],posted_data[avail_max],posted_data[weight_min],posted_data[weight_max],price_min,price_max,avail_min,avail_max,weight_min,weight_max");
require "./auth.php";
require_once ("include/func/func.search.php");
require_once ("include/func/func.core.php");
// empty unneccessary session variables

if (x_session_is_registered("chosenProductConfiguration"))
x_session_unregister("chosenProductConfiguration");

if (x_session_is_registered("masterCustomizationArray"))
x_session_unregister("masterCustomizationArray");

define("GET_ALL_CATEGORIES", true);
require $xcart_dir."/include/categories.php";
## Commented not in use ###########
###if($active_modules["Manufacturers"])
###include $xcart_dir."/modules/Manufacturers/customer_manufacturers.php";

$tmp=strstr($QUERY_STRING, "$XCART_SESSION_NAME=");
if (!empty($tmp))
$QUERY_STRING=ereg_replace("$XCART_SESSION_NAME=([0-9a-zA-Z]*)", "", $QUERY_STRING);


########### added for break URL rewriting#############
$searchPageURL = $_SERVER['REQUEST_URI'];
$_element = explode("/",$searchPageURL);
//echo "<pre>";
//print_r($_element);
$scindex = array_search("search",$_element);
if($scindex !== false){
	$keyid =	$scindex+1;
	$mode =  $_element[$scindex];
	$HTTP_GET_VARS['searchkey'] = urldecode($_element[--$scindex]);

	$HTTP_GET_VARS['key_id'] =  $_element[$keyid];

}

## Commented not in use ###########
##if (!empty($active_modules['SnS_connector']) && $REQUEST_METHOD == 'POST') {
##if ($simple_search == 'Y') {
###func_generate_sns_action("SiteSearch");
##}
##else {
###func_generate_sns_action("AdvancedSearch");
###}
###}

x_session_register("search_data");

$sortparam = $sortparam = strip_tags($HTTP_GET_VARS['sortby']);
if($sortparam  == "USERRATING")
{
	$displayHeading = "top rated by users";
}
elseif($sortparam  == "BESTSELLING")
{
	$displayHeading = "best selling products";
}
elseif($sortparam  == "MOSTRECENT")
{
	$displayHeading = "most recent products";
}
elseif($sortparam  == "EXPERTRATING")
{
	$displayHeading = "top rated by experts";
}
else
{
	$displayHeading = "top rated by experts";
	$sortparam = "EXPERTRATING";
}
$smarty->assign("sortMethod",$sortparam);



# The list of the fields allowed for searching
$allowable_search_fields = array (
"substring",
"by_title",
"by_shortdescr",
"by_fulldescr",
"extra_fields",
"by_keywords",
"categoryid",
"category_main",
"category_extra",
"search_in_subcategories",
"price_max",
"price_min",
"price_max",
"avail_min",
"avail_max",
"weight_min",
"weight_max",
"manufacturers");

$_old_search_data = "";

if ($REQUEST_METHOD == 'GET' && $mode == "search") {
	# Check the variables passed from GET-request
	$get_vars = array();
	foreach ($HTTP_GET_VARS as $k=>$v) {
		if (in_array($k, $allowable_search_fields))
		$get_vars[$k] = $v;
	}

	# Prepare the search data
	if (!empty($get_vars)) {
		$_old_search_data = $search_data["products"];
		$search_data["products"] = $get_vars;
	}
}
$search_data["products"]["forsale"] = "Y";
include $xcart_dir."/include/search.php";
if (!empty($search_data["products"]) && !empty($products)) {

	#### Not in use ###########
	####if (!empty($active_modules["Subscriptions"])) {
	# Get the subscription plans
	####include $xcart_dir."/modules/Subscriptions/subscription.php";

	#####}

	# Generate the URL of the search result page for accesing it via GET-request
	$search_url_args = array();
	foreach ($search_data["products"] as $k=>$v) {
		if (in_array($k, $allowable_search_fields) && !empty($v)) {
			if (is_array($v)) {
				foreach ($v as $k1=>$v1)
				$search_url_args[] = $k."[".$k1."]=".urlencode($v1);
			}
			else {
				$search_url_args[] = "$k=".urlencode($v);
			}
		}
	}

	if ($search_url_args && $page > 1)
	$search_url_args[] = "page=$page";

	$search_url = "search.php?mode=search".(!empty($search_url_args) ? "&".implode("&", $search_url_args) : "");
	$smarty->assign("search_url", $search_url);
}

/*set product types */
$smarty->assign("selectedproductypid", $search_data["products"]["by_type"]);
$productType = strtolower($search_data["products"]["by_TypeName"]);
$smarty->assign("productType", $productType);

/* set category */
$smarty->assign("catid", $search_data["products"]["by_category"]);
$categoryName = strtolower($search_data["products"]["by_categoryName"]);
$smarty->assign("categoryName", $categoryName);

unset($search_data["products"]["forsale"]);

if ($_old_search_data)
$search_data["products"] = $_old_search_data;

if(empty($search_data["products"]))
$search_data["products"] = '';
$smarty->assign("search_prefilled", $search_data["products"]);
$search_data["products"]["forsale"] = "Y";

if(empty($products))
define("GET_ALL_CATEGORIES", 1);

if (!empty($QUERY_STRING)) {
	$location[] = array(func_get_langvar_by_name("lbl_search_results"), "");
	$smarty->assign("main","search");
}
else {
	$location[] = array(func_get_langvar_by_name("lbl_advanced_search"), "");
	$smarty->assign("main","advanced_search");
}
# Assign the current location line
$smarty->assign("location", $location);
include_once("./include/func/func.mkcore.php");
#### Not in use ###
##$themes = func_load_subcategoriesByName("Themes");
###$smarty->assign("themescount", count($themes));


###$smarty->assign("themes", $themes);

//load  main categories
$rootCategories=func_load_root_categories();
$smarty->assign("rootcategories", $rootCategories);
//load product types
$productTypes =func_load_product_type();
$smarty->assign("producttypes", $productTypes);
$advance =$_GET["advance"];
if($advance=="true")
{
	/*$breadCrumb=array(
	array('reference'=>'#','title'=>'Advance Search')
	);*/

	$smarty->assign("breadCrumb",$breadCrumb);
}
else
{
	if(!empty($productType))
	{
		$breadcrumblink =  "for $productType";
	}
	else if(!empty($categoryName))
	{
		$breadcrumblink =  "for $categoryName";
	}
	else
	{
		//$breadcrumblink =  "search results";
	}
	$smarty->assign("breadCrumb",$breadcrumblink);

}

$captions = array();
if(isset($HTTP_GET_VARS['advance']))
{
	$designers = func_query("SELECT d.id , c.firstname  as f, c.lastname as l FROM  $sql_tbl[mk_designer]
					as d INNER JOIN $sql_tbl[customers] as c ON c.login = d.customerid  ORDER BY  c.firstname");
	$smarty->assign("designers", $designers);
}
$smarty->assign("thickbox","<link rel=\"stylesheet\" href=\"$xcart_web_dir/skin1/css/thickbox.css\" type=\"text/css\" media=\"screen\" />");
$captions[0] =  "Search Results";
##$smarty->assign("captions", $captions);
$smarty->assign("advance", $advance);

########## Load the search types ##########
$prdStr = '';
foreach($productTypes as $_k=>$val){
	$prdStr .= $val[1].',';
}

$sql ="select keyword,orignal_keyword from mk_stats_search where key_id='".$keywordid."'";
$result = db_query($sql);
while($row = db_fetch_array($result)){
	$meta_keyword = $row['keyword'];
	$meta_orignal_keyword = $row['orignal_keyword'];
}

$uniq_keyword = str_replace($meta_keyword ,"",	$meta_orignal_keyword);

if(!empty($uniq_keyword)){
$metaKeywords = $meta_orignal_keyword.",".$prdStr;
$smarty->assign("metaDescription","Buy ".$meta_orignal_keyword." at Myntra. Variety of product Options - T-Shirt, Mug, Poster, Keychain, Calendar,  Watch, Pendant, Sweatshirt, and many more. Design your own Merchandize." );
$smarty->assign("pageTitle","Buy ".$meta_orignal_keyword." online in India" );

}else{
$metaKeywords = $meta_keyword.",".$prdStr;
$smarty->assign("metaDescription","Buy ".$meta_keyword." Merchandize at Myntra. Variety of product Options - T-Shirt, Mug, Poster, Keychain, Calendar,  Watch, Pendant, Sweatshirt, and many more. Design your own Merchandize." );
$smarty->assign("pageTitle","Buy ".$meta_keyword." Merchandize online in India" );
}

$smarty->assign ("metaKeywords",$metaKeywords);
$recently_added = get_recently_added_search($keywordid);
$popular_search= get_most_popular_search();

$product_filter_results = get_product_filter_results($keywordid);
$pageheading = " Filter results by product type";
$smarty->assign("pageheading",$pageheading);
$smarty->assign("keywordid",$keywordid);
$smarty->assign("product_filter_results",$product_filter_results);

$recently_searched = get_recently_searched($keywordid);
$smarty->assign("recently_added", $recently_added );
$smarty->assign("popular_search", $popular_search);
$smarty->assign("recently_searched",$recently_searched);
func_display("customer/mkSearchResult.tpl",$smarty);




?>