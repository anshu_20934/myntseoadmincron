<?php
/**
 * Script para la generaci�n de CAPTCHAS
 *
 * @author  Jose Rodriguez <jose.rodriguez@exec.cl>
 * @license GPLv3
 * @link    http://code.google.com/p/cool-php-captcha
 * @package captcha
 * @version 0.3
 *
 */
$id = $_GET['id'];
if(empty($id)) {
	// This is a forced condition on myntra, to call captcha with an id parameter.
	// If this parameter is not set, then we do not generate any captcha at all !!!
	return;
}
 
chdir(dirname(__FILE__));
define('NO-TOP-NAV',true);
include_once("../auth.php");
include_once(HostConfig::$documentRoot."/captcha/SimpleCaptcha.php");
ob_clean();

header("Cache-Control: private, no-store, no-cache, proxy-revalidate");
header("Pragma: no-cache");
header("Expires: Wed, 19 Apr 2000 11:43:00 GMT");

$captcha = new SimpleCaptcha();
// Image generation
$captcha->CreateImage($_GET['id']);



?>
