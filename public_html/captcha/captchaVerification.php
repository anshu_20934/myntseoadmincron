<?php
include_once("../auth.php");

$uniqueId = $_POST['uniqueId'];
$userInputCaptcha = $_POST['userInputCaptcha'];

if(empty($userInputCaptcha) || empty($uniqueId)) {
    echo 'false';
    exit;
}

// get session captcha to compare with user captcha
$sessionCaptcha=null;
if(!empty($XCART_SESSION_VARS['captchasAtMyntra'][$uniqueId])) {
    // Note: Including auth.php removes $_SESSION set user session variables. We store those in POST as auth.php does not unset POST data
    $sessionCaptcha  = $XCART_SESSION_VARS['captchasAtMyntra'][$uniqueId];
}

if (empty($sessionCaptcha) || ($userInputCaptcha != $sessionCaptcha)) {
    $captchaVerified = false;
    
} else {
    unset($XCART_SESSION_VARS['captchasAtMyntra'][$uniqueId]);
    $captchaVerified = true;
    if($uniqueId == 'contactUsPage'){
        $XCART_SESSION_VARS['captchaValid'] = 1;
    }
}


if($captchaVerified)	{
    echo 'true';
    exit;

}else{
    echo 'false';
    exit;
}
?>