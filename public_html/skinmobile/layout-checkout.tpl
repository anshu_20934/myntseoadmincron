{include file="inc/doctype.tpl"}
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# {$facebook_app_name}: http://ogp.me/ns/fb/{$facebook_app_name}#">
    {include file="inc/head-meta.tpl"}
    {include file="inc/head-css.tpl"}
    {include file="inc/head-script.tpl"}
    {block name=pagecss}{/block}
</head>
<body class="{$_MAB_css_classes} {if $smarty.server.HTTPS}secure{/if}">
    {include file="inc/gtm.tpl"}
    <div class="mk-body">
    <div class="mk-wrapper">
        {include file="inc/header-checkout.tpl"}
        {block name=body}{/block}
        {include file="inc/footer.tpl"}
    </div>
    </div>
    {block name=lightboxes}{/block}
    {include file="inc/body-css.tpl"}
    {include file="inc/body-script.tpl"}
    {block name=pagejs}{/block}
    {block name=macros}{/block}
</body>
</html>
