{$rs="<span class=rupees>Rs. </span>"}
<div class="order-summary">
    <h2>Order Summary</h2>
    {*if $address}
    <div class="shipping-address">
        <span class="lbl">Shipping Address</span>
        <a class="blue-text" href="{$https_location}/checkout-address.php?change=1" onClick="_gaq.push(['_trackEvent', 'address', 'edit',window.location.toString()]);return true;">Change</a>
        <address>
        	<span class="addressID hide">{$addressID}</span>
            <strong>{$address.name}</strong><br>
            {$address.address|nl2br}</br>
            {if $address.locality}{$address.locality}<br>{/if}
            {$address.city} - {$address.pincode}, {$address.statename}<br>
            <span class="lbl">Mobile:</span> {$address.mobile}
        </address>
    </div>
    {/if*}

    <div class="total-items">
        <span class="lbl">{$totalItems} Item(s)</span>
        <a  class="blue-text" href="{$http_location}/mkmycart.php" onClick="_gaq.push(['_trackEvent', 'order_summary', 'edit',window.location.toString()]);return true;">/ GO TO BAG</a>
    </div>
    <div class="list">
        {* BEGIN: list of items *}
        {* cart_view should be complete|summary. base on this the tpl will render the markup *}
        {$cart_view="summary"}
        {include file="checkout/secure-order-summary.tpl"}
        {* END: list of items *}
    </div>
</div>

