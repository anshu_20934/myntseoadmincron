{assign var=numrows value=0}
{foreach name=combo item=productsSet key=discountId
from=$productsInCart}
<div class="prod-set">
	{foreach name=outer
        item=product key=itemid from=$productsSet.productSet} {assign
        var=numrows value=$numrows+1}
        <div class="row prod-item" id="product_view_{$itemid}">
            {if $cart_view eq "summary"}
            <div class="col1">
  				{assign var="couponApplicable" value="COUPON APPLICABLE"}
		       	{if $styleExclusionFG eq 1 && !$product.couponApplicable}
					{assign var="couponApplicable" value="COUPON NOT APPLICABLE"}
	            {elseif $condNoDiscountCouponFG eq 1 && $product.isDiscounted}
					{assign var="couponApplicable" value="COUPON NOT APPLICABLE"}
            	{/if}
            
            	<div class="prod-name"> 
                    {if !$product.freeItem && $product.landingpageurl|trim} <a
                        href="{$http_location}/{$product.landingpageurl}"
                        onclick="_gaq.push(['_trackEvent','pdp_click', Myntra.Data.pageName, 'cart_title']);"
                        target="_blank">{$product.productStyleName|truncate:80}</a> {else}
                   		<span class="free-prod-name">{$product.productStyleName|truncate:80}</span>
                    {/if} 
                </div>
                    <div class="size-qty-wrap">
                        {if $cart_view eq "summary"} 
                        <span
                            class="mk-custom-drop-down size"> 
                            <label>Size:</label> 
                            {if $product.sizenames && $product.sizequantities} 
                                {if $product.sizenames|@count gt 0} {if $product.flattenSizeOptions}
                                    {assign var=selectedSize value=$product.sizenameunified} 
                                   {else}
                                    {assign var=selectedSize value=$product.sizename} {/if} <!-- We only show the selected size in the combo initially. Other sizes are loaded by ajax -->
                                {if $product.freeItem} 
                                    <span class="mk-freesize black">{$selectedSize}</span>
                                {else}<span class="prod-selected-size black">{$selectedSize}</span> 
                                {/if} 
                            {/if} 
                            {else} 
                            <span class="black">NA</span>
                            {/if} 
                         </span> 
                         <span class="mk-custom-drop-down qty"> 
                             {assign var=sku value=$product.productStyleId|cat:'-'|cat:$product.sizename}
                            {assign var=cssqty value=""} 
                             {if ($checkInStock) and ($product_availability[$itemid] lt $product.quantity or
                            $product.quantity eq 0 or $total_consolidated_sale[$sku].stockout
                            eq true)} 
                                {assign var=cssqty value="qty-err"} 
                            {/if}
                            {$maxQty=min($cartItemMaxQty,$sku_info[$itemid].availableItems)}
                            <span class="slash">/</span>
                            <input type="hidden" name="maxqty" class="max-available-qty" value="{$maxQty}" />
                            <label class="{if $cssqty eq 'qty-err'} grey strike {else} grey {/if}">Qty:</label> 
                            {if $maxQty gt 0 && $product.freeItem neq 1}
                                <span class="prod-qty {if $cssqty eq 'qty-err'} grey strike {else} black {/if}">{$product.quantity}</span> 
                            {else} <span class="mk-freesize ps-qty-wrapper {if $cssqty eq 'qty-err'} grey strike {else} black {/if}">{$product.quantity}</span>
                            <!-- Kundan: on changing item size, it was not reflecting in case of Out of stock items: maxQty will be equal to 0 -->
                            <input type="hidden" name="{$itemid}qty"
                            value="{$product.quantity}" /> {/if} </span> {else} <span
                            class="lbl">Size:</span> 
                            {if $product.flattenSizeOptions}
                                {$product.sizenameunified} {else} <span class="black">{$product.sizename}</span> {/if} <span
                            class="sep">/</span> <span class="lbl {if $cssqty eq 'qty-err'} grey strike {else} black {/if}">Qty:</span>
                        <span class="{if $cssqty eq 'qty-err'} grey strike {else} black {/if}">{$product.sizequantity}</span> {/if}
                </div>
                {if $styleExclusionFG eq 1}
	               	<div class="couponApplicability">
    	  	        	<span>{$couponApplicable}</span>
        	    	</div>	             
            	{/if}
            </div>
            {/if}

            <div class="col2">
            
                {if $product.productPrice eq 0} 
                <div class="amount red">0</div>
				{else}

               {if (isset($product.pricePerItemAfterDiscount) && $product.pricePerItemAfterDiscount gte 0) || $product.freeItem eq 1}
                    {if $cart_view neq "complete"}
                        <div class="amount red">
                            <span class="red">{$rs}</span>
                            <span class="strike rupees gray">
                                {math equation="a*b" a=$product.productPrice b=$product.quantity assign="netPrice"}
                                {$netPrice|round|number_format:0:".":","}
                            </span>
                            {if $product.pricePerItemAfterDiscount eq 0}0{else} 
                                {math equation="a*b" a=$product.pricePerItemAfterDiscount b=$product.quantity assign="priceAfterDiscount"}
                                {$priceAfterDiscount|round|number_format:0:".":","}
                            {/if}
                        </div>
                        <div class="discount red">
                            <span> {include file="string:{$product.discountDisplayText}"}</span>
                        </div>
                    {else}
                        <div class="discount">
                            {if $product.discountQuantity gt 0} 
                                <span class="red">{$rs}</span>
                                <span class="strike rupees gray"> 
                                    {math equation="a*b" a=$product.productPrice b=$product.quantity assign="netPrice"}{$netPrice|round|number_format:0:".":","}
                                </span>
                                <span class="amount red">
                                    {if $product.quantity neq $product.discountQuantity} 
                                        {$product.discountQuantity} 0
                                    {/if}
                                </span>
                            {else} 
                                <span class="strike rupees gray">
                                    {math equation="a*b" a=$product.productPrice b=$product.quantity assign="netPrice"}
                                    {$netPrice|round|number_format:0:".":","}
                                </span>
                            {/if} 
                            {if $product.pricePerItemAfterDiscount eq 0}0{else} 
                                {math equation="a*b" a=$product.pricePerItemAfterDiscount b=$product.quantity assign="priceAfterDiscount"}
                                <span>{$priceAfterDiscount|round|number_format:0:".":","}</span>
                            {/if}
                            <span> 
                        </div>
                        <div class="amount red">
                            {include file="string:{$product.discountDisplayText}"}</span>
                        </div>
                {/if} 
                {else}
                    <div class="amount red">
                    {$rs}
                        {(($product.productPrice)*($product.quantity-$product.discountAmount))|round|number_format:0:".":","}
                    </div>
                        <div class="discount">
                            {if $styleExclusionFG eq 0 && $condNoDiscountCouponFG eq 1 && !($product.isDiscounted) }
                                <span class="gray">{$product.isDiscounted} COUPON APPLICABLE</span>
                            {/if}
                        </div>
                {/if} 
                {/if}
            </div>
        </div>
        {/foreach}
</div>
{/foreach}
        <div class="total">
            <p class="total-item-discount">ITEM TOTAL {$rs} {if isset($eossSavings) && $eossSavings gt 0}<span class="grey strike">{$amount|round|number_format:0:".":","}</span>{/if}<span class="red"> {math equation="a-b" a=$amount b=$eossSavings assign=subTotal}{$subTotal|round|number_format:0:".":","}</span></p>
            <p class="total-shipping-fee">SHIPPING {if $shippingCharge > 0}FEE (+){$rs} <span class="red"> {$shippingCharge|round|number_format:0:".":","}</span>{else}<span class="green">FREE</span>{/if}</p>
            <p class="total-cart-discount">{if isset($cartLevelDiscount) && $cartLevelDiscount gt 0 &&
        $cartLevelDiscount}CART DISCOUNT (-) {$rs}<span class="green">{$cartLevelDiscount|round|number_format:0:".":","}</span>{/if}</p>
            <p class="total-coupon-discount">{if isset($couponDiscount) && $couponDiscount gt 0 &&
        $couponDiscount}COUPON DISCOUNT (-) {$rs}<span class="green"><span class="coupon-value">{$couponDiscount|round|number_format:0:".":","}</span></span>{/if}</p>
            {if isset($cashdiscount) && $cashdiscount gt 0}
                <p class="total-cash-back">CASHBACK (-) {$rs}<span class="green"> {$cashdiscount|round|number_format:0:".":","}</span></p>
            {/if}
            {if $loyaltyPointsUsedRupees && $loyaltyPointsUsedRupees gt 0}
                <p class="total-cash-back">PRIVILEGE POINTS (-) {$rs}<span class="green"> {$loyaltyPointsUsedRupees|round|number_format:0:".":","}</span></p>
            {/if}
            <p class="total-giftwrap-charge">{if $giftWrapEnabled && $isGiftOrder}GIFT WRAP CHARGE {$rs}<span class="red">{$giftingCharges}</span>{/if}</p>
            <p class="emi-charges hide">EMI PROCESSING FEE: <span class="value rupees red">{if $emi_charge > 0}{$emi_charge|round|number_format:0:".":","}{else}
                FREE{/if}</span></p>
            <p class="cod-charges hide">COD CHARGES: {$rs}<span class="value rupees red">0.00</span></p>
            <p class="additional-discount hide">ADDITIONAL DISCOUNTS: (-) {$rs}<span class="value rupees green">0.00</span></p>
            {if $vatCharge > 0}<p class="total-vatcharge-fee">VAT Collected (+) {$rs}<span class="red">{$vatCharge|round|number_format:0:".":","}</span></p>{/if}
            <p class="final-calculated-value">TOTAL
             <span class="total-value you-pay value red" data-original-value="{$rs} {$totalAmount|round|number_format:0:'.':','}"> {$rs} {$totalAmount|round|number_format:0:".":","}</span></p>
        </div>
        
