<div class="mod edit-bag-popup">
    <div class="hd"><div class="back"><span class="back-img"></span></div><div class="header">MODIFY ITEM</div><div class="close-wrapper"><span class="btn-close"></span></div></div>
    <div class="bd mk-cf">
        <div class="main-content slide-main-content mk-cf">
                <div class="prod-name-wrapper list-header"></div>
                <ul class="single-step-list">
                    <li class="edit-sizeqty-link" data-content-id="lb-edit-size"><span class="anim-link prompt edit-size-span">EDIT SIZE / QTY</span><span class="mk-level1-arrownav"></span>
                        	<div class="lb-edit-size link-details" style="display:none;">
                        	<div class="prod-name-wrapper"></div>
                            <div class="size-wrapper"></div>
                            <div class="qty-label-wrapper">Qty</div>
                            <div class="qty-wrapper"></div>
                            <div class="btn btn-orange-noplay lb-save-changes">SAVE CHANGES</div>
                            <div class="lb-editbag-error-msg lb-error-msg"></div>
                            <form method="post" action="modifycart.php" class="savecartchanges">
                                <input type="hidden" name="_token" value="{$USER_TOKEN}"> 
                                <input type="hidden" name="styleid" value="">
                                <input type="hidden" class="lb-selected-size" name="" value=""> 
                                <input type="hidden" class="lb-selected-qty" name="" value=""> 
                                <input type="hidden" name="cartitem" value=""> 
                                <input type="hidden" name="itemId" value=""> 
                                <input type="hidden" name="actionType" value="">
                                <input type="hidden" name="update" value="size">
                                <input type="hidden" name="redirectTo" value="cart">
                                <input type="hidden" name="itemType" value="">
                            </form>
                        </div>
                    </li>
                    <li class="move-to-wishlistbag-link">
                        <div class="prompt"></div>
                    </li>
                    <li class="remove-from-wishlistbag-link">
                        <div class="prompt"></div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <input type="hidden" class="editStyleId" value="">
    <input type="hidden" class="lb-prod-view-id" value="">
<!-- Login background coming from PHP now -->
<input type="hidden" name="loginbg" value="{$loginbg}" class="mk-login-bg-path" />
