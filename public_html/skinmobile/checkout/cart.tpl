{extends file="layout.tpl"}  {block name=body}
<script>
Myntra.Data.pageName="cart";
Myntra.Data.socialAction='cart-wishlist';
var socialPost = {$socialPost};
var socialPostArray={$socialArray};
var loc = window.location.href;
var patt=/wishlist/g;
var isWishlistRequest = patt.test(loc);
if(isWishlistRequest) {
	{assign var=isMoveToWishlistRequest value=true} 
}
else {
	{assign var=isMoveToWishlistRequest value=false}
}
Myntra.Cart = Myntra.Cart || {};
            Myntra.Cart.Data = {ldelim}
                isGiftOrder : {$isGiftOrder},
                items : '{$_cart}', 
                amount : {$amount},
                totalAmount : {$totalAmount},
                totalQuantity : {$totalQuantity},
                mrp : {$mrp},
                couponDiscount : {$couponDiscount},
                cashDiscount : {$cashdiscount},
                totalCashBackAmount : {$totalCashBackAmount},
                cashBackAmountDisplayOnCart : {$cashBackAmountDisplayOnCart},
                shippingCharge : {$shippingCharge},
                giftCharge : {$giftCharge},
                savings : {$savings},
                cartLevelDiscount : {$cartLevelDiscount},
                productAdded : "{$productAdded}",
            {rdelim};
</script>
{$rs="
<span class=rupees>Rs. </span>
"}
{assign var=isStockError value=false}
<div class="checkout cart-page">
    <div class="tabbed-header">
    <div class="header-tab-wrapper">
        <h2 class="shoppping-bag-header">
            Bag {if $totalItems gt 0} <span
                class="gray">({$totalItems})</span>
            {/if}
        </h2>
        <h2 id="wishlist" class="wishlist-header">
            Wishlist {if $totalSavedItemCount gt 0} <span
                class="gray">({$totalSavedItemCount}) </span> {/if}
        </h2>
       </div>
    </div>
    <div class="bag-slider-wrapper {if $isMoveToWishlistRequest || $smarty.get.wlreq}wishlist-active{/if}">
    <div class="bag-wrapper">
    <div class="cart-list">
        {if $totalItems gt 0} {if $cartDiscountMessage }
        <div class="cart-discount-msg">
            {include file="string:{$cartDiscountMessage}"} {if
            $cartDiscountExclusionTT} &nbsp; &nbsp; <span
                id="cart-discount-exclusion-tt"
                style="color: #808080; text-decoration: underline;"
                title='{include file="string:{$cartDiscountExclusionTT}"}'>
                <br/>Conditions Apply</span> {/if}
        </div>
        {/if} {$has_error = ($checkInStock and ($items_out_of_stock or
        $items_zero_quantity or $product_intotal_out_of_stock))} 
        {capture name=cart_total}
        <div class="total">
            {if $shippingCharge > 0}
                <div class="shipping-item">
                    <div class="shipping-advice-msg">
                        Buy for <span class="rupees">{$rs}</span> {$free_shipping_amount} or
                        more and make your shipping <span class="green">FREE !</span> 
                    </div>
                </div>           
            {/if}
            <p class="total-item-discount">ITEM TOTAL {$rs} {if isset($eossSavings) && $eossSavings gt 0}<span class="grey strike">{$amount|round|number_format:0:".":","}</span>{/if}<span class="red"> {($amount-$eossSavings)|round|number_format:0:".":","}</span></p>
            <p class="total-cart-discount">{if isset($cartLevelDiscount) && $cartLevelDiscount gt 0 &&
        $cartLevelDiscount}BAG DISCOUNT (-) {$rs}<span class="green">{$cartLevelDiscount|round|number_format:0:".":","}</span>{/if}</p>
            <p class="total-coupon-discount">{if isset($couponDiscount) && $couponDiscount gt 0 &&
        $couponDiscount}COUPON DISCOUNT (-) {$rs}<span class="green"><span class="coupon-value">{$couponDiscount|round|number_format:0:".":","}</span></span>{/if}</p>
            {if isset($cashdiscount) && $cashdiscount gt 0}
                <p class="total-cash-back">CASHBACK (-) {$rs}<span class="green"> {$cashdiscount|round|number_format:0:".":","}</span></p>
            {/if}
            <p class="total-giftwrap-charge">{if $giftWrapEnabled && $isGiftOrder}GIFT WRAP CHARGE: {$rs}<span class="rupees red">{$giftingCharges}</span>{/if}</p>
            <div id="vat-spot" class="{if $vatCharge <= 0} hide {/if}"><p class="total-vatcharge-fee">VAT Collected (+) {$rs}<span class="red"> {$vatCharge|round|number_format:0:".":","} </span></p></div>
			<p class="total-shipping-fee">SHIPPING {if $shippingCharge > 0}FEE (+){$rs} <span class="red"> {$shippingCharge|round|number_format:0:".":","}</span>{else}<span class="green">FREE</span>{/if}</p>
            <p class="final-calculated-value">TOTAL <span class="rupees red">{$rs}</span>
             <span class="total-value red"> {$totalAmount|round|number_format:0:".":","}</span></p>
        </div>
        
        {/capture} 
        {* BEGIN: success messages *} {if $pname}
        <div class="success page-success">
            <span class="success-icon"></span> Added to bag: <strong>{$pname}</strong>
        </div>
        {/if} {* END: success messages *} {* BEGIN: error messages *} 
        {if $redirectSource eq 'mi'}
            <div class="err page-error">
                It seems your cart details have been modified during the checkout
                process.<br> Please confirm the details below and proceed.
            </div>
        {/if} 
        {if ($checkInStock) and ($items_out_of_stock or $items_zero_quantity or $product_intotal_out_of_stock)} 
            {if $items_zero_quantity}
                <div class="err page-error">Zero units have been ordered for some
                    items. Please enter a larger number or click 'Remove'.</div>
                    {assign var=isStockError value=true}
            {elseif $product_intotal_out_of_stock}
                <div class="err page-error">We're sorry, we don't have enough stock to
                    process your entire order - Please review the items.</div>
                    {assign var=isStockError value=true}
            {else}
                <div class="err page-error">
                    <span class="error-icon"></span>Some items in your bag are out of
                    stock - Please review them to place your order.</div>
                    {assign var=isStockError value=true}
            {/if} 
        {/if} 
        {* END: error messages *} 
        {* BEGIN: list of items *} {*
        cart_view should be complete|summary. base on this the tpl will render
        the markup *} {$cart_view="complete"} {include
        file="checkout/cart-details.tpl"} {* END: list of items *}

{if $totalItems gt 0}
    <div class="cart-context-msg apply-coupons {if isset($couponDiscount) && $couponDiscount gt 0 &&
        $couponDiscount} hide {else} show {/if}">
        <h2 class="apply-coupons-link" id="apply-coupons-link" >
            <span>APPLY COUPONS</span>
        </h2>
        {if $showVerifyMobileMsg && $mobile_status neq 'V'}
            {assign var=mobileverificationrequired value=1}
                <div id="discount_coupon_message" class="message vmessage mt10" >
                <a href="javascript:void(0)" id="showSMSVerification" onclick="_gaq.push(['_trackEvent', 'coupon_widget', 'verify_now', 'click']);">Verify your mobile to use your coupons</a>
            </div>
        {/if}
    </div>
    <div class="cart-context-msg remove-applied-coupons {if isset($couponDiscount) && $couponDiscount gt 0 &&
        $couponDiscount} show {else} hide {/if}">
        <div class="cart-context-sub-msg">
            <p class="coupon-msg">
            <span class="ajax-success"></span><span class="black">You have successfully saved <span class="red">Rs. {$couponDiscount}</span> by applying coupon {$couponCode} on {$couponAppliedItemsCount} {if {$couponAppliedItemsCount} gt 1}items{else}item{/if} in your bag. </span>
            </p>
            <span class="remove-coupons-link">REMOVE COUPON</span>
        </div>
    </div>
    {/if} 
        {if ($giftWrapEnabled || $isGiftOrder) && $totalItems gt 0}
            <div class="gift-wrapper">
            <div class="inline" style="margin-left:-7px;">
            <input type='checkbox' style="display:none;" name="gift-wrap-select" id="giftcheckbox" class="gift-wrap-cart gift-wrap-select" {if $isGiftOrder}checked="true" {/if}/><label for="giftcheckbox" class="custom-checkbox {if $isGiftOrder}checked {/if}"></label>
            </div>
            <div class="inline">
                <div class="gift-holder">    
                    <div class="gift-chkbox-div"> 
                        <div class="gift-line-wrapper"> Gift wrap this order for <em>Rs.</em> {$giftingCharges}
                        </div>    
                        <div {if $isGiftOrder}data-gift-to="{$giftTo}" data-gift-from="{$giftFrom}" data-gift-msg="{$giftMessage|escape}" class="gift-data gift-msg-edit" {else}class="mk-hide gift-msg-edit"{/if}> EDIT&nbsp;MESSAGE</div>                                          
                    </div>
                    <p class="gift-wrap-info">Cash on Delivery not available for gift orders</p>        
               </div>  
               </div>  
           </div>            
        {/if}
        <div class="total-row mk-cf">
            <div class="col3">{$smarty.capture.cart_total}</div>
        </div>
        {else}
        <div class="empty-cart">
            <div class="err">Your Shopping Bag is empty</div>
            <div class="action">
                <a href="/" class="btn normal-btn btn-grey btn-continue-shopping">Continue
                    Shopping</a>
            </div>
        </div>
        {/if}
    </div>
{if $totalItems gt 0}
    <div class="mk-clear"></div>
    <button type="button" class="btn btn-place-order {if $isStockError eq false}btn-orange active{else}btn-grey inactive{/if}">
        PLACE ORDER{if $isStockError eq false}<span class="ico-login-small"></span>{/if}
    </button>
{/if}
</div>
    <div class="save-list cart-sign-in-sub-msg wishlist-wrapper {if $isMoveToWishlistRequest || $smarty.get.wlreq}allReadyWishList{/if}">
        {if $totalSavedItemCount gt 0} 
            {include file="checkout/saved-details.tpl"}         
        {else}
        <div class="empty-saved">
            <p>Your Personal List of Items that you would like to Buy from Myntra</p>
            <div class="err">Your wishlist is empty</div>
            {if $totalItems gt 0 && !$login}
            <div class="cart-sign-in-sub-msg">
                Please <span class="btn-login">Login</span> to view your Wishlist
                Items that you may have added earlier.
            </div>
            {/if}
        </div>
        {/if}
    </div>
    </div>
    <div style="clear:both;"></div>
    {include file="checkout/modal-sms-verification.tpl"}  
</div>
{/block} 
{block name=pagejs}
{/block}
{block name=cartpageRightmenu}
    <div id="lb-coupons" class="content-slide-wrapper coupons-wrapper content-inactive">
         {include file="checkout/modal-coupons.tpl"}
    </div>
    <div id="lb-editbag" class="content-slide-wrapper editbag-wrapper content-inactive">
         {include file="checkout/modal-editbag.tpl"}
    </div>
    <div class="content-slide-wrapper giftwrap-wrapper content-inactive">
    </div>
{/block}
{block name=macros}
    {include file="macros/cart-macros.tpl"}
{/block}

