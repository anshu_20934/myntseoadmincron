                <div id="smsVerification" class="lightbox verifybox">
                        <div class="mod">
                                <div class="hd"><h2 class="title">Mobile Verification</h2></div>
                                <div class="bd">
                                        <form action="mkpaymentoptions.php{if $addressID&&$addressID>0 }?address={$addressID}{/if}" method="post" id="change-profile" name="change-profile">
                                    <input type="hidden" name="mode" value="update-profile-data" />
                                    <input type="hidden" name="login" id="login" value="{$userProfileData.login}" />
                                                <input type="hidden" id="_token" name="_token" value="{$USER_TOKEN}" />
                                    <div class="v-code-note">
                                        <div id="v-code-note-message" {if $verified_for_another eq 1 || $mobile_status eq 'E'} class="error" {/if} >
                                                {if $verified_for_another eq 1}
                                                        Your mobile number is unverified because it has been registered against another user. Please enter a valid mobile number to use your coupons.
                                                            {elseif $userProfileData.mobile eq ''}
                                                    Please provide a contact
                                                    number on which we can reach you for any issues related to your
                                                    transactions. This number is also required to be verified for
                                                    usage of any use-specific coupons that you may have been issued
                                                    through Myntra.
                                            {elseif $userProfileData.mobile neq "" && $mobile_status neq "V" && $mobile_status neq "E" && $verified_for_another eq 0}
                                                    Please verify your
                                                    specified contact number to be able to use any user-specific
                                                    coupons that you may have been issued through Myntra. We may use
                                                    this number to contact you for issues related to any of your
                                                    transactions on Myntra.
                                                {elseif $mobile_status eq 'E'}
                                                You have exceeded the maximum no. of attempts to verify this mobile number. If you have still not received the verification code or are facing any other difficulties, please provide an alternate number and retry. Or, try again after 6 hours.
                                            {/if}
                                                                {if $mrp_skipMobileVerification neq 1 && $mobile_status neq 'V'}
                                            <p>
                                                In case you do not receive your verification code from Myntra, please call us at our Customer Support number at <strong>080-43541999</strong> anytime and get your number verified instantly.
                                            </p>
                                            {/if}
                                        </div>
                                    </div>
                                    <div class="v-code">
                                        <div class="row"><label>Name :</label> <span>{$userProfileData.firstname} {$userProfileData.lastname}</span></div>
                                        <div class="row"><label>Email :</label> <span class="email">{$userProfileData.login}</span></div>
                                        <div class="row"><label>Mobile  :</label> <span><input type="text" id="mobile" name="mobile" value="{$userProfileData.mobile}" class="profile-mobile" maxlength="10" > <div class="verify-loading mk-hide" id="mobile-verify-loading" style="*position:relative;*top:-25px"></div></span></div>
                                        {if $mrp_skipMobileVerification neq 1}<button id="verify-mobile" class="verify-mobile-cart-page btn small-btn primary-btn btn-grey">Verify Mobile Now</button>
                                        {elseif $showVerifyMobileMsg && $mobile_status neq 'V'}<button id="verify-mobile" class="verify-mobile-cart-page btn small-btn primary-btn btn-grey">Verify Mobile Now</button>
                                        {else}{if $verified_for_another eq 1}<button id="verify-mobile" class="verify-mobile-cart-page btn small-btn normal-btn">Edit Mobile</button>{/if}{/if}
                                        <div id="mobile-error" class="captcha-message error hide"></div>
                                    </div>
                                        </form>
                                        <div class="captcha-block hide" id="mobile-captcha-block">
                                                <fieldset>
                                                    <legend>Type the characters you see in the picture</legend>
                                            <form id="mobile-captchaform" method="post" action="">
                                                <div class="mk-cf captcha-details">
                                                    <div class="p5 corners black-bg4 span-7">
                                                        <img src="{$cdn_base}/skin1/images/spacer.gif" id="mobile-captcha" />
                                                    </div>
                                                    <a href="javascript:void(0)" onclick="document.getElementById('mobile-captcha').src='{$http_location}/captcha/captcha.php?id=paymentpage&rand='+Math.random();document.getElementById('mobile-captcha-form').focus();" id="change-image" class="no-decoration-link">Change text</a>
                                                </div>
                                
                                                <div class="mk-cf mt10 captcha-entry">
                                                    <input type="text" name="mobile-userinputcaptcha" id="mobile-captcha-form" />
                                                    <input type="hidden" name="mobile-login" class="btn-captcha" id="mobile-login" value="{$userProfileData.login}" >
                                                    <a href="javascript:void(0)" class="next-button btn primary-btn small-btn verify-captcha" id="mobile-verify-captcha" style="*position:relative;*top:-15px" onClick="verifyMobileCaptcha()">Confirm</a>
                                                    <div id="mobile-captcha-loading" class="captcha-loading mk-hide"></div>
                                                </div>
                                                <div class="captcha-message hide" id="mobile-captcha-message"></div>
                                            </form>
                                        </fieldset>
                                </div>
                                    <form action="mkpaymentoptions.php{if $addressID&&$addressID>0 }?address={$addressID}{/if}" method="post" id="verify-mobile-code" name="verify-mobile-code" class="mt20 mb10 hide">
                                        <input type="hidden" name="mode" value="verify-mobile-code">
                                        <div class="four-dig-code">
                                        <p id="mobile-message"></p>
                                            <div class="row">
                                                <label style="*position:relative;*top:-4px">Verification Code : </label>
                                                <span style="*display:inline-block"><input id="mobile-code" name="mobile-code" size="4" style="*position:relative;*top:4px"></span>
                                        </div>
                                            <div class="verify-loading" id="code-verify-loading" style="display:none; margin-right: 100px;"></div>
                                            <input type="button" value="Submit" id="submit-code" class="submit-code btn primary-btn small-btn">
                                        </div>
                                        <div class="four-dig-code-note">
                                           <p id="mobile-verification-error">If you have not received your verification code, please wait for atleast 2 minutes before pressing submit button</p>
                                        </div>
                                    </form>
                                </div>
                        </div>
                {if $divid eq 'success'}
                        <a onclick="javascript:removeCoupon();" class="cashback-btn btn normal-btn small-btn">Remove</a>
                {/if}                   
        </div>


