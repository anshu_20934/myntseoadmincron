<div class="mod coupons-popup">
    <div class="hd">
<div class="header">APPLY COUPONS</div><div class="close-wrapper"><span class="btn-close"></span></div></div>
    <div class="bd mk-cf">
        <div class="main-content slide-main-content mk-cf">
                <ul class="single-step-list">
                    <li class="choose-coupon-link {if $eligible_coupons_count lte 0 and $ineligible_coupons_count lte 0}hide{/if}" data-content-id="choose-coupon"><span class="anim-link prompt choose-coupons-span">CHOOSE FROM YOUR COUPONS</span><span class="mk-level1-arrownav"></span>
                        {*<div class="choose-coupon link-details" style="display:none;">
                            <div class="choose-coupons-body">    
                                {if $eligible_coupons_count gt 0}
                                    <div class="applicable-coupons title">APPLICABLE COUPONS <span class="gray">[{$eligible_coupons|count}]</span></div>
                                    {section name=prod_num loop=$eligible_coupons}
                                        {if $eligible_coupons[prod_num].coupon neq "" and prod_num<2}
                                            <div class="eligible-coupon-item">
                                                <input type="radio" id="{$eligible_coupons[prod_num].coupon}" class="applicable-coupon-radio" name="applicable-coupon-radio"value="{$eligible_coupons[prod_num].coupon|upper}"> 
                                                {assign var=desclength value = strlen($eligible_coupons[prod_num].couponTypeDescription)}
                                                <label for="{$eligible_coupons[prod_num].coupon}" class="coupon-off"><span class="rupees {if !$eligible_coupons[prod_num].disabled}red{/if} {if $desclength>15}condense-line{/if}">{$eligible_coupons[prod_num].couponTypeDescription}</span></label>
                                                <div class="coupon-off-amount gray {if !$eligible_coupons[prod_num].disabled}red{/if}">Save Rs {$eligible_coupons[prod_num].MRPAmount}</div>
                                                <div class="coupon-desc">
                                                    <p>Expiry:  {$eligible_coupons[prod_num].expire|date_format:"%d %b, %Y"} ({math equation= "ceil(x/86400) - ceil(y/86400) + 1" x=$eligible_coupons[prod_num].expire y=$today assign="valid_days"}{$valid_days}    {if $valid_days eq 1}day{else}days{/if})</p>
                                                    <p>Minimum Purchase: <span class="rupees">{$eligible_coupons[prod_num].minimum}</span></p>
                                                    <p class="{if $eligible_coupons[prod_num].disabled}gray{/if}">Code: {$eligible_coupons[prod_num].coupon|upper}</p>    
                                                </div>
                                            </div>
                                        {/if}
                                    {/section}    
                                    <button type="button" class="btn btn-apply-user-coupon btn-orange">APPLY COUPON<span class="ico-login-small"></span></button>
                                    <div class="lb-coupon-error-msg lb-error-msg"></div>
                                {else}
                                    <div class="no-eligible-coupon-wrapper">
                                        <p class="no-eligible-coupon-text-line1">YOUR COUPONS ARE NOT APPLICABLE</p>
                                        <p class="no-eligible-coupon-text-line2">Some coupons can be applied only on {if $condNoDiscountCouponFG eq 1}NON-DISCOUNTED ITEMS of{/if} certain brands, or if the items in your bag are above a minimum value.</p>
                                    </div>
                                {/if}
                                {if $ineligible_coupons_count gt 0}
                                    <div class="ineligible-coupons title">INELIGIBLE COUPONS<span class="gray">[{$ineligible_coupons|count}]</span></div>
                                        {section name=prod_num loop=$ineligible_coupons}
                                        {if $ineligible_coupons[prod_num].coupon neq ""}
                                            <div class="ineligible-coupon-item {if ($eligible_coupons_count eq 0) and ($smarty.section.prod_num.index eq 0)}selected{/if}">
                                            {assign var=desclength value = strlen($ineligible_coupons[prod_num].couponTypeDescription)}
                                                <label class="coupon-off"><span class="rupees {if !$rest_coupons[prod_num].disabled}red{/if} {if $desclength>15}condense-line{/if}">{$ineligible_coupons[prod_num].couponTypeDescription}</span></label>
                                                    {if $condNoDiscountCouponFG eq 1}                                                     
                                             				<div class="coupon-off-amount gray {if !$rest_coupons[prod_num].disabled}red{/if}"><span class="expand-ineligible-coupons {if ($eligible_coupons_count eq 0) and ($smarty.section.prod_num.index eq 0)}expanded{/if}"></span><p class="ineligible-couponoff-text">Min Purchase Rs. {$ineligible_coupons[prod_num].minimum}</p></div>                                                     
                                         			{else}
                                              			{math equation="x-y" x=$ineligible_coupons[prod_num].minimum y=$totalItemsAmount assign="couponRestAmount"}
                                         				{if $couponRestAmount > 0}
                                             				<div class="coupon-off-amount gray {if !$rest_coupons[prod_num].disabled}red{/if}">BUY for Rs {$couponRestAmount} MORE <span class="expand-ineligible-coupons {if ($eligible_coupons_count eq 0) and ($smarty.section.prod_num.index eq 0)}expanded{/if}"></span></div>
                                         				{/if}    
                                         			{/if} 
                                                <div class="coupon-desc" {if ($eligible_coupons_count eq 0) and ($smarty.section.prod_num.index eq 0)}style="display: block;"{/if}>
                                                    <p>Expiry: {$ineligible_coupons[prod_num].expire|date_format:"%d %b, %Y"}</p>
                                                    <p>Minimum Purchase: <span class="rupees">{$ineligible_coupons[prod_num].minimum}</span></p>
                                                    <p class="{if $ineligible_coupons[prod_num].disabled}gray{/if}">Code: {$ineligible_coupons[prod_num].coupon|upper}</p>    
                                                </div>
                                            </div>
                                            {/if}
                                        {/section}    
                                    {/if}
                        </div>
</div>*}
                    </li>
                    <li class="enter-coupon-code-link" data-content-id="enter-coupon-code"><span class="anim-link prompt enter-coupon-span">ENTER A COUPON CODE</span><span class="mk-level1-arrownav"></span>
                        <div class="enter-coupon-code link-details" style="display:none;" >
                        <input type="text" name="othercouponcode" id="othercouponcode" autocomplete="off" class="coupon-code user-coupon-code" placeholder="Enter Coupon Code">
                        <div class="promo-coupons choose-coupon ">
                   
                        </div>
                            <form action="mkpaymentoptions.php{if $addressID&&$addressID>0 }?address={$addressID}{/if}"
                                method="post" id="cashcouponform" name="cashcouponform"
                                class="{if $cashCouponMessage && $cashdivid eq 'success' } hide {/if}"
                                onsubmit="return redeemCashCoupon();">
                                <input type="hidden" value="{$appliedCouponCode}" id="lb-selected-coupon-code" class="cb-input" name="c-code">
                            </form>
                        <button type="button" class="btn btn-apply-user-coupon btn-orange">
                            APPLY COUPON<span class="ico-login-small"></span>
                        </button>
                        {math equation="x+y" x=$eligible_coupons_count y=$ineligible_coupons_count assign="totalApplicableCoupon"}
                        <input type="hidden" name="totalApplicableCoupon" value="{$totalApplicableCoupon}" class="totalApplicableCoupon" />
                        <div class="lb-coupon-error-msg lb-error-msg"></div>
                        </div>
                    </li>
                </ul>


            </div>
        </div>
    </div>
<!-- Login background coming from PHP now -->
<input type="hidden" name="loginbg" value="{$loginbg}" class="mk-login-bg-path" />
