{assign var=numrows value=0}
{foreach name=combo item=productsSet key=discountId
from=$productsInCart}
<div class="prod-set">
    {assign var=isCombo value=false} 
    {if $discountId gt 0 } 
        {assign var=isCombo value=true} 
    <div class="combo">
        {if $cart_view eq "complete"}
        <div class="combo-header-main row header">
            <div class="col2">
                <div class="para">This item is eligible for a combo offer </div>
                <div><span class=red>{include file="string:{$productsSet.label_text}"}</span></div>
                <div class="para"> To avail this offer, please complete this purchase from your computer.</div>
            </div>
        </div>
        {/if} {/if} {*Combo Loop starts here*} {foreach name=outer
        item=product key=itemid from=$productsSet.productSet} {assign
        var=numrows value=$numrows+1}
        <div class="row prod-item" id="product_view_{$itemid}">
            {if $cart_view eq "complete"}
            <div class="col1">
                {if !$product.freeItem && $product.landingpageurl|trim} 
                <a href="{$http_location}/{$product.landingpageurl}" target="_blank"
                    onclick="_gaq.push(['_trackEvent','pdp_click', Myntra.Data.pageName, 'cart_image']);">
                {/if} 
                <img src="{myntraimage key="style_96_128" src=$product.cartImagePath}" alt="image of {$product.productStyleName}"> 
                {if !$product.freeItem && $product.landingpageurl|trim} </a> {/if}
                    <div class="size-qty-wrap">
                        {if $cart_view eq "complete"} 
                        
                        <span
                            class="mk-custom-drop-down size"> 
                            <label>Size:</label> 
                            {if $product.sizenames && $product.sizequantities} 
                                {if $product.sizenames|@count gt 0} {if $product.flattenSizeOptions}
                                    {assign var=selectedSize value=$product.sizenameunified} 
                                   {else}
                                    {assign var=selectedSize value=$product.sizename} {/if} <!-- We only show the selected size in the combo initially. Other sizes are loaded by ajax -->
                                {if $product.freeItem} 
                                    <span class="mk-freesize black">{$selectedSize}</span>
                                {else}<span class="prod-selected-size black">{$selectedSize}</span> 
                                        <select class="sel-size" name="{$itemid}size">
                                            <option value="{$product.skuid}" selected>{$selectedSize}</option>
                                        </select> 
                                {/if} 
                            {/if} 
                            {else} 
                            <span class="black">NA</span>
                            {/if} 
                         </span> 
                         <span class="mk-custom-drop-down qty"> 
                             {assign var=sku value=$product.productStyleId|cat:'-'|cat:$product.sizename}
                            {assign var=cssqty value=""} 
                             {if ($checkInStock) and ($product_availability[$itemid] lt $product.quantity or
                            $product.quantity eq 0 or $total_consolidated_sale[$sku].stockout
                            eq true)} 
                                {assign var=cssqty value="qty-err"} 
                            {/if}
                            {$maxQty=min($cartItemMaxQty,$sku_info[$itemid].availableItems)}
                            <span class="slash">/</span>
                            <input type="hidden" name="maxqty" class="max-available-qty" value="{$maxQty}" />
                            <label class="{if $cssqty eq 'qty-err'} grey strike {else} grey {/if}">Qty:</label> 
                            {if $maxQty gt 0 && $product.freeItem neq 1}
                                <span class="prod-qty {if $cssqty eq 'qty-err'} grey strike {else} black {/if}">{$product.quantity}</span>
                                <select class="sel-qty ps-qty-wrapper" name="{$itemid}qty"> 
                                {for $i=1 to $maxQty}
                                    <option value="{$i}"{if $i eq $product.quantity} selected{/if}>{$i}</option>
                                {/for} 
                                {for $i=1 to $maxQty}
                                    {if $i eq $product.quantity} <span>{$i}</span>{/if}
                                    {if $product.quantity} eq 0}<span>0</span>{/if}
                                    {if $product.quantity} eq null}<span>0</span>{/if}
                                {/for}
                                {if $product.quantity > $maxQty }
                                    <option disabled selected>{$product.quantity}</option> 
                                    <span class="grey strike">{$product.quantity}</span>
                                {/if}
                            </select> 
                            {else} <span class="mk-freesize ps-qty-wrapper {if $cssqty eq 'qty-err'} grey strike {else} black {/if}">{$product.quantity}</span>
                            <!-- Kundan: on changing item size, it was not reflecting in case of Out of stock items: maxQty will be equal to 0 -->
                            <input type="hidden" name="{$itemid}qty"
                            value="{$product.quantity}" /> {/if} </span> {else} <span
                            class="lbl">Size:</span> 
                            {if $product.flattenSizeOptions}
                                {$product.sizenameunified} {else} <span class="black">{$product.sizename}</span> {/if} <span
                            class="sep">/</span> <span class="lbl {if $cssqty eq 'qty-err'} grey strike {else} black {/if}">Qty:</span>
                        <span class="{if $cssqty eq 'qty-err'} grey strike {else} black {/if}">{$product.sizequantity}</span> {/if}
             {if $cart_view eq "complete"}
                <div class="err item-error stock-err">
                    {if ($checkInStock) and $product.quantity eq 0 } 
                        Zero unit(s) ordered 
                    {elseif ($checkInStock) and ($product_availability[$itemid] lte 0)} 
                        THIS ITEM IS SOLD OUT
                    {elseif ($checkInStock) and $product_availability[$itemid] lt $product.quantity} 
                        ONLY {$product_availability[$itemid]} UNIT(s) AVAILABLE 
                    {/if}
                </div>
                {/if}
                    <div class="ps-size-wrapper"></div>
                </div>
            </div>
            {/if}

            <div class="col2">

                {if $cart_view eq "complete"}
                <form id="cartform_{$itemid}" method="post" action="modifycart.php" class="form-cart-item">
                    <input type="hidden" name="_token" value="{$USER_TOKEN}"> 
                    <input type="hidden" name="actionType" value="">
                    <input type="hidden" name="cartitem" value="{$itemid}"> 
                    <input type="hidden" name="styleid" value="{$product.productStyleId}"> 
                    <input type="hidden" name="update" value="1"> 
                </form>
                {/if} 
            </div>

            <div class="col3">
				{assign var="couponApplicable" value="COUPON APPLICABLE"}
		       	{if $styleExclusionFG eq 1 && !$product.couponApplicable}
					{assign var="couponApplicable" value="COUPON NOT APPLICABLE"}
	            {elseif $condNoDiscountCouponFG eq 1 && $product.isDiscounted}
					{assign var="couponApplicable" value="COUPON NOT APPLICABLE"}
            	{/if}
            
                {if $product.productPrice eq 0} 
                    {if $cart_view eq "complete"}
                        <div class="discount"></div>
                    {/if}
                <div class="amount red">0</div>
                {if $cart_view neq "complete"}
                <div class="discount"></div>
                {/if} {else}
                <div class="prod-name">
                    {if $cart_view eq "complete"} {if !$product.freeItem &&
                    $product.landingpageurl|trim} <a
                        href="{$http_location}/{$product.landingpageurl}"
                        onclick="_gaq.push(['_trackEvent','pdp_click', Myntra.Data.pageName, 'cart_title']);"
                        target="_blank">{$product.productStyleName}</a> {else} <span
                        class="free-prod-name">{$product.productStyleName}</span> {/if}
                    {else} {if !$product.freeItem && $product.landingpageurl|trim} <a
                        href="{$http_location}/{$product.landingpageurl}"
                        onclick="_gaq.push(['_trackEvent','pdp_click', Myntra.Data.pageName, 'cart_title']);"
                        target="_blank">{$product.productStyleName|truncate:80}</a> {else}
                    <span class="free-prod-name">{$product.productStyleName|truncate:80}</span>
                    {/if} {/if}
                    {if $product.supplyType eq "JUST_IN_TIME"}
                    <div class="supplier-details">Supplied by partner</div>
                    {/if}
                </div>
               {if (isset($product.pricePerItemAfterDiscount) && $product.pricePerItemAfterDiscount gte 0) || $product.freeItem eq 1}
                    {if $cart_view neq "complete"}
                        <div class="amount red">
                            <span class="red">{$rs}</span>
                            <span class="strike rupees gray">
                                {math equation="a*b" a=$product.productPrice b=$product.quantity assign="netPrice"}
                                {$netPrice|round|number_format:0:".":","}
                            </span>
                            {if $product.pricePerItemAfterDiscount eq 0}0{else} 
                                {math equation="a*b" a=$product.pricePerItemAfterDiscount b=$product.quantity assign="priceAfterDiscount"}
                                {$priceAfterDiscount|round|number_format:0:".":","}
                            {/if}
                        </div>
                        <div class="discount red">
                            <span> {include file="string:{$product.discountDisplayText}"}</span>
                        </div>
                    {else}
                        <div class="discount">
                            {if $product.discountQuantity gt 0} 
                                <span class="red">{$rs}</span>
                                <span class="strike rupees gray"> 
                                    {math equation="a*b" a=$product.productPrice b=$product.quantity assign="netPrice"}{$netPrice|round|number_format:0:".":","}
                                </span>
                                <span class="amount red">
                                    {if $product.quantity neq $product.discountQuantity} 
                                        {$product.discountQuantity} FREE
                                    {/if}
                                </span>
                            {else} 
                                <span class="strike rupees gray">
                                    {math equation="a*b" a=$product.productPrice b=$product.quantity assign="netPrice"}
                                    {$netPrice|round|number_format:0:".":","}
                                </span>
                            {/if} 
                            {if $product.pricePerItemAfterDiscount eq 0}0{else} 
                                {math equation="a*b" a=$product.pricePerItemAfterDiscount b=$product.quantity assign="priceAfterDiscount"}
                                <span>{$priceAfterDiscount|round|number_format:0:".":","}</span>
                            {/if}
                            <span> 
                        </div>
                        <div class="amount red">
                            {include file="string:{$product.discountDisplayText}"}</span>
                        </div>
                        {if $styleExclusionFG eq 1}
                        	<div class="couponApplicability">
                        		<span>{$couponApplicable}</span>
                        	</div>
                        {/if}
                	{/if} 
                {else}
                    <div class="amount red">
                    {$rs}
                        {(($product.productPrice)*($product.quantity-$product.discountAmount))|round|number_format:0:".":","}
                    </div>
                   	{if $cart_view eq "complete"}
                   		{if $styleExclusionFG eq 1}
                        	<div class="couponApplicability">
                        		<span>{$couponApplicable}</span>
                        	</div>
                        {else}
                        	<div class="discount">
                            	{if $condNoDiscountCouponFG eq 1 && !($product.isDiscounted) }
                                	<span class="gray">{$product.isDiscounted} COUPON APPLICABLE</span>
                            	{/if}
                        	</div>
                        {/if}                                           	
                   	{/if}
                {/if} {*{if $cart_view eq "complete"} {if
                $product.discountDisplayText neq ''}
                <div class="tool-text">{include
                    file="string:{$product.discountDisplayText}"}</div>
                {elseif $cashback_gateway_status eq 'on' &&
                $enableCBOndiscountedProducts eq 'true'}
                <div
                    class="cashback-info {if $cashback_displayOnCartPage neq '1'}hide{/if}">
                    Cashback: {$rs}{math equation="(w-x) * z" w=$product.productPrice
                    x=$product.discountAmount y=$product.quantity z=0.10
                    assign="cashback"} {$cashback|round|number_format:0:".":","} per
                    item</div>
                {elseif $cashback_gateway_status eq 'on' &&
                $enableCBOndiscountedProducts eq 'false' && $product.discountAmount
                eq 0}
                <div
                    class="cashback-info {if $cashback_displayOnCartPage neq '1'}hide{/if}">
                    Cashback: {$rs}{math equation="(w-x) * z" w=$product.productPrice
                    x=$product.discountAmount y=$product.quantity z=0.10
                    assign="cashback"} {$cashback|round|number_format:0:".":","} per
                    item</div>
                {/if}*} {if $cart_view eq "complete" && $product.freeItem neq 1}
                <div class="action">
                    <form method="post" action="/mksaveditem.php"
                        class="form-save-item">
                        <input type="hidden" name="actionType" value="addFromCart"> <input
                            type="hidden" name="itemId" value="{$itemid}"> <input
                            type="hidden" name="redirectTo" value="cart"> <input
                            type="hidden" name="_token" value="{$USER_TOKEN}"> <input
                            type="hidden" name="productId" value="{$product.productStyleId}">
                        <button type="submit" style="display:none;" class="mk-move-to-wishlist link-btn" 
                        value="WISHLIST">WISHLIST</button>
                    </form>
                    <form method="get" action="/modifycart.php"
                        class="form-remove-item">
                        <input type="hidden" name="remove" value="true"> <input
                            type="hidden" name="itemid" value="{$itemid}"> <input
                            type="hidden" name="_token" value="{$USER_TOKEN}">
                        <button type="submit" style="display:none;" class="mk-remove link-btn" 
                        value="REMOVE">REMOVE</button>
                    </form>
                <span class="mk-edit"></span>
                </div>
                {/if} {/if}
            </div>
        </div>
        {/foreach} {if $isCombo}
    </div>
    {/if}

</div>
{/foreach}
<script>
    Myntra.Data.pageName='cart';
    Myntra.Cart = Myntra.Cart || {};
    Myntra.Cart.Data = {ldelim}
        isGiftOrder : {$isGiftOrder},
        items : '{$_cartItems}', 
        amount : {$amount},
        totalAmount : {$totalAmount},
        totalQuantity : {$totalQuantity},
        mrp : {$mrp},
        couponDiscount : {$couponDiscount},
        cashDiscount : {$cashdiscount},
        totalCashBackAmount : {$totalCashBackAmount},
        cashBackAmountDisplayOnCart : {$cashBackAmountDisplayOnCart},
        shippingCharge : {$shippingCharge},
        giftCharge : {$giftCharge},
        savings : {$savings},
        cartLevelDiscount : {$cartLevelDiscount},
{if isset($productAdded)}
        productAdded : {$productAdded},
{else}
        productAdded : 0,
{/if}
    {rdelim};
</script>
