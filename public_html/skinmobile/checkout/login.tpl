{extends file="layout-checkout.tpl"}

{block name=body}
<div class="checkout login-page mk-cf">
    <h1>Email Address</h1>

    <div class="main-content">
        <div class="login-header">Your order details will be sent by email</div>
        {if $errMsg}
            <div class="err">{$errMsg}</div>
        {/if}
        <form method="post" class="frm-checkout-login">
            <div class="loading"></div>
            <div class="email">
                <label>Your email address</label>
                <input type="text" name="email" value="{$smarty.post.email}">
            </div>

            <div class="toggle-pass">
                <input type="checkbox" name="existing-user" value="1"{if $showPassInput} checked="true"{/if}>
                I have an account already
            </div>

            <div class="pass{if not $showPassInput} hide{/if}">
                <button type="button" name="fpass" class="link-btn btn-forgot-pass">Forgot password?</button>
                <label>Your password</label>
                <input type="password" name="password" value="">
                <div class="fpass-msg {if not $fpassDone}hide{/if}">
                    <p>An email with a link to reset your password has been sent to :</p>
                    <p><strong>{$fpassEmail}</strong></p>
                </div>
            </div>

            <div class="page-action">
                <button type="submit" name="login" class="btn primary-btn btn-continue">Continue <span class="proceed-icon"></span></button>
                <input type="hidden" name="menu_usertype" value="C">
                <input type="hidden" name="_action" value="">
                <input type="hidden" name="_token" value="{$USER_TOKEN}">
            </div>

            <div class="fb-connect">
                <div>You can also choose to</div>
                <button type="button" class="btn normal-btn btn-fb-connect"><span class="ico-fb-small"></span> Connect with Facebook</button>
            </div>
        </form>
    </div>

    {include file="checkout/summary.tpl"}
</div>
{/block}

