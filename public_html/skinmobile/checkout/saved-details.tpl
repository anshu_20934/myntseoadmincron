{foreach $savedItems as $discountId => $productsSet name=combo}
<div class="prod-set">
    {assign var=isCombo value=false} 
    {if $discountId gt 0 } 
    	{assign var=isCombo value=true} 
    	{if $productsSet.dre_minMore gt 0} 
    		{assign var=isConditionMet value=false} 
    	{else} 
    		{assign var=isConditionMet value=true} 
    	{/if}
    <div class="combo">
        <div class="combo-header-main row header">
            <div class="discount-msg">{include file="string:{$productsSet.label_text}"}</div>
            <div class="discount-amt gray">
                YOU PAY <span class="combo-savings-msg">{$rs}{math equation="a-b"
                    a=$productsSet.comboTotal b=$productsSet.comboSavings
                    assign="comboActualTotal"}{$comboActualTotal|round|number_format:0:".":","}</span>
                / YOU SAVE <span class="combo-savings-msg">{$rs}{$productsSet.comboSavings|round|number_format:0:".":","}</span>
            </div>
            <!--<a class="combo-completion-btn btn normal-btn" isConditionMet="{$isConditionMet}"
                    ctype="{if $productsSet.dre_buyCount>0}c{else}a{/if}"
                    min="{if $productsSet.dre_buyCount>0}{$productsSet.dre_buyCount}{else}{$productsSet.dre_buyAmount}{/if}"                    {if $isConditionMet}
                        Edit Combo!
                    {else}
                        Complete Combo Now!
                    {/if}
                </a>-->
            <div class="combo-completion-msg gray">{*{if $isConditionMet}*} Buy
                before the discount expires&nbsp;{*else} Move to bag to get discount
                {/if*}</div>
        </div>
        {/if} {*Combo Loop starts here*} 
        {foreach $productsSet.productSet as
        $itemid => $product name=item}
        <div class="row prod-item {if $smarty.foreach.item.first}first{/if}" id="product_view_{$itemid}">
            <div class="col1">
                {if !$product.freeItem && $product.landingpageurl|trim} <a
                    href="{$http_location}/{$product.landingpageurl}" target="_blank"
                    onclick="_gaq.push(['_trackEvent','pdp_click', Myntra.Data.pageName, 'saved_image']);">
                    {/if} <img src="{$product.cartImagePath}"
                    alt="image of {$product.productStyleName}"> {if !$product.freeItem
                    && $product.landingpageurl|trim} </a> {/if}
                <div class="size-qty-wrap">
                     {assign var=sku value=$product.productStyleId|cat:'-'|cat:$product.sizename}
                        {assign var=cssqty value=""} 
                        {if ($checkInStock) and
                        ($product_availability[$itemid] lt $product.quantity or
                        $product.quantity eq 0 or $total_consolidated_sale[$sku].stockout
                        eq true)} 
                            {assign var=cssqty value="qty-err"} 
                        {/if} 
                    <span class="mk-custom-drop-down size"> <label>Size:</label> {if
                        $product.sizenames && $product.sizequantities} {if
                        $product.sizenames|@count gt 0} 
                        {if $product.flattenSizeOptions}
                            {assign var=selectedSize value=$product.sizenameunified} 
                        {else}
                            {assign var=selectedSize value=$product.sizename} 
                        {/if}
                        {if $product.flattenSizeOptions}
                            {assign var=productsizenames value=$product.sizenamesunified}
                        {else} 
                            {assign var=productsizenames value=$product.sizenames}
                        {/if} 
                        {if $product.freeItem} 
                            <span class="mk-freesize prod-selected-size black">{$selectedSize}</span>
                        {else} 
                            <span class="prod-selected-size black">{$selectedSize}</span>
                            <span class="slash">/</span>
                            <select class="sel-size" name="{$itemid}size"> 
                            {assign var=i value=0} 
                            {foreach from=$productsizenames item=sizename}
                            <option value="{$product.skuid}"{if $sizename eq $selectedSize} selected{/if}>{$selectedSize}</option>
                            {assign var=i value=$i+1} {/foreach}
                    </select> {/if} {/if} {else} NA {/if} </span> 
                    <span class="mk-custom-drop-down qty"> <label class="{if $cssqty eq 'qty-err'} grey strike {else} black {/if}">Qty:</label> 
                        {if $product.freeItem} 
                        <span class="mk-freesize {if $cssqty eq 'qty-err'} grey strike {else} black {/if}">{$product.quantity}</span>
                        {else} 
                        <span class="prod-qty {if $cssqty eq 'qty-err'} grey strike {else} black {/if}">{$product.quantity}</span>
                        <select class="sel-qty ps-qty-wrapper" name="{$itemid}qty"
                        {if $product.productPrice eq 0}disabled="true"{/if}> {for $i=1 to
                            10}
                            <option value="{$i}"{if $i eq $product.quantity} selected{/if}>{$i}</option>
                            {/for}
                            </select> 
                            {for $i=1 to $maxQty}
                                {if $i eq $product.quantity} <span>{$selectedSize}</span>{/if}
                            {/for}
                    {/if} </span>
                    <div class="err item-error stock-err">
                        {if ($checkInStock) and $product.quantity eq 0 } Zero unit(s) ordered 
                        {elseif ($checkInStock) and $product_availability[$itemid] lte 0} THIS ITEM IS SOLD OUT
                        {elseif ($checkInStock) and $product_availability[$itemid] lt $product.quantity} ONLY {$product_availability[$itemid]} UNIT(s) AVAILABLE 
                        {/if}
                    </div>
                    <div class="ps-size-wrapper"></div>
                </div>
            </div>
            <div class="col2">
                <form id="cartform_{$itemid}" method="post"
                    action="/mksaveditem.php" class="form-saved-item">
                    <input type="hidden" name="_token" value="{$USER_TOKEN}"> <input
                        type="hidden" name="itemId" value="{$itemid}"> <input
                        type="hidden" name="styleid" value="{$product.productStyleId}"> <input
                        type="hidden" name="redirectTo" value="cart"> <input type="hidden"
                        name="actionType" value="1">
                </form>
            </div>
            <div class="col3">
                <div class="prod-name">
                    {if !$product.freeItem && $product.landingpageurl|trim} <a
                        href="{$http_location}/{$product.landingpageurl}"
                        onclick="_gaq.push(['_trackEvent','pdp_click', Myntra.Data.pageName, 'saved_title']);"
                        target="_blank">{$product.productStyleName}</a> {else} <span>{$product.productStyleName}</span>
                    {/if}
                </div>
                {if $product.productPrice eq 0 || $product.freeItem}
                    <div class="amount">0</div>
                {else} 
                {if $product.pricePerItemAfterDiscount neq ''}
                    <div class="discount">
                        <span class="red">{$rs}</span>
                        <span class="strike gray">{$product.productPrice*$product.quantity|round|number_format:0:".":","}</span>
                        <span> {($product.pricePerItemAfterDiscount*$product.quantity)|round|number_format:0:".":","}</span>
                    </div>
                    <div class="amount red">
                        {include file="string:{$product.discountDisplayText}"}
                    </div>
                {else}
                <div class="amount red">{$rs}
                    {($product.productPrice*($product.quantity-$product.discountAmount))|round|number_format:0:".":","}
                </div>
                {/if}
                <div class="action">
                    <form method="post" action="/mksaveditem.php" class="move-to-cart">
                        <input type="hidden" name="actionType" value="moveToCart"> <input
                            type="hidden" name="itemId" value="{$itemid}"> <input
                            type="hidden" name="redirectTo" value="cart"> <input
                            type="hidden" name="_token" value="{$USER_TOKEN}">
                        <button type="submit"  style="display:none;" class="mk-move-saved-item-to-bag link-btn"
                            onclick="_gaq.push(['_trackEvent', 'cart', 'move_saved_item_to_bag'])">BAG</button>
                    </form>
                    <form method="POST" action="/mksaveditem.php"
                        class="saved-remove-item">
                        <input type="hidden" name="actionType" value="delete"> <input
                            type="hidden" name="itemId" value="{$itemid}"> <input
                            type="hidden" name="redirectTo" value="cart"> <input
                            type="hidden" name="_token" value="{$USER_TOKEN}">
                        <button type="submit"  style="display:none;" class="mk-remove-saved-item link-btn"
                            onclick="_gaq.push(['_trackEvent', 'cart', 'remove_saved_item'])">REMOVE</button>
                    </form>
                    <span class="mk-edit"></span>
                </div>
                {/if}
            </div>
        </div>
        {/foreach} 
    {if $isCombo}
    	</div>
    {/if} 
   </div>
   {/foreach}
