{extends file="layout-checkout.tpl"}

{block name=body}
<script>
Myntra.Data.pageName="shipping_address";
{if $expressbuy eq '1'}
    var fromPage = "expressbuy.php";
{else}
    var fromPage = "mkpaymentoptions.php";
{/if}
</script>
<div class="checkout address-page mk-cf">
    
{include file="checkout/slide-summary.tpl"}
    <div class="main-content">
        <h2>Choose an Address</h2>
        <div class="address-list-wrap">
        </div>

        <div class="page-action select-address">
            <button class="btn primary-btn btn-continue btn-orange">Continue <span class="ico-login-small"></span></button>
        </div>
        <div class='or'>OR</div>
        <h2 class="formheader">CREATE A NEW ADDRESS</h2>
        <div class="address-form-wrap"></div>
        
        <div class="page-action">
            <button class="btn primary-btn btn-save-continue btn-orange">Save &amp; Continue <span class="ico-login-small"></span></button>
        </div>
    </div>
    
   
</div>
{/block}
{block name=macros}
    {include file="macros/cart-macros.tpl"}
{/block}

