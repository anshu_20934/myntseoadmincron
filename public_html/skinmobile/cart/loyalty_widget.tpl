	{if $loyaltyEnabled && $showLoyaltyBlock}
	    <div class="coupons-widget mk-f-left" id="cart_loyalty_options">
			<form action="mkpaymentoptions.php{if $addressID&&$addressID>0 }?address={$addressID}{/if}" method="post" id="loyaltyPointsForm" name="loyaltyPointsForm" class="{if $loyaltyPointsUsageMessage && $loyaltyPointsdivid eq 'success' } hide {/if}" onsubmit="return redeemLoyaltyPoints();">
		    	<div class="coupon-block">
		        	<div class="cashback-message">
						{if $totalActiveLoyaltyPoints eq 0}
							You do not have any Points.</br>Complete this purchase to earn {$loyaltyPointsEarnAfterPurchase} Points
						{elseif $totalActiveLoyaltyPoints > 0 && !$isLoyaltyPointsAboveThreshold}
							You have {$totalActiveLoyaltyPoints} Points. You need minimum {$loyaltyPointsThreshold} Points to use them as cash.
						{/if}
	                </div>
	        		<input type="hidden" value="{$totalActiveLoyaltyPoints}" name="userLoyaltyPoints" id="userLoyaltyPoints">
	        		<input type="hidden" value="" name="loyaltyPoinsUsage" id="loyaltyPoinsUsage">
			     </div>
				{if $totalActiveLoyaltyPoints && $isLoyaltyPointsAboveThreshold}
                	<div class="redeem-lp"></div>
                	<span class="cashback-heading">Redeem Points</span><br/> 
                	<span class="cashback-msg">
                    	Worth <span class="rupees discount">Rs. {$totalActiveLoyaltyPointsRupees|number_format:0:".":","} </span>
                	</span>
                {/if}
     		</form>
			{if $loyaltyPointsdivid eq 'success'}
                <div class="remove-lp"></div>
                <span class="cashback-heading">Points redeem</span><br/> 
                <span class="cashback-msg">{$loyaltyPointsUsageMessage}</span>
			{/if}
            {if $loyaltyPointsdivid eq 'error'}
                 {if $loyaltyPointsUsageMessage}
                      <div class="err-div">
                            <span class="{$divid} mt10 err err-msg" id="loyalty_points_message">
                                   {$loyaltyPointsUsageMessage}
                            </span>
                      </div>
                 {/if}                                                                                                                                                         
            {/if}

		</div>
	{/if}



