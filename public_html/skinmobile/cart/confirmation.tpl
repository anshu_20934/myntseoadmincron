{extends file="layout-checkout.tpl"}
{block name=body}
{$rs="<span class=rupees>Rs. </span>"}
    <script type="text/javascript">
        var orderid='{$orderid}';
        Myntra.Data.socialAction='purchase';
        Myntra.Data.pageName='confirmation';
        var socialPost = {$socialPost};
        var socialPostArray={$socialArray};
    </script>
    <script>
            Myntra.Confirmation = Myntra.Confirmation || [];
            Myntra.Confirmation.Data = {
            orderId : {$orderid},
            shipping : {$shippingCharges},
            shippingCode : '{$userinfo.s_zipcode}',
            shippingCity : '{$userinfo.s_city}',
            paymentOption : '{$paymentoption}',
            couponCode : '{$couponCode}',
            totalAmount : {$amountafterdiscount|string_format:'%d'},
            totalQuantity : {$totalQuantity},
            products : '{$products}',
            productIds : '{$styleIdList}',
            isFirstOrder : {$isFirstOrder},
       };
        var cartItemIds = Myntra.Confirmation.Data.productIds.split(',');
        var pdpServiceUrl = '{$pdpServiceUrl}';
        var cartGAData = {};      
     </script>

<script language="JavaScript" src="https://media.richrelevance.com/rrserver/js/1.0/p13n.js"></script>
<script type="text/javascript">
   // rich relevance instrumentation
   
   function getRRCookie(cname)
	{
		if(localStorage){
			var ls_rr_sid = localStorage.getItem('lscache-rr_sid'); // geting from local storage
			
			if( ls_rr_sid ){
				return ls_rr_sid;
			}
		}
				
		var name = Myntra.Data.cookieprefix+""+cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) 
		{
		  var c = ca[i].trim();
		  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
		}
		return "";
	}
	
	var R3_COMMON = null;
	var R3_ITEM = null;
   
  
		
		R3_COMMON = new r3_common();

		
		R3_COMMON.setApiKey('54ed2a6e-d225-11e0-9cab-12313b0349b4');
		
		if(Myntra.Data.cookieprefix != "" ){
			R3_COMMON.setBaseUrl(window.location.protocol+'//integration.richrelevance.com/rrserver/');
		}else{
			R3_COMMON.setBaseUrl(window.location.protocol+'//recs.richrelevance.com/rrserver/');
		}
		
		R3_COMMON.setClickthruServer(window.location.protocol+"//"+window.location.host);
		var rr_sid = getRRCookie("rr_sid")+"";
		R3_COMMON.setSessionId(rr_sid);
		
		if(typeof Myntra.Data.userHashId == "undefined" || Myntra.Data.userHashId == "" ){
			R3_COMMON.setUserId(rr_sid);
		}else{
			R3_COMMON.setUserId(Myntra.Data.userHashId+"");
		}
		
		RR.jsonCallback = function (){
			//console.dir(RR.data.JSON.placements);
			//console.dir("done");
		}; // call back performed, once rr call is executed.

		var R3_PURCHASED = new r3_purchased();
				
		R3_PURCHASED.setOrderNumber(Myntra.Confirmation.Data.orderId+'');
		
		try{
			var products = JSON.parse(Myntra.Confirmation.Data.products);
		}catch(err){
			return; // unable to parse the items
		}
		
		
		products.forEach(function(k,v){			
			R3_PURCHASED.addItemIdPriceQuantity(k.id+'', k.price+'', k.quantity+'');		
		});
				
		

		rr_flush_onload();
		
		r3();
		
	
</script>

{*include file="cart/conversion_trackings.tpl"*}
  <div class="checkout confirm-page mk-cf">
   
   

    {* mainContent *}
    <div class="main-content">
     <h2>Confirmation</h2>
        {if $message}
            <p class="message">
                {$message}
            </p>
        {else}
            <p class="success-msg">
                Your order has been placed.
                <br/>
                <div>Order No: {$orderid}</div>
                {*<ul class="amount">
                    <li class="shipping-charges">
                        <div class="name">Shipping</div>
                        {if $shippingRate > 0}
                            <div class="value">{$rs} {$shippingRate|round|number_format:0:".":","}</div>
                        {else}
                            <div class="value red">Free</div>
                        {/if}
                    </li>
                    {if $paybyoption eq 'emi'}
                        <li class="emi-charges">
                            <div class="name">EMI Processing Fee</div>
                            {if $emi_charge > 0}
                                <div class="value rupees red">{$rs} {$emi_charge|round|number_format:0:".":","}</div>
                            {else}
                                <div class="value red">Free</div>
                            {/if}
                        </li>
                    {/if}
                    {if $giftamount > 0}
                        <li class="shipping-charges">
                            <div class="name">Gift Charges</div>
                            <div class="value rupees red">{$rs} {$giftamount|round|number_format:0:".":","}</div>
                        </li>
                    {/if}
                    <li class="order-total">
                        <div class="name">Amount charged</div>
                        <div class="value red">
                            {$rs} {math equation="a-b+c+d" a=$grandTotal b=$eossSavings c=$shippingRate d=$giftamount assign=subTotal}{$subTotal|round|number_format:0:".":","}
                            {if isset($eossSavings) && $eossSavings gt 0}
                                {math equation="d+e+f" d=$grandTotal e=$shippingRate f=$giftamount assign=total}
                                <span class="strike gray">{$total|round|number_format:0:".":","}</span>
                            {/if}
                        </div>
                        {if isset($eossSavings) && $eossSavings gt 0}
                             <br><div class="value discount">DISCOUNT {$rs} {$eossSavings|round|number_format:0:".":","}</div>
                        {/if}
                    </li>
                    {if $cartLevelDiscount neq 0}
                        <li class="coupon-discount">
                            <div class="name">Bag Discount</div>
                            <div class="value discount red">(-) {$rs} {$cartLevelDiscount|round|number_format:0:".":","}</div>
                        </li>
                    {/if}
                    {if $codCharges && $codCharges gt 0}
                    <li class="cod-charges">
                        <div class="name">COD Charges</div>
                        <div class="value">{$rs} $codCharges</div>
                    </li>
                    {/if}
                    {if $coupon_discount neq 0}
                        <li class="coupon-discount">
                            <div class="name">Coupon</div>
                            <div class="value discount red">(-) {$rs} {$coupon_discount|round|number_format:0:".":","}</div>
                        </li>
                    {/if}
                    {if $cashCouponCode && $totalcashdiscount gt 0}
                        <li class="cash-discount">
                            <div class="name">Cashback</div>
                            <div class="value discount red">(-) {$rs} {$totalcashdiscount|round|number_format:0:".":","}</div>
                        </li>
                    {/if}
                    <li class="additional-discount hide">
                        <div class="name">Additional Discounts</div>
                        <div class="value discount red">(-) {$rs} 0.00</div>
                    </li>
                    <li class="grand-total">
                        <div class="name">You {if $paybyoption eq 'cod'}Pay{else}Paid{/if}</div>
                    <div class="you-pay value red">{$rs} {$amountafterdiscount|round|number_format:0:".":","}</div>
                    </li>
                </ul>
                *}
            </p>
            
        {/if}
        {if $paybyoption == "cod"}
            <div>
                {*<p class="body-txt"><span>Payment through <span class="red"> cash on delivery</span></span>. <br> Please keep <span class="red">{$rupeesymbol} {$amountafterdiscount|round|number_format:0:".":","}</span> ready in cash at the shipping address.</p>*}
                <p class="body-txt"><span>Amount due <span class="red">{$rupeesymbol} {$amountafterdiscount|round|number_format:0:".":","}</span> at the time of delivery.</p>
            </div>
        {else}
            <div>
                <p class="body-txt"><span>Amount charged <span class="red">{$rupeesymbol} {$amountafterdiscount|round|number_format:0:".":","}</span>.</p>
            </div>
        {/if}
        <div class="google-play-button">
            <div class="play-img">
                <a href="https://play.google.com/store/apps/details?id=com.myntra.android&referrer=utm_campaign%3DOrganic-App-Install%26utm_source%3DM-site%26utm_medium%3DOrder-Confirmation%26utm_content%3DDownload-App"><img alt="Myntraapp on Google Play" src="https://developer.android.com/images/brand/en_app_rgb_wo_45.png"></a>
            </div>
            <p class="play-msg">NOW YOU CAN ALSO TRACK YOUR ORDER ON THE MYNTRA ANDROID APP</p>
        </div>
        <div>
	    <p class="body-txt">Your order is currently being processed. As the product(s) in your order are shipped, you will receive an email stating the expected delivery date for your product(s).</p>    
            <p class="body-txt">A confirmation email has been sent to <span class="email">{$userinfo.b_email}</span></p>
            <p class="body-txt"><strong>Note :We do not demand your banking and credit card details verbally or telephonically. Please do not divulge your details to fraudsters and imposters falsely claiming to be calling on Myntra.com's behalf.</strong></p>
        </div>

		{if $confirmationConfigMsg}
			 <div id="special-coupons" class="info-block">	    	 	
		    	<p class="body-txt">
		    	
					<em >{$confirmationConfigMsg}</em><br />
				</p>
			</div>
		{/if}
		{if $fest_coupons}
	    <div id="special-coupons" class="info-block">
	    	
	    	{if $coupon_percent }
	    	
		    	<h3>YOU HAVE WON A 100% CASHBACK COUPON{if $fest_coupons|count > 1}S{/if}</h3>
		    	<p class="body-txt">
		    	
					<em >You have won a cashback coupon worth Rs. {$coupon_value}. <br />This coupon can be used for a {$coupon_percent}% off up to a maximum discount of Rs. {$coupon_value}.</em><br />
				</p>
	    	
	    	
	    	{else}
	    	
		    	<h3>YOU HAVE WON {$fest_coupons|count} COUPON{if $fest_coupons|count > 1}S{/if}</h3>
		    	<p class="body-txt">
		    	
					<em >You have won {$fest_coupons|count} coupons worth Rs. {$coupon_value} each</em><br />
				</p>
			
			{/if}
			
			{*
			<p class="body-txt">
			You can use these coupons on your future purchases at Myntra<br />
								<span class="or">OR</span>
					<a class="share-link" href="{$http_location}/mymyntra.php?view=mymyntcredits#top">TRANSFER THEM TO YOUR FRIENDS</a>
	    	</p> *}
	    	
				<div class="mk-mynt-promo-codes mk-active-coupons">
				<div class="mk-coupons-content">
				<table>
					<col width="120" />
					<col width="290" />
					<col width="190" />
					<tr class="mk-table-head"> 
						<th>Coupon</th>
						<th class="left">Description</th>
						<th>Exp Date</th>
					</tr>
					{section name=prod_num loop=$fest_coupons}
					<tr class="{cycle name='active_cycle' values='mk-odd,mk-even'}">
						<td>{$fest_coupons[prod_num].coupon|upper}</td>
						<td class="left">
						{if $fest_coupons[prod_num].couponType eq "absolute"}
                           	<span class="rupees">Rs. {$fest_coupons[prod_num].MRPAmount|round}</span> off
                           {elseif $fest_coupons[prod_num].couponType eq "percentage"}
                           	{$fest_coupons[prod_num].MRPpercentage}% off
                           {elseif $fest_coupons[prod_num].couponType eq "dual"}
                           	{$fest_coupons[prod_num].MRPpercentage}% off upto
                           	<span class="rupees">Rs. {$fest_coupons[prod_num].MRPAmount|round}</span>
                        	{/if}
                       	{if $fest_coupons[prod_num].minimum neq '' && $fest_coupons[prod_num].minimum gt 0}
                           	on a minimum purchase of <span class="rupees">Rs. {$fest_coupons[prod_num].minimum|round}</span>.
                       	{/if}
						</td>
						<td>{$fest_coupons[prod_num].expire|date_format:"%e %b, %Y"}
                       	({math equation= "ceil(x/86400) - ceil(y/86400) + 1" x=$fest_coupons[prod_num].expire y=$today assign="valid_days"}{$valid_days}
                       	{if $valid_days eq 1}day{else}days{/if} to expiry)</td>
					</tr> 
					{/section}
				</table>
				</div>
			</div>
		</div>
		{/if}

        {if $loyaltyEnabled && $totalLoyaltyPointsAwarded}
            <div id="special-coupons" class="info-block">
                <h3>{$totalLoyaltyPointsAwarded} Privilege Point{if $totalLoyaltyPointsAwarded gt 1}s{/if} <span class="gray">added to your account for this purchase </span></h3>
            </div>    
        {/if}

        <a href="{$http_location}" title="Continue Shopping" class="btn normal-btn" onClick="_gaq.push(['_trackEvent', 'confirmation', 'continue_shopping']);return true;">Continue Shopping</a>
    </div>
    {*mainContent@End*}
    {include file="checkout/confirmation-summary.tpl"}
</div>
    {if $fireSyncGACall}
    <script language="JavaScript" src="https://ssl.google-analytics.com/ga.js"></script>
    {/if}
        <script type="text/javascript">
        try {ldelim}
            //var pageTracker = _gat._getTracker("{$ga_acc}");
            _gaq.push(['_setCustomVar',
            //pageTracker._setCustomVar(
                    1,
                    "Already-Bought",
                    "Yes",
                    1]);

                _gaq.push(['_addTrans',
                "{$orderid}",                                     // Order ID
                "",
                "{$amountafterdiscount|string_format:'%.2f'}",      // Total
                "{$vat}",                                           // Tax
                "{$shippingRate}",                                // Shipping
                "{$userinfo.s_city}",                             // City
                "{$userinfo.s_state}",                            // State
                "{$userinfo.s_country}"]                              // Country
                );

            {assign var='loopIndex' value=0}
            {assign var='netTotalPrice' value=0}
            {assign var='productidDelimString' value=''}
            {assign var='productTypeDelimString' value=''}
            {assign var='totalQuantity' value=0}

            {***products detail for my things conversion***}
            var myThingsProducts = new Array();

            {foreach name=outer item=product from=$productsInCart}
                {foreach key=key item=item from=$product}
                    {if $key eq 'productId'}
                    {assign var='productId' value=$item}
                    {assign var='productidDelimString' value=$productidDelimString|cat:$productId|cat:"|"}
                    {/if}

                    {if $key eq 'unitPrice'}
                    {assign var='unitPrice' value=$item}
                    {/if}

                    {if $key eq 'productStyleName'}
                    {assign var='productStyleName' value=$item}
                    {/if}

                    {if $key eq 'quantity'}
                    {assign var='quantity' value=$item}
                    {assign var='totalQuantity' value=$quantity+$totalQuantity}
                    {/if}

                    {if $key eq 'discount'}
                    {assign var='discount' value=$item}
                    {/if}

                    {if $key eq 'coupon_discount'}
                    {assign var='coupon_discount' value=$item}
                    {/if}

                    {if $key eq 'productTypeLabel'}
                    {assign var='productTypeLabel' value=$item}
                    {assign var='productTypeDelimString' value=$productTypeDelimString|cat:$productTypeLabel|cat:"|"}
                    {/if}

                      {if $key eq 'productCatLabel'}
                    {assign var='productCatLabel' value=$item}
                    {/if}

                    {if $key eq 'totalPrice'}
                    {assign var='totalPrice' value=$item}
                    {assign var='netTotalPrice' value=$netTotalPrice+$totalPrice}
                    {/if}

                    {***added for tyroo conversion tracking***}
                    {if $key eq 'productPrice'}
                        {assign var='productPrice' value=$item}
                        {*assign var='itemString' value=":prod:$productPrice:qty:$quantity"*}
                        {*assign var='lineItemString' value=$lineItemString|cat:$itemString*}
                    {/if}
                    {***tyroo till here***}

                {/foreach}
                {assign var='itemString' value=":prod:$productPrice:qty:$quantity"}
                {assign var='lineItemString' value=$lineItemString|cat:$itemString}
                {assign var='totalDiscountOnItems' value=$coupon_discount}
                {assign var='discountPerItem' value=$totalDiscountOnItems/$quantity}
                {assign var='unitPriceAfterDiscount' value=$unitPrice-$discountPerItem}



                    _gaq.push(['_addItem',
                    "{$orderid}",            // Order ID
                    "{$productId}",         //pid
                    "{$productStyleName}",  //style
                    "{$productCatLabel}",   //articleType|Brand
                    "{$unitPriceAfterDiscount}",        //unit price
                    "{$quantity}"           // Quantity
                    ]);

                {***products detail for my things conversion    ***}
                myThingsProducts[{$loopIndex}] = {ldelim}id: "{$productId}",price:"{$productPrice|number_format:'2':'.':''}",qty:"{$quantity}"{rdelim};

                {assign var='loopIndex' value=$loopIndex+1}
            {/foreach}

                {***tyroo conversion tracking***}
                    {*****MOD: not sending (p,q) tuples anymore****}
            {*if $lineItemString|strlen > 40*}
                {assign var='lineItemString' value=":prod:$amountafterdiscount:qty:1"}
            {*/if*}

            {*if $payment_method neq 'chq'*}
                _gaq.push(['_trackTrans']);

                {if $recordGACall}
                    {literal}
                        _gaq.push(function(){
                            var newdiv = document.createElement('img');
                            var newGASrc= https_loc+"/baecon/{/literal}{$orderid}{literal}";
                            newdiv.setAttribute("src",newGASrc);
                            newdiv.setAttribute("width","1");
                            newdiv.setAttribute("height","1");
                            newdiv.setAttribute("alt","");
                            document.appendChild(newdiv);
                        });
                    {/literal}
                {/if}

            // Yahoo Conversion Tracking
            /*window.ysm_customData = new Object();
            window.ysm_customData.conversion = "transId={$orderid},currency=INR,amount={$amountafterdiscount|string_format:'%.2f'}";
            var ysm_accountid  = "1GJC30EFMMKEBN6R4ASQ2SBNQCG";
            document.write("<SCR" + "IPT language='JavaScript' type='text/javascript' "
            + "SRC=https://" + "srv2.wa.marketingsolutions.yahoo.com" + "/script/ScriptServlet" + "?aid=" + ysm_accountid
            + "></SCR" + "IPT>");*/

            {*/if*}
        {rdelim} catch(err) {ldelim}{rdelim}
        
        </script>

        <!--my things conversion tracking-->
        
        <!--my things conversion tracking-->
<!--retarget pixel tracking-->
{/block}

{block name=lightboxes}
{/block}
{block name="macros"}
    {include file="macros/confirmation-macros.tpl"}
    {include file="inc/ga.tpl"} 
{/block}
