	{if $cashback_gateway_status eq 'on'}
	    <div class="coupons-widget mk-f-left" id="cart_cash_coupon_options">
    		<!--h2>{if $myntCashdivid eq 'success'}<span class="tick-large-icon"></span>{elseif $myntCashdivid eq 'error'}<span class="wrong-large-icon"></span>{/if}CASHBACK</h2-->
    		{assign var=total_amout_to_be_paid value=$totalAmount+$giftamount+$myntCashUsage}
			{if $myntCashUsageMessage && $myntCashdivid eq 'success'}
				<!--div class="cashback-message" id="discount_cashcoupon_message">
						{$myntCashUsageMessage}
				</div-->
			{/if}
			<form action="mkpaymentoptions.php{if $addressID&&$addressID>0 }?address={$addressID}{/if}" method="post" id="myntCashForm" name="myntCashForm" class="{if $myntCashUsageMessage && $myntCashdivid eq 'success' } hide {/if}" onsubmit="return redeemMyntCash();">
		    	<div class="coupon-block">
		        	<div class="cashback-message">
						{if $myntCashDetails.balance eq 0} You do not have any cashback balance
						{else}<!-- You can redeem <span class="rupees discount">Rs. {if $myntCashDetails.balance lte $totalAmount}{$myntCashDetails.balance|number_format:2}{else}{$totalAmount|number_format:2}{/if}</span> 
							{if $myntCashDetails.balance gt $totalAmount}of your <span class="rupees discount"> Rs. {$myntCashDetails.balance|number_format:2} </span> cashback{/if}-->
						{/if}
	                </div>
	        		<input type="hidden" value="{$myntCashDetails.balance}" id="userCashAmount" class="cb-input" name="userAmount">
	        		<input type="hidden" value="{$cashcoupons.coupon}" id="cashcoupon" class="cb-input" name="cashcoupon">
	        		<input type="hidden" value="{$appliedCouponCode}" id="c-code" class="cb-input" name="c-code">
	        		 <input type="hidden" name="removecouponcode" id="removecouponcode" class="coupon-code">
	        		<input type="hidden" value="" id="removecashcoupon" class="cb-input" name="cashcoupon">
	        		<input type="hidden" value="{$appliedCashbackCode}" id="code" class="cb-input" name="code">
	        		<input type="hidden" value="{$myntCashDetails.balance}" id="userAmount" class="cb-input" name="userAmount">
	        		 <input type="hidden" value="use" id="useMyntCash" class="cb-input" name="useMyntCash">
			     </div>
				{if $myntCashDetails.balance gt 0}<button id="redeemMyntCashButton" class="cashback-btn btn btn-grey mk-hide" onclick="javascript:redeemMyntCash();">Redeem</button>
                <div class="redeem-cb"></div>
                <span class="cashback-heading">Redeem My Cashback</span><br/> 
                <span class="cashback-msg">
                    You can redeem <span class="rupees discount">Rs. {if $myntCashDetails.balance lte $totalAmount}{$myntCashDetails.balance|number_format:2}{else}{$totalAmount|number_format:2}{/if} {if $myntCashDetails.balance gt $totalAmount}of your <span class="rupees discount"> Rs. {$myntCashDetails.balance|number_format:2} </span> cashback{/if}

                    </span>
                </span>
                {/if}
     		</form>
			{if $myntCashdivid eq 'success'}
				<button id="removeMyntCashButton" onclick="javascript:removeMyntCash();" class="cashback-btn btn btn-grey mk-hide">Remove</button>
                <div class="remove-cb"></div>
                <span class="cashback-heading">Cashback Redeemed</span><br/> 
                <span class="cashback-msg">{$myntCashUsageMessage}</span>
			{/if}
             {if $myntCashdivid eq 'error'}
                 {if $myntCashUsageMessage}
                      <div class="err-div">
                            <span class="{$divid} mt10 err err-msg" id="discount_coupon_message">
                                   {$myntCashUsageMessage}
                            </span>
                      </div>
                 {/if}                                                                                                                                                         {/if}

		</div>
	{/if}



