<div class="mk-product-media mk-fit-mobile mk-f-left {if $zoomenabled}zoom-enabled{/if}">
    <div class="flexslider">
      <ul class="slides">
        {foreach $area_icons as $key=>$item}
        <li data-itr={$item@index}>
          {if $item@first}
            <img title="" src='{myntraimage key="style_360_480" src=$item.image|replace:"_images":"_images_360_480"}' class='pdpimages' data-src='{myntraimage key="style_1080_1440" src=$item.image|replace:"_images":"_images_1080_1440"}'>
          {else}
            <img data-iter="{$item@index}" title="" src='{$cdn_base}/skin2/images/spacer.gif' class='pdpimages lazy mk-lazy-white-border' data-lazy-src='{myntraimage key="style_360_480" src=$item.image|replace:"_images":"_images_360_480"}' data-src='{myntraimage key="style_1080_1440" src=$item.image|replace:"_images":"_images_1080_1440"}'>
          {/if}
        </li>
        {/foreach}
      </ul>
    </div>
    <div class="zoom-tooltip hide">Tap to Zoom<br>Swipe for more views</div>
</div>
<script type="text/javascript">
  var pdpImagePaths = new Object(); 
  {foreach $area_icons as $key=>$item}
    {literal}
    pdpImagePaths[{/literal}{$key}{literal}] = {"src" : {/literal}'{myntraimage key="style_1080_1440" src=$item.image|replace:"_images":"_images_1080_1440"}'{literal}, "loaded": false};
    {/literal}
  {/foreach}
</script>