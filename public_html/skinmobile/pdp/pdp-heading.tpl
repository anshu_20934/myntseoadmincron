<div class="mk-pdp-heading">
    <div class="mk-pdp-carousel-breadcrumbs ">
        {foreach key=navitemidx item=navitem from=$nav_selection_arr_url}
            {if $navitemidx|strpos:"<img" !== 0 }
                {if !$navitem@first}/ {/if}<a href="{$navitem}">{$navitemidx}</a>
            {/if}
        {/foreach}
    </div>
    <div class="mk-product-name"><h1>{$productDisplayName}</h1></div>
</div>