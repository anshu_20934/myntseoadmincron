<div class="zoom-overlay mk-hide"></div>
<div class="zoom-block mk-hide">
    <div id="zoom-container" class="zoom-container rs-carousel szoom-carousel">
        <div class="close-btn"><span class="close-icon"></span></div>
        <div id="zoom-wrapper">
            <div id="zoom-scroll">
                <ul class="mk-pdp-zoom rs-carousel-runner szoom to-be-loaded">
                {assign var=i value=0}
                {section name="area" loop=$area_icons}
                    <li id="product-zoom-img" class="product-zoom-img {if $smarty.section.area.first}mk-show{else}mk-hide{/if}">
                        <img class="szoom-imgs" src='{$cdn_base}/skin2/images/spacer.gif'  data-src='{myntraimage key="style_1080_1440" src=$area_icons[area].image|replace:"_images":"_images_1080_1440"}' />
                    </li>
                    {assign var=i value=$i+1}
                {/section}
                </ul>
            </div>
        </div>
        <img class="szoom-loader mk-show mk-zoom-loaders" width="35" height="29" src="{$cdn_base}/skin2/images/loader_transparent.gif">
        <span class="prev-next-btns prev-icon" id="view-prev-szoom"></span>
        <span class="prev-next-btns next-icon" id="view-next-szoom"></span>
    </div>
    <div class="zoom-thumbs">
        <ul class="mk-pdp-zoom-thumbs">
        {assign var=i value=0}
        {section name="area" loop=$area_icons}
        <li class="product-zoom-img-thumbs" data-thumb-pos="{$i}">
            <img src='{$cdn_base}/skin2/images/spacer.gif' data-thumb-pos="{$i}" data-lazy-src={myntraimage key="style_48_64" src=$area_icons[area].image|replace:"_images":"_images_48_64"} />
        </li>
        {assign var=i value=$i+1}
        {/section}
    </div>
</div>
