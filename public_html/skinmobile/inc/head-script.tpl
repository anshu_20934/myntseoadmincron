
    <script>
        var _gaq = _gaq || [];
        var pluginUrl ='//www.google-analytics.com/plugins/ga/inpage_linkid.js';
        _gaq.push(['_require', 'inpage_linkid', pluginUrl]);
        _gaq.push(['_setAccount', '{$ga_acc}']);
        _gaq.push(['_setDomainName', '{$cookiedomain}']);
        var http_loc = '{$http_location}';
        var https_loc = '{$https_location}';
        var cdn_base = '{$cdn_base}';
        var secure_cdn_base = '{$secure_cdn_base}';
        var facebook_app_id = '{$facebook_app_id}';
        var facebook_app_name='{$facebook_app_name}';
        var s_account= '{$analytics_account}';
        var social_share_variant='{$socialShareVariant}';
        {literal}
        var Myntra = Myntra || {};
        Myntra.Data = Myntra.Data || {};
        {/literal}
        Myntra.Data.userEmail = "{$login}";
        Myntra.Data.userHashId = "{$AVAIL_USER_ID_MD5}";
        Myntra.Data.userFBId ="{$facebook_uid}";
        Myntra.Data.socialAction = 'wishlist';
        Myntra.Data.socialExcludeText = "{$socialPermissionCategoryExcludeText}";
        Myntra.Data.nonFBRegCouponValue = "{$mrp_nonfbCouponValue}";
        Myntra.Data.fbRegCouponValue = "{$mrp_fbCouponValue}";
        
        Myntra.Data.RegCouponSignUpOverLayList = "{$regCouponSignUpOverLayList}";
        Myntra.Data.signupOffersTextMobile = "{$signupOffersTextMobile}";
        Myntra.Data.FBRegCouponSignUpOverLayList = "{$fBregCouponSignUpOverLayList}";
        
        Myntra.Data.token = "{$USER_TOKEN}";
        Myntra.Data.showAutoSuggest = "{$enableAutoSuggest}";
        Myntra.Data.showAdminAdditionalStyleDetails = "{$showAdminAdditionalStyleDetails}";
		Myntra.Data.cookiedomain = "{$cookiedomain}";
	    Myntra.Data.cookieprefix = "{$cookieprefix}";
        Myntra.Data.loginTimeout = {if $loginTimeout}{$loginTimeout*1000}{else}15000{/if};
        Myntra.Data.enableGuestLogin = {if $enableGuestLogin}1{else}0{/if};
       	Myntra.Data.configMode = "{$configMode}";
        Myntra.Data.enableSpy = {if $enableSpy}1{else}0{/if};
       	Myntra.Data.spyId = {if $enableSpy}"{$spyId}"{else}undefined{/if};
        Myntra.Data.pageName="static";
        Myntra.Data.lazyElems = new Array();
        Myntra.Data.lazySeparator = "#!#";
        Myntra.Data.ServerCheck = "{$nmServerName}";
        Myntra.Data.EmailUUID = {if $email_uuid}"{$email_uuid}"{else}""{/if};
        Myntra.Data.isTablet = {if $isTablet}true{else}false{/if};
        Myntra.Data.isMobileApp = {if $isMobileApp}true{else}false{/if};
        Myntra.Data.tooltipTextDuration = {if $homeTooltipDuration}{$homeTooltipDuration}{else}5000{/if};
        Myntra.Data.UTM = Myntra.Data.UTM || {};
        Myntra.Data.UTM.Source = "{$utm_source}";
        Myntra.Data.UTM.Medium = "{$utm_medium}";
        Myntra.Data.UTM.Campaign = "{$utm_campaignName}";
        Myntra.Data.UTM.CampaignId = "{$utm_campaignId}";
        Myntra.Data.UTM.OctaneEmail = "{$utm_octaneEmail}";
        Myntra.Data.Device = Myntra.Data.Device || {};
        Myntra.Data.Device.isTablet = {$isTablet};
        Myntra.Data.Device.isMobile = {$isMobile};
        Myntra.Data.Device.Type = "{$deviceType}";
        Myntra.Data.Device.Name = "{$deviceName}";
        Myntra.Data.isLoggedIn = '{if $login}1{else}0{/if}';
        Myntra.Data.isBuyer = '{if $returningCustomer}1{else}0{/if}';
        Myntra.Data.promoCoupons = '{$promoCoupons}';
        Myntra.Data.promoCouponsVariant = '{$promoCouponsVariant}';
        Myntra.Data.promoCouponsMobileVariant = '{$promoCouponsMobileVariant}';
    </script>

    {if $tracelytics_header}
        {$tracelytics_header}
    {/if}

    <script>
        trackNode = ((document.getElementsByTagName('head') || [null])[0] || 
                        document.getElementsByTagName('script')[0].parentNode); 
     {literal}
        // tracking wrapper: args are same as GA _trackevent 
        function myntrack(category, action, opt_label, opt_value, opt_noninteraction) {
            var args = Array.prototype.slice.call(arguments);
            if (args.length < 3) { return; }
            args.unshift('_trackEvent');
            _gaq.push(args);
        }
      {/literal}
    </script>

    <!--[if lte IE 8]>
        {if $smarty.server.HTTPS}
        <script type="text/javascript" src="{$secure_cdn_base}/skin2/js/html5shiv.js.jgz"></script>
        <script type="text/javascript" src="{$https_location}/skinmobile/js/css3-mediaqueries.js"></script>
        {else}
        <script type="text/javascript" src="{$cdn_base}/skin2/js/html5shiv.js.jgz"></script>
        <script type="text/javascript" src="{$http_location}/skinmobile/js/css3-mediaqueries.js"></script>
        {/if}
    <![endif]-->
