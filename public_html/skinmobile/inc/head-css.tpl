<!--

/* @license
 * MyFonts Webfont Build ID 1917439, 2012-01-13T10:14:35-0500
 *
 * The fonts listed in this notice are subject to the End User License
 * Agreement(s) entered into by the website owner. All other parties are
 * explicitly restricted from using the Licensed Webfonts(s).
 *
 * You may obtain a valid license at the URLs below.
 *
 * Webfont: DIN Mittel Bold by Elsner+Flake
 * URL: http://www.myfonts.com/fonts/ef/dinmittel/ef-bold/
 * Licensed pageviews: 10,000
 *
 * Webfont: DIN Mittel Medium by Elsner+Flake
 * URL: http://www.myfonts.com/fonts/ef/dinmittel/medium/
 * Licensed pageviews: unspecified
 *
 *
 * License: http://www.myfonts.com/viewlicense?type=web&buildid=1917439
 * Webfonts copyright: Copyright of design and/or digital redesign and Trademark by Veronika Elsner, Guenther Flake GbR or its licensors. Copyright of digital font software by Veronika Elsner, Guenther Flake GbR. This font software may not be reproduced, modified, decoded, disc
 *
 * © 2012 Bitstream Inc
*/




 -->

{if $isMobileApp}

  {if $smarty.server.HTTPS}
    {literal}
      <style type="text/css">
        @font-face {
           font-family: 'WhitneyHTF-Book';
            src: url('{/literal}{$https_location}{literal}/skin2/fonts/WhitneyHTF-Book.eot?#iefix') format('eot'),
               url('{/literal}{$https_location}{literal}/skin2/fonts/WhitneyHTF-Book.woff') format('woff'),
               url('{/literal}{$https_location}{literal}/skin2/fonts/WhitneyHTF-Book.ttf') format('truetype');
        }

        @font-face {
           font-family: 'WhitneyHTF-SemiBold';
            src: url('{/literal}{$https_location}{literal}/skin2/fonts/WhitneyHTF-SemiBold.eot?#iefix') format('eot'),
               url('{/literal}{$https_location}{literal}/skin2/fonts/WhitneyHTF-SemiBold.woff') format('woff'),
               url('{/literal}{$https_location}{literal}/skin2/fonts/WhitneyHTF-SemiBold.ttf') format('truetype');
        }

      </style>
    {/literal}
  {else}
    {literal}
    <style type="text/css">
        @font-face {
           font-family: 'WhitneyHTF-Book';
            src: url('{/literal}{$http_location}{literal}/skin2/fonts/WhitneyHTF-Book.eot?#iefix') format('eot'),
               url('{/literal}{$http_location}{literal}/skin2/fonts/WhitneyHTF-Book.woff') format('woff'),
               url('{/literal}{$http_location}{literal}/skin2/fonts/WhitneyHTF-Book.ttf') format('truetype');
        }

        @font-face {
           font-family: 'WhitneyHTF-SemiBold';
            src: url('{/literal}{$http_location}{literal}/skin2/fonts/WhitneyHTF-SemiBold.eot?#iefix') format('eot'),
               url('{/literal}{$http_location}{literal}/skin2/fonts/WhitneyHTF-SemiBold.woff') format('woff'),
               url('{/literal}{$http_location}{literal}/skin2/fonts/WhitneyHTF-SemiBold.ttf') format('truetype');
        }
    </style>
    {/literal}
  {/if}

{else}

  {if $smarty.server.HTTPS}
    {literal}
    <style type="text/css">
     @font-face {
       font-family: 'din-med';
        src: url('{/literal}{$https_location}{literal}/skin2/fonts/1D41FF_1_0.eot?#iefix') format('eot'),
           url('{/literal}{$https_location}{literal}/skin2/fonts/1D41FF_1_0.woff') format('woff'),
           url('{/literal}{$https_location}{literal}/skin2/fonts/1D41FF_1_0.ttf') format('truetype');
    }
    </style>
    {/literal}
  {else}
    {literal}
    <style type="text/css">
     @font-face {
       font-family: 'din-med';
        src: url('{/literal}{$http_location}{literal}/skin2/fonts/1D41FF_1_0.eot?#iefix') format('eot'),
           url('{/literal}{$http_location}{literal}/skin2/fonts/1D41FF_1_0.woff') format('woff'),
           url('{/literal}{$http_location}{literal}/skin2/fonts/1D41FF_1_0.ttf') format('truetype');
    }
    </style>
    {/literal}
  {/if}


{/if}


    {foreach from=$cssFiles item=href}
    <link href="{$href}" rel="stylesheet" type="text/css">
    {/foreach}
<!--[if IE]>
    {if $smarty.server.HTTPS}
    <link rel="stylesheet" href="{$secure_cdn_base}/skin2/css/myntra-ie1.css.jgz">
    {else}
    <link rel="stylesheet" href="{$cdn_base}/skin2/css/myntra-ie1.css.jgz">
    {/if}
<![endif]-->
