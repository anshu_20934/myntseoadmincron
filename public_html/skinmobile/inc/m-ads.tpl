<div id="m-ad-cnt">
    {if isset($is_mobile_home) && isset($mobile_promotions)}
        <div class="mobile-ad-home-wrapper">
            {foreach from=$mobile_promotions key=k item=mobile_promotion}
                <div class="banner">
                    {if isset($mobile_promotion.coupon)}
                        <div class="bookmark-icon"></div>
                    {/if}
                    <a href="{$mobile_promotion.url}" class="mobile-ad mobile-ad-home-margin">
                        <div class="mobile-ad-description">
                            {$mobile_promotion.desc}
                        </div>
                        {if isset($mobile_promotion.coupon)}
                            <span>Use coupon code &nbsp</span>
                            <span class="font-weight:bold">{$mobile_promotion.coupon}</span>
                        {/if}
                    </a>
                </div>
            {/foreach}
        </div>
    {elseif isset($mobile_promotions) }
        {foreach from=$mobile_promotions key=k item=mobile_promotion}
            <div class="banner">
                {if isset($mobile_promotion.coupon)}
                    <div class="bookmark-icon"></div>
                {/if}
                <a href="{$mobile_promotion.url}" class="mobile-ad mobile-ad-home-margin">
                    <div class="left-section ">
                        <div class="mobile-ad-title">
                            {$mobile_promotion.title}
                        </div>
                        <div class="mobile-ad-description">
                            {$mobile_promotion.desc}
                        </div>
                    </div>
                    {if isset($mobile_promotion.coupon) || isset($mobile_promotion.label) }
                        {*both coupon and label/button are not present then dont render this block*}
                        <div class="right-section">
                            {if isset($mobile_promotion.coupon)}
                                <div class="mobile-ad-coupon-text">Use coupon code</div>
                                <div class="mobile-ad-coupon">{$mobile_promotion.coupon}</div>
                            {/if}
                            {if isset($mobile_promotion.label)}
                                <div class="mobile-ad-button">{$mobile_promotion.label}</div>
                            {/if}
                        </div>
                    {/if}
                </a>
            </div>
        {/foreach}
    {/if}
</div>

