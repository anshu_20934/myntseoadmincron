<div class="search-overlay">
	<div class ="search-header">
                <div class="search-form-container">
                <div class="input-wrapper">
                    <form class="mk-search-form" action="" method="" onsubmit="Myntra.Header.menuSearchFormSubmit(this); return false;">
                        <button type="submit" name=""></button>
                        <input type="text" value="" autocomplete="off" data-searchboxtype="searchboxall" class="mk-site-search mk-placeholder mk-f-left" id="search_query" placeholder="SEARCH MYNTRA">
                        <div class="clear-text-btn"></div>
                        
                    </form>
                    </div>
                </div>
		<div class="btn primary-btn search-close-container btn-grey">Cancel</div>
	</div>
	<div class="search-results-body">
		
	</div>
</div>
