<header class="mk-site-hd mk-cf mk-checkout-hd">
       <div class="mk-checkout-header mk-checkout-header-border">
           <div class="customer-promises mk-cf">
               <div>100% SECURE CHECKOUT</div>
               <div class="grey-text">PAY WITH CONFIDENCE</div>
           </div>
        <a href="/">
        <div class="mk-logo">
        {if $smarty.server.HTTPS}
            <img src="https://d6kkp3rk1o2kk.cloudfront.net/skinmobile/images/myntra-logo-v2.png" alt="Myntra" title="Online Shopping store for Branded Shoes, Clothing, Accessories & Fashion wear in India.">
        {else}
            <img src="http://myntra.myntassets.com/skinmobile/images/myntra-logo-v2.png" alt="Myntra" title="Online Shopping store for Branded Shoes, Clothing, Accessories & Fashion wear in India.">
        {/if}
        </div>
        </a>
        {$pn = $smarty.server.REQUEST_URI|regex_replace:"/\?.*$/":""}
        {* TODO: Find a better way to set the selected/completed states of the _steps_ *}
        {$selCart = (
            $pn eq "/mkmycart.php" or
            $pn eq "/mkmycartmultiple.php" or
            $pn eq "/mkmycartmultiple_re.php")}
        {$selLogin = (
            $pn eq "/checkout-login.php")}
        {$selAddr = (
            $pn eq "/checkout-address.php" or
            $pn eq "/mkcustomeraddress.php")}
        {$selPay = (
            $pn eq "/mkpaymentoptions.php" or
            $pn eq "/mkpaymentoptions_re.php")}
        {$tickCart = (
            $pn eq "/checkout-login.php" or
            $pn eq "/checkout-address.php" or
            $pn eq "/mkcustomeraddress.php" or
            $pn eq "/mkpaymentoptions.php" or
            $pn eq "/mkpaymentoptions_re.php" or
            $pn eq "/confirmation.php")}
        {$tickLogin = (
            $pn eq "/checkout-address.php" or
            $pn eq "/mkcustomeraddress.php" or
            $pn eq "/mkpaymentoptions.php" or
            $pn eq "/mkpaymentoptions_re.php" or
            $pn eq "/confirmation.php" or
            $login)}
        {$tickAddr = (
            $pn eq "/mkpaymentoptions.php" or
            $pn eq "/mkpaymentoptions_re.php" or
            $pn eq "/confirmation.php" or
            $selectedaddressid gt 0)}
        {$tickPay = (
            $pn eq "/confirmation.php")}
        {$i=1}
        <!--<ul class="steps clear">
            <li class="checkout-tab {if $selAddr}selected{/if} {if $tickAddr}completed_tab{/if}"> 
            <div class="checkout-tab-num {if $tickAddr}completed_num{/if} {if $selAddr}selected_num{/if}">{if $tickAddr}<span class = "tick"></span>{else}{$i}{/if} </div> 
            <div> DELIVERY </div>
            </li>{$i=$i+1}<li class="checkout-tab  {if $selPay}selected{/if} {if $tickPay}completed_tab{/if}">
             <div class="checkout-tab-num  {if $tickPay}completed_num{/if} {if $selPay}selected_num{/if}">{if $tickPay}<span class = "tick"></span>{else}{$i}{/if}</div> 
            <div>PAYMENT</div>
            </li>
        </ul>
        <span class="mk-f-right checkout-promise">
            256-bit ssl <span class="lock-icon"></span>
        </span>-->
    </div>
    
    
</header>

