{foreach from=$jsFiles item=href}
<script
	type="text/javascript" src="{$href}"></script>
{/foreach} 

{if $prefetchFiles} {literal}
<script>{/literal}
    {foreach from=$prefetchFiles item=prefetchFile}
        Myntra.Data.lazyElems.push('{$prefetchFile}' + Myntra.Data.lazySeparator + "body");
    {/foreach}
{literal}</script>
{/literal} {/if}

