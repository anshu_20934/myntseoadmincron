<footer class="secure mk-site-ft mk-cf">
    <div class="mynt-footer">
        <div class="footer-content">
            <div class="support-block">
                <div><span class="customer-call">{$customerSupportCall}</span><span class="customer-call-separator">/</span><a class="email-support" href="mailto:support@myntra.com">support@myntra.com</a></div>
                <div>© myntra.com</div>
                <div><a href="{$http_location}/faqs#top">FAQs</a><span class="customer-call-separator">/</span><a href="#" onclick="javascript:document.location.href='{$http_location}/sdvo?redirecturl='+document.location">View Desktop Site</a></div>
            </div>
            {if $smarty.server.https}
            <div class="secure-shopping">
                <div><span class="lock-icon"></span></div>
                <div>100% Safe and Secure Shopping</div>
            </div>
            <hr class="main" />
            {else}
            {/if}
        </div>
    </div>
</footer>
