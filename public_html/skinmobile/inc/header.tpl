<div class="m-header {if $showSearchInHeader}with-search-box{/if}">
    <div class="m-header-left">
        <span class="m-slide-menu menu-icon"></span>
        <div class="mk-header-welcome">
        <div class="vertical-saperator"></div>
            {$pn = $smarty.server.REQUEST_URI|regex_replace:"/\?.*$/":""}
            {if $login}
                <span class="mk-welcome welcome-logout mk-left">Welcome, {if $userfirstname neq ""}{$userfirstname|truncate:10}{else}{$login|truncate:10}{/if}
                    <br><span class="mk-sign-out nav-logout-link">Logout</span> 
                <form class="header-logout-form" method="post" action="{$http_location}/include/login.php">
                    <input type="hidden" name="mode" value="logout">
                    <input type="hidden" name="_token" value="{$USER_TOKEN}">
                </form> 
                </span>    
            {elseif $pn eq "/register.php"}
                <p class="link-btn right" >&nbsp</p>
            {else}
                <p class="btn-login right" >Login / Register  </p>
            {/if}
        </div>
    </div>
    <div class="m-header-center">
        <a class="m-myntra-icon" href="{$http_location}"><img src="{$cdn_base}/skinmobile/images/myntra-logo-v2.png" alt="Myntra" title="" /></a>
    </div>
    <div class="m-header-right">
        <a href="{$http_location}/mkmycart.php"><span class="m-cart-icon">{$totalQuantity}</span></a>
    </div>
    {if $homepageLayout}
    <div class="get-started-tooltip-container">
    <div class="get-started-tooltip">
        <div class="get-started-tooltip-arrow"></div>
        <div class="get-started-tooltip-content">{$homeTooltipText}</div>
    </div>    
    </div>
    {/if}
    {if $showSearchInHeader}
    <div class="m-header-search">    
        <div class="mk-search">
            <span class="pseudo-search-box pseudo-search-input" type="text" data-searchboxtype="headersearchbox">SEARCH MYNTRA</span>
        </div> 
    </div>
    {/if}
    <input type='hidden' class="pseudo-rightmenuwidthaspect-container">
</div>