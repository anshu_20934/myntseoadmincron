<div class="m-banners-area m-border {if $isTablet}tablet{/if}">
{foreach name=staticBannerImage key=key item=staticBannerImage from=$staticBanners }
            <span class="m-static-banner {if $isTablet}tablet{/if} {if $smarty.foreach.staticBannerImage.index mod 3 == 2}m-three{/if} {if $smarty.foreach.staticBannerImage.index mod 2 == 1}m-two{/if}"><a href="{$http_loc}{$staticBannerImage.target_url}" onClick="_gaq.push(['_trackEvent', 'banner', window.location.toString(), '{$staticBannerImage.target_url}']);return true;"><img class="lazy" data-lazy-src="{myntraimage key='mobile_static_banner' src=$staticBannerImage.image_url}" alt="{$staticBannerImage.alt_text}" src="{$cdn_base}/skin2/images/spacer.gif"/></a></span>
        {/foreach}

</div>

