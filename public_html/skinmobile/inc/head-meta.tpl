
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=0" />
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <!-- scheck:{$nmServerName} -->
    <meta name="description" content="{$metaDescription|escape}">
    <meta name="keywords" content="{$metaKeywords|escape}">
    <title>{if $pageTitle ne ''}{$pageTitle}{elseif $pageHeader ne ''}{$pageHeader}{else}Myntra{/if}</title>
    {if $noindex eq 2 or $noRobotIndexing}
    <meta name="robots" content="noindex, nofollow" />
    {/if}
    {if $smarty.server.HTTPS}
        <link rel="shortcut icon" href="{$secure_cdn_base}/skin1/icons/favicon.ico">
    {else}
        <link rel="shortcut icon" href="{$cdn_base}/skin1/icons/favicon.ico">
    {/if}
    {* SOCIAL *}
	<link href="https://plus.google.com/109282217040005996404" rel="publisher" />
	<meta property="fb:app_id" content="{$facebook_app_id}" /> 
	<meta property="og:site_name" content="Myntra" />
	<meta property="og:title" content="{$pageTitle|trim}" />
	{block name=baseHttpLoc}{/block}
    {if $pageTypePdp}
		<meta property="og:type" content="{$facebook_app_object_name}" />
		<meta property="og:url" content="{$pageCleanURL}" />
		<meta property="og:image" content="{$defaultImage|replace:'_images':'_images_360_480'}" />
		<meta property="og:description" content="{$fb_meta_decription|escape}" />
		<meta property="fb:admins" content="520074227">
		<link rel="image_src" href="{$defaultImage|replace:'_images':'_images_96_128'}" />
	{else}
		<meta property="og:type" content="website" />
		<meta property="og:url" content="http://www.myntra.com" />
		<meta property="og:image" content="{$cdn_base}/skin2/images/logo.png" />
		<link rel="image_src" href="{$cdn_base}/skin2/images/logo.png" />
	{/if}
	{* end of SOCIAL *}
	
	{if $pageTypeSearch}
	<link rel="canonical" href="{$canonicalPageUrl}">
        {if $previousLinkUrl }
        <link rel="prev" href="{$previousLinkUrl}">	
        {/if}
        {if $nextLinkUrl}
        <link rel="next" href="{$nextLinkUrl}">
        {/if}
	{/if}
