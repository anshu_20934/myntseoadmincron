{if $analyticsStatus}
    {include file="inc/analytics_data_collection.tpl"}
{/if}


<!-- Google Code for Smart Pixel -->

<script type="text/javascript">
        {if $lt_new_session eq "true"}
        {/if}
        {if $_MAB_GA_calls}
            {$_MAB_GA_calls}
        {/if}
	_gaq.push(['_trackPageview']);
	_gaq.push(['_trackPageLoadTime']);
    </script>
<!--[if lt IE 8]>
	<script src="/skin2/js/json2.js" type="text/javascript"></script>
<![endif]-->

{if $tracelytics_footer}
    {$tracelytics_footer}
{/if}