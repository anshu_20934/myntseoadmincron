<div id="nav" data-scrollinit="false">
    <div id="scroller">
        <div id="list">
        <ul>
            <li>
                <span class="pseudo-search-box pseudo-search-input" type="text" data-searchboxtype="navsearchbox">SEARCH MYNTRA</span>
            </li>
        </ul>        
        <ul class="mk-nav-levels mk-nav-level-1 mk-nav-ul mk-level-1 mk-cf">
        {foreach key=indexA item=itemsA from=$topNavigationWidget name=topNavigationWidgetLoop}
        {if $itemsA.child|count gt 0 && $itemsA.link_name|strpos:"<" !== 0}
            {assign var="skip" value="false"}
            {foreach key=indexB item=itemsB from=$itemsA.child}
                    {if $itemsB.link_name|strpos:"<" !== 0}
                        {assign var="skip" value="true"}
                    {/if}
            {/foreach}
           
            {if $skip == "true" }
            <li {if $itemsA@first}class="mk-nav-level-first"{/if}>
                <a data-contextid="mk-level-1" {if $itemsA.child|count gt 0}data-sublistid="mk-level-2-{$itemsA.link_name|lower|replace:' ':'-'|replace:'&':''}"{/if}
                  {if $itemsA.child|count eq 0}href="{$itemsA.link_url}?nav_id={$itemsA.id}"{/if} {if $itemsA@last}rel="nofollow" target="_blank"{/if} onClick="_gaq.push(['_trackEvent', 'megamenu', Myntra.Data.pageName, '{$itemsA.link_name}']);return true;">{$itemsA.link_name|truncate:22}<span class="mk-level1-arrownav"></span></a>
            </li>
            {/if}
        {/if}
        {/foreach}
        </ul>
        {foreach key=indexA item=itemsA from=$topNavigationWidget name=topNavigationWidgetLoop}
        {if $itemsA.child|count gt 0}
        <ul class="mk-nav-levels mk-nav-level-2 mk-nav-ul mk-level-2-{$itemsA.link_name|lower|replace:' ':'-'|replace:'&':''}" style="display:none">
            <li class="mk-nav-level-first mk-nav-back-link"><a data-contextid="mk-level-2-{$itemsA.link_name|lower|replace:' ':'-'|replace:'&':''}" data-parentlistid="mk-level-1"><span class="mk-nav-back"></span>{$itemsA.link_name|truncate:22}</a></li>
            {foreach key=indexB item=itemsB from=$itemsA.child}
            {if $itemsB.link_name|strpos:"<" !== 0}
                {assign var="showAsLink" value="false"}
                {if $itemsB.child|count eq 0}
                {assign var="showAsLink" value="true"}
                {/if}
            {if $showAsLink eq "false"}
            <li>
                <a data-contextid="mk-level-2-{$itemsA.link_name|lower|replace:' ':'-'|replace:'&':''}" data-sublistid="mk-level-3-{$itemsA.link_name|lower|replace:' ':'-'|replace:'&':''}-{$itemsB.link_name|lower|replace:' ':'-'|replace:'&':''}">{$itemsB.link_name|truncate:22}<span class="mk-level1-arrownav"></span></a>
            </li>
            {else}
            <li>
                <a href="{$itemsB.link_url}?nav_id={$itemsB.id}">{$itemsB.link_name|truncate:22}<span class="mk-level1-arrownav hide"></span></a>
            </li>
            {/if}
            {/if}
            {/foreach}   
        </ul>
        {/if}
        {/foreach}
        {foreach key=indexA item=itemsA from=$topNavigationWidget name=topNavigationWidgetLoop}
            {if $itemsA.child|count gt 0}
            {foreach key=indexB item=itemsB from=$itemsA.child}
            {if $itemsB.link_name|strpos:"<" !== 0}
            <ul class="mk-nav-levels mk-nav-level-3 mk-nav-ul mk-level-3-{$itemsA.link_name|lower|replace:' ':'-'|replace:'&':''}-{$itemsB.link_name|lower|replace:' ':'-'|replace:'&':''}" style="display:none">
                <li class="mk-nav-level-first mk-nav-back-link"><a data-contextid="mk-level-3-{$itemsA.link_name|lower|replace:' ':'-'|replace:'&':''}-{$itemsB.link_name|lower|replace:' ':'-'|replace:'&':''}" data-parentlistid="mk-level-1"><span class="mk-nav-back"></span>{$itemsA.link_name|truncate:22}</a></li>
                <li class="mk-nav-level-second mk-nav-back-link"><a data-contextid="mk-level-3-{$itemsA.link_name|lower|replace:' ':'-'|replace:'&':''}-{$itemsB.link_name|lower|replace:' ':'-'|replace:'&':''}" data-parentlistid="mk-level-2-{$itemsA.link_name|lower|replace:' ':'-'|replace:'&':''}"><span class="mk-nav-back"></span>{$itemsB.link_name|truncate:22}</a></li>
                {foreach key=indexC item=itemsC from=$itemsB.child}
                    {if $itemsC.link_name|strpos:"<" !== 0}
                    <li><a href="{$itemsC.link_url}?nav_id={$itemsC.id}">{$itemsC.link_name|truncate:22}<span class="mk-level1-arrownav hide"></span></a></li>    
                    {/if}
                {/foreach}
            </ul>    
            {/if}
            {/foreach}
            {/if}
        {/foreach}
            
        {if !$login}
            <div class="mk-nav-partition">&nbsp;</div>
        {/if}
        <ul class="mk-nav-levels mk-nav-links mk-cf">
            <li class="mk-nav-levels-first user-menu">
            {$pn = $smarty.server.REQUEST_URI|regex_replace:"/\?.*$/":""}
                {if $login}
                <p class="mk-welcome welcome-logout">Welcome, {if $userfirstname neq ""}{$userfirstname|truncate:15}{else}{$login|truncate:15}{/if}
                    <br><span class="mk-sign-out nav-logout-link">Logout</span> 
                <form class="header-logout-form" method="post" action="{$http_location}/include/login.php">
                    <input type="hidden" name="mode" value="logout">
                    <input type="hidden" name="_token" value="{$USER_TOKEN}">
                </form> 
                </p>
   
                {elseif $pn eq "/register.php"}
                    <p class="link-btn right" >&nbsp</p>
                {else}
                    <p class="small-btn btn-login right">Login / Register</p>
                    <input type="hidden" id="loginSplashVariant" value="{$loginSplashVariant}" />
                {/if}
                </li>
            </li>
            <li class="mk-nav-levels-first user-menu checklogin"><a href="{$http_location}/mymyntra.php?view=myorders#top">Track Order</a></li>
            <li class="mk-nav-levels-first user-menu checklogin"><a href="{$http_location}/mkmycart.php#">My Bag {if  $cartTotalQuantity > 0}<span class="m-menu-notification">{$cartTotalQuantity}</span> <span class="m-menu-notification-cart-total mk-f-right"><span class="rupee">Rs. </span> {$totalAmountCart|round|number_format:0:".":","}</span>{/if}</a> </li>
            <li class="mk-nav-levels-first user-menu checklogin"><a href="{$http_location}/mkmycart.php#wishlist">My Wishlist {if login} <span class="m-menu-notification">{$savedItemCount}</span> {/if}</a></li>
            <li class="mk-nav-levels-first user-menu checklogin"><a href="{$http_location}/mymyntra.php#">My Myntra</a></li>
            <li class="mk-nav-levels-first user-menu "><a href="{$http_location}/recently-viewed">Recently Viewed <span class="m-menu-notification">{$recentViewedCount}</span></a> </li>
             <li class="mk-nav-levels-first user-menu "><a href="{$http_location}/faqs">Customer Service</a></li>
          </ul>
          </div>
    </div>
</div>