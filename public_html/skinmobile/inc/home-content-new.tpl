<div class="m-banners-area m-border tablet">
<div class="mk-home-features">
	<div class='col'>
		<a class="mk-home-feature" 
			{if $subBanners[0].target_url|strpos:"http" === 0}
				href="{$subBanners[0].target_url}" target="_blank"
			{else}
				href="{$http_location}{$subBanners[0].target_url}"
			{/if}
			onClick="_gaq.push(['_trackEvent', 'banner', window.location.toString(), '{$subBanners[0].target_url}']);return true;">
			<img class="lazy" data-lazy-src="{myntraimage key="static_banner" src=$subBanners[0].image_url}" alt="{$subBanners[0].alt_text}" src="{$cdn_base}/skin2/images/spacer.gif"/>
		</a>
		<a class="mk-home-feature" 
			{if $subBanners[1].target_url|strpos:"http" === 0}
				href="{$subBanners[1].target_url}" target="_blank"
			{else}
				href="{$http_location}{$subBanners[1].target_url}"
			{/if}
			onClick="_gaq.push(['_trackEvent', 'banner', window.location.toString(), '{$subBanners[1].target_url}']);return true;">
			<img class="lazy" data-lazy-src="{myntraimage key="static_banner" src=$subBanners[1].image_url}" alt="{$subBanners[1].alt_text}" src="{$cdn_base}/skin2/images/spacer.gif"/>
		</a>
	</div>
	<div class='col'>
		<a class="mk-home-feature-sale" 
			{if $saleBanners[0].target_url|strpos:"http" === 0}
				href="{$saleBanners[0].target_url}" target="_blank"
			{else}
				href="{$http_location}{$saleBanners[0].target_url}"
			{/if}
			onClick="_gaq.push(['_trackEvent', 'banner', window.location.toString(), '{$saleBanners[0].target_url}']);return true;">
			<img class="lazy" data-lazy-src="{myntraimage key="static_banner" src=$saleBanners[0].image_url}" alt="{$saleBanners[0].alt_text}" src="{$cdn_base}/skin2/images/spacer.gif"/>
		</a>
	</div>
	<div class='col'>
		<a class="mk-home-feature" 
			{if $subBanners[2].target_url|strpos:"http" === 0}
				href="{$subBanners[2].target_url}" target="_blank"
			{else}
				href="{$http_location}{$subBanners[2].target_url}"
			{/if}
			onClick="_gaq.push(['_trackEvent', 'banner', window.location.toString(), '{$subBanners[2].target_url}']);return true;">
			<img class="lazy" data-lazy-src="{myntraimage key="static_banner" src=$subBanners[2].image_url}" alt="{$subBanners[2].alt_text}" src="{$cdn_base}/skin2/images/spacer.gif"/>
		</a>
		<a class="mk-home-feature" 
			{if $subBanners[3].target_url|strpos:"http" === 0}
				href="{$subBanners[3].target_url}" target="_blank"
			{else}
				href="{$http_location}{$subBanners[3].target_url}"
			{/if}
			onClick="_gaq.push(['_trackEvent', 'banner', window.location.toString(), '{$subBanners[3].target_url}']);return true;">
			<img class="lazy" data-lazy-src="{myntraimage key="static_banner" src=$subBanners[3].image_url}" alt="{$subBanners[3].alt_text}" src="{$cdn_base}/skin2/images/spacer.gif"/>
		</a>
	</div>
</div> <!-- End .home-features -->

<div class="mk-home-features">
	{foreach name=staticBannerImage key=key item=staticBannerImage from=$staticBanners }
		<a class="mk-home-feature {if $smarty.foreach.staticBannerImage.index mod 3 == 1}middle{/if}" 
			{if $staticBannerImage.target_url|strpos:"http" === 0}
				href="{$staticBannerImage.target_url}" target="_blank"
			{else}
				href="{$http_location}{$staticBannerImage.target_url}"
			{/if}
			onClick="_gaq.push(['_trackEvent', 'banner', window.location.toString(), '{$staticBannerImage.target_url}']);return true;">
			<img class="lazy" data-lazy-src="{myntraimage key="static_banner" src=$staticBannerImage.image_url}" alt="{$staticBannerImage.alt_text}" src="{$cdn_base}/skin2/images/spacer.gif"/>
		</a>
	{/foreach}
</div> 
</div>
<!-- End .home-features -->
