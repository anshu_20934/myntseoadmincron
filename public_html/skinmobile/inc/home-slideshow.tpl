<div class="mk-border mk-slideshow-wrapper flexslider  {if $isTablet}tablet{/if}">
        <ul class="mk-slideshow slides">
                {foreach name=slideShowImage key=key item=slideShowImage
                from=$slideShowBanners }
                <li><a href="{$http_loc}{$slideShowImage.target_url}"
                        onClick="_gaq.push(['_trackEvent', 'slide_show', '{$slideShowImage.target_url}', 'click']);return true;"><img
                                src="{if $smarty.foreach.slideShowImage.index < 2}{myntraimage key="
                                mobile_slideshow" src=$slideShowImage.image_url}{else}{$cdn_base}/skin2/images/spacer.gif{/if}
                                " alt="{$slideShowImage.alt_text}"
                                {if $smarty.foreach.slideShowImage.index> 1}class="lazy"
                                data-lazy-src="{$slideShowImage.image_url}"{/if}/></a></li>
                {/foreach}
        </ul>
</div>

