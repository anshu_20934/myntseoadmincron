{assign var="referermail" value=$referer|replace:' ':''}
<div class="main-login-page mod">
	<div class="hd"><span class="heading">REGISTER</span><div class="close-wrapper"><span class="btn-close"></span></div></div>
	        <div class="main-content login-content-slider">
            {if $referermail}
            <div class="referermail">
                Congratulations!<br /> You have been referred by
                <div class="mail">{$referermail}</div>
                To be a part of Mynt Club.
            </div>
            {else} {/if}
            <div class="login-panel">
                {if $referermail}
                <div class="h2">Mynt Club - Myntra Rewards & Loyalty Program</div>
                <div class="h4">Welcome to India's largest Online Fashion Store</div>
                {/if}
                <div class="login-wrapper">
                    <p class="context-msg-area">LOGIN TO VIEW YOUR BAG, ORDERS AND
                        WISHLIST</p>
                    <div class="connect-with-facebook">
                        <button type="button" class="btn normal-btn btn-fb-connect"
                            data-referer="{$mrp_referer}">
                            <span class="ico-fb-small"></span> LOGIN VIA FACEBOOK
                        </button>
                        <div class="err err-fb"></div>
                        <p class="login-using-email">OR</p>
                    </div>
                    <form method="post" class="frm-login"
                        action="{if $secureLoginEnabled}{$https_location}{else}{$http_location}{/if}/mklogin.php">
                        <input type="email" name="email" value=""
                            placeholder="Email Address" />
                        <div class="err err-user"></div>
                        <input type="password" name="password" value="" autocomplete="off" placeholder="Myntra.com password" />
                        <div class="err err-pass"></div>
                        <button type="submit" class="btn btn-signin btn-orange">
                            LOGIN<span class="ico-login-small"></span>
                        </button>
                        <p>
                            <a class="btn-forgot-pass" id="forgot-password" href="#forgot-password">Forgot Password?</a>
                        </p>
                        <hr class="divider">
                        <p class="text-grey1 pad-top15">NEW TO MYNTRA?</></p>
                        <button type="button" class="btn btn-register-now btn-grey">REGISTER
                            NOW</button>
                        <p class="login-note-text">
                    {$signupAmtMsgMobile}    
                    {*{if $mrp_fbCouponValue eq $mrp_nonfbCouponValue}
                            
                    {else}
                            Sign up with Facebook and get coupons worth <span class="rupees discount">{$rupeesymbol}{$signupAmtFB}</span>
                            <br> or Sign up via email and get coupons worth <span class="rupees discount">{$rupeesymbol}{$signupAmtMsg}</span>.
                    {/if}*}
                        </p>
                        <input type="hidden" name="_token" value="{$USER_TOKEN}" /> <input
                            type="hidden" name="mode" value="signin" /> <input type="hidden"
                            name="menu_usertype" value="C" />
						<input type="hidden" name="secure_invoked" value="{if isset($is_secure)}1{else}0{/if}" />
                    </form>
                </div>
            </div>
            <div class="register-panel">
                {if $referermail}
                <div class="h2">Mynt Club - Myntra Rewards & Loyalty Program</div>
                <div class="h4">Welcome to India's largest Online Fashion Store</div>
                {/if}
                <div class="reg-wrapper">
                    <p class="login-note-text">
                    {$signupAmtMsgMobile}
                    {*{if $mrp_fbCouponValue eq $mrp_nonfbCouponValue}
                            {$signupAmtMsg}.
                    {else}
                            Sign up with Facebook {$signupAmtMsgFB}
                            <br> {$signupAmtMsg}
                    {/if}*}
                    </p>
                    <div class="connect-with-facebook">
                        <button type="button" class="btn normal-btn btn-fb-connect"
                            data-referer="{$mrp_referer}">
                            <span class="ico-fb-small"></span> REGISTER VIA FACEBOOK
                        </button>
                        <div class="err err-fb"></div>
                        <p class="login-using-email">OR</p>
                    </div>
                    <form method="post" class="frm-signup"
                        action="{if $secureLoginEnabled}{$https_location}{else}{$http_location}{/if}/mklogin.php">
                        <input type="email" name="email" value=""
                            placeholder="Email address" />
                        <div class="err err-user"></div>
                        
                        <input type="password" name="password" value="" 
                            autocomplete="off" placeholder="Enter a password" />
                        <div class="err err-pass"></div>
                        
                        <input type="tel" name="mobile" value=""
                            placeholder="Mobile number" maxlength="10" />
                        <div class="err err-mobile"></div>
                        
                        <div class="gender-section">
                            <label for="menu-gender" class="gender_label">Gender</label>
                            <div class="btn wrapper-change-gender-on" id="gender-selector">
                                <div class="btn-change-gender-on gender-selector-sm"></div>
                                <div class="selected-gender">Male</div>
                            </div>
                            <div class="err err-gender"></div>
                        </div>
                        <button type="submit" class="btn btn-signup btn-orange">
                            REGISTER<span class="ico-login-small"></span>
                        </button>
                        <hr class="divider">
                        <p class="text-grey1">HAVE AN ACCOUNT?</></p>
                        <button type="button" class="btn btn-login-now btn-grey">LOGIN NOW</button>
                        <input type="hidden" name="_token" value="{$USER_TOKEN}" /> <input
                            type="hidden" name="mode" value="signup" /> <input type="hidden"
                            name="menu_usertype" value="C" /> <input type="hidden"
                            name="mrp_referer" id="mrp_referer" value="{$mrp_referer}" />
                        <input type="hidden" name="secure_invoked" value="{if isset($is_secure)}1{else}0{/if}" />
                    </form>
                </div>
            </div>
        </div>
    </div>
<!-- Forgot Password Message -->
<div class="forgot-pass-msg">
    <p class="ico-email">&nbsp;</p>
    <p class="reset"><span class="tick-ico"> &nbsp; &nbsp; &nbsp; </span> An email with a link to reset your password has been sent to :</p>
    <p class="email"></p>
    <p class="gray">Please check your email and click the password reset link</p>
    <button type="button" class="btn btn-forgot-pass-ok" >OK</button>
</div>
<!-- Login background coming from PHP now -->
<input
    type="hidden" name="loginbg" value="{$loginbg}" class="mk-login-bg-path" />

