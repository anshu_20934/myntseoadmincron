{extends file="layout.tpl"}

{block name=body}
<script>
var pageName = '{$landingPage}';
</script>
<div class="mk-one-column mk-landing-page">
	{if $landingPage eq 'men' || $landingPage eq 'women' || $landingPage eq 'kids'}
		<h1>For {$landingPage}</h1>
	{elseif $landingPage eq 'starnstyle' }
		<h1>Star N Style</h1>
    {elseif $landingPage eq 'starnstyle-kalki' }
		<h1>Star N Style - Kalki</h1>
    {elseif $landingPage eq 'stylezone' }
		<h1>Style Zone</h1>
	{else}
		<h1>{$landingPage}</h1>
	{/if}
	<h3>
		{if $landingPage eq 'men'}
			The largest collection of Men's clothing, footwear and accessories
		{elseif $landingPage eq 'women'}
			The largest collection of Women's clothing, footwear and accessories
		{elseif $landingPage eq 'kids'}
			The largest collection of Kids' clothing, footwear and accessories
	    {elseif $landingPage eq 'stylezone' }
			INSPIRATION&nbsp;&nbsp;&#183;&nbsp;&nbsp;TRENDS&nbsp;&nbsp;&#183;&nbsp;&nbsp;CREATE & SHARE LOOKS&nbsp;&nbsp;&#183;&nbsp;&nbsp;STYLE TIPS&nbsp;&nbsp;&#183;&nbsp;&nbsp;CELEBRITIES 
		{/if}
	</h3>	
	<section class="mk-site-main">
        {include file="inc/home-slideshow.tpl"}
        {include file="inc/home-content.tpl"}
	</section>
</div>
<div class="mk-clear"></div>
{/block}

