{extends file="layout.tpl"} {block name=body}
<div class="home-page-wrapper">
{if $isTablet}
	<div class="nav-links-wrapper">
    <ul class="nav-links">
        {foreach key=indexA item=itemsA from=$topNavigationWidget name=topNavigationWidgetLoop}
            <li><a href="{$itemsA.link_url}?nav_id={$itemsA.id}">{$itemsA.link_name}</a></li>
        {/foreach}
        <!-- <li><a href="{$http_location}/men">Men</a></li>
        <li><a href="{$http_location}/women">Women</a></li>
        <li><a href="{$http_location}/kids">Kids</a></li>
        <li><a href="{$http_location}/home-decor">Home & Decor</a></li>
        <li><a href="{$http_location}/sale">Sale</a></li> -->
    </ul>
    </div>
{/if}

{include file="inc/home-slideshow.tpl"}
<script>
Myntra.Data.pageName='home';
</script>
<div class= "mk-clear"></div>

{if $isTablet}
    <div class="mk-recent-viewed mk-carousel hide"></div>
{/if}
{if $isTablet}
    {include file="inc/home-content-new.tpl"}
{else}
    {include file="inc/home-content.tpl"}
{/if}

<div class="mk-clear"></div>
    {include file="inc/m-ads.tpl"}
    <div class="mk-clear"></div>
<div class="m-home-menu m-border">
    <ul class="mk-nav-levels mk-home-nav-links">
        <li class="mk-nav-levels-first">{$pn =
            $smarty.server.REQUEST_URI|regex_replace:"/\?.*$/":""} {if $login}

            <p class="mk-welcome welcome-logout">
                Welcome, {if $userfirstname neq
                ""}{$userfirstname|truncate:15}{else}{$login|truncate:15}{/if} <br>
                <span class="mk-sign-out nav-logout-link">Logout</span>
            </p>
            <form class="header-logout-form" method="post"
                action="{$http_location}/include/login.php">
                <input type="hidden" name="mode" value="logout"> <input
                    type="hidden" name="_token" value="{$USER_TOKEN}">
            </form>
 {elseif $pn eq "/register.php"}
            <p class="link-btn right">&nbsp</p> {else}
            <p class="small-btn btn-login right">
                Login / Register
            </p> <input type="hidden" id="loginSplashVariant"
            value="{$loginSplashVariant}" /> {/if}</li>
        <li class="mk-nav-levels-first checklogin"><a
            href="{$http_location}/mymyntra.php?view=myorders#top">Track Order</a></li>
        <li class="mk-nav-levels-first checklogin"><a
            href="{$http_location}/mkmycart.php#">My Bag {if $cartTotalQuantity > 0}<span
                class="m-menu-notification">{$cartTotalQuantity}</span> <span
                class="m-menu-notification-cart-total mk-f-right"><span class="rupee">Rs. </span>
                    {$totalAmountCart|round|number_format:0:".":","}</span>{/if} </a></li>
        <li class="mk-nav-levels-first checklogin"><a
            href="{$http_location}/mkmycart.php#wishlist">My Wishlist {if $login}
                <span class="m-menu-notification">{$savedItemCount}</span> {/if}</a>
        </li>
        <li class="mk-nav-levels-first checklogin"><a
            href="{$http_location}/mymyntra.php#">My Myntra</a></li>
        <li class="mk-nav-levels-first"><a
            href="{$http_location}/recently-viewed">Recently Viewed <span
                class="m-menu-notification">{$recentViewedCount}</span> </a></li>
        <li class="mk-nav-levels-first"><a href="{$http_location}/faqs">Customer
                Service</a></li>
    </ul>
</div>
{if !$isTablet}
    <div class="mk-recent-viewed mk-carousel hide"></div>
{/if}
</div>
{/block}
{block name=macros}
    {include file="macros/base-macros.tpl"}
{/block}
