{extends file="layout.tpl"}


{block name=navigation}
    
{/block}

{block name=body}

<script>
Myntra.Data.pageName="pdp";
var avail_dynamic_param='{$avail_dynamic_param}';
var curr_style_id = "{$productStyleId}";
var allProductOptionDetails={$allProductOptionDetails};
Myntra.Data.PDPsocialExclude={if $socialExclude}true{else}false{/if};
Myntra.Data.socialAction='wishlist';
Myntra.Data.PDPImagesCount={$area_icons|@count};
Myntra.PDP =  Myntra.PDP || {};
var vatEnabled = {if $vatEnabled}true{else}false{/if};
var vatCouponMessage = "";
var vatThreshold = "";
var showVatMessage = {if $showVatMessage}true{else}false{/if};
if (vatEnabled){

    vatCouponMessage = "{$vatCouponMessage}";
    vatThreshold = "{$vatThreshold}";
}
var cilckForOfferMobile = "{$cilckForOfferMobile}";
Myntra.PDP.Data = {
        id : "{$productStyleId}",
        vendorId : "{$style_properties.article_number}",
        name : "{$productStyleLabel}",
        brand : "{$style_properties.global_attr_brand}",
        gender : "{$gender}",
        articleType : "{$catalogData.articletype}",
        subCategory : "{$catalogData.sub_category}",
        category : "{$catalogData.master_category}",
        image : "{$style_properties.default_image}",
        searchImage : "{$style_properties.search_image}",
        price : "{$originalprice}",
        discount : "{$discountAmount}",
        cleanURL : "{$pageCleanURL}",
        inStock : "{$inStock}",
        isStyleDisabled: "{$disableProductStyle}"
};

//google conversion analytics track events
var gaEventCategory = Myntra.PDP.Data.category ? Myntra.PDP.Data.category : '',
    gaEventArticleType = Myntra.PDP.Data.articleType ? Myntra.PDP.Data.articleType : '',
    gaEventGender = Myntra.PDP.Data.gender ? Myntra.PDP.Data.gender : '',
    gaEventBrand = Myntra.PDP.Data.brand ? Myntra.PDP.Data.brand : '';

</script>

<div class="mk-centered mk-product-details mk-product-page">
    <!-- Start. product-heading -->
    {include file="pdp/pdp-heading.tpl"}
    <!-- End. product-heading -->
    <!-- Start. product-media -->
    {include file="pdp/pdp-slideshow.tpl"}
    <!-- End .product-media -->
    <!-- Start. Price -->
    <div class="mk-pdp-price" {if !$discountlabeltext}style="padding-bottom:1px;"{/if}>
        <span class="red">
            Rs. {if $discountamount > 0}
                {math equation="x - d" x=$originalprice d=$discountamount assign="finalamount"}{$finalamount|round|number_format:0:".":","} <span class="mrp">{$originalprice|round|number_format:0:".":","}</span>
            {else}
                {$originalprice|round|number_format:0:".":","}
            {/if}
        </span>
    </div>
    {if !$disableProductStyle}
    <!-- DISCOUNT SECTION HERE -->
    {if $discountlabeltext} 
    	{if $show_combo}
    		<div class="clearfix combo-pdp-block" style="padding-bottom:1px;">
        {/if}
        <div class="discount-message-pdp" {if !$show_combo}style="padding-bottom:1px;padding-top:1px;"{/if}>
            {if $show_combo}
            <div class="discount-message para">
                This item is eligible for a combo offer:
            </div>
            <div class="discount-message">
            	<span class=red>{include file="string:{$discounttooltiptext}"}</span>
            </div>
            <div class="discount-message para">To avail this offer, please complete
                this purchase from your computer.</div>
            {else}
               <p class="mk-discount" style="ltext-align:center;padding-bottom: 15px;color: #d14747" >
                  {if $discountamount > 0}
                      {include file="string:{$discountlabeltext}"} 
                  {/if}
               </p>
            {/if}
        </div>
        {if $show_combo}
    		</div>
    	{/if} 
    {/if}
    <!-- END DISCOUNT HERE-->
    <div class="mk-coupon-info">
            {if $cilckForOfferMobile=='control' && $PDPCouponWidgetMobileEnable}
            <div>
                <span class='coupon-pdp-icon'></span>
                <span style="color:#4490A5">{$PDPCouponWidgetTitleTextMobile}</span>
            </div>
            {/if}
    </div>
    {if $showVatMessage}<div class="mk-pdp-vat-message">{$vatMessage}</div> {/if}
    <!--Start. Size Buttons-->
    {if $sizeOptionSKUMapping}
    <div class="mk-size-buttons">
        <div class="mk-custom-drop-down mk-size-drop mk-size-drop-pdp">
            {if $allSizeOptionSKUMapping|count == 1}
                
                {foreach from=$allSizeOptionSKUMapping key=key item=val}
                {assign var=sku value=$key}
                    <div class="mk-freesize btn-onesize" data-count="{$val[2]}">{$val[0]}</div>
                    {if $val[2] <= 3}<div class="mk-count-message">Only <span>{$val[2]}</span> left in stock</div>{/if}
                {/foreach}
                {if $dynamicSizeChart eq 'true' && $sizeRepresentationImageUrl neq ''}
                    <a id="sizechart-new" class="link-btn">WHAT SIZE TO BUY?</a>
                {elseif $size_chart_image_path neq ''}
                    <a id="sizechart-old"  class="link-btn" data-src="{myntraimage key="size_chart" src=$size_chart_image_path}">Size Chart</a>
                {/if}

                <!-- paroksh  jealous fit info-->
                {*if $specificAttributeFitInfo[0]}
                    <button id="jealous-fit-guide" class="link-btn">{$brandname} HIP FIT GUIDE</button>
                {/if*}
                <!-- end paroksh -->

                {if $productDisclaimerTitle|trim neq ""}
                <div class="mk-product-disclaimer-container" {if $sizeRepresentationImageUrl eq '' && $size_chart_image_path eq ''}style="margin-top:15px"{/if}>
                <div class="mk-product-disclaimer">
                    {$productDisclaimerTitle} {if $productDisclaimerText|trim neq ""}<a href="javascript:void(0)" class="mk-product-disclaimer-link link">Know More</a>{/if}
                </div>
                </div>
                {/if}
            {else}
                {if $pdpColorGrouping && $colourSelectVariant == 'test' && $relatedColorStyleDetailsArray}
                <div class="mk-product-option-cont">
                    <div class="mk-colour">
                        <div class="lbl">{$relatedColorStyleDetailsArray|count} More Colour{if $relatedColorStyleDetailsArray|count gt 1}s{/if}:</div>
                        <ul class="mk-cf">
                            {foreach from=$relatedColorStyleDetailsArray key=key item=val}
                                <li>
                                    <a href="{$http_location}/{$val.url}{if $nav_id}?nav_id={$nav_id}{/if}" data-id="{$key}">
                                        {assign "title" "{$val.base_color}{if $val.color1 neq 'NA' && $val.color1 neq ''} / {$val.color1}{/if}"}
                                        <img src="{myntraimage key='style_48_64' src=$val.image|replace:'_images_180_240':'_images_48_64'|replace:'/style_search_image/':'/properties/'}" title="{$title}" alt="{$title}" />
                                    </a>
                                </li>
                            {/foreach}
                        </ul>
                    </div>
                </div>    
                {/if}            
                <div class="flat-size-options">
                    <div class="options">
                        <input type="hidden" name="mk-size" class="mk-size" value="" />
                        {if $preselectSKU}
                            <input type="hidden" name="preselectSKU" id="preselectSKU" value="{$preselectSKU}" />
                        {/if}    
                        {$j=1}
                        
                        {foreach $allSizeOptionSKUMapping as $key => $val}
                            
                            {if $val[1]}
                            <button class="btn mk-mobile-pdp-size-btn btn-grey size-btn {if $renderTooltip}vtooltip{/if}"
                                data-count="{$val[2]}" 
                                value="{$key}" 
                                onclick="sizeSelect({$key},{$j},this)"
                                {foreach $val[3] as $key1 => $val1}
                                    data-{$key1}="{$val1}" 
                                {/foreach}
                                >{$val[0]}</button>
                            {else}    
                            <button class="btn size-btn mk-mobile-pdp-size-btn unavailable {if $renderTooltip}vtooltip{/if}" 
                                title="Sold out - tap to request size" 
                                data-count="{$val[2]}" 
                                value="{$key}" 
                                onclick="sizeSelect({$key},{$j},this)"
                                {foreach $val[3] as $key1 => $val1}
                                    data-{$key1}="{$val1}" 
                                {/foreach}
                                >{$val[0]}
                                <span class="strike"></span></button>
                            {/if}
                            {$j=$j+1}
                        {/foreach}
                    </div>
                </div>
                <div class="mk-size-guides">
                {if $dynamicSizeChart eq 'true' && $sizeRepresentationImageUrl neq ''}
                    <a id="sizechart-new" class="link-btn">WHAT SIZE TO BUY?</a>
                {elseif $size_chart_image_path neq ''}
                    <a id="sizechart-old"  class="link-btn" data-src="{myntraimage key="size_chart" src=$size_chart_image_path}">Size Chart</a>
                {/if}
                </div>
                <!-- paroksh  jealous fit info-->
                {*if $specificAttributeFitInfo[0]}
                    <button id="jealous-fit-guide" class="link-btn">{$brandname} HIP FIT GUIDE</button>
                {/if*}
                <!-- end paroksh -->
                                            
                <div class="mk-hide mk-count-message">Only <span>0</span> left in stock</div>
                {if $productDisclaimerTitle|trim neq ""}
                <div class="mk-product-disclaimer-container">
                <div class="mk-product-disclaimer">
                    {$productDisclaimerTitle} {if $productDisclaimerText|trim neq ""}<a href="javascript:void(0)" class="mk-product-disclaimer-link link">Know More</a>{/if}
                </div>
                </div>
                {/if}
            {/if}
        </div>    
    </div> 
    <!--End. Size Buttons-->
    <!--Start. Buy Button-->
    <div class="mk-buy-div">
        <!-- FORM HERE -->
        <form action="/mkretrievedataforcart.php?pagetype=productdetail" id="hiddenForm" method="post" class="add-to-cart-form" >
            <input type="hidden" name="_token" value="{$USER_TOKEN}" />
            <input type="hidden" id="count" value="{$i}">
            <input type="hidden" name="productStyleId" id="productStyleId" value="{$productStyleId}">
            <input type="hidden" name="uploadedImagePath" id="imagepath" value="">
            <input type="hidden" name="unitPrice" value="{if $discountprice}{$discountprice}{else}{$originalprice}{/if}">
            <input type="hidden" name="pagetype" value="productdetail">
            <input type="hidden" name="totalOptions" id="totalOptions" value="{$Options}">
            <input type="hidden" name="productName" id="productName" value="{$designer[6]}">
            <input type="hidden" name="productTypeName" id="productTypeName" value="{$productTypeLabel}">
            <input type="hidden" name="productStyleLabel" value="{$productDisplayName}">
            <input type="hidden" name="productTypeId" id="productTypeId" value="{$productTypeId}">
            <input type="hidden" name="gender" value="{$gender}">
            <input type="hidden" name="brand" value="{$brandname}">
            <input type="hidden" name="articleType" value="{$articleType}">

            <input type="hidden" name="referrer" value="productDetail">
            <input type="hidden" name="referredTo"  id= "referredTo" value="">
            <input type="hidden" name="quantityMenu" id="quantityMenu" size="3" maxlength="4" value="1">
            <input type="hidden" name="productSKUID" id="productSKUID" value="{$sku}" >
            <input type="hidden" name="selectedsize" value="">
            <input type="hidden" name="quantity"  id="quantity" value="1">
            {**AVAIL*}
            <input type="hidden" name="uqs"  id="uqs" value="{$uqs}">
            <input type="hidden" name="uq"  id="uq" value="{$uq}">
            <input type="hidden" name="t_code"  id="t_code" value="{$t_code}">
            {**END AVAIL*}
            {if $productOptions}
                {section name=options loop=$productOptions}
                <input type="hidden" name="option{$smarty.section.options.index}name"  id="option{$smarty.section.options.index}name"  value="{$productOptions[options][0]}">
                <input type="hidden" name="option{$smarty.section.options.index}value" id="option{$smarty.section.options.index}value" value="">
                <input type="hidden" name="option{$smarty.section.options.index}id" id="option{$smarty.section.options.index}id"  value="" >
                {/section}
            {/if}

            {assign var='i' value=1}
            {if $sizeOptions}
                {foreach from=$sizeOptions key=key item=val}
                    <input type="hidden" name="sizequantity[]" id="sizequantity{$i}" class="sizequantity">
                    <input type="hidden" name="sizename[]" id="sizename{$i}" value="{$key}">
                    {assign var='i' value=$i+1}
                {/foreach}
            {else}
                <input type="hidden" name="sizequantity[]"  class="sizequantity">
                <input type="hidden" name="sizename[]" id="sizename1" value="NA">
            {/if}
        </form>

       <form action="mksaveditem.php?actionType=add"  method="post" class="save-for-later-form" >


       </form>
        <!-- END FORM -->
    
        <button class="m-buynow-button mk-add-to-cart btn primary-btn btn-orange">Buy Now <span class="ico-login-small"></span></button> 
    </div>
    <!--End. Buy Button-->
    {if $SoldByVendorText}

        <div id="vendorDetails" class="mk-buy-div" style="text-align: left">
            <div><span>Sold by</span><span>{$SoldByVendorText}</span></div>
            {if $ShowSuppliedByPartner}
            <div><span>Supplied by</span><span>Partner</span></div>
            {/if}
        </div>
    {/if}
    <!--Start. Add to wishlish-->
    <ul class="other-options">
    {if $expressBuyEnabled}
    <li class="add-button-group mk-express-buy">
        <button class="btn-transparent">
        <span class="express-buy-icon"></span>
        <span class="orange-text">Express buy</span>
        <span class="grey-text"> / Checkout with this item instantly</span>
        </button>
        
        
    </li>
    {/if}
    {if $showtelesalesNumber && !$buynowdisabled && $telesalesNumber|trim}
    <li class="mk-telesales">
        <a href="tel:{$telesalesNumber|replace:'-':''}">
        <span class="telesales-icon"></span>
        <span class="orange-text">Call to order</span> 
        <span class="grey-text"> / {$telesalesNumber}</span>					            
        </a>
    </li>
    {/if}
    <li class="m-wishlist-button mk-save-for-later mk-add-to-wishlist add-button-group">
        <button class="btn-transparent">
        <span class="wishlist-icon"></span>
        <span class="orange-text">Add to wishlist</span>
        <span class="grey-text"> / Save for later</span>
        </button>

    </li>
    
    
    </ul>
        <div id="notify-cont" class="mk-hide notify-cont">
            <p>This size is currently out of stock <br />Do you want to be notified when it is available?</p>
            <form class="notify-me-form" id="notify-pdp">
                <input type="text" name="notifymeEmail" {if $login}value="{$login}"{else}placeholder="Your email address"{/if} class="notify-email {if !$login}empty{/if}" />
                <input type="hidden" name="sku" value="" class="notify-sku" />
                <input type="hidden" name="pageName" value="pdp">
                <input type="hidden" name="_token" value="{$USER_TOKEN}">
                <br/>
                <div class="notify-msg"></div>
                <br/>
                <div class="notify-btn-grp">
                     <button class="notify-button btn normal-btn small-btn">Notify Me</button> <br />
                     <a href="#" class="notify-cancel" onClick="_gaq.push(['_trackEvent', 'pdp', 'notifyme', 'cancel']);return true;">CANCEL</a>
                </div>
            </form>
        </div>
    <!--End. Add to wishlish-->
    {else}
        {if $productDisclaimerTitle|trim neq ""}
            <div class="mk-product-disclaimer">
                {$productDisclaimerTitle} {if $productDisclaimerText|trim neq ""}<a href="javascript:void(0)" class="mk-product-disclaimer-link link">Know More</a>{/if}
            </div>
        {/if}
        <div class="mk-sold-out">This product is currently sold out</div>
    {/if}

        {else}
            {if $productDisclaimerTitle|trim neq ""}
             <div class="mk-product-disclaimer-container">
                 <div class="mk-product-disclaimer">
                     {$productDisclaimerTitle} {if $productDisclaimerText|trim neq ""}<a href="javascript:void(0)" class="mk-product-disclaimer-link link">Know More</a>{/if}
                 </div>
             </div>
             {/if}
             <div class="mk-sold-out">This product is currently sold out</div>
        {/if}

    <!--Start. Accordian for product desc-->
    <div class="m-product-desc ">
         {if $style_note|trim || $descriptionAndDetails|trim || $materials_care_desc|trim || $size_fit_desc|trim}
            <div class="accordion">
                 {if $style_note|trim neq ""}
                 <h2 >Style Note</h2>
                 <span class="accordion-content">
                 {$style_note}
                 </span>
                 {assign var='fl' value=1}
                 {/if}
                 {if $descriptionAndDetails|trim neq ""}
                 <h2>Product Details</h2>
                 <span class="accordion-content">
                 {$descriptionAndDetails}
                 </span>
                 {assign var=fl value=1}
                 {/if}
                 {if $materials_care_desc|trim neq ""}
                 <h2>Material & Care</h2>
                 <span class="accordion-content">
                 {$materials_care_desc}
                 </span>
                 {assign var=fl value=1}
                 {/if}
                 {if $size_fit_desc|trim neq ""}
                 <h2>Size & Fit</h2>
                 <span class="accordion-content">
                 {$size_fit_desc}
                 </span>
                 {/if}    
             </div>     
        {/if}
            <!-- Start. Pincode Servicability Widget-->
            <div class="delivery-network">
                <div class="delivery-network-header">Delivery Time & Cash on Delivery</div>
                <div class="changeable ul">
                    CHECK YOUR PIN CODE
                </div>
            </div>
            <!-- End. Pincode Servicability Widget-->
        </div>
    <!--End. Accordian for product desc-->

    <div class="product-share-info">
    <h5 class="pdp-product-code grey">Product Code: {$productStyleId}</h5> <!-- Product Code -->
    <div class="mk-share-links">
    {if $pdpSocial}
        <input type="hidden" name="pageURL" id="pageURL" value="{$pageCleanURL}"></input>
        <div class="gm"><a href="https://mail.google.com/mail/?view=cm&ui=2&fs=1&tf=1"></a></div>
        <div class="fb"></div>
        <div class="tw"></div>
        <div class="gp"></div>
        <div class="pt"></div>
    {/if}
    </div>
    </div>

    <div class="mk-clear"></div>

    <!--Start. Carousel for Recommended products -->
    <div class="m-product-reco mk-carousel hide"></div>
    <!--End.  Carousel for Recommended products -->
    <div class="mk-clear"></div>

    <!--Start. Menu-->
    <div class="m-pdp-menu">
    {if $agegroup|lower eq 'adults-unisex'}
        {assign var=genderLinkinParam value="Men:Women"}
    {elseif $agegroup|lower eq 'kids-unisex'}
        {assign var=genderLinkinParam value="Boys:Girls"}
    {else}
        {assign var=genderLinkinParam value=$gender}
    {/if}
    <ul class="bread-crumb-bottom mk-f-right">
        <li><a href="{$http_location}/{$articleType|lower|replace:' ':'-'}#!gender={$genderLinkinParam}|brands={$brandname}" onClick="_gaq.push(['_trackEvent', 'pdp', 'breadcrum', '{$articleType}']);return true;">More {$articleType} FROM {$brandname}<span class="mk-pdp-menu-icon"></span></a></li>
        <li class="slash"> / </li>
        <li><a href="{$http_location}/{$brandname|lower|replace:' ':'-'}#!gender={$genderLinkinParam}" onClick="_gaq.push(['_trackEvent', 'pdp', 'breadcrum', '{$brandname}']);return true;" class="mk-last">All Products from {$brandname}<span class="mk-pdp-menu-icon"></span></a></li>
    </ul>

    </div>
    <!--End. Menu-->
    <div class="mk-clear"></div>
</div>
{/block}

{block name="lightboxes"}
{if $productDisclaimerText|trim neq ""}
<div id="product-disclaimer-info" class="lightbox infobox">
	<div class="mod">
		<div class="hd">
			<div class="title">{$articleType}</div>
		</div>
		<div class="bd body-text">
			<p>{$productDisclaimerText}</p>
		</div>
		<div class="ft">
		</div>
	</div>
</div>
{/if}

{/block}


{block name=pagejs}
    
{/block}
{block name=pdppageRightmenu}
	<div class="content-slide-wrapper pincodewidget-wrapper content-inactive">
    </div>
    <div class="content-slide-wrapper  couponinfowidget-wrapper content-inactive">
    </div>
{/block} 
{block name=macros}
    {include file="macros/pdp-macros.tpl"}
{/block}

