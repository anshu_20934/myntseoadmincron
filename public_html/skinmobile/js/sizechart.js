/******SIZE CHART *********/
Myntra.SizeChartBox = function(id, cfg) {
	var cfg = {width:'95',isWidthAspect:false};
    var obj = Myntra.RightMenu($('#lb-sizechart'),cfg);
    var infoOpen,
    infoClose,
    infoBlock,
    sizeScroll,
    sizeSelLi,
    sizeSelBlock,    
    addToBtn,
    passObj,
    close,
    formToSubmit,
    sizeOutOfStock,
    sizeImgCont,
    equiSizeUL,
    equiSizeLI,
    loaded=false,
    mLinImg,
    countLi=0,
    noToScroll=4,
    initScroll=true,
    sizetoselect='',
    sizetoselectVal ='',
    inOfSel=0;
    $('.close-wrapper').fastClick(function(){
    	obj.hide();
    });

    var addtocart=function(e,selSizeName){
    //    alert('add');
        var quantity = formToSubmit.find(":hidden[name='quantity']").val();
        var sku_=selSizeName;
        
        size=sizeSelBlock.find('li.size-val-sel').text();
//        /alert(sku_+size);return false;
        //var size= sizeDropCont.find('.mk-size option:selected').text();
        var productStyleId=formToSubmit.find(":hidden[name='productStyleId']").val();
        formToSubmit.find(":hidden[name='sizequantity[]']").val(quantity);
        formToSubmit.find(":hidden[name='productSKUID']").val(sku_);
        formToSubmit.find(":hidden[name='selectedsize']").val(size);

        /*commenting to redirect user to cart */
        //TRIGGER FORM SUBMIT
        formToSubmit.submit();
        //alert(sku_);
        //Create a new lightbox here for order confirmation;
        
        
        $('.mk-size-error').css('display','none');

    };
    
    obj.onBeforeShow = function(){
        $('#tab-list').removeClass('mk-f-left').removeClass('lft-cont');
        $('#measurement-box').removeClass('mk-f-right').removeClass('rgt-cont');
        $('#tab-list').insertAfter('#measurement-box');
        $('#tab-list .out-of-stock').insertBefore('#lb-sizechart #sub-btn .primary-btn');
        //$('#lb-sizechart').css('left',($('.m-body').width()-$('#lb-sizechart').width())/2);
        $(window).resize(function(){
            //$('#lb-sizechart').css('left',($('.m-body').width()-$('#lb-sizechart').width())/2);
        });

    }
    
    obj.onAfterShow = function (){
        if(loaded){
            equiSizeLI.each(function(){
                var sizeSplit=$(this).html();
                //alert(sizeSplit.search('/')>0)
                if(sizeSplit.search('/')>0){
                    sizeSplit=sizeSplit.replace('/','<br />');
                    $(this).html(sizeSplit);
                }
            });
        
            $(equiSizeUL).each(function(){
                var h_=$(this).height();
            //    alert(h_);
                $(this).parent().height(h_);
            });
            inOfSel=sizeSelBlock.find('li.size-val-sel').index();
            if(inOfSel==-1){
                    inOfSel=sizeSelBlock.find('li.size-val-sel-inactive').index();
            }
            
            //inti scroll after load    
            if(sizeScroll.length && $('.size-scroll-btns .rs-carousel-runner > li').size()>5){
                $("#size-select").addClass("slides");
                if($('.size-scroll-equi').length){
                    $('.size-scroll-equi .size-items').addClass("slides");
                    $('.size-scroll-equi').flexslider({
                        animation: "slide",
                        startAt: 0,                     //Integer: The slide that the slider should start on. Array notation (0 = first slide)
                        maxItems: 4,
                        controlNav: false,
                        itemMargin: 0,
                        move: 0,
                        minItems: 1, // use function to pull in initial value
                        itemWidth: 30,
                        slideshow: false,
                        touch: false
                    });
                }
                $('#size-scrollable_1').flexslider({
                    animation: "slide",
                    startAt: 0,                     //Integer: The slide that the slider should start on. Array notation (0 = first slide)
                    maxItems: 4,
                    controlNav: false,
                    itemMargin: 0,
                    move: 0,
                    minItems: 1, // use function to pull in initial value
                    itemWidth: 30,
                    slideshow: false,
                    touch: false,
                    before: function(slider){
                        $('.size-scroll-equi').each(function(){
                            $(this).data('flexslider').flexAnimate(slider.animatingTo);
                        });
                    }
                });
            }
        }else{
            $('.tab-btns  li.default').click();
        }
    }
    
    var p_show = obj.show;
    obj.show = function(sizeObj){
            sizetoselect = sizeObj.text;
            sizetoselectVal = sizeObj.val;
            //alert(sizetoselect+'--'+sizetoselectVal );
            if(loaded){
                obj.sizeUpdate(sizeObj);
            }
            p_show.call(this);
            obj.onAfterShow();
            obj.scrollTo();
    };
    obj.scrollTo = function(){
        if(loaded){
                if(countLi>4){
                //SCROLL PAGE DETERMINE HERE
                    //get the index of item to scroll here
                    var selectedIndex=(sizeSelBlock.find('li.size-val-sel-inactive').length)?sizeSelBlock.find('li.size-val-sel-inactive').index():sizeSelBlock.find('li.size-val-sel').index();
                    sizeScroll.each(function(){
                        //$(this).data('carousel').goToItem(selectedIndex+1);
                    });
                }
            initScroll=false;
        }
    };
        
    //INIT METHOD 
    obj.init = function() {

        infoOpen=$('#info-open'),
        infoClose=$('#info-close'),
        infoBlock=$('#info-block'),
        sizeScroll=$(".size-scrollable"),
        sizeSelLi=$("#size-select li.size-available"),
        sizeOutOfStock=$("#size-select li.size-inactive"),
        sizeSelBlock=$("#size-select"),    
        addToBtn=$('#sub-btn .sub-btn'),
        close=$(id+' .close'),
        equiSizeLI=$('.equi-sizes li'),
        equiSizeUL='.equi-sizes',
        sizeImgCont=$('#measurement-box'),
        mLinImg=$('.size-image'),
        passObj={
            "text":sizeSelBlock.find('li.size-val-sel').text(),
            "flag":"fromPop",
            "val":sizeSelBlock.find('li.size-val-sel').length?sizeSelBlock.find('li.size-val-sel').attr('data-value'):''
        },
        formToSubmit=$('#sizeForm-'+curr_style_id);
        //alert('createMeasureBlocks');
        if(typeof Myntra.Data.ageGroup !='undefined' && typeof sizePositionObj[Myntra.Data.ageGroup.toLowerCase()] !== 'undefined'){
            if(typeof sizePositionObj[Myntra.Data.ageGroup.toLowerCase()][Myntra.Data.articleType.toLowerCase()] !== 'undefined'){
                obj.createMeasureBlocks(sizePositionObj[Myntra.Data.ageGroup.toLowerCase()][Myntra.Data.articleType.toLowerCase()]);
            }
        }
        infoOpen.toggle(
            function(){
                infoOpen.removeClass('inactive').addClass('active');
                infoBlock.slideUp();            
            },
            function(){
                infoOpen.removeClass('active').addClass('inactive');
                infoBlock.slideDown();
        });
        

        sizeSelLi.fastClick(function(){
            _gaq.push(['_trackEvent','sizechart','sizechart', 'sizechart-size-available']);
            var sizeInd=$(this).index();
            var selVal=$(this).text();
            var jsonObj=Myntra.Data.sizeMeasurementsObj[sizeInd];
            obj.switchMeasurements(jsonObj,selVal);
            $(this).parent().find('li').removeClass('size-val-sel size-val-sel-inactive');    
            $(this).addClass('size-val-sel');
            passObj.val=$(this).attr('data-value');
            passObj.text=$(this).text();
            addToBtn.css('cursor','pointer').removeAttr('disabled');
            $('.out-of-stock').removeClass('visible');
        });
        sizeOutOfStock.bind("click",function(){
            _gaq.push(['_trackEvent','sizechart','sizechart', 'sizechart-size-unavailable']);
            var sizeInd=$(this).index();
            var selVal=$(this).text();
            var jsonObj=Myntra.Data.sizeMeasurementsObj[sizeInd];
            obj.switchMeasurements(jsonObj,selVal);
            $(this).parent().find('li').removeClass('size-val-sel size-val-sel-inactive');$(this).addClass('size-val-sel-inactive');
            passObj.val='';
            passObj.text='';
            addToBtn.css('cursor','not-allowed').attr('disabled','disabled');
            $('.out-of-stock').addClass('visible');
        });
        
        addToBtn.addClass('btn-orange-noplay');
        addToBtn.bind('click',function(){
            _gaq.push(['_trackEvent','sizechart','sizechart', 'sizechart-buy-button']);
            //alert(passObj.val);
            if(sizeSelBlock.find('li.size-val-sel').length){
                passObj.val=sizeSelBlock.find('li.size-val-sel').attr('data-value');
            }
            
            if(passObj.val!=''){
                $(document).trigger('myntra.sizechart.addtocart',passObj.val);
            }
            else{
                return;
            }
        });
        //set width of the size li here
        //calculte the width
        
        countLi=sizeSelBlock.find('li').length;
        var liWidth=sizeSelBlock.find('li').width();
        if(countLi<noToScroll){
            var ulWidthToSet=(liWidth*countLi)+(countLi-1);
            //calculte the margin inorder to align to center
            var marginToSet=((250-ulWidthToSet)/2);
            sizeScroll.css({'width':ulWidthToSet+'px','margin-left':marginToSet+'px','margin-right':marginToSet+'px'});
            }
        
        //Update Background of Equivalent sizes
        $(equiSizeUL+':odd').addClass('odd');
        //alert('init end');
        
    };
    
    obj.switchMeasurements = function(jsonObj,selVal){
        for(var key in jsonObj){
        
                if(jsonObj.hasOwnProperty(key)){
                switch(jsonObj[key]['type']){
                    case "flat":
                        if(jsonObj[key]['value']['value']!=null && jsonObj[key]['value']['value']!='' && jsonObj[key]['value']['value']!="null" )
                        {        //show/hide the illustration lines here
                            $('.'+key.replace(/ /g,'-').toLowerCase()+'-dotted').css({'visibility':'visible'});        
                              if($('span.'+key.replace(/ /g,'-').toLowerCase()).length){
                                $('span.'+key.replace(/ /g,'-').toLowerCase()).hide();
                                $('span.'+key.replace(/ /g,'-').toLowerCase()).html('<span >'+key+'</span><br/>'+jsonObj[key]['value']['value']+' '+(jsonObj[key]['value']['unit']=='Inches'?'inches':jsonObj[key]['value']['unit'])).fadeIn(1200);
                            }
                        //Give info incase of footwear
                        if(Myntra.Data.articleTypeId==127 || Myntra.Data.articleTypeId==92 || Myntra.Data.articleTypeId==93 || Myntra.Data.articleTypeId==94 || Myntra.Data.articleTypeId==95 || Myntra.Data.articleType==96)
                         {  
                            var infoText='* Foot size '+jsonObj[key]['value']['value']+' '+(jsonObj[key]['value']['unit']=='Inches'?'inches':jsonObj[key]['value']['unit'])+' fits Indian Size <b>'+selVal+'</b>';
                            if($('.foot-size').length){
                                $('.foot-size').fadeOut(function(){$(this).html(infoText).fadeIn();});
                            }
                            else{
                                $('<span></span>').addClass('foot-size').html(infoText).appendTo(sizeImgCont);
                            }
                         }    
                        }
                        break;
                    case "variable":
                        var variableKey=Myntra.Data.styleSizeAttributes[key];
                        sizeImgCont.find('.'+key.replace(/ /g,'-').toLowerCase()).hide();
                        sizeImgCont.find('.'+key.replace(/ /g,'-').toLowerCase()).html('<span>'+key+'</span><br/>'+jsonObj[key][variableKey]['value']+' '+(jsonObj[key][variableKey]['unit']=='Inches'?'inches':jsonObj[key]['value']['unit'])).fadeIn(1200);
                        break;
                    default:
                        break;
                }
            }
        }
    };
    
    obj.createMeasureBlocks = function(mPosObj){
        var blockHeight=400;
        var index=1;
        for(var key in mPosObj){
            if(mPosObj.hasOwnProperty(key)){
                //console.log('key->',key,'<===> value->',mPosObj[key]);
                $('<span></span>').addClass('measurement '+key.replace(/ /g,'-').toLowerCase()).css({'left':mPosObj[key][0]+'px','top':mPosObj[key][1]+'px'}).appendTo(sizeImgCont);
                //Create Illustration block and position it use sizePositionObj[indexed]
                $('<span></span>').addClass('size-image '+key.replace(/ /g,'-').toLowerCase()+'-dotted').css({'background-position':'0 -'+(blockHeight*index)+'px','visibility':'hidden'}).appendTo(sizeImgCont);
            }
            index=index+1;
        }
        
    };
    
    obj.sizeUpdate = function(sizeObj){
        var selCount=0;
        
        if(sizeObj.text!='' && sizeObj.text!= 'SELECT A SIZE'){
            
            sizeSelLi.removeClass('size-val-sel');
            sizeOutOfStock.removeClass('size-val-sel-inactive');
            $('#size-select li').each(function(){
                if($(this).attr('data-value')==sizeObj.val){
                    if($(this).hasClass('size-available')){
                        $(this).addClass('size-val-sel');
                        addToBtn.css('cursor','pointer').removeAttr('disabled');
                        $('.out-of-stock').removeClass('visible');
                        passObj.val=$(this).attr('data-value');
                    }
                    else{
                        $(this).addClass('size-val-sel-inactive');
                        addToBtn.css('cursor','not-allowed').attr('disabled','disabled');
                        $('.out-of-stock').addClass('visible');
                        passObj.val='';
                        passObj.text='';
                    }
                    var sizeInd=$(this).index();
                    var selVal=$(this).text();
                    var jsonObj=Myntra.Data.sizeMeasurementsObj[sizeInd];
                    obj.switchMeasurements(jsonObj,selVal);
                    
                    
                }
            
            });
        }
        else 
        { 
            if(!sizeSelBlock.find('li.size-val-sel-inactive').length){
                selCount=sizeSelLi.length;
                if(selCount>1){
                    var midVal=Math.ceil(selCount/2);
                    var sizeInd=sizeSelBlock.find('li.size-available:eq('+(midVal-1)+')').index();
                    var selVal=sizeSelBlock.find('li.size-available:eq('+(midVal-1)+')').text();
                    var jsonObj=Myntra.Data.sizeMeasurementsObj[sizeInd];
                    obj.switchMeasurements(jsonObj,selVal);
                    sizeSelLi.removeClass('size-val-sel size-val-sel-inactive');
                    sizeSelBlock.find('li.size-available:eq('+(midVal-1)+')').addClass('size-val-sel');
                }
                else if(selCount==1){
                    var sizeInd=sizeSelLi.index();
                    var selVal=sizeSelLi.text();
                    var jsonObj=Myntra.Data.sizeMeasurementsObj[sizeInd];
                    obj.switchMeasurements(jsonObj,selVal);
                    sizeSelLi.addClass('size-val-sel');
                }
            }
        }
        
    };
    
    //Override beforeHide method
    obj.beforeHide = function(){
        if(sizeSelBlock.find('li.size-val-sel').length){
            passObj.text=sizeSelBlock.find('li.size-val-sel').text();
            passObj.val=sizeSelBlock.find('li.size-val-sel').attr('data-value');
            $(document).trigger('myntra.sizechart.done', passObj);
        }
        else if(sizeSelBlock.find('li.size-val-sel-inactive').length){
            passObj.text=sizeSelBlock.find('li.size-val-sel-inactive').text();
            passObj.val=sizeSelBlock.find('li.size-val-sel-inactive').attr('data-value');
            $(document).trigger('myntra.sizechart.done', passObj);
        }
    };
    
    obj.loadMainImage = function(){
        $('.size-image').css('background-image','url('+Myntra.Data.sizeImg+')');
    };
    
    obj.loadOnPage =function(sizeObj){
        //var sizeChartUrl=http_loc+"/size_scaling.php?styleid="+sizeObj.styleid+"&options="+JSON.stringify(allProductOptionDetails);
        //var sizeChartUrl="http://www.localhost.local/size_scaling.php?styleid=1962&options=[{%22id%22:%226490%22,%22name%22:%22Size%22,%22value%22:%22UK7%22,%22price%22:null,%22default_option%22:%22N%22,%22sku_id%22:%225134%22,%22is_active%22:true,%220%22:%226490%22,%221%22:%22Size%22,%222%22:%22UK7%22,%223%22:null,%224%22:%22N%22,%225%22:%225134%22,%226%22:%221%22},{%22id%22:%226491%22,%22name%22:%22Size%22,%22value%22:%22UK8%22,%22price%22:null,%22default_option%22:%22N%22,%22sku_id%22:%225135%22,%22is_active%22:true,%220%22:%226491%22,%221%22:%22Size%22,%222%22:%22UK8%22,%223%22:null,%224%22:%22N%22,%225%22:%225135%22,%226%22:%221%22},{%22id%22:%226492%22,%22name%22:%22Size%22,%22value%22:%22UK9%22,%22price%22:null,%22default_option%22:%22N%22,%22sku_id%22:%225136%22,%22is_active%22:false,%220%22:%226492%22,%221%22:%22Size%22,%222%22:%22UK9%22,%223%22:null,%224%22:%22N%22,%225%22:%225136%22,%226%22:%221%22},{%22id%22:%226493%22,%22name%22:%22Size%22,%22value%22:%22UK10%22,%22price%22:null,%22default_option%22:%22N%22,%22sku_id%22:%225137%22,%22is_active%22:false,%220%22:%226493%22,%221%22:%22Size%22,%222%22:%22UK10%22,%223%22:null,%224%22:%22N%22,%225%22:%225137%22,%226%22:%221%22}]";    
        $.ajax({
            type:"POST",
            url:"/size_scaling.php",
            data:"isd="+Myntra.PDP.Data.isStyleDisabled+"&styleid="+sizeObj.styleid+"&options="+JSON.stringify(allProductOptionDetails),
            beforeSend:function(){
                //alert('beforesend');
                obj.showLoading();
            },
            success: function(data){
                //alert('success');
                //obj.show();
                if(data)
                {    
                    if(!$('#lb-sizechart .size-scrollable-box').length){
                        $('#lb-sizechart .mod').append(data);
                        obj.init();
                        obj.loadMainImage();
                    }
                }
            },
            complete: function(){
                obj.hideLoading();
                //alert('text='+sizeObj.text+'  val='+sizeObj.val);
                if(sizetoselect!=''){
                    sizeObj.text=sizetoselect;
                }
                if(sizetoselectVal!=''){
                    sizeObj.val=sizetoselectVal;
                }
                obj.sizeUpdate(sizeObj);
                loaded=true;
                obj.onBeforeShow();
                obj.onAfterShow();
                
            }
        });
        
    };

    
    //EVENTS FOR COMMUNICATION WITH BASE PAGE
    //$(document).bind('myntra.sizechart.selectsize',selectSize);
    $(document).bind('myntra.sizechart.addtocart',addtocart);
    return obj;
};
    
Myntra.InitSizeChart = function(styleObj) {
    $('.main-slide-wrapper').append('<div id="lb-sizechart" class="content-slide-wrapper lb-sizechart content-inactive"><div class="mod"><div class="close-wrapper"><span class="btn-close"></span></div></div></div>');
    var obj = Myntra.SizeChartBox("#lb-sizechart");
    //Handle the size from PDP to popuo here
    //FIRE this event while calling the sizechart, Send the selcted size to the popup
    $(document).bind('myntra.sizechart.show', function(e,data) {
        obj.show(data);
    });    
    $(document).bind('myntra.sizechart.load', function(e,data) {
        obj.loadOnPage(data);
    });
    /**
     * Kundan: now we track: which were the styles for which user clicked on their size charts: old or new
     * We maintain the list of unique style ids in cookie.
     * After order is placed sucessfully,this cookie is read, the style-ids in order are matched with these styles
     * and the matched style-ids are then pushed to Mongo DB eventually. 
     */
    $(document).bind('myntra.sizechart.trackclick', function(e,styleId) {
        var currCookieValue = Myntra.Utils.Cookie.get(Myntra.Data.sizechartCookieConstants.NewSizechartCookie);
        if(currCookieValue != null) {
            currCookieValue = decodeURIComponent(currCookieValue);
        }
        var newCookieValue = Myntra.Utils.setCompositeData(styleId, currCookieValue, true, Myntra.Data.sizechartCookieConstants.Delimiter, Myntra.Data.sizechartCookieConstants.MaxStylesPerCookie);
        Myntra.Utils.Cookie.set(Myntra.Data.sizechartCookieConstants.NewSizechartCookie, newCookieValue, Myntra.Data.sizechartCookieConstants.Expiry);
    });
};


Myntra.InitPDP = function() {
var passObj ={};
    $("#sizechart-new").on('click',function(e){
        e.preventDefault();
        $(window).scrollTop(0);
        _gaq.push(['_trackEvent','sizechart','sizechart', 'sizechart-pdp-link']);
        passObj = {
            //"text":$('.mk-product-guide .mk-size').length?$('.mk-size option:selected').text():'',
            //"val":$('.mk-product-guide .mk-size').length?$('.mk-size option:selected').val():'',
            "text":$('.flat-size-options .size-btn.selected').length?$('.flat-size-options .size-btn.selected').text():'',
            "val":$('.flat-size-options .size-btn.selected').length?$('.flat-size-options .size-btn.selected').val():'',
            "styleid":curr_style_id
        };
        $(document).trigger('myntra.sizechart.show', passObj);//Firing the event to show popup also send selcted data along with it.
        if(typeof Myntra.Data.sizechartCookieConstants != "undefined") {
            $(document).trigger('myntra.sizechart.trackclick', curr_style_id);
        }
    });

    passObj = {
            //"text":$('.mk-product-guide .mk-size').length?$('.mk-size option:selected').text():'',
            //"val":$('.mk-product-guide .mk-size').length?$('.mk-size option:selected').val():'',
            "text":$('.flat-size-options .size-btn.selected').length?$('.flat-size-options .size-btn.selected').text():'',
            "val":$('.flat-size-options .size-btn.selected').length?$('.flat-size-options .size-btn.selected').val():'',
            "styleid":curr_style_id
        };
    
    $(document).trigger('myntra.sizechart.load', passObj);//load sizechart behind on page load

    $(document).bind('myntra.sizechart.done', function(e,data) {
        //Handle the size from POPUP to PDP here
        //FIRE this event inside Popup
        var carry=false;
        
        //SET VALUES BY TAKING FROM DROP DOWN based on Value
        
        //var selectedValue=$("#mk-size-pdp option[value='"+data.val+"']").text();
        //$('.mk-size-drop-pdp div span').text(selectedValue);
        //$("#mk-size-pdp").val(data.val).trigger('change');

        // trigger the size-buttons selected state
        $('.flat-size-options .size-btn[value="' + data.val + '"]').trigger('click');
        if(!carry){
            $('#productSKUID').val('');
        }
    });
    
    //OLD SIZE CHART
    $('#sizechart-old').click(function(e){
        e.preventDefault();
        $(window).scrollTop(0);
        $(document).trigger('myntra.oldsizechart.show',$(this).attr('data-src'));
        if(typeof Myntra.Data.sizechartCookieConstants != "undefined") {
            $(document).trigger('myntra.oldsizechart.trackclick',curr_style_id);
        }
    });
    
};

Myntra.OldSizeChartBox = function(id, cfg) {
	var cfg = {width:'95',isWidthAspect:false};
    var objS = Myntra.RightMenu($('#lb-sizechart-old'),cfg);
    var p_show = objS.show;
    objS.show = function(imgUrl){
        $('#lb-sizechart-old .mod .bd').html('<img class="sizechart-old-img" src="'+imgUrl+'">').css('max-width','840px');
        p_show.call(this);
    };
    return objS;
};

Myntra.InitOldSizeChart = function (id,cfg){
    $('.main-slide-wrapper').append('<div id="lb-sizechart-old" class="content-slide-wrapper lb-sizechart-old content-inactive"><div class="mod"><div class="close-wrapper"><span class="btn-close"></span></div><div class="bd"></div></div></div>');
    var oldS = Myntra.OldSizeChartBox("#lb-sizechart-old");
    $(document).bind('myntra.oldsizechart.show', function(e,data) {
        oldS.show(data);
        //oldS.center();
    });
    
    /**
     * Kundan: now we track: which were the styles for which user clicked on their size charts: old or new
     * We maintain the list of unique style ids in cookie.
     * After order is placed sucessfully,this cookie is read, the style-ids in order are matched with these styles
     * and the matched style-ids are then pushed to Mongo DB eventually. 
     */
    $(document).bind('myntra.oldsizechart.trackclick', function(e, styleId) {
        var currCookieValue = Myntra.Utils.Cookie.get(Myntra.Data.sizechartCookieConstants.OldSizechartCookie);
        if(currCookieValue != null) {
            currCookieValue = decodeURIComponent(currCookieValue);
        }
        var newCookieValue = Myntra.Utils.setCompositeData(styleId, currCookieValue, true, Myntra.Data.sizechartCookieConstants.Delimiter, Myntra.Data.sizechartCookieConstants.MaxStylesPerCookie);
        Myntra.Utils.Cookie.set(Myntra.Data.sizechartCookieConstants.OldSizechartCookie, newCookieValue, Myntra.Data.sizechartCookieConstants.Expiry);    
    });
    $('.close-wrapper').fastClick(function(){
    	oldS.hide();
    });
};

$(document).ready(function() {
    //alert('ready');
    Myntra.InitSizeChart();
    Myntra.InitOldSizeChart();
    Myntra.InitPDP();
});
