
$.ui.autocomplete.prototype._renderItem=function(ul,item){
    var c=item.label;
    c=c.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)("+$.ui.autocomplete.escapeRegex(this.term)+")(?![^<>]*>)(?![^&;]+;)","gi"),"<strong>$1</strong>");    
    if(this.options.type == 'brandsbox'){
    	return $("#mk-brands ."+item.label.toLowerCase().replace(/\W+/g, "")+"-brands-filter").show();
    }
    else if(this.options.type == 'interestsbox'){
    	return $("#mk-interests ."+item.label.toLowerCase().replace(/\W+/g, "")+"-interest-filter").show();
    }        
    else if(this.options.type == 'searchbox'){
       $(ul).addClass("searchbox");
       return $( "<li></li>" )
			.data( "item.autocomplete", item )
			.append( $( "<a></a>" ).html("<span class='mk-autocomplete-result'>" + c + "</span>" + "<span>"+item.count+"</span>") )
			.appendTo(ul); 
    }else{
       return $( "<li></li>" )
			.data( "item.autocomplete",item)
			.append( $( "<a></a>" ).text(item.label) )
			.appendTo(ul); 
        
    }
};

$.ui.autocomplete.prototype._renderMenu =  function( ul, items ) {
		var self = this,
			currentCategory = "";
			$.each( items, function( index, item ) {
			if ( item.category != currentCategory && self.options.type == 'searchbox') {
				ul.append( "<li class='ui-autocomplete-category'>" + item.category + " : </li>" );
				currentCategory = item.category;
			}
			self._renderItem( ul, item );
		});
	};



$.ui.autocomplete.prototype._moveMenu =  function( ul ) {
	if($('#localityDiv')[0])
		$(ul).appendTo("#localityDiv");
}