var state = 0;
var borderH2 = $(".accordion h2").first();

borderH2.css("border-top","1px #d5d5d5 solid");
borderH2.css('padding-top',"3px");
(function($) {
    $.fn.accordion = function(opts) {

        if (state == 1)
            return ;
        else
            state=1;
        var d = {
            speed: 200,
            showOpen: true,
            openId: 'toggle-all-open',
            showClose: true,
            closeId: 'toggle-all-closed',
            activeClass: 'ui-state-active'
        },
            p, a, h, t;

        t = $(this);
        h = t.find('h2');
        if( opts )
            $.extend(d, opts );
        
        h.each(function() {
            $(this).click(function(event) {
            		var same = 0;
                    if ($(this).hasClass('ui-state-active'))
                    {
                        same = 1;
                    }
                    $('.ui-state-active').removeClass('ui-state-active');
                    
                    $(".accordion .accordion-content").each(function(index,element){
                    	if($(element).css('display')==='inline')
                    		$(element).css('display','block');
                    });
                    $(".accordion .accordion-content").slideUp();
                    if (same === 0)
                    	$(this).toggleClass(d.activeClass).nextUntil('h2').slideToggle(d.speed);
                    $(".accordion .accordion-content").each(function(index,element){
                    	if($(element).css('display')==='inline')
                    		$(element).css('display','block');
                    });
                    state = 1;
                
                return false;
            });
        });

//        p = $('<p></p>').addClass('toggle').prependTo(t);

        if (d.showOpen) {
            a = $('<a></a>', {
                id: d.openId,
                name: d.closeId,
                href: '#'
            }).text('show all').appendTo(p);
            a.bind('click', function(e) {
                e.preventDefault();
                h.each(function() {
                    $(this).addClass(d.activeClass).nextUntil('h2').slideDown(d.speed);
                });
            });
        }

        if (d.showClose) {
            a = $('<a></a>', {
                id: d.closeId,
                name: d.closeId,
                href: '#'
            }).text('hide all').appendTo(p);
            a.bind('click', function(e) {
                e.preventDefault();
                h.each(function() {
                    $(this).removeClass(d.activeClass).nextUntil('h2').slideUp(d.speed);
                });
            });
        }
    }
})(jQuery)

