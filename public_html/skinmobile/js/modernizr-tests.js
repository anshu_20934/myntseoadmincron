Modernizr.addTest('opera_android', function(){
    var test = (navigator.userAgent.match(/opera/i));
    if(navigator.userAgent.match(/Chrome/i) && navigator.userAgent.match(/Firefox/i)){
        test=null;
    }
    return test;
});
Modernizr.addTest('firefox', function(){
    var test = (navigator.userAgent.match(/firefox/i));
    return test;
});
Modernizr.addTest('android', function(){
    var test = ((navigator.userAgent.match(/Android/i) && navigator.userAgent.match(/AppleWebKit/i)));
    if(navigator.userAgent.match(/Chrome/i) && navigator.userAgent.match(/Firefox/i)){
        test=null;
    }
    return test;
});
Modernizr.addTest('ie_browser', function(){
    var test = (navigator.userAgent.match(/internet explorer/i) || navigator.userAgent.match(/IEMOBILE/i));
    return test;
});
Modernizr.addTest('iphone', function(){
    var test = ((navigator.userAgent.match(/iphone/i) && navigator.userAgent.match(/AppleWebKit/i)));
    if(navigator.userAgent.match(/Chrome/i) && navigator.userAgent.match(/Firefox/i)){
        test=null;
    }
    return test;
});