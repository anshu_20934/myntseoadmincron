
Myntra.Header = {
    ActiveSearchBox: "",

    init: function() {
        var searchBoxType;
        var searchInputEl = $(".search-form-container .mk-site-search"),
            searchInputClearEl = $(".search-form-container .clear-text-btn"),
            searchInputCancelEl = $(".search-close-container");
        
        //Cart Button click Handler
        Myntra.Utils.extendClickEvent($('.m-cart-icon'), function(e){
            location.href=http_loc+"/mkmycart.php";
        });

        //Login Button click Handler
        //Myntra.Utils.extendClickEvent($(".btn-login"), function(e) {
        $(".btn-login").click(function(e) {
                Myntra.Header.loginPopUp(e);
        });

        //Logout Button click Handler
        Myntra.Utils.extendClickEvent($('.nav-logout-link'), function(e) {
            Myntra.Header.logOutUser(e);
        });

        Myntra.Utils.extendClickEvent(searchInputCancelEl, function(e) {
            if(Myntra.Header.ActiveSearchBox=="navsearchbox")
            {
                $('.m-slide-menu').trigger('click');
            }
            $(".search-form-container .mk-site-search").val('');
            $(".search-overlay").hide();
            $("body").removeClass('iosorientationfix');
            return false;
        });

        Myntra.Utils.extendClickEvent(searchInputClearEl, function() {
            searchInputEl.val('');
            searchInputClearEl.hide();
            //searchInputEl.focus();
            setTimeout("document.getElementById('search_query').focus()",500);
        });

        searchInputEl.keyup(function() {
            var str=$.trim($(this).val());   
            if(str != ''){
                searchInputClearEl.show();
            }
            else {
                searchInputClearEl.hide();
            }
        });

        $(searchInputEl).each(function(){
            $(this).blur(function(event){
                if($(event.currentTarget).val() == ""){
                    $(this).closest("form").find(".mk-placeholder-text").show();
                }
                return true;
            });
        });

        // Search autocomeplete
        if(Myntra.Data.showAutoSuggest == 1) {
            var cache = {},lastXhr;
                $(".mk-site-search").autocomplete({
                minlength:  1,
                delay:      0,
                autoFocus:  false,
                type:       "searchbox",
                select:     function(event,ui){
                                if(Myntra.Header.ActiveSearchBox == "navsearchbox" || Myntra.Header.ActiveSearchBox == "headersearchbox"){
                                    $('.search-form-container .mk-site-search').val(ui.item.value);
                                    Myntra.Header.menuSearchFormSubmit($('.search-form-container .mk-site-search'));
                                }
                            },
                source:     function( request, response ) {
                                var term = request.term;
                                if ( term in cache ) {
                                    response( cache[ term ] );
                                    return;
                                }

                                lastXhr = $.getJSON( "/myntra/auto_suggest.php", request, function( data, status, xhr ) {
                                cache[ term ] = data;
                                if ( xhr === lastXhr ) {
                                    if(data.length){
                                        response( data );
                                    }
                                    else{
                                        response([{
                                            "category" : "",
                                            "count" : "",
                                            "id" : "Search " + term,
                                            "label" : "Search " + term,
                                            "value" : term
                                        }]);
                                    }
                                }
                            });
                    },
                appendTo:   ".search-results-body",
                position:   {
                                my: "left top",
                                at: "left bottom"
                            }
                });
        }
    },
    
    loginPopUp: function(e){
        Myntra.Utils.preventDefault(e);
        if($(e.target).closest('ul').hasClass('mk-nav-links')){
        	$.pageslide.close();
        }
        //Myntra.Utils.slideObj.show();
        _gaq.push(['_trackEvent', 'header', 'signin', 'click']);
        if($(e.target).text().toLowerCase().replace(' ', '') == 'login') {
            $(document).trigger('myntra.login.show', [{action: 'login'}]);
        } else {
            $(document).trigger('myntra.login.show');
        }  		
    },
    logOutUser: function(e){   
	    Myntra.Utils.preventDefault(e);
	    _gaq.push(['_trackEvent', 'header', 'signout', 'click']);
	    window.localStorage.removeItem('PincodeWidget.ServiceabilityData');
	    var _obj = $(e.target);
	    do {
	    	_obj=_obj.parent();
	    }while(_obj.find('.header-logout-form').length==0)
	    	_obj.find('.header-logout-form').submit();
    },
        
    menuSearchFormSubmit: function(form){
        var str=$.trim($('.search-form-container .mk-site-search').val());   
        if(str != ''){
            _gaq.push(['_trackEvent', 'search_button', Myntra.Data.pageName, str]);
            str = str.replace(/\s+/g, '-').replace(/\//g, '__');
            location.href=http_loc+"/"+encodeURIComponent(str)+"?userQuery=true";
            return false;
        }
        else{
        
            return false;
        }
    },
    tempDisableCart:function() {
        $('.m-cart-icon').off();
        setTimeout(function(){
            $('.m-cart-icon').on("click", function(){
                location.href=http_loc+"/mkmycart.php";
            });
        },500); 
    },
    tempDisableMyntraIcon:function() {
        $('.m-myntra-icon').click(function(e){
            Myntra.Utils.preventDefault(e);
        });
        setTimeout(function(){
            $('.m-myntra-icon').click(function(){
                location.href=http_loc+"/";
            });
        },500); 
    }
};
$(document).ready(function() {
    Myntra.Header.init();
});
