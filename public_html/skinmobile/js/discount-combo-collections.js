Myntra.sCombo.styles = function() {
    this.collection	=	{
        models: [],
        length: function(){
                return this.models.length;
            }
    };
};

Myntra.sCombo.styles.prototype = {
    get : function(attrs){
        var attrs = attrs || {};
        if(attrs.uid){
            for(var a in this.collection.models){
                if(this.collection.models[a].get('uid') == attrs.uid)
                    return this.collection.models[a];
            }
        } else {
            return Myntra.sCombo.Err('not supported');
        }
        return false;
    },
    add : function(obj) {
        this.collection.models.push(obj);
        return true;
    },
    removeLast : function(){
        this.collection.models.pop();
    },
    remove: function(uid) {
        var y = $.grep(this.collection.models, function(styleObj,i) {
            return styleObj.get('uid') != uid.get('uid');
        });
        this.collection.models = y;
        return true;
    },
    unsetAll: function(){
        for(var i in this.collection){
            this.collection[i].unsetChecked();
        }
    },
    getAllChecked: function() {
        var checkedList = [];
        for(var i in this.collection.models){
            if(this.collection.models[i].isChecked()){
                checkedList.push(this.collection.models[i]);
            }
        }
        return checkedList;
    },
    getAllModified: function() {
        var checkedList = [];
        for(var i in this.collection.models){
            if(this.collection.models[i].isEditedFromCart()){
                checkedList.push(this.collection.models[i]);
            }
        }
        return checkedList;
    },
    getAllRemoved: function() {
        var checkedList = [];
        for(var i in this.collection.models){
            if(this.collection.models[i].isRemovedFromCart()){
                checkedList.push(this.collection.models[i]);
            }
        }
        return checkedList;
    },
    getAllAdded: function() {
        var checkedList = [];
        for(var i in this.collection.models){
            if(this.collection.models[i].isAddedToCart()){
                checkedList.push(this.collection.models[i]);
            }
        }
        return checkedList;
    },
    setCartData: function(){
        var styleid,
            cartTotal=0,
            getAmount=0,
            quantity=0;
        var allchecked = this.getAllChecked();
        Myntra.sCombo.quantity_in_combo = 0;
        Myntra.sCombo.amount_in_combo	= 0;
        for(var i in allchecked){
            styleid = allchecked[i].get('styleid');
            quantity = allchecked[i].get('quantity') ? parseInt(allchecked[i].get('quantity')) : 0;
            cartTotal += Myntra.sCombo.solrProducts[styleid].price * quantity;
            Myntra.sCombo.quantity_in_combo += quantity;
            Myntra.sCombo.amount_in_combo += Myntra.sCombo.solrProducts[styleid].price * quantity;
        }
        Myntra.sCombo.cartTotal = cartTotal;
        if(!Myntra.sCombo.isAmount)
            Myntra.sCombo.minMore = Myntra.sCombo.min - Myntra.sCombo.quantity_in_combo;
        else
            Myntra.sCombo.minMore = Myntra.sCombo.min - Myntra.sCombo.amount_in_combo;
        Myntra.sCombo.isConditionMet = Myntra.sCombo.minMore<=0?true:false;
        if(Myntra.sCombo.isConditionMet){
            if(Myntra.sCombo.isAmount & Myntra.sCombo.getAmount > 0 ){
                Myntra.sCombo.cartSavings = Myntra.sCombo.getAmount;
            } else if(Myntra.sCombo.getPercent){
                Myntra.sCombo.cartSavings = Myntra.sCombo.cartTotal * Myntra.sCombo.getPercent / 100 ;
            }
        } else {
            Myntra.sCombo.cartSavings = 0;
        }
        Myntra.sCombo.discountedTotal = Myntra.sCombo.cartTotal - Myntra.sCombo.cartSavings;
    },

    eventCheckboxStyle: function(attrs){
        return Myntra.Combo.Err('implement eventcheckboxstyle');
    },
    eventChangeSize: function(attrs){
        var uid = attrs['uid'];
        var styleObj = this.get({uid:uid});
        if(!styleObj) Myntra.sCombo.Err('size change style obj not fould');
        //styleObj.set({skuid:skuid});
        var size = $("#item_combo_lay_"+styleObj.get('uid')+" .cow-size-loader option:selected").html();
        styleObj.set({size:size});
        Myntra.sCombo.UI.ModifySizeSetQuantity(styleObj);
        Myntra.sCombo.UI.refreshUI();
    }
};


Myntra.sCombo.setStyles = function() {
    Myntra.sCombo.styles.apply(this, arguments);
};
Myntra.Utils.extend(Myntra.sCombo.setStyles, Myntra.sCombo.styles, {
    eventCheckboxStyle: function(attrs){
        var uid = attrs['uid'];
        var styleObj = this.get({uid:uid});
        if(!styleObj){
            var _tmp_style = Myntra.sCombo.comboSet[uid];
            if(!_tmp_style) return Myntra.sCombo.Err('no match in solr');
            var styleobj = new Myntra.sCombo.styleModel();
            styleobj.set({uid: uid , styleid: _tmp_style.styleid , skuid: 0, quantity: 0,size: ''});
            this.add(styleobj);
            styleObj = this.get({uid: uid});
        }
        if(!styleObj) return Myntra.sCombo.Err('no style object constructed');

        if(!styleObj.isChecked()){
            styleObj.setChecked();
            styleObj.elCheckbox().removeClass("unchecked").addClass("checked");
            Myntra.sCombo.UI.showFlip(styleObj);
            styleObj.loadSizes(function(){
                Myntra.sCombo.UI.showFlip(styleObj);
                if(styleObj.isAvailable()){
                    Myntra.sCombo.UI.checkStyle(styleObj);
                    /*var qnty = 1;
                    if(!styleObj.isInCart()){
                        styleObj.set({quantity:qnty});
                    }*/
                } else {
                    //styleObj.elLi().remove();
                    //collection.removeLast();
                }
                Myntra.sCombo.UI.refreshUI();
            });
            Myntra.sCombo.UI.refreshUI();
        } else {
            styleObj.unsetChecked();
            if(styleObj.geti('size') && styleObj.geti('quantity')){
            } else {
                styleObj.set({size:null,skuid:null,quantity:null});
            }
            Myntra.sCombo.UI.uncheckStyle(styleObj);
            Myntra.sCombo.UI.refreshUI();
        }
        return true;
    }
});

Myntra.sCombo.freeStyles = function() {
    Myntra.sCombo.styles.apply(this, arguments);
};
Myntra.Utils.extend(Myntra.sCombo.freeStyles, Myntra.sCombo.styles, {
    eventCheckboxStyle: function(attrs){
        var uid = attrs['uid'];
        var styleObj = this.get({uid:uid});
        if(!styleObj){
            var _tmp_style = Myntra.sCombo.comboSet[uid];
            if(!_tmp_style) return Myntra.sCombo.Err('no match in solr');
            var styleobj = new Myntra.sCombo.styleModel();
            styleobj.set({uid: uid , styleid: _tmp_style.styleid , skuid: 0, quantity: 0,size: ''});
            styleobj.setItemAddedByDiscountEngine();
            this.add(styleobj);
            styleObj = this.get({uid: uid});
        }
        if(!styleObj) return Myntra.sCombo.Err('no style object constructed');

        if(!styleObj.isChecked()){
            _gaq.push(['_trackEvent', 'combo_free', 'select_style', 'check', parseInt(styleObj.get('styleid'))]);
            var alreadyChecked = Myntra.sCombo.cFreeStyles.getAllChecked();
            for(var i in alreadyChecked){
                alreadyChecked[i].unsetChecked();
                Myntra.sCombo.UI.uncheckBoxStyle(alreadyChecked[i]);
                Myntra.sCombo.UI.priceMessageContent(false,
                    alreadyChecked[i].get('uid'),alreadyChecked[i].get('size'),alreadyChecked[i].get('quantity'));
            }
            styleObj.setChecked();
            Myntra.sCombo.UI.checkBoxStyle(styleObj);
            styleObj.loadSizes(function(){
                if(styleObj.isAvailable()){
                    Myntra.sCombo.UI.checkBoxStyle(styleObj);
                    var sizeList = styleObj.get('sizeList');
                    var cnt = 0;
                    for(var i in sizeList){
                        cnt++;
                    }
                    if(cnt == 1){
                        for(var i in sizeList){
                            styleObj.set({skuid: i , size: sizeList[i][0], quantity : 1});
                        }
                        Myntra.sCombo.UI.priceMessageContent(true,styleObj.get('uid'),styleObj.get('size'),styleObj.get('quantity'));
                        $("#item_combo_lay_"+styleObj.get('uid')+" .main-flip .edit-btn").addClass("hide");
                    } else {
                         Myntra.sCombo.UI.refreshUI();
                        return Myntra.sCombo.Err('more than one item');
                    }
                }
                console.log(Myntra.sCombo.cFreeStyles.getAllChecked());
                Myntra.sCombo.UI.refreshUI();
            });
            Myntra.sCombo.UI.refreshUI();
        } else {
            styleObj.unsetChecked();
            _gaq.push(['_trackEvent', 'combo_free', 'select_style', 'uncheck', parseInt(styleObj.get('styleid'))]);
            if(styleObj.geti('size') && styleObj.geti('quantity')){
            } else {
                styleObj.set({size:null,skuid:null,quantity:null});
            }
            Myntra.sCombo.UI.uncheckStyle(styleObj);
            Myntra.sCombo.UI.refreshUI();
        }
        Myntra.sCombo.UI.refreshUI();
        return true;
    }

});
