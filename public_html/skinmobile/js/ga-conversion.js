function getGAActionAndLabel(category,articleType,gender,brand){
    var gaEventAction = 'na',
        gaEventLabel = 'na';
    if(category) {
        gaEventAction = category;
        if(articleType) {
            gaEventAction += ' | ' + articleType;
        }
    }

    if(gender) {
        gaEventLabel = gender;
        if(brand) {
            gaEventLabel += ' | ' + brand;
        }
    } else {
        if(gaEventBrand) {
            gaEventLabel = '| '+ brand;
        }
    }

    return {
        action: gaEventAction.replace(/-/g,' ').toLowerCase(),
        label: gaEventLabel.replace(/-/g,' ').toLowerCase()
    };
}
