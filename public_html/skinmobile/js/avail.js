//AVAIL
Myntra.emark=null;


function getRRCookie(cname)
{
	if( localStorage ){
		var ls_rr_sid = localStorage.getItem('lscache-rr_sid'); // geting from local storage
		
		if( ls_rr_sid ){
			return ls_rr_sid;
		}
	}
	
	var name = Myntra.Data.cookieprefix+""+cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) 
	{
	  var c = ca[i].trim();
	  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
	}
	return "";
}

var R3_COMMON = null;
var R3_ITEM = null;
function availOnLoad() {
	
	// Rich relevance
	$.getScript(window.location.protocol+"//media.richrelevance.com/rrserver/js/1.0/p13n.js",function(){
		
		R3_COMMON = new r3_common();

		
		R3_COMMON.setApiKey('54ed2a6e-d225-11e0-9cab-12313b0349b4');
		
		if(Myntra.Data.cookieprefix != "" ){
			R3_COMMON.setBaseUrl(window.location.protocol+'//integration.richrelevance.com/rrserver/');
		}else{
			R3_COMMON.setBaseUrl(window.location.protocol+'//recs.richrelevance.com/rrserver/');
		}
		
		R3_COMMON.setClickthruServer(window.location.protocol+"//"+window.location.host);
		var rr_sid = getRRCookie("rr_sid")+"";
		R3_COMMON.setSessionId(rr_sid);
		
		if(typeof Myntra.Data.userHashId == "undefined" || Myntra.Data.userHashId == "" ){
			R3_COMMON.setUserId(rr_sid);
		}else{
			R3_COMMON.setUserId(Myntra.Data.userHashId+"");
		}
		
		RR.jsonCallback = function (){
			//console.dir(RR.data.JSON.placements);
			//console.dir("done");
		}; // call back performed, once rr call is executed.


		
		switch (Myntra.Data.pageName) {
		case "home":
			// do nothing 
			break;
		case "pdp":
			R3_ITEM = new r3_item();
			R3_ITEM.setId(Myntra.PDP.Data.id+"");
			R3_ITEM.setName(Myntra.PDP.Data.name+"");
			
			R3_COMMON.addCategoryHintId(Myntra.PDP.Data.articleTypeId+"");

			R3_COMMON.addPlacementType('item_page.Horizontal');

			rr_flush_onload();
			
			r3();
			break;
		case "cart":
			break;
		}
	});
	
	
	$.getScript("http://service.sg.avail.net/2009-02-13/dynamic/54ed2a6e-d225-11e0-9cab-12313b0349b4/emark.js", function() {
		Myntra.emark = new Emark();
		//Myntra.emarkC =new Emark();
		switch (Myntra.Data.pageName) {
		case "home":
			//DO NOTHING
			break;
		case "pdp":
			//PAHSE-1-->getRecommendation-Presentation,lgClickedOn, If added to cart call logAddedToCart
			avail_dynamic_param = "append andcat in subtemplate ALL with " + avail_dynamic_param;
			var t_code = Myntra.Utils.getQueryParam("t_code");
			if (t_code != '') {
				Myntra.emark.logClickedOn(curr_style_id, t_code);
			}
			var product = "ProductID:" + curr_style_id;
			var recProducts = Myntra.emark.getRecommendations("Productpage", [product], [avail_dynamic_param]);
			//do the logClickedonHere on PDP page load
			Myntra.emark.commit(function() {
				//do presentation here
				displayAvail(recProducts.values, recProducts.trackingcode);

			});

			break;
        /* Gopi: search is not used
		case "search":

			var curUrl = ("'" + window.location + "'").search('#!');
			var pageFilter = -1;
			if (curUrl != -1) {
				pageFilter = ("'" + window.location + "'").search('#!page');
			}
			var phrase = "Phrase:" + query;
			var recProducts = Myntra.emark.getRecommendations("SearchPage", [phrase]);
			Myntra.emark.commit(function() {
				if (curUrl == -1) { //displayAvail(recProducts.values,recProducts.trackingcode);
				}
				else if (pageFilter != -1) { //displayAvail(recProducts.values,recProducts.trackingcode);
				}
			});
			break;
        */
		case "cart":
			var recProducts = Myntra.emark.getRecommendations("ShoppingCart");
			Myntra.emark.commit(function() {
				displayAvail(recProducts.values, recProducts.trackingcode);
			});

			break;
		}

	});
}
function displayAvail(styleIds, trackingCode, page) {
	Myntra.Data.pageName=(page)?page:Myntra.Data.pageName;
	var rec_url = http_loc + "/avail_ajax_recommendation.php?avail_ids=" + styleIds + "&avail_track=" + trackingCode + "&page=" + Myntra.Data.pageName;
	//var rec_url=http_loc+"/avail_ajax_recommendation.php?avail_ids=2336,1728,1594,3678,2000,1649,4354,1727,4854,5332,3642,5050&avail_track=b47206d9b2f8ebf5b67de5143e83b860&page="+pageName;
	$.ajax({
		type: "GET",
		url: rec_url,
		success: function(data) {
			if (data != 0){
				switch(Myntra.Data.pageName){
					case "addedtocart":
						$('.recommend-block').html(data);
						if($('.mk-matchmaker-horizontal .rs-carousel-runner > li').size()>4){
							$('.mk-matchmaker-horizontal').carousel({
								itemsPerPage: 4, // number of items to show on each page
								itemsPerTransition: 1, // number of items moved with each transition
								noOfRows: 1, // number of rows (see demo)
								nextPrevLinks: true, // whether next and prev links will be included
								pagination: false, // whether pagination links will be included
								speed: 'normal' // animation speed
							});
						}
						$(document).trigger('myntra.addedtocart.center');
				break;
				case "cart":
					$('.cart-recommend-block').hide().html(data).fadeIn(700,function(){
						if($('.mk-matchmaker-horizontal .rs-carousel-runner > li').size()>5){
							$('.cart-recommend-block .mk-carousel-five').carousel({
								itemsPerPage: 5, // number of items to show on each page
								itemsPerTransition: 1, // number of items moved with each transition
								noOfRows: 1, // number of rows (see demo)
								nextPrevLinks: true, // whether next and prev links will be included
								pagination: false, // whether pagination links will be included
								speed: 'normal' // animation speed
							});
						}
					});
                    // show the err msg on hover of buy now button unless size is selected
                    $('.cart-recommend-block .inline-add').hover(
                        function() {
                            if ($(this).prev().find('.inline-select').val() == '0') {
                                $(this).next('.err').show();
                            }
                        },
                        function() {
                            $(this).next('.err').hide();
                        }
                    );
					Myntra.Data.styleids=[];
					$('.inline-add-to-cart-form').find(":hidden[name='productStyleId']").each(function(i, el) {
						Myntra.Data.styleids.push(el.value);
					});
				   $.ajax({
				        type: 'GET',
				        url: '/myntra/ajax_getsizes.php?ids=' + Myntra.Data.styleids.join(','),
				        dataType: 'json',
				        beforeSend:function() {
				        },
				        success: function(resp) {
				            //load in the select
				        	Myntra.Data.dataRecentSizes=resp;
				        	$('.inline-add-to-cart-form').each(function(e,el){
				        		Myntra.Utils.loadRecentSizes($(el));
				        	});

				        },
				        complete: function(xhr, status) {
				        }
				    });

				break;
				case "pdp":
					if ($('#mk-matchmaker').length){
						$('#mk-matchmaker').hide().append(data).fadeIn(1000);
						$('.mk-match-cycle').cycle({
							timeout: 9000,
							pager: '.mk-cycle-nav'
						});
					}
				break;
				}
			}
			else
			{
				$('.cart-recommend-block').hide()
				$('.recommend-block').hide();
				$('#mk-matchmaker').hide();
			}
			Myntra.Utils.showAdminStyleDetails();
		}
	});

}
//END AVAIL

$(window).load(function(){

	availOnLoad();


});



