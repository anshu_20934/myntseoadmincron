
function getRRCookie(cname)
    {
		var ls_rr_sid = localStorage.getItem('lscache-rr_sid'); // geting from local storage
			
		if( ls_rr_sid ){
			return ls_rr_sid;
		}
    	var name = Myntra.Data.cookieprefix+""+cname + "=";
    	var ca = document.cookie.split(';');
    	for(var i=0; i<ca.length; i++) 
    	{
    	  var c = ca[i].trim();
    	  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
    	}
    	return "";
    }

var R3_COMMON = null;
var R3_ITEM = null;

$(window).load(function(){
    Myntra.emark=null;


	// Rich relevance
	$.getScript(window.location.protocol+"//media.richrelevance.com/rrserver/js/1.0/p13n.js",function(){
		
		R3_COMMON = new r3_common();

		
		R3_COMMON.setApiKey('54ed2a6e-d225-11e0-9cab-12313b0349b4');
		
		if(Myntra.Data.cookieprefix != "" ){
			R3_COMMON.setBaseUrl(window.location.protocol+'//integration.richrelevance.com/rrserver/');
		}else{
			R3_COMMON.setBaseUrl(window.location.protocol+'//recs.richrelevance.com/rrserver/');
		}
		
		R3_COMMON.setClickthruServer(window.location.protocol+"//"+window.location.host);
		var rr_sid = getRRCookie("rr_sid")+"";
		R3_COMMON.setSessionId(rr_sid);
		
		if(typeof Myntra.Data.userHashId == "undefined" || Myntra.Data.userHashId == "" ){
			R3_COMMON.setUserId(rr_sid);
		}else{
			R3_COMMON.setUserId(Myntra.Data.userHashId+"");
		}
		
		RR.jsonCallback = function (){
			//console.dir(RR.data.JSON.placements);
			//console.dir("done");
		}; // call back performed, once rr call is executed.


		
		switch (Myntra.Data.pageName) {
		case "home":
			// do nothing 
			break;
		case "pdp":
			R3_ITEM = new r3_item();
			R3_ITEM.setId(Myntra.PDP.Data.id+"");
			R3_ITEM.setName(Myntra.PDP.Data.name+"");
			
			R3_COMMON.addCategoryHintId(Myntra.PDP.Data.articleTypeId+"");

			R3_COMMON.addPlacementType('item_page.Horizontal');

			rr_flush_onload();
			
			r3();
			break;
		case "cart":
			var R3_CART = new r3_cart();
			
			try{
				var cartItems = JSON.parse(Myntra.Cart.Data.items);
			}catch(err){
				return; // unable to parse the items
			}
			 cartItems.forEach(function(val,idx){
	                R3_CART.addItemId(val.id+'');
	         });
			
			
			R3_COMMON.addPlacementType('item_page.Horizontal');

			rr_flush_onload();
			
			r3();
			break;
		}
	});
	
	if(Myntra.Data.pageName != 'pdp'){
		// if not pdp, return from here
		return ;
	}
    
    $.getScript("http://service.sg.avail.net/2009-02-13/dynamic/54ed2a6e-d225-11e0-9cab-12313b0349b4/emark.js", function() {
        Myntra.emark = new Emark();
        //Myntra.emarkC =new Emark();
        switch (Myntra.Data.pageName) {
        case "home":
            //DO NOTHING
            break;
        case "pdp":
            /* similar products*/
            //PAHSE-1-->getRecommendation-Presentation,lgClickedOn, If added to cart call logAddedToCart
            avail_dynamic_param = "append andcat in subtemplate ALL with " + avail_dynamic_param;
            var t_code = Myntra.Utils.getQueryParam("t_code");
            if (t_code != '') {
                Myntra.emark.logClickedOn(curr_style_id, t_code);
            }
            var product = "ProductID:" + curr_style_id;                
            recProductsSimilar= Myntra.emark.getRecommendations("Productpage", [product], [avail_dynamic_param]);
            
            /* cross selling products*/
            /*if (Myntra.PDP.showExtraRec)
            {
                cross_sell_avail_dynamic_param1 = "append andcat in subtemplate ALL with " + cross_sell_avail_dynamic_param1;
                cross_sell_avail_dynamic_param2 = "append orcat in subtemplate ALL with " + cross_sell_avail_dynamic_param2;
                recProductsMatchWith= Myntra.emark.getRecommendations("Productpage", [product], [cross_sell_avail_dynamic_param1,cross_sell_avail_dynamic_param2]);
            }*/
            //do the logClickedonHere on PDP page load
            Myntra.emark.commit(function() {
                //do presentation here
                var json = {};
                json['similar'] = {};
                json['similar']['styleids'] = recProductsSimilar.values;
                json['similar']['trackingcode'] = recProductsSimilar.trackingcode;
                json['similar']['tabid'] = 1;
                /*if (Myntra.PDP.showExtraRec)
                {
                    json['matchWith'] = {};
                    json['matchWith']['styleids'] = recProductsMatchWith.values;
                    json['matchWith']['trackingcode'] = recProductsMatchWith.trackingcode;
                    json['matchWith']['tabid'] = 2;
                    
                }*/
                /*json['similar'] = {};
                json['similar']['styleids'] = [["42273"], ["42035"], ["42272"], ["40373"], ["40380"],["33726"], ["33732"], ["38411"], ["33738"], ["38402"]];
                json['similar']['trackingcode'] = "3d6913f75ba3262c0276f697ad9c2d5c";
                json['similar']['tabid'] = 1;*/
    /*            json['matchWith'] = {};
                json['matchWith']['styleids'] = [["42273"], ["42035"], ["42272"], ["40373"], ["40380"],["33726"], ["33732"], ["38411"], ["42273"], ["42035"]];
                json['matchWith']['trackingcode'] = "3d6913f75ba3262c0276f697ad9c2d5c";
                json['matchWith']['tabid'] = 2;*/
                
                displayAvail("pdp",json);
            });

            break;
        case "cart":
            var recProducts = Myntra.emark.getRecommendations("ShoppingCart");
            Myntra.emark.commit(function() {
                displayAvail(recProducts.values, recProducts.trackingcode);
            });

            break;
        }

    });
    /*
function getItemsWidth(){
 	   var _itemwidth=125;
 	   var screenWidth=screen.width;
 	   if(screenWidth<=480){
 		  _itemwidth=125;
 	   }else if(screenWidth>480 && screenWidth<=768){
 		  _itemwidth=125;
 	   }else {
 		  _itemwidth=142;
 	   }
 	   return 125;
    }
    */
var flexdata;
function flexRecommedInit(data) {
	   var recentDiv = $('.m-product-reco');
	   recentDiv.attr('id',"tempFlex");
	   var itsClass=recentDiv.attr('class');
	   recentDiv.before("<div class='"+itsClass+"'></div>");
	   $('#tempFlex').remove();
	   var recentDiv = $('.m-product-reco');
	   recentDiv.removeClass('mk-carousel');
	   if(!recentDiv.hasClass('flexslider')){
		   recentDiv.addClass('flexslider')
		   }
	   recentDiv.html('');
	   recentDiv.html(data);
	   recentDiv.find(".mk-match").addClass("slides");
	   var itemwidth=$('.m-product-reco.flexslider .slides .mk-cycle-inner').width();
	   itemnum = recentDiv.find('.mk-cycle-inner').length;
	   var divwidth=itemwidth*itemnum;
	   recentDiv.css({'width':divwidth});
       if (itemnum === 0){
           recentDiv.hide();
           return;
       }
       recentDiv.find('.rec-title').remove();
       recentDiv.find('h3').remove();
       recentDiv.prepend('<h3>You May Also Want To Consider</h3>');
       recentDiv.find('span').remove();
       $('.m-product-reco .mk-match:gt(0)').remove();
	    //getRecentSizes();
       /*$('.availability').each(function(){
       var sizes = "<p>"+$(this).attr("data-sizes")+"</p>";
       $(this).append(sizes);
  });*/
    $('.m-product-reco.flexslider').flexslider({
    	selector: '.slides > .mk-cycle-inner',
        animation: "slide",
        startAt: 0,                     //Integer: The slide that the slider should start on. Array notation (0 = first slide)
        slideshow: true,
        animationLoop: true,
        itemMargin: 0,
        move: 0,
        minItems: 1, // use function to pull in initial value
        maxItems: 4,
        itemWidth: itemwidth
      });
    $(".mk-recent-viewed-ul li").addClass("flex-recently-viewed-product");
    recentDiv.show();
    $(".mk-match .mk-cycle-inner a").fastClick(function(e){
         _gaq.push(['_trackEvent', 'pdp_click', Myntra.Data.pageName, 'recent_widget']);
         window.location.href = $(this).attr('href');
    });
} 

function displayAvail(page, json) {
    Myntra.Data.pageName=(page)?page:Myntra.Data.pageName;
    var rec_url = http_loc + "/avail_ajax_recommendation.php",
        data = "json=" + JSON.stringify(json) + "&page=" + Myntra.Data.pageName;
    $.ajax({
        type: "POST",
        url: rec_url,
        data: data,
        success: function(data) {
            data = $.parseJSON(data);
            if (data != 0){
            	flexdata=data[1];
                flexRecommedInit(flexdata);
                var flexresizeTimer;
                $(window).resize(function() {
                    clearTimeout(flexresizeTimer);
                    flexresizeTimer = setTimeout(function() {
                        if(mobileOrientationCheck.isChanged()) {
                        	flexRecommedInit(flexdata);
                    	}
                    }, 200);
                });
                }else{
                    $('.mk-product-reco').hide();
                }
            }
        });
        
    }

});

