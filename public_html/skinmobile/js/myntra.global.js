Myntra = Myntra || {};

Myntra.loadImageQueue = function(){
    var lazy_images = $('.lazy');
    $.each(lazy_images, function() {
        $(this).attr("src", $(this).attr("data-lazy-src"));
        $(this).removeAttr("data-lazy-src");
        $(this).removeClass("lazy");
    });
};

Myntra.loadLazyElements = function() {
    if( typeof Myntra.Data.lazyElems != "undefined" && Myntra.Data.lazyElems.length > 0){
        var elementsProcessed = [];

        for(var index in Myntra.Data.lazyElems){
			var elemdata = Myntra.Data.lazyElems[index];
			if(typeof elemdata == "string"){
	            // Remove the duplicates out
	            if($.inArray(elemdata, elementsProcessed) != -1){
	                continue;
	            }
	            elementsProcessed.push(elemdata);
	            if($.trim(elemdata).length != 0){
	                var index = elemdata.indexOf(Myntra.Data.lazySeparator);
	                var elem = elemdata.substr(0, index);
	    			var elemparent = elemdata.substr(index + Myntra.Data.lazySeparator.length);
	                // Add child element and throw the custom change event to notify listerners
	                $(elemparent).append(elem).trigger("lazy.change");
	            }
        	}
		}
	}
};


$(window).load(function() {
    Myntra.loadImageQueue();
    Myntra.loadLazyElements();
});

/* Global JS
(myntra.global.js)
------------------------------------ */

var sizeSelect = function(sku, idx, el) {
    el = $(el);
    var sizeDropCont = el.closest('.mk-size-drop-pdp');
    el.parent().removeClass('error');
    $(".select-err").remove();
    $('.selected').removeClass('selected');
    sizeDropCont.find('.saved-message').hide();
    el.addClass('selected');
    if (el.hasClass('unavailable')) {
        sizeDropCont.find('.add-button-group, .mk-count-message').addClass('mk-hide');
        var notifyForm= $(document).find('.notify-cont');
        notifyForm.removeClass('mk-hide');
        $(document).find('.mk-buy-div, .mk-add-to-wishlist').addClass('mk-hide');
        var notifyButton= $(document).find('.notify-btn-grp')
        notifyButton.show();
        $(".notify-msg").html("").removeClass("error").removeClass("success");
    }
    else {
        $('.mk-add-to-wishlist').show();
        var cntMsg = sizeDropCont.find('.mk-count-message');
        el.siblings('.mk-size').val(sku);
        sizeDropCont.closest('.add-button-group').removeClass('mk-hide');
        var notifyForm= $(document).find('.notify-cont');
        notifyForm.addClass('mk-hide');
        $(document).find('.mk-buy-div, .mk-add-to-wishlist').removeClass('mk-hide');
        sizeDropCont.closest('.notify-cont').addClass('mk-hide');
        (el.data('count') <= 3)
            ? cntMsg.removeClass('mk-hide').find('span').text(el.data('count'))
            : cntMsg.addClass('mk-hide');
    }
};


var sizeSelected = function(sizeDropCont){
	return ((sizeDropCont.find('.mk-size').val()!=0) || (sizeDropCont.find('.mk-freesize').length));
};

$(document).ready(function(){
	/* CLOSE NOTIFY WHEN CANCEL IS CLICKED */
	$('.notify-cancel').live('click', function(e){
		var sizeDropCont = $(document).find('.mk-size-drop-pdp');
		e.preventDefault();
		sizeDropCont.find('.add-button-group').removeClass('mk-hide');
		var notifyForm= $(document).find('.notify-cont');
	    notifyForm.addClass('mk-hide');
	    $(document).find('.mk-buy-div').removeClass('mk-hide');
        $(".selected").removeClass("selected");
	    //sizeDropCont.find('.mk-size-drop ul li a').removeClass('selected').css('background','#fff');
		//var selectedValue=sizeDropCont.find(".mk-size option[value='0']").text();
		//sizeDropCont.find('.mk-size-drop div span').text(selectedValue);
		
	});

	/* NOTIFY ME EMAIL */
	$('.notify-button').live('click',function(e){
		e.preventDefault();
		var notifyMsg = $(this).closest('.notify-cont').find('.notify-msg').html(''),
			formToSubmit = $(this).closest('.notify-me-form'),
			notifyBtns = $(this).closest('.notify-btn-grp'),
			email = formToSubmit.find("input.notify-email").val();
		if(Myntra.Utils.validateEmail(email)){
			var sku = $(document).find('.selected').val();
			var url = http_loc + "/subs_notification.php";
			formToSubmit.find(":hidden[name='sku']").val(sku);
			$.ajax({
				type: "POST",
				url: url,
				data: formToSubmit.serialize()
			});
			notifyMsg
				.addClass('success')
				.removeClass('error')
				.html('<span class="icon-success"></span> We will notify you when this size is available.');
			notifyBtns.hide();
			setTimeout(function(){
				$('.notify-cancel').trigger('click');
			},4000);
			var pageName = formToSubmit.find(":text[name='pageName']").val();
			var parameterForGA = sku + '-' + email;
			_gaq.push(['_trackEvent', pageName, 'notifyme', parameterForGA]);
		} else {
			notifyMsg
				.removeClass('success')
				.addClass('error')
				.html('Please enter a Valid Email Address ');
		}
	});

	
	$('.mk-add-to-cart, .mk-express-buy').live('click',function(){
		var sizeDropCont = $(document).find('.mk-size-buttons');
		var formToSubmit = $(document).find('.add-to-cart-form');
		if(sizeSelected(sizeDropCont)){
			var quantity = formToSubmit.find(":hidden[name='quantity']").val();
			var sku_;
			var size;
			if(sizeDropCont.find('.mk-freesize').length)
			{
				size = sizeDropCont.find('.mk-freesize').text();
			}
			else 
			{
				size = sizeDropCont.find('.flat-size-options .size-btn.selected').text();
				sku_ = sizeDropCont.find('.mk-size').val();
			}
			var productStyleId = formToSubmit.find(":hidden[name='productStyleId']").val();
			var articleType = formToSubmit.find(":hidden[name='articleType']").val();
			var pd = {'id':productStyleId,'at':articleType};

			formToSubmit.find(":hidden[name='sizequantity[]']").val(quantity);
			if(!sizeDropCont.find('.mk-freesize').length)
			{
				formToSubmit.find(":hidden[name='productSKUID']").val(sku_);
			}
			formToSubmit.find(":hidden[name='selectedsize']").val(size);
			//AJAX ADD TO CART
			/*commenting to redirect user to cart */
			//FORM SUBMIT TRIGGER
                        if($(this).hasClass('mk-express-buy')){
                            formToSubmit.attr('action','/oneclickcart.php');
                        }
            _gaq.push(['_trackEvent','Cart_conversion', Myntra.PDP.gaConversionData.action, Myntra.PDP.gaConversionData.label]); 
			formToSubmit.submit();
			//alert(sku_);
			//Create a new lightbox here for order confirmation;
		}
		else {
			sizeDropCont.find('.flat-size-options .options').addClass('error');
            if (!$(".select-err")[0])sizeDropCont.find('.flat-size-options .options').after('<div class="select-err red">Please select a size</div>');
			
		}
	}).live("mouseenter", function() {
		if(!sizeSelected($(this).closest('.mk-product-option-cont'))) {
			if($(".zoom-window").length){
				$(this).closest('.add-button-group').find('.mk-size-error').css('top', '40px').show();
			}
			else{
				$(this).closest('.add-button-group').find('.mk-size-error').css('top', '-3px').show();
			}	
		}
	}).live("mouseleave", function() {
		$(this).closest('.add-button-group').find('.mk-size-error').hide();
	});
	
	Myntra.saveItemAsync = function(saveObj){
		var rURL = http_loc+"/mksaveditem.php",
			sizeDropCont = $('.mk-product-page');
        var addBtnGrp = sizeDropCont.find('.mk-add-to-wishlist');
        var savedMsg = $('.savedMsg');
        if (savedMsg.length){
            savedMsg.html('Saving....');
        }
        else {
            addBtnGrp.append("<div class='saved-message'><span></span>Saving...</div>");
        }
        $(savedMsg).show();
		$.ajax({
			type: "POST",
			url: rURL,
			data: "actionType=add&productSKUID="+saveObj.sku+"&sizequantity="+saveObj.qty+"&_token="+Myntra.Data.token,
			success: function(data) {
				var addBtnGrp = sizeDropCont.find('.add-button-group');
				if (window.location.pathname!='/mkmycart.php'){
					if(Myntra.Utils.Cookie.get('_sku')){
						Myntra.Utils.Cookie.del('_sku');
						Myntra.Utils.Cookie.del('_url');
						Myntra.Utils.Cookie.del('_saveitem_ex_social');
                    }
                    savedMsg = addBtnGrp.find('.saved-message');
                    savedMsg.html('Product Saved');
                    setTimeout(function() {
                    	savedMsg.fadeOut('slow');
                    }, 4000);
				}
				else {
					window.location=document.URL;
				}
				if(data == 'saved'){
					Myntra.Header.setCount('saved',Myntra.Header.getCount('saved')+1);
				}
				FB.getLoginStatus(function(response) {
					if(response.status === 'connected') {
						$(document).trigger('myntra.fb.wishlist.post',{href:saveObj.url,exSocial:saveObj.excludeSocial,miniPIP:saveObj.isMini});
					}
					else 
					{
						if(saveObj.isMini){ 
							if(!Myntra.Data.MiniPIPsocialExclude){

								Myntra.Data.MiniSocialShareUrl=saveObj.url;
								if(addBtnGrp.find('.wishlist-fb-login').length){
									addBtnGrp.find('.wishlist-fb-login').show();
								}
								else{
									if(social_share_variant=='test'){
										addBtnGrp.append('<div class="wishlist-fb-login"><em></em><span class="share-yes">SHARE YOUR WISHLIST</span></div>')
									}
								}
							}
						}
						else if(!Myntra.Data.PDPsocialExclude){
							if(addBtnGrp.find('.wishlist-fb-login').length){
								addBtnGrp.find('.wishlist-fb-login').show();
							}
							else{
								if(social_share_variant=='test'){
									addBtnGrp.append('<div class="wishlist-fb-login"><em></em><span class="share-yes">SHARE YOUR WISHLIST</span></div>')
								}
							}
						}			
					}
				});
			}
		});
	};


	$('.mk-save-for-later').live('click',function(){
		var sizeDropCont = $(document).find('.mk-size-buttons');
		var formToSubmit = $(document).find('.add-to-cart-form');
		
		var quantity = formToSubmit.find(":hidden[name='quantity']").val();
		var	pUrl = sizeDropCont.parent().find('.mk-more-info').length ? sizeDropCont.parent().find('.mk-more-info').attr('href') : window.location.href;
		if(sizeDropCont.find('.mk-freesize').length){
			sku_=formToSubmit.find(":hidden[name='productSKUID']").val();;
		}
		else{
			sku_= sizeDropCont.find('.mk-size').val();
		}

		if(sizeSelected(sizeDropCont)){
			if(!Myntra.Data.userEmail){
				$(document).trigger('myntra.login.show', {onLoginDone: function() {
                    Myntra.Utils.Cookie.set('_sku',sku_);
                    Myntra.Utils.Cookie.set('_url',pUrl);
                    Myntra.Utils.Cookie.set('_saveitem',1);
                    location.reload();
                }});
			}
			else {
			//AJAX SAVE FOR LATER
				var isMiniPIP=($(this).hasClass('miniPIP-wishlist'))?true:false;
				var exclude=false;
	   	    	if(typeof Myntra.Data.MiniPIPsocialExclude != 'undefined'){
	   	       		exclude=Myntra.Data.MiniPIPsocialExclude;
	   	    	}
	   	    	else{
	   	    		exclude=Myntra.Data.PDPsocialExclude;
	   	    	}
				Myntra.saveItemAsync({sku:sku_,qty:quantity, url:pUrl,excludeSocial:exclude,sizeCont:sizeDropCont,isMini:isMiniPIP});
			}
		}
		else{
            //sizeDropCont.find('.mk-size-drop .jqTransformSelectOpen').click();
			sizeDropCont.find('.flat-size-options .options').addClass('error');
            if (!$(".select-err")[0])sizeDropCont.find('.flat-size-options .options').after('<div class="select-err red">Please select a size</div>');
		}
	}).live("mouseenter", function() {
		if(!sizeSelected($(this).closest('.mk-product-option-cont'))) {
			$(this).closest('.add-button-group').find('.mk-size-error').css('top', '32px').show();
		}
	}).live("mouseleave", function() {
		$(this).closest('.add-button-group').find('.mk-size-error').hide();
	});

});
