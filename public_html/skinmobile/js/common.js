//Search form submit functionality
function menuSearchFormSubmit(form)
{
    var str=$.trim($(form).find(".mk-site-search").val());
	if(str != '')
    {
		_gaq.push(['_trackEvent', 'search_button', Myntra.Data.pageName, str]);
        str = str.replace(/\s+/g, '-').replace(/\//g, '__');
        window.location=http_loc+"/"+encodeURIComponent(str)+"?userQuery=true";
        return false;
    }
    else{
        return false;
    }
}
// placeholder fix for all inputs only in IE - 
$(document).ready(function() {
    if (!Modernizr.input.placeholder) {
        $(function() {
            var input = document.createElement("input");
            if (('placeholder' in input) == false) {
                $('[placeholder]').focus(function() {
                    var i = $(this);
                    if (i.val() == i.attr('placeholder')) {
                        i.val('').removeClass('placeholder');
                        if (i.hasClass('password')) {
                            i.removeClass('password');
                            this.type = 'password';
                        }
                        if (i.hasClass('tel')) {
                            i.removeClass('tel');
                            this.type = 'tel';
                        }
                    }
                }).blur(function() {
                    var i = $(this);
                    if (i.val() == '' || i.val() == i.attr('placeholder')) {
                        if (this.type == 'password') {
                        	this.setAttribute('type', 'text')
                            i.addClass('password');
                        }
                        if (this.type == 'tel') {
                        	this.setAttribute('type', 'text')
                            i.addClass('tel');
                        }
                        i.addClass('placeholder').val(i.attr('placeholder'));
                    }
                }).blur().parents('form').submit(function() {
                    $(this).find('[placeholder]').each(function() {
                        var i = $(this);
                        if (i.val() == i.attr('placeholder'))
                            i.val('');
                    });
                });
            }
        });
    }
});
// this is taking care of the orientation of the mobile.
var mobileOrientationCheck = new Myntra.Utils.mobileOrientation;
var winResizeTimer;
$(window).resize(function() {
    clearTimeout(winResizeTimer);
    winResizeTimer = setTimeout(function() {
    	mobileOrientationCheck.set();
    }, 200);
});

//Monitor hashchanges in page for all nav, search box, pdp zoom and all popups
/*$(document).ready(function(){
    $(window).hashchange( function(event){
        //console.log("1", window.location.hash);
        var hashStr=window.location.hash;
        var processHashTag =  (hashStr != "#!") && (hashStr != "#!default") && (hashStr.indexOf("=") == -1) && (hashStr != "");
        //console.log("3", processHashTag);
        if(processHashTag){
            var id=hashStr.substring(hashStr.indexOf("#!")+2, hashStr.length);
            var obj=$("#"+id).data("lightboxObj");
            if(typeof obj != "undefined"){
                obj.show();
            }
        }
        else{
            //console.log(hashStr.indexOf("=") == -1);
            if(hashStr.indexOf("=") == -1){
                //console.log("4", Myntra.LightBox.lightboxId);
                if(Myntra.LightBox.lightboxId != ""){
                    var obj=$("#"+Myntra.LightBox.lightboxId).data("lightboxObj");
                    if(typeof obj != "undefined"){
                        obj.hide();
                    }
                    Myntra.LightBox.lightboxId="";
                }
            }
        }
        
    });
    
});*/

