Myntra.CouponinfoWidget = function() {
    var markup = ['<div id="couponinfo-widget" class="infobox">',
        ' <div class="close-wrapper"><span class="btn-close"></span></div>',
        ' <div class="mod"></div>',
        '</div>'].join('');

    $('.couponinfowidget-wrapper').html(markup);
    var obj = Myntra.RightMenu($('.couponinfowidget-wrapper')),
        codLimitsFetched = false;
    getCouponInfo()

    function getCouponInfo(){

        var that = this;
        $.get(
            http_loc+"/ajaxPDPCouponWidget.php",
            { styleid: Myntra.PDP.Data.id, price:parseInt(Myntra.PDP.Data.price)}
        ).done(function ( data ) {
                data = $.parseJSON(data);
                console.log('done',data,data.status);
                if(data.status == 'success'){
                    that.data = data.data;
                    that.message = data.message || '';
                    that.numberOfCoupons = data.numberOfCoupons;
                    console.log('num coupons',data.numberOfCoupons,data.abTest);
                    that.abTest = 'test';

                }
                _gaq.push(['_trackEvent','click_for_offer_mobile', 'coupon_info_click', Myntra.PDP.Data.id, 1, true]);
                var bodyMarkup;
                if(data.status == 'nocoupon' && data.message){
                    bodyMarkup=[
                        '   <div class="bd body-text">',
                        '   <p class="msg" style="font-size:17px;padding-top: 20px">',
                            data.message  ,
                        '   </p>',
                        ' </div>'
                    ].join('');
                }else {
                    /* Begin init code */
                    bodyMarkup  = [
                        '<div class="hd">',
                           '<div class="title" style="font-size:18px">OFFER</div>',
                                '</div>',
                                   '<div class="bd body-text">',
                                   '<p class="msg" style="font-size:16px">',
                                   'GET THIS PRODUCT FOR',
                                   '</p>',
                                   '<div class="red" style="font-size:16px"><span style="text-transform:none">Rs.'+ that.data.coupons[0].price  +' </span><span class="coupon-discounted-price"></span></div>',
                                '<br>',
                                   '<div style="font-size:14px">WHEN YOU APPLY COUPON CODE <span class="red">"<span class="coupon-code red">'+ that.data.coupons[0].coupon  +'</span>"</span> IN YOUR BAG<div>',
                                '<br>',
                                   '<div class="line"></div>',
                                   '<div style="font-size:14px; text-align:left;padding-bottom:4px;">DETAILS</div>',
                                   '<div class="coupon-details" style="font-size:12px;color:grey;">',
                                        '<hr>',
                                        '<p class="details" style="text-align:left">',
                                        'Get '+ that.data.coupons[0].coupondiscount  +' off on ',
                                        ' ',(that.data.coupons[0].minimum>0?'a minimum purchase of Rs: '+that.data.coupons[0].minimum:'this product'),
                                        '<br/>',
                                        '</p>',
                                        '<div style="text-align:left"><span class="coupon-tnc">'+ that.data.coupons[0].couponExtraTNC  +'</span> </div>',
                                        '<div style="text-align:left">Valid till <span class="coupon-validity">'+ that.data.coupons[0].enddate  +' </span></div>',
                                        '<div style="text-align:left"><span class="coupon-vat-message">'+vatCouponMessage+' </span></div>',
                                    '</div>',
                                '</div>',
                            '</div>',
                        '</div>'


                    ].join('');  }

                $('.couponinfowidget-wrapper .mod').html(bodyMarkup);
                //bind the close here


            });
    }



    return obj;
};
