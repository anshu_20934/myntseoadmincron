Myntra.PincodeWidget = function(cfg) {

	/* Begin init code */
	var markup = ['<div id="pincode-widget" class="infobox">',
	              '<div class="close-wrapper"><span class="btn-close"></span></div>',
							'<div class="mod">',
								'<div class="hd">',
									'<div class="title">DELIVERY TIME & <span>CoD</span> AVAILABILITY</div>',
								'</div>',
								'<div class="bd body-text">',
									'<p class="msg">',
										'Please enter your PIN Code to check delivery time and Cash on Delivery availability.',
									'</p>',
									'<form>',
										'<div class="row"><input type="tel" placeholder="PIN CODE" max="999999" id="pincode" pattern="[0-9]*" name="pincode" class="small" size="6" maxlength="6" /><div class="err" id="pincode-error">Enter a valid 6 digit PIN Code</div></div>',
										'<button class="primary-btn btn-grey small-btn btn">Check PIN Code</button>',
									'</form>',
									'<div class="result">',
										'<div class="loading"></div>',
										'<div id="outside-network">',
											'<p><span><span class="icon-error"></span>Unfortunately, we do not ship to your PIN Code!</span></p>',
										'</div>',
										'<p id="delivery-time"><span>',
											'<span class="icon-success"></span>It\'ll take <em class="red">6-7 days</em> for us to deliver this product to your PIN Code.<br />',
											'<span class="grey">Actual delivery time may vary depending upon other items in your order.</span>',
										'</span></p>',
										'<p id="cod-available"><span><span class="icon-success"></span>Cash on Delivery is available for your PIN Code<br />for all orders between <em class="red">Rs. 500 - Rs. 10,000</em>.</span></p>',
										'<p id="only-cod-available"><span><span class="icon-alert"></span>ONLY cash on delivery payment option is available for your PIN Code<br />for all orders between <em class="red">Rs. 500 - Rs. 10,000</em>.</span></p>',
										'<p id="cod-unavailable"><span><span class="icon-alert"></span>Cash on delivery payment option is NOT available for your PIN Code.</span></p>',
									'</div>',
								'</div>',
								'<div class="ft"></div>',
							'</div>',
						'</div>'].join('');
	$('.pincodewidget-wrapper').append(markup);
	
	var obj = Myntra.RightMenu($('.pincodewidget-wrapper')),
		codLimitsFetched = false;

	$('#pincode-widget .title .info-icon').remove();
	
	/* Override lightbox's show method to focus on pincode input box */
	var lbShow = obj.show;
	obj.show = function() {
		lbShow();
		$(this).val('');
		resetResults();
		//$('#pincode-widget #pincode').focus();
	};

	$('#pincode-widget #pincode').focus(function() {
		$(this).val('');
		resetResults();
	});

    $("#pincode-widget form button").click(function(){
          var pincode = $('#pincode-widget form').find('#pincode').val();
          if (!pincode || !/^\d{6}$/.test(pincode)) {
               $('#pincode-error').css('visibility', 'visible');
               return false;
          }

        });
	$('#pincode-widget form').submit(function() {
		resetResults();
		var pincode = $(this).find('#pincode').val(),
			loader = $('#pincode-widget .loading');
		/* Validate Pincode for 6 digit number */
		/*if (!pincode || !/^\d{6}$/.test(pincode)) {
			$(this).find('#pincode-error').css('visibility', 'visible');
			return false;
		}*/
		_gaq.push(['_trackEvent','PincodeWidget', pincode, 'CheckPincode', cfg.styleid]);
		loader.fadeIn();
		
		/* Validation successful, sending Ajax Request */
		var request = sendAjax(pincode);
		request.done(function(result) {
			loader.fadeOut();
			if (result.status == 'SUCCESS') {
				setBrowserStorage(result.data);
				showResults(result.data);
			}
		});
		return false;
	});
	/* End init code */
	
	/* Called on Form Submit and in fetchAndStoreJSON, sends ajax request and returns jqxhr object */
	/* When pincode is not give, fetches data for user's default address */
	function sendAjax(pincode) {
		var ajaxUrl = http_loc + '/ajaxPincodeWidget.php?' + getQuryPrameters();
		if (pincode) {
			ajaxUrl += '&pincode=' + pincode;
		}
		
		/* So as not to query Feature Gate with every request */
		if (codLimitsFetched) {
			ajaxUrl += '&gotCod=1';
		}
		
		return $.ajax({
			type: "GET",
			url: ajaxUrl
		});

		function getQuryPrameters() {
			var skuid = $(".flat-size-options .options .mk-size").val() || cfg.skuid;
			var payLoad = {
				styleid : cfg.styleid,
				skuid : skuid,
				availableinwarehouses : $(".flat-size-options .options button[value="+skuid+"]").data().availableinwarehouses || "",
				leadtime : $(".flat-size-options .options button[value="+skuid+"]").data().leadtime || "",
				supplytype : $(".flat-size-options .options button[value="+skuid+"]").data().supplytype || ""
			}
			return decodeURIComponent($.param(payLoad));
		}
	}
	
	/* Fetches data for given pincode or user's default shipping address, sends data to setBrowserStorage */
	function fetchAndStoreJSON(pincode) {
		var request = sendAjax(pincode);
		request.done(function(result) {
			if (result.status == 'SUCCESS') {
				setBrowserStorage(result.data);
			}
		});
	}
	
	/* Stores raw serviceability data in localStorage as a JSON string */
	function setBrowserStorage(json) {
		if (!localStorage || !json.SERVICEABLE.length) return; // Unserviceable pincodes' data is not stored
		try{
			localStorage.setItem('PincodeWidget.ServiceabilityData', JSON.stringify(json)); // Store in persistent localStorage	
		}catch(e){
			return;
		}
		$(window).trigger('myntra.pincodewidget.datachange');
	}
	
	/* Populates the Pincode Widget Infobox */
	function showResults(json) {
		var outsideNetwork = $('#outside-network'),
		deliveryTime = $('#delivery-time'),
		codAvailable = $('#cod-available'),
		onlyCodAvailable = $('#only-cod-available'),
		codUnavailable = $('#cod-unavailable');
	
		/* Insert COD Limits into markup */
		if (!codLimitsFetched && json.codLimits) {
			codAvailable.add(onlyCodAvailable).find('em.red').html(json.codLimits);
			codLimitsFetched = true;
		}
		
		deliveryTime.find('em.red').html(json.DELIVERY_PROMISE_TIME + ' days');
		if ($.inArray('cod', json.SERVICEABLE) > -1) {
			deliveryTime.fadeIn();
			if ($.inArray('on', json.SERVICEABLE) > -1) {
				codAvailable.fadeIn();
				_gaq.push(['_trackEvent','PincodeWidget', pincode, 'Result_COD_1_Online_1_DeliveryTime_' + json.DELIVERY_PROMISE_TIME, cfg.styleid]);
			} else {
				onlyCodAvailable.fadeIn();
				_gaq.push(['_trackEvent','PincodeWidget', pincode, 'Result_COD_1_Online_0_DeliveryTime_' + json.DELIVERY_PROMISE_TIME, cfg.styleid]);
			}
		} else {
			if ($.inArray('on', json.SERVICEABLE) > -1) {
				deliveryTime.add(codUnavailable).fadeIn();
				_gaq.push(['_trackEvent','PincodeWidget', pincode, 'Result_COD_0_Online_1_DeliveryTime_' + json.DELIVERY_PROMISE_TIME, cfg.styleid]);
			} else {
				outsideNetwork.fadeIn();
				_gaq.push(['_trackEvent','PincodeWidget', pincode, 'Result_COD_0_Online_0', cfg.styleid]);
			}
		}
	}
	
	/* Clears the Pincode Widget Infobox */
	function resetResults() {
		$('#pincode-widget .result > p, #pincode-widget .result > div').hide();
		$('#pincode-widget #pincode-error').css('visibility', 'hidden');
	}
	
	/* Expose relevant methods and variables */
	obj.fetchAndStoreJSON = fetchAndStoreJSON;
	obj.initialized = true;
	return obj;
};
