//List-slide-animation class starts here -
Myntra.ListAnimation = function(refobj,cfg) {
    if(typeof refobj != "object") {
        return;
    }
    var obj = Myntra.MkBase();
    obj.refobj = refobj || {};
    var _obj = obj.refobj;
    _obj.cfg = cfg || {};
    _obj.listPos='';
    var animationTime = 700;
    var animationType = "swing";
    var listObjUl=_obj.find('ul.single-step-list');
    var listObjLi=_obj.find('.single-step-list .anim-link');
    var back = _obj.find('.back');
    var mainslidewrapper=_obj.closest('.main-slide-wrapper');
    var mainContent = listObjUl.closest('.slide-main-content');
    var pageOverlay=$('.mk-body-overlay');
    var currentListObj={};	
    var isListOptimization=false;	/* variable for checking if right list optimization has to be performed or not */
    var optimizedInitItems=15;
    var animation = "off";
/* Bellow check is for android and opera which gets messed up due to some css-properties - removing them. */
    if(Modernizr.opera_android!=null) {
        mainContent.addClass('android-anim-fix');
    }
/* this operation reclaculates height of list object, its container, mainslide object and the m-body according to change in height of the list */
    _obj.recalculateObjHeight = function(){
    	//mainslidepagewrapper.css({'z-index':10000});
    	var mbodyheight=$('.m-body').outerHeight();
    	var bodyheight=$('body').outerHeight();
    	var contentheight=_obj.outerHeight();
    	var minHeight=screen.height;
    	var heightArray=[contentheight,minHeight];
    	if(_obj.listPos=="right"){
    		var thisHeight = currentListObj.find('div.link-details').outerHeight() + 50; 	
    		mainContent.css({'height': thisHeight});
    		currentListObj.closest('.content-slide-wrapper').css({'height':'auto'});
    		heightArray=[contentheight,minHeight,thisHeight];
    	}
    	var max_of_heightArray = Math.max.apply(Math, heightArray);
    	max_of_heightArray=max_of_heightArray;
    	mainslidewrapper.css({'height':max_of_heightArray});
    	pageOverlay.css({'height':max_of_heightArray});
    	$('.m-body').css({'height':max_of_heightArray});
    	_obj.find('.bd.mk-cf').css({'height':'auto'});
    	//Myntra.Utils.scrollToPos(0,0);
    }
/* method for animating the list on left side */
    _obj.animateLeft = function(currentLi) {
    	if(_obj.listPos!="right"){
    		var sec_array2=[];
/* checking the list optimazation required in few places before animating (specially for ios performance problems with filter having huge lists) */
    		if(animation=="on"){
    		var isListOptimization = (typeof(currentLi.attr('data-isOptimized'))!="undefined" && screen.width<780) ? currentLi.attr('data-isOptimized'):false;
    		if(isListOptimization){
           		isListOptimization_obj = currentLi.find("."+currentLi.attr('data-optimizedContainer'));
        		var this_obj = isListOptimization_obj;
        			var sec_array2 = isListOptimization_obj.find('a').slice(optimizedInitItems);
        			//isListOptimization_obj.find('a').slice(optimizedInitItems).remove();
                    isListOptimization_obj.find('a').slice(optimizedInitItems).css({'display':'none'});
        	}
    		}
    		_obj.listPos="right";
    		var detailsDiv = currentLi.find('div.link-details');
/* in general right part of the list is hidden and when list animates to right we have to show it */
    		detailsDiv.show();
/* animating diffretly for aniamating browsers and non animating ones */
    		/* checking the list optimazation required in few places before animating (specially for ios performance problems with filter having huge lists) */
    		if(animation=="on"){
    		if(Modernizr.opera_android!=null || Modernizr.ie_browser!=null){
    			mainContent.css({'left' : "-100%"});
    		}else if(Modernizr.iphone!=null){
/* only for iphone as it is doing two diffrent things.. not animating rightmenu and animating list */
    			mainContent.addClass('anim-left-iphone');
    		}else {
    			mainContent.addClass('anim-left');
    		}
    		}else {
    			mainContent.css({'left' : "-100%"});
    		}
    		var thisHeight = currentLi.find('div.link-details').outerHeight()+30; 	
			mainContent.css({'height': thisHeight});
			currentLi.closest('.content-slide-wrapper').css({'height':'auto'});
			$('.main-slide-wrapper').css({'height': currentLi.closest('.content-slide-wrapper').outerHeight()+7});
			currentListObj=currentLi;
/* after the animation is finished we do the optimization process end */
			if(animation=="on"){
			setTimeout(function(){
    		if(!isListOptimization){
    			_obj.recalculateObjHeight();
    		}
			back.show();
        	if(isListOptimization){
        			//isListOptimization_obj.append(sec_array2);
                    isListOptimization_obj.find('a').css({'display':'block'});
        			var thisHeight = currentLi.find('div.link-details').outerHeight()+30; 	
        			mainContent.css({'height': thisHeight});
        			currentLi.closest('.content-slide-wrapper').css({'height':'auto'});
        			$('.main-slide-wrapper').css({'height': currentLi.closest('.content-slide-wrapper').outerHeight()+7});
        			_obj.recalculateObjHeight();
        		}
    		},animationTime)
		}else {
			_obj.recalculateObjHeight();
			back.show();
		}
    	}
        return false;
    };
/* method for animating the list rightwards */
    _obj.animateRight = function(e) {
    	if(_obj.listPos!="left"){
    		var sec_array2=[];
    		if(animation=="on"){
    		/* checking the list optimazation required in few places before animating (specially for ios performance problems with filter having huge lists) */
    		var isListOptimization = (typeof(currentListObj.attr) === "function" && typeof(currentListObj.attr('data-isOptimized'))!="undefined" && screen.width<780) ? currentListObj.attr('data-isOptimized'):false;
    		if(isListOptimization){
    			isListOptimization_obj = currentListObj.find("."+currentListObj.attr('data-optimizedContainer'));
        		var this_obj = isListOptimization_obj;
        			var sec_array2 = isListOptimization_obj.find('a').slice(optimizedInitItems);
        			isListOptimization_obj.find('a').slice(optimizedInitItems).remove();
        	}
    		}
    		_obj.listPos="left";
    		/* animating diffretly for aniamating browsers and non animating ones */
    		if(animation=="on"){
    		if(Modernizr.opera_android!=null || Modernizr.ie_browser!=null){
    			mainContent.css({'left' : "0px"});
    		}else if(Modernizr.iphone!=null){
/* only for iphone as it is doing two diffrent things.. not animating rightmenu and animating list */
    			mainContent.removeClass('anim-left-iphone');
    		}else {
    			mainContent.removeClass('anim-left');
    		}
    		}else {
    			mainContent.css({'left' : "0px"});
    		}
    		mainContent.css({height: 'auto'});
    		var thisHeight =_obj.find('ul.single-step-list').height();
    		$('.content-slide-wrapper.content-active').css({'height':'auto'});
    		//$('.main-slide-wrapper').css({'height': $('.content-slide-wrapper.content-active').outerHeight()+5});
    		back.hide();
    		//_obj.trigger('right-animation-end');
    		if(animation=="on"){
    		setTimeout(function(){
        	if(isListOptimization){
    			isListOptimization_obj.append(sec_array2);
       			_obj.recalculateObjHeight();
    		}
        	Myntra.Utils.scrollToPos(0);
			},animationTime)
    		}
    	}
        return false;
    };
    _obj.reinit = function(){ 	
    	_obj.beforeBack();
    	_obj.animateRight();
    	_obj.find('.link-details').hide();
        if($('.lb-error-msg').length > 0) {
            $('.lb-error-msg').html('');
        }
    }
    function init() {
        var animlink = _obj.find('.single-step-list .anim-link');
        $(animlink).fastClick(function(){
            _obj.animateLeft($(this).parent());
        });
        listObjLi.parent().children('span').on("click", function(){
        	_obj.animateLeft($(this).parent());
        });
        back.fastClick(function(e) {
        	_obj.beforeBack(e);
        	_obj.animateRight(e);
        	_obj.find('.link-details').hide();
            if($('.lb-error-msg').length > 0) {
                $('.lb-error-msg').html('');
            }
            e.stopPropagation();
            Myntra.Utils.preventDefault(e);
        });
    }
    _obj.beforeBack = function() {
        if (typeof _obj.cfg !="undefined" && typeof _obj.cfg.beforeBack === 'function') {
            _obj.cfg.beforeBack.call(_obj);
        }
    };
    init();
    return _obj;
};

// slide-content-animation here -
Myntra.RightMenu = function(refobj,cfg,itslistObj) {
    if(typeof refobj != "object") {
        return;
    }
    var obj = Myntra.MkBase();
    obj.refobj = refobj || {};
    var _obj = obj.refobj;
    _obj.cfg = cfg || {};
    _obj.cfg.isWidthAspect = typeof(_obj.cfg.isWidthAspect) !="undefined" ? _obj.cfg.isWidthAspect : true;
    _obj.listPos='';
    var animationTime = 700;
    var animationType = "swing";
    var listObjUl=_obj.find('ul.single-step-list');
    var mainslidewrapper=_obj.closest('.main-slide-wrapper');
    var close = _obj.find('.close-wrapper');
    var mainContent = listObjUl.closest('.slide-main-content');
    var mod = _obj.find('.mod');
    var pageOverlay=$('.mk-body-overlay');
    var progress=null;
    var slideInClass="slidein3d-anim";
    var slideInInitClass="slidein3d-anim-init";
    var animation = "off";
    if(animation=="on"){
    //animation for left right starting here
    if(Modernizr.opera_android!=null) {
        mainContent.addClass('android-anim-fix');
    }
    }
    _obj.recalculateObjHeight = function(){
    	//mainslidepagewrapper.css({'z-index':10000});
    	var mbodyheight=$('.m-body').outerHeight();
    	var bodyheight=$('body').outerHeight();
    	var contentheight=_obj.outerHeight();
    	var minHeight=600;
    	var screenHeight=screen.height;
    	var heightArray=[contentheight,minHeight,screenHeight];
    	var max_of_heightArray = Math.max.apply(Math, heightArray);
    	max_of_heightArray=max_of_heightArray;
    	mainslidewrapper.css({'height':max_of_heightArray});
    	pageOverlay.css({'height':max_of_heightArray});
    	$('.m-body').css({'height':max_of_heightArray});
    	_obj.find('.bd.mk-cf').css({'height':'auto'});
    	//alert(minHeight+"  "+contentheight+"    "+ max_of_heightArray)
    	Myntra.Utils.scrollToPos(0,0);
    }
    _obj.reapplyPageHeight = function(){
    	$('.m-body').css({'height':'auto'});
    	Myntra.Utils.scrollToPos(0,0);
    }
    _obj.recalculateObjWidth = function(){
    	_obj.aspectRatio= (typeof($('.pseudo-rightmenuwidthaspect-container'))!="undefined" && _obj.cfg.isWidthAspect) ? ($('.pseudo-rightmenuwidthaspect-container').css('opacity')) : 1;
    	_obj.mainWidth = parseInt(_obj.cfg.width || 80)*_obj.aspectRatio + "%";
    	mainslidewrapper.css({'width':_obj.mainWidth});
    	pageOverlay.css({'height':screen.width});
    }
    function init() {
/* here we check which platform and browser it is and we change the init class and the animation class variables which is responsible for all animations
the inner containers and other animation for these containers on which this class is applied is taken care from the css rightmenu.css*/
    	if(animation=="on"){
        if(Modernizr.opera_android!=null || Modernizr.ie_browser!=null || Modernizr.iphone!=null || Modernizr.firefox!=null){
        	slideInClass="slidein-noanim";
            slideInInitClass="slidein-noanim-init";
        }else if(Modernizr.android!=null) {
        	slideInClass="slidein2d-anim";
            slideInInitClass="slidein2d-anim-init";
        }else {
        	slideInClass="slidein3d-anim";
            slideInInitClass="slidein3d-anim-init";
        }
    	}else{
        	slideInClass="slidein-noanim";
            slideInInitClass="slidein-noanim-init";
    	}
        if(!mainslidewrapper.hasClass(slideInInitClass)) {
            mainslidewrapper.addClass(slideInInitClass);
        }
        var closeslide = _obj.find('.close-wrapper');
        closeslide.on('click',function(e) {
        	_obj.find('.link-details').hide();
            _obj.hide();
        });
        closeslide.fastClick(function(e) {
        	_obj.find('.link-details').hide();
        	_obj.hide();
        });

    }
//animation for left right ending here-----
//animation for show hide starting here
    _obj.show=function() {
    	$.pageslide.close();
    	_obj.recalculateObjWidth();
    	_obj.recalculateObjHeight();
        var winResizeSlideBox;
/* only while showing the rightmenu we bind this resize event 
 * we unbind it everytime we hide it */
        $(window).bind('resize.listOb',function() {
            clearTimeout(winResizeSlideBox);
            winResizeSlideBox = setTimeout(function() {
                if(mobileOrientationCheck.isChanged()) {
                	if(typeof(itslistObj)!="undefined"){
                		itslistObj.recalculateObjHeight();
                	}else {

                		_obj.recalculateObjHeight();
                	}
                }
            }, 500);
        });
/* we have to check this conditon for animation or no animation brwosers and put this conditional css */
        if(animation=="on"){
		if(Modernizr.opera_android!=null || Modernizr.ie_browser!=null || Modernizr.iphone!=null || Modernizr.firefox!=null){
			mainslidewrapper.css({'right' : "0"});
		}else{
			mainslidewrapper.css({'left' : "100%"});
		}
        }else{
        	mainslidewrapper.css({'right' : "0"});
        }
    	//$('.content-slide-wrapper').css({'top':$(window).scrollTop()});
        var _refobj = _obj;
        var _cfg = _obj.cfg;
        var _fx = _obj.fx;
        if(_refobj.hasClass('content-active')){
            _obj.hide();
            return;
        }
        if($('.content-slide-wrapper.content-active').length>0) {
            _obj.hide(function() {
                _obj.beforeShow();
                	_refobj.removeClass('content-inactive');
                	_refobj.addClass('content-active');
                	pageOverlay.show();
                	/* we have to check this conditon for animation or no animation brwosers and put this conditional css */
            		
                	if(animation == "on" || Modernizr.opera_android!=null || Modernizr.ie_browser!=null || Modernizr.iphone!=null || Modernizr.firefox!=null){
            			mainslidewrapper.css({'right' : "0"});
            		}
                    _refobj.parent().addClass(slideInClass);
                    _obj.afterShow();
            });    
        }else {
            _obj.beforeShow();
            	_refobj.removeClass('content-inactive');
            	_refobj.addClass('content-active');
            	pageOverlay.show();
                _refobj.parent().addClass(slideInClass);
/* we have to check this conditon for animation or no animation brwosers and put this conditional css */
        		if(animation == "on" || Modernizr.opera_android!=null || Modernizr.ie_browser!=null || Modernizr.iphone!=null || Modernizr.firefox!=null){
        			mainslidewrapper.css({'right' : "0"});
        		}
                _obj.afterShow();
        }

    };
    _obj.hide=function(fx){   
        var _refobj = _obj;
        var _cfg = _obj.cfg;
        var _fx = fx;
        $('.content-slide-wrapper.content-active').each(function(){
            _obj.beforeHide();
            _refobj.parent().removeClass(slideInClass);
            var that=this;
            setTimeout(function(){
                $(that).removeClass('content-active');
                $(that).addClass('content-inactive');
                _refobj.removeClass('content-active');
                _refobj.addClass('content-inactive');
                if(typeof _fx === "function"){
                    _fx();   
                   }
           },animationTime);
            if(typeof this.afterHide != "undefined"){
                _obj.afterHide();
            }
            _obj.reapplyPageHeight();
/*unbinding the resize event */
            $(window).unbind('resize.listOb');
            pageOverlay.hide();
            if(typeof(itslistObj)!="undefined"){
                itslistObj.animateRight();	
            }
        });
    };
    _obj.beforeShow = function() {
        if (typeof _obj.cfg !="undefined" && typeof _obj.cfg.beforeShow === 'function') {
            _obj.cfg.beforeShow.call(_obj);
        }
    };
    _obj.afterShow = function() {
        if (typeof _obj.cfg !="undefined" && typeof _obj.cfg.afterShow === 'function') {
            _obj.cfg.afterShow.call(_obj);
        }
    };
    _obj.beforeHide = function() {
        if (typeof _obj.cfg !="undefined" && typeof _obj.cfg.beforeHide === 'function') {
            _obj.cfg.beforeHide.call(_obj);
        }
    };
    _obj.afterHide = function() {
        if (typeof _obj.cfg !="undefined" && typeof _obj.cfg.afterHide === 'function') {
            _obj.cfg.afterHide.call(_obj);
        }
    };
    _obj.beforeRight = function() {
        if (typeof _obj.cfg !="undefined" && typeof _obj.cfg.beforeRight === 'function') {
            _obj.cfg.beforeRight.call(__obj);
        }
    };
    _obj.showLoading = function() {
        if (!$(_obj).find('.loading').length) {
            mod.append('<div class="loading"></div>');
        }
        if (!progress) {
            progress = _obj.find('.loading');
        }
        progress.show();
    };
    _obj.hideLoading = function() {
        progress && progress.hide();
    };
//animation for show hide ending here-----
    init();
    return _obj;
};