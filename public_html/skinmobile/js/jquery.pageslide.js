/*
 * jQuery pageSlide
 * Version 2.0
 * http://srobbin.com/jquery-pageslide/
 *
 * jQuery Javascript plugin which slides a webpage over to reveal an additional interaction pane.
 *
 * Copyright (c) 2011 Scott Robbin (srobbin.com)
 * Dual licensed under the MIT and GPL licenses.
*/
var scroller;
(function($){
    // Convenience vars for accessing elements
    var $body = $('.mk-page-container'), $page = $('.mk-page'), $bodyOverlay = $('.mk-body-overlay'), $icon = $('.m-body .menu-icon'), $pageslide = $('#nav');
    $("#nav").on('touchmove', function (e) { Myntra.Utils.preventDefault(e);; });
    var _sliding = false,   // Mutex to assist closing only once
        _lastCaller;        // Used to keep track of last element to trigger pageslide
    
    // If the pageslide element doesn't exist, create it
    if( $pageslide.length == 0 ) {
         $pageslide = $('<div />').attr( 'id', 'nav' )
                                  .css( 'display', 'none' )
                                  .appendTo( $('body') );
    }
    
    /*
     * Private methods 
     */
    function _load( url, useIframe ) {
        // Are we loading an element from the page or a URL?
        /*if ( url.indexOf("#") === 0 ) {                
            // Load a page element                
            $(url).clone(true).appendTo( $pageslide.empty() ).show();
        } else {
            // Load a URL. Into an iframe?
            if( useIframe ) {
                var iframe = $("<iframe />").attr({
                                                src: url,
                                                frameborder: 0,
                                                hspace: 0
                                            })
                                            .css({
                                                width: "100%",
                                                height: "100%"
                                            });
                
                $pageslide.html( iframe );
            } else {
                $pageslide.load( url );
            }
            
            $pageslide.data( 'localEl', false );
            
        }*/
    }
    
    // Function that controls opening of the pageslide
    function _start( direction, speed ) {
        var slideWidth = $pageslide.outerWidth( true ),
            bodyAnimateIn = {},
            slideAnimateIn = {};
        $('body, html').scrollTop(0);    

        // If the slide is open or opening, just ignore the call
        if( _sliding ) return;            
        _sliding = true;
                                                                    
        switch( direction ) {
            case 'left':
                $pageslide.css({ left: '-' + slideWidth + 'px' });
                $body.css({ left: '0px auto', 'margin': '' });
                //bodyAnimateIn['margin-left'] = '-=' + slideWidth;
                //slideAnimateIn['right'] = '+=' + slideWidth;
                break;
            default:
                $pageslide.css({ left: '0px'});
                $body.css({ left : slideWidth + 'px'});
                //bodyAnimateIn['margin-left'] = '+=' + slideWidth;
                //slideAnimateIn['left'] = '+=' + slideWidth;
                break;
        }

        $page.css({ left: '0px auto' });
        $icon.addClass('selected');
        $bodyOverlay.css({'display' : 'block', 'height' : screen.height, 'width' :'100%'});
        //$(".m-body,body").css({'height' : screen.height});
        //$body.css({'height' : screen.height});
        //$bodyOverlay.height($page.height() + 20);//Adding 20px height offset due to some padding overheads

        
        $pageslide.show();
        _sliding = false;
        if($("#nav").attr("data-scrollinit") != "true"){
            scroller = new iScroll('nav');
            $("#nav").attr("data-scrollinit", "true");
        }
        else{
            scroller.refresh();
        }
        /*
        if(Modernizr.csstransitions){
            $body.css(bodyAnimateIn);
            $pageslide.show().css(slideAnimateIn);
            $pageslide.show();
            _sliding = false;
        }
        else{
             Animate the slide, and attach this slide's settings to the element
            $body.animate(bodyAnimateIn, speed);
            $pageslide.show();
            $pageslide.show().animate(slideAnimateIn, speed, function() {
                _sliding = false;
            });
        } 
        */           
    }
      
    /*
     * Declaration 
     */
    $.fn.pageslide = function(options) {
        var $elements = this;
        //Replacing on click with fastbutton
        //Myntra.Utils.extendClickEvent($elements, function(e) {            
        $($elements).fastClick(function(e) {            
            var $self = $(this),
                settings = $.extend({ href: "" }, options);
            
            // Prevent the default behavior and stop propagation
            Myntra.Utils.preventDefault(e);;
        	//e.cancelBubble is supported by IE -
            // this will kill the bubbling process.
            e.cancelBubble = true;
            e.returnValue = false;
     
            //e.stopPropagation works only in Firefox.
            if ( e.stopPropagation ) e.stopPropagation();
            
            if ( $pageslide.is(':visible') && $self[0] == _lastCaller ) {
                // If we clicked the same element twice, toggle closed
                $.pageslide.close();
            } else {                 
                // Open
                $.pageslide( settings );

                // Record the last element to trigger pageslide
                _lastCaller = $self[0];
            }

            Myntra.Utils.hideTooltipAndRearrangeHeader();
        });

        _gaq.push(['_trackEvent', 'header', 'navmenu', Myntra.Data.pageName]);
    };
    
    /*
     * Default settings 
     */
    $.fn.pageslide.defaults = {
        speed:      200,        // Accepts standard jQuery effects speeds (i.e. fast, normal or milliseconds)
        direction:  'right',    // Accepts 'left' or 'right'
        modal:      false,      // If set to true, you must explicitly close pageslide using $.pageslide.close();
        iframe:     true,       // By default, linked pages are loaded into an iframe. Set this to false if you don't want an iframe.
        href:       null        // Override the source of the content. Optional in most cases, but required when opening pageslide programmatically.
    };
    
    /*
     * Public methods 
     */
    
    // Open the pageslide
    $.pageslide = function( options ) {        
        $("body,html").addClass("iosorientationfix");
        // Extend the settings with those the user has provided
        var settings = $.extend({}, $.fn.pageslide.defaults, options);
        // Are we trying to open in different direction?
        if( $pageslide.data( 'direction' ) && $pageslide.data( 'direction' ) != settings.direction) {
            $.pageslide.close(function(){
                _load( settings.href, settings.iframe );
                _start( settings.direction, settings.speed );
            });
        } else {                
            _load( settings.href, settings.iframe );
            _start( settings.direction, settings.speed );
        }
        
        $pageslide.data( settings );
    }
    
    // Close the pageslide
    $.pageslide.close = function( callback ) {
        
        $("body,html").removeClass("iosorientationfix");
        var $pageslide = $('#nav'),
            slideWidth = $pageslide.outerWidth( true ),
            speed = $pageslide.data( 'speed' ),
            bodyAnimateIn = {},
            slideAnimateIn = {}
                        
        // If the slide isn't open, just ignore the call
        if( $pageslide.is(':hidden') || _sliding ) return;            
        _sliding = true;
        
        switch( $pageslide.data( 'direction' ) ) {
            case 'left':
                $pageslide.css({'margin-left': '0px'});
                $body.css({'right' : slideWidth, left: '-' + slideWidth + 'px', 'position' : 'relative', 'margin': '0px auto'});
                //bodyAnimateIn['margin-left'] = '+=' + slideWidth;
                //slideAnimateIn['right'] = '-=' + slideWidth;
                break;
            default:
                $pageslide.css({'right' : '0px', 'left': '-' + slideWidth + 'px'});
                $body.css({'left' : '0px', 'position' : 'relative', 'margin' : '0px auto'});
                //bodyAnimateIn['margin-left'] = '-=' + slideWidth;
                //slideAnimateIn['left'] = '-=' + slideWidth;
                break;
        }
        $page.css({ left: '0px auto', 'position': 'relative' });
        $icon.removeClass('selected');

        /* To reset nav back to original state */
        $(".mk-nav-ul").hide();
        $(".mk-nav-level-1").show();
        //alert("1");
        /*var navItems=$("#list").find(":visible").filter(".mk-nav-ul");
        if(!navItems.hasClass("mk-level-1")){
            navItems.find("li:first a").trigger("click");
            //navItems.find("li:first a").get(0).click();
            //alert("3");
            scroller.refresh();
        }*/
        try {
        scroller.refresh();
        }catch(e)
        {}
        
        $("#scroller").css("-webkit-transform", "translate3d(0px, 0px, 0px) scale(1)");
        /* To reset nav back to original state */
        _sliding = false;
        $pageslide.hide();
	    $bodyOverlay.hide();
    }
    
    /* Events */
    
    // Don't let clicks to the pageslide close the window
    $pageslide.click(function(e) {
        e.stopPropagation();
    });
/*
    // Close the pageslide if the document is clicked or the users presses the ESC key, unless the pageslide is modal
    $(document).bind('click keyup', function(e) {
        // If this is a keyup event, let's see if it's an ESC key
        if( e.type == "keyup" && e.keyCode != 27) return;
        
        // Make sure it's visible, and we're not modal        
        if( $pageslide.is( ':visible' ) && !$pageslide.data( 'modal' ) ) {            
            $.pageslide.close();
        }
    });
*/
    
})(jQuery);
