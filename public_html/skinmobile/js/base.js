
Myntra.MkBase = function() {
    var obj = {},
        props = {},
        _data = {};

    obj.set = function(key, val) {
        props[key] = val;
    };

    obj.get = function(key) {
        return props[key];
    };

    obj.data = function(key, val) {
        if (typeof key === 'object') {
            $.extend(true, _data, key);
            return obj;
        }
        if (typeof key === 'undefined') {
            return _data;
        }
        if (typeof val === 'undefined') {
            return _data[key];
        }
        _data[key] = val;
    };

    obj.showLoading = function(){};
    obj.hideLoading = function(){};

    obj.onAjaxError = function(resp, name) {
    };

    obj.onAjaxSuccess = function(resp, name) {};

    obj.ajax = function(params, name, method) {
        method = method || "GET";
        $.ajax({
            type: method,
            url: obj.get("ajax_url"),
            data: params,
            dataType: "json",
            beforeSend:function() {
                obj.showLoading();
            },
            success: function(resp) {
                if (!resp) {
                    alert('Invalid JSON data returned from the Server');
                    return;
                }
                (resp.status === 'SUCCESS') ? obj.onAjaxSuccess(resp, name) : obj.onAjaxError(resp, name);
            },
            complete: function(xhr, status) {
                obj.hideLoading();
            },
            error: function(xhr, textStatus, errorThrown) {
            }
        });
    };

    return obj;
};

// es5-shim for bind
if (!Function.prototype.bind) {
    Function.prototype.bind = function(that) {
        var slice = Array.prototype.slice,
        target = this,
        args = slice.call(arguments, 1);
        return function() {
            return target.apply(that, args.concat(slice.call(arguments)));
        };
    };
}


/* Simple JavaScript Inheritance
 * By John Resig http://ejohn.org/
 * MIT Licensed.
 * http://ejohn.org/blog/simple-javascript-inheritance/
 */
// Inspired by base2 and Prototype
(function() {
    var initializing = false, fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/;
    // The base Class implementation (does nothing)
    Myntra.Class = function(){};

    // Create a new Class that inherits from this class
    Myntra.Class.extend = function(prop) {
        var _super = this.prototype;

        // Instantiate a base class (but only create the instance,
        // don't run the init constructor)
        initializing = true;
        var prototype = new this();
        initializing = false;

        // Copy the properties over onto the new prototype
        for (var name in prop) {
            // Check if we're overwriting an existing function
            prototype[name] = typeof prop[name] == "function" &&
                typeof _super[name] == "function" && fnTest.test(prop[name]) ?
            (function(name, fn){
                return function() {
                    var tmp = this._super;

                    // Add a new ._super() method that is the same method
                    // but on the super-class
                    this._super = _super[name];

                    // The method only need to be bound temporarily, so we
                    // remove it when we're done executing
                    var ret = fn.apply(this, arguments);
                    this._super = tmp;

                    return ret;
                };
            })(name, prop[name]) :
            prop[name];
        }

        // The dummy class constructor
        function Class() {
            // All construction is actually done in the init method
            if ( !initializing && this.init )
                this.init.apply(this, arguments);
        }

        // Populate our constructed prototype object
        Class.prototype = prototype;

        // Enforce the constructor to be what we expect
        Class.prototype.constructor = Class;

        // And make this class extendable
        Class.extend = arguments.callee;

        return Class;
    };
}());



(function() { "use strict";
Myntra.Base = Myntra.Class.extend({
    init: function() {
        this._conf = {};
        this._callbacks = {};
    },

    // method read/write configurations/settings
    conf: function(key, val) {
        if ($.isPlainObject(key)) {
            $.extend(true, this._conf, key);
            return this;
        }
        if (arguments.length === 0) {
            return this._conf;
        }
        if (arguments.length === 1) {
            return this._conf[key];
        }
        this._conf[key] = val;
    }
});
}());

