var filtersIconEl=$(".filters-icon");
filterProperties["brands"]={
	getURLParam: function(){
		var brandsStr=configArray["brands"].join(":");
		return (brandsStr != "")?"brands="+unescape(brandsStr):"";
	},
	processURL: function(str){
		configArray["brands"]=Myntra.Search.getFilterValues(unescape(str));
		return true;
	},
	updateUI: function(){
		/*// unselect all brands
		$(".mk-brands-filters .mk-labelx_check").removeClass("checked").addClass("unchecked");

		// update UI selection for brands in configArray
		for(var brand=0;brand<configArray["brands"].length; brand++){
			var selector=configArray["brands"][brand].toLowerCase().replace(/\W+/g, "") + "-brand-filter";
			$("."+selector).removeClass("unchecked").addClass("checked");
		}

		$("[data-target=brands-types] .selected").html(Myntra.Utils.truncateStr(configArray["brands"].join(", "))); */

		Myntra.Search.Filters.updateUI("brands");
		return true;
	},
	setToDefault: function(){
		configArray["brands"]=[];
		return true;
	},
	resetParams: function(){
		return true;
	},
    updateFilterVisibility: function(filters){
        if(!Myntra.Search.Data.userTriggered || (Myntra.Search.Data.filterClickedName != "" && Myntra.Search.Data.filterClickedName!="brands") || (Myntra.Search.Data.filterClickedName != "" && Myntra.Search.Data.filterClickedName=="brands" && configArray["brands"].length == 0)){
            $(".mk-brands-filters .mk-labelx_check").each(function (index,element){
                if(typeof filters.brands_filter_facet == "undefined" || typeof filters.brands_filter_facet[$(element).attr('data-value')] == 'undefined'){
                    $(element).find(".mk-filter-product-count").hide();
                    $(element).addClass('disabled');
                }else{
                    $(element).find(".mk-filter-product-count").show().html("["+filters.brands_filter_facet[$(element).attr('data-value')]+"]")
                    $(element).removeClass("disabled");
                }
            });
        }
    },
    setFilterClickedName: function(){
        Myntra.Search.Data.filterClickedName = "brands";
	},
	updateClearFilterUI: function(){
		var resetCondition = (configArray["brands"].length >= 1);
		Myntra.Search.Filters.updateClearFiltersUI("brands", resetCondition);
	}
};
filterProperties["articletypes"]={
    getURLParam: function(){
        var brandsStr=configArray["articletypes"].join(":");
        return (brandsStr != "")?"articletypes="+unescape(brandsStr):"";
    },
    processURL: function(str){
        configArray["articletypes"]=Myntra.Search.getFilterValues(unescape(str));
        return true;
    },
    updateUI: function(){
        /*// unselect all brands
        $(".mk-articletypes-filters .mk-labelx_check").removeClass("checked").addClass("unchecked");
        // update UI selection for brands in configArray
        for(var articletype=0;articletype<configArray["articletypes"].length; articletype++){
            var selector=configArray["articletypes"][articletype].toLowerCase().replace(/\W+/g, "") + "-at-filter";
            $("."+selector).removeClass("unchecked").addClass("checked");
        }

        $("[data-target=articletypes-types] .selected").html(Myntra.Utils.truncateStr(configArray["articletypes"].join(", "))); */

		Myntra.Search.Filters.updateUI("articletypes");
        return true;
    },
    setToDefault: function(){
        configArray["articletypes"]=[];
        return true;
    },
    resetParams: function(){
        return true;
    },
    updateFilterVisibility: function(filters){
        if(!Myntra.Search.Data.userTriggered || (Myntra.Search.Data.filterClickedName != "" && Myntra.Search.Data.filterClickedName!="articletypes") || (Myntra.Search.Data.filterClickedName != "" && Myntra.Search.Data.filterClickedName=="articletypes" && configArray["articletypes"].length == 0)){
            $(".mk-articletypes-filters .mk-labelx_check").each(function (index,element){
                if(typeof filters.global_attr_article_type_facet == "undefined" || typeof filters.global_attr_article_type_facet[$(element).attr('data-value')] == 'undefined'){
                    $(element).find(".mk-filter-product-count").hide();
                    $(element).addClass('disabled');
                }else{
                    $(element).find(".mk-filter-product-count").show().html("["+filters.global_attr_article_type_facet[$(element).attr('data-value')]+"]");
                    $(element).removeClass("disabled");
                }
            });
        }
    },
    setFilterClickedName: function(){
        Myntra.Search.Data.filterClickedName = "articletypes";
    },
    updateClearFilterUI: function(){
    	var resetCondition = (configArray["articletypes"].length >= 1);
		Myntra.Search.Filters.updateClearFiltersUI("articletypes", resetCondition);
    }
};
filterProperties["sizes"]={
	getURLParam: function(){
		var sizesStr=configArray["sizes"].join(":");
		return (sizesStr != "")?"sizes="+unescape(sizesStr):"";
	},
	processURL: function(str){
		configArray["sizes"]=Myntra.Search.getFilterValues(unescape(str));
		return true;
	},
	updateUI: function(){
		/*// unselect all sizes
		$(".mk-sizes-filters .mk-labelx_check").removeClass("checked").addClass("unchecked");
		
		// update UI selection for sizes in configArray
		for(var size=0;size<configArray["sizes"].length;size++){
			var selector=configArray["sizes"][size].toLowerCase().replace(/\W+/g, "") + "-sizes-filter";
			$("."+selector).removeClass("unchecked").addClass("checked");
		}

		$("[data-target=sizes-types] .selected").html(Myntra.Utils.truncateStr(configArray["sizes"].join(", "))); */

		Myntra.Search.Filters.updateUI("sizes");
		return true;
	},
	setToDefault: function(){
		configArray["sizes"]=[];
		return true;
	},
	resetParams: function(){
		return true;
	},
    updateFilterVisibility: function(filters){
        if(!Myntra.Search.Data.userTriggered || (Myntra.Search.Data.filterClickedName != "" && Myntra.Search.Data.filterClickedName!="sizes") || (Myntra.Search.Data.filterClickedName != "" && Myntra.Search.Data.filterClickedName=="sizes" && configArray["sizes"].length == 0)){
            $(".mk-sizes-filters .unchecked").remove();
            $("#sizes-types-wrapper").removeAttr("data-clickhandlers");
            var showSizeFilter=false;
            
            // Check if only one size available
            var size_count=0;
            for(var i in filters.sizes_facet){
                size_count++;
            }
            if(size_count > 1){
            	if($.inArray("sizes",dynamicFilters) == -1){
            		dynamicFilters.push("sizes");
            	}
                for(var i in filters.sizes_facet){
                    showSizeFilter=true;
                    var checked = "unchecked";
                    if($(".mk-sizes-filters ."+i.toLowerCase().replace(/\W+/g, "")+'-sizes-filter').length > 0){
                        checked = "checked";
                        $(".mk-sizes-filters ."+i.toLowerCase().replace(/\W+/g, "")+'-sizes-filter').remove();
                    }
                    var labelHtml = '<label class="mk-labelx_check '+checked+' '+i.toLowerCase().replace(/\W+/g, "")+'-sizes-filter" data-key="sizes" data-value="'+i+'"><span class="cbx"></span>'+i.toUpperCase().replace(/ /g,"")+'</label>';
                    if(typeof sizeScrollPane != "undefined"){
                        $(".mk-sizes-filters").append(labelHtml);
                    }
                    else{
                        $(".mk-sizes-filters").append(labelHtml);
                    }
                }
            }

            $(".mk-sizes-filters .mk-labelx_check").fastClick(function(e){
            	Myntra.Search.Filters.genericFilterClick(e);
            });

            if(!showSizeFilter){
				$("[data-name='Sizes']").hide();
            	if($.inArray("sizes",dynamicFilters) != -1){
            		Myntra.Search.Utils.removeValueFromArray("sizes",dynamicFilters);
           	 	}
            }
		    else{
				$("[data-name='Sizes']").show();
		    }
            filterProperties["sizes"].updateUI();
        }
    },
    setFilterClickedName: function(){
        Myntra.Search.Data.filterClickedName = "sizes";
    },
	updateClearFilterUI: function(){
		var resetCondition = (configArray["sizes"].length >= 1);
		Myntra.Search.Filters.updateClearFiltersUI("sizes", resetCondition);
	}
};
filterProperties["gender"]={
	getURLParam: function(){
		var genderStr=configArray["gender"].join(":");
		return (genderStr != "")?"gender="+unescape(genderStr):"";
	},
	processURL: function(str){
		configArray["gender"]=Myntra.Search.getFilterValues(unescape(str));
		return true;
	},
	updateUI: function(){
		/*// unselect all gender
		$(".mk-gender-filters .mk-labelx_check").removeClass("checked").addClass("unchecked");
								
		// update UI selection for gender in configArray
		for(var gender=0;gender<configArray["gender"].length;gender++){
			var selector=configArray["gender"][gender].toLowerCase().replace(/\W+/g, "") + "-gender-filter";
			$("."+selector).removeClass("unchecked").addClass("checked");
		}

		$("[data-target=gender-types] .selected").html(Myntra.Utils.truncateStr(configArray["gender"].join(", "))); */
		Myntra.Search.Filters.updateUI("gender");
		return true;
	},
	setToDefault: function(){
		configArray["gender"]=[];
		return true;
	},
	resetParams: function(){
		return true;
	},
    updateFilterVisibility: function(filters){
        if(!Myntra.Search.Data.userTriggered || (Myntra.Search.Data.filterClickedName != "" && Myntra.Search.Data.filterClickedName!="gender") || (Myntra.Search.Data.filterClickedName != "" && Myntra.Search.Data.filterClickedName=="gender" && configArray["gender"].length == 0)){
            $(".mk-gender-filters .mk-labelx_check").each(function (index,element){
                if(typeof filters.global_attr_gender != "undefined" && typeof filters.global_attr_gender[$(element).attr('data-value')] == 'undefined'){
                    $(element).addClass('disabled');
                }else{
                    $(element).removeClass("disabled");
                }
            });
        }
    },
    setFilterClickedName: function(){
        Myntra.Search.Data.filterClickedName = "gender";
    },
	updateClearFilterUI: function(){
		var resetCondition = (configArray["gender"].length >= 1);
		Myntra.Search.Filters.updateClearFiltersUI("gender", resetCondition);
	}
};
filterProperties["colour"]={
	getURLParam: function(){
		var colourStr=configArray["colour"].join(":");
		return (colourStr != "")?"colour="+unescape(colourStr):"";
		},
	processURL: function(str){
		configArray["colour"]=Myntra.Search.getFilterValues(unescape(str));
		return true;
	},
	updateUI: function(){
		/*// unselect all gender
		$(".mk-colour-filters .mk-labelx_check").removeClass("checked").addClass("unchecked");
								
		// update UI selection for gender in configArray
		for(var gender=0;gender<configArray["colour"].length;gender++){
			var selector=configArray["colour"][gender].toLowerCase().replace(/\W+/g, "") + "-colour-filter";
			$("."+selector).removeClass("unchecked").addClass("checked");
		}

		$("[data-target=colour-types] .selected").html(Myntra.Utils.truncateStr(configArray["colour"].join(", "))); */
		Myntra.Search.Filters.updateUI("colour");
		return true;
	},
	setToDefault: function(){
		configArray["colour"]=[];
		return true;
	},
	resetParams: function(){
		return true;
	},
    updateFilterVisibility: function(filters){
        if(!Myntra.Search.Data.userTriggered || (Myntra.Search.Data.filterClickedName != "" && Myntra.Search.Data.filterClickedName!="colour") || (Myntra.Search.Data.filterClickedName != "" && Myntra.Search.Data.filterClickedName=="colour" && configArray["colour"].length == 0)){
            $(".mk-colour-filters .mk-labelx_check").each(function (index,element){
                if(typeof filters.colour_family_list != "undefined" && typeof filters.colour_family_list[$(element).attr('data-value')] == 'undefined'){
                    $(element).addClass('disabled');
                }else{
                    $(element).removeClass("disabled");
                }
            });
        }
    },
    setFilterClickedName: function(){
        Myntra.Search.Data.filterClickedName = "colour";
    },
	updateClearFilterUI: function(){
		var resetCondition = (configArray["colour"].length >= 1);
		Myntra.Search.Filters.updateClearFiltersUI("colour", resetCondition);
	}
};
filterProperties["noofitems"]={
	getURLParam: function(){
		var noofitems=parseInt(configArray["noofitems"]);
		return (!isNaN(noofitems))?"noofitems="+noofitems:24;
	},
	processURL: function(str){
		configArray["noofitems"]=str;
		return true;
	},
	updateUI: function(){
		// set noofitems dropdown
		$(".noofitems-select").val(configArray["noofitems"]);
		return true;
	},
	setToDefault: function(){
		configArray["noofitems"]=Myntra.Search.Data.defaultNoOfItems;
		return true;
	},
	resetParams: function(){
		configArray["page"]=0;
		return true;
	},
    updateFilterVisibility: function(filters){
    },
    setFilterClickedName: function(){
	},
	updateClearFilterUI: function(){
	
	}
};
filterProperties["page"]={
	getURLParam: function(){
		var pagenum=parseInt(configArray["page"])+1;
		return (!isNaN(pagenum))?"page="+pagenum:0;
	},
	processURL: function(str){
		configArray["page"]=parseInt(str)-1;
		if(configArray["page"] == 0){
		    Myntra.Search.InfiniteScroll.pageSetToFirst=true;
		}
		Myntra.Search.InfiniteScroll.pageSetFromHash=true;
		return true;
	},
	updateUI: function(){
		return true;
	},
	setToDefault: function(){
		configArray["page"]=Myntra.Search.Data.pageDefault;
		Myntra.Search.InfiniteScroll.pageSetToFirst=true;
		Myntra.Search.InfiniteScroll.pageSetFromHash=true;
		// Myntra.Search.Data.onPageLoad=true;
		return true;
	},
	resetParams: function(){
		return true;
	},
    updateFilterVisibility: function(filters){
    },
    setFilterClickedName: function(){
	},
	updateClearFilterUI: function(){
	
	}
};
filterProperties["sortby"]={
	getURLParam: function(){
		return "sortby="+configArray["sortby"];
	},
	processURL: function(str){
		configArray["sortby"]=str;
		return true;
	},
	updateUI: function(){
		// set sortyby param
		$(".mk-product-filters li").removeClass("mk-sort-selected");
		var sortParam=(configArray["sortby"]=="")?"popular":configArray["sortby"].toLowerCase();
		$(".sort-by-type").text($(".mk-sort-"+sortParam+" a").attr("data-displayname"));		
		$(".mk-product-filters .mk-sort-"+sortParam).addClass("mk-sort-selected");
		$(".mk-product-filters .mk-price-desc").hide();
		$(".mk-product-filters .mk-price-asc").hide();
		return true;
	},
	setToDefault: function(){
		configArray["sortby"]=Myntra.Search.Data.sortByDefault;
		return true;
	},
	resetParams: function(){
		configArray["page"]=0;
		return true;
	},
    updateFilterVisibility: function(filters){
    },
    setFilterClickedName: function(){
	},
	updateClearFilterUI: function(){
	
	}
};
filterProperties["discountpercentage"]={
	getURLParam: function(){
		var discountStr=configArray["discountpercentage"];
		return (discountStr != "")?"discountpercentage="+unescape(discountStr):"";
	},
	processURL: function(str){
		configArray["discountpercentage"]=unescape(str);
		return true;
	},
	updateUI: function(){
		// unselect all checks
		$(".mk-discountpercentage-filters .mk-labelx_check").removeClass("checked").addClass("unchecked");
								
		// update UI selection for price range in configArray
		var selector="mk-"+configArray["discountpercentage"]+"-discountpercentage-filter";
		$("."+selector).removeClass("unchecked").addClass("checked");

		var str=configArray["discountpercentage"];
		if(str != ""){
			$("[data-target=discountpercentage-types] .selected").html(configArray["discountpercentage"] + "% and above"); 	
		}
		else{
			$("[data-target=discountpercentage-types] .selected").html(str); 
		}
		

		return true;
	},
	setToDefault: function(){
		configArray["discountpercentage"]="";
		return true;
	},
	resetParams: function(){
		return true;
	},
    updateFilterVisibility: function(filters){
        /*if(!userTriggered || (Myntra.Search.Data.filterClickedName != "" && Myntra.Search.Data.filterClickedName!="discountpercentage") || (Myntra.Search.Data.filterClickedName != "" && Myntra.Search.Data.filterClickedName=="discountpercentage" && configArray["discountpercentage"].length == 0)){
            $(".mk-discountpercentage-filters .mk-labelx_check").each(function (index,element){
                if(typeof filters.discount_percentage_details.counts == "undefined" || typeof filters.discount_percentage_details.counts[$(element).attr('data-value')] == 'undefined'){
                    $(element).find(".mk-filter-product-count").hide();
                    $(element).addClass('disabled').hide();
                }else{
                    $(element).find(".mk-filter-product-count").show().html("["+filters.discount_percentage_details.counts[$(element).attr('data-value')]+"]")
                    $(element).removeClass("disabled").show();
                }
            });
        }*/
    },
    setFilterClickedName: function(){
        Myntra.Search.Data.filterClickedName = "discountpercentage";
	},
	updateClearFilterUI: function(){
		var resetCondition = (configArray["discountpercentage"] != "");
		Myntra.Search.Filters.updateClearFiltersUI("discountpercentage", resetCondition);
	}
};
filterProperties["pricerange"]={
	getURLParam: function(){
		return "pricerange="+configArray["pricerange"].rangeMin+":"+configArray["pricerange"].rangeMax;
	},
	processURL: function(str){
		pricerange_values=Myntra.Search.getFilterValues(str);
		var minVal=parseInt(pricerange_values[0]);
		var maxVal=parseInt(pricerange_values[1]);
		if(!isNaN(minVal) && !isNaN(maxVal) && minVal >= Myntra.Search.Data.priceRangeMin && minVal <= Myntra.Search.Data.priceRangeMax && maxVal >= Myntra.Search.Data.priceRangeMin && maxVal <= Myntra.Search.Data.priceRangeMax){
			if(minVal<maxVal){
				configArray["pricerange"].rangeMin=minVal;
				configArray["pricerange"].rangeMax=maxVal;
			}
			else{
				configArray["pricerange"].rangeMin=maxVal;
				configArray["pricerange"].rangeMax=minVal;
			}
		}
		return true;
	},
	updateUI: function(){
		// unselect all checks
		$(".mk-price-filters .mk-labelx_check").removeClass("checked").addClass("unchecked");
								
		// update UI selection for price range in configArray
		var selector="mk-"+configArray["pricerange"].rangeMin+"-"+configArray["pricerange"].rangeMax+"-pricerange-filter";
		$("."+selector).removeClass("unchecked").addClass("checked");

		if(configArray["pricerange"].rangeMin != Myntra.Search.Data.priceRangeMin || configArray["pricerange"].rangeMax != Myntra.Search.Data.priceRangeMax){
			$("[data-target=pricerange-types] .selected").html("Rs. " + configArray["pricerange"].rangeMin+" - Rs. "+configArray["pricerange"].rangeMax); 
		}
		else{
			$("[data-target=pricerange-types] .selected").html(""); 	
		}

		return true;
	},
	setToDefault: function(){
		configArray["pricerange"].rangeMin=Myntra.Search.Data.priceRangeMin;
		configArray["pricerange"].rangeMax=Myntra.Search.Data.priceRangeMax;
		return true;
	},
	resetParams: function(){
		return true;
	},
    updateFilterVisibility: function(filters){
        if(!Myntra.Search.Data.userTriggered || (Myntra.Search.Data.filterClickedName != "" && Myntra.Search.Data.filterClickedName!="pricerange")){
        }
    },
    setFilterClickedName: function(){
        Myntra.Search.Data.filterClickedName = "pricerange";
	},
	updateClearFilterUI: function(){
		var resetCondition = (configArray["pricerange"].rangeMin != Myntra.Search.Data.priceRangeMin || configArray["pricerange"].rangeMax != Myntra.Search.Data.priceRangeMax);
		Myntra.Search.Filters.updateClearFiltersUI("pricerange", resetCondition);

		if(!resetCondition){
			$(".mk-price-filters .mk-labelx_check").removeClass("checked").addClass("unchecked");
		}
	}
};