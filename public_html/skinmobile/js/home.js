$('.m-static-banner a').each(function(){
    // Open up landing / list pages in tablets
    if(Myntra.Data.isTablet){
        return;
    }
    var data = { '/men' : '.mk-level-2-men' , '/women' : '.mk-level-2-women' , '/kids' : '.mk-level-2-kids' , '/home-decor' : '.mk-level-2-home--decor' };
    var showNav = false;
    var elem = this;
    var navItemSel = '';
    for (var link in data){
        if(elem.href.indexOf(link) != -1){
            showNav = true;
            navItemSel = data[link];
            break;
        }
    }
    if(showNav){
        $(this).click(function(e){
            e.stopPropagation();
            Myntra.Utils.preventDefault(e);
            $.pageslide();
            /* For now changing css and opening the nav. But need to find a workaround that works with fastClick */
            $(".mk-nav-ul").hide();
            $(navItemSel).show();
        });
    }
});
$(window).load(function(){
    $(".get-started-tooltip-container").fastClick(function(){
        Myntra.Utils.hideTooltipAndRearrangeHeader();
    });
    setTimeout(function(){
        Myntra.Utils.hideTooltipAndRearrangeHeader();
    }, Myntra.Data.tooltipTextDuration);
    $('.mk-slideshow a').fastClick(function(){
        window.location.href=$(this).attr('href');
    });
});

