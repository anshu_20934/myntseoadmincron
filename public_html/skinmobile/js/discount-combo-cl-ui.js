//////  UI //////////
Myntra.sCombo.UI= {
	moveItemToFront: function(styleObj){
		var parentDiv = $("#item_combo_lay_"+styleObj.get('uid')).parents('.cart-widget-items');
		var li = $("#item_combo_lay_"+styleObj.get('uid')).detach();
		li.prependTo(parentDiv);
	},
    refreshUI: function(){
		if(!$.isEmptyObject(Myntra.sCombo.products)){
				var collection = Myntra.sCombo.cSetStyles;
		        Myntra.sCombo.cSetStyles.setCartData();
			var completionMessage = "";
			if(Myntra.sCombo.freeItems.length > 0){
				$(".combo-overlay-total .savings-msg").addClass("hide");
			}
    	    if(Myntra.sCombo.cartSavings>0){
        	    $(".combo-overlay-total .total").html(Myntra.sCombo.discountedTotal);
            	$(".combo-overlay-total .savings-msg").css("float","none");
	        } else {
    	        $(".combo-overlay-total .total").html(Myntra.sCombo.cartTotal);
        	    $(".combo-overlay-total .savings-msg").css("float","left");
	        }
    	    $(".combo-overlay-total .savings").html(Myntra.sCombo.cartSavings);
        	if(Myntra.sCombo.isConditionMet){
	                completionMessage = "<span class=\"tick-small-icon\">&nbsp;</span>COMBO COMPLETE!";// <i>Modify Combo</i>";
					// Multiple free items
					if(Myntra.sCombo.freeItems.length > 1){
						completionMessage +=' <span class="combo-free-item-choose">Choose your free gift</span>';
					}
    	        } else {
        	    if(Myntra.sCombo.isAmount){
            	    completionMessage = "<span class=\"warning-small-icon\">&nbsp;</span>COMBO INCOMPLETE. Rs. "+Myntra.sCombo.amount_in_combo+"/- SELECTED SO FAR. <em>Rs. "+Myntra.sCombo.minMore+"/- MORE TO GO!</em>";
	            } else {
    	            completionMessage = "<span class=\"warning-small-icon\">&nbsp;</span>COMBO INCOMPLETE. "+Myntra.sCombo.quantity_in_combo+" SELECTED SO FAR. <em>"+Myntra.sCombo.minMore+" MORE TO GO!</em>";
        	    }
        	}
		}
		else if (!$.isEmptyObject(Myntra.sCombo.freeproducts)){
			$(".combo-header-sub").removeClass("hide").addClass("hide");
			$(".combo-overlay-total").removeClass("hide").addClass("hide");
				var collection = Myntra.sCombo.cFreeStyles;
				Myntra.sCombo.cFreeStyles.setCartData();
				$(".combo-header-sub").removeClass("hide").addClass("hide");
            $(".combo-overlay-total").removeClass("hide").addClass("hide");
			$("#cart-combo-overlay .hd").html("CHOOSE YOUR FREE ITEM");
		}
		if(collection == undefined) return Myntra.sCombo.Err('no cllection defined in refresh ui');
		if(collection.getAllAdded().length != 0){
			$(".combo-add-to-cart-button").removeAttr("disabled");	
		} else {
			$(".combo-add-to-cart-button").attr("disabled","true");
		}
        var addToCartBtnText = "";
        if(collection.isConditionMet){
            $(".combo-overlay-total .message").css("display","none");
            addToCartBtnText = "ADD COMBO TO BAG <span style=\"font-size:15px;\">&raquo;</span>";
        }else {
            $(".combo-overlay-total .message").css("display","block");
            addToCartBtnText = "ADD COMBO TO BAG <span style=\"font-size:15px;\">&raquo;</span>";
        }
        $(".completion-message").html(completionMessage);
    },
	checkBoxStyle: function(styleObj){
		styleObj.elCheckbox().removeClass("unchecked").addClass("checked");
	},
	uncheckBoxStyle: function(styleObj){
		styleObj.elCheckbox().removeClass("checked").addClass("unchecked");
	},
    checkStyle: function(styleObj){
        if(!styleObj) return Myntra.sCombo.Err('size set quan no  modal');
		Myntra.sCombo.UI.checkBoxStyle(styleObj);
		Myntra.sCombo.UI.showFlip(styleObj);
        //if(!styleObj.isInCart()){
            $("#item_combo_lay_"+styleObj.get('uid')+" .edit-flip").removeClass("hide");
        //}
        if(styleObj.get('size') && styleObj.get('quantity')) {
            $("#item_combo_lay_"+styleObj.get('uid')+" .main-flip .size-text-hdr").removeClass("hide");
            $("#item_combo_lay_"+styleObj.get('uid')+" .main-flip .qnty-text-hdr").removeClass("hide");
            $("#item_combo_lay_"+styleObj.get('uid')+" .main-flip .size-text-sub").html(styleObj.get('size'));
            $("#item_combo_lay_"+styleObj.get('uid')+" .main-flip .qnty-text-sub").html(styleObj.get('quantity'));
        }

    },
    uncheckStyle: function(styleObj){
        if(!styleObj) return Myntra.sCombo.Err('size set quan no  modal');
        styleObj.elCheckbox().removeClass("checked").addClass("unchecked");
        $("#item_combo_lay_"+styleObj.get('uid')+" .edit-flip").addClass("hide");
        $("#item_combo_lay_"+styleObj.get('uid')+" .size-box").addClass("hide");
        styleObj.set({size:''});
        styleObj.set({quantity:''});
        styleObj.set({skuid:''});
        Myntra.sCombo.UI.priceMessageContent(false,styleObj.get('uid'),styleObj.geti('size'),styleObj.geti('quantity'));
        //$("#item_combo_lay_"+styleObj.get('uid')+" .main-flip .size-text-hdr").addClass("hide");
        //$("#item_combo_lay_"+styleObj.get('uid')+" .main-flip .qnty-text-hdr").addClass("hide");
        $("#item_combo_lay_"+styleObj.get('uid')+" p.qty-msg").html("&nbsp;");
        $("#item_combo_lay_"+styleObj.get('uid')+" p.size-msg").html("&nbsp;");
        $("#item_combo_lay_"+styleObj.get('uid')+" .flip-wrapper .edit-flip").addClass("hide");
        //$("#item_combo_lay_"+styleObj.get('uid')+" .main-flip .edit-btn").addClass("hide");
        //$("#item_combo_lay_"+styleObj.get('uid')+" .main-flip .size-text-sub").html('');


    },
	hideEditFlip: function(styleObj){
		$("#item_combo_lay_"+styleObj.get('uid')+" .edit-flip").addClass("hide");
	},


	//Takes care of setting dropdowns too
    showFlip: function(styleObj){
        if(!styleObj) return Myntra.sCombo.Err('size set quan no  modal');
        $("#item_combo_lay_"+styleObj.get('uid')+" .edit-flip").removeClass("hide");
        $("#item_combo_lay_"+styleObj.get('uid')+" .size-box").removeClass("hide");
        $("#item_combo_lay_"+styleObj.get('uid')+" .size-info-box").removeClass("hide");
        $("#item_combo_lay_"+styleObj.get('uid')+" .size-info").removeClass("hide");
        $(styleObj.elSizeS()).empty().append('<option value="-">-</option>');

        var data = styleObj.get('sizeList');
//            var sizes = data[styleObj.get('styleid')];
        var sizes = data;
        var sizeS = $("#item_combo_lay_"+styleObj.get('uid')+" .cow-size-loader");
        var markup = [];
        if(sizes){
            var noOfFields = 0;
            var selectedSku = styleObj.get('skuid');
            var key, val, sel, dis, qty, qty_h;
            for(key in sizes){
                noOfFields++;
            }
            var isSingleElement = (noOfFields==1)?true:false;
            for (key in sizes) {
                dis = '';
                sel = (key==selectedSku)?'selected="true" ':'';
                val = sizes[key][0];
                qty_h = " qty='" +sizes[key][2]+"' ";

                if (key==selectedSku || isSingleElement){
                    $("#item_combo_lay_"+styleObj.get('uid')+" .size-text-sub").html(val);
//                              $(".item_"+styleId+" .size-text-sub").html(val);
                    //var qty = $(".item_"+styleId+" .edit-flip .quantity").val();
                    //qty = $("#item_combo_lay_"+styleObj.get('uid')+" .edit-flip .quantity").val();
					qty = styleObj.get('quantity');
                    $("#item_combo_lay_"+styleObj.get('uid')+" .qnty-text-sub").html(qty);
                  // $(".item_"+styleId+" .qnty-text-sub").html(qty);
                }
                var sizeInfoBox = $("#item_combo_lay_"+styleObj.get('uid')+" .size-info-box");
                if(isSingleElement){
                    $(sizeInfoBox).html(val);
                    $(sizeInfoBox).attr("skuid",key);
                    $(sizeInfoBox).attr("qty",sizes[key][2]);
                    $(sizeInfoBox).removeClass("hide").addClass("mk-freesize");
                    $(sizeS).addClass("hide");
                    $(sizeInfoBox).removeClass("hide");
//                    styleObj.set({skuid:key});
//                    styleObj.set({size:val});
                    break;
                }else {
                    $(sizeInfoBox).addClass("hide").removeClass("mk-freesize");
                    $(sizeS).removeClass("hide");
                }

                if (sizes[key][2]==0) {
                    continue; // don't show the unavailable sizes
                }
                markup.push(['<option value="', key, '" ', sel, dis, qty_h, '>', val, '</option>'].join(''));
            }
            if(markup.length==0 && $(sizeInfoBox).hasClass('hide')){
                $("#item_combo_lay_"+styleObj.get('uid')+" .size-msg").html("SOLD OUT");
                $("#item_combo_lay_"+styleObj.get('uid')+" .size-info").addClass("hide");
            }
            else{
                sizeS.append(markup.join("\n"));
            }
            //if(isSingleElement){
                //var sizeEl = $("#item_combo_lay_"+styleObj.get('uid')+" .size-info .mk-freesize");
                //styleObj.set({skuid:key, size:val, quantity:1});
            //}
            Myntra.sCombo.UI.ModifySizeSetQuantity(styleObj);

        } else {
            Myntra.sCombo.Err('no size');
        }
    },
    ModifySizeSetQuantity: function(styleObj){
        if(!styleObj) return Myntra.sCombo.Err('size set quan no  modal');
        var selectElement = styleObj.elSizeS();
        var qty,sel,selectedQty;
		if(styleObj.get('quantity')){
			selectedQty = styleObj.get('quantity');
		}
        if($(selectElement).hasClass('size-info-box')){ // Freesize
            qty = $(selectElement).attr("qty");
        } else {
            qty = $(selectElement).find("option:selected").attr("qty");
        }
        try{
            qty = parseInt(qty);
            var max = qty;
            if(max>10){
                max = 10;
            }
            var opt = [];
            for(var i=1; i<=max; i++){
				sel = (i==selectedQty)?'selected="true" ':'';
                opt.push(['<option value="', i, '" '+ sel +' >', i, '</option>'].join(''));
            }
            if(max>0){
                $("#item_combo_lay_"+styleObj.get('uid')+" .quantity-info select").empty().append(opt.join("\n"));
                $("#item_combo_lay_"+styleObj.get('uid')+" .quantity-info").css("display","block");
            }
            if(qty<=3 && qty>0){
                $("#item_combo_lay_"+styleObj.get('uid')+" p.qty-msg").html("ONLY "+qty+" UNIT(S) IN STOCK");
            }else if(qty==0) {
                $("#item_combo_lay_"+styleObj.get('uid')+" .quantity-info").css("display","none");
                $("#item_combo_lay_"+styleObj.get('uid')+" p.qty-msg").html("UNIT(S) OUT OF STOCK");
            }else {
                $("#item_combo_lay_"+styleObj.get('uid')+" p.qty-msg").html("&nbsp;");
            }
        } catch(e) {
        }

    },
    priceMessageContent: function(show,uid,size,quantity){
        if(show && size && quantity){
            $("#item_combo_lay_"+uid+" .main-flip .size-text-hdr").removeClass("hide");
            $("#item_combo_lay_"+uid+" .main-flip .qnty-text-hdr").removeClass("hide");
            $("#item_combo_lay_"+uid+" .main-flip .edit-btn").removeClass("hide");
        } else {
            $("#item_combo_lay_"+uid+" .main-flip .size-text-hdr").addClass("hide");
            $("#item_combo_lay_"+uid+" .main-flip .qnty-text-hdr").addClass("hide");
            $("#item_combo_lay_"+uid+" .main-flip .edit-btn").addClass("hide");
        }
        if(size == null) size = ' ';
        if(quantity == null ) quantity = ' ';

        $("#item_combo_lay_"+uid+" .main-flip .size-text-sub").html(size);
        $("#item_combo_lay_"+uid+" .main-flip .qnty-text-sub").html(quantity);
    }

}
