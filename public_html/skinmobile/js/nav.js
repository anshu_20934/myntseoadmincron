Myntra.Nav = {
    init: function() {
        /* Initiating Pageslide here */
        $(".m-slide-menu").pageslide();

    /* Nav menu interaction javascript - this needs to support tap,click,fast button */
        //$("#nav").delegate(".mk-nav-levels a", 'tap', function(event){
        //$(".mk-nav-levels a").fastClick(function(event){
        $(".mk-nav-ul a").fastClick(function(event){
            /*event.stopPropagation();
            event.preventDefault();*/
            var sublistid = $(event.currentTarget).attr("data-sublistid");
            var parentlistid = $(event.currentTarget).attr("data-parentlistid");
            var contextid = $(event.currentTarget).attr("data-contextid");
            if(typeof sublistid != "undefined" && sublistid != ""){
                $("."+contextid).hide();
                $("."+sublistid).show();  
            }
            else if(typeof parentlistid != "undefined" && parentlistid){
                $("."+contextid).hide();
                $("."+parentlistid).show();
            }
           else{
                var target = $(event.target);
                var href = target.attr('href');

                if (href !== '') {
                  if (target.attr('target') === '_blank') {
                      window.open(target.attr('href'));
                  } else {
                      window.location.href = target.attr('href');
                  }
                  return false;
                }
            }
            /*if(typeof sublistid != "undefined" && sublistid != ""){
                var newEl=$("."+sublistid),
                    curEl=$("."+contextid);

                newEl.css({"left" : "232px", "display" : "block", "position" : "relative"});
                newEl.animate({"left" : "0px"}, 500);
                curEl.css({"left" : "-232px", "display" : "none", "position" : "relative"});
                curEl.animate({"left" : "-232px"}, 500);


                //$("."+contextid).hide();
                //$("."+sublistid).show();  
            }
            else if(typeof parentlistid != "undefined" && parentlistid){
                $("."+contextid).hide();
                $("."+parentlistid).show();
            }
           else{
                var target = $(event.target);
                var href = target.attr('href');

                if (href !== '') {
                  if (target.attr('target') === '_blank') {
                      window.open(target.attr('href'));
                  } else {
                      window.location.href = target.attr('href');
                  }
                  return false;
                }
            }*/
            scroller.refresh();
        });

        var loginPrompt = function(e){
            e.stopPropagation();
            Myntra.Utils.preventDefault(e);
            var link = $(this).attr("href");
            var presentPage = window.location.href;
            var patt=/mkmycart.php/g;
            var patt2=/wishlist/g;
            var isWishlist=patt2.test(link);
            var patt3=/mymyntra/g;
            var isMymynttra=patt3.test(link);
            var isCartPage = patt.test(presentPage);
            if(isCartPage && !isMymynttra) {
                var alreadyAtBag=$('.cart-page .shoppping-bag-header.selected-tab').length>0?true:false;
                    if((alreadyAtBag && isWishlist) || isWishlist) {
                        $(".cart-page .wishlist-header").trigger('click');   
                    }  else {
                        $(".cart-page .shoppping-bag-header").trigger('click');    
                    }
                    $.pageslide.close();
                    return false;
                }
            if(Myntra.Data.userEmail=="" || Myntra.Data.userEmail == null) {
                $(document).trigger('myntra.login.show', {action: 'login', onLoginDone : function(){
                    location.href = link;
                }});
            } 
            else {
                location.href = link;
            }
        };
        //$('.mk-nav-levels-first.checklogin a').fastClick(loginPrompt);
        //$('.mk-nav-levels-first.checklogin a').live("click", loginPrompt);
        $('#nav').delegate(".mk-nav-levels-first.checklogin a", "click", loginPrompt);
        $('.mk-home-nav-links').delegate(".mk-nav-levels-first.checklogin a", "click", loginPrompt);
       
        $("#nav").hammer().bind('swipe', function() {
            $.pageslide.close();
            event.stopPropagation();
            Myntra.Utils.preventDefault(event);
        });
        
        $(".mk-body-overlay").fastClick(function(event){
            $.pageslide.close();
            $(this).hide();
            $('.main-slide-wrapper .close-wrapper').click();
            event.stopPropagation();
            Myntra.Utils.preventDefault(event);
            
        });
        $(".mk-body-overlay").hammer().bind('swipe', function() {
            $.pageslide.close();
            event.stopPropagation();
            Myntra.Utils.preventDefault(event);
        });
        var searchInputEl=$("#search_query"),
            searchOverlayEl=$(".search-overlay"),
            bodyOverlayEl=$(".mk-body-overlay");

        var showSearch = function(e) {
            Myntra.Header.ActiveSearchBox=$(e.target).attr("data-searchboxtype");
            if(Myntra.Header.ActiveSearchBox=="navsearchbox") {
            	$.pageslide.close();
                searchOverlayEl.removeClass("headerlink").addClass("navlink");
            }
            else {
                searchOverlayEl.removeClass("navlink").addClass("headerlink");
            }
            bodyOverlayEl.hide();
            searchOverlayEl.show();
            Myntra.Utils.extendObjToScreen($(".search-overlay"));
            searchInputEl.attr("data-searchboxtype", Myntra.Header.ActiveSearchBox);
            $('.search-overlay .search-results-body .ui-autocomplete').html('');
            $(".search-form-container .clear-text-btn").css({'display' : 'none'});
            //$(".search-results-body").css({'height' : screen.height, 'overflow-y': 'auto'});
            $('.pseudo-search-box').blur();
            searchInputEl.focus();
            $('body').addClass('iosorientationfix');
            
        };

        Myntra.Utils.extendClickEvent($(".pseudo-search-box"), function(e){
            showSearch(e);
        });
    }
};

$(document).ready(function(){
    Myntra.Nav.init();
});

$(window).load(function() {
    window.onresize = function(event) {
        Myntra.Utils.extendObjToScreen($(".search-overlay"));
        $(".search-results-body").css({'height' : screen.height});
    }
    var timeout = null;
    $(window).scroll(function() {
        clearTimeout(timeout);
        timeout = setTimeout(function() {
            	Myntra.Utils.extendObjToScreen($(".search-overlay"));
        }, 100);
    });
});
