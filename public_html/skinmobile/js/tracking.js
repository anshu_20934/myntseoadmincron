
function trackingOmniture(){
	if(typeof(s_analytics_firesync) != "undefined" && s_analytics_firesync) {
		// Sync call already triggered. Check analytics_data_collection.. 
		return;
	}
	
	// Omniture page call. Using s_analytics as s is getting unset by some piece of code and is not visible here
	if(typeof(s_analytics) != "undefined"){
		s_analytics.t();
	} 
}

function tracking(){
    // GA tracking page load call
	var gaScript = document.createElement('script');
	gaScript.type = 'text/javascript';
	gaScript.async = true;
    gaScript.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(gaScript, s);
	_gaq.push(['_trackEvent','GA.JS', 'load']);

    //fallback for dc.js
    /*
    setTimeout(function(){
        if (typeof _gat == 'undefined') {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
            _gaq.push(['_trackEvent','GA.JS', 'load']);
        }
    }, 1000); */
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  	ga('create', 'UA-1752831-15', 'auto');
  	ga('set', '&uid', Myntra.Data.userHashId);
  	ga('send', 'pageview');

    // Mediamind page load call
	var mm = document.createElement('script');
	mm.type = 'text/javascript';
	mm.async = true;
	var mmsrc = "";
    var ebRand = Math.random() * 1000000;
	if('https:' == document.location.protocol){
		mmsrc = "HTTPS://bs.serving-sys.com/BurstingPipe/ActivityServer.bs?cn=as&ActivityID=112557&rnd=" + ebRand;
	}
	else{
		mmsrc = "HTTP://bs.serving-sys.com/BurstingPipe/ActivityServer.bs?cn=as&ActivityID=112557&rnd=" + ebRand;
	}
	mm.src = mmsrc;
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(mm, s);

    

    //trial tracking for adRoll
    adroll_adv_id = "F5GPVDZIVBEFTJDGGMGWOR";
    adroll_pix_id = "D2T5ATF7D5C5TEKFA4CVB7"; 
    __adroll_loaded=true;

    var scr = document.createElement("script");
    var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
    scr.setAttribute('async', 'true');
    scr.type = "text/javascript";
    scr.src = host + "/j/roundtrip.js";
    ((document.getElementsByTagName('head') || [null])[0] ||
      document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
}

//GA related calls
Myntra.GA = Myntra.GA || {};

Myntra.GA.gaTrackEventAjax = function(category, action, label){
	_gaq.push(['_trackEvent', category, action, label]);
	return false;
}

Myntra.GA.gaTrackPageViewAjax = function(url){
	_gaq.push(['_trackPageview', url]);
	return false;
}

$(window).load(function() {
	trackingOmniture();
    tracking();
});

