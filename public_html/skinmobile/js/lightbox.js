Myntra.LightBox = function(id, cfg) {
    
    cfg = cfg || {};
    cfg.isModal = true;
    // if a template is used in a popup and in a page
    cfg.isPopup = (typeof cfg.isPopup === 'undefined') ? true : cfg.isPopup;
    //cfg.isModal = (typeof cfg.isModal === 'undefined') ? true : cfg.isModal;
    
    cfg.disableCloseButton = (typeof cfg.disableCloseButton === 'undefined') ? false : cfg.isModal;
    cfg.disableShim = (typeof cfg.disableShim === 'undefined') ? false : cfg.disableShim;

    // track the no of show/hide so that we do some cleanup/actions at the last hide
    Myntra.LightBox.lbShownCount = Myntra.LightBox.lbShownCount || 0;

    // track the references of the shown lightboxes
    Myntra.LightBox.boxes = Myntra.LightBox.boxes || [];

    // move the dialog into immediate child of the body.
    if (cfg.isPopup && !$(id).parent().is('body')) {
        $('body').append($(id));
    }

    if (cfg.isPopup) {
        $(document).ready(function(){
            Myntra.Utils.extendObjToScreen($("#lightbox-shim-0"));
            if(typeof($(id) != "undefined")){
                Myntra.Utils.extendObjToScreen($(id));
            }
        });
    }
    var obj = Myntra.MkBase(),
        panel = $(id),
        mod = $(id + ' .mod'),
        close = $(id + ' .close'),
        progress = null,
        //shim = null,
        shim = $('#lightbox-shim-0'),
        body = $(id + ' .mod .bd');
    //set the identifier in the lightbox object
    obj.set("id", id);
    //set the lightbox object as part of data object of each element

    //if (!shim) {
    //    shim = $('<div class="lightbox-shim" id="' + panel.attr('id') + '-shim"></div>').appendTo('body');
    //}
    if (!shim.length) {
        if(!$("#lightbox-shim-0").length){
            shim = $('<div class="lightbox-shim" id="lightbox-shim-0"></div>').appendTo('body');
        }
        else{
            shim=$("#lightbox-shim-0");
            if(!cfg.disableShim){
                shim.show();
            }
        }
    }

    if (cfg.isPopup) {
        if (!close.length && !cfg.disableCloseButton) {
            mod.prepend('<div class="close"></div>');
            close = $(id + ' .close');
            close.click(function(e) {
                obj.hide();
                Myntra.Header.tempDisableCart();
                Myntra.Header.tempDisableMyntraIcon();
            });
        }
        obj.center = function() {
            /*var dy = Math.round(($(window).height() - mod.outerHeight(false)) / 2);
            if (dy < 20) {
                dy = 20;
                mod.css('margin-bottom', dy + 'px');
            }*/
            var dy=20;
            mod.css('margin-top', dy + 'px');
        };
        var resizeTimer;
        $(window).resize(function() {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function() {
                if(mobileOrientationCheck.isChanged()) {
                //obj.center();
                if(obj.isModal()) {
                    $(obj.get('id')).css({'top':$(window).scrollTop()});
                }
                if(typeof($(obj.get('id')) != "undefined")) {
                    Myntra.Utils.extendObjToScreen($(obj.get('id')));
                 }
            	}
            }, 200);
        });
    }
    else {
        obj.center = function(){};
    }

    /*
    panel.click(function(e) {
        if (obj.isModal()) { return; }
        $(e.target).is(panel) && obj.hide();
    });
*/
    obj.getId = function() {
        return panel.attr('id');
    };

    obj.isModal = function() {
        return cfg.isModal;
    };

    obj.isPopup = function() {
        return cfg.isPopup;
    };

    obj.showLoading = function() {
        if (!$(id + ' .loading').length) {
            mod.append('<div class="loading"></div>');
        }
        if (!progress) {
            progress = $(id + ' .loading');
        }
        progress.show();
    };

    obj.hideLoading = function() {
        progress && progress.hide();
    };

    /*obj.addIdToUrlHash = function(id){
        Myntra.LightBox.lightboxId=id.substring(id.indexOf("#")+1 ,id.length);
        window.location.hash="#!"+Myntra.LightBox.lightboxId;
    }

    obj.removeIdFromUrlHash = function(id){
        Myntra.LightBox.lightboxId=id.substring(id.indexOf("#")+1 ,id.length);
        window.location.hash= "#!default";
    }*/

    obj.beforeShow = function() {
        //this.addIdToUrlHash(this.get("id"));
        if (typeof cfg.beforeShow === 'function') {
            cfg.beforeShow.call(obj);
        }
        else if (cfg.evtPrefix) {
            $(document).trigger(cfg.evtPrefix + '.beforeShow');
        }
    };

    obj.afterShow = function() {
        if(obj.isModal()) {
            $(obj.get('id')).css({'top':$(window).scrollTop()});
        }
        if (typeof cfg.afterShow === 'function') {
            cfg.afterShow.call(obj);
        }
        else if (cfg.evtPrefix) {
            $(document).trigger(cfg.evtPrefix + '.afterShow');
        }
    };

    obj.setShim = function(){
        /*var dy = Math.round(mod.outerHeight(false));
        shim.css({"width" : dy + "px"});
        console.log(dy, dy*2);*/
        if(!cfg.disableShim){
            shim.show();
        }
    };

    obj.show = function(a_cfg) {
        var prevLB;
        $.extend(cfg, a_cfg);

        // prevent showing the same popup over and over
        // (for example double clicking a link the triggers showing the same popup 2 times)
        if (Myntra.LightBox.boxes.length && Myntra.LightBox.boxes[Myntra.LightBox.boxes.length - 1] === obj) {
            return;
        }

        // hide the previous box, if exists
        prevLB = Myntra.LightBox.boxes.pop();
        if (prevLB) {
            prevLB.tmpHide();
            Myntra.LightBox.boxes.push(prevLB);
        }

        obj.beforeShow();
        if (Myntra.LightBox.lbShownCount <= 0) {
            Myntra.LightBox.scrollTop = $(window).scrollTop();
            //$('html,body,.m-body').css({'overflow-y':'hidden', 'margin-right':'6px'});
            //$('body').height('1000px');
            $(window).scrollTop(Myntra.LightBox.scrollTop);
        }
        panel.fadeIn();
        obj.center();
        obj.setShim();
        obj.afterShow();

        Myntra.LightBox.lbShownCount += 1;

        // push the last box
        Myntra.LightBox.boxes.push(obj);
    };

    obj.beforeHide = function() {
        //this.removeIdFromUrlHash(this.get("id"));
        if (typeof cfg.beforeHide === 'function') {
            cfg.beforeHide.call(obj);
        }
        else if (cfg.evtPrefix) {
            $(document).trigger(cfg.evtPrefix + '.beforeHide');
        }
    };

    obj.afterHide = function() {
        if (typeof cfg.afterHide === 'function') {
            cfg.afterHide.call(obj);
        }
        else if (cfg.evtPrefix) {
            $(document).trigger(cfg.evtPrefix + '.afterHide');
        }
    };

    obj.hide = function() {
        var prevLB;
        obj.beforeHide();
        if (progress) { progress.hide(); }
        panel.fadeOut(400, function() {
            Myntra.LightBox.lbShownCount -= 1;
            if (Myntra.LightBox.lbShownCount <= 0) {
                //$('html,body,.m-body').css({'overflow':'auto','margin-right':'0'});
                //$('html,body,.m-body').removeAttr('style');
                $(window).scrollTop(Myntra.LightBox.scrollTop);
            }
        });
        shim.hide();
        obj.afterHide();

        // pop the last box
        Myntra.LightBox.boxes.pop(obj);

        // show the previous box, if exists
        prevLB = Myntra.LightBox.boxes.pop();
        if (prevLB) {
            prevLB.tmpShow();
            Myntra.LightBox.boxes.push(prevLB);
        }
    };

    obj.tmpShow = function() {
        panel.show();
        shim.show();
        if(!cfg.disableShim){
            obj.center();
        }
    };

    obj.tmpHide = function() {
        panel.hide();
        shim.hide();
    };

    obj.clearPopupContent = function(){
        body.html('');
    }

    if (panel.is('.infobox')) {
        panel.find('.hd .title').prepend('<span class = "info-icon"></span>');
        panel.find('.ft').append('<button class="btn small-btn normal-btn btn-ok">Close</button>');
        panel.find('.ft .btn-ok').click( function() {
            obj.hide();
        });
    }

    return obj;
};

Myntra.LightBox.lightboxId="";

Myntra.LightBox.hideAll = function() {
    var lb, i;
    for (i = Myntra.LightBox.boxes.length - 1; i >= 0; i--) {
        lb = Myntra.LightBox.boxes[i];
        lb.hide();
    }
    Myntra.LightBox.boxes.length = 0;
};

Myntra.MsgBox = (function(){
    var obj, title, hd, bd;

    var init = function() {
        var h = '';
        h += '<div id="lb-msg-box" class="lightbox">';
        h += '<div class="mod">';
        h += '    <div class="hd">';
        h += '      <h2></h2>';
        h += '  </div>';
        h += '  <div class="bd">';
        h += '  </div>';
        h += '  <div class="ft">';
        h += '      <button class="btn normal-btn small-btn btn-ok">Close</button>';
        h += '  </div>';
        h += '</div>';
        h += '</div>';
        $('body').append(h);
        obj = Myntra.LightBox('#lb-msg-box');
        title = $('#lb-msg-box h2');
        hd = $('#lb-msg-box .hd');
        bd = $('#lb-msg-box .bd');
        $('#lb-msg-box .btn-ok').click(function(e) { obj.hide() });
    };

    return {
        show: function(a_title, content, cfg) {
            if (!obj) { init(); }
            title.text(a_title);
            a_title ? hd.show() : hd.hide();
            bd.html(content);
            obj.show();
            if (cfg && cfg.autohide) {
                setTimeout(function() { obj.hide() }, cfg.autohide);
            }
            return obj;
        },

        hide: function() {
            obj.hide();
        }
    };
})();

