scrollProperties["scrollPage"]={
    getURLParam: function(){
        return "scrollPage="+configArray["page"];
    },
    processURL: function(str){
        scrollConfigArray["scrollPage"]=parseInt(str);
    },
    updateUI: function(){
    },
    setToDefault: function(){
    },
    resetParams: function(){
    },
    updateFilterVisibility: function(filters){
    },
    setFilterClickedName: function(){
    }
};
scrollProperties["scrollPos"]={
    getURLParam: function(elem){        
        scrollFactor=(($(window).scrollTop()-Myntra.Search.InfiniteScroll.getSearchContainerOffsetTop())/Myntra.Search.InfiniteScroll.getPageHeight()).toFixed(2);
        // alert($(window).scrollTop());
        // alert(Myntra.Search.InfiniteScroll.getSearchContainerOffsetTop());
        // alert(Myntra.Search.InfiniteScroll.getPageHeight());
        // alert(scrollFactor);
        return "scrollPos="+scrollFactor;
    },
    processURL: function(str){
        scrollConfigArray["scrollPos"]=parseFloat(str);
    },
    updateUI: function(){
    },
    setToDefault: function(){
    },
    resetParams: function(){
    },
    updateFilterVisibility: function(filters){
    },
    setFilterClickedName: function(){
    }
};