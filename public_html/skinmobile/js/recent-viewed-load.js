$(window).load(function(){
var flexdata;
    $.ajax({
        type:"GET",
        url:http_loc+'/getRecentItems.php?limit=4',
        dataType:"json",
        success: function(data){
            if(data.status == 'SUCCESS'){
            	flexdata=data.content;
            	flexRecentViewedInit(flexdata);
                var flexresizeTimer;
                $(window).resize(function() {
                    clearTimeout(flexresizeTimer);
                    flexresizeTimer = setTimeout(function() {
                        if(mobileOrientationCheck.isChanged()) {
                        	flexRecentViewedInit(flexdata);
                    	}
                    }, 200);
                });
            }else{
                $('.mk-recent-viewed').hide();
            }
        }
    });
   function flexRecentViewedInit(data) {
	   var recentviewed = $('.mk-recent-viewed');
	   recentviewed.attr('id',"tempFlex");
	   var itsClass=recentviewed.attr('class');
	   recentviewed.before("<div class='"+itsClass+"'></div>");
	   $('#tempFlex').remove();
	   var recentviewed = $('.mk-recent-viewed');
	   recentviewed.html('');
	   recentviewed.html(data);
       //getRecentSizes();
	   recentviewed.addClass('flexslider');
	   recentviewed.find('.rec-title').remove();
       $('#tempFlex').remove();
       recentviewed.removeClass('mk-carousel');
       $(".mk-recent-viewed-ul").addClass("slides").removeClass('rs-carousel-runner');
       $(".mk-recent-viewed-ul li").addClass("flex-recently-viewed-product").removeClass('rs-carousel-item');
	   var itemwidth=$('.mk-recent-viewed.flexslider .slides > li').width();
	   itemnum = $(".mk-recent-viewed-ul li").length;
	   var divwidth=itemwidth*itemnum;
	   var widthOfViewPort=recentviewed.css('max-width');
	   var viewportItems = 5;
	   if(widthOfViewPort>0 && widthOfViewPort<= 320) {
		   viewportItems=2;
	   }else if(widthOfViewPort>320 && widthOfViewPort <= 600) {
		   viewportItems=3;
	   }else if(widthOfViewPort>600) {
		   viewportItems=5;
	   }
	   recentviewed.css({'width':divwidth});
       $('.mk-recent-viewed.flexslider').flexslider({
           animation: "slide",
           startAt: 0,                     //Integer: The slide that the slider should start on. Array notation (0 = first slide)
           slideshow: true,
           animationLoop: true,
           slideshowSpeed : 2500,
           pauseOnAction : false,
           itemMargin: 0,
           move: 0,
           minItems: 1, // use function to pull in initial value
           maxItems: widthOfViewPort,
           itemWidth: itemwidth
         });
        recentviewed.show();
       $(".mk-recent-viewed-ul a").fastClick(function(e) {
            _gaq.push(['_trackEvent', 'home_click', Myntra.Data.pageName, 'recent_widget']);
            window.location.href = $(this).attr('href');
        });
   }  
});
