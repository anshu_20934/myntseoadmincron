function loadImageQueue(){
/*	$(".lazy").lazyload({
        effect : "fadeIn"
    });
    $(".lazy").removeClass("lazy");
*/
/*    var lazy_images = $('.lazy');
    $.each(lazy_images, function() {
        $(this).attr("src", $(this).attr("lazy-src"));
        $(this).removeAttr("lazy-src");
        $(this).removeClass("lazy");
    });*/
}

Myntra.Combo = {};
Myntra.Combo.ComboOverlay = {};
Myntra.Combo.isConditionMet = false;
Myntra.Combo.selectedStyles = [];
Myntra.Combo.min = 0;
Myntra.Combo.cartSavings = 0;
Myntra.Combo.cartTotal = 0;
Myntra.Combo.products = {};
Myntra.Combo.selectedStylesQntyChanged = [];
Myntra.Combo.addedStyles = {};
Myntra.Combo.removedStyles = {};
Myntra.Combo.quantity_in_combo = 0;
Myntra.Combo.isRemoved = false;
Myntra.Combo.allSelectedStyles = Object();
Myntra.Combo.solrProducts = [];
Myntra.Combo.ctype = "";
Myntra.Combo.isAmount = false;
Myntra.Combo.amount_in_combo = 0;

Myntra.Combo.ReInitializeVariables = function () {
    //these commented variables are set in the
//    Myntra.Combo.isConditionMet = {$isConditionMet|@json_encode};
//    Myntra.Combo.selectedStyles = {$selectedStyles|@json_encode};
//    Myntra.Combo.minMore = {$minMore};
//    Myntra.Combo.cartSavings = {$comboSavings};
//    Myntra.Combo.cartTotal = {$comboTotal};
//    Myntra.Combo.products = {$products|@json_encode};

    Myntra.Combo.selectedStylesQntyChanged = [];
    Myntra.Combo.addedStyles = {};
    Myntra.Combo.removedStyles = {};
    Myntra.Combo.quantity_in_combo = 0;
    Myntra.Combo.isRemoved = false;
    Myntra.Combo.allSelectedStyles = Object();
    Myntra.Combo.minMore = Myntra.Combo.min;
    Myntra.Combo.imagesDownloaded = false;
}

Myntra.Combo.SelectStyles = function () {
    for(var i=0; i<Myntra.Combo.selectedStyles.length; i++){
		$(".item_"+Myntra.Combo.selectedStyles[i]+" .checkbox").addClass("unchecked");
        Myntra.Combo.SelectStyle($(".item_"+Myntra.Combo.selectedStyles[i]+" .checkbox")[0], true);
        $(".acombo .cart-widget-box ul").prepend($(".item_" + Myntra.Combo.selectedStyles[i]));
    }
    //Myntra.Combo.SortStyles();
}

Myntra.Combo.SelectStyle = function(input, firstRun) {
    var styleId = $(input).attr("data-styleid");
    var skuid = $(input).attr("data-skuid");
	if(skuid=='') skuid = 0;
	else skuid = parseInt(skuid);
    if($(input).hasClass("unchecked")){//if selected
        Myntra.Combo.LoadSizes(styleId);
        if(Myntra.Combo.removedStyles[styleId]!=null)
        Myntra.Combo.removedStyles[styleId][skuid] = 0;

        $(input).parents(".cart-widget-item-container").find(".mk-product-image").addClass("cart-widget-item-box-selected");
        
        $(input).parents(".cart-widget-item-container").find(".size-selection-row-"  + styleId).removeClass("hide").css("visibility", "visible");
        var qnty = 1;
        $(".acombo .quantity-selection-row-"  + styleId).each(function(index){
			$(this).removeClass("hide");
			$(this).css("visibility", "visible");
			var select = $(this).find("select");
//            if(parseInt($(this).find("input").attr("data-skuid"))==skuid){
 //               $(this).css("visibility", "visible");
                if(!firstRun){
                    $(select).val("1");
			$(select).find("option[value=1]").attr("selected","true");
		}
                else
                    qnty = parseInt($(select).val());
				if(!$(select).hasClass("cow-quantity-select-disabled"))
					$(select).removeAttr('disabled');
//            }
        });
        if(Myntra.Combo.allSelectedStyles[styleId]==null) Myntra.Combo.allSelectedStyles[styleId] = [];
        Myntra.Combo.allSelectedStyles[styleId][skuid] = qnty;
        if(!firstRun){
            if(Myntra.Combo.addedStyles[styleId]==null) Myntra.Combo.addedStyles[styleId] = [];
            Myntra.Combo.addedStyles[styleId][skuid] = qnty;
        }
        if(!firstRun){
            Myntra.Combo.cartTotal += Myntra.Combo.solrProducts[styleId].price;
        }
        Myntra.Combo.quantity_in_combo += qnty;
        Myntra.Combo.amount_in_combo = Myntra.Combo.cartTotal;

        if(!Myntra.Combo.isAmount)
            Myntra.Combo.minMore = Myntra.Combo.min - Myntra.Combo.quantity_in_combo;
        else
            Myntra.Combo.minMore = Myntra.Combo.min - Myntra.Combo.amount_in_combo;

        Myntra.Combo.isConditionMet = Myntra.Combo.minMore<=0?true:false;

        if(!firstRun){
            if(Myntra.Combo.isConditionMet && Myntra.Combo.solrProducts[styleId].dre_amount>0){
                Myntra.Combo.cartSavings = Myntra.Combo.solrProducts[styleId].dre_amount;
            }
            if(Myntra.Combo.isConditionMet && Myntra.Combo.solrProducts[styleId].dre_percent>0){
                Myntra.Combo.cartSavings = Myntra.Combo.cartTotal * Myntra.Combo.solrProducts[styleId].dre_percent / 100;
            }
        }
//		$(".quantity-selection-row-"  + styleId + " select").change();
		if(firstRun){
			$(input).attr("disabled","true");
			$(input).parents(".cart-widget-item-container").find(".size-selection-row-"  + styleId);
		}
		if(firstRun){
                	$('#cow-size-select-'+styleId).attr('disabled','disabled');
                	$('#'+styleId+'_cow_qty').attr('disabled','disabled');
        	}
		$(input).removeClass('unchecked');
		$(input).addClass('checked');
		if(!firstRun)
			$(input).parents(".cart-combo-widget-image").find(".flip-wrapper .edit-flip").removeClass("hide");
			//$(input).parents(".cart-combo-widget-image").find(".flip-wrapper").quickFlipper();

    } else {//if removed
		if(!$(input).parents(".cart-combo-widget-image").find(".edit-flip").hasClass("hide"))
			$(input).parents(".cart-combo-widget-image").find(".edit-flip").addClass("hide");
        for(var i=0; i<Myntra.Combo.selectedStyles.length; i++){
            if(Myntra.Combo.selectedStyles[i]==styleId){
                Myntra.Combo.isRemoved = true;
                break;
            }
        }
        if(Myntra.Combo.addedStyles[styleId]!=null) Myntra.Combo.addedStyles[styleId][skuid] = null;
        if(Myntra.Combo.allSelectedStyles[styleId]!=null) Myntra.Combo.allSelectedStyles[styleId][skuid] = null;

		$(input).parents(".cart-widget-item-container").find(".mk-product-image").removeClass("cart-widget-item-box-selected");

        $(input).parents(".cart-widget-item-container").find(".size-selection-row-"  + styleId).addClass("hide").css("visibility", "hidden");

        var qnty = 0;
        $(".acombo .quantity-selection-row-"  + styleId).each(function(index){
			$(this).addClass("hide");
			if($(this).find("select").attr("data-skuid")=='')
				$(this).find("select").attr("data-skuid","0");
            if(parseInt($(this).find("select").attr("data-skuid"))==skuid){
                $(this).css("visibility", "hidden");
                qnty = $(this).find("select").val();
				if(qnty==null) qnty = 1
				else qnty = parseInt(qnty);
            }
        });

		
        if(Myntra.Combo.removedStyles[styleId]==null) Myntra.Combo.removedStyles[styleId] = [];
        Myntra.Combo.removedStyles[styleId][skuid] = qnty;

        Myntra.Combo.cartTotal -= (Myntra.Combo.solrProducts[styleId].price * qnty);

        Myntra.Combo.quantity_in_combo -= qnty;
        Myntra.Combo.amount_in_combo = Myntra.Combo.cartTotal;

        if(Myntra.Combo.ctype=='c') Myntra.Combo.minMore = Myntra.Combo.min - Myntra.Combo.quantity_in_combo;
        if(Myntra.Combo.ctype=='a') Myntra.Combo.minMore = Myntra.Combo.min - Myntra.Combo.amount_in_combo;
	Myntra.Combo.isConditionMet = Myntra.Combo.minMore<=0?true:false;
        if(!Myntra.Combo.isConditionMet){
            Myntra.Combo.cartSavings = 0;
        }
        if(Myntra.Combo.isConditionMet && Myntra.Combo.solrProducts[styleId].dre_percent>0){
            Myntra.Combo.cartSavings = Myntra.Combo.cartTotal * Myntra.Combo.solrProducts[styleId].dre_percent / 100;
        }

		$(input).removeClass('checked');
		$(input).addClass('unchecked');
		$(input).parents(".cart-widget-item-container").find("p.qty-msg").html("&nbsp;");
		$(input).parents(".cart-widget-item-container").find("p.size-msg").html("&nbsp;");
    		$(input).parents(".cart-combo-widget-image").find(".flip-wrapper .edit-flip").addClass("hide");
		$(input).parents(".cart-combo-widget-image").find(".main-flip .edit-btn").addClass("hide");
        	$(input).parents(".cart-combo-widget-image").find(".main-flip .size-text-sub").html('');
        $(input).parents(".cart-combo-widget-image").find(".main-flip .qnty-text-sub").html('');
        $(input).parents(".cart-combo-widget-image").find(".main-flip .size-text-hdr").addClass("hide");
        $(input).parents(".cart-combo-widget-image").find(".main-flip .qnty-text-hdr").addClass("hide");	
}
}

Myntra.Combo.ModifySize = function(selectElement) {
	Myntra.Combo.ModifySizeSetQuantity(selectElement);
	var prevSkuid = $(selectElement).attr("data-skuid");
	if(prevSkuid == '' ) prevSkuid = 0;
    var skuid = $(selectElement).find("option:selected").val();
    if(skuid=='-')
	skuid = $(selectElement).parents(".cart-widget-item-container").find(".size-info-box").attr("skuid");
   if(prevSkuid==skuid) return;
    $(selectElement).attr("data-skuid", skuid);
	$(selectElement).parents(".cart-widget-item-container").find("select").attr("data-skuid", skuid);
    $(selectElement).parents(".cart-widget-item-container").find(".checkbox").attr("data-skuid", skuid);
    var styleId = $(selectElement).parents(".cart-widget-item-container").find(".checkbox").attr("data-styleid");
    Myntra.Combo.allSelectedStyles[styleId][skuid] = Myntra.Combo.allSelectedStyles[styleId][prevSkuid];
    Myntra.Combo.addedStyles[styleId][skuid] = Myntra.Combo.addedStyles[styleId][prevSkuid];
    delete Myntra.Combo.allSelectedStyles[styleId][prevSkuid] ;
    delete Myntra.Combo.addedStyles[styleId][prevSkuid];
	if(skuid!='-' && skuid!=''){
		$(selectElement).parents(".size-box").find(".size-msg").html("&nbsp;");
		$(selectElement).parents(".cart-widget-item-container").find(".save-btn").removeAttr('disabled');
	}else{
		$(selectElement).parents(".size-box").find(".size-msg").html("PLEASE SELECT A SIZE");
		$(selectElement).parents(".cart-widget-item-container").find(".save-btn").attr('disabled','disabled');
	}
	
}

Myntra.Combo.ModifySizeSetQuantity = function(selectElement){
	if($(selectElement).hasClass('size-info-box')){ // Freesize
		var qty = $(selectElement).attr("qty");
	} else {
	var qty = $(selectElement).find("option:selected").attr("qty");
	}
	try{
		qty = parseInt(qty);
		var max = qty;
		if(max>10){
			max = 10;
		}
		var opt = [];
		for(var i=1; i<=max; i++){
			opt.push(['<option value="', i, '">', i, '</option>'].join(''));
		}
		if(max>0){
			$(selectElement).parents(".cart-widget-item-container").find(".quantity-info select").empty().append(opt.join("\n"));	
			$(selectElement).parents(".cart-widget-item-container").find(".quantity-info").css("display","block");
		}

		if(qty<=3 && qty>0){
			$(selectElement).parents(".cart-widget-item-container").find("p.qty-msg").html("ONLY "+qty+" UNIT(S) IN STOCK");
		}else if(qty==0) {
			$(selectElement).parents(".cart-widget-item-container").find(".quantity-info").css("display","none");
			$(selectElement).parents(".cart-widget-item-container").find("p.qty-msg").html("UNIT(S) OUT OF STOCK");
		}else {
			$(selectElement).parents(".cart-widget-item-container").find("p.qty-msg").html("&nbsp;");
		}
		
	} catch(e) {
	}	
}

Myntra.Combo.ModifyQuantity = function(inputElement) {
    var styleId = inputElement.getAttribute("data-styleId");
    var skuid = inputElement.getAttribute("data-skuid");
    if(skuid == '') skuid = 0;
    var qnty = Myntra.Combo.allSelectedStyles[styleId][skuid];
    if($(inputElement).val() == '') $(inputElement).val(1);
    var changedQnty = parseInt(inputElement.value);
    if(changedQnty < 1) {
		changedQnty = 1;
		$(inputElement).val(1);
    }
    if(qnty == undefined) qnty = 0;
    //if there is no change in quantity
    if(qnty==changedQnty) return;
    var qntyReducedBy = qnty-changedQnty;
    Myntra.Combo.cartTotal -= (Myntra.Combo.solrProducts[styleId].price * qntyReducedBy);
    Myntra.Combo.quantity_in_combo -= qntyReducedBy;
    Myntra.Combo.amount_in_combo = Myntra.Combo.cartTotal;

    if(!Myntra.Combo.isAmount)
        Myntra.Combo.minMore = Myntra.Combo.min - Myntra.Combo.quantity_in_combo;
    else
        Myntra.Combo.minMore = Myntra.Combo.min - Myntra.Combo.amount_in_combo;
    Myntra.Combo.isConditionMet = Myntra.Combo.minMore<=0?true:false;
    if(Myntra.Combo.solrProducts[styleId].dre_amount>0){
        if(!Myntra.Combo.isConditionMet)
            Myntra.Combo.cartSavings = 0;
        else
            Myntra.Combo.cartSavings = Myntra.Combo.solrProducts[styleId].dre_amount;
    }
    if(!Myntra.Combo.isConditionMet) Myntra.Combo.cartSavings = 0;
    if(Myntra.Combo.isConditionMet && Myntra.Combo.solrProducts[styleId].dre_percent>0){
        if(!Myntra.Combo.isConditionMet)
            Myntra.Combo.cartSavings = 0;
        else
            Myntra.Combo.cartSavings = Myntra.Combo.cartTotal * Myntra.Combo.solrProducts[styleId].dre_percent / 100;
    }

    if(Myntra.Combo.addedStyles[styleId]!=null) Myntra.Combo.addedStyles[styleId][skuid] = changedQnty;
    if(Myntra.Combo.allSelectedStyles[styleId]!=null) Myntra.Combo.allSelectedStyles[styleId][skuid] = changedQnty;

    if(Myntra.Combo.removedStyles[styleId]!=null){
        if(qntyReducedBy>=0) {
            Myntra.Combo.removedStyles[styleId][skuid] = qntyReducedBy;
        }else{
            Myntra.Combo.removedStyles[styleId][skuid] = 0;
        }
    }

};

Myntra.Combo.ComboOverlay.RefreshUI = function() {
    if(Myntra.Combo.isConditionMet && Myntra.Combo.freeItem != undefined && Myntra.Combo.freeItem.price != undefined && Myntra.Combo.freeItem.price > 0){
	Myntra.Combo.cartSavings = 0;//Myntra.Combo.freeItem.price;
    } else if(Myntra.Combo.freeItem != undefined && Myntra.Combo.freeItem.price != undefined && Myntra.Combo.freeItem.price > 0 ){
	Myntra.Combo.cartSavings = 0 ;
   }
    var completionMessage = "";

    var discountedTotal = Myntra.Combo.cartTotal - Myntra.Combo.cartSavings;
    var comboOverlayTotalMessage = "<div class=\"cart-discount left\">YOU PAY <span class=\"cost\" style=\"color: #D20000;\">Rs. <strong>";
    if(Myntra.Combo.cartSavings>0){
        $(".acombo .combo-overlay-total .total").html(Math.round(discountedTotal));
		$(".acombo .combo-overlay-total .savings-msg").css("float","none");
		
    } else {
        $(".acombo .combo-overlay-total .total").html(Math.round(Myntra.Combo.cartTotal));
		$(".acombo .combo-overlay-total .savings-msg").css("float","left");
    }

    if(Myntra.Combo.freeItem.price != undefined && Myntra.Combo.freeItem.price !="" ){
    	//$(".acombo .combo-overlay-total .savings").html(Math.round(Myntra.Combo.freeItem.price));
	$(".savings-msg").hide();
	
	}	
    else	
    $(".acombo .combo-overlay-total .savings").html(Math.round(Myntra.Combo.cartSavings));

    if(Myntra.Combo.isConditionMet){
		completionMessage = "<span class=\"tick-small-icon\">&nbsp;</span>COMBO COMPLETE!";// <i>Modify Combo</i>";
    } else {
        if(Myntra.Combo.isAmount){
            completionMessage = "<span class=\"warning-small-icon\">&nbsp;</span>COMBO INCOMPLETE. Rs. "+Math.round(Myntra.Combo.amount_in_combo)+"/- SELECTED SO FAR. <em>Rs. "+Math.round(Myntra.Combo.minMore)+"/- MORE TO GO!</em>";
        } else {
            completionMessage = "<span class=\"warning-small-icon\">&nbsp;</span>COMBO INCOMPLETE. "+Myntra.Combo.quantity_in_combo+" SELECTED SO FAR. <em>"+Myntra.Combo.minMore+" MORE TO GO!</em>";
        }
		//$(".combo-overlay-total .message").val("COMPLETE THE COMBO TO SAVE!");
    }

    var addToCartBtnText = "";
    if(Myntra.Combo.isConditionMet){
		$(".acombo .combo-overlay-total .message").css("display","none");
        addToCartBtnText = "ADD COMBO TO BAG <span style=\"font-size:15px;\">&raquo;</span>";
    }else {
		$(".acombo .combo-overlay-total .message").css("display","block");
        addToCartBtnText = "ADD COMBO TO BAG <span style=\"font-size:15px;\">&raquo;</span>";
    }
	
	
/*
	if(Myntra.Combo.isConditionMet){
		
		$(".price-message .main").html("");
		$(".price-message .discount").html("");
	}else{
        $(".price-message .main").html("");
        $(".price-message .discount").html("");
	}*/
/*    if(Myntra.Combo.addedStyles.length==0 && Myntra.Combo.removedStyles.length==0){//diable the add to cart btn
        //$(".add-to-cart").attr("enabled", "true");
    }else {
        //$(".add-to-cart").attr("enabled", "false");
    }
<p class="price-text red-text discount">{if $isConditionMet}{$rupeesymbol}<span class="strike">{$widgetdata.price|number_format:0:".":","}</span> {/if}</p>
*/
 /*   if(Myntra.Combo.isRemoved){
        $(".cow-replace-item-msg").html("You have replaced your original item(s).");
    } else {
        $(".cow-replace-item-msg").html("&nbsp;");
    }
   */
    $(".acombo .completion-message").html(completionMessage);

};

Myntra.Combo.LoadSizeResponse =  function(styleId, sizes) {
	var sizeNode = $(".size-selection-row-"+styleId+" select");
    // styleid = frm.find(":hidden[name='productStyleId']").val(),
     //   selectedSize = sizeNode.val();
	var selectedSku = parseInt($(".item_"+styleId+" .checkbox").attr("data-skuid"));
	var sizes = sizes[styleId],
        markup = [];
	if (!sizes) { return }
   
    var noOfFields = 0;
    for(key in sizes){
            noOfFields++;
    }
    var isSingleElement = (noOfFields==1)?true:false;
    
	var key, val, sel, dis, qty, qty_h;
    for (key in sizes) {
        dis = '';
        sel = (key==selectedSku)?'selected="true" ':'';
        val = sizes[key][0];
	qty_h = " qty='" +sizes[key][2]+"' ";
        
        if (key==selectedSku || isSingleElement){
                $(".item_"+styleId+" .size-text-sub").html(val);
		var qty = $(".item_"+styleId+" .edit-flip .quantity").val();
                $(".item_"+styleId+" .qnty-text-sub").html(qty);
        }

	if(isSingleElement){
        $(".size-selection-row-"+styleId+" .size-info-box").html(val);
		$(".size-selection-row-"+styleId+" .size-info-box").attr("skuid",key);
		$(".size-selection-row-"+styleId+" .size-info-box").attr("qty",sizes[key][2]);
		$(".size-selection-row-"+styleId+" .size-info-box").removeClass("hide");
		$(".size-selection-row-"+styleId+" .size-info-box").addClass("mk-freesize");	
		$("#cow-size-select-"+styleId).addClass("hide");
		$(".item_"+styleId+" .save-btn").removeAttr('disabled');
		break;
        }else {
	        $(".size-selection-row-"+styleId+" .size-info-box").addClass("hide");
        	$("#cow-size-select-"+styleId).removeClass("hide");
		$(".size-selection-row-"+styleId+" .size-info-box").removeClass("mk-freesize");
	}

        if (sizes[key][2]==0) {
            continue; // don't show the unavailable sizes
        }
        markup.push(['<option value="', key, '" ', sel, dis, qty_h, '>', val, '</option>'].join(''));
    };

	if(markup.length==0 && $(".size-selection-row-"+styleId+" .size-info-box").hasClass('hide')){
    		$(".size-selection-row-"+styleId+" .size-msg").html("SOLD OUT");
		$(".size-selection-row-"+styleId+" .size-info").addClass("hide");
		$(".size-selection-row-"+styleId).parent("cart-widget-item-container").attr('disabled','disabled');
	}
    else{
        sizeNode.append(markup.join("\n"));
	}
	if(isSingleElement){
		var sizeEl = $(".size-selection-row-"+styleId+" .size-info .mk-freesize");
		Myntra.Combo.ModifySizeSetQuantity(sizeEl);
	}
};

Myntra.Combo.LoadSizes = function(styleId) {
	var objId="select#cow-size-select-"+styleId;
    $.ajax({
        type: 'GET',
        url: '/myntra/ajax_getsizes.php?ids=' + styleId,
        dataType: 'json',        
		beforeSend:function(){
            $(objId).empty().append('<option value="-">-</option>');
			$(".size-selection-row-"+styleId+" .size-info-box").html("&nbsp;");
        },
        success: function(data){
			Myntra.Combo.LoadSizeResponse(styleId, data);
		if(Myntra.Combo.MiniPipMode==true) {
			$(".item_"+styleId+" .save-btn").trigger("click"); 
			Myntra.Combo.MiniPipMode = false;
		}
	 	}
    });
};

Myntra.Combo.AddSkuFromMiniPip = function(styleid, skuid){
	Myntra.Combo.MiniPipMode = true;
    $(".item_"+styleid).each(function(index){
        var sid = parseInt($(this).find(":checkbox").attr("data-skuid"));
	var prevSkuid = $(this).find("select[data-skuid]").val();
        //if(sid==0){
            $(this).find(".checkbox[data-skuid]").attr("data-skuid",skuid);
            $(this).find("select[data-skuid]").attr("data-skuid",skuid);
	    //$(this).find("select[data-skuid]").val(skuid);
        //}
		if(prevSkuid != '-'){
        Myntra.Combo.SelectStyle($(this).find(".checkbox")[0], false);
        Myntra.Combo.ComboOverlay.RefreshUI();
        }

            if(!$(this).find(".checkbox").hasClass("checked")){
				//$(this).find(".checkbox").trigger('click');
                Myntra.Combo.SelectStyle($(this).find(".checkbox")[0], false);
                Myntra.Combo.ComboOverlay.RefreshUI();
	    	}
		var noOfElementsAdded = $(".checked").length;
                if($(".checked").attr('disabled')!=null)
                        noOfElementsAdded -= $(".checked[disabled=disabled]").length;
                if(noOfElementsAdded>0){
                        $(".combo-add-to-cart-button").removeAttr("disabled");
                }else{
                        $(".combo-add-to-cart-button").attr("disabled","true");
                }

	$(this).find(".save-btn").removeAttr("disabled");	
	/*var that = this;	
	setTimeout(function(){
		$(that).find(".save-btn").trigger("click")
	},10000);*/

    });
}

Myntra.Combo.SaveSizeQty = function(ccwi) {
	$(ccwi).find(".main-flip .edit-btn").removeClass("hide");
	//$(ccwi).find(".flip-wrapper").quickFlipper();
	$(ccwi).find(".flip-wrapper .edit-flip").addClass("hide");
	var size =$(ccwi).find(".edit-flip .cow-size-loader option:selected").html();
	if(size=='-')
	size = $(ccwi).find(".edit-flip .size-info-box").html();
	var qty = $(ccwi).find(".edit-flip .quantity").val();
	$(ccwi).find(".main-flip .size-text-sub").html(size);
	$(ccwi).find(".main-flip .qnty-text-sub").html(qty);
	$(ccwi).find(".main-flip .size-text-hdr").removeClass("hide");
	$(ccwi).find(".main-flip .qnty-text-hdr").removeClass("hide");
	clearTimeout(Myntra.Combo.SaveTimeOut);
}

Myntra.Combo.ClearSizeQty = function(ccwi) {
	$(ccwi).find(".main-flip .edit-btn").removeClass("hide");
	$(ccwi).find(".edit-flip").addClass("hide");
}


Myntra.Combo.RegisterEvents = function () {
//	$(".flip-wrapper").quickFlip();
	$(".acombo .main-flip .edit-btn").click(function(event) {
		$(this).addClass("hide");
		var styleid = $(this).parents(".cart-combo-widget-image").find(".checkbox").attr("data-styleid");
		 _gaq.push(['_trackEvent', 'combo', 'click_edit_details', '', parseInt(styleid)]);
		$(this).parents(".cart-combo-widget-image").find(".edit-flip").removeClass("hide");
	});	

	$(".acombo .edit-flip .save-btn").click(function(event) {
		if($(this).attr('disabled')=="disabled") return false;
		var selectElement = $(this).parents(".edit-flip").find(".cow-size-loader");
		//Myntra.Combo.ModifySize($(selectElement)[0]);
		//             Myntra.Combo.ModifySize($(selectElement)[0]);

               selectElement = $(selectElement)[0];
               var prevSkuid = $(selectElement).attr("data-skuid");
        if(prevSkuid == '' ) prevSkuid = 0;
    var skuid = $(selectElement).find("option:selected").val();
   if(skuid=='-')
        skuid = $(selectElement).parents(".cart-widget-item-container").find(".size-info-box").attr("skuid");
   //if(prevSkuid==skuid) return;
    $(selectElement).attr("data-skuid", skuid);
        $(selectElement).parents(".cart-widget-item-container").find("select").attr("data-skuid", skuid);
    $(selectElement).parents(".cart-widget-item-container").find(".checkbox").attr("data-skuid", skuid);
    var styleId = $(selectElement).parents(".cart-widget-item-container").find(".checkbox").attr("data-styleid");                           
    Myntra.Combo.allSelectedStyles[styleId][skuid] = Myntra.Combo.allSelectedStyles[styleId][prevSkuid];                                    
    Myntra.Combo.addedStyles[styleId][skuid] = Myntra.Combo.addedStyles[styleId][prevSkuid];                                                
    delete Myntra.Combo.allSelectedStyles[styleId][prevSkuid] ;                                                                             
    delete Myntra.Combo.addedStyles[styleId][prevSkuid];                                                                                    
                                                                                                                                            
                                                                                                                                            
        if(skuid!='-' && skuid!=''){                                                                                                        
                $(selectElement).parents(".size-box").find(".size-msg").html("&nbsp;");                                                     
                $(selectElement).parents(".cart-widget-item-container").find(".save-btn").removeAttr('disabled');                           
        }else{                                                                                                                              
                $(selectElement).parents(".size-box").find(".size-msg").html("PLEASE SELECT A SIZE");                                       
                $(selectElement).parents(".cart-widget-item-container").find(".save-btn").attr('disabled','disabled');                      
        }                                                                                                                                   
                                                                                                                                            

		Myntra.Combo.ComboOverlay.RefreshUI();
		var quantitySelectElement = $(this).parents(".edit-flip").find(".quantity-info").find(".quantity");

		Myntra.Combo.ModifyQuantity($(quantitySelectElement)[0]);
        	//Myntra.Combo.ComboOverlay.RefreshUI();
		$(this).parents(".cart-combo-widget-image").find(".main-flip .edit-btn").removeClass("hide");
		$(this).parents(".edit-flip").addClass("hide");
		var size = $(this).parents(".edit-flip").find(".cow-size-loader option:selected").html();
		if(size=='-')
			size = $(this).parents(".edit-flip").find(".size-info-box").html();
		var qty = $(this).parents(".edit-flip").find(".quantity").val();
		$(this).parents(".cart-combo-widget-image").find(".main-flip .size-text-sub").html(size);
		$(this).parents(".cart-combo-widget-image").find(".main-flip .qnty-text-sub").html(qty);
		$(this).parents(".cart-combo-widget-image").find(".main-flip .size-text-hdr").removeClass("hide");
		$(this).parents(".cart-combo-widget-image").find(".main-flip .qnty-text-hdr").removeClass("hide");

		var styleid = $(this).parents(".cart-combo-widget-image").find(".checkbox").attr("data-styleid");
         _gaq.push(['_trackEvent', 'combo', 'edit-details', 'save', parseInt(styleid)]);

	});

	$(".acombo .cart-widget-box .rs-carousel-action-next").die().live('click',function(event) {
		if(!Myntra.Combo.imagesDownloaded){
			Myntra.Utils.loadImageQueue();		
			Myntra.Combo.imagesDownloaded = true;
		}
	});

    $(".acombo .cart-widget-box .rs-carousel-action-prev").die().live('click',function(event) {
        if(!Myntra.Combo.imagesDownloaded){
            Myntra.Utils.loadImageQueue();
            Myntra.Combo.imagesDownloaded = true;
        }
    });

    $(".acombo .edit-flip .cancel-btn").click(function(event) {
	$(this).parents(".cart-combo-widget-image").find(".main-flip .edit-btn").removeClass("hide");
    	$(this).parents(".edit-flip").addClass("hide");
	var input = $(this).parents(".cart-combo-widget-image").find(".checkbox");
	var skuid = $(input).attr("data-skuid");
	if(skuid==0){
		Myntra.Combo.SelectStyle($(input)[0],false);	
		Myntra.Combo.ComboOverlay.RefreshUI();	
	}
	        var styleid = $(this).parents(".cart-combo-widget-image").find(".checkbox").attr("data-styleid");
         _gaq.push(['_trackEvent', 'combo', 'edit-details', 'cancel', parseInt(styleid)]);
    });

    //checkbox on click event
    $(".acombo .checkbox").click(function(event) {
        var input = $(this)[0];
		var isDisabled = $(input).attr('disabled')!=null ? true : false;
		if(isDisabled) return ;

		var styleId = $(input).attr("data-styleid");
		if($(input).hasClass("unchecked")){
            _gaq.push(['_trackEvent', 'combo', 'select_style', 'check', parseInt(styleId)]);
        } else {
            _gaq.push(['_trackEvent', 'combo', 'select_style', 'uncheck', parseInt(styleId)]);
        }

        Myntra.Combo.SelectStyle(input, false);
        Myntra.Combo.ComboOverlay.RefreshUI();
		
	var noOfElementsAdded = $(".acombo .checked").length;
	if($(".acombo .checked").attr('disabled')!=null)
		noOfElementsAdded -= $(".acombo .checked[disabled=disabled]").length; 
	if(noOfElementsAdded>0){
		$(".acombo .combo-add-to-cart-button").removeAttr("disabled");
	}else{
		$(".acombo .combo-add-to-cart-button").attr("disabled","true");
	}

    });

    lb = Myntra.LightBox('#cart-combo-overlay');
    //cancel the pop-up event
    $(".combo-overlay-cancel").click(function(e){
        _gaq.push(['_trackEvent', 'combo', 'cancel', Myntra.Data.pageName,parseInt(Myntra.Combo.ComboId)]);
		lb.hide();
    });
    //initialize tool-tipi
   /* $("input:checkbox").each(function(index){
        var styleId = $(this).attr("value");
        Myntra.InitTooltip('#cow-tooltip-'+styleId, '#cow-price-'+styleId, {side:'below'});
    });
*/
	//bind on change to the quantity
    $(".acombo .quantity-info .quantity").change(function(event){
        Myntra.Combo.ModifyQuantity($(this)[0]);
        Myntra.Combo.ComboOverlay.RefreshUI();
    });
    $(".acombo .size-info select").change(function(event){
        //Myntra.Combo.ModifySize($(this)[0]);
        //Myntra.Combo.ComboOverlay.RefreshUI();
	Myntra.Combo.ModifySizeSetQuantity($(this)[0]);
	if($(this).find("option:selected").val()!='-'){
                $(this).parents(".cart-widget-item-container").find(".save-btn").removeAttr('disabled');
        }else{
                $(this).parents(".cart-widget-item-container").find(".save-btn").attr('disabled','disabled');
        }

    });
    $('.acombo .quantity-box').keydown(function(event) {
            var key = event.charCode || event.keyCode || 0;
            return (
            key == 8 || key == 9 || key == 46 || key == 13 || (key >= 48 && key <= 57)) && (!event.shiftKey);
    });

    $('.acombo .combo-add-to-cart-button').click(function(){
        var isValidated = true;
        $(".checked").parents(".cart-widget-item-container").find("select option:selected").each(function(index){
            var skuid = $(this).parents(".cart-widget-item-container").find(".size-info-box").attr("skuid");
	    if($(this).val()=="-" && skuid==""){
                $(this).parents(".size-box").find(".size-msg").html("PLEASE SELECT A SIZE");
				isValidated = false;
                //return false;
            }
            if($(this).val()==0){
                isValidated = false;
                return false;
            }
        });
        if(!isValidated)
            return false;

        //gaTrackEvent('discount_combo_layer_add_to_cart','submit','');
        var postArray = [];
        for(var iStyleId in Myntra.Combo.addedStyles){
            for(var iSkuId in Myntra.Combo.addedStyles[iStyleId]){
                var postItem = Object();
                postItem["skuid"] = iSkuId;
                postItem["quantity"] = Myntra.Combo.addedStyles[iStyleId][iSkuId];
                if(postItem["quantity"]!=null && postItem["quantity"] != 0) postArray.push(postItem);
            }
        }
        var postRemoveArray = [];
        var postRemoveItem = Object();
        for(var iStyleId in Myntra.Combo.removedStyles){
            for(var iSkuId in Myntra.Combo.removedStyles[iStyleId]){
                postRemoveItem["skuid"] = iSkuId;
                postRemoveItem["quantity"] = Myntra.Combo.removedStyles[iStyleId][iSkuId];
                postRemoveArray.push(postItem);
            }
        }
        _gaq.push(['_trackEvent', 'buy_now', Myntra.Data.pageName, 'combo_overlay', parseInt(Myntra.Combo.ComboId)]);
		$.ajax({
            type: 'POST',
            url: "/mkretrievedataforcart.php?pagetype=productdetail&lead=recently_viewed",
            data:     {
                'fromComboOverlay' : 1,
                'cartPostData' : JSON.stringify(postArray),
                '_token' : $('input:hidden[name=_token]').val()
            },
            beforeSend:function(){
                Myntra.LightBox('#cart-combo-overlay').showLoading();
            },
            success: function(data){
                window.parent.location = document.location.protocol+'//'+document.location.hostname+"/mkmycart.php";
            },
            error: function(data){
                //alert('Error in saving. Please try again later.');
                return false;
            }
        });

        return false;
    });
}

Myntra.Combo.InitQuickLook = function() {


    //$(".combo-quick-look").die("click");
    //$(".category .item, .product-listing .item").die("mouseover");
    //$(".category .item, .product-listing .item").die("mouseout");

    var lb = null;
    $(".acombo .combo-quick-look").live('click', function(){
            var styleid=$(this).attr("data-styleid");
            var styleurl=$(this).attr("data-href");
            var fromCart=1;
            if(typeof styleid != "undefined" || styleid != ""){
                lb = Myntra.QuickLook.loadQuickLook(styleid, styleurl, fromCart);
                if(typeof $(this).attr("data-widget") != "undefined"){
    				_gaq.push(['_trackEvent', 'quicklook', Myntra.Data.pageName, $(this).attr("data-widget"), parseInt(styleid)]);
    			}
    			else{
    				_gaq.push(['_trackEvent', 'quicklook', window.location.toString(), parseInt(styleid)]);
    			}
            }

    });


    $(".acombo .cart-combo-widget-image").live("mouseover", function(event){
	    if(!$(this).hasClass("cart-widget-item-box-pre-added")){
                $(this).parent().find(".combo-quick-look").show();
	    }
    });

    $(".acombo .cart-combo-widget-image").live("mouseout", function(event){
	if(!$(this).hasClass("cart-widget-item-box-pre-added")){
            $(this).parent().find(".combo-quick-look").hide();
	}
    });

    $("#back-to-combo-actn-btn").live("click", function(event){
	event.stopPropagation();
        lb.hide();
		 _gaq.push(['_trackEvent', 'combo', 'cancel_quicklook', '']);
//        event.stopPropagation();
    });
 var sizeSelected= function(sizeDropCont){
        if(sizeDropCont.find('.mk-size').val()==0){
            return false;
        }
        else
        {
            return true;
        }
    };

    $(".quick-look-combo-btn").live("click", function(event){
        //if(Myntra.MiniPDP.ifSizeOptionSelected()){
			var sizeDropCont=$(this).closest('.mk-product-option-cont');
        if(sizeSelected(sizeDropCont)){
            //gaTrackEvent('quick_look_add_to_combo','submit','');
            var styleid = $('#hiddenFormMiniPIP').find('input[name="productStyleId"]').val();
			var skuid = parseInt(sizeDropCont.find('.mk-size').val());
            //var skuid = parseInt($('#hiddenFormMiniPIP').find('input[name="productSKUID"]').val());
            lb.hide();
			_gaq.push(['_trackEvent', 'combo', 'select_style', 'quicklook', parseInt(styleid)]);
            Myntra.Combo.AddSkuFromMiniPip(styleid, skuid);
        } else {
			$(sizeDropCont).find('.mk-size-error').removeClass('mk-hide');
		}
        event.stopPropagation();
    });



    //Myntra.InitQuickLook();

}

Myntra.Combo.ComboOverlay.Init = function () {
	Myntra.sCombo.ComboOverlay.Init();
	return;
    Myntra.Combo.ReInitializeVariables();
    Myntra.Combo.RegisterEvents();
    Myntra.Combo.SelectStyles();
	Myntra.Combo.ComboScroll();
    Myntra.Combo.ComboOverlay.RefreshUI();
    Myntra.Combo.InitQuickLook();
	_gaq.push(['_trackEvent', 'combo', 'click', Myntra.Data.pageName, parseInt(Myntra.Combo.ComboId)]);
}

Myntra.Combo.ComboScroll = function () {
	$('.cart-widget-box').carousel({
                        itemsPerPage: 5, // number of items to show on each page
                        itemsPerTransition: 5, // number of items moved with each transition
                        noOfRows: 1, // number of rows (see demo)
                        nextPrevActions: true, // whether next and prev links will be included
                        pagination: false, // whether pagination links will be included
                        speed: 'normal', // animation speed
	//});		
						after: function(event, data){
	//$('.mk-carousel-four').bind("carouselAfter", function(event, data) {//function(index, noOfItems){
	    var from_index = $('.cart-widget-box ').carousel('getPage')*5-4;	
		var noOfItems = $('.cart-widget-box ').carousel('getNoOfItems');
		var to_index = from_index+4;
		if(to_index>noOfItems){
			to_index = noOfItems;
		}
		$(".pagination-start").html(from_index);
		$(".pagination-end").html(to_index);
	}
	});
}



/***************CART INITIALIZATION STARTS *****************************/

Myntra.InitCartComboOverlay = function(){        
	$(".combo-completion-btn").live('click', function(){
                //get all styleIds and pass it to 
        	var discountId=$(this).attr("discountId");
                var min=$(this).attr("min");
                var ctype=$(this).attr("ctype");
                var isConditionMet=$(this).attr("isConditionMet");
				var freeCartC = $(this).attr("freeCartC");
				if(freeCartC == undefined) freeCartC = 0;
                if(isConditionMet){
                    //gaTrackEvent('cart_discount_combo_show','show_combo_layer','complete');
                } else {
                    //gaTrackEvent('cart_discount_combo_show','show_combo_layer','incomplete');
                }
		if(typeof discountId != "undefined" || discountId != ""){
			Myntra.CartComboOverlay.loadOverlay(discountId, isConditionMet, min, ctype, freeCartC);
        	}
	});
}

Myntra.CartComboOverlay = (function(){
    var obj = {};
    var lb;
    
    obj.loadOverlay = function(id, isConditionMet,  min, ctype, freeCartC){
    	lb = Myntra.LightBox('#cart-combo-overlay');
    	obj.getData(id, isConditionMet,  min, ctype, freeCartC);
    };
    
    obj.getData = function(id, isConditionMet,  min, ctype, freeCartC){
    	lb.show();
    	lb.clearPopupContent();
    	lb.showLoading();
        $.get( http_loc+"/getCartComboOverlay.php?id="+id+"&icm="+isConditionMet+"&m="+min+"&ct="+ctype+"&freecartc="+freeCartC, null, function(data){
    			lb.hideLoading();
                $("#cart-combo-overlay .bd").html(data);
                //Myntra.Utils.loadImageQueue();
                lb.center();
				if(freeCartC == 1)
					Myntra.sCombo.ComboOverlay.Init();
				else
	                Myntra.Combo.ComboOverlay.Init();
    	});
    };
    
    return obj;
})();

/********************** CART INITIALIZATION ENDS *****************************/

/********************** PDP INITIALIZATION STARTS ***************************/

Myntra.InitPDPComboOverlay = function(){        
	$(".combo-overlay-btn").click(function(){
                var styleId = $(this).attr("data-styleid");
                //get all styleIds and pass it to 
                //gaTrackEvent('pdp_discount_combo_show','show_combo_layer','incomplete');
		if(typeof styleId != "undefined" || styleId != ""){
			Myntra.PDPComboOverlay.loadOverlay(styleId);
        	}
	});
}

Myntra.PDPComboOverlay = (function(){
    var obj = {};
    var lb;
    
    obj.loadOverlay = function(id){
    	lb = Myntra.LightBox('#cart-combo-overlay');
    	obj.getData(id);
    };
    
    obj.getData = function(id){
    	lb.show();
    	lb.clearPopupContent();
    	lb.showLoading();
        $.get( http_loc+"/getPdpComboOverlay.php?sid="+id, null, function(data){
    		lb.hideLoading();
                $("#cart-combo-overlay .bd").html(data);
                //Myntra.Utils.loadImageQueue();
    		lb.center();
                //loadImageQueue();
                Myntra.Combo.ComboOverlay.Init();
    	});
    };
    
    return obj;
})();

/********************** PDP INITIALIZATION ENDS *****************************/
