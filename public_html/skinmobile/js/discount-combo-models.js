Myntra.sCombo.styleModel= function(){
    var	_id = null,
        _uid = null,
        _isModified= false,
        _initialAttributes ={},
        _inCart= false,
        _isChecked=	false,
        _isSizeLoaded=	false,
        _isQuantityLoaded= false,
        _isAvailable=true,
        _itemAddedByDiscountEngine=false;
    this._cp_attributes = {};
    var attributes =  {
        uid: null,
        quantityList:	[],
        sizeList:	[],
        skuid: null,
        quantity: null,
        styleid: null,
        size: null
    };
    this.tojson= function(){
        return attributes;
    },
    this.getId= function(id){
        return _id;
    };
    this.get= function(attr)	{
        return attributes[attr];
    };
    this.geti = function(attr){
        return _initialAttributes[attr];
    };
    this.set= function(attrs){
        if (!attrs) return attributes;
        var now = attributes;
        if(!_initialAttributes){
            _initialAttributes = now;
        }
        for (var attr in attrs) {
            if(!_initialAttributes[attr])
                _initialAttributes[attr] = attrs[attr];
            attributes[attr] = attrs[attr];
            this._cp_attributes[attr] = attrs[attr];
        }
        return attributes;
    };
    this.restore = function(){
        if(_initialAttributes){
            this.set(_initialAttributes);
        }
    };
    this.setInCart= function(){
        _inCart = true;
    };
    this.unsetInCart= function(){
        _inCart = false;
    };
    this.setItemAddedByDiscountEngine= function(){
        _itemAddedByDiscountEngine = true;
    };
    this.itemAddedByDiscountEngine= function(){
        return _itemAddedByDiscountEngine;
    };
    this.setChecked= function(){
        if(_inCart &&
                _initialAttributes['quantity'] == this.get('quantity') &&
                _initialAttributes['skuid'] == this.get('skuid') ){
            _isModified = false;
        }
        _isChecked = true;
    };
    this.setSize= function(size){
        //check for size existence
        this.set({size: size});
    };
    this.setQuantity= function(q){
        this.set({quantity: parseInt(q)});
        //check for existence
    };
    this.unsetChecked= function(){
        if(_inCart){
            _isModified = true;
        }
        _isChecked = false;
    };
    this.isAvailable= function(){
        return _isAvailable;
    };
    this.setIsAvailable= function(isA){
        _isAvailable = isA;
    };
    this.isInCart= function(){
        return _inCart;
    };
    this.isChecked= function(){
        return _isChecked;
    };
    this.isRemovedFromCart= function(){
        return !this.isChecked() && _inCart;
    };
    this.isEditedFromCart= function(){
        return this.isChecked() && _isModified && _inCart && this.get('skuid') != this.geti('skuid');
    };
    this.isAddedToCart= function(){
        return this.isChecked() && (!_inCart || (this.get('skuid') != this.geti('skuid')));
    };

    this.elLi = function(){
        if(this.get('uid') == null) return Myntra.sCombo.Err('stylemodla el li no uid');
        var a = $("#item_combo_lay_"+this.get('uid'));
        return a;
    };
    this.elCheckbox = function(){
        if(this.get('uid') == null) return Myntra.sCombo.Err('stylemodla el checkbbox no uid');
        return $("#item_combo_lay_"+this.get('uid')+"  .checkbox");
    };
    this.elSizeS= function(){
        if(this.get('uid') == null) return Myntra.sCombo.Err('stylemodla el size sleect no uidid');
        if(_isSizeLoaded ){
            var s = this.get('sizeList');
            var cnt=0;
            for(var i in s) cnt++;
                if(cnt <= 1) return $("#item_combo_lay_"+this.get('uid')+" .size-info-box");
                else return $("#item_combo_lay_"+this.get('uid')+" select.cow-size-loader");
        } else {
            return $("#item_combo_lay_"+this.get('uid')+" select.cow-size-loader");
        }
    };

    this.elSizeD=function(){
        if(!this.get('uid')) return Myntra.sCombo.Err('stylemodla el sizeD no uid');
        return $("#item_combo_lay_"+this.get('uid')+" select.cow-size-loader");
    };
    this.elSizeSkuid=function(){
        if(!this.get('uid')) return Myntra.sCombo.Err('stylemodla el sizeD no uid');
        if(_isSizeLoaded ){
            var s = this.get('sizeList');
            var cnt=0;
            for(var i in s) cnt++;
                if(cnt <= 1) return $("#item_combo_lay_"+this.get('uid')+" .size-info-box").attr("skuid");
                else return $("#item_combo_lay_"+this.get('uid')+" select.cow-size-loader").val();
        } else {
            return $("#item_combo_lay_"+this.get('uid')+" select.cow-size-loader").val();
        }
    };
    this.elQuanS= function(){
        if(!this.get('uid')) return Myntra.sCombo.Err('style modal el quanS no styleid');
        var a = $("#item_combo_lay_"+this.get('uid')+" select.quantity");
        return a;
    };
    this.elQuanD= function(){
        if(!this.get('styleid')) return Myntra.sCombo.Err('style modal el quanD no styleid');
        return $('.quantity-selection-row-'+this.get('styleid'));
    };

    this.loadSizes = function(callback_fn){
        if(_isSizeLoaded){
            if(this.isAvailable())
                callback_fn();
            else {
                that.unsetChecked();
                that.elCheckbox().removeClass("checked").addClass("unchecked").attr("disabled","disabled");
                $("#item_combo_lay_"+that.get('uid')+" .price-text").append(' &nbsp; <span style="color:red;">SOLD OUT</span>');
                callback_fn();
                //that.elLi().remove();
            }
        } else {
            var that = this;
            $.ajax({
                type: 'GET',
                url: '/myntra/ajax_getsizes.php?ids=' + this.get('styleid'),
                dataType: 'json',
                beforeSend:function(){
                    $(that.elSizeD()).empty().append('<option value="-">-</option>');
                },
                success: function(data){
                    for(var i in data){
                        that.set({sizeList: data[i]});
                        var cnt = 0, avail_cnt=0;
                        for(var j in data[i]){
                            cnt ++;
                            if(data[i][j][2] == 0 || data[i][j][2] == null){
                                avail_cnt++;
                            }
                        }
                        if(cnt == avail_cnt ) that.setIsAvailable(false);
                    }

                    _isSizeLoaded = true;
                    if(that.isAvailable())
                        callback_fn();
                    else {
                        that.unsetChecked();
                        that.elCheckbox().removeClass("checked").addClass("unchecked").attr("disabled","disabled");
                        $("#item_combo_lay_"+that.get('uid')+" .price-text").append(' &nbsp; <span style="color:red;">SOLD OUT</span>');
                        callback_fn();
                        //that.elLi().remove();
                    }
                }
            });
        }
    };
}