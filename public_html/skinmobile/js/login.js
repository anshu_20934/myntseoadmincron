
Myntra.Utils.trackOmnitureLoginAttempt = function() {
    if(typeof(s_analytics) != "undefined") {
        s_analytics.linkTrackVars='events';
        s_analytics.linkTrackEvents='event7';
        s_analytics.events='event7';
        s_analytics.tl(true, 'o', 'Login Popup');
    }
}
Myntra.Utils.trackOmnitureLoginSuccessful = function() {
    if(typeof(s_analytics) != "undefined") {
        s_analytics.linkTrackVars='events';
        s_analytics.linkTrackEvents='event21';
        s_analytics.events='event21';
        s_analytics.tl(true, 'o', 'Login Success');
    }
}

Myntra.xdMessage = function(msg) {
    $(document).trigger('myntra.xdmsg', msg);
};

Myntra.LoginBox = function(id, cfg) {   
    cfg = cfg || {};
    var obj = Myntra.RightMenu($('.content-slide-wrapper.login-wrapper'), cfg),
        mainDiv = $(id + ' .main-content'),
        fpDiv = $(id + ' .forgot-pass-msg'),
        fpDivEmail = $(id + ' .forgot-pass-msg .email'),
        fbErr = $(id + ' .err-fb'),
        frmLogin = $(id + ' .frm-login'),
        frmSignup = $(id + ' .frm-signup'),
        loginPanel = $(id + ' .login-panel'),
        signUpPanel = $(id + ' .register-panel'),
        mainContent = $(id + ' .main-content'),
        loginUser = frmLogin.find('input[name="email"]'),
        loginPass = frmLogin.find('input[name="password"]'),
        signupUser = frmSignup.find('input[name="email"]'),
        signupPass = frmSignup.find('input[name="password"]'),
        signupMobile = frmSignup.find('input[name="mobile"]'),
        signupGender = frmSignup.find('.selected-gender'),
        loginUserErr = frmLogin.find('.err-user'),
        loginPassErr = frmLogin.find('.err-pass'),
        signupUserErr = frmSignup.find('.err-user'),
        signupPassErr = frmSignup.find('.err-pass'),
        signupMobileErr = frmSignup.find('.err-mobile'),
        signupGenderErr = frmSignup.find('.err-gender'),
        loginBtn = frmLogin.find('.btn-signin'),
        signupBtn = frmSignup.find('.btn-signup'),
        hdTitle = $(id + ' .hd .h2'),
        title = hdTitle.text(),
        tmrTimeout;
    $('<iframe id="mklogin-iframe" name="mklogin-iframe" src="javascript:void(0)"></iframe')
        .css({'position':'absolute', 'height':'0', 'top':'-100px', 'left': '-1000px'})
        .appendTo('body');
    frmLogin.attr('target', 'mklogin-iframe');
    frmSignup.attr('target', 'mklogin-iframe');

    var onmessage = function(e) {
        (e.origin === https_loc || e.origin === http_loc) && $(document).trigger('myntra.xdmsg', e.data);
    };
    window.addEventListener
        ? window.addEventListener('message', onmessage, false)
        : window.attachEvent
            ? window.attachEvent('onmessage', onmessage)
            : null;

    var hideErrors = function() {
        loginUserErr.html('');
        loginPassErr.html('');
        signupUserErr.html('');
        signupPassErr.html('');
        signupMobileErr.html('');
        signupGenderErr.html('');
        fbErr.html('');
    };
    var validateUser = function(user, errUser) {
        if (!user.val()) {
            errUser.text('Please enter your email address');
            return false;
        }
        else if (!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(user.val())) {
            errUser.text('Please enter a valid email address');
            return false;
        }
        else {
            errUser.text('');
            return true;
        }
    };

    var validatePass = function(pass, errPass) {
        if (!pass.val()) {
            errPass.text('Please enter your password');
            return false;
        }
        else if (pass.val().length > 0 && pass.val().length < 6) {
            errPass.text('Minimum 6 characters required');
            return false;
        }
        else {
            errPass.text('');
            return true;
        }
    };

    var validateMobile = function(mobile, errMobile) {
        if (!mobile.val()) {
            errMobile.text('Please enter your mobile number');
            return false;
        }
        else if (!/^\d{10}$/.test(mobile.val())) {
            errMobile.text('Enter a valid mobile number');
            return false;
        }
        else {
            errMobile.text('');
            return true;
        }
    };
        
    var validateGender = function(gender, errGender) {
        if(!gender.length){
            errGender.text('');
            return true;
        }
        else if(gender.text() !="") {
            errGender.text('');
            return true;
            }
        else {
            errGender.text('Please select gender');
            return false;
        }
    };
    
    $(document).bind('myntra.xdmsg', function(e, msg) {
        var data = {},
            kvPairs = msg.split(';'),
            errs, err, i, n, kv;
        for (i = 0, n = kvPairs.length; i < n; i += 1) {
            kv = kvPairs[i].split('=');
            data[kv[0]] = kv[1];
        }
        if (!(data.type === 'signin' || data.type === 'signup')) {
            return;
        }

        if (tmrTimeout) {
            clearTimeout(tmrTimeout);
            tmrTimeout = null;
            Myntra.Utils.Cookie.del('_loginerr');
        }

        if (data.status === 'auth-error') {
            Myntra.Utils.Cookie.set('_loginerr', cfg.invoke + '*' + cfg.action + '*authfail');
            _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'auth_error']);
            obj.reloadPage();
        }
        else if (data.status === 'data-error') {
            obj.hideLoading();
            _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'data_error']);
            errs = data.errors.split('~');
            for (i = 0, n = errs.length; i < n; i += 1) {
                err = errs[i];
                if (err === 'email') {
                    val = 'Email address is invalid';
                    cfg.action === 'signup' ? signupUserErr.html(val) : loginUserErr.html(val);
                }
                if (err === 'pass') {
                    val = 'Please enter your password';
                    cfg.action === 'signup' ? signupPassErr.html(val) : loginPassErr.html(val);
                }
                if (err === 'mobile') {
                    val = 'Mobile number is invalid';
                    if (cfg.action === 'signup') signupMobileErr.html(val);
                }
            }
        }
        else if (data.status === 'success') {
            Myntra.Utils.Cookie.set('_mklogin', cfg.invoke + '*' + cfg.action + '*success');
            Myntra.Utils.trackOmnitureLoginSuccessful();
                obj.reloadPage();
        }
        else if (data.status === 'error') {
            obj.hideLoading();
            if (data.errorcode === '1') {
                if (cfg.action === 'signup') {
                    signupUserErr.html('Email Address already exists.');
                    _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'failure_username_already_exists']);
                }
                else if (cfg.action === 'signin') {
                    _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'failure_password_invalid']);
                    loginUserErr.html('Email Address and Password do not match.');
                }
            }
            else if (data.errorcode === '2') {
                if (cfg.action === 'signin') {
                    _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'failure_login_not_found']);
                    loginUserErr.html('Email Address and Password do not match.');
                }
            }
        }
    });

    var fadeSignup = function() {
    	Myntra.Utils.scrollToPos(0,0);
    	$(".loginbox .heading").text("LOGIN");
        $(".loginbox .login-panel").show();
        $(".loginbox .login-panel").css({'margin-left':'0%'});
        $(".login-content-slider").removeClass('register-active');
        $(".loginbox .register-panel").hide();
        $(".loginbox .login-content-slider").css({'height': $(".loginbox .login-panel").css('height')});
    };

    var fadeLogin = function() {
    	Myntra.Utils.scrollToPos(0,0);
    	$(".loginbox .heading").text("REGISTER");
        $(".loginbox .register-panel").css({'margin-left':'50%'});
        $(".loginbox .register-panel").show();
        $(".login-content-slider").addClass('register-active');
        $(".loginbox .login-panel").hide();
        $(".loginbox .login-content-slider").css({'height': $(".loginbox .register-panel").css('height')});
    };
    obj.showSignup = function() {
            fadeLogin();
            //frmSignup.find('input').filter(':first').focus();
        return false;
    };
    obj.showLogin = function(act) {
            fadeSignup();
            //frmLogin.find('input').filter(':first').focus();
        return false;
    };
    //frmLogin.find('input').focus(fadeSignup);
        $('.loginbox .btn-login-now').click(fadeSignup);
        $('.loginbox .btn-register-now').click(fadeLogin);        
        $(".login-wrapper .btn-register-now").click();    //initiating
    var handleTimeout = function() {
        tmrTimeout = setTimeout(function() {
            Myntra.Utils.Cookie.set('_loginerr', cfg.invoke + '*' + cfg.action + '*timeout');
            location.reload();
        }, Myntra.Data.loginTimeout);
    };

    frmLogin.submit(function(e) {
        var r1 = validateUser(loginUser, loginUserErr),
            r2 = validatePass(loginPass, loginPassErr);
        if (r1 && r2) {
            cfg.action = 'signin';
            obj.showLoading();
            handleTimeout();
        }
        else {
            Myntra.Utils.preventDefault(e);
            return;
        }
    });

    frmSignup.submit(function(e) {
        var r1 = validateUser(signupUser, signupUserErr),
            r2 = validatePass(signupPass, signupPassErr),
            r3 = validateMobile(signupMobile, signupMobileErr);
            r4 = validateGender(signupGender,signupGenderErr)
        if (r1 && r2 && r3 && r4) {
            cfg.action = 'signup';
            obj.showLoading();
            handleTimeout();
        }
        else {
            Myntra.Utils.preventDefault(e);
            return;
        }
    });

    // forgot password interaction
     $(id + ' .btn-forgot-pass-ok').click(function(e) {
        obj.hide();
        _gaq.push(['_trackEvent', cfg.invoke, 'forgot_passoword_popup', 'continue_shopping']);
    });
    var showForgotPassMsg = function() {
        var btnOk = $(id + ' .btn-forgot-pass-ok');
        
        $(".loginbox .heading").text("NO WORRIES!");
        fpDivEmail.html(loginUser.val());
        mainDiv.hide();
        fpDiv.show();
        hideForgotPassMsg = function() {
            $(id + ' .mod').removeClass('forgot-pw');
            $(".loginbox .heading").text("LOGIN");
            mainDiv.show();
            fpDiv.hide();
            hideForgotPassMsg = function(){};
        }
    };
    var hideForgotPassMsg = function(){};
    $(id + ' .btn-forgot-pass').click(function(e) {
        e.preventDefault();
        if (validateUser(loginUser, loginUserErr)) {
            $.ajax({
                type: "POST",
                url: "/reset_password.php",
                data: "action=FORGOTPASSWORD&username=" + loginUser.val(),
                beforeSend:function(){
                    obj.showLoading();
                },
                success: function(msg){
                    msg = $.trim(msg);
                    if (msg === 'success') {
                        showForgotPassMsg();
                    }
                    else {
                        loginUserErr.html("This email is not registered with Myntra.com");
                    }
                },

                
                complete: function(xhr, status) {
                    obj.hideLoading();
                }
            });
            _gaq.push(['_trackEvent', cfg.invoke, 'forgot_password',window.location.toString()]);
        } else {
            Myntra.Utils.preventDefault(e);
            return;
        }
    });

    // facebook connect interaction
    $(id + ' .btn-fb-connect').click(function() {
        _gaq.push(['_trackEvent', cfg.invoke, 'fb_connect', 'click']);
        var referer = $(this).attr('data-referer');
        Myntra.FBConnect(referer);
    });
    $(document).bind('myntra.fblogin.auth', function(e, data) {
        obj.showLoading();
    });
    $(document).bind('myntra.fblogin.done', function(e, data) {
        obj.hideLoading();
        switch (data.status) {
        case 'success':
            if (cfg.isPopup) { obj.hide(); }
            Myntra.Utils.Cookie.set('_mklogin', cfg.invoke + '*fb_connect*success_' + data.fb_action);
            if(Myntra.Utils.Cookie.get('_sku')){
                if(typeof Myntra.Data.MiniPIPsocialExclude != 'undefined'){
                    Myntra.Utils.Cookie.set('_saveitem_ex_social',Myntra.Data.MiniPIPsocialExclude);
                }
                else {
                    Myntra.Utils.Cookie.set('_saveitem_ex_social',Myntra.Data.PDPsocialExclude);
                }
            }
            if(typeof(s_analytics) != "undefined") {
                s_analytics.eVar18='success';
            }
            Myntra.Utils.trackOmnitureLoginSuccessful();
            obj.reloadPage();
            break;
        case 'failure':
            _gaq.push(['_trackEvent', cfg.invoke, 'fb_connect', 'failure']);
            fbErr.html('Connect with Facebook failed.');
            break;
        case 'access-denied':
            _gaq.push(['_trackEvent', cfg.invoke, 'fb_connect', 'access-denied']);
            fbErr.html('Facebook Connect failed. Permission denied.');
        }
    });

    obj.reloadPage = function(){
        if (typeof cfg.onLoginDone === "function") {
            obj.hide();
            Myntra.Data.userEmail = cfg.action === 'signin' ? loginUser.val() : signupUser.val();
            cfg.onLoginDone();
        }
/*        else if (location.pathname == '/mkmycartmultiple_re.php'
                || location.pathname == '/mkmycartmultiple.php'
                || location.pathname == '/mkmycart.php') {
            location.href = https_loc + '/checkout-address.php';
        }
*/        else {
            location.reload();
        }
    };

    obj.adjustHeight = function() {
        loginPanel.height(signUpPanel.height());
    };

    obj.showErrMsg = function(err) {
        err.action == 'signin' ? loginUserErr.html(err.message) : signupUserErr.html(err.message);
        _gaq.push(['_trackEvent', cfg.invoke, err.action, err.name]);
    };

    // override show method
    var p_show = obj.show;
    obj.show = function() {
        hideForgotPassMsg();
        hideErrors();
        p_show.call(this);
        obj.adjustHeight();
    };

    obj.setInvokeType = function(val) {
        cfg.invoke = val;
    };

    obj.setCfg = function(name, value){
        cfg[name]=value;
    };

   obj.changeGender = function(gender_obj) {
        if(gender_obj.hasClass("btn-change-gender-off"))
        {
            gender_obj.removeClass("btn-change-gender-off");
            gender_obj.closest('.btn').removeClass("wrapper-change-gender-off");
            $('.selected-gender').toggleClass("flo-left");
            $('.selected-gender').html("MALE");
        }
    else
        {
            gender_obj.addClass("btn-change-gender-off");
            gender_obj.closest('.btn').addClass("wrapper-change-gender-off");
            $('.selected-gender').toggleClass("flo-left");
            $('.selected-gender').html("FEMALE");
        }
    }
    
    return obj;
};

Myntra.InitLogin = function(id) {
    if (!$(id).length) { return; }
    var cfg = {isModal:false, width:'90'};
    var obj = Myntra.LoginBox(id, cfg);
    $(document).bind('myntra.login.hide', function(e, data) {
    	obj.hide();
    });
    $(document).bind('myntra.login.show', function(e, data) {
        var invoke = data && data.invoke? data.invoke : 'header';
        obj.setInvokeType(invoke);
        if (data && data.onLoginDone) {
            obj.setCfg("onLoginDone", data.onLoginDone);
        }
        if (data && data.afterHide) {
            obj.setCfg("afterHide", data.afterHide);
        }
        obj.setCfg("currentAnimAction", 'login');
        obj.show();
        $("#gender-selector").swipe({
            click: function(){
                var _obj = $("#gender-selector .gender-selector-sm");
                obj.changeGender(_obj);
            },
            swipe: function(){
                var _obj = $("#gender-selector .gender-selector-sm");
                obj.changeGender(_obj);
            },
            triggerOnTouchEnd: true,
            allowPageScroll: false
        });        
        
        Myntra.Utils.trackOmnitureLoginAttempt();

        if (data && data.action == 'login') {
            obj.showLogin(data.action);
        }
        if (data && data.showerr) {
            var act = data.action == 'signin' ? 'login' : 'sign up';
            data.message = 'Something went wrong. Please ' + act + ' again';
            obj.showErrMsg(data);
        }
        if(data && data.sessionExpired){
            obj.showLogin();
            data.message = 'Your session is expired. Please login again';
            obj.showErrMsg(data);
        }
    });

    // exclude some pages from showing the splash screen
    if (location.pathname == '/register.php' || location.pathname == '/mksystemerror.php') {
        return true;
    }
    
    //$('.gender-sel').parent().jqTransform();

    var ckLoginErr = Myntra.Utils.Cookie.get('_loginerr');
    if (ckLoginErr) {
        Myntra.Utils.Cookie.del('_loginerr');
        var parts = ckLoginErr.split('*');
        $(document).trigger('myntra.login.show', {showerr:1, invoke:parts[0], action:parts[1], name:parts[2]});
    }


    if(typeof Myntra.Data.EmailUUID != "undefined" && Myntra.Data.EmailUUID != "" && Myntra.Data.EmailUUID != null && Myntra.Data.userEmail == ""){
        $(document).trigger('myntra.login.show', [{onLoginDone: function() {
            var giftCardAjaxHelperUrl = http_loc+"/giftCardsAjaxHelper.php?";
            $.ajax({
                url: giftCardAjaxHelperUrl+"type=gc&mode=activate&gc="+Myntra.Data.EmailUUID,
                success: function(data) {
                    var result = $.parseJSON(data);
                    if(result.status != "failed"){
                        Myntra.Utils.Cookie.set('_mkgc', 'success*'+result.message);
                    }
                    else{
                        Myntra.Utils.Cookie.set('_mkgc', 'failed*'+result.message);    
                    }
                    location.href = '/';
            }});    
        }}]);
        Myntra.Utils.trackOmnitureLoginAttempt();
        Myntra.Utils.Cookie.set('splash', 1, 30);
    }
    else{
        if (!Myntra.Utils.Cookie.get('splash')) {
            // condition: query param ".nsp" should NOT present AND user is NOT logged in
            /*
            if (!Myntra.Utils.getQueryParam('.nsp') && !Myntra.Data.userEmail && $('#loginSplashVariant').val() != 'test') {
                obj.setInvokeType('splash');
                obj.show();
                Myntra.Utils.trackOmnitureLoginAttempt();
            }
            */
            Myntra.Utils.Cookie.set('splash', 1, 30);
        }
    }
    return true;
};

Myntra.InitRegLogin = function(id) {
    if (!$(id).length) { return; }
    var cfg = {isPopup:false, width:'90'};
    var obj = Myntra.LoginBox(id, cfg);
    //$('.gender-sel').parent().jqTransform();
    obj.setInvokeType('reg_page');
    var view = Myntra.Utils.getQueryParam('view');
    if (location.pathname === '/register.php' && view) {
        obj.showLogin();
        obj.setCfg("onLoginDone", function() { location.reload(); });
    }
    else {
        obj.showSignup();
        obj.setCfg("onLoginDone", function() { location.reload(); });
    }
    obj.adjustHeight();

    var ckLoginErr = Myntra.Utils.Cookie.get('_loginerr');
    if (ckLoginErr) {
        Myntra.Utils.Cookie.del('_loginerr');
        var parts = ckLoginErr.split('*');
        var err = {showerr:1, invoke:parts[0], action:parts[1], name:parts[2]};
        var act = err.action == 'signin' ? 'login' : 'sign up';
        err.message = 'Something went wrong. Please ' + act + ' again';
        obj.showErrMsg(err);
    }

    return true;
};


$(document).ready(function() {
    $('#reg-login').length ? Myntra.InitRegLogin('#reg-login') : Myntra.InitLogin('#lb-login');
    if(Modernizr.safari) {
        $('input').css("line-height","1");
    }
    $('.reg-wrapper input[type="password"]').blur();
    $('.main-login-page .close-wrapper').fastClick(function() {
    	$(document).trigger('myntra.login.hide');
    });
    if(Myntra.Utils.Cookie.get('__prompt_login')){
        Myntra.Utils.Cookie.del('__prompt_login');
        Myntra.expiryLoginPrompt();
    }
});

//depopulate sign up fields
$(window).load(function() {
    setTimeout(function() {
        $('.loginbox .frm-signup input[type=text], .loginbox .frm-signup input[type=password]').val('');
    }, 1500);
});

Myntra.expiryLoginPrompt =function(){
	$('#lb-login .close').hide();
	$(document).trigger('myntra.login.show', {onLoginDone: function() {
		if(Myntra.Utils.getQueryParam('promptlogin',location.href)){
            var newQS=Myntra.Utils.removeQueryParam('promptlogin',location.href);
            window.location=location.protocol+'//'+location.hostname + location.pathname + '?' +newQS; 
        }
        else {
            location.reload();                
        }

	},action:'signin',sessionExpired:true,isModal:true});
};

Myntra.expiryReloadLoginPrompt = function(){
Myntra.Utils.Cookie.set('__prompt_login', 1, 30);
location.reload();
}
