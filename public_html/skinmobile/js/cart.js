
$(document).ready(function() {
    $('label').click(function() {});
    initCouponBox();     // initialize coupon box
    var obj=$(".apply-coupons-link");
    var func= function(e){
    	Myntra.Utils.stopPropagation(e);
        Myntra.Utils.preventDefault(e);
        if(Myntra.Data.userEmail=="" || Myntra.Data.userEmail == null)
            {
                $(document).trigger('myntra.login.show', [{action: 'login', onLoginDone : function(){
                    var loc = window.location.href;
                    var isCoupon = "&selectCoupons=true";
                    if(loc.indexOf("?") != -1) {
                        isCoupon = "&selectCoupons=true";
                    }  else {
                        isCoupon = "?selectCoupons=true";
                    }
                    if(loc.indexOf("#") != -1) {
                        loc = window.location.href.substring(0, loc.indexOf("#")) + isCoupon + window.location.href.substring(loc.indexOf("#"));
                    }  else {
                        loc += isCoupon;
                    }
                    window.location.href= loc;
                }}]);
            } else {
                showCouponBox();    
            }
};
Myntra.Utils.extendClickEvent(obj,func);
    var loc = window.location.href;
    var couponsShowRequest = Myntra.Utils.getQueryParam('selectCoupons',loc);
    if(couponsShowRequest=='true'){
        showCouponBox();
    }
    initEditBox();        // initialize edit box
    var obj=$(".mk-edit");
    var func= function(e){
        var formobj =$('#lb-editbag form.savecartchanges');
        var currentProdId = "product_view_"+$(e.target).closest('.action').find('input[name="itemId"]').val();
        $('#lb-editbag .lb-prod-view-id').val(currentProdId);
        var moveToWishlistBagLink = $('#lb-editbag .move-to-wishlistbag-link div');
        var removeFromWishlistBagLink = $('#lb-editbag .remove-from-wishlistbag-link div');
        if($(e.target).closest('.save-list').length>0) {
            formobj.find('input[name="itemType"]').val('wishlist-item');
            if(moveToWishlistBagLink.hasClass('lb-move-to-wishlist')){
                moveToWishlistBagLink.removeClass('lb-move-to-wishlist')
            }
            moveToWishlistBagLink.addClass('lb-move-to-bag');
            moveToWishlistBagLink.text('MOVE TO BAG');
            removeFromWishlistBagLink.addClass('lb-remove-saved-item');
            removeFromWishlistBagLink.text('REMOVE FROM WISHLIST');
        }else {
            formobj.find('input[name="itemType"]').val('cart-item');
            if(moveToWishlistBagLink.hasClass('lb-move-to-bag')){
                moveToWishlistBagLink.removeClass('lb-move-to-bag')
            }
            moveToWishlistBagLink.addClass('lb-move-to-wishlist');
            moveToWishlistBagLink.text('MOVE TO WISHLIST');
            removeFromWishlistBagLink.addClass('lb-remove');
            removeFromWishlistBagLink.text('REMOVE FROM BAG');
        }
        showEditBox($(e.target).closest('.prod-item'));
    };
    Myntra.Utils.extendClickEvent(obj,func);
    $('#cart-discount-exclusion-tt').fastClick(function(){
        if($(this).hasClass("tt-visible")){
            $(this).removeClass("tt-visible");
            $(this).closest(".cart-discount-msg").find(".cart-tt-text").remove();
        }
        else{
            var str=$(this).attr("title");
            $(this).after("<span class='cart-tt-text'><br/>" + str + "</span>");
            $(this).addClass("tt-visible");
        }
    });
    var newCartCreatedTime = Myntra.Utils.Cookie.get("ncc");
    if(typeof newCartCreatedTime != "undefined" && newCartCreatedTime != "null"){
        _gaq.push(['_trackEvent', 'new_cart_created', newCartCreatedTime, null, null, true]);
    }
    Myntra.Utils.Cookie.del("ncc");
    
    var obj=$(".cart-page .btn-place-order.active");
    var func= function(e){
        Myntra.Utils.preventDefault(e);
        if (Myntra.Data.userEmail || Myntra.Data.enableGuestLogin) {
            location.href = https_loc + '/checkout-address.php';
        }
        else {
            $(document).trigger('myntra.login.show', [{onLoginDone: function() {location.href = https_loc + '/checkout-address.php';}}]);
        }
    };
    Myntra.Utils.extendClickEvent(obj,func);
    $(".cart-page .shoppping-bag-header").click(function(){
        $(".cart-page .bag-wrapper").show();
        $(".cart-page .wishlist-wrapper").css({'margin-left':'0%'});
        $(".bag-slider-wrapper").removeClass('wishlist-active');
        $(".cart-page .tabbed-header h2").removeClass('selected-tab');
        $(".cart-page .shoppping-bag-header").addClass('selected-tab');
        $(".cart-page .wishlist-wrapper").hide();
        $(".cart-page .wishlist-wrapper").removeClass('allReadyWishList');    
        $(".cart-page .bag-slider-wrapper").css({'height': $(".cart-page .bag-wrapper").css('height')});
    });

    $(".cart-page .wishlist-header").click(function(){
        $(".cart-page .wishlist-wrapper").css({'margin-left':'50%'});
        $(".cart-page .wishlist-wrapper").show();
        $(".bag-slider-wrapper").addClass('wishlist-active');
        $(".cart-page .tabbed-header h2").removeClass('selected-tab');
        $(".cart-page .wishlist-header").addClass('selected-tab');
        $(".cart-page .bag-wrapper").hide();
        $(".cart-page .bag-slider-wrapper").css({'height': $(".cart-page .wishlist-wrapper").css('height')});
    });

    var loc = window.location.href;
    var patt=/wishlist/g;
    var isWishlistRequest = patt.test(loc);
    if(!isWishlistRequest) {
        $(".cart-page .shoppping-bag-header").click();    //initiating
    }  else {
        $(".cart-page .wishlist-header").click();    //initiating
    }
    
    var obj=$(".cart-page .btn-place-order.inactive");
    var func= function(e){
        Myntra.Utils.preventDefault(e);
        Myntra.Utils.scrollToPos(0);
        alert("One or more items are out of stock.");
    };
    Myntra.Utils.extendClickEvent(obj,func);

    var dataSizes = [], styleIds = [];
    // have to remove this four lines below.. its only hardcoded testing data - 
    
    var loadSizes = function(frm) {
        if (frm.data('sizes-loaded')) { 
            return; 
        }
        var sizeNode = frm.find('.sel-size'),
            styleId = frm.find('input[name="styleid"]').val(),
            selectedSize = sizeNode.val(),
            sizes = dataSizes[styleId],
            markup = [];
            sizeMarkup=[];
        if (!sizes) { return; }
        var count=0;

        if(count == 1) {
            var sizename = $(sizeNode).attr("name");
            var sizetext = $(sizeNode).find("option").text();
            $(sizeNode).before('<span class="temp-size-sel mk-freesize"></span>');
            $(sizeNode).remove();
            tempSizeSpan = frm.find('.temp-size-sel');
            $(tempSizeSpan).html(sizetext);
            $(tempSizeSpan).append('<input type="hidden" name="'+sizename+'" val="'+selectedSize+'" />');
            return;
        }
        var key, val, sel, title, dis;
        var cartItemName = frm.find('input[name="cartitem"]').val();
        if(typeof(cartItemName)== "undefined")
            {
            cartItemName = frm.find('input[name="itemId"]').val();
            }
        for (key in sizes) {
            title = ''; dis = '';
            sel = key == selectedSize ? ' selected="true"' : '';
            val = sizes[key][0];
            var isAvailable = "";
            var isDisabled="";
            if (!sizes[key][1]) {
                //Kundan: PORTAL-2115: in enabled state
                //continue; //If we don't want to show thShow OOS sizes also in the combo for size on cart page e unavailable sizes, uncomment this line
                //title = 'title="SOLD-OUT"';//On hover of the option, show sold-out
                //dis = ' disabled="disabled"';//disable the out of stock option
                //val += ' - SOLD OUT';//append sold-out next to the size-option
            }
            markup.push(['<option value="', key, '"', sel, dis, title, '>', val, '</option>'].join(''));
            if(sizes[key][1]==false){
                isAvailable="unavailable";
                isDisabled="disabled";
            }
            var freesize="Freesize";
            var onesize="Onesize";
            if(sizes[key][0].toString().toLowerCase()==freesize.toString().toLowerCase() || sizes[key][0].toString().toLowerCase()==onesize.toString().toLowerCase())
                sizeMarkup = "<div class='btn-onesize'>"+sizes[key][0]+"</div><button type='button' class='"+isAvailable+" btn btn-size btn-grey selected' style='display:none' value="+key+" name='"+cartItemName+"size'>"+sizes[key][0]+"</button>";
            else {
                if(sizes[key][0].toString().toLowerCase() == ($('#product_view_'+cartItemName+' .prod-selected-size').text()).toString().toLowerCase())
                    sizeMarkup = sizeMarkup +"<button type='button' "+isDisabled+" class='"+isAvailable+" btn btn-size btn-grey selected' value="+key+" name='"+cartItemName+"size'>"+sizes[key][0]+"</button>";
                else
                    sizeMarkup = sizeMarkup +"<button type='button' "+isDisabled+" class='"+isAvailable+" btn btn-size btn-grey' value="+key+" name='"+cartItemName+"size'>"+sizes[key][0]+"</button>";

            }
        };
        frm.closest('.prod-item').find('.size-qty-wrap').find('.ps-size-wrapper').html(sizeMarkup);
        if(markup.length) {
            sizeNode.html(markup.join("\n")); //.removeClass('jqTransformHidden').jqTransSelect();
        }
        frm.data('sizes-loaded', true);
    }; 

    $('.cart-page .prod-set input[name="styleid"]').each(function(i, el) {
        styleIds.push(el.value);
    });
    $.ajax({
        type: 'GET',
        url: '/myntra/ajax_getsizes.php?ids=' + styleIds.join(','),
        dataType: 'json',
        beforeSend:function() {
        },
        success: function(resp) {
            if (!resp) {
                return;
            }
            dataSizes = resp;
            $('.form-cart-item, .form-saved-item').each(function(i, el) {
                loadSizes($(el));
            });
        },
        complete: function(xhr, status) {
            $(document).trigger('myntra.editBox.show'); 
        },
        error: function(xhr, textStatus, errorThrown) {
        }
    });


    function disablePlaceOrder() {
        $(".btn-place-order").attr("disabled","disabled");
    }

    function enablePlaceOrder() {
        $(".btn-place-order").removeAttr("disabled");
    }
});

Myntra.CouponBox = function(id) {
	var cfg={};
    var couponListAnimobj = Myntra.ListAnimation($('.content-slide-wrapper.coupons-wrapper'));
    var couponRightMenuobj = Myntra.RightMenu($('.content-slide-wrapper.coupons-wrapper'),cfg,couponListAnimobj);
    $(document).bind('myntra.couponBox.show',function(e, data) {
        couponRightMenuobj.show();
        reset();
    });
    var promoCoupons = [];
    var firstFetch = true;
    /* Cart Page counpon redeem */
    function fetchCoupons() {
        var coupons = [];
        $.ajax({
            type: 'GET',
            url: '/myntra-absolut-service/absolut/myntcoupons',
            dataType: 'json',
            async: false,
            success: function(resp) {
                if (resp['status']['statusType'] == 'SUCCESS') {
                    coupons = resp['data'];
                }
            },
            error: function(xhr, textStatus, errorThrown) {
            }
        });
        
        
        
        return coupons;
    }

    function redeemCoupon() {
        var string = $("#lb-selected-coupon-code").val();
        var pattern = /^[a-zA-Z0-9\s]+$/g;
        if (string == '' || !pattern.test(string)|| string == "Enter a Coupon") {
            $('.lb-coupon-error-msg').html("Please enter a valid coupon code to redeem.");
            return;
        }
        _gaq.push([ '_trackEvent', 'payment_page', 'coupon_widget','coupon_redeem' ]);
            var data = {"couponcode" : string};
            couponRightMenuobj.showLoading();
            $.ajax({
                type: 'POST',
                url: '/ajaxCartHelper.php',
                data: data,
                dataType: 'json',
                success: function(resp) {
                    if(resp.status == "SUCCESS") {
                        $('.coupon-msg').html(resp.data.couponMessage);
                        $('.total-items-value').html("ITEM TOTAL <span class='rupees red'>Rs. "+ resp.data.totalMRP+"</span>");
                        $('.total-coupon-discount').html("COUPON DISCOUNT (-) Rs. <span class='rupees green'><span class='coupon-value'>"+resp.data.couponDiscount+"</span></span>");
                        if (resp.data.vat > 0) {
                            $('#vat-spot').removeClass("hide").html('<p class="total-vatcharge-fee">VAT Collected (+) <span class="rupees">Rs. </span><span class="red">'+resp.data.vat+'</span></p>');
                        }
                            $('.final-calculated-value .total-value').html(resp.data.totalAmount);
                            $('.total .total-shipping-fee').html(resp.data.shippingText);
                            $('.cart-context-msg.apply-coupons').hide();
                            $('.cart-context-msg.remove-applied-coupons').show();
                            $('#lb-coupons .close-wrapper').trigger('click');
                            _gaq.push([ '_trackEvent', 'Coupons', string.toUpperCase(),'success']);
                    }
                    else {
                        _gaq.push([ '_trackEvent', 'Coupons', string.toUpperCase(),'error: '+resp.data.couponMessage]);
                        $('#lb-coupons .lb-coupon-error-msg').html(resp.data.couponMessage);
                        }
                        couponRightMenuobj.hideLoading();
                },
                complete: function(xhr, status) {
                    couponRightMenuobj.hideLoading();
                },
                error: function(xhr, textStatus, errorThrown) {
                    $('#lb-coupons .lb-coupon-error-msg').html(errorThrown);
                    _gaq.push([ '_trackEvent', 'Coupons', string.toUpperCase(),'error: network']);
                }
            });
    }
    function removeCoupon()
    {
        var string = $("#lb-selected-coupon-code").val();
        _gaq.push(['_trackEvent', 'payment_page', 'coupon_widget', 'remove_coupon']);
            var data = {"removecouponcode" : "remove"};
            Myntra.Utils.extendObjToScreen($(".lightbox-shim"));
            $('.lightbox-shim').show();
            $.ajax({
                type: 'POST',
                url: '/ajaxCartHelper.php',
                data: data,
                dataType: 'json',
                success: function(resp) {
                    $('.cart-context-msg .coupon-msg').html(resp.data.couponMessage);
                    $('.total-items-value').html("");
                    $('.total-coupon-discount').html('');
                    $('.final-calculated-value .total-value').html(resp.data.totalAmount);
                    $('.total .total-shipping-fee').html(resp.data.shippingText);
                    $('.cart-context-msg.apply-coupons').show();
                    $('.cart-context-msg.remove-applied-coupons').hide();
                    $('.lightbox-shim').hide();
                    if (resp.data.vat <= 0) {
                        $('#vat-spot').addClass("hide");
                    }
                    _gaq.push([ '_trackEvent', 'Coupons', string.toUpperCase(),'remove']);
                },
                complete: function(xhr, status) {
                },
                error: function(xhr, textStatus, errorThrown) {
                    $('#lb-coupons .lb-coupon-error-msg').html(errorThrown);
                    _gaq.push([ '_trackEvent', 'Coupons', string.toUpperCase(),'error: network']);
                }
            });
    }
    function reset() {
       $($('#lb-coupons .enter-coupon-code-link .anim-link')[0]).trigger('click');
        var obj = $("#lb-coupons .applicable-coupon-radio:first");
        if(obj.length>0) {
            obj.closest('div').find('.coupon-desc').show();
            $("#lb-coupons :radio").removeClass("selected").closest('.eligible-coupon-item').removeClass("selected");
            obj.addClass("selected").closest('.eligible-coupon-item').addClass("selected");
            var couponNum = obj.attr('id');
            $("#lb-selected-coupon-code").val(couponNum);
            obj.attr('checked', true);
        }
        $('.lb-coupon-error-msg').html('');
        $(".promo-coupons").show();
        
    }
    function init() {
        $("#lb-coupons .choose-coupons-span").on("click",function(e){
        	$("#lb-coupons .header").text('CHOOSE COUPONS');
        });
        $("#lb-coupons .enter-coupon-span").on("click",function(e){
            if (promoCoupons.length == 0 && firstFetch) {
                 if (Myntra.Data.promoCouponsMobileVariant == 'validate') {
                    couponRightMenuobj.showLoading();
                    $.each(fetchCoupons(), function(index, coupon) {
                        if (coupon.displayType == 'OPEN' || coupon.displayType == 'TAGGED' ) {
                            promoCoupons.push(coupon);
                        }
                    });
                    
                    couponRightMenuobj.hideLoading();
				}
                firstFetch = false;
            }

            var couponItems = "";
            $.each(promoCoupons, function(index, promoCoupon) {
                var grayText = "";
                var redText = "";
                if(!promoCoupon.applicable){
                    grayText = " gray ";
                }else{
                     redText = " red ";                 
                }
 
                var descCondense = "";
                if(promoCoupon.couponDescriptionText.length > 15){
                    descCondense = " condense-line ";
                    promoCoupon.couponDescriptionText = promoCoupon.couponDescriptionText.split("on")[0];
                }              
                
                var benifitText = " ";
                
                if(promoCoupon.minMore > 0){
                	benifitText = "Shop for Rs "+Math.ceil(promoCoupon.minMore)+" more to apply";
                }else if (typeof(promoCoupon.benefit) != "undefined"){
                	benifitText = "Save Rs "+Math.floor(promoCoupon.benefit)+" "; 
                }

                var expiryText = (new Date(promoCoupon.expiryDate)).toDateString();
 
                var couponText = "<div class='eligible-coupon-item'>                    <input type='radio' id='"+promoCoupon.coupon+"' class='applicable-coupon-radio' name='applicable-coupon-radio' value='"+promoCoupon.coupon+"'>                    <label for="+promoCoupon.coupon+" class='coupon-off'>                        <span class='rupees "+redText+descCondense+" '>"+promoCoupon.couponDescriptionText+"                        </span>                    </label>                    <div class='coupon-off-amount gray "+redText+"'>"+benifitText+" </div>                    <div class='coupon-desc'>                        <p>Expiry: "+expiryText+"  </p>                        <p>Minimum Purchase: <span class='rupees'> Rs "+(promoCoupon.conditionMin)+"</span></p>                        <p class=' "+grayText+" '>Code: "+promoCoupon.coupon+"</p>                    </div>                </div>";
                
                couponItems +=couponText;
            });
            
            if(promoCoupons.length > 0){
                couponItems = '<div class="applicable-coupons title">APPLICABLE COUPONS <span class="gray">['+promoCoupons.length+']</span></div>'+couponItems;
            }
            couponItems = '<div class = "choose-coupons-body">'+couponItems+'</div>';

            $(".promo-coupons").html(couponItems);
            couponListAnimobj.recalculateObjHeight();
            $(".promo-coupon").on("click", function(e){
                $("#lb-selected-coupon-code").val($(this).text());
                $("#othercouponcode").val($(this).text());
                $(".promo-coupons").hide();
            });
        	$("#lb-coupons .header").text('ENTER COUPON');
        });
        $("#lb-coupons .back").on("click",function(e){
        	$("#lb-coupons .header").text('APPLY COUPONS');
        });
        $("#othercouponcode").keydown(function(e) {
            if (e.keyCode == '13') {
                Myntra.Utils.preventDefault(e);
                redeemCoupon();
            }
            $("#lb-selected-coupon-code").val($(this).val());
        });
        $(".btn-apply-user-coupon").on("click", function(e){
            Myntra.Utils.preventDefault(e);
            redeemCoupon();
        });
        $("#othercouponcode").on("click", function(e){
            $(".promo-coupons").show();
            couponListAnimobj.recalculateObjHeight();
        });


        $('#lb-coupons .anim-link').fastClick(function(){
            var detailsDiv = $(this).closest('li').find('div.link-details');
            if(detailsDiv.find('.applicable-coupon-radio.selected').length > 0){
                $('#lb-selected-coupon-code').val(detailsDiv.find('.applicable-coupon-radio.selected').val());
            } else if(detailsDiv.find('#othercouponcode').length > 0){
                $('#lb-selected-coupon-code').val(detailsDiv.find('#othercouponcode').val());
            } else {
                $('#lb-selected-coupon-code').val('');
            }
        });
        var obj=$(".remove-coupons-link");
        var func= function(e){
            Myntra.Utils.preventDefault(e);
            removeCoupon();
            $("#lb-selected-coupon-code").val('');
            $("#othercouponcode").val('');
        };
        Myntra.Utils.extendClickEvent(obj,func);
   
        $("#othercouponcode").bind('input paste keyup', function(event) {
            var _this = this;
            // Short pause to wait for paste to complete
            setTimeout( function() {
                var text = $(_this).val();
                $("#lb-selected-coupon-code").val(text);
            }, 100);
        });
        $("#lb-coupons").on("click",".applicable-coupon-radio",function() {
            if ($(this).hasClass('selected')) {
                $(this).prop('checked', false);
                $(this).toggleClass('selected');
                $(this).closest('div').toggleClass('selected');
                $(this).closest('div').find('.coupon-desc').hide();
                $('#lb-selected-coupon-code').val('');
            } else {
                $(this).closest('div.choose-coupons-body').find('.coupon-desc').hide();
                $(this).closest('div').find('.coupon-desc').show();
                $("#lb-coupons :radio").removeClass("selected").closest('.eligible-coupon-item').removeClass("selected");
                $("#lb-coupons :checked").addClass("selected").closest('.eligible-coupon-item').addClass("selected");
                var couponNum = $(this).attr('id');
                $("#lb-selected-coupon-code").val(couponNum);
                $("#lb-coupons .lb-coupon-error-msg").html('');
            }
            $("#lb-coupons .expand-ineligible-coupons").removeClass('expanded');
            $('#lb-coupons .ineligible-coupon-item.selected').removeClass('selected');
        });
        $("#lb-coupons .expand-ineligible-coupons").on("click",function() {
            var couponBody = $(this).closest('div.choose-coupons-body');
            var couponItem = $(this).closest('div.ineligible-coupon-item');
            couponBody.find('.coupon-desc').hide();
            couponBody.find('.selected').removeClass('selected');
            if($(this).hasClass('expanded')){
                $("#lb-coupons .expand-ineligible-coupons").removeClass('expanded');
            } else {
                $("#lb-coupons .expand-ineligible-coupons").removeClass('expanded');
                $(this).addClass('expanded');
                couponItem.find('.coupon-desc').show();
                couponItem.toggleClass('selected');
            }
            });
    }
    init();

    return couponRightMenuobj;
};
var initCouponBox = function() {
    if($('#lb-coupons').length>0)
       var initCouponObj = Myntra.CouponBox('#lb-coupons');
};
var showCouponBox = function(){
    _gaq.push(['_trackEvent', 'cart', 'apply-coupon', 'click']);
    $(document).trigger('myntra.couponBox.show');       
};

Myntra.EditBox = function(id) {  
	var cfg={};
	var editBoxListAnimObj = Myntra.ListAnimation($('.content-slide-wrapper.editbag-wrapper'));
    var editBoxRightMenuObj = Myntra.RightMenu($('.content-slide-wrapper.editbag-wrapper'),cfg,editBoxListAnimObj);
    var lastClickedProdData ="";
    var userClickWaiting=false;
    var isSizeUpdate=false;
    var isQtyUpdate=false;
    var formobj =$('#lb-editbag form.savecartchanges');
    $(document).bind('myntra.editBox.show', function(e, data) {

    if(userClickWaiting==false && typeof(data) == "undefined"){
        return false;
    }
    if(typeof(data) == "undefined" && userClickWaiting==true) {
        data=lastClickedProdData;
    }
    if(typeof(data) != "undefined" && $(data).find('.ps-size-wrapper').html()==""){
        userClickWaiting=true;
        editBoxRightMenuObj.showLoading();
    }
    if(typeof(data) != "undefined"){
        lastClickedProdData = data;
    }
    if($(data).find('.ps-size-wrapper').html()=="") {
        return false;
    }
    initData(data);
    editBoxRightMenuObj.show();
    $($('#lb-editbag .back')[0]).trigger('click');
    });

    function initData(data) {
        $('#lb-editbag .lb-editbag-error-msg').html("");
        isSizeUpdate=false;
        isQtyUpdate=false;
        var prodName=$(data).find('.prod-name a').text();
        var sizeObjs=$(data).find('.ps-size-wrapper');
        var qtyObj=$(data).find('.ps-qty-wrapper').clone(true);
        var stid=$(data).find('input[name="styleid"]').val();
        $('.size-wrapper').html($(sizeObjs).html());
        $('.qty-wrapper').html(qtyObj);
        $('.prod-name-wrapper').html(prodName);
        var form2obj = $(data); 
        var cartItemName = form2obj.find('input[name="cartitem"]').val();
        if(typeof(cartItemName)== "undefined")
            {
            cartItemName = form2obj.find('input[name="itemId"]').val();
            }
        formobj.find('input[name="cartitem"]').val(cartItemName);
        formobj.find('input[name="itemId"]').val(cartItemName);
        $('#lb-editbag input[name="actionType"]').attr('value', '');
        formobj.find('input[name="styleid"]').val(form2obj.find('input[name="styleid"]').val());
        $('#lb-editbag .lb-selected-qty').attr('name', $('#lb-editbag .sel-qty').attr('name'));
        $('#lb-editbag .lb-selected-qty').attr('value', $('#lb-editbag .sel-qty').attr('value'));
        $('#lb-editbag .lb-selected-size').attr('name', $('#lb-editbag .btn-size.selected').attr('name'));
        $('#lb-editbag .lb-selected-size').attr('value', $('#lb-editbag .btn-size.selected').attr('value'));
        editBoxRightMenuObj.hideLoading();
    }
    function init() { 
    $('.qty-wrapper .sel-qty').live('change', function(e) {
        isQtyUpdate=true;
        $('#lb-editbag .lb-selected-qty').attr('name', $(this).attr('name'));
        $('#lb-editbag .lb-selected-qty').attr('value', $(this).attr('value'));
    });
    $(".edit-size-span").on("click",function(e){
    	$("#lb-editbag .header").text('EDIT SIZE / QUANTITY');
    });
    $("#lb-editbag .back").on("click",function(e){
    	$("#lb-editbag .header").text('MODIFY ITEM');
    });
    $(".move-to-wishlistbag-link").on("click",function(e){
        if(Myntra.Data.userEmail=="" || Myntra.Data.userEmail == null)
        {
            $(document).trigger('myntra.login.show', [{action: 'login', onLoginDone : function(){
                var obj=$(".move-to-wishlistbag-link").find("div");
                if(obj.hasClass('lb-move-to-wishlist'))
                    {
                    _gaq.push(['_trackEvent', 'cart', 'save_item']);
                    var currProdView = $('#lb-editbag .lb-prod-view-id').val();
                    $('#'+currProdView+' .form-save-item').submit();
                    }
                else if(obj.hasClass('lb-move-to-bag')){
                    _gaq.push(['_trackEvent', 'cart', 'move_saved_item_to_bag']);
                    var currProdView = $('#lb-editbag .lb-prod-view-id').val();
                    $('#'+currProdView+' .move-to-cart').submit();
                }
            }}]);
        } else {
            var obj=$(".move-to-wishlistbag-link").find("div");
            if(obj.hasClass('lb-move-to-wishlist'))
                {
                _gaq.push(['_trackEvent', 'cart', 'save_item']);
                var currProdView = $('#lb-editbag .lb-prod-view-id').val();
                $('#'+currProdView+' .form-save-item').submit();
                }
            else if(obj.hasClass('lb-move-to-bag')){
                _gaq.push(['_trackEvent', 'cart', 'move_saved_item_to_bag']);
                var currProdView = $('#lb-editbag .lb-prod-view-id').val();
                $('#'+currProdView+' .move-to-cart').submit();
            }
        }
    });
    $(".remove-from-wishlistbag-link").on("click",function(e){
        var obj=$(".remove-from-wishlistbag-link").find("div");
        if(obj.hasClass('lb-remove'))
            {
            _gaq.push(['_trackEvent', 'cart', 'remove_item']);
            var currProdView = $('#lb-editbag .lb-prod-view-id').val();
            $('#'+currProdView+' .form-remove-item').submit();
            }
        else if(obj.hasClass('lb-remove-saved-item')){
            _gaq.push(['_trackEvent', 'cart', 'remove_saved_item']);
            var currProdView = $('#lb-editbag .lb-prod-view-id').val();
            $('#'+currProdView+' .saved-remove-item').submit();
        }
    });
    $('.size-wrapper .btn-size').live('click', function(e) {
        isSizeUpdate=true;
        $('.size-wrapper button').removeClass('selected');
        $(this).addClass('selected');
        $('#lb-editbag .lb-selected-size').attr('name', $(this).attr('name'));
        $('#lb-editbag .lb-selected-size').attr('value', $(this).attr('value'));
    });
    $('#lb-editbag .lb-save-changes').on('click', function(e) {
        Myntra.Utils.preventDefault(e);
        var actType =  formobj.find('input[name="itemType"]').val();
        var actionurl="";
        if(actType == 'wishlist-item') {
            actionurl = '/mksaveditem.php';
            $('#lb-editbag input[name="actionType"]').attr('value', 'updateSku');
        }else {
            actionurl = '/modifycart.php';
        }
        formobj.attr('action', actionurl);
        if(isSizeUpdate && isQtyUpdate)
        {
        var selectedSKUID = formobj.find('.lb-selected-size').val();
        _gaq.push(['_trackEvent', 'cart', 'size_update_saved', selectedSKUID]);
        var selectedQuantity = formobj.find('.lb-selected-qty').val();
        _gaq.push(['_trackEvent', 'cart', 'quantity_update_saved', selectedQuantity]);
        editBoxRightMenuObj.showLoading();
        formobj.submit();
        }
        else if(isSizeUpdate)
            {
            var selectedSKUID = formobj.find('.lb-selected-size').val();
            _gaq.push(['_trackEvent', 'cart', 'size_update_saved', selectedSKUID]);
            editBoxRightMenuObj.showLoading();
            formobj.submit();
            }
        else if(isQtyUpdate)
            {
            var selectedQuantity = formobj.find('.lb-selected-qty').val();
            var selectedQuantity = formobj.find('input[name="actionType"]').val('updateQuantity');
            _gaq.push(['_trackEvent', 'cart', 'quantity_update_saved', selectedQuantity]);
            editBoxRightMenuObj.showLoading();
            formobj.submit();
            }
        else {
            $('#lb-editbag .lb-editbag-error-msg').html("Please edit the size or quantity to update.");
        }
        });
    }
    init();
    return editBoxRightMenuObj;     
};
var initEditBox = function() {
    if($('#lb-editbag').length>0)
        var obj = Myntra.EditBox('#lb-editbag');
    };
var showEditBox = function(data){
     _gaq.push(['_trackEvent', 'cart', 'edit_item', 'click']);
     $(document).trigger('myntra.editBox.show', data);       
};
// Verify Mobile code Starts
$(document).ready(function(){

    if($('#smsVerification').length){
        var lb_smsVerifiy = Myntra.LightBox('#smsVerification');
        $('#showSMSVerification').click( function() {
                lb_smsVerifiy.show();
        });
    }

    $('#mobile').keydown(function(e) {
        if (e.keyCode == '13') {
            Myntra.Utils.preventDefault(e);
            if ($(".verify-mobile-cart-page").css("display") != "none") {
                $(".verify-mobile-cart-page").trigger("click");
            }
        }
    });
    $('#mobile-captcha-form').keydown(function(e) {
        if (e.keyCode == '13') {
            Myntra.Utils.preventDefault(e);
            $("#mobile-verify-captcha").trigger("click");
        }
    });

    $(".verify-mobile-cart-page").live('click',function(e){
        if(!validateMobileNumber()) {
            verifyMobileCommon("cartpage");
        }
        Myntra.Utils.stopPropagation(e);
    });
    $(".submit-code").live('click',function(){
        var profileForm=$("#verify-mobile-code");
        var codeStr = $("#mobile-code", profileForm).val();
        if (codeStr != "") {
            $("#submit-code").hide();
            $("#code-verify-loading").show();
            $.ajax({
                type: "POST",
                url: "mkpaymentoptions.php",
                data: "&mode=verify-mobile-code&mobile-code=" +codeStr,
                success: function(data) {
                    var responseObj=$.parseJSON(data);
                    var status=responseObj.status;
                    var message = responseObj.message;
                    var num_attempts = responseObj.attempts;
                    if(status==1){
                        //reload the page verification was successful.
                        $("#mobile-verification-error").hide();
                        $("#mobile-verification-error").removeClass("error");
                        $("#mobile-verification-error").text(message).fadeIn('slow');
                        window.location.reload();
                    } else {
                        $("#submit-code").show();
                        $("#code-verify-loading").hide();
                        mobileVerificationSubmitCount++;
                        if(mobileVerificationSubmitCount>5) {
                            //hide and ask him to resend the sms.
                            $("#verify-mobile-code").slideUp();
                            $("#v-code-note-message").hide();
                            $("#v-code-note-message").addClass("error");
                            $("#v-code-note-message").text("You have exceeded the maximum no. of attempts to validate the SMS code. To try verifying your mobile again please try with a new verification code.").fadeIn('slow');
                        } else {
                            $("#mobile-verification-error").text(message);
                            $("#mobile-verification-error").addClass("error");
                        }
                    }
                },
                error:function (xhr, ajaxOptions, thrownError){
                    $("#submit-code").show();
                    $("#code-verify-loading").hide();
                    $("#mobile-verification-error").text("Internal Error occured. Please retry again.");
                    $("#mobile-verification-error").addClass("error");
                }
            });
            _gaq.push(['_trackEvent', 'payment_page', 'coupon_widget', 'submit_mobile_code']);
        }
    });
});

function verifyMobileCommon(id) {
    $("#verify-mobile").hide();
    $("#verify-mobile-code").slideUp();
    var profileForm=$("#change-profile");
    $("#mobile-verify-loading").show();
    var mobile = $("#mobile", profileForm).val();
    var login = $("#login", profileForm).val();
    $("#mobile",profileForm).attr("disabled","disabled");

    $.ajax({
    type: "POST",
    url: "mymyntra.php",
    data: "&mode=check-mobile-user&mobile=" + $("#mobile", profileForm).val() + "&login=" + $("#login", profileForm).val()+"&_token="+$("#_token", profileForm).val(),
    success: function(msg){
            msg = $.trim(msg);
            if (msg == "sendSMS") {
                $.ajax({
                type: "POST",
                url: "mymyntra.php",
                data: "&mode=generate-code&mobile=" + $("#mobile", profileForm).val() + "&login=" + $("#login", profileForm).val()+"&_token="+$("#_token", profileForm).val(),
                success: function(msg){
                        msg = $.trim(msg);
                        $("#mobile-verify-loading").hide();
                        if (msg == "showCaptcha")       {
                            $("#v-code-note-message").hide();
                            $("#v-code-note-message").removeClass("error");
                            $("#v-code-note-message").text("Note: Please verify your specified contact number to be able to use any user-specific coupons that you may have been issued through Myntra. We may use this number to contact you for issues related to any of your transactions on Myntra.").fadeIn('slow');
                            $("#verify-mobile").attr("value","Send Again")
                            $("#mobile-captcha").attr("src", https_loc+'/captcha/captcha.php?id='+id+'&rand='+Math.random());
                            $("#mobile-captcha-form").val("");
                            $("#mobile-captcha-block").show();
                        $(".change-password-link").hide();
                    } else if (msg == "verifyFailed"){
                        $("#verify-mobile").show();
                        $("#mobile",profileForm).removeAttr("disabled");
                        $("#verify-mobile").attr("value","VERIFY");
                        $("#v-code-note-message").hide();
                            $("#v-code-note-message").addClass("error");
                            $("#v-code-note-message").text("You have exceeded the maximum no. of attempts to verify this mobile number. If you have still not received the verification code or are facing any other difficulties, please provide an alternate number and retry. Or, try again after 6 hours.").fadeIn('slow');
                    }
                    else{
                        $("#verify-mobile").show();
                        $("#mobile",profileForm).removeAttr("disabled");
                        $("#v-code-note-message").hide();
                            $("#v-code-note-message").removeClass("error");
                            $("#v-code-note-message").text("Note: Please verify your specified contact number to be able to use any user-specific coupons that you may have been issued through Myntra. We may use this number to contact you for issues related to any of your transactions on Myntra.").fadeIn('slow');
                        mobileVerificationSubmitCount=0;
                        $("#verify-mobile").attr("value","Send Again")
                    $("#verify-mobile-code").slideDown();
                        $(".mobile-code-entry").show();
                            $("#mobile-message").html("Please enter the verification code that was sent to <strong>" + mobile + "</strong>");

                            $("#mobile-message").show();
                            $("#mobile-code").val("");
                        $("#mobile-verification-error").removeClass("error");
                        $("#mobile-verification-error").text("If you have not received your verification code, please wait for atleast 2 minutes before pressing submit button");
                    }

                    },
                    error:function (xhr, ajaxOptions, thrownError) {
                    $("#mobile-verify-loading").hide();
                        $("#verify-mobile").show();
                        $("#mobile",profileForm).removeAttr("disabled");
                        $("#v-code-note-message").hide();
                        $("#v-code-note-message").addClass("error");
                        $("#v-code-note-message").text("An error has occured while sending the sms please retry again").fadeIn('slow');
                    }
                });
            } else if (msg=="exceed"){
                //number of attempts to be tried exceeds disable the edit button and show him the message to retry after 6 hours
                $("#mobile",profileForm).removeAttr("disabled");
                $("#mobile-verify-loading").hide();
                $("#verify-mobile").show();
                $("#v-code-note-message").hide();
                $("#v-code-note-message").addClass("error");
                $("#v-code-note-message").text("You have exceeded the maximum no. of attempts to verify this mobile number. If you have still not received the verification code or are facing any other difficulties, please provide an alternate number and retry. Or, try again after 6 hours.").fadeIn('slow');

            } else if (msg == "verified"){
                //The number is already verified by some other user
                $("#mobile-verify-loading").hide();
                $("#verify-mobile").show();
                $("#mobile",profileForm).removeAttr("disabled");
                $("#v-code-note-message").hide();
                $("#v-code-note-message").addClass("error");
                $("#v-code-note-message").text("This number has been verified with us by another user. Please use another mobile number.").fadeIn('slow');
            } else if (msg == "skipVerify"){
                $("#mobile-verify-loading").hide();
                $("#v-code-note-message").hide();
                $("#verify-mobile").hide();
                window.location.reload();

            }else {
                //Some other error has occured asked him to retry.
                $("#mobile-verify-loading").hide();
                $("#verify-mobile").show();
                $("#mobile",profileForm).removeAttr("disabled");
                $("#v-code-note-message").hide();
                $("#v-code-note-message").addClass("error");
                $("#v-code-note-message").text("An error has occured while sending the sms please retry again").fadeIn('slow');
            }
    },
        error:function (xhr, ajaxOptions, thrownError) {
        $("#mobile-verify-loading").hide();
            $("#verify-mobile").show();
            $("#mobile",profileForm).removeAttr("disabled");
            $("#v-code-note-message").hide();
            $("#v-code-note-message").addClass("error");
            $("#v-code-note-message").text("An error has occured while sending the sms please retry again").fadeIn('slow');
        }
    });
    _gaq.push(['_trackEvent', 'cart_page', 'coupon_widget', 'verify_mobile']);
}

function validateMobileNumber() {
    var numericReg  =  /(^[1-9]\d*$)/;
    var error = false;
    var profileForm=$("#change-profile");
    var mobileVal=$("#mobile", profileForm).val();
    $("#mobile-error", profileForm).hide();
    if(mobileVal == "" || mobileVal.length < 10 || !numericReg.test(mobileVal)){
        error=true;
        $("#mobile-error", profileForm).text("Enter a valid mobile number.").slideDown();
    }
    return error;

}

function verifyMobileCaptcha() {
    if($("#mobile-verify-captcha").hasClass("btn-disabled")){
        return;
    }
    if($('#mobile-captcha-form').val() == ""){
        $("#mobile-captcha-message").html("Please enter the text.");
        $("#mobile-captcha-message").addClass("error");
        $("#mobile-captcha-message").slideDown();
        return;
    }
    $("#mobile-captcha-loading").show();
    $("#mobile-verify-captcha").addClass("btn-disabled")
    $("#mobile-captcha-message").html("").removeClass("error");
    $("#mobile-captcha-message").slideUp();
    $.ajax({
        type: "POST",
        url: https_loc+"/mobile_verification.php",
        data: "mobile=" + $('#mobile').val() + "&userinputcaptcha=" + $('#mobile-captcha-form').val() + "&login=" + $('#mobile-login').val() + "&id=paymentpage",
        success: function(msg){

            var resp=$.trim(msg);
            $("#mobile-captcha-loading").hide();
            $("#mobile-verify-captcha").removeClass("btn-disabled");
            if(resp=='CaptchaConfirmed') {
                $("#mobile-captcha-block").hide();
                $(".verify-mobile-cart-page").trigger("click");

            } else if(resp=='WrongCaptcha') {
                $("#mobile-captcha-message").html('Wrong Code entered');
                $("#mobile-captcha-message").addClass('error');
                $("#mobile-captcha-message").slideDown();
           }
        },
        error:function (xhr, ajaxOptions, thrownError){
            $("#mobile-captcha-loading").hide();
            $("#mobile-verify-captcha").removeClass("btn-disabled");
            $("#mobile-captcha-message").html('Internal Error Occured. Please Retry again');
            $("#mobile-captcha-message").addClass('error');
            $("#mobile-captcha-message").slideDown();
        }
    });
    _gaq.push(['_trackEvent', 'cart_page', 'coupon_widget', 'verify_mobile_captcha']);
}


// Verify mobile code ends