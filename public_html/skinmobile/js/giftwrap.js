var photo;
var photoText;
(function() { "use strict";
    if(Myntra.Data.isTablet) {
        photo = '           <div class="photo"></div>';
        photoText = "";
    }
    else {
        photo = '';
        photoText = "<div class='note'><p>PLEASE NOTE:</p><p>Gift packaging slip/invoice will not include any pricing, discount,or payment information</p><p>All original products tags with MRP will be intact</p></div>";    
    }
  //CREATE  SELF INVOKING FUNCTION
var tplBox = [
'<div id="lb-gift-wrap" class="lb-gift-wrap">',
'   <div class="close-wrapper"><span class="btn-close"></span></div>',
'   <div class="mod">',
'        <div class="hd">',
'               <h2><span class="lbl">ADD MESSAGE</span></h2>',
'        </div>',
'       <div class="bd">',
'              <div class="success-msg mk-hide"><span class="icon"></span> MESSAGE SAVED</div>',
photo,
'           <div class="details">',
'               <div class="input-row"><span class="static-text to">Dear </span>',
'              <input type="text" name="giftTo" id="gift-to" value="" maxlength="60" placeholder="Recipient name"/>',
'              <div class="err-msg">PLEASE ENTER A NAME</div></div>',
'              <textarea maxlength="200" rows="5" id="gift-msg" placeholder="Message" name="MESSAGE" value=""></textarea>',
'              <div class="err-msg">PLEASE ENTER A MESSAGE</div>',
'               <div class="input-row"><span class="static-text from">From </span>',
'              <input type="text" name="giftFrom" id="gift-from" value="" maxlength="60" placeholder="Sender name" />',
'              <div class="err-msg">PLEASE ENTER A NAME</div></div>',
'              <div class="status-msg"></div>',
'              <button class="btn primary-btn btn-proceed btn-orange-noplay">Save</button>',
'              <button class="btn normal-btn btn-cancel btn-grey">Cancel</button>',
photoText,
'           </div>',
'       </div>',
'   </div>',
'</div>'
].join('');

Myntra.giftWrapBox =  { 
    init: function(){
        this.root = $('.giftwrap-wrapper').html(tplBox);
        this.isDataSaved=false;
        this.isReadOnly=false;
        this.input=this.root.find('input,textarea,button');
        this.giftTo=this.root.find('#gift-to');
        this.giftFrom=this.root.find('#gift-from');
        this.giftMsg=this.root.find('#gift-msg');
        this.btnSave=this.root.find('.btn-proceed').on('click', $.proxy(this.validate, this));
        var _this = this;
        this.btnCancel=this.root.find('.btn-cancel').on('click', 
        		$.proxy(this.hide, this) 
        		);
        this.errMsg=this.root.find('.err-msg');
        this.statMsg=this.root.find('.status-msg');
        this.successMsg=this.root.find('.success-msg');
        this.gBox = Myntra.RightMenu($('.content-slide-wrapper.giftwrap-wrapper'),{isWidthAspect:false});
        this.gBoxMod = this.root.find('.mod');
        this.gBoxHead = this.root.find('.hd');
        this._initialized = true;
        this.targetClicked = '';
        var _that=this;
        $('.close-wrapper').fastClick(function(){
        	_that.gBox.hide();
        });
    },

    show: function(giftData){
        !this._initialized && this.init();
        this.targetClicked = giftData.elm;
        var that = this;
        if(giftData.isData){
            this.isReadOnly=giftData.readOnly;
            if(this.isReadOnly){
                this.gBoxHead.find('.lbl').text('Your Personal Message');
                this.input.prop('disabled',true);
                this.btnSave.hide();
                this.btnCancel.hide();
            }
            this.isDataSaved=true;
            this.giftTo.val(giftData.to);
            this.giftFrom.val(giftData.from);
            this.giftMsg.val(giftData.msg);
        }
        
        this.gBox.show();
        this.gBox.beforeHide=function(){
            that.setData();
            
        }
    },
    
    hide : function(){
        this.gBox && this.gBox.hide();
    },
    
    save : function(){
        var that=this;
        $.ajax({
            type: 'POST',
            url: (location.protocol === 'https:' ? 's_' : '') + 'saveGiftingDetails.php',
            data: "_token=" + Myntra.Data.token + "&mode=save&giftTo=" + this.giftTo.val()+"&giftFrom="+this.giftFrom.val()+"&giftMessage="+this.giftMsg.val(),
            beforeSend:function(){
                that.gBox.showLoading();
            },
            success: function(data){
                data = jQuery.parseJSON(data);
                that.gBox.hideLoading();
                if(data.status=='success'){
                    that.gBoxMod.css('min-height','120px');
                    that.successMsg.siblings().hide();
                    that.successMsg.show();
                    that.isDataSaved=true;
                    window.location=document.URL;
                    that.gBox.hide();
                }
                else {
                    that.statMsg.text('Oops! something went wrong. Please try again.');
                 }
            }
       });
    },
    validate : function(){
        if(!$.trim(this.giftTo.val()) || !$.trim(this.giftFrom.val()) || !$.trim(this.giftMsg.val())){
            !$.trim(this.giftTo.val()) ? this.giftTo.next().css('visibility','visible'):this.giftTo.next().css('visibility','hidden');
            !$.trim(this.giftFrom.val()) ? this.giftFrom.next().css('visibility','visible'):this.giftFrom.next().css('visibility','hidden');
            !$.trim(this.giftMsg.val()) ? this.giftMsg.next().css('visibility','visible'):this.giftMsg.next().css('visibility','hidden');
        }
        else {
            this.save();
            this.errMsg.css('visibility','hidden');
        }

    },
    setData : function(){
        var tag = $(this.targetClicked.target).prop('tagName');
        if(!this.isReadOnly && this.isDataSaved){
            if(tag === 'INPUT'){
                $(this.targetClicked.target).siblings('.gift-msg-edit').data('gift-from',this.giftFrom.val()).data('gift-to',this.giftTo.val()).data('gift-msg',this.giftMsg.val()).show();
            }
            else{
                $(this.targetClicked.target).data('gift-from',this.giftFrom.val()).data('gift-to',this.giftTo.val()).data('gift-msg',this.giftMsg.val()).show();
            }
        }
        else {
            $(this.targetClicked.target).attr('checked',false);
            $('.class_checkbox').removeClass('checked');
        }
        
        this.giftTo.val('');
        this.giftFrom.val('');
        this.giftMsg.val('');
        this.statMsg.text('');
        this.errMsg.css('visibility','hidden');
        
    }
};
})();// end of "use strict" wrapper

/////*******THIS PART BELOW IS THE INVOKING SECTION***********//////////
$(document).ready(function(){
    if($('.gift-wrap-select').length || $('.gift-msg-edit').length){
        Myntra.initGiftWrap();
    }    
});

Myntra.initGiftWrap = function(){ "use strict";
    var changeStatus = function(postData){
        $.ajax({
            type: 'POST',
            url:(location.protocol === 'https:' ? 's_' : '' ) + 'saveGiftingDetails.php',
            data: "_token=" + Myntra.Data.token + "&"+postData,
            success: function(data){
                data = jQuery.parseJSON(data);
                if(data.status=='success'){
                    window.location=window.location;
                }
            }
        });
    };
    $(".gift-wrap-select").on('change',function(el){
        if($(this).is(':checked')){
        	$('.class_checkbox').addClass('checked');
        } else {
        	$('.class_checkbox').removeClass('checked');
        }
    });
	
    //JUST CLICK HANDLERS 
    $(".gift-wrap-select").on('click',function(el){
        if($(this).is(':checked')){
            //GA tracking
            if($(this).hasClass('gift-wrap-cart')){
                _gaq.push(['_trackEvent','gift_wrap_cart', 'opt_in']);
            }
            else {
                _gaq.push(['_trackEvent','gift_wrap_payment', 'opt_in']);
            }
            if(!$(this).siblings('.gift-msg-edit').data('gift-msg')){
                //init lightbox construct a plain HTML and show 
                Myntra.giftWrapBox.show({isData:false,elm:el});
            }
            else {
                changeStatus('mode=selectGift');
            }
        }
        else {
            //GA tracking
            if($(this).hasClass('gift-wrap-cart')){
                _gaq.push(['_trackEvent','gift_wrap_cart', 'opt_out']);
            }
            else {
                _gaq.push(['_trackEvent','gift_wrap_payment', 'opt_out']);
            }
            //fire ajax call to clear the giftStatus = N
            changeStatus('mode=removeGift');
        }
        

    });
    
    //attach event for Edit message 
    $(".gift-msg-edit").on('click',function(el){
        var giftData={
                from:$(this).attr('data-gift-from'),
                to:$(this).attr('data-gift-to'),
                msg:$(this).attr('data-gift-msg'),
                elm:el,
                isData:true,
                readOnly:$(this).data('gift-readonly')
        }
        Myntra.giftWrapBox.show(giftData);
    });

};



