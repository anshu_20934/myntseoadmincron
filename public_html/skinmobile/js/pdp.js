Myntra.PDPImages = Myntra.PDPImages || {};

$(function () {
  if (Myntra.PDPImages.init) { return; }

  (function (context, classname) {

    // Variables
    var zoomImgWidth = 1080, zoomImgHeight = 1440, noOfImagesPreloaded = 0, w = window,
        viewPortWidth = Myntra.Utils.getViewportWidth(),
        viewPortHeight = Myntra.Utils.getViewportHeight(),
        elBody = $("body"), elMBody = $(".m-body"), elZoomTooltip = $(".zoom-tooltip"),
        elZoomWrapper, elZoomWrapperImg, elZoomWrapperCloseTooltip, elPDPImages = ".pdpimages", elZoomLoader;

    // Bind the show Zoom handler to the elements
    var bindShowZoom = function () {
      // elPDPImages - the images present in the slide show in pdp
      $(elPDPImages).hammer().bind("tap", function(event) {
        preloadAndShowImage($(this).attr("data-src"), $(this).parent().attr("data-itr"));
      });
    };

    // Bind the show Zoom handler to the elements
    var bindHideZoom = function () {
      // elZoomWrapperCloseTooltip - close button in the zoom overlay
      elZoomWrapperCloseTooltip.hammer({ prevent_default : true }).bind("tap", function(event) {
        hideZoom();
      });

      // elZoomWrapperImg - the img tag in the zoom overlay holding the zoomed image
      elZoomWrapperImg.hammer().bind("tap", function(event) {
        hideZoom();
        Myntra.Utils.stopPropagation(event);
      });

      /*elZoomWrapper.hammer({ prevent_default : true }).bind("tap", function(event) { 
        Myntra.Utils.stopPropagation(event);
      });*/
    };

    // Construct the zoom overlay elements and attach it to dom. Basically keep it ready i say!
    var setupZoom = function () {
      elZoomWrapper = $('<div id="zoom-wrapper">' + //zoom overlay
          '<div class="close closetop">Tap to close</div>' + //close button
          '<div class="zoom-loader"></div>' + //loading indicator
          '<img src="' + cdn_base + '/skin2/images/spacer.gif" title=""/>' + //zoom image placeholder
          '</div>').appendTo('body'); //append to body
      elZoomWrapperImg = $("img", elZoomWrapper);
      elZoomWrapperCloseTooltip = $(".close", elZoomWrapper);
      elZoomLoader = $(".zoom-loader", elZoomWrapper);
    };

    // Enuf from the clown! Hide the zoom overlay i say!
    var hideZoom = function () {
      resetBodyStyles();
      elMBody.show();      
      elZoomWrapper.hide();
      scrollPosition({"x": 0, "y": 0});
    };

    // Show the zoom overlay i say!
    var showZoom = function(imgSrc) {
      elZoomWrapperImg.attr("src", imgSrc);
      elZoomWrapper.show();
      elMBody.hide();
      showZoomWrapperCloseTooltip();
      scrollPosition(getXY());
    };

    // Set the position for the zoom overlay close tooltip and show
    var showZoomWrapperCloseTooltip = function() {
      elZoomWrapperCloseTooltip.css({"left": (zoomImgWidth - 150)/2, "top": (zoomImgHeight - 35)/2});
      elZoomWrapperCloseTooltip.show();
      setTimeout(function(){
        hideZoomWrapperCloseTooltip();
      }, 5000);
    };

    // Hide the zoom overlay close tooltip.
    var hideZoomWrapperCloseTooltip = function() {
      elZoomWrapperCloseTooltip.hide();
    };

    // Preload every image before showing it
    // When a new slide is shown in the pdp page the corresponding image is preloaded in the "after" function of flexslider. 
    // This preload is to double check so that we dont end up showing a blank zoom overlay to the user.
    var preloadAndShowImage = function (imgSrc, idx) {
      if(pdpImagePaths[idx].loaded){
        hideZoomLoader();
        showZoom(imgSrc);
      }
      else{
        var img = new Image();
        showZoomLoader();
        img.onload = function(){
            pdpImagePaths[idx].loaded = true;
            hideZoomLoader();
            showZoom(imgSrc);
        };
        img.src = imgSrc;
      }
    };

    // Ok we played with the DOM a little bit by setting styles to body. Lets remove them now.
    var resetBodyStyles = function () {
      elBody.removeAttr("style");
    };

    // Show the Tap to zoom tooltip in pdp page
    var showZoomTooltip = function () {
      elZoomTooltip.show();
      setTimeout(function(){
        hideZoomTooltip();
      }, 8000);
    };

    // Hide the Tap to zoom tooltip in pdp page
    var hideZoomTooltip = function(){
      elZoomTooltip.hide();
    };

    // Show loading indicator until the Zoom image is loaded
    var showZoomLoader = function () {
      elZoomLoader.show();
    };

    // Hide loading indicator once the Zoom image is loaded
    var hideZoomLoader = function () {
      elZoomLoader.hide();
    };

    // To set the position where the loading indicator has to appear
    var setupZoomLoader = function () {
      elZoomLoader.css({"left": (zoomImgWidth - 35)/2, "top": (zoomImgHeight - 30)/2});
    };

    // Setup the width and Height for the Zoom overlay.
    var setWH = function () {
      elZoomWrapper.css({"width" : zoomImgWidth,"height" : zoomImgHeight,"overflow" : "auto"});
      elBody.css({"overflow-x" : "auto"});
    };

    // Scroll to the given posObj.x and posObj.y
    var scrollPosition = function (posObj) {
      window.scroll(posObj.x,posObj.y);
    };

    // Get the center x,y co-ordinates of the zoom overlay 
    var getXY = function () {
      return {"x" : (zoomImgWidth - viewPortWidth)/2, "y" : (zoomImgHeight - viewPortHeight)/2};
    };

    var processResize = function () {
      var supportsOrientationChange = "onorientationchange" in window,
          orientationEvent = supportsOrientationChange ? "orientationchange" : "resize";

      $(window).bind(orientationEvent, function(e) {
        resetValuesOnResize();
      });
    };

    var resetValuesOnResize = function () {
      viewPortWidth = Myntra.Utils.getViewportWidth();
      viewPortHeight = Myntra.Utils.getViewportHeight();
    };

    var init = function () {
      /**
       * Initialise flexslider for the pdp inpage images after onload event of the page.
       * Animation is set to slide and automatic slideshow is set to false.
       * Before function prefetches the zoomed image for a given image.
       */
      $(classname).flexslider({
        animation: "slide",
        slideshow: false,
        start: function () { //once the slider starts enable the tooltip and hide it after some time
          showZoomTooltip();
        },
        after: function (slider) { // after every slide we start loading the zoomed image for the current slide
          var imgIdx = $(".flex-active-slide").attr("data-itr");
          if( !pdpImagePaths[imgIdx].loaded ){
            var _img = new Image();
            var imgSrc = $(".flex-active-slide img").attr("data-src");
            _img.onload = function(){
              noOfImagesPreloaded++;
              pdpImagePaths[imgIdx].loaded=true;
            }
            _img.src = imgSrc;
          }
        }
      });

      if(typeof elZoomWrapper == "undefined"){
        setupZoom();
      }

      bindShowZoom();

      bindHideZoom();

      setupZoomLoader();

      processResize();

    };

    context.init = init;

  })(Myntra.PDPImages, ".flexslider");
});

$(window).load(function(){
  Myntra.PDPImages.init();
});
