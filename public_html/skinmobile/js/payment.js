
function formatNumber(num) {
    num = num.toFixed(2).split(".")[0];
    if (num.length <= 3) { return num; }
    var m = /(\d+)(\d{3})$/.exec(num);
    var inp = m[1];
    var last = m[2];
    var out = [];
    if (inp.length % 2 === 1) {
        out.push(inp.substr(0,1));
        inp = inp.substr(1);
    }
    var rx = /\d{2}/g;
    while (m = rx.exec(inp)) {
        out.push(m[0]);
    }
    out.push(last);
    return out.join(',');
};

$(function() {
    
    var $items = $('.pay-block .tabs ul li');
    if(total_amount==0){
        $items.click(function() {
            return false;
        });
    } else {
        $items.click(function() {
            var id = $(this).attr('id').replace('pay_by_', '');
            if(Myntra.Payments.paymentCoupon.hasPaymentCoupon){
                var allowedTabs = Myntra.Payments.paymentCoupon.allowedTabs;
                if($.inArray(id,allowedTabs) === -1){
                    binPromotionBox('wrongMethod');
                    return;
                }
            }         

            if($(this).hasClass('selected')) {}
            else{

                $('.err-card').html("");
 //               $(".pay-btn").removeAttr("disabled");

            }

            $items.removeClass('selected');
            $(this).addClass('selected');
            var index = $items.index($(this)),
                form = $('.pay-block > .tab-content > form').hide().eq(index).show(),
                formtoSubmit = form.attr("id");
            $("#paymentFormToSubmit").val(formtoSubmit);

            $(".pay-btn").show();

            
            if(is_icici_gateway_up) {
                var buttonText = "Pay Now";
            } else {
                var buttonText = "Proceed to Payment";
            }

            if(citi_discount>0) {
                $("#citi_discount_div").html('Avail ' + citi_discount +'% discount by paying with your Citibank <a href="#pay_by_dc" onClick="$(\'#pay_by_dc\').click();return false;">Debit Card</a>');
            }

            if(icici_discount>0) {
                $("#icici_discount_div").html(icici_discount_text);
            }

            $(".order-summary .cod-charges").hide();
            $(".order-summary .additional-discount").hide();
            $(".order-summary .grand-total > .value").text("Rs. " + formatNumber(total_amount));

            if(onlinePaymentEnabled == 1) {
                if(formtoSubmit == 'credit_card') {
                    if(is_icici_gateway_up)    showUserCardType(document.getElementById('credit_card').card_number.value);
                    _gaq.push(['_trackEvent', 'payment_page', 'switch_payment_mode', 'creditcard']);
                } else if (formtoSubmit == 'net_banking') {
                    // Pay with Net Banking -or- Pay with Cash Card option is selected
                    buttonText = "Proceed to Payment";
                    netBankingOptionChange();
                    _gaq.push(['_trackEvent', 'payment_page', 'switch_payment_mode', 'netbanking']);
                } else if (formtoSubmit == 'debit_card') {
                    if(is_icici_gateway_up) showUserCardType(document.getElementById('debit_card').card_number.value);
                    _gaq.push(['_trackEvent', 'payment_page', 'switch_payment_mode', 'debitcard']);
                } else if(formtoSubmit == 'phone_payment') {
                    $(".pay-btn").hide();
                    _gaq.push(['_trackEvent', 'payment_page', 'switch_payment_mode', 'pay_by_phone']);
                } else if (formtoSubmit == 'emi') {
                    if(!emiEnabled) $(".pay-btn").hide(); 
                    showUserCardType(document.getElementById('emi').card_number.value);
                    _gaq.push(['_trackEvent', 'payment_page', 'switch_payment_mode', 'emi']);
                } else if(formtoSubmit == 'cod') {
                    if(codCharge>0) {
                        $(".order-summary .cod-charges").show().find(" > .value").text(formatNumber(codCharge));
                        var finalAmount = total_amount + codCharge;
                        finalAmount = finalAmount<0?0:finalAmount;
                        updateTotal(finalAmount);
                    }
                    _gaq.push(['_trackEvent', 'payment_page', 'switch_payment_mode', 'cod']);
                    $(".pay-btn").hide();
                    codTabClicked = true;
                }
                
            } else {
                if(formtoSubmit == 'cod') {
                    if(codCharge>0) {
                        $(".order-summary .cod-charges").show().find(" > .value").text(formatNumber(codCharge));
                        var finalAmount = total_amount + codCharge;
                        finalAmount = finalAmount<0?0:finalAmount;
                        updateTotal(finalAmount);
                    }
                    // Cash on delivery option is selected
                    buttonText = "Place the Order";
                    $(".pay-btn").hide();
                    if(codErrorCode>0) {
                        if(!codTabClicked){
                             _gaq.push(['_trackEvent', 'payment_page', 'cod_error_message', $('#codErrorCode').val()]);
                             if(codErrorCode==2){
                                _gaq.push(['_trackEvent', 'payment_page', 'cod_unserviceable_zipcode', $('#addressPincode').val()]);
                             }
                        }
                    }
                    _gaq.push(['_trackEvent', 'payment_page', 'switch_payment_mode', 'cod']);
                    codTabClicked = true;
                }
                $(".pay-btn").hide();
             } 
            if (formtoSubmit != 'emi') {
                $('.you-pay').html($('.you-pay').attr('data-original-value'));
                $('.emi-charges').hide();
                if($('.order-summary-total').hasClass('no-cashback')){
                    $('.order-summary-total').addClass('hide');
                }
            } else if ($('form#emi .emi-duration .selected').length) {
                var row = $('form#emi .emi-duration .selected').closest('tr');
                $('.emi-charges').show().find('.value').html(row.find('.fee').html());
                $('.order-summary-total').removeClass('hide');
                $('.you-pay').html(row.find('.total').html());
            }
            $(".pay-btn").find('.btn-pay-txt').html(buttonText);

        });

        $items.filter('[class*=selected]').first().click();
        if(oneClickCod)  {
            $('#pay_by_cod').click();
        }
    }

/*    // Close icon for Coupon SMS Verification
    $('#smsVerification .close').click( function(e) {
        $('#smsVerification').hide();
    });
*/
});

$(document).ready(function(){
    /* Toggle Text */
    toggle_text_all(".toggle-text");
    /*trigger cashback form submit*/
    $(".remove-cb").bind("click",function(){
        removeMyntCash();
    });
    $(".redeem-cb").bind("click",function(){
    	redeemMyntCash();
    });

    $(".remove-lp").bind("click",function(){
        removeLoyaltyPoints();
    });

    $(".redeem-lp").bind("click",function(){
        useLoyaltyPoints();
    }); 

    $('.cancel-payment-btn').click(function() {
        if(paymentProcessingPopup!=null) {
            if(checkPaymentWindow !=null) {
                checkPaymentWindow = window.clearInterval(checkPaymentWindow);
            }
            if(paymentChildWindow!=null) {
                paymentChildWindow.close();
            }
            paymentProcessingPopup.hide();
            window.focus();
            $.ajax({
                type: "POST",
                 url: "declineLastOrder.php",
                 data: "reason_code=2&order_type="+Myntra.Data.OrderType,
                 success: function(data) {
                 },
                 error:function (xhr, ajaxOptions, thrownError){
                 }
            });
        }
    });
    
    $('form#emi .emi-duration table tr').live('click', function() {
        if(!$(this).hasClass("emi-disabled")){
            if(!$(this).closest('table').find('.radio-btn').hasClass("disabled")){
                $(this).closest('table').find('.radio-btn').removeClass('selected');
                $(this).find('.radio-btn').addClass('selected');
                $('.emi-charges').show().find('.value').html($(this).find('.fee').html());
                $('.you-pay').html($(this).find('.total').html());
                //TODO: update hidden form field for EMI Options here
                $('form#emi #emi_bank').val($(this).find('.duration').attr('data-duration'));
            }
        }    
    });

    $('form#emi select#emi-bank').live('change', function() {
        //TODO: update hidden form field for EMI Bank here
    });
    
});

/*
 * This function finds all HTML elements with "toggle-text" class and binds the "focus"/"blur" events for
 * toggling the default text in those elements.
 */
function toggle_text_all(selector) {
    $(selector).each(function(e) {
        toggle_text_elements[this.id] = $(this).val();
        addToggleText(this);
    });
}

function validateCard(cardValue){
         // accept only digits, dashes or spaces
                if (/[^0-9-\s]+/.test(cardValue)){
                     $('.err-card').html("Please enter valid card number");
 //                    $(".pay-btn").attr("disabled","disabled");
                    return false;
                }
                
                var nCheck = 0, nDigit = 0, bEven = false;
                cardValue = cardValue.replace(/\D/g, "");
             
                if(cardValue===null||cardValue.length<13){
                    $('.err-card').html("Please enter valid card number");
 //                   $(".pay-btn").attr("disabled","disabled");
                    return false;
                }
               
                for (var n = cardValue.length - 1; n >= 0; n--) {
                    var cDigit = cardValue.charAt(n),
                        nDigit = parseInt(cDigit, 10);

                    if (bEven) {
                        if ((nDigit *= 2) > 9) nDigit -= 9;
                    }

                    nCheck += nDigit;
                    bEven = !bEven;
                }

                var re = (nCheck % 10) == 0;
        
                if(nCheck==0)
                    re=false;
        
                 if(!re){
                    $('.err-card').html("Entered card number is invalid.");
 //                   $(".pay-btn").attr("disabled","disabled");  
                    return false;
                }

                return true;
    }

function addToggleText(elem) {
    $(elem).addClass("blurred");
    $(elem).focus(function() {
        if ($(this).val() == toggle_text_elements[this.id]) {
            $(this).val("");
            $(this).removeClass("blurred");
        }
    }).blur(function() {
        if ($(this).val() == "") {
            $(this).addClass("blurred");
            $(this).val(toggle_text_elements[this.id]);
        }
    });
}

function getCardType(cardNumber){
    cardNumber = cardNumber.toString();
    var tempCardNumber=cardNumber;
    tempCardNumber=cardNumber.replace(/-/g,"");
    tempCardNumber=tempCardNumber.replace(/ /g,"");
    cardNumber=tempCardNumber.valueOf();
    if(cardNumber.match(/\D/)){
        return false ;
    }
    if(cardNumber===null||cardNumber.length<13||cardNumber.search(/[a-zA-Z]+/)!=-1){
        return false;
    }
    returnValue=true;
    if(returnValue){
        var cardIdentifiers={1:{identifier:[4],numlength:[13,14,15,16]},2:{identifier:[51,52,53,54,55],numlength:[16]},3:{identifier:[34,37],numlength:[15]},4:{identifier:[300,301,302,303,304,305,36,38],numlength:[14]},5:{identifier:[502260,504433,5044339,504434,504435,504437,504645,504753,504775,504809,504817,504834,504848,504884,504973,504993,508159,600206,603123,603845,622018,508227,508192,508125,508126],numlength:[16,17,18,19]}};

        var cardType=false;
        for(ruleId in cardIdentifiers){
            var temp=cardIdentifiers[ruleId];
            if($.inArray(cardNumber.length,temp.numlength)>-1){
                for(i=0;i<temp.identifier.length;i++){
                    if(cardNumber.substr(0,(temp.identifier[i]+"").length)==temp.identifier[i]){
                        return ruleId;
                        break;
                    }
                }
                if(ruleId==5){
                    cardType="others";
                }
            }
        }
        if(cardType){
            return cardType;
        }else{
            return false;
        }
    }
    return false;
}

function getCardTypeForFade(cardNumber){
    cardNumber = cardNumber.toString();
    var tempCardNumber=cardNumber;
    tempCardNumber=cardNumber.replace(/-/g,"");
    tempCardNumber=tempCardNumber.replace(/ /g,"");
    cardNumber=tempCardNumber.valueOf();
    if(cardNumber.match(/\D/)){
        return false ;
    }
    if(cardNumber===null||!cardNumber.length||cardNumber.search(/[a-zA-Z]+/)!=-1){
        return false;
    }
    var cardIdentifiers={1:{identifier:[4]},2:{identifier:[51,52,53,54,55]},3:{identifier:[34,37]},4:{identifier:[300,301,302,303,304,305,36,38]},5:{identifier:[502260,504433,5044339,504434,504435,504437,504645,504753,504775,504809,504817,504834,504848,504884,504973,504993,508159,600206,603123,603845,622018,508227,508192,508125,508126]}};
    var cardType=false;
    if(tempCardNumber.length>=4) {
        for(ruleId in cardIdentifiers){
            var temp=cardIdentifiers[ruleId];
            for(i=0;i<temp.identifier.length;i++){
                if(cardNumber.substr(0,(temp.identifier[i]+"").length)==temp.identifier[i]){
                    return ruleId;
                    break;
                }
            }
            if(ruleId==5){
                cardType="others";
            }
        }
        return cardType;
    }
    return false;
}

function updateTotal(total) {
    var markup = '<span class="rupees">Rs. </span>' + formatNumber(total);
    $(".order-summary .grand-total > .value").html(markup);
    $(".order-summary .total-amount").html(markup);
}

function showUserCardType(cardNumber) {
    var cardID = getCardType(cardNumber);
    var cardType = getCardTypeForFade(cardNumber);

    if(citi_discount>0) {
        if(cardID) {
            var bin = cardNumber.substring(0,6);
            if($.inArray(bin,citiCardArray) > -1) {
                var citiDiscount = amountToDiscount*citi_discount/100;
                citiDiscount = Math.round(citiDiscount);
                $(".order-summary .additional-discount").show().find("> .value").text("(-) Rs. " + formatNumber(citiDiscount));
                var finalAmount = total_amount - citiDiscount;
                finalAmount = finalAmount<0?0:finalAmount;
                updateTotal(finalAmount);
                $("#citi_discount_div").html("Congrats! A discount of <span style='color:#d20000'>Rs. " + formatNumber(citiDiscount) +"</span> has been applied");
            } else {
                $(".order-summary .additional-discount").hide();
                updateTotal(total_amount);
                $("#citi_discount_div").html('Avail ' + citi_discount +'% discount by paying with your Citibank <a href="#pay_by_dc" onClick="$(\'#pay_by_dc\').click();return false;">Debit Card</a>');
            }
        } else {
            $(".order-summary .additional-discount").hide();
            updateTotal(total_amount);
            $("#citi_discount_div").html('Avail ' + citi_discount +'% discount by paying with your Citibank <a href="#pay_by_dc" onClick="$(\'#pay_by_dc\').click();return false;">Debit Card</a>');
        }
    }

    if(icici_discount>0) {
        if(cardID) {
            var bin = cardNumber.substring(0,6);
            if($.inArray(bin,iciciCardArray) > -1) {
                var iciciDiscount = amountToDiscount*icici_discount/100;
                iciciDiscount = Math.round(iciciDiscount);
                $(".order-summary .additional-discount").show().find("> .value").text("(-) Rs. " + formatNumber(iciciDiscount));
                var finalAmount = total_amount - iciciDiscount;
                finalAmount = finalAmount<0?0:finalAmount;
                updateTotal(finalAmount);
                $("#icici_discount_div").fadeOut('slow',function(){
                    $("#icici_discount_div").css("background-color","#ffffcc");
                    $("#icici_discount_div").html("Congrats! A discount of <span style='color:#d20000'>Rs. " + formatNumber(iciciDiscount) +"</span> has been applied").fadeIn(3000,function() {
                        $("#icici_discount_div").css("background-color","#ffffff");
                    });
                });
            } else {
                $(".order-summary .additional-discount").hide();
                updateTotal(total_amount);
                $("#icici_discount_div").html(icici_discount_text);
            }
        } else {
            $(".order-summary .additional-discount").hide();
            updateTotal(total_amount);
            $("#icici_discount_div").html(icici_discount_text);
        }
    }

    if(cardType==1){
        //visa
        $(".master").animate({'opacity': '0.3'});
        $(".maestro").animate({'opacity': '0.3'});
        $('#debit_card .ostar').fadeIn();
        $('#debit_card .field-msg').animate({'opacity': '0'});
    } else if(cardType==2){
        //masterCard
        $(".visa").animate({'opacity': '0.3'});
        $(".maestro").animate({'opacity': '0.3'});
        $('#debit_card .ostar').fadeIn();
        $('#debit_card .field-msg').animate({'opacity': '0'});
    } else if(cardType==5){
        //maestro
        $(".visa").animate({'opacity': '0.3'});
        $(".master").animate({'opacity': '0.3'});
        $('#debit_card .ostar').fadeOut();
        $('#debit_card .field-msg').animate({'opacity': '1'});
    } else if(cardType==3 || cardType==4 || cardType=="others"){
        $(".cardType").animate({'opacity': '1'});
        $('#debit_card .ostar').fadeOut();
        $('#debit_card .field-msg').animate({'opacity': '1'});
    } else{
        $(".cardType").animate({'opacity': '1'});
        $('#debit_card .ostar').fadeOut();
        $('#debit_card .field-msg').animate({'opacity': '1'});
    }

     $('.err-card').html("");
 //    $(".pay-btn").removeAttr("disabled");
}


function validatePaymentForm(formID){
    if(formID=='net_banking') {
        if($("#netBanking").val() == '') {
            alert("Please select a valid Netbanking option");
            return false;
        }
    } 
    else if (formID=='one-click-credit_card'||formID=='credit_card'||formID=='emi') {
        var form = document.getElementById(formID);

        if($('#'+formID).find('input[name="use_saved_card"]').length && $('#'+formID).find('input[name="use_saved_card"]').val() == 'true'){

            if(!$('.card-selected').length){
                alert('please select a card to continue');
                return false;
            }
            else{
                var saved_cvv_code = $('#'+formID).find('.card-selected').closest('li.card').find('input[name="saved_cvv_code"]').val();
                $('#'+formID).find('input[name="saved_cvv_code"]').val('');
                $('#'+formID).find('.card-selected').closest('li.card').find('input[name="saved_cvv_code"]').val(saved_cvv_code);
                $('#'+formID).find('input[name="payment_instrument"]').val($('#'+formID).find('.card-selected').data('key'));
                if((/\D/.test(saved_cvv_code)) || saved_cvv_code.length < 3){
                    alert("Please enter a valid card CVV number");
                    return false;
                }
                else {
                    $('#'+formID).find('input[name="cvv_code"]').val(saved_cvv_code);
                }

            }
        }
        else {
            
            var card_number = form.card_number.value;


            var st =  validateCard(card_number);

               if(!st){
                return false;
               }

            var cardID = getCardType(card_number);
            if(!cardID){
                alert("Please enter a valid credit card number");
                return false;
            }
        
            if(formID=='emi') {
                var bin = card_number.substring(0,6);
                var bankName = form.selectedBank.value;
                if($.inArray(bin,Myntra.Payments.EMICardArray[bankName]) > -1) {
                
                } else {
                    alert("Card number provided is not eligible for EMI. Please pay using any other payment mode or try with a different card number.");
                    return false;
                }
            
                var cvv = form.cvv_code.value;
                if (cvv.length != 3 && bankName == "CITI") {
                    alert("Please enter a valid card CVV number");
                    return false;
                }
            }

            var card_month = form.card_month.value;
            if(card_month == null || card_month == '') {
               alert("Please select card expiry month");
               return false;
            }
            var card_year = form.card_year.value;
            if(card_year == null || card_year == '') {
                alert("Please select card expiry year");
                return false;
            }

            var cvv_code = $('#'+formID).find('.cc-form').find('input[name="cvv_code"]').val();
            /*Clear all the other CVV codes except the selected on*/
            $('#'+formID).find('input[name="cvv_code"]').val('');
            $('#'+formID).find('.cc-form').find('input[name="cvv_code"]').val(cvv_code);
            if((/\D/.test(cvv_code)) || cvv_code.length < 3){
                alert("Please enter a valid card CVV number");
                return false;
            }

            var bill_name = form.bill_name.value;
            if(bill_name==null || bill_name == '' ){
                alert("Please enter the name as it appears on card");
                return false;
            }
            if(Myntra.Data.OrderType != "gifting"){
                var b_firstname = form.b_firstname.value;
                if(b_firstname==null || b_firstname == '' || b_firstname == 'Name'){
                    alert("Please enter a billing name");
                    return false;
                }

                var b_address = form.b_address.value;
                if(b_address==null || b_address == '' || b_address == 'Enter your billing address here'){
                    alert("Please enter a billing address");
                    return false;
                }

                var b_city = form.b_city.value;
                if(b_city==null || b_city == '' || b_city == 'City'){
                    alert("Please enter a billing city");
                    return false;
                }

                var b_state = form.b_state.value;
                if(b_state==null || b_state == '' || b_state == 'State'){
                    alert("Please enter a billing state");
                    return false;
                }

                var b_zipcode = form.b_zipcode.value;
                if(b_zipcode==null || b_zipcode == '' || b_zipcode == 'PinCode' ||(/\D/.test(b_zipcode))){
                    alert("Please enter a valid PinCode/ZipCode");
                    return false;
                }
            }
        }
    } else if (formID=='debit_card') {

        var form = document.getElementById(formID);
        
        if($('#'+formID).find('input[name="use_saved_card"]').length && $('#'+formID).find('input[name="use_saved_card"]').val() == 'true'){
            if(!$('.card-selected').length){
                alert('please select a card to continue');
                return false;
            }
            else{
                var saved_cvv_code = $('#'+formID).find('.card-selected').closest('li.card').find('input[name="saved_cvv_code"]').val();
                $('#'+formID).find('input[name="saved_cvv_code"]').val('');
                $('#'+formID).find('.card-selected').closest('li.card').find('input[name="saved_cvv_code"]').val(saved_cvv_code);
                $('#'+formID).find('input[name="payment_instrument"]').val($('#'+formID).find('.card-selected').data('key'));
                if((/\D/.test(saved_cvv_code)) || saved_cvv_code.length < 3){
                    alert("Please enter a valid card CVV number");
                    return false;
                }
                else {
                    $('#'+formID).find('input[name="cvv_code"]').val(saved_cvv_code);
                }

            }
        }
        else {
            // /console.log('use new form');return false;
           var card_number = form.card_number.value;
         var st =  validateCard(card_number);

               if(!st){
                return false;
               }

           var cardID = getCardType(card_number);
           if(!cardID){
                alert("Please enter a valid debit card number");
                return false;
           }

          if(cardID==1 || cardID == 2) {
            var card_month = form.card_month.value;
            if(card_month == null || card_month == '') {
                alert("Please select card expiry month");
                return false;
            }

            var card_year = form.card_year.value;
            if(card_year == null || card_year == '') {
                alert("Please select card expiry year");
                return false;
            }

            var cvv_code = $('#'+formID).find('.cc-form').find('input[name="cvv_code"]').val();
            $('#'+formID).find('input[name="cvv_code"]').val('');
            $('#'+formID).find('.cc-form').find('input[name="cvv_code"]').val(cvv_code);
            if((/\D/.test(cvv_code)) || cvv_code.length < 3){
                alert("Please enter a valid card CVV number");
                return false;
            }
          }

           var bill_name = form.bill_name.value;
           if(bill_name==null || bill_name == '' ){
             alert("Please enter the name as it appears on card");
             return false;
           }
      }

    } 
    else if(formID=='one-click-default-credit_card'){
        //validate CVVvar cvv_code = form.cvv_code.value;
          var form = document.getElementById(formID);
          var cvv_code = form.cvv_code.value;
            if((/\D/.test(cvv_code)) || cvv_code.length < 3){
                alert("Please enter a valid card CVV number");
                return false;
            }
        
    }
    else if(formID=='one-click-cod'){
        return true;
    }
    else if (formID=='cashcard') {
        return true;
    } else if(formID=='cod') {
        return true;
    }
    return true;
}
function submitPaymentForm(validate){
    var elem = $('#paymentFormToSubmit').val();
    var form = $('#'+elem);

    if(validate){
        if(validatePaymentForm(elem)){

        } else{
            return false;
        }
    }

    /* BIN based promotion starts*/
    if(Myntra.Payments.paymentCoupon.hasPaymentCoupon && !$('.nothing-to-pay').length){
        // Need to check if the user has proper method selected or 
        var formID = elem;
        if(formID=='credit_card' || formID == 'debit_card'){
            var binNum = form.find('input[name="card_number"]').val().substr(0,6);
            checkBinPromotion(binNum,function(){
                paymentFormSubmission(validate,elem,form);
            },true);
        }else{
            binPromotionBox('wrongMethod');
            return false;
        }
    }else{
        paymentFormSubmission(validate,elem,form);
    }
    /* BIN based promotion ends*/
}


function paymentFormSubmission(validate,elem,form){
    if(validate){
        _gaq.push(['_trackEvent', 'payment_page', 'proceed_to_pay', elem]);
    } else {
        _gaq.push(['_trackEvent', 'payment_page', 'use_intl_creditcard']);
    }
    //Removing popup for all mobile payments. As its getting blocked for some browsers.
    form.attr("target","");
    form.submit();
    return false;     
        /**
        if(Myntra.Data.isMobileApp){
            //In case of mobile app open the payments page in the current window itself
            form.attr("target","");
            form.submit();
            return false; 
        }
        $.browser.chrome = /chrom(e|ium)|crios/.test(navigator.userAgent.toLowerCase());
         if(!$.browser.chrome){
             var wdh=800;//0.75*screen.width;//800
             var hgt=400;//0.75*screen.height;//400
             var st="location=1,status=1,scrollbars=1,top=150,left=200,width="+wdh+",height="+hgt;
             paymentChildWindow=window.open('',"MyntraPayment",st);
         }
         var html = '<html><head><title>Myntra Payment</title></head><body><div><center>';
         html = html + '<h2>Processing Payment</h2>';
         html = html + '<img src="https://d6kkp3rk1o2kk.cloudfront.net/images/image_loading.gif">';
         html = html + '<div>Enter your card/bank authentication details in the window once it appears on your screen. <br/>';
         html = html + 'Please do not refresh the page or click the back button of your browser.<br/>';
         html = html + 'You will be redirected back to order confirmation page after the authentication.<br/><br/></div>';
         html = html + '</center></div></body></html>';

        if(paymentChildWindow!=null && !$.browser.chrome){
            try {
                paymentChildWindow.document.open();
                paymentChildWindow.document.write(html);
                paymentChildWindow.document.close();
            } catch (err){

            }
             if(!$.browser.chrome){
                paymentChildWindow.focus();
                if(checkPaymentWindow !=null) checkPaymentWindow = window.clearInterval(checkPaymentWindow);
                checkPaymentWindow = window.setInterval("checkClose()",1000);
             }
        }
        if(!$.browser.chrome){
            cfg = {"isModal":true,"disableCloseButton":true,"disableShim": true};
            paymentProcessingPopup = Myntra.LightBox('#splash-payments-processing',cfg);
            paymentProcessingPopup.show();
        }
    }
    if($.browser.chrome){
        form.attr("target","");
    }
    form.submit();
    return false;
    **/
}

function checkClose() {
    if(paymentChildWindow!=null) {
        if(paymentChildWindow.closed) {
            //$("#pay_by_cod").trigger("click");
            //$(".notification-error").html("payment seem to have failed");
            checkPaymentWindow = window.clearInterval(checkPaymentWindow);
            $.ajax({
                type: "POST",
                 url: "declineLastOrder.php",
                 data: "reason_code=1&order_type="+Myntra.Data.OrderType,
                 success: function(data) {
                     var resp=$.trim(data);
                     paymentProcessingPopup.hide();
                     window.focus();
                 },
                 error:function (xhr, ajaxOptions, thrownError){
                     paymentProcessingPopup.hide();
                     window.focus();
                 }
            });
        }
    }
}

function showAddressForm(emi){
    if (emi) {
        $('.address-form-emi').show();
        $(".address-list-wrap-emi .addressRest").hide();
        $("form#emi #cc_b_firstname").val("");
        $("form#emi #cc_b_address").val("");
        $("form#emi #cc_b_city").val("");
        $("form#emi #cc_b_state").val("");
        $("form#emi #cc_b_zipcode").val("");
        $(".address-list-wrap-emi .jqTransformChecked").removeClass('jqTransformChecked');
    }
    else{
        $('.address-form-cc').show();
        $(".address-list-wrap .addressRest").hide();
        $("form#credit_card #cc_b_firstname").val("");
        $("form#credit_card #cc_b_address").val("");
        $("form#credit_card #cc_b_city").val("");
        $("form#credit_card #cc_b_state").val("");
        $("form#credit_card #cc_b_zipcode").val("");
        $(".address-list-wrap .jqTransformChecked").removeClass('jqTransformChecked');
        
        
    }
}
$(document).ready(function(){
	// coupon "Know More" popup
    
	$('.coupon-info-link').click(function() {
		cfg = {"isModal":true};
		var couponInfo =  Myntra.LightBox('#lb-coupon-info',cfg);
		couponInfo.show();
    });
    $('input[placeholder]').placeholder({ color: '#D5D5D5'});
    $('textarea[placeholder]').placeholder({ color: '#D5D5D5'});
    
    $('#captcha-form').keydown(function(event) {
        if (event.keyCode == '13') {
            event.preventDefault();
            $("#cod-verify-captcha").trigger("click");
        }
    });
});

function verifyCaptcha() {
    if($("#cod-verify-captcha").hasClass("btn-disabled")){
        return;
    }
    if($('#captcha-form').val() == ""){
        $("#cod-captcha-message").html("Please enter the text.");
        $("#cod-captcha-message").addClass("error");
        $("#cod-captcha-message").slideDown();
        return;
    }
    $("#cod-captcha-loading").show();
    $("#cod-verify-captcha").addClass("btn-disabled")
    $("#cod-captcha-message").html("").removeClass("error");
    $("#cod-captcha-message").slideUp();
    $.ajax({
        type: "POST",
        url: https_loc+"/cod_verification.php",
        data: "codloginid="+ loginid + "&condition=clickverifycaptcha&confirmmode=customerpvqueued&mobile=" + $('#cod-mobile').val() + "&userinputcaptcha=" + $('#captcha-form').val() ,
        success: function(msg){

            var resp=$.trim(msg);
            $("#cod-captcha-loading").hide();
            $("#cod-verify-captcha").removeClass("btn-disabled");
            if(resp=='CaptchaConfirmed') {
                $("#cod-verify-captcha").hide();//addClass("btn-disabled");
                $("#cod-captcha-loading").show();
                $("#cod-captcha-message").html('Code verified. Please wait while your order is being processed.');
                $("#cod-captcha-message").addClass('success');
                $("#cod-captcha-message").slideDown();
                $("#cod").trigger("submit");
                _gaq.push(['_trackEvent', 'payment_page', 'cod_captcha', 'success']);
            } else if(resp=='WrongCaptcha') {
                $("#cod-captcha-message").html('Wrong Code entered');
                $("#cod-captcha-message").addClass('error');
                $("#cod-captcha-message").slideDown();
                _gaq.push(['_trackEvent', 'payment_page', 'cod_captcha', 'failure']);
            }
        },
        error:function (xhr, ajaxOptions, thrownError) {
            $("#cod-captcha-loading").hide();
            $("#cod-verify-captcha").removeClass("btn-disabled");
            $("#cod-captcha-message").html('Wrong Code entered');
            $("#cod-captcha-message").addClass('error');
            $("#cod-captcha-message").slideDown();
        }
    });
}

/* Cart Page counpon redeem */
/*function redeemCoupon(){
    var couponForm = $("#couponform");
    var string = $("#othercouponcode", couponForm).val();
    var pattern = /^[a-zA-Z0-9\s]+$/g;
    if(string == '' || !pattern.test(string) || string == "Enter a Coupon") {
        alert("Please enter a valid coupon code to redeem.");
        return;
    }
    _gaq.push(['_trackEvent', 'coupon_widget', 'coupon_redeem', 'click']);
    $("#couponcode", couponForm).val(string);
    couponForm.submit();
}*/
function redeemCashCoupon(){
    var userCashAmount =  parseFloat($("#userCashAmount").val());
    if(userCashAmount !='' && userCashAmount>0) {
        _gaq.push(['_trackEvent', 'coupon_widget', 'redeem_cashcoupon', 'click']);
        $("#redeemCashCouponButton").hide();
        document.cashcouponform.submit();
    }
}

function removeMyntCash()
{
        _gaq.push(['_trackEvent', 'payment_page', 'coupon_widget', 'remove_cashcoupon']);
        var couponForm = $("#myntCashForm");
        $("#useMyntCash", couponForm).val('remove');
        var appliedCoupon=$("#c-code").val();
        if(appliedCoupon != ''){
          $("#code", couponForm).val(appliedCoupon);
        }else{
                 $("#code", couponForm).val('');
        }
        document.myntCashForm.submit();
}

function redeemMyntCash(){
	var userCashAmount =  parseFloat($("#userCashAmount").val());
	if(userCashAmount !='' && userCashAmount>0) {
		_gaq.push(['_trackEvent', 'payment_page', 'coupon_widget', 'redeem_cashcoupon']);
		$("#redeemMyntCashButton").hide();
		document.myntCashForm.submit();
	}
}

function useLoyaltyPoints(){
    var userLoyaltyPoints =  parseFloat($("#userLoyaltyPoints").val());
    if(userLoyaltyPoints !='' && userLoyaltyPoints > 0) {
        _gaq.push(['_trackEvent', 'payment_page', 'loyalty_points', 'use_loyalty_points']);
        $("#loyaltyPoinsUsage").val('use');
        document.loyaltyPointsForm.submit();
    }
}

function removeLoyaltyPoints(){
        _gaq.push(['_trackEvent', 'payment_page', 'loyalty_points', 'remove_loyalty_points']);
        var loyaltyPointsForm = $("#loyaltyPointsForm");
        $("#loyaltyPoinsUsage", loyaltyPointsForm).val('remove');
        document.loyaltyPointsForm.submit();
}

/*
function removeCoupon()
{
    _gaq.push(['_trackEvent', 'coupon_widget', 'remove_coupon', 'click']);
    var couponForm = $("#couponform");
    $("#couponcode", couponForm).val('');
    $("#removecouponcode", couponForm).val('remove');
    document.couponform.submit();
}
*/
function removeCashCoupon()
{
    _gaq.push(['_trackEvent', 'coupon_widget', 'remove_cashcoupon', 'click']);
        var couponForm = $("#cashcouponform");
    $("#removecashcoupon", couponForm).val('removecoupon');
    var appliedCoupon=$("#c-code").val();
    if(appliedCoupon != ''){
      $("#code", couponForm).val(appliedCoupon);
    }else{
         $("#code", couponForm).val('');
    }
    document.cashcouponform.submit();
}

$("#discount_coupon_message").hover(
  function (is) {
    $(this).find("span").find("a").attr("style","color:#FF0000");
  },
  function () {
    $(this).find("span").find("a").attr("style","color:#COCOCO");
  }
);



function clearTextArea(){
    $("#othercouponcode").val('').blur();
}
$("#discount_cashcoupon_message").hover(
  function (is) {
    $(this).find("span").find("a").attr("style","color:#FF0000");
  },
  function () {
    $(this).find("span").find("a").attr("style","color:#COCOCO");
  }
);

$(".select-coupons li").hover(
  function () {
    $(".select_this_coupon a",this).addClass("mk-hover");
  },
  function () {
    $(".select_this_coupon a",this).removeClass("mk-hover");
  }
);

$(".your-coupons .select-coupons li .inline-block").click( function () {
    $('.your-coupons .select-coupons').slideUp('fast');
    $(this).parent().find(".select_this_coupon a").click();
});

function redeemCoupon2(couponCode){
    $('.your-coupons .select-coupons').slideUp('fast');
    var couponForm = $("#couponform");
    var string = couponCode;
    $("input#othercouponcode").val(couponCode).blur();
    //redeemCoupon();
    $('.coupon-apply-btn').removeAttr('disabled');
    /*$("#couponcode").val(couponCode);
    var pattern = /^[a-zA-Z0-9\s]+$/g;
    if(string == '' || !pattern.test(string)) {
        alert("Please enter a valid coupon code to redeem.");
        return;
    }
    _gaq.push(['_trackEvent', 'coupon_widget', 'coupon_select', 'click']);*/
    //couponForm.submit();
    $('.coupon-apply-btn', couponForm).click();
}

/*function verifyMobileCommon(id)    {
    $("#verify-mobile").hide();
    $("#verify-mobile-code").slideUp();
    var profileForm=$("#change-profile");
    $("#mobile-verify-loading").show();
    var mobile = $("#mobile", profileForm).val();
    var login = $("#login", profileForm).val();
    $("#mobile",profileForm).attr("disabled","disabled");
    
    $.ajax({
        type: "POST",
        url: "mymyntra.php",
        data: "&mode=check-mobile-user&mobile=" + $("#mobile", profileForm).val() + "&login=" + $("#login", profileForm).val()+"&_token="+$("#_token", profileForm).val(),
        success: function(msg){
            msg = $.trim(msg);
            if (msg == "sendSMS") {
                   $.ajax({
                       type: "POST",
                       url: "mymyntra.php",
                       data: "&mode=generate-code&mobile=" + $("#mobile", profileForm).val() + "&login=" + $("#login", profileForm).val()+"&_token="+$("#_token", profileForm).val(),
                       success: function(msg){
                           msg = $.trim(msg);
                           $("#mobile-verify-loading").hide();
                              if (msg == "showCaptcha")    {
                                  $("#v-code-note-message").hide();
                                  $("#v-code-note-message").removeClass("error");
                                  $("#v-code-note-message").text("Note: Please verify your specified contact number to be able to use any user-specific coupons that you may have been issued through Myntra. We may use this number to contact you for issues related to any of your transactions on Myntra.").fadeIn('slow');
                                  $("#verify-mobile").attr("value","Send Again")
                                  $("#mobile-captcha").attr("src", https_loc+'/captcha/captcha.php?id='+id+'&rand='+Math.random());
                                  $("#mobile-captcha-form").val("");
                                  $("#mobile-captcha-block").show();
                               $(".change-password-link").hide();
                           } else if (msg == "verifyFailed"){
                               $("#verify-mobile").show();
                               $("#mobile",profileForm).removeAttr("disabled");
                               $("#verify-mobile").attr("value","VERIFY");
                               $("#v-code-note-message").hide();
                                  $("#v-code-note-message").addClass("error");
                                  $("#v-code-note-message").text("You have exceeded the maximum no. of attempts to verify this mobile number. If you have still not received the verification code or are facing any other difficulties, please provide an alternate number and retry. Or, try again after 6 hours.").fadeIn('slow');
                           }
                           else{
                               $("#verify-mobile").show();
                               $("#mobile",profileForm).removeAttr("disabled");
                               $("#v-code-note-message").hide();
                                     $("#v-code-note-message").removeClass("error");
                                     $("#v-code-note-message").text("Note: Please verify your specified contact number to be able to use any user-specific coupons that you may have been issued through Myntra. We may use this number to contact you for issues related to any of your transactions on Myntra.").fadeIn('slow');
                               mobileVerificationSubmitCount=0;
                               $("#verify-mobile").attr("value","Send Again")
                               $("#verify-mobile-code").slideDown();
                               $(".mobile-code-entry").show();
                               $("#mobile-message").html("Please enter the verification code that was sent to <strong>" + mobile + "</strong>");

                               $("#mobile-message").show();
                               $("#mobile-code").val("");
                               $("#mobile-verification-error").removeClass("error");
                               $("#mobile-verification-error").text("If you have not received your verification code, please wait for atleast 2 minutes before pressing submit button");
                           }

                       },
                          error:function (xhr, ajaxOptions, thrownError) {
                           $("#mobile-verify-loading").hide();
                           $("#verify-mobile").show();
                           $("#mobile",profileForm).removeAttr("disabled");
                           $("#v-code-note-message").hide();
                           $("#v-code-note-message").addClass("error");
                           $("#v-code-note-message").text("An error has occured while sending the sms please retry again").fadeIn('slow');
                          }
                   });
               } else if (msg=="exceed"){
                      //number of attempts to be tried exceeds disable the edit button and show him the message to retry after 6 hours
                      $("#mobile",profileForm).removeAttr("disabled");
                   $("#mobile-verify-loading").hide();
                   $("#verify-mobile").show();
                   $("#v-code-note-message").hide();
                   $("#v-code-note-message").addClass("error");
                   $("#v-code-note-message").text("You have exceeded the maximum no. of attempts to verify this mobile number. If you have still not received the verification code or are facing any other difficulties, please provide an alternate number and retry. Or, try again after 6 hours.").fadeIn('slow');

               } else if (msg == "verified"){
                      //The number is already verified by some other user
                      $("#mobile-verify-loading").hide();
                   $("#verify-mobile").show();
                   $("#mobile",profileForm).removeAttr("disabled");
                   $("#v-code-note-message").hide();
                   $("#v-code-note-message").addClass("error");
                   $("#v-code-note-message").text("This number has been verified with us by another user. Please use another mobile number.").fadeIn('slow');
               } else if (msg == "skipVerify"){
                   $("#mobile-verify-loading").hide();
                   $("#v-code-note-message").hide();
                   $("#verify-mobile").hide();
                   window.location.reload();

               }else {
                      //Some other error has occured asked him to retry.
                      $("#mobile-verify-loading").hide();
                   $("#verify-mobile").show();
                   $("#mobile",profileForm).removeAttr("disabled");
                   $("#v-code-note-message").hide();
                   $("#v-code-note-message").addClass("error");
                   $("#v-code-note-message").text("An error has occured while sending the sms please retry again").fadeIn('slow');
               }
        },
           error:function (xhr, ajaxOptions, thrownError) {
            $("#mobile-verify-loading").hide();
            $("#verify-mobile").show();
            $("#mobile",profileForm).removeAttr("disabled");
            $("#v-code-note-message").hide();
            $("#v-code-note-message").addClass("error");
            $("#v-code-note-message").text("An error has occured while sending the sms please retry again").fadeIn('slow');
           }
    });
    _gaq.push(['_trackEvent', 'coupon_widget', 'verify_mobile', mobile]);
}
*/
function validateMobileNumber() {
    var numericReg  =  /(^[1-9]\d*$)/;
    var error = false;
    var profileForm=$("#change-profile");
    var mobileVal=$("#mobile", profileForm).val();
    $("#mobile-error", profileForm).hide();
    if(mobileVal == "" || mobileVal.length < 10 || !numericReg.test(mobileVal)){
        error=true;
        $("#mobile-error", profileForm).text("Enter a valid mobile number.").slideDown();
    }
    return error;

}



function verifyMobileCaptcha() {
    if($("#mobile-verify-captcha").hasClass("btn-disabled")){
        return;
    }
    if($('#mobile-captcha-form').val() == ""){
        $("#mobile-captcha-message").html("Please enter the text.");
        $("#mobile-captcha-message").addClass("error");
        $("#mobile-captcha-message").slideDown();
        return;
    }
    $("#mobile-captcha-loading").show();
    $("#mobile-verify-captcha").addClass("btn-disabled")
    $("#mobile-captcha-message").html("").removeClass("error");
    $("#mobile-captcha-message").slideUp();
    $.ajax({
        type: "POST",
        url: https_loc+"/mobile_verification.php",
        data: "mobile=" + $('#mobile').val() + "&userinputcaptcha=" + $('#mobile-captcha-form').val() + "&login=" + $('#mobile-login').val() + "&id=paymentpage",
        success: function(msg){

            var resp=$.trim(msg);
            $("#mobile-captcha-loading").hide();
            $("#mobile-verify-captcha").removeClass("btn-disabled");
            if(resp=='CaptchaConfirmed') {
                $("#mobile-captcha-block").hide();
                //$(".verify-mobile-cart-page").trigger("click");

            } else if(resp=='WrongCaptcha') {
                $("#mobile-captcha-message").html('Wrong Code entered');
                $("#mobile-captcha-message").addClass('error');
                $("#mobile-captcha-message").slideDown();
           }
        },
        error:function (xhr, ajaxOptions, thrownError){
            $("#mobile-captcha-loading").hide();
            $("#mobile-verify-captcha").removeClass("btn-disabled");
            $("#mobile-captcha-message").html('Internal Error Occured. Please Retry again');
            $("#mobile-captcha-message").addClass('error');
            $("#mobile-captcha-message").slideDown();
        }
    });
    _gaq.push(['_trackEvent', 'coupon_widget', 'verify_mobile_captcha', 'click']);
}

function netBankingOptionChange() {
        var selectText = $("#netBanking option:selected").text();
        selectText  = selectText.toLowerCase();
        if(selectText.indexOf('icici') != -1) {
            if(icici_discount>0) {
                var iciciDiscount = amountToDiscount*icici_discount/100;
                iciciDiscount = Math.round(iciciDiscount);
                var finalAmount = total_amount - iciciDiscount;
                finalAmount = finalAmount<0?0:finalAmount;
                $(".order-summary .additional-discount").show().find("> .value").text("(-) Rs. " + formatNumber(iciciDiscount));
                updateTotal(finalAmount);
                $("#icici_discount_div").fadeOut('slow',function() {
                    $("#icici_discount_div").css("background-color","#ffffcc");
                    $("#icici_discount_div").html("Congrats! A discount of <span style='color:#d20000'>Rs. " + formatNumber(iciciDiscount) +"</span> has been applied").fadeIn(3000,function() {
                        $("#icici_discount_div").css("background-color","#ffffff");
                    });
                });
            }
        } else {
            $(".order-summary .additional-discount").hide();
            updateTotal(total_amount);
            $("#icici_discount_div").html(icici_discount_text);
        }
}

$(document).ready(function() {

    $('input[placeholder]').placeholder({ color: '#D5D5D5'});
    $('textarea[placeholder]').placeholder({ color: '#D5D5D5'});
    $('.your-coupons > a').click(function(e) {
        var couponList = $('.your-coupons .select-coupons'); 
        if(couponList.length > 0){
            if(couponList.css('display') == "none")    {
                _gaq.push(['_trackEvent', 'coupon_widget', 'coupon_dropdown', 'click']);
            }
            couponList.slideToggle();
            //e.stopPropagation();
        }
    });
    
    $('.coupons-widget .err-div .close').click( function() {
        $(this).closest('.err-div').fadeOut("fast");
        $(this).parents(".coupons-widget").find(".wrong-large-icon").fadeOut("fast");
        //$(this).parents(".coupons-widget").find(".wrong-large-icon").remove();
    });

    $('.your-coupons .select-coupons .close').click( function() {
        $('.your-coupons .select-coupons').slideUp();
    });

    $('.your-coupons .select-coupons li').hover(
        function () {
          $(this).addClass('couponSelect');
        },
        function () {
            $('.your-coupons .select-coupons li').removeClass('couponSelect');
        }
    );

    $("#lmore").click(function(){
        $('.lmore').toggleClass("hide");
    });

    $(".code-enter .coupon-code").keydown(function(event) {
        if(event.keyCode == '13'){
            event.preventDefault();
            redeemCoupon();
        }
    }).keyup(function(event) {
        if($(this).val()=='' || $(this).val()==null) {
            $('.coupon-apply-btn').attr('disabled','disabled');
        } else {
            $('.coupon-apply-btn').removeAttr('disabled');
        }
    });
    
    /*$('#mobile').keydown(function(event) {
        if (event.keyCode == '13') {
            event.preventDefault();
            if ($(".verify-mobile-cart-page").css("display") != "none") {
                $(".verify-mobile-cart-page").trigger("click");
            }
        }
    });*/

    $('#mobile-captcha-form').keydown(function(event) {
        if (event.keyCode == '13') {
        event.preventDefault();
        $("#mobile-verify-captcha").trigger("click");
        }
    });

    /*$(".verify-mobile-cart-page").live('click',function(){
        if(!validateMobileNumber()) {
            verifyMobileCommon("paymentpage");
        }
    });*/

    $(".submit-code").live('click',function(){
        var profileForm=$("#verify-mobile-code");
        var codeStr = $("#mobile-code", profileForm).val();
        if (codeStr != "") {
            $("#submit-code").hide();
            $("#code-verify-loading").show();
            $.ajax({
                type: "POST",
                url: "mkpaymentoptions.php",
                data: "&mode=verify-mobile-code&mobile-code=" +codeStr,
                success: function(data) {
                    var responseObj=$.parseJSON(data);
                    var status=responseObj.status;
                    var message = responseObj.message;
                    var num_attempts = responseObj.attempts;
                    if(status==1){
                        //reload the page verification was successful.
                        $("#mobile-verification-error").hide();
                        $("#mobile-verification-error").removeClass("error");
                        $("#mobile-verification-error").text(message).fadeIn('slow');
                        window.location.reload();
                    } else {
                        $("#submit-code").show();
                        $("#code-verify-loading").hide();
                        mobileVerificationSubmitCount++;
                        if(mobileVerificationSubmitCount>5) {
                            //hide and ask him to resend the sms.
                            $("#verify-mobile-code").slideUp();
                            $("#v-code-note-message").hide();
                            $("#v-code-note-message").addClass("error");
                            $("#v-code-note-message").text("You have exceeded the maximum no. of attempts to validate the SMS code. To try verifying your mobile again please try with a new verification code.").fadeIn('slow');
                        } else {
                            $("#mobile-verification-error").text(message);
                            $("#mobile-verification-error").addClass("error");
                        }
                    }
                },
                error:function (xhr, ajaxOptions, thrownError){
                    $("#submit-code").show();
                    $("#code-verify-loading").hide();
                    $("#mobile-verification-error").text("Internal Error occured. Please retry again.");
                    $("#mobile-verification-error").addClass("error");
                }
            });
            _gaq.push(['_trackEvent', 'coupon_widget', 'submit_mobile_code', 'click']);
        }
    });
    $("#order-summary").hover(function(){
        //$(this).toggleClass('black-bg4');
        $(this).toggleClass("bgcolor");
    });
    $("#order-summary").live('click',function(){
        $("#order-summary").toggleClass('downarrow', 'uparrow');
        $("#order-cart-items").toggle(400);
        if(!orderSummaryDislplayed) {
            $("#cart-pre-loading").show();
            orderSummaryDislplayed = true;
            $.ajax({
                type: "POST",
                url: "getMyCartData.php",
                data: "",
                success: function(data) {
                     $("#cart-pre-loading").hide();
                    if(data!=null) $("#order-cart-items").html(data);
                },
                error:function (xhr, ajaxOptions, thrownError){
                    orderSummaryDislplayed = false;
                     $("#cart-pre-loading").hide();
                }
            });
            _gaq.push(['_trackEvent', 'payment_page', 'order_summary', 'click']);
        }

    });

    $("#phone-order-confirm").live('click',function(){
        $("#phone_payment").submit();
        _gaq.push(['_trackEvent', 'payment_page', 'pay_by_phone', 'confirm_order']);
    });
//When page loads...
    //On Click Event
    $("span.discount_option input").click(function() {
           $(".tab_content").hide();
            var activeTab = $(this).attr("class");
            $(activeTab).fadeIn();
        });

    });

// Cash on Delivery Mobile verification
/*
$('.num-edit-btn').click(function(){
    $(this).hide();
    $('.edit-your-number').show();
    $('.your-number').hide();
    $('.verify-block').hide();
});

$('.cancel-btn').click(function(){
    $('.edit-your-number').hide();
    $('.num-error').hide();
    $('.warning').show();
    $('.num-edit-btn').show();
    $('.your-number').show();
    $('.verify-block').show();

});
*/
/*
$('.save-btn').click(function(){
   var mobinum = $('input.mobile-number').val();
    var numericReg  =  /(^-?\d\d*\.\d*$)|(^-?\d\d*$)|(^-?\.\d\d*$)/;

    if((mobinum == userMobileNumber) ||
        (mobinum == '') ||
        (!numericReg.test(mobinum)) ||
        (mobinum.length < 10)
        ){
            return;
    }else{

        $('.your-number #userMobile').html(mobinum);
        $('.verify-sms #userMobile').html(mobinum);
        $('.verify-block').show();
        $('.num-edit-btn').show();
        $('.your-number').show();
        $('.edit-your-number').hide();

        $('#mynt-verify-btn').addClass('disable-btn');
        $('#edit-btn-mobile-verify').attr('disabled','true');
        $.ajax({
            type: "POST",
            url: "mkpaymentoptions.php",
            data: "&mode=update-mobile-number&new-mobile-number="+mobinum+"&login="+loginid,
            success: function(data) {
                var resp=$.trim(data);
                if(resp == "0")    {
                    //prompt for verification
                    $('.num-error').hide();
                    $('.warning').show();
                }
                if(resp == "1"){
                    $('.warning').hide();
                    $('.num-error').show();
                }
                userMobileNumber = mobinum;
                $('#mynt-verify-btn').removeClass('disable-btn');
                $('#edit-btn-mobile-verify').removeAttr('disabled');
            },
            error:function (xhr, ajaxOptions, thrownError){
                 $('.warning').show();
                 $('#mynt-verify-btn').removeClass('disable-btn');
                 $('#edit-btn-mobile-verify').removeAttr('disabled');
            }
        });
    }

    try { $(this).blur(); } catch (ex) {}
});*/


function checkBinPromotion(binNum,clbk,showLoader){
    var promotionCheckUrl = '/myntra-absolut-service/absolut/coupon/isvalidbin/default?bin='+binNum;
    var currentRequest = $.ajax({
        url: promotionCheckUrl,
        type: 'GET',
        contentType: 'application/json',
        headers : {
            Accept : "application/json; charset=utf-8",
            'X-CSRF-TOKEN': Myntra.Data.token,
            'Accept-Language': 'en-US,en;q=0.8'
        },
        beforeSend : function(){
            if(currentRequest != null){
                currentRequest.abort();
            }
            if(showLoader){
                binPromotionBox('showProcessing');
            }
        },
        success: function(d) {
            if(d.applicability && d.pgCouponApplied){
                binPromotionBox('hide');
                clbk(); // Call the callback to continue the flow
            }else{
                binPromotionBox('wrongCard');
            }
        },

        error: function (error) {
            binPromotionBox('serverError');
            _gaq.push(['_trackEvent', 'mobile_payment', 'error: ' + error]);
        }

    });
}

// function showBinErrorBox(errorType){
//     var binErrorBox = Myntra.LightBox('#bin-promotion-error', {isModal:true});
//     if(errorType == 'wrongMethod'){
//         $('#bin-promotion-error').find('.wrong-card-entered').addClass('mk-hide');
//         $('#bin-promotion-error').find('.wrong-method-chosen').removeClass('mk-hide');
//     }else{
//         $('#bin-promotion-error').find('.wrong-card-entered').removeClass('mk-hide');
//         $('#bin-promotion-error').find('.wrong-method-chosen').addClass('mk-hide');
//     }
//     binErrorBox.show();
// }


function binPromotionBox(type){
    var binErrorBox = Myntra.LightBox('#bin-promotion-box', {isModal:true});
    var $el = $('#bin-promotion-box');
    if(type == 'hide') {
        binErrorBox.hide();
        return;
    }
    $el.find('.removing-coupon').addClass('mk-hide');
    $el.find('.bin-promotion-error').addClass('mk-hide');
    $el.find('.bin-promotion-server-error').addClass('mk-hide');
    $el.find('.checking-bin-promotion').addClass('mk-hide');

    if(type == 'removingCoupon'){
        $el.find('.removing-coupon').removeClass('mk-hide');
    }else if(type == 'showProcessing'){
        $el.find('.checking-bin-promotion').removeClass('mk-hide');
    }else if(type == 'wrongMethod'){
        $el.find('.wrong-card-entered').addClass('mk-hide');
        $el.find('.wrong-method-chosen').removeClass('mk-hide');
        $el.find('.bin-promotion-error').removeClass('mk-hide');
    }else if(type == 'wrongCard'){
        $el.find('.wrong-card-entered').removeClass('mk-hide');
        $el.find('.wrong-method-chosen').addClass('mk-hide');
        $el.find('.bin-promotion-error').removeClass('mk-hide');
    }else if(type == 'serverError'){
        $el.find('.bin-promotion-server-error').removeClass('mk-hide');
    }
    if(!$el.is(":visible")) binErrorBox.show();
}