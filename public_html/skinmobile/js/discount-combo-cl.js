Myntra.sCombo = {
    Err: function(msg){
        console.log(msg);
        return false;
    },
    cartTotal: 0,
    minMore: 0,
    isAmount: false,
    cartSavings: 0,
    discountedTotal: 0,
    quantity_in_combo: 0,
    amount_in_combo: 0,
	Test: {},
	getCollection: function(combotype){
		if(!combotype)
			combotype = 'defaultitemlist';
		switch(combotype){
			case 'defaultitemlist':return Myntra.sCombo.cSetStyles;break;
			case 'freeitem'		:return Myntra.sCombo.cFreeStyles;break;
		}
		return this.Err('no collection found '+ combotype);
	}
}

Myntra.sCombo.ComboOverlay= {};




Myntra.sCombo.addItemsToCart= function(){
    var addedToCartList = Myntra.sCombo.cSetStyles.getAllAdded();
    var freeItemAddedToCartList = Myntra.sCombo.cFreeStyles.getAllAdded();
    for( var i in freeItemAddedToCartList){
        addedToCartList.push(freeItemAddedToCartList[i]);
    }
    var addedToCartArr = [];
    if(addedToCartList.length){
        for(var uid in addedToCartList){
            var postItem = Object();
            postItem["skuid"] = addedToCartList[uid].get('skuid');
            postItem["quantity"] = addedToCartList[uid].get('quantity');
            postItem["itemAddedByDiscountEngine"] = addedToCartList[uid].itemAddedByDiscountEngine();
            if(postItem["quantity"]!=null && postItem["quantity"] != 0) addedToCartArr.push(postItem);
        }

    $.ajax({
        type: 'POST',
        url: "/mkretrievedataforcart.php?pagetype=productdetail&lead=recently_viewed",
        data:     {
            'fromComboOverlay' : 1,
            'cartPostData' : JSON.stringify(addedToCartArr),
            '_token' : $('input:hidden[name=_token]').val()
        },
        beforeSend:function(){
            Myntra.LightBox('#cart-combo-overlay').showLoading();
        },
        success: function(data){
            window.parent.location = document.location.protocol+'//'+document.location.hostname+"/mkmycart.php";
        },
        error: function(data){
            return false;
        }
    });
    } else {
        window.parent.location = document.location.protocol+'//'+document.location.hostname+"/mkmycart.php";
    }
}
Myntra.sCombo.modifyItemsInCart= function(){
    Myntra.sCombo.addItemsToCart();
    return;
    var modifiedInCartList = Myntra.sCombo.cSetStyles.getAllModified();
    var modifiedInCartArr = [];
    if(0){//modifiedInCartList.length){
        for(var uid in modifiedInCartList){
            var postItem = Object();
            postItem["skuid"] = modifiedInCartList[uid].get('skuid');
            postItem["quantity"] = modifiedInCartList[uid].get('quantity');
            postItem['cartitemid'] = Myntra.sCombo.selectedStyleProductMap[postItem["skuid"]];
            if(postItem["quantity"]!=null && postItem["quantity"] != 0) modifiedInCartArr.push(postItem);
        }
        var postItem, qtyStr, sizeStr, cartItemId,
            postData = Object();
        for(var i in modifiedInCartArr){
            postItem = modifiedInCartArr[i];
            cartItemId = postItem.cartitemid;
            qtyStr = cartItemId+'qty';
            sizeStr = cartItemId+'size';
            postData[qtyStr+''] = postItem.quantity;
            postData[sizeStr+''] = postItem.skuid;
            postData['cartitem'] = postItem.cartitemid;
            postData['fromComboOverlay'] = 1;
            postData['update'] = 'size';
            postData['_token'] = $('input:hidden[name=_token]').val();
            $.ajax({
                type: 'POST',
                url: "/modifycart.php",
                data: postData,
                beforeSend:function(){
                    Myntra.LightBox('#cart-combo-overlay').showLoading();
                },
                success: function(data){
                    Myntra.sCombo.addItemsToCart();
                },
                error: function(data){
                    return false;
                }
            });
        }
    } else {
        Myntra.sCombo.addItemsToCart();
    }
}
Myntra.sCombo.removeItemsFromCart= function(){
    var removedFromCartList = Myntra.sCombo.cSetStyles.getAllRemoved();
    var removedFromCartArr = [];
    var removingFreeItem = false;
    var freeItemAddedToCartList = Myntra.sCombo.cFreeStyles.getAllRemoved();
    for( var i in freeItemAddedToCartList){
        removedFromCartList.push(freeItemAddedToCartList[i]);
        removingFreeItem = true;
    }
    if(removedFromCartList.length){
        for(var uid in removedFromCartList){
            var postItem = Object();
            postItem["skuid"] = removedFromCartList[uid].geti('skuid');
            postItem["quantity"] = removedFromCartList[uid].geti('quantity');
            postItem['cartitemid'] = Myntra.sCombo.selectedStyleProductMap[postItem["skuid"]].itemid;
            if(postItem["quantity"]!=null && postItem["quantity"] != 0) removedFromCartArr.push(postItem);
        }
        for(var i in removedFromCartArr){
            postItem = removedFromCartArr[i];
            $.ajax({
                type: 'GET',
                url: "/modifycart.php",
                data:     {
                    'fromComboOverlay' : 1,
                    'remove'	:	'true',
                    'itemid'   :  postItem.cartitemid,
                    '_token' : $('input:hidden[name=_token]').val(),
                    'removingFreeItem' : removingFreeItem
                },
                beforeSend:function(){
                    Myntra.LightBox('#cart-combo-overlay').showLoading();
                },
                success: function(data){
                    Myntra.sCombo.modifyItemsInCart();
                },
                error: function(data){
                    alert('Something went wrong. Please refresh the page and try again later');
                    return false;
                }
            });
        }
    } else {
        Myntra.sCombo.modifyItemsInCart();
    }
}