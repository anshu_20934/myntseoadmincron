
Myntra.Search.getFilterValues = function(str){
    return str.replace(/-/g, ":").replace(/~:/g, "~-").split(":");   
}

Myntra.Search.InitNoResultsPage = function(){
    // Tie GA Calls to Clicks on display categories in no results page
    $("#no_result a, .no_result_disp_categories a").live("click", function(){
        if($(this).attr('href') != "undefined"){
            _gaq.push(['_trackEvent', 'no_results_page', 'browse', $(this).attr('href')]);
        }
    });
    
}

Myntra.Search.Filters = (function(){
    var obj={};
    var BACK_BUTTON_SELECTOR=".back-btn-cnt";
    var FILTER_LIGHTBOX_SELECTOR="#lb-filters";
    var FILTER_BUTTON_SELECTOR="#filter-btn";
    var SORT_BUTTON_SELECTOR="#sort-btn";
    var FILTER_TYPES_SELECTOR=".filter-types-wrapper";
    var FILTER_CONTENT_SELECTOR=".filter-content";
    var FILTER_CLOSE_SELECTOR="#lb-filters .close";
    obj.loadingTimerId;
    obj.filtersVisible=false;
    obj.sortFilterApplied=false;
    obj.filterClickInterval=500;
    obj.filterClickCheck=true;
    obj.init = function(){
        $(".filter-types li > span").fastClick(function(e){
        	if(obj.filterClickTimerCheck==false){
        		return false;						// disabling clicks on li links
        	}
        	obj.filterClickTimerCheck=false;
        	setTimeout(function(){
        		obj.filterClickTimerCheck=true;			// enabling click on li links
        	},obj.filterClickInterval)
            var el;
            if(e.target.nodeName == "SPAN"){
               el=$(e.target).closest("li");
            }
            else{
              el=$(e.target);
            }
           
            var filter=el.attr("data-target");
            e.preventDefault();
            e.stopPropagation();
            this.hideFilterTypes();
            this.showFilterContent(filter, el);
            return false;
        }.bind(this));

        $(FILTER_CLOSE_SELECTOR).fastClick(function(){
            obj.filtersVisible=false;
        });

        $(".mk-clear-all-filters").fastClick(function(e){
            obj.clearAllFilters(e);
        });

        $(".mk-product-sort li").fastClick(function(e){
            var el;
            if(e.target.nodeName != "LI"){
              el=$(e.target).closest("li");
            }
            else{
              el=$(e.target);
            }
            var anchorEl=el.find("a");
            Myntra.Search.Utils.setSortBy(anchorEl.attr("data-sort"), anchorEl);
            filterRightMenuObj.hide();
            e.preventDefault();
            e.stopPropagation();
            return false;
        });
/*
        var filterObj = Myntra.LightBox(FILTER_LIGHTBOX_SELECTOR, {
            "beforeHide": function(){
                obj.resetFiltersPopup()
            }, 
            "afterShow" : function(){
                var scrollObj = $("#filter-types").data("scrollObj");
                if(typeof scrollObj == "undefined"){
                    scrollObj = new iScroll("filter-types", {vScrollbar : true});
                    $("#filter-types").data("scrollObj", scrollObj);
                }
                else{
                    scrollObj.refresh();
                }
                $(".filter-types-wrapper").on('touchmove', function (e) { e.preventDefault(); });
            }
        });
*/
        var sortBtn = $(SORT_BUTTON_SELECTOR).click(function(e) {
        	sortRightMenuObj.show();
        });
        var filterBtn = $(FILTER_BUTTON_SELECTOR).fastClick(function() {
            obj.filtersVisible=true;
            filterRightMenuObj.show();
        });
        $('.btn-show-filter-results').fastClick(function(){
        	filterListAnimObj.reinit();
        	filterRightMenuObj.hide();
        });
        $('.btn-show-sort-results').fastClick(function(){
        	sortRightMenuObj.hide();
        });
        //$(window).trigger("resize");
    };    

    obj.updateUI = function(filter){
        // unselect all filters
        $(".mk-"+filter+"-filters .mk-labelx_check").removeClass("checked").addClass("unchecked");

        // update UI selection for filters in configArray
        for(var i=0;i<configArray[filter].length; i++){
            var selector=configArray[filter][i].toLowerCase().replace(/\W+/g, "") + "-"+filter+"-filter";
            $("."+selector).removeClass("unchecked").addClass("checked");
        }

        //update the selected filters text
        $("[data-target="+filter+"-types] .selected").html(Myntra.Utils.truncateStr(configArray[filter].join(", "))); 
    };

    obj.updateClearFiltersUI = function(filter, resetCondition){
        var clearFilterEl=$("."+filter+"-types").find(".mk-clear-filter"),
            filterHeaderEl=$("."+filter+"-types").find(".filter-header"),
            clearAllFilterEl=$(".mk-clear-all-filters"),
            clearAllFilterHeaderEl=$(".filter-types-wrapper .filter-header"); 
            
        if(resetCondition){
            clearFilterEl.removeClass("mk-hide");
            filterHeaderEl.show();
        }
        else{
            clearFilterEl.addClass("mk-hide");
            filterHeaderEl.hide();
        }

        if(Myntra.Search.Utils.getNoOfFiltersSet()){
            filtersIconEl.addClass("selected");
            clearAllFilterEl.removeClass("mk-hide");
            clearAllFilterHeaderEl.show();
        }
        else{
            filtersIconEl.removeClass("selected");
            clearAllFilterEl.addClass("mk-hide");
            clearAllFilterHeaderEl.hide();
        }
    };

    obj.resetFiltersPopup = function(){
        this.showFilterTypes();
        this.hideFilterContent();
    };

    obj.initNewFilter = function(el){
        $(el).fastClick(function(e){
           var el;
            if(e.target.nodeName == "SPAN"){
                el=$(e.target).closest("li");
            }
            else{
                el=$(e.target);
            }
            var filter=el.attr("data-target");    
            this.hideFilterTypes();
            this.showFilterContent(filter, $(e.currentTarget));
        }.bind(this));
    }

    obj.addNewFilterType = function(filterTargetName){
        $(".filter-types li[data-target='"+filterTargetName+"']").fastClick(function(e){
            var el;
            if(e.target.nodeName == "SPAN"){
               el=$(e.target).closest("li");
               filterListAnimObj.animateLeft(el);
            }
            else{
              el=$(e.target);
            }
            var filter=el.attr("data-target");
            obj.hideFilterTypes();
            obj.showFilterContent(filter, el);
            e.preventDefault();
            e.stopPropagation();
            return false;
        });
    };

    obj.hideBackButton = function(){
        $(BACK_BUTTON_SELECTOR).hide();
    }

    obj.showBackButton = function(){
        $(BACK_BUTTON_SELECTOR).show();
    };

    obj.showFilterTypes = function(){
        $(FILTER_TYPES_SELECTOR).show();
        $('.types-header').show();
    };

    obj.hideFilterTypes = function(){
       // $(FILTER_TYPES_SELECTOR).hide();
        $('.types-header').hide();
    };

    obj.showFilterContent = function(filter, el){
        var id="#"+filter;
        //Check for brands filter only
        if(id === "#brands-types"){
            $(".mk-brands-filter-input").show();    
        }
        $(id).show();
        if(!$(el).is('li'))
        	{
        	el=$(el).closest('li');
        	}
        that=this;
        setTimeout(function(){
        	that.updateFilterTypeNameInHeader($(el).attr("data-name"));
        },500)
        
        this.removeHrefAttr(filter);
        this.bindFilterClickHandlers(filter);
        this.bindClearFilterHandler(filter);
    };

    obj.removeHrefAttr = function(filter){
        if($("#"+filter+"-wrapper a").length){
            $("#"+filter+"-wrapper a").removeAttr("href");
        }
    }

    obj.clearAllFilters = function(e){
        var el=e.currentTarget;
        Myntra.Search.Data.filterClickedName = "none";
        Myntra.Search.Data.userTriggered=true;
        $(".mk-clear-brands-input").click();
        $(".brands-autocomplete-input").blur();
        Myntra.Search.Utils.setPageToFirst();
        Myntra.Search.Utils.setFilterOptionsInUrlHash([]);
        for(prop in filterProperties){
            filterProperties[prop].setToDefault();
            filterProperties[prop].updateClearFilterUI();
        }
        e.stopPropagation();
        e.preventDefault();
        _gaq.push(['_trackEvent', 'search_page_filters', 'clear_all_filters']);
    };

    obj.bindClearFilterHandler = function(filter){
        if(typeof $("#"+filter+"-wrapper").attr("data-clearhandler") == "undefined"){
            $("#"+filter + " .mk-clear-filter").fastClick(function(e){
                obj.clearFilter(e);
            });
            $("#"+filter + " .mk-loader").fastClick(function(e){
                Myntra.Search.InfiniteScroll.dismissPopup();
            });
            $("#"+filter+"-wrapper").attr("data-clearhandler","true");
        }
    };

    obj.clearFilter = function(e){
        var el=e.currentTarget;
        Myntra.Search.Data.filterClickedName = "none";
        Myntra.Search.Data.userTriggered=true;
        var key=$(el).attr("data-key");
        if (key == 'brands') {
            $(".mk-clear-brands-input").click();
            $(".brands-autocomplete-input").blur();

        }
        filterProperties[key].setToDefault();
        filterProperties[key].updateClearFilterUI();
        Myntra.Search.Utils.setPageToFirst();
        Myntra.Search.Utils.setFilterOptionsInUrlHash([key].concat(dynamicFilters));

        for(var m=0;m<dynamicFilters.length;m++){
            filterProperties[dynamicFilters[m]].setToDefault();
            filterProperties[dynamicFilters[m]].updateClearFilterUI();
        }
        filterRightMenuObj.recalculateObjHeight();
        _gaq.push(['_trackEvent', 'search_page_filters', 'clear_filter',key]);
        e.stopPropagation();
        e.preventDefault();     
        return false;
    };

    obj.bindFilterClickHandlers = function(filter){
        if(typeof $("#"+filter+"-wrapper").attr("data-clickhandlers") == "undefined"){
            if(filter == "discountpercentage-types"){
                $("#"+filter+"-wrapper" + " .mk-labelx_check").fastClick(function(e){
                    obj.discountFilterClick(e);
                });
            }
            else if(filter == "pricerange-types"){
                $("#"+filter+"-wrapper" + " .mk-labelx_check").fastClick(function(e){
                    obj.priceFilterClick(e);
                });
            }
            else{ 
                $("#"+filter+"-wrapper" + " .mk-labelx_check").fastClick(function(e){
                    obj.genericFilterClick(e);
                });
            }
            $("#"+filter+"-wrapper").attr("data-clickhandlers", "true");
        }
    };

    obj.genericFilterClick = function(e){
        var el=e.currentTarget;

        if(!$(el).hasClass("disabled")){
            var key=$(el).attr("data-key");
            var value=$(el).attr("data-value").replace(/\//g,"~/").replace(/-/g,"~-");
            if($(el).hasClass("unchecked")){
                $(el).removeClass("unchecked").addClass("checked");
                if($.inArray(value,configArray[key]) == -1){
                    configArray[key].push(value);
                }
            }
            else{
                $(el).removeClass("checked").addClass("unchecked");
                if($.inArray(value,configArray[key]) != -1){                    
                    configArray[key] = Myntra.Search.Utils.removeValueFromArray(value, configArray[key]);
                }
            }
            filterProperties[key].updateClearFilterUI();
            filterProperties[key].setFilterClickedName();
            Myntra.Search.Utils.setPageToFirst();
            Myntra.Search.Utils.setFilterOptionsInUrlHash([key]);
            _gaq.push(['_trackEvent', 'search_page_filters', key, value]);
            Myntra.Search.Data.userTriggered=true;
        }
        e.preventDefault();
        e.stopPropagation();
        filterRightMenuObj.recalculateObjHeight();
        return false;
    };

    obj.priceFilterClick = function(e){
        var el=e.currentTarget;
        var key=$(el).attr("data-key");
        var minVal=$(el).attr("data-rangeMin");
        var maxVal=$(el).attr("data-rangeMax");
        if($(el).hasClass("checked")){
            $(el).removeClass("checked").addClass("unchecked");
            Myntra.Search.Utils.priceRange(Myntra.Search.Data.priceRangeMin, Myntra.Search.Data.priceRangeMax);
            filterProperties[key].updateClearFilterUI();
        }
        else{
            $(".mk-labelx_check.price-range-filter").removeClass("checked").addClass("unchecked");
            $(el).removeClass("unchecked").addClass("checked");
            Myntra.Search.Utils.priceRange(minVal, maxVal);
            filterProperties[key].updateClearFilterUI();
        }
        filterProperties[key].setFilterClickedName();
        _gaq.push(['_trackEvent', 'search_page_filters', key, 'click']);
        Myntra.Search.Data.userTriggered=true;
    };

    obj.discountFilterClick = function(e){
        var el=e.currentTarget;
        var key=$(el).attr("data-key");
        var value=$(el).attr("data-value");
        if($(el).hasClass("unchecked")){
            $(el).removeClass("unchecked").addClass("checked");
            configArray[key]=value;
        }
        else{
            $(el).removeClass("checked").addClass("unchecked");
            configArray[key]="";
        }
        filterProperties[key].updateClearFilterUI();
        filterProperties[key].setFilterClickedName();
        Myntra.Search.Utils.setPageToFirst();
        Myntra.Search.Utils.setFilterOptionsInUrlHash([key]);
        _gaq.push(['_trackEvent', 'search_page_filters', key, 'click']);
        Myntra.Search.Data.userTriggered=true;
    };

    obj.updateFilterTypeNameInHeader = function(name){
        if(name !== ""){
            $(".filter-type-name").html(" / " + name);
        }
    };

    obj.removeFilterTypeNameInHeader = function(){
        $(".filter-type-name").html("");
    }

    obj.hideFilterContent = function(){
        //$(FILTER_CONTENT_SELECTOR).hide();
        $(".mk-brands-filter-input").hide();
        this.removeFilterTypeNameInHeader();
    };
    var sortListAnimObj = Myntra.ListAnimation($('.content-slide-wrapper.sort-wrapper'),{'width':'88'});
    var sortRightMenuObj = Myntra.RightMenu($('.content-slide-wrapper.sort-wrapper'),{'width':'88'},sortListAnimObj);
    var filterListAnimObj = Myntra.ListAnimation($('.content-slide-wrapper.filter-wrapper'),{
        "beforeBack": function(e){
            obj.resetFiltersPopup();
            var bai = $(".brands-autocomplete-input");
            var cbi = bai.siblings(".mk-clear-brands-input");
            bai.val("");
            cbi.hide();
            cbi.trigger('click');
            bai.blur();
        },
        'listOptimization':true,
    	'width':'88'
    		});
    var filterRightMenuObj = Myntra.RightMenu($('.content-slide-wrapper.filter-wrapper'), {
        "beforeHide": function(){
            obj.resetFiltersPopup();
        },
        isWidthAspect:false,
        width:'88'
    },filterListAnimObj);
    return obj;
})();

Myntra.Search.InfiniteScroll = (function(){
    var obj = {};
    
    var prevScrollPos=0,
        curScrollPos=0,
        infScrollCallInProgress=false,
        calltype="searchpagefilters",
        currentPage,
        searchContainerOffsetTop,
        pageHeight = 1000,
        /* This keeps track of pages that have been loaded till now. */
        pagesLoadedArr = [],
        /* This keeps track of pages that have been loaded till now but hidden. */
        pagesLoadedButHidden = [],
        /* This store the html for each page against the page number. */
        pagesHtml = new Object(),
        scrollTimerID = null,
        hashStr,
        hashCleanStr,
        keyValue;
    
    obj.pageSetToFirst=true;
    obj.pageSetFromHash=false;
    obj.defaultTopPosition=0;

    var productListingUlElem=$('.search-list-container'),
        searchPageContentsElem=$("#mk-search-results"),
        moreProductsElem=$(".mk-more-products-link"),
        moreProductsAjaxLoaderElem=$(".more-products-loading-indicator"),        
        productsCountElem=$(".mk-product-count"),
        productsLeftCountElem=$(".products-left-count"),
        moreProductsSectionElem=$(".mk-infscroll-loader");

    var scrollFactor;
    
    obj.init = function(){

        var self = obj;
        /* hashstr incase the page url contains filters already set or if it is a pdp back button request */
        hashStr=window.location.hash;
        hashCleanStr=(hashStr).substring((hashStr).indexOf("#!")+2, (hashStr).length);
        keyValue=hashCleanStr.split("=");
        currentPage=parseInt(keyValue[1]);

        this.setBasicPageProperties();
        this.computePageHeight();
        this.attachPageInViewTrigger();
        this.attachPDPRedirectCalls();        
        //this.triggerLazyLoad();
        searchContainerOffsetTop=$(".search-list-container").offset().top;
        
        /* window.scroll event triggers too many events in a short span. Fix by setting a timer and letting the scroll event once in 330 ms*/
        $(window).bind("scrollstart scrollstop",function(e) {
            //obj.processScroll();
            if(scrollTimerID){
                    clearTimeout(scrollTimerID);
                    scrollTimerID=null;
            }
            scrollTimerID=setTimeout(function(){obj.processScroll();}, 330);
            return false;
        });

        var prevOrientation,curOrientation;

        //Detect whether device supports orientationchange event, otherwise fall back to the resize event.
        var supportsOrientationChange = "onorientationchange" in window,
        orientationEvent = supportsOrientationChange ? "orientationchange" : "resize";

        $(window).bind(orientationEvent, function(e) {
            scrollFactor=curScrollPos/pageHeight;
            //alert("scrollFactor//" + scrollFactor + "//curScrollPos//" + curScrollPos + "//pageHeight//" + pageHeight + "//" + e.type);
            setTimeout(function(){
                //alert("containerWidht//" + $(".mk-prod-list").width() + "//outerWidth//" + $(".mk-prod-list").outerWidth());    
                //iOS Safari is throwing orientation change event even when i scroll which leads to needless processing and computation.
                //So check if the orientation has really changed #facepalm
                if (window.orientation & 2) {
                    curOrientation = "landscape";
                } else {
                    curOrientation = "portrait";
                }
                if(curOrientation != prevOrientation){
                    obj.onOrientationChange(scrollFactor);
                }
                prevOrientation=curOrientation;
            },500);
            return false;
        });
    };

    obj.onOrientationChange = function(scrollFactor){
        obj.setHeightForPages(obj.computePageHeight());
        obj.resetScrollPosition(scrollFactor);        
    };

    obj.resetScrollPosition = function(scrollFactor){
        $('html, body').animate({scrollTop:pageHeight*scrollFactor}, 100, function(){});
    };

    obj.attachPageInViewTrigger = function(){
        $(".m-search-list").each(function(){
            $(this).unbind('inview');
            $(this).bind('inview', function(event, isInView) {
              if (isInView) {
                  // element is in viewport set it as the current page
                  currentPage=parseInt($(this).attr("data-page"));
              } else {
                // element has gone out of viewport
              }
            });
        });
    };

    obj.setHeightForPages = function(height){
        $(".m-search-list").height(height + "px");
    };

    /* By default the search php loads the first page. every other subsequent page is loaded by the ajax search php. */
    /* Here take that into account and push the first page into pagesLoadedArr. Also store the html is pagesHtml for future reference */
    obj.setBasicPageProperties = function(){
        pagesLoadedArr.push(Myntra.Search.Data.currentPage);
        pagesHtml[Myntra.Search.Data.currentPage]=$(".m-search-list[data-page='"+Myntra.Search.Data.currentPage+"']").html();
    };

    /* To keep track of the height of a single page. This pageHeight and window.scrollTop is used to keep track which page the user is currently viewing */
    obj.computePageHeight = function(){
        //pageHeight=$(".m-search-list ul").first().height();
        var containerWidth=$(".mk-prod-list").width();
        var perItemWidth=150;
        var perItemHeight=250;
        var itemsPerRow=Math.ceil(containerWidth/perItemWidth);
        var noOfRows=Myntra.Search.Data.defaultNoOfItems/itemsPerRow;
        pageHeight=perItemHeight*noOfRows;
        return pageHeight;
    };

    obj.processScroll = function() {
            curScrollPos=$(window).scrollTop();
            var _curScrollPos=parseInt(curScrollPos);
            var _prevScrollPos=parseInt(prevScrollPos);
            var direction="down";
            if(_curScrollPos < _prevScrollPos){
                direction="up";
            }
            if(curScrollPos!=prevScrollPos){
                if(currentPage > Myntra.Search.Data.totalPages){
                    currentPage = Myntra.Search.Data.totalPages;
                }
                var mainslideobj=$('.main-slide-wrapper');
                if(!mainslideobj.hasClass('slidein-anim') && !mainslideobj.hasClass('slidein-noanim'))
                	{
                	obj.checkToLoadNextPage(obj.getCurrentPagePosition(), direction);
                	}
            }
            prevScrollPos=curScrollPos;
    };
    
    obj.checkToLoadNextPage = function(cpage, dir){
        if(cpage >= 0 && cpage <= Myntra.Search.Data.totalPages){
            var pagesToLoad=[];
            pagesToLoad.push(cpage-3,cpage-2,cpage-1,cpage,cpage+1,cpage+2,cpage+3);
            /*if(dir == "down"){
                pagesToLoad.push(cpage,cpage+1,cpage+2,cpage+3);    
            }
            else{
                pagesToLoad.push(cpage-2,cpage-1,cpage,cpage+1);
            }*/
            obj.loadToBeVisiblePages(pagesToLoad);
            var pagesToHide=[];
            pagesToHide=pagesLoadedArr.diff(pagesToLoad);
            if(pagesToHide.length){
                pagesToHide=pagesToHide.diff(pagesLoadedButHidden);
                obj.replaceOutOfFoldPagesWithDummy(pagesLoadedArr.diff(pagesToLoad));
            }
        }
    };

    obj.loadToBeVisiblePages = function(pages){
            var pagesLength=pages.length;
            for(i=0;i<pagesLength;i++){
                    var pageNum=pages[i];
                    if(pageNum >= 0 && pageNum <= Myntra.Search.Data.totalPages){//|| typeof pagesHtml[pageNum] == "undefined"
                            if($.inArray(pageNum,pagesLoadedArr) == -1){
                                obj.ajaxSearchCall(pageNum);
                            }
                            else{
                                if($(".m-search-list[data-page='"+pageNum+"']").html() == ""){
                                    $(".m-search-list[data-page='"+pageNum+"']").html($(pagesHtml[pageNum]));
                                    pagesLoadedButHidden.remove(pageNum);
                                }
                            }
                            $(".m-search-list[data-page='"+pageNum+"']").attr("data-role", "exist");
                    }
            }
            //this.triggerLazyLoad();        
    };

    obj.replaceOutOfFoldPagesWithDummy = function(pages){
            var pagesLength=pages.length;
            for(i=0;i<pagesLength;i++){
                    var pageNum=pages[i];
                    if(pageNum >= 0 && pageNum <= Myntra.Search.Data.totalPages){
                            if($.inArray(pageNum,pagesLoadedArr) != -1 && $(".m-search-list[data-page='"+pageNum+"']").attr("data-role") != "dummy"){                                    
                                    $(".m-search-list[data-page='"+pageNum+"']").attr("data-role","dummy");
                                    $(".m-search-list[data-page='"+pageNum+"']").height(pageHeight + "px");
                                    $(".m-search-list[data-page='"+pageNum+"']").html("");
                                    pagesLoadedButHidden.push(pageNum);
                            }
                    }
            }        
    };

    obj.ajaxSearchCall = function(page){
        configArray["page"] = page;
        page_to_be_loaded = page;
        if(!this.isNextPageOutOfLimit()){
            infScrollCallInProgress=true;
            this.pageSetToFirst=false;
            var querystr=JSON.stringify(configArray);
            this.showLoadingGraphic();
            this.addPlaceholderDiv(page);
            _gaq.push(['_trackEvent', 'search_page', 'showmoreproducts','']);
            this.getData(calltype, http_loc+"/myntra/ajax_search.php?nav_id="+Myntra.Search.Data.navId+"&showadditionaldetail="+Myntra.Search.Data.showAdditionalProductDetails, querystr);
        }
        //this.resetPagesHtml();
        return false;
    };

    obj.addPlaceholderDiv = function(page){
        if(!$(".m-search-list[data-page='"+page+"']").length){
            productListingUlElem.append('<div class="m-search-list" data-role="exist" data-page="'+page+'"></div>');
        }
    }

    obj.getData = function(type, url, querystr)
    {
        $.ajax ({
            url: url,
            data: {"query" : querystr},
            cache: true,
            success: function(data)
            {
                obj.updateSearchPageUI(data);
            }
        });
    }
    
    obj.isNextPageOutOfLimit = function(){
        return (configArray["page"] + 1 > Myntra.Search.Data.totalPages)?true:false;
    };

    obj.getPagesHtml = function(){
        return pagesHtml;
    };

    obj.getProductsVisible = function(){
        return productsVisible;
    };

    obj.getCurrentPagePosition = function(){
        var pg=Math.floor(($(window).scrollTop()-searchContainerOffsetTop)/pageHeight)
        return (pg<0)?0:pg;
    };

    obj.getSearchContainerOffsetTop = function(){
        return searchContainerOffsetTop;
    };

    obj.getPageHeight = function(){
        return pageHeight;
    };

    obj.postProcessData = function(result){
        var pageNum=result.current_page;
        if($.inArray(pageNum,pagesLoadedArr) == -1){
            pagesLoadedArr.push(pageNum);
            pagesHtml[pageNum]=result.html;
            $(productListingUlElem).find(".m-search-list[data-page='"+pageNum+"']").html($(result.html));
        }
        else if(pageNum == 0){
            pagesLoadedArr.push(pageNum);
            pagesHtml[pageNum]=result.html;
            $(productListingUlElem).find(".m-search-list[data-page='"+pageNum+"']").html($(result.html));
        }
    };

    obj.updateSearchPageUI = function(data, params){

        var result=$.parseJSON(data);

        this.postProcessData(result, params);

        this.attachPageInViewTrigger();

        this.updateLeftNavFilter(result.filters);

        this.updateProductCount(result);

        this.setPageCounters(result);
        
        this.updateNumberOfProductsCount(result);
        
        this.hideLoadingGraphic(result);

        $('.mk-search-contents .mk-sub-text').remove();
        if(result.query_level == 'FULL'){
            var additional_text_div = '<div class="mk-sub-text mk-help-text">NO MATCHES FOUND / SHOWING RESULTS FOR PARTIAL MATCHES INSTEAD</div>';
            $('.mk-search-contents .mk-product-count').before(additional_text_div);
        }    

        this.pageSetFromHash=false;
        
        this.showProductListing();
        
        if(filterClicked){
            if($(window).scrollTop() > this.defaultTopPosition){
                Myntra.Search.Utils.scrollToTop(this.defaultTopPosition);
            }
            filterClicked=false;
        }
        
        //this.triggerLazyLoad();

        if(Myntra.Search.Filters.sortFilterApplied){
            this.dismissPopup();
            Myntra.Search.Filters.sortFilterApplied=false;
        }

        this.attachPDPRedirectCalls();        
        
        infScrollCallInProgress=false;
    
        Myntra.Search.Data.onPageLoad=false;

        if(result.current_page == scrollConfigArray["scrollPage"]){
            setTimeout(function(){$(document).trigger("myntra.search.backbutton.load.done");}, 1000);
        }
        
        return false;
    };

    obj.dismissPopup = function(){
        $("#lb-sort .close").trigger("click");
        $("#lb-filters .close").trigger("click");
    };

    obj.resetPagesHtml = function(){
        for(pageNum in pagesHtml){
            var htmlStr=$(".m-search-list[data-page='"+pageNum+"']").html();
            if(htmlStr != ""){
                pagesHtml[pageNum]=$(".m-search-list[data-page='"+pageNum+"']").html();
            } 
        }
    };

    obj.updateLeftNavFilter = function(filters){
        this.updateFilterProperties(filters);    
        for(prop in filterProperties){
            filterProperties[prop].updateFilterVisibility(filters);
        }
        var dataKey="";
        $(".mk-labelx_check.checked").each(function(index,element){
            if(dataKey == "" ){
                dataKey = $(element).attr("data-key"); 
            }
            else if(dataKey != $(element).attr('data-key')){
                dataKey="";
                return false;
            }
        });
        if(dataKey !=""){
            $(".mk-"+dataKey+"-filters"+" .mk-labelx_check").each(function(index,element){
               $(element).removeClass('disabled').find(".mk-filter-product-count").show(); 
            });
        };
        Myntra.Search.Data.userTriggered=false;
    };

    obj.updateFilterProperties = function(filters){
        var has_article_attr = false;
        for(attr in filters.article_type_attributes){
            has_article_attr = true;
            break;
        }
        if(!has_article_attr){
            $(".article_type_attr").remove();
            $(".article_type_attr_placeholder").remove();
            for(prop in filterProperties){
                if(prop.indexOf("_article_attr") != -1){
                    if($.inArray(prop,dynamicFilters) != -1){
                        Myntra.Search.Utils.removeValueFromArray(prop,dynamicFilters);
                    }    
                    delete filterProperties[prop];
                }
            }
        }else if($(".article_type_attr").length == 0){
            for(filter_tmp in filters.article_type_attributes){
                var article_type_attr_value = filter_tmp.replace(/ /g,"_"); 
                if($.inArray(article_type_attr_value,dynamicFilters) == -1){
                    dynamicFilters.push(article_type_attr_value);
                }
                (function(article_type_attr_value) {
                configArray[article_type_attr_value]=[];
                filterProperties[article_type_attr_value]={
                    getURLParam: function(){
                        var ataStr=configArray[article_type_attr_value].join(":");
                        return (ataStr != "")?article_type_attr_value+"="+unescape(ataStr):"";
                    },
                    processURL: function(str){
                        configArray[article_type_attr_value]=Myntra.Search.getFilterValues(unescape(str));
                        return true;
                    },
                    updateUI: function(){
                        // unselect all brands
                        $(".article_type_attr .mk-labelx_check").removeClass("checked").addClass("unchecked");

                        // update UI selection for brands in configArray
                        for(var attr=0;attr<configArray[article_type_attr_value].length;attr++){
                            var selector=configArray[article_type_attr_value][attr].toLowerCase().replace(/\W+/g, "") + "-ata-filter";
                            $("."+selector).removeClass("unchecked").addClass("checked");
                        }
                        
                        $("[data-name='"+article_type_attr_value+"'] .selected").html(Myntra.Utils.truncateStr(configArray[article_type_attr_value].join(", ")));

                        //Myntra.Search.Filters.updateUI(article_type_attr_value);
                        return true;
                    },
                    setToDefault: function(){
                        configArray[article_type_attr_value]=[];
                        return true;
                    },
                    resetParams: function(){
                        configArray["page"]=0;
                        return true;
                    },
                    updateFilterVisibility: function(filters){
                        if((Myntra.Search.Data.filterClickedName != "" && Myntra.Search.Data.filterClickedName!=article_type_attr_value) || (Myntra.Search.Data.filterClickedName != "" && Myntra.Search.Data.filterClickedName==article_type_attr_value && configArray[article_type_attr_value].length == 0)){
                            $(".mk-"+article_type_attr_value+"-filters .mk-labelx_check").each(function (index,element){
                                if((typeof filters.article_type_attributes[article_type_attr_value] == 'undefined') || (typeof filters.article_type_attributes[article_type_attr_value].values[$(element).attr('data-value')] == 'undefined')){
                                    $(element).addClass('disabled');
                                }else{
                                    $(element).removeClass("disabled");
                                }
                            });
                        }
                    },
                    setFilterClickedName: function(){
                        Myntra.Search.Data.filterClickedName = article_type_attr_value;
                    },
                    updateClearFilterUI: function(){
                        var resetCondition = (configArray[article_type_attr_value].length >= 1);
                        Myntra.Search.Filters.updateClearFiltersUI(article_type_attr_value, resetCondition);
                    }
                };
                }(article_type_attr_value));
                // Append new li for this article type
                var props = filters.article_type_attributes[article_type_attr_value];
                var article_specific_attr_html = ''+
                '<div id="'+props.title.replace(/ /g,'_')+'-types" class="'+props.title.replace(/ /g,'_')+'-types filter-content link-details article_type_attr" style="display:none;" data-scrollinit="false"><div class="filter-header"><span class="mk-loader mk-f-left"></span><span data-key="'+props.title.replace(/ /g,"_")+'_article_attr" class="mk-clear-filter mk-f-right mk-hide">Reset</span></div><div id="'+props.title.replace(/ /g,'_')+'-types-wrapper" class="filter-content-wrapper">'+
                    '<div class="mk-filter-content">'+
                    '<div class="mk-filter-wrapper mk-'+props.title.replace(/ /g,'_')+'_article_attr-filters mk-resizable-box">';
                    for(prop in props.values){
                        if(prop.toLowerCase() != 'na'){
                        article_specific_attr_html += 
                        '<label class="mk-labelx_check unchecked '+prop.toLowerCase().replace(/ /g,"-").replace(/\W+/g,'')+'-ata-filter" data-key="'+props.title.replace(/ /g,"_")+'_article_attr" data-value="'+prop+'"><span class="cbx"></span>'+prop.toLowerCase().toUpperCase()+'</label>';
                        }
                    }
                     article_specific_attr_html += 
                    '</div>'+
                    '</div>'+
                '</div></div>';    
                //$(".search-filters").append(article_specific_attr_html);

                var article_type_attr_placeholder_html = ''+'<li data-name="'+ props.title +'" class="article_type_attr_placeholder" data-target="'+props.title.replace(/ /g,'_')+'-types"><span class="anim-link prompt">'+props.title+'</span><span class="mk-level1-arrownav"></span><span class="selected"></span>'+article_specific_attr_html+'</li>';
                var newFilterEl = $(".filter-types").append(article_type_attr_placeholder_html);
                newFilterEl = $(".filter-types li").last();
                Myntra.Search.Filters.addNewFilterType(props.title.replace(/ /g,'_')+'-types');

                Myntra.Search.Filters.initNewFilter(newFilterEl);
                
                filterProperties[article_type_attr_value].updateUI();
            }
        }
    };
    
    obj.showLoadingGraphic = function(){
        moreProductsElem.hide();
        moreProductsAjaxLoaderElem.show();
        if(Myntra.Search.Filters.filtersVisible){
            $("." + Myntra.Search.Data.filterClickedName + "-types .filter-header .mk-loader").fadeIn().loading({
                "speed": 400,
                "maxDots": 3,
                "word": "Loading"
            });
            $(".filter-types-wrapper .filter-header .mk-loader").fadeIn().loading({
                "speed": 400,
                "maxDots": 3,
                "word": "Loading"
            });

        }
    };

    obj.hideLoadingGraphic = function(result){
        var productCount=parseInt(result.product_count);
        moreProductsElem.show();
        moreProductsAjaxLoaderElem.hide();
        if(Myntra.Search.Filters.filtersVisible){
            $("." + Myntra.Search.Data.filterClickedName + "-types .filter-header .mk-loader").loading("stop");
            $(".filter-types-wrapper .filter-header .mk-loader").loading("stop");
            $(".filter-types-wrapper .types-header .mk-loader").loading("stop");

            if(!isNaN(productCount) && productCount > 0){
                $("." + Myntra.Search.Data.filterClickedName + "-types .filter-header .mk-loader").text(productCount + " Found");
                $(".filter-types-wrapper .filter-header .mk-loader").text(productCount + " Found");
                $(".filter-types-wrapper .types-header .mk-loader").text(productCount + " Found");
            }
            else{
                $("." + Myntra.Search.Data.filterClickedName + "-types .filter-header .mk-loader").text("");
                $(".filter-types-wrapper .filter-header .mk-loader").text("");
                $(".filter-types-wrapper .types-header .mk-loader").text("");
            }            
        }
    };
    
    obj.showProductListing = function(){
        searchPageContentsElem.css("visibility","visible");
    };
    
    obj.hideProductListing = function(){
        searchPageContentsElem.css("visibility","hidden");
    };
    
    obj.updateProductCount = function(result){
        if(result.product_count != null){
            productsCountElem.html(result.product_count + ((result.product_count == 1)?" Product":" Products"));
        }    
        else{
            productsCountElem.html("No Products");
        }
    };
    
    obj.setPageCounters = function(result){
        Myntra.Search.Data.currentPage=parseInt(result.current_page);
        Myntra.Search.Data.totalPages=parseInt(result.page_count);
    };
    
    obj.updateNumberOfProductsCount = function(result){
        var total_products=parseInt(result.product_count);
        products_left_count = total_products - ((Myntra.Search.Data.currentPage+1)*Myntra.Search.Data.defaultNoOfItems);
        if(products_left_count > 0){
            moreProductsSectionElem.show();
            productsLeftCountElem.html(products_left_count);
        }
        else{
            moreProductsSectionElem.hide();
        }
    };

    obj.preparePageForFilters = function(){
        pagesLoadedArr=[];
        pagesHtml = new Object();
        productListingUlElem.html("");
    };

    obj.preparePageForBackButtonLoad = function(){
        var self = obj;
        obj.computePageHeight();
        obj.loadDummyPages(scrollConfigArray["scrollPage"]);
        obj.checkToLoadNextPage(scrollConfigArray["scrollPage"], "down");
        $(document).bind("myntra.search.backbutton.load.done", function(){
            $(document).unbind("myntra.search.backbutton.load.done");
            setTimeout(function(thisObj){thisObj.resetScrollPositionOnBackButton(scrollConfigArray["scrollPos"]);}, 250, self);
        });
    };

    obj.resetScrollPositionOnBackButton = function(scrollPos){
        var self = obj;
        var finalScrollPosition = (pageHeight*scrollPos)+searchContainerOffsetTop;
        var documentHeight = $(document).height();
        if(documentHeight > finalScrollPosition){
            $('html, body').animate({scrollTop:finalScrollPosition}, 100, function(){});
        }
        else{
            setTimeout(function(thisObj){thisObj.resetScrollPositionOnBackButton(scrollConfigArray["scrollPos"]);}, 250, self);
        }
    };

    obj.loadDummyPages = function(page){
        var htmlStr="";
        for(i=0;i<=page;i++){
            htmlStr+="<div class='m-search-list' data-role='dummy' style='height:"+pageHeight+"px' data-page='"+i+"'></div>";
        }
        this.preparePageForFilters();
        productListingUlElem.append(htmlStr);
    };

    obj.triggerLazyLoad = function(){
        $(".jquery-lazyload").lazyload({ 
            effect : "fadeIn",
            placeholder : cdn_base+"/skin2/images/loader_180x240.gif"
        });
        $(".jquery-lazyload").removeClass("jquery-lazyload");
    };
    
    obj.attachPDPRedirectCalls = function(){
        $(".mk-product").unbind("click");
        $(".mk-product").bind("click", function(e){
              Myntra.Search.Data.filterClicked = true;
              Myntra.Search.Data.onBeforeUnloadFired = true;
              Myntra.Search.Utils.setScrollOptionsInUrlHash(["scrollPage", "scrollPos"], e.target);          
              var idx = $(".mk-product").index($(this)),
                  position = idx+1,
                  pdpUrl = $(this).find("a").attr("href"),
                  queryString = "searchQuery=" + configArray["url"] + "&serp=" + position+"&uq="+configArray["uq"];
              _gaq.push(['_trackEvent', 'pdp_click', Myntra.Data.pageName, 'search_results']);
              if(pdpUrl.indexOf("?") != -1){
                  window.location = pdpUrl + "&" + queryString;
              }
              else{
                  window.location = pdpUrl + "?" + queryString;
              }
              return false;
        });
    };

    return obj;
})();

var viewPortWidth;
var viewPortHeight;

Myntra.Search.InitSearchPage = function(){

    Myntra.Search.InfiniteScroll.init();

    Myntra.Search.Filters.init();
/*
    if($( ".filter-content" ).length){
        // We have enabled lazy loading of filter elements on the search page
        // Since they will be loaded after onload we will have to refresh the 
        // jsScrollPane if the height has increased
        $(".filter-content").each(function(){
            var that = this;
            var lazyHandler = function(){
                $(that).unbind("lazy.change");
                //Let all updates happen so wait for 50 msecs
                setTimeout(
                    function(){
                        $(that).bind("lazy.change", lazyHandler);
                    }, 50);
            }
            $(this).bind("lazy.change", lazyHandler);
        });
    }
*/
    if($( "#mk-brands" ).length){
        var brandsIndex=[];
        var b=0;
        $("#mk-brands .mk-labelx_check").each(function(){
            brandsIndex.push({
                label:$(this).attr("data-value"),
                show:0
            });
        });
        var a=$.ui.autocomplete.prototype._renderItem;
        
        $(".brands-autocomplete-input").autocomplete({
            source:brandsIndex,
            type:"brandsbox",
            appendTo:"#mk-brand-search-results",
            search:function(){
                hideAllBrands();
            }
        });

        $("#mk-brands").bind("lazy.change", function(){
            $("#mk-brands").unbind("lazy.change");
            var that = this;
            setTimeout(function(){
                var brandsIndex=[];
                $("#mk-brands .mk-labelx_check").each(function(){
                    brandsIndex.push({
                        label:$(this).attr("data-value"),
                        show:0
                    });
                });

                $(".brands-autocomplete-input").autocomplete({
                    source:brandsIndex,
                    type:"brandsbox",
                    appendTo:"#mk-brand-search-results",
                    search:function(){
                        hideAllBrands();
                    }
                })
                $("#mk-brands").bind("lazy.change", that);
            }, 50);
        });
        
        function showAllBrands(){
            $("#mk-brands .mk-labelx_check").show();
        }
        
        function hideAllBrands(){
            $("#mk-brands .mk-labelx_check").hide();
        }
        
        $(".mk-clear-brands-input").hide();
        $(".brands-autocomplete-input").live("click keyup", function() {
            var bai = $(".brands-autocomplete-input");
            var cbi = bai.siblings(".mk-clear-brands-input");
            if(bai.val() == "" || bai.val() == bai.attr("data-placeholder")) {
                showAllBrands();
                cbi.hide();
            } else { 
                cbi.show();
            }
        });
        
        $(".brands-autocomplete-input").bind("click focus", function() {
            var bai = $(".brands-autocomplete-input");
            if(bai.val() == bai.attr("data-placeholder")) {
                bai.val("");
            }
        });
        
        $(".brands-autocomplete-input").bind("blur", function() {
            var bai = $(".brands-autocomplete-input");
            if(bai.val() == "") {
                bai.val(bai.attr('data-placeholder'));
            }
        });
        
        $(".mk-clear-brands-input").click(function(e) {
            var bai = $(".brands-autocomplete-input");
            var cbi = bai.siblings(".mk-clear-brands-input");
            bai.val("");
            cbi.hide();
            bai.trigger("click");
            _gaq.push(['_trackEvent', 'search_page_filters', 'brands_auto_complete','clear']);
            e.stopPropagation();
        });
        
    }

    // brands=Rockport/sizes=/gender=/pricerange=100-18099/noofitems=24/sortby=/page=1
    $(window).hashchange( function(event){        
        if(!Myntra.Search.Data.onBeforeUnloadFired){
            filterClicked=true;
            for(prop in filterProperties){
                filterProperties[prop].setToDefault();
                filterProperties[prop].updateUI();
                filterProperties[prop].updateClearFilterUI();
            }
            for(prop in scrollProperties){
                scrollProperties[prop].setToDefault();
                scrollProperties[prop].updateUI();
            }
            var hash_str=location.hash;
            if(hash_str != ""){
                if(hash_str.indexOf("scrollPos") >= 0 && hash_str.indexOf("scrollPage") >= 0 && !scrollConfigArray["scrollProcessed"]){
                    var query_str=(location.hash).substring((location.hash).indexOf("#!")+2, (location.hash).length);
                    var query_options=query_str.replace(/\//g, "|").replace(/~\|/g,"~/").split("|");
                    
                    var optionslength=query_options.length;
                    for(var option=0;option<optionslength;option++){
                        var token=query_options[option];
                        var keyvalue=token.split("=");
                        if(filterProperties[keyvalue[0]] !== undefined && typeof keyvalue[1] != "undefined" && keyvalue[1] != ""){
                            filterProperties[keyvalue[0]].processURL(keyvalue[1]);
                            filterProperties[keyvalue[0]].updateUI();
                            filterProperties[keyvalue[0]].updateClearFilterUI();
                        }
                        if(scrollProperties[keyvalue[0]] !== undefined && typeof keyvalue[1] != "undefined" && keyvalue[1] != ""){
                            scrollProperties[keyvalue[0]].processURL(keyvalue[1]);
                            scrollProperties[keyvalue[0]].updateUI();
                        }
                    }
                    
                    Myntra.Search.InfiniteScroll.preparePageForBackButtonLoad();
                    scrollConfigArray["scrollProcessed"]=true;
                }
                else{
                    var query_str=(location.hash).substring((location.hash).indexOf("#!")+2, (location.hash).length);
                    var query_options=query_str.replace(/\//g, "|").replace(/~\|/g,"~/").split("|");
                    var optionslength=query_options.length;
                    for(var option=0;option<optionslength;option++){
                        var token=query_options[option];
                        var keyvalue=token.split("=");
                        if(filterProperties[keyvalue[0]] !== undefined && typeof keyvalue[1] != "undefined" && keyvalue[1] != ""){
                            filterProperties[keyvalue[0]].processURL(keyvalue[1]);
                            filterProperties[keyvalue[0]].updateUI();           
                            filterProperties[keyvalue[0]].updateClearFilterUI();
                        }
                    }

                    Myntra.Search.InfiniteScroll.preparePageForFilters();
                    Myntra.Search.InfiniteScroll.ajaxSearchCall(0);    
                }        
            }
            else{
                configArray["page"]=1;
                filterClicked=false;
                if(!Myntra.Search.Data.onPageLoad){
                    for(prop in filterProperties){
                        filterProperties[prop].updateUI();
                        filterProperties[prop].updateClearFilterUI();
                    }
                    configArray["filteredByUser"] = true;
                    Myntra.Search.Utils.setPageToFirst();
                    Myntra.Search.InfiniteScroll.ajaxSearchCall(0);
                }
            }
            Myntra.Search.Data.onPageLoad=false;
        }
    });

    $(window).hashchange();
    
    /*var ajaxLoaderScrollTimerID;
    var ajaxLoadeInitialOffsetFromTop=$(".mk-ajax-loader-msg").css("top");
    if( ajaxLoadeInitialOffsetFromTop != null){
         ajaxLoadeInitialOffsetFromTop= ajaxLoadeInitialOffsetFromTop.substring(0, ajaxLoadeInitialOffsetFromTop.indexOf("px"));
         $(window).scroll(function() {
            Myntra.Search.Utils.AjaxLoaderPositionReset(ajaxLoadeInitialOffsetFromTop);
        });
    }*/
    
    return true;
};

Myntra.Search.Utils.AjaxLoaderPositionReset = function(initialOffsetFromTop){
    var searchResultsSectionOffset=$(".mk-search-contents").offset().top;
    var searchResultsSectionHeight=searchResultsSectionOffset + $(".mk-search-contents").height();
    var currentLoaderMsgOffsetTop=$(".mk-ajax-loader-msg").css("top");
    
    if(currentLoaderMsgOffsetTop != null){
        currentLoaderMsgOffsetTop=currentLoaderMsgOffsetTop.substring(0,currentLoaderMsgOffsetTop.indexOf("px"))
    }
    
    var newPosition=parseInt(initialOffsetFromTop)+parseInt($(window).scrollTop());
    if( newPosition > initialOffsetFromTop && newPosition < (searchResultsSectionHeight+ searchResultsSectionOffset)){
        $(".mk-ajax-loader-msg").css({top: newPosition});
    }
    else{
        $(".mk-ajax-loader-msg").css({top: initialOffsetFromTop});
    }
}

Myntra.Search.Utils.gaTrackingForAjaxFilters = function(property, name){
    if(!trackingBooleans[property]){
        // Myntra.GA.gaTrackEventAjax("search_page_filters",name,"");
        _gaq.push(['_trackEvent', 'search_page_filters', name]);
        Myntra.Search.Utils.updateTrackingBooleans(property);        
    }
};

Myntra.Search.Utils.setAllTrackingBooleansToFalse = function(){
    for (var property in trackingBooleans) {
        trackingBooleans[property] = false;
    }
};

Myntra.Search.Utils.updateTrackingBooleans = function(property){
    if(!trackingBooleans[property]){
        trackingBooleans[property]=true;
    }    
};                    

Myntra.Search.Utils.priceRange = function(minVal, maxVal){
    if(configArray.pricerange.rangeMin != minVal || configArray.pricerange.rangeMax != maxVal ){
        configArray.pricerange.rangeMin=minVal;
        configArray.pricerange.rangeMax=maxVal;
        Myntra.Search.Utils.setPageToFirst();
        Myntra.Search.Utils.setFilterOptionsInUrlHash(["pricerange"]);
    }
};

Myntra.Search.Utils.setSortBy = function(sortby, el){
    $(".mk-product-sort li").removeClass("mk-sort-selected");
    $(el).closest("li").addClass("mk-sort-selected");
    if(sortby == "PRICE"){
        sortby=(configArray["sortby"] == "PRICEA")?"PRICED":"PRICEA";
    }
    configArray["sortby"]=sortby;
    Myntra.Search.Filters.sortFilterApplied=true;
    Myntra.Search.Utils.setPageToFirst();
    Myntra.Search.Utils.setFilterOptionsInUrlHash(["sortby", "page"]);
    Myntra.GA.gaTrackPageViewAjax($(el).attr("rel"));
    _gaq.push(['_trackEvent', 'search_page', 'set_sort_by',sortby]);
    return false;
};

Myntra.Search.Utils.setPage = function(page, el){
    configArray.page=page;
    Myntra.Search.Utils.setFilterOptionsInUrlHash(["page"]);
    Myntra.GA.gaTrackPageViewAjax($(el).attr("rel"));
    return false;
};

Myntra.Search.Utils.setPageToFirst = function(){
    Myntra.Search.InfiniteScroll.pageSetToFirst=true;
    configArray.page=0;
};

Myntra.Search.Utils.setPagePrev = function(el){
    var page_num=parseInt(configArray.page)-1;
    if(page_num >= 0 && page_num < total_pages){ 
        Myntra.Search.Utils.setPage(parseInt(configArray.page)-1, el);
    }
    return false;
};

Myntra.Search.Utils.setPageNext = function(el){
    var page_num=parseInt(configArray.page)+1;
    if(page_num >= 0 && page_num < total_pages){
        Myntra.Search.Utils.setPage(parseInt(configArray.page)+1, el);
    }    
    return false;
};

Myntra.Search.Utils.scrollToTop = function(target_top){
    Myntra.Search.FilterScroller.scrollInProgress=true;
    $('html, body').animate({scrollTop:target_top}, 800, function(){
        Myntra.Search.FilterScroller.scrollInProgress=false;
    });
}

Myntra.Search.Utils.removeValueFromArray = function(brand, array){
    var i = array.indexOf(brand);
    if(i != -1) array.splice(i, 1);
    return array;
}

Myntra.Search.Utils.setScrollOptionsInUrlHash = function(filter_types, el){
    
    var current_hash_str=location.hash;
    var current_query_str="";
    var query_hash=new Array();
    var query_obj=new Object();
    var elem=$(el);
    
    if(current_hash_str != ""){
        current_query_str=(current_hash_str).substring((current_hash_str).indexOf("#!")+2, (current_hash_str).length);
        query_hash=current_query_str.replace(/\//g, "|").replace(/~\|/g,"~/").split("|");
    }
    
    if(filter_types.length==0){
        query_hash=[];
    }
    else{
        for(var filteridx=0;filteridx<filter_types.length;filteridx++){
    
            var filter_type=filter_types[filteridx];
            var newstr="";
            var arr_idx=0;
            if(scrollProperties[filter_type] !== undefined ){
                newstr=scrollProperties[filter_type].getURLParam(elem);
                scrollProperties[filter_type].resetParams();
            }
            
            for(var idx=0;idx<query_hash.length;idx++){
                var keyvalsplit=query_hash[idx].split("=");
                if(keyvalsplit[0] == filter_type){
                    query_hash.splice(idx,1);
                    arr_idx=idx;
                }
            }
                    
            if(newstr != ""){
                if(arr_idx==0){
                    query_hash.push(newstr);
                }
                else{
                    query_hash.splice(arr_idx, 0, newstr);
                }    
            }    
        }
    }
    var query_hash_str="";
    query_hash_str=query_hash.join("|");
    
    // if(filter_types!='page' && filter_types!='sortby,page' &&
    // filter_types!='noofitems,page'){$('#avail-container').hide();}
    
    window.location.hash= "#!" + query_hash_str;
}

// set config array in one place and construct url every time
// brands=Nike/gender=Men/sizes=XL/pricerange=100-500/sortby=DISCOUNT/noofitems=24/page=1
Myntra.Search.Utils.setFilterOptionsInUrlHash = function(filter_types){

    var current_hash_str=location.hash;
    var current_query_str="";
    var query_hash=new Array();
    var query_obj=new Object();
    
    if(current_hash_str != ""){
        current_query_str=(current_hash_str).substring((current_hash_str).indexOf("#!")+2, (current_hash_str).length);
        query_hash=current_query_str.replace(/\//g, "|").replace(/~\|/g,'~/').split("|");
    }
    
    if(filter_types.length==0){
        query_hash=[];
    }
    else{
        for(var filteridx=0;filteridx<filter_types.length;filteridx++){
            
            var filter_type=filter_types[filteridx];
            var newstr="";
            var arr_idx=0;
            if(filterProperties[filter_type] !== undefined ){
                newstr=filterProperties[filter_type].getURLParam();
                filterProperties[filter_type].resetParams();
            }
            
            for(var idx=0;idx<query_hash.length;idx++){
                var keyvalsplit=query_hash[idx].split("=");
                if(keyvalsplit[0] == filter_type){
                    query_hash.splice(idx,1);
                    arr_idx=idx;
                }
            }
            if(newstr != ""){
                if(arr_idx==0){
                    query_hash.push(newstr);
                }
                else{
                    query_hash.splice(arr_idx, 0, newstr);
                }    
            }    
        }
    }
    
    var query_hash_str="";
    query_hash_str=query_hash.join("|");
    // if(filter_types!='page' && filter_types!='sortby,page' &&
    // filter_types!='noofitems,page'){$('#avail-container').hide();
    
    window.location.hash= "#!" + query_hash_str;
}

//Checks and returns a true if atleast one filter option is selected. false is none of the options is selected
Myntra.Search.Utils.getNoOfFiltersSet = function(){
    var checkedFilters=$(".content-slide-wrapper.filter-wrapper").find(".checked");
    return (checkedFilters.length > 0)?true:false;
    /*var filterSelected=false;
    for(prop in configArray){
        var type = typeof configArray[prop];
        if(type == "object"){
            if(prop == "pricerange" && (configArray["pricerange"].rangeMin != Myntra.Search.Data.priceRangeMin || configArray["pricerange"].rangeMax != Myntra.Search.Data.priceRangeMax)){
                filterSelected=true;
                break;
            }
            else{
                if(configArray[prop].length >=1){
                    filterSelected=true;
                    break;
                }
            }
        }
        else if(type == "string" && prop == "discountpercentage"){
            if(configArray[prop] != ""){
                filterSelected=true;
                break;
            }
        }
    }
    return filterSelected;*/
}

// note: to support IE (anger rising as I type), you'll need this.
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(obj, start) {
         for (var i = (start || 0), j = this.length; i < j; i++) {
             if (this[i] === obj) { return i; }
         }
         return -1;
    }
}

if (!Array.prototype.diff) {
    Array.prototype.diff = function(a) {
            return this.filter(function(i) {return !(a.indexOf(i) > -1);});
    };
}

if (!Array.prototype.remove) {
    Array.prototype.remove = function(value_to_remove) {
            return this.splice(this.indexOf(value_to_remove), 1);;
    };
}

Myntra.Search.Utils.toCurrencyString=function(num){
    var c=0,d='.',t=',';
    var n = num.toString();
    c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

Myntra.Search.FilterScroller = (function(){
    var obj = {};
    
    var prevScrollPos = 0,
        prevWinWidth = 0,
        onRight = $(window).width() > 1150 ? true : false,
        goToTopElem = $(".go-to-top-section"),
        goToTopRight = $('.go-to-top-btn.right');

    $(window).resize( function() {
        onRight = $(window).width() > 1150 ? true : false;        
    });
    
    obj.scrollInProgress=false;
    
    obj.showGoToTop = function(){
        if(!this.scrollInProgress){
            if(onRight) {
                goToTopElem.hide();
                goToTopRight.css('top', ($(window).height() - 44)/2).show();
            } else {
                goToTopRight.hide();
                goToTopElem.css({
                    "position":"fixed",
                    "top":0,
                    "left":$(".mk-left-nav").offset().left
                }).show();
            }
        }
    }
    
    obj.hideGoToTop = function(){
        goToTopRight.hide();
        goToTopElem.hide().removeAttr("style");
    }
    
    obj.endOfFilterScroll = function(){
        //return $(".mk-search-contents").offset().top + $(".mk-search-contents").height() - 100;
        return 0;
    }
    
    obj.timerToDetectScroll = function(filterSectionOffset) {
        var currScrollPos = $(window).scrollTop(),
            currWinWidth = $(window).width();
        
        if (onRight) {
            filterSectionOffset = $(window).height()/2;
        }
        
        if(currScrollPos != prevScrollPos || currWinWidth != prevWinWidth) {
            if(currScrollPos > filterSectionOffset && (onRight || currScrollPos < this.endOfFilterScroll())) {
                this.showGoToTop();
            }
            else{
                this.hideGoToTop();
            }
            prevScrollPos = currScrollPos;
            prevWinWidth = currWinWidth;
        }
    }
    
    return obj;
})();

$(document).ready(function(){
    if(!Myntra.Search.Data.noResultsPage){
        Myntra.Search.InitSearchPage();
    }
    else{
        Myntra.Search.InitNoResultsPage();
    }
    _gaq.push(['_trackEvent','search_page_visit', window.location.toString(), null, null, true]);
});

