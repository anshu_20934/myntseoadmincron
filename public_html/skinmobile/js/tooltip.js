
Myntra.Tooltip = Myntra.Base.extend({
    init: function(tip, ctx, cfg) {
        cfg = cfg || {side:'below'};
        this._super();
        this.tip = tip ? $(tip) : $('<div class="myntra-tooltip"/>').appendTo('body');
        this.conf(cfg);
        this.ctx = $(ctx);
        this.tmr = null;

        this.insertArrows();
        if (!this.tip.parent().is('body')) {
            this.tip.appendTo('body');
        }

        this.bd = this.tip.find('> .bd');
        if (!this.bd.length) {
            this.bd = $('<div class="bd"/>').appendTo(this.tip);
        }

        if (this.ctx.length && !this.conf('nohover')) {
            if (this.conf('useTitle')) {
                this.backupTitle();
            }
            this.addHoverEvents();
        }
    },

    setCtx: function(ctx) {
        this.ctx = $(ctx);
    },

    setBody: function(content) {
        this.bd.html(content);
    },

    backupTitle: function() {
        this.ctx.each(function(i, el) {
            var el = $(el), title = el.attr('title');
            if (!title) { return }
            el.attr('data-title', title).removeAttr('title');
        });
    },

    insertArrows: function() {
        if (this.conf('side') === 'below') {
            this.tip.prepend('<div class="arrow a-out arrow-top-outer"></div><div class="arrow a-in arrow-top-inner"></div>');
        }
        else if (this.conf('side') === 'above') {
            this.tip.prepend('<div class="arrow a-out arrow-bottom-outer"></div><div class="arrow a-in arrow-bottom-inner"></div>');
        }
        else if (this.conf('side') === 'left') {
            this.tip.prepend('<div class="arrow a-out arrow-right-outer"></div><div class="arrow a-in arrow-right-inner"></div>');
        }
        else if (this.conf('side') === 'right') {
            this.tip.prepend('<div class="arrow a-out arrow-left-outer"></div><div class="arrow a-in arrow-left-inner"></div>');
        }
        this.arrowOut = this.tip.find('.a-out');
        this.arrowIn = this.tip.find('.a-in');
    },

    show: function(e) {
        this.tip.css('top', '-500px');
        this.tip.css('display', 'block');

        if (this.conf('useTitle') && e) {
            this.setBody($(e.currentTarget).data('title'));
        }

        var el = e ? (this.conf('element') ? $(this.conf('element'), e.currentTarget) : $(e.currentTarget)) : this.ctx,
            ch = el.outerHeight(),
            cw = el.outerWidth(),
            p = el.offset(),
            th = this.tip.outerHeight(),
            tw = this.tip.outerWidth(),
            side = this.conf('side'),
            x, y,
            gap = 10;

        if (side === 'left' || side === 'right') {
            y = Math.round(th / 2) - 12;
            this.arrowOut.css('top', y);
            this.arrowIn.css('top', y + 3);
        }
        else if (side === 'above' || side === 'below') {
            x = Math.round(tw / 2) - 12;
            this.arrowOut.css('left', x);
            this.arrowIn.css('left', x + 3);
        }

        if (side === 'below') {
            p.top = p.top + ch + gap;
            p.left = p.left - tw / 2 + cw / 2;
        }
        else if (side === 'above') {
            p.top = p.top - th - gap;
            p.left = p.left - tw / 2 + cw / 2;
        }
        else if (side === 'left') {
            p.top = p.top - th / 2 + ch / 2;
            p.left = p.left - tw - gap;
        }
        else if (side === 'right') {
            p.top = p.top - th / 2 + ch / 2;
            p.left = p.left + cw + gap;
        }

        this.tip.css('left', p.left + 'px');
        this.tip.css('top', p.top + 'px');
        this.tip.fadeIn();
    },

    hide: function() {
        this.tip.fadeOut();
    },

    addHoverEvents: function() {
        var that = this;
        this.ctx.mouseenter(function(e) {
            if (that.tmr) {
                clearTimeout(that.tmr);
                that.tmr = null;
            }
            //that.show(e);
        });
        this.ctx.mouseleave(function(e) {
            that.tmr = setTimeout(function() { that.hide() }, 100);
        });
        this.tip.mouseenter(function(e) {
            if (that.tmr) {
                clearTimeout(that.tmr);
                that.tmr = null;
            }
        });
        this.tip.mouseleave(function(e) {
            that.tmr = setTimeout(function() { that.hide() }, 100);
        });
    }
});


Myntra.InitTooltip = function(tip, ctx, cfg) {
    tip = $(tip);
    ctx = $(ctx);
    if (!tip || !ctx) { return; }
    if (tip.length > 1) { return; }

    cfg = cfg || {};
    cfg.side = cfg.side || 'below';

    if (!tip.parent().is('body')) {
        tip.appendTo('body');
    }

    if (cfg.side === 'below') {
        tip.prepend('<div class="arrow a-out arrow-top-outer"></div><div class="arrow a-in arrow-top-inner"></div>');
    }
    else if (cfg.side === 'above') {
        tip.prepend('<div class="arrow a-out arrow-bottom-outer"></div><div class="arrow a-in arrow-bottom-inner"></div>');
    }
    else if (cfg.side === 'left') {
        tip.prepend('<div class="arrow a-out arrow-right-outer"></div><div class="arrow a-in arrow-right-inner"></div>');
    }
    else if (cfg.side === 'right') {
        tip.prepend('<div class="arrow a-out arrow-left-outer"></div><div class="arrow a-in arrow-left-inner"></div>');
    }

    var arrow = tip.find('.arrow'),
        arrowOut = tip.find('.a-out'),
        arrowIn = tip.find('.a-in'),
        x, y, p, th, tw, ch, cw,
        cached = false,
        gap = 10,
        tmr = null;

    ctx.mouseenter(function(e) {
        if (tmr) {
            clearTimeout(tmr);
            tmr = null;
        }
        if (!cached) {
            tip.css('top', '-500px');
            tip.css('display', 'block');
            th = tip.outerHeight();
            tw = tip.outerWidth();
            cached = true;
        }

        var el = cfg.element ? $(cfg.element, this) : $(this);
        ch = el.outerHeight();
        cw = el.outerWidth();
        p = el.offset();
        //console.log(tw, th, cw, ch, p);

        if (cfg.side === 'left' || cfg.side === 'right') {
            y = Math.round(th / 2) - 12;
            arrowOut.css('top', y);
            arrowIn.css('top', y + 3);
        }
        else if (cfg.side === 'above' || cfg.side === 'below') {
            x = Math.round(tw / 2) - 12;
            arrowOut.css('left', x);
            arrowIn.css('left', x + 3);
        }

        if (cfg.side === 'below') {
            p.top = p.top + ch + gap;
            p.left = p.left - tw / 2 + cw / 2;
        }
        else if (cfg.side === 'above') {
            p.top = p.top - th - gap;
            p.left = p.left - tw / 2 + cw / 2;
        }
        else if (cfg.side === 'left') {
            p.top = p.top - th / 2 + ch / 2;
            p.left = p.left - tw - gap;
        }
        else if (cfg.side === 'right') {
            p.top = p.top - th / 2 + ch / 2;
            p.left = p.left + cw + gap;
        }

        tip.css('left', p.left + 'px');
        tip.css('top', p.top + 'px');
        tip.fadeIn();
    });
    ctx.mouseleave(function(e) {
        tmr = setTimeout(function() { tip.fadeOut() }, 100);
    });
    tip.mouseenter(function(e) {
        if (tmr) {
            clearTimeout(tmr);
            tmr = null;
        }
    });
    tip.mouseleave(function(e) {
        tmr = setTimeout(function() { tip.fadeOut() }, 100);
    });
};

Myntra.InitTooltipFromTitle = function(ctx, cfg) {
    $(ctx).each(function(i, el) {
        var node = $(el),
            txt = node.attr('title');
        if (!txt) { return; }
        node.removeAttr('title');
        var tip = $('<div class="myntra-tooltip">' + txt + '</div>').appendTo('body');
        Myntra.InitTooltip(tip, node, cfg);
    });
};

