$(window).load(function(){
	//WISHLIST ACTION
	$(document).bind('myntra.fb.wishlist.post', function(e,data) {
	if(!data.exSocial){
		FB.api(
			'/me/'+facebook_app_name+':wishlist',
			'post',
			{product:decodeURIComponent(data.href)},
			function(response) {
				if (!response || response.error) {
					return {status:"error",msg:response.error};
				} else {
					return {status:"success",id:response.id};
				}
			});
	}
	else {
		return {status:"error",msg:'product excluded from posting to fb'};
	}
		
	});
	
	//PURCHASE ACTION
	$(document).bind('myntra.fb.purchase.post', function(e,data) {
		for(var i=0;i<data.hrefs.length;i++){
				FB.api(
					'/me/'+facebook_app_name+':buy',
					'post',
					{product:http_loc+'/'+decodeURIComponent(data.hrefs[i])},
					function(response) {
						if (!response || response.error) {
							return {status:"error",msg:response.error};
						} else {
							return {status:"success",id:response.id};
						}
				});
			}
	
	});
	
	 $('body').delegate('.wishlist-fb-login span,.purchase-fb-login span','click',function(){
			var referer = $(this).attr('data-referer');
	        Myntra.FBConnect(referer);
	        $(this).parent().hide();
				
	});
	
		//PAGE LOAD FIRE
	$(document).bind('myntra.fb.init.done',function(e,data){
		//CHECK IF THE SAVE COOKIE SET, THEN SAVE
		FB.getLoginStatus(function(response) {
			var FBStatus='',FBId='';
			if (response.status === 'connected') {
				FBStatus = 'authorized';
				FBId = response.userID; 
			} 
			
			//bind events
			switch (Myntra.Data.socialAction){
					case "wishlist":
				    //	login with facebook fire below on page load
						if(Myntra.Utils.Cookie.get('_sku')){
							Myntra.saveItemAsync({sku:Myntra.Utils.Cookie.get('_sku'),qty:1,url:Myntra.Utils.Cookie.get('_url'),excludeSocial:Myntra.Utils.Cookie.get('_saveitem_ex_social'),isMini:false});
						}
						//bind succesful fb login
						$(document).bind('myntra.fblogin.auth', function(e, data) {
							FB.getLoginStatus(function(response) {
								if(response.status === 'connected') {
									var isMiniPIP=false;
						   	    	var socialShareUrl=window.location.href;
						   	    	var exclude=false;
						   	    	if(typeof Myntra.Data.MiniPIPsocialExclude != 'undefined'){
						   	    		isMiniPIP=true;
						   	    		socialShareUrl=Myntra.Data.MiniSocialShareUrl;
						   	    		exclude=Myntra.Data.MiniPIPsocialExclude;
						   	    	}
						   	    	else{
						   	    		exclude=Myntra.Data.PDPsocialExclude;
						   	    	}
						   	    	
							    	$(document).trigger('myntra.fb.wishlist.post',{href:socialShareUrl,miniPIP:isMiniPIP,exSocial:exclude});
								}
								
							});
						});
					break;
					case "cart-wishlist":
						if(socialPost){
							if(FBStatus == 'authorized' && FBId != ''){
								$(document).trigger('myntra.fb.wishlist.post',{href:http_loc+'/'+socialPostArray[0],exSocial:false});
							}
						}
					break;
					case "purchase":
						if(socialPost){
							if(FBStatus != 'authorized' || FBId == ''){
								$('#share-fb-cont').append('<div class="purchase-fb-login"><em></em><span class="share-yes">SHARE YOUR PURCHASE WITH YOUR FRIENDS</span></div>')
							}else{	
								$(document).trigger('myntra.fb.purchase.post',{hrefs:socialPostArray});
							}
						}
						$(document).bind('myntra.fblogin.auth', function(e, data) {
							FB.getLoginStatus(function(response) {
								if (response.status === 'connected') {
									$(document).trigger('myntra.fb.purchase.post',{hrefs:socialPostArray});
								}
							});
						});
							
					break;
				
				}
			});
		});
		
});