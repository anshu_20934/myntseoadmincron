
Myntra.FBInit=function(){
    window.fbAsyncInit = function() {
        FB.init({
        appId      : facebook_app_id,
        channelUrl : (location.protocol == 'https:' ? https_loc + '/s_fbchannel.php' : http_loc + '/fbchannel.php'),
        status     : true,
        cookie     : true,
        xfbml      : true,
        oauth      : true
        });
        $(document).trigger('myntra.fb.init.done');
        
        //Show loyalty widget only when the like button is loaded
        // FB.Event.subscribe('xfbml.render', function() {
        //       $('#loyalty-widget').show(); // If any loyaly points widget is there show it
        // });
        //Like Button Loyalty flow for mobile.
        // FB.Event.subscribe('edge.create',function(response) {
        //         Myntra.FB.loyaltyFlow('like_flow',response);
        //     }
        // );
        //Unlike event listener for loyalty flow for mobile
//       	FB.Event.subscribe('edge.remove',function(url){
//            //When user unlike a product call the loyalty points flow
//            Myntra.FB.loyaltyFlow('unlike',url);
//        });
    };
    (function(d){
        var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
        js = d.createElement('script'); js.id = id; js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        d.getElementsByTagName('head')[0].appendChild(js);
    }(document));
}


Myntra.FBConnect = function(referer) {
    FB.login(function(response) {
        if (response.authResponse) {
            // user successfully connected with FB
            $(document).trigger('myntra.fblogin.auth');

            // now do the post actions in our end
            var url = '/myntra/ajax_facebook_login.php';
            if (referer) {
                url += '?ref=' + referer;
            }
            $.post(url,
                {menu_usertype:"C", menu_redirect:"1", menu_filetype:"myhome",_token:Myntra.Data.token},
                function(data){
                    if (data.status == "success") {
                      	data.fb_action = data.fb_action || 'login';
                        $(document).trigger('myntra.fblogin.done', data);
                    }
                    else{
                        $(document).trigger('myntra.fblogin.done', {status:'failure'});
                    }
                },
                "json"
            );
        }
        else {
            $(document).trigger('myntra.fblogin.done', {status:'access-denied'});
        }
    },
    {scope:'publish_actions,publish_stream,user_birthday,user_interests,user_likes,user_location,user_relationships,email,friends_birthday,friends_interests,friends_relationships,read_stream'}
    );
};

Myntra.FB = Myntra.FB || {};

/** 
 * Loyalty flow for FB action
 * 
 * fb_action : like,unlike,
 * url: abs_url the user has liked or unliked.
 *
 * TODO : add csrf token
 * Check if the user is allowed to get points
**/
Myntra.FB.loyaltyFlow = function(fb_flow,data,cb){
  cb = cb ? cb : function(){};
  if(fb_flow === 'like_flow' || fb_flow === 'checkout_flow'){
      //Like is only applicable for pdp page and certain applicable user. This is implmented in loyaltyUICheck
      if(!Myntra.FB.loyaltyUICheck(fb_flow,data)){
          cb("Loyalty points not applicable.");
          return;
      }

      Myntra.FB.loyaltyLoader('show',fb_flow);
      $.post('/myntra/loyalty.php',{fb_flow:fb_flow,data:data},function(res, textStatus, jqXHR){
          if (textStatus !== "success") {
                  //TODO : handle error condition here
                  Myntra.FB.loyaltyLoader('hide',fb_flow);
                  cb("Cannot assign Loyalty Points. Try again later");
                  return;
          }
          var status = res.status; // Any error while calulating the loyalty points
          if(status === 'ok'){
            var pointsAwarded = parseInt(res.points_awarded);
            Myntra.FB.loyaltyLoader('hide',fb_flow);

            // like_flow is applicable for pdb
            var strMsg = pointsAwarded;
            strMsg += (pointsAwarded > 1) ? " Loyalty Points " : " Loyalty Point ";
            $('#loyalty-widget').html('<i class="icon-tick"></i><span class="lbl">'+strMsg+'</span><span> added to your account.</span>');
            cb(null);
          }else{
            //Handle loyalty related error and show it in UI
            Myntra.FB.loyaltyLoader('hide',fb_flow);
            cb("Cannot assign Loyalty Points. Try again later");
          }
      });
  }
};

/**
 * Filtering the flow at UI level. Checks are also there in portal as well as services
 * To avoid unnecessary Ajax request. Not fully relying on UI
 * @return {[type]} [description]
 */
Myntra.FB.loyaltyUICheck = function(fb_flow,data){
    var return_val = false;
    if(fb_flow === 'like_flow'){
        // Flag set in UI as hidden value for checking user allowed to get points for loyalty
        if($('#canGetLoyaltyPointsForLike').val()){
            //In case or like data will be url of the page liked
            return_val = true;
            var arr = data.split('/');
            if(arr[2] === 'www.facebook.com') return_val = false; //In case the like button is for Myntra facebook page
        }else{
            return_val = false;
        }
        if(return_val){
            // Check misuse at UI level for a like button 
            var currentLikeUrl = data.substring(0,data.indexOf('?'));
            if(Myntra.FB.lastLikedUrl && Myntra.FB.lastLikedUrl === currentLikeUrl ){
                return_val = false;
            }
            Myntra.FB.lastLikedUrl = currentLikeUrl; // Store the last liked url to stop misue at UI level
        }
    }else if(fb_flow === 'checkout_flow'){
        return_val = true;
    }
    return return_val;
};

Myntra.FB.loyaltyLoader = function(action,fb_flow){
    if(fb_flow === 'like_flow'){
        var loyaltyWidget = $('#loyalty-widget');
        if(action === 'show')
            loyaltyWidget.html('<img src="http://myntra.myntassets.com/skin2/images/loader.gif"/> <span>Processing request for Loyalty Points.</span>');
        if(action === 'hide')
            loyaltyWidget.html('');
    }
};

$(window).load(function(){
    Myntra.FBInit();
   
});

