(function ($) {

    $(document).bind("checkinview",function () {
        var vpH = Myntra.Utils.getViewportHeight(),
            scrolltop = (document.documentElement.scrollTop ?
                document.documentElement.scrollTop :
                document.body.scrollTop),
            els = [];
        
        // elements that are bound to inview are cached here
        $.each($.cache, function () {
            if (this.events && this.events.inview) {
                els.push(this.handle.elem);
            }
        });

        if (els.length) {
            $(els).each(function () {
                var $el = $(this),
                    top = $el.offset().top,
                    height = $el.height(),
                    inview = ($el.attr('data-inview') == 'true') || false;
                if (scrolltop > (top + height) || scrolltop + vpH < top) {
                    if (inview) {
                        $el.attr('data-inview', false);
                        $el.trigger('inview', [ false ]);                        
                    }
                } else if (scrolltop < (top + height)) {
                    if (!inview) {
                        $el.attr('data-inview', true);
                        $el.trigger('inview', [ true ]);
                    }
                }
            });
        }
    });
    
    // trigger the event on page load
    $(function () {
        $(document).trigger("checkinview");
    });
})(jQuery);