Myntra.Utils.extend = function(subc, superc, overrides) {
    var F = function(){};
    F.prototype = superc.prototype;
    subc.prototype = new F();
    subc.prototype.constructor = subc;
    subc.superclass = superc.prototype;
    for (var i in overrides) {
        if (!overrides.hasOwnProperty(i)) { continue; }
        subc.prototype[i] = overrides[i];
    }

};

Myntra.Utils.mobileOrientation = function(){
    var _lastOrientation = "";
    var _isChanged = "";
    this.set = function() {
        hasOrientationChanged();
        var width = $(window).width();
        var height = $(window).height();
        if (width > height) {
            _lastOrientation = "landscape";
        } else {
            _lastOrientation = "portrait";
        }
    };
    this.get = function() {
        if (_lastOrientation == "")
            this.set();
        return _lastOrientation;
    };
    this.isChanged = function() {
        return _isChanged;
    };
    var hasOrientationChanged = function() {
        var width = $(window).width();
        var height = $(window).height();
        var presentOrientation;
        if (width > height) {
            presentOrientation = "landscape";
        } else {
            presentOrientation = "portrait";
        }
        if (_lastOrientation == presentOrientation) {
            _isChanged = false;
        } else {
            _isChanged = true;
        }
    };
};

Myntra.Utils.extendObjToScreen = function(obj) {
    var _width = $('body').outerWidth();
    var _height = $(document).height()-$(window).scrollTop();
    if(_height<obj.find('.mod').outerHeight()) {
        _height = obj.find('.mod').outerHeight();;
    }
    obj.css({width : _width ,height : _height});
};

Myntra.Utils.extendClickEvent = function(obj,func) {
    //console.log(obj.length, obj);
    if(!obj.length){
        return;
    }
    $(obj).fastClick(function(e){
        func(e);
    });
};

Myntra.Utils.getQueryParam = function(name,url) {
    if ( typeof url == "undefined" ){ return; }
    if ( url.indexOf('#') != -1 ) {
        url = url.substring(0, url.indexOf('#'));
    }
    var params = {};
    var qstr = (url)?(url.substring(url.indexOf('?'))):location.search;
    if (!qstr) { return ''; }
    qstr = qstr.replace(/^\?/, '');
    var nvPairs = qstr.split('&');
    for (var i = 0, n = nvPairs.length; i < n; i += 1) {
        var parts = nvPairs[i].split('=');
        params[parts[0]] = parts[1];
    }
    Myntra.Utils.getQueryParam = function(name) {
        return (params[name] || '');
    };
    return (params[name] || '');
};



Myntra.Utils.validateEmail = function(email){
     var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
     return re.test(email);
    
};



Myntra.Utils.scrollToPos = function (pos,interval){
    interval=interval||0;
    $('html,body').animate({scrollTop:pos},interval,'easeOutCirc',function(){$(window).scrollTop(pos);
    });
    

};

/**
 * This is used to store a composite string (delimited values)
 * basically, it concatenates value to existingValue and returns if the value is either not found in existingValue,
 * or found but unique is set to false
 * Basically the unique flag decides whether to repeat the occurrence of value, if it already exists in existingValue: it should be boolean
 * delimiter is the character (or string) used to separate the values
 * limit: limit the number of entries to this number: most recent
 */
Myntra.Utils.setCompositeData = function (value, existingValue, unique, delimiter, limit) {
    if(typeof unique  === "undefined") {
        var unique = true;
    }
    if(typeof delimiter === "undefined") {
        var delimiter = "|";
    }
    if(typeof limit === "undefined") {
        var limit = -1;
    }
    if(existingValue != null && existingValue !="") {
        var valueArr = existingValue.split(delimiter);
        var found =  (unique && $.inArray(value,valueArr) != -1);
        if(!found || !unique) {
            valueArr.push(value);
            if(limit > 0 && valueArr.length > limit) {
                valueArr = valueArr.reverse();
                valueArr.splice(limit,valueArr.length-limit);
                valueArr = valueArr.reverse();
            }
            value = valueArr.join(delimiter);
        }
    }
    return value;
};

Myntra.Utils.Validation = {} || Myntra.Utils.Validation;

Myntra.Utils.Validation.nameCheck = function(selector, errSelector, isDefaultValuePresent){
    var el=$(selector);
    var errEl=$(errSelector);
    if(!el || !errEl){
        return false;
    }
    var nameVal = el.val(),
        nameExp = /^[A-Za-z ]*$/;
    if (!nameVal) {
        errEl.html("Please enter your name").show();
        el.addClass("mk-err");
        return false;
    }
    if(isDefaultValuePresent && ($(selector).val() == $(selector).attr("data-placeholder"))){
        $(errSelector).html("Please enter a valid name").show();
        return false;
    }
    if(!nameExp.test(nameVal)) {
        errEl.html("Name can contain only letters").show();
        el.addClass("mk-err");
        return false;
    }
    if(nameVal.length < 2) {
        errEl.html("Name must have at least two characters").show();
        el.addClass("mk-err");
        return false;
    }
    errEl.html("").hide();
    el.removeClass("mk-err");
    return true;
}

Myntra.Utils.Validation.emailCheck = function(selector, errSelector) {
    var el=$(selector);
    var errEl=$(errSelector);
    if(!el || !errEl){
        return false;
    }
    var emailVal = el.val(),
        emailExp = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; 
    if (!emailVal) {
        errEl.html("Please enter your email address").show();
        el.addClass("mk-err");
        return false;
    }
    if(el.val() == el.attr("data-placeholder")){
        errEl.html("Please Enter a valid email address").show();
        el.addClass("mk-err");
        return false;
    }
    if(!emailExp.test(emailVal)) {
        errEl.html("Please enter a valid email address").show();
        el.addClass("mk-err");
        return false;
    }
    el.removeClass("mk-err");
    errEl.html("").hide();
    return true;
};

Myntra.Utils.Validation.messageCheck = function(selector) {
    var el=$(selector);
    var errEl=$(errSelector);
    if(!el || !errEl){
        return false;
    }
    var messageVal = el.val(),
        emailExp = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; 
    if (!messageVal) {        
        errEl.html("Please enter your message").show();
        el.addClass("mk-err");
        return false;
    }    
    errEl.html("").hide();
    el.removeClass("mk-err");
    return true;
};

Myntra.Utils.Validation.isNumber = function(num) {
  return (typeof num == 'string' || typeof num == 'number') && !isNaN(num - 0) && num != null;
};

Myntra.Utils.allowZoom = function(flag){
    $('head meta[name=viewport]').remove();
    if(flag){
        $('head').prepend('<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=10.0, minimum-scale=1, user-scalable=1" />');
    } else {
        $('head').prepend('<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=0" />');
    }

};
/* Copied code need to validate its cross browser effectiveness */
Myntra.Utils.getZoomLevel = function(){
    var ovflwTmp = $('html').css('overflow');
    $('html').css('overflow','scroll');
    var viewportwidth;
    // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight 
    if (typeof window.innerWidth != 'undefined')  {
        viewportwidth = window.innerWidth;
    } else if (typeof(document.documentElement) != 'undefined'  &&
        typeof document.documentElement.clientWidth != 'undefined' &&
        document.documentElement.clientWidth != 0) {
        // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document) 
        viewportwidth = document.documentElement.clientWidth;
    } else {
        // older versions of IE viewportwidth = document.getElementsByTagName('body')[0].clientWidth;
    }
    var windW = $(window).width();
    var scrollBarW = viewportwidth - windW;
    if(!scrollBarW) return 1;
    $('html').css('overflow',ovflwTmp);
    return  (15 / scrollBarW);
}

 /* return a function, that will call the underlying 'fn' after the myntra login is done
 * author: gopinath.k@myntra.com
 */
Myntra.Utils.afterLogin = function(fn) {
    return function() {
        var context = this, args = arguments;
        if (!Myntra.Data.userEmail) {
            $(document).trigger('myntra.login.show', {onLoginDone: function(data) {
                fn.apply(context, args);
            }});
        }
        else {
            fn.apply(context, args);
        }
    };
};

Myntra.Utils.isTouchDevice = function(){
    try{
        document.createEvent("TouchEvent");
        return true;
    }catch(e){
        return false;
    }
}

Myntra.Utils.touchScroll = function(id){
    if(isTouchDevice()){ //if touch events exist...
        var el=document.getElementById(id);
        var scrollStartPos=0;

        document.getElementById(id).addEventListener("touchstart", function(event) {
            scrollStartPos=this.scrollTop+event.touches[0].pageY;
            event.preventDefault();
        },false);

        document.getElementById(id).addEventListener("touchmove", function(event) {
            this.scrollTop=scrollStartPos-event.touches[0].pageY;
            event.preventDefault();
        },false);
    }
};

/**
 * Truncate a string to the given length, breaking at word boundaries and adding an elipsis
 * @param string str String to be truncated
 * @param integer limit Max length of the string
 * @return string
 */
Myntra.Utils.truncateStr = function (str, limit) {
    var bits, i;
    if(typeof limit == "undefined"){
        limit=45;
    }

    if ("string" !== typeof str) {
        return '';
    }
    bits = str.split('');
    if (bits.length > limit) {
        for (i = bits.length - 1; i > -1; --i) {
            if (i > limit) {
                bits.length = i;
            }
            else if (' ' === bits[i]) {
                bits.length = i;
                break;
            }
        }
        bits.push('...');
    }
    return bits.join('');
};
// END: truncate

Myntra.Utils.BBSupport = (function(){
    var obj = {};

    obj.lightBoxId = "";

    obj.addIdToUrlHash = function(id){
        this.lightboxId=id.substring(id.indexOf("#")+1 ,id.length);
        window.location.hash="#!"+this.lightboxId;
    }

    obj.removeIdFromUrlHash = function(id){
        this.lightboxId=id.substring(id.indexOf("#")+1 ,id.length);
        window.location.hash= "#!default";
    }

    return obj;
    
})();
Myntra.Utils.getViewportWidth = function() {
    xWidth = null;
    if(window.screen != null)
      xWidth = window.screen.availWidth;

    if(window.innerWidth != null)
      xWidth = window.innerWidth;

    if(document.body != null)
      xWidth = document.body.clientWidth;

    return xWidth;
};
Myntra.Utils.getViewportHeight = function() {
    xHeight = null;
    if(window.screen != null)
    xHeight = window.screen.availHeight;

    if(window.innerHeight != null)
    xHeight =   window.innerHeight;

    if(document.body != null)
    xHeight = document.body.clientHeight;

    return xHeight;
};
Myntra.Utils.preventDefault = function(e) {
    if(typeof(e.preventDefault)=="undefined"){
        e.returnValue = false;
    }else{
        e.preventDefault();
    }
};
Myntra.Utils.stopPropagation = function(e) {
	//e.cancelBubble is supported by IE -
	// this will kill the bubbling process.
	e.cancelBubble = true;
	e.returnValue = false;

	//e.stopPropagation works only in Firefox.
	if ( e.stopPropagation ) e.stopPropagation();
};
Myntra.Utils.hideTooltipAndRearrangeHeader = function(){
    if($(".get-started-tooltip-container").length){
        $(".get-started-tooltip-container").fadeOut();
    }
};