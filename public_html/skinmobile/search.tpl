{extends file="layout.tpl"}
{block name=body}
    {if $total_items ge 1}
        {include file="inc/m-ads.tpl"}
        <div class="m-search-page-header">
        {if $navflag}
            <h1>
                {foreach key=navitemidx item=navitem from=$nav_selection_arr_url}
                    {if !$navitem@first} <span class="slash-style">/</span> {/if}
                    {if $navitem@last}{$navitemidx}{else}<a href="{$navitem}">{$navitemidx}</a>{/if}
                {/foreach}    
            </h1>
        {else}
            <h1>{if $h1 && $h1|trim neq ""}{$h1}{elseif $synonymQuery && $synonymQuery|trim neq ""}{$synonymQuery}{else}{$query|truncate:40|replace:"-":" "|capitalize}{/if}</h1>
        {/if}
        {if $query_level=='FULL'}
            <div class="mk-sub-text mk-help-text">NO MATCHES FOUND / SHOWING RESULTS FOR PARTIAL MATCHES INSTEAD</div>
        {elseif $synonymQuery && $synonymQuery|trim neq ""}
            <div class="mk-sub-text mk-help-text">NO MATCHES FOUND / SHOWING RESULTS FOR <span class="mk-spl-text">{$query}</span> INSTEAD</div>
        {else}
        {/if}
        <div class="mk-product-count">{if $total_items}{$total_items}{else}no{/if} Products</span> found</div>
        <div class="filters-sort-section">
            <button class="btn btn-grey" id="sort-btn">SORT: <span class="sort-by-type">{if $sort_param_in_url eq 'POPULAR' || $sort_param_in_url eq ''}Popularity{elseif $sort_param_in_url eq 'DISCOUNT'}Discount{elseif $sort_param_in_url eq 'RECENCY'}What's New{elseif $sort_param_in_url eq 'PRICEA'}Price L - H{else}Price H - L{/if}</span></button>
            <button class="btn btn-grey" id="filter-btn">filters <span class="filters-icon"></span></button>
        </div>    
    </div>
    <div class="search-list-container mk-cf">
        <div class="m-search-list" data-role="exist" data-page="{$current_page-1}">
            {include file="search/search-list.tpl"}
        </div>    
    </div>
    {if $page_count gt 1}
    <div class="mk-infscroll-loader mk-cf">
        {*<a class="mk-more-products-link btn normal-btn" href="{if $current_page+1 le $page_count && $current_page ge 1}{$http_location}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page+1}{/if}" rel="{if $current_page+1 le $page_count && $current_page ge 1}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page+1}{/if}" onclick="return Myntra.Search.Utils.triggerManualInfiniteScroll();">Show more products <span class="products-left-content">(<span class="products-left-count">{if $total_items > 24}{$total_items - $noofitems}{/if}</span>)</span></a>*}
        <div class="more-products-loading-indicator" style="display:none;">
            <img src="{$cdn_base}/skin2/images/loader_transparent.gif">
        </div>
    </div>
    {/if}
    {else}
    <div class="m-search-page-header mk-no-results">
        <h1><span class="search-icon"></span> {if $h1 && $h1|trim neq ""}{$h1}{elseif $synonymQuery && $synonymQuery|trim neq ""}{$synonymQuery}{else}{$query|truncate:40|replace:"-":" "|capitalize}{/if}</h1>
        <div class="mk-no-results-sub-section-first">
            <div class="mk-sub-text">OOPS - WE COULDN'T FIND ANY MATCHES!</div>
        </div>
        <div class="mk-no-results-sub-section-second">
            <div class="mk-sub-text">HOW ABOUT SEARCHING AGAIN?</div>
            <div class="mk-sub-text mk-help-text">PLEASE CHECK THE SPELLING, OR TRY LESS SPECIFIC SEARCH TERMS</div>
        </div>
        <div class="mk-search">
            <span class="pseudo-search-box pseudo-search-input" type="text" data-searchboxtype="headersearchbox"><span class="" style="padding-left: 5px;">Search Myntra</span></span>
        </div>    
        {if $topNavigationWidget}
        <div class="mk-no-results-sub-section-third">
            <div class="mk-sub-text">OR BROWSE OUR EXTENSIVE CATALOG BY:</div>
            <ul>
                {foreach key=indexA item=itemsA from=$topNavigationWidget name=topNavigationWidgetLoop}
                    <li><a class="mk-level1" href="{$itemsA.link_url}?nav_id={$itemsA.id}">{$itemsA.link_name}</a></li>
                {/foreach}
            </ul>
        </div>
        {/if}
    </div>
    {/if}


<script type="text/javascript">
    Myntra.Data.pageName='search';
    Myntra.Search = Myntra.Search || {};
    Myntra.Search.Data = Myntra.Search.Data || {};
        Myntra.Search.Data = {
            pageName : "search",
            noResultsPage : {if $total_items ge 1}false{else}true{/if},
            batchProcess : false,
            onBeforeUnloadFired : false,
            pageSetToFirst : false,
            userTriggered : false,
            onPageLoad : true,
            filterClicked : false,
            filterClickedName : "none",
            navId : "{$nav_id}",
            scrollLockEnabled : "{$scrollLockVariant}" == "test" ? true : false,
            stickyFiltersEnabled : "{$scrollLockVariant}" == "test" ? true : false,
            totalPages : parseInt({$page_count}),
            showAdditionalProductDetails : "{$show_additional_product_details}",
            query : "{$query|lower}",
            parameterQuery : '{$parameter_query}',
            boostedQuery : "{$boosted_query}",
            currentPage : parseInt({$current_page})-1,
            pageDefault : ((this.currentPage) > this.totalPages) ? 0 : this.currentPage,
            priceRangeMin : parseInt({$rangeMin}),
            priceRangeMax : parseInt({$rangeMax}),
            sortByDefault : {if $sort_param_in_url}"{$sort_param_in_url}"{else}""{/if},
            defaultNoOfItems : parseInt({$noofitems}),
            noOfAutoInfiniteScrolls : parseInt({$noOfAutoInfiniteScrolls}),
            userQuery : {if $userQuery}true{else}false{/if},
            queryLevel : "{$query_level}",
            scrollThreshold : 100,
            userActionPerformed : false,
            priceRangeMin : parseInt({$rangeMin}),
            priceRangeMax : parseInt({$rangeMax}),
            listGroup : "{$listGroup}",
            searchCategoryList : "{$productListData.searchCategoryList}",
            searchTopCategory : "{$productListData.searchTopCategory}",
            searchSubCategoryList : "{$productListData.searchSubCategoryList}",
            searchTopSubCategory : "{$productListData.searchTopSubCategory}",
            searchBrandList: "{$productListData.searchBrandList}",
            searchTopBrand : "{$productListData.searchTopBrand}",
            searchGenderList : "{$productListData.searchGenderList}",
            searchTopGender : "{$productListData.searchTopGender}",
            searchArticleTypeList : "{$productListData.searchArticleTypeList}",
            searchTopArticleType : "{$productListData.searchTopArticleType}",
            searchColorList : "{$productListData.searchColorList}",
            searchTopColor : "{$productListData.searchTopColor}",

        };
    {literal}

    var dynamicFilters=[];
    var articleTypeAttributesList=[];
    var filterProperties=new Object();
      var scrollProperties=new Object();

      var configArray=new Object();
    configArray["url"]=Myntra.Search.Data.query;
    configArray["parameter_query"]=Myntra.Search.Data.parameterQuery;
    configArray["boosted_query"]=Myntra.Search.Data.boostedQuery;
    configArray["query_level"] = Myntra.Search.Data.queryLevel;
    configArray["page"]=Myntra.Search.Data.currentPage;
    configArray["noofitems"]=Myntra.Search.Data.defaultNoOfItems;
    configArray["gender"]=[];
    configArray["sizes"]=[];
    configArray["brands"]=[];
    configArray["articletypes"]=[];
    configArray["discountpercentage"]="";
    configArray["pricerange"]={"rangeMin" : Myntra.Search.Data.priceRangeMin, "rangeMax" : Myntra.Search.Data.priceRangeMax};
    configArray["sortby"]=Myntra.Search.Data.sortByDefault;
    configArray["uq"]=Myntra.Search.Data.userQuery;

    var scrollConfigArray=new Object();
    scrollConfigArray["scrollPos"]=0;
    scrollConfigArray["scrollPage"]=0;
    scrollConfigArray["scrollProcessed"]=false;

    var trackingBooleans=new Object();
    trackingBooleans["gender"]=false;
    trackingBooleans["sizes"]=false;
    trackingBooleans["brands"]=false;
    trackingBooleans["pricerange"]=false;
    trackingBooleans["noofitems"]=false;

    avail_cookie_ids={/literal}'{$avail_cookie_ids}';
    AVAIL_USER_ID='{$AVAIL_USER_ID}'{literal};
    
    //Google Conversion Analytics 
    var gaEventCategory = Myntra.Search.Data.searchCategoryList.split(',').length ==  1 ? Myntra.Search.Data.searchCategoryList : '',
    gaEventArticleType = Myntra.Search.Data.searchArticleTypeList.split(',').length ==  1 ? Myntra.Search.Data.searchArticleTypeList : '',
    gaEventGender = Myntra.Search.Data.searchGenderList.split(',').length ==  1 ? Myntra.Search.Data.searchGenderList : '',
    gaEventBrand = Myntra.Search.Data.searchBrandList.split(',').length ==  1 ? Myntra.Search.Data.searchBrandList : '';


</script>
{/literal}

{if $total_items ge 1}
    <script type="text/javascript">
        noResultsPage=false;
    </script>
    {if $article_type_attributes}
        <script type="text/javascript">
        {foreach key=attribute item=props from=$article_type_attributes}
            configArray["{$props.title|replace:' ':'_'}_article_attr"]=[];
            trackingBooleans["{$props.title|replace:' ':'_'}_article_attr"]=false;
            articleTypeAttributesList.push("{$props.title|replace:' ':'_'}_article_attr");
        {/foreach}
        </script>
    {/if}

    {if $article_type_attributes}
        <script type="text/javascript">
        {foreach key=attribute item=props from=$article_type_attributes}
            {literal}
            filterProperties["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"]={
                getURLParam: function(){
                    var ataStr=configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"].join(":");
                    return (ataStr != "")?"{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"+"="+unescape(ataStr):"";
                },
                processURL: function(str){
                    configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"]=Myntra.Search.getFilterValues(unescape(str));
                    return true;
                },
                updateUI: function(){
                    //unselect all brands
                    $(".article_type_attr .mk-labelx_check").removeClass("checked").addClass("unchecked");

                    
                    //update UI selection for brands in configArray
                    for(var attr=0;attr<configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"].length;attr++){
                        var selector=configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"][attr].toLowerCase().replace(/\W+/g, "") + "-ata-filter";
                        $("."+selector).removeClass("unchecked").addClass("checked");
                    }

                    $("[data-target={/literal}{$props.title|replace:' ':'_'}_article_attr{literal}-types] .selected").html(Myntra.Utils.truncateStr(configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"].join(", "))); 

                    //Myntra.Search.Filters.updateUI("{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}");
                    return true;
                },
                setToDefault: function(){
                    configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"]=[];
                    return true;
                },
                resetParams: function(){
                    configArray["page"]=0;
                    return true;
                },
                updateFilterVisibility: function(filters){
                    if((Myntra.Search.Data.filterClickedName != "" && Myntra.Search.Data.filterClickedName!="{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}") || (Myntra.Search.Data.filterClickedName != "" && Myntra.Search.Data.filterClickedName=="{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}" && configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"].length == 0)){
                        $(".mk-{/literal}{$props.title|replace:' ':'_'}{literal}_article_attr-filters .mk-labelx_check").each(function (index,element){
                            if(typeof filters.article_type_attributes.{/literal}{$props.title|replace:' ':'_'}_article_attr{literal} == 'undefined' || typeof filters.article_type_attributes.{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}.values[$(element).attr('data-value')] == 'undefined'){
                                $(element).addClass('disabled');
                            }else{
                                $(element).removeClass("disabled");
                            }
                        });
                    }
                },
                setFilterClickedName: function(){
                    Myntra.Search.Data.filterClickedName = "{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}";
                },
                updateClearFilterUI: function(){
                    var resetCondition = (configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"].length >= 1);
                    Myntra.Search.Filters.updateClearFiltersUI("{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}", resetCondition);
                }};
            {/literal}
        {/foreach}
        </script>
    {/if}
{/if}

{/block}

{block name=pagejs}
    
{/block}
{block name=searchpageRightmenu}
	<div class="content-slide-wrapper sort-wrapper content-inactive">
         {include file="search/sort.tpl"}
    </div>
    <div class="content-slide-wrapper filter-wrapper content-inactive">
        {include file="search/filters.tpl"}
    </div>
{/block}
{block name=macros}
    {include file="macros/search-macros.tpl"}
{/block}

