{assign var="minNonLazyFilters" value="7"}
<div class="mod">
    <div class="hd"><div class="back back-btn-cnt"><span class="back-img"></span></div>Filters<span class="filter-type-name" id="filter-type-name"></span><div class="close-wrapper"><span class="btn-close"></span></div></div>
    <div class="bd mk-cf">
		<button type="button" class="btn btn-show-filter-results btn-orange-noplay">SHOW RESULTS</button>
        <div class="search-filters">
            <div class="filter-types-wrapper">
            <div class="types-header">
                <span class="mk-loader mk-f-left"></span>
                <span class="mk-clear-all-filters mk-f-right mk-hide">Reset All</span>    
            </div>
            <div id="filter-types" class="slide-main-content">
                <ul class="filter-types single-step-list">
                    {if $article_type_filters}
                        <li data-name="Categories" data-target="articletypes-types" data-isOptimized=true data-optimizedContainer="optimizedContainer"><span class="anim-link prompt">Categories</span><span class="mk-level1-arrownav"></span><span class="selected"></span>
                                    {if $article_type_filters}
            	<div id="articletypes-types" class="articletypes-types filter-content link-details" style="display:none;" data-scrollinit="false">
                    <div class="filter-header">
                        <span class="mk-loader mk-f-left"></span>    
                        <span data-key="articletypes" class="mk-clear-filter mk-f-right mk-hide">Reset</span>
                    </div>
                    <div id="articletypes-types-wrapper" class="filter-content-wrapper">
                    <div class="mk-filter-content">
                    <div id="mk-articletypes" class="optimizedContainer mk-filter-wrapper mk-articletypes-filters">
                        {foreach key=article_type item=count from=$faceted_search_results_new->global_attr_article_type_facet name="article_type"}
                            {assign var="url_desc_l" value="$query-$article_type"|lower|replace:' ':'-'}
                            {if $smarty.foreach.article_type.iteration < $minNonLazyFilters}
                                <a href="{$http_location}/{myntralink url_desc="$url_desc_l" parent_landingpage_url="$query" global_attr_article_type_facet="$article_type" is_landingpage="y" generatelink="{$generatelink}"}" {if $nofollowForSearchFilterLinks} rel="nofollow" {/if} class="mk-labelx_check unchecked clearfix {$article_type|lower|trim|regex_replace:'/\W+/':''}-articletypes-filter" data-key="articletypes" data-value="{$article_type}"><span class="cbx"></span><span class="mk-filter-display">{$article_type|lower|capitalize} <span class="mk-filter-product-count">[{$count}]</span></span></a>
                            {/if}
                            {if $smarty.foreach.article_type.iteration == $minNonLazyFilters}
                                <script>
                            {/if}
                            {if $smarty.foreach.article_type.iteration >= $minNonLazyFilters}
                                Myntra.Data.lazyElems.push('<a href="{$http_location}/{myntralink url_desc="$url_desc_l" parent_landingpage_url="$query" global_attr_article_type_facet="$article_type" is_landingpage="y" generatelink="{$generatelink}"}" class="mk-labelx_check unchecked clearfix {$article_type|lower|trim|regex_replace:'/\W+/':''}-articletypes-filter" data-key="articletypes" data-value="{$article_type}"><span class="cbx"></span><span class="mk-filter-display">{$article_type|lower|capitalize} <span class="mk-filter-product-count">[{$count}]</span></span></a>' + Myntra.Data.lazySeparator + '.mk-articletypes-filters');
                            {/if}
                        {/foreach}
                            {if $smarty.foreach.article_type.iteration >= $minNonLazyFilters}
                                </script>
                            {/if}
                    </div>
                    </div>
                    </div>
                </div>
            {/if}
                        </li>
                    {/if}
    		        <li data-name="Sizes"  data-target="sizes-types" style="{if $sizes_filter && $sizes_filter_count gt 1}display:block;{else}display:none{/if}"><span class="anim-link prompt">Sizes</span><span class="mk-level1-arrownav"></span><span class="selected"></span>
    		        <div id="sizes-types" class="sizes-types filter-content link-details" style="display:none;" data-scrollinit="false">
                    <div class="filter-header">
                        <span class="mk-loader mk-f-left"></span>    
                        <span data-key="sizes" class="mk-clear-filter mk-f-right mk-hide">Reset</span>
                    </div>    
                    <div id="sizes-types-wrapper" class="filter-content-wrapper">
                    <div class="mk-filter-content">
                    <div class="mk-filter-wrapper mk-sizes-filters clearfix">
                            {foreach key=size item=count from=$faceted_search_results_new->sizes_facet}
                                <label class="mk-labelx_check unchecked clearfix {$size|lower|trim|regex_replace:'/\W+/':''}-sizes-filter" data-key="sizes" data-value="{$size}"><span class="cbx"></span>{$size|upper|replace:" ":""}</label>
                            {/foreach}
                    </div>
                    </div>
                    </div>
                </div>
    		        </li>
                    {if $brands_search_results && $brands_search_results_count gt 1}
                        <li data-name="Brands" id="brands-link" data-target="brands-types" data-isOptimized=true data-optimizedContainer="optimizedContainer"><span class="anim-link prompt">Brands</span><span class="mk-level1-arrownav"></span><span class="selected"></span>
                        {if $brands_search_results && $brands_search_results_count gt 1}
            	<div id="brands-types" class="brands-types filter-content link-details" style="display:none;" data-scrollinit="false">
                    <div class="filter-header">
                        <span class="mk-loader mk-f-left"></span>    
                        <span data-key="brands" class="mk-clear-filter mk-f-right mk-hide">Reset</span>
                    </div>    
                    <div class="mk-brands-filter-input" style="display:none;">
                        <input class="ui-autocomplete-input brands-autocomplete-input" value="SEARCH BRANDS" data-placeholder="SEARCH BRANDS" type="text" autocomplete="off">
                        <span class="mk-clear-brands-input"></span>
                    </div>
                    <div id="brands-types-wrapper" class="filter-content-wrapper">
                        <div class="mk-filter-content">
                            <div id="mk-brand-search-results" class="mk-filter-wrapper mk-brands-filters" style="display:none">
                            </div>
                            <div id="mk-brands" class="optimizedContainer mk-filter-wrapper mk-brands-filters ui-widget-content mk-resizable-box">
                            {foreach key=brand item=count from=$faceted_search_results_new->brands_filter_facet name="brands"}
                                {if $smarty.foreach.brands.iteration < $minNonLazyFilters}
                                <a href="{$http_location}/{$brand|lower|trim|replace:' ':'-'}" class="mk-labelx_check unchecked clearfix {$brand|lower|trim|regex_replace:'/\W+/':''}-brands-filter" data-key="brands" data-value="{$brand}"><span class="cbx"></span><span class="mk-filter-display">{$brand} <span class="mk-filter-product-count">[{$count}]</span></span></a>
                                {/if}
                                {if $smarty.foreach.brands.iteration == $minNonLazyFilters}
                                    <script>
                                {/if}
                                {if $smarty.foreach.brands.iteration >= $minNonLazyFilters}
                                    Myntra.Data.lazyElems.push('<a href="{$http_location}/{$brand|lower|trim|replace:' ':'-'}" class="mk-labelx_check unchecked clearfix {$brand|lower|trim|regex_replace:'/\W+/':''}-brands-filter" data-key="brands" data-value="{$brand}"><span class="cbx"></span><span class="mk-filter-display">{$brand} <span class="mk-filter-product-count">[{$count}]</span></span></a>' + Myntra.Data.lazySeparator + '.mk-brands-filters.mk-resizable-box');
                                {/if}
                            {/foreach}
                            {if $smarty.foreach.brands.iteration >= $minNonLazyFilters}
                                </script>
                            {/if}
                        </div>
                    </div>
                </div>
                </div>
            {/if}
                        </li>
                    {/if}
                    {if $discountPercentageBreakupCount gt 1}
                        <li data-name="Discount" data-target="discountpercentage-types"><span class="anim-link prompt">Discount</span><span class="mk-level1-arrownav"></span><span class="selected"></span>
                                    {if $discountPercentageBreakupCount gt 1}
            	<div id="discountpercentage-types" class="discountpercentage-types filter-content link-details" style="display:none;" data-scrollinit="false">
                    <div class="filter-header">
                        <span class="mk-loader mk-f-left"></span>    
                        <span data-key="discountpercentage" class="mk-clear-filter mk-f-right mk-hide">Reset</span>
                    </div>    
                    <div id="discountpercentage-types-wrapper" class="filter-content-wrapper">
                    <div class="mk-filter-content">
                    <div class="mk-filter-wrapper mk-discountpercentage-filters">
                    {if $discountPercentageBreakup}
                        {foreach item=discountpercengate from=$discountPercentageBreakup key=k}
                        <label class="mk-labelx_check unchecked clearfix discountpercentage-filter mk-{$k}-discountpercentage-filter" data-key="discountpercentage" data-value="{$k}">
                            <span class="rbtn"></span> {$k} % AND ABOVE
                        </label>
                        {/foreach}
                    {/if}    
                    </div>
                    </div>
                </div>
                </div>
            {/if} 
                        </li>
                    {/if}
                    {if $rangeMin neq $rangeMax}
                    	<li data-name="Price" data-target="pricerange-types"><span class="anim-link prompt">Price</span><span class="mk-level1-arrownav"></span><span class="selected"></span>
                    	            {if $rangeMin neq $rangeMax}
            	<div id="pricerange-types" class="pricerange-types filter-content link-details" style="display:none;" data-scrollinit="false">
                    <div class="filter-header">
                        <span class="mk-loader mk-f-left"></span>    
                        <span data-key="pricerange" class="mk-clear-filter mk-f-right mk-hide">Reset</span>
                    </div>    
                    <div id="pricerange-types-wrapper" class="filter-content-wrapper">
                    <div class="mk-filter-content">
                    <div class="mk-filter-wrapper mk-price-filters">
                    {if $priceBreakup}
                        {foreach item=range from=$priceBreakup}
                        <label class="mk-labelx_check unchecked clearfix price-range-filter mk-{$range.rangeMin}-{$range.rangeMax}-pricerange-filter" data-key="pricerange" data-rangeMin="{$range.rangeMin}" data-rangeMax="{$range.rangeMax}">
                            <span class="rbtn"></span>Rs. {$range.rangeMin|number_format:0:".":","} - Rs. {$range.rangeMax|number_format:0:".":","}
                        </label>
                        {/foreach}
                    {/if}    
                    </div>
                    {*<div class="mk-filter-wrapper mk-pricerange-filters">
                        <div class="mk-price-range" data-rangeMin="{$rangeMin}" data-rangeMax="{$rangeMax}"></div>
                        <div class="mk-amount"></div>
                    </div>*}
                    </div>
                </div>
                </div>
            {/if}
                    	</li>
                    {/if}
                    {if $fit_filters_arr && $fit_filters_arr_count gt 1}
                        <li data-name="Gender" data-target="gender-types"><span class="anim-link prompt">Gender</span><span class="mk-level1-arrownav"></span><span class="selected"></span>
                                    {if $fit_filters_arr && $fit_filters_arr_count gt 1}
            	<div id="gender-types" class="gender-types filter-content link-details" style="display:none;" data-scrollinit="false">
                    <div class="filter-header">
                        <span class="mk-loader mk-f-left"></span>    
                        <span data-key="gender" class="mk-clear-filter mk-f-right mk-hide">Reset</span>
                    </div>    
                    <div id="gender-types-wrapper" class="filter-content-wrapper">
                    <div class="mk-filter-content">
                    <div class="mk-filter-wrapper mk-gender-filters">
                    {foreach key=fit item=count from=$fit_filters_arr}
                    <label class="mk-labelx_check unchecked clearfix {$fit|lower|trim|regex_replace:'/\W+/':''}-gender-filter" data-key="gender" data-value="{$fit}"><span class="cbx"></span><span class="mk-filter-display">{if $fit|lower eq "boy" }Boys{elseif $fit|lower eq "girl"}Girls{else}{$fit|lower|capitalize}{/if}</span></label>
                    {/foreach}
                    </div>
                    </div>
                </div>
                </div>
            {/if}
                        </li>
                    {/if}
                    {if $colour_arr && $colour_arr_count gt 1}
                        <li data-name="Colour" data-target="colour-types"><span class="anim-link prompt">Colour</span><span class="mk-level1-arrownav"></span><span class="selected"></span>
                                    {if $colour_arr && $colour_arr_count gt 1}
            	<div id="colour-types" class="colour-types filter-content link-details" style="display:none;" data-scrollinit="false">
                    <div class="filter-header">
                        <span class="mk-loader mk-f-left"></span>    
                        <span data-key="colour" class="mk-clear-filter mk-f-right mk-hide">Reset</span>
                    </div>    
                    <div id="colour-types-wrapper" class="filter-content-wrapper">
                    <div class="mk-filter-content">
                    <div class="mk-filter-wrapper mk-colour-filters mk-resizable-box">
                    {foreach key=colour item=count from=$colour_arr}
                    <label class="mk-labelx_check unchecked clearfix {$colour|lower|trim|regex_replace:'/\W+/':''}-colour-filter {$bgPosition}" data-key="colour" data-value="{$colour}" data-swatch="{$colorSwatchMapping[$colour]}"><span class="cbx"></span><span class="mk-color-swatch" style="background-position:-588px -{math equation='((x-1) * 11) + ((x-1)*2)' x=$colorSwatchMapping[$colour]}px"></span><span class="mk-filter-display">{$colour|lower|capitalize}</span></label>
                    {/foreach}
                    </div>
                    </div>
                </div>
                </div>
            {/if}
                        </li>
                    {/if}
                    {if $article_type_attributes}
                        {foreach key=attribute item=props from=$article_type_attributes}
                            <li class="article_type_attr_placeholder" data-name="{$props.title}" data-target="{$props.title|replace:' ':'_'|regex_replace:'/\W+/':''}_article_attr-types"><span class="anim-link prompt">{$props.title}</span><span class="mk-level1-arrownav"></span><span class="selected"></span>
                                        {if $article_type_attributes}

            	<div id="{$props.title|replace:' ':'_'}_article_attr-types" class="{$props.title|replace:' ':'_'|regex_replace:'/\W+/':''}_article_attr-types article_type_attr filter-content link-details" style="display:none;" data-scrollinit="false">
                    <div class="filter-header">
                        <span class="mk-loader mk-f-left"></span>    
                        <span data-key="{$props.title|replace:' ':'_'}_article_attr" class="mk-clear-filter mk-f-right mk-hide">Reset</span>
                    </div>    
                    <div id="{$props.title|replace:' ':'_'}_article_attr-types-wrapper" class="filter-content-wrapper">
                    <div class="mk-filter-content">
                    <div class="mk-filter-wrapper mk-{$props.title|replace:' ':'_'}_article_attr-filters">
                    {foreach key=prop item=count from=$props.values}
                        {if $prop neq 'na'}
                        <label class="mk-labelx_check unchecked clearfix {$prop|lower|replace:' ':'-'|regex_replace:'/\W+/':''}-ata-filter" data-key="{$props.title|replace:' ':'_'}_article_attr" data-value="{$prop}"><span class="cbx"></span><span class="mk-filter-display">{$prop|lower|capitalize}</span></label>
                        {/if}
                    {/foreach}
                    </div>
                    </div>
                </div>
                </div>                   
            {/if}
                            </li>
                        {/foreach}
                    {/if}
                </ul>
            </div>
            </div>
            </div>
         </div>
         </div>
