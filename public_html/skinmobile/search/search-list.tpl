    <ul class="mk-cf mk-prod-list">
        {foreach name=widgetdataloop item=widgetdata from=$products}
        <li class="mk-product mk-product-style-details" data-id="prod_{$widgetdata.id}">
                <a style="display:block;" class="clearfix" href="{if $widgetdata.landingpageurl}{$http_location}/{$widgetdata.landingpageurl}{/if}{if $nav_id}?nav_id={$nav_id}{/if}" title="{$widgetdata.product}">
                {*<img {if $smarty.foreach.widgetdataloop.index gt 3}class="jquery-lazyload" original="{myntraimage key="style_180_240" src=$widgetdata.searchimagepath}" {/if} src="{if $smarty.foreach.widgetdataloop.index gt 3}{$cdn_base}/skin2/images/loader_180x240.gif{else}{myntraimage key="style_180_240" src=$widgetdata.searchimagepath}{/if}" alt="{$widgetdata.product}">*}

                                <img src="{myntraimage key="style_180_240" src=$widgetdata.searchimagepath}" alt="{$widgetdata.product}">
                <span class="mk-prod-brand-name">{*{$current_page} - prod_{$widgetdata.id} - *}{$widgetdata.brands_filter_facet}</span>
                {*<span class="mk-prod-name">{$widgetdata.product|strip|truncate:53}</span>*}
                <span class="mk-prod-price red">{$rupeesymbol}{if $widgetdata.discount > 0}{$widgetdata.discounted_price|number_format:0:".":","} <span class="strike gray">{$widgetdata.price|number_format:0:".":","}</span>{else}{$widgetdata.price|number_format:0:".":","}{/if}</span>
                {if $widgetdata.discount_label}<span class="mk-discount red">{include file="string:{$widgetdata.discount_label}"}</span>{/if}
                <span class="mk-prod-sizes tooltip-content">{if $widgetdata.sizes neq '' && $widgetdata.sizes neq ' '}{$widgetdata.sizes|replace:",":", "}{else}SOLD OUT{/if}</span>
            </a>
        </li>
        {/foreach}
    </ul>
