<div class="mod">
	<div class="hd">Sort By<div class="close-wrapper"><span class="btn-close"></span></div></div>
	<div class="bd mk-cf">
		<div class="mk-product-filters">
			<ul class="mk-product-sort">  <!-- Note: different option? -->
				<li class="mk-sort-popular {if $sort_param_in_url eq 'POPULAR' || $sort_param_in_url eq ''}mk-sort-selected{/if}"><span class="opt"></span><a href="{$http_location}/{$query}/POPULAR/{$current_page}" rel="/{$query}/POPULAR/{$current_page}" class="popular-sort-filter" data-sort="POPULAR" data-displayname="Popularity">Popularity</a></li>
				<li class="mk-sort-discount {if $sort_param_in_url eq 'DISCOUNT'}mk-sort-selected{/if}"><span class="opt"></span><a href="{$http_location}/{$query}/DISCOUNT/{$current_page}" rel="/{$query}/DISCOUNT/{$current_page}" class="discount-sort-filter" data-sort="DISCOUNT" data-displayname="Discount">Discount</a></li>
				<li class="mk-sort-recency {if $sort_param_in_url eq 'RECENCY'}mk-sort-selected{/if}"><span class="opt"></span><a href="{$http_location}/{$query}/RECENCY/{$current_page}" rel="/{$query}/RECENCY/{$current_page}" class="recency-sort-filter" data-sort="RECENCY" data-displayname="What's New">What's New</a></li>
				<li class="mk-sort-pricea {if $sort_param_in_url eq 'PRICEA'}mk-sort-selected{/if}"><span class="opt"></span><a href="{$http_location}/{$query}/PRICEA/{$current_page}" rel="/{$query}/PRICEA/{$current_page}" class="pricea-sort-filter" data-sort="PRICEA" data-displayname="Price L - H">Price Low to High</a></li>
				<li class="mk-sort-priced {if $sort_param_in_url eq 'PRICED'}mk-sort-selected{/if}"><span class="opt"></span><a href="{$http_location}/{$query}/PRICED/{$current_page}" rel="/{$query}/PRICED/{$current_page}" class="priced-sort-filter" data-sort="PRICED" data-displayname="Price H - L">Price High to Low</a></li>
			</ul> <!-- End .product-sort -->
		</div>		
	</div>
	</div>
