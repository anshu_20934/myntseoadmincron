{include file="inc/doctype.tpl"}
<head
	prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# {$facebook_app_name}: http://ogp.me/ns/fb/{$facebook_app_name}#">
{include file="inc/head-meta.tpl"} {include file="inc/head-css.tpl"}
{include file="inc/head-script.tpl"} {block name=pagecss}{/block}
</head>
<body>
	{include file="inc/nav.tpl"}
	{include file="inc/gtm.tpl"}
	<div class="m-body">

		<div class="mk-page-container {if $fluid}fluid{/if}">
			<div class="mk-page">
				{include file="inc/header.tpl"}
				{block name=body}{/block} 
				{block name=footer} {include file="inc/footer.tpl"} {/block}
			</div>
		</div>
		<div class="mk-body-overlay hide"></div>
		{block name=rightmenu}
			<div class="main-slide-wrapper inheader">
				{block name=loginRightmenu}
					{if !$login}
    					<div id="lb-login" class="content-slide-wrapper login-wrapper loginbox content-inactive">
        					{include file="inc/mod-login.tpl"}
    					</div>
					{/if}
				{/block}
				{block name=searchpageRightmenu}{/block}
				{block name=cartpageRightmenu}{/block} 
				{block name=pdppageRightmenu}{/block}     					
			</div>
		{/block}
	</div>
	{block name=lightboxes}{/block}
	{*if not $login and $currentPage neq "/register.php"} 
		{include file="inc/modal-signup.tpl"} 
	{/if*} 
	{include file="inc/search.tpl"}
	{include file="inc/body-css.tpl"} {include file="inc/body-script.tpl"}
	{block name=pagejs}{/block}
        {block name=macros}{/block}
</body>
</html>
