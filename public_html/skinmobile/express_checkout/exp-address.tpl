{if $hasAddress}

<div class="default-address">
	
	<address>
            <span class="address-name"><em>{$address.name}</em><a href="{$https_location}/checkout-address.php?change=1&expressbuy" onclick="_gaq.push(['_trackEvent', 'address', 'edit',window.location.toString()]);return true;" class="blue-text" id="address-change">Change Shipping address</a></span>
          {$address.address|nl2br}</br>
          {if $address.locality}{$address.locality}<br>{/if}
          {$address.city} - {$address.pincode}, {$address.statename}<br>
          <span class="lbl">Mobile:</span> {$address.mobile}
    </address>
</div>
{else}
<div class="address-form-wrap">
</div>
{/if}