<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/env/Host.config.php';
$geoDocumentRoot = HostConfig::$documentRoot;

require_once "$geoDocumentRoot/include/class/MClassLoader.php";

MClassLoader::addToClassPath("$geoDocumentRoot/include/class/");
MClassLoader::addToClassPath("$geoDocumentRoot/include/dao/class/");
MClassLoader::addToClassPath("$geoDocumentRoot/env/");
