<?php
	chdir(dirname(__FILE__));
	include_once("./auth.php");
	include_once("$xcart_dir/include/func/func.mkcore.php");
	include_once("$xcart_dir/include/func/func.order.php");
	include_once ("$xcart_dir/modules/coupon/database/CouponAdapter.php");
	include_once ("$xcart_dir/modules/coupon/CouponValidator.php");
	include_once ("$xcart_dir/include/class/class.feature_gate.keyvaluepair.php");
	include_once ("$xcart_dir/include/class/class.widget.keyvaluepair.php");
	include_once ("$xcart_dir/include/class/oms/EventCreationManager.php");
	include_once ("$xcart_dir/securesessioncheck.php");
	
		/*The below fucction checks for the user secure session expiry and then returns the flag PROMPT_LOGIN*/
	global $isMobile;
	if(!$isMobile){
		isSecureSessionExpired();
	}


	$sessionCaptcha = $XCART_SESSION_VARS['captchasAtMyntra']['codVerificationPage'];
	
	if(empty($sessionCaptcha) && empty($_POST['userinputcaptcha']) && empty($_POST['adminconfirmed']) && empty($_GET['adminconfirmed'])) {
		// We discard this check in case order is being admin verified.
		echo 'WrongCaptcha'; exit;
	}

	$isAdminConfirmed = 'false';
	$captchaVerified = 'false';
	
	$confirmmode = $_POST['confirmmode']; // This is coming from codVerificationCaptcha.tpl

    // check for captcha in COD confirmation for online and not for admin(telesales)
	if($_POST['condition']=='clickverifycaptcha' && empty($_POST['adminconfirmed']) && empty($_GET['adminconfirmed'])) {
		$userinputCaptcha = $_POST['userinputcaptcha'];
		
		if(!empty($userinputCaptcha)) {
			//check for QA automated user
			$qausers = WidgetKeyValuePairs::getWidgetValueForKey('automated.captcha.verification.user');
			//check if the login is present in the WidgetKeyvaluepair
			$isqauser = false;
			$isqauser = strpos($qausers, ','.$login.',');
			$qacaptchacode = WidgetKeyValuePairs::getWidgetValueForKey('automated.captcha.verification.captchacode');
			if(($isqauser !== false ) && (strpos($login,'@myntra.com')) && !empty($qacaptchacode) &&  ($qacaptchacode == $userinputCaptcha)){
		    	$captchaVerified = 'true';
		    	echo "CaptchaConfirmed";
				exit;
			}
		    else if (empty($sessionCaptcha) || ($userinputCaptcha != $sessionCaptcha)) {
		        $captchaVerified = 'false';
		        echo 'WrongCaptcha'; exit;
		    } else {
		    	$captchaVerified = 'true';
		    	echo "CaptchaConfirmed";
				exit;
		    }
		}
	}

    // request for order placement from admin(telesales etc)
	if ($_GET['adminconfirmed']=='true') {

        // set confirm mode
		$confirmmode = $_GET['confirmmode'];
		$isAdminConfirmed = 'true';
		$captchaVerified = 'true';

        // set captcha token
		if($confirmmode=='pptocodqueued') {
			$captchaToken = 'PP-COD-Admin-Verified';
		} else if($confirmmode=='pvtocodqueued') {
			$captchaToken = 'PV-COD-Admin-Verified';
		} else if($confirmmode=='declinedtocodqueued') {
			$captchaToken = 'DECLINED-COD-Admin-Verified';
		} else if($confirmmode=='telesalesdirectcod') {
			$captchaToken = 'TELESALES-ORDER-PLACED-COD-Admin-Verified';
		}else if($confirmmode=='telesalesconverttocod') {
			$captchaToken = 'TELESALES-ORDER-CONVERT-COD-Admin-Verified';
		} else {
			echo "WrongOrder"; exit;
		}

        // set orderid(order would have already been placed in the case) and login
		$codorderid = mysql_real_escape_string(filter_input(INPUT_GET,"codorderid", FILTER_SANITIZE_NUMBER_INT));
		$codloginid = mysql_real_escape_string(filter_input(INPUT_GET,"codloginid", FILTER_SANITIZE_EMAIL));
		
		$order_result = func_query("SELECT * FROM xcart_orders WHERE orderid='". $codorderid ."'");
		if(empty($codloginid)) {
			$codloginid = $order_result[0]['login'];
		}
		if($confirmmode=='declinedtocodqueued') {
			$couponCode = $order_result[0]['coupon'];
			$cashCouponCode = $order_result[0]['cash_coupon_code'];
			$cashdiscount = $order_result[0]['cash_redeemed'];
			
			if(!empty($couponCode) || !empty($cashCouponCode))
			{
				$adapter = CouponAdapter::getInstance();
				$validator = CouponValidator::getInstance();
			}
			
			if(!empty($couponCode))
			{
			    $couponMessage = "";
		
			    // Find whether the coupon is still valid for the user.
			    $isValid = false;
				try {
		            $coupon = $adapter->fetchCoupon($couponCode);
				    $isValid = $validator->isCouponValidForUser($coupon, $codloginid);
				}
				catch (CouponException $ex) {
				    $couponMessage = $ex->getMessage();
				    $couponMessage = "<font color=\"red\">$couponMessage</font>";
				}
			    
				//
				// If coupon is invalid, then clean up the coupon 
				// and redirect back to the cart page.
				//
				if (!$isValid) {
				    // XXX: Must find a way to notify the user.
				    // Currently, we are displaying the couponMessage.
				    echo $couponMessage; exit;
				}
			}
			if(!empty($cashCouponCode))
	        {
	            $couponMessage = "";
	
	            // Find whether the coupon is still valid for the user.
	            $isValid = false;
                try {
                    $coupon = $adapter->fetchCoupon($cashCouponCode);
                    $isValid = $validator->isCouponValidForUser($coupon, $codloginid);
                }
                catch (CouponException $ex) {
                    $couponMessage = $ex->getMessage();
                    $couponMessage = "<font color=\"red\">$couponMessage</font>";
                }

                //
                // If coupon is invalid, then clean up the coupon
                // and redirect back to the cart page.
                //
                if (!$isValid) {
                    // XXX: Must find a way to notify the user.
                    // Currently, we are displaying the couponMessage.
                    echo $couponMessage; exit;
                }
	        }
	        
			if(!empty($couponCode)) {
				# Update the number of times the coupon has been used once the order is placed
				$adapter->lockCouponForUser($couponCode, $codloginid);
			}
			
			if(!empty($cashCouponCode)) {
                # Update the number of times the coupon has been used once the order is placed
                $adapter->lockCouponForUser($cashCouponCode, $codloginid);
        	}
	        
		}
	} else {
		
		$codorderid = mysql_real_escape_string(filter_input(INPUT_POST,"codorderid", FILTER_SANITIZE_NUMBER_INT));
		$codloginid = mysql_real_escape_string(filter_input(INPUT_POST,"codloginid", FILTER_SANITIZE_EMAIL));
		// We are not using Zip dial any more. We might as well use this to store user input Captcha, and the text generated by captcha
		$captchaToken = "SESSION:$sessionCaptcha-USER:$userinputCaptcha";
	}
	
	if(empty($codorderid) || empty($codloginid)) {
		echo "WrongOrder";
		exit;
	}
	
	$queryOrderAlreadyConfirmed = "select * from cash_on_delivery_info where order_id='" . $codorderid .  "' and login_id='" . $codloginid . "' and order_confirmed='Y'";
	$resulCheckOrderConfirmed = db_query($queryOrderAlreadyConfirmed);
	$confirmedOrder = db_num_rows($resulCheckOrderConfirmed);
	
	if($confirmedOrder>=1) {
		echo "OrderAlreadyConfirmed";
		exit;
	}
	
	
	if($confirmmode=='pptocodqueued' || $confirmmode=='declinedtocodqueued' || $confirmmode=='telesalesconverttocod' || $confirmmode=='telesalesdirectcod') {
		// Do nothing. PP or Declined orders will be converted to Q or OH as a COD order.
		
	} else {
		/* We need to confirm that order being confirmed is the last one placed by the user.
		 * A simple check for that, is to see if the orderid, is the maximum orderid in xcart_orders table for cod payment_method.
		 * This way, we can get rid of confusion of confirming multiple orders by same login_id simultaneously.
		 */
		$queryMaxorderid = "select max(orderid) as maxid from xcart_orders where login='" . $codloginid . "' and payment_method='cod' group by login";
		$resultMaxorderid = db_query($queryMaxorderid);
	
		$result2 = db_fetch_array($resultMaxorderid);
		$maxorderid = $result2['maxid'];
		
		// There is already some other order placed by user. That latest orderid is known from $maxorderid value.
		if(!empty($maxorderid) && $maxorderid!=$codorderid) {
			// A simple check for maxorder and current order id, will ensure that we are verifying the latest placed order only
			echo "WrongOrder";
			exit;
		}
	}

	$mobileno = mysql_real_escape_string($_POST['mobile']);
	
	if(empty($mobileno)) {
		$mobileno = func_query_first_cell("select mobile from xcart_orders where orderid='$codorderid'");
	}
	
	$randomFourDigitCode = rand(1000, 9999);
	
	// We are saving this cookie value in DB for Cash on delivery Email verification purposes.
	$uuid = stripslashes(getMyntraCookie('MYNTRA_SHOPPING_ID'));
	if(empty($uuid)) {
		$uuid = 'MYNTRACOD';
	}
	
	$cartData = "null";
	
	if($confirmmode!='pptocodqueued') {
		// Do nothing. Cart data remains null;
		
		$retrieveCartDataInCODQuery = "select cart_data, shopping_cookie, contact_number from cash_on_delivery_info where order_id='$codorderid' group by order_id";
		$retrieveCartfromCOD = func_query_first($retrieveCartDataInCODQuery);
		
		if(!empty($retrieveCartfromCOD['cart_data'])) {
			// Retrieving from first entry cash_on_delivery_info table. Using cart_data from mk_shopping_cart would be wrong here.
			$cartData = "'" . mysql_real_escape_string($retrieveCartfromCOD['cart_data']) . "'";
			$uuid = $retrieveCartfromCOD['shopping_cookie'];
			$mobileno = $retrieveCartfromCOD['contact_number'];
		} else {
			// First insertion, we need to fetch cart_data from mk_shopping_cart to store as snapshot.
			$shoppingCartQuery = "select cart_data from mk_shopping_cart where cookie='$uuid'";
			$cartdataResults = func_query_first($shoppingCartQuery);
			
			if(!empty($cartdataResults['cart_data'])) {
				$cartData = "'" . mysql_real_escape_string($cartdataResults['cart_data']) . "'";
			}
		}
		
		if(empty($cartData)) {
			echo "WrongOrder";
			exit;
		}
	}
	
	$removeCODNoCaptchaDataSQL = " delete from cash_on_delivery_info where order_id='$codorderid' ";
	$removeCODNoCaptchaDataSQL.= " and login_id='$codloginid' and zd_transactiontoken='CaptchaNotVerified'";
	db_query($removeCODNoCaptchaDataSQL);
	
	// Mark all entries for the particular orderid, to 'C'ancelled state.
	$query = "update cash_on_delivery_info set order_confirmed='C' where order_id='$codorderid' and login_id='$codloginid'";
	db_query($query);
	
		
	if($captchaVerified=='false') {
		// Insert an entry inside the cash_on_delivery_info table, which maps the orderid/loginid to the verification code, and mobile no.
		// order_confirmed column value is set to default 'N'
		$query = "insert into cash_on_delivery_info (order_id, login_id, verification_code, contact_number, shopping_cookie, order_confirmed, zd_transactiontoken, cart_data) values (";
		$query = $query . "$codorderid, '$codloginid', $randomFourDigitCode, '$mobileno', '$uuid', 'N', '$captchaToken', $cartData)";
		
		// An insert query should happen whenever a wrong captcha is entered.
		db_query($query);
		echo 'WrongCaptcha'; exit;
		
	} else if($captchaVerified=='true') {
		
		$query = "insert into cash_on_delivery_info (order_id, login_id, verification_code, contact_number, shopping_cookie, order_confirmed, zd_transactiontoken, cart_data) values (";
		$query.= "$codorderid, '$codloginid', $randomFourDigitCode, '$mobileno', '$uuid', 'Y', '$captchaToken', $cartData)";
		db_query($query);
		
		/* On captcha verification, we do the following things now:
		 * (2) In column cod_pay_status, we set the value as 'pending' to indicate that order is shipping and payment is in pending state
		 * (3) The cod_pay_status is updated to 'Paid', 'Unpaid' or 'WrittenOff' status by OPERATIONS guys based on payments received, through admin panels
		 * (4) cod_pay_status = 'Unpaid' when item is returned back to myntra.
		 * (5) The last used mobile number for the sms verification could be different from what is stored in customer shipping address.
		 *     So we need to update that issues_contact_number too in xcart_orders table with the last mobile number on which sms verification was sent.
		 */
		$codCharge = FeatureGateKeyValuePairs::getInteger('codCharge');
		//additional cod charge
		if($codCharge>0) {
			$codCharge = $codCharge;
		} else {
			$codCharge = 0;
		}
			
		$query = "update xcart_orders set cod_pay_status='pending', issues_contact_number='$mobileno',cod='$codCharge' where orderid = '$codorderid'";
		db_query($query);
		
		if($isAdminConfirmed=='true') {
			$adminlogin = $XCART_SESSION_VARS['identifiers']['A']['login'];
			
			if($confirmmode=='pptocodqueued' || $confirmmode=='declinedtocodqueued' ||  $confirmmode=='telesalesconverttocod' || $confirmmode=='telesalesdirectcod') {
				if($confirmmode=='pptocodqueued'){ 
					func_addComment_order($codorderid, $adminlogin, 'PP to COD', 'Order Type Change PP to COD', 'PP order converted to COD and status changed to Queued');
				} else if($confirmmode=='declinedtocodqueued'){
					func_addComment_order($codorderid, $adminlogin, 'Declined to Queued', 'COD Order Confirmed', 'Order status changed from Declined to Queued');
				}else if ($confirmmode=='telesalesconverttocod'){
					func_addComment_order($codorderid, $adminlogin, 'telesales converted to COD', 'COD Order Confirmed', 'Order status changed from PP to Queued');
					db_query("insert into telesales_logs (orderid,cclogin,action) values('$codorderid','$adminlogin','CONVERT')");
				}else if ($confirmmode=='telesalesdirectcod'){
					func_addComment_order($codorderid, $adminlogin, 'telesales created COD Order', 'COD Order Confirmed', 'COD Order created');
					db_query("insert into telesales_logs (orderid,cclogin,action) values('$codorderid','$adminlogin','PLACED')");
				}
	    		
	    		$query = "UPDATE xcart_orders set payment_method='cod', changerequest_status = 'Y' WHERE orderid = '$codorderid';";
				db_query($query);

				EventCreationManager::pushCompleteOrderEvent($codorderid);
				
				// mkorderBook.php checks for payment_gateway_name and does some operations if it is 'ebs' or 'ccavenue'
				// We need to bypass those checks, as they interfere with COD flow. So we run this update query.
				$query = "UPDATE mk_payments_log set payment_gateway_name=concat(payment_gateway_name,'-cod-change') where orderid='$codorderid'";
				db_query($query);
				
			} else if($confirmmode=='pvtocodqueued') {
	    		func_addComment_order($codorderid, $adminlogin, 'PV to Queued', 'COD Order Confirmed', 'Order status changed from PV to Queued');
	    		
	    		$query = "UPDATE xcart_orders set changerequest_status = 'Y' WHERE orderid = '$codorderid';";
				db_query($query);
			}			
			// We need to redirect to order confirmation page directly
			$url = "$http_location/mkorderBook.php?type=cod&orderid=$codorderid&admincodloginid=$codloginid";
			if(!empty($confirmmode)) { 
				$url.= "&codconfirmmode=" . $confirmmode ;
			}
			header("Location: $url");
			exit;
		}
				
		// We can echo some result for javascript in codverification.tpl to verify response.
		echo "CaptchaConfirmed";
		exit;
				
	}
?>