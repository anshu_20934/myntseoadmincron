<?php

/**
 * Code specific to cart page for the PTG campaign.
 * 
 * @author Rohan.Railkar
 */

//
// Get the list of product type ids qualifying for the campaign PTG.
// We do not store PTG with the product in the session. So, we gotta make
// a query.
//
global $system_ptg_for_campaign;
$query = "SELECT DISTINCT id
    			FROM mk_product_type 
    		   WHERE product_type_groupid = '$system_ptg_for_campaign';";

$result = db_query($query);
$allowed_pts = array();
if ($result) {
    while ($row = db_fetch_row($result)) {
        array_push($allowed_pts, $row[0]);
    }
}

// Calculate after discount price of the footwear.
$ptg_price = 0.0;
foreach ($myCart->getProducts() as $cartProduct) {
    if (in_array($cartProduct->getProductTypeId(), $allowed_pts)) {
        $ptg_price +=
        ($cartProduct->getTotalProductPrice()
        + $cartProduct->getTotalAdditionalCharges()
        - $cartProduct->getDiscount());
    }
}

// Check if the amount qualifies for the scheme.
global $system_ptg_campaign_threshold;
global $system_ptg_campaign_threshold_calc;
if ($ptg_price >= $system_ptg_campaign_threshold) {
    $qualified_for_ptg_campaign = true;

    // Set the flag.
    x_session_register("qualified_for_ptg_campaign");
    $smarty->assign("ptg_qualify", true);
    $smarty->assign("ptg_threshold", $system_ptg_campaign_threshold);
}
else if ($ptg_price >= $system_ptg_campaign_threshold_calc) {
     $qualified_for_surprise_ptg_campaign = true;

    // Set the surprise flag.
    x_session_register("qualified_for_surprise_ptg_campaign");
    // Clear the flag.
    x_session_unregister("qualified_for_ptg_campaign");
    unset($XCART_SESSION_VARS["qualified_for_ptg_campaign"]);
    $smarty->assign("ptg_not_qualify", true);
    $smarty->assign("ptg_threshold", $system_ptg_campaign_threshold);
}
else {

    // Clear the flag.
    x_session_unregister("qualified_for_ptg_campaign");
    unset($XCART_SESSION_VARS["qualified_for_ptg_campaign"]);
     x_session_unregister("qualified_for_surprise_ptg_campaign");
    unset($XCART_SESSION_VARS["qualified_for_surprise_ptg_campaign"]);
    $smarty->assign("ptg_not_qualify", true);
    $smarty->assign("ptg_threshold", $system_ptg_campaign_threshold);
}
