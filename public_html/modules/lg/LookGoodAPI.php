<?php
#use lg;
include_once (dirname(__FILE__)."/../RestAPI/RestRequest.php");
include_once $xcart_dir."/include/func/func.mail.php";


/*
 * Class to contain the code for GiftCards implementation
 */
class LookGoodAPI {
	
	public static function getLookBook($lookBookID) {

		$url = HostConfig::$lookGoodServiceHost.'/lg/lookbook/'.$lookBookID;
		$restRequest = new RestRequest($url, 'GET');
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody);
        if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
        
		return $returnObject;
	}
	
	public static function getStarNStyle($starNStyleId) {

		$url = HostConfig::$lookGoodServiceHost.'/lg/starnstyle/'.$starNStyleId;
		$restRequest = new RestRequest($url, 'GET');
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody);
        if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
        
		return $returnObject;
	}
	
	
	public static function getAllStarNStyle() {

		$url = HostConfig::$lookGoodServiceHost.'/lg/starnstylelist/';
		$restRequest = new RestRequest($url, 'GET');
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody);
        if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
        
		return $returnObject;
	}
	
public static function getStarDiary($starId) {

		$url = HostConfig::$lookGoodServiceHost.'/lg/starnstyle/'.$starId;
		$restRequest = new RestRequest($url, 'GET');
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody);
        if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
        
		return $returnObject;
	}
	
	
	
}

