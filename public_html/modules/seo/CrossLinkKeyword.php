<?php

/**
 * Created by Arun Kumar K
 * Date: 19 Apr, 2011
 * purpose:to provide all abstract functionalities for managing crosslinking keywords
 */
include_once($xcart_dir."/include/func/func.db.php");
include_once($xcart_dir."/include/func/func.utilities.php");

class CrossLinkKeyword{
        
    function __construct(){}

    /* adds keyword
     * @param:(array)$queryData - array of promo to be inserted
     * @return:(int)last insert id/(bool)false
     */
    public function addKeyword($queryData){
        return func_array2insert('mk_crosslink_keyword',sanitizeDBValues($queryData));
    }

    /* updates keyword based on condition
     * @param:(array)$queryData - array of promo to be update
     * @return:void
     */
    public function updateKeyword($queryData,$condition){
        func_array2update('mk_crosslink_keyword', sanitizeDBValues($queryData),$condition);
    }

    public function deleteKeyword($id){        
        db_query("delete from mk_crosslink_keyword where id = '".sanitizeDBValues($id)."'");
    }

    /* get keyword based on condition(filter)
     * @param:(array)$filter - array of filters
     * @param:(array)$order - array for order by
     * @param:(array)$offset - start point of result set
     * @param:(array)$limit - count of records
     * @return:(array)$SEMPromoInfo
     */
    public function getKeyword($filter=NULL,$order=NULL,$offset=NULL,$limit=NULL){
        $sql = "select id, crosslink_keyword, crosslink_keyword_url, is_active from mk_crosslink_keyword";
        $promo = sanitizedQueryBuilder($sql, $filter, $order, $offset, $limit);        
        return $promo;
    }
    
    public function getKeywordCount($keyword="") {
		$query ="select count(*) as cnt from  mk_crosslink_keyword ";
		if(!empty($keyword))  $query .= "where crosslink_keyword like '%$keyword%'";
    	$res = func_query($query);
    	return $res[0]['cnt'];
    	
    }
	public function searchKeyword($keyword, $filter=NULL,$order=NULL,$offset=NULL,$limit=NULL) {
        $sql = "select id, crosslink_keyword, crosslink_keyword_url, is_active from mk_crosslink_keyword where  crosslink_keyword like '%".$keyword."%'";
        $promo = sanitizedQueryBuilder($sql, $filter, $order, $offset, $limit);        
        return $promo;
    }
	
	}
