<?php
namespace contactus;

/**
 * The classs exposes all exceptions related to myntra contact us
 * @author: Arun Kumar K 
 */

require_once(\HostConfig::$documentRoot."/exception/MyntraException.php");

class MContactUsException extends \MyntraException
{
    private static $M_CONTACTUS_EXCEPTIONS = array(
        
        'NULL_ISSUE_VALUE' => 'Please enter a valid issue name',
        'PARENT_ISSUE_MISSING' => 'Parent issue for the current issue is missing',

        'DUPLICATE_ISSUE_AT_LEVEL_1' => 'Duplicate issue name at level 1, please enter other name',
        'DUPLICATE_ISSUE_AT_LEVEL_2' => 'Duplicate issue name at level 2, please enter other name',
        'DUPLICATE_ISSUE_AT_LEVEL_3' => 'Duplicate issue name at level 3, please enter other name',
        'ISSUE_NOT_CREATED' => 'Issue can not be created, please try again',

        'NO_ISSUE_TO_UPDATE' => 'Problem in updating the issue, please try again',
        'ISSUE_RENAME_TO_OTHER' => 'An issue can not be renamed as "Other/Others"',

        //can add anymore exception key=>value pairs here ...

        );
	/**
	 * Constructor.
	 *
	 * @param String $message Error message.
	 * @param int $code Error code.
	 */
	public function __construct($keyException){        
	    parent::__construct(self::$M_CONTACTUS_EXCEPTIONS[$keyException]);
	}


}
