<?php
namespace contactus;
/**
 * The classs exposes abstract APIs for handling myntra contact-us (issues).
 * @author: Arun Kumar K
 * legends:
 *      DAO - data access object
 *      issue - issues are nothing but issue categories for customer to contact myntra on
 */
use contactus\dao\MContactUsDAO;

class MContactUsIssueManager{

    private $mContactUsDAO;

    
    public function __construct(){
        //initialize DAO
        $this->mContactUsDAO = new MContactUsDAO();

    }



    /*
     * adds a new isssue with its fields with making necessary arrangements for issue levels ans fields
     * @param:(string)$newIssue - new issue to be created
     * @param:(string)$issueLevel_1 - higher level issue selected
     * @param:(string)$issueLevel_2 - second higher level issue selected
     * @param:(int)$active - is need to be active
     * @param:(array) - array of fields in key=>value pair
     * @return:(int)$issueId
     * @throws:(MContactUsException)$e
     */
    public function addContactUsIssue($newIssue, $issueLevel_1=null, $issueLevel_2=null, $issueEmail=null, $issueAction=null, $active=1,
                                      $fieldName=array(), $fieldType, $maxchar=array(), $fieldAction=array(), $activeField){

        
        if(empty($newIssue)){            
            throw new MContactUsException("NULL_ISSUE_VALUE");
        } else {
            $newIssue = trim($newIssue);
        }
        
        if(empty($issueLevel_1)){
            $issueLevel_1 = null;
        } else {
            $issueLevel_1 = trim($issueLevel_1);
        }

        if(empty($issueLevel_2)){
            $issueLevel_2 = null;
        } else {
            $issueLevel_2 = trim($issueLevel_2);
        }

        if(empty($issueEmail)){
            $issueEmail = null;
        } else {
            $issueEmail = trim($issueEmail);
        }

        if(empty($issueAction)){
            $issueAction = null;
        } else {
            $issueAction = trim($issueAction);
        }

        if(!isset($active)){
            $active = 0;
        } else {
            $active = 1;
        }

        $issueLevel = $this->getIssueLevelFromUserData($issueLevel_1, $issueLevel_2);        

        switch($issueLevel){
            case "level_1"  :   //new issue going to be the level 1 issue
                                $this->checkIssueLevelUniqueness($newIssue, $issueLevel_1, $issueLevel_2, $issueLevel);
                                $issueId = $this->mContactUsDAO->insertContactUsIssue($newIssue, $issueLevel_1,
                                                            $issueLevel_2, $issueEmail, $issueAction, $active );
                                break;

            case "level_2"  :   //new issue going to be the level 2 issue
                                $this->checkIssueLevelUniqueness($issueLevel_1, $newIssue, $issueLevel_2, $issueLevel);
                                $issueId = $this->mContactUsDAO->insertContactUsIssue($issueLevel_1, $newIssue,
                                                            $issueLevel_2, $issueEmail, $issueAction, $active );
                                break;

            case "level_3"  :   //new issue going to be the level 3 issue
                                $this->checkIssueLevelUniqueness($issueLevel_1, $issueLevel_2, $newIssue, $issueLevel);
                                $issueId = $this->mContactUsDAO->insertContactUsIssue($issueLevel_1, $issueLevel_2,
                                                            $newIssue, $issueEmail, $issueAction, $active );
                                break;
        }

        if(!$issueId){
            throw new MContactUsException("ISSUE_NOT_CREATED");
        }

        // insert issue fields only in case the issue does not read as 'other' or 'others'(case insensitive)
        if(strtolower($newIssue) != 'other' && strtolower($newIssue) != 'others' && is_array($fieldType)){
            foreach($fieldType as $idx=>$issueFieldType){

                $fieldName[$idx] = trim($fieldName[$idx]);
                if(empty($fieldName[$idx])){
                    $fieldName[$idx] = null;
                }

                $maxchar[$idx] = trim($maxchar[$idx]);
                if(empty($maxchar[$idx])){
                    $maxchar[$idx] = null;
                }

                $fieldAction[$idx] = trim($fieldAction[$idx]);
                if(empty($fieldAction[$idx])){
                    $fieldAction[$idx] = null;
                }

                if(!isset($activeField[$idx])){
                    $activeField[$idx] = 0;
                } else {
                    $activeField[$idx] = 1;
                }

                $this->mContactUsDAO->insertContactUsIssueField($issueId, $issueFieldType, $fieldName[$idx],
                                                                $maxchar[$idx], $fieldAction[$idx], $activeField[$idx]);
            }
        }
        return $issueId;
    }

    public function editContactUsIssue($issueId, $editIssue, $issueEmail, $issueAction, $isActiveIssue,
                            $fieldIdEdit, $fieldNameEdit=array(), $fieldTypeEdit, $maxcharEdit=array(), $fieldEditAction=array(), $isActiveFieldEdit,
                            $fieldName=array(), $fieldType, $maxchar=array(), $fieldAction=array(), $isActiveField){

        if(empty($issueId)){
            throw new MContactUsException("NO_ISSUE_TO_UPDATE");
        }
        
        if(empty($editIssue)){
            throw new MContactUsException("NULL_ISSUE_VALUE");
        } else {            
            $editIssue = trim($editIssue);
        }        

        if(!isset($isActiveIssue)){
            $isActiveIssue = 0;
        } else {
            $isActiveIssue = 1;
        }

        $issueDetail = $this->getContactUsIssueWithField($issueId, false, false);
        $issueDetail = $issueDetail['issueArray'];
        
        $issueLevel = $this->getIssueLevelFromDB($issueDetail[0]['issue_level_1'], $issueDetail[0]['issue_level_2'],
                                                $issueDetail[0]['issue_level_3']);

        switch($issueLevel){
            case "level_1"  :   //edit issue is the level 1 issue
                                if(
                                    (strtolower($issueDetail[0]['issue_level_1']) != 'other' && strtolower($issueDetail[0]['issue_level_1']) != 'others') &&
                                    (strtolower($editIssue) == 'other' || strtolower($editIssue) == 'others')
                                  ){
                                    throw new MContactUsException("ISSUE_RENAME_TO_OTHER");
                                }
                                
                                $this->checkIssueLevelUniqueness($editIssue, $issueDetail[0]['issue_level_2'],
                                                    $issueDetail[0]['issue_level_3'], $issueLevel, $issueId);
                                
                                $issueChildren = $this->mContactUsDAO->getIssueChildren($issueDetail[0]['issue_level_1']);
                                
                                foreach($issueChildren as $idx=>$childInfo){
                                    $this->mContactUsDAO->updateContactUsIssue($childInfo['issue_id'], $editIssue, null, null, null, $issueLevel );
                                }

                                $this->mContactUsDAO->updateContactUsIssue($issueId, $editIssue, $issueEmail, $issueAction, $isActiveIssue, $issueLevel );

                                break;

            case "level_2"  :   //edit issue is the level 2 issue
                                if(
                                    (strtolower($issueDetail[0]['issue_level_2']) != 'other' && strtolower($issueDetail[0]['issue_level_2']) != 'others') &&
                                    (strtolower($editIssue) == 'other' || strtolower($editIssue) == 'others')
                                  ){
                                    throw new MContactUsException("ISSUE_RENAME_TO_OTHER");
                                }
                                $this->checkIssueLevelUniqueness($issueDetail[0]['issue_level_1'], $editIssue,
                                                    $issueDetail[0]['issue_level_3'], $issueLevel, $issueId);

                                $issueChildren = $this->mContactUsDAO->getIssueChildren($issueDetail[0]['issue_level_1'],
                                                                                    $issueDetail[0]['issue_level_2']);
                                
                                foreach($issueChildren as $idx=>$childInfo){
                                    $this->mContactUsDAO->updateContactUsIssue($childInfo['issue_id'], $editIssue, null, null, null, $issueLevel );
                                }

                                $this->mContactUsDAO->updateContactUsIssue($issueId, $editIssue, $issueEmail, $issueAction, $isActiveIssue, $issueLevel );

                                break;

            case "level_3"  :   //edit issue is the level 3 issue
                                if(
                                    (strtolower($issueDetail[0]['issue_level_3']) != 'other' && strtolower($issueDetail[0]['issue_level_3']) != 'others') &&
                                    (strtolower($editIssue) == 'other' || strtolower($editIssue) == 'others')
                                  ){
                                    throw new MContactUsException("ISSUE_RENAME_TO_OTHER");
                                }
                                $this->checkIssueLevelUniqueness($issueDetail[0]['issue_level_1'], $issueDetail[0]['issue_level_2'],
                                                            $editIssue, $issueLevel, $issueId);
                                $this->mContactUsDAO->updateContactUsIssue($issueId, $editIssue, $issueEmail, $issueAction, $isActiveIssue, $issueLevel );
                                break;
        }

       
        // edit issue fields
        if(is_array($fieldIdEdit)){
            foreach($fieldIdEdit as $idx=>$fieldId){

                $fieldNameEdit[$idx] = trim($fieldNameEdit[$idx]);
                if(empty($fieldNameEdit[$idx])){
                    $fieldNameEdit[$idx] = null;
                }

                $maxcharEdit[$idx] = trim($maxcharEdit[$idx]);
                if(empty($maxcharEdit[$idx])){
                    $maxcharEdit[$idx] = null;
                }

                $fieldEditAction[$idx] = trim($fieldEditAction[$idx]);
                if(empty($fieldEditAction[$idx])){
                    $fieldEditAction[$idx] = null;
                }

                if(!isset($isActiveFieldEdit[$idx])){
                    $isActiveFieldEdit[$idx] = 0;
                } else {
                    $isActiveFieldEdit[$idx] = 1;
                }

                $this->mContactUsDAO->updateContactUsIssueField($fieldId, $fieldTypeEdit[$idx], $fieldNameEdit[$idx],
                                                                $maxcharEdit[$idx], $fieldEditAction[$idx], $isActiveFieldEdit[$idx]);
            }
        }

        // insert new issue fields
        if(is_array($fieldType)){
            foreach($fieldType as $idx=>$issueFieldType){

                $fieldName[$idx] = trim($fieldName[$idx]);
                if(empty($fieldName[$idx])){
                    $fieldName[$idx] = null;
                }

                $maxchar[$idx] = trim($maxchar[$idx]);
                if(empty($maxchar[$idx])){
                    $maxchar[$idx] = null;
                }

                $fieldAction[$idx] = trim($fieldAction[$idx]);
                if(empty($fieldAction[$idx])){
                    $fieldAction[$idx] = null;
                }

                if(!isset($isActiveField[$idx])){
                    $isActiveField[$idx] = 0;
                } else {
                    $isActiveField[$idx] = 1;
                }

                $this->mContactUsDAO->insertContactUsIssueField($issueId, $issueFieldType, $fieldName[$idx],
                                                                $maxchar[$idx], $fieldAction[$idx], $isActiveField[$idx]);
            }
        }        

    }

    public function getIssueLevelFromUserData($issueLevel_1, $issueLevel_2){

        $issueLevel_1_Flag = empty($issueLevel_1);
        $issueLevel_2_Flag = empty($issueLevel_2);

        if ($issueLevel_1_Flag && $issueLevel_2_Flag){
            return "level_1";

        } elseif (!$issueLevel_1_Flag && $issueLevel_2_Flag){
            return "level_2";

        } elseif (!$issueLevel_1_Flag && !$issueLevel_2_Flag){
            return "level_3";

        } else {
            throw new MContactUsException("PARENT_ISSUE_MISSING");
        }

    }

    
    /*
     @throws:(MContactUsException)$e
     */ 
    public function getIssueLevelFromDB($issueLevel_1, $issueLevel_2, $issueLevel_3){
        
        $issueLevel_1_Flag = empty($issueLevel_1);
        $issueLevel_2_Flag = empty($issueLevel_2);
        $issueLevel_3_Flag = empty($issueLevel_3);

        if($issueLevel_1_Flag && $issueLevel_2_Flag && $issueLevel_3_Flag){
            throw new MContactUsException("NULL_ISSUE_VALUE");

        } elseif (!$issueLevel_1_Flag && $issueLevel_2_Flag && $issueLevel_3_Flag){
            return "level_1";

        } elseif (!$issueLevel_1_Flag && !$issueLevel_2_Flag && $issueLevel_3_Flag){
            return "level_2";

        } elseif (!$issueLevel_1_Flag && !$issueLevel_2_Flag && !$issueLevel_3_Flag){
            return "level_3";

        } else {
            throw new MContactUsException("PARENT_ISSUE_MISSING");
        }
    }

    private function checkIssueLevelUniqueness($issueLevel_1, $issueLevel_2=null, $issueLevel_3=null, $levelOfIssue, $excludedId=null){

        switch($levelOfIssue){

            case "level_1"  :   $issue = $this->mContactUsDAO->getLevel_1_IssueOnName($issueLevel_1, $excludedId);
                                if(!empty($issue)){
                                    throw new MContactUsException("DUPLICATE_ISSUE_AT_LEVEL_1");
                                }
                                break;

            case "level_2"  :   $issue = $this->mContactUsDAO->getLevel_2_IssueOnName($issueLevel_1, $issueLevel_2, $excludedId);
                                if(!empty($issue)){
                                    throw new MContactUsException("DUPLICATE_ISSUE_AT_LEVEL_2");
                                }
                                break;

            case "level_3"  :   $issue = $this->mContactUsDAO->getLevel_2_IssueOnName($issueLevel_1, $issueLevel_2, $excludedId);
                                if(empty($issue)){
                                    throw new MContactUsException("PARENT_ISSUE_MISSING");
                                }
            
                                $issue = $this->mContactUsDAO->getLevel_3_IssueOnName($issueLevel_1, $issueLevel_2, $issueLevel_3, $excludedId);
                                if(!empty($issue)){
                                    throw new MContactUsException("DUPLICATE_ISSUE_AT_LEVEL_3");
                                }
                                break;

        }
    }

    public function getContactUsIssueWithField($issueId=null, $active=false, $fieldDetailNeeded=true){

        $issueInfo = $this->mContactUsDAO->getContactUsIssue($issueId, $active);

        if($fieldDetailNeeded){
            if(is_array($issueInfo)){
                foreach($issueInfo as $idx=>$issue){
                    $issueInfo[$idx]['field'] = $this->mContactUsDAO->getContactUsIssueField($issue['issue_id'], $active);
                }
            }
        }

        // to get a right JSON when output of the result is empty array
        // this on empty gives a proper JSON empty( [] )object for js whose length=0 
        if(empty($issueInfo)){
            $issueInfo = array();
        }

        return array("issueArray"=>$issueInfo,"issueJSON"=>json_encode($issueInfo));
    }


}
?>