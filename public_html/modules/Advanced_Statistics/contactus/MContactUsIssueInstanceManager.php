<?php
namespace contactus;
/**
 * The classs exposes abstract APIs for handling myntra contact-us (issues).
 * @author: Arun Kumar K
 * legends:
 *      DAO - data access object
 *      instance - an instance of an issue is the response given by the customer against an issue. 
 */
use contactus\dao\MContactUsDAO;


class MContactUsIssueInstanceManager{

    public $MContactUsDAO;

    public function __construct(){
        //initialize DAO
        $this->mContactUsDAO = new MContactUsDAO();

    }

    public function addContactUsIssueInstanceField($instanceId, $field=array()){

        if(!empty($field)){
            if(is_array($field)){
                foreach($field as $id=>$val){
                    if(!empty($val)){
                        $this->mContactUsDAO->insertContactUsIssueInstanceField($instanceId, $id, $val);
                    }                        
                }
            }
        }        
    }
}
?>