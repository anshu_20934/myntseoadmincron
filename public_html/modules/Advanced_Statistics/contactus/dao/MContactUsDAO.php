<?php
namespace contactus\dao;

/**
 * The classs exposes DB manipulation API's for contact-us issues and its instances
 * @author: Arun Kumar K
 * legends:
 *      M - Myntra
 */

require_once \HostConfig::$documentRoot."/include/func/func.db.php";
require_once \HostConfig::$documentRoot."/include/func/func.utilities.php";



class MContactUsDAO{

    //contactus table names
    const M_CONTACT_ISSUE_TABLE = "contactus_issue";
    const M_CONTACT_ISSUE_FIELD_TABLE = "contactus_issue_field";
    const M_CONTACT_ISSUE_INSTANCE_TABLE = "contactus_issue_instance";
    const M_CONTACT_ISSUE_INSTANCE_FIELD_TABLE = "contactus_issue_instance_field";

   
    /*
     * get contact us issule levels in an associative array
     * @param:(bool)$active - true=gets active issues in the level, false= all issues
     * @return:(array)$issueLevels
     */
    public function getContactUsIssueLevel($active=false){

        $issueLevel_1 = $issueLevel_2 = array();

        $sql = "select issue_id, issue_level_1, is_active from " .self::M_CONTACT_ISSUE_TABLE. " where 1 ";

        // get only active
        if($active){
            $sql .= " AND is_active=1 ";
        }

        // get only issue_level_1
        $sql .= " AND (issue_level_2 is null OR issue_level_2='') ";
        $sql .= " AND (issue_level_3 is null OR issue_level_3='') ";

        $issueLevel_1 = func_query($sql, true);

        $sql = "select issue_id, issue_level_1, issue_level_2, is_active from " .self::M_CONTACT_ISSUE_TABLE. " where 1 ";

        // get only active
        if($active){
            $sql .= " AND is_active=1 ";
        }

        // get only issue_level_1
        $sql .= " AND issue_level_1 is not null AND issue_level_1!='' ";
        $sql .= " AND issue_level_2 is not null AND issue_level_2!='' ";
        $sql .= " AND (issue_level_3 is null OR issue_level_3='') ";

        $issueLevel_2 = func_query($sql, true);

        return array( "level_1"=>$issueLevel_1, "level_2"=>$issueLevel_2 );

    }
    /*
     * gets the level 1 issue by name
     * @param:(string)$issueLevel_1
     * @param:(int)$excludeId - check for uniqueness while updating excluding this
     * @return:(array)$issue - detail of issueLevel_1
     */
    public function getLevel_1_IssueOnName($issueLevel_1, $excludeId=null){
        $sql = "select
                    *
                from
                    " . self::M_CONTACT_ISSUE_TABLE. " where 1 ";
        $sql .= " AND issue_level_1='".sanitizeDBValues($issueLevel_1)."' ";
        $sql .= " AND (issue_level_2 is null OR issue_level_2='') ";
        $sql .= " AND (issue_level_3 is null OR issue_level_3='') ";

        if($excludeId != null){
            $sql .= " AND issue_id != '".sanitizeDBValues($excludeId)."' ";
        }

        $issueLevel_1_Info = func_query($sql, true);
        return $issueLevel_1_Info;
    }


    /*
     * gets the level 2 issue by name
     * @param:(string)$issueLevel_1
     * @param:(string)$issueLevel_2
     * @param:(int)$excludeId - check for uniqueness while updating excluding this
     * @return:(array)$issue - detail of issueLevel_2
     */
    public function getLevel_2_IssueOnName($issueLevel_1, $issueLevel_2, $excludeId=null){

        $sql = "select
                    *
                from
                    " . self::M_CONTACT_ISSUE_TABLE. " where 1 ";
        $sql .= " AND issue_level_1='".sanitizeDBValues($issueLevel_1)."' ";
        $sql .= " AND issue_level_2='".sanitizeDBValues($issueLevel_2)."' ";
        $sql .= " AND (issue_level_3 is null OR issue_level_3='') ";

        if($excludeId != null){
            $sql .= " AND issue_id != '".sanitizeDBValues($excludeId)."'";    
        }

        $issueLevel_2_Info = func_query($sql, true);
        return $issueLevel_2_Info;
    }

    /*
     * gets the level 3 issue by matching all 3 level issues by name
     * @param:(string)$issueLevel_1
     * @param:(string)$issueLevel_2
     * @param:(string)$issueLevel_3
     * @param:(int)$excludeId - check for uniqueness while updating excluding this
     * @return:(array)$issue - detail of issueLevel_3
     */
    public function getLevel_3_IssueOnName($issueLevel_1, $issueLevel_2, $issueLevel_3, $excludeId=null){
        $sql = "select
                    *
                from
                    " . self::M_CONTACT_ISSUE_TABLE ." where 1 ";

        $sql .= " AND issue_level_1='".sanitizeDBValues($issueLevel_1)."' ";
        $sql .= " AND issue_level_2='".sanitizeDBValues($issueLevel_2)."' ";
        $sql .= " AND issue_level_3='".sanitizeDBValues($issueLevel_3)."' ";


        if($excludeId != null){
            $sql .= " AND issue_id != '".sanitizeDBValues($excludeId)."' ";
        }

        $issueLevel_3_Info = func_query($sql, true);
        return $issueLevel_3_Info;
    }

    public function getIssueChildren($issueLevel_1, $issueLevel_2=null, $active=false){
        
        $sql = "select
                    *
                from
                    " . self::M_CONTACT_ISSUE_TABLE. " where 1 ";
        $sql .= " AND issue_level_1='".sanitizeDBValues($issueLevel_1)."' ";
        $sql .= " AND issue_level_2 is not null AND issue_level_2!='' ";

        if(!empty($issueLevel_2)){
            $sql .= " AND issue_level_2='".sanitizeDBValues($issueLevel_2)."' ";
            $sql .= " AND issue_level_3 is not null AND issue_level_3!='' ";
        }

        // get only active
        if($active){
            $sql .= " AND is_active=1 ";
        }

        $issueChildrenInfo = func_query($sql, true);
        return $issueChildrenInfo;
    }

    /*
     * gets all issues or the issue for which id provided
     * @param:(int)$issueID
     * @param:(bool)$active -- true=fetch only active issue : false=fetch all
     * @return:(array)$issue - detail of issue
     */
    public function getContactUsIssue($issueId=null, $active=false){
        
        $filter = array();

        $sql = "select
                    *
                from
                    " . self::M_CONTACT_ISSUE_TABLE;

        if(!empty($issueId)){
            $filter += array("issue_id" => $issueId);
        }

        // get only active issue
        if($active){
            $active = 1;
            $filter += array("is_active" => $active);
        }

        $order = array("issue_level_1"=>"asc");
        $order += array("issue_level_2"=>"asc");
        $order += array("issue_level_3"=>"asc");

        $issueInfo = sanitizedQueryBuilder($sql, $filter, $order, null, null, true);
        return $issueInfo;
    }

    /*
     * gets all fields on an issue
     * @param:(int)$issueID
     * @param:(bool)$active -- true=fetch only active field : false=fetch all
     * @return:(array)$issueField - detail of issue field
     */
    public function getContactUsIssueField($issueId, $active=false){

        $filter = array();
        
        $sql = "select
                    *
                from
                    " . self::M_CONTACT_ISSUE_FIELD_TABLE;


        $filter = array("issue_id" => $issueId);

        // get only active issue
        if($active){
            $active = 1;
            $filter += array("is_active" => $active);
        }

        $issueFieldInfo = sanitizedQueryBuilder($sql, $filter, null, null, null, true);
        return $issueFieldInfo;
    }

    /*
     * gets fields
     * @param:(string)$fieldIdCSV
     * @param:(bool)$active -- true=fetch only active field : false=fetch all
     * @return:(array)$issueField - detail of issue field
     */
    public function getContactUsIssueFieldFromCSV($fieldCSV, $active=false){

        $fieldCSV = trim($fieldCSV,',');

        $filter = array();

        $sql = "select
                    *
                from
                    " . self::M_CONTACT_ISSUE_FIELD_TABLE. " where issue_field_id in (".sanitizeDBValues($fieldCSV).") ";


        if($active){            
            $sql .= " AND is_active=1 ";
        }

        $issueFieldInfo = func_query($sql, true);
        return $issueFieldInfo;
    }

    /*
     * insert an issue
     * @param:(string)$issueLevel_1
     * @param:(string)$issueLevel_2
     * @param:(string)$issueLevel_3
     * @param:(int)$active
     * @return:(int)$issueId - (bool)false
     */
    public function insertContactUsIssue($issueLevel_1, $issueLevel_2, $issueLevel_3, $email, $action, $active){

        $queryData = array(
            "issue_level_1" => $issueLevel_1,
            "issue_level_2" => $issueLevel_2,
            "issue_level_3" => $issueLevel_3,
            "support_email" => $email,
            "action" => $action,
            "is_active" => $active,
        );

        return func_array2insertWithTypeCheck(self::M_CONTACT_ISSUE_TABLE, $queryData);
    }

    /*
     * insert an issue field
     * @param:(int)$issueId
     * @param:(string)$fieldType
     * @param:(string)$fieldLabel
     * @param:(int)$maxchar
     * @param:(int)$active
     * @return:(int)$issueId - (bool)false
     */
    public function insertContactUsIssueField($issueId, $fieldType, $fieldLabel, $maxchar, $action, $active){
        
        $queryData = array(
            "issue_id" => $issueId,
            "field_type" => $fieldType,
            "field_label" => $fieldLabel,
            "maxchar" => $maxchar,
            "action" => $action,
            "is_active" => $active,
        );

        return func_array2insertWithTypeCheck(self::M_CONTACT_ISSUE_FIELD_TABLE, $queryData);
    }
    
    public function insertContactUsIssueInstance($leafIssueId, $subject=null, $detail=null, $email){

        $queryData = array(
            "leaf_issue_id" => $leafIssueId,
            "issue_subject" => $subject,
            "issue_detail" => $detail,
            "customer_email" => $email,
            "response_time" => time()
        );

        return func_array2insertWithTypeCheck(self::M_CONTACT_ISSUE_INSTANCE_TABLE, $queryData);
    }

    public function insertContactUsIssueInstanceField($instanceId, $fieldId, $fieldValue=null){

        $queryData = array(
            "instance_id" => $instanceId,
            "issue_field_id" => $fieldId,
            "issue_field_value" => $fieldValue            
        );

        func_array2insertWithTypeCheck(self::M_CONTACT_ISSUE_INSTANCE_FIELD_TABLE, $queryData);
    }

    public function updateContactUsIssue($issueId, $editIssue, $email, $action, $isActiveIssue, $issueLevel ){

        if(!empty($editIssue)){
            $queryData = array("issue_".$issueLevel => $editIssue);
        }

        if(isset($isActiveIssue)){
            $queryData += array("is_active" => $isActiveIssue);
        }

        if(isset($email)){
            $queryData += array("support_email" => $email);
        }

        if(isset($action)){
            $queryData += array("action" => $action);    
        }

        func_array2updateWithTypeCheck(self::M_CONTACT_ISSUE_TABLE, $queryData, array("issue_id" => $issueId));

    }

    public function updateContactUsIssueField($fieldId, $fieldType, $fieldName, $maxchar, $action, $isActiveField){
        if(!empty($fieldType)){
            $queryData = array("field_type" => $fieldType);
        }

        if(isset($isActiveField)){
            $queryData += array("is_active" => $isActiveField);
        }
        
        $queryData += array("maxchar" => $maxchar);
        $queryData += array("field_label" => $fieldName);
        $queryData += array("action" => $action);

        func_array2updateWithTypeCheck(self::M_CONTACT_ISSUE_FIELD_TABLE, $queryData, array("issue_field_id" => $fieldId));
    }
}
?>