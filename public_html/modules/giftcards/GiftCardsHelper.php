<?php

use giftcard\GiftCard;
include_once (dirname(__FILE__)."/../RestAPI/RestRequest.php");
include_once $xcart_dir."/include/func/func.mail.php";


/*
 * Gift Cards Helper Class to communicate with portal services framework. Interface between JAVA and PHP layer.
 */
class GiftCardsHelper{
	
	/*
	 * Get all occasions data
	 */
	public static function getAllOccasions() {
		$url = HostConfig::$portalServiceHost.'/giftcards/occasion';
		$restRequest = new RestRequest($url, 'GET');
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		
		$returnObject = json_decode($responseBody);
        if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }		
		return $returnObject;
	}
	
	/*
	 * Get Occasions by Id
	 */
	public static function getOccasionById($id) {
		$url = HostConfig::$portalServiceHost.'/giftcards/occasion/'.$id;
		$restRequest = new RestRequest($url, 'GET');
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		
		$returnObject = json_decode($responseBody);
        if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }		
		return $returnObject;
	}
	
	/*
	 * Validate a Gift Card. 
	 * The amount for which a user is trying to purchase a gift card must be within the minValue and maxValue defined in the GiftCard Model in the services framework.
	 */
	public static function validateGiftCard($amount=500){
		$url = HostConfig::$portalServiceHost.'/giftcards';
		$giftCard = new GiftCard();
		$giftCard->setAmount($amount);
		$restRequest = new RestRequest($url, 'POST', json_encode($giftCard));
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers, 'Content-Type: application/json');
		array_push($headers, 'action: validate');
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		
		$returnObject = json_decode($responseBody);
		$resp=array();
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
		if($returnObject->status == "success"){
			$resp["isGiftCardValid"]=true;
			$resp["message"]=$returnObject->message;
		}
		else{
			$resp["isGiftCardValid"]=false;
			$resp["message"]=$returnObject->message;
		}
		return $resp;
	}
	
	/*
	 * Buy a GiftCard
	 */
	public static function buyGiftCard($giftCardOrder){
		$url = HostConfig::$portalServiceHost.'/giftcards/order';
		
		$restRequest = new RestRequest($url, 'POST', json_encode($giftCardOrder));

		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers, 'Content-Type: application/json');
		array_push($headers, 'action: buy');
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		
		$returnObject = json_decode($responseBody);
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
        return $returnObject;
	}
	
	/*
	 * Get Gift Card by Order id
	 */
	public function getGiftCardOrderById($orderid){
		$url = HostConfig::$portalServiceHost.'/giftcards/order/'.$orderid;

        $restRequest = new RestRequest($url, 'GET');
        $headers = array();
        array_push($headers, 'Accept: application/json');
        array_push($headers,'Content-Type: application/json');

        $restRequest ->setHttpHeaders($headers);
        $restRequest ->execute();
        $responseInfo = $restRequest ->getResponseInfo();
        $responseBody = $restRequest ->getResponseBody();
        $returnObject = json_decode($responseBody);
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
        return $returnObject;
	}
	
	/*
	 * Get Gift Cards by Sender Email
	 */
	public function getGiftCardsBySenderEmail($sender_email){
		$url = HostConfig::$portalServiceHost.'/giftcards?sender_email='.$sender_email;

        $restRequest = new RestRequest($url, 'GET');
        $headers = array();
        array_push($headers, 'Accept: application/json');
        array_push($headers,'Content-Type: application/json');

        $restRequest ->setHttpHeaders($headers);
        $restRequest ->execute();
        $responseInfo = $restRequest ->getResponseInfo();
        $responseBody = $restRequest ->getResponseBody();
        $returnObject = json_decode($responseBody);
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
        return $returnObject;
	}
	
	/*
	 * Get Gift Cards Orders by Email
	 */
	public function getGiftCardOrdersByEmail($email){
		$url = HostConfig::$portalServiceHost.'/giftcards/order?email='.$email;
		
        $restRequest = new RestRequest($url, 'GET');
        $headers = array();
        array_push($headers, 'Accept: application/json');
        array_push($headers,'Content-Type: application/json');

        $restRequest ->setHttpHeaders($headers);
        $restRequest ->execute();
        $responseInfo = $restRequest ->getResponseInfo();
        $responseBody = $restRequest ->getResponseBody();
        $returnObject = json_decode($responseBody);
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
        return $returnObject;
		
	}
	
	
	/*
	 * Get Gift Cards by Recipient Email
	 */
	public function getGiftCardsByRecipientEmail($recipient_email){
		$url = HostConfig::$portalServiceHost.'/giftcards?recipient_email='.$recipient_email;

        $restRequest = new RestRequest($url, 'GET');
        $headers = array();
        array_push($headers, 'Accept: application/json');
        array_push($headers,'Content-Type: application/json');

        $restRequest ->setHttpHeaders($headers);
        $restRequest ->execute();
        $responseInfo = $restRequest ->getResponseInfo();
        $responseBody = $restRequest ->getResponseBody();
        $returnObject = json_decode($responseBody);
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
        return $returnObject;
		
	}
	
	/*
	 * Get Gift Cards by Email id. Returns both gift cards sent and received by the given email id.
	 */
	public function getGiftCardsByEmail($email){
		$url = HostConfig::$portalServiceHost.'/giftcards?email='.$email;

        $restRequest = new RestRequest($url, 'GET');
        $headers = array();
        array_push($headers, 'Accept: application/json');
        array_push($headers,'Content-Type: application/json');

        $restRequest ->setHttpHeaders($headers);
        $restRequest ->execute();
        $responseInfo = $restRequest ->getResponseInfo();
        $responseBody = $restRequest ->getResponseBody();
        $returnObject = json_decode($responseBody);
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
        return $returnObject;
	}
	
	/*
	 * Get Gift Card by Id
	 */
	public function getGiftCardById($id){
		$url = HostConfig::$portalServiceHost.'/giftcards/id/'.$id;

        $restRequest = new RestRequest($url, 'GET');
        $headers = array();
        array_push($headers, 'Accept: application/json');
        array_push($headers,'Content-Type: application/json');

        $restRequest ->setHttpHeaders($headers);
        $restRequest ->execute();
        $responseInfo = $restRequest ->getResponseInfo();
        $responseBody = $restRequest ->getResponseBody();
        $returnObject = json_decode($responseBody);
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
        return $returnObject;
		
	}
	
	/*
	 * To get all Gift Card Orders.
	 */
	public function getActivatedGiftCardsByEmail($email){
		$url = HostConfig::$portalServiceHost.'/giftcards/activated?email='.$email;

        $restRequest = new RestRequest($url, 'GET');
        $headers = array();
        array_push($headers, 'Accept: application/json');
        array_push($headers,'Content-Type: application/json');

        $restRequest ->setHttpHeaders($headers);
        $restRequest ->execute();
        $responseInfo = $restRequest ->getResponseInfo();
        $responseBody = $restRequest ->getResponseBody();
        $returnObject = json_decode($responseBody);
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
        return $returnObject;
	}
	
	/*
	 * Log activation information for a gift card.
	 * Pass - Login on which it is activated, activation date and gift card id
	 */
	public function logGiftCardActivation($giftCardLog){
		$url = HostConfig::$portalServiceHost.'/giftcards/log';
		
		$restRequest = new RestRequest($url, 'POST', json_encode($giftCardLog));
        $headers = array();
        array_push($headers, 'Accept: application/json');
        array_push($headers,'Content-Type: application/json');

        $restRequest ->setHttpHeaders($headers);
        $restRequest ->execute();
        $responseInfo = $restRequest ->getResponseInfo();
        $responseBody = $restRequest ->getResponseBody();
        $returnObject = json_decode($responseBody);
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
        return $returnObject;
	}
	
	/*
	 * Get Gift Card Info for activation. Here the gift card information retrieved is used to get the amount
	 * to be credited to the user's cashback account.
	 * Activation can happen in 3 ways. Using
	 * 1. email_uuid - unique url generated for each giftcard
	 * 2. pin - pin sent in the email
	 * 3. id - gift card id
	 * NOTE: $card was introduced only recently to accomodate card-pin vouchers. To make it backward-nice, 
	 * we changed pin to card everywhere needed and modified pin to be auxillary
	 */
	public function getGiftCardInfoForActivation($type=null, $email_uuid=null, $pin=null, $id=null, $card){
		if($type == "gc"){
			$url = HostConfig::$portalServiceHost.'/giftcards/activate?type=email_uuid&email_uuid='.$email_uuid;
		} else if ($type == "pin") {
            $giftbigCardPrefix = FeatureGateKeyValuePairs::getFeatureGateValueForKey('giftbig.card.prefix');
            if (substr($card, 0, strlen($giftbigCardPrefix)) == $giftbigCardPrefix) {
                $headers = array();
                array_push($headers, 'Accept: application/json');
                array_push($headers,'Content-Type: application/json');

                $params = array();
                $params['type'] = 'GIFTBIG';
                $params['cardNumber'] = (string)$card;
                $params['pin'] = (string)$pin;
                global $XCART_SESSION_VARS;
                $params['login'] = $XCART_SESSION_VARS['login'];

                $url = HostConfig::$giftServiceHost."/giftcard/redeem";

                $request = new RestRequest($url, 'POST', json_encode($params));
                $request->setHttpHeaders($headers);
                $request->execute();
                $response = json_decode($request->getResponseBody());
                $responseInfo = $request->getResponseInfo();

                $transaction = new stdClass();
                $transaction->status = "failed";
                if ($responseInfo['http_code'] == 200) {
                    $transaction->message = $response->status->statusMessage;
                    $giftBig = new stdClass();

                    if ($response->status->statusType == 'SUCCESS') {
                        $transaction->status = "success";
                        $giftBig->card = $card;
                        $giftBig->amount = $response->data->amount;
                        $giftBig->transactionId = $response->data->transactionId;
                    }

                    $transaction->giftBig = $giftBig;
                } else {
                    $transaction->message = "Sorry, something went wrong. Please try again.";
                }
                return $transaction;
            } else if (substr($card, 0, 4) == 'GIFT') {
                $url = HostConfig::$portalServiceHost.'/giftcards/activate?type=pin&pin='.$card;
            } else {
                    $msg = new stdClass();
                    $msg->status = "failed";
                    $msg->type = "card";
                    $msg->message = "INVALID CARD NUMBER";
                    return $msg;
            }
		} else if ($type == "id"){
			$url = HostConfig::$portalServiceHost.'/giftcards/activate?type=id&id='.$id;
		}
		
        $restRequest = new RestRequest($url, 'GET');
        $headers = array();
        array_push($headers, 'Accept: application/json');
        array_push($headers,'Content-Type: application/json');

        $restRequest ->setHttpHeaders($headers);
        $restRequest ->execute();
        $responseInfo = $restRequest ->getResponseInfo();
        $responseBody = $restRequest ->getResponseBody();
        $returnObject = json_decode($responseBody);
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
        
        return $returnObject;
	}
	
	/*
	 * Get Gift Card Balance. Here the gift card information retrieved is used to get the balance
	 * present in the user's gift card.
	 */
	public function getGiftCardBalance($type=null, $email_uuid=null, $pin=null, $id=null, $card){
		if ($type == "balance") {
            $headers = array();
            array_push($headers, 'Accept: application/json');
            array_push($headers,'Content-Type: application/json');

            $params = array();
            $params['type'] = 'GIFTBIG';
            $params['cardNumber'] = (string)$card;
            $params['pin'] = (string)$pin;
            global $XCART_SESSION_VARS;
            $params['login'] = $XCART_SESSION_VARS['login'];

            $url = HostConfig::$giftServiceHost."/giftcard/balance";

            $request = new RestRequest($url, 'POST', json_encode($params));
            $request->setHttpHeaders($headers);
            $request->execute();
            $response = json_decode($request->getResponseBody());
            $responseInfo = $request->getResponseInfo();

            $transaction = new stdClass();
            $transaction->status = "failed";
            if ($responseInfo['http_code'] == 200) {
                $transaction->message = $response->status->statusMessage;
                $giftBig = new stdClass();

                if ($response->status->statusType == 'SUCCESS') {
                    $transaction->status = "success";
                    $giftBig->card = $card;
                    $giftBig->amount = $response->data->amount;
                    $giftBig->transactionId = $response->data->transactionId;
                    $transaction->balance = $giftBig->amount;
                }

               $transaction->giftBig = $giftBig;
                
            } else {
                    $transaction->message = "Sorry, something went wrong. Please try again.";
            }
            
            return $transaction;
        }  
        else {
            $msg = new stdClass();
            $msg->status = "failed";
            $msg->type = "card";
            $msg->message = "INVALID CARD NUMBER";
            return $msg;
        }
		
	}	
	/*
	 * Modify properties of an existing gift card. 
	 * Gift Card Object which has some of the properties changed is passed to this function which is then stored in the db.
	 */
	public function modifyGiftCard($giftCard){
		$url = HostConfig::$portalServiceHost.'/giftcards';
		$restRequest = new RestRequest($url, 'POST', $giftCard);
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers, 'Content-Type: application/json');
		array_push($headers, 'action: modify');
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		
		$returnObject = json_decode($responseBody);
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
        echo "<PRE>hello";
        var_dump($returnObject);
        exit;
        return $returnObject;
	}
	
	/*
	 * Modify properties of an existing gift card occasion
	 */
	public function modifyGiftCardOccasion($giftCardOccasion){
		$url = HostConfig::$portalServiceHost.'/giftcards';
		$restRequest = new RestRequest($url, 'POST', $giftCardOccasion);
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers, 'Content-Type: application/json');
		array_push($headers, 'action: modify');
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		
		$returnObject = json_decode($responseBody);
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
        echo "<PRE>hello";
        var_dump($returnObject);
        exit;
        return $returnObject;
	}
	
	/*
	 * Modify an existing gift card message that is linked to one or more gift cards.
	 */
	public function modifyGiftCardMessage($giftCardMessage){
		$url = HostConfig::$portalServiceHost.'/giftcards';
		$restRequest = new RestRequest($url, 'POST', $giftCardMessage);
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers, 'Content-Type: application/json');
		array_push($headers, 'action: modify');
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		
		$returnObject = json_decode($responseBody);
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
        echo "<PRE>hello";
        var_dump($returnObject);
        exit;
        return $returnObject;
	}
	
	/*
	 * Modify properties of an existing gift card order
	 */
	public function modifyGiftCardOrder($giftCardOrder){
		$url = HostConfig::$portalServiceHost.'/giftcards/order';
		$restRequest = new RestRequest($url, 'POST', $giftCardOrder);
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers, 'Content-Type: application/json');
		array_push($headers, 'action: modify');
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		
		$returnObject = json_decode($responseBody);
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
        echo "<PRE>hello";
        var_dump($returnObject);
        exit;
        return $returnObject;
	}

	public static function setGiftCardOrderComplete($giftCardId){
		$url = HostConfig::$portalServiceHost.'/giftcards/ordercomplete/'.$giftCardId;
		
		$restRequest = new RestRequest($url, 'GET');
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers, 'Content-Type: application/json');
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		
		$returnObject = json_decode($responseBody);
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
        
        return $returnObject;
	}
	
	public static function sendEmailForGiftCardAsync($giftCardId, $isAdmin='false'){
		$url = HostConfig::$portalServiceHost.'/giftcards/sendemail/'.$giftCardId;
		
		GiftCardsHelper::curl_request_async($url, array("isAdmin" => $isAdmin), 'GET');
		
		return true;
		/*$restRequest = new RestRequest($url, 'GET');
        $headers = array();
        array_push($headers, 'Accept: application/json');
        array_push($headers,'Content-Type: application/json');

        $restRequest ->setHttpHeaders($headers);
        $restRequest ->execute();
        $responseInfo = $restRequest ->getResponseInfo();
        $responseBody = $restRequest ->getResponseBody();
        $returnObject = json_decode($responseBody);
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
        return $returnObject;*/		
	}
	
	public static function sendEmailForGiftCard($giftCardId, $isAdmin='false'){
		$url = HostConfig::$portalServiceHost.'/giftcards/sendemail/'.$giftCardId.'?isAdmin='.$isAdmin;
		
		//GiftCardsHelper::curl_request_async($url, array("isAdmin" => $isAdmin), 'GET');
		
		//return true;
		$restRequest = new RestRequest($url, 'GET');
        $headers = array();
        array_push($headers, 'Accept: application/json');
        array_push($headers,'Content-Type: application/json');

        $restRequest ->setHttpHeaders($headers);
        $restRequest ->execute();
        $responseInfo = $restRequest ->getResponseInfo();
        $responseBody = $restRequest ->getResponseBody();
        $returnObject = json_decode($responseBody);
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
         	//Todo - put some logs here
        }
        return $returnObject;		
	}
	
	// $type must equal 'GET' or 'POST'
	public function curl_request_async($url, $params, $type='POST')
	{
	    foreach ($params as $key => &$val) {
	      if (is_array($val)) $val = implode(',', $val);
	      $post_params[] = $key.'='.urlencode($val);
	    }
	    $post_string = implode('&', $post_params);
	
	    $parts=parse_url($url);
	
	    $fp = fsockopen($parts['host'],
	        isset($parts['port'])?$parts['port']:80,
	        $errno, $errstr, 30);
	
	    // Data goes in the path for a GET request
	    if('GET' == $type) $parts['path'] .= '?'.$post_string;
	
	    $out = "$type ".$parts['path']." HTTP/1.1\r\n";
	    $out.= "Host: ".$parts['host']."\r\n";
	    $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
	    $out.= "Content-Length: ".strlen($post_string)."\r\n";
	    $out.= "Connection: Close\r\n\r\n";
	    // Data goes in the request body for a POST request
	    if ('POST' == $type && isset($post_string)) $out.= $post_string;
		
	    fwrite($fp, $out);
	    fclose($fp);
	}
	
	public static function sendDiscountRuleErrorEmail($heading=null, $content= null, $username = '') {

		$emailSubject = "Error in Discount Engine - $heading";

        $str = '<br/>';
        $str .= 'REQUEST_URI = '.$_SERVER['REQUEST_URI'].'<br/>';
        $str .= 'HTTP_HOST = '.$_SERVER['HTTP_HOST'].'<br/>';
        $str .= 'HTTP_USER_AGENT = '.$_SERVER['HTTP_USER_AGENT'].'<br/>';
        $str .= 'SERVER_NAME = '.$_SERVER['SERVER_NAME'].'<br/>';
        $str .= 'SERVER_ADDR = '.$_SERVER['SERVER_ADDR'].'<br/>';
        $str .= 'REMOTE_ADDR = '.$_SERVER['REMOTE_ADDR'].'<br/>';

		$emailContent = "User : $username<br/><br/>$str.$content";


		$subjectargs = array("TITLE"=>$emailSubject);
		$args = array("CONTENT" => $emailContent);
		sendMessageDynamicSubject("discount_rule_changes", $args, DISCOUNT_RULE_MODIFICATION_ERROR_EMAILS ,'MyntraAdmin-DiscountRule', $subjectargs);
	}	
}

