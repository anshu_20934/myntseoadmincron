<?php 
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";


class VatCalculator{

	public static function refreshVatOnCart(Mcart $cart){
		
		$vatEnabled = FeatureGateKeyValuePairs::getBoolean("vat.enabled", false);
		if($vatEnabled) {
			$vatCommonRate = FeatureGateKeyValuePairs::getFloat("vat.commonrate", 0);
			$vatDiscountThreshold = FeatureGateKeyValuePairs::getFloat("vat.discountthreshold", 0);
			
			
			$vatFilterConfiguration = json_decode(WidgetKeyValuePairs::getWidgetValueForKey('vatFilterConfiguration'),true);
			
			/*
			 * each of the following will be a associative array of form 
			 * array(
			 * 			"key"=>array("vat"=>vat-value,
			 *                        "threshold"=>discount-percentage-threshold
			 *                      )
			 *      )
			 */
			$exclusionBrands = $vatFilterConfiguration["exclusion"]["brands"];			
			$exclusionStyleTypes = $vatFilterConfiguration["exclusion"]["types"];
			$exclusionLabels = $vatFilterConfiguration["exclusion"]["article_types"];

			$inclusionBrands = $vatFilterConfiguration["inclusion"]["brands"];			
			$inclusionStyleTypes = $vatFilterConfiguration["inclusion"]["types"];
			$inclusionLabels = $vatFilterConfiguration["inclusion"]["article_types"];
			
		
			if(($vatCommonRate > 0) && ($vatDiscountThreshold >= 0)) {
				$cart->setAdditionalCharges(0);
				$thisAdditionalCharges = 0;
				foreach ($cart->getProducts() as $product) {
					
					$styleInfo = $product->getStyleArray();
					
					
					if(isset($exclusionBrands[strtolower($styleInfo["brand"])]) || isset($exclusionLabels[$styleInfo["article_type"]])
						|| isset($exclusionStyleTypes[$styleInfo["productTypeId"]])){
						continue;
					}

					$productLevelVatRate = -1;
					$productLevelVatThreshold = -1;

					if(isset($inclusionBrands[strtolower($styleInfo["brand"])])){
						$vatConfig = $inclusionBrands[strtolower($styleInfo["brand"])];
						if(isset($vatConfig["vat"])){
							$productLevelVatRate = $vatConfig["vat"];
						}

						if(isset($vatConfig["threshold"])){
							$productLevelVatThreshold = $vatConfig["threshold"];
						}
					}

					if(isset($inclusionLabels[$styleInfo["article_type"]])){
						$vatConfig = $inclusionLabels[$styleInfo["article_type"]];
						if(isset($vatConfig["vat"])){
							$productLevelVatRate = $vatConfig["vat"];
						}
					
						if(isset($vatConfig["threshold"])){
							$productLevelVatThreshold = $vatConfig["threshold"];
						}
					}

					if(isset($inclusionStyleTypes[$styleInfo["productTypeId"]])){
						$vatConfig = $inclusionStyleTypes[$styleInfo["productTypeId"]];
						if(isset($vatConfig["vat"])){
							$productLevelVatRate = $vatConfig["vat"];
						}

						if(isset($vatConfig["threshold"])){
							$productLevelVatThreshold = $vatConfig["threshold"];
						}
					}

					//get MRP of product = quantity x product price
					$productMRP = $product->getTotalProductPrice();
					$product->setTotalAdditionalCharges(0);
					$product->setUnitAdditionalCharges(0);
					//get discounted price
					$productPrice = (($product->getProductPrice() + $product->getUnitAdditionalCharges()) * $product->getQuantity()) - $product->getDiscount() - ($product->getDiscountAmount() * $product->getDiscountQuantity()) - $product->getCartDiscount();
					//calculate total discount percentage
					$discountAmount = $productMRP - $productPrice;
					$discountPercentage = ($discountAmount/$productMRP) * 100;
					//if over all discount is more than vat discount threshold then apply vat

					// get min of thresholds to apply vat
					$productLevelVatThreshold = ($productLevelVatThreshold >= 0)?$productLevelVatThreshold:$vatDiscountThreshold;

					if($discountPercentage >= $productLevelVatThreshold) {
						// apply the min of two -- (article, common) vat value
						$vatRate = min($vatCommonRate, $product->getVatRate());

						// get filter level vat rate 
						$vatRate = ($productLevelVatRate >= 0 && $productLevelVatRate <= $product->getVatRate() )
							? $productLevelVatRate : $vatRate;

						$productVat = $vatRate * $productPrice / 100;
						$product->setTotalAdditionalCharges($productVat);
						$unitVat = $productVat/$product->getQuantity();
						$product->setUnitAdditionalCharges($unitVat);
					}
					$thisAdditionalCharges += $product->getTotalAdditionalCharges();
				}
				$cart->setAdditionalCharges($thisAdditionalCharges);
			}
		}
	}

	public static function getVatOnStyle($brand, $productTypeId, $article_type){	
		//The following code is duplicated. It can be avoided.
		$vatCommonRate = FeatureGateKeyValuePairs::getFloat("vat.commonrate", 0);
		$vatDiscountThreshold = FeatureGateKeyValuePairs::getFloat("vat.discountthreshold", 0);

		$vatFilterConfiguration = json_decode(WidgetKeyValuePairs::getWidgetValueForKey('vatFilterConfiguration'),true);

		$exclusionBrands = $vatFilterConfiguration["exclusion"]["brands"];			
		$exclusionStyleTypes = $vatFilterConfiguration["exclusion"]["types"];
		$exclusionLabels = $vatFilterConfiguration["exclusion"]["article_types"];

		$inclusionBrands = $vatFilterConfiguration["inclusion"]["brands"];			
		$inclusionStyleTypes = $vatFilterConfiguration["inclusion"]["types"];
		$inclusionLabels = $vatFilterConfiguration["inclusion"]["article_types"];


		if(($vatCommonRate > 0) && ($vatDiscountThreshold >= 0)) {
			if(isset($exclusionBrands[strtolower($brand)]) || isset($exclusionLabels[$article_type])
				|| isset($exclusionStyleTypes[$productTypeId])){
					return $vatDiscountThreshold;
			}

			$productLevelVatRate = -1;
			$productLevelVatThreshold = -1;

			if(isset($inclusionBrands[strtolower($brand)])){
				$vatConfig = $inclusionBrands[strtolower($brand)];
				if(isset($vatConfig["vat"])){
					$productLevelVatRate = $vatConfig["vat"];
				}

				if(isset($vatConfig["threshold"])){
					$productLevelVatThreshold = $vatConfig["threshold"];
				}		
			}

			if(isset($inclusionLabels[$article_type])){
				$vatConfig = $inclusionLabels[$article_type];
				if(isset($vatConfig["vat"])){
					$productLevelVatRate = $vatConfig["vat"];
				}

				if(isset($vatConfig["threshold"])){
					$productLevelVatThreshold = $vatConfig["threshold"];
				}
			}

			if(isset($inclusionStyleTypes[$productTypeId])){
				$vatConfig = $inclusionStyleTypes[$productTypeId];
				if(isset($vatConfig["vat"])){
					$productLevelVatRate = $vatConfig["vat"];
				}

				if(isset($vatConfig["threshold"])){
					$productLevelVatThreshold = $vatConfig["threshold"];
				}
			}
		
			// get min of thresholds to apply vat
			return ($productLevelVatThreshold >= 0)?$productLevelVatThreshold:$vatDiscountThreshold;			
		}
	}
}


