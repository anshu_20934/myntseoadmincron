<?php
include_once($xcart_dir."/include/func/func.db.php");
include_once($xcart_dir."/include/func/func.utilities.php");
include_once($xcart_dir."/modules/serviceRequest/ServiceRequestException.php");
include_once($xcart_dir."/modules/serviceRequest/SRNonOrder.php");
include_once($xcart_dir."/modules/serviceRequest/SROrder.php");
include_once $xcart_dir."/modules/apiclient/WarehouseApiClient.php";
include_once($xcart_dir."/modules/apiclient/PincodeApiClient.php");

use feedback\instance\MFBSRInstance;

/**
 * The classs exposes abstract APIs for handling service requests(order or non-order related)
 * @author: Arun Kumar K
 * legends:
 *      SR - service request
 *      TAT - Turn Around Time
 */

class ServiceRequest{

    const SRTABLE = "mk_sr_tracker";//table

    public $categoryObj;
    public $subCategoryObj;
    public $statusObj;
    public $priorityObj;
    public $commentObj;
    public $notificationObj;    

    function __construct(){
        $this->_initializeSR();
    }

    /*
     * initializes SR with all necessary objects
     * so that not necessary to instantiate objects for each.
     * @return: void
     */
    public function _initializeSR(){
        $this->categoryObj  = new SRCategory();
        $this->subCategoryObj  = new SRSubCategory();
        $this->statusObj  = new SRStatus();
        $this->priorityObj  = new SRPriority();

        // TODO handling comments and notification
        //$this->commentObj  = new SRComment();
        //$this->notificationObj  = new SRNotification();
    }

    /*
     * gets the priority for SR
     * @param:(int)$SRId - SR id
     * @return:(array)$priority - all info of priority
     */
    public function getPriority($SRId){
        $sql = "select p.* from " . SRPriority::SRPRIORITYTABLE ." as p, " . ServiceRequest::SRTABLE .
                " as s where s.priority=p.id and s.SR_id='" . sanitizeDBValues($SRId) . "'";
        return func_query($sql, true);
    }

    /*
     * gets the status for SR
     * @param:(int)$SRId - SR id
     * @return:(array)$status - all info of status
     */
    public function getStatus($SRId){
        $sql = "select s.* from ".SRStatus::SRSTATUSTABLE." as s,
                ".ServiceRequest::SRTABLE." as sr
                where
                    sr.status=s.id
                and
                    sr.SR_id='".sanitizeDBValues($SRId)."'";

        return func_query($sql, true);
    }    

    /*
     * gets the category for SR
     * @param:(int)$SRId - SR id
     * @return:(array)$category - all info of category
     */
    public function getCategory($SRId){
        $sql = "select
                    c.* from ".SRCategory::SRCATEGORYTABLE." as c,
                    ".ServiceRequest::SRTABLE." as sr
                where
                    sr.category_id=c.id
                and
                    sr.SR_id='".sanitizeDBValues($SRId)."'";

        return func_query($sql, true);
    }

    /*
     * gets the sub-category for SR
     * @param:(int)$SRId - SR id
     * @return:(array)$sub-category - all info of sub-category
     */
    public function getSubCategory($SRId){
        $sql = "select
                    sc.* from ".SRSubCategory::SRSUBCATEGORYTABLE." as sc,
                    ".ServiceRequest::SRTABLE." as sr
                where
                    sr.sub_category_id=sc.id
                and
                    sr.SR_id='".sanitizeDBValues($SRId)."'";

        return func_query($sql, true);
    }

    /*
     * adds the SR
     * @return:(bool)false on failure, (int)$lastInsertId on success
     */
    public function createSR($categoryId, $subCatId, $priorityId, $SLA=null, $department, $channel, $callbackTime=null,
                             $reporter, $assignee=null, $copyto=null, $crSummary, $noteStatus=1){

        //authenticate reporter for Customer Support person
        if(!$this->isValidUserRoleToChangeStatus($reporter)){
            return false;
        }

        //create summary is mandatory
        if(empty($categoryId)|| empty($subCatId) || empty($priorityId) || empty($department) ||
                empty($channel) || empty($crSummary)){
            return false;
        }

        //generate unique customer SR id
        $customerSRId = $this->generateCustomerSRId();

        //status will always be 'open' while creating a SR
        $status = $this->statusObj->getSRStatusByName('open');
        $statusId = $status[0]['id'];

        $createDate = time();

        // timestamp and login detail prefixed to summary
        $prefixSummary = '----------------&#10;';
        $prefixSummary .=  date('r').'&#10;';
        $prefixSummary .=  $reporter.'&#10;';
        $crSummary = $prefixSummary.$crSummary;

        $queryData = array(
                        'customer_SR_id'=>$customerSRId,
                        'category_id'=>$categoryId,'sub_category_id'=>$subCatId,
                        'priority'=>$priorityId,'status'=>$statusId,
                        'SR_department'=>$department,'SR_channel'=>$channel,
                        'created_date'=>$createDate,
                        'reporter'=>$reporter,'assignee'=>$assignee,
                        'copyto'=>$copyto,'create_summary'=>$crSummary,
                        'notification_status'=>$noteStatus
                        );

        //calculate due date on SLA
        if(!is_null($SLA)){
            $dueDate = $SLA+$createDate;
            $queryData += array('due_date'=>$dueDate);
        }

        //calculate due date on SLA
        if(!empty($callbackTime)){            
            $queryData += array('callbacktime'=>$callbackTime);
        }

        $lastInsertId = func_array2insert(self::SRTABLE,sanitizeDBValues($queryData));
        return $lastInsertId;

        // TODO send notification on every SR action
    }

    /*
     * deletes the SR(soft delete by making flag N)
     * @return:(bool)
     */
    public function deleteSR($SRId, $deletedBy){
        
        //authenticate resolver for Customer Support person
        if(!$this->isValidUserRoleToManage($deletedBy)){
            return false;
        }

        //resolver and close summary needed to close
        if(empty($SRId) || empty($deletedBy)) {
            return false;
        }

        //delete time
        $deleteTime = time();

        $queryData = array(
                        'deletedby'=>$deletedBy,
                        'modified_date'=>$deleteTime,
                        'sr_active'=>'N',//soft deletion
                        );

        $condition = "SR_id = '".sanitizeDBValues($SRId)."'";

        $flag = func_array2update(self::SRTABLE, sanitizeDBValues($queryData),$condition);
        return $flag;

        // TODO send notification on every SR action
    }

    /*
     * hard delete
     * @return:(bool)
     */
    public function deleteSRIrreversible($SRId){

        if(empty($SRId)){
            return false;
        }

        $flag = db_query("delete from ".self::SRTABLE." where SR_id='".sanitizeDBValues($SRId)."'");
        return $flag;
    }

    /*
     * updates the SR
     * @return:(bool)
     */
    public function updateSR($SRId, $updatedBy, $categoryId=null, $subCatId=null, $priorityId=null, $statusId=null, $SLA=null,
                             $department=null, $channel=null, $callbackTime=null,
                             $assignee=null, $copyto=null, $crSummary=null, $comment=null,
                             $noteStatus=null){

        //authenticate resolver for Customer Support person
        if(!$this->isValidUserRoleToChangeStatus($updatedBy)){
            return false;
        }


        $queryData = array();

        //updated by info
        $queryData+=array("updatedby" => $updatedBy) ;
        $queryData+=array("modified_date" => time()) ;

        if (!empty($categoryId)){
            $queryData+= array("category_id" => $categoryId);
            $categoryName = $this->categoryObj->getSRCategory(array("id"=>$categoryId));
            $categoryName = $categoryName[0]['SR_category'];
        }
        if(!empty($subCatId)){
            $queryData+=array("sub_category_id" => $subCatId);
            $subCatName = $this->subCategoryObj->getSRSubCategory($categoryId,$subCatId);
            $subCatName = $subCatName[0]['SR_subcategory'];
        }
        if(!empty($priorityId)){
            $queryData+=array("priority" => $priorityId);
            $priorityName = $this->priorityObj->getSRPriority(array("id"=>$priorityId));
            $priorityName = $priorityName[0]['SR_priority'];
        }
        if(!empty($statusId)){
            $queryData+=array("status" => $statusId);
            $statusName = $this->statusObj->getSRStatus(array("id"=>$statusId));
            $statusName = $statusName[0]['SR_status'];
            // status can not be updated to close(close is a separate action)
            if(strtolower($statusName) == 'close'){
                return false;
            }
        }
        if(!empty($department)){
            $queryData+=array("SR_department" => $department);
        }
        if(!empty($channel)){
            $queryData+=array("SR_channel" => $channel);            
        }

        //calculate due date based on created date of SR
        if(!is_null($SLA)){
            $SRDate = $this->getSRDate($SRId);
            $dueDate = $SLA+$SRDate[0]['created_date'];
            $queryData+=array("due_date" => $dueDate) ;
        }
        
        $queryData+=array("assignee" => $assignee);//not compulsory hence can be updated by a null value
        $queryData+=array("copyto" => $copyto);
        $queryData+=array("callbacktime" => $callbackTime);

        // modify summary by prefixing with latest comment and other fields with timestamp and login 
        if(!empty($crSummary)){
            $prefixSummary = '----------------&#10;';
            $prefixSummary .=  date('r').'&#10;';
            $prefixSummary .=  'Updated by: '.$updatedBy.'&#10;';
            $prefixSummary .=  'Category: '.$categoryName.'&#10;';
            $prefixSummary .=  'Sub-category: '.$subCatName.'&#10;';
            $prefixSummary .=  'Priority: '.$priorityName.'&#10;';
            $prefixSummary .=  'Status: '.$statusName.'&#10;';
            $prefixSummary .=  'Department: '.$department.'&#10;';
            $prefixSummary .=  'Channel: '.$channel.'&#10;';
            if(!empty($callbackTime)){
                $prefixSummary .=  'Callback time: '.date('Y-m-d H:i',$callbackTime).'&#10;';
            }            
            $prefixSummary .=  'Comment: '.$comment.'&#10;';
            $prefixSummary .= '----------------&#10;&#10;';
            $crSummary = $prefixSummary.$crSummary;

            $queryData+=array("create_summary" => $crSummary);
        }
        //value can be 1/0 hence empty() check does not work
        if(isset($noteStatus)){
            $queryData+=array("notification_status" => $noteStatus);
        }

        $condition = "SR_id = '".sanitizeDBValues($SRId)."'";


        //check for changing from 'close' state to something else
        $SRInfo = $this->getSR(array('fSRid'=>$SRId));
        $oldSRStatusId = $SRInfo[0]['status_id'];
        $oldStatusInfo = $this->statusObj->getSRStatus(array('id'=>$oldSRStatusId));
        if(strtolower($oldStatusInfo[0]['SR_status']) == 'close') {
            return false;
        }        

        $flag = func_array2update(self::SRTABLE, sanitizeDBValues($queryData),$condition);

        return $flag;

        
        // TODO send notification on every SR action
    }

    /*
     * closes the SR
     * @return:(bool)
     */
    public function closeSR($SRId, $resolver, $closeSummary, $copyto=null, $noteStatus=null, $MFBSendStatus=1, $overRideResolver=false){

        //authenticate resolver for Customer Support person
        if($overRideResolver === false){
            if(!$this->isValidUserRoleToChangeStatus($resolver)){
                return false;
            }
        }

        //check for status already in 'close' state
        $SRInfo = $this->getSR(array('fSRid'=>$SRId));
        $oldSRStatusId = $SRInfo[0]['status_id'];
        $oldStatusInfo = $this->statusObj->getSRStatus(array('id'=>$oldSRStatusId));
        if(strtolower($oldStatusInfo[0]['SR_status']) == 'close') {
            return false;
        } 

        //status will always be 'close' while closing a SR
        $status = $this->statusObj->getSRStatusByName('close');
        $statusId = $status[0]['id'];

        //resolver and close summary needed to close
        if(empty($resolver) || empty($closeSummary)) {            
            return false;
        }

        //calculate close date based on created date of SR
        $closeTime = time();
        $SRDate = $this->getSRDate($SRId);
        $createTime = $SRDate[0]['created_date'];

        //TAT
        $TAT = $this->convertSRTimeDifference($createTime,$closeTime);

        $queryData = array(
                        'status'=>$statusId,
                        'resolver'=>$resolver,
                        'close_summary'=>$closeSummary,
                        'closed_date'=>$closeTime,
                        'TAT'=>$TAT['s'],
                        );
        $queryData+=array("copyto" => $copyto);
        //value can be 1/0 hence empty() check does not work
        if(isset($noteStatus)){
            $queryData+=array("notification_status" => $noteStatus);
        }
        
        $condition = "SR_id = '".sanitizeDBValues($SRId)."'";

        $flag = func_array2update(self::SRTABLE, sanitizeDBValues($queryData),$condition);

        //send MFB on service request closure
        if($flag && $MFBSendStatus == 1){
            $MFBSRInstance = new MFBSRInstance();
            $formURL = "http://www.myntra.com/feedback.php";
            $MFBSRInstance->sendSRMFBMail($formURL, $SRId);
        }

        return $flag;
        
        // TODO send notification on every SR action
    }


    /*
     * retrieves SR date(created,close,due)
     * @param:(int)$SRId
     * @return:(int)$createdTime
     */
    public function getSRDate($SRId){
        $filter = array("SR_id"=>$SRId);
        $sql = "select
                    created_date, due_date, closed_date
                from
                    " . self::SRTABLE;
        
        $serviceRequests = sanitizedQueryBuilder($sql,$filter, null, null, null, true);
        return $serviceRequests;
    }

    /*
     * count of SRs
     * @return:(int)$count
     */
    public function getSRCount($filter=null, $useRO=true){
        $sql = "select
                    count(sr.SR_id) as SR_count
                from
                    ".self::SRTABLE. " sr
                left join
                    ".SRNonOrder::SRNONORDERTABLE." nosr
                on
                    sr.SR_id=nosr.SR_id
                left join
                    ".SROrder::SRORDERTABLE." osr
                on
                    sr.SR_id=osr.SR_id
                left join
                    ".SRCategory::SRCATEGORYTABLE." c
                on
                    c.id=sr.category_id
                left join
                    ".SRSubCategory::SRSUBCATEGORYTABLE." sc
                on
                    sc.id=sr.sub_category_id
                left join
                    ".SRPriority::SRPRIORITYTABLE." p
                on
                    p.id=sr.priority
                left join
                    ".SRStatus::SRSTATUSTABLE." s
                on
                    s.id=sr.status
                where
                    sr.sr_active='Y'";

        if($filter!=null){

            if (!empty($filter['fSRid'])) {
                $SRId = $filter['fSRid'];
                $sql .= " AND sr.SR_id = '".$SRId."'";
            }
            if (!empty($filter['fCSRid'])) {
                $cSRId = $filter['fCSRid'];
                $sql .= " AND sr.customer_SR_id = '".$cSRId."'";
            }
            if (!empty($filter['fSROid'])) {
                $orderId = $filter['fSROid'];
                $sql .= " AND osr.order_id = '".$orderId."'";
            }
            if (!empty($filter['fSRType'])) {
                $SRType = $filter['fSRType'];
                if($SRType == 'order'){
                    $sql .= " AND osr.order_id is not null";
                } else {
                    $sql .= " AND osr.order_id is null";
                }
            }
            if (!empty($filter['fSRRef'])) {
                $SRRef = $filter['fSRRef'];
                if($SRRef == 'Y'){
                    $sql .= " AND osr.refund='Y'";
                } else {
                    $sql .= " AND osr.refund='N'";
                }
            }
            if (!empty($filter['fSRComp'])) {
                $SRComp = $filter['fSRComp'];
                if($SRComp == 'Y'){
                    $sql .= " AND osr.compensation='Y'";
                } else {
                    $sql .= " AND osr.compensation='N'";
                }
            }
            if (!empty($filter['fCat'])) {
                $sql .= " AND sr.category_id = '".sanitizeDBValues($filter['fCat'])."'";
            }
            if (!empty($filter['fSCat'])) {
                $sql .= " AND sr.sub_category_id = '".sanitizeDBValues($filter['fSCat'])."'";
            }
            if (!empty($filter['fStat'])) {
                $sql .= " AND sr.status = '".sanitizeDBValues($filter['fStat'])."'";
            }
            if (!empty($filter['fPri'])) {
                $sql .= " AND sr.priority = '".sanitizeDBValues($filter['fPri'])."'";
            }
            if (!empty($filter['SR_department'])) {
                $sql .= " AND sr.SR_department = '".sanitizeDBValues($filter['SR_department'])."'";
            }
            if (!empty($filter['SR_channel'])) {
                $sql .= " AND sr.SR_channel = '".sanitizeDBValues($filter['SR_channel'])."'";
            }
            if (!empty($filter['fStartCallback'])) {
                $callbackTime = $filter['fStartCallback'];
                $sql .= " AND sr.callbacktime >= '".sanitizeDBValues($callbackTime)."'";
            }
            if (!empty($filter['fEndCallback'])) {
                $callbackTime = $filter['fEndCallback'];
                $sql .= " AND sr.callbacktime <= '".sanitizeDBValues($callbackTime)."'";
            }
            if (!empty($filter['fStartCrDate'])) {
                $createTime = $filter['fStartCrDate'];
                $sql .= " AND sr.created_date >= '".sanitizeDBValues($createTime)."'";
            }
            if (!empty($filter['fEndCrDate'])) {
                $createTime = $filter['fEndCrDate'];
                $sql .= " AND sr.created_date <= '".sanitizeDBValues($createTime)."'";
            }
            if (!empty($filter['fStartDuDate'])) {
                $dueTime = $filter['fStartDuDate'];
                $sql .= " AND sr.due_date >= '".sanitizeDBValues($dueTime)."'";
            }
            if (!empty($filter['fEndDuDate'])) {
                $dueTime = $filter['fEndDuDate'];
                $sql .= " AND sr.due_date <= '".sanitizeDBValues($dueTime)."'";
            }
            if (!empty($filter['fStartClDate'])) {
                $closeTime = $filter['fStartClDate'];
                $sql .= " AND sr.closed_date >= '".sanitizeDBValues($closeTime)."'";
            }
            if (!empty($filter['fEndClDate'])) {
                $closeTime = $filter['fEndClDate'];
                $sql .= " AND sr.closed_date <= '".sanitizeDBValues($closeTime)."'";
            }
            if (!empty($filter['fAge'])) {
                $createdBack = time()-($filter['fAge']*86400);//so many days back
                $sql .= " AND sr.created_date <= '".sanitizeDBValues($createdBack)."'";
            }
            if (!empty($filter['fRepo'])) {
                $sql .= " AND sr.reporter LIKE '%".sanitizeDBValues($filter['fRepo'])."%'";
            }
            if (!empty($filter['fReso'])) {
                $sql .= " AND sr.resolver LIKE '%".sanitizeDBValues($filter['fReso'])."%'";
            }
            if (!empty($filter['fCust'])) {
                $sql .= " AND
                        (
                            nosr.customer_email LIKE '%".sanitizeDBValues($filter['fCust'])."%'
                            or
                            osr.customer_email LIKE '%".sanitizeDBValues($filter['fCust'])."%'
                        )";
            }

        }

        $serviceRequests = sanitizedQueryBuilder($sql, $filter=null, $order=null, $offset=null, $limit=null, $useRO);
        return $serviceRequests[0]['SR_count'];
    }

    /*
     * retrieves the SR
     * @return:(array)$serviceRequests
     */
    public function getSR($filter=NULL,$order=NULL,$offset=NULL,$limit=NULL,$useRO=true){
       
       $sql = "select
                    nosr.id, nosr.customer_email, nosr.last_name,
                    IF((nosr.first_name IS NULL OR nosr.first_name=''),o.firstname,nosr.first_name) as first_name,
                    IF((nosr.mobile IS NULL OR nosr.mobile=''),o.mobile,nosr.mobile) as mobile,
                    osr.id as order_sr_id, osr.order_id, osr.items, osr.customer_email as order_customer_email,
                    osr.refund, osr.refund_type, osr.refund_value,
                    osr.compensation, osr.compensation_type, osr.compensation_value,
                    sr.SR_id, sr.customer_SR_id, sr.category_id, c.SR_category, sr.sub_category_id, sc.SR_subcategory,
                    sr.priority as priority_id , p.SR_priority, sr.status as status_id, s.SR_status, sr.SR_department, sr.SR_channel,
                    IF((sr.callbacktime IS NULL OR sr.callbacktime=0),'NA',from_unixtime(sr.callbacktime,'%Y-%m-%d %H:%i')) as callbacktime_date,
                    IF((sr.callbacktime IS NULL OR sr.callbacktime=0),'',sr.callbacktime) as callbacktime,
                    sr.created_date as createtime,sr.due_date as duetime, sr.closed_date as closetime,
                    from_unixtime(sr.created_date,'%Y-%m-%d %H:%i') as created_date,
                    IF((sr.due_date IS NULL OR sr.due_date=0),'',from_unixtime(sr.due_date,'%Y-%m-%d %H:%i')) as due_date,
                    IF((sr.modified_date IS NULL OR sr.modified_date=0),'',from_unixtime(sr.modified_date,'%Y-%m-%d %H:%i')) as modified_date,
                    from_unixtime(sr.closed_date,'%Y-%m-%d %H:%i') as closed_date, sr.TAT,
                    sr.reporter, sr.assignee, sr.updatedby,sr.deletedby, sr.resolver, sr.create_summary, sr.close_summary,
                    sr.copyto, sr.notification_status,
                    IF((o.shippeddate IS NOT NULL AND completeddate IS NULL AND delivereddate IS NULL),unix_timestamp(now())-shippeddate,null) as delivery_ageing,                     
                    o.status, o.payment_method, o.courier_service, o.warehouseid,o.s_address as order_address,o.s_city as order_city,
                    o.s_state as order_state,o.s_zipcode as order_zipcode
                from
                    ".self::SRTABLE. " sr
                left join
                    ".SRNonOrder::SRNONORDERTABLE." nosr
                on
                    sr.SR_id=nosr.SR_id
                left join
                    ".SROrder::SRORDERTABLE." osr
                on
                    sr.SR_id=osr.SR_id
                left join
                     xcart_orders o
                on
                    o.orderid=osr.order_id
                left join
                    ".SRCategory::SRCATEGORYTABLE." c
                on
                    c.id=sr.category_id
                left join
                    ".SRSubCategory::SRSUBCATEGORYTABLE." sc
                on
                    sc.id=sr.sub_category_id
                left join
                    ".SRPriority::SRPRIORITYTABLE." p
                on
                    p.id=sr.priority
                left join
                    ".SRStatus::SRSTATUSTABLE." s
                on
                    s.id=sr.status
                WHERE
                    sr.sr_active='Y'";

        if($filter!=null){

            if (!empty($filter['fSRid'])) {
                $SRId = $filter['fSRid'];
                $sql .= " AND sr.SR_id = '".$SRId."'";
            }
            if (!empty($filter['fCSRid'])) {
                $cSRId = $filter['fCSRid'];
                $sql .= " AND sr.customer_SR_id = '".$cSRId."'";
            }
            if (!empty($filter['fSROid'])) {
                $orderId = $filter['fSROid'];
                $sql .= " AND osr.order_id = '".$orderId."'";
            }
            if (!empty($filter['fSRType'])) {
                $SRType = $filter['fSRType'];
                if($SRType == 'order'){
                    $sql .= " AND osr.order_id is not null";
                } else {
                    $sql .= " AND osr.order_id is null";
                }
            }
            if (!empty($filter['fSRRef'])) {
                $SRRef = $filter['fSRRef'];
                if($SRRef == 'Y'){
                    $sql .= " AND osr.refund='Y'";
                } else {
                    $sql .= " AND osr.refund='N'";
                }
            }
            if (!empty($filter['fSRComp'])) {
                $SRComp = $filter['fSRComp'];
                if($SRComp == 'Y'){
                    $sql .= " AND osr.compensation='Y'";
                } else {
                    $sql .= " AND osr.compensation='N'";
                }
            }
            if (!empty($filter['fCat'])) {
                $sql .= " AND sr.category_id = '".sanitizeDBValues($filter['fCat'])."'";
            }
            if (!empty($filter['fSCat'])) {
                $sql .= " AND sr.sub_category_id = '".sanitizeDBValues($filter['fSCat'])."'";
            }
            if (!empty($filter['fStat'])) {
                $sql .= " AND sr.status = '".sanitizeDBValues($filter['fStat'])."'";
            }
            if (!empty($filter['fPri'])) {
                $sql .= " AND sr.priority = '".sanitizeDBValues($filter['fPri'])."'";
            }
            if (!empty($filter['SR_department'])) {
                $sql .= " AND sr.SR_department = '".sanitizeDBValues($filter['SR_department'])."'";
            }
            if (!empty($filter['SR_channel'])) {
                $sql .= " AND sr.SR_channel = '".sanitizeDBValues($filter['SR_channel'])."'";
            }
            if (!empty($filter['fStartCallback'])) {
                $callbackTime = $filter['fStartCallback'];
                $sql .= " AND sr.callbacktime >= '".sanitizeDBValues($callbackTime)."'";
            }
            if (!empty($filter['fEndCallback'])) {
                $callbackTime = $filter['fEndCallback'];
                $sql .= " AND sr.callbacktime <= '".sanitizeDBValues($callbackTime)."'";
            }
            if (!empty($filter['fStartCrDate'])) {
                $createTime = $filter['fStartCrDate'];
                $sql .= " AND sr.created_date >= '".sanitizeDBValues($createTime)."'";
            }
            if (!empty($filter['fEndCrDate'])) {
                $createTime = $filter['fEndCrDate'];
                $sql .= " AND sr.created_date <= '".sanitizeDBValues($createTime)."'";
            }
            if (!empty($filter['fStartDuDate'])) {
                $dueTime = $filter['fStartDuDate'];
                $sql .= " AND sr.due_date >= '".sanitizeDBValues($dueTime)."'";
            }
            if (!empty($filter['fEndDuDate'])) {
                $dueTime = $filter['fEndDuDate'];
                $sql .= " AND sr.due_date <= '".sanitizeDBValues($dueTime)."'";
            }
            if (!empty($filter['fStartClDate'])) {
                $closeTime = $filter['fStartClDate'];
                $sql .= " AND sr.closed_date >= '".sanitizeDBValues($closeTime)."'";
            }
            if (!empty($filter['fEndClDate'])) {
                $closeTime = $filter['fEndClDate'];
                $sql .= " AND sr.closed_date <= '".sanitizeDBValues($closeTime)."'";
            }
            if (!empty($filter['fAge'])) {
                $createdBack = time()-($filter['fAge']*86400);//so many days back
                $sql .= " AND sr.created_date <= '".sanitizeDBValues($createdBack)."'";
            }
            if (!empty($filter['fRepo'])) {
                $sql .= " AND sr.reporter LIKE '%".sanitizeDBValues($filter['fRepo'])."%'";
            }
            if (!empty($filter['fReso'])) {
                $sql .= " AND sr.resolver LIKE '%".sanitizeDBValues($filter['fReso'])."%'";
            }
            if (!empty($filter['fCust'])) {
                $sql .= " AND
                        (
                            nosr.customer_email LIKE '%".sanitizeDBValues($filter['fCust'])."%'
                            or 
                            osr.customer_email LIKE '%".sanitizeDBValues($filter['fCust'])."%'
                        )";
            }

        }

        $serviceRequests = sanitizedQueryBuilder($sql, $filter=null, $order, $offset, $limit, $useRO);

        if(!is_array($serviceRequests)){
           return array();
        }

        //get derived columns for SRs
        $serviceRequests = $this->getDerivedColumnsForSR($serviceRequests);            

        return $serviceRequests;
    }

    /*
     * converts time in sec to readable format like d:h:m:s(if one parameter is given)
     * calculates Time difference for given 2 times(if 2 params are given)
     * @param:(int)$fromTime
     * @param:(int)$toTime -when not given function converts the $fromTime in different formats
     * @return:(array)$timeDifference or $time in different formats
     */
    public function convertSRTimeDifference($fromTime,$toTime=null){

        //difference in secs
        if($toTime != null){
            $secs = $toTime-$fromTime;

        //to tell secs in number of days and hours format            
        } else {
            $secs = $fromTime;
        }


        //difference in d:h:m:s
        $days = floor($secs/86400);//days

        $secModulusDay = $secs%86400;
        $hours = floor($secModulusDay/3600);//hours

        $secModulusHour = $secModulusDay%3600;
        $mins = floor($secModulusHour/60);//mins

        $remainingSecs = $secModulusHour%60;//secs

        //TODO difference in h:m:s
        
        return array(
                    's'=>$secs,
                    'd'=>$days,
                    'h'=>$hours,
                    'd:h'=>"$days Days:$hours Hours",
                    'd:h:m'=>"$days D:$hours H:$mins M",
                    'd:h:m:s'=>"$days D:$hours H:$mins M:$remainingSecs S"
                    );
    }

    /*
     * converts day:hour into time in sec
     * @param:(int)$day
     * @param:(int)$hour
     * @return:(int)$seconds
     */
    public function convertDayHourToTime($day,$hour){
        $sec = $day*86400;
        $sec += $hour*3600;
        return $sec;
    }

    /*
     * converts date(mm/dd/yyyy) into time in sec
     * @param:(string)$date - in mm/dd/yyyy
     * @param:(string)$startOrEnd - specify 'start' or 'end' of day
     * @return:(int)$seconds
     */
    public function getDateToTime($date, $startOrEnd){
        $date = explode('/',$date);
        $year = $date[2];
        $month = $date[0];
        $day = $date[1];
        if($startOrEnd == 'start'){
            $dateInSec = mktime(0,0,0,$month,$day,$year);
        }else if($startOrEnd == 'end'){
            $dateInSec = mktime(23,59,59,$month,$day,$year);
        }
        return $dateInSec;
    }

    /*
     * validates user based on role type(CS)
     * @param:(string)$login
     * @return:(bool)
     */
    public function isValidUserRoleToChangeStatus($login){
        $sql = "select
                    count(x.login) as count
                from
                    xcart_customers x,mk_user_roles ur,mk_roles r
                where
                    ur.login=x.login
                and
                    ur.roleid=r.id
                and
                    r.type='CS'
                and
                    x.login='".$login."'";

        $serviceRequests = sanitizedQueryBuilder($sql, null, null, null, null, true);

        if($serviceRequests[0]['count'] > 0)
            return true;
        else
            return false;
    }

    /*
     * validates user based on role type(CS)
     * @param:(string)$login
     * @return:(bool)
     */
    public function isValidUserRoleToManage($login){

        global $XCART_SESSION_VARS;        

        //get the filename which is being clicked
        $pageUrlRequest = explode('/admin/', $_SERVER['SCRIPT_NAME']);
        $pageRequest = $pageUrlRequest[count($pageUrlRequest)-1];

        //get the roles which can access the page
        $dbApdater = \RolesAccessDBAdapter::getInstance();
        $userRolesDB = $dbApdater->fetchRolesAccess("pagename = '$pageRequest'");

        if(empty($userRolesDB)){
            return false;
        }

        $roleAllowedForPageStr = array($userRolesDB->getRoles());
        $extraLoginsAllowedForPageStr = array($userRolesDB->getExtraLoginsAllowed());

        $roleAllowedForPage = explode(",", implode(",", $roleAllowedForPageStr));
        $extraLoginsAllowedForPage = explode(",", implode(",", $extraLoginsAllowedForPageStr));

        //get roles of the logged in user
        $roles=$XCART_SESSION_VARS['user_roles'];

        //check for the user role
        $userRoles = array();
        foreach($roles as $role) {
            $userRoles[] = $role['role'];
        }

        //if role and extra login matches then only is a valid customer care to change
        if(count(array_intersect($userRoles, $roleAllowedForPage)) > 0){
            if(in_array($login, $extraLoginsAllowedForPage)){
                return true;
            }
        }
        
        return false;
    }


    /*
     * adds derived columns such as ageing, deviation, order status, payment method, delivery ageing, courier, tracking, warehouse name
     * DC(delivery center) detail for ML courier service  etc..
     * @param:(array)$allSR - the array should contain columns named 'createtime' and 'duetime' in timestamp
     * to calculate ageing and deviation.
     * @return:(array)$allSR - with new keys are 'ageing' ,'deviation' added.
     */
    protected function getDerivedColumnsForSR($allSR){
        $currentTime = time();
        if(!empty($allSR)){

            //get all warehouse detail to later replace warehouseid with name
            $wareHouses = $this->getWarehouseDetail();
            
            foreach($allSR as $idx=>$SR){

                //calculate ageing and deviation for SRs
                if($SR['SR_status'] != 'close'){
                    //calculate ageing(current date-created date) for all SR
                    $ageing = $this->convertSRTimeDifference($currentTime-$SR['createtime']);//in days:hours
                    $allSR[$idx]['ageing'] = $ageing['d:h'];

                    //calculate deviation for all non-closed and delayed(duedate <= currentdate) SR     *
                    if(!empty($SR['duetime']) && $SR['duetime']<=$currentTime){
                        $deviation = $this->convertSRTimeDifference($currentTime-$SR['duetime']);//in days:hours
                        $allSR[$idx]['deviation'] = $deviation['d:h'];
                    }

                } else if($SR['SR_status'] == 'close'){
                    //calculate ageing(closed date-created date) for all SR
                    $ageing = $this->convertSRTimeDifference($SR['closetime']-$SR['createtime']);//in days:hours
                    $allSR[$idx]['ageing'] = $ageing['d:h'];

                    //calculate deviation for all closed and delayed(duedate <= closed date) SR     *
                    if(!empty($SR['duetime']) && $SR['duetime']<=$SR['closetime']){
                        $deviation = $this->convertSRTimeDifference($SR['closetime']-$SR['duetime']);//in days:hours
                        $allSR[$idx]['deviation'] = $deviation['d:h'];
                    }
                }

                // get the DC(delivery center) detail
                if(!empty($allSR[$idx]['courier_service']) && !empty($allSR[$idx]['order_zipcode'])){
                    $DCInfo = PincodeApiClient::getDCDetail($allSR[$idx]['courier_service'], $allSR[$idx]['order_zipcode']);
                    $allSR[$idx]['DC_info'] = $DCInfo[0]['cityCode'];
                    
                    if(!empty($DCInfo[0]['areaCode'])){
                        $allSR[$idx]['DC_info'] .= '/'.$DCInfo[0]['areaCode'];
                    }

                    if(!empty($DCInfo[0]['areaName'])){
                        $allSR[$idx]['DC_info'] .= '/'.$DCInfo[0]['areaName'];
                    }

                }


                //get derived columns for order related information for SR 
                $this->getOrderInfoForSR($allSR, $idx, $SR);

                // replace warehouseid with name
                foreach($wareHouses as $wIdx=>$wDetail){
                    if($allSR[$idx]['warehouseid'] == $wDetail['id']){
                        $allSR[$idx]['warehouseid'] = $wDetail['name'];        
                    }
                }
            }
            
        } else {
            $allSR = array();
        }
        return $allSR;

    }

    /*
     * get all warehouse details
     * @return:(array)$warehouses
     */
    public function getWarehouseDetail(){
        return $warehouses = WarehouseApiClient::getAllWarehouses();        
    }

    /*
     * Creates a unique SR id for customer(to share with)
     * @return:(string)$customerSRId
     */
    public function generateCustomerSRId(){

        // Get a unique customer SR id.(format : YYMMDDxxxx)
        $date = date("ymd");
        $customerSRId = $date . $this->getRandomString(4);

        while ($this->existsCustomerSRId($customerSRId)) {
            $customerSRId = $date . $this->getRandomString(4);
        }

        return $customerSRId;
    }

    /*
     * checks hether a customer SR id already exists.
     * @param:(string)$customerSRId
     * @return:(bool)
     */
    public function existsCustomerSRId($customerSRId)
    {
        $result = func_query_first("SELECT customer_SR_id
                                    FROM " .self::SRTABLE. "
                                    WHERE customer_SR_id = '$customerSRId'", true);

        return !empty($result);
    }

    /**
     * Generate a random string of given length.
     */
    private function getRandomString($length)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';

        // Append a random character until we reach the desired length.
        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, strlen($characters))];
        }

        return $string;
    }

    /* departments for which SR need to be assigned
     * @return:(array)$departments
     */
    public function getSRDepartment(){
        return array(
            0=>'Operations',
            1=>'Logistics',
            2=>'Finance',
            3=>'CC',
            4=>'Category',
            5=>'Inventory',
            6=>'Engineering',
            7=>'ML',
            8=>'3PL',
            9=>'Warehouse',
            10=>'QMT',
            11=>'Returns',
            12=>'RTO',
            13=>'Catalog',
            14=>'IT'            
        );
    }

    /* channels from which SRs raised
     * @return:(array)$channels
     */
    public function getSRChannel(){
        return array(
            0=>'Phone',
            1=>'Chat',
            2=>'Email',
            3=>'Walk-in',            
        );
    }

    /* get order information for all SRs
     * @param:$allSRs - array of SRs need to be added with order related info
     * @param:$currentSRIndex - is the current index of the $allSRs array which need to be added by order info
     * @param:$SR - current SR for which more columns of order need to be added
     * @return:$allSRs - array of SRs with order info
     */
    protected function getOrderInfoForSR(&$allSRs, $currentSRIndex, $SR){

        //order statuses
        $orderStatus = array(
            'OH'=>'On Hold',
            'I'=>'Not Finished',
            'Q'=>'Queued',
            'P'=>'Processed',
            'B'=>'Backordered',
            'D'=>'Declined',
            'F'=>'Rejected',
            'C'=>'Complete',
            'PD'=>'Pending Check Collection',
            'PP'=>'Preprocessed',
            'CD'=>'COD Verification',            
            'PV'=>'COD Pending Verification',
            'SH'=>'Shipped',
            'DL'=>'Delivered',
            'WP'=>'Work in Progress',
            'PK'=>'Packed',
            'L'=>'Lost'
        );

        //order courier service -- change by an pre-populated array of mk_sourier_Serice table code=>name
        $courierServiceResult = db_query("select code, courier_service from mk_courier_service", true);
        $courierService = array();
        if ($courierServiceResult){
            while ($row=db_fetch_array($courierServiceResult)){
                $courierService[$row['code']] = $row['courier_service'];         
            }
        }

        //replace order status with string
        if(!empty($SR['status'])){
            $allSRs[$currentSRIndex]['status'] = $orderStatus[$SR['status']];
        }

        //delivery ageing in days
        if(!empty($SR['delivery_ageing'])){
            $deliveryAgeing = $this->convertSRTimeDifference($SR['delivery_ageing']);
            $allSRs[$currentSRIndex]['delivery_ageing'] = $deliveryAgeing['d:h'];
        }

        //replace courier service code with name
        if(!empty($SR['courier_service'])){
            $allSRs[$currentSRIndex]['courier_service'] = $courierService[$SR['courier_service']];
        }

        //get shipment tracking number(waybill number)
        if(!empty($SR['order_id'])){
            $allSRs[$currentSRIndex]['tracking'] = func_query_first_cell("select tracking_no from mk_shipment_tracking where ref_id='".$SR['order_id']."'", true);
        }        
    }
}


final class SRPriority{

    const SRPRIORITYTABLE = "mk_sr_priority";

     public function getSRPriority($filter=null){

        $sql = "select * from ".self::SRPRIORITYTABLE;
        $serviceRequestPriority = sanitizedQueryBuilder($sql, $filter, null, null, null, true);

        return $serviceRequestPriority;
    }

    public function setSRPriority($priorityId=null, $name=null, $description=null, $active=null, $default=null){

        $queryData = array();
        if(!empty($name)){
            $queryData += array('SR_priority'=>$name);
        }
        if(!empty($active)){
            $queryData += array('priority_active'=>$active);
        }
        if(!empty($default)){
            $queryData += array('priority_default'=>$default);
        }

        $queryData += array('priority_description'=>$description);
        $condition = "id = '".sanitizeDBValues($priorityId)."'";

        $flag = func_array2update(self::SRPRIORITYTABLE, sanitizeDBValues($queryData),$condition);
        return $flag;
    }

    public function addSRPriority($name, $description=null, $active='Y', $default='N'){

        if(empty($name)){
            return false;
        }

        $queryData = array(
            'SR_priority'=>$name,
            'priority_description'=>$description,
            'priority_active'=>$active,
            'priority_default'=>$default,
        );
        return func_array2insert(self::SRPRIORITYTABLE,sanitizeDBValues($queryData));
    }

    public function isValidSRPriority($priorityName,$priorityId=null){

        //check for same priority name
        $priority = $this->getSRPriority(array('SR_priority'=>$priorityName));        
        if(is_array($priority)){
            foreach($priority as $idx=>$p){

                $pName = strtolower($p['SR_priority']);
                $priorityName = strtolower($priorityName);

                //validate while updating
                if($priorityId!=null){                    
                    if($pName==$priorityName && $p['id']!=$priorityId){                        
                        return false;
                    }

                } else {
                    if($pName==$priorityName){
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /* checks for at least 1 priority be made as default
     * @param:(char)$$flag - 'Y'/'N'
     * @return:(bool)
     */
    public function isDefaultSRPrioritySet($defaultFlag, $id=null){

        $flag = true;

        //if the current priority(add/update) is selected as default
        if($defaultFlag == 'Y'){
            $sql = "update ".self::SRPRIORITYTABLE." set priority_default='N'";
            $flag = db_query($sql);

        } else {
            $sql = "select count(id) as count from ".self::SRPRIORITYTABLE." where priority_default='Y'";
            if(!empty($id)){
                $sql .= " AND id!='".$id."'";
            }
            $count = func_query_first_cell($sql, true);
            if($count < 1){
                $flag = false;
            }
        }

        return $flag;

    }


}


final class SRStatus{

    const SRSTATUSTABLE = "mk_sr_status";

    public function getSRStatus($filter=null){

        $sql = "select * from ".self::SRSTATUSTABLE;
        $serviceRequestStatus = sanitizedQueryBuilder($sql, $filter, null, null, null, true);

        return $serviceRequestStatus;
    }
    
    /*
     * gets the status for status name
     * @param:(string)$status - status name
     * @return:(array)$status - all info of status
     */
    public function getSRStatusByName($statusName){
        $sql = "select * from ".self::SRSTATUSTABLE." where SR_status='".$statusName."'";

        return func_query($sql, true);
    }

    public function setSRStatus($statusId, $name=null, $description=null, $active=null){
        $queryData = array();
        if(!empty($name)){
            $queryData += array('SR_status'=>$name);
        }
        if(!empty($active)){
            $queryData += array('status_active'=>$active);
        }

        $queryData += array('status_description'=>$description);
        $condition = "id = '".sanitizeDBValues($statusId)."'";
        $flag = func_array2update(self::SRSTATUSTABLE, sanitizeDBValues($queryData),$condition);
        return $flag;
    }

    public function addSRStatus($name, $description=null, $active='Y'){
        if(empty($name)){
            return false;
        }

        $queryData = array(
            'SR_status'=>$name,
            'status_description'=>$description,
            'status_active'=>$active
        );
        return func_array2insert(self::SRSTATUSTABLE,sanitizeDBValues($queryData));
    }

    public function isValidSRStatus($statusName,$statusId=null){

        //check for same status name
        $status = $this->getSRStatus(array('SR_status'=>$statusName));
        if(is_array($status)){
            foreach($status as $idx=>$s){

                $sName = strtolower($s['SR_status']);
                $statusName = strtolower($statusName);

                //validate while updating
                if($statusId!=null){
                    if($sName==$statusName && $s['id']!=$statusId){
                        return false;
                    }

                } else {
                    if($sName==$statusName){
                        return false;
                    }
                }
            }
        }
        return true;
    }
}


class SRCategory{

    const SRCATEGORYTABLE = "mk_sr_categories";

    public function getSRCategory($filter=null){

        $sql = "select
                    id, SR_category, category_description, owners, SLA_in_sec, category_type, cat_active
                from
                    ".self::SRCATEGORYTABLE;
        $serviceRequestCategories = sanitizedQueryBuilder($sql, $filter, null, null, null, true);
        
        return $serviceRequestCategories;
    }

    /*
     * gets the category for category name
     * @param:(string)$category - category name
     * @return:(array)$category - all info of category
     */
    public function getSRCategoryByName($categoryName){
        $sql = "select * from ".self::SRCATEGORYTABLE." where lower(SR_category)=lower('".$categoryName."')";

        return func_query($sql, true);
    }

    public function setSRCategory($categoryId, $name=null,$categoryType=null, $SLAinSec=null,  $description=null, $active=null, $owners=null){

        $queryData = array();
        if(!empty($name)){
            $queryData += array('SR_category'=>$name);
        }
        if(!empty($categoryType)){
            $queryData += array('category_type'=>$categoryType);
        }
        if(!empty($SLAinSec)){
            $queryData += array('SLA_in_sec'=>$SLAinSec);
        }
        if(!empty($active)){
            $queryData += array('cat_active'=>$active);
        }
        $queryData += array('category_description'=>$description);
        $queryData += array('owners'=>$owners);
        
        $condition = "id = '".sanitizeDBValues($categoryId)."'";

        $flag = func_array2update(self::SRCATEGORYTABLE, sanitizeDBValues($queryData),$condition);
        return $flag;
    }

    public function addSRCategory($catNname, $catType, $SLAinSec, $description=null, $active='Y', $owners=null){

        if(empty($catNname) || empty($catType)){//|| is_null($SLAinSec
            return false;
        }

        $queryData = array(
            'SR_category'=>$catNname,
            'category_type'=>$catType,
            'SLA_in_sec'=>$SLAinSec,
            'category_description'=>$description,
            'cat_active'=>$active,
            'owners'=>$owners
        );

        return func_array2insert(self::SRCATEGORYTABLE,sanitizeDBValues($queryData));
    }

    public function isValidSRCategory($categoryName, $categoryType, $categoryId=null){

        //check for same status name
        $category = $this->getSRCategoryByName($categoryName);
        if(is_array($category)){
            foreach($category as $idx=>$c){

                $cName = strtolower($c['SR_category']);
                $categoryName = strtolower($categoryName);
                $cType = strtolower($c['category_type']);
                $categoryType = strtolower($categoryType);

                //validate while updating
                if($categoryId!=null){
                    if($cName==$categoryName && $cType==$categoryType && $c['id']!=$categoryId){
                        return false;
                    }

                } else {
                    if($cName==$categoryName && $cType==$categoryType){
                        return false;
                    }
                }
            }
        }
        return true;
    }
}


class SRSubCategory{

    const SRSUBCATEGORYTABLE = "mk_sr_subcategories";

    public function getSRSubCategory($categoryId, $subCategoryId=null){

        if($subCategoryId == null){
            $sql = "select                        
                        sc.id, sc.SR_subcategory, sc.subcategory_description, sc.owners,
                        IFNULL(sc.SLA_in_sec,c.SLA_in_sec) as SLA_in_sec, subcat_active
                    from
                        ".self::SRSUBCATEGORYTABLE." as sc,
                        ".SRCategory::SRCATEGORYTABLE." as c
                    where
                        c.id=sc.SR_category_id
                    and
                        c.id='".sanitizeDBValues($categoryId)."'";
        } else {
            $sql = "select
                        sc.id, sc.SR_subcategory, sc.subcategory_description, sc.owners,
                        IFNULL(sc.SLA_in_sec,c.SLA_in_sec) as SLA_in_sec, subcat_active
                    from
                        ".self::SRSUBCATEGORYTABLE." as sc,
                        ".SRCategory::SRCATEGORYTABLE." as c
                    where
                        c.id=sc.SR_category_id
                    and
                        c.id='".sanitizeDBValues($categoryId)."'
                    and
                        sc.id='".sanitizeDBValues($subCategoryId)."'";
        }

        return func_query($sql, true);
    }


    public function getAllSRSubCategoryViewList(){

        $sql = "select
                    sc.id as subcat_id, sc.SR_subcategory, sc.subcategory_description, sc.owners,
                    sc.SLA_in_sec, subcat_active , c.id as cat_id, c.SR_category
                from
                    ".self::SRSUBCATEGORYTABLE." as sc,
                    ".SRCategory::SRCATEGORYTABLE." as c
                where
                    c.id=sc.SR_category_id";

        return func_query($sql, true);
    }

     public function getCategoryOnSubCategory($subCategoryId){
        $sql = "select
                    sc.id as sub_cat_id, sc.SR_subcategory, sc.subcategory_description, sc.owners as sub_cat_owners,
                    c.id as cat_id, c.SR_category, c.category_description, c.owners as cat_owners,
                    IFNULL(sc.SLA_in_sec,c.SLA_in_sec) as SLA_in_sec
                from
                    ".self::SRSUBCATEGORYTABLE." as sc,
                    ".SRCategory::SRCATEGORYTABLE." as c
                where
                    c.id=sc.SR_category_id
                and
                    sc.id='".sanitizeDBValues($subCategoryId)."'";
        return func_query($sql, true);
    }


    public function getCatSubCategoryOnName($subCategoryName, $catName, $active=null){
        $sql = "select
                    sc.id as sub_cat_id, sc.SR_subcategory, sc.subcategory_description, sc.owners as sub_cat_owners,
                    c.id as cat_id, c.SR_category, c.category_description, c.owners as cat_owners,
                    IFNULL(sc.SLA_in_sec,c.SLA_in_sec) as SLA_in_sec
                from
                    ".self::SRSUBCATEGORYTABLE." as sc,
                    ".SRCategory::SRCATEGORYTABLE." as c
                where
                    c.id=sc.SR_category_id
                and
                    sc.SR_subcategory='".sanitizeDBValues($subCategoryName)."'
                and
                    c.SR_category='".sanitizeDBValues($catName)."'";
        // get only active act and subcat
        if(!empty($active)){
            $sql .= " and c.cat_active = 'Y' and sc.subcat_active='Y' ";
        }

        return func_query($sql, true);
    }

    /*
     * gets the subcategory for sub-category name and cat-id
     * @param:(string)$sub-category - subcategory name
     * @param:(int)$category - category id
     * @return:(array)$subcategory - all info of sub-category
     */
    public function getSRCategoryByNameAndCatId($subCategoryName, $categoryId){
        $sql = "select * from ".self::SRSUBCATEGORYTABLE." where lower(SR_subcategory)=lower('".$subCategoryName."') and SR_category_id='".$categoryId."'";

        return func_query($sql, true);
    }

    public function setSRSubCategory($subCatId, $name=null, $categoryId, $SLAinSec=null,  $description=null, $active=null, $owners=null){

        $queryData = array();
        if(!empty($name)){
            $queryData += array('SR_subcategory'=>$name);
        }
        if(!empty($categoryId)){
            $queryData += array('SR_category_id'=>$categoryId);
        }
        if(!empty($SLAinSec)){
            $queryData += array('SLA_in_sec'=>$SLAinSec);
        }
        if(!empty($active)){
            $queryData += array('subcat_active'=>$active);
        }
        $queryData += array('subcategory_description'=>$description);
        $queryData += array('owners'=>$owners);

        $condition = "id = '".sanitizeDBValues($subCatId)."'";

        $flag = func_array2update(self::SRSUBCATEGORYTABLE, sanitizeDBValues($queryData),$condition);
        return $flag;
    }

    public function addSRSubCategory($subCatNname, $catId, $SLAinSec, $description=null, $active='Y', $owners=null){

        if(empty($subCatNname) || empty($catId)){//|| is_null($SLAinSec)
            return false;
        }

        $queryData = array(
            'SR_subcategory'=>$subCatNname,
            'SR_category_id'=>$catId,
            'SLA_in_sec'=>$SLAinSec,
            'subcategory_description'=>$description,
            'subcat_active'=>$active,
            'owners'=>$owners
        );

        return func_array2insert(self::SRSUBCATEGORYTABLE,sanitizeDBValues($queryData));
    }

    public function isValidSRSubCategory($subCategoryName, $categoryId, $subCategoryId=null){

        //check for same subcat name under a cat
        $subCategory = $this->getSRCategoryByNameAndCatId($subCategoryName,$categoryId);
        if(is_array($subCategory)){
            foreach($subCategory as $idx=>$sc){

                $scName = strtolower($sc['SR_subcategory']);
                $subCategoryName = strtolower($subCategoryName);                

                //validate while updating
                if($subCategoryId!=null){
                    if($scName==$subCategoryName && $sc['SR_category_id']==$categoryId && $sc['id']!=$subCategoryId){
                        return false;
                    }

                } else {
                    if($scName==$subCategoryName && $sc['SR_category_id']==$categoryId){
                        return false;
                    }
                }
            }
        }
        return true;
    }
}


class SRComment{

    const SRCOMMENTTABLE = "mk_sr_comments";

    public function getSRComment($SRId, $commentId=null){

        if($commentId == null){
            $sql = "select c.* from ".self::SRCOMMENTTABLE." as c, ".ServiceRequest::SRTABLE." as s where c.id=s.SR_id and s.SR_id='".sanitizeDBValues($SRId)."'";
        } else {
            $sql = "select c.* from ".self::SRCOMMENTTABLE." as c, ".ServiceRequest::SRTABLE." as s where c.id=s.SR_id and c.id='".sanitizeDBValues($commentId)."' and s.SR_id='".sanitizeDBValues($SRId)."'";
        }

        return  func_query($sql, true);
    }

    public function setSRComment($commentId, $comment, $commentBy, $commentDate){
        $queryData = array('comment'=>$comment,'comment_by'=>$commentBy,'comment_date'=>$commentDate);
        $condition = "id = '".sanitizeDBValues($commentId)."'";

        $flag = func_array2update(self::SRCOMMENTTABLE, sanitizeDBValues($queryData),$condition);
        return $flag;
        //TODO send notification on every SR action
    }

    public function addSRComment($comment, $commentBy, $commentDate){
        
        $queryData = array('comment'=>$comment,'comment_by'=>$commentBy,'comment_date'=>$commentDate);
        return func_array2insert(self::SRCOMMENTTABLE, sanitizeDBValues($queryData));

        //TODO send notification on every SR action
    }    

    public function deleteSRComment($commentId){
        db_query("delete from ".self::SRCOMMENTTABLE." where id = '".sanitizeDBValues($commentId)."'");

        //TODO send notification on every SR action
    }

}

//TODO notification class
/*class SRNotification{

}*/