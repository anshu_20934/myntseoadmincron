<?php

/**
 * The class helps in notifying all exceptions related to SR 
 * @author: Arun Kumar K
 *
 */
class ServiceRequestException extends Exception
{
    private static $SREXCEPTIONS = array(
        'NOT_VALID_ORDER_SR'=>'Service Request is already raised for the category/subcategory or wrong category or sub-category.',
        'NOT_SUPER_ADMIN'=>'User has to be super admin to commit this action.',
        'NOT_VALID_CALLBACK_TIME'=>'Callback time can not be a past time',
        
        'REF_TYPE_ERROR'=>'Please select refund type.',
        'REF_VAL_ERROR'=>'Please select approriate refund values.',
        'REF_COUPON_NOT_EXIST'=>'Coupon does not exists.',
        'REF_COUPON_DETAIL_ERROR'=>'Please provide all necessary detail for coupon refund.',
        'REF_CASH_DETAIL_ERROR'=>'Please provide all necessary detail for cash refund.',
        'REF_CASHBACK_DETAIL_ERROR'=>'Please provide all necessary detail for cashback refund.',
        'REF_CHEQUE_DETAIL_ERROR'=>'Please provide all necessary detail for cheque refund.',
        'REF_ONLINE_DETAIL_ERROR'=>'Please provide all necessary detail for online refund.',

        'COMP_TYPE_ERROR'=>'Please select compensation type.',
        'COMP_VAL_ERROR'=>'Please select compensation value(gift/coupon).',
        'COMP_COUPON_NOT_EXIST'=>'Coupon does not exists',
        'COMP_COUPON_DETAIL_ERROR'=>'Please provide all necessary detail for coupon compensation.',
        'COMP_GIFT_DETAIL_ERROR'=>'Please provide all necessary detail for gift compensation.',

        'CREATE_SR_ERROR'=>'Service Request can not be created, please try again.',
        'UPDATE_SR_ERROR'=>'Service Request can not be updated, please try again.',
        'CLOSE_SR_ERROR'=>'Service Request can not be closed, please try again.',
        'DELETED_SR'=>'Service Request is already deleted.',
        'DELETE_SR_ERROR'=>'Service Request can not be deleted, please try again.',

        'CREATE_ORDER_SR_ERROR'=>'Order Service Request can not be created, please try again.',
        'UPDATE_ORDER_SR_ERROR'=>'Order Service Request can not be updated, please try again.',

        'CREATE_NON-ORDER_SR_ERROR'=>'Non-Order Service Request can not be created, please try again.',
        'UPDATE_NON_ORDER_SR_ERROR'=>'Non-Order Service Request can not be updated, please try again.',

        'CREATE_SR_PRIORITY_ERROR'=>'Service Request priority can not be added, please try again.',
        'UPDATE_SR_PRIORITY_ERROR'=>'Service Request priority can not be updated, please try again.',
        'NOT_VALID_SR_PRIORITY'=>'The Priority already exists.',
        'SR_PRIORITY_BLANK'=>'Priority should not be blank.',
        'NO_DEFAULT_SR_PRIORITY'=>'At least any one priority should be chosen as default',

        'CREATE_SR_STATUS_ERROR'=>'Service Request status can not be added, please try again.',
        'UPDATE_SR_STATUS_ERROR'=>'Service Request status can not be updated, please try again.',
        'NOT_VALID_SR_STATUS'=>'The Status already exists.',
        'SR_STATUS_BLANK'=>'Status should not be blank.',

        'SLA_CATEGORY_IS_NULL'=>'SLA for category is null',
        'CREATE_SR_CATEGORY_ERROR'=>'Service Request category can not be added, please try again.',
        'UPDATE_SR_CATEGORY_ERROR'=>'Service Request category can not be updated, please try again.',
        'NOT_VALID_SR_CATEGORY'=>'The Category already exists.',
        'SR_CATEGORY_BLANK'=>'Category should not be blank.',

        'SLA_SUBCATEGORY_IS_NULL'=>'SLA for sub-category is null',
        'CREATE_SR_SUBCATEGORY_ERROR'=>'Service Request sub-category can not be added, please try again.',
        'UPDATE_SR_SUBCATEGORY_ERROR'=>'Service Request sub-category can not be updated, please try again.',
        'NOT_VALID_SR_SUBCATEGORY'=>'The sub-category already exists.',
        'SR_SUBCATEGORY_BLANK'=>'Sub-Category should not be blank.',

        //can add anymore exception key=>value pairs here ...

        );
	/**
	 * Constructor.
	 * 
	 * @param String $message Error message.
	 * @param int $code Error code.
	 */
	public function __construct($keyException){
        
	    parent::__construct(self::$SREXCEPTIONS[$keyException], $code=null, $previous=null);
	}

	
}
