<?php
/**
 * The class exposes abstract APIs for handling non-order related service requests
 * @author: Arun Kumar K
 * legends:
 *      SR - service request
 *      nosr - non-order service request
 */
include_once($xcart_dir."/modules/serviceRequest/ServiceRequest.php");


class SRNonOrder extends ServiceRequest{

    const SRNONORDERTABLE = "mk_non_order_sr";  

    function __construct(){
        parent::__construct();
    }


    //before nonorderSR add parent SR
    public function createNonOrderSR($SRId, $customerEmail=null, $customerName=null, $customerPhone=null){

        $queryData = array('SR_id'=>$SRId);        
        $queryData += array('customer_email'=>$customerEmail);
        $queryData += array('first_name'=>$customerName);
        $queryData += array('mobile'=>$customerPhone);

        return func_array2insert(self::SRNONORDERTABLE,sanitizeDBValues($queryData));

        //send notification on every SR action

    }

    //before nonorderSR update parent SR
    public function updateNonOrderSR($id, $customerEmail=null, $customerName=null, $customerPhone=null){

        $queryData = array();
        $queryData += array("customer_email" => $customerEmail) ;//not compulsory hence can be updated by a null value
        $queryData += array('first_name'=>$customerName);
        $queryData += array('mobile'=>$customerPhone);

        $condition = "id = '".sanitizeDBValues($id)."'";

        $flag = func_array2update(self::SRNONORDERTABLE, sanitizeDBValues($queryData),$condition);
        return $flag;

        //send notification on every SR action
    }    

    /*
     * count of SRs
     * @return:(int)$count
     */
    public function getNonOrderSRCount($filter=null){
        $sql = "select
                    count(sr.SR_id) as SR_count
                from
                    ".self::SRNONORDERTABLE. " nosr
                inner join
                    ".self::SRTABLE." sr
                on
                    sr.SR_id=nosr.SR_id
                left join
                    ".SRCategory::SRCATEGORYTABLE." c
                on
                    c.id=sr.category_id
                left join
                    ".SRSubCategory::SRSUBCATEGORYTABLE." sc
                on
                    sc.id=sr.sub_category_id
                left join
                    ".SRPriority::SRPRIORITYTABLE." p
                on
                    p.id=sr.priority
                left join
                    ".SRStatus::SRSTATUSTABLE." s
                on
                    s.id=sr.status
                where
                    sr.sr_active='Y'";

        if($filter!=null){

            if (!empty($filter['id'])) {
                $nonOrderSRId = $filter['id'];
                $sql .= " AND nosr.id = '".$nonOrderSRId."'";
            }
            if (!empty($filter['fSRid'])) {
                $SRId = $filter['fSRid'];
                $sql .= " AND sr.SR_id = '".$SRId."'";
            }
            if (!empty($filter['fCat'])) {
                $sql .= " AND sr.category_id = '".sanitizeDBValues($filter['fCat'])."'";
            }
            if (!empty($filter['fSCat'])) {
                $sql .= " AND sr.sub_category_id = '".sanitizeDBValues($filter['fSCat'])."'";
            }
            if (!empty($filter['fStat'])) {
                $sql .= " AND sr.status = '".sanitizeDBValues($filter['fStat'])."'";
            }
            if (!empty($filter['SR_department'])) {
                $sql .= " AND sr.SR_department = '".sanitizeDBValues($filter['SR_department'])."'";
            }
            if (!empty($filter['SR_channel'])) {
                $sql .= " AND sr.SR_channel = '".sanitizeDBValues($filter['SR_channel'])."'";
            }
            if (!empty($filter['fStartCallback'])) {
                $callbackTime = $filter['fStartCallback'];
                $sql .= " AND sr.callbacktime >= '".sanitizeDBValues($callbackTime)."'";
            }
            if (!empty($filter['fEndCallback'])) {
                $callbackTime = $filter['fEndCallback'];
                $sql .= " AND sr.callbacktime <= '".sanitizeDBValues($callbackTime)."'";
            }
            if (!empty($filter['fStartCrDate'])) {
                $createTime = $filter['fStartCrDate'];
                $sql .= " AND sr.created_date >= '".sanitizeDBValues($createTime)."'";
            }
            if (!empty($filter['fEndCrDate'])) {
                $createTime = $filter['fEndCrDate'];
                $sql .= " AND sr.created_date <= '".sanitizeDBValues($createTime)."'";
            }
            if (!empty($filter['fStartDuDate'])) {
                $dueTime = $filter['fStartDuDate'];
                $sql .= " AND sr.due_date >= '".sanitizeDBValues($dueTime)."'";
            }
            if (!empty($filter['fEndDuDate'])) {
                $dueTime = $filter['fEndDuDate'];
                $sql .= " AND sr.due_date <= '".sanitizeDBValues($dueTime)."'";
            }
            if (!empty($filter['fStartClDate'])) {
                $closeTime = $filter['fStartClDate'];
                $sql .= " AND sr.closed_date >= '".sanitizeDBValues($closeTime)."'";
            }
            if (!empty($filter['fEndClDate'])) {
                $closeTime = $filter['fEndClDate'];
                $sql .= " AND sr.closed_date <= '".sanitizeDBValues($closeTime)."'";
            }
            if (!empty($filter['fAge'])) {
                $createdBack = time()-($filter['fAge']*86400);//so many days back
                $sql .= " AND sr.created_date <= '".sanitizeDBValues($createdBack)."'";
            }
            if (!empty($filter['fRepo'])) {
                $sql .= " AND sr.reporter LIKE '%".sanitizeDBValues($filter['fRepo'])."%'";
            }
            if (!empty($filter['fReso'])) {
                $sql .= " AND sr.resolver LIKE '%".sanitizeDBValues($filter['fReso'])."%'";
            }
            if (!empty($filter['fCust'])) {
                $sql .= " AND nosr.customer_email LIKE '%".sanitizeDBValues($filter['fCust'])."%'";
            }            
        }

        $NonOrderServiceRequests = sanitizedQueryBuilder($sql, null, null, null, null, true);

        return $NonOrderServiceRequests[0]['SR_count'];
    }

    public function getNonOrderSR($filter=NULL,$order=NULL,$offset=NULL,$limit=NULL){

        $sql = "select
                    nosr.id, nosr.customer_email,nosr.first_name,nosr.mobile,
                    sr.SR_id, sr.customer_SR_id, sr.category_id, c.SR_category, sr.sub_category_id, sc.SR_subcategory,
                    sr.priority as priority_id , p.SR_priority, sr.status as status_id, s.SR_status, sr.SR_department, sr.SR_channel,
                    IF((sr.callbacktime IS NULL OR sr.callbacktime=0),'NA',from_unixtime(sr.callbacktime,'%Y-%m-%d %H:%i')) as callbacktime_date,
                    IF((sr.callbacktime IS NULL OR sr.callbacktime=0),'',sr.callbacktime) as callbacktime,
                    sr.created_date as createtime,sr.due_date as duetime,sr.closed_date as closetime,
                    from_unixtime(sr.created_date,'%b %D, %Y') as created_date,                      
                    IF((sr.due_date IS NULL OR sr.due_date=0),'',from_unixtime(sr.due_date,'%b %D, %Y')) as due_date,
                    IF((sr.modified_date IS NULL OR sr.modified_date=0),'',from_unixtime(sr.modified_date,'%b %D, %Y')) as modified_date,
                    from_unixtime(sr.closed_date,'%b %D, %Y') as closed_date, sr.TAT,
                    sr.reporter, sr.assignee, sr.updatedby, sr.resolver, sr.create_summary, sr.close_summary,
                    sr.copyto, sr.notification_status, sr.sr_active
                from
                    ".self::SRNONORDERTABLE. " nosr
                inner join
                    ".parent::SRTABLE." sr
                on
                    sr.SR_id=nosr.SR_id
                left join
                    ".SRCategory::SRCATEGORYTABLE." c
                on
                    c.id=sr.category_id
                left join
                    ".SRSubCategory::SRSUBCATEGORYTABLE." sc
                on
                    sc.id=sr.sub_category_id
                left join
                    ".SRPriority::SRPRIORITYTABLE." p
                on
                    p.id=sr.priority
                left join
                    ".SRStatus::SRSTATUSTABLE." s
                on
                    s.id=sr.status
                WHERE
                    sr.sr_active='Y'";

        if($filter!=null){

            if (!empty($filter['id'])) {
                $nonOrderSRId = $filter['id']; 
                $sql .= " AND nosr.id = '".$nonOrderSRId."'";
            }
            if (!empty($filter['fSRid'])) {
                $SRId = $filter['fSRid']; 
                $sql .= " AND sr.SR_id = '".$SRId."'";
            }            
            if (!empty($filter['fCat'])) {
                $sql .= " AND sr.category_id = '".sanitizeDBValues($filter['fCat'])."'";
            }
            if (!empty($filter['fSCat'])) {
                $sql .= " AND sr.sub_category_id = '".sanitizeDBValues($filter['fSCat'])."'";
            }
            if (!empty($filter['fStat'])) {
                $sql .= " AND sr.status = '".sanitizeDBValues($filter['fStat'])."'";
            }
            if (!empty($filter['SR_department'])) {
                $sql .= " AND sr.SR_department = '".sanitizeDBValues($filter['SR_department'])."'";
            }
            if (!empty($filter['SR_channel'])) {
                $sql .= " AND sr.SR_channel = '".sanitizeDBValues($filter['SR_channel'])."'";
            }
            if (!empty($filter['fStartCallback'])) {
                $callbackTime = $filter['fStartCallback'];
                $sql .= " AND sr.callbacktime >= '".sanitizeDBValues($callbackTime)."'";
            }
            if (!empty($filter['fEndCallback'])) {
                $callbackTime = $filter['fEndCallback'];
                $sql .= " AND sr.callbacktime <= '".sanitizeDBValues($callbackTime)."'";
            }
            if (!empty($filter['fStartCrDate'])) {
                $createTime = $filter['fStartCrDate'];
                $sql .= " AND sr.created_date >= '".sanitizeDBValues($createTime)."'";
            }
            if (!empty($filter['fEndCrDate'])) {
                $createTime = $filter['fEndCrDate'];
                $sql .= " AND sr.created_date <= '".sanitizeDBValues($createTime)."'";
            }
            if (!empty($filter['fStartDuDate'])) {
                $dueTime = $filter['fStartDuDate'];
                $sql .= " AND sr.due_date >= '".sanitizeDBValues($dueTime)."'";
            }
            if (!empty($filter['fEndDuDate'])) {
                $dueTime = $filter['fEndDuDate'];
                $sql .= " AND sr.due_date <= '".sanitizeDBValues($dueTime)."'";
            }
            if (!empty($filter['fStartClDate'])) {
                $closeTime = $filter['fStartClDate'];
                $sql .= " AND sr.closed_date >= '".sanitizeDBValues($closeTime)."'";
            }
            if (!empty($filter['fEndClDate'])) {
                $closeTime = $filter['fEndClDate'];
                $sql .= " AND sr.closed_date <= '".sanitizeDBValues($closeTime)."'";
            }
            if (!empty($filter['fAge'])) {
                $createdBack = time()-($filter['fAge']*86400);//so many days back
                $sql .= " AND sr.created_date <= '".sanitizeDBValues($createdBack)."'";
            }
            if (!empty($filter['fRepo'])) {
                $sql .= " AND sr.reporter LIKE '%".sanitizeDBValues($filter['fRepo'])."%'";
            }
            if (!empty($filter['fReso'])) {
                $sql .= " AND sr.resolver LIKE '%".sanitizeDBValues($filter['fReso'])."%'";
            }
            if (!empty($filter['fCust'])) {
                $sql .= " AND nosr.customer_email LIKE '%".sanitizeDBValues($filter['fCust'])."%'";
            }
        }

        $NonOrderServiceRequests = sanitizedQueryBuilder($sql, $filter=null, $order, $offset, $limit, true);

        if(!is_array($NonOrderServiceRequests)){
           return array();
        }
        
        return $NonOrderServiceRequests;
    }
}