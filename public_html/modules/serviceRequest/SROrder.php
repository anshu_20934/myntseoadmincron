<?php
/**
 * The class exposes abstract APIs for handling order related service requests
 * @author: Arun Kumar K
 * legends:
 *      SR - service request
 *      osr - order service request 
 */
include_once($xcart_dir."/include/func/func.db.php");
include_once($xcart_dir."/modules/serviceRequest/ServiceRequest.php");
include_once($xcart_dir."/modules/coupon/database/CouponAdapter.php");
include_once($xcart_dir."/modules/coupon/CouponValidator.php");


class SROrder extends ServiceRequest{

    const SRORDERTABLE = "mk_order_sr";
    private $orderId;
    public $item;
    public $customer;

    function __construct(){
        parent::__construct();
    }

    public function initializeSRForOrder($orderId){
        $this->orderId = $orderId;
        $this->_setItem($orderId);
        $this->_setCustomer($orderId);
    }
    
    private function _setItem($orderId){

        $sql = "select
                    s.article_number as id,s.product_display_name as name,
                    CASE od.item_status
                        WHEN 'A' THEN 'Assigned'
                        WHEN 'D' THEN 'Done'
                        WHEN 'IC' THEN 'Cancelled'
                        WHEN 'H' THEN 'On Hold'
                        WHEN 'I' THEN 'In-progress'
                        WHEN 'L' THEN 'Lost'
                        WHEN 'N' THEN 'New'
                        WHEN 'OD' THEN 'Ops Done'
                        WHEN 'QD' THEN 'QA Done'
                        WHEN 'R' THEN 'Returned'
                        WHEN 'S' THEN 'Sent Outside'
                        WHEN 'UA' THEN 'Un-assigned'
                    END AS item_status
                from mk_style_properties s
                inner join xcart_products p on p.product_style_id=s.style_id
                inner join xcart_order_details od on od.productid=p.productid
                inner join xcart_orders o on o.orderid=od.orderid
                where o.orderid='".$orderId."'";
        $this->item = func_query($sql, true);
    }

    private function _setCustomer($orderId){

        $sql = "select login from xcart_orders where orderid='".$orderId."'";
        $this->customer = func_query($sql, true);

    }    

    //before orderSR add do a check for validity of orderSR(cat/subcat) and add parent SR
    public function createOrderSR($SRId, $items, $refund='N', $refundType=null, $refundValue=null,
                                  $compensation='N', $compensationType=null, $compensationValue=null){

        $queryData = array('SR_id'=>$SRId);
        $queryData += array('order_id'=>$this->orderId,'items'=>$items);
        $queryData += array('customer_email'=>$this->customer[0]['login']);
        $queryData += array('refund'=>$refund,'compensation'=>$compensation);

        if($refund == 'N'){
            $refundType=null;
            $refundValue=null;
        }
        $queryData += array('refund_type'=>$refundType);
        $queryData += array('refund_value'=>$refundValue);

        if($compensation == 'N'){
            $compensationType=null;
            $compensationValue=null;
        }       
        $queryData += array('compensation_type'=>$compensationType);
        $queryData += array('compensation_value'=>$compensationValue);

        return func_array2insert(self::SRORDERTABLE,sanitizeDBValues($queryData));

        //send notification on every SR action

    }

    //before orderSR update do a check for validity of orderSR(cat/subcat) and update parent SR
    public function updateOrderSR($id, $items=null, $refund=null, $refundType=null, $refundValue=null,  
                                  $compensation=null, $compensationType=null, $compensationValue=null){

        $queryData = array();
        if(!empty($items)){
            $queryData+=array("items" => $items);
        }

        if(empty($refund)){
            $refund = 'N';
        }
        $queryData+=array("refund" => $refund);//can be null
        if($refund == 'N'){
            $refundType=null;
            $refundValue=null;
        }
        $queryData += array('refund_type'=>$refundType);
        $queryData += array('refund_value'=>$refundValue);

        if(empty($compensation)){
            $compensation = 'N';
        }
        $queryData+=array("compensation" => $compensation);//can be null
        if($compensation == 'N'){
            $compensationType=null;
            $compensationValue=null;
        }
        //not compulsory hence can be updated by a null value
        $queryData += array('compensation_type'=>$compensationType);
        $queryData += array('compensation_value'=>$compensationValue);

        $condition = "id = '".sanitizeDBValues($id)."'";

        $flag = func_array2update(self::SRORDERTABLE, sanitizeDBValues($queryData),$condition);
        return $flag;

        //send notification on every SR action
    }   

    /*
     * count of order SRs
     * @return:(int)$count
     */
    public function getOrderSRCount($filter=NULL){
        $sql = "select
                    count(osr.id) as SR_count
                from
                    ".self::SRORDERTABLE. " osr
                inner join
                    ".parent::SRTABLE." sr
                on
                    sr.SR_id=osr.SR_id
                left join
                    ".SRCategory::SRCATEGORYTABLE." c
                on
                    c.id=sr.category_id
                left join
                    ".SRSubCategory::SRSUBCATEGORYTABLE." sc
                on
                    sc.id=sr.sub_category_id
                left join
                    ".SRPriority::SRPRIORITYTABLE." p
                on
                    p.id=sr.priority
                left join
                    ".SRStatus::SRSTATUSTABLE." s
                on
                    s.id=sr.status
                where
                    sr.sr_active='Y'";

        if($filter!=null){

            if (!empty($filter['id'])) {
                $orderSRId = $filter['id'];
                $sql .= " AND osr.id = '".$orderSRId."'";
            }
            if (!empty($filter['order_id'])) {
                $orderId = $filter['order_id'];
                $sql .= " AND osr.order_id = '".$orderId."'";
            }
            if (!empty($filter['fSRid'])) {
                $SRId = $filter['fSRid'];
                $sql .= " AND sr.SR_id = '".$SRId."'";
            }
            if (!empty($filter['fCat'])) {
                $sql .= " AND sr.category_id = '".sanitizeDBValues($filter['fCat'])."'";
            }
            if (!empty($filter['fSCat'])) {
                $sql .= " AND sr.sub_category_id = '".sanitizeDBValues($filter['fSCat'])."'";
            }
            if (!empty($filter['fStat'])) {
                $sql .= " AND sr.status = '".sanitizeDBValues($filter['fStat'])."'";
            }
            if (!empty($filter['SR_department'])) {
                $sql .= " AND sr.SR_department = '".sanitizeDBValues($filter['SR_department'])."'";
            }
            if (!empty($filter['SR_channel'])) {
                $sql .= " AND sr.SR_channel = '".sanitizeDBValues($filter['SR_channel'])."'";
            }
            if (!empty($filter['fStartCallback'])) {
                $callbackTime = $filter['fStartCallback'];
                $sql .= " AND sr.callbacktime >= '".sanitizeDBValues($callbackTime)."'";
            }
            if (!empty($filter['fEndCallback'])) {
                $callbackTime = $filter['fEndCallback'];
                $sql .= " AND sr.callbacktime <= '".sanitizeDBValues($callbackTime)."'";
            }
            if (!empty($filter['fStartCrDate'])) {
                $createTime = $filter['fStartCrDate'];
                $sql .= " AND sr.created_date >= '".sanitizeDBValues($createTime)."'";
            }
            if (!empty($filter['fEndCrDate'])) {
                $createTime = $filter['fEndCrDate'];
                $sql .= " AND sr.created_date <= '".sanitizeDBValues($createTime)."'";
            }
            if (!empty($filter['fStartDuDate'])) {
                $dueTime = $filter['fStartDuDate'];
                $sql .= " AND sr.due_date >= '".sanitizeDBValues($dueTime)."'";
            }
            if (!empty($filter['fEndDuDate'])) {
                $dueTime = $filter['fEndDuDate'];
                $sql .= " AND sr.due_date <= '".sanitizeDBValues($dueTime)."'";
            }
            if (!empty($filter['fStartClDate'])) {
                $closeTime = $filter['fStartClDate'];
                $sql .= " AND sr.closed_date >= '".sanitizeDBValues($closeTime)."'";
            }
            if (!empty($filter['fEndClDate'])) {
                $closeTime = $filter['fEndClDate'];
                $sql .= " AND sr.closed_date <= '".sanitizeDBValues($closeTime)."'";
            }
            if (!empty($filter['fAge'])) {
                $createdBack = time()-($filter['fAge']*86400);//so many days back
                $sql .= " AND sr.created_date <= '".sanitizeDBValues($createdBack)."'";
            }
            if (!empty($filter['fRepo'])) {
                $sql .= " AND sr.reporter LIKE '%".sanitizeDBValues($filter['fRepo'])."%'";
            }
            if (!empty($filter['fReso'])) {
                $sql .= " AND sr.resolver LIKE '%".sanitizeDBValues($filter['fReso'])."%'";
            }
            if (!empty($filter['fCust'])) {
                $sql .= " AND osr.customer_email LIKE '%".sanitizeDBValues($filter['fCust'])."%'";
            }
            if (!empty($filter['fSRCompType'])) {
                $SRCompType = $filter['fSRCompType'];
                if($SRCompType == 'Y'){
                    $sql .= " AND osr.compensation='Y'";
                } else {
                    $sql .= " AND osr.compensation='N'";
                }
            }
        }

        $serviceRequests = sanitizedQueryBuilder($sql, null, null, null, null, true);
        return $serviceRequests[0]['SR_count'];
    }

    public function getOrderSR($filter=NULL,$order=NULL,$offset=NULL,$limit=NULL){

        $sql = "select
                    osr.id, osr.order_id, osr.items, osr.customer_email,
                    osr.refund, osr.refund_type, osr.refund_value,
                    osr.compensation, osr.compensation_type, osr.compensation_value,
                    sr.SR_id, sr.customer_SR_id, sr.category_id, c.SR_category, sr.sub_category_id, sc.SR_subcategory,
                    sr.priority as priority_id , p.SR_priority, sr.status as status_id, s.SR_status, sr.SR_department, sr.SR_channel,
                    IF((sr.callbacktime IS NULL OR sr.callbacktime=0),'NA',from_unixtime(sr.callbacktime,'%Y-%m-%d %H:%i')) as callbacktime_date,
                    IF((sr.callbacktime IS NULL OR sr.callbacktime=0),'',sr.callbacktime) as callbacktime,
                    sr.created_date as createtime,sr.due_date as duetime,sr.closed_date as closetime,
                    from_unixtime(sr.created_date,'%Y-%m-%d %H:%i') as created_date,
                    IF((sr.due_date IS NULL OR sr.due_date=0),'',from_unixtime(sr.due_date,'%Y-%m-%d %H:%i')) as due_date,
                    IF((sr.modified_date IS NULL OR sr.modified_date=0),'',from_unixtime(sr.modified_date,'%Y-%m-%d %H:%i')) as modified_date,
                    from_unixtime(sr.closed_date,'%Y-%m-%d %H:%i') as closed_date, sr.TAT,
                    sr.reporter, sr.assignee, sr.updatedby, sr.resolver, sr.create_summary, sr.close_summary,
                    sr.copyto, sr.notification_status, sr.sr_active,
                    IF((o.shippeddate IS NOT NULL AND completeddate IS NULL AND delivereddate IS NULL),unix_timestamp(now())-shippeddate,null) as delivery_ageing,
                    o.status, o.payment_method, o.courier_service, o.warehouseid,o.s_address as order_address,o.s_city as order_city,
                    o.s_state as order_state,o.s_zipcode as order_zipcode
                from
                    ".self::SRORDERTABLE. " osr
                inner join
                    ".parent::SRTABLE." sr
                on
                    sr.SR_id=osr.SR_id
                left join
                     xcart_orders o
                on
                    o.orderid=osr.order_id
                left join
                    ".SRCategory::SRCATEGORYTABLE." c
                on
                    c.id=sr.category_id
                left join
                    ".SRSubCategory::SRSUBCATEGORYTABLE." sc
                on
                    sc.id=sr.sub_category_id
                left join
                    ".SRPriority::SRPRIORITYTABLE." p
                on
                    p.id=sr.priority
                left join
                    ".SRStatus::SRSTATUSTABLE." s
                on
                    s.id=sr.status
                where
                    sr.sr_active='Y'";

        if($filter!=null){

            if (!empty($filter['id'])) {
                $orderSRId = $filter['id'];
                $sql .= " AND osr.id = '".$orderSRId."'";
            }
            if (!empty($filter['order_id'])) {
                $orderId = $filter['order_id'];
                $sql .= " AND osr.order_id = '".$orderId."'";
            }
            if (!empty($filter['fSRid'])) {
                $SRId = $filter['fSRid'];
                $sql .= " AND sr.SR_id = '".$SRId."'";
            }
            if (!empty($filter['fCat'])) {
                $sql .= " AND sr.category_id = '".sanitizeDBValues($filter['fCat'])."'";
            }
            if (!empty($filter['fSCat'])) {
                $sql .= " AND sr.sub_category_id = '".sanitizeDBValues($filter['fSCat'])."'";
            }
            if (!empty($filter['fStat'])) {
                $sql .= " AND sr.status = '".sanitizeDBValues($filter['fStat'])."'";
            }
            if (!empty($filter['SR_department'])) {
                $sql .= " AND sr.SR_department = '".sanitizeDBValues($filter['SR_department'])."'";
            }
            if (!empty($filter['SR_channel'])) {
                $sql .= " AND sr.SR_channel = '".sanitizeDBValues($filter['SR_channel'])."'";
            }
            if (!empty($filter['fStartCallback'])) {
                $callbackTime = $filter['fStartCallback'];
                $sql .= " AND sr.callbacktime >= '".sanitizeDBValues($callbackTime)."'";
            }
            if (!empty($filter['fEndCallback'])) {
                $callbackTime = $filter['fEndCallback'];
                $sql .= " AND sr.callbacktime <= '".sanitizeDBValues($callbackTime)."'";
            }
            if (!empty($filter['fStartCrDate'])) {
                $createTime = $filter['fStartCrDate'];
                $sql .= " AND sr.created_date >= '".sanitizeDBValues($createTime)."'";
            }
            if (!empty($filter['fEndCrDate'])) {
                $createTime = $filter['fEndCrDate'];
                $sql .= " AND sr.created_date <= '".sanitizeDBValues($createTime)."'";
            }
            if (!empty($filter['fStartDuDate'])) {
                $dueTime = $filter['fStartDuDate'];
                $sql .= " AND sr.due_date >= '".sanitizeDBValues($dueTime)."'";
            }
            if (!empty($filter['fEndDuDate'])) {
                $dueTime = $filter['fEndDuDate'];
                $sql .= " AND sr.due_date <= '".sanitizeDBValues($dueTime)."'";
            }
            if (!empty($filter['fStartClDate'])) {
                $closeTime = $filter['fStartClDate'];
                $sql .= " AND sr.closed_date >= '".sanitizeDBValues($closeTime)."'";
            }
            if (!empty($filter['fEndClDate'])) {
                $closeTime = $filter['fEndClDate'];
                $sql .= " AND sr.closed_date <= '".sanitizeDBValues($closeTime)."'";
            }
            if (!empty($filter['fAge'])) {
                $createdBack = time()-($filter['fAge']*86400);//so many days back
                $sql .= " AND sr.created_date <= '".sanitizeDBValues($createdBack)."'";
            }
            if (!empty($filter['fRepo'])) {
                $sql .= " AND sr.reporter LIKE '%".sanitizeDBValues($filter['fRepo'])."%'";
            }
            if (!empty($filter['fReso'])) {
                $sql .= " AND sr.resolver LIKE '%".sanitizeDBValues($filter['fReso'])."%'";
            }
            if (!empty($filter['fCust'])) {
                $sql .= " AND osr.customer_email LIKE '%".sanitizeDBValues($filter['fCust'])."%'";
            }
            if (!empty($filter['fSRCompType'])) {
                $SRCompType = $filter['fSRCompType'];
                if($SRCompType == 'Y'){
                    $sql .= " AND osr.compensation='Y'";
                } else {
                    $sql .= " AND osr.compensation='N'";
                }
            }
        }

        $orderServiceRequests = sanitizedQueryBuilder($sql, $filter=null, $order, $offset, $limit, true);

        if(is_array($orderServiceRequests)){
            foreach($orderServiceRequests as $idx=>$SR){
                //items array
                $orderServiceRequests[$idx]['items'] = explode(',',$SR['items']);
            }
        } else {
            $orderServiceRequests = array();
        }

        //get derived columns for SRs
        $orderServiceRequests = $this->getDerivedColumnsForSR($orderServiceRequests);

        return $orderServiceRequests;
    }

    /*
     * count of orders
     * @return:(int)$count
     */
    public function getOrderCount($filter=NULL){
        $sql = "select
                    count(distinct osr.order_id) as order_count
                from
                    ".self::SRORDERTABLE. " osr
                inner join
                    ".parent::SRTABLE." sr
                on
                    sr.SR_id=osr.SR_id
                left join
                    ".SRCategory::SRCATEGORYTABLE." c
                on
                    c.id=sr.category_id
                left join
                    ".SRSubCategory::SRSUBCATEGORYTABLE." sc
                on
                    sc.id=sr.sub_category_id
                left join
                    ".SRPriority::SRPRIORITYTABLE." p
                on
                    p.id=sr.priority
                left join
                    ".SRStatus::SRSTATUSTABLE." s
                on
                    s.id=sr.status
                where
                    sr.sr_active='Y'";

        if($filter!=null){

            if (!empty($filter['id'])) {
                $orderSRId = $filter['id'];
                $sql .= " AND osr.id = '".$orderSRId."'";
            }
            if (!empty($filter['fSRid'])) {
                $SRId = $filter['fSRid'];
                $sql .= " AND sr.SR_id = '".$SRId."'";
            }
            if (!empty($filter['fCat'])) {
                $sql .= " AND sr.category_id = '".sanitizeDBValues($filter['fCat'])."'";
            }
            if (!empty($filter['fSCat'])) {
                $sql .= " AND sr.sub_category_id = '".sanitizeDBValues($filter['fSCat'])."'";
            }
            if (!empty($filter['fStat'])) {
                $sql .= " AND sr.status = '".sanitizeDBValues($filter['fStat'])."'";
            }
            if (!empty($filter['SR_department'])) {
                $sql .= " AND sr.SR_department = '".sanitizeDBValues($filter['SR_department'])."'";
            }
            if (!empty($filter['SR_channel'])) {
                $sql .= " AND sr.SR_channel = '".sanitizeDBValues($filter['SR_channel'])."'";
            }
            if (!empty($filter['fStartCrDate'])) {
                $createTime = $filter['fStartCrDate'];
                $sql .= " AND sr.created_date >= '".sanitizeDBValues($createTime)."'";
            }
            if (!empty($filter['fEndCrDate'])) {
                $createTime = $filter['fEndCrDate'];
                $sql .= " AND sr.created_date <= '".sanitizeDBValues($createTime)."'";
            }
            if (!empty($filter['fStartDuDate'])) {
                $dueTime = $filter['fStartDuDate'];
                $sql .= " AND sr.due_date >= '".sanitizeDBValues($dueTime)."'";
            }
            if (!empty($filter['fEndDuDate'])) {
                $dueTime = $filter['fEndDuDate'];
                $sql .= " AND sr.due_date <= '".sanitizeDBValues($dueTime)."'";
            }
            if (!empty($filter['fStartClDate'])) {
                $closeTime = $filter['fStartClDate'];
                $sql .= " AND sr.closed_date >= '".sanitizeDBValues($closeTime)."'";
            }
            if (!empty($filter['fEndClDate'])) {
                $closeTime = $filter['fEndClDate'];
                $sql .= " AND sr.closed_date <= '".sanitizeDBValues($closeTime)."'";
            }
            if (!empty($filter['fAge'])) {
                $createdBack = time()-($filter['fAge']*86400);//so many days back
                $sql .= " AND sr.created_date <= '".sanitizeDBValues($createdBack)."'";
            }
            if (!empty($filter['fRepo'])) {
                $sql .= " AND sr.reporter LIKE '%".sanitizeDBValues($filter['fRepo'])."%'";
            }
            if (!empty($filter['fReso'])) {
                $sql .= " AND sr.resolver LIKE '%".sanitizeDBValues($filter['fReso'])."%'";
            }
            if (!empty($filter['fCust'])) {
                $sql .= " AND osr.customer_email LIKE '%".sanitizeDBValues($filter['fCust'])."%'";
            }
            if (!empty($filter['fSRCompType'])) {
                $SRCompType = $filter['fSRCompType'];
                if($SRCompType == 'Y'){
                    $sql .= " AND osr.compensation='Y'";
                } else {
                    $sql .= " AND osr.compensation='N'";
                }
            }
        }
        $serviceRequests = sanitizedQueryBuilder($sql, null, null, null, null, true);
        return $serviceRequests[0]['order_count'];
    }

     public function getNotClosedOrderSR($orderId){

        $sql = "select
                    osr.id, osr.order_id, osr.items, osr.customer_email, osr.compensation,
                    osr.compensation_type, osr.compensation_value,
                    sr.SR_id, sr.customer_SR_id, sr.category_id, c.SR_category, sr.sub_category_id, sc.SR_subcategory,
                    sr.priority as priority_id , p.SR_priority, sr.status as status_id, s.SR_status,
                    sr.due_date as duetime,
                    from_unixtime(sr.created_date,'%Y-%m-%d %H:%i') as created_date,
                    IF((sr.due_date IS NULL OR sr.due_date=0),'',from_unixtime(sr.due_date,'%Y-%m-%d %H:%i')) as due_date,
                    from_unixtime(sr.closed_date,'%Y-%m-%d %H:%i') as closed_date, sr.TAT,
                    sr.reporter, sr.assignee, sr.resolver, sr.create_summary, sr.close_summary,
                    sr.copyto, sr.notification_status
                from
                    ".self::SRORDERTABLE. " osr
                inner join
                    ".parent::SRTABLE." sr
                on
                    sr.SR_id=osr.SR_id
                left join
                    ".SRCategory::SRCATEGORYTABLE." c
                on
                    c.id=sr.category_id
                left join
                    ".SRSubCategory::SRSUBCATEGORYTABLE." sc
                on
                    sc.id=sr.sub_category_id
                left join
                    ".SRPriority::SRPRIORITYTABLE." p
                on
                    p.id=sr.priority
                left join
                    ".SRStatus::SRSTATUSTABLE." s
                on
                    s.id=sr.status
                where
                    s.SR_status!='close'
                and
                    osr.order_id='".$orderId."'
                and
                    sr.sr_active='Y'";

        $orderServiceRequests = sanitizedQueryBuilder($sql, null, null, null, null, true);

        if(is_array($orderServiceRequests)){
            foreach($orderServiceRequests as $idx=>$SR){
                //items array
                $orderServiceRequests[$idx]['items'] = explode(',',$SR['items']);
            }
        } else {
            $orderServiceRequests = array();
        }


        return $orderServiceRequests;
    }

    public function isValidOrderSR($orderId,$categoryId,$subCategoryId,$id=null){

        // order sr can not be created without proper cat & subcat id & orderid
        if(empty($categoryId) || empty($subCategoryId) || empty($orderId)){
            return false;
        }

        //check for same cat/subcat of SRs which are not closed
        $orderServiceRequests = $this->getNotClosedOrderSR($orderId);
        if(is_array($orderServiceRequests)){
            foreach($orderServiceRequests as $idx=>$SR){
                //validate while updating
                if($id!=null){

                    if($SR['category_id']==$categoryId && $SR['sub_category_id']==$subCategoryId && $SR['id']!=$id){
                        return false;
                    }

                } else {

                    if($SR['category_id']==$categoryId && $SR['sub_category_id']==$subCategoryId){
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public function getJSONCoupon($coupon){

        //coupon DB adaptor instance
        $couponAdapter = CouponAdapter::getInstance();

        //fetch the coupon.        
        try {
            $couponObj = $couponAdapter->fetchCoupon($coupon);
            $couponDetail = array(
                'coupon'=>$couponObj->getCode(),
                'minimum-order-amount'=>$couponObj->getMinimum(),
                'coupon-type'=>$couponObj->getType(),//couponType
                'free-shipping'=>$couponObj->isFreeShipping(),
                'absolute-discount'=>$couponObj->getAbsoluteDiscount(),//MRPdiscount
                'percentage-discount'=>$couponObj->getPercentDiscount(),//MRPpercentage
                'expired'=>$couponObj->isExpired(),//boolean
                'description'=>$couponObj->getDescription(),//description
            );
            
        } catch (CouponNotFoundException $ex) {
            return false;
        }
        return json_encode($couponDetail);
    }

    public function validateCoupon($coupon, $orderCustomer){

        //coupon DB adaptor instance
        $couponAdapter = CouponAdapter::getInstance();

        //validator instance
        $validator = CouponValidator::getInstance();

        //check for coupon existence.        
        try {
            $coupon = $couponAdapter->fetchCoupon($coupon);
            
        } catch (CouponNotFoundException $ex) {
            return $ex->getMessage();
        }

        //Check validity of the coupon for the user.
        try {
            return $validator->isCouponValidForUser($coupon, $orderCustomer);
            
        } catch (CouponExpiredException $ex) {
            return $ex->getMessage();
        }
        catch (CouponNotStartedException $ex) {
            return $ex->getMessage();
        }
        catch (CouponUsageExpiredException $ex) {
            return $ex->getMessage();
        }
        catch (CouponLockedException $ex) {
            return $ex->getMessage();
        }
        catch (CouponUsageExpiredForUserException $ex) {
            return $ex->getMessage();
        }
        catch (CouponNotActiveException $ex) {
            return $ex->getMessage();
        }
        catch (CouponLockedForUserException $ex) {
            return $ex->getMessage();
        }
        catch (CouponNotValidForUserException $ex) {
            return $ex->getMessage();
        }
        catch (CouponChannelException $ex) {
            return $ex->getMessage();
        }
        catch (CouponUserNotLoggedInException $ex) {
            return $ex->getMessage();
        }
        catch (CouponUserMobilenotVerifiedException $ex) {
            return $ex->getMessage();
        }
    }
}