<?php

require_once "$xcart_dir/include/solr/solrProducts.php";
include_once (dirname(__FILE__)."/../cart/Product.php");
include_once (dirname(__FILE__)."/../RestAPI/RestRequest.php");
include_once $xcart_dir."/include/func/func.mail.php";

/*
 * Class to contain the code for DiscountEngine implementation
 */
class DiscountEngine{
	
	public static function asignDiscountToStyle($styleId) {
		global $XCART_SESSION_VARS,$drelog;
        $username = $XCART_SESSION_VARS['login'];
        $url = HostConfig::$styleServieUrl.'/pdp/'.$styleId;
		$restRequest = new RestRequest($url, 'GET');
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$partialReturnObject = json_decode($responseBody,true);

        if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
            $drelog->debug('asignDiscountToStyle:--'.print_r($restRequest,true));
            //self::sendDiscountRuleErrorEmail('asignDiscountToStyle',self::formatRestRequestForMail($restRequest),$username);
        }
        $responseObject = array();

        if ($partialReturnObject && isset($partialReturnObject['data']) && isset($partialReturnObject['data']['discountData'])) {
            $returnObject = $partialReturnObject['data']['discountData'];
            $styleObject = array();
            $styleObject['discountType'] = $returnObject['discountType'];
            $styleObject['discountPercent'] = $returnObject['discountPercent'];
            $styleObject['discountId'] = $returnObject['discountId'];
            $styleObject['discountAmount'] = $returnObject['discountAmount'];
            $styleObject['icon'] = $returnObject['icon'];
            $styleObject['cartDiscountId'] = $returnObject['id'];
            $styleObject['discountRuleHistoryId'] = $returnObject['discountRuleHistoryId'];
            $styleObject['styleType'] = 'Style'; 
            $styleObject['displayText'] = array();
            $params = array();
            $params['percent'] = $returnObject['discountPercent'];             
            $styleObject['displayText']['id'] = 'Flat_PDP_Percent';
            $styleObject['displayText']['params'] = $params;

            $styleObject['discountToolTipText'] = array();
            $styleObject['discountToolTipText']['id'] = 'Flat_PDP_Tooltip_Percent';
            $styleObject['discountToolTipText']['params'] = $params;

              

            $responseObject[$partialReturnObject['data']['id']]=$styleObject;

        }
	   
		return json_decode(json_encode($responseObject), false);
	}
	
	public static function getDataForAStyle($page, $styleId) {
		$styleIds = array();
		array_push($styleIds, $styleId);
		$returnObject = self::getDataForMultipleStyles($page, $styleIds);
		return $returnObject->$styleId;
	}
	
	public static function getComboAssociatedStyleids($comboId){
		global $drelog;
		if(!$comboId) return false;

		$url = HostConfig::$portalServiceHost.'/discount/rule/'.$comboId."/styles";
		global $XCART_SESSION_VARS,$drelog;
		$username = $XCART_SESSION_VARS['login'];
		$restRequest = new RestRequest($url, 'GET');
		$headers = array();
		array_push($headers, 'Accept: application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
			$drelog->debug('getComboAssociatedStyleids:--'.print_r($restRequest,true));
		}
		return json_decode($responseBody);

	}


        public static function getDataForPageOnTimeDiff($page, $timeDiff)
        {
                $url = HostConfig::$portalServiceHost.'/discount/styles/currdiscount?page='.$page;
                global $XCART_SESSION_VARS,$drelog;
                $username = $XCART_SESSION_VARS['login'];

                $restRequest = new RestRequest($url, 'POST', $idsToPost);
                $headers = array();
                array_push($headers, 'Accept: application/json');
                array_push($headers,'Content-Type: application/json');
                array_push($headers,'timeDiff: '.$timeDiff);
                array_push($headers,'user: '.$username);

                $restRequest ->setHttpHeaders($headers);
                $drelog->debug('getDataForPageOnTimeDiff:--'.print_r($restRequest, true));
                $restRequest ->execute();
                $responseInfo = $restRequest ->getResponseInfo();
                $responseBody = $restRequest ->getResponseBody();
                $partialReturnObject = json_decode($responseBody);
                if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
                        $drelog->debug('getDataForPageOnTimeDiff:--'.print_r($restRequest,true));
                        self::sendDiscountRuleErrorEmail('getDataForPageOnTimeDiff',self::formatRestRequestForMail($restRequest),$username);
                }
                $drelog->debug('getDataForPageOnTimeDiff:--'.print_r($partialReturnObject));
                return $partialReturnObject;
        }

	public static function getDataForMultipleStyles($page, $styleIds,$timeDiff=0) {
		$url = HostConfig::$styleServieUrl.'/pdp';
		global $XCART_SESSION_VARS,$drelog;
        $username = $XCART_SESSION_VARS['login'];
		$totalStyles = count($styleIds);
		$blockSize = 2000;
		$blockCount = ceil($totalStyles/$blockSize);
		$returnObject=null;
        $finalResponse = new stdClass();
		// get data in a batch size defined by $blockSize
		for ($i=0;$i<$blockCount;$i++) {
			$idsToPost = array_slice($styleIds, $i*$blockSize, $blockSize);
			$idsToPost = json_encode($idsToPost);			
			$restRequest = new RestRequest($url, 'POST', $idsToPost);
			$headers = array();				
			array_push($headers, 'Accept: application/json');
			array_push($headers,'Content-Type: application/json');
            array_push($headers,'timeDiff: '.$timeDiff);
            array_push($headers,'user: '.$username);
			$restRequest ->setHttpHeaders($headers);
			$restRequest ->execute();
			$responseInfo = $restRequest ->getResponseInfo();
			$responseBody = $restRequest ->getResponseBody();
			$partialReturnObject = json_decode($responseBody,true);
            if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
                $drelog->debug('asignDiscountToStyle:--'.print_r($restRequest,true));
                //self::sendDiscountRuleErrorEmail('getDataForMultipleStyles',self::formatRestRequestForMail($restRequest),$username);
            }
            if ($partialReturnObject && isset($partialReturnObject['data'])) {
                $partialReturnObject = $partialReturnObject['data'];
                foreach ($partialReturnObject as $pObject) {
                    if (isset($pObject['discountData'])) {

                        $returnObject = $pObject['discountData'];

                        $styleObject = array();
                        $styleObject['discountType'] = $returnObject['discountType'];
                        $styleObject['discountPercent'] = $returnObject['discountPercent'];
                        $styleObject['discountId'] = $returnObject['discountId'];
                        $styleObject['discountAmount'] = $returnObject['discountAmount'];
                        $styleObject['icon'] = $returnObject['icon'];
                        $styleObject['cartDiscountId'] = $returnObject['id'];
                        $styleObject['discountRuleHistoryId'] = $returnObject['discountRuleHistoryId'];
                        $styleObject['styleType'] = 'Style';  
                        $styleObject['displayText'] = array();
                        $params = array();
                        $params['percent'] = $returnObject['discountPercent'];             
                        $styleObject['displayText']['id'] = 'Flat_PDP_Percent';
                        $styleObject['displayText']['params'] = $params;

                        $styleObject['discountToolTipText'] = array();
                        $styleObject['discountToolTipText']['id'] = 'Flat_PDP_Tooltip_Percent';
                        $styleObject['discountToolTipText']['params'] = $params;



                        $finalResponse->$pObject['id']=$styleObject;
                    }
                }
            }
		
		}
		return json_decode(json_encode($finalResponse), false);
	}
	
	public static function executeOnCart($cart){
		$profileHandler = Profiler::startTiming("discount-engine-call");
        global $XCART_SESSION_VARS,$drelog;
        $username = $XCART_SESSION_VARS['login'];
		$cartJsonObject = $cart->getJsonForDiscount();
		$url = HostConfig::$portalServiceHost.'/discount/cart';
		$restRequest = new RestRequest($url, 'POST', $cartJsonObject);
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'action: applyDiscount');
		array_push($headers,'user: '.$username);
 
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
            $drelog->debug('asignDiscountToStyle:--'.print_r($restRequest,true));
            //self::sendDiscountRuleErrorEmail('executeOnCart',self::formatRestRequestForMail($restRequest),$username);
        }
		$discountedCart = json_decode($responseBody);
		$cart->setDiscountFields($discountedCart);
		Profiler::endTiming($profileHandler);
		return $cart;
	}


	public static function redeemVoucher($orderid, $vcode){
            global $XCART_SESSION_VARS,$drelog;
            $username = array_push($headers,$XCART_SESSION_VARS['login']);
            //$cartJsonObject = $cart->getJsonForDiscount();
            $drelog->debug("RedeemVoucher - ".print_r($username, true));
            
            //echo "Json string - " .$cartJsonObject . "<BR>";
            $url = HostConfig::$portalServiceHost.'/voucher/code/'.$vcode;
            $restRequest = new RestRequest($url, 'PUT', $cartJsonObject);

            $headers = array();
            array_push($headers, 'Accept:  application/json');
            array_push($headers,'Content-Type: application/json');
            array_push($headers,'orderid: '.$orderid);
            array_push($headers, 'user: '.$XCART_SESSION_VARS['login']);
            //array_push($headers,$username);

            $restRequest ->setHttpHeaders($headers);
            $restRequest ->execute();
            $responseInfo = $restRequest ->getResponseInfo();
            $responseBody = $restRequest ->getResponseBody();
            if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
                $drelog->debug('redeemVoucher:--'.print_r($restRequest,true));
				return null;
                //self::sendDiscountRuleErrorEmail('executeOnCart',self::formatRestRequestForMail($restRequest),$username);
            }
            $voucherDetails = json_decode($responseBody);
            //echo "<PRE>".print_r($discountedCart, true)."</PRE>";
            //$cart->setDiscountFields($discountedCart);
            return $voucherDetails;
	}
        
    public static function formatRestRequestForMail($rest){
        $rest_str =  print_r($rest, true);
        return '</br><pre>'.$rest_str.'</pre>';
    }

    public static function sendDiscountRuleErrorEmail($heading=null, $content= null, $username = '') {

		$emailSubject = "Error in Discount Engine - $heading";

        $str = '<br/>';
        $str .= 'REQUEST_URI = '.$_SERVER['REQUEST_URI'].'<br/>';
        $str .= 'HTTP_HOST = '.$_SERVER['HTTP_HOST'].'<br/>';
        $str .= 'HTTP_USER_AGENT = '.$_SERVER['HTTP_USER_AGENT'].'<br/>';
        $str .= 'SERVER_NAME = '.$_SERVER['SERVER_NAME'].'<br/>';
        $str .= 'SERVER_ADDR = '.$_SERVER['SERVER_ADDR'].'<br/>';
        $str .= 'REMOTE_ADDR = '.$_SERVER['REMOTE_ADDR'].'<br/>';

		$emailContent = "User : $username<br/><br/>$str.$content";


		$subjectargs = array("TITLE"=>$emailSubject);
		$args = array("CONTENT" => $emailContent);
		sendMessageDynamicSubject("discount_rule_changes", $args, DISCOUNT_RULE_MODIFICATION_ERROR_EMAILS ,'MyntraAdmin-DiscountRule', $subjectargs);
	}
	
	
	public static function attachVoucherToOrder($orderid, $itembarcode,$login){
            global $drelog;
            $username = array_push($headers,$login);
            $drelog->debug("orderid $orderid itembarcode $itembarcode");
            $url = HostConfig::$portalServiceHost.'/voucher/detail/'.$itembarcode;
            $restRequest = new RestRequest($url, 'PUT', '');

            $headers = array();
            array_push($headers, 'Accept:  application/json');
            array_push($headers,'Content-Type: application/json');
            array_push($headers,'orderid: '.$orderid);
            array_push($headers, 'user: '.$login);

            $restRequest ->setHttpHeaders($headers);
            $restRequest ->execute();
            $responseInfo = $restRequest->getResponseInfo();
            $responseBody = $restRequest->getResponseBody();
            $drelog->debug("info ".print_r($responseInfo,true));
            
            $drelog->debug("response body ".print_r($responseBody,true));
            if($responseInfo['http_code'] == 200){
	            /*if($responseBody || isset($responseBody->status) || isset($responseBody->message)) {
	               return  $responseBody->status;
	            }*/
	            return true;
            }else{
            	return false;
            }
            return false;
	}

    public static function OOSActionForDiscounts($styleIds,$action){
	global $drelog;
        $headers = array();
        if(!is_array($styleIds)) $styleIds= array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'action: '.$action);
        array_push($headers,'user: ossaction');
		$request = new RestRequest(HostConfig::$portalServiceHost.'/discount/oosaction', "POST",json_encode($styleIds));

		$request->setHttpHeaders($headers);
		$request->execute();
        $responseBody = $request ->getResponseBody();
        if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
            $drelog->debug('OOS disable discount error:--'.print_r($request,true));
            $subjectargs = array("TITLE"=>"OOS discount Actions Failure");
            $args = array("CONTENT" => "Action :-".$action." <br/> Item Ids :- ".implode(',',$styleIds));
            $to = DISCOUNT_RULE_MODIFICATION_ERROR_EMAILS;
            sendMessageDynamicSubject("discount_rule_changes", $args,$to,'MyntraAdmin-DiscountRule', $subjectargs);
            return false;
        }
        $discountIds = json_decode($responseBody);
	$drelog->debug("OOS discount Ids modified ".print_r($discountIds,true));
        if(is_array($discountIds) && count($discountIds)){
            $error = false;
            foreach($discountIds as $dId){
                $dStyleIds = DiscountEngine::getComboAssociatedStyleids($dId);
                $solrProducts = new solrProducts();
                $discountDetails = DiscountEngine::getDataForMultipleStyles("search", $dStyleIds);
                try{
                    foreach ($dStyleIds as $ds) {
                        $solrProducts->addStyleToIndex($ds, true, false,true,$discountDetails->$ds);
                    }
                } catch (Exception $ex) {
                    $msg = 'OOS solr error for discountId '.$dId." Error Message:-".$ex->getMessage();
                    $drelog->debug($msg);
                    $subjectargs = array("TITLE"=>"OOS discount Actions Error");
                    $args = array("CONTENT" => "Action :-".$action." <br/> Item Ids :- ".implode(',',$styleIds)." <br/> Message:- ".$msg);
                    $to = DISCOUNT_RULE_MODIFICATION_ERROR_EMAILS;
                    sendMessageDynamicSubject("discount_rule_changes", $args,$to,'MyntraAdmin-DiscountRule', $subjectargs);
                    $error = true;
                }
            }
            if($error) return false;
            //All is Well. notify people if discounts are disabled

            $subjectargs = array("TITLE"=>"OOS discount Actions Success");
            $args = array("CONTENT" => "Action :-".$action." <br/> Item Ids :- ".implode(',',$styleIds));
            $to = DISCOUNT_RULE_MODIFICATION_ERROR_EMAILS;
            sendMessageDynamicSubject("discount_rule_changes", $args,$to,'MyntraAdmin-DiscountRule', $subjectargs);
        }
        return true;
	}
}