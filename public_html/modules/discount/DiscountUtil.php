<?php

include_once(dirname(__FILE__)."/../../include/func/func.db.php");
include_once(dirname(__FILE__)."/DiscountConfig.php");
include_once(dirname(__FILE__)."/algo/DiscountAlgo.php");
include_once(dirname(__FILE__)."/algo/FlatDiscount.php");

function func_get_rule_impl($ruleid){
    $ruleData = func_select_first('mk_discount_rules', array('ruleid'=>$ruleid, 'enabled'=>1));
    if($ruleData == null){
    	return null;
    }
    $data = json_decode($ruleData['data'], true);
	$startDate = $ruleData['start_date'];
	$endDate = $ruleData['end_date'];
	$algoid = $ruleData['algoid'];
	$enabled = $ruleData['enabled'];
	$class = DiscountConfig::$discountConfigMap[$algoid][DiscountConfig::DISCOUNT_IMPL_CLASS];
	$impl = new $class($ruleid, $algoid, $data, $startDate, $endDate, $enabled);
	return $impl;
}
?>
