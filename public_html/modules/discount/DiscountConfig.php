<?php

/*
 * Class to contain discount specific data
 */
class DiscountConfig{
	const FLAT_DISCOUNT_ID = 'FLAT';
	
	const DISCOUNT_NAME = 'name';
	const DISCOUNT_INPUT_TPL = 'input_tpl';
	const DISCOUNT_IMPL_CLASS = 'impl_class';
	
	public static $discountConfigMap = array(
		self::FLAT_DISCOUNT_ID => array(
			self::DISCOUNT_NAME => "Flat Discount",
			self::DISCOUNT_INPUT_TPL => "flat_input.tpl",
			self::DISCOUNT_IMPL_CLASS => "FlatDiscount"
		)
	);
}