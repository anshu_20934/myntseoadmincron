<?php
include_once(dirname(__FILE__)."/DiscountAlgo.php");

class FlatDiscount extends DiscountAlgo{
	
	private $isPercentage = 1;
	private $value = 0.0;
	
	public function __construct($ruleid, $algoid, $data, $startDate, $endDate, $enabled){
		parent::__construct($ruleid, $algoid, $data, $startDate, $endDate, $enabled);
		
		$value = $data['value'];
		$isPercentage = $data['isPercentage'];
		
		if (empty($value) || $isPercentage === NULL){
			$this->log->debug(__FILE__.__LINE__."Incomplete Input data specified (value,isPercentage) = ($value,$isPercentage)");
			//throw new DiscountInvalidInputException("Incomplete Input data specified");
			return;
		}
		if (!is_numeric($value) || !is_int($isPercentage)){
			$this->log->debug(__FILE__.__LINE__."Invalid Input data specified (value,isPercentage) = ($value,$isPercentage)");
			//throw new DiscountInvalidInputException("Invalid Input data specified");
			return;
		}
		$this->value = $value;
		$this->isPercentage = $isPercentage;
	}
	
	public function applyDiscount(&$products){
		$date = time();
		if($this->enabled != 1){
			$this->log->debug(__FILE__.__LINE__.": Not applying any discount as the discount rule is not enabled");
			return $products;
		}
		if($date < $this->startDate || $date > $this->endDate){
			$this->log->debug(__FILE__.__LINE__.": Not applying any discount as current time does not lie between startdate: ".$this->startDate." and enddata: ".$this->endDate);
			return $products;
		}
		
		foreach($products as $key=>$product){
			 if($this->isPercentage == 1){
				$product->setDiscountAmount(floor($product->getTotalProductPrice() * $this->value / 100));
				if(!empty($this->value)) {
					// Discount is nonzero. Then we need to generate the label information.
					$product->discountLabelText = "$this->value%";
					$discountPerItem = floor($product->getProductPrice() * $this->value / 100);
					$product->discountDisplayText = "You save Rs. $discountPerItem per Item";
				}
				$product->discounttype = 'percent';
				$product->discountvalue = $this->value;
			}else{
				$product->setDiscountAmount($this->value*$product->getQuantity());
				$percentageValue = floor($product->getDiscountAmount()/$product->getTotalProductPrice()*100);
				if(!empty($this->value)) {
					// Discount is nonzero. Then we need to generate the label information.
					$product->discountLabelText = "Rs. $this->value";
					$product->discountDisplayText = "$percentageValue% Off";
				}
				$product->discounttype = 'absolute';
				$product->discountvalue = $this->value;
			}
			$this->log->debug(__FILE__.__LINE__.": Discount amount calculated to be : ".$product->getDiscountAmount());
			$product->discountRuleId = $this->ruleid;
			$product->discountAlgoID = $this->algoid;
			$product->discountData = $this->data;
			$products[$key] = $product;
		}
		return $products;
	}
	
	public function getDiscountValue(){
		if($this->isPercentage){
			if($this->value > 100){
				return 100;
			}else if($this->value < 0){
				return 0;
			}
		}
		if($this->value < 0){
			return 0;
		}
		return $this->value;
	}
}
