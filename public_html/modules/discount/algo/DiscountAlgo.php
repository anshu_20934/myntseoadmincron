<?php
//include(dirname(__FILE__)."/../exception/DiscountInvalidInputException.php");

/*
 * Base class for the DiscountAlgo implementations
 */
class DiscountAlgo{
	
	protected $ruleid = null;
	protected $startDate = null;
	protected $endDate = null;
	protected $data = null;// Field to store the discount data retrieved from database
	protected $algoid = null;
	protected $enabled = 1;
	protected $log = null;
	
	public function __construct($ruleid, $algoid, $data, $startDate, $endDate, $enabled){
		global $weblog;
		$this->log = $weblog;
		if(empty($ruleid)){
			$this->log->debug(__FILE__.__LINE__."Empty rule id specified");
			//throw new DiscountInvalidInputException("Empty rule id specified");
			return;
		}
		if (empty($startDate) || empty($endDate) || empty($algoid)){
			$this->log->debug(__FILE__.__LINE__."Incomplete Input data specified (startDate,endDate,algoid) = ($startDate,$endDate,$algoid)");
			//throw new DiscountInvalidInputException("Incomplete Input data specified");
			return;
		}
		if (! is_numeric($startDate) || !is_numeric($endDate)){
			$this->log->debug(__FILE__.__LINE__."Invalid Input data specified (startDate,endDate,algoid) = ($startDate,$endDate,$algoid)");
			//throw new DiscountInvalidInputException("Invalid Input data specified");
			return;
		}
		$this->ruleid = $ruleid;
		$this->startDate = $startDate;
		$this->endDate = $endDate;
		$this->algoid = $algoid;
		$this->data = $data;
		$this->enabled = $enabled;
	}
	/**
	 * Dummy method for alllying the discount on the cart items
	 * Need to be overridden by individual algo implementation
	 */
	public function applyDiscount(&$products){
		return $products;
	}
}
