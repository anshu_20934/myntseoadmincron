<?php

include_once ("$xcart_dir/include/func/func.db.php");
include_once ("$xcart_dir/modules/coupon/database/CouponAdapter.php");

/**
 * The singleton class for generation of E-Vouchers on the fly.
 * 
 * @author Rohan.Railkar
 * @since March 3, 2011
 */
class EVoucherGenerator
{
    /**
     * The singleton instance.
     */
    private static $instance = null;
    
    /**
     * Coupon database adapter.
     */
    private $couponAdapter = null;

    /**
     * @return CouponAdapter The singleton instance.
     */
    public static function getInstance()
    {
        if (EVoucherGenerator::$instance == null) {
            EVoucherGenerator::$instance = new EVoucherGenerator();
        }

        return EVoucherGenerator::$instance;
    }

    /**
     * Constructor.
     */
    private function __construct()
    {
        $this->couponAdapter = CouponAdapter::getInstance();
    }
    
    /**
     * Generates E-Voucher.
     * 
     * @return string CouponCode, if voucher has already not been given.
     * 				  null otherwise.
     */
    public function generateEVoucher($userName, $length, $startDate, $endDate, $orderid)
    {
        global $system_campaign_name;
        if ($this->existsEVoucherForOrder(
                $userName, $system_campaign_name, $orderid))
        {
            return null;
        }
        
        $couponCodePrefix = 'EV';
        $suffixLength = $length - strlen($couponCodePrefix);
        $groupName = 'EVouchers';
        $type = 'absolute';
        $amount = 1000.00;
        $percentage = 0.00;
        $minCartVal = 2500.00;
        
        $couponCode = $this->couponAdapter->generateSingleCouponForUser(
            $couponCodePrefix, 
            $suffixLength, 
            $groupName,
            $startDate,
            $endDate,
            $userName,
            $type,
            $amount,
            $percentage,
            $minCartVal);
            
        $this->addMapping($couponCode, $userName, $orderid);
            
        return $couponCode;
    }
    
    /**
     * Make a note of a coupon-user pairing.
     */
    private function addMapping($couponCode, $userName, $orderid)
    {
        global $system_campaign_name;
        db_query(
        	"INSERT INTO mk_evoucher_mapping 
        		(coupon, login, campaign, orderid)
        	VALUES 
        		('$couponCode', '$userName', '$system_campaign_name', '$orderid');"
        );
    }
    
    /**
     * @return Whether an EVoucher is already issued to the user.
     */
    private function existsEVoucherForOrder($userName, $campaign, $orderid)
    {
        $result =
            func_query_first(
            	"SELECT coupon 
				   FROM mk_evoucher_mapping
        		  WHERE login = '$userName' 
        		  	AND campaign = '$campaign' 
        		  	AND orderid = '$orderid';");
        
        return !empty($result);
    }
}