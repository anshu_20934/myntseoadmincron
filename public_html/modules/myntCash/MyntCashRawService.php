<?php
include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
/*
 * This is portal side implementation of cashback/myntcash raw services
 */
class MyntCashRawService{
	
	public static function getBalance($login){
		
		if(empty($login) || trim($login)==""){
			return array("storeCreditBalance"=>0,
						"earnedCreditbalance"=>0,
						"balance"=>0,
						"login"=>$login
					);
		}
		
		$qry = "select * from cashback_account where login = '".$login."' ";
		$result = func_query($qry);
		$storeCredit = 0.0;
		$earnedCredit = 0.0;
		
		foreach($result as $row){
			if($row["account_type"] == 1){
				$earnedCredit += $row["balance"];
			}elseif($row["account_type"] == 2){
				$storeCredit += $row["balance"];
			}
		}
		return array("storeCreditBalance"=>$storeCredit,
						"earnedCreditbalance"=>$earnedCredit,
						"balance"=>$storeCredit+$earnedCredit,
						"login"=>$login
					);
	}
	
	public static function debit(MyntCashTransaction $trs,$user){
		
		if(empty($user)||$user==""||$user=="anonymous"){
			return array("status"=>"error",
					"message"=>" un-authorized access ");
		}	
					
		$transaction = $trs->getArray();
		$debitAmount = $transaction["debitAmount"];
		$storeCredit = 0.0;
		$earnedCredit = 0.0;
		$login = $transaction["login"];
		
		$balance = 0.0;
		$storeCreditAmount = 0.0;
		$earnedCreditAmount = 0.0;
		$earnedCreditAccount = null;
		$storeCreditAccount = null;
		$qry = "select * from cashback_account where login = '".$login."'";
		
		$rows = func_query($qry);
		
		foreach($rows as $row){
			if($row["account_type"]==1){
				$earnedCreditAccount = $row;
				$earnedCreditAmount = $row["balance"];
				$balance += $earnedCreditAmount;
			}elseif($row["account_type"]==2){
				$storeCreditAccount = $row;
				$storeCreditAmount = $row["balance"];
				$balance += $storeCreditAmount;
			}
		}
		
		
		
		if($storeCreditAmount > $debitAmount){
			$storeCredit = $debitAmount;
			$storeCreditAccount["balance"] = $storeCreditAccount["balance"] - $storeCredit;
		}elseif($debitAmount <= ($balance)){
			$storeCredit = $storeCreditAmount;
			$storeCreditAccount["balance"] = $storeCreditAccount["balance"] - $storeCredit;
			$earnedCredit = $debitAmount - $storeCreditAmount;
			$earnedCreditAccount["balance"] = $earnedCreditAccount["balance"] - $earnedCredit;
			
		}elseif( abs($debitAmount - $balance)<1.0 ){
			$storeCredit = $storeCreditAmount;
			$storeCreditAccount["balance"] = $storeCreditAccount["balance"] - $storeCredit;
			$earnedCredit = $earnedCreditAmount;
			$earnedCreditAccount["balance"] = $earnedCreditAccount["balance"] - $earnedCredit;
		}		
		else{
			return array("status"=>"error",
					"message"=>"earned credit : ".$earnedCreditAmount."  store credit : ".$storeCreditAmount."  requested amount : ".$debitAmount." "
					);
		}
		
					
		db_query("SET AUTOCOMMIT=0");
		db_query("START TRANSACTION");
		try{
			
		 	if($earnedCredit > 0.0){
				db_query("UPDATE cashback_account SET balance =".$earnedCreditAccount["balance"]." , version = version+1
						WHERE id =".$earnedCreditAccount["id"]." and version =".$earnedCreditAccount["version"]);
		
				if(mysql_affected_rows()!=1){
					db_query("ROLLBACK");
					throw new Exception("cannot update cashback_account");
				}
				$balance = $balance - $earnedCredit;
				db_query("INSERT into cashback_transaction_log
						(cashback_account_id, item_type, item_id, business_process, credit_outflow, balance, modified_by, descripion, goodwill_reason)
						values ( ".$earnedCreditAccount["id"].", '".$transaction["itemType"]."',".$transaction['itemId'].", '".$transaction['businessProcess']."',".$earnedCredit.",".$balance.",'".$user."','".$transaction["description"]."','".$transaction["goodwillReason"]."') ");
				
			}
		
			if($storeCredit > 0.0){
				db_query("UPDATE cashback_account SET balance =".$storeCreditAccount["balance"]." , version = version+1
						WHERE id =".$storeCreditAccount["id"]." and version =".$storeCreditAccount["version"]);
				if(mysql_affected_rows()!=1){
					db_query("ROLLBACK");
					throw new Exception("cannot update cashback_account");
				}
		
				$balance = $balance - $storeCredit;
				db_query("INSERT into cashback_transaction_log
						(cashback_account_id, item_type, item_id, business_process, credit_outflow, balance, modified_by, descripion, goodwill_reason)
						values ( ".$storeCreditAccount["id"].", '".$transaction["itemType"]."',".$transaction['itemId'].", '".$transaction['businessProcess']."',".$storeCredit.",".$balance.",'".$user."','".$transaction["description"]."','".$transaction["goodwillReason"]."') ");
			}		
			$trs->setEarnedCreditAmount($earnedCredit);
			$trs->setStoreCreditAmount($storeCredit);
		}catch(Exception $e){
			$ret = array("status"=>"error","message"=>"".$e->getMessage());
		}
		db_query("COMMIT");
		db_query("SET AUTOCOMMIT=1");
		
		if(!empty($ret) && isset($ret["status"]) && $ret["status"]=="error"){
			return $ret;
		}
		
		
		
		//$ret = //array("status"=>"success","message"=>"success");
		
		//** old cashback table updates **//
		$adapter = CouponAdapter::getInstance();
		$adapter->debitCashback($transaction["login"], $earnedCredit+$storeCredit, $transaction["description"]);
		return $trs->getArray();
			
	}
	
	public static function credit(MyntCashTransaction $trs,$user){
		
		if(empty($user)||$user==""||$user=="anonymous"){
			return array("status"=>"error",
						 "message"=>" un-authorized access ");
		}
		
		$transaction = $trs->getArray();
		
		$earnedCredit = $transaction["earnedCreditAmount"];	
		$storeCredit = $transaction["storeCreditAmount"];
		$login = $transaction["login"];
		
		$earnedCreditAccount = null;
		$storeCreditAccount = null;
		$qry = "select * from cashback_account where login = '".$login."'";
		
		$rows = func_query($qry);
		
		foreach($rows as $row){
			if($row["account_type"]==1){
				$earnedCreditAccount = $row;
			}elseif($row["account_type"]==2){
				$storeCreditAccount = $row;
			}
		}
		
		if($earnedCreditAccount==null){
			$earnedCreditAccount = array(
										"account_type"=>1,
										"login"=>$transaction["login"],
										
										"version"=>0
										);
			$earnedCreditAccount["balance"] = $earnedCredit;
			$balance = 0.0;
		}else{
			$balance = $earnedCreditAccount["balance"];
			$earnedCreditAccount["balance"] = $earnedCreditAccount["balance"] + $earnedCredit;			
		}
		
		if($storeCreditAccount==null){
			$storeCreditAccount = array(
					"account_type"=>2,
					"login"=>$transaction["login"],
					
					"version"=>0
			);
			$storeCreditAccount["balance"] = $storeCredit;
			
		}else{
			$balance = $balance + $storeCreditAccount["balance"];
			$storeCreditAccount["balance"] = $storeCreditAccount["balance"]+$storeCredit;
		}
		
			
		db_query("SET AUTOCOMMIT=0");
		db_query("START TRANSACTION");
		try{
			
			if(isset($earnedCreditAccount["id"])){
				if($earnedCredit > 0){
					db_query("UPDATE cashback_account SET balance =".$earnedCreditAccount["balance"]." , version = version+1 
							  WHERE id =".$earnedCreditAccount["id"]." and version =".$earnedCreditAccount["version"]);
					
					if(mysql_affected_rows()!=1){
						db_query("ROLLBACK");
						throw new Exception("cannot update cashback_account");
					}
					$balance = $balance + $earnedCredit;
					db_query("INSERT into cashback_transaction_log
							(cashback_account_id, item_type, item_id, business_process, credit_inflow, balance, modified_by, descripion, goodwill_reason)
							values ( ".$earnedCreditAccount["id"].", '".$transaction["itemType"]."',".$transaction['itemId'].", '".$transaction['businessProcess']."',".$earnedCredit.",".$balance.",'".$user."','".$transaction["description"]."','".$transaction["goodwillReason"]."') ");
				}
				
			}else{
				db_query("INSERT into cashback_account (login,account_type,balance,version)
						                        values ('".$earnedCreditAccount["login"]."',".$earnedCreditAccount["account_type"].",".$earnedCreditAccount["balance"].",".$earnedCreditAccount["version"].")");
				if(mysql_affected_rows()!=1){
					db_query("ROLLBACK");
					throw new Exception("cannot insert into cashback earnedCredit account");
				}
				
				if( $earnedCredit > 0){
					$balance = $balance + $earnedCredit;
					db_query("INSERT into cashback_transaction_log
							(cashback_account_id, item_type, item_id, business_process, credit_inflow, balance, modified_by, descripion, goodwill_reason)
							values ( ".mysql_insert_id().", '".$transaction["itemType"]."',".$transaction['itemId'].", '".$transaction['businessProcess']."',".$earnedCredit.",".$balance.",'".$user."','".$transaction["description"]."','".$transaction["goodwillReason"]."') ");
				}
			}
			
			if(isset($storeCreditAccount["id"]) ){
				if($storeCredit > 0){
					db_query("UPDATE cashback_account SET balance =".$storeCreditAccount["balance"]." , version = version+1
							WHERE id =".$storeCreditAccount["id"]." and version =".$storeCreditAccount["version"]);
					if(mysql_affected_rows()!=1){
						db_query("ROLLBACK");
						throw new Exception("cannot update cashback_account");
					}
					
					$balance = $balance + $storeCredit;
					db_query("INSERT into cashback_transaction_log
							(cashback_account_id, item_type, item_id, business_process, credit_inflow, balance, modified_by, descripion, goodwill_reason)
							values ( ".$storeCreditAccount["id"].", '".$transaction["itemType"]."',".$transaction['itemId'].", '".$transaction['businessProcess']."',".$storeCredit.",".$balance.",'".$user."','".$transaction["description"]."','".$transaction["goodwillReason"]."') ");
				}
					
			}else{
				db_query("INSERT into cashback_account (login,account_type,balance,version)
						values ('".$storeCreditAccount["login"]."',".$storeCreditAccount["account_type"].",".$storeCreditAccount["balance"].",".$storeCreditAccount["version"].")");
				
				if(mysql_affected_rows()!=1){
					db_query("ROLLBACK");
					throw new Exception("cannot insert into cashback storeCredit account");
				}
				
				if( $storeCredit > 0){
					$balance = $balance + $storeCredit;
					db_query("INSERT into cashback_transaction_log
						(cashback_account_id, item_type, item_id, business_process, credit_inflow, balance, modified_by, descripion, goodwill_reason)
						values ( ".mysql_insert_id().", '".$transaction["itemType"]."',".$transaction['itemId'].", '".$transaction['businessProcess']."',".$storeCredit.",".$balance.",'".$user."','".$transaction["description"]."','".$transaction["goodwillReason"]."') ");
				}
				
			}
		}catch(Exception $e){
			$ret = array("status"=>"error","message"=>"".$e->getMessage());
		}		
		db_query("COMMIT");
		db_query("SET AUTOCOMMIT=1");
		
		if(!empty($ret) && isset($ret["status"]) && $ret["status"]=="error"){
			return $ret;
		}
		$ret = array("status"=>"success","message"=>"success");
		
		//** old cashback table updates **//
		$adapter = CouponAdapter::getInstance();
		$adapter->creditCashback($transaction["login"], $earnedCredit+$storeCredit, $transaction["description"]);
		return $ret;		
	}
	
	public static function getTransactionLogs($login){
		$qry1 = "select tl.cashback_account_id as cashbackAccountId, tl.item_type as itemType, tl.item_id as itemId,
		         tl.business_process as businesProcess, tl.credit_inflow as creditInflow,
		         tl.credit_outflow as creditOutflow,
		         tl.balance as balance, tl.modified_by as modifiedBy,
		         tl.modified_on as modifiedOn , tl.descripion as description,tl.goodwill_reason as goodwillReason,
		         ca.login from cashback_transaction_log as tl, cashback_account as ca  where ca.login = '".$login."' and ca.id=tl.cashback_account_id and ca.account_type=1  order by tl.modified_on ";
		
		$qry2 = "select tl.cashback_account_id as cashbackAccountId, tl.item_type as itemType, tl.item_id as itemId,
		         tl.business_process as businesProcess, tl.credit_inflow as creditInflow,
		         tl.credit_outflow as creditOutflow,
		         tl.balance as balance, tl.modified_by as modifiedBy,
		         tl.modified_on as modifiedOn , tl.descripion as description,tl.goodwill_reason as goodwillReason,
		         ca.login from cashback_transaction_log as tl, cashback_account as ca  where ca.login = '".$login."' and ca.id=tl.cashback_account_id and ca.account_type=2  order by tl.modified_on ";
		$eLogs = func_query($qry1);
		$sLogs = func_query($qry2);

		if(empty($eLogs)|| $eLogs == null){
			$eLogs = array();
		}
		if(empty($sLogs)|| $sLogs == null){
			$sLogs = array();
		}
		
		$returnArray = array();
		
		while(!empty($eLogs) || !empty($sLogs)){
			if(empty($eLogs)){
				$returnArray[] = array_shift($sLogs);
			}else if(empty($sLogs)){
				$returnArray[] = array_shift($eLogs);
			}else{
				if( strtotime($eLogs[0]["modifiedOn"])<strtotime($sLogs[0]["modifiedOn"])){
					$returnArray[] = array_shift($eLogs);
				}else if(strtotime($eLogs[0]["modifiedOn"]) == strtotime($sLogs[0]["modifiedOn"]) ){
					if($eLogs[0]["creditInflow"] > 0 && $sLogs[0]["creditInflow"] > 0 ){
						if($eLogs[0]["balance"]  < $sLogs[0]["balance"]){
							$returnArray[] = array_shift($eLogs);
							$returnArray[] = array_shift($sLogs);
						}else{							
							$returnArray[] = array_shift($sLogs);
							$returnArray[] = array_shift($eLogs);
						}
					}else{
						if($eLogs[0]["balance"]  > $sLogs[0]["balance"]){
							$returnArray[] = array_shift($eLogs);
							$returnArray[] = array_shift($sLogs);
						}else{
							$returnArray[] = array_shift($sLogs);
							$returnArray[] = array_shift($eLogs);
						}
					}
				}
				else{
					$returnArray[] = array_shift($sLogs);
				}
			}
		}	
		
		return $returnArray;
	}
	
	public static function getCreditInFlowFromUser($login,$days){
		$DAY_IN_S =  60 * 60 * 24;
		$startTime = time() - ($days * $DAY_IN_S);
		$startDate = date('Y-m-d H:i:s',$startTime);
		$endDate = date('Y-m-d H:i:s',time());
		
		$qry = "select sum(credit_inflow) as credit_inflow from cashback_transaction_log as tl
		where tl.modified_by = '".$login."' and tl.modified_on between '$startDate' and '$endDate' ";
		
		$amount = func_query_first($qry);
		if(!empty($amount)){
			if($amount["credit_inflow"] == null || !isset($amount["credit_inflow"])){
				$amount["credit_inflow"]=0.0;
			}
			return array("amount"=>$amount["credit_inflow"]);
		}else{
			return array("amount"=>0.0);
		}
		
	} 
	
	public static function getManualCreditInFlowForUser($login,$days){
		$DAY_IN_S =  60 * 60 * 24;
		$startTime = time() - ($days * $DAY_IN_S);
		$startDate = date('Y-m-d H:i:s',$startTime);
		$endDate = date('Y-m-d H:i:s',time());
		
		$qry = "select sum(tl.credit_inflow) as credit_inflow from cashback_transaction_log as tl, cashback_account as ca 
		        where ca.login = '".$login."' and ca.id=tl.cashback_account_id and tl.modified_on between '$startDate' and '$endDate' ";
		
		$amount = func_query_first($qry);
		if(!empty($amount)){
			if($amount["credit_inflow"] == null || !isset($amount["credit_inflow"])){
				$amount["credit_inflow"]=0.0;
			}
			return array("amount"=>$amount["credit_inflow"]);
		}else{
			return array("amount"=>0.0);
		}
	}
}
