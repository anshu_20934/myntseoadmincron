<?php
include_once  "$xcart_dir/modules/myntCash/MyntCashBusinessProcesses.php";
include_once  "$xcart_dir/modules/myntCash/MyntCashItemTypes.php";
class MyntCashTransaction{
	private  $login;
	private  $earnedCreditAmount;
	
	private  $storeCreditAmount;
	
	private  $debitAmount;
	
	private  $description;
	private  $itemType;
	private  $itemId;
	private  $businessProcess;
 
	private $goodwillReason;	
	/*
	 * constructor
	 * 
	 */
	
	function __construct($login,$itemType,$itemId,$businessProcess,$earnedCreditAmount,$storeCreditAmount,$debitAmount,$description){
		$this->login = $login;
		$this->itemType = $itemType;
		$this->itemId = $itemId;
		$this->earnedCreditAmount = $earnedCreditAmount;
		$this->storeCreditAmount = $storeCreditAmount;
		$this->debitAmount = $debitAmount;
		$this->description = $description;
		$this->businessProcess = $businessProcess;
	}
	
	public function getLogin(){
		return $this->login;
	}
	
	public function setEarnedCreditAmount($amount){
		$this->earnedCreditAmount = $amount;
	}
	
	public function setStoreCreditAmount($amount){
		$this->storeCreditAmount = $amount;
	}
	
	public function setDebitAmount($amount){
		$this->debitAmount = $amount;
	}
	
	public function setDescription($description){
		$this->description = $description;
	}
	
	public function setOrderID($orderID){
		$this->orderId = $orderID;
	}
	
	public function setBusinessProcess($process){
		$this->businessProcess = $process;
	}
	
	public function getDebitAmount(){
		return $this->debitAmount;
	}

	public function setGoodwillReason($reason){
		$this->goodwillReason = $reason;	
	}

	public function getJson(){
		$tansaction = array();
		$tansaction["login"] = $this->login;
		$tansaction["itemType"] = $this->itemType;
		$tansaction['itemId'] = $this->itemId;
		$tansaction["earnedCreditAmount"] = $this->earnedCreditAmount ;
		$tansaction["storeCreditAmount"] = $this->storeCreditAmount ;
		$tansaction["debitAmount"] = $this->debitAmount ;
		$tansaction["description"] = $this->description ;
		$tansaction["businessProcess"] = $this->businessProcess;
		$tansaction["goodwillReason"] = $this->goodwillReason;
		return json_encode($tansaction);
		
	}

	public function getArray(){
		$tansaction = array();
		$tansaction["login"] = $this->login;
		$tansaction["itemType"] = $this->itemType;
		$tansaction['itemId'] = $this->itemId;
		$tansaction["earnedCreditAmount"] = $this->earnedCreditAmount ;
		$tansaction["storeCreditAmount"] = $this->storeCreditAmount ;
		$tansaction["debitAmount"] = $this->debitAmount ;
		$tansaction["description"] = $this->description ;
		$tansaction["businessProcess"] = $this->businessProcess;
		$tansaction["goodwillReason"] = $this->goodwillReason;
		
		return $tansaction;
	}
}
