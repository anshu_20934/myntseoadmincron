<?php
class MyntCashItemTypes{
	const ORDER = "ORDER";
	const GOOD_WILL_ID = "GOOD_WILL_ID";
	const RETURN_ID = "RETURN_ID";
	const ITEM_ID = "ITEM_ID";
	const GIFT_VOUCHER = "GIFT_VOUCHER";
}
