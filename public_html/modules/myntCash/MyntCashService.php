<?php
include_once ("$xcart_dir/modules/RestAPI/RestRequest.php");
include_once ("$xcart_dir/modules/myntCash/MyntCashCalculator.php");
include_once ("$xcart_dir/modules/myntCash/MyntCashTransaction.php");
include_once ("$xcart_dir/modules/myntCash/MyntCashBusinessProcesses.php");
include_once "$xcart_dir/modules/myntCash/MyntCashServiceException.php";
include_once "$xcart_dir/modules/myntCash/MyntCashRawService.php";
include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
class MyntCashService{
	
	/*
	 *  service response codes
	 */
	const serviceNotAvailable = "serviceNotAvailable";
	const noEnoughBalanceForDebitRequest = "noEnoughBalanceForDebitRequest";
	const creditDebitFailure = "creditDebitFailure";
	
	
	/*
	 * gets the MyntCash details of user
	 * 
	 * @params login :- login of the user
	 */
	public static function getUserMyntCashDetails($login){
		
		$profileHandler = Profiler::startTiming("MyntCash service-call : getUserMyntCashDetails");
		
		$serviceFG = FeatureGateKeyValuePairs::getBoolean("useCashbackService");
		if(!$serviceFG){
			$returnObject = MyntCashRawService::getBalance($login);
			Profiler::endTiming($profileHandler);
			return $returnObject;
		}
		
		$url = HostConfig::$portalServiceHost.'/cashback/balance/'.$login;		
		
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
		
		$weblog->debug("getUserMyntCashDetails : $url");
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'action: applyDiscount');
		array_push($headers,'user: '.$username);
		
		$restRequest = new RestRequest($url);
		
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
		
		$weblog->debug("getUserMyntCashDetails : $responseBody");
		Profiler::endTiming($profileHandler);

				
		return $returnObject;
	}
	
	/*
	 * fetches raw transaction logs for a given user login $login
	 */
	public static function getUserMyntCashTransacions($login){
		$profileHandler = Profiler::startTiming("MyntCash service-call : getUserMyntCashTransacions");
		
		$serviceFG = FeatureGateKeyValuePairs::getBoolean("useCashbackService");
		if(!$serviceFG){
			$returnObject = MyntCashRawService::getTransactionLogs($login);
			Profiler::endTiming($profileHandler);
			return array_reverse($returnObject);
		}
		
		$url = HostConfig::$portalServiceHost.'/cashback/transactionlogs/'.$login;
		
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
		
		$weblog->debug("getUserMyntCashTransacions : $url");
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);
		
		$restRequest = new RestRequest($url);
		
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
		
		Profiler::endTiming($profileHandler);
		
		foreach ($returnObject as $id => $ret){
			$returnObject[$id]["login"] = $login;
			$returnObject[$id]["modifiedOn"] = date("d-m-Y H:i:s",(int) ($ret['modifiedOn']/1000));
		}
		
		$weblog->debug("getUserMyntCashTransacions : $responseBody");
		return array_reverse($returnObject);
	}	
	
	/*
	 * get total credit inflown *from* a particular user in past $days
	 */
	public static function getCreditInFlowFromUser($login,$days){
		$profileHandler = Profiler::startTiming("MyntCash service-call : getCreditInFlowFromUser");
		
		$url = HostConfig::$portalServiceHost.'/cashback/getCreditInFlowFromUser/'.$login."/".$days;
		
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
		
		$serviceFG = FeatureGateKeyValuePairs::getBoolean("useCashbackService");
		if(!$serviceFG){
			$returnObject = MyntCashRawService::getCreditInFlowFromUser($login, $days);
			Profiler::endTiming($profileHandler);
			return $returnObject;
		}
		
		$weblog->debug("getCreditInFlowFromUser : $url");
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'action: applyDiscount');
		array_push($headers,'user: '.$username);
		
		$restRequest = new RestRequest($url);
		
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
		
		$weblog->debug("getCreditInFlowFromUser : $responseBody");
		Profiler::endTiming($profileHandler);
		return $returnObject;
	}
	
		
	/*
	 * get total credit inflown *for* a particular user in past $days
	*/
	public static function getManualCreditInFlowForUser($login,$days){
		$profileHandler = Profiler::startTiming("MyntCash service-call : getManualCreditInFlowForUser");
	
		$url = HostConfig::$portalServiceHost.'/cashback/getManualCreditInFlowForUser/'.$login."/".$days;
	
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
		
		$serviceFG = FeatureGateKeyValuePairs::getBoolean("useCashbackService");
		if(!$serviceFG){
			$returnObject = MyntCashRawService::getManualCreditInFlowForUser($login, $days);
			Profiler::endTiming($profileHandler);
			return $returnObject;
		}
	
		$weblog->debug("getUserMyntCashDetails : $url");
	
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'action: applyDiscount');
		array_push($headers,'user: '.$username);
	
		$restRequest = new RestRequest($url);
	
	
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
	
		$weblog->debug("getManualCreditInFlowForUser : $responseBody");
		Profiler::endTiming($profileHandler);
		return $returnObject;
	}
	
	/*
	 * Credits to the user myntcash account 
	 * @param : well formed transaction class object
	 */
	public static function creditToUserMyntCashAccount(MyntCashTransaction $transaction){
		$profileHandler = Profiler::startTiming("MyntCash service-call");
		
		// create cashcoupon if it does not exist, before firing service call
		// service expects cashcoupon created before credit call 
		
		$adapter = CouponAdapter::getInstance();
		$adapter->creditCashback($transaction->getLogin(), 0, "CREATE_COUPON");
		
		//--
		
		$url = HostConfig::$portalServiceHost.'/cashback/credit';
		
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
		
		if(empty($username)||$username==""||$username==null){
			$username="SYSTEM";
		}
		
		$serviceFG = FeatureGateKeyValuePairs::getBoolean("useCashbackService");
		if(!$serviceFG){
			$returnObject = MyntCashRawService::credit($transaction, $username);
			Profiler::endTiming($profileHandler);
			
						
			return $returnObject;
		}
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);
		
		$transactionJson = $transaction->getJson();
		$restRequest = new RestRequest($url, 'POST', $transactionJson);
		
		$weblog->debug("creditToUserMyntCashAccount : ".print_r($transactionJson,true)."\n : headers: ".print_r($headers,true));
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
		
		Profiler::endTiming($profileHandler);
		$weblog->info("creditToUserMyntCashAccount : $responseBody");
		
				
		return $returnObject;
		
	}
	
	/*
	 * debits from user MyntCash account 
	 * @param : well formed transaction class object
	 */
	public static function debitFromUserMyntCashAccount(MyntCashTransaction $transaction){
		$profileHandler = Profiler::startTiming("MyntCash service-call");
	
		$url = HostConfig::$portalServiceHost.'/cashback/debit';
	
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
		
		if(empty($username)||$username==""||$username==null){
			$username="SYSTEM";
		}
	
		$serviceFG = FeatureGateKeyValuePairs::getBoolean("useCashbackService");
		if(!$serviceFG){
			$returnObject = MyntCashRawService::debit($transaction, $username);
			Profiler::endTiming($profileHandler);
			
						
			return $returnObject;
		}
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);
	
		$transactionJson = $transaction->getJson();
		$restRequest = new RestRequest($url, 'POST', $transactionJson);
	
		$weblog->debug("debitFromUserMyntCashAccount : ".print_r($transactionJson,true)."\n : headers: ".print_r($headers,true));
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
	
		Profiler::endTiming($profileHandler);
		$weblog->debug("debitFromUserMyntCashAccount : $responseBody");
		
				
		return $returnObject;
	
	}
	
	
	/*
	 * credits and debits in an atomic call from user MyntCash account
	* @param : well formed transaction class objects
	*/
	public static function creditAndDebitFromUserMyntCashAccount(MyntCashTransaction $creditTransaction,MyntCashTransaction $debitTransaction){
		$profileHandler = Profiler::startTiming("MyntCash service-call");
	
		// create cashcoupon if it does not exist, before firing service call
		// service expects cashcoupon created before credit call
		
		$adapter = CouponAdapter::getInstance();
		$adapter->creditCashback($creditTransaction->getLogin(), 0, "CREATE_COUPON");
		
		//--
		
		
		$url = HostConfig::$portalServiceHost.'/cashback/creditDebit';
	
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
	
		if(empty($username)||$username==""||$username==null){
			$username="SYSTEM";
		}
	
		$serviceFG = FeatureGateKeyValuePairs::getBoolean("useCashbackService");
		if(!$serviceFG){
			//$returnObject = MyntCashRawService::debit($transaction, $username);
			//Profiler::endTiming($profileHandler);
				
	
			return $returnObject;
		}
	
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);
	
		$transactionJsonC = $creditTransaction->getJson();
		$transactionJsonD = $debitTransaction->getJson();
		$restRequest = new RestRequest($url, 'POST', "[$transactionJsonC,$transactionJsonD]");
	
		$weblog->debug("creditAndDebitFromUserMyntCashAccount : ".print_r($transactionJsonC,true)." \n ".print_r($transactionJsonD,true)."\n : headers: ".print_r($headers,true));
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
	
		Profiler::endTiming($profileHandler);
		$weblog->debug("creditDebitFromUserMyntCashAccount : $responseBody");
	
	
		return $returnObject;
	
	}
	
	
	/*
	 *debits from myntcash account for the given transaction object and logs to subsequent orderID
	*@params  $transaction : transaction object for the debit
	*@param $orderID : orderid for which myntcash usage is to be logged
	*
	*@return : returns true if sucess or return self::creditDebitFailure / return self::serviceNotAvailable 
	*          in case of failures
	*/
	public static function creditAndDebitMyntCashForExchangeOrder (MyntCashTransaction $creditTransaction,MyntCashTransaction $debitTransaction,$orderId,$refundAmount,$itemDetails){
		
		if(empty($itemDetails)){
			return false;
		}
		
		$earnedCreditUsed = 0.0;
		$storeCreditUsed = 0.0;
		
		foreach($itemDetails as $item){
			$earnedCreditUsed += $item["earned_credit_usage"];
			$storeCreditUsed += $item["store_credit_usage"];
		}
		
		if(($earnedCreditUsed + $storeCreditUsed) == $refundAmount){
			$creditTransaction->setEarnedCreditAmount($earnedCreditUsed);
			$creditTransaction->setStoreCreditAmount($storeCreditUsed);
		}else{
			if($refundAmount < $storeCreditUsed){
				$creditTransaction->setStoreCreditAmount($refundAmount);
				$creditTransaction->setEarnedCreditAmount(0.0);
			}elseif(($refundAmount-$storeCreditUsed)<$earnedCreditUsed){
				$creditTransaction->setStoreCreditAmount($storeCreditUsed);
				$creditTransaction->setEarnedCreditAmount($refundAmount - $storeCreditUsed);
			}else{
				$creditTransaction->setStoreCreditAmount($refundAmount - $earnedCreditUsed);
				$creditTransaction->setEarnedCreditAmount($earnedCreditUsed);
			}
				
		}
		
		
			
	
		$myntCashUsageResponse = MyntCashService::creditAndDebitFromUserMyntCashAccount($creditTransaction, $debitTransaction);
								
		if(isset($myntCashUsageResponse["status"]) && $myntCashUsageResponse["status"] == "error"){
			return self::creditDebitFailure;
		}elseif(empty($myntCashUsageResponse)){
			return self::serviceNotAvailable;
		}else{
			return MyntCashCalculator::updateMyntCashUsageOnOrder($myntCashUsageResponse, $orderId);
		}
		
	}
	
	/*
	 *debits from myntcash account for the given transaction object and logs to subsequent orderID 
	 *@params  $transaction : transaction object for the debit
	 *@param $orderID : orderid for which myntcash usage is to be logged
	 */
	public static function debitMyntCashForOrder (MyntCashTransaction $transaction,$orderId){
		if($transaction->getDebitAmount() > 0 ){
			$myntCashUsageResponse = MyntCashService::debitFromUserMyntCashAccount($transaction);
			
			if(isset($myntCashUsageResponse["status"]) && $myntCashUsageResponse["status"] == "error"){
				return self::noEnoughBalanceForDebitRequest;
			}elseif(empty($myntCashUsageResponse)){
				return self::serviceNotAvailable;
			}else{			
				return MyntCashCalculator::updateMyntCashUsageOnOrder($myntCashUsageResponse, $orderId);
			}
		}else{
			return true;
		}
	}
	
	/*
	 *debits from myntcash account for the given transaction object and logs to subsequent giftcard-order
	*@params  $transaction : transaction object for the debit
	*@param $orderID : orderid of  giftcard-order , for which myntcash usage is to be logged
	*/
	public static function debitMyntCashForGiftCard(MyntCashTransaction $transaction,$orderId){
		$myntCashUsageResponse = MyntCashService::debitFromUserMyntCashAccount($transaction);
		return MyntCashCalculator::updateMyntCashUsageOnGiftCard($myntCashUsageResponse, $orderId);
	}
	
	/*
	 * fetchs formatted transaction logs to display on mymyntra page for a given user-login:$login
	 * @param $login : user login id
	 */
	public static function getUserMyntCashTransacionsForMyMyntra($login,$all){
		$profileHandler = Profiler::startTiming("MyntCash service-call");
		
		$serviceFG = FeatureGateKeyValuePairs::getBoolean("useCashbackService");
		if(!$serviceFG){
			
			$returnObject = MyntCashRawService::getTransactionLogs($login);
						
			$newReturnObj = array();			
			$tsKey = -1;
			$tempLog =null;
			foreach ($returnObject as $id => $ret){
				if($tsKey != $ret['modifiedOn'] ){
					if($tsKey == -1){
						$tempLog = $ret;
						$tsKey = $ret['modifiedOn'];
					}else{
						$newReturnObj[] = $tempLog;
						$tsKey = $ret['modifiedOn'];
						$tempLog = $ret;
					}
						
				}else{
					if($tempLog["description"] == $ret['description']){
						$tempLog["creditInflow"] += $ret["creditInflow"];
						$tempLog["creditOutflow"] += $ret["creditOutflow"];
						$tempLog["balance"] = $ret['balance'];
						$tempLog["description"] = $ret['description'];
					}else{
						$newReturnObj[] = $tempLog;
						$tsKey = $ret['modifiedOn'];
						$tempLog = $ret;
					}
				}
			}
			if(!empty($tempLog)){
				$newReturnObj[] = $tempLog;
			}
									
			Profiler::endTiming($profileHandler);
			return array_reverse($newReturnObj);		
			
			
			
	}
		
		$url = HostConfig::$portalServiceHost.'/cashback/transactionlogs/'.$login;
            if ($all == true)  {
                $url = HostConfig::$portalServiceHost.'/cashback/transactionlogsnew/'.$login;
          	}
		
		global $XCART_SESSION_VARS;
		$username = $XCART_SESSION_VARS['login'];
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);
		
		$restRequest = new RestRequest($url);
		
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
		
		Profiler::endTiming($profileHandler);
		
		if(empty($returnObject) || (isset($returnObject["status"])&&$returnObject["status"]=="error")){
			return array();
		}
		
		
		$newReturnObj = array();
		
		$tsKey = -1;
		$tempLog =null;
		foreach ($returnObject as $id => $ret){
			$ret["modifiedOn"] = $ret["modifiedOn"]/1000;
			if($tsKey != $ret['modifiedOn'] ){				
				if($tsKey == -1){
					$tempLog = $ret;					
					$tsKey = $ret['modifiedOn'];
				}else{
					$newReturnObj[] = $tempLog;
					$tsKey = $ret['modifiedOn'];
					$tempLog = $ret;
				}
			
			}else{				
					if($tempLog["description"] == $ret['description']){
						$tempLog["creditInflow"] += $ret["creditInflow"];
						$tempLog["creditOutflow"] += $ret["creditOutflow"];
						$tempLog["balance"] = $ret['balance'];
						$tempLog["description"] = $ret['description'];
					}else{
						$newReturnObj[] = $tempLog;
						$tsKey = $ret['modifiedOn'];
						$tempLog = $ret;
					}
			}
		}
		if(!empty($tempLog)){
			$newReturnObj[] = $tempLog;
		}
		
		foreach ($newReturnObj as $id => $ret){
			$newReturnObj[$id]["login"] = $login;
			$newReturnObj[$id]["modifiedOn"] = date("d-m-Y H:i:s",(int) ($ret['modifiedOn']));
		}
		
		return array_reverse($newReturnObj);
					
	}
	
	/*
	 * credit MyntCash for item cancellation
	 * 
	 * @param $itemDetails : expects in format array( 1=>array("earned_credit_usage"=> , "store_credit_usage" =>) ,
	 * 												  2=>array("earned_credit_usage"=> , "store_credit_usage" =>) 
	 * 												) each sub array representing cashback/myntcash usage by resprective items
	 */
	public static function creditMyntCashForItemCancellation(MyntCashTransaction $transaction,$refundAmount, $itemDetails){
		if(empty($itemDetails)){
			return false;
		}

		global $weblog;
				
		$earnedCreditUsed = 0.0;
		$storeCreditUsed = 0.0; 
		
		foreach($itemDetails as $item){
			$earnedCreditUsed += $item["earned_credit_usage"];
			$storeCreditUsed += $item["store_credit_usage"];				
		}
		
		if(($earnedCreditUsed + $storeCreditUsed) == $refundAmount){
			$transaction->setEarnedCreditAmount($earnedCreditUsed);
			$transaction->setStoreCreditAmount($storeCreditUsed);
		}else{
			if($refundAmount < $storeCreditUsed){
				$transaction->setStoreCreditAmount($refundAmount);
				$transaction->setEarnedCreditAmount(0.0);
			}elseif(($refundAmount-$storeCreditUsed)<$earnedCreditUsed){
				$transaction->setStoreCreditAmount($storeCreditUsed);
				$transaction->setEarnedCreditAmount($refundAmount - $storeCreditUsed);
			}else{
				$transaction->setStoreCreditAmount($refundAmount - $earnedCreditUsed);
				$transaction->setEarnedCreditAmount($earnedCreditUsed);
			}
			
		}
		$response = MyntCashService::creditToUserMyntCashAccount($transaction);
		
		if($response["status"] == "error"){
			return false;
		}

		return true;
	}
	
	/*
	 * credit MyntCash for item return
	*/
	public static function creditMyntCashForItemReturn(MyntCashTransaction $transaction,$refundAmount, $returnDetails){
		if(empty($returnDetails)){
			return false;
		}	
		
		$earnedCreditUsed = 0.0;
		$storeCreditUsed = 0.0;
		$selfDelivery = 0.0;
		
				
		$earnedCreditUsed += $returnDetails["earned_credit_usage"];
		$storeCreditUsed += $returnDetails["store_credit_usage"];
		$selfDelivery += $returnDetails["delivery_credit"];
		
		if($selfDelivery > 0){
			$refundAmount -= $selfDelivery;		// to credit selfDelivery credit as a seperate transaction
		}
		
		if(($earnedCreditUsed + $storeCreditUsed) == $refundAmount){
			$transaction->setEarnedCreditAmount($earnedCreditUsed);
			$transaction->setStoreCreditAmount($storeCreditUsed);
		}else{
			if($refundAmount < $storeCreditUsed){
				$transaction->setStoreCreditAmount($refundAmount);
				$transaction->setEarnedCreditAmount(0.0);
			}elseif(($refundAmount-$storeCreditUsed)<$earnedCreditUsed){
				$transaction->setStoreCreditAmount($storeCreditUsed);
				$transaction->setEarnedCreditAmount($refundAmount - $storeCreditUsed);
			}else{
				$transaction->setStoreCreditAmount($refundAmount - $earnedCreditUsed);
				$transaction->setEarnedCreditAmount($earnedCreditUsed);
			}
				
		}
		
		$response = MyntCashService::creditToUserMyntCashAccount($transaction);
		
		if($response["status"] == "error"){
			return false;
		}
		
		if($selfDelivery > 0){
			//resetting the transaction to credit self delivery credit
			$transaction->setStoreCreditAmount(0.0);
			$transaction->setEarnedCreditAmount($selfDelivery);
			$transaction->setBusinessProcess(MyntCashBusinessProcesses::RETURN_SHIPPING);			
			$response = MyntCashService::creditToUserMyntCashAccount($transaction);
			if($response["status"] == "error"){
				return false;
			}
		}	
	
		return true;
	}
	
	
	
	
	
	/*
	 * credit MyntCash for order cancellation
	 */
	public static function creditMyntCashForOrderCancellation(MyntCashTransaction $transaction,$refundAmount, $orderDetails){
		if(empty($orderDetails)){
			return false;
		}
		
		$earnedCreditUsed = 0.0;
		$storeCreditUsed = 0.0;
		
		$earnedCreditUsed = $orderDetails["earned_credit_usage"];
		$storeCreditUsed = $orderDetails["store_credit_usage"];
		
		if(($earnedCreditUsed + $storeCreditUsed) == $refundAmount){
			$transaction->setEarnedCreditAmount($earnedCreditUsed);
			$transaction->setStoreCreditAmount($storeCreditUsed);
		}else{
			if($refundAmount < $storeCreditUsed){
				$transaction->setStoreCreditAmount($refundAmount);
				$transaction->setEarnedCreditAmount(0.0);
			}elseif(($refundAmount-$storeCreditUsed)<$earnedCreditUsed){
				$transaction->setStoreCreditAmount($storeCreditUsed);
				$transaction->setEarnedCreditAmount($refundAmount - $storeCreditUsed);
			}else{
				$transaction->setStoreCreditAmount($refundAmount - $earnedCreditUsed);
				$transaction->setEarnedCreditAmount($earnedCreditUsed);
			}
				
		}
		
		$response = MyntCashService::creditToUserMyntCashAccount($transaction);
		
		if($response["status"] == "error"){
			return false;
		}
		
		return true;
		
	}
}