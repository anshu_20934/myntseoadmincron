<?php


class MyntCashCalculator{
	
	
	public static function applyMyntCashUsageOnCart(Cart $cart,$userEnteredAmount, $myntCashDetails)
	{
		
		$cartSubTotal = $cart->getCartSubtotal() - $cart->getTotalCouponDiscount() - $cart->getLoyaltyPointsCashUsed();
		
				
		$totalUsage = ($cartSubTotal > $myntCashDetails['balance']) ? $myntCashDetails['balance'] : $cartSubTotal;
		$totalUsage = ($totalUsage > $userEnteredAmount) ? $userEnteredAmount : $totalUsage;
		
		$earnedCreditUsage = 0.0;
		$storeCreditUsage = 0.0;
								
		foreach($cart->getProducts() as $id=>$product){
			$productSubtotal =
			(($product->getProductPrice()
					+ $product->getUnitAdditionalCharges())
					* $product->getQuantity()) - $product->getDiscount() - ($product->getDiscountAmount() * $product->getDiscountQuantity()) 
					- $product->getCartDiscount() 
					- ($product->getLoyaltyPointsUsed()*$cart->getLoyaltyPointsConversionFactor());
			
			$productUsage = $totalUsage * ($productSubtotal / $cartSubTotal);
			
			$product->setMyntCashUsage($productUsage);
		}
		
		$remainingCashAmount = $userEnteredAmount - $totalUsage;
		$shippingOrGiftingCharges = $cart->getShippingCharge() + $cart->getGiftCharge() - $cart->getGiftOrShippingLoyaltyPointsUsageCashEqualent();
		
		if($shippingOrGiftingCharges < 0.1){
			$shippingOrGiftingCharges = 0.0;
		}
		
		$shippingOrGiftingUsage = 0.0;
		if( $remainingCashAmount > 0 && $shippingOrGiftingCharges > 0) {
			if($remainingCashAmount >= $shippingOrGiftingCharges)
				$shippingOrGiftingUsage = $shippingOrGiftingCharges;
			else
				$shippingOrGiftingUsage = $remainingCashAmount;
			
			$cart->setGiftOrShippingMyntCashUsage($shippingOrGiftingUsage);
		}else{
			$cart->setGiftOrShippingMyntCashUsage(0.0);
		}
			
		$cart->setMyntCashUsage();
		
	}
	
	
	
	
	public static function removeMyntCashUsage(Cart $cart){
		foreach($cart->getProducts() as $id=>$product){			
			$product->setMyntCashUsage(0); 				// set product usage to 0			
		}
		$cart->setGiftOrShippingMyntCashUsage(0);   // set gift charges and shipping cost related usaage to 0
		$cart->setMyntCashUsage();					// recompute the usage
	}
	
	
	public static function updateMyntCashUsageOnOrder($usageResponse,$orderid){
		if($usageResponse['businessProcess'] != MyntCashBusinessProcesses::CASHBACK_USED){
			return false;
		}
		$order_result = func_query("SELECT orderid,subtotal,shipping_cost,gift_charges,total,cash_redeemed FROM xcart_orders WHERE group_id=". $orderid);
		
		$totalOrderAmount = 0.0;
		
		foreach($order_result as $order) {
			$totalOrderAmount += $order["cash_redeemed"];
		}
				
		foreach($order_result as $order) {
			$orderId = $order['orderid'];
			$orderDetails = func_query("SELECT itemid,orderid,cash_redeemed,amount,total_amount from xcart_order_details where orderid = $orderId");
			
			/*$total = 0.0;
			
			foreach($orderDetails as $id => $orderDetail){
				$total += $orderDetail['total_amount'];
			}*/
		
			foreach($orderDetails as $id => $orderDetail){
				$earnedCreditUsage = $usageResponse["earnedCreditAmount"] * $orderDetail['cash_redeemed'] / $totalOrderAmount;
				$storeCreditUsage = $usageResponse["storeCreditAmount"] * $orderDetail['cash_redeemed'] / $totalOrderAmount;
	
				db_query("UPDATE xcart_order_details SET store_credit_usage	= $storeCreditUsage , earned_credit_usage = $earnedCreditUsage WHERE itemid = ".$orderDetail['itemid']." and orderid = ".$orderDetail['orderid']);
				
			}
			
			db_query("UPDATE xcart_orders SET  earned_credit_usage	= ".$usageResponse['earnedCreditAmount']." , store_credit_usage =".$usageResponse['storeCreditAmount']." WHERE orderid = ".$orderDetail['orderid']);
		}
		
		return true;
			
	}
	
	public static function updateMyntCashUsageOnGiftCard($usageResponse,$orderId){
		if($usageResponse['businessProcess'] != MyntCashBusinessProcesses::CASHBACK_USED){
			return false;
		}
		
		db_query("UPDATE gift_cards_orders SET store_credit_usage	= ".$usageResponse['storeCreditAmount']." , earned_credit_usage =".$usageResponse['earnedCreditAmount']." WHERE orderid = ".$orderId);
		
		if(db_affected_rows()>0){
			return true;			
		}else{
			return false;
		}			
			
	}
		
	
}