<?php

class CommonActions {
	public static function authenticateUser($request) {
		global $weblog;

		$username = $request->getUsername();
		$password = $request->getUserPassword();
		
		if(!$username || $username == ""){
			RestUtils::sendResponse(401);
		}
		
		try{
			$userRole = PermissionUtils::authenticateUser($username, $password);
		} catch(InvalidArgumentException $e){
			RestUtils::sendResponse(401);
		}
		$weblog->info("Authenticated User with role - $userRole");
		
		if($userRole==null || $userRole != 'SuperUser'){
			RestUtils::sendResponse(401);
		}
		
		return true;
	}
}
?>