<?php
include_once("$xcart_dir/include/class/class.feature_gate.keyvaluepair.php");
include_once("$xcart_dir/include/func/func_sku.php");
include_once("$xcart_dir/modules/apiclient/SkuApiClient.php");
include_once("$xcart_dir/include/solr/solrProducts.php");
include_once (dirname(__FILE__)."/../RestAPI/RestRequest.php");
include_once (dirname(__FILE__)."/../styleService/CatalogServiceClient.php");

class StyleActions
{

    
    function handleRequest($request)
    {
        global $weblog, $sql_tbl;
               $requestData = $request->getRequestVars();
            $action = strtolower($requestData['data']['action']);
            $brandId = trim($requestData['data']['brandId']);
            $articleTypeId = trim($requestData['data']['articleTypeId']);
            $weblog->debug("StylesActions:debug:begin");
            $weblog->info("StylesActions: perform action $action for Sku");
            switch($action) {
                case 'getallbrands':
                    return $this->getAllBrands();
                case 'getallarticletypes':
                    return $this->getAllArticleTypes();
                case 'getbrandarticlename':
                    return $this->getBrandArticleName($brandId, $articleTypeId);
                case 'getstyleinfo':
                    $skus = $requestData['data']['skus']['sku'];
                    return $this->getStyleInfoForSkus($skus);
                case 'getstyledetails':
                    return $this->getStyleDetails($requestData['data']['styles']);
                case 'refreshstylecache':
                    return $this->refreshStyleCache($requestData['data']['styles']['style']);
                default:
                $responseObject = array();
                $responseObject['status'] = array();
                $responseObject['status']['statusCode'] = 600;
                $responseObject['status']['statusMessage'] = "No action supported";
                $responseObject['status']['statusType'] = "SUCCESS";
                return $responseObject;
            }
        return false;
    }
    
    
    private function getAllArticleTypes(){
        $responseObject = array();
        $articlesSQL = "SELECT id AS articleTypeId, typename AS articleTypeName, typecode AS articleTypeCode
         FROM mk_catalogue_classification WHERE parent1 != -1 and parent2 != -1 ORDER BY articleTypeName";
        $results = func_query($articlesSQL,true); 
        $articlesData = array();
        $counter=0;
        if($results) {
            foreach($results as $row) {
                $data = array();
                $data['articletype']['articleTypeId'] = $row['articleTypeId'];
                $data['articletype']['articleTypeName'] = $row['articleTypeName'];
                $data['articletype']['articleTypeCode'] = $row['articleTypeCode'];
                $articlesData[]=$data;
            }
        }
        $responseObject['status'] = array();
        $responseObject['status']['statusCode'] = 600;
        $responseObject['status']['statusMessage'] = "All Article Types";
        $responseObject['status']['statusType'] = "SUCCESS";
        $responseObject['data'] = $articlesData;
        return $responseObject;
    }
    
    private function getAllBrands(){
        $responseObject = array();
        $brandsSQL = "SELECT id AS brandId, attribute_value AS brandName, attribute_code as brandCode
         FROM mk_attribute_type_values WHERE attribute_type = 'Brand' and is_active = 1 ORDER BY brandName";
        $results = func_query($brandsSQL,true); 
        $brandsData = array();
        if($results) {
            foreach($results as $row) {
                $data = array();
                $data['brand']['brandId'] = $row['brandId'];
                $data['brand']['brandName'] = $row['brandName'];
                $data['brand']['brandCode'] = $row['brandCode'];
                $brandsData[]=$data;
            }
        }
        $responseObject['status'] = array();
        $responseObject['status']['statusCode'] = 600;
        $responseObject['status']['statusMessage'] = "Active Brands";
        $responseObject['status']['statusType'] = "SUCCESS";
        $responseObject['data'] = $brandsData;
        return $responseObject;
    }
    
    private function getBrandArticleName($brandId,$articleTypeId){
        
        $responseObject = array();
        $resultData = array();
        if(!empty($brandId)){
            $brandsSQL = "SELECT attribute_value as brandName FROM mk_attribute_type_values where id = $brandId
            AND attribute_type = 'Brand'";
            $brand = func_query_first($brandsSQL,true);
             
            $data['brandName'] = $brand['brandName'];
        }else{
            $data['brandName'] = "";
        }
        if(!empty($articleTypeId)){
            $articlesSQL = "SELECT typename AS articleTypeName from mk_catalogue_classification where id = $articleTypeId";
            $article = func_query_first($articlesSQL,true);
             
            $data['articleTypeName'] = $article['articleTypeName'];
        }else{
            $data['articleTypeName'] = "";
        }
        $resultData[]=$data;
        
        $responseObject['status'] = array();
        $responseObject['status']['statusCode'] = 600;
        $responseObject['status']['statusMessage'] = "Brand And Article";
        $responseObject['status']['statusType'] = "SUCCESS";
        $responseObject['data'] = $resultData;
        return $responseObject;
    }

    private function getStyleInfoForSkus($skus) {
        global $weblog;
        $weblog->info("Get style info for Skus = ". print_r($skus, true));
        $skuIds = array();
        if(count($skus) == 1){
            $skuIds[] = $skus['id'];
        }else{
            foreach($skus as $object) {
                $skuIds[] = $object['id'];
            }    
        }
        
        $responseObject = array();
        if(!empty($skuIds)) {
            $skuIdsStr = implode(",", $skuIds);
     
            $stylsInfoResults = func_query("select mps.id, sosm.sku_id, msp.global_attr_age_group, msp.global_attr_gender,
                                 msp.global_attr_base_colour, msp.global_attr_colour1, msp.global_attr_colour2, msp.global_attr_fashion_type,
                                 msp.global_attr_season, msp.global_attr_year, msp.global_attr_usage, mpo.value as size,mcc1.typename as global_attr_master_category
                                 from mk_product_style mps, mk_style_properties msp, mk_catalogue_classification mcc, mk_product_options mpo,
                                 mk_styles_options_skus_mapping sosm, mk_catalogue_classification mcc1 
                                 where mps.id=sosm.style_id and msp.style_id=sosm.style_id and msp.global_attr_article_type=mcc.id 
                                 and msp.global_attr_master_category=mcc1.id
                                 and mpo.id=sosm.option_id and sosm.sku_id in ($skuIdsStr)", true);
            
            
            $responseObject['status'] = array();
            $responseObject['status']['statusCode'] = 600;
            $responseObject['status']['statusMessage'] = "Style Info Retrieved Successfully";
            $responseObject['status']['statusType'] = "SUCCESS";
            $responseObject['data'] = array();
            foreach($stylsInfoResults as $styleInfo) {
                $responseObject['data'][] = array('style'=>$styleInfo);
            }
        
        } else {
            $responseObject['status'] = array();
            $responseObject['status']['statusCode'] = 700;
            $responseObject['status']['statusMessage'] = "No SkuIds passed";
            $responseObject['status']['statusType'] = "FAILURE";
        }
        return $responseObject;
    }


    private function getStyleDetails($styles) {
        global $weblog;
        $styleIds = array();
        
        if(isset($styles['id']))
            $styles = array($styles);
        
        foreach($styles as $object) {
            $styleIds[] = $object['id'];
        }
        
        $responseObject = array();
        if(!empty($styleIds)) {
            $styleIdsStr = implode(",", $styleIds);
     
            $stylsInfoResults = func_query("select msp.style_id as id,msp.product_display_name as displayName,mcc.typename as articleTypeName from mk_style_properties msp, mk_catalogue_classification mcc
                                 where  msp.global_attr_article_type=mcc.id 
                                 and msp.style_id in ($styleIdsStr)", true);
            
            
            $responseObject['status'] = array();
            $responseObject['status']['statusCode'] = 600;
            $responseObject['status']['statusMessage'] = "Style Info Retrieved Successfully";
            $responseObject['status']['statusType'] = "SUCCESS";
            $responseObject['data'] = array();
            foreach($stylsInfoResults as $styleInfo) {
                $responseObject['data'][] = array('style'=>$styleInfo);
            }
        
        } else {
            $responseObject['status'] = array();
            $responseObject['status']['statusCode'] = 700;
            $responseObject['status']['statusMessage'] = "No style passed";
            $responseObject['status']['statusType'] = "FAILURE";
        }
        return $responseObject;
    }

    private function reindex_service_call($stylesToAdd) {
        $url = HostConfig::$styleServieUrl.'/index/';
        $returnObject=null;
        //if(!is_array($stylesToAdd)) $stylesToAdd= array();
        $headers = array();
        array_push($headers, 'Accept:  application/json');
        array_push($headers,'Content-Type: application/json');
        $request = new RestRequest($url, "POST",json_encode($stylesToAdd));
        $request->setHttpHeaders($headers);
        $request->execute();
        $responseInfo = $request->getResponseInfo();
        $responseBody = $request->getResponseBody();
        $partialReturnObject = json_decode($responseBody);
        if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
            $drelog->debug('reindex_service_call:--'.print_r($request,true));
        }
        if(empty($returnObject)) {
            $returnObject = $partialReturnObject;   
        } else {
            foreach ($partialReturnObject as $key=>$value) {
                $returnObject->$key = $value;
            }
        }
        return $returnObject; 
}

    private function refreshStyleCache($styles) {
        global $weblog;
        global $solrlog, $xcart_dir;
        // invalidate the pdp cache 
        include_once($xcart_dir."/Cache/Cache.php");
        include_once("$xcart_dir/Cache/CacheKeySet.php");
        $solrProducts = new SolrProducts();
        $weblog->info("Refreshing solr and redis cache for styles = ". print_r($styles, true));
        $styleIds = array();
        if(isset($styles['id']))
            $styles = array($styles);
        foreach($styles as $object) {
            $styleIds[] = $object['id'];
        }
        $responseObject = array();
        if(!empty($styleIds)) {
            foreach($styleIds as $id){
                $cache = new Cache(PDPCacheKeys::$prefix.$id, PDPCacheKeys::keySet());
                $cache->invalidate();
                $stylesToAdd[] = $id;
            }

            $isCSRSuccess = true ; 
            $csrMessage = "Catalog service call successful#"; 
            
            $isSSRSuccess = true;
            $ssrMessage = "Style service call successful#"; 

            $bothMessage = "Catalog Service Message: ".$csrMessage.". ".
            			"Search Service Message: ".$ssrMessage.". "; 
            
            $responseObject['status'] = array();
            $responseObject['status']['statusCode'] = 500;
            $responseObject['status']['statusType'] = "FAILURE";
            
            if($isCSRSuccess){
            	if($isSSRSuccess){
            		$responseObject['status']['statusCode'] = 600;
            		$responseObject['status']['statusMessage'] = "All Style related caches successfully cleared.".$bothMessage;
            		$responseObject['status']['statusType'] = "SUCCESS";
            	}
            	else {
            		$responseObject['status']['statusMessage'] = "Catalog Service Cache was cleared but Search Service Cache was not cleared & Solr Not reindexed".$bothMessage;
            	}
            }
            else {
            	if($isSSRSuccess){
            		$responseObject['status']['statusMessage'] = "Style Service Cache was cleared but Catalog Service Cache was not cleared.".$bothMessage;
            	}
            	else {
            		$responseObject['status']['statusMessage'] = "Failed to clear cache for both Style Service as well as Catalog Service.".$bothMessage;
            	}
            }
        }   else {
            $responseObject['status'] = array();
            $responseObject['status']['statusCode'] = 700;
            $responseObject['status']['statusMessage'] = "No style passed";
            $responseObject['status']['statusType'] = "FAILURE";
        }
        return $responseObject;
    }

}

?>
