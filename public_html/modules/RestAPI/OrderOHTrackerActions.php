<?php

use oms\OrderDetailRequestProcessor;

class OrderOHTrackerActions{

	function handleRequest($request)    {
		global $portalapilog, $sql_tbl ;

		$requestParams = $request->getRequestVars();
		$data = $requestParams['data'];
		
		$response =  $this->validateOhResolutionData($data); 
		
		$finalResponse = array();
		
		if($response === true) {
			
			$processor= new OrderDetailRequestProcessor();
			$processor->updateOHResponse($data['orderId'], $data['resolution'], $data['disposition'], $data['attemptNumber'], $data['attemptTime'], $data['userId'], $data['remarks'], $data['action'], $data['address'], $data['phoneNo']);
			
			$finalResponse['status']['statusCode'] = 600;
			$finalResponse['status']['statusMessage'] = "Order No ".$data['orderId']." updated successfully with resolution ".$data['resolution']." and disposition ".$data['disposition']." for action ". $data['action'];
			$finalResponse['status']['statusType'] = "SUCCESS";
		} else {
			$finalResponse['status']['statusCode'] = 901;
			$finalResponse['status']['statusType'] = "ERROR";
			$finalResponse['status']['statusMessage'] = $response;
		}
		
		return $finalResponse;
	}
	
	
	private function validateOhResolutionData($data) {
		if(!$data['action'] || empty($data['action'])) {
			return "Please provide action to be performed for on hold resolution";
		}
		
		if(!$data['orderId'] || empty($data['orderId'])) {
			return "Please provide orderid for action ".$data['action'];
		}
		
		$validOrder = func_query_first("select orderid from xcart_orders where orderid = ".$data['orderId'], true);
		if(!$validOrder)
			return "OrderId ".$data['orderId']." is invalid";
		
		if(!$data['userId'] || empty($data['userId'])) {
			return "Please provide userId for action ".$data['action'];
		}
		
		if(!$data['attemptNumber'] || empty($data['attemptNumber'])) {
			return "Attempt Number not provided for action ".$data['action'];
		}
		
		if(!$data['attemptTime'] || empty($data['attemptTime'])) {
			return "Attempt Time is not provided for action ".$data['action']." on order ".$data['orderId'];
		}
		
		if(!$data['resolution'] || empty($data['resolution'])) {
			return "Resolution is not provided for action ".$data['action']." on order ".$data['orderId'];
		}
		if(!$data['disposition'] || empty($data['disposition'])) {
			return "Disposition is not provided for action ".$data['action']." on order ".$data['orderId'];
		}
		
		$row = func_query_first("select reason,reason_display_name,parent_reason from mk_order_action_reasons where action ='oh_disposition' and is_internal_reason = 0 and parent_reason = '".$data['resolution']."' and reason = '".$data['disposition']."'", true);
		if(!$row) {
			return "Invalid Resolution ".$data['resolution']." with Disposition ".$data['disposition']." is not provided for action ".$data['action']." on order ".$data['orderId'];
		}
		
		if(!$data['remarks'] || empty($data['remarks'])) {
			return "Remarks is not provided for action ".$data['action']." on order ".$data['orderId'];
		}
		
		if(strtolower($data['action']) == 'modify') {
		
			if($data['disposition'] == 'NAV' || $data['disposition'] == 'NAPV') {
				if($data['address'] && !empty($data['address'])) {
					if(!$data['address']['street'] || empty($data['address']['street'])) {
						return "Street is not provided for action ".$data['action']." on order ".$data['orderId'];
					}
					if(!$data['address']['locality'] || empty($data['address']['locality'])) {
						return "Locality is not provided for action ".$data['action']." on order ".$data['orderId'];
					}
					if(!$data['address']['city'] || empty($data['address']['city'])) {
						return "City is not provided for action ".$data['action']." on order ".$data['orderId'];
					}
					if(!$data['address']['state'] || empty($data['address']['state'])) {
						return "State is not provided for action ".$data['action']." on order ".$data['orderId'];
					}
					if(!$data['address']['zipcode'] || empty($data['address']['zipcode'])) {
						return "Zipcode is not provided for action ".$data['action']." on order ".$data['orderId'];
					}
				} else {
					return "Address not provided for action ".$data['action']." on order ".$data['orderId'];
				}	
			} 
			
			if($data['disposition'] == 'NPV' || $data['disposition'] == 'NAPV') {
				if(!$data['phone'] || empty($data['phone'])) {
					return "Phone number is not provided for action ".$data['action']." on order ".$data['orderId'];
				}
			}
		}
		
		return true;
	}
}

?>