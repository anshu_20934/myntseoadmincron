<?php
use saveditems\action\SavedItemRemoveAction;

use saveditems\action\SavedItemAddAction;

use enums\cart\CartContext;
use style\builder\CachedStyleBuilder;

include_once "$xcart_dir/include/func/func.mkcore.php";
include_once ("$xcart_dir/include/class/mcart/class.MCartFactory.php");
include_once ("$xcart_dir/include/class/mcart/class.MCart.php");
include_once $xcart_dir.'/include/func/func.order.php';
include_once("$xcart_dir/include/func/func.courier.php");

class CartActions {

	function handleRequest($request)    {
		global $portalapilog;
		$requestData = $request->getRequestVars();
		$action = strtolower($requestData['data']['action']);
		$portalapilog->info("CartAction: perform action $action");
		$context = strtolower($requestData['data']['context']);
		if(empty($context))
			$context = CartContext::DefaultContext;
		switch($action) {
			case 'applycoupon':
				$coupon = $requestData['data']['coupon'];
				$mcartFactory= new \MCartFactory();
				$myCart = $mcartFactory->getCurrentUserCart(true, $context);
				$myCart->addCouponCode($coupon, MCart::$__COUPON_TYPE_DEFAULT);
				$myCart->refresh(false,true);
				MCartUtils::persistCart($myCart);
				return CartActions::cartToArray($myCart);

			case 'removecoupon':
				$coupon = $requestData['data']['coupon'];
				$mcartFactory= new \MCartFactory();
				$myCart = $mcartFactory->getCurrentUserCart(true, $context);
				$myCart->removeCoupon($coupon, MCart::$__COUPON_TYPE_DEFAULT);
				$myCart->refresh(false,true);
				MCartUtils::persistCart($myCart);
				return CartActions::cartToArray($myCart);
			case 'removemyntcredit':
				$mcartFactory= new \MCartFactory();
				$myCart = $mcartFactory->getCurrentUserCart(true, $context);
				$myCart->removeMyntCashUsage();
				$myCart->refresh(false,true);
				MCartUtils::persistCart($myCart);
				return CartActions::cartToArray($myCart);
			case 'applymyntcredit':
				$mcartFactory= new MCartFactory();
				$useAmount = $requestData['data']['useAmount'];
				$myCart = $mcartFactory->getCurrentUserCart(true, $context);
				$myCart->enableMyntCashUsage($useAmount);
				$myCart->refresh(false,true);
				MCartUtils::persistCart($myCart);
				return CartActions::cartToArray($myCart);
			case 'removeloyaltypointsusage':
				$mcartFactory= new \MCartFactory();
				$myCart = $mcartFactory->getCurrentUserCart(true, $context);
				$myCart->disableLoyaltyPointsUsage();
				$myCart->refresh(false,true);
				MCartUtils::persistCart($myCart);
				return CartActions::cartToArray($myCart);
			case 'enableloyaltypointsusage':
				$mcartFactory= new \MCartFactory();
				$myCart = $mcartFactory->getCurrentUserCart(true, $context);
				$myCart->enableLoyaltyPointsUsage(0);
				$myCart->refresh(false,true);
				MCartUtils::persistCart($myCart);
				return CartActions::cartToArray($myCart);			
			case 'addgiftmessage':
				$mcartFactory= new \MCartFactory();
				$giftTo = filter_var($requestData['data']["giftMessage"]['recipient'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
				$giftFrom = filter_var($requestData['data']["giftMessage"]['sender'],  FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
				$giftMessage = filter_var($requestData['data']["giftMessage"]['message'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
				$giftMessage = substr($giftTo,0,60)."___".substr($giftFrom,0,60)."___".substr($giftMessage,0,200);
				$giftingCharges = WidgetKeyValuePairs::getInteger('giftwrap.charges');
				$myCart = $mcartFactory->getCurrentUserCart(true, $context);
				$myCart->setIsGiftOrder(true);
				$myCart->setGiftCharge($giftingCharges);
				$myCart->setGiftMessage($giftMessage);
				$myCart->setAsChanged();
				$myCart->refresh(false,true);
				MCartUtils::persistCart($myCart);
				return CartActions::cartToArray($myCart);
			case 'removegiftmessage':
				$mcartFactory= new \MCartFactory();
				$myCart = $mcartFactory->getCurrentUserCart(true, $context);
				$myCart->setIsGiftOrder(false);
				$myCart->setGiftCharge(0);
				$myCart->setGiftMessage(null);
				$myCart->setAsChanged();
				$myCart->refresh(false,true);
				MCartUtils::persistCart($myCart);
				return CartActions::cartToArray($myCart);
			case 'changegift':
				$mcartFactory= new \MCartFactory();
				$myCart = $mcartFactory->getCurrentUserCart(true, $context);
				$product = $myCart->getProduct($requestData['data']["itemId"]);
				if($product!=null){
					$myCart->removeItem($product,false);
					$mcartItem= new MCartNPItem($requestData['data']["itemId"],$requestData['data']["skuId"],1);
					$mcartItem->itemAddedByDiscountEngine = true;
					$mcartItem->discountRuleId = $product->discountRuleId;
					$myCart->addProduct($mcartItem);
					MCartUtils::persistCart($myCart);
				}
				return CartActions::cartToArray($myCart);
				 
			case 'addressserviceability':
				$mcartFactory= new MCartFactory();
				$myCart = $mcartFactory->getCurrentUserCart(true, $context);
				return CartActions::getAddressServiceability($myCart, $requestData['data']);
			case 'addressserviceabilitynew':
				return CartActions::getAddressServiceabilityNew($requestData['data']);
			case "movetowishlist":
				$mcartFactory= new MCartFactory();
				$myCart = $mcartFactory->getCurrentUserCart(true, $context);
				$cartItem = $myCart->getProduct($requestData['data']["itemId"]);
				if($cartItem!=null){
					global $XCART_SESSION_VARS;
					$myCart->removeItem($cartItem,true);
					$_GET["_token"]=$XCART_SESSION_VARS['USER_TOKEN'];//TODO
					$_POST["_token"]=$XCART_SESSION_VARS['USER_TOKEN'];//TODO to remove later
					$_REQUEST["_token"]=$XCART_SESSION_VARS['USER_TOKEN'];//TODO to remove later
					$_POST["productSKUID"] = $cartItem->getSkuId();
					$_POST['sizequantity'] = array(0=>$cartItem->getQuantity());
					$actionHandler = new SavedItemAddAction();
					$val = $actionHandler->run();
					MCartUtils::updateSessionCartVariables($actionHandler->getSaved(),CartContext::SavedContext);
					MCartUtils::persistCart($myCart);
					MCartUtils::updateSessionCartVariables($myCart,$context);
				}
				return CartActions::cartToArray($myCart);
			case "movetocart":
				global $XCART_SESSION_VARS;
				$_GET["_token"]=$XCART_SESSION_VARS['USER_TOKEN'];//TODO
				$_POST["_token"]=$XCART_SESSION_VARS['USER_TOKEN'];//TODO to remove later
				$_REQUEST["_token"]=$XCART_SESSION_VARS['USER_TOKEN'];//TODO to remove later
				$_REQUEST["itemId"] = $requestData['data']["itemId"];
				$actionHandler = new SavedItemRemoveAction();
				$myWishlist=$actionHandler->getSaved();
				$wishlistItem = $myWishlist->getProduct($requestData['data']["itemId"]);
				if($wishlistItem!=null){
					$actionHandler->run();
					$mcartFactory= new MCartFactory();
					$myCart = $mcartFactory->getCurrentUserCart(true, $context);
					try {
						$mcartItem= new MCartNPItem($wishlistItem->getProductId(),$wishlistItem->getSkuId(),$wishlistItem->getQuantity());
						$myCart->addProduct($mcartItem);
					} catch (ProductAlreadyInCartException $e) {
					}
					MCartUtils::persistCart($myCart);
					MCartUtils::updateSessionCartVariables($myCart,$context);
				}
				return CartActions::cartToArray($myWishlist);
			default:
				$responseObject = array();
				$responseObject['status'] = array();
				$responseObject['status']['statusCode'] = 600;
				$responseObject['status']['statusMessage'] = "No action supported";
				$responseObject['status']['statusType'] = "SUCCESS";
				return $responseObject;
		}
		return false;
	}


	/**
	 * Get coupons in sorted order.
	 * @param $myCart \MCart
	 */
	public static function cartToArray($myCart) {

		$styleMap = array();
		foreach($myCart->getProducts() as $itemId => $cartItem){
			$style = $cartItem->getStyleArray();
			$styleMap[] = $style["id"];
		}
		$cachedStyleBuilderObj = CachedStyleBuilder::getInstance();
		$styleMap = $cachedStyleBuilderObj->getStyleObjects($styleMap);
		foreach($myCart->getProducts() as $itemId => $cartItem){
			$style = $cartItem->getStyleArray();
			
			$styleObj = $styleMap[$style["id"]];

			if (empty($styleObj)) {
				continue;
			}
			$styleProperties=$styleObj->getStyleProperties();
			$style["default_image"] = $styleProperties["default_image"];
			$productOptions = array();
			$allProductOptionDetails = $styleObj->getAllProductOptions();
			$allSizeOptions = $styleObj->getSizeUnificationDataForAllOptions();
			foreach($allProductOptionDetails as $option){
				$productOptions [$option["id"]]["id"]=$option["id"];
				$productOptions [$option["id"]]["name"]=$option["name"];
				$productOptions [$option["id"]]["value"]=$option["value"];
				$productOptions [$option["id"]]["sku_id"]=$option["sku_id"];
				$productOptions [$option["id"]]["price"]=$option["price"];
				$productOptions [$option["id"]]["unifiedSize"]=$allSizeOptions[$option["value"]];

                $maxCount = 
                    FeatureGateKeyValuePairs::getFeatureGateValueForKey("maxQuantityForCartItemDisplay"); 
				if($cartItem->getSkuId() == $option["id"]){ // TO use availability of cart item for selected sku
					$productOptions [$option["id"]]["sku_count"] = $cartItem->getItemAvailableQuantity()>$maxCount ? 
                        $maxCount : $cartItem->getItemAvailableQuantity();
					$productOptions [$option["id"]]["is_active"]=$option["is_active"];
					$productOptions [$option["id"]]["available"]=($cartItem->getItemAvailability()==2);
				}else{
					$productOptions [$option["id"]]["sku_count"] = $option["sku_count"]>$maxCount ? 
                        $maxCount :  $option["sku_count"];
					$productOptions [$option["id"]]["is_active"]=$option["is_active"];
					$productOptions [$option["id"]]["available"]=false;
				}
			}
			$productOptionDetails = $styleObj->getActiveProductOptionsData();
			foreach($productOptionDetails as $option){
				$productOptions [$option["id"]]["available"]=true;
			}
			
			$order = 0;
			foreach($allSizeOptions as $nu=>$u){
				foreach( $productOptions as $oid=>$op){
					if($nu == $op['value']){
						$productOptions [$oid]["order"]=$order++;
						break;
					}
				}
			}
				
				
			if($cartItem->isItemAddedByDiscountEngine()==true && $cartItem->comboId==0){
				$paramsArray = convertToArrayRecursive($myCart->displayData->params);
				$cartItem->discountDisplayText = array("id"=>"Free_Item_CartLevel_CM_MC_IL","params"=>$paramsArray);
			}
				
			$style["productOptions"] = $productOptions;
			$cartItem->setStyleArray($style);
		}
		$cartDisplayData = $myCart->displayData;
		if($cartDisplayData->id=="Voucher_Cart_CartLevel_CA_NM") {
			$myCart->displayData->id = WidgetKeyValuePairs::getWidgetValueForKey('DiscVoucherBannerCartTopNM');
		} else if($cartDisplayData->id=="Voucher_Cart_CartLevel_CM") {
			$myCart->displayData->id = WidgetKeyValuePairs::getWidgetValueForKey('DiscVoucherBannerCartTopCM');
		}
		MCartUtils::updateSessionCartVariables($myCart,$myCart->getContext());
		$result=$myCart->toArray();

		return $result;
	}

	public static function getJewelleryItemsInCart($productsInCart){
		$jewelleryItemsInOrder = 'none'; // other values can be 'some', 'all';

		$productAttrType = array();
		$productStyleIds = array();
		foreach($productsInCart as $product){
			$productAttrType[] = $product['article_type'];
			$productStyleIds[] = $product['productStyleId'];
		}

		$productAttrType = array_unique($productAttrType);
		$productStyleIds = array_unique($productStyleIds);

		if(empty($productAttrType)|| empty($productStyleIds)){
			return $jewelleryItemsInOrder;
		}

		$fineJewelleryItem = func_query("select * from mk_attribute_type at, mk_attribute_type_values av, mk_style_article_type_attribute_values sav where
				at.id = av.attribute_type_id and av.id = sav.article_type_attribute_value_id and
				at.catalogue_classification_id in (".implode(",", $productAttrType).") and sav.product_style_id in (".implode(",", $productStyleIds).") and av.attribute_value = 'Fine Jewellery'", true);

		if($fineJewelleryItem && count($fineJewelleryItem)>0){
			if(count($fineJewelleryItem) == count($productStyleIds)){
				$jewelleryItemsInOrder = 'all';
			}else{
				$jewelleryItemsInOrder = 'some';
			}
		}

		return $jewelleryItemsInOrder;
	}


	/**
	 *
	 * @param MCart $myCart
	 * @param unknown_type $requestData
	 */
	public static function getAddressServiceability($myCart, $requestData) {

		global $XCART_SESSION_VARS;
		$addressMap = array();
		$login = $myCart->getLogin();
		$mobile = $XCART_SESSION_VARS['mobile'];
		$totalAmount = $myCart->getMrp()+ $myCart->getAdditionalCharges() - $myCart->getDiscountAmount() -
		$myCart->getDiscount() - $myCart->getTotalMyntCashUsage() + $myCart->getShippingCharge() -
		$myCart->getCartLevelDiscount() + $myCart->getGiftCharge();
		$productsInCart = MCartUtils::convertCartToArray($myCart);
		$cashDiscount = $myCart->getTotalMyntCashUsage();
		$jewelleryItemsInOrder = CartActions::getJewelleryItemsInCart($productsInCart);
		$productids = $myCart->getProductIdsInCart();
		$giftWrapOrder = $myCart->getIsGiftOrder();
		
		if(!is_array($requestData["address"][0])){
			$addresses[] = $requestData["address"];
		}else{
			$addresses = $requestData["address"];
		}
		
		$skuDetails = array();
		foreach($productsInCart as $index=>$product) {
			$productsInCart[$index]['style_id'] = $product['productStyleId'];
			$productsInCart[$index]['sku_id'] = $product['skuid'];
			$skuDetails[$product['skuid']] = array('id'=>$product['skuid'], 'availableItems'=> $product['availableItems']);
			if(!empty($product['availableInWarehouses']) && count($product['availableInWarehouses']) > 0) {
				foreach(explode(",", $product['availableInWarehouses']) as $whId)
					$skuDetails[$product['skuid']]['warehouse2AvailableItems'][$whId] = 1;
			} 
		}
								
		foreach($addresses as $address){
			$country = $address['country'];
			$zipcode = $address['pincode'];
			global  $courierServiceabilityVersion;
			$paymentModes = getServiceablePaymentModesForProducts($zipcode, $productsInCart, false, false, $skuDetails);
			if(empty($codAvailability) || empty($codAvailability["errorCode"]) || $codAvailability["errorCode"]==2 || $codAvailability["errorCode"]==7 ){
				if($courierServiceabilityVersion == 'new') {
					$codAvailability = verifyCODElegiblityForUser($login,$mobile,$country,$totalAmount,$zipcode,$productsInCart,$productids,$cashDiscount,$jewelleryItemsInOrder,$giftWrapOrder,$paymentModes);
				} else {
					$codAvailability = verifyCODElegiblityForUser($login,$mobile,$country,$totalAmount,$zipcode,$productsInCart,$productids,$cashDiscount,$jewelleryItemsInOrder);
				}
			}
			
			$addressMap[$country][$zipcode]["cod"]["available"] = empty($codAvailability["errorCode"]);
			$addressMap[$country][$zipcode]["cod"]["errorCode"] = $codAvailability["errorCode"];
			$addressMap[$country][$zipcode]["cod"]["errorMessage"] = $codAvailability["errorMessage"];
			 
			foreach($myCart->getProducts() as $itemId=>$cartItem){
				$whId2CountMap = $skuDetails[$cartItem->getSkuId()]['warehouse2AvailableItems'];
				$serviceability = fetchProductServiceabilityAndTat($cartItem->getStyleId(), $cartItem->getSkuId(), $zipcode, array_keys($whId2CountMap), $cartItem->getSupplyType(), $cartItem->getLeadTime(), $paymentModes);
				if(empty($addressMap[$country][$zipcode]["serviceability"]) && strcmp($serviceability["DELIVERY_PROMISE_TIME"],$addressMap[$country][$zipcode]["serviceability"]["DELIVERY_PROMISE_TIME"])){
					$addressMap[$country][$zipcode]["serviceability"] = $serviceability;
				}
			}
			
		}
		return $addressMap;
    }

    /**
     *
     * @param MCart $myCart
     * @param unknown_type $requestData
     */
    public static function getAddressServiceabilityNew( $data) {
    
    	global $XCART_SESSION_VARS;
    	$addressMap = array();
    	$login = $data[cart][login];
    	$mobile = $XCART_SESSION_VARS['mobile'];
    	$totalAmount = $data[cart][totalPrice];
    	$productsInCart = $data[cart][sCartItems];
    	$cashDiscount = $data[cart][myntCashUsage];
    	$jewelleryItemsInOrder = $data[cart][hasJewellery];
    	foreach($data[cart][sCartItems] as &$item){
    		$productids[]=$item[styleId];
    		$item[skuid]=$item[skuId];
    		$item[productStyleId]=$item[styleId];
    	}
    	$giftWrapOrder = $data[cart][giftOrder];
    
    	$addresses = $data["addresses"];
    	
    	$skuDetails = array();
    	foreach($productsInCart as $index=>$product) {
    		$productsInCart[$index]['style_id'] = $product['productStyleId'];
    		$productsInCart[$index]['sku_id'] = $product['skuid'];
	   		if($product['skuAvailabilityDetail'] && !empty($product['skuAvailabilityDetail'])) {
    			$productsInCart[$index]['availableInWarehouses'] = $product['skuAvailabilityDetail']['availableInWarehouses'];
    			$productsInCart[$index]['supplyType'] = $product['skuAvailabilityDetail']['supplyType'];
    			$productsInCart[$index]['leadTime'] = $product['skuAvailabilityDetail']['leadTime'];
    		} else {
    			$productsInCart[$index]['supplyType'] = 'ON_HAND';
    			$productsInCart[$index]['leadTime'] = 0;
    			$productsInCart[$index]['availableInWarehouses'] = array_keys($product['warehouseIdToItemCountMap']);
    		}
    	}
    
    	foreach($addresses as $address){
    		$country = $address['country'][code];
    		$zipcode = $address['pincode'];
    		$paymentModes = array();
    		if($address[addressServiceability][codServiceability]=="SERVICEABLE")
    			$paymentModes[]="cod";
    		if($address[addressServiceability][nonCODServiceability]=="SERVICEABLE")
    			$paymentModes[]="on";
    		
    		if(empty($codAvailability) || empty($codAvailability["errorCode"]) || $codAvailability["errorCode"]==2 || $codAvailability["errorCode"]==7 ){
    			$codAvailability = verifyCODElegiblityForUser($login,$mobile,$country,$totalAmount,$zipcode,$productsInCart,$productids,$cashDiscount,$jewelleryItemsInOrder,$giftWrapOrder,$paymentModes);
    		}
    			
    		$addressMap[$country][$zipcode]["cod"]["available"] = empty($codAvailability["errorCode"]);
    		$addressMap[$country][$zipcode]["cod"]["errorCode"] = $codAvailability["errorCode"];
    		$addressMap[$country][$zipcode]["cod"]["errorMessage"] = $codAvailability["errorMessage"];
    
    		foreach($productsInCart as $cartItem){
    			//$serviceability = checkProductServiceability($cartItem[styleId], $cartItem[skuId], $zipcode, $paymentModes);
    			$warehouses = explode(",", $cartItem['availableInWarehouses']);
    			if(!empty($warehouses)) {
    				$serviceability = fetchProductServiceabilityAndTat($cartItem[styleId], $cartItem[skuId], $zipcode, $warehouses, $cartItem['supplyType'], $cartItem['leadTime'], $paymentModes);
    				$addressMap[$country][$zipcode]["serviceability"] = $serviceability;
    			}
    			/* if(empty($addressMap[$country][$zipcode]["serviceability"]) && strcmp($serviceability["DELIVERY_PROMISE_TIME"],$addressMap[$country][$zipcode]["serviceability"]["DELIVERY_PROMISE_TIME"])){
    				$addressMap[$country][$zipcode]["serviceability"] = $serviceability;
    			} */
    		}
    	}
    	return $addressMap;
    }
}


?>
