<?php
include_once("$xcart_dir/include/func/func.mk_old_returns_tracking.php");

class RtoActions{

    function handleRequest($request){
    	global $weblog, $sql_tbl;
       		$requestData = $request->getRequestVars();
        	$action = $requestData['data']['action'];
 			switch($action) {
				case 'getRtoDetails':
					return $this->getRtoDetails($requestData['data']);
				default:
					$responseObject = array();
		            $responseObject['status'] = array();
		            $responseObject['status']['statusCode'] = 600;
		            $responseObject['status']['statusMessage'] = "No action defined for $action";
		            $responseObject['status']['statusType'] = "ERROR";
                	return $responseObject;
		}

        return false;
    }

	function getRtoDetails($data) {
		$orderid = $data['orderId'];
		$rtoDetails = array();
		$responseDetails = array();

		if($orderid){
			$rtoDetails = getRTODetailsForOrderId($orderid);
		}
		if($rtoDetails){
			$responseDetails['rto_details'] = $rtoDetails;
			$responseDetails['status']['statusCode'] = 900;
			$responseDetails['status']['statusType'] = "SUCCESS";
		} else {
			$responseDetails['status']['statusCode'] = 500;
			$responseDetails['status']['statusType'] = "ERROR";
			$responseDetails['status']['statusMessage'] = "No returns found for given order";
		}

		return $responseDetails;
	}
}
?>
