<?php

include_once($xcart_dir."/Cache/TableCache.php");
include_once "$xcart_dir/include/dao/class/search/class.MyntraPageDBAdapter.php";
    
class MyntraPageActions {
    
	function handleRequest($request)    {
            global $portalapilog;
            $requestData = $request->getRequestVars();
            $action = strtolower($requestData['data']['action']);
            $portalapilog->info("MyntraPageActions: perform action $action");
            $tableName = strtolower($requestData['data']['tableName']);
            $tableKey = strtolower($requestData['data']['tableKey']);
            $keys = $requestData['data']['keys'];
            switch($action) {
        	case 'refresh':
                    $cache = TableCache::getTableCache($tableName, $tableKey);
                    foreach ($keys as $key) {
                        $cache->refresh($key);
                    }
                    return true;
            case 'refreshcuratedfromlandiing' :
                    $dbAdapter=MyntraPageDBAdapter::getInstance();
                    $cache = TableCache::getTableCache("curated_search_list", "fk_parameterized_page_id");
                    foreach ($keys as $key) {
                        $parameterized_page_id = $dbAdapter->getParametrizedIdForPageUrl($key);
                        $cache->refresh($parameterized_page_id);
                    }
                    return true;               
        	default:
        		$responseObject = array();
        		$responseObject['status'] = array();
        		$responseObject['status']['statusCode'] = 600;
        		$responseObject['status']['statusMessage'] = "No action supported";
        		$responseObject['status']['statusType'] = "SUCCESS";
        		return $responseObject;
            }	
            return false;        
	}
}        

?>
