<?php
include_once "$xcart_dir/include/func/func.loginhelper.php";
include_once "$xcart_dir/include/sessionstore/serialization_format_wrapper.php";
use revenue\payments\service\PaymentServiceInterface;
class SessionActions {

	function handleRequest($request)    {
		global $portalapilog, $sql_tbl;
       	$requestData = $request->getRequestVars();
        $action = strtolower($requestData['data']['action']);
        $portalapilog->info("OrderActions: perform action $action");
        switch($action) {
        	case 'getloginfromsession':
        		return $this->getLoginFromSession($requestData['data']);
        	case 'getloginexpiryflagfromsession':
        		return $this->getSessionExpiryStatus($requestData['data']);
        	case 'getloginandsessionstatus':
        		return $this->getLoginAndSessionStatus($requestData['data']);
        	default:
        		$responseObject = array();
        		$responseObject['status'] = array();
        		$responseObject['status']['statusCode'] = 600;
        		$responseObject['status']['statusMessage'] = "No action supported";
        		$responseObject['status']['statusType'] = "SUCCESS";
        		return $responseObject;
        }	
        return false;
	}
	
	private function getLoginFromSession($requestData){
		$responseObject = array();
		global $portalapilog;

		$portalapilog->info(print_r($requestData, true));
		$sessionId = $requestData['sessionid'];
		$session = getSessionBySessionId($sessionId);
		$session_data = x_session_unserialize($sessionId, $session["data"]);
		$userid = $session_data['login'];
		
		$responseObject['status'] = array();
		if(empty($userid)){
			$responseObject['status']['statusType'] = "FAILURE";
			$responseObject['status']['statusMessage'] = "No login Id found for the session.";
		}else{
			$responseObject['status']['statusType'] = "SUCCESS";
			$responseObject['status']['statusMessage'] = "Login Id extracted successfuly.";
			$responseObject['login'] = $userid;
		}
		
		return $responseObject;
	}
	
	private function getSessionExpiryStatus($requestData){
		$responseObject = array();
		global $portalapilog;
		$portalapilog->info(print_r($requestData, true));
		$sessionId = $requestData['sessionid'];
		$secureSessionId = $requestData['securesessionid'];
		$session_data = getSessionBySessionId($sessionId);
		$session = x_session_unserialize($sessionId, $session_data['data']);
		$responseObject['status'] = array();
		if(PaymentServiceInterface::showLoginPromptForUser($session['login'])){
			if($secureSessionId != $session['sxid']){
				$responseObject['status']['statusType'] = "SUCCESS";
				$responseObject['status']['statusMessage'] = "sxid doesnt match.expired";
				$responseObject['expired'] ="true";
			}
			else {
				if($session['prompt_login']){
					$responseObject['status']['statusType'] = "SUCCESS";
					$responseObject['status']['statusMessage'] = "threshold crossed.expired (".$curtime."-".$lastSessionTime.">".$sessionInactiveThreshold.")";
					$responseObject['expired'] = "true";
				}else {
					$sessionInactiveThreshold = FeatureGateKeyValuePairs::getInteger("sessionInactiveThreshold", 15*60);
					$curtime = time();
					if(!isset($session['lastSessionTime'])) {
						$lastSessionTime = $curtime;	
					} else {
						$lastSessionTime = $session['lastSessionTime'];
					}
					if(($curtime - $lastSessionTime) > $sessionInactiveThreshold){
						$responseObject['status']['statusType'] = "SUCCESS";
						$responseObject['status']['statusMessage'] = "threshold crossed.expired (".$curtime."-".$lastSessionTime.">".$sessionInactiveThreshold.")";
						$responseObject['expired'] = "true";
					}
					else {
						$responseObject['status']['statusType'] = "SUCCESS";
						$responseObject['status']['statusMessage'] = "session is active(".$curtime."-".$lastSessionTime.">".$sessionInactiveThreshold.")";
						$responseObject['expired'] = "false";	
					}
				}
			}
		}
		else {
			$responseObject['status']['statusType'] = "SUCCESS";
			$responseObject['status']['statusMessage'] = "Session check disabled. Session never expires";
			$responseObject['expired'] = "false";
		}

		return $responseObject;
	}
	
	private function getLoginAndSessionStatus($requestData){
		$responseObject = array();
		global $portalapilog;
		$portalapilog->info(print_r($requestData, true));
		$sessionId = $requestData['sessionid'];
		$secureSessionId = $requestData['securesessionid'];
		$session_data = getSessionBySessionId($sessionId);
		$session = x_session_unserialize($sessionId, $session_data['data']);
		$responseObject['status'] = array();
		if (isGuestCheckout()) {
			$responseObject['status']['statusType'] = "SUCCESS";
			$responseObject['status']['statusMessage'] = "session is active";
			$responseObject['expired'] = "false";
			$responseObject['login']=$session['glogin'];
			return $responceObject;
		}
		if(PaymentServiceInterface::showLoginPromptForUser($session['login'])){
			if($secureSessionId != $session['sxid']){
				$responseObject['status']['statusType'] = "SUCCESS";
				$responseObject['status']['statusMessage'] = "sxid doesnt match.expired";
				$responseObject['expired'] ="true";
			}
			else {
				if($session['prompt_login']){
					$responseObject['status']['statusType'] = "SUCCESS";
					$responseObject['status']['statusMessage'] = "threshold crossed.expired (".$curtime."-".$lastSessionTime.">".$sessionInactiveThreshold.")";
					$responseObject['expired'] = "true";
				}else {
					$sessionInactiveThreshold = FeatureGateKeyValuePairs::getInteger("sessionInactiveThreshold", 15*60);
					$curtime = time();
					if(!isset($session['lastSessionTime'])) {
						$lastSessionTime = $curtime;
					} else {
						$lastSessionTime = $session['lastSessionTime'];
					}
					if(($curtime - $lastSessionTime) > $sessionInactiveThreshold){
						$responseObject['status']['statusType'] = "SUCCESS";
						$responseObject['status']['statusMessage'] = "threshold crossed.expired (".$curtime."-".$lastSessionTime.">".$sessionInactiveThreshold.")";
						$responseObject['expired'] = "true";
					}
					else {
						$responseObject['status']['statusType'] = "SUCCESS";
						$responseObject['status']['statusMessage'] = "session is active(".$curtime."-".$lastSessionTime.">".$sessionInactiveThreshold.")";
						$responseObject['expired'] = "false";
						$responceObject['login']=$session['login'];
						$responseObject['lastSearchUrl']=$session['lastSearchUrl'];
					}
				}
			}
		}
		else {
			$responseObject['status']['statusType'] = "SUCCESS";
			$responseObject['status']['statusMessage'] = "Session check disabled. Session never expires";
			$responseObject['expired'] = "false";
			$responseObject['login']=$session['login'];
			$responseObject['lastSearchUrl']=$session['lastSearchUrl'];
		}
	
		return $responseObject;
	}
}

?>
