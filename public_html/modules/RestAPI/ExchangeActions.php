<?php
include_once("$xcart_dir/include/class/oms/OrderExchangeManager.php");

class ExchangeActions{
	
    function handleRequest($request){
    	global $weblog, $sql_tbl;
       		$requestData = $request->getRequestVars();
        	$action = $requestData['data']['action'];
 			switch($action) {
				case 'createnewreturn':
					return $this->createNewReturnForExchange($requestData['data']);
				default:
					$responseObject = array();
		            $responseObject['status'] = array();
		            $responseObject['status']['statusCode'] = 600;
		            $responseObject['status']['statusMessage'] = "No action defined for $action";
		            $responseObject['status']['statusType'] = "SUCCESS";
	            	return $responseObject;
		}
        return false;
    }
    
    private function createNewReturnForExchange($requestData){
    	$manager = new OrderExchangeManager();
    	global $weblog;
    	$responseObject = array(); 
    	$responseObject['status']['statusCode'] = 900;
    	$responseObject['status']['statusType'] = "SUCCESS";
    	
    	$weblog->info(print_r($requestData, true));
    	$exchangeid = $requestData['exchangeid'];
    	$login = $requestData['login'];
    	$courierCode = $requestData['courierCode'];
    	$trackingNumber = $requestData['trackingNumber'];

    	$response = $manager->createReturnidForExchanges($exchangeid, $login, $courierCode, $trackingNumber);
    	
    	if(!$response["status"]){
    		$responseObject['status']['statusCode'] = 901;
    		$responseObject['status']['statusType'] = "FAILURE";
    		$responseObject['returnid'] = $response['returnid'];
    	} else {
    		$responseObject['returnid'] = $response['returnid'];
    		$responseObject['return_row'] = $response['return_row'];
    	}
    	$responseObject['status']['statusMessage'] = $response['message'];
    	
    	return $responseObject;
    }
}
?>
