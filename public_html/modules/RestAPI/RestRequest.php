<?php

require_once($xcart_dir."/Profiler/Profiler.php");

class RestRequest
{
        protected static $latency = array();
	protected $url;
	protected $verb;
	protected $requestBody;
	protected $requestLength;
	protected $username;
	protected $password;
	protected $acceptType;
	protected $responseBody;
	protected $responseInfo;
	protected $httpHeaders;
	protected $timeOut;

	public function __construct ($url = null, $verb = 'GET', $requestBody = null, $timeOut = 60)
	{
		$this->url				= $url;
		$this->verb				= $verb;
		$this->requestBody		= $requestBody;
		$this->requestLength		= 0;
		$this->username			= null;
		$this->password			= null;
		$this->acceptType		= 'application/json';
		$this->responseBody		= null;
		$this->responseInfo		= null;
		$this->timeOut 			= $timeOut;

		if ($this->requestBody !== null)
		{
			$this->buildPostBody();
		}
		
	}

	public function setHttpHeaders($headerArray)
	{
		$this->httpHeaders = $headerArray;
	}
	
	public function setRequestMethod($verb='GET')
	{
		$this->verb	= $verb;
	}
	
	public function flush ()
	{
		$this->requestBody		= null;
		$this->requestLength	= 0;
		$this->verb				= 'GET';
		$this->responseBody		= null;
		$this->responseInfo		= null;
	}

	public function getTimeOut()
	{
		return $this->timeOut;
	}
	
	public function setTimeOut($timeOut=60)
	{
		return $this->timeOut = $timeOut;
	}


	public function getResponseBody()
	{
		return $this->responseBody;
	}
	
	public function getResponseInfo()
	{
		return $this->responseInfo;
	}
	
	public function execute ()
	{
                $profiler_handle = Profiler::startTiming("WMS");
                $start_time = microtime(true);
		$ch = curl_init();
		$this->setAuth($ch);

		try
		{
			switch (strtoupper($this->verb))
			{
				case 'GET':
					$this->executeGet($ch);
					break;
				case 'POST':
					$this->executePost($ch);
					break;
				case 'PUT':
					$this->executePut($ch);
					break;
				case 'DELETE':
					$this->executeDelete($ch);
					break;
				default:
					throw new InvalidArgumentException('Current verb (' . $this->verb . ') is an invalid REST verb.');
			}
                        self::trackLatency($start_time);
                        Profiler::endTiming($profiler_handle);
		}
		catch (InvalidArgumentException $e)
		{
                        self::trackLatency($start_time);
                        Profiler::endTiming($profiler_handle);
			curl_close($ch);
			throw $e;
		}
		catch (Exception $e)
		{
                        self::trackLatency($start_time);
                        Profiler::endTiming($profiler_handle);
			curl_close($ch);
			throw $e;
		}
                
	}
        
	public function buildPostBody ($data = null)
	{
		$data = ($data !== null) ? $data : $this->requestBody;
		
/*		if (!is_array($data))
		{
			$weblog->debug("throwing new invalid argument exception ". $data);
			
			throw new InvalidArgumentException('Invalid data input for postBody.  Array expected');
		}
*/
		//$data = http_build_query($data, '', '&');
		$this->requestBody = $data;
	}

	protected function executeGet ($ch)
	{		
		$this->doExecute($ch);
	}

	protected function executePost ($ch)
	{
		if (!is_string($this->requestBody))
		{
			$this->buildPostBody();
		}

		
		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->requestBody);
		curl_setopt($ch, CURLOPT_POST, 1);

		$this->doExecute($ch);
	}

	protected function executePut ($ch)
	{
		if (!is_string($this->requestBody))
		{
			$this->buildPostBody();
		}

		$this->requestLength = strlen($this->requestBody);

		$fh = fopen('php://memory', 'rw');
		fwrite($fh, $this->requestBody);
		rewind($fh);

		curl_setopt($ch, CURLOPT_INFILE, $fh);
		curl_setopt($ch, CURLOPT_INFILESIZE, $this->requestLength);
		curl_setopt($ch, CURLOPT_PUT, true);

		$this->doExecute($ch);

		fclose($fh);
	}

	protected function executeDelete ($ch)
	{
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
		$this->doExecute($ch);
	}

	protected function doExecute (&$curlHandle)
	{
		global $weblog;
		$weblog->debug("In do execute");
		$this->setCurlOpts($curlHandle);
		$this->responseBody = curl_exec($curlHandle);
		$this->responseInfo	= curl_getinfo($curlHandle);
		$weblog->debug("In do execute done with response " . $this->responseBody);
		curl_close($curlHandle);
	}

	protected function setCurlOpts (&$curlHandle)
	{
		curl_setopt($curlHandle, CURLOPT_TIMEOUT, $this->timeOut);
		curl_setopt($curlHandle, CURLOPT_URL, $this->url);
		curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curlHandle, CURLOPT_HTTPHEADER, $this->httpHeaders);
		//array ('Accept: ' . $this->acceptType)
	}

	protected function setAuth (&$curlHandle)
	{
		if ($this->username !== null && $this->password !== null)
		{
			curl_setopt($curlHandle, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
			curl_setopt($curlHandle, CURLOPT_USERPWD, $this->username . ':' . $this->password);
		}
	}

        public static function trackLatency($start_time)
        {
            array_push(self::$latency, microtime(true) - $start_time);
        }


        public static function getLatencies()
        {
            return self::$latency;
        }
}

?>
