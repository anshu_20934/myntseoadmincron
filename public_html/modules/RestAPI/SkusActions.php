<?php
include_once("$xcart_dir/include/class/class.feature_gate.keyvaluepair.php");
include_once("$xcart_dir/include/func/func_sku.php");
include_once("$xcart_dir/modules/apiclient/SkuApiClient.php");
include_once("$xcart_dir/modules/apiclient/AtpApiClient.php");
//include_once("$xcart_dir/modules/apiclient/RecommendationsApiClient.php");
include_once("$xcart_dir/modules/RestAPI/DiscountActions.php");

class SkusActions
{
    // GET REQUEST FOR THIS

    function handleRequest($request)
    {
    	global $weblog, $sql_tbl;
       		$requestData = $request->getRequestVars();
        	$action = strtolower($requestData['data']['action']);
            $weblog->debug("SkusActions:debug:begin");
            $weblog->info("SkusActions: perform action $action for Sku");
			switch($action) {
                case 'notify':
				case 'enable':
				case 'disable':
				case 'reindex':
					return $this->enableDisableSku($requestData['data'], $action);
				case 'filteractiveskus':
					return $this->filterActiveSkus($requestData['data']);
				default:
					$responseObject = array();
		            $responseObject['status'] = array();
		            $responseObject['status']['statusCode'] = 600;
		            $responseObject['status']['statusMessage'] = "No action defined for $action";
		            $responseObject['status']['statusType'] = "SUCCESS";
	            	return $responseObject;
		}
		
        return false;
    }
	
	private function filterActiveSkus($request_data) {
    	$responseObject = array();
    	$skus = $request_data['skus']['sku'];
        $sku_ids = array();
        if(count($skus) == 1){
	        foreach($skus as $object) {
	        	$sku_ids[] = $object;
	        }
        }else{
	        foreach($skus as $object) {
	        	$sku_ids[] = $object['id'];
	        }	
        }
        $sku_ids = implode(",", $sku_ids);
        
		if ($sku_ids && $sku_ids != "") {
			$activeSkusSql = "Select sku_id from mk_styles_options_skus_mapping where sku_id in ($sku_ids)";
			$results = func_query($activeSkusSql);
			$activeSkus = "";
			if($results) {
				foreach($results as $row) {
					$activeSkus .= $activeSkus==""?"":",";
					$activeSkus .= $row['sku_id'];
				}
			}
			$responseObject['status'] = array();
            $responseObject['status']['statusCode'] = 600;
            $responseObject['status']['statusMessage'] = "Active SkuIds filtered";
            $responseObject['status']['statusType'] = "SUCCESS";
			$responseObject['data'] = $activeSkus;
		} else {
			$responseObject['status'] = array();
        	$responseObject['status']['statusCode'] = 700;
        	$responseObject['status']['statusMessage'] = "No SkuIds passed";
        	$responseObject['status']['statusType'] = "FAILURE";
		}
		return $responseObject; 
    }
    
    function enableDisableSku($request_data, $action) 
    {
        global $weblog, $sql_tbl;
        $weblog->debug("SkusActions:enableDisableSku:begin");
        $invModule = FeatureGateKeyValuePairs::getFeatureGateValueForKey('portal.inventory.module', 'wms');
        $skus = $request_data['skus']['sku'];
        $sku_ids = array();
        if(isset($skus['id'])){
	        $skus = array($skus);
        }
        
        foreach($skus as $object) {
        	$sku_ids[] = $object['id'];
        }	
        $mode = strtolower($request_data['mode']);
        $user = $request_data['userId'];
	    // can be called as bulk operation...
    	$responseObject = array();
        if ($sku_ids && count($sku_ids) > 0) {
            /* $statusMessage = "";
            if ($action == "enable") {
                if ($mode == "manual") {
                    $extra_info['reason'] = getReasonByCode("MANE");
                } else {
                    $extra_info['reason'] = getReasonByCode("ISAE");
                    $user = "AUTOSYSTEM";
                }
                $extra_info["reasonid"] = $extra_info["reason"]['id'];
                $extra_info['user'] = $user;
                $extra_info['value'] = 1;
                foreach ($sku_ids as $sku_id) {
                    //$available_count = $skuId2AvailableCountMap[$sku_id];
                    $weblog->debug("SkusActions:enableDisableSku:updateSKUPropertyWMS:begin");
                    updateSKUPropertyWMS(trim($sku_id), 'enabled', '1', $user, $extra_info, false);
                    $weblog->debug("SkusActions:enableDisableSku:updateSKUPropertyWMS:done:".$sku_id);
                }
                RecommendationsApiClient::enableSkusInRecommendations($sku_ids, $user);
                $statusMessage = "Enabled sku successfully";
                DiscountActions::OOSActionForDiscounts($sku_ids,"enable");
                
            } else if ($action == "disable") {
                if ($mode == "manual") {
                    $extra_info['reason'] = getReasonByCode("MAND");
                } else {
                    $extra_info['reason'] = getReasonByCode("OSAD");
                    $user = "AUTOSYSTEM";
                }
                $extra_info["reasonid"] = $extra_info["reason"]['id'];
                $extra_info['value'] = 0;
                $extra_info['user'] = $user;
                foreach ($sku_ids as $sku_id) {
                    updateSKUPropertyWMS(trim($sku_id), 'enabled', '0', $user, $extra_info, false);
                }
                RecommendationsApiClient::disableSkusInRecommendations($sku_ids,$user);
                $statusMessage = "Disabled sku successfully";
                DiscountActions::OOSActionForDiscounts($sku_ids,"disable");
                
            } else if ($action == "reindex") {
                $statusMessage = "Reindexed sku successfully";
                if(strtolower($invModule) == 'atp') {
                	disableEnableSkusInBulkAtp($sku_ids);
                }
            } else {
                $responseObject['status'] = array();
                $responseObject['status']['statusCode'] = 600;
                $responseObject['status']['statusMessage'] = "No matching operation found";
                $responseObject['status']['statusType'] = "FAILURE";
                return $responseObject;
            } */
        	
        	$disabledSkus = array(); $enabledSkus = array();
        	
        	foreach(array_chunk($sku_ids, 100) as $batch_skuids) {
        		$response = disableEnableSkusInBulkAtp($batch_skuids);
        		if(!empty($response[0])) {
        			$disabledSkus = array_merge($disabledSkus, $response[0]);
        		}
        		if(!empty($response[1]))
        			$enabledSkus = array_merge($enabledSkus, $response[1]);
        	}
        	
        	if(!empty($enabledSkus)) {
                //availswitchoff
        		//RecommendationsApiClient::enableSkusInRecommendations($enabledSkus, $user);
        		DiscountActions::OOSActionForDiscounts($enabledSkus,"enable");
        	}
        	if(!empty($disabledSkus)) {
                //availswitchoff
        		//RecommendationsApiClient::disableSkusInRecommendations($disabledSkus,$user);
        		DiscountActions::OOSActionForDiscounts($disabledSkus,"disable");
        	}
        	
        	//reIndexSKUsInBulk($sku_ids);

            if ($action == "notify") {
                foreach ($skus as $object) {
                    $sku = array();
                    $sku['id'] = $object['id'];
                    $sku['count'] = $object['count'];
                    $statusMessage = notifySkuInStock($sku);
                }
            }
                    
            $responseObject['status'] = array();
            $responseObject['status']['statusCode'] = 600;
            $responseObject['status']['statusMessage'] = $statusMessage;
            $responseObject['status']['statusType'] = "SUCCESS";
        } else {
            $responseObject['status'] = array();
            $responseObject['status']['statusCode'] = 700;
            $responseObject['status']['statusMessage'] = "No SkuIds passed";
            $responseObject['status']['statusType'] = "FAILURE";
        }
        return $responseObject;
    }
    
}

?>
