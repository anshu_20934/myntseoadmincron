<?php


/*
 * @package Permission
 * @author mitesh.gupta@myntra.com
 */

use user\User;

use user\UserAuthenticationManager;

class PermissionUtils {
	
	
	public static function authenticateUser($userid, $passwd){
		global $weblog;
		if(empty($userid)||empty($passwd)) {
			throw new InvalidArgumentException("Authentication information is not complete");
		}
		$user_data = func_query_first("SELECT role,password,password_md5 FROM mk_api_users WHERE userid='$userid' and status=1");
		$user = new User($userid,User::USER_TYPE_ADMIN,$user_data['password'],$user_data['password_md5'],null);
		$authenticator = new UserAuthenticationManager();
		
		try{
			$authenticated = $authenticator->authenticateUser($user, $passwd,User::USER_TYPE_ADMIN);
			if($authenticated && $user->getPasswordMD5()!=$user_data['password_md5']){
				db_query("update mk_api_users set password_md5='{$user->getPasswordMD5()}' where userid='$userid'");
			}
		}catch(Exception $ex){
			
		}
		$weblog->info("Authenticate User - $userid");
		if($authenticated){
			return $user_data['role'];
		}
		
		return null;
	}
	
	/**
	 * Helper function to get the permission array associated with a user
	 * @param string $login login id of the user we want to get the permission for
	 * @throws InvalidArgumentException If parameters passed are null or empty
	 * @static
	 * @access public
	 * @return ArrayObject An array containing list of permission name associated with the user.
	 */
	public static function getUserPermission($login){
		if(empty($login)) {
			throw new InvalidArgumentException("Passed parameter is null or empty ");
		}
		
		$permissionQuery = "SELECT permission_name from mk_permission_name where id in (SELECT distinct pid from mk_roles_to_permission where rid in (SELECT rid FROM mk_user_to_roles where login='$login'))";
		$permissions = func_query($permissionQuery);
		$toReturn = array();
		foreach($permissions as $perm){
			$toReturn[] = $perm['permission_name'];
		}
		
		return $toReturn;
	}
	
	/**
	 * Helper function to get the list of users associated with a role.
	 * @param string $rid Role id of the role we want to get the users list for.
	 * @throws InvalidArgumentException If parameters passed are null or empty.
	 * @static
	 * @access public
	 * @return ArrayObject An array containing list of users info associated with the role.
	 */
	
	public static function getUserFromRole($rid){
		
		if(empty($rid)) {
			throw new InvalidArgumentException("Passed parameter is null or empty ");
		}
		
		$userSelectQuery = "SELECT login, usertype, firstname, lastname from xcart_customers where login in (SELECT login from mk_user_to_roles where rid='$rid')";
		$users = func_query($userSelectQuery);
		return $users;
	}
	
	
	/**
	 * Helper function to get the permission array associated with a set of role ids.
	 * @param array $rids Role id array to get the permissions for
	 * @throws InvalidArgumentException If parameters passed are null or empty
	 * @static
	 * @access public
	 * @return ArrayObject An array containing list of permission name associated with role id's.
	 */
	
	public static function getPermissionsFromRids($rids){
		if(empty($rids)&&!is_array($rids)) {
			throw new InvalidArgumentException("Passed parameter is null or empty or is not an array ");
		}	
		
		$comma_separated = implode(",", $rids);
		
		$permissionQuery = "SELECT id,permission_name from mk_permission_name where id in (SELECT distinct pid from mk_roles_to_permission where rid in ($comma_separated))";
		$permissions = func_query($permissionQuery);
		$toReturn = array();
		foreach($permissions as $perm){
			$toReturn[$perm['id']] = $perm['permission_name'];
		}
		
		return $toReturn;		
	}
	
	/**
	 * Helper function to get all roles name present in the database as an array indexed with id. * 
	 * 
	 * @return ArrayObject Containing the list of role name.
	 * @access public
	 * @static
	 */
	
	public static function getAllRoleList(){
		$wmsRoleQuery = "select id,role_name from mk_user_roles_name";
		$wmsRoleResult = func_query($wmsRoleQuery);
		$roleresults=array();
		foreach($wmsRoleResult as $role){
				$roleresults[$role['id']]=$role['role_name'];
		}
		return $roleresults;
	}	
	
	/**
	 * Helper function to get all permission name present in the database as an array indexed with id. * 
	 * 
	 * @return ArrayObject Containing the list of permission name.
	 * @access public
	 * @static
	 */
	
	public static function getAllPermissionList(){
		$wmsPermissionQuery = "select id,permission_name from mk_permission_name";
		$wmsPermissionResult = func_query($wmsPermissionQuery);
		$permissionResults=array();
		foreach($wmsPermissionResult as $permission){
				$permissionResults[$permission['id']]=$permission['permission_name'];
		}
		return $permissionResults;
	}
	
	/**
	 * Helper function to get list of roles id's associated with a user
	 * @param string $login login id of the user we want to get the roles for
	 * @throws InvalidArgumentException If parameters passed are null or empty
	 * @return ArrayObject An array indexed with the Role ID's associated with the user with value true.
	 * @access public
	 * @static
	 */
	
	public static function getUserRoleIds($login){
		
		if(empty($login)) {
			throw new InvalidArgumentException("Passed parameter is null or empty ");
		}
		
		$userRoleIdQuery = "SELECT rid FROM mk_user_to_roles where login='$login'";
		$userRoleIdResult = func_query($userRoleIdQuery);
		$roleIdresults=array();
		foreach($userRoleIdResult as $id){
				$roleIdresults[$id['rid']]=true;
		}
		
		return $roleIdresults;
	}
	
	/**
	 * Helper function to get list of role names associated with a user
	 * @param string $login login id of the user we want to get the roles for
	 * @throws InvalidArgumentException If parameters passed are null or empty
	 * @return ArrayObject An array indexed with the key as Role ID's and value as Role Name associated with the user.
	 * @access public
	 * @static
	 */
	
	public static function getUserRoleName($login){
		
		if(empty($login)) {
			throw new InvalidArgumentException("Passed parameter is null or empty ");
		}
		
		$userRoleNameQuery = "SELECT id,role_name from mk_user_roles_name where id in (SELECT rid FROM mk_user_to_roles where login='$login')";
		$userRoleNameResult = func_query($userRoleNameQuery);
		$roleNameresults=array();
		foreach($userRoleNameResult as $role){
				$roleNameresults[$role['id']]=$role['role_name'];
		}
		
		return $roleNameresults;
	}
	
	/**
	 * Helper function to delete the roles associated with a user
	 * @param string $login login id of the user we want to delete the roles for
	 * @param int $id (Optional) Role ID If present will delete the particular role ID associated with the user.
	 * @throws InvalidArgumentException If parameters passed are null or empty 
	 * @return bool returns true on success or false on error. 
	 * @access public
	 * @static
	 */
	
	public static function deleteUserRole($login,$rid=null){
		
		if(empty($login)) {
			throw new InvalidArgumentException("Passed parameter is null or empty ");
		}
		
		if(empty($rid)){
			$delroles="DELETE from mk_user_to_roles where login='$login' ";
		}			
		else {
			$delroles="DELETE from mk_user_to_roles where login='$login' AND rid=$rid";		
		}

		$status = db_query($delroles);
		
		return $status;
	}
	
	/**
	 * Helper function to remove the permissions assigned to a role 
	 * @param int $rid Role ID for which we want to remove permission for.
	 * @param int $pid (Optional) Permission ID If present will delete the particular permission associated with the role.
	 * @throws InvalidArgumentException If parameters passed are null or empty 
	 * @return bool returns true on success or false on error. 
	 * @access public
	 * @static
	 */
	
	public static function deleteRolePermission($rid,$pid=null){
		
		if(empty($rid)) {
			throw new InvalidArgumentException("Passed parameter is null or empty ");
		}
		
		if(empty($pid)){
			$delroles="DELETE from mk_roles_to_permission where rid='$rid' ";
		}			
		else {
			$delroles="DELETE from mk_roles_to_permission where rid='$rid' AND pid=$pid";		
		}

		$status = db_query($delroles);
		
		return $status;
	}
	
	/**
	 * Helper function to assign a permission to a role.
	 * @param int $rid Role ID for which we are adding the permission.
	 * @param int $pid Permission id for the permissions to add.
	 * @throws InvalidArgumentException If parameters passed are null or empty. 
	 * @return bool returns true on success or false on error. 
	 * @access public
	 * @static
	 */
	
	public static function addRolePermission($rid,$pid) {
		
		if(empty($pid)||empty($rid)) {
			throw new InvalidArgumentException("Passed parameters are null or empty ");
		}
		
		$wmsInsertRolePermissionQuery="insert into mk_roles_to_permission (rid,pid) values ('$rid','$pid')";
		
		return db_query($wmsInsertRolePermissionQuery);		
	}
	
	/**
	 * Helper function to add a role ID associated with a user.
	 * @param string $login login id of the user we want to add the roles for.
	 * @param int $rid Role ID associated with the user.
	 * @throws InvalidArgumentException If parameters passed are null or empty. 
	 * @return bool returns true on success or false on error. 
	 * @access public
	 * @static
	 */
	
	public static function addUserRole($login,$rid) {
		
		if(empty($login)||empty($rid)) {
			throw new InvalidArgumentException("Passed parameters are null or empty ");
		}
		
		$wmsRoleInsertQuery="insert into mk_user_to_roles (login,rid) values ('$login','$rid')";
		
		return db_query($wmsRoleInsertQuery);		
	}
	
	
	/**
	 * Helper function add a new role in the DB.
	 * @param string $rollName Name for the role we want to add.
	 * @throws InvalidArgumentException If parameters passed are null or empty. 
	 * @return bool returns true on success or false on error. 
	 * @access public
	 * @static
	 */
	
	public static function addRole($rollName){
		if(empty($rollName)) {
			throw new InvalidArgumentException("Passed parameter is null or empty ");
		}
		
		$wmsRoleInsertQuery="insert into mk_user_roles_name (role_name) values ('$rollName')";
		
		return db_query($wmsRoleInsertQuery);
		
	}
	
	/**
	 * Helper function to get the role name corresponding to the provided role id from the Database.
	 * @param int $rid Role ID for the role we want to retreive.
	 * @throws InvalidArgumentException If parameters passed are null or empty. 
	 * @return string returns role name corresponding to the role id passed. 
	 * @access public
	 * @static
	 */
	
	public static function getRole($rid){
		if(empty($rid)) {
			throw new InvalidArgumentException("Passed parameter is null or empty ");
		}
		
		$userRoleNameQuery = "SELECT role_name from mk_user_roles_name where id='$rid'";
		$roleName =  func_query_first($userRoleNameQuery);
		return $roleName['role_name'];
	}
	
	/**
	 * Helper function delete a role from the DB.
	 * @param int $rid role id of the role we want to delete.
	 * @throws InvalidArgumentException If parameters passed are null or empty. 
	 * @return bool returns true on success or false on error. 
	 * @access public
	 * @static
	 */
	
	public static function deleteRole($rid){
		//Remember to Delete this rid from mk_user_to_roles table also and mk_roles_to_permission.
		if(empty($rid)) {
			throw new InvalidArgumentException("Passed parameter is null or empty ");
		}
		
		$delrole="DELETE from mk_user_roles_name where id='$rid' ";
		$status = db_query($delrole);
		
		$delUserRole="DELETE from mk_user_to_roles where rid='$rid'";
		$status = $status&db_query($delUserRole);
		
		$status = $status & PermissionUtils::deleteRolePermission($rid);
		
		return $status;
		
	}	
	
	/**
	 * Helper function delete a Permission from the DB.
	 * @param int $pid Permission id of the permission to delete.
	 * @throws InvalidArgumentException If parameters passed are null or empty. 
	 * @return bool returns true on success or false on error. 
	 * @access public
	 * @static
	 */
	
	public static function deletePermission($pid){
		
		if(empty($pid)) {
			throw new InvalidArgumentException("Passed parameter is null or empty ");
		}
		
		$delPermission="DELETE from mk_permission_name where id='$pid' ";
		$status = db_query($delPermission);
		
		$delPermissionRole="DELETE from mk_roles_to_permission where pid='$pid'";
		$status = $status&db_query($delPermissionRole);
		
		return $status;
		
	}
	
	/**
	 * Helper function add a new permission in the DB.
	 * @param string $permissionName Name for the permission we want to add.
	 * @throws InvalidArgumentException If parameters passed are null or empty. 
	 * @return bool returns true on success or false on error. 
	 * @access public
	 * @static
	 */
	
	public static function addPermission($permissionName) {
		if(empty($permissionName)) {
			throw new InvalidArgumentException("Passed parameter is null or empty ");
		}
		
		$wmsPermissionInsertQuery="insert into mk_permission_name (permission_name) values ('$permissionName')";
		
		return db_query($wmsPermissionInsertQuery);
		
	}
	
}


?>