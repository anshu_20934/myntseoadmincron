<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Raghav
 * Date: 15/9/11
 * Time: 5:48 PM
 * To change this template use File | Settings | File Templates.
 */




class TaskEngineActions
{
    // GET REQUEST FOR THIS
    function handleRequest($request)
    {
        global $weblog;
		$requestData = $request->getRequestVars();
        $action = strtolower($requestData['action']);
        $weblog->info("Inside task engine - Execute Url - $action");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $action);
        curl_exec($ch);
        curl_close($ch);
    }
}

?>
