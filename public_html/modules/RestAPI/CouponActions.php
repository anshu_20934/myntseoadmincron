<?php
use enums\cart\CartContext;

include_once ("$xcart_dir/include/class/mcart/class.MCartFactory.php");
include_once ("$xcart_dir/include/class/mcart/class.MCart.php");

class CouponActions {
    
	function handleRequest($request)    {
            global $portalapilog;
            $requestData = $request->getRequestVars();
            $action = strtolower($requestData['data']['action']);
            $portalapilog->info("OrderActions: perform action $action");
            $context = strtolower($requestData['data']['context']);
            if(empty($context))
            	$context = CartContext::DefaultContext;
            switch($action) {
        	case 'getcoupons':
                        $mcartFactory= new \MCartFactory();

                        $myCart = $mcartFactory->getCurrentUserCart(true, $context);
                       	return $this->sortCoupons($myCart);

            case 'generate':
                        return $this->generateCoupon($requestData);
        	default:
        		$responseObject = array();
        		$responseObject['status'] = array();
        		$responseObject['status']['statusCode'] = 600;
        		$responseObject['status']['statusMessage'] = "No action supported";
        		$responseObject['status']['statusType'] = "SUCCESS";
        		return $responseObject;
            }	
            return false;        
	}
        
        
            /**
     * Get coupons in sorted order.
     */
    public function sortCoupons($myCart) {

        $login = $myCart->getLogin();
        
        $adapter = CouponAdapter::getInstance();
        $calculator = CouponDiscountCalculator::getInstance();
        
        // 1. Get user specific coupons.
        $userSpecificCoupons = $adapter->getCouponsForCartPage($login, $myCart);
        
        // 2. Get general coupons which are allowed to be auto-applied.
        $generalCoupons = $adapter->getGeneralCoupons($myCart);
        
        $couponsToBeSorted = array_merge($userSpecificCoupons, $generalCoupons);
        
        // 3. Call CouponDiscountCalculator.applyCouponOnCart() on each of these coupons.
        
        $couponsInfo = array();
        
        if(count($couponsToBeSorted) == 0) {
            return $couponsInfo;
        }

        foreach($couponsToBeSorted as $coupon) {
            
            $couponInfo = array();
            
            $tempCart = clone $myCart;
            $tempCart->clearVat();
            
            $discountCouponObject = $adapter->fetchCoupon($coupon['coupon']);

            //$couponInfo["expiryDate"] = date("Y:d:M", $discountCouponObject->getEndDate());
            $couponInfo["coupon"] = $coupon["coupon"];
            $couponInfo["expiryDate"] = $discountCouponObject->getEndDate()*1000;
            $couponInfo["conditionMin"] = $coupon["minimum"];
            $couponInfo["conditionMax"] = $coupon["maxAmount"];
            $couponInfo["couponAmount"] = $coupon["MRPAmount"];
            $couponInfo["couponPercentage"] = $coupon["MRPpercentage"];
            
            try {
                $calculator->applyCouponOnCart($discountCouponObject, $tempCart);
                
                $couponInfo["isApplicable"] = true;
                $couponInfo["benefit"] = $tempCart->getDiscount();
            }
            catch(Exception $e) {
                // This means coupon is not applicable on this cart.
                $couponInfo["isApplicable"] = false;
                        
                $applicableSubtotal = $calculator->calculateApplicableSubtotal($tempCart,
                                               $tempCart->getCouponApplicableCartItems(),
                                               $discountCouponObject->getMaxUsagePerCart());

                $couponInfo["minMore"] = $discountCouponObject->getMinimum() - $applicableSubtotal;                
            }
            
            $couponInfo["applicableProducts"] = $tempCart->getCouponApplicableCartItems();
            $couponInfo["notApplicableProducts"] = $tempCart->getCouponNotApplicableCartItems();
            $couponInfo["couponApplicabilityMessage"] = $tempCart->getCouponApplicabilityMessage();
            
            array_push($couponsInfo, $couponInfo);
        }
        
        // 4. Sort the coupons based on benefits.
        \usort($couponsInfo, array($this, "sortLogic"));
        
        return $couponsInfo;
    }
       

    private function sortLogic($a, $b){
        if($a["benefit"] != $b["benefit"]) {
            return $b["benefit"] - $a["benefit"];            
        }
        if($a["benefit"] == 0) {
            if($a["minMore"] != $b["minMore"]) {
                return $a["minMore"] - $b["minMore"];
            }
        }
        return $a["expiryDate"] - $b["expiryDate"];
    }

    private function generateCoupon($requestData) {
        $responseObject = array();
        $responseObject['status'] = array();

        $couponType = $requestData['data']['type'];
        $ruleExecutor = \RuleExecutor::getInstance();
        switch($couponType) {
            case 'signup':
                $ruleExecutor->runSynchronous("customer", $requestData['data']['login'], "1");
                $responseObject['status']['statusCode'] = 200;
                $responseObject['status']['statusMessage'] = "Coupon(s) generated successfully";
                $responseObject['status']['statusType'] = "SUCCESS";
                break;
            case 'fb_signup':
                $ruleExecutor->runSynchronous("customer", $requestData['data']['login'], "5");
                $responseObject['status']['statusCode'] = 200;
                $responseObject['status']['statusMessage'] = "Coupon(s) generated successfully";
                $responseObject['status']['statusType'] = "SUCCESS";
                break;
            default:
                $responseObject['status']['statusCode'] = 600;
                $responseObject['status']['statusMessage'] = "no such coupon type available";
                $responseObject['status']['statusType'] = "FAILURE";
        }
        return $responseObject;
    }
}
?>
