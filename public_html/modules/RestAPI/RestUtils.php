<?php
require_once "$xcart_dir/modules/RestAPI/Request.php";
require_once "$xcart_dir/utils/http/SimpleParser.php";

class RestUtils
{
	
	public static function createRequestObject($uniqueId)
	{
		global $portalapilog;

		// get our verb
		$request_method = strtolower($_SERVER['REQUEST_METHOD']);

		$requestObject = new Request();
		
		// we'll store our data here
		$request_data = array();
		
		switch ($request_method)
		{
			// gets are easy...
			case 'get':
				$request_data = $_GET;
				$portalapilog->info("ApiRequestHandler: Get data (ID: $uniqueId) - ".print_r($_GET,true));
				break;
			// so are posts
			case 'post':
				$post_data_string = file_get_contents('php://input');
				$portalapilog->info("ApiRequestHandler: Post data (ID: $uniqueId) - $post_data_string");
				if($requestObject->getContentType() == 'json') {
					$request_data = json_decode($post_data_string, true);
				} else {
					$request_data = SimpleParser::xmlToArray($post_data_string);
				}
				break;
			// here's the tricky bit...
			case 'put':
				// basically, we read a string from PHP's special input location,
				// and then parse it out into an array via parse_str... per the PHP docs:
				// Parses str  as if it were the query string passed via a URL and sets
				// variables in the current scope.
				$put_data_string = file_get_contents('php://input');
				$portalapilog->info("ApiRequestHandler: Put data (ID: $uniqueId) - $put_data_string");
				if($requestObject->getContentType() == 'json') {
					$request_data = json_decode($put_data_string, true);
				} else {
					$request_data = SimpleParser::xmlToArray($put_data_string);
				}
				//$request_data['handler'] = $request['portalApiRequest']['handler'];
				//$request_data['data'] = $request['portalApiRequest']['data'];
				break;
		}

		// store the method
		$requestObject->setMethod($request_method);

		// set the raw data, so we can access it if needed (there may be
		// other pieces to your requests)
		$requestObject->setRequestVars($request_data);

		return $requestObject;
	}
	
	public static function sendResponse($status = 200, $body = '', $content_type = 'text/xml')
	{
		if(trim($body) != '')
		{
			$status_header = 'HTTP/1.1 ' . $status . ' ' . RestUtils::getStatusCodeMessage($status);
			header($status_header);
			header("Content-type: $content_type");
			echo $body;
			exit;
		}
		// we need to create the body if none is passed
		else
		{
			// create some body messages
			$message = '';
			// this is purely optional, but makes the pages a little nicer to read
			// for your users.  Since you won't likely send a lot of different status codes,
			// this also shouldn't be too ponderous to maintain
			switch($status)
			{
				case 401:
					$message = 'You must be authorized to view this page.';
					break;
				case 404:
					$message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
					break;
				case 500:
					$message = 'The server encountered an error processing your request.';
					break;
				case 501:
					$message = 'The requested method is not implemented.';
					break;
			}

			// servers don't always have a signature turned on (this is an apache directive "ServerSignature On")
			$signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];

			// this should be templatized in a real-world solution
			$body = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
						<html>
							<head>
								<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
								<title>' . $status . ' ' . RestUtils::getStatusCodeMessage($status) . '</title>
							</head>
							<body>
								<h1>' . RestUtils::getStatusCodeMessage($status) . '</h1>
								<p>' . $message . '</p>
								<hr />
								<address>' . $signature . '</address>
							</body>
						</html>';
			
			$status_header = 'HTTP/1.1 ' . $status . ' ' . RestUtils::getStatusCodeMessage($status);
			header($status_header);
			header("Content-type: text/html");
			echo $body;
			exit;
		}
	}

	public static function getStatusCodeMessage($status)
	{
		// these could be stored in a .ini file and loaded
		// via parse_ini_file()... however, this will suffice
		// for an example
		$codes = Array(
		    100 => 'Continue',
		    101 => 'Switching Protocols',
		    200 => 'OK',
		    201 => 'Created',
		    202 => 'Accepted',
		    203 => 'Non-Authoritative Information',
		    204 => 'No Content',
		    205 => 'Reset Content',
		    206 => 'Partial Content',
		    300 => 'Multiple Choices',
		    301 => 'Moved Permanently',
		    302 => 'Found',
		    303 => 'See Other',
		    304 => 'Not Modified',
		    305 => 'Use Proxy',
		    306 => '(Unused)',
		    307 => 'Temporary Redirect',
		    400 => 'Bad Request',
		    401 => 'Unauthorized',
		    402 => 'Payment Required',
		    403 => 'Forbidden',
		    404 => 'Not Found',
		    405 => 'Method Not Allowed',
		    406 => 'Not Acceptable',
		    407 => 'Proxy Authentication Required',
		    408 => 'Request Timeout',
		    409 => 'Conflict',
		    410 => 'Gone',
		    411 => 'Length Required',
		    412 => 'Precondition Failed',
		    413 => 'Request Entity Too Large',
		    414 => 'Request-URI Too Long',
		    415 => 'Unsupported Media Type',
		    416 => 'Requested Range Not Satisfiable',
		    417 => 'Expectation Failed',
		    500 => 'Internal Server Error',
		    501 => 'Not Implemented',
		    502 => 'Bad Gateway',
		    503 => 'Service Unavailable',
		    504 => 'Gateway Timeout',
		    505 => 'HTTP Version Not Supported'
		);

		return (isset($codes[$status])) ? $codes[$status] : '';
	}
}

?>