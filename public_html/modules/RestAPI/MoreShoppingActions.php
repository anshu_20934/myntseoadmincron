<?php

class MoreShoppingActions {
    
	function handleRequest($request) {

            $requestData = $request->getRequestVars();
            $action = strtolower($requestData['data']['action']);
        
            switch($action) {
                case 'moreshoppingurls':
                    //return array(1,2);
                    return MoreShoppingActions::moreShoppingUrls();
            }
            return false;	       		
    }

    public function moreShoppingUrls(){
        
        global $XCART_SESSION_VARS;
        
        $lasturls  = array();
        $response = array();
        
        $lasturls[] = $XCART_SESSION_VARS["last_search_page_1"];
        $data = array();
            
        $uriparts = explode('?', $lasturls[0]);
        $uribase = "";
        if(strlen($uriparts[0]))
            $uribase = substr($uriparts[0],1);
        
        if(!empty($uribase)){
            $data["text"] = str_replace(array("/", "-"), " ", $uribase);
            $data["link"] = urlencode($lasturls[0]);
            $response[] = $data;
        }
        
        $response[] = array("text"=>"men", "link"=>urlencode("/men"));
        $response[] = array("text"=>"women", "link"=>urlencode("/women"));
        
        if(empty($uribase))
            $response[] = array("text"=>"home page", "link"=>urlencode("/"));
            
        return $response;
    }
}

?>
