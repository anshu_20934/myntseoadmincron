<?php
/**
 * This class handles different kind of attribute related read only request
 * @author Ravi
 *
 */
class AttributeActions
{
	/**
	 * This handles the request related to Attribute request like get all season name
	 * @param XML $request
	 * @return XML responseObject
	 */
	function handleRequest($request)
    {
       	$requestData = $request->getRequestVars();
        $action = trim($requestData['data']['action']);
        switch($action)
        {
        	case 'getAllAttributeValue':
        		$attributeType = trim($requestData['data']['attributeType']);
				return $this->getAllAttributeValue($attributeType);
			case 'getAttributeValueNameById':
				$attributeValueId = trim($requestData['data']['attributeValueId']);
				return $this->getAttributeValueNameById($attributeValueId);
			default:
				$responseObject = array();
	            $responseObject['status'] = array();
	            $responseObject['status']['statusCode'] = 600;
	            $responseObject['status']['statusMessage'] = "No action supported";
	            $responseObject['status']['statusType'] = "SUCCESS";
	            return $responseObject;
			}
		
        return false;
    }
	
    /**
     * Private Method gives all values correspnding to a given attribute type
     * @param String $attributeType this is the attribute name like Season, Brand etc.
     */
    private function getAllAttributeValue($attributeType)
    {
    	$responseObject = array();
    	//get the attribute type id using the attribute type
    	$attributeId = func_query_first_cell("SELECT id FROM mk_attribute_type WHERE attribute_type = '$attributeType'");
    	if($attributeId == '')
    	{
    		$responseObject['status']['statusCode'] = 600;
    		$responseObject['status']['statusMessage'] = "No attribute type found";
    		$responseObject['status']['statusType'] = "SUCCESS";
    	}
    	else
    	{
    		$attributeSQL = "SELECT id AS attributeValueId, attribute_value AS attributeValueName FROM mk_attribute_type_values WHERE attribute_type_id = '$attributeId' AND is_active ='1'";
    		$results = func_query($attributeSQL, true);
    		$attributeValueData = array();
    		$counter = 0;
    		if($results)
    		{
    			foreach($results as $row)
    			{
    				$data = array();
    				$data['attributeValue']['attributeValueId'] = $row['attributeValueId'];
    				$data['attributeValue']['attributeValueName'] = $row['attributeValueName'];
    				$attributeValueData[]=$data;
    			}
    		}
    		$responseObject['status'] = array();
    		$responseObject['status']['statusCode'] = 600;
    		$responseObject['status']['statusMessage'] = "All Attribute Type Values";
    		$responseObject['status']['statusType'] = "SUCCESS";
    		$responseObject['data'] = $attributeValueData;
    	}
    	return $responseObject;
    }
    
    /**
     * Private Method gives the attribute value name given attribute value id
     * @param int $attributeId
     */
    private function getAttributeValueNameById($attributeValueId)
    {
    	$responseObject = array();
    	$attributeValueName = func_query_first_cell("SELECT attribute_value FROM mk_attribute_type_values WHERE id = '$attributeValueId'");
    	if($attributeValueName == '')
    	{
    		$responseObject['status']['statusCode'] = 600;
    		$responseObject['status']['statusMessage'] = "No attribute value found";
    		$responseObject['status']['statusType'] = "SUCCESS";
    	}
    	else
    	{
    		$responseObject['status'] = array();
    		$responseObject['status']['statusCode'] = 600;
    		$responseObject['status']['statusMessage'] = "Attribute Value Name";
    		$responseObject['status']['statusType'] = "SUCCESS";
    		$responseObject['data']['attributeValueName'] = $attributeValueName;
    	}
    	return $responseObject;
    }
}