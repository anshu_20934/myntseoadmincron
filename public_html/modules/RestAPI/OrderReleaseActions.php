<?php
/*
 * Created on Oct 21, 2012
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
*/
 
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/include/func/func.core.php");
include_once("$xcart_dir/include/func/func.db.php");
include_once("$xcart_dir/include/func/func.mk_old_returns_tracking.php");

class OrderReleaseActions {
	
    function handleRequest($request)    {
		$requestParams = $request->getRequestVars();    
		$action = $requestParams['data']['action'];
    	switch($action) {
    		case 'updateReleaseStatus':
    			return $this->updateReleaseStatus($requestParams['data'], $requestParams['userId']);
    			break;
    		case 'preQueueRTO':
    			return $this->preQueueRTO($requestParams['data'], $requestParams['userId']);
    			break;
    		case 'addOrdersToBatch':
    			return $this->addOrderToRtsBatch($requestData['data']);
    			break;
    		default:
				$responseObject = array();
	            $responseObject['status'] = array();
	            $responseObject['status']['statusCode'] = 600;
	            $responseObject['status']['statusMessage'] = "No action supported";
	            $responseObject['status']['statusType'] = "SUCCESS";
	            return $responseObject;
    	}
    	
    	return false;
    }
    
    private function updateReleaseStatus($data, $userId) {
    	$releases = $data['orderRelease'];
    	$responseObject = array();
    	if($releases && !empty($releases)) {
    		if(isset($releases['id']))
    			$releases = array($releases);
			foreach($releases as $row)	{
				if($row['id'] && !empty($row['id'])) {
					$order_release_row = func_query_first("Select orderid, status from xcart_orders where orderid = ". $row['id'], true);
					if($order_release_row) {
						if($row['status'] == 'DL' || $row['status'] == 'L') {
							if($order_release_row['status'] != 'SH') {
								$responseObject['status']['statusCode'] = 901;
								$responseObject['status']['statusType'] = "ERROR";
								$responseObject['status']['statusMessage'] = "Order Release ".$row['id']." is not in Shipped status";
								return $responseObject;
							}	
						} else {
							$responseObject['status']['statusCode'] = 901;
							$responseObject['status']['statusType'] = "ERROR";
							$responseObject['status']['statusMessage'] = "Order Release cannot be marked ".$row['status']." from here";
							return $responseObject;
						}				
					} else {
						$responseObject['status']['statusCode'] = 901;
						$responseObject['status']['statusType'] = "ERROR";
						$responseObject['status']['statusMessage'] = "Order Release ".$row['id']." is not valid";
						return $responseObject;
					}
				} else {
					$responseObject['status']['statusCode'] = 901;
					$responseObject['status']['statusType'] = "ERROR";
					$responseObject['status']['statusMessage'] = "Order Release ".$row['id']." is not valid";
					return $responseObject;
				}   		
    		}
    		
    		// should come here only after everything is valid..
    		foreach($releases as $row)	{
    			if($row['status'] == 'DL') {
    				func_change_order_status($row['id'], 'DL', $userId, "Updating delivered from OMT. ".$row['comment'], "", false);
    			} else if ($row['status'] == 'L') {
    				func_change_order_status($row['id'], 'L', $userId, "Marking shipment as Lost from OMT. ".$row['comment'], "", false);
    			} else if ($row['status'] == 'F'){
					func_change_order_status($row['id'], 'F', $userId, "Marking shipment as Lost from OMT. ".$row['comment']);
				} 
    		}
    		$responseObject['status']['statusCode'] = 600;
			$responseObject['status']['statusType'] = "SUCCESS";
			$responseObject['status']['statusMessage'] = "OrderRelease(s) updated successfully";
			return $responseObject;     		    		
    	} else {
    		$responseObject['status']['statusCode'] = 901;
			$responseObject['status']['statusType'] = "ERROR";
			$responseObject['status']['statusMessage'] = "No Order releases present in the request";
			return $responseObject;
    	}
    }	

	private function preQueueRTO($data, $userId){
		$releases = $data['orderRelease'];
		$responseObject = array();
		
		if($releases && !empty($releases)) {
			if(isset($releases['id']))
    			$releases = array($releases);
	    	foreach($releases as $row) {
		    	if($row['id'] && !empty($row['id'])){
							    		
		    		$order_release_row = func_query_first("Select orderid, status from xcart_orders where orderid = ". $row['id'], true);
					if($order_release_row) {
						if($order_release_row['status'] != 'SH') {
							$responseObject['status']['statusCode'] = 901;
							$responseObject['status']['statusType'] = "ERROR";
							$responseObject['status']['statusMessage'] = "Order Release ".$row['id']." is not in Shipped status";
							return $responseObject;
						}				
					} else {
						$responseObject['status']['statusCode'] = 901;
						$responseObject['status']['statusType'] = "ERROR";
						$responseObject['status']['statusMessage'] = "Order Release ".$row['id']." is not valid";
						return $responseObject;
					}
		    		
		    	} else {
		    		$responseObject['status']['statusCode'] = 901;
					$responseObject['status']['statusType'] = "ERROR";
					$responseObject['status']['statusMessage'] = "Order Release ".$row['id']." is not valid";
					return $responseObject;
		    	}
	    	}
	    	
	    	//If all releases are valid... then it should come here...
	    	$statusMessage = "";
	    	foreach($releases as $row) {
	    		if(isRTO($row['id']) === true){
	    			$statusMessage .= "RTO is already filed for order release " . $row['id']."\n";
	    		}else{
		    		$status = prequeueRTO($row['id'], $userId);
		    		if($status !== true){
		    			$statusMessage .= "RTO creation failed for order release " . $row['id'].". $status\n";
		    		} else {
		    			$statusMessage .= "RTO created for order release " . $row['id']."\n";
		    		}
	    		}
	    	}
	    	$responseObject['status']['statusCode'] = 600;
			$responseObject['status']['statusType'] = "SUCCESS";
			$responseObject['status']['statusMessage'] = $statusMessage;
			return $responseObject; 	    	
		} else {
			$responseObject['status']['statusCode'] = 901;
			$responseObject['status']['statusType'] = "ERROR";
			$responseObject['status']['statusMessage'] = "No OrderReleases present in the request";
			return $responseObject;
	    } 
	}
	
	private function addOrderToRtsBatch($requestData){
		$releaseid = $requestData['orderRelease']['id'];
		$batchid = $requestData['batchid'];
		$login = $requestData['login'];
	
		if($batchid ==null || $batchid == ''){
			$sql = "insert into ready_to_ship_batch (id,orderids,created_by,created_time,last_updated_time) values (null,case when orderids is null then '$releaseid' else concat(orderids,',',$releaseid) end,'$login',now(),now()) ON DUPLICATE KEY UPDATE orderids = concat(orderids,',',values(orderids)),last_updated_time = now()";
		}else{
			$sql = "update ready_to_ship_batch set orderids = concat(orderids,',',$releaseid),last_updated_time = now() where id = $batchid";
		}
		func_query($sql);
		
		if($batchid != '' && $batchid != null){
			$statusMessage = "Batchid updated";
		}else{
			$statusMessage = "Inseted into DB";
			$batchid = db_insert_id();
		}
		
		$responseObject['status']['statusCode'] = 600;
		$responseObject['status']['statusType'] = "SUCCESS";
		$responseObject['status']['statusMessage'] = $statusMessage;
		$responseObject['status']['batchid'] = $batchid;
		return $responseObject;
	}
	
}
?>
