<?php
include_once($xcart_dir."/Cache/Cache.php");
include_once("$xcart_dir/Cache/CacheKeySet.php");
include_once($xcart_dir."/include/func/func.courier.php");
include_once ("$xcart_dir/include/class/mcart/class.MCartFactory.php");
include_once ("$xcart_dir/include/class/mcart/class.MCart.php");
include_once ("$xcart_dir/include/class/mcart/class.MCartUtils.php");

use enums\cart\CartContext;
use style\builder\CachedStyleBuilder;

class PincodesActions{
	
    function handleRequest($request){
    	global $weblog, $sql_tbl;
   		$requestData = $request->getRequestVars();
    	$action = $requestData['data']['action'];
		switch($action) {
			case 'clearCache':
				return $this->clearCache($requestData['data']);
			case 'isServiceable':
				return $this->isServiceable($requestData['data']);
			case 'serviceablePaymentModes':
                return  $this->get_servicable_payment_modes_for_zipcode($requestData['data']);
            default:
				$responseObject = array();
	            $responseObject['status'] = array();
	            $responseObject['status']['statusCode'] = 600;
	            $responseObject['status']['statusMessage'] = "No action defined for $action";
	            $responseObject['status']['statusType'] = "SUCCESS";
            	return $responseObject;
		}
		
        return false;
    }
    
    private function get_servicable_payment_modes_for_zipcode($requestData){
    	$context = strtolower($requestData['context']);
    	if(empty($context))
    		$context = CartContext::DefaultContext;
    	
    	$mcartFactory= new MCartFactory();
    	$myCart = $mcartFactory->getCurrentUserCart(true, $context);
    	$servicePaymentModes = array();
    	if($myCart != null) {
    		$productsInCart = MCartUtils::convertCartToArray($myCart);
    		$skuDetails = array();
    		foreach($productsInCart as $index=>$product) {
    			$productsInCart[$index]['style_id'] = $product['productStyleId'];
    			$productsInCart[$index]['sku_id'] = $product['skuid'];
    			$skuDetails[$product['skuid']] = array('id'=>$product['skuid'], 'availableItems'=> $product['availableItems'], 'warehouse2AvailableItems'=> $product['warehouse2AvailableItems']);
    		}
    		
    		$payment_modes = getServiceablePaymentModesForProducts($requestData['pincode'], $productsInCart, false, false, $skuDetails);
    		
	        $servicePaymentModes['codSupported'] = false;
	        if(in_array('cod', $payment_modes)){
	            $servicePaymentModes['codSupported'] = true;
	        }
	        
	        $servicePaymentModes['nonCodSupported'] = false;
	        if(in_array('on', $payment_modes)){
	            $servicePaymentModes['nonCodSupported'] = true;
	        }
    	}
        return $servicePaymentModes;
    }
    
    private function clearCache($requestData){
    	global $weblog;
    	$responseObject = array(); 
    	$responseObject['status']['statusCode'] = 900;
    	$responseObject['status']['statusType'] = "SUCCESS";
    	$pincodes = $requestData['pincodes'];
    	$weblog->debug("Clearing cache for pincodes: ". $pincodes);
    	if(!empty($pincodes) && $pincodes != ''){
    		$pincodeArr = explode(',',$pincodes);
    		if(!empty($pincodeArr)){
    			foreach ( $pincodeArr as $pincode ) {
	    			$cache = new \Cache(LMSPincodeCacheKeys::$prefix.$pincode, LMSPincodeCacheKeys::keySet());
	    			$cache->invalidate();	
    			}
    		}
    	}
    	return $responseObject;
    }
    
    private function isServiceable($requestData) {

    	$order = func_query_first("Select warehouseid from xcart_orders where orderid = ".$requestData['orderId']);
    	$productsInOrder = func_create_array_products_of_order(array($requestData['orderId']));
    	
    	$couriers = func_get_order_serviceabile_courier($order['payment_method'], $requestData['newZipcode'], $productsInOrder, $order['warehouseid'], $order['total']);
    	
    	$responseObject = array();
    	$responseObject['status']['statusCode'] = 900;
    	$responseObject['status']['statusType'] = "SUCCESS";
    	$responseObject['data']['isServiceable'] = count($couriers)>0?"true":"false";
    	
    	return $responseObject;
    	
    }
}
?>
