<?php
use order\exception\CartOOSException;

include_once("$xcart_dir/include/class/class.feature_gate.keyvaluepair.php");
include_once("$xcart_dir/include/func/func_sku.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/include/func/func.core.php");
include_once("$xcart_dir/modules/apiclient/SkuApiClient.php");
include_once("$xcart_dir/tracking/trackingUtil.php");
include_once("$xcart_dir/include/func/func.mk_old_returns_tracking.php");
include_once("$xcart_dir/include/class/oms/ReturnsStatusProcessor.php");
include_once("$xcart_dir/include/class/mcart/class.MCart.php");
include_once("$xcart_dir/include/class/oms/OrderDataCorrectionManager.php");
/*include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
include_once("$xcart_dir/include/class/enums/base/SizeChartTrackerConstants.php");*/
include_once("$xcart_dir/include/class/search/UserInterestCalculator.php");
include_once("$xcart_dir/include/class/oms/OrderWHManager.php");
include_once("$xcart_dir/env.php");
include_once("$xcart_dir/modules/apiclient/ReleaseApiClient.php");
include_once("$xcart_dir/include/class/oms/EventCreationManager.php");
include_once("$xcart_dir/include/class/oms/OrderProcessingMailSender.php");
include_once($xcart_dir."/modules/giftcards/GiftCardsHelper.php");
include_once $xcart_dir."/include/class/class.mymyntra.php";
include_once("$xcart_dir/include/class/oms/OrderExchangeManager.php");
include_once($xcart_dir."/include/class/oms/OrderProcessingMailSender.php");
include_once "$xcart_dir/include/func/func.loginhelper.php";
include_once "$xcart_dir/modules/myntCash/MyntCashTransaction.php";
include_once "$xcart_dir/modules/myntCash/MyntCashService.php";
include_once "$xcart_dir/include/sessionstore/serialization_format_wrapper.php";
require_once $xcart_dir."/modules/coupon/PaymentGatewayCouponValidator.php";
include_once($xcart_dir."/include/class/class.mail.multiprovidermail.php");

use order\dataobject\builder\OrderDOBuilder;
use order\exception\CartTamperedException;
use order\exception\EmptyCartException;
use order\exception\NoSuitableGatewayException;
use order\exception\OrderCreationFailedException;
use order\exception\UserNotLoggedInException;
use order\dataobject\builder\OrderInputParamDOBuilder;
use order\manager\OrderManager;
use giftcard\manager\GiftCardOrderManager;
use enums\cart\CartContext;
use abtest\MABTest;
use feedback\instance\MFBInstance;
use feedback\manage\MFB;
use abtest\resolver\ResolverFactory;
use abtest\algos\MABTestSegmentationAlgoFactory;


class OrderActions{
	
    function handleRequest($request)    {
    	global $portalapilog, $sql_tbl ;
       		$requestData = $request->getRequestVars();
        	$action = strtolower($requestData['data']['action']);
        	$orderId = trim($requestData['data']['orderId']);
            $portalapilog->debug("OrderActions:debug:begin");
            $portalapilog->info("OrderActions: perform action $action");
    		switch($action) {
				case 'getskuwisecount':
					return $this->getSkuWiseCount($orderId);
				case 'markordershipped':
					return $this->markOrdersShipped($requestData['data']['orderIds']);
				case 'sendshipmentstatusmail':
					return $this->send_shipment_status_mail($requestData['data']);
				case 'fileudfororder':
					return $this->file_ud_for_order($requestData['data']);
				case 'shipmentstatusupdate':
					return $this->update_shipment_status($requestData['data']);	
				case 'createorder':
					return $this->create_new_order($requestData['data']);
				case 'creategiftorder':
					return $this->create_new_gift_card_order($requestData['data']);
				case 'queueorder':
					return $this->queue_order($requestData['data']);
				case 'resendstalereleases':
					return $this->resend_stale_release_data($requestData['data']);
				case 'queuegiftorder':
					return $this->queue_gift_card_order($requestData['data']);
				case 'prequeuerto':
					return $this->prequeue_rto($requestData['data']);
				case 'pushorderxmltoqueue' :
					return $this->pushorderxmltoqueue($requestData['data']);
				case 'evaluateonholdrules':
					return $this->evaluateOrderOnHoldRules($requestData['data']);
				case 'sendordernotification':
					return $this->sendOrderNotification($requestData['data']);
				case 'onholdorder':
					return $this->onhold_order($requestData['data']);
				case 'getshippingmethodfororder':
					return $this->getShippingMethod($requestData['data']);
				case 'courierchange':
					return $this->courierChange($requestData['data']);	
				case 'addordercomment':
					return $this->addOrderComment($requestData['data']);	
				case 'getordercomment':
					return $this->getOrderComments($requestData['data']);
                case 'getfeedbackformhtml':
                    return $this->getMFBHTML($requestData['data']);
                case 'getnps':
                	return $this->getNps($requestData['data']);
                case 'markshipmentslost':
                    return $this->markShipmentsLost($requestData['data']);
				default:
                        		$responseObject = array();
                                        $responseObject['status'] = array();
                                        $responseObject['status']['statusCode'] = 600;
                                        $responseObject['status']['statusMessage'] = "No action supported";
                                        $responseObject['status']['statusType'] = "SUCCESS";
                                        return $responseObject;
			}
		
        return false;
    }
	
    private function getSkuWiseCount($orderId){
    	$responseObject = array();
    	$sql = "SELECT sku_id, xod.amount as totalcount, xo.warehouseid FROM xcart_orders xo,xcart_order_details xod,
    	 mk_order_item_option_quantity oq, mk_styles_options_skus_mapping so 
    	 WHERE xo.orderid = xod.orderid  AND xod.itemid = oq.itemid
    	  AND oq.optionid = so.option_id AND xod.product_style = so.style_id and xo.orderid = '$orderId' and xo.status in ('RFR', 'WP') and xod.item_status != 'IC'";
    	$results = func_query($sql); 
    	$skusData = array();
    	$counter=0;
    	if($results) {
			foreach($results as $row) {
				$data = array();
				$data['sku']['skuId'] = $row['sku_id'];
				$data['sku']['count'] = $row['totalcount'];
				$data['sku']['warehouseId'] = $row['warehouseid'];
				$skusData[]=$data;
			}
		}
		$responseObject['status'] = array();
        $responseObject['status']['statusCode'] = 600;
        $responseObject['status']['statusMessage'] = "Sku Counts Retrieved Successfully";
        $responseObject['status']['statusType'] = "SUCCESS";
		$responseObject['data'] = $skusData;
		return $responseObject;
    }

    private function prequeue_rto($requestData){
    	
    	$responseObject = array('status'=>array());
    	$responseObject['status']['statusCode'] = 900;
    	$responseObject['status']['statusType'] = "SUCCESS";
    	
    	$order = $requestData['order'];
    	if($order['orderId'] != ''){
    		if(isRTO($order['orderId']) === true){
    			$responseObject['status']['statusMessage'] = "RTO is already filed for order id=" . $order['orderId'];
    		}else{
	    		$status = prequeueRTO($order['orderId'], 'LMS');
                        //Refund at pre-queue
                        $enableRTOprequeueRefund = FeatureGateKeyValuePairs::getFeatureGateValueForKey('enable.rto.prequeue.refund');
                        $customerReject = true; // Hard coding as customer reject. Waiting for PM's response
                        if ($enableRTOprequeueRefund === "true") {
                            $orderdata = func_order_data($order['orderId']);
                            $cancel_args = array(
                                'cancellation_type' 			=> 'CCC',
                                'reason'				=> $customerReject==true?"RTOCR":"RTO",
                                'orderid2fullcancelordermap'            => array($order['orderId']=>false),
                                'generateGoodWillCoupons'		=> false,
                                'orderid2couponDetails'			=> false
                            );
                            
                            func_change_order_status($orderdata['order']['orderid'], 'F', $orderdata['order']['login'], "Cancelling order and refunding amount to customer", "", false, '', $cancel_args);
                        }
	    		if($status !== true){
	    			$responseObject['status']['statusCode'] = 901;
	    			$responseObject['status']['statusType'] = "ERROR";
	    			$responseObject['status']['statusMessage'] = $status;
	    		} else {
	    			$is_exchange = $order['isExchange'];
	    			if($is_exchange == 'true'){
	    				$manager = new OrderExchangeManager();
	    				$returnStatus = $order['returnStatus'] == 'RT' ? 'RPU' : 'RRD';
	    				$response = $manager->handleExchangeOrderFailedDelivery($order['orderId'], $returnStatus);
	    				if(!$response){
	    					$responseObject['status']['statusCode'] = 901;
	    					$responseObject['status']['statusType'] = "ERROR";
	    				}
	    				$responseObject['status']['statusMessage'] = $response['message'];
	    			}
	    		}
    		}
    	}else{
    		$responseObject['status']['statusMessage'] = "Order id not found in the request.";
    	}
    	return $responseObject;
    }
    
	private function markOrdersShipped($commaSeperatedOrderIds){
		$responseObject = array();
    	global $portalapilog;
    	$orderIds = explode(',', $commaSeperatedOrderIds);
    	$portalapilog->logger("Marking orders shipped. Order ids: " . print_r($orderIds, true));
		$final_response = mark_orders_shipped($orderIds, 'LMS-ADMIN',null,false, false, false);
		$successfulOrderIds = array();
		$unsuccessfulOrderIds = array();
		$responseObject['status'] = array();
		if(isset($final_response['failed'])) {
			$failedOrderData="";
			foreach ($final_response['failed'] as $failed_order){
				$failedOrderData .= "[OrderId=".$failed_order["orderid"] . " Error: " . $failed_order['errormsg'] . "]";
			}
			$responseObject['status']['statusCode'] = 601;
			$responseObject['status']['statusType'] = "ERROR";
			$responseObject['status']['statusMessage'] = "Failed to mark some orders shipped.";
			$responseObject['data'] = $failedOrderData;
		}else{
			$responseObject['status']['statusCode'] = 600;
			$responseObject['status']['statusType'] = "SUCCESS";
			$responseObject['status']['statusMessage'] = "Orders marked shipped.";
		}    
		
		return $responseObject;
	}
    /**
     * Assuming this API is used only by LMS. Harcoding the 
     * @global type $portalapilog
     * @param type $commaSeparatedShipmentIds
     * @return string
     */
    private function markShipmentsLost($data) {
        //echo print_r($data);
        //echo $print_r($data); return;
        $releaseId = $data['orderRelease']['id'];
        $responseObject = array();
        global $portalapilog;
        $portalapilog->logger("Marking orders lost. Order id: " . print_r($releaseId, true));
        // Assuming that only logistics uploads this and all the lost orders happen from logistics and are in-transit lost orders
        $cancel_args = array(
        'cancellation_type' => 'LOC',
        'reason' => 'LIT',
        'generateGoodWillCoupons' => false,
        'is_oh_cancellation' => false,
        'orderid2fullcancelordermap' => array($releaseId => false));
        $final_response = func_change_order_status($releaseId, 'L', 'System Script', "Marking the order as Lost", '', false, '', $cancel_args);
        $responseObject['status'] = array();
        if ($final_response['status'] == 'failure') {
            $responseObject['status']['statusCode'] = 601;
            $responseObject['status']['statusType'] = "ERROR";
            $responseObject['status']['statusMessage'] = "Failed to mark shipment lost";
        } else if ($final_response['exOrder'] === true) {
            $responseObject['status']['statusCode'] = 600;
            $responseObject['status']['statusType'] = "SUCCESS";
            $responseObject['status']['statusMessage'] = "Shipment marked lost. No issue refund for exchange order";
        } else {
            $responseObject['status']['statusCode'] = 600;
            $responseObject['status']['statusType'] = "SUCCESS";
            $responseObject['status']['statusMessage'] = "Shipment marked lost. Refund has been issued";
        }
        return $responseObject;
    }

    private function send_shipment_status_mail($requestData){
		global $portalapilog;
		$order = $requestData['order'];
    	$responseObject = array(); 
    	$responseObject['status']['statusCode'] = 900;
    	$responseObject['status']['statusType'] = "SUCCESS";
    	if(strtolower($order['status']) == 'out_for_pickup'){
    		$processor = new ReturnsStatusProcessor();
	    	$processor->sendPickupStatusUpdateMail($order['returnId'],$order['status'],'','',$order["deliveryStaff"]);
	    	return $responseObject;	
    	}else if(strtolower($order['status']) == 'shipped'){
    		$orderIds = explode(',', $order['orderIds']);
			if(!empty($orderIds)){	
				$portalapilog->debug("Sending ship notification to customer for orders:  " . $order['orderIds']);
				foreach($orderIds as $orderId){
					$order_data = func_order_data($orderId);
					if (empty($order_data)){
						$portalapilog->debug("No order found to send notification to customer for order:  " . $orderId);
						continue;
					}
					if($order_data["order"]['status']=='SH'){
						$portalapilog->debug("Sending ship notification to customer for order:  " . $orderId);
						OrderProcessingMailSender::func_send_order_shipped_email($order_data["order"], $order_data['userinfo']);
						$portalapilog->debug("Sent ship notification to customer for order:  " . $orderId);
					}else{
						$portalapilog->debug('Not sending email for order: ' . $orderId . ' which is in ' .$order_data["order"]['status'] . ' state.');
					}
				}
			}
	    	return $responseObject;	
	    }else if(strtolower($order['status']) == 'exchange_shipped'){
	    	$orderIds = explode(',', $order['orderIds']);
	    	if(!empty($orderIds)){
	    		$portalapilog->debug("Sending ship notification to customer for exchange orders:  " . $order['orderIds']);
	    		foreach($orderIds as $orderId){
	    			$order_data = func_order_data($orderId);
	    			if (empty($order_data)){
	    				$portalapilog->debug("No order found to send notification to customer for order:  " . $orderId);
	    				continue;
	    			}
	    				
	    			if($order_data["order"]['status']=='SH'){
	    				$portalapilog->debug("Sending ship notification to customer for ex-order:  " . $orderId);
	    				$mailArgs = array("courier_service_display" => $order_data["order"]["courier_service_display"], "tracking" => $order_data["order"]["tracking"]);
	    				OrderProcessingMailSender::send_exchange_order_mail($order_data["order"], "exchange_shipped", $mailArgs);
	    			}
	    		}
	    	}
	    	return $responseObject;
	    }else if(strtolower($order['status']) == 'failed_exchange'){
	    	$orderId = $order['id'];
	    	$order_data = func_order_data($orderId);
	    	$portalapilog->debug("Sending exchange failed notification to customer for order:  " . $orderId);
	    	OrderProcessingMailSender::send_exchange_order_mail($order_data["order"], "exchange_failed_delivery", '');
	    	return $responseObject;
	    }else if(strtolower($order['status']) == 'successful_exchange'){
	    	// mark exchange order as delivered and corresponding return as picked up.
	    	$orderId = $order['id'];
	    	$order_data = func_order_data($orderId);
	    	if($order_data['order']['status'] == 'SH') {
    			func_change_order_status($orderId, 'DL', 'LMSAdmin', "Updated via LMS shipment interface", "", false, '',false,false,'',false, time());
    			$order_payment_method = $order_data['order']['payment_method'];
    			if($order_payment_method != 'cod'){
    				func_change_order_status($orderId, 'C', 'LMSAdmin', "Updated via LMS shipment interface", "", false);
    			}
		    	$portalapilog->debug("Sending exchange failed notification to customer for order:  " . $orderId);
		    	OrderProcessingMailSender::send_exchange_order_mail($order_data["order"], "exchange_successful_delivery", '');
		    	$return = func_query_first("select ex.returnid, xr.status from exchange_order_mappings ex, xcart_returns xr 
		    			where xr.returnid = ex.returnid and ex.exchange_orderid = $orderId", true);
		    	if($return['returnid'] && $return['status'] == 'RPI'){
		    		$processor = new ReturnsStatusProcessor();
		    		$comment = 'Marking return as picked-up upon successful exchange delivery';
		    		$response = $processor->changeReturnsStatus(trim($return['returnid']), 'RPU', '', '', '', '', 'LMS_API',$comment);
		    	}
	    	}
	    	return $responseObject;
	    }else if(strtolower($order['status']) == 'out_for_exchange'){
	    	$orderId = $order['id'];
	    	$order_data = func_order_data($orderId);
	    	$portalapilog->debug("Sending ship notification to customer for ex-order:  " . $orderId);
	    	$mailArgs = array("deliveryStaff_name" => $order["deliveryStaff"]["firstName"], "deliveryStaff_mobile" => $order["deliveryStaff"]["mobile"]);
	    	OrderProcessingMailSender::send_exchange_order_mail($order_data["order"], "exchange_out_for_delivery", $mailArgs);
	    	return $responseObject;
    	}else{
	    	if($order['id'] != '' && $order['status'] != ''){
	    		$time = strtotime("".$order['deliveredDate']);
		        $myDate = $time!=null?date( 'jS F Y', $time ):null;
		        $orderStatus = strtolower($order['status']);
		        $order_data_db = func_order_data($order['id']);
		        if($order_data_db['order']['status'] == 'SH') {
			        if($orderStatus == 'successful_delivery' || $orderStatus == 'delayed_delivery') {
			        	func_change_order_status($order['id'], 'DL', 'LMSAdmin', "Updated via LMS shipment interface", "", false, '',false,false,'',false, $time);
			        	$order_payment_method = func_query_first_cell("select payment_method from xcart_orders where orderid = ".$order['id'],true);
	                    if($order_payment_method != 'cod'){
	                    	func_change_order_status($order['id'], 'C', 'LMSAdmin', "Updated via LMS shipment interface", "", false);
	                    }
			        }
			        
			        if(strtolower($order['skipCustomerNotification'])!='true'){
			        	
			    		$mailsent = OrderProcessingMailSender::send_shipment_status_update_mail($order['id'], $orderStatus, $myDate, $order["deliveryStaff"]["firstName"],  $order["deliveryStaff"]["mobile"]);
			    		$portalapilog->debug(print_r($mailsent,true));
			        }
		        }	
	    	}	
    	}
    	return $responseObject;
	}
	
	private function file_ud_for_order($requestData){
		$order = $requestData['orders']['order'];
		$responseObject = array(); 
    	$responseObject['status']['statusCode'] = 900;
    	$responseObject['status']['statusType'] = "SUCCESS";
    	
    	if($order['id'] != ''){
    		global $portalapilog;
    		$sql = "select orderid from mk_old_returns_tracking_ud where orderid = ".$order['id'];
    		$portalapilog->debug($sql);
    		$ud_order = func_query_first($sql,true);
    		$remark = $order['remark']." --- reason ::  ".$order['reason'];
    		if(!$ud_order){
    			$data['orderid'] = $order['id'];
				$data['currstate'] = addslashes($order['rt_action']);
				$data['iscod']	 = addslashes($order['iscod']);
				$data['custname']= addslashes($order['cust_name']);
				$data['pickupaddress'] = addslashes($order['cust_addr']);
				$data['pickupcity'] = addslashes($order['cust_city']);
				$data['pickupstate'] = addslashes($order['cust_state']);
				$data['pickupcountry'] = addslashes($order['cust_country']);
				$data['pickupzipcode'] = addslashes($order['cust_zipcode']);
				$data['phonenumber'] = addslashes($order['contact_no']);
				$data['courier'] = addslashes($order['courier']);
				$data['trackingno'] = addslashes($order['tracking_num']);
				$data['attemptcount'] = addslashes($order['delivery_attempts']);
				$data['reason'] = addslashes($order['reason']);
				unset($data['remark']);
	    		createUDOrder('LMS_API', $data,$remark);	
    		}else{
				// addUDAction($order['id'], '', 'UDQ', 'LMS_API',$remark,null,null,$order['reason']);    			
    		}
    	}else{
    		$responseObject['status']['statusType'] = "FAILED";
    	}
    	return $responseObject;
	}
	
    private function create_new_gift_card_order($requestData){
		global $portalapilog, $http_location;
		// Initialization
		$portalapilog->info(print_r($requestData, true));
		$userid = strtolower($requestData['login']);
		$pgDiscount = $requestData['pgDiscount'];
		$emiCharge = $requestData['emiCharge'];
		$addressId = $requestData['address'];
		$clientCode = $requestData['clientContext'];
		$cartContext = $requestData['cartContext'];
		$paymentMethod = $requestData['paymentMethod'];
		$b_firstname = $requestData['b_firstname'];
		$b_address = $requestData['b_address'];
		$b_city = $requestData['b_city'];
		$b_state = $requestData['b_state'];
		$b_country = $requestData['b_country'];
		$b_zipcode = $requestData['b_zipcode'];
		$sessionId = $requestData['sessionId'];
		$session = getSessionBySessionId($sessionId);
		$session_data = x_session_unserialize($sessionId, $session["data"]);
		$lastUpdatedTime = $session_data['LAST_CART_UPDATE_TIME'];
		
		//Get the cart for which Order needs to be created
		$mcartFactory = new MCartFactory();
		$cart = $mcartFactory->getCartForLogin($userid, false, CartContext::GiftCardContext);
		//Create Order Data Object Builder from which Order DO and all child DOs get created
		$orderDOBuilder = new OrderDOBuilder();
                
		$responseObject = array();
		$responseObject['status'] = array();
		$responseObject['status']['statusType'] = "FAILURE";
		
		//Build OrderDO from the cart
		try {
			if($cart == null) {
				throw new EmptyCartException("Cart Empty while checkout: may be due to concurrent activity");
			}
			$orderDO = $orderDOBuilder->buildOrderActions($cart, $addressId, $clientCode, $paymentMethod, $b_firstname, $b_address, $b_city, $b_state, $b_country, $b_zipcode, $sessionId);
		} catch (UserNotLoggedInException $exc) {
			$responseObject['exception'] = $exc;
			$responseObject['redirectURL'] = "$http_location";
			$responseObject['status']['statusMessage'] = $exc->getMessage();
		} catch (EmptyCartException $exc) {
			$responseObject['exception'] = $exc;
			$responseObject['redirectURL'] = "$http_location/mkmygiftcardcart.php";
			$responseObject['status']['statusMessage'] = $exc->getMessage();
		} catch (Exception $e) {
			$responseObject['exception'] = $e;
			$responseObject['redirectURL'] = $https_location . "/mkgiftcardpaymentoptions.php?transaction_status=N";
			$responseObject['status']['statusMessage'] = $e->getMessage();
		}
		// Create Order and send response
		if(!empty($orderDO)){
			//Get an instance of OrderManager, which will do the top level business transaction
			$orderManager = new GiftCardOrderManager($orderDO);
			//Stage-1: Create order by doing checkout
			try {
                                $orderManager->checkoutOrderActions($pgDiscount, $emiCharge, $lastUpdatedTime);

                                $amountDO = $orderDO->getAmountDO();
                                $finalAmountToCharge = $amountDO->getFinalAmountToCharge();
                                $orderId = $orderDO->getOrderId();
                                $session_data["gcorderID"]=$orderId;
                                updateSessionDataBySessionId($sessionId, x_session_serialize($sessionId, $session_data));
		
				$responseObject['order'] = array();
				$responseObject['order']['orderId'] = $orderId;
				$responseObject['order']['amount'] = $finalAmountToCharge;
		
				$responseObject['status']['statusType'] = "SUCCESS";
				$responseObject['status']['statusMessage'] = "Order placed successfuly.";
		
			} catch (UserNotLoggedInException $unli) {
				//User's session somehow lost
				$responseObject['exception'] = $unli;
				$responseObject['redirectURL'] = $https_location;
				$responseObject['status']['statusMessage'] = $unli->getMessage();
			} catch (EmptyCartException $ece) {
				$responseObject['exception'] = $exe;
				$responseObject['redirectURL'] = "$http_location/mkmygiftcardcart.php";
				//stale cart , cart is made empty from another tab/window - cannot proceed. Rebuild the cart again
				$responseObject['status']['statusMessage'] = $ece->getMessage();
			} catch (CartTamperedException $cte) {
				$responseObject['exception'] = $cte;
				$responseObject['redirectURL'] = "$http_location/mkmygiftcardcart.php";
				//Cart was possibly tampered. Rebuild the cart again
				$responseObject['status']['statusMessage'] = $cte->getMessage();
			} catch (NoShippingAddressException $exc) {
				$responseObject['exception'] = $exc;
				$responseObject['redirectURL'] = $https_location . "/mkmygiftcardcart.php";
				$responseObject['status']['statusMessage'] = $exc->getMessage();
			} catch(CouponException $cpexc) {
				$responseObject['exception'] = $cpexc;
				$responseObject['redirectURL'] = $https_location . "/mkgiftcardpaymentoptions.php";
				//NOTE: Removed x_session_register: redirectedToCart:
				$responseObject['status']['statusMessage'] = $cpexc->getMessage();
			} catch(OrderCreationFailedException $ordex) {
				$responseObject['exception'] = $ordex;
				$responseObject['redirectURL'] = $https_location . "/mkgiftcardpaymentoptions.php?transaction_status=N";
				$responseObject['status']['statusMessage'] = "Order creation failed: ".$ordex->getDO()." Your Transaction failed. We request you to kindly re-try the transaction.";
			} catch (Exception $e) {
				$responseObject['exception'] = $e;
				$responseObject['redirectURL'] = $https_location . "/mkgiftcardpaymentoptions.php?transaction_status=N";
				$responseObject['status']['statusMessage'] = $e->getMessage();
			}
		}
		return $responseObject;
	}
        
	private function create_new_order($requestData){
		global $portalapilog, $http_location;
		// Initialization
		$portalapilog->info(print_r($requestData, true));
		$userid = strtolower($requestData['login']);
		$pgDiscount = $requestData['pgDiscount'];
		$emiCharge = $requestData['emiCharge'];
		$addressId = $requestData['address'];
		$clientCode = $requestData['clientContext'];
        $cartContext = $requestData['cartContext'];
		// Bin number
        $binNumber = $requestData['binNumber'];

        $paymentMethod = $requestData['paymentMethod'];
		$b_firstname = $requestData['b_firstname'];
		$b_address = $requestData['b_address'];
		$b_city = $requestData['b_city'];
		$b_state = $requestData['b_state'];
		$b_country = $requestData['b_country'];
		$b_zipcode = $requestData['b_zipcode'];
		$sessionId = $requestData['sessionId'];
		$session = getSessionBySessionId($sessionId);
		$session_data = x_session_unserialize($sessionId, $session["data"]);
		$lastUpdatedTime = $session_data['LAST_CART_UPDATE_TIME'];
		
		//Get the cart for which Order needs to be created
	#$mcartFactory = new MCartFactory();
	#	$cart = $mcartFactory->getCartForLogin($userid, false, $cartContext);
     # $cart = getCartForOrderRequest($requestData);
     //$userId = strtolower($requestData['login']);
        $cart = $this->getCartForOrderRequest($requestData);
		//Create Order Data Object Builder from which Order DO and all child DOs get created
		$orderDOBuilder = new OrderDOBuilder();
		
		$responseObject = array();
		$responseObject['status'] = array();
		$responseObject['status']['statusType'] = "FAILURE";


        // Validate for Payment Gateway Coupons
        if(!PaymentGatewayCouponValidator::isCouponValidForBin($binNumber,$sessionId)){
            $responseObject['status']['statusMessage'] = "Bin validation failed";
            return $responseObject;
        }

		//Build OrderDO from the cart
		try {
			if($cart == null) {
				throw new EmptyCartException("Cart Empty while checkout: may be due to concurrent activity");
			}
			$orderDO = $orderDOBuilder->buildOrderActions($cart, $addressId, $clientCode, $paymentMethod, $b_firstname, $b_address, $b_city, $b_state, $b_country, $b_zipcode, $sessionId);
            if (isGuestCheckout()) {
                $orderDO->setLogin($userid);
                $orderDO->getBillingAddressDO()->setEmail($userid);
            }
		} catch (UserNotLoggedInException $exc) {
			$responseObject['exception'] = $exc;
			$responseObject['redirectURL'] = "$http_location";
			$responseObject['status']['statusMessage'] = $exc->getMessage();
		} catch (EmptyCartException $exc) {
			$responseObject['exception'] = $exc;
			$responseObject['redirectURL'] = "$http_location/mkmycart.php?at=c&pagetype=productdetail&src=po";
			$responseObject['status']['statusMessage'] = $exc->getMessage();
		} catch (Exception $e) {
			$responseObject['exception'] = $e;
			$responseObject['redirectURL'] = $https_location . "/mkpaymentoptions.php?transaction_status=N";
			$responseObject['status']['statusMessage'] = $e->getMessage();
		}
		// Create Order and send response
		if(!empty($orderDO)){
			//Get an instance of OrderManager, which will do the top level business transaction
			$orderManager = new OrderManager($orderDO);
			//Stage-1: Create order by doing checkout
			try {
                $orderManager->checkoutOrderActions($pgDiscount, $emiCharge, $lastUpdatedTime);

                $amountDO = $orderDO->getAmountDO();
                $finalAmountToCharge = $amountDO->getFinalAmountToCharge();

                // hot fix to block cod hacck

                $cashRedeemed = $amountDO->getCashRedeemed();
                $codAmountRange = trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('cod.limit.range'));
                $codAmountRangeArray = explode("-", $codAmountRange);
                $codMinVal = $codAmountRangeArray[0];
                $codMaxVal = $codAmountRangeArray[1];
                if($paymentMethod == 'cod' && (($finalAmountToCharge+$cashRedeemed) < $codMinVal || $finalAmountToCharge > $codMaxVal)) {
                    throw new CartTamperedException("Cart tampered Cash on Delivery is currently available for order values between Rs. $codMinVal to Rs. $codMaxVal only");
                }   

                $orderId = $orderDO->getOrderId();
				$session_data['orderID'] = $orderId;
				updateSessionDataBySessionId($sessionId, x_session_serialize($sessionId, $session_data));
	            	
				$responseObject['order'] = array();
				$responseObject['order']['orderId'] = $orderId;
				$responseObject['order']['amount'] = $finalAmountToCharge;
		
				$responseObject['status']['statusType'] = "SUCCESS";
				$responseObject['status']['statusMessage'] = "Order placed successfuly.";
				
				EventCreationManager::pushCompleteOrderEvent($orderId);
		
			} catch (UserNotLoggedInException $unli) {
				//User's session somehow lost
				$responseObject['exception'] = $unli;
				$responseObject['redirectURL'] = $https_location;
				$responseObject['status']['statusMessage'] = $unli->getMessage();
			} catch (EmptyCartException $ece) {
				$responseObject['exception'] = $ece;
				$responseObject['redirectURL'] = "$http_location/mkmycart.php?at=c&pagetype=productdetail&src=po";
				//stale cart , cart is made empty from another tab/window - cannot proceed. Rebuild the cart again
				$responseObject['status']['statusMessage'] = $ece->getMessage();
			} catch (CartTamperedException $cte) {
				$responseObject['exception'] = $cte;
				$responseObject['redirectURL'] = "$http_location/mkmycart.php?at=c&pagetype=productdetail&src=mi";
				//Cart was possibly tampered. Rebuild the cart again
				$responseObject['status']['statusMessage'] = $cte->getMessage();
			} catch (CartOOSException $coose) {
				$responseObject['exception'] = $coose;
				$responseObject['redirectURL'] = "$http_location/mkmycart.php?at=c&pagetype=productdetail&src=mi";
				//Cart was possibly tampered. Rebuild the cart again
				$responseObject['status']['statusMessage'] = $coose->getMessage();
			} catch (NoShippingAddressException $exc) {
				$responseObject['exception'] = $exc;
				$responseObject['redirectURL'] = $https_location . "/mkcustomeraddress.php?change=true";
				$responseObject['status']['statusMessage'] = $exc->getMessage();
			} catch(CouponException $cpexc) {
				error_log("Inside coupon expeption 448 ".$login." final amout to charge ".$finalAmountToCharge);
				$responseObject['exception'] = $cpexc;
				$responseObject['redirectURL'] = $https_location . "/mkpaymentoptions.php";
				//NOTE: Removed x_session_register: redirectedToCart:
				$responseObject['status']['statusMessage'] = $cpexc->getMessage();
			} catch(OrderCreationFailedException $ordex) {
				$responseObject['exception'] = $ordex;
				$responseObject['redirectURL'] = $https_location . "/mkpaymentoptions.php?transaction_status=N";
				$responseObject['status']['statusMessage'] = "Order creation failed: ".$ordex->getDO()." Your Transaction failed. We request you to kindly re-try the transaction.";
			} catch (Exception $e) {
				error_log("Inside general expeption 448 ".$e->getMessage().$login." final amout to charge ".$finalAmountToCharge);
				$responseObject['exception'] = $e;
				$responseObject['redirectURL'] = $https_location . "/mkpaymentoptions.php?transaction_status=N";
				$responseObject['status']['statusMessage'] = $e->getMessage();
			}
		}
		return $responseObject;
	}
	
        /*
         * Function to queue a gift card order
         * In case of gift cards there are only two statuses - Pre Processed(PP) and Complete(C)
         * If a order is successful then it moves to Complete(C) state otherwise it stays in Pre Processed(PP) state
         */
	private function queue_gift_card_order($requestData){
            
                /* Get user profile data */
                $mymyntra = new MyMyntra(mysql_real_escape_string($login));
                $customer = $mymyntra->getBasicUserProfileData($login);
                $customerMobileNo = $customer["mobile"];
                
                /* Create coupon adapter instance */
		$adapter = CouponAdapter::getInstance();
		/*$tracker = new BaseTracker(BaseTracker::CONFIRMATION);*/
		global $sql_tbl, $portalapilog, $server_name, $https_location, $http_location;
		
		/* Initialization */
		$portalapilog->info(print_r($requestData, true));
		$clientCode = $requestData['clientContext'];
		$cartContext = $requestData['cartContext'];
                /* Response object creation */
		$responseObject = array();
		$responseObject['status'] = array();
		$responseObject['status']['statusType'] = "FAILURE";
		
                /* Order id to be processed */
		$orderid = $requestData['orderid'];
                /* Order status */
		$status = strtolower($requestData['status']);

                /* Get gift card order details */
		$order_result = \GiftCardsHelper::getGiftCardOrderById($orderid);
                /* Convert object to array */
                //$order_result=objectToArray($order_result);

                /* Login email for the given order id */
		$login = $order_result->login;
		
		/* TODO: should set the global $XCART_SESSION_VARS['login'] appropriatly based on the caller */
		global $XCART_SESSION_VARS;
		$XCART_SESSION_VARS['login'] = $login;
                
                /* Set all gift card order properties to variables */
		$paymentoption = $order_result->paymentOption;
		$order_channel= CartContext::GiftCardContext;
		$prev_status = ($order_result->status == 0)?"PP":"C";
                $subTotal = $order_result->subTotal;
		$couponCode = "";
		$cashCouponCode = $order_result->cashCouponCode;
                $cashDiscount = $order_result->cashRedeemed;
                $codCharge = $order_result->codCharge;
                $productDiscount = $order_result->discount;
                $cartDiscount = $order_result->cartDiscount;
                $paymentGatewayDiscount = $order_result->paymentGatewayDiscount;
                $emiCharge = $order_result->paymentSurcharge;
		$amountToBePaid = $subTotal - $cashRedeemed + $codCharge + $paymentSurcharge - $cartDiscount - $cartDiscount - $paymentGatewayDiscount;
		$amountToBePaid = number_format($amountToBePaid, 2, ".", '');
		$amountToBePaid = $amountToBePaid < 1 ? 0 : $amountToBePaid; //less than 1re payment is not accepted by banks to making the amount to zero in case final amount falls below 1re.

                /* No fraud checking happens for giftcards */
                
                /* Check for if coupon validation feature gate is turned on or whether any of coupons/cashback has been used */
		if(FeatureGateKeyValuePairs::getBoolean("validation.coupon.enabled")===true&&(!empty($couponCode)||!empty($cashCouponCode))) {
                    $validator = CouponValidator::getInstance();
                    if(!empty($couponCode)) {
                        $usedCoupon=$couponCode;
                    } else {
                        $usedCoupon=$cashCouponCode;
                    }
                    try {
			$coupon = $adapter->fetchCoupon($usedCoupon);
			$isValid = $validator->isCouponAllowedForUse($coupon, $login);
                    } catch (CouponException $ex) {
                        $status = 'fraud';
			$couponValidationFailedMessage = "\nCoupon ($usedCoupon) validation has failed. [{$ex->getMessage()}]";
                    }
		}

                /* Succesful transaction  */
		if($status == 'success') { // $isPaymentValid should translate to $status success
            if($paymentoption == "NP"){//if no payment(order amount 0 by discount or coupon discount)
            	$orderstatus = "Q";
            } else {
            	$orderstatus = "Q";
            }    

            $portalapilog->debug("gift card order id --- ". $orderid ." --- gift card order status --- ". $orderstatus);
                        
            /* Get current user gift card cart */
            $mcartFactory= new MCartFactory();
            $myCart= $mcartFactory->getCartForLogin($login, false, $cartContext);
                    
                    /* If payment option is not chq/cod/NP */
		    if($paymentoption != "chq" && $paymentoption != "cod" && $paymentoption != "NP"){
            	/* If payment option is online - cc/dc/netbanking/emi */
                if($paymentoption == "on") {
                	if(!empty($cashDiscount)){
                    	$trs = new MyntCashTransaction($login,MyntCashItemTypes::ORDER, $orderid, MyntCashBusinessProcesses::CASHBACK_USED, 0, 0,$cashDiscount,  "Usage on order no. $orderid");
                        $myntCashUsageResponse = MyntCashService::debitMyntCashForGiftCard($trs,$orderid);
                    }
                }			
                        
                /* Get gift card id */
                $giftCardId = $order_result->giftCard->id;
                /* Set orderstatus to complete */
                $orderstatus = 'C';
                /* Send gift card order confirmation mail */
                func_gift_card_order_confirm_mail($order_result, $customer);
                /* Set Gift Card Order as complete */
                \GiftCardsHelper::setGiftCardOrderComplete($giftCardId);
                /* Trigger async email send request to email the gift card */
                \GiftCardsHelper::sendEmailForGiftCardAsync($giftCardId, "true");
                $msg = "Dear ".$customer["firstname"].",Your order ".$orderid." has been placed successfully. Thank you www.myntra.com" ;			
                /* Send sms through cell next */
                $portalapilog->info("sms on order confirmation order id :: $orderid, mobile :: $customerMobileNo, msg :: $msg");
                func_send_sms_cellnext($customerMobileNo,$msg);
            }    
                    
            $myCart->setOrderID($orderid);
            /* Delete the cart */
            MCartUtils::deleteCart($myCart, true);
                    
            $uuid = stripslashes(getMyntraCookie('MYNTRA_SHOPPING_ID'));
            setMyntraCookie("MYNTRA_UNQ_COOKIE_ID",substr($uuid,1,20),time() - 3600,'/',$cookiedomain);
			
            /*$tracker->fireRequest();*/
            $responseObject['order'] = array();
            $responseObject['order']['orderstatus'] = $orderstatus;
            $responseObject['status']['statusType'] = "SUCCESS";
        	return $responseObject;
		}
		if($status == 'suspect'){
            //set status on hold because the transaction is marked as suspect
        	$orderstatus = "OH";
            $responseObject['order'] = array();
            $responseObject['order']['orderstatus'] = $orderstatus;
            $responseObject['status']['statusType'] = "SUCCESS";
            return $responseObject;
		}
		if($status == 'fraud') { //!$isPaymentDataCorrect
        	$message = "Your order has been rejected.";
            $orderstatus = "N";
		
            /* Get current user gift card cart */
            $mcartFactory= new MCartFactory();
            $myCart= $mcartFactory->getCartForLogin($login, false, $cartContext);
                    	
            $username = $login;
            $amountToBeTaken = $amountToBePaid;
            $mail_detail = array(
            	"to"=>'alrt_ebs_fraud_attempt@myntra.com',
				"subject"=>"Urgent! Possible user fraud for Orderid:$orderid",
				"content"=>"Based on payment gateway verification a suspect order has been placed by $customerFName  $customerMobileNo, orderID = $orderid.
				This order has been declined and will not be processed.
				Further Details are:
				Username: $username
				Amount to be paid: $amountToBeTaken",
				"from_name"=>'Myntra Admin',
				"from_email"=>'admin@myntra.com',
				"header"=>'');
            if(!empty($couponValidationFailedMessage)){
				$mail_detail['content'].="
                    	Extra:$couponValidationFailedMessage";
            }
            send_mail_on_domain_check($mail_detail);
            $redirectURL = $https_location . "/mkgiftcardpaymentoptions.php?transaction_status=N";

            /*$tracker->fireRequest();*/
            $responseObject['order'] = array();
            $responseObject['order']['orderstatus'] = $orderstatus;
            $responseObject['order']['message'] = $message;
            $responseObject['redirectURL'] = $redirectURL;
            $responseObject['servername'] = $server_name;
            $responseObject['status']['statusType'] = "SUCCESS";
            return $responseObject;
		}
		
		if($status == 'failure') { //!$isPaymentSuccessfull
            $redirectURL = $https_location . "/mkgiftcardpaymentoptions.php?transaction_status=N";
            $message = "We are sorry, your payment has not been successful.Please <a href='".$http_location."/mkgiftcardpaymentoptions.php'>click here</a> to try again";
            $orderstatus = "N";
            //$sql = "UPDATE ".$sql_tbl['orders']." SET tf_status ='TF',status='D',response_server='$server_name'  WHERE orderid = '".intval($orderid)."' ";  ### Update the table in case transaction get failed
            //db_query($sql);
                    
            /* Unlock the coupon/cashback account in case the transaction has failed */
            if(!empty($couponCode) && $prev_status != 'Q') {
                // Unlock the coupon.
            	$adapter->unlockCouponForUser($couponCode, $login);
            }
            if(!empty($cashCouponCode) && $prev_status != 'Q') {
            	// Unlock the coupon.
				$adapter->unlockCouponForUser($cashCouponCode, $login);
            }
			
            /*$tracker->fireRequest();*/
            $responseObject['order'] = array();
            $responseObject['order']['orderstatus'] = $orderstatus;
            $responseObject['order']['message'] = $message;
            $responseObject['redirectURL'] = $redirectURL;
            $responseObject['status']['statusType'] = "SUCCESS";
        	return $responseObject;
		}
		return $responseObject;
    }
	
	private function queue_order($requestData){
	    $clientCode = $requestData['clientContext'];
        $cartContext = $requestData['cartContext'];
		$giftCardAmount = 0;
		if (isset($requestData['giftCardAmount'])) { 
			$giftCardAmount = $requestData['giftCardAmount'];
		}
		$adapter = CouponAdapter::getInstance();
		/*$tracker = new BaseTracker(BaseTracker::CONFIRMATION);*/
		global $sql_tbl, $portalapilog, $server_name, $https_location, $http_location;
		//PAYMENT-104
		$sessionId = $requestData['sessionId'];
		if($sessionId != null){
			$session = getSessionBySessionId($sessionId);
			$session_data = x_session_unserialize($sessionId,$session["data"]);
			$session_data_cleared = MCartUtils::clearCartVariables(CartContext::DefaultContext,$session_data);
			updateSessionDataBySessionId($sessionId, x_session_serialize($sessionId,$session_data_cleared));
		}
		//end
		// Initialization
		$portalapilog->info(print_r($requestData, true));
		$responseObject = array();
		$responseObject['status'] = array();
		$responseObject['status']['statusType'] = "FAILURE";
		
		$orderid = $requestData['orderid'];
		$status = strtolower($requestData['status']);

		$order_result = func_query("SELECT * FROM $sql_tbl[orders] WHERE orderid='". $orderid ."'");

		$login = $order_result[0]['login'];
		
		/* TODO: should set the global $XCART_SESSION_VARS['login'] appropriatly based on the caller */
		global $XCART_SESSION_VARS;
		$XCART_SESSION_VARS['login'] = $login;
		
		$paymentoption = $order_result[0]['payment_method'];
		$order_channel=  $order_result[0]['channel'];
		$prev_status = $order_result[0]['status'];
		$couponCode = $order_result[0]['coupon'];
		$cashCouponCode = $order_result[0]['cash_coupon_code'];
		$amountToBePaid = $order_result[0]['subtotal'] - $order_result[0]['coupon_discount'] - $order_result[0]['cash_redeemed'] + $order_result[0]['cod'] + $order_result[0]['payment_surcharge'] - $order_result[0]['discount'] - $order_result[0]['cart_discount'] - $order_result[0]['pg_discount'];
		$amountToBePaid = number_format($amountToBePaid, 2, ".", '');
		$amountToBePaid = $amountToBePaid < 1 ? 0 : $amountToBePaid; //less than 1re payment is not accepted by banks to making the amount to zero in case final amount falls below 1re.

		if($prev_status != 'PP' && $prev_status != 'PV' && $prev_status != 'D') {
			$responseObject = array();
			$responseObject['order']['orderstatus'] = "N";
			$responseObject['status']['statusType'] = "FAILURE";
			$responseObject['status']['statusMessage'] = "Order is in invalid status";
			return $responseObject;
		}
		
		if(FeatureGateKeyValuePairs::getBoolean("validation.coupon.enabled")===true&&(!empty($couponCode)||!empty($cashCouponCode))) {
			$validator = CouponValidator::getInstance();
			if(!empty($couponCode)) {
				$usedCoupon=$couponCode;
			} else {
				$usedCoupon=$cashCouponCode;
			}
			try {
				$coupon = $adapter->fetchCoupon($usedCoupon);
				$isValid = $validator->isCouponAllowedForUse($coupon, $login);
			} catch (CouponException $ex) {
				$status = 'fraud';
				$couponValidationFailedMessage = "\nCoupon ($usedCoupon) validation has failed. [{$ex->getMessage()}]";
			}
		
		}

		if($status == 'success') { // $isPaymentValid should translate to $status success
			$mcartFactory= new MCartFactory();
			$orderPlacementFlow = 'new';
			/*$internalUsersList = FeatureGateKeyValuePairs::getFeatureGateValueForKey('orderplacement.flow.new.internalusers');
			if($internalUsersList && !empty($internalUsersList)) {
				if(strtolower($internalUsersList) == 'all') {
					$pos = strpos($login, "@myntra.com");
					if($pos !== false)
						$orderPlacementFlow = 'new';
				} else {
					$enabledUsers = explode(",", $internalUsersList);
					if(in_array($login, $enabledUsers)) {
						$orderPlacementFlow = 'new';
					}
				}
			} else {
				$orderplacementABTest = MABTest::getInstance()->getUserSegment('orderplacement');
				if($orderplacementABTest == 'test')
					$orderPlacementFlow = 'new';
			}*/
			
			if($order_channel==='telesales'){
				$myCart= $mcartFactory->getCartForLogin($login, false,'telesales');
				$saleTypeArray =Array("orderid"=>$orderid,
						"sales_agent"=>$telesalesUser);
				func_array2insertWithTypeCheck("mk_tele_sales",$saleTypeArray);
			
			}else if($order_channel==='telesalen'){
				$myCart= $mcartFactory->getCartForLogin($login, false, $cartContext);
				$agent = $session_data["identifiers"]["A"]["login"];
				$saleTypeArray =Array("orderid"=>$orderid,
						"sales_agent"=>$agent);
				func_array2insertWithTypeCheck("mk_tele_sales",$saleTypeArray);
				$commentArrayToInsert = array("orderid" => $orderid, "commenttype" => "Automated", "commenttitle" => "status change", "commentaddedby" => "System", "description" => "Order placed by $agent", "commentdate" => time());
				func_array2insert("mk_ordercommentslog", $commentArrayToInsert);
			}else{
                $myCart = $this->getCartForOrderRequest($requestData); 
            
			}
			
			
			if($orderPlacementFlow == 'new'){
				$orderstatus = 'Q';
				global $server_name;
				
				$updateSql = "UPDATE xcart_orders SET gift_card_amount='".$giftCardAmount."', status='Q',queueddate='".time()."', response_server='$server_name', cashback_processed = 1 WHERE orderid = $orderid";
				db_query($updateSql);
				
				func_addComment_status($orderid, "Q", $order_result[0]['status'], "Payment Successful. Queuing Order.", "AUTOSYSTEM");
				EventCreationManager::pushInventoryUpdateToAtpEvent($orderid);
				EventCreationManager::pushOrderConfirmationEvent($orderid, $giftCardAmount);
				if($paymentoption == "cod")	{
					if(!empty($couponCode)) {
						$adapter->updateUsageByUser($couponCode,$login,$order_result[0]['subtotal'], $order_result[0]['coupon_discount'], $orderid);
						$adapter->unlockCouponForUser($couponCode, $login);
					}
			    }
				if($paymentoption == "on") {
					if(!empty($couponCode) && $prev_status != 'Q') {
						$adapter->updateUsageByUser($couponCode,
								$userName,
								$order_result[0]['subtotal'],
								$order_result[0]['coupon_discount'],
								$orderid);
						$adapter->unlockCouponForUser($couponCode, $login);
					}
				}
			} else {
				/*$amountdetail = $order_result[0];
				
				$orderstatus = 'Q';
				if(!empty($amountdetail['cash_redeemed'])){
					$trs = new MyntCashTransaction($login, MyntCashItemTypes::ORDER, $orderid, MyntCashBusinessProcesses::CASHBACK_USED, 0, 0,$amountdetail['cash_redeemed'],  "Usage on order no. $orderid");
					$myntCashUsageResponse = MyntCashService::debitMyntCashForOrder($trs,$orderid);
					if($myntCashUsageResponse !== true){
						$orderstatus = myntCashDebitfailureProcedure($orderid,$myntCashUsageResponse); // handle failure cases and put the order ON HOLD
					}
				}
				
				$onholdVerificationRequired = false;
				if($orderstatus == 'Q') {
					$response = check_order_onhold_rules($orderid, $login, $payment_method, $order_channel, $myCart->getTotalMyntCashUsage(), false);
					if($response['status'] == 'OH') {
						if($response['verificationRequired'] === true) {
							$onholdVerificationRequired = true;
							$curr_time = date('Y-m-d H:i:s');
							$sql = "insert ignore into cod_oh_orders_dispostions(orderid,disposition,reason,trial_number,created_time,created_by) values ($orderid,'OH','".$response['sub_reason']."',0,'$curr_time','SYSTEM')";
							func_query($sql);
						}
				
						$oh_reason = get_oh_reason_for_code($response['reason'], $response['sub_reason']);
						$sql = "update xcart_orders set on_hold_reason_id_fk = ".$oh_reason['id']." where orderid = $orderid";
						func_query($sql);
						$commentArrayToInsert = Array ("orderid" => $orderid, "commenttype" => "Automated", "commenttitle" => "status change", "commentaddedby" => "System", "description" => $response['description'], "commentdate" => time());
						func_array2insert("mk_ordercommentslog", $commentArrayToInsert);
						$orderstatus = 'OH';
					} else {
						$orderstatus = 'Q';
					}
					
					if($myCart!=null){
						$productArray=MCartUtils::convertCartToArray($myCart);
					}
					$productsInCartTemp=$productArray;
					
					#############LOG USER BEHAVIOUR#############
					foreach ($productsInCartTemp as $individualProduct){
						UserInterestCalculator::trackAction($individualProduct['productStyleId'], UserInterestCalculator::PURCHASE);
					}
					##########################################
					
					$productsInCart = func_create_array_products_of_order($orderid, $productsInCartTemp);
					
					#
					#Query to fetch the order details like couponcode, tax, totalamount etc.
					#
					$amountdetail['zipcode'] = $order_result[0]['s_zipcode'];
					
					$customer = $order_result[0];//func_customer_order_address_detail($login, $orderid);
					$CustomerFirstName = $customer['firstname'];
					$CustomerLastName = $customer['lastname'];
					
					$amountdetail['discount'] = $amountdetail['discount'] + $amountdetail['cash_discount'] ;
                    $amountdetail['loyalty_credit'] = $amountdetail['loyalty_points_used']*$amountdetail['loyalty_points_conversion_factor'];
					$cashCouponCode=$amountdetail['cash_coupon_code'];
					$cashdiscount=$amountdetail['cash_redeemed'];
					$couponCode = $amountdetail['coupon'];
					$coupondiscount = $amountdetail['coupon_discount'];
					$productDiscount = $amountdetail['discount'];
					$cartDiscount = $amountdetail['cart_discount'];
					$pg_discount = $amountdetail['pg_discount'];
					$emi_charge = $amountdetail['payment_surcharge'];
					$order_time = null;//ordertime::get_order_time_string($orderid);
					$order_time_data = null;//ordertime::get_order_time_data($orderid);
					
					#
					#Check if payment option is not check payment then only coupon status will be updated
					#
					if($paymentoption == "on") {
						// Update the coupon usage, if it hasn't already been updated for this order.
						if(!empty($couponCode) && $prev_status != 'Q') {
							$userName = $login;
							$adapter->updateUsageByUser($couponCode, $userName, $amountdetail['subtotal'], $amountdetail['coupon_discount'], $orderid);
							// Unlock the coupon.
							$adapter->unlockCouponForUser($couponCode, $userName);
						}
					}
					// determine if cashback should be given on the orderid
					global $casback_earnedcredits_enabled;
					$process_cashback = true;
					if (!$casback_earnedcredits_enabled) {
						//update cashback_processed flag for the order
						func_array2update('orders',array('cashback_processed' => 1), "orderid=".$orderid);
						$process_cashback = false;
					}
					
					$invoiceid = func_generate_invoice_id($orderid, $orderstatus, $session_id,$process_cashback,$amountdetail);
					
					#
					#Calculate Net amount to be paid
					#
					$amountafterdiscount = $amountdetail['total'] + $amountdetail['cod'];
					
					// Promo for cleartrip ..
					if($amountafterdiscount  > 500 && $system_cleartrip_offer){
						sendMessageDynamicSubject("cleartrip_offer", null, $login, null, null);
					}
					
					if($amountdetail['ref_discount'] > '0'){
						$amtAfterRefDeducton = $amountdetail['total'] - $amountdetail['ref_discount'];
						$amtAfterRefDeducton = $amtAfterRefDeducton + $amountdetail['cod'];
						$amount = $amtAfterRefDeducton;
					} else {
						$amount = $amountafterdiscount;
					}
					
					if($paymentoption == "cod" && $orderstatus == "OH" && $onholdVerificationRequired) {
						$callNumber = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
						$msg = "Dear $CustomerFirstName, We need to verify some details to confirm your order with order id $orderid. We will contact you at your phone number $customerMobileNo by the end of day today or tomorrow morning. If you prefer, you can call our customer support at $callNumber at your convenience to confirm this order";
						$amountToPay = $amountdetail['total']+$amountdetail['cod'];
						OrderProcessingMailSender::notifyCodOnHoldOrderManualVerification($productsInCart,$amountdetail,$CustomerFirstName,$login,$orderid,0,$customer);
					} else {
						$delivery_date = date("jS M Y", getDeliveryDateForOrder($amountdetail['date'], $productsInCart, $amountdetail['zipcode'], $paymentoption, false, false, $orderid));
						$msg = "Hi ". $CustomerFirstName. ", Your order with order id ".$orderid." has been successfully placed. It will be shipped from our warehouse within the next 24 hours. Thank you for shopping at Myntra";
						if($paymentoption == "chq"){
							OrderProcessingMailSender::notifyOrderConfirmation('check',$productsInCart,$amountdetail,$CustomerFirstName,$login,$orderid,$customer);
						} else if($paymentoption == "NP" || $paymentoption == "on"){
							OrderProcessingMailSender::notifyOrderConfirmation('on',$productsInCart,$amountdetail,$CustomerFirstName,$login,$orderid,$customer);
						} else if($paymentoption == "cod"){
							OrderProcessingMailSender::notifyOrderConfirmation('cod',$productsInCart,$amountdetail,$CustomerFirstName,$login,$orderid,$customer);
						}
					}
						
					if($paymentoption == "cod") {
						$couponCode=$amountdetail['coupon'];
						$cashCouponCode=$amountdetail['cash_coupon_code'];
						$cashdiscount=$amountdetail['cash_redeemed'];
						if(!empty($couponCode)) {
							// In case some coupon code was applied, we need to unlock it.
							$adapter->updateUsageByUser($couponCode,$login,$amountdetail['subtotal'], $amountdetail['coupon_discount'], $orderid);
							//Unlock the coupon.
							$adapter->unlockCouponForUser($couponCode, $login);
						}
					}
					
					$customerMobileNo = $amountdetail['issues_contact_number'];
					$portalapilog->info("sms on order confirmation order id :: $orderid, mobile :: $customerMobileNo, msg :: $msg");
					func_send_sms_cellnext($customerMobileNo,$msg);
						
					// AS part of new wms.. the order should be moved immediately to WP state as there is not going to
					// be any manual assignment.
					// also move item_status to assign it to OPS Manager which is going to be id 1.
					if($orderstatus == 'Q') {
						$portalapilog->info("Move order to WP state. Current Status - $orderstatus.");
						$order_query = "UPDATE $sql_tbl[orders] SET status ='WP' WHERE orderid = $orderid";
						db_query($order_query);
						$order_detail_query = "UPDATE $sql_tbl[order_details] SET item_status = 'A', assignee=1, assignment_time = ".time()." where orderid = $orderid";
						db_query($order_detail_query);
						func_addComment_status($orderid, "WP", "Q", "Auto moving order to WP state from Q", "AUTOSYSTEM");
					}
						
					###UNCOMMENT THE FOLLOWING SECTION AFTER TESTING
					
					// All successfully done
					// check if this order needs to be split.
					$portalapilog->info("OrderWHManager: start");
					if($myCart && $myCart != null) {
					$myCart->refresh(false,false,false,true);
						$sku_details = $myCart->getAllSkus();
					}
					
					$courierServiceabilityVersion = FeatureGateKeyValuePairs::getFeatureGateValueForKey('lms.courierserviceability.version', 'old');
					if($courierServiceabilityVersion == 'new') {
						$whIdToOrderIds = \OrderWHManager::assignWarehouseForOrder($orderid, $customer, $productsInCart, $sku_details);
						if($whIdToOrderIds) {
							\OrderWHManager::updateInventoryAfterSplit($orderid, $whIdToOrderIds, "Order created from $orderid and moved to WP state", "AUTOSYSTEM");
							if($orderstatus == 'Q') {
								$releaseIdsToPush = array();
								foreach($whIdToOrderIds as $whId=>$oids) {
									$releaseIdsToPush = array_merge($releaseIdsToPush, $oids);
								}
								\ReleaseApiClient::pushOrderReleaseToWMS($releaseIdsToPush);
							}
						}
					} else {
						$whIdToOrderId = \OrderWHManagerOld::assignWarehouseForOrder($orderid, $customer, $productsInCart, $sku_details);
						if($whIdToOrderId) {
							\OrderWHManagerOld::updateInventoryAfterSplit($orderid, $whIdToOrderId, "Order created from $orderid and moved to WP state", "AUTOSYSTEM");
							if($orderstatus == 'Q') {
								\ReleaseApiClient::pushOrderReleaseToWMS(array_values($whIdToOrderId));
							}
						}
					}
					$portalapilog->info("OrderWHManager: end");
					#
					#If amount data greater than or equal to offer amount
					#
					$amountafterdiscount = number_format($amountafterdiscount,2,".",'');
				}
				$portalapilog->debug("order status --- ". $orderstatus);
				
				EventCreationManager::pushCompleteOrderEvent($orderid); */
			}				
			//capturing channel(utm_source and utm_medium) info for order
			$eventInfo = array('event_type'=>'order');
			//to capture an identifiable int value for tracking to optimize audit
			if(is_int($orderid) || is_numeric($orderid)){
				$eventInfo += array('event_value'=>$orderid);
			}
			utmTrackForEvent($eventInfo);
			
			$myCart->setOrderID($orderid);
			
			MCartUtils::deleteCart($myCart, true);
			
			$uuid = stripslashes(getMyntraCookie('MYNTRA_SHOPPING_ID'));
			global $cookiedomain;
			setMyntraCookie("MYNTRA_UNQ_COOKIE_ID",substr($uuid,1,20),time() - 3600,'/',$cookiedomain);
				
			//$tracker->fireRequest();
			$responseObject['order'] = array();
			$responseObject['order']['orderstatus'] = $orderstatus;
			$responseObject['status']['statusType'] = "SUCCESS";
		}
		if($status == 'suspect'){
			//set status on hold because the transaction is marked as suspect
			$orderstatus = "OH";
			$responseObject['order'] = array();
			$responseObject['order']['orderstatus'] = $orderstatus;
			$responseObject['status']['statusType'] = "SUCCESS";
		}
		
		if($status == 'fraud') { //!$isPaymentDataCorrect
			$message = "Your order has been rejected.";
			$orderstatus = "N";
				
			//Call function to create the array of all prodcuts of that order
			$mcartFactory= new MCartFactory();
			if($order_channel==='telesales'){
				$myCart= $mcartFactory->getCartForLogin($login, false,'telesales');
			}else{
				$myCart= $mcartFactory->getCartForLogin($login, false, $cartContext);
			}
			if($myCart!=null){
				$productArray=MCartUtils::convertCartToArray($myCart);
			}
			$productsInCartTemp=$productArray;
				
			$productsInCart = func_create_array_products_of_order($orderid, $productsInCartTemp);
			$sql = "SELECT subtotal,firstname,issues_contact_number, coupon_discount, coupon AS couponcode,discount, cart_discount,shipping_cost, tax, ordertype, ref_discount, gift_status,gift_charges, total, cod, qtyInOrder, additional_info,cash_redeemed,cash_coupon_code FROM $sql_tbl[orders] WHERE orderid = '".$orderid."'";
			$amountdetail = func_query_first($sql);
			$couponCode = $amountdetail['couponcode'];
			$coupondiscount = $amountdetail['coupon_discount'];
			$productDiscount = $amountdetail['discount'];
			$pg_discount = $amountdetail['pg_discount'];
			$emi_charge = $amountdetail['payment_surcharge'];
			$cartDiscount = $amountdetail['cart_discount'];
			$amountafterdiscount = $amountdetail['total'] + $amountdetail['cod'];
				
			$sql = "UPDATE ".$sql_tbl['orders']." SET status ='F',response_server='$server_name'  WHERE orderid = '".intval($orderid)."' ";  ### Update the table in case fraud happened in EBS gateway
			$customerMobileNo = $amountdetail['issues_contact_number'];
			$customerFName = $amountdetail['firstname'];
			db_query($sql);
		
			$username = $login;
			$amountToBeTaken = $amountToBePaid;
			$mail_detail = array(
					"to"=>'alrt_ebs_fraud_attempt@myntra.com',
					"subject"=>"Urgent! Possible user fraud for Orderid:$orderid",
					"content"=>"Based on payment gateway verification a suspect order has been placed by $customerFName  $customerMobileNo, orderID = $orderid.
					This order has been declined and will not be processed.
					Further Details are:
					Username: $username
					Amount to be paid: $amountToBeTaken",
					"from_name"=>'Myntra Admin',
					"from_email"=>'admin@myntra.com',
					"header"=>'',
					);
					if(!empty($couponValidationFailedMessage)){
					$mail_detail['content'].="
					Extra:$couponValidationFailedMessage";
			}
			send_mail_on_domain_check($mail_detail);
			$redirectURL = $https_location . "/mkpaymentoptions.php?transaction_status=N";
			
			/*$tracker->fireRequest();*/
			$responseObject['order'] = array();
			$responseObject['order']['orderstatus'] = $orderstatus;
			$responseObject['order']['message'] = $message;
			$responseObject['redirectURL'] = $redirectURL;
			$responseObject['servername'] = $server_name;
			$responseObject['status']['statusType'] = "SUCCESS";
		}
				
		if($status == 'failure') { //!$isPaymentSuccessfull
			$redirectURL = $https_location . "/mkpaymentoptions.php?transaction_status=N";
			$message = "We are sorry, your payment has not been successful.Please <a href='".$http_location."/mkpaymentoptions.php?pagetype=productdetail'>click here</a> to try again";
			$orderstatus = "N";
				
			$sql = "UPDATE ".$sql_tbl['orders']." SET tf_status ='TF',status='D',response_server='$server_name'  WHERE orderid = '".intval($orderid)."' ";  ### Update the table in case transaction get failed
			db_query($sql);
			$userName = $login;
			$amountdetail = $order_result[0];
			//unlock the coupon in case transaction is declined
			$couponCode = $amountdetail['coupon'];
			if(!empty($couponCode) && $prev_status != 'Q') {
				// Unlock the coupon.
				$adapter->unlockCouponForUser($couponCode, $userName);
			}
			$cashCouponCode=$amountdetail['cash_coupon_code'];
			if(!empty($cashCouponCode) && $prev_status != 'Q') {
			// Unlock the coupon.
				$adapter->unlockCouponForUser($cashCouponCode, $userName);
			}
				
			/*$tracker->fireRequest();*/
			$responseObject['order'] = array();
			$responseObject['order']['orderstatus'] = $orderstatus;
			$responseObject['order']['message'] = $message;
			$responseObject['redirectURL'] = $redirectURL;
			$responseObject['status']['statusType'] = "SUCCESS";
		}
		
		return $responseObject;
	}
	
    function getMFBHTML($requestData) {
        
        $data = $requestData;
        $MFBConfigArray = array();

        if ($data['feedbackName'] == 'order') {

                $MFBConfigArrayControl = array(
                    "CUSTOMER_EMAIL" => $data['login'],
                    "REFERENCE_ID" => $data['orderid'],
                    "REFERENCE_TYPE" => "ORDER", // SHIPMENT,ORDER,RETURN,SR
                    "FEEDBACK_NAME" => "order"
                );
                // A-B test variant		
                $MFBConfigArrayTest = array(
                    "CUSTOMER_EMAIL" => $data['login'],
                    "REFERENCE_ID" => $data['orderid'],
                    "REFERENCE_TYPE" => "SELECTION", // SHIPMENT,ORDER,RETURN,SR,SELECTION
                    "FEEDBACK_NAME" => "selection"
                );

            // get the A-B test veraint based on NPSratio algo
            $NPStestResult = "control"; // set the default A-B test result
            if (MABTestSegmentationAlgoFactory::getSegmentationAlgo("NPSRatio") != null) {
                $NPSSegment = MABTestSegmentationAlgoFactory::getSegmentationAlgo("NPSRatio")->generateSegment("NPSRatio");
                $NPStestResult = ResolverFactory::getFastResolver("NPSRatio")->resolve($NPSSegment);
            }

            // choose the MFB template based on A-B test
            if (strtolower($NPStestResult) == strtolower("control")) {
                $MFBConfigArray = $MFBConfigArrayControl;
            } else {
                $MFBConfigArray = $MFBConfigArrayTest;
            }
        } else {

            $MFBConfigArray = array(
                "CUSTOMER_EMAIL" => $data['login'],
                "REFERENCE_ID" => $data['orderid'],
                "REFERENCE_TYPE" => $data['referenceType'], // SHIPMENT,ORDER,RETURN,SR
                "FEEDBACK_NAME" => $data['feedbackName']
            );
        }
        $MFBInstanceObj = new MFBInstance();
        $MFBContent = $MFBInstanceObj->getMFBMailContent($MFBConfigArray);

        if ($MFBContent !== false) {
            //send mail with MFB content appended as (type-critical)
            $bodyArgs['MFB_FORM'] = htmlentities($MFBContent['MFBMailHTML']);
        	
        } else {
            $bodyArgs['MFB_FORM'] = "";
            $responseObject['status']['statusCode'] = 601;
            $responseObject['status']['statusType'] = "FAILURE";
        }
        $responseObject['status']['statusCode'] = 900;
        $responseObject['status']['statusType'] = "SUCCESS";
        $responseObject['data'] = $bodyArgs; // The MFB_FORM field of data has the html
        return $responseObject;
    }
    
    function getNps($requestData) {
    
    	$data = $requestData;
    	$MFBConfigArray = array();
    
    	if ($data['feedbackName'] == 'order') {
    
    		$MFBConfigArrayControl = array(
    				"CUSTOMER_EMAIL" => $data['login'],
    				"REFERENCE_ID" => $data['orderid'],
    				"REFERENCE_TYPE" => "ORDER", // SHIPMENT,ORDER,RETURN,SR
    				"FEEDBACK_NAME" => "order"
    		);
    		// A-B test variant
    		$MFBConfigArrayTest = array(
    				"CUSTOMER_EMAIL" => $data['login'],
    				"REFERENCE_ID" => $data['orderid'],
    				"REFERENCE_TYPE" => "SELECTION", // SHIPMENT,ORDER,RETURN,SR,SELECTION
    				"FEEDBACK_NAME" => "selection"
    		);
    
    		// get the A-B test veraint based on NPSratio algo
    		$NPStestResult = "control"; // set the default A-B test result
    		if (MABTestSegmentationAlgoFactory::getSegmentationAlgo("NPSRatio") != null) {
    			$NPSSegment = MABTestSegmentationAlgoFactory::getSegmentationAlgo("NPSRatio")->generateSegment("NPSRatio");
    			$NPStestResult = ResolverFactory::getFastResolver("NPSRatio")->resolve($NPSSegment);
    		}
    
    		// choose the MFB template based on A-B test
    		if (strtolower($NPStestResult) == strtolower("control")) {
    			$MFBConfigArray = $MFBConfigArrayControl;
    		} else {
    			$MFBConfigArray = $MFBConfigArrayTest;
    		}
    	} else {
    
    		$MFBConfigArray = array(
    				"CUSTOMER_EMAIL" => $data['login'],
    				"REFERENCE_ID" => $data['orderid'],
    				"REFERENCE_TYPE" => $data['referenceType'], // SHIPMENT,ORDER,RETURN,SR
    				"FEEDBACK_NAME" => $data['feedbackName']
    		);
    	}
    	$MFBInstanceObj = new MFBInstance();
    	$MFBContent = $MFBInstanceObj->getMFBMailContentJson($MFBConfigArray);
    
    	if ($MFBContent !== false) {
    		//send mail with MFB content appended as (type-critical)
    		// NPS detials from array
    		$bodyArgs = $MFBContent;
    		 
    	} else {
    		$bodyArgs = null;
    		$responseObject['status']['statusCode'] = 601;
    		$responseObject['status']['statusType'] = "FAILURE";
    	}
    	$responseObject['status']['statusCode'] = 900;
    	$responseObject['status']['statusType'] = "SUCCESS";
    	$responseObject['data'] = $bodyArgs; // The MFB_FORM field of data has the html
    	return $responseObject;
    }

    private function onhold_order($requestData){
		
		$adapter = CouponAdapter::getInstance();
		global $sql_tbl, $portalapilog, $server_name;
		
		// Initialization
		$portalapilog->info(print_r($requestData, true));
		$responseObject = array();
		$responseObject['status'] = array();
		$responseObject['status']['statusType'] = "FAILURE";
		
		$orderid = $requestData['orderid'];
		$order_info = func_query_first("select * from $sql_tbl[orders] where orderid = '".$orderid."'");
		
		if( ($order_info['status'] == 'PP' || $order_info['status'] == 'D') && $order_info['warehouseid'] == 0) {
			$productsInCart = func_create_array_products_of_order($orderid);
			$itemsAvailable = check_order_item_availability_atp($productsInCart);
			if (!$itemsAvailable) {
				$responseObject['status']['statusCode'] = "F1";
				$responseObject['status']['statusMessage'] = "NoInventory";
				return $responseObject;
			}
            //func_change_order_status($orderid, 'Q', "Payment Service Automation Script" ,"Queing order from Payments Service Automation script","",false);
            $updateSql = "UPDATE xcart_orders SET status='Q',queueddate='".time()."', response_server='".$server_name."', cashback_processed = 1 WHERE orderid = $orderid";
            db_query($updateSql);

            func_addComment_status($orderid, "Q", $order_info['status'], "Queing order from Payments Service Automation script.", "AUTOSYSTEM");

            $oh_reason = get_oh_reason_for_code("PPOH");
            EventCreationManager::pushOrderProcessEvent($orderid, array('comment' => "Queing order from Payments Service Automation script.", 
                'onHold' => 1, 'onHoldReasonId' => $oh_reason['id']));

            $responseObject['status']['statusType'] = "SUCCESS";
            $responseObject['status']['statusCode'] = "S1";
            $responseObject['status']['statusMessage'] = "Order put on Hold";

            return $responseObject;
		} else {
			$responseObject['status']['statusType'] = "FAILURE";
			$responseObject['status']['statusCode'] = "F2";
			$responseObject['status']['statusMessage'] = "Invalid order status";
			return $responseObject;
		}
	}	

	private function update_shipment_status($requestData){
		$order = $requestData['order'];
    	$responseObject = array(); 
    	$responseObject['status']['statusCode'] = 900;
    	$responseObject['status']['statusType'] = "SUCCESS";
    	global $portalapilog;
    	
    	$order = $requestData['order'];
    	if(!empty($order['orderId'])){
	    	if($order['forceUpdate']){
	    		if($order['newStatus'] == 'DL'){
					$res = OrderDataCorrectionManager::markOrderDelivered($order['orderId'], $order['currentStatus'],'LMS_ADMIN');
	    		}elseif($order['newStatus'] == 'FD'){
	    			$res = OrderDataCorrectionManager::markOrderShipped($order['orderId'], $order['currentStatus'],'LMS_ADMIN');
	    		}    	
	    		if($res['status']){
	    			$responseObject['order'] = array('orderId' => $order['orderId'],'status'=>'SUCCESS');
	    		}else{
					$responseObject['status']['statusCode'] = 901;
			        $responseObject['status']['statusType'] = "FAILED";
	    			$responseObject['order'] = array('orderId' => $order['orderId'],'status'=>'FAILED');
	    		}
	    	}else{
				$portalapilog->debug("feature will be implemented later");	
			}	
    	}else{
    		$responseObject['status']['statusCode'] = 901;
		    $responseObject['status']['statusType'] = "FAILED";
    		$responseObject['order'] = array('orderId' => $order['orderId'],'status'=>'FAILED');
    	}
    	
    	return $responseObject;
    			
	}

	private function resend_stale_release_data($requestData){
		// This api is created to re-send data to WMS for their stale releases.
		$responseObject = array();
		$releaseIds = explode(",", $requestData['releaseids']);
		$user = $requestData['loginid'];
		$responseObject['status'] = array();
		$response = \ReleaseApiClient::pushOrderReleaseToWMS($releaseIds,$user,'update');
		if($response === true){
			$responseObject['status']['statusCode'] = 600;
			$responseObject['status']['statusMessage'] = "Release data retrieved successfully";
			$responseObject['status']['statusType'] = "SUCCESS";
			return $responseObject;
		} else {
			$responseObject['status']['statusCode'] = 601;
			$responseObject['status']['statusMessage'] = "Release data retrieval failed - ".$response;
			$responseObject['status']['statusType'] = "FAILURE";
			return $responseObject;
		}
	}
	
	
	private function pushorderxmltoqueue($data){
		include_once("$xcart_dir/modules/amqpclients/producer/AMQPMessageProducer.php");
		$client = AMQPMessageProducer::getInstance();
		$resp = $client->sendMessage('orderEvents', array('orderData' => $data['orderCreationMessage']), 'orderCreationMessage','xml',true);
		$responseObject = array();
		$responseObject['status'] = array();
		$responseObject['status']['statusCode'] = 600;
		$responseObject['status']['statusMessage'] = $resp?"message sent successfully":"message sending failed";
		$responseObject['status']['statusType'] = "SUCCESS";
		return $responseObject;
	}
	
	private function evaluateOrderOnHoldRules($data) {
		$orderid = $data['orderRelease']['id'];
		
		$responseObject = array();
		$responseObject['status'] = array();
		$responseObject['data']['putOnHold'] = 0;
		
		$onholdVerificationRequired = false;
		$order = func_query_first("Select * from xcart_orders where orderid = $orderid");
		$response = check_order_onhold_rules($order['group_id'], $order['login'], $order['payment_method'], $order['channel'], $order['cash_redeemed'], false);
		if($response['status'] == 'OH') {
			if($response['verificationRequired'] == true) {
				$onholdVerificationRequired = true;
				$curr_time = date('Y-m-d H:i:s');
				$sql = "insert ignore into cod_oh_orders_dispostions(orderid,disposition,reason,trial_number,created_time,created_by) values ($order[group_id],'OH','".$response['sub_reason']."',0,'$curr_time','SYSTEM')";
				db_query($sql);
			}
	
			$oh_reason = get_oh_reason_for_code($response['reason'], $response['sub_reason']);
			$commentArrayToInsert = array("orderid" => $orderid, "commenttype" => "Automated", "commenttitle" => "status change", "commentaddedby" => "System", "description" => $response['description'], "commentdate" => time());
			func_array2insert("mk_ordercommentslog", $commentArrayToInsert);
			
			$responseObject['data']['putOnHold'] = 1;
			$responseObject['data']['onHoldReasonId'] = $oh_reason['id'];
			
		}
		$responseObject['data']['onHoldVerificationRequired'] = $onholdVerificationRequired?1:0;
		
		
		$responseObject['status']['statusCode'] = 600;
		$responseObject['status']['statusMessage'] = "Rules evaluated successfully";
		$responseObject['status']['statusType'] = "SUCCESS";
		
		return $responseObject;
	}
	
	private function sendOrderNotification($data) {
		$orderid = $data['notificationData']['orderId'];
		$status = $data['notificationData']['status'];
		$isOnHold = $data['notificationData']['isOnHold'];
		
		$mailstatus = false;
		switch($status) {
			case 'Q':
				if($isOnHold == 'false') {
					$mailstatus = OrderProcessingMailSender::sendOrderConfirmationNotification($orderid);
				} else {
					//$mailstatus = OrderProcessingMailSender::notifyCodOrderOnHold($orderid);
				}
				break;
			default:
				$responseObject['status']['statusCode'] = 901;
				$responseObject['status']['statusMessage'] = "No notification sender available for this request";
				$responseObject['status']['statusType'] = "FAILURE";
				return $responseObject;
		}
		
		if($mailstatus) {
			$responseObject['status']['statusCode'] = 600;
			$responseObject['status']['statusMessage'] = "Notification sent successfully";
			$responseObject['status']['statusType'] = "SUCCESS";
		} else {
			$responseObject['status']['statusCode'] = 900;
			$responseObject['status']['statusMessage'] = "Notification sending failed";
			$responseObject['status']['statusType'] = "FAILURE";
		}
		
		return $responseObject;
	}
	
	function getShippingMethod($data){
		$orderid = $data['orderRelease']['id']; 
		$warehouseid = $data['orderRelease']['warehouseId'];
		$orderStatus = $data['orderRelease']['status'];
		$courierService = $data['orderRelease']['courierService'];
		
		$order = func_query_first("select * from xcart_orders where orderid = $orderid");
		if(!$order){
			$order = func_query_first("select * from xcart_orders where group_id = $orderid");
		}
		
		$orderStatus = $order['status']=='OH'?'OH':$orderStatus;
		$shippingMethod = getShippingMethod($order,$warehouseid,$courierService,$orderStatus);
		if($shippingMethod == 'XPRESS') {
			$pickingTimeInHrs = FeatureGateKeyValuePairs::getFeatureGateValueForKey('xpressshipping.pickingtime');
			$qcTimeInHrs = FeatureGateKeyValuePairs::getFeatureGateValueForKey('xpressshipping.qctime');
		} else {
			$pickingTimeInHrs = FeatureGateKeyValuePairs::getFeatureGateValueForKey('normalshipping.pickingtime');
			$qcTimeInHrs = FeatureGateKeyValuePairs::getFeatureGateValueForKey('normalshipping.qctime');
		}

		$responseObject = array();
		$responseObject['status']['statusCode'] = 600;
		$responseObject['status']['statusMessage'] = "Shipping method returned successfully";
		$responseObject['status']['statusType'] = "SUCCESS";
		$responseObject['data']['orderRelease']['shippingMethod'] = $shippingMethod;
		$responseObject['data']['orderRelease']['pickingTime'] = $pickingTimeInHrs;
		$responseObject['data']['orderRelease']['qcTime'] = $qcTimeInHrs;
		
		return $responseObject;
	}

	function courierChange($data) {
		$orderId = $data['orderRelease']['id'];
		$courierService = $data['orderRelease']['courierService'];
		$trackingNumber = $data['orderRelease']['trackingNumber'];
                
		$response = func_update_courier_change($orderId, $courierService, $trackingNumber);
                
                $responseObject = array();
                $responseObject['status'] = array();
                
                if($response === true){
			$responseObject['status']['statusCode'] = 600;
			$responseObject['status']['statusMessage'] = "Portal updation for courier change - Success.";
			$responseObject['status']['statusType'] = "SUCCESS";
		} else {
			$responseObject['status']['statusCode'] = 601;
			$responseObject['status']['statusMessage'] = "Portal updation for courier change - Failure.";
			$responseObject['status']['statusType'] = "FAILURE";
		}
	        
	        return $responseObject;
	}

	function addOrderComment($data) {
		$orderId        = $data['actionHistory']['entityId'];
		$addedBy        = $data['actionHistory']['createdBy'];
		$commentType    = $data['actionHistory']['disposition'];
		$commentTitle   = $data['actionHistory']['actionCode'];
		$commentDetails = $data['actionHistory']['remarks'];

		func_addComment_order($orderId, $addedBy, $commentType, $commentTitle, $commentDetails);

                $responseObject = array();
                $responseObject['status'] = array();
                $responseObject['status']['statusCode'] = 600;
                $responseObject['status']['statusMessage'] = "Portal request to write order comment - Success.";
                $responseObject['status']['statusType'] = "SUCCESS";

	        return $responseObject;
	}

        function getOrderComments($data) {
            $orderId = $data['orderid'];
            $commentsql = "SELECT commentid,orderid,commenttype,commenttitle,description," .
                          "commentaddedby,from_unixtime(commentdate) AS commentdate " .
                          "FROM mk_ordercommentslog WHERE orderid = '$orderId' " .
                          "ORDER BY commentdate DESC";
            $commentList = func_query($commentsql);

            $responseObject = array();
            $responseObject['status'] = array();
            $responseObject['status']['statusCode'] = 600;
            $responseObject['status']['statusType'] = "SUCCESS";
            $responseObject['status']['statusMessage'] = "Successfully retrieved order comments";

            $responseObject['data'] = array();
            if ($commentList) {
                $actionHistory[] = array();
                foreach ($commentList as $order_comment_data) {
                    $data = array();
                    $data['actionHistory']['id'] = $order_comment_data['commentid'];
                    $data['actionHistory']['entityId'] = $order_comment_data['orderid'];
                    $data['actionHistory']['disposition'] = $order_comment_data['commenttype'];
                    $data['actionHistory']['actionCode'] = $order_comment_data['commenttitle'];
                    $data['actionHistory']['remarks'] = $order_comment_data['description'];
                    $data['actionHistory']['createdBy'] = $order_comment_data['commentaddedby'];
                    $data['actionHistory']['createdOn'] = $order_comment_data['commentdate'];
                    $actionHistory[]=$data;
                }
            }

            $responseObject['data'] = $actionHistory;

            return $responseObject;
        }

        private function getCartForOrderRequest($requestData) {
            $cartContext = $requestData['cartContext'];
            $mcartFactory = new MCartFactory();
            if (isGuestCheckout()) {
                return $mcartFactory->getCurrentUserCart(false, $cartContext);
            } else if (!strtolower($requestData['login'])) {
               return  $mcartFactory->getCartForLogin($requestData['login'], false, $cartContext);
            } else {
                return $mcartFactory->getCurrentUserCart(false, $cartContext);
            }        
        }
}

?>
