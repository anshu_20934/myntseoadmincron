<?php
include_once("$xcart_dir/include/class/class.feature_gate.keyvaluepair.php");
include_once("$xcart_dir/include/class/class.widget.keyvaluepair.php");
include_once("$xcart_dir/include/func/func.mkcore.php");
include_once("$xcart_dir/modules/discount/DiscountEngine.php");
class DiscountActions
{

	
    function handleRequest($request)
    {
    	global $weblog, $sql_tbl;
       		$requestData = $request->getRequestVars();
        	$action = strtolower($requestData['data']['action']);
            $weblog->info("DiscountActions: perform action $action");
    		switch($action) {
				case 'setdiscountrulechanged':
					return $this->setDiscountRuleChanged();
				default:
				$responseObject = array();
	            $responseObject['status'] = array();
	            $responseObject['status']['statusCode'] = 600;
	            $responseObject['status']['statusMessage'] = "No action supported";
	            $responseObject['status']['statusType'] = "SUCCESS";
	            return $responseObject;
			}
		
        return false;
    }
    
    private function setDiscountRuleChanged(){
    	$responseObject = array();
    	$ruleChangeSQL = "update mk_widget_key_value_pairs set value=value+1 WHERE `key` = 'discountEngine.change.revisionNumber'";
    	$results = db_query($ruleChangeSQL); 
		WidgetKeyValuePairs::refreshKeyValuePairsInCache();
		return $responseObject;
    }

    public static function OOSActionForDiscounts($skuIds,$action){
        if(!$action) return;
		$styleIdQuery = "select distinct style_id from mk_styles_options_skus_mapping where sku_id in ( ".implode(",", $skuIds)." )";
        $styleIds = func_query_column($styleIdQuery,'style_id');
        if(!empty($styleIds)) {
            $OOSStyleIds = array();
            if($action == 'enable') {
                $OOSStyleIds = $styleIds;
            } else {
                foreach($styleIds as $styleId) {
                    if(!func_is_style_instock($styleId)) {
                        $OOSStyleIds[] = $styleId;
                    }
                }
            }

            if (!empty($OOSStyleIds)) {
                DiscountEngine::OOSActionForDiscounts($OOSStyleIds, $action);
            }
        }
    }
}

?>
