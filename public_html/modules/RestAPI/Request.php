<?php

class Request
{
	private $request_vars;
	private $data;
	private $http_username;
	private $user_password;
	private $http_accept;
	private $method;
	private $content_type;

	public function __construct()
	{
		$this->request_vars		= array();
		$this->data				= '';
		$this->content_type		= (strpos($_SERVER['CONTENT_TYPE'], 'json')) ? 'json' : 'xml';
		$this->http_username	= $_SERVER['PHP_AUTH_USER'];
		$this->user_password    = $_SERVER['PHP_AUTH_PW'];
		$this->method			= 'get';
		$this->http_accept	= (strpos($_SERVER['HTTP_ACCEPT'], 'json')) ? 'json' : 'xml';
	}
	
	public function getRequestHandler() {
		return $this->request_vars['handler'];
	}
	
	public function setData($data)
	{
		$this->data = $data;
	}

	public function setMethod($method)
	{
		$this->method = $method;
	}

	public function setRequestVars($request_vars)
	{
		$this->request_vars = $request_vars;
	}
	
	public function getUsername() {
		return $this->http_username;		
	}
	
	public function getUserPassword() {
		return $this->user_password;
	}

	public function getData()
	{
		return $this->data;
	}

	public function getMethod()
	{
		return $this->method;
	}

	public function getHttpAccept()
	{
		return $this->http_accept;
	}

	public function getRequestVars()
	{
		return $this->request_vars;
	}
	
	public function getContentType(){
		return $this->content_type;
	}
}

?>