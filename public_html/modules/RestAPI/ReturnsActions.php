<?php
include_once("$xcart_dir/include/class/oms/ReturnsStatusProcessor.php");
include_once("$xcart_dir/include/func/func.returns.php");

class ReturnsActions{
	
    function handleRequest($request){
    	global $weblog, $sql_tbl;
       		$requestData = $request->getRequestVars();
        	$action = $requestData['data']['action'];
 			switch($action) {
				case 'pickupStatusUpdate':
					return $this->processReturnPickupStatusUpdate($requestData['data']);
				case 'returnonhold':
					return $this->pickupOnHold($requestData['data']);
				case 'onholdreturnapproved':
					return $this->pickupOnHoldApproved($requestData['data']);
				case 'onholdreturnrejected':
					return $this->pickupOnHoldRejected($requestData['data']);
				case 'getreturnsfororder':
					return $this->getReturnsForOrder($requestData['data']);
				case 'createreturnsfororder':
					return $this->createReturnForOrder($requestData['data']);
				case 'getreturnsreasons':
					return $this->getReturnsReasons();
                case 'getrefundamount':
                    return $this->getRefundAmount($requestData['data']);
				default:
					$responseObject = array();
		            $responseObject['status'] = array();
		            $responseObject['status']['statusCode'] = 600;
		            $responseObject['status']['statusMessage'] = "No action defined for $action";
		            $responseObject['status']['statusType'] = "SUCCESS";
	            	return $responseObject;
		}
		
        return false;
    }


	function getReturnsReasons() {
		$responseDetails = array();
		$returnReasons = get_all_return_reasons(true);

		if($returnReasons){
			foreach($returnReasons as $returnReason)
				$responseDetails[]['return_reasons'] = $returnReason;
			$responseDetails['status']['statusCode'] = 900;
			$responseDetails['status']['statusType'] = "SUCCESS";
		} else {
			$responseDetails['status']['statusCode'] = 500;
			$responseDetails['status']['statusType'] = "ERROR";
			$responseDetails['status']['statusMessage'] = "Unable to retrieve returns reasons";
		}

		return $responseDetails;
	}

	function getReturnsForOrder($data) {
		$orderid = $data['orderid'];
		$returnid = $data['returnid'];
		$login = $data['customerLogin'];
		$returnDetails = array();
		$responseDetails = array();

		if($orderid){
			$returnDetails = get_return_request_for_order($orderid);
		} else if($returnid){
			$returnDetails = get_return_request_for_return($returnid);
		} else if($login){
			$returnDetails = get_basic_return_request_for_user($login);
		}
		if($returnDetails){
			foreach($returnDetails as $return) {
                $return['status_code'] = $return['status'];
                $responseDetails[]['return_details'] = $return;
                $responseDetails['returnid'] = $return['returnid'];
            }
			$responseDetails['status']['statusCode'] = 900;
			$responseDetails['status']['statusType'] = "SUCCESS";
		} else {
			$responseDetails['status']['statusCode'] = 500;
			$responseDetails['status']['statusType'] = "ERROR";
			$responseDetails['status']['statusMessage'] = "No returns found for given order";
		}

		return $responseDetails;
	}

	function createReturnForOrder($data) {
		$responseDetails = array();
		$orderid = $data['orderid'];
		$itemid = $data['itemid'];
		$option = $data['itemOption'];
		$quantity = $data['quantity'];
		$customer_login = $data['customerLogin'];
		$userid = $data['userid'];
		$reasoncode = $data['returnReason'];
		$pickup_courier = $data['pickupCourier'];
		if(!$pickup_courier){
			$pickup_courier = $data['pickupBy'];
		}
		$details = mysql_real_escape_string($data['returnDetails']);
		$return_mode = $data['returnMethod'];
		$selected_address_id = $data['selectedAddressId'];
		$refundMode = $data['refundMode'];
		$refundNeftAccountId = $data['refundNeftAccountId'];

		$response = func_add_new_return_request($orderid, $itemid, $option, $quantity, $customer_login, $userid,
			$reasoncode, $details, $return_mode, $selected_address_id, false, $pickup_courier, false, false,
			$refundMode, $refundNeftAccountId,true);

		if($response){
			$returnDetails = $response['return_req'];
			if((!$returnDetails || !$returnDetails['returnid']) && $response['returnid']){
				$returnDetails['returnid'] = $response['returnid'];
                $returnDetails['status_code'] = $returnDetails['status'];
				$response['return_req']['return_details'] = $returnDetails;
			}
			$responseDetails['return_details'] = $response['return_req'];
			$responseDetails['return_details']['statusCode'] = $responseDetails['return_details']['status'];
            $responseDetails['status']['statusCode'] = 900;
			$responseDetails['status']['statusType'] = "SUCCESS";
			$responseDetails['returnid'] = $response['returnid'];
			$responseDetails['return_address'] = $response['return_address'];
		} else {
			$responseDetails['status']['statusCode'] = 500;
			$responseDetails['status']['statusType'] = "ERROR";
			$responseDetails['status']['statusMessage'] = "No returns found for given order";
		}

		return $responseDetails;
	}

    function getRefundAmount($data) {
        $responseDetails = array();
        $itemid = $data['itemid'];
        $quantity = $data['quantity'];
        $return_mode = $data['returnMethod'];

        $response = func_calculate_refund_amount($itemid,$quantity,$return_mode);

        if($response){
            $responseDetails['status']['statusCode'] = 900;
            $responseDetails['status']['statusType'] = "SUCCESS";
            $responseDetails['refundAmount'] = $response['refundAmount'];
            $responseDetails['cashbackRedeemed'] = $response['cashbackRedeemed'];
        } else {
            $responseDetails['status']['statusCode'] = 500;
            $responseDetails['status']['statusType'] = "ERROR";
            $responseDetails['status']['statusMessage'] = "No returns found for given order";
        }

        return $responseDetails;
    }


	private function processReturnPickupStatusUpdate($requestData){
    	$processor = new ReturnsStatusProcessor();
    	global $weblog;
    	$responseObject = array();
    	$responseObject['status']['statusCode'] = 900;
    	$responseObject['status']['statusType'] = "SUCCESS";
    	$returns = $requestData['returns']['return'];
    	if(isset($returns['id'])){
    		$returns = array($returns);
    	}
    	foreach ($returns as $return){
    		$comment = " -- reason :: ".$return['reason']." -- remark ::::".$return['remark'];
    		if($return['status'] == 'SUCCESS'){
	    		$status_change_response = $processor->changeReturnsStatus(trim($return['id']), 'RPU', 'ML', '', '', '', 'LMS_API','Marking return as pickedup upon LMS successful pickup'.$comment);
				if($status_change_response['status']){
					$responseObject['returns'][] = array('returnid'=>$return['id'],'status'=>'SUCCESS');
				}else{
					$responseObject['returns'][] = array('returnid'=>$return['id'],'status'=>'FAILED','reason'=>$status_change_response['reason']);
				}
    		}elseif($return['status'] == 'REJECT' ){
    			$status_change_response = $processor->processReturnPickupFailure($return['id'],$comment);
    			if($status_change_response['status']){
					$responseObject['returns'][] = array('returnid'=>$return['id'],'status'=>'SUCCESS');
				}else{
					$responseObject['returns'][] = array('returnid'=>$return['id'],'status'=>'FAILED','reason'=>$status_change_response['reason']);
				}
    		}
            elseif($return['status'] == 'REQUE_NOT_ALLOWED'){
                $status_change_response = $processor->processReturnPickupFailure($return['id'],$comment, true);
                if($status_change_response['status']){
                    $responseObject['returns'][] = array('returnid'=>$return['id'],'status'=>'SUCCESS');
                }else{
                    $responseObject['returns'][] = array('returnid'=>$return['id'],'status'=>'FAILED','reason'=>$status_change_response['reason']);
                }
            }
            else{
    			$responseObject['returns'][] = array('returnid'=>$return['id'],'status'=>'SUCCESS');
    			$status_change_response['status'] = true;
    		}
	    	$time = strtotime("".$return['pickupdate']);
		    $myDate = date( 'jS F Y', $time );
		    if($status_change_response['code'] != 'ALREADY_IN_RRD' && $status_change_response['status']){
		    	$processor->sendPickupStatusUpdateMail($return['id'],$return['status'],$myDate,$return['reason']);	
		    }
    	}
    	return $responseObject;
    }

    function pickupOnHold($data) {
    	$returnId = $data['returnId'];
    	$responseObject = array();
    	$responseObject['status'] = array();
    	$response = send_return_onhold_mail($returnId, "return_onhold_email", $devSetup);
    	if($response === true){
    		$responseObject['status']['statusCode'] = 200;
    		$responseObject['status']['statusMessage'] = "Email sent successfully";
    		$responseObject['status']['statusType'] = "SUCCESS";
    	} else {
    		$responseObject['status']['statusCode'] = 201;
    		$responseObject['status']['statusMessage'] = "Email sending failed";
    		$responseObject['status']['statusType'] = "FAILURE";
    	}
    		
    	return $responseObject;
    }
    
    function pickupOnHoldApproved($data){
    	$returnId = $data['returnId'];
    	$responseObject = array();
    	$responseObject['status'] = array();
    	$response = send_return_onhold_mail($returnId, "return_onhold_approved_email", $devSetup);
    	if($response === true){
    		$responseObject['status']['statusCode'] = 200;
    		$responseObject['status']['statusMessage'] = "Email sent successfully";
    		$responseObject['status']['statusType'] = "SUCCESS";
    	} else {
    		$responseObject['status']['statusCode'] = 201;
    		$responseObject['status']['statusMessage'] = "Email sending failed";
    		$responseObject['status']['statusType'] = "FAILURE";
    	}
    	
    	return $responseObject;
    }
    
    function pickupOnHoldRejected($data){
    	$returnId = $data['returnId'];
    	$responseObject = array();
    	$responseObject['status'] = array();
    	$response = send_return_onhold_mail($returnId, "return_onhold_rejected_email", $devSetup);
    	if($response === true){
    		$responseObject['status']['statusCode'] = 200;
    		$responseObject['status']['statusMessage'] = "Email sent successfully";
    		$responseObject['status']['statusType'] = "SUCCESS";
    	} else {
    		$responseObject['status']['statusCode'] = 201;
    		$responseObject['status']['statusMessage'] = "Email sending failed";
    		$responseObject['status']['statusType'] = "FAILURE";
    	}
    	 
    	return $responseObject;
    }
}
?>
