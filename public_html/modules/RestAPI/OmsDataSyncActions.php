<?php
/*
 * Created on 14-Mar-2013
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 
 class OmsDataSyncActions{
    
    function handleRequest($request)    {
    	global $portalapilog, $sql_tbl;        
       		$requestData = $request->getRequestVars();
        	$action = strtolower($requestData['data']['action']);
        	$userId = $requestData['data']['userId'];
        	$releaseId = trim($requestData['data']['orderRelease']['id']);
            $portalapilog->debug("OmsDataSyncActions:debug:begin");
            $portalapilog->info("OmsDataSyncActions: perform action $action");
    		switch($action) {
				case 'packorder':
					return $this->packRelease($releaseId,$userId);
				default:
				$responseObject = array();
	            $responseObject['status'] = array();
	            $responseObject['status']['statusCode'] = 600;
	            $responseObject['status']['statusMessage'] = "No action supported";
	            $responseObject['status']['statusType'] = "SUCCESS";
	            return $responseObject;
			}
		
        return false;
    }
    
    function packRelease($releaseId,$userId){
       	$currentTime = time();
    	$result =false;
        $db_query_retries=3;
        do{
        $result = db_query("update xcart_orders set status = 'PK',packeddate = $currentTime where orderid = $releaseId");
        }while (--$db_query_retries >0 && $result===false);
        func_addComment_status($releaseId, 'PK', 'WP', "marking as pk from wp", $userId);
		$responseObject = array();
        $responseObject['status'] = array();
        $responseObject['status']['statusCode'] = 200;
        $responseObject['status']['statusMessage'] = "marked order pk successfully";
        $responseObject['status']['statusType'] = "SUCCESS";
        return $responseObject;
    }
    
 }
?>
