<?php 

include_once ("$xcart_dir/modules/RestAPI/RestRequest.php");

class CatalogServiceClient {

    public static function clearCache($styles) {
        $url = HostConfig::$catalogServiceHost.'/product/clearcacheinbulk';
        $returnObject=null;
        $headers = array();
        array_push($headers, 'Accept:  application/json');
        array_push($headers,'Content-Type: application/xml');
        array_push($headers,'Authorization: Basic YXBpYWRtaW46bTFudHJhUjBja2V0MTMhIw==');
        $request = new RestRequest($url, "POST");
        $request->setHttpHeaders($headers);
        $stylesStrArr = array("<listEntry>");
        foreach ($styles as $eachStyle) {
        	array_push($stylesStrArr, "<list>".$eachStyle."</list>");
        }
        array_push($stylesStrArr, "</listEntry>");
        $body = implode("\n", $stylesStrArr);
        $request->buildPostBody($body);
        
        $request->execute();
        $responseInfo = $request->getResponseInfo();
        $responseBody = $request->getResponseBody();
        $partialReturnObject = json_decode($responseBody);
        if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
            global $portalapilog;
        	$portalapilog->debug('catalogServiceClient - clear cache:--'.print_r($request,true));
        }
            
        if(empty($returnObject)) {
            $returnObject = $partialReturnObject;   
        } else {
            foreach ($partialReturnObject as $key=>$value) {
                $returnObject->$key = $value;
            }
        }
        return $returnObject; 
	}
}
