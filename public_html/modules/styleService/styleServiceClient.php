<?php 

include_once ("$xcart_dir/modules/RestAPI/RestRequest.php");

class styleServiceClient {

    public static function reindex_bulk_service_call($stylesToAdd) {
        $url = HostConfig::$styleServieUrl.'/index/';
        $returnObject=null;
        //if(!is_array($stylesToAdd)) $stylesToAdd= array();
        $headers = array();
        array_push($headers, 'Accept:  application/json');
        array_push($headers,'Content-Type: application/json');
        $request = new RestRequest($url, "POST",json_encode($stylesToAdd));
        $request->setHttpHeaders($headers);
        $request->execute();
        $responseInfo = $request->getResponseInfo();
        $responseBody = $request->getResponseBody();
        $partialReturnObject = json_decode($responseBody);
        if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
            global $portalapilog;
        	$portalapilog->debug('styleServiceClient - portalrequesthandler - refresh style  call:--'.print_r($request,true));
        }
            
        if(empty($returnObject)) {
            $returnObject = $partialReturnObject;   
        } else {
            foreach ($partialReturnObject as $key=>$value) {
                $returnObject->$key = $value;
            }
        }
        return $returnObject; 
	}

    public static function reindex_single_service_call($styleId) {
        $url = HostConfig::$styleServieUrl.'/index/'.$styleId;
        $returnObject=null;
        //if(!is_array($stylesToAdd)) $stylesToAdd= array();
        $headers = array();
        array_push($headers, 'Accept:  application/json');
        array_push($headers,'Content-Type: application/json');
        $request = new RestRequest($url);
        $request->setHttpHeaders($headers);
        $request->execute();
        $responseInfo = $request->getResponseInfo();
        $responseBody = $request->getResponseBody();
        $partialReturnObject = json_decode($responseBody);
        if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
            global $$drelog;
        	$drelog->debug('styleServiceClient - reindex_single_service_call:--'.print_r($request,true));
        }
        if(empty($returnObject)) {
            $returnObject = $partialReturnObject;   
        } else {
            foreach ($partialReturnObject as $key=>$value) {
                $returnObject->$key = $value;
            }
        }
        return $returnObject; 
	}
}
