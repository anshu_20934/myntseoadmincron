<?php 

include_once ("$xcart_dir/modules/RestAPI/RestRequest.php");

class PortalRequestHandlerClient {

	public static function refreshStyles($styles){
		if(empty($styles)) return;
		
		$url = \HostConfig::$baseUrl . "/RequestHandler.php" ;
		$returnObject=null;
		$headers = array();
		array_push($headers,'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'Authorization: Basic YXBpYWRtaW46bTFudHJhUjBja2V0MTMhIw==');
		$request = new RestRequest($url,'POST');
		$request->setHttpHeaders($headers);
		$stylesStrArr = array();
		foreach ($styles as $eachStyle) {
			array_push($stylesStrArr, "{\"id\":\"$eachStyle\"}");
		}
		$stylesStr = implode(",", $stylesStrArr);
		$body = "{
		                        \"data\": {
		                                \"action\": \"refreshstylecache\",
		                                \"styles\": {
		                                        \"style\":[$stylesStr]
		                                }
		                        },
		                        \"handler\": \"Style\"
		                }";
		$request->buildPostBody($body);
		
		$request->execute();
		$responseInfo = $request->getResponseInfo();
		$responseBody = $request->getResponseBody();
		
		$partialReturnObject = json_decode($responseBody);
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
			global $drelog;
			$drelog->debug('reindex_single_service_call:--'.print_r($request,true));
		}
		if(empty($returnObject)) {
			$returnObject = $partialReturnObject;
		} else {
			foreach ($partialReturnObject as $key=>$value) {
				$returnObject->$key = $value;
			}
		}
		return $returnObject;
	} 
}
