<?php

include "$xcart_dir/modules/coupon/exception/CouponNotActiveException.php";
include "$xcart_dir/modules/coupon/exception/CouponExpiredException.php";
include "$xcart_dir/modules/coupon/exception/CouponNotStartedException.php";
include "$xcart_dir/modules/coupon/exception/CouponUsageExpiredException.php";
include "$xcart_dir/modules/coupon/exception/CouponLockedException.php";
include "$xcart_dir/modules/coupon/exception/CouponUsageExpiredForUserException.php";
include "$xcart_dir/modules/coupon/exception/CouponLockedForUserException.php";
include "$xcart_dir/modules/coupon/exception/CouponNotValidForUserException.php";
include "$xcart_dir/modules/coupon/exception/CouponChannelException.php";
include "$xcart_dir/modules/coupon/exception/CouponUserNotLoggedInException.php";
include "$xcart_dir/modules/coupon/exception/CouponUserMobileNotVerifiedException.php";
include "$xcart_dir/modules/coupon/exception/CouponGroupNameNotValidException.php";
include_once "$xcart_dir/modules/coupon/mrp/SmsVerifier.php";

/**
 * The validator to check whether a coupon is valid, whether it is valid for a 
 * particular user, whether it is applicable on any products in the cart.
 * (Singleton)
 * 
 * @author Rohan.Railkar
 */
class CouponValidator
{
	/**
	 * The singleton instance.
	 */
	private static $instance = NULL;
	
	/**
	 * The CouponAdapter to access database information.
	 */
	private $adapter;
	
	/**
	 * @return CouponValidator The singleton instance.
	 */
	public static function getInstance()
	{
		if (CouponValidator::$instance == NULL) {
			CouponValidator::$instance = new CouponValidator();
		}
		
		return CouponValidator::$instance;
	}
	
	/**
	 * Constructor.
	 */
	private function __construct()
	{
		$this->adapter = CouponAdapter::getInstance();
	}
	
	/**
	 * @param Coupon $couponCode
	 * 
	 * @return boolean Whether the coupon exists and is not expired.
	 * 
	 * @throws CouponNotActiveException
	 * @throws CouponExpiredException
	 * @throws CouponNotStartedException
	 * @throws CouponUsageExpiredException
	 * @throws CouponLockedException
	 */
	public function isCouponValid(Coupon $coupon, $validateCouponGroup=false)
	{
	    $couponCode = $coupon->getCode();
		// Check validity of coupon group (cashback should not be used as coupon)
		if ($validateCouponGroup && ($coupon->getGroupName() == 'CASHBACK')) {
			throw new CouponGroupNameNotValidException($couponCode);										
		}
	    
	    // Check if coupon is disabled.
	    if (!$coupon->isActive()) {
	        throw new CouponNotActiveException($couponCode);
	    }
	    
		// Check if coupon is expired.
		if ($coupon->isExpired()) {
			throw new CouponExpiredException($couponCode,
											 $coupon->getEndDate());
		}
		
		// Check if the coupon is yet to start.
		if ($coupon->isNotStarted()) {
			throw new CouponNotStartedException($couponCode,
												$coupon->getStartDate(),
												$coupon->getEndDate());
		}
		
		// Check if the coupon usage is expired.
		if ($coupon->isUsageExpired()) {
			throw new CouponUsageExpiredException($couponCode);
		}
		
		// Check if the coupon is locked.
		if (!$coupon->isFree()) {
			throw new CouponLockedException($couponCode);
		}
		
		// Check if the coupon is allowed for either the channel or  device
		if($coupon->isCouponDeviceTagged()){
			if (!$coupon->isDeviceAllowed()){
				throw new CouponChannelException($couponCode);
			}
		}
		else{
			if (!$coupon->isChannelAllowed()){
				throw new CouponChannelException($couponCode);
			}
		}

		
		return TRUE;
	}
	
	/**
	 * @param Coupon $coupon
	 * @param string $userName
	 * 
	 * @return boolean Whether the coupon is valid and free for the user.
	 * 
	 * @throws CouponExpiredException
	 * @throws CouponNotStartedException
	 * @throws CouponUsageExpiredException
	 * @throws CouponLockedException
	 * @throws CouponUsageExpiredForUserException
	 * @throws CouponNotActiveException
	 * @throws CouponLockedForUserException
	 * @throws CouponNotValidForUserException
	 * @throws CouponUserNotLoggedInException
	 * @throws CouponUserMobileNotVerifiedException
	 */
	public function isCouponValidForUser(Coupon $coupon, $userName, $validateCouponGroup = false)
	{
	    // Check coupon's validity.
		if (!$this->isCouponValid($coupon, $validateCouponGroup)) {
		    return FALSE;
		}
		// Check whether the user has reached the usage limit.
		if ($coupon->isUsageExpiredForUser($userName)) {
		    throw new CouponUsageExpiredForUserException($coupon->getCode(),
		                                                 $userName);
		}
		
		// Check if the user can acquire a lock on the coupon.
		if (!$coupon->isFreeForUser($userName)) {
		    throw new CouponLockedForUserException($coupon->getCode(),
		                                           $userName);
		}
		
		// Check if user is allowed.
		if (!$coupon->isAllowedForUser($userName)) {
		    if (empty($userName)) {
		        throw new CouponUserNotLoggedInException($coupon->getCode());
		    }
		    else {
		        throw new CouponNotValidForUserException($coupon->getCode(),
		                                                 $userName);
		    }
		}
		
		// Check if the user's mobile is verified for MRP coupons.
		/*if (in_array($coupon->getGroupName(), array('FirstLogin', 'MrpReferrals'))) {
		    $result = func_query_first("SELECT mobile FROM xcart_customers WHERE login = '$userName';");
		    $mobile = $result['mobile'];
		    
		    // Get the sms verification status.
		    $smsVerifier = SmsVerifier::getInstance();
		    if ($smsVerifier->getStatus($mobile, $userName) != 'V') {
		        throw new CouponUserMobileNotVerifiedException($coupon->getCode(), $userName, $mobile);
		    }
		}*/
		
		return TRUE;
	}
	
	
	/**
	 * /**
	 * @param Coupon $coupon
	 * @param string $userName
	 * 
	 * @return boolean Whether the coupon allowed for use. The function does not check for coupon lock
	 * 
	 * @throws CouponExpiredException
	 * @throws CouponNotStartedException
	 * @throws CouponUsageExpiredException
	 * @throws CouponLockedException
	 * @throws CouponUsageExpiredForUserException
	 * @throws CouponNotActiveException
	 * @throws CouponLockedForUserException
	 * @throws CouponNotValidForUserException
	 * @throws CouponUserNotLoggedInException
	 * @throws CouponUserMobileNotVerifiedException
	 */
	 
	public function isCouponAllowedForUse(Coupon $coupon, $userName){
	  	$couponCode = $coupon->getCode();
	    
	    // Check if coupon is disabled.
	    if (!$coupon->isActive()) {
	        throw new CouponNotActiveException($couponCode);
	    }
	    
		// Check if coupon is expired.
		if ($coupon->isExpired()) {
			throw new CouponExpiredException($couponCode,
											 $coupon->getEndDate());
		}
		
		// Check if the coupon is yet to start.
		if ($coupon->isNotStarted()) {
			throw new CouponNotStartedException($couponCode,
												$coupon->getStartDate(),
												$coupon->getEndDate());
		}
		
		// Check if the coupon usage is expired.
		if ($coupon->isUsageExpired()) {
			throw new CouponUsageExpiredException($couponCode);
		}
		
		
		// Check if the coupon is allowed for the channel.
		if (!$coupon->isChannelAllowed()) {
		    throw new CouponChannelException($couponCode);
		}
		
		// Check whether the user has reached the usage limit.
		if ($coupon->isUsageExpiredForUser($userName)) {
			throw new CouponUsageExpiredForUserException($coupon->getCode(),
			$userName);
		}
		
				
		// Check if user is allowed.
		if (!$coupon->isAllowedForUser($userName)) {
			if (empty($userName)) {
				throw new CouponUserNotLoggedInException($coupon->getCode());
			}
			else {
				throw new CouponNotValidForUserException($coupon->getCode(),
				$userName);
			}
		}
		
		// Check if the user's mobile is verified for MRP coupons.
		if (in_array($coupon->getGroupName(), array('FirstLogin', 'MrpReferrals'))) {
			$result = func_query_first("SELECT mobile FROM xcart_customers WHERE login = '$userName';");
			$mobile = $result['mobile'];
		
			// Get the sms verification status.
			$smsVerifier = SmsVerifier::getInstance();
			if ($smsVerifier->getStatus($mobile, $userName) != 'V') {
				throw new CouponUserMobileNotVerifiedException($coupon->getCode(), $userName, $mobile);
			}
		}
		
		return TRUE;
	}
}
