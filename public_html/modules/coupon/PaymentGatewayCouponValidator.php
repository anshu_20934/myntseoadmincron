<?php
require_once "$xcart_dir/auth.php";
require_once $xcart_dir."/modules/coupon/database/CouponAdapter.php";
include_once ("$xcart_dir/modules/RestAPI/RestRequest.php");

/**
 * The validator to check whether a payment gateway coupon is valid, whether it is valid for a
 * particular bin number,
 * (Singleton)
 *
 * @author Kishore
 */
class PaymentGatewayCouponValidator
{

    public static function isCouponValidForBin($binNumber,$sessionId)
    {
        global $XCART_SESSION_VARS;

        $url =  HostConfig::$portalAbsolutServiceInternalHost."/coupon/isvalidbin/default?bin=".$binNumber;
        $headers = array('Accept: application/json', 'Accept-Language: en-US','Content-Type:application/json','X-CSRF-TOKEN:' . $XCART_SESSION_VARS['USER_TOKEN'],"Cookie: ".HostConfig::$cookieprefix."xid=$sessionId;");

        $restRequest = new RestRequest($url);
        $restRequest ->setHttpHeaders($headers);
        $restRequest ->execute();
        $responseInfo = $restRequest ->getResponseInfo();
        $responseBody = $restRequest ->getResponseBody();

        if(empty($responseBody)){
            return false;
        }

        $returnObject = json_decode($responseBody,true);

        return ($returnObject["applicability"] && $returnObject["pgCouponApplied"]) || (!$returnObject["pgCouponApplied"]);
    }

}
