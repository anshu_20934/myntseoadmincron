<?php

include_once ("$xcart_dir/include/func/func.utilities.php");
include_once ("$xcart_dir/include/func/func.mkcore.php");

/**
 * Coupon Data Structure. It reflects the database fields in the table for 
 * coupons.
 * 
 * @author Rohan.Railkar
 */
class Coupon
{
    /**
     * The coupon code
     */
    private $couponCode;
    
    private $groupName = '';
    private $channel = '';
    private $applicable_device='';
    
    /**
     * Usage related metadata
     * 
     * Note: By default [maxUsagePerByUser = maxUsage]
     */
    private $startDate = 0;
    private $endDate = 0;
    private $maxUsage = 0;
    private $isInfinite = FALSE;
    private $maxUsagePerCart = -1;
	private $timesUsed = 0;
	private $timesLocked = 0;
	private $maxUsageByUser = 0;
	private $isInfinitePerUser = FALSE;
	
	/**
	 * Upper and lower limit for coupon redemption
	 */
	private $minAmount = 0.0;
	private $maxAmount = 0.0;
	
	private $minCount = 0;
	private $maxCount = 0;
	
	/**
	 * Who are allowed to use the coupon
	 */
	private $users = '';
	private $excludeUsers = '';
	
	/**
	 * Offer the coupon discount only when the user makes payment through 
	 * selected avenue(s).
	 */
	private $paymentMethod = '';
	
	/**
	 * Coupon creation related data
	 */
	private $timestamp = 0;
	private $creator = '';
	private $comment = '';
	
	/**
	 * How much is the discount
	 */
	private $type = '';
	private $MRPAmount = 0.0;
	private $MRPpercentage = 0.0;
	private $freeShipping = FALSE;
	
	/**
	 * Whether the coupon is currently active
	 */
	private $isActive = TRUE;
	
	/**
	 * Coupon applicability
	 */
	private $productTypeIds = '';
	private $excludeProductTypeIds = '';
	private $styleIds = '';
	private $excludeStyleIds = '';
	private $catStyleIds = '';
	private $excludeCatStyleIds = '';
	private $categoryIds = '';
	private $excludeCategoryIds = '';
	private $SKUs = '';
	private $excludeSKUs = '';
    private $groupType = '';
    
	/**
	 * Elaborate constructor.
	 */
    public function __construct($couponCode,
                                $groupName,
                                $channel,
                                $startDate,
                                $endDate,
                                $maxUsage,
                                $isInfinite,
                                $maxUsagePerCart,
                                $timesUsed,
                                $timesLocked,
                                $maxUsageByUser,
                                $isInfinitePerUser,
                                $minAmount,
                                $maxAmount,
                                $users,
                                $excludeUsers,
                                $paymentMethod,
                                $timestamp,
                                $creator,
                                $comment,
                                $type,
                                $MRPAmount,
                                $MRPpercentage,
                                $freeShipping,
                                $isActive,
                                $productTypeIds,
                                $excludeProductTypeIds,
                                $styleIds,
                                $excludeStyleIds,
                                $catStyleIds,
                                $excludeCatStyleIds,
                                $categoryIds,
                                $excludeCategoryIds,
                                $SKUs,
                                $excludeSKUs,
					    		$minCount=0,
					    		$maxCount=0,
                                $groupType='',
                                $applicable_device=null)
    {
        $this->couponCode = $couponCode;
        $this->groupName = $groupName;
        $this->channel = $channel;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->maxUsage = $maxUsage;
        $this->isInfinite = $isInfinite;
        $this->maxUsagePerCart = $maxUsagePerCart;
        $this->timesUsed = $timesUsed;
        $this->timesLocked = $timesLocked;
        $this->maxUsageByUser = $maxUsageByUser;
        $this->isInfinitePerUser = $isInfinitePerUser;
        $this->minAmount = $minAmount;
        $this->maxAmount = $maxAmount;
        $this->minCount = $minCount;
        $this->maxCount = $maxCount;
        $this->users = $users;
        $this->excludeUsers = $excludeUsers;
        $this->paymentMethod = $paymentMethod;
        $this->timestamp = $timestamp;
        $this->creator = $creator;
        $this->comment = $comment;
        $this->type = $type;
        $this->MRPAmount = $MRPAmount;
        $this->MRPpercentage = $MRPpercentage;
        $this->freeShipping = $freeShipping;
        $this->isActive = $isActive;
        $this->productTypeIds = $productTypeIds;
        $this->excludeProductTypeIds = $excludeProductTypeIds;
        $this->styleIds = $styleIds;
        $this->excludeStyleIds = $excludeStyleIds;
        $this->catStyleIds = $catStyleIds;
        $this->excludeCatStyleIds = $excludeCatStyleIds;
        $this->categoryIds = $categoryIds;
        $this->excludeCategoryIds = $excludeCategoryIds;
        $this->SKUs = $SKUs;
        $this->excludeSKUs = $excludeSKUs;
        $this->groupType = $groupType;
        $this->applicable_device=$applicable_device;
    }
    
    /**
     * Getter for coupon code.
     */
    public function getCode()
    {
        return $this->couponCode;
    }
    
    public function getTimesUsed()
    {
        return $this->timesUsed;
    }
    /**
     * Getter for max usage per cart.
     */
    public function getMaxUsagePerCart()
    {
        return $this->maxUsagePerCart;
    }
    
    /**
     * Getter for product type IDs
     */
    public function getPTIds()
    {
        return $this->productTypeIds;
    }
    
    /**
     * Getter for product style IDs
     */
    public function getPSIds()
    {
        return $this->catStyleIds . $this->styleIds;
    }
    
    /**
     * Getter for categories.
     */
    public function getCategories()
    {
        return $this->categoryIds;
    }
    
    /**
     * Getter for product IDs
     */
    public function getSKUs()
    {
        return $this->SKUs;
    }
    
    /**
     * Getter for excluded product type IDs
     */
    public function getExclPTIds()
    {
        return $this->excludeProductTypeIds;
    }
    
    /**
     * Getter for excluded product style IDs
     */
    public function getExclPSIds()
    {
        return $this->excludeCatStyleIds . $this->excludeStyleIds;
    }
    
    /**
     * Getter for excluded categories.
     */
    public function getExclCategories()
    {
        return $this->excludeCategoryIds;
    }
    
    /**
     * Getter for excluded product IDs
     */
    public function getExclSKUs()
    {
        return $this->excludeSKUs;
    }
    
    /**
     * Getter for the bottom limit.
     */
    public function getMinimum()
    {
        return $this->minAmount;
    }
    
    /**
     * Getter for the upper limit.
     */
    public function getMaximum()
    {
        return $this->maxAmount;
    }
    
    /**
     * Getter for the bottom limit on item count.
     */
    public function getMinimumCount()
    {
    	return $this->minCount;
    }
     
    /**
     * Getter for the upper limit on item count.
     */
    public function getMaximumCount()
    {
    	return $this->maxCount;
    }
    	
    	
    
    
    /**
     * Getter for the type of the discount.
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Getter for free shiping.
     */
    public function isFreeShipping()
    {
        return $this->freeShipping;
    }
    
    /**
     * Getter for the absolute discount.
     */
    public function getAbsoluteDiscount()
    {
        return $this->MRPAmount;
    }
    
    /**
     * Getter for the percent discount.
     */
    public function getPercentDiscount()
    {
        return $this->MRPpercentage;
    }
    
    /**
     * Getter for start date.
     */
    public function getStartDate()
    {
        return $this->startDate;
    }
    
    /**
     * Getter for end date.
     */
    public function getEndDate()
    {
        return $this->endDate;
    }
    
    /**
     * Getter for $isActive.
     */
    public function isActive()
    {
        return ($this->isActive == 'A');
    }
    
    /**
     * @return boolean Whether the coupon endDate is over.
     */
    public function isExpired()
    {
        // Get normalized timestamp for current day.
        $now = time();
        $nowDate = date('d F Y', $now);
        $nowDateTime = strtotime($nowDate);
        
        // Get normalized timestamp for end date.
        $expireDate = date('d F Y', $this->endDate);
        $expireDateTime = strtotime($expireDate);
        
        return $nowDateTime > $expireDateTime;
    }
    
    /**
     * @return boolean Whether the coupon startDate is yet to arrive.
     */
    public function isNotStarted()
    {
        // Get normalized timestamp for current day.
        $now = time();
        $nowDate = date('d F Y', $now);
        $nowDateTime = strtotime($nowDate);
        
        // Get normalized timestamp for start date.
        $startDate = date('d F Y', $this->startDate);
        $startDateTime = strtotime($startDate);
        
        return $nowDateTime < $startDateTime;
    }
    
    /**
     * @param double $amount
     * @return boolean Whether subtotal is within the min/max specified range.
     */
    public function isAmountAllowed($amount)
    {
        if (!empty($this->minAmount)) {
            if ($this->minAmount > $amount) {
                return FALSE;
            }
        }
        
        if (!empty($this->maxAmount) && ($this->maxAmount != 0.0)) {
            if ($this->maxAmount < $amount) {
                return FALSE;
            }
        }
        
        return TRUE;
    }
    
    /**
     * @return boolean Whether the coupon has been used upto its limit.
     */
    public function isUsageExpired()
    {
        return (!$this->isInfinite) && ($this->timesUsed >= $this->maxUsage);
    }
    
    /**
     * @return boolean Whether a lock can be acquired on the coupon.
     */
    public function isFree()
    {
        return $this->isInfinite ||
               (($this->timesUsed + $this->timesLocked) < $this->maxUsage);
    }
    
   
        /**
         * @param int itemCount
         * @return boolean whether item count in between min/maxCount range.
         */
    public function isCountAllowed($itemCount){
    	if(!empty($this->minCount)){
    		if ($this->minCount > $itemCount) {
    			return FALSE;
    		}
    	}
    	if (!empty($this->maxCount) && ($this->maxCount != 0.0)) {
    		if ($this->maxCount < $itemCount) {
    			return FALSE;
    		}
    	}
    		
    	return TRUE;
    }
    					
    
    /**
     * @return boolean Whether the coupon can be redeemed by given user
     */
    public function isAllowedForUser($user)
    {
        // The return value.
        $return = TRUE;
        
        // Is user specified in the list of users.
        if ($this->users != null && $this->users != '') {
            $userPatternsArray = trim_all(explode(",", $this->users));
            $return &= $this->isPatternMatching($user, $userPatternsArray);
        }
        
        // Is user specified to be excluded for the coupon.
        if ($this->excludeUsers != null && $this->excludeUsers != '') {
            $excludeUsersArray = trim_all(explode(",", $this->excludeUsers));
            $return &= !$this->isPatternMatching($user, $excludeUsersArray);
        }
        
        return $return;
    }
    
    /**
     * @return boolean Whether the user has used the coupon for maximum allowed
     * number of times.
     */
    public function isUsageExpiredForUser($user)
    {
        $used = CouponAdapter::getInstance()->
            getCouponUsageForUser($this->couponCode, $user);

        return (!$this->isInfinitePerUser) && ($used >= $this->maxUsageByUser);
    }
    
    /**
     * @return boolean Whether the user can acquire a lock on the coupon.
     */
    public function isFreeForUser($user)
    {
        $used = CouponAdapter::getInstance()->
            getCouponUsageForUser($this->couponCode, $user);
        
        $locked = CouponAdapter::getInstance()->
            getCouponLockUsageForUser($this->couponCode, $user);
            
        return $this->isInfinitePerUser ||
               ($used + $locked < $this->maxUsageByUser);
    }
    
    /**
     * @return boolean Whether the coupon offers a discount at cart level.
     */
    public function isCartLevelDiscount()
    {
        if ($this->productTypeIds == '' &&
            $this->excludeProductTypeIds == '' &&
            $this->styleIds == '' &&
            $this->excludeStyleIds == '' &&
            $this->catStyleIds == '' &&
            $this->excludeCatStyleIds == '' &&
            $this->categoryIds == '' &&
            $this->excludeCategoryIds == '' &&
            $this->SKUs == '' &&
            $this->excludeSKUs == '')
        {
            return TRUE;
        }
        
        return FALSE;
    }
    
    /**
     * @return boolean Whether $user matches with any of the user patterns.
     */
    private function isPatternMatching($user, $patterns)
    {
        $occurs = false;
        foreach ($patterns as $pattern) {
            $occurs |= !(stripos($user, $pattern) === false);
        }
        
        return $occurs;
    }
    
    /**
     * @return Whether the current channel is allowed for the coupon.
     */
    public function isChannelAllowed()
    {
        global $XCART_SESSION_VARS;
	    $sessionChannel = web\utils\ChannelType::getChannel($XCART_SESSION_VARS["channel"]);

        if(strtolower($this->channel) == "mobile"){
        	if($sessionChannel != web\utils\ChannelType::MOBILE && $sessionChannel != web\utils\ChannelType::TABLET){
        		return false;
        	}
        }else if(strtolower($this->channel) == "tablet"){
        	if($sessionChannel != web\utils\ChannelType::TABLET){
        		return false;
        	}
        }else if(strtolower($this->channel) == "mobile_app"){
            if($sessionChannel != web\utils\ChannelType::MOBILE_APP){
                return false;
            }
        }else if(strtolower($this->channel) == "complete_mobile_domain"){
            if($sessionChannel != web\utils\ChannelType::MOBILE_APP && $sessionChannel != web\utils\ChannelType::TABLET && $sessionChannel != web\utils\ChannelType::MOBILE){
                return false;
            }
        }else if(strtolower($this->channel) == "desktop"){
        	if($sessionChannel != web\utils\ChannelType::DESKTOP){
        		return false;
        	}
        }
        return true;     
    }

    public function isCouponDeviceTagged(){
        if(!empty($this->applicable_device))
            return true;
        else
            return false;
    }

    public function isDeviceAllowed(){
        global $XCART_SESSION_VARS;

            if(!empty($XCART_SESSION_VARS["device"])){
                $sessionDeviceString = $XCART_SESSION_VARS["device"];
                $sessionDevice=explode(" ",$sessionDeviceString);
                $allowedDeviceList=explode(",",$this->applicable_device);
                foreach ($allowedDeviceList as &$value){
                    if(strcasecmp ($sessionDevice[0] , $value)==0)
                        return true;
                }
            }

        return false;

    }
    
    /**
     * Get a textual description of the coupon discount.
     */
    public function getDescription()
    {
        $csym = $config.General.currency_symbol;
        $csym = 'Rs';
        
        switch ($this->type) {
            case "absolute" :
                return "Upto $csym " . $this->MRPAmount . " off";
                break;
            case "percentage" :
                return $this->MRPpercentage . "% off";
                break;
            case "dual" :
                return $this->MRPpercentage . "% off upto $csym " 
                    . $this->MRPAmount;
        }
    }
    
    /**
     * Get group name.
     */
    public function getGroupName()
    {
        return $this->groupName;
    }

    /**
     * @return string Group type
     */
    public function getGroupType()
    {
        return $this->groupType;
    }


    
    /**
     * Get users
     */
    
    public function getUsers()
    {
    	return $this->users;	
    }
    
    
    /**
     * this function checks if a coupon group is such that the coupon
     * belonging to it is applicable on all styles irrespective of discount
     */
    
    public static function isApplyOnAllCouponGroup($couponGroup){
    	 
    	$applyOnAllCouponGroups = (string)WidgetKeyValuePairs::getWidgetValueForKey('applyOnAllCouponGroups');
    	$applyOnAllGroups = array_map('strtolower', explode(",", $applyOnAllCouponGroups));
    	return in_array(strtolower($couponGroup), $applyOnAllGroups);
    }
    
    public static function isSpecialCouponGroup($couponGroup){    
    	$specialCouponGroups = (string)WidgetKeyValuePairs::getWidgetValueForKey('specialStylesCouponGroup');
    	$specialGroups = array_map('strtolower', explode(",", $specialCouponGroups));
    	return in_array(strtolower($couponGroup), $specialGroups);
    }

    public static function isCouponGroupExists($widgetKey, $couponGroup){    
    	$couponGroupsString = (string)WidgetKeyValuePairs::getWidgetValueForKey($widgetKey);
    	$couponGroupsArray = array_map('strtolower', explode(",", $couponGroupsString));
    	return in_array(strtolower($couponGroup), $couponGroupsArray);
    }

}
