<?php

include_once "$xcart_dir/include/func/func.db.php";

/**
 * Module for gathering, storing and altering the mobile verification data.
 * 
 * NOTE: 
 * -----
 * status codes
 * U - Unverified
 * V - Verified
 * O - old / obsolete
 * 
 * LOGIC:
 * ------
 * A mapping is mobile vs. login.
 * A mapping initially enters in U status.
 * 
 * Upon successful SMS verification, the status changes to V.
 * 
 * To avoid frauds, we disallow verification of a mapping, if the same mobile 
 * is in a mapping with another login with V status.
 * 
 * If a login with a V status adds another mapping, the status of previous 
 * mapping should be changed to O.
 * 
 * Bottom-line: There can be atmost 1 mapping in V status for any mobile.
 * 
 * @author Rohan.Railkar
 */
class SmsVerifier
{
    /**
     * Database constants.
     */
    private $tbl_mapping = 'mk_mobile_mapping';
    
    /**
     * The singleton instance.
     */
    private static $instance = NULL;

    /**
     * @return SmsVerifier The singleton instance.
     */
    public static function getInstance()
    {
        if (SmsVerifier::$instance == NULL) {
            SmsVerifier::$instance = new SmsVerifier();
        }

        return SmsVerifier::$instance;
    }

    /**
     * Constructor.
     */
    private function __construct()
    {
        // Empty.
    }
    
    /**
     * Add a new mapping.
     */
    public function addNewMapping($mobile, $login, $status = 'V', $date_verified = null)
    {
        // Change the V status of any previous mobiles for the login to O.
        $query =
            "UPDATE " . $this->tbl_mapping . " 
            	SET status = 'O'
              WHERE login = '$login'
              	AND mobile != '$mobile'
                AND status = 'V';";
        
        db_query($query);
        // check if the mobile, login is alredy verified
        $query =
        	"SELECT status from ". $this->tbl_mapping . "
        		WHERE login = '$login'
        		  AND mobile = '$mobile';";
        
        $mobile_status = func_query_first_cell($query);
        if($mobile_status == 'V')	{
        	return;
        }

        if($date_verified==null){
            $t=time();
            $date_verified=$t;
        }
        // Add the new mapping.
        $query = 
        	"REPLACE INTO " . $this->tbl_mapping . " 
        		(mobile, login, status, date_verified, cod_verification_status )
        	VALUES
        		('$mobile', '$login', '$status', '$date_verified', '0');";
        
        db_query($query);
    }
    
    /**
     * Skip the verification logic for mobile numbers. Marks mobile as verified and sets 
     * the flag for skipped_verification.
     * 
     * @param mobile number  $mobile
     * @param email id $login
     */
    
    public function skipVerification($mobile,$login)
    {
    	$result = func_query_first(
        	"SELECT login 
        	   FROM " . $this->tbl_mapping . " 
        	  WHERE mobile = '$mobile'
        	  	AND login != '$login'
        	    AND (status = 'V' OR status = 'O');");
        
        // Failure.
        if (!empty($result)) {
            return;
        }
    	
        $time = time();
        $query = 
            "UPDATE " . $this->tbl_mapping . "
            	SET status ='V', date_verified='$time', skipped_verification='1'
            	WHERE mobile='$mobile' and login='$login';";

        db_query($query);
    }
    
    /**
     * Verify a mapping.
     * 
     * @return true 	If the verification is successful.
     * 		   false 	If the verification is not successful.
     */
    public function verifyMapping($mobile, $login, $verifyCOD=false)
    {
        //
        // Check if the mobile is already verified (currently / previously) for 
        // some other login.
        //
        $result = func_query_first(
        	"SELECT login 
        	   FROM " . $this->tbl_mapping . " 
        	  WHERE mobile = '$mobile'
        	    AND login != '$login'
        	    AND (status = 'V' OR status = 'O');");
        
        // Failure.
        if (!empty($result)) {
            return false;
        }
        
        // Update the status of desired login to Verified.
        $time = time();
        $query = 
            "REPLACE INTO " . $this->tbl_mapping . "
             	(mobile, login, status, date_verified)
             VALUES
             	('$mobile', '$login', 'V', '$time');";
        
        db_query($query);
      
		$query = "UPDATE mk_mobile_code 
				set num_attempts = 0, cap_attempts = 0 
				where mobile='$mobile' and login='$login';";
		
		db_query($query);

        if($verifyCOD){
            db_query("UPDATE mk_mobile_mapping set cod_verification_status=1".
                        " where  mobile='".mysql_real_escape_string($mobile)."'".
                        " and login='".mysql_real_escape_string($login)."'");
        }

        return true;
    }
	
    /**
     * Get the verification status of a mapping.
     * Return empty string if mapping is non existent.
     */
    public function getStatus($mobile, $login)
    {
        $query = 
            "SELECT status 
               FROM " . $this->tbl_mapping . " 
              WHERE mobile = '$mobile'
                AND login = '$login';";
        
        $result = db_query($query);
        $row = db_fetch_array($result);
        
        if (empty($row)) {
            return '';
        }
        else {
			
			$result2 = func_query_first_cell(
				"SELECT num_attempts from mk_mobile_code
					WHERE mobile='$mobile' and login='$login';");
			
			if( $row['status'] != 'V' )	{
				if ($result2['num_attempts'] >= 4)	{
					return 'E';
				} 
			}  

            return $row['status'];
        }
    }

	/** 
	 * Get the number of verification attempts for a mapping
	 * Return empty string if mapping does not exist.
	 */
	 public function getAttempts($mobile,$login)
	 {
		$query = 
			"SELECT num_attempts 
				from mk_mobile_code
				where mobile='$mobile' 
				and login='$login';";
	 
		$result = db_query($query);
        $row = db_fetch_array($result);
        
        if (empty($row)) {
            return '';
        }
        else {
            return $row['num_attempts'];
        }
	 }

	 public function lockMobileLogin($mobile, $login)
	 {
		$time=time();
		$query= "UPDATE mk_mobile_code 
				SET timeLocked='$time'
				WHERE mobile='$mobile'
				AND login='$login';";
		db_query($query);
					
	 }
    
    /**
     * Add a mobile no vs. verification code mapping. [Replace]
     */
    public function addMobileCodeMapping($mobile, $code, $login)
    {
	    	$query =
            "SELECT num_attempts, cap_attempts
               FROM mk_mobile_code
               WHERE mobile = '$mobile' and login='$login';";
        
        $result = db_query($query);
        $row = db_fetch_array($result);
        	global $weblog;
        if(!empty($row) && $row['cap_attempts'] >= 3)
			return "verifyFailed";
		else if(empty($row))	{
		
        	$num_attempts = 1;
			$query2= "insert into  mk_mobile_code
				(mobile,code,num_attempts,cap_attempts,login)
				VALUES
				('$mobile', '$code', '$num_attempts', '0', '$login')";
             
         
       		db_query($query2);
			return "showverifybtn";
        } else {
        	$num_attempts = $row['num_attempts']+ 1;
        	$cap_attempts = $row['cap_attempts'];
        }
         
        if($cap_attempts == 2 && ($num_attempts - $cap_attempts >= 3)) {	
		
		$time = time();
        $query3 = "update mk_mobile_code
					set timeLocked = '$time'
					where mobile='$mobile'
					and login='$login'";
		db_query($query3); 

			return "verifyFailed";
		}
   
        if($num_attempts - $cap_attempts <=2){
        	$query2 =
			"update mk_mobile_code
				set code='$code', num_attempts= $num_attempts
			 WHERE mobile='$mobile' and login='$login';";	
         
       		db_query($query2);
       		return "showverifybtn";
        } else {
        	return "showCaptcha";
        }  
     }
    
    /**
     * Verify a code against the mobile.
     * 
     * @return true		If the code matches.
     * 		   false 	Otherwise.
     */
    public function verifyMobileCode($mobile, $code, $login)
    {
        $query =
            "SELECT code
               FROM mk_mobile_code
              WHERE mobile = '$mobile'
			  AND login = '$login';";
        
        $result = db_query($query);
        $row = db_fetch_array($result);
        
        if (!empty($row)) {
            return ($row['code'] == $code);
        }
        else {
            return false;
        }
    }
    
    /**
     * Remove a mobile - code mapping.
     */
    public function updateCaptchaAttempts($mobile,$login)
    {
		$query =
            "UPDATE mk_mobile_code
             SET cap_attempts = cap_attempts + 1
             WHERE mobile = '$mobile'
			 AND login = '$login';";
        db_query($query);    
       
	}
    
	public function checkCODVerifiedStatus($mobile, $login){
		$query="SELECT * from mk_mobile_mapping where mobile='$mobile' and login='$login' and cod_verification_status=1;";
		$row = func_query_first($query);
		if(empty($row)){
			return FALSE;
		} else {
			return TRUE;
		}
	}
    /**
     * Get a verified login associated with the mobile.
     */
    public function getVerifiedLoginFor($mobile, $checkCODFlag = false)
    {
    	
	$query = "SELECT login FROM " . $this->tbl_mapping . " WHERE mobile = '$mobile'";
	$query .= ($checkCODFlag)?" AND cod_verification_status = 1;":" AND (status = 'V' or status = 'O');";
        
		$row = func_query_first($query, true);
        if (empty($row)) {
            return null;
        }
        return $row['login'];
	
    }
       	
    public function verifyViaCellNext ($mobile, $login, $source="coupon")
    {
    	$verifiedLogin = $this->getVerifiedLoginFor($mobile, $source=="cod");
    	if(!empty($verifiedLogin) && ($verifiedLogin == $login))	{
    		//no action required, success
    		return 0;
       	}
    	
    	if(!empty($verifiedLogin) &&($verifiedLogin != $login))	{
    		//mobile verified by another person
    		return 2;
    	}
    	    	
    	$time = time();
    	$query = "SELECT * from MRP_cellnext_stats WHERE mobile = '$mobile';";
    	$result = func_query_first($query);
    	
    	if(empty($result)) {
    		// first verification attempt
    		$query = "INSERT INTO MRP_cellnext_stats (mobile, login, captured_lock_on)".
    				"VALUES ('$mobile','$login','$time');";
    		func_query($query);
	    	// Update cod_verification_status to "ongoing"
	    	if($source == "cod"){
	    		$query =             
		    		"UPDATE " . $this->tbl_mapping . " 
		    		SET cod_verification_status = '2'
		       		WHERE mobile = '$mobile'
		            AND login = '$login';";
	    		func_query($query);
	    	}
    	
    	} else {
    		// entry exist for the mobile no
    		//check if current time is withing the 10 min range
    		if($time > ($result['captured_lock_on'] + 600 )){
			
    			if ($result['login'] == $login)	{
					func_query("UPDATE MRP_cellnext_stats set captured_lock_on='$time' where mobile = '$mobile';");
				} 
				else {
					$prevLogin = $result['login'];
					
					func_query("UPDATE MRP_cellnext_stats set captured_lock_on='$time', login='$login' where mobile = '$mobile';");
					func_query("UPDATE mk_mobile_mapping set cod_verification_status=0 where mobile='$mobile' and login='$prevLogin';");
					// Update cod_verification_status to "ongoing"
	    			if($source == "cod"){
		    			$query =             
			    		"UPDATE " . $this->tbl_mapping . " 
			    		SET cod_verification_status = '2'
			       		WHERE mobile = '$mobile'
			            AND login = '$login';";
		    			func_query($query);
	    			}
				}
    		}    
    	}	
    	return 1; 
    }
    
    public function updateViaCellNext ($mobile, $timeRecieved){

    	//mobile has already been cod-verified
    	$query = "SELECT * from mk_mobile_mapping where mobile='$mobile' and cod_verification_status = 1";	   	
		$result = func_query_first($query);
		if(!empty($result)){
			return;
		}

		//no entry is found for the mobile
		$query = "SELECT * from MRP_cellnext_stats where mobile='$mobile'";
		$result = func_query_first($query);  
		if(empty($result)){
			return;
		}
		
		if(($timeRecieved > $result['captured_lock_on']) && ($timeRecieved < ($result['captured_lock_on'] + 600)))
		{
			// update the cod_verification status
			$login = $result['login'];
			func_query("UPDATE mk_mobile_mapping set cod_verification_status=1 where  mobile='$mobile' and login='$login'; ");	 
		}
    	
    }

    public function updateCODMobileVerifyToOngoing($mobile, $login){
        db_query("UPDATE mk_mobile_mapping set cod_verification_status=2".
                    " where  mobile='".mysql_real_escape_string($mobile)."'".
                    " and login='".mysql_real_escape_string($login)."'");
    }
}
