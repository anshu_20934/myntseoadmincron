<?php

/**
 * Order class for MRP project.
 * 
 * @author Rohan.Railkar
 */
class Order
{
    private $orderId;
    private $login;
    private $QueuedDate;
    
    /**
     * Constructor.
     */
    public function __construct($row)
    {
        $this->orderId = $row['orderid'];
        $this->login = $row['login'];
        $this->QueuedDate = $row['queueddate'];
    }
    
    /**
     * Getter for orderid.
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
    
    /**
     * Getter for login.
     */
    public function getLogin()
    {
        return $this->login;
    }
    
    /**
     * Getter for Queued date.
     */
    public function getQueuedDate()
    {
        return $this->QueuedDate;
    }
}
