<?php

include_once "$xcart_dir/modules/coupon/mrp/Rule.php";
include_once "$xcart_dir/modules/coupon/database/MRPDataAdapter.php";
include_once "$xcart_dir/modules/coupon/database/MRPRulesAdapter.php";
include_once "$xcart_dir/modules/coupon/mrp/exception/MrpRuleNotFoundException.php";

/**
 * The time buffer (time window) for considering the events.
 * Default: half an hour
 */

define("TIME_BUFFER", 30 * 60);
/**
 * Rule executor is invoked by the cron after a fixed time interval. It 
 * iterates through all transactions / events in that time window and 
 * applies all active rules on the corresponding customers.
 * 
 * @author Rohan.Railkar
 */
class RuleExecutor
{
    /**
     * The singleton instance.
     */
    private static $instance = NULL;  
    
    /**
     * MRP Rules adapter.
     */
    private $rulesAdapter;

    /**
     * Data adapter for fetching orders and customers.
     */
    private $mrpDataAdapter;
    
    /**
     * @return RuleExecutor The singleton instance.
     */
    public static function getInstance()
    {
        if (RuleExecutor::$instance == NULL) {
            RuleExecutor::$instance = new RuleExecutor();
        }
        return RuleExecutor::$instance;
    }

    /**
     * Constructor.
     */
    private function __construct()
    {
        $this->rulesAdapter = MRPRulesAdapter::getInstance();
        $this->mrpDataAdapter = MRPDataAdapter::getInstance();
    }
    
    /**
     * Synchronous call for invoking a rule on an entity.
     */
    public function runSynchronous($entityType, $entityId, $ruleId)
    {
        try {
            $rule = $this->rulesAdapter->fetchRule($ruleId);
        }
        catch (MrpRuleNotFoundException $e) {
            return;
        }
        
        // Rule should be synchronous.
        if ($rule->isSynchronous() == 0) 
        {
            return;
        }
        
        // Entity is customer.
        if ($entityType == "customer") {
            
            try {
                $customer = $this->mrpDataAdapter->fetchCustomer($entityId);
                $rule->applyOnCustomer($customer);
                return;
            }
            catch (MrpCustomerNotFoundException $e) {
                return;
            }
        }
        
        // Entity is order.
        else if ($entityType == "order") {
            
            try {
                $order = $this->mrpDataAdapter->fetchOrder($entityId);
                $rule->applyOnOrder($order);
                return;
            }
            catch (MrpOrderNotFoundException $e) {
                return;
            }
        }
        //Entity login is where we operate on just login id.
        else if ($entityType == "coupon"){
        	try {
        		
        		$rule->applyOnCoupon($entityId);
        		return;
        	}
        	catch (MrpCustomerNotFoundException $e) {
        		return;
        	}
        	
        	
        }
    }
    
    /**
     * Run the rule executor.
     */
    public function run()
    {
        echo "executing...\n";
        
        // Gather rules.
        $rules = $this->rulesAdapter->getAllAsynchronousActiveRootRules();
        
        // Gather orders / customers
        $orders = $this->mrpDataAdapter->fetchQueuedOrdersInTimeBuffer(TIME_BUFFER);
        //$customers = $this->mrpDataAdapter->fetchCustomersInTimeBuffer(TIME_BUFFER);
           
		echo "**Applying on orders\n";
        // Apply rules on orders.
        foreach ($orders as $order) {
            foreach ($rules as $rule) {
                $rule->applyOnOrder($order);
            }
        }
        
        /*
        echo "**Applying on customers\n";
        // Apply rules on customers.
        foreach ($customers as $customer) {
            echo $customer->getLogin() . "\n";
            foreach ($rules as $rule) {
                $rule->applyOnCustomer($customer);
            }
        }*/
    }
}
