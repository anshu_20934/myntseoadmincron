<?php

require_once ("$xcart_dir/exception/MyntraException.php");

/**
 * A base class for all exceptions concerned with MRP project.
 * 
 * @author Rohan.Railkar
 */
class MrpException extends MyntraException
{
	/**
	 * Constructor.
	 */
	public function __construct($message, $code=0, $previous=null)
	{
	    parent::__construct($message, $code, $previous);
	}
}
