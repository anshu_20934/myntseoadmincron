<?php

include_once ("$xcart_dir/modules/coupon/mrp/exception/MrpException.php");

/**
 * This exception should be thrown when the MRP rule is not found, or is inactive.
 * 
 * @author Rohan.Railkar
 */
class MrpRuleNotFoundException extends MrpException
{
    private $ruleid;
    
	/**
	 * Constructor.
	 * overrides Exception.__construct
	 * 
	 * @param String $ruleid ID of the rule.
	 */
	public function __construct($ruleid)
	{
		parent::__construct("No active rule found for Rule Id: $ruleid");
		$this->ruleid = $ruleid;
	}
	
	/**
	 * Getter for the rule'd ID.
	 */
	public function getRuleId()
	{
	    return $this->ruleid;
	}
}
