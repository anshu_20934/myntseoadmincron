<?php

include_once ("$xcart_dir/modules/coupon/mrp/exception/MrpException.php");

/**
 * This exception should be thrown when the customer corresponding to given 
 * login is not found in the database.
 * 
 * @author Rohan.Railkar
 */
class MrpCustomerNotFoundException extends MrpException
{
    private $login;
    
	/**
	 * Constructor.
	 * overrides Exception.__construct
	 * 
	 * @param String $login Login for the customer that was not found.
	 */
	public function __construct($login)
	{
		parent::__construct("Customer with login $login not found!");
		$this->login = $login;
	}
	
	/**
	 * Getter for the customer's login.
	 */
	public function getLogin()
	{
	    return $this->login;
	}
}
