<?php

include_once ("$xcart_dir/modules/coupon/mrp/exception/MrpException.php");

/**
 * This exception should be thrown when the order corresponding to given 
 * ID is not found in the database.
 * 
 * @author Rohan.Railkar
 */
class MrpOrderNotFoundException extends MrpException
{
    private $orderId;
    
	/**
	 * Constructor.
	 * overrides Exception.__construct
	 * 
	 * @param String $orderId ID for the order that was not found.
	 */
	public function __construct($orderId)
	{
		parent::__construct("Order with ID $orderId not found!");
		$this->orderId = $orderId;
	}
	
	/**
	 * Getter for the orderId.
	 */
	public function getOrderId()
	{
	    return $this->orderId;
	}
}
