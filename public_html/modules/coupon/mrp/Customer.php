<?php

include_once "$xcart_dir/modules/coupon/database/MRPDataAdapter.php";

/**
 * Customer class for MRP.
 * 
 * @author Rohan.Railkar
 */
class Customer
{
    private $login;
    private $firstLogin;
    private $lastLogin;
    private $referer;
    private $QueuedOrders;
    
    /**
     * Constructor from the data array.
     */
    public function __construct($row)
    {
        $this->login = $row['login'];
        $this->firstLogin = $row['first_login'];
        $this->lastLogin = $row['last_login'];
        $this->referer = $row['referer'];
        
        $adapter = MRPDataAdapter::getInstance();
        $this->QueuedOrders = $adapter->getQueuedOrders($this->login);
    }
    
    /**
     * Getter for the login.
     */
    public function getLogin()
    {
        return $this->login;
    }
    
    /**
     * Getter for the first login time.
     */
    public function getFirstLogin()
    {
        return $this->firstLogin;
    }
    
    /**
     * Getter for the last login time.
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }
    
    /**
     * Getter for the referer.
     */
    public function getReferer()
    {
        return $this->referer;
    }    
    
    /**
     * Getter for the Queued orders.
     */
    public function getQueuedOrders()
    {
        return $this->QueuedOrders;
    }
}
