<?php

include_once "$xcart_dir/modules/coupon/mrp/Customer.php";
include_once "$xcart_dir/modules/coupon/database/MRPDataAdapter.php";
include_once "$xcart_dir/modules/coupon/database/MRPRulesAdapter.php";
include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
include_once "$xcart_dir/include/func/func.mail.php";
include_once "$xcart_dir/modules/coupon/MRPConstants.php";
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";
require_once "$xcart_dir/include/class/class.mail.multiprovidermail.php";

/**
 * The Rule for an MRP campaign.
 * 
 * @author Rohan.Railkar
 */
class Rule
{
    private $rulesAdapter;
    private $dataAdapter;
    private $couponAdapter;
    
    private $id;
    private $name;
    private $isActive = false;
    private $isSynchronous = false;

    private $mrpCouponConfiguration;
    /**
     * Executor will consider only the root level rules.
     */
    private $isRootLevel = true;
    
    /**
     * Ids of child rules.
     */
    private $childRules = array();
    
    /**
     * Concerned objects. e.g.- customer(s), order.
     */
    private $applicableOn;
    
    /**
     * If the rule can be validated without any special function.
     */
    private $isPrimitiveRule = true;
    
    /**
     * These parameters will be empty for a root level rule.
     */
    private $fieldToCompare;
    private $operatorToCompare;
    private $valueToCompare;
    
    /**
     * These parameters will be empty for a child level rule.
     */
    private $successAction;
    private $failureAction;
    
    /**
     * Verification function for non primitive child rules.
     */
    private $nonPrimitiveFunction;
    
    /**
     * Construct from array.
     */
    public function __construct($rule)
    {
        $this->rulesAdapter = MRPRulesAdapter::getInstance();
        $this->dataAdapter = MRPDataAdapter::getInstance();
        $this->couponAdapter = CouponAdapter::getInstance();
        
        $this->id = $rule['ruleid'];
        $this->name = $rule['name'];
        $this->isActive = $rule['isActive'];
        $this->isRootLevel = $rule['isRootLevel'];
        $this->childRules = trim_all(explode(",", $rule['childRules']));
        $this->applicableOn = $rule['applicableOn'];
        $this->isPrimitiveRule = $rule['isPrimitive'];
        $this->fieldToCompare = $rule['fieldToCompare'];
        $this->operatorToCompare = $rule['operatorToCompare'];
        $this->valueToCompare = $rule['valueToCompare'];
        $this->successAction = $rule['successAction'];
        $this->failureAction = $rule['failureAction'];
        $this->nonPrimitiveFunction = $rule['nonPrimitiveFunction'];
        $this->isSynchronous = $rule['isSynchronous'];
        $this->readCouponConfiuration();    
     }
    
 public function readCouponConfiuration(){
    	global $mrpDefaultCouponConfiguration;
        
		$this->mrpCouponConfiguration['mrp_firstLogin_nonfbreg_numCoupons'] = 
			FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.nonfbreg.numCoupons", $mrpDefaultCouponConfiguration['mrp_firstLogin_nonfbreg_numCoupons']);
		$this->mrpCouponConfiguration['mrp_firstLogin_nonfbreg_mrpAmount'] = 
			FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.nonfbreg.mrpAmount", $mrpDefaultCouponConfiguration['mrp_firstLogin_nonfbreg_mrpAmount']);
		$this->mrpCouponConfiguration['mrp_firstLogin_nonfbreg_validity'] =
			FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.nonfbreg.validity", $mrpDefaultCouponConfiguration['mrp_firstLogin_nonfbreg_validity']);
		$this->mrpCouponConfiguration['mrp_firstLogin_nonfbreg_minCartValue'] =
			FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.nonfbreg.minCartValue", $mrpDefaultCouponConfiguration['mrp_firstLogin_nonfbreg_minCartValue']);
		
		$this->mrpCouponConfiguration['mrp_firstLogin_fbreg_numCoupons'] = 
			FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.fbreg.numCoupons", $mrpDefaultCouponConfiguration['mrp_firstLogin_fbreg_numCoupons']);
		$this->mrpCouponConfiguration['mrp_firstLogin_fbreg_mrpAmount'] = 
			FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.fbreg.mrpAmount", $mrpDefaultCouponConfiguration['mrp_firstLogin_fbreg_mrpAmount']);
		$this->mrpCouponConfiguration['mrp_firstLogin_fbreg_validity'] =
			FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.fbreg.validity", $mrpDefaultCouponConfiguration['mrp_firstLogin_fbreg_validity']);
		$this->mrpCouponConfiguration['mrp_firstLogin_fbreg_minCartValue'] =
			FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.fbreg.minCartValue", $mrpDefaultCouponConfiguration['mrp_firstLogin_fbreg_minCartValue']);
			
			
		$this->mrpCouponConfiguration['mrp_refFirstPurchase_numCoupons'] = 
			FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.numCoupons", $mrpDefaultCouponConfiguration['mrp_refFirstPurchase_numCoupons']);
		$this->mrpCouponConfiguration['mrp_refFirstPurchase_mrpAmount'] = 
			FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.mrpAmount", $mrpDefaultCouponConfiguration['mrp_refFirstPurchase_mrpAmount']);
		$this->mrpCouponConfiguration['mrp_refFirstPurchase_mrpPercentage'] =
			FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.mrpPercentage", 0);
		$this->mrpCouponConfiguration['mrp_refFirstPurchase_couponType'] =
			FeatureGateKeyValuePairs::getFeatureGateValueForKey("mrp.refFirstPurchase.couponType", 'absolute');
		$this->mrpCouponConfiguration['mrp_refFirstPurchase_validity'] =
			FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.validity", $mrpDefaultCouponConfiguration['mrp_refFirstPurchase_validity']);
		$this->mrpCouponConfiguration['mrp_refFirstPurchase_minCartValue'] =
			FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.minCartValue", $mrpDefaultCouponConfiguration['mrp_refFirstPurchase_minCartValue']);
				
			
		$this->mrpCouponConfiguration['mrp_refRegistration_numCoupons'] = 
			FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.numCoupons", $mrpDefaultCouponConfiguration['mrp_refRegistration_numCoupons']);
		$this->mrpCouponConfiguration['mrp_refRegistration_mrpAmount'] = 
			FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.mrpAmount", $mrpDefaultCouponConfiguration['mrp_refRegistration_mrpAmount']);
		$this->mrpCouponConfiguration['mrp_refRegistration_mrpPercentage'] =
			FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.mrpPercentage", 0);
		$this->mrpCouponConfiguration['mrp_refRegistration_couponType'] =
			FeatureGateKeyValuePairs::getFeatureGateValueForKey("mrp.refRegistration.couponType", 'absolute');
		$this->mrpCouponConfiguration['mrp_refRegistration_validity'] =
			FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.validity", $mrpDefaultCouponConfiguration['mrp_refRegistration_validity']);
		$this->mrpCouponConfiguration['mrp_refRegistration_minCartValue'] =
			FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.minCartValue", $mrpDefaultCouponConfiguration['mrp_refRegistration_minCartValue']);     
        
    }
     
    /**
     * Getter for isSynchronous.
     */
    public function isSynchronous()
    {
        return $this->isSynchronous;
    }
    
    /**
     * Apply the root level rule on an order.
     */
    public function applyOnOrder($order)
    {
        if ($this->applicableOn != 'order') {
            return;
        }
        
        echo "applyOnOrder(" . $order->getOrderId() . ")\n";
        
        if (!$this->isRootLevel) {
            // TODO throw exception.
            return;
        }
        
          
        // Check if this rule was previously applied.
        if ($this->rulesAdapter->wasSuccessfullyApplied($this->id, $order->getOrderId())) {
            return;
        }
        
        // Check validity with all child-rules.
        $isValid = true;
        foreach ($this->childRules as $childRule) {
            $rule = $this->rulesAdapter->fetchRule($childRule);
            $isValid &= $rule->isApplicableOnOrder($order);
        }
        
        echo "valid: >>$isValid<<\n";
        
        if ($isValid) {
            
            // Success action.
            echo "SuccAct: >>" . $this->successAction . "<<\n";
            $codeString = "\$this->" . $this->successAction . "(\$order);";
            echo $codeString . "\n";
            return eval($codeString);
        }
        else {
            
            // Failure action.
            echo "FailAct: >>" . $this->failureAction . "<<\n";
            $codeString = "\$this->" . $this->failureAction . "(\$order);";
            echo $codeString . "\n";
            return eval($codeString);
        }
    }
    
    /**
     * Apply the root level rule on a customer.()
     */
    public function applyOnCustomer($customer)
    {
        if ($this->applicableOn != 'customer') {
            return;
        }
        
        if (!$this->isRootLevel) {
            // TODO throw exception.
            return;
        }
        
        // Check if the rule was successfully applied earlier.
        if ($this->rulesAdapter->wasSuccessfullyApplied($this->id, $customer->getLogin())) {
            return;
        }

        // Check validity with all child-rules.
        $isValid = true;
        foreach ($this->childRules as $childRule) {
            $rule = $this->rulesAdapter->fetchRule($childRule);
            $isValid &= $rule->isApplicableOnCustomer($customer);
        }
        
        if ($isValid) {
            
            // Success action.
            $codeString = "\$this->" . $this->successAction . "(\"" . $customer->getLogin() . "\");";
            return eval($codeString);
        }
        else {
            
            // Failure action.
            $codeString = "\$this->" . $this->failureAction . "(\"" . $customer->getLogin() . "\");";
            return eval($codeString);
        }
    }
    
    /**
     * Apply the root level rule on a login.()
     */
    public function applyOnLogin($login)
    {
    	if ($this->applicableOn != 'login') {
    		return;
    	}
    
    	if (!$this->isRootLevel) {
    		// TODO throw exception.
    		return;
    	}
    
    	// Check if the rule was successfully applied earlier.
    	if ($this->rulesAdapter->wasSuccessfullyApplied($this->id, $login)) {
    		return;
    	}
    
    	// Check validity with all child-rules.
    	$isValid = true;
    	foreach ($this->childRules as $childRule) {
    		if(empty($childRule)) continue;
    		$rule = $this->rulesAdapter->fetchRule($childRule);
    		$isValid &= $rule->isApplicableOnCustomer($login);
    	}
    
    	if ($isValid) {
    		
    		// Success action.
    		$codeString = "\$this->" . $this->successAction . "(\"" . $login . "\");";
    		return eval($codeString);
    	}
    	else {
    
    		// Failure action.
    		$codeString = "\$this->" . $this->failureAction . "(\"" . $login . "\");";
    		return eval($codeString);
    	}
    }
    
    /**
     * Apply the root level rule on coupon
     */
    public function applyOnCoupon($coupon)
    {
    	if ($this->applicableOn != 'coupon') {
    		return;
    	}
    
    	if (!$this->isRootLevel) {
    		// TODO throw exception.
    		return;
    	}
    
    	// Check if the rule was successfully applied earlier.
    	if ($this->rulesAdapter->wasSuccessfullyApplied($this->id, $coupon)) {
    		return;
    	}
    
    	// Check validity with all child-rules.
    	$isValid = true;
    	
    	/*
    	 * check for child rules
    	 * 
    	 */
    	
    	if ($isValid) {
    		// Success action.
    		$codeString = "\$this->" . $this->successAction . "(\"" . $coupon . "\");";
    		return eval($codeString);
    	}
    	else {
    
    		// Failure action.
    		$codeString = "\$this->" . $this->failureAction . "(\"" . $coupon . "\");";
    		return eval($codeString);
    	}
    }
    
    /**
     * @return boolean Whether a child rule is applicable on the order.
     */
    public function isApplicableOnOrder($order)
    {
        if ($this->isRootLevel) {
            return false;
        }
        
        // Non-primitive child rule.
        if (!$this->isPrimitiveRule) {
            $ret;
            $codeStr = "\$ret = \$this->" . $this->nonPrimitiveFunction . "(\$order);";
            echo "eval: >>$codeStr<<\n";
            eval($codeStr);
            return (empty($ret) ? false : true);
        }
        
        // To follow -- Code for any primitive child rules on orders.
    }
    
    /**
     * @return boolean Whether a child rule is applicable on the customer.
     */
    public function isApplicableOnCustomer($customer)
    {
        if ($this->isRootLevel) {
            return false;
        }
        
        // Non-primitive child rule.
        if (!$this->isPrimitiveRule) {
            $ret;
            $codeStr = "\$ret = \$this->" . $this->nonPrimitiveFunction . "(\$customer);";
            eval($codeStr);
            return (empty($ret) ? false : true);
        }
        
        // Primitive child rule.
        $toCompare;
        switch ($this->fieldToCompare) {
            case "first_login" :
                $toCompare = $customer->getFirstLogin();
                break;
            default :
                $toCompare = $this->fieldToCompare;
        }
        
        $toCompareWith;
        switch ($this->valueToCompare) {
            case "last_login" :
                $toCompareWith = $customer->getLastLogin();
                break;
            default :
                $toCompareWith = $this->valueToCompare;
        }
        
        $operator;
        switch ($this->operatorToCompare) {
            case "equals" :
                $operator = "==";
                break;
            default :
                $opertor = $this->operatorToCompare;
        }
        
        $ret;
        $codeStr = "\$ret = (" . $toCompare . $operator . $toCompareWith . ");";
		eval($codeStr);
        return (empty($ret) ? false : true);
    }
    
    /**
     * Returns the array of IDs of child rules.
     */
    public function getChildRules()
    {
        return $this->childRules;
    }
    
    public function generate_first_login_coupons($login, $num_coupons, $mrpAmount, $minCartValue, $validity, $template,$utm_medium,$couponConfig=false)
    {
	   	   	
	   	$couponDetailMsg = "";
        $couponCode = "";
        $minValue = "";
	   	
	   	if($couponConfig && is_array($couponConfig)){
	   		
	   		foreach ($couponConfig as $id=>$couponTemplate){
	   			
	   			
	   			if((float)$couponTemplate["percent_discount"] > 0.0 && (float)$couponTemplate["amount_discount"] > 0.0){
	   				$description = "Registration on Myntra - ".$couponTemplate["percent_discount"] ."% off upto Rs. ". $couponTemplate["amount_discount"]." coupon";
	   				
	   				$mailerDescription = "EXTRA ".$couponTemplate["percent_discount"] ."% OFF";
                    $minValue = $couponTemplate["minimum_purchase"];
	   			}elseif((float)$couponTemplate["percent_discount"] > 0.0){
	   				$description = "Registration on Myntra - ".$couponTemplate["percent_discount"] ."% off  coupon";
	   				
	   				$mailerDescription = "EXTRA ".$couponTemplate["percent_discount"] ."% OFF";
                    $minValue = $couponTemplate["minimum_purchase"];
	   			}else{
	   				$description = "Registration on Myntra - Rs. ". $couponTemplate["amount_discount"]." coupon";
	   				
	   				$mailerDescription = "Rs. ".$couponTemplate["amount_discount"] ." OFF";
                    $minValue = $couponTemplate["minimum_purchase"];
	   				
	   			}
	   			
	   			$startDate = strtotime(date('d F Y', time()));
	   			$endDate = $startDate + $couponTemplate["valid_days"] * 24 * 60 * 60;
	   			$couponCode = $this->couponAdapter->generateSingleCouponForUser("New", "8", $couponTemplate["coupon_group"], $startDate, $endDate, $login,  $couponTemplate["coupon_type"], (float)$couponTemplate["amount_discount"], (float)$couponTemplate["percent_discount"],(float) $couponTemplate["minimum_purchase"], $description);
	   			
	   			// Logging success of first login of mrp.
	   			$this->rulesAdapter->log($this->id, $this->name, time(), $login, '1', $couponCode, $login);
	   			
	   			// Add customer-coupon mapping.
	   			$this->dataAdapter->addCouponMapping($login, $couponCode);
	   			
	   			
	   			
		   			$couponDetailMsg = $mailerDescription;
		   		}
	   	
	   		
	   	}else{
	   		$description = "Registration on Myntra - Rs. $mrpAmount coupon";
	   		
	   		$mailerDescription = "Rs. ".$mrpAmount ." OFF";
            $minValue = $minCartValue;
	   		
	   		
	    	for ($i = 0; $i < $num_coupons; $i++) {
	        	$startDate = strtotime(date('d F Y', time()));
	            $endDate = $startDate + $validity * 24 * 60 * 60;
	            $couponCode = $this->couponAdapter->generateSingleCouponForUser("New", "8", "FirstLogin", $startDate, $endDate, $login, 'absolute', $mrpAmount, '', $minCartValue, $description);
	            
	            // Logging success of first login of mrp.
	            $this->rulesAdapter->log($this->id, $this->name, time(), $login, '1', $couponCode, $login);
	
	            // Add customer-coupon mapping.
	            $this->dataAdapter->addCouponMapping($login, $couponCode);
				
	            $couponDetailMsg = $mailerDescription;
	        }
	   	}
        $ref_num_coupons = $this->mrpCouponConfiguration['mrp_refFirstPurchase_numCoupons'];
    	$ref_mrpAmount = $this->mrpCouponConfiguration['mrp_refFirstPurchase_mrpAmount'];
    	$reg_mrpAmount = $this->mrpCouponConfiguration['mrp_refRegistration_mrpAmount'];
    	$reg_num_coupons = $this->mrpCouponConfiguration['mrp_refRegistration_numCoupons'];
		
    	$couponMailerMsg = WidgetKeyValuePairs::getWidgetValueForKey("registrationMailerCouponMsg");
        $system_free_shipping_amount = WidgetKeyValuePairs::getWidgetValueForKey('shipping.charges.cartlimit');
    	// Mail the customer.
        global $http_location;
        $login_tw = str_replace("@", "%40", $login);
        $args = array (
            "USER"			=> $login,
            "USER_TW"		=> $login_tw,
            "COUPON_MSG"	=> $minValue,
			"COUPON_CODE"	 => $couponCode,
            "COUPON_DETAILS"	=> $couponDetailMsg,
            "SHIPPING_THRESHOLD" => $system_free_shipping_amount ?: 999,
        	"VALIDITY"		=> $validity,
            "REFERANDEARNURL"=> $http_location."/mymyntra.php?view=myreferrals&utm_source=MRP_mailer&utm_medium=".$utm_medium."&utm_campaign=refer_and_earn",
            "MRPLANDINGPAGE" => $http_location."/myntclub?utm_source=MRP_mailer&utm_medium=".$utm_medium."&utm_campaign=mrp_know_more",
         	
        );

        //Add active coupons;
        // Commenting out as $my_coupons not used here - 09/01/2015 RevLabs
        /*include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
        $couponAdapter = CouponAdapter::getInstance();
        $my_coupons = $couponAdapter->getActiveCouponsForUserInMyMyntra($login);
        */
        $i=1;
        $couponCodeHtml="";
        /*foreach($my_coupons as $couponKey=>$couponValue){
            $couponCodeHtml = $couponCodeHtml . '<span style="color:#333333;">Coupon '.$i.': <b>'.$couponValue.'</b><br/></span>';
            $i++;
            if($i==11){
                break;
            }
        }*/
        //$args["COUPON_CODE"]=$couponCodeHtml;
        

        $mail_detail = array(
                                "to"=>trim($login),
                    			"template"=>$template,
                    			"header"=>"$mrp_header",
        						"footer"=>"$mrp_footer",
                                "from_name"=>'Myntra.com',
                                "from_email"=>"no-reply@myntra.com",
                               	"mail_type"=> MailType::CRITICAL_TXN
        );
        $multiProviderMailer = new MultiProviderMailer($mail_detail, $args);
        return $multiProviderMailer->sendMail();
    }
    
    public function generate_refRegistration_coupons($login,$referer)
    {
		$num_coupons = $this->mrpCouponConfiguration['mrp_refRegistration_numCoupons'];		
		$mrpAmount = $this->mrpCouponConfiguration['mrp_refRegistration_mrpAmount'];
		
		$mrpPercentage = $this->mrpCouponConfiguration['mrp_refRegistration_mrpPercentage'];
		$couponType = $this->mrpCouponConfiguration['mrp_refRegistration_couponType'];
		
        $minCartValue = $this->mrpCouponConfiguration['mrp_refRegistration_minCartValue'];
        $validity =  $this->mrpCouponConfiguration['mrp_refRegistration_validity'];
        $description = "Referral of $login - Rs. $mrpAmount coupon";
	   		
        for ($i = 0; $i < $num_coupons; $i++) {
        	$startDate = strtotime(date('d F Y', time()));
            $endDate = $startDate + $validity * 24 * 60 * 60;
			$couponCode = $this->couponAdapter->generateSingleCouponForUser("Reg", "8", "MrpReferrals", $startDate, $endDate, $referer, $couponType, $mrpAmount, $mrpPercentage, $minCartValue, $description);
            
            // Logging success of registration of mrp.
            $this->rulesAdapter->log($this->id, $this->name, time(), $login, '1', $couponCode, $referer);

            // Add customer-coupon mapping.
            $this->dataAdapter->addCouponMapping($referer, $couponCode);
            if(isset($couponCodeString))	{
				$couponCodeString.=", ";
			}
			$couponCodeString.=$couponCode;
        }
        
        // Update mk_referral table with coupon codes
        func_array2update("mk_referral_log", array("regcompleteCoupon"=>$couponCodeString), "referer='$referer' and referred='$login'");	
		
        $ref_num_coupons = $this->mrpCouponConfiguration['mrp_refFirstPurchase_numCoupons'];
    	$ref_mrpAmount = $this->mrpCouponConfiguration['mrp_refFirstPurchase_mrpAmount'];
    	
		// Mail the referral
        global $http_location;
		$template = "mrp_ref_registration";
		$referer_tw = str_replace("@", "%40", $referer);	
		$args = 
            array (
                "REFERER"        => $referer,
                "REFERER_TW"	 => $referer_tw,
                "REFERRED"       => $login,
                "MRP_AMOUNT"     => $mrpAmount*$num_coupons,
            	"REF_AMOUNT"	 => $ref_num_coupons*$ref_mrpAmount,
                "REFERANDEARNURL"=> $http_location."/mymyntra.php?view=myreferrals&utm_source=MRP_mailer&utm_medium=referral_registration&utm_campaign=refer_and_earn",
                "MRPLANDINGPAGE" => $http_location."/myntclub?utm_source=MRP_mailer&utm_medium=referral_registration&utm_campaign=mrp_know_more",
            	"NUM_COUPONS"	=> $num_coupons,
        		"EACH_COUPON_VALUE" => $mrpAmount,
            	"VALIDITY"		=> $validity,
        		"MIN_PURCHASE_AMOUNT" => $minCartValue,
				"MRP_AMOUNT"  =>  $mrpAmount*$num_coupons
		);
		
		$mail_detail = array(
								"to"=>trim($referer),
		                    	"template"=>$template,
		                    	"header"=>"mrp_header",
								"footer"=>"mrp_footer",
		                        "from_name"=>'Mynt Club',
		                        "from_email"=>"no-reply@myntra.com",
		                        "mail_type"=> MailType::CRITICAL_TXN
		);
		$multiProviderMailer = new MultiProviderMailer($mail_detail, $args);
		//return $multiProviderMailer->sendMail();
    }
    /**
     * Action to be taken on a customer's first login.
     */
    public function first_login_action($login)
    {
    	$couponConfig = json_decode(WidgetKeyValuePairs::getWidgetValueForKey("registrationCouponConfig"),true);
    	
    	$num_coupons = $this->mrpCouponConfiguration['mrp_firstLogin_nonfbreg_numCoupons'];
    	$mrpAmount = $this->mrpCouponConfiguration['mrp_firstLogin_nonfbreg_mrpAmount'];
    	$minCartValue = $this->mrpCouponConfiguration['mrp_firstLogin_nonfbreg_minCartValue'];
        $validity =  $this->mrpCouponConfiguration['mrp_firstLogin_nonfbreg_validity'];
        
		$this->generate_first_login_coupons($login, $num_coupons,$mrpAmount,$minCartValue,$validity,"mrp_first_login_new","welcome_non_fb",$couponConfig);       

		$referer = func_query_first_cell("SELECT referer from mk_referral_log where referred='$login' and success='1';");
		if(!empty($referer)) {
			$this->generate_refRegistration_coupons($login,$referer);
		}	
	}
    /**
     * Action to be taken on a customer's first login through facebook.
     */
    public function first_fblogin_action($login)
    {
    	$num_coupons = $this->mrpCouponConfiguration['mrp_firstLogin_fbreg_numCoupons'];
    	$mrpAmount = $this->mrpCouponConfiguration['mrp_firstLogin_fbreg_mrpAmount'];
        $minCartValue = $this->mrpCouponConfiguration['mrp_firstLogin_fbreg_minCartValue'];
        $validity =  $this->mrpCouponConfiguration['mrp_firstLogin_fbreg_validity'];

        $couponConfig = json_decode(WidgetKeyValuePairs::getWidgetValueForKey("fbRegistrationCouponConfig"),true);
        
        $this->generate_first_login_coupons($login, $num_coupons,$mrpAmount,$minCartValue,$validity,"mrp_fbfirst_login_new","welcome_fb",$couponConfig); 
      
        //send referral coupon to referrer
        $referer = func_query_first_cell("SELECT referer from mk_referral_log where referred='$login' and success='1';");
        if(!empty($referer)) {
            $this->generate_refRegistration_coupons($login,$referer);
        }	
    }

    /**
     * First login failure action.
     */
    public function first_login_failure($login)
    {
        // Log failure.
        $this->rulesAdapter->log($this->id, $this->name, time(), $login, '0', '', '');
    }
    
    /**
     * Verification function for referral.
     */
    public function verify_referral($order)
    {
        $userName = $order->getLogin();
        $orderid = $order->getOrderId();
        echo "Order's userName: >>$userName<<\n";
        echo "no of orders: >>" . count($this->dataAdapter->getQueuedOrdersBefore($order->getQueuedDate(), $userName)) . "<<\n";
        
        if (count($this->dataAdapter->getQueuedOrdersBefore($order->getQueuedDate(), $userName)) > 0 ) {
            return false;
        }
        
        $customer = $this->dataAdapter->fetchCustomer($userName);
        $referer = $customer->getReferer();

        if(empty($referer))	{
        	return false;
        }
        
        return true;
    }
    
    /**
     * Action for verification and application for referral.
     */
    public function referral_action($order)
    {
        echo "inside referral_action\n";
        
        $userName = $order->getLogin();
        $customer = $this->dataAdapter->fetchCustomer($userName);
        $referer = $customer->getReferer();
        
        $num_coupons = $this->mrpCouponConfiguration['mrp_refFirstPurchase_numCoupons'];        
        
    	$mrpAmount = $this->mrpCouponConfiguration['mrp_refFirstPurchase_mrpAmount'];    	
    	$mrpPercentage = $this->mrpCouponConfiguration['mrp_refFirstPurchase_mrpPercentage'];    	
    	$couponType = $this->mrpCouponConfiguration['mrp_refFirstPurchase_couponType'];
    	
        $minCartValue = $this->mrpCouponConfiguration['mrp_refFirstPurchase_minCartValue'];
        $validity =  $this->mrpCouponConfiguration['mrp_refFirstPurchase_validity'];
        
        echo "customer: $userName, referer: $referer\n";
        for ($i = 0; $i < $num_coupons; $i++) {
            echo "inside for loop\n";
            
            $startDate = strtotime(date('d F Y', time()));
            $endDate = $startDate + $validity * 24 * 60 * 60;
            $desc = "Referral of $userName - Rs. $mrpAmount coupon";

            echo "will generate coupon..\n";
            
            $couponCode = $this->couponAdapter->generateSingleCouponForUser("REF", 8, "MrpReferrals", $startDate, $endDate, $referer, $couponType, $mrpAmount, $mrpPercentage, $minCartValue, $desc);
		
            echo "generated. adding log..\n";
            // Logging success of referral of Mrp.
            $this->rulesAdapter->log($this->id, $this->name, time(), $order->getOrderId(), '1', $couponCode, $referer);

            echo "adding mapping\n";
            // Add customer-coupon mapping.
            $this->dataAdapter->addCouponMapping($referer, $couponCode);
            
			if(isset($couponCodeString))	{
				$couponCodeString.=", ";
			}
			$couponCodeString.=$couponCode;
            echo "generated... coupon: >>$couponCode<< for referer: $referer\n";
        }


		//Update mk_referral_log to set the dateFirstPurchase 
		$firstQueuedDate = $order->getQueuedDate();
		func_array2update("mk_referral_log", array("dateFirstPurchase" =>$firstQueuedDate, "ordQueuedCoupon"=>$couponCodeString), 
		"referer='$referer' AND referred='$userName'");
				
        // Mail the referer.
        $template = "mrp_referral_successful";
        $referer_tw = str_replace("@", "%40", $referer);
        global $http_location;
        $args = 
            array (
                "REFERER"        => $referer,
                "REFERER_TW"	 => $referer_tw,
                "REFERRED"       => $userName,
                "MRP_AMOUNT"     => $mrpAmount * $num_coupons,
                "REFERANDEARNURL"=> $http_location."/mymyntra.php?view=myreferrals&utm_source=MRP_mailer&utm_medium=referral_first_purchase&utm_campaign=refer_and_earn",
                "MRPLANDINGPAGE" => $http_location."/myntclub?utm_source=MRP_mailer&utm_medium=referral_first_purchase&utm_campaign=mrp_know_more",
            	"NUM_COUPONS"	=> $num_coupons,
        		"EACH_COUPON_VALUE" => $mrpAmount,
            	"VALIDITY"		=> $validity,
        		"MIN_PURCHASE_AMOUNT" => $minCartValue
        );
        
        $subject_args=array ("MRP_AMOUNT"  => $mrpAmount * $num_coupons);
        //sendMessageDynamicSubject($template, $args, $referer, "MRP",$subject_args);
        
    }
    
    /**
     * Referral failure action.
     */
    public function referral_failure($order)
    {
        //commenting it because lots of orders getting logged in mk_mrp_logs table with failure results
        //$this->rulesAdapter->log($this->id, $this->name, time(), $order->getOrderId(), '0', '', '');
    }
    
    /**
     * success action for registration first coupon usage 
     * 
     * @param string $login
     */
    public function reg_coupon_expiry_extension($coupon){
    	
    	$noOfdays = FeatureGateKeyValuePairs::getInteger("newLoginCouponExtendDays",0);
    	$couponAdapter = CouponAdapter::getInstance();
    	try{
    		$couponObject = $couponAdapter->fetchCoupon($coupon);
    	}catch(Exception $e){
    		print_r($e);
    		return false;
    	}
    	
    	$login = $couponObject->getUsers();
    	$couponAdapter->extendExpiryForCoupons($login, null, $noOfdays,"FirstLogin");
    	
    	//logging success , this prevents multiple application of the rule
    	$this->rulesAdapter->log($this->id, $this->name, time(),$coupon, '1', "", $login);
    }
    
    /**
     * registration first coupon usage failure action.
     * 
     */
    public function reg_coupon_expiry_extension_fail($coupon)
    {
    	// Log failure.
    	$this->rulesAdapter->log($this->id, $this->name, time(), $coupon, '0', '', '');
    }
    
}
