<?php

include "$xcart_dir/modules/coupon/exception/CouponAmountException.php";
include "$xcart_dir/modules/coupon/exception/CouponCountException.php";
include "$xcart_dir/modules/coupon/exception/CouponNotApplicableException.php";
include "$xcart_dir/modules/coupon/exception/CouponCorruptedException.php";
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";


/**
 * This singleton takes input a coupon and a cart, and calculates discount on 
 * the cart and each product in turn.
 * 
 * @author Rohan.Railkar
 */
class CouponDiscountCalculator
{
    /**
     * Singleton instance.
     */
    private static $calculator = NULL;
    
    /**
     * Coupon database adapter.
     */
    private $adapter;
    
    /**
     * Constructor.
     */
    private function __construct()
    {
        $this->adapter = CouponAdapter::getInstance();
    }
    
    /**
     * Access the singleton instance.
     */
    public static function getInstance()
    {
        if (CouponDiscountCalculator::$calculator == NULL) {
            CouponDiscountCalculator::$calculator =
                new CouponDiscountCalculator();
        }
        
        return CouponDiscountCalculator::$calculator;
    }
    
    /**
     
     *This function checks whether the given coupon group is special.
     *A coupon belonging to special group can be applied on specific styleids
    
    */

    private function isSpecialCouponGroup($couponGroup){
   	//return false;
    	$specialStylesCouponGroup = (string)WidgetKeyValuePairs::getWidgetValueForKey('specialStylesCouponGroup');
	$specialGroups = array_map('strtolower', explode(",", $specialStylesCouponGroup));
	return in_array(strtolower($couponGroup), $specialGroups);
    }
    
    private function isFilterConditionCouponGroup($couponGroup){
    	//return false;
    	$specialFilterAppliedCouponGroup = json_decode(WidgetKeyValuePairs::getWidgetValueForKey('specialFilterAppliedCouponGroup'),true);
    	if(array_key_exists($couponGroup, $specialFilterAppliedCouponGroup)){ 
    		return $specialFilterAppliedCouponGroup[$couponGroup]; 		    		
    	}else{
    		return false;
    	}
    	
    }
    	
    /**
     * Apply the coupon on the cart. Modifies the cart and internal product
     * objects to include discount amounts.
     * <br>
     * NOTE: This function should be called only after validating the coupon
     * for the user. 
     * 
     * @param Coupon $coupon
     * @param Cart $cart
     * 
     * @return void
     * 
     * @throws CouponNotApplicableException
     * @throws CouponAmountException
     * @throws CouponCorruptedException
     */

    public function applyCouponOnCart(Coupon $coupon, Cart $cart)
    {
     
    // Feature gate condition to enable Non-discount-products coupon
    // Use these feature in class.MCart.php
		$condNoDiscountCoupon = FeatureGateKeyValuePairs::getBoolean('condNoDiscountCoupon');
		$styleExclusionFG = FeatureGateKeyValuePairs::getBoolean('styleExclusion');                
		/* 
		 *	  These two conditions enables the coupon on discounted items as well :
   		 *	  $condApplyOnAllCoupon = true // through featureGate
	         *        coupon's group is in the widget corresponding to key "applyOnAllCouponGroups" 
		 */
		$condApplyOnAllCoupon = FeatureGateKeyValuePairs::getBoolean('condApplyOnAllCoupon');
		

		if($condApplyOnAllCoupon && Coupon::isApplyOnAllCouponGroup($coupon->getGroupName())){
			$condNoDiscountCoupon = false;
		}

		$enableSpecialCoupon = (string)FeatureGateKeyValuePairs::getFeatureGateValueForKey('enableSpecialCoupon');
		
		if($enableSpecialCoupon == 'true' && Coupon::isSpecialCouponGroup($coupon->getGroupName())){
			$styleExclusionFG = false;
		}
                
                $coupon_users = $coupon->getUsers();
                $noStyleExclusionForUserCoupon = FeatureGateKeyValuePairs::getBoolean('noStyleExclusionForUserCoupon');
                if($coupon_users != '' && $noStyleExclusionForUserCoupon) {
                    $styleExclusionFG = false;
                }

                $noStyleExclusionForSomeGroups = FeatureGateKeyValuePairs::getBoolean('noStyleExclusionForSomeGroups');
                if($noStyleExclusionForSomeGroups && 
                            \Coupon::isCouponGroupExists('noStyleExclusionForSomeGroups', $coupon->getGroupName())) {
                    $styleExclusionFG = false;
                }

	        //exempting MYNTRASHOP Coupon , if login is different from @myntra.com, coupon is caught while checking the validity against user 
		
		if($coupon->getCode()=="HappyHour25"){
			$condNoDiscountCoupon=false;
		}
		
		// Get an array applicable product IDs in the sorted order of unit prices.
		$applicableProductsPriceList=array();
		$isFilterConditionCoupon=false;
		$applicableProductsCount=0;
		$applicableProducts = $this->getRedeemableProducts($coupon, $cart,$condNoDiscountCoupon,$styleExclusionFG,
															$applicableProductsPriceList,$isFilterConditionCoupon,$applicableProductsCount);
        $cart->setCouponApplicableCartItems($applicableProducts);
                
		// Check cart subtotal.
		// after excluding the sum of prior discounted products
		
		/*if($styleExclusionFG) {
			$cartSubtotal = $cart->getCouponApplicableItemsSubtotal($condNoDiscountCoupon);
		}
		else if ($condNoDiscountCoupon){
			$cartSubtotal = $cart->getNonDiscountedItemsSubtotal();
		}else {
			$cartSubtotal = $cart->getCartSubtotal();
		}*/
		
		
        // Coupon cannot be redeemed on any of the products in the cart.
        if (empty($applicableProducts)) {
          	$enableSpecialCoupon = (string)FeatureGateKeyValuePairs::getFeatureGateValueForKey('enableSpecialCoupon');      
			$couponCode = $coupon->getCode();
            $couponGroup = (string)$coupon->getGroupName();
			if($enableSpecialCoupon == 'true'){
	                	//$specialStylesCouponGroup = (string)WidgetKeyValuePairs::getWidgetValueForKey('specialStylesCouponGroup');
	    	        	//if(strcasecmp($couponGroup, $specialStylesCouponGroup) == 0){
	       	            if($this->isSpecialCouponGroup($couponGroup)){
								throw new CouponNotApplicableException($coupon->getCode(), 1);
	                	}
	                	else{
	                       		throw new CouponNotApplicableException($coupon->getCode());
	                	}
			}
			else{
				if($isFilterConditionCoupon){
					throw new CouponNotApplicableException($coupon->getCode(), 1);
				}else{
					throw new CouponNotApplicableException($coupon->getCode());
				}
			}
        }

		$cartApplicableSubTotal = $cart->getCouponApplicableSubtotal($applicableProductsPriceList);
		
        if (!$coupon->isAmountAllowed($cartApplicableSubTotal)) {
            throw new CouponAmountException($coupon->getCode(),
                                            $coupon->getMinimum(),
                                            $coupon->getMaximum(),$applicableProducts);
        }
        if(!$coupon->isCountAllowed( $applicableProductsCount )){
        	throw  new CouponCountException($coupon->getCode(),
        	                                            $coupon->getMinimumCount(),
        	                                            $coupon->getMaximumCount(),$applicableProducts);
        }

        
        // Check if the discount is not applicable on specific products.
        if ($coupon->isCartLevelDiscount()) {
            $this->applyCouponDiscount($coupon, $applicableProducts, $cart);
            
            // Easiest case!
            return;
        }
        
        
        // Otherwise, apply the discount across all applicable products.
        $this->applyCouponDiscount($coupon, $applicableProducts, $cart);
    }
    
    /**
     * Apply the coupon discount on all applicable products from the cart.
     * NOTE: $applicableProducts IDs are sorted in descending order of unit 
     * price.
     */
    private function applyCouponDiscount(Coupon $coupon,
                                         $applicableProducts,
                                         Cart $cart)
    {
        /*
         * if max_percentage type coupon
         */
        if($coupon->getType() == 'max_percentage' || $coupon->getType() == 'max_percentage_dual'){
        	$this->applyMaxTypeCoupon($coupon, $applicableProducts, $cart);
        	return;
        }
    	    	
    	
        // Total discountable cost.
        $applicableSubtotal =
            $this->calculateApplicableSubtotal($cart,
                                               $applicableProducts,
                                               $coupon->getMaxUsagePerCart());

        // Total discount.
        $discount =
            $this->calculateCouponDiscountOnPrice($coupon, $applicableSubtotal);
            
        // No of items on which the coupon can be applied.
        $count = $coupon->getMaxUsagePerCart();
        
        // No of items in the cart on which the discount has been applied.
        $cartCount = 0;
        
        // Apply the discount on all products in the ratio of their total cost.
        foreach ($applicableProducts as $id) {
            if (($count < 0) || ($cartCount < $count)) {
                $product = $cart->getProduct($id);
                $productCount = $product->getQuantity();
                
                $applyCount =
                    (($count < 0) || ($productCount <= ($count - $cartCount)))
                        ? $productCount 
                        : ($count - $cartCount);
                
                $productSubtotal =
                    (($product->getProductPrice()
                      + $product->getUnitAdditionalCharges())
                     * $applyCount - ($product->getDiscountAmount()*$product->discountQuantity ) - $product->getCartDiscount());
            
                $productDiscount =
                    $discount * $productSubtotal / $applicableSubtotal;
                
                $product->setCouponDiscount($productDiscount);
                $product->setCouponCode($coupon->getCode());
                
                $cartCount += $applyCount;
            }
        }
        

        $cart->setCouponAppliedItemCount($cartCount);
        $cart->setCouponAppliedSubtotal($applicableSubtotal);
        // Apply the discount on the cart.
        $cart->setCouponDiscount($discount);
    }
    
    /*
     * apply max_percentage type coupons
     */    
    private function applyMaxTypeCoupon(Coupon $coupon,
                                         $applicableProducts,
                                         Cart $cart){
    	
    	// No of items on which the coupon can be applied.
    	$count = $coupon->getMaxUsagePerCart();
    	
    	// No of items in the cart on which the discount has been applied.
    	$cartCount = 0;
    	
    	$couponDiscount = 0.0;
    	
    	$applicableSubtotal = 0.0;
    	
    	// Apply the discount on all products in the ratio of their total cost.
    	foreach ($applicableProducts as $id) {
    		if (($count < 0) || ($cartCount < $count)) {
    			$product = $cart->getProduct($id);
    			$productCount = $product->getQuantity();
    	
    			$applyCount =
    			(($count < 0) || ($productCount <= ($count - $cartCount)))
    			? $productCount
    			: ($count - $cartCount);
    	
    			$productTotal =
    			(($product->getProductPrice()
    					+ $product->getUnitAdditionalCharges())
    					* $applyCount );
    			
    			// should prorate the discounts item wise and consolidate to appplycount
    			$productItemCartDiscount =  ($product->getDiscountAmount()*$product->discountQuantity ) + $product->getCartDiscount();
    	
    			$appliedPercentDiscount = round(100*$productItemCartDiscount / $productTotal,2);
    			
    			
    			if($coupon->getType() == "max_percentage" ){
    				if( $appliedPercentDiscount < $coupon->getPercentDiscount()){
    					$productCouponDiscount = (($coupon->getPercentDiscount() - $appliedPercentDiscount) * $productTotal)/100.0;
    					//$discount * $productSubtotal / $applicableSubtotal;

    					$couponDiscount += $productCouponDiscount;
    					$applicableSubtotal += $productTotal;

    					$product->setCouponDiscount($productCouponDiscount);
    					$product->setCouponCode($coupon->getCode());
    					$cartCount += $applyCount;
    				}
    			}else if($coupon->getType() == "max_percentage_dual" ){
    				
    				$productCouponDiscount = (($coupon->getPercentDiscount()) * ($productTotal-$productItemCartDiscount))/100.0;
    				
    				$productCouponDiscount = round($productCouponDiscount,2);
    				
    				$finalDiscountOverAll = 100 * ($productCouponDiscount + $productItemCartDiscount)/$productTotal;
    				
    				$finalDiscountOverAll = round($finalDiscountOverAll,2);
    				
    				if($finalDiscountOverAll > $coupon->getAbsoluteDiscount()){
    					
    					  					
    					$productCouponDiscount = (($coupon->getAbsoluteDiscount() - $appliedPercentDiscount) * $productTotal)/100.0;   					
    					
    					$productCouponDiscount = round($productCouponDiscount,2);
    				}
    				
    			}
    			 
    			
    		}
    	}
    	
    	
    	$cart->setCouponAppliedItemCount($cartCount);
    	$cart->setCouponAppliedSubtotal($applicableSubtotal);
    	// Apply the discount on the cart.
    	$cart->setCouponDiscount($couponDiscount);
    	
    }
    
    /**
     * Find the total cost of products with given ID from the cart.
     * NOTE: Product $ids are in descend order of unit price. 
     */
    public function calculateApplicableSubtotal(Cart $cart, $ids, $count)
    {
        // Return value.
        $subtotal = 0.0;
        
        // Number of items on which discount has been applied.
        $cartCount = 0;
        
        // Iterate on the applicable products until 
        foreach ($ids as $id) {
            if (($count < 0) || ($cartCount < $count)) {
                $product = $cart->getProduct($id);
                $productCount = $product->getQuantity();
                
                $applyCount =
                    (($count < 0) || ($productCount <= ($count - $cartCount)))
                        ? $productCount 
                        : ($count - $cartCount);
                
                $subtotal +=
                    (($product->getProductPrice()
                      + $product->getUnitAdditionalCharges())
                     * $applyCount)- ($product->getDiscountAmount()*$product->discountQuantity) - $product->getCartDiscount();
                
                $cartCount += $applyCount;
            }
        }
        
        return $subtotal;
    }
    
    /* 
	Function to get list of style Ids for $couponCode
	Use of row-level cache
    */
    private function getSpecialCouponStyleIds($couponGroup){
	
	//$this->cache = new \Cache("SpecialCoupon:".$couponCode);
	//$specialCouponStyleIds = $this->cache->get(function($couponCode){ 
		
		$result = array();
		/*$tableList = func_query("show tables like 'mk_coupon_allowed_style_ids'");
		$tableExist = !(empty($tableList));
		if(!$tableExist) return $result;*/
		
		$query = "select styleIdList from mk_coupon_allowed_style_ids where coupon_group = '$couponGroup'";
		$result = func_query($query, true);
		return $result;
		
	//	}, array($couponCode), "styleIdList");
	//return $specialCouponStyleIds;
    }

    private function getStyleIdExcludedForCouponGroup($styleId, $couponGroup){
        $result = array();
        $query = "select * from mk_coupon_excluded_style_ids where coupon_group = '$couponGroup' and style_id = '$styleId'";
        $result = func_query($query, true);
        if (empty($result)) {return false;}
        return true;
    } 

    /**
     * Find IDs of products in the cart on which the coupon is applicable.
     * 
     * @param Coupon $coupon
     * @param Cart $cart
     * 
     * @return array of IDs of redeemable products in the cart
     */
    private function getRedeemableProducts(Coupon $coupon, Cart $cart,$condNoDiscountCoupon,$styleExclusionFG,&$applicableProductsList,&$isFilterConditionCoupon,&$applicableProductsCount)
    {
        $coupon_users = $coupon->getUsers();
        $noStyleExclusionForUserCoupon = FeatureGateKeyValuePairs::getBoolean('noStyleExclusionForUserCoupon');
        if($coupon_users != '' && $noStyleExclusionForUserCoupon) {
            $styleExclusionFG = false;
        }

        $noStyleExclusionForSomeGroups = FeatureGateKeyValuePairs::getBoolean('noStyleExclusionForSomeGroups');
        if($noStyleExclusionForSomeGroups && 
                        \Coupon::isCouponGroupExists('noStyleExclusionForSomeGroups', $coupon->getGroupName())) {
            $styleExclusionFG = false;
        }

	$enableSpecialCoupon = (string)FeatureGateKeyValuePairs::getFeatureGateValueForKey('enableSpecialCoupon');
	$enableCouponGroupStyleExclusion = (string)FeatureGateKeyValuePairs::getFeatureGateValueForKey('enableCouponGroupStyleExclusion');

        $priceArray = array();
        $totalPriceArray = array();
        $isCartLevelCoupon = $coupon->isCartLevelDiscount();
	        
	        // Iterate over the products in the cart to find applicable products.
		
		$couponCode = $coupon->getCode();
		$couponGroup = (string)$coupon->getGroupName();
		
		$isSpecialGroup = $this->isSpecialCouponGroup($couponGroup);
		
		$isFilteredGroup = $this->isFilterConditionCouponGroup($couponGroup);
		
		if($isFilteredGroup){
			
			$isFilterConditionCoupon = true;
			
			$exclusionBrands = $isFilteredGroup["exclusion"]["brands"];
			$exclusionGender = $isFilteredGroup["exclusion"]["gender"];
			$exclusionLabels = $isFilteredGroup["exclusion"]["labels"];
			
			$inclusionBrands = $isFilteredGroup["inclusion"]["brands"];
			$inclusionGender = $isFilteredGroup["inclusion"]["gender"];
			$inclusionLabels = $isFilteredGroup["inclusion"]["labels"];
                        
                        $exclusionMessage = $isFilteredGroup["exclusionMessage"];
		}
                
		$couponNotApplicableProducts = array();
                $couponNotApplicableProducts[-1] = "";
                
                $couponApplicabilityMessage = array();
                
                if($isFilteredGroup) {
                    array_push($couponApplicabilityMessage, $exclusionMessage);
                }
                if($styleExclusionFG) {
                    $styleExclusionMessage = (string)WidgetKeyValuePairs::getWidgetValueForKey('style-exclusion-message');
                    array_push($couponApplicabilityMessage, $styleExclusionMessage);
                }
                if($condNoDiscountCoupon) {
                    $discountedProductMessage = (string)WidgetKeyValuePairs::getWidgetValueForKey('coupon-discounted-exclusion-message');
                    array_push($couponApplicabilityMessage, $discountedProductMessage);
                }
                if($enableSpecialCoupon && $isSpecialGroup) {
                    $specialCouponMessage = (string)WidgetKeyValuePairs::getWidgetValueForKey('special-coupon-exclusion-message');
                    array_push($couponApplicabilityMessage, $specialCouponMessage);
                }
                
		//Commented for hotfix , this new table logic causing the problem
		$cartStyleIds = $cart->getStyleIds();
		$sql = "select style_id from mk_coupon_included_style_ids where coupon_group ='".$couponGroup."' and style_id IN (".implode(",",$cartStyleIds).")";
		$inclusionStyles = func_query_column($sql,0,true); 
		
		foreach ($cart->getProducts() as $id=>$product) {
			
			
			$productStyleId = $product->getProductStyleId();
	
			/*
			 * filters at the level of brands, labels, gender
			 */
			if($isFilteredGroup){
				$styleInfo = $product->getStyleArray();
				
				/*
				 * exclusion block
				 */
				
				if(sizeof($exclusionBrands)>0){
					if(in_array(strtolower($styleInfo["brand"]),$exclusionBrands)){
                                                $couponNotApplicableProducts[$id] = $exclusionMessage;
						continue;
					}
				}
				
				if(sizeof($exclusionGender)>0){
					if(in_array(strtolower($styleInfo["gender"]),$exclusionGender)){
                                                $couponNotApplicableProducts[$id] = $exclusionMessage;
						continue;
					}
				}
				
				if(sizeof($exclusionLabels)>0){
					if(in_array($styleInfo["article_type"],$exclusionLabels)){
                                                $couponNotApplicableProducts[$id] = $exclusionMessage;
						continue;
					}
				}
				
				/*
				 * inclusion block
				 */
				
				if(sizeof($inclusionBrands)>0){
					if(!in_array(strtolower($styleInfo["brand"]),$inclusionBrands)){
                                                $couponNotApplicableProducts[$id] = $exclusionMessage;
						continue;
					}
				}
				
				if(sizeof($inclusionGender)>0){
					if(!in_array(strtolower($styleInfo["gender"]),$inclusionGender)){
                                                $couponNotApplicableProducts[$id] = $exclusionMessage;
						continue;
					}
				}
				
				if(sizeof($inclusionLabels)>0){
					if(!in_array($styleInfo["article_type"],$inclusionLabels)){
                                                $couponNotApplicableProducts[$id] = $exclusionMessage;
						continue;
					}
				}
				
				
			}
			
			
			
			 
			// *check whether the product is a discounted product to skip coupon application
			// Update this condition too
			// remove this if else and use couponApplicableSubtotal
			
			if($styleExclusionFG && (!$product->isCouponApplicable())) {
                                $styleExclusionMessage = (string)WidgetKeyValuePairs::getWidgetValueForKey('cart-tooltip-for-coupon');
                                $couponNotApplicableProducts[$id] = $styleExclusionMessage;
				continue;
			}
			 
			if ($condNoDiscountCoupon && ($product->isDiscountedProduct()) ){
                                $discountedProductMessage = (string)WidgetKeyValuePairs::getWidgetValueForKey('coupon-discounted-exclusion-message');
                                $couponNotApplicableProducts[$id] = $discountedProductMessage;
				continue;
			}
			 
			if($enableSpecialCoupon == 'true'){
				if( $isSpecialGroup && !in_array($productStyleId,$inclusionStyles)) {
                                    $specialCouponMessage = (string)WidgetKeyValuePairs::getWidgetValueForKey('special-coupon-exclusion-message');
                                    $couponNotApplicableProducts[$id] = $specialCouponMessage;
                                    continue;
                                }
			}
			
            if($enableCouponGroupStyleExclusion == 'true'){
                if($this->getStyleIdExcludedForCouponGroup($productStyleId,$couponGroup)){
                                    $styleExcludedCouponMessage = (string)WidgetKeyValuePairs::getWidgetValueForKey('style-coupongroup-exclusion-message');
                                    $couponNotApplicableProducts[$id] = $styleExcludedCouponMessage;
                                    continue;
                }
            }

			if ($isCartLevelCoupon || $this->isCouponApplicableOnProduct($coupon, $product)) {
				$priceArray[$id] = $product->getProductPrice() + $product->getUnitAdditionalCharges();
				$totalPriceArray[] = $priceArray[$id] * $product->getQuantity() - ($product->getDiscountAmount()*$product->getDiscountQuantity() ) - $product->getCartDiscount();
				$applicableProductsCount = $applicableProductsCount + $product->getQuantity();
			}
		}

        $cart->setCouponNotApplicableCartItems($couponNotApplicableProducts);
        $cart->setCouponApplicabilityMessage($couponApplicabilityMessage);
                
        // Sort the unit price array in descending unit prices order.
        arsort($priceArray);
        $applicableProductsList = $totalPriceArray;
        return array_keys($priceArray);
    }
    
    public function isCouponApplicableOnAnyProduct(Coupon $coupon, Cart $cart){
    	// Iterate over the products in the cart to find applicable products.
        foreach ($cart->getProducts() as $id=>$product) {
        	if ($this->isCouponApplicableOnProduct($coupon, $product)) {
				return TRUE;				              	
            }
        }
        return FALSE;	
    }
    
        
    /**
     * @return boolean Whether given coupon is applicable for the given product.
     * 
     * NOTE: This function should not be called for Cart-Level coupons.
     * Cart-Level coupons are those where all fields like PT-ID, PS-ID, Cat-ID 
     * are empty.
     */
    private function isCouponApplicableOnProduct(Coupon $coupon,
                                                 Product $product)
    {
        // Return value.
        $return = FALSE;
        
        // Product properties
        $productId = $product->getProductId();
        $productStyleId = $product->getProductStyleId();
        
        $productTypeId = $product->getProductTypeId();
        $productCategories =
            $this->adapter->getCategoriesForProduct($productId);
            
        // Coupon properties
        $couponPTs = trim_all(explode(",", $coupon->getPTIds()));
        $couponPSs = trim_all(explode(",", $coupon->getPSIds()));
        
        $couponCategories = trim_all(explode(",", $coupon->getCategories()));
        $couponSKUs = trim_all(explode(",", $coupon->getSKUs()));
        $couponExclPTs = trim_all(explode(",", $coupon->getExclPTIds()));
        $couponExclPSs = trim_all(explode(",", $coupon->getExclPSIds()));
        $couponExclCategories =
            trim_all(explode(",", $coupon->getExclCategories()));
        $couponExclSKUs = trim_all(explode(",", $coupon->getExclSKUs()));
        
        // If the product's PT/PS/SKU matches with a specified value in coupon.
        if (in_array($productTypeId, $couponPTs) ||
            in_array($productStyleId, $couponPSs) ||
            in_array($productId, $couponSKUs))
        {
            $return = TRUE;
        }
        
        //
        // Check if any of the product's category appears in any of the 
        // specified categories.
        //
        foreach ($productCategories as $category) {
            if (in_array($category, $couponCategories)) {
                $return = TRUE;
            }
        }
        
        //
        // If the product's PT/PS/SKU matches with a specified value to be 
        // excluded in coupon.
        //
        if (in_array($productTypeId, $couponExclPTs) ||
            in_array($productStyleId, $couponExclPSs) ||
            in_array($productId, $couponExclSKUs))
        {
            $return = FALSE;
        }
        
        //
        // Check if any of the product's category appears in any of the 
        // specified excluded categories.
        //
        foreach ($productCategories as $category) {
            if (in_array($category, $couponExclCategories)) {
                $return = FALSE;
            }
        }
        
        return $return;
    }
    
    /**
     * For absolute discount, simply return the amount of coupon discount.
     * For percent discount, calculate the percentage on given price.
     * 
     * @param Coupon $coupon
     * @param double $price The price of cart, the product or the item.
     * 
     * @throws CouponCorruptedException
     */
    private function calculateCouponDiscountOnPrice(Coupon $coupon, $price, $mrpPrice = 0)
    {
        $type = $coupon->getType();
        
        // If the coupon gives dual discount.
        if ($type == 'dual') {
            $abs = $coupon->getAbsoluteDiscount();
            $discount = $price * $coupon->getPercentDiscount() / 100;
            $discount = ($discount > $abs) ? $abs : $discount;
        }
        
        // If the coupon gives absolute discount.
        elseif ($type == 'absolute') {
            $discount = $coupon->getAbsoluteDiscount();
        }
        
        // If the coupon gives percentage discount.
        elseif ($type == 'percentage') {
            $discount = $price * $coupon->getPercentDiscount() / 100;
        }
        
        elseif ($type == 'max_percentage_dual'){
        	$productDiscount = (1 - $price / $mrpPrice) * 100;
        	$discount = $price * $coupon->getPercentDiscount() / 100; // coupon discount
        	$couponAndProductDiscountPercent = 100 - (($price-$discount)/$mrpPrice)*100;        	
        	
        	if(($couponAndProductDiscountPercent) > $coupon->getAbsoluteDiscount()){
        		$difDiscount = ($coupon->getAbsoluteDiscount() - $productDiscount);
        		
        		if($difDiscount > 1){
        			$discount = ($mrpPrice * $difDiscount) / 100;
        		}else{
        			$discount = 0;
        		}
        	}
        }elseif ($type == 'max_percentage'){
        	$productDiscount = (1 - $price / $mrpPrice) * 100;
        	$discount = $price * $coupon->getPercentDiscount() / 100;
        	       	
        	$difDiscount = ($coupon->getPercentDiscount() - $productDiscount);
               		
        		if($difDiscount > 1){
        			$discount = ($mrpPrice * $difDiscount) / 100;
        		}else{
        			$discount = 0;
        		}
        	
        }
        
        // Abnormal coupon.
        else {
            $couponCode = $coupon->getCode();
            throw new CouponCorruptedException(
                $couponCode,
                "Unrecognized coupon type for coupon $couponCode.");
        }
        
        return ($discount > $price) ? $price : $discount;
    }

    /**
    *  Public Function to return discouted price after applying coupon
    *
    *	@param $discountedPrice : product price after item discount
    *	@param $MrpPrice : product price before discounts
    *
    ***/
     public function getCouponDiscountedPrice(Coupon $coupon,$discountedPrice, $MrpPrice=0){

        $finalDiscount = $this->calculateCouponDiscountOnPrice($coupon, $discountedPrice,$MrpPrice);
        return $discountedPrice - $finalDiscount;    

     }
}
