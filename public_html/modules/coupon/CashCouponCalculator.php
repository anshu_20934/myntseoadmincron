<?php

include_once "$xcart_dir/modules/coupon/exception/CouponAmountException.php";
include_once "$xcart_dir/modules/coupon/exception/CouponNotApplicableException.php";
include_once "$xcart_dir/modules/coupon/exception/CouponCorruptedException.php";

/**
 * This singleton takes input a coupon and a cart, and calculates cash discount on
 * the cart and each product in turn.
 *
 * @author Rohan.Railkar
 */
class CashCouponCalculator
{
    /**
     * Singleton instance.
     */
    private static $calculator = NULL;

    /**
     * Coupon database adapter.
     */
    private $adapter;

    /**
     * Constructor.
     */
    private function __construct()
    {
        $this->adapter = CouponAdapter::getInstance();
    }

    /**
     * Access the singleton instance.
     */
    public static function getInstance()
    {
        if (CashCouponCalculator::$calculator == NULL) {
            CashCouponCalculator::$calculator =
                new CashCouponCalculator();
        }

        return CashCouponCalculator::$calculator;
    }

    /**
     * Apply the coupon on the cart. Modifies the cart and internal product
     * objects to include discount amounts.
     * <br>
     * NOTE: This function should be called only after validating the coupon
     * for the user.
     *
     * @param Coupon $coupon
     * @param Cart $cart
     * @param string $userAmount
     *
     * @return void
     *
     * @throws CouponNotApplicableException
     * @throws CouponAmountException
     * @throws CouponCorruptedException
     */
    public function applyCouponOnCart(Coupon $coupon, Cart $cart,$userEnteredAmount)
    {
    	
        // Check cart subtotal.
        $cartSubtotal = $cart->getCartSubtotal();
        if (!$coupon->isAmountAllowed($cartSubtotal)) {
            throw new CouponAmountException($coupon->getCode(),
                                            $coupon->getMinimum(),
                                            $coupon->getMaximum());
        }

        // Get an array applicable product IDs in the sorted order of unit prices.
        $applicableProducts = $this->getRedeemableProducts($coupon, $cart);

        // Check if the discount is not applicable on specific products.
        if ($coupon->isCartLevelDiscount()) {
            $this->applyCouponDiscount($coupon, $applicableProducts, $cart,$userEnteredAmount);

            // Easiest case!
            return;
        }
        // Coupon cannot be redeemed on any of the products in the cart.
        if (empty($applicableProducts)) {
            throw new CouponNotApplicableException($coupon->getCode());
        }

        // Otherwise, apply the discount across all applicable products.
        $this->applyCouponDiscount($coupon, $applicableProducts, $cart,$userEnteredAmount);
    }

    /**
     * Apply the coupon discount on all applicable products from the cart.
     * NOTE: $applicableProducts IDs are sorted in descending order of unit
     * price.
     */
    private function applyCouponDiscount(Coupon $coupon,
                                         $applicableProducts,
                                         Cart $cart,$userAmount)
    {
        // Total discountable cost.
        $applicableSubtotal =
            $this->calculateApplicableSubtotal($cart,
                                               $applicableProducts,
                                               $coupon->getMaxUsagePerCart());

        // Total discount.
        $discount =
            $this->calculateCouponDiscountOnPrice($coupon, $applicableSubtotal);
            
        if($userAmount<$discount){
        	$discount = $userAmount;
        }    
        // No of items on which the coupon can be applied.
        $count = $coupon->getMaxUsagePerCart();

        // No of items in the cart on which the discount has been applied.
        $cartCount = 0;

        // Apply the discount on all products in the ratio of their total cost.
        foreach ($applicableProducts as $id) {
            if (($count < 0) || ($cartCount < $count)) {
                $product = $cart->getProduct($id);
                $productCount = $product->getQuantity();

                $applyCount =
                    (($count < 0) || ($productCount <= ($count - $cartCount)))
                        ? $productCount
                        : ($count - $cartCount);

                $productSubtotal =
                    (($product->getProductPrice()
                      + $product->getUnitAdditionalCharges())
                     * $applyCount) - $product->getDiscount() - ($product->getDiscountAmount() * $product->getDiscountQuantity()) - $product->getCartDiscount();

                $productDiscount =
                    $discount * $productSubtotal / $applicableSubtotal;

                $product->setCashCouponDiscount($productDiscount);
                $product->setCashCouponCode($coupon->getCode());

                $cartCount += $applyCount;
                $totalCashDiscountOffered += $productDiscount;
            }
        }
        
        $remainingCashAmount = $userAmount - $discount;
        $shippingOrGiftingCharges = $cart->getShippingCharge() + $cart->getGiftCharge();
        if( $remainingCashAmount > 0 && $shippingOrGiftingCharges > 0) {
        	if($remainingCashAmount > $shippingOrGiftingCharges)
        		$discount += $shippingOrGiftingCharges;
        	else
        		$discount += $remainingCashAmount;
        }
        
        // Apply the discount on the cart.
        $cart->setCashCouponDiscount($discount);
    }

    /**
     * Find the total cost of products with given ID from the cart.
     * NOTE: Product $ids are in descend order of unit price.
     */
    private function calculateApplicableSubtotal(Cart $cart, $ids, $count)
    {
        // Return value.
        $subtotal = 0.0;

        // Number of items on which discount has been applied.
        $cartCount = 0;

        // Iterate on the applicable products until
        foreach ($ids as $id) {
            if (($count < 0) || ($cartCount < $count)) {
                $product = $cart->getProduct($id);
                $productCount = $product->getQuantity();

                $applyCount =
                    (($count < 0) || ($productCount <= ($count - $cartCount)))
                        ? $productCount
                        : ($count - $cartCount);

                $subtotal +=
                    (($product->getProductPrice()
                      + $product->getUnitAdditionalCharges() )
                     * $applyCount) - $product->getDiscount() - ($product->getDiscountAmount()*$product->getDiscountQuantity());

                $cartCount += $applyCount;
            }
        }
        
        $subtotal -= $cart->getCartLevelDiscount();
        
        return $subtotal;
    }

    /**
     * Find IDs of products in the cart on which the coupon is applicable.
     *
     * @param Coupon $coupon
     * @param Cart $cart
     *
     * @return array of IDs of redeemable products in the cart
     */
    private function getRedeemableProducts(Coupon $coupon, Cart $cart)
    {
        $priceArray = array();
        $isCartLevelCoupon = $coupon->isCartLevelDiscount();

        // Iterate over the products in the cart to find applicable products.
        foreach ($cart->getProducts() as $id=>$product) {
            if ($isCartLevelCoupon || $this->isCouponApplicableOnProduct($coupon, $product)) {
                $priceArray[$id] = $product->getProductPrice()
                    + $product->getUnitAdditionalCharges() - $product->getDiscount();
            }
        }

        // Sort the unit price array in descending unit prices order.
        arsort($priceArray);
        return array_keys($priceArray);
    }

    /**
     * @return boolean Whether given coupon is applicable for the given product.
     *
     * NOTE: This function should not be called for Cart-Level coupons.
     * Cart-Level coupons are those where all fields like PT-ID, PS-ID, Cat-ID
     * are empty.
     */
    private function isCouponApplicableOnProduct(Coupon $coupon,
                                                 Product $product)
    {
        // Return value.
        $return = FALSE;

              
        // Product properties
        $productId = $product->getProductId();
        $productStyleId = $product->getProductStyleId();

        $productTypeId = $product->getProductTypeId();
        $productCategories =
            $this->adapter->getCategoriesForProduct($productId);

        // Coupon properties
        $couponPTs = trim_all(explode(",", $coupon->getPTIds()));
        $couponPSs = trim_all(explode(",", $coupon->getPSIds()));

        $couponCategories = trim_all(explode(",", $coupon->getCategories()));
        $couponSKUs = trim_all(explode(",", $coupon->getSKUs()));
        $couponExclPTs = trim_all(explode(",", $coupon->getExclPTIds()));
        $couponExclPSs = trim_all(explode(",", $coupon->getExclPSIds()));
        $couponExclCategories =
            trim_all(explode(",", $coupon->getExclCategories()));
        $couponExclSKUs = trim_all(explode(",", $coupon->getExclSKUs()));

        // If the product's PT/PS/SKU matches with a specified value in coupon.
        if (in_array($productTypeId, $couponPTs) ||
            in_array($productStyleId, $couponPSs) ||
            in_array($productId, $couponSKUs))
        {
            $return = TRUE;
        }

        //
        // Check if any of the product's category appears in any of the
        // specified categories.
        //
        foreach ($productCategories as $category) {
            if (in_array($category, $couponCategories)) {
                $return = TRUE;
            }
        }

        //
        // If the product's PT/PS/SKU matches with a specified value to be
        // excluded in coupon.
        //
        if (in_array($productTypeId, $couponExclPTs) ||
            in_array($productStyleId, $couponExclPSs) ||
            in_array($productId, $couponExclSKUs))
        {
            $return = FALSE;
        }

        //
        // Check if any of the product's category appears in any of the
        // specified excluded categories.
        //
        foreach ($productCategories as $category) {
            if (in_array($category, $couponExclCategories)) {
                $return = FALSE;
            }
        }

        /*
	    // Cashback applicable on discounted products check
        include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";
        $enable_cashback_discounted_products = FeatureGateKeyValuePairs::getFeatureGateValueForKey('enableCBOnDiscountedProducts');
      	if($enable_cashback_discounted_products == "false")	{
       		//check if there is any discount due to discount engine.
       		if($product->getDiscount() > 0)	{
      			$return = FALSE;
       		}
       	}
        */
        return $return;
    }

    /**
     * For absolute discount, simply return the amount of coupon discount.
     * For percent discount, calculate the percentage on given price.
     *
     * @param Coupon $coupon
     * @param double $price The price of cart, the product or the item.
     *
     * @throws CouponCorruptedException
     */
    private function calculateCouponDiscountOnPrice(Coupon $coupon, $price)
    {
        $type = $coupon->getType();

        // If the coupon gives dual discount.
        if ($type == 'dual') {
            $abs = $coupon->getAbsoluteDiscount();
            $discount = $price * $coupon->getPercentDiscount() / 100;
            $discount = ($discount > $abs) ? $abs : $discount;
        }

        // If the coupon gives absolute discount.
        elseif ($type == 'absolute') {
            $discount = $coupon->getAbsoluteDiscount();
        }

        // If the coupon gives percentage discount.
        elseif ($type == 'percentage') {
            $discount = $price * $coupon->getPercentDiscount() / 100;
        }

        // Abnormal coupon.
        else {
            $couponCode = $coupon->getCode();
            throw new CouponCorruptedException(
                $couponCode,
                "Unrecognized coupon type for coupon $couponCode.");
        }

        return ($discount > $price) ? $price : $discount;
    }
}

