<?php

require_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
include_once("$xcart_dir/Cache/CacheKeySet.php");


class SpecialCouponGenerator {
	
	private static $generationSlabs = null;
	
	public static function generateSpecialCouponsForOrder($orderId){
		
		$existing_Coupons = func_query_first("SELECT * from myntra_fest_order_coupon_map WHERE 	order_id = $orderId");
		
		if(!empty($existing_Coupons)) { return;}		
		
		
		$query = "SELECT sum(subtotal) as subtotal, sum(discount) as discount, sum(cart_discount) as cart_discount, sum(coupon_discount) as coupon_discount, sum(cash_redeemed) as cash_redeemed, sum(pg_discount) as pg_discount, sum(payment_surcharge) as payment_surcharge, sum(total) as total, sum(shipping_cost) as shipping_cost, sum(gift_charges) as gift_charges, sum(cod) as cod, sum(tax) as tax, sum(cashback) as cashback,login FROM xcart_orders WHERE group_id = $orderId group by group_id";
		
		$order_details = func_query_first($query);
		
		if(empty($order_details)){
           global $errorlog;
           $errorlog->error("MFEST ERROR :- got empty order details, for the order no : $orderId");
		}
		
		
		$amount = $order_details["total"]+ $order_details["cash_redeemed"];

		$isSetLevel = WidgetKeyValuePairs::getBoolean("specialCouponGenerationSetLevel",false);
		if($isSetLevel){
			$amount = SpecialCouponGenerator::getApplicableAmount($orderId,$amount);
		}
		
		$orderMaxDiscountRatio = (float) WidgetKeyValuePairs::getWidgetValueForKey('myntraFestCouponMaxDiscountPercent');
				
		
		$minAmount = WidgetKeyValuePairs::getWidgetValueForKey('myntraFestCouponPercentDiscountMinOrderValue');
		
		if( !FeatureGateKeyValuePairs::getBoolean('myntraFestSLabCoupons') && $amount < $minAmount){
			return;
		}	

		$amount = $orderMaxDiscountRatio * $amount;
		
		$couponCodes = SpecialCouponGenerator::generateSpecialCoupons($amount, $order_details["login"], $orderId );
		
		SpecialCouponGenerator::insertIntoCouponOrderMapping($couponCodes, $orderId);
		
	}
	
	private static function getApplicableAmount($orderid,$amount){
		if($amount <=0){
			return $amount;
		}
		$query = " select od.cash_redeemed as cash_redeemed,
		od.actual_product_price as actual_product_price,od.amount as amount ,od.discount as discount
		,od.cart_discount_split_on_ratio as cart_discount ,od.coupon_discount_product as coupon_discount,
od.pg_discount as pg_discount,od.cashback, od.product_style as style,
sp.global_attr_brand as brand ,sp.global_attr_gender as gender,sp.global_attr_article_type as articleType,sp.global_attr_age_group as ageGroup from xcart_order_details as od, xcart_orders as o,mk_style_properties as sp
where od.orderid = o.orderid and od.product_style=sp.style_id and  o.group_id = $orderid";
		
		$product_details = func_query($query);
		
		$couponsConfig = json_decode(WidgetKeyValuePairs::getWidgetValueForKey("specialCouponGenerationConfig"),true);
		$inclusionStylesList = explode(",",WidgetKeyValuePairs::getWidgetValueForKey("specialCouponGenerationInclusionStyles"));
			
		$inclusionStylesSet = array();
		
		foreach ($inclusionStylesList as $id=>$style){
			$style = trim($style);
            if($style!== "")$inclusionStylesSet[$style]=$id;
        }
		
		
		$exclusionBrands = $couponsConfig["exclusion"]["brands"];
		$exclusionGender = $couponsConfig["exclusion"]["ageGroup"];
		$exclusionLabels = $couponsConfig["exclusion"]["labels"];
			
		$inclusionBrands = $couponsConfig["inclusion"]["brands"];
		$inclusionGender = $couponsConfig["inclusion"]["ageGroup"];
		$inclusionLabels = $couponsConfig["inclusion"]["labels"];
		
		
		foreach ($product_details as $id => $product_detail){
			$product_total_amount = ($product_detail["actual_product_price"]*$product_detail["amount"]) - ($product_detail["discount"]+$product_detail["coupon_discount"]+$product_detail["cart_discount"]+$product_detail["pg_discount"]);
			if(sizeof($exclusionBrands)>0){
				if(in_array(strtolower($product_detail["brand"]),$exclusionBrands)){
						$amount = $amount - $product_total_amount;	
						continue;
				}
				
			}
			
			
			if(sizeof($exclusionGender)>0){
				if(in_array(strtolower($product_detail["ageGroup"]),$exclusionGender)){
					$amount = $amount - $product_total_amount;
					continue;
				}
				
			}
			

			if(sizeof($exclusionLabels)>0){
				if(in_array(strtolower($product_detail["articleType"]),$exclusionLabels)){
					$amount = $amount - $product_total_amount;
					continue;
				}
				
			}
			
			
			if(sizeof($inclusionBrands)>0){
				if(!in_array(strtolower($product_detail["brand"]),$inclusionBrands)){
					$amount = $amount - $product_total_amount;
					continue;
				}
				
			}
				
				
			if(sizeof($inclusionGender)>0){
				if(!in_array(strtolower($product_detail["ageGroup"]),$inclusionGender)){
					$amount = $amount - $product_total_amount;
					continue;
				}
				
			}
				
			
			if(sizeof($inclusionLabels)>0){
				if(!in_array(strtolower($product_detail["articleType"]),$inclusionLabels)){
					$amount = $amount - $product_total_amount;
					continue;
				}
				
			}
			
			if(sizeof($inclusionStylesSet)){
				if(!array_key_exists($product_detail["style"],$inclusionStylesSet)){
					$amount = $amount - $product_total_amount;
					continue;
				}
				
			}
			
		}
		
		return $amount;

	} 
	
	
	public static function setGenerationSlabs(){
		
		//$cache = new \Cache("myntraFestCouponSlabs");
		
		
		/*
		$query = "SELECT * from myntra_festival_slabs where 1";
		$slabDetails = func_query($query);
		
		$genSlabs= array();
		
		foreach ($slabDetails as $slab){
			$genSlabs[intval($slab["lowerBound"])] = array(
														"lowerBound" => intval($slab["lowerBound"]),
														"noOfCoupons" => intval($slab["noOfCoupons"]),
														"CouponValue" => intval($slab["CouponValue"]),
														"CouponMinPurchaseValue" => intval($slab["CouponMinPurchaseValue"])
														); 
		}
		
		ksort($genSlabs);
		*/
		SpecialCouponGenerator::getSlabs();
	}
	
	public static function getSlabs() {
		$cache = new \Cache(SpecialCouponCacheKeys::$prefix, SpecialCouponCacheKeys::keySet());
		SpecialCouponGenerator::$generationSlabs = $cache->get(function(){
			
			$query = "SELECT id, lowerBound, noOfCoupons, CouponValue, CouponMinPurchaseValue from myntra_festival_slabs where isActive=1";
			$slabDetails = func_query($query);
			$genSlabs= array();
			foreach ($slabDetails as $slab){
				$genSlabs[intval($slab["lowerBound"])] = array(
						"id" => intval($slab["id"]),
						"lowerBound" => intval($slab["lowerBound"]),
						"noOfCoupons" => intval($slab["noOfCoupons"]),
						"CouponValue" => intval($slab["CouponValue"]),
						"CouponMinPurchaseValue" => intval($slab["CouponMinPurchaseValue"])
				);
			}
			
			ksort($genSlabs);
			return $genSlabs;
		},array(), SpecialCouponCacheKeys::$myntraFestSlabDetails);	

		return SpecialCouponGenerator::$generationSlabs;
	}

	public static function deleteSlab($slabId){
		db_query("update myntra_festival_slabs set isActive=0, updatedOn=now()  where id='" . $slabId . "'");
		$cache = new \Cache(SpecialCouponCacheKeys::$prefix, SpecialCouponCacheKeys::keySet());
		$cache->invalidate();
		
	}

	public static function setSlabs($slab,$mode) {
		if(!empty($slab) && ($mode==1 || $mode==2)){
			//validation : if an active entry already exists for the same lowerBound then return an error
			SpecialCouponGenerator::getSlabs();
			if(array_key_exists($slab["lowerBound"], SpecialCouponGenerator::$generationSlabs) && $mode==1)
				return -1;
			if($slab["lowerBound"]==0 || $slab["noOfCoupons"]==0 || $slab["CouponValue"]==0 || $slab["CouponMinPurchaseValue"]==0)
				return -2;
			if($mode==2){//update
				if($slab["id"]==0) return -3;
				SpecialCouponGenerator::deleteSlab($slab["id"]);
			}
			unset($slab["id"]);
			
			func_array2insert("myntra_festival_slabs", $slab);
			$cache = new \Cache(SpecialCouponCacheKeys::$prefix, SpecialCouponCacheKeys::keySet());
			$cache->invalidate();
		}	
	}
	
	private static function generateSpecialCoupons($amount ,$login ,$orderId ){
		
		$isSlabCoupons = FeatureGateKeyValuePairs::getBoolean('myntraFestSLabCoupons');
		
		
		$adapter = CouponAdapter::getInstance();
		$couponCodes = array();
		
		$description = "coupons generated for myntra festival. orderID : $orderId";
			
			
					
			
		$couponsPrefix = WidgetKeyValuePairs::getWidgetValueForKey('myntraFestCouponPrefix');
		$couponsGroupName = WidgetKeyValuePairs::getWidgetValueForKey('myntraFestCouponGroupName');
		$couponEndDate = WidgetKeyValuePairs::getWidgetValueForKey('myntraFestCouponEndDate');
		
		$startDate = strtotime(date('d F Y', time()));
		$endDate = strtotime($couponEndDate);
		
		
		if($isSlabCoupons){
			
			SpecialCouponGenerator::setGenerationSlabs();
			
			$slabParams = SpecialCouponGenerator::getCouponGenerationParams($amount);
			
			if($slabParams == null){
				//echo "\n no slab found, amount : $amount \n";
				return;
			}
			
			
			$baseCouponAmount = $slabParams["CouponValue"];	
			$baseCouponMinAmount = $slabParams["CouponMinPurchaseValue"];
			$noOfBaseCoupons = $slabParams["noOfCoupons"];
						
			
			
			
			for($i = 0 ; $i < $noOfBaseCoupons ; $i++){			
				
	            $couponCode = $adapter->generateSingleCouponForUser($couponsPrefix, "8", $couponsGroupName, $startDate, $endDate, $login, 'absolute', $baseCouponAmount, '', $baseCouponMinAmount, $description);
	            $couponCodes[] = $couponCode;
			}
		}else{
			
			$percent = WidgetKeyValuePairs::getWidgetValueForKey('myntraFestCouponPercentDiscount');

						
			$couponCodes[] = $adapter->generateSingleCouponForUser($couponsPrefix, "8", $couponsGroupName, $startDate, $endDate, $login, 'dual', $amount , $percent, 0, $description);
			
		}	
		return $couponCodes;
				
	}
	
	private static function getCouponGenerationParams($amount){

		$noOfSlabs = sizeof(SpecialCouponGenerator::$generationSlabs);
		$prevBound = 0;
		$prevParams = null;
		foreach (SpecialCouponGenerator::$generationSlabs as $bound=>$params){
			if($amount >= $prevBound && $amount < $bound ){
				break;
			}else{
				$prevBound = $bound ;
				$prevParams = $params;
			}
		}
		
		return $prevParams;		
	}
	
	private static function insertIntoCouponOrderMapping($couponCodes,$orderId){
		//print_r($couponCodes);
		foreach($couponCodes as $id=>$couponCode){
			db_query("insert into myntra_fest_order_coupon_map (order_id , coupon_code)  values ( $orderId , '$couponCode' )")	;		
		}
	}
	
	public static function getAllSpecialCouponForOrder($orderId){
		$coupons=array();
		//JOIN with table xcart_discount_coupons
		$result=db_query("select fcm.coupon_code, xcd.* from myntra_fest_order_coupon_map fcm,xcart_discount_coupons xcd where fcm.order_id=".$orderId." and fcm.coupon_code=xcd.coupon");
		while ($row = db_fetch_array($result)) {
			array_push($coupons, $row);
		}
		return $coupons;
	
	}	


	public static function shareCoupon($from, $to , $couponCode){
		
		$couponsGroupName = WidgetKeyValuePairs::getWidgetValueForKey('myntraFestCouponGroupName');
		
		$query = "UPDATE xcart_discount_coupons SET users='$to' WHERE users='$from' and coupon='$couponCode' and groupName = '$couponsGroupName' ";
		db_query($query);
		
		if( db_affected_rows() == 1){
			
			//send email to transforee ----------------------
			$from_name = func_query_first("SELECT firstname, lastname from xcart_customers where login = '$from' ");
			$coupon_details = func_query_first("SELECT * from xcart_discount_coupons where coupon = '$couponCode' ");
			
			
			if($coupon_details["couponType"]=="dual"){
				$template = "myntra_fest_coupon_share_template_dual";
			
				$args = array( 	"FROM"	=> $from_name["firstname"]." ".$from_name["lastname"],
						"COUPON_CODE"	=> $couponCode,
						"COUPON_AMOUNT" => $coupon_details["MRPAmount"],
						"COUPON_PERCENT" => $coupon_details["MRPpercentage"],
						"COUPON_EXP" => date( "d F, Y", $coupon_details["expire"])
				);
			}else{			
			$template = "myntra_fest_coupon_share_template";
				
			$args = array( 	"FROM"	=> $from_name["firstname"]." ".$from_name["lastname"],
					"COUPON_CODE"	=> $couponCode,
					"COUPON_AMOUNT" => $coupon_details["MRPAmount"],
					"COUPON_MIN" => $coupon_details["minimum"],
					"COUPON_EXP" => date( "d F, Y", $coupon_details["expire"])					
				);
			}
		 
			$mail_details = array(
					"template" => $template,
					"to" => $to,
					"bcc" => "myntramailarchive@gmail.com",
					"header" => 'header',
					"footer" => 'footer',
					"mail_type" => MailType::CRITICAL_TXN
			);
			$multiPartymailer = new MultiProviderMailer($mail_details, $args);
			$multiPartymailer->sendMail();
				
			//----------------------------------------------
			
			
			db_query( "INSERT into myntra_fest_coupon_share_log (`from` , `to` , `couponCode` ) values ( '$from' , '$to' , '$couponCode' )");
			return json_encode(array("status"=>"success","message"=>"Coupon ".$couponCode." has been transferred to ".$to));
		}else{
			return json_encode(array("status"=>"failed","message"=>"error in coupon transfer"));
		}
	}

}
