<?php

/**
 * This exception is thrown when a cashback is redeemed as coupon
 * 
 * @author Aditi.Goswami
 */
class CouponGroupNameNotValidException extends CouponException
{
	/**
	 * Constructor.
	 * overrides Exception.__construct
	 * 
	 * @param String $couponCode Code for the coupon that was not found.
	 */
	public function __construct($couponCode)
	{
		parent::__construct("Cashback cannot be redeemed as coupon.", 1, $couponCode);
	}
}
