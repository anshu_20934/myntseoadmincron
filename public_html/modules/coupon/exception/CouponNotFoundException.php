<?php

include ("$xcart_dir/modules/coupon/exception/CouponException.php");

/**
 * This exception is thrown when there is no coupon corresponding to a 
 * specified coupon code.
 * 
 * @author Rohan.Railkar
 */
class CouponNotFoundException extends CouponException
{
	/**
	 * Constructor.
	 * overrides Exception.__construct
	 * 
	 * @param String $couponCode Code for the coupon that was not found.
	 */
	public function __construct($couponCode)
	{
		parent::__construct("Sorry, this coupon is not valid.", 1, $couponCode);
	}
}
