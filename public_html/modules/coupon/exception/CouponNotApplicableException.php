<?php

/**
 * This exception is thrown when the coupon is not redeemable on any of the 
 * products in the cart.
 * 
 * @author Rohan.Railkar
 */
class CouponNotApplicableException extends CouponException
{
	/**
	 * Constructor.
	 * overrides Exception.__construct
	 * 
	 * @param String $couponCode
	 */
	public function __construct($couponCode,$isSpecial=false)
	{
		if($isSpecial){
			parent::__construct(
			"Sorry, Your cart has no applicable products for this coupon - $couponCode",
			1,
			$couponCode);
		}else{
			parent::__construct(
				"Sorry, this coupon is not valid.",
				1,
				$couponCode);
		}
	}
}
