<?php

/**
 * This exception is thrown when the coupon is not allowed for the current channel.
 * 
 * @author Rohan.Railkar
 */
class CouponChannelException extends CouponException
{
	/**
	 * Constructor.
	 * overrides Exception.__construct
	 * 
	 * @param String $couponCode Code for the coupon that was not found.
	 */
	public function __construct($couponCode)
	{
		parent::__construct(
			"Sorry, this coupon can only be used from a mobile device.",
			1,
			$couponCode);
	}
}
