<?php

/**
 * The exception to be thrown when the end date of the coupon is past.
 * 
 * @author Rohan.Railkar
 */
class CouponExpiredException extends CouponException
{
	private $endDate;
	
	/**
	 * Constructor.
	 * 
	 * @param String $couponCode
	 * @param int $endDate
	 */
	public function __construct($couponCode, $endDate)
	{
		parent::__construct(
			"Sorry, this Coupon has expired.",
			1,
			$couponCode);
		
		$this->endDate = $endDate;
	}
	
	/**
	 * Getter for $endDate.
	 */
	public function getEndDate()
	{
		return $this->endDate;
	}
}