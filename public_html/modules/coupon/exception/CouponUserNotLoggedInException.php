<?php

/**
 * This exception is thrown when the user is not logged in while redeeming the
 * coupon, for the user specific coupons.
 * 
 * @author Rohan.Railkar
 */
class CouponUserNotLoggedInException extends CouponException
{
	/**
	 * Coupon Code.
	 */
	private $couponCode;
	
	/**
	 * Constructor.
	 * overrides Exception.__construct
	 * 
	 * @param String $couponCode
	 */
	public function __construct($couponCode)
	{
		parent::__construct(
			"Sorry, this coupon is not valid for this user account.",
			1,
			$couponCode);
	}
}