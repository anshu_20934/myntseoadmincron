<?php

/**
 * The exception to be thrown when the maxUsage is completed.
 * 
 * @author Rohan.Railkar
 */
class CouponUsageExpiredException extends CouponException
{
	public function __construct($couponCode)
	{
		parent::__construct(
			"Sorry, you have crossed the usage limit of this coupon.",
			1,
			$couponCode);
	}
}