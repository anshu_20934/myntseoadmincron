<?php

require_once ("$xcart_dir/exception/MyntraException.php");

/**
 * A base class for all exceptions concerned with coupons.
 * 
 * @author Rohan.Railkar
 */
class CouponException extends MyntraException
{
	/**
	 * The coupon for which the exception was thrown.
	 * Note: The coupon might be non-existent.
	 */
	private $couponCode;
    
	/**
	 * Constructor.
	 */
	public function __construct($message, $code, $couponCode)
	{
	    $this->couponCode = $couponCode;
		parent::__construct($message, $code);
	}
	
	/**
	 * Getter for coupon code.
	 */
	public function getCouponCode()
	{
		return $this->couponCode;
	}
}
