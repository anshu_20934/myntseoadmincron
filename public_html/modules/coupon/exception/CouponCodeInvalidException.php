<?php

require_once ("$xcart_dir/exception/MyntraException.php");

/**
 * A base class for all exceptions concerned with coupons.
 * 
 * @author Kundan.Burnwal
 */
class CouponCodeInvalidException extends CouponException
{
	/**
	 * Constructor.
	 */
	public function __construct($message, $code, $couponCode)
	{
		parent::__construct($message, $code, $couponCode);
	}
}
