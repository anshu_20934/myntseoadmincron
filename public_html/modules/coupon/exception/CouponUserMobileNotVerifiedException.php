<?php

/**
 * This exception is thrown when the mobile verification for the user is not
 * done for the coupon's user.
 * 
 * @author Rohan.Railkar
 */
class CouponUserMobileNotVerifiedException extends CouponException
{
	/**
	 * The user.
	 */
	private $userName;
	
	/**
	 * The unverified mobile number.
	 */
	private $mobile;
	
	/**
	 * Constructor.
	 * overrides Exception.__construct
	 */
	public function __construct($couponCode, $userName, $mobile)
	{
	    $msg = "Your mobile number $mobile has to be verified to be able to redeem this coupon.";
	    
	    if (empty($mobile)) {
	        $msg = "You must specify a mobile number to be able to redeem this coupon.";
	    }
	    
		parent::__construct($msg, 1, $couponCode);
	}
}
