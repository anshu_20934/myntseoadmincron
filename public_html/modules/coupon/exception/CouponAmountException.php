<?php

/**
 * The exception to be thrown when the start date of coupon is yet to arrive.
 * 
 * @author Rohan.Railkar
 */

include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";

class CouponAmountException extends CouponException
{
	private $minimum;
	private $maximum;
	
	/**
	 * Constructor.
	 * 
	 * @param String $couponCode
	 * @param double $minimum
	 * @param double $maximum
	 */
	public function __construct($couponCode, $minimum, $maximum,$applicableproducts)
	{
	    $range;
	    if ($minimum != 0.0 && $maximum != 0.0) {
	        $range = "between Rs. $minimum & Rs. $maximum";
	    }
	    else if ($minimum == 0.0 && $maximum != 0.0) {
	        $range = "less than Rs. $maximum";
	    }
	    else if ($minimum != 0.0 && $maximum == 0.0) {
	        $range = "more than Rs. $minimum";
	    }
	    
	    // Feature gate condition to enable Non-discount-products coupon
	    $condNoDiscountCoupon = FeatureGateKeyValuePairs::getBoolean('condNoDiscountCoupon');
	    $styleExclusionFG = FeatureGateKeyValuePairs::getBoolean('styleExclusion');
	    
	    //multiple messaging must be pushed to templates
	    if ($condNoDiscountCoupon || $styleExclusionFG){
	    	$extraLeft='<span style="color : #808080;">';
	    	$extraRight='</span>';
	    	$errorMessage="Coupons are only applicable on certain products and when the value of such products is above a minimum value.";
	    }
	    else{
	    	$conditionalStatment="";
	    	$extraLeft="";
	    	$extraRight="";
	    	$errorMessage="This coupon is only valid on a purchase $range. $extraRight $conditionalCountStatment";
	    }
	    
		parent::__construct(
			"$extraLeft $errorMessage",
				1,
			$couponCode);
		
		$this->minimum = $minimum;
		$this->maximum = $maximum;
	}
	
	/**
	 * Getter for $minimum.
	 */
	public function getMinimum()
	{
		return $this->minimum;
	}
	
/**
	 * Getter for $maximum.
	 */
	public function getMaximum()
	{
		return $this->maximum;
	}
}