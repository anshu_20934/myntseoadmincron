<?php

/**
 * The exception to be thrown when the start date of coupon is yet to arrive.
 * 
 * @author Raghavendra kolli
 */

include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";

class CouponCountException extends CouponException
{
	private $minimum;
	private $maximum;
	
	/**
	 * Constructor.
	 * 
	 * @param String $couponCode
	 * @param double $minimum
	 * @param double $maximum
	 */
	public function __construct($couponCode, $minimum, $maximum,$applicableproducts)
	{
	    $minimum = (int) $minimum;
	    $maximum = (int) $maximum;
	    if ( $minimum != 0 && $maximum != 0) {
	        $range = "between no. $minimum & no. $maximum";
	        
	        $errorMessage="This coupon is only valid on a purchase of $minimum to $maximum number of items";
	         
	    }else if ($minimum == 0 && $maximum != 0) {
	        $range = "less than no. $maximum";
	        $errorMessage="This coupon is only valid on a purchase of less than $maximum number of items";
	    }
	    else if ($minimum != 0 && $maximum == 0) {
	        $errorMessage="This coupon is only valid on a purchase of more than $minimum number of items";
	    }
	    
	    	$conditionalStatment="";
	    	$extraLeft="";
	    	$extraRight="";
	    
	  
	    
		parent::__construct(
			"$extraLeft $errorMessage",
				1,
			$couponCode);
		
		$this->minimum = $minimum;
		$this->maximum = $maximum;
	}
	
	/**
	 * Getter for $minimum.
	 */
	public function getMinimum()
	{
		return $this->minimum;
	}
	
/**
	 * Getter for $maximum.
	 */
	public function getMaximum()
	{
		return $this->maximum;
	}
}