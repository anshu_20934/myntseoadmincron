<?php

/**
 * This exception is thrown when the user is not allowed to use the coupon.
 * 
 * @author Rohan.Railkar
 */
class CouponNotValidForUserException extends CouponException
{
	/**
	 * The user.
	 */
	private $userName;
	
	/**
	 * Constructor.
	 * overrides Exception.__construct
	 * 
	 * @param String $couponCode Code for the coupon that was not found.
	 */
	public function __construct($couponCode, $userName)
	{
		parent::__construct(
			"Sorry, this coupon is not valid for this user account.",
			1,
			$couponCode);
	}
}