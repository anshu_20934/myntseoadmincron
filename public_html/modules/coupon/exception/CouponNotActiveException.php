<?php

/**
 * This exception is thrown when the concernted coupon is not in active status.
 * 
 * @author Rohan.Railkar
 */
class CouponNotActiveException extends CouponException
{
	/**
	 * Constructor.
	 * overrides Exception.__construct
	 * 
	 * @param String $couponCode Code for the coupon that was not active.
	 */
	public function __construct($couponCode)
	{
		parent::__construct("Sorry, this Coupon has expired.",
		                    1,
		                    $couponCode);
	}
}