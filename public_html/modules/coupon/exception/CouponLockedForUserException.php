<?php

/**
 * This exception is thrown when the coupon is already locked for user under
 * a pending payment.
 * 
 * @author Rohan.Railkar
 */
class CouponLockedForUserException extends CouponLockedException
{
	/**
	 * The user.
	 */
	private $userName;
	
	/**
	 * Constructor.
	 * overrides Exception.__construct
	 * 
	 * @param String $couponCode Code for the coupon that was not found.
	 */
	public function __construct($couponCode, $userName)
	{
		parent::__construct(
			"Sorry, this coupon is temporarily locked and will be unlocked in 15 minutes. Try again after some time.",
			1,
			$couponCode);
		
		$this->userName=$userName;
	}
	
	/**
	 * returns user name for exception
	 */
	public function getUserName(){
		return $this->userName;
	}
}
