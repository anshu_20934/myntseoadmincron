<?php

/**
 * The exception to be thrown when the start date of coupon is yet to arrive.
 * 
 * @author Rohan.Railkar
 */
class CouponNotStartedException extends CouponException
{
	private $startDate;
	private $endDate;
	
	/**
	 * Constructor.
	 * 
	 * @param String $couponCode
	 * @param int $startDate
	 * @param int $endDate
	 */
	public function __construct($couponCode, $startDate, $endDate)
	{
		parent::__construct(
			"Sorry, this Coupon has expired.",
			1,
			$couponCode);
		
		$this->startDate = $startDate;
		$this->endDate = $endDate;
	}
	
	/**
	 * Getter for $startDate.
	 */
	public function getStartDate()
	{
		return $this->startDate;
	}
	
/**
	 * Getter for $endDate.
	 */
	public function getEndDate()
	{
		return $this->endDate;
	}
}