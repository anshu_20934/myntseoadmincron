<?php

/**
 * This exception is thrown when the coupon is locked due to a pending payment.
 * 
 * @author Rohan.Railkar
 */
class CouponLockedException extends CouponException
{
	/**
	 * Constructor.
	 * overrides Exception.__construct
	 * 
	 * @param String $couponCode Code for the coupon that was not found.
	 */
	public function __construct($couponCode)
	{
		parent::__construct(
			"Sorry, this coupon is temporarily locked and will be unlocked in 15 minutes. Try again after some time.",
			1,
			$couponCode);
	}
}
