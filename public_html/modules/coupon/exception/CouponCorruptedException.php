<?php

/**
 * This exception is thrown when something is wrong in the coupon data.
 * 
 * @author Rohan.Railkar
 */
class CouponCorruptedException extends CouponException
{
	/**
	 * Constructor.
	 * overrides Exception.__construct
	 * 
	 * @param String $couponCode
	 */
	public function __construct($couponCode, $message)
	{
		parent::__construct($message, 1, $couponCode);
	}
}