<?php

/**
 * This exception is thrown when the user has reached the max usage limit for 
 * each user on the coupon.
 * 
 * @author Rohan.Railkar
 */
class CouponUsageExpiredForUserException extends CouponException
{
	/**
	 * The user.
	 */
	private $userName;
	
	/**
	 * Constructor.
	 * overrides Exception.__construct
	 * 
	 * @param String $couponCode Code for the coupon that was not found.
	 */
	public function __construct($couponCode, $userName)
	{
		parent::__construct(
			"Sorry, you have crossed the usage limit of this coupon.",
			1,
			$couponCode);
	}
}
