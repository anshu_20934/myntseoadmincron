<?php
global $mrpDefaultCouponConfiguration;
$mrpDefaultCouponConfiguration = array(

'mrp_firstLogin_nonfbreg_numCoupons' => 2,
'mrp_firstLogin_nonfbreg_mrpAmount' => 250,
'mrp_firstLogin_nonfbreg_minCartValue' => 1000,
'mrp_firstLogin_nonfbreg_validity' => 30,

'mrp_firstLogin_fbreg_numCoupons'=>3,
'mrp_firstLogin_fbreg_mrpAmount'=>250,
'mrp_firstLogin_fbreg_minCartValue'=>1000,
'mrp_firstLogin_fbreg_validity' => 30,

'mrp_refFirstPurchase_numCoupons' => 2,
'mrp_refFirstPurchase_mrpAmount' => 500,
'mrp_refFirstPurchase_minCartValue' => 2000,
'mrp_refFirstPurchase_validity' => 90,

'mrp_refRegistration_numCoupons' => 1,
'mrp_refRegistration_mrpAmount' => 250,
'mrp_refRegistration_minCartValue'=> 1000,
'mrp_refRegistration_validity' => 30,

'cust_survey_mrpAmount' => 500,
'cust_survey_minCartValue' => 1500,
'cust_survey_validity' => 7,

); 

/*


);*/

?>
