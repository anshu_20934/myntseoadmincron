<?php

include_once "$xcart_dir/include/func/func.db.php";
include_once "$xcart_dir/modules/coupon/mrp/Rule.php";
include_once "$xcart_dir/modules/coupon/mrp/exception/MrpRuleNotFoundException.php";


/**
 * Database adapter for accessing MRP related database tables.
 * 
 * @author Rohan.Railkar
 */
class MRPRulesAdapter
{
    /**
     * The singleton instance.
     */
    private static $instance = NULL;
    
    /**
     * Database constants.
     */
    private $tbl_rules = "mk_mrp_rules";
    private $tbl_mrp_log = "mk_mrp_log";

    /**
     * @return MRPRulesAdapter The singleton instance.
     */
    public static function getInstance()
    {
        if (MRPRulesAdapter::$instance == NULL) {
            MRPRulesAdapter::$instance = new MRPRulesAdapter();
        }

        return MRPRulesAdapter::$instance;
    }

    /**
     * Constructor.
     */
    private function __construct()
    {
        // Empty.
    }
    
    /**
     * @return Rule
     */
    public function fetchRule($ruleId)
    {
        $query = "SELECT *
        			FROM " . $this->tbl_rules . "
        		   WHERE isActive = 1
        		     AND ruleid = '$ruleId';";
        
        $result = db_query($query,true);
        $row = db_fetch_array($result);
        
        if (empty($row)) {
            throw new MrpRuleNotFoundException($ruleId);
        }
        else {
            return new Rule($row);
        }
    }
    
    /**
     * @return Array of active rules.
     */
    public function getAllAsynchronousActiveRootRules()
    {
        $query = "SELECT * 
        			FROM " . $this->tbl_rules . " 
        		   WHERE isRootLevel = 1 
        		     AND isSynchronous = 0
        		     AND isActive = 1;";
        
        $result = db_query($query,true);
        $rules = array();
        while ($row = db_fetch_array($result)) {
            $rule = new Rule($row);
            array_push($rules, $rule);
        }
        
        return $rules;
    }
    
    /**
     * Logging function.
     */
    public function log($ruleid, 
                        $rulename, 
                        $timestamp, 
                        $appliedOn, 
                        $success, 
                        $coupon, 
                        $userName)
    {
        $query = "INSERT INTO " . $this->tbl_mrp_log . " 
        			(ruleid, rulename, timestamp, appliedOn, result, coupon, couponUser)
        		  VALUES
        		    ('$ruleid', '$rulename', '$timestamp', '$appliedOn', '$success', '$coupon', '$userName');";
        
        db_query($query);
    }
    
    /**
     * Function to scan the mrp logs to fetch past success of a rule over an order / customer.
     */
    public function wasSuccessfullyApplied($ruleId, $entity)
    {
        $query = 
            "SELECT * 
               FROM " . $this->tbl_mrp_log . " 
              WHERE ruleid = '$ruleId' 
                AND appliedOn = '$entity';";
        
        $result = db_query($query,true);
        
        while ($row = db_fetch_array($result)) {
            if ($row['result'] == 1) {
                return true;
            }
        }
        
        return false;
    }
}
