
<?php

include_once "$xcart_dir/modules/coupon/mrp/Customer.php";
include_once "$xcart_dir/modules/coupon/mrp/Order.php";
include_once "$xcart_dir/modules/coupon/mrp/exception/MrpOrderNotFoundException.php";
include_once "$xcart_dir/modules/coupon/mrp/exception/MrpCustomerNotFoundException.php";

/**
 * Database adapter to query customers and orders table to fetch data for
 * applying MRP rules.
 * 
 * @author Rohan.Railkar
 */
class MRPDataAdapter
{
    /**
     * The singleton instance.
     */
    private static $instance = NULL;
    
    /**
     * Table names.
     */
    private $tbl_customers = 'xcart_customers';
    private $tbl_orders = 'xcart_orders';
    private $tbl_coupon_mapping = 'mk_mrp_coupons_mapping';
    
    /**
     * Column names.
     */
    
    // Orders table.
    private $ord_orderid = 'orderid';
    private $ord_login = 'login';
    private $ord_date = 'date';
    private $ord_Queuedddate = 'queueddate';
    private $ord_status = 'status';
    
    // Customers table.
    private $cust_login = 'login';
    private $cust_last_login = 'last_login';

    /**
     * @return MRPDataAdapter The singleton instance.
     */
    public static function getInstance()
    {
        if (MRPDataAdapter::$instance == NULL) {
            MRPDataAdapter::$instance = new MRPDataAdapter();
        }

        return MRPDataAdapter::$instance;
    }

    /**
     * Constructor.
     */
    private function __construct()
    {
        // Empty.
    }
    
    /**
     * @return Order
     * @throws MrpOrderNotFoundException
     */
    public function fetchOrder($orderid)
    {
        $query = "SELECT * 
        			FROM " . $this->tbl_orders . " 
        		   WHERE " . $this->ord_orderid . " = $orderid;";
        
        $result = db_query($query, true);
        $row = db_fetch_array($result);
        
        if (empty($row)) {
            throw new MrpOrderNotFoundException($orderid);
        }
        
        return new Order($row);
    }
    
    /**
     * @return Customer
     */
    public function fetchCustomer($login)
    {
        $query = "SELECT * 
        			FROM " . $this->tbl_customers . " 
        		   WHERE " . $this->cust_login . " = '$login';";
        
        $result = db_query($query);
        $row = db_fetch_array($result);
        
        if (empty($row)) {
            throw new MrpCustomerNotFoundException($login);
        }
        
        return new Customer($row);
    }
    
    /**
     * @return string User's login.
     */
    public function getUserForOrder($orderid)
    {
        $query = "SELECT " . $this->ord_login . "
        			FROM " . $this->tbl_orders . "
        		   WHERE " . $this->ord_orderid . " = $orderid;";
        
        $result = db_query($query, true);
        $row = db_fetch_array($result);
        
        if (empty($row)) {
            return null;
        }
        
        return $row[$this->ord_login];
    }
    
    /**
     * @return Array of orderIds.
     */
    public function getOrdersForUser($login)
    {
        $query = "SELECT " . $this->ord_orderid . "  
        			FROM " . $this->tbl_orders . "
        		   WHERE login = '$login';";
        
        $result = db_query($query, true);
        $orderIds = array();
        while ($row = db_fetch_array($result)) {
            array_push($orderIds, $row['orderid']);
        }
        
        return $orderIds;
    }
    
    /**
     * @return An array of Order's placed in the TIME_BUFFER.
     */
    public function fetchQueuedOrdersInTimeBuffer($time_buffer)
    {
    	$query = "SELECT *
					FROM " . $this->tbl_orders . " 
					WHERE " . $this->ord_Queuedddate . " > unix_timestamp(now()) - $time_buffer and ".$this->ord_status." in ('Q','WP','SH','DL','C'); ";
			
        $result = db_query($query, true);
        $orders = array();
        while ($row = db_fetch_array($result)) {
            $order = new Order($row);
            array_push($orders, $order);
        }
        
        return $orders;
    }
    
    /**
     * @return An array of Customer's logged in the TIME_BUFFER.
     */
    public function fetchCustomersInTimeBuffer($time_buffer)
    {
        $query = "SELECT *
        			FROM " . $this->tbl_customers . " 
        		   WHERE " . $this->cust_last_login . " > unix_timestamp(now()) - $time_buffer;";
        
        $result = db_query($query, true);
        $customers = array();
        while ($row = db_fetch_array($result)) {
            $customer = new Customer($row);
            array_push($customers, $customer);
        }
        
        return $customers;
    }
    
    /**
     * @return string Login for the user who referred the given user.
     */
    public function getReferrerForUser($login)
    {
        $query = "SELECT referer 
        			FROM " . $this->tbl_customers . " 
        		   WHERE login = '$login';";
        
        $result = func_query_first($query);
        return $result['referer'];
    }
    
    /**
     * Get Queued orders for a customer. Return array of orderids.
     */
    public function getQueuedOrders($login)
    {
        $query = "SELECT orderid 
        			FROM " . $this->tbl_orders . " 
        		   WHERE login = '$login'
        		     AND status = 'Q';";
        
        $result = db_query($query);
        $orderids = array();
        while ($row = db_fetch_array($result)) {
            array_push($orderids, $row['orderid']);
        }
        
        return $orderids;
    }
    
    /**
     * Get Queued orders for a customer before the given date. Return array of orderids.
     */
    public function getQueuedOrdersBefore($date, $login)
    {
        $query = "SELECT orderid 
					FROM  $this->tbl_orders
					WHERE  $this->ord_login = '$login'
					AND Queueddate < '$date' 
        		    AND status in ('Q','WP','SH','DL','C');";
        $result = db_query($query);
		$orderids = array();
		
        while ($row = db_fetch_array($result)) {
            array_push($orderids, $row['orderid']);
        }
        
		echo count($orderids);
        return $orderids;
    }
    
    /**
     * Add a customer-couponCode mapping for a generated user-tagged coupon.
     */
    public function addCouponMapping($userName, $couponCode)
    {
        $query = "INSERT INTO " . $this->tbl_coupon_mapping . " 
        			(login, coupon)
        		  VALUES
        		    ('$userName', '$couponCode');";
        
        db_query($query);
    }
}
