            <?php

require_once $xcart_dir.'/modules/RestAPI/RestRequest.php';
require_once $xcart_dir.'/env/Host.config.php';

class CouponAPIClient {

   
    
    public static function fetchCoupon($couponCode, $username='') {
        return self::get('/coupon/'.$couponCode);
    }

    public static function createCouponGroup($groupname,
                                      $channel,
                                      $timestamp,
                                      $creator,
                                       $groupType="",$active='A',$count=0,$applicableDevice)
    {
        $request = array();
        $request['groupname'] = $groupname;
        $request['channel']  = $channel;
        $request['timeCreated'] = $timestamp;
        $request['creator'] = $creator;
        $request['groupType'] = $groupType;
        $request['active'] = $active;
        $request['count'] = $count;
        $request['applicableDevice'] = $applicableDevice;

        return self::post('/couponGroup/', $request);

    }

    public static function createCouponGroupBinRange($groupname,$stratBin,$endBin) {
        $request = array();
        $request['groupName'] = $groupname;
        $request['startBin']  = $stratBin;
        $request['endBin'] = $endBin;
        return self::post('/couponBinRange/',$request);

    }


    public static function modifyCouponGroupBinRange($groupname,$binRange) {

        $request = array();
        $request['groupName'] = $groupname;
        $request['binRange']  = $binRange;
        return self::put('/couponBinRange/', $request);

    }

    public static function modifyCouponGroup($groupname,$channel,$status,$editedBy,$time,$groupType,$applicableDevice) {

        $request = array();
        $request['groupname'] = $groupname;
        $request['channel']  = $channel;
        $request['active'] = $status;
        $request['groupType'] = $groupType;
        $request['lastEdited'] = $time;
        $request['lastEditedBy'] = $editedBy;
        $request['applicableDevice'] = $applicableDevice;
        return self::put('/couponGroup/', $request);


    }

    public static function changeCouponGroupStatus($groupname,$status,$login,$time) {
        $request = array();
        $request['groupname'] = $groupname;
        $request['active'] = $status;
        $request['lastEdited'] = $time;
        $request['lastEditedBy'] = $login;
        return self::put('/couponGroup/changeStatus', $request);


    }


    public static function changeCouponStatus($couponCode,$status,$login,$condition) {

        $request = array();
        $request['code'] = $couponCode;
        $request['status'] = $status;
        $request['lastEditedBy'] = $login;
        return self::put('/coupon/', $request);
    }

     public static function addCoupon($couponCodeNew, $groupIdNew, $sdate,
    $edate, $maxUsageNew, $isInfiniteNew, $maxUsagePerCartNew,
    $times_used, $times_lockedNew, $maxUsageByUserNew,
    $isInfinitePerUserNew, $minAmountNew, $maxAmountNew, $usersNew,
    $excludeUsersNew, $timestamp, $creatorNew,
    $couponTypeNew, $MRPAmountNew, $MRPpercentageNew,
    $freeShipping, $isActiveNew, $commentsNew,
    $productTypeIds, $excludeProductTypeIds, $styleIds,
    $excludeStyleIds, $catStyleIds, $excludeCatStyleIds,
    $categoryIds, $excludeCategoryIds, $SKUs, $excludeSKUs, $description, $showInMyMyntra,$minCountNew,$maxCountNew) {

        $request = array();
        $request['code'] = $couponCodeNew;
        $request['startdate'] = $sdate;
        $request['expire'] = $edate;
        $request['times'] = $maxUsageNew;
        $request['maximum'] = 0.0;
        $request['isInfinite'] = $isInfiniteNew;
        $request['maxUsagePerCart'] = $maxUsagePerCartNew;
        $request['times_used']  = $times_used;
        $request['times_locked'] = $times_lockedNew;
        $request['maxUsageByUser'] = $maxUsageByUserNew;
        $request['isInfinitePerUser'] = $isInfinitePerUserNew;
        $request['minimum'] = $minAmountNew;
        $request['maxAmount'] = $maxAmountNew;
        $request['users']  = $usersNew;
        $request['excludeUsers'] = $excludeUsersNew;
        $request['timeCreated'] = $timestamp;
        $request['couponType'] = $couponTypeNew;
        $request['mrpAmount'] = $MRPAmountNew;
        $request['mrpPercentage'] = $MRPpercentageNew;
        $request['freeShipping']  = $freeShipping;
        $request['status'] = $isActiveNew;
        $request['comments'] = $commentsNew;
        $request['producttypeid'] = $productTypeIds;
        $request['excludeProductTypeIds'] = $excludeProductTypeIds;
        $request['styleIds']  = $styleIds;
        $request['excludeStyleIds'] = $excludeStyleIds;
        $request['catStyleIds'] = $catStyleIds;
        $request['excludeCatStyleIds'] = $excludeCatStyleIds;
        $request['categoryIds'] = $categoryIds;
        $request['excludeCategoryIds'] = $excludeCategoryIds;
        $request['skus'] = $SKUs;
        $request['excludeSKUs']  = $excludeSKUs;
        $request['description'] = $description;
        $request['showInMyMyntra'] = $showInMyMyntra;
        $request['min_count'] = $minCountNew;
        $request['max_count'] = $maxCountNew;
        $request['groupName'] = $groupIdNew;
        return self::post('/coupon', $request);


    }

    public static function modifyCoupon($couponCode, $sdate, $edate,
    $freeShipping, $couponType, $MRPpercentage, $MRPAmount, $maxUsage,
    $isInfinite, $maxUsagePerCart, $maxUsageByUser, $isInfinitePerUser,
    $minAmount, $maxAmount, $isActive, $users,
    $excludeUsers, $comments, $productTypeIds, $excludeProductTypeIds,
    $styleIdString, $excludeStyleIdString, $catStyleIds,
    $excludeCatStyleIds, $categoryIds, $excludeCategoryIds, $time,$lastEditedBy, $description, $showInMyMyntra,$minCount, $maxCount) {


        $request = array();
        $request['code'] = $couponCode;
        $request['startdate'] = $sdate;
        $request['expire'] = $edate;
        $request['freeShipping']  = $freeShipping;
        $request['couponType'] = $couponType;
        $request['mrpPercentage'] = $MRPpercentage;
        $request['mrpAmount'] = $MRPAmount;
        $request['times'] = $maxUsage;
        $request['maximum'] = 0.0;
        $request['isInfinite'] = $isInfinite;
        $request['maxUsagePerCart'] = $maxUsagePerCart;
        $request['maxUsageByUser'] = $maxUsageByUser;
        $request['isInfinitePerUser'] = $isInfinitePerUser;
        $request['minimum'] = $minAmount;
        $request['maxAmount'] = $maxAmount;
        $request['users']  = $users;
        $request['excludeUsers'] = $excludeUsers;
        $request['status'] = $isActive;
        $request['comments'] = $comments;
        $request['producttypeid'] = $productTypeIds;
        $request['excludeProductTypeIds'] = $excludeProductTypeIds;
        $request['styleIds']  = $styleIdString;
        $request['excludeStyleIds'] = $excludeStyleIdString;
        $request['catStyleIds'] = $catStyleIds;
        $request['excludeCatStyleIds'] = $excludeCatStyleIds;
        $request['categoryIds'] = $categoryIds;
        $request['excludeCategoryIds'] = $excludeCategoryIds;
        $request['lastEdited'] = $time;
        $request['lastEditedBy']  = $lastEditedBy;
        $request['description'] = $description;
        $request['showInMyMyntra'] = $showInMyMyntra;
        $request['min_count'] = $minCount;
        $request['max_count'] = $maxCount;
        return self::put('/coupon', $request);




    }

    public static function deleteCouponGroup($groupname) {

        $request = array();
        $request['groupname'] = $groupname;
        return self::deletePost('/couponGroup/'.$groupname, $request);
    }

    public static function deleteCoupon($couponCode) {
        $request = array();
        $request['code'] = $couponCode;
        return self::deletePost('/coupon/'.$couponCode,$request);
    }

    public static function getCouponGroup($groupname) {
        return self::get('/couponGroup/groupName/'.$groupname);
    }


    public static function getCouponBinRange($groupname) {
        return self::get('/couponBinRange/groupName/'.$groupname);
    }

    public static function getCouponUsage($coupon,$login) {
        return self::get('/couponUsage/login/'.$login.'/coupon/'.$coupon);
    }

    public static function lockCouponForUser($couponCode, $userName) {
        
        $request = array();
        $request['coupon'] = $couponCode;
        $request['userId'] = $userName;
        return self::post('/lockUserCoupon',$request);
    }

    public static function updateUsageByUser($couponCode, $userName, $subtotal, $discount, $orderid) {

        $request = array();
        $request['coupon'] = $couponCode;
        $request['login'] = $userName;
        $request['subtotal'] = $subtotal;
        $request['discount'] = $discount;
        $request['orderid'] = $orderid;
        return self::post('/updateUsageByUser',$request);
    }

    public static function unlockCouponForUser($couponCode,$userName) {
        $request = array();
        $request['coupon'] = $couponCode;
        $request['userId'] = $userName;
        return self::post('/unlockUserCoupon',$request);
    }

    public static function unlockAllCoupons() {
        return self::post('/unlockAllCoupons',$request);
    }

    public static function cancelCouponUsageForOrder($orderid,$subtotal,$login,$coupon_discount,$coupon) {

        $request = array();
        $request['coupon'] = $coupon;
        $request['login'] = $login;
        $request['subtotal'] = $subtotal;
        $request['discount'] = $coupon_discount;
        $request['orderid'] = $orderid;
        return self::post('/cancelUsageForOrder',$request);
    }

    public static function getCoupons($time,$login) {
        $request = array();
        $request['expire'] = $time;
        $request['users'] = $login;
        return self::post('/coupons',$request);
    }


    public static function getMyntraCreditsCoupon($login) {
        $request = array();
        $request['users'] = $login;
        return self::post('/myntcreditcoupons',$request);
    }

    public static function getActiveCouponsCount($login,$today) {
        $request = array();
        $request['users'] = $login;
        $request['expire'] = $today;

        return self::post('/getActiveCouponsCount',$request);
    }

    public static function extendExpiryForCoupons($login,$couponCode,$expire,$couponGroup) {
         $request = array();
        $request['expire'] = $expire;
        $request['users']  = $login;
        $request['code'] = $couponCode;
        $request['groupName'] = $couponGroup;

        return self::post('/extendExpiryForCoupons', $request);
    }

    public static function searchCoupons($coupon,$sdate,$edate,$searchGroup ,$searchChannel,$searchUsageGTE ,
        $searchUsageLTE , $searchUsagePerUserGTE, $searchUsagePerUserLTE,   $searchPercentageGTE,
         $searchPercentageLTE,$searchAbsGTE , $searchAbsLTE , $searchFreeShipping , $searchMinGTE,
          $searchMinLTE,  $searchMaxGTE, $searchMaxLTE, $searchDesc ,$searchAllowedUsers, $searchExclUsers,$offset,$rowsPerPage)  {
        $request = array();
         $request['coupon'] = $coupon;
        $request['sdate'] = $sdate?$sdate:null;
        $request['edate']  = $edate?$edate:null;
        $request['groupName'] = $searchGroup;
        $request['channel'] = $searchChannel;
        $request['searchUsageGTE']  = $searchUsageGTE;
        $request['searchUsageLTE'] = $searchUsageLTE;
        $request['searchUsagePerUserGTE'] = $searchUsagePerUserGTE;
        $request['searchUsagePerUserLTE']  = $searchUsagePerUserLTE;
        $request['searchPercentageGTE'] = $searchPercentageGTE;
        $request['searchPercentageLTE'] = $searchPercentageLTE;
        $request['searchAbsGTE']  = $searchAbsGTE;
        $request['searchAbsLTE'] = $searchAbsLTE;
        $request['searchFreeShipping'] = $searchFreeShipping;
        $request['searchMinGTE']  = $searchMinGTE;
        $request['searchMinLTE'] = $searchMinLTE;
        $request['searchMaxGTE'] = $searchMaxGTE;
        $request['searchMaxLTE']  = $searchMaxLTE;
        $request['searchDesc'] = $searchDesc;
        $request['searchAllowedUsers']  = $searchAllowedUsers;
        $request['searchExclUsers'] = $searchExclUsers;
        $request['offset']  = $offset;
        $request['rowsPerpage'] = $rowsPerPage;
        return self::post('/searchcoupons', $request);
    }

     public static function searchCouponGroups($searchGroup)  {
        $request = array();
        $request['groupname'] = $searchGroup;
        return self::post('/searchcoupongroups', $request);
    }


    public static function generateBulkCoupons($couponPrefix, $suffixLength, $count, 
        $groupIdNew, $sdate,
        $edate, $maxUsageNew, $isInfiniteNew, $maxUsagePerCartNew,
        $times_used, $times_lockedNew, $maxUsageByUserNew,
        $isInfinitePerUserNew, $minAmountNew, $maxAmountNew, $usersNew,
        $excludeUsersNew, $timestamp, $creatorNew,
        $couponTypeNew, $MRPAmountNew, $MRPpercentageNew,
        $freeShipping, $isActiveNew, $commentsNew,
        $productTypeIds, $excludeProductTypeIds, $styleIds,
        $excludeStyleIds, $catStyleIds, $excludeCatStyleIds,
        $categoryIds, $excludeCategoryIds, $SKUs, $excludeSKUs, $description, 
        $showInMyMyntra ,$minCountNew,$maxCountNew, $usersListFile) {

        
        $data = array();
        if (!empty($usersListFile)) {
                    $data['file'] =  "@/".realpath($usersListFile);

        }
        $request = array();
        $request['couponPrefix'] = $couponPrefix;
        $request['suffixLength'] = $suffixLength;
        $request['noOfCoupons'] = $count;
        $request['startdate'] = $sdate;
        $request['expire'] = $edate;
        $request['times'] = $maxUsageNew;
        $request['isInfinite'] = $isInfiniteNew;
        $request['maxUsagePerCart'] = $maxUsagePerCartNew;
        $request['times_used']  = $times_used;
        $request['times_locked'] = $times_lockedNew;
        $request['maximum'] = 0.0;
        $request['maxUsageByUser'] = $maxUsageByUserNew;
        $request['isInfinitePerUser'] = $isInfinitePerUserNew;
        $request['minimum'] = $minAmountNew;
        $request['maxAmount'] = $maxAmountNew;
        $request['users']  = $usersNew;
        $request['excludeUsers'] = $excludeUsersNew;
        $request['timeCreated'] = $timestamp;
        $request['provider'] = $creatorNew;
        $request['couponType'] = $couponTypeNew;
        $request['mrpAmount'] = $MRPAmountNew;
        $request['mrpPercentage'] = $MRPpercentageNew;
        $request['freeShipping']  = $freeShipping;
        $request['status'] = $isActiveNew;
        $request['comments'] = $commentsNew;
        $request['producttypeid'] = $productTypeIds;
        $request['excludeProductTypeIds'] = $excludeProductTypeIds;
        $request['styleIds']  = $styleIds;
        $request['excludeStyleIds'] = $excludeStyleIds;
        $request['catStyleIds'] = $catStyleIds;
        $request['excludeCatStyleIds'] = $excludeCatStyleIds;
        $request['categoryIds'] = $categoryIds;
        $request['excludeCategoryIds'] = $excludeCategoryIds;
        $request['skus'] = $SKUs;
        $request['excludeSKUs']  = $excludeSKUs;
        $request['description'] = $description;
        $request['showInMyMyntra'] = $showInMyMyntra;
        $request['min_count'] = $minCountNew;
        $request['max_count'] = $maxCountNew;
        $request['groupName'] = $groupIdNew;
        $data['couponData'] =json_encode($request);
        self::postMultipart($data,"/couponsBulk");
    }

    public static function addExclIncl($groups,$styles,$exclIncl,$op) {

        $request = array();
        $request['groups'] = $groups;
        $request['styleIds'] = $styles;
        $request['inclusion'] =$exclIncl;
        $request['update'] = $op;
        self::post('/bulkOpsInclusionExclusion',$request);
    }

     public static function getCouponsForUserInMyMyntra($login) {
         $couponsResponse = self::get('/getCouponsForUserInMyMyntraAdmin/'.$login);
        return $couponsResponse['coupons'];
    }


    public static function generateRefundCouponForCancellation($couponCode,$userName,$couponDiscount,
                                                        $minCartVal) {
        $request = array();
        $request['coupon'] = $couponCode;
        $request['login'] = $userName;
        $request['mrpAmount'] =$couponDiscount;
        $request['minCartVal'] = $minCartVal;
        $request['couponTypeToGenerate'] = "RFND";
        return self::post('/couponGenerate',$request);

    }


    public static function deleteDiscountCaps($styles) {
        
        return self::post('/updateDiscountCap/delete',$styles);

    }

    public static function insertDiscountCaps($cappings) {
        
        return self::post('/updateDiscountCap/insert',$cappings);

    }

    public static function updateDiscountCaps($cappings) {
        
        return self::post('/updateDiscountCap/update',$cappings);

    }

    public static function getDiscountCaps($styles) {
        
        return self::post('/getDiscountCap',$styles);

    }

    private static function post($action,$data) {
        $url = \HostConfig::$couponServiceurl.$action;
        $method = 'POST';
        $body = json_encode($data);
        $request = new \RestRequest($url, $method, $body);
        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-type: application/json';
        $headers[] = 'Accept-Language: en-US';
        $request->setHttpHeaders($headers);
        $request->execute();
        return self::extractResponse($request);
    }

    private static function put($action,$data) {
        $url = \HostConfig::$couponServiceurl.$action;
        $method = 'PUT';
        $body = json_encode($data);
        $request = new \RestRequest($url, $method, $body);
        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-type: application/json';
        $headers[] = 'Accept-Language: en-US';
        $request->setHttpHeaders($headers);
        $request->execute();
        return self::extractResponse($request);

    }

    private static function deletePost($action,$data) {
        $url = \HostConfig::$couponServiceurl.$action;
        $method = 'DELETE';
        $body = json_encode($data);
        $request = new \RestRequest($url, $method, $body);
        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-type: application/json';
        $headers[] = 'Accept-Language: en-US';
        $request->setHttpHeaders($headers);
        $request->execute();
        return self::extractResponse($request);
    }

    private static function get($action) {
        $url = \HostConfig::$couponServiceurl.$action;
        $method = 'GET';
        $request = new \RestRequest($url, $method);
        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-type: application/json';
        $headers[] = 'Accept-Language: en-US';
        $request->setHttpHeaders($headers);
        $request->execute();
        return self::extractResponse($request);

    }

    private static function postMultipart($data,$action) {


        $img=file_get_contents($filename);    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_COOKIE, $cookie);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch , CURL_HTTPHEADER , "Content-Type: multipart/form-data" );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_REFERER, "http://internalweb.com/");
        curl_setopt($ch, CURLOPT_URL, \HostConfig::$couponServiceurl.$action);
        $page = curl_exec($ch);

    }
    private static function extractResponse($request) {
        $info = $request->getResponseInfo();
        $body = $request->getResponseBody();
        if($info['http_code'] == 200){
            $response = json_decode($body,true);
            if ($response["status"]["statusCode"]==0 || $response["status"]["statusCode"]==3 || $response["status"]["statusCode"]==200) {
                return $response["data"];
            } else {
                return null;
            }
        } else {
            return null;
        }

}

}

?>
