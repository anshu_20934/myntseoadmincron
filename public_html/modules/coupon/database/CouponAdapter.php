<?php
include_once ("../auth.php");
include_once($xcart_dir."/Profiler/Profiler.php");
require_once "$xcart_dir/modules/coupon/Coupon.php";
require_once "$xcart_dir/modules/coupon/exception/CouponNotFoundException.php";
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";
include_once "$xcart_dir/modules/coupon/CouponDiscountCalculator.php";

require_once $xcart_dir.'/include/class/csvutils/class.csvutils.ArrayCSVWriter.php';

include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");

include_once("$xcart_dir/modules/coupon/database/couponAPIClient.php");


/**
 * This singleton class provides database access for the coupon related
 * functionality.
 *
 * @author Rohan.Railkar
 */
class CouponAdapter
{
    private $tbl_coupons;
    private $tbl_coupon_groups;
    private $tbl_coupon_usage;
    private $tbl_orders;
    private $tbl_coupon_templates;
    private $tbl_cashback_log;
	private $couponMyntraCreditAmount=0;
    private $tbl_coupon_group_bin_range;
	
    /**
     * The singleton instance.
     */
    private static $instance = NULL;

    /**
     * @return CouponAdapter The singleton instance.
     */
    public static function getInstance()
    {
        if (CouponAdapter::$instance == NULL) {
            CouponAdapter::$instance = new CouponAdapter();
        }

        return CouponAdapter::$instance;
    }

    /**
     * Constructor.
     */
    private function __construct()
    {
        global $sql_tbl;
        $this->tbl_coupons = $sql_tbl['discount_coupons'];
        $this->tbl_coupon_groups = $sql_tbl['coupon_group'];
        $this->tbl_coupon_usage = $sql_tbl['discount_coupons_login'];
        $this->tbl_orders = $sql_tbl['orders'];
        $this->tbl_coupon_templates = $sql_tbl['coupon_templates'];
        $this->tbl_cashback_log = $sql_tbl['cashback_log'];
        
        //$this->tbl_cashback_account = $sql_tbl['cashback_account'];
        $this->tbl_cashback_account = 'mk_cashback_account';
        //$this->tbl_cashback_transaction_log = $sql_tbl['cashback_transaction_log'];
        $this->tbl_cashback_transaction_log = 'mk_cashback_transacion_log';
        $this->tbl_coupon_group_bin_range = "mk_coupon_bin_range";
    }

    /**
     * Fetch details of the coupon from database and return an instance of a
     * Coupon.
     *
     * @param String $couponCode
     * @return Coupon The coupon fetched from database.
     * @throws CouponNotFoundException
     */
    function fetchCoupon($couponCode,$ro=false)
    {
    	/*
    	$rofg = FeatureGateKeyValuePairs::getBoolean('readOnlyCouponFetchInCart');
    	// overrride $ro if feature gate value is false
    	if(!$rofg){
    		$ro = false;
    	}
    	
        // Query the database in Read-Only fashion.
        $query = "SELECT *
        			FROM " . $this->tbl_coupons . " 
        		   WHERE coupon = '$couponCode';";
        
       	$couponUsed = db_query($query,$ro);

        // Fetch an associative array.
        $row = array();
        $row = db_fetch_array($couponUsed);

        if (empty($row)) {
            throw new CouponNotFoundException($couponCode);
        }

        // Find the channel ID for the coupons, from its group.
        $channel = db_query("SELECT channel,group_type,applicable_device
        			  		   FROM " . $this->tbl_coupon_groups . " 
        			 		  WHERE groupname = '" . $row['groupName'] . "';",$ro);
        $array = db_fetch_array($channel);
        $channel = $array['channel'];
        $groupType = $array['group_type'];
        $applicable_device = $array['applicable_device'];

        // Construct the coupon.
        $coupon = new Coupon($row['coupon'],
        $row['groupName'],
        $channel,
        $row['startdate'],
        $row['expire'],
        $row['times'],
        $row['isInfinite'],
        $row['maxUsagePerCart'],
        $row['times_used'],
        $row['times_locked'],
        $row['maxUsageByUser'],
        $row['isInfinitePerUser'],
        $row['minimum'],
        $row['maxAmount'],
        $row['users'],
        $row['excludeUsers'],
        $row['paymentMethod'],
        $row['timeCreated'],
        $row['provider'],
        $row['comments'],
        $row['couponType'],
        $row['MRPAmount'],
        $row['MRPpercentage'],
        $row['freeShipping'],
        $row['status'],
        $row['productTypeIds'],
        $row['excludeProductTypeIds'],
        $row['styleIds'],
        $row['excludeStyleIds'],
        $row['catStyleIds'],
        $row['excludeCatStyleIds'],
        $row['categoryIds'],
        $row['excludeCategoryIds'],
        $row['SKUs'],        // SKUs
        $row['excludeSKUs'],
        $row['min_count'],
        $row['max_count'],
        $groupType,
        $applicable_device);

        return $coupon;*/
        $row =  CouponAPIClient::fetchCoupon($couponCode);

        $coupon = new Coupon($row['code'],
        $row['couponGroup']['groupname'],
        $row['couponGroup']['channel'],
        $row['startdate'],
        $row['expire'],
        $row['times'],
        $row['isInfinite'],
        $row['maxUsagePerCart'],
        $row['times_used'],
        $row['times_locked'],
        $row['maxUsageByUser'],
        $row['isInfinitePerUser'],
        $row['minimum'],
        $row['maxAmount'],
        $row['users'],
        $row['excludeUsers'],
        $row['paymentMethod'],
        $row['timeCreated'],
        $row['provider'],
        $row['comments'],
        $row['couponType'],
        $row['mrpAmount'],
        $row['mrpPercentage'],
        $row['freeShipping'],
        $row['status'],
        $row['productTypeIds'],
        $row['excludeProductTypeIds'],
        $row['styleIds'],
        $row['excludeStyleIds'],
        $row['catStyleIds'],
        $row['excludeCatStyleIds'],
        $row['categoryIds'],
        $row['excludeCategoryIds'],
        $row['skus'],        // SKUs
        $row['excludeSKUs'],
        $row['min_count'],
        $row['max_count'],
        $row['couponGroup']['groupType'],
        $row['couponGroup']['applicableDevice']);

        return $coupon;

    }

    /**
     * Whether a coupon group already exists with the specified name.
     */
    public function existsCouponGroup($groupname)
    {
        /*$result = func_query_first("SELECT groupname
        		    				  FROM " . $this->tbl_coupon_groups . "
        						     WHERE groupname = '$groupname';");
                                     */
        $result = CouponAPIClient::getCouponGroup($groupname);
        return !empty($result);
    }

    /**
     * Whether a coupon already exists with the specified coupon code.
     */
    public function existsCoupon($couponCode)
    {
       /* $result = func_query_first("SELECT coupon
        						      FROM " . $this->tbl_coupons . "
        						     WHERE coupon = '$couponCode';");
                                     */
        $result = CouponAPIClient::fetchCoupon($couponCode);    
        return !empty($result);
    }
    
    /**
     * Whether a coupon template already exists with the specified name.
     */
    public function existsCouponTemplate($name)
    {
        $result = func_query_first("SELECT templateName
        						      FROM " . $this->tbl_coupon_templates . "
        						     WHERE templateName = '$name';");
        
        return !empty($result);
    }

    /**
     * Create the coupon group.
     */
    public function createCouponGroup($groupname,
                                      $channel,
                                      $timestamp,
                                      $creator,
                                      $template = null, $groupType="",$applicabelDeviceNew=null)
    {
        /*if(empty($applicabelDeviceNew)){
           db_query(
            "INSERT INTO " . $this->tbl_coupon_groups . "
                (groupname, channel, templateName, timeCreated, lastEdited, creator, lastEditedBy, active, count,group_type,applicable_device)
             VALUES
                ('$groupname', '$channel', '$template', '$timestamp', '$timestamp', '$creator', '$creator', 'A', 0,'$groupType',null);");     
        }
        else{
           db_query(
        	"INSERT INTO " . $this->tbl_coupon_groups . "
	        	(groupname, channel, templateName, timeCreated, lastEdited, creator, lastEditedBy, active, count,group_type,applicable_device)
	         VALUES
	        	('$groupname', '$channel', '$template', '$timestamp', '$timestamp', '$creator', '$creator', 'A', 0,'$groupType','$applicabelDeviceNew');");
        }*/

        CouponAPIClient::createCouponGroup($groupname,
                                      $channel,
                                      $timestamp,
                                      $creator,$groupType,'A',0,$applicabelDeviceNew);
    }

    /**
     * Create the coupon bin range
     */
    public function createCouponGroupBinRange($groupname,$bin_range)
    {
        $bin_ranges = explode(",", $bin_range);
        foreach($bin_ranges as $bin){
            if(!empty($bin)){
                $bins = explode("-",$bin);
                $start_bin = $bins[0];
                $end_bin =  count($bins) > 1?$bins[1]:$bins[0];
                CouponAPIClient::createCouponGroupBinRange($groupname,$start_bin,$end_bin);
            }
        }          
        
    }


    /**
     * Modify Coupon Bin Range
     */
    public function modifyCouponGroupBinRange($groupname,$bin_range)
    {
        CouponAPIClient::modifyCouponGroupBinRange($groupname,$bin_range);
    }

    /**
     * Update coupon group data. Reflect the status change for coupons.
     */
    public function modifyCouponGroup($groupname, $channel, $status, $template=null, $editedBy=null,$groupType="",$applicabelDeviceNew=null)
    {
        $time = time();

        CouponAPIClient::modifyCouponGroup($groupname,$channel,$status,$editedBy,$time,$groupType,$applicabelDeviceNew);
        /*if(empty($applicabelDeviceNew)){

            $query = "UPDATE " . $this->tbl_coupon_groups . "
                        SET channel = '$channel', active = '$status', lastEdited = '$time', templateName = '$template', lastEditedBy = '$editedBy', group_type='$groupType',applicable_device=null
                      WHERE groupname = '$groupname';";
        }
        else{
        
            $query = "UPDATE " . $this->tbl_coupon_groups . "
        	   		 SET channel = '$channel', active = '$status', lastEdited = '$time', templateName = '$template', lastEditedBy = '$editedBy', group_type='$groupType',applicable_device='$applicabelDeviceNew'
        		     WHERE groupname = '$groupname';";
        }
         
        db_query($query);
         
        db_query("UPDATE " . $this->tbl_coupons . "
					 SET status = '$status' 
				   WHERE groupName = '$groupname';");
                */
        
        // Set active_count.
		/*
        db_query("UPDATE " . $this->tbl_coupon_groups . " 
        			 SET active_count = 
        			 	 (SELECT count(*)
        			 	    FROM " . $this->tbl_coupons . " 
        			 	   WHERE status = 'A'
        			 	     AND groupName = '$groupname')
        		   WHERE groupname = '$groupname';");
       */
    }

    
    public function getCouponForBulkInsert($couponCodeNew, $groupIdNew, $sdate,
    $edate, $maxUsageNew, $isInfiniteNew, $maxUsagePerCartNew,
    $times_used, $times_lockedNew, $maxUsageByUserNew,
    $isInfinitePerUserNew, $minAmountNew, $maxAmountNew, $usersNew,
    $excludeUsersNew, $timestamp, $creatorNew,
    $couponTypeNew, $MRPAmountNew, $MRPpercentageNew,
    $freeShipping, $isActiveNew, $commentsNew,
    $productTypeIds, $excludeProductTypeIds, $styleIds,
    $excludeStyleIds, $catStyleIds, $excludeCatStyleIds,
    $categoryIds, $excludeCategoryIds, $SKUs, $excludeSKUs, $description, $showInMyMyntra){
    	
    }
    
    
    /**
     * Add a coupon to the coupons table, and increment the count in coupon
     * groups table for the corresponding group.
     */
    public function addCoupon($couponCodeNew, $groupIdNew, $sdate,
    $edate, $maxUsageNew, $isInfiniteNew, $maxUsagePerCartNew,
    $times_used, $times_lockedNew, $maxUsageByUserNew,
    $isInfinitePerUserNew, $minAmountNew, $maxAmountNew, $usersNew,
    $excludeUsersNew, $timestamp, $creatorNew,
    $couponTypeNew, $MRPAmountNew, $MRPpercentageNew,
    $freeShipping, $isActiveNew, $commentsNew,
    $productTypeIds, $excludeProductTypeIds, $styleIds,
    $excludeStyleIds, $catStyleIds, $excludeCatStyleIds,
    $categoryIds, $excludeCategoryIds, $SKUs, $excludeSKUs, $description, $showInMyMyntra,$minCountNew=0,$maxCountNew=0)
    {
        $count = 0;
        $groupSuccess = false;

        // Update count in groups table.
        /*if (!empty($groupIdNew)) {

            $query = "SELECT count
                    	FROM " . $this->tbl_coupon_groups . "
                   	   WHERE groupname = '$groupIdNew';";

            $group = db_query($query, true);
            $row = db_fetch_array($group);

            // If group exists,
            if ($row != null) {
                $count = $row['count'];
                //$count++;

                //$query = "UPDATE " . $this->tbl_coupon_groups . "
            	//		 	 SET count = '$count', active_count = active_count + 1
            	//	   	   WHERE groupname = '$groupIdNew'";

                //$groupSuccess = db_query($query);
            }

            // No such group exists.
            else {
                $groupIdNew = null;
            }
        }*/
if($MRPpercentageNew==null)
{
	$MRPpercentageNew=0;
}
    CouponAPIClient::addCoupon($couponCodeNew, $groupIdNew, $sdate,
    $edate, $maxUsageNew, $isInfiniteNew, $maxUsagePerCartNew,
    $times_used, $times_lockedNew, $maxUsageByUserNew,
    $isInfinitePerUserNew, $minAmountNew, $maxAmountNew, $usersNew,
    $excludeUsersNew, $timestamp, $creatorNew,
    $couponTypeNew, $MRPAmountNew, $MRPpercentageNew,
    $freeShipping, $isActiveNew, $commentsNew,
    $productTypeIds, $excludeProductTypeIds, $styleIds,
    $excludeStyleIds, $catStyleIds, $excludeCatStyleIds,
    $categoryIds, $excludeCategoryIds, $SKUs, $excludeSKUs, $description, $showInMyMyntra,$minCountNew,$maxCountNew);
      /*  $query =
        	"INSERT INTO " . $this->tbl_coupons . " (
        		coupon, groupName, startdate, expire, times,
        		isInfinite, maxUsagePerCart, times_used, times_locked, 
        		maxUsageByUser, isInfinitePerUser, minimum, maxAmount, users, 
        		excludeUsers, timeCreated, provider, couponType, MRPAmount, 
        		MRPpercentage, freeShipping, status, comments, productTypeIds, 
        		excludeProductTypeIds, styleIds, excludeStyleIds, catStyleIds, 
        		excludeCatStyleIds, categoryIds, excludeCategoryIds, SKUs,
        		excludeSKUs, description, showInMyMyntra, lastEdited, lastEditedBy,min_count,max_count) 
        	 VALUES (
        	    '$couponCodeNew', '$groupIdNew', '$sdate',
        	    '$edate', '$maxUsageNew', '$isInfiniteNew', 
        	    '$maxUsagePerCartNew', '$times_used', '$times_lockedNew', 
        	    '$maxUsageByUserNew', '$isInfinitePerUserNew', '$minAmountNew', 
        	    '$maxAmountNew', '$usersNew', '$excludeUsersNew', '$timestamp', 
        	    '$creatorNew', '$couponTypeNew', '$MRPAmountNew', 
        	    '$MRPpercentageNew', '$freeShipping', '$isActiveNew', 
        	    '$commentsNew', '$productTypeIds', '$excludeProductTypeIds', 
        	    '$styleIds', '$excludeStyleIds', '$catStyleIds', 
        	    '$excludeCatStyleIds', '$categoryIds', '$excludeCategoryIds', 
        	    '$SKUs', '$excludeSKUs', '$description', '$showInMyMyntra',
        	    '$timestamp', '$creatorNew','$minCountNew','$maxCountNew');";

        // Add the coupon to db.
        $success = db_query($query);
        */
        // If there is an error adding the coupon, reset the group count.
        /* if (!$success && $groupIdNew != null && $groupSuccess) {
            $count--;
            $query = "UPDATE " . $this->tbl_coupon_groups . "
            			 SET count = '$count', active_count = active_count - 1
            		   WHERE groupname = '$groupIdNew'";

            db_query($query);
        }*/
    }
    
    /*
     * generate bulk coupons
     */
    public function generateBulkCoupons($couponPrefix, $suffixLength, $count, 
        $groupIdNew, $sdate,
        $edate, $maxUsageNew, $isInfiniteNew, $maxUsagePerCartNew,
        $times_used, $times_lockedNew, $maxUsageByUserNew,
        $isInfinitePerUserNew, $minAmountNew, $maxAmountNew, $usersNew,
        $excludeUsersNew, $timestamp, $creatorNew,
        $couponTypeNew, $MRPAmountNew, $MRPpercentageNew,
        $freeShipping, $isActiveNew, $commentsNew,
        $productTypeIds, $excludeProductTypeIds, $styleIds,
        $excludeStyleIds, $catStyleIds, $excludeCatStyleIds,
        $categoryIds, $excludeCategoryIds, $SKUs, $excludeSKUs, $description, 
        $showInMyMyntra ,$minCountNew=0,$maxCountNew=0, $usersListFile=null)
 	{


        CouponAPIClient::generateBulkCoupons($couponPrefix, $suffixLength, $count, 
        $groupIdNew, $sdate,
        $edate, $maxUsageNew, $isInfiniteNew, $maxUsagePerCartNew,
        $times_used, $times_lockedNew, $maxUsageByUserNew,
        $isInfinitePerUserNew, $minAmountNew, $maxAmountNew, $usersNew,
        $excludeUsersNew, $timestamp, $creatorNew,
        $couponTypeNew, $MRPAmountNew, $MRPpercentageNew,
        $freeShipping, $isActiveNew, $commentsNew,
        $productTypeIds, $excludeProductTypeIds, $styleIds,
        $excludeStyleIds, $catStyleIds, $excludeCatStyleIds,
        $categoryIds, $excludeCategoryIds, $SKUs, $excludeSKUs, $description, 
        $showInMyMyntra ,$minCountNew,$maxCountNew, $usersListFile);
 		// global $xcart_dir;
 		
   //  	if($usersListFile ==null)
   //  	{
    		
   //  		$time = date("YmdHis",time());
	  //   	$outFileName ="$xcart_dir/var/tmp/bulkCoupons$time.csv";
	    	
	  //   	$outfile = fopen($outFileName, "w+");
	    	
	  //   	if($outfile!==false)
	  //   	{
	    		
	  //   		for ($i = 0; $i < $count; $i++){
	  //   			$couponCode = $couponPrefix . $this->getRandomString($suffixLength);
	    			
	    			
	  //   			while ($this->existsCoupon($couponCode) || isset($customers_coupon_email_map[$couponCode]) ){
	    				
	  //   				$couponCode =
	  //   				$couponPrefix . $this->getRandomString($suffixLength);
	    				 
	  //   			}
	  //   			$customers_coupon_email_map[$couponCode] = $usersNew;
	  //   		}
	    		
	  //   		foreach ($customers_coupon_email_map as $couponCodeNew=>$usersNew){
	  //   			fwrite($outfile,"'$couponCodeNew','$groupIdNew','$sdate','$edate','$maxUsageNew','$isInfiniteNew','$maxUsagePerCartNew','$times_used','$times_lockedNew','$maxUsageByUserNew','$isInfinitePerUserNew','$minAmountNew','$maxAmountNew','$minCountNew','$maxCountNew','$usersNew','$excludeUsersNew','$timestamp','$creatorNew','$couponTypeNew','$MRPAmountNew','$MRPpercentageNew','$freeShipping','$isActiveNew','$commentsNew','$productTypeIds','$excludeProductTypeIds','$styleIds','$excludeStyleIds','$catStyleIds','$excludeCatStyleIds','$categoryIds','$excludeCategoryIds','$SKUs','$excludeSKUs','$description','$showInMyMyntra','$timestamp','$creatorNew'\n");
	    			
	  //   		}
	    		
	  //   		fclose($outfile);
	    		
	  //   		if ($this->bulkFileInsert($outFileName)){
	    			
	    			
	  //   			if (!file_exists("$xcart_dir/var/tmp/bulk_coupons_output")) {
	  //   				mkdir("$xcart_dir/var/tmp/bulk_coupons_output");
	  //   			}
	    			
	  //   			$outFileName ="$xcart_dir/var/tmp/bulk_coupons_output/bulkCouponsFinal$time.csv";
	    			
	  //   			$outfile = fopen($outFileName, "w");
	    			
	  //   			if($outfile !== false){
	    				
	  //   				foreach ($customers_coupon_email_map as $couponCodeNew=>$usersNew){
	    					
	  //   					$insertResult = func_query_first("select 1 from xcart_discount_coupons where coupon = '$couponCodeNew' and users = '$usersNew' ");
	    					
	  //   					if(!empty($insertResult)){
	  //   						fwrite($outfile, " $usersNew, $couponCodeNew \n");
	  //   					}
	    						
	  //   				}
	    			
	    				
	  //   			}
	    			
	  //   			fclose($outfile);
	    			
	  //   			/*header("Content-Type: application/zip");
	  //   			header("Content-Disposition: attachment; filename=bulkCouponsFinal$time.csv");
	  //   			header("Content-Length: " . filesize($outFileName));
	    			
	  //   			readfile($outFileName);*/
	  //   			return $outFileName;
	  //   		}
   //  		}else{
	  //   		echo "error in creating local bulk file";
	  //   		exit();
	  //   	}
	    	
   //  	}    	
   //  	else{
    		    		
   //  		$ft = fopen($usersListFile,"r");
    	
    
	  //   	$customers_coupon_email_map = array();
	    	
	  //   	while(!feof($ft))	{
	  //   		$line = fgets($ft);
	  //   		if ($line != "") {
	  //   			$array = explode(",", trim($line));
	  //   			$array[0] = mysql_real_escape_string($array[0]);
	    
	    			
	    			
	  //   			$couponCode = $couponPrefix . $this->getRandomString($suffixLength);
	  //   			while ($this->existsCoupon($couponCode) || isset($customers_coupon_email_map[$couponCode]) ){
	  //   				$couponCode =
	  //   				$couponPrefix . $this->getRandomString($suffixLength);
	    				
	  //   			}
	  //   			$customers_coupon_email_map[$couponCode] = $array[0];
	  //   		}
	  //   	}
	  //   	fclose($ft);
	    	
	  //   	$time = date("YmdHis",time());
	  //   	$outFileName ="$xcart_dir/var/tmp/bulkCoupons$time.csv";
			
	  //   	$outfile = fopen($outFileName, "w+");
	    	
	  //   	if($outfile !== false){
	    	
	  //   		foreach ($customers_coupon_email_map as $couponCodeNew=>$usersNew){
	  //   			fwrite($outfile,"'$couponCodeNew','$groupIdNew','$sdate','$edate','$maxUsageNew','$isInfiniteNew','$maxUsagePerCartNew','$times_used','$times_lockedNew','$maxUsageByUserNew','$isInfinitePerUserNew','$minAmountNew','$maxAmountNew','$minCountNew','$maxCountNew','$usersNew','$excludeUsersNew','$timestamp','$creatorNew','$couponTypeNew','$MRPAmountNew','$MRPpercentageNew','$freeShipping','$isActiveNew','$commentsNew','$productTypeIds','$excludeProductTypeIds','$styleIds','$excludeStyleIds','$catStyleIds','$excludeCatStyleIds','$categoryIds','$excludeCategoryIds','$SKUs','$excludeSKUs','$description','$showInMyMyntra','$timestamp','$creatorNew'\n");
	    			
	    			
	  //   		}
	    		
	  //   		fclose($outfile);
		    	
	  //   	if ($this->bulkFileInsert($outFileName)){
	    			
	    			
	  //   			if (!file_exists("$xcart_dir/var/tmp/bulk_coupons_output")) {
	  //   				mkdir("$xcart_dir/var/tmp/bulk_coupons_output");
	  //   			}
	    			
	  //   			$outFileName ="$xcart_dir/var/tmp/bulk_coupons_output/bulkCouponsFinal$time.csv";
	    			
	  //   			$outfile = fopen($outFileName, "w+");
	    			
	  //   			if($outfile !== false){
	    				
	  //   				foreach ($customers_coupon_email_map as $couponCodeNew=>$usersNew){
	    					
	  //   					$insertResult = func_query_first("select 1 from xcart_discount_coupons where coupon = '$couponCodeNew' and users = '$usersNew' ");
	    					
	  //   					if(!empty($insertResult)){
	  //   						fwrite($outfile, " $usersNew, $couponCodeNew \n");
	  //   					}else{
	  //   						fwrite($outfile, " $usersNew, NOCOUPON \n");
	  //   					}
	    						
	  //   				}
	    			
	    				
	  //   			}
	    			
	  //   			fclose($outfile);
	    			
	  //   			/*header("Content-Type: application/zip");
	  //   			header("Content-Disposition: attachment; filename=bulkCouponsFinal$time.csv");
	  //   			header("Content-Length: " . filesize($outFileName));
	    			
	  //   			readfile($outFileName);*/
	  //   			return $outFileName;
	  //   		}
	  //   	}else{
	  //   		echo "error in creating local bulk file";
	  //   		exit();
	  //   	}
	    	
	    	
	    	
   //  	}
    
    }
    
    
    public function bulkFileInsert($filePath){
    	$qry = "LOAD DATA LOCAL INFILE '$filePath' 
    			IGNORE INTO TABLE xcart_discount_coupons FIELDS TERMINATED BY ','  enclosed by \"\'\"  LINES TERMINATED BY '\\n' 				
    			(
        		coupon, groupName, startdate, expire, times,
        		isInfinite, maxUsagePerCart, times_used, times_locked, 
        		maxUsageByUser, isInfinitePerUser, minimum, maxAmount,min_count,max_count,users, 
        		excludeUsers, timeCreated, provider, couponType, MRPAmount, 
        		MRPpercentage, freeShipping, status, comments, productTypeIds, 
        		excludeProductTypeIds, styleIds, excludeStyleIds, catStyleIds, 
        		excludeCatStyleIds, categoryIds, excludeCategoryIds, SKUs,
        		excludeSKUs, description, showInMyMyntra, lastEdited, lastEditedBy) 
    	
    	";
    	
    	return $this->mysqlLoadFileQuery($qry);
    }
    
    
    private function mysqlLoadFileQuery($qry){
    	$rdb = DBConfig::$rw;
    	
    	$returnStr = true;
    	
    	$con = mysql_connect($rdb["host"],$rdb["user"],$rdb["password"],true,128);
    	if (!$con) {
    		die('Could not connect: ' . mysql_error());
    	}
    	
    	
    	if(mysql_select_db($rdb["db"],$con)){
    		if(!mysql_query($qry,$con)){
    			$returnStr= mysql_error($con);
    		}    		
    	}else{
    		$returnStr = mysql_error($con);
    	}
    	
    	
    	mysql_close($con);
    	return true;
    	
    }
    
    /**
     * Add multiple coupons with same description in a group.
     */
    public function addMultipleCoupons($couponPrefix, $suffixLength, $count, 
        $groupIdNew, $sdate,
        $edate, $maxUsageNew, $isInfiniteNew, $maxUsagePerCartNew,
        $times_used, $times_lockedNew, $maxUsageByUserNew,
        $isInfinitePerUserNew, $minAmountNew, $maxAmountNew, $usersNew,
        $excludeUsersNew, $timestamp, $creatorNew,
        $couponTypeNew, $MRPAmountNew, $MRPpercentageNew,
        $freeShipping, $isActiveNew, $commentsNew,
        $productTypeIds, $excludeProductTypeIds, $styleIds,
        $excludeStyleIds, $catStyleIds, $excludeCatStyleIds,
        $categoryIds, $excludeCategoryIds, $SKUs, $excludeSKUs, $description, 
        $showInMyMyntra,$minCountNew=0,$maxCountNew=0)
    {
        // Iterate over the count.
        for ($i = 0; $i < $count; $i++) {
            
            // Get a unique coupon code.
            $couponCode = $couponPrefix . $this->getRandomString($suffixLength);
            while ($this->existsCoupon($couponCode)) {
                $couponCode = 
                    $couponCodePrefix . $this->getRandomString($suffixLength);
            }
            
            // Add the coupon
            $this->addCoupon($couponCode, $groupIdNew, $sdate, $edate, 
                $maxUsageNew, $isInfiniteNew, $maxUsagePerCartNew, $times_used, 
                $times_lockedNew, $maxUsageByUserNew, $isInfinitePerUserNew, 
                $minAmountNew, $maxAmountNew, $usersNew, $excludeUsersNew, 
                $timestamp, $creatorNew, $couponTypeNew, $MRPAmountNew, 
                $MRPpercentageNew, $freeShipping, $isActiveNew, $commentsNew, 
                $productTypeIds, $excludeProductTypeIds, $styleIds, 
                $excludeStyleIds, $catStyleIds, $excludeCatStyleIds, 
                $categoryIds, $excludeCategoryIds, $SKUs, $excludeSKUs, 
                $description, $showInMyMyntra,$minCountNew,$maxCountNew);
        }
    }
    
    /**
     * Add a new coupon template.
     */
    public function addTemplate($name, $validity, $freeShipping, $type, 
        $MRPAmount, $MRPpercentage, $maxUsage, $isInfinite, $maxUsagePerCart, 
        $maxUsageByUser, $isInfinitePerUser, $minimum, $maxAmount, $users, 
        $excludedUsers, $comments, $productTypeIds, $styleIds,
        $excludeProductTypeIds, $excludeStyleIds, $creator, $description, $showInMyMyntra)
    {
        $time = time();
    if($MRPpercentage=='' || $MRPpercentage==null)
{
	$MRPpercentage=100;
}

        $query = 
            "INSERT INTO " . $this->tbl_coupon_templates . " 
            	(templateName, validity, freeShipping, couponType, MRPAmount, 
            	 MRPpercentage, times, isInfinite, maxUsagePerCart, 
        		 maxUsageByUser, isInfinitePerUser, minimum, maxAmount, users, 
        		 excludeUsers, comments, productTypeIds, styleIds, lastEdited,
        		 excludeProductTypeIds, excludeStyleIds, timeCreated, 
        		 creator, lastEditedBy, description, showInMyMyntra)
        	 VALUES ('$name', '$validity', '$freeShipping', '$type', '$MRPAmount', 
                 '$MRPpercentage', '$maxUsage', '$isInfinite', '$maxUsagePerCart', 
                 '$maxUsageByUser', '$isInfinitePerUser', '$minimum', '$maxAmount', 
                 '$users', '$excludedUsers', '$comments', '$productTypeIds', 
                 '$styleIds', '$time', '$excludeProductTypeIds',
                 '$excludeStyleIds', '$time', '$creator', '$creator', '$description', '$showInMyMyntra');";
                 
         db_query($query);
    }
    
    /**
     * Modify the coupon template
     */
    public function modifyTemplate($name, $validity, $freeShipping, $type, 
        $MRPAmount, $MRPpercentage, $maxUsage, $isInfinite, $maxUsagePerCart, 
        $maxUsageByUser, $isInfinitePerUser, $minimum, $maxAmount, $users, 
        $excludedUsers, $comments, $productTypeIds, $styleIds,
        $excludeProductTypeIds, $excludeStyleIds, $timeCreated, $creator, $login,
        $description, $showInMyMyntra)
    {
        $time = time();
        
        $query = 
            "REPLACE INTO " . $this->tbl_coupon_templates . " 
            	(templateName, validity, freeShipping, couponType, MRPAmount, 
            	 MRPpercentage, times, isInfinite, maxUsagePerCart, 
        		 maxUsageByUser, isInfinitePerUser, minimum, maxAmount, users, 
        		 excludeUsers, comments, productTypeIds, styleIds, lastEdited,
        		 excludeProductTypeIds, excludeStyleIds, timeCreated, creator, 
        		 lastEditedBy, description, showInMyMyntra)
        	 VALUES ('$name', '$validity', '$freeShipping', '$type', '$MRPAmount', 
                 '$MRPpercentage', '$maxUsage', '$isInfinite', '$maxUsagePerCart', 
                 '$maxUsageByUser', '$isInfinitePerUser', '$minimum', '$maxAmount', 
                 '$users', '$excludedUsers', '$comments', '$productTypeIds', 
                 '$styleIds', '$time', '$excludeProductTypeIds',
                 '$excludeStyleIds', '$timeCreated', '$creator', '$login', 
                 '$description', '$showInMyMyntra');";
                 
         db_query($query);
    }
    
    /**
     * Get coupon template names in an 
     */
    public function getCouponTemplates()
    {
        $query = "SELECT templateName FROM " . $this->tbl_coupon_templates . ";";
        $result = db_query($query, true);

        $templates = array();
        if ($result) {
            while ($row = db_fetch_row($result)) {
                array_push($templates, $row[0]);
            }
        }

        return $templates;
    }

    /**
     * Fetch group from database.
     */
    public function fetchCouponGroupArray($groupname)
    {
        /*$query = "SELECT *
        			FROM " . $this->tbl_coupon_groups . "
        		   WHERE groupname = '$groupname';";
        return func_query_first($query);*/
        return CouponAPIClient::getCouponGroup($groupname);
    }

    /**
     * Fetch group from database.
     */
    public function fetchCouponBinRange($groupname)
    {
        /*$query = "SELECT *
                    FROM " . $this->tbl_coupon_group_bin_range . "
                   WHERE group_name = '$groupname';";

        $raw_data = func_query($query);
        $binRanges = "";
        foreach ($raw_data as $row) {
            $binRanges .= $row["start_bin"]."-".$row["end_bin"].",";
        }
        return $binRanges;
        */

        $couponBinRange = CouponAPIClient::getCouponBinRange($groupname);
        return $couponBinRange;
    }

    /**
     * Fetch coupon details in an array.
     */
    public function fetchCouponArray($couponCode)
    {
        /*$query = "SELECT *
        			FROM " . $this->tbl_coupons . " 
        		   WHERE coupon = '$couponCode';";

        return func_query_first($query);
        */
        return CouponAPIClient::fetchCoupon($couponCode);
    }

    /**
     * Edit a coupon.
     */
    public function modifyCoupon($couponCode, $sdate, $edate,
    $freeShipping, $couponType, $MRPpercentage, $MRPAmount, $maxUsage,
    $isInfinite, $maxUsagePerCart, $maxUsageByUser, $isInfinitePerUser,
    $minAmount, $maxAmount, $isActive, $users,
    $excludeUsers, $comments, $productTypeIds, $excludeProductTypeIds,
    $styleIdString, $excludeStyleIdString, $catStyleIds,
    $excludeCatStyleIds, $categoryIds, $excludeCategoryIds, $lastEditedBy=null, $description='', $showInMyMyntra=1,$minCount=0, $maxCount=0)
    {
        $time = time();
        
       /* $query = "SELECT times_used, status, groupName 
                    FROM " . $this->tbl_coupons . "  
                   WHERE coupon = '$couponCode';";

        $couponUsed = db_query($query, true);
        

        // Fetch an associative array.
        $row = array();
        $row = db_fetch_array($couponUsed);
        */
        $row = CouponAPIClient::fetchCoupon($couponCode);

        $times_used = $row['times_used'];
        $status= $row['status']; 
        $groupName = $row['groupName'];

        // ALERT :: use times_used, times_locked 
        // If the usage is exhausted, change the status to 'Used'.
        if ($maxUsage <= $times_used && !$isInfinite) {
            $isActive = 'U';
        }
        CouponAPIClient::modifyCoupon($couponCode, $sdate, $edate,
    $freeShipping, $couponType, $MRPpercentage, $MRPAmount, $maxUsage,
    $isInfinite, $maxUsagePerCart, $maxUsageByUser, $isInfinitePerUser,
    $minAmount, $maxAmount, $isActive, $users,
    $excludeUsers, $comments, $productTypeIds, $excludeProductTypeIds,
    $styleIdString, $excludeStyleIdString, $catStyleIds,
    $excludeCatStyleIds, $categoryIds, $excludeCategoryIds, $time,$lastEditedBy, $description, $showInMyMyntra,$minCount, $maxCount);
      /*  $query =
		    "UPDATE " . $this->tbl_coupons . "  
		    	SET startdate = '$sdate', 
		    		expire = '$edate', freeShipping = '$freeShipping', 
		    		couponType = '$couponType', MRPpercentage = '$MRPpercentage', 
		    		MRPAmount = '$MRPAmount', times = '$maxUsage', 
		    		isInfinite = '$isInfinite', maxUsagePerCart = '$maxUsagePerCart',
		    		maxUsageByUser = '$maxUsageByUser', 
		    		isInfinitePerUser = '$isInfinitePerUser',
		    		minimum = '$minAmount', maxAmount = '$maxAmount', min_count = '$minCount',max_count = '$maxCount',
		    		status = '$isActive',
		    		users = '$users', excludeUsers = '$excludeUsers',
		    		comments = '$comments', productTypeIds = '$productTypeIds',
		    		excludeProductTypeIds = '$excludeProductTypeIds', 
		    		styleIds = '$styleIdString', 
		    		excludeStyleIds = '$excludeStyleIdString', 
		    		catStyleIds = '$catStyleIds', 
		    		excludeCatStyleIds = '$excludeCatStyleIds', 
		    		categoryIds = '$categoryIds', 
		    		excludeCategoryIds = '$excludeCategoryIds',
		    		lastEdited = '$time', lastEditedBy = '$lastEditedBy',
		    		description = '$description', showInMyMyntra = '$showInMyMyntra'
		      WHERE coupon = '$couponCode';";

        db_query($query);
        */
        // Set active_count.
        /* db_query("UPDATE " . $this->tbl_coupon_groups . " 
        			 SET active_count = 
        			 	 (SELECT count(*)
        			 	    FROM " . $this->tbl_coupons . " 
        			 	   WHERE status = 'A'
        			 	     AND groupName = '$groupName')
        		   WHERE groupname = '$groupName';");
        		   */
    }

    /**
     * Delete the coupon. Clear the corresponding usage and reduce the
     * group's count.
     */
    public function deleteCoupon($couponCode, $condition = '')
    {
        // Find the group name.
        /*$result = db_query("SELECT groupName, status
        					  FROM " . $this->tbl_coupons . " 
        					 WHERE coupon = '$couponCode'");
        $row = db_fetch_array($result);
        $groupname = $row['groupName'];
        $status = $row['status'];

        // Delete the coupon.
        db_query("DELETE FROM " . $this->tbl_coupons . "
        		   WHERE coupon = '$couponCode' $condition;");

        // Delete the usage.
        db_query("DELETE FROM " . $this->tbl_coupon_usage . "
        		   WHERE coupon = '$couponCode';");
        */

        CouponAPIClient::deleteCoupon($couponCode);
        /*
        // Reduce the group's count.
        $result = db_query("SELECT count, active_count
        					  FROM " . $this->tbl_coupon_groups . " 
        					 WHERE groupname = '$groupname'");
        $row = db_fetch_array($result);
        $count = $row['count'];
        $count--;
        $active_count = $row['active_count'];
        if ($status == 'A') {
            $active_count--;
        }

        db_query("UPDATE " . $this->tbl_coupon_groups . "
        		     SET count = '$count', active_count = '$active_count'
        		   WHERE groupname = '$groupname';"); 
        */
    }
    
    /**
     * Delete a coupon template.
     */
    public function deleteTemplate($templateName)
    {
        // Remove the template.
        $query = "DELETE FROM " . $this->tbl_coupon_templates . " 
        		   WHERE templateName = '$templateName';";
        
        db_query($query);
        
        // Remove the template references from coupon groups.
        $query = "UPDATE " . $this->tbl_coupon_groups . " 
        		     SET templateName = '' 
        		   WHERE templateName = '$templateName';";
        
        db_query($query);
    }

    /**
     * Change status of a coupon group.
     */
    public function changeCouponGroupStatus($groupname, $status, $login)
    {
        $time = time();
        
        // Group.
        /*db_query("UPDATE " . $this->tbl_coupon_groups . "
        			 SET active = '$status', lastEdited = '$time', lastEditedBy = '$login' 
        		   WHERE groupname = '$groupname';");
        

        // Underlying coupons.
        db_query("UPDATE " . $this->tbl_coupons . "
                     SET status = '$status'
         		   WHERE groupName = '$groupname';");
        */
       CouponAPIClient::changeCouponGroupStatus($groupname,$status,$login,$time);

        /*
        // Set active_count.
        db_query("UPDATE " . $this->tbl_coupon_groups . " 
        			 SET active_count = 
        			 	 (SELECT count(*)
        			 	    FROM " . $this->tbl_coupons . " 
        			 	   WHERE status = 'A'
        			 	     AND groupName = '$groupname')
        		   WHERE groupname = '$groupname';");
        		   */
    }

    /**
     * Change the status of a coupon. [A - active, D - disabled, U - used]
     */
    public function changeCouponStatus($couponCode, $status, $condition, $login)
    {
        // Fetch group name and status.
       /* $result = db_query("SELECT groupName, status
        					  FROM " . $this->tbl_coupons . " 
        					 WHERE coupon = '$couponCode'");
        $row = db_fetch_array($result);
        $groupname = $row['groupName'];
        $prev_status = $row['status'];
        
        // Change coupon status.
        db_query("UPDATE " . $this->tbl_coupons . "
        			 SET status = '$status', lastEditedBy = '$login' 
        		   WHERE coupon = '$couponCode' $condition");
        */
        CouponAPIClient::changeCouponStatus($couponCode,$status,$login);
        
        /*
        // ALERT: Only when the prev status is 'A', should this query be run.
        // Set active_count.
        db_query("UPDATE " . $this->tbl_coupon_groups . " 
        			 SET active_count = 
        			 	 (SELECT count(*)
        			 	    FROM " . $this->tbl_coupons . " 
        			 	   WHERE status = 'A'
        			 	     AND groupName = '$groupname')
        		   WHERE groupname = '$groupname';");
        		   */
    }

    /**
     * Delete a coupon group.
     */
    public function deleteCouponGroup($groupname)
    {
        // Usage data.
        /*db_query("DELETE FROM " . $this->tbl_coupon_usage . "
        		   WHERE coupon 
        		   	  IN (SELECT coupon 
        		   	        FROM " . $this->tbl_coupons . " 
        		   	       WHERE groupName = '$groupname');");

        // Group.
        db_query("DELETE FROM " . $this->tbl_coupon_groups . "
        		   WHERE groupname = '$groupname';");

        // Underlying coupons.
        db_query("DELETE FROM " . $this->tbl_coupons . "
				   WHERE groupName = '$groupname';");
                   */

          CouponAPIClient::deleteCouponGroup($groupname);
    }

    /**
     * @return int Number of times the user has used the coupon.
     */
    public function getCouponUsageForUser($couponCode, $userName)
    {
        /*$query = "SELECT times_used
        		    FROM " . $this->tbl_coupon_usage . " 
                   WHERE login = '$userName' 
                     AND coupon = '$couponCode';";

        $couponUsed = db_query($query);

        // Fetch an associative array.
        $row = array();
        $row = db_fetch_array($couponUsed);*/

        $row = CouponAPIClient::getCouponUsage($couponCode,$userName);

        return ($row == null) ? 0 : $row['times_used'];
    }

    /**
     * @return int Number of times the coupon has been locked for the user.
     */
    public function getCouponLockUsageForUser($couponCode, $userName)
    {
        /*$query = "SELECT times_locked
                    FROM " . $this->tbl_coupon_usage . "  
                   WHERE login = '$userName' 
                     AND coupon = '$couponCode';";

        $couponUsed = db_query($query);

        // Fetch an associative array.
        $row = array();
        $row = db_fetch_array($couponUsed);*/
        $row = CouponAPIClient::getCouponUsage($couponCode,$userName);

        return ($row == null) ? 0 : $row['times_locked'];
    }

    /**
     * Lock the coupon for the user.
     */
    public function lockCouponForUser($couponCode, $userName)
    {
        CouponAPIClient::lockCouponForUser($couponCode, $userName);
        /*// Early exit.
        if ($couponCode == null || empty($couponCode)) {
            return;
        }

        $should_lock_coupons = WidgetKeyValuePairs::getWidgetValueForKey('should.lock.coupons');
        $should_lock_coupons = trim($should_lock_coupons);
        
        if($should_lock_coupons!='t') {
        	return;
        }
        
        $query = "SELECT times_locked
                    FROM " . $this->tbl_coupon_usage . "  
                   WHERE login = '$userName' 
                     AND coupon = '$couponCode';";

        $couponUsed = db_query($query);

        // Fetch an associative array.
        $row = array();
        $row = db_fetch_array($couponUsed);

        if (empty($row)) {
            $result =
            db_query("INSERT INTO " . $this->tbl_coupon_usage . "
                         	(coupon, login, times_locked) 
                          VALUES 
                          	('$couponCode', '$userName', 1);");
        }
        else {
            $locked = $row['times_locked'];
            $locked++;
            $result =
            db_query("UPDATE " . $this->tbl_coupon_usage . "
                             SET times_locked = $locked 
                           WHERE login = '$userName' 
                             AND coupon = '$couponCode';");
        }

        //
        // Update the coupons table.
        //
        $query = "SELECT times_locked
                    FROM " . $this->tbl_coupons . "  
                   WHERE coupon = '$couponCode';";

        $couponUsed = db_query($query);

        // Fetch an associative array.
        $row = array();
        $row = db_fetch_array($couponUsed);

        $locked = $row['times_locked'];
        $locked++;

        $result =
        db_query("UPDATE " . $this->tbl_coupons . "
                         SET times_locked = $locked 
                       WHERE coupon = '$couponCode';");
        */
    }

    public function updateCashbackUsageByUser($couponCode, $login)
    {
	
	    $sql = "update xcart_discount_coupons set times_locked = 0, times_used=0, status='A' where coupon='$couponCode'";
		db_query($sql);
		$sql = "update xcart_discount_coupons_login set times_used=0, times_locked=0 where coupon='$couponCode' AND login='$login'";
		db_query($sql); 	

    }
	
    /**
     * Note a successful use by user.
     *
     * @param string $couponCode
     * @param string $userName
     * @param double $subtotal The cart value prior to the coupon discount
     * @param double $discount The coupon discount on the cart
     */
    public function updateUsageByUser($couponCode, $userName, $subtotal, $discount, $orderid = null)
    {
        //
        // Update coupon usage table.
        //
        CouponAPIClient::updateUsageByUser($couponCode, $userName, $subtotal, $discount, $orderid);
        /*$query = "SELECT times_used, subtotal, discount
                    FROM " . $this->tbl_coupon_usage . "  
                   WHERE login = '$userName' 
                     AND coupon = '$couponCode';";

        $couponUsed = db_query($query);

        // Fetch an associative array.
        $row = array();
        $row = db_fetch_array($couponUsed);

        if (empty($row)) {
            $result = db_query(
                "INSERT INTO " . $this->tbl_coupon_usage . "  
                (coupon, login, times_used, subtotal, discount) 
                VALUES ('$couponCode', '$userName', 1, $subtotal, $discount);");
        }
        else {
            $used = $row['times_used'];
            $st = $row['subtotal'];
            $disc = $row['discount'];

            $used++;
            $st += $subtotal;
            $disc += $discount;

            $result = db_query(
            	"UPDATE " . $this->tbl_coupon_usage . "  
                    SET times_used = $used, subtotal = $st, discount = $disc 
                  WHERE login = '$userName' 
                    AND coupon = '$couponCode';");
        }

        //
        // Update the coupons table.
        //
        $query = "SELECT times_used, subtotal, discountOffered, times, isInfinite, groupName
                    FROM " . $this->tbl_coupons . "  
                   WHERE coupon = '$couponCode';";

        $couponUsed = db_query($query);

        // Fetch an associative array.
        $row = array();
        $row = db_fetch_array($couponUsed);

        // Increment the times_used, subtotal and discount.
        $used = $row['times_used'];
        $used++;
        $stc = $row['subtotal'];
        $stc += $subtotal;
        $discc = $row['discountOffered'];
        $discc += $discount;
        $groupName = $row['groupName'];
        
        // Change the status of the coupon to 'U', if the usage is exhausted.
        $statusUpdate = "";
        if ($row['times'] <= $used && !$row['isInfinite']) {
            $statusUpdate = ", status = 'U'";
        }

        $result =
        db_query("UPDATE " . $this->tbl_coupons . "
                         SET times_used = $used, 
                         	 subtotal = $stc, 
                             discountOffered = $discc " . $statusUpdate . "
                       WHERE coupon = '$couponCode';");
		/*
        // Settle active count.
        db_query("UPDATE " . $this->tbl_coupon_groups . " 
        			 SET active_count = 
        			 	 (SELECT count(*)
        			 	    FROM " . $this->tbl_coupons . " 
        			 	   WHERE status = 'A'
        			 	     AND groupName = '$groupName')
        		   WHERE groupname = '$groupName';");
       */ 
        // Cashback.
        /*if ($groupName == 'CASHBACK' && $discount > 0) {
            $this->debitCashback($userName, $discount, "Usage on order no. $orderid");
        }
        */
    }
    
    /**
     * Decrement the coupon lock count for the user.
     *
     * @param $couponCode
     * @param $user
     */
    public function unlockCouponForUser($couponCode, $userName)
    {
        CouponAPIClient::unlockCouponForUser($couponCode,$userName);
        // Early exit.
        /*if ($couponCode == null || empty($couponCode)) {
            return;
        }

        //
        // Update coupon usage table.
        //
        $query = "SELECT times_locked
                    FROM " . $this->tbl_coupon_usage . "  
                   WHERE login = '$userName' 
                     AND coupon = '$couponCode';";

        $couponUsed = db_query($query);

        // Fetch an associative array.
        $row = array();
        $row = db_fetch_array($couponUsed);

        if (empty($row)) {
            $result =
            db_query("INSERT INTO " . $this->tbl_coupon_usage . "
                          (coupon, login, times_locked) 
                          VALUES ('$couponCode', '$userName', 0);");
        }
        else {
            $locked = $row['times_locked'];
            $locked--;
            $result =
            db_query("UPDATE " . $this->tbl_coupon_usage . "
                             SET times_locked = $locked 
                           WHERE login = '$userName' 
                             AND coupon = '$couponCode';");
        }

        //
        // Update the coupons table.
        //
        $query = "SELECT times_locked
                    FROM " . $this->tbl_coupons . "  
                   WHERE coupon = '$couponCode';";

        $couponUsed = db_query($query);

        // Fetch an associative array.
        $row = array();
        $row = db_fetch_array($couponUsed);

        $locked = $row['times_locked'];
        $locked--;

        $result =
        db_query("UPDATE " . $this->tbl_coupons . "
                         SET times_locked = $locked 
                       WHERE coupon = '$couponCode';");

        */
    }

    /**
     * The function to unlock all the locked coupons. This function should be
     * called by the cron job.
     */
    public function unlockAllCoupons()
    {
        // Update the coupons table.
        /*$query = "UPDATE " . $this->tbl_coupons . "
        			 SET times_locked = 0;";

        db_query($query);

        // Update the coupons usage table.
        $query = "UPDATE " . $this->tbl_coupon_usage . "
        			 SET times_locked = 0;";

        db_query($query);
    */
        CouponAPIClient::unlockAllCoupons();
    }
    
    /**
     * Decrement back a coupon's usage count, when the corresponding order is
     * cancelled (Rejected) from the admin interface.
     */
    public function cancelCouponUsageForOrder($orderid)
    {

        // Fetch the corresponding coupon code.
        $query =
            "SELECT coupon, login, subtotal, coupon_discount 
               FROM " . $this->tbl_orders . "
              WHERE orderid = '$orderid';";
        
        $result = func_query_first($query, TRUE);
        $coupon = $result['coupon'];
        
        // Coupon should not be empty / null.
        if (empty($coupon)) {
            return;
        }
        
        $subtotal = $result['subtotal'];
        $coupon_discount = $result['coupon_discount'];
        
        CouponAPIClient::cancelCouponUsageForOrder($orderid,$subtotal,$result['login'],$coupon_discount,$coupon);


        /*
        // Decrement usage in coupons table. // Also increase expiry my a month
        $expiry = time() + (30*24*60*60);
        $query =
            "UPDATE " . $this->tbl_coupons . "
                SET times_used = times_used - 1,
                    subtotal = subtotal - $subtotal,
                    discountOffered = discountOffered - $coupon_discount,
                    expire = $expiry,
                    status = 'A'
              WHERE coupon = '$coupon';";
        
        db_query($query);
        
        // Decrement usage in coupon usage table.
        $login = $result['login'];
        $query =
            "UPDATE " . $this->tbl_coupon_usage . "
                SET times_used = times_used - 1, 
                    subtotal = subtotal - $subtotal,
                    discount = discount - $coupon_discount
              WHERE coupon = '$coupon'
                AND login = '$login';";
        
        db_query($query);
        
        // Settle active count.
        $result = func_query_first("SELECT groupName FROM " . $this->tbl_coupons . " WHERE coupon = '$coupon';");
        $groupName = $result['groupName'];
        */
        /*
        db_query("UPDATE " . $this->tbl_coupon_groups . " 
        			 SET active_count = 
        			 	 (SELECT count(*)
        			 	    FROM " . $this->tbl_coupons . " 
        			 	   WHERE status = 'A'
        			 	     AND groupName = '$groupName')
        		   WHERE groupname = '$groupName';");*/
    }
    
/**
     * Decrement back a coupon's usage count, when the corresponding order is
     * cancelled (Rejected) from the admin interface.
     */
    public function cancelCouponUsageForItem($coupon, $login, $itemprice, $itemdiscount)
    {
        // Decrement usage in coupons table.
        /*$query =
            "UPDATE " . $this->tbl_coupons . "
                SET subtotal = subtotal - $itemprice,
                    discountOffered = discountOffered - $itemdiscount
              WHERE coupon = '$coupon';";
        
        db_query($query);
        
        // Decrement usage in coupon usage table.
        $query =
            "UPDATE " . $this->tbl_coupon_usage . "
                SET subtotal = subtotal - $itemprice,
                    discount = discount - $itemdiscount
              WHERE coupon = '$coupon'
                AND login = '$login';";
        
        db_query($query);
        */
        CouponAPIClient::cancelCouponUsageForOrder(null,$itemprice,$login,$itemdiscount,$coupon);
    }

    /**
     * Get array of categories for a given product.
     */
    public function getCategoriesForProduct($productId)
    {
        $query = "SELECT categoryid
                    FROM xcart_products_categories 
                   WHERE productid = '$productId';";

        $result = db_query($query, true);

        $categories = array();
        while ($row = db_fetch_array($result)) {
            array_push($categories, $row['categoryid']);
        }

        return $categories;
    }
    
    
	/**
     * Creates a n-times Deactivated usable coupon with a unique coupon code with
     * given specifications.
     */
    public function generateMultipleUsableCouponForUser($couponCodePrefix,
                                                $suffixLength,
                                                $groupname,
                                                $times,
                                                $startDate,
                                                $endDate,
                                                $userName,
                                                $type,
                                                $amount,
                                                $percentage,
                                                $minCartVal,
                                                $description="")
    {
        // Get lock on tables.
        /*db_query("LOCK TABLES "
                 . $this->tbl_coupon_groups . " READ, "
                 . $this->tbl_coupons . " READ;");*/
                 
        // Create group if it does not already exist.
        if (!$this->existsCouponGroup($groupname)) {
            $this->createCouponGroup(
                $groupname, 'online', time(), 'myntraprovider');
        }

        // Get a unique coupon code.
        $couponCode = $couponCodePrefix . $this->getRandomString($suffixLength);
        
        while ($this->existsCoupon($couponCode)) {
            $couponCode =
            $couponCodePrefix . $this->getRandomString($suffixLength);
        }
        
        // Add the coupon.
        $this->addCoupon($couponCode, $groupname, $startDate, $endDate, $times, 0,
            -1, 0, 0, $times, 0, $minCartVal, 0, $userName, '', time(),
            'AutoCreated', $type, $amount, $percentage, 0, 'D', '', '', '', 
            '', '', '', '', '', '', '', '', $description, 0);
        
        // Release locks.
        //db_query("UNLOCK TABLES;");
        
        // Return the unique couponCode.
        return $couponCode;
    }

    /**
     * Creates a one-time usable coupon with a unique coupon code with
     * given specifications.
     */
    public function generateSingleCouponForUser($couponCodePrefix,
                                                $suffixLength,
                                                $groupname,
                                                $startDate,
                                                $endDate,
                                                $userName,
                                                $type,
                                                $amount,
                                                $percentage,
                                                $minCartVal,
                                                $description="")
    {
        // Get lock on tables.
        /*db_query("LOCK TABLES "
                 . $this->tbl_coupon_groups . " READ, "
                 . $this->tbl_coupons . " READ;");*/
                 
        // Create group if it does not already exist.
        if (!$this->existsCouponGroup($groupname)) {
            $this->createCouponGroup(
                $groupname, 'online', time(), 'myntraprovider');
        }

        // Get a unique coupon code.
        $couponCode = $couponCodePrefix . $this->getRandomString($suffixLength);
        
        while ($this->existsCoupon($couponCode)) {
            $couponCode =
            $couponCodePrefix . $this->getRandomString($suffixLength);
        }
        
        // Add the coupon.
        $this->addCoupon($couponCode, $groupname, $startDate, $endDate, 1, 0,
            -1, 0, 0, 1, 0, $minCartVal, 0, $userName, '', time(),
            'AutoCreated', $type, $amount, $percentage, 0, 'A', '', '', '', 
            '', '', '', '', '', '', '', '', $description, 1);
        
        // Release locks.
        //db_query("UNLOCK TABLES;");
        
        // Return the unique couponCode.
        return $couponCode;
    }

    /**
     * Generate a random string of given length.
     */
    private function getRandomString($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $string = '';

        // Append a random character until we reach the desired length.
        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, strlen($characters)-1)];
        }

        return $string;
    }
    
    /**
     * Add a referral mapping.
     * 
     * @return true		If the referred doesn't already exist on the site.
     * 		   false	If the referred already exists on the site.
     */
    public function addNewReferral($referer, $referred)
    {
        $result = func_query_first("SELECT login 
        							  FROM xcart_customers 
        							 WHERE login = '$referred';");
        
        // $referred already exists.
        if (!empty($result)) {
            return false;
        }
				
        $result2 = func_query_first("SELECT dateReferred
										FROM mk_referral_log
										WHERE referer='$referer' 
										AND referred='$referred';");
		
		$time = time();	
		if(empty($result2))	{
			$query = 
            "INSERT INTO mk_referral_log 
            	(referer, referred, dateReferred, invitesSent)
             VALUES
             	('$referer', '$referred', '$time', 1 );";
		} else {
				
			if( $time - $result2['dateReferred'] < 86400 ) {
				return false;
			}
			$query = 
				"UPDATE mk_referral_log 
				SET dateReferred = '$time', invitesSent=invitesSent+1
				WHERE referer='$referer' and referred='$referred'";
		}
    
        db_query($query);
        return true;
    }
    
    /**
     * Referred/non-referred customer joined.
     */
	public function customerJoined($customer, $referer='')
    {
		if(!empty($referer))	{
			$time = time();
			$entry = func_query_first_cell("SELECT * from mk_referral_log 
								WHERE referer='$referer'
								AND referred='$customer';");
				
			if(empty($entry))	{
				db_query("INSERT INTO mk_referral_log 
						(referer,referred,success, dateJoined)
						VALUES
						('$referer','$customer','1','$time');");
			} else {		
			
			db_query("UPDATE mk_referral_log 
						SET success = '1', 
						dateJoined = '$time'
						WHERE referred = '$customer'
						AND referer = '$referer';");		 
			}			 
		}
    }

    
    /**
     * Fetch open invites.
     */
    public function getOpenInvitesForReferer($referer)
    {
        $query =
            "SELECT *
               FROM mk_referral_log 
              WHERE referer = '$referer'
                AND (dateJoined IS NULL
                	OR dateJoined = '0') order by dateReferred desc;";
        
        $result = db_query($query);
        $invites = array();
        while ($row = db_fetch_array($result)) {
            array_push($invites, $row);
        }
        return $invites;
    }
    
    /**
	 * Active invites 
	 */
	public function getActiveInvitesForReferer($referer)
	{	
		$query =	
			"SELECT * 
				FROM mk_referral_log 
				WHERE referer='$referer'
					AND success = 1
					AND dateFirstPurchase is NOT NULL";
		$result = db_query($query);
        $invites = array();
        while ($row = db_fetch_array($result)) {
            array_push($invites, $row);
        }
        return $invites;
				
	}
	
    /**
     * Fetch accepted invites.
     */
    public function getAcceptedInvitesForReferer($referer)
    {
        $query =
            "SELECT *
               FROM mk_referral_log 
              WHERE referer = '$referer'
                AND success = 1 
				AND dateFirstPurchase is NULL;";
        
        $result = db_query($query);
        $invites = array();
        while ($row = db_fetch_array($result)) {
            array_push($invites, $row);
        }
        return $invites;
    }
    
    /**
     * Get the coupons which are tagged to the single user and have the flag 
     * showInMyMyntra set to true.
     */
    public function getCouponsForUserInMyMyntra($login)
    {
        /*$coupons = array();
        //commenting out because now we are not showing cashback coupon in mycoupons section
        //$cashback = $this->getCashbackCoupon($login);

        //if (!empty($cashback)) {
        //    array_push($coupons, $cashback);
        //}

	$time = time();	
        $query = 
            "SELECT * 
               FROM " . $this->tbl_coupons . " 
              WHERE showInMyMyntra = 1 
                AND users = '$login'
                AND groupName != 'CASHBACK'
                AND ( status = 'A' or status = 'U') 
		       ORDER BY expire;";
        
        $result = db_query($query);
        while ($row = db_fetch_array($result)) {
            array_push($coupons, $row);
        }
        return $coupons;
        */
        return CouponAPIClient::getCouponsForUserInMyMyntra($login);
    }

    /**
     * Get the coupons which are tagged to the single user and have the flag 
     * showInMyMyntra set to true and are active.
     */
    public function getActiveCouponsForUserInMyMyntra($login)
    {
        /*$coupons = array();
        $time = time(); 
        $query = 
            "SELECT coupon 
               FROM " . $this->tbl_coupons . " 
              WHERE showInMyMyntra = 1 
                AND users = '$login'
                AND groupName != 'CASHBACK'
                AND status = 'A' 
                AND expire > UNIX_TIMESTAMP(NOW())
               ORDER BY expire
               LIMIT 10";
       
        $result = db_query($query);
        while ($row = db_fetch_array($result)) {
            array_push($coupons, $row['coupon']);
        }
        return $coupons;
        */

        $today = strtotime(date("d F Y", time()));

        $couponsResponse = CouponAPIClient::getCoupons($today,$login);
        return $couponsResponse['coupons'];

    }

    /**
     * Get the coupons which are tagged to the single user and have the flag 
     * showInMyMyntra set to true and are active.
     */
    public function getAllActiveCouponsForUserInMyMyntra($login)
    {
        $coupons = array();
        $time = time(); 
        $query = 
            "SELECT coupon 
               FROM " . $this->tbl_coupons . " 
              WHERE showInMyMyntra = 1 
                AND users = '$login'
                AND groupName != 'CASHBACK'
                AND status = 'A' 
                AND startdate <= unix_timestamp(from_unixtime(unix_timestamp(), '%Y-%m-%d'))
                AND expire >= unix_timestamp(from_unixtime(unix_timestamp(), '%Y-%m-%d'))
               ORDER BY expire";
        
        $result = db_query($query, true);
        while ($row = db_fetch_array($result)) {
            array_push($coupons, $row['coupon']);
        }
        return $coupons;
    }
    
    /**
     * Get the coupons for the cart page.
     */
    public function getCashCoupons($login)
           {
                   if (empty($login)) {
                   return array();
                   }
                   $coupons = array();
                   $query =
                       "SELECT *
                          FROM " . $this->tbl_coupons . " c, ".$this->tbl_coupon_groups." g
                         WHERE c.groupid=g.groupid AND g.grouptype= 'cash' AND c.groupName != 'CASHBACK'
                           AND c.users = '$login';";
                   return func_query($query);

           }
           
   /**
     * @param double $amount
     * @param double $minAmount
     * @param double $maxAmount
     * @return boolean Whether subtotal is within the min/max specified range of the coupon.
     * This is duplicate logic from modules/coupon/Coupon.php, func isAmountAllowed.
     */
	public function isAmountAllowed($amount, $minAmount, $maxAmount){
		if (!empty($minAmount)) {
            if ($minAmount > $amount) {
                return FALSE;
            }
        }
        
        if (!empty($maxAmount) && ($maxAmount != 0.0)) {
            if ($maxAmount < $amount) {
                return FALSE;
            }
        }
        
        return TRUE;
	}           
	
	/**
     * @return boolean Whether the coupon offers a discount at cart level.
     * This is duplicate logic from modules/coupon/Coupon.php, func isCartLevelDiscount.
     */
	public function isCartLevelDiscount($productTypeIds,$excludeProductTypeIds,$styleIds,$excludeStyleIds,
				$catStyleIds,$excludeCatStyleIds,$categoryIds,$excludeCategoryIds,$SKUs,$excludeSKUs)
	{
        if ($productTypeIds == '' &&
            $excludeProductTypeIds == '' &&
            $styleIds == '' &&
            $excludeStyleIds == '' &&
            $catStyleIds == '' &&
            $excludeCatStyleIds == '' &&
            $categoryIds == '' &&
            $excludeCategoryIds == '' &&
            $SKUs == '' &&
            $excludeSKUs == '')
        {
            return TRUE;
        }
        
        return FALSE;
	}
        
    public function getGeneralCoupons(Cart $cart) {
        
        $coupons = array();
        $widgetValue = (string)WidgetKeyValuePairs::getWidgetValueForKey('generalCouponsToBeAutoApplied');
        
        if(strlen($widgetValue) == 0) {
            return $coupons;
        }

        $generalCouponsToBeAutoApplied = array_map('strtolower', explode(",", $widgetValue));
        $couponList = "'" . implode("','",$generalCouponsToBeAutoApplied) . "'";        
        
        $today = strtotime(date("d F Y", time()));

           $query =
               "SELECT *
                  FROM " . $this->tbl_coupons . "
                 WHERE  expire >= $today
                   AND groupName != 'CASHBACK'
                   AND status = 'A'
                   AND coupon in ($couponList)
              ORDER BY expire ASC;";

           	$result = db_query($query);
                
         	while ($row = db_fetch_array($result)) {
         		// remove the coupons which have already been used
         		if(!($row['isInfinite']) && (($row['times_used'] + $row['times_locked']) == $row['maxUsageByUser'])) {
         			continue;
         		}
         		
         		// coupon is applied at cart level	
         		if($this->isCartLevelDiscount($row['productTypeIds'],
        										$row['excludeProductTypeIds'],
        										$row['styleIds'],
										        $row['excludeStyleIds'],
										        $row['catStyleIds'],
										        $row['excludeCatStyleIds'],
										        $row['categoryIds'],
										        $row['excludeCategoryIds'],
										        $row['SKUs'],       
										        $row['excludeSKUs'])){
         			array_push($coupons, $row);
         			continue;	
         		}

                    $coupon = $this->fetchCoupon($row['coupon'],true);
         	    $calculator = CouponDiscountCalculator::getInstance();
         	    if($calculator->isCouponApplicableOnAnyProduct($coupon,$cart)) {
         	    	array_push($coupons, $row);  
         	    }
         	}

           return $coupons;        
    }
	
    public function getCouponsForCartPage($login, $cart)
       {
           if (empty($login)) {
               return array();
           }

           $coupons = array();
           $today = strtotime(date("d F Y", time()));

           $rows = CouponAPIClient::getCoupons($today,$login);

           /*$query =
               "SELECT *
                  FROM " . $this->tbl_coupons . "
                 WHERE  expire >= $today
                   AND  users = '$login'
                   AND groupName != 'CASHBACK'
                   AND status = 'A'
                   AND showInMyMyntra = 1
              ORDER BY expire ASC;";

           	$result = db_query($query);
            */
           	
           	// Feature gate condition to enable Non-discount-products coupon
           	$condNoDiscountCoupon = FeatureGateKeyValuePairs::getBoolean('condNoDiscountCoupon');
           	$styleExclusionFG = FeatureGateKeyValuePairs::getBoolean('styleExclusion');
                
                if(FeatureGateKeyValuePairs::getBoolean('noStyleExclusionForUserCoupon')) {
                    $styleExclusionFG = false;
                }
                
      	        $noStyleExclusionForSomeGroups = FeatureGateKeyValuePairs::getBoolean('noStyleExclusionForSomeGroups');
                
                $cartSubTotalStyleExclusionFilter = $cart->getCouponApplicableItemsSubtotal();
                $cartSubTotalDiscountedItemsFilter = $cart->getNonDiscountedItemsSubtotal();
                $cartSubTotalNoFilter = $cart->getCartSubtotal();

       		/*if ($styleExclusionFG){
       			$cartSubTotal = $cart->getCouponApplicableItemsSubtotal();
			}
			else if($condNoDiscountCoupon) {
				$cartSubTotal = $cart->getNonDiscountedItemsSubtotal();
			}
			else {
				$cartSubTotal = $cart->getCartSubtotal();
			}*/
			
         	while ($row = $rows) {
         		$row['disabled'] = false;
         		// remove the coupons which have already been used
         		if(!($row['isInfinite']) && (($row['times_used'] + $row['times_locked']) == $row['maxUsageByUser'])) {
         			continue;
         		}
                        
                        $styleExclusionForThisCoupon = $styleExclusionFG;
         		
                        if($noStyleExclusionForSomeGroups && 
                                        \Coupon::isCouponGroupExists('noStyleExclusionForSomeGroups', $row['groupName'])) {
                            $styleExclusionForThisCoupon = false;
                        }
                        
                        if ($styleExclusionForThisCoupon){
                            $cartSubTotal = $cartSubTotalStyleExclusionFilter;
			}
			else if($condNoDiscountCoupon) {
				$cartSubTotal = $cartSubTotalDiscountedItemsFilter;
			}
			else {
				$cartSubTotal = $cartSubTotalNoFilter;
			}

                        if(!$this->isAmountAllowed($cartSubTotal, $row['minimum'], $row['maxAmount'])){
                    $row['disabled'] = true;
         		}

         		// coupon is applied at cart level	
         		if($this->isCartLevelDiscount($row['productTypeIds'],
        										$row['excludeProductTypeIds'],
        										$row['styleIds'],
										        $row['excludeStyleIds'],
										        $row['catStyleIds'],
										        $row['excludeCatStyleIds'],
										        $row['categoryIds'],
										        $row['excludeCategoryIds'],
										        $row['SKUs'],       
										        $row['excludeSKUs'])){
         			array_push($coupons, $row);
         			continue;	
         		}

         		$coupon = $this->fetchCoupon($row['coupon'],true);
         	    $calculator = CouponDiscountCalculator::getInstance();
         	    if($calculator->isCouponApplicableOnAnyProduct($coupon,$cart)) {
         	    	array_push($coupons, $row);  
         	    }
         	}

           return $coupons;
       }
    
    /**
     * Fetch the cashback coupon for a user.
     */
    public function getCashbackCoupon($login)
    {
        $query =
            "SELECT * 
               FROM " . $this->tbl_coupons . "
              WHERE groupName = 'CASHBACK'
                AND users = '$login';";
        
        return func_query_first($query);
    }
    /**
     * fetch myntracreditscoupon //todo remove getCashbackCoupon and use this function every where  
     * @param  $login
     * @return array
     */
    public function getMyntraCreditsCoupon($login)
    {
    	if(!empty($login)) {        
       //     	$query =
       //         "SELECT *
       //            FROM " . $this->tbl_coupons . "
       //           WHERE groupName = 'CASHBACK'
       //             AND users = '$login'
		   		// AND MRPAmount > 0 ;";
                   //$cashback=func_query_first($query);
                   $cashback = CouponAPIClient::getMyntraCreditsCoupon($login);
		   if(!empty($cashback))
		   {
                   	$coupons = array();
	                   array_push($coupons, $cashback);
		   }
    	}
    	return $coupons;
    }

    /**
     * Creates a infinitely usable coupon with a unique coupon code with
     * given specifications.
     */
    public function generateCashbackCouponForUser($couponCodePrefix,
                                                $suffixLength,
                                                $groupname,
                                                $startDate,
                                                $endDate,
                                                $userName,
                                                $type,
                                                $amount,
                                                $percentage,
                                                $minCartVal)
    {
        // Create group if it does not already exist.
        if (!$this->existsCouponGroup($groupname)) {
            $this->createCouponGroup(
                $groupname, 'online', time(), 'myntraprovider');
        }

        // Get a unique coupon code.
        $couponCode = $couponCodePrefix . $this->getRandomString($suffixLength);
        
        while ($this->existsCoupon($couponCode)) {
            $couponCode =
            $couponCodePrefix . $this->getRandomString($suffixLength);
        }
        
        // Add the coupon.
        $this->addCoupon($couponCode, $groupname, $startDate, $endDate, 1, 0,
            -1, 0, 0, 1, 0, $minCartVal, 0, $userName, '', time(),
            'AutoCreated', $type, $amount, $percentage, 0, 'A', '', '', '', 
            '', '', '', '', '', '', '', '', '', 1);
        
        // Return the unique couponCode.
        return $couponCode;
    }
    
    /**
     * Increment Cashback coupon balance for a customer. Create a cashback coupon,
     * if the same does not exist already.
     */
    public function creditCashback($login, $amount, $description)
    {
	$handle = Profiler::startTiming("cashback_creditCashback");
       /* $result = func_query_first(
            "SELECT * 
               FROM " . $this->tbl_coupons . " 
              WHERE groupName = 'CASHBACK'
                AND users = '$login';");*/
        $result = CouponAPIClient::getMyntraCreditsCoupon($login);
        $coupon;
        $balance;
            
        if (empty($result)) {
            // Cashback coupon does not exist. Create one.
            $startDate = strtotime('01 May 2011');
            $endDate = strtotime('31 December 2037');
            $balance = $amount;
            $coupon = $this->generateCashbackCouponForUser("CASHBACK", 8, "CASHBACK", $startDate, $endDate, $login, 'absolute', $amount, 0.00, 0.00);
        }
        else if($description==="CREATE_COUPON"){
        	return true; // return if coupon was already created 
        }
        else{
            // Cashback coupon exists.
            $coupon = $result['coupon'];
            $mrpAmount = $result['MRPAmount'];
            $mrpAmount += $amount;
            $balance = $mrpAmount;
            // Set amount.
           /* $query =
                "UPDATE " . $this->tbl_coupons . "
                    SET MRPAmount = '$mrpAmount'
                    WHERE coupon = '$coupon';";


            db_query($query);*/
            CouponAPIClient::modifyCoupon($coupon, null, null,
    null, null, null, $mrpAmount, null,
    null, null, null, null,
    null, null, null, null,
    null, null, null, null,
    null, null, null,
    null, null, null, null,null, null, null,null, null);
        }

        // Log.
        $time = time();
        $description = mysql_real_escape_string($description);
        if($amount == 0.0 && $description == "CREATE_COUPON"){
        	return; // by pass logging for zero amount credits at the time cashcoupon creation
        }
        $query =
	        "INSERT INTO " . $this->tbl_cashback_log . " 
	             (login, coupon, date, txn_type, amount, balance, description)
	         VALUES
	             ('$login', '$coupon', '$time', 'C', '$amount', '$balance', '$description');";

        db_query($query);
	Profiler::endTiming($handle);

        //Returning here to make the testing gifting feature independent of cashback auditing 
		return;
		
        return $coupon;
    }
    
    function creditCashbackWithBreakDown($login,$coupon,$orderID,$itemID,$quantityId,$storeCreditAmt,$earnedCreditAmt,$BusinessProcessID,$description){
    	
    	
    	$result = func_query_first(
    			"SELECT *
    			FROM " . "mk_cashback_account" . "
    			WHERE login = '$login';");
    	 
    	$time = time();
    	$couponAccountID = $result["id"];
    	$availableStoreCredit = $result["store_credit"];
    	$availableEarnedCredit = $result["earned_credit"];
    	
    	
    	$newStoreCredit = $availableStoreCredit + $storeCreditAmt;
    	$newEarnedCredit = $availableEarnedCredit + $earnedCreditAmt;
    	if(empty($result)){
    		$query="INSERT INTO ".$this->tbl_cashback_account."  
    				(login,coupon,store_credit,earned_credit,modified_on) 
    				VALUES 
    		        ('$login','$coupon',$newStoreCredit,$newEarnedCredit,$time);";
    	} 
    	else {
    		$query="UPDATE  ".$this->tbl_cashback_account."
    		SET store_credit=$newStoreCredit , 
    			earned_credit=$newEarnedCredit,
    			modified_on=$time 
    		WHERE id=$couponAccountID ";
    		 
    	}
    	
    	/*
    	 * To check if this is the right way if doing this
    	 */
    	if(empty($result)){
    		db_query($query);
    		$couponAccountID = db_insert_id();
    	}
    	else{
    		db_query($query);
    	}	
		
    	
    	$query =
    	"INSERT INTO  $this->tbl_cashback_transaction_log (
					    	coupon_account_id ,
					    	order_id ,
					    	item_id ,
					    	quantity_id ,
					    	business_process ,
					    	store_credit_inflow ,
					    	store_credit_outflow ,
					    	earned_credit_inflow ,
					    	earned_credit_outflow ,
					    	user ,
					    	modified_on ,
					    	descripion
					    	)
					    	VALUES (
					    	'$couponAccountID', '$orderID' , '$itemID' , $quantityId ,'$BusinessProcessID',$storeCreditAmt ,0.0, $earnedCreditAmt ,0.0,  'automactic'  , $time , '$description'
					    	)";
    	
    	
    	db_query($query); 	
    	
    	
    }
    
    function getCashBackToBeCreditedForOrderid($cashback_gateway_status, $enable_cashback_discounted_products, $login, $total, $orderid) {
    	
    	if($cashback_gateway_status != "off") {
	    	$useGlobalCashback = false;
	    	
	    	if($enable_cashback_discounted_products == "true") {
	    		$useGlobalCashback = true;
	    	}    	
	    	
	    	if($cashback_gateway_status == "internal") {
				$pos = strpos($login, "@myntra.com");
				if($pos == false){
					// update order table with cashback value        		
	        		return 0;
				}		
			}
			
			$cashback = 0.0;
			if($useGlobalCashback)	{
				$cashback = round($total * 0.10);
	        }	
			
	        $item_query = "select itemid, total_amount, coupon_discount_product, discount_rule_id from xcart_order_details where item_status != 'IC' and orderid = ".$orderid;
	        $items_result=func_query($item_query);
	        foreach($items_result as $item) {
	        	if(!$useGlobalCashback && (!empty($item['discount_rule_id']))){
	        		continue;
	        	}
	        	$item_cashback = round(($item['total_amount'] - $item['coupon_discount_product'])*0.10);
	        	if(!$useGlobalCashback)	{
	        		$cashback += $item_cashback;
	        	}        	
	       	}
	       	
	       	if($cashback > 0)	{
	       		return $cashback; 
	       	} 
    	}
    	
       	return 0;            	
    }
    
    function creditCashBackForOrderid($cashback_gateway_status, $useGlobalCashback, $login, $total, $orderid)
    {
       	if($cashback_gateway_status == "internal") {
			$pos = strpos($login, "@myntra.com");
			if($pos == false){
				// update order table with cashback value
        		$update_data = array('cashback_processed' => 1 );
        		func_array2update("orders", $update_data, "orderid = ".$orderid);
        		return;
			}		
		}
		
		$cashback = 0.0;
		if($useGlobalCashback)	{
			$cashback = round($total * 0.10);
        }	
		
        $item_query = "select itemid, total_amount, coupon_discount_product, discount_rule_id from xcart_order_details where item_status != 'IC' and orderid = ".$orderid;
        $items_result=func_query($item_query);
        foreach($items_result as $item) {
        	if(!$useGlobalCashback && (!empty($item['discount_rule_id']))){
        		continue;
        	}
        	$item_cashback = round(($item['total_amount'] - $item['coupon_discount_product'])*0.10);
        	if(!$useGlobalCashback)	{
        		$cashback += $item_cashback;
        	}
        	//update item with cashback value.
        	$update_data = array('cashback'=>$item_cashback);
        	func_array2update("order_details", $update_data, "itemid = ".$item['itemid']);
       	}
       	
       	if($cashback > 0)	{
       		$this->creditCashback($login, $cashback, 'Cashback on order no. '.$orderid);
       	}
       	
       	// update order table with cashback value
        $update_data = array('cashback'=>$cashback,'cashback_processed' => 1 );
        func_array2update("orders", $update_data, "orderid = ".$orderid);
    }
    
    /**
     * CREDIT CASH BACK FOR ALL ORDERS GOT COMPLETED IN LAST $time_buffer
     */
    function creditCashBackForOrders($cashback_gateway_status,$enable_cashback_discounted_products)
    {
		$time = time();
		$timeminus30days = $time - 30 * 24 * 60 * 60;
    	
		$useGlobalCashback = false;
    	if($enable_cashback_discounted_products == "true") {
    		$useGlobalCashback = true;
    	}
        $order_query="select login,(total + cash_redeemed) as total,orderid from xcart_orders where payment_method = 'cod' and date < $timeminus30days and cashback_processed=0 and status in ('DL', 'C')";
   		$order_result=func_query($order_query, true);
   		foreach ($order_result as $order) {
			$this->creditCashBackForOrderid($cashback_gateway_status,$useGlobalCashback, $order['login'], $order['total'], $order['orderid']);
        }
    }
    
    /**
     * Decrement the Cashback coupon balance for the customer.
     */
    public function debitCashback($login, $amount, $description, $timeStamp = false)
    {
	$handle = Profiler::startTiming("cashback_debitCashback");
       /* $result = func_query_first(
            "SELECT * 
               FROM " . $this->tbl_coupons . " 
              WHERE groupName = 'CASHBACK'
                AND users = '$login';");
                */

        $result = CouponAPIClient::getMyntraCreditsCoupon($login);

        
        $coupon = $result['coupon'];
        $mrpAmount = $result['MRPAmount'];
        $mrpAmount -= $amount;
        $mrpAmount = ($mrpAmount < 0.00) ? 0.00 : $mrpAmount;
        $balance = $mrpAmount;
        
        // Set amount.
        /*$query =
            "UPDATE " . $this->tbl_coupons . " 
                SET MRPAmount = '$mrpAmount'
                WHERE coupon = '$coupon';";
                


        db_query($query);*/
        CouponAPIClient::modifyCoupon($coupon, null, null,
    null, null, null, $mrpAmount, null,
    null, null, null, null,
    null, null, null, null,
    null, null, null, null,
    null, null, null,
    null, null, null, null,null, null, null,null, null);
        
        // Log.
        //shifting the time by 2 secs for correctly displaying summary when payment_method != 'cod'
        if(!$timeStamp){
        	$timeStamp = time() - 2 ;
        }  
        $description = mysql_real_escape_string($description);
        $query =
            "INSERT INTO " . $this->tbl_cashback_log . " 
                 (login, coupon, date, txn_type, amount, balance, description)
             VALUES
                 ('$login', '$coupon', '$timeStamp', 'D', '$amount', '$balance', '$description');";

        db_query($query);
	Profiler::endTiming($handle);
        
    }
    
    
    
    public function debitCashbackWithBreakDown($login,$orderID,$itemID,$quantityId,$amount,$BusinessProcessID,$description){
    	 
    	$result = func_query_first(
    			"SELECT *
    			FROM $this->tbl_cashback_account  WHERE login = '$login';");
    	
    	
    	 
    	
    	 
    	$couponAccountID = $result["id"];
    	$availableStoreCredit = $result["store_credit"];
    	$availableEarnedCredit = $result["earned_credit"];
    	
    	if($amount <= $availableStoreCredit){
    		$storeCreditAmt = $amount;
    		$earnedCreditAmt = 0.0;
    	}else {
    		$storeCreditAmt = $availableStoreCredit;
    		$earnedCreditAmt = ($amount-$availableStoreCredit);
    		$earnedCreditAmt = ($earnedCreditAmt <$availableEarnedCredit) ? $earnedCreditAmt: $availableEarnedCredit;
    	}
    	
    	$time = time() - 2 ;
    	$query = "UPDATE $this->tbl_cashback_account 
    				SET store_credit = ".($availableStoreCredit - $storeCreditAmt). ",
					earned_credit  = ".($availableEarnedCredit - $earnedCreditAmt). ",
					modified_on = ".$time."   
					WHERE id=".$couponAccountID.";";
    	 
    	
    	db_query($query);
    	$description = mysql_real_escape_string($description);
    	
    	 
    	    	 
    	$query =
    	"INSERT INTO  mk_cashback_transacion_log (
	    	coupon_account_id ,
	    	order_id ,
	    	item_id ,
	    	quantity_id ,
	    	business_process ,
	    	store_credit_inflow ,
	    	store_credit_outflow ,
	    	earned_credit_inflow ,
	    	earned_credit_outflow ,
	    	user ,
	    	modified_on ,
	    	descripion
    	)
    	VALUES (
    	'$couponAccountID', '$orderID' , '$itemID' , $quantityId ,'$BusinessProcessID',0.0, $storeCreditAmt ,0.0, $earnedCreditAmt , 'automactic'  , $time , '$description'
    	)";
    	 
    	 
    	db_query($query);
    	 
    	 
    	 
    }
    /**
     * Fetch cashback transactions for a user.
     */
    public function getCashbackTransactions($login)
    {
        $query =
            "SELECT * 
               FROM " . $this->tbl_cashback_log . " 
              WHERE login = '$login' ORDER BY date desc;";
        
        $result = db_query($query);
        
        $transactions = array();
        while ($row = db_fetch_array($result)) {
            array_push($transactions, $row);
        }
        return $transactions;
    }
    
	public function getCashbackTransactionsForUser($login, $start, $limit)
    {
	$handle = Profiler::startTiming("cashback_getCashbackTransactionsForUser");
        $query =
            "SELECT SQL_CALC_FOUND_ROWS * 
               FROM " . $this->tbl_cashback_log . " 
              WHERE  login = '$login' ORDER BY date desc";
        
        $query .=" LIMIT $start, $limit";
        
        $transactions = func_query($query, TRUE);
       	$total = func_query("SELECT FOUND_ROWS() total", TRUE);
        
       	foreach($transactions as $index=>$txn) {
       		$transactions[$index]['date'] = date("d-m-Y H:i:s", $txn['date']);
       	}
       	
	Profiler::endTiming($handle);
       	return array("results" => $transactions, "count" => $total[0]["total"]);
    }
    
	public function debitCashbackForUser($login, $amount, $description)
    {
        /*$result = func_query_first(
        	"SELECT * FROM " . $this->tbl_coupons . " WHERE groupName = 'CASHBACK' AND users = '$login'");
            */
    
        $result = CouponAPIClient::getMyntraCreditsCoupon($login);
        $coupon = $result['coupon'];
        $couponAmount = $result['MRPAmount'];
        
        if($couponAmount <= 0.00) {
        	return $amount;
        }
        
        $balance_amount = 0.00;
        if($couponAmount > $amount) {
        	$couponAmount -= $amount; 
        } else {
        	$balance_amount = $amount - $couponAmount;
        	$couponAmount = 0.00;
        }
        
        // Set amount.
        /*$query =
            "UPDATE " . $this->tbl_coupons . " 
                SET MRPAmount = '$couponAmount'
              WHERE coupon = '$coupon'";

        db_query($query);
        */
        CouponAPIClient::modifyCoupon($coupon, null, null,
    null, null, null, $couponAmount, null,
    null, null, null, null,
    null, null, null, null,
    null, null, null, null,
    null, null, null,
    null, null, null, null,null, null, null,null, null);
        
        // Log.
        $time = time();
        $description = mysql_real_escape_string($description);
        $query =
            "INSERT INTO " . $this->tbl_cashback_log . " 
                 (login, coupon, date, txn_type, amount, balance, description)
             VALUES
                 ('$login', '$coupon', '$time', 'D', '$amount', '$couponAmount', '$description');";

        db_query($query);
        
        return $balance_amount;
    }
    
    public function getTotalCouponWorth($login) {
		$time = strtotime(date("d F Y", time()));;
		//$query = "SELECT * from xcart_discount_coupons where groupName <> 'CASHBACK' and showInMyMyntra='1' and users='$login' and status='A' and expire >= '$time'";
		//$result = db_query($query);
        $rows = CouponAPIClient::getCoupons($today,$login);
		$totalCouponWorth = 0;
		$activeCoupons = false;
		
		while ($row = $rows) {
			//if the coupon is active, use its value
			//check for whether coupon is active, taken from mymyntra.php
			
			if(($row['isInfinite']) && ($row['times_used'] <= $row['maxUsageByUser']) || ($row['isInfinite'] ||(($row['times_used'] + $row['times_locked']) < $row['maxUsageByUser'])))	{
				$type = $row['couponType'];
				$amount = $row['MRPAmount'];
				$perc	= $row['MRPpercentage'];
			
				if($type == 'absolute')	{
					$totalCouponWorth += $amount;
				} else if($type == 'percentage')	{
					$totalCouponWorth += $perc * 10; // assuming the value of product is 1000Rs.
				} else {
					$totalCouponWorth += $amount;
				}
				$activeCoupons = true;
			}
		}
		// read the feature gate value for cashback
		$cashback_gateway_status = FeatureGateKeyValuePairs::getFeatureGateValueForKey('cashbackenabled');
		if($cashback_gateway_status == "internal") {
			$pos = strpos($login, "@myntra.com");
			if($pos === false)
			$cashback_gateway_status = "off";
			else
			$cashback_gateway_status = "on";
		}
		$totalCouponWorth = intval($totalCouponWorth);
		return $totalCouponWorth;
    }
    
    public function getMyntraCredits($login) {
		$profileHandler = Profiler::startTiming("cashback_getMyntraCredits");
		$cashback_gateway_status = FeatureGateKeyValuePairs::getFeatureGateValueForKey('cashbackenabled');
    	if($cashback_gateway_status == "on")	{
			//$myntraCredits = func_query_first_cell("SELECT MRPAmount from xcart_discount_coupons where groupName='CASHBACK' and users='$login'");
			$result = CouponAPIClient::getMyntraCreditsCoupon($login);
            $myntraCredits = intval($result['MRPAmount']);
			Profiler::endTiming($profileHandler);
			return $myntraCredits;
		} else {
			return 0;
		}
    }

	public function getHeaderMessage($login)
	{	
        // Amit Rana(2013-10-22) : This data is not used anywhere now. Removing it to avoid unnecessary computation.
		if (true || empty($login))	{
			return "";
		}
		
		$totalCouponWorth = $this->getTotalCouponWorth($login);
		
		global $abHeaderUi;
		if($abHeaderUi == 'v3'){
            $couponString = "<span style=\"\"><b class=\"tool-tip-title\">My Coupons</b>Rs. ".$totalCouponWorth."</span>";
        }else{
            $couponString = "<span style=\"\">My Coupons&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp; <a style=\"\" href=\"$http_loc/mymyntra.php?view=mymyntcredits\" onclick=\"_gaq.push(['_trackEvent', 'header', 'view_mynt_credits', 'view_mynt_credits']);\">Rs. ".$totalCouponWorth."</a></span>";
        }
        
        $myntraCredits = $this->getMyntraCredits($login);
        $totalCredits = $totalCouponWorth + $myntraCredits;
	
		$cashbackString = "";
		$cashback_gateway_status = FeatureGateKeyValuePairs::getFeatureGateValueForKey('cashbackenabled');
		if($cashback_gateway_status == "on")	{
            if($abHeaderUi == 'v3'){
                $cashbackString = "<span class=\"\"><b class=\"tool-tip-title\">Cashback Account</b> Rs. ".$myntraCredits."</span>";
            }else{
                $cashbackString = "<span class=\"\">Cashback Account&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp; <a style=\"display:inline;\" href=\"$http_loc/mymyntra.php?view=mymyntcredits\" onclick=\"_gaq.push(['_trackEvent', 'header', 'view_mynt_credits', 'view_mynt_credits']);\">Rs. ".$myntraCredits."</a></span>";
            }
		}
        $this->couponMyntraCreditAmount = $myntraCredits;
        if($abHeaderUi == 'v3'){
            $totalString = "<span style='color:#666;'>(<b style='font-weight:normal' class='popup-trigger'>Rs. ".$totalCredits."</b>)</span>";
            return "<span>".$totalString."</span><div class='myntra-tooltip tooltip-myntcre'><div class='cre-val'><span class='tip-title'><i class='tool-tip-title'>Total Mynt Credits</i> Rs. ".$totalCredits."</span>".$couponString.$cashbackString."</div></div>";
        }else{
            $totalString = "<span class=\"left\"><b>Mynt Credits: <a href=\"$http_loc/mymyntra.php?view=mymyntcredits\" onclick=\"_gaq.push(['_trackEvent', 'header', 'view_mynt_credits','view_mynt_credits']);\">Rs. ".$totalCredits."</a></b><a href='javascript:void(0);' class='popup-trigger'> What's this? &nbsp;&nbsp;</a></span>";
            return "<h4>".$totalString."</h4><span class='span-pop-up hide'><h4><b>Total Mynt Credits : Rs.".$totalCredits."</b><span class='close-button'>x</span></h4>".$couponString."<br>".$cashbackString."</span>";
        }
		/*
		if(!$activeCoupons && ($myntraCredits == 0))	{	
			return "<b><a href=\"$http_loc/mymyntra.php?view=myreferrals\">Mynt Credits: 0</a></b>";
		} 
		
		if(!$activeCoupons && ($myntraCredits != 0))	{
			return " <b><a href=\"$http_loc/mymyntra.php?view=myreferrals\">Mynt Credits: $myntraCredits</a></b>";
		}
	
		if($activeCoupons && ($myntraCredits == 0))	{
			return "<b><a href=\"$http_loc/mymyntra.php?view=mycredits\">Mynt Credits: $totalCouponWorth</a></b>";
		}
		if($activeCoupons && ($myntraCredits != 0))	{
			return "<b><a href=\"$http_loc/mymyntra.php?view=mycredits\">Mynt Credits: $totalCouponWorth + $myntraCredits</a></b>";
		} */
	}

	public function getTotalCredits($login)
	{	
		$myntCredits = array();
		$myntCredits['couponsCount'] = 0;
		$myntCredits['cashback'] = 0;
		if (empty($login))	{
			return $myntCredits;
		}
		$myntCredits['couponsCount'] = $this->getActiveCouponsCount($login);
        //$myntCredits['cashback'] = $this->getMyntraCredits($login);
        return $myntCredits;
	}

	public function getMyMyntraCredit(){
		return $this->couponMyntraCreditAmount;
	}


    public function generateRefundCouponForCancellation($couponCode,$userName,$couponDiscount,
                                                        $minCartVal) {
        return CouponAPIClient::generateRefundCouponForCancellation($couponCode,$userName,$couponDiscount,$minCartVal);
    }

	public function generateCouponForCancellation($couponCode,$couponCodePrefix,$suffixLength,$groupname,$startDate,$endDate,$userName,$type,
														$amount,$percentage,$minCartVal,$description="",$fromItem){
		$coupon = "";
		global $weblog;
		if(!empty($couponCode)) {
			$origCoupon = $this->fetchCoupon($couponCode);
			if(!empty($origCoupon)){
				$groupname = $origCoupon->getGroupName();
				if(FeatureGateKeyValuePairs::getBoolean("myntrashoppingfestCoupons") && $groupname == WidgetKeyValuePairs::getWidgetValueForKey("myntraFestCouponGroupName")){
					$endDate = time()+(7*24*60*60); // changing the expirey date to 7 days
				}
				$description = "Issued as part of order cancellation as replacement of coupon ".$couponCode;
				if($origCoupon->getType() == 'absolute'){
					$minCartVal = ($amount*$origCoupon->getMinimum() ) / $origCoupon->getAbsoluteDiscount();
				}	
			}
		}
		
		$minCartVal = round($minCartVal);
		$amount = round($amount);
		$coupon = $this->generateSingleCouponForUser($couponCodePrefix,$suffixLength,$groupname,$startDate,$endDate,$userName,$type,$type=='percentage' ?0.0:$amount,$percentage,$minCartVal,$description);
		
		$descriptionText = "";
		
		if(strtolower($type) == "absolute"){
			$descriptionText = " $coupon : Rs $amount off with minimum purchase of Rs $minCartVal with expiry ".date("F jS Y", $endDate);
		}else if (strtolower($type) == "percentage"){
			$descriptionText = " $coupon : $percentage % off with minimum purchase of Rs $minCartVal with expiry ".date("F jS Y", $endDate);
		}else if (strtolower($type) == "dual"){
			$descriptionText = " $coupon : $percentage % off upto Rs $amount  off with minimum purchase of Rs $minCartVal with expiry ".date("F jS Y", $endDate);
		}
		
		return array('coupon'=>$coupon, 'minCartVal'=>$minCartVal,
						'description' => $descriptionText );
	}
	
	public function getActiveCouponsCount($login) {
		if(empty($login)) {
			return 0;
		}
		$today = strtotime(date("d F Y", time()));
		$result = CouponAPIClient::getActiveCouponsCount($login,$today);
		/*$query = "SELECT SUM(CASE WHEN isInfinite = 1 THEN 1 WHEN isInfinite = 0 AND times_used + times_locked < maxUsageByUser THEN 1 ELSE 0 END) AS count
					FROM " . $this->tbl_coupons . "
				   WHERE expire >= $today
					 AND showInMyMyntra = 1
					 AND users = '$login'
					 AND groupName != 'CASHBACK'
					 AND status = 'A';";
		$result = func_query_first($query);
        */
		return !$result['activeCount'] || $result['activeCount'] == '' ? 0 : $result['activeCount'];
	}
	
	public function getExpiringSoonCoupons($login) {
		$expiring = array();
		$expiring['count'] = 0;
		if(empty($login)) {
			return $expiring;
		}
		
		$today = strtotime(date("d F Y", time()));
		
		$query = "SELECT SUM(CASE WHEN isInfinite = 1 THEN 1 WHEN isInfinite = 0 AND times_used + times_locked < maxUsageByUser THEN 1 ELSE 0 END) AS count,
						 CEILING(expire/86400) - CEILING(UNIX_TIMESTAMP(NOW())/86400) + 1 as expdays
					FROM " . $this->tbl_coupons . " t1,
					  (SELECT MIN(expire) AS expiry
						FROM " . $this->tbl_coupons . "
						WHERE NOT(isInfinite = 0 && times_used + times_locked >= maxUsageByUser)
						AND expire >= $today
						AND showInMyMyntra = 1
						AND users = '$login'
						AND groupName != 'CASHBACK'
						AND status = 'A') t2
					WHERE t1.expire >= $today
					AND t1.expire BETWEEN unix_timestamp(from_unixtime(t2.expiry,'%Y-%m-%d')) AND (unix_timestamp(from_unixtime(t2.expiry,'%Y-%m-%d')) + 86400)
					AND t1.showInMyMyntra = 1
					AND t1.users = '$login'
					AND t1.groupName != 'CASHBACK'
					AND t1.status = 'A'";
		$expiring = func_query_first($query);
		return $expiring;
	}
	
    public function getReferralsCount($referer)
    {
        $query =
            "SELECT SUM(CASE WHEN success = '0' THEN 1 ELSE 0 END) AS open,
            		SUM(CASE WHEN success = 1 AND dateFirstPurchase IS NULL THEN 1 ELSE 0 END) AS registered,
            		SUM(CASE WHEN success = 1 AND dateFirstPurchase IS NOT NULL THEN 1 ELSE 0 END) AS converted
               FROM mk_referral_log 
              WHERE referer = '$referer';";
        $result = func_query_first($query);
        return $result;
    }
    
    /*
     * extends the expiry date for the given number of days
     * if $couponCode == null, applies on whole $couponGroup
     * if $couponGroup == null, applies on a single $coupon
     * if $couponGroup,$couponCode != null, applies on a couponcode within the given coupon group
     * 
     */
    
    
    public function extendExpiryForCoupons($login,$couponCode,$noOfdays,$couponGroup=null){

        CouponAPIClient::extendExpiryForCoupons($login,$couponCode,$noOfdays*86400,$couponGroup);
    	/*if($noOfdays==0) return true;
    	if($couponGroup==null && $couponCode !=null){
    		
    		$query = "SELECT expire FROM $this->tbl_coupons WHERE coupon='$couponCode' and users='$login'";
    		
    		$result = func_query_first($query);
    		
    		if(empty($result)){
    			return false;
    		}
    		$extendedSeconds =$result["expire"]+ $noOfdays*86400;
    		
    		db_query("update $this->tbl_coupons 
    				  set expire =$extendedSeconds
    				  where status='A' and coupon = '$couponCode' and users='$login'");
    		
    		return true;   		
    	}
    	else if($couponGroup!=null && $couponCode==null){
    		
    		$query = "SELECT coupon,expire FROM $this->tbl_coupons WHERE groupName='$couponGroup' and users='$login'";
    		
    		$result = func_query($query);
    			
    		if(empty($result)){
    			return false;
    		}
    		
    		foreach ($result as $row){
    			$extendedSeconds =$row["expire"]+ $noOfdays*86400;
	    		db_query("update $this->tbl_coupons
	    				set expire =$extendedSeconds 
	    				where status='A' and coupon='".$row["coupon"]."' and groupName='$couponGroup' and users='$login'");    		
	    				
    		}
    		
    		return true;
    	}
    	else if ($couponCode!=null && $couponGroup!=null){
    		$query = "SELECT expire FROM $this->tbl_coupons WHERE coupon='$couponCode' and users='$login' and groupName='$couponGroup'";
    		
    		$result = func_query($query);
    			
    		if(empty($result)){
    			return false;
    		}
    		 		
    		foreach ($result as $row){
    			$extendedSeconds = $row["expire"]+$noOfdays*86400;
    			db_query("update $this->tbl_coupons
    					set expire =$extendedSeconds
    					where status='A' and coupon = '$couponCode'  and users='$login' and groupName='$couponGroup'");
    		}
    		return true;
    		
    		
    	}
        */
    	
    }
    
}
?>
