<?php
/**
 * Created by PhpStorm.
 * Author: Arun Kumar K
 * Date: 19 Apr, 2011
 * Time: 11:30:13 AM
 * purpose:to provide all abstract functionalities for SEM(Search Engine Marketing)
 */
include_once($xcart_dir."/include/func/func.db.php");
class Sem{
    //sem promo members
    public $promoIdentifier;
    private $promoContent;
    private $promoUrl;
    private $promoCreatedTime;
    private $semCookieName;
    private $semCookieData;
    private static $semCookiePath = '/';
    private static $semCookieDomain;//can be set for domain/ subdomain separately
    private $semCookieExpireTime = 0;//till end of session(close browser)
    const SEM_PROMO_COOKIE_NAME = 'SEMP';//cookie name for sem promo(if need to change, change here)
    const SEM_PROMO_SESSION_COOKIE_NAME = 'SEMP_SESSION';

    
    function __construct(){}

    /* adds SEM promo info
     * @param:(array)$queryData - array of promo to be inserted
     * @return:(int)last insert id/(bool)false
     */
    public function addPromo($queryData){
        return func_array2insert('mk_sem_promo',sanitizeDBValues($queryData));
    }

    /* updates SEM promo info based on condition
     * @param:(array)$queryData - array of promo to be update
     * @return:void
     */
    public function updatePromo($queryData,$condition){
        func_array2update('mk_sem_promo', sanitizeDBValues($queryData),$condition);
    }

    public function deletePromo($id){        
        db_query("delete from mk_sem_promo where id = '".sanitizeDBValues($id)."'");
    }

    /* get SEM promo info based on condition(filter)
     * @param:(array)$filter - array of filters
     * @param:(array)$order - array for order by
     * @param:(array)$offset - start point of result set
     * @param:(array)$limit - count of records
     * @return:(array)$SEMPromoInfo
     */
    public function getPromo($filter=NULL,$order=NULL,$offset=NULL,$limit=NULL){
        $sql = "select id,identifier,promo_content,promo_url,from_unixtime(promo_created_date,'%b %D, %Y') as created_date from mk_sem_promo";
        $promo = sanitizedQueryBuilder($sql, $filter, $order, $offset, $limit);

        if(is_array($promo) && count($promo) == 1){//both conditions are needed since count(false)==1
            $this->promoIdentifier = $promo[0]['idenitfier'];
        }
        return $promo;
    }    

    /*
     * @return:(string) promo identifier
     */
    public function getPromoIdentifier(){
        return $this->promoIdentifier;
    }

    /* sets cookie for set duration for the domain
     * @param:(array)$cookieData
     * @param:(string)$domain
     * @return:void
     */
    public function setSEMCookie($cookieName,$cookieData,$cookieExpire=null,$domain=null){
        //get the domain from parameter or else from global variable set in config.php
        self::$semCookieDomain = ($domain != null)? $domain : $GLOBALS['xcart_http_host'] ;
        $this->semCookieName = $cookieName;
        $this->semCookieData = $cookieData;
        $this->semCookieExpireTime = ($cookieExpire != null)? $cookieExpire : $this->semCookieExpireTime;        
        setMyntraCookie($this->semCookieName,serialize($this->semCookieData),$this->semCookieExpireTime,self::$semCookiePath);
    }

    /*
     * @return:cookie value
     */
    public function getSEMCookie($cookieName=null){
        if($cookieName == null){
            return array(
                        "name"=> $this->semCookieName,
                        "content"=> unserialize(getMyntraCookie($this->semCookieName))
                    );
        } else {
            return array(
                        "name"=> $cookieName,
                        "content"=> unserialize(getMyntraCookie($cookieName))                       
                    );
        }
    }

    /* adds SEM routing info
     * @param:(array)$queryData - array  to be inserted
     * @return:(int)last insert id/(bool)false
     */
    public function addRoutingInfo($queryData){
        return func_array2insert('mk_sem_routing',sanitizeDBValues($queryData));
    }

    /* updates SEM routing info based on condition
     * @param:(array)$queryData - array to update
     * @return:void
     */
    public function updateRoutingInfo($queryData,$condition){
        func_array2update('mk_sem_routing', sanitizeDBValues($queryData),$condition);
    }

    public function deleteRoutingInfo($id){
        db_query("delete from mk_sem_routing where id = '".sanitizeDBValues($id)."'");
    }

    /* get SEM routing info based on condition(filter)
     * @param:(array)$filter - array of filters
     * @param:(array)$order - array for order by
     * @param:(array)$offset - start point of result set
     * @param:(array)$limit - count of records
     * @return:(array)$SEMroutingInfo
     */
    public function getRoutingInfo($filter=NULL,$order=NULL,$offset=NULL,$limit=NULL){
        $sql = "select id,url,landing_url,from_unixtime(created_date,'%b %D, %Y') as created_date,is_active from mk_sem_routing";
        $routingInfo = sanitizedQueryBuilder($sql, $filter, $order, $offset, $limit);
        return $routingInfo;
    }

}