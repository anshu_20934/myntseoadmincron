<?php

require_once("$xcart_dir/lib/php-amqplib/amqp.inc");
require_once "$xcart_dir/utils/http/SimpleParser.php";
require_once("$xcart_dir/env/Rabbit.config.php");
require_once("$xcart_dir/Profiler/Profiler.php");

class AMQPMessageProducer{
	
	public static $NO_OF_RETRIES = 3;
	public static $exchangeName = '';
	
	private $amqpConn = NULL;
    private $channel = NULL;
    private $logger;
    
    private $persistIfUnsuccessful = true;
    private static $instance;
    
    public static function getInstance(){
		if(!self::$instance){
			self::$instance = new AMQPMessageProducer();
		}
		return self::$instance;    	
    }
    
	private function __construct() {
		global $weblog;
		$this->logger = $weblog; 
	}
	
	/**
	 * this method persists the data into db for later retires 
	 * 
	 * @param unknown_type $queueName
	 * @param unknown_type $data
	 * @param unknown_type $props
	 * @param unknown_type $format
	 */	
	private function saveToDB($queueName,$data,$props,$format,$rootElement = ''){
		$json_props = null;
		if($props){
			$json_props = json_encode($props);
		}
		func_query("insert into mk_queue_retry_data(queue_name,format,props,data,status,root_element) values ('$queueName','$format','$json_props','$data','CREATED','$rootElement')");
	}
	
	/**
     * 
     * publish data to queue
     * @param unknown_type $data
     */
    private function publishMessage($routingKey,$data,$format,$props='',$exchangeName='',$rootEle,$headers = null){
    	$handle = Profiler::startTiming("AMQPMessageProducer-publishMessage");
    	if($exchangeName == null){
    		$exchangeName = self::$exchangeName;
    	}
        $handle = Profiler::startTiming("AMQPMessageProducer-obtainConnection");
    	if(!$this->amqpConn){
    		foreach(RabbitConfig::$conn as $connection){
    			for($i=0;$i<self::$NO_OF_RETRIES;$i++){
			    	try{
						$this->amqpConn = new AMQPConnection($connection["host"], $connection["port"], $connection['user'], $connection['password']);
						$this->channel = $this->amqpConn->channel();
						break;
					}catch (Exception $ex){
						$this->logger->error($ex->getMessage());
					}    		
		    	}	
    		}
    	}
        Profiler::endTiming($handle);
		if(!$this->amqpConn && $this->persistIfUnsuccessful){
			$this->saveToDB($routingKey, $data, $props, $format);
			return true;
		}    	
        $handle = Profiler::startTiming("AMQPMessageProducer-publishMessage");
    	try {
			//$props = array_merge($props,$headers);
    		$msg = new AMQPMessage($data,$props);
			$this->channel->basic_publish($msg,$exchangeName, $routingKey,true);
			$this->logger->info("message sent successfully");
    	} catch (Exception $e) {
    		if($this->persistIfUnsuccessful){
    			$this->saveToDB($routingKey, $data, $props, $format);
    		}
    		$this->logger->error("message sending failed with exception :::::: ".$e->getMessage());
    	}
    	Profiler::endTiming($handle);
    	return true;
    }
    
    
    public function __destruct(){
    	try{
    		if($this->channel)
    			$this->channel->close();
    		if($this->amqpConn)
				$this->amqpConn->close();
    	}catch (Exception $ex){
    		//$this->logger->info($ex->getMessage());
    	}
    }
    
    
	/**
	 * send data to queue in the specified format
	 * 
	 * @param String $queueName 
	 * @param $data
	 * @param String $rootEle root element for xml/json data
	 * @param String $format values: xml/json/serial/string
	 * @param boolean $persistIfUnsuccessful app will try to push message 3 times, up on failure 
	 */
    public function sendMessage($queueName,$data,$rootEle,$format = 'xml',$persistIfUnsuccessful=true,$exchangeName='',$headers=NULL) {
    	$this->persistIfUnsuccessful = $persistIfUnsuccessful;
    	switch ($format) {
    		case 'xml': $this->sendMessageAsXml($queueName,$data, $rootEle,$exchangeName,$headers);
    		break;
    		case 'json':$this->sendMessageAsJson($queueName,$data,$exchangeName,$headers);
    		break;
    		case 'serial':$this->serializeAndSendMessage($queueName,$data,$exchangeName,$headers);
    		break;
    		case 'string':$this->sendStringMessage($queueName,$data,$exchangeName,$headers);
    		break;
    	}
    }
    
	private function sendMessageAsXml($queueName,$data,$rootEle,$exchangeName='',$headers=NULL) {
    	$xmlString = \SimpleParser::arrayToXml($data, $rootEle);
    	$props = array("content_type" => "text/xml","content_encoding" => "UTF-8");
        return $this->publishMessage($queueName,$xmlString,'xml',$props,$exchangeName,$rootEle,$headers);
    }
    
    private function sendMessageAsJson($queueName,$data,$exchangeName='',$headers=NULL){
    	$jsonData = json_encode($data,true);
    	$props = array("content_type" => "text/json","content_encoding" => "UTF-8");
    	return $this->publishMessage($queueName,$jsonData,'json',$props,$exchangeName,'',$headers);
    }
    
    private function serializeAndSendMessage($queueName,$data,$exchangeName='',$headers=NULL){
    	$serializedData = serialize($queueName,$data);
    	return $this->publishMessage($queueName,$serializedData,'serial','',$exchangeName,'',$headers);
    }

    private function sendStringMessage($queueName,$data,$exchangeName='',$headers=NULL){
    	if(is_array($data)){
    		return false;
    	}
    	return $this->publishMessage($queueName,$data,'string','',$exchangeName,'',$headers);
    } 
    
    
    public function resendMessages(){
    	$messages = func_query("select * from mk_queue_retry_data where status = 'CREATED'",true);
    	foreach($messages as $message){
    		$ret = $this->sendMessage($message['queue_name'],$message['data'],$message['root_element'],$message['format'],false);
    		if($ret){
    			func_query("update mk_queue_retry_data set status = 'SENT' where id = $message[id]");
    		}
    	}
    } 
}

?>
