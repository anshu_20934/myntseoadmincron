<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: config.php,v 1.3.2.1 2006/05/02 11:27:18 max Exp $
#

if ( !defined('XCART_START') ) { header("Location: ../../"); die("Access denied"); }
#
# Global definitions for Magnifier module
#
# PLEASE DO NOT CHANGE ANY VALUES DEFINED IN THIS FILE, AS IT WILL HAVE
# A NEGATIVE EFFECT ON THE LOOK OF X-MAGNIFIER IMAGE VIEWER AND
# THE FUNCTIONALITY OF X-MAGNIFIER IN GENERAL
#

$addons['Magnifier'] = true;

$gd_not_loaded = false;
if (extension_loaded('gd')) {
	$gd_config = gd_info();
	if (!empty($gd_config["GIF Read Support"]) && !empty($gd_config["JPG Support"]) && !empty($gd_config["PNG Support"])) {
		$gd_config["correct_version"] = true;
	}   
} else {
	$gd_not_loaded = true;
}

$sql_tbl['images_Z'] = "xcart_images_Z";

$config['available_images']['Z'] = "M";
define("NO_CHANGE_LOCATION_Z", true);

if (defined("TOOLS")) {
	$tbl_demo_data[] = 'images_Z';
}

$max_image_size = 2000;

$x_tile_size = 100;
$y_tile_size = 100;

$x_thmb = 80;
$y_thmb = 65;

$x_work_area = 366-2;
$y_work_area = 281-2;

$jpg_qlt_tile = '80';
$jpg_qlt_level = '85';
$jpg_qlt_thmb = '95';

if (!function_exists("imagejpeg") || !function_exists("imagecopyresampled") || !function_exists("imageCreatetruecolor")) {
	func_unset($active_modules, "Magnifier");
	return;
}
?>
