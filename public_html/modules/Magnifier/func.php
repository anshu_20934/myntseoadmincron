<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. | 
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: func.php,v 1.3.2.3 2006/06/27 05:29:43 max Exp $
#
# Functions of the Magnifier module
#

#
# Write XML data to file
#
function func_magnifier_write_xml($data, $fname) {
	$fp = @fopen($fname,"w+");
	if ($fp) {
		fwrite($fp, $data);
		fclose($fp);
		return true;
	}

	return false;
}

#
# Write image content to file
#
function func_magnifier_write_tile($data, $fname, $qlt) {
	if (!is_writable(dirname($fname)))
		return false;

	imagejpeg($data, $fname, $qlt);

	return true;
}

#
# Directory copy
#
function func_magnifier_dircpy($source, $dest) {
	$ddir = opendir($source);
	if (!$ddir)
		return false;

	while ($file = readdir($ddir)) {
		if ($file == "." || $file == ".." || !is_file($source.DIRECTORY_SEPARATOR.$file))
			continue;

		@copy($source.DIRECTORY_SEPARATOR.$file, $dest.DIRECTORY_SEPARATOR.$file);
	}
	closedir($ddir);
}

function func_magnifier_get_levels_gradation($x_, $y_, $max_) {
	global $x_work_area, $y_work_area;

	if ($x_/$y_ > $x_work_area/$y_work_area) {
		$x_fit = $x_work_area;
		$y_fit = ceil($x_fit*$y_/$x_);
	} else {
		$y_fit = $y_work_area;
		$x_fit = ceil($y_fit*$x_/$y_);
	}

	if ($max_ >= 1500) {
		$num_levels = 4;
	} elseif ($max_ >= 2*max($x_work_area, $y_work_area) && $max_ < 1500) {
		$num_levels = 3;
	} elseif ($max_ >= max($x_work_area, $y_work_area) && $max_ < 2*max($x_work_area, $y_work_area)) {
		$num_levels = 2;
	} else {
		$num_levels = 1;
	}

	if ($num_levels > 1) {
		$max_zoom = $x_/$x_fit;
		$zoom_step = ($max_zoom - 1)/($num_levels-1);
	} else {
		$max_zoom = 1;
	}

	$data_lvls = array();   

	for ($i = 0; $i < $num_levels; $i++) {
		$data_lvls[$i]["zoom"] = round(($i*$zoom_step + 1)*100);
		$data_lvls[$i]["rate_zoom"] = $data_lvls[$i]["zoom"]/(100*$max_zoom);
		$data_lvls[$i]["slice"] = (($i == 0) ? false : true);
		$data_lvls[$i]["save_level"] = (($i == 0) ? true : false);
	}
	
	return $data_lvls;
}

function func_magnifier_get_levels($pict, $folder2save){
	global $x_tile_size, $y_tile_size, $jpg_qlt_tile, $jpg_qlt_level, $jpg_qlt_thmb, $x_thmb, $y_thmb, $max_image_size; 

	list($x, $y, $type) = getimagesize($pict);

	switch ($type) {
		case 1:
			if (!function_exists("imagecreatefromgif"))
				return false;
			$handle = @imagecreatefromgif($pict);
			break;

		case 2:
			if (!function_exists("imagecreatefromjpeg"))
				return false;
			$handle = @imagecreatefromjpeg($pict);
			break;

		case 3:
			if (!function_exists("imagecreatefrompng"))
				return false;
			$handle = @imagecreatefrompng($pict);
			break;

		default:
			return false;
	}	

	if (!$handle)
		return false;

	$max = max($x, $y);
	$min = min($x, $y);

#	Create thumbnail
	$thmb = imageCreatetruecolor($x_thmb, $y_thmb);

	if ($x/$y > $x_thmb/$y_thmb) {
		$x_visible = $x_thmb*$y/$y_thmb;
		$x_start = ceil(($x - $x_visible)/2);
		$x_end = $x_visible;
		$y_start = 0;
		$y_end = $y;
	} else {
		$y_visible =  $y_thmb*$x/$x_thmb;
		$y_start = ceil(($y - $y_visible)/2);
		$y_end = $y_visible;
		$x_start = 0;
		$x_end = $x;
	}

	imagecopyresampled($thmb, $handle, 0, 0, $x_start, $y_start, $x_thmb, $y_thmb, $x_end, $y_end);
	if (!func_magnifier_write_tile($thmb, $folder2save."thumbnail.jpg", $jpg_qlt_thmb))
		return false;

	# Resize if the images is too big
	if ($max > $max_image_size) {
		$size_in_pixel = $max_image_size;
		$final_x = ceil($x*$max_image_size/$max);
		$final_y = ceil($y*$max_image_size/$max);
		imagecopyresampled($handle, $handle, 0, 0, 0, 0, $final_x, $final_y, $x, $y);
		$x = $final_x;
		$y = $final_y;
		$max = max($x, $y);
		$min = min($x, $y);
	}	

	$data_lvls = func_magnifier_get_levels_gradation($x, $y, $max);

	$xml_tiles = "<image>";
	
	foreach ($data_lvls as $key => $lvl) {
	                                                  
#		$size_in_pixel : Size max of the label in pixel.  The size of the picture being
#		proportional to the original, this value define maximum size
#		of largest side with dimensions of the picture.
		
		$size_in_pixel = $max*$lvl["rate_zoom"];

		$rate = $max/$size_in_pixel;
		$final_x = $x/$rate;
		$final_y = $y/$rate;

		if ($final_x > $x) {
			$final_x = $x;
			$final_y = $y;
		}

		$final_x = ceil($final_x);
		$final_y = ceil($final_y);

		$black_picture = imageCreatetruecolor($final_x,$final_y);
		imagecopyresampled($black_picture, $handle, 0, 0, 0, 0, $final_x, $final_y, $x, $y);

		# save entire picture
		if ($lvl["save_level"]) {
			if (!func_magnifier_write_tile($black_picture, $folder2save."level_".$key.".jpg", $jpg_qlt_level))
				return false;
		}	
	
		# get tiles and save it
		if ($lvl["slice"]) {
		
			$tile = imageCreatetruecolor($x_tile_size,$y_tile_size);
		
			$x_current_size = 0;
			$xi = 0;
		
			while ($x_current_size < $final_x) {
				$y_current_size = 0;
				$yi = 0;
	
				while ($y_current_size < $final_y) {

					if ((($y_current_size + $y_tile_size) > $final_y || ($x_current_size + $x_tile_size) > $final_x)) {
				
						$x_boundary_tile = $x_tile_size;
						$y_boundary_tile = $y_tile_size;
						
						if (($y_current_size + $y_tile_size)>$final_y) {
							$y_boundary_tile = $final_y - $y_current_size;
						}
	
						if (($x_current_size + $x_tile_size)>$final_x) {
							$x_boundary_tile = $final_x - $x_current_size;
						}
						
						$boundary_tile = imageCreatetruecolor($x_boundary_tile,$y_boundary_tile);
						imagecopyresampled($boundary_tile, $black_picture, 0, 0, $x_current_size, $y_current_size, $x_boundary_tile, $y_boundary_tile, $x_boundary_tile, $y_boundary_tile);
						if (!func_magnifier_write_tile($boundary_tile, $folder2save.$key."_".$yi."_".$xi.".jpg", $jpg_qlt_tile))
							return false;
						imagedestroy($boundary_tile);
						
					} else {	
	
						imagecopyresampled($tile, $black_picture, 0, 0, $x_current_size, $y_current_size, $x_tile_size, $y_tile_size, $x_tile_size, $y_tile_size);
						if (!func_magnifier_write_tile($tile, $folder2save.$key."_".$yi."_".$xi.".jpg", $jpg_qlt_tile))
							return false;
					}
					
					$yi++;
					$y_current_size = $y_current_size + $y_tile_size;
				}
				$xi++;
				$x_current_size = $x_current_size + $x_tile_size;
			}
	
			imagedestroy($tile);
		}
	
		imagedestroy($black_picture);
	
		if (empty($xi))
			$xi=1;
		if (empty($yi))
			$yi=1;
		$xml_tiles .= '<level num="'.$key.'" nRows="'.func_magnifier_num2code($yi, 98453).'" nColumns="'.func_magnifier_num2code($xi, 87211).'" zoom="'.$lvl["zoom"].'"/>';
	}	
	
	imagedestroy($handle);

	if (!func_magnifier_write_xml($xml_tiles."</image>", $folder2save."description.xml"))
		return false;

	return array("x" => $x, "y" => $y );
}

#
# Convert number to code
#
function func_magnifier_num2code($code, $add_factor) {
	static $rings = array(
		"3389346389738472262599114482527855889427381238444584595943195771",
		"6543431752478212416915624651999643875859187898938638181555644598",
		"2491485317542518145612579559617757785326169219124574632676537313",
		"4556148722172396539996566819578923627598695825577466123613669461",
		"1485625192626198631849785736115253725224476985848932991473184598"
	);

	for ($i = 0; $i < strlen((string)$add_factor); $i++) {
		$code = $code*substr((string)$add_factor, $i, 1);
	}
	$code = $code*substr($rings[0], substr($add_factor, 0, 1), 1);
	$numbers = array();

	# Get 'crc'
	$crc = 0;
	$str = str_replace(".", "", (string)$code);
	for( $i = 0; $i < strlen($str); $i++) {
		$crc += ord(substr($str, $i, 1));
		$numbers[] = substr($str, $i, 1);
	}
	$crc = dechex(round($crc / substr($add_factor, -1)));
	$crc .= dechex(strlen($crc));

	# Multiply rings and number in cycle
	$x = 0;
	foreach($rings as $i => $ring) {
		$step = substr($add_factor, $i, 1);

		foreach ($numbers as $idx => $n) {
			if ($x >= strlen($ring)) {
				$x -= strlen($ring)-1;
			}
			$numbers[$idx] = $n*substr($ring, $x, 1);
			$x += $step;
		}
	}

	# Convert number to HEX
	$result = "";
	foreach ($numbers as $n) {
		$s = dechex($n);
		$result .= dechex(strlen($s)).$s;
	}

	return $result.$crc;
}

?>
