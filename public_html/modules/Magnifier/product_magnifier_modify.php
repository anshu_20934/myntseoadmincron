<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: product_magnifier_modify.php,v 1.2.2.3 2006/06/27 05:29:43 max Exp $
#
#
#

if ( !defined('XCART_SESSION_START') ) { header("Location: ../../"); die("Access denied"); }


if ($mode == "product_zoomer") {

	# Add image
	if (empty($file_upload_data) || empty($file_upload_data["Z"])) {
		func_refresh("zoomer");
	}

	$query_data = array(
		"id" => $productid,
		"date" => time()
	);
	$imageid = func_array2insert("images_Z", $query_data);

	$root_folder2save = func_image_dir("Z");
	$folder2save = $root_folder2save.DIRECTORY_SEPARATOR.$productid.DIRECTORY_SEPARATOR.$imageid.DIRECTORY_SEPARATOR;
	if (!file_exists($folder2save)) {
		func_mkdir($folder2save);
	}
	
	$image_size = func_magnifier_get_levels($file_upload_data["Z"]["file_path"], $folder2save);
	if (!$image_size) {
		func_rm_dir($xcart_dir."/images/Z/".$productid."/".$imageid);
		db_query("DELETE FROM $sql_tbl[images_Z] WHERE imageid=".$imageid);
		@unlink($file_upload_data["Z"]["file_path"]);
		
		$top_message["content"] = func_get_langvar_by_name("msg_adm_incorrect_format_4zoomer");
		$top_message["type"] = "E";
		func_refresh("zoomer");
	} 

	$query_data = array(
		"image_x" => $image_size["x"],
		"image_y" => $image_size["y"],
		"image_path" => "./images/Z/".$productid."/".$imageid."/level_0.jpg",
		"image_type" => "image/jpeg",
		"md5" => func_md5_file($xcart_dir."/images/Z/".$productid."/".$imageid."/level_0.jpg"),
		"image_size" => func_filesize($xcart_dir."/images/Z/".$productid."/".$imageid."/level_0.jpg"),
		"filename" => $file_upload_data["Z"]['filename'],
		"orderby" => func_query_first_cell("SELECT MAX(orderby) FROM $sql_tbl[images_Z] WHERE id = '$productid'")+1
	);
	func_array2update("images_Z", $query_data, "imageid = '$imageid'");	
	
	if ($geid && !empty($fields['new_z_image'])) {
		while ($pid = func_ge_each($geid, 1, $productid)) {
			$query_data['id'] = $pid;
			$query_data['date'] = time();
			$pimageid = func_array2insert("images_Z", $query_data);

			$pfolder2save = $root_folder2save.DIRECTORY_SEPARATOR.$pid.DIRECTORY_SEPARATOR.$pimageid.DIRECTORY_SEPARATOR;
			if (!file_exists($pfolder2save)) {
				func_mkdir($pfolder2save);
			}	

			func_magnifier_dircpy($folder2save, $pfolder2save);
		}
	}
	
	@unlink($file_upload_data["Z"]["file_path"]); 

	$top_message["content"] = func_get_langvar_by_name("msg_adm_images_added_4zoomer");
	func_refresh("zoomer");

} elseif ($mode == "zoomer_update_availability" && !empty($zoomer_image)) {

	# Update images
	foreach ($zoomer_image as $key => $value) {
		db_query("UPDATE $sql_tbl[images_Z] SET orderby='".$value["orderby"]."', avail='".$value["avail"]."' WHERE imageid='$key'");
	}
	
	$top_message["content"] = func_get_langvar_by_name("msg_adm_images_updated_4zoomer");
	func_refresh("zoomer");

} elseif ($mode == "product_zoomer_delete") {

	# Delete images
	if (!empty($iids)) {
		db_query("DELETE FROM $sql_tbl[images_Z] WHERE imageid IN ('".implode("','", array_keys($iids))."')");
		foreach($iids as $imageid => $tmp) {
			func_rm_dir(func_image_dir("Z").DIRECTORY_SEPARATOR.$productid.DIRECTORY_SEPARATOR.$imageid);
		}
		$top_message["content"] = func_get_langvar_by_name("msg_adm_images_deleted_4zoomer");
	}
	
	func_refresh("zoomer");
}

if (!empty($productid)) {
	$zoomer_images = func_query("SELECT * FROM $sql_tbl[images_Z] WHERE id = '".$productid."' ORDER BY orderby, imageid");
	if (!empty($zoomer_images))
		$smarty->assign("zoomer_images", $zoomer_images);
}

$smarty->assign("gd_not_loaded", $gd_not_loaded);
$smarty->assign("gd_config", $gd_config);

?>
