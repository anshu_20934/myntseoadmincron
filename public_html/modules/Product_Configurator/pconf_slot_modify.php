<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: pconf_slot_modify.php,v 1.13 2006/01/11 06:56:16 mclap Exp $
#

if ( !defined('XCART_SESSION_START') ) { header("Location: ../../"); die("Access denied"); }

if (empty($active_modules["Product_Configurator"]))
	return;

x_load('backoffice');

#
# Add/modify Configurable product
#
if ($REQUEST_METHOD == "POST") {

	$page_anchor = "#slot";

	if (is_array($posted_data)) {
		if ($action == "update_markups" || $action == "delete_markups") {
			foreach ($posted_data as $k=>$v) {
				if ($v["delete"] && $action == "delete_markups") {
					db_query("DELETE FROM $sql_tbl[pconf_slot_markups] WHERE markupid='$k'");
					continue;
				}
				elseif ($action != "delete_markups") {
					db_query("UPDATE $sql_tbl[pconf_slot_markups] SET markup='".func_convert_number($v['markup'])."', markup_type='$v[markup_type]', membershipid = '$v[membershipid]' WHERE markupid='$k'");
				}
			}

			$page_anchor = "#price";
		}

		if ($action == "delete_rules") {
			foreach ($posted_data as $k=>$v) {
				if ($v["delete"])
					db_query("DELETE FROM $sql_tbl[pconf_slot_rules] WHERE slotid='$slot' AND index_by_and='$k'");
			}

			$page_anchor = "#rules";
		}

		if ($action == "update_rules") {
			$index_by_and = func_query_first_cell("SELECT MAX(index_by_and) FROM $sql_tbl[pconf_slot_rules] WHERE slotid='$slot'") + 1;
			foreach ($posted_data as $k=>$v) {
				db_query("INSERT INTO $sql_tbl[pconf_slot_rules] (slotid, ptypeid, index_by_and) VALUES ('$slot', '$v', '$index_by_and')");
			}

			$page_anchor = "#rules";
		}
		elseif ($action == "update_slot") {
			if (!empty($posted_data["move_to_step"])) {
				$step_string = "stepid='$posted_data[move_to_step]', ";
			}

			db_query("UPDATE $sql_tbl[pconf_slots] SET $step_string orderby='$posted_data[orderby]', status='$posted_data[status]' WHERE slotid='$slot'");
			func_languages_alt_insert($language_var_names["slot_name"].$slot, $posted_data["slot_name"], $current_language);
			func_languages_alt_insert($language_var_names["slot_descr"].$slot, $posted_data["slot_descr"], $current_language);
		}
	}

	if ($action == "update_markups" && doubleval($new_markup)!=0) {
		#
		# Create new price modifier
		#
		if (func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[pconf_slot_markups] WHERE slotid='$slot' AND membershipid = '$new_membershipid'") == 0)
			db_query("INSERT INTO $sql_tbl[pconf_slot_markups] (slotid, markup, markup_type, membershipid) VALUES ('$slot', '".func_convert_number($new_markup)."', '$new_markup_type', '$new_membershipid')");

		$page_anchor = "#price";
	}

	func_header_location("product_modify.php?productid=$productid&mode=pconf&edit=slot&slot=$slot".$page_anchor);
}

if (!empty($slot)) {
	#
	# Get the slot data
	#
	$slot_data = func_query_first("SELECT * FROM $sql_tbl[pconf_slots] WHERE slotid='$slot'");
	$step = $slot_data["stepid"];
	$slot_data["slot_name"] = func_get_languages_alt($language_var_names["slot_name"].$slot_data["slotid"], $current_language);
	$slot_data["slot_descr"] = func_get_languages_alt($language_var_names["slot_descr"].$slot_data["slotid"], $current_language);

	#
	# Get data for step related with this slot
	#
	$wizards = func_query("SELECT * FROM $sql_tbl[pconf_wizards] WHERE productid='$productid' ORDER BY orderby, stepid");

	if (is_array($wizards)) {
		$counter = 1;
		foreach ($wizards as $k=>$v) {
			$wizards[$k]["step_name"] = func_get_languages_alt($language_var_names["step_name"].$v["stepid"], $current_language);
			$wizards[$k]["step_counter"] = $counter++;
			if ($v["stepid"] == $step) {
				$current_step = $wizards[$k];
			}
		}
	}
}

#
# Get the prodict types information
#
$provider_condition = ($single_mode ? "" : " AND provider='$product_info[provider]'");

$product_types = func_query("SELECT * FROM $sql_tbl[pconf_product_types] WHERE 1 $provider_condition ORDER BY orderby, ptype_name");

#
# Get the rule array for the slot
#
$indexes_by_and = func_query("SELECT index_by_and FROM $sql_tbl[pconf_slot_rules] WHERE slotid='$slot' GROUP BY index_by_and ORDER BY index_by_and");

if (is_array($indexes_by_and)) {
	foreach ($indexes_by_and as $k=>$v) {
		$rule_by_or["index_by_and"] = $v["index_by_and"];
		$rule_by_or["rules_by_and"] = func_query("SELECT $sql_tbl[pconf_product_types].* FROM $sql_tbl[pconf_product_types], $sql_tbl[pconf_slot_rules] WHERE $sql_tbl[pconf_product_types].ptypeid=$sql_tbl[pconf_slot_rules].ptypeid AND $sql_tbl[pconf_slot_rules].slotid='$slot' AND $sql_tbl[pconf_slot_rules].index_by_and='$v[index_by_and]'");
		$rules_by_or[] = $rule_by_or;
	}

	$smarty->assign("rules_by_or", $rules_by_or);
}

#
# Get the markups data for current slot
#
$markups = func_query("SELECT * FROM $sql_tbl[pconf_slot_markups] WHERE slotid='$slot' ORDER BY markupid");

$smarty->assign("markups", $markups);
$smarty->assign("wizards", $wizards);
$smarty->assign("slot_data", $slot_data);
$smarty->assign("slot", $slot);
$smarty->assign("step", $step);
$smarty->assign("wizard_data", $current_step);
$smarty->assign("product_types", $product_types);
$smarty->assign("memberships",func_get_memberships());

?>
