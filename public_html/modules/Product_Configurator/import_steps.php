<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: import_steps.php,v 1.9 2006/04/12 10:48:39 max Exp $
#

/******************************************************************************
Used cache format:
Products (by Product ID):
	data_type: 	PI
	key:		<Product ID>
	value:		[<Product code> | RESERVED]
Products (by Product code):
	data_type: 	PR
	key:		<Product code>
	value:		[<Product ID> | RESERVED]
Products (by Product name):
	data_type:  PN
	key:		<Product name>
	value:		[<Product ID> | RESERVED]
Wizard steps (by Product ID / Product code / Product name):
	data_type: 	St
	key:		<Product ID / Product code / Product name><EOL><Step name>
	value:		[<Step ID> | RESERVED]
Wizard steps (by Step ID):
	data_type: 	Si
	key:		<Step ID>
	value:		<Step ID>
Old step IDs:
	data_type:	oPW
	key:		<Product ID>_<Step name>
	value:		<Step ID>
Deleted product data:
	data_type:	DP
	key:		<Product ID>
	value:		<Flags>

Note: RESERVED is used if ID is unknown
	EOL - End-of_line symbol (\n)
******************************************************************************/


if ( !defined('XCART_SESSION_START') ) { header("Location: ../"); die("Access denied"); }

if ($import_step == "process_row") {
	#
	# PROCESS ROW from import file
	#

	# Check productid / productcode / product
	list($_productid, $_variantid) = func_import_detect_product($values);
	if (is_null($_productid) || ($action == "do" && empty($_productid))) {
		func_import_module_error("msg_err_import_log_message_14");
		return false;
	}

	# Check step
	foreach ($values['step'] as $k => $v) {
		func_import_get_pb_cache($values, "St", $v, true);
	}

	$values['productid'] = $_productid;

	$data_row[] = $values;
}
elseif ($import_step == "finalize") {
	#
	# FINALIZE rows processing: update database
	#

	# Drop old data
	if ($import_file["drop"][strtolower($section)] == "Y") {
		# Delete data by provider
		if ($provider_condition) {
			$products = db_query("SELECT productid FROM $sql_tbl[products] WHERE provider = '".addslashes($import_data_provider)."'");
			if ($products) {
				while ($productid = db_fetch_array($products)) {	
					$productid = $productid['productid'];
					$stepids = func_query_column("SELECT stepid FROM $sql_tbl[pconf_wizards] WHERE productid = '$productid'");
					if (!empty($stepids)) {
						func_import_save_cache_ids("oPW", "SELECT stepid, productid, step_name FROM $sql_tbl[pconf_wizards] WHERE productid = '$productid'");
						db_query("DELETE FROM $sql_tbl[pconf_wizards] WHERE productid = '$productid'");
						$slotids = func_query_column("SELECT slotid FROM $sql_tbl[pconf_slots] WHERE stepid IN ('".implode("','", $stepids)."')");
						if (!empty($slotids)) {
							db_query("DELETE FROM $sql_tbl[pconf_slots] WHERE stepid IN ('".implode("','", $stepids)."')");
							db_query("DELETE FROM $sql_tbl[pconf_slot_rules] WHERE stepid IN ('".implode("','", $stepids)."')");
							db_query("DELETE FROM $sql_tbl[pconf_slot_markups] WHERE stepid IN ('".implode("','", $stepids)."')");
						}
					}
				}
			}
		}
		else {
			# Delete all old data
			func_import_save_cache_ids("oPW", "SELECT stepid, productid, step_name FROM $sql_tbl[pconf_wizards]");

			db_query("DELETE FROM $sql_tbl[pconf_wizards]");
			db_query("DELETE FROM $sql_tbl[pconf_slots]");
			db_query("DELETE FROM $sql_tbl[pconf_slot_rules]");
			db_query("DELETE FROM $sql_tbl[pconf_slot_markups]");
		}
			
		$import_file["drop"][strtolower($section)] = "";
	}

	#
	# Import data...
	#
	foreach ($data_row as $row) {
		# Import product configurator steps
		foreach ($row['step'] as $k => $v) {
			# Detect step
			$_stepid = func_query_first_cell("SELECT $sql_tbl[pconf_wizards].stepid FROM $sql_tbl[pconf_wizards], $sql_tbl[products] WHERE $sql_tbl[pconf_wizards].productid = $sql_tbl[products].productid AND $sql_tbl[pconf_wizards].productid = '$row[productid]' AND $sql_tbl[pconf_wizards].step_name = '".addslashes($v)."' AND $sql_tbl[products].provider = '".addslashes($import_data_provider)."'");

			$data = array(
				"productid"	=> $row['productid'],
				"step_name"	=> addslashes($v)
			);
			if (isset($row['descr'][$k]))
				$data['step_descr'] = addslashes($row['descr'][$k]);

			if (isset($row['orderby'][$k]))
				$data['orderby'] = addslashes($row['orderby'][$k]);

			if (empty($_stepid)) {

				# Add step
				$_stepid = func_import_get_cache("oPW", array($row['productid'], $v));
				if (!empty($_stepid) && func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[pconf_wizards] WHERE stepid = '$_stepid'") == 0)
					$data['stepid'] = $_stepid;
				$_stepid = func_array2insert("pconf_wizards", $data);
				$result[strtolower($section)]["added"]++;

			} else {

				# Update step
				func_array2update("pconf_wizards", $data, "stepid = '$_stepid'");
				$result[strtolower($section)]["updated"]++;
			}

			if (!empty($_stepid)) {
				func_import_save_pb_cache($row, "St", $v, $_stepid);
				func_import_save_cache("Si", $_stepid, $_stepid);
			}
		}

		func_flush(". ");
	}
}
elseif ($import_step == "export") {
	# Export data	

	while (($id = func_export_get_row($data)) !== false) {
		if (empty($id))
			continue;

		# Get data
		$mrow = func_query("SELECT $sql_tbl[pconf_wizards].* FROM $sql_tbl[pconf_wizards], $sql_tbl[products] WHERE $sql_tbl[pconf_wizards].productid = $sql_tbl[products].productid AND $sql_tbl[pconf_wizards].productid = '$id'".(empty($provider_sql) ? "" : " AND $sql_tbl[products].provider = '$provider_sql'"));
		if (empty($mrow))
			continue;

		# Get product signature
		$p_row = func_export_get_product($id);
		if (empty($p_row))
			continue;

		foreach ($mrow as $row) {
			$p_row['step'][] = $row['step_name'];
			$p_row['descr'][] = $row['step_descr'];
			$p_row['orderby'][] = $row['orderby'];
		}

		# Write row	
		if (!func_export_write_row($p_row))
			break;
	}
}

?>
