<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: func.php,v 1.2 2006/01/11 06:56:16 mclap Exp $
#
if ( !defined('XCART_START') ) { header("Location: ../../"); die("Access denied"); }

#
# This function calculates the available quantity for sale for product
#
function func_pconf_get_max_quantity($configuration) {
	global $sql_tbl, $cart;
	global $config;

	$max_quantity = "not defined";

	if (is_array($configuration["steps"])) {
		foreach ($configuration["steps"] as $stepid=>$stepinfo) {
			if (is_array($stepinfo["slots"])) {
				foreach ($stepinfo["slots"] as $slotid=>$slotinfo) {
					$in_cart = 0;
					if (!empty($cart) && !empty($cart["products"])) {
						foreach ($cart["products"] as $cart_item) {
							if ($cart_item["productid"] == $id)
								$in_cart += $cart_item["amount"];
						}
					}

					if ($config["General"]["unlimited_products"] == "Y")
						$avail = $config["Appearance"]["max_select_quantity"];
					else
						$avail = func_query_first_cell("SELECT MAX(avail)-$in_cart FROM $sql_tbl[products] WHERE productid='$slotinfo[productid]'");

					if ($max_quantity == "not defined" || $max_quantity > $avail)
						$max_quantity = $avail;
				}
			}
		}
	}
	elseif ($config["Product_Configurator"]["allow_to_add_empty_product_to cart"] == "Y") {
		$max_quantity = 1;
	}

	return ($max_quantity > 0 ? $max_quantity : 0);
}

?>
