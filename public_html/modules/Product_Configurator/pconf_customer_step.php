<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: pconf_customer_step.php,v 1.41.2.4 2006/08/02 07:38:45 max Exp $
#

if ( !defined('XCART_SESSION_START') ) { header("Location: ../../"); die("Access denied"); }


if(empty($active_modules["Product_Configurator"]))
	func_header_location("product.php?productid=".$productid);

x_load('product','taxes');

x_session_register("cart");

#
# The checking of bidirectional requirements is turned on
#
$check_bidirectional_requirements = "Y";

#
# Use 'OR' rule while checking of the product type requirements (else 'AND' rule will be used)
#
$check_product_type_requirements_by_or = 'Y';

#
# This function gather the step information
#
function func_get_slots_info($stepid, $step_counter) {
	global $sql_tbl, $config, $continue_button, $user_account;
	global $configurations, $productid;
	global $language_var_names, $store_language;

	#
	# Get slots data
	#
	$slots_data = func_query("SELECT * FROM $sql_tbl[pconf_slots] WHERE stepid='$stepid' AND status!='N' ORDER BY orderby, slotid");

	if (is_array($slots_data)) {
		foreach ($slots_data as $k=>$v) {
			$slots_data[$k]["slot_name"] = func_get_languages_alt($language_var_names["slot_name"].$v["slotid"], $store_language);
			$slots_data[$k]["slot_descr"] = func_get_languages_alt($language_var_names["slot_descr"].$v["slotid"], $store_language);
		#
		# Each slot must have the rules for products inserting
		#
			$slots_data[$k]["have_rules"] = func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[pconf_slot_rules] WHERE slotid='$v[slotid]'");
			if (!empty($configurations[$productid]["steps"][$step_counter]["slots"][$v["slotid"]])) {
				$product_selected = $configurations[$productid]["steps"][$step_counter]["slots"][$v["slotid"]];
				$slots_data[$k]["have_rules"] = func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[pconf_slot_rules] WHERE slotid='$v[slotid]'");
				#
				# Get the price modifier data
				#
				$slots_data[$k]["price_modifier"] = func_query_first("SELECT * FROM $sql_tbl[pconf_slot_markups] WHERE slotid='$v[slotid]' AND membershipid IN ('$user_account[membershipid]', 0) ORDER BY membershipid DESC");
				$configurations[$productid]["steps"][$step_counter]["slots"][$v["slotid"]]["price_modifier"] = $slots_data[$k]["price_modifier"];
				#
				# Get the product data
				#
				global $pconf;
				$pconf = $productid;
				$slots_data[$k]["product"] = func_select_product($product_selected["productid"], @$user_account["membershipid"], false, true);
				if (is_array($slots_data[$k]["product"])) {
				#
				# Correct the product data
				#
					$slots_data[$k]["product"] = func_array_merge($product_selected, $slots_data[$k]["product"]);
					if ($slots_data[$k]["product"]["options"]) {
						list($variant, $slots_data[$k]["product"]["product_options"]) = func_get_product_options_data($slots_data[$k]["product"]["productid"], $slots_data[$k]["product"]["options"], @$user_account["membershipid"]);
						if($variant)
							$slots_data[$k]["product"] = func_array_merge($slots_data[$k]["product"], $variant);
						$slots_data[$k]["product"]["options_surcharge"] = 0;
						if ($slots_data[$k]["product"]["product_options"]) {
							foreach($slots_data[$k]["product"]["product_options"] as $o) {
								$slots_data[$k]["product"]["options_surcharge"] += ($o['modifier_type'] == '%'?($slots_data[$k]["product"]["price"]*$o['price_modifier']/100):$o['price_modifier']);
							}
						}

						$slots_data[$k]["product"]["price"] += $slots_data[$k]["product"]["options_surcharge"];
					}
					#
					# Apply price modifier
					#
					if ($slots_data[$k]["price_modifier"]["markup_type"] == "$")
						$slots_data[$k]["product"]["price"] = max($slots_data[$k]["product"]["price"] + $slots_data[$k]["price_modifier"]["markup"], 0);
					elseif ($slots_data[$k]["price_modifier"]["markup_type"] == "%")
						$slots_data[$k]["product"]["price"] = max($slots_data[$k]["product"]["price"] + price_format($slots_data[$k]["product"]["price"] * $slots_data[$k]["price_modifier"]["markup"] / 100), 0);

					#
					# Get the taxes info and taxed price
					#
					$slots_data[$k]["product"]["taxes"] = func_get_product_taxes($slots_data[$k]["product"], $user_account["login"]);
				}
			}
			elseif ($slots_data[$k]["status"] == "M") {
				$continue_button = false;
			}
		}
	}

	return $slots_data;
}


#
# Get the current step id
#
if (empty($configurations[$productid]["current_stepid"]))
	$configurations[$productid]["current_stepid"] = 1;

$current_stepid = $configurations[$productid]["current_stepid"];


if ($mode == "continue") {
	$configurations[$productid]["current_stepid"]++;
	if ($configurations[$productid]["reconfigure"])
		$configurations[$productid]["current_stepid"] = "last";

	func_header_location("pconf.php?productid=".$productid);
}

if ($mode == "back") {
	$steps_number = func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[pconf_wizards] WHERE productid='$productid'");
	if ($current_stepid == "last")
		$configurations[$productid]["current_stepid"] = $steps_number;
	else
		$configurations[$productid]["current_stepid"] = ($current_stepid-1<1 ? 1 : $current_stepid-1);

	func_header_location("pconf.php?productid=".$productid);
}

if ($mode == "update") {
	$step = intval($step);
	$configurations[$productid]["current_stepid"] = ($step>0?$step:1);
	func_header_location("pconf.php?productid=".$productid);
}

if ($mode == "reset") {
	unset($configurations[$productid]);
	func_header_location("pconf.php?productid=".$productid);
}

if ($mode == "reconfigure") {
	if (!empty($wlitem)) {
		$wldata = func_query_first("SELECT * FROM $sql_tbl[wishlist] WHERE wishlistid='$wlitem'");
		if (!empty($wldata)) {
			$configurations[$productid] = unserialize($wldata["object"]);
			$configurations[$productid]["reconfigure"] = true;
			$configurations[$productid]["wishlistid"] = $wlitem;
			$configurations[$productid]["cartid"] = "";
			$configurations[$productid]["amount"] = $wldata["amount"];
			$configurations[$productid]["options"] = unserialize($wldata["options"]);
		}
	}
	elseif (!empty($itemid) && is_array($cart["products"])) {
		foreach ($cart["products"] as $k=>$v) {
			if ($v["cartid"] == $itemid) {
				$configurations[$v["productid"]] = $v["pconf_data"];
				$configurations[$v["productid"]]["reconfigure"] = true;
				$configurations[$v["productid"]]["cartid"] = $itemid;
				$configurations[$v["productid"]]["wishlistid"] = "";
				$configurations[$v["productid"]]["amount"] = $v["amount"];
				$configurations[$v["productid"]]["options"] = $v["options"];
				break;
			}
		}
	}

	func_header_location("pconf.php?productid=".$productid);
}


if ($REQUEST_METHOD == "POST") {

	if ($mode == "pconf_update") {
	#
	# Update the configurable product data...
	#
		if (intval($amount) <= 0)
			func_header_location("pconf.php?productid=$productid");

		$max_quantity = func_pconf_get_max_quantity($configurations[$productid]);
		if (intval($amount) > $max_quantity)
			func_header_location("pconf.php?productid=$productid&error=amount");

		#
		# Get the wizards info for this product
		#
		$wizards = func_query("SELECT * FROM $sql_tbl[pconf_wizards] WHERE productid='$productid' ORDER BY orderby, stepid");

		$product_price = 0;

		if (is_array($wizards)) {
			$counter = 1;
			foreach ($wizards as $k=>$v) {
				$v["step_counter"] = $counter++;
				$wizards[$k]["slots"] = func_get_slots_info($v["stepid"], $v["step_counter"]);
				if (is_array($wizards[$k]["slots"])) {
					foreach ($wizards[$k]["slots"] as $k1=>$v1) {
						$product_price += $v1["product"]["price"];
					}
				}
			}
		}

		if (!empty($configurations[$productid]["cartid"])) {
		#
		# in the cart
		#
			if (is_array($cart["products"])) {
				$cartid = $configurations[$productid]["cartid"];
				foreach ($cart["products"] as $k=>$v) {
					if ($v["cartid"] == $cartid) {
					#
					# Configurable product in the cart is found
					#
						if (!empty($active_modules["Product_Options"])) {
						#
						# Do addition to cart with options
						#
							$options=array();
							if(!empty($product_options)) {
								if ($active_modules["Product_Options"])
									if(func_check_product_options ($productid, $product_options))
										$cart["products"][$k]["options"] = $product_options;
							}
						}

						$cart["products"][$k]["amount"] = $amount;
						$cart["products"][$k]["price"] = $product_price;
						$cart["products"][$k]["free_price"] = $product_price;
						$cart["products"][$k]["pconf_data"] = $configurations[$productid];
						#
						# Delete all subproducts
						#
						foreach ($cart["products"] as $k1=>$v1) {
							if ($v1["hidden"] != $cartid)
								$products[] = $v1;
						}

						$cart["products"] = $products;

						$mode = "add";
						$productindex = $k;
						$added_product = func_select_product($productid, @$user_account["membershipid"]);
						include $xcart_dir."/modules/Product_Configurator/pconf_customer_cart.php";
						break;
					}
				}
			}

			func_header_location("cart.php");
		}
		elseif (!empty($active_modules["Wishlist"]) && !empty($configurations[$productid]["wishlistid"])) {
		#
		# in the wish list
		#
			$wishlistid = $configurations[$productid]["wishlistid"];

			db_query("UPDATE $sql_tbl[wishlist] SET amount='$amount', options='".addslashes(serialize($product_options))."', object='".addslashes(serialize($configurations[$productid]))."' WHERE wishlistid='$wishlistid'");

			unset($configurations[$productid]);

			if (!empty($active_modules["Gift_Registry"])) {
			#
			# Redirect to the gift registry
			#
				$eventid = func_query_first_cell("SELECT event_id FROM $sql_tbl[wishlist] WHERE wishlistid='$wishlistid'");
				if (!empty($eventid))
					func_header_location("giftreg_manage.php?eventid=$eventid&mode=products");
			}

			#
			# Redirect to the wish list
			#
			func_header_location("cart.php?mode=wishlist");
		}

		func_header_location("pconf.php?productid=$productid");
	}

	if ($mode == "add") {
		$pconf = $productid;
		$product_info = func_select_product($addproductid, $user_account["membershipid"]);
		$options=array();
		if(!empty($product_options)) {
			if ($active_modules["Product_Options"])
				if(func_check_product_options ($addproductid, $product_options))
					$options = $product_options;
		}

		$product = array("productid"=>$addproductid, "productcode"=>$product_info["productcode"], "product"=>$product_info["product"], "amount"=>1,"options"=>$options);
		$configurations[$productid]["steps"][$current_stepid]["slots"][$slot] = $product;
	}

	func_header_location("pconf.php?productid=$productid");

}

if ($mode == "delete" && !empty($productid) && !empty($slot)) {
#
# Delete product from the slot
#
	unset($configurations[$productid]["steps"][$current_stepid]["slots"][$slot]);
	func_header_location("pconf.php?productid=$productid");
}

$smarty->assign("mode","configure_step");

#
# Get the wizards info for this product
#
$wizards = func_query("SELECT * FROM $sql_tbl[pconf_wizards] WHERE productid='$productid' ORDER BY orderby, stepid");


if (is_array($wizards)) {
#
# Assign counters for steps and get the current step info
#
	$counter = 1;
	foreach ($wizards as $k=>$v) {
		$wizards[$k]["step_name"] = func_get_languages_alt($language_var_names["step_name"].$v["stepid"], $store_language);
		$wizards[$k]["step_counter"] = $counter++;
		if ($current_stepid == $wizards[$k]["step_counter"])
			$current_step = $wizards[$k];
	}

	if (empty($current_step) || $current_stepid == "last") {
	#
	# Prepare for the configurator summary displaying
	#
		$smarty->assign("mode", "pconf_summary");

		$configurations[$productid]["reconfigure"] = true;
		$current_stepid = $configurations[$productid]["current_stepid"] = "last";
		x_session_save("configurations");

		$max_quantity = func_pconf_get_max_quantity($configurations[$productid]);
		$smarty->assign("max_quantity", $max_quantity);

		if(!empty($active_modules["Product_Options"])) {
			$options = $configurations[$productid]["options"];
			include $xcart_dir."/modules/Product_Options/customer_options.php";
		}

		$smarty->assign("amount", $configurations[$productid]["amount"]);

		$total_cost = 0;
		$taxed_total_cost = 0;
		$taxes = array();

		foreach ($wizards as $k=>$v) {
			$wizards[$k]["slots"] = func_get_slots_info($v["stepid"], $v["step_counter"]);
			if (is_array($wizards[$k]["slots"])) {
				foreach ($wizards[$k]["slots"] as $k1=>$v1) {
					if (empty($v1["product"]))
						continue;
					$total_cost += $v1["product"]["price"];
					$taxed_total_cost += $v1["product"]["taxed_price"];
					if (is_array($v1["product"]["taxes"])) {
						foreach ($v1["product"]["taxes"] as $tax_name=>$product_tax) {
							if (!isset($taxes[$tax_name])) {
								$taxes[$tax_name] = $product_tax;
								$taxes[$tax_name]["tax_value"] = 0;
							}

							$taxes[$tax_name]["tax_value"] += $product_tax["tax_value"];
						}
					}

					$configurations[$productid]["steps"][$v["step_counter"]]["slots"][$v1["slotid"]]["price"] = $v1["product"]["price"];
				}
			}
		}

		$configurations[$productid]["price"] = price_format($total_cost);

		x_session_save("configurations");

		$smarty->assign("total_cost", $total_cost);
		$smarty->assign("taxed_total_cost", $taxed_total_cost);
		$smarty->assign("taxes", $taxes);

		if (!empty($configurations[$productid]["cartid"]) && is_array($cart["products"])) {
			foreach ($cart["products"] as $k=>$v) {
				if ($v["cartid"] == $configurations[$productid]["cartid"]) {
					$smarty->assign("update", "cart");
					break;
				}
			}
		}
		elseif (!empty($configurations[$productid]["wishlistid"])) {
			$smarty->assign("update", "wishlist");
		}
	}
	else {
	#
	# Prepare step for displaying
	#
		$continue_button = true;

		$current_step["slots"] = func_get_slots_info($current_step["stepid"], $current_stepid);
		$current_step["step_descr"] = func_get_languages_alt($language_var_names["step_descr"].$current_step["stepid"], $store_language);
		$smarty->assign("continue_button", $continue_button);
	}
}


#
# Assign the current step for displaying in the location line
#
if ($current_stepid == "last")
	$location[] = array(func_get_langvar_by_name("lbl_summary"),"");
else
	$location[] = array(func_get_langvar_by_name("lbl_step")." $current_stepid","pconf.php?productid=$productid");

if ($current_stepid > 1)
	$previous_step = $current_stepid;

#
# $slot is $sql_tbl["pconf_slots"].slotid field
#
if (!empty($slot)) {
#
# Get the products list that satisfied with slot rules
#
	if (is_array($current_step["slots"])) {
		foreach ($current_step["slots"] as $k=>$v) {
			if ($v["slotid"] == $slot) {
				$current_slot = $v;
				break;
			}
		}
	}

	if (empty($current_slot))
		func_header_location("error_message.php?page_not_found");

	#
	# Get the rule array for the slot
	#

	#
	# Get the indexes by AND
	#
	$indexes_by_and = func_query("SELECT index_by_and FROM $sql_tbl[pconf_slot_rules] WHERE slotid='$slot' GROUP BY index_by_and ORDER BY index_by_and");

	if (is_array($indexes_by_and)) {
	#
	# Get the indexes by OR and generate the query string to search products
	#
		foreach ($indexes_by_and as $k=>$v) {
			$rule_by_or["index_by_and"] = $v["index_by_and"];
			$rule_by_or["rules_by_and"] = func_query("SELECT $sql_tbl[pconf_product_types].* FROM $sql_tbl[pconf_product_types], $sql_tbl[pconf_slot_rules] WHERE $sql_tbl[pconf_product_types].ptypeid=$sql_tbl[pconf_slot_rules].ptypeid AND $sql_tbl[pconf_slot_rules].slotid='$slot' AND $sql_tbl[pconf_slot_rules].index_by_and='$v[index_by_and]'");
			if (is_array($rule_by_or["rules_by_and"])) {
				foreach ($rule_by_or["rules_by_and"] as $k1=>$v1) {
					$ptypes_and[] = $v1["ptypeid"];
					$tmp = func_get_languages_alt("ptype_name_".$v1['ptypeid'], $shop_language);
					if (!empty($tmp))
						$rule_by_or["rules_by_and"][$k1]['ptype_name'] = $tmp;
				}

				$ptype_or_condition_array[] = "$sql_tbl[pconf_products_classes].ptypeid IN (".implode(",", $ptypes_and).")";
			}

			$ptype_condition = "(".implode(" OR ", $ptype_or_condition_array).")";
			$rules_by_or[] = $rule_by_or;
		}
	}

	#
	# Get the initial products list
	#
	if (!empty($ptype_condition)) {

    	$old_search_data = $search_data["products"];
		$search_data["products"] = array(
		);

		$search_data["products"]['_']['left_joins']['pconf_products_classes'] = array(
			"on" => "$sql_tbl[products].productid = $sql_tbl[pconf_products_classes].productid"
		);
		$search_data["products"]['_']['where'] = array(
			"$sql_tbl[pconf_products_classes].productid IS NOT NULL",
			"$sql_tbl[products].forsale != 'N'",
			$ptype_condition
		);
	
		$REQUEST_METHOD = "GET";
		$mode = "search";
		$_inner_search = true;
		include $xcart_dir."/include/search.php";
		$_inner_search = false;

		$search_data["products"] = $old_search_data;
		x_session_save("search_data");
	
		$smarty->assign("navigation_script","pconf.php?productid=$productid&slot=$slot");

		$slot_products = $products;
		unset($products, $old_search_data);

		if (!empty($slot_products)) {
			foreach ($slot_products as $k => $v)
				$slot_products[$k]["classes"] = func_query("SELECT classid, ptypeid FROM $sql_tbl[pconf_products_classes] WHERE productid = '$v[productid]'");

			#
			# Check if the found products satisfy the slot rules
			#
			$_slot_products_rules = array();

			# Prepare the product tyoe indexes array
			$_rules_prepared = array();
			for ($i = 0; $i < count($rules_by_or); $i++) {
				if (is_array($rules_by_or[$i]["rules_by_and"]) && count($rules_by_or[$i]["rules_by_and"]) > 1) {
					$_rule_by_and = array();
					for ($j = 0; $j < count($rules_by_or[$i]["rules_by_and"]); $j++)
						$_rule_by_and[] = $rules_by_or[$i]["rules_by_and"][$j]["ptypeid"];
				}
				else
					$_rule_by_and = $rules_by_or[$i]["rules_by_and"][0]["ptypeid"];
				$_rules_prepared[] = $_rule_by_and;
			}

			# Check found products
			if (!empty($_rules_prepared)) {
				foreach ($slot_products as $k => $_product) {
					foreach ($_rules_prepared as $_rule) {
						if (is_array($_rule)) {
							$_rule_condition = "IN ('" . implode("','", $_rule) . "')";
							$_ptype_limit = count($_rule);
						} else {
							$_rule_condition = "= '$_rule'";
							$_ptype_limit = 1;
						}

						if (func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[products], $sql_tbl[pconf_products_classes] WHERE $sql_tbl[products].productid=$sql_tbl[pconf_products_classes].productid AND $sql_tbl[products].productid = '$_product[productid]' AND $sql_tbl[pconf_products_classes].ptypeid ".$_rule_condition) == $_ptype_limit)
							$_slot_products_rules[$k][] = 1;
						else
							$_slot_products_rules[$k][] = 0;
					}
				}
				
				# Exclude products that is not satisfy the rules
				if (!empty($_slot_products_rules)) {
					foreach ($_slot_products_rules as $k => $v) {
						if (!strstr(implode("", $v), "1"))
							unset($slot_products[$k]);
					}
				}
			}
		}
	}

	if (is_array($configurations)) {
		#
		# Get list of products that already are in the slots
		#
		$found_products_specs = array();
		foreach ($configurations as $c_productid=>$c_step_info) {
			if ($c_productid != $productid)
				continue;

			if (!is_array($c_step_info["steps"]))
				continue;

			foreach ($c_step_info["steps"] as $c_stepid=>$c_slot_info) {
				if (!is_array($c_slot_info["slots"]))
					continue;

				foreach ($c_slot_info["slots"] as $c_slotid=>$c_product_info) {
					if ($c_slotid == $slot) continue;

					$filled_slot["slot_name"] = func_query_first_cell("SELECT slot_name FROM $sql_tbl[pconf_slots] WHERE slotid='$c_slotid'");
					$filled_slot["slot_name"] = func_get_languages_alt($language_var_names["slot_name"].$c_slotid, $store_language);
					$filled_slot["product"] = $c_product_info;
					if ($check_bidirectional_requirements == "Y") {
						$tmp_ptype_info = func_query_first("SELECT ptypeid, classid FROM $sql_tbl[pconf_products_classes] WHERE productid='$c_product_info[productid]'");
						$tmp_specs_info = func_query("SELECT specid FROM $sql_tbl[pconf_class_specifications] WHERE classid='$tmp_ptype_info[classid]'");
						if (is_array($tmp_specs_info)) {
							foreach ($tmp_specs_info as $t_spec)
								$found_products_specs[$c_product_info["productid"]][$tmp_ptype_info["ptypeid"]][] = $t_spec["specid"];
						}
					}

					$filled_slots[] = $filled_slot;
				}
			}
		}

		#
		# Get the requirements (on specifications) list
		#
		if (is_array($filled_slots) && is_array($ptypes_and)) {
			foreach ($filled_slots as $k=>$v) {
				$classes = func_query("SELECT * FROM $sql_tbl[pconf_products_classes] WHERE productid='".$v["product"]["productid"]."'");
				if (!is_array($classes))
					continue;

				foreach ($classes as $k1=>$v1) {
					$requirements = func_query("SELECT * FROM $sql_tbl[pconf_class_requirements] WHERE classid='$v1[classid]' ORDER BY ptypeid");
					if (!is_array($requirements))
						continue;

					foreach ($requirements as $k2=>$v2) {
						if (in_array($v2["ptypeid"], $ptypes_and) && $v2["specid"]>0) {
							if (is_array($required_specifications[$v2["ptypeid"]]) && in_array($v2["specid"], $required_specifications[$v2["ptypeid"]]))
								continue;
							else
								$required_specifications[$v2["ptypeid"]][] = $v2["specid"];
						}
					}
				}
			}
		}

		#
		# Checking if found products are satisfied to requirements
		#
		if (is_array($slot_products) && is_array($required_specifications)) {
			foreach ($slot_products as $k=>$v) {
				$_flag = ($check_product_type_requirements_by_or == 'Y' ? false : true);
				if (is_array($v["classes"]) && !empty($v["classes"])) {
					foreach ($v["classes"] as $_pclass) {
	
						if (!is_array($required_specifications[$_pclass["ptypeid"]]))
							continue;
				
						$spec_query = " AND specid IN (".implode(",", $required_specifications[$_pclass["ptypeid"]]).")";
						$check_requirements = func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[pconf_class_specifications] WHERE classid='$_pclass[classid]' $spec_query");
				
						$_flag_tmp = ($check_requirements == count($required_specifications[$_pclass["ptypeid"]]));
						$_flag = ( $check_product_type_requirements_by_or == 'Y' ? $_flag || $_flag_tmp : $_flag && $_flag_tmp);

					}
				}

				if ($_flag) {
					$products[] = $v;
				}
			}

			$slot_products = $products;
		}

		#
		# Checking if slotted products are satisfied reqs of the found products
		# if no - found product removed from search results
		#
		if ($check_bidirectional_requirements == "Y" && is_array($slot_products) && is_array($filled_slots)) {
			#
			# Get the requirements list
			#
			foreach ($slot_products as $k=>$v) {
				$required_specifications = array();
				$classes = func_query("SELECT * FROM $sql_tbl[pconf_products_classes] WHERE productid='".$v["productid"]."'");
				if (is_array($classes)) {
					foreach ($classes as $k1=>$v1) {
						$requirements = func_query("SELECT * FROM $sql_tbl[pconf_class_requirements] WHERE classid='$v1[classid]' ORDER BY ptypeid");
						if (is_array($requirements)) {
							foreach ($requirements as $k2=>$v2) {
								if ($v2["specid"]>0) {
									$required_specifications[$v2["ptypeid"]][] = $v2["specid"];
								}
							} # /foreach
						}
					} # /foreach
				}

				#
				# Checking found product...
				#
				$flag = true;
				if (is_array($required_specifications)) {
					foreach ($required_specifications as $t_ptypeid=>$t_specs) {
						foreach ($found_products_specs as $t_prodtypes) {
							if (!is_array($t_prodtypes) || empty($t_prodtypes[$t_ptypeid]))
								continue;

							foreach ($t_specs as $t_specid)
								if (!in_array($t_specid, $t_prodtypes[$t_ptypeid])) {
									$flag = false;
							}
						}
					}
				}

				if ($flag)
					$slot_products_2[] = $v;
			} # /foreach
		}

		if (is_array($slot_products_2)) {
			$slot_products = $slot_products_2;
			unset($slot_products_2);
		}

		$smarty->assign("filled_slots", $filled_slots);
	}

	if (!empty($active_modules["Product_Options"]) && is_array($slot_products)) {
		foreach ($slot_products as $k=>$v) {
			$slot_products[$k]["product_options_counter"] = func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[classes] WHERE productid='$v[productid]'");
		}
	}

	if (is_array($slot_products)) {
		#
		# Navigation pages
		#
		$total_items_in_search =  count($slot_products);
		$objects_per_page = $config["Appearance"]["products_per_page"];
		$total_nav_pages = ceil($total_items_in_search/$objects_per_page)+1;

		include $xcart_dir."/include/navigation.php";

		$smarty->assign("navigation_script","pconf.php?productid=$productid&slot=$slot");
		$slot_products = array_slice($slot_products, $first_page, $objects_per_page);
		foreach ($slot_products as $k=>$v) {
			$slot_products[$k]["taxes"] = func_get_product_taxes($slot_products[$k], $login);
		}
	}

	$location[] = array(func_get_langvar_by_name("lbl_pconf_select_the_n", array("slot" => $current_slot['slot_name']), false, true),"");
	$smarty->assign("slot_products", $slot_products);
	$smarty->assign("rules_by_or", $rules_by_or);
	$smarty->assign("slot_data", $current_slot);
	$smarty->assign("slot", $slot);
}

$smarty->assign("wizards", $wizards);
$smarty->assign("wizard_data", $current_step);
$smarty->assign("step", $current_stepid);
$smarty->assign("previous_step", $previous_step);

?>
