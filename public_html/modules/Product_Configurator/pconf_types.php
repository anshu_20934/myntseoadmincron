<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: pconf_types.php,v 1.14 2006/01/11 06:56:16 mclap Exp $
#

if ( !defined('XCART_SESSION_START') ) { header("Location: ../../"); die("Access denied"); }

x_load('backoffice');

x_session_register("ptypes_per_page", 10);

if ($REQUEST_METHOD == "POST") {

	if ($action == "update_page") {
		if (!empty($types_per_page1))
			$ptypes_per_page = intval($types_per_page1);
		elseif (!empty($types_per_page2))
			$ptypes_per_page = intval($types_per_page2);

		if (empty($ptypes_per_page))
			$ptypes_per_page = 10;

		func_header_location("pconf.php?mode=types");
	}

	if (is_array($posted_types)) {
		#
		# Update existing product types
		#
		foreach ($posted_types as $k=>$v) {
			if (!empty($v["delete"]) && $action == "delete") {
				#
				# Delete product type and all related information
				#
				db_query("DELETE FROM $sql_tbl[pconf_product_types] WHERE ptypeid='$k' $provider_condition");
				db_query("DELETE FROM $sql_tbl[pconf_specifications] WHERE ptypeid='$k'");
				db_query("DELETE FROM $sql_tbl[pconf_slot_rules] WHERE ptypeid='$k'");
				$classids = func_query("SELECT classid FROM $sql_tbl[pconf_products_classes] WHERE ptypeid='$k'");
				if (is_array($classids)) {
					foreach ($classids as $k2=>$v2) {
						db_query("DELETE FROM $sql_tbl[pconf_class_specifications] WHERE classid='$v2[classid]'");
						db_query("DELETE FROM $sql_tbl[pconf_class_requirements] WHERE classid='$v2[classid]'");
					}
				}

				db_query("DELETE FROM $sql_tbl[pconf_products_classes] WHERE ptypeid='$k'");
				continue;
			}
			elseif ($action != "delete") {
				$query_data = array(
					"orderby" => $v['orderby']
				);
				if ($shop_language == $config['default_admin_language'])
					$query_data['ptype_name'] = $v['ptype_name'];

				func_array2update("pconf_product_types", $query_data, "ptypeid='$k' ".$provider_condition);
				func_languages_alt_insert("ptype_name_".$k, $v['ptype_name'], $shop_language);
			}

			if (is_array($v["specifications"])) {
				#
				# Update specifications for this product type
				#
				foreach ($v["specifications"] as $k1=>$v1) {
					if (!empty($v1["delete"]) && $action == "delete") {
						db_query("DELETE FROM $sql_tbl[pconf_specifications] WHERE specid='$k1'");
						continue;
					}
					elseif ($action != "delete") {
						db_query("UPDATE $sql_tbl[pconf_specifications] SET spec_name='$v1[spec_name]', orderby='$v1[orderby]' WHERE specid='$k1'");
					}
				}
			}

			if ($action != "delete" && !empty($v["new_specification"]) && func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[pconf_specifications] WHERE ptypeid='$k' AND spec_name='$v[new_specification]'") == 0)
				db_query("INSERT INTO $sql_tbl[pconf_specifications] (ptypeid, spec_name) VALUES ('$k', '$v[new_specification]')");
		}
	}

	if ($action != "delete" && !empty($new_type)) {
		#
		# New product type
		#
		if (func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[pconf_product_types] WHERE ptype_name='$new_type' $provider_condition") == 0)
			db_query("INSERT INTO $sql_tbl[pconf_product_types] (provider, ptype_name) VALUES ('$login', '$new_type')");
	}

	func_header_location("pconf.php?mode=types".($page?"&page=$page":""));
} #/ if ($REQUEST_METHOD == "POST")

#
# Get the prodict types information
#
$total_items_in_search = func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[pconf_product_types] WHERE 1 $provider_condition");

$objects_per_page = $ptypes_per_page;

$total_nav_pages = ceil($total_items_in_search/$objects_per_page)+1;
require $xcart_dir."/include/navigation.php";

$smarty->assign("navigation_script", "pconf.php?mode=types");
$smarty->assign("ptypes_per_page", $ptypes_per_page);

$product_types = func_query("SELECT * FROM $sql_tbl[pconf_product_types] WHERE 1 $provider_condition ORDER BY orderby, ptype_name LIMIT $first_page, $objects_per_page");

if (is_array($product_types)) {
	foreach ($product_types as $k=>$v) {
		$product_types[$k]["specifications"] = func_query("SELECT * FROM $sql_tbl[pconf_specifications] WHERE ptypeid='$v[ptypeid]' ORDER BY orderby, spec_name");
		$tmp = func_get_languages_alt("ptype_name_".$v['ptypeid'], $shop_language);
		if (!empty($tmp))
			$product_types[$k]['ptype_name'] = $tmp;
	}
}

$smarty->assign("product_types", $product_types);

$smarty->assign("mode", "types");
?>
