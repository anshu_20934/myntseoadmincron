<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: pconf_search.php,v 1.17 2006/01/11 06:56:16 mclap Exp $
#

if ( !defined('XCART_SESSION_START') ) { header("Location: ../../"); die("Access denied"); }

x_load('product');

x_session_register("pconf_search_data");


if ($REQUEST_METHOD == "POST") {
#
# Update the search_data
#
	if (is_array($post_data)) {
		foreach ($post_data as $k=>$v) {
			if (is_array($v))
				$pconf_search_data[$k] = $v;
			else
				$pconf_search_data[$k] = trim(stripslashes($v));
		}
		if (empty($post_data["search_in_subcategories"]))
			$pconf_search_data["search_in_subcategories"] = "";

		#
		# This actions need to avoid the lost of previous seletions
		#
		if ($pconf_search_data["product_status"] != "C") {
			if (empty($post_data["no_product_type"])) {
				$pconf_search_data["no_product_type"] = "";
				if (empty($post_data["product_type"]))
					$pconf_search_data["product_type"] = "";
			}
		}
	}
	
	func_header_location("pconf.php?mode=search&action=go");
} #/ if ($REQUEST_METHOD == "POST")

if ($action == "go" and is_array($pconf_search_data)) {
#
# Perform the search
#
	foreach ($pconf_search_data as $k=>$v) {
		$search_data[$k] = is_array($v) ? $v : addslashes($v);
	}

	$old_search_data = $search_data["products"];

	$search_data["products"] = array(
		"productid" => $search_data['productid'],
		"substring" => $search_data['substring'],
		"categoryid" => $search_data['categoryid'],
		"search_in_subcategories" => $search_data['search_in_subcategories'],
		"category_main" => "Y"
	);

	# Search for product types
	if (!empty($search_data["product_status"])) {
		$search_data["products"]['_']['where'] = array("$sql_tbl[products].product_type='".$search_data["product_status"]."'");
	}

	$search_data["products"]['_']['left_joins']['ppc'] = array(
		"tblname" => "pconf_products_classes",
		"on" => "$sql_tbl[products].productid = ppc.productid"
	);
	$search_data["products"]['_']['fields'] = array("COUNT(ppc.productid) as types_count");

	# If search for non configurable products
	if ($search_data["product_status"] != 'C') {

		if (!empty($search_data["no_product_type"])) {
			# Search for products with no any product types assigned

			$search_data["products"]['_']['fields'][] = "COUNT($sql_tbl[pconf_products_classes].productid) as counter";
			$search_data["products"]['_']['fields_count'][] = "COUNT($sql_tbl[pconf_products_classes].productid) as counter";
			$search_data["products"]['_']['left_joins']['pconf_products_classes'] = array(
				"on" => "$sql_tbl[products].productid = $sql_tbl[pconf_products_classes].productid"
			);
			$search_data["products"]['_']['having'] = array("counter = 0");

		} elseif (!empty($search_data["product_type"])) {
			# Search for products with specified product types assigned

			$search_data["products"]['_']['inner_joins']['pconf_products_classes'] = array(
				"on" => "$sql_tbl[products].productid = $sql_tbl[pconf_products_classes].productid AND $sql_tbl[pconf_products_classes].ptypeid IN (".implode(",", $search_data["product_type"]).")"
			);
		}
	}

	$REQUEST_METHOD = "GET";
	$mode = "search";
	include $xcart_dir."/include/search.php";

	$search_data["products"] = $old_search_data;
	x_session_save("search_data");

	$smarty->assign("navigation_script","pconf.php?mode=search&action=go");

} else {
	include $xcart_dir."/include/categories.php";
}

#
# Get the prodict types information
#
$product_types = func_query("SELECT * FROM $sql_tbl[pconf_product_types] WHERE 1 $provider_condition ORDER BY orderby, ptype_name");

if (is_array($product_types)) {
	if (!empty($search_data["product_type"])) {
		foreach ($product_types as $k=>$v)
			foreach ($search_data["product_type"] as $k1=>$v1) {
				if ($v["ptypeid"] == $v1)
					$product_types[$k]["selected"] = 1;
			}
	}
	$smarty->assign("product_types", $product_types);
}

$smarty->assign("search_data", $pconf_search_data);

$smarty->assign("mode", "search");
?>
