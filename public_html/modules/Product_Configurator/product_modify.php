<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: product_modify.php,v 1.24.2.5 2006/07/31 06:38:44 max Exp $
#

if ( !defined('XCART_SESSION_START') ) { header("Location: ../../"); die("Access denied"); }

if (empty($active_modules["Product_Configurator"]) || ($REQUEST_METHOD == "POST" && $mode != "pconf"))
	return;

x_load('backoffice','category','product');

if ((!empty($productid) and $product_info["product_type"] == "C") || $mode == "pconf") {
	$old_mode = $mode;
	$mode = "pconf";
	$smarty->assign("product", $product_info);
	$smarty->assign("main", "product_configurator");
	$smarty->assign("main_mode", "manage");
	$is_pconf = true;
}

if ($edit == "wizard") {
	include $xcart_dir."/modules/Product_Configurator/pconf_wizard_modify.php";
	$smarty->assign("mode", "wizard");
}
elseif ($edit == "slot") {
	include $xcart_dir."/modules/Product_Configurator/pconf_slot_modify.php";
	$smarty->assign("mode", "slot");
}
else
	$smarty->assign("mode", "product_modify");

#
# Add/modify Configurable product
#
if ($REQUEST_METHOD == "POST" && $mode == "pconf") {

	$fillerror = empty($categoryid);
	$fillerror = ($fillerror || empty($product));
	$fillerror = ($fillerror || empty($descr));
	$sku_is_exist = (func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[products] WHERE productcode='$productcode' AND productid!='$productid' AND provider = '".addslashes($login_type == "A" ? $provider : $login)."'") ? true : false);
	$fillerror = ($fillerror || $sku_is_exist);

    $isnt_perms_T = func_check_image_storage_perms($file_upload_data, "T");
    $isnt_perms_P = func_check_image_storage_perms($file_upload_data, "P");
	$fillerror = ($fillerror || $isnt_perms_T !== true);
	$fillerror = ($fillerror || $isnt_perms_P !== true);



	if (!$fillerror) {
		if ($productid == "") {
		#
		# New product
		#
			db_query("INSERT INTO $sql_tbl[products] (productcode, provider, add_date, product_type) VALUES ('$productcode', '$login', '".time()."', 'C')");

			$productid = db_insert_id();

			#
			# Insert zero price for configurable product
			#
			db_query("INSERT INTO $sql_tbl[pricing] (productid, quantity, price) VALUES ('$productid', '1', '0')");

			if (func_check_image_posted($file_upload_data, "T")) {
				func_save_image($file_upload_data, "T", $productid);
			}
			if (func_check_image_posted($file_upload_data, "P")) {
				func_save_image($file_upload_data, "P", $productid);
			}
		}
		else {
		#
		# Update existing product
		#
			if (func_check_image_posted($file_upload_data, "T")) {
				func_save_image($file_upload_data, "T", $productid);
			}
			if (func_check_image_posted($file_upload_data, "P")) {
				func_save_image($file_upload_data, "P", $productid);
			}

			if($fields['thumbnail'] == 'Y' && $geid) {
				$img = func_addslashes(func_query_first("SELECT * FROM $sql_tbl[images_T] WHERE id = '$productid'"));
				unset($img['imageid']);
				while ($pid = func_ge_each($geid, 1, $productid)) {
					$img['id'] = $pid;
					func_array2insert($sql_tbl['images_T'], $img, true);
				}
			}

			if($fields['product_image'] == 'Y' && $geid) {
				$img = func_addslashes(func_query_first("SELECT * FROM $sql_tbl[images_P] WHERE id = '$productid'"));
				unset($img['imageid']);
				while ($pid = func_ge_each($geid, 1, $productid)) {
					$img['id'] = $pid;
					func_array2insert($sql_tbl['images_P'], $img, true);
				}
			}
		}

		# For existing product: get the categories list before updating
		if ($product_info) {
			$_old_product_categories = func_query("SELECT categoryid FROM $sql_tbl[products_categories] WHERE productid='$productid'");
			if (is_array($_old_product_categories)) {
				foreach ($_old_product_categories as $k=>$v)
					$old_product_categories[] = $v["categoryid"];
			}
		}

		# Prepare and update categories associated with product...
		if (!func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[products_categories] WHERE categoryid = '$categoryid' AND productid = '$productid' AND main = 'Y'")) {
			db_query("DELETE FROM $sql_tbl[products_categories] WHERE productid = '$productid' AND (main = 'Y' OR categoryid = '$categoryid')");
			db_query("INSERT INTO $sql_tbl[products_categories] (categoryid, productid, main) VALUES ('$categoryid', '$productid', 'Y')");
		}
		if ($geid && $fields['categoryid']) {
			while($pid = func_ge_each($geid, 1, $productid)) {
				if (!func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[products_categories] WHERE categoryid = '$categoryid' AND productid = '$pid' AND main = 'Y'")) {
					db_query("DELETE FROM $sql_tbl[products_categories] WHERE productid = '$pid' AND (main = 'Y' OR categoryid = '$categoryid')");
					db_query("INSERT INTO $sql_tbl[products_categories] (categoryid, productid, main) VALUES ('$categoryid', '$pid', 'Y')");
				}
			}
		}

		if (!empty($categoryids)) {
			foreach ($categoryids as $k=>$v) {
				if(!func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[products_categories] WHERE categoryid = '$v' AND productid = '$productid'")) {
					db_query("INSERT INTO $sql_tbl[products_categories] (categoryid, productid, main) VALUES ('$v', '$productid', 'N')");
				}
				if ($geid && $fields['categoryids']) {
					while($pid = func_ge_each($geid, 1, $productid)) {
						if (!func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[products_categories] WHERE categoryid = '$v' AND productid = '$pid'")) {
							db_query("INSERT INTO $sql_tbl[products_categories] (categoryid, productid, main) VALUES ('$v', '$pid', 'N')");
						}
					}
				}
			}
			db_query("DELETE FROM $sql_tbl[products_categories] WHERE productid = '$productid' AND main != 'Y' AND categoryid NOT IN ('".implode("','", $categoryids)."')");
			if ($geid && $fields['categoryids']) {
				while($pid = func_ge_each($geid, 100, $productid)) {
					db_query("DELETE FROM $sql_tbl[products_categories] WHERE productid IN ('".implode("','",$pid)."') AND main != 'Y' AND categoryid NOT IN ('".implode("','", $categoryids)."')");
				}
			}
		} else {
			db_query("DELETE FROM $sql_tbl[products_categories] WHERE productid = '$productid' AND main != 'Y'");
			if ($geid && $fields['categoryids']) {
				while($pid = func_ge_each($geid, 100, $productid)) {
					db_query("DELETE FROM $sql_tbl[products_categories] WHERE productid IN ('".implode("','",$pid)."') AND main != 'Y'");
				}
			}
		}

		db_query("UPDATE $sql_tbl[products] SET product='$product', descr='$descr', fulldescr='$fulldescr', avail='1', list_price='0.00', weight='0', productcode='$productcode', forsale='$forsale', min_amount='$min_amount' WHERE productid='$productid'");

		# Group editing of products functionality
		if ($geid && !empty($fields)) {
			$query_data = array();
			foreach($fields as $k => $v) {
				if (!in_array($k, array("efields", "price", "thumbnail", "product_image", "categoryid", "categoryids", "taxes", "membershipids"))) {
					$query_data[$k] = $$k;
				}
			}
			if (!empty($query_data)) {
				while ($pid = func_ge_each($geid, 100, $productid)) {
					func_array2update("products", $query_data, "productid IN ('".implode("','", $pid)."')");
				}
			}
		}

		# Update categories data cache
		if (!empty($active_modules['Fancy_Categories'])) {

			if (!is_array($old_product_categories) || empty($old_product_categories)) {

				# Update in productid-based mode (new product)
				$cats = func_fc_check_rebuild($productid, "P");
				if (!empty($cats))
					func_fc_build_categories($cats, 10);

			} else {

				# Update old product categories
				if ($config['Appearance']['count_products'] == 'Y') {
					$_categoryids = func_array_merge($categoryids, array($categoryid));
					$diff = array_diff($old_product_categories, $_categoryids);
					if (!empty($diff)) {
						$cats = func_fc_check_rebuild($old_product_categories);
						if (!empty($cats))
							func_fc_build_categories($cats, 10);
					}
				}
				$cats = func_fc_check_rebuild($productid, "P");
				if (!empty($cats))
					func_fc_build_categories($cats, 10);
			}
		}

		#
		# Update products counter for selected categories
		#
		if (is_array($old_product_categories))
			$categoryids = func_array_merge($old_product_categories, $categoryids);
        $categoryids = func_array_merge($categoryids, array($categoryid));
		func_recalc_product_count(func_get_category_parents($categoryids));

		if ($active_modules["Extra_Fields"]) {
			include $xcart_dir."/modules/Extra_Fields/extra_fields_modify.php";
		}

		if ($active_modules["Manufacturers"]) {
			@include $xcart_dir."/modules/Manufacturers/product_manufacturer.php";
		}

		func_build_quick_flags($productid);
		func_build_quick_prices($productid);

	} else {

		$top_message = array(
			"type" => "E"
		);
		if ($isnt_perms_T !== true) {
			$top_message['content'] = $isnt_perms_T['content'];

		} elseif ($isnt_perms_P !== true) {
			$top_message['content'] = $isnt_perms_P['content'];

		} elseif ($sku_is_exist) {
			$top_message['content'] = func_get_langvar_by_name("msg_adm_err_sku_exist");
			$top_message["fillerror"] = true;

		} else {
			$top_message['content'] = func_get_langvar_by_name("msg_adm_err_product_upd");
			$top_message["fillerror"] = true;
		}

		$product_modified_data = $HTTP_POST_VARS;
		foreach ($product_modified_data as $k=>$v)
			$product_modified_data[$k] = stripslashes($v);
		$product_modified_data["productid"] = $productid;

	}

	func_refresh("", "&mode=pconf");
}

if ($mode == "pconf") {
	if (!empty($product_info))
		$pconf_title = func_get_langvar_by_name("lbl_adm_product_management");
	else
		$pconf_title = func_get_langvar_by_name("lbl_adm_add_product");

	$smarty->assign("pconf_title", $pconf_title);
}

if (!empty($old_mode))
	$mode = $old_mode;

if ($mode == "pconf" || $product_info["product_type"] == "C") {
#
# Define data for the navigation within section
#
	$dialog_tools_data = array();

	$dialog_tools_data["left"][] = array("link" => $pm_link, "title" => func_get_langvar_by_name("lbl_product_details"));

	if (!empty($product_info)) {

		if (!empty($avail_languages))
			$dialog_tools_data["left"][] = array("link" => $pm_link."&section=lng", "title" => func_get_langvar_by_name("txt_international_descriptions"));
		if (!empty($active_modules["Product_Options"])) {
			$dialog_tools_data["left"][] = array("link" => $pm_link."&section=options", "title" => func_get_langvar_by_name("lbl_product_options"));
			$dialog_tools_data["left"][] = array("link" => $pm_link."&section=variants", "title" => func_get_langvar_by_name("lbl_product_variants"));
		}
		if (!empty($active_modules["Upselling_Products"]))
			$dialog_tools_data["left"][] = array("link" => $pm_link."&section=upselling", "title" => func_get_langvar_by_name("lbl_upselling_links"));
		if (!empty($active_modules["Detailed_Product_Images"]))
			$dialog_tools_data["left"][] = array("link" => $pm_link."&section=images", "title" => func_get_langvar_by_name("lbl_detailed_images"));
		if (!empty($active_modules["Customer_Reviews"]))
			$dialog_tools_data["left"][] = array("link" => $pm_link."&section=reviews", "title" => func_get_langvar_by_name("lbl_customer_reviews"));
		if (!empty($active_modules["Manufacturers"]))
			$dialog_tools_data["left"][] = array("link" => $pm_link."&section=manufacturer", "title" => func_get_langvar_by_name("lbl_manufacturer"));

		$dialog_tools_data["left"][] = array("separator"=>"Y");

		$dialog_tools_data["left"][] = array("link" => $pm_link."&mode=pconf&edit=wizard", "title" => func_get_langvar_by_name("lbl_pconf_manage_confwiz"));

	}

	$dialog_tools_data["right"][] = array("link" => "search.php", "title" => func_get_langvar_by_name("lbl_search_products"));
	$dialog_tools_data["right"][] = array("link" => "product_modify.php", "title" => func_get_langvar_by_name("lbl_add_product"));
	if ($current_area == "A" or !empty($active_modules["Simple_Mode"]))
		$dialog_tools_data["right"][] = array("link" => $xcart_catalogs["admin"]."/categories.php", "title" => func_get_langvar_by_name("lbl_categories"));
	if (!empty($active_modules["Manufacturers"]))
		$dialog_tools_data["right"][] = array("link" => "manufacturers.php", "title" => func_get_langvar_by_name("lbl_manufacturers"));
	$dialog_tools_data["right"][] = array("link" => "orders.php", "title" => func_get_langvar_by_name("lbl_orders"));

	$dialog_tools_data["right"][] = array("separator"=>"Y");

	$dialog_tools_data["right"][] = array("link" => "pconf.php?mode=search", "title" => func_get_langvar_by_name("lbl_pconf_search"));
	$dialog_tools_data["right"][] = array("link" => "product_modify.php?mode=pconf", "title" => func_get_langvar_by_name("lbl_pconf_confproduct"));
	$dialog_tools_data["right"][] = array("link" => "pconf.php?mode=types", "title" => func_get_langvar_by_name("lbl_pconf_define_types"));
	$dialog_tools_data["right"][] = array("link" => "pconf.php?mode=about", "title" => func_get_langvar_by_name("lbl_pconf_about"));

}

?>
