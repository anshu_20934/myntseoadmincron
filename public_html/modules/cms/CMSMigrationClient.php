<?php

include_once ("$xcart_dir/modules/RestAPI/RestRequest.php");

class CMSMigrationClient {
    public static function bulkRemigrate($styles){
        $url = HostConfig::$cmsServiceHost.'/catalog/migration/';
        $returnObject=null;
        $headers = array();
        array_push($headers, 'Accept:  application/json');
        array_push($headers,'Content-Type: application/json');
        array_push($headers,'Authorization: Basic YXBpYWRtaW46bTFudHJhUjBja2V0MTMhIw==');
        $request = new RestRequest($url, "POST");
        $request->setHttpHeaders($headers);
        $stylesStrArr = array();
        foreach ($styles as $eachStyle) {
            array_push($stylesStrArr,"{ \"id\" : ".$eachStyle.", \"action\" : \"remigrate\", \"updatedBy\" : \"x5admin\", \"comments\" : \"x5admin\" }");
        }
        $body = implode(",", $stylesStrArr);
        $body = "[".$body."]";
        $request->buildPostBody($body);
        
        $request->execute();
        $responseInfo = $request->getResponseInfo();
        $responseBodyJson = $request->getResponseBody();
        $responseBody = json_decode($responseBodyJson);
        if($responseInfo['http_code'] == 200 &&  isset($responseBody->status) && $responseBody->status->statusType == 'SUCCESS'){
            global $portalapilog;
            $portalapilog->debug('CMSMigrationClient - remigration done:--'.print_r($responseBody,true));
            return true;
        }
        else{
            global $portalapilog;
            $portalapilog->debug('CMSMigrationClient - remigration failed:--'.print_r($responseBody,true));
            return false;
        }
    }
}
?>