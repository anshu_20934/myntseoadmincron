<?php
namespace feedback\instance;
/**
 * The class exposes abstract APIs for handling Service Request FB Instance
 * @author: Arun Kumar K
 * legends:
 *      MFB - Myntra FeedBack
 *      MFBSR - Myntra FeedBack Service Request - FB related to Service Request 
 */
global $xcart_dir;
require_once($xcart_dir."/include/class/class.mail.multiprovidermail.php");
require_once($xcart_dir."/modules/serviceRequest/ServiceRequest.php");

use feedback\manage\MFB;

class MFBSRInstance extends MFBInstance{

    const MFB_INTERVAL = 0;

/*
     * checks for any FB mail sent in last MFB_INTERVAL days
     * @param:(int)$MFBId
     * @param:(string)$email
     * @return: (bool)
     */
    public function isMFBIntervalSatisfiesCustomer($MFBId, $customerEmail){
        $lastSentDate = $this->getMFBInstanceLastSentDateForCustomer($MFBId, $customerEmail);
        $dayDifference = floor((time()-$lastSentDate)/86400);        
        return ($dayDifference>=self::MFB_INTERVAL);
    }   


    /*
     * adds HTML <form> to the MFB questionnaire
     * @param:(string)$linkInEmail - <form> action url
     * @param:(int)$SRId - service request id
     * @return: (bool)
     */
    public function sendSRMFBMail($linkInEmail, $SRId){        

        //get SR detail based on SR id
        $SR = new \ServiceRequest();
        $SRInfo = $SR->getSR(array('fSRid'=>$SRId));

        if(!empty($SRInfo[0]['customer_email'])){
            $customerEmail = $SRInfo[0]['customer_email'];
        }
        if(!empty($SRInfo[0]['order_customer_email'])){
            $customerEmail = $SRInfo[0]['order_customer_email']; 
        }
        
        //do not send mail if email not exists
        if(empty($customerEmail)){
            return; 
        }

        //get the current selected SR FB
        $MFB = new MFB();
        $MFBInfo = $MFB->getMFBByName("SR");
        $MFBId = $MFBInfo[0]['feedbackid'];

        //check MFB interval of 30 days
        $MFBIntervalTestPass = $this->isMFBIntervalSatisfiesCustomer($MFBId, $customerEmail);

        if($MFBIntervalTestPass && !empty($SRInfo[0]['closed_date']) && !empty($SRInfo[0]['created_date'])){

            //get SR FB form
            $MFB_HTML = $MFB->getMFBActiveQuestionnaire($MFBId);

            //add this SR FB instance into DB
            $addMFBInstance = $this->addMFBInstance($MFBId , $SRInfo[0]['SR_id'], $customerEmail, "SR");

            //send feedback only then when instance is captured
            if($addMFBInstance !== false){
                
                //wrap MFB HTML questionnaire with <form>
                //and hidden fields required to send with order FB mail
                $MFBInstance = $this->getMFBInstances(array("instanceid"=>$addMFBInstance));
                $formHead = $MFBInfo[0]['title'];
                $MFB_HTML = $this->wrapMFBHTMLQuestionnaire($MFB_HTML['questionnaireHTML'], $linkInEmail, $addMFBInstance,
                                                                $MFBInstance[0]['authentic_key'], $formHead);

                //alternative MFB link to submit the form
                $MFBParams = array(
                    "allowCrossDomainRequest"   => 1,
                    "MFBInstanceId"             => $addMFBInstance,
                    "MFBInstanceAuthKey"        => $MFBInstance[0]['authentic_key']
                );
                $MFBLink = $linkInEmail."?".http_build_query($MFBParams);

                //email template for MFB order
                $template = "voc_SR";

                //send order MFB mail as (type-critical)                
                $bodyArgs = array(
                            "USER"				=> ($SRInfo[0]['first_name'])?$SRInfo[0]['first_name']:"Myntra Customer",
                            "SR_CREATE_DATE"    => date('d-F-Y',$SRInfo[0]['createtime']),
                            "SR_CLOSED_DATE"	=> date('d-F-Y',$SRInfo[0]['closetime']),
                            "SR_MFB_FORM"	    => $MFB_HTML,
                            "REDIRECT_INFO"     => '<p>Please click on the "Submit" button once you are done. '.
		    					'If you are unable to submit your feedback, please click <a href="'.$MFBLink.'" target="_blank">here</a></p>',		    					
                            "DECLARATION"       => "This mail is intended only for $customerEmail"
                            );

                $mail_details = array(
                        "template" => $template,
                        "to" => $customerEmail,
                        "bcc" => "myntramailarchive@gmail.com",
                        "header" => 'header',
                        "footer" => 'footer',
                        "mail_type" => \MailType::CRITICAL_TXN
                    );
                $multiPartymailer = new \MultiProviderMailer($mail_details, $bodyArgs);
                $flag = $multiPartymailer->sendMail();

                if($flag === false){
                    //delete MFB instance in case of mail failure so that to send order MFB mail again later                    
                    $this->deleteMFBInstance($addMFBInstance);
                }
            }
        }
    }

}
?>