<?php
namespace feedback\instance;

include_once($xcart_dir."/include/func/func.db.php");
include_once($xcart_dir."/include/func/func.utilities.php");

use feedback\manage\MFB;


/**
 * The classs exposes abstract APIs for handling feedback instance(creating, updating and sending, receiving FB(feedback) mails
 * and sending FB survey mails).
 * @author: Arun Kumar K
 * legends:
 *      MFB - Myntra FeedBack
 */

class MFBInstance{

    const MFB_INSTANCE_TABLE = "mk_feedback_instance";
    const MFB_INSTANCE_DATA_TABLE = "mk_feedback_instance_data";
    const MFB_INSTANCE_SPAN = 30;//days
    /*
     * get the last MFB instance sentdate for the customer based on MFB type
     * @param:(int)$MFBId - MFB id
     * @param:(string)$email
     * @return:(int)$date - in unix timestamp
     */
    public function getMFBInstanceLastSentDateForCustomer($MFBId, $customerEmail){
        $lastSentDate = func_query_first_cell("select
                                    max(sent_date)
                                from
                                    ".self::MFB_INSTANCE_TABLE."
                                where
                                    feedback_id=".sanitizeDBValues($MFBId)."
                                and
                                    customer_email='".sanitizeDBValues($customerEmail)."'");

        return $lastSentDate;
    }

    /* generates MFB auth key
     * @return:(string)$MFBauthKey
     */
    public function generateMFBAuthKey(){
        $MFBAuthKey = md5(rand().time());
        return $MFBAuthKey;
    }

    /* validates MFB against MFBinstanceId, auth key, received date, expiry date in DB
     * @param:(int)$MFBInstanceId
     * @param:(string)$MFBAuthKey
     * @return:(bool)
     */
    public function isValidMFB($MFBInstanceId, $MFBAuthKey){
        $MFBInstance = $this->getMFBInstances(array("instanceid"=>$MFBInstanceId));

        return ($MFBInstance[0]['authentic_key']==$MFBAuthKey && $MFBInstance[0]['expiry_date']>=time());
    }

    /* validates if NPS captured or not by looking at received_date
     * @param:(int)$MFBInstanceId     
     * @return:(bool)
     */
    public function isNpsCaptured($MFBInstanceId){
    	$MFBInstance = $this->getMFBInstances(array("instanceid"=>$MFBInstanceId));
    
    	return (!empty($MFBInstance[0]['received_date']));
    }
    
    
    /* check whether instance data already added for a feedback in DB
     * @param:(int)$MFBInstanceId
     * @return:(bool)
     */
    public function isInstanceDataNotCaptured($MFBInstanceId){
    	//check the count of data for the instance
        //if count =0, means no need to add anymore instance data
        // and if count=1, means first data to add is NPS data, which we would have 
        $dataCountForInstance = $this->getMFBInstanceDataCount($MFBInstanceId);
        if($dataCountForInstance < 2){
            return true;
        }
        return false;
    }
    
    /*
     * adds the feedback instance
     * @param:(int)$MFBId - feedback id
     * @param:(int)$MFBReference - an integeer id as reference(eg orderid,SRid)
     * @param:(string)$customerEmail - email to whom MFB is sent
     * @param:(string)$MFBReferenceType - explains what sort of reference is $MFBReference
     * @return:(int)$MFBInstanceId, (bool)false - on failure
     */
    public function addMFBInstance($MFBType , $MFBReference, $customerEmail, $MFBReferenceType=null){

        if(empty($MFBType) || empty($MFBReference) || empty($customerEmail)){
            return false;
        }

        //generate random auth key
        $MFBAuthKey = $this->generateMFBAuthKey();

        $queryData = array(
            'feedback_id'=>$MFBType,
            'feedback_reference'=>$MFBReference,
        	'reference_type'=>$MFBReferenceType,	
            'sent_date'=>time(),
            'expiry_date'=>time()+(self::MFB_INSTANCE_SPAN*86400),//30 days
            'customer_email'=>$customerEmail,
            'authentic_key'=>$MFBAuthKey
        );
        return func_array2insert(self::MFB_INSTANCE_TABLE,sanitizeDBValues($queryData));
    }

    
    /*
     * updates MFB instance
     * @param:(int)$MFBInstanceId
     * @return:(bool)
     */
    public function setMFBInstance($MFBInstanceId){        
        $queryData = array("received_date"=>time());
        $condition = "instanceid = '".sanitizeDBValues($MFBInstanceId)."'";
        $flag = func_array2update(self::MFB_INSTANCE_TABLE, sanitizeDBValues($queryData),$condition);
        return $flag;
    }

    /*
     * gets the MFB Instance
     * @param:(array)$filter - can be different columns of MFB_INSTANCE_TABLE
     * @param:(array)$order - can be ordered by columns of MFB_INSTANCE_TABLE
     * @return:(array)$MFBInstance - FB instances(generated from different FB type) of different feedback types
     */
    public function getMFBInstances($filter,$order=null){
        $sql = "select
                    *
                from
                    " . self::MFB_INSTANCE_TABLE;

        $MFBInstances = sanitizedQueryBuilder($sql,$filter,$order);
        return $MFBInstances;
    }

    /*
     * adds the feedback instance data(response of feedback)
     * @param:(int)$MFBInstanceId
     * @param:(array)$selectedQuestionOptionArray - array of selected questions and its answers
     * @return:(bool)false/true 
     */
    public function addMFBInstanceData($MFBInstanceId, $selectedQuestionOptionArray, $NPS=false){

        //check the count of data for the instance
        if(empty($NPS)){
        	//if count >0, means no need to add anymore instance data
        	$dataCountForInstance = $this->getMFBInstanceDataCount($MFBInstanceId);
        	if($dataCountForInstance > 0){
        		return true;
        	} 	
        } else {
        	//if count >1, means no need to add anymore instance data
        	// because first data to add is NPS data, which we would have
        	$dataCountForInstance = $this->getMFBInstanceDataCount($MFBInstanceId);
        	if($dataCountForInstance > 1){
        		return true;
        	}
        }
         
       

        if(empty($selectedQuestionOptionArray) || !is_array($selectedQuestionOptionArray)){
            return false;
        }

        //get MFB type
        $MFB = new MFB();
        
        foreach($selectedQuestionOptionArray as $question=>$option){

            //get the question type
            $MFBQuestion = $MFB->MFBQ->getMFBQuestions(array("questionid"=>$question,"active"=>1));

            $optionNames = '';

            //if multiple options are selected store option_values as csv in DB
            if(is_array($option)){
                foreach($option as $key=>$optionId){
                    //get the option name
                    $MFBOption = $MFB->MFBQ->MFBOption->getMFBOptions(array("optionid"=>$optionId,"active"=>1));

                    //for text and text area take post value
                    if($MFBQuestion[0]['question_type'] == 'text' || $MFBQuestion[0]['question_type'] == 'textbox'){
                        $optionNames .= $optionId;
                        
                    } else {
                        $optionNames .= $MFBOption[0]['option_value'].",";
                    }


                }
                $optionNames = trim($optionNames, ',');                
                
            } else {                
                //get the option name
                $MFBOption = $MFB->MFBQ->MFBOption->getMFBOptions(array("optionid"=>$option,"active"=>1));
                $optionNames = $MFBOption[0]['option_value']; 

            }
            
            $queryData = array(
                'feedback_instance_id'=>$MFBInstanceId,
                'questionid'=>$question,
                'response'=>$optionNames
            );
            func_array2insert(self::MFB_INSTANCE_DATA_TABLE,sanitizeDBValues($queryData));
        }

        return true;
    }

    
    /*
     * adds the feedback instance data(response of feedback)
     * @param:(int)$MFBInstanceId
     * @param:(array)$selectedQuestionOptionArray - array of selected questions and its answers
     * @return:(bool)false/true
     */
    public function addMFBInstanceDataForNps($MFBInstanceId, $NPSDisplayorder){
    
    	//check the count of data for the instance
    	//if count >0, means no need to add anymore instance data    	
    	$dataCountForInstance = $this->getMFBInstanceDataCount($MFBInstanceId);
    	if($dataCountForInstance > 0){
    		return true;
    	}
    	
    	//get instance detail
    	$MFBInstance = $this->getMFBInstances(array("instanceid"=>$MFBInstanceId));
    	
    	//get MFB type
    	$MFB = new MFB();
    	$MFBQuestions = $MFB->getMFBActiveNpsQuestion($MFBInstance[0]['feedback_id']);
    	$MFBOption = $MFB->MFBQ->MFBOption->getMFBOptions(array("questionid"=>$MFBQuestions[0]["questionid"],"active"=>1, "display_order"=>$NPSDisplayorder));
    	
    	$queryData = array(
    			'feedback_instance_id'=>$MFBInstanceId,
    			'questionid'=>$MFBQuestions[0]["questionid"],
    			'response'=>$MFBOption[0]['option_value']
    	);
    	func_array2insert(self::MFB_INSTANCE_DATA_TABLE,sanitizeDBValues($queryData));
    	
    	return true;
    }
    
    
    /*
     * gets the MFB Instance data
     * @param:(array)$filter - can be different columns of MFB_INSTANCE_DATA_TABLE
     * @param:(array)$order - can be ordered by columns of MFB_INSTANCE_DATA_TABLE
     * @return:(array)$MFBInstanceData - feedback responses stored against questions
     */
    public function getMFBInstanceData($filter,$order=null){
        $sql = "select
                    *
                from
                    " . self::MFB_INSTANCE_DATA_TABLE;

        $MFBInstances = sanitizedQueryBuilder($sql,$filter,$order);
        return $MFBInstances;
    }

    /*
     * gets the MFB Instance data count on instance id
     * @param:(int)$MFBinstanceId
     * @return:(int)$count
     */
    public function getMFBInstanceDataCount($MFBInstanceId){

        $count = func_query_first_cell("select
                    count(*) as datacount
                from
                    ".self::MFB_INSTANCE_DATA_TABLE."
                where
                    feedback_instance_id = ".$MFBInstanceId) ;


        return $count;
    }
    
    /*
     * deletes instance on id
     * @param:(int)$MFBinstanceId
     */
    public function deleteMFBInstance($MFBInstanceId){
    	//delete MFB instance in case of mail failure so that to send order MFB mail again later
    	$query="delete from ".self::MFB_INSTANCE_TABLE." where instanceid='".$MFBInstanceId."'";
    	db_query($query);
    }

    /*
     * adds HTML <form> to the MFB questionnaire
     * @param:(string)$MFB_HTML - HTML questionnaire
     * @param:(string)$formURL - url need to be put in <form action="">
     * @param:(int)$MFBInstanceId - HTML questionnaire
     * @param:(string)MFBAuthKey - to authenticate MFB instance
     * @param:(string)$formHead - heading for the form
     * @param:(int)$setResponse - 0/1 - 0-for redirection from email, 1- feedback response
     * @param:(string)$submitButtonName
     * @return: (string)$HTML
     */
    public function wrapMFBHTMLQuestionnaire($MFBHTMLQuestionnaire, $formURL, $MFBInstanceId, $MFBAuthKey, $formHead=null,
                                             $setResponse=0, $submitButtonProperties=null, $NPS=false){

        $wrapperTop = '<div class="MFB_wrapper_outer" id="MFB_wrapper_outer">';

        if(!empty($formHead)){
            $wrapperTop .=  '<h3 style="font: 13px/1.5 din-med,Arial,Sans-Serif;padding:0;margin:0;">'.$formHead.'</h3><hr>';
        }

        $wrapperTop .= '<form name="MFB_form" id="MFB_form" action="'.$formURL.'" method="POST">';

        //set this to avoid nocookie warning from nocookie_warning.php
        //and to accept the cross domain request without cookie(xid) set
        $wrapperTop .= '<input type="hidden" name="allowCrossDomainRequest" id="allowCrossDomainRequest" value="1"/>';
        
	    // TODO temporarily setting $setResponse as 1 to submit feedback straight away from the email
	    if(empty($NPS)){
	    	$setResponse=1;
	    	$setNPS=0;
	    	
	    } else {
	    	$setNPS=1;
	    }
        
        if($setResponse==1){
        	$wrapperTop .= '<input type="hidden" name="MFB_response" id="MFB_response" value="'.$setResponse.'"/>';
        }
        
        if($setNPS==1){
        	$wrapperTop .= '<input type="hidden" name="MFB_NPS_response" id="MFB_NPS_response" value="'.$setNPS.'"/>';
        }
        

        $wrapperTop .= '<input type="hidden" name="MFBInstanceId" id="MFBInstanceId" value="'.$MFBInstanceId.'"/>
                            <input type="hidden" name="MFBInstanceAuthKey" id="MFBInstanceAuthKey" value="'.$MFBAuthKey.'"/>';

        $button_div_cssStyles = "border-top: 1px solid #c6c6c6;padding: 20px 0;";

        $wrapperBottom = '<div style="'.$button_div_cssStyles.'" class="button-div">';
        
        $MFB_submit_cssStyles = 'color: #fff;background-color: #f27521;font: 14px/26px din-med,arial,sans-serif;vertical-align: middle;cursor: pointer;padding: 5px;border: none;border-radius: 3px;box-shadow: 1px 1px 1px #999;text-transform: uppercase;min-width: 100px;';

        if(empty($submitButtonProperties) && !is_array($submitButtonProperties)){
            $wrapperBottom .= '<input style="'.$MFB_submit_cssStyles.'" type="submit" name="MFB_submit" id="MFB_submit" value="Submit"/>';
        } else {
            $wrapperBottom .= '<input style="'.$MFB_submit_cssStyles.'" type="'.$submitButtonProperties['type'].'" name="MFB_submit" id="MFB_submit" value="'.$submitButtonProperties['value'].'" class="btn primary-btn"/>';
        }

        $wrapperBottom .=  '</div>
                            <div style="clear:both;display:none;"></div>
		                    </form></div>';
        return $wrapperTop.$MFBHTMLQuestionnaire.$wrapperBottom;
    }

    /*
     * builds the feedback survey HTML
     * @param:(int)$MFBId - feedback id
     * @param:(int)$MFBInstanceId - feedback instance id     
     * @return:(string)$MFBHTMLsurvey
     */
    public function getMFBSurvey($MFBId, $MFBInstanceId){

        //to get MFB questions
        $MFB = new MFB();
        $MFBQuestions = $MFB->MFBQ->getMFBQuestions(array("feedbackid"=>$MFBId,"active"=>1),array("display_order"=>"asc"));

        //get all MFB instance data
        $MFBInstanceData = $this->getMFBInstanceData(array("feedback_instance_id"=>$MFBInstanceId));

        foreach($MFBQuestions as $key=>$question){

            $HTMLquestion .= '<div class="MFBQ_wrapper" id="MFBQ_'.$question['questionid'].'" style="margin-bottom:35px;">'.
				            '<div class="MFBQ" style="margin-bottom:10px;">'.$question['question'].'</div>';
            //wrap all options in <ul>
            $HTMLquestion .= '<ul class="MFB_option_wrapper" style="list-style-type:none;display:table-row-group;">';
            
            foreach($MFBInstanceData as $key=>$instance){
                //get the optionHTML where question matches
                if($instance['questionid'] == $question['questionid']){
                    $HTMLquestion .= '<li class="MFB_option" style="float:left;margin-right:20px;">
                                        <label>'.$instance['response'].'</label>
                                      </li>';
                }
            }

            $HTMLquestion .= '</ul>';
            $HTMLquestion .= '<div style="clear:both;display:none;"></div>'.
                         '</div>';
        }

        //outermost wrapper
        $wrapperStartOuter = '<div class="MFB_wrapper_outer" id="MFB_wrapper_outer" style="width:100%;height:auto;background-color:#eee;">';
        $wrapperEndOuter = '</div>';
        
        //questionnaire wrapper HTML
        $wrapperStart = '<div class="MFB_wrapper" id="MFB_wrapper" style="width:auto;height:auto;padding:20px">';
        $wrapperEnd = '</div>';

        return $wrapperStartOuter.$wrapperStart.$HTMLquestion.$wrapperEnd.$wrapperEndOuter;

    } 
    
    
   /*
    * builds the feedback mail content in HTML with all requireed dynamic parameters(auth-key,instanceid etc)
    * @param:(array)$MFBConfigArray    
    * @return:(array)[(string)$MFBMailHTMLContent,(int)instanceId] | (bool) false
    */    
    public function getMFBMailContent($MFBConfigArray){    	
    	
    	if(!empty($MFBConfigArray['CUSTOMER_EMAIL']) && !empty($MFBConfigArray['FEEDBACK_NAME']) 
    			&& !empty($MFBConfigArray['REFERENCE_ID'])){    	
  
    		$feedbackFormActionURL = "http://www.myntra.com/feedback.php";
    		
    		//get the MFB by name
    		$MFB = new MFB();
    		$MFBInfo = $MFB->getMFBByName($MFBConfigArray['FEEDBACK_NAME']);
    		$MFBId = $MFBInfo[0]['feedbackid'];    		
    	
    		//get FB form
    		$MFB_HTML = $MFB->getMFBActiveQuestionnaire($MFBId);
    	
    		//add reference id for FB instance into DB
    		$addMFBInstance = $this->addMFBInstance($MFBId , $MFBConfigArray['REFERENCE_ID'], 
    				$MFBConfigArray['CUSTOMER_EMAIL'], $MFBConfigArray['REFERENCE_TYPE']);
    	
    		//send feedback only then when instance is captured
    		if($addMFBInstance !== false){
    	
    			//wrap MFB HTML questionnaire with <form>
    			//and hidden fields required to send with order FB mail
    			$MFBInstance = $this->getMFBInstances(array("instanceid"=>$addMFBInstance));
    			$MFB_HTML = $this->wrapMFBHTMLQuestionnaire($MFB_HTML['questionnaireHTML'], $feedbackFormActionURL, 
    					$addMFBInstance, $MFBInstance[0]['authentic_key'], $MFBInfo[0]['title']);
    	
    			//alternative MFB link to submit the form
    			$MFBParams = array(
    					"allowCrossDomainRequest"   => 1,
    					"MFBInstanceId"             => $addMFBInstance,
    					"MFBInstanceAuthKey"        => $MFBInstance[0]['authentic_key']
    			);
    	
    			
    			$MFBLink = $feedbackFormActionURL."?".http_build_query($MFBParams);    			
    	
    			$MFBMailContent = "<div style='background:#fff;color:#333;font-family:din-med,Arial,Sans-Serif;width:760px;margin:0 auto'>";
    			$MFBMailContent .= $MFB_HTML;
    			$MFBMailContent .= "<br/>";    			
    			$MFBMailContent .= '<p>Please click on the "Submit" button once you are done. '.
    					'If you are unable to submit your feedback, please click <a href="'.$MFBLink.'" target="_blank">here</a>.</p>';    					
    			$MFBMailContent .= "</div>";	
				    	
    			return array(
    					"MFBMailHTML"=>$MFBMailContent, 
    					"MFBInstanceId"=>$addMFBInstance
    					); 
    		}
    	
    	} else {
    		return false;
    	}    	
    }

    
    /*
     * returns an array with only NPS question detail
     * @param:(array)$MFBConfigArray
     * @return:(array)[(string)MFBTitle,(String)MFBNpsQuestion, (String) MFBLink, (int)instanceId] | (bool) false
     */
    public function getMFBMailContentJson($MFBConfigArray){
    	 
    	if(!empty($MFBConfigArray['CUSTOMER_EMAIL']) && !empty($MFBConfigArray['FEEDBACK_NAME'])
    			&& !empty($MFBConfigArray['REFERENCE_ID'])){
    
    		$feedbackFormActionURL = "http://www.myntra.com/feedback.php";
    
    		//get the MFB by name
    		$MFB = new MFB();
    		$MFBInfo = $MFB->getMFBByName($MFBConfigArray['FEEDBACK_NAME']);
    		$MFBId = $MFBInfo[0]['feedbackid'];
    		 
    		//get FB form
    		$MFBNpsQuestion = $MFB->getMFBActiveNpsQuestion($MFBId);
    		 
    		//add reference id for FB instance into DB
    		$addMFBInstance = $this->addMFBInstance($MFBId , $MFBConfigArray['REFERENCE_ID'],
    				$MFBConfigArray['CUSTOMER_EMAIL'], $MFBConfigArray['REFERENCE_TYPE']);
    		 
    		//send feedback only then when instance is captured
    		if($addMFBInstance !== false){    			 
    			
    			//get instance data with authentic key
    			$MFBInstance = $this->getMFBInstances(array("instanceid"=>$addMFBInstance));  			
    			 
    			//alternative MFB link to submit the form
    			$MFBParams = array(
    					"allowCrossDomainRequest"   => 1,
    					"MFBInstanceId"             => $addMFBInstance,
    					"MFBInstanceAuthKey"        => $MFBInstance[0]['authentic_key'],
    					"MFB_response"				=> 0,
    					
    			);    			 
    			 
    			$MFBLink = $feedbackFormActionURL."?".http_build_query($MFBParams);
    			 
    			return array(    					    					
    					"MFBTitle"=>$MFBInfo[0]['title'],
    					"MFBNpsQuestion"=>$MFBNpsQuestion[0]["question"],
    					"MFBLink"=>$MFBLink
    			);
    		}
    		 
    	} else {
    		return false;
    	}
    }
}
?>
