<?php
namespace feedback\instance;
/**
 * The class exposes abstract APIs for handling order FB Instance
 * @author: Arun Kumar K
 * legends:
 *      MFB - Myntra FeedBack
 *      MFBOrder - Myntra FeedBack Order - FB related to order
 */
global $xcart_dir;
include_once($xcart_dir."/include/func/func.db.php");
include_once($xcart_dir."/include/class/class.mail.multiprovidermail.php");

use feedback\manage\MFB;

class MFBOrderInstance extends MFBInstance{

    const MFB_INTERVAL = 45;
    const TRACK_HISTORY = "mk_tracking_history";

    /*
     * fetches all orders (status C or DL) and 24 hours old since is completed(delivered)
     * @return: (array)$MFBorders
     */
    // TODO // Change the now(). to use php time function
    private function getAllMFBEligibleOrders(){

        $query= "select o.orderid,o.login,concat(o.firstname,' ',o.lastname)as name,
                IF((`date` IS NULL OR `date`=0),'',from_unixtime(`date`,'%d-%M-%Y')) as ordered_date,
                IF((`shippeddate` IS NULL OR `shippeddate`=0),'',from_unixtime(`shippeddate`,'%d-%M-%Y')) as shipped_date,
                IF((`delivereddate` IS NULL OR `delivereddate`=0),'',from_unixtime(`delivereddate`,'%d-%M-%Y')) as delivered_date,
                IF((`completeddate` IS NULL OR `completeddate`=0),'',from_unixtime(`completeddate`,'%d-%M-%Y')) as completed_date,
                payment_method
                from xcart_orders o
                where
                    o.status in ('C','DL')
                and
                    o.feedback_request='N'
                and
                    (
                        o.completeddate <= unix_timestamp(now()) -86400
                    OR
                        o.delivereddate <= unix_timestamp(now()) -86400
                    )
                order by o.orderid";
        
        $result=db_query($query,true);

        $MFBorders=array();
        $i=0;
        if ($result){

            while ($row=db_fetch_array($result))
            {
                $MFBorders[$i] = $row;
                $i++;
            }
        }

        return $MFBorders;
    }

    /*
     * checks for any FB mail sent in last MFB_INTERVAL days
     * @param:(int)$MFBId
     * @param:(string)$email
     * @return: (bool)
     */
    public function isMFBIntervalSatisfiesCustomer($MFBId, $customerEmail){
        $lastSentDate = $this->getMFBInstanceLastSentDateForCustomer($MFBId, $customerEmail);
        $dayDifference = floor((time()-$lastSentDate)/86400);
        return ($dayDifference>=self::MFB_INTERVAL); 
    }


    /*
     * adds HTML <form> to the MFB questionnaire
     * @param:(string)$linkInEmail - <form> action url
     * @return: (bool)
     */
    public function sendOrderMFBMail($linkInEmail){

        //get all MFB eligible orders
        $ordersNeedFB = $this->getAllMFBEligibleOrders();

        //get the current selected order FB
        $MFB = new MFB();
        $MFBId = $MFB->getSelectedOrderMFB();
        $MFBId = $MFBId[0]['feedbackid'];

        //get order FB form
        $MFBActiveQuestionnaire = $MFB->getMFBActiveQuestionnaire($MFBId);        
        
        if(is_array($ordersNeedFB)){
            foreach($ordersNeedFB as $order){

                //check MFB interval of 45 days
                $MFBIntervalTestPass = $this->isMFBIntervalSatisfiesCustomer($MFBId, $order['login']);

                if($MFBIntervalTestPass){

                    //if order delivered then only send MFB
                    if(!empty($order['delivered_date'])){

                        //add this order FB instance into DB
                        $addMFBInstance = $this->addMFBInstance($MFBId , $order['orderid'], $order['login'],"ORDER");

                        // check FG whether to enter transitional instance id
                        $transitionCondition = \FeatureGateKeyValuePairs::getFeatureGateValueForKey('MFB.Order.Transition');
                        if($transitionCondition == 'true'){
                            $this->setTransitionalInstance($addMFBInstance);
                        }

                        //send feedback only then when instance is captured
                        if($addMFBInstance !== false){

                            //update MFB sent flag in orders table
                            $query="update xcart_orders set feedback_request ='Y' where orderid='".$order['orderid']."'";
                            db_query($query);

                            //wrap MFB HTML questionnaire with <form>
                            //and hidden fields required to send with order FB mail
                            $MFBInstance = $this->getMFBInstances(array("instanceid"=>$addMFBInstance));
                            $formHead = "Myntra Order Feedback";
                            $MFB_HTML = $this->wrapMFBHTMLQuestionnaire($MFBActiveQuestionnaire['questionnaireHTML'],
                                                                        $linkInEmail, $addMFBInstance, $MFBInstance[0]['authentic_key'], $formHead);

                            //alternative MFB link to submit the form
                            $MFBParams = array(
                                "allowCrossDomainRequest"   => 1,
                                "MFBInstanceId"             => $addMFBInstance,
                                "MFBInstanceAuthKey"        => $MFBInstance[0]['authentic_key']
                            );
                            $MFBLink = $linkInEmail."?".http_build_query($MFBParams);

                            //email template for MFB order
                            $template = "voc_order";

                            //send order MFB mail as (type-critical)
                            $subjectArgs = array("ORDER_ID"=>$order['orderid']);
                            $bodyArgs = array(
                                        "USER"				=> $order['name'],
                                        "ORDER_ID"			=> $order['orderid'],
                                        "DELIVERY_DATE"		=> $order['delivered_date'],
                                        "ORDER_MFB_FORM"	=> $MFB_HTML,
                                        "REDIRECT_INFO"     => "<p>Please click on the \"Continue\" button above once you are done. ".
                                                                "This will take you to a separate landing page on Myntra. ".
                                                                "where you are one click away from submitting your feedback. ".
                                                                "Do ensure that the landing page domain name is www.myntra.com ".
                                                                "and the page contains the Myntra logo.</p>".
                                                                "<p><b>Note :</b> ".
                                                                "If you are unable to continue, please refresh the page and try again. ".
                                                                "If that does not solve the problem, please click on this $MFBLink to submit your feedback.</p>".
                                                                "<p>Do note that all feedback links are active for a period of ".parent::MFB_INSTANCE_SPAN." days ".
                                                                "only from the date of receipt of this mail.</p>",

                                        "DECLARATION"       => "This mail is intended only for ".$order['login']
                                        );
                            $customKeywords = array_merge($subjectArgs, $bodyArgs);
                            $mail_details = array(
                                    "template" => $template,
                                    "to" => $order['login'],
                                    "bcc" => "myntramailarchive@gmail.com",
                                    "header" => 'header',
                                    "footer" => 'footer',
                                    "mail_type" => \MailType::CRITICAL_TXN
                                );
                            $multiPartymailer = new \MultiProviderMailer($mail_details, $customKeywords);
                            $flag = $multiPartymailer->sendMail();

                            if($flag === false){
                                //delete MFB instance in case of mail failure so that to send order MFB mail again later                                
                                $this->deleteMFBInstance($addMFBInstance);

                                //revert order flag for FB sent to FB not sent
                                $query="update xcart_orders set feedback_request ='N' where orderid='".$order['orderid']."'";
                                db_query($query);
                            }
                        }
                    }

                /* don't entertain any orders of the same customer placed in the MFB interval time
                 * for MFB. 
                 */
                } else {
                    //update MFB sent flag in orders table
                    $query="update xcart_orders set feedback_request ='Y' where orderid='".$order['orderid']."'";
                    db_query($query);

                }
            }
        }
    }

    /*
     * adds an instnace id in trackning table as a demarcation
     * to send approriate coupon for order feedback instance 
     * @param:(string)$instanceId - MFB instance id(transitional)
     * @return: (void)
     */
    public function setTransitionalInstance($instanceId){

        $transitionalInstanceId = $this->getTransitionalInstance();

        if($transitionalInstanceId === false){            
            $queryData = array(
                'utm_source'=>'direct',
                'utm_medium'=>'direct',
                'event_type'=>'transitional_feedback_instance_id',
                'event_value'=>$instanceId,
                'track_time'=>time()
            );
            return func_array2insert(self::TRACK_HISTORY,sanitizeDBValues($queryData));
        }
    }

    /*
     * gets the demarcation instance id fro tracking table
     * @return: (int)$instanceId | (bool)false
     */
    public function getTransitionalInstance(){
        $instanceId = func_query_first_cell("select
                                    event_value
                                from
                                    ".self::TRACK_HISTORY."
                                where
                                    event_type='transitional_feedback_instance_id'", true);

        if(empty($instanceId)){
            return false;
        }

        return $instanceId;
    }

    /*
     * checks the transitioanl instnace id in trackning table
     * to send approriate coupon for order feedback instance
     * @param:(string)$instanceId - MFB instance id
     * @return: (bool)
     */
    public function isTransitionalInstance($instanceId){

        $transitionalInstanceId = $this->getTransitionalInstance();
        if($transitionalInstanceId !== false){
            $flag = ($transitionalInstanceId <= $instanceId);
            return $flag;
        }

        return true;
    }
}
?>