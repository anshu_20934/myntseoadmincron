<?php
namespace feedback\manage;

/**
 * The classs exposes abstract APIs for handling feedback management.
 * @author: Arun Kumar K
 * legends:
 *      MFB - Myntra FeedBack
 *      MFBQ - Myntra FeedBack Question
 */

include_once($xcart_dir."/include/func/func.db.php");


class MFBOption{

	const MFB_OPT_TABLE = "mk_feedback_option";//table

	/*
	 * gets options for MFBQ
	* @param:(array)$filter - can be different columns of MFB_OPT_TABLE
	* @param:(array)$order - can be ordered by columns of MFB_OPT_TABLE
	* @return:(array)$options - all info of options
	*/
	public function getMFBOptions($filter,$order=null){
		$sql = "select
		*
		from
		" . self::MFB_OPT_TABLE;

		$MFBQuestions = sanitizedQueryBuilder($sql,$filter,$order);
		return $MFBQuestions;
	}

	/*
	 * builds questionnaire HTML (set of question and its options)
	* @param:(array)$MFBOptions - all options of a question
	* @param:(array)$MFBquestion
	* @return:(string)$HTML - HTML questionnaire
	*/
	public function buildMFBQuestionHTML($MFBOptions, $MFBquestion, $selectedOptionId=null, $selectedOptionIdValuePair=null, $selectedOptionIdsArray=null, $NPS=false){

		$MFBQ_wrapper_cssStyles = 'background-color: #f2f2f2;padding: 10px;margin: 0 0 20px;font: 13px/1.5 din-med,Arial,Sans-Serif;';

		//show '*' for mandatory question
		if($MFBquestion['mandatory']==1){
			$HTMLquestion = '<div id="MFBQ_'.$MFBquestion['questionid'].'" style="'.$MFBQ_wrapper_cssStyles.'">'.
					'<div class="MFBQ" style="margin-bottom:10px;">'.$MFBquestion['question'].'<span style="color:red;">&nbsp;*</span></div>';
		} else {
			$HTMLquestion = '<div id="MFBQ_'.$MFBquestion['questionid'].'" style="'.$MFBQ_wrapper_cssStyles.'">'.
					'<div class="MFBQ" style="margin-bottom:10px;">'.$MFBquestion['question'].'</div>';
		}

		$MFB_option_wrapper_cssStyles = 'list-style-type:none;display: inline-block;margin: 0;padding: 0;';

		//wrap all options in <ul>
		$HTMLquestion .= '<ul style="'.$MFB_option_wrapper_cssStyles.'">';

		//build option HTML based on question type
		switch($MFBquestion['question_type']){

			case 'radio'    :   $HTMLquestion .= $this->buildRadio($MFBOptions, $selectedOptionId);
			break;
			
			case 'continuousradio'    :
				if(empty($NPS)){
					// continuous radio is not needed here since this NPS question is answered before hand in mail itself
					$HTMLquestion .= $this->buildContinuousRadio($MFBOptions, $selectedOptionId);
				}   
				
			break;

			case 'checkbox' :   $HTMLquestion .= $this->buildCheckBox($MFBOptions, $selectedOptionIdsArray);
			break;

			case 'text'     :   $HTMLquestion .= $this->buildText($MFBOptions, $selectedOptionIdValuePair);
			break;

			case 'textbox'  :   $HTMLquestion .= $this->buildTextBox($MFBOptions, $selectedOptionIdValuePair);
			break;

			// TODO options of dropdown
			/*case 'dropdown' :   $HTMLquestion .= $this->buildDropDown($MFBOptions);
			 break;

			case 'dropdownmulti' :  $HTMLquestion .= $this->buildDropDownMulti($MFBOptions);
			break;*/
		}

		$HTMLquestion .= '</ul>';
		$HTMLquestion .= '<div style="clear:both;display:none;"></div>'.
				'</div>';

		return $HTMLquestion;
	}


	/*
	 * builds radio options
	* @param:(array)$MFBOptions - all radio options of a question
	* @return:(string)$HTML
	*/
	private function buildRadio($MFBOptions, $selectedOptionId=null){

		foreach($MFBOptions as $key=>$option){
			$qId = $option['questionid'];
			$optId = $option['optionid'];

			$MFB_option_cssStyles = 'float:left;margin: 0 0 0 0;text-align:center;';
			$label_radio_cssStyles = 'color: #808080;font: 10px / 1.5 din-med,Arial,Sans-Serf;display: block; height: 65px; text-align: center; width: 50px;';
			$radio_button_cssStyles = 'cursor: pointer;height: 10px;width: 10px;';
			// TODO posted(selected) option should always take highest priority, here currently default
			// TODO can also take highest priority
			if($option['option_default']==1 || $selectedOptionId==$optId){
				$HTMLOption .= '<li class="MFB_option" style="'.$MFB_option_cssStyles.'">
				<input style="'.$radio_button_cssStyles.'" type="radio" id="MFB['.$qId.']['.$optId.']" name="MFB['.$qId.'][]"
				value="'.$optId.'" checked="checked"/>
				<label for="MFB['.$qId.']['.$optId.']" class="selected label-radio" style="'.$label_radio_cssStyles.'">'.$option['option_value'].'</label>
				</li>';
			} else {
				$HTMLOption .= '<li class="MFB_option" style="'.$MFB_option_cssStyles.'">
				<input style="'.$radio_button_cssStyles.'" type="radio" id="MFB['.$qId.']['.$optId.']" name="MFB['.$qId.'][]"
				value="'.$optId.'"/>
				<label for="MFB['.$qId.']['.$optId.']" class="label-radio" style="'.$label_radio_cssStyles.'">'.$option['option_value'].'</label>
				</li>';
			}
		}

		return $HTMLOption;
	}
	
	/*
	 * builds Continuous radio options
	* @param:(array)$MFBOptions - all radio options of a question
	* @return:(string)$HTML
	*/
	private function buildContinuousRadio($MFBOptions, $selectedOptionId=null){

		foreach($MFBOptions as $key=>$option){
			$qId = $option['questionid'];
			$optId = $option['optionid'];

			$MFB_option_cssStyles = 'float:left;text-align: center;';
			$label_radio_cssStyles = 'color: #808080;font: 10px / 1.5 din-med,Arial,Sans-Serf;display: block; height: 65px; text-align: center; width: 20px;';
			$radio_button_cssStyles = 'cursor: pointer;height: 10px;width: 10px;';
			// TODO posted(selected) option should always take highest priority, here currently default
			// TODO can also take highest priority
			if($option['option_default']==1 || $selectedOptionId==$optId){
				$HTMLOption .= '<li class="MFB_option" style="'.$MFB_option_cssStyles.'">
				<input style="'.$radio_button_cssStyles.'" type="radio" id="MFB['.$qId.']['.$optId.']" name="MFB['.$qId.'][]"
				value="'.$optId.'" checked="checked"/>
				<label for="MFB['.$qId.']['.$optId.']" class="selected label-radio" style="'.$label_radio_cssStyles.'">'.$option['option_value'].'</label>
				</li>';
			} else {
				$HTMLOption .= '<li class="MFB_option" style="'.$MFB_option_cssStyles.'">
				<input style="'.$radio_button_cssStyles.'" type="radio" id="MFB['.$qId.']['.$optId.']" name="MFB['.$qId.'][]"
				value="'.$optId.'"/>
				<label for="MFB['.$qId.']['.$optId.']" class="label-radio" style="'.$label_radio_cssStyles.'">'.$option['option_value'].'</label>
				</li>';
			}
		}

		return $HTMLOption;
	}

	/*
	 * builds checkbox options
	* @param:(array)$MFBOptions - all checkbox options of a question
	* @return:(string)$HTML
	*/
	private function buildCheckBox($MFBOptions, $selectedOptionIdsArray=null){

		foreach($MFBOptions as $key=>$option){

			$qId = $option['questionid'];
			$optId = $option['optionid'];

			if($option['option_default']==1 || in_array($optId, $selectedOptionIdsArray)){
				$HTMLOption .= '<li class="MFB_option" style="float:left;margin-right:20px;text-align:center;">
				<input type="checkbox" id="MFB['.$qId.']['.$optId.']." name="MFB['.$qId.']['.$optId.']."
				value="'.$optId.'" checked="checked"/>
				<label for="MFB['.$qId.']['.$optId.']." class="selected label-checkbox" style="display: block; height: 65px; text-align: center; width: 50px;">'.$option['option_value'].'</label>
				</li>';
			} else {
				$HTMLOption .= '<li class="MFB_option" style="float:left;margin-right:20px;text-align:center;">
				<input type="checkbox" id="MFB['.$qId.']['.$optId.']." name="MFB['.$qId.']['.$optId.']."
				value="'.$optId.'"/>
				<label for="MFB['.$qId.']['.$optId.']." class="label-checkbox" style="display: block; height: 65px; text-align: center; width: 50px;">'.$option['option_value'].'</label>
				</li>';
			}
		}

		return $HTMLOption;
	}

	/*
	 * builds textbox option
	* @param:(array)$MFBOptions - a textbox option of a question
	* @return:(string)$HTML
	*/
	private function buildTextBox($MFBOptions, $selectedOptionIdValuePair=null){

		foreach($MFBOptions as $key=>$option){

			$qId = $option['questionid'];
			$optId = $option['optionid'];

			//is textbo is posted by FB form
			$isPostText = array_key_exists($optId, $selectedOptionIdValuePair);

			if($option['option_default']==1 || $isPostText){

				//if selectedOptvalue is null then show the default one
				$option['option_value'] = ($isPostText)? $selectedOptionIdValuePair[$optId] : $option['option_value'];

				$HTMLOption .= '<li class="MFB_option" style="float:left;margin-right:20px;">
				<input type="text" id="MFB['.$qId.']['.$optId.']" name="MFB['.$qId.']['.$optId.']"
				value="'.$option['option_value'].'" style="width:300px;" maxlength="'.$option['maxchars'].'"/>
				<label for="MFB['.$qId.']['.$optId.']">max '.$option['maxchars'].' chars</label>
				</li>';
			} else {
				$HTMLOption .= '<li class="MFB_option" style="float:left;margin-right:20px;">
				<input type="text" id="MFB['.$qId.']['.$optId.']" name="MFB['.$qId.']['.$optId.']"
				value="" style="width:300px;" maxlength="'.$option['maxchars'].'"/>
				<label for="MFB['.$qId.']['.$optId.']">max '.$option['maxchars'].' chars</label>
				</li>';
			}
		}

		return $HTMLOption;
	}

	/*
	 * builds text option
	* @param:(array)$MFBOptions - a text option of a question
	* @return:(string)$HTML
	*/
	private function buildText($MFBOptions, $selectedOptionIdValuePair=null){

		foreach($MFBOptions as $key=>$option){

			$qId = $option['questionid'];
			$optId = $option['optionid'];

			$MFB_option_cssStyles = 'float:left;margin:0;';
			$textarea_cssStyles = 'border: 0;display: block;width: 250px;height: 100px;';
			$textarea_label_cssStyles = 'margin-top: 5px;color: #808080;font: 10px/16px din-med,arial,sans-serif;text-transform: uppercase;';

			//is text is posted by FB form
			$isPostText = array_key_exists($optId, $selectedOptionIdValuePair);

			if($option['option_default']==1 || $isPostText){

				//if selectedOptvalue is null then show the default one
				$option['option_value'] = ($isPostText)? $selectedOptionIdValuePair[$optId] : $option['option_value'];

				$HTMLOption .= '<li class="MFB_option" style="'.$MFB_option_cssStyles.'">
				<textarea id="MFB['.$qId.']['.$optId.']" name="MFB['.$qId.']['.$optId.']" style="'.$textarea_cssStyles.'">'.$option['option_value'].'</textarea>
				<label style="'.$textarea_label_cssStyles.'" for="MFB['.$qId.']['.$optId.']">max '.$option['maxchars'].' chars</label>
				</li>';
			} else {
				$HTMLOption .= '<li class="MFB_option" style="'.$MFB_option_cssStyles.'">
				<textarea id="MFB['.$qId.']['.$optId.']" name="MFB['.$qId.']['.$optId.']" style="'.$textarea_cssStyles.'"></textarea>
				<label style="'.$textarea_label_cssStyles.'" for="MFB['.$qId.']['.$optId.']">max '.$option['maxchars'].' chars</label>
				</li>';
			}
		}

		return $HTMLOption;
	}
}
?>