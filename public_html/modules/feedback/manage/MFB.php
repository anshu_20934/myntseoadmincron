<?php
namespace feedback\manage;

/**
 * The classs exposes abstract APIs for handling feedback management.
 * @author: Arun Kumar K
 * legends:
 *      MFB - Myntra FeedBack
 */

include_once($xcart_dir."/include/func/func.db.php");
include_once($xcart_dir."/include/func/func.utilities.php");


class MFB{

    const MFBTABLE = "mk_feedback";//table

    public $MFBQ;

    function __construct(){
        $this->_initializeMFB();
    }

    /*
     * initializes MFB with all necessary objects
     * so that not necessary to instantiate objects for each.
     * @return: void
     */
    public function _initializeMFB(){
        $this->MFBQ  = new MFBQuestion();
    }

    /*
     * gets the current selected order MFB
     * @return:(array)$MFB['feedbackid']
     */
    public function getSelectedOrderMFB(){
        $filter = array("selected_order_FB"=>1, "active"=>1);
        $sql = "select
                    *
                from
                    " . self::MFBTABLE;

        $MFB = sanitizedQueryBuilder($sql,$filter);
        return $MFB;
    }

    /*
     * gets the current selected SR MFB
     * @return:(array)$MFB['feedbackid']
     */
    public function getSelectedSRMFB(){
        $filter = array("selected_SR_FB"=>1, "active"=>1);
        $sql = "select
                    feedbackid
                from
                    " . self::MFBTABLE;

        $MFB = sanitizedQueryBuilder($sql,$filter);
        return $MFB;
    }
    
    /*
     * gets the MFB by unique feedback name
    * @return:(array)$MFB['feedbackid']
    */
    public function getMFBByName($feedbackName){
    	$filter = array("name"=>$feedbackName, "active"=>1);
    	$sql = "select
    	feedbackid
    	,name
    	,title    	
    	from
    	" . self::MFBTABLE;
    
    	$MFB = sanitizedQueryBuilder($sql,$filter);
    	return $MFB;
    }

    /*
     * gets the MFB based on filter
     * @param:(array)$filter - can be different columns of MFBTABLE
     * @param:(array)$order - can be ordered by columns of MFBTABLE
     * @return:(array)$MFB['feedbackid']
     */
    public function getMFB($filter,$order=null){
        $sql = "select
                    *
                from
                    " . self::MFBTABLE;

        $MFBQuestions = sanitizedQueryBuilder($sql,$filter,$order);
        return $MFBQuestions;
    }

    /*
     * gets the complete questionnaire(HTML,array,JSON) for MFB
     * @param:(int)$MFBId
     * @param:(bool)$NPS defaults false - to recognize whether its post nps change
     * @return: (array) array($HTML,JSON,MFBQuesion and options)- all active elements
     */
    public function getMFBActiveQuestionnaire($MFBId, $NPS=false){
        $MFBQuestions = $this->MFBQ->getMFBQuestions(array("feedbackid"=>$MFBId,"active"=>1),array("display_order"=>"asc"));

        //needed for CLient side scripts
        $MFBJSON = array();
        
        if(!empty($MFBQuestions) && is_array($MFBQuestions)){
            foreach($MFBQuestions as $key=>$question){
            	// to support prior NPS changes
            	if(empty($NPS)){
            		//get the all active options of all active questions
            		$MFBOptions = $this->MFBQ->MFBOption->getMFBOptions(array("questionid"=>$question['questionid'],"active"=>1),
            				array("display_order"=>"asc"));
            		$MFBQuestions[$key]['options'] = $MFBOptions;
            		
            		$MFBJSON[$question['questionid']]['q_type'] = $question['question_type'];
            		$MFBJSON[$question['questionid']]['mandatory'] = $question['mandatory'];
            		
            		//get maxchars for client side script in case of text,textbox
            		if($question['question_type'] == 'text' || $question['question_type'] == 'textbox'){
            			$MFBJSON[$question['questionid']]['maxchars'] = $MFBOptions[0]['maxchars'];
            		}
            		
            		
            		//build HTML based on OPtion build rule
            		$MFBQuestionnaireHTML .= $this->MFBQ->MFBOption->buildMFBQuestionHTML($MFBOptions, $question);
            		
            	} else {
            		// not to show NPS question on landing page
            		if(strtolower($question['question_type']) != "continuousradio"){
            			//get the all active options of all active questions
            			$MFBOptions = $this->MFBQ->MFBOption->getMFBOptions(array("questionid"=>$question['questionid'],"active"=>1),
            					array("display_order"=>"asc"));
            			$MFBQuestions[$key]['options'] = $MFBOptions;
            			
            			$MFBJSON[$question['questionid']]['q_type'] = $question['question_type'];
            			$MFBJSON[$question['questionid']]['mandatory'] = $question['mandatory'];
            			
            			//get maxchars for client side script in case of text,textbox
            			if($question['question_type'] == 'text' || $question['question_type'] == 'textbox'){
            				$MFBJSON[$question['questionid']]['maxchars'] = $MFBOptions[0]['maxchars'];
            			}
            			
            			
            			//build HTML based on OPtion build rule
            			$MFBQuestionnaireHTML .= $this->MFBQ->MFBOption->buildMFBQuestionHTML($MFBOptions, $question, null, null, null, true);
            		
            		}	
            	}
            	
                
            }

            //questionnaire wrapper HTML
            $wrapperStart = '<div id="MFB_wrapper" style="padding:20px">';
            $wrapperEnd = '</div>';
            
            return array(
                "questionnaireArray"=>$MFBQuestions,
                "questionnaireJSON"=>json_encode($MFBJSON),
                "questionnaireHTML"=>$wrapperStart.$MFBQuestionnaireHTML.$wrapperEnd
            );
        }
    }

        
    /*
     * gets the NPS question only
     * @param:(int)$MFBId
     * @return: String question
     */
    public function getMFBActiveNpsQuestion($MFBId){
        $MFBQuestions = $this->MFBQ->getMFBQuestions(array("feedbackid"=>$MFBId,"active"=>1,"question_type"=>"continuousradio"),array("display_order"=>"asc"));
		return $MFBQuestions;        
    }
    		
    		
    /*
     * gets the complete questionnaire(HTML,array,JSON) for MFB
     * @param:(int)$MFBId
     * @param:(array)$selectedQuestionOptionArray - user selected answers
     * @return: (array) array($HTML,JSON,MFBQuesion and options)- all active elements
     */
    public function prePopulateMFBQuestionnaire($MFBId, $selectedQuestionOptionArray, $NPS=false){
        $MFBQuestions = $this->MFBQ->getMFBQuestions(array("feedbackid"=>$MFBId,"active"=>1),array("display_order"=>"asc"));

        //needed for CLient side scripts
        $MFBJSON = array();
        
        if(!empty($MFBQuestions) && is_array($MFBQuestions)){
            foreach($MFBQuestions as $key=>$question){
            	
            	if(empty($NPS)){
            		//get the all active options of all active questions
            		$MFBOptions = $this->MFBQ->MFBOption->getMFBOptions(array("questionid"=>$question['questionid'],"active"=>1),
            				array("display_order"=>"asc"));
            		$MFBQuestions[$key]['options'] = $MFBOptions;
            		
            		$MFBJSON[$question['questionid']]['q_type'] = $question['question_type'];
            		$MFBJSON[$question['questionid']]['mandatory'] = $question['mandatory'];
            		
            		//get maxchars for client side script in case of text,textbox
            		if($question['question_type'] == 'text' || $question['question_type'] == 'textbox'){
            			$MFBJSON[$question['questionid']]['maxchars'] = $MFBOptions[0]['maxchars'];
            		}
            		
            		//build question HTML based on question type
            		switch($question['question_type']){
            		
            			case 'continuousradio'    :
            			case 'radio'    :   if(empty($selectedQuestionOptionArray[$question['questionid']][0])){
            				$selectedRadioOption = null;
            			} else {
            				$selectedRadioOption = $selectedQuestionOptionArray[$question['questionid']][0];
            			}
            			$MFBQuestionnaireHTML .= $this->MFBQ->MFBOption->buildMFBQuestionHTML($MFBOptions,
            					$question, $selectedRadioOption);
            			break;
            		
            			case 'checkbox' :   if(empty($selectedQuestionOptionArray[$question['questionid']]) && !is_array($selectedQuestionOptionArray[$question['questionid']])){
            				$selectedOptionIdsArray = null;
            			} else {
            				$selectedOptionIdsArray = $selectedQuestionOptionArray[$question['questionid']];
            			}
            			$MFBQuestionnaireHTML .= $this->MFBQ->MFBOption->buildMFBQuestionHTML($MFBOptions,
            					$question, null, null, $selectedOptionIdsArray);
            			break;
            		
            		
            			case 'textbox'  :   if(empty($selectedQuestionOptionArray[$question['questionid']]) && !is_array($selectedQuestionOptionArray[$question['questionid']])){
            				$selectedTextOptionIdValuePair = null;
            			} else {
            				$selectedTextOptionIdValuePair = $selectedQuestionOptionArray[$question['questionid']];
            			}
            			$MFBQuestionnaireHTML .= $this->MFBQ->MFBOption->buildMFBQuestionHTML($MFBOptions,
            					$question, null, $selectedTextOptionIdValuePair);
            			break;
            		
            			case 'text'     :   if(empty($selectedQuestionOptionArray[$question['questionid']]) && !is_array($selectedQuestionOptionArray[$question['questionid']])){
            				$selectedTextOptionIdValuePair = null;
            			} else {
            				$selectedTextOptionIdValuePair = $selectedQuestionOptionArray[$question['questionid']];
            			}
            			$MFBQuestionnaireHTML .= $this->MFBQ->MFBOption->buildMFBQuestionHTML($MFBOptions,
            					$question, null, $selectedTextOptionIdValuePair);
            			break;           		
            		
            		}
            		
            	// NPS flow		
            	} else {
            		// not to show NPS question on landing page
            		if(strtolower($question['question_type']) != "continuousradio"){
            		
            			//get the all active options of all active questions
            			$MFBOptions = $this->MFBQ->MFBOption->getMFBOptions(array("questionid"=>$question['questionid'],"active"=>1),
            					array("display_order"=>"asc"));
            			$MFBQuestions[$key]['options'] = $MFBOptions;
            		
            			$MFBJSON[$question['questionid']]['q_type'] = $question['question_type'];
            			$MFBJSON[$question['questionid']]['mandatory'] = $question['mandatory'];
            		
            			//get maxchars for client side script in case of text,textbox
            			if($question['question_type'] == 'text' || $question['question_type'] == 'textbox'){
            				$MFBJSON[$question['questionid']]['maxchars'] = $MFBOptions[0]['maxchars'];
            			}
            		
            			//build question HTML based on question type
            			switch($question['question_type']){
            		
            				case 'continuousradio'    :
            				case 'radio'    :   if(empty($selectedQuestionOptionArray[$question['questionid']][0])){
            					$selectedRadioOption = null;
            				} else {
            					$selectedRadioOption = $selectedQuestionOptionArray[$question['questionid']][0];
            				}
            				$MFBQuestionnaireHTML .= $this->MFBQ->MFBOption->buildMFBQuestionHTML($MFBOptions,
            						$question, $selectedRadioOption);
            				break;
            		
            				case 'checkbox' :   if(empty($selectedQuestionOptionArray[$question['questionid']]) && !is_array($selectedQuestionOptionArray[$question['questionid']])){
            					$selectedOptionIdsArray = null;
            				} else {
            					$selectedOptionIdsArray = $selectedQuestionOptionArray[$question['questionid']];
            				}
            				$MFBQuestionnaireHTML .= $this->MFBQ->MFBOption->buildMFBQuestionHTML($MFBOptions,
            						$question, null, null, $selectedOptionIdsArray);
            				break;
            		
            		
            				case 'textbox'  :   if(empty($selectedQuestionOptionArray[$question['questionid']]) && !is_array($selectedQuestionOptionArray[$question['questionid']])){
            					$selectedTextOptionIdValuePair = null;
            				} else {
            					$selectedTextOptionIdValuePair = $selectedQuestionOptionArray[$question['questionid']];
            				}
            				$MFBQuestionnaireHTML .= $this->MFBQ->MFBOption->buildMFBQuestionHTML($MFBOptions,
            						$question, null, $selectedTextOptionIdValuePair);
            				break;
            		
            				case 'text'     :   if(empty($selectedQuestionOptionArray[$question['questionid']]) && !is_array($selectedQuestionOptionArray[$question['questionid']])){
            					$selectedTextOptionIdValuePair = null;
            				} else {
            					$selectedTextOptionIdValuePair = $selectedQuestionOptionArray[$question['questionid']];
            				}
            				$MFBQuestionnaireHTML .= $this->MFBQ->MFBOption->buildMFBQuestionHTML($MFBOptions,
            						$question, null, $selectedTextOptionIdValuePair);
            				break;
            		
            		
            			}
            		}		
            	}
            	
            }

            //questionnaire wrapper HTML
            $wrapperStart = '<div id="MFB_wrapper" style="width:auto;height:auto;padding:20px">';
            $wrapperEnd = '</div>';

            return array(
                "questionnaireArray"=>$MFBQuestions,
                "questionnaireJSON"=>json_encode($MFBJSON),
                "questionnaireHTML"=>$wrapperStart.$MFBQuestionnaireHTML.$wrapperEnd
            );
        }
    }

    /*
     * validates the complete questionnaire for MFB for different type of questions
     * @param:(int)$MFBId
     * @param:(array)$selectedQuestionOptionArray - user selected answers
     * @return: (int)$MFBQ_Id -on invlaid answer for a question ,(bool)true on all valid questions
     */
    //ToDO currently checking only for mandatory, later we can validate for right i/p(using php filters), text limit, regEx check. 
    public function validateMFBQuestionnaire($MFBId, $selectedQuestionOptionArray, $NPS=false){
        $MFBQuestions = $this->MFBQ->getMFBQuestions(array("feedbackid"=>$MFBId,"active"=>1),array("display_order"=>"asc"));
        
        if(!empty($MFBQuestions) && is_array($MFBQuestions)){
            foreach($MFBQuestions as $key=>$question){     

            	if(empty($NPS)){						
					// validating(for mandatory) question based on type
					switch ($question ['question_type']) {
						
						case 'continuousradio' :
						case 'radio' :
							if ($question ['mandatory'] == 1 && empty ( $selectedQuestionOptionArray [$question ['questionid']] [0] )) {
								return $question ['questionid'];
							}
							break;
						
						case 'checkbox' :
							if ($question ['mandatory'] == 1 && empty ( $selectedQuestionOptionArray [$question ['questionid']] ) && ! is_array ( $selectedQuestionOptionArray [$question ['questionid']] )) {
								return $question ['questionid'];
							}
							break;
						
						case 'textbox' :
							if ($question ['mandatory'] == 1 && empty ( $selectedQuestionOptionArray [$question ['questionid']] ) && ! is_array ( $selectedQuestionOptionArray [$question ['questionid']] )) {
								return $question ['questionid'];
							}
							break;
						
						case 'text' :
							if ($question ['mandatory'] == 1 && empty ( $selectedQuestionOptionArray [$question ['questionid']] ) && ! is_array ( $selectedQuestionOptionArray [$question ['questionid']] )) {
								return $question ['questionid'];
							}
							break;
					}
            			
            	} else {
            		// not to show NPS question on landing page
            		if(strtolower($question['question_type']) != "continuousradio"){
            			 
            			//validating(for mandatory) question based on type
            			switch($question['question_type']){
            		
            				case 'continuousradio'    :
            				case 'radio'    :   if($question['mandatory'] == 1 && empty($selectedQuestionOptionArray[$question['questionid']][0])){
            					return $question['questionid'];
            				}
            				break;
            		
            				case 'checkbox' :   if($question['mandatory'] == 1 && empty($selectedQuestionOptionArray[$question['questionid']])
            				&& !is_array($selectedQuestionOptionArray[$question['questionid']])){
            					return $question['questionid'];
            				}
            				break;
            		
            		
            				case 'textbox'  :   if($question['mandatory'] == 1 && empty($selectedQuestionOptionArray[$question['questionid']])
            				&& !is_array($selectedQuestionOptionArray[$question['questionid']])){
            					return $question['questionid'];
            				}
            				break;
            		
            				case 'text'     :   if($question['mandatory'] == 1 && empty($selectedQuestionOptionArray[$question['questionid']])
            				&& !is_array($selectedQuestionOptionArray[$question['questionid']])){
            					return $question['questionid'];
            				}
            				break;
            			}
            		}
            	}
            	
            }
            //all mandatory questions are selected
            return true;
        }
    }
}
?>