<?php
namespace feedback\manage;

/**
 * The classs exposes abstract APIs for handling feedback management.
 * @author: Arun Kumar K
 * legends:
 *      MFB - Myntra FeedBack
 *      MFBQ - Myntra FeedBack Question
 */

include_once($xcart_dir."/include/func/func.db.php");


class MFBQuestion{

    const MFBQTABLE = "mk_feedback_question";//table

    public $MFBOption;

    function __construct(){
        $this->_initializeMFBQ();
    }

    /*
     * initializes MFBQuestion with all necessary objects
     * so that not necessary to instantiate objects for each.
     * @return: void
     */
    public function _initializeMFBQ(){
        $this->MFBOption  = new MFBOption;
    }
    
    /*
     * gets the priority for SR
     * @param:(array)$filter - can be different columns of MFBQTABLE
     * @param:(array)$order - can be ordered by columns of MFBQTABLE
     * @return:(array)$question - all info of question
     */
    public function getMFBQuestions($filter,$order=null){
        $sql = "select
                    *
                from
                    " . self::MFBQTABLE;

        $MFBQuestions = sanitizedQueryBuilder($sql,$filter,$order);
        return $MFBQuestions;
    }
    // TODO MFB implementation
}
?>