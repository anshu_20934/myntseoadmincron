<?php 

class LoyaltyPointsAccountTransaction{

		private $login;
		private $activeCreditPoints =0;
		private $inActiveCreditPoints =0;
		private $activeDebitPoints =0;
		private $inActiveDebitPoints =0;
		private $description;
		private $itemType;
		private $itemId;
		private $businessProcess;
		
		
		function __construct($login,
				$activeCreditPoints,
		  $inActiveCreditPoints,
		  $activeDebitPoints,
		  $inActiveDebitPoints,
		  $description,
		  $itemType,
		  $itemId,
		  $businessProcess){
			
			$this->login = $login;
			$this->activeCreditPoints = $activeCreditPoints;
			$this->inActiveCreditPoints = $inActiveCreditPoints;
			$this->activeDebitPoints = $activeDebitPoints;
			$this->inActiveDebitPoints = $inActiveDebitPoints;
			$this->description = $description;
			$this->itemType = $itemType;
			$this->itemId = $itemId;
			$this->businessProcess = $businessProcess;
			
			
		}
		
		function toJson(){
			$tansaction = array();
			$tansaction["login"] = $this->login;
			$tansaction["itemType"] = $this->itemType;
			$tansaction['itemId'] = $this->itemId;
			$tansaction["activeCreditPoints"] = $this->activeCreditPoints ;
			$tansaction["inActiveCreditPoints"] = $this->inActiveCreditPoints ;
			$tansaction["activeDebitPoints"] = $this->activeDebitPoints ;
			$tansaction["inActiveDebitPoints"] = $this->inActiveDebitPoints ;
			$tansaction["description"] = $this->description ;
			$tansaction["businessProcess"] = $this->businessProcess;
			
			return json_encode($tansaction);
		}
			



}