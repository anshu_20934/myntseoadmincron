<?php

include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalClient.php";


class LoyaltyPointsPortalHelper{

	const LOYALTY_POINTS_CACHED_TIER_INFO = 'loyalty_points_cache_tier_info';

	public static function fetchLoyaltyPointsDetailsFromSevice($login){
		$loyaltyPointsHeaderDetails = LoyaltyPointsPortalClient::getAccountInfo($login);
		// Auto accept TNC for new user. This come in last moment and fixed here, as there is already a service call here. Need more discussion and reviews.
        if(isset($loyaltyPointsHeaderDetails["tNcAccepted"])  && !$loyaltyPointsHeaderDetails["tNcAccepted"]){
        	$setTNCResponse = LoyaltyPointsPortalClient::setTNCaccepted($login);
        	if(!empty($setTNCResponse) && !empty($setTNCResponse['status']['statusType']) && $setTNCResponse['status']['statusType'] == 'SUCCESS'){
        		// No need to do anything if success
        	}else{
        		// Log the error if tnc 
        		$errorlog->error("Loyalty Points Service Error! LoyaltyPointsPortalHelper:setLoyaltyPointsHeaderDetails , setTNCResponse : ". print_r($setTNCResponse,true));
        	}
        	// After auto accepting TNC we need to fetch the header details again
        	$loyaltyPointsHeaderDetails = LoyaltyPointsPortalClient::getAccountInfo($login);
        }
        return $loyaltyPointsHeaderDetails;
	}

	public static function setLoyaltyPointsHeaderDetails($login){
		global $XCART_SESSION_VARS,$smarty,$weblog,$errorlog;
		$loyaltyPointsHeaderDetails = array();
	    if(empty($XCART_SESSION_VARS['loyaltyPointsHeaderDetails']) || (!empty($XCART_SESSION_VARS['loyaltyPointsHeaderDetails']['updatedTimeStamp']) && (time() - $XCART_SESSION_VARS['loyaltyPointsHeaderDetails']['updatedTimeStamp']) > 300)){
	        $loyaltyPointsHeaderDetails = LoyaltyPointsPortalHelper::fetchLoyaltyPointsDetailsFromSevice($login);
	        $weblog->debug("setLoyaltyPointsHeaderDetails : Fetching loyalty points details and cashing in session for header");
	        $loyaltyPointsHeaderDetails['updatedTimeStamp'] = time();
	        $XCART_SESSION_VARS['loyaltyPointsHeaderDetails'] = $loyaltyPointsHeaderDetails;
	    }else{
	    	$weblog->debug("setLoyaltyPointsHeaderDetails : Using loyaltypoints cashed session");
	        $loyaltyPointsHeaderDetails = $XCART_SESSION_VARS['loyaltyPointsHeaderDetails'];
	    }
	    $smarty->assign("loyaltyPointsHeaderDetails",$loyaltyPointsHeaderDetails);
	}

	public static function resetLoyaltyPointsHeaderDetails($login){
		global $XCART_SESSION_VARS,$smarty,$weblog;
		// if (x_session_is_registered("loyaltyPointsHeaderDetails"))
  //        x_session_unregister("loyaltyPointsHeaderDetails");    
		$weblog->debug("resetLoyaltyPointsHeaderDetails : Resetting loyalty points details for header.");
		$loyaltyPointsHeaderDetails = LoyaltyPointsPortalHelper::fetchLoyaltyPointsDetailsFromSevice($login);
        $loyaltyPointsHeaderDetails['updatedTimeStamp'] = time();
        $XCART_SESSION_VARS['loyaltyPointsHeaderDetails'] = $loyaltyPointsHeaderDetails;
        $smarty->assign("loyaltyPointsHeaderDetails",$loyaltyPointsHeaderDetails);
	}

	public static function getTierInfo(){
		global $xcache;
		if($xcache==null){
			$xcache = new XCache();
		}
		$tierSlabsInfo = $xcache->fetchObject(self::LOYALTY_POINTS_CACHED_TIER_INFO);
		if($tierSlabsInfo == NULL) {
			// Fetch tier information from the services
			$loyaltyPointsDetails = LoyaltyPointsPortalClient::getAccountInfoForMyMyntra('test');
			if(empty($loyaltyPointsDetails) || (!empty($loyaltyPointsDetails['status']) && $loyaltyPointsDetails['status']['statusMessage'] != 'SUCCESS')){
				//Error condition send loyalty error page
				return false;
			}
			$tierSlabsInfo = $loyaltyPointsDetails['tierInfo']['tierSlabsInfo'];
			$xcache->store(self::LOYALTY_POINTS_CACHED_TIER_INFO, $tierSlabsInfo, 604800);
		}
		return $tierSlabsInfo;
	}
}

?>