<?php 

include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";

class LoyaltyPointsHelper{
	
	public static function applyLoyaltyPointsUsageOnCart(MCart $cart,$userEnteredLoyaltyPoints, $avaliableLoyaltyPoints,$conversionFactor)
	{
		
	
		/*
		 * cart value after discounts 
		 */
		$cartSubTotal = $cart->getCartSubtotal() - $cart->getTotalCouponDiscount();
		
		$loyaltyPointsConversionFactor = $conversionFactor;//FeatureGateKeyValuePairs::getFloat("loyaltyPointsConversionFactor",1);
		
		if($loyaltyPointsConversionFactor == 0 || $loyaltyPointsConversionFactor == null){
			return ;
		}
		
		$avaliableLoyaltyPointsCash = $avaliableLoyaltyPoints * $loyaltyPointsConversionFactor;
		$userEnteredLoyaltyPointsCash = $userEnteredLoyaltyPoints * $loyaltyPointsConversionFactor;
	
		$totalUsage = ($cartSubTotal > $avaliableLoyaltyPointsCash) ? $avaliableLoyaltyPointsCash : $cartSubTotal;
		$totalUsage = ($totalUsage > $userEnteredLoyaltyPointsCash) ? $userEnteredLoyaltyPointsCash : $totalUsage;
	
		
		foreach($cart->getProducts() as $id=>$product){
			$productSubtotal =
			(($product->getProductPrice()
					+ $product->getUnitAdditionalCharges())
					* $product->getQuantity()) - $product->getDiscount() - ($product->getDiscountAmount() * $product->getDiscountQuantity()) - $product->getCartDiscount();
				
			$productUsage = $totalUsage * ($productSubtotal / $cartSubTotal);
				
			$product->setLoyaltyPointsUsed(($productUsage/$loyaltyPointsConversionFactor),$loyaltyPointsConversionFactor);
		}
	
		$remainingCashAmount = $userEnteredLoyaltyPointsCash - $totalUsage;
		$shippingOrGiftingCharges = $cart->getShippingCharge() + $cart->getGiftCharge();
		$shippingOrGiftingUsage = 0.0;
		if( $remainingCashAmount > 0 && $shippingOrGiftingCharges > 0) {
			if($remainingCashAmount >= $shippingOrGiftingCharges)
				$shippingOrGiftingUsage = $shippingOrGiftingCharges;
			else
				$shippingOrGiftingUsage = $remainingCashAmount;
				
			$cart->setGiftOrShippingLoyaltyPointsUsage($shippingOrGiftingUsage/$loyaltyPointsConversionFactor);
		}else{
			$cart->setGiftOrShippingLoyaltyPointsUsage(0.0);
		}
				
		$cart->setLoyaltyPointsUsage($loyaltyPointsConversionFactor);
	
	}
	
	public static function removeLoyaltyPointsUsage(MCart $cart){
		foreach ($cart->getProducts() as $product){
			$product->setLoyaltyPointsUsed(0, 0);
		}
		$cart->setGiftOrShippingLoyaltyPointsUsage(0);
		$cart->setLoyaltyPointsUsage(0);
	}
	
	
}