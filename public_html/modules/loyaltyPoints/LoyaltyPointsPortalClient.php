<?php 

include_once ("$xcart_dir/modules/RestAPI/RestRequest.php");
include_once ("$xcart_dir/modules/loyaltyPoints/LoyaltyPointsAccountTransaction.php");
include_once ("$xcart_dir/modules/loyaltyPoints/LoyaltyPointsStyleObject.php");

class LoyaltyPointsPortalClient{
	
	

	/*
	 *  service response codes
	*/
	const serviceNotAvailable = "serviceNotAvailable";
	const noEnoughBalanceForDebitRequest = "noEnoughBalanceForDebitRequest";
	const creditDebitFailure = "creditDebitFailure";
	
	const ACTIVE_ACCOUNT = 1;
	const INACTIVE_ACCOUNT = 2;
	const CONSOLIDATED = 3;
	
	const SOCIAL_LIKE = "SOCIAL_LIKE";
	const SOCIAL_SHARE = "SOCIAL_SHARE";
	

	
	public static function getAccountInfo($login, $cumulativeInfo=false){
		$profileHandler = Profiler::startTiming("loyalty points service-call : getAccountInfo");
		
		$cumulativeUrl = "";
		if($cumulativeInfo){
			$cumulativeUrl="cumulative/";
		}
		
		$url = HostConfig::$loyaltyPointsService."/loyalty/loyaltyPoints/accountInfo/$cumulativeUrl".$login;
		
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
		
		$weblog->error("get loyalty points account info : $url");
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);
		
		$restRequest = new RestRequest($url);
		
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
		
		$weblog->error("get loyalty points account info : $responseBody");
		Profiler::endTiming($profileHandler);
		

		return isset($returnObject["userAccountEntry"])?$returnObject["userAccountEntry"]
		:array("activePointsBalance"=>0,"inActivePointsBalance"=>0,"cumulativeActivePoints"=>0,"login"=>$login);

	}
	
	public static function getTransactionHistory($login, $accountType=LoyaltyPointsPortalClient::ACTIVE_ACCOUNT,$start=0,$size=100){
		$profileHandler = Profiler::startTiming("loyalty points service-call : getTransactionHistory");
	
		$accountUrl = "activePoints";
		if($accountType == LoyaltyPointsPortalClient::INACTIVE_ACCOUNT){
			$accountUrl="inActivePoints";
		}else if($accountType == LoyaltyPointsPortalClient::CONSOLIDATED){
			$accountUrl = "consolidated";
		}
	
		$url = HostConfig::$loyaltyPointsService."/loyalty/loyaltyPoints/transactionHistory/$accountUrl/".$login."?start=$start&size=$size";
		
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
	
		$weblog->error("get loyalty points transaction history url : $url");
	
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);
	
		$restRequest = new RestRequest($url);
	
	
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
		
		$weblog->error("get loyalty points transaction history : $responseBody");
		Profiler::endTiming($profileHandler);
	
		if($accountType == LoyaltyPointsPortalClient::ACTIVE_ACCOUNT){
			return $returnObject["activePointsTransactionHistory"];
		}elseif($accountType == LoyaltyPointsPortalClient::INACTIVE_ACCOUNT){
			return $returnObject["inActivePointsTransactionHistory"];			
		}elseif($accountType == LoyaltyPointsPortalClient::CONSOLIDATED){
			return $returnObject;			
		}else{
			return array();
		}
		
	
	}

	public static function getOrderHistory($orderid, $start=0,$size=100){
		$profileHandler = Profiler::startTiming("loyalty points service-call : getOrderHistory");
	
		$url = HostConfig::$loyaltyPointsService."/loyalty/loyaltyPointsManager/order/".$orderid."?start=$start&size=$size";
		
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
	
		$weblog->error("get loyalty points transaction history url : $url");
	
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);
	
		$restRequest = new RestRequest($url);
	
	
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
		
		$weblog->error("get loyalty points order history : $responseBody");
		Profiler::endTiming($profileHandler);

		return $returnObject['logs'];
	}
	
	public static function creditActivePointsForSocialAction($loyaltyPointsAction, $login){
		// TODO
	}
	
	public static function setTNCaccepted($login){
		$profileHandler = Profiler::startTiming("loyalty points service-call : getAccountInfo");
		
		
		$url = HostConfig::$loyaltyPointsService."/loyalty/loyaltyPointsManager/users/acceptTNC/".$login;
		
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
		
		$weblog->error("set TNC loyaltypoints: $url");
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);
		
		$restRequest = new RestRequest($url,"POST","");
		
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
		
		$weblog->error("set TNC loyaltypoints: $responseBody");
		Profiler::endTiming($profileHandler);
		
		
		return $returnObject;
	}
	
	public static function getAccountInfoForMyMyntra($login){
		$profileHandler = Profiler::startTiming("loyalty points service-call : getAccountInfo");
		
		
		$url = HostConfig::$loyaltyPointsService."/loyalty/loyaltyPointsManager/myMyntra/accountInfo/".$login;
		
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
		
		$weblog->error("get loyalty points account info : $url");
		
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);
		
		$restRequest = new RestRequest($url);
		
		
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
		
		$weblog->error("get loyalty points account info : $responseBody");
		Profiler::endTiming($profileHandler);
		
		
		return $returnObject;
	}
	
	
	public static function debitForOrderUsage($login,$orderId,$points,$description){
		$profileHandler = Profiler::startTiming("loyalty points service-call : debitForOrderUsage");
        $points = floor($points);
	
		$url = HostConfig::$loyaltyPointsService."/loyalty/loyaltyPointsManager/debit/order";
	
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
	
		$weblog->error("get loyalty points account info : $url");
	
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);
	
		$input = array("login"=>$login,
				"orderId"=>$orderId,
				"points"=>$points,
				"description"=>$description
				
		);
		$restRequest = new RestRequest($url,"POST",json_encode($input));
	
	
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
	
		$weblog->error("get loyalty points debitForOrderUsage : $responseBody");
		Profiler::endTiming($profileHandler);
	
	
		return $returnObject;
	}
	
	
	public static function creditDebitActiveForExchangeOrder($login,$returnOrderId,$newOrderId,$points){
		
		$points =  ceil($points);
		
		$profileHandler = Profiler::startTiming("loyalty points service-call : creditDebitForExchangeOrder");
	
	
		$url = HostConfig::$loyaltyPointsService."/loyalty/loyaltyPointsManager/creditDebitActivePointForExchange";
	
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
	
		$weblog->error("get loyalty points account info : $url");
	
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);

		$description = "Refunding points used for order no ".$returnOrderId;
	
		$returnedOrder = array("login"=>$login,
				"orderId"=>$returnOrderId,
				"points"=>$points,
				"description"=>$description	
		);

		$description = "Redeeming points on exchange order no ".$newOrderId;
	
		$newOrder = array("login"=>$login,
				"orderId"=>$newOrderId,
				"points"=>$points,
				"description"=>$description	
		);

		$input =array("returnedOrder"=>$returnedOrder,
				"newOrder"=>$newOrder
		);
		
		
		$restRequest = new RestRequest($url,"POST",json_encode($input));
	
	
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
		
		$weblog->error("get loyalty points creditDebitActiveForExchangeOrder : $responseBody");
		Profiler::endTiming($profileHandler);
	
	
		return $returnObject;
	}
	
	public static function creditDebitInactiveForExchangeOrder($login,$returnOrderId,$newOrderId,$points,$description1,$description2){
		
		$points =  ceil($points);
		
		$profileHandler = Profiler::startTiming("loyalty points service-call : creditDebitForExchangeOrder");
	
	
		$url = HostConfig::$loyaltyPointsService."/loyalty/loyaltyPointsManager/creditDebitInactivePointForExchange";
	
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
	
		$weblog->error("get loyalty points account info : $url");
	
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);

		$description = "Reversing credit of points for order no ".$returnOrderId;
	
		$returnedOrder = array("login"=>$login,
				"orderId"=>$returnOrderId,
				"points"=>$points,
				"description"=>$description1	
		);

		//$description = "Awarding points for exchange order no ".$newOrderId;
	
		$newOrder = array("login"=>$login,
				"orderId"=>$newOrderId,
				"points"=>$points,
				"description"=>$description2	
		);

		$input =array("returnedOrder"=>$returnedOrder,
				"newOrder"=>$newOrder
		);
		
		
		$restRequest = new RestRequest($url,"POST",json_encode($input));
	
	
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
		
		$weblog->error("get loyalty points creditDebitInactiveForExchangeOrder : $responseBody");
		Profiler::endTiming($profileHandler);
	
	
		return $returnObject;
	}
	
	public static function creditForOrderConfirmation($login,$orderId,$points,$description){
		
		$points =  ceil($points);
		
		$profileHandler = Profiler::startTiming("loyalty points service-call : creditForOrderConfirmation");
	
	
		$url = HostConfig::$loyaltyPointsService."/loyalty/loyaltyPointsManager/credit/orderConfirmation";
	
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
	
		$weblog->error("get loyalty points account info : $url");
	
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);
	
		$input = array("login"=>$login,
				"orderId"=>$orderId,
				"points"=>$points,
				"description"=>$description
	
		);
		$restRequest = new RestRequest($url,"POST",json_encode($input));
	
	
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
	
		$weblog->error("get loyalty points creditForOrderConfirmation : $responseBody");
		Profiler::endTiming($profileHandler);
	
	
		return $returnObject;
	}
	
	
	public static function creditForItemCancellation($login,$orderId,$points,$description){
		
		$points =  ceil($points);
		
		$profileHandler = Profiler::startTiming("loyalty points service-call : creditForOrderConfirmation");
	
	
		$url = HostConfig::$loyaltyPointsService."/loyalty/loyaltyPointsManager/credit/itemCancellation";
	
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
	
		$weblog->error("get loyalty points account info : $url");
	
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);
	
		$input = array("login"=>$login,
				"orderId"=>$orderId,
				"points"=>$points,
				"description"=>$description
	
		);
		$restRequest = new RestRequest($url,"POST",json_encode($input));
	
	
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
	
		$weblog->error("get loyalty points creditForOrderConfirmation : $responseBody");
		Profiler::endTiming($profileHandler);
	
	
		return $returnObject;
	}
	
	
	public static function debitForItemCancellation($login,$orderId,$points,$description){
		
	    $points = floor($points);
		$profileHandler = Profiler::startTiming("loyalty points service-call : creditForOrderConfirmation");
	
	
		$url = HostConfig::$loyaltyPointsService."/loyalty/loyaltyPointsManager/debit/itemCancellation";
	
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
	
		$weblog->error("get loyalty points account info : $url");
	
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);
	
		$input = array("login"=>$login,
				"orderId"=>$orderId,
				"points"=>$points,
				"description"=>$description
	
		);
		$restRequest = new RestRequest($url,"POST",json_encode($input));
	
	
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
	
		$weblog->error("get loyalty points creditForOrderConfirmation : $responseBody");
		Profiler::endTiming($profileHandler);
	
	
		return $returnObject;
	}	
	
	public static function activatePointsForOrder($login,$orderId,$points,$description){
		
		$points =  ceil($points);
		
		$profileHandler = Profiler::startTiming("loyalty points service-call : creditForOrderConfirmation");
	
	
		$url = HostConfig::$loyaltyPointsService."/loyalty/loyaltyPointsManager/activatePoints";
	
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
	
		$weblog->error("get loyalty points points activation : $url");
	
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);
	
		$input = array("login"=>$login,
				"orderId"=>$orderId,
				"points"=>$points,
				"description"=>$description
	
		);
		$restRequest = new RestRequest($url,"POST",json_encode($input));
	
	
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
	
		$weblog->error("get loyalty points points activation : $responseBody");
		Profiler::endTiming($profileHandler);
	
	
		return $returnObject;
	}
	
	public static function debit(LoyaltyPointsAccountTransaction $transaction){
		$profileHandler = Profiler::startTiming("loyalty points service-call : debit");
	
	
		$url = HostConfig::$loyaltyPointsService."/loyalty/loyaltyPoints/debit";
	
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
	
		$weblog->error("get loyalty points account info : $url");
	
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);
	
		$restRequest = new RestRequest($url,"POST",$transaction->toJson());
	
	
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
	
		$weblog->error("get loyalty points debit : $responseBody");
		Profiler::endTiming($profileHandler);
	
	
		return $returnObject;
	}
	
	public static function credit(LoyaltyPointsAccountTransaction $transaction){
		$profileHandler = Profiler::startTiming("loyalty points service-call : credit");
	
		
		$url = HostConfig::$loyaltyPointsService."/loyalty/loyaltyPoints/credit";
	
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
	
		$weblog->error("get loyalty points account info : $url");
	
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);
	
		
		$restRequest = new RestRequest($url,"POST",$transaction->toJson());
	
	
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
	
		$weblog->error("get loyalty points credit : $responseBody");
		Profiler::endTiming($profileHandler);
	
	
		return $returnObject;
	}
	
	public static function getConfirmationOrderInfo($orderid){
		
		return array("orderId"=>$orderid,"activePointsCredited"=>0,"inActivePointsCredited"=>0);
		
		$profileHandler = Profiler::startTiming("loyalty points service-call : getAccountInfo");
	
		$cumulativeUrl = "";
		if($cumulativeInfo){
			$cumulativeUrl="cumulative/";
		}
	
		$url = HostConfig::$loyaltyPointsService."/loyalty/loyaltyPoints/accountInfo/$cumulativeUrl".$login;
	
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
	
		$weblog->error("get loyalty points account info : $url");
	
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$username);
	
		$restRequest = new RestRequest($url);
	
	
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
	
		$weblog->error("get loyalty points account info : $responseBody");
		Profiler::endTiming($profileHandler);
	
	
		return isset($returnObject["userAccountEntry"])?$returnObject["userAccountEntry"]
		:array("activePointsBalance"=>0,"inActivePointsBalance"=>0,"cumulativeActivePoints"=>0,"login"=>$login);
	
	}

	public static function getStyleLoyaltyFactor(LoyaltyPointsStyleObject $style){
		$profileHandler = Profiler::startTiming("loyalty points service-call : style conversion factor");
	
		
		$url = HostConfig::$loyaltyPointsService."/loyalty/loyaltyPointsManager/index/stylesList";
	
		global $XCART_SESSION_VARS,$weblog;
		$weblog->error("get loyalty points for style : $style->getStyleId()");
	
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');	
		
		$request = array();
		$request["styleObjects"] = array($style->toArray());

        $restRequest = new RestRequest($url,"POST",json_encode($request));

		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
	
		$weblog->error("get loyalty points for style :");
		Profiler::endTiming($profileHandler);
		
		if($returnObject["status"]["statusType"] == "SUCCESS"){
		    return $returnObject["loyaltypointsStyleIndex"][0]["awardFactor"];
		}
		return 0;	
	}

	public static function getTierLogs($login, $start=0,$size=100){
		$profileHandler = Profiler::startTiming("loyalty points service-call : getOrderHistory");
	
		$url = HostConfig::$loyaltyPointsService."/loyalty/loyaltyPoints/accountTierInfo/".$login."?start=$start&size=$size";
		
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
	
		$weblog->error("get loyalty points tier url : $url");
	
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
	
		$restRequest = new RestRequest($url);
	
	
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
		
		$weblog->error("get loyalty points tier : $responseBody");
		Profiler::endTiming($profileHandler);

		return $returnObject['userGroupEntry'];
	}

	public static function computeTierInfo($login, $start=0,$size=100){
		$profileHandler = Profiler::startTiming("loyalty points service-call : getOrderHistory");
	
		$url = HostConfig::$loyaltyPointsService."/loyalty/loyaltyPointsManager/process/users/computeTierInfo/".$login;
		
		global $XCART_SESSION_VARS,$weblog;
		$username = $XCART_SESSION_VARS['login'];
	
		$weblog->error("get loyalty points tier url : $url");
		$request = "";
	
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
	
		$restRequest = new RestRequest($url,"POST",json_encode($request));
	
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		$returnObject = json_decode($responseBody,true);
		
		$weblog->error("get loyalty points tier : $responseBody");
		Profiler::endTiming($profileHandler);

		return $returnObject;
	}

	
}

