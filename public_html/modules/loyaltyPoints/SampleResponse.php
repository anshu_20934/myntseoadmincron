<?php 
$myMyntraResponse = '
{
    "status": {
        "statusCode": 1,
        "statusMessage": "SUCCESS",
        "statusType": "SUCCESS",
        "totalCount": 0
    },
    "userAccountInfo": {
        "activePointsBalance": 500,
        "inActivePointsBalance": 500,
        "tNcAccepted": false,
        "thresholdForUsage": 10,
        "conversionFactor": 2,
        "cashEqualantAmount": 1000,
        "cumulativeActivePoints": 0,
        "login": "rgv@local.com"
    },
    "tierInfo": {
        "presentTierNumber": 1,
        "loyaltyPointsUserAwardFactor": ".07",
        "validUpto": 1408555000,
        "tierSlabsInfo": [
            {
                "tierNumber": 0,
                "loyaltyPointsUserAwardFactor": ".06",
                "minPurchaseValue": -9000,
                "tierMessage": "test message 1"
            },
            {
                "tierNumber": 1,
                "loyaltyPointsUserAwardFactor": ".7",
                "minPurchaseValue": 2300,
                "tierMessage": "150 welcome points"
            },
            {
                "tierNumber": 2,
                "loyaltyPointsUserAwardFactor": ".8",
                "minPurchaseValue": 4300,
                "tierMessage": "500 upgrade points"
            },
            {
                "tierNumber": 3,
                "loyaltyPointsUserAwardFactor": ".9",
                "minPurchaseValue": 5000,
                "tierMessage": "1000 upgrade points"
            }
        ]
    },
    "activePointsTransactionHistory": [
        {
            "loyaltyPointsAccountId": 19,
            "itemId": 0,
            "itemType": "ORDER",
            "businesProcess": "POINTS_ACTIVATED",
            "creditInflow": 1000,
            "creditOutflow": 0,
            "balance": 1000,
            "modifiedBy": "",
            "modifiedOn": 1377013147000,
            "description": "Activated points for order id : 123"
        },
        {
            "loyaltyPointsAccountId": 19,
            "itemId": 124,
            "itemType": "ORDER",
            "businesProcess": "POINTS_ACTIVATED",
            "creditInflow": 1000,
            "creditOutflow": 0,
            "balance": 2000,
            "modifiedBy": "",
            "modifiedOn": 1377013972000,
            "description": "test test"
        }
    ],
    "inActivePointsTransactionHistory": [
        {
            "loyaltyPointsAccountId": 20,
            "itemId": 123,
            "itemType": "ORDER",
            "businesProcess": "POINTS_AWARDED",
            "creditInflow": 1000,
            "creditOutflow": 0,
            "balance": 1000,
            "modifiedBy": "",
            "modifiedOn": 1377011602000,
            "description": "3. test test"
        },
        {
            "loyaltyPointsAccountId": 20,
            "itemId": 0,
            "itemType": "ORDER",
            "businesProcess": "POINTS_ACTIVATED",
            "creditInflow": 0,
            "creditOutflow": 1000,
            "balance": 0,
            "modifiedBy": "",
            "modifiedOn": 1377013147000,
            "description": "Activated points for order id : 123"
        },
        {
            "loyaltyPointsAccountId": 20,
            "itemId": 124,
            "itemType": "ORDER",
            "businesProcess": "POINTS_AWARDED",
            "creditInflow": 1000,
            "creditOutflow": 0,
            "balance": 1000,
            "modifiedBy": "",
            "modifiedOn": 1377013743000,
            "description": "test test"
        },
        {
            "loyaltyPointsAccountId": 20,
            "itemId": 124,
            "itemType": "ORDER",
            "businesProcess": "POINTS_ACTIVATED",
            "creditInflow": 0,
            "creditOutflow": 1000,
            "balance": 0,
            "modifiedBy": "",
            "modifiedOn": 1377013972000,
            "description": "Test test"
        }
    ]
}';

?>