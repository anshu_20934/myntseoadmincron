<?php 

class LoyaltyPointsStyleObject{

        private $styleId;
        private $brand;
        private $gender;
        private $articleType;
        
        function __construct($styleId,
                $brand,
          $gender,
          $articleType){
            
            $this->styleId = $styleId;
            $this->brand = $brand;
            $this->gender = $gender;
            $this->articleType = $articleType;
        }

        public function getStyleId(){
            return $this->styleId;
        }

        function toArray(){
            $style = array();
            $style["styleId"] = $this->styleId;
            $style["brand"] = $this->brand;
            $style['gender'] = $this->gender;
            $style["articleType"] = $this->articleType ;
            return $style;
        }
}