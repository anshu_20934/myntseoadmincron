<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: func.php,v 1.4 2006/01/11 06:56:21 mclap Exp $
#
# Functions for XAffiliate module
#

if ( !defined('XCART_START') ) { header("Location: home.php"); die("Access denied"); }

#
# Get partner affliates
#
function func_get_affiliates($user, $level = -1) {
	global $sql_tbl, $config;

	if(!$user)
		return false;

	if($level == -1)
		$level = func_get_affiliate_level($user);
	$childs = func_query("SELECT * FROM $sql_tbl[customers] WHERE parent = '$user'");
	if($childs) {
		for($x = 0; $x < count($childs); $x++) {
			$childs[$x]['level'] = func_get_affiliate_level($childs[$x]['login']);
			$childs[$x]['sales'] = func_query_first_cell("SELECT SUM(commissions) FROM $sql_tbl[partner_payment] WHERE login = '".$childs[$x]['login']."'");
			$tmp = func_get_affiliates($childs[$x]['login'], $level+1);
			$childs_sales = 0;
			if($tmp) {
				$childs[$x]['childs'] = $tmp;
				for($y = 0; $y < count($tmp); $y++)
					$childs_sales += $tmp[$y]['sales']+$tmp[$y]['childs_sales'];
			}
			$childs[$x]['childs_sales'] = $childs_sales;
		}
	}
	return $childs;
}

#
# Get afiiliate level
#
function func_get_affiliate_level($user) {
	global $sql_tbl;

	if(!$user)
		return false;

	$level = 0;
	do {
		$user = func_query_first_cell("SELECT parent FROM $sql_tbl[customers] WHERE login = '$user'");
		$level++;
	} while($user);
	return $level;
}

#
# Get parents array
#
function func_get_parents($user) {
	global $sql_tbl, $config;
	$parent = func_query_first_cell("SELECT parent FROM $sql_tbl[customers] WHERE login = '$user'");
	if($parent) {
		$parents[] = array("login" => $parent, "level" => func_get_affiliate_level($parent));
		$parents = func_array_merge($parents, func_get_parents($parent));
	}
	return $parents;
}

?>
