<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: config.php,v 1.9 2006/01/11 06:56:21 mclap Exp $
#

if ( !defined('XCART_START') ) { header("Location: ../../"); die("Access denied"); }
#
# Global definitions for X-Affiliate module
#

$addons['XAffiliate'] = true;

$sql_tbl["partner_adv_campaigns"] = "xcart_partner_adv_campaigns";
$sql_tbl["partner_adv_clicks"] = "xcart_partner_adv_clicks";
$sql_tbl["partner_adv_orders"] = "xcart_partner_adv_orders";
$sql_tbl["partner_banners"] = "xcart_partner_banners";
$sql_tbl["partner_banners_elements"] = "xcart_partner_banners_elements";
$sql_tbl["partner_clicks"] = "xcart_partner_clicks";
$sql_tbl["partner_commissions"] = "xcart_partner_commissions";
$sql_tbl["partner_payment"] = "xcart_partner_payment";
$sql_tbl["partner_plans"] = "xcart_partner_plans";
$sql_tbl["partner_plans_commissions"] = "xcart_partner_plans_commissions";
$sql_tbl["partner_product_commissions"] = "xcart_partner_product_commissions";
$sql_tbl["partner_commissions"] = "xcart_partner_commissions";
$sql_tbl["partner_tier_commissions"] = "xcart_partner_tier_commissions";
$sql_tbl["partner_views"] = "xcart_partner_views";

if (defined("TOOLS")) {
	$tbl_keys["partner_clicks.login"] = array(
		"keys" => array("partner_clicks.login" => "customers.login"),
		"where" => "customers.usertype = 'B'",
		"fields" => array("clickid","bannerid")
	);
	$tbl_keys["partner_clicks.bannerid"] = array(
		"keys" => array("partner_clicks.bannerid" => "partner_banners.bannerid"),
		"fields" => array("clickid","login")
	);
	$tbl_keys["partner_clicks.productid"] = array(
		"keys" => array("partner_clicks.productid" => "products.productid"),
		"where" => "partner_clicks.productid != 0",
		"fields" => array("clickid","bannerid","login")
	);
	$tbl_keys["partner_commissions.login"] = array(
		"keys" => array("partner_commissions.login" => "customers.login"),
		"where" => "customers.usertype = 'B'",
		"fields" => array("plan_id")
	);
	$tbl_keys["partner_commissions.plan_id"] = array(
		"keys" => array("partner_commissions.plan_id" => "partner_plans.plan_id"),
		"fields" => array("login")
	);
	$tbl_keys["partner_product_commissions.orderid"] = array(
		"keys" => array("partner_product_commissions.orderid" => "orders.orderid"),
		"fields" => array("itemid","login")
	);
	$tbl_keys["partner_product_commissions.itemid"] = array(
		"keys" => array("partner_product_commissions.itemid" => "order_details.itemid"),
		"fields" => array("orderid","login")
	);
	$tbl_keys["partner_product_commissions.login"] = array(
		"keys" => array("partner_product_commissions.login" => "customers.login"),
		"where" => "customers.usertype = 'B'",
		"fields" => array("orderid","itemid")
	);
	$tbl_keys["partner_payment.login"] = array(
		"keys" => array("partner_payment.login" => "customers.login"),
		"where" => "customers.usertype = 'B'",
		"fields" => array("payment_id","orderid")
	);
	$tbl_keys["partner_payment.orderid"] = array(
		"keys" => array("partner_payment.orderid" => "orders.orderid"),
		"fields" => array("payment_id","login")
	);
	$tbl_keys["partner_plans_commissions.plan_id"] = array(
		"keys" => array("partner_plans_commissions.plan_id" => "partner_plans.plan_id"),
		"fields" => array("commission","commission_type","item_id","item_type")
	);
	$tbl_keys["partner_views.login"] = array(
		"keys" => array("partner_views.login" => "customers.login"),
		"where" => "customers.usertype = 'B'",
		"fields" => array("bannerid","productid")
	);
	$tbl_keys["partner_views.bannerid"] = array(
		"keys" => array("partner_views.bannerid" => "partner_banners.bannerid"),
		"fields" => array("login","productid")
	);
	$tbl_keys["partner_views.productid"] = array(
		"keys" => array("partner_views.productid" => "products.productid"),
		"where" => "partner_views.productid != 0",
		"fields" => array("bannerid","login")
	);
	$tbl_keys["partner_adv_clicks.campaignid"] = array(
		"keys" => array("partner_adv_clicks.campaignid" => "partner_adv_campaigns.campaignid")
	);
	$tbl_keys["partner_adv_orders.campaignid"] = array(
		"keys" => array("partner_adv_orders.campaignid" => "partner_adv_campaigns.campaignid"),
		"fields" => array("orderid")
	);
	$tbl_keys["partner_adv_orders.orderid"] = array(
		"keys" => array("partner_adv_orders.orderid" => "orders.orderid"),
		"fields" => array("campaignid")
	);
	$tbl_demo_data[] = 'partner_adv_campaigns';
	$tbl_demo_data[] = 'partner_adv_clicks';
	$tbl_demo_data[] = 'partner_adv_orders';
	$tbl_demo_data[] = 'partner_banners';
	$tbl_demo_data[] = 'partner_banners_elements';
	$tbl_demo_data[] = 'partner_clicks';
	$tbl_demo_data[] = 'partner_commissions';
	$tbl_demo_data[] = 'partner_payment';
	$tbl_demo_data[] = 'partner_plans';
	$tbl_demo_data[] = 'partner_plans_commissions';
	$tbl_demo_data[] = 'partner_product_commissions';
	$tbl_demo_data[] = 'partner_commissions';
	$tbl_demo_data[] = 'partner_tier_commissions';
	$tbl_demo_data[] = 'partner_views';
}

?>
