<?php
require_once("$xcart_dir/modules/Registration/registration_module.php");
require_once("$xcart_dir/include/fb/facebook_login.php");


/**
     *    Facebook Registration class implementing registration process when user uses facebook connect.
     *    @example		<code>
     *    					$registration = new FacebookRegistration("password");
     *    					$registration->executeActions();
     *    				</code>
     *        
	 *    @package		Registration
	 *    @author		Mitesh Gupta (mitesh.gupta@myntra.com)
*/


class FacebookRegistration extends Registration {
	protected $fblogin,$facebook,$fb_me,$fb_friends,$fb_likes,$sendForgotPasswdEmail=false;
	
	/**
	 *		Constructor to get a object of this class.
	 *		@throws Exception Message = "Facebook API Exception" Code=11, if the login field is null or empty.
	 *		@throws Exception Message = "You seemed to be logged out of Facebook, please try again" Code=12, if the user is not logged into facebook.
	 *		@throws Exception Message = "Login Already Exists" Code=1, if the login entry already exists in the customers database.
	 * 		@param string $passwd (Optional) Password for the user been registered. Default is null, if null a random password of length 10 is generated and a forgot password mail is sent to the user if configured through admin interface.
	 * 		@param Facebook $facebook (Optional) Facebook object to be used by registration. If passed this object will be used else a new Facebook object will be created using default appID and secret will be used. 
	 * 		@param string $usertype (Optional) Usertype for the login. Default value 'C'.
	 * 		@param string $channel (Optional) Channel for facebook registration. Default value 'FB'.
	 * 		@access public
	 * 		
	 */
	
	public function __construct($passwd=null,$fblogin=null,$fb_me=null,$fb_friends=null,$fb_likes=null,$usertype='C',$channel = 'FB'){
		
		if(empty($fblogin)){
			$fblogin = new Facebook_Login();
		}
		
		$facebook = $fblogin->facebook;
		$this->fblogin = $fblogin;
		$this->facebook = $facebook;
		$fb_uid = $facebook->getUser();
		
		if(empty($fb_me)){
			$facebook_session = $facebook->getUser();
			if ($facebook_session) {
				try {
					$userAlreadyPresent = func_query_first_cell("SELECT COUNT(*) FROM mk_facebook_user WHERE fb_uid='$fb_uid'");
    				if($userAlreadyPresent==0) {
			    		//make a entry into fb db
    					$entryQuery = "insert into mk_facebook_user (fb_uid) values ('$fb_uid')";
    					func_query($entryQuery);
    				}
    				$fb_me = $facebook->api('/me');
		  			/*$multiQuery = "{ 
				  		'query1':'SELECT uid,first_name,last_name,name,birthday_date,sex,hometown_location,relationship_status,current_location,activities,interests,music,tv,movies,books,quotes,about_me,email,family FROM user WHERE uid =$fb_uid',
			     		'query2':'SELECT uid,first_name,last_name,name,birthday_date,sex,hometown_location,current_location,family,relationship_status,activities,interests,music,tv,movies,books,quotes,about_me from user WHERE uid in (SELECT uid2 FROM friend WHERE uid1 = $fb_uid)'
     				}";
	  	
				  	$param = array(       
			     		'method' => 'fql.multiquery',       
			     		'queries' => $multiQuery,       
			     		'callback' => '');       
					$queryresults = $facebook->api($param);
				  	//$fb_data = $facebook->api(array("method"=>"fql.query","query"=>"SELECT uid,first_name,last_name,name,birthday_date,sex,hometown_location,relationship_status,current_location,activities,interests,music,tv,movies,books,quotes,about_me,email,family FROM user WHERE uid =$fb_uid"));
				    $fb_me = $queryresults[0]['fql_result_set'][0];
				    $fb_friends = $queryresults[1]['fql_result_set'];
				    $fb_likes = null;//$queryresults[2]['fql_result_set'];*/
				    $this->fb_me = $fb_me;
				    $this->fb_friends = $fb_friends;
				    $this->fb_likes = $fb_likes;
				} catch (FacebookApiException $e) {
					error_log($e);
					throw new Exception('Facebook API Exception',11);
				}
			} else {
				throw new Exception('You seemed to be logged out of Facebook, please try again',12);
			}
		} else {
			$this->fb_me = $fb_me;
			$this->fb_friends =$fb_friends;
			$this->fb_likes = $fb_likes;
		}
		
		if(empty($passwd)){
			$this->sendForgotPasswdEmail=true;
		}
		
		$referer = '';
		if($fb_me) {
			$fb_firstname = $fb_me["first_name"];
			$fb_lastname =  $fb_me["last_name"];
			$fb_email = $fb_me["email"];
			$fb_birthday = $fb_me["birthday"];
			$fb_aboutme = $fb_me["about_me"];
			$fb_gender = $fb_me["gender"];
			$referer = $fb_me["mrp_referer"];
			$relationship_status = $fb_me["relationship_status"];
			
		} else {
			throw new Exception('Facebook user info not present');
		}
		
		parent::__construct($fb_email,$usertype,$channel,$fb_firstname,$fb_lastname,$passwd);
		
		//set AboutMe
		$this->setAboutMe($fb_aboutme);
		
		//set Birthday
		$birth_date = date_create_from_format('m/d/Y',$fb_birthday);
		$this->setBirthdate($birth_date);
		
		//set Gender
		$this->setGender($fb_gender);
		
		// set Referer.
		$this->setReferer($referer);
		
		// set Relationship status.
		$this->setRelationshipStatus($relationship_status);
	}
	
	public static function post_async($url, $params)
	{
	    foreach ($params as $key => &$val) {
	      if (is_array($val)) $val = implode(',', $val);
	        $post_params[] = $key.'='.urlencode($val);
	    }
	    $post_string = implode('&', $post_params);
	
	    $parts=parse_url($url);
	
	    $fp = fsockopen($parts['host'],
	        isset($parts['port'])?$parts['port']:80,
	        $errno, $errstr, 30);
	
	    $out = "POST ".$parts['path']." HTTP/1.1\r\n";
	    $out.= "Host: ".$parts['host']."\r\n";
	    $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
	    $out.= "Content-Length: ".strlen($post_string)."\r\n";
	    $out.= "Connection: Close\r\n\r\n";
	    if (isset($post_string)) $out.= $post_string;
	
	    fwrite($fp, $out);
	    fclose($fp);
	}
	
	/**
	 * Override the parent class method to include saving the additional facebook data in the Facebook table mk_facebook_user.
	 * @access public
	 */
	
	public function saveToDB(){
		parent::saveToDB();
		$this->insertOrUpdateFaceBookDB();
	}
	
	/**
	 * Helper method called from {@link saveToDB()} method to populate additional facebook table mk_facebook_user
	 * @access protected 
	 */
	
	protected function insertOrUpdateFaceBookDB() {
		$fb_access_token = $this->facebook->getAccessToken();
    	$topass=array('fb_access_token'=>$fb_access_token);
		global $http_location;
		self::post_async(HostConfig::$selfHostURL."/include/fb/fb_database_update.php", $topass);
		//$this->fblogin->insertOrUpdateFaceBookDB($this->fb_me,$this->fb_friends,$this->fb_likes);		
	}
	
	/**
	 * Method to send a forgotpassword/password recovery/password reset mail to the user. 
	 * Calling this method can be configured through admin interface. If the supplied password is empty and admin has configured to send a password reset mail
	 * then a password recovery mail is sent to the user.
	 * @access public
	 * @return bool true if the mail is sent successfully. 
	 */
	
	public function sendPasswordResetEmail(){
		global $xcart_dir;
		if($this->sendForgotPasswdEmail){
			include_once "$xcart_dir/include/func/func.reset_password.php";
            return sendResetPasswordMail($this->login,"facebookregistation",$this->firstname);
		}
	}
}

?>