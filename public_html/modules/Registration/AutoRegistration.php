<?php
require_once("$xcart_dir/modules/Registration/registration_module.php");
require_once("$xcart_dir/include/fb/facebook_login.php");

/**
     *    Auto Registration class implementing registration process for automatically registering the user if desiered.
     *        
	 *    @package		Registration
	 *    @author		Mitesh Gupta (mitesh.gupta@myntra.com)
*/

class AutoRegistration extends Registration {
	
	private function postAsync($url,$post_string) {
		
		$parts=parse_url($url);
		$fp = fsockopen($parts['host'], isset($parts['port']) ? $parts['port'] : 80, $errno, $errstr, 30);
		
		$out = "POST ".$parts['path']." HTTP/1.1\r\n";
		$out.= "Host: ".$parts['host']."\r\n";
		$out.= "User-Agent: Curl Guest Reset Password\r\n";
		$out.= "Content-Type: application/x-www-form-urlencoded\r\n";
		$out.= "Content-Length: ".strlen($post_string)."\r\n";
		$out.= "Connection: Close\r\n\r\n";
		if (isset($post_string)) $out.= $post_string;

		fwrite($fp, $out);
		fclose($fp);
	}
	
	public function __construct($login,$usertype='C',$channel = 'AT',$firstname=null,$lastname=null,$address=null,$city=null,$state=null,$country=null,$zipcode=null,$mobile=null){
 		try {
 			parent::__construct($login,$usertype,$channel,$firstname,$lastname);
 		} catch (Exception $e){
 			//user already registered; do we want to update the info to db? if yes info may not be reliable. we may want to send a forgot password mail in this case.
 			//sendPasswordResetEmail();
 			throw $e;
 		}
 		
 		parent::setBillingAddress($address,$city,$state,$country,$zipcode);
 		parent::setShippingAddress($address,$city,$state,$country,$zipcode);
 		parent::setMobile($mobile);
 		
	}
	
	public function sendPasswordResetEmail($template=null) {
		$this->firePasswordResetEmailAsync($template);
	}
	
	private function firePasswordResetEmailAsync($template){
		$post_string = "type=$type&cacheid=0&clearcache=Clear";
		global $http_location;
		$url = $http_location . "/reset_password.php";
		$post_string = "action=FORGOTPASSWORD&username=".$this->login."&template=$template";
		$this->postAsync($url, $post_string);
	}
}


?>