<?php
require_once("$xcart_dir/include/func/func.db.php"); 
require_once("$xcart_dir/include/func/func.db.php");
require_once("$xcart_dir/include/func/func.mail.php");
require_once("$xcart_dir/init.php");
include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
include_once "$xcart_dir/modules/coupon/mrp/RuleExecutor.php";
require_once($xcart_dir.'/include/sessionstore/session_store_wrapper.php');

x_load('cart','category','crypt','mail','user');

/**
     *    Base Registration class implementing the helper functions to be used during registration.
     *    All other registration calsses extends this base class and overide its functions as and when needed.
     *    @example		<code>
     *    					$registration = new Registration("mitesh.gupta@myntra.com",'C','RG');
     *    					$registration->executeActions();
     *    				</code>
     *        
	 *    @package		Registration
	 *    @author		Mitesh Gupta (mitesh.gupta@myntra.com)
*/
class Registration {
	
	protected $login, $passwd, $usertype, $channel, $title,$firstname, $lastname, $regCopounKey,$gender,$aboutme,$birthdate,$mobile;
	protected $b_address, $b_city, $b_state, $b_country, $b_zipcode, $s_address, $s_city, $s_state, $s_country, $s_zipcode;
	protected $userInfo;
	protected $profile_values = array();
	protected $personna_values = array();
	protected $referer;
	
	/**
	 *		Constructor to get a object of this class.
	 *		@throws Exception Message = "Login Field is empty" Code=10, if the login field is null or empty.
	 *		@throws Exception Message = "Login Already Exists" Code=1, if the login entry already exists in the customers database.
	 * 		@param string $login (Needed) Login id of the user been registered
	 * 		@param string $usertype (Optional) Usertype for the login. Default value 'C'
	 * 		@param string $channel (Optional) Channel for which registration is done eg. ON for online, BS for brandshop, FB for facebook. Default value RG
	 * 		@param string $firstname (Optional) First name for the user been registered. Default to null.
	 * 		@param string $lastname (Optional) Last name of the user been registered. Default to null.
	 * 		@param string $passwd (Optional) Password for the user been registered. Default is null, if null a random password of length 10 is generated.
	 * 		@access public
	 */
	
	public function __construct($login,$usertype='C',$channel = 'RG', $firstname=null,$lastname=null,$passwd=null) {
		if(empty($login)) throw new Exception("Login Field is empty",10);
		$this->login = trim($login);				
		
		if($passwd!=null && !empty($passwd)) {
			$this->passwd = $passwd;
		} else {
			// code to generate a new random password here and send mail to user
			$this->passwd = $this->genRandomString(10);
			//$this->sendResetPasswordMail = true;
		}
		
		if($channel){
			$this->channel = $channel;
		}
		if($firstname) {
			$this->firstname = trim($firstname);
		}
		
		if($lastname) {
			$this->lastname = trim($lastname);
		}
		
		$this->usertype=$usertype;	
		if(Registration::checkUser($login)) {
			// user exists
			throw new Exception("Login Already Exists",1);
		}	
	}
		
	/**
	 * Function to check if the user already exists in the Customers Database
	 * @param string $login Login id for the user.
	 * @return bool true if the login exists in the customer database, false otherwise.
	 * @access public
	 * @static
	 */
	public static function checkUser($login) {
		if(func_query_first_cell("SELECT COUNT(*) FROM xcart_customers WHERE login='$login'") > 0) {
			return true;
		} else return false;
	}
	
	//helper functions
	
	/**
	 * Helper function to set password for the user, should be called before {@link saveToDB()} is called.
	 * @param string $passwd Password to be set for user been registered.
	 * @access protected
	 */
	
	protected function setPassword($passwd) {
		$this->passwd = $passwd;
	}
	
	/**
	 * Helper function to set Title for the user, should be called before {@link saveToDB()} is called.
	 * @param string $title Title to be set for user been registered. e.g Mr. Mrs. Ms. etc.
	 * @access protected
	 */
	
	protected function setTitle($title){
		$this->title = $title;
	}
	
	/**
	 * Helper function to set user type for the user, should be called before {@link saveToDB()} is called.
	 * @param string $usertype User type to be set for user been registered.
	 * @access protected
	 */
	
	protected function setUserType($usertype) {
		$this->usertype = $usertype;
	}
	
	/**
	 * Helper function to set first name for the user, should be called before {@link saveToDB()} is called.
	 * @param string $firstname First name to be set for user been registered.
	 * @access protected
	 */
	
	protected function setFirstName($firstname) {
		$this->firstname = trim($firstname);
	}
	
	/**
	 * Helper function to set last name for the user, should be called before {@link saveToDB()} is called.
	 * @param string $lastname Last name to be set for user been registered.
	 * @access protected
	 */
	
	protected function setLastName($lastname) {
		$this->lastname = trim($lastname);
	}
	
	/**
	 * Helper function to set gender for the user, should be called before {@link saveToDB()} is called.
	 * @param string $gender Gender to be set for user been registered. Sample input 'M' for male, 'F' for female, 'U' for undisclosed
	 * @access protected
	 */
	
	protected function setGender($gender){
		$this->gender = $gender;
	}
	
	public function setRelationshipStatus($relationship_status)
	{
	    $this->relationship_status = $relationship_status;
	}
	
	public function setReferer($referer)
	{
	    $this->referer = $referer;
	}
	
	/**
	 * Helper function to set about me for the user, should be called before {@link saveToDB()} is called.
	 * @param string $aboutme About me to be set for user been registered.
	 * @access protected
	 */
	
	protected function setAboutMe($aboutme){
		$this->aboutme = trim($aboutme);
	}
	
	/**
	 * Helper function to set date of birth for the user, should be called before {@link saveToDB()} is called.
	 * @param date $birthday date of birth to be set for user been registered.
	 * @access protected
	 */
	
	protected function setBirthdate($birthday){
		$this->birthdate = $birthday;
	}
	
	/**
	 * Helper function to set billing address for the user, should be called before {@link saveToDB()} is called.
	 * @param string $address billing address to be set for user been registered.
	 * @param string $city billing city to be set for user been registered.
	 * @param string $state billing state to be set for the user been registered.
	 * @param string $country billing country to be set for the user been registered.
	 * @param string $zipcode billing zipcode to be set for the user been registered.
	 * @access protected
	 */
	
	protected function setBillingAddress($address=null,$city=null,$state=null,$country=null,$zipcode=null){
		$this->b_address = trim($address);
		$this->b_city=trim($city);
		$this->b_state=trim($state);
		$this->b_country = trim($country);
		$this->b_zipcode = trim($zipcode);
	}
	
	/**
	 * Helper function to set shipping address for the user, should be called before {@link saveToDB()} is called.
	 * @param string $address shipping address to be set for user been registered.
	 * @param string $city shipping city to be set for user been registered.
	 * @param string $state shipping state to be set for the user been registered.
	 * @param string $country shipping country to be set for the user been registered.
	 * @param string $zipcode shipping zipcode to be set for the user been registered.
	 * @access protected
	 */
	
	
	protected function setShippingAddress($address=null,$city=null,$state=null,$country=null,$zipcode=null){
		$this->s_address = trim($address);
		$this->s_city=trim($city);
		$this->s_state=trim($state);
		$this->s_country = trim($country);
		$this->s_zipcode = trim($zipcode);
	}
	
	/**
	 * Helper function to set mobile number for the user, should be called before {@link saveToDB()} is called.
	 * @param string $mobile Mobile number to be set for user been registered.
	 * @access protected
	 */
	
	protected function setMobile($mobile){
		$this->mobile = trim($mobile);
	}
	
	/**
	 * Function to be called if a copoun needs to be generated at the time of registration. 
	 * Since each registration channel may have different copouns requirement, its best to overide this method in the subclass.
	 * In this class it generates a registration coupon key to be stored in the database. 
	 * Should be called before {@link saveToDB()} is called.
	 * @access public
	 */	
	
	public function generateCoupon() {
		$this->regCopounKey = md5(rand().time());
	}
	
	/**
	 * Function to send a registration email to the user email address. 
	 * Mail template is selected from mk_registation_emails table and can be configured from the admin interface.
	 * @param array $args An array containing parameters to be used by the email template.
	 * @access public
	 * @todo  verify, and make sure that common values like first name are always treated as they are written in the code.
	 */
	
	public function sendEmail($args=array()) {
		$currentTime = time(); 
		$mailQuery = "select registration_email_template from 	mk_registration_emails where channel = '".$this->channel ."' and start_date < $currentTime and end_date > $currentTime order by id desc";
		$mailTemplate = func_query_first_cell($mailQuery); 
		$args['FIRST_NAME'] = $args['USER'] = $this->firstname;
		$args['LAST_NAME'] = $this->lastname;
		$args['USER_NAME'] = $args['LOGIN'] = $this->login;
		sendMessage($mailTemplate, $args,$this->login);
	}
	
	/**
	 *		Function to generate a random string may be required to generate a new password
	 * 		@params int $size If no size is provided default value of 10 is used
	 * 		@return string		Random string Generated
	 * 		@access protected
	 */
	protected function genRandomString($size=10) {
		$length = $size;
		$characters = "!$-=+@#|0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWZYZ";
		for ($p = 0; $p < $length; $p++) {
			$string .= $characters[mt_rand(0, strlen($characters))];
		}
		return $string;
	}
	
	/**
	 * Function to save the user data to xcart_customers database. Generally this method should be called in the end. 
	 * A subclass having to save information to other database table depending on the requirement should over write this method.
	 * @access public
	 */	
	
	public function saveToDB() {
		$profile_values['login'] = mysql_real_escape_string($this->login);
		$profile_values['usertype'] = mysql_real_escape_string($this->usertype);
		$crypted = "";
		$profile_values['password'] = $crypted;
		$profile_values['password_md5']= addslashes(user\UserAuthenticationManager::encryptToMD5(($this->passwd)));
		$profile_values['firstname'] = mysql_real_escape_string($this->firstname);
		$profile_values['lastname'] = mysql_real_escape_string($this->lastname);
		$profile_values['b_firstname'] = mysql_real_escape_string($this->firstname);
		$profile_values['b_lastname'] = mysql_real_escape_string($this->lastname);
		$profile_values['s_firstname'] = mysql_real_escape_string($this->firstname);
		$profile_values['s_lastname'] =mysql_real_escape_string($this->lastname);
		$profile_values['termcondition'] = '1';
		############### fields added later ######################### 
		$profile_values['gender'] = mysql_real_escape_string($this->gender);
		$profile_values['referer'] = mysql_real_escape_string($this->referer);
		$profile_values['about_me'] = mysql_escape_string($this->aboutme);    
		$profile_values['DOB'] = date_format($this->birthdate,'Y-m-d')?date_format($this->birthdate,'Y-m-d'):$this->birthdate;		
		$profile_values['reg_coupon_gen_key'] = $this->regCopounKey;
		
		
		$profile_values['b_address'] = mysql_real_escape_string(str_ireplace("\r\n"," ", $this->b_address));
		$profile_values['b_city'] = mysql_real_escape_string($this->b_city);
		$profile_values['b_state'] = mysql_real_escape_string($this->b_state);
		$profile_values['b_county'] = mysql_real_escape_string($this->b_country);
		$profile_values['b_country'] = mysql_real_escape_string($this->b_country);
		$profile_values['b_zipcode'] = mysql_real_escape_string($this->b_zipcode);
		
		$profile_values['s_address'] = mysql_real_escape_string(str_ireplace("\r\n"," ", $this->b_address));
		$profile_values['s_city'] = mysql_real_escape_string($this->s_city);
		$profile_values['s_state'] = mysql_real_escape_string($this->s_state);
		$profile_values['s_county'] = mysql_real_escape_string($this->s_country);
		$profile_values['s_country'] = mysql_real_escape_string($this->s_country);        
		$profile_values['s_zipcode'] = mysql_real_escape_string($this->s_zipcode);
		
		$profile_values['mobile'] = mysql_real_escape_string($this->mobile);
		$profile_values['title'] = mysql_real_escape_string($this->title);
		func_array2insert('customers', $profile_values);
		
		$personna_values['login'] = mysql_real_escape_string($this->login);
		$personna_values['relationship_status'] = mysql_real_escape_string($this->relationship_status);
		func_array2insertWithTypeCheck('customer_personna', $personna_values);
		// Update the referral log.

		$couponAdapter = CouponAdapter::getInstance();
		$couponAdapter->customerJoined($profile_values['login'], $profile_values['referer']);
		
		// Synchronous call to new login / MRP function.
		global $weblog;
		$logging_str = "Generating First login coupons for Facebook user".$profile_values['login'];
		$weblog->warn($logging_str);
		$ruleExecutor = RuleExecutor::getInstance();
		$ruleExecutor->runSynchronous("customer", $profile_values['login'], "5");    // "5" is the ruleId for "Facebook NewLogin" rule.
	}
	
	/**
	 * Function to be called if the user needs to be automatically logged into the system after completing registration. 
	 * @global $XCARTSESSID xcart session id used to make the session entry into mk_session table.
	 * @global string $userfirstname Firstname of the user logging in.
	 * @global string $login Login id for the logged in user.
	 * @global string $login_type User type of the logged in user.
	 * @global array $identifiers Array containing indetifiers to indentify the logged in user. 
	 * @global string $registered Setted to "Y" for the logged in user.
	 * @global mixed $newuser_info	User info object for the logged in user.
	 * @access public
	 */	
		
	public function autoLogin(){
		global $XCARTSESSID,$userfirstname,$login,$login_type,$identifiers;
		
		x_session_unregister("userfirstname");
            	x_session_unregister("account_type");
            	x_session_unregister("login");
            	x_session_unregister("mobile");
            	x_session_unregister("login_type");
            	x_session_unregister("identifiers");
		
		$_curtime = time();
		$login = $this->login;
		$login_type = 'C';
		
		db_query("UPDATE xcart_customers SET last_login='$_curtime', first_login='$_curtime' WHERE login = '$login'");
		
		$account_created_date = $_curtime;
		$userfirstname = $this->firstname;
		
		$mobile = $this->mobile;
		
		$checkFBuserid = db_query("SELECT fb_uid FROM mk_facebook_user WHERE myntra_login='$login'");
		$numFBrows = db_num_rows($checkFBuserid);
		$FBrow  = db_fetch_array($checkFBuserid);
		$fb_uid = '';
		if (!empty($numFBrows)){
			$fb_uid = $FBrow['fb_uid'];
			x_session_register('fb_uid');
		}

		x_session_register("userfirstname");
		x_session_register("account_type");
		x_session_register("login", $login);
		x_session_register("mobile", $mobile);
		x_session_register("login_type", $login_type);

		updateSessionUpdatedFlagBySessionId($XCARTSESSID, 'Y');

		x_session_register("identifiers",array());
		$identifiers['C'] = array (
			'login' => $login,
			'firstname' => $userfirstname,
			'login_type' => $login_type,
			'first_login_date' => $account_created_date,
		);
	}

	/**
	 * Function to redirect the user to a particular url after competing the registation process. 
	 * @param string $location path to the file where the user should pe redirected. Default redirection to home page.
	 * @access public
	 */

 	public function redirectTo($location=null){
		if($location){
			func_header_location($location, false);
		} else {
			func_header_location("/", false);
		}
	}

	/**
	 * Function to execute registration actions based on the execution order configured from admin interface for a particular channel.
	 * 
	 * @access public
	 */

	public function executeActions() {
		global $xcache;
		$channel = $this->channel;
		$actionItemResults = $xcache->fetchAndStore(function($channel) {
			$actionItemResults = array();
			$actionItemsQuery = "SELECT action FROM mk_registration_actions WHERE channel = '".$channel."' order by execution_order";
			$actionItemResults = func_query($actionItemsQuery,true);
			return $actionItemResults;
		}, array($channel), "registrationModule:executeAction-".$channel,3600*12);
	
		foreach ($actionItemResults as $actionItem) {
			$funcname = $actionItem['action'];
			if(method_exists($this, $funcname)) $this->$funcname();
		}
	}
}

?>