<?php

require_once("$xcart_dir/modules/Registration/fb_registration.php");
require_once("$xcart_dir/init.php");

class FacebookAppRegistration extends FacebookRegistration {
	
	public function __construct($passwd=null,$fblogin=null,$fb_me=null,$fb_friends=null,$fb_likes=null,$usertype='C',$channel = 'FA'){
		parent::__construct($passwd,$fblogin,$fb_me,$fb_friends,$fb_likes,$usertype,$channel);
	}
	
	public function sendEmail($args=array()){
    	global $http_location;
		$rand_id= md5(rand().time());
        $update=array("resetpwd_key"=>$rand_id,"isvalid_resetkey"=>"1");
        $result=func_array2update('xcart_customers', $update, "login='".$this->login."'");
        $rowsAffected = mysql_affected_rows();
	    if(empty($result) || $rowsAffected == 0 ) {echo "DBError";return;}
        $link="<a href='".$http_location."/reset_password.php?key=".$rand_id."'>".$http_location."/reset_password.php?key=".$rand_id." </a>";
		$args["LINK"] = $link;
		$args["CONTEST_NAME"] = "Your Name Makes a Difference";
    	parent::sendEmail($args);	
    }
	
}