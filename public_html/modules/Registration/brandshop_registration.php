<?php
require_once("$xcart_dir/modules/Registration/registration_module.php");

/**
     *    BrandShop Registration class implementing registration process for brand shops
     *        
	 *    @package		Registration
	 *    @author		Mitesh Gupta (mitesh.gupta@myntra.com)
*/

class BrandShopRegistration extends Registration {
	
	protected $callParentSaveToDB = true, $orgId, $orgName, $accountType, $employeeId, $args=array(),$status;
	
	public function __construct($login,$orgId,$employeeId,$usertype='C',$channel = 'BS',$firstname=null,$lastname=null,$accountType='OG',$passwd=null,$orgName=null){
		$this->employeeId = trim($employeeId);
		$this->orgId =trim($orgId);
		$this->accountType = trim($accountType);
		$this->orgName = trim($orgName);
		try{
			//echo "calling BrandShopRegistration counstructor for $login, orgID = $orgId, accountType = $accountType<br />";
			parent::__construct($login,$usertype,$channel,$firstname,$lastname,$passwd);
		} catch(Exception $e){
			//user was alreay present into the customer db we will promote him to brandshop in saveToDB method
			if($e->getCode()==1) { // user registered				
				//echo "User already registered with myntra, checking weather it is registered with brandshop also for $login<br />";
				$this->callParentSaveToDB = false;
				if($this->checkEmpLoginExists($login, $orgId)){
					//echo "User is registerd with both brandshop and myntra, throwing exception for $login, organisationId= $orgId<br />";
					throw new Exception("The employee id and email address provided by you are already registered with us.<br>Please login directly with the password that has been mailed to you.", 5);
				}
			} else throw $e;
		}
	}
	
	protected function setAccountType($accountType){
		$this->accountType = trim($accountType);
	}
	
	protected function setStatus($status){
		$this->status = $status;
	}
	
	#
	#Check if employee id alredy exits
	#
	protected function checkEmpLoginExists($login,$orgId){
		global $sql_tbl;

		if(func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[org_user_map] WHERE login='$login' AND orgid='$orgId'") > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public function generateCoupon(){
		$user = '';
        $styleid = '';
        $discount = 300;
        $max_amount = 0;
        $expirydate = time()+(86400*30);//30 days
        $coupon_type = 'absolute';
        $usage = 1;
        $peruser = 'N';
        $min_amount = 0;
        $productid = 0;
        $freeship = 'Y';
        $coupon_code = func_free_coupon_on_registration($user,$styleid,$discount,$max_amount,$expirydate,$coupon_type,$usage,$peruser,$min_amount,$productid,$freeship);
        $this->args += array("COUPON" => $coupon_code);
	}
	
	public function saveToDB(){
		if($this->callParentSaveToDB){
			parent::saveToDB();
		}
		
		//update the xcart_customers table
		$profile_values = array();
		$profile_values['orgid'] = mysql_real_escape_string($this->orgId);
		$profile_values['activity'] = "Y";
		$profile_values['addressoption'] = 'M';
		$profile_values['account_type'] = mysql_real_escape_string($this->accountType);
		$profile_values['status'] = mysql_real_escape_string($this->status);
		//print_r($profile_values);
		
		func_array2update('xcart_customers', $profile_values, "login='$this->login'");
		
		//insert into org_user_map table		
		$orgusermap = array();
		$orgusermap['orgid'] = mysql_real_escape_string($this->orgId);
		$orgusermap['login'] = mysql_real_escape_string($this->login);
		$orgusermap['active'] = "1";
		$orgusermap['usetype'] = mysql_real_escape_string($this->accountType);
		$orgusermap['employee_id'] = mysql_real_escape_string($this->employeeId);
		func_array2insert('org_user_map', $orgusermap);
	}
	
	
	public function sendEmail($args=array()){
    	//login to calculate args
    	$this->args += $args;
		$this->args["PASSWORD"] = $this->passwd;
		$this->args["ORGANIZATION"] = $this->orgName;
		//echo "BS :: sendEmail<br/>";	
    	parent::sendEmail($this->args);	
    }
}