<?php
require_once("$xcart_dir/modules/Registration/registration_module.php");
require_once("$xcart_dir/init.php");

/**
     *    Online Registration class implementing registration process for online channel
     *    @example		<code>
     *    					$registration = new OnlineRegistration("mitesh.gupta@myntra.com",'mitesh','gupta','password','password','captcha');
     *    					$registration->executeActions();
     *    				</code>
     *        
	 *    @package		Registration
	 *    @author		Mitesh Gupta (mitesh.gupta@myntra.com)
*/

class OnlineRegistration extends Registration {

	/**
	 *		Constructor to get a object of this class.
	 *		@throws Exception Message = "One or more required fields are empty" Code=2, if the login, passwd1, passwd2, $captcha_i field is null or empty.
	 *		@throws Exception Message = "Supplied passwords does not match with each other" Code=3, if passwd1 != passwd2
	 *		@throws Exception Message = "Login Already Exists" Code=1, if the login entry already exists in the customers database.
	 *		@throws Exception Message = "Incorrect captcha text entered. Please try again" Code=4, if incorrect captcha value is entered by the user. 
	 * 		@param sting $login (Needed) Login id of the user been registered
	 * 		@param string $firstname (Needed) First name for the user been registered.
	 * 		@param string $lastname (Needed) Last name of the user been registered.
	 * 		@param string $passwd1 (Needed) Password for the user been registered. 
	 * 		@param string $passwd2 (Needed) Confirm Password for the user been registered.
	 * 		@param string $captcha_i (Needed) Captcha value as entered by user been registered.
	 * 		@param string $usertype (Optional) Usertype for the login. Default value 'C'
	 * 		@param string $channel (Optional) Channel for which registration is done eg. ON for online, BS for brandshop, FB for facebook. Default value ON
	 * 		@global array $XCART_SESSION_VARS Needed to get the captcha value to be matched against with.
	 * 		@access public
	 */
	
	public function __construct($login,$firstname,$lastname,$passwd1,$passwd2,$captcha_i,$usertype='C',$channel = 'ON'){
		global $XCART_SESSION_VARS;
		
		echo "calling OnlineRegistration counstructor for $login<br>";
		
		$checkEmpty = ( empty($login) || empty($passwd1) || empty($passwd2)|| empty($captcha_i));
		
		if ($checkEmpty){
			throw new Exception("One or more required fields are empty",2);
		}
		
		if($passwd1 != $passwd2) {
			throw new Exception("Supplied passwords does not match with each other",3);
		}
		
		$captcha_s=$XCART_SESSION_VARS["captcha"]["register"];
		
		echo "Captha Test $captcha_s, $captcha_i <br>";
		
		if ($captcha_i != $captcha_s) {
			echo "Throwing Exception: Incorrect captcha <br>";
			throw new Exception("Incorrect captcha text entered. Please try again",4);
		}
		
		echo "Captha Test Passed <br>";
		
		parent::__construct($login,$usertype,$channel,$firstname,$lastname,$passwd1);
		
	}
}
