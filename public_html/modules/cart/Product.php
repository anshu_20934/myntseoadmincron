<?php

/**
 * An abstraction of a product in the shopping-cart.
 * 
 * @author Rohan.Railkar
 */
class Product
{
    /**
     * Catalog related metadata.
     */
    protected $productId;
    protected $productTypeId;
    protected $productStyleId;
    protected $productStyleType;
    
    protected $vatRate = 0.0;
    protected $quantity = 0;
    
    /**
     * Excludes VAT
     */
    protected $unitPrice = 0.0;
    
    /**
     * $quantity * $unitPrice
     * Excludes VAT
     */
    protected $totalPrice = 0.0;
    
    /**
     * Unit price inclusive of VAT
     */
    protected $productPrice = 0.0;
    
    /**
     * Provision for markup etc.
     */
    protected $unitAdditionalCharges = 0.0;
    
    /**
     * $quantity * $productPrice
     * Total price inclusive of VAT
     */
    protected $totalProductPrice = 0.0;
    
    /**
     * Total additional charges.
     */
    protected $totalAdditionalCharges = 0.0;
    
    /**
     * Total discount amount
     */
    protected $totalDiscount = 0.0;
    
    /**
     * Coupon code, if any.
     */
    protected $couponCode = '';
    
    
    /**
     * Total Cash discount amount
     * @deprecated use totalMyntCashUsage
     */
    protected $totalCashDiscount = 0.0;
    
    
    /* TotalmyntCashUsage = storeCreditmyntCashUsage + earnedCreditmyntCashUsage */
    
    /*
     * MyntCash storeCredit usage
     */    
    protected $storeCreditMyntCashUsage= 0.0;
    
    /*
     * MyntCash earnedCredit usage
    */
    protected $earnedCreditMyntCashUsage= 0.0;
    
    /*
     * TotalMyntCash usage
     */
    protected $totalMyntCashUsage = 0.0;

    /**
     * Coupon code, if any.
     */
    protected $cashCouponCode = '';
    
    /**
     * Applied discount rule id
     */
    public $discountRuleId = null;
    
    /**
     *
     * Id defining the set of items to be grouped in cart
     */
    public $comboId = 0;
    
    /**
     * Discount Applied at Cart Level is split on the ratio here
     */
    public $cartDiscount = 0;
    
    /**
	 * Discount Related Data
	 */
	public $styleType = 1;
	
    /**
     * Discount Rule Revision Id - need to store with order for BI purpose
     */
	public $discountRuleRevId = 0;
	
    /**
     * Quantity on which discount is applied
     */
	public $discountQuantity = 0;
	
    /**
     * Associated Styles with this product for discount
     */
	public $associatedStyleIds = null;
	
    /**
     * Is discount rule applied on this item
     */
	public $isDiscountRuleApplied = false;
	
    /**
     * Is this product discounted
     */
	public $isDiscountedProduct = false;

	/**
     * Is this item returnable
     */
	public $isReturnable = true;

    /**
     * Applied total discount amount
     */
	public $totalDiscountAmount = 0;
    
    /**
     * Applied unit discount amount
     */
    public $discountAmount = 0.0;
    
    /**
     * Discount data applied
     */
    public $discountData = null;
    
    /**
     * Discount Algo ID
     */
    public $discountAlgoID = null;

    /**
     * Discount Display text on PDP and cart page
     */
    public $discountDisplayText = null;
    
    /**
     * Discount Display text for solr search results
     */
    public $discountLabelText = null;
    
    /*************************************************************************
     * Total amount payable by customer =                                    *
     * ($totalProductPrice + $totalAdditionalCharges - $totalDiscount - $totalCashDiscount)       *
     *************************************************************************/
    
    /**
     * Constructor. Creates the product from an element in the xcart session
     * array, viz products-in-cart.
     */
    public function __construct($product)
    {
        $this->productId = $product['productId'];
        
        //XXX: ... And, the typos transcend!
        $this->productTypeId = $product['producTypeId'];
        
        $this->productStyleId = $product['productStyleId'];
        $this->productStyleType = $product['productStyleType'];
        $this->vatRate = $product['vatRate'];
        $this->quantity = $product['quantity'];
        $this->unitPrice = $product['unitPrice'];
        $this->totalPrice = $this->quantity * $this->unitPrice;
        
        // $unitPrice * (1 + $vatRate / 100)
        $this->productPrice = $product['productPrice'];
        
        $this->totalProductPrice = $this->productPrice * $this->quantity;
        $this->totalDiscount = $product['discount'];
        if(empty($product['discount'])){
        	$this->totalDiscount = 0.0;
        }
        $this->totalCashDiscount = $product['cashdiscount'];
    }
    
    /**
     * Getter for product ID
     */
    public function getProductId()
    {
        return $this->productId;
    }
    
    /**
     * Getter for product type ID
     */
    public function getProductTypeId()
    {
        return $this->productTypeId;
    }
    
    /**
     * Getter for product style ID
     */
    public function getProductStyleId()
    {
        return $this->productStyleId;
    }
    
    /**
     * Getter for the quantity.
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
    
    /**
     * Getter for total price, exclusive of VAT.
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }
    
    /**
     * Getter for the unit price, inclusive of VAT.
     */
    public function getProductPrice()
    {
        return $this->productPrice;
    }
    
    /**
     * Getter for the unit additional charges.
     */
    public function getUnitAdditionalCharges()
    {
        return $this->unitAdditionalCharges;
    }

    /**
     * Setter for the unit additional charges.
     */
    public function setUnitAdditionalCharges($charges) {
        $this->unitAdditionalCharges = $charges;
    }
    
    /**
     * Getter for $totalAdditionalCharges.
     */
    public function getTotalAdditionalCharges()
    {
        return $this->totalAdditionalCharges;
    }
    
    /**
     * Setter for $totalAdditionalCharges.
     */
    public function setTotalAdditionalCharges($charges) {
        $this->totalAdditionalCharges = $charges;
    }
    
    /**
     * Getter for the total price, inclusive of VAT.
     */
    public function getTotalProductPrice()
    {
        return $this->totalProductPrice;
    }
    
    /**
     * Getter for the total VAT
     */
    public function getVAT()
    {
        return $this->totalPrice * $this->vatRate / 100;
    }

    /**
     * Getter for the VAT rate
     */
    public function getVatRate()
    {
        return $this->vatRate;
    }

    /**
     * Getter for discount amount applicable on the product
     */
    public function getDiscountAmount(){
    	return $this->discountAmount;
    }
    
    /**
     * Setter for discount amount
     */
    public function setDiscountAmount($amount)
    {
    	$this->discountAmount = $amount;
    }
    
    /**
     * Getter for coupon discount.
     */
    public function getDiscount()
    {
        return $this->totalDiscount;
    }
    
    /**
     * Get the coupon code.
     */
    public function getCouponCode()
    {
        return $this->couponCode;
    }
    /**
     * Getter for cash discount.
     * @deprecated use getTotalMyntCashUsage
     */
    public function getCashDiscount()
    {
        return $this->totalCashDiscount;
    }
    
    /*
     * Getter for totalMyntCashUsage
     */
    public function getTotalMyntCashUsage()
    {
    	return $this->totalMyntCashUsage;
    }        	
	/*
	 * Set MyntCash usage
	*/
	public function setMyntCashUsage($totalMyntCashUsage)
	{		
		$this->totalMyntCashUsage = $totalMyntCashUsage;
	}
	
    public function getCouponDiscount()
    {
    	return $this->totalDiscount;
    }
    /**
     * Get the cash coupon code.
     */
    public function getCashCouponCode()
    {
        return $this->cashCouponCode;
    }
    
       
    /**
     * Setter for discount.
     */
    public function setCouponDiscount($discount)
    {
        $this->totalDiscount = $discount;
    }
    
    /**
     * Setter for the coupon code.
     */
    public function setCouponCode($couponCode)
    {
        $this->couponCode = $couponCode;
    }
    /**
     * Setter for cash discount.
     * @deprecated use setMyntCashUsage
     */
    public function setCashCouponDiscount($discount)
    {
        $this->totalCashDiscount = $discount;
    }
	
    
     
    /**
     * Setter for the cash coupon code.
     * @deprecated 
     */
    public function setCashCouponCode($couponCode)
    {
        $this->cashCouponCode = $couponCode;
    }
       
    /**
     * Clear the discount on the product.
     * @deprecated use clearCouponDiscount instead
     */
    public function clearDiscount()
    {
        $this->totalDiscount = 0.0;
    }
    /**
     * Clear the cash discount on the product.
     */
    public function clearCashDiscount()
    {
        $this->totalCashDiscount = 0.0;
    }
    
    /**
     * Merge with another instance of same product.
     */
    public function mergeProduct($product)
    {
        $this->quantity += $product->getQuantity();
        $this->totalPrice += $product->getTotalPrice();
        $this->totalProductPrice += $product->getTotalProductPrice();
        $this->totalAdditionalCharges += $product->getTotalAdditionalCharges();
        $this->totalDiscount += $product->getDiscount();
        
        $couponCode = $product->getCouponCode();
        if (!empty($CouponCode)) {
            $this->couponCode = $CouponCode;
        } 
    }
    public function toArray(){
    	$totalDisc = $this->totalDiscount;
    	if(empty($totalDisc)){ // Hack to make it decimal
    		$this->totalDiscount = 0.0;
    	}
    	$reflect = new ReflectionClass($this);
    	$props   = $reflect->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED | ReflectionProperty::IS_PRIVATE);
    	$arr = array();
    	foreach ($props as $prop) {
    		if(!$prop->isStatic()  && $prop->class == "Product"){
    			$p = $prop->getName();
    			$arr[$prop->getName()]=$this->$p;
    		}
    
    	}
    	return $arr;
    }
    
    public function getDiscountQuantity(){
    	return $this->discountQuantity;
    }
    
    public function getDiscountRuleRevId(){
    	return $this->discountRuleRevId;
    }
    
    public function isDiscountedProduct(){
    	return $this->isDiscountedProduct;
    }
 
    public function getDiscountRuleId(){
    	return $this->discountRuleId;
    }
    public function isDiscountRuleApplied(){
    	return $this->isDiscountRuleApplied;
    }
    public function isReturnable(){
    	return $this->isReturnable;
    }
    
    public function getComboId(){
    	return $this->comboId;
    }
    
    public function getDiscountDisplayText(){
    	return $this->discountDisplayText;
    }
    
    public function getDiscountData(){
    	return $this->discountData;
    }
    
}
