<?php

include_once "$xcart_dir/modules/cart/Product.php";

/**
 * A shopping cart consisting of Products.
 * 
 * @author Rohan.Railkar
 */
class Cart
{
    /**
     * Array of Products.
     * productId (string) => Product
     */
    protected $productsArray = array();
    
    /**
     * Array of item counts per Product.
     * productId (string) => No of items (int).
     */
    protected $productQtyArray = Array();
    
    /**
     * Total items in cart
     */
    protected $noOfItems = 0;
    
    /**
     * Excluding VAT
     */
    protected $price = 0.0;
    
    /**
     * Inclusive of VAT
     */
    protected $mrp = 0.0;
    
    /**
     * Value Added Tax
     */
    protected $vat = 0.0;
    
    /**
     * Provision for markup etc.
     */
    protected $additionalCharges = 0.0;
    
    /**
     * Total discount
     */
    protected $discount = 0.0;
    /**
     * Total cash discount
     * @deprecated use cashbackUsage 
     */
    protected $cashdiscount = 0.0;
    
    /**
     * 
     * Cart level EOSS discounts
     * 
     */
    public $discountOnCart = 0;
    
    public $setDisplayData = null;
    
    public $displayData = null;
    /*
     * $mrp = $price + $additionalCharges + $vat [[Always]]
     * Total price payable by customer = $mrp - $discount
     */
    
    /**
     * Constructor. Creates the cart and contained products from the xcart
     * session array, viz. products-in-cart.
     */
    public function __construct($productsArray)
    {
        foreach ($productsArray as $key => $productArray) {
            $product = new Product($productArray);
            $this->addProduct($product);
        }
    }
    
    /**
     * Getter for MRP.
     */
    public function getMrp()
    {
        return $this->mrp;
    }
    
    /**
     * Setter for MRP.
     */
    public function setMrp($mrp)
    {
        $this->mrp = $mrp;
    }
    
    /**
     * Getter for VAT.
     */
    public function getVat()
    {
        return $this->vat;
    }
    
    /**
     * Getter for total additional charges.
     */
    public function getAdditionalCharges()
    {
        return $this->additionalCharges;
    }
    
    /**
     * Setter for total additional charges.
     */
    public function setAdditionalCharges($charges)
    {
    	$this->additionalCharges = $charges;
    }
    
    /**
     * Getter for discount.
     * @deprecated use getTotalCouponDiscount() instead
     */
    public function getDiscount()
    {
        return $this->discount;
    }
    /**
     * Getter for cash discount.
     * @deprecated
     * use class.Mcart->getTotalMyntCashUsage()
     */
    public function getCashDiscount()
    {   
        return $this->cashdiscount;
    }    
    /**
     * Get the product specific discount on the cart.
     * @deprecated - Mar 21, 2012
     * Please use getTotalProductDiscount() in class.MCart.php
     */
    
    public function getDiscountAmount(){
    	$discountAmount = 0.0;
    	foreach ($this->getProducts() as $product){
    		$discountAmount += $product->totalDiscountAmount;
    	}
    	return $discountAmount;
    }
    
    /**
     * Getter for products array.
     * @return array  \Product
     */
    public function getProducts()
    {
        return $this->productsArray;
    }
    
    /**
     * Get the product in cart with given ID
     * 
     * @return MCartNPItem
     * 
     */
    public function getProduct($id)
    {
        return $this->productsArray[$id];
    }
    
    /**
     * Getter for products count.
     */
    public function getProductQtyArray()
    {
        return $this->productQtyArray;
    }
    
    /**
     * Get total item count.
     */
    public function getItemQuantity()
    {
        $count = 0;
        foreach ($this->productQtyArray as $id=>$qty) {
            $count += $qty;
        }
        
        return $count;
    }
    
    /**
     * Setter for discount.
     */
    public function setCouponDiscount($discount)
    {
        $this->discount = $discount;
    }
    /**
     * Setter for cash discount.
     * @deprecated use setMyntCashUsage in excented class Mcart
     */
    public function setCashCouponDiscount($discount)
    {  
        $this->cashdiscount = $discount;
    }
    
    /**
     * Add product to the cart.
     */
    public function addProduct($product)
    {
        $productId = $product->getProductId();
        $quantity = $product->getQuantity();
        
        $this->noOfItems += $quantity;
        $this->price += $product->getTotalPrice();
        $this->mrp += $product->getTotalProductPrice();
        $this->additionalCharges += $product->getTotalAdditionalCharges();
        $this->vat += $product->getVAT();
        $this->discount += $product->getDiscount();
        $this->cashdiscount += $product->getCashDiscount();        
        if (!empty($this->productsArray[$productId])) {
            $this->productQtyArray[$productId] += $quantity;
            $oldProduct = $this->productsArray[$productId];
            $oldProduct->mergeProduct($product);
            $this->productsArray[$productId] = $oldProduct;
        }
        else {
            $this->productsArray[$productId] = $product;
            $this->productQtyArray[$productId] = $quantity;
        }
    }
    
    /**
     * Empty the cart.
     */
    public function clear()
    {
        $this->productsArray = array();
        $this->productQtyArray = array();
        $this->noOfItems = 0;
        $this->price = 0.0;
        $this->mrp = 0.0;
        $this->vat = 0.0;
        $this->discount = 0.0;
        $this->cashdiscount = 0.0;
    }
    
    /**
     * Clear all the discounts on the cart.
     * @deprecated use clearCouponDiscount instead
     */
    public function clearDiscount()
    {
        $this->discount = 0.0;
        
        // Iterate over products to remove discounts.
        foreach ($this->productsArray as $id=>$product) {
            $product->clearDiscount();
        }
    }
    /**
     * Clear all the cash discounts on the cart.
     */
    public function clearCashDiscount()
    {
        $this->cashdiscount = 0.0;

        // Iterate over products to remove discounts.
        foreach ($this->productsArray as $id=>$product) {
            $product->clearCashDiscount();
        }
    }

    /**
     * Returns the value on which cashback needs to be applied.
     */
    public function getCashBackAmountForDisplay()
    {
		$cashBackDisplay = 0.0;
        // Iterate over products to remove discounts.
        foreach ($this->productsArray as $id=>$product) {
       		if($product->getDiscountAmount() == 0)  {
            	$cashBackDisplay += $product->getTotalProductPrice();
            }
        }
        return number_format($cashBackDisplay, 2, ".", '');
    	
    }
  public function getTotalCashBackAmount()
    {
		$totalAmount = 0.0;
        // Iterate over products to remove discounts.
        foreach ($this->productsArray as $id=>$product) {
       		if($product->getDiscountAmount() == 0)  {
            	$totalAmount += $product->getTotalProductPrice()- $product->getCouponDiscount() - $product->getCashDiscount();
            }
        }
        return number_format($totalAmount, 2, ".", '');
    	
    }
    public function getCashbackToGivenOnCart()
    {
            $amount=0;
            foreach ($this->productsArray as $id=>$product) {
                    if(empty($product->discountRuleId))
                    {
                            $amount += $product->getTotalProductPrice() - $product->getDiscount();
                    }
            }
            $cashback_tobe_given=round($amount * 0.10);
            return $cashback_tobe_given;
    }
    
    
    /**
     * Put the product data back into the session array.
     */
    public function deconstructCart(&$productsInCart)
    {
        // Iterate over the session to update coupon discounts.
	foreach ($productsInCart as $key=>$product) {
            $id = $product['productId'];
            $cartProduct = $this->getProduct($id);
            $product['totalProductPrice']=$cartProduct->getTotalProductPrice();
            
            $discount = $cartProduct->getDiscountAmount();
            if ($product['totalProductPrice'] < $discount) {
                $discount = $product['totalProductPrice'];
            }
            $product['discountAmount'] = $discount;
	        $cartProduct->discountAmount = $discount;
            $product['discountData'] = $cartProduct->discountData;
            $product['discountAlgoID'] = $cartProduct->discountAlgoID;
            $product['discountRuleId'] = $cartProduct->discountRuleId;
            
            $couponDiscount = $cartProduct->getDiscount();
	        if ($product['totalProductPrice'] < $couponDiscount) {
                $couponDiscount = $product['totalProductPrice'];
            }
            $product['couponDiscount'] = $couponDiscount;
            $cartProduct->setCouponDiscount($couponDiscount);
            $totalDiscount = $cartProduct->getDiscountAmount() + $cartProduct->getDiscount();
	        if ($product['totalProductPrice'] < $totalDiscount) {
                $totalDiscount = $product['totalProductPrice'];
            }
            $product['discount'] = $totalDiscount;
            $priceafterdiscount=$product['totalProductPrice'] - $cartProduct->getDiscount();
            $cashdiscount = $cartProduct->getCashDiscount();
            if ($priceafterdiscount < $cashdiscount) {
                $cashdiscount = $priceafterdiscount;
            }
            $cartProduct->setCashCouponDiscount($cashdiscount);
            $product['cashdiscount'] = $cashdiscount;
            $product['discountDisplayText'] = $cartProduct->discountDisplayText;
            $this->productsArray[$id] = $cartProduct;
            
            //
            // Set back the product in the array.
            // Stupid PHP requires this inspite the parameter having passed
            // by reference. :(
            //
            $productsInCart[$key] = $product;
        }
    }
    
    public function toArray(){
    	$reflect = new ReflectionClass($this);
    	$props   = $reflect->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED | ReflectionProperty::IS_PRIVATE);
    	$arr = array();
    	foreach ($props as $prop) {
    		if(!$prop->isStatic()  && $prop->class == "Cart"){
    			$p = $prop->getName();
    			if($p==="productsArray"){
    				$temp;
    				foreach($this->$p as $key => $value){
    					$temp[$key]=$value->toArray();
    				}
    				$arr[$prop->getName()]=$temp;
    			}else{
    				$arr[$prop->getName()]=$this->$p;
    			}
    		}

    	}
    	return $arr;
    }
    
    public function getSetDisplayData(){
    	return $this->setDisplayData;
    }
    
    public function getDisplayData(){
    	return $this->displayData;
    }
}
