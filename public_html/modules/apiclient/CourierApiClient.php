<?php

include_once("$xcart_dir/modules/apiclient/ApiClient.php");

/**
 * The class contains all Courier Service API calls.
 */
class CourierApiClient {
    
    private static $courierCache;
    
    private static function getCache() {
        if(self::$courierCache == null) {
            self::$courierCache = new \Cache(CourierApiCacheKeys::$prefix, CourierApiCacheKeys::keySet());
        }
        return self::$courierCache;
    }

    public static function getCourierServiceApiClient($action, $key, $username='') {
        return new ApiClient('courierServiceUrl', $action, $key, $username);
    }

    public static function getCourierPref($whId) {
        global $weblog;
        global $errorlog;
        $weblog->debug("Getting courier preference for WH-id: " . $whId);
        $apiClient = CourierApiClient::getCourierServiceApiClient('getCourierPreference/' . $whId, 'courier');
        $apiClient->getDataForAction();
        $courierList = array();
        $status = 'success';

        if ($apiClient->isSuccess()) {
            $data = $apiClient->getData();
            if ($apiClient->getTotalCount() > 1) {
                $courierList = array_values($data[0]);
            } else {
                $courierList = $data;
            }
        } else {
            $errorlog->error("Get courier preference for WH-id $whId failed. Error Msg: " . $apiClient->getErrorMsg());
            $status = 'failure';
        }

        return $return = array('status' => $status, 'data' => $courierList);        
    }

    public static function getTrackingNumberForPincode($whId, $pincode, $isCOD, $isEx='false') {
        global $errorlog;
        $apiClient = CourierApiClient::getCourierServiceApiClient('getTrackingNumber/' . $whId."/".$pincode ."/".$isCOD ."/".$isEx, false);
        $apiClient->getDataForAction();
        if ($apiClient->isSuccess()) {
            $response = $apiClient->getData();
            return $response;
        } else {
            $errorlog->error("Get tracking number for pincode $pincode - iscod - $isCOD in whId - $whId failed. Error Msg: " . $apiClient->getErrorMsg());
        }
        return false;
    }
    
    public static function getTrackingNumberForCourier($courier, $whId, $isCOD, $zipcode) {
    	global $errorlog;
    	$apiClient = CourierApiClient::getCourierServiceApiClient("getTrackingNumber/$courier/$whId/$isCOD/$zipcode", false);
    	$apiClient->putXmlRequest(false, false);
    	if ($apiClient->isSuccess()) {
    		$response = $apiClient->getData();
    		return $response;
    	} else {
    		$errorlog->error("Get tracking number for courier $courier - iscod - $isCOD in whId - $whId failed. Error Msg: " . $apiClient->getErrorMsg());
    	}
    	return false;   
    }
        
    public static function setCourierPref($whId, $whbarcode, $couriers, $addOrUpdate, $user='') {
        global $weblog;
        global $errorlog;
        $weblog->debug("Setting courier preferences. WH id-" . $whId . " barcode: " . $whbarcode .
                " Courier Operator: " . implode(',', $couriers));
        $request = array();
        $request['warehouseId'] = $whId;
        $request['code'] = $whbarcode;
        $i = 1;
        foreach ($couriers as $courier) {
            $request[] = array('courierCodes' => $courier);
        }

        if ($addOrUpdate == 'add') {
            $apiClient = CourierApiClient::getCourierServiceApiClient('addCouriersToWarehouse', 'courier', $user);
        } else {
            $apiClient = CourierApiClient::getCourierServiceApiClient('updateCourierPreference', 'courier', $user);
        }
        $apiClient->postXmlRequest('warehouse', $request);
        $status = 'success';
        $msg = "";
        if ($apiClient->isSuccess()) {
            $data = $apiClient->getData();
            $msg = "Successfully saved the courier preferences.";
        } else {
            $errorlog->error("Get courier preference for WH-id $whId failed. Error Msg: " . $apiClient->getErrorMsg());
            $status = 'failure';
            $msg = "Failed to save the courier preferences. " . $apiClient->getErrorMsg();
        }
        return array("status" => $status, "msg" => $msg);
    }    
    public static function updateCourier($courierCode, $enabled, $returnSupported, $user='') {
        global $weblog;
        global $errorlog;
        $weblog->debug("Getting courier for code: " . $courierCode);
        $apiClient = CourierApiClient::getCourierServiceApiClient('search?q=code.eq:' . $courierCode, 'courier', $user);
        $apiClient->getDataForAction();

        if ($apiClient->isSuccess()) {
            $data = $apiClient->getData();
            $id = $data[0]["id"];
            if(!empty($id)){
            
	            $weblog->debug("Updating courier[id: " . $id . ", code:" . 
	            	$courierCode . " : enabled=" . $enabled ." returnSupported=" . $returnSupported);
	            $request = array();
		        $request['id'] = $id;
		        $request['enabled'] = $enabled;
		        $request['returnSupported'] = $returnSupported;
		    	$apiClient = CourierApiClient::getCourierServiceApiClient($id, 'courier', $user);
		    	$apiClient->putXmlRequest('courier', $request);
		        $status = 'success';
		        $msg = "";
		        if ($apiClient->isSuccess()) {
		            $data = $apiClient->getData();
		            $msg = "Successfully updated courier.";
		            $weblog->debug($msg);
		        } else {
		        	$msg = "Failed to update courier with code=" . $courierCode . ". Error Msg: " . $apiClient->getErrorMsg();
		        	$errorlog->error($msg);
		            $weblog->error($msg);
		            $status = 'failure';
		        }
            } else {
		        	$msg = "No courier found for code=" . $courierCode;
		            $errorlog->error($msg);
		            $status = 'failure';
		    }
        } else {
        	$msg = "Failed to find courier with code=" . $courierCode . ". Error Msg: " . $apiClient->getErrorMsg();
            $errorlog->error($msg);
            $status = 'failure';
        }

        return $return = array('status' => $status, 'msg' =>"[LMS Message]: " . $msg);
    }
    
    public static function getActiveCouriers(){
    	$cache = self::getCache();
    	$cachedcouriers = $cache->get(function($params){
	 		$apiClient = CourierApiClient::getCourierServiceApiClient('search', 'courier', '');
	 		$apiClient->search("enabled.eq:true");
			$return_couriers = array();
			if($apiClient->isSuccess()) {
				$couriers = $apiClient->getData();
				foreach($couriers as $courier){
					$return_couriers[] = array('code' => $courier['code'], 'display_name' => $courier['name']);
				}
			}
			return $return_couriers;
    	}, array(), CourierApiCacheKeys::$activeCouriers, (24*3600));
        return $cachedcouriers; 		   	
    }
    
    public static function getAllCouriers(){
        $cache = self::getCache();
        $cachedcouriers = $cache->get(function($params){
	    	$apiClient = CourierApiClient::getCourierServiceApiClient('', 'courier', '');
	    	$apiClient->search();
	    	$return_couriers = array();
	    	if($apiClient->isSuccess()) {
				$couriers = $apiClient->getData();
				foreach($couriers as $courier){
					$return_couriers[] = array('code' => $courier['code'], 'display_name' => $courier['name']);
				}
			}
			return $return_couriers;
    	}, array(), CourierApiCacheKeys::$allCouriers, (24*3600));
    	return $cachedcouriers;
    }
    
    public static function getAllCourierDetails() {
        $cache = self::getCache();
    	$courierDetails = $cache->get(function($params){
	    	$apiClient = CourierApiClient::getCourierServiceApiClient('getAllCourierDetails/false', 'courier', '');
	    	$apiClient->getDataForAction();
	    	if($apiClient->isSuccess()) {
	    		$couriers = $apiClient->getData();
				return $couriers;    		
	    	}
    		return false;
    	}, array(), CourierApiCacheKeys::$courierDetails, (30*60));
    	
    	return $courierDetails;
    }
    
    public static function getAllCourierCapacityMap() {
    	$courierDetails = CourierApiClient::getAllCourierDetails();
    	$courier2CapacityMap = array();
    	if($courierDetails) {
    		foreach($courierDetails as $courierDetail) {
    			$serviceTypes = $courierDetail['serviceTypes']['serviceType'];
    			foreach($serviceTypes as $serviceType) {
    				if($serviceType['name'] == 'codLimit')
    					$courier2CapacityMap[$courierDetail['code']] = $serviceType['value']; 
    			}
    		}
    	}
    	return $courier2CapacityMap;
    }
    
	public static function getCapacityForCourier($courierCode){
        $courierDetails = CourierApiClient::getAllCourierDetails();
        if($courierDetails) {
            foreach($courierDetails as $courierDetail) {
                if($courierDetail['code'] == $courierCode) {
                    $serviceTypes = $courierDetail['serviceTypes']['serviceType'];
                    foreach($serviceTypes as $serviceType) {
                        if($serviceType['name'] == 'codLimit') {
                            $capacity = array('CODLimit'=>$serviceType['value']);
                            return $capacity;
                        }
                    }
                }
            }
        }
        return array('CODLimit'=> 0);
    }
    
    /*public static function getTrackingNumber($couriercode) {
     	global $weblog;
	    global $errorlog;
	    $weblog->debug("Calling get tracking number for courier: $couriercode");
	    $apiClient = CourierApiClient::getCourierServiceApiClient('getTrackingNumber/' . $couriercode, 'trackingNumber');
	    $apiClient->getDataForAction();
	    $trackingNumber = false;
	    if ($apiClient->isSuccess()) {
	    	$response = $apiClient->getData();
	    	$trackingNumber = $response[0];
	    } else {
	    	$errorlog->error("Get courier preference for WH-id $whId failed. Error Msg: " . $apiClient->getErrorMsg());
	    }
	    return $trackingNumber;
    }*/
}
