<?php
include_once("$xcart_dir/modules/apiclient/ApiClient.php");
include_once("$xcart_dir/modules/apiclient/DeliveryCenterApiClient.php");
include_once("$xcart_dir/modules/apiclient/DeliveryStaffApiClient.php");

class TripApiClient {

	public static function updateLocalDeliveryTripStatus($orderid,$mobile) {
		if(!$orderid || !$mobile) {
			return;
		}
		
		$order = func_query_first("Select * from xcart_orders where orderid = $orderid");
		if($order['status'] != 'SH') {
			$response = array("status" => "FAILURE","msg"=>"Order $orderid is not in shipped state");
		} else {
			$request[] = array('orderId' => $orderid,'mode'=>'Mobile','verficationNo'=>$mobile);	
			$apiClient = new ApiClient('tripServiceUrl', 'updateTripOrderOnTheFly', 'tripOrderResponse', $mobile);
			$apiClient->postXmlRequest('tripOrder', $request);
			if($apiClient->isSuccess()) {
				$response = array("status" => "SUCCESS","msg"=>"Marked order delivered successfully");
			}else {
				$response = array("status" => "FAILURE", "msg"=>"Failed to update order as delivered. Failure Reason: " . $apiClient->getErrorMsg(), "reason"=>$apiClient->getErrorMsg());
			}
		}
		return $response;
	}
	
	public static function getActiveTripForOrder($orderId) {
        global $weblog;
        global $errorlog;
        $apiClient = new ApiClient('tripServiceUrl', 'getActiveTripForOrder/' . $orderId, 'trip');
        $apiClient->getDataForAction();
        $resp = array();
        if ($apiClient->isSuccess()) {
            $response = $apiClient->getData();
            $trip = $response[0];
            if(!empty($trip)){
	            $resp['status']='success';
	            $resp['data']=$trip;
            }else{
				$resp = array('status'=>'failure', 'msg'=>'No active trip found for the order.');
			}
        } else {
        	$msg = "Get active trip for order=$orderId failed. Error Msg: " . $apiClient->getErrorMsg();
            $errorlog->error(msg);
            $resp['status']='failure';
            $resp['msg']=$msg;
        }
        return $resp;
    }
	
	public static function getOrderTripDetail($orderId){
		global $weblog;
		$trip = TripApiClient::getActiveTripForOrder($orderId);
		$weblog->debug(print_r($trip, true));
		$response = array();
		if($trip['status'] == 'success'){
			$dc = DeliveryCenterApiClient::getDeliveryCenter($trip['data']['deliveryCenterId']);			
			$ds = DeliveryStaffApiClient::getDeliveryStaff($trip['data']['deliveryStaffId']);
			$response = array('status'=>'success', 'trip'=>$trip['data'],"dc"=> $dc['data'], 'ds'=>$ds['data']);
		}else{
			$response =  $trip;
		}
		return $response;
	}
}
?>