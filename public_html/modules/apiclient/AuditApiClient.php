<?php
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");
include_once("$xcart_dir/modules/apiclient/ApiClient.php");

class AuditApiClient
{
	public static function getAuditServiceApiClient($action, $key, $username='')
	{
		return new ApiClient('auditServiceUrl', $action, $key, $username);
	}
	
	public static function sendStyleAuditInfo($styleAuditData, $user='System', $comments='php.auditapi.client')
	{
		$apiClient = AuditApiClient::getAuditServiceApiClient('bulk', 'styleInfo', $user);
		$requests = array();
		$request = array();
		foreach($styleAuditData as $style)
		{
			//make an array of changed value which need to be audited
			if($style['entityPropertyOldValue'] != $style['entityPropertyNewValue'])
			{
				$auditData = array('audit' => array(
						"actorId" => $user,
						"actorName" => $user,
						"entityId" => $style['entityId'],
						"entityName" => $style['entityName'],
						"entityProperty" => $style['entityProperty'],
						"entityPropertyOldValue" => $style['entityPropertyOldValue'],
						"entityPropertyNewValue" => $style['entityPropertyNewValue'],
						"operationType" => $style['operationType'],
						"comments" => $comments
						));
				$request[] = $auditData;
			}
		}
		if(!empty($request))
		{
			//if no. of audit results is large break the request in multiple
			$maxBulkRequest = 50;
			$totalAuditCount = sizeof($request);
			$noLoop = floor($totalAuditCount/$maxBulkRequest);
			$responseData = array();
			for($i = 0; $i <= $noLoop; $i++)
			{
				$requests['data'] = array_slice($request, $i*$maxBulkRequest, $maxBulkRequest);
				$apiClient->postXmlRequest('auditEntriesResponse', $requests);
				if($apiClient->isSuccess())
				{
					$responseData[] = $apiClient->getData();
				}
			}
		}
		return $responseData;
	}
}
?>