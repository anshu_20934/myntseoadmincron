<?php

include_once("$xcart_dir/modules/apiclient/ApiClient.php");
include_once("$xcart_dir/modules/apiclient/CourierApiClient.php");
include_once($xcart_dir . "/Cache/Cache.php");

class ServiceabilityApiClient {

    private static function getServiceabilityApiClient($action, $key, $username = '') {
        return new ApiClient('pincodeServiceUrl', $action, $key, $username);
    }

    public static function getServiceabilityDetailsForZipcode($zipcode, $tagName = false) {
        $apiClient = ServiceabilityApiClient::getServiceabilityApiClient("checkServiceability/$zipcode", 'courier');
        $apiClient->getDataForAction();
        if ($apiClient->isSuccess()) {
            $couriers = $apiClient->getData();
            return $couriers;
        }
        return false;
    }

    public static function getWarehouseCourierServiceability($requestData) {
        $serviceabilityDetails = array();
        if ($requestData) {
            $apiClient = ServiceabilityApiClient::getServiceabilityApiClient("isServiceable", 'order');
            $apiClient->putXmlRequest('CourierServiceabilityRequest', $requestData);
            if ($apiClient->isSuccess()) {
                $response = $apiClient->getData();
                if ($response && !empty($response)) {
                    $order = $response[0];
                    $items = $order['items']['item'];

                    if ($items && !empty($items)) {

                        if (!isset($items[0]))
                            $items = array($items);

                        foreach ($items as $item) {
                            if (isset($item['paymentTypes']['paymentType']) && !empty($item['paymentTypes']['paymentType'])) {
                                $paymentTypes = $item['paymentTypes']['paymentType'];
                                if (!isset($paymentTypes[0]))
                                    $paymentTypes = array($paymentTypes);

                                foreach ($paymentTypes as $paymentType) {
                                    if (isset($paymentType['warehouses']['warehouse']) && !empty($paymentType['warehouses']['warehouse'])) {
                                        $warehouses = $paymentType['warehouses']['warehouse'];
                                        if (!isset($warehouses[0]))
                                            $warehouses = array($warehouses);
                                        foreach ($warehouses as $warehouse) {
                                            $couriers = $warehouse['couriers']['courier'];
                                            if (!is_array($couriers))
                                                $couriers = array($couriers);
                                            foreach ($couriers as $courier) {
                                                if (!in_array($courier, $serviceabilityDetails[$item['skuId']][$warehouse['warehouseId']]))
                                                    $serviceabilityDetails[$item['skuId']][$warehouse['warehouseId']][] = $courier;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $serviceabilityDetails;
    }

    public static function getServiceabilityDetails($requestData, $checkPaymentMode = false) {
        $serviceabilityDetails = false;
        if ($requestData) {
            $apiClient = ServiceabilityApiClient::getServiceabilityApiClient("isServiceable", 'order');
            $apiClient->putXmlRequest('CourierServiceabilityRequest', $requestData);

            if (!$apiClient->isSuccess()) {
                return "ERROR : " . $apiClient->getErrorMsg();
            }

            if ($apiClient->isSuccess()) {
                $serviceabilityDetails = array();
                $response = $apiClient->getData();
                if ($response && !empty($response)) {
                    $capacityMap = CourierApiClient::getAllCourierCapacityMap();

                    $order = $response[0];
                    $items = $order['items']['item'];

                    if ($items && !empty($items)) {

                        if (!isset($items[0]))
                            $items = array($items);

                        foreach ($items as $item) {

                            if (isset($item['paymentTypes']['paymentType']) && !empty($item['paymentTypes']['paymentType'])) {
                                $paymentTypes = $item['paymentTypes']['paymentType'];

                                if (!isset($paymentTypes[0]))
                                    $paymentTypes = array($paymentTypes);

                                foreach ($paymentTypes as $paymentType) {
                                    if (!$checkPaymentMode || empty($checkPaymentMode)) {
                                        if (isset($paymentType['warehouses']['warehouse']) && !empty($paymentType['warehouses']['warehouse'])) {
                                            $warehouses = $paymentType['warehouses']['warehouse'];
                                            if (!isset($warehouses[0]))
                                                $warehouses = array($warehouses);
                                            foreach ($warehouses as $warehouse) {
                                                $couriers = $warehouse['couriers']['courier'];
                                                if (!is_array($couriers))
                                                    $couriers = array($couriers);
                                                if (!empty($couriers)) {
                                                    $serviceabilityDetails[$item['skuId']][] = $paymentType['paymentMode'];
                                                }
                                            }
                                        } else {
                                            $serviceabilityDetails[$item['skuId']][] = array();
                                        }
                                    } else {
                                        if ($checkPaymentMode == $paymentType['paymentMode']) {
                                            if (isset($paymentType['warehouses']['warehouse']) && !empty($paymentType['warehouses']['warehouse'])) {
                                                $warehouses = $paymentType['warehouses']['warehouse'];
                                                if (!isset($warehouses[0]))
                                                    $warehouses = array($warehouses);
                                                foreach ($warehouses as $warehouse) {
                                                    $couriers = $warehouse['couriers']['courier'];
                                                    if (!is_array($couriers))
                                                        $couriers = array($couriers);
                                                    foreach ($couriers as $courier) {
                                                        $serviceabilityDetails[$item['skuId']][$warehouse['warehouseId']][$courier] = array('CODLimit' => $capacityMap[$courier]);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                $serviceabilityDetails[$item['skuId']] = array();
                            }
                        }
                    }
                }
            }
        }
        return $serviceabilityDetails;
    }

}

?>
