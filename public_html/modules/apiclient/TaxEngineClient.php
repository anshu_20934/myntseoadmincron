<?php

include_once("$xcart_dir/env/Host.config.php");

// This API client is to stamp the government tax for each non-cancelled line in a given release

class TaxEngineClient {

    public static function getTaxEngineClient($action, $key, $username = '') {
        return new ApiClient('taxationServiceUrl', $action, $key, $username);
    }

    public static function fetchGovtTax($sourceWarehouseId, $destinationPincode, $styleId, $itemCost, $login) {
        global $errorlog;
        $inputQuery = "styleId=" . $styleId . "&warehouseId=" . $sourceWarehouseId . "&pincode=" . $destinationPincode . "&mrpValue=" . $itemCost;
        $apiClient = TaxEngineClient::getTaxEngineClient('fetchGovtTax', 'govtTaxInfo', $login);

        $apiClient->getDataForAction($inputQuery);
        $orderDetail = array();
        $status = 'success';

        if ($apiClient->isSuccess()) {
            $data = $apiClient->getData();

            $orderDetail = array();
            $orderDetail['govtTaxAmount'] = $data[0]['taxAmount'];
            $orderDetail['govtTaxRate'] = $data[0]['taxRate'];
            $orderDetail['govtTaxRuleName'] = $data[0]['govtTaxRuleName'];
        } else {
            $errorlog->error("Unable to fetch govt tax " . $apiClient->getErrorMsg());
            $status = 'failure';
        }

        return $return = array('status' => $status, 'data' => $orderDetail);
    }

}
