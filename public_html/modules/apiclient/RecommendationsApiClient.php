<?php
include_once("$xcart_dir/utils/http/HttpClient.class.php");
include_once("$xcart_dir/utils/Helper.php");

class RecommendationsApiClient{
	
	private static function enableDisableSkusInRecommendations($styleIds,$user='System',$enableDisableFlag){
		$params = array();
		if(array($styleIds)){
			foreach($styleIds as $styleId){
				$params[] = array("method"=>"setProductValidity","params"=>array("ProductID"=> $styleId,"Valid"=>$enableDisableFlag));
			}
		}else{
			$params[] = array("method"=>"setProductValidity","params"=>array("ProductID"=> $styleIds,"Valid"=>$enableDisableFlag));
		}
		$url = Helper::getPropertyValue('recoServiceUrl');
		global $weblog;

		$url = $url;
		//post_async(HostConfig::$selfHostURL."/asyncGetPostHandler.php",array("url"=>$url,"method"=>"POST","data"=>json_encode($params),"user"=>$user));

	}
	
	public static function enableSkusInRecommendations($skuIds,$user='System'){
		global $weblog;
		$styleIdQuery = "select distinct style_id from mk_styles_options_skus_mapping where sku_id in ( ".implode(",", $skuIds)." )";
        $styleIds = func_query_column($styleIdQuery,'style_id');
        if(!empty($styleIds)){
        	RecommendationsApiClient::enableDisableSkusInRecommendations($styleIds, $user,true);	
        }
	}
	
	public static function disableSkusInRecommendations($skuIds,$user='System'){
		global $weblog;
		$styleIdQuery = "select style_id from mk_styles_options_skus_mapping where sku_id in ( ".implode(",", $skuIds)." ) and style_id not in (select style from mk_product_options po, mk_styles_options_skus_mapping msosm where po.id = msosm.option_id and is_active = 1 and msosm.sku_id in ( ".implode(",", $skuIds)." ) )";
        $styleIds = func_query_column($styleIdQuery,'style_id');
        if(!empty($styleIds)){
			RecommendationsApiClient::enableDisableSkusInRecommendations($styleIds, $user,false);
        }
	}
	
}
?>