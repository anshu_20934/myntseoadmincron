<?php
/**
 * Created by PhpStorm.
 * User: nitin.gurram
 * Date: 18/05/15
 * Time: 8:24 PM
 */

include_once("$xcart_dir/modules/apiclient/ApiClient.php");

class AtpApiClientV2 {

    private static function getClient($path, $response, $username='System') {
        return new ApiClient('atpServiceUrl', $path, $response, $username);
    }

    public static function getInventoryFromAtp($skuIds,$supply_type,$seller_id)
    {
        if ($skuIds) {
            $orderInventoryRequest = '';
            $sellerSupplyTypeSkuEntry = array();
            $data = array();
            foreach ($skuIds as $key => $skuId) {
                $sellerSupplyTypeSkuEntry['sellerId'] = $seller_id;
                $sellerSupplyTypeSkuEntry['skuId'] = $skuId;
                $sellerSupplyTypeSkuEntry['supplyType'] = $supply_type;
                $data[] = array('sellerSupplyTypeSku' => $sellerSupplyTypeSkuEntry);
            }

            if (!empty($data)) {
                $orderInventoryRequest['data'] = $data;
                $orderInventoryRequest['storeId'] = 1;

                $apiClient = AtpApiClientV2::getClient('inventory/orderInventory/', 'orderInventory');
                $apiClient->putXmlRequest('orderInventoryRequest', $orderInventoryRequest);

                if ($apiClient->isSuccess()) {
                    $sku_details = $apiClient->getData();
                    //echo "Avail Response " . print_r($sku_details, true);
                    $skuId2InvRowMap = array();
                    foreach ($sku_details as $row) {
                        $skuInvRow = array();
                        $skuInvRow['storeId'] = 1;
                        $skuInvRow['sellerId'] = $row['sellerId'];
                        $skuInvRow['skuId'] = $row['skuId'];
                        $skuInvRow['supplyType'] = $row['supplyType'];
                        $skuInvRow['leadTime'] = $row['leadTime'];
                        $skuInvRow['availableCount'] = $row['availableInventory'];
                        if ($row['availableInventory'] > 0) $skuInvRow['availableInWarehouses'] = $row['availableInWarehouses'];
                        $skuId2InvRowMap[$row['skuId']] = $skuInvRow;
                    }
                    return $skuId2InvRowMap;
                }
            }
        }
        return false;
    }

    public static function blockATPInventoryForItem($optionId = null, $itemId, $quantity)
    {
        if($optionId){
            $items = func_query("select od.seller_id,od.supply_type,sosm.sku_id,od.amount from xcart_order_details od,
                            mk_order_item_option_quantity oq, mk_styles_options_skus_mapping sosm where od.itemid = oq.itemid and
                            sosm.option_id = $optionId and od.product_style = sosm.style_id and od.itemid = $itemId group by od
                            .seller_id,od.supply_type,sosm.sku_id");
        }else {
            $items = func_query("select od.seller_id,od.supply_type,sosm.sku_id,od.amount from xcart_order_details od,
                            mk_order_item_option_quantity oq, mk_styles_options_skus_mapping sosm where od.itemid = oq.itemid and
                            oq.optionid = sosm.option_id and od.product_style = sosm.style_id and od.itemid = $itemId group by od
                            .seller_id,od.supply_type,sosm.sku_id");
        }

        if ($items) {
            $data = array();
            foreach ($items as $key => $item) {
                $blockInventoryEntry['sellerId'] = $item['seller_id'];
                $blockInventoryEntry['skuId'] = $item['sku_id'];
                $blockInventoryEntry['quantity'] = $quantity;
                $blockInventoryEntry['supplyType'] = $item['supply_type'];
                $data[] = array('blockInventory' => $blockInventoryEntry);
            }

            if (!empty($data)) {
                $blockInventoryRequest['data'] = $data;
                $blockInventoryRequest['storeId'] = 1;

                $apiClient = AtpApiClientV2::getClient('inventory/blockInventory/', 'blockInventoryResponse');
                $apiClient->putXmlRequest('blockInventoryRequest',$blockInventoryRequest);

                if($apiClient->isSuccess()){
                    return true;
                }
            }

        }
        return false;
    }

    public static function blockATPInventory($orderId, $itemIds = null)
    {
        if ($itemIds != null) {
            if (!is_array($itemIds)) {
                $itemIds = array($itemIds);
            }
            $items = func_query("select od.seller_id,od.supply_type,sosm.sku_id,od.amount from xcart_order_details od,
                                mk_order_item_option_quantity oq, mk_styles_options_skus_mapping sosm where od.itemid = oq.itemid
                                and oq.optionid = sosm.option_id and od.product_style = sosm.style_id and
                                od.itemid in (" . implode(",", $itemIds) . ") group by od.seller_id,od.supply_type,sosm.sku_id");
        } else {
            $items = func_query("select od.seller_id,od.supply_type,sosm.sku_id,od.amount from xcart_orders o, xcart_order_details od,
                                mk_order_item_option_quantity oq, mk_styles_options_skus_mapping sosm where o.orderid = od.orderid and
                                od.itemid = oq.itemid and oq.optionid = sosm.option_id and od.product_style = sosm.style_id
                                and od.orderid = $orderId group by od.seller_id,od.supply_type,sosm.sku_id");
        }
        if ($items) {
            $data = array();
            foreach ($items as $key => $item) {
                $blockInventoryEntry['sellerId'] = $item['seller_id'];
                $blockInventoryEntry['skuId'] = $item['sku_id'];
                $blockInventoryEntry['quantity'] = $item['amount'];
                $blockInventoryEntry['supplyType'] = $item['supply_type'];
                $data[] = array('blockInventory' => $blockInventoryEntry);
            }

            if (!empty($data)) {
                $blockInventoryRequest['data'] = $data;
                $blockInventoryRequest['storeId'] = 1;

                $apiClient = AtpApiClientV2::getClient('inventory/blockInventory/', 'blockInventoryResponse');
                $apiClient->putXmlRequest('blockInventoryRequest', $blockInventoryRequest);

                if ($apiClient->isSuccess()) {
                    return true;
                }
            }

        }
        return false;
    }
}

?>