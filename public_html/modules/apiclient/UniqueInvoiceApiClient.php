<?php

include_once("$xcart_dir/env/Host.config.php");
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UniqueInvoiceApiClient
 *
 * @author sriram.manoj
 */
class UniqueInvoiceApiClient {

    public static function getUniqueInvoiceApiClient($action, $key, $username = '') {
        return new ApiClient('omsSequenceGeneratorUrl', $action, $key, $username);
    }

    public static function getUniqueInvoice($orderReleaseId, $warehouseId) {

        $uniqueInvoiceId = func_query_first_cell("select `value` from order_additional_info where order_id_fk in (" . $orderReleaseId . ") and `key` = 'UNIQUE_INVOICE_NUMBER'");

        if ($uniqueInvoiceId) {
            return $uniqueInvoiceId;
        } else {
            $client = UniqueInvoiceApiClient::getUniqueInvoiceApiClient('getSequenceNumber', false);
            $query = 'orderReleaseId=' . $orderReleaseId . '&warehouseId=' . $warehouseId;
            $client->get($query);
            if ($client->isSuccess()) {
                $uid = $client->getData();
                $data = '';
                foreach ($uid['SequenceGeneratorEntry'][0] as $key => $value) {
                    if ($key == 'lastSequenceNumber') {
                        $data = $value;
                    }
                }
                func_query("insert into order_additional_info(order_id_fk,`key`,`value`,created_on) values ($orderReleaseId,'UNIQUE_INVOICE_NUMBER','$data',now())");
                return $data;
            } else {
                return '';
            }
        }
    }

}
