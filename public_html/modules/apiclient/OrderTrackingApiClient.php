<?php

include_once("$xcart_dir/modules/apiclient/ApiClient.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/include/class/class.feature_gate.keyvaluepair.php");

class OrderTrackingApiClient {

	public static function getOrderTrackingApiClient($action, $key, $username='') {
		return new ApiClient('orderTrackingServiceUrl', $action, $key, $username);
	}
	
	public static function getTrackingDetailsForOrder($courieroperator, $trackingNo, $level="LEVEL1"){
		$apiClient = OrderTrackingApiClient::getOrderTrackingApiClient("getOrderTrackingDetail/$courieroperator/$trackingNo?level=".strtoupper($level), 'orderTracking');
		$apiClient->getDataForAction();
		if($apiClient->isSuccess()) {
			return $apiClient->getData();
		}
		return array();
	}
}

?>