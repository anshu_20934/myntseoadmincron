<?php

include_once("$xcart_dir/modules/apiclient/ApiClient.php");
include_once("$xcart_dir/modules/apiclient/ItemApiClientOld.php");
include_once($xcart_dir."/Cache/Cache.php");

class ItemApiClient {

	private static function getItemServiceApiClient($action, $username='') {
		return new ApiClient('orchWMSIntegrationUrl', $action, false, $username);
	}
	
	public static function getRejectedItemServiceApiClient($action, $username='') {
		return new ApiClient('itemRejectReasonServiceUrl', $action, 'rrEntry', $username);
	}

	public static function moveItemsToAcceptedReturns($itembarcodes, $warehouseid, $username){
		$apiClient = ItemApiClient::getItemServiceApiClient("acceptedReturns");
		$request = array();
		$request['warehouseId'] = $warehouseid;
		$request['destinationBinBarcode'] = 1;

		$returnbinBarcode = trim(WidgetKeyValuePairs::getWidgetValueForKey("returns.accept.binbarcode.".$warehouseid));
		$request['destinationBinBarcode'] = $returnbinBarcode;

		$count = 0;
		$itemBarcodeList = array();
		foreach($itembarcodes as $barcode) {
			$itemBarcodeList[$count++]['itemBarcode'] = $barcode;
		}
		$request['itemBarcodes'] = $itemBarcodeList;

		$apiClient->putXmlRequest("updateOrderReleaseRequest", $request);

		if(!$apiClient->isSuccess()) {
			return $apiClient->getErrorMsg();
		}
	}
	
	public static function getItemQualityCheckCodes($stage=false) {
		$cache = new \Cache("RejectReasons", "rejectReasons-$stage");
		$params = array($stage);
		
		$qualityCheckCodes = $cache->get(function($stage){
			$apiClient = ItemApiClient::getRejectedItemServiceApiClient('');
			$query = false;
			if($stage)
				$query = "rejectedAt.eq:$stage";
			
			$apiClient->search($query, array('limit' => -1));
			$qualityCheckCodes = array(); 
			if($apiClient->isSuccess()) {
				$data = $apiClient->getData();
				foreach($data as $row) {
					$qualityCheckCodes[$row['id']] = array( 'id'=> $row['id'], 'quality' => $row['quality'], 'rejectReason' => $row['rejectReason'], 'rejectReasonDescription' => $row['rejectReasonDescription']); 
				}
			}
			return $qualityCheckCodes;
		}, $params, "rejectreasons-$stage", 24*60*60);
		
		return $qualityCheckCodes;
	}
	
	public static function get_itemids_for_barcodes($warehouseId, $itemBarCodes, $mapping=true) {
		$items = ItemApiClient::get_items_for_barcodes($warehouseId, $itemBarCodes);
		$results = array();
		if($mapping) {
			foreach($items as $row) {
				$results["".$row['barcode']] = $row['barcode'];
			}
		} else {
			foreach($items as $row) {
				if(!in_array($row['barcode'], $results))
					$results[] = $row['barcode'];
			}
		}
		return $results;
	}
	
	public static function get_items_for_barcodes($warehouseId, $itemBarCodes) {
		if($warehouseId <= 0)
			return array();
		
		$apiClient = ItemApiClient::getItemServiceApiClient("getItemDetails/$warehouseId");
		if(!is_array($itemBarCodes)) {
			$itemBarCodes = array($itemBarCodes);
		}
	
		if(count($itemBarCodes) > 0) {
			$itemBarcodesQuery = "itemIds=".implode("&itemIds=", $itemBarCodes);
			$apiClient->get($itemBarcodesQuery);
				
			if($apiClient->isSuccess())
				return $apiClient->getData();
		}
		return array();
	}

	public function get_skuitems_for_order($orderid, $warehouseId=false) {
		if($orderid != '' && $orderid != null){
			if(!$warehouseId) {
				$order = func_query_first("select warehouseid from xcart_orders where orderid = $orderid", true);
				$warehouseId = $order['warehouseid'];
			}
			
			if($warehouseId <= 0)
				return array();
			
			$apiClient = ItemApiClient::getItemServiceApiClient("getItemDetails/$warehouseId/$orderid");
			$orderid = trim($orderid);
			$apiClient->getDataForAction();
			if($apiClient->isSuccess())
				return $apiClient->getData();
		}
		return false;
	}
	
	public static function getSkuWiseItemDetailsForOrder($orderid, $warehouseId) {
		global $weblog;
		$orderid = trim($orderid);
		
		$data = ItemApiClient::get_skuitems_for_order($orderid, $warehouseId);

		if($data !== false) {
			$sku2ItemsMapping = array();
			foreach($data as $row) {
				if(!isset($sku2ItemsMapping[$row['sku']['id']]))
				$sku2ItemsMapping[$row['sku']['id']] = array();
					
				$sku2ItemsMapping[$row['sku']['id']][] = $row['barcode'];
			}
			$weblog->info("Sku Items fetched for Order $orderid - ". print_r($sku2ItemsMapping, true));
			return $sku2ItemsMapping;
		}
		return false;
	}

	public static function get_issued_itembarcodes_for_order($orderid) {
		$item_bar_codes = array();
		$orderid = trim($orderid);
		$data = ItemApiClient::get_skuitems_for_order($orderid);
		if($data !== false) {
			foreach($data as $row) {
				$item_bar_codes[] = $row['barcode'];
			}
			return $item_bar_codes;
		}
		return false;
	}

	public static function get_returnable_itembarcodes_and_skuids_for_order($orderid) {
		$itembarcode2ItemDetails = array();
		$orderid = trim($orderid);
		
		$items = ItemApiClient::get_skuitems_for_order($orderid);
		foreach($items as $row) {
			$itembarcode2ItemDetails[$row['barcode']] = array('itemStatus'=>$row['itemStatus'],
					'skuId'=>$row['sku']['id'],
					'warehouseId'=>$row['sku']['warehouseId']);
		
		}
		return $itembarcode2ItemDetails;
	}

	public static function remove_item_order_association($orderId, $warehouseId, $itemBarCodes, $itemStatus, $quality=false, $user, $issue=false, $reject_comment=false) {
		global $weblog, $xcart_dir;
		if(!is_array($itemBarCodes)){
			$itemBarCodes = array($itemBarCodes);
		}
		if($itemStatus == 'RETURN_FROM_OPS') {
			$apiClient = ItemApiClient::getItemServiceApiClient("updateItemQAFail/$warehouseId", $user);
		} else {
			$apiClient = ItemApiClient::getItemServiceApiClient("updateItemCustomerReturns/$warehouseId", $user);
		}
		
		$itemUpdateReq = array('orderId'=>$orderId, 'itemStatus'=>$itemStatus);
		if($quality){
			$itemUpdateReq['quality'] = $quality;
		}
		if($issue){
			$itemUpdateReq['rejectReason'] = $issue;
		}
		if($reject_comment){
			$itemUpdateReq['rejectReasonDescription'] = $reject_comment;
		}
		$itemUpdateReq['itemBarcodes'] = array();
		foreach ($itemBarCodes as $barcode) {
			$itemUpdateReq['itemBarcodes'][] = array('itemBarcode'=>$barcode); 
		}
		$apiClient->putXmlRequest('updateItemRequest', $itemUpdateReq);
		if($apiClient->isSuccess()) {
			return true;
		}
		else
			return $apiClient->getErrorMsg();
	}
	
    public static function cancelOrderUpdateSkusItems($orderid, $warehouseId, $user, $item_status, $quality = false, $rejectReason = false, $rejectReasonDescription = false) {
        global $weblog, $xcart_dir;
        $orderid = trim($orderid);
        
        $issued_items = ItemApiClient::get_skuitems_for_order($orderid, $warehouseId);

        if ($issued_items !== false) {
            $weblog->info("Items issued for order $orderid = " . print_r($issued_items, true));
            if (count($issued_items) > 0) {
                $itemIds = array();
                foreach ($issued_items as $item) {
                    $itemIds[] = $item['barcode'];
                }
                $status = ItemApiClient::remove_item_order_association($orderid, $warehouseId, $itemIds, $item_status, $quality, $user, $rejectReason, $rejectReasonDescription);
            }
            return array();
        }
        return false;
    }

	public static function cancelItemsFromOrder($orderid, $warehouseId, $skuid, $original_qty, $qty_to_cancel, $user) {
		global $weblog, $xcart_dir;
		$orderid = trim($orderid);
		
		$issued_items = ItemApiClient::get_skuitems_for_order($orderid, $warehouseId);
		
		if ($issued_items !== false) {
			
			$issuedSkuItems = array();
			foreach($issued_items as $item) {
				if($item['sku']['id'] == $skuid)
					$issuedSkuItems[] = $item;
			}
			
			if( count($issuedSkuItems) > 0) {
				// check the no of items that will be present in the order item....
				$keepCount = $original_qty - $qty_to_cancel;

				// if the keep count is <= items issued
				// then from issued items remove the items to keep
				$status = true;
				if ($keepCount < count($issuedSkuItems)) {
					$index = $keepCount;
					$itemIds = array();
					$removedItemBarCodes = array();
					for ($index; $index < count($issuedSkuItems); $index++) {
						$itemIds[] = $issuedSkuItems[$index]['barcode'];
						$removedItemBarCodes[] = $issuedSkuItems[$index]['barcode'];
					}
						
					if(count($itemIds) > 0) {
						$staus = ItemApiClient::remove_item_order_association($orderid, $warehouseId, $itemIds, 'RETURN_FROM_OPS', false, $user);
					}
				}
			}
		} else {
			$weblog->info("Failed to get items for order $orderid - ");
			return false;
		}
		return array();
	}

	public static function validateOrderItemsStatus($orderid, $warehouseId, $valid_statuses) {
		$items = ItemApiClient::get_skuitems_for_order($orderid, $warehouseId);
		if($items && count($items) > 0) {
			foreach($items as $item) {
				if(!in_array($item['itemStatus'], $valid_statuses)){
					return false;
				} else {
					$valid_items[] = $item;
				}
			}
			return $valid_items;
		}
		return false;
	}
	
	
	/*public static function getStoredItemsForSku($skuIds, $warehouseId) {
		
		$items = array();
		if(!is_array($skuIds))
			$skuIds = array($skuIds);
		
		$apiClient = ItemApiClient::getItemServiceApiClient('');
		$apiClient->search("sku.id.in:".implode(",", $skuIds)."___itemStatus.eq:STORED___quality.eq:Q1___warehouseId.eq:$warehouseId&start=0&fetchSize=1");
		if($apiClient->isSuccess()) {
			$items = $apiClient->getData();
		} else {
			return $apiClient->getErrorMsg();
		}
		return $items;
	} */
}

?>
