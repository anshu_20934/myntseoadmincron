<?php

include_once("$xcart_dir/modules/apiclient/ApiClient.php");
include_once("$xcart_dir/include/func/func.mk_orderbook.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once ("$xcart_dir/include/class/class.feature_gate.keyvaluepair.php");

class ShipmentApiClient {

	public static function getOrderServiceApiClient($action, $key, $username='') {
		return new ApiClient('lmsOrderServiceUrl', $action, $key, $username);
	}
	
	public static function getOrdersInLMSOrderFormat($order){
		
		$productsInCart = func_create_array_products_of_order($order['orderid']);
		
		$orderObj = array("orderId"=>$order["orderid"],
		"title"=>ereg_replace("[^A-Za-z0-9 _]", "", $order["title"]), 
		"firstName"=>ereg_replace("[^A-Za-z0-9 _]", "", $order["s_firstname"]),
		"lastName"=>ereg_replace("[^A-Za-z0-9 _]", "", $order["s_lastname"]), 
		"address"=>ereg_replace("[^A-Za-z0-9 _]", "", $order["original_address"]), 
		"city"=>substr($order["s_city"], 0,50), 
		"state"=>$order["s_state"], 
		"country"=>$order["s_country"],  
		"zipcode"=>$order["s_zipcode"],  
		"mobile"=>$order["mobile"],  
		"custMobile"=>"",
		"email"=>$order["login"],
        "cod"=>$order["payment_method"] == 'cod'? 'true':'false',
        "codAmount"=>$order["payment_method"] == 'cod'?round(($order["total"] + $order["cod"])):0, 
        "warehouseId"=>$order["warehouseid"], 
        "shipmentType"=>'DL',
       	"courierOperator" =>$order["courier_service"], 
        "trackingNumber"=>$order["tracking"],
		"total"=>$order["total"],
	    "giftCertDiscount"=>$order["giftcert_discount"],
		"discount"=>$order["discount"],
	    "cartDiscount"=>$order["cart_discount"],
	    "refDiscount"=>$order["ref_discount"],
	    "couponDiscount"=>$order["coupon_discount"],
	    "shippingCost"=>$order["shipping_cost"],
	    "giftCharges"=>$order["gift_charges"],
	    "collectedAmount"=>$order["collected_amount"],
	    "refundAmount"=>$order["refund_amount"],
	    "cashBack"=>$order["cashback"],
            "govtTaxAmount"=>$order["govt_tax_amount"],
	    "cashRedeemed"=>$order["cash_redeemed"],
		"promiseDate"=>date('Y-m-d\TH:i:s', getDeliveryDateForOrder($order['queueddate'], $productsInCart, $order['s_zipcode'], $order['payment_mode'],$order['courier_service'], $order['warehouseid']))
        );
		
		$productDetails = '';
		foreach($productsInCart as $product) {
			if(!empty($productDetails)) $productDetails .= ",";
			$productDetails .= $product['typename'].":".$product['product_display_name'];
		}
		$orderObj['itemDescription'] = $productDetails;
        
		$sqlAmountDetail = "select sum(price) subtotal, sum(taxamount) tax from xcart_order_details where item_status!= 'IC' and orderid = " . $order["orderid"];
		$amountDetail = func_query_first($sqlAmountDetail, true);
		if($amountDetail!== false){
			$orderObj["subtotal"]=$amountDetail["subtotal"];			
			$orderObj["tax"]=$amountDetail["tax"];
		}
		$salesorderid = func_query_first_cell("select ex.releaseid from exchange_order_mappings ex, xcart_orders o where "
				 ."ex.exchange_orderid = $order[orderid] and o.orderid = ex.exchange_orderid and o.status not in ('F', 'D', 'L')", true);
		if($salesorderid){
			$orderObj['salesOrderId'] = $salesorderid;
		}
        $sqlCustDetail = "SELECT xc.mobile AS cust_mobile from xcart_customers xc WHERE xc.login = '" . $order["login"] ."'";
    	$custDetail = func_query_first($sqlCustDetail, true);
    	if($custDetail!== false){
    		$orderObj["custMobile"] = $custDetail["cust_mobile"]; 
    	}
    	
		return $orderObj;
	}

	public static function pushOrderToLMS($orderInDB) {
		global $weblog;
	
		$weblog->info("Pushing-To-LMS Order ID: " . $order['orderId']);
		$order = ShipmentApiClient::getOrdersInLMSOrderFormat($orderInDB);
			
		$request = array('data' => array());
       	$request['data'][] = array('order' => $order);	
		
		$apiClient = ShipmentApiClient::getOrderServiceApiClient('bulkCreateOrUpdate', 'order');
		$apiClient->postXmlRequest('orderResponse', $request);
		$status=true;
		if(!$apiClient->isSuccess()) {
			$status = $apiClient->getErrorMsg();
		}
		$weblog->debug("Pushed LD orders status: " . $status); 
		return $status;
	}

	public static function createReturnPickUpRequest($returnid,$courier_service,$moveToStatus=NULL,$deliveryCenterId=NULL) {
		global $weblog;
		
		if(!$returnid) {
			return;
		}
		
		$template = "xo.orderid as orderId, xo.title as title, xo.s_firstname as firstName, " .
                        "xo.s_lastname as lastName, xr.address as address, xr.itemid, xr.city as city, xr.state as state," .
                        "xr.country as country, xr.zipcode as zipcode, xr.mobile as mobile, xc.mobile AS custMobile, xr.login as email" .
                        ", if(xo.payment_method = 'cod', 'true','false') as cod, concat(msp.product_display_name,':Size - ',po.value,':Qty - ',xr.quantity,':SKU code - ',xr.skucode,
                           ':Article No - ',msp.article_number,':MRP per qty - ',od.price) as itemDescription, " .
                "null as codAmount, xo.warehouseid as warehouseId, " .
                "'PU' as shipmentType, '$courier_service' as courierOperator, '$moveToStatus' as status, '$deliveryCenterId' as deliveryCenterId, null as trackingNumber,xr.returnid as returnId, msp.style_id as styleIds";

                $return_detail_query .= "SELECT  $template from xcart_orders xo, xcart_order_details od, xcart_returns xr, xcart_customers xc, mk_order_item_option_quantity oq, mk_product_options po, mk_style_properties msp " .
                                " where od.itemid = xr.itemid and od.itemid = oq.itemid and od.product_style = msp.style_id and oq.optionid = po.id and xo.login = xc.login and xo.orderid = xr.orderid and xr.returnid = $returnid";

		$return = func_query_first($return_detail_query);
		$return['title'] = ereg_replace("[^A-Za-z0-9 _]", "", $return["title"]);
		$return['firstName'] = ereg_replace("[^A-Za-z0-9 _]", "", $return["firstName"]);
		$return['lastName'] = ereg_replace("[^A-Za-z0-9 _]", "", $return["lastName"]);
		$return['address'] = ereg_replace("[^A-Za-z0-9 _]", "", $return["address"]);

    	$request['data'][] = array('order' => $return);	
		
		$apiClient = ShipmentApiClient::getOrderServiceApiClient('createReturn', 'order');
		$apiClient->postXmlRequest('orderResponse', $request);
		$status = array();
		if($apiClient->isSuccess()) {
			$response_data = $apiClient->getData();
			$rtOrders = array_values($response_data);
			foreach ( $rtOrders as $order ) {
				$weblog->debug("Filing: " . $order['returnId']);
				$tracking_number = $order['trackingNumber'];
				$deliveryCenterCode = $order['deliveryCenterCode'];
			}
			$invalidOrderIds = null;
			$status = array("status" => "success","msg"=>"Return pushed successfully for local delivery.","tracking_number" => $tracking_number,"deliveryCenterCode"=>$deliveryCenterCode);
		}else {
			$status["status"] = "Failed";
			$status["msg"] = "Error occurred while pushing return for local delivery.";
		}
		return $status;
	}

	public static function fileRto($orderid){
               $apiClient = ShipmentApiClient::getOrderServiceApiClient('updateOrderStatus/'.$orderid.'/RTO', 'tripOrderResponse');
               $apiClient->post();
               if ($apiClient->isSuccess()) {
                       return array('status'=>true);
               }else{
                       return array('status'=>false,'reason'=>$apiClient->getErrorMsg());
               }
      }
	
	public static function rejectReturn($returnid){
		$apiClient = ShipmentApiClient::getOrderServiceApiClient("updateOrderFromPickup/$returnid/REJ", 'orderResponse');
		$apiClient->post();
		$status_code = $apiClient->getStatusCode();
		//875 means return is not available in lms
		if($apiClient->isSuccess() || $status_code == 875){	
				$status["status"] = "success";
				$status["msg"] = "Rejected in lms";	
			}else{
				$status["status"] = "failed";
				$status["msg"] = $response_data['statusMessage'];
			}
		return $status;
	}
	
}

?>
