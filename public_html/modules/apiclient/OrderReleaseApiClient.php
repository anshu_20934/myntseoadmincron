<?php
include_once("$xcart_dir/env/Host.config.php");

// This API client is to stamp the government tax for each non-cancelled line in a given release

class OrderReleaseApiClient {

    public static function getOrderReleaseApiClient($action, $key, $username = '') {
        return new ApiClient('omsReleaseServiceUrl', $action, $key, $username);
    }
    
    public static function stampGovtTax($orderid, $login) {
        global $weblog;
        global $errorlog;
        $apiClient = OrderReleaseApiClient::getOrderReleaseApiClient('stampGovtTax/'.$orderid, 'orderReleaseEntry', $login);
        
        $apiClient->getDataForAction();
        $orderDetail = array();
        $status = 'success';

        if ($apiClient->isSuccess()) {
            $data = $apiClient->getData();
            if ($apiClient->getTotalCount() > 1) {
                $orderDetail = array_values($data[0]);
            } else {
                $orderDetail = $data;
            }
        } else {
            $errorlog->error("Unable to stamp govt tax " . $apiClient->getErrorMsg());
            $status = 'failure';
        }

        return $return = array('status' => $status, 'data' => $orderDetail);
        
    }
}