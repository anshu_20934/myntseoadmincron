<?php

include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");
include_once("$xcart_dir/modules/apiclient/ApiClient.php");

class NotificationApiClient {
	
	
    private static function getLocalEmailSenderClient($action, $key, $username='') {
		return new ApiClient('notificationServiceUrl', $action, $key, $username);
	}
	
	
	public static function sendEmail($to,$from,$subject,$body,$mime)
	{
		global $weblog;
		$retMsg = '';
		$apiClient = NotificationApiClient::getLocalEmailSenderClient('email/amazon','emailResponse');
		$data = array("body"=>$body,"sender"=>$from,"to"=>$to,"subject"=>$subject,"mimeType"=>$mime,"bcc"=>'myntramailarchive@gmail.com',"smtp"=>"AMAZON");
		$apiClient->postXmlRequest('emailEntry',$data);
		
		$statusCode = $apiClient->getStatusCode();
		$data = $apiClient->getData();
		$errMsg = $apiClient->getErrorMsg();
		if($statusCode == '500'){
			$retMsg = "API Call Failure. See if notification service is up".$apiClient->getErrorMsg();
		}else {
			$retMsg="SUCCESS";
		}
		return $retMsg;
	}
	
	
	
	
	
}
