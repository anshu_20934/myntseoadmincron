<?php
/**
 * Created by PhpStorm.
 * User: nitin.gurram
 * Date: 28/04/15
 * Time: 3:59 PM
 */

include_once("$xcart_dir/env/Host.config.php");

// This API client is to stamp the government tax for each non-cancelled line in a given release

class OrdeLineApiClient {

    public static function getOrdeLineApiClient($action, $key, $username = '') {
        return new ApiClient('taxationServiceUrl', $action, $key, $username);
    }

    public static function getByStoreLineId($storeLineId)
    {
        global $errorlog;
        $apiClient = OrdeLineApiClient::getOrdeLineApiClient('getByStoreLineId/' . $storeLineId, 'orderLine');

        $apiClient->getDataForAction();
        $orderLineDetail = array();
        $status = 'success';

        if ($apiClient->isSuccess()) {
            $data = $apiClient->getData();
            if ($apiClient->getTotalCount() == 0) {
                $errorlog->error("Unable to fetch data from oms " . $apiClient->getErrorMsg());
                $status = 'failure';
            } else if ($apiClient->getTotalCount() > 1) {
                $orderLineDetail = array_values($data[0]);
            } else {
                $orderLineDetail = $data;
            }
        } else {
            $errorlog->error("Unable to fetch data from oms " . $apiClient->getErrorMsg());
            $status = 'failure';
        }

        return $return = array('status' => $status, 'data' => $orderLineDetail);

    }
}