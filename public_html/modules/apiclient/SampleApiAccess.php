<?php
    include_once("../../auth.php");
    include_once("$xcart_dir/modules/apiclient/ApiClient.php");
    include_once("$xcart_dir/modules/apiclient/SkuApiClient.php");
    include_once("$xcart_dir/modules/apiclient/MinacsApiClient.php");
	/*
    $apiClient = new ApiClient('skuServiceUrl', '');
    $response = $apiClient->search();
    $weblog->info("Http Client Response is - ".print_r($response, true));
    
    $apiClient = new ApiClient('skuServiceUrl', '');
    $response = $apiClient->search('vendorArticleNo:L40276');
    $weblog->info("Http Client Response is - ".print_r($response, true));
    
    $apiClient = new ApiClient('skuServiceUrl', '');
    $response = $apiClient->getById(3816);
    $weblog->info("Http Client Response is - ".print_r($response, true));
    
    $apiClient = new ApiClient('skuServiceUrl', 'get_brands');
    $response = $apiClient->getDataForAction();
    $weblog->info("Http Client Response is - ".print_r($response, true));
       
    $apiClient = new ApiClient('skuServiceUrl', 'get_inventory_counts');
    $response = $apiClient->getDataForAction(array('skuId'=>18845));
    $weblog->info("Http Client Response is - ".print_r($response, true));
    
    $apiClient = new ApiClient('skuServiceUrl', '');
    $response = $apiClient->update(18845, 'sku', array('quantitySold'=>2));
    $weblog->info("Http Client Response is - ".$response);
    
    
    $invcountreq = array();
    $invcountreq['data'] = array('invCount'=>array('column'=>'inv_count', 'count'=>1, 'operation'=>'increment', 'quality'=>'Q1', 'skuId'=>18845, 'warehouseId'=>1));
    
    $apiClient = new ApiClient('skuServiceUrl', 'change_count');
    $response = $apiClient->putXmlRequest('invCountResponse', $invcountreq);
    $weblog->info("Http Client Response is - ".$response);
    
    
    $skulocationReq = array();
    $skulocationReq['data'] = array();
    $skulocationReq['data']['skuLocation'] = array('skuId'=>18845);
    
    $invcountreq = array();
    $invcountreq['data'] = array('invCount'=>array('column'=>'inv_count', 'count'=>1, 'operation'=>'increment', 'quality'=>'Q1', 'skuId'=>18845, 'warehouseId'=>1));
    
    $apiClient = new ApiClient('skuServiceUrl', 'change_count');
    $response = $apiClient->putXmlRequest('invCountResponse', $invcountreq);
    $weblog->info("Http Client Response is - ".$response);
    
    $invcountreq = array();
    $invcountreq['data'] = array();
    $invcountreq['data'][] = array('invCount'=>array('quality'=>'Q1', 'skuId'=>18490));
    $invcountreq['data'][] = array('invCount'=>array('quality'=>'Q1', 'skuId'=>18491));
    $invcountreq['data'][] = array('invCount'=>array('quality'=>'Q1', 'skuId'=>18492));
    $invcountreq['data'][] = array('invCount'=>array('quality'=>'Q1', 'skuId'=>18493));
    
    $apiClient = new ApiClient('skuServiceUrl', 'get_avaliable_count');
    $response = $apiClient->postXmlRequest('invCountResponse', $invcountreq);
        
    $weblog->info("Http Client Response is - ".print_r($response, true));
    
    
    $invcountreq = array('data' => array('invCount'=>array('quality'=>'Q1', 'skuId'=>18492)));
    $apiClient = new ApiClient('skuServiceUrl', 'get_avaliable_count');
    $response = $apiClient->postXmlRequest('invCountResponse', $invcountreq);
        
    $weblog->info("Http Client Response is - ".print_r($response, true));
    
    $invcountreq = array();
    $invcountreq['data'] = array();
    $invcountreq['data'][] = array('invCount'=>array('quality'=>'Q1', 'skuId'=>18490));
    $invcountreq['data'][] = array('invCount'=>array('quality'=>'Q1', 'skuId'=>18491));
    $invcountreq['data'][] = array('invCount'=>array('quality'=>'Q1', 'skuId'=>18492));
    $invcountreq['data'][] = array('invCount'=>array('quality'=>'Q1', 'skuId'=>18493));
    
    $apiClient = new ApiClient('skuServiceUrl', 'get_avaliable_count');
    $response = $apiClient->postXmlRequest('invCountResponse', $invcountreq);
    $weblog->info("Http Client Response is - ".print_r($response, true));
    
    $apiClient = new ApiClient('itemServiceUrl', '');
    $response = $apiClient->update(2, "item", array('quality'=>'Q2', 'orderId'=>0, 'itemStatus'=>'STORED'));
    $weblog->info("Http Client Response is - ".print_r($response, true));
    
    $apiClient = new ApiClient('itemServiceUrl', '');
    $response = $apiClient->search('orderId:1752947');
	$weblog->info("Http Client Response is - ".print_r($response, true));
    
    
	$apiClient = new ApiClient('itemServiceUrl', '');
	$skuCodes = "PUMASPSH59135,DCMSTSHT67298";
    $response = $apiClient->detailsearch("skuBarcode.in:$skuCodes");
    
    $skuCode2itemIdsMapping = array();
	if($response['success'] && $response['success'] == 1) {
		$items = $response['results']['data']['item'];	
		foreach ($items as $item) {
			if(!isset($skuCode2itemIdsMapping[$item['skuBarcode']]))
				$skuCode2itemIdsMapping[$item['skuBarcode']] = array();
			
			$skuCode2itemIdsMapping[$item['skuBarcode']][] = $item['itemId'];
		}
	}
	$weblog->info("Http Client Response is - ".print_r($skuCode2itemIdsMapping, true));
	$itemIds = "";
	foreach ($skuCode2itemIdsMapping as $skuCode=>$itemIdsArray) {
		if($itemIds != "")
			$itemIds .= ",";
		
		$itemIds .= implode(",", $itemIdsArray);
	}
		
	$apiClient = new ApiClient('locationTrackerServiceUrl', '');
	$response = $apiClient->search("entityId.in:$itemIds"."___entityName.eq:com.myntra.framework.entities.Item___active.eq:true", false);
	$weblog->info("Http Client Response is - ".print_r($response, true));
	
	$invcountreq = array();
    $invcountreq['data'] = array('invCount'=>array('column'=>'inv_count', 'count'=>1, 'operation'=>'increment', 'quality'=>'Q1', 'skuId'=>18845, 'warehouseId'=>1));
    
    $apiClient = new ApiClient('skuServiceUrl', 'change_count', 'sanjay.yadav');
    $response = $apiClient->putXmlRequest('invCountResponse', $invcountreq);
    $weblog->info("Http Client Response is - ". print_r($response, true));
    
	$update_data = array('orderId'=>0, 'itemStatus'=>'RETURN_FROM_OPS');
	$apiClient = new ApiClient('itemServiceUrl', '', 'sanjay.yadav');
	$response = $apiClient->bulkUpdate($update_data, "id.in:2,3");
	$weblog->info("Http Client Response is - ". print_r($response, true));
    
    $apiClient = new ApiClient('cartonItemServiceUrl', '', '');
    $response = $apiClient->search('carton.id.eq:9');
	$weblog->info("Http Client Response is - ".print_r($response, true));
	
	$apiClient = new ApiClient('locationTrackerServiceUrl', '', '');
	$response = $apiClient->search("entityId.in:1,2,3,9"."___entityName.eq:com.myntra.wms.entities.location.Carton___active.eq:true", false);
	$weblog->info("Http Client Response is - ".print_r($response, true));
	
	$apiClient = new ApiClient('binServiceUrl', 'getToOutsideBulk', '');
    $response = $apiClient->getDataForAction(array('binBarcodes'=>'BAINST-01-A-03___BAEXLO-01-A-01___BAINST-01-A-04'));
    $weblog->info("Http Client Response is - ". print_r($response, true));
    
    $apiClient = new ApiClient('locationTrackerServiceUrl', 'cartoncheckout', 'sanjay.yadav');
	$request = array();
	$request["checkOutside"] = true;
	$request['cartonBarcodes'] = 'C000000009';
	$request['sourceBinBarcode'] = 'BAEXLO-01-A-01';
	$request['sourceBinConfirmationCode'] = '4B0F';
	$request['userId'] = 1;
	//$xmlString = SimpleParser::arrayToXml($request, "movementEntry");
	$response = $apiClient->postXmlRequest("movementEntry", $request);
	$weblog->info("Http Client Response is - ". print_r($response, true));
	
   	$apiClient = new ApiClient('skuServiceUrl', '', 'sku');
    $apiClient->update(18845, 'sku', array('quantitySold'=>2));
    
    $invcountreq = array();
    $invcountreq['data'] = array();
    $invcountreq['data'][] = array('invCount'=>array('quality'=>'Q1', 'skuId'=>18492));
    $invcountreq['data'][] = array('invCount'=>array('quality'=>'Q1', 'skuId'=>18493));
    
    $apiClient = new ApiClient('skuServiceUrl', 'get_avaliable_count', 'invCount');
    $apiClient->postXmlRequest('invCountResponse', $invcountreq);
    
    $invcountreq = array();
    $invcountreq['data'] = array('invCount'=>array('column'=>'inv_count', 'count'=>1, 'operation'=>'increment', 'quality'=>'Q1', 'skuId'=>18845, 'warehouseId'=>1));
    
    $apiClient = new ApiClient('skuServiceUrl', 'change_count', '');
    $apiClient->putXmlRequest('invCountResponse', $invcountreq);
    
    
    $apiClient = new ApiClient('binServiceUrl', 'getToOutsideBulk', '');
    $response = $apiClient->getDataForAction(array('binBarcodes'=>'BAINST-01-A-03___BAEXLO-01-A-01___BAINST-01-A-04'));
    $weblog->info("Http Client Response is - ". print_r($response, true));
   

    $response = SkuApiClient::getSkuDetailsWithInvCountsForCodes('UCFBTSHT18476');
    $weblog->info("Http Client Response is - ". print_r($response, true));
    
    $response = SkuApiClient::getSkuDetailsWithInvCountsForIds(26077);
    $weblog->info("Http Client Response is - ". print_r($response, true));
    */
    
    MinacsApiClient::pushOnHoldOrder(4983177, 'Suspicious address length issues');
    
?>
