<?php
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");
include_once($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");
include_once("$xcart_dir/modules/apiclient/ApiClient.php");

class SkuApiClient {

	public static function getSkuServiceApiClient($action, $key, $username='') {
		return new ApiClient('skuServiceUrl', $action, $key, $username);
	}
	
	private static function getAvailableInvCount($skuIds, $user='System') {
		$apiClient = SkuApiClient::getSkuServiceApiClient('get_avaliable_count', 'invCount', $user);
		$warehouse_ids = explode(",",trim(WidgetKeyValuePairs::getWidgetValueForKey('warehouse.ids.enabled')));
		$batchSize = FeatureGateKeyValuePairs::getInteger("wmscall.skubatch.threshold", 500);
		if(empty($batchSize))
			$batchSize = 500;
		$requests = array();
		$noOfSkus = 0;
		$invcountreq = array('data' => array());
		foreach ($skuIds as $skuId) {
			if($skuId && !empty($skuId) ){
				if($warehouse_ids && !empty($warehouse_ids)) {
					foreach($warehouse_ids as $warehouseId) {
						$invcountreq['data'][] = array('invCount'=>array('quality'=>'Q1', 'skuId'=>$skuId, 'warehouseId'=>$warehouseId));
					}
				} else {
					$invcountreq['data'][] = array('invCount'=>array('quality'=>'Q1', 'skuId'=>$skuId));
				}
				$noOfSkus++;
			}
			
			if($noOfSkus == $batchSize) {
				$requests[] = $invcountreq;
				$noOfSkus = 0;
				$invcountreq = array('data' => array());
			}
		}
		if($noOfSkus > 0) 
			$requests[] = $invcountreq;
			
		$response_data = array();
		foreach($requests as $request) {
        	$apiClient->postXmlRequest('invCountResponse', $request);
            if($apiClient->isSuccess()) {
	            $response_data = array_merge($response_data, $apiClient->getData());
            }
        }
        return $response_data;
	} 
	
	public static function getAvailableInvCountMap($skuIds, $groupbyKey='skuId', $user='System') {
		$availableCountMap = array();
		
		$jitSourcingEnabled = FeatureGateKeyValuePairs::getBoolean("jit.sourcing.enabled", false);
		if(!$jitSourcingEnabled) {
			$response_data = SkuApiClient::getAvailableInvCount($skuIds, $user);
			foreach($response_data as $row) {
	          	if(!isset($availableCountMap[$row[$groupbyKey]]))
					$availableCountMap[$row[$groupbyKey]] = 0;
					
				$row['count'] = $row['count']>0?$row['count']:0;
				
				$availableCountMap[$row[$groupbyKey]] += $row['count'];
        		}	
		} else {
			$response_data = SkuApiClient::getSkuDetailsWithInvCountsForIds($skuIds, $user);
			if($groupbyKey == 'skuId') $groupbyKey = 'id';
			foreach($response_data as $row) {
          		if(!isset($availableCountMap[$row[$groupbyKey]]))
					$availableCountMap[$row[$groupbyKey]] = 0;
				
				if($row['jitSourced'] == 1)
					$availableCountMap[$row[$groupbyKey]] = 99;
				else
					$availableCountMap[$row[$groupbyKey]] += $row['invCount']>0?$row['invCount']:0;
			}
		}
		
		return $availableCountMap;
	}
	
	public static function updateSkuBlockedOrderCount($skus, $increase, $user, $warehouseId) {
		global $weblog;
		
		$myntraOwnedWarehouses = FeatureGateKeyValuePairs::getFeatureGateValueForKey('wms.myntra.warehouses');
		$myntraOwnedWarehouses = explode(",", $myntraOwnedWarehouses);
		$myntraItem = true;
		if(!in_array($warehouseId, $myntraOwnedWarehouses))
			return true;
			
		$invcountreq['data'] = array();
		foreach($skus as $id=>$quantity) {
			if($increase){
	    		$invcountreq['data'][] = array('invCount' => array('column'=>'blocked_order_count', 'count'=>$quantity, 'operation'=>'increment', 'quality'=>'Q1', 'skuId'=>$id, 'warehouseId'=>$warehouseId));
			} else {
				$invcountreq['data'][] = array('invCount' => array('column'=>'blocked_order_count', 'count'=>$quantity, 'operation'=>'decrement', 'quality'=>'Q1', 'skuId'=>$id, 'warehouseId'=>$warehouseId));
			}
		}
		
		$apiClient = SkuApiClient::getSkuServiceApiClient('change_count', '', $user);
	    $apiClient->putXmlRequest('invCountResponse', $invcountreq);
	    if($apiClient->isSuccess()) {
	    	$weblog->info("Successfully Updated blocked order count for skus - ".print_r($skus, true));
	    	return true;
	    } else {
	    	$weblog->info("Failed to update blocked order count for sku - ".print_r($skus, true));
	    	return false;
	    }
	}
	
	public static function moveSkuBlockedOrderCount($skuId, $count, $fromWhId, $toWhId, $user='') {
		
		global $weblog;
		$invcountreq['data'] = array();
	    $invcountreq['data'][] = array('invCount' => array('column'=>'blocked_order_count', 'count'=>$count, 'operation'=>'decrement', 'quality'=>'Q1', 'skuId'=>$skuId, 'warehouseId'=>$fromWhId));
		$invcountreq['data'][] = array('invCount' => array('column'=>'blocked_order_count', 'count'=>$count, 'operation'=>'increment', 'quality'=>'Q1', 'skuId'=>$skuId, 'warehouseId'=>$toWhId));
		
		$apiClient = SkuApiClient::getSkuServiceApiClient('change_count', '', $user);
	    $apiClient->putXmlRequest('invCountResponse', $invcountreq);
	    if($apiClient->isSuccess()) {
	    	$weblog->info("Successfully Moved blocked order count for sku - $skuId from $fromWhId to $toWhId warehouse");
	    	return true;
	    } else {
	    	$weblog->info("Failed to Moved blocked order count for sku - $skuId from $fromWhId to $toWhId warehouse");
	    	return false;
	    }
	}
	
	public static function getSkuDetails($skuIds){
	    $skus = array();
		if(empty($skuIds)) {
			return $skus;
		}
		
		if(!is_array($skuIds))
			$skuIds = array($skuIds);
		
		$apiClient = SkuApiClient::getSkuServiceApiClient('','sku');
		
		$req2skuIds = array();
		$index = 0;
		foreach ($skuIds as $id) {
 			if(count($req2skuIds[$index]) < 200) {
				$req2skuIds[$index][] = $id;
			} else {
				$index++;
				$req2skucodes[$index][] = $id;
			}
 		}
		
		foreach ($req2skuIds as $sku_ids) {
 			$apiClient->search("id.in:".implode(",",$sku_ids), array('limit' => 200));
			if($apiClient->isSuccess()) {
				$fetchedskus = $apiClient->getData();
				$skus = array_merge($skus, $fetchedskus);
			}
		}
		
	    return $skus;
	}

	public static function getSkuDetailsForSkuCode($skucodes){
            if(!empty($skucodes)) {
		if(is_array($skucodes)){
			$apiClient = SkuApiClient::getSkuServiceApiClient('','sku');
			$apiClient->search("code.in:".implode(",",$skucodes), array('limit' => -1));
		}else{
			$apiClient = SkuApiClient::getSkuServiceApiClient('','sku');
			$apiClient->search("code.eq:".$skucodes);
		}
		if($apiClient->isSuccess()){
			return $apiClient->getData();
		}
            }
	}
	
	public static function updateSku($skuId,$params){
	    if(!empty($skuId)) {
		$apiClient = SkuApiClient::getSkuServiceApiClient('update', 'sku');
		$apiClient->update($skuId, 'sku', $params);
		if($apiClient->isSuccess()){
			return $apiClient->getData();
		}		
	    }
	    return false;
        }
	
	public static function getSkuWithCodeStartingBy($str){
		if(!empty($str)) {
		    $apiClient = SkuApiClient::getSkuServiceApiClient('', 'sku');
		    $apiClient->search("code.like:$str", array('limit' => -1));
		    if($apiClient->isSuccess()){
			return $apiClient->getData();
		    }
                }
                return array();		
	}
	
	public static function getSkuDetailsWithInvCountsForIds($skuIds){
		if(!empty($skuIds)) {
		    if(!is_array($skuIds))
			$skuIds = array($skuIds);
			
			$warehouse_ids = explode(",",trim(WidgetKeyValuePairs::getWidgetValueForKey('warehouse.ids.enabled')));
			
		    $apiClient = SkuApiClient::getSkuServiceApiClient('get_sku_details','skuDetails');
		    $batchSize = FeatureGateKeyValuePairs::getInteger("wmscall.skubatch.threshold", 500);
		    if(empty($batchSize))
		    	$batchSize = 500;
		    $skuDetailsReq = array('data' => array());
		    $noOfSkus = 0;
		    $requests = array();
		    foreach ($skuIds as $skuId) {
				if($skuId && !empty($skuId) ){
					if($warehouse_ids && !empty($warehouse_ids)) {
						foreach($warehouse_ids as $warehouseId) {
							$skuDetailsReq['data'][] = array('skuDetails'=>array('quality'=>'Q1', 'id'=>$skuId, 'warehouseId'=>$warehouseId));
						}
					} else {
						$skuDetailsReq['data'][] = array('skuDetails'=>array('quality'=>'Q1', 'id'=>$skuId));
					}
					$noOfSkus++;
				}
				
				if($noOfSkus == $batchSize) {
					$requests[] = $skuDetailsReq;
					$noOfSkus = 0;
					$skuDetailsReq = array('data' => array());
				}
		    }
		    
		    if($noOfSkus > 0)
		    	$requests[] = $skuDetailsReq;
		    	
		    $response_data = array();
		    foreach($requests as $request) {
		    	$profileHandler = Profiler::startTiming("wms-sku-detail-call");
		    	$apiClient->postXmlRequest('skuDetailsResponse', $request);
		    	if($apiClient->isSuccess()) {
		    		$response_data = array_merge($response_data, $apiClient->getData());
		    	}
		    	Profiler::endTiming($profileHandler);
		    }
		    
		    return $response_data;
		}
		return array();
	}
	
	public static function getSkuDetailsInvCountMap($skuIds) {
		$availableCountMap = array();
		$response_data = SkuApiClient::getSkuDetailsWithInvCountsForIds($skuIds);
		
		foreach($response_data as $row) {
			$skuId2WarehouseCountsRows[$row['id']][] = $row;
		}
		
		$skuId2DetailsMap = array(); 		
 		foreach(array_keys($skuId2WarehouseCountsRows) as $skuId) {
 			$skuRow =  $skuId2WarehouseCountsRows[$skuId][0];
 			
 			$sku = array('id'=>$skuId, 'code'=>$skuRow['code'], 'jitSourced'=>$skuRow['jitSourced']);
 			$jitSourcingEnabled = FeatureGateKeyValuePairs::getBoolean("jit.sourcing.enabled", false);
 			if(!$jitSourcingEnabled)
 				$sku['jitSourced'] = false;
 			
			foreach($skuId2WarehouseCountsRows[$skuId] as $row) {
				$row['invCount'] = $row['invCount']>0?$row['invCount']:0;
	 			if(!isset($sku['availableItems']))
	 				$sku['availableItems'] = 0;
	 				
	 			$sku['availableItems'] += $row['invCount'];
	 			if(!isset($sku['warehouse2AvailableItems']))
	 				$sku['warehouse2AvailableItems'] = array();
	 			if($row['invCount'] > 0)
	 				$sku['warehouse2AvailableItems'][$row['warehouseId']] = $row['invCount'];
			}
			$sku['availableCount'] = $sku['availableItems'];
			$skuId2DetailsMap[$skuId] = $sku;			
 		}
		
		return $skuId2DetailsMap;
	}
	
	public static function getSkuDetailsWithInvCountsForCodes($skuCodes){
	    if(!is_array($skuCodes))
			$skuCodes = array($skuCodes);
			
	    $apiClient = SkuApiClient::getSkuServiceApiClient('get_sku_details','skuDetails');
	    $skuDetailsReq = array('data' => array());
	    
	    $warehouse_ids = explode(",",trim(WidgetKeyValuePairs::getWidgetValueForKey('warehouse.ids.enabled')));
	    $batchSize = FeatureGateKeyValuePairs::getInteger("wmscall.skubatch.threshold", 500);
	    if(empty($batchSize))
	    	$batchSize = 500;
	    $noOfSkus = 0;
	    $requests = array();
	    foreach ($skuCodes as $skuCode) {
			if($skuCode && !empty($skuCode) ){
				if($warehouse_ids && !empty($warehouse_ids)) {
					foreach($warehouse_ids as $warehouseId) {
						$skuDetailsReq['data'][] = array('skuDetails'=>array('quality'=>'Q1', 'code'=>$skuCode, 'warehouseId'=>$warehouseId));
					}
				} else {
					$skuDetailsReq['data'][] = array('skuDetails'=>array('quality'=>'Q1', 'code'=>$skuCode));
				}
				$skuDetailsReq['data'][] = array('skuDetails'=>array('quality'=>'Q1', 'code'=>$skuCode));
				$noOfSkus++;
			}
			
			if($noOfSkus == $batchSize) {
				$requests[] = $skuDetailsReq;
				$noOfSkus = 0;
				$skuDetailsReq = array('data' => array());
			}
	    }
	    
	    $apiClient->postXmlRequest('skuDetailsResponse', $skuDetailsReq);
	
	    if($apiClient->isSuccess()){
			return $apiClient->getData();
	    }
		return array();
	}	
}

?>
