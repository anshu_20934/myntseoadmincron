<?php
    include_once("$xcart_dir/utils/Helper.php");
    include_once("$xcart_dir/utils/http/HttpClient.class.php");
    include_once("$xcart_dir/utils/http/SimpleParser.php");
    include_once("$xcart_dir/include/func/func.db.php");
    include_once("$xcart_dir/include/class/class.mail.multiprovidermail.php");

/**
 * Created by PhpStorm.
 * User: arpit.jain
 * Date: 26/10/15
 * Time: 4:19 PM
 */
class PpsResponseHandler {

    private $apiServiceUrl;
    private $apiAccessor;
    private $dataKey;
    private $action;
    private $success;
    private $errorMsg;
    private $data;
    private $requestUri;
    private $postData;
    private $request_method = "GET";
    private $totalCount=0;
    private $statusCode;

    public function __construct($serviceType, $action, $datakey, $audituser) {
        $serviceUrl = Helper::getPropertyValue($serviceType);
        $this->action = $action;
        $this->apiAccessor = (empty($audituser)?'System':$audituser);
        $this->apiServiceUrl = $serviceUrl;
        $this->dataKey = $datakey;
        $this->success = false;
        $this->request_method = "GET";
        $this->postData = false;
    }

    public function isSuccess() {
        return $this->success;
    }

    public function getData() {
        return $this->data;
    }

    public function getErrorMsg() {
        return $this->errorMsg;
    }

    public function getTotalCount(){
        return $this->totalCount;
    }
    public function getStatusCode(){
        return $this->statusCode;
    }

    private function extractDataFromResponse($response) {
        $decodedResponse = json_decode($response, true);
        if($decodedResponse['errorCode'] !== null) {
            $this->statusCode = 500;
            $this->success = false;
            $this->errorMsg = "Exchange Creation (PPS) Failed: ".$decodedResponse['errorReason'];
            $this->data = array();
        } else{
            if($decodedResponse['ppsId'] !== null && $decodedResponse['exchangeOrderId'] != null) {
                if($decodedResponse['success'] == true) {
                    $this->statusCode = '200';
                    $this->success = true;
                    $this->data = $decodedResponse['exchangeOrderId'];
                } else {
                    $this->statusCode = '500';
                    $this->success = false;
                    $this->errorMsg = 'Exchange already exists';
                    $this->data = array();
                }
            } else {
                $this->statusCode = 500;
                $this->success = false;
                if($decodedResponse['success'] !== true){
                    $this->errorMsg = 'PPS api response success is FALSE';
                } else if($response['ppsId'] === null){
                    $this->errorMsg = 'PPS api response ppsId is NULL';
                } else{
                    $this->errorMsg = 'PPS api response error';
                }
                $this->data = array();
            }
        }
    }

    public function post($data=false, $contentType = "application/json") {
        $this->requestUri = $this->apiServiceUrl .$this->action;
        $response_string = HttpClient::quickPost($this->requestUri, $data , $this->apiAccessor, $contentType);
        $this->extractDataFromResponse($response_string);
    }
}