<?php

include_once("$xcart_dir/modules/apiclient/ApiClient.php");
include_once("$xcart_dir/include/class/oms/EventCreationManager.php");
require_once("$xcart_dir/Profiler/Profiler.php");
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";

class ReleaseApiClient {

	public static function getReleaseApiClient($action, $key, $username='') {
		return new ApiClient('wmsReleaseUrl', $action, $key, $username);
	}
	
	public static function getWarehouseReleaseApiClient($action, $key, $username='') {
		return new ApiClient('orchWMSIntegrationUrl', $action, $key, $username);
	}
	
	/*public static function pushOrderReleaseToWMSOld($releaseIds, $user='System', $action='') {
		
		$pushToWMS = explode(",", FeatureGateKeyValuePairs::getFeatureGateValueForKey('orderreleases.wmspush.enable.warehouseids'));
		
		$pushToUnicomWMS = explode(",", FeatureGateKeyValuePairs::getFeatureGateValueForKey('orderreleases.unicompush.enable.warehouseids'));
		
		global $weblog;
		if($action == ''){
			$apiClient = ReleaseApiClient::getReleaseApiClient('bulkCreate', 'orderReleaseResponse', $user);
		}else{
			$apiClient = ReleaseApiClient::getReleaseApiClient('bulkUpdate', 'orderReleaseResponse', $user);
		}
		
		if(!is_array($releaseIds))
			$releaseIds = array($releaseIds);
		
		$orderReleaseResponse = array('data' => array());
		$handle = Profiler::startTiming("ReleaseApiClient-getReleaseApiClient");
		foreach($releaseIds as $orderid){
			$orderRelease = func_query_first("select orderid, group_id, queueddate, status, warehouseid, gift_status, shipping_method from xcart_orders where orderid=".$orderid);
			if(!$orderRelease || !in_array($orderRelease['warehouseid'], $pushToWMS)) {
               	continue;
            }
            $handle = Profiler::startTiming("ReleaseApiClient-getReleaseApiClient");
			$isOnHold = $orderRelease['status'] == "OH" ? 1 : 0;
			
			switch ($orderRelease['status']){
				case 'WP':
				case 'OH':
				case 'Q':
				case 'RFR':
					$orderReleaseStatus = 'NEW';
					break;
				case 'D':
				case 'F':
					$orderReleaseStatus = 'CANCELLED';
					break;
				case 'DL':
				case 'C':
				case 'PK':
				case 'SH':
					$orderReleaseStatus = 'COMPLETED';
					break;
				default:
					$orderReleaseStatus = 'NEW';
					$isOnHold = 1;
			}
				
			$orderReleaseLines = array();
			$orderReleaseDetails = func_query("select sku_id, sum(case when item_status = 'IC' then 0 else amount end) as quantity, item_status, od.product_style, sp.global_attr_article_type, sp.global_attr_sub_category from xcart_order_details od, mk_order_item_option_quantity oq, 
					mk_styles_options_skus_mapping som, mk_style_properties sp where od.itemid = oq.itemid and oq.optionid = som.option_id and sp.style_id = od.product_style and od.orderid=".$orderid." group by sku_id");
			// isFragile - to be feature-gated
			$fragileItems = explode(',', trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('fragile.article.types')));
	
			$weblog->info(print_r($orderReleaseDetails, true));
			if($orderReleaseDetails) {
				foreach($orderReleaseDetails as $index => $releaseDetails){
					$qty = $action=='cancelrelease'?0: $releaseDetails['quantity'];
					$orderReleaseLines[] = array('orderReleaseLines'=>array('quantity' => $qty, 
							'sku' => array('id' => $releaseDetails['sku_id']),
							'isFragile'=>(in_array($releaseDetails['global_attr_sub_category'], $fragileItems) 
									|| in_array($releaseDetails['global_attr_article_type'], $fragileItems)) ?1:0));
				}
			}
		    $cutOffTime = func_query_first_cell("select value from order_additional_info where order_id_fk = $orderid and `key` = 'EXPECTED_PICKING_TIME'"); 
			$orderReleaseXML = array('orderDate' => date("Y-m-d\\TH:i:s", $orderRelease['queueddate']), 'portalOrderReleaseId' => $orderRelease['orderid'],
					'orderId' => $orderRelease['group_id'], $orderReleaseLines, 'priority' => '1', 'isGift' => ($orderRelease['gift_status']=='Y'?true:false), 
					'isOnHold' => $isOnHold, 'warehouse' => array('id' => $orderRelease['warehouseid']), 'cutOffTime' => $cutOffTime?$cutOffTime:date("Y-m-d\\TH:i:s", time()), 'isExpress' => ($orderRelease['shipping_method'] == 'XPRESS'?true:false));
			if($orderReleaseStatus != ''){
				$orderReleaseXML['orderReleaseStatus'] = $orderReleaseStatus;
			}
			$orderReleaseResponse['data'][] = array('orderRelease' => $orderReleaseXML);
			
		}
		
		if(empty($orderReleaseResponse['data'])){
			Profiler::endTiming($handle);
			return;
		} else {
			if($action == ''){
				$apiClient->postXmlRequest('orderReleaseResponse', $orderReleaseResponse);
			}else{
				$apiClient->putXmlRequest('orderReleaseResponse', $orderReleaseResponse);
			}
			Profiler::endTiming($handle);
			if($apiClient->isSuccess()) {
				return true;
			} else 
				return $apiClient->getErrorMsg();
		}
	}	*/
	
	public static function pushOrderReleaseToWMS($releaseIds, $user='System', $action='') {
		global $weblog;
		if(!is_array($releaseIds))
			$releaseIds = array($releaseIds);
		
		$releases = OrderModelTransformer::createNewOrderReleaseModel($releaseIds);
		
		$myntraOwnedWarehouses = FeatureGateKeyValuePairs::getFeatureGateValueForKey('wms.myntra.warehouses');
		$myntraOwnedWarehouses = explode(",", $myntraOwnedWarehouses);
		
		$myntraReleasesRequest = array();
		$unicomReleasesRequest = array();
		
		foreach($releases as $release) {
			if($release['warehouseId']) {
				if(in_array($release['warehouseId'], $myntraOwnedWarehouses)) {
					$myntraReleasesRequest['data'][] = array('orderReleaseEntry' => $release);
				} else {
					$unicomReleasesRequest['data'][] = array('orderReleaseEntry' => $release);
				}
			}
		}
		
		if( (isset($myntraReleasesRequest['data']) && count($myntraReleasesRequest['data']) > 0) 
			|| 	(isset($unicomReleasesRequest['data']) && count($unicomReleasesRequest['data']) > 0)	) {
			$errorMsg = "";
			
			if(count($myntraReleasesRequest['data']) > 0) {
				/*$myntraWmsHandle = Profiler::startTiming("ReleaseApiClient-pushToMyntraWMS");
				if($action == ''){
					$apiClient = ReleaseApiClient::getWarehouseReleaseApiClient('bulkCreateOrderRelease', 'orderReleaseResponse', $user);
					$apiClient->postXmlRequest('orderReleaseResponse', $myntraReleasesRequest);
				}else{
					$apiClient = ReleaseApiClient::getWarehouseReleaseApiClient('bulkUpdateOrderRelease', 'orderReleaseResponse', $user);
					$apiClient->putXmlRequest('orderReleaseResponse', $myntraReleasesRequest);
				}
				Profiler::endTiming($myntraWmsHandle);
				if(!$apiClient->isSuccess()) { 
					$errorMsg .= $apiClient->getErrorMsg();
				}*/
				EventCreationManager::pushWMSReleaseEvent($myntraReleasesRequest, $action);
			}
			
			if(count($unicomReleasesRequest['data']) > 0) {
				/*$unicomWmsHandle = Profiler::startTiming("ReleaseApiClient-pushToUnicomWMS");
				if($action == ''){
					$apiClient = ReleaseApiClient::getWarehouseReleaseApiClient('bulkCreateOrderRelease', 'orderReleaseResponse', $user);
					$apiClient->postXmlRequest('orderReleaseResponse', $unicomReleasesRequest);
				}else{
					$apiClient = ReleaseApiClient::getWarehouseReleaseApiClient('bulkUpdateOrderRelease', 'orderReleaseResponse', $user);
					$apiClient->putXmlRequest('orderReleaseResponse', $unicomReleasesRequest);
				}
				Profiler::endTiming($unicomWmsHandle);
				if(!$apiClient->isSuccess()) {
					$error .= $apiClient->getErrorMsg();
				}*/
				EventCreationManager::pushWMSReleaseEvent($unicomReleasesRequest, $action);
			}
			
			if($errorMsg == "")
				return true;
			else 
				return $errorMsg;
		} else {
			return "Invalid releases - ". implode(",", $releaseIds);
		}
	}


	public static function pushOrderReleaseToUnicom($releaseIds, $user='System', $action='') {
		global $weblog;
		if(!is_array($releaseIds))
			$releaseIds = array($releaseIds);

		$releases = OrderModelTransformer::createNewOrderReleaseModel($releaseIds);

		$myntraOwnedWarehouses = FeatureGateKeyValuePairs::getFeatureGateValueForKey('wms.myntra.warehouses');
		$myntraOwnedWarehouses = explode(",", $myntraOwnedWarehouses);

		$unicomReleasesRequest = array();

		foreach($releases as $release) {
			if($release['warehouseId']) {
				if(!in_array($release['warehouseId'], $myntraOwnedWarehouses)) {
					$unicomReleasesRequest['data'][] = array('orderReleaseEntry' => $release);
				}
			}
		}

		if( isset($unicomReleasesRequest['data']) && count($unicomReleasesRequest['data']) > 0) {
			$errorMsg = "";

			if(count($unicomReleasesRequest['data']) > 0) {
				EventCreationManager::pushWMSReleaseEvent($unicomReleasesRequest, $action);
			}

			if($errorMsg == "")
				return true;
			else
				return $errorMsg;
		} else {
			return "Invalid releases - ". implode(",", $releaseIds);
		}
	}
	
}



