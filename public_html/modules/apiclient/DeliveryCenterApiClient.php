<?php
include_once("$xcart_dir/modules/apiclient/ApiClient.php");
class DeliveryCenterApiClient {
  	private static function getDeliveryCenterApiClient($action, $key, $username='') {
        return new ApiClient('deliveryCenterServiceUrl', $action, $key, $username);
    }
	public static function getDeliveryCenter($dcId) {
		global $errorlog;
        $apiClient = DeliveryCenterApiClient::getDeliveryCenterApiClient('' . $dcId, 'deliveryCenter');
        $apiClient->getDataForAction();
        $trip = false;
        $resp = array();
        if ($apiClient->isSuccess()) {
            $response = $apiClient->getData();
            $dc = $response[0];
            $resp['status']='success';
            $resp['data']=$dc;
        } else {
        	$msg = "Get delivery center for id =$dcId failed. Error Msg: " . $apiClient->getErrorMsg();
            $errorlog->error($msg);
            $resp['status']='failure';
            $resp['msg']=$msg;
        }
        return $resp;
    }
    
    public static function getDeliveryCenterID($dcCode) {
    	global $errorlog;
    	$apiClient = DeliveryCenterApiClient::getDeliveryCenterApiClient('', 'deliveryCenter');
    	if(!is_array($dcCode)){
    		$dcCode = array($dcCode);
    	}
    	$xcache = new XCache();
    	$dcid = $xcache->fetch($dcCode."_id");
    	$resp = array();
    	if($dcid == ''){
	    	$apiClient->search("code.in:".implode(",", $dcCode));
	    	if ($apiClient->isSuccess()) {
	    		$response = $apiClient->getData();
	    		$dcid = $response[0];
	    		$xcache->store($dcCode."_id", $dcid, 3600);
	    		$resp['status']='success';
	    		$resp['data']=$dcid;
	    	} else {
	    		$msg = "Get delivery center id failed. Error Msg: " . $apiClient->getErrorMsg();
	    		$errorlog->error($msg);
	    		$resp['status']='failure';
	    		$resp['msg']=$msg;
	    	}
    	} else {
    		$resp['status']='success';
    		$resp['data']=$dcid;
    	}
    	
    	return $resp;
    }
}
?>