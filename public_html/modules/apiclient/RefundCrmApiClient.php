<?php
include_once("$xcart_dir/modules/apiclient/ApiClient.php");

class RefundCrmApiClient {

	public static function getRefundServiceApiClient($action, $key, $username='') {
		return new ApiClient('crmRightnowServiceUrl', $action, $key=false, $username);
	}
	
	// TODO added this function for testing purpose
	public static function generateRefundEntryObject(){
		$refundEntryObj = array(
				"login"=>"arun.kumar@myntra.com",
				"orderId"=>"17927372",//17927372
				"bankRefundAmount"=>1.00,
				"refundMode"=>"OR",
				"refundPaymentAccountId"=>435667,
				"returnId"=>78789
				);
		return $refundEntryObj;
	}
	
	// TODO added this function for testing purpose
	public static function generateNeftEntryObject(){
		$neftEntryObj = array(
				"login"=>"arun.kumar@myntra.com",
				"bankName"=>"citi",
				"branch"=>"bangalore",
				"accountType"=>"SAVINGS",
				"accountNumber"=>"9988777889",
				"accountName"=>"Arun Kumar K",
				"ifscCode"=>"citi789799"
		);		
		
		return $neftEntryObj;
	}
	
	public static function generateRefundForReturn($refundEntryObj) {
		global $weblog;			
				
		$apiClient = RefundCrmApiClient::getRefundServiceApiClient('task/refund');
		$apiClient->postXmlRequest('refundEntry', $refundEntryObj);
		
		//return $apiClient;
		
		$status=true;
		if(!$apiClient->isSuccess()) {
			$status = $apiClient->getErrorMsg();
		}
		$weblog->debug("posted refund entry object to crm refund service: " . $status); 
		return $status;
	}

	public static function saveNEFTBankAccount($bankAccountObj) {
		global $weblog;
	
		$apiClient = RefundCrmApiClient::getRefundServiceApiClient('task/bank_account/save');
		$apiClient->postXmlRequest('neftAccountEntry', $bankAccountObj);		
		
		return $apiClient;
	}
	
	public static function getNEFTBankAccount($login, $neftAccountId=null) {
		global $weblog;
		
		$query = array("login"=>$login);
		
		if(!empty($neftAccountId)){
			$query += array("neftAccountId"=>$neftAccountId);
		}
		
		// build urlencoded query string
		$querystring = '';
		foreach($query as $key=>$value) {
			$querystring .= urlencode($key).'='.urlencode($value).'&';
		}
		// Eliminate unnecessary & */
		$querystring = substr($querystring, 0, -1);
	
		$apiClient = RefundCrmApiClient::getRefundServiceApiClient('task/bank_account/detail');
		$apiClient->get($querystring);		
	
		return $apiClient;
	}
	
}

?>
