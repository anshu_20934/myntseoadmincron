<?php

include_once("$xcart_dir/modules/apiclient/ApiClient.php");
include_once($xcart_dir."/Cache/Cache.php");

class ItemApiClientOld {

	private static function getItemServiceApiClient($action, $username='') {
		return new ApiClient('itemServiceUrl', $action, 'item', $username);
	}
	
	public static function getRejectedItemServiceApiClient($action, $username='') {
		return new ApiClient('itemRejectReasonServiceUrl', $action, 'rrEntry', $username);
	}
	
	public static function getOrchItemServiceApiClient($action, $username='') {
		return new ApiClient('', $action, 'item', $username);
	}
	
	public static function getItemQualityCheckCodes($stage=false) {
		$cache = new \Cache("RejectReasons", "rejectReasons-$stage");
		$params = array($stage);
		
		$qualityCheckCodes = $cache->get(function($stage){
			$apiClient = ItemApiClientOld::getRejectedItemServiceApiClient('');
			$query = false;
			if($stage)
				$query = "rejectedAt.eq:$stage";
			
			$apiClient->search($query);
			$qualityCheckCodes = array(); 
			if($apiClient->isSuccess()) {
				$data = $apiClient->getData();
				foreach($data as $row) {
					$qualityCheckCodes[$row['id']] = array( 'id'=> $row['id'], 'quality' => $row['quality'], 'rejectReason' => $row['rejectReason'], 'rejectReasonDescription' => $row['rejectReasonDescription']); 
				}
			}
			return $qualityCheckCodes;
		}, $params, "rejectreasons-$stage", 24*60);
		
		return $qualityCheckCodes;
	}

	public function get_skuitems_for_order($orderid, $warehouseId=false) {
		if($orderid != '' && $orderid != null){
			$apiClient = ItemApiClientOld::getItemServiceApiClient('');
			$orderid = trim($orderid);
			if(!$warehouseId) {
				$order = func_query_first("select warehouseid from xcart_orders where orderid = $orderid", true);
				$warehouseId = $order['warehouseid'];
			}
			$apiClient->search("orderId.eq:$orderid");
			if($apiClient->isSuccess())
				return $apiClient->getData();
		}
		return false;
	}
	
	public static function get_items_for_barcodes($warehouseId, $itemBarCodes) {
		$apiClient = ItemApiClientOld::getItemServiceApiClient('');
		if(!is_array($itemBarCodes)) {
			$apiClient->search("barcode.eq:".$itemBarCodes);
		} else {
			$itemBarcodesQuery = "";
			foreach ($itemBarCodes as $barcode){
				$itemBarcodesQuery .= $itemBarcodesQuery==""?"":",";
				$itemBarcodesQuery .= trim($barcode);
			}
			$apiClient->search("barcode.in:$itemBarcodesQuery");
		}
		if($apiClient->isSuccess()) 
			return $apiClient->getData();
		else
			return array();
	}
	
	public static function get_itemids_for_barcodes($warehouseId, $itemBarCodes, $mapping=true) {
		$items = ItemApiClientOld::get_items_for_barcodes($warehouseId, $itemBarCodes);
		$results = array();
		if($mapping) {
			foreach($items as $row) {
				$results["".$row['barcode']] = $row['id'];
			}
		} else {
			foreach($items as $row) {
				if(!in_array($row['id'], $results))
				$results[] = $row['id'];
			}
		}
		return $results;
	}
	
	/* public static function update_not_found_items($scannedItemBarCodes, $login){
		global $weblog;
		$update_data = array('orderId'=>0, 'itemStatus'=>'NOT_FOUND');
		$success = ItemApiClientOld::update_items_in_bulk($scannedItemBarCodes, $update_data, $login);
		if($success) {
			$weblog->info("return_request.php: Wrong item received. Update sent to WMS");
			return array("success" => true, "message" => "marked item status to 'Not-Found'");
		} else {
			$weblog->info("return_request.php: Wrong item received. could not update the WMS");
			return array("success" => false, "message" => "Failed to update item status to 'Not-Found'");
		}
	} */

	public static function update_items_in_bulk($warehouseId, $itemIds, $update_data, $user) {
		global $xcart_dir;
		$apiClient = ItemApiClientOld::getItemServiceApiClient('bulkupdate', $user);

		$itemUpdateReq = array('data' => array());
		foreach ($itemIds as $itemid) {
			$iteminfo = array('id'=>$itemid);
			foreach ($update_data as $key=>$value) {
				$iteminfo[$key] = $value;
			}
			$itemUpdateReq['data'][] = array('item'=>$iteminfo);
		}

		$apiClient->putXmlRequest('itemResponse', $itemUpdateReq);

		if($apiClient->isSuccess()) {
			return true;
		} else {
			return $apiClient->getErrorMsg();
		}
	}

	public static function getSkuWiseItemDetailsForOrder($orderid, $warehouseId) {
		global $weblog;
		$orderid = trim($orderid);
		
		$data = ItemApiClientOld::get_skuitems_for_order($orderid, $warehouseId);

		$sku2ItemsMapping = array();
		if($data) {
			foreach($data as $row) {
				if(!isset($sku2ItemsMapping[$row['sku']['id']]))
				$sku2ItemsMapping[$row['sku']['id']] = array();
					
				$sku2ItemsMapping[$row['sku']['id']][] = $row['barcode'];
			}
		}
		$weblog->info("Sku Items fetched for Order $orderid - ". print_r($sku2ItemsMapping, true));
		return $sku2ItemsMapping;
	}

	public static function get_issued_itembarcodes_for_order($orderid) {
		$item_bar_codes = array();
		$orderid = trim($orderid);
		$apiClient = ItemApiClientOld::getItemServiceApiClient('');
		$apiClient->search("orderId.eq:$orderid");
		if($apiClient->isSuccess()) {
			$data = $apiClient->getData();
			foreach($data as $row) {
				$item_bar_codes[] = $row['barcode'];
			}
		}
		return $item_bar_codes;
	}

	public static function get_returnable_itembarcodes_and_skuids_for_order($orderid) {
		$itembarcode2ItemDetails = array();
		$orderid = trim($orderid);
		$apiClient = ItemApiClientOld::getItemServiceApiClient('');
		$apiClient->search("orderId.eq:$orderid"."___itemStatus.in:ACCEPTED_RETURNS,SHIPPED");
		if($apiClient->isSuccess()) {
			$data = $apiClient->getData();
			foreach($data as $row) {
				$itembarcode2ItemDetails[$row['barcode']] = array('itemStatus'=>$row['itemStatus'],
									'skuId'=>$row['sku']['id'],
									'warehouseId'=>$row['sku']['warehouseId']);
				
			}
		}
		return $itembarcode2ItemDetails;
	}

	public static function get_issued_itemcartoncodes_for_order($orderids) {
		$item_bar_codes = array();
		$orderid = trim($orderid);
		$apiClient = ItemApiClientOld::getItemServiceApiClient('');

		if(is_array($orderids)){
			$apiClient->detailsearch("orderId.in:".implode(",",$orderids));
		}else{
			$apiClient->detailsearch("orderId.eq:".$orderids);
		}

		if($apiClient->isSuccess()) {
			$data = $apiClient->getData();
			foreach($data as $row) {
				$item_bar_codes[] = array('orderid'=> $row['orderId'],'cartonBarcode'=>$row['cartonBarcode']);
			}
		}
		return $item_bar_codes;
	}
	/**
	 *  this is used from rts page. output format is diff from existing api
	 *
	 */
	public static function get_items_for_orders($orderids) {
		$orderId2Items = array();
		$orderIds = array();
		foreach($orderids as $orderid){
			$orderIds[] = trim($orderid);
		}
		if(!empty($orderIds)) {
			$apiClient = ItemApiClientOld::getItemServiceApiClient('');
			$apiClient->search("orderId.in:".implode(",", $orderIds));
			if($apiClient->isSuccess()) {
				$data = $apiClient->getData();
				foreach($data as $row) {
					if(!isset($orderId2Items[$row['orderId']]))
						$orderId2Items[$row['orderId']] = array();
						
					$orderId2Items[$row['orderId']][] = $row;
				}
			}
		}
		return $orderId2Items;
	}
	
	public static function get_issued_items_for_orders($orderids) {
		if(!is_array($orderids))
			$orderids = array($orderids);
		$orderId2Items = array();
		$orderIds = array();
		foreach($orderids as $orderid){
			$orderIds[] = trim($orderid);
		}
		if(!empty($orderIds)) {
			$apiClient = ItemApiClientOld::getItemServiceApiClient('');
			$apiClient->search("orderId.in:".implode(",", $orderIds));
			if($apiClient->isSuccess()) {
				$data = $apiClient->getData();
				foreach($data as $row) {
					if(!isset($orderId2Items[$row['orderId']])) {
						$orderId2Items[$row['orderId']] = array();
					}
					$orderId2Items[$row['orderId']][] = $row;
				}
			}
		}
		return $orderId2Items;
	}

	public static function get_items_locations_for_skucodes($skuCodes, $warehouseId) {
		$apiClient = ItemApiClientOld::getItemServiceApiClient('');
		 
		$req2skucodes = array();
		$index = 0;
		foreach ($skuCodes as $skucode) {
			if(count($req2skucodes[$index]) < 300) {
				$req2skucodes[$index][] = $skucode;
			} else {
				$index++;
				$req2skucodes[$index][] = $skucode;
			}
		}
			
		$items = array();
		foreach ($req2skucodes as $skucodes) {
			$apiClient->detailsearch("sku.code.in:".implode(",", $skucodes)."___itemStatus.eq:STORED___quality.eq:Q1___warehouseId.eq:$warehouseId", false);
			if($apiClient->isSuccess()) {
				$fetcheditems = $apiClient->getData();
				$items = array_merge($items, $fetcheditems);
			}
		}
			
		$skuCode2LocationMapping = array();
		foreach ($items as $item) {
			$skucode = $item['skuBarcode'];
			if(!isset($skuCode2LocationMapping[$skucode]))
			$skuCode2LocationMapping[$skucode] = array();
				
			if(!in_array($item['binBarcode'], $skuCode2LocationMapping[$skucode]))
			$skuCode2LocationMapping[$skucode][] = $item['binBarcode'];
		}

		return $skuCode2LocationMapping;
	}

	public static function remove_item_order_association($warehouseId, $itemBarCodes, $itemStatus, $quality=false, $user, $issue=false, $reject_comment=false) {
		global $weblog, $xcart_dir;
		if(!is_array($itemBarCodes)){
			$itemBarCodes = array($itemBarCodes);
		}
		
		$itemBarcode2ItemId = ItemApiClientOld::get_itemids_for_barcodes($warehouseId, $itemBarCodes);
		
		$apiClient = ItemApiClientOld::getItemServiceApiClient('bulkupdate', $user);
		$itemUpdateReq['data'] = array();
		foreach ($itemBarCodes as $barcode) {
			$itemId = $itemBarcode2ItemId[$barcode];
			if(!$itemId || $itemId == ''){
				$weblog->debug("there is no item for the barcode given for removing item order association :::: sku $skuItemBarCode  item status $itemStatus user $user");
				return false;
			}
			
			$update_data = array('orderId'=>0, 'itemStatus'=>$itemStatus, 'id'=>$itemId);
			if($quality){
				$update_data['quality'] = $quality;
			}
			if($issue){
				$update_data['rejectReason'] = $issue;
			}
			if($reject_comment){
				$update_data['rejectReasonDescription'] = $reject_comment;
			}
			$itemUpdateReq['data'][] = array('item'=>$update_data); 
		}
		
		$apiClient->putXmlRequest('itemResponse', $itemUpdateReq);
		if($apiClient->isSuccess()) {
			/* // Remove this item from the carton if any..
			include_once("$xcart_dir/modules/apiclient/CartonApiClient.php");
			CartonApiClient::removeItemsFromCarton(array($itemId)); */
			return true;
		}
		else
			return $apiClient->getErrorMsg();
	}
	
    public static function cancelOrderUpdateSkusItems($orderid, $warehouseId, $user, $item_status, $quality = false, $rejectReason = false, $rejectReasonDescription = false) {
        global $weblog, $xcart_dir;
        $orderid = trim($orderid);
        
        ItemApiClientOld::get_sku_itembarcodes_for_order($orderid, $warehouseId);

        if ($apiClient->isSuccess()) {
            $issued_items = $apiClient->getData();
            $weblog->info("Items issued for order $orderid = " . print_r($issued_items, true));
            if (count($issued_items) > 0) {
                $itemIds = array();
                foreach ($issued_items as $item) {
                    $itemIds[] = $item['id'];
                }
                $status = ItemApiClientOld::remove_item_order_association($warehouseId, $itemIds, $item_status, $quality, $user, $rejectReason, $rejectReasonDescription);
            }
        }
    }

	public static function cancelItemsFromOrder($orderid, $warehouseId, $skuid, $original_qty, $qty_to_cancel, $user) {
		global $weblog, $xcart_dir;
		$orderid = trim($orderid);
		$apiClient = ItemApiClientOld::getItemServiceApiClient('', $user);
		$apiClient->search("orderId.eq:$orderid"."___sku.id.eq:$skuid");

		if($apiClient->isSuccess()) {
			$issued_items = $apiClient->getData();
			if( count($issued_items) > 0) {
				// check the no of items that will be present in the order item....
				$keepCount = $original_qty - $qty_to_cancel;

				// if the keep count is <= items issued
				// then from issued items remove the items to keep
				$status = true;
				if ($keepCount <= count($issued_items)) {
					$index = $keepCount;
					$itemIds = array();
					$removedItemBarCodes = array();
					for ($index; $index < count($issued_items); $index++) {
						$itemIds[] = $issued_items[$index]['id'];
						$removedItemBarCodes[] = $issued_items[$index]['barcode'];
					}
						
					if(count($itemIds) > 0) {
						$staus = ItemApiClientOld::remove_item_order_association($warehouseId, $itemIds, 'RETURN_FROM_OPS', false, $user);
					}
				}
			}
		} else {
			$weblog->info("Failed to get items for order $orderid - ". $apiClient->getErrorMsg());
			return false;
		}
		return array();
	}

	/* public static function getOrderIdsForItemsInCartons($cartonBarCodes) {
		global $xcart_dir, $weblog;
		include_once("$xcart_dir/modules/apiclient/CartonApiClient.php");
		include_once("$xcart_dir/modules/apiclient/BinApiClient.php");
		include_once("$xcart_dir/modules/apiclient/LocationApiClient.php");
		$carton2OrderIdMap = array();

		// first get the carton ids for these barcodes
		$cartonId2BarcodeMap = CartonApiClient::getCartonIdsForCartonBarcodes($cartonBarCodes);
		$cartonIds = array_keys($cartonId2BarcodeMap);
		if($cartonIds && count($cartonIds) > 0) {
			$weblog->info("Valid Carton Bar codes = ". print_r(array_values($cartonId2BarcodeMap), true));
				
			//get location for each of these cartonIds
			$cartonIdToBincode = LocationApiClient::getCartonsLocation($cartonIds);
				
			//check if carton can be moved to exit code..
			$binBarcodes = array_values($cartonIdToBincode);
			$binBarcodes = array_unique($binBarcodes);
			$canMoveToExitFromBins = BinApiClient::checkCanMoveToExitFromCurrentBin($binBarcodes);
			$movableCIds = array();
			foreach ($cartonIdToBincode as $cId => $barcode) {
				if(in_array($barcode, $canMoveToExitFromBins)) {
					$validCartonIds[] = $cId;
				}
			}
				
			$unmoveableCIds = array_diff(array_keys($cartonIdToBincode), $movableCIds);
				
			if(count($movableCIds) > 0) {
				$cartonId2itemIds = CartonApiClient::getItemsFromCartons($movableCIds);
				$weblog->info("Items found in the carton = ". print_r($cartonId2itemIds, true));

				$itemids = array();
				foreach ($cartonId2itemIds as $cId=>$cartonItemIds) {
					$itemids = array_merge($itemids, $cartonItemIds);
				}

				$itemid2OrderIdMap = array();
				if($itemids && count($itemids) > 0) {
					$apiClient = ItemApiClientOld::getItemServiceApiClient('');
					if(count($itemids) == 1) {
						$apiClient->search("id.eq:".$itemids[0]);
					} else {
						$apiClient->search("id.in:".implode(",", $itemids));
					}
					 
					if($apiClient->isSuccess()) {
						$items = $apiClient->getData();
						foreach ($items as $item) {
							if($item['itemStatus'] == 'ISSUED' && $item['orderId'] != 0) {
								$itemid2OrderIdMap[$item['id']] = $item['orderId'];
							} else {
								$itemid2OrderIdMap[$item['id']] = 'Invalid';
							}
						}
					}
				}
				$weblog->info("Item 2 Order ID = ". print_r($itemid2OrderIdMap, true));
				foreach ($cartonId2itemIds as $cId=>$cartonItemIds) {
					foreach ($cartonItemIds as $itemid) {
						if($itemid2OrderIdMap[$itemid] != 'Invalid') {
							$orderId = $itemid2OrderIdMap[$itemid];
						} else {
							$orderId = 'Invalid';
							break;
						}
					}
					if(is_numeric($orderId))
					$carton2OrderIdMap[$cartonId2BarcodeMap[$cId]] = $orderId;
					else if ($orderId)
					$carton2OrderIdMap[$cartonId2BarcodeMap[$cId]] = 'Invalid';
					else
					$carton2OrderIdMap[$cartonId2BarcodeMap[$cId]] = 'Empty';
				}
			}
				
			$moveableCartonBarCodes = array();
			foreach ($movableCIds as $cId) {
				$moveableCartonBarCodes[] = $cartonId2BarcodeMap[$cId];
			}
			$unmoveableCartonBarCodes = array();
			foreach ($unmoveableCIds as $cId) {
				$unmoveableCartonBarCodes[] = $cartonId2BarcodeMap[$cId];
			}
				
			$invalidCartonBarcodes = array_diff($cartonBarCodes, $moveableCartonBarCodes, $unmoveableCartonBarCodes);
				
			return array($moveableCartonBarCodes, $unmoveableCartonBarCodes, array_values($invalidCartonBarcodes), $carton2OrderIdMap);
		}
		return array();
	} */

	public static function validate_current_item_status($itemBarCodes, $statusToCheck) {

		$apiClient = ItemApiClientOld::getItemServiceApiClient('');

		$response = array();
		$validItemIds = array();
		$invalidItemIds = array();

		$batch = array();
		foreach ($itemBarCodes as $barcode) {
			if(count($batch) == 250) {
				$apiClient->search("barcode.in:".implode(",", $batch));
				if($apiClient->isSuccess()) {
					$data = $apiClient->getData();
					foreach($data as $item) {
						if($item['itemStatus'] != $statusToCheck)
						$invalidItemIds[] = $item['id'];
						else
						$validItemIds[] = $item['id'];
					}
				} else {
					$response['success'] = false;
					$response['errorMsg'] = $apiClient->getErrorMsg();
					return $response;
				}
				$batch = array();
			}
			$batch[] = $barcode;
		}

		if(count($batch) > 0) {
			$apiClient->search("barcode.in:".implode(",", $batch));
			if($apiClient->isSuccess()) {
				$data = $apiClient->getData();
				foreach($data as $item) {
					if($item['itemStatus'] != $statusToCheck)
					$invalidItemIds[] = $item['id'];
					else
					$validItemIds[] = $item['id'];
				}
			} else {
				$response['success'] = false;
				$response['errorMsg'] = $apiClient->getErrorMsg();
				return $response;
			}
		}

		if(count($invalidItemIds) > 0){
			$response['success'] = false;
			$response['errorMsg'] = "Itembarcodes ".implode(",", $invalidItemIds)." are not in $statusToCheck status";
		} else {
			$response['success'] = true;
			$response['itemids'] = $validItemIds;
		}
		return $response;
	}

	public static function validateOrderItemsStatus($orderid, $warehouseId, $valid_statuses) {
		$items = ItemApiClientOld::get_skuitems_for_order($orderid, $warehouseId);
		if($items && count($items) > 0) {
			foreach($items as $item) {
				if(!in_array($item['itemStatus'], $valid_statuses)){
					return false;
				} else {
					$valid_items[] = $item;
				}
			}
			return $valid_items;
		}
		return false;
	}
	public static function getStoredItemsForSku($skuIds, $warehouseId) {
		$items = array();
		if(!is_array($skuIds))
			$skuIds = array($skuIds);
		
		$apiClient = ItemApiClientOld::getItemServiceApiClient('');
		$apiClient->search("sku.id.in:".implode(",", $skuIds)."___itemStatus.eq:STORED___quality.eq:Q1___warehouseId.eq:$warehouseId&start=0&fetchSize=1");
		if($apiClient->isSuccess()) {
			$items = $apiClient->getData();
		} else {
			return $apiClient->getErrorMsg();
		}
		return $items;
	}
}

?>
