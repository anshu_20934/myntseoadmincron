<?php

define("itemEntityName", 'com.myntra.wms.entities.Item');
define("cartonEntityName", 'com.myntra.wms.entities.location.Carton');

include_once("$xcart_dir/modules/apiclient/ApiClient.php");
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";

class LocationApiClient {

	private static function getLocationServiceApiClient($action, $username='') {
		return new ApiClient('locationTrackerServiceUrl', $action, 'locationTracker', $username);
	}
	
	private static function getItemLocationDetails($itemIds, $include_virtual_bins=true) {
		$location_details = array();
		$apiClient = LocationApiClient::getLocationServiceApiClient('');
		$items_batch = array();
		foreach($itemIds as $itemId) {
			if(count($items_batch) < 250) {
				$items_batch[] = $itemId;
			} else {
				if($include_virtual_bins) {
					$apiClient->search("entityId.in:".implode(",",$items_batch)."___entityName.eq:".itemEntityName."___active.eq:true");
				} else {
					$apiClient->search("entityId.in:".implode(",",$items_batch)."___entityName.eq:".itemEntityName."___active.eq:true___bin.isVirtual.eq:false");
				}
				
				$data = $apiClient->getData();
				$location_details = array_merge($location_details, $data);
				$items_batch = array();
			}
		}
		
		if(count($items_batch) > 0) {
			if($include_virtual_bins) {
				$apiClient->search("entityId.in:".implode(",",$items_batch)."___entityName.eq:".itemEntityName."___active.eq:true");
			} else {
				$apiClient->search("entityId.in:".implode(",",$items_batch)."___entityName.eq:".itemEntityName."___active.eq:true___bin.isVirtual.eq:false");
			}
			
			$data = $apiClient->getData();
			$location_details = array_merge($location_details, $data);
		}
		
		return $location_details;
	}
	
	public static function addLocationInfoForItems($coreItems, $warehouseId) {
		global $weblog;
		
		$myntraOwnedWarehouses = FeatureGateKeyValuePairs::getFeatureGateValueForKey('wms.myntra.warehouses');
		$myntraOwnedWarehouses = explode(",", $myntraOwnedWarehouses);
		if(!in_array($warehouseId, $myntraOwnedWarehouses))
			return $coreItems;
		
		$itemids = array();
		foreach ($coreItems as $item) {
			$itemids[] = $item['id'];
		}
		
		$location_details = LocationApiClient::getItemLocationDetails($itemids);
		
		$itemIdToLocationMap = array();
		$itemIdToIsVirtualMap = array();
		$itemIdToRandomCodeMap = array();
		if(count($location_details) > 0) {
			foreach ($location_details as $item_location) {
				$binBarcode = $item_location['bin']['barcode'];
				$itemIdToLocationMap[$item_location['entityId']] = $binBarcode;
				$itemIdToIsVirtualMap[$item_location['entityId']] = $item_location['bin']['isVirtual'];
				$itemIdToRandomCodeMap[$item_location['entityId']] = $item_location['bin']['randomCode'];
			}	
		}
		
		foreach ($coreItems as $index=>$item) {
			$coreItems[$index]['binbarcode'] = $itemIdToLocationMap[$item['id']];
			$coreItems[$index]['isVirtual'] = $itemIdToIsVirtualMap[$item['id']];
			$coreItems[$index]['randomCode'] = $itemIdToRandomCodeMap[$item['id']];
		}
		
		return $coreItems;
	} 
	
	/*
	 * 
	 
	 public static function getCartonLocationDetails($cartonIds) {
		$location_details = array();
		$apiClient = LocationApiClient::getLocationServiceApiClient('');
		$cIds_batch = array();
		foreach($cartonIds as $cId) {
			if(count($cIds_batch) < 250) {
				$items_batch[] = $cId;
			} else {
				$apiClient->search("entityId.in:".implode(",",$cIds_batch)."___entityName.eq:".cartonEntityName."___active.eq:true");
				$data = $apiClient->getData();
				$location_details = array_merge($location_details, $data);
				$cIds_batch = array();
			}
		}
		
		if(count($items_batch) > 0) {
			$apiClient->search("entityId.in:".implode(",",$cIds_batch)."___entityName.eq:".cartonEntityName."___active.eq:true");
			$data = $apiClient->getData();
			$location_details = array_merge($location_details, $data);
		}
		
		return $location_details;
	}
	 
	public static function getCartonsLocation($cartonIds) {
		if(!is_array($cartonIds))
			$cartonIds = array($cartonIds);
		
		$cartonIds = array_unique($cartonIds);
		
		$location_details = LocationApiClient::getCartonLocationDetails($cartonIds);
		
		$cartonIdToBincode = array();
		if(count($location_details) > 0) {
			foreach ($location_details as $carton_location) {
				$binBarcode = $carton_location['bin']['barcode'];
				$cartonIdToBincode[$carton_location['entityId']] = $binBarcode;
			}	
		}
		
		return $cartonIdToBincode;
	}
	*/
	
	public static function moveItemsToExitBin($binLocation2ItemsMap, $username) {
		$invalidItemIds = array();
		$errorMsg = "";
		$apiClient = LocationApiClient::getLocationServiceApiClient('itemcheckout', $username);
		foreach ($binLocation2ItemsMap as $binCode=>$binItemsInfo) {
			$request = array();
			$request["checkOutside"] = true;
			
			$request['sourceBinBarcode'] = $binCode;
			$request['sourceBinConfirmationCode'] = $binItemsInfo['code'];
			$request['userId'] = 1;
			$itemIds = array();
			$count = 0;
			foreach($binItemsInfo['items'] as $itemId) {
				$request[$count++]['itemBarcodes'] = $itemId;
				$itemIds[] = $itemId;
			}
			
			$apiClient->postXmlRequest("movementEntry", $request);
			
			if(!$apiClient->isSuccess()) {
				$errorMsg .= $apiClient->getErrorMsg()."<br>";
				$invalidItemIds = array_merge($invalidItemIds, $itemIds);
			}
		}
		
		return array($invalidItemIds, $errorMsg);
	}
	
	/*public static function moveCartonsToExitBin($cartonBarCodes, $username) {
		
		global $xcart_dir, $weblog;
		include_once("$xcart_dir/modules/apiclient/CartonApiClient.php");
		$cartonId2BarcodeMap = CartonApiClient::getCartonIdsForCartonBarcodes($cartonBarCodes);
		$cartonIds = array_keys($cartonId2BarcodeMap);
		
		$location_details = LocationApiClient::getCartonLocationDetails($cartonIds);
		
		$cartonIdToBinMap = array();
		if(count($location_details) > 0) {
			foreach ($location_details as $carton_location) {
				$cartonIdToBinMap[$carton_location['entityId']] = $carton_location['bin'];
			}	
		}
		
		$cartonBarCode2IdMap = array_flip($cartonId2BarcodeMap);
		$apiClient = LocationApiClient::getLocationServiceApiClient('cartoncheckout', $username);
		
		$successMsg = $errorMsg = "";
		foreach ($cartonBarCodes as $cartonCode) {
			$request = array();
			$request["checkOutside"] = true;
			$request['cartonBarcodes'] = $cartonCode;
			
			$cid = $cartonBarCode2IdMap[$cartonCode];
			$bin_info =  $cartonIdToBinMap[$cid];
			
			$request['sourceBinBarcode'] = $bin_info['barcode'];
			$request['sourceBinConfirmationCode'] = $bin_info['randomCode'];
			$request['userId'] = 1;
			$apiClient->postXmlRequest("movementEntry", $request);
			
			if($apiClient->isSuccess()) {
				$successMsg .= "Successfully Moved Carton $cartonCode to Exit Bin<br>";
			} else {
				$errorMsg .= "Failed to move Carton $cartonCode to Exit Bin (".$apiClient->getErrorMsg().")<br>";
			}
		}
		
		return array($successMsg, $errorMsg);
	} */
	
	public static function moveItemsToBin($itembarcodes, $warehouseid, $binbarcode, $binrandomcode, $username) {
		
		$myntraOwnedWarehouses = FeatureGateKeyValuePairs::getFeatureGateValueForKey('wms.myntra.warehouses');
		$myntraOwnedWarehouses = explode(",", $myntraOwnedWarehouses);
		if(!in_array($warehouseid, $myntraOwnedWarehouses))
			return "";
		
		if($binbarcode && !empty($binbarcode) && $binrandomcode && !empty($binrandomcode)) {
		
		    $invalidItemIds = array();
		    $errorMsg = "";
		    $apiClient = LocationApiClient::getLocationServiceApiClient('itemcheckin', $username);
		
		    $request = array();
		    $request["checkOutside"] = false;
			
		    $request['destinationBinBarcode'] = $binbarcode;
		    $request['destinationBinConfirmationCode'] = $binrandomcode;
		    $request['userId'] = 1;
		
		    $count = 0;
		    foreach($itembarcodes as $barcode) {
		        $request[$count++]['itemBarcodes'] = $barcode;
		    }
			
		    $apiClient->postXmlRequest("movementEntry", $request);
			
		    if(!$apiClient->isSuccess()) {
			return $apiClient->getErrorMsg();
		    }
		} else {
                    return "Destination QA bin is not defined. Please contact OMS admin.";
                }
		return "";
	}
	
	public static function acceptItemsAtWarehouse($itembarcodes, $warehouseid, $username) {
		
		$myntraOwnedWarehouses = FeatureGateKeyValuePairs::getFeatureGateValueForKey('wms.myntra.warehouses');
		$myntraOwnedWarehouses = explode(",", $myntraOwnedWarehouses);
		if(!in_array($warehouseid, $myntraOwnedWarehouses))
			return "";
		
		$returnbinBarcode = trim(WidgetKeyValuePairs::getWidgetValueForKey("returns.accept.binbarcode.".$warehouseid));
		if(!is_array($itembarcodes))
			$itembarcodes = array($itembarcodes);
		
		$invalidItemIds = array();
		$errorMsg = "";
		$apiClient = LocationApiClient::getLocationServiceApiClient('acceptedReturns', $username);
		
		$request = array();
		$request['destinationBinBarcode'] = $returnbinBarcode;
		$request['userId'] = 1;
		
		$count = 0;
		foreach($itembarcodes as $barcode) {
			$request[$count++]['itemBarcodes'] = $barcode;
		}
			
		$apiClient->postXmlRequest("movementEntry", $request);
			
		if(!$apiClient->isSuccess()) {
			return $apiClient->getErrorMsg();
		}
		
		return "";
	}
}
