<?php
include_once("$xcart_dir/modules/apiclient/ApiClient.php");

class CartonApiClient {

	private static function getCartonServiceApiClient($action, $username='') {
		return new ApiClient('cartonServiceUrl', $action, 'carton', $username);
	}
	
	private static function getCartonItemServiceApiClient($action, $username='') {
		return new ApiClient('cartonItemServiceUrl', $action, 'cartonItem', $username);
	}
	
	public static function getCartonIdsForCartonBarcodes($cartonBarCodes) {
		$apiClient = CartonApiClient::getCartonServiceApiClient('');
		if(!is_array($cartonBarCodes)) {
			$cartonBarCodes = array($cartonBarCodes);
		}
		$cartonBarCodes = array_unique($cartonBarCodes);
		
		if(count($cartonBarCodes) == 1) {
			$apiClient->search("barcode.eq:".$cartonBarCodes[0]);
		} else {
			$apiClient->search("barcode.in:".implode(",", $cartonBarCodes));
		}
		
		$cartonId2BarcodeMap = array();
		if($apiClient->isSuccess()) {
			$carton_data = $apiClient->getData();
			foreach ($carton_data as $carton) {
				$cartonId2BarcodeMap[$carton['id']] = $carton['barcode'];
			}
		}
		
		return $cartonId2BarcodeMap;
	}
	
	public static function getItemsFromCartons($cartonIds) {
		$apiClient = CartonApiClient::getCartonItemServiceApiClient('');
		
		if(!is_array($cartonIds)) {
			$cartonIds = array($cartonIds);	
		}
		
		$cartonIds = array_unique($cartonIds);
		if(count($cartonIds) == 1) {
			$apiClient->search("carton.id.eq:".$cartonIds[0]."___active:true");
		} else {
			$apiClient->search("carton.id.in:".implode(",", $cartonIds)."___active:true");
		}
		
		$cartonId2itemId = array();
		if($apiClient->isSuccess()) {
			$cartonItems = $apiClient->getData();
			foreach ($cartonItems as $cartonItem) {
				if(!isset($cartonId2itemId[$cartonItem['carton']['id']]) )
					$cartonId2itemId[$cartonItem['carton']['id']] = array();
						
				$cartonId2itemId[$cartonItem['carton']['id']][] = $cartonItem['itemId'];
			}
		}
		return $cartonId2itemId;
	}
	
	public static function removeItemsFromCarton($itemIds) {
		$apiClient = CartonApiClient::getCartonItemServiceApiClient('freePreviousAssociations');
		
		$request = array('data' => array());
		foreach ($itemIds as $id) {
			$request['data'][] = array('cartonItem'=>array('itemId'=>$id));
		}
		
		$apiClient->postXmlRequest('cartonItemResponse', $request);
		if($apiClient->isSuccess())
			return true;
		else 
			return false;
	}
}