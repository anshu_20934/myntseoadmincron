<?php
    include_once("$xcart_dir/env/Host.config.php");
    include_once("$xcart_dir/modules/apiclient/PpsResponseHandler.php");

/**
 * Created by PhpStorm.
 * User: arpit.jain
 * Date: 06/10/15
 * Time: 1:35 PM
 */

class PpsApiClient {

    public static function getPpsApiClient($action, $key, $username = '') {
        return new PpsResponseHandler('ppsServiceUrl', $action, $key, $username);
    }

    public static function ppsEnabledCreateExchange($original_order, $old_item, $oldOptionId, $pps_id, $qty_to_exchange, $sku_id, $optionid, $userid, $reason_code, $comment, $selected_address_id) {
        global $errorlog;

        //TODO: Add ppsServiceUrl in the portalConfigs-base.mustache file, Blank right now
        $apiClient = PpsApiClient::getPpsApiClient('/v1/createExchangeOrder', 'responseClassType');

        $items = array();
        $items[] = array("originalOrderLineId" => $old_item['itemid'],
            "reasonCode" => $reason_code,
            "original" => array(
                "type" => "SKU",
                "itemIdentifier" => $old_item['sku_id'],
                "sellerId" => $old_item['seller_id'], //TODO: Condfirm about change of seller in exchanged order
                "quantity" => $old_item['amount'],
                "pricePerUnit" => intval($old_item['actual_product_price']), //TODO: Confirm about this price
                "optionId" => $oldOptionId,
                "styleId" => $old_item['product_style']),
            "exchange" => array(
                "type" => "SKU",
                "itemIdentifier" => $sku_id,
                "sellerId" => $old_item['seller_id'], //TODO: Condfirm about change of seller in exchanged order
                "quantity" => $qty_to_exchange,
                "pricePerUnit" => intval($old_item['actual_product_price']), //TODO: Confirm about this price
                "optionId" => $optionid,
                "styleId" => $old_item['product_style']));

        $ppsRequestArrayData = array(
            "originalOrderID" => $original_order['group_id'],
            "originalPpsID" => $pps_id,
            "reasonCode" => $reason_code,
            "userComments" => $comment,
            "userLogin" => $userid,
            "txRefId" => 'EXCHANGE'.$original_order['orderid'].'SKU'.$old_item['sku_id'].time(),
            "items" => $items,
            "shippingAddress" => PpsApiClient::getShippingAddress($original_order['login'], $selected_address_id)
        );
        $ppsRequestJsonBody = json_encode($ppsRequestArrayData);

        $apiClient->post($ppsRequestJsonBody, "application/json");
        $status = 'success';

        if ($apiClient->isSuccess() && $apiClient->getStatusCode() == '200') {
            $data = $apiClient->getData();
            if ($data == null){
                $errorlog->error("Unable to create exchange from PPS api " . $apiClient->getErrorMsg());
                $status = 'failure';
            } else {
                $exchangeOrderId = $data;
            }
        } else {
            $errorlog->error("Unable to create exchange from PPS api " . $apiClient->getErrorMsg());
            $status = 'failure';
            $warning_message = $apiClient->getErrorMsg();
        }

        return array('status' => $status, 'exchangeOrderId' => $exchangeOrderId, 'warning_message' => $warning_message);
    }

    private static function getShippingAddress($login, $selected_address_id) {
        if(!is_array($selected_address_id)){
            $address_sql = "select * from mk_customer_address where login = '$login' and id = $selected_address_id";
            $new_address = func_query_first($address_sql);
            return array(
                "receiverName" => $new_address['name'],
                "shippingAddress" => $new_address['address'],
                "shippingLocality" => $new_address['locality'],
                "shippingCity" => $new_address['city'],
                "shippingState" => $new_address['state'],
                "shippingCountry" => $new_address['country'],
                "shippingZipcode" => $new_address['pincode'],
                "receiverMobile" => $new_address['mobile'],
                "receiverEmail" => $new_address['email']);
        }
        else{
            return array(
                "receiverName" => $selected_address_id['name'],
                "shippingAddress" => $selected_address_id['address'],
                "shippingLocality" => $selected_address_id['locality'],
                "shippingCity" => $selected_address_id['city'],
                "shippingState" => $selected_address_id['state'],
                "shippingCountry" => $selected_address_id['country'],
                // MyMyntra's address array contains pincode as 'zipcode'
                "shippingZipcode" => $selected_address_id['zipcode'],
                "receiverMobile" => $selected_address_id['mobile'],
                "receiverEmail" => $selected_address_id['email']);
        }
    }
}