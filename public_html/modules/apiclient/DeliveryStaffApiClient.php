<?php
include_once("$xcart_dir/modules/apiclient/ApiClient.php");
class DeliveryStaffApiClient {
  	private static function getDeliveryStaffApiClient($action, $key, $username='') {
        return new ApiClient('deliveryStaffServiceUrl', $action, $key, $username);
    }
    
	public static function getDeliveryStaff($dsId) {
		global $errorlog;
        $apiClient = DeliveryStaffApiClient::getDeliveryStaffApiClient('' . $dsId, 'deliveryStaff');
        $apiClient->getDataForAction();
        $trip = false;
        $resp = array();
        if ($apiClient->isSuccess()) {
            $response = $apiClient->getData();
            $ds = $response[0];
            $resp['status']='success';
            $resp['data']=$ds;
        } else {
        	$msg = "Get delivery staff for id =$dcId failed. Error Msg: " . $apiClient->getErrorMsg();
            $errorlog->error($msg);
            $resp['status']='failure';
            $resp['msg']=$msg;
        }
        return $resp;
    }
	
}
?>