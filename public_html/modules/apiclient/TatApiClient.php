<?php
include_once("$xcart_dir/modules/apiclient/ApiClient.php");
include_once($xcart_dir."/Cache/Cache.php");
include_once("$xcart_dir/Cache/CacheKeySet.php");

class TatApiClient{
	
	 	
 	public static function getTatApiClient($action, $key, $username='') {
		return new ApiClient('tatServiceUrl', $action, $key, $username);
	}
	
        // This API will be scrapped after on hold migration
        /**
         * The response is an array of promise times formatted string (java date format) 
         * @param type $zipcode
         * @param type $whid
         * @param type $courierCode
         * @param type $shippingMethod
         * @param type $skipHolidays
         * @param type $leadTime
         * @return type
         */
	public static function getPromiseDate($zipcode, $whid, $courierCode, $shippingMethod, $skipHolidays = true, $leadTime = 0) {
            $apiClient = TatApiClient::getTatApiClient('getPromiseDate', 'promiseDateEntry');
            if ($shippingMethod == 'XPRESS') {
                $shippingMethod = 'EXPRESS';
            }
            $searchQuery = "warehouseId=".$whid."&courierCode=".$courierCode."&shippingMethod=".$shippingMethod."&pincode=".$zipcode."&skipHolidays=".TatApiClient::getBooleanString($skipHolidays)."&leadTime=".$leadTime;
            $apiClient->getDataForAction($searchQuery);
            if ($apiClient->isSuccess()) {
                $promiseDateEntry = $apiClient->getData();
                $promiseDateEntries = array();
                if (isset($promiseDateEntry[0]['shippingCutOff'])) {
                        $shippingCutOffSplit = explode("+",$promiseDateEntry[0]['shippingCutOff']);
                        $shippingCutOffSplitAgain = explode(".",$shippingCutOffSplit[0]);
                        $shippingCutOffString = str_replace("T"," ",$shippingCutOffSplitAgain[0]);
                        $promiseDateEntries['shippingCutOff'] = $shippingCutOffString;
                }
                if (isset($promiseDateEntry[0]['pickByCutOff'])) {
                        $pickByCutOffSplit = explode("+",$promiseDateEntry[0]['pickByCutOff']);
                        $pickByCutOffSplitAgain = explode(".",$pickByCutOffSplit[0]);
                        $pickByCutOffString = str_replace("T"," ",$pickByCutOffSplitAgain[0]);
                        $promiseDateEntries['pickByCutOff'] = $pickByCutOffString;
                }
                if (isset($promiseDateEntry[0]['packByCutOff'])) {
                        $packByCutOffSplit = explode("+",$promiseDateEntry[0]['packByCutOff']);
                        $packByCutOffSplitAgain = explode(".",$packByCutOffSplit[0]);
                        $packByCutOffString = str_replace("T"," ",$packByCutOffSplitAgain[0]);
                        $promiseDateEntries['packByCutOff'] = $packByCutOffString;
                }
                if (isset($promiseDateEntry[0]['promiseDate'])) {
                        $promiseDateSplit = explode("+",$promiseDateEntry[0]['promiseDate']);
                        $promiseDateSplitAgain = explode(".",$promiseDateSplit[0]);
                        $promiseDateString = str_replace("T"," ",$promiseDateSplitAgain[0]);
                        $promiseDateEntries['promiseDate'] = $promiseDateString;
                }
                if (isset($promiseDateEntry[0]['qcByCutOff'])) {
                        $qcByCutOffSplit = explode("+",$promiseDateEntry[0]['qcByCutOff']);
                        $qcByCutOffSplitAgain = explode(".",$qcByCutOffSplit[0]);
                        $qcByCutOffString = str_replace("T"," ",$qcByCutOffSplitAgain[0]);
                        $promiseDateEntries['qcByCutOff'] = $qcByCutOffString;
                }
                return $promiseDateEntries;
            } else {
                return array();
            }
        }
        
        private static function getBooleanString($booleanValue) {
                if ($booleanValue) {
                        return "true";
                } else {
                        return "false";
                }
        }
        
	public static function getTat($zipcode, $whids, $paymentMethod=false, $courier=false, $serviceType='NORMAL'){

        $apiClient = TatApiClient::getTatApiClient('search','turnAroundTime');
        
        $searchQuery = "destination.eq:".$zipcode."___serviceType.eq:".$serviceType;
        if(count($whids) > 1){
			$searchQuery.="___warehouseId.in:".implode(",",$whids);			        	
        }else{
        	$searchQuery.="___warehouseId.eq:".$whids[0];
        }
        if($paymentMethod){
        	$searchQuery.="___paymentMode.eq:".$paymentMethod;
        }
        if($courier){
        	$searchQuery.="___courierCode.eq:".$courier;
        }
        
        $apiClient->search($searchQuery);
        if ($apiClient->isSuccess()) {
        	$tatResponse = $apiClient->getData();
        	$minTat = false;
        	foreach($tatResponse as $tat){
        		if(!$minTat || $tat['tatTime'] < $minTat){
        			$minTat = $tat['tatTime']; 
        		}
        	}
        	if(!$minTat){
        		$minTat = FeatureGateKeyValuePairs::getInteger('lms.tat.defaultNoOfDays',5) * 24; 
        	}
        	return $minTat;
        }else{
        	return FeatureGateKeyValuePairs::getInteger('lms.tat.defaultNoOfDays',5) * 24;
        }
	}
	
	public static function getCourierCutOffs($zipcode,$whid,$paymentMethod, $courier, $serviceType='NORMAL'){
		$params = array();
		$params[] = $zipcode;
		$params[] = $whid;
		$params[] = $courier;
		$params[] = $paymentMethod;
		$params[] = $serviceType;
		
		$cache = new \Cache(ExpressTATCacheKeys::$prefix, ExpressTATCacheKeys::keySet($whid, $zipcode, $paymentMethod, $courier));
	
		$tatRes = $cache->get(function($zipcode,$whid,$courier,$paymentMethod,$serviceType){
			$apiClient = TatApiClient::getTatApiClient('search','turnAroundTime');
	        $searchQuery = "destination.eq:".$zipcode."___serviceType.eq:".$serviceType."___warehouseId.eq:".$whid."___paymentMode.eq:".$paymentMethod."___courierCode.eq:".$courier;
	        $apiClient->search($searchQuery, array('sort'=>'shippingCutoff', 'dir'=>'ASC'));
	        
	        if ($apiClient->isSuccess()) {
	        	$tatResponse = $apiClient->getData();
	        	return $tatResponse;
	        }else{
	        	return null;
	        }	
		}, $params, ExpressTATCacheKeys::getKey($whid, $zipcode, $paymentMethod, $courier),300);
	
		return $tatRes;
        
	}
	
	public static function getCachedMinimumTat($zipcode, $whIds, $courier=false, $serviceType='NORMAL'){
	
		$cachedTat = array();
		$fetchForWarehouses = array();
		foreach($whIds as $whId) {
			$key = TATCacheKeys::getTatKey($whId, $zipcode, $courier, $serviceType);
			$cache = new \Cache(TATCacheKeys::$prefix, array($key));
			$val = $cache->getValue($key);
			if($val) {
				$cachedTat[] = $val; 
			} else {
				$fetchForWarehouses[] = $whId;
			}
		}
		
		if(!empty($fetchForWarehouses)) {
			$apiClient = TatApiClient::getTatApiClient('search','turnAroundTime');
			$searchQuery = "destination.eq:".$zipcode."___serviceType.eq:".$serviceType;
			if(count($fetchForWarehouses) > 1){
				$searchQuery.="___warehouseId.in:".implode(",",$fetchForWarehouses);
			}else{
				$searchQuery.="___warehouseId.eq:".$fetchForWarehouses[0];
			}
			if($courier){
				$searchQuery.="___courierCode.eq:".$courier;
			}
			$apiClient->search($searchQuery);
			if ($apiClient->isSuccess()) {
				$tatResponse = $apiClient->getData();
				$whId2Tat = array();
				foreach ($fetchForWarehouses as $whId) {
					$whId2Tat[$whId] = false;
				}
				
				foreach($tatResponse as $tat){
					if(!$whId2Tat[$tat['warehouseId']] || $tat['tatTime'] < $whId2Tat[$tat['warehouseId']]){
						$whId2Tat[$tat['warehouseId']] = $tat['tatTime'];
					}
				}
				
				foreach($whId2Tat as $whId => $minTat) {
					if(!$minTat){
						$minTat = FeatureGateKeyValuePairs::getInteger('lms.tat.defaultNoOfDays',5) * 24;
					}
					$key = TATCacheKeys::getTatKey($whId, $zipcode, $courier);
					$cache = new \Cache(TATCacheKeys::$prefix, array($key));
					$cache->setVal($key, $minTat, 14400);
					$cachedTat[] = $minTat;
				}
			}else{
				$cachedTat[] = FeatureGateKeyValuePairs::getInteger('lms.tat.defaultNoOfDays',5) * 24;
			}
		}
	
		return min($cachedTat);
	}
	
}
?>

