<?php
include_once("$xcart_dir/modules/apiclient/ApiClient.php");

class BinApiClient {

	private static function getBinServiceApiClient($action, $username='') {
		return new ApiClient('binServiceUrl', $action, 'bin', $username);
	}
	
	public static function checkCanMoveToExitFromCurrentBin($binBarcodes) {
		$apiClient = BinApiClient::getBinServiceApiClient('getToOutsideBulk');
		
		$canMoveToExitFromBins = array();
		if( count($binBarcodes) > 0 ) {
			$binBarcodesStr = implode("___", $binBarcodes);
			$apiClient->getDataForAction(array('binBarcodes'=>$binBarcodesStr));
			
			if($apiClient->isSuccess()) {
				$bins_data = $apiClient->getData();
				foreach ($bins_data as $bin) {
					$canMoveToExitFromBins[] = $bin['barcode'];
				}
			}
		}
		
		return $canMoveToExitFromBins;		
	}
}