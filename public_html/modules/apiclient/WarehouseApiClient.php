<?php
include_once("$xcart_dir/modules/apiclient/ApiClient.php");

/**
 * The class contains all Warehouse(WMS) service calls.
 */
class WarehouseApiClient {

	private static function getWarehouseServiceApiClient($action, $username='') {
		return new ApiClient('warehouseServiceUrl', $action, 'warehouse', $username);
	}
	
	public static function getAllWarehouses() {
		global $weblog;
		
		$xcache = new XCache();
		$warehouseList = $xcache->fetch("warehouses");
		if(!$warehouseList || empty($warehouseList) || count($warehouseList) == 0) {
		
			$apiClient = WarehouseApiClient::getWarehouseServiceApiClient('');
			$apiClient->search(false, array('limit' => 200));
			$warehouseList = array();
			if($apiClient->isSuccess()) {
				$warehouseList = $apiClient->getData();
			}
			$xcache->store("warehouses", $warehouseList, 86400);
		}
		$weblog->info("Warehouses - ".print_r($warehouseList, true));
		return $warehouseList;			
	}
	
	public static function getWarehouseDetails($warehouseId) {
		global $weblog, $errorlog;
		$warehouse = false;
		$warehouses = WarehouseApiClient::getAllWarehouses();
		if($warehouses && !empty($warehouses) && count($warehouses) > 0){
			foreach($warehouses as $row) {
				if($row['id'] == $warehouseId) {
					$warehouse = $row;
					break;
				}
			}
		}
		return $warehouse;			
	}
	
	public static function getWarehouseId2NameMap() {
		$warehouses = WarehouseApiClient::getAllWarehouses();
		foreach($warehouses as $row) {
			$whId2NameMap[$row['id']] = $row['name'];
		}
		return $whId2NameMap;
	}
}