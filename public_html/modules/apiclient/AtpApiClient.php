<?php

include_once("$xcart_dir/modules/apiclient/ApiClient.php");
include_once($xcart_dir."/Cache/Cache.php");

class AtpApiClient {

	private static $storeId = 1;

	private static function getAtpInventoryServiceApiClient($action, $username='System') {
		return new ApiClient('atpServiceUrl', 'inventory/'.$action, 'portalInventory', $username);
	}

	private static function getAvailableStoreInventoryForSkus($skuIds, $user = 'System') {
		global $weblog;

		$weblog->debug("in getAvailableStoreInventoryForSkus");

		if(!empty($skuIds)) {
			$action = 'portalInventory/';
			$apiClient = AtpApiClient::getAtpInventoryServiceApiClient($action, $user);

			$portalInventoryRequest = array('skus' => array());
			$portalInventoryRequest = array('storeId'=>1);

			foreach($skuIds as $skuId) {
				$portalInventoryRequest['skus'][] = array('id'=>$skuId);
			}

			$apiClient->putXmlRequest('portalInventoryRequest', $portalInventoryRequest);

			if($apiClient->isSuccess()){
				return $apiClient->getData();
			}
		}

		return false;
	}

	public static function getAvailableInventoryForSkus($skuIds, $user = 'System') {
		global $weblog;

		$invModule = FeatureGateKeyValuePairs::getFeatureGateValueForKey('portal.inventory.module', 'wms');

		if(!empty($skuIds)) {
			if(!is_array($skuIds))
				$skuIds = array($skuIds);

/* no longer required - should always hit ATP for inv count
			if(strtolower($invModule) == 'wms') {
				$sku_details = SkuApiClient::getSkuDetailsWithInvCountsForIds($skuIds);
				$skuId2InvRowMap = array();
				foreach($sku_details as $row) {
					$skuInvRow = $skuId2InvRowMap[$row['id']];
					if(!$skuInvRow) {
						$skuInvRow = array();
						$skuInvRow['storeId'] = 1;
						$skuInvRow['sellerId'] = 1;
						$skuInvRow['skuId'] = $row['id'];
						$skuInvRow['supplyType'] = 'ON_HAND';
						$skuInvRow['leadTime'] = 0;
						$skuInvRow['availableCount'] = $row['invCount'];
						if($row['invCount'] > 0)
							$skuInvRow['availableInWarehouses'] = $row['warehouseId'];
					} else {
						$skuInvRow['availableCount'] += $row['invCount'];
						if($row['invCount'] > 0) {
							if(!empty($skuInvRow['availableInWarehouses']))
								$skuInvRow['availableInWarehouses'] .= ",";

							$skuInvRow['availableInWarehouses'] .= $row['warehouseId'];
						}
					}
					$skuId2InvRowMap[$row['id']] = $skuInvRow;
				}
				return $skuId2InvRowMap;
			} else {*/

				$sellerInventoryRows = AtpApiClient::getAvailableStoreInventoryForSkus($skuIds);

				if($sellerInventoryRows && !empty($sellerInventoryRows) && count($sellerInventoryRows) > 0){
					$skuId2InvRowMap = array();
							foreach($sellerInventoryRows as $row) {
								if(!($row['supplyType'] == 'ON_HAND' && $row['availableInventory'] <= 0)) {
									if(empty($skuId2InvRowMap[$row['skuId']])) {
										$tempRow['availableCount'] = $row['availableInventory'];
										$tempRow['supplyType'] = $row['supplyType'];
										$skuId2InvRowMap[$row['skuId']] = $tempRow;
									}
								}
					}

					foreach($skuIds as $id) {
						if(empty($skuId2InvRowMap[$id]))
							$skuId2InvRowMap[$id] = array();
					}

					return $skuId2InvRowMap;
				}
		//	}
		}
		return array();
	}

	public static function getAvailableInventoryForSkuSupplyType($itemDetails, $user = 'System') {
		if(!empty($itemDetails)) {
			$action = 'availableInventory/'.AtpApiClient::$storeId;
			$apiClient =  new ApiClient('atpServiceUrl', 'inventory/'.$action, 'inventory', $user);
			$inventoryResponse = array();
			$data = array();
			foreach($itemDetails as $key => $item){
				$inventory['storeId'] = 1;
				$inventory['sellerId'] = $item['seller_id'];
				$inventory['skuId'] = $item['sku_id'];
				$inventory['blockedOrderCount'] = $item['amount'];
				$inventory['supplyType'] = $item['supply_type'];
				$data[] = array('inventory' => $inventory);
			}
			$inventoryResponse['data'] = $data;
			$apiClient->putXmlRequest('inventoryResponse',$inventoryResponse);
			if($apiClient->isSuccess()){
				return $apiClient->getData();
			}
		}
		return false;
	}

	public static function getAvailableInventoryForItems($itemIds, $user = 'System') {
		if($itemIds){
			$itemDetails = func_query("select od.seller_id,od.supply_type,sosm.sku_id from xcart_order_details od, mk_order_item_option_quantity oq, mk_styles_options_skus_mapping sosm where od.itemid = oq.itemid and oq.optionid = sosm.option_id and od.product_style = sosm.style_id and od.itemid in (".implode(",",$itemIds).") group by od.seller_id,od.supply_type,sosm.sku_id");
			$invModule = FeatureGateKeyValuePairs::getFeatureGateValueForKey('portal.inventory.module', 'wms');
			if($invModule == 'wms'){
				$skuIds = array();
				foreach($itemDetails as $key => $item){
					$skuIds[] = $item['sku_id'];
				}
				$skuDetails = SkuApiClient::getSkuDetailsInvCountMap($skuIds);
				return $skuDetails;
			}

			$rows = AtpApiClient::getAvailableInventoryForSkuSupplyType($itemDetails);
			$skuId2WarehouseCountsRows= array();
			if($rows){
				foreach($rows as $row) {
					$skuId2WarehouseCountsRows[$row['skuId']] = $row;
				}

				$skuId2DetailsMap = array();
				foreach($skuId2WarehouseCountsRows as $skuId => $skuRow) {
					$sku = array('id'=>$skuId, 'code'=>$skuRow['skuCode'], 'availableCount' => $skuRow['availableCount']);
					if(!empty($skuRow['availableInWarehouses'])) {
						foreach(explode(",", $skuRow['availableInWarehouses']) as $whId) {
							$sku['warehouse2AvailableItems'][$whId] = $skuRow['availableCount'];
						}
					}
					$skuId2DetailsMap[$skuId] = $sku;
				}

				return $skuId2DetailsMap;
			}
		}
	}

}

?>