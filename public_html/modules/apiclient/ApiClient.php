<?php
	include_once("$xcart_dir/utils/Helper.php");
	include_once("$xcart_dir/utils/http/HttpClient.class.php");
	include_once("$xcart_dir/utils/http/SimpleParser.php");
	include_once("$xcart_dir/include/func/func.db.php");
	include_once("$xcart_dir/include/class/class.mail.multiprovidermail.php");
	
	class ApiClient {
		
		private $apiServiceUrl;
		private $apiAccessor;
		private $dataKey;
		private $action;
		private $success;
		private $errorMsg;
		private $data;
		private $requestUri;
		private $postData;
		private $request_method = "GET";
		private $totalCount=0;
		private $statusCode;
		
		public function __construct($serviceType, $action, $datakey, $audituser) {
			$serviceUrl = Helper::getPropertyValue($serviceType);
			$this->action = $action;
			$this->apiAccessor = (empty($audituser)?'System':$audituser);
			$this->apiServiceUrl = $serviceUrl;
			$this->dataKey = $datakey;
			$this->success = false;
			$this->request_method = "GET";
			$this->postData = false;
		}
		
		public function isSuccess() {
			return $this->success;
		}
		
		public function getData() {
			return $this->data;
		}
		
		public function getErrorMsg() {
			return $this->errorMsg;
		}
		
		public function getTotalCount(){
			return $this->totalCount;
		}
		public function getStatusCode(){
			return $this->statusCode;
		}

		private function extractDataFromResponse($response) {
			if($response) {
				$this->statusCode = $response['statusCode'];
				if($response['success'] && $response['success'] == 1) {
					$this->success = true;
					if($response['results']['status']['totalCount'] > 0) {
						$totalCount = $response['results']['status']['totalCount'];
						$this->totalCount = $totalCount;
						$key = $this->dataKey;
						if($key !== false) {
							if($response['results']['data'] && $response['results']['data'][$key]) {
								$this->data = $response['results']['data'][$key];
							} else {
								$this->data = array();
							}
						} else {
							if($response['results']['status']['totalCount'] > 1) {
								$this->data = $response['results']['data'];
							} else {
								if(isset($response['results']['data'][0]))
									$this->data = $response['results']['data'][0];
								else
									$this->data = $response['results']['data'];
							}
						}
					} else {
						$this->data = array();
					}
				} else {
					$this->success = false;
					$this->errorMsg = $response['failureMsg'];
					$this->data = array();
					$this->handleApiFailure();
				}
			} else {
				$this->statusCode = 500;
				$this->success = false;
				$this->errorMsg = 'Status response not found in the response: Check the service response';
				$this->data = array();
				$this->handleApiFailure();
			}
		}
		
		public function search($query = false, $limit = false) {
			$this->requestUri = $this->apiServiceUrl . "search/"; 
			$queryString = "";
			if($limit) {
				$queryString.= "?".Helper::getCommonRequestParams($limit);
			}
			if($query) {
				$queryString .= ($queryString==""?"?q=$query":"&q=$query");
			}
			$this->requestUri .= $queryString;
			
			$response_string = HttpClient::quickGet($this->requestUri, $this->apiAccessor);
			$response = Helper::decode_response($response_string);
			$this->extractDataFromResponse($response);
		}
		
		public function detailsearch($query = false, $limit = false) {
			$this->requestUri = $this->apiServiceUrl . "details/search/"; 
			
			$queryString = "";
			if($limit) {
				$queryString.= "?".Helper::getCommonRequestParams($_REQUEST);
			}
			if($query) {
				$queryString .= ($queryString==""?"?q=$query":"&q=$query");
			}
			$this->requestUri .= $queryString;
			
			$response_string = HttpClient::quickGet($this->requestUri, $this->apiAccessor);
			$response = Helper::decode_response($response_string);
			$this->extractDataFromResponse($response);
		}
		
		public function getById($id) {
			$this->requestUri = $this->apiServiceUrl . $id;
        	$response_string = HttpClient::quickGet($this->requestUri, $this->apiAccessor);
        	$response = Helper::decode_response($response_string);
			$this->extractDataFromResponse($response);
		}
		
		public function getDataForAction($criteria_arr=false) {
			global $weblog;
			$this->requestUri = $this->apiServiceUrl .$this->action;
			$response_string = HttpClient::quickGet($this->requestUri, $this->apiAccessor, $criteria_arr);
			$response = Helper::decode_response($response_string);
			$this->extractDataFromResponse($response);
		}
		
		public function get($query) {
			global $weblog;
			$this->requestUri = $this->apiServiceUrl .$this->action;
			$this->requestUri .= "?".$query;
			$response_string = HttpClient::quickGet($this->requestUri, $this->apiAccessor);
			$response = Helper::decode_response($response_string);
			$this->extractDataFromResponse($response);
		}
		
		public function post($data=false, $contentType = "application/xml") {
			$this->requestUri = $this->apiServiceUrl .$this->action;
			$response_string = HttpClient::quickPost($this->requestUri, $data , $this->apiAccessor, $contentType);
			$response = Helper::decode_response($response_string);
			$this->extractDataFromResponse($response);
		}

		public function update($id, $rootNode, $params) {
			$this->requestUri = $this->apiServiceUrl . $id;
			$data_string = SimpleParser::arrayToXml($params, $rootNode);
			$this->request_method = "PUT";
			$this->postData = $data_string;
			$response_string = HttpClient::quickPut($this->requestUri, $data_string, $this->apiAccessor);
			$response = Helper::decode_response($response_string);
			$this->extractDataFromResponse($response);
		}

		public function bulkUpdate($setParams, $whereCondition) {
			$this->requestUri = $this->apiServiceUrl.'bulkupdate/';
			$setQuery = "";
			foreach ($setParams as $key=>$value) {
				$setQuery .= $setQuery==""?"":"___";
				$setQuery .= "$key:$value";
			}
			$this->requestUri .= "?set=$setQuery"."&where=$whereCondition";
			$response_string = HttpClient::quickGet($this->requestUri, $this->apiAccessor);
			$response = Helper::decode_response($response_string);
			$this->extractDataFromResponse($response);
		}
		
		public function putXmlRequest($rootEle, $object) {
			$xmlString = false;
			if($object) {
				$xmlString = SimpleParser::arrayToXml($object, $rootEle);
				$this->postData = $xmlString;
			}
			$this->requestUri = $this->apiServiceUrl.$this->action;
			$this->request_method = "PUT";
			$response_string = HttpClient::quickPut($this->requestUri, $xmlString, $this->apiAccessor);
			$response = Helper::decode_response($response_string);
			$this->extractDataFromResponse($response);
		}

		public function customPutXmlRequest($rootEle, $object) {
			$xmlString = false;
			if($object) {
				$xmlString = SimpleParser::arrayToXml($object, $rootEle);
				$this->postData = $xmlString;
			}
			$this->requestUri = $this->apiServiceUrl.$this->action;
			$this->request_method = "PUT";
			//echo "Request: ".print_r($xmlString,true);
			$response_string = HttpClient::quickPut($this->requestUri, $xmlString, $this->apiAccessor);
			$response = Helper::decode_response($response_string);
			//echo "Response: ".print_r($response,true);
			$this->extractDataFromResponse($response);
		}

		public function postXmlRequest($rootEle, $object) {
			$xmlString = false;
			if($object) {
				$xmlString = SimpleParser::arrayToXml($object, $rootEle);
				$this->postData = $xmlString;
			}
			$this->requestUri = $this->apiServiceUrl.$this->action;
			$this->request_method = "POST";
			$response_string = HttpClient::quickPost($this->requestUri, $xmlString, $this->apiAccessor);
			$response = Helper::decode_response($response_string);
			$this->extractDataFromResponse($response);
		}

		public function customPost($queryData) {
			$this->requestUri = $this->apiServiceUrl .$this->action;
			$this->request_method = "POST";
			if($queryData) {
				$querystring = '';
				foreach($queryData as $key=>$value) {
					$querystring .= urlencode($key).'='.urlencode($value).'&';
				}
				$querystring = substr($querystring, 0, -1); // Eliminate unnecessary & */
			}
			$this->postData = $querystring;
			$response_string = HttpClient::quickPost($this->requestUri, $querystring , $this->apiAccessor, 'application/x-www-form-urlencoded');
			return $response_string;
		}

		private function handleApiFailure() {
			$errorCode = $this->statusCode;
			if($errorCode == 500 || ($errorCode >= 51 && $errorCode <= 58)){
				$urlHit = $this->requestUri;
				$postData = $this->postData;
				$request_method = $this->request_method;
				/*if($request_method != 'GET'){
					db_query("insert into api_failed_calls(url, data, request_method, error_code, status,created_on) values ('$urlHit','$postData', '$request_method', '$errorCode', 'retry',now())");
				}*/
				//$this->notifyApiFailure(constant("WMS_API_FAILURE_NOTIFICATIONS"));
			}
		} 
		
		public function notifyApiFailure($sendTo) {
			global $weblog, $server_name;
			// Alert from system for api failure...
			$contents = "Api Call Failed<br>";
			$contents .= "Request Method: ".$this->request_method."<br>";
			$contents .= "Service Url Called - ".$this->requestUri."<br>";
			$contents .= "Calling portal server - <b>$server_name</b><br>";
			$contents .= "Calling portal page - <b>".$_SERVER["PHP_SELF"]."</b><br>";
			$contents .= "Time - ". date('j-n-Y H:i:s')."<br>";
			$contents .= "Admin User - ".$this->apiAccessor."<br>";
			$contents .= "Failure Reason - ".$this->errorMsg."<br>";
			if($this->postData) {
				$contents .= "<br/>POST/PUT Data - <br>".htmlentities($this->postData);
			}
			
			$url_parts = parse_url($this->requestUri);
            $path = $url_parts['path'];
            $path_parts = explode("/", $path);

            $subject = "Api Call Failed - ";
            $subject .= $path_parts[1] . " - ";
            if($path_parts[2] == 'platform') {
                    $subject .= $path_parts[2]."/".$path_parts[3];
            } else {
                    $subject .= $path_parts[2]." - ".$path_parts[3];
            }
			
			$mail_details = array( 
								//"header"=>"Content-Type: text/plain",
								"from_email"=>'admin@myntra.com',
								"from_name"=>'OMS Admin',
								"to" => $sendTo,
								"mail_type" => MailType::NON_CRITICAL_TXN,
								"subject"=> $subject,
								"content"=> $contents
							);
			$multiPartymailer = new MultiProviderMailer($mail_details);
			return $multiPartymailer->sendMail();
		}
	}

?>
