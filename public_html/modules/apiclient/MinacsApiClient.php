<?php

	include_once("$xcart_dir/modules/apiclient/ApiClient.php");
	include_once("$xcart_dir/include/func/func.order.php");
	include_once ("$xcart_dir/include/class/class.feature_gate.keyvaluepair.php");
	include_once("$xcart_dir/utils/http/SimpleParser.php");
	include_once("$xcart_dir/env/Host.config.php");
	
	class MinacsApiClient {
		
		private static function getApiClient($action, $username='') {
			return new ApiClient('minacsServiceUrl', $action, '', $username);
		}
		
		public static function pushOnHoldOrder($orderid, $reasonCode) {

			$pushToThirdPartyEnabled = FeatureGateKeyValuePairs::getBoolean('condCODOH.orders.push.thirdparty.enabled', false);
			if($pushToThirdPartyEnabled) {
			
				$data = func_query_first("Select o.group_id as SrNo, o.group_id as OrderId, concat(c.firstname,' ',c.lastname) as CustomerName, o.issues_contact_number as RegisteredPhoneNo, o.mobile as ShippingPhoneNo, o.s_address as Address, o.s_locality as Locality, o.s_city as City, o.s_state as State, o.s_country as Country, o.s_zipcode as Zipcode, from_unixtime(o.date, '%m/%d/%Y %H:%i:%s') as OrderCreationTime, group_concat(o.orderid) as allOrderIds, sum(o.subtotal) as OrderTotal, sum(o.coupon_discount) as CouponUsed, sum(o.cash_redeemed) as CashBackUsed, sum(o.discount+o.cart_discount) as NetDiscount, sum(o.total) as TobePaid from xcart_orders o, xcart_customers c where o.login = c.login and o.group_id = $orderid group by o.group_id");
	
				$products = func_query("select od.product_style, sp.product_display_name, po.value as size from xcart_order_details od, mk_style_properties sp, mk_order_item_option_quantity oq, mk_product_options po where od.product_style = sp.style_id and od.itemid = oq.itemid and oq.optionid = po.id and od.orderid in (".$data['allOrderIds'].")");
				$data['Products'] = ''; $data['ProductLinks'] = '';
				$urlPrefix = HostConfig::$baseUrl;
				foreach($products as $product) {
					if(!empty($data['Products'])) {
						$data['Products'] .= "#";
						$data['ProductLinks'] .= "#";
					}
					$data['Products'] .= $product['product_display_name'].':'.$product['size'];
					$data['ProductLinks'] .= $urlPrefix.$product['product_style'];
				}
				
				unset($data['allOrderIds']);
				
				$data['ReasonForOnHold'] = func_query_first_cell("select reason_display_name from mk_order_action_reasons where action = 'oh_disposition' and reason = '$reasonCode' and is_internal_reason = 1", true);
				
				$api_credentails = func_query_first("select * from mk_api_users where source = 'myntra-portal' and target = 'minacs'", true);
				$data['Uid'] = $api_credentails['userid'];
				$data['Pwd'] = $api_credentails['password'];
				
				$apiClient = MinacsApiClient::getApiClient('AddCustomer');
				$response_string = $apiClient->customPost($data);
				return self::parseAndCheckResponse($apiClient, $response_string);
			}
			return true;
		}
		
		public static function removeOnHoldOrder($orderid, $reason) {
			$pushToThirdPartyEnabled = FeatureGateKeyValuePairs::getBoolean('condCODOH.orders.push.thirdparty.enabled', false);
			if($pushToThirdPartyEnabled) {
				$data = array();
				$api_credentails = func_query_first("select * from mk_api_users where source = 'myntra-portal' and target = 'minacs'", true);
				$data['Uid'] = $api_credentails['userid'];
				$data['Pwd'] = $api_credentails['password'];
				$data['ReasonofCancel'] = $reason;
				$data['OrderId'] = $orderid;
				
				$apiClient = MinacsApiClient::getApiClient('CancelOrder');
				$response_string = $apiClient->customPost($data);
				return self::parseAndCheckResponse($apiClient, $response_string);
			}
			return true;
		} 
		
		
		private static function parseAndCheckResponse($apiClient, $responseString) {
			if($responseString) {
				$response = SimpleParser::xmlToArray($responseString);
				if($response != 'Successful') {
					$apiClient->notifyApiFailure(constant("MINACS_API_FAILURE_NOTIFICATIONS"));
					return false;
				}
			} else {
				$apiClient->notifyApiFailure(constant("MINACS_API_FAILURE_NOTIFICATIONS"));
				return false;
			}
			return true;
		}
	}

?>