<?php

include_once("$xcart_dir/modules/apiclient/ApiClient.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/include/class/oms/OrderModelTransformer.php");

class OrderApiClient {

	public static function getOrderServiceApiClient($action, $key, $username='') {
		return new ApiClient('orderServiceUrl', $action, $key, $username);
	}

	public static function getExchangeOrderServiceApiClient($action, $key, $username='') {
		return new ApiClient('exchangeOrderServiceUrl', $action, $key, $username);
	}

	public static function createOrder($orderid){
		//TODO: To be enabled after performance of e5-e7 boxes improves.
		/* $transformedOrder = OrderModelTransformer::transformToNewModel($orderid);
		if($transformedOrder) {
			$apiClient = OrderApiClient::getOrderServiceApiClient('', false);
			$apiClient->postXmlRequest('orderResponse', $transformedOrder);
			if($apiClient->isSuccess()) {
				return true;
			}
		} */
		
		return false;
	}

	public static function createExchangeOrder($original_order, $old_item, $qty_to_exchange, $sku_id, $optionid, $userid, $reason_code, $comment, $selected_address_id){
		global $errorlog;

		$apiClient = OrderApiClient::getExchangeOrderServiceApiClient('createExchange/', false);
		$exchangeOrderEntry = array(
								"orderId" => $original_order['group_id'],
								"exchangeLineEntries" => array(array(
									"orderLineId" => $old_item['itemid'],
									"quantity" => $qty_to_exchange,
									"reasonCode" => $reason_code,
									"skuId" => $sku_id,
									"optionid" => $optionid,
									"styleId" => $old_item['product_style'])),
								"reasonCode" => $reason_code,
								"userComment" => $comment,
								"userLogin" => $userid,
								"shippingAddressEntry" => OrderApiClient::getShippingAddress($original_order['login'], $selected_address_id),
								"billingAddressEntry" => OrderApiClient::getBillingAddress($original_order['login'], $selected_address_id));
		$exchangeRequestJsonBody = json_encode($exchangeOrderEntry);

		$apiClient->post($exchangeRequestJsonBody, "application/json");
		$status = 'success';

		if ($apiClient->isSuccess() && $apiClient->getStatusCode() == '1045') {
			$data = $apiClient->getData();
			if ($data == null){
				$errorlog->error("Unable to create exchange " . $apiClient->getErrorMsg());
				$status = 'failure';
			} else {
				$exchangeOrder = $data;
			}
		} else {
			$errorlog->error("Unable to create exchange " . $apiClient->getErrorMsg());
			$status = 'failure';
			$warning_message = $apiClient->getErrorMsg();
		}

		return array('status' => $status, 'exchangeOrder' => $exchangeOrder, 'warning_message' => $warning_message);
	}

	public static function queueExchangeOrder($orderId){
		global $errorlog;

		$apiClient = OrderApiClient::getExchangeOrderServiceApiClient('queueExchange/'.$orderId, false);

		$apiClient->customPutXmlRequest();
		$status = 'success';

		if ($apiClient->isSuccess() && $apiClient->getStatusCode() == '1004') {
			$data = $apiClient->getData();
			if ($data == null){
				$errorlog->error("Unable to queue exchange " . $apiClient->getErrorMsg());
				$status = 'failure';
			} else {
				$exchangeOrderId = $data;
			}
		} else {
			$errorlog->error("Unable to queue exchange " . $apiClient->getErrorMsg());
			$status = 'failure';
			$warning_message = $apiClient->getErrorMsg();
		}

		return array('status' => $status, 'exchangeOrderId' => $exchangeOrderId, 'warning_message' => $warning_message);
	}

	private static function getShippingAddress($login, $selected_address_id) {
		if(!is_array($selected_address_id)){
			$address_sql = "select * from mk_customer_address where login = '$login' and id = $selected_address_id";
			$new_address = func_query_first($address_sql);
			return array(
				"receiverName" => $new_address['name'],
				"shippingAddress" => $new_address['address'],
				"shippingLocality" => $new_address['locality'],
				"shippingCity" => $new_address['city'],
				"shippingState" => $new_address['state'],
				"shippingCountry" => $new_address['country'],
				"shippingZipcode" => $new_address['pincode'],
				"receiverMobile" => $new_address['mobile'],
				"receiverEmail" => $new_address['email']);
		}
		else{
			return array(
				"receiverName" => $selected_address_id['name'],
				"shippingAddress" => $selected_address_id['address'],
				"shippingLocality" => $selected_address_id['locality'],
				"shippingCity" => $selected_address_id['city'],
				"shippingState" => $selected_address_id['state'],
				"shippingCountry" => $selected_address_id['country'],
				// MyMyntra's address array contains pincode as 'zipcode'
				"shippingZipcode" => $selected_address_id['zipcode'],
				"receiverMobile" => $selected_address_id['mobile'],
				"receiverEmail" => $selected_address_id['email']);
		}
	}

	private static function getBillingAddress($login, $selected_address_id) {
		if(!is_array($selected_address_id)){
			$address_sql = "select * from mk_customer_address where login = '$login' and id = $selected_address_id";
			$new_address = func_query_first($address_sql);
			return array(
				"billingFirstName" => $new_address['name'],
				"billingAddress" => $new_address['address'],
				"billingCity" => $new_address['city'],
				"billingState" => $new_address['state'],
				"billingCounty" => $new_address['country'],
				"billingZipCode" => $new_address['pincode'],
				"billingMobile" => $new_address['mobile'],
				"billingEmail" => $new_address['email']);
		}
		else{
			return array(
				"billingFirstName" => $selected_address_id['name'],
				"billingAddress" => $selected_address_id['address'],
				"billingCity" => $selected_address_id['city'],
				"billingState" => $selected_address_id['state'],
				"billingCounty" => $selected_address_id['country'],
				// MyMyntra's address array contains pincode as 'zipcode'
				"billingZipCode" => $selected_address_id['zipcode'],
				"billingMobile" => $selected_address_id['mobile'],
				"billingEmail" => $selected_address_id['email']);
		}
	}
}

?>
