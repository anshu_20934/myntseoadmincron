<?php
include_once("$xcart_dir/modules/apiclient/ApiClient.php");
include_once($xcart_dir."/Cache/Cache.php");
include_once("$xcart_dir/Cache/CacheKeySet.php");
include_once($xcart_dir."/include/func/func.order.php");

/**
 * The class contains all Pincode service calls.
 */
class PincodeApiClient {

	public static function getCouriersForPincode($pincode) {
		global $weblog;
		
		//Remove unnecessary spaces
		$pincode = trim($pincode);	
			
		$pincodeObj = array();		
		
		//Considering all pincodes to be numeric.
		if(isInteger($pincode)){
			$mode = FeatureGateKeyValuePairs::getFeatureGateValueForKey('lms.courier_serviceability.fetch.mode');
			$weblog->debug("Courier serviceability fetch mode: " . $mode);
			if(strcasecmp($mode,'local')==0){
				$sql = "SELECT  courier as courierCode, cod_supported as codSupported, enabled as nonCodSupported, pickup_supported as pickupSupported, " .
						"exchange_supported as exchangeSupported FROM mk_zipcode_courier_support_mapping WHERE zipcode = $pincode";
				$pincodeObj = func_query($sql, true);
			}else{
				$cache = new \Cache(LMSPincodeCacheKeys::$prefix.$pincode, LMSPincodeCacheKeys::keySet());
				$pincodeObj = $cache->get(function($pincode){
					$apiClient = new ApiClient('pincodeServiceUrl', 'getCouriers/' . $pincode, 'pincode', '');
					$apiClient->getDataForAction();
					if($apiClient->isSuccess()) {
						$pincodeList = $apiClient->getData();
						return $pincodeList;
					}		
				}, array($pincode), "PincodeCourierMap", (60*60));
			}
		}else{
			$weblog->info($pincode . " is an invalid pincode. Hence, it's not serviceable.");
		}
		return empty($pincodeObj)?false:$pincodeObj;
	}
	
    public static function getAreaDetailForPincode($couriercode, $pincode) {
    	$pincodeList = array();
	    if ($couriercode != null && $couriercode != "") {
	        global $weblog;
	        $apiClient = new ApiClient('pincodeServiceUrl', 'search/?q=number.eq:'.$pincode. '___courierCode:'.$couriercode,'pincode', '');
	        $apiClient->getDataForAction();	        
	        if ($apiClient->isSuccess()) {
	            $pincodeList = $apiClient->getData();
	        }
	       
	    }
	    return $pincodeList;
}

    /*
     * fetches the DC(delivery center) detail based on courier service code and pincode
     * @param $couriercode - courier service code
     * @param $pincode - zip code
     * @return DC info
     */
    public static function getDCDetail($couriercode, $pincode) {
    	$pincodeList = array();
	    if (!empty($couriercode) && !empty($pincode)) {
            $xcache = new XCache();
            // get DC info for SR(service request) from cache
            $xcacheDCDetailKeyForSR = "DC_Detail_".$couriercode."_".$pincode;
            $pincodeList = $xcache->fetch($xcacheDCDetailKeyForSR);
            if(empty($pincodeList) || count($pincodeList) == 0) {
                $apiClient = new ApiClient('pincodeServiceUrl', 'search/?q=number.eq:'.$pincode. '___courierCode:'.$couriercode,'pincode', '');
                $apiClient->getDataForAction();
                if ($apiClient->isSuccess()) {
                    $pincodeList = $apiClient->getData();
                    // invalidate DC info in xcache once in a day
                    $xcache->store($xcacheDCDetailKeyForSR, $pincodeList, 86400);
                }
            }
	    }
	    return $pincodeList;
    }
	
}
