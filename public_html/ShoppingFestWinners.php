<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/auth.php';
include_once "$xcart_dir/include/func/func.order.php";

if(!FeatureGateKeyValuePairs::getBoolean("shoppingFestWinnersBoard")){
	echo "<meta http-equiv=\"Refresh\" content=\"0;URL=$http_location \" >";
}

if (empty($login)) {
	$queryString = $_SERVER['QUERY_STRING']
	? isset($_GET['view'])
	? $_SERVER['QUERY_STRING']
	: $_SERVER['QUERY_STRING']
	: "view=default";
	echo "<meta http-equiv=\"Refresh\" content=\"0;URL=$http_location/register.php?".$queryString."&view=leaderboard\" >";
}


$results = json_decode(WidgetKeyValuePairs::getWidgetValueForKey('shoppingFestResultsJson'),true);

$smarty->assign("resultsBoards",$results);

$smarty->display("ShoppingFestWinners.tpl");