<?php
include_once './auth.php';

use abtest\resolve\ResolverFactory;
use abtest\resolver\impl\RandomAlgoResolver;
use abtest\resolver\impl\NewUserOldUserRandomAlgoResolver;

$test=$_GET['test'];
$variant=$_GET['variant'];

if(!$test || !$variant){
        func_header_location("/");
        exit;
}

$resolver=abtest\resolver\ResolverFactory::getFastResolver($test);
if($resolver==NULL){
	func_header_location("/");
	exit;
}

$cookieValue=$resolver->reverseResolve($variant);
if(empty($cookieValue)){
	func_header_location("/");
	exit;
}

$cookieName=$cookieprefix.'mab';
if(!empty($_COOKIE[$cookieName]))
$cookieCombinedTestValue=explode('||',$_COOKIE[$cookieName]);
foreach ($cookieCombinedTestValue as $value){
	$split=explode('|',$value);
	$cookieCombinedValue[$split[0]]=$split[1];
}
$cookieCombinedValue[$test]=$cookieValue;

$cookieValueArray=array();
foreach ($cookieCombinedValue as $key=>$value){
	if(empty($value)){
		continue;
	}
	$cookieValueArray[]="$key|$value";
}
setcookie($cookieName, implode('||',$cookieValueArray), time() + (60 * 60 * 24 * 365), '/', HostConfig::$cookiedomain);
func_header_location("/");