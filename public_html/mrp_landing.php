<?php
	header("HTTP/1.1 301 Moved Permanently");
	header("Location: /faqs#mynt-club");
	exit;

require "./auth.php";
include_once './mrp_description.php';
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";

$login=$XCART_SESSION_VARS['login'];
if(!empty($login))
$smarty->assign("login", $login);

$pageName = 'mymyntra';
include_once $xcart_dir."/webengage.php";
include_once $xcart_dir."/gtm.php";

global $cashback_gateway_status, $mrpCouponConfiguration;

// Add the word describing the number of coupons
convert_coupon_numbers_to_words($mrpCouponConfiguration);

$nonfbregValue = $mrpCouponConfiguration['mrp_firstLogin_nonfbreg_numCoupons']*$mrpCouponConfiguration['mrp_firstLogin_nonfbreg_mrpAmount'];
$fbregValue = $mrpCouponConfiguration['mrp_firstLogin_fbreg_numCoupons']*$mrpCouponConfiguration['mrp_firstLogin_fbreg_mrpAmount'];
$refregValue = $mrpCouponConfiguration['mrp_refRegistration_numCoupons']*$mrpCouponConfiguration['mrp_refRegistration_mrpAmount'];
$refpurValue = $mrpCouponConfiguration['mrp_refFirstPurchase_numCoupons']*$mrpCouponConfiguration['mrp_refFirstPurchase_mrpAmount'];

$smarty->assign("nonfbregValue", $nonfbregValue);
$smarty->assign("fbregValue", $fbregValue);
$smarty->assign("refregValue", $refregValue);
$smarty->assign("refpurValue", $refpurValue);
$smarty->assign("mrpCouponConfiguration", $mrpCouponConfiguration);


$condNoDiscountCouponFG = FeatureGateKeyValuePairs::getBoolean('condNoDiscountCoupon');
$smarty->assign('condNoDiscountCouponFG', $condNoDiscountCouponFG);

$styleExclusionFG = FeatureGateKeyValuePairs::getBoolean('styleExclusion');
$smarty->assign('styleExclusionFG', $styleExclusionFG);

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setStaticPageVars(AnalyticsBase::MRP_LANDING_PAGE);
$analyticsObj->setTemplateVars($smarty);

if($skin == "skin2"){
	$smarty->display("mrp-landing.tpl");
}
else{
	if ($cashback_gateway_status == "on")	{	
		func_display("mrp-landing.tpl",$smarty);
	}else	{
		func_display("mrp-landing-without-cashback.tpl",$smarty);
	}	
}

?>