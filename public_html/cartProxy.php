<?php
include_once ("./auth.php");
include_once("./include/class/mcart/class.MCart.php");
include_once("./include/func/func.utilities.php");
include_once(\HostConfig::$documentRoot."/modules/RestAPI/CartActions.php");

use style\StyleGroup;
use style\builder\CachedStyleBuilder;
use style\builder\StyleBuilder;
use enums\cart\CartContext;

define("CART_PROXY",true);
header('Content-type: application/json');
$action  = $_GET["cartRequest"];
$realTime =  $_GET["rt"];
$_POST["fromComboOverlay"]=1;
$cartType=$_GET["cartType"];
$_GET["_token"]=$XCART_SESSION_VARS['USER_TOKEN'];//TODO 
$_POST["_token"]=$XCART_SESSION_VARS['USER_TOKEN'];//TODO to remove later
$_REQUEST["_token"]=$XCART_SESSION_VARS['USER_TOKEN'];//TODO to remove later

if($action === "add" && checkCSRF($_GET["_token"], "cartproxy", "add")){
	$requestData=json_decode($_GET["requestData"]);
	$cartPostData[0][skuid]=$requestData->skuId;
	$cartPostData[0][quantity]=$requestData->quantity;
	$cartPostData[0]['sizequantityArray'] =	  array(0=>$requestData->quantity);
	$cartPostData[0]['itemAddedByDiscountEngine']=false;
	$_POST[cartPostData]=json_encode($cartPostData);
	$_POST["_token"]=$XCART_SESSION_VARS['USER_TOKEN'];
	include("./mkretrievedataforcart_re.php");
	$myCart=$mcart;
}else if($action==="update" && checkCSRF($_GET["_token"], "cartproxy", "update")){
	$requestData=json_decode($_GET["requestData"]);
	$_REQUEST["_token"]=$XCART_SESSION_VARS['USER_TOKEN'];
	$HTTP_POST_VARS['update']="size";
	$_POST["fromComboOverlay"]=1;
	include("./modifycart.php");
}else if($action==="remove" && checkCSRF($_GET["_token"], "cartproxy", "remove")){
	$requestData=json_decode($_GET["requestData"]);
	$_REQUEST["_token"]=$XCART_SESSION_VARS['USER_TOKEN'];
	$HTTP_GET_VARS['remove']="true";
	$HTTP_GET_VARS["itemid"]=$requestData->itemId;
	$_POST["fromComboOverlay"]=1;
	include("./modifycart.php");
}else if($action==="clear" && checkCSRF($_GET["_token"], "cartproxy", "clear")){
	$requestData=json_decode($_GET["requestData"]);
	$_REQUEST["_token"]=$XCART_SESSION_VARS['USER_TOKEN'];
	$HTTP_GET_VARS["removeall"]="true";
	$_POST["fromComboOverlay"]=1;
	include("./modifycart.php");
	$myCart->clear();
	MCartUtils::persistCart($myCart);
}else if($action==="customize" && checkCSRF($_GET["_token"], "cartproxy", "customize")){
	$requestData=json_decode($_GET["requestData"]);
	$_REQUEST["_token"]=$XCART_SESSION_VARS['USER_TOKEN'];
	$HTTP_POST_VARS['update']="customize";
	include("./modifycart.php");
}else{
	$mcartFactory= new MCartFactory();
	$myCart = $mcartFactory->getCurrentUserCart(true);
	if($realTime == "true")
		$myCart->refresh(false,false,false,true);
	else
		$myCart->refresh();
	
	MCartUtils::persistCart($myCart);
	
}

$result = CartActions::cartToArray($myCart);
echo json_encode($result);
