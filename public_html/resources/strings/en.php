<?php

//Discount related display strings

//BuyXGetY
$_rst['BuyX_GetY_PDP_Tooltip'] = 'BUY <em>{$dre_buyCount-$dre_count} {$dre_placeHolderText}</em> & GET <em>{$dre_count} more {$dre_placeHolderText}</em> ';
$_rst['BuyX_GetY_Search'] = '(Buy {$widgetdata.dre_buyCount-$widgetdata.dre_count} Get {$widgetdata.dre_count} )';
$_rst['BuyX_GetY_PDP'] = '(Buy {$dre_buyCount-$dre_count} Get {$dre_count} )';
//kept null for now to avoid confusion
$_rst['BuyX_GetY_Cart_Combo'] = '';
$_rst['BuyX_GetY_Cart_Combo_CC'] = 'You have <em>{$productsSet.dre_totalCount} {$productsSet.dre_placeHolderText}</em> in your bag';
$_rst['BuyX_GetY_Cart_Combo_CNM'] = 'Buy <em>{$productsSet.dre_buyCount-$productsSet.dre_count} {$productsSet.dre_placeHolderText}</em> and get <em>{$productsSet.dre_count} more {$productsSet.dre_placeHolderText}</em>!';
$_rst['BuyX_GetY_Cart_Icon'] = '<span class="success-icon">&nbsp;</span>';
$_rst['BuyX_GetY_CartLevel_CM'] = '';
$_rst['BuyX_GetY_CartLevel_CC_TT'] = '';
$_rst['BuyX_GetY_CartLevel_CC_NM'] = '';

//Free Items
$_rst['Free_Item_PDP_Tooltip_CC'] = 'BUY <em>{$dre_buyCount} {$dre_placeHolderText}</em> & GET <em>{if $dre_count gt 1}{$dre_count}{/if} {$dre_itemName}</em>';
$_rst['Free_Item_PDP_Tooltip_CC_MC'] = 'BUY <em>{$dre_buyCount} {$dre_placeHolderText}</em> & GET a gift';
$_rst['Free_Item_PDP_Tooltip_CA'] = 'BUY <em>{$dre_placeHolderText}</em> worth <em><span class="rupees">Rs. </span>{$dre_buyAmount}</em> & GET <em>{if $dre_count gt 1}{$dre_count}{/if} {$dre_itemName}</em>';
$_rst['Free_Item_PDP_Tooltip_CA_MC'] = 'BUY <em>{$dre_placeHolderText}</em> worth <em><span class="rupees">Rs. </span>{$dre_buyAmount}</em> & GET a gift';
$_rst['Free_Item_PDP_Tooltip'] = 'Buy this and get <em>{if $dre_count gt 1}{$dre_count}{/if} {$dre_itemName}</em>!';
$_rst['Free_Item_PDP_Tooltip_MC'] = 'Buy this and get a gift';
$_rst['Free_Item_Search'] = '<div class=\'gift red-text right\'>Get <b>Gift</b><i>&nbsp;</i></div>';
$_rst['Free_Item_Search_C'] = '<div class=\'gift red-text right\'>Get <b>Gift</b>*<i>&nbsp;</i></div>';
$_rst['Free_Item_PDP'] = 'Get <b>Gift</b><i>&nbsp;</i>';
$_rst['Free_Item_PDP_C'] = 'Get <b>Gift</b>*<i>&nbsp;</i>';
$_rst['Free_Item_Cart_Combo_CA'] = 'Buy <em>{$productsSet.dre_placeHolderText}</em> worth at least <em><span class="rupees">Rs. </span>{$productsSet.dre_buyAmount}</em> and get <em>{if $productsSet.dre_count gt 1}{$productsSet.dre_count}{/if} {$productsSet.dre_itemName}</em>!';
$_rst['Free_Item_Cart_Combo_CA_MC'] =  'Buy <em>{$productsSet.dre_placeHolderText}</em> worth at least <em><span class="rupees">Rs. </span>{$productsSet.dre_buyAmount}</em> and get a gift';
$_rst['Free_Item_Cart_Combo_CC'] = 'Buy at least <em>{$productsSet.dre_buyCount} {$productsSet.dre_placeHolderText}</em> and get <em>{if $productsSet.dre_count gt 1}{$productsSet.dre_count}{/if} {$productsSet.dre_itemName}</em>!';
$_rst['Free_Item_Cart_Combo_CC_MC'] = 'Buy at least <em>{$productsSet.dre_buyCount} {$productsSet.dre_placeHolderText}</em> and get a gift';
$_rst['Free_Item_Cart_Combo'] = 'Your free product has been added to the Cart!';
$_rst['Free_Item_Cart_Combo_MC'] = 'Your free product has been added to the Cart!';
$_rst['Free_Item_Cart_Icon'] = '<span class="success-icon">&nbsp;</span>';
$_rst['Free_Item_CartLevel_CM'] = '<span class="success-icon"></span> <span class="success" style="font-size:14px;">Congratulations! <em>{$dre_cart_itemName}</em> worth <em><span class="rupees">Rs. </span>{$dre_cart_itemPrice}</em> has been added to your bag.</span>';
$_rst['Free_Item_CartLevel_CA_NM'] = 'Add items worth <em><span class="rupees">Rs. </span>{$dre_cart_minMore}</em> more to reach <em><span class="rupees">Rs. </span>{$dre_cart_buyAmount}</em> and receive a <em>{$dre_cart_itemName}</em> worth <em><span class="rupees">Rs. </span>{$dre_cart_itemPrice}</em>';
$_rst['Free_Item_CartLevel_CA_NM_MC'] = 'Add items worth <em><span class="rupees">Rs. </span>{$dre_cart_minMore}</em> more to reach <em><span class="rupees">Rs. </span>{$dre_cart_buyAmount}</em> and choose a free item up to <em><span class="rupees">Rs. </span>{$dre_cart_freeItemsMaxWorth}</em> from our set of items';
$_rst['Free_Item_CartLevel_CC_NM'] = 'Add <em>{$dre_cart_minMore}</em> more items and receive a <em>{$dre_cart_itemName}</em> worth <em><span class="rupees">Rs. </span>{$dre_cart_itemPrice}</em>';
$_rst['Free_Item_CartLevel_CC_NM_MC'] = 'Add <em>{$dre_cart_minMore}</em> more items and choose a free item up to <em><span class="rupees">Rs. </span>{$dre_cart_freeItemsMaxWorth}</em> from our set of items';
$_rst['Free_Item_CartLevel_CM_MC'] = '<span class="success-icon"></span> <span class="success" style="font-size:14px;">Congratulations! Your free item has been added to your bag.</span><div class="gray" style="margin-top:8px;">Would you like to <a freecartc="1" discountid="{$dre_cart_discountId}" min="1" ctype="c" isconditionmet="true" class="combo-completion-btn " style="margin:0px;">change your gift</a>?</div>';
$_rst['Free_Item_CartLevel_TT'] = 'This Offer is not valid on {$dre_cart_cart_excluded_brands_str} items';
//condition met multiple choice item level
$_rst['Free_Item_CartLevel_CM_MC_IL'] = '<a freecartc="1" discountid="{$dre_cart_discountId}" min="1" ctype="c" isconditionmet="true" class="combo-completion-btn" style="margin:0px;font-size:12px;">change gift</a>';
//Flat Discount
$_rst['Flat_PDP_Tooltip_Percent'] = 'Buy this item and get <em>{$dre_percent}% </em> off';
$_rst['Flat_PDP_Tooltip_Amount'] = 'Buy this item and get <em>{$rupeesymbol} {$dre_amount|round|number_format:0:".":","} </em> off';
$_rst['Flat_Search_Percent'] = '({$widgetdata.dre_percent}% OFF)';
$_rst['Flat_Search_Amount'] = '({$rupeesymbol}{$widgetdata.dre_amount|round|number_format:0:".":","} OFF)';
$_rst['Flat_PDP_Percent'] = '({$dre_percent}% OFF)';
$_rst['Flat_PDP_Amount'] = '({$rupeesymbol}{$dre_amount|round|number_format:0:".":","} OFF)';
$_rst['Flat_Cart_P'] = '({$product.dre_percent}% Off)';
$_rst['Flat_Cart_A'] = '({$rs} {$product.dre_amount|round|number_format:0:".":","} Off)';
$_rst['Flat_Cart_Icon'] = '<span class="success-icon">&nbsp;</span>';
$_rst['Flat_Cart_Icon_L'] = '<span class="success-icon-large">&nbsp;</span>';
$_rst['Flat_Confirmation_Percent'] = '({$product.dre_percent}% OFF)';
$_rst['Flat_Confirmation_Amount'] = '({$rs}{$product.dre_amount|round|number_format:0:".":","} OFF)';

//Flat Conditional
/*** TODO Need to remove this block in next release */
$_rst['FC_Cart_Count_Amount'] = 'Buy <em>{$productsSet.dre_buyCount} {$productsSet.dre_placeHolderText}</em> and get <em><span class="rupees">Rs. </span>{$productsSet.dre_amount|round|number_format:0:".":","} Off</em> on this combo';
$_rst['FC_Cart_Count_Percent'] = 'Buy <em>{$productsSet.dre_buyCount} {$productsSet.dre_placeHolderText}</em> and get <em>{$productsSet.dre_percent}% Off</em> on this combo';
$_rst['FC_Cart_Amount_Amount'] = 'Buy <em>{$productsSet.dre_placeHolderText}</em> worth <em><span class="rupees">Rs. </span>{$productsSet.dre_buyAmount}</em> and get <em><span class="rupees">Rs. </span>{$productsSet.dre_amount|round|number_format:0:".":","} Off</em> on this combo';
$_rst['FC_Cart_Amount_Percent'] = 'Buy <em>{$productsSet.dre_placeHolderText}</em> worth <em><span class="rupees">Rs. </span>{$productsSet.dre_buyAmount}</em> and get <em>{$productsSet.dre_percent}% Off</em> on this combo';
$_rst['FC_Cart_Count_Amount_CM'] = 'Buy <em>{$productsSet.dre_buyCount} {$productsSet.dre_placeHolderText}</em> and get <em><span class="rupees">Rs. </span>{$productsSet.dre_amount|round|number_format:0:".":","} Off</em> on this combo';
$_rst['FC_Cart_Count_Percent_CM'] = 'Buy <em>{$productsSet.dre_buyCount} {$productsSet.dre_placeHolderText}</em> and get <em>{$productsSet.dre_percent}% Off</em> on this combo';
$_rst['FC_Cart_Amount_Amount_CM'] = 'Buy <em>{$productsSet.dre_placeHolderText}</em> worth <em><span class="rupees">Rs. </span>{$productsSet.dre_buyAmount}</em> and get <em><span class="rupees">Rs. </span>{$productsSet.dre_amount|round|number_format:0:".":","} Off</em> on this combo';
$_rst['FC_Cart_Amount_Percent_CM'] = 'Buy <em>{$productsSet.dre_placeHolderText}</em> worth <em><span class="rupees">Rs. </span>{$productsSet.dre_buyAmount}</em> and get <em>{$productsSet.dre_percent}% Off</em> on this combo';
/*** End of the removal block ***/
$_rst['FC_Cart_Combo_Count_Amount'] = 'Buy <em>{$productsSet.dre_buyCount} {$productsSet.dre_placeHolderText}</em> and get <em><span class="rupees">Rs. </span>{$productsSet.dre_amount|round|number_format:0:".":","} Off</em> on this combo';
$_rst['FC_Cart_Combo_Count_Percent'] = 'Buy <em>{$productsSet.dre_buyCount} {$productsSet.dre_placeHolderText}</em> and get <em>{$productsSet.dre_percent}% Off</em> on this combo';
$_rst['FC_Cart_Combo_Amount_Amount'] = 'Buy <em>{$productsSet.dre_placeHolderText}</em> worth <em><span class="rupees">Rs. </span>{$productsSet.dre_buyAmount}</em> and get <em><span class="rupees">Rs. </span>{$productsSet.dre_amount|round|number_format:0:".":","} Off</em> on this combo';
$_rst['FC_Cart_Combo_Amount_Percent'] = 'Buy <em>{$productsSet.dre_placeHolderText}</em> worth <em><span class="rupees">Rs. </span>{$productsSet.dre_buyAmount}</em> and get <em>{$productsSet.dre_percent}% Off</em> on this combo';
$_rst['FC_Cart_Combo_Count_Amount_CM'] = 'Buy <em>{$productsSet.dre_buyCount} {$productsSet.dre_placeHolderText}</em> and get <em><span class="rupees">Rs. </span>{$productsSet.dre_amount|round|number_format:0:".":","} Off</em> on this combo';
$_rst['FC_Cart_Combo_Count_Percent_CM'] = 'Buy <em>{$productsSet.dre_buyCount} {$productsSet.dre_placeHolderText}</em> and get <em>{$productsSet.dre_percent}% Off</em> on this combo';
$_rst['FC_Cart_Combo_Amount_Amount_CM'] = 'Buy <em>{$productsSet.dre_placeHolderText}</em> worth <em><span class="rupees">Rs. </span>{$productsSet.dre_buyAmount}</em> and get <em><span class="rupees">Rs. </span>{$productsSet.dre_amount|round|number_format:0:".":","} Off</em> on this combo';
$_rst['FC_Cart_Combo_Amount_Percent_CM'] = 'Buy <em>{$productsSet.dre_placeHolderText}</em> worth <em><span class="rupees">Rs. </span>{$productsSet.dre_buyAmount}</em> and get <em>{$productsSet.dre_percent}% Off</em> on this combo';
$_rst['FC_PDP_ToolTip_Count_Amount'] = 'BUY <em>{$dre_buyCount} {$dre_placeHolderText}</em> & GET <em><span class="rupees">Rs. </span>{$dre_amount|round|number_format:0:".":","}</em> Off on the combo';
$_rst['FC_PDP_ToolTip_Count_Percent'] = 'BUY <em>{$dre_buyCount} {$dre_placeHolderText}</em> & GET <em>{$dre_percent}%</em> Off on the combo';
$_rst['FC_PDP_ToolTip_Amount_Amount'] = 'BUY <em>{$dre_placeHolderText}</em> worth <em><span class="rupees">Rs. </span>{$dre_buyAmount}</em> & GET <em><span class="rupees">Rs. </span>{$dre_amount|round|number_format:0:".":","}</em> Off on the combo';
$_rst['FC_PDP_ToolTip_Amount_Percent'] = 'BUY <em>{$dre_placeHolderText}</em> worth <em><span class="rupees">Rs. </span>{$dre_buyAmount}</em> & GET <em>{$dre_percent}%</em> Off on the combo';
$_rst['FC_Search_Percent'] = '<em>{$widgetdata.dre_percent}% OFF *</em>';
$_rst['FC_Search_Amount'] = '<em>{$rupeesymbol} {$widgetdata.dre_amount|round|number_format:0:".":","} OFF *</em>';
$_rst['FC_PDP_Percent'] = '<strong>{$dre_percent}% Off</strong> when bought with Special Combo Offer';
$_rst['FC_PDP_Amount'] = 'Special Combo Offer - save upto <strong><span class="rupees">Rs. </span>{$dre_amount|round|number_format:0:".":","}</strong>';
$_rst['FC_Cart_CM_Amount'] = '({$rs} {$product.discountAmount|round|number_format:0:".":","} Off)';
$_rst['FC_Cart_CM_Percent'] = '({$product.dre_percent}% Off)';
$_rst['FC_Confirmation_Percent'] = '({$product.dre_percent}% OFF)';
$_rst['FC_Confirmation_Amount'] = '({$rs}{$product.discount|round|number_format:0:".":","} OFF)';
$_rst['Flat_CartLevel_CM_BA'] = 'Congratulations! You have got <em>Rs.{$dre_cart_amount|round|number_format:0:".":","}</em> off on your cart value';
$_rst['Flat_CartLevel_CM_BP'] = 'Congratulations! You have got <em>{$dre_cart_percent}%</em> off on your cart value';
$_rst['Flat_CartLevel_CC_TT'] = '';
$_rst['Flat_CartLevel_CA_TT'] = '';
$_rst['Flat_CartLevel_TT']	  = 'This Offer is not valid on {$dre_cart_cart_excluded_brands_str} items';
$_rst['Flat_CartLevel_CC_BA'] = 'Add <em>{$dre_cart_buyCount}</em> or more items and get <em>Rs.{$dre_cart_amount|round|number_format:0:".":","}</em> off on your cart value';
$_rst['Flat_CartLevel_CC_BP'] = 'Add <em>{$dre_cart_buyCount}</em> or more items and get <em>{$dre_cart_percent}%</em> off on your cart value';
$_rst['Flat_CartLevel_CA_BA'] = 'Add items worth <em>Rs.{$dre_cart_minMore}</em> more to reach <em>Rs.{$dre_cart_buyAmount}</em> and get <em>Rs.{$dre_cart_amount|round|number_format:0:".":","}</em> off on your cart value';
$_rst['Flat_CartLevel_CA_BP'] = 'Add items worth <em>Rs.{$dre_cart_minMore}</em> more to reach <em>Rs.{$dre_cart_buyAmount}</em> and get <em>{$dre_cart_percent}%</em> off on your cart value';


//voucher cart level
/*$_rst['Voucher_Cart_CartLevel_CM'] = 'Congratulations! A <strong>Free Voucher</strong> has been added to your cart.';
$_rst['Voucher_Cart_CartLevel_CC_NM'] = 'Add <b>{$dre_cart_buyCount}</b> or more products to receive a <strong>Free Voucher</strong>';
$_rst['Voucher_Cart_CartLevel_CA_NM'] = 'Add products worth <b><span class="rupees">Rs. </span>{$dre_cart_buyAmount}</b> or more to receive a <strong>Free Voucher</strong>';
$_rst['MyMyntra_V_FNC'] = '';
$_rst['MyMyntra_V_FC'] = '<span class="redeemed-msg">Congratulations! You have received {$dre_displayText} for <strong>Rs.{$dre_amount}</strong> </span><span class="sub-msg">The Coupon <strong>{$dre_coupon_code}</strong> has been added to your Myntra Credits</span>';
$_rst['MyMyntra_V_TT_FNC'] = '<div><strong>Voucher Code: {$dre_code}</strong></div><div>You have won {$dre_displayText} with this order.</div>';
$_rst['MyMyntra_V_TT_FC'] = '<div><strong>Voucher Code: {$dre_code}</strong></div><div>You have received {$dre_displayText} for <span class="rupees">Rs. </span>{$dre_amount} with this order.</div>';
$_rst['MyMyntra_V_ERR_VCDMO'] = '<span class="error-msg">The Voucher is invalid since it is not associated with this particular order </span>';
*/

$_rst['Voucher_Cart_CartLevel_CM'] = 'Congratulations! <strong class="free-voucher-cart-link"> <i>Paris Calling </i> Gift Voucher</strong> has been added to your cart.';
$_rst['Voucher_Cart_CartLevel_CC_NM'] = 'Add <b>{$dre_cart_buyCount}</b> or more products to receive a <strong>Free Voucher</strong>';
$_rst['Voucher_Cart_CartLevel_CA_NM'] = 'Buy for <b><span class="rupees">Rs. </span>{$dre_cart_buyAmount}</b> or more to receive a <strong class="free-voucher-cart-link"><i>Paris Calling </i> Gift Voucher</strong>.' ;
$_rst['Voucher_Cart_CartLevel_CC_TT'] = 'Add {$dre_cart_buyCount} or more products and stand a chance to win a trip for 2 to Paris. Also win from 100 iPad 2, 300 iPod Nano, 5000 Timex watches and more. Assured gift for all.';
$_rst['Voucher_Cart_CartLevel_CA_TT'] = 'Shop for <span class="rupees">Rs. </span>{$dre_cart_buyAmount} or above and stand a chance to win a trip for 2 to Paris. Also win from 100 iPad 2, 300 iPod Nano, 5000 Timex watches and more. Assured gift for all.';
$_rst['Voucher_Cart_CartLevel_TT']    = 'This Offer is not valid on {$dre_cart_cart_excluded_brands_str} items';
$_rst['MyMyntra_V_FNC'] = '';
$_rst['MyMyntra_V_FC'] = '<span class="redeemed-msg">Congratulations! You have received {$dre_displayText} for <strong>Rs.{$dre_amount|round|number_format:0:".":","}</strong> </span><span class="sub-msg">The Coupon <strong><span style="color:red;">{$dre_coupon_code}</span></strong> has been added to your Mynt Credits</span>';
$_rst['MyMyntra_V_TT_FNC'] = '<div><strong>Voucher Code: {$dre_code}</strong></div><div>You have won {$dre_displayText} with this order.</div>';
$_rst['MyMyntra_V_TT_FC'] = '<div><strong>Voucher Code: {$dre_code}</strong></div><div>You have received {$dre_displayText} for <span class="rupees">Rs. </span>{$dre_amount|round|number_format:0:".":","} with this order.</div>';
$_rst['MyMyntra_V_ERR_VCDMO'] = '<span class="error-msg">The Voucher code is invalid or it is not associated with this particular order </span>';

$_rst['Cart_Free_Shipping_Msg'] = 'Buy for <b><span class="rupees">Rs. </span>{$free_shipping_amount|round|number_format:0:".":","}</b> or more and we shall make your Shipping <strong>FREE</strong>';


/*///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
// Adding IDs which will be used by refactored discount service.


//BuyCount_GetCount
$_rst['BuyCount_GetCount_PDP_Tooltip'] = 'Add atleast <em>{$dre_buyCount} {$dre_placeHolderText}</em> to the combo and get <em>{$dre_count}</em> of them absolutely free!';
$_rst['BuyCount_GetCount_Search'] = '(Add {$widgetdata.dre_buyCount} Get {$widgetdata.dre_count} Free)';
$_rst['BuyCount_GetCount_PDP'] = '(Add {$dre_buyCount} Get {$dre_count} Free)';
//kept null for now to avoid confusion
$_rst['BuyCount_GetCount_Cart'] = '';
$_rst['BuyCount_GetCount_Cart_CC'] = 'Congratulations! You have <em>{$productsSet.dre_totalCount} {$productsSet.dre_placeHolderText}</em> in your bag for <em>free</em>';
$_rst['BuyCount_GetCount_Cart_CNM'] = 'Add atleast <em>{$productsSet.dre_buyCount} {$productsSet.dre_placeHolderText}</em> to the combo and get <em>{$productsSet.dre_count}</em> of them absolutely free!';
$_rst['BuyCount_GetCount_Cart_Icon'] = '<span class="success-icon">&nbsp;</span>';
$_rst['BuyCount_GetCount_CartLevel_CM'] = '';
$_rst['BuyCount_GetCount_CartLevel_CC_TT'] = '';
$_rst['BuyCount_GetCount_CartLevel_CC_NM'] = '';


//Flat Discount with Amount Benefit
$_rst['NoCondition_GetAmount_PDP_Tooltip'] = 'Buy this item and get <em>{$rupeesymbol} {$dre_amount|round|number_format:0:".":","} </em> off';
$_rst['NoCondition_GetAmount_Search'] = '({$rupeesymbol}{$widgetdata.dre_amount|round|number_format:0:".":","} OFF)';
$_rst['NoCondition_GetAmount_PDP'] = '({$rupeesymbol}{$dre_amount|round|number_format:0:".":","} OFF)';
$_rst['NoCondition_GetAmount_Cart'] = '({$rs} {$product.dre_amount|round|number_format:0:".":","} Off)';
$_rst['NoCondition_GetAmount_Cart_Icon'] = '<span class="success-icon">&nbsp;</span>';
$_rst['NoCondition_GetAmount_Cart_Icon_L'] = '<span class="success-icon-large">&nbsp;</span>';
$_rst['NoCondition_GetAmount_Confirmation'] = '({$rs}{$product.dre_amount|round|number_format:0:".":","} OFF)';

//Flat Discount with Percent Benefit
$_rst['NoCondition_GetPercent_PDP_Tooltip'] = 'Buy this item and get <em>{$dre_percent}% </em> off';
$_rst['NoCondition_GetPercent_Search'] = '({$widgetdata.dre_percent}% OFF)';
$_rst['NoCondition_GetPercent_PDP'] = '({$dre_percent}% OFF)';
$_rst['NoCondition_GetPercent_Cart'] = '({$product.dre_percent}% Off)';
$_rst['NoCondition_GetPercent_Cart_Icon'] = '<span class="success-icon">&nbsp;</span>';
$_rst['NoCondition_GetPercent_Cart_Icon_L'] = '<span class="success-icon-large">&nbsp;</span>';
$_rst['NoCondition_GetPercent_Confirmation'] = '({$product.dre_percent}% OFF)';


//Buy Count Get Amount
$_rst['BuyCount_GetAmount_PDP_ToolTip'] = 'Buy at least <em>{$dre_buyCount} {$dre_placeHolderText}</em> and get <em><span class="rupees">Rs. </span>{$dre_amount|round|number_format:0:".":","}</em> off on the combo';
$_rst['BuyCount_GetAmount_Search'] = '<em>{$rupeesymbol} {$widgetdata.dre_amount|round|number_format:0:".":","} OFF *</em>';
$_rst['BuyCount_GetAmount_PDP'] = 'Special Combo Offer - save upto <strong><span class="rupees">Rs. </span>{$dre_amount|round|number_format:0:".":","}</strong>';
$_rst['BuyCount_GetAmount_Cart'] = '({$rs} {$product.discountAmount|round|number_format:0:".":","} Off)';
$_rst['BuyCount_GetAmount_Cart_CNM'] = 'Buy at least <em>{$productsSet.dre_buyCount} {$productsSet.dre_placeHolderText}</em> and get <em><span class="rupees">Rs. </span>{$productsSet.dre_amount|round|number_format:0:".":","} Off</em> on this combo';
$_rst['BuyCount_GetAmount_Confirmation'] = '({$rs}{$product.discount|round|number_format:0:".":","} OFF)';
$_rst['BuyCount_GetAmount_CartLevel_CM'] = 'Congratulations! You have got <em>Rs.{$dre_cart_amount|round|number_format:0:".":","}</em> off on your cart value';
$_rst['BuyCount_GetAmount_CartLevel_TT'] = 'This Offer is not valid on {$dre_cart_cart_excluded_brands_str} items';
$_rst['BuyCount_GetAmount_CartLevel_NM'] = 'Add <em>{$dre_cart_buyCount}</em> or more items and get <em>Rs.{$dre_cart_amount|round|number_format:0:".":","}</em> off on your cart value';

// Buy Count Get Percent
$_rst['BuyCount_GetPercent_PDP_ToolTip'] = 'Buy at least <em>{$dre_buyCount} {$dre_placeHolderText}</em> and get <em>{$dre_percent}%</em> off on the combo';
$_rst['BuyCount_GetPercent_Search'] = '<em>{$widgetdata.dre_percent}% OFF *</em>';
$_rst['BuyCount_GetPercent_PDP'] = '<strong>{$dre_percent}% Off</strong> when bought with Special Combo Offer';
$_rst['BuyCount_GetPercent_Cart'] = '({$product.dre_percent}% Off)';
$_rst['BuyCount_GetPercent_Cart_CNM'] = 'Buy at least <em>{$productsSet.dre_buyCount} {$productsSet.dre_placeHolderText}</em> and get <em>{$productsSet.dre_percent}% Off</em> on this combo';
$_rst['BuyCount_GetPercent_Confirmation'] = '({$product.dre_percent}% OFF)';
$_rst['BuyCount_GetPercent_CartLevel_CM'] = 'Congratulations! You have got <em>{$dre_cart_percent}%</em> off on your cart value';
$_rst['BuyCount_GetPercent_CartLevel_TT'] = 'This Offer is not valid on {$dre_cart_cart_excluded_brands_str} items';
$_rst['BuyCount_GetPercent_CartLevel_NM'] = 'Add <em>{$dre_cart_buyCount}</em> or more items and get <em>{$dre_cart_percent}%</em> off on your cart value';

// Buy Amount Get Amount
$_rst['BuyAmount_GetAmount_PDP_ToolTip'] = 'Buy <em>{$dre_placeHolderText}</em> worth at least <em><span class="rupees">Rs. </span>{$dre_buyAmount}</em> and get <em><span class="rupees">Rs. </span>{$dre_amount|round|number_format:0:".":","}</em> off on the combo';
$_rst['BuyAmount_GetAmount_Search'] = '<em>{$rupeesymbol} {$widgetdata.dre_amount|round|number_format:0:".":","} OFF *</em>';
$_rst['BuyAmount_GetAmount_PDP'] = 'Special Combo Offer - save upto <strong><span class="rupees">Rs. </span>{$dre_amount|round|number_format:0:".":","}</strong>';
$_rst['BuyAmount_GetAmount_Cart'] = '({$rs} {$product.discountAmount|round|number_format:0:".":","} Off)';
$_rst['BuyAmount_GetAmount_Cart_CNM'] = 'Buy <em>{$productsSet.dre_placeHolderText}</em> worth at least <em><span class="rupees">Rs. </span>{$productsSet.dre_buyAmount}</em> and get <em><span class="rupees">Rs. </span>{$productsSet.dre_amount|round|number_format:0:".":","} Off</em> on this combo';
$_rst['BuyAmount_GetAmount_Confirmation'] = '({$rs}{$product.discount|round|number_format:0:".":","} OFF)';
$_rst['BuyAmount_GetAmount_CartLevel_CM'] = 'Congratulations! You have got <em>Rs.{$dre_cart_amount|round|number_format:0:".":","}</em> off on your cart value';
$_rst['BuyAmount_GetAmount_CartLevel_TT'] = 'This Offer is not valid on {$dre_cart_cart_excluded_brands_str} items';
$_rst['BuyAmount_GetAmount_CartLevel_NM'] = 'Add items worth <em><span class="rupees">Rs. </span>{$dre_cart_minMore}</em> more to get <em><span class="rupees">Rs. </span>{$dre_cart_amount|round|number_format:0:".":","}</em> off on your cart value';

// Buy Amount Get Percent
$_rst['BuyAmount_GetPercent_PDP_ToolTip'] = 'Buy <em>{$dre_placeHolderText}</em> worth at least <em><span class="rupees">Rs. </span>{$dre_buyAmount}</em> and get <em>{$dre_percent}%</em> off on the combo';
$_rst['BuyAmount_GetPercent_Search'] = '<em>{$widgetdata.dre_percent}% OFF *</em>';
$_rst['BuyAmount_GetPercent_PDP'] = '<strong>{$dre_percent}% Off</strong> when bought with Special Combo Offer';
$_rst['BuyAmount_GetPercent_Cart'] = '({$product.dre_percent}% Off)';
$_rst['BuyAmount_GetPercent_Cart_CNM'] = 'Buy <em>{$productsSet.dre_placeHolderText}</em> worth at least <em><span class="rupees">Rs. </span>{$productsSet.dre_buyAmount}</em> and get <em>{$productsSet.dre_percent}% Off</em> on this combo';
$_rst['BuyAmount_GetPercent_Confirmation'] = '({$product.dre_percent}% OFF)';
$_rst['BuyAmount_GetPercent_CartLevel_CM'] = 'Congratulations! You have got <em>{$dre_cart_percent}%</em> off on your cart value';
$_rst['BuyAmount_GetPercent_CartLevel_TT'] = 'This Offer is not valid on {$dre_cart_cart_excluded_brands_str} items';
$_rst['BuyAmount_GetPercent_CartLevel_NM'] = 'Add items worth <em><span class="rupees">Rs. </span>{$dre_cart_minMore}</em> more to get <em>{$dre_cart_percent}%</em> off on your cart value';


// BuyCount Get Free Items
$_rst['BuyCount_GetFreeunlistedItem_PDP_Tooltip'] = 'Buy at least <em>{$dre_buyCount} {$dre_placeHolderText}</em> and get <em>{if $dre_count gt 1}{$dre_count}{/if} {$dre_itemName}</em> absolutely free!';
$_rst['BuyCount_GetFreeunlistedItem_MC_PDP_Tooltip'] ='Buy at least <em>{$dre_buyCount} {$dre_placeHolderText}</em> and get a free gift';
$_rst['BuyCount_GetFreeunlistedItem_Cart'] = 'Buy at least <em>{$productsSet.dre_buyCount} {$productsSet.dre_placeHolderText}</em> and get <em>{if $productsSet.dre_count gt 1}{$productsSet.dre_count}{/if} {$productsSet.dre_itemName}</em> absolutely free!';
$_rst['BuyCount_GetFreeunlistedItem_MC_Cart'] = 'Buy at least <em>{$productsSet.dre_buyCount} {$productsSet.dre_placeHolderText}</em> and get a free gift';
$_rst['BuyCount_GetFreeunlistedItem_CartLevel_NM'] = 'Add <em>{$dre_cart_minMore}</em> more items and receive a <em>{$dre_cart_itemName}</em> worth <em><span class="rupees">Rs. </span>{$dre_cart_itemPrice}</em> absolutely free {if $cartSlabDiscountMessage} <br /> This would replace the free gift present in your bag right now.{/if}';
$_rst['BuyCount_GetFreeunlistedItem_MC_CartLevel_NM'] = 'Add <em>{$dre_cart_minMore}</em> more items and choose a free item up to <em><span class="rupees">Rs. </span>{$dre_cart_freeItemsMaxWorth}</em> from our set of items {if $cartSlabDiscountMessage} <br /> This would replace the free gift present in your bag right now.{/if}';
$_rst['BuyCount_GetFreeunlistedItem_Search'] = '<div class=\'gift red-text right\'>Get <b>Free Gift</b>*<i>&nbsp;</i></div>';
$_rst['BuyCount_GetFreeunlistedItem_PDP'] = 'Get <b>Free Gift</b><i>&nbsp;</i>';
$_rst['BuyCount_GetFreeunlistedItem_Cart_Icon'] = '<span class="success-icon">&nbsp;</span>';
$_rst['BuyCount_GetFreeunlistedItem_CartLevel_CM'] = '<span class="success-icon"></span> <span class="success" style="font-size:14px;">Congratulations! <em>{$dre_cart_itemName}</em> worth <em><span class="rupees">Rs. </span>{$dre_cart_itemPrice}</em> has been added to your bag.</span>';
$_rst['BuyCount_GetFreeunlistedItem_MC_CartLevel_CM'] = '<span class="success-icon"></span> <span class="success" style="font-size:14px;">Congratulations! Your free item has been added to your bag.</span><br /><div class="gray" style="margin-top:8px; display:inline;">Would you like to <a freecartc="1" discountid="{$dre_cart_discountId}" min="1" ctype="c" isconditionmet="true" class="combo-completion-btn " style="margin:0px;">change your free gift</a>?</div>';
$_rst['BuyCount_GetFreeunlistedItem_CartLevel_TT'] = 'This Offer is not valid on {$dre_cart_cart_excluded_brands_str} items';




// BuyAmount Get Free Items
$_rst['BuyAmount_GetFreeunlistedItem_PDP_Tooltip'] = 'Buy <em>{$dre_placeHolderText}</em> worth at least <em><span class="rupees">Rs. </span>{$dre_buyAmount}</em> and get <em>{if $dre_count gt 1}{$dre_count}{/if} {$dre_itemName}</em> absolutely free!';
$_rst['BuyAmount_GetFreeunlistedItem_MC_PDP_Tooltip'] = 'Buy <em>{$dre_placeHolderText}</em> worth at least <em><span class="rupees">Rs. </span>{$dre_buyAmount}</em> and get a free gift';
$_rst['BuyAmount_GetFreeunlistedItem_Cart'] = 'Buy <em>{$productsSet.dre_placeHolderText}</em> worth at least <em><span class="rupees">Rs. </span>{$productsSet.dre_buyAmount}</em> and get <em>{if $productsSet.dre_count gt 1}{$productsSet.dre_count}{/if} {$productsSet.dre_itemName}</em> absolutely free!';
$_rst['BuyAmount_GetFreeunlistedItem_MC__Cart'] =  'Buy <em>{$productsSet.dre_placeHolderText}</em> worth at least <em><span class="rupees">Rs. </span>{$productsSet.dre_buyAmount}</em> and get a free gift';
$_rst['BuyAmount_GetFreeunlistedItem_CartLevel_NM'] = 'Add items worth <em><span class="rupees">Rs. </span>{$dre_cart_minMore}</em> more to receive a <em>{$dre_cart_itemName}</em> worth <em><span class="rupees">Rs. </span>{$dre_cart_itemPrice}</em> absolutely free {if $cartSlabDiscountMessage} <br /> This would replace the free gift present in your bag right now.{/if}';
$_rst['BuyAmount_GetFreeunlistedItem_MC_CartLevel_NM'] = 'Add items worth <em><span class="rupees">Rs. </span>{$dre_cart_minMore}</em> more to choose a free item up to <em><span class="rupees">Rs. </span>{$dre_cart_freeItemsMaxWorth}</em> from our set of items {if $cartSlabDiscountMessage} <br /> This would replace the free gift present in your bag right now.{/if}';
$_rst['BuyAmount_GetFreeunlistedItem_Search'] = '<div class=\'gift red-text right\'>Get <b>Free Gift</b>*<i>&nbsp;</i></div>';
$_rst['BuyAmount_GetFreeunlistedItem_PDP'] = 'Get <b>Free Gift</b><i>&nbsp;</i>';
$_rst['BuyAmount_GetFreeunlistedItem_Cart_Icon'] = '<span class="success-icon">&nbsp;</span>';
$_rst['BuyAmount_GetFreeunlistedItem_CartLevel_CM'] = '<span class="success-icon"></span> <span class="success" style="font-size:14px;">Congratulations! <em>{$dre_cart_itemName}</em> worth <em><span class="rupees">Rs. </span>{$dre_cart_itemPrice}</em> has been added to your bag.</span>';
$_rst['BuyAmount_GetFreeunlistedItem_MC_CartLevel_CM'] = '<span class="success-icon"></span> <span class="success" style="font-size:14px;">Congratulations! Your free item has been added to your bag.</span><br /><div class="gray" style="margin-top:8px; display:inline;">Would you like to <a freecartc="1" discountid="{$dre_cart_discountId}" min="1" ctype="c" isconditionmet="true" class="combo-completion-btn " style="margin:0px;">change your free gift</a>?</div>';
$_rst['BuyAmount_GetFreeunlistedItem_CartLevel_TT'] = 'This Offer is not valid on {$dre_cart_cart_excluded_brands_str} items';



// Flat Get Free Items
$_rst['NoCondition_GetFreeunlistedItem_PDP_Tooltip'] = 'Buy this and get <em>{if $dre_count gt 1}{$dre_count}{/if} {$dre_itemName}</em> absolutely free!';
$_rst['NoCondition_GetFreeunlistedItem_MC_PDP_Tooltip'] = 'Buy this and get afree gift';
$_rst['NoCondition_GetFreeunlistedItem_Cart'] = 'Your free product has been added to the Cart!';
$_rst['NoCondition_GetFreeunlistedItem_MC__Cart'] = 'Your free product has been added to the Cart!';
$_rst['NoCondition_GetFreeunlistedItem_Search'] = '<div class=\'gift red-text right\'>Get <b>Free Gift</b><i>&nbsp;</i></div>';
$_rst['NoCondition_GetFreeunlistedItem_PDP'] = 'Get <b>Free Gift</b><i>&nbsp;</i>';
$_rst['NoCondition_GetFreeunlistedItem_Cart_Icon'] = '<span class="success-icon">&nbsp;</span>';
$_rst['NoCondition_GetFreeunlistedItem_CartLevel_CM'] = '<span class="success-icon"></span> <span class="success" style="font-size:14px;">Congratulations! <em>{$dre_cart_itemName}</em> worth <em><span class="rupees">Rs. </span>{$dre_cart_itemPrice}</em> has been added to your bag.</span>';
$_rst['NoCondition_GetFreeunlistedItem_MC_CartLevel_CM'] = '<span class="success-icon"></span> <span class="success" style="font-size:14px;">Congratulations! Your free item has been added to your bag.</span><br /><div class="gray" style="margin-top:8px; display:inline;">Would you like to <a freecartc="1" discountid="{$dre_cart_discountId}" min="1" ctype="c" isconditionmet="true" class="combo-completion-btn " style="margin:0px;">change your free gift</a>?</div>';
$_rst['NoCondition_GetFreeunlistedItem_CartLevel_TT'] = 'This Offer is not valid on {$dre_cart_cart_excluded_brands_str} items';


?>
