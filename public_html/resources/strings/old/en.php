<?php

//Discount related display strings

$_rst['BuyX_GetY_PDP_Tooltip'] = 'Buy atleast <em>{$dre_buyCount} {$dre_placeHolderText}</em> and get least valued <em>{$dre_count}</em> each absolutely free!';
$_rst['BuyX_GetY_Search'] = '(Buy {$widgetdata.dre_buyCount} Get {$widgetdata.dre_count})';
$_rst['BuyX_GetY_PDP'] = '(Buy {$dre_buyCount} Get {$dre_count})';
$_rst['BuyX_GetY_Cart'] = '(Buy {$product.dre_buyCount} Get {$product.dre_count})';
$_rst['BuyX_GetY_Cart_CC'] = 'Buy at least <em>{$productsSet.dre_buyCount} {$productsSet.dre_placeHolderText}</em> and get least valued <em>{$productsSet.dre_count}</em> each absolutely free!';
$_rst['BuyX_GetY_Cart_Icon'] = '';
$_rst['BuyX_GetY_CartLevel_CM'] = 'Congratulations! <em>{$dre_cart_count} items</em> have been added to your cart for free';
$_rst['BuyX_GetY_CartLevel_CC_TT'] = 'Add <em>{$dre_cart_buyCount}</em> or more items and get least valued <em>{$dre_count}</em> each absolutely free!';
$_rst['BuyX_GetY_CartLevel_CC_NM'] = 'Add <em>{$dre_cart_buyCount}</em> or more items and get least valued <em>{$dre_count}</em> each absolutely free!';



//Free Items
$_rst['Free_Item_PDP_Tooltip_CC'] = 'Buy atleast <i>{$dre_buyCount} No(s).</i> of <strong>{$dre_placeHolderText}</strong> <b>(Select Products Only)</b> and get <i>{$dre_count} No(s).</i> <strong>{$dre_itemName}</strong> worth <em>Rs. {$dre_itemPrice}</em> absolutely free!';
$_rst['Free_Item_PDP_Tooltip_CA'] = 'Buy <strong>{$dre_placeHolderText}</strong> worth <i>Rs. {$dre_buyAmount}</i> <b>(Select Products Only)</b> and get <i>{$dre_count} No(s).</i> <strong>{$dre_itemName}</strong> worth <em>Rs. {$dre_itemPrice}</em> absolutely free!';
$_rst['Free_Item_PDP_Tooltip'] = 'Buy this and get <i>{$dre_count} No(s).</i> <strong>{$dre_itemName}</strong> worth <em>Rs. {$dre_itemPrice}</em> absolutely free!';
$_rst['Free_Item_Search'] = '<div class=\'gift right\'>Get <b>Free Gift</b><i>&nbsp;</i></div>';
$_rst['Free_Item_Search_C'] = '<div class=\'gift right\'>Get <b>Free Gift</b>*<i>&nbsp;</i></div>';
$_rst['Free_Item_PDP'] = 'Get <b>Free Gift</b><i>&nbsp;</i>';
$_rst['Free_Item_PDP_C'] = 'Get <b>Free Gift</b>*<i>&nbsp;</i>';
$_rst['Free_Item_Cart_CA'] = 'Buy <strong>{$productsSet.dre_placeHolderText}</strong> worth <i>Rs. {$productsSet.dre_minMore}</i> <b>(Select Products Only)</b> and get <i>{$productsSet.dre_count} No(s).</i> <strong>{$productsSet.dre_itemName}</strong> worth <em>Rs. {$productsSet.dre_itemPrice}</em> absolutely free!';
$_rst['Free_Item_Cart_CC'] = 'Buy <i>{$productsSet.dre_minMore} No(s).</i> more <strong>{$productsSet.dre_placeHolderText}</strong> <b>(Select Products Only)</b> and get <i>{$productsSet.dre_count} No(s).</i> <strong>{$productsSet.dre_itemName}</strong> worth <em>Rs. {$productsSet.dre_itemPrice}</em> absolutely free!';
$_rst['Free_Item_Cart'] = 'Your free product has been added to the Cart!';
$_rst['Free_Item_Cart_Icon'] = '<span class="sploff-icon">&nbsp;</span>';
$_rst['Free_Item_CartLevel_CM'] = 'Congratulations! <strong>{$dre_cart_itemName}</strong> has been added to your cart';
$_rst['Free_Item_CartLevel_CA_NM'] = 'Add products worth <b>Rs. {$dre_cart_buyAmount}</b> or more to receive a <strong>{$dre_cart_itemName}</strong> absolutely FREE';
$_rst['Free_Item_CartLevel_CC_NM'] = 'Add <b>{$dre_cart_buyCount}</b> or more products to receive a <strong>{$dre_cart_itemName}</strong> absolutely FREE';

//Flat Discount
$_rst['Flat_PDP_Tooltip_Percent'] = 'Buy this item and get <em>{$dre_percent}% </em> off';
$_rst['Flat_PDP_Tooltip_Amount'] = 'Buy this item and get <em>Rs. {$dre_amount|round|number_format:0:".":","} </em> off';
$_rst['Flat_Search_Percent'] = '<div class=\'sploff right\'><b>{$widgetdata.dre_percent}%</b> Off <i>&nbsp;</i></div>';
$_rst['Flat_Search_Amount'] = '<div class=\'sploff right\'><b>Rs.{$widgetdata.dre_amount|round|number_format:0:".":","}</b> Off <i>&nbsp;</i></div>';
$_rst['Flat_PDP_Percent'] = '<b>{$dre_percent}%</b> Off <i>&nbsp;</i>';
$_rst['Flat_PDP_Amount'] = '<b>Rs.{$dre_amount|round|number_format:0:".":","}</b> Off <i>&nbsp;</i>';
$_rst['Flat_Cart_P'] = 'You save <em> Rs. {$product.dre_discountAmount|round|number_format:0:".":","}</em> per item';
$_rst['Flat_Cart_A'] = 'You save <em> Rs. {$product.dre_discountAmount|round|number_format:0:".":","}</em> per item';
$_rst['Flat_Cart_Icon'] = '<span class="sploff-icon">&nbsp;</span>';
$_rst['Flat_Cart_Icon_L'] = '<span class="sploff-large-icon">&nbsp;</span>';

//Flat Conditional
$_rst['FC_Cart_Count_Amount'] = 'Buy at least <i>{$productsSet.dre_buyCount}</i> <strong>{$productsSet.dre_placeHolderText}</strong> and get <em>Rs. {$productsSet.dre_amount|round|number_format:0:".":","} Off</em> on this combo';
$_rst['FC_Cart_Count_Percent'] = 'Buy at least <i>{$productsSet.dre_buyCount}</i> <strong>{$productsSet.dre_placeHolderText}</strong> and get <em>{$productsSet.dre_percent}% Off</em> on this combo';
$_rst['FC_Cart_Amount_Amount'] = 'Buy <strong>{$productsSet.dre_placeHolderText}</strong> worth at least <i>Rs. {$productsSet.dre_buyAmount|round|number_format:0:".":","}</i> and get <em>Rs. {$productsSet.dre_amount|round|number_format:0:".":","} Off</em> on this combo';
$_rst['FC_Cart_Amount_Percent'] = 'Buy <strong>{$productsSet.dre_placeHolderText}</strong> worth at least <i>Rs. {$productsSet.dre_buyAmount|round|number_format:0:".":","}</i> and get <em>{$productsSet.dre_percent}% Off</em> on this combo';
$_rst['FC_PDP_ToolTip_Count_Amount'] = 'Buy at least <i>{$dre_buyCount}</i> <strong>{$dre_placeHolderText}</strong> and get <em>Rs. {$dre_amount|round|number_format:0:".":","}</em> off on the combo';
$_rst['FC_PDP_ToolTip_Count_Percent'] = 'Buy at least <i>{$dre_buyCount}</i> <strong>{$dre_placeHolderText}</strong> and get <em>{$dre_percent}%</em> off on the combo';
$_rst['FC_PDP_ToolTip_Amount_Amount'] = 'Buy <strong>{$dre_placeHolderText}</strong> worth at least <i>Rs. {$dre_buyAmount|round|number_format:0:".":","}</i> and get <em>Rs. {$dre_amount|round|number_format:0:".":","}</em> off on the combo';
$_rst['FC_PDP_ToolTip_Amount_Percent'] = 'Buy <strong>{$dre_placeHolderText}</strong> worth at least <i>Rs. {$dre_buyAmount|round|number_format:0:".":","}</i> and get <em>{$dre_percent}%</em> off on the combo';
$_rst['FC_Search_Percent'] = '<div class=\'sploff right\'><b>{$widgetdata.dre_percent}%</b> Off *<i>&nbsp;</i></div>';
$_rst['FC_Search_Amount'] = '<div class=\'sploff right\'><b>Rs. {$widgetdata.dre_amount|round|number_format:0:".":","}</b> Off *<i>&nbsp;</i></div>';
$_rst['FC_PDP_Percent'] = '<strong>{$dre_percent}% Off</strong> when bought with Special Combo Offer';
$_rst['FC_PDP_Amount'] = 'Special Combo Offer - save upto <strong>Rs. {$dre_amount|round|number_format:0:".":","}</strong>';
$_rst['FC_Cart_CM_Amount'] = 'You save <em> Rs. {$product.dre_discountAmount|number_format:2:".":","}</em> per item';
$_rst['FC_Cart_CM_Percent'] = 'You save <em> Rs. {$product.dre_discountAmount|number_format:2:".":","}</em> per item';
$_rst['Flat_CartLevel_CM_BA'] = 'Congratulations! You have got <b>{$rs}{$dre_cart_amount|round|number_format:0:".":","}</b> off on your cart value';
$_rst['Flat_CartLevel_CM_BP'] = 'Congratulations! You have got <b>{$dre_cart_percent}%</b> off on your cart value';
$_rst['Flat_CartLevel_CC_TT'] = '';
$_rst['Flat_CartLevel_CA_TT'] = '';
$_rst['Flat_CartLevel_TT']    = 'This Offer is not valid on {$dre_cart_cart_excluded_brands_str} items';
$_rst['Flat_CartLevel_CC_BA'] = 'Add <b>{$dre_cart_buyCount}</b> or more products and get <em>{$rs}{$dre_cart_amount}</em> off on your cart value';
$_rst['Flat_CartLevel_CC_BP'] = 'Add <b>{$dre_cart_buyCount}</b> or more products and get <em>{$dre_cart_percent}%</em> off on your cart value';
$_rst['Flat_CartLevel_CA_BA'] = 'Add products worth <b>Rs.{$dre_cart_buyAmount|round|number_format:0:".":","}</b> or more and get <b>{$rs}{$dre_cart_amount|round|number_format:0:".":","}</b> off on your cart value';
$_rst['Flat_CartLevel_CA_BP'] = 'Add products worth <b>Rs.{$dre_cart_buyAmount|round|number_format:0:".":","}</b> or more and get <b>{$dre_cart_percent}%</b> off on your cart value';

//voucher cart level
/*$_rst['Voucher_Cart_CartLevel_CM'] = 'Congratulations! A <strong>Free Voucher</strong> has been added to your cart.';
$_rst['Voucher_Cart_CartLevel_CC_NM'] = 'Add <b>{$dre_cart_buyCount}</b> or more products to receive a <strong>Free Voucher</strong>';
$_rst['Voucher_Cart_CartLevel_CA_NM'] = 'Add products worth <b>Rs. {$dre_cart_buyAmount}</b> or more to receive a <strong>Free Voucher</strong>';
$_rst['MyMyntra_V_FNC'] = '';
$_rst['MyMyntra_V_FC'] = '<span class="redeemed-msg">Congratulations! You have received {$dre_displayText} for <strong>Rs.{$dre_amount}</strong> </span><span class="sub-msg">The Coupon <strong>{$dre_coupon_code}</strong> has been added to your Myntra Credits</span>';
$_rst['MyMyntra_V_TT_FNC'] = '<div><strong>Voucher Code: {$dre_code}</strong></div><div>You have won {$dre_displayText} with this order.</div>';
$_rst['MyMyntra_V_TT_FC'] = '<div><strong>Voucher Code: {$dre_code}</strong></div><div>You have received {$dre_displayText} for Rs. {$dre_amount} with this order.</div>';
$_rst['MyMyntra_V_ERR_VCDMO'] = '<span class="error-msg">The Voucher is invalid since it is not associated with this particular order </span>';
*/

$_rst['Voucher_Cart_CartLevel_CM'] = 'Congratulations! <strong class="free-voucher-cart-link"> <i>Paris Calling </i> Gift Voucher</strong> has been added to your cart.';
$_rst['Voucher_Cart_CartLevel_CC_NM'] = 'Add <b>{$dre_cart_buyCount}</b> or more products to receive a <strong>Free Voucher</strong>';
$_rst['Voucher_Cart_CartLevel_CA_NM'] = 'Buy for <b>Rs. {$dre_cart_buyAmount}</b> or more to receive a <strong class="free-voucher-cart-link"><i>Paris Calling </i> Gift Voucher</strong>.' ;
$_rst['Voucher_Cart_CartLevel_CC_TT'] = 'Add {$dre_cart_buyCount} or more products and stand a chance to win a trip for 2 to Paris. Also win from 100 iPad 2, 300 iPod Nano, 5000 Timex watches and more. Assured gift for all.';
$_rst['Voucher_Cart_CartLevel_CA_TT'] = 'Shop for Rs. {$dre_cart_buyAmount} or above and stand a chance to win a trip for 2 to Paris. Also win from 100 iPad 2, 300 iPod Nano, 5000 Timex watches and more. Assured gift for all.';
$_rst['Voucher_Cart_CartLevel_TT']    = 'This Offer is not valid on {$dre_cart_cart_excluded_brands_str} items';
$_rst['MyMyntra_V_FNC'] = '';
$_rst['MyMyntra_V_FC'] = '<span class="redeemed-msg">Congratulations! You have received {$dre_displayText} for <strong>Rs.{$dre_amount}</strong> </span><span class="sub-msg">The Coupon <strong><span style="color:red;">{$dre_coupon_code}</span></strong> has been added to your Mynt Credits</span>';
$_rst['MyMyntra_V_TT_FNC'] = '<div><strong>Voucher Code: {$dre_code}</strong></div><div>You have won {$dre_displayText} with this order.</div>';
$_rst['MyMyntra_V_TT_FC'] = '<div><strong>Voucher Code: {$dre_code}</strong></div><div>You have received {$dre_displayText} for Rs. {$dre_amount} with this order.</div>';
$_rst['MyMyntra_V_ERR_VCDMO'] = '<span class="error-msg">The Voucher code is invalid or it is not associated with this particular order </span>';

$_rst['Cart_Free_Shipping_Msg'] = 'Buy for <b>Rs. {$free_shipping_amount}</b> or more and we shall make your Shipping <strong>FREE</strong>';

?>
