<?php


include_once("./auth.php");

include_once ("$xcart_dir/include/class/mcart/class.MCart.php");
include_once ("$xcart_dir/include/class/mcart/class.MCartFactory.php");
include_once ("$xcart_dir/include/class/mcart/class.MCartUtils.php");
include_once ("$xcart_dir/include/class/search/SearchProducts.php");
include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
include_once($xcart_dir."/include/func/func.mkcore.php");
require_once($xcart_dir."/include/func/func.utilities.php");
require_once($xcart_dir."/include/class/widget/class.static.functions.widget.php");
include_once($xcart_dir."/include/class/search/UserInterestCalculator.php");
include_once($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");
include_once($xcart_dir."/include/class/notify/class.notify.php");
$publickey  = "6Le_oroSAAAAANGCD-foIE8DPo6zwcU1FaW3GiIX";
$privatekey = "6Le_oroSAAAAAKuArW6iPxhOlk0QVJMwWOTGVQnt";
include_once($xcart_dir."/include/func/func.utilities.php");
require_once($xcart_dir."/tracking/trackingUtil.php");
require_once($xcart_dir."/modules/discount/DiscountEngine.php");
include_once($xcart_dir."/Cache/Cache.php");

use revenue\discounts\views\ComboOverlayView; 


$shownewui= $_MABTestObject->getUserSegment("productui");
$productuiABparam=$shownewui;

$smarty->assign("shownewui", $shownewui);
$smarty->assign("productuiABparam", $productuiABparam);



/** Starts here ****/

if(isset($HTTP_GET_VARS["sid"]) ) {
    $styleId=filter_input(INPUT_GET,"sid", FILTER_SANITIZE_NUMBER_INT);
}

$styleId=sanitize_int($styleId);

$params = array("sid"=>$styleId);

$view = new ComboOverlayView('pdp', $params);
$view->display();

?>
