<?php
include_once("./auth.php");
include_once("$xcart_dir/include/class/oms/ReturnsStatusProcessor.php");
include_once($xcart_dir."/include/class/widget/class.widget.keyvaluepair.php");

$label_for = $_GET['for'];
$pages = array();
if ($label_for == 'addresslabel') {
	$return_address_row = func_query_first("SELECT * FROM mk_widget_key_value_pairs where `key` = 'returnAddress'");
	$return_address = $return_address_row['value'];
	$html = '<html>';
	$html .= '<head>';
	$html .= '<title>Address Label</title>';
	$html .= "<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />";
	$html .= '</head>';
	$html .= '<body style="font-size: 13px; width:95%; margin: 0 auto;">';
	$html .= '<p>To,<br>'.$return_address.'</p>';
	$html .= '</body>';
	$html .= '</html>';
	$pages[] = $html;
	$filename = "addresslabel.pdf";
} elseif ($label_for == 'returndetails') {		
	$returnid = filter_var($_GET['id'], FILTER_SANITIZE_STRING);
	
	$processor = new ReturnsStatusProcessor();
	$return_details = $processor->getReturnDetails($returnid);
	if($return_details['status']){
		$return_details = $return_details['return_details'];		
	}else{
		echo "$returnid does not exist";
		exit;
	}
	global $smarty;
	$callNumber = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
	$smarty->assign("orderid", $return_details['groupid']);
	$smarty->assign("returnid", $return_details['returnid']);
	$smarty->assign("returnmode", $return_details['return_mode']);
	$smarty->assign("phonenumber", $callNumber);
	$filename = "returnsform.pdf";
	//func_display("mymyntra/return-form.tpl",$smarty);exit;
	if($skin === "skin2") {
        $pages[] = $smarty->fetch("my/return-form.tpl");
    } else {
        $pages[] = func_display("mymyntra/return-form.tpl", $smarty,false);
    }
}

require_once("$xcart_dir/include/mpdf/mpdf.php");

$mpdf = new mPDF('en-GB','A4','','',5,5,10,0,5,0);
$mpdf->WriteHTML($pages[0]);
$mpdf->Output($filename,"D");
exit;
/*$pdf=new HTML2FPDF();
foreach( $pages as $page) {
	$pdf->AddPage();
	$pdf->WriteHTML($page);	
}
$pdf->Output($filename,"D");
exit;*/
?>