<?php
use abtest\MABTest;
use FeatureGateKeyValuePairs;
include_once($xcart_dir."/include/class/class.mymyntra.php");

global $telesales;
global $skin;
if ($telesales || $skin != 'skin2') {
	$smarty->assign('isWebEngageFeedbackEnabled', false);
	$smarty->assign('isWebEngageNotificationsEnabled', false);
	return;
}

$enabledPages = array();
$smarty->assign('navId', $nav_id);
$smarty->assign('utm_campaign', filter_input(INPUT_GET, 'utm_campaign', FILTER_SANITIZE_STRING));

// WebEngage Feedback module
$isWebEngageFeedbackEnabled = (MABTest::getInstance()->getUserSegment('WebEngageFeedback') == 'test');
if ($isWebEngageFeedbackEnabled) {
	$enabledPages = FeatureGateKeyValuePairs::getStrArray('WebEngage.feedback.enabledPages');
	$isWebEngageFeedbackEnabled = in_array($pageName, $enabledPages, true);
}
if ($isWebEngageFeedbackEnabled) {
	$smarty->assign('webEngageFeedbackRegexpJSON', FeatureGateKeyValuePairs::getFeatureGateValueForKey('WebEngage.feedback.regexpJSON'));
}
$smarty->assign('pageName', $pageName);
$smarty->assign('isWebEngageFeedbackEnabled', $isWebEngageFeedbackEnabled);

// WebEngage Notifications module
$isWebEngageNotificationsEnabled = (MABTest::getInstance()->getUserSegment('WebEngageNotifications') == 'test' && FeatureGateKeyValuePairs::getBoolean('WebEngage.notifications.enabled'));
$smarty->assign('isWebEngageNotificationsEnabled', $isWebEngageNotificationsEnabled);

if ($login && ($isWebEngageFeedbackEnabled || $isWebEngageNotificationsEnabled)) {
	$mymyntra = new \MyMyntra(mysql_real_escape_string($login));
	$userProfileData = $mymyntra->getUserProfileData();
	$smarty->assign('userFirstName', trim($userProfileData['firstname']));
	$smarty->assign('userLastName', trim($userProfileData['lastname']));
	$smarty->assign('userMobile', $userProfileData['mobile']);
	$smarty->assign('userGender', $userProfileData['gender']);
	$smarty->assign('userDOB', $userProfileData['DOB']);
	$XCART_SESSION_VARS["userfirstname"] = trim($userProfileData['firstname']);
	$XCART_SESSION_VARS["userlastname"] = trim($userProfileData['lastname']);
	$XCART_SESSION_VARS["mobile"] = $userProfileData['mobile'];
	$XCART_SESSION_VARS["gender"] = $userProfileData['gender'];
	$XCART_SESSION_VARS["DOB"] = $userProfileData['DOB'];
}

?>