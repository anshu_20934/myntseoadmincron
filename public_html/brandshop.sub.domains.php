<?php
/* add new brandshop with
 * following subdomain settings
 */
$domain_position = strpos($_SERVER['SERVER_NAME'],'www.');
//since SSL termination is happening at LB need to check this
//nginx will set this variable while proxy forwarding to haproxy
if($_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'){
    $_SERVER['HTTPS'] = 'on';
}


/* host and base url setting for
 * subdomains based on http or https protocol  
 */
if($domain_position === false){
    
    if(!empty($_SERVER['HTTPS'])){
        $xcart_https_host = $_SERVER['SERVER_NAME'];
        $orgbaseurl = $baseurl = "https://" . $xcart_http_host;

    } else {
        $xcart_http_host = $_SERVER['SERVER_NAME'];
        $orgbaseurl = $baseurl = $http_location = "http://" . $xcart_http_host;
    }
	
    $baseurl_length = strlen($baseurl);//to append '/' or not
    if(strpos($baseurl,'/',$baseurl_length-1) === false)
    	$baseurl .= '/';
	
}
?>
