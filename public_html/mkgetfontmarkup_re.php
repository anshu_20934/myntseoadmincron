<?php
require "./auth.php";
include_once("./include/func/func.mkcore.php");

$smarty->assign("location", $location);  
$offset=$_GET['offset'];
$limit = 16;

//get orientation id from customization arrary
$windowid = $_GET["windowid"];
$thisCustomizationArray = $XCART_SESSION_VARS["customizationArrayEx"][$windowid];

//check for text limitation customization
$curren_selected_area = $thisCustomizationArray['curr_selected_area'];
$curren_selected_orientation = $thisCustomizationArray['areas'][$curren_selected_area]['curr_selected_orientation'];
$orientationid = $thisCustomizationArray['areas'][$curren_selected_area]['orientations'][$curren_selected_orientation]['orientationid'];
$isTextLimitation = $thisCustomizationArray['areas'][$curren_selected_area]['orientations'][$curren_selected_orientation]['textLimitation'];

if($isTextLimitation){
	$query = "select f.name,f.image_name,f.id as font_id from mk_text_font_detail d,mk_fonts f
				where f.id=d.font_id and d.orientation_id='{$orientationid}' group by d.font_id LIMIT $limit OFFSET $offset";	
	$result = db_query($query);
	$fonts=array();
	if ($result){
		$i=0;
		while($row=db_fetch_row($result)){
			$fonts[$i] = $row;
			$i++;
		}
	}
	$fontNos = count($fonts);
} else {
	$fonts = func_getFontDetails(16, $offset);
	$fontNos = func_getFontNos();
}

$fromIndex = $offset+1;
$toIndex = $offset+17;
if($toIndex > $fontNos) {
	$toIndex = $fontNos;
}
$prevOffset = $offset-16;
$nextOffset = $offset+16;

$markup = '<div class="overlay" style="display:block;_position:absolute;_width:1300px;_height:1150px;">&nbsp;</div>';
$markup .= '<div id="selection"> ';
$markup .= '<div class="wrapper"> ';
$markup .= '<div class="head"><p><a href="javascript: hideFontPickerDiv()"><img src="'.$cdn_base.'/skin1/mkimages/click-to-close.gif"></a>Please select a Font</p>';
$markup .= '</div>';
$markup .= '<div class="body" id="fontPopupBody"> ';
$markup .= '<div class="serialfonts">';
$markup .= '<ul>';
for($i=0;$i<count($fonts);$i++){
	$font = $fonts[$i];
	if($isTextLimitation)
		$markup .= '<li><a href="javascript:setSelectedFontValuesForTextLimit(\''.$font[0].'\',\''.$font[1].'\','.$font[2].','.$orientationid.');"><img src="images/fonts/'.$font[1].'" ></a></li>';
	else
		$markup .= '<li><a href="javascript:setSelectedFontValues(\''.$font[0].'\',\''.$font[1].'\');"><img src="images/fonts/'.$font[1].'" ></a></li>';
}
$markup .= '</ul>';
$markup .= '<div class="clearall"></div>';
$markup .= '</div>';
$markup .= '<p> Showing '.$fromIndex.' to '.$toIndex.' of '.$fontNos.' | ';
if($fromIndex == 1) {
	$markup .= 'Previous | ';
}
else {
	$markup .= "<a href=\"javascript: populateFontMarkup(".$prevOffset.",'".$windowid."')\">Previous</a> | ";
}
if($toIndex == $fontNos) {
	$markup .= 'Next ';
}
else {
	$markup .= "<a href=\"javascript: populateFontMarkup(".$nextOffset.",'".$windowid."')\">Next</a>" ;
}
$markup .= '| <span class="mandatory">Note: Fonts will determine the size of the text </span></p>';
$markup .= '<p> <img id="font_image" src="images/fonts/blank.png">';
$markup .= '<input type="hidden" id="font_value" />';
$markup .= '<input type="hidden" id="font_id" />';
$markup .= '<input type="hidden" id="font_size" />';
$markup .= '</p><p> ';
$markup .= '<input class="submit" type="submit" name="submitButton" value="select" border="0" onclick="javascript: submitFontType();">';
$markup .= '</p>';
//$markup .= '<br>';
//$markup .= '<p><a href="#">Privacy Policy</a> | <a href="#">Legal</a> | <a href="#">Site Map</a></p>';
$markup .= '</div>';
$markup .= '<div class="foot"></div>';
$markup .= '</div>';
$markup .= '</div>';

echo($markup);
?>
