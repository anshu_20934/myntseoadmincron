<?php
if (isset($_GET['expressbuy']))
    define ('ONE_CLICK', true);
$GLOBALS['minPageName']='checkout-address';
define ('_CONTROLLER_', 'checkout-address');
require_once $_SERVER['DOCUMENT_ROOT'].'/auth.php';
include_once("$xcart_dir/include/cssjs.min.php");

/** Base Tracker for Atlas Instrumentation **/
require_once $xcart_dir."/include/class/instrumentation/BaseTracker.php";
$tracker = new BaseTracker(BaseTracker::CHECKOUT_PAGE_ADDRESS_VIEW);

if (empty($login)) {
    func_header_location($https_location . '/checkout-login.php');
    exit;
}
if(empty($_SERVER['HTTPS'])){
	//Page to be accessed only over http redirect him to https
	func_header_location($https_location .$_SERVER['REQUEST_URI'],false);
}
if($_SERVER['HTTP_HOST'] != $xcart_https_host){
	//redirect if someone opened with any other url
	func_header_location($https_location . $_SERVER['REQUEST_URI'],false);
}

$pageName = 'checkout';
include_once $xcart_dir."/webengage.php";

global $telesales;
if (!isset($_GET['change'])&&!$telesales) {
    $url = parse_url($_SERVER['HTTP_REFERER']);
    $selectedAddress = $XCART_SESSION_VARS['selected_address'];
    if (strtoupper($url['path'])!='/MKPAYMENTOPTIONS.PHP' && isset($selectedAddress) && $selectedAddress>0) {
       func_header_location($https_location . '/mkpaymentoptions.php?address='.$selectedAddress);
       exit;
    }
    $rows = func_select_query('mk_customer_address', array('login' => $login, 'default_address' => 1));
    foreach ($rows as $row) {
        $defaultAddress = $row['id'];
        func_header_location($https_location . '/mkpaymentoptions.php?address='.$defaultAddress);
        exit;
    }
}
require_once "$xcart_dir/cart_details.php";
if (empty($mCartItems)) {
    func_header_location($http_location . '/mkmycart.php');
    exit;
}

/** Fire Atlas Request **/
$tracker->fireRequest();

if (isset($_GET['expressbuy']))
    $smarty->assign('expressbuy','1');
else
    $smarty->assign('expressbuy','0');
$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setContextualPageData(AnalyticsBase::CHECKOUT_SHIPPING);
$analyticsObj->setChooseShippingAddressEventVars();
$analyticsObj->setTemplateVars($smarty);
$smarty->display('checkout/address.tpl', true);
