<?php
require_once "./auth.php";
include_once(HostConfig::$documentRoot."/include/class/instrumentation/BaseTracker.php");
$tracker = new BaseTracker(BaseTracker::SIGNIN);
$loginStartTime	 = microtime(true);
$loginlog = $logger_manager->getLogger('MYNTRALOGINLOGGER');

require_once("$xcart_dir/include/sessionstore/session_store_wrapper.php");
x_load('crypt','user');

function sendResponse() {
    global $msg, $http_location;
    $msg_txt = '';
    foreach ($msg as $key => $val) {
        if ($msg_txt) $msg_txt .= ';';
        $msg_txt .= $key . '=' . $val;
    }
    $msg_txt_enc = rawurlencode($msg_txt);
    echo <<<HTML
<!DOCTYPE html>
<html>
    <head></head>
    <body>
    <script>
    if (typeof top.postMessage !== 'undefined') {
        top.postMessage("{$msg_txt}", "{$http_location}");
    }
    else {
        location.href = "{$http_location}/xd.php?msg={$msg_txt_enc}";
    }
    </script>
    </body>
</html>
HTML;
    exit;
}
$userAuthManager = new user\UserAuthenticationManager();
$password =  $userAuthManager->generateNewTeleSalesPwd("surjith.meethal@myntra.com", 30);
echo $password."<br>";
$adapter = new user\dao\UserDAO();
$user = $adapter->getUserBasicDetails("surjith.meethal@myntra.com");
