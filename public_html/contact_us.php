<?php
require_once "./auth.php";
require_once \HostConfig::$documentRoot."/XPM/MAIL.php";
require_once \HostConfig::$documentRoot."/include/class/class.mail.template.resolver.php";
require_once \HostConfig::$documentRoot."/include/func/func.utilities.php";

use contactus\MContactUsIssueManager;
use contactus\MContactUsIssueInstanceManager;
// 301 redirect when url explicitely reads contact_us.php
//if(strpos($_SERVER['REQUEST_URI'],'contact_us.php' !== false)){
if(preg_match('/contact_us/', $_SERVER['REQUEST_URI'])){    
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: $http_location/contactus");
    exit();
}

$pageName = 'static';
include_once $xcart_dir."/webengage.php";
include_once $xcart_dir."/gtm.php";

// get order info for the customer
if($login != ''){
    require_once \HostConfig::$documentRoot."/include/func/func.mkcore.php";
    require_once \HostConfig::$documentRoot."/include/class/class.mymyntra.php";


    $mymyntra = new MyMyntra(mysql_real_escape_string($login));

    // We need to fetch item details, so passing 'true' as parameter.
    $orders = $mymyntra->getUserOrderHistory(true, -1);
    $orderItems = array();
    foreach ($orders as $orderId => $sorders) {
        
        $subOrders = $sorders['orders'];
        foreach ($subOrders as $subOrder) {
            foreach ($subOrder['items'] as $item) {
                $itemId = $item['itemid'];
                $orderItems["$orderId-$itemId"] = array(
                    'orderId' => $orderId,
                    'itemId' => $itemId,
                    'styleName' => $item['stylename'],
                    'styleId' => $item['product_style'],
                    'option' => $item['option'],
                    'amtPaid' => (float)$item['final_price_paid'],
                    'qty' => $item['quantity']
                );
            }
        }
    }
    
    $orderItemsJson = json_encode($orderItems);
    $smarty->assign('orderItemsJson', $orderItemsJson);
    $smarty->assign("orders", $orders);
}

$random = mt_rand();
$smarty->assign("random", $random);

define("CONTACTUS_UPLOAD_FILE_DIR",\HostConfig::$documentRoot.DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."contactus-customer-images".DIRECTORY_SEPARATOR);

$mContactUsIssueManager = new MContactUsIssueManager();
$mContactUsIssueInstanceManager = new MContactUsIssueInstanceManager();

// params to get all active issues with its fields
$issueID = null;
$active = true;

// get all active issues with their fields
$isuueDetail = $mContactUsIssueManager->getContactUsIssueWithField($issueID,$active);

// post submit contactus
if ($_SERVER['REQUEST_METHOD'] === 'POST'){

    // cheack token to avoid CSRF
    if(checkCSRF($_POST['_token'], "contact_us")){

        // take care of subsequent insertion on refresh
        if($XCART_SESSION_VARS['MyntraContactusSubmission'] == $_POST['timestamp']){

            $captchaError = false; //Adding a second level check for captcha
            $captchText = filter_input(INPUT_POST, 'captcha_text', FILTER_SANITIZE_STRING);
            $captchaValid = $XCART_SESSION_VARS['captchaValid'];
            if(empty($captchaValid)){
                $captchaError = true;
                $captchaErrorMessage = "Wrong captcha";
            }else{
                unset($XCART_SESSION_VARS['captchaValid']);
            }
            $inputError = false;
            
            // filter email to validate
            $email = $login;

            // filter other inputs to sanitize
            $filterOption = array(
                "contactus_subject" => FILTER_SANITIZE_STRING,
                "contactus_detail" => FILTER_SANITIZE_STRING,
                "issue_level_1" => FILTER_SANITIZE_NUMBER_INT,
                "issue_level_2" => FILTER_SANITIZE_NUMBER_INT,
                "issue_level_3" => FILTER_SANITIZE_NUMBER_INT,
            );

            $contactusCustomerInput = filter_input_array(INPUT_POST, $filterOption);

            $subject = $contactusCustomerInput['contactus_subject'];
            $detail = $contactusCustomerInput['contactus_detail'];
            $issueLevel_1_Id = $contactusCustomerInput['issue_level_1'];
            $issueLevel_2_Id = $contactusCustomerInput['issue_level_2'];
            $issueLevel_3_Id = $contactusCustomerInput['issue_level_3'];

            // filter fields array
            $fields = filter_input(INPUT_POST, "field", FILTER_SANITIZE_STRING, array("flags"=>FILTER_REQUIRE_ARRAY));

            //create file upload directory
            if(!file_exists(CONTACTUS_UPLOAD_FILE_DIR)){
                mkdir(CONTACTUS_UPLOAD_FILE_DIR,0777) ;
            }

            //upload file by validating total size and type of the file
            $fielsUploaded = array();
            $allowedMimeType = array('image/jpeg','image/jpg','image/png','image/gif','image/x-png','image/pjpeg');
            $maxFileSize = 2097152;//2MB
            $size = 0;
            
            if(!empty($HTTP_POST_FILES['contactus_file']['name'][0]) && is_array($HTTP_POST_FILES['contactus_file']['name'])){
                $filesExist = true;
            } else {
                $filesExist = false;
            }

            $typeError = false;
            $sizeError = false;
            $uploadError = false;

            if($filesExist){

                foreach ($HTTP_POST_FILES["contactus_file"]["error"] as $key => $error) {

                    if ($error == UPLOAD_ERR_OK) {

                        if(in_array($HTTP_POST_FILES["contactus_file"]["type"][$key] , $allowedMimeType)){

                            $size += $HTTP_POST_FILES["contactus_file"]["size"][$key];
                            $tmp_name = $HTTP_POST_FILES["contactus_file"]["tmp_name"][$key];
                            $name = $HTTP_POST_FILES["contactus_file"]["name"][$key];
                            $baseName = time()."_".basename($name);

                            if(move_uploaded_file($tmp_name, CONTACTUS_UPLOAD_FILE_DIR.$baseName)){
                                $fielsUploaded[] = CONTACTUS_UPLOAD_FILE_DIR.$baseName;
                            }

                        } else {
                            $typeError = true;
                            break;
                        }
                    } else {
                        $uploadError = true;
                        break;
                    }
                }
                if($size > $maxFileSize){
                    $sizeError = true;
                }
            }

            // capture issue and send mail on no file error (if file uploaded)
            if(!$captchaError && !$typeError && !$sizeError && !$inputError && !$uploadError){

                // capture leaf node issue, in case if there is no field exists for any issue,
                // we haave leaf issue to understand the problem
                $leafIssueId = (!empty($issueLevel_3_Id))? $issueLevel_3_Id : ((!empty($issueLevel_2_Id))? $issueLevel_2_Id : $issueLevel_1_Id ) ;

                // add instance data
                $instanceId = $mContactUsIssueInstanceManager->mContactUsDAO->insertContactUsIssueInstance($leafIssueId, $subject, $detail, $email);

                // add instance field data
                $mContactUsIssueInstanceManager->addContactUsIssueInstanceField($instanceId, $fields);

                // derive subject : <level1> : <level2> : <level3>
                if(empty($subject)){

                    // query on leaf node to get level 1, 2 and 3
                    $leafIsuueDetail = $mContactUsIssueManager->getContactUsIssueWithField($leafIssueId, $active=false, $fieldNeeded=false);
                    $leafIsuueDetail = $leafIsuueDetail['issueArray'];
                    $derivedSubject = $leafIsuueDetail[0]['issue_level_1']." : ".$leafIsuueDetail[0]['issue_level_2']." : ".$leafIsuueDetail[0]['issue_level_3'];
                    
                    // trim space and trim : from start and end in the order
                    $derivedSubject = trim($derivedSubject, ' :');

                    // send the mail to
                    $supportEmail = trim($leafIsuueDetail[0]['support_email']);
                    if(empty($supportEmail)){
                        $supportEmail = "support@myntra.com";    
                    }

                } else {
                    $derivedSubject = $subject;
                    
                    // in case of issue level 1 "other"
                    $supportEmail = "support@myntra.com";
                }

                // just to take care that XPM mailer does not fail on empty subject
                if(empty($derivedSubject)){
                    $derivedSubject = "No Subject";
                }

                // derive content : for mandatory fields as
                // <mandatory1 : val>
                // <mandatory2 : val>
                $content = "<strong>$email</strong>,<br/><p>Has provided following detail:</p><p>";
                if(is_array($fields) && !empty($fields)){
                    $fieldDetail = $mContactUsIssueInstanceManager->mContactUsDAO->getContactUsIssueFieldFromCSV(implode(',',array_keys($fields)));

                    foreach($fieldDetail as $field){
                        $content .= $field['field_label']." : <strong>".$fields[$field['issue_field_id']]."</strong><br/>";
                    }


                }
                $content .= "</p>";
                
                // basic params
                $to = $supportEmail;
                $cc = "myntramailarchive@gmail.com";
                // header and footer(if needed)
                /*$hdrTemplate = EmailTemplateResolver::getTemplate('header');
                $header = EmailTemplateResolver::replaceKeywordsWithValues($hdrTemplate['body'],null);

                $fooTemplate = EmailTemplateResolver::getTemplate('footer');
                $footer = EmailTemplateResolver::replaceKeywordsWithValues($fooTemplate['body'],null);*/

                // body
                if(!empty($detail)){
                    $content .= "<p>".$detail."</p>";
                    $content = $header.$content.$footer;

                } else {
                    $content .= "<p>No description from the client</p>";
                    $content = $header.$content.$footer;
                }

                // set mail object and params
                $mail = new MAIL;
                $xpmFunc=new FUNC5();
                $xpmMime= new MIME5();

                $mail->from($fromEmail=$email, $fromName=$email);
                $mail->addto($to);
                $mail->addcc($cc);
                $mail->subject($derivedSubject);
                $mail->html($content);// can be text() if content is only text, html() for html content

                // header
                //$mail->addheader("ReplayTo", 'abc@myntra.com');

                // priority
                //public function priority($level = null, $debug = null) ;

                // add attachment and delete the file from system
                foreach($fielsUploaded as $file){
                    $mail->attach(file_get_contents($file),  $xpmFunc->mime_type(basename($file)), basename($file), null, null,
                                $disposition='inline',$xpmMime->unique());

                    unlink($file);
                }

                // sending resource can be local, client, sendmail, qmail
                if ($mail->send('sendmail')){
                    // show message from FG
                    $message = FeatureGateKeyValuePairs::getFeatureGateValueForKey('Contactus.Message');
                    $smarty->assign("message", $message);

                } else {                    
                    // populate the form
                    $smarty->assign("error_message","Sorry, there is a problem occurred in submitting the form please try again.");
                }

                // to avoid subsequent insert on refresh
                unset($XCART_SESSION_VARS['MyntraContactusSubmission']);

            } else {// file error

                // unlink files
                if(is_array($fielsUploaded)){
                    foreach($fielsUploaded as $file){
                        unlink($file);
                    }
                }
                if($captchaError){
                    $smarty->assign("error_message", $captchaErrorMessage);
                }else if($inputError){
                    $smarty->assign("error_message", $inputErrorMessage);
                } elseif($typeError){
                    $smarty->assign("error_message","please upload images of type png, jpg or gif only");
                    
                } elseif($sizeError){
                    $smarty->assign("error_message","please upload images of size not exceeding 2MB");
                } elseif($uploadError){
                    $smarty->assign("error_message","please upload images of type png, jpg or gif only and of size not exceeding 2MB");
                }
            }
        }
        
    } else {
        $smarty->assign("error_message","Sorry, there is a problem occurred in submitting the form, please try again");
    }
}



// set the submission session to avoid sub-sequent insertion on refresh
$XCART_SESSION_VARS['MyntraContactusSubmission'] = time();

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setStaticPageVars(AnalyticsBase::CONTACT_US);
$analyticsObj->setTemplateVars($smarty);

// check FG whether to make customer login or not
$loginCondition = FeatureGateKeyValuePairs::getFeatureGateValueForKey('Contactus.Login');

$smarty->assign("login_condition",$loginCondition);
$smarty->assign("time",$XCART_SESSION_VARS['MyntraContactusSubmission']);
$smarty->assign("issue_detail",$isuueDetail['issueJSON']);
func_display("static_pages/about_pages/contactus-dynamic.tpl", $smarty);
?>
