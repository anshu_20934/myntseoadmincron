<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: orders.php,v 1.24 2006/01/11 06:55:57 mclap Exp $
#

echo "Access to this page for viewing any order related information is disabled.<br>You can view the relevant information on Prism  (http://prism.mynt.myntra.com)<br><br>Please send an email to servicedesk@myntra.com and send the following details :<br><br>Name :<br>Location :<br>Manager :<br>Business Justification :   (( what info in this xcart page was used by you )<br><br>Request servicedesk@myntra.com for access for prism.mynt.myntra.com with prism readonly role.";
exit(0);

define("NUMBER_VARS", "posted_data[total_min],posted_data[total_max],posted_data[price_min],posted_data[price_max]");
require "./auth.php";
require $xcart_dir."/include/security.php";
include_once $xcart_dir."/modules/apiclient/WarehouseApiClient.php";

x_session_register("search_data");

$smarty->assign("show_order_details", "Y");

if ($mode=="subscriptions" && $active_modules["Subscriptions"])
    include $xcart_dir."/modules/Subscriptions/subscriptions.php";
else {

if ($mode == 'delete_all') {
#
# Delete ALL orders and move them to the orders_deleted table
#
	include $xcart_dir."/include/process_order.php";
}

include $xcart_dir."/include/orders.php";

}

#
#load all organization
#
$query = "select id, subdomain from mk_org_accounts";
$organization = func_query($query, true);
$smarty->assign("organization", $organization);

$query_courier_details = "SELECT DISTINCT code,courier_service FROM mk_courier_service";
$courier_details = func_query($query_courier_details, true);
$smarty->assign("courier_details", $courier_details);

$warehouses = WarehouseApiClient::getAllWarehouses();
$smarty->assign('warehouses', $warehouses);


@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
