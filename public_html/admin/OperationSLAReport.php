<?php
require_once "../admin/auth.php";
require_once "../include/security.php";

$i=0;
$report_list[$i++] = array("name"=>"Overall - SLA","link"=>"../admin/AdminReportViewer.php?report_name=/reports/ReportRenderer.php&format=html&prompt_filters=yes&action=genreport&report=28");
$report_list[$i++] = array("name"=>"Operations - SLA","link"=>"../admin/AdminReportViewer.php?report_name=/reports/ReportRenderer.php&format=html&prompt_filters=yes&action=genreport&report=29");
$report_list[$i++] = array("name"=>"Returns - SLA","link"=>"../admin/AdminReportViewer.php?report_name=/reports/ReportRenderer.php&format=html&prompt_filters=yes&action=genreport&report=37");
$report_list[$i++] = array("name"=>"RTO - SLA","link"=>"../admin/AdminReportViewer.php?report_name=/reports/ReportRenderer.php&format=html&prompt_filters=yes&action=genreport&report=38");
$report_list[$i++] = array("name"=>"All Combined Reports - SLA","link"=>"../admin/AdminReportViewer.php?report_name=/reports/ReportRenderer.php&format=html&prompt_filters=yes&action=genreport&report=0&report_title=Operations%20SLA%20Report&report_list=28,29,37,38");

//$report_list[$i++] = array("name"=>"Category & Assignee Wise - Orders","link"=>"../reports/categoryassigneewiseorders.php");

$smarty->assign("report_list",$report_list);
$smarty->assign("main","reports_link_list");
func_display("admin/home.tpl",$smarty);