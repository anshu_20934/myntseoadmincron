<?php

namespace sizeunification\web\controllers;
use sizeunification\action\SizeMappingActions;

require_once $_SERVER["DOCUMENT_ROOT"]."/auth.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/exception/MyntraException.php";
use base\BaseController;
use sizeunification\enums\SizeUnificationActionType;
use sizeunification\action\SizeScaleActions;

class SizeMappingController /* extends BaseController */ {
	public function run(){
		$result = null;
		$xaction = $_REQUEST["xaction"];
		$actionInstance = new SizeMappingActions();
		switch ($xaction) {
			case SizeUnificationActionType::Create:
				$result = $actionInstance->actionInsertOrUpdateMapping();
				break;
			case SizeUnificationActionType::Read:
				$result = $actionInstance->actionFetchMappings();
				break;
			case SizeUnificationActionType::Delete:
				try {
					$result = $actionInstance->actionDeleteMapping();
				} catch (\MyntraException $e) {
					$result = array("status"=>"failed","message"=>$e->getMessage(),"title"=>"Deletion Failed");
				}
				break;
			case SizeUnificationActionType::FetchUnifiedScales:
				$result = $actionInstance->actionFetchUnifiedScales();
				break;
			case SizeUnificationActionType::FetchStyleDetails:
				$result = $actionInstance->actionFetchStyleDetails();
				break;
			case SizeUnificationActionType::FetchMatchingStyles:
				$result = $actionInstance->actionFetchMatchingStyles();
				break;
			case SizeUnificationActionType::FetchUnifiedSizes:
				$result = $actionInstance->actionFetchUnifiedSizes();
				break;
			case SizeUnificationActionType::CopyMappings:
				$result = $actionInstance->actionCopyMappings();
				break;
			default:
				;
			break;
		}
		echo json_encode($result);
	}
}
//echo json_encode("Hello"); /* return;
$controller = new SizeMappingController();
$controller->run();