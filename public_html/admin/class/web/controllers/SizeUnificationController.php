<?php

namespace sizeunification\web\controllers;

require_once $_SERVER["DOCUMENT_ROOT"]."/auth.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/admin/size_unification_admin_new_function.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/exception/MyntraException.php";

use base\BaseController;
use sizeunification\enums\SizeUnificationActionType;
use sizeunification\action\SizeUnificationActions;

class SizeUnificationController /* extends BaseController */ {
	
	public function run(){
		$actionInstance = new SizeUnificationActions();
		$xaction = $_REQUEST["xaction"];
		$response = null;
		switch ($xaction) {
			
			case SizeUnificationActionType::ReUnifyStyles:
				$response = $actionInstance->actionReUnifyStyles();
				break;

			case SizeUnificationActionType::ClearMappingsForStyles:
				$response = $actionInstance->actionClearMappings();
				break;
			case SizeUnificationActionType::ClearMeasurementsForStyles:
				$response = $actionInstance->actionClearMeasurements();
				break;
			case SizeUnificationActionType::ReApplyMappingsForStyles:
				$response = $actionInstance->actionReApplyMappings();
				break;
			case SizeUnificationActionType::ReApplyMeasurementsForStyles:
				$response = $actionInstance->actionReApplyMeasurements();
				break;
			case SizeUnificationActionType::ReApplyImageForStyles:
				$response = $actionInstance->actionReApplySizeChartImages();
				break;
			case SizeUnificationActionType::ClearCacheForStyles:
				$response = $actionInstance->actionClearStyleCache();
				break;
			default:
				;
		}
		echo json_encode($response);
	}
}
$controller = new SizeUnificationController();
$controller->run();