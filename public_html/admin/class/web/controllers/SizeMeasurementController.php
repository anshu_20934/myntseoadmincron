<?php

namespace sizeunification\web\controllers;

require_once $_SERVER["DOCUMENT_ROOT"]."/auth.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/exception/MyntraException.php";
use base\BaseController;
use sizeunification\enums\SizeUnificationActionType;
use sizeunification\action\SizeScaleActions;
use sizeunification\action\SizeMeasurementActions;
class SizeMeasurementController /* extends BaseController */ {
	public function run(){
		$xaction = $_REQUEST["xaction"];
		$actionInstance = new SizeMeasurementActions();
		switch ($xaction) {
			case SizeUnificationActionType::Create:
				$response = $actionInstance->actionInsertOrUpdateMeasurement();
				break;
			case SizeUnificationActionType::Read:
				$response = $actionInstance->actionFetchMeasurements();
				break;
			case SizeUnificationActionType::Delete:
				try {
					$response = $actionInstance->actionDeleteMeasurement();
				} catch (\MyntraException $e) {
					$response = array("status"=>"failed","message"=>$e->getMessage(),"title"=>"Deletion Failed");
				}
				break;
			case SizeUnificationActionType::CopyMeasurements:
				$response = $actionInstance->actionCopyMeasurements();
				break;
			default:
				;
			break;
		}
		echo json_encode($response);
	}
}
$controller = new SizeMeasurementController();
$controller->run();