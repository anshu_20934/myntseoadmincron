<?php

namespace sizeunification\web\controllers;
use sizeunification\action\SizeScaleValueActions;

require_once $_SERVER["DOCUMENT_ROOT"]."/auth.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/exception/MyntraException.php";
use base\BaseController;
use sizeunification\enums\SizeUnificationActionType;
use sizeunification\action\SizeScaleActions;

class SizeScaleValueController /* extends BaseController */ {
	public function run(){
		$xaction = $_REQUEST["xaction"];
		$actionInstance = new SizeScaleValueActions();
		switch ($xaction) {
			case SizeUnificationActionType::Create:
				$response = $actionInstance->actionInsertOrUpdateScale();
				echo $response;
				break;
			case SizeUnificationActionType::Read:
				$scales = $actionInstance->actionFetchScales();
				echo json_encode($scales);
				break;
			case SizeUnificationActionType::Delete:
				try {
					$result = $actionInstance->actionDeleteScale();
				} catch (\MyntraException $e) {
					$result = array("status"=>"failed","message"=>$e->getMessage(),"title"=>"Deletion Failed");
				}
				echo json_encode($result);					
				break;
			case SizeUnificationActionType::MoveUp:
				try {
					echo $actionInstance->actionMove();
				} catch (\MyntraException $e) {
					echo json_encode(array("status"=>"failed","message"=>$e->getMessage(),"title"=>"Move Failed"));
				}
				break;
			case SizeUnificationActionType::MoveDown:
				try {
					echo $actionInstance->actionMove(false);
				} catch (\MyntraException $e) {
					echo json_encode(array("status"=>"failed","message"=>$e->getMessage(),"title"=>"Move Failed"));
				}
				break;
			default:
				;
			break;
		}
	}
}
$controller = new SizeScaleValueController();
$controller->run(); 