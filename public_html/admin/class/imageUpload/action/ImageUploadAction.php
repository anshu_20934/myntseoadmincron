<?php

namespace imageUpload\action;


//include_once "$xcart_dir/admin/class/imageUpload/manager/ImageUploadManager.php";

use imageUpload\manager\ImageUploadManager;

use base\action\BaseAuthenticatedAuthorizedAction;


class ImageUploadAction extends BaseAuthenticatedAuthorizedAction{
	var $styleIds,$manager,$imageFile,$imageSize,$imageFileLocation,$uploadedFile;

	public function __construct(){
		$this->manager = new ImageUploadManager();
		$this->styleIds = explode("\n",$this->getRequestVar('style_id_list'));
		
		$fileName = $this->getRequestVar("fileName","image");
		
		$this->uploadedFile = $this->getUploadedFileInRequest($fileName);
		$this->imageFile = $this->uploadedFile['name'];
		$this->imageSize = $this->uploadedFile['size'];
		$this->imageFileLocation = $this->uploadedFile['tmp_name'];
	}

	public function uploadSizeChartImage() {
		$errorMessage = '';
		$output = array();
		//validating if style ids are numeric
		$output = $this->manager->validateStyleValue($this->styleIds);
		//var_dump($output);
		//$styleIdsCSV = $output['csv'];
		$styleIds = $output['styles'];
		$errorMessage = $output['errorMessage'];

		if($errorMessage == ''){
			//validating if styles present in db
			$output = $this->manager->validateStylesInDB($styleIds);

			$allValidStyle = $output['allValidStyle'];
			$errorMessage .= $output['errorMessage'];

			//if styles are valid, check for validation of uploaded image
			if($allValidStyle){
				if(isset($this->uploadedFile)){
					$output = $this->manager->validateUploadedImage($this->imageFile,$this->imageSize,$this->styleIds);
					$errorMessage .= $output['errorMessage'];
				}
				else{
					$errorMessage .= "</br> Please select and upload proper images";
				}
			}
		}


		if($errorMessage != '')
		{
			$errorMessage .= "No images uploaded please correct all the above errors.<br/>";
		}
		else{//perform uploading and saving part
			$output = $this->manager->saveSizeChartImage($this->imageFile, $this->imageFileLocation, $this->styleIds);
			if($output['errorMessage'] == ''){
				$message = $output['message'];
				$message .= "Updated size chart image successfully for styles: ".implode(",", $styleIds)."<br/>";
			}
			else{
				$errorMessage .= $output['errorMessage'];
			}
		}

		$result = array();
		$result['message'] = $message;
		$result['errorMessage'] = $errorMessage;
			
		return $result;
	}
}
?>