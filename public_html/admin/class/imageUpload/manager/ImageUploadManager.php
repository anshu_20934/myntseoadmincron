<?php
namespace imageUpload\manager;

global $xcart_dir;

require_once "$xcart_dir/admin/auth.php";
include_once "$xcart_dir/include/func/func.image.php";
include_once "$xcart_dir/utils/imagetransferS3.php";


use base\manager\BaseManager;
use style\dao\StyleDBAdapter;

class ImageUploadManager extends BaseManager{

	var $imageUploadMaxSize,$s_upload_dir,$s_cdn_upload_dir_db,$styleDBAdapter;
	
	public function __construct(){
		global $cdn_base;
		global $xcart_dir;
		$this->imageUploadMaxSize =\FeatureGateKeyValuePairs::getInteger('styleImageUploadMaxSize', 300);
		$this->s_upload_dir=$xcart_dir.'/images/style/sizechartimages/';
		$this->s_cdn_upload_dir_db="$cdn_base/images/style/sizechartimages/";
		$this->styleDBAdapter = StyleDBAdapter::getInstance();
	}
	
	public function validateStyleValue($styleIds){
		$errorMessage ='';
		$totalStyles = count($styleIds);
		//loop through all style ids and sanitize them. If non numberic style id found, then give error
		foreach ($styleIds as $index=>$styleId){
			$escapedStyleId = trim(mysql_real_escape_string($styleId));
			$escapedStyleId = str_replace('\r',"", $escapedStyleId);

			$styleIds[$index] = $escapedStyleId;
			if(!is_numeric($escapedStyleId)){
				$errorMessage .= "Style id : ".$escapedStyleId." is not numeric<br/>";
			}
		}
		$result = array();
		$result['styles'] = $styleIds;
		$result['errorMessage'] = $errorMessage;

		return $result;
	}

	public function validateStylesInDB($styleIds){
		$result = array();
		$allValidStyle = true;
		$styleIdsCSV =  implode(",", $styleIds);
		$styleProperties = $this->styleDBAdapter->getStylePropertiesForStyleIDList($styleIdsCSV); 
		//func_query("SELECT * FROM mk_style_properties WHERE style_id in ($styleIdsCSV)", TRUE);
		$errorMessage = '';
		if($styleProperties == false || count($styleProperties) != count($styleIds)){ //if rows corresponding to some style ids not fetched
			foreach ($styleProperties as $index=>$value){
				$styleIdsFromDb[$index] = $value['style_id'];
			}
			foreach ($styleIds as $index => $value){
				if(!in_array($value, $styleIdsFromDb)){
					$errorMessage .= "Style Id ".$value." is not a valid style id<br/>";
					$allValidStyle = false;
				}
			}
		}
		$result['errorMessage'] = $errorMessage;
		$result['allValidStyle'] = $allValidStyle;
		return $result;
	}

	public function validateUploadedImage($imageFile,$imageSize){
		$errorMessage = '';
		$imagesExceedingSizeLimit = '';
		$sizeChartImageExt = strtolower(substr($imageFile, -4));
		if($sizeChartImageExt != '.jpg'){
			$errorMessage .= "Size chart image uploaded is not a jpg file<br/>";
		}
		elseif ($imageSize > 1024*$this->imageUploadMaxSize){
			$errorMessage .= "Size chart image exceeds size limit of $this->imageUploadMaxSize Kb<br/>";
		}
		
		$result = array();
		$result['errorMessage'] = $errorMessage;
		return $result;
	}

	public function saveSizeChartImage($imageFile,$imageFileLocation,$styleIds){
		$message = '';
		//check if size chart already exists
		if(!is_file($this->s_upload_dir.$imageFile)){//if not exists, then upload it
			$message =  "Uploaded size chart image to CDN<br/>";
			$sizeChartImageURL = func_resize_backup_uploaded_product_image($imageFileLocation, $imageFile, 'sizechart', true);
		}
		else{//else create image url from cdn directory
			$message = "Image already present in CDN<br/>";
			$sizeChartImageURL=$this->s_cdn_upload_dir_db.$imageFile;
		}
		if($sizeChartImageURL == null)
		{
			$sizeChartImageURL = "";
			$errorMessage .= "Image URL is null. Error in uploading image<br/>";
		}
		//updating db
		$successful = false;
		$query_data["size_chart_image"]  = $sizeChartImageURL;
		foreach ($styleIds as $index=>$value){
			$successful = $this->styleDBAdapter->updateProductStyleTable($query_data, $value);
			if($successful == false){
				$errorMessage .= "Error while updating mk_product_style for styleId ".$value."<br/>";
			}
			//func_array2updateWithTypeCheck("mk_product_style", $query_data, "id='$value' ");
		}
		
		$result = array();
		if($errorMessage != ''){ $message = '';}
		
		$result['message'] = $message;
		$result['errorMessage'] = $errorMessage;
		return $result;
	}
}
?>