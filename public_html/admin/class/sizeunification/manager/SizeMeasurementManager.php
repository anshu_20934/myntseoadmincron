<?php
namespace sizeunification\manager;
use sizeunification\dao\SizeMeasurementDAO;
use sizeunification\dao\SizeScaleDAO;
use base\manager\BaseManager;

class SizeMeasurementManager extends BaseManager {
	
	var $dao,$concatExclusionList;
	public function __construct(){
		$this->dao = new SizeMeasurementDAO();
		//these are the scales for which size cannot be concatenated with the size-value
		$this->concatExclusionList = array('Age','Text Size', 'Equivalent Size');
	}
	
	public function getMeasurementsForArticleTypeBrandAgeGroup($articleTypeId,$brandId,$ageGroup,$styleId=null){
		$ret = $this->dao->getMeasurementsForArticleTypeBrandAgeGroup($articleTypeId, $brandId, $ageGroup,$styleId);
		if($ret == null){
			$ret = false;
		}
		return $ret;
	}
	
	public function updateMeasurement($measurementId, $sizeValueId, $measurement){
		$concatSizeArr = $this->getConcatenatedSize($sizeValueId);
		$size = $concatSizeArr[$sizeValueId]; 
		return $this->dao->updateMeasurement($measurementId, $sizeValueId, $size, $measurement);
	}
	
	public function getConcatenatedSize($sizeValueIds){
		return $this->dao->getSize($sizeValueIds);
	}
	
	public function getStyleDetails($styleId,$vendorArticleNumber){
		return $this->dao->getStyleDetails($styleId,$vendorArticleNumber);
	}
	
	public function insertMeasurement($measurement, $searchByCombination){
		$measurementArrRep = array(	"brand_id"=>$measurement["brandid"],
								"age_group"=>$measurement["agegroup"],
								"article_type_id"=>$measurement["articletypeid"],
								"sizeValueId"=>$measurement["sizeid"],
								"jsonOutput"=>array("type"=>"json", 
								"value"=>stripslashes($measurement["measurement"]))
						 );
		if(!$searchByCombination){
			$measurementArrRep["style_id"] = $measurement["styleid"];
		}
		$sizeValueId = $measurementArrRep["sizeValueId"];
		$concatSizeArr = $this->getConcatenatedSize($sizeValueId);
		$size = $concatSizeArr[$sizeValueId];
		$measurementArrRep["size"] = $size;
		return $this->dao->insertMeasurement($measurementArrRep);
	}
	
	public function copyMeasurements($targetBrandId,$targetAgeGroup,$articleTypeId,$brandId,$ageGroup, $styleId=null, $deleteTargetMeasurementsBeforeCopy = false){
		$src = array("articleType"=>$articleTypeId, "brand"=>$brandId, "ageGroup"=>$ageGroup, "styleId"=>$styleId);
		$dest = array("articleType"=>$articleTypeId, "brand"=>$targetBrandId, "ageGroup"=>$targetAgeGroup);
		if($deleteTargetMeasurementsBeforeCopy){
			$this->dao->deleteMeasurements($articleTypeId, $targetBrandId, $targetAgeGroup, $targetStyleId);
		}
		else{
			//check whether mappings exist in target already..
			$countExistingMeasurements = $this->dao->doMeasurementsExist($articleTypeId, $targetBrandId, $targetAgeGroup, $targetStyleId);
			if($countExistingMeasurements > 0){
				return array("status"=>"failed","message"=>"Target already has $countExistingMeasurements mappings. Can copy only to an empty target.Else choose the option to delete before copy");
			}
		}
		return $this->dao->copyMeasurements($src, $dest);
	}
	
	public function copyMeasurementsToStyle($targetStyleId,$articleTypeId,$brandId,$ageGroup, $styleId=null, $deleteTargetMeasurementsBeforeCopy = false){
		//first fetch the style-details for this style-id
		$styleDetails = $this->dao->getStyleDetails($targetStyleId);
		if(empty($styleDetails)){
			return array("status"=>"failed","message"=>"Style: ".$targetStyleId." does not exist");
		}
		$styleDetails = $styleDetails[0];
		if($articleTypeId != $styleDetails["articletypeid"]){
			return array("status"=>"failed","message"=>"Cannot copy unification measurements of style: ".$targetStyleId." because it belongs to a different article-type(source: {$articleTypeId} is not equal to target: {$styleDetails["articletypeid"]})");
		}
		$src = array("articleType"=>$articleTypeId, "brand"=>$brandId, "ageGroup"=>$ageGroup, "styleId"=>$styleId);
		$dest = array("articleType"=>$styleDetails["articletypeid"], "brand"=>$styleDetails["brandid"], "ageGroup"=>$styleDetails["agegroup"],"styleId"=>$targetStyleId);

		if($deleteTargetMeasurementsBeforeCopy){
			$this->dao->deleteMeasurements($styleDetails["articletypeid"], $styleDetails["brandid"], $styleDetails["agegroup"], $targetStyleId);
		}
		else {
			//check whether measurements exist in target already..
			$countExistingMeasurements = $this->dao->doMeasurementsExist($styleDetails["articletypeid"], $styleDetails["brandid"], $styleDetails["agegroup"], $targetStyleId);
			if($countExistingMeasurements > 0){
				return array("status"=>"failed","message"=>"Target already has $countExistingMeasurements measurements. Can copy only to an empty target.Else choose the option to delete before copy");
			}
		}
		return $this->dao->copyMeasurements($src, $dest);
	}
}