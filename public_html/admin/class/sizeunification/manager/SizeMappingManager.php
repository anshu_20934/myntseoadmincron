<?php
namespace sizeunification\manager;
use sizeunification\dao\SizeMappingDAO;
use sizeunification\dao\SizeScaleDAO;
use base\manager\BaseManager;

class SizeMappingManager extends BaseManager {
	
	var $dao;
	public function __construct(){
		$this->dao = new SizeMappingDAO();
	}
	
	public function getMappingsForArticleTypeBrandAgeGroup($articleTypeId,$brandId,$ageGroup,$styleId=null){
		return $this->dao->getMappingsForArticleTypeBrandAgeGroup($articleTypeId, $brandId, $ageGroup,$styleId);
	}
	
	public function updateMapping($mappingId, $unifiedSizeValueId, $sizeValueId){
		$concatSizeArr = $this->getConcatenatedSize(array($unifiedSizeValueId,$sizeValueId));
		$size = $concatSizeArr[$sizeValueId]; 
		$unifiedSize = $concatSizeArr[$unifiedSizeValueId];
		return $this->dao->updateMapping($mappingId, $sizeValueId, $size, $unifiedSizeValueId, $unifiedSize);
	}
	
	public function getConcatenatedSize($sizeValueIds){
		return $this->dao->getSize($sizeValueIds);
	}
	
	public function getStyleDetails($styleId,$vendorArticleNumber){
		return $this->dao->getStyleDetails($styleId,$vendorArticleNumber);
	}
	
	public function getUnifiedScalesForInsert($articleTypeId,$brandId,$ageGroup){
		$unifiedScales = $this->dao->getExistingUnifiedScale($articleTypeId, $brandId, $ageGroup);
		if(empty($unifiedScales)){
			$scaleDAO = new SizeScaleDAO();
			$unifiedScales = $scaleDAO->getScalesForArticleType($articleTypeId, $asCombo = true, $unified = true);
		}
		return $unifiedScales;
	}
	
	public function getMatchingStyles($articleTypeId,$brandId,$ageGroup,$styleId){
		return $this->dao->findMatchingStyles($articleTypeId, $brandId, $ageGroup, $styleId);
	}

	public function insertMapping($mapping, $searchByCombination){
		$mappingArrRep = array(	"brandId"=>$mapping["brandid"],
								"age_group"=>$mapping["agegroup"],
								"article_type_id"=>$mapping["articletypeid"],
								"sizeValueId"=>$mapping["sizeid"],
								"unifiedSizeValueId"=>$mapping["unifiedsizeid"]
						 );
		if(!$searchByCombination){
			$mappingArrRep["styleID"] = $mapping["styleid"];
		}
		$unifiedSizeValueId = $mappingArrRep["unifiedSizeValueId"];
		$sizeValueId = $mappingArrRep["sizeValueId"];
		$concatSizeArr = $this->getConcatenatedSize(array($unifiedSizeValueId,$sizeValueId));
		$size = $concatSizeArr[$sizeValueId];
		$unifiedSize = $concatSizeArr[$unifiedSizeValueId];
		$mappingArrRep["size"] = $size;
		$mappingArrRep["unifiedSize"] = $unifiedSize;
		return $this->dao->insertMapping($mappingArrRep);
	}
	
	public function getUnifiedSizes($articleTypeId,$brandId,$ageGroup, $styleId=null){
		return $this->dao->getUnifiedSizes($articleTypeId, $brandId, $ageGroup, $styleId);
	}
	
	public function copyMappings($targetBrandId,$targetAgeGroup,$articleTypeId,$brandId,$ageGroup, $styleId=null, $deleteTargetMappingsBeforeCopy = false){
		$src = array("articleType"=>$articleTypeId, "brand"=>$brandId, "ageGroup"=>$ageGroup, "styleId"=>$styleId);
		$dest = array("articleType"=>$articleTypeId, "brand"=>$targetBrandId, "ageGroup"=>$targetAgeGroup);
		if($deleteTargetMappingsBeforeCopy){
			$this->dao->deleteMappings($articleTypeId, $targetBrandId, $targetAgeGroup, $targetStyleId);
		}
		else{
			//check whether mappings exist in target already..
			$countExistingMappings = $this->dao->doMappingsExist($articleTypeId, $targetBrandId, $targetAgeGroup, $targetStyleId);
			if($countExistingMappings > 0){
				return array("status"=>"failed","message"=>"Target already has $countExistingMappings mappings. Can copy only to an empty target.Else choose the option to delete before copy");
			}
		}
		return $this->dao->copyMappings($src, $dest);
	}
	
	public function copyMappingsToStyle($targetStyleId,$articleTypeId,$brandId,$ageGroup, $styleId=null, $deleteTargetMappingsBeforeCopy = false){
		//first fetch the style-details for this style-id
		$styleDetails = $this->dao->getStyleDetails($targetStyleId);
		if(empty($styleDetails)){
			return array("status"=>"failed","message"=>"Style: ".$targetStyleId." does not exist");
		}
		$styleDetails = $styleDetails[0];
		if($articleTypeId != $styleDetails["articletypeid"]){
			return array("status"=>"failed","message"=>"Cannot copy unification mappings of style: ".$targetStyleId." because it belongs to a different article-type(source: {$articleTypeId} is not equal to target: {$styleDetails["articletypeid"]})");
		}
		$src = array("articleType"=>$articleTypeId, "brand"=>$brandId, "ageGroup"=>$ageGroup, "styleId"=>$styleId);
		$dest = array("articleType"=>$styleDetails["articletypeid"], "brand"=>$styleDetails["brandid"], "ageGroup"=>$styleDetails["agegroup"],"styleId"=>$targetStyleId);
		if($deleteTargetMappingsBeforeCopy){
			$this->dao->deleteMappings($styleDetails["articletypeid"], $styleDetails["brandid"], $styleDetails["agegroup"], $targetStyleId);
		}
		else {
			//check whether mappings exist in target already..
			$countExistingMappings = $this->dao->doMappingsExist($styleDetails["articletypeid"], $styleDetails["brandid"], $styleDetails["agegroup"], $targetStyleId);
			if($countExistingMappings > 0){
				return array("status"=>"failed","message"=>"Target already has $countExistingMappings mappings. Can copy only to an empty target.Else choose the option to delete before copy"); 
			}
		}
		return $this->dao->copyMappings($src, $dest);
	}
	
}