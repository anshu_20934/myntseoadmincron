<?php
namespace sizeunification\action;
use sizeunification\manager\SizeMappingManager;

use sizeunification\enums\SizeUnificationActionType;

use enums\base\ActionType;

use sizeunification\dao\SizeMappingDAO;

use base\action\BaseAuthenticatedAuthorizedAction;

class SizeMappingActions extends BaseAuthenticatedAuthorizedAction {
	
	var $dao,$manager,$searchBy;
	var $brandId,$articleTypeId,$ageGroup,$styleId,$mappingId;
	var $targetBrandId,$targetAgeGroup,$targetStyleIds;
	var $deleteTargetMappingsBeforeCopy;
	public function __construct(){
		$this->dao = new SizeMappingDAO();
		$this->manager = new SizeMappingManager();
		$this->searchBy = $this->getRequestVar("searchBy","combination");
		$this->brandId = $this->getRequestVar("brand",null);
		$this->articleTypeId = $this->getRequestVar("articleType",null);
		$this->ageGroup = $this->getRequestVar("ageGroup",null);
		$this->mappingId = $this->getRequestVar("mappingid",null);
		$this->styleId = $this->getRequestVar("styleId",null);
		$this->targetBrandId = $this->getRequestVar("targetBrand",null);
		$this->targetAgeGroup = $this->getRequestVar("targetAgeGroup",null);
		$this->targetStyleIds = $this->getRequestVar("targetStyleIds",null);
		$this->deleteTargetMappingsBeforeCopy = $this->getRequestVar("deleteTargetMappingsBeforeCopy",false,"boolean");
	}
	
	public function actionFetchMappings(){
		$mappings = $this->manager->getMappingsForArticleTypeBrandAgeGroup($this->articleTypeId,$this->brandId,$this->ageGroup,$this->styleId);
		return array("count" => count($mappings) , "results"=>$mappings);
	}
	
	public function actionInsertOrUpdateMapping(){
		$data = $this->getRequestVar("results",null);
		if(isset($data)){
			$fieldJsonObj = json_decode($data);
			$fields = convertToArrayRecursive($fieldJsonObj);
			//this might be a useless call from ExtJs Store; ignore it
			if(empty($fields["sizeid"])||empty($fields["unifiedsizeid"])){
				return;
			}
			$response;
			if(!empty($fields["mappingid"])){
				$response = $this->doUpdate($fields);
			} else {
				$response = $this->doInsert($fields);
				//$response = print_r($fields,true);
			}
			return $response;
		}
		
	}
	
	public function actionFetchUnifiedScales(){
		$unifiedScales = null;
		$unifiedScales = $this->manager->getUnifiedScalesForInsert($this->articleTypeId, $this->brandId, $this->ageGroup);
		return array("count" => count($unifiedScales) , "results"=>$unifiedScales);
	}
	
	private function doInsert($fields){
		$searchByCombination = ($this->searchBy === "combination");
		return $this->manager->insertMapping($fields,$searchByCombination);
	}
	
	private function doUpdate($fields){
		return $this->manager->updateMapping($fields["mappingid"], $fields["unifiedsizeid"],$fields["sizeid"]);
	}

	public function actionDeleteMapping(){
		if(isset($this->mappingId)){
			try {
				$result = $this->dao->deleteMapping($this->mappingId);
				return $result;
			} catch(\MyntraException $e){
				throw $e;
			}	
		}else {
			throw new \MyntraException("mapping id is received as null,so cannot delete");
		}
	}
	
	public function actionFetchStyleDetails(){
		$vendorArticleNumber = 	$this->getRequestVar("vendorArticleNumber");
		$styleDetails = $this->manager->getStyleDetails($this->styleId, $vendorArticleNumber);
		return array("count"=> count($styleDetails), "results"=>$styleDetails);		
	}
	
	public function actionFetchMatchingStyles(){
		$matchingStyles = $this->manager->getMatchingStyles($this->articleTypeId, $this->brandId, $this->ageGroup, $this->styleId);
		return array("count" => count($matchingStyles) , "results"=>$matchingStyles);
	}
	
	public function actionFetchUnifiedSizes(){
		$unifiedSizes = $this->manager->getUnifiedSizes($this->articleTypeId, $this->brandId, $this->ageGroup, $this->styleId);
		return array("count" => count($unifiedSizes) , "results"=>$unifiedSizes);
	}
	
	public function actionCopyMappings(){
		$retval="";
		if(!empty($this->targetStyleIds)){
			foreach(explode(",", $this->targetStyleIds) as $eachStyleId){
				$eachStyleId = intval($eachStyleId);
				if($eachStyleId){
					$ret = $this->manager->copyMappingsToStyle($eachStyleId, $this->articleTypeId, $this->brandId, $this->ageGroup,$this->styleId, $this->deleteTargetMappingsBeforeCopy);
					if(is_array($ret) && isset($ret["status"]) && $ret["status"] == "failed"){
						//$retval[$eachStyleId] = "Failed: ".$ret["message"];
						$retval .= $eachStyleId.": Failed: ".$ret["message"]."\n";
					}
					elseif(is_int($ret)){
						//$retval[$eachStyleId] = "Success: ".$ret." Mappings copied!";
						$retval .= $eachStyleId.": Success: ".$ret." Mappings copied!\n";
					}
				}
			}
		}
		elseif (!empty($this->targetBrandId) && !empty($this->targetAgeGroup)){
			$ret = $this->manager->copyMappings($this->targetBrandId, $this->targetAgeGroup, $this->articleTypeId, $this->brandId, $this->ageGroup,$this->styleId, $this->deleteTargetMappingsBeforeCopy);
			if(is_array($ret) && isset($ret["status"]) && $ret["status"] == "failed"){
				$retval = "Failed: ".$ret["message"];
			}
			elseif(is_int($ret)){
				$retval = "Success: ".$ret." Mappings copied!";
			}
		}
		return $retval;
	}
}