<?php
namespace sizeunification\action;
use sizeunification\enums\SizeUnificationActionType;
use enums\base\ActionType;
use sizeunification\dao\SizeScaleValueDAO;
use base\action\BaseAuthenticatedAuthorizedAction;

class SizeScaleValueActions extends BaseAuthenticatedAuthorizedAction {
	var $dao;
	public function __construct(){
		$this->dao = new SizeScaleValueDAO();
	}
	public function actionFetchScales(){
		$scaleId = $this->getRequestVar("scaleid");
		$asCombo = $this->getRequestVar("asCombo", false);
		$unified = $this->getRequestVar("unified",false);
		$scaleValues = $this->dao->getScaleValuesForScale($scaleId, $asCombo,$unified);
		return array("count" => count($scaleValues) , "results"=>$scaleValues);
	}
	
	public function actionInsertOrUpdateScale(){
		$data = $this->getRequestVar("results",null);
		if(isset($data)){
			$fieldJsonObj = json_decode($data);
			$fields = convertToArrayRecursive($fieldJsonObj);
			if($fields["sizevalue"]== ''){
				return;
			}
			$fields["scaleid"] = $this->getRequestVar("scaleid",null);
			$response; 
			if(!empty($fields["scalevalueid"])){
				$response = $this->doUpdate($fields);
			} else {
				$response = $this->doInsert($fields);
			}
			return $response;
			//return print_r($fields, true);
		}
		
	}
	
	private function doInsert($fields){
		return $this->dao->insertSize($fields);
	}
	
	private function doUpdate($fields){
		return $this->dao->updateSize($fields);
	}

	public function actionDeleteScale(){
		$scaleValueIdToDelete = $this->getRequestVar("scalevalueid");
		try {
			$result = $this->dao->deleteSize($scaleValueIdToDelete);
			return $result;
		} catch(\MyntraException $e){
			throw $e;
		}
	}
	/**
	 * 
	 * by default, moveup. else move-down
	 * @param boolean $moveUp
	 */
	public function actionMove($moveUp = true){
		$scaleId = $this->getRequestVar("scaleid",null);
		$scaleValueId = $this->getRequestVar("scalevalueid");
		$existingSortOrder = $this->getRequestVar("sortorder");
		if($moveUp){
			$ret = $this->dao->getMaxSortOrderLessThanGiven($scaleId, $existingSortOrder);
		}else {
			$ret = $this->dao->getMinSortOrderGreaterThanGiven($scaleId, $existingSortOrder);
		}
		if(!empty($ret)){
			return $this->dao->swapSortOrders($scaleValueId, $existingSortOrder, $ret["sizevalueid"], $ret["sortorder"]);
		}
		else {
			throw new \MyntraException("Cannot move - already at the top/bottom extreme");
		}
	}
}