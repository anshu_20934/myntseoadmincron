<?php
namespace sizeunification\action;
use sizeunification\enums\SizeUnificationActionType;

use enums\base\ActionType;

use sizeunification\dao\SizeScaleDAO;

use base\action\BaseAuthenticatedAuthorizedAction;

class SizeScaleActions extends BaseAuthenticatedAuthorizedAction {
	private $dao;
	public function __construct(){
		$this->dao = new SizeScaleDAO();
	}
	
	public function actionFetchScales(){
		$articleTypeId = $this->getRequestVar("articleType");
		$asCombo = $this->getRequestVar("asCombo",false);
		$scales = $this->dao->getScalesForArticleType($articleTypeId, $asCombo);
		return array("count" => count($scales) , "results"=>$scales);
	}
	
	public function actionInsertOrUpdateScale(){
		$data = $this->getRequestVar("results",null);
		if(isset($data)){
			$fieldJsonObj = json_decode($data);
			$fields = convertToArrayRecursive($fieldJsonObj);
			if(empty($fields["name"])){
				return;
			}
			$response;
			if(!empty($fields["scaleid"])){
				$response = $this->doUpdate($fields);
			} else {
				$response = $this->doInsert($fields);
			}
			return $response;
		}
	}
	
	private function doInsert($fields){
		return $this->dao->insertScale($fields);
	}
	
	private function doUpdate($fields){
		return $this->dao->updateScale($fields);
	}

	public function actionDeleteScale(){
		$articleTypeId = $this->getRequestVar("articleType");
		$scaleIdToDelete = $this->getRequestVar("scaleid");
		try {
			$result = $this->dao->deleteScale($scaleIdToDelete, $articleTypeId);
			return $result;
		} catch(\MyntraException $e){
			throw $e;
		}
	}
}