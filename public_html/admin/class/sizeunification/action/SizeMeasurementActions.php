<?php
namespace sizeunification\action;
use sizeunification\manager\SizeMeasurementManager;

use sizeunification\enums\SizeUnificationActionType;

use enums\base\ActionType;

use sizeunification\dao\SizeMeasurementDAO;

use base\action\BaseAuthenticatedAuthorizedAction;

class SizeMeasurementActions extends BaseAuthenticatedAuthorizedAction {
	
	var $dao,$manager,$searchBy;
	
	public function __construct(){
		$this->dao = new SizeMeasurementDAO();
		$this->manager = new SizeMeasurementManager();
		$this->searchBy = $this->getRequestVar("searchBy","combination");
		$this->brandId = $this->getRequestVar("brand",null);
		$this->articleTypeId = $this->getRequestVar("articleType",null);
		$this->ageGroup = $this->getRequestVar("ageGroup",null);
		$this->measurementId = $this->getRequestVar("measurementid",null);
		$this->styleId = $this->getRequestVar("styleId",null);
		$this->targetBrandId = $this->getRequestVar("targetBrand",null);
		$this->targetAgeGroup = $this->getRequestVar("targetAgeGroup",null);
		$this->targetStyleIds = $this->getRequestVar("targetStyleIds",null);
		$this->deleteTargetMeasurementsBeforeCopy = $this->getRequestVar("deleteTargetMeasurementsBeforeCopy",false,"boolean");
	}
	
	public function actionFetchMeasurements(){
		$brandId = $this->getRequestVar("brand",null);
		$articleTypeId = $this->getRequestVar("articleType",null);
		$ageGroup = $this->getRequestVar("ageGroup",null);
		$this->searchBy = $this->getRequestVar("searchBy","combination");
		$styleId = $this->getRequestVar("styleId",null);
		$measurements = $this->manager->getMeasurementsForArticleTypeBrandAgeGroup($articleTypeId,$brandId,$ageGroup,$styleId);
		return array("count" => count($measurements) , "results"=>$measurements);
	}
	
	public function actionInsertOrUpdateMeasurement(){
		$this->searchBy = $this->getRequestVar("searchBy","combination");
		$data = $this->getRequestVar("results",null);
		if(isset($data)){
			$fieldJsonObj = json_decode($data);
			$fields = convertToArrayRecursive($fieldJsonObj);
			//this might be a useless call from ExtJs Store; ignore it
			if(empty($fields["sizeid"])||empty($fields["measurement"])){
				return;
			}
			$response;
			if(!empty($fields["measurementid"])){
				$response = $this->doUpdate($fields);
			} else {
				$response = $this->doInsert($fields);
			}
			return $response;
		}
	}
	
	private function doInsert($fields){
		$searchByCombination = ($this->searchBy === "combination");
		return $this->manager->insertMeasurement($fields,$searchByCombination);
	}
	
	private function doUpdate($fields){
		return $this->manager->updateMeasurement($fields["measurementid"], $fields["sizeid"],$fields["measurement"]);
	}
	
	public function actionCopyMeasurements(){
		$retval="";
		if(!empty($this->targetStyleIds)){
			foreach(explode(",", $this->targetStyleIds) as $eachStyleId){
				$eachStyleId = intval($eachStyleId);
				if($eachStyleId){
					$ret = $this->manager->copyMeasurementsToStyle($eachStyleId, $this->articleTypeId, $this->brandId, $this->ageGroup,$this->styleId, $this->deleteTargetMeasurementsBeforeCopy);
					if(is_array($ret) && isset($ret["status"]) && $ret["status"] == "failed"){
						$retval .= $eachStyleId.": Failed: ".$ret["message"]."\n";
					}
					elseif(is_int($ret)){
						$retval .= $eachStyleId.": Success: ".$ret." Measurements copied!\n";
					}
				}
			}
		}
		elseif (!empty($this->targetBrandId) && !empty($this->targetAgeGroup)){
			$ret = $this->manager->copyMeasurements($this->targetBrandId, $this->targetAgeGroup, $this->articleTypeId, $this->brandId, $this->ageGroup,$this->styleId, $this->deleteTargetMeasurementsBeforeCopy);
			if(is_array($ret) && isset($ret["status"]) && $ret["status"] == "failed"){
				$retval = "Failed: ".$ret["message"];
			}
			elseif(is_int($ret)){
				$retval = "Success: ".$ret." Measurements copied!";
			}
		}
		return $retval;
	}
	public function actionDeleteMeasurement(){
		$measurementId = $this->getRequestVar("measurementid",null);
		try {
			$result = $this->dao->deleteMeasurement($measurementId);
			return $result;
		} catch(\MyntraException $e){
			throw $e;
		}
	}
}