<?php
namespace sizeunification\action;
use base\action\BaseAuthenticatedAuthorizedAction;
require_once $_SERVER["DOCUMENT_ROOT"]."/admin/size_unification_admin_new_function.php";

class SizeUnificationActions extends BaseAuthenticatedAuthorizedAction {
	private $sizeUnificationInstance, $listOfStyles;
	public function __construct(){
		$this->listOfStyles = $this->getRequestVar("styleIds","");
		$this->sizeUnificationInstance = new \SizeUnification($this->listOfStyles, $echoToBeDone = false, $incremental = false);
	}
	public function actionClearMappings(){
		return $this->sizeUnificationInstance->clearMappings();
	}
	public function actionClearMeasurements(){
		return $this->sizeUnificationInstance->clearMeasurements();
	}
	public function actionClearSizeChartImages(){
		return $this->sizeUnificationInstance->clearImage();
	}
	
	public function actionClearStyleCache(){
		return $this->sizeUnificationInstance->clearCache();
	}
	
	public function actionApplyMappings(){
		$message = $this->sizeUnificationInstance->updateSizeMappings();
		/* $result = $this->sizeUnificationInstance->updateSizeStringInMappingRules();
		if($result){
			$message .= "\nSize string was updated on all the mappings successfully.";
		}else {
			$message .= "\nFailed to update Size string in mappings.";
		} */
		return $message;
	}
	public function actionApplyMeasurements(){
		$message = $this->sizeUnificationInstance->updateSizeMeasurements();
		/* $result = $this->sizeUnificationInstance->updateSizeStringInRepresentationRules();
		if($result){
			$message .= "\nSize string was updated on all the measurements successfully.";
		}else {
			$message .= "\nFailed to update Size string in measurements.";
		} */
		return $message;
	}
	public function actionApplySizeChartImages(){
		return $this->sizeUnificationInstance->updateImage();
	}	
	public function actionReApplyMappings(){
		$result = $this->actionClearMappings();
		$result1 = $this->actionApplyMappings();
		if($result == null || $result1 == null || $result['StatusCode'] != '200' || $result['StatusCode'] != '200' 
			|| $result1['numOptions'] == 0 || $result1['numOptions'] < $result['numOptions']){
			$message = "Failed to re-apply mappings." . ($result==null?"":(" ClearMappings-Msg : ".$result['Message'])) . ($result1==null?"":(" ApplyMappings-Msg : ".$result1['Message']));
            $status = "FAILURE";
            $statusCode = "500";
		}else{
			$message = " ClearMappings-Msg : ". $result['Message'] . " \n " . " ApplyMappings-Msg : " . $result1['Message'];
            $status = "SUCCESS";
            $statusCode = "200";
		}
		$res = array('Status' => $status,'StatusCode' => $statusCode,'Message' =>$message,'numOptions' => $result1['numOptions']);
		return $res;
	}
	public function actionReApplyMeasurements(){
		$result = $this->actionClearMeasurements();
		$result1 = $this->actionApplyMeasurements(); //.= "\n".
		if($result == null || $result1 == null || $result['StatusCode'] != '200' || $result['StatusCode'] != '200' 
			|| $result1['numOptions'] == 0 || $result1['numOptions'] < $result['numOptions']){
			$message = "Failed to apply measurements." . ($result==null?"":(" ClearMeasurements-Msg : ".$result['Message'])) . ($result1==null?"":(" ApplyMeasurements-Msg : ".$result1['Message']));
            $status = "FAILURE";
            $statusCode = "500";
		}else{
			$message = " ClearMeasurements-Msg : ". $result['Message'] . "\n" . " ApplyMeasurements-Msg : ". $result1['Message'];
            $status = "SUCCESS";
            $statusCode = "200";
		}
		$res = array('Status' => $status,'StatusCode' => $statusCode,'Message' =>$message,'numOptions' => $result1['numOptions']);
		return $res;
	}
	public function actionReApplySizeChartImages(){
		$result = $this->actionClearSizeChartImages();
		$result1 = $this->actionApplySizeChartImages();
		if($result == null || $result1 == null || $result['StatusCode'] != '200' || $result['StatusCode'] != '200' 
			|| $result1['numOptions'] == 0 || $result1['numOptions'] < $result['numOptions']){
			$message = "Failed to reapply size chart images."  . ($result==null?"":(" ClearSizeChartImage-Msg : ".$result['Message'])) . ($result1==null?"":(" ApplySizeChartImage-Msg : ".$result1['Message']));
            $status = "FAILURE";
            $statusCode = "500";
		}else{
			$message = " ClearSizeChartImage-Msg : ". $result['Message'] . "\n" . " ApplySizeChartImage-Msg : ". $result1['Message'];
            $status = "SUCCESS";
            $statusCode = "200";
        }
        $res = array('Status' => $status,'StatusCode' => $statusCode,'Message' =>$message,'numOptions' => $result1['numOptions']);
		return $res;
	}

	public function actionReUnifyStyles(){
		$result = $this->actionReApplyMappings();
		$result1 = $this->actionReApplyMeasurements();
		$result2 = $this->actionReApplySizeChartImages();
		$result3 = $this->actionClearStyleCache();
		if($result == null || $result1 == null || $result2 == null || $result3 == null || 
			$result['StatusCode'] != '200' || $result1['StatusCode'] != '200' || $result2['StatusCode'] != '200' || $result3['StatusCode'] != '200'){
			$message = "Failed to reunify styles."  . ($result==null?"":(" ReApplyMappings-Msg : ".$result['Message'])) . ($result1==null?"":(" ReApplyMeasurements-Msg : ".$result1['Message'])) . 
		                ($result2==null?"":(" ReApplySizeChartImages-Msg : ".$result2['Message'])) . ($result3==null?"":(" ClearStyleCache-Msg : ".$result3['Message'])) ;
            $status = "FAILURE";
            $statusCode = "500";
		}else{
			$message = " ReApplyMappings-Msg : ".$result['Message'] . "\n" . " ReApplyMeasurements-Msg : ". $result1['Message']  . "\n" . " ReApplySizeChartImages-Msg : ". $result2['Message']  . "\n" . " ClearStyleCache-Msg : ". $result3['Message'];
            $status = "SUCCESS";
            $statusCode = "200";
		}
		$numOptions = min(($result==null || $result['numOptions']==null)?0:$result['numOptions'], ($result1==null || $result1['numOptions']==null)?0:$result1['numOptions'],
			($result2==null || $result2['numOptions']==null)?0:$result2['numOptions'],($result==null || $result3['numOptions']==null)?0:$result3['numOptions']);

		$res = array('Status' => $status,'StatusCode' => $statusCode,'Message' =>$message,'numOptions' => $numOptions);

		return $res;
	}
}