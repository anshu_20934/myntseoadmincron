<?php
namespace sizeunification\dao;;
use base\dao\BaseDAO;
/**
 * DAO for size_chart_scale and size_chart_scale_value tables
 * 
 * Basically, for each Article Type (e.g. Casual Shoes), we store the Size Units (e.g. UK, US, EURO) in size-chart_scale 
 * Against each scale, we store other details like: scale-name, human-readable-scale-name, backward-compatible-scale-name
 * backward-compatible-scale-name is used in contanation with scale-value (e.g. UK combined with 8) to form the size (UK8)
 * At times, I've seen that scale value itself contains scale like 1-2Y,0-6M (in cases where age is mentioned), because 
 * concatenation is blindly scale-name followed by scale-value. 
 * Currently, it is not possible to concatenate conditionally: prefix scale-name or postfix scale-name: it is always prefix.
 * But since it concerns only age right now, we're not giving it too much importance.
 * //TODO: have a boolean flag stored in size-unification-rules table which decides the order of concatenation of Scale Name and Scale Value
 * 
 * @author kundan
 *
 */
class SizeScaleDAO extends BaseDAO {
	
	private $tbl;
	
	public function __construct(){
		$this->tbl = "size_chart_scale";
	}
	
	public function getScalesForArticleType($articleTypeId,$asCombo = false,$unified=false){
		
		if(!$asCombo){
			$columns=array("id as scaleid","scale_name as name","id_catalogue_classification_fk as articletypeid",
						   "human_readable_scale_name as human_readable_scale_name","backward_compat_scale_name as backward_compat_scale_name");
		}
		else {
			if($unified){
				$columns=array("id as unifiedscaleid","scale_name as unifiedscale");
			}else {
				$columns=array("id as scaleid","scale_name as scale");
			}
		}
		return func_select_columns_query(
			$tbl=$this->tbl,
			$cols=$columns, 
			$filters=array("id_catalogue_classification_fk"=>$articleTypeId), 
			$start=null, $count=null, $orders=null, 
			$fetchMode="assoc");
	}
	
	public function insertScale($scaleArr){
		$replaceArr = array("scaleid"=>"id", "name"=>"scale_name","articletypeid"=>"id_catalogue_classification_fk");
		$scaleArrRep = replaceKeys($scaleArr, $replaceArr);
		unset($scaleArrRep["id"]);
		return func_array2insertWithTypeCheck($this->tbl, $scaleArrRep);
	}
	
	public function updateScale($scaleArr){
		$replaceArr = array("scaleid"=>"id", "name"=>"scale_name","articletypeid"=>"id_catalogue_classification_fk");
		$scaleArrRep = replaceKeys($scaleArr, $replaceArr);
		$where = array("id" => $scaleArrRep["id"]);
		unset($scaleArrRep["id"]);
		return func_array2updateWithTypeCheck($this->tbl, $scaleArrRep, $where);
	}
	
	public function deleteScale($scaleid,$articleTypeId){
		if(!$this->checkForScaleReferencesBeforeDelete($scaleid)){
			$criteria = array("id"=>$scaleid, "id_catalogue_classification_fk"=>$articleTypeId);
			return func_array2deleteWithTypeCheck($this->tbl, $criteria);
		} else {
			throw new \MyntraException("Cannot delete this Scale - References exist to this scale");
		}
	}
	
	public function checkForScaleReferencesBeforeDelete($scaleid){
		$criteria = array("id_size_chart_scale_fk"=>$scaleid);
		$results = func_select_columns_query("size_chart_scale_value","count(*) as numrefs",$criteria);
		$refsOfScaleId = $results[0]["numrefs"];
		return $refsOfScaleId > 0;
	}
}