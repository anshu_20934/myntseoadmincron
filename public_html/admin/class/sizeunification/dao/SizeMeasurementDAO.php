<?php
namespace sizeunification\dao;
use base\dao\BaseDAO;
/**
 * DAO for size measurement rules: mk_size_representation_rule 
 * @author kundan
 */
class SizeMeasurementDAO extends BaseDAO {
	private $tbl;
	
	public function __construct(){
		$this->tbl = "mk_size_representation_rule";
	}
	public function getMeasurementsForArticleTypeBrandAgeGroup($articleTypeId,$brandId,$ageGroup,$styleId){
		
		$sql = "SELECT rep.id as measurementid,rep.article_type_id as articletypeid,rep.brand_id as brandid,rep.age_group as agegroup,
				rep.style_id as styleid,sv.id as sizeid,rep.size as size,jsonOutput as measurement
				FROM {$this->tbl} rep inner join size_chart_scale_value sv on rep.sizeValueId = sv.id
				  inner join size_chart_scale sc on sv.id_size_chart_scale_fk = sc.id
				WHERE rep.article_type_id = $articleTypeId AND rep.brand_id = $brandId AND rep.age_group ='{$ageGroup}'";
		if(!empty($styleId)){
			$sql .= " AND rep.style_id = $styleId";
		}
		else {
			$sql .= " AND rep.style_id is null";
		}
		return func_query($sql);
	}

	public function doMeasurementsExist($articleTypeId,$brandId,$ageGroup,$styleId){
		$sql = "SELECT count(ur.id) as measurementcount
					FROM {$this->tbl} ur 
					WHERE ur.article_type_id = $articleTypeId AND ur.brand_id = $brandId AND ur.age_group = '{$ageGroup}'";
		if(!empty($styleId)){
			$sql .=" AND ur.style_id = $styleId";
		}else {
			$sql .=" AND ur.style_id IS NULL";
		}
		$result = $this->getRAWData($sql,true);
		return $result["measurementcount"];
	}
	
	public function getSize($sizeValueIds){
		if(is_array($sizeValueIds)){
			$sizeValueIds = implode(",", $sizeValueIds);
		}
		$sql = "select scval.id as sizeid, concat(sc.backward_compat_scale_name,scval.size_value) as concatsize 
				from size_chart_scale_value scval inner join size_chart_scale sc on scval.id_size_chart_scale_fk = sc.id 
				where scval.id in ($sizeValueIds)";
		return getResultsetAsPrimaryKeyValuePair($sql, "sizeid", "concatsize");  
	}
	
	public function getExistingUnifiedScale($articleTypeId,$brandId,$ageGroup){
		$sql = "select distinct unisc.scale_name as unifiedscale,unisc.id as unifiedscaleid
				from mk_size_unification_rules uni
				inner join size_chart_scale_value uniscval on uni.unifiedSizeValueId = uniscval.id
				inner join size_chart_scale unisc on unisc.id = uniscval.id_size_chart_scale_fk
				where uni.article_type_id = '{$articleTypeId}' and uni.brandId = '{$brandId}' and uni.age_group = '{$ageGroup}'";
		return func_query($sql);
	}
	
	public function insertMeasurement($measurementArr){
		if(func_array2insertWithTypeCheck($this->tbl, $measurementArr)){
			return db_affected_rows();	
		}
		else { 
			return false;
		}
	}
	
	public function updateMeasurement($measurementId,$sizeValueId,$size,$measurement){
		$set = array("sizeValueId" => $sizeValueId, "size"=> $size, "jsonOutput"=> array("type"=> "json", value=> $measurement));
		$where = array("id"=>$measurementId);
		if(func_array2updateWithTypeCheck($this->tbl, $set, $where)){
			return db_affected_rows();	
		}
		else { 
			return false;
		}
	}
	
	public function deleteMeasurement($measurementId){
			$criteria = array("id"=>$measurementId);
			return func_array2deleteWithTypeCheck($this->tbl, $criteria);
	}
	
	public function checkForMeasurementReferencesBeforeDelete($scaleid){
		$criteria = array("id_size_chart_scale_fk"=>$scaleid);
		$results = func_select_columns_query("size_chart_scale_value","count(*) as numrefs",$criteria);
		$refsOfMeasurementId = $results[0]["numrefs"];
		return $refsOfMeasurementId > 0;
	}
	
	public function getStyleDetails($styleId, $vendorArticleNo=null){
		$sql = "select sp.style_id as styleid, sp.article_number as vendorarticlenumber, 
				sp.global_attr_article_type as articletypeid,av.id as brandid, sp.global_attr_age_group as agegroup
				from mk_style_properties sp
				inner join mk_attribute_type_values av on (av.attribute_type = 'Brand' and av.attribute_value = sp.global_attr_brand)
				inner join mk_catalogue_classification cc on cc.id = sp.global_attr_article_type";
		if(isset($styleId)&& strlen($styleId)>0){
			$sql.= " where sp.style_id = '{$styleId}'"; 	
		}elseif (isset($vendorArticleNo) && strlen($vendorArticleNo)>0){
			$sql.= " where sp.article_number = '{$vendorArticleNo}'";
		}
		return $this->getRAWData($sql);
	}
	
	public function deleteMeasurements($articleTypeId,$brandId,$ageGroup,$styleId){
		$criteria = array("article_type_id"=>$articleTypeId,"brand_id"=>$brandId,"age_group"=>$ageGroup);
		if(!empty($styleId)){
			$criteria["style_id"] = $styleId;
		}else {
			$criteria["style_id"] = null;
		}
		return func_array2deleteWithTypeCheck($this->tbl, $criteria);
	}
	
	public function copyMeasurements($src,$dest){
		$criteria = array("article_type_id"=>$src["articleType"],"brand_id"=>$src["brand"],"age_group"=>$src["ageGroup"]);
		if(!empty($src["styleId"])){
			$criteria["style_id"] = $src["styleId"];
		}else {
			$criteria["style_id"] = null;
		}
		$whereClause = getSQLWhereClause($criteria);
	
		if(empty($dest["styleId"])){
			$dest["styleId"] = "null";
		}
		$sql = "INSERT INTO ".$this->tbl."(article_type_id,brand_id,age_group,style_id,sizeValueId,size,jsonOutput,article_type_attribute_value_id)
					(SELECT {$dest["articleType"]},{$dest["brand"]},'{$dest["ageGroup"]}',{$dest["styleId"]}, 
							sizeValueId,size,jsonOutput,article_type_attribute_value_id
					FROM ".$this->tbl.$whereClause.")";
		db_exec($sql);
		return db_affected_rows();
	}
	
}