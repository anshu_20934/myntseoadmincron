<?php
namespace sizeunification\dao;;
use base\dao\BaseDAO;
/**
 * DAO for size_chart_scale_value tables
 * 
 * Basically, for each Scale, there is a list of scale-values allowed.
 * e.g. in UK Size the size values allowed are: 3,4,5,6,7,8,9,10 etc. 
 * There is an interesting filed called intra_scale_size_order which determines the sort-order of the scale value within this scale.
 * @author kundan
 *
 */
class SizeScaleValueDAO extends BaseDAO {
	var $tbl = "size_chart_scale_value";
	
	public function getScaleValuesForScale($scaleId,$asCombo=false,$unified=false){
		if(!$asCombo){
			$columns=array("id as scalevalueid","size_value as sizevalue","id_size_chart_scale_fk as scaleid",
							"intra_scale_size_order as sortorder");
		}
		else {
			$columns = $unified ? array("id as unifiedsizeid","size_value as unifiedsize") :
									array("id as sizeid","size_value as size"); 
		}
		return func_select_columns_query(
			$tbl=$this->tbl,
			$cols=$columns, 
			$filters=array("id_size_chart_scale_fk"=>$scaleId), 
			$start=null, $count=null, $orders="intra_scale_size_order", 
			$fetchMode="assoc");
	}
	
	public function insertSize($scaleArr){
		$scaleArr["sortorder"] = $this->getSortOrderForInsert($scaleArr["scaleid"]);
		$replaceArr = array("scaleid"=>"id_size_chart_scale_fk", "sizevalue"=>"size_value","sortorder"=>"intra_scale_size_order");
		$scaleArrRep = replaceKeys($scaleArr, $replaceArr);
		$scaleArrRep["intra_scale_size_order"] = $this->getSortOrderForInsert($scaleArr["scaleid"]);
		unset($scaleArrRep["scalevalueid"]);
		return func_array2insertWithTypeCheck($this->tbl, $scaleArrRep);
	}
	
	public function updateSize($scaleArr){
		$replaceArr = array("scalevalueid"=>"id", "sizevalue"=>"size_value","scaleid"=>"id_size_chart_scale_fk","sortorder"=>"intra_scale_size_order");
		$scaleArrRep = replaceKeys($scaleArr, $replaceArr);
		$where = array("id" => $scaleArrRep["id"]);
		unset($scaleArrRep["id"]);
		return func_array2updateWithTypeCheck($this->tbl, $scaleArrRep, $where);
	}
	
	public function deleteSize($sizeScaleValueId){
		if(!$this->checkForSizeReferencesBeforeDelete($sizeScaleValueId)){
			$criteria = array("id"=>$sizeScaleValueId);
			return func_array2deleteWithTypeCheck($this->tbl, $criteria);
		} else {
			throw new \MyntraException("Cannot delete this Size - References exist to this size");
		}
	}
	
	public function checkForSizeReferencesBeforeDelete($scaleValueId){
		$sql = "select count(*) from (
					select id from mk_size_unification_rules where sizeValueId = '{$scaleValueId}' or unifiedSizeValueId = '{$scaleValueId}'
					union
					select id from mk_size_representation_rule where sizeValueId = '{$scaleValueId}' ) tbl";
		$numReferences = func_query_first_cell($sql,true);
		return $numReferences > 0;
	}
	
	public function getSortOrderForInsert($scaleId){
		$res = func_select_columns_query(
			$tbl=$this->tbl,
			$columns=array("max(intra_scale_size_order) as max_sort_order"), 
			$filters=array("id_size_chart_scale_fk"=>$scaleId), 
			$start=0, $count=1, $orders = null, $fetchMode = "array");
		return (isset($res) && $res[0][0] >=0) ? $res[0][0]+1 : 0;
	}
	
	/**
	 * This would be used for Move-Up operation typically
	 * Finds the highest sort order less than a given sort order for a particular size-scale
	 * If any sort order less than givenSortOrder is not found, then it returns the given sort-order only
	 * @param unknown_type $sizeScaleId
	 * @param unknown_type $givenSortOrder
	 */
	public function getMaxSortOrderLessThanGiven($sizeScaleId,$givenSortOrder) {
		$projection = "intra_scale_size_order as sortorder, id as sizevalueid";
		$whereClause = "id_size_chart_scale_fk = $sizeScaleId and intra_scale_size_order < $givenSortOrder";
		$sql = "SELECT $projection FROM {$this->tbl} WHERE $whereClause ORDER BY intra_scale_size_order DESC LIMIT 1";
		$val = func_query_first($sql);
		return $val;
	}
	
	/**
	 * This would be used for Move-Down operation typically
	 * Finds the highest sort order less than a given sort order for a particular size-scale
	 * If any sort order less than givenSortOrder is not found, then it returns the given sort-order only
	 * @param unknown_type $sizeScaleId
	 * @param unknown_type $givenSortOrder
	 */
	public function getMinSortOrderGreaterThanGiven($sizeScaleId,$givenSortOrder) {
		$projection = "intra_scale_size_order as sortorder, id as sizevalueid";
		$whereClause = "id_size_chart_scale_fk = $sizeScaleId and intra_scale_size_order > $givenSortOrder";
		$sql = "SELECT $projection FROM {$this->tbl} WHERE $whereClause ORDER BY intra_scale_size_order ASC LIMIT 1";
		$val = func_query_first($sql);
		return $val;
	}
	
	/**
	* Given two size-values and their sort-orders, interchange their sort-orders
	* This is typically required in move-up or move-down operation
	* @param unknown_type $imageId1
	* @param unknown_type $sortOrder1
	* @param unknown_type $imageId2
	* @param unknown_type $sortOrder2
	*/
	public function swapSortOrders($sizeValueId1,$sortOrder1,$sizeValueId2,$sortOrder2) {
		$tempSortOrder = -1;
		db_query("SET AUTOCOMMIT=0");
		db_query("START TRANSACTION");
		$wasSuccessful1 = $this->updateSortOrder($sizeValueId1, $tempSortOrder);
		$wasSuccessful2 = $this->updateSortOrder($sizeValueId2, $sortOrder1);
		$wasSuccessful3 = $this->updateSortOrder($sizeValueId1, $sortOrder2);
		$wasSuccessful = $wasSuccessful1 && $wasSuccessful2 && $wasSuccessful3;
		if($wasSuccessful) {
			db_query("COMMIT");
			db_query("SET AUTOCOMMIT=1");
			return true;
		} else {
			db_query("ROLLBACK");
			db_query("SET AUTOCOMMIT=1");
			return false;
		}
	}
	
	public function updateSortOrder($sizeValueId, $sortOrder) {
		$toUpdate = array("intra_scale_size_order"=>$sortOrder);
		$whereClause = array("id"=>$sizeValueId);
		return func_array2updateWithTypeCheck($this->tbl, $toUpdate, $whereClause);
	}
}