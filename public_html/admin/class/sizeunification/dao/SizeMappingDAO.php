<?php
namespace sizeunification\dao;
use base\dao\BaseDAO;
/**
 * DAO for size mapping rules: mk_size_unification_rules
 * 
 * @author kundan
 *
 */
class SizeMappingDAO extends BaseDAO {
	private $tbl;
	
	public function __construct(){
		$this->tbl = "mk_size_unification_rules";
	}
	public function getMappingsForArticleTypeBrandAgeGroup($articleTypeId,$brandId,$ageGroup,$styleId){
		$sql = "SELECT ur.id as mappingid,ur.article_type_id as articletypeid,ur.brandId as brandid,ur.age_group as agegroup,ur.styleID as styleid,
				sc.id as scaleid,sc.scale_name as scale,sv.id as sizeid,sv.size_value as size,usc.id as unifiedscaleid,usc.scale_name as unifiedscale,
				usv.id as unifiedsizeid,usv.size_value as unifiedsize
				FROM {$this->tbl} ur inner join size_chart_scale_value sv on ur.sizeValueId = sv.id
				inner join size_chart_scale_value usv on ur.unifiedSizeValueId = usv.id
				inner join size_chart_scale sc on sv.id_size_chart_scale_fk = sc.id
				inner join size_chart_scale usc on usv.id_size_chart_scale_fk = usc.id
				WHERE ur.article_type_id = $articleTypeId AND ur.brandId = $brandId AND ur.age_group = '{$ageGroup}'";
		if(!empty($styleId)){
			$sql .=" AND styleID = $styleId";
		}else {
			$sql .=" AND styleID IS NULL";
		}
		return $this->getRAWData($sql);
	}
	
	public function doMappingsExist($articleTypeId,$brandId,$ageGroup,$styleId){
		$sql = "SELECT count(ur.id) as mappingcount
				FROM {$this->tbl} ur 
				WHERE ur.article_type_id = $articleTypeId AND ur.brandId = $brandId AND ur.age_group = '{$ageGroup}'";
		if(!empty($styleId)){
			$sql .=" AND ur.styleID = $styleId";
		}else {
			$sql .=" AND ur.styleID IS NULL";
		}
		$result = $this->getRAWData($sql,true);
		return $result["mappingcount"];
	}
	
	public function getSize($sizeValueIds){
		if(is_array($sizeValueIds)){
			$sizeValueIds = implode(",", $sizeValueIds);
		}
		$sql = "select scval.id as sizeid, concat(sc.backward_compat_scale_name,scval.size_value) as concatsize 
				from size_chart_scale_value scval inner join size_chart_scale sc on scval.id_size_chart_scale_fk = sc.id 
				where scval.id in ($sizeValueIds)";
		return getResultsetAsPrimaryKeyValuePair($sql, "sizeid", "concatsize"); 
	}
	
	public function getExistingUnifiedScale($articleTypeId,$brandId,$ageGroup){
		$sql = "select distinct unisc.scale_name as unifiedscale,unisc.id as unifiedscaleid
				from mk_size_unification_rules uni
				inner join size_chart_scale_value uniscval on uni.unifiedSizeValueId = uniscval.id
				inner join size_chart_scale unisc on unisc.id = uniscval.id_size_chart_scale_fk
				where uni.article_type_id = '{$articleTypeId}' and uni.brandId = '{$brandId}' and uni.age_group = '{$ageGroup}'";
		return $this->getRAWData($sql);
	}
	
	public function insertMapping($mappingArr){
		return func_array2insertWithTypeCheck($this->tbl, $mappingArr);
	}
	
	public function updateMapping($mappingId,$sizeValueId,$size,$unifiedSizeValueId,$unifiedSize){
		$setClause = array("sizeValueId"=>$sizeValueId, "unifiedSizeValueId"=>$unifiedSizeValueId, "size"=>$size, "unifiedSize"=>$unifiedSize);
		$where = array("id" => $mappingId);
		return func_array2updateWithTypeCheck($this->tbl, $setClause, $where);
	}
	
	public function deleteMapping($mappingId){
			$criteria = array("id"=>$mappingId);
			return func_array2deleteWithTypeCheck($this->tbl, $criteria);
	}
	
	public function deleteMappings($articleTypeId,$brandId,$ageGroup,$styleId){
		$criteria = array("article_type_id"=>$articleTypeId,"brandId"=>$brandId,"age_group"=>$ageGroup);
		if(!empty($styleId)){
			$criteria["styleID"] = $styleId;
		}else {
			$criteria["styleID"] = null;
		}
		return func_array2deleteWithTypeCheck($this->tbl, $criteria);
	}
	
	public function copyMappings($src,$dest){
		$criteria = array("article_type_id"=>$src["articleType"],"brandId"=>$src["brand"],"age_group"=>$src["ageGroup"]);
		if(!empty($src["styleId"])){
			$criteria["styleID"] = $src["styleId"];
		}else {
			$criteria["styleID"] = null;
		}
		$whereClause = getSQLWhereClause($criteria);
		
		if(empty($dest["styleId"])){
			$dest["styleId"] = "null";
		}		
		$sql = "INSERT INTO ".$this->tbl."(article_type_id,brandId,age_group,styleID,sizeValueId,size,unifiedsizevalueid,unifiedsize)
				(SELECT {$dest["articleType"]},{$dest["brand"]},'{$dest["ageGroup"]}',{$dest["styleId"]}, 
						sizeValueId,size,unifiedsizevalueid,unifiedsize
				FROM ".$this->tbl.$whereClause.")";
		db_exec($sql);
		return db_affected_rows();
	}
	
	public function findMatchingStyles($articleTypeId,$brandId,$ageGroup, $styleId){
		$sql = "select sp.style_id as styleid, sp.article_number as vendorarticlenumber, sp.product_display_name as displayname, 
				cc.typename as articleType,sp.global_attr_brand as brand, sp.global_attr_age_group as ageGroup,ps.styletype as status,
				group_concat(po.value SEPARATOR ' ') as sizes,
				group_concat(po.unifiedSize SEPARATOR ' ') as mappedsizes,
				count(po.value) as numsizes,
				count(po.unifiedSize) as nummappedsizes,
				count(po.size_representation) as nummeasuredsizes,
				case when (sp.size_representation_image_url is null) then 0 else 1 end as isunified
				from mk_style_properties sp 
				inner join mk_product_style ps on sp.style_id = ps.id
				inner join mk_attribute_type_values av on (av.attribute_type = 'Brand' and av.attribute_value = sp.global_attr_brand)
				inner join mk_catalogue_classification cc on cc.id = sp.global_attr_article_type
				inner join mk_product_options po on po.style = sp.style_id
				inner join mk_styles_options_skus_mapping skumap on skumap.option_id = po.id
				where sp.global_attr_article_type = '{$articleTypeId}' and av.id = '{$brandId}' and sp.global_attr_age_group = '{$ageGroup}'";
		if(!empty($styleId)){
			$sql .= " and sp.style_id = '{$styleId}'";
		}
		$sql .= " group by sp.style_id";
		return $this->getRAWData($sql);		
	}
	
	public function getStyleDetails($styleId, $vendorArticleNo=null){
		$sql = "select sp.style_id as styleid, sp.article_number as vendorarticlenumber, 
					   sp.global_attr_article_type as articletypeid,cc.typename as articletype,
					   av.id as brandid,sp.global_attr_brand as brand,
					   sp.global_attr_age_group as agegroup
				from mk_style_properties sp
				inner join mk_attribute_type_values av on (av.attribute_type = 'Brand' and av.attribute_value = sp.global_attr_brand)
				inner join mk_catalogue_classification cc on cc.id = sp.global_attr_article_type";
		if(isset($styleId)&& strlen($styleId)>0){
			$sql.= " where sp.style_id = '{$styleId}'"; 	
		}elseif (isset($vendorArticleNo) && strlen($vendorArticleNo)>0){
			$sql.= " where sp.article_number = '{$vendorArticleNo}'";
		}
		return $this->getRAWData($sql);
	}
	
	public function getUnifiedSizes($articleTypeId,$brandId,$ageGroup, $styleId){
		$sql = "select DISTINCT unifiedSizeValueId as unifiedsizeid, unifiedSize as unifiedsize
				from mk_size_unification_rules 
				where brandId = '{$brandId}' and article_type_id = '{$articleTypeId}' and age_group = '{$ageGroup}'";
		if(isset($styleId)&& strlen($styleId)>0){
			$sql.= " and styleID = '{$styleId}'";
		}
		return $this->getRAWData($sql);
	}
	
}