<?php
namespace sizeunification\enums;
use enums\base\ActionType;

abstract class SizeUnificationActionType extends ActionType {
	const FetchUnifiedScales = "fetchunifiedscales";	
	const FetchStyleDetails = "fetchstyledetails";
	const FetchMatchingStyles = "fetchmatchingstyles";
	const FetchUnifiedSizes = "fetchunifiedsizes";
	
	const CopyMappings = "copymappings";
	const CopyMeasurements = "copymeasurements";
	
	const ClearMappingsForStyles = "clearmappingsforstyles";
	const ClearMeasurementsForStyles = "clearmeasurementsforstyles";
	
	const ClearCacheForStyles = "clearcacheforstyles";
	
	const ReApplyMappingsForStyles = "reapplymappingsforstyles";
	const ReApplyMeasurementsForStyles = "reapplymeasurementsforstyles";
	const ReApplyImageForStyles = "reapplyimageforstyles";

	const ReUnifyStyles = "reunifystyles";
}