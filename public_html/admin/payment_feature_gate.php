<?php
require "./auth.php";
require_once(dirname(__FILE__)."/../env/RemoteTimeouts.Config.php");

require $xcart_dir."/include/security.php";
include_once("$xcart_dir/include/class/class.feature_gate.keyvaluepair.php");
$baseURL= HostConfig::$paymentServiceInternalHost."/admin";
if($object=='keyvaluepairs'){
	
	if($mode=='add'){

		$newkey = $_POST['key_field_new'];
		$newvalue = $_POST['value_field_new'];
		$newdesc = $_POST['description_field_new'];
		
		$postdata = array();
		$postdata["name"] = $newkey;
		$postdata["value"] = $newvalue;
		$postdata["description"] = $newdesc;
		$postdata = json_encode($postdata);
 
		$url = $baseURL."/featuregate/";
		
		$ch = curl_init($url);
		curl_setopt($ch,CURLOPT_HTTPHEADER,array('user:'.$login,'Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_POST      ,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS    ,$postdata);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
		curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
		curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1); // RETURN THE CONTENTS OF THE CALL
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, \RemoteTimeouts::$localDCConnectTimeout);
		curl_setopt($ch, CURLOPT_TIMEOUT, \RemoteTimeouts::$paymentCurlTimeout);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
		
		$response = curl_exec($ch);
		curl_close($ch);

	} else if($mode == 'delete') {
		
		$keyid = $_POST['delete_id'];
		$key = $_POST['delete_name'];
		
		$url = $baseURL."/featuregate/".$keyid;
		$ch = curl_init($url);
		
		curl_setopt($ch,CURLOPT_HTTPHEADER,array('user:'.$login));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
		curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
		curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1); // RETURN THE CONTENTS OF THE CALL
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, \RemoteTimeouts::$localDCConnectTimeout);
		curl_setopt($ch, CURLOPT_TIMEOUT, \RemoteTimeouts::$paymentCurlTimeout);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
		
		$response = curl_exec($ch);
		
	
	} else if($mode == 'update') {
		
		$updateId = $_POST['update_id'];
		$updateName = $_POST['update_name'];
		$updateValue = $_POST['update_value'];
		$updateDesc = $_POST['update_desc'];
		
		$postdata = array();
		$postdata["name"] = $updateName;
		$postdata["value"] = $updateValue;
		$postdata["description"] = $updateDesc;
		$postdata = json_encode($postdata);
		$url = $baseURL."/featuregate/".$updateId;

		$ch = curl_init($url);
		curl_setopt($ch,CURLOPT_HTTPHEADER,array('user:'.$login,'Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_POST      ,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS    ,$postdata);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
		curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
		curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1); // RETURN THE CONTENTS OF THE CALL
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, \RemoteTimeouts::$localDCConnectTimeout);
		curl_setopt($ch, CURLOPT_TIMEOUT, \RemoteTimeouts::$paymentCurlTimeout);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
		
		$response = curl_exec($ch);
		curl_close($ch);
		
	}

	//$results=func_query("select * from mk_feature_gate_key_value_pairs");
	//print_r($results);
	$url = $baseURL."/featuregate/"; 
	$ch = curl_init($url);
	
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
	curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
	curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1); // RETURN THE CONTENTS OF THE CALL
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, \RemoteTimeouts::$localDCConnectTimeout);
	curl_setopt($ch, CURLOPT_TIMEOUT, \RemoteTimeouts::$paymentCurlTimeout);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
	
	$response = curl_exec($ch);
	
	curl_close($ch);
	$response = trim($response);
	$results=json_decode($response, true);
	
	$smarty->assign("payment_keyvaluepairs",  $results);
	$smarty->assign("payment_action_php","payment_feature_gate.php?object=keyvaluepairs");
	$smarty->assign("main","payment_keyvaluepairs_common");
	
	func_display("admin/home.tpl",$smarty);

	exit;
}


