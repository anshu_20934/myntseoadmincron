<?php
include_once("auth.php");
require_once $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.utilities.php";
require_once $xcart_dir."/include/solr/solrStatsSearch.php";

$key_id=$_POST['keyid'];
$page_title=$_POST['page_title'];
$page_h1=$_POST['page_h1'];
$meta_description=$_POST['meta_description'];
$meta_keyword=$_POST['meta_keyword'];
$is_valid=$_POST['is_valid'];
$description=$_POST['description'];
$page_promotion=$_POST['page_promotion'];

if(!empty($key_id)){
    $statsInfo = func_query_first("select page_promotion,keyword from mk_stats_search where key_id='".$key_id."'");
    
    $queryData = array(
                    "page_title"=>$page_title,
                    "page_h1"=>$page_h1,
                    "meta_description"=>$meta_description,
                    "meta_keyword"=>$meta_keyword,
                    "is_valid"=>$is_valid,
                    "description"=>$description,
                    "page_promotion"=>$page_promotion,
                );
    func_array2update('mk_stats_search', sanitizeDBValues($queryData),"key_id='".$key_id."'");

    //updating solr
    //$solrSearch = new solrStatsSearch();
    //$solrSearch->update_document(trim($key_id));

    //send mail in case html banner(page_promotion) changes
    if($statsInfo['page_promotion'] != $page_promotion){
        $mail_detail = array(
                    "to"=>"pm@myntra.com",
                    "subject"=>"Myntra HTML banner update",
                    "content"=>"<br/>".$identifiers['A']['firstname']." updated a HTML banner for <a href='$http_location/".$statsInfo['keyword']."'>$http_location/".$statsInfo['keyword']."</a><br/>",
                    "from_name"=>$identifiers['A']['firstname'],
                    "from_email"=>'noreply@myntra.com',
                    "header"=>"Content-Type: text/html; charset=ISO-8859-1 \n"
                );
        send_mail_on_domain_check($mail_detail,$check_domain=true);
    }
}
?>
