<?php
require_once "./auth.php";

include_once("../include/func/func.db.php");
include_once("../sanitizept.inc.php");
include_once("../include/func/func.mail.php");
include_once("../include/func/func.order.php");
include_once("$xcart_dir/include/class/oms/OrderWHManager.php");
include_once("$xcart_dir/include/class/oms/EventCreationManager.php");

$orderid= $HTTP_POST_VARS['orderid'];
$dateadded= $HTTP_POST_VARS['dateadded'];
$addedby= $HTTP_POST_VARS['addedby'];
$commenttype= $HTTP_POST_VARS['commenttype'];
$commenttitle= $HTTP_POST_VARS['commenttitle'];
$commentdetails=$HTTP_POST_VARS['commentdetails'];
$new_address=$HTTP_POST_VARS['new_address'];
$country = $HTTP_POST_VARS['country'];
$state = $HTTP_POST_VARS['state'];
$state_text = $HTTP_POST_VARS['state-text'];
$city = $HTTP_POST_VARS['city'];
$pincode = $HTTP_POST_VARS['pincode'];
$mobile = $HTTP_POST_VARS['mobile'];
$recipient_name = $HTTP_POST_VARS['recipient_name'];
$email = $HTTP_POST_VARS['email'];
$locality = $HTTP_POST_VARS['locality'];
$childorderids = $HTTP_POST_VARS['childorderids'];

$gift_wrap_message=$HTTP_POST_VARS['gift_wrap_message'];
$changerequest=$HTTP_POST_VARS['changerequest'];
$onhold=$HTTP_POST_VARS['onhold'];
//if(!empty($HTTP_POST_FILES['attachment'])){
$imagefile_name = $HTTP_POST_FILES['attachment']['name'];
//$commentdetails = sanitize_paranoid_string($commentdetails);
$action=$HTTP_POST_VARS['action'];
if($action == 'validate_zipcode'){
    $payment_method = $_POST['payment_method'];
    $returnAry = array();
    $state = $_POST['state'];
    $order_details = func_query_first("select * from xcart_orders where orderid = $orderid");
    $productsInCart = func_create_array_products_of_order($orderid);
    if($order_details['ordertype'] == 'ex'){
        //$supported_couriers = func_check_exchange_serviceability($pincode);
        $returnAry['serviceable'] = func_check_exchange_serviceability_products($pincode, $productsInCart, $order_details['warehouseid'])?true:false;
    } else {
        //$supported_couriers = func_get_serviceable_couriers($pincode, $payment_method, 'IN');
        //$supported_couriers = func_get_serviceable_couriers($pincode, $order_details['payment_method'], $productsInCart,$order_details['warehouseid']);
        if($order_details['payment_method'] == 'cod') {
            $couriers = func_get_order_serviceabile_courier('cod', $pincode, $productsInCart, $order_details['warehouseid'], $order_details['total']);
            $returnAry['serviceable'] = ($couriers && count($couriers)>0)?true:false;
        } else {
            $couriers = func_get_order_serviceabile_courier('on', $pincode, $productsInCart, $order_details['warehouseid'], $order_details['total']);
            $returnAry['serviceable'] = ($couriers && count($couriers)>0)?true:false;
        }
    }
    /* if($supported_couriers && count($supported_couriers) > 0){
        $returnAry['serviceable'] = 'true';
    }else{
        $returnAry['serviceable'] = 'false';
    } */

    echo json_encode($returnAry);
    exit;
}

if(!empty($imagefile_name)){
    $image_name = basename($imagefile_name);

    $imagefile_size = $HTTP_POST_FILES['attachment']['size'];
    $imagefile_tmp_name = $HTTP_POST_FILES['attachment']['tmp_name'];

    //upload the image file to data directory
    $data_dir = "data";
    $max_filesize = "10485760";

    if($imagefile_size <= 0) die ("$image_name is empty.");

    if($imagefile_size > $max_filesize) {
        echo "Error: $image_name is too big. " .
            number_format($max_filesize) . "bytes is the limit.";
    }

    if(!@move_uploaded_file($imagefile_tmp_name, "$data_dir/$image_name"))
        echo "Can't copy $imagefile_name to $file_name.";
}
//finally save file data into database
$time=time();
$allowAddressChange = true;

//$order_status_inDB=func_get_order_status($orderid);
$order_releases_status=func_get_group_orders_status($orderid);
foreach($order_releases_status as $release_status) {
    if(!($release_status['status'] == 'OH' || $release_status['status'] == 'Q' || $release_status['status'] == 'WP' || $release_status['status'] == 'RFR')){
        //one of the releases already moved ahead. don't allow address change
        $allowAddressChange = false;
        break;
    }
}

if($changerequest=="Y" && ( $order_status_inDB == 'C')){
    $smarty->assign("commentSuccess","N");
    func_display("addOrder_changerequest.tpl",$smarty);
} else{
    if($new_address == "" || ($new_address != "" && $allowAddressChange)) {
        $old_address_query = "SELECT CONCAT(s_firstname,' ',s_lastname) customer_name,login,s_address,s_country,s_state,s_city,s_zipcode,mobile,s_email FROM xcart_orders WHERE orderid=".$orderid;
        $old_address_result = func_query($old_address_query);

        $customer_info_query = "SELECT CONCAT(customer.firstname, ' ', customer.lastname) customer_name
	    		FROM xcart_customers customer LEFT JOIN xcart_orders orders ON customer.login = orders.login 
	    		WHERE orders.orderid = " . $orderid;

        $customer_info_result = func_query($customer_info_query);

        $update_params = "";
        $full_address = "";
        if($recipient_name != ""){
            $update_params .= ", s_firstname = '".$recipient_name . "'";
            $full_address = $full_address.$recipient_name.",";
        }
        if ($new_address != "") {
            $update_params .= " , s_address = '" . $new_address . "'";
            $full_address = $full_address.$new_address;
        }
        if ($country != "") {
            $update_params .= " , s_country = '" . $country . "'";
            $full_address = $full_address.",".$country;
        }
        if ($state != "") {
            $update_params .= " , s_state = '" . $state . "'";
            $newstate=$state;
            $full_address = $full_address.",".$state;
        } else if ($state_text != "" && $country != "IN") {
            $update_params .= " , s_state = '" . $state_text . "'";
            $newstate = $state_text;
            $full_address = $full_address.",".$state_text;
        }
        if($locality != ""){
            $update_params .= ", s_locality = '".$locality . "'";
            $full_address = $full_address.",".$locality;
        }
        if ($city != "") {
            $update_params .= " , s_city = '" . $city . "'";
            $full_address = $full_address.",".$city;
        }
        if ($pincode != "") {
            $update_params .= " , s_zipcode = '" . $pincode . "'";
            $full_address = $full_address.",".$pincode;
        }
        if($mobile != ""){
            $update_params .= " , mobile = '".$mobile."'";
            $full_address = $full_address.",".$mobile;
        }
        if($email != ""){
            $update_params .= ", s_email = '".$email . "'";
            $full_address = $full_address.",".$email;
        }

        if($childorderids != ""){
            $childorderids .= ','.$orderid;
        }else{
            $childorderids = $orderid;
        }

        $orderids = explode(",", $childorderids);
        if ($update_params != "") {

            $update_params .= ", tracking = null";
            $releases = array();
            foreach ($orderids as $oid) {

                $order_details = func_query_first("Select * from xcart_orders where orderid = $oid", true);
                $serviceType = "DELIVERY";
                if ($order_details['ordertype'] == 'ex') {
                    $serviceType = "EXCHANGE";
                }
                $productsInCart = func_create_array_products_of_order($oid);
                if($order_details['payment_method'] == 'cod') {
                    $couriers = func_get_order_serviceabile_courier('cod', $pincode, $productsInCart, $order_details['warehouseid'], $order_details['total'],$serviceType);
                } else {
                    $couriers = func_get_order_serviceabile_courier('on', $pincode, $productsInCart, $order_details['warehouseid'], $order_details['total'],$serviceType);
                }
                if (!$couriers || $couriers[0] == "") {
                    $smarty->assign("commentSuccess","N");
                    $smarty->assign("errorMsg", "New pincode is not serviceable");
                    func_display("addOrder_changerequest.tpl",$smarty);
                    return;
                }
                if($order_details['shipping_method'] != 'NORMAL' && $old_address_result['s_zipcode'] != $pincode) {
                    db_query("Update xcart_orders set shipping_method = 'NORMAL' where orderid = $oid");
                    func_addComment_order($oid, $addedby, 'SHIPPING_METHOD', 'ADDRESS_CHANGE', 'Shipping method changed from '.$order_details['shipping_method'].' to NORMAL because the address was modified');
                }
                $sql = "UPDATE xcart_orders SET " . substr($update_params, 2) . ", courier_service = '".$couriers[0]."' WHERE orderid = $oid" ;
                db_query($sql);
                
                $release_details = array();
                $release_details['oid'] = $oid;
                $release_details['warehouseid'] = $order_details['warehouseid'];
                $release_details['courier'] = $couriers[0];
                $release_details['payment_method'] = $order_details['payment_method'];
                $release_details['pincode'] = $pincode;
                array_push($releases, $release_details);
                //updateShippingMethodForOrder($oid);
                //EventCreationManager::pushReleaseUpdateEvent($oid, $addedby, $couriers[0], $orderid2TrackingNo[$oid], (empty($recipient_name)?false:$recipient_name),$new_address,$city,$locality, $newstate, $pincode, $country, $mobile, $email);

            }
            \EventCreationManager::pushCompleteOrderEvent($orderids[0]);
            
            foreach ($releases as $release) {
                $whCourier2OrderId[$release['warehouseid'].'__'.$release['courier']] = $release['oid'];
                $orderid2TrackingNo = \OrderWHManager::assignTrackingNumberForOrders($whCourier2OrderId, $release['payment_method'], $release['pincode']);
            }
            if($HTTP_POST_VARS['addToShippingList']=='true'){
                $customer_change_addresses_query ="INSERT INTO mk_customer_address(login,NAME,address,city,state,country,pincode,email,mobile,locality,datecreated)"
                    ."VALUES('".$old_address_result[0]['login'].
                    "','".$recipient_name."','".
                    $new_address."','".$city."','".$newstate."','".$country."','".$pincode."','".$email."','".$mobile."','".$locality."',SYSDATE())";
                db_query($customer_change_addresses_query);
            }

            func_addComment_order($orderid, $addedby, 'ORDER_UPDATE', 'ADDRESS_CHANGE', 'Shipping address modified');
            $commentdetails = $commentdetails."::Old Address->" .$old_address_result[0]["customer_name"].",".mysql_real_escape_string($old_address_result[0]["s_address"]).",".$old_address_result[0]["s_country"].",".$old_address_result[0]["s_state"].",".$old_address_result[0]["s_city"].",".$old_address_result[0]["s_zipcode"].",".$old_address_result[0]["mobile"].",".$old_address_result[0]["s_email"];
        }

        for($i=0; $i<count($orderids); ++$i){
            $query = "insert into `mk_ordercommentslog` (orderid,commentaddedby,commenttitle,commenttype,newaddress,giftwrapmessage,description,attachmentname,commentdate) values ($orderids[$i],'$addedby','$commenttitle','$commenttype','$full_address','$gift_wrap_message','$commentdetails','$image_name','$time')";
            db_query($query);
        }
        if($changerequest=="Y"||$commenttype=="Change Request"){
            $query = "update xcart_orders set changerequest_status='Y' where orderid='$orderid'";
            db_query($query);

            $from ="MyntraAdmin";
            $loginEmail="operations@myntra.com";
            $bcc="customerservice@myntra.com";
            $args = array("ORDER_ID"	=>$orderid	);

            if($changerequest=="Y") {
                $template = "addedChangeRequest";
                sendMessage($template, $args, $loginEmail,$from,$bcc);
            }
        }
        if($changerequest=="Y"){
            $smarty->assign("commentSuccess","Y");
            func_display("addOrder_changerequest.tpl",$smarty);
        } elseif($onhold=="Y"){
            $smarty->assign("commentSuccess","Y");
            func_display("AddOnhold_Comment.tpl",$smarty);
        } else {
            $smarty->assign("commentSuccess","Y");
            func_display("addOrder_comments.tpl",$smarty);
        }
    } else {
        $smarty->assign("commentSuccess","N");
        $smarty->assign("errorMsg", "Address change can be performed only when all the shipments of order in WIP, OnHold or Queued Status");
        func_display("addOrder_changerequest.tpl",$smarty);
    }
}
?>
