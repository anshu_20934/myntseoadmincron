<?php
require "./auth.php";
include_once("$xcart_dir/include/security.php");
use geo\ZipDAO;
$orderid = $_GET['orderid'];

$order = func_query_first("select status, ordertype from xcart_orders where orderid = $orderid", true);
if($order) {
	if($order['status'] == 'WP' || $order['status'] == 'OH' || $order['status'] == 'Q') {
		$splitOrders = func_query_column("SELECT orderid from xcart_orders where orderid != $orderid and group_id=$orderid", 0, true);
		if($splitOrders){
			$smarty->assign("splitOrders", $splitOrders);
		}
		$date=date("d",time())."-".date("M",time()).",".date("Y",time());
		$smarty->assign("orderid", $orderid);
		$smarty->assign("date", $date);
		$current_shipping_country_query = "Select orders.warehouseid, orders.s_country,orders.payment_method, orders.total from xcart_orders orders where orderid=".$orderid;
		$current_shipping_country = func_query_first($current_shipping_country_query, true);
		$customer_address_query = "SELECT address.name,address.address,languages.value,address.country,
		address.state state_code,states.state state,address.city,address.pincode,address.mobile,address.email,address.locality
		FROM mk_customer_address address 
		LEFT JOIN xcart_orders orders ON address.login = orders.login 
		LEFT JOIN xcart_states states ON states.code=address.state
		LEFT JOIN xcart_languages languages ON languages.name=CONCAT('country_',address.country)
		WHERE orders.orderid=".$orderid." ORDER BY default_address DESC";
		//$default_mobile_query = "SELECT address.mobile FROM mk_customer_address address LEFT JOIN xcart_orders orders ON address.login = orders.login WHERE orders.orderid=".$orderid." ORDER BY default_address DESC LIMIT 1";
		$customer_address_result = func_query($customer_address_query);
		if($customer_address_result){
			$productsInCart = func_create_array_products_of_order($orderid); 
			foreach ($customer_address_result as $idx=>$customer_address){
				if($order['ordertype'] == 'ex'){
		            //$supported_couriers = func_check_exchange_serviceability($customer_address['pincode'])?'true':'false';
					$customer_address_result[$idx]['serviceable'] = func_check_exchange_serviceability_products($customer_address['pincode'], $productsInCart, $current_shipping_country['warehouseid'])?true:false;
		        } else {
		        	//$supported_couriers = func_get_serviceable_couriers($customer_address['pincode'], $current_shipping_country['payment_method'], $customer_address['country']);
		        	if($current_shipping_country['payment_method'] == 'cod') {
		        		$couriers = func_get_order_serviceabile_courier('cod', $customer_address['pincode'], $productsInCart, $current_shipping_country['warehouseid'], $current_shipping_country['total']);
		        		$customer_address_result[$idx]['serviceable'] = ($couriers && count($couriers)>0)?true:false;
		        	} else {
		        		$couriers = func_get_order_serviceabile_courier('on', $customer_address['pincode'], $productsInCart, $current_shipping_country['warehouseid'], $current_shipping_country['total']);
		        		$customer_address_result[$idx]['serviceable'] = ($couriers && count($couriers)>0)?true:false;
		        	}
                }
			}
			$customer_address_result_json = json_encode($customer_address_result);
		}
		$smarty->assign("payment_method", $current_shipping_country['payment_method']);
		$smarty->assign("customer_addresses_list", $customer_address_result);
		$states = func_query("SELECT $sql_tbl[states].stateid, $sql_tbl[states].state, $sql_tbl[states].code AS state_code, $sql_tbl[states].country_code FROM $sql_tbl[states] WHERE $sql_tbl[states].country_code='IN' order by $sql_tbl[states].state, $sql_tbl[states].code");
		$smarty->assign("states",$states);
		$xcache = new XCache();
		$stateZipsJson = $xcache->fetch("statezips_xcache");
		
		if(!$stateZipsJson){
			include_once "$xcart_dir/geoinit.php";
			$zipDAO = new ZipDAO();
			$stateZips = $zipDAO->getAllStateLevelZips();
			$stateZipsJson =  json_encode($stateZips);
			$xcache->store("statezips_xcache", $stateZipsJson);
		}
		
		$smarty->assign("stateZipsJson",$stateZipsJson);
		
		$smarty->assign("default_mobile", $default_mobile);
	} else {
		$smarty->assign("errorMsg", "Address change can be performed only when order is in WIP, OnHold or Queud Status");
	}
} else {
	$smarty->assign("errorMsg", "Invalid Order Id");
}
func_display("addOrder_changerequest.tpl",$smarty);

?>
