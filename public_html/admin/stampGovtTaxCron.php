<?php 
 
include_once("auth.php");
include_once("$xcart_dir/include/class/oms/OrderWHManager.php");
include_once("$xcart_dir/include/class/oms/EventCreationManager.php");
include_once("$xcart_dir/include/func/func.order.php"); 
 
$ordersWithStampTaxFailure = func_query("select orderid from xcart_orders where status = 'OH' and warehouseid > 0 and on_hold_reason_id_fk = 29");
$shipmentids = array();
foreach ($ordersWithStampTaxFailure as $orders) {
    array_push($shipmentids, $orders['orderid']);
} 
 
foreach($shipmentids as $order) {
        echo "\nStamping govt tax for order: $order";
        \OrderWHManager::stampTax($order);
        db_query("update xcart_orders set status = 'Q' where orderid = " . $order);
        \EventCreationManager::pushCompleteOrderEvent($order);
        sleep(2);
        \EventCreationManager::pushReadyForReleaseEventWithQueuedDate($order, time(), 'System');
        usleep(500);
}