<?php
require './auth.php';
require $xcart_dir.'/include/security.php';
require_once $xcart_dir.'/include/func/func.itemsearch.php';


if($modify_assignee == 'modify') {
	#Sanitizing Input id
	if(preg_match('/^[0-9]+$/',$id)){
		$sql = "UPDATE {$sql_tbl[mk_assignee]} SET name='".mysql_real_escape_string($name)."', status='".($status == 'on' ?1:0)."', threshold='". $threshold ."', operations_location='".mysql_real_escape_string($operations_location)."' WHERE id='{$id}'";
		db_query($sql);
	}
	func_header_location('manage_resources.php');
}
elseif($add_assignee == 'add') {
	$sql = " INSERT INTO {$sql_tbl[mk_assignee]}(`name`, `status`, `threshold`,`operations_location`) VALUES ('".mysql_real_escape_string($name)."','1','". $threshold ."','".mysql_real_escape_string($operations_location)."')";
	db_query($sql);
	func_header_location('manage_resources.php');
}

#
#Get List of Resources
#
$sql = "SELECT * FROM {$sql_tbl[mk_assignee]} ORDER BY id";
$assignees = func_query($sql);

#
#Get List of Locations 
#
$sql = "SELECT * FROM {$sql_tbl[operations_location]} ";
$sql_result = func_query($sql);
foreach ($sql_result as $key=>$value){
	$operations_locations[$value['id']] = $value['name'];
}

$smarty->assign('assignees', $assignees);
$smarty->assign('operations_locations', $operations_locations);
$smarty->assign('main', 'manage_resources');
func_display('admin/home.tpl', $smarty);

?>
