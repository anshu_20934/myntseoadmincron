<?php
/*****************************************************************************\
 +-----------------------------------------------------------------------------+
 | X-Cart                                                                      |
 | Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
 | All rights reserved.                                                        |
 +-----------------------------------------------------------------------------+
 | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
 | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
 | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
 |                                                                             |
 | THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
 | THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
 | FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
 | AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
 | PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
 | CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
 | COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
 | (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
 | LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
 | AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
 | OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
 | AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
 | THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
 | THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
 |                                                                             |
 | The Initial Developer of the Original Code is Ruslan R. Fazliev             |
 | Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
 | Ruslan R. Fazliev. All Rights Reserved.                                     |
 +-----------------------------------------------------------------------------+
 \*****************************************************************************/

#
# $Id: search.php,v 1.141.2.8 2006/06/06 05:56:52 svowl Exp $
#
include_once("../include/func/func.inventory.php");
if ( !defined('XCART_SESSION_START') ) { header("Location: ../"); die("Access denied"); }
$inv_mode=$HTTP_POST_VARS["inv_mode"];

$searchresults=func_get_producttypes();
$stylesresults=func_get_productstyles();
$skuresults=func_get_allsku();

if($inv_mode=="inv_search"){
	$skuno=$HTTP_POST_VARS["sku"];
	$prod_type=$HTTP_POST_VARS["pr_type"];

	$prod_style=$HTTP_POST_VARS["style"];
	//if($skuno!="")
	$sku_list=func_get_skusearchlist($prod_type,$prod_style,$skuno);
	//else
	//$sku_list=func_get_skudetails($skuno);
	//$smarty->assign("type_cond",$sku_list);
}
else{
	$sku_list=func_get_skulist();




}
$total=0;
foreach ($sku_list as $sku){
	$total+=$sku['sku_value'];
}
$total=number_format($total,2,".",',');
$smarty->assign("total",$total);
$smarty->assign("searchresult",$sku_list);
$smarty->assign("types",$searchresults);

$smarty->assign("styles",$stylesresults);
$smarty->assign("skuresults",$skuresults);


$smarty->assign("main","inventory_search");


?>