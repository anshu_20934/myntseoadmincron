<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: partner_banners.php,v 1.28 2006/01/11 06:55:58 mclap Exp $
#

define('USE_TRUSTED_POST_VARIABLES',1);
$trusted_post_variables = array("add");

require "./auth.php";
require $xcart_dir."/include/security.php";

x_load('files');

if (!$active_modules['XAffiliate'])
    func_header_location ("error_message.php?access_denied&id=6");

$location[] = array(func_get_langvar_by_name("lbl_banners"), "");

#
# Define data for the navigation within section
#
$dialog_tools_data["left"][] = array("link" => "partner_banners.php", "title" => func_get_langvar_by_name("lbl_banners_list"));
$dialog_tools_data["left"][] = array("link" => "partner_banners.php?banner_type=T", "title" => func_get_langvar_by_name("lbl_add_text_link"));
$dialog_tools_data["left"][] = array("link" => "partner_banners.php?banner_type=G", "title" => func_get_langvar_by_name("lbl_add_graphic_banner"));
$dialog_tools_data["left"][] = array("link" => "partner_banners.php?banner_type=M", "title" => func_get_langvar_by_name("lbl_add_media_rich_banner"));
$dialog_tools_data["left"][] = array("link" => "partner_banners.php?banner_type=P", "title" => func_get_langvar_by_name("lbl_add_product_banner"));
$dialog_tools_data["right"][] = array("link" => "banner_info.php", "title" => func_get_langvar_by_name("lbl_banners_statistics"));

if ($userfile_type != "application/x-shockwave-flash" && substr($userfile_name, -4) == '.swf')
	$userfile_type = "application/x-shockwave-flash";

if (!empty($close)) {
	$banner_type = '';
	$mode = 'close';
	$bannerid = '';
}
elseif (($mode == "upload") && func_is_image_userfile($userfile, $userfile_size, $userfile_type)) {

	$userfile = func_move_uploaded_file("userfile");
	list($img_size, $img_width, $img_height) = func_get_image_size($userfile);

	$image = addslashes(func_file_get($userfile, true));

	if (!is_numeric($image_width) || (!is_numeric($image_height)) || ($image_width < 1) || ($image_height < 1)) {
		if ($img_width && $img_height) {
			$image_width = $img_width;
			$image_height = $img_height;
		}
		elseif($width && $height) {
			$image_width = $width;
			$image_height = $height;
		}
	}

	db_query("INSERT INTO $sql_tbl[partner_banners_elements] (data, data_type, data_x, data_y) VALUES ('$image', '$userfile_type', '$image_width', '$image_height')");
	@unlink($userfile);
	$banner_type = "M";
}
elseif ($mode == 'add' && $add && $banner_type && $add['banner']) {
	if ($banner_type == 'G') {
		$userfile = func_move_uploaded_file("userfile");
		$fp = func_fopen($userfile, "rb", true);
		if ($fp !== false) {
			$add['body'] = addslashes(fread($fp, filesize($userfile)));
			$add['image_type'] = $userfile_type;
			fclose($fp);
		}
	}

	$add['banner_type'] = $banner_type;
	$banner_type = '';
	if ($bannerid) {
		db_query("UPDATE $sql_tbl[partner_banners] SET banner = '$add[banner]', ".($add['body']?"body = '$add[body]', image_type = '$add[image_type]', ":"")."avail = '$add[avail]', is_image = '$add[is_image]', is_name = '$add[is_name]', is_descr = '$add[is_descr]', is_add = '$add[is_add]', banner_type = '$add[banner_type]', open_blank = '$add[open_blank]', legend = '$add[legend]', alt = '$add[alt]', direction = '$add[direction]', banner_x = '$add[banner_x]', banner_y = '$add[banner_y]' WHERE bannerid = '$bannerid'");
	}
	else {
		db_query("INSERT INTO $sql_tbl[partner_banners] (banner, body, avail, is_image, is_name, is_descr, is_add, banner_type, open_blank, legend, alt, direction, image_type, banner_x, banner_y) VALUES ('$add[banner]', '$add[body]', '$add[avail]', '$add[is_image]', '$add[is_name]', '$add[is_descr]', '$add[is_add]', '$add[banner_type]', '$add[open_blank]', '$add[legend]', '$add[alt]', '$add[direction]', '$add[image_type]', '$add[banner_x]', '$add[banner_y]')");
	}

	func_header_location("partner_banners.php?banner_type=$banner_type&bannerid=$bannerid");
}
elseif ($mode == "delete") {
	if ($bannerid) {
		db_query ("DELETE FROM $sql_tbl[partner_banners] WHERE bannerid = '$bannerid'");
	}
	elseif ($elementid) {
		db_query("DELETE FROM $sql_tbl[partner_banners_elements] WHERE elementid = '$elementid'");
		func_header_location("partner_element_list.php");
	}
}

if (!empty($mode)) {
	func_header_location("partner_banners.php".($banner_type?"?banner_type=".$banner_type:""));
}

if ($bannerid) {
	$banner = func_query_first("SELECT * FROM $sql_tbl[partner_banners] WHERE bannerid = '$bannerid'");
	$smarty->assign ("banner", $banner);
	$banner_type = $banner['banner_type'];
}

$banners = func_query ("SELECT * FROM $sql_tbl[partner_banners]");
$smarty->assign ("banners", $banners);

$elements = func_query ("SELECT elementid, data_type FROM $sql_tbl[partner_banners_elements] ORDER BY elementid");
$smarty->assign ("elements", $elements);

$smarty->assign ("banner_type", $banner_type);
$smarty->assign ("main", "partner_banners");

# Assign the current location line
$smarty->assign("location", $location);

# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
