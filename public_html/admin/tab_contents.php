<?php
require "./auth.php";
require_once("../include/func/func.utilities.php");

$messages = array();

$table = 'mk_tab_content';

$action = $_GET['action'];
$tab_id = $_GET['tab_id'];

$id = $_POST['id'];
$display_order = $_POST['display_order'];
$product_id = $_POST['product_id'];
$enabled = $_POST['enabled'];
$enabled = $_POST['enabled'];

if ($action == 'edit') {
    $id = $_GET['id'];
    if (!empty($product_id)) {
        func_array2update($table, array('product_id' => $product_id, 'tab_id' => $tab_id, 'display_order' => $display_order, 'enabled' => $enabled), 'id="' . $id . '"');
    }
    $tab_content = func_select_first($table, array('id' => $id));
    $smarty->assign("tab_content", $tab_content);
    $smarty->assign("id", $id);
} else if ($action == 'add') {

    if (!empty($product_id)) {
        $id = func_array2insert($table, array('product_id' => $product_id, 'tab_id' => $tab_id, 'display_order' => $display_order, 'enabled' => $enabled));
    }
}

if (empty($action)) {
    $action = 'add';
}

$tab_contents = func_select_query($table, array('tab_id' => $tab_id));
$smarty->assign("tab_contents", $tab_contents);

$smarty->assign("tab_id", $tab_id);
$smarty->assign("action", $action);
$smarty->assign("dropdown", array('front_banner' => 'Front banner', 'right_banner' => 'Right banner', 'right_bottom_banner' => 'Right bottom banner', 'sports_jerseys' => 'Sports Jerseys'));

$banners = func_select_query($table, null, null, null, array('datecreated' => 'desc'));

$smarty->assign("messages", $messages);
$smarty->assign("banners", $banners);
@include $xcart_dir . "/modules/gold_display.php";
func_display("admin/tab_contents.tpl", $smarty);
?>
