<?php

$xcart_dir = "/home/myntradomain/public_html";
include_once("auth.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/modules/amqpclients/producer/AMQPMessageProducer.php");
include_once("WMSDBUtil.php");
require_once($xcart_dir."/include/class/ShutdownHook.php");

$client = AMQPMessageProducer::getInstance();
$interval = 1; //in seconds
$threshold = 5; //net inventory threshold

$readDBCreds = WMSDBUtil::getCredentialsFromConfig(WMSDBUtil::ATP_READ);

//$atpConn = mysql_connect('atpdbmaster.myntra.com:3306','MyntAtpUser','UrJ4KTteK473Pwk#',TRUE);
$atpConn = mysql_connect('192.168.68.179:3307',$readDBCreds['username'],$readDBCreds['password'],TRUE);

// Check if connection exists
if (!$atpConn) {
    die('Could not connect to ATP db: ' . mysql_error().'\n');
}

// Check if DB is accessible
if(mysql_select_db('myntra_atp', $atpConn ) === false) {
        echo "ATP db unaccessible \n";
        exit;
}

$query = "select sku_id,supply_type,seller_id from inventory where enabled=1 and last_modified_on > date_sub(now(),interval ".$interval." MINUTE) and inventory_count-Greatest(blocked_order_count,0)-Greatest(blocked_future_count,0)<".$threshold;

$results = mysql_query($query, $atpConn);

if ($results){
  while ($row = mysql_fetch_array($results, MYSQL_ASSOC)){
    $skuId = $row['sku_id'];
    $supplyType = $row['supply_type'];
    $sellerId = $row['seller_id'];
    $inventoryEntry = array('storeId' => '1', 'sellerId' => $sellerId, 'skuId' => $skuId, 'supplyType' => $supplyType);
    $request = array('inventoryEntry' => $inventoryEntry);
    $client->sendMessage('atpReindexSkuQueue', $request, 'reindexInventory', 'xml', true, 'inv');
//    var_dump($request);
    print("Pushed for skuId: $skuId\n");
  }
}

mysql_close($atpConn);
?>
