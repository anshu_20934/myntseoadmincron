<?php
require_once "WMSDBConnUtil.php";
//ini_set('memory_limit','2000M');
function getOms2ImsFKBoc($warehouseId, &$imsFKMoreBocQueries, &$imsFKLessBocQueries, $extSellerIds)
{

    $fkStoreIds = array(2, 3, 4, 5);


$imsConn = WMSDBConnUtil::getConnection('imsdb1.myntra.com:3306', TRUE, WMSDBConnUtil::IMS_READ);
$omsConn = WMSDBConnUtil::getConnection("omsdb1.myntra.com:3306", TRUE, WMSDBConnUtil::IMS_READ);
$wmsConn = WMSDBConnUtil::getConnection('wmsdb2.myntra.com:3306', TRUE, WMSDBConnUtil::IMS_READ);


    if (!$omsConn || !$wmsConn) {
        die('Could not connect: ' . mysql_error());
    }

    if (mysql_select_db('myntra_oms', $omsConn) === false) {
        exit;
    }
    if (mysql_select_db('myntra_wms', $wmsConn) === false) {
        exit;
    }
    if (mysql_select_db('myntra_ims', $imsConn) === false) {
        exit;
    }

    $limit = 30000;
    $hasMore = true;
    $omsMinId = 1;

    #$omsMinIdQuery = "select min(id) as minId, max(id) as maxId from order_release where status_code in ('Q','RFR','WP') and is_on_hold = 0 AND login = 'FLIPKART' AND courier_code = 'EK' AND store_id in (" . implode(",", $fkStoreIds) . ") AND warehouse_id = " . $warehouseId;

    $omsMinIdQuery = "select min(id) as minId, max(id) as maxId from order_release where status_code in ('Q','RFR','WP') and is_on_hold = 0 AND store_id in (" . implode(",", $fkStoreIds) . ") AND warehouse_id = " . $warehouseId;
    $omsMinIdResults = mysql_query($omsMinIdQuery, $omsConn);
    if ($omsMinIdResults) {
        while ($row = mysql_fetch_array($omsMinIdResults, MYSQL_ASSOC)) {
            $omsMinId = $row['minId'];
            $omsMaxId = $row['maxId'];
        }
    }

    @mysql_free_result($omsMinIdResults);

    $skuToOmsBocFK = array();
    while ($hasMore) {
        // Loop over item list
        $nextOmsMinId = $omsMinId + $limit;
        $omsBocQueryFK = "select ol.sku_id as sku_id, ol.seller_id as seller_id, sum(ol.quantity) as blocked_order_quantity from order_line ol join order_release orl on (orl.id = ol.order_release_id_fk) where orl.id >= " . $omsMinId . " and orl.id < " . $nextOmsMinId . " and orl.status_code in ('Q','RFR','WP') AND ol.seller_id in (" . $extSellerIds . ") AND orl.is_on_hold = 0 AND orl.store_id in (" . implode(",", $fkStoreIds) . ") AND ol.supply_type = 'ON_HAND' and ol.status_code != 'IC' and orl.warehouse_id = " . $warehouseId . " group by ol.sku_id, ol.seller_id";

        $omsBocResultsFK = mysql_query($omsBocQueryFK, $omsConn);
        if ($omsBocResultsFK) {
            while ($row = mysql_fetch_array($omsBocResultsFK, MYSQL_ASSOC)) {
                $skuId = $row['sku_id'];
                $sellerId = $row['seller_id'];
                $skuSeller = $skuId . "_" . $sellerId;
                $boq = $row['blocked_order_quantity'];

                if ($skuToOmsBocFK[$skuSeller]) {
                    $skuToOmsBocFK[$skuSeller] = $skuToOmsBocFK[$skuSeller] + $boq;
                } else {
                    $skuToOmsBocFK[$skuSeller] = $boq;
                }
            }
        }

        if ($nextOmsMinId >= $omsMaxId)
            $hasMore = false;

        $omsMinId = $nextOmsMinId;
        @mysql_free_result($omsBocResultsFK);
    }

    $issuedItemsWmsQuery = "select itm.order_id order_id, itm.sku_id sku_id, count(*) issuedCount from item itm where item_status='ISSUED' and itm.warehouse_id=" . $warehouseId . " group by itm.sku_id, itm.order_id";

    $issuedItemsWmsResults = mysql_query($issuedItemsWmsQuery, $wmsConn);
    while ($itemrow = mysql_fetch_array($issuedItemsWmsResults, MYSQL_ASSOC)) {
        $issuedItemsOmsResults = mysql_query("select orl.id as id, ol.seller_id as seller_id, ol.sku_id as sku_id from order_line ol join order_release orl on (orl.id = ol.order_release_id_fk) where ol.order_release_id_fk = " . $itemrow['order_id'] . " and ol.sku_id = " . $itemrow['sku_id'] . " and ol.status_code != 'IC' and orl.status_code in ('PK', 'SH', 'DL', 'C','L','RTO') AND is_on_hold = 0 AND store_id in (" . implode(",", $fkStoreIds) . ")", $omsConn);
        $releaseId = false;
        while ($release = mysql_fetch_array($issuedItemsOmsResults, MYSQL_ASSOC)) {
            $releaseId = $release['id'];
            $skuSeller = $itemrow['sku_id'] . "_" . $release['seller_id'];
            $missedCount = $itemrow['issuedCount'];
            break;
        }
        if ($releaseId) {
            if ($skuToOmsBocFK[$skuSeller]) {
                $skuToOmsBocFK[$skuSeller] = $skuToOmsBocFK[$skuSeller] + $missedCount;
            } else {
                $skuToOmsBocFK[$skuSeller] = $missedCount;
            }
        }
    }
    @mysql_free_result($issuedItemsWmsResults);
    @mysql_free_result($issuedItemsOmsResults);


    $offset = 0;
    $limit = 200000;
    $hasMore = true;

    while ($hasMore) {
        $skuToWmsBocMapFK = array();
        $allWarehouseSkusQuery = "select id, sku_id, seller_id, blocked_order_count from wh_inventory where warehouse_id = $warehouseId and store_id in (" . implode(",", $fkStoreIds) . ") and seller_id in (" . $extSellerIds . ") and supply_type='ON_HAND' LIMIT $offset, $limit";

        $allWarehouseData = mysql_query($allWarehouseSkusQuery, $imsConn);
        if ($allWarehouseData && mysql_num_rows($allWarehouseData)) {
            while ($row = mysql_fetch_array($allWarehouseData, MYSQL_ASSOC)) {
                $skuSeller = $row['sku_id'] . "_" . $row['seller_id'];
                $skuToWmsBocMapFK[$skuSeller] = $row['blocked_order_count'];
            }
        }
        @mysql_free_result($allWarehouseData);
        if (empty($skuToWmsBocMapFK)) {
            $hasMore = false;
            continue;
        }

        foreach ($skuToWmsBocMapFK as $skuSeller => $boc) {
            $query = false;
            $startIndx = strpos($skuSeller, "_");
            $sellerId = substr($skuSeller, $startIndx + 1);
            $skuId = substr($skuSeller, 0, $startIndx);

            if (!isset($skuToOmsBocFK[$skuSeller])) {
                if ($boc > 0 || $boc < 0) {
                    $query = "update wh_inventory set blocked_order_count = 0 where warehouse_id = $warehouseId and store_id in (" . implode(",", $fkStoreIds) . ") and sku_id = $skuId and seller_id = $sellerId and supply_type='ON_HAND'";
                    $imsFKMoreBocQueries[] = $query;
                }
            } else {
                $shouldBeQty = $skuToOmsBocFK[$skuSeller];
                if ($boc > $shouldBeQty) {
                    // ok for now - since there is one to one mapping between warehouse and store.
                    $query = "update wh_inventory set blocked_order_count = blocked_order_count - " . ($boc - $shouldBeQty) . " where warehouse_id = $warehouseId and store_id in (" . implode(",", $fkStoreIds) . ") and sku_id = $skuId and seller_id = $sellerId and supply_type='ON_HAND'";
                    $imsFKMoreBocQueries[] = $query;
                } else if ($boc < $shouldBeQty) {
                    $query = "update wh_inventory set blocked_order_count = blocked_order_count + " . ($shouldBeQty - $boc) . " where warehouse_id = $warehouseId and store_id in (" . implode(",", $fkStoreIds) . ") and sku_id = $skuId and seller_id = $sellerId and supply_type='ON_HAND'";
                    $imsFKLessBocQueries[] = $query;
                }
            }
        }
        $offset += $limit;
    }

}

?>

