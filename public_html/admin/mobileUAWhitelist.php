<?php 
require_once "auth.php";
require_once $xcart_dir."/include/security.php";
use mobile\dao\MobileUAWhitelistDBAdapter;

$dbAdapter = new MobileUAWhitelistDBAdapter();
if ($mode == "add") {
    $data = $dbAdapter->getWhitelist();
    $device = mysql_real_escape_string($add_device);
    $browser = mysql_real_escape_string($add_browser);
    $data[$device] = $browser;
    $dbAdapter->updateWhitelist($data);
}elseif ($mode == "save") {
    $data = array();
    foreach($device as $key=>$value){
        $data[$value] = $browser[$key];
    }
    $dbAdapter->updateWhitelist($data);
}

$data = $dbAdapter->getWhitelist();

$smarty->assign("data",$data);
$smarty->assign("main","mobileUAWhitelist");
func_display("admin/home.tpl",$smarty);
?>
