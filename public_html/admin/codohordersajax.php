<?php
require "./auth.php";
include_once("$xcart_dir/env/Host.config.php");
$orderid = $_REQUEST['order_id'];
$login = $_REQUEST['login'];
$from_date = $_REQUEST['from_date'];
$to_date = $_REQUEST['to_date'];
$trial_number = $_REQUEST['trial_number'];
$action = $_REQUEST['action'];


function createSearchQuery($request) {
	global $sqllog;
	$sql = "select SQL_CALC_FOUND_ROWS xo.*, xo.s_firstname as name, concat(xo.s_address,' ',xo.s_locality,' ',xo.s_state,' ',xo.s_zipcode) as address, FROM_UNIXTIME(date,'%d/%m/%Y %H:%i:%s') as orderdate, mr.reason_display_name as reason from cod_oh_orders_dispostions codd,xcart_orders xo,mk_order_action_reasons mr where xo.orderid = codd.orderid and xo.status = 'OH' and codd.disposition = 'OH' and codd.reason = mr.reason and xo.orderid = xo.group_id ";
	
	if($request['order_id'] != ''){
		$sql = $sql." and xo.orderid = ".$request['order_id'];
	}else{
		
		if($request['login'] != ''){
			$sql = $sql." and xo.login = '".$request['login']."'";
		}
		
		$from_date = $request['from_date'];
		if($from_date != ''){
			$dateObj = date_parse_from_format('d/m/Y H:i:s', $from_date);
			$return_from_date = $dateObj['year']."-".$dateObj['month']."-".$dateObj['day']." ".$dateObj['hour'].":".$dateObj['minute'].":".$dateObj['second'];
			$sql = $sql." and xo.date  >= unix_timestamp('$return_from_date')";
		}
		
		$to_date = $request['to_date'];
		if($to_date != ''){
			$dateObj = date_parse_from_format('d/m/Y H:i:s', $to_date);
		    $return_to_date = $dateObj['year']."-".$dateObj['month']."-".$dateObj['day']." ".$dateObj['hour'].":".$dateObj['minute'].":".$dateObj['second'];
			$sql = $sql." and xo.date  <= unix_timestamp('$return_to_date')";
		}
		 
		$trial_number = $request['trial_number'];
		if($trial_number != null){
			$sql = $sql." and codd.trial_number in ($trial_number)";
		}

		$action = $request['action'];
		if($action == 'search'){
			$start = $request['start'] == null ? 0 : $request['start'];
			$limit = $request['limit'] == null ? 30 : $request['limit'];
		}
		
		if($action == 'downloadreport'){
			$start = $request['start'] == null ? 0 : $request['start'];

			$limit = $request['limit'] == null ? 4000 : $request['limit'];
		}
		
		if(!empty($request["sort"])) {
			$sql .= " ORDER BY " . $request["sort"];
			$sql .= " " . ($request["dir"] == 'DESC' ? "DESC" : "ASC");
		}
		$sql .=" LIMIT $start, $limit";
	}
	$sqllog->info("COD OH Search Query = $sql");

	return $sql;
}

if($action == 'search'){
	$sql = createSearchQuery($_REQUEST);
	$results = func_query($sql,true);
	$total = func_query("SELECT FOUND_ROWS() total", TRUE);
	header('Content-type: text/x-json');

	print json_encode(array("results" => $results, "count" => $total[0]["total"]));
}
if ($action == 'downloadreport') {
	$progresskey = $_REQUEST['progresskey'];
	$xcache = new XCache();

	$progressinfo = array('status'=>'PROCESSING');

	$xcache->store($progresskey."_progressinfo", $progressinfo, 3600);
	$sql = createSearchQuery($_REQUEST);
	$results = func_query($sql,true);

	$return_data = array();
	if($results) {
		$xls_data = formatForXls($results);
		$file_name_suffix = date('d-m-y-H:i:s');

		$filename = 'cod-oh-report-'.$file_name_suffix.".xls";
		$fullpath = "/admin/report_download/".$filename;
		$weblog->info("Write to file - $fullpath");

		$objPHPExcel = new PHPExcel();
		$objPHPExcel = func_write_data_to_excel($objPHPExcel, $xls_data);
		ob_clean();
		flush();
		try {
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save($xcart_dir.$fullpath);
		}catch(Exception $ex) {
			$weblog->info("Error Occured = ". $ex->getMessage());
		}

		$weblog->info("Saved");

		$return_data['success'] = true;
		$return_data['file_location'] = $fullpath;
	} else {
		$return_data['success'] = false;
		$return_data['msg'] = "No data to generate return";
	}

	$return_data['status'] = 'DONE';
	$xcache->store($progresskey."_progressinfo", $return_data, 3600);
	header('Content-type: text/x-json');

	print json_encode($return_data);

} else if ($action == 'downloadminacsreport') {
	$sql = "Select o.group_id as orderid, concat(c.firstname,' ',c.lastname) as customerName, o.issues_contact_number, o.mobile, o.s_address, o.s_locality, o.s_city, o.s_state, o.s_zipcode, o.date, moar.reason_display_name from xcart_orders o, xcart_customers c, cod_oh_orders_dispostions codd, mk_order_action_reasons moar where o.login = c.login and o.orderid = codd.orderid and codd.reason = moar.reason and o.status = 'OH' and o.on_hold_reason_id_fk = 3 and o.orderid = o.group_id";
	if($orderid != ''){
		$sql = $sql." and o.group_id = $orderid";
	}else{
		if($login != ''){
			$sql = $sql." and o.login = '$login'";
		}
		
		if($trial_number != null){
			$sql = $sql." and codd.trial_number in ($trial_number)";
		}

		if($from_date != ''){
			$dateObj = date_parse_from_format('d/m/Y H:iresponse.responseText:s', $from_date);
			$return_from_date = $dateObj['year']."-".$dateObj['month']."-".$dateObj['day']." ".$dateObj['hour'].":".$dateObj['minute'].":".$dateObj['second'];
			$sql = $sql." and o.date  >= unix_timestamp('$return_from_date')";
		}

		if($to_date != ''){
			$dateObj = date_parse_from_format('d/m/Y H:i:s', $to_date);
			$return_to_date = $dateObj['year']."-".$dateObj['month']."-".$dateObj['day']." ".$dateObj['hour'].":".$dateObj['minute'].":".$dateObj['second'];
			$sql = $sql." and o.date  <= unix_timestamp('$return_to_date')";
		}

		if(!empty($request["sort"])) {
			$sql .= " ORDER BY o.orderid DESC";
		}
	}
	
	$sql .=" LIMIT 0, 1000";
	$sqllog->info("Minacs SearhcURL - ". $sql);
	$results = func_query($sql,true);
	
	$return_data = array();
	if($results) {
		$xls_data = formatForMinacsXls($results);
		$file_name_suffix = date('d-m-y-H:i:s');
		$filename = 'cod-oh-report-'.$file_name_suffix.".xls";
		$fullpath = "/admin/report_download/".$filename;
		$weblog->info("Write to file - $fullpath");
		$objPHPExcel = new PHPExcel();
		$objPHPExcel = func_write_data_to_excel($objPHPExcel, $xls_data);
		ob_clean();
		flush();
		try {
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save($xcart_dir.$fullpath);
		}catch(Exception $ex) {
			$weblog->info("Error Occured = ". $ex->getMessage());
		}
		$weblog->info("Saved");
		$return_data['success'] = true;
		$return_data['file_location'] = $fullpath;
	} else {
		$return_data['success'] = false;
		$return_data['msg'] = "No data to generate return";
	}
	
	$return_data['status'] = 'DONE';
	$xcache->store($progresskey."_progressinfo", $return_data, 3600);
	header('Content-type: text/x-json');
	print json_encode($return_data);
	
}

function formatForXls($ordersData) {
	$finalList = array();
	$orderids = array();
	foreach($ordersData as $index=>$order) {
		$orderids[] = $order['orderid'];	
	}
	
	$products_sql = "SELECT xo.group_id as orderid, xo.total, xod.product_style, sp.product_display_name,
			    	(xod.total_amount - xod.coupon_discount_product - xod.cash_redeemed - xod.pg_discount) as item_amount
					FROM xcart_orders xo, xcart_order_details xod, mk_style_properties sp, mk_product_style ps 
					where xo.orderid = xod.orderid and xod.product_style = sp.style_id and sp.style_id = ps.id and 
					xod.item_status <> 'IC' AND xo.group_id in (".implode(",", $orderids).")";
	$products = func_query($products_sql, true);
	foreach($products as $product) {
		if(!isset($order2Products[$product['orderid']]))
			$order2Products[$product['orderid']] = array();
		
		$order2Products[$product['orderid']][] = $product; 
		
		if(!isset($order2TotalAmount[$product['orderid']]))
			$order2TotalAmount[$product['orderid']] = 0;
			
		$order2TotalAmount[$product['orderid']] += $product['item_amount']; 
	}					
	
	foreach($ordersData as $index=>$order) {
		$finalList[$index]['Sr No'] = $index+1;
		$finalList[$index]['Order Id'] = $order['orderid'];
		$finalList[$index]['Customer Name'] = $order['name'];
		$finalList[$index]['Phone No'] = $order['issues_contact_number'];
		$finalList[$index]['Address'] = $order['s_address'];
		$finalList[$index]['Locality'] = $order['s_locality'];
		$finalList[$index]['City'] = $order['s_city'];
		$finalList[$index]['State'] = $order['s_state'];
		$finalList[$index]['Zipcode'] = $order['s_zipcode'];
		$finalList[$index]['Order Creation Time'] = $order['orderdate'];
		$finalList[$index]['Reason for On-Hold'] = $order['reason'];
		$orderProductNames = '';
		$orderProductUrls = '';
		$urlPrefix = HostConfig::$baseUrl;
		foreach($order2Products[$order['orderid']] as $product) {
			$orderProductNames .= $orderProductNames==''?(''.$product['product_display_name']):('#'.$product['product_display_name']);
			$orderProductUrls .= $orderProductUrls==''?(''.$urlPrefix.$product['product_style']):('#'.$urlPrefix.$product['product_style']);
		}
		$finalList[$index]['Products'] = $orderProductNames;
		$finalList[$index]['Product Links'] = $orderProductUrls;
		$finalList[$index]['Price'] = $order2TotalAmount[$order['orderid']];
	}
	
	return $finalList;
}

function formatForMinacsXls($ordersData) {
	$finalList = array();
	$orderids = array();

	foreach($ordersData as $index=>$order) {
		$orderids[] = $order['orderid'];
	}

	$allOrderIds = '';
	$amountSql = "select o.group_id, group_concat(o.orderid) as allOrderIds, sum(o.subtotal) as subtotal, sum(o.coupon_discount) as coupon_discount, sum(o.cash_redeemed) as cash_redeemed, sum(o.discount+o.cart_discount) as discount, sum(o.total) as total from xcart_orders o where o.group_id in (".implode(",", $orderids).") group by o.group_id";
	$amount_rows = func_query($amountSql, true);
	foreach($amount_rows as $row) {
		if(!isset($order2AmountDetails[$row['group_id']]))
			$order2AmountDetails[$row['group_id']] = $row;
		
		$allOrderIds .= $allOrderIds==''?$row['allOrderIds']:','.$row['allOrderIds'];
	}
	
	$products_sql = "SELECT xo.group_id as orderid, xod.product_style, sp.product_display_name, po.value as size
	FROM xcart_orders xo, xcart_order_details xod, mk_style_properties sp, mk_product_style ps,  
	mk_order_item_option_quantity oq, mk_product_options po where xo.orderid = xod.orderid and 
	xod.product_style = sp.style_id and sp.style_id = ps.id and xod.itemid = oq.itemid and oq.optionid = po.id and 
	xod.item_status <> 'IC' AND xo.group_id in ($allOrderIds)";

	$products = func_query($products_sql, true);
	foreach($products as $product) {
		if(!isset($order2Products[$product['orderid']]))
			$order2Products[$product['orderid']] = array();

		$order2Products[$product['orderid']][] = $product;
	}

	foreach($ordersData as $index=>$order) {
		global $baseUrl;
		$finalList[$index]['Sr No'] = $index+1;
		$finalList[$index]['Order Id'] = $order['orderid'];
		$finalList[$index]['Customer Name'] = $order['customerName'];
		$finalList[$index]['Registered Phone No'] = $order['issues_contact_number'];
		$finalList[$index]['Shipping Phone No'] = $order['mobile'];
		$finalList[$index]['Address'] = $order['s_address'];
		$finalList[$index]['Locality'] = $order['s_locality'];
		$finalList[$index]['City'] = $order['s_city'];
		$finalList[$index]['State'] = $order['s_state'];
		$finalList[$index]['Zip code'] = $order['s_zipcode'];
		$finalList[$index]['Order Creation Time'] = date('m/d/Y H:i:s', $order['date']);
		$finalList[$index]['Reason for On-Hold'] = $order['reason_display_name'];

		$orderProductNames = '';
		$orderProductUrls = '';
		$urlPrefix = HostConfig::$baseUrl;

		foreach($order2Products[$order['orderid']] as $product) {
			$orderProductNames .= $orderProductNames==''?(''.$product['product_display_name'].':'.$product['size']):('#'.$product['product_display_name'].':'.$product['size']);
			$orderProductUrls .= $orderProductUrls==''?(''.$urlPrefix.$product['product_style']):('#'.$urlPrefix.$product['product_style']);
		}

		$finalList[$index]['Products'] = $orderProductNames;
		$finalList[$index]['Product Links'] = $orderProductUrls;

		$finalList[$index]['Order Total'] = $order2AmountDetails[$order['orderid']]['subtotal'];
		$finalList[$index]['Net Discount'] = $order2AmountDetails[$order['orderid']]['discount'];
		$finalList[$index]['Coupon Used'] = $order2AmountDetails[$order['orderid']]['coupon_discount'];
		$finalList[$index]['Cash Back Used'] = $order2AmountDetails[$order['orderid']]['cash_redeemed'];
		$finalList[$index]['To be Paid'] = $order2AmountDetails[$order['orderid']]['total'];
	}
	return $finalList;
}

?>