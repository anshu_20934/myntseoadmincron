<?php
/**
 * Shows payment service logs for entered orderids
 * @author: Shashank D
 * 
 */
require "./auth.php";
require $xcart_dir."/include/security.php";
use revenue\payments\service\PaymentServiceInterface;

$orderIds = $_POST['csv-file-list'];
$orderArray = array();
$allOrderLogs = array();

if(!empty($orderIds)){
    $value = strtolower(trim($orderIds));
    if(!empty($value)) {
        $orderArray = array_map("trim", explode(",",$value));  
    } 
}

if(!empty($orderArray)){
    //print_r($orderArray);
    foreach ($orderArray as $orderid){
        $orderPaymentServiceLog = PaymentServiceInterface::getPaymentLog($orderid);
        $orderPaymentServiceLog = json_decode($orderPaymentServiceLog, true);
        $errMsg = "Not Found";
        $temp = array();
        $temp['orderId'] = $orderid;
        $temp['gatewayName'] = (!empty($orderPaymentServiceLog['gatewayName']))?$orderPaymentServiceLog['gatewayName']:$errMsg;
        $temp['paymentOption'] = (!empty($orderPaymentServiceLog['paymentOption']))?$orderPaymentServiceLog['paymentOption']:$errMsg;
        $temp['amount'] = (!empty($orderPaymentServiceLog['amount']))?$orderPaymentServiceLog['amount']:$errMsg;
        $temp['isComplete'] = (!empty($orderPaymentServiceLog['isComplete']))?$orderPaymentServiceLog['isComplete']:$errMsg;
        $allOrderLogs[$orderid] = $temp;    
    }
}

//print_r($allOrderLogs);
$smarty->assign("plogs", $allOrderLogs);
$smarty->assign("main","pl_report");

/*
switch($mode){
    case 'exportPLReport':
        $fileName = "service_requests_report_".date("dmy");
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$fileName.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        $HTML = func_display("admin/ServiceRequest/service_request_report_export.tpl", $smarty, false);        
        echo $HTML;
        exit;
        break;
}
*/

func_display("admin/home.tpl",$smarty);
exit;