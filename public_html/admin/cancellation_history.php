<?php
include_once 'auth.php';
include_once $xcart_dir.'include/security.php';
include_once $xcart_dir.'/include/func/func.mkcore.php';
include_once $xcart_dir.'/include/func/func.order.php';
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");

$action = $_POST['action'];
if(!$action)
	$action = 'view';

if($action == 'search') {
	$return_arr = func_get_cancelled_orders($_POST);
	header('Content-type: text/x-json');
	print json_encode($return_arr);	
} else if ($action == 'download') {
	$return_arr = func_get_cancelled_orders($_POST, false);
	$objPHPExcel = func_create_results_file($return_arr['results']);
	$filename = func_save_results_file($objPHPExcel, "cancellations");
	header('Content-type: text/x-json');
	print json_encode($filename);
} else if($action == 'email_report') {
	$response = func_send_results_file_mail($_POST);
	header('Content-type: text/x-json');
	print json_encode($response);
} else if($action == 'mark_refund_process') {
	$orderid = $_POST['orderid'];
	$refund_mode = $_POST['refund_mode'];
	$refund_amount = $_POST['refund_amount'];
	$remarks =  $_POST['refund_comment'];
	
	$comment = "";
	if(!empty($_POST['itemid'])) {
		$itemid = $_POST['itemid'];
		$comment .= "Itemid:$itemid\n";
	}
	
	$comment .= "Refund Amount:$refund_amount";
	if($refund_mode == 'C') {
		$comment .= "\nRefund Mode: Coupon\nRefund Coupon:".$_POST['coupon'];
	} else {
		$comment .= "\nRefund Mode: Payment Gateway\nPG Name: ".$_POST['pg_name'];
	}
	
	$comment .= "\nRemarks:$remarks";
	func_addComment_order($orderid, $_POST['login'], 'Note', 'Refund Processed', $comment);
	header('Content-type: text/x-json');
	print json_encode(true);
}else {
	$smarty->assign("action", $action);
	$smarty->assign("main", "cancellation_history");
	func_display("admin/home.tpl", $smarty);
}
?>