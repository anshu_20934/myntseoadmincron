<?php
class MY_AdminController {
    protected function render($tpl,$data){
        foreach($data as $key=>$value){
            $this->tpl->assign($key,$value);
        }
	$this->tpl->assign('cntr_tpl',$tpl);        
        func_display("admin/home.tpl",$this->tpl);
        exit();
    }
    protected function render_ajax(){

    }
    protected function render_json($data){
        echo json_encode($data);
        exit();
    }
    protected function render_raw($data){
        echo $data;
        exit();
    }
	protected function redirect404() {
		echo "page does not exist. Go back";
		die();
	}
	protected function redirect($page) {
		header("Location: $page");
	}

    var $tpl ; //Template engine Obj
	var $id;
	var $base_tpl_path = 'admin/dre/';
	var $base_service_url;
	var $loggedInUser;
	var $serviceError ='';
	var $useServer = true;
	var $phpErrors = array();

	public function __construct($id) {
        global $XCART_SESSION_VARS,$smarty;
		$this->base_service_url = HostConfig::$portalServiceHost;
		$loggedInAdminUser = $XCART_SESSION_VARS['identifiers']['A']['login'];
		if(!$loggedInAdminUser) {
            $this->redirect("/admin/index.php");
		}
		$this->loggedInUser = $loggedInAdminUser;
		if(!$id) {
			$this->id = 'index';
			$this->get = array();
			$this->post = array();
			$this->request = array();
		} else {
			$this->id = $id;
			$this->get = $_GET;
			$this->post = $_POST;
			$this->request = $_REQUEST; //Secure these
		}
		$this->tpl = $smarty;
		$this->act();
	}

	public function act() {
		$action = $this->id.'Action';
		if(method_exists($this,$action)) {
			$this->$action();
            //call_user_func_array(array($this, $method), $params);
		} else {
			$this->redirect404();
		}
	}


}

