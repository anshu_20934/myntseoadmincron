<?php
class EossController extends MY_AdminController{

	/* Default action for this controller */
	public function indexAction() {
		$this->mode = 'main';
		$discount = array();
        $data['discount_count'] = count($discount);
        $data['discount_json'] = json_encode($discount);

        $data['discount_style_map_json'] = $this->getDiscountStyleMap();
		
		$data['styles_json'] =  json_encode($this->getStyles());

        $data['items_json'] =  json_encode($this->getUnlistedItems());
        $data['discounted_items_json'] = json_encode($this->getDiscountedUnlistedItems());

        $data['vouchers_json']           = json_encode($this->getVoucherList());
		//$enabledSetRules = bindec(FeatureGateKeyValuePairs::getFeatureGateValueForKey('faida.enabledSetRules'));
	$enabledSetRules = 0;
           	$data['enabledSetRules'] = $enabledSetRules;
		if($this->serviceError) {
            $data['dreError'] = $this->serviceError;
		}
		$html = $this->base_tpl_path.'index.tpl';
        $data['page'] = $this->mode;
	$data['loggedInUser'] = $this->loggedInUser;
	        $this->render($this->base_tpl_path.'index.tpl',$data);
	}

    public function itemlistpopupAction(){
        $items = $this->getUnlistedItems();
        echo "<table style='text-align:left;'><tr ><th>ItemId</th><th>ItemName</th></tr>";
        foreach($items as $it){
            echo "<tr><td>{$it['style_id']}</td><td>{$it['product_display_name']}</td></tr>";
        }
        echo "</table>";
        die();
    }
	public function refreshDiscountAction() {
		$discount = $this->getDiscountRuleData();
        $this->render_json($discount);
	}
	public function refreshstylemapAction() {
		$sdMap = $this->getDiscountStyleMap();
		echo $sdMap;
		die();
	}
	public function refreshStyleAction() {
		$styles = $this->getStyles();
		echo json_encode($styles);
		die();
	}
	public function refreshdiscountedunlisteditemsAction(){
		$items = $this->getDiscountedUnlistedItems();
		echo json_encode($items);
		die();
	}

    private function getVoucherList() {
        array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$this->loggedInUser);
		$request = new RestRequest($this->base_service_url.'/voucher/', 'GET');
		$request->setHttpHeaders($headers);
		$request->execute();
		$res = 	json_decode($request->getResponseBody());
        if(!$res) $res = array();
        return $res;
        //$this->render_json($res);
	}

	public function discountAction() {
		global $drelog;
		$txt = $this->post['model'];
		$method = $_SERVER['REQUEST_METHOD'];
		$id = $_GET['id'];
		if($id && $method !='GET') $method = 'PUT';
		
        if($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'] == 'DELETE') {
            $method = 'DELETE';
        }
		//Manipulations to sync with java service
		$n_txt = json_decode($txt);
		if(isset($n_txt->ruleType)) {
			unset($n_txt->ruleType);
		}
		
		
		$prevStyles = array();
		if(isset($n_txt->prevDiscountStyles)){
			
			foreach ($n_txt->prevDiscountStyles as $prevStyle){
				//$prevStyles[] = $prevStyle["styleId"];
				if(isset($prevStyle->styleId)){
					$prevStyles[]=$prevStyle->styleId;					
				}				
			}			
			unset($n_txt->prevDiscountStyles);
		}
		
        $count_filter = count($n_txt->discountFilters);
        for ($i=0; $i < $count_filter; $i++) {
    		if(isset($n_txt->discountFilters[$i]->brand)) {
	    		if($n_txt->discountFilters[$i]->brand == 'all') unset($n_txt->discountFilters[$i]->brand);
		    }
    		if(isset($n_txt->discountFilters[$i]->articleType)) {
	    		if($n_txt->discountFilters[$i]->articleType == 'all') unset($n_txt->discountFilters[$i]->articleType);
		    }
    		if(isset($n_txt->discountFilters[$i]->fashionType)) {
	    		if($n_txt->discountFilters[$i]->fashionType == 'all') unset($n_txt->discountFilters[$i]->fashionType);
		    }
    		if(isset($n_txt->discountFilters[$i]->season)) {
	    		if($n_txt->discountFilters[$i]->season == 'all') unset($n_txt->discountFilters[$i]->season);
		    }
    		if(isset($n_txt->discountFilters[$i]->year)) {
	    		if($n_txt->discountFilters[$i]->year == 'all') unset($n_txt->discountFilters[$i]->year);
		    }
            if(isset($n_txt->discountFilters[$i]->style_id_arr)) {
	    		unset($n_txt->discountFilters[$i]->style_id_arr);
		    }
    		if(count($n_txt->discountFilters) == 0) {
	    		$n_txt->discountFilters['updated_by'] == $this->loggedInUser;
		    }
        }
		
		$n_txt = json_encode($n_txt);
		// end manipulation
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'user: '.$this->loggedInUser);
		if($id && $method !='GET' && $method != 'DELETE' && $this->get['parent'] == true  ) {
			array_push($headers,'parent: true');
		}
		$request = new RestRequest($this->base_service_url.'/discount/'.$id, $method,$n_txt);
		$request->setHttpHeaders($headers);
		$request->execute();
		
		$data = $request;
		$res = 	json_decode($data->getResponseBody());
	    $responseInfo = $data->getResponseInfo();	
		$drelog->debug("response - " . print_r($data, true));

		if(!$res) {
			$res = $data->getResponseBody();
		} else {
			$d= $data->getResponseBody();
			$a = json_decode($d);
			if(!$a->status) {
				header("status: 200");
				header('Cache-Control: no-cache, must-revalidate');
				header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
				header('Content-type: application/json');
				if($method !='GET') {
					$this->addToSolr($a->discountStyles, $a->id,$prevStyles);
				}
                if($_GET['g']=='rules') {
                    echo json_encode($a->discountRules);
                    die();
                }
                if($_GET['g']=='styles') {
                    echo json_encode($a->discountStyles);
                    die();
                }
				echo $d;
				die();
			}		
		}
        if($responseInfo['http_code'] == 200){
            	header("status: 200");
				header('Cache-Control: no-cache, must-revalidate');
				header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
				header('Content-type: application/json');
                echo "";
                die();
        }
        else {
		$this->sendDiscountRuleModificationEmail('Discount Action',$this->formatRestRequestForMail($request));
		$d= $data->getResponseBody();
		$a = json_decode($d);
		header("Status: 500");
        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
		if(!$a->message) {
			echo 'SDNLM7279'.print_r($d,true);
		} else {
			echo $a->message;
		}
		die();
        }
	}


    //Private functions used by controllers' actions

    private function getDiscountStyleMap() {
		$newData = array();
		$request = $this->requestDiscountServer('','GET','','/discount/style');
		$data = $request->getResponseBody();
		if(!json_decode($data)) {
			$this->phpErrors[] = 'Style Map not fetched. Please try again later';
			$this->sendDiscountRuleModificationEmail('get Discount Style Map',$this->formatRestRequestForMail($request));
			return array();
		}
		return $data;
	}

	private function getDiscountRuleData() {
		$request = $this->requestDiscountServer('','GET','');
		if(!$request->getResponseBody()) {
			$this->serviceError = ' Service down';
			$this->sendDiscountRuleModificationEmail('get Discount Rule',$this->formatRestRequestForMail($request));
		} else {
			$res = json_decode($request->getResponseBody());
			if(isset($res->status) || isset($res->message)) {
				$this->sendDiscountRuleModificationEmail('get Discount Rule',$this->formatRestRequestForMail($request));
				header("status: 500");
                header('Cache-Control: no-cache, must-revalidate');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Content-type: application/json');
				echo $res->message;
			} else {
				echo $json = $request->getResponseBody();
			}
		}
		die();
	}

	private function addToSolr($discountStyles, $discountId,$prevStyleIds=null)
	{
        try
        {
            $styleIds = array();
            foreach ($discountStyles as $ds) {
                $styleIds[] = $ds->styleId;
            }
            if($prevStyleIds != null){
            	$styleIds = array_unique(array_merge($styleIds,$prevStyleIds));
            }
            $solrProducts = new solrProducts();
            $discountDetails = DiscountEngine::getDataForMultipleStyles("search", $styleIds);
            foreach ($styleIds as $ds) {
                $solrProducts->addStyleToIndex($ds, true, false,true,$discountDetails->$ds);
            }
        } catch (Exception $ex) {
            $msg = "Discount saved successfully, but styles were not added to the Solr -> please contact dev team with the discountId - " . $discountId;
            $this->sendDiscountRuleModificationEmail('Add to Solr',$msg." ".$ex->getMessage());
            header("status: 500");
            echo $msg;
            die();
        }
	}


	private function requestDiscountServer($id='',$method='', $txt, $url='') {
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		array_push($headers,'User: '.$this->loggedInUser);
		if($this->useServer) {
			if(!$url) {
				if($method=='GET') {
					$request = new RestRequest($this->base_service_url.'/discount/', 'GET',$txt);
				}
			} else {
					$request = new RestRequest($this->base_service_url.$url,$method, $txt );
			}
			$request->setHttpHeaders($headers);
			$request->execute();
			return $request;
		}
		else {
			return ;
		}
	}
	
	private function sendDiscountRuleModificationEmail($heading=null, $content= null) {
        DiscountEngine::sendDiscountRuleErrorEmail($heading, $content,$this->loggedInUser);
	}
	

    private function getUnlistedItems(){
        $sql = "SELECT s.style_id,
                       s.style_id  AS id,
                       p.name as product_display_name,
                       p.price
                FROM   mk_style_properties s
                       INNER JOIN mk_product_style p
                         ON p.id = s.style_id
                WHERE  p.styletype = 'FRG'
                       AND product_display_name != ''";
        $items = func_query($sql);
        if(!$items) $items = array();
        return $items;
    }

    private function getDiscountedUnlistedItems(){
        $sql = "SELECT *
                FROM   discount_free_item";
        $items = func_query($sql);
        if(!$items) $items = array();
        return $items;
    }

    private function getStyles(){
        $sql = "SELECT s.style_id,
                       s.style_id  AS id,
                       product_display_name,
                       global_attr_brand,
                       cc.typename AS global_attr_article_type,
                       global_attr_fashion_type,
                       global_attr_season,
                       global_attr_year ,
                       p.price
                FROM   mk_style_properties s
                       INNER JOIN mk_catalogue_classification cc
                         ON s.global_attr_article_type = cc.id
                       INNER JOIN mk_product_style p
                         ON p.id = s.style_id
                WHERE  p.styletype = 'P'
                       AND product_display_name != ''
                       AND global_attr_brand IS NOT NULL
                       AND global_attr_article_type IS NOT NULL
                       AND global_attr_fashion_type IS NOT NULL
                       AND global_attr_season IS NOT NULL
                       AND global_attr_year IS NOT NULL";
		$styles = func_query($sql);
		if(!$styles) $styles = array();
        return $styles;
    }

    private function formatRestRequestForMail($rest){
        return DiscountEngine::formatRestRequestForMail($rest);
    }
}
