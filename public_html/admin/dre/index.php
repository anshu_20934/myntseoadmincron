<?php
define('FUNC_SKU_CALL', 'Y');
include_once "./../auth.php";
require_once "$xcart_dir/modules/RestAPI/RestRequest.php";
require_once "$xcart_dir/include/solr/solrProducts.php";
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";
include_once "MY_AdminController.php";
include_once "controllers.php";
include_once "VoucherfreebieController.php";

//$template ="admin/dre/index.tpl";
$smarty->assign("main",'discount_engine');

if($isAccessAllowed == false) {
	// This variable is set in role_access_control.php
	func_display("admin/home.tpl",$smarty);
	exit;
} 

$action  = isset($_REQUEST['a'])?$_REQUEST['a']:'';
$smarty->caching = 0;
if(isset($_REQUEST['c'])){
   //Implemet aotu loading of controller 
    $c = new VoucherfreebieController($action);
} else {
$c = new EossController($action);
}
