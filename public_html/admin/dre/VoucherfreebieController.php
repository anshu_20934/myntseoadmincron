<?php
class VoucherfreebieController extends MY_AdminController{

	public function indexAction() {
        $this->mode = 'main';
        $data['page'] = $this->mode;
        $this->render($this->base_tpl_path.'voucherfreebie.tpl',$data);
    }

	public function savemapAction(){

		$freebieId = 1; //Not Coupon
		$watches_csv = $this->post['watches_remap'];
		$ipod_csv    = $this->post['ipod_remap'];
		$ipad2_csv   = $this->post['ipad2_remap'];
		$paris_csv   = $this->post['paris_remap'];
		if($watches_csv != '') $datam['2']                = explode(",",$watches_csv);
		if($ipod_csv != '') $datam['3']                 = explode(",",$ipod_csv);
		if($ipad2_csv != '') $datam['4']                 = explode(",",$ipad2_csv);
		if($paris_csv != '') $datam['5']    = explode(",",$paris_csv);

		$error = false;		
		foreach($datam as $freebieType=>$valueArr){
			foreach($valueArr as $val){
				if(!is_numeric(trim($val))) {
					$error['message'][]= "Incorrect Orderid . Please go back and check the values";
					$error['message'][]= '=====OrderId: '.$val."\n<br/>";
					$error['message'][]= '=====Freebie Type: '.$freebieType;
					break;
				}
			}
		}
		if(!$error){
            $i = 0 ;
	    foreach($datam as $freebieType=>$valueArr){
		    foreach($valueArr as $val){
			$orderid = trim($val);

			$headers = array();
			array_push($headers, 'Accept:  application/json');
			array_push($headers,'Content-Type: application/json');
			array_push($headers,'user: '.$this->loggedInUser);
			array_push($headers,'freebieType: '.$freebieType);
			array_push($headers,'freebieId: '.$freebieId);

			$request = new RestRequest($this->base_service_url.'/voucher/order/'.$orderid, 'PUT');
			$request->setHttpHeaders($headers);
			$request->execute();
			$res = 	json_decode($request->getResponseBody());
			$info = $request->getResponseInfo();
			if($info['http_code'] != 200 || $res ) {
				DiscountEngine::sendDiscountRuleErrorEmail('Freebie Mapping',
					DiscountEngine::formatRestRequestForMail($request),$this->loggedInUser);
                    if($res->message) $errorString = $res->message;
                    else $errorString = print_r($res,true);
                $limessages[$i]['content']='OrderId: '.$val.'-----Freebie Type: '.$freebieType.'<br/>Error:- '.$errorString;
                $limessages[$i]['status']= 'error';
			} else {
                $limessages[$i]['content']='OrderId: '.$val.'-----Freebie Type: '.$freebieType.
                $limessages[$i]['status']= '<br/>success';
            }
            $i++;
		    }
		}
		}
		$data['error'] = $error;
        $data['limessages'] = $limessages;
		$this->mode = 'main';
        $data['page'] = $this->mode;
        $this->render($this->base_tpl_path.'voucherfreebie_saved.tpl',$data);

	}

}
