<?php
require "./auth.php";
require_once("../include/func/func.utilities.php");

$messages = array();
$table = 'mk_config';

$action = $_GET['action'];
$property = $_POST['property'];
$value = $_POST['value'];
$order = $_POST['order'];
$enabled = $_POST['enabled'];
$type = $_POST['type'];
$page = $_POST['page'];

if ($action == 'edit') {
    $id = $_GET['id'];
    if (!empty($property)) {
        func_array2update($table, array( 'property' => $property, 'value' => $value, '`order`' => $order, 'enabled' => $enabled,'`type`' => $type, 'page' => $page), 'id="' . $id . '"');
    }
    $property = func_select_first($table, array('id' => $id));
    $smarty->assign("property", $property);
    $smarty->assign("id", $id);
} else if ($action == 'add') {
    if (!empty($property)) {
        $id = func_array2insert($table, array( 'property' => $property, 'value' => $value, '`order`' => $order, 'enabled' => $enabled, '`type`' => $type , 'page' => $page ));
    }  
}

if (empty($action)) {
    $action = 'add';
}
$smarty->assign("action", $action);

$banners = func_select_query($table, null, null, null, array('datecreated' => 'desc'));

$smarty->assign("messages", $messages);
$smarty->assign("properties", $banners);
@include $xcart_dir . "/modules/gold_display.php";
func_display("admin/properties.tpl", $smarty);
?>
