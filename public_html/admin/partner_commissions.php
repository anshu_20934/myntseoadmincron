<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: partner_commissions.php,v 1.15 2006/01/11 06:55:58 mclap Exp $
#

require "./auth.php";
require $xcart_dir."/include/security.php";
 
if(!$active_modules['XAffiliate'])
    func_header_location ("error_message.php?access_denied&id=9");

$location[] = array(func_get_langvar_by_name("lbl_commissions"), "");

#
# Define data for the navigation within section
# 
$dialog_tools_data["right"][] = array("link" => "partner_plans.php", "title" => func_get_langvar_by_name("lbl_affiliate_plans"));

if ($mode == "apply") {
	if (is_array($plans)) {
		foreach ($plans as $k=>$v) {
			db_query("REPLACE INTO $sql_tbl[partner_commissions] (login, plan_id) VALUES ('$k', '$v')");
		}
	}

	func_header_location("partner_commissions.php?mode=go&page=$page&partner=$partner&applied");
}
elseif ($mode == "apply_global" && $pc) {
	$partners = func_query_column("SELECT login FROM $sql_tbl[customers] WHERE usertype = 'B'".(($partner && $use_filter) ? " AND login LIKE '%$partner%'" : ""));
	if ($partners) {
		foreach ($partners as $v) {
			db_query("REPLACE INTO $sql_tbl[partner_commissions] (login, plan_id) VALUES ('$v', '$pc')");
		}
	}
	$top_message["content"] = func_get_langvar_by_name("txt_plan_was_successfully_applied");
	$top_message["type"] = "I";
	
	func_header_location("partner_commissions.php?mode=go&pc=$pc&partner=$partner&use_filter=$use_filter");
}
elseif ($mode == "go") {
	$partner_info = func_query("SELECT $sql_tbl[partner_commissions].plan_id, $sql_tbl[customers].login, $sql_tbl[customers].firstname, $sql_tbl[customers].lastname FROM $sql_tbl[customers] LEFT JOIN $sql_tbl[partner_commissions] ON $sql_tbl[partner_commissions].login=$sql_tbl[customers].login WHERE $sql_tbl[customers].usertype = 'B' AND $sql_tbl[customers].login LIKE '".((empty($partner) || $use_filter != 'Y') ? "%" : "%$partner%")."' ORDER BY $sql_tbl[customers].lastname, $sql_tbl[customers].firstname");
	$smarty->assign ("partner_info", $partner_info);
}

$partners = func_query ("SELECT * FROM $sql_tbl[customers] WHERE usertype='B' ORDER BY lastname, firstname");
$partner_plans = func_query ("SELECT * FROM $sql_tbl[partner_plans] ORDER BY plan_title,plan_id");

$smarty->assign ("partner_plans", $partner_plans);
$smarty->assign ("partners", $partners);
$smarty->assign ("partner", $partner);
$smarty->assign ("use_filter", $use_filter);
$smarty->assign ("mode", $mode);

$smarty->assign ("main", "commissions");

# Assign the current location line
$smarty->assign("location", $location);

# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
