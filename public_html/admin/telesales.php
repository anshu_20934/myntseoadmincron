<?php
require_once "./auth.php";
require_once \HostConfig::$documentRoot."/include/security.php";
include_once \HostConfig::$documentRoot."/include/func/func.mk_orderbook.php";
include_once \HostConfig::$documentRoot."/include/class/class.feature_gate.keyvaluepair.php";
include_once \HostConfig::$documentRoot."/include/class/class.mymyntra.php";
include_once \HostConfig::$documentRoot."/include/class/mcart/class.MCart.php";
include_once \HostConfig::$documentRoot."/sanitizept.inc.php";
include_once \HostConfig::$documentRoot."/exception/MyntraException.php";

include_once \HostConfig::$documentRoot."/modules/coupon/mrp/SmsVerifier.php";
include_once \HostConfig::$documentRoot."/include/class/oms/EventCreationManager.php";
use mcart\exception\ProductAlreadyInCartException;

use order\dataobject\builder\OrderDOBuilder;
use order\exception\CartTamperedException;
use order\exception\EmptyCartException;
use order\exception\NoSuitableGatewayException;
use order\exception\OrderCreationFailedException;
use order\exception\UserNotLoggedInException;
use order\exception\CashOnDeliveryEligibilityFailedException;
use order\exception\NoShippingAddressException;
use order\dataobject\builder\OrderInputParamDOBuilder;
use order\manager\OrderManager;

$access_granted=true;

foreach ($XCART_SESSION_VARS['user_roles'] as $roledetails){
	if($roledetails['role']==='TCS'){
		$access_granted=true;
		break;
	}
}

if(!$access_granted===true){
	echo "Access Denied!!";
	exit;
}

global $weblog;
$cloginid = mysql_real_escape_string(trim($loginid));
$ccartid = mysql_real_escape_string(trim($cartid));
$zipcode = mysql_real_escape_string(trim($pincode));
$styleid = mysql_real_escape_string(trim($styleid));


if($action=='login' ){
	$mcartFactory= new MCartFactory();
	$mcart= $mcartFactory->getCartForLogin($cloginid,true,'telesales');
	if($mcart==null){
		$response['itemsincart']=0;
	}else{
		$response['itemsincart']=$mcart->getItemQuantity();
	}
	$customer_details=func_get_customer_detail($cloginid);
	if(!empty($customer_details)){
		$response['status']='S';
		$response['firstname']=$customer_details['firstname'];
		$response['lastname']=$customer_details['lastname'];
		$response['loginid']=$loginid;
		$response['mobile']=$customer_details['mobile'];
	}else{
		$response['status']='F';
		$response['errorcode']='0'; //Record not found
		$response['loginid']=$loginid;
	}
	$profile_values = array();
	$profile_values['last_login'] = time();
	func_array2update('customers', $profile_values, "login='$cloginid'");
	showMessage();

}else if($action=='codcheck'){
	$response['status']='S';
	$response['zipcode']=$zipcode;
	
	/* $productsInCart = func_create_array_products_of_order($orderid);
	$jewelleryItemsInOrder = 'none'; // other values can be 'some', 'all'
	$jewelleryItemsCount = 0;
	foreach($productsInCart as $product) { 
		$fineJewelleryItem = func_query("select * from mk_attribute_type at, mk_attribute_type_values av, mk_style_article_type_attribute_values sav where at.id = av.attribute_type_id and av.id = sav.article_type_attribute_value_id and at.catalogue_classification_id = ".$product['article_type']." and sav.product_style_id = ".$product['productStyleId'] ." and av.attribute_value = 'Fine Jewellery'", true);
		if($fineJewelleryItem && count($fineJewelleryItem)>0){
			$jewelleryItemsCount++;
			$jewelleryItemsInOrder = 'some';
		}
	}
	if(count($productsInCart) == $jewelleryItemsCount)
		$jewelleryItemsInOrder = 'all'; */
	
	//if(func_check_cod_serviceability_products($zipcode, $productsInCart, $jewelleryItemsInOrder)){
	if(is_zipcode_servicable_cod($zipcode)){
		$response['available']='true';
	}else{
		$response['available']='false';
	}
	showMessage();	

}else if($action=='getavailabilityselect'){
	
	$checkInStock=FeatureGateKeyValuePairs::getFeatureGateValueForKey('checkInStock');
	$fetchall = (empty($checkInStock) || $checkInStock=='false');
		
	$sizeOptions = func_load_product_options($styleid, 0, $fetchall, true, true);
	$sizeNew="";
		
		
	$unifiedSizeOptions =  Size_unification::getUnifiedSizeForOptions($sizeOptions, $styleid, "array");
	$sizeOptionSKUMapping =array();
	foreach ($sizeOptions as $options) {
		foreach ($unifiedSizeOptions as $key=>$value) {
			if($options['value'] == $key) {
				$sizeOptionSKUMapping[$options['sku_id']] = $value;
			}
		}
	}
	echo json_encode($sizeOptionSKUMapping);
	exit;	

}else if($action=='registeruser'){
	$customer_details=func_get_customer_detail($cloginid);
	if(!empty($customer_details)){
		$response['status']='F';
		$response['errorcode']=2;//Already exists
		$response['firstname']=$customer_details['firstname'];
		$response['lastname']=$customer_details['lastname'];
		$response['loginid']=$customer_details['email'];
		$response['mobile']=$customer_details['mobile'];
	}else{
		$profile_values = array();
		$profile_values['email'] = $cloginid;
		$profile_values['login'] = $cloginid;
		$profile_values['mobile'] = mysql_escape_string($phoneno);
		$profile_values['firstname']=mysql_escape_string($username);
		$profile_values['usertype'] = "C";
		$pass=rand(12345,99999)."";
		$crypted = "";
		$profile_values['password'] = $crypted;
		$profile_values['password_md5'] = addslashes(user\UserAuthenticationManager::encryptToMD5($pass));
		$profile_values['termcondition'] = '1';
		$profile_values['addressoption'] = 'M';
                $_currenttime = time();
		$profile_values['first_login'] = $_currenttime;
		$profile_values['last_login'] = $_currenttime;                
		func_array2insert('customers', $profile_values);
		$response['status']='S';
		$response['loginid']=$loginid;
		$response['mobile']=mysql_escape_string($phoneno);
		include_once \HostConfig::$documentRoot."/modules/coupon/database/CouponAdapter.php";
		include_once (\HostConfig::$documentRoot."/modules/coupon/mrp/RuleExecutor.php");
		include_once \HostConfig::$documentRoot."/include/func/func.reset_password.php";
		$smsVerifier = SmsVerifier::getInstance();
		$smsVerifier->addNewMapping($profile_values['mobile'], $profile_values['login']);
		$couponAdapter = CouponAdapter::getInstance();
		$couponAdapter->customerJoined($profile_values['login'], $profile_values['referer']);
		sendResetPasswordMail($cloginid,'telesales_registration',empty($username)?"Customer":$username);
		// Synchronous call to the MRP New Login rule.
		$logging_str= "Generating First login coupons for ".$profile_values['login'];
		$weblog->warn($logging_str);
		$ruleExecutor = RuleExecutor::getInstance();
		$ruleExecutor->runSynchronous("customer", $profile_values['login'], "1");    // "1" is the ruleId for "NewLogin" rule.
		$weblog->warn("Done generating coupons");
	}
	$customers=func_get_customer_detail_from_mobile($response['mobile']);
	foreach ($customers as $cust){
		$res['firstname']=$cust['firstname'];
		$res['lastname']=$cust['lastname'];
		$res['loginid']=$cust['email'];
		$res['mobile']=$cust['mobile'];
		$response['customers'][]=$res;
	}
	showMessage();

}else if($action=='resetpassword'){
	include_once \HostConfig::$documentRoot."/include/func/func.reset_password.php";
	sendResetPasswordMail($cloginid);
	$response['status']='S';
    writeMessage();
	showMessage();

}else if($action=='updatecart'||  $action=='placeorder'){

    if(empty($cloginid)){		
        writeMessage('F', 3, "Not logged in");

	}else{	

		$skuid = trim($skuid);
		$shipaddressid = mysql_real_escape_string(trim($addressid));
		$selectedQuantity = mysql_real_escape_string(trim($quantity));
		$postCouponCode = mysql_real_escape_string(trim($couponcode));
		$pm = mysql_real_escape_string(trim($pm));
        
		$mcartFactory = new MCartFactory();
		$mcart = $mcartFactory->getCartForLogin($cloginid,true,'telesales');
		$smarty->assign("itemsincart",$response['itemsincart']);

        // Instance of the coupon database liaison.
        $adapter = CouponAdapter::getInstance();
        
        // coupons available for the customer
        $coupons = $adapter->getCouponsForCartPage($cloginid, $mcart);

        // cashback coupons available for the customer
        $cashCoupons = $adapter->getMyntraCreditsCoupon($cloginid);

        // get user profile data and check mobile verified or not and COD
        $myMyntra = new MyMyntra(mysql_real_escape_string($cloginid));
        $userProfileData = $myMyntra->getUserProfileData();
        $smsVerifier = SmsVerifier::getInstance();

        // TODO understand whether is needed in telesales
        $verifyMobileforCOD = false;// currently(2012-06-26) is false in FG
        if($verifyMobileforCOD){
            $CODStatus = $smsVerifier->checkCODVerifiedStatus($userProfileData['mobile'],$userProfileData['login']);
        }

        // check mobile status for the customer(verified or not)
        $mobileStatus = $smsVerifier->getStatus($userProfileData['mobile'], $userProfileData['login']);

        // TODO - need to take this variable from public_html/auth.php
        $skipMobileVerification = true;// currently(2012-06-26) is true in FG
        if($mobileStatus != 'V' && $skipMobileVerification){
            $smsVerifier->skipVerification($userProfileData['mobile'], $userProfileData['login']);
            $mobileStatus = $smsVerifier->getStatus($userProfileData['mobile'], $userProfileData['login']);
        }

        // TODO --can we redeem any coupon without mobile verification
        // mobile status !='V' don't show firstlogin and referral coupon groups
        // because just to authenticate the coupon usage on right user
        if($mobileStatus != 'V'){
            if( !empty($coupons) && is_array($coupons) ){
                foreach($coupons as $idx=>$coupon){
                    if(in_array($coupon['groupName'], array('FirstLogin', 'MrpReferrals'))){
                        unset($coupons[$idx]);
                    }
                }
            } else {
                $coupons = '';
            }
        }


        // various actions on cart
        if($subaction == 'addtocart'){
			$mcartItem= new MCartNPItem(MCartUtils::genProductId(),$skuid,$selectedQuantity);
			try {
				$mcart->addProduct($mcartItem);
                
			} catch (ProductAlreadyInCartException $productInCartEx) {
				
			}
			
		}else if($subaction == 'removefromcart'){
			$mcartItem=$mcart->getProduct($productid);
			$mcart->removeItem($mcartItem);

        // remove coupon
		}
		else if($subaction == 'uesMyntCash'){
			
			$userCashAmount = $HTTP_POST_VARS["userAmount"];
			$mcart->enableMyntCashUsage($userCashAmount);
			
			$mcart->refresh(false,true,true,true);
		}else if($subaction == 'removeMyntCash'){
			$mcart->removeMyntCashUsage();
			
			$mcart->refresh(false,true,true,true);
		}
		elseif($subaction == 'removecoupon'){
            $couponCodeArrayRem=array_keys($mcart->getAllCouponCodes(MCart::$__COUPON_TYPE_DEFAULT));            
	        $mcart->removeCoupon($couponCodeArrayRem[0], MCart::$__COUPON_TYPE_DEFAULT);
			$mcart->refresh(false,true,true,true);

        //remove cashback
		}/*elseif($subaction == 'removecashcoupon'){
            $cashCouponCodeArrayRem=array_keys($mcart->getAllCouponCodes(MCart::$__COUPON_TYPE_CASHBACK));
	        $mcart->removeCoupon($cashCouponCodeArrayRem[0], MCart::$__COUPON_TYPE_CASHBACK);            
            $mcart->refresh(false,true,true,true);

        // apply coupon
        }*/elseif($subaction == 'addcoupon'){
            $mcart->addCouponCode($postCouponCode,MCart::$__COUPON_TYPE_DEFAULT);
            $mcart->refresh(false,true,true,true);

        // apply cashback
        }/*elseif($subaction == 'addcashcoupon'){
            $mcart->addCouponCode($cashCoupons[0]['coupon'], MCart::$__COUPON_TYPE_CASHBACK,array('userCashAmount'=>$cashCoupons[0]['MRPAmount']));            
            $mcart->refresh(false,true,true,true);
        }*/

        // show coupon message(success/failure) and remove coupon on failure from mcart
        $discountCouponArray = array_keys($mcart->getAllCouponCodes(MCart::$__COUPON_TYPE_DEFAULT));
        $discountCouponCode = $discountCouponArray[0];

        if(!empty($discountCouponCode)){            
            $applicationCode = $mcart->getCouponApplicationCode($discountCouponCode, MCart::$__COUPON_TYPE_DEFAULT);

            if($applicationCode === MCart::$__COUPON_APP_CODE_SUCCESS){
                $discount = number_format($mcart->getDiscount(), 2, ".", ',');
                $couponMessage = "Congratulations! You have "
                        . "successfully saved Rs. $discount by using coupon "
                        . "<b>".strtoupper($discountCouponCode)."</b>";

            }else{
                $couponMessage = $mcart->getCouponApplicationErrorMessage($discountCouponCode,  MCart::$__COUPON_TYPE_DEFAULT);
                $mcart->removeCoupon($discountCouponCode, MCart::$__COUPON_TYPE_DEFAULT);
            }
        }

        // show cashback message(success/failure) and remove cashback on failure from mcart
        $cashCouponCodeArray=array_keys($mcart->getAllCouponCodes(MCart::$__COUPON_TYPE_CASHBACK));
        $cashCouponCode=$cashCouponCodeArray[0];
        
		if(!empty($cashCouponCode)){            
            $applicationCode = $mcart->getCouponApplicationCode($cashCouponCode, MCart::$__COUPON_TYPE_CASHBACK);

            if($applicationCode === MCart::$__COUPON_APP_CODE_SUCCESS){
                $cashdiscount = $mcart->getCashDiscount();
                $abs_discount = $cashCoupons[0]['MRPAmount'];
                
                $cashCouponMessage = "You have redeemed <em>Rs. ".number_format(round($cashdiscount), 2, ".", ',')."</em> of your <em>Rs. ".number_format($cashCoupons[0]['MRPAmount'],2)."</em> cashback";
                if($cashdiscount <= $abs_discount){
                    $cash_left = $abs_discount - $cashdiscount;
                    $cashCouponMessage .= "<br/>Current balance: Rs ".number_format(round($cash_left), 2, ".", ',');
                }
				

            }else{
                $cashCouponMessage = $mcart->getCouponApplicationErrorMessage($cashCouponCode,  MCart::$__COUPON_TYPE_CASHBACK);
                $mcart->removeCoupon($cashCouponCode, MCart::$__COUPON_TYPE_CASHBACK);
            }
        }
        
        
        $myntCashDetails = MyntCashService::getUserMyntCashDetails($mcart->getLogin());
        $smarty->assign("myntCashDetails",$myntCashDetails);
        
        
        if($mcart->isMyntCashUsed()){
        	$isMyntCashUsed=true;
        	//$smarty->assign("appliedCashbackCode", $cashCouponCode);
        	//$applicationCode = $myCart->getCouponApplicationCode($cashCouponCode, MCart::$__COUPON_TYPE_CASHBACK);
        
        	$myntCashUsage = number_format($mcart->getTotalMyntCashUsage(), 2, ".", ',');
        	$smarty->assign("myntCashUsage", $myntCashUsage);
        	$smarty->assign("isMyntCashUsed",$isMyntCashUsed);
        	/*$abs_discount=$cashcoupons[0]['MRPAmount'];
        		if($cashdiscount <= $abs_discount)
        		{
        	$cash_left=$abs_discount - $cashdiscount;
        	$cashCouponDesc= "Use your Myntra credits. Current balance: Rs $cash_left ";
        	}*/
        	$myntCashUsageMessage = "You have redeemed <em>Rs. $myntCashUsage</em> of your <em>Rs. ".number_format($myntCashDetails['balance'],2)."</em> cashback";
        	$smarty->assign("myntCashdivid", "success");
        	$smarty->assign("myntCashUsageMessage", $myntCashUsageMessage);
        
        }else{
        	$myntCashAvailableAmount = (!empty($myntCashDetails["balance"]))? $myntCashDetails["balance"] : 0 ;
        	$myntCashUsageMessage = "Use your Myntra credits. Current balance: Rs ".number_format(round($myntCashAvailableAmount), 2, ".", ',')."<br/>";
        	$smarty->assign('myntCashUsageMessage',$myntCashUsageMessage);
        }
        

        // save cart in DB
		MCartUtils::persistCart($mcart);

		$response['status'] = 'S';
		$products = MCartUtils::convertCartToArray($mcart);

        // TODO --to remove undefined bug in js in telesales(cart count), is required
        if($mcart==null){
			$response['itemsincart'] = 0;
		}else{
			$response['itemsincart'] = $mcart->getItemQuantity();
		}

		$additionalCharges =  $mcart->getAdditionalCharges();
		$totalMRP = $mcart->getMrp();
		$vat = $mcart->getVat();
		$totalQuantity = $response['itemsincart'];

        // TODO --to check cart discount is needed or not
        $cartDiscount = $mcart->getCartLevelDiscount();

		$coupondiscount = $mcart->getDiscount();
		$cashdiscount = $mcart->getCashDiscount();
		$productDiscount = $mcart->getDiscountAmount();
		$shippingCharge = $mcart->getShippingCharge();

        // TODO --to check cart discount need to be considered or not
		$totalAmount = $totalMRP + $additionalCharges + $shippingCharge - $coupondiscount - $cashdiscount - $productDiscount;
		$cashBackDisplayAmount = $totalMRP + $additionalCharges - $productDiscount - $coupondiscount;

        // TODO -- to check is needed or not
        $cashback_tobe_given=$mcart->getCashbackToGivenOnCart();
		
		$mrp = $totalMRP;
		$amount = $totalMRP + $additionalCharges;
		
		if( $totalAmount < 0) {
			$totalAmount = 0;
		}

        $totalAmount = round($totalAmount);
		$vat = ($vat < 0 ? 0 : $vat);

        // TODO --to check $totalCashBackAmount is needed or not, remove if not
        $enable_cashback_discounted_products = "false";// currently is false in FG
        if(!empty($enable_cashback_discounted_products)){
            if($enable_cashback_discounted_products == "true")	{
                $totalCashBackAmount = $totalAmount;
            } else {
                $totalCashBackAmount = $mcart->getTotalCashBackAmount();
            }
        } else {
            $totalCashBackAmount = $mcart->getTotalCashBackAmount();
        }

        if($totalCashBackAmount < 0)	{
            $totalCashBackAmount = 0;
        }
        // TODO --to check $totalCashBackAmount is needed or not, remove if not        
        
        if(!empty($products)){
            foreach($products as $idx => $product){
                if(!empty($product["discountAmount"])) {
                    $pricePerItemAfterDiscount = round($product["productPrice"]-($product["discountAmount"]/$product["discountQuantity"]));
                    $product['pricePerItemAfterDiscount'] = $pricePerItemAfterDiscount;
                    $products[$idx] = $product;
                }
                if(!empty($product['discountDisplayText'])){
                    foreach($product['discountDisplayText']->params as $k=>$v) {
                        $product['dre_'.$k] = $v;
                    }
                    $product['discountDisplayText'] = $_rst[$product['discountDisplayText']->id];
                    $products[$idx] = $product;
                }
            }

        }                

		if(!empty($shipaddressid)){
			$address = func_select_first('mk_customer_address', array('id' => $shipaddressid));
			$s_country = $address['country'] ;
			$s_state = $address['state'] ;
			$s_city = $address['city'] ;
            $s_zipcode = strtolower($address['pincode']);
			$s_email=$address['email'];
			if(!empty($address)){
				$result = func_select_first('xcart_states', array('code' => $address['state']));
				$address['statename']= empty($result['state'])?$address['state']:$result['state'];;
				$result = func_select_first('xcart_languages', array('name' => 'country_'.$address['country'],'topic' => 'Countries'));
				$address['countryname']= $result['value'];
				
				/* if($products) {
					$jewelleryItemsCount = 0;
					$skuDetails = array();
					foreach($products as $index=>$product) {
						$fineJewelleryItem = func_query("select * from mk_attribute_type at, mk_attribute_type_values av, mk_style_article_type_attribute_values sav where at.id = av.attribute_type_id and av.id = sav.article_type_attribute_value_id and at.catalogue_classification_id = ".$product['article_type']." and sav.product_style_id = ".$product['productStyleId'] ." and av.attribute_value = 'Fine Jewellery'", true);
						if($fineJewelleryItem && count($fineJewelleryItem)>0){
							$jewelleryItemsCount++;
							$jewelleryItemsInOrder = 'some';
						}
						$products[$index]['style_id'] = $product['productStyleId'];
						$products[$index]['sku_id'] = $product['skuid'];
						$skuDetails[$product['skuid']] = array('id'=>$product['skuid'], 'availableItems'=> $product['availableItems'], 'warehouse2AvailableItems'=> $product['warehouse2AvailableItems']);
					}
					
					if(count($products) == $jewelleryItemsCount)
						$jewelleryItemsInOrder = 'all';
			 
					 
					$address['isServiceable'] = func_check_cod_serviceability_products($address['pincode'],$products, $jewelleryItemsInOrder, false, $skuDetails);
				} else {
					$address['isServiceable'] = is_zipcode_servicable_cod($address['pincode']);
				} */
			}
			$shippingCity = strtolower($address ['city']);
			$country = $address['country'];
			$state = $address['statename'];
			$shippingoptions = func_load_shipping_options();
			$logisticId = $shippingoptions[0]['code'] ;

			/*if($country!="IN" ){
				$smarty->assign('can_be_converted_COD',"0");
			}else{
				$smarty->assign('can_be_converted_COD',"1");
			}*/
			//$smarty->assign('can_be_converted_COD',"1");
		}

        // assuming by default that all orders are COD enabled
        $smarty->assign('can_be_converted_COD',"1");


        if($pm == "cod"){
            $codCharge = FeatureGateKeyValuePairs::getInteger('codCharge');
            //additional discount for citibank
            if($codCharge>0) {
                $cod_charge = $codCharge;
            }
            $response['mode']='COD';
        }else{
            // commenting since is not used
            //$response['mode']='IVR';
        }
        
		$readyForCheckout = $mcart->isReadyForCheckout();
		$product_availability = $mcart->getItemsAvailabilityArray();
		$smarty->assign('product_availability',$product_availability);

        if(!$readyForCheckout){
            writeMessage('F', 3, "Item/s is/are out of stock in the cart, remove those items from the cart.");
        }
        
		$smarty->assign('readyForCheckout',$readyForCheckout);
		$smarty->assign("vat", number_format($vat, 2, ".", ','));
		$smarty->assign("amount", number_format(round($amount), 2, ".", ','));
		$smarty->assign("totalAmount", number_format(round($totalAmount), 2, ".", ','));   // totalAmount and totalMRP are same if there is no discount coupon
		$smarty->assign("mrp", number_format($mrp, 2, ".", ','));

        // coupons display
		$smarty->assign("coupons",$coupons);
		$smarty->assign("couponCode", $discountCouponCode);
        $smarty->assign('couponDiscount',number_format(round($coupondiscount), 2, ".", ','));
        $smarty->assign('couponMessage',$couponMessage);

        // cashback display
        $smarty->assign("cashcoupons",$cashCoupons);
        $smarty->assign("cashCouponCode", $cashCouponCode);
        $smarty->assign("cashCouponDiscount",number_format(round($cashdiscount), 2, ".", ','));
        $smarty->assign('cashCouponMessage',$cashCouponMessage);
       
		/*if(empty($cashCouponCode)){
			$cashbackAmount = (!empty($cashCoupons[0]['MRPAmount']))? $cashCoupons[0]['MRPAmount'] : 0 ;
            $cashCouponMessage = "Use your Myntra credits. Current balance: Rs ".number_format(round($cashbackAmount), 2, ".", ',')."<br/>";
            $smarty->assign('myntCashUsageMessage',$cashCouponMessage);
        }*/
 
        $smarty->assign("shippingCharges", number_format(round($shippingCharge), 2, ".", ','));

        $smarty->assign("eossSavings", number_format(round($productDiscount), 2, ".", ','));

	if($action=='placeorder' && empty($shipaddressid)){
		writeMessage('F', 2, "Please choose shipping Address");
		$readyForCheckout=false;
	}
        // checkout
		if($action=='placeorder' && $readyForCheckout) {
			if($mcart->getItemQuantity()==0){				
				writeMessage('F', 4, "Cart is empty");
			}

			if(empty($shipaddressid)){
                writeMessage('F', 2, "Please choose shipping Address");
			}           
			
			// set cart modified time
            $mcart->setLastContentUpdatedTime($XCART_SESSION_VARS['LAST_CART_UPDATE_TIME']);           

            //Create Order Data Object Builder from which Order DO and all child DOs get created
            $orderDOBuilder = new OrderDOBuilder();

            //Build OrderDO from the cart
            try {
                if($mcart == null) {
                    throw new EmptyCartException("Cart Empty while checkout: may be due to concurrent activity");
                }
                
                $orderDO = $orderDOBuilder->build($mcart);
                $orderDO->setChannel("telesales");
                
            } catch (UserNotLoggedInException $exc) {                         
                writeMessage($status='F', $code=3, $message=$exc->__toString());
                
            } catch (EmptyCartException $exc) {
                writeMessage($status='F', $code=3, $message=$exc->__toString());
                
            } catch (Exception $exc) {
                writeMessage($status='F', $code=3, $message=$exc->__toString());
            }

            //Get an instance of OrderManager, which will do the top level business transaction
            $orderManager = new OrderManager($orderDO);

            //Stage-1: Create order by doing checkout
            try {
                $orderManager->checkout();
                EventCreationManager::pushCompleteOrderEvent($orderDO->getOrderId());
            } catch (UserNotLoggedInException $exc) {
                writeMessage($status='F', $code=3, $message=$exc->__toString());

            } catch (EmptyCartException $exc) {
                writeMessage($status='F', $code=3, $message=$exc->__toString());

            } catch (CartTamperedException $exc) {
                writeMessage($status='F', $code=3, $message=$exc->__toString());

            } catch (CashOnDeliveryEligibilityFailedException $exc) {
                $smarty->assign('can_be_converted_COD',"0");
                writeMessage($status='F', $code=3, $message=$exc->__toString());

            } catch (NoShippingAddressException $exc) {
                writeMessage($status='F', $code=3, $message=$exc->__toString());

            } catch(CouponException $exc) {
                writeMessage($status='F', $code=3, $message=$exc->__toString());

            } catch(OrderCreationFailedException $exc) {
                writeMessage($status='F', $code=3, $message=$exc->__toString());

            } catch (NoSuitableGatewayException $exc) {
                writeMessage($status='F', $code=3, $message=$exc->__toString());

            } catch (Exception $exc) {
                writeMessage($status='F', $code=3, $message=$exc->__toString());
            }

            // get orderid
            $orderid = $orderDO->getOrderId();


			db_query("insert into telesales_logs (orderid,cclogin,action) values('$orderid','$login','CREATED')");

			$smarty->assign('order_message','placed');
			$smarty->assign('payment_method',$pm);
			$response['orderid']=$orderid;
		}
		
		$smarty->assign('orderid',$orderid);
		$smarty->assign("productsInCart",$products);
		$response['html']= func_display("admin/telesales_cart.tpl", $smarty, false);
		
	}		
showMessage();

}else if($action=='getshippingaddress' || $action=='addshippingaddress' ){

	if($action=='addshippingaddress'){
		   	$data = array(
                'login' => $cloginid,
                'default_address' => 0,
                'name' => mysql_escape_string(trim($name)),
                'address' => mysql_escape_string(trim($address)),
                'city' => mysql_escape_string(trim($city)),
                'state' => mysql_escape_string(trim($state)),
                'country' => mysql_escape_string(trim($country)),
                'pincode' => mysql_escape_string(trim($pincode)),
                'mobile' => mysql_escape_string(trim($mobile)),
                'email' => mysql_escape_string(trim($email)),
                'phone' => mysql_escape_string(trim($telephone)),
                'datecreated' => date("Y-m-d H:i:s")
    	    );

   		$id = func_array2insert('mk_customer_address', $data);
	}

	$mcartFactory = new MCartFactory();
	$mcart = $mcartFactory->getCartForLogin($cloginid,true,'telesales');
	$productsInCart = false;
	$jewelleryItemsInOrder = 'none'; // other values can be 'some', 'all'
	if($mcart && $mcart != null) {
		$productsInCart = MCartUtils::convertCartToArray($mcart);
		$jewelleryItemsCount = 0;
		$skuDetails = array();
		foreach($productsInCart as $index=>$product) {
			$fineJewelleryItem = func_query("select * from mk_attribute_type at, mk_attribute_type_values av, mk_style_article_type_attribute_values sav where at.id = av.attribute_type_id and av.id = sav.article_type_attribute_value_id and at.catalogue_classification_id = ".$product['article_type']." and sav.product_style_id = ".$product['productStyleId'] ." and av.attribute_value = 'Fine Jewellery'", true);
			if($fineJewelleryItem && count($fineJewelleryItem)>0){
				$jewelleryItemsCount++;
				$jewelleryItemsInOrder = 'some';
			}
			$productsInCart[$index]['style_id'] = $product['productStyleId'];
			$productsInCart[$index]['sku_id'] = $product['skuid'];
			$skuDetails[$product['skuid']] = array('id'=>$product['skuid'], 'availableItems'=> $product['availableItems'], 'warehouse2AvailableItems'=> $product['warehouse2AvailableItems']);
		}
		
		if(count($productsInCart) == $jewelleryItemsCount)
			$jewelleryItemsInOrder = 'all';
			 
	}
	
	$addresses = func_select_query('mk_customer_address', array('login' => $cloginid), 0, 9, array('datecreated' => 'desc'));
	$country = isset($addresses[0]) ? $addresses[0]['country'] : "";
	$city = isset($addresses[0]) ?  $addresses[0]['state'] : "";
	$state = isset($addresses[0]) ?  $addresses[0]['city'] : "";

    foreach($addresses as $key=>$address){
        $state=getState($address['state']);
        $addresses[$key]['state_code']=$address['state'];
        $addresses[$key]['state']=$state;
        //$country=getCountry($address['country']);
        //Passing country code instead of name because drop down expects code to match selected
        $country=$address['country'];
        $addresses[$key]['country_code']=$address['country'];
        $addresses[$key]['country']=getCountry($country);
        if($productsInCart) {
        	$addresses[$key]['isServiceable'] = func_check_cod_serviceability_products($address['pincode'],$productsInCart, $jewelleryItemsInOrder, false, $skuDetails);
        } else {
        	$addresses[$key]['isServiceable'] = is_zipcode_servicable_cod($address['pincode']);
        }
	}

	$response['status']='S';
	$countries = func_get_countries();
	$states = func_get_states("IN");
	$smarty->assign("countries", $countries);
	$smarty->assign("states", $states);
	$smarty->assign("addresses",$addresses);
	$response['html']= func_display("admin/shipping_address.tpl", $smarty, false);
    writeMessage();
	showMessage();
}

$smarty->assign("main","telesales_view");
func_display("admin/home.tpl",$smarty);

function getState($state){
	$result = func_select_first('xcart_states', array('code' => $state));
	return empty($result['state'])?$state:$result['state'];
}
function getCountry($country){
	$result = func_select_first('xcart_languages', array('name' => 'country_'.$country,'topic' => 'Countries'));
	return $result['value'];
}

function writeMessage($status=null, $code=null, $message=null){
    global $response;

    if(!empty($status)){
        $response['status'] = $status;
    }
    if(!empty($code)){
        $response['errorcode'] = $code;
    }
    if(!empty($message)){
        $response['errormessage'] = $message;
    }    
}

function showMessage(){
    global $response;

    echo json_encode($response);
    exit;
}
