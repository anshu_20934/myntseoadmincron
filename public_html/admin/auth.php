<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: auth.php,v 1.40 2006/02/10 14:27:30 svowl Exp $
#
define('AREA_TYPE', 'A');
if (!defined('ADMIN_INIT')) {
	define('ADMIN_INIT', 1);
}
@include_once "./top.inc.php";
@include_once "../top.inc.php";
@include_once "../../top.inc.php";
@include_once "../../../top.inc.php";
@include_once("../class.imageconverter.php");
if (!defined('DIR_CUSTOMER')) die("ERROR: Can not initiate application! Please check configuration.");
define('MYNTRA_HOME_DIR',$xcart_dir);
@include_once("../class.imageconverter.php");
@include_once("adminerrorMessagesArray.php");
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");


################ LOGGERS INITIALIZED  #################
define('LOG4PHP_DIR', MYNTRA_HOME_DIR."/log4php");
define('LOG4PHP_CONFIGURATION', MYNTRA_HOME_DIR."/myntralogger.properties");
define('LOG4PHP_CONFIGURATOR_CLASS',LOG4PHP_DIR."/LoggerPropertyConfigurator"); 
include_once(LOG4PHP_DIR . '/LoggerManager.php');
$weblog = LoggerManager::getLogger('MYNTRAWEBLOGGER');
$errorlog = LoggerManager::getLogger('MYNTRAERRORLOGGER');
$sqllog = LoggerManager::getLogger('MYNTRASQLLOGGER'); 
$apilog = LoggerManager::getLogger('WMSAPILOGGER');
$portalapilog = LoggerManager::getLogger('PORTALAPILOGGER');
$solrlog = LoggerManager::getLogger('SOLRLOGGER');
$maillog = LoggerManager::getLogger('MAILLOGGER');
$reportlog = LoggerManager::getLogger('REPORTLOGGER');
$fraudOrderLog 		= LoggerManager::getLogger('FRAUDORDERLOGGER');
$authLogger         = LoggerManager::getLogger('MYNTRAAUTHLOGGER');
## usage ###
## FOR ALL INFO AND DEBUG LOG WILL GO INTO #####
## $weblog->info($msg);
## $weblog->debug($msg);
## For ALL SQL LOG
## $sqllog->debug($sql); 
## FOR ALL ERROR LOG
## $errorlog->error($errormsg);

###################################################################
include_once $xcart_dir."/admininit.php";
@include_once $xcart_dir . "/admin/order_type_search.php";
x_session_register("login");
x_session_register("login_type");

x_session_register("logged");

x_session_register("export_ranges");

$smarty->assign("js_enabled", "Y");

x_session_register("top_message");
if (!empty($top_message)) {
	$smarty->assign("top_message", $top_message);
	if($config['Adaptives']['is_first_start'] != 'Y')
		$top_message = "";
	x_session_save("top_message");
}

$current_area="A";

//minified js and css for admin interface
$smarty->assign("admin_js_path",$admin_js_path);
$smarty->assign("cdn_base",$cdn_base);


include $xcart_dir."/include/get_language.php";

$location = array();
$location[] = array(func_get_langvar_by_name("lbl_main_page"), "home.php");

@include $xcart_dir."/modules/gold_auth.php";
include $xcart_dir."/include/check_useraccount.php";

x_session_save();

# Create the user types list for search form
$usertypes = array("A"=>func_get_langvar_by_name("lbl_administrator"), "P"=>func_get_langvar_by_name("lbl_provider"), "C"=>func_get_langvar_by_name("lbl_customer"));
$usertypes['B'] = func_get_langvar_by_name("lbl_partner");

if (!empty($active_modules["Simple_Mode"])) {
	unset($usertypes["A"]);
	$usertypes["P"] = func_get_langvar_by_name("lbl_administrator");
}

$smarty->assign("redirect","admin");

include_once "role_access_control.php";

putenv("TZ=Asia/Calcutta");

$cashback_gateway_status = FeatureGateKeyValuePairs::getFeatureGateValueForKey('cashbackenabled');

$system_free_shipping_amount = WidgetKeyValuePairs::getWidgetValueForKey('shipping.charges.cartlimit');
$system_shipping_rate = WidgetKeyValuePairs::getWidgetValueForKey('shipping.charges.amount');

// code to read mrp configuration related data
include_once "$xcart_dir/modules/coupon/MRPConstants.php";
const COUPONCONFIGURATION_CACHEKEY = "couponconfiguration_xcache";
global $xcache ;
if($xcache==null){
	$xcache = new XCache();
}
$mrpCouponConfiguration = $xcache->fetchAndStore(function() {
global $mrpDefaultCouponConfiguration;
//$mrpCouponConfiguration = FeatureGateKeyValuePairs::getAssociativeArray('mrp.couponConfiguration',$mrpDefaultCouponConfiguration);
$couponConfiguration['mrp_firstLogin_nonfbreg_numCoupons'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.nonfbreg.numCoupons", $mrpDefaultCouponConfiguration['mrp_firstLogin_nonfbreg_numCoupons']);
$couponConfiguration['mrp_firstLogin_nonfbreg_mrpAmount'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.nonfbreg.mrpAmount", $mrpDefaultCouponConfiguration['mrp_firstLogin_nonfbreg_mrpAmount']);
$couponConfiguration['mrp_firstLogin_nonfbreg_validity'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.nonfbreg.validity", $mrpDefaultCouponConfiguration['mrp_firstLogin_nonfbreg_validity']);
$couponConfiguration['mrp_firstLogin_nonfbreg_minCartValue'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.nonfbreg.minCartValue", $mrpDefaultCouponConfiguration['mrp_firstLogin_nonfbreg_minCartValue']);

$couponConfiguration['mrp_firstLogin_fbreg_numCoupons'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.fbreg.numCoupons", $mrpDefaultCouponConfiguration['mrp_firstLogin_fbreg_numCoupons']);
$couponConfiguration['mrp_firstLogin_fbreg_mrpAmount'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.fbreg.mrpAmount", $mrpDefaultCouponConfiguration['mrp_firstLogin_fbreg_mrpAmount']);
$couponConfiguration['mrp_firstLogin_fbreg_validity'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.fbreg.validity", $mrpDefaultCouponConfiguration['mrp_firstLogin_fbreg_validity']);
$couponConfiguration['mrp_firstLogin_fbreg_minCartValue'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.fbreg.minCartValue", $mrpDefaultCouponConfiguration['mrp_firstLogin_fbreg_minCartValue']);


$couponConfiguration['mrp_refFirstPurchase_numCoupons'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.numCoupons", $mrpDefaultCouponConfiguration['mrp_refFirstPurchase_numCoupons']);
$couponConfiguration['mrp_refFirstPurchase_mrpAmount'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.mrpAmount", $mrpDefaultCouponConfiguration['mrp_refFirstPurchase_mrpAmount']);
$couponConfiguration['mrp_refFirstPurchase_validity'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.validity", $mrpDefaultCouponConfiguration['mrp_refFirstPurchase_validity']);
$couponConfiguration['mrp_refFirstPurchase_minCartValue'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.minCartValue", $mrpDefaultCouponConfiguration['mrp_refFirstPurchase_minCartValue']);


$couponConfiguration['mrp_refRegistration_numCoupons'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.numCoupons", $mrpDefaultCouponConfiguration['mrp_refRegistration_numCoupons']);
$couponConfiguration['mrp_refRegistration_mrpAmount'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.mrpAmount", $mrpDefaultCouponConfiguration['mrp_refRegistration_mrpAmount']);
$couponConfiguration['mrp_refRegistration_validity'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.validity", $mrpDefaultCouponConfiguration['mrp_refRegistration_validity']);
$couponConfiguration['mrp_refRegistration_minCartValue'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.minCartValue", $mrpDefaultCouponConfiguration['mrp_refRegistration_minCartValue']);

$couponConfiguration['cust_survey_validity'] =
	FeatureGateKeyValuePairs::getInteger("mrp.cust.survey.validity", $mrpDefaultCouponConfiguration['cust_survey_validity']);
$couponConfiguration['cust_survey_mrpAmount'] =
	FeatureGateKeyValuePairs::getInteger("mrp.cust.survey.mrpAmount", $mrpDefaultCouponConfiguration['cust_survey_mrpAmount']);
$couponConfiguration['cust_survey_minCartValue'] =
	FeatureGateKeyValuePairs::getInteger("mrp.cust.survey.minCartValue", $mrpDefaultCouponConfiguration['cust_survey_minCartValue']);

	return $couponConfiguration;
}, array(), COUPONCONFIGURATION_CACHEKEY);


include_once "$xcart_dir/include/class/abtest/MABTest.php";
?>
