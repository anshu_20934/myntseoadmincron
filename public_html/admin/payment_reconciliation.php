<?php
require_once "./auth.php";
require_once $xcart_dir . "/include/security.php";
//require_once $xcart_dir."/PHPExcel/PHPExcel.php";

$file = $HTTP_POST_FILES['reconciliation_file']['tmp_name'];

$startTime=time();

$messages = array();
if ($file) {
	set_time_limit (0);
	
	$fd = fopen ($file, "r");
	$count = 0 ;
	$k=0;
	$sql = array();
	while (!feof ($fd)) {
		$error =false;
		$k++;
		
		$buffer = fgetcsv($fd, 4096);
		
		if($k==1) {
			continue;
		}
		//echo "HERE11";print_r($buffer);var_dump($buffer);fclose($fd);exit;
		
		$orderid = (string)trim($buffer[0]);//$cell[1];
		
		if(empty($orderid)){
			$messages[] = "<span style='color:red'>Error at row $k: Order id is empty</span>";
			$error = true;
			continue;
		}
		
		if(!is_numeric($orderid)||((int)$orderid!=$orderid)) {
			$messages[] = "<span style='color:red'>Error at row $k :  orderid $orderid is not integer</span>";
			$error = true;
			continue;
		}
		
		$captured_amount = (string)trim($buffer[1]);//$cell[2];	
		//echo "$captured_amount";exit;
		if(!empty($captured_amount) && !is_numeric($captured_amount)) {
			$messages[] = "<span style='color:red'>Error at row $k Captured Amount: $captured_amount for orderid $orderid is not numeric</span>";
			$error = true;
		}
		
		$refund = (string)trim($buffer[2]);//$cell[3];
		if(!empty($refund) && (!is_numeric($refund) || $refund >0)) {
			$messages[] = "<span style='color:red'>Error at row $k Refund amount: $refund for orderid $orderid is not numeric or is +ve</span>";
			$error = true;
		}
		
		$charge_back = (string)trim($buffer[3]);//$cell[4];
		if(!empty($charge_back) && (!is_numeric($charge_back) || $charge_back>0)) {
			$messages[] = "<span style='color:red'>Error at row $k charge back amount: $charge_back for orderid $orderid is not numeric or is +ve</span>";
			$error = true;
		}
		
		$tdr = (string)trim($buffer[4]);//$cell[5];
		if(!empty($tdr) && !is_numeric($tdr)) {
			$messages[] = "<span style='color:red'>Error at row $k tdr amount: $tdr for orderid $orderid is not numeric</span>";
			$error = true;
		}
		
		$service_tax = (string)trim($buffer[5]);//$cell[6];
		if(!empty($service_tax) && !is_numeric($service_tax)) {
			$messages[] = "<span style='color:red'>Error at row $k service tax: $service_tax for orderid $orderid is not numeric<span>";
			$error = true;
		} 	
		
		$payment_gateway_name = (string)trim($buffer[6]);//$cell[7];
		
		$cap_date = (string)trim($buffer[7]);
	
		if(!empty($cap_date)) {
			
			try{
				$captured_date = date_create_from_format('d/m/Y',$cap_date)->getTimestamp();
			} catch (Exception $e){
				
			}
		}
		
		if(empty($captured_date)  || $captured_date < 0) {
			$messages[] = "<span style='color:red'>Invalid date format at row $k: $cap_date for orderid $orderid, expected format dd/mm/yyyy<span>";
			$error = true;
		}
		
		if($error) continue;
		
		$dataToInsert = array();
		if(!empty($_POST['overwriteCapturedAmount'])) {
			$dataToInsert['captured_amount'] = $captured_amount;
		}
		
		if(!empty($_POST['overwriteRefundAmount'])) {
			$dataToInsert['refund'] = $refund;
		}
		
		if(!empty($_POST['overwriteCBAmount'])) {
			$dataToInsert['chargeback'] = $charge_back;
		}
		
		if(!empty($_POST['overwriteTDR'])) {
			$dataToInsert['tdr'] = $tdr;
		}
		
		if(!empty($_POST['overwriteServiceTax'])) {
			$dataToInsert['service_tax'] = $service_tax;
		}
		
		if(!empty($_POST['overwriteGatewayName'])) {
			$dataToInsert['payment_gateway_name'] = $payment_gateway_name;
		}
		
		if(!empty($_POST['overwriteCapturedDate'])) {
			$dataToInsert['captured_date'] = $captured_date;
		}
		
		$dataToInsert['update_time'] = time();
		$dataToInsert['update_by'] = $login;				
		$alreadyPresent = func_query_first_cell("select count(*) from mk_payment_reconciliation_log where orderid=$orderid",true); //sending to slave db
		if($alreadyPresent>0) {
			//update
			func_array2updateWithTypeCheck("mk_payment_reconciliation_log", $dataToInsert, "orderid=$orderid");
		} else {
			//insert
			$dataToInsert['orderid'] = $orderid;
			$dataToInsert['insert_time'] = time();
			$sql[] = "('" . implode("', '", $dataToInsert) . "')";
			
			
			if($count%1000 ==0) {
				$insert_query ='INSERT INTO mk_payment_reconciliation_log (' . implode(", ", array_keys($dataToInsert)) . ') VALUES '.implode(',', $sql);
				//print_r($insert_query);exit; 
				db_query($insert_query);
				
				unset($sql);
				$sql=array();
			}
		}
		$count++ ;
		unset($buffer);
	}
	
	//Process Remaining Inserts
	if(!empty($sql)) {
		$dataToInsert['orderid'] = $orderid;
		$dataToInsert['insert_time'] = time();
		$insert_query ='INSERT INTO mk_payment_reconciliation_log (' . implode(", ", array_keys($dataToInsert)) . ') VALUES '.implode(',', $sql); 
		db_query($insert_query);				
		unset($sql);
	}
	fclose ($fd);
	$messages [] ="<span style='color:green'>". $count . ' entries from reconciliation file processed successfully' . "</span>";
	if(empty($_POST['overwriteCapturedAmount'])) {
		$smarty->assign('unselect_capture', 1);
	}
	
	if(empty($_POST['overwriteRefundAmount'])) {
		$smarty->assign('unselect_refund', 1);
	}
	
	if(empty($_POST['overwriteCBAmount'])) {
		$smarty->assign('unselect_chargeback', 1);
	}
	
	if(empty($_POST['overwriteTDR'])) {
		$smarty->assign('unselect_tdr', 1);
	}
	
	if(empty($_POST['overwriteServiceTax'])) {
		$smarty->assign('unselect_tax', 1);
	}
	
	if(empty($_POST['overwriteGatewayName'])) {
		$smarty->assign('unselect_gateway', 1);
	}
	
	if(empty($_POST['overwriteCapturedDate'])) {
		$smarty->assign('unselect_date', 1);
	}

}
$endTime=time();
//echo "TOTAL TIME TAKEN  = " . ($endTime-$startTime);

$smarty->assign("messages",$messages);
$smarty->assign("main","payment_reconciliation");
func_display("admin/home.tpl",$smarty);
?>