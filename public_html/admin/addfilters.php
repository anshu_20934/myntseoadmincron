<?php
/**
 * Created by Nishith Shrivastav.
 * User: myntra
 * Date: Nov 13, 2010
 * Time: 7:53:54 PM
 */
#
# Purpose : This file is created to add, edit and delete filters ######
#

require "./auth.php";

require $xcart_dir."/include/security.php";
include_once HostConfig::$documentRoot ."/Profiler/Profiler.php";
require $xcart_dir."/utils/imagetransferS3.php";
use imageUtils\Jpegmini;
define('FORM_ACTION','addfilters.php');
$location[] = array("Add Filters", "");
define("FILTERS_IMAGES_DIR",'../images/filters');
$groupToModify = '';
if($REQUEST_METHOD == "GET" && $_GET['mode'] == 'changeorder')
{
    $sql="update mk_filters set display_order='".$_GET['order']."' where id=".$_GET['id'];
              $upd=func_query($sql);
    exit;
}
if($REQUEST_METHOD == "POST")
{
      if($HTTP_POST_VARS['mode'] == 'getAllFilters')
      {
            $get_sql ="SELECT * FROM  mk_filters where is_active=1";
            $allfilters = func_query($get_sql);
            $all_filter_html="<option value=''>None</option>";
            foreach($allfilters as $value)
            {
                $all_filter_html.="<option value='".$value['id']."'>".$value['filter_name']."</option>";
            }
            echo $all_filter_html;exit;
      }
      else if($HTTP_POST_VARS['mode'] == 'addNewFilter')
      {
           if(empty($HTTP_POST_VARS["filter_name"]) || empty($HTTP_POST_VARS["filter_group_id"])){ echo "Please enter valid data";exit;}
           $query_data = array(
                        "filter_name"  =>(!empty($HTTP_POST_VARS["filter_name"]) ? trim($HTTP_POST_VARS["filter_name"]) : ''),
                        "filter_group_id"  =>(!empty($HTTP_POST_VARS["filter_group_id"]) ? $HTTP_POST_VARS["filter_group_id"] : ''),
                        "display_order"  =>(!empty($HTTP_POST_VARS["display_order"]) ? $HTTP_POST_VARS["display_order"] : ''),
                        "is_active" =>  '1'
                        ) ;

           $filter_id=func_array2insert('mk_filters', $query_data);
           $map_name='';

           if(!empty($HTTP_POST_VARS["filter_map_id"]))
           {
              $query_data = array(
                               "filter_id"  =>$filter_id,
                               "filter_map_id"  =>(!empty($HTTP_POST_VARS["filter_map_id"]) ? $HTTP_POST_VARS["filter_map_id"] : ''),
                               "is_active" =>  '1'
                               ) ;
              func_array2insert ('mk_filter_mapping', $query_data);
              $map_name_sql="select filter_name from mk_filters where id=".$HTTP_POST_VARS["filter_map_id"];
              $map_name = func_query_first_cell($map_name_sql);
           }

          $html='<tr>
                        <td width="20%" align="center">'.$HTTP_POST_VARS["filter_name"].'</td>
                        <td width="20%" align="center">'.$map_name.'</td>
                        <td width="10%" align="center">Yes</td>
                        <td width="10%" align="center">'.$HTTP_POST_VARS["display_order"].'</td>
                        <td width="20%" align="center"><a href="'.$http_location.'/admin/addfilters.php?mode=modifyfilter&filterid='.$filter_id.'">Modify</a></td>
                    </tr>';
          echo $html;exit;
      }
      else if ($HTTP_POST_VARS['mode'] == 'savefilter')
      {
            $logo_uploadfile = FILTERS_IMAGES_DIR.DIRECTORY_SEPARATOR.basename($HTTP_POST_FILES['logo']['name']);

            if(is_uploaded_file($HTTP_POST_FILES['logo']['tmp_name']))
            {
                if (move_uploaded_file($HTTP_POST_FILES['logo']['tmp_name'], $logo_uploadfile))
                {
                    $logo_image_path=str_replace("../","",$logo_uploadfile);
                    
                    if(moveToDefaultS3Bucket($logo_image_path)&& Jpegmini::copyCompressedPlaceholderFile($logo_image_path)&& myntra\utils\cdn\imagetransferS3::compress($logo_image_path)){
                        $logo_image_path = "$cdn_base/$logo_image_path";
                        Profiler::increment("imagesize-filter-logo", filesize(HostConfig::$documentRoot.$logo_image_path));
                    }else{
                     	$logo_image_path = null;
                    }
                    
                }
            }
            else
            {
                $logo_image_path='';
            }
            $banner_uploadfile = FILTERS_IMAGES_DIR.DIRECTORY_SEPARATOR.basename($HTTP_POST_FILES['top_banner']['name']);
            if(is_uploaded_file($HTTP_POST_FILES['top_banner']['tmp_name']))
            {
                if (move_uploaded_file($HTTP_POST_FILES['top_banner']['tmp_name'], $banner_uploadfile))
                {
                    $banner_image_path=str_replace("../","",$banner_uploadfile);
                    if(moveToDefaultS3Bucket($banner_image_path)&& Jpegmini::copyCompressedPlaceholderFile($banner_image_path)&& myntra\utils\cdn\imagetransferS3::compress($banner_image_path)){
                       	$banner_image_path = "$cdn_base/$banner_image_path";
                        Profiler::increment("imagesize-filter-banner", filesize(HostConfig::$documentRoot.$banner_image_path));
                    }else{
                        $banner_image_path = null;
                    }
                }
            }
            else
            {
                $banner_image_path='';
            }
          $narrow_by=implode(",",$HTTP_POST_VARS["narrow_by"]);
          $query_data = array(
                "filter_name"  =>(!empty($HTTP_POST_VARS["filter_name"]) ? trim($HTTP_POST_VARS["filter_name"]) : ''),
                "display_order"  =>(!empty($HTTP_POST_VARS["display_order"]) ? $HTTP_POST_VARS["display_order"] : ''),
                "filter_url"  =>(!empty($HTTP_POST_VARS["filter_url"]) ? trim($HTTP_POST_VARS["filter_url"]) : ''),
                "filter_group_id"  =>(!empty($HTTP_POST_VARS["filter_group_id"]) ? $HTTP_POST_VARS["filter_group_id"] : ''),
                "logo_url"  =>(!empty($HTTP_POST_VARS["logo_url"]) ? $HTTP_POST_VARS["logo_url"] : ''),
                "top_banner_url"  =>(!empty($HTTP_POST_VARS["top_banner_url"]) ? $HTTP_POST_VARS["top_banner_url"] : ''),
                "top_banner_alt_tag"  =>(!empty($HTTP_POST_VARS["top_banner_alt_tag"]) ? $HTTP_POST_VARS["top_banner_alt_tag"] : ''),
                "logo_alt_tag"  =>(!empty($HTTP_POST_VARS["logo_alt_tag"]) ? $HTTP_POST_VARS["logo_alt_tag"] : ''),
                "about_filter"  =>(!empty($_POST['about_filter']) ? mysql_real_escape_string($_POST['about_filter']): ''),
                "narrow_by"  =>(!empty($narrow_by) ? $narrow_by : 1),
                "is_active" =>  (!empty($HTTP_POST_VARS["is_active"]) ? $HTTP_POST_VARS["is_active"] : '0'),
				"h1_title" =>  (!empty($HTTP_POST_VARS["h1_title"]) ? trim($HTTP_POST_VARS["h1_title"]) : ''),
				"meta_keywords" =>  (!empty($HTTP_POST_VARS["meta_keywords"]) ? trim($HTTP_POST_VARS["meta_keywords"]) : ''),
				"meta_description" =>  (!empty($HTTP_POST_VARS["meta_description"]) ? trim($HTTP_POST_VARS["meta_description"]) : ''),
				"page_title" =>  (!empty($HTTP_POST_VARS["page_title"]) ? trim($HTTP_POST_VARS["page_title"]) : '')
			   ) ;
                if(!empty($logo_image_path))
                    $query_data["logo"]  = "$logo_image_path";
                if(!empty($banner_image_path))
                    $query_data["top_banner"]  = "$banner_image_path";
                if(!empty($HTTP_POST_VARS["remove_top_banner"]))
                    $query_data["top_banner"]  ='';
                if(!empty($HTTP_POST_VARS["remove_logo"]))
                    $query_data["logo"]  ='';
          $id=$HTTP_POST_VARS["filter_id"];
          func_array2update("mk_filters", $query_data, "id='$id'");
          $sql="select * from mk_filter_mapping where filter_id=".$id;
          $result=func_query($sql);
          $filter_map_id=  (!empty($HTTP_POST_VARS["filter_map_id"]) ? $HTTP_POST_VARS["filter_map_id"] : '');
          if(!empty($result[0]))
          {
              $sql="update mk_filter_mapping set filter_map_id='".$filter_map_id."' where filter_id=".$id;
              $upd=func_query($sql);
          }
          else
          {
              if(!empty($filter_map_id))
              {
                  $query_data = array(
                       "filter_id"  =>$id,
                       "filter_map_id"  =>$filter_map_id,
                       "is_active" =>  '1'
                       ) ;
                  func_array2insert ('mk_filter_mapping', $query_data);
              }
          }
      }
}
if(isset($_GET['mode']) && $_GET['mode'] == 'modifyfilter')
{
    $filter_id=$_GET['filterid'];
    $sql ="SELECT f.*,g.group_name FROM  mk_filters f join mk_filter_group g on f.filter_group_id=g.id where f.id=".$filter_id;
    $filter_details = func_query($sql);
    if(empty($filter_details[0])){echo "Filter doesn't exists";exit;}
    $filter_details=$filter_details[0];
    $map_sql = "select * from mk_filter_mapping where filter_id=".$filter_id;
    $mapping_details = func_query($map_sql);
    $sql ="SELECT * FROM  mk_filter_group";
    $grouprecords = func_query($sql);
    $sql ="SELECT * FROM  mk_filters where is_active=1";
    $filters = func_query($sql);
    $smarty->assign("filter_id",$filter_id);
    $smarty->assign("method","modifyfilter");
    $smarty->assign("filter_details",$filter_details);
    $smarty->assign("mapping_details",$mapping_details);
    $smarty->assign("groups",$grouprecords);
    $smarty->assign("filters",$filters);
    $smarty->assign("selected_narrow_by",explode(",",$filter_details['narrow_by']));

}
else
{
    $main_array=array();
    $filter_groups=array();
    $sql ="SELECT * FROM  mk_filter_group";
    $grouprecords = func_query($sql);
    //echo "<pre>";print_r($grouprecords);die;
    foreach($grouprecords as $groups)
    {
        $filter_sql="select f.*,g.group_name from mk_filters f join mk_filter_group g on f.filter_group_id=g.id where filter_group_id=".$groups['id'];
        $filter_by_groups = func_query($filter_sql);

        foreach($filter_by_groups as $key=>$filters)
        {
            $filter_map_sql="select f.filter_name from mk_filter_mapping m join mk_filters f on m.filter_map_id=f.id where m.filter_id=".$filters['id'];
            $filter_map = func_query($filter_map_sql);
             if(!empty($filter_map[0]))
              $filter_by_groups[$key]['mapsto']= $filter_map[0]['filter_name'];
        }
        $main_array[$groups['group_name']]=$filter_by_groups;
        $filter_groups[$groups['group_name']]=$groups['id'];

    }
    //echo "<pre>";print_r($main_array);print_r($grouprecords);die;
    $smarty->assign("groups",$filter_groups);
    $smarty->assign("filters",$main_array);
    $smarty->assign("method","list");
}

$smarty->assign("main","addfilters");


# Assign the current location line
$smarty->assign("location", $location);
$smarty->assign("form_action", FORM_ACTION);
# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>

