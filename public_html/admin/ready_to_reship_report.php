<?php
require "./auth.php";
require_once $xcart_dir."/include/func/func.mkcore.php";
require_once $xcart_dir."/include/func/func.returns.php";
require_once $xcart_dir."/PHPExcel/PHPExcel.php";

x_load('db');

$couriers = get_supported_courier_partners(false);

$sql = "select * from xcart_returns where status = 'RQF'";
$results = func_query($sql);
$selected_results = array();
$weblog->info("Results - ". count($results));
if(count($results) > 0) {
	foreach($results as $row) {
		$available_couriers = check_pickup_availability($row['zipcode']);
		$supported = false;
		foreach ($available_couriers as $courier) {
			if($courier['name'] == $_GET['format']) {
				$supported = true;
			}
		}
		if($supported) {
			$selected_results[] = $row;
		}
	}
}
$weblog->info("Filtered Results - ". count($selected_results));
$return_requests = format_return_requests_fields($selected_results);

$filename = "";
$file_name_suffix = "-reship-".date("dmy");
foreach($couriers as $courier) {
	if($courier['code'] == $_GET['format']) {
		$filename =  $courier['code'].$file_name_suffix;
	}
}

if($filename != "") {
	$xls_data = get_xls_data_from_requests($return_requests, $_GET['format']);
		
	$objPHPExcel = new PHPExcel();
	$objPHPExcel = func_write_data_to_excel($objPHPExcel, $xls_data, 0);
	
	header("Content-Disposition: attachment; filename=$filename.xls");
	ob_clean();
	flush();
	try {
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->setPreCalculateFormulas(false);
		$objWriter->save('php://output');
	}catch(Exception $ex) {
		$weblog->info("Error Occured = ". $ex->getMessage());
	}
	$weblog->info("Saved");
}
?>