<?php

header("Location: orders_search_new.php");

require "./auth.php";
require_once $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/class/class.ordersearch.php";



if($HTTP_GET_VARS){
	$query_string = http_build_query($HTTP_GET_VARS);
}
echo $query_string;
# Creates an array of search criteria and passes it to a function to create a query
if($mode == "search")
{
	#
	#create search query using ordersearch class function
	$search_criteria = array(
		'ORDER_STATUS' 			=> $order_status,
		'ORDER_MAX_QUEUED_TIME'	=> $order_max_queued_time,
		'ORDER_MIN_QUEUED_TIME'	=> $order_min_queued_time,
		'ORDER_BY'				=> $order_by,
		'SORT_ORDER'			=> $sort_order,
		'LOGIN'					=> $order_login,
		'SHIP_NAME'				=> $ship_name,
		'ORDERID'				=> $orderid,
		'OPS_LOCATION'			=> $ops_location
	);

	$page = $_GET['page'];
	if(!$page || $page == "") {
		$page = 1;
	}
	
	if(!isset($objects_per_page)){
		if ($current_area == 'C')
			$objects_per_page = $config[Appearance][products_per_page];
		else
			$objects_per_page = $config[Appearance][products_per_page_admin];
	}
    
	$offset = ($page-1)*$objects_per_page;

    //check if any filter is set
    $filterSet = false;
    foreach($search_criteria as $filter){
        if(!empty($filter)){
            $filterSet = true;
            break;
        }
    }

    //run limit based on between when no filter is selected(as optimization)
    if($filterSet===false){
        //construct between instead of limit query
        $maxOrderId = func_query_first_cell("select max(orderid) as maxorderid from xcart_orders;");
        $lowerLimit = ($maxOrderId-($page*$objects_per_page))+1;
        while($lowerLimit<1){
            $lowerLimit++;
        }
        $upperLimit = ($maxOrderId-($page*$objects_per_page))+$objects_per_page;
        while($upperLimit>$maxOrderId){
            $upperLimit--;
        }
        $betweenCondition = " AND orderid between $lowerLimit and $upperLimit ";
        $orders_sql = ordersearch::func_create_search_query($search_criteria, $betweenCondition);
    } else {
        $orders_sql = ordersearch::func_create_search_query($search_criteria, null);
        $orders_sql .= " LIMIT $offset, $objects_per_page";
    }


	$sqllog->debug("File name## orders_search.php ## SQL Info::>   $orders_sql");
	$orderresult = func_query($orders_sql, TRUE);

	foreach ($orderresult as $key=>$val){
		if($val['order_date'])
			$orderresult[$key]['order_date'] = date('jS m Y H:i:s',$val['order_date']);

		if($val['order_queueddate'])
			$orderresult[$key]['order_queueddate'] = date('jS m Y H:i:s',$val['order_queueddate']);

		$is_customizable_sql="select itemid from xcart_order_details where is_customizable = '0' and orderid=".$val['order_id'];
        $is_customizable=func_query($is_customizable_sql);
        if(!empty($is_customizable) && !empty($is_customizable[0]))
        {
        	$orderresult[$key]['is_customizable']=0;
        }
        else
    		$orderresult[$key]['is_customizable']=1;
	}
	$next_page_req = count($orderresult) < 30 ? false:true;

	$smarty->assign('current_page', $page);
	$smarty->assign('next_page_req', $next_page_req);
	$smarty->assign('orderresult', $orderresult);
	#
	#Navigation url
	#
	$URL = "orders_search.php?{$query_string}";
}else
	$URL = 'orders_search.php';

$smarty->assign('navigation_script', $URL);
$smarty->assign('status', $status);
$smarty->assign('mode', $mode);
$smarty->assign('query_string',$query_string);
$smarty->assign('main', 'orders_search');
func_display("admin/home.tpl", $smarty);

?>