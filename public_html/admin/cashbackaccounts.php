<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
require_once $xcart_dir."/modules/coupon/database/CouponAdapter.php";
include_once "$xcart_dir/modules/myntCash/MyntCashService.php";

function get_all_transactions($request) {
	if(empty($request['userlogin'])) {
		return false;
	}
	
	$start = $request['start'] == null ? 0 : $request['start'];
	$limit = $request['limit'] == null ? 30 : $request['limit'];
	
	$couponAdapter = CouponAdapter::getInstance();
	//$results = $couponAdapter->getCashbackTransactionsForUser($request['userlogin'], $start, $limit);
	$results = MyntCashService::getUserMyntCashTransacionsForMyMyntra($request['userlogin'],true);
	$count = sizeof($results);
	if($limit!=0){
		$results = array_slice($results,$start,$limit);
	}
	
	return array("results" => $results, "count" =>$count );
	
	
}

if ($REQUEST_METHOD == "POST") {
	if ($_POST['action'] == 'search') {
		
		$retarr = get_all_transactions($_POST);
	
		header('Content-type: text/x-json');
		print json_encode($retarr);
	}
	
} else {
	$searchlogin = $_GET['login'];
	$searchcoupon = $_GET['coupon'];
	
	$smarty->assign("searchlogin", $searchlogin);
	$smarty->assign("searchcoupon", $searchcoupon);
	
	$goodwillReason = func_query("select id, reason as name from cashback_goodwill_reason_code order by name asc", TRUE);
	$goodwillReason = json_encode($goodwillReason, true);
	
	$smarty->assign("goodwillReason",$goodwillReason);
	
	$smarty->assign("main","cashbackaccounts");
	func_display("admin/home.tpl", $smarty);
}
?>
