<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.order.php";

function func_get_late_order_status_counts(){
   $sql="(select t.s as status ,count(*) as count from 
   (select o.status as s,max(xo.estimated_processing_date) as m from xcart_orders o,xcart_order_details xo
    where xo.orderid=o.orderid and o.status in ('Q','WP','OH') and xo.estimated_processing_date !='' 
	group by o.orderid) as t where t.m<unix_timestamp(now()) group by t.s)";
    $info=func_query($sql);
    return $info;
}


function func_get_late_order_info(){
    global $sql_tbl;
/*    $sql = "select ol.name,o.orderid,o.order_name,o.status,from_unixtime(o.date) as order_date,
            from_unixtime(max(od.estimated_processing_date)) as estimated_time
            from xcart_orders o,xcart_order_details od, mk_operations_location ol
            where ol.id=o.operations_location and o.orderid=od.orderid
            and (o.status='Q' or o.status='WP' or o.status='OH')
            and unix_timestamp(now())>((select max(estimated_processing_date) from xcart_order_details
            where orderid=o.orderid and estimated_processing_date!='' and item_status in ('UA','A','S','H','OD')))
            group by o.orderid order by o.operations_location,o.status
			";*/
	  $sql="SELECT ol.name as location_name,o.orderid,o.queueddate,DATEDIFF(now(),from_unixtime(o.queueddate)) as queued_age,o.order_name,ps.name as style_name,od.item_status,o.status as order_status,from_unixtime(o.date) as order_date,
		  from_unixtime(proctime.max_processing_time) as estimated_time,DATEDIFF(now(),from_unixtime(proctime.max_processing_time)) as late_age
		  FROM xcart_orders o,xcart_order_details od, mk_operations_location ol,mk_product_style ps,
		  (select o.orderid as orderid,max(od.estimated_processing_date) as max_processing_time from xcart_orders o, xcart_order_details od
		  where o.orderid=od.orderid and od.estimated_processing_date!='' and item_status in ('UA','A','S','H','OD')
		  group by o.orderid) as proctime
		  WHERE ol.id=o.operations_location and o.orderid=od.orderid and ps.id=od.product_style and proctime.orderid=od.orderid and
		  (o.status='Q' or o.status='WP' or o.status='OH') and unix_timestamp(now())>(od.estimated_processing_date
	  )  order by o.operations_location,proctime.max_processing_time,o.orderid ";
    $info=func_query($sql);
    return $info;
}

function func_get_late_orders_by_product_type(){
    global $sql_tbl;
    $sql = "select product_type,pt.name as product_name,sum(count) as count,count(orderid) as ordercount 
            from mk_product_type pt,(
                select o.orderid,od.product_type,sum(od.amount) as count
                from xcart_orders o,xcart_order_details od
                where o.orderid=od.orderid and (o.status='Q' or o.status='WP' or o.status='OH')
                and od.amount>0 and unix_timestamp(now())>((select max(estimated_processing_date) from xcart_order_details
                where orderid=o.orderid and estimated_processing_date!='' and item_status in ('UA','A','S','H','OD')))
                group by o.orderid order by product_type) temp
            where pt.id=product_type group by product_type
            ";

    $info=func_query($sql);
    return $info;
}


function func_get_processing_times(){
    global $sql_tbl;
    $sql="select name,processing_time as time_in_hours from mk_product_type order by name;";
    $info=func_query($sql);
    return $info;
}

if($HTTP_GET_VARS['type'])
  $type=$HTTP_GET_VARS['type'];
else $type="lateorders";

if($type == "lateorders"){
   $late_order_info = func_get_late_order_info();
   $late_order_status_info = func_get_late_order_status_counts();
   $total_count=0;
   foreach($late_order_status_info as $status){
	   $total_count+=$status[count];
   }
   $smarty->assign("lateorders",$late_order_info);
   $smarty->assign("totallatecount",$total_count);
   $smarty->assign("lateorderstatus",$late_order_status_info);
}
else if($type=="times"){
   $processingtimes = func_get_processing_times();
   $smarty->assign("processingtimes",$processingtimes);
}
else if($type=="lateproducts"){
   $late_by_product_types = func_get_late_orders_by_product_type();
   $smarty->assign("lateproducts",$late_by_product_types);
}
$mode = "display";
$smarty->assign('mode',$mode);
$smarty->assign("main","late_order_report");
func_display("admin/home.tpl",$smarty);




?>
