<?php
require "./auth.php";
require $xcart_dir."/include/security.php";

if($mode=='search'){
	$sql = "SELECT ord.orderid, ord.status, ord.login, ord.phone, date_format(FROM_UNIXTIME(ord.date,'%Y-%m-%d'), '%d/%m/%Y') as date,
			date_format(FROM_UNIXTIME(ord.queueddate,'%Y-%m-%d'), '%d/%m/%Y') as queueddate, ord.total, ord.s_firstname as firstname, 
			ord.s_lastname as lastname, ot.ref_id, ot.outlet_name, ot.outlet_city, ot.area_mgr 
			FROM $sql_tbl[orders] ord inner join $sql_tbl[outlet_info] ot on ord.orderid = ot.orderid";
	if(!empty($refid)){
		$sql .=" WHERE UPPER(ot.ref_id) like '%".strtoupper($refid)."%'";
	}
	$orders = func_query($sql);
	$smarty->assign("orders",$orders);
	$smarty->assign("refid",$refid);
}
$smarty->assign("main","outlet_orders");
func_display("admin/home.tpl",$smarty);
?>
