<?php

include_once("auth.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/modules/amqpclients/producer/AMQPMessageProducer.php");
include_once("updateInventory.php");
require_once "WMSDBConnUtil.php";

$client = AMQPMessageProducer::getInstance();

echo "[" . date('j-m-Y H:i:s') . "(";

$imsConn = WMSDBConnUtil::getConnection('imsdb1.myntra.com:3306', TRUE, WMSDBConnUtil::IMS_READ);
$atpConn = WMSDBConnUtil::getConnection('atpdbmaster.myntra.com:3306', TRUE, WMSDBConnUtil::IMS_WRITE);
$wmsConn = WMSDBConnUtil::getConnection('wmsdb2.myntra.com:3306', TRUE, WMSDBConnUtil::IMS_READ);
$simConn = WMSDBConnUtil::getConnection("sellerdb2.myntra.com:3307", TRUE, WMSDBConnUtil::IMS_READ);

// $to = "engg_erp_wms@myntra.com";
$to = "aduri.sabarinath@myntra.com";
$fromHeader = "From: inventorysync@myntra.com";
$subject = "[WMSALERT] ON_HAND IMS and ATP Sync Report";

if (!$imsConn || !$atpConn || !$simConn) {
    die('Could not connect: ' . mysql_error());
}

if (mysql_select_db('myntra_ims', $imsConn) === false) {
    echo "WMS db unaccessible \n";
    exit;
}

if (mysql_select_db('myntra_seller', $simConn) === false) {
    echo "SIM db unaccessible \n";
    exit;
}


if (mysql_select_db('myntra_atp', $atpConn) === false) {
    echo "ATP db unaccessible \n";
    exit;
}

if (mysql_select_db('myntra_wms', $wmsConn) === false) {
    echo "WMS db unaccessible \n";
    exit;
}
$finalSkuQueries = array();
$finalAllSkus = array();
$finalAllDisabledSkusWithInv = array();
$finalSimAtpMismatch = array();
$myntraSellerIds = "1,19,21,25,29,30";
$orderEnabledWhIds = "28,36,81,89,93,118";
$invDicrepancyThreshold = 50;
$enabledDiscrepancyThreshold = 50; 

$authHeader = "Authorization: Basic YXBpYWRtaW46bTFudHJhUjBja2V0MTMhIw==";
$appPropBaseUrl = "http://erptools.myntra.com/myntra-tools-service/platform/tools/properties/search/?q=name.eq:";


$appPropSellerUrl = $appPropBaseUrl."wms.myntra.inventory.myntraSeller.ids";
$output = shell_exec("curl -v -H '" . $authHeader . "' -H 'Content-Type:application/xml' -XGET '" . $appPropSellerUrl . "' -d '" . $data . "'");
$xml = simplexml_load_string($output);
if (isset($xml->status->statusType) && $xml->status->statusType == "SUCCESS" && isset($xml->data->applicationProperty->value))
    $myntraSellerIds = $xml->data->applicationProperty->value;


$appPropWhUrl = $appPropBaseUrl."wms.myntra.inventory.warehouse.ids";
$output = shell_exec("curl -v -H '" . $authHeader . "' -H 'Content-Type:application/xml' -XGET '" . $appPropWhUrl . "' -d '" . $data . "'");
$xml = simplexml_load_string($output);
if (isset($xml->status->statusType) && $xml->status->statusType == "SUCCESS" && isset($xml->data->applicationProperty->value)){
    $orderEnabledWhIds = $xml->data->applicationProperty->value;
}

$appPropWhUrl = $appPropBaseUrl."atp.syncIMSATP.overall.threshold";
$output = shell_exec("curl -v -H '" . $authHeader . "' -H 'Content-Type:application/xml' -XGET '" . $appPropWhUrl . "' -d '" . $data . "'");
$xml = simplexml_load_string($output);
if (isset($xml->status->statusType) && $xml->status->statusType == "SUCCESS" && isset($xml->data->applicationProperty->value)){
    $orderEnabledWhIds = $xml->data->applicationProperty->value;
}

$appPropWhUrl = $appPropBaseUrl."atp.syncIMSATP.inv.dicrepancies.threshold";
$output = shell_exec("curl -v -H '" . $authHeader . "' -H 'Content-Type:application/xml' -XGET '" . $appPropWhUrl . "' -d '" . $data . "'");
$xml = simplexml_load_string($output);
if (isset($xml->status->statusType) && $xml->status->statusType == "SUCCESS" && isset($xml->data->applicationProperty->value)){
    $invDicrepancyThreshold = $xml->data->applicationProperty->value;
}

$appPropWhUrl = $appPropBaseUrl."atp.syncIMSATP.enabled.dicrepancies.threshold";
$output = shell_exec("curl -v -H '" . $authHeader . "' -H 'Content-Type:application/xml' -XGET '" . $appPropWhUrl . "' -d '" . $data . "'");
$xml = simplexml_load_string($output);
if (isset($xml->status->statusType) && $xml->status->statusType == "SUCCESS" && isset($xml->data->applicationProperty->value)){
    $enabledDiscrepancyThreshold = $xml->data->applicationProperty->value;
}

for ($loop = 0; $loop < 2; $loop++) {
    $offset = 0;
    $limit = 3000;
    $hasMore = true;
    $allSkus = array();
    $allDisabledSkusWithInv = array();
    $allSimAtpMismatch = array();
    $skuQueries = array();
    $avlOS = 0;
    $invOS = 0;
    $invavlOS = 0;
    $allSkuIds = array();

    $skusQuery = "select distinct sku_id from wh_inventory where last_modified_on >= (NOW() - INTERVAL 1 DAY) and supply_type='ON_HAND' and seller_id in (".$myntraSellerIds.") and warehouse_id in (".$orderEnabledWhIds.")";
    //$skusQuery = "select distinct sku_id from wh_inventory where sku_id=12355190";

    $sku_result = mysql_query($skusQuery, $imsConn);
    if ($sku_result) {
        while ($row = mysql_fetch_array($sku_result, MYSQL_ASSOC)) {
            $allSkuIds[] = $row['sku_id'];
        }
    }
    @mysql_free_result($sku_result);
    $skusQuery = "select distinct sku_id from seller_item_master where last_modified_on >= (NOW() - INTERVAL 1 DAY)";
    //$skusQuery = "select distinct sku_id from seller_item_master where sku_id=12355190";

    $sku_result = mysql_query($skusQuery, $simConn);
    if ($sku_result) {
        while ($row = mysql_fetch_array($sku_result, MYSQL_ASSOC)) {
            $allSkuIds[] = $row['sku_id'];
        }
    }
    @mysql_free_result($sku_result);
    $allSkuIds = array_unique($allSkuIds);

    while ($hasMore) {
        $skuIds = array();
        $skuIds = array_slice($allSkuIds, $offset, $limit);

        $warningSkus = array();
        $skuId2AtpInvMap = array();
        $skuId2AtpAvlMap = array();
        $skuId2ImsInvMap = array();
        $skuId2ImsAvlMap = array();
        $skuId2SellerMap = array();

        if (empty($skuIds)) {
            $hasMore = false;
            continue;
        }

        $skusQuery = "select sku_id, seller_id from seller_item_master where enabled=1 and sku_id in (".implode(",", $skuIds).")";
        $sku_result = mysql_query($skusQuery, $simConn);
        if ($sku_result) {
            while ($row = mysql_fetch_array($sku_result, MYSQL_ASSOC)) {
                $skuId2SellerMap[$row['sku_id']] = $row['seller_id'];
            }
        }
        @mysql_free_result($sku_result);

        $atpInvCountQuery = "select seller_id,sku_id, inventory_count as net_count,available_in_warehouses,enabled,blocked_order_count from inventory where store_id = 1 and seller_id in (" . $myntraSellerIds . ") and supply_type = 'ON_HAND' and sku_id in (" . implode(",", $skuIds) . ")";
        $atpInvResults = mysql_query($atpInvCountQuery, $atpConn);
        if ($atpInvResults) {
            while ($row = mysql_fetch_array($atpInvResults, MYSQL_ASSOC)) {
                $skuId2AtpInvMap[$row['seller_id']][$row['sku_id']] = $row['net_count'];
                $skuId2AtpAvlMap[$row['seller_id']][$row['sku_id']] = $row['available_in_warehouses'];
                if ($row['net_count'] > $row['blocked_order_count'] && $row['enabled'] == 0) {
                    $allDisabledSkusWithInv[] = $row['sku_id'];
                }
                if(!isset($skuId2SellerMap[$row['sku_id']]) && $row['enabled']==1){
                    $allSimAtpMismatch[] = $row['sku_id'] . "_0"; 
                }else if(isset($skuId2SellerMap[$row['sku_id']]) && (($row['enabled']=='1' && $skuId2SellerMap[$row['sku_id']] != $row['seller_id']) || ($row['enabled']=='0' && $skuId2SellerMap[$row['sku_id']] == $row['seller_id']))){
                    $allSimAtpMismatch[] = $row['sku_id'] . "_" . $skuId2SellerMap[$row['sku_id']];
                }
            }
        }
        @mysql_free_result($atpInvResults);

        $syncSkus = array();
        $imsInvCountQuery = "select sku_id, seller_id, sum(Greatest(inventory_count,0)) as net_count,group_concat(if(Greatest(inventory_count,0)-Greatest(blocked_order_count,0)>0,warehouse_id,null) separator ',') as avlWh  from wh_inventory where sku_id in (" . implode(",", $skuIds) . ") and warehouse_id in (".$orderEnabledWhIds.") and store_id =1 and seller_id in (" . $myntraSellerIds . ") and supply_type = 'ON_HAND' group by sku_id, seller_id";
        $imsInvResults = mysql_query($imsInvCountQuery, $imsConn);
        if ($imsInvResults) {
            while ($row = mysql_fetch_array($imsInvResults, MYSQL_ASSOC)) {
                $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']] = $row['net_count'];
                $skuId2ImsAvlMap[$row['seller_id']][$row['sku_id']] = $row['avlWh'];
                if (isset($skuId2AtpInvMap[$row['seller_id']][$row['sku_id']]) && ($skuId2AtpInvMap[$row['seller_id']][$row['sku_id']] != $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']] || array_diff(explode(",", $skuId2AtpAvlMap[$row['seller_id']][$row['sku_id']]), explode(",", $skuId2ImsAvlMap[$row['seller_id']][$row['sku_id']])))) {
                    $syncSkus[] = $row['sku_id'] . "_" . $row['seller_id'];
                    if ($skuId2AtpInvMap[$row['seller_id']][$row['sku_id']] != $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']] && array_diff(explode(",", $skuId2AtpAvlMap[$row['seller_id']][$row['sku_id']]), explode(",", $skuId2ImsAvlMap[$row['seller_id']][$row['sku_id']]))) {
                        $invavlOS++;
                        $diff = $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']] - $skuId2AtpInvMap[$row['seller_id']][$row['sku_id']];
                        $skuQueries[] = "update inventory set inventory_count=inventory_count + " . $diff . ",available_in_warehouses='" . $skuId2ImsAvlMap[$row['seller_id']][$row['sku_id']] . "', last_modified_on = now()  where supply_type = 'ON_HAND' and store_id = 1 and seller_id=" . $row['seller_id'] . " and sku_id=" . $row['sku_id'];
                    } else if ($skuId2AtpInvMap[$row['seller_id']][$row['sku_id']] != $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']]) {
                        $invOS++;
                        $diff = $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']] - $skuId2AtpInvMap[$row['seller_id']][$row['sku_id']];
                        $skuQueries[] = "update inventory set inventory_count=inventory_count + " . $diff . ", last_modified_on = now()  where supply_type = 'ON_HAND' and store_id = 1 and seller_id=" . $row['seller_id'] . " and sku_id=" . $row['sku_id'];
                    } else {
                        $avlOS++;
                        $skuQueries[] = "update inventory set available_in_warehouses='" . $skuId2ImsAvlMap[$row['seller_id']][$row['sku_id']] . "', last_modified_on = now()  where supply_type = 'ON_HAND' and store_id = 1 and seller_id=" . $row['seller_id'] . " and sku_id=" . $row['sku_id'];
                    }
                }
            }
        }
        @mysql_free_result($imsInvResults);


        $allSkus = array_merge($allSkus, $syncSkus);

        $offset += $limit;
    }
    if ($loop == 0) {
        $finalSkuQueries = $skuQueries;
        $finalAllSkus = $allSkus;
        $finalAllDisabledSkusWithInv = $allDisabledSkusWithInv;
        $finalSimAtpMismatch = array_unique($allSimAtpMismatch);
    } else {
        $finalAllSkus = array_intersect($finalAllSkus, $allSkus);
        $finalSkuQueries = array_intersect($finalSkuQueries, $skuQueries);
        $finalAllDisabledSkusWithInv = array_intersect($finalAllDisabledSkusWithInv, $allDisabledSkusWithInv);
        $finalSimAtpMismatch = array_intersect($finalSimAtpMismatch,array_unique($allSimAtpMismatch));
    }
}
mysql_close($atpConn);
mysql_close($imsConn);
mysql_close($simConn);

$finalSimAtpMismatchString = "\n";

if (count($finalSimAtpMismatch) > 0) {    

    foreach ($finalSimAtpMismatch as $simATPMismatch) {
        $arr = explode("_", $simATPMismatch);
        $finalSimAtpMismatchString = $finalSimAtpMismatchString . "SkuId : " . $arr[0] . ", sim_enabled_seller : " . ($arr[1]=="0" ? "null": $arr[1]) . "\n";
    }
}

$update = true;

if (count($finalAllSkus) > 0 || count($finalAllDisabledSkusWithInv) > 0 || count($finalSimAtpMismatch > 0)) {
    if(count($finalAllSkus) > $invDicrepancyThreshold || count($finalSimAtpMismatch) > $invDicrepancyThreshold){
        $to = $to . ",wmsoncall@myntra.com,noc@myntra.com,myntra.sms.alerts@gmail.com";
        $subject = "[CRITICAL] READONLY ".$subject;
        $update = false;
    }
    $report = "Out of sync:" . count($finalAllSkus) . "\n";
    $report = $report . "Inv and Aval out of sync: " . $invavlOS . "\n Inv out of sync:" . $invOS . "\n Avl out of sync:" . $avlOS . "\n";
    if(count($finalSimAtpMismatch) > 0){
        $report = $report . "SIM ATP Enabled Seller Mismatch Count : " . count($finalSimAtpMismatch) . "\n";
    }
    $report = $report."\n Disabled Skus with sellable Inventory : ".count($finalAllDisabledSkusWithInv)."\n Skus:".implode(",",array_unique($finalAllDisabledSkusWithInv));

    if(count($finalSimAtpMismatch) > 0){
        $report = $report . "\n SIM ATP Enabled Seller Mismatch : " . $finalSimAtpMismatchString . "\n";
    }

    mail($to, $subject, $report . "\n Inv Out of Sync Skus: " . implode("\n", $allSkus), $fromHeader);
    echo $report;
} else {
    echo "NA";
}
$errQueries = "";

if(count($finalAllSkus)>0 && $update){
    updateInventory(null, null, null, null, $finalAllSkus, $finalSkuQueries, null, null, null, $errQueries,false);
}

if (count($finalSimAtpMismatch) > 0 && $update) {
    foreach ($finalSimAtpMismatch as $simATPMismatch) {
        $arr = explode("_", $simATPMismatch);
        $simEntryEntry=array();
        $newEntry = array('skuId' => $arr[0]);
        $simEntryEntry['newEntry'] = $newEntry;
        $client->sendMessage('imsSyncSellerInventoryQueue', $simEntryEntry, "", 'json', true, 'ims');
    }
}

echo "\n)" . date('j-m-Y H:i:s') . "]";
?>
