<?php 

use enums\base\ActionType;
use landingpage\action\LPAddAction;
//use landingpage\action\LPDeleteAction;
use landingpage\action\LPUpdateAction;
use landingpage\action\LPViewAction;
use landingpage\exception\LPException;

$actionType = RequestVars::getVar("actionType");
$lpid = RequestVars::getVar("lpid");
$title =filter_var(RequestVars::getVar("lptitle"),FILTER_SANITIZE_STRING);
$stitle =filter_var(RequestVars::getVar("lpstitle"),FILTER_SANITIZE_STRING);
$name = filter_var(RequestVars::getVar("lpname"),FILTER_SANITIZE_STRING);
$description = filter_var(RequestVars::getVar("lpdesc"),FILTER_SANITIZE_STRING);
$image = RequestVars::getVar("lpimage");

switch($actionType) {
		case ActionType::Add:
			$action = new LPAddAction($title,$stitle,$name,$description,$image);
			$lpid = $action->run();
			break;
		case ActionType::Update:
			$action = new LPUpdateAction($lpid,$title,$stitle,$name,$description,$image);
			$action->run();
			break;
	
}

$action = new LPViewAction($lpid);
$action->run();