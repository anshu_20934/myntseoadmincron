<?php 

use enums\base\ActionType;
use landingpage\action\SectionAddAction;
use landingpage\action\SectionDeleteAction;
use landingpage\action\SectionUpdateAction;
use landingpage\action\SectionViewAction;
use landingpage\action\SectionMoveAction;
use landingpage\exception\SectionException;

$actionType = RequestVars::getVar("actionType");
$lpid = RequestVars::getVar("lpid");
$title = filter_var(RequestVars::getVar("title"),FILTER_SANITIZE_STRING);
$description = filter_var(RequestVars::getVar("description"),FILTER_SANITIZE_STRING);
$link = RequestVars::getVar("target_url");
$solrurl = RequestVars::getVar("url");
$noofproducts = RequestVars::getVar("no_of_products");
$sortorder = RequestVars::getVar("sort_order");
$sectionid = RequestVars::getVar("section_id");
$iscarousel = RequestVars::getVar("is_carousel");
$oldsortorder = $sortorder;
$sectionidtoswap = RequestVars::getVar("section_id_to_exchange");
$newsortorder = RequestVars::getVar("new_sort_order");

switch($actionType) {
		case ActionType::Add:
			$action = new SectionAddAction($title,$description,$link,$solrurl,$noofproducts,$iscarousel,$sortorder,$lpid);
			$action->run();
			break;
		case ActionType::Update:
			$action = new SectionUpdateAction($sectionid,$title,$description,$link,$solrurl,$noofproducts,$iscarousel,$sortorder,$lpid);
			$action->run();
			break;
		case ActionType::Delete:
			$action = new SectionDeleteAction($sectionid,$lpid);
			$action->run();
			break;
		case ActionType::MoveUp:
			$action = new SectionMoveAction($sectionid,$newsortorder,$sectionidtoswap,$oldsortorder,$lpid);
			$action->run();
			break;
		case ActionType::MoveDown:
			$action = new SectionMoveAction($sectionid,$newsortorder,$sectionidtoswap,$oldsortorder,$lpid);
			$action->run();
			break;
	
}

$action = new SectionViewAction($lpid);
$action->run();