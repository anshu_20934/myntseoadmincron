<?php 

/************
This file is entry point for Landinpage configuration
This will show all the exisiting landing pages 
Upon selecting one redirects to lpControllerView to fetch the correct details and displays on the screen
Else creates a new one, save and lpControllerView to show the same on the page.
*******/

require_once  "../auth.php";
require_once HostConfig::$documentRoot."/include/security.php";
require_once HostConfig::$documentRoot."/include/func/func.utilities.php";


require_once "lpconfig.php";
?>