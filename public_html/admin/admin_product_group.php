<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/
#
# $Id: categories.php,v 1.31 2006/02/14 14:45:23 max Exp $
#
require "./auth.php";
require $xcart_dir."/include/security.php";

/*
##function to get template detail selected for modification##
*/
#================

if(empty($mode)) $mode = "";
if(!empty($HTTP_POST_VARS['mode']))
	$mode = $HTTP_POST_VARS['mode'];
#================

//$styleid=$_GET['styleid'];
//$optionid=$_GET['optionid'];
$template="admin_product_group";

if ($REQUEST_METHOD == "POST" ||$REQUEST_METHOD == "GET" ){ 
	/*
	##updating template details ##
	*/
	if ($mode == "update_product_group"){
		if (is_array($posted_data)){
			func_header_location("admin_product_group.php");			
		}				
		
		
	}elseif($mode == "delete_product_group"){
		//deleting selected product groups
		if (is_array($posted_data)) {
			foreach ($posted_data as $productgroupid=>$v){
				if (empty($v["to_delete"]))
					continue;
				db_query ("DELETE FROM mk_product_group WHERE id=".$productgroupid);					
			}
			func_header_location("admin_product_group.php");			
		}		
		
	}elseif ($mode == "add_product_group"){
			 
		db_query("insert into mk_product_group (name,label,title,description,default_style_id,type,url,image,display_order,is_active) values ('".$_POST['name']."','".$_POST['label']."','".$_POST['title']."','".$_POST['description']."','".$_POST['default_style_id']."','".$_POST['type']."','".$_POST['url']."','".$_POST['image']."','".$_POST['display_order']."','".$_POST['is_active']."') ");
		
	 }
	$product_group=func_query("select * from mk_product_group ");
	//echo "<pre>";print_r($product_group);exit;
	
}

#==============

$smarty->assign("product_group", $product_group);
$smarty->assign("mode", $mode);
$smarty->assign ("errMsg", $err_msg);
#==============
$smarty->assign("main",$template);
# Assign the current location line
$smarty->assign("location", $location);
$smarty->assign("dialog_tools_data", $dialog_tools_data);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
