<?php
require "./auth.php";
require_once("../include/func/func.utilities.php");

$messages = array();

$table = 'mk_tabs';

$action = $_GET['action'];
$name = $_POST['name']; 
$display_order = $_POST['display_order'];
$enabled = $_POST['enabled']; 
$tags = $_POST['tags'];

if ($action == 'edit') {
    $id = $_GET['id'];
    if (!empty($name)) {
        func_array2update($table, array( 'name' => $name,  'display_order' => $display_order, 'enabled' => $enabled, 'tags' => $tags ), 'id="' . $id . '"');
    }
    $tab = func_select_first($table, array('id' => $id));
    $smarty->assign("tab", $tab);
    $smarty->assign("id", $id);
} else if ($action == 'add') {  
    if ( !empty($name)) {
        $id = func_array2insert($table, array( 'name' => $name,  'display_order' => $display_order, 'enabled' => $enabled , 'tags' => $tags));
    }
}

if (empty($action)) {
    $action = 'add';
}
$smarty->assign("action", $action);
$smarty->assign("dropdown", array('front_banner' => 'Front banner', 'right_banner' => 'Right banner', 'right_bottom_banner' => 'Right bottom banner', 'sports_jerseys' => 'Sports Jerseys' ));

$tabs = func_select_query($table, null, null, null, array('datecreated' => 'desc'));

$smarty->assign("messages", $messages);
$smarty->assign("tabs", $tabs);
@include $xcart_dir . "/modules/gold_display.php";
func_display("admin/tabs.tpl", $smarty);
?>
