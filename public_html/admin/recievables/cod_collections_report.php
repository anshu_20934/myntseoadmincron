<?php
	require "../auth.php";
	require_once $xcart_dir."/include/security.php";
	require_once $xcart_dir."/include/func/func.courier.php";

	$couriers1 = get_all_courier_partners();
	foreach( $couriers1 as &$value){
		$couriers[$value["code"]] = $value["display_name"]; 
	}
	$couriers["DEFAULT"] = "Select a carrier";
	$smarty->assign("couriers",$couriers);
	
	$fromDate = $_REQUEST["fromDate"];
	$endDate = $_REQUEST["toDate"];
	$courier = $_REQUEST["carrierid"];
	$smarty->assign("fromDate",$fromDate);
	$smarty->assign("toDate",$endDate);
	$smarty->assign("carrierid",$courier);
	if($endDate){
		$toDate = "01-".$endDate;
		$toDateParts = split("-",$endDate);
		if($toDateParts[0] == 12){
			$toDate = "01-01-".($toDateParts[1]+1);
		}	
	}else{
		$toDateParts = split("-",$fromDate);
		if($toDateParts[0] == 12){
			$toDate = "01-01-".($toDateParts[1]+1);
		}else{
			$toDate = "01-".($toDateParts[0]+01)."-".($toDateParts[1]);
		}
	}
	if($_REQUEST["mode"] && $_REQUEST["mode"] == 'download'){
		$query = "" ;
		if($fromDate){
			if($courier && $courier != "DEFAULT"){
				$query = "select orderid,subtotal,discount,coupon_discount,cash_redeemed,pg_discount,total,from_unixtime(date) as date,status,payment_method,from_unixtime(completeddate) as completeddate,cod_pay_status from xcart_orders where completeddate >= unix_timestamp(str_to_date('01-".$fromDate."','%d-%m-%Y')) and completeddate < unix_timestamp(str_to_date('".$toDate."','%d-%m-%Y')) and status='C' and cod_pay_status = 'paid' and payment_method = 'cod' and courier_service = '".$courier."'";	
			}else{
				$query = "select orderid,subtotal,discount,coupon_discount,cash_redeemed,pg_discount,total,from_unixtime(date) as date,status,payment_method,from_unixtime(completeddate) as completeddate,cod_pay_status from xcart_orders where completeddate >= unix_timestamp(str_to_date('01-".$fromDate."','%d-%m-%Y')) and completeddate < unix_timestamp(str_to_date('".$toDate."','%d-%m-%Y')) and status='C' and cod_pay_status = 'paid' and payment_method = 'cod'";
			}
			$data = func_query($query,true);
		}
		$filename =  $courier."_".date("dmy");
		if($courier == 'DEFAULT'){
			$filename = "collections_report_".date("dmy");
		}
		if($filename != "") {
			$objPHPExcel = new PHPExcel();
			$objPHPExcel = func_write_data_to_excel($objPHPExcel, $data, 0);
			header("Content-Disposition: attachment; filename=$filename.xls");
			ob_clean();
			flush();
			try {
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$objWriter->setPreCalculateFormulas(false);
				$objWriter->save('php://output');
			}catch(Exception $ex) {
				$weblog->info("Error Occured = ". $ex->getMessage());
			}
		}
	}else{
	$query = "" ;
		if($fromDate){
			if($courier && $courier != "DEFAULT"){
				$query = "select month(from_unixtime(completeddate)) mon,year(from_unixtime(completeddate)) yr,date_format(from_unixtime(completeddate),'%M-%y') as monthName, count(1) as orders ,sum(total) as amount from xcart_orders where completeddate >= unix_timestamp(str_to_date('01-".$fromDate."','%d-%m-%Y')) and completeddate < unix_timestamp(str_to_date('".$toDate."','%d-%m-%Y')) and status='C' and cod_pay_status = 'paid' and payment_method = 'cod' and courier_service = '".$courier."' group by yr,mon ";	
			}else{
				$query = "select month(from_unixtime(completeddate)) mon,year(from_unixtime(completeddate)) yr,date_format(from_unixtime(completeddate),'%M-%y') as monthName, count(1) as orders ,sum(total) as amount from xcart_orders where completeddate >= unix_timestamp(str_to_date('01-".$fromDate."','%d-%m-%Y')) and completeddate < unix_timestamp(str_to_date('".$toDate."','%d-%m-%Y')) and status='C' and cod_pay_status = 'paid' and payment_method = 'cod' group by yr,mon";
			}
			$data = func_query($query,true);	
		}
		$smarty->assign("data",$data);
		$smarty->assign("main","cod_collections_report");
		func_display("admin/home.tpl", $smarty);	
	}
?>