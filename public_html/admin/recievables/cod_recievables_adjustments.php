<?php
	require "../auth.php";
	require_once $xcart_dir."/include/security.php";
	require_once $xcart_dir."/include/func/func.courier.php";
	$couriers1 = get_all_courier_partners();
	foreach( $couriers1 as &$value){
		$couriers[$value["code"]] = $value["display_name"]; 
	}
	$couriers["DEFAULT"] = "Select a carrier";
	$smarty->assign("couriers",$couriers);
	$month = $_POST["month"];	
	$smarty->assign("month",$month);
	$courierName = $couriers[$_POST["carrierid"]];
	$description = $_POST["description"];
	$amount = $_POST["amount"];
	$action = $_POST["action"];
	if($month){
		if($_POST["carrierid"] != "DEFAULT" && $courierName && $description && $amount){
			$sqlquery = "insert into cod_recievables(creationTime,courierName,description,amount,type,lastUpdatedTime) values (str_to_date('01-".$month."','%d-%m-%Y'),'".$courierName."','".$description."',".$amount.",'adjustment',now())";
			func_query($sqlquery);
			$smarty->assign("message","Adjustment saved successfully!");
		}
		$sqlquery = "select id,courierName,description,amount from cod_recievables where type = 'adjustment' and date_format(creationTime,'%m-%Y') = '".$month."'";
		$data = func_query($sqlquery,true);
		$weblog->debug(empty($data));
		if(!empty($data)){
			$smarty->assign("adj_data",$data);	
		}
	}
	
	$smarty->assign("main","cod_recievables_adjustments");
	$smarty->assign("monthName",date("F Y",strtotime("01-".$month)));
	$smarty->assign("month",$month);
	func_display("admin/home.tpl", $smarty);
?>
