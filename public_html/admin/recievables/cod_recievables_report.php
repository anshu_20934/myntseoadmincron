<?php
require "../auth.php";
require_once $xcart_dir."/include/security.php";

$startDate = $_POST["startDate"];
$endDate = $_POST["endDate"];

if($startDate && $endDate){
	$fromDate = "01-".$startDate;
	$toDate = "01-".$endDate;
	$toDateParts = split("-",$endDate);

	if($toDateParts[0] == 12){
		$toDate = "01-01-".($toDateParts[1]+1);
	}else{
		$toDate = "01-".($toDateParts[0]+01)."-".$toDateParts[1];
	}
	$toMon = $toDateParts[0];
	$toYr = $toDateParts[1];
	
	$fromDateParts = split("-",$fromDate);
	$fromMon = $fromDateParts[1]+00;
	$fromYr = $fromDateParts[2];
	
	$smarty->assign("startDate","$startDate");
	$smarty->assign("endDate","$endDate");
	
	$query = "select sum(case when type='mon_start_value' then amount else 0 end) amount,sum(case when type = 'adjustment' then amount else 0 end) adjustments,min(case when ifnull(type,'')='mon_start_value' then lastUpdatedTime else adddate(now(),2) end) startupdate_date,date_format(creationTime,'%M %y') dateinformat,month(creationTime) mon,year(creationTime) yr from cod_recievables where date(creationTime) >= date(str_to_date('".$fromDate."','%d-%m-%Y')) and date(creationTime) < date(str_to_date('".$toDate."','%d-%m-%Y')) group by yr,mon";
	$startValues = func_query($query,true);
	global $weblog;
	while (($fromYr < $toYr) || ($fromYr == $toYr && $fromMon <= $toMon )) {
		$fromDateForThisPart = '01-'.$fromMon.'-'.$fromYr;
		if($fromMon == 12){
			$toDateForThisPart = '01-01-'.($fromYr+1);
		}else{
			$toDateForThisPart = '01-'.($fromMon+01).'-'.$fromYr;	
		}
		$query = "select count(1) as orders, sum(subtotal) as value,sum(case when ifnull(coupon_discount,0) != 0 then 1 else 0 end) noOfcpndiscounts,sum(ifnull(coupon_discount,0)) coupondiscount,sum(case when ifnull(discount,0) != 0 then 1 else 0 end) noOfdiscounts,sum(ifnull(discount,0)) discount,sum(case when ifnull(pg_discount,0) != 0 then 1 else 0 end) noOfpgdiscounts,sum(ifnull(pg_discount,0)) pgdiscount,sum(case when ifnull(cash_redeemed,0) != 0 then 1 else 0 end) noOfcashbacks,sum(ifnull(cash_redeemed,0)) cashback,month(from_unixtime(date)) mon,year(from_unixtime(date)) yr from xcart_orders where payment_method = 'cod' and date >= unix_timestamp(str_to_date('".$fromDateForThisPart."','%d-%m-%Y')) and date < unix_timestamp(str_to_date('".$toDateForThisPart."','%d-%m-%Y'))";

		$data1[] = func_query_first($query,true);	

		if($fromMon == 12){
			$fromMon = 1;
			$fromYr = $fromYr+1;
		}else{
			$fromMon = $fromMon+01;
		}	
	}

	$query = "select count(1) as collections,sum(ifnull(total,0)) amt,month(from_unixtime(completeddate)) mon,year(from_unixtime(completeddate)) yr from xcart_orders where completeddate >= unix_timestamp(str_to_date('".$fromDate."','%d-%m-%Y')) and completeddate < unix_timestamp(str_to_date('".$toDate."','%d-%m-%Y')) and status='C' and cod_pay_status = 'paid' and payment_method = 'cod' group by yr,mon";
	$data2 = func_query($query,true);
	$query = "SELECT MONTH(lastactivity) mon,YEAR(lastactivity)  yr,SUM(CASE WHEN returntype = 'RT' THEN 1 ELSE 0 END) AS rtoq, SUM(CASE WHEN returntype = 'RT' THEN total  ELSE 0 END) AS rtoq_value,SUM(CASE WHEN returntype = 'RTO' THEN 1 ELSE 0 END) AS rto,SUM(CASE WHEN returntype = 'RTO' THEN total ELSE 0 END) AS rto_value FROM  xcart_orders o, (SELECT rt.orderid,rt.returntype,rta.returnid,Max(rta.recorddate) AS lastactivity FROM   mk_old_returns_tracking rt,mk_old_returns_tracking_activity rta WHERE  rt.id = rta.returnid AND rt.currstate IN ( 'RTC', 'END', 'RTOC' )  GROUP  BY rta.returnid,rt.returntype,rt.orderid HAVING lastactivity >= str_to_date('".$fromDate."','%d-%m-%Y') AND lastactivity < str_to_date('".$toDate."','%d-%m-%Y')) taba WHERE  o.orderid = taba.orderid and o.payment_method = 'cod' GROUP  BY yr,mon;";
	$data3 = func_query($query,true);
	
	$data=array();
	
	foreach( $data1 as &$value1){
		if(!$data[$value1["mon"]."-".$value1["yr"]]){
			$data[$value1["mon"]."-".$value1["yr"]] = array("mon"=>$value1["mon"], "yr"=>$value1["yr"],"display_name"=>"","starting_value"=>"","tot_orders"=>"","tot_amount"=>"","noof_coupon_disc"=>"","tot_coupon_disc"=>"","collections"=>"","collected_amount"=>"","discounts"=>"","no_of_discounts"=>"","rtoq"=>"","rtoq_value"=>"","rto"=>"","rto_value"=>"","no_of_pg_disc"=>"","tot_pg_disc"=>"","cashback"=>"","no_of_cashbacks"=>"");
			$data[$value1["mon"]."-".$value1["yr"]]["display_name"] = date("F Y",strtotime("01-".$value1["mon"]."-".$value1["yr"]));
		}
		$data[$value1["mon"]."-".$value1["yr"]]["tot_orders"] = $value1["orders"];
		$data[$value1["mon"]."-".$value1["yr"]]["tot_amount"] = $value1["value"];
		$data[$value1["mon"]."-".$value1["yr"]]["noof_coupon_disc"] = $value1["noOfcpndiscounts"];
		$data[$value1["mon"]."-".$value1["yr"]]["tot_coupon_disc"] = $value1["coupondiscount"]; 
		$data[$value1["mon"]."-".$value1["yr"]]["discounts"] = $value1["discount"];
		$data[$value1["mon"]."-".$value1["yr"]]["no_of_discounts"] = $value1["noOfdiscounts"];
		$data[$value1["mon"]."-".$value1["yr"]]["no_of_pg_disc"] = $value1["noOfpgdiscounts"];
		$data[$value1["mon"]."-".$value1["yr"]]["tot_pg_disc"] = $value1["pgdiscount"]; 
		$data[$value1["mon"]."-".$value1["yr"]]["cashback"] = $value1["cashback"];
		$data[$value1["mon"]."-".$value1["yr"]]["no_of_cashbacks"] = $value1["noOfcashbacks"];
	}
	
	foreach( $startValues as &$value){
		if(!$data[$value["mon"]."-".$value["yr"]]){
			$data[$value["mon"]."-".$value["yr"]] = array("mon"=>$value["mon"], "yr"=>$value["yr"], "display_name"=>"","starting_value"=>"","tot_orders"=>"","tot_amount"=>"","noof_coupon_disc"=>"","tot_coupon_disc"=>"","collections"=>"","collected_amount"=>"","discounts"=>"","no_of_discounts"=>"","rtoq"=>"","rtoq_value"=>"","rto"=>"","rto_value"=>"");
		}			
		$data[$value["mon"]."-".$value["yr"]]["display_name"] = $value["dateinformat"];
		$data[$value["mon"]."-".$value["yr"]]["starting_value"] = $value["amount"];
		$data[$value["mon"]."-".$value["yr"]]["adjustments"] = $value["adjustments"];
		$data[$value["mon"]."-".$value["yr"]]["startupdate_date"] = $value["startupdate_date"];
	}
	
	foreach( $data2 as &$value3){
		if(!$data[$value3["mon"]."-".$value3["yr"]]){
			$data[$value3["mon"]."-".$value3["yr"]] = array("mon"=>$value3["mon"], "yr"=>$value3["yr"],"display_name"=>"","starting_value"=>"","tot_orders"=>"","tot_amount"=>"","noof_coupon_disc"=>"","tot_coupon_disc"=>"","collections"=>"","collected_amount"=>"","discounts"=>"","no_of_discounts"=>"","rtoq"=>"","rtoq_value"=>"","rto"=>"","rto_value"=>"","no_of_pg_disc"=>"","tot_pg_disc"=>"","cashback"=>"","no_of_cashbacks"=>"");
			$data[$value3["mon"]."-".$value3["yr"]]["display_name"] = date("F Y",strtotime("01-".$value3["mon"]."-".$value3["yr"]));
		}			
		$data[$value3["mon"]."-".$value3["yr"]]["collections"] = $value3["collections"];
		$data[$value3["mon"]."-".$value3["yr"]]["collected_amount"] = $value3["amt"];
	}
	foreach( $data3 as &$value4){
		if(!$data[$value4["mon"]."-".$value4["yr"]]){
			$data[$value4["mon"]."-".$value4["yr"]] = array("mon"=>$value4["mon"], "yr"=>$value4["yr"],"display_name"=>"","starting_value"=>"","tot_orders"=>"","tot_amount"=>"","noof_coupon_disc"=>"","tot_coupon_disc"=>"","collections"=>"","collected_amount"=>"","discounts"=>"","no_of_discounts"=>"","rtoq"=>"","rtoq_value"=>"","rto"=>"","rto_value"=>"","no_of_pg_disc"=>"","tot_pg_disc"=>"","cashback"=>"","no_of_cashbacks"=>"");
			$data[$value4["mon"]."-".$value4["yr"]]["display_name"] = date("M Y",strtotime("01-".$value3["mon"]."-".$value3["yr"]));
		}			
		$data[$value4["mon"]."-".$value4["yr"]]["rtoq"] = $value4["rtoq"];
		$data[$value4["mon"]."-".$value4["yr"]]["rtoq_value"] = $value4["rtoq_value"];
		$data[$value4["mon"]."-".$value4["yr"]]["rto"] = $value4["rto"];
		$data[$value4["mon"]."-".$value4["yr"]]["rto_value"] = $value4["rto_value"];
	}
	unset($data["-"]);
	foreach( $data as &$value5){
		$value5["closing_val"] = $value5["starting_value"]+$value5["tot_amount"]-$value5["discounts"] - $value5["coupondiscount"] - $value5["rtoq_value"]- $value5["rto_value"] - $value5["collected_amount"]-$value5["pg_discount"] - $value5["cashback"]+$value5["adjustments"];
		$value5["closing_no"] = $value5["tot_orders"] - $value5["collections"] - $value5["rto"] - $value5["rtoq"];

		if($_POST["action"] == "recalculate"){
			if($value5["mon"] == 12){
				$sqlquery = "select id from cod_recievables where creationTime = str_to_date('01-01-".($value5["yr"]+1)."','%d-%m-%Y') and type = 'mon_start_value'";
				$id = func_query_first($sqlquery,true);
				$id = $id['id'];
				if($id > 0){
					$sqlquery = "update cod_recievables set amount = ".$value5["closing_val"]." , lastUpdatedTime = now() where  id = $id";					
				}else{
					$sqlquery = "insert into cod_recievables(creationTime,amount,type,lastUpdatedTime) values ( str_to_date('01-01-".($value5["yr"]+1)."','%d-%m-%Y'),".$value5["closing_val"].",'mon_start_value',now()) on duplicate key update amount = values(amount),lastUpdatedTime = values(lastUpdatedTime)";	
				}
				$data["01-".($value5["yr"]+1)]["starting_value"] = $value5["closing_val"];
			}else{
				$sqlquery = "select id from cod_recievables where creationTime = str_to_date('01-".($value5["mon"]+01)."-".$value5["yr"]."','%d-%m-%Y') and type = 'mon_start_value'";
				$id = func_query_first($sqlquery,true);
				$id = $id['id'];
				if($id > 0){
					$sqlquery = "update cod_recievables set amount = ".$value5["closing_val"]." , lastUpdatedTime = now() where  id = $id";					
				}else{
					$sqlquery = "insert into cod_recievables(creationTime,amount,type,lastUpdatedTime) values ( str_to_date('01-".($value5["mon"]+01)."-".$value5["yr"]."','%d-%m-%Y'),".$value5["closing_val"].",'mon_start_value',now()) on duplicate key update amount = values(amount),lastUpdatedTime = values(lastUpdatedTime)";	
				}
				$data[($value5["mon"]+01)."-".$value5["yr"]]["starting_value"] = $value5["closing_val"];	
			}
			func_query($sqlquery);		
		}
	}
	if($toDateParts[0] == 12){
		unset($data["01-".($toDateParts[1]+1)]);
	}else{
		unset($data[($toDateParts[0] + 01)."-".$toDateParts[1]]);
	}
	$smarty->assign("data",$data);	
}
	$smarty->assign("main","cod_recievables_report");
	func_display("admin/home.tpl", $smarty);	
?>
