<?php
define("REPORTS_CONNECTION",true);
include_once("../auth.php");
include_once("$xcart_dir/include/func/func.reports.php");

function runReports()
{
	global $sql_ro;
	$sql_host = $sql_ro['host'];
	$sql_user = $sql_ro['user'];
	$sql_password = $sql_ro['password'];
	$sql_db = $sql_ro['db'];
	global $smarty, $reportlog;
	$reportlog->info("REPORT LOG >>> Fetching al reports ");
	$time= time();
	$scheduled_reports=func_get_scheduled_reports();
	$reportlog->info("REPORT LOG >>> Total number of reports to be scheduled : " . count($scheduled_reports) );
	$count_my = 1;
	foreach ($scheduled_reports as $report)
	{
		$reportlog->info("REPORT LOG >>> $$$$$$$$$$$$$$$$$$$$$$$$$$$$ START REPORT -- $count_my $$$$$$$$$$$$$$$$$$$$$$$$ ");
		$count_my++;
		$rid=$report['reportid'];
		//check if the reportid has an entry in mk_reports table before trying to run the report
		$query = "select count(*) from mk_reports where id=$rid";
		$res = db_query($query, true);
		$row = db_fetch_row($res);
		if( $row[0] == 0)
		{
			$reportlog->info("REPORT LOG >>> Invalid report id : $rid got from schedule table...Skip execution..");
			$reportlog->info("REPORT LOG >>> $$$$$$$$$$$$$$$$$$$$$$$$$$$$ END REPORT $$$$$$$$$$$$$$$$$$$$$$$$ ");
			continue;
		}
		// end checking validity of the report
		$scheduleid=$report['id'];
		$scheduletype=$report['type'];
		$optionstr=$report['optionstr'];
		$nextrun=$report['nextrun'];
		$dateid=$report['dateid'];
		$month=$report['month'];
		$day=$report['day'];
		$year=$report['year'];
		$mailto=$report['mailto'];
		$reportlog->info("REPORT LOG >>> Running report ID :: $rid, scheduleid :: $scheduleid, scheduletype :: $scheduletype, nextrun :: $nextrun");
		//write code to get value for from_date filter using scheduletype and optionstr
		//$scheduletype=strtolower($scheduletype);
		$from_date=9999;
		//write code to get value for to_date filter using scheduletype and optionstr
		$to_date=1111111111111111111;
		if(!empty($dateid))
		{
			$from_date=mktime(0,0,0, date("m")-$month , date("d")-$day, date("Y")-$year);
			$to_date=mktime(0,0,0 ,date("m"), date("d"), date("Y"));
		}

		$lastrun=$nextrun;
		if($scheduletype=="DAL")
		{
			// $from_date=mktime(0,0,0, date("m")  , date("d")-1, date("Y"));
			// $to_date=mktime(0,0,0 ,date("m")  , date("d"), date("Y"));
			$myarray =explode(":",$optionstr);
			$tomorrow  = mktime($myarray[0],$myarray[1], $myarray[2], date("m")  , date("d")+1, date("Y"));
			$nextrun=$tomorrow;
		}
		elseif ($scheduletype=="WEK")
		{
			// $from_date=mktime(0,0,0, date("m")  , date("d")-7, date("Y"));
			// $to_date=mktime(0,0,0 ,date("m")  , date("d"), date("Y"));
			$initarray=explode("|",$optionstr);
			$dayofweek=$initarray[0];
			$optstr=$initarray[1];
			$myarray =explode(":",$optstr);
			$optstring=$dayofweek."|".$optstr;
			$day=date('w');
			$daydif;
			if($day<$dayofweek)
			{
				$daydif=$dayofweek-$day;
			}
			else
			{
				$daydif=7-$day+$dayofweek;
			}
			//  $today=mktime($myarray[0], $myarray[1], $myarray[2], date("m")  , date("d"), date("Y"));
			$nextrun=mktime($myarray[0],$myarray[1], $myarray[2], date("m")  , date("d")+$daydif, date("Y"));
		}
		elseif ($scheduletype=="MON")
		{
			$initarray=explode("|",$optionstr);
			$dayofmonth=$initarray[0];
			$optstr=$initarray[1];
			// $from_date=mktime(0,0,0, date("m")-1  , date("d"), date("Y"));
			//$to_date=mktime(0,0,0 ,date("m")  , date("d"), date("Y"));
			$myarray =explode(":",$optstr);
			//$optstring=$dayofmonth."|".$optstr;
			$daymonth=date('j');
			//$today=mktime($myarray[0], $myarray[1], $myarray[2], date("m")  , date("d"), date("Y"));
			$nextrun  = mktime($myarray[0],$myarray[1], $myarray[2], date("n")+1  ,$dayofmonth, date("Y"));
		}

		//Get all filters for this report
		$filters=func_get_filters($rid);
		$conditions = array();
		$dateconditions=array();
		$exportfilters=array();
		$i=0;
		$dat=0;
		foreach ($filters as $filter)
		{
			$filtertype=$filter['type'];
			$filtername=$filter['name'];
			$default_value=$filter['default_value'];
			$cond=$default_value;
			$exportcond1=$filtername;
			$exportcond2=$default_value;
			if($filtertype=="DAT")
			{
				$filtername=str_replace(" ","_",$filtername);
				$filtername=strtolower($filtername);
				if($filtername=="from_date")
				{
					$cond=$from_date;
				}
				elseif ($filtername=="to_date")
				{
					$cond=$to_date;
				}
				$date=date("d",$cond)."-".date("M",$cond).",".date("Y",$cond);
				$exportcond2=$date;
				$dateconditions[$dat]=$cond;
				$dat++;
			}
			$conditions[$i] = $cond;
			$exportfilters[$i]=$exportcond1.": ".$exportcond2;
			$i++;
		}

		$reportlog->info("REPORT LOG  >>> Fetching report details for report id : $rid" );
		$reportdetails=func_getreportquery($rid);
		$report_type = $reportdetails[5];
		$reportname=$reportdetails[0];
		$reportquery=$reportdetails[1];
		if( $report_type === "Q")
		{
			$sumquery=$reportdetails[2];
				
			$reportlog->info("REPORT LOG >>> report type : Query");
			$reportlog->info("REPORT LOG >>> report name : $reportname ");
			$reportlog->info("REPORT LOG >>> report query : $reportquery ");
			$reportlog->info("REPORT LOG >>> sum query : $sumquery ");
			//$mailto=$reportdetails[3];

			$size = count($conditions);
			$datsize=count($dateconditions);
			//$smarty->assign("size",count($filternames));
			//$smarty->assign("cond1",$conditions[0]);
			//$smarty->assign("cond2",$conditions[1]);
			//$smarty->assign("cond3",$conditions[2]);
			//func_display("view_report.tpl",$smarty);
			$ckhflag=false;
			$findme   = array("FRMDAT","TODAT");
			foreach ($findme as $chk)
			{
				$pos = strpos($reportquery, $chk);
				if ($pos === false)
				{
				}
				else
				{
					$chkflag=true;
				}
			}
			if($chkflag&&$datsize>0)
			{
				for ($j = 0; $j < $datsize; $j++)
				{
					$reportquery = str_replace($findme[$j],$dateconditions[$j],$reportquery);
					$sumquery = str_replace($findme[$j],$dateconditions[$j],$sumquery);
				}
			}
			for ($j = 0; $j < $size; $j++)
			{
				$reportquery = str_replace_count("?",$conditions[$j],$reportquery,1);
			}
			for ($j = 0; $j < $size; $j++)
			{
				$sumquery = str_replace_count("?",$conditions[$j],$sumquery,1);
			}
			$finalquery=$reportquery;

			if(!empty($sumquery))
			{
				$finalquery=$finalquery." union ".$sumquery;
			}
			$reportlog->info("REPORT LOG >>> Executing Query :: $finalquery ");
			$result = db_query($finalquery, true);
			$reportlog->info("REPORT LOG >>> Query execution success ");
			$reportresult = array();
			$coloums = array();
			if ($result)
			{
				$k=0;
				while ($row=db_fetch_array($result))
				{
					$reportresult[$k] = $row;
					$k++;
				}
			}
		}
		elseif( $report_type === "SP" )
		{
			$reportlog->info("REPORT LOG >>> report type : Stored Procedure");
			$finalquery = mysql_real_escape_string(trim($reportquery));

			$reportlog->info("REPORT LOG >>> Executing Stored Procedure :: $finalquery ");
			$mysqli = new mysqli("$sql_host","$sql_user","$sql_password","$sql_db");
			/* check connection */
			if (mysqli_connect_errno())
			{
				$reportlog->info("Connect failed: ".mysqli_connect_error());
				continue;
			}

			if($mysqli->multi_query($finalquery))
			{
				do
				{
					/* store first result set */
					if ($result = $mysqli->store_result())
					{
						while ($row = $result->fetch_assoc())
						{
							$reportresult[] = $row;
						}
						$result->close();
					}
				} while ($mysqli->next_result());
			}
			else
			{
				$reportlog->info("Fetching result failed - ".$mysqli->error);
				continue;
			}
			$reportlog->info("REPORT LOG >>> Query Stored Procedure success ");
		}
		foreach($reportresult[0] as $key1=>$item1)
		{
			$coloums[]=$key1;
		}

		$reportlog->info("REPORT LOG >>> Now updating lastrun and nextrun for this report ");
		$query="update `mk_reports_schedule` set lastrun='$lastrun' ,nextrun='$nextrun' where id='$scheduleid' ";
		$reportlog->info("REPORT LOG >>> Executing Query :: $query");
		db_query($query);
		$reportlog->info("REPORT LOG >>> Query execution success ");
		$smarty->assign("reportid",$rid);
		$smarty->assign("rname", $reportname);
		$smarty->assign("reportname", $reportname);
		$smarty->assign("coloums", $coloums);
		$smarty->assign("reportresult", $reportresult);
		$smarty->assign("filters", $exportfilters);
			
		$body = $smarty->fetch('excel_reports.tpl');
			
		$subject=$reportname." Report";

		$headers = "Content-Type: text/html; charset=ISO-8859-1 " . "\n";
			
		$reportlog->info("REPORT LOG >>> Now Sending Mail to :: ".$mailto);
		$mail_detail = array(
                            "to"=>$mailto,
                            "subject"=>$subject,
                            "content"=>$body,
                            "from_name"=>'Myntra Admin',
                            "from_email"=>'admin@myntra.com',
                            "headers"=>$headers,
		);
		send_mail_on_domain_check($mail_detail);
		$reportlog->info("REPORT LOG >>> ... Mail sent");

		$reportlog->info("REPORT LOG >>> $$$$$$$$$$$$$$$$$$$$$$$$$$$$ END REPORT $$$$$$$$$$$$$$$$$$$$$$$$ ");
		/*if(empty($mailto))
		 {
			$mailmsg="e-mail could not be sent as e-mail address not defined for this report ";
			}
			else
			{
			$mailmsg="Report Mailed Succesfully!!!!! ";
			}
			$smarty->assign("mailmsg",$mailmsg);
			$smarty->assign("mailsent","Y");*/
		//func_display("view_report.tpl",$smarty);
	}
	return $time;
}
