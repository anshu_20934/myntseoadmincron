<?php
require "./auth.php";
include_once("$xcart_dir/include/func/func.order.php");

$mode = $_GET['mode'];
$startDate = $_GET['startDate'];
$endDate = $_GET['endDate'];
$paymentmethod = $_GET['paymentmethod'];
$shippingmethod = $_GET['shippingmethod'];

$orderids = $_GET['orderids'];

if(!empty($orderids) && $mode=='markdelivered') {
	func_change_order_status($orderids, 'DL', $login, " From Bulk change shipped to delivered interface.", "", false);
	
	// For online orders, we need to mark them as Completed immediately as well.
	$onlineQuery = "select orderid from xcart_orders where payment_method='on' and orderid in (" . implode(",", $orderids) . ")";	
	$getOnlineOrders = func_query($onlineQuery,true);
	
	$onlineOrders = array();
	foreach($getOnlineOrders as $key=>$value) {
		$onlineOrders[] = $value['orderid'];
	}
	
	if(!empty($onlineOrders)) {
		func_change_order_status($onlineOrders, 'C', $login, " From Bulk change shipped to delivered interface.", "", false);
	}
	
	// Change mode to search to prevent repeat marking of orders as DL.
	$mode='search';
}

$searchFilters ="&mode=" . $mode . "&startDate=" . $startDate . 
				"&endDate=" . $endDate . "&paymentmethod=" . $paymentmethod . "&shippingmethod=" . $shippingmethod;

$smarty->assign("searchFilters", $searchFilters);


$parametersList = "orderid, login, total, from_unixtime(date) as placeddate, from_unixtime(shippeddate) as shippeddate, firstname, s_address, s_city, s_state, s_country, s_zipcode, 
		  		   mobile, cash_coupon_code, cash_redeemed, coupon, coupon_discount, subtotal as actualamount, 
		  		   cash_redeemed+coupon_discount as totalcoupondiscount, discount as eossdiscount,
		  		   payment_method, courier_service, tracking, cod ";

$queryCondition = " from xcart_orders where status='SH' and delivereddate is null and shippeddate is not null";

if(!empty($startDate)) {
	$queryCondition.= " and date>=unix_timestamp('$startDate') ";
	$smarty->assign("startDate", $startDate);
}

if(!empty($endDate)) {
	$queryCondition.= " and date<=unix_timestamp('$endDate') ";
	$smarty->assign("endDate", $endDate);
}

$queryConditionMore = '';
if(!empty($paymentmethod)) {
	$queryConditionMore.= " and payment_method='$paymentmethod'";
	$smarty->assign("paymentmethod", $paymentmethod);
}

if(!empty($shippingmethod)) {
	$queryConditionMore.= " and courier_service='$shippingmethod'";
	$smarty->assign("shippingmethod", $shippingmethod);
}

$countResults = func_query_first_cell("select count(*) $parametersList $queryCondition $queryConditionMore");

$query = "select $parametersList $queryCondition $queryConditionMore";

$sortordermode = $_POST['sortorder'];
if(!empty($sortordermode)) {
	$query.= " order by orderid $sortordermode ";
	$smarty->assign("sortordermode", $sortordermode);
}

$sortvendormode = $_POST['sortvendor'];
if(!empty($sortvendormode)) {
	$query.= " order by courier_service $sortvendormode ";
	$smarty->assign("sortvendormode", $sortvendormode);
}


$sortshippeddate = $_POST['sortshippeddate'];
if(!empty($sortshippeddate)) {
	$query.= " order by shippeddate $sortshippeddate ";
	$smarty->assign("sortshippedmode", $sortshippeddate);
}


$limit=$_GET['limit'];
if(empty($limit)) {
	$limit=10;
}

$page=$_GET['page'];

if(empty($page)) {
	$page=1;
}

$totalPages = ceil($countResults/$limit);

$smarty->assign("pagenum", $page);
$smarty->assign("totalPages", $totalPages);

$query.= " limit " . ($page-1)*$limit . "," . $limit;

$results = func_query($query);

$allOrderids = func_query("select orderid $queryCondition $queryConditionMore");
$orderList=array();
$pagenumorders = 1;
$index = 1;
foreach ($allOrderids as $singleresult) {
	$orderList[$pagenumorders][] = $singleresult['orderid'];
	if($index%$limit==0) {
		$index=1;
		$pagenumorders++;
	} else {
		$index++;
	}
}

foreach($orderList as $key=>$value) {
	$orderList[$key] = implode(", ", $value);
}

$smarty->assign("orderList", $orderList);

if(!empty($results)) {
	$smarty->assign("shippedorders", $results);
}

$uniquePaymentMethods = func_query("select payment_method $queryCondition group by payment_method");
$smarty->assign("uniquepaymentmethods", $uniquePaymentMethods);

$uniqueShippingMethods = func_query("select courier_service $queryCondition group by courier_service");
$smarty->assign("uniqueshippingmethods", $uniqueShippingMethods);

$template ="mark_shipped_orders_as_delivered";
$smarty->assign("main",$template);

func_display("admin/home.tpl",$smarty);
?>