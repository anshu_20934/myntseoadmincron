<?php
require "./auth.php";
include_once("$xcart_dir/include/class/oms/ItemManager.php");
require_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";

include_once "$xcart_dir/modules/myntCash/MyntCashTransaction.php";
include_once "$xcart_dir/modules/myntCash/MyntCashService.php";


x_load('db','order', 'mail');

$action = $_POST['action'];
if($action == 'override'){

	$orderid = $_GET['orderid'];
	$order = func_select_order($orderid);
	$userlogin = $order['login'];
	$userinfo = func_userinfo($userlogin, 'C');

	$itemid = $_POST['itemid'];
	$newPrice = $_POST['newPrice'];
	$comment = $_POST['overrideComment'];
	$item = func_query_first("SELECT price, amount, difference_refund, product_style from xcart_order_details where itemid = $itemid");
	$originalPrice = $item['price'];
	$amount = $item['amount'];
	$originalDiffenenceRefund = $item['difference_refund'];
	
	if($originalDiffenenceRefund == 0.00){
		$style_properties_sql = "SELECT product_display_name FROM mk_style_properties WHERE style_id = ".$item['product_style']."";
		$style_properties = func_query_first($style_properties_sql);
		$product_display_name = $style_properties['product_display_name'];
		
		// get the group_id for split orders.
		$gsql = "SELECT group_id FROM xcart_orders WHERE orderid = $orderid";
		$groupid = func_query_first_cell($gsql);
		
		$refund_amount = (($originalPrice - $newPrice)*$amount);
		$couponAdapter = CouponAdapter::getInstance();
		//$couponCode = $couponAdapter->creditCashback($userlogin, $refund_amount, "Cashback due to difference in price of $product_display_name for orderid. - $groupid ");
		
		// refund into myntCash
		$trs = new MyntCashTransaction($userlogin, MyntCashItemTypes::ORDER, $groupid,  MyntCashBusinessProcesses::MRP_MISMATCH , 0,$refund_amount,0 ,  "Cashback due to difference in price of $product_display_name for orderid. - $groupid ");
		MyntCashService::creditToUserMyntCashAccount($trs);
		
		
		$order['difference_refund'] = $refund_amount;
		$difference_refund['difference_refund'] = $refund_amount;
		$r = func_array2updateWithTypeCheck('xcart_order_details', $difference_refund, "itemid = ".$itemid);
		func_addComment_order($groupid, $login, 'Note', 'Item Price Overridden', $comment."   Refund amount Rs. $refund_amount");
		
		// notify the customer
		$mobile = $userinfo['mobile'];
		$msg = "You have been refunded Rs. $refund_amount into your Myntra cashback account for your order no. $groupid due to a price mismatch.";
		$msg .= " Thank you for buying from www.myntra.com";
		func_send_sms($mobile, $msg);
		
		$template = "price_override";
	    //build content and subject of the mail
	    $subjectArgs = array("ORDER_ID"=>$groupid);
	    
	    $productsInOrder = func_create_array_products_of_order($orderid, array(), true, explode(",", $itemid));
	    $sql = "SELECT sum(price*amount) as subtotal, sum(discount) as discount, sum(cart_discount_split_on_ratio) as cart_discount, sum(coupon_discount_product) as coupon_discount, sum(cash_redeemed) as cash_redeemed, sum(item_loyalty_points_used) as item_loyalty_points_used, sum(pg_discount) as pg_discount, sum(total_amount+taxamount-coupon_discount_product-cash_redeemed-pg_discount) as total,sum(cashback) as cashback, sum(difference_refund) as difference_refund, sum(taxamount) as tax FROM xcart_order_details WHERE itemid in (".implode(",", array($itemid)).")";
	    $order_amount_details = func_query_first($sql);
	    $order_amount_details['payment_method'] = $order['payment_method'];	
	    $order_amount_details['loyalty_credit'] = $order_amount_details['item_loyalty_points_used']*$order['loyalty_points_conversion_factor'];	
	    if($order['status']=='F'&&$order['payment_surcharge'] > 0) {
	    	$order_amount_details['payment_surcharge'] = $order['payment_surcharge'];
	    }
	    $itemdetails = create_product_detail_table_new($productsInOrder, $order_amount_details, 'email');
	    
	    $http_location = HostConfig::$httpHost;
	    $mycouponsurl = $http_location.'/mymyntra.php?view=mymyntcredits';
	    $myntCreditURL = "You can view the details of the same by <a href=\"$mycouponsurl\">clicking here</a>.";
	    $bodyArgs = array(
	                    "USER"=>$userinfo['firstname'] != "" ? $userinfo['firstname'] : "Customer",
						"ORDER_ID"=>$groupid,
	    				"ITEM_DETAILS"=>$itemdetails,
	                    "REFUND_AMOUNT"=>$refund_amount,
	    				"MYNT_CREDIT_URL"=>$myntCreditURL
	                );
	    
	    $customKeywords = array_merge($subjectArgs, $bodyArgs);
	    $mailDetails = array(
	            "template" => $template,
	            "to" => $userinfo['email'],
	            "bcc" => "myntramailarchive@gmail.com",
	            "header" => 'header',
	            "footer" => 'footer',
	            "mail_type" => \MailType::CRITICAL_TXN
	        );
	    
	    $multiPartymailer = new \MultiProviderMailer($mailDetails, $customKeywords);
	    $flag = $multiPartymailer->sendMail();
			
		$returnArray['status'] ='SUCCESS';
		$returnArray['successMsg'] = "A refund amount of Rs. ".$refund_amount." is added to the cashback account";
	}
	else{
		$returnArray['status'] = 'failed';
		$returnArray['errorMsg'] = "Already provided cashback for the selected item."; 
	}
	echo json_encode($returnArray);
	exit;
} else{
	$productsInCart = ItemManager::getProductDetailsForItemIds(explode(",", $_GET['itemid']), $orderid);
	if($productsInCart) {
		$smarty->assign("productsInCart", $productsInCart);
		$smarty->assign("action", "override");
	} else {
		$smarty->assign("errorMsg", "Selected Items are not present in order no. - $orderid. Please refresh order page to verify!");
	}
	
	$smarty->assign("itemid", $_GET['itemid']);
	$smarty->assign("orderid", $_GET['orderid']);
}

$smarty->assign("main","price_override");
func_display("admin/home.tpl",$smarty);
?>