<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: statistics.php,v 1.39 2006/04/07 11:28:00 svowl Exp $
#

require "./auth.php";
require $xcart_dir."/include/security.php";

$location[] = array(func_get_langvar_by_name("lbl_order_report"), "report_orders.php");



if(empty($mode)) { $mode = "" ; }
if(empty($status)) { $status =""; }
if(empty($Startdate)) { $Startdate = "" ; }
if(empty($Enddate)) { $Enddate= "" ; }

$i =0;
if ($REQUEST_METHOD == "POST") {


	      $orders = array();
		  $SQL = array();
		  if ($mode == "search") {

                $Date1 =explode('/',$Startdate);
				$Date2 =explode('/',$Enddate);
				
                
                $startdate = mktime(0,0,0,$Date1[0],$Date1[1],$Date1[2]);
				$enddate   = mktime(0,0,0,$Date2[0],$Date2[1],$Date2[2]);


				 if($status != 'All'){
                     $SQLText =" AND o.payment_status = '$status' GROUP BY o.tracking ";

   				 }else{
                      $SQLText =" GROUP BY o.tracking ";
				 }

				 while($startdate <= $enddate ){

					
                    
                    if($report_type == 'week'){  
                    
						   $stdate = $startdate + 7 * 24 * 60 * 60;
						   
						   $stdate1 = date('Y',$stdate).date('m',$stdate).date('d',$stdate)."000000"; 
						   $startdate1  = date('Y',$startdate).date('m',$startdate).date('d',$startdate)."000000"; 
					
					}elseif($report_type == 'month'){
              
						  $stdate = $startdate + (30 * 24 * 60 * 60); 
						  $stdate1 = date('Y',$stdate).date('m',$stdate).date('d',$stdate)."000000"; 
						  $startdate1  = date('Y',$startdate).date('m',$startdate).date('d',$startdate)."000000"; 
         
					}
					$SQL[$i] = "SELECT  od.price, o.tracking, o.status, count(od.productid) as no_of_products,DATE_FORMAT($startdate1,'%m/%d/%y') as duration1,DATE_FORMAT($stdate1,'%m/%d/%y') as duration2 FROM $sql_tbl[orders] as o , $sql_tbl[order_details] as od WHERE  o.orderid = od.orderid  AND (date >= $startdate1 AND date <= $stdate1) ";

					 $SQL[$i] = $SQL[$i].$SQLText ;
					 $orders[$i] = func_query($SQL[$i]);
					                     
					 $startdate = $stdate;
                     $i++;  

                 }//end of while  

				// calculate the and subtotal & total of all sales made
			     $total = 0;
			   
                 for($j=0 ;$j< count($SQL);$j++){
					
					$rs = mysql_query($SQL[$j]);
				    while($row = mysql_fetch_array($rs)){
                          $total += $row[0] * $row[3]; 

							
					}
					mysql_free_result($rs);
		         } 
                /*end of for loop*/

                       
			}

}



#
# Assign Smarty variables and show template
#
$smarty->assign("stat", $status);
$smarty->assign("mode", $mode);
$smarty->assign("SDate", $Startdate);
$smarty->assign("EDate", $Enddate);
$smarty->assign("orders", $orders);
$smarty->assign("total", $total);

$smarty->assign("main", "report_orders");

# Assign the current location line
$smarty->assign("location", $location);

# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
