
<?php
define("REPORTS_CONNECTION",true);
require "./auth.php";
require $xcart_dir."/include/security.php";
include_once("../include/func/func.reports.php");
require_once("../include/class/class.html2fpdf.php");


global $sql_ro;
$sql_host = $sql_ro['host'];
$sql_user = $sql_ro['user'];
$sql_password = $sql_ro['password'];
$sql_db = $sql_ro['db'];

//session_start();
//putenv("TZ=Asia/Calcutta");
//date_default_timezone_set('Asia/Calcutta');
//db_query("SET SESSION time_zone = 'Asia/Calcutta'; ");
if(isset($_POST['mode'])|| isset($_POST['export'])){
	$rid = $_POST['reportid'];
	$filternames = $_POST['fname'];
	$filtertypes = $_POST['ftype'];
	if(isset($_POST['export'])){
		$filternames= $XCART_SESSION_VARS['adminfilters'];
		$filtertypes =$XCART_SESSION_VARS['adminfiltersTypes'];
		$rid= $XCART_SESSION_VARS['adminreport'];
		$reportid=$rid;
	}
	
	$conditions = array();
	$dateconditions=array();
	$exportfilters=array();
	$filtervalues=array();
	$dat=0;
	if(isset($_POST['export'])){
		$filtervalues= $XCART_SESSION_VARS['adminfiltervalues'];
	}else{
		for($i=0;$i<count($filternames);$i++){
			$filtervalues[$filternames[$i]]=$_POST[$filternames[$i]];
		}
		$XCART_SESSION_VARS['adminfiltervalues']=$filtervalues;
	}
	
	for($i=0;$i<count($filternames);$i++){
		$fieldTypeName = $filternames[$i]; // added fo get the field name , and check its a from date or to date
		$cond=$filtervalues[$filternames[$i]];
		$exportcond1=$filternames[$i];
		$exportcond2=$filtervalues[$filternames[$i]];
		if($filtertypes[$i]=="DAT"){
			//$smarty->assign("reportname", $cond);
			//if user enters date in format 1/1/2008 with out the zeros
			$my_cond=explode("/",$cond);
			$new_cond="";
			for($d=0;$d<2;$d++){
				$my_cond[$d]=substr($my_cond[$d]+100,1)."/";
			}
		for($m=0;$m<count($my_cond);$m++){
				$new_cond.=$my_cond[$m];
			}
			$cond=unix_date($new_cond,$fieldTypeName);
			$date=date("d",$cond)."-".date("M",$cond).",".date("Y",$cond);
			$exportcond2=$date;
			$dateconditions[$dat]=$cond;
			$dat++;
			
		}
	if($filtertypes[$i]=="SPL"||$filtertypes[$i]=="DPL"){
			//$smarty->assign("reportname", $cond);
				
			$name_value=explode(":",$cond);
			$exportcond2=$name_value[0];
			$cond=$name_value[1];
			
		}
		$conditions[$i] = $cond;
		$exportfilters[$i]=$exportcond1.": ".$exportcond2;
	}
	$reportdetails=func_getreportquery($reportid);
	$reportname=$reportdetails[0];
	$reportquery=$reportdetails[1];
	$reporttype=$reportdetails[5];
	$sumquery=$reportdetails[2];
	$mailto=$reportdetails[3];
	if( $reporttype == "Q")
	{
		$size = count($conditions);
		$datsize=count($dateconditions);
		//	$smarty->assign("size",count($filternames));
		//	$smarty->assign("cond1",$conditions[0]);
		//	$smarty->assign("cond2",$conditions[1]);
		//	$smarty->assign("cond3",$conditions[2]);
		//	func_display("view_report.tpl",$smarty);
		$ckhflag=false;
		$findme   = array("FRMDAT","TODAT");
		foreach ($findme as $chk) 
		{
			$pos = strpos($reportquery, $chk);
			if ($pos === false) {
			}
			else{
				$chkflag=true;
			}
		}
		if($chkflag&&$datsize>0)
		{
			for ($j = 0; $j < $datsize; $j++)
			{
				$reportquery = str_replace($findme[$j],$dateconditions[$j],$reportquery);
				$sumquery = str_replace($findme[$j],$dateconditions[$j],$sumquery);
			}
		}
		for ($j = 0; $j < $size; $j++)
		{
			$reportquery = str_replace_count("?",$conditions[$j],$reportquery,1);
		}
		for ($j = 0; $j < $size; $j++)
		{
			$sumquery = str_replace_count("?",$conditions[$j],$sumquery,1);
		}
		$finalquery=$reportquery;
	
	
		if(!empty($sumquery))
		{
			$finalquery=$finalquery." union ".$sumquery;
		}

        $queries = explode("union", $finalquery);
        $reportresult = array();
        $weblog->info("Query started at time: ". date("m/d/Y H:i:s", time()));
        foreach ($queries as $q) {
            $result = db_query($q, TRUE);
            if ($result) {
                while ($row = db_fetch_array($result))
                {
                    $reportresult[] = $row;
                }
            }
        }
        $weblog->info("Query finished at time: ". date("m/d/Y H:i:s", time()));
	}
	elseif( $reporttype == "SP" )
	{
		$finalquery = $reportquery;
		$finalquery = mysql_real_escape_string(trim($reportquery));

		$mysqli = new mysqli("$sql_host","$sql_user","$sql_password","$sql_db");
		/* check connection */
		if (mysqli_connect_errno()) 
		{
			$weblog->info("Connect failed: ".mysqli_connect_error());
			continue;
		}

		if($mysqli->multi_query($finalquery)) 
		{
			do 
			{
				/* store first result set */                                          
				if ($result = $mysqli->store_result())
				{                              
					while ($row = $result->fetch_assoc()) 
					{
                				$reportresult[] = $row;
            				}
            				$result->close();                                                 
        			}
    			} while ($mysqli->next_result());                                         
		}		                                                                             
	}
	$coloums = array();
	foreach($reportresult[0] as $key1=>$item1){
		$coloums[]=$key1;
	}
	$filters=$exportfilters;
	//unset($_POST['mode']);
	
	if(!isset($_POST['export'])){
		$smarty->assign("reportid",$rid);
		$smarty->assign("coloums", $coloums);
		$smarty->assign("reportname",$reportname);
		$smarty->assign("mailto",$mailto);
		/*foreach($reportresult as $k=>$_data){
			 $time_stamp_date = strtotime($_data['date']);
	     	 $reportresult[$k]['date'] = date("Y-m-d H:i:s", $time_stamp_date);
		}*/
		$smarty->assign("reportresult", $reportresult);
		//session_start();
	    $XCART_SESSION_VARS['adminfilters']=$_POST['fname'];
		$XCART_SESSION_VARS['adminfiltersTypes']=$_POST['ftype'];
	    $XCART_SESSION_VARS['adminreport']=$_POST['reportid'];
		func_display("view_report.tpl",$smarty);
		exit;
	}

	//Header("Content-Type: application/vnd.ms-excel");
	//Header("Content-Disposition: attachment; filename='data.xls'");
	//func_display("excel_reports.tpl",$smarty);

	if($_POST['exporttype']=='xls'){
		$rname = $_POST['report_name'];
		$rname = $_POST['report_name'];
		//$coloums=$_POST['columns'];
		$filename=$rname.".xls";
		$filename = str_replace(" ","_",$filename);
		Header("Content-Type: application/vnd.ms-excel");
		Header("Content-Disposition: attachment; filename=$filename");
		$smarty->assign("rname", $rname);
		$smarty->assign("filters", $filters);
		$smarty->assign("coloums", $coloums);
		$smarty->assign("reportresult", $reportresult);
		func_display("excel_reports.tpl",$smarty);
	}
	else if($_POST['exporttype']=='pdf'){
		
		$rname = $_POST['report_name'];
		$smarty->assign("rname", $rname);
		$smarty->assign("coloums", $coloums);
		$smarty->assign("filters", $filters);
		$smarty->assign("reportresult", $reportresult);
		$pdfcontent = $smarty->fetch('../skin1/excel_reports.tpl');
		$pdf=new HTML2FPDF();
		$pdf->AddPage();
		$pdf->WriteHTML($pdfcontent);
		$pdf->Output();

	}
	else if($_POST['exporttype']=='mail'){
		$rname = $_POST['report_name'];
		$mailto = $_POST['mailto'];
		$smarty->assign("rname", $rname);
		$smarty->assign("coloums", $coloums);
		$smarty->assign("reportresult", $reportresult);
		$smarty->assign("filters", $filters);
		$body = $smarty->fetch('excel_reports.tpl');
		//mail("nitin.sirohi@gmail.com", "test", $body,"test@myntra.com");
		$subject=$rname." Report";
      
		//$mailto='ashutosh.lawania@gmail.com,mukesh.bansal@gmail.com,vineet.saxena@gmail.com,sankar.bora@gmail.com,agrawal.rahul@gmail.com,raveen.sastry@gmail.com';
		$headers = "Content-Type: text/html; charset=ISO-8859-1 " . "\n";
        $mail_detail = array(
                            "to"=>$mailto,
                            "subject"=>$subject,
                            "content"=>$body,
                            "from_name"=>'Myntra Admin',
                            "from_email"=>'admin@myntra.com',
                            "headers"=>$headers,
                        );
        send_mail_on_domain_check($mail_detail);		
		if(empty($mailto)){
			$mailmsg="e-mail could not be sent as e-mail address not defined for this report ";
		}
		else{
			$mailmsg="Report Mailed Succesfully!!!!! ";
		}
		$smarty->assign("mailmsg",$mailmsg);
		$smarty->assign("mailsent","Y");
		func_display("view_report.tpl",$smarty);

	}
}
else{
	$reportid = $_GET['reportid'];
	$reportdetails=func_getreportquery($reportid);
	$reportname=$reportdetails[0];
		$fquery = "select name,datatype_id,attribute from `mk_reports_filters` where report_id='$reportid' order by id asc ";
		$fresult = db_query($fquery);
		$filtersarray = array();
		if ($fresult)
		{
			$i=0;
	
			while ($row=db_fetch_array($fresult))
			{
				$filtersarray[$i] = $row;
				$i++;
			}
		}
		$filters = array();
		for($j=0;$j<count($filtersarray);$j++)
		{
			$filters[$j][0]=$filtersarray[$j]['name'];
			$filters[$j][3]=str_replace(" ","_",$filtersarray[$j]['name']);
			$filters[$j][1]=$filtersarray[$j]['datatype_id'];
			if($filtersarray[$j]['datatype_id']==SPL)
			{
				$myarray = explode(",",$filtersarray[$j]['attribute']);
				$optionarray = array();
				for($k=0;$k<=count($filtersarray);$k++)
				{
					$optionarray[$myarray[$k]]=$myarray[$k];
				}
				$filters[$j][2] = $optionarray;
			}
			if($filtersarray[$j]['datatype_id']==DPL)
			{
				$dquery = $filtersarray[$j]['attribute'];
				$dresult = db_query($dquery);
				$optionarray = array();
				if ($dresult)
				{
					$a=0;
					while ($row=db_fetch_row($dresult))
					{
						//$filtersarray[$a] = $row;
						$optionarray[$row[0]]=$row[1];
						$a++;
					}
				}	
				$filters[$j][2] = $optionarray;
			}	
			//$filters[$j][2]=$filtersarray[$j]['attribute'];
		}
		$date=date("d",time())."-".date("M",time()).",".date("Y",time());
		$smarty->assign("date", $date);
		$smarty->assign("filters", $filters);
		$smarty->assign("reportid",$reportid);
		$smarty->assign("reportname",$reportname);
		func_display("view_report.tpl",$smarty);
	
}


?>


