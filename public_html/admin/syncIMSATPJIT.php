<?php

include_once("auth.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/modules/amqpclients/producer/AMQPMessageProducer.php");
include_once("updateInventory.php");
require_once "WMSDBConnUtil.php";

$client = AMQPMessageProducer::getInstance();

echo "[" . date('j-m-Y H:i:s') . "(";
$imsConn = WMSDBConnUtil::getConnection('imsdb1.myntra.com:3306', TRUE, WMSDBConnUtil::IMS_WRITE);
$atpConn = WMSDBConnUtil::getConnection('atpdbmaster.myntra.com:3306', TRUE, WMSDBConnUtil::IMS_WRITE);
$wmsConn = WMSDBConnUtil::getConnection('wmsdb2.myntra.com:3306', TRUE, WMSDBConnUtil::IMS_READ);
$vmsConn = WMSDBConnUtil::getConnection('marketplacedb2.myntra.com:3307', TRUE, WMSDBConnUtil::IMS_READ);

$to = "engg_erp_wms@myntra.com,marketplaceoncall@myntra.com";
$fromHeader = "From: inventorysync@myntra.com";
$subject = "[WMSALERT]ERP Inventory: JIT - IMS and ATP Data Sync Report";

if (!$imsConn || !$atpConn || !$wmsConn || !$vmsConn) {
    die('Could not connect: ' . mysql_error());
}

if (mysql_select_db('myntra_ims', $imsConn) === false) {
    echo "WMS db unaccessible \n";
    exit;
}

if (mysql_select_db('myntra_vms', $vmsConn) === false) {
    echo "VMS db unaccessible \n";
    exit;
}

if (mysql_select_db('myntra_atp', $atpConn) === false) {
    echo "ATP db unaccessible \n";
    exit;
}

if (mysql_select_db('myntra_wms', $wmsConn) === false) {
    echo "WMS db unaccessible \n";
    exit;
}
$finalSkuQueries = array();
$finalAllSkus = array();
$myntraSellerIds = "1,19,21,25,29,30";
$reportInvMismatch = array();
$reportAvlMismatch = array();
$reportVendorMismatch = array();
$reportInvMismatchImsZero = array();

$authHeader = "Authorization: Basic YXBpYWRtaW46bTFudHJhUjBja2V0MTMhIw==";
$appPropUrl = "http://erptools.myntra.com/myntra-tools-service/platform/tools/properties/search/?q=name.eq:wms.myntra.inventory.myntraSeller.ids";
$whPropUrl = "http://erptools.myntra.com/myntra-tools-service/platform/tools/properties/search/?q=name.eq:jit.warehouse.id";
$output = shell_exec("curl -H '" . $authHeader . "' -H 'Content-Type:application/xml' -XGET '" . $appPropUrl . "' -d '" . $data . "'");
$whIdsOutput = shell_exec("curl -H '" . $authHeader . "' -H 'Content-Type:application/xml' -XGET '" . $whPropUrl . "' -d '" . $data . "'"); 
$xml = simplexml_load_string($output);
$whIdsXml = simplexml_load_string($whIdsOutput); 

if ($xml->status->statusType == "SUCCESS")
    $myntraSellerIds = $xml->data->applicationProperty->value;
if ($whIdsXml->status->statusType == "SUCCESS") {
    $whIds = $whIdsXml->data->applicationProperty->value;
}
else {
    echo "Unable to retrieve application property for jit warehouses \n";
    exit;
}
$vendorIds = array();
        $vendorQuery = "select id from vendor where status <> 'CLOSED'";
        $vendor_result = mysql_query($vendorQuery, $vmsConn);
        if ($vendor_result) {
            while ($row = mysql_fetch_array($vendor_result, MYSQL_ASSOC)) {
                $vendorIds[] = $row['id'];
            }
        }
        $vendorsCsv=implode(",", $vendorIds);

for ($loop = 0; $loop < 2; $loop++) {
    $offset = 0;
    $limit = 10000;
    $hasMore = true;
    $allSkus = array();
    $skuQueries = array();
   $reportLoopInvMismatch = array();
        $reportLoopAvlMismatch = array();
        $reportLoopVendorMismatch = array();
    $reportLoopInvMismatchImsZero = array(); 
    $totalImsAtpMismatch = 0;

    while ($hasMore) {
	//TEst
        $warningSkus = array();
        $skuId2AtpInvMap = array();
        $skuId2AtpAvlMap = array();
        $skuId2ImsInvMap = array();
        $skuId2ImsAvlMap = array();
        $skuId2ImsVendorMap = array();
        $skuMultipleVendorArray = array();
        $skuId2AtpVendorMap = array();
        $skuSellerMap=array();
        $skuIds = array();
        //$skusQuery = "select id from mk_skus where enabled = 1 LIMIT $offset, $limit";
        $skusQuery = "select sku_id as id from wh_inventory where inventory_count>0 and supply_type='JUST_IN_TIME' and last_synced_on >= (NOW() - INTERVAL 1 DAY) LIMIT $offset, $limit";
        $sku_result = mysql_query($skusQuery, $imsConn);
        if ($sku_result) {
            while ($row = mysql_fetch_array($sku_result, MYSQL_ASSOC)) {
                $skuIds[] = $row['id'];
            }
        }
        @mysql_free_result($sku_result);
        echo "Sku Ids: " . $skusQuery . " " . count($skuIds) . "\n";
        if (empty($skuIds)) {
            $hasMore = false;
            continue;
        }


        $syncSkus = array();
        $imsInvCountQuery = "select sku_id, seller_id, vendor_id,inventory_count-blocked_order_count as net_count,warehouse_id as avlWh from wh_inventory where warehouse_id in (" . $whIds . ") and inventory_count-blocked_order_count>0 and sku_id in (" . implode(",", $skuIds) . ") and  vendor_id in (" . $vendorsCsv . ")   and store_id =1 and seller_id in (" . $myntraSellerIds . ") and supply_type = 'JUST_IN_TIME'";
        $imsInvResults = mysql_query($imsInvCountQuery, $imsConn);
        if ($imsInvResults) {
            while ($row = mysql_fetch_array($imsInvResults, MYSQL_ASSOC)) {
                if ($row['net_count'] > 0 && isset($skuSellerMap[$row['seller_id']][$row['sku_id']])) {
                    $skuMultipleVendorArray[] = $row['seller_id'] . "," . $row['sku_id'] . "\n" . $row['vendor_id'] . "," . $skuId2ImsVendorMap[$row['seller_id']][$row['sku_id']];
                    unset($skuId2ImsInvMap[$row['seller_id']][$row['sku_id']]);
                    unset ($skuId2ImsAvlMap[$row['seller_id']][$row['sku_id']]);
                    unset ($skuId2ImsVendorMap[$row['seller_id']][$row['sku_id']]);

                } else {
                    $skuSellerMap[$row['seller_id']][$row['sku_id']]=true;
                    $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']] = $row['net_count'];
                    $skuId2ImsAvlMap[$row['seller_id']][$row['sku_id']] = $row['avlWh'];
                    $skuId2ImsVendorMap[$row['seller_id']][$row['sku_id']] = $row['vendor_id'];
                }
            }
        }
        @mysql_free_result($imsInvResults);
        $totCount = 0;
        foreach ($skuSellerMap as $type) {
            $totCount+= count($type);
        }
	echo "Total sku count for this iteration: ". $totCount . "\n";
        $atpInvCountQuery = "select vendor_id,seller_id,sku_id, inventory_count-blocked_order_count as net_count,available_in_warehouses from inventory where store_id = 1 and seller_id in (" . $myntraSellerIds . ") and supply_type = 'JUST_IN_TIME' and sku_id in (" . implode(",", $skuIds) . ")";
    $atpInvResults = mysql_query($atpInvCountQuery, $atpConn);
        if ($atpInvResults) {
            while ($row = mysql_fetch_array($atpInvResults, MYSQL_ASSOC)) {
                $skuId2AtpInvMap[$row['seller_id']][$row['sku_id']] = $row['net_count'];
                $skuId2AtpAvlMap[$row['seller_id']][$row['sku_id']] = $row['available_in_warehouses'];
                $skuId2AtpVendorMap[$row['seller_id']][$row['sku_id']] = $row['vendor_id'];
                if (isset($skuId2ImsInvMap[$row['seller_id']][$row['sku_id']]) && ($skuId2AtpVendorMap[$row['seller_id']][$row['sku_id']] != $skuId2ImsVendorMap[$row['seller_id']][$row['sku_id']] || $skuId2AtpInvMap[$row['seller_id']][$row['sku_id']] != $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']] || $skuId2AtpAvlMap[$row['seller_id']][$row['sku_id']] != $skuId2ImsAvlMap[$row['seller_id']][$row['sku_id']])) {
                    $totalImsAtpMismatch++;
                    $syncSkus[] = $row['sku_id'] . "_" . $row['seller_id'];
                    // $diff = $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']] - $skuId2AtpInvMap[$row['seller_id']][$row['sku_id']];
                    $skuQueries[] = "update inventory set vendor_id=" . $skuId2ImsVendorMap[$row['seller_id']][$row['sku_id']] . ",inventory_count= " . $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']] . ",available_in_warehouses='" . $skuId2ImsAvlMap[$row['seller_id']][$row['sku_id']] . "', last_modified_on = now()  where supply_type = 'JUST_IN_TIME' and store_id = 1 and seller_id=" . $row['seller_id'] . " and sku_id=" . $row['sku_id'];

                    if ($skuId2AtpInvMap[$row['seller_id']][$row['sku_id']] != $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']]) {
                        $reportLoopInvMismatch[] = $row['sku_id'];
                    }

                    if ($skuId2AtpVendorMap[$row['seller_id']][$row['sku_id']] != $skuId2ImsVendorMap[$row['seller_id']][$row['sku_id']]) {
            			$reportLoopVendorMismatch[] = $row['sku_id'];
                    }
                    if ($skuId2AtpAvlMap[$row['seller_id']][$row['sku_id']] != $skuId2ImsAvlMap[$row['seller_id']][$row['sku_id']]) {
                        $reportLoopAvlMismatch[] = $row['sku_id'];
                    }
                }
		        else if(!isset($skuId2ImsInvMap[$row['seller_id']][$row['sku_id']]) && $row['net_count'] > 0) {
		            // No positive inventory in IMS but inventory in ATP
                    $totalImsAtpMismatch++;
                    $syncSkus[] = $row['sku_id'] . "_" . $row['seller_id'];
                    // $diff = $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']] - $skuId2AtpInvMap[$row['seller_id']][$row['sku_id']];
                    $skuQueries[] = "update inventory set inventory_count=0, available_in_warehouses='', last_modified_on = now()  where supply_type = 'JUST_IN_TIME' and store_id = 1 and seller_id=" . $row['seller_id'] . " and sku_id=" . $row['sku_id'];
                    $reportLoopInvMismatchImsZero[] = $row['sku_id'];
		        }
            }
        }


        @mysql_free_result($atpInvResults);

        $allSkus = array_merge($allSkus, $syncSkus);

        $offset += $limit;
    }
    if ($loop == 0) {
        $finalSkuQueries = $skuQueries;
        $finalAllSkus = $allSkus;
	    $reportInvMismatch=$reportLoopInvMismatch;
	    $reportVendorMismatch=$reportLoopVendorMismatch;
	    $reportAvlMismatch=$reportLoopAvlMismatch;
        $reportInvMismatchImsZero=$reportLoopInvMismatchImsZero;;
    } else {
        $finalAllSkus = array_intersect($finalAllSkus, $allSkus);
        $finalSkuQueries = array_intersect($finalSkuQueries, $skuQueries);
	    $reportInvMismatch=array_intersect($reportInvMismatch,$reportLoopInvMismatch);
        $reportVendorMismatch=array_intersect($reportVendorMismatch,$reportLoopVendorMismatch);
        $reportAvlMismatch=array_intersect($reportAvlMismatch,$reportLoopAvlMismatch);
        $reportInvMismatchImsZero=array_intersect($reportInvMismatchImsZero, $reportLoopInvMismatchImsZero);
    }
//cvb
//$hasMore=false;
}

mysql_close($imsConn);
mysql_close($wmsConn);
mysql_close($vmsConn);
print_r($finalSkuQueries);
$updateLimit=0;
foreach ($finalSkuQueries as $query) {
    $result = mysql_query($query, $atpConn);
//    echo $query."\n";	
    if(!$result){
        $errQueries = $errQueries.$query."\n";
    }
    @mysql_free_result($result);
//   $updateLimit++; 
}

//echo "###errQueries### \n" . $errQueries . "\n";
$reindexLimit=0;
print_r($finalAllSkus);
foreach (array_unique($finalAllSkus) as $skuSeller) {
    $startIndx = strpos($skuSeller,"_");
    $sellerId=substr($skuSeller, $startIndx+1);
    $skuId=substr($skuSeller, 0, $startIndx);
    $inventoryEntry = array('storeId' => '1', 'sellerId' => $sellerId, 'skuId' => $skuId, 'supplyType' => 'JUST_IN_TIME');
    $request = array('inventoryEntry' => $inventoryEntry);
    print_r($request);	
    $client->sendMessage('atpReindexSkuQueue', $request, 'reindexInventory', 'xml', true, 'inv');
 //  $reindexLimit++;
}
    $report = "Out of sync:" . $totalImsAtpMismatch . "\n";
    $report = $report . "Inv out of sync: " . implode(",", $reportInvMismatch) . "\nAvl out of sync:" . implode(",", $reportAvlMismatch) . "\nVendor out of sync:" . implode(",", $reportVendorMismatch) . "\n\n" . "Inv set in ATP But not IMS: " . implode(",", $reportInvMismatchImsZero) . "\n";
    $report=$report."Skus with multiple approved vendors and postive inventory:".print_r($skuMultipleVendorArray,1)."\n";
    mail($to, $subject, $report, $fromHeader);
    echo $report;
//    echo implode(",", $allSkus);

mysql_close($atpConn);

//echo "###skuQueries### \n".implode("\n",$skuQueries);
echo ")" . date('j-m-Y H:i:s') . "]";
?>

