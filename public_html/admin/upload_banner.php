<?php
require_once "./auth.php";
ini_set('display_errors', true);
require_once($xcart_dir."/include/security.php");
require_once($xcart_dir."/AMAZONS3/util/S3.php");
require_once($xcart_dir."/include/func/func.core.php");

include_once \HostConfig::$documentRoot ."/utils/imagetransferS3.php";
include_once \HostConfig::$documentRoot ."/Profiler/Profiler.php";
use imageUtils\Jpegmini;
ini_set("max_execution_time", 360000);//Global-30S,Local-360000S
//ini_set("file_uploads", 1);//G-1,L-1
//ini_set("post_max_size", 60);//G-8M,L-60MB
//ini_set("upload_max_filesize", 3);//G-2M,L-3
//ini_set("memory_limit", 900);//G-1280M,L-900M
//ini_set("max_input_time", 180);//G-60S,L-180S

define('S3_BUCKET_NAME', 'myntrawebimages');//subdomain,test20110701
define('S3_DOMAIN', 's3.amazonaws.com');//domain

S3::setAuth($system_amazon_access_key, $system_amazon_secret_key);//set accessket\y and secret key from config file
//S3::putBucket(S3_BUCKET_NAME, S3::ACL_PUBLIC_READ);//initialize bucket does not affect if already exists

$monthName = strtolower(date('FY'));

define("BANNER_IMAGES_REL_PATH","images".DIRECTORY_SEPARATOR."banners");
define("BANNER_IMAGES_DIR",HostConfig::$documentRoot.DIRECTORY_SEPARATOR.BANNER_IMAGES_REL_PATH);
define("BANNER_IMAGES_SUB_DIR",HostConfig::$documentRoot.DIRECTORY_SEPARATOR.BANNER_IMAGES_REL_PATH.DIRECTORY_SEPARATOR.$monthName);

//create mailer directory
if(!file_exists(BANNER_IMAGES_DIR)){
    mkdir(BANNER_IMAGES_DIR,0777) ;
}

//create a separate directory every month
if(!file_exists(BANNER_IMAGES_SUB_DIR)){
    mkdir(BANNER_IMAGES_SUB_DIR,0777) ;
}

if ($REQUEST_METHOD == "POST") {

    $tmp_file = $HTTP_POST_FILES['image']['tmp_name'];
    $file_name = $HTTP_POST_FILES['image']['name'];
    copy($tmp_file, BANNER_IMAGES_SUB_DIR.DIRECTORY_SEPARATOR.time().'-' . $file_name);
    //if files exist in the folder then only perform s3 transfer
    $files = filesInDirectory(BANNER_IMAGES_SUB_DIR);

    if($files['count'] > 0) {
        foreach($files['files'] as $file){
            if(strpos($file, "_mini") !== false){
                continue;
            }
            $uploadfile = BANNER_IMAGES_SUB_DIR.DIRECTORY_SEPARATOR.$file;

            //move image/s to S3
            if(S3::putObjectFile($uploadfile, S3_BUCKET_NAME, BANNER_IMAGES_REL_PATH.DIRECTORY_SEPARATOR.$monthName."/".$file, S3::ACL_PUBLIC_READ, array(), null, 2592000) === false){
                $message .= "Failed to move $file. Please try moving to s3 again<br/>";

            } else {
               if(Jpegmini::copyCompressedPlaceholderFile($uploadfile)&&myntra\utils\cdn\imagetransferS3::compress($uploadfile)){
                //on successful transfer remove file otherwise retain it to subsequent transfer
                //unlink($uploadfile);
                    $message .= "$file moved successfully to '".HostConfig::$cdnBase."/".BANNER_IMAGES_REL_PATH."/$monthName/$file'<br/>";
                    Profiler::increment("imagesize-banner-general", filesize(HostConfig::$documentRoot.$uploadfile)); 
                }else{
                    $message .= "Failed to compress $file. Please try uploading again<br/>";
                }
            }
        }

        //rrmdir(BANNER_IMAGES_SUB_DIR);//not removing directory because files uploaded by somebody else may be lost
        
    } else {
        $message = "No images to move to S3";
    }

    $top_message["content"] = $message;
    $top_message["anchor"] = "featured";
}

//retrieve S3 mailer bucket of the current month in reverse chronological order
$bucketContent = S3::getBucket(S3_BUCKET_NAME, BANNER_IMAGES_REL_PATH.DIRECTORY_SEPARATOR."$monthName");
$bucketContentIndex = array();

if(is_array($bucketContent)){
    foreach($bucketContent as $key=>$val){
        $bucketContentIndex[] = $val;
    }
    $bucketContent = quickSortRecursive( $bucketContentIndex, $left = 0 , $right = NULL , 'time');
}
 
$countImg = sizeof($bucketContent);
for($i=($countImg-1);$i>=0;$i--){
    $HTML .= "<br/><br/><a href='".HostConfig::$cdnBase."/".$bucketContent[$i]['name']."' target='blank'><img src='".HostConfig::$cdnBase."/".$bucketContent[$i]['name']."' height='75'/>&nbsp;".HostConfig::$cdnBase."/".$bucketContent[$i]['name']."</a>";
}


# Assign the result
$smarty->assign("main","uploadbanner");
$smarty->assign("top_message", $top_message);
# Assign the current location line
$smarty->assign("location", $location);
# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);
$smarty->assign("html", $HTML);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
