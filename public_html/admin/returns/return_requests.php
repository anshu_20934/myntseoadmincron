<?php
require "../auth.php";
require $xcart_dir."/include/security.php";
require $xcart_dir."/include/func/func.returns.php";
include_once("$xcart_dir/modules/apiclient/WarehouseApiClient.php");

$uniqueId = uniqid();
$smarty->assign('progresskey', $uniqueId);

$searchreturnid = $_GET['returnid'];
$searchstatus = $_GET['status'];
$searchorderid = $_GET['orderid'];
$searchlogin = $_GET['login'];

$timefilters = get_time_filters();
$couriers = get_pickup_courier_partners();
$statuses = get_all_return_statuses();

$new_statuses =  array();
foreach ($statuses as $index => $status){
	$new_statuses[] = $status;
}
$warehouses = WarehouseApiClient::getAllWarehouses();

$smarty->assign("warehouses",json_encode($warehouses));
$smarty->assign("timefilters",json_encode($timefilters));
$smarty->assign("couriers", json_encode($couriers));
$smarty->assign("statuses", json_encode($new_statuses));
$smarty->assign("returnid", $searchreturnid);
$smarty->assign("status", $searchstatus);
$smarty->assign("orderid", $searchorderid);
$smarty->assign("searchlogin", $searchlogin);

$smarty->assign("main","return_requests");
func_display("admin/home.tpl", $smarty);

?>