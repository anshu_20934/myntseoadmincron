<?php

require("../auth.php");
include_once("$xcart_dir/include/security.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/modules/apiclient/LocationApiClient.php");
include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");
include_once("$xcart_dir/include/class/class.mail.multiprovidermail.php");

echo "Access to this page for changing item status is disabled.<br><br>For any issues related to item status, please send mail to wmsoncall@myntra.com";
exit(0);

$action = $_REQUEST['action'];
$warehouseId = $_REQUEST['warehouseid'];
$curr_status = $_REQUEST['currentstatus'];

$uploadstatus = false;
$success_info = "";
$error_info = "";

if (empty($action) )
    $action = 'upload';

if ($action == 'verify') {
    if(!isset($_FILES) && isset($HTTP_POST_FILES))
        $_FILES = $HTTP_POST_FILES;

    $errorMsg = "";
    if(!empty($_FILES['move_items_to_cr']['name'])) {
        $uploaddir = '../../bulkmoveitemstocr/';
        $extension = explode(".",basename($_FILES['move_items_to_cr']['name']));
        $uploadfile = $uploaddir . "move_items_to_cr" .date('d-m-Y-h-i-s').".".$extension[1];
        if (move_uploaded_file($_FILES['move_items_to_cr']['tmp_name'], $uploadfile)) {
            $itemsTobeMoved = array();
            $invalidItems = array();
            $fd = fopen ($uploadfile, "r");
            while (!feof ($fd)) {
                $buffer = fgetcsv($fd, 4096);
                $itemid = trim($buffer[0]);
                if(empty($itemid))
                    continue;

                if(!isInteger($itemid)){
                    $invalidItems[] = $itemid;
                    continue;
                }

                $itemsTobeMoved[] = $itemid;
            }
            fclose ($fd);
            if(count($invalidItems) > 0)
                $errorMsg .= "Following Items are not valid - ".implode(",", $invalidItems);
        } else {
            $errorMsg .=  "<br>File not uploaded, try again.. <br>";
            $action = "upload";
        }
    } else {
        $errorMsg .=  "<br>No file name provided..<br>";
        $action = "upload";
    }

    if(empty($errorMsg)){
        $uniqueId = uniqid();
        $smarty->assign('progresskey', $uniqueId);
        $smarty->assign('itemsTobeMoved', implode(", ", $itemsTobeMoved));
        $smarty->assign('uploadorderFile',$uploadfile);
    } else {
        $action = 'upload';
        $smarty->assign("errorMsg", $errorMsg);
    }
} else if ($action == 'confirm') {
    $filetoread  = $HTTP_POST_VARS['filetoread'];
    $progressKey = $HTTP_POST_VARS['progresskey'];
    if(file_exists($filetoread)){
        $fd = fopen ($filetoread, "r");
        $itemsTobeMoved = array();
        while (!feof ($fd)) {
            $buffer = fgetcsv($fd, 4096, ",");
            $itemid = trim($buffer[0]);
            if(empty($itemid))
                continue;
            $itemsTobeMoved[] = $itemid;
        }
        fclose($fd);
	$item_barcodes_tobe_moved = array_unique($itemsTobeMoved);
    foreach(array_chunk($item_barcodes_tobe_moved, 100) as $item_barcodes) {

        if($curr_status == 'A_R') {
                $response = "";
        } else {
                $response = ItemApiClient::moveItemsToAcceptedReturns($item_barcodes, $warehouseId, 'nitin.gurram');
        }

        if($response == "") {
	        $success = ItemApiClient::remove_item_order_association(0, $warehouseId, $item_barcodes, 'CUSTOMER_RETURNED', false, 'nitin.gurram@myntra.com');
        	if($success)
                	$success_info .= "\nSuccessfully moved items to CR: \n".implode(",",$item_barcodes);
       		 else
                	$error_info .= "\nFailed to update item status to CR\n".implode(",",$item_barcodes);
        } else {
               $error_info .= "\nAccept Returns call failed \n".implode(",",$item_barcodes);
        }
        sleep(2);
    }

        $mail_details = array(
            //"header"=>"Content-Type: text/plain",
            "from_email"=>'admin@myntra.com',
            "from_name"=>'OMS Admin',
            "to" => 'nitin.gurram@myntra.com',
            "cc" => 'nitin.gurram@mynta.com',
            "mail_type" => MailType::CRITICAL_TXN,
            "subject"=> "Moving items to C_R",
            "content"=> "below items are successfully moved <br/><br/><br/> $success_info <br/> below items are failed to move <br/><br/><br/> $error_info"
        );
        $multiPartymailer = new MultiProviderMailer($mail_details);
        $multiPartymailer->sendMail();
    } else {
    	$smarty->assign("errorMsg", "Uploaded file not found.");
    }
    $action = 'upload';
}
$smarty->assign("success_info", $success_info);
$smarty->assign("error_info", $error_info);
$smarty->assign("action", $action);
$smarty->assign("warehouseid", $warehouseId);
$smarty->assign("currentstatus", $curr_status);
$smarty->assign("main", "bulk_move_items_cr");
func_display("admin/home.tpl",$smarty);

?>