Ext.onReady(function() {
	Ext.QuickTips.init();
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
	Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
	Ext.Ajax.on('requestexception', myMask.hide, myMask);
	
	var downloadMask;
	
	var downloadPanel = new Ext.form.FormPanel( {
		renderTo : 'contentPanel',
		title : 'Download Panel',
		id : 'downloadPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 1},
			defaults : {width : 400, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items: [{
				items: [{
		        	xtype : 'combo',
		        	store : new Ext.data.JsonStore({
		        		fields : [ 'code', 'display_name'],
		        		data : couriers,
		        		sortInfo : {
		        			field : 'display_name',
		        			direction : 'ASC'
		        		}
		        	}),
		        	emptyText : 'Select Courier Service',
		        	displayField : 'display_name',
		        	valueField : 'code',
		        	fieldLabel : 'Courier Service',
		        	name : 'courier_service',
		        	mode: 'local',
		        	triggerAction: 'all'
				}]
			}]
		}],
		buttons : [ {
			text : 'Download Ready to pickup report',
			handler : function() {
				downloadAction('pickup');
			}
		}, {
			text : 'Download Ready to Re-Ship report',
			handler : function() {
				downloadAction('reship');
			}
		}]
	});
	
	var downloadAction = function(reportFor) {
		myMask.show();
		var form = downloadPanel.getForm();
		var courierCode = form.findField("courier_service").getValue();
		if(courierCode == "") {
			alert("Please select a courier to download Return Requests ready for Pickup");
			myMask.hide();
			return;
		}
		var format = "";
		if(reportFor == 'pickup') {
			window.open(httpLocation + "/admin/returns/ready_to_pickup_report.php?courier="+courierCode);
		} else if(reportFor == 'reship') {
			window.open(httpLocation + "/admin/returns/ready_to_reship_report.php?courier="+courierCode);
		}
		myMask.hide();
	}
	
	var statusStore = new Ext.data.JsonStore({
		fields : [ 'code', 'display_name' ,'color' ],
		data : statusArr
	});

	var return_status = new Ext.ux.form.SuperBoxSelect({
		xtype : 'superboxselect',
		fieldLabel : 'Return Status',
		emptyText : 'Select Return Status',
		name : 'status',
		anchor : '100%',
		store : statusStore,
		mode : 'local',
		displayField : 'display_name',
		valueField : 'code',
		forceSelection : true
	});
	
	
	var dateNamesField = {
    	xtype : 'combo',
    	emptyText : 'Select Date Type',
    	store : statusStore,
    	mode: 'local',
    	displayField : 'display_name',
    	valueField : 'code',
    	triggerAction: 'all',
    	fieldLabel : 'Date When',
    	name : 'datetype'
	};
	
	var searchPanel = new Ext.form.FormPanel({
		renderTo : 'contentPanel',
		title : 'Search Panel',
		id : 'formPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 2},
			defaults : {width : 450, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [{
				items : [{
					xtype : 'numberfield',
		        	id : 'return_id',
		        	fieldLabel : 'Return Id'
				}]
			}, {
				items: [return_status]
			}, {
				items : [{
		        	xtype : 'combo',
		        	store : new Ext.data.JsonStore({
		        		fields : [ 'code', 'display_name'],
		        		data : timefilters,
		        		sortInfo : {
		        			field : 'code',
		        			direction : 'ASC'
		        		}
		        	}),
		        	emptyText : 'Select Time filter',
		        	displayField : 'display_name',
		        	valueField : 'code',
		        	fieldLabel : 'Time Filter',
		        	name : 'filter_id',
		        	mode: 'local',
		        	triggerAction: 'all'
				}]
			},{
				items : [{
					xtype : 'numberfield',
		        	id : 'group_id',
		        	fieldLabel : 'Order Id'
				}]
			}, {
				items : [{
					xtype : 'numberfield',
		        	id : 'order_id',
		        	fieldLabel : 'Shipment Id'
				}]
			}, {
				items : [{
					xtype : 'numberfield',
		        	id : 'item_id',
		        	fieldLabel : 'Item Id'
				}]
			}, {
				items :[{
					xtype : 'textfield',
		        	id : 'login',
		        	fieldLabel : 'Login',
		        	width : 200
				}]
			}, {
				items : [{
		        	xtype : 'combo',
		        	store : new Ext.data.JsonStore({
		        		fields : [ 'code', 'display_name'],
		        		data : couriers,
		        		sortInfo : {
		        			field : 'display_name',
		        			direction : 'ASC'
		        		}
		        	}),
		        	emptyText : 'Select Courier Service',
		        	displayField : 'display_name',
		        	valueField : 'code',
		        	fieldLabel : 'Courier Service',
		        	name : 'courier_service',
		        	mode: 'local',
		        	triggerAction: 'all'
				}]
			}, {
				items :[{
					xtype : 'textfield',
		        	id : 'tracking_no',
		        	fieldLabel : 'Tracking No',
		        	width : 200
				}]
			},
			{
				items : [{
		        	xtype : 'combo',
		        	store : new Ext.data.JsonStore({
		        		fields : [ 'id', 'name'],
		        		data : warehouses,
		        		sortInfo : {
		        			field : 'name',
		        			direction : 'ASC'
		        		}
		        	}),
		        	emptyText : 'Select warehouse',
		        	displayField : 'name',
		        	valueField : 'id',
		        	fieldLabel : 'WareHouse',
		        	name : 'warehouseid',
		        	mode: 'local',
		        	triggerAction: 'all'
				}]
			}, {
				items : [{
					xtype : 'numberfield',
					id : 'store_return_id',
					fieldLabel : 'Store Return Id'
				}]
			}, {
				items : [{
					xtype : 'numberfield',
					id : 'store_id',
					fieldLabel : 'Store Id'
				}]
			}, {
				items: []
			},{
				items: [dateNamesField]
			}, {
				items : [{
					xtype : 'datefield',
	                name : 'from_date',
	                width : 150,
	                fieldLabel : "From Date"
				}]
			}, {
				items : [{
					xtype : 'datefield',
					name : 'to_date',
					width : 150,
					fieldLabel : "To Date"
				}]
			}]
		}],
		buttons : [ {
			text : 'Search',
			handler : function() {
				reloadGrid();
			}
		},{
			text : 'Download Report',
			handler : function() {
				var params = getSearchPanelParams();
				params["action"] = "downloadreport";
				params["progresskey"] = progresskey;
				downloadMask = new Ext.LoadMask(Ext.get('formPanel'), {msg: "Generating File"});
				downloadMask.show();
				Ext.Ajax.request({
					url : 'returnsajax.php',
					params : params,
				    success : function(response, action) {
						
					}
				});
				timerid = setInterval(trackStatus, 3000);
			}
		}]
	});
	
	
	function trackStatus() {
		Ext.Ajax.request({
			url : "/admin/getprogressinfo.php?type=status&progresskey="+progresskey,
			method : 'GET',
			mask: false,
		    success : function(response, action) {
		    	var jsonObj = Ext.util.JSON.decode(response.responseText);
		    	if(jsonObj.status == 'DONE'){
		    		clearInterval(timerid);
		    		downloadMask.hide();
		    		var jsonObj = Ext.util.JSON.decode(response.responseText);
					if(jsonObj.success) {
						var html = "Click <a href = '"+httpLocation + jsonObj.file_location+"' target='_blank'> here </a> to download file";
			    		Ext.Msg.show({
			    		    title:        "Download Link",
			    		    msg:        html,
			    		    icon:        Ext.MessageBox.INFO
			    		});
					} else {
						Ext.MessageBox.alert('Error', jsonObj.msg);
					}
		    		
		    	}
			}
		});
	}
	
	function getSearchPanelParams() {
		var form = searchPanel.getForm();
		
		if (form.isValid()) {
			var params = {};
			params["return_id"] = form.findField("return_id").getValue();
			params["group_id"] = form.findField("group_id").getValue();
			params["filter_id"] = form.findField("filter_id").getValue();
			params["order_id"] = form.findField("order_id").getValue();
			params["item_id"] = form.findField("item_id").getValue();
			params["status"] = form.findField("status").getValue();
			params["login"] = form.findField("login").getValue();
			params["courier_service"] = form.findField("courier_service").getValue();
			params["tracking_no"] = form.findField("tracking_no").getValue();
			params['date_type'] = form.findField("datetype").getValue();
			params["from_date"] = form.findField("from_date").getRawValue();
			params["to_date"] = form.findField("to_date").getRawValue();
			params["warehouseid"] = form.findField("warehouseid").getValue();
			params["store_return_id"] = form.findField("store_return_id").getValue();
			params["store_id"] = form.findField("store_id").getValue();
			return params;
		} else {
			return false;
		}
	}
	
	function reloadGrid() {
		
		var params = getSearchPanelParams();
		
		if (params == false) {
			Ext.MessageBox.alert('Error', 'Errors in the search form');
			return false;
		}
		if(params['filter_id'] != '' && params['status'] == ''){
			Ext.MessageBox.alert('Error', 'Please select status for time filter to work');
			return false;
		}
		if(params['date_type'] != '' && (params['from_date'] == '' || params['to_date'] == '')){
			Ext.MessageBox.alert('Error', 'Please select from date or to date');
			return false;
		}
		if(params['filter_id'] != '' && (params['date_type'] != '' && (params['from_date'] != '' || params['to_date'] != ''))){
			Ext.MessageBox.alert('Error', 'Please select either Time Filter or Date When Filter. Both of them do not work together');
			return false;
		}
		params["action"] = "search";
		myMask.show();
		searchResultsStore.baseParams = params;
		searchResultsStore.load();
	}
	
	var sm = new Ext.grid.CheckboxSelectionModel();
	
	var requestIdLinkRender = function(value, p, record) {
		return '<a href="' + httpLocation + '/admin/returns/return_request.php?id=' + record.data["returnid"] + '" target="_blank">' + record.data["returnid"] + '</a>';
	}
	
	var statusRenderer = function(value, p, record) {
		var status = record.data['status'];
		for (var i=0; i< statusArr.length; i++) {
			if (statusArr[i].code == status) {
				return statusArr[i].display_name;
			}
		}
		return status;
	}
	
	var courierRenderer = function(value, p, record) {
		var courier_code = record.data['courier_service'];
		for (var i=0; i< couriers.length; i++) {
			if (couriers[i].code == courier_code) {
				return couriers[i].display_name;
			}
		}
		return courier_code;
	}
	
	var colModel = new Ext.grid.ColumnModel({
		defaults: {
	        sortable: true
	    },
	    columns: [
	        sm,
	        {id : 'returnid', header : "Return Id", sortable: true, width: 75, dataIndex : 'returnid', renderer : requestIdLinkRender},
	        {id : 'group_id', header : "Order Id", sortable: true, width: 75, dataIndex : 'group_id'},
	        {id : 'filter_id', header : "Filter Id", sortable: true, width: 75, dataIndex : 'filter_id'},
	        {id : 'shipment_id', header : "Shipment Id", sortable: false, width: 75, dataIndex : 'shipment_id'},
	        {id : 'itemid', header : "Item Id", sortable: true, width: 75, dataIndex : 'itemid'},
	        {id : 'skucode', header : "SKUCode", sortable: false, width: 100, dataIndex : 'skucode'},
	        {id : 'product_display_name', header : "Product Name", sortable: false, width: 100, dataIndex : 'product_display_name'},
	        {id : 'productStyleName', header : "Style Name", sortable: false, width: 100, dataIndex : 'productStyleName'},
	        {id : 'article_number', header : "Article No", sortable: false, width: 100, dataIndex : 'article_number'},
	        {id : 'reason', header : "Reason", sortable: false, width: 100, dataIndex : 'reason'},
	        {id : 'quantity', header : "Quantity", sortable: false, width: 50, dataIndex : 'quantity'},
	        {id : 'status', header : "Return Status", sortable: true, width: 125, dataIndex: 'status', renderer : statusRenderer},
	        {id : 'return_mode', header : "Return Mode", sortable: false, width: 75, dataIndex : 'return_mode'},
	        {id : 'courier_service', header : "Courier", sortable: false, width: 75, dataIndex : 'courier_service', renderer : courierRenderer},
			{id : 'store_return_id', header : "Store Return Id", sortable: true, width: 75, dataIndex : 'store_return_id'},
			{id : 'store_id', header : "Store Id", sortable: true, width: 75, dataIndex : 'store_id'},
			{id : 'couponcode', header : "Refund Coupon", sortable: false, width: 125, dataIndex : 'couponcode'},
	        {id : 'refundamount', header : "Refund Amount", sortable: false, width: 100, dataIndex : 'refundamount'},
	        {id : 'login', header : "Customer Login", sortable: true, width: 150, dataIndex : 'login'},
	        {id : 'name', header : "Customer Name", sortable: true, width: 150, dataIndex : 'name'},
	        {id : 'pickup_address', header : "Address", sortable: false, width: 300, dataIndex : 'pickup_address'},
	        {id : 'createdby', header : "Created By", sortable: true, width: 300, dataIndex : 'createdby'},
	        {id : 'createddate', header : "Request Date", sortable: true, width: 125, dataIndex : 'createddate'},
	        {id : 'sentfortrackingdate', header : "Sent Tracking Date", sortable: true, width: 125, dataIndex : 'sentfortrackingdate'},
	        {id : 'pickupinitdate', header : "Pikcup Init Date", sortable: true, width: 125, dataIndex : 'pickupinitdate'},
	        {id : 'pickupdate', header : "PickUp Date", sortable: true, width: 125, dataIndex : 'pickupdate'},
	        {id : 'delivereddate', header : "Delivered Date", sortable: true, width: 125, dataIndex : 'delivereddate'},
	        {id : 'receiveddate', header : "Received Date", sortable: true, width: 125, dataIndex : 'receiveddate'},
	        {id : 'qapassdate', header : "QA Pass Date", sortable: true, width: 125, dataIndex : 'qapassdate'},
	        {id : 'qafaildate', header : "QA Fail Date", sortable: true, width: 125, dataIndex : 'qafaildate'},
	        {id : 'reshippeddate', header : "Re-Shipped Date", sortable: true, width: 125, dataIndex : 'reshippeddate'},
	        {id : 'redelivereddate', header : "Re-Delivered Date", sortable: true, width: 125, dataIndex : 'redelivereddate'},
	        {id : 'cancelleddate', header : "Cancelled Date", sortable: true, width: 125, dataIndex : 'cancelleddate'}
	    ]
	});
		
	var searchResultsStore = new Ext.data.Store({
		url: 'returnsajax.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['returnid', 'group_id', 'filter_id','shipment_id', 'itemid', 'article_number', 'product_display_name', 'productStyleName', 
	  	  	          'skucode', 'quantity', 'status', 'return_mode', 'store_return_id', 'store_id' , 'courier_service', 'couponcode', 'refundamount',
	  	  	          'login', 'name', 'pickup_address', 'createddate', 'sentfortrackingdate', 'RPI-fdate',
	  	  	          'RPU-fdate', 'RSD-fdate', 'RRC-fdate', 'qapassfdate', 'RQCF-fdate', 'RRS-fdate', 
	  	  	          'RSD-fdate', 'RRD-fdate','createdby','reason']
		}),
		sortInfo:{field : 'returnid', direction:'DESC'},
		remoteSort: true
	});

	var pagingBar = new Ext.PagingToolbar({
		pageSize : 30,
	    store: searchResultsStore,
	    displayInfo: true
	});
	
	var menuAction = function(status) {
    	var selected = searchResults.initialConfig.sm.getSelected();
		
    	Ext.Ajax.request({
			url : 'returnsajax.php',
			params : {"action" : "status_change", "returnid" : selected.data.returnid,
						"itemid" : selected.data.itemid, "status" : status,
						"login": loginUser},
		    success : function(response, action) {
				reloadGrid();
			}
		});
	}
	
	// from Return Request Queued, pickup initiated or picked up 
	var statusMenu1 = new Ext.menu.Menu({ 
		items: [ 
	        {
	        	text : 'Return Received',
	 	        tooltip : 'Return Received',
	 	        iconCls : 'add',
	 	        handler : function() {
	 	        	menuAction("RRC");
	 	        }
	        }
		] 
	});
	
	// from Return Received 
	var statusMenu2 = new Ext.menu.Menu({ 
		items: [ 
	        {
	        	text : 'Return QA Fail',
        		tooltip : 'Return QA Fail',
        		iconCls : 'add',
        		handler : function() {
        			menuAction("RQF");
        		}
	        }
		] 
	});
	
	// From QA Pass, Refund Issued - 
	var statusMenu3 = new Ext.menu.Menu({ 
		items: [ 
	        {
	        	text : 'Return Item Stocked',
		        tooltip : 'Return Item Stocked',
		        iconCls : 'add',
		        handler : function() {
		        	menuAction("RIS");
		        }
	        }, {
	        	text : 'Return Item Rejected',
		        tooltip : 'Return Item Rejected',
		        iconCls : 'add',
		        handler : function() {
		        	menuAction("RIJ");
		        }
	        }
		] 
	});
	
	// from QA Fail - 
	var statusMenu4 = new Ext.menu.Menu({
	    items: [
	    {
	        text : 'Return Re-shipped',
	        tooltip : 'Return Re-shipped',
	        iconCls : 'add',
	        handler : function() {
	        	menuAction("RRS");
	        }
        }
	]
	});
	
	// From return re-shipped state
	var statusMenu5 = new Ext.menu.Menu({
	    items: [
		{
	        text : 'Reshipped Return Delivered',
	        tooltip : 'Reshipped Return Delivered',
	        iconCls : 'add',
	        handler : function() {
	        	menuAction("RSD");
	        }
        }]
	});
	
	searchResults = new Ext.grid.EditorGridPanel({
	    store  : searchResultsStore,
	    cm : colModel,
	    id : 'searchResults-panel',
	    title : 'Search Results',
	    frame : true,
		loadMask : false,
		autoHeight : true,
		sm: sm,
		bbar : pagingBar,
		stripeRows : true,
		autoScroll : true,
		viewConfig : {
			enableRowBody : true
		},
		tbar:[],
		listeners : {
			'render' : function(comp) {
				// TODO get the focus on the orderid
				Ext.getCmp("return_id").focus();
			}
			/*,
			"rowcontextmenu" : function(grid, row, event) {
	    		grid.getSelectionModel().selectRow(row);
	    		
	    		var selected = grid.initialConfig.sm.getSelected();
	    		
	    		if ((selected.data.status == 'RPI' || selected.data.status == 'RPU') && selected.data.return_mode != "self") {
	    			event.preventDefault();
		    	    event.stopEvent();
		    	    statusMenu1.showAt(event.getXY());
	    		} else if (selected.data.status == 'RRC') {
	    			event.preventDefault();
		    	    event.stopEvent();
		    	    statusMenu2.showAt(event.getXY());
	    		} else if (selected.data.status == 'RQP') {
	    			event.preventDefault();
		    	    event.stopEvent();
		    	    statusMenu3.showAt(event.getXY());
	    		} else if (selected.data.status == 'RQF') {
	    			/*event.preventDefault();
		    	    event.stopEvent();
		    	    statusMenu4.showAt(event.getXY());
	    		} else if (selected.data.status == 'RRS') {
	    			event.preventDefault();
		    	    event.stopEvent();
		    	    statusMenu5.showAt(event.getXY());
	    		} 
	    	}*/
		}
	});
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
		     downloadPanel, searchPanel, searchResults
		],
		listeners : {
			'afterrender' : function(comp) {
				if(get_status && get_status != "") {
					searchPanel.getForm().findField("status").setValue(get_status);
				}
				if(get_login && get_login != "") {
					searchPanel.getForm().findField("login").setValue(get_login);
				}
				if(get_returnid && get_returnid != "") {
					searchPanel.getForm().findField("return_id").setValue(get_returnid);
				}
				if(get_orderid && get_orderid != "") {
					searchPanel.getForm().findField("order_id").setValue(get_orderid);
				}
			}
		}
	});
});
