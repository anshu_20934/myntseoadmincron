<?php
include_once '../auth.php';
include_once("$xcart_dir/include/security.php");
include_once("$xcart_dir/include/func/func.returns.php");

if($_SERVER['REQUEST_METHOD'] == 'POST') {
	$action = $_POST['action'];
	if($action == 'update') {
		global $weblog;
		$id = $_POST['id'];
		if(isset($_POST['sla'])) {
			$sla = $_POST['sla'];
			$query = "update mk_return_state_transitions set sla = $sla where id = $id";
			db_query($query);
		}
		$result['result'] = 'success';
		echo(json_encode($result));
	}
	if($action == 'delete'){
		$id = $_POST['id'];
		$query = "delete from mk_return_state_transitions where id = $id";
		$result['result'] = 'success';
		echo(json_encode($result));
	}
	if($action == 'add'){
		$toState = $_POST['toState'];
		$fromState = $_POST['fromState'];
		$description = $_POST['description'];
		$sla = $_POST['sla'];
		$query = "select * from mk_return_state_transitions where from_state = '$fromState' and to_state = '$toState'";
		$transition = func_query_first($query,true);
		if($transition['id'] != null){
			$query = "insert into mk_return_state_transitions (from_state,to_state,description,sla,admin_access,roles_allowed) values ('$fromState','$toState','$description',$sla,0,'AD,CS')";
			func_query($query);
			$result['result'] = 'success';
		}else{
			$result['message'] = "A transition already exists from $fromState to $toState";
			$result['result'] = 'failure';	
		}
		
		echo(json_encode($result));
	}
}
if($ajaxRequest != 'true'){
	$statuses = get_all_return_statuses();
	$statuscode2Name = array();
	foreach ($statuses as $status_row) {
		$statuscode2Name[$status_row['code']] = $status_row['display_name'];
	}
	$query = "SELECT * from mk_return_state_transitions";
	$state_transitions = func_query($query);
	$ajaxRequest = $_POST['ajaxRequest'];
		
	$query = "SELECT * from mk_return_state_transitions";
	$state_transitions = func_query($query);
	
	foreach ($state_transitions as $index=>$transition) {
		$state_transitions[$index]['from_state'] = $statuscode2Name[$transition['from_state']];
		$state_transitions[$index]['to_state'] = $statuscode2Name[$transition['to_state']];	
	}
	
	$states = func_query("select code,display_name,end_state from mk_return_status_details");
	$fromStates = array();
	foreach($states as $state){
		if($state['end_state'] != 1){
			$fromStates[$state['code']] = $state['display_name'];
		}
	}
	$toStates = array();
	foreach($states as $state){
		$tempStates = func_query("select code,display_name from mk_return_status_details where code not in (select to_state from mk_return_state_transitions where from_state = '".$state['code']."') and code != '".$state['code']."'");
		foreach($tempStates as $tempState){
			$toStates[$state['code']][$tempState['code']] = $tempState['display_name'];	
		}
	}
	$query = "select type,name from mk_roles";
	$roles = func_query($query,true);
	$smarty->assign("transitions", $state_transitions);
	$smarty->assign("fromStates", $fromStates);
	$smarty->assign("toStates", $toStates);
	$smarty->assign("main", "returns_sla_editor");
	func_display("admin/home.tpl", $smarty);
		
}	


?>