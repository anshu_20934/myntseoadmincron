<?php
require "../auth.php";
//include_once("$xcart_dir/include/security.php");
require_once $xcart_dir."/include/func/func.mkcore.php";
require_once $xcart_dir."/include/func/func.returns.php";
require_once $xcart_dir."/PHPExcel/PHPExcel.php";

x_load('db');

$data_tempalte_sql = "select reports_template,code from $sql_tbl[courier_service] where enable_status = 1 and reports_template is not null";
$data_tempalte_row = func_query($data_tempalte_sql);
$courier_templates = array();
foreach ($data_tempalte_row as $key => $value){
	$courier_templates[$value['code']] = $value['reports_template'];
}

	$data_sql = "select returnid,zipcode,country,orderid from xcart_returns where status = 'RRRS'";	
	$data_results = func_query($data_sql);
	$selected_results = array();
	$weblog->info("Results - ". count($data_results));
	if(count($data_results) > 0) {
		foreach($data_results as $row) {
			$available_couriers = func_get_serviceable_couriers($row['zipcode'],'on',$row['country']);
			if(count($available_couriers) > 0){
				$available_couriers = $available_couriers[0];
				if($courier_templates[$available_couriers] != ''){
					$return_results = func_query_first("select ".$courier_templates[$available_couriers]." from xcart_orders where orderid = ".$row['orderid']);
					$selected_results[$available_couriers][] = $return_results;
				}else{
					unset($row['zipcode']);
					unset($row['country']);
					$selected_results['NO_COURIER'][] = $row;
				}
			}else{
				unset($row['zipcode']);
				unset($row['country']);
				$selected_results['NO_COURIER'][] = $row;
			}
		}
	}
	
	$filename = "ready-to-reship-returns-".date("dmy");
	
	if($filename != "" && count($selected_results) > 0) {
		$objPHPExcel = new PHPExcel();
		$sheet_counter = 0;
		foreach ($selected_results as $key => $value){
			$weblog->info("key ". print_r($value,true));
			$objPHPExcel = func_write_data_to_excel($objPHPExcel, $value,  $sheet_counter++, "reship-$key");
		}		
		header("Content-Disposition: attachment; filename=$filename.xls");
		ob_clean();
		flush();
		try {
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->setPreCalculateFormulas(false);
			$objWriter->save('php://output');
		}catch(Exception $ex) {
			$weblog->info("Error Occured = ". $ex->getMessage());
		}
		$weblog->info("Saved");
	} else {
		echo "No returns found to be reshipped.";
	}

?>