<?php

include_once("../auth.php");
//include_once("$xcart_dir/include/security.php");
include_once("$xcart_dir/include/func/func.mkcore.php");
include_once("$xcart_dir/include/class/class.mymyntra.php");
include_once("$xcart_dir/include/func/func.returns.php");
include_once("$xcart_dir/include/class/oms/OrderExchangeManager.php");
include_once("$xcart_dir/modules/apiclient/AtpApiClientV2.php");

global $portalapilog;
if($_SERVER['REQUEST_METHOD'] == 'POST') {
	
	if($_POST['action'] == "newaddress") {
		$name = mysql_real_escape_string($_POST['name']);
		$address = mysql_real_escape_string($_POST['address']);
		$city = mysql_real_escape_string($_POST['city']);
		$state = mysql_real_escape_string($_POST['state']);
		$country = mysql_real_escape_string($_POST['country']);
		$zipcode = mysql_real_escape_string($_POST['zipcode']);
		$mobile = mysql_real_escape_string($_POST['mobile']);
		$email = mysql_real_escape_string($_POST['email']);
		$phone = mysql_real_escape_string($_POST['phone']);
		$defaultAddId = mysql_real_escape_string($_POST['defaultAddId']);
		
		if($defaultAddId && $defaultAddId != "") {
			$update_array = array('default_address'=>0);
			func_array2update("mk_customer_address", $update_array, 'id='.$defaultAddId);
		}
		
		$insertdata = array('login'=>$_POST['customer_login'], 'default_address'=>true, 'name'=>$name, 'address'=>$address, 'city'=>$city, 'state'=>$state, 'country'=>$country,
							'pincode'=>$zipcode, 'email'=>$email, 'mobile'=>$mobile, 'phone'=>$phone, 'datecreated'=>date("Y-m-d H:i:s"));
		$new_add_id = func_array2insert("mk_customer_address", $insertdata);
		
		$data = array("SUCCESS", $new_add_id);

		header('Content-type: text/x-json');
		print json_encode($data);
		exit;
	} else {
		$orderid = $_POST['orderid'];
		$itemid = $_POST['itemid'];
		$quantity = $_POST['quantity'];
		$option_sku_id = $_POST['size'];
		$option_sku_id_split = explode("-", $option_sku_id);
		$optionid = $option_sku_id_split[0];
		$sku_id = $option_sku_id_split[1];
		$customer_login = $_POST['customer_login'];
		$userid = $_POST['userid'];
		$reasoncode_displayname = $_POST['return-reason'];
		$reasoncode_displayname_split = explode("-", $reasoncode_displayname);
		$reasoncode = $reasoncode_displayname_split[0];
		$displayname = $reasoncode_displayname_split[1];
		$pickup_courier = $_POST['pickup-courier'];
		$details = mysql_real_escape_string($_POST['return-details']);
		$return_mode = $_POST['return-method'];
		$selected_address_id = $_POST['selected-address-id'];
		
		$response = OrderExchangeManager::createExchangeforOrder($orderid, $itemid, $optionid, $quantity, $sku_id, $userid, $reasoncode, $details, $selected_address_id);
		$smarty->assign("exchange_orderid", $response['exchange_orderid']);
		if($response['status'] == false && $response['not_allowed'] == true){
			$smarty->assign("errorMsg", $response['warning_message']);
		} else {
			if($response['ppsId'] != null && !empty($response['ppsId'])) {
				$smarty->assign("errorMsg", $response['message']);
				//TODO: How the UI end will be in case of PPS aware orders
			} else {
				$itemsql = "SELECT a.*, a.completion_time AS completion_time, d.name AS stylename, f.login, " .
						"f.status as status, f.delivereddate,f.login, f.queueddate AS queueddate, " .
						"a.quantity_breakup, a.assignment_time AS assignment_time, f.payment_method as paymentmethod, " .
						" sp.default_image, f.warehouseid, f.loyalty_points_conversion_factor " .
						"FROM (((xcart_order_details a JOIN mk_product_style d ON a.product_style=d.id) " .
						"LEFT JOIN mk_style_properties sp on d.id=sp.style_id) " .
						"LEFT JOIN xcart_orders f ON a.orderid=f.orderid) where a.item_status != 'IC' AND f.orderid = $response[exchange_orderid] AND a.itemid = $response[newitemid]";

				$singleitem = func_query_first($itemsql);
				if(!empty($singleitem['default_image'])) {
					// Rectangular thumbnails
					$singleitem['default_image'] = str_replace("_images", "_images_96_128", $singleitem['default_image']);
				}
				$smarty->assign("singleitem", $singleitem);
				$smarty->assign("errorMsg", $response['message']);
			}
		}
		$smarty->assign("details", $details);
		$smarty->assign("reason", $displayname);
		$smarty->assign("action", "show_summary");
	}
} else {
	$orderid = $_GET['orderid'];
	$itemid = $_GET['itemid'];
	
	$item_return_sql = "Select returnid from $sql_tbl[returns] where status != 'RRD' and itemid = $itemid";
	$return_req = func_query_first($item_return_sql);
	
	$item_exchange_sql = "select ex.exchange_orderid from exchange_order_mappings ex, xcart_orders o where o.orderid=ex.exchange_orderid 
	and ex.itemid = $itemid and o.status not in ('F', 'D', 'L') order by exchange_orderid DESC";
	$exchange_req = func_query_first($item_exchange_sql);
	if($exchange_req['exchange_orderid']){
		$exchangeRTOd = func_query_first_cell("select id from mk_old_returns_tracking where orderid = $exchange_req[exchange_orderid]");
	}

	$exchangesEnabled = FeatureGateKeyValuePairs::getBoolean('exchanges.enabled',false);
    // These values are hard-coded to ALWAYS display the exchange operations on x5 - UI
    // All the required validations will be done by OMS (PHP will also hit the OMS exchange api)
    $return_req = false;
    $exchangeRTOd = true;
	if($exchangesEnabled && (!$exchange_req || $exchangeRTOd) && !$return_req) {
		
		$itemsql = "SELECT a.*, a.completion_time AS completion_time, d.name AS stylename, f.login, " .
	           "f.status as status, f.delivereddate, f.queueddate AS queueddate, f.shippeddate, f.ordertype, " .
	           "a.quantity_breakup, a.assignment_time AS assignment_time, f.payment_method as paymentmethod, " .
	           " sp.default_image, f.warehouseid, f.loyalty_points_conversion_factor " .
	           "FROM (((xcart_order_details a JOIN mk_product_style d ON a.product_style=d.id) " .
	           "LEFT JOIN mk_style_properties sp on d.id=sp.style_id) " .
	           "LEFT JOIN xcart_orders f ON a.orderid=f.orderid) where a.item_status != 'IC' AND a.orderid = $orderid AND a.itemid = $itemid";
	
		$singleitem = func_query_first($itemsql);
		//Make a service call to OMS, get the delivered date
		$exchangeable = false;
		$errormsg = "";
		if($singleitem['delivereddate'] && $singleitem['delivereddate'] > 0) { 
			if($singleitem['delivereddate'] + 30*24*60*60 > time()) {
				$exchangeable = true;
			} 
			else{
				$exchangeable = false;
			}
			if($singleitem['is_returnable'] == 0){
				$exchangeable = false;
				$errormsg = "Points have been activated for this item.";
			}
			if(!$exchangeable) {
				$roles=$XCART_SESSION_VARS['user_roles'];
				$userRoles = array();
				foreach($roles as $role){
					$userRoles[] = $role['role'];
				}
				if(in_array('CST', $userRoles)){
					$exchangeable = true;
				} else {
					$exchangeable = false;
					$errormsg = "Item can only be exchanged within 30 days after delivery";
				}
			}
		} else {
			$errormsg = "Item can only be exchanged post delivery";
			$exchangeable = false;
		}
		
		if($exchangeable) {
		
			if(!empty($singleitem['default_image'])) {
				// Rectangular thumbnails
				$singleitem['default_image'] = str_replace("_images", "_images_96_128", $singleitem['default_image']);
			}
			
			$singleitem['totalProductPrice'] = $singleitem['price']*$singleitem['amount'];
			$singleitem['couponDiscount'] = $singleitem['coupon_disount_product'];
			$singleitem['cartDiscount'] = $singleitem['cart_discount_split_on_ratio'];
			$singleitem['totalMyntCashUsage'] = $singleitem['cash_redeemed'];
			
			$mymyntra = new MyMyntra(mysql_real_escape_string($singleitem['login']));
			
			$sql = "select * from ((mk_order_item_option_quantity moioq LEFT JOIN mk_product_options po on moioq.optionid = po.id) LEFT JOIN mk_styles_options_skus_mapping mosm ON moioq.optionid = mosm.option_id) where moioq.itemid = $itemid";
			$order_option_qty = func_query_first($sql);
			
			$optionsQty = $mymyntra->getBreakUpQuantityArray($order_option_qty['value'].":".$order_option_qty['quantity']);
			$singleitem['style_id'] = $singleitem['product_style'];
			$singleitem['sku_id'] = $order_option_qty['sku_id'];
			
			foreach ($optionsQty as $option) {
				if($option['quantity']) {
					$singleitem['option'] = $option['option'];
					$singleitem['size'] = $option['size'];
					$singleitem['quantity'] = $option['quantity'];
					break;
				}
			}
			
			$singleitem['sizes'] = array();

			$enabledNewInventory = FeatureGateKeyValuePairs::getFeatureGateValueForKey('exchange.newinventory', 'true');
			if ($enabledNewInventory == 'true') {
				$singleitem['sizes'] = OrderExchangeManager::getItemSkuDetailsFromATP($itemid);
			} else {
				$singleitem['sizes'] = OrderExchangeManager::getItemSkuDetails($itemid);
			}

			$availableInWarehouses = false;
			foreach($singleitem['sizes'] as $size) {
				if($size['is_available']) {
					$product = array('style_id'=> $singleitem['style_id'], 'sku_id'=>$size['sku_id']);
				}
				if(!empty($size['availableInWarehouses'])) {
					if(!$availableInWarehouses)
						$availableInWarehouses = explode(",", $size['availableInWarehouses']);
					else
						$availableInWarehouses = array_merge($availableInWarehouses, explode(",", $size['availableInWarehouses']));
				} 
			}
			$availableInWarehouses = array_values(array_unique($availableInWarehouses));
			
			//$order_query = "select * from xcart_orders where orderid = $orderid";
			//$order = func_query_first($order_query,true);
			$pickup_charges = WidgetKeyValuePairs::getWidgetValueForKey('returnPickupCharges');
			$selfDeliveryCredit = WidgetKeyValuePairs::getWidgetValueForKey('returnSelfDeliveryCredit');
			
			$selected_address_id = intval($_GET['address']);
			$weblog->info("Selected addressId - $selected_address_id");
			
			$address_sql = "Select * from mk_customer_address where login='".$singleitem['login']."' order by default_address DESC";
			$addresses = func_query($address_sql);
			
			$selectedAddress = false;
			$defaultAddress = false;
			foreach ($addresses as $index=>$address) {
				$addresses[$index]['state_display'] = func_get_state($address['state'], $address['country']);
				$addresses[$index]['country_display'] = func_get_country($address['country']);
				if(!empty($availableInWarehouses)) {
					$addresses[$index]['is_serviceable'] = func_check_exchange_serviceability_products($address['pincode'], array($product), $availableInWarehouses);
				} else {
					$addresses[$index]['is_serviceable'] = 0;
				}
				
				if($selected_address_id != 0 && $selected_address_id == $address['id'] ) {
					$selectedAddress = $addresses[$index];
				} 
				
				if($address['default_address'] == 1) {
					$defaultAddress = $addresses[$index];
				}
			}
			
			if($selectedAddress === false) {
				$selectedAddress = $defaultAddress;
				$selected_address_id = $defaultAddress['id'];
			}
			
			$countries = func_get_countries();
			$india_states = func_get_states('IN');
			global $weblog;
			
			$curr_ordertype = $singleitem['ordertype'];
			$exorderid = $orderid;
			$original_delivereddate = $singleitem['delivereddate'];
			$original_shippeddate = $singleitem['shippeddate'];
			while($curr_ordertype == 'ex'){
				$parent_orderid = func_query_first_cell("select releaseid from exchange_order_mappings where exchange_orderid = $exorderid", true);
				$parent_order = func_query_first("select ordertype, delivereddate, shippeddate from xcart_orders where orderid = $parent_orderid", true);
				if($parent_order['ordertype'] == 'ex'){
					$exorderid = $parent_orderid;
				} else {
					$original_delivereddate = $parent_order['delivereddate'];
					$original_shippeddate = $parent_order['shippeddate'];
					$curr_ordertype = '';
					break;
				}
			} 
			if($original_delivereddate && $original_delivereddate != 0){
				$within_30_days = ($original_delivereddate + 30*24*60*60 > time())?'true':'false';
			}else{
				$within_30_days = 'false';
			}
			if($within_30_days != 'true'){
				$return_reasons = get_all_return_reasons_after_30days();
			}else{
				$return_reasons = get_all_return_reasons(true);	
			}
			
			$smarty->assign("action", "confirm_exchange");
			$smarty->assign("orderid", $orderid);
			//$smarty->assign("order", $order);
			$smarty->assign("singleitem", $singleitem);
			$smarty->assign("states", $india_states);
			$smarty->assign("countries", $countries);
			$smarty->assign("pickupcharges", $pickup_charges);
			$smarty->assign("selfDeliveryCredit", $selfDeliveryCredit);
			$smarty->assign("selectedAddressId", $selected_address_id);
			$smarty->assign("defaultAddressId", $defaultAddress['id']);
			$smarty->assign("selectedAddress", $selectedAddress);
			$smarty->assign("allAddresses", $addresses);
			$smarty->assign("reasons", $return_reasons);
			$smarty->assign("within30days",$within_30_days);
			$smarty->assign("warehouseid", $singleitem['warehouseid']);
			$smarty->assign("exchangeable", true);
		} else {
			$smarty->assign("action", "confirm_exchange");
			$smarty->assign("errorMsg", $errormsg);
			$smarty->assign("exchangeable", false);
		}
	} else {
		$smarty->assign("action", "confirm_exchange");
		if(!$exchangesEnabled) {
			$smarty->assign("errorMsg", "Exchange feature is disabled currently. Please try again later.");
			$smarty->assign("exchangeable", false);
		} elseif($exchange_req) {
			$smarty->assign("errorMsg", "Exchange is already created for the selected item.");
			$smarty->assign("exchange_orderid", $exchange_req['exchange_orderid']);
			$smarty->assign("exchangeable", false);
		} elseif($return_req) {
			$smarty->assign("errorMsg", "Return Request is already created for the selected item.");
			$smarty->assign("exchangeable", false);
		}
	}
}

$smarty->assign("main", 'exchange_item');
func_display("admin/home.tpl",$smarty);

?>
