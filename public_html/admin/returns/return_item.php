<?php

include_once("../auth.php");
//include_once("$xcart_dir/include/security.php");
include_once("$xcart_dir/include/func/func.mkcore.php");
include_once("$xcart_dir/include/class/class.mymyntra.php");
include_once("$xcart_dir/include/func/func.returns.php");

if($_SERVER['REQUEST_METHOD'] == 'POST') {
	
	if($_POST['action'] == "newaddress") {
		$name = mysql_real_escape_string($_POST['name']);
		$address = mysql_real_escape_string($_POST['address']);
		$city = mysql_real_escape_string($_POST['city']);
		$state = mysql_real_escape_string($_POST['state']);
		$country = mysql_real_escape_string($_POST['country']);
		$zipcode = mysql_real_escape_string($_POST['zipcode']);
		$mobile = mysql_real_escape_string($_POST['mobile']);
		$email = mysql_real_escape_string($_POST['email']);
		$phone = mysql_real_escape_string($_POST['phone']);
		$defaultAddId = mysql_real_escape_string($_POST['defaultAddId']);
		
		if($defaultAddId && $defaultAddId != "") {
			$update_array = array('default_address'=>0);
			func_array2update("mk_customer_address", $update_array, 'id='.$defaultAddId);
		}
		
		$insertdata = array('login'=>$_POST['customer_login'], 'default_address'=>true, 'name'=>$name, 'address'=>$address, 'city'=>$city, 'state'=>$state, 'country'=>$country,
							'pincode'=>$zipcode, 'email'=>$email, 'mobile'=>$mobile, 'phone'=>$phone, 'datecreated'=>date("Y-m-d H:i:s"));
		$new_add_id = func_array2insert("mk_customer_address", $insertdata);
		
		$data = array("SUCCESS", $new_add_id);

		header('Content-type: text/x-json');
		print json_encode($data);
		exit;
	} else {
		$orderid = $_POST['orderid'];
		$itemid = $_POST['itemid'];
		$option = $_POST['item_option'];
		$quantity = $_POST['quantity'];
		$customer_login = $_POST['customer_login'];
		$userid = $_POST['userid'];
		$reasoncode = $_POST['return-reason'];
		$pickup_courier = $_POST['pickup-courier'];
		if(!$pickup_courier){
			$pickup_courier = $_POST['pickup-by'];
		}
		$details = mysql_real_escape_string($_POST['return-details']);
		$return_mode = $_POST['return-method'];
		$selected_address_id = $_POST['selected-address-id'];
		
		$response = func_add_new_return_request($orderid, $itemid, $option, $quantity, $customer_login, $userid, $reasoncode, $details, $return_mode, $selected_address_id,true,$pickup_courier);
			
		$smarty->assign("returnid", $response['returnid']);
		$smarty->assign("return_req", $response['return_req']);
		$smarty->assign("return_address", $response['return_address']);
		$smarty->assign("errorMsg", $response['warning_message']);
		$smarty->assign("action", "show_summary");
	}
} else {
	$orderid = $_GET['orderid'];
	$itemid = $_GET['itemid'];
	
	$item_return_sql = "Select returnid from $sql_tbl[returns] where status != 'RRD' and itemid = $itemid";
	$return_req = func_query_first($item_return_sql);
	
	if(!$return_req) {
		
		$itemsql = "SELECT a.*,msosm.sku_id, a.completion_time AS completion_time, d.name AS stylename, f.login, " .
	           "f.status as status, f.delivereddate,f.login, f.queueddate AS queueddate, f.loyalty_points_conversion_factor, " .
	           "a.quantity_breakup, a.assignment_time AS assignment_time, f.payment_method as paymentmethod, " .
	           "sp.default_image, f.warehouseid, sp.global_attr_article_type, sp.global_attr_master_category, sp.global_attr_sub_category " .
	           "FROM (((((xcart_order_details a JOIN mk_product_style d ON a.product_style=d.id) " .
	           "LEFT JOIN mk_style_properties sp on d.id=sp.style_id) " .
	           "LEFT JOIN mk_order_item_option_quantity oq on oq.itemid=a.itemid) " .
	           "LEFT JOIN mk_styles_options_skus_mapping msosm on msosm.option_id=oq.optionid) " .
	           "LEFT JOIN xcart_orders f ON a.orderid=f.orderid) where a.item_status != 'IC' AND f.orderid = $orderid AND a.itemid = $itemid";
	
		$singleitem = func_query_first($itemsql);
		$return_disabled_styles = FeatureGateKeyValuePairs::getFeatureGateValueForKey('return.disable.category');
		$return_disabled_styles = explode(",", $return_disabled_styles);
		$pickup_disabled_articletypes = explode(',',WidgetKeyValuePairs::getWidgetValueForKey('pickup.disabled.articletypes'));
		
		$non_returnable_error_msg = "";
		$returnable = false;
		$pickup_excluded = false;
		//pickup changes	
		if(in_array($singleitem['global_attr_article_type'], $pickup_disabled_articletypes)){
			$pickup_excluded = true;
		}
		
		if($singleitem['delivereddate'] && $singleitem['delivereddate'] > 0 ){
			if($singleitem['delivereddate'] + 30*24*60*60 > time()){
				
				$returnable = true;
				
			}
			else {
				$returnable = false;
				$non_returnable_error_msg = "Item can only be returned within 30 days from date of delivery.";
			}
		}else{
			$returnable = false;
			$non_returnable_error_msg = "Item can only be returned post delivery.";
		}

		if($singleitem['is_returnable'] == 0){
			$returnable = false;
			$non_returnable_error_msg = "Points have been activated for this item.";
		}
		if($returnable) {
			if(in_array($singleitem['global_attr_article_type'], $return_disabled_styles) || 
					in_array($singleitem['global_attr_master_category'], $return_disabled_styles) || 
					in_array($singleitem['global_attr_sub_category'], $return_disabled_styles)){
				$returnable = false;
				$non_returnable_error_msg = "Selected product category type cannot be returned. Please check categories which can be returned.";
			}
		}
		
		if(!$returnable) {
			$roles=$XCART_SESSION_VARS['user_roles'];
			$userRoles = array();
			foreach($roles as $role){
				$userRoles[] = $role['role'];
			}
			if(in_array('CST', $userRoles)){
				$returnable = true;
			}
		}
		
		if($returnable) {
		
			if(!empty($singleitem['default_image'])) {
				// Rectangular thumbnails
				$singleitem['default_image'] = str_replace("_images", "_images_96_128", $singleitem['default_image']);
			}
			
			$mymyntra = new MyMyntra(mysql_real_escape_string($singleitem['login']));
			
			//$sql = "select * from mk_order_item_option_quantity moioq, mk_product_options po where moioq.optionid = po.id and moioq.itemid = $itemid";
			$sql = "select * from ((mk_order_item_option_quantity moioq LEFT JOIN mk_product_options po on moioq.optionid = po.id) LEFT JOIN mk_styles_options_skus_mapping mosm ON moioq.optionid = mosm.option_id) where moioq.itemid = $itemid";
			$order_option_qty = func_query_first($sql);
			
			$optionsQty = $mymyntra->getBreakUpQuantityArray($order_option_qty['value'].":".$order_option_qty['quantity']);
			
			foreach ($optionsQty as $option) {
				if($option['quantity']) {
					$singleitem['option'] = $option['option'];
					$singleitem['size'] = $option['size'];
					$singleitem['quantity'] = $option['quantity'];
					break;
				}
			}
			
			//$order_query = "select * from xcart_orders where orderid = $orderid";
			//$order = func_query_first($order_query,true);
			$pickup_charges = WidgetKeyValuePairs::getWidgetValueForKey('returnPickupCharges');
			$selfDeliveryCredit = WidgetKeyValuePairs::getWidgetValueForKey('returnSelfDeliveryCredit');
			
			$selected_address_id = intval($_GET['address']);
			$weblog->info("Selected addressId - $selected_address_id");
			
			$address_sql = "Select * from mk_customer_address where login='".$singleitem['login']."' order by default_address DESC";
			$addresses = func_query($address_sql);
			
			$selectedAddress = false;
			$defaultAddress = false;
			foreach ($addresses as $index=>$address) {
				$addresses[$index]['state_display'] = func_get_state($address['state'], $address['country']);
				$addresses[$index]['country_display'] = func_get_country($address['country']);
				$couriers = func_get_pickup_couriers($address['pincode'], $singleitem['product_style'], $order_option_qty['sku_id']);
				if($couriers) {
					$addresses[$index]['is_serviceable'] = true;
					$addresses[$index]['courier'] = $couriers[0];
				}
				
				if($selected_address_id != 0 && $selected_address_id == $address['id'] ) {
					$selectedAddress = $addresses[$index];
				} 
				
				if($address['default_address'] == 1) {
					$defaultAddress = $addresses[$index];
				}
			}
			
			if($selectedAddress === false) {
				$selectedAddress = $defaultAddress;
				$selected_address_id = $defaultAddress['id'];
			}
			
			$return_mode = $_GET['returnmode'];
			
			$countries = func_get_countries();
			$india_states = func_get_states('IN');
			
			if($singleitem['delivereddate'] && $singleitem['delivereddate'] > 0 && 
					$singleitem['delivereddate'] + 30*24*60*60 > time()){
				$within_30_days = 'true';
			}else{
				$within_30_days = 'false';
			}
			


			if($within_30_days != 'true'){
				$return_reasons = get_all_return_reasons_after_30days();
			}else{
				$return_reasons = get_all_return_reasons(true);	
			}
			
			$smarty->assign("action", "confirm_return");
			$smarty->assign("orderid", $orderid);
			//$smarty->assign("order", $order);
			$smarty->assign("singleitem", $singleitem);
			$smarty->assign("breadCrumb",$breadCrumb);
			$smarty->assign("states", $india_states);
			$smarty->assign("countries", $countries);
			$smarty->assign("pickupcharges", $pickup_charges);
			$smarty->assign("selfDeliveryCredit", $selfDeliveryCredit);
			$smarty->assign("selectedAddressId", $selected_address_id);
			$smarty->assign("defaultAddressId", $defaultAddress['id']);
			$smarty->assign("selectedAddress", $selectedAddress);
			$smarty->assign("returnMode", $return_mode);
			$smarty->assign("allAddresses", $addresses);
			$smarty->assign("reasons", $return_reasons);
			$smarty->assign("within30days",$within_30_days);
			$smarty->assign("warehouseid", $singleitem['warehouseid']);
			$smarty->assign("returnable", true);
			$smarty->assign("pickup_excluded",$pickup_excluded);
		} else {
			$smarty->assign("action", "confirm_return");
			$smarty->assign("returnable", false);
			$smarty->assign("nonReturnableErrorMsg", $non_returnable_error_msg);
		}
	} else {
		$smarty->assign("returnable", false);
		$smarty->assign("action", "confirm_return");
		$smarty->assign("return_id", $return_req['returnid']);
	}
}

$smarty->assign("main", 'return_item');
func_display("admin/home.tpl",$smarty);

?>
