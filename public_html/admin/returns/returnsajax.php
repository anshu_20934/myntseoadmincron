<?php
require '../auth.php';
require $xcart_dir . '/include/func/func.returns.php';
require_once $xcart_dir."/PHPExcel/PHPExcel.php";

function get_filter_start_or_end_time($filterId){
		$starttime='';
		$endtime='';
		//get corresponding date limits
		if($filterId == "1"){
			$endtime = time();
			$starttime = strtotime("-2 days",time());
		}
		if($filterId == "2"){
			$endtime = strtotime("-2 days",time());
			$starttime = strtotime("-5 days",time());
		}
		if($filterId == "3"){
			$endtime = strtotime("-5 days",time());
			$starttime = strtotime("-10 days",time());
		}
		if($filterId == "4"){
			$endtime = strtotime("-10 days",time());
			$starttime = '';
		}
		return array('start'=>$starttime,'end'=>$endtime);
}


function create_search_query($request) {
	global $weblog;
	
	$weblog->info("returnsajax.php:  Create query with params - ".$request['return_id'].
	", ".$request['order_id'].", ".$request['status'].", ".$request['login']. ", ". $request['date_type'].
	", ". $request['from_date']. ", ".$request['to_date'].", ".$request['item_id'].", ".$request['filter_id']);
	
	$useAudit = false;
	$cond_params = array();


	if(!empty($request['store_return_id'])) {
		return "select SQL_CALC_FOUND_ROWS xr.*, o.group_id, o.orderid as shipment_id, xsr.store_return_id from xcart_returns xr
				INNER JOIN xcart_orders o on xr.orderid = o.orderid INNER JOIN xcart_store_returns xsr on xsr.returnid = xr.returnid
				where xsr.returnid=xr.returnid and xsr.store_return_id=".$request['store_return_id'];
	}
	if(!empty($request['store_id'])) {
		$storeSql = "select SQL_CALC_FOUND_ROWS xr.*, o.group_id, o.orderid as shipment_id, xsr.store_return_id from xcart_returns xr
				INNER JOIN xcart_orders o on xr.orderid = o.orderid INNER JOIN xcart_store_returns xsr on xsr.returnid = xr.returnid
				where xsr.returnid=xr.returnid and xsr.store_id=".$request['store_id'];
		if(!empty($request['tracking_no'])){
			$storeSql = $storeSql." and xr.tracking_no='".$request['tracking_no']."'";
		}
		return $storeSql;
	}
	if(!empty($request['return_id'])) {
		$cond_params[] = "returnid = ".$request['return_id'];
	}elseif(!empty($request['order_id'])) {
		$cond_params[] = "xr.orderid = ".$request['order_id'];
	}elseif(!empty($request['group_id'])) {
		$cond_params[] = "o.group_id = ".$request['group_id'];
	}elseif(!empty($request['item_id'])) {
		$cond_params[] = "itemid = " . $request['item_id'];
	}else{
		if(!empty($request['login'])) {
			$cond_params[] = "xr.login = '".$request['login']."'";
		}
		if(!empty($request['courier_service'])) {
			$cond_params[] = "xr.courier_service = '".$request['courier_service']."'";
		}
		if(!empty($request['tracking_no'])) {
			$cond_params[] = "tracking_no = '".$request['tracking_no']."'";
		}
		if(!empty($request['warehouseid'])){
			$cond_params[] = "xr.warehouseid = ".$request['warehouseid'];
		}
		if(!empty($request['status'])) {
			$statuses = explode(",",$request['status']);
			$status_query = '';
			
			foreach($statuses as $status) {
					if($status_query != '') { $status_query .= ' OR '; }
					$status_query .= "xr.status = '$status'";
			}
			$cond_params[] = "($status_query)";
		}
		if(!empty($request['date_type'])) {
			if(!empty($request['from_date']) || !empty($request['to_date'])) {
				$useAudit = true;
				$cond_params[] = "a.to_status = '$request[date_type]'";	
			}
			if(!empty($request['from_date'])) {
				$return_from_date_break = explode("/",$request['from_date']);
		        $return_from_date = $return_from_date_break[2]."-".$return_from_date_break[0]."-".$return_from_date_break[1];
				$cond_params[] = " a.audit_time >= unix_timestamp('$return_from_date') "; 
			}
			if(!empty($request['to_date'])) {
				$return_to_date_break = explode("/",$request['to_date']);
		        $return_to_date = $return_to_date_break[2]."-".$return_to_date_break[0]."-".$return_to_date_break[1];
				$cond_params[] = " a.audit_time <= unix_timestamp('$return_to_date') "; 
			}	
		}elseif(!empty($request['status']) && !empty($request['filter_id'])) {
			$useAudit = true;
			$cond_params[] = "a.to_status in ( '".implode("','", explode(",", $request[status]))."')";
			$startendTime = get_filter_start_or_end_time($request['filter_id']);
			$starttime = $startendTime['start'];
			$endtime = $startendTime['end'];
			if($starttime !='')
				$cond_params[] = " a.audit_time >= $starttime";
			if($endtime != '')
				$cond_params[] = " a.audit_time <= $endtime";
		}		
	}
	if($useAudit){
		$sql = "select SQL_CALC_FOUND_ROWS xr.*, o.group_id, o.orderid as shipment_id from xcart_returns xr, xcart_orders o ,mk_return_transit_details a WHERE xr.orderid = o.orderid and xr.returnid = a.returnid ";
	}else{
		$sql = "select SQL_CALC_FOUND_ROWS xr.*, o.group_id, o.orderid as shipment_id from xcart_returns xr, xcart_orders o WHERE xr.orderid = o.orderid ";	
	}
		
	if(count($cond_params) > 0) {
		$sql .= "AND ".implode(" AND ", $cond_params);
	}
	if($useAudit){
		$sql .= " group by xr.returnid ";
	}
	if(!empty($request["sort"])) {
		$sql .= " ORDER BY xr." . $request["sort"];
		$sql .= " " . ($request["dir"] == 'DESC' ? "DESC" : "ASC");
	}
	//echo $sql;
	return $sql;	
}

if ($_POST['action'] == 'search') {
	$search_sql = create_search_query($_POST);
	$start = $_POST['start'] == null ? 0 : $_POST['start'];
	$limit = $_POST['limit'] == null ? 30 : $_POST['limit'];
	$search_sql .=" LIMIT $start, $limit";
	$weblog->debug($search_sql);
	$results = func_query($search_sql, TRUE);
	$total = func_query("SELECT FOUND_ROWS() total", TRUE);
	
	$return_requests = format_return_requests_fields($results);
	
	$retarr = array("results" => $return_requests, "count" => $total[0]["total"]);

	header('Content-type: text/x-json');
	print json_encode($retarr);
}

if ($_POST['action'] == 'downloadreport') {
	
	$progresskey = $_REQUEST['progresskey'];
	$xcache = new XCache();
    $progressinfo = array('status'=>'PROCESSING');
    $xcache->store($progresskey."_progressinfo", $progressinfo, 3600);
    
	$search_sql = create_search_query($_POST);
	$results = func_query($search_sql, TRUE);
	$return_requests = format_return_requests_fields($results);
	
	$return_data = array();
	if($return_requests) {
		$xls_data = get_xls_data_from_requests($return_requests, 'report');
		$alphabets = range('A','Z');
		$file_name_suffix = date('d-m-y-H:i:s');
		$filename = 'returns-report-'.$file_name_suffix.".xls";
		$fullpath = "/admin/returns_report/".$filename;
		
		$weblog->info("Write to file - $fullpath");
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel = func_write_data_to_excel($objPHPExcel, $xls_data);
		ob_clean();
		flush();
		
		try {
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save($xcart_dir.$fullpath);
		}catch(Exception $ex) {
			$weblog->info("Error Occured = ". $ex->getMessage());
		}
		$weblog->info("Saved");
		$return_data['success'] = true;
		$return_data['file_location'] = $fullpath;
	} else {
		$return_data['success'] = false;
		$return_data['msg'] = "No data to generate return";
	}
	$return_data['status'] = 'DONE';
	$xcache->store($progresskey."_progressinfo", $return_data, 3600);

	header('Content-type: text/x-json');
	print json_encode($return_data);
}
