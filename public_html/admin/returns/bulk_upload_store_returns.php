<?php

require("../auth.php");
include_once("$xcart_dir/include/security.php");
include_once("$xcart_dir/include/class/class.mail.multiprovidermail.php");
include_once("$xcart_dir/modules/apiclient/OrderLineApiClient.php");
include_once("$xcart_dir/include/func/func.returns.php");
require_once("parsecsv.lib.php");

$action = $_REQUEST['action'];
$storeid = 2;

$uploadstatus = false;
$success_info = "";
$error_info = "";

if (empty($action)) $action = 'upload';

if ($action == 'verify') {
    if (!isset($_FILES) && isset($HTTP_POST_FILES)) $_FILES = $HTTP_POST_FILES;

    $returnInfo = "";
    $errorMsg = "";
    if (!empty($_FILES['bulk_upload_store_returns']['name'])) {
        $uploaddir = '../../bulk_upload_store_returns/';
        $extension = explode(".", basename($_FILES['bulk_upload_store_returns']['name']));
        $uploadfile = $uploaddir . "bulk_upload_store_returns" . date('d-m-Y-h-i-s') . "." . $extension[1];
        if (move_uploaded_file($_FILES['bulk_upload_store_returns']['tmp_name'], $uploadfile)) {

            $csv = new parseCSV($uploadfile);
            $returnData = $csv->data;

            $returnInfo .= "Total Number of Returns - " . $result = count($returnData) . "<br />";
            $returnInfo .= "Store Return IDs that will be created: " . "<br />";
            foreach ($returnData as $row) {
                $returnInfo .= $row['Return ID'] . ", ";
            }
        } else {
            $errorMsg .= "<br>File not uploaded, try again.. <br>";
            $action = "upload";
        }
    } else {
        $errorMsg .= "<br>No file name provided..<br>";
        $action = "upload";
    }

    if (empty($errorMsg)) {
        $uniqueId = uniqid();
        $smarty->assign('progresskey', $uniqueId);
        $smarty->assign('returnsTobeUploaded', $returnInfo);
        $smarty->assign('uploadorderFile', $uploadfile);
    } else {
        $action = 'upload';
        $smarty->assign("errorMsg", $errorMsg);
    }
} else if ($action == 'confirm') {
    $filetoread = $HTTP_POST_VARS['filetoread'];
    $progressKey = $HTTP_POST_VARS['progresskey'];
    if (file_exists($filetoread)) {

        $csv = new parseCSV($filetoread);
        $returnData = $csv->data;
        $returnsCreated = array();
        $returnsFailed = array();

        foreach ($returnData as $row) {
            $store_return_id = $row['Return ID'];
            $store_order_id = $row['Order Id'];
            $store_line_id = $row['ORDER ITEM ID'];
            $tracking_number = $row['Tracking Id'];
            $seller_id= $row['Seller Id'];
            if($seller_id=='6c759cd9b92349b7'){
                $storeid =3;
            }
            else {
                $storeid =2;
            }
           // $quantity = $row['Quantity'];
            $reason = substr($row['Sub Reason'], 0, 250);
            $description = $row['Comments'];

            if (empty($store_return_id) || empty($store_order_id) || empty($store_line_id) || empty($tracking_number)) continue;

            $orderLineResponse = OrdeLineApiClient::getByStoreLineId($store_line_id);

            if ($orderLineResponse['status'] != 'failure') {
                $orderLineDetails = $orderLineResponse['data'];

                $itemid = $orderLineDetails[0]['id'];
                $skuId = $orderLineDetails[0]['skuId'];
                $styleId = $orderLineDetails[0]['styleId'];

                $optionIdSql = "select option_id from mk_styles_options_skus_mapping som where sku_id = $skuId and style_id = $styleId";
                $results = func_query_first($optionIdSql);
                $optionId = $results['option_id'];
                db_query("update mk_order_item_option_quantity set optionid=".$optionId." where itemid=".$itemid);

                $optionDetailsSql = "select value from mk_product_options where id=$optionId";
                $optionResults = func_query_first($optionDetailsSql);
                $option = $optionResults['value'];

                $skudetails = SkuApiClient::getSkuDetails($skuId);
                $skucode = $skudetails[0]['code'];
                $orderid = $orderLineDetails[0]['orderReleaseId'];
                $quantity = 1;
                $customer_login = "engg_erp_alerts@myntra.com";
                $address['name'] = "Flipkart Returns";
                $address['address'] = "Flipkart Returns";
                $address['city'] = "Flipkart Returns";
                $address['state'] = "Flipkart Returns";
                $address['country'] = "Flipkart Returns";
                $address['pincode'] = "560100";
                $address['mobile'] = "1111111111";
                $address['email'] = "engg_erp_alerts@myntra.com";
                $address['phone'] = "1111111111";

                $return_response = func_add_new_store_return_request($orderid, $itemid, $skucode, $quantity, $option,
                $customer_login,
                    $address, $reason, $description, "bulk.upload", $tracking_number, "EKL" ,$store_return_id, $store_order_id,
                    $store_line_id, $storeid);

                $returnsCreated[] = $store_return_id;
            } else {
                $returnsFailed[] = $store_return_id;
            }
        }

        if(!empty($returnsCreated)) {
            $success_info .= "\nSuccessfully created returns: \n" . implode(",", $returnsCreated);
        }
        if(!empty($returnsFailed)) {
            $error_info .= "\nFailed to  create returns: \n" . implode(",", $returnsFailed);
        }
        /*
                $mail_details = array(
                    //"header"=>"Content-Type: text/plain",
                    "from_email"=>'admin@myntra.com',
                    "from_name"=>'OMS Admin',
                    "to" => 'nitin.gurram@myntra.com',
                    "cc" => 'nitin.gurram@mynta.com',
                    "mail_type" => MailType::CRITICAL_TXN,
                    "subject"=> "Moving items to C_R",
                    "content"=> "below returns are successfully uploaded <br/><br/><br/> $success_info <br/> below returns are failed to upload <br/><br/><br/> $error_info"
                );
                $multiPartymailer = new MultiProviderMailer($mail_details);
                $multiPartymailer->sendMail();
        */
    } else {
        $smarty->assign("errorMsg", "Uploaded file not found.");
    }
    $action = 'upload';
}
$smarty->assign("success_info", $success_info);
$smarty->assign("error_info", $error_info);
$smarty->assign("action", $action);
$smarty->assign("storeid", $storeid);
$smarty->assign("main", "bulk_upload_store_returns");
func_display("admin/home.tpl", $smarty);

?>