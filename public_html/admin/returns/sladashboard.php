<?php
include_once '../auth.php';
include_once("$xcart_dir/include/security.php");
include_once("$xcart_dir/include/func/func.returns.php");

if($_SERVER['REQUEST_METHOD'] == 'POST') {
	$action = $_POST['action'];
	if($action == 'overview') {
		$sla_data = array();
		$sla_data[] = func_get_returns_overview(2);
		$sla_data[] = func_get_returns_overview(7);
		$sla_data[] = func_get_returns_overview(30);
		$retarr = array("results" => $sla_data, 'total_count'=>3);
	} else if($action == 'pendingReturns') {
		$sla_data = array();
		$sla_data[] = func_create_pending_retuns_data(2);
		$sla_data[] = func_create_pending_retuns_data(7);
		$sla_data[] = func_create_pending_retuns_data(30);
		$retarr = array("results" => $sla_data, 'total_count'=>3);
	} else if ($action == 'slaDetails') {
		if(!empty($_POST['from_date'])) {
			$return_from_date_break = explode("/", $_POST['from_date']);
	        $return_from_date = $return_from_date_break[2]."-".$return_from_date_break[0]."-".$return_from_date_break[1];
		}
		if(!empty($_POST['to_date'])) {
			$return_to_date_break = explode("/", $_POST['to_date']);
	        $return_to_date = $return_to_date_break[2]."-".$return_to_date_break[0]."-".$return_to_date_break[1];
		}
		$sla_data = func_create_sla_compliance_data($return_from_date, $return_to_date);
		$retarr = array("results" => $sla_data, 'total_count'=>count($sla_data));
	}	
	
	header('Content-type: text/x-json');
	print json_encode($retarr);
} else {
	$smarty->assign("main","returns_sla_dashboard");
	func_display("admin/home.tpl", $smarty);
}

?>