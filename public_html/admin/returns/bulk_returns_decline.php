<?php

require("../auth.php");
include_once("$xcart_dir/include/security.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/include/class/oms/ReturnsStatusProcessor.php");
include_once("$xcart_dir/include/class/class.mail.multiprovidermail.php");
include_once("$xcart_dir/include/class/oms/EventCreationManager.php");

$processor = new ReturnsStatusProcessor();
$action = $_REQUEST['action'];
$uploadstatus = false;
$msg = "";
if (empty($action) )
    $action = 'upload';

if ($action == 'verify') {
    if(!isset($_FILES) && isset($HTTP_POST_FILES))
        $_FILES = $HTTP_POST_FILES;

    $errorMsg = "";
    if(!empty($_FILES['decline_return_orders']['name'])) {
        $uploaddir = '../../bulkreturndecline/';
        $extension = explode(".",basename($_FILES['decline_return_orders']['name']));
        $uploadfile = $uploaddir . "decline_return_orders" .date('d-m-Y-h-i-s').".".$extension[1];
        if (move_uploaded_file($_FILES['decline_return_orders']['tmp_name'], $uploadfile)) {
            $returnIds = array();
            $invalidReturnIds = array();
            $fd = fopen ($uploadfile, "r");
            while (!feof ($fd)) {
                $buffer = fgetcsv($fd, 4096);
                $returnid = trim($buffer[0]);
                if(empty($returnid))
                    continue;

                if(!isInteger($returnid)){
                    $invalidReturnIds[] = $returnid;
                    continue;
                }

                $returnIds[] = $returnid;
                if(count($returnIds) > 100) {
                    $errorMsg .= "Maximum of 100 orders can be Returns Declined using bulk uploader in single execution.<br/>";
                    break;
                }
            }
            fclose ($fd);
            if(count($invalidReturnIds) > 0)
                $errorMsg .= "Following Orders are not valid - ".implode(",", $invalidReturnIds);
        } else {
            $errorMsg .=  "<br>File not uploaded, try again.. <br>";
            $action = "upload";
        }
    } else {
        $errorMsg .=  "<br>No file name provided..<br>";
        $action = "upload";
    }

    if(empty($errorMsg)){
        $uniqueId = uniqid();
        $smarty->assign('progresskey', $uniqueId);
        $smarty->assign('returnIds', implode(", ", $returnIds));
        $smarty->assign('uploadorderFile',$uploadfile);
    } else {
        $action = 'upload';
        $smarty->assign("errorMsg", $errorMsg);
    }
} else if ($action == 'confirm') {
    $filetoread  = $HTTP_POST_VARS['filetoread'];
    $progressKey = $HTTP_POST_VARS['progresskey'];
    if(file_exists($filetoread)){
        $fd = fopen ($filetoread, "r");
        $returnIds = array();
        while (!feof ($fd)) {
            $buffer = fgetcsv($fd, 4096, ",");
            $returnid = trim($buffer[0]);
            if(empty($returnid))
                continue;
            $returnIds[] = $returnid;
        }
        fclose($fd);
	$returns = array_unique($returnIds);
	foreach($returns as $returnid){
            $pushEventsEnabled = FeatureGateKeyValuePairs::getBoolean('rms.declineevents.push.enabled', false);
            if(!$pushEventsEnabled) {
                $status_change_response = $processor->changeReturnsStatus($returnid, 'RRD', '', '', '', '', 'ADMIN_SCRIPT', 'Bulk Declining
            returns');
                $msg .= $status_change_response." : ".$returnid;
            }else {
                \EventCreationManager::pushRMSDeclineEvent($returnid);
            }
        }
        $smarty->assign("successMsg", "Declined ".count($returnIds)." Return orders successfully!");
    } else {
    	$smarty->assign("errorMsg", "Uploaded file not found.");
    }
    $action = 'upload';
}
$smarty->assign("action", $action);
$smarty->assign("main", "bulk_return_decline");
func_display("admin/home.tpl",$smarty);

?>