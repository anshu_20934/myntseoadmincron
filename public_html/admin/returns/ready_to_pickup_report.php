<?php
require "../auth.php";
//include_once("$xcart_dir/include/security.php");
require_once $xcart_dir."/include/func/func.returns.php";
require_once $xcart_dir."/include/func/func.courier.php";
require_once $xcart_dir."/PHPExcel/PHPExcel.php";

x_load('db');

$selectedCourierCode = $_GET['courier'];
$selectedCourier = false;

$pickupEnabledCouriers = get_pickup_courier_partners();
foreach($pickupEnabledCouriers as $courier) {
	if($courier['code'] == $selectedCourierCode)
		$selectedCourier = $courier;
}

if($selectedCourier) {
	$data_tempalte_sql = "select pickup_reports_template from $sql_tbl[courier_service] where code = '$selectedCourierCode'";
	$data_tempalte_row = func_query_first($data_tempalte_sql, true);

	$return_results = func_query("select ".$data_tempalte_row['pickup_reports_template']." from xcart_returns xa where status in ('RRQP', 'RPUQ2', 'RPUQ3') and courier_service = '$selectedCourierCode'");
	$weblog->info("Ready to pickup returns for courier $selectedCourierCode - ". print_r($return_results,true));	
	
	$filename =  "ready-to-pickup-report-$selectedCourierCode-".date("d-m-y H:i:s");
	
	if($filename != "" && count($return_results)>0) {
		$objPHPExcel = new PHPExcel();
		
		$objPHPExcel = func_write_data_to_excel($objPHPExcel, $return_results);
		
		header("Content-Disposition: attachment; filename=$filename.xls");
		ob_clean();
		flush();
		
		try {
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->setPreCalculateFormulas(false);
			$objWriter->save('php://output');
		}catch(Exception $ex) {
			$weblog->info("Error Occured = ". $ex->getMessage());
		}
		$weblog->info("Saved");
	} else {
		echo "No returns found to be picked up for courier $selectedCourierCode";
	}
} else {
	echo "Return pickup is not enabled for the selected courier";
}

?>