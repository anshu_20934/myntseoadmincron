<?php
require "../auth.php";
require $xcart_dir."/include/security.php";
require $xcart_dir."/include/func/func.returns.php";
require_once $xcart_dir."/PHPExcel/PHPExcel.php";
require_once $xcart_dir.'/include/class/excel_reader2.php';
require_once $xcart_dir.'/include/func/func.mk_shipment_tracking.php';
include_once("$xcart_dir/include/class/oms/ReturnsStatusProcessor.php");

$action  = $_POST['action'];
$uploadStatus = false;
if($action == '')
    $action ='upload';

$processor = new ReturnsStatusProcessor();
$weblog->info("Returns Bulk Uploader : action = ". $action);

if ($REQUEST_METHOD == "POST" && $action=='verify') {
	if(!isset($_FILES) && isset($HTTP_POST_FILES))
        $_FILES = $HTTP_POST_FILES;

    if(!empty($_FILES['returnsdetail']['name'])) {
      	$uploaddir1 = '../../bulkorderupdates/';
        $extension = explode(".",basename($_FILES['returnsdetail']['name']));
        $uploadfile1 = $uploaddir1 . "pickup-report-".date('d-m-Y-h-i-s').".".$extension[1];
        if (move_uploaded_file($_FILES['returnsdetail']['tmp_name'], $uploadfile1)) {
          	$uploadStatus = true;
        }
    }

    $errormsg = "";
    if($uploadStatus) {
    	$status = $_POST['status'];
    	$courier = $_POST['courier'];
    	$weblog->info("Update status - $status - for courier - $courier");
    	$returnListDisplay =  array();
    	if(empty($status)) {
    		$errormsg .= 'Please select status to update for Returns.<br>';
    	} else if(empty($courier)) {
    		$errormsg .= 'Please select a courier to update for Returns.<br>';
    	} else if (($status == 'RPI' || $status == 'RPI2' || $status == 'RPI3') && $courier == 'ML') {
    		$errormsg .= 'Marking Pickup Initiated is not allowed for ML courier from bulk uploader.<br>';
    	} else if($status == 'RPI' || $status == 'RPI2' || $status == 'RPI3' || $status == 'RPU' || $status == 'RPF' || $status == 'RPF2' || $status == 'RPF3') {
            $objReader = PHPExcel_IOFactory::createReader('Excel5');
            $objPHPExcel = $objReader->load($uploadfile1);
 	    	$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
        	
 	    	$returnIdCol = '';
 	    	$tokenNoCol = '';
 	    	$trackingNoCol = '';
 	    	$remarksCol = '';
 	    	
 	    	foreach (range('A', 'Z') as $alphabet) {
 	    		$cellObj = ($objPHPExcel->getActiveSheet()->getCell($alphabet.'1'));
 	    		if(trim($cellObj->getvalue()) == 'Return No') {
 	    			$returnIdCol = $alphabet;
 	    		}
 	    		if(trim($cellObj->getvalue()) == 'Token No' && $courier != 'ML') {
 	    			$tokenNoCol = $alphabet;
 	    		}
 	    		if(trim($cellObj->getvalue()) == 'Tracking No') {
 	    			$trackingNoCol = $alphabet;
 	    		}
 	    		if(trim($cellObj->getvalue()) == 'Remarks') {
 	    			$remarksCol = $alphabet;
 	    		}
 	    	}
 	    	
 	    	if(!empty($returnIdCol) && 
 	    			( (!empty($tokenNoCol) && ($status == 'RPI' || $status == 'RPI2' || $status == 'RPI3')) 
 	    			|| $status == 'RPU' || $status == 'RPF' || $status == 'RPF2' || $status == 'RPF3')) {
	 	    	$returnIdsVisited = array();
	 	    	$tokenNosVisited = array();
	 	    	$trackingNosVisited = array();
	 	    	
	 	    	for($i=2; $i<=$lastRow; $i++){//start from second row ignore heading
	   	    		$cell1Obj = ($objPHPExcel->getActiveSheet()->getCell($returnIdCol.$i));
	                
	   	    		$returnid = trim($cell1Obj->getvalue());
	                if(empty($returnid))
	            		continue;
	                
	                if($returnIdsVisited[$returnid]){
	                	$errormsg = "Row No $i - has duplicate Return Number $returnid.<br>";
	                } else {
	                	$returnIdsVisited[$returnid] = true;
		                $sql = "SELECT * from $sql_tbl[returns] where returnid = $returnid";
			        	$result = func_query_first($sql);
			        	if(!$result) {
			        		$errormsg .= "Row No $i - $returnid is no a valid Return Number.<br>";
			        	} else {
			        		if($result['return_mode'] == 'self') {
			        			$errormsg .= "Row No $i - Return Number $returnid is a self-shipped return.<br>";
			        		}
			        		
			        		if($result['courier_service'] != $courier) {
			        			$errormsg .= "Row No $i - Return Number $returnid is not assigned to courier $courier.<br>";
			        		}
			        		
		        			$allowedStatuses = get_all_transitions_for_status($result['status']);
		        			if( !in_array( $status, $allowedStatuses)) {
		        				$errormsg .= "Row No $i - Return Number $returnid cannot be marked $status from status $result[status].<br>";
		        			}
			        	}
	                }
		        	
	                $token_no = '';
	                if($status == 'RPI' || $status == 'RPI2' || $status == 'RPI3') {
		                $cell1Obj = ($objPHPExcel->getActiveSheet()->getCell($tokenNoCol.$i));
		                $token_no = trim($cell1Obj->getvalue());
		                /* if(empty($token_no)) {
		                	$errormsg .= "Row No $i - does not have token no.<br>";
	                	} */
		                
		                if(!empty($token_no) && isset($tokenNosVisited[$token_no])) {
		                	$errormsg .= "Row No $i - has duplicate token no.<br>";
	                	} else {
	                		$tokenNosVisited[$token_no] = true;
	                	}
	                }
	                if(!empty($trackingNoCol)) { 
	                    $cell1Obj = ($objPHPExcel->getActiveSheet()->getCell($trackingNoCol.$i));
	                    $trackingno = trim($cell1Obj->getvalue());
	                    if($status == 'RPU') {
	                        if(empty($trackingno) && empty($result['tracking_no'])) {
	                	    $errormsg .= "Row No $i - tracking no is missing.<br>";
	                        }
	                    } 
                	    if(isset($trackingNosVisited[$trackingno])) {
                	        $errormsg .= "Row No $i - has duplicate tracking no.<br>";
                	    }
                	    $trackingNosVisited[$trackingno] = true;
                        }

	                $cell1Obj = ($objPHPExcel->getActiveSheet()->getCell($remarksCol.$i));
	                $remarks = trim($cell1Obj->getvalue());
	                
	                $returnListDisplay[] = array('returnid'=>$returnid, 'courier'=>$courier, 'tokenNo' => $token_no, 'trackingNo' => $trackingno, 'remarks' => $remarks);
	        	}
 	    	} else {
 	    		$errormsg .= "Some of the columns are not present in the uploaded file.<br>";
 	    	}
		} else {
			$errormsg .=  "Invalid status $status to update.<br>";
		}
    } else {
        $errormsg .=  "File not uploaded, try again...";
    }
    if(!empty($errormsg )) {
    	$action = "upload";
    } else {
    	$smarty->assign('returnListDisplay',$returnListDisplay);
    }
    $smarty->assign('message',$msg);
    $smarty->assign('errormessage',$errormsg);
    $smarty->assign('courier', $courier);
    $smarty->assign('uploadreturnsFile',$uploadfile1);
    $smarty->assign('status', $status);
}

if ($REQUEST_METHOD == "POST" && $action=='confirmupload') {
	
	$filetoread  = $HTTP_POST_VARS['filetoread'];
	$updateto  = $HTTP_POST_VARS['status'];
    if(file_exists($filetoread))
    {
        $objReader = PHPExcel_IOFactory::createReader('Excel5');
        $objPHPExcel = $objReader->load($filetoread);

        $courier = $_POST['courier'];
        
    	$returnIdCol = '';
 	    $trackingNoCol = '';
 	    $tokenNoCol = '';
 	    $remarksCol = '';
 	    
 	    foreach (range('A', 'Z') as $alphabet) {
 	    	$cellObj = ($objPHPExcel->getActiveSheet()->getCell($alphabet.'1'));
 	    	if(trim($cellObj->getvalue()) == 'Return No') {
 	    		$returnIdCol = $alphabet;
 	    	}
 	    	if(trim($cellObj->getvalue()) == 'Tracking No') {
 	    		$trackingNoCol = $alphabet;
 	    	}
 	    	
 	    	if(($updateto == 'RPI' || $updateto == 'RPI2'  || $updateto == 'RPI3') && trim($cellObj->getvalue()) == 'Token No') {
 	    		$tokenNoCol = $alphabet;
 	    	}
 	    	
 	    	if(trim($cellObj->getvalue()) == 'Remarks') {
 	    		$remarksCol = $alphabet;
 	    	}
 	    	
 	    }
        
 	    $count = 0;
 	   	$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
        for($i=2; $i<=$lastRow; $i++){//start from second row ignore heading
   	    	$cell1Obj = ($objPHPExcel->getActiveSheet()->getCell($returnIdCol.$i));
            $returnid = trim($cell1Obj->getvalue());
        	
            if(empty($returnid))
            	continue;

            $token_no = '';
            $trackingno = false;
            $remarks = '';

            if(!empty($remarksCol)) {
            	$cell1Obj = ($objPHPExcel->getActiveSheet()->getCell($remarksCol.$i));
            	$remarks = trim($cell1Obj->getvalue());
            }
            
			if(!empty($trackingNoCol)) {
	            $cell1Obj = ($objPHPExcel->getActiveSheet()->getCell($trackingNoCol.$i));
	            $trackingno = trim($cell1Obj->getvalue());
	            if(empty($trackingno))
	            	$trackingno = false;
			}
            
            if($updateto == 'RPI' || $updateto == 'RPI2' || $updateto == 'RPI3') {
            	$cell1Obj = ($objPHPExcel->getActiveSheet()->getCell($tokenNoCol.$i));
            	$tokenNo = trim($cell1Obj->getvalue());
            	if(empty($tokenNo))
            		$tokenNo = '';
            	$comment = "Marking as return pick up initiated via bulk uploader. Remarks - $remarks";
            	$processor->changeReturnsStatus($returnid, $updateto, $courier, $trackingno, '', '', $login , $comment, '', NULL, '', '', $tokenNo);
            }
            
            if($updateto == 'RPU') {
            	$comment = "Marking as return picked up via bulk uploader. Remarks - $remarks";
            	$processor->changeReturnsStatus($returnid, 'RPU', $courier, $trackingno, '', '', $login , $comment);
            }
            
            if($updateto == 'RPF' ||  $updateto == 'RPF2' ||  $updateto == 'RPF3') {
            	$comment = "Marking as return pick up failed via bulk uploader. Remarks - $remarks";
            	$processor->changeReturnsStatus($returnid, $updateto, $courier, false, '', '', $login , $comment);
            }
            
	        $count++;
        }
        
        if($updateto == 'RPI' || $updateto == 'RPI2' || $updateto == 'RPI3') {
    		$msg =  "$count Return Requests are successfully moved to Pickup Initiated state.";
        } else if($updateto == 'RPU') {
        	$msg =  "$count Return Requests are successfully marked Picked Up.";
        } else if($updateto == 'RPF' || $updateto == 'RPF2' || $updateto == 'RPF3') {
        	$msg =  "$count Return Requests are successfully moved to Pickup Failed status.";
        }
    }
	$action = 'upload';
	$smarty->assign('message',$msg);
}

$couriers = get_pickup_courier_partners();
$smarty->assign('couriers',$couriers);

$smarty->assign('action',$action);
$smarty->assign("main","upload_bulk_returns");
func_display("admin/home.tpl",$smarty);

?>
