<?php
require "../auth.php";
require("$xcart_dir/include/security.php");
include_once("$xcart_dir/include/func/func.mkcore.php");
include_once("$xcart_dir/include/func/func.returns.php");
include_once("$xcart_dir/include/func/func.courier.php");
include_once("$xcart_dir/include/class/oms/ReturnsStatusProcessor.php");
include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");
include_once("$xcart_dir/modules/apiclient/LocationApiClient.php");
include_once("$xcart_dir/modules/apiclient/WarehouseApiClient.php");
x_load('db','order');

$quality_check_codes = ItemApiClient::getItemQualityCheckCodes("CUSTOMER_RETURNED");
$warehouses = WarehouseApiClient::getAllWarehouses();

$action = $_REQUEST['action'];
if(empty($action)) {
    $action = 'view';
}
global $weblog;
$returnid = $_REQUEST['id'];

$return_details = format_return_request_fields($returnid);
$order_details_query = "select o.* from xcart_orders o where o.orderid = ".$return_details['orderid'];
$order_details = func_query_first($order_details_query,true);
$order_warehouse = "N/A";
foreach ($warehouses as $warehouse) {
    if ($warehouse['id'] == $order_details['warehouseid']) {
        $order_warehouse = $warehouse['name'];
    }
}
if(!$return_details){
	if($action == 'queue_to_lms'){
		$returnarray['status'] = false;
		$returnarray['errorMsg'] = "Please enter a valid return id";
		echo json_encode($returnarray);
		exit;
	}
	$smarty->assign("errormessage", "Please enter a valid return id");
	$smarty->assign("main", "return_request_details");
	func_display("admin/home.tpl", $smarty);
	exit;	
}	

if($action == 'addNewComment'){
	$title = mysql_real_escape_string($_POST["title"]);
	$description = mysql_real_escape_string($_POST["description"]);
	add_returns_logs_usercomment($returnid, $description, $login, $title, '', '');
	
	func_header_location("return_request.php?id=$returnid&msg=Successfully added Comment");
}

if($action == 'fileupload'){
	$error = false;
	$commenttime = time();
	$login = $_POST['login'];
	for ($i = 0; $i < 5; $i++) {
		$imagefile_name = $HTTP_POST_FILES['attachment'.$i]['name'];
		if(!empty($imagefile_name)){
			$comment = $_POST['comment'.$i];
			$image_name = basename($imagefile_name);
			$imagefile_size = $HTTP_POST_FILES['attachment'.$i]['size'];
			$imagefile_tmp_name = $HTTP_POST_FILES['attachment'.$i]['tmp_name'];
			
			$data_dir = "../data/returns/$returnid";
			
			if(file_exists("$data_dir/$image_name")){
				$image_name = time().".".end(explode(".", $image_name));
			}
			$max_filesize = "10485760";
			if($imagefile_size <= 0) {
				$error = true;
				$errorMsg = "$image_name is empty";
			}
	
			if($imagefile_size > $max_filesize) {
				$error = true;
				$errorMsg = "Error: $image_name is too big. ".number_format($max_filesize) . "bytes is the limit.";
			}
			mkdir("$data_dir",0777,true);
			if(!@move_uploaded_file($imagefile_tmp_name, "$data_dir/$image_name")){
				$error = true;
				$errorMsg = "Can't copy $imagefile_name to $file_name. Please get in touch with tech team";
			}
			$insert_sql = "insert into mk_returns_comments_log (returnid,commenttype,commenttitle,addedby,description,adddate,image_url) values ($returnid,'Note','File uploaded','$login','$comment',$commenttime,'$image_name')";
			func_query($insert_sql);
		}	
	}
	if($error){
		func_header_location("return_request.php?id=$returnid&msg=$errorMsg");	
	}else{
		$fileUploadMessage = "Files uploaded successfully, Please check comments to find files.";
		func_header_location("return_request.php?id=$returnid&fileuploadmessage=$fileUploadMessage");
	}
}elseif($action == 'return_status_change') {
	$orderid = $_POST['orderid'];
	$itemid = $_POST['itemid'];
	$status = $_POST["status"];
	$courier_service = $_POST["courier_service"];
	if($courier_service == "other") {
		$courier_service = mysql_real_escape_string($_POST["courier_service_custom"]);
	}
	$tracking_no = $_POST["trackingno"];
	$user_comment = $_POST['comment'];
	$itemBarCodes = trim($_POST['item_barcodes_list']);
	//$qapassquality = $_POST['qapass-quality'];
	$qcReasonId = $_POST['qc-reason'];
	if($qcReasonId && !empty($qcReasonId)) {
		$qcReason = $quality_check_codes[$qcReasonId];
	} else {
		$qcReason = false;
	}
	//$validated_reasoncode = $_POST['accurate-reason'];
	$deliveryCenterCode = $_POST['dc-code'];
	if($deliveryCenterCode == ''){
		$deliveryCenterCode = $_POST['dc_code'];
	}
	$shipped_items = $_POST['shipped_items'];
	$skip_validation = $_POST['skip_validation'];
	if($skip_validation == 'yes'){
		$user_comment .= "<br/>  Skipping item barcode to sku id validation"; 	
	}
	$warehouseid = $_POST["warehouseid"];
	if($warehouseid == ''){
		$warehouseid = $_POST["warehouse_id"];
	}
	$processor = new ReturnsStatusProcessor();
	$status_change_response = $processor->changeReturnsStatus($returnid, $status, $courier_service, $tracking_no, $itemBarCodes, $qcReason, $login,$user_comment,$shipped_items, $warehouseid, $deliveryCenterCode, $validated_reasoncode);
	if($status_change_response['status']){
		$errorMsg = 'Status changed successfully';
		func_header_location("return_request.php?id=$returnid&msg=$errorMsg");
	}else{
		$errorMsg = $status_change_response['reason'];
		func_header_location("return_request.php?id=$returnid&msg=$errorMsg");	
	}
}elseif($action == 'validateitembarcodes'){ 
	$barcodes = $_POST['barcodes'];
	$orderid = $_POST['orderid'];
	$itemid = $_POST['itemid'];
	$processor = new ReturnsStatusProcessor();
	$response = $processor->validateItemBarCodes($barcodes, $orderid, $itemid);
	echo json_encode($response);
	exit;
}elseif($action == 'queue_to_lms'){
	if($return_details['return_mode'] == 'pickup'){
		$to_status = 'RPI3';
                if($return_details['status'] == 'RPUQ2'){
                    $to_status = 'RPI2';
                }
 		if($return_details['status'] == 'RRQP'){
 			$to_status = 'RPI';
 		}
		$push_response = push_return_to_lms($returnid, $return_details['zipcode'], $return_details['courier_service'], $to_status, $login);
		if(!$push_response['status']){
			$warning_msg = $push_response['reason'];
		}
		$returnarray = array();
		if(!$warning_msg){
			$returnarray['status'] = true;
			include_once("$xcart_dir/env/Host.config.php");
			$http_location = HostConfig::$httpHost;
			$returnarray['url'] = "$http_location/admin/returns/return_request.php?id=$returnid";
		}else{
			$returnarray['status'] = false;
			$returnarray['errorMsg'] = $warning_msg;
		}
		echo json_encode($returnarray);
		exit;
	}else{
		echo json_encode(array('status'=>true));
		exit;
	}
}else {
	$allowed_status = get_allowed_status_transitions($return_details['status'],$login,false);
	$deliveryCenters = json_decode(trim(WidgetKeyValuePairs::getWidgetValueForKey('selfship.dccodes')), true);
	$couriers = get_supported_courier_partners();
	$comments = get_all_return_logs($returnid);
	$return_reasons = get_all_return_reasons();
	$message = $_GET['msg'];
	$fileuploadmessage = $_GET['fileuploadmessage'];
	if($return_details['orderid']){
		$coreItemDetails = ItemApiClient::get_skuitems_for_order($return_details['orderid']);	
	}
	$skuItems = array();
	$skuCodeToItemsCount = array();
	$van_query = "select sp.article_number AS article_number, xod.supply_type AS supply_type from xcart_order_details xod LEFT JOIN mk_style_properties sp on (sp.style_id = xod.product_style) where xod.itemid = ".$return_details['itemid'];
	$van_details = func_query_first($van_query,true);
        $supply_type = $van_details['supply_type'];
       /*
        if ($supply_type == "JUST_IN_TIME") {
            foreach($quality_check_codes as $qualityId => $quality)
            {
                if ($quality['quality'] == 'Q1') {
                    unset($quality_check_codes[$qualityId]);
                }
            }
        }
       */
        $return_details['supply_type'] = $supply_type;
        $return_details['article_number'] = $van_details['article_number'];
	$van_details = $van_details['article_number'];
	if(array($coreItemDetails[0])){
		$itemBarCodeNumber = 0;
		foreach($coreItemDetails as $item) {
			if($item['sku']['vendorArticleNo'] != '' && $item['sku']['vendorArticleNo'] == $van_details){
				if($itemBarCodeNumber == 0){
					$return_details['itemBarCode'] = $item['barcode'];	
				}else{
					$return_details['itemBarCode'] = $return_details['itemBarCode'].",". $item['barcode'];
				}
				$itemBarCodeNumber++;
			}
		}	
	}else{
		if($coreItemDetails['sku']['vendorArticleNo'] != '' && $coreItemDetails['sku']['vendorArticleNo'] == $van_details){
			$return_details['itemBarCode'] = $coreItemDetails['barcode'];
			break;			
		}
	}
	
	$audit_sql = "select b.*,s.display_name as to_name from (select a.returnid,a.created_by,audit_time,s.display_name as from_name,a.to_status from mk_return_transit_details a left outer join mk_return_status_details s on a.from_status = s.code where a.returnid = $returnid) b,mk_return_status_details s where b.to_status = s.code";

	$audits = func_query($audit_sql,true);
	foreach($audits as $index=>$audit) {
		$audits[$index]['aud_time'] = date('d-m-Y H:i:s', $audit['audit_time']);
	}
	
	$smarty->assign("audit", $audits);
	$smarty->assign("warehouses", $warehouses);
	$smarty->assign("dccodes", $deliveryCenters);
	$smarty->assign("errormessage", $message);
	$smarty->assign("fileuploadmessage", $fileuploadmessage);
	$smarty->assign("return_details", $return_details);
	$smarty->assign("comments_log", $comments);
	$smarty->assign("statuses", $allowed_status);
	$smarty->assign("couriers", $couriers);
	$smarty->assign("reasons", $return_reasons);
	$smarty->assign("return_mode", $return_details['return_mode']);
	$smarty->assign("returnid", $returnid);
	$smarty->assign("quality_check_codes", $quality_check_codes);
	$smarty->assign("main", "return_request_details");
        $smarty->assign("order_warehouse", $order_warehouse);
	func_display("admin/home.tpl", $smarty);
}

?>
