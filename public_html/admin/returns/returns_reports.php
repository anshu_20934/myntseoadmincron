<?php
include_once '../auth.php';
include_once("$xcart_dir/include/security.php");
$report = $_GET['report'];
if($report != ''){
	switch ($report) {
		case "pin_codes":
				$sql = "select zipcode,count(1) as 'no of returns' from xcart_returns group by zipcode order by count(1) desc limit 20";
		break;
		case "conversion_rate":
				$sql = "select count(1) as 'Total no of returns',ifnull(sum(case when receiveddate is not null then 1 else 0 end),0) as 'Recieved at warehouse' from xcart_returns";
		break;
		case "contribution_ratio":
				$sql = "select reason as 'Reason',count(1) as 'No of Returns' from xcart_returns group by reason";
		break;
		case "avg_received_time":
				$sql = "select avg(receiveddate - createddate)/3600000 as 'Average Time to reach WH' from xcart_returns where receiveddate is not null";
		break;
		case "avg_processing_time":
				$sql = "select avg(case when restockeddate is not null then restockeddate - createddate else reshippeddate - createddate end)/3600000 as 'Average Time to process' from xcart_returns where (reshippeddate is not null or restockeddate is not null)";
		break;
		case "qa_pass_rate":
				$sql = "select count(1) as 'Returns Recieved at WH', sum(case when qapassdate is not null then 1 else 0 end) as 'QA pass items' from xcart_returns where receiveddate is not null";
		break;
		case "brand_wise_breakup":
			$sql = "select msp.global_attr_brand as 'Brand',count(1) as 'No of Returns' from xcart_returns rt, xcart_order_details xod,mk_style_properties msp where rt.itemid = xod.itemid and xod.product_style = msp.style_id group by msp.global_attr_brand";
		break;
		case "category_wise_breakup":
			$sql = "select mcc.typename as 'Category',count(1) as 'No of Returns' from xcart_returns rt, xcart_order_details xod,mk_style_properties msp,mk_catalogue_classification mcc where rt.itemid = xod.itemid and xod.product_style = msp.style_id and msp.global_attr_sub_category = mcc.id group by typename";
		break;
	}	
	$data = func_query($sql);
	global $weblog;
	if(count($data[0]) > 0){
		$smarty->assign("headers", array_keys($data[0]));	
		$smarty->assign("data",$data);
		$html = func_display("admin/main/returnsreportsajax.tpl",$smarty,false);
	    echo $html;
	    exit;	
	}else{
		echo "<b>No results found!!</b>";
		exit;
	}
}else{
	$smarty->assign("main", "returns_reports");
	func_display("admin/home.tpl", $smarty);
}
?>