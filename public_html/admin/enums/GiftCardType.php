<?php
/*
 * Created on Sep 23, 2012
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 class GiftCardType {
 	
 	private static $enumValues = array('Email', 'Pdf');
 	
 	public static function getEnumNameForValue($searchKey) {
 		foreach(self::$enumValues as $key=>$name) {
 			if($key == $searchKey)
 				return $name;		
 		}
 		return $searchKey;
 	}
 	
 }
?>
