<?php
/*
 * Created on Sep 23, 2012
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 class GiftCardStatus {
 	
 	private static $enumValues = array('Undelivered', 'Active', 'InActive', 'Activated');
 	
 	public static function getEnumNameForValue($searchKey) {
 		foreach(self::$enumValues as $key=>$name) {
 			if($key == $searchKey)
 				return $name;		
 		}
 		return $searchKey;
 	}
 	
 }
 
?>