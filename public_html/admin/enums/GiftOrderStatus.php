<?php
/*
 * Created on Sep 23, 2012
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 class GiftOrderStatus {
 	
 	private static $enumValues = array('Pre Processed', 'Complete');
 	
 	public static function getEnumNameForValue($searchKey) {
 		foreach(self::$enumValues as $key=>$name) {
 			if($key == $searchKey)
 				return $name;		
 		}
 		return $searchKey;
 	}
 }
 
?>