<?php 
include_once "auth.php";
include_once "$xcart_dir/include/security.php";
include_once "$xcart_dir/include/func/func.order.php";
include_once "$xcart_dir/PHPExcel/PHPExel.php";
$action  = $HTTP_POST_VARS['action'];
global $weblog;
$uploadStatus = false;
if(!$action || $action == '')
    $action ='upload';

if ($REQUEST_METHOD == "POST" && $action=='verify') {
	if(!isset($_FILES) && isset($HTTP_POST_FILES))
    	$_FILES = $HTTP_POST_FILES;

    $file_extn = '';
    if(!empty($_FILES['orderidsfile']['name'])) {
            $extension = explode(".",basename($_FILES['orderidsfile']['name']));
            $file_extn = $extension[1];
           	$uploadStatus = true;
    }
    $progressKey = $HTTP_POST_VARS['progresskey'];
    
    if($uploadStatus)
    {    
	    if(!$errormsg || $errormsg == "") {
	    	
	    	if($file_extn == 'xlsx') {
				$objReader = PHPExcel_IOFactory::createReader('Excel2007');
			} else {
				$objReader = PHPExcel_IOFactory::createReader('Excel5');
			}
			
			$objPHPExcel = $objReader->load($_FILES['orderidsfile']['tmp_name']);
	 		$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
	 		$comments = array();
	 		$commenttitle = "Comments updated by bulk-upload feature";
	 		for($i=2; $i<=$lastRow; $i++){//start from second row ignore heading
		   	    $cellObj1 = ($objPHPExcel->getActiveSheet()->getCell('A'.$i));
		        $cellObj2 = ($objPHPExcel->getActiveSheet()->getCell('B'.$i));
		   	    $orderid = trim($cellObj1->getvalue());
		        $commenttext = trim($cellObj2->getvalue());
		        
		        if(trim($orderid) != ""){
		        	$comments[$orderid] = $commenttext;
		        }
	 		}
	 		
			if(empty($comments)) {
	 			$errormsg = "No orderids present in the file. Please check the file and upload again.";
	 		} else {
	 			$xcache = new XCache();
	        	$progressinfo = array('total'=>count($comments), 'completed'=>0);
	        	$xcache->store($progressKey."_progressinfo", $progressinfo, 86400);
	        	$notUpdated = array();
	        	foreach ($comments as $orderid=>$comment) {
	        		if($comment != "")
	        		    func_addComment_order($orderid, $_POST['login'], 'Note', $commenttitle, $comment);
	        		else $notUpdated[] = $orderid; 
	        	}
	 			$msg = "Added comment for ".(count($comments) - count($notUpdated))." orders successfully.";
	 			if(count($notUpdated) > 0){
	 			    $errormsg = "Following orderids were not updated because of no comment provided - " ;
	 			    $errormsg .= implode(" ", $notUpdated);
	 			}
	 		}
	    }
    } else {
    	$errormsg = "Not able to upload file. Please try again";
    }
    $smarty->assign("progresskey", $progressKey);
    $smarty->assign("message", $msg);
    $smarty->assign("errormessage", $errormsg);
}    
    
if($REQUEST_METHOD == "GET") {
	$uniqueId = uniqid();
	$smarty->assign('progresskey', $uniqueId);  
}

$smarty->assign("action", $action);
$smarty->assign("main", "bulk_orders_comments");
func_display("admin/home.tpl", $smarty);

?>