<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.order.php";
include_once($xcart_dir."/modules/apiclient/WarehouseApiClient.php");

if($REQUEST_METHOD == "POST") {
	$action = $_POST['action'];
	
	if($action == 'search') {
		$retarr = func_get_shipped_orders($_POST);
		header('Content-type: text/x-json');
		print json_encode($retarr);	
	}
} else {
	$warehouse_results = WarehouseApiClient::getAllWarehouses();
	$warehouses = array();
	foreach($warehouse_results as $row) {
		$warehouses[] = array('id'=>$row['id'], 'display_name'=>$row['name']);
	}
	$smarty->assign("warehouses", json_encode($warehouses));
	$smarty->assign("main", "shipped_orders");
	func_display("admin/home.tpl", $smarty);
}
?>