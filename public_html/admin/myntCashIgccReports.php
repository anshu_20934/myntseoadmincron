<?php
//help from upload_payment_completed_orders.php
require "./auth.php";
require $xcart_dir."/include/security.php";
require_once "user_tagging_activation_function.php";



if($REQUEST_METHOD =="POST"){
	header('Content-type: text/x-json');
	
	if($action=='tGwSearch'){
		
		$qry = "select tl.* , ac.login as login, cs.firstname as firstname,cs.lastname as lastname,cs.mobile as mobile  from cashback_transaction_log as tl, cashback_account as ac , xcart_customers as cs
				where tl.cashback_account_id = ac.id and cs.login = ac.login and modified_on > from_unixtime($startTime) and modified_on < from_unixtime($endTime) and 
				business_process in ('REFUND','CASHBACK_REVERSAL','FRAUD_REVERSAL','GOOD_WILL','PRODUCT_MISMATCH','MISSING_ITEM','EXTRA_COURIER_CHARGES_REFUND','DELIVERY_DISPUTE_REFUND','MRP_MISMATCH')
								order by modified_on desc";
		
		$result = func_query($qry,true);
		
		$start = $_POST['start'] == null ? 0 : $_POST['start'];
		$limit = $_POST['limit'] == null ? 30 : $_POST['limit'];
		
		$count = sizeof($result);
		if($limit!=0){
			$result = array_slice($result,$start,$limit);
		}
		
		print json_encode(array("results" => $result, "count" =>$count ));
	}elseif($action == 'agentWiseGwSearch'){
		$qry = "select modified_by , sum(credit_inflow) as credit_inflow,count(*) as no_of_credits,count(distinct ac.login) as no_of_cms from cashback_transaction_log as tl, cashback_account as ac
		where tl.cashback_account_id = ac.id and   modified_on > from_unixtime($startTime) and modified_on < from_unixtime($endTime) and
		business_process in ('REFUND','CASHBACK_REVERSAL','FRAUD_REVERSAL','GOOD_WILL','PRODUCT_MISMATCH','MISSING_ITEM','EXTRA_COURIER_CHARGES_REFUND','DELIVERY_DISPUTE_REFUND','MRP_MISMATCH')
		group by tl.modified_by" ;
		
		$result = func_query($qry,true);
		
		
		print json_encode(array("results"=>$result));
	}elseif($action == 'agentWiseGwLimitsUsage'){
		$logins = explode(",",$userLogins);
		$logins = array_map(trim, $logins);
		$result = array();
		//$i = 0; 
		foreach($logins as $loginID){
			$dayAmount = getManualCreditInFlowForUserWithCreditCount($loginID,1); 
			$weekAmount = getManualCreditInFlowForUserWithCreditCount($loginID,7);
			$monthAmount = getManualCreditInFlowForUserWithCreditCount($loginID,30);
				
			$limits = WidgetKeyValuePairs::getWidgetValueForKey("myntCashAdminInflowLimits");
			$limits = explode(",",$limits);
			
			$result[] = array("login"=>$loginID,
							"dayAmount"=>$dayAmount["amount"],
							"dayCount"=>$dayAmount["noOfCredits"],
							"dayRemaining"=>($limits[0]-$dayAmount["amount"]),
							"weekAmount"=>$weekAmount["amount"],
							"weekRemaining"=>($limits[1]-$weekAmount["amount"]),
							"weekCount"=>$weekAmount["noOfCredits"],
							"monthAmount"=>$monthAmount["amount"],
							"monthRemaining"=>($limits[2]-$monthAmount["amount"]),
							"monthCount"=>$monthAmount["noOfCredits"],
							"dayLimit"=>$limits[0],
							"weekLimit" => $limits[1],
							"monthLimit" => $limits[2]
						);
			//$i++;
			//if($i==10)break;//limiting query for only 10 users at a time
		}
		print json_encode(array("results"=>$result));
		
	}elseif($action == "coustomerWiseGW"){
		$logins = explode(",",$userLogins);
		$logins = array_map(trim, $logins);
		$loginString = implode("','", $logins);
		$result = array();
			
		$qry = "select sum(credit_inflow) as amount, ac.login as login,count(*) as no_of_credits from cashback_transaction_log as tl, cashback_account as ac
			where tl.cashback_account_id = ac.id and  
			business_process in ('REFUND','CASHBACK_REVERSAL','FRAUD_REVERSAL','GOOD_WILL','PRODUCT_MISMATCH','MISSING_ITEM','EXTRA_COURIER_CHARGES_REFUND','DELIVERY_DISPUTE_REFUND','MRP_MISMATCH')
			and login in ('$loginString')  group by login";
	
		$result = func_query($qry,true);
		print json_encode(array("results"=>$result));
	}elseif($action == "reasonWiseGW"){
		$qry = "select sum(credit_inflow) as credit_inflow, sum(credit_outflow) as credit_outflow, count(*) as no_of_credits,business_process from cashback_transaction_log as tl
		where  modified_on > from_unixtime($startTime) and modified_on < from_unixtime($endTime) and
		business_process in ('REFUND','CASHBACK_REVERSAL','FRAUD_REVERSAL','GOOD_WILL','PRODUCT_MISMATCH','MISSING_ITEM','EXTRA_COURIER_CHARGES_REFUND','DELIVERY_DISPUTE_REFUND','MRP_MISMATCH')
		group by business_process order by credit_inflow desc";
		
		$result = func_query($qry,true);
		print json_encode(array("results"=>$result));
	}elseif($action == "goodwillReasonWiseGW"){
		$qry = "select sum(credit_inflow) as credit_inflow, sum(credit_outflow) as credit_outflow, count(*) as no_of_credits,goodwill_reason from cashback_transaction_log as tl
		where  modified_on > from_unixtime($startTime) and modified_on < from_unixtime($endTime) and business_process = 'GOOD_WILL' group by goodwill_reason order by credit_inflow desc";
		
		$result = func_query($qry,true);
		print json_encode(array("results"=>$result));
	}elseif($action == "orderLookup"){
		
		$qry=" select xo.orderid,xo.group_id,sp.product_display_name,xd.quantity_breakup as size,
				xd.item_status,sp.global_attr_gender, from_unixtime(xo.date) as order_time,from_unixtime(xd.canceltime) as cancel_time
				,from_unixtime(xo.shippeddate) as shipped_time,from_unixtime(xo.delivereddate) as delivery_time,from_unixtime(xr.createddate) as returnfiled_date,
				from_unixtime(xr.refundeddate) as refunded_date,xd.amount as quantity,
				(xd.price*xd.amount-xd.discount-cart_discount_split_on_ratio - xd.coupon_discount_product) as paid_amount
				 from
				xcart_orders as xo
				left join 
				xcart_order_details as xd on xo.orderid = xd.orderid
				left join 
				mk_style_properties as sp on xd.product_style =  sp.style_id
				left join 
				xcart_returns as xr on xo.orderid = xr.orderid
				
				where xo.orderid=$orderid 	";
		$result = func_query($qry,true);
		print json_encode(array("results"=>$result));
	}
	else{
		print json_encode(array($_POST));
	}
}
else {
	$smarty->assign("main",'myntcash_IGCC_repots');
	func_display("admin/home.tpl",$smarty);
}

function getManualCreditInFlowForUserWithCreditCount($login,$days){
	$DAY_IN_S =  60 * 60 * 24;
	$startTime = time() - ($days * $DAY_IN_S);
	$startDate = date('Y-m-d H:i:s',$startTime);
	$endDate = date('Y-m-d H:i:s',time());
	
	$qry = "select sum(credit_inflow) as credit_inflow ,  count(*) as no_of_credits from cashback_transaction_log as tl
		where tl.modified_by = '".$login."' and tl.modified_on between '$startDate' and '$endDate' ";

		
	$amount = func_query_first($qry);
	if(!empty($amount)){
		if($amount["credit_inflow"] == null || !isset($amount["credit_inflow"])){
			$amount["credit_inflow"]=0.0;
			$amount["no_of_credits"]=0;
		}
		return array("amount"=>$amount["credit_inflow"],"noOfCredits"=>$amount["no_of_credits"]);
	}else{
		return array("amount"=>0,"noOfCredits"=>0);
	}
}


?>
