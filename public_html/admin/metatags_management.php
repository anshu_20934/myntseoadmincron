<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: manufacturers.php,v 1.6 2006/01/11 06:55:57 mclap Exp $
#

define("IS_MULTILANGUAGE", true);
define('USE_TRUSTED_POST_VARIABLES',1);
$trusted_post_variables = array("descr");

require "./auth.php";
require $xcart_dir."/include/security.php";
if(empty($pageid))$pageid = '';

// how many rows to show per page
$rowsPerPage =15;
// by default we show first page
$pageNum = 1;
// if $_GET['page'] defined, use it as page number
if(isset($_GET['page']))
{
    $pageNum = $_GET['page'];
}
// counting the offset
$offset = ($pageNum - 1) * $rowsPerPage;
/********************************************************/

if ($REQUEST_METHOD == "POST" ) 
{
	 if($mode == 'save')
	 {
	 	$query_data = array(
        "pageurl" => $pagename,
		"keywords" => $keywords,
		"title" => $title,
		"description" => $metadesc,
		"qualifier" => $qualifier,
		"tagtype" =>$tagtype,
		"query_identifier" => $query_identifier,
		"qualifier_pos" => $qualifier_pos,
		"external_content" =>mysql_escape_string($extContent),
		"comments" => ($comments),
		"rssfeeds" =>mysql_escape_string($rssfeed),
		 "h1tag" => mysql_escape_string($h1tag), 
		 "h2tag" => mysql_escape_string($h2tag),
		 "relatedkeywords" =>mysql_escape_string($relatedkeywords),
		 "imagesource" => mysql_escape_string($imagesource)
        	        
	     );
		
	  $last_pageid = func_array2insert(
					"seo",$query_data );
		
		foreach($HTTP_POST_VARS['hidAppendTag'] as $_k => $_v) {
			 if(!empty($_v)){
			 	 $checked = false;
	    	      foreach($HTTP_POST_VARS['chkAppendTag'] as $k => $v){
	    	   	    if($v == $_v){
	    	   	    	$checked = true; break;
	    	   	    }
	    	   	    
	    	      }
				  $query_data = array(
		        		 "pageid" => $last_pageid,
						 "paramid" => $_v,
						 "isoverride" => ($checked) ? "1" : "0"
						);
			      //func_array2insert("mk_url_param_override_map",$query_data);	
			 }
			
		}
					
					
	  $top_message["content"] = "Meta tag information updated .";
	  func_header_location("metatags_management.php");					
	 }
	 elseif($mode == "update")
	 {
	 	$id= intval($pageid);
        $query_data = array(
        "pageurl" => $pagename,
		"keywords" => $keywords,
		"title" => $title,
		"description" => $metadesc,
		"qualifier" => $qualifier,
		"tagtype" =>$tagtype,
		"query_identifier" => $query_identifier,
		"qualifier_pos" => $qualifier_pos,
		"external_content" =>mysql_escape_string($extContent),
		"comments" => ($comments),
		"rssfeeds" =>mysql_escape_string($rssfeed),
		 "h1tag" => mysql_escape_string($h1tag), 
		 "h2tag" => mysql_escape_string($h2tag),
		 "relatedkeywords" =>mysql_escape_string($relatedkeywords),
		 "imagesource" => mysql_escape_string($imagesource)
        	        
	    );
		func_array2update("seo",
			      $query_data , "id='$id'");
	      
			      
	    foreach($HTTP_POST_VARS['hidAppendTag'] as $_k => $_v) {
	    	 if(!empty($_v)){
	    	  $checked = false;
	    	   foreach($HTTP_POST_VARS['chkAppendTag'] as $k => $v){
	    	   	    if($v == $_v){
	    	   	    	$checked = true; break;
	    	   	    }
	    	   	    
	    	   }
	    	  	
	    	   $sql = "SELECT pageid FROM mk_url_param_override_map WHERE pageid = '".$id."' AND 
	    	          paramid ='".$_v."' ";
	    	   if(db_num_rows(db_query($sql)) == 0){	
		    	   $query_data = array(
	        		 "pageid" => $id,
					 "paramid" => $_v,
					 "isoverride" => ($checked)? "1" : "0"
					);
		    	   //func_array2insert("mk_url_param_override_map",$query_data);	
	    	  }else{
	    	  	 $query_data = array(
	        		 "isoverride" => ($checked)? "1" : "0"
				 );
		    	 //func_array2update("mk_url_param_override_map",$query_data,"pageid ='".$id."' AND paramid ='".$_v."' ");	
	    	  	
	    	  }
	    	 }
	    	
	    }
			      
		
		      
		$top_message["content"] = "Meta tag information updated .";	  
		func_header_location("metatags_management.php");    
	 }
	 else if($mode == "modify")
	 {
	 	if (is_array($posted_data)) {
				 foreach ($posted_data as $pid=>$v) {
					if (empty($v["to_delete"]))
							continue;
				  	$pageid = $pid;
				  }
		 }
	 }
	 elseif($mode == 'delete')
	 {
	 	
	 	if (is_array($posted_data)) {
				 foreach ($posted_data as $pid=>$v) {
					if (empty($v["to_delete"]))
							continue;
				  	db_query ("DELETE FROM $sql_tbl[seo] WHERE id='$pid'");
					$top_message["content"] = func_get_langvar_by_name("msg_adm_prdstyle_opt_del");
				  }
		 }
	 
	 }
	 
	
	
}
## Get the seo params detail
$seoParams = array();
$sql = "SELECT * FROM mk_seo_params order by id";
$rsSeoParams = db_query($sql);
while($rsRows = db_fetch_array($rsSeoParams)){
	$seoParams[$rsRows['name']] = $rsRows['id'];
}
$smarty->assign("seoParams",$seoParams);

if ($_GET['mode'] == "add" ||  $mode == "modify")
{
   if($mode == "modify")	
   { 
		$query = "SELECT  * FROM $sql_tbl[seo] WHERE id= '".$pageid."' "; 
		$rsPage = db_query($query);
		$rsPageList = db_fetch_array($rsPage);
		$smarty->assign("pageid",$pageid);
		/*$overrideParams = array();
		$sql = "SELECT om.paramid, p.name FROM mk_url_param_override_map om INNER JOIN mk_seo_params p 
		ON p.id = om.paramid WHERE om.pageid = '".intval($pageid)."' AND om.isoverride = 1 ";
		$rsOverRideParams = db_query($sql);
		while($rsRows = db_fetch_array($rsOverRideParams)){
			$overrideParams[$rsRows['name']] = $rsRows['paramid'];
		}
		$smarty->assign("overrideParams",$overrideParams);*/


		
   }
	
}
else
{
	$query = "SELECT  * FROM $sql_tbl[seo] LIMIT $offset , $rowsPerPage";
	$rsPageList = func_query($query);
	$tagsnum = func_query ("SELECT count($sql_tbl[seo].id) as ctr from $sql_tbl[seo] ");
	$numrows = $tagsnum[0]['ctr'];
	$maxPage = ceil($numrows/$rowsPerPage);
}

$self = "metatags_management.php";

if ($pageNum > 1)
{
   $page  = $pageNum - 1;
   $prev  = "<a href=\"$self?page=$page\">Previous</a> ";
  
}
else
{
   $prev  = 'Previous'; // we're on page one, don't print previous link
   
}

if ($pageNum < $maxPage)
{
   $page = $pageNum + 1;
   $next = " <a href=\"$self?page=$page\">Next</a>  ";
 
}
else
{
   $next = 'Next'; // we're on the last page, don't print next link
    // nor the last page link
}
$smarty->assign("page",$pageNum);
$smarty->assign("previous",$prev);
$smarty->assign("next",$next);


$smarty->assign("pagelist",$rsPageList);
$smarty->assign("main","metatags");

# Assign the current location line
$smarty->assign("location", $location);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);

?>
