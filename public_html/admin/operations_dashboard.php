<?php
require './auth.php';
require $xcart_dir.'/include/security.php';
require_once $xcart_dir.'/include/func/func.itemsearch.php';
require_once $xcart_dir.'/include/func/func.order.php';

if($HTTP_GET_VARS) {
	$query_string = http_build_query($HTTP_GET_VARS);
	$mode = $HTTP_GET_VARS[mode];
}


#
#Load assignee list
#
$sql = "SELECT a.name as name, a.id as id, count(*) as assigned_count FROM {$sql_tbl[mk_assignee]} a, {$sql_tbl[order_details]} b, {$sql_tbl[orders]} c WHERE b.orderid = c.orderid and c.orgid = '0' and a.status=1 and a.id = b.assignee and b.item_status='A' and c.status='WP' GROUP BY a.name ORDER BY a.name";
$assignees = func_query($sql);


#
#Load Item Locations
#
$sql = "SELECT a.location_name as location_name, a.id as id, count(*) as location_count FROM {$sql_tbl[operation_location]} a, {$sql_tbl[order_details]} b, {$sql_tbl[orders]} c WHERE b.orderid = c.orderid and c.orgid = '0' and a.status=1 and a.id=b.location  and c.status='Q' GROUP BY a.location_name ORDER BY a.location_name";
$locations = func_query($sql);


$sql = "SELECT COUNT(*) as count,status  FROM {$sql_tbl[orders]} group by status";
$status_count=func_query($sql);
$state_count=array();
foreach($status_count as $item){
    $state_count[$item[status]]=$item[count];
}

#Load RTS Count
#
$sql = "SELECT COUNT(*) AS wp_count FROM {$sql_tbl[orders]} WHERE status='WP'";
$rts_count = func_get_ready_to_ship_orders("","","","",true);
if($state_count[WP]>0)
$state_count[$item[status]]-=$rts_count;


#Load Shipped today
#
$sql = "select count(*) from xcart_orders o where  status='C' and completeddate > (unix_timestamp(now())-24*60*60);";
$shipped_today = func_query_first_cell($sql);

#Load LATE Count
#
$sql = "select count(*) from xcart_orders o
where  (status='Q' or status='WP' or status='OH') and
unix_timestamp(now())>((select max(estimated_processing_date) from xcart_order_details where orderid=o.orderid and estimated_processing_date!='' and item_status in ('UA','A','S','H','OD')))";
$late_count = func_query_first_cell($sql);
$total_outstanding = func_query_first_cell("select count(*) from xcart_orders where  (status='Q' or status='WP' or status='OH')");

/*
select count(*) from xcart_orders o
where  status='C' and completeddate > (unix_timestamp(now())-2*24*60*60) and order_name not like 'rbk%';

select count(*) from xcart_orders o
where  status='C' and completeddate > (unix_timestamp(now())-2*24*60*60) and order_name not like 'rbk%' and 
completeddate >=((select max(estimated_processing_date) from xcart_order_details where orderid=o.orderid and estimated_processing_date!='' )) ;

select count(*) from xcart_orders o
where  status='C' and completeddate > (unix_timestamp(now())-2*24*60*60) and order_name not like 'rbk%' and 
completeddate <((select max(estimated_processing_date) from xcart_order_details where orderid=o.orderid and estimated_processing_date!='' )) ;


 */

#
#Load Orders Queued counts
#
$seconds_in_24_hours = 86400;
$sql = "SELECT CEILING((UNIX_TIMESTAMP()-queueddate)/{$seconds_in_24_hours}) AS pending_brackets,COUNT(*) AS queued_count FROM {$sql_tbl[orders]} WHERE status='Q' and orgid='0' GROUP BY pending_brackets";
$sql_result = func_query($sql);
$orders_pending_counts = array (
	lt_24 		=> 0,
	gt_24_lt_48	=> 0,
	gt_48_lt_72	=> 0,
	gt_72		=> 0,
	total 		=> 0,
);
foreach($sql_result as $key=>$value){
	
	$orders_pending_counts[total] += $value[queued_count];
	if($value[pending_brackets] == 1)
	{
		$orders_pending_counts[lt_24] 		= $value[queued_count];
	}
	elseif($value[pending_brackets] == 2)
	{
		$orders_pending_counts[gt_24_lt_48] = $value[queued_count];
	}
	elseif($value[pending_brackets] == 3)
	{
		$orders_pending_counts[gt_48_lt_72] = $value[queued_count];
	}
	elseif($value[pending_brackets] > 3)
	{
		$orders_pending_counts[gt_72]		+=$value[queued_count];
	}
}


#
#Load Item Status Counts
#
$sql = "SELECT a.item_status as item_status,count(*) as status_count from {$sql_tbl[order_details]} a, {$sql_tbl[orders]} b where a.orderid = b.orderid and b.orgid='0' and ((a.item_status='D' and DATE_FORMAT(b.date,\"%Y-%m-%d\")=CURDATE()) or b.status='Q' or b.status='WP' or b.status='OH') group by item_status";
$sql_result = func_query($sql);
foreach($sql_result as $key=>$value){
	if (isset($value[item_status])){
		$items_status_counts[$value[item_status]] = $value[status_count];
		if(($value[item_status] == 'N')|| ($value[item_status] == 'UA') || ($value[item_status] == 'A') || ($value[item_status] == 'F') || ($value[item_status] == 'H') || ($value[item_status] == 'OD') || ($value[item_status]='D')|| ($value[item_status]='QD') ||($value[item_status] == 'S')){
			$items_status_counts[PENDING] += $value[status_count];
		}
	}
}



$smarty->assign('query_string', $query_string);
$smarty->assign('main', 'operations_dashboard');
$smarty->assign('http_location', $http_location);

$smarty->assign('assignees', $assignees);
$smarty->assign('locations', $locations);
$smarty->assign('rts_count', $rts_count);
$smarty->assign('shipped_today', $shipped_today);
$smarty->assign('late_count', $late_count);
$smarty->assign('total_outstanding_orders', $total_outstanding);
$smarty->assign('state_count', $state_count);
$smarty->assign('orders_pending_counts', $orders_pending_counts);
$smarty->assign('items_status_counts', $items_status_counts);


func_display('admin/home.tpl', $smarty);
?>
