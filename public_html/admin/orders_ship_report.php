<?php
require ("./auth.php");
include_once($xcart_dir."/include/security.php");
include_once($xcart_dir."/include/func/func.mkcore.php");
include_once($xcart_dir."/include/func/func.order.php");
include_once($xcart_dir."/modules/apiclient/ShipmentApiClient.php");
include_once($xcart_dir."/PHPExcel/PHPExcel.php");
include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");
include_once($xcart_dir."/modules/apiclient/WarehouseApiClient.php");
include_once($xcart_dir."/modules/apiclient/CourierApiClient.php");


function get_myntra_delivery_from_orders($rts_orderids){
	if(count($rts_orderids) > 0 ) {
		$template_query="select reports_template from mk_courier_service where code='ML'";
		$template=func_query_first_cell($template_query);
		
		if(!$template) {
			$template = "orderid as orderId, title as title, s_firstname as firstName, " .
					"s_lastname as lastName, s_address as address, s_city as city, s_state as state," .
					"s_country as country, s_zipcode as zipcode, mobile as mobile, s_email as email" .
			        ", if(payment_method = 'cod', 'true','false') as cod, " .
			        "if(payment_method = 'cod',round(total + cod),null) as codAmount";
		}
		$order_details_query .= "SELECT  $template from xcart_orders where orderid in (".implode(',', $rts_orderids).")";
		$order_details = func_query($order_details_query);
		return $order_details;
	}
	
}
function get_xls_data_from_orders($courier, $rts_orderids, $paymentmethod) {
	$distinct_order_ids = array();
	foreach ($rts_orderids as $orderid) {
		if(!in_array($orderid, $distinct_order_ids)) {
			$distinct_order_ids[] = $orderid;
		}
	}
	
	if(count($distinct_order_ids) > 0 ) {
		$template_query="select reports_template from mk_courier_service where code='$courier'";
		$template=func_query_first_cell($template_query);
		
		if(!$template) {
			$template = 'orderid as "Order Number", CONCAT(s_firstname," ",s_lastname) as "Customer Name", CONCAT(s_address,",",s_country) as Address, s_zipcode as Pincode, s_city as City, mobile as Mobile, phone as Phone';
		}
		
		if($paymentmethod == 'cod') { 
			switch($courier){
				case 'AR' :
					$template .= ', 1 as weight, round(total + cod) as cod_value'; break;
				case 'FX' :
					$template .= ',round(total + cod) as "COD AMOUNT"'; break;
				case 'QS' :
					$template .= ''; break;
				default :
					 $template .= ', 1 as weight, round(total + cod) as amount';
			}
		}
		
		if($courier == 'BD') {
			$order_details_query .= "SELECT  o.$template, product_display_name as contents, article_number as \"Prod/Sku Code\" from xcart_orders o, xcart_order_details od, mk_style_properties sp where o.orderid = od.orderid and od.product_style = sp.style_id and o.orderid in (".implode(',', $distinct_order_ids).") group by o.orderid";	
		} else {
			$order_details_query .= "SELECT  $template from xcart_orders where orderid in (".implode(',', $distinct_order_ids).")";
		}
		$order_details = func_query($order_details_query);
		
		return $order_details;
	}
	return false;
}

function write_order_data_for_courier_to_sheet($objPHPExcel, $index, $sheetname, $orderids, $courier, $paymentmethod) {
	$order_details = get_xls_data_from_orders($courier, $orderids, $paymentmethod);
	return func_write_data_to_excel($objPHPExcel, $order_details, $index, $sheetname);
}

global $weblog;

$action = $_REQUEST['action'];

$batchid = $_REQUEST['batchid'];

if(!$action)
	$action = 'view';

$mode = $_REQUEST['mode'];

if(!$mode){
	$mode = 'gen_rts_report';
}
$weblog->debug("RTS page mode: ". $mode);

$noofcourierfilters = 1;
/*if($action == 'mark_orders_shipped'){
	$final_response = array('successmsg'=>'', 'errormsg'=>'');
	$input_orderids = trim($_POST['orderids']);
	$weblog->debug("Orders to ship: " . $input_orderids);
	$input_orderids = explode(",", $input_orderids);
	$progressKey = $_POST['progresskey'];
	
	$orderids = array();
	foreach($input_orderids as $index=>$orderid) {
		if(trim($orderid) != "") 
			$orderids[] = trim($orderid);
	}
	
	$final_response = mark_orders_shipped($orderids, $_POST['login'], $progressKey);
	$errorMsg = "";
	$successMsg = "";
	$uncsuccessfulOrderIds = array();
	if(isset($final_response['unsuccessful'])){
		$uncsuccessfulOrderIds = $final_response['unsuccessful']['orderids'];
		$errorMsg = "Following order were not marked Shipped - ".implode(", ", $uncsuccessfulOrderIds).". Error - ".$final_response['unsuccessful']['errormsg']."<br>";
	}
	$failedOrderIds = array();
	if(isset($final_response['failed']) && count($final_response['failed'])) {
		foreach($final_response['failed'] as $record) {
			$errorMsg .= "Order ".$record['orderid']." cannot be marked Shipped. Error - ".$record['errormsg']."<br>";
			$failedOrderIds[] = $record['orderid'];			
		}
	}
	
	if(isset($final_response['invalid'])){
		$errorMsg .= "Folllowing orders are invalid - ".implode(", ", $final_response['invalid']['orderids']);
	}
	if(isset($final_response['successful'])){
		$successMsg = "Folllowing orders were successfully marked Shipped - ".implode(", ", $final_response['successful']['orderids']);
	}
	
	echo json_encode(array('successful_orderids'=>$final_response['successful']['orderids'],
							'successmsg'=>$successMsg,
							'failed_orderids'=> array_merge($uncsuccessfulOrderIds, $failedOrderIds),
							'invalid_orderids'=> $final_response['invalid']['orderids'],
							'errormsg'=>$errorMsg));
	exit(0);
	
}*/
if($action == 'save_courier_pref'){
	$noofcourierfilters = $_POST['noofcouriers'];
	$whid = $_POST['wh_id'];
	$whbarcode = $_POST['wh_barcode'];
	$addOrUpdate = $_POST['add_or_Update'];
	$courier_filters = array();
	for($i=1; $i<=$noofcourierfilters; $i++) {
		$courier_filters[] = $_POST['filter_'.$i];
	}
	
	$resp = CourierApiClient::setCourierPref($whid, $whbarcode, $courier_filters, $addOrUpdate, $login);
	print json_encode($resp);
	exit(0);
}else if($action == 'get_courier_pref'){
	$whid = $_POST['wh_id'];
	$courier_filters = CourierApiClient::getCourierPref($whid);	
	echo json_encode($courier_filters);
	exit(0);
}else if($action == 'generate_batch_report' || $action == 'generate_report' || $action == 'order_check') {
	$noofcourierfilters = $_POST['noofcouriers'];
	$courier_filters = array();
	$rts_order_buckets = array();
	$shipped_order_buckets = array();
	$orderTrackingMap = array();
	
	$rts_order_buckets['non_serviceable'] = array('cod_orders'=>array(), 'on_orders'=>array());
	
	$input_orderids = trim($_POST['orderids']);
	if($action == 'generate_batch_report'){
		$input_orderids = func_query_first("select orderids from ready_to_ship_batch where id = $batchid");
		$input_orderids = $input_orderids['orderids'];
		$input_orderids = explode(",", $input_orderids);
	}else{
		$input_orderids = explode("\r\n", $input_orderids);
	}
	
	$orderids = array();
	$invalid_orders = array();
	foreach($input_orderids as $index=>$orderid) {
		if(trim($orderid) != ""){
			if(isInteger(trim($orderid))){
				$orderids[] = trim($orderid);	
			}else{
				$invalid_orders[] = trim($orderid);
			}
		}
		
	}
	$smarty->assign("courier_filters", $courier_filters);
	$smarty->assign("orderids", implode("\n", $orderids));
	
	if(!empty($orderids)){
		if($action == 'order_check') {		
			$validated_orders = validateOrdersForShipping($orderids);
			$remaining_orderids = $validated_orders['VALID_ORDERS'];
			$cancelled_orderids  = $validated_orders['CANCELLED_ORDERS'];
			$not_wp_orderids = $validated_orders['NOT_WIP_ORDERS'];
			$qa_pending_wp_orderids = $validated_orders['QA_PENDING_ORDERS'];
			$no_courier_orderids = $validated_orders['NO_COURIER_ORDERS'];
			$invalid_orders = array_merge($invalid_orders,$validated_orders['INVALID_ORDERS']);
		} else {
			$remaining_orderids = $orderids;
		}
	}

	$orders_to_ship = array();
	if(count($remaining_orderids) > 0) {
		$qa_done_orders_sql = "SELECT orderid, s_zipcode, s_country, payment_method, courier_service, tracking from $sql_tbl[orders] where orderid in (".implode(",", $remaining_orderids).")";
		$qa_done_orders = func_query($qa_done_orders_sql, true);
	 	$qa_done_orderids = array();
		if($qa_done_orders) {
			foreach ($qa_done_orders as $order) {
				$qa_done_orderids[] = $order['orderid'];
				$courier = $order['courier_service'];
				if($courier){
					$tracking_no = $order['tracking'];
					if(!$tracking_no || $tracking_no == null || empty($tracking_no)) {
						$tracking_no = '';
					} else {
						$orders_to_ship[] = $order;
					}
					$orderTrackingMap[$order['orderid']] = $tracking_no;
					if(!$rts_order_buckets[$courier]){
						$rts_order_buckets[$courier] = array('cod_orders'=>array(), 'on_orders'=>array());
					}
					
					if($order['payment_method'] == 'cod'){
						$rts_order_buckets[$courier]['cod_orders'][] = $order['orderid'];
					} else {
						$rts_order_buckets[$courier]['on_orders'][] = $order['orderid'];
					}
				}else if(!$courier || empty($courier)){
					$no_courier_orderids[] = $order['orderid'];
				}
			}
		}
		$remaining_orderids = array_diff($remaining_orderids, $qa_done_orderids);
	}
	
	if($action == 'order_check'){
		$retarr;
		if(count($invalid_orders)>0 || count($remaining_orderids) >0){
			if(count($remaining_orderids)>0){
				$invalid_orders = array_merge($invalid_orders,$remaining_orderids);
			}
			$retarr = array('remark'=> 'Invalid Order', 'orders'=> $invalid_orders);
		}else if(count($cancelled_orderids) > 0) {
			$retarr = array('remark'=> 'Cancelled Order', 'orders'=> $cancelled_orderids);
		}else if(count($not_wp_orderids) > 0) {
			$retarr = array('remark'=> 'Not WIP Order', 'orders'=> $not_wp_orderids);
		}else if(count($qa_pending_wp_orderids) > 0) {
			$retarr = array('remark'=> 'Not Complete QA done Order', 'orders'=> $qa_pending_wp_orderids);
		}else if(count($no_courier_orderids) > 0) {
			$retarr = array('remark'=> 'Courier Assignment not done', 'orders'=> $no_courier_orderids);
		}else if(count($orders_to_ship) > 0) {
			// add batch
			$order = $orders_to_ship[0];
			$batchid = addOrderToRtsBatch($order['orderid'], $batchid, $login);
			$final_response = mark_orders_packed($order['orderid'], $login);
			$errorMsg = "";
			$successMsg = "";
			if(isset($final_response['unsuccessful'])){
				$uncsuccessfulOrderIds = $final_response['unsuccessful']['orderids'];
				$errorMsg = "Cannot mark ship. Error - ".$final_response['unsuccessful']['errormsg']."<br>";
			}
			if(isset($final_response['failed']) && count($final_response['failed']) > 0) {
				foreach($final_response['failed'] as $record) {
					$errorMsg = "Cannot mark ship. Error - ".$record['errormsg']."<br>";			
				}
			}
			if(isset($final_response['invalid'])){
				$errorMsg = "Invalid Order";
			}
			if(isset($final_response['successful']['orderids'])){
				if($order['payment_method'] == 'cod')
					$successMsg = "Packed. To be shipped via ".$order['courier_service']." COD";
				else
					$successMsg = "Packed. To be shipped via ".$order['courier_service']." Non-COD";
			}
			
			if($errorMsg != "")
				$retarr = array('courier'=>$order['courier_service'], 'remark'=> $errorMsg, 'orders'=> array($order['orderid']), 'tracking_no'=>$order['tracking']);
			if($successMsg != "")
				$retarr = array('courier'=>$order['courier_service'], 'remark'=> $successMsg, 'orders'=> array($order['orderid']), 'tracking_no'=>$order['tracking']);
				
		}else{
			//
			foreach($rts_order_buckets as $courier=>$bucket) {
				if(count($bucket['cod_orders']) > 0) {
					$batchid = addOrderToRtsBatch($bucket['cod_orders'][0], $batchid, $login);
					$retarr = array('courier'=>$courier, 'courier'=>$courier, 'remark'=> $courier.' COD', 'orders'=> $bucket['cod_orders'], 'tracking_no'=>$orderTrackingMap[$bucket['cod_orders'][0]]);
				}
				if(count($bucket['on_orders']) > 0) {
					$batchid = addOrderToRtsBatch($bucket['on_orders'][0], $batchid, $login);
					$retarr = array('courier'=>$courier, 'courier'=>$courier, 'remark'=> $courier.' Non-COD', 'orders'=> $bucket['on_orders'], 'tracking_no'=>$orderTrackingMap[$bucket['on_orders'][0]]);
				}
			}
		}
		$retarr['batchid'] = $batchid;				
		header('Content-type: text/x-json');
		print json_encode($retarr);
		exit(0);
	} else {
		$objPHPExcel = new PHPExcel();
		$sheet_counter = 0;
		foreach($rts_order_buckets as $courier=>$bucket) {
			if(count($bucket['cod_orders']) > 0) {
				$order_details = get_xls_data_from_orders($courier, $bucket['cod_orders'], 'cod');
				$objPHPExcel = func_write_data_to_excel($objPHPExcel, $order_details, $sheet_counter++, $courier.' COD Orders', true, '', true);
				$weblog->info("Wrote Rts Orders [".$courier."] - [cod] - ". implode(",", $bucket['cod_orders']));
			}
			if(count($bucket['on_orders']) > 0) {
				$order_details = get_xls_data_from_orders($courier, $bucket['on_orders'], 'on');
				$objPHPExcel = func_write_data_to_excel($objPHPExcel, $order_details, $sheet_counter++, $courier.' Non COD Orders', true, '', true);
				$weblog->info("Wrote Rts Orders [".$courier."] - [non cod] - ". implode(",", $bucket['on_orders']));
			}
		}
		
		foreach($shipped_order_buckets as $courier=>$bucket) {
			if(count($bucket['cod_orders']) > 0) {
				$order_details = get_xls_data_from_orders($courier, $bucket['cod_orders'], 'cod');
				$objPHPExcel = func_write_data_to_excel($objPHPExcel, $order_details, $sheet_counter++, 'Shipped - '.$courier.' COD Orders', true, '', true);
				$weblog->info("Wrote Shipped Orders [".$courier."] - [cod] - ". implode(",", $bucket['cod_orders']));
			}
			if(count($bucket['on_orders']) > 0) {
				$order_details = get_xls_data_from_orders($courier, $bucket['on_orders'], 'on');
				$objPHPExcel = func_write_data_to_excel($objPHPExcel, $order_details, $sheet_counter++,'Shipped - '.$courier.' Non COD Orders', true, '', true);
				$weblog->info("Wrote Shipped Orders [".$courier."] - [non cod] - ". implode(",", $bucket['on_orders']));
			}
		}
		
		if(count($remaining_orderids) > 0) {
			$objPHPExcel = func_write_data_to_excel($objPHPExcel, $remaining_orderids,  $sheet_counter++, 'Invalid Orders', false, 'Order Id');
			$weblog->info("Wrote Invalid Order Ids - ". implode(",", $remaining_orderids));
		}
		if(count($cancelled_orderids) > 0) {
			$objPHPExcel = func_write_data_to_excel($objPHPExcel, $cancelled_orderids,  $sheet_counter++, 'Cancelled Orders', false, 'Order Id');
			$weblog->info("Wrote Cancelled Orders - ". implode(",", $cancelled_orderids));
		}
		if(count($not_wp_orderids) > 0) {
			$objPHPExcel = func_write_data_to_excel($objPHPExcel, $not_wp_orderids,  $sheet_counter++, 'Not WIP Orders', false, 'Order Id');
			$weblog->info("Wrote Not WIP Orders - ". implode(",", $not_wp_orderids));
		}
		if(count($qa_pending_wp_orderids) > 0) {
			$objPHPExcel = func_write_data_to_excel($objPHPExcel, $qa_pending_wp_orderids,  $sheet_counter++, 'Not Complete QA done Orders', false, 'Order Id');
			$weblog->info("Wrote Not All QA Done Orders - ". implode(",", $qa_pending_wp_orderids));
		}
		if(count($no_courier_orderids) > 0) {
			$objPHPExcel = func_write_data_to_excel($objPHPExcel, $no_courier_orderids,  $sheet_counter++, 'Courier Not Assigned Orders', false, 'Order Id');
			$weblog->info("Wrote Courier Not Assigned Orders - ". implode(",", $no_courier_orderids));
		}
		$objPHPExcel->setActiveSheetIndex(0);
		
		$filename = 'ready-to-ship-report-'.date('d-m-y-H:i:s').'.xls';
		header("Content-Disposition: attachment; filename=$filename");
		ob_clean();
		flush();
		try {
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
		}catch(Exception $ex) {
			$weblog->info("Error Occured = ". $ex->getMessage());
		}
		$weblog->info("Saved");
		exit(0);
	}
}
	
$sql = "select courier_service as name, code, color_code from mk_courier_service where enable_status=1";
$couriers=func_query($sql);
$courier_color_map = array();
foreach ( $couriers as $c ) {
    $courier_color_map[$c['code']] = $c['color_code'];   
} 

if($mode == 'set_courier_pref'){
	$warehouseList = WarehouseApiClient::getAllWarehouses();
	$smarty->assign("warehouses", $warehouseList);	
}

$smarty->assign("num_courier_filters", $noofcourierfilters);
$smarty->assign("couriers_json", json_encode($couriers));
$smarty->assign("couriers", $couriers);
$smarty->assign("courier_color_map", json_encode($courier_color_map));
$smarty->assign("action", $action);
$smarty->assign("mode", $mode);
$smarty->assign("title", ($mode=='gen_rts_report'?"Generate RTS Report":"Set Courier Preference"));
$smarty->assign("main", "orders_ship_report");
func_display("admin/home.tpl",$smarty);
?>
