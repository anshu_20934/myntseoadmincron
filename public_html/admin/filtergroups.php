<?php
/**
 * Created by Nishith Shrivastav.
 * User: myntra
 * Date: Nov 13, 2010
 * Time: 7:53:54 PM
 */
#
# Purpose : This file is created to add, edit and delete filter groups ######
#

require "./auth.php";

require $xcart_dir."/include/security.php";
define('FORM_ACTION','filtergroups.php');
$location[] = array("Filter Groups", "");
define("FILTER_GROUP_IMAGES_DIR",'../skin1/mkimages');
$groupToModify = '';

if($REQUEST_METHOD == "POST"){

          $isError = false;
          switch($HTTP_POST_VARS['mode']){
          ############## Condition to add groups ############
          	case  "addgroup" :

                    if(empty($HTTP_POST_VARS['group_name'])){
                        $isError = true;
                    }

                    if(!$isError)
                    {
                        $query_data = array(
                            "group_name"  =>(!empty($HTTP_POST_VARS["group_name"]) ? trim($HTTP_POST_VARS["group_name"]) : ''),
                            "group_label"  =>(!empty($HTTP_POST_VARS["group_name"]) ? trim($HTTP_POST_VARS["group_label"]) : ''),
                            "description"  =>(!empty($HTTP_POST_VARS["description"]) ? trim($HTTP_POST_VARS["description"]) : ''),
                            "config"  =>(!empty($HTTP_POST_VARS["config"]) ? $HTTP_POST_VARS["config"] : ''),
                            "display_order"  =>(!empty($HTTP_POST_VARS["display_order"]) ? $HTTP_POST_VARS["display_order"] : ''),
                            "is_active" =>  (!empty($HTTP_POST_VARS["is_active"]) ? '1' : '0')
                            ) ;

                        func_array2insert ('mk_filter_group', $query_data);
                        $top_message["content"] = "Added new group.";
                    }
                    else
                    {
                        $top_message["content"] = "Error! Please input valid data.";
                        $top_message["anchor"] = "featured";
                    }

            break;
		   ########### Condition to update ###############
          	case "update":
			   if (is_array($posted_data))
               {          

				    foreach ($posted_data as $id=>$v)
                    {
                        if (empty($v["to_delete"]))
                          continue;

                        $query_data = array(
                            "group_name"  =>(!empty($v["group_name"]) ? trim($v["group_name"]) : ''),
                            "group_label"  =>(!empty($v["group_label"]) ? trim($v["group_label"]) : ''),
                            "description"  =>(!empty($v["description"]) ? trim($v["description"]) : ''),
                            "config"  =>(!empty($v["config"]) ? $v["config"] : ''),
                            "display_order"  =>(!empty($v["display_order"]) ? $v["display_order"] : 0),
                            "is_active" =>  (!empty($v["is_active"]) ? $v["is_active"] : 0)
                        ) ;
                        func_array2update("mk_filter_group", $query_data, "id='$id'");
                    }
                          $top_message["content"] = "Group edited successfully!!";
                          $top_message["anchor"] = "featured";

                }
                       break;

          ########### Condition to delete the record ###############
          	case  "delete" :
			   if (is_array($posted_data)) {
				    foreach ($posted_data as $groupid=>$v) {
						 if (empty($v["to_delete"]))
							  continue;
						 db_query("delete from mk_filter_group where id ='".$groupid."'") ;
					}

				  $top_message["content"] = "Group deleted!!";
	              $top_message["anchor"] = "featured";

	           }
	           break;

          ########### Condition to delete the record ###############
          case "modify":
		  	    if (is_array($posted_data)) {
				    foreach ($posted_data as $groupid=>$v) {
						 if (empty($v["to_delete"]))
							  continue;
					     $groupToModify =  $groupid;

					}

				  ##$top_message["content"] = func_get_langvar_by_name("msg_adm_prdstyle_upd");
	              ##func_header_location("admin_product_styles.php");

	           }
	           break;
        }  ####### End of switch case

}
$sql ="SELECT * FROM  mk_filter_group";

//if(!empty($groupToModify)){
//   $editQuery =  $sql." where style_group_id ='".$groupToModify."'"  ;
//   $recordsToEdit = db_fetch_array(db_query($editQuery));
//   $smarty->assign("editRecords",$recordsToEdit);
//}
$grouprecords = func_query($sql);
$smarty->assign("groups",$grouprecords);

$smarty->assign("main","filtergroups");


# Assign the current location line
$smarty->assign("location", $location);
$smarty->assign("form_action", FORM_ACTION);
# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
