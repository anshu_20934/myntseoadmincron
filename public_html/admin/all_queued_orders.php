<?php
require "./auth.php";
ini_set("display_errors",1);
require $xcart_dir."/include/security.php";
include_once($xcart_dir."/include/func/func.itemsearch.php");

if($HTTP_POST_VARS){
	$query_string = http_build_query($HTTP_POST_VARS);
	$locationid = $HTTP_POST_VARS['item_location'];
}
if($HTTP_GET_VARS){
	$query_string = http_build_query($HTTP_GET_VARS);
	$locationid = $HTTP_GET_VARS['item_location'];
}

//if(true)
{
	#
	#Calling function to create search query for number of records
	#
	$sql = func_load_all_queued_orders($operations_location);
	$sqllog->debug("File name##assignee_worksheet.php##SQL Info::>$sql");
	$_res = db_query($sql);
	$total_items = db_num_rows($_res);
	$smarty->assign("total_items", $total_items);
    
	if ($total_items > 0)
	{
		$page = $HTTP_GET_VARS["page"];

		#
		# Prepare the page navigation
		#
		if (!isset($objects_per_page)) {
			if ($current_area == "C")
			$objects_per_page = $config["Appearance"]["products_per_page"];
			else
			$objects_per_page = $config["Appearance"]["products_per_page_admin"];
		}

		if($objects_per_page ==0){
			$objects_per_page=5;
		}
		$total_nav_pages = ceil($total_items/$objects_per_page)+1;

		require $xcart_dir."/include/navigation.php";

		$search_sql = func_load_all_queued_orders($operations_location);

		$search_sql .=" LIMIT $first_page, $objects_per_page";

		$sqllog->debug("File name##assignee_worksheet.php##SQL Info::>$search_sql");
		$itemResult = func_query($search_sql);
		#
		#navigation url
		#
		if($mode == "search")
            $URL = "all_queued_orders.php?".$query_string;
		else
            $URL = "all_queued_orders.php?";

		$smarty->assign("navigation_script",$URL);
		$smarty->assign("first_item", $first_page+1);
		$smarty->assign("last_item", min($first_page+$objects_per_page, $total_items));
		$smarty->assign("total_items",$total_items);
		$smarty->assign ("itemResult", $itemResult);
		$smarty->assign ("sizeofitemresult", $objects_per_page);

	}
}

if($action=="ex"){
    #
    #Sending confirmation to operation head of unassigned items
    #

    $search_sql = func_load_all_queued_orders($operations_location);
    $sqllog->debug("File name##assignee_worksheet.php##SQL Info::>$search_sql");
	$itemResultReport = func_query($search_sql);

    #
	#Code to write the report content in a file
	#
    $file_name_suffix = time();
    $queued_orders = fopen($xcart_dir."/admin/item_assignment_report/queued_order_report_$file_name_suffix.csv","a");
    $summary_table = "Orderid, ItemId, Product Type, Product Style, Quantity Breakup, Queued Date, Total Quantity";
    fwrite($queued_orders, $summary_table);
    fwrite($queued_orders, "\r\n");
    $string = "";

    $oldorderid = "";
    foreach($itemResultReport AS $key=>$array){
        if($array[orderid] != $oldorderid){
            $orderid = "$array[orderid]";
        }else{
            $orderid = "L";
        }

        $quantity_breakup = str_replace("," , " || ", $array[quantity_breakup]);

        $string .= $orderid . ", " . $array[itemid] . ", " . $array[p_type] . ", " . $array[p_style] . ", " . $quantity_breakup . ", " . $array[queueddate] . ", " . $array[total_quantity];
        $string .= "\r\n";
       
        $oldorderid = $array[orderid];
    }
     fwrite($queued_orders, $string);
     fwrite($queued_orders, "\r\n");

	
#
#Download 
#
    $filename = "queued_order_report_$file_name_suffix.csv";
	$dir = "../admin/item_assignment_report/";
	force_download($filename, $dir);

}

#
#Load operations locations
#
$sql = "SELECT * FROM $sql_tbl[operations_location] WHERE status=1";
$sql_result = func_query($sql);
foreach ($sql_result as $key=>$value)
	$operations_locations[$value['id']] = $value['name'];


$smarty->assign ("status", $status);
$smarty->assign ("operations_locations", $operations_locations);
$smarty->assign ("query_string", $query_string);
$smarty->assign ("mode", $mode);


$smarty->assign("main","all_queued_orders");
func_display("admin/home.tpl",$smarty);
?>
