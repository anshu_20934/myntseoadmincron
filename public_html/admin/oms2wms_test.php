<?php
require_once "WMSDBConnUtil.php";
function getOms2WmsBoc($warehouseId,&$wmsMoreBocQueries,&$wmsLessBocQueries,$myntraSellerIds){
 

$imsConn = WMSDBConnUtil::getConnection('imsdb1.myntra.com:3306', TRUE, WMSDBConnUtil::IMS_READ);
$omsConn = WMSDBConnUtil::getConnection("omsdb2.myntra.com:3306", TRUE, WMSDBConnUtil::IMS_READ);
$wmsConn = WMSDBConnUtil::getConnection('wmsdb2.myntra.com:3306', TRUE, WMSDBConnUtil::IMS_READ);
  if (!$omsConn || !$wmsConn) {
    die('Could not connect: ' . mysql_error());
  }

  if(mysql_select_db('myntra_oms', $omsConn ) === false) {
    exit;
  }
  if(mysql_select_db('myntra_wms', $wmsConn ) === false) {
    exit;
  }
  if(mysql_select_db('myntra_ims', $imsConn ) === false) {
    exit;
  }

  $limit = 30000;
  $hasMore= true;
  $omsMinId = 1;


  $omsMinIdQuery = "select min(id) as minId, max(id) as maxId from order_release where status_code in ('Q','RFR','WP') AND warehouse_id = ".$warehouseId;

  $omsMinIdResults = mysql_query($omsMinIdQuery, $omsConn);
  if ($omsMinIdResults){
    while ($row = mysql_fetch_array($omsMinIdResults, MYSQL_ASSOC)){
      $omsMinId = $row['minId'];
      $omsMaxId = $row['maxId'];
    }
  }

  @mysql_free_result($omsMinIdResults);

  $skuToOmsBoc = array();

  while($hasMore){
    $nextMinId = $omsMinId + $limit;
  // Loop over item list
  echo "started... getOms2WmsBoc";
  $omsBocQuery = "select ol.sku_id as sku_id, ol.seller_id as seller_id, sum(ol.quantity) as blocked_order_quantity from order_line ol join order_release orl on (orl.id = ol.order_release_id_fk) where orl.id >= ".$omsMinId." and orl.id < ".$nextMinId." and orl.status_code in ('Q','RFR','WP') and ol.seller_id in (".$myntraSellerIds.")  AND ol.supply_type = 'ON_HAND' and ol.store_line_id is null and ol.status_code != 'IC' and orl.warehouse_id=".$warehouseId." and ol.sku_id in (13275527) group by ol.sku_id, ol.seller_id";

  $omsBocResults = mysql_query($omsBocQuery, $omsConn);
  if ($omsBocResults){
    while ($row = mysql_fetch_array($omsBocResults, MYSQL_ASSOC)){

      $skuSeller= $row['sku_id']."_".$row['seller_id'];

      if($skuToOmsBoc[$skuSeller])
        $skuToOmsBoc[$skuSeller] = $skuToOmsBoc[$skuSeller] + $row['blocked_order_quantity'];
      else
        $skuToOmsBoc[$skuSeller] = $row['blocked_order_quantity'];

    }
  }

  #if($nextMinId >= $omsMaxId)
  $hasMore = false;

    $omsMinId = $nextMinId;
  @mysql_free_result($omsBocResults);
}

  $issuedItemsWmsQuery="select itm.order_id order_id, itm.sku_id sku_id, count(*) issuedCount from item itm where item_status='ISSUED' and itm.warehouse_id=".$warehouseId." group by itm.sku_id, itm.order_id";

  $issuedItemsWmsResults= mysql_query($issuedItemsWmsQuery, $wmsConn);

  while ($itemrow = mysql_fetch_array($issuedItemsWmsResults, MYSQL_ASSOC)){

    $issuedItemsOmsResults = mysql_query("select orl.id as id, ol.seller_id as seller_id, ol.sku_id as sku_id from order_line ol join order_release orl on (orl.id=ol.order_release_id_fk) where ol.order_release_id_fk = ".$itemrow['order_id']." and ol.sku_id = ".$itemrow['sku_id']." and ol.status_code != 'IC' and orl.status_code in ('PK', 'SH', 'DL', 'C','L','RTO')", $omsConn);

    $releaseId = false;
    while ($release = mysql_fetch_array($issuedItemsOmsResults, MYSQL_ASSOC)){
        $releaseId = $release['id'];
        $skuSeller= $itemrow['sku_id']."_".$release['seller_id'];
        $missedCount= $itemrow['issuedCount'];
        break;
    }
    if($releaseId) {
      if($skuToOmsBoc[$skuSeller]) {
        $skuToOmsBoc[$skuSeller] = $skuToOmsBoc[$skuSeller] + $missedCount;
      }else{
        $skuToOmsBoc[$skuSeller] = $missedCount;
      }
    }
  }
  @mysql_free_result($issuedItemsWmsResults);
  @mysql_free_result($issuedItemsOmsResults);


  $offset = 0;
  $limit = 200000;
  $hasMore= true;

  while($hasMore) {
    echo " second loop ";
    $skuToWmsBocMap = array();
    $allWarehouseSkusQuery = "select id, sku_id, seller_id, blocked_order_count from wh_inventory where warehouse_id = $warehouseId and store_id = 1 and seller_id in (".$myntraSellerIds.") and supply_type='ON_HAND' and sku_id = 13275527 LIMIT $offset, $limit";
    $allWarehouseData = mysql_query($allWarehouseSkusQuery, $imsConn);
    if ($allWarehouseData && mysql_num_rows($allWarehouseData)){
      while($row = mysql_fetch_array($allWarehouseData, MYSQL_ASSOC)){
          $skuSeller=$row['sku_id']."_".$row['seller_id'];
        $skuToWmsBocMap[$skuSeller] = $row['blocked_order_count'];
      }
    }
    @mysql_free_result($allWarehouseData);
    if(empty($skuToWmsBocMap)) {
      $hasMore = false;
      continue;
    }

    foreach($skuToWmsBocMap as $skuSeller => $boc) {
      $query = false;
      $startIndx = strpos($skuSeller,"_");
      $sellerId=substr($skuSeller, $startIndx+1);
      $skuId=substr($skuSeller,0,$startIndx);

      if(!isset($skuToOmsBoc[$skuSeller])) {
        if($boc > 0 || $boc<0) {
          $query = "update wh_inventory set blocked_order_count = 0 where warehouse_id = $warehouseId and store_id = 1 and sku_id = $skuId and seller_id = ".$sellerId." and supply_type='ON_HAND'";
          $wmsMoreBocQueries[] = $query;
        }
      } else {
        $shouldBeQty = $skuToOmsBoc[$skuSeller];
        if($boc > $shouldBeQty) {
          $query = "update wh_inventory set blocked_order_count = blocked_order_count - ".($boc - $shouldBeQty)." where warehouse_id = $warehouseId and store_id = 1 and sku_id = $skuId and seller_id = ".$sellerId." and supply_type='ON_HAND'";
          $wmsMoreBocQueries[] = $query;
        }
        else if ($boc < $shouldBeQty){
          $query = "update wh_inventory set blocked_order_count = blocked_order_count + ".($shouldBeQty - $boc)." where warehouse_id = $warehouseId and store_id = 1 and sku_id = $skuId and seller_id = ".$sellerId." and supply_type='ON_HAND'";
          $wmsLessBocQueries[] = $query;
        }
      }
    }
    $offset += $limit;
  }

}
?>




