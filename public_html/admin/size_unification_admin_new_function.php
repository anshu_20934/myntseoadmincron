<?php

class SizeUnification
{
    var $listOfStyles;
    var $styleIdsStr;
    var $allProductOptions;
    var $allStyles;
    var $echoToBeDone;
    var $incremental;
    
    var $message;
    
    public function __construct($listOfStyles, $echoToBeDone = false, $incremental = false)
    {
        $this->listOfStyles = $listOfStyles;
        if (is_array($listOfStyles)) {
            $this->styleIdsStr = implode(",", $listOfStyles);
        } elseif (!empty($listOfStyles)) {
            $this->styleIdsStr = $listOfStyles;
        }
        $this->echoToBeDone = $echoToBeDone;
        $this->incremental  = $incremental && empty($this->styleIdsStr);
    }

    public function clearMappings()
    {
    	if ($this->echoToBeDone) {
    		echo "Clearing size unification data".(empty($this->listOfStyles)?" for all styles":" for style ids:".$this->listOfStyles);
    	}
    	//clear size unification data
    	$baseSql2 = "update mk_product_options set AllSizes = null, unified_size_value_id =null, unifiedSize = null";
    	if (!empty($this->styleIdsStr)) {
    		$numOptionsCleared = updateQuery($baseSql2 . " WHERE style in ($this->styleIdsStr)");
    	} else {  
    		//clear size unification data in batches
    		$sizeOptionStatsSql = "select min(id) as minoptionid,max(id) as maxoptionid,count(id) as numoptions from mk_product_options";
    		$stats              = func_query_first($sizeOptionStatsSql, true);
    		$numOptionsCleared  = sqlBatchUpdate($baseSql2, "id", $stats["minoptionid"], $stats["maxoptionid"], 8000, $this->echoToBeDone);
    	}
    	$message = "Size Unification Mapping was cleared for $numOptionsCleared size options for these styles";
    	$status = "SUCCESS";
        $statusCode = "200";
        $result = array('Status' => $status,'StatusCode' => $statusCode,'Message' =>$message,'numOptions' => $numOptionsCleared );
        //return $message;
        return $result;
    }
    
    public function clearMeasurements()
    {
    	if ($this->echoToBeDone) {
    		echo "Clearing size unification data".(empty($this->listOfStyles)?" for all styles":" for style ids:".$this->listOfStyles);
    	}
    	//clear size unification data
    	$baseSql2 = "update mk_product_options set size_representation = null";
    	if (!empty($this->styleIdsStr)) {
    		$numOptionsCleared = updateQuery($baseSql2 . " WHERE style in ($this->styleIdsStr)");
    	} else {
    		//clear size unification data in batches
    		$sizeOptionStatsSql = "select min(id) as minoptionid,max(id) as maxoptionid,count(id) as numoptions from mk_product_options";
    		$stats              = func_query_first($sizeOptionStatsSql, true);
    		$numOptionsCleared  = sqlBatchUpdate($baseSql2, "id", $stats["minoptionid"], $stats["maxoptionid"], 8000,  $this->echoToBeDone);
    	}
    	$message = "Size Unification Mapping was cleared for $numOptionsCleared size options for these styles";
    	$status = "SUCCESS";
        $statusCode = "200";
        $result = array('Status' => $status,'StatusCode' => $statusCode,'Message' =>$message,'numOptions' => $numOptionsCleared );
        //return $message;
        return $result;
    }
    
	public function clearImage(){
		if ($this->echoToBeDone) {
			echo "Clearing size chart image".(empty($this->listOfStyles)?" for all styles":" for style ids:".$this->listOfStyles);
		}
		//clear new size chart image
		$baseSql1 = "update mk_style_properties set size_representation_image_url = null";
		if (!empty($this->styleIdsStr)) {
			$numStylesCleared  = updateQuery($baseSql1 . " WHERE style_id in ($this->styleIdsStr)");
		} else {
			//clear new size chart image in batches
			$styleStatsSql    = "select min(style_id) as minstyleid,max(style_id) as maxstyleid,count(style_id) as numstyles from mk_style_properties";
			$stats            = func_query_first($styleStatsSql, true);
			$numStylesCleared = sqlBatchUpdate($baseSql1, "style_id", $stats["minstyleid"], $stats["maxstyleid"], 2000,  $this->echoToBeDone);		
		}
		$message = "$numStylesCleared styles were cleared for new size chart image.";
		$status = "SUCCESS";
        $statusCode = "200";
        $result = array('Status' => $status,'StatusCode' => $statusCode,'Message' =>$message,'numOptions' => $numStylesCleared );
        //return $message;
        return $result;
	}    
    public function clear()
    {
        if ($this->echoToBeDone) {
            echo "Clearing size chart image, size representation and unification data".(empty($this->listOfStyles)?" for all styles":" for style ids:".$this->listOfStyles);
        }
        //clear new size chart image
        $baseSql1 = "update mk_style_properties set size_representation_image_url = null";
        //clear size unification data
        $baseSql2 = "update mk_product_options set AllSizes = null, size_representation = null, unified_size_value_id =null, unifiedSize = null";
        if (!empty($this->styleIdsStr)) {
            $numStylesCleared  = updateQuery($baseSql1 . " WHERE style_id in ($this->styleIdsStr)");
            $numOptionsCleared = updateQuery($baseSql2 . " WHERE style in ($this->styleIdsStr)");
        } else {
            //clear new size chart image in batches
            $styleStatsSql    = "select min(style_id) as minstyleid,max(style_id) as maxstyleid,count(style_id) as numstyles from mk_style_properties";
            $stats            = func_query_first($styleStatsSql, true);
            $numStylesCleared = sqlBatchUpdate($baseSql1, "style_id", $stats["minstyleid"], $stats["maxstyleid"], 2000, TRUE);
            
            //clear size unification data in batches
            $sizeOptionStatsSql = "select min(id) as minoptionid,max(id) as maxoptionid,count(id) as numoptions from mk_product_options";
            $stats              = func_query_first($sizeOptionStatsSql, true);
            $numOptionsCleared  = sqlBatchUpdate($baseSql2, "id", $stats["minoptionid"], $stats["maxoptionid"], 8000, TRUE);
        }
        $message = "$numStylesCleared styles were cleared for new size chart image. Size Unification was cleared for $numOptionsCleared size options for these styles";
        /*$status = "SUCCESS";
        $statusCode = "200";
        $result = array('Status' => $status,'StatusCode' => $statusCode,'Message' =>$message,'numOptions' => $numOptionsCleared );*/
        return $message;
        //return $result;
    }
    
    /**
     * Populate Size Field in size-representation-rule table by concatenating scale-name and value
     */
    public function updateSizeStringInRepresentationRules()
    {
        $query = "update mk_size_representation_rule
					set size = concat(
								(select backward_compat_scale_name 
								from size_chart_scale as sca,size_chart_scale_value as val where sca.id = val.id_size_chart_scale_fk and val.id = sizevalueId)
								,(select size_value from size_chart_scale_value as val where val.id = sizevalueId)
							   )
					where id > 0;";
        return func_query($query);
    }
    
    /**
     * Populate Size & UnifiedSize Field in size-unification-rule table by concatenating scale-name and value
     */
    public function updateSizeStringInMappingRules()
    {
    	if($this->echoToBeDone){
    		echo "\n Updating All size strings and unified-size strings in unification rules table by concating";
    	}
        $query = "update mk_size_unification_rules
				set size = concat(
							(select backward_compat_scale_name from size_chart_scale as sca,size_chart_scale_value as val where sca.id = val.id_size_chart_scale_fk and val.id = sizevalueId )
							,(select size_value from size_chart_scale_value as val where val.id = sizevalueId)
							),
				unifiedSize = concat(
								(select backward_compat_scale_name  from size_chart_scale as sca,size_chart_scale_value as val where sca.id = val.id_size_chart_scale_fk and val.id = unifiedSizeValueId )
								,(select size_value from size_chart_scale_value as val where val.id = unifiedSizeValueId)
							)			
				where id > 0;";
        return func_query($query);
    }
    
    /**
     * This is a comparator function used in alphabetical sorting of Attributes Array based on attribute name
     * @param unknown_type $artAttr1
     * @param unknown_type $artAttr2
     */
    public static function cmpAttribute($artAttr1, $artAttr2)
    {
        if ($artAttr1[attribute] === $artAttr2[attribute]) {
            return 0;
        } elseif ($artAttr1[attribute] < $artAttr2[attribute]) {
            return -1;
        } else {
            return 1;
        }
    }
    
    /**
     * Top Level: UpdateRep
     */
    public function updateSizeMeasurements()
    {
        $numProdOptsUpdated = 0;
        $result             = $this->getAllProductOptions();
        if (empty($result)) {
            $message = "No Product Options to apply measurements";
        } else {
            /*
            //Kundan: we are not using any article-type specific attribute in matching size representation rule.
            $attributeIdsArr = $sizeUnification->getAllFitAttributeIds();
            $attributeIds = "(".implode(",", $attributeIdsArr).")";
            unset($attributeIdsArr);
            */
            if ($this->echoToBeDone) {
            	echo "Trying to fetch Measurement for Product Options:\n";
                echo "Note that you need to have run UpdateSca before running this step, else things will fail terribly:\n";
                echo "=================================================================";
            }
            for ($i = 0; !empty($result[$i]); $i++) {
                //Get the style details from query results
                $productOptionId = $result[$i][id];
                $styleId         = $result[$i][style_id];
                $brand           = $result[$i][brand_id];
                $type            = $result[$i][article_type_id];
                $sizevalueId     = $result[$i][sizevalueId];
                $unifiedSizeValueId= $result[$i][unifiedSizeValueId];
                $unifiedSize     = $result[$i][unifiedSize];
                $ageGroup        = $result[$i][global_attr_age_group];
                
                if ($this->echoToBeDone) {
                    echo "\n$productOptionId($styleId)";
                }
                //PORTAL-2811: now product option needs to have a unified size-value-id so as to apply mneasurement
                if(empty($unifiedSizeValueId)){
                	if ($this->echoToBeDone) {
                		echo "Failed: Product option doesn't have a mapped unified size option";
                	}
                	$representation = null;
                }
                else {
                	
	                //Kundan: with input from Kumar Anand, although we have article_type_specific_attribute_id in mk_size_representation rule
	                //it is NOT being populated in this table ever. (Although it is populated in the mk_style_article_type_attribute_values.
	                //So don't use $article_type_attribute_value_id in the match process
	                //$article_type_attribute_value_id = $sizeUnification->getArticleTypeSpecificAttributeId($attributeIds, $styleId);
	                
	                $filters = array();
	                $filters["sizevalueId"] = $unifiedSizeValueId;
	                
	                $representation = null;
	                //Attempt 1: see if there is a measurement available for this style-id itself:
	                if ($this->echoToBeDone) {
	                    echo "match with AT,AgeGrp,Brand,style-id:";
	                }
	                
	                $filters           = array_merge($filters, array(
	                    "article_type_id" => $type,
	                    "age_group" => $ageGroup,
	                    "brand_id" => $brand,
	                    "style_id" => $styleId
	                ));
	                $representationArr = func_select_columns_query("mk_size_representation_rule", "jsonOutput", $filters, 0, 1, null, "row");
	                if (!empty($representationArr)) {
	                    $representation = $representationArr[0][0];
	                    if ($this->echoToBeDone) {
	                        echo "Pass! ";
	                    }
	                } else {
	                    if ($this->echoToBeDone) {
	                        echo "Failed; ";
	                    }
	                    //Attempt 2: drop the style-id too, but add article-type-attribute, if present for this style
	                    $filters["style_id"] = null;
	                    /*//Kundan: see the note above for article_type_attribute_value_id: this field is not populated ever in the size-representation rule table,
	                     * //So don't use this field in the match process
	                     if(!empty($article_type_attribute_value_id)) {
	                     $filters["article_type_attribute_value_id"] = $article_type_attribute_value_id;
	                     }*/
	                    if ($this->echoToBeDone) {
	                        echo "match with AT,AgeGrp,Brand:";
	                    }
	                    $representationArr = func_select_columns_query("mk_size_representation_rule", "jsonOutput", $filters, 0, 1, null);
	                    if (!empty($representationArr)) {
	                        $representation = $representationArr[0][0];
	                        if ($this->echoToBeDone) {
	                            echo "Pass! ";
	                        }
	                    } else {
	                        if ($this->echoToBeDone) {
	                            echo "Failed; ";
	                        }
	                        //Kundan: we no longer support non-brand specific unification
	                        //we've exhausted all possible match attempts. No representation found.
	                        $representation = null;
	                        /*
	                        //Attempt 3: drop the brand too (over and above style_id which was previously dropped)
	                        if($this->echoToBeDone){ echo "match with AT,AgeGrp:";}
	                        unset($filters["brand_id"]);
	                        $representationArr = func_select_columns_query("mk_size_representation_rule", "jsonOutput", $filters, 0, 1, null);
	                        if(!empty($representationArr)) {
	                        $representation = $representationArr[0][0];
	                        if($this->echoToBeDone){ echo "Pass! "; }
	                        }
	                        else {
	                        //Attempt 4: Drop article-type attribute, over and above brand_id and style_id which were previously dropped
	                        //Kundan: we anyways never populate article_type_attribute_value_id column in the size-representation rule table,
	                        //So drop this from the match logic
	                        
	                        //$filters["article_type_attribute_value_id"] = null;
	                        //$representationArr = func_select_columns_query("mk_size_representation_rule", "jsonOutput", $filters, 0, 1);
	                        //if(!empty($representationArr)) { $representation = $representationArr[0]; }
	                        //we've exhausted all possible match attempts. No representation found.
	                        $representation = null;
	                        if($this->echoToBeDone){ echo "Failed; "; }
	                        }
	                        */
	                    }
	                }
            	}
                $updated = 0;
                if (!empty($representation)) {
                    $updated = $this->updateSizeRepresentation($representation, $productOptionId);
                }
                $numProdOptsUpdated += $updated;
                if ($this->echoToBeDone) {
                    echo $updated ? "UPDATED MEASUREMENT" : "NO MATCHING MEASUREMENT FOUND";
                }
            }
        }
        $this->message = "Finished UpdateSizeMeasurements ; Total no. of Product Options Updated = $numProdOptsUpdated";
        if ($this->echoToBeDone) {
            echo "\n".$this->message."\n";
        }
        //return $this->message;
        $message = $this->message;
        $status = "SUCCESS";
        $statusCode = "200";
        $result = array('Status' => $status,'StatusCode' => $statusCode,'Message' =>$message,'numOptions' => $numProdOptsUpdated );
        return $result;

    }
    
    /**
     * Top Level: UpdateSca
     */
    public function updateSizeMappings()
    {
        $result             = $this->getAllProductOptions();
        $numProdOptsUpdated = 0;
        if (!empty($result)) {
            for ($i = 0; !empty($result[$i]); $i++) {
                $productOptionId = $result[$i][id];
                $styleId         = $result[$i][style_id];
                $brand           = $result[$i][brand_id];
                $type            = $result[$i][article_type_id];
                $sizeValueId     = $result[$i][sizeValueId];
                $ageGroup        = $result[$i][global_attr_age_group];
                
                if ($this->echoToBeDone) {
                    echo "\n$productOptionId($styleId)";
                }
                
                $size       = str_replace(" ", "", $result[$i][size]);
                $topFilters = array();
                
                if (empty($sizevalueId)) {
                    $topFilters["lower(size)"] = strtolower($size);
                } else {
                    $topFilters["sizeValueId"] = $sizeValueId;
                }
                
                $unifiedSize        = null;
                $unifiedSizeValueId = null;
                
                //Attempt 1: see if there is a unified size available for this style-id itself:
                if ($this->echoToBeDone) {
                    echo "match with AT,AgeGrp,Brand,style-id:";
                }
                
                $filters  = array(
                    "article_type_id" => $type,
                    "age_group" => $ageGroup,
                    "brandId" => $brand,
                    "styleId" => $styleId
                );
                $scaleArr = func_select_columns_query("mk_size_unification_rules", array(
                    "unifiedSize",
                    "unifiedSizeValueId",
                    "sizeValueId"
                ), array_merge($filters, $topFilters), 0, 1, null, "row");
                
                if (!empty($scaleArr)) {
                    $unifiedSize        = $scaleArr[0][0];
                    $unifiedSizeValueId = $scaleArr[0][1];
                    $sizeValueId 		= $scaleArr[0][2];
                    if ($this->echoToBeDone) {
                        echo "Pass! ";
                    }
                } else {
                    if ($this->echoToBeDone) {
                        echo "Failed; ";
                    }
                    //Attempt 2: drop the style-id too
                    $filters["styleId"] = null;
                    if ($this->echoToBeDone) {
                        echo "match with AT,AgeGrp,Brand:";
                    }
                    
                    $scaleArr = func_select_columns_query("mk_size_unification_rules", array(
                        "unifiedSize",
                        "unifiedSizeValueId",
                        "sizeValueId"
                    ), array_merge($filters, $topFilters), 0, 1, null, "row");
                    if (!empty($scaleArr)) {
                        $unifiedSize        = $scaleArr[0][0];
                        $unifiedSizeValueId = $scaleArr[0][1];
                        $sizeValueId 		= $scaleArr[0][2];
                        if ($this->echoToBeDone) {
                            echo "Pass! ";
                        }
                    } else {
                        //Kundan: We now remove any non-brand specific unification
                        //we've exhausted all possible match attempts. No representation found.
                        $unifiedSize        = null;
                        $unifiedSizeValueId = null;
                        if ($this->echoToBeDone) {
                            echo "Failed; ";
                        }
                        
                        /*
                        if($this->echoToBeDone){ echo "Failed; "; }
                        //Attempt 3: drop the brand too (over and above style_id which was previously dropped)
                        if($this->echoToBeDone){ echo "match with AT,AgeGrp:"; }
                        unset($filters["brandId"]);
                        $scaleArr = func_select_columns_query("mk_size_unification_rules", array("unifiedSize","unifiedSizeValueId"), array_merge($filters, $topFilters), 0, 1, null,"row");
                        if(!empty($scaleArr)) {
                        $unifiedSize = $scaleArr[0][0]; $unifiedSizeValueId = $scaleArr[0][1];
                        if($this->echoToBeDone){ echo "Pass! "; }
                        }
                        else {
                        //we've exhausted all possible match attempts. No representation found.
                        $unifiedSize = null; $unifiedSizeValueId = null;
                        if($this->echoToBeDone){ echo "Failed; "; }
                        }*/
                    }
                }
                $updated = 0;
                if ($unifiedSize!=='' && !empty($unifiedSizeValueId)) {
                    //Get AllSizes for the filter
                    $sizesArr = func_select_columns_query("mk_size_unification_rules", "size", array_merge($filters, array(
                        "unifiedSizeValueId" => $unifiedSizeValueId
                    )), null, null, null, "onecolumnrow");
                    $allSizes = implode(",", $sizesArr);
                    $updated  = $this->updateSizeScaling($productOptionId, $sizeValueId, $unifiedSize, $unifiedSizeValueId, $allSizes);
                }
                $numProdOptsUpdated += $updated;
                if ($this->echoToBeDone) {
                    echo $updated ? "UPDATED SIZE MAPPINGS" : "NO MATCHING SCALE FOUND";
                }
            } //end iteration over $results
        } //end if: not empty results
        
        $this->message = "Finished UpdateSizeMappings ; Total no. of Product Options Updated = $numProdOptsUpdated";
        if($this->echoToBeDone){ echo "\n".$this->message."\n"; }
        //return $this->message;
        $message = $this->message;
        $status = "SUCCESS";
        $statusCode = "200";
        $result = array('Status' => $status,'StatusCode' => $statusCode,'Message' =>$message,'numOptions' => $numProdOptsUpdated );
        return $result;
    } //end function: updateSizeMappings
    
    /**
     * Top Level: UpdateIma handler
     */
    public function updateImage()
    {
        $numStylesForWhichImagesWereAttached = 0;
        $articleTypeAttributes               = $this->getArticleTypeAttributes();
        $allStyles                           = $this->getAllStyles();
        foreach ($allStyles as $eachStyleRow) {
        	
        	$styleId 	 = $eachStyleRow[style_id];
        	$articleTypeId = $eachStyleRow[global_attr_article_type];
        	$ageGroup 	 = $eachStyleRow[global_attr_age_group]; 
            if ($this->echoToBeDone) {
                echo "\nUpdating Image for Style:$styleId:";
            }
            
            $query = "update mk_style_properties set size_representation_image_url = null where style_id = $styleId";
            updateQuery($query);
            
            $retunval = $this->preconditionForUpdateImage($styleId);
            if($retunval) {
            	$attributesArrForComp = $this->getStyleAttributesForComp($styleId,$articleTypeId,$articleTypeAttributes);
            	$retunval = $this->updateSizeChartImage($styleId, $articleTypeId, $attributesArrForComp, $ageGroup, $echoToBedone);
		    if (!$retunval) {
	                $retunval = $this->updateSizeChartImage($styleId, $articleTypeId, $attributesArrForComp,$ageGroup);
	            }
	            if (!$retunval) {
	                $retunval = $this->updateSizeChartImage($styleId, $articleTypeId, $attributesArrForComp);
	            }
	            if (!$retunval) {
		            $this->message = "ERROR:STYLE-IMAGE-FIELDS OR STYLE-IMAGE-RULES NOT MATCHED:$styleId:No match found for the AT to fetch image";
    	        	if($this->echoToBeDone) {
    	        		echo $this->message;
    	        	}
				}
            }
            else {
            	$this->message = "ERROR:MAPPINGS OR MAPPINGS NOT FOUND FOR AT LEAST ONE PRODUCT OPTION FOR STYLE:$styleId:FAILED TO ATTACH SIZE SIZE CHART IMAGE";
            	if($this->echoToBeDone) {
		            echo $this->message;
	            }
            }
            $numStylesForWhichImagesWereAttached += $retunval ? 1 : 0;
        }
        $this->message = "$numStylesForWhichImagesWereAttached out of " . count($allStyles) . " styles updated with new size chart image";
        if($this->echoToBeDone){
        	echo "\n".$this->message;
        } 
        //return $this->message;
        $message = $this->message;
        $status = "SUCCESS";
        $statusCode = "200";
        $result = array('Status' => $status,'StatusCode' => $statusCode,'Message' =>$message,'numOptions' => $numStylesForWhichImagesWereAttached );
        return $result;
    }
    
    /**
     * Get SQL Trail, which is used for restricting the search to specific style-ids, or restrict to incremental unification, if required
     */
    private function getSQLTrail()
    {
        $trail = "";
        if ($this->incremental) {
            $trail .= " and s.size_representation_image_url is null ";
        }
        if (!empty($this->styleIdsStr)) {
            $trail .= " and s.style_id in (" . $this->styleIdsStr . ") ";
        }
        return $trail;
    }
    
    /**
     * Get All Product Options which we intend to Unify
     */
    private function getAllProductOptions()
    {
        $query = "  select p.id as id, p.value as size, p.id_size_chart_scale_value_fk as sizevalueId, p.unified_size_value_id as unifiedSizeValueId, p.unifiedSize as unifiedSize, m.style_id, 
					s.global_attr_article_type as article_type_id, s.global_attr_age_group as global_attr_age_group, a.id as brand_id
					from mk_product_options as p,
					mk_styles_options_skus_mapping as m ,
					mk_style_properties as s,
					mk_attribute_type_values as a,
					mk_catalogue_classification as cc
					where (p.name = 'Size' or p.name = 'Kids Size')
					and a.attribute_type = 'Brand'
					and p.id=m.option_id
					and m.style_id=s.style_id
					and a.attribute_value = s.global_attr_brand
					and s.global_attr_article_type = cc.id
					and s.global_attr_age_group is not null
					and s.global_attr_article_type is not null
					and m.style_id is not null
					and p.value is not null
					";
        //and cc.parent2 in (9,10)            
        $query = $query . $this->getSQLTrail()." order by s.style_id,p.id ";
        if ($this->echoToBeDone){
            echo "\nFetching all product options from DB into array".(empty($this->listOfStyles)?" for all styles":" for style ids:".$this->listOfStyles);
        }
        $this->allProductOptions = func_query($query, true);
        if ($this->echoToBeDone){
            echo "\nFetched " . sizeof($this->allProductOptions) . " Product Options to unify\n";
        }
        return $this->allProductOptions;
    }
    
    /**
     * Get all Styles from DB
     */
    private function getAllStyles()
    {
        if (empty($this->allStyles)) {
            $query = "  select s.style_id,
						s.global_attr_article_type, s.global_attr_age_group
						from mk_style_properties s inner join mk_catalogue_classification cc
						on s.global_attr_article_type = cc.id
						and s.global_attr_age_group is not null
						and s.global_attr_article_type is not null
					";
            //and cc.parent2 in (9,10)
            $query = $query . $this->getSQLTrail(). " order by s.style_id ";
            if ($this->echoToBeDone){
                echo "Fetching Style Data from DB into array".(empty($this->listOfStyles)?" for all styles":" for style ids:").$this->listOfStyles.":";
            }
            $this->allStyles = func_query($query, true);
            if ($this->echoToBeDone){
                echo "Fetched " . sizeof($this->allStyles) . " styles";
            }
        }
        return $this->allStyles;
    }
    
    /**
     * Get all Style Images based on article-type, attributes, age-group
     */
    private function getAllStyleImages()
    {
        $query = "select article_type_id,attribute_field,defaultValue,internalOrder
				  from mk_style_image_fields order by article_type_id,internalOrder asc;";
        return func_query($query, true);
    }
    
    /**
     * Get all Fit Attribute Ids
     */
    private function getAllFitAttributeIds()
    {
        $query = "select id from mk_attribute_type_values where attribute_type_id in (select id from mk_attribute_type where attribute_type = 'Fit');";
        return func_query_column($query, 0, true);
    }
    
    /**
     * Get the article-type specific attribute id applicable for this style
     * This is unused here because we don't use article type specific attribute in matching the measurements so far
     */
    private function getArticleTypeSpecificAttributeId($attributeIds, $styleId)
    {
        $query = "select article_type_attribute_value_id from mk_style_article_type_attribute_values
				  where article_type_attribute_value_id in $attributeIds
				  and product_style_id = $styleId";
        return func_query_first_cell($query, true);
    }
    
    /**
     * Returns the number of rows updated (should be 0 or 1)
     */
    private function updateSizeRepresentation($representation, $productOptionId)
    {
        return func_array2updateWithTypeCheck("mk_product_options", array(
            "size_representation" => $representation
        ), array(
            "id" => $productOptionId
        ));
    }
    
    /**
     * Returns the number of rows updated (should be 0 or 1)
     */
    private function updateSizeScaling($productOptionId, $sizeValueId, $unifiedSize, $unifiedSizeValueId, $allSizes)
    {
        return func_array2updateWithTypeCheck("mk_product_options", array(
        	"id_size_chart_scale_value_fk" => $sizeValueId,
            "unifiedSize" => $unifiedSize,
            "unified_size_value_id" => $unifiedSizeValueId,
            "AllSizes" => $allSizes
        ), array(
            "id" => $productOptionId
        ));
    }
    
    private function getArticleTypeAttributes()
    {
        //Kundan:
        //The other way, more efficient way of doing this would have been using this query:
        //select article_type_id, group_concat(attribute_field), group_concat(defaultValue), group_concat(internalOrder) 
        //from mk_style_image_fields group by article_type_id
        
        $styleImageRows = $this->getAllStyleImages();
        
        //Commend added by Kundan:
        //This is a Map between Article-Type-Id and array of attributes
        //Array of attributes: each attributes is in turn an associative array containing keys as: 
        //attribute, defaultAttribute, order
        //Currently, I see that for each article-type id, there is only one attribute in DB, 
        //but there is a provision for as many attributes as you want. 
        $articleTypeAttributes = array();
        for ($i = 0; !empty($styleImageRows[$i]); $i++) {
            $articleTypeId = $styleImageRows[$i][article_type_id];
            $attributeIndex = 0;
            $attributeIndex = sizeof($articleTypeAttributes[$articleTypeId]);
            $articleTypeAttributes[$articleTypeId][$attributeIndex][attribute] = $styleImageRows[$i][attribute_field];
            $articleTypeAttributes[$articleTypeId][$attributeIndex][defaultAttribute] = $styleImageRows[$i][defaultValue];
            $articleTypeAttributes[$articleTypeId][$attributeIndex][order] = $attributeIndex;
        }
        
        //Commend added by Kundan: for each article-type, get CSV of all attributes into searchField
        foreach ($articleTypeAttributes as $articleTypeId => $row) {
            $searchField = "";
            if (sizeof($row) > 1) {
                foreach ($row as $attrRow) {
                    $searchField .= "'" . $attrRow["attribute"] . "',";
                }
                //remove the last comma in searchField
                $searchField = substr($searchField, 0, strlen($searchField) - 1);
                $articleTypeAttributes[$articleTypeId][searchField] = "(" . $searchField . ")";
                //sort array based on attributevalue
                usort($articleTypeAttributes[$articleTypeId], array(self,cmpAttribute));
            } else {
                $articleTypeAttributes[$articleTypeId][searchField] = "('" . $row[0][attribute] . "')";
            }
        }
        return $articleTypeAttributes;
    }
    
    private function preconditionForUpdateImage($styleId) {
    	
    	//Kundan: For each of the product options which has an SKU Mapping, make sure that they have size_representation as well as unifiedSize
    	//If you find a single such Product Option, then it means either measurement or mapping is missing, and it cannot be unified
    	$sql                = "select po.id from
    						   mk_product_options po inner join mk_styles_options_skus_mapping som on (po.id = som.option_id and som.sku_id is not null)
    					       where po.style = $styleId and (po.size_representation is null or po.unifiedSize is null)";
    	$nonUnifiedProdOpts = getSingleColumnAsArray($sql, true);
    	if (!empty($nonUnifiedProdOpts)) {
    		if ($echoToBedone) {
    			$this->message = "UNIFICATION ERROR:REPRESENTATION-OR-MAPPING-MISSING:$styleId:Representation is null for one or more options of this style";
    	        echo "\n" . $this->message;
    		}
    		return false;
    	}
    	return true;
    }
    
    private function getStyleAttributesForComp($styleId,$articleTypeId,$articleTypeAttributes){
    	$searchField = $this->getSearchField($articleTypeAttributes, $articleTypeId);
    	//Comment added by Kundan: Get me the Attributes & Values applicable for this Style-ID (and article-Type)
    	$query   = "select map.attribute_value as value,attr.attribute_type as type
    				from mk_style_properties as sp,
    				mk_style_article_type_attribute_values as st,mk_attribute_type_values as map,mk_attribute_type as attr
    				where
    				st.article_type_attribute_value_id = map.id
    				and map.attribute_type_id = attr.id
    				and sp.style_id=st.product_style_id
    				and global_attr_article_type=$articleTypeId and sp.style_id =$styleId  and attr.attribute_type in $searchField
    				order by attr.attribute_type";
    	$styleAttributes = getResultsetAsPrimaryKeyValuePair($query, "type", "value", true);
    	        
    	//make an array of attributes: based on which we we search for size-chart-image
    	$attributesArrForComp = $articleTypeAttributes[$articleTypeId];
    	//populate value field in this array with the corresponding attribute value of the style
    	for ($i = 0; !empty($attributesArrForComp[$i]); $i++) {
	    	$attrNameInAT                    = $attributesArrForComp[$i][attribute];
	    	$attrValueForStyle               = $styleAttributes[$attrNameInAT];
	    	$attributesArrForComp[$i][value] = (!empty($attrValueForStyle)) ? $attrValueForStyle : $attributesArrForComp[$i][defaultAttribute];
    	}
    	return $attributesArrForComp;
    }
    
    private function getSearchField($articleTypeAttributes, $articleTypeId){
    	$searchField    = $articleTypeAttributes[$articleTypeId][searchField];
    	if (empty($searchField)) {
    		$searchField = "('Default')";
    	}
    	return $searchField;
    }
    /**
     * Get Attribute Comparison String to be used for query, given Attribute Comparison Array
     * @param unknown_type $attributesArrForComp
     * @return string
     */
    private function getAttributeStringForComp($attributesArrForComp){
    	$compString = "";
    	for ($j = 0; !empty($attributesArrForComp[$j]); $j++) {
    		if ($j != 0) {
    			$compString = $compString . ",";
    		}
    		$compString .= $attributesArrForComp[$j][attribute] . ":" . $attributesArrForComp[$j][value];
    	}
    	$compString = "{" . $compString . "}";
    	return $compString;
    }
    
    private function updateSizeChartImage($styleId, $articleTypeId, &$attributesArrForComp, $ageGroup = "", $echoToBedone = "")
    {
        $this->message  = "";
        $ageGroupClause = empty($ageGroup) ? " and ageGroup is NULL" : " and ageGroup = '{$ageGroup}'";
        
        //Comment added by Kundan:
        //We do a greedy search for size-chart image based on attributeType & attributeValue
        //First we try based on the attributesArrForComp.
        //If Image is found, well & good.
        //Else, set the attributeValue to default value one by one, based on order
        //e.g. say we've three attribute-values: [0] => Collar:Round ; [1] => Fit:Slim ; [2] => Sleeve:Long Sleeve ;
        //and default values of these attributes are: [0] = Collar:Round,order:0 ; [1] = Fit:Regular, order=1 ; [2] => Sleeve:Short Sleeve, order=2 ;
        //so in 1st iteration, it will search for: Collar:Round,Fit:Slim,Sleeve:Long Sleeve
        //if not found, it will search for: Collar:Round,Fit:Regular,Sleeve:Long Sleeve
        //if not found, it will search for: Collar:Round,Fit:Regular,Sleeve:Short Sleeve
        for ($i = 0; !empty($attributesArrForComp[$i]); $i++) {
            $compString = $this->getAttributeStringForComp($attributesArrForComp);
            
            $query    = "select image_url from mk_style_image_rules 
					     where article_type_id=$articleTypeId and attribute_string = '$compString'";
            $query    = $query . $ageGroupClause;
            $imageUrl = func_query_first_cell($query, true);
            
            if (!empty($imageUrl)) {
                $query         = "update mk_style_properties set size_representation_image_url ='$imageUrl' where style_id =$styleId";
                $this->message = "SUCCESS:UNIFICATION-SUCCESSFUL:$styleId";
                if($this->echoToBeDone) {
                	echo $this->message;
                }
                func_query($query);
                return true;
            } else {
                //Comment added by Kundan
                //for the current set of attribute-values, no style image was found
                //so substitute one of the attribute-value with the default value for the attribute
                // for $ith element make it as default
                for ($j = 0; !empty($attributesArrForComp[$j]); $j++) {
                    if ($attributesArrForComp[$j][order] == $i) {
                        $attributesArrForComp[$j][value] = $attributesArrForComp[$j][defaultAttribute];
                    }
                }
            }
        }
        return false;
    }
	/**
	 * Clear Redis Cache and do solr reindexing for selected styles
	 */    
    public function clearCache(){
    	require_once dirname(__FILE__)."/../modules/styleService/PortalRequestHandlerClient.php";
        $styleIdsArray = explode(",",$this->styleIdsStr);
        $response = PortalRequestHandlerClient::refreshStyles($styleIdsArray);
        if($response->status->statusType == "SUCCESS"){
                //Don't do commit: it will auto commit anyways in 15 mins to solr: in the interest of performance
                //return "Solr-Reindex was done & PDP Cache was cleared for ".count($styleIdsArray)." styles: ".$this->styleIdsStr."; PDP Cache was invalidated immediately,though Search Cache will take 15 mins to get invalidated from now"; 
                $message = "Solr-Reindex was done & PDP Cache was cleared for ".count($styleIdsArray)." styles: ".$this->styleIdsStr."; PDP Cache was invalidated immediately,though Search Cache will take 15 mins to get invalidated from now";
                $status = "SUCCESS";
                $statusCode = "200";
                $result = array('Status' => $status,'StatusCode' => $statusCode,'Message' =>$message,'numOptions' => count($styleIdsArray) );
                return $result;
        } else {
                $message = "Error occurred: while Solr-Reindexing and clearing PDP cache for styles: ".$this->styleIdsStr;
                $status = "FAILURE";
                $statusCode = "500";
                $result = array('Status' => $status,'StatusCode' => $statusCode,'Message' =>$message,'numOptions' => count($styleIdsArray) );
                return $result;
                //return "Error occurred: while Solr-Reindexing and clearing PDP cache for styles: ".$this->styleIdsStr;
        }
    }
}

/**
* Runs an Update/Insert/Delete Query and returns the number of rows affected
* @param unknown_type $sql
*/
function updateQuery($sql) {
	$res = db_query($sql);
	return db_affected_rows();
}

/**
 * Performs the update operation in batches:
 * e.g. update mk_style_properties set style_representation_image_url = null
 * will be performed in batches based on the range of the $whereInClauseColumnName
 *
 * @param unknown_type $sql  sql is of the form: "UPDATE TBL SET SOMECOL=SOMEVALUE"
 * @param unknown_type $whereInClauseColumnName of numeric type
 * @param unknown_type $minVal
 * @param unknown_type $maxVal
 * @param unknown_type $numValues
 * @author Kundan
 */
function sqlBatchUpdate($sql, $whereInClauseColumnName, $minVal, $maxVal, $batchSize, $verbose = FALSE) {
	$numRowsUpdated = 0;
	for($i=$minVal;$i<=$maxVal;$i=$i+$batchSize){
		$high = $i+$batchSize - 1;
		$sql2exec = $sql . " WHERE $whereInClauseColumnName between {$i} and {$high}";
		$numRowsUpdated += updateQuery($sql2exec);
		if($verbose) { echo ".";}
	}
	return $numRowsUpdated;
}

/**
 * Top level control invokes this.
 * @param unknown_type $mode:should be one of: ClearAll|UpdateSca|UpdateRep|UpdateIma|UpdateAll
 * @param unknown_type $listOfStyles: comma separated list of style-ids
 * @param unknown_type $echoToBedone
 * @param unknown_type $updateSizeString
 * @param unknown_type $isIncremental
 */
function doSizeUnification($mode, $listOfStyles = "", $echoToBedone = false, $updateSizeString = false, $isIncremental = false)
{
    $sizeUnification = new SizeUnification($listOfStyles, $echoToBedone, $isIncremental);
    if ($mode == "ClearAll") {
        $message = $sizeUnification->clear();
        return $message;
    }
	if ($mode == "UpdateSca" || $mode == "UpdateAll") {
        if ($updateSizeString) {
        	$sizeUnification->updateSizeStringInMappingRules();
        }
        $message = $sizeUnification->updateSizeMappings($echoToBedone);
    }
    if ($mode == "UpdateRep" || $mode == "UpdateAll") {
        if ($updateSizeString) {
        	$sizeUnification->updateSizeStringInRepresentationRules();
        }
        $message = $sizeUnification->updateSizeMeasurements($echoToBedone);
    }
    if ($mode == "UpdateIma" || $mode == "UpdateAll") {
        $message = $sizeUnification->updateImage();
    }
    if ($mode == "ClearCache") {
    	$message = $sizeUnification->clearCache();
    }
    //return $message;
    return $message["Message"];
}

?>
