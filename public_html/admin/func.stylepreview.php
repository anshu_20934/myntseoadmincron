<?php
function changeCustImage($custAreaID,$customizationAreaImage,$largeImage,$actualCustomizeAreaId)
{
	global $sql_tbl,$orderid,$productID;
	$extension = explode(".",$largeImage);
	$extlen = count($extension)-1;
	$imgExt = $extension[$extlen] ;
	$customizationAreaImage ="../".$customizationAreaImage;
	$isJPG = false;
	if ($imgExt == 'jpg' || $imgExt == 'jpeg')
	{
	    $image  = imagecreatefromjpeg($customizationAreaImage);
	    $isJPG = true;
	}else
	{
    	$image  = imagecreatefrompng($customizationAreaImage);
	}
//	echo $customizationAreaImage;
	// fill the background color
    $bg = imagecolorallocate($image, 0, 0, 0);
    $w = imagecolorallocate($image, 255, 255, 255);
    $b = imagecolorallocate($image, 0, 0, 0);
    $style = array($b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,
					$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w);
    imagesetstyle($image,$style);
    imagesetthickness($image,1);
    $designOrientationDetails1=func_get_orientation_for_customization_area($custAreaID);
    $xcuststart = $designOrientationDetails1[0][2];
    $ycuststart = $designOrientationDetails1[0][3];
    $customizationAreaWidth = $designOrientationDetails1[0][4];
	$customizationAreaHeight = $designOrientationDetails1[0][5];
	$orientationID =  $designOrientationDetails1[0][7];
	
	
	
	
	
	/*imagepolygon($image,
	                 array (
	                        $xcuststart, $ycuststart,
	                        $xcuststart, $ycuststart+$customizationAreaHeight,
	                        $xcuststart+$customizationAreaWidth, $ycuststart+$customizationAreaHeight,
	                        $xcuststart+$customizationAreaWidth, $ycuststart
	                 ),
	                 4,
	                 IMG_COLOR_STYLED);*/
	  /*  if ($isJPG){
		$tempBaseFile = '../images/customized/custadmin'.time().'.png';
		    imagejpeg($image,$tempBaseFile);
		}else{
			$tempBaseFile = '../images/customized/custadmin'.time().'.png';
	  		  imagepng($image,$tempBaseFile);
    	}*/
             
	                 
	    $imgPath =    ".".$largeImage   ;   
	     
	    if ($isJPG)
	    {
	        $pasteImage=imagecreatefromjpeg($imgPath);
	    }
	    else
	    {
	        $pasteImage=imagecreatefrompng($imgPath);
		}
		//echo $pasteImage;
		list($width_orig, $height_orig) = getimagesize($imgPath);
    	$ratio_orig = $width_orig/$height_orig;
        $widthMax = max($customizationAreaWidth,$width_orig);
        $heightMax = max($customizationAreaHeight,$height_orig);   
        $pasteimagepath = $imgPath ;
         
        $widthinCA;//width of the image pasted in customization area
        $heightinCA;//height of the image pasted in customization area

        if (($width_orig > $customizationAreaWidth) && ($width_orig >= $height_orig)){

            $widthinCA = $customizationAreaWidth;
            $heightinCA = $widthinCA/$ratio_orig;
            if ($heightinCA > $customizationAreaHeight){
            	$heightinCA = $customizationAreaHeight;
            	$widthinCA = $heightinCA * $ratio_orig;
            }
        }elseif (($height_orig > $customizationAreaHeight) && ($height_orig >= $width_orig)){

            $heightinCA = $customizationAreaHeight;
            $widthinCA = $ratio_orig * $heightinCA;
            if ($widthinCA > $customizationAreaWidth){
            	$widthinCA = $customizationAreaWidth;
            	$heightinCA = $widthinCA/$ratio_orig;
            }

        }elseif (($width_orig <= $customizationAreaWidth) && ($width_orig >= $height_orig)){

            $widthinCA = $width_orig;
            $heightinCA = $widthinCA/$ratio_orig;
            if ($heightinCA > $customizationAreaHeight){
            	$heightinCA = $customizationAreaHeight;
            	$widthinCA = $heightinCA * $ratio_orig;
            }
        }elseif (($height_orig <= $customizationAreaHeight) && ($height_orig >= $width_orig)){

            $heightinCA = $height_orig;
            $widthinCA = $ratio_orig * $heightinCA;
            if ($widthinCA > $customizationAreaWidth){
            	$widthinCA = $customizationAreaWidth;
            	$heightinCA = $widthinCA/$ratio_orig;
            }
        } 
        
               
	  //TODO carry out resizing only for those images which are larger in size than cust area
	  $len =4;
        $imageManipulator = new mkImageTransform();
        $imageManipulator->sourceFile = $pasteimagepath;
	    if ($isJPG){
	        $imageManipulator->jpegOutputQuality = 100;
	        $imageManipulator->targetFile = '../images/customized/custAdminP'.time().get_rand_id($len).'.jpg';
	    }else{
	        $imageManipulator->targetFile ='../images/customized/custAdminP'.time().get_rand_id($len).'.png';
		}
       
        $imageManipulator->maintainAspectRatio = 1;
         $imageManipulator->resizeToWidth = $widthinCA;
        $imageManipulator->resizeToHeight = $heightinCA;
        $imageManipulator->resize();

        if ($isJPG){
	        $pasteImage = imagecreatefromjpeg($imageManipulator->targetFile);
	    }else{
    	    $pasteImage = imagecreatefrompng($imageManipulator->targetFile);
		}
		//delete unneccessary image 
        $pasteImageToDelete =  $imageManipulator->targetFile;
        //store the resized file path in the session array
      //  $custarray['pastefile'] = $imageManipulator->targetFile;
        //by default dx,dy are 0

        $dx = ($customizationAreaWidth - $widthinCA)/2;
        $dy = ($customizationAreaHeight - $heightinCA)/2;

        imagecopyresampled($image, $pasteImage, $xcuststart+$dx,$ycuststart+$dy, 0, 0, $widthinCA, $heightinCA, $widthinCA, $heightinCA);
	   
         $len =4;
	    if ($isJPG){
    	    $tempCustImageFile = "../images/customized/custAdminFinal".time().get_rand_id($len).".jpg";
			imagejpeg($image,$tempCustImageFile);
			$baseimage = imagecreatefromjpeg($tempCustImageFile);
			@unlink($pasteImageToDelete);
	    }else{
    	    $tempCustImageFile = "../images/customized/custAdminFinal".time().get_rand_id($len).".png";
			imagepng($image,$tempCustImageFile);
			$baseimage = imagecreatefrompng($tempCustImageFile);
			@unlink($pasteImageToDelete);
		}
       $tempImageChanged = substr($tempCustImageFile,1);
       $query = "UPDATE $sql_tbl[mk_xcart_order_customized_area_image_rel] SET area_image_final ='".$tempImageChanged."' WHERE orderid = '".$orderid."' AND area_id = '".$actualCustomizeAreaId."' 
	   AND item_id ='".$productID."' ";
	    db_query($query);
       return $tempCustImageFile;
        
}
?>
