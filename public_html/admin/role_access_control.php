<?php
#
# added by nitin sirohi 22-1-2008 
#
include_once $xcart_dir."/include/dao/class/admin/RolesAccessDBAdapter.php";
include_once "$xcart_dir/include/class/admin/RolesAccess.php";

if((php_sapi_name()=='cli'))
{
	return;
}
$isAccessAllowed = false;
$isCSUser= false;
$isCSLead= false;
$smarty->assign("auth_status","NotAuthorized");
$roles=$XCART_SESSION_VARS['user_roles'];
$smarty->assign("roles",$roles);
$ini_array = parse_ini_file("actions.ini");

//get the filename which is being clicked:
$pageurl_request = explode('/admin/', $_SERVER['SCRIPT_NAME']);
$page_request = $pageurl_request[count($pageurl_request)-1];

//create an array of roles the user has
$userRoles = array();
foreach($roles as $role)
{
	$userRoles[] = $role['role'];
	if($role['role']=='CS' || $role['role']=='CST') $isCSUser=true;
	if($role['role']=='CST') $isCSLead=true;

}
$smarty->assign("isCSUser",$isCSUser);
$smarty->assign("isCSLead",$isCSLead);
//if the user is admin by default he can access all pages
if(in_array('AD', $userRoles))
{
	$isAccessAllowed = true;
	$smarty->assign("auth_status","Authorized");
	return;
}
//get the roles which can access that page
$dbApdater = RolesAccessDBAdapter::getInstance();
$userRolesDB = $dbApdater->fetchRolesAccess("pagename = '$page_request'");
if(!empty($userRolesDB))
{
	$roleAllowedForPageStr = array($userRolesDB->getRoles());
	$extraLoginsAllowedForPageStr = array($userRolesDB->getExtraLoginsAllowed());

	$roleAllowedForPage = explode(",", implode(",", $roleAllowedForPageStr));
	$extraLoginsAllowedForPage = explode(",", implode(",", $extraLoginsAllowedForPageStr));

	//check if the users one of roles is in that list
	if(count(array_intersect($userRoles, $roleAllowedForPage)) > 0)
	{
		$isAccessAllowed = true;
		$smarty->assign("auth_status","Authorized");
		return;
	}
	if(in_array($login, $extraLoginsAllowedForPage))
	{
		$isAccessAllowed = true;
		$smarty->assign("auth_status","Authorized");
	}
}
//if access is not allowed then exit and display no access page
if(!$isAccessAllowed)
{
	func_display("admin/home.tpl",$smarty);
	exit;
}
?>
