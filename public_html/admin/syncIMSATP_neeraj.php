<?php

include_once("auth.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/modules/amqpclients/producer/AMQPMessageProducer.php");
include_once("updateInventory.php");
require_once "WMSDBConnUtil.php";

$client = AMQPMessageProducer::getInstance();

echo "[" . date('j-m-Y H:i:s') . "(";

$imsConn = WMSDBConnUtil::getConnection('imsdb1.myntra.com:3306', TRUE, WMSDBConnUtil::IMS_READ);
$atpConn = WMSDBConnUtil::getConnection('atpdbmaster.myntra.com:3306', TRUE, WMSDBConnUtil::IMS_WRITE);
$wmsConn = WMSDBConnUtil::getConnection('wmsdb2.myntra.com:3306', TRUE, WMSDBConnUtil::IMS_READ);
$simConn = WMSDBConnUtil::getConnection("sellerdb2.myntra.com:3307", TRUE, WMSDBConnUtil::IMS_READ);

$to = "wmsteam@myntra.com,nitish.jha@myntra.com,abhijith.pradeep@myntra.com,abhijit.pati@myntra.com,anubhav.agrawal@myntra.com,nitish.jha@myntra.com,arpit.jain@myntra.com,sanjay.yadav@myntra.com,aduri.sabarinath@myntra.com,rahul.kaura@myntra.com,govind.krishnan@myntra.com,narayanan.ps@myntra.com,rajeev.verma1@myntra.com,anshu.verma@myntra.com,bharani.cv@myntra.com,navneet.agarwal@myntra.com,kislay.verma@myntra.com,akanksha.gupta@myntra.com,nikhil.suri@myntra.com,neeraj.gangwar@myntra.com,engg_erp_wms@myntra.com";
$fromHeader = "From: inventorysync@myntra.com";
$subject = "[WMSALERT]ERP Inventory: IMS and ATP";

if (!$imsConn || !$atpConn || !$simConn) {
    die('Could not connect: ' . mysql_error());
}

if (mysql_select_db('myntra_ims', $imsConn) === false) {
    echo "WMS db unaccessible \n";
    exit;
}

if (mysql_select_db('myntra_seller', $simConn) === false) {
    echo "SIM db unaccessible \n";
    exit;
}


if (mysql_select_db('myntra_atp', $atpConn) === false) {
    echo "ATP db unaccessible \n";
    exit;
}

if (mysql_select_db('myntra_wms', $wmsConn) === false) {
    echo "WMS db unaccessible \n";
    exit;
}
$finalSkuQueries = array();
$finalAllSkus = array();
$finalAllDisabledSkusWithInv = array();
$myntraSellerIds = "1,19,21,25,29,30";
$orderEnabledWhIds = "28,36,81,89,93,118,213";

$authHeader = "Authorization: Basic YXBpYWRtaW46bTFudHJhUjBja2V0MTMhIw==";
$appPropBaseUrl = "http://erptools.myntra.com/myntra-tools-service/platform/tools/properties/search/?q=name.eq:";


$appPropSellerUrl = $appPropBaseUrl."wms.myntra.inventory.myntraSeller.ids";
$output = shell_exec("curl -v -H '" . $authHeader . "' -H 'Content-Type:application/xml' -XGET '" . $appPropSellerUrl . "' -d '" . $data . "'");
$xml = simplexml_load_string($output);
if ($xml->status->statusType == "SUCCESS")
    $myntraSellerIds = $xml->data->applicationProperty->value;


$appPropWhUrl = $appPropBaseUrl."wms.myntra.inventory.warehouse.ids";
$output = shell_exec("curl -v -H '" . $authHeader . "' -H 'Content-Type:application/xml' -XGET '" . $appPropWhUrl . "' -d '" . $data . "'");
$xml = simplexml_load_string($output);
if ($xml->status->statusType == "SUCCESS"){
    $orderEnabledWhIds = $xml->data->applicationProperty->value;
}


$skuIds = array();
$handle = fopen($argv[1], "r");
while (($line = fgets($handle)) !== false) {
    array_push($skuIds, $line);
}

#print_r($skuIds);
#exit;

$skuIdsBatches = array_chunk($skuIds, 1000);
echo "Total batches: " . count($skuIdsBatches) . PHP_EOL;

for ($loop = 0; $loop < 1; $loop++) {
    $offset = 0;
    $limit = 3000;
    $hasMore = true;
    $allSkus = array();
    $allDisabledSkusWithInv = array();
    $skuQueries = array();
    $avlOS = 0;
    $invOS = 0;
    $invavlOS = 0;
    $skuBatchIndex = 0;

    while ($hasMore) {

        $warningSkus = array();
        $skuId2AtpInvMap = array();
        $skuId2AtpAvlMap = array();
        $skuId2ImsInvMap = array();
        $skuId2ImsAvlMap = array();

        if (!isset($skuIdsBatches[$skuBatchIndex])) {
            $hasMore = false;
            continue;
        }

        $skuIds = $skuIdsBatches[$skuBatchIndex];
        echo "Running batch " . $skuBatchIndex . PHP_EOL;
        #print_r($skuIds);
        $skuBatchIndex++;

        #$skuIds = array();
        #$skusQuery = "select id from mk_skus where enabled = 1 LIMIT $offset, $limit";
        #$sku_result = mysql_query($skusQuery, $wmsConn);
        #if ($sku_result) {
        #    while ($row = mysql_fetch_array($sku_result, MYSQL_ASSOC)) {
        #        $skuIds[] = $row['id'];
        #    }
        #}
        #@mysql_free_result($sku_result);
        #$skuIds = []
        #if (empty($skuIds)) {
            #$hasMore = false;
        #    continue;
        #}


        $atpInvCountQuery = "select seller_id,sku_id, inventory_count as net_count,available_in_warehouses,enabled,blocked_order_count from inventory where store_id = 1 and seller_id in (" . $myntraSellerIds . ") and supply_type = 'ON_HAND' and sku_id in (" . implode(",", $skuIds) . ")";
        # print $atpInvCountQuery; exit;
        $atpInvResults = mysql_query($atpInvCountQuery, $atpConn);
        if ($atpInvResults) {
            while ($row = mysql_fetch_array($atpInvResults, MYSQL_ASSOC)) {
                $skuId2AtpInvMap[$row['seller_id']][$row['sku_id']] = $row['net_count'];
                $skuId2AtpAvlMap[$row['seller_id']][$row['sku_id']] = $row['available_in_warehouses'];
                if ($row['net_count'] > $row['blocked_order_count'] && $row['enabled'] == 0) {
                    $allDisabledSkusWithInv[] = $row['sku_id'];
                }
            }
        }
        @mysql_free_result($atpInvResults);

        $syncSkus = array();
        $imsInvCountQuery = "select sku_id, seller_id, sum(Greatest(inventory_count,0)) as net_count,group_concat(if(Greatest(inventory_count,0)-Greatest(blocked_order_count,0)>0,warehouse_id,null) separator ',') as avlWh  from wh_inventory where sku_id in (" . implode(",", $skuIds) . ") and warehouse_id in (".$orderEnabledWhIds.") and store_id =1 and seller_id in (" . $myntraSellerIds . ") and supply_type = 'ON_HAND' group by sku_id, seller_id";
        $imsInvResults = mysql_query($imsInvCountQuery, $imsConn);
        if ($imsInvResults) {
            while ($row = mysql_fetch_array($imsInvResults, MYSQL_ASSOC)) {
                $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']] = $row['net_count'];
                $skuId2ImsAvlMap[$row['seller_id']][$row['sku_id']] = $row['avlWh'];
                if (isset($skuId2AtpInvMap[$row['seller_id']][$row['sku_id']]) && ($skuId2AtpInvMap[$row['seller_id']][$row['sku_id']] != $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']] || array_diff(explode(",", $skuId2AtpAvlMap[$row['seller_id']][$row['sku_id']]), explode(",", $skuId2ImsAvlMap[$row['seller_id']][$row['sku_id']])))) {
                    $syncSkus[] = $row['sku_id'] . "_" . $row['seller_id'];
                    if ($skuId2AtpInvMap[$row['seller_id']][$row['sku_id']] != $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']] && array_diff(explode(",", $skuId2AtpAvlMap[$row['seller_id']][$row['sku_id']]), explode(",", $skuId2ImsAvlMap[$row['seller_id']][$row['sku_id']]))) {
                        $invavlOS++;
                        $diff = $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']] - $skuId2AtpInvMap[$row['seller_id']][$row['sku_id']];
                        $skuQueries[] = "update inventory set inventory_count=inventory_count + " . $diff . ",available_in_warehouses='" . $skuId2ImsAvlMap[$row['seller_id']][$row['sku_id']] . "', last_modified_on = now()  where supply_type = 'ON_HAND' and store_id = 1 and seller_id=" . $row['seller_id'] . " and sku_id=" . $row['sku_id'];
                    } else if ($skuId2AtpInvMap[$row['seller_id']][$row['sku_id']] != $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']]) {
                        $invOS++;
                        $diff = $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']] - $skuId2AtpInvMap[$row['seller_id']][$row['sku_id']];
                        $skuQueries[] = "update inventory set inventory_count=inventory_count + " . $diff . ", last_modified_on = now()  where supply_type = 'ON_HAND' and store_id = 1 and seller_id=" . $row['seller_id'] . " and sku_id=" . $row['sku_id'];
                    } else {
                        $avlOS++;
                        $skuQueries[] = "update inventory set available_in_warehouses='" . $skuId2ImsAvlMap[$row['seller_id']][$row['sku_id']] . "', last_modified_on = now()  where supply_type = 'ON_HAND' and store_id = 1 and seller_id=" . $row['seller_id'] . " and sku_id=" . $row['sku_id'];
                    }
                }
            }
        }
        @mysql_free_result($imsInvResults);


        $allSkus = array_merge($allSkus, $syncSkus);

        $offset += $limit;
    }
    if ($i == 0) {
        $finalSkuQueries = $skuQueries;
        $finalAllSkus = $allSkus;
        $finalAllDisabledSkusWithInv = $allDisabledSkusWithInv;
    } else {
        $finalAllSkus = array_intersect($finalAllSkus, $allSkus);
        $finalSkuQueries = array_intersect($finalSkuQueries, $skuQueries);
        $finalAllDisabledSkusWithInv = array_intersect($finalAllDisabledSkusWithInv, $allDisabledSkusWithInv);
    }
}
mysql_close($atpConn);
mysql_close($imsConn);
mysql_close($atpConn);
#if (count($allSkus) > 0) {
    $report = "Out of sync:" . count($finalAllSkus) . "\n";
    $report = $report . "Inv and Aval out of sync: " . $invavlOS . "\n Inv out of sync:" . $invOS . "\n Avl out of sync:" . $avlOS . "\n";
    $report = $report."\n Disabled Skus with sellable Inventory : ".count($finalAllDisabledSkusWithInv)."\n Skus:".implode(",",array_unique($finalAllDisabledSkusWithInv));
    mail($to, $subject, $report . "\n Out of Sync Skus: " . implode("\n", $allSkus), $fromHeader);
    echo $report;
//    echo implode(",", $allSkus);
//    print_r($finalSkuQueries);
#} else {
#    echo "NA";
#}
$errQueries = "";
echo "###errQueries### \n" . $errQueries . "\n";
#updateInventory(null, null, null, null, $finalAllSkus, $finalSkuQueries, null, null, null, $errQueries,false);
if (count($finalAllDisabledSkusWithInv) > 0) {
    $simSku = array();
    $simQuery = "select sku_id, seller_id from seller_item_master where enabled=1 and seller_id in (".$myntraSellerIds.") and sku_id in (" . implode(",", $finalAllDisabledSkusWithInv) . ")";
    $simResuls = mysql_query($simQuery, $simConn);
    if ($simResuls) {
        while ($row = mysql_fetch_array($simResuls, MYSQL_ASSOC)) {
            $simSku[$row['sku_id']] = $row['seller_id'];
        }
        foreach (array_unique($finalAllDisabledSkusWithInv) as $disbaledSkusWithInv) {
            $skuId = $disbaledSkusWithInv;
            if (isset($simSku[$skuId])) {
                $simEntryEntry=array();
                $newEntry = array('skuId' => $skuId, 'sellerId' => $simSku[$skuId],'enabled'=>1);
                $simEntryEntry['newEntry'] =$newEntry;
//                print_r($simEntryEntry);
                $client->sendMessage('imsSyncSellerInventoryQueue', $simEntryEntry, "", 'json', true, 'ims');
            }
        }
    }

}
//echo "###skuQueries### \n".implode("\n",$skuQueries);
echo ")" . date('j-m-Y H:i:s') . "]";
?>

