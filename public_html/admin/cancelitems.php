<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
include_once("$xcart_dir/include/class/oms/ItemManager.php");
include_once("$xcart_dir/include/class/oms/EventCreationManager.php");
require_once $xcart_dir."/include/func/func.order.php";
include_once("$xcart_dir/include/class/oms/OrderWHManager.php");

x_load('db','order', 'mail');

$action = $_POST['action'];

if ($action == 'cancel') {
	
	$response = ItemManager::cancelItems($_POST);
	if($response['is_exchange_order'] === true){
		$orderid = $_POST['orderid'];
		$exchangeMap = func_query_first("select * from exchange_order_mappings where exchange_orderid = $orderid");
		$itemid = $exchangeMap['itemid'];
		$old_orderid = $exchangeMap['releaseid'];
		$old_item = reset(ItemManager::getItemDetailsForItemIds($itemid, $old_orderid));
		$qty_to_cancel = intval($_REQUEST['item_quantity_'.$_REQUEST['itemids']]);
		if($qty_to_cancel < $old_item['amount']){
			$new_item_row = ItemManager::splitItem($old_item, $qty_to_cancel);
			
			// created a new item-row. This one will be used for creating a return
			$item_opt_qty = $new_item_row['item_option_qty'];
			unset($new_item_row['itemid']);
			unset($new_item_row['sku_id']);
			unset($new_item_row['item_option_qty']);
			unset($new_item_row['quantity']);
				
			$new_itemid = func_array2insert('order_details', $new_item_row);
			$item_opt_qty['itemid'] = $new_itemid;
			func_array2insert('mk_order_item_option_quantity', $item_opt_qty);
			
			$old_item_opt_qty = $old_item['item_option_qty'];
			unset($old_item['item_option_qty']);
			unset($old_item['sku_id']);
			unset($old_item['quantity']);
				
			func_array2update('order_details', $old_item, "itemid = ".$old_item['itemid']);
			if($old_item_opt_qty)
				func_array2update('mk_order_item_option_quantity', $old_item_opt_qty, "itemid = ".$old_item['itemid']);
               }
		
		$orderRow = func_query_first("Select group_id from xcart_orders where orderid = ".$exchangeMap['releaseid']);
		EventCreationManager::pushCompleteOrderEvent($orderRow['group_id']);
	}
        \OrderWHManager::stampTax($orderid);
	EventCreationManager::pushCompleteOrderEvent($response['order_groupid']);
        // Order for which tracking number has to be regenerated $_POST['orderid']
        // This order is the release for which tracking number has to be regenerated
        // Get all the order details        
        $order_details = func_select_order($_POST['orderid']);
        if ($order_details['courier_service'] != '' && $order_details['courier_service'] == "FD") {
            db_query("update xcart_orders set courier_service = '".$order_details['courier_service']."', tracking = '' where orderid = '".$_POST['orderid']."' limit 1");
            $result = func_query_first("select status from xcart_orders where orderid= ".$_POST['orderid']);
            if($result['status'] != 'F'){
                $whCourier2OrderId = array("FD" => $_POST['orderid']);
                \OrderWHManager::assignTrackingNumberForOrders($whCourier2OrderId, '', '');
            }
        }
	$smarty->assign("errorMsg", $response['failure']);
	$smarty->assign("successMsg", $response['success']);
	$smarty->assign("action", "complete");
		
} else { 
	// list all the items admin has selected to cancel
	$productsInCart = ItemManager::getProductDetailsForItemIds(explode(",", $_GET['itemids']));
	
	$cancel_reason_info = func_get_item_cancel_reasons(false);
	
	$smarty->assign("cancellation_types",$cancel_reason_info['types']);
	$smarty->assign("cancellation_reasons",$cancel_reason_info['type2reasons']);
	
	$smarty->assign("action", "confirm");
	$smarty->assign("productsInCart", $productsInCart);
	$smarty->assign("itemids", $_GET['itemids']);
	$smarty->assign("orderid", $_GET['orderid']);
}

$smarty->assign("main","cancel_items");
func_display("admin/home.tpl",$smarty);

?>
