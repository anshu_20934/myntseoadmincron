<?php
/**
 * controller file to manage all order related service requests(add,edit,update,close,delete,view)
 * @author: Arun Kumar K
 * legends:
 *      SR - service request
 *      osr - order service request
 */
require_once "auth.php";
require_once $xcart_dir."/include/security.php";
include_once($xcart_dir."/modules/serviceRequest/SROrder.php");
include_once($xcart_dir."/modules/serviceRequest/SRNonOrder.php");

$orderId = (!empty($_GET['orderid']))?$_GET['orderid']:$_POST['orderid'];
$orderSRId = (!empty($_GET['order_sr_id']))?$_GET['order_sr_id']:$_POST['order_sr_id'];
$mode = (!empty($_GET['mode']))?$_GET['mode']:$_POST['mode'];
$SRPostMode = (!empty($_GET['sr_post_mode']))?$_GET['sr_post_mode']:$_POST['sr_post_mode'];

$orderSR = new SROrder();
$orderSR->initializeSRForOrder($orderId);

//all order sr
$allOrderSR = $orderSR->getOrderSR(array('order_id'=>$orderId));

//calculate deviation for all non-closed and delayed(duedate<=currentdate) SR
$currentTime = time();
foreach($allOrderSR as $idx=>$SR){
    if($SR['SR_status'] != 'close'){
        //calculate ageing(current date-created date) for all order SR
        $ageing = $orderSR->convertSRTimeDifference($currentTime-$SR['createtime']);//in days:hours
        $allOrderSR[$idx]['ageing'] = $ageing['d:h'];

        //deviation
        if(!empty($SR['duetime']) && $SR['duetime']<=$currentTime){
            $deviation = $orderSR->convertSRTimeDifference($currentTime-$SR['duetime']);//in days:hours
            $allOrderSR[$idx]['deviation'] = $deviation['d:h'];
        }
    }
}

//cat/subcat
$allCategory = $orderSR->categoryObj->getSRCategory();

foreach($allCategory as $idx=>$cat){
    $allSubCategoryForCat = $orderSR->subCategoryObj->getSRSubCategory($cat['id']);

    //convert SLA to string for sub-category
    foreach($allSubCategoryForCat as $sidx=>$sCat){
        $subTimeString = $orderSR->convertSRTimeDifference($sCat['SLA_in_sec']);
        $allSubCategoryForCat[$sidx]['SLA_string'] = $subTimeString['d:h'];
    }

    $catTimeString = $orderSR->convertSRTimeDifference($cat['SLA_in_sec']);
    $allCategory[$idx]['SLA_string'] = $catTimeString['d:h'];

    $allCategory[$idx]['subCategories'] = $allSubCategoryForCat;
}

//all priority
$allPriority = $orderSR->priorityObj->getSRPriority();

//all status
$allStatus = $orderSR->statusObj->getSRStatus();
//remove status 'close' from the list(for update)
foreach($allStatus as $idx=>$status){
    if(strtolower($status['SR_status']) == 'close'){
        unset($allStatus[$idx]);
    }
}

//all departments
$allDepartment = $orderSR->getSRDepartment();

//all channels
$allChannel = $orderSR->getSRChannel();

//all items in order
$allItemsInorder = $orderSR->item;

//order customer
$orderCustomer = $orderSR->customer;

//all refund types
$refundTypes = array(
    'coupon'=>'Coupon',
    'cash'=>'Cash',
    'cashback'=>'Cashback',
    'cheque'=>'Cheque',
    'online'=>'Online Transfer',    
);

//administrator
$adminFlag = $orderSR->isValidUserRoleToManage($login);

$smarty->assign("sr_category",$allCategory);
$smarty->assign("sr_priority",$allPriority);
$smarty->assign("sr_status",$allStatus);
$smarty->assign("sr_department",$allDepartment);
$smarty->assign("sr_channel",$allChannel);
$smarty->assign("sr_items",$allItemsInorder);
$smarty->assign("order_customer",$orderCustomer);
$smarty->assign("refund_types",$refundTypes);
$smarty->assign("SR_admin",$adminFlag);
$smarty->assign("orderid",$orderId);
$smarty->assign("order_sr_id",$orderSRId);
$smarty->assign("order_sr",$allOrderSR);

switch($mode){

    case 'addServieRequest':
        $smarty->assign("form_type",'addServieRequest');
        $smarty->assign("success","N");

        //handle post form submit
        if($SRPostMode == 1){
            $flag=true;//add status

            $categoryInfo = $orderSR->subCategoryObj->getCategoryOnSubCategory($_POST['subcategory']);
            $categoryId = $categoryInfo[0]['cat_id'];

            //calculate due date based on SLA(of cat/subcategory) and override by subcategory
            $SLA = $categoryInfo[0]['SLA_in_sec'];

            //items selected doe order SR
            $itemCSV = implode(',', $_POST['item']);

            //execute all the subtasks and catch exceptions
            try{
                //check for validity of order SR
                $flag = $orderSR->isValidOrderSR($orderId,$categoryId,$_POST['subcategory']);
                if($flag === false){
                    throw new ServiceRequestException('NOT_VALID_ORDER_SR');
                }

                //callback time validate
                if($_POST['callback'] == 'Y'){
                    $callbackTime = strtotime($_POST['callbacktime']);

                    if($callbackTime < time()){
                        throw new ServiceRequestException('NOT_VALID_CALLBACK_TIME');
                    }

                } else {
                    $callbackTime = null;
                }

                //one or more refund types (csv of types)
                $refundType = implode(',',$_POST['refund_type']);

                //compose refund values(coupon,cash,cashback,cheque and online)
                $refundValue = array();

                //coupon detail if exists
                if(in_array('coupon',$_POST['refund_type'])){
                    if(!empty($_POST['ref_coupon_code'])){
                        $JSONCouponDetail = $orderSR->getJSONCoupon($_POST['ref_coupon_code']);
                        if($JSONCouponDetail === false){
                            throw new ServiceRequestException('REF_COUPON_NOT_EXIST');
                        }

                        //validate coupon
                        $mixedResult = $orderSR->validateCoupon($_POST['ref_coupon_code'], $orderCustomer);//return true/false/message
                        if($mixedResult === false){

                        } else if($mixedResult === true){

                            //in case of exception set the message
                        } else{
                            throw new Exception($mixedResult);
                        }

                        $refundValue[]=array(
                            'type'=>'coupon',
                            'detail'=>json_decode($JSONCouponDetail),
                        );
                    } else {
                        throw new ServiceRequestException('REF_COUPON_DETAIL_ERROR');
                    }
                }

                //cash detail if exists
                if(in_array('cash',$_POST['refund_type'])){
                    if(!empty($_POST['ref_cash_amount']) && !empty($_POST['ref_cash_date'])){
                        $refundValue[]=array(
                            'type'=>'cash',
                            'detail'=>array(
                                'amount'=>$_POST['ref_cash_amount'],
                                'trans_date'=>$_POST['ref_cash_date'],
                                'summary'=>$_POST['ref_cash_description']
                            ),
                        );
                    } else {
                        throw new ServiceRequestException('REF_CASH_DETAIL_ERROR');
                    }
                }

                //cashback detail if exists
                if(in_array('cashback',$_POST['refund_type'])){
                    if(!empty($_POST['ref_cashback_transaction']) && !empty($_POST['ref_cashback_amount']) && !empty($_POST['ref_cashback_date'])){
                        $refundValue[]=array(
                            'type'=>'cashback',
                            'detail'=>array(
                                'transaction'=>$_POST['ref_cashback_transaction'],
                                'amount'=>$_POST['ref_cashback_amount'],
                                'trans_date'=>$_POST['ref_cashback_date'],
                            )
                        );
                    } else {
                        throw new ServiceRequestException('REF_CASHBACK_DETAIL_ERROR');
                    }
                }

                //cheque detail if exists
                if(in_array('cheque',$_POST['refund_type'])){
                    if(!empty($_POST['ref_cheque_name']) && !empty($_POST['ref_cheque_number']) && !empty($_POST['ref_cheque_amount']) && !empty($_POST['ref_cheque_date'])){
                        $refundValue[]=array(
                            'type'=>'cheque',
                            'detail'=>array(
                                'cheque_name'=>$_POST['ref_cheque_name'],
                                'cheque_number'=>$_POST['ref_cheque_number'],
                                'amount'=>$_POST['ref_cheque_amount'],
                                'trans_date'=>$_POST['ref_cheque_date'],
                                'summary'=>$_POST['ref_cheque_description']
                            ),
                        );
                    } else {
                        throw new ServiceRequestException('REF_CHEQUE_DETAIL_ERROR');
                    }
                }

                //online detail if exists
                if(in_array('online',$_POST['refund_type'])){
                    if(!empty($_POST['ref_online_amount']) && !empty($_POST['ref_online_date'])){
                        $refundValue[]=array(
                            'type'=>'online',
                            'detail'=>array(
                                'amount'=>$_POST['ref_online_amount'],
                                'trans_date'=>$_POST['ref_online_date'],
                                'summary'=>$_POST['ref_online_description']
                            ),
                        );
                    } else {
                        throw new ServiceRequestException('REF_ONLINE_DETAIL_ERROR');
                    }
                }

                //check for refund
                if($_POST['refund'] == 'Y' ){
                    if(empty($refundType)){
                        throw new ServiceRequestException('REF_TYPE_ERROR');
                    }else if(empty($refundValue)){
                        throw new ServiceRequestException('REF_VAL_ERROR');
                    }
                }

                //compose json structure of all refund values(coupon,cash,cashback,cheque and online)
                $jsonRefundValue = json_encode($refundValue);

                //insert SR
                $flag = $orderSR->createSR($categoryId, $_POST['subcategory'], $_POST['priority'], $SLA,
                    $_POST['department'], $_POST['channel'], $callbackTime,
                    $reporter=$login, $assignee=null, $copyto=null, $_POST['create_summary'],
                    $noteStatus=1);
                $lastSRId = $flag;
                if(empty($lastSRId)){
                    throw new ServiceRequestException('CREATE_SR_ERROR');
                }

                //insert order SR on successful SR
                $flag = $orderSR->createOrderSR($lastSRId, $itemCSV, $_POST['refund'], $refundType, $jsonRefundValue);
                if(empty($flag)){
                    $orderSR->deleteSRIrreversible($lastSRId);
                    throw new ServiceRequestException('CREATE_ORDER_SR_ERROR');
                }

                //on success of all subtasks
                $smarty->assign("success","Y");

            }catch(ServiceRequestException $SR){
                $smarty->assign("success","N");
                $smarty->assign("message",$SR->getMessage());
                
            }catch(Exception $ex){
                $smarty->assign("success","N");
                $smarty->assign("message",$ex->getMessage());
            }
        }

        func_display("admin/ServiceRequest/manageOrderSR.tpl",$smarty);
        break;


    case 'updateServieRequest':
        $orderServiceRequest = $orderSR->getOrderSR(array('id'=>$orderSRId));

        //parse refund detail to edit        
        $refundValueObj = json_decode($orderServiceRequest[0]['refund_value']);
        $refundDetailForSmarty = array();        
        foreach($refundValueObj as $typeObj){
            //since is an object typecast to array
            $refundDetailForSmarty[$typeObj->type] = (array)$typeObj->detail;
        }
        $smarty->assign("refund",$orderServiceRequest[0]['refund']);
        $smarty->assign("refund_type_detail",$refundDetailForSmarty);

        //format callbacktime according to jquery datetimepicker
        if(!empty($orderServiceRequest[0]['callbacktime'])){
            $smarty->assign("callbackTime",date('m/d/Y H:i',$orderServiceRequest[0]['callbacktime']));
        }

        $smarty->assign("form_type",'updateServieRequest');
        $smarty->assign("sr_order",$orderServiceRequest);

        $smarty->assign("success","N");
        //handle post form submit
        if($SRPostMode == 1){
            $flag=true;//update status

            $categoryInfo = $orderSR->subCategoryObj->getCategoryOnSubCategory($_POST['subcategory']);
            $categoryId = $categoryInfo[0]['cat_id'];

            //calculate due date based on SLA(of cat/subcategory) and override by subcategory
            $SLA = $categoryInfo[0]['SLA_in_sec'];

            //items selected for order SR
            $itemCSV = implode(',', $_POST['item']);

            try{
                //check for validity of order SR
                $flag = $orderSR->isValidOrderSR($orderId,$categoryId,$_POST['subcategory'],$_POST['order_sr_id']);
                if($flag === false){
                    throw new ServiceRequestException('NOT_VALID_ORDER_SR');
                }

                //callback time validate
                if($_POST['callback'] == 'Y'){
                    $callbackTime = strtotime($_POST['callbacktime']);

                    //callback time should not be a past time if is updated to new time
                    if($callbackTime != $orderServiceRequest[0]['callbacktime'] && $callbackTime < time()){
                        throw new ServiceRequestException('NOT_VALID_CALLBACK_TIME');
                    }

                } else {
                    $callbackTime = null;
                }

                //one or more refund types (csv of types)
                $refundType = implode(',',$_POST['refund_type']);

                //compose refund values(coupon,cash,cashback,cheque and online)
                $refundValue = array();

                //coupon detail if exists
                if(in_array('coupon',$_POST['refund_type'])){
                    if(!empty($_POST['ref_coupon_code'])){
                        $JSONCouponDetail = $orderSR->getJSONCoupon($_POST['ref_coupon_code']);
                        if($JSONCouponDetail === false){
                            throw new ServiceRequestException('REF_COUPON_NOT_EXIST');
                        }

                        //validate coupon
                        $mixedResult = $orderSR->validateCoupon($_POST['ref_coupon_code'], $orderCustomer);//return true/false/message
                        if($mixedResult === false){

                        } else if($mixedResult === true){

                        //in case of exception set the message
                        } else{
                            throw new Exception($mixedResult);
                        }

                        $refundValue[]=array(
                            'type'=>'coupon',
                            'detail'=>json_decode($JSONCouponDetail),
                            );
                    } else {
                        throw new ServiceRequestException('REF_COUPON_DETAIL_ERROR');
                    }
                }

                //cash detail if exists
                if(in_array('cash',$_POST['refund_type'])){
                    if(!empty($_POST['ref_cash_amount']) && !empty($_POST['ref_cash_date_edit'])){
                        $refundValue[]=array(
                            'type'=>'cash',
                            'detail'=>array(
                                'amount'=>$_POST['ref_cash_amount'],
                                'trans_date'=>$_POST['ref_cash_date_edit'],
                                'summary'=>$_POST['ref_cash_description']
                                ),
                            );
                    } else {
                        throw new ServiceRequestException('REF_CASH_DETAIL_ERROR');
                    }
                }

                //cashback detail if exists
                if(in_array('cashback',$_POST['refund_type'])){
                    if(!empty($_POST['ref_cashback_transaction']) && !empty($_POST['ref_cashback_amount']) && !empty($_POST['ref_cashback_date_edit'])){
                        $refundValue[]=array(
                            'type'=>'cashback',
                            'detail'=>array(
                                'transaction'=>$_POST['ref_cashback_transaction'],
                                'amount'=>$_POST['ref_cashback_amount'],
                                'trans_date'=>$_POST['ref_cashback_date_edit'],
                                )
                            );
                    } else {
                        throw new ServiceRequestException('REF_CASHBACK_DETAIL_ERROR');
                    }
                }

                //cheque detail if exists
                if(in_array('cheque',$_POST['refund_type'])){
                    if(!empty($_POST['ref_cheque_name']) && !empty($_POST['ref_cheque_number']) && !empty($_POST['ref_cheque_amount']) && !empty($_POST['ref_cheque_date_edit'])){
                        $refundValue[]=array(
                            'type'=>'cheque',
                            'detail'=>array(
                                'cheque_name'=>$_POST['ref_cheque_name'],
                                'cheque_number'=>$_POST['ref_cheque_number'],
                                'amount'=>$_POST['ref_cheque_amount'],
                                'trans_date'=>$_POST['ref_cheque_date_edit'],
                                'summary'=>$_POST['ref_cheque_description']
                                ),
                            );
                    } else {
                        throw new ServiceRequestException('REF_CHEQUE_DETAIL_ERROR');
                    }
                }

                //online detail if exists
                if(in_array('online',$_POST['refund_type'])){
                    if(!empty($_POST['ref_online_amount']) && !empty($_POST['ref_online_date_edit'])){
                        $refundValue[]=array(
                            'type'=>'online',
                            'detail'=>array(
                                'amount'=>$_POST['ref_online_amount'],
                                'trans_date'=>$_POST['ref_online_date_edit'],
                                'summary'=>$_POST['ref_online_description']
                                ),
                            );
                    } else {
                        throw new ServiceRequestException('REF_ONLINE_DETAIL_ERROR');
                    }
                }

                //check for refund
                if($_POST['refund'] == 'Y' ){
                    if(empty($refundType)){
                        throw new ServiceRequestException('REF_TYPE_ERROR');
                    }else if(empty($refundValue)){
                        throw new ServiceRequestException('REF_VAL_ERROR');
                    }
                }

                //compose json structure of all refund values(coupon,cash,cashback,cheque and online)
                $jsonRefundValue = json_encode($refundValue);

                //update SR (taken care of 'close' status and changing from 'close' status)
                $flag = $orderSR->updateSR( $orderServiceRequest[0]['SR_id'], $updatedBy=$login, $categoryId,
                                            $_POST['subcategory'], $_POST['priority'], $_POST['status'], $SLA,
                                            $_POST['department'], $_POST['channel'], $callbackTime,
                                            $assignee=null, $copyto=null, $_POST['create_summary'], $_POST['comment'],
                                            $noteStatus=null);
                if($flag === false){
                    throw new ServiceRequestException('UPDATE_SR_ERROR');
                }

                //update order SR on successful SR
                $flag = $orderSR->updateOrderSR($orderServiceRequest[0]['id'], $itemCSV,
                                                $_POST['refund'], $refundType, $jsonRefundValue );
                if($flag === false){
                    throw new ServiceRequestException('UPDATE_ORDER_SR_ERROR');
                }

                //on success of all subtasks
                $smarty->assign("success","Y");

            }catch(ServiceRequestException $SR){
                $smarty->assign("success","N");
                $smarty->assign("message",$SR->getMessage());
                
            }catch(Exception $ex){
                $smarty->assign("success","N");
                $smarty->assign("message",$ex->getMessage());
            }

        }

        func_display("admin/ServiceRequest/manageOrderSR.tpl",$smarty);
        break;


    case 'closeServieRequest':
        $orderServiceRequest = $orderSR->getOrderSR(array('id'=>$orderSRId));

        //parse refund detail to edit        
        $refundValueObj = json_decode($orderServiceRequest[0]['refund_value']);
        $refundDetailForSmarty = array();
        foreach($refundValueObj as $typeObj){
            //since is an object typecast to array
            $refundDetailForSmarty[$typeObj->type] = (array)$typeObj->detail;
        }
        $smarty->assign("refund",$orderServiceRequest[0]['refund']);
        $smarty->assign("refund_type_detail",$refundDetailForSmarty);

        $smarty->assign("form_type",'closeServieRequest');
        $smarty->assign("success","N");
        $smarty->assign("sr_order",$orderServiceRequest);

        //handle post form submit
        if($SRPostMode == 1){
            $flag=true;//close status

            try{
                //one or more refund types (csv of types)
                $refundType = implode(',',$_POST['refund_type']);

                //compose refund values(coupon,cash,cashback,cheque and online)
                $refundValue = array();

                //coupon detail if exists
                if(in_array('coupon',$_POST['refund_type'])){
                    if(!empty($_POST['ref_coupon_code'])){
                        $JSONCouponDetail = $orderSR->getJSONCoupon($_POST['ref_coupon_code']);
                        if($JSONCouponDetail === false){
                            throw new ServiceRequestException('REF_COUPON_NOT_EXIST');
                        }

                        //validate coupon
                        $mixedResult = $orderSR->validateCoupon($_POST['ref_coupon_code'], $orderCustomer);//return true/false/message
                        if($mixedResult === false){

                        } else if($mixedResult === true){

                        //in case of exception set the message
                        } else{
                            throw new Exception($mixedResult);
                        }

                        $refundValue[]=array(
                            'type'=>'coupon',
                            'detail'=>json_decode($JSONCouponDetail),
                            );
                    } else {
                        throw new ServiceRequestException('REF_COUPON_DETAIL_ERROR');
                    }
                }

                //cash detail if exists
                if(in_array('cash',$_POST['refund_type'])){
                    if(!empty($_POST['ref_cash_amount']) && !empty($_POST['ref_cash_date_close'])){
                        $refundValue[]=array(
                            'type'=>'cash',
                            'detail'=>array(
                                'amount'=>$_POST['ref_cash_amount'],
                                'trans_date'=>$_POST['ref_cash_date_close'],
                                'summary'=>$_POST['ref_cash_description']
                                ),
                            );
                    } else {
                        throw new ServiceRequestException('REF_CASH_DETAIL_ERROR');
                    }
                }

                //cashback detail if exists
                if(in_array('cashback',$_POST['refund_type'])){
                    if(!empty($_POST['ref_cashback_transaction']) && !empty($_POST['ref_cashback_amount']) && !empty($_POST['ref_cashback_date_close'])){
                        $refundValue[]=array(
                            'type'=>'cashback',
                            'detail'=>array(
                                'transaction'=>$_POST['ref_cashback_transaction'],
                                'amount'=>$_POST['ref_cashback_amount'],
                                'trans_date'=>$_POST['ref_cashback_date_close'],
                                )
                            );
                    } else {
                        throw new ServiceRequestException('REF_CASHBACK_DETAIL_ERROR');
                    }
                }

                //cheque detail if exists
                if(in_array('cheque',$_POST['refund_type'])){
                    if(!empty($_POST['ref_cheque_name']) && !empty($_POST['ref_cheque_number']) && !empty($_POST['ref_cheque_amount']) && !empty($_POST['ref_cheque_date_close'])){
                        $refundValue[]=array(
                            'type'=>'cheque',
                            'detail'=>array(
                                'cheque_name'=>$_POST['ref_cheque_name'],
                                'cheque_number'=>$_POST['ref_cheque_number'],
                                'amount'=>$_POST['ref_cheque_amount'],
                                'trans_date'=>$_POST['ref_cheque_date_close'],
                                'summary'=>$_POST['ref_cheque_description']
                                ),
                            );
                    } else {
                        throw new ServiceRequestException('REF_CHEQUE_DETAIL_ERROR');
                    }
                }

                //online detail if exists
                if(in_array('online',$_POST['refund_type'])){
                    if(!empty($_POST['ref_online_amount']) && !empty($_POST['ref_online_date_close'])){
                        $refundValue[]=array(
                            'type'=>'online',
                            'detail'=>array(
                                'amount'=>$_POST['ref_online_amount'],
                                'trans_date'=>$_POST['ref_online_date_close'],
                                'summary'=>$_POST['ref_online_description']
                                ),
                            );
                    } else {
                        throw new ServiceRequestException('REF_ONLINE_DETAIL_ERROR');
                    }
                }

                //check for refund
                if($_POST['refund'] == 'Y' ){
                    if(empty($refundType)){
                        throw new ServiceRequestException('REF_TYPE_ERROR');
                    }else if(empty($refundValue)){
                        throw new ServiceRequestException('REF_VAL_ERROR');
                    }
                }

                //compose json structure of all refund values(coupon,cash,cashback,cheque and online)
                $jsonRefundValue = json_encode($refundValue);
                
                //one or more comp. types (csv of types)
                $compensationType = implode(',',$_POST['compensation_type']);

                //compose compensation values(coupon,gift and cashback)
                $compensationValue = array();

                //coupon detail if exists
                if(in_array('coupon',$_POST['compensation_type'])){
                    if(!empty($_POST['coupon_code'])){
                        $JSONCouponDetail = $orderSR->getJSONCoupon($_POST['coupon_code']);
                        if($JSONCouponDetail === false){
                            throw new ServiceRequestException('COMP_COUPON_NOT_EXIST');
                        }

                        //validate coupon
                        $mixedResult = $orderSR->validateCoupon($_POST['coupon_code'], $orderCustomer);//return true/false/message
                        if($mixedResult === false){

                        } else if($mixedResult === true){

                        //in case of exception set the message
                        } else{
                            throw new Exception($mixedResult);
                        }

                        $compensationValue[]=array(
                            'type'=>'coupon',
                            'detail'=>json_decode($JSONCouponDetail),
                            );
                    } else {
                        throw new ServiceRequestException('COMP_COUPON_DETAIL_ERROR');
                    }
                }

                //gift detail if exists
                if(in_array('gift',$_POST['compensation_type'])){
                    if(!empty($_POST['gift_description'])){
                        $compensationValue[]=array(
                            'type'=>'gift',
                            'detail'=>array('description'=>$_POST['gift_description']),
                            );
                    } else {
                        throw new ServiceRequestException('COMP_GIFT_DETAIL_ERROR');
                    }
                }                

                //check for compensation
                if($_POST['compensation'] == 'Y' ){
                    if(empty($compensationType)){
                        throw new ServiceRequestException('COMP_TYPE_ERROR');
                    }else if(empty($compensationValue)){
                        throw new ServiceRequestException('COMP_VAL_ERROR');
                    }
                }

                //compose json structure of all compensation values(coupon,gift and cashback)
                $jsonCompensationValue = json_encode($compensationValue);

                //close SR
                $flag = $orderSR->closeSR( $orderServiceRequest[0]['SR_id'],$resolver=$login, $_POST['close_summary'],
                                               $copyto=null, $noteStatus=null, $_POST['MFBSendStatus']);
                if($flag === false){
                    throw new ServiceRequestException('CLOSE_SR_ERROR');
                }

                //update order SR for compensation
                $flag = $orderSR->updateOrderSR($orderServiceRequest[0]['id'], $itemCSV=null,
                                                $_POST['refund'], $refundType, $jsonRefundValue,
                                                $_POST['compensation'], $compensationType, $jsonCompensationValue);
                if($flag === false){
                    throw new ServiceRequestException('UPDATE_ORDER_SR_ERROR');
                }

                //on success of all subtasks
                $smarty->assign("success","Y");

            } catch(ServiceRequestException $SR){
                $smarty->assign("success","N");
                $smarty->assign("message",$SR->getMessage());

            } catch(Exception $ex){
                $smarty->assign("success","N");
                $smarty->assign("message",$ex->getMessage());
            }

        }

        func_display("admin/ServiceRequest/manageOrderSR.tpl",$smarty);
        break;


    case 'deleteServieRequest':
        $orderServiceRequest = $orderSR->getOrderSR(array('id'=>$orderSRId));
        $flag=true;//delete status

        try{

            //check for already deleted
            if($orderServiceRequest[0]['sr_active'] == 'N'){
                throw new ServiceRequestException('DELETED_SR');
            }

            //check for user authenticity and delete SR
            $flag = $orderSR->deleteSR( $orderServiceRequest[0]['SR_id'],$deletedBy=$login);
            if($flag === false){
                throw new ServiceRequestException('DELETE_SR_ERROR');
            }

            //on success of all subtasks
            echo "Service Request is deleted successfully";

        }catch(ServiceRequestException $SR){
            echo $SR->getMessage();
        }
        break;


    case 'getCompCoupon':
        if(!empty($_POST['coupon']) || !empty($_GET['coupon'])){
            $coupon = (!empty($_POST['coupon']))?$_POST['coupon']:$_GET['coupon'];
            $jsonCouponDetail = $orderSR->getJSONCoupon($coupon);
            echo $jsonCouponDetail; //false//JSON

        } else{
            echo false;
        }
        break;


    case 'validateCoupon':
        if((!empty($_POST['coupon']) || !empty($_GET['coupon'])) && (!empty($_POST['orderCustomer']) || !empty($_GET['orderCustomer']))){
            $coupon = (!empty($_POST['coupon']))?$_POST['coupon']:$_GET['coupon'];
            $customer = (!empty($_POST['orderCustomer']))?$_POST['orderCustomer']:$_GET['orderCustomer'];
            $result = $orderSR->validateCoupon($coupon, $customer);//return true/exceptionKey
            echo $result;//false/true/message

        } else {
            echo 'Coupon or order customer is blank, please enter right couon code';
        }
        break;
}