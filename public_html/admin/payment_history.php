<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: payment_history.php,v 1.15 2006/01/11 06:55:58 mclap Exp $
#

require "./auth.php";
require $xcart_dir."/include/security.php";

$ctime = time() + $config["Appearance"]["timezone_offset"];

$start_date = mktime (0,0,0,date("m",$ctime),1,date("Y",$ctime));
$end_date = $ctime;

if ($mode=="go") {
	$partner_condition = ($partner ? " AND $sql_tbl[partner_payment].login='$partner'" : "");

	if($StartMonth) {
		$start_date=mktime(0,0,0,$StartMonth,$StartDay,$StartYear);
		$end_date=mktime(23,59,59,$EndMonth,$EndDay,$EndYear);
	}

	$query = "SELECT $sql_tbl[partner_payment].*, $sql_tbl[customers].* FROM $sql_tbl[partner_payment], $sql_tbl[customers] WHERE $sql_tbl[partner_payment].paid='Y' AND $sql_tbl[partner_payment].login=$sql_tbl[customers].login AND $sql_tbl[partner_payment].add_date>='$start_date' AND $sql_tbl[partner_payment].add_date<='$end_date' $partner_condition ORDER BY $sql_tbl[partner_payment].add_date desc";

	$total_history = count(func_query($query));

	# Nav bar code
	$objects_per_page = 10;

	$total_nav_pages = ceil ($total_history/$objects_per_page)+1;
	require $xcart_dir."/include/navigation.php";
	$smarty->assign ("navigation_script", "payment_history.php?StartMonth=$StartMonth&StartDay=$StartDay&StartYear=$StartYear&EndMonth=$EndMonth&EndDay=$EndDay&EndYear=$EndYear&partner=$partner&mode=go");

	$smarty->assign ("history", func_query ("$query LIMIT $first_page, $objects_per_page"));
}

$partners = func_query ("SELECT * FROM $sql_tbl[customers] WHERE usertype='B' ORDER BY lastname");
$smarty->assign ("partners", $partners);

$smarty->assign ("history", $history);
$smarty->assign ("partner", $partner);
$smarty->assign("start_date",$start_date);
$smarty->assign("end_date",$end_date);
$smarty->assign ("main", "payment_history");

# Assign the current location line
$smarty->assign("location", $location);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
