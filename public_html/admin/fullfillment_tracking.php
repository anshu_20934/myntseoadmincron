<?php
include_once("auth.php");
//include_once("$xcart_dir/include/security.php");

// Model

/**
 * @param  $days
 * @return #Fexecute_sql_fetch_first|?
 */
function get_in_house_orders($days) {

    $sql = 'select count(*) as count from (
    select  o.orderid, d.location as location, count(location) as actual, count(distinct location) as location_count
    from
    xcart_orders o, xcart_order_details d
    where
    d.orderid = o.orderid and o.completeddate between  (UNIX_TIMESTAMP() - (86400 * '.$days.')) and  UNIX_TIMESTAMP()
    group by
    o.orderid having location_count = 1 ) AS T where location = 3;';

    return execute_sql_fetch_first($sql);
}

/**
 * @param  $days
 * @return #Fexecute_sql_fetch_first|?
 */
function get_avg_in_house($days) {

    $sql = 'select avg(completeddate - queueddate) as average from (
    select o.completeddate, o.queueddate, o.orderid , d.location as location , count(location) as actual, count(distinct location) as location_count
    from
    xcart_orders o, xcart_order_details d
    where
    d.orderid = o.orderid and o.completeddate between  (UNIX_TIMESTAMP() - (86400 * '.$days.')) and  UNIX_TIMESTAMP()
    group by
    o.orderid having location_count = 1 ) AS T where location = 3;';

    return execute_sql_fetch_first($sql);
}

/**
 * @param  $start - numbers of days
 * @param  $end  - numbers of days
 * @param  $days - numbers of days
 * @return #Fexecute_sql_fetch_first|?
 */
function get_shipped_in_house($start, $end, $days) {

    $sql = 'select count(*) as count from (
       select o.completeddate, o.queueddate , o.orderid ,  d.location as location ,count(location) as actual ,count(distinct location) as location_count
       from
       xcart_orders o, xcart_order_details d
       where
       d.orderid = o.orderid and o.completeddate between  (UNIX_TIMESTAMP() - (86400 * '.$days.')) and  UNIX_TIMESTAMP() and
       (o.completeddate - o.queueddate) between  (86400 * '.$start.' ) and  (86400 * '.$end.')
       group by
       o.orderid having location_count = 1 ) AS T where location = 3;';


    return execute_sql_fetch_first($sql);
}

/**
 * @param  $start
 * @param  $end
 * @param  $days
 * @return #Fexecute_sql_fetch_first|?
 */
function get_sla_in_house($days) {

    $sql = 'select count(*) as count  from (
    select  o.orderid, d.location as location, count(location) as actual, count(distinct location) as location_count
    from
    xcart_orders o, xcart_order_details d
    where
    d.orderid = o.orderid and o.completeddate between  (UNIX_TIMESTAMP() - (86400 * '.$days.')) and  UNIX_TIMESTAMP() and
    (d.estimated_processing_date - o.completeddate) < 0
    group by
    o.orderid having location_count = 1 ) AS T where location = 3;';

    return execute_sql_fetch_first($sql);
}

/**
 * @param  $days
 * @return #Fexecute_sql_fetch_first|?
 */
function get_out_sourced_orders($days) {

    $sql = 'select count(*) as count from (
    select  o.orderid,  d.location as location, count(location) as actual, count(distinct location) as location_count
    from
    xcart_orders o, xcart_order_details d
    where
    d.orderid = o.orderid and o.completeddate between  (UNIX_TIMESTAMP() - (86400 * '.$days.')) and  UNIX_TIMESTAMP()
    group by
    o.orderid )
    AS T
    where
    ( location_count = 1 and location != 3 ) or (location_count > 1);';

    return execute_sql_fetch_first($sql);
}

/**
 * @param  $days
 * @return #Fexecute_sql_fetch_first|?
 */
function get_avg_out_sourced($days) {

    $sql = 'select avg(completeddate - queueddate) as average from (
    select o.completeddate, o.queueddate, o.orderid ,  d.location as location ,count(location) as actual ,count(distinct location) as location_count
    from
    xcart_orders o, xcart_order_details d
    where
    d.orderid = o.orderid and o.completeddate between  (UNIX_TIMESTAMP() - (86400 * '.$days.')) and  UNIX_TIMESTAMP()
    group by
    o.orderid )
    AS T
    where
    ( location_count = 1 and location != 3 ) or (location_count > 1);';

    return execute_sql_fetch_first($sql);
}

/**
 * @param  $start - numbers of days
 * @param  $end  - numbers of days
 * @param  $days - numbers of days
 * @return #Fexecute_sql_fetch_first|?
 */
function get_shipped_out_sourced($start, $end, $days) {

    $sql = 'select count(*) as count from (
    select  o.orderid ,  d.location as location ,count(location) as actual ,count(distinct location) as location_count
    from
    xcart_orders o, xcart_order_details d
    where
    d.orderid = o.orderid and o.completeddate between  (UNIX_TIMESTAMP() - (86400 * '.$days.')) and  UNIX_TIMESTAMP() and
    (o.completeddate - o.queueddate) between  (86400 * '.$start.') and  (86400 * '.$end.')
    group by
    o.orderid )
    AS T
    where
    ( location_count = 1 and location != 3 ) or (location_count > 1);';


    return execute_sql_fetch_first($sql);
}

/**
 * @param  $start
 * @param  $end
 * @param  $days
 * @return #Fexecute_sql_fetch_first|?
 */
function get_sla_out_sourced($days) {

    $sql = 'select count(*) as count from (
    select  o.orderid ,  d.location as location ,count(location) as actual ,count(distinct location) as location_count
    from
    xcart_orders o, xcart_order_details d
    where
    d.orderid = o.orderid and o.completeddate between  (UNIX_TIMESTAMP() - (86400 * '.$days.')) and  UNIX_TIMESTAMP()  and
    (d.estimated_processing_date - o.completeddate ) < 0
    group by
    o.orderid )
    AS T
    where
    ( location_count = 1 and location != 3 ) or (location_count > 1);';

    return execute_sql_fetch_first($sql);
}

//
$days = array(1, 7, 30);

// In house data ..
$orders = array() ;

foreach($days as $day){
    $result = get_in_house_orders($day);
    $orders[$day] = $result['count'];
}

//
$avg = array();

foreach($days as $day){
    $result =  get_avg_in_house($day);
    $avg[$day] = $result['average'] / (3600);
}

//
$shipped_24 = array();

foreach($days as $day){
    $result =  get_shipped_in_house(0, 1, $day);
    $shipped_24 [$day] = ($result['count'] / ($orders[$day] == 0 ? 1 : $orders[$day])  ) * 100;
}


//
$shipped_24_48 = array();

foreach($days as $day){
    $result =  get_shipped_in_house(1, 2, $day);
    $shipped_24_48 [$day] =  ($result['count'] /($orders[$day] == 0 ? 1 : $orders[$day])  ) * 100;
}

//
$shipped_48_72 = array();

foreach($days as $day){
    $result =  get_shipped_in_house(2, 3, $day);
    $shipped_48_72 [$day] =  ($result['count'] / ($orders[$day] == 0 ? 1 : $orders[$day])  ) * 100;
}

//
$shipped_72 = array();

foreach($days as $day){
    $shipped_72 [$day] =  100 - ($shipped_48_72[$day] + $shipped_24_48[$day] + $shipped_24[$day] ); 
}

//
$shipped_outside = array();

foreach($days as $day){
    $result =  get_sla_in_house($day);
    $shipped_outside [$day] =  ($result['count'] / ($orders[$day] == 0 ? 1 : $orders[$day]) ) * 100;

}


// Out sourced data ..
$out_sourced_orders = array() ;

foreach($days as $day){
    $result = get_out_sourced_orders($day);
    $out_sourced_orders[$day] = $result['count'];
}


//
$out_sourced_avg = array();

foreach($days as $day){
    $result =  get_avg_out_sourced($day);
    $out_sourced_avg[$day] = $result['average'] / (3600);
}

//
$out_sourced_shipped_24 = array();

foreach($days as $day){
    $result =  get_shipped_out_sourced(0, 1, $day);
    $out_sourced_shipped_24 [$day] = ($result['count'] / ($out_sourced_orders[$day] == 0 ? 1 : $out_sourced_orders[$day])  ) * 100;
}


//
$out_sourced_shipped_24_48 = array();

foreach($days as $day){
    $result =  get_shipped_out_sourced(1, 2, $day);
    $out_sourced_shipped_24_48 [$day] =  ($result['count'] / ($out_sourced_orders[$day] == 0 ? 1 : $out_sourced_orders[$day]) ) * 100;
}

//
$out_sourced_shipped_48_72 = array();

foreach($days as $day){
    $result =  get_shipped_out_sourced(2, 3, $day);
    $out_sourced_shipped_48_72 [$day] =  ($result['count'] / ($out_sourced_orders[$day] == 0 ? 1 : $out_sourced_orders[$day])  ) * 100;
}

//
$out_sourced_shipped_72 = array();

foreach($days as $day){
    $out_sourced_shipped_72 [$day] = 100 - ($out_sourced_shipped_48_72[$day] + $out_sourced_shipped_24_48 [$day] + $out_sourced_shipped_24[$day]) ; 
}

$out_sourced_shipped_outside = array();

foreach($days as $day){
    $result =  get_sla_out_sourced($day);
    $out_sourced_shipped_outside [$day] =  ($result['count'] / ($out_sourced_orders[$day] == 0 ? 1 : $out_sourced_orders[$day]) ) * 100;

}


// View ..

$column = array('','Yesterday', 'Last 7 days', 'Last 30 Days');

$message ='<h3> IN HOUSE ORDERS </h3>';
$message .='<table cellpadding="3" cellspacing="0" width="600px" border="1">';
$message .='<tr style="background-color:#94b6f9;">';
foreach($column as $value) {
    $message .='<td style="text-align: center;font-size:14px;font-weight:bold;">'.$value.'</td>';
}
$message .='</tr>';

$message .='<tr style="background-color:\'#d0d8e8,#e9edf4\'"><td style="text-align: left;">Total # Orders Shipped</td>';
foreach($orders as $value){
    $message .='<td style="text-align: center;">'.$value.'</td>';
}
$message .='</tr>';

$message .='<tr style="background-color:\'#d0d8e8,#e9edf4\'"><td style="text-align: left;"># Average fulfilment time [Completion Date - Queued Date]</td>';
foreach($avg as $value){
    $message .='<td style="text-align: center;">'.round($value).'</td>';
}
$message .='</tr>';

$message .='<tr style="background-color:\'#d0d8e8,#e9edf4\'"><td style="text-align: left;"># % of orders shipped within 24 hours</td>';
foreach($shipped_24 as $value){
    $message .='<td style="text-align: center;">'.round($value).'</td>';
}
$message .='</tr>';

$message .='<tr style="background-color:\'#d0d8e8,#e9edf4\'"><td style="text-align: left;"># % of Orders shipped within 24-48 hours</td>';
foreach($shipped_24_48 as $value){
    $message .='<td style="text-align: center;">'.round($value).'</td>';
}
$message .='</tr>';

$message .='<tr style="background-color:\'#d0d8e8,#e9edf4\'"><td style="text-align: left;"># % of Orders shipped within 48-72 hours</td>';
foreach($shipped_48_72 as $value){
    $message .='<td style="text-align: center;">'.round($value).'</td>';
}
$message .='</tr>';

$message .='<tr style="background-color:\'#d0d8e8,#e9edf4\'"><td style="text-align: left;"># % of Orders shipped after 72 hours</td>';
foreach($shipped_72 as $value){
    $message .='<td style="text-align: center;">'.round($value).'</td>';
}
$message .='</tr>';

$message .='<tr style="background-color:\'#d0d8e8,#e9edf4\'"><td style="text-align: left;">% Orders shipped outside SLA</td>';
foreach($shipped_outside as $value){
    $message .='<td style="text-align: center;">'.round($value).'</td>';
}
$message .='</tr>';

$message .='</table>';


$message .='<h3> OUT SOURCED ORDERS </h3>';
$message .='<table cellpadding="3" cellspacing="0" width="600px" border="1">';
$message .='<tr style="background-color:#94b6f9;">';
foreach($column as $value) {
    $message .='<td style="text-align: center;font-size:14px;font-weight:bold;">'.$value.'</td>';
}
$message .='</tr>';

$message .='<tr style="background-color:\'#d0d8e8,#e9edf4\'"><td style="text-align: left;">Total # Orders Shipped</td>';
foreach($out_sourced_orders as $value){
    $message .='<td style="text-align: center;">'.$value.'</td>';
}
$message .='</tr>';

$message .='<tr style="background-color:\'#d0d8e8,#e9edf4\'"><td style="text-align: left;"># Average fulfilment time [Completion Date - Queued Date]</td>';
foreach($out_sourced_avg as $value){
    $message .='<td style="text-align: center;">'.round($value).'</td>';
}
$message .='</tr>';

$message .='<tr style="background-color:\'#d0d8e8,#e9edf4\'"><td style="text-align: left;"># % of orders shipped within 24 hours</td>';
foreach($out_sourced_shipped_24 as $value){
    $message .='<td style="text-align: center;">'.round($value).'</td>';
}
$message .='</tr>';

$message .='<tr style="background-color:\'#d0d8e8,#e9edf4\'"><td style="text-align: left;"># % of Orders shipped within 24-48 hours</td>';
foreach($out_sourced_shipped_24_48 as $value){
    $message .='<td style="text-align: center;">'.round($value).'</td>';
}
$message .='</tr>';

$message .='<tr style="background-color:\'#d0d8e8,#e9edf4\'"><td style="text-align: left;"># % of Orders shipped within 48-72 hours</td>';
foreach($out_sourced_shipped_48_72 as $value){
    $message .='<td style="text-align: center;">'.round($value).'</td>';
}
$message .='</tr>';

$message .='<tr style="background-color:\'#d0d8e8,#e9edf4\'"><td style="text-align: left;"># % of Orders shipped after 72 hours</td>';
foreach($out_sourced_shipped_72 as $value){
    $message .='<td style="text-align: center;">'.round($value).'</td>';
}
$message .='</tr>';

$message .='<tr style="background-color:\'#d0d8e8,#e9edf4\'"><td style="text-align: left;">% Orders shipped outside SLA</td>';
foreach($out_sourced_shipped_outside as $value){
    $message .='<td style="text-align: center;">'.round($value).'</td>';
}
$message .='</tr>';

$message .='</table>';

echo $message;

?>