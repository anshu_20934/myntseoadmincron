<?php 
require_once( "../auth.php");
require_once($xcart_dir."/include/security.php");
require_once($xcart_dir."/include/SimpleImage.php");
require_once($xcart_dir."/include/func/func.db.php");
require_once($xcart_dir."/include/func/func.utilities.php");
include_once"$xcart_dir/utils/imagetransferS3.php";

include_once HostConfig::$documentRoot ."/Profiler/Profiler.php";
use imageUtils\Jpegmini;

$table = 'mk_widget_banners';
$SIZE_X = 550;
$SIZE_Y = 210;

$mode = $_POST["mode"];
$url = $_POST["url"];
$alt = $_POST["alt"];
$anchor = $_POST["anchor"];
$banner = $_POST["banner"];

if ($mode == "save") 
{
    if(!empty($banner)){
    	foreach ( $banner as $key=>$banner_arr){
    		$banner_id = $key;
    		$url = $banner_arr["url"];
    		$alt = $banner_arr["alt"];
    		$anchor = $banner_arr["anchor"];
    		
    		$updateWidgetQuery = "UPDATE mk_widget_banners SET url='$url' , alt='$alt', anchor='$anchor' where id=$key";
    		db_query($updateWidgetQuery);
    	}
    }
} elseif ( $mode == "add" ) {
    $file_name = $HTTP_POST_FILES['image']['name'];
	$tmp_file = $HTTP_POST_FILES['image']['tmp_name'];
	
	$lastPositionQuery = "select max(display_order) as max from mk_widget_banners  where banner_type='old'";
	$res = db_query($lastPositionQuery);
	if( mysql_num_rows($res) == 0 ){
		$lastPosition = 1;
	} else {
		$lastPositionRes = mysql_fetch_assoc($res);
		$lastPosition = $lastPositionRes["max"] + 1;
	}
	
    if ($tmp_file && !empty($url)) {
        $image = new SimpleImage();
        $image->load($tmp_file);
        $file_location = 'images/banners/' . time() . '-' . $file_name;
        $index = strrpos($file_name,".");
        $filePath = $xcart_dir ."/". $file_location;
        $image->save($filePath);
        if(moveToDefaultS3Bucket($file_location) && Jpegmini::copyCompressedPlaceholderFile($file_location)&& myntra\utils\cdn\imagetransferS3::compress($file_location)){
    	    $insertSql = "INSERT INTO mk_widget_banners(url, anchor, alt, display_order,banner_type,image) values('$url', '$anchor', '$alt', $lastPosition,'old','$cdn_base/$file_location')";
            $res = db_query(($insertSql));
            Profiler::increment("imagesize-homepage-banner", filesize($filePath));
        }else{
            $weblog->error("Failed while compressing banner image $filePath or moving images : $file_location ");
            $errorlog->error("Failed while compressing banner image $filePath or moving images : $file_location ");
        }
    }
} elseif ( $mode == "delete" ) {
	if( !empty($banner_id) ) {
		$sql = "DELETE FROM mk_widget_banners where id=$banner_id ";
		db_query($sql);
	}
} elseif ( $mode == "move_up" ) {
	if(!empty($banner_id)) {
		$getPositionQuery  = "select display_order from mk_widget_banners where id=$banner_id";
		$result = func_query_first($getPositionQuery);
		if(!empty($result)) {
			$position = $result['display_order'];
			if($position>1) {
				$updateQuery = "update mk_widget_banners set display_order=display_order-1 where display_order=$position and id=$banner_id";
		        db_query($updateQuery);
		        $earlierposition = $position - 1;
				$updateQuery2 = "update mk_widget_banners set display_order=display_order+1 where display_order=$earlierposition and id!=$banner_id and banner_type='old'";
				db_query($updateQuery2);
			}
		}
	}
} elseif ( $mode == "move_down" ) {
	if(!empty($banner_id)) {
		$getLastPositionQuery  = "select max(display_order) as max from mk_widget_banners  where banner_type='old'";
		$resultLast = func_query_first($getLastPositionQuery);
		$positionLast = $resultLast['max'];
		
		$getPositionQuery  = "select display_order from mk_widget_banners where id=$banner_id";
		$result = func_query_first($getPositionQuery);
		if(!empty($result)) {
			$position = $result['display_order'];
			if($position<$positionLast) {
				$updateQuery = "update mk_widget_banners set display_order=display_order+1 where display_order=$position and id=$banner_id";
		        db_query($updateQuery);
		        $earlierposition = $position + 1;
				$updateQuery2 = "update mk_widget_banners set display_order=display_order-1 where display_order=$earlierposition and id!=$banner_id  and banner_type='old'";
				db_query($updateQuery2);
			}
		}
	}
}

$data = array();
$banners = func_select_query($table, array('banner_type' => 'old'), null, null, array('display_order' => 'asc'));

$smarty->assign("banners",$banners);
$smarty->assign("main","banners_widget");
func_display("admin/home.tpl",$smarty);
?>
