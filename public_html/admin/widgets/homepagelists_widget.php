<?php 
require "../auth.php";
require_once $xcart_dir."/include/security.php";

if ($mode == "save") 
{
    if(!empty($list)){
    	//checks if there are wrong sku ids or blank sku ids in update
       	$wrongSkuIdFlag = false;
        $errorMessage = "";
        $blankSkuIdFlag = false;
        foreach ( $list as $key=>$list_arr){
                if(isset($list_arr["skus"])){
                        $sku_list = $list_arr["skus"];
                        foreach($sku_list as $key=>$sku){
                        //if sku is not an integer or blank
                                $sku = trim($sku);
                                if (preg_match("/[^0-9]/",$sku) || $sku == '') {
                                        //blank sku id
                                        if($sku == ''){
                                                if(!$blankSkuIdFlag) {
                                                        $errorMessage = "Please delete the entries with blank Sku Id before updating other. ".$errorMessage;
                                                        $blankSkuIdFlag = true;
                                                }
                                        }
                                        else{
                                        	if(!$wrongSkuIdFlag){
                                        		$wrongSkuIdFlag = true;
                                        		$errorMessage = $errorMessage."Following Sku Ids are not correct:(";
                                        	}
                                                $errorMessage = $errorMessage . $sku . ", ";
                                        }
                                }
                        }
                }
        }
        if($wrongSkuIdFlag) $errorMessage = substr($errorMessage, 0, -2)."). Please correct it before updating.";
        if($blankSkuIdFlag) $wrongSkuIdFlag = true;        
        if($wrongSkuIdFlag){
                $top_message["content"] = $errorMessage;
                $top_message["type"]="E";
        }
        else {
    	foreach ( $list as $key=>$list_arr){
    		$widget_id = $key;
    		$widget_name = $list_arr["list_name"];
    		$updateWidgetQuery = "UPDATE mk_widget SET widget_name='$widget_name' where id=$widget_id";
    		db_query($updateWidgetQuery);
    		if(isset($list_arr["skus"])){
    			$sku_list = $list_arr["skus"];
    			foreach($sku_list as $key=>$sku){
    				$updateSkuQuery = "UPDATE mk_widget_sku_list SET sku='". $sku ."' where id=$key";
    				db_query($updateSkuQuery);
    			}
    			$top_message["content"] = "Sku Ids successfully updated.";
    		} elseif ( isset($list_arr["tag"]) ){
    			$tag_arr = $list_arr["tag"];
				foreach($tag_arr as $tagid=>$tag_data){
					$updateTagQuery = "UPDATE mk_widget_tag SET tag='" . $tag_data["tag"] . "' , `limit`=" . $tag_data["limit"] . "  where id=$tagid";
					db_query($updateTagQuery);
				}
    		} elseif (isset($list_arr["query"]) ){
    			$querytext = $list_arr["query"];
    			$dbsafequery = mysql_real_escape_string($querytext);
				$updateQueryText = "UPDATE mk_widget_query SET querytext='$dbsafequery' where widget_id=$widget_id";
				db_query($updateQueryText);
    		}
		}
		}    	
    }
} elseif ( $mode == "move_list_up" ){
	if(!empty($widget_id) ) {
		$getPositionQuery  = "select display_order from mk_widget_page_mapping where widget_id=$widget_id";
		$result = func_query_first($getPositionQuery);
		if(!empty($result)) {
			$position = $result['display_order'];
			if($position>1) {
				$updateQuery = "update mk_widget_page_mapping set display_order=display_order-1 where display_order=$position and widget_id=$widget_id";
		        db_query($updateQuery);
		        $earlierposition = $position - 1;
				$updateQuery2 = "update mk_widget_page_mapping set display_order=display_order+1 where display_order=$earlierposition and widget_id!=$widget_id and page_name='home'";
				db_query($updateQuery2);
			}
		}
	} 
} elseif ( $mode == "move_list_down" ){
	if(!empty($widget_id) ) {
		$getLastPositionQuery  = "select max(display_order) as max from mk_widget_page_mapping where page_name='home'";
		$resultLast = func_query_first($getLastPositionQuery);
		$positionLast = $resultLast['max'];
		
		$getPositionQuery  = "select display_order from mk_widget_page_mapping where widget_id=$widget_id";
		$result = func_query_first($getPositionQuery);
		if(!empty($result)) {
			$position = $result['display_order'];
			if($position<$positionLast) {
				$updateQuery = "update mk_widget_page_mapping set display_order=display_order+1 where display_order=$position and widget_id=$widget_id";
		        db_query($updateQuery);
		        $earlierposition = $position + 1;
				$updateQuery2 = "update mk_widget_page_mapping set display_order=display_order-1 where display_order=$earlierposition and widget_id!=$widget_id and page_name='home'";
				db_query($updateQuery2);
			}
		}
	} 
} elseif ( $mode == "delete_list") {
	if ( !empty($widget_id) ) {
		$deletePageQuery = "delete from mk_widget_page_mapping where widget_id=$widget_id";
		db_query($deletePageQuery);
		$selectTypeQuery = "select widget_type from mk_widget where id=$widget_id";
		$result = db_query($selectTypeQuery);
		$type = mysql_fetch_assoc($result);
		$type = $type["widget_type"];
		if($type == "sku"){
			$deleteSkusQuery = "delete from mk_widget_sku_list where widget_id=$widget_id";
			db_query($deleteSkusQuery);
		} elseif ( $type == "tag" ) {
			$deleteTagQuery = "delete from mk_widget_tag where widget_id=$widget_id";
			db_query($deleteTagQuery);
		}
		$deleteWidgetQuery = "delete from mk_widget where id=$widget_id";
		db_query($deleteWidgetQuery);
	}
} elseif ($mode == "add_list" ){
	if( !empty($widget_type) || !empty($widget_name) ){
		if($widget_type == "tag"){
			if(!empty($widget_tag) && !empty($widget_limit)){
				$sql = "INSERT INTO mk_widget(widget_name, widget_type) VALUES ('$widget_name', '$widget_type')";
				db_query($sql);
				$wid = db_insert_id();
				if(!empty($wid)){
					$sql = "INSERT INTO mk_widget_tag(widget_id, tag, `limit`) VALUES ($wid, '$widget_tag', $widget_limit)";
					db_query($sql);
					$maxDisplayOrderQuery = "SELECT max(display_order) as max from mk_widget_page_mapping where page_name='home'";
					$res = db_query($maxDisplayOrderQuery);
					$max = mysql_fetch_assoc($res);
					if(empty($max)){
						$display_order = 1;
					} else {
						$display_order = $max["max"] + 1;
					}
					$insertToPageMappingQuery = "INSERT INTO mk_widget_page_mapping(widget_id, page_name, display_order) VALUES($wid, 'home', $display_order)";
					db_query($insertToPageMappingQuery);
				}
				
			}
		} elseif ( $widget_type == "sku" ) {
			$sql = "INSERT INTO mk_widget(widget_name, widget_type) VALUES ('$widget_name', '$widget_type')";
			db_query($sql);
			$sql = "SELECT * from mk_widget where widget_name = '$widget_name' and widget_type='$widget_type'";
			$res = db_query($sql);
			$widget_id_res = mysql_fetch_assoc($res);
			$wid = $widget_id_res["id"];
			if(!empty($wid)){
				$maxDisplayOrderQuery = "SELECT max(display_order) as max from mk_widget_page_mapping where page_name='home'";
				$res = db_query($maxDisplayOrderQuery);
				$max = mysql_fetch_assoc($res);
				if(empty($max)){
					$display_order = 1;
				} else {
					$display_order = $max["max"] + 1;
				}
				$insertToPageMappingQuery = "INSERT INTO mk_widget_page_mapping(widget_id, page_name, display_order) VALUES($wid, 'home', $display_order)";
				db_query($insertToPageMappingQuery);
			}
		} elseif ($widget_type == "query") {
			if(!empty($widget_query)) {
				$sql = "INSERT INTO mk_widget(widget_name, widget_type) VALUES ('$widget_name', '$widget_type')";
				db_query($sql);
				$wid = db_insert_id();
				if(!empty($wid)){
					$sql = "INSERT INTO mk_widget_query(widget_id, querytext) VALUES ($wid, '$widget_query')";
					db_query($sql);
					$maxDisplayOrderQuery = "SELECT max(display_order) as max from mk_widget_page_mapping where page_name='home'";
					$res = db_query($maxDisplayOrderQuery);
					$max = mysql_fetch_assoc($res);
					if(empty($max)){
						$display_order = 1;
					} else {
						$display_order = $max["max"] + 1;
					}
					$insertToPageMappingQuery = "INSERT INTO mk_widget_page_mapping(widget_id, page_name, display_order) VALUES($wid, 'home', $display_order)";
					db_query($insertToPageMappingQuery);
				}		
			}
		}
	}
} elseif ( $mode == "move_sku_up" ){
	if(!empty($widget_id) && !empty($skuid)) {
		$getPositionQuery  = "select display_order from mk_widget_sku_list where widget_id=$widget_id and id=$skuid";
		$result = func_query_first($getPositionQuery);
		if(!empty($result)) {
			$position = $result['display_order'];
			if($position>1) {
				$updateQuery = "update mk_widget_sku_list set display_order=display_order-1 where display_order=$position and widget_id=$widget_id and id=$skuid";
		        db_query($updateQuery);
		        $earlierposition = $position - 1;
				$updateQuery2 = "update mk_widget_sku_list set display_order=display_order+1 where display_order=$earlierposition and widget_id=$widget_id and id!=$skuid";
				db_query($updateQuery2);
			}
		}
	} 
} elseif ( $mode == "move_sku_down" ){
	if(!empty($widget_id) ) {
		$getLastPositionQuery  = "select max(display_order) as max from mk_widget_sku_list where widget_id=$widget_id";
		$resultLast = func_query_first($getLastPositionQuery);
		$positionLast = $resultLast['max'];
		
		$getPositionQuery  = "select display_order from mk_widget_sku_list where widget_id=$widget_id and id=$skuid";
		$result = func_query_first($getPositionQuery);
		if(!empty($result)) {
			$position = $result['display_order'];
			if($position<$positionLast) {
				$updateQuery = "update mk_widget_sku_list set display_order=display_order+1 where display_order=$position and widget_id=$widget_id and id=$skuid";
		        db_query($updateQuery);
		        $earlierposition = $position + 1;
				$updateQuery2 = "update mk_widget_sku_list set display_order=display_order-1 where display_order=$earlierposition and id!=$widget_id and id!=$skuid";
				db_query($updateQuery2);
			}
		}
	} 
} elseif ( $mode == "delete_sku") {
	if ( !empty($widget_id) && !empty($skuid)) {
		$deletePageQuery = "delete from mk_widget_sku_list where widget_id=$widget_id and id=$skuid";
		db_query($deletePageQuery);
	}
} elseif ( $mode == "add_sku") {
	//check if the style id is not integer
        if(!empty($skuid)) {
        	$skuid = trim($skuid);
                if (!preg_match("/[^0-9]/",$skuid) && $skuid !== '') {
                        if(!empty($widget_id)){
                                $getLastPositionQuery = "SELECT max(display_order) as max from mk_widget_sku_list where widget_id=$widget_id";
                                $topDisplayOrderRes = db_query($getLastPositionQuery);
                                $topDisplayOrder = mysql_fetch_assoc($topDisplayOrderRes);
                                if(empty($topDisplayOrder)){
                                        $display_order = 1;
                                }
                                else {
                                        $display_order = $topDisplayOrder["max"] + 1;
                                }
                                $insertQuery = "INSERT INTO mk_widget_sku_list(widget_id, sku, display_order) VALUES($widget_id, '". $skuid ."', $display_order)";
                                db_query($insertQuery);
                                $top_message["content"] = "Sku Id ('".$skuid."') successfully added.";
                        }
                }
                else {
                        if($skuid == '') $top_message["content"] = "Entered Sku Id is blank. Please enter correct Sku Id";
                        else $top_message["content"] = "Entered Sku Id ('".$skuid."') is not a correct Sku Id.";
                        $top_message["type"]="E";
                }
        }
	/*
	if ( !empty($add_sku) ) {
		foreach($add_sku as $key=>$sku_arr){
			if(!empty($add_sku[$key])){
				$getLastPositionQuery = "SELECT max(display_order) as max from mk_widget_sku_list where widget_id=$key";
				$topDisplayOrderRes = db_query($getLastPositionQuery);
				$topDisplayOrder = mysql_fetch_assoc($topDisplayOrderRes);
				if(empty($topDisplayOrder)){
					$display_order = 1;
				} else {
					$display_order = $topDisplayOrder["max"] + 1;
				}
				$insertQuery = "INSERT INTO mk_widget_sku_list(widget_id, sku, display_order) VALUES($key, '". $add_sku[$key] ."', $display_order)";
				db_query($insertQuery);
			}
		}
	}
	*/
} elseif ( $mode == "add" ) {
	if(!empty($link_url) && !empty($link_name) ) {
		$sql = "INSERT INTO mk_widget_most_popular(link_name, link_url, is_permanent) VALUES('$link_name', '$link_url', $is_permanent)";
		db_query($sql);
	}
} elseif ( $mode == "delete" ) {
	if( !empty($update_link_id) ) {
		$sql = "DELETE FROM mk_widget_most_popular where id=$update_link_id ";
		db_query($sql);
		
	}
}

$data = array();
$sql = "SELECT * from mk_widget as w INNER JOIN mk_widget_page_mapping as wpm on (w.id = wpm.widget_id) where page_name = 'home' order by display_order";

$res = db_query($sql);
while($widget_arr = mysql_fetch_assoc($res)){
	$data[] = array("widget_id"=>$widget_arr["widget_id"], "display_order"=>$widget_arr["display_order"], "widget_type"=>$widget_arr["widget_type"],
					"widget_name"=>$widget_arr["widget_name"] );
}

foreach ($data as $key=>$widget) {
	if($widget["widget_type"] == "sku"){
		$sql = "SELECT * from mk_widget_sku_list where widget_id=".$widget["widget_id"] . " order by display_order";
		$sku_result = db_query($sql);
		$sku_list =  array();
		while($sku_arr = mysql_fetch_array($sku_result)){
			$sku_list[] = array("sku"=>$sku_arr["sku"], "display_order"=>$sku_arr["display_order"], "skuid"=>$sku_arr["id"]);
		}
		$data[$key]["skus"] = $sku_list;
	} elseif($widget["widget_type"] == "tag") {
		$sql = "SELECT * from mk_widget_tag where widget_id=".$widget["widget_id"];
		$tag_result = db_query($sql);
		$tag_arr = mysql_fetch_assoc($tag_result);
		$data[$key]["tag"] = $tag_arr["tag"];
		$data[$key]["limit"] = $tag_arr["limit"];
		$data[$key]["tagid"] = $tag_arr["id"];
	} elseif($widget["widget_type"] == "query") {
		$sql = "SELECT * from mk_widget_query where widget_id=".$widget["widget_id"];
		$query_result = db_query($sql);
		$query_arr = mysql_fetch_assoc($query_result);
		$data[$key]["query"] = stripslashes($query_arr["querytext"]);
	}
}

$smarty->assign ("top_message", $top_message);
$smarty->assign("data",$data);
$smarty->assign("main","homepagelists_widget");
func_display("admin/home.tpl",$smarty);
?>