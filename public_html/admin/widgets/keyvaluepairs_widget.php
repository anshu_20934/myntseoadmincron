<?php 

require "../auth.php";

require $xcart_dir."/include/security.php";

include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");


$mailBody = "<div>Change Initiated by : '$login'  xid: '$XCARTSESSID'</div>"; 

if(isset($mode)){
	//Get old values for logging in case there is some action
	$oldResultsArr = WidgetKeyValuePairs::getAllWidgetKeyValuePairs();
	$oldResults = array();
	foreach($oldResultsArr as $result){
	    $oldResults[$result['key']] = $result['value'];
	}
}

if($mode == 'add') {
/*
	$newkey = $_POST['key_field_new'];

	$newvalue = $_POST['value_field_new'];

	$newdesc = $_POST['description_field_new'];

		

	$insert_arr = array("`key`"=>mysql_real_escape_string($newkey), "`value`"=>mysql_real_escape_string($newvalue), "`description`"=>mysql_real_escape_string($newdesc));

		

	func_array2insertWithTypeCheck_log("mk_widget_key_value_pairs", $insert_arr, true, array("`user_name`"=>$login));

*/	



	$newkey = $_POST['key_field_new'];

	$newvalue = $_POST['value_field_new'];

	$newdesc = $_POST['description_field_new'];

	$mailBody .= "<div>Key Added</div>";
	$mailBody .= "<div>New Key : $newkey</div>";
	$mailBody .= "<div>New Value : $newvalue</div>";
	$mailBody .= "<div>New Description : $newdesc</div>";

	

	$newkeyvalueQuery = "insert into `mk_widget_key_value_pairs` (`key`, `value`, `description`) values 

						 ('" . mysql_real_escape_string($newkey) . "', '" . mysql_real_escape_string($newvalue) . "', 

						  '" . mysql_real_escape_string($newdesc) . "')";

						  

	db_query($newkeyvalueQuery);

	

	WidgetKeyValuePairs::refreshKeyValuePairsInCache();

	

} else if($mode == 'delete') {

	$keyid = $_POST['keyvalue_id'];


	if(!empty($keyid)) {

		

/*		$delete_arr = array("`id`"=>$keyid);

		func_array2deleteWithTypeCheck_log("mk_widget_key_value_pairs", $delete_arr, true, array("`user_name`"=>$login));		*/

	

		$deleteQuery = "delete from mk_widget_key_value_pairs where id=$keyid";

		db_query($deleteQuery);

		$mailBody .= "<div>Deleting key : $keyid</div>";
	}

	WidgetKeyValuePairs::refreshKeyValuePairsInCache();



} else if($mode == 'update') {

	$updatekeysarray = $_POST['update_key_field'];

	$updatevaluesarray = $_POST['update_value_field'];

	$updatedescriptionsarray = $_POST['update_description_field'];

	$updatedCheckBox = $_POST['update_checkbox_field'];


	foreach($updatekeysarray AS $id=>$keyname) {

	if($updatedCheckBox[$id] == "on") {

		$keyvalueid = $id;

		$updatekeyname = mysql_real_escape_string($keyname);

		$updatevaluefield = mysql_real_escape_string(trim($updatevaluesarray[$id]));

		$updatedescriptionfield =  mysql_real_escape_string($updatedescriptionsarray[$id]);

        if(strcmp(trim($updatevaluesarray[$id]),$oldResults[$updatekeyname]) != 0 ){
            $mailBody .= "<div>Changed Key : $updatekeyname</div>";
            $mailBody .= "<div>Changed Val : $updatevaluefield</div>";
        }

		/*$update_arr = array("`key`" => $updatekeyname, "`value`" => $updatevaluefield, "`description`" => $updatedescriptionfield);

		$where_arr = array("`id`" => $keyvalueid);

		func_array2updateWithTypeCheck_log("mk_widget_key_value_pairs", $update_arr, $where_arr, true, array("`user_name`"=>$login));*/

        $sql = "UPDATE mk_widget_key_value_pairs SET `key`='$updatekeyname', `value`='$updatevaluefield', description='$updatedescriptionfield' 

        		WHERE id=$keyvalueid";

        db_query($sql);

    }

	}

	

	WidgetKeyValuePairs::refreshKeyValuePairsInCache();

}

// Send email only when there is something to report
if(isset($mode) && strlen($mailBody) > 50){
	@mail(EmailListsConfig::$nocAndPortalPlatform, 'Notification : Widget Key Value Pairs changed', $mailBody, "Content-Type: text/html; charset=ISO-8859-1 " . "\n");
}


$results = WidgetKeyValuePairs::getAllWidgetKeyValuePairs();



$smarty->assign("keyvaluepairs",  $results);

$smarty->assign("main","keyvaluepairs_widget");

func_display("admin/home.tpl",$smarty);

?>
