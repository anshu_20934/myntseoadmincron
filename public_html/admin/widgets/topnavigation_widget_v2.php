<?php 
require "../auth.php";
require $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/class/widget/class.widget.topnavv2.php";

$tableName="mk_widget_top_nav_v2";

if ($mode == "add_link" ) 
{
	if ( !empty($link_name) && !empty($link_url) && !empty($parent_id) ){
		$sql = "Select max(display_order) from $tableName where parent_id = $parent_id";
		$res = db_query($sql);
		if(mysql_num_rows($res) == 0){
			$display_order = 1;
		} else {
			$display_order = mysql_fetch_array($res);
			$display_order = $display_order[0] + 1;
		}
		$sql = "INSERT INTO $tableName( parent_id, display_order, link_name, link_url ) VALUES ( $parent_id, $display_order, '$link_name', '$link_url' ) ";
		db_query($sql);
	}
} else if($mode == "move_link_up"){
	if(!empty($move_link_id) && !empty($move_link_parent_id)) {
		$getPositionQuery  = "select display_order from $tableName where id=$move_link_id";
		$result = func_query_first($getPositionQuery);
		if(!empty($result)) {
			$position = $result['display_order'];
			if($position>1) {
				$updateQuery = "update $tableName set display_order=display_order-1 where display_order=$position and id=$move_link_id";
		        db_query($updateQuery);
		        $earlierposition = $position - 1;
				$updateQuery2 = "update $tableName set display_order=display_order+1 where display_order=$earlierposition and id!=$move_link_id and parent_id=$move_link_parent_id";
				db_query($updateQuery2);
			}
		}
    }
} else if($mode == "move_link_down"){
	if(!empty($move_link_id) && !empty($move_link_parent_id)) {
		$getLastPositionQuery  = "select max(display_order) as max from $tableName where parent_id=$move_link_parent_id";
		$resultLast = func_query_first($getLastPositionQuery);
		$positionLast = $resultLast['max'];
		
		$getPositionQuery  = "select display_order from $tableName where id=$move_link_id";
		$result = func_query_first($getPositionQuery);
		if(!empty($result)) {
			$position = $result['display_order'];
			if($position<$positionLast) {
				$updateQuery = "update $tableName set display_order=display_order+1 where display_order=$position and id=$move_link_id";
		        db_query($updateQuery);
		        $earlierposition = $position + 1;
				$updateQuery2 = "update $tableName set display_order=display_order-1 where display_order=$earlierposition and id!=$move_link_id and parent_id=$move_link_parent_id";
				db_query($updateQuery2);
			}
		}
    }
} elseif($mode == "link_delete"){
	if(!empty($move_link_id) && !empty($move_link_parent_id)) {
		$getPositionQuery  = "select display_order from $tableName where id=$move_link_id";
		$result = func_query_first($getPositionQuery);
		$position = $result['display_order'];
		$delete_ids = array($move_link_id);
		$index = 1;
		while(true){
			$getDeleteIdsQuery = "select id from $tableName where parent_id in (". implode( ',', $delete_ids) .") and id not in (". implode( ',', $delete_ids) .") ";
			$res = db_query($getDeleteIdsQuery);
			if(mysql_num_rows($res) == 0){
				break; // Exit the loop as no additional data is retrieved
			}
			while($id_arr = mysql_fetch_assoc($res)){
				$delete_ids[] = $id_arr["id"];
			}
		}
		$deleteQuery = "delete from $tableName where id in (" . implode( ',', $delete_ids) .") ";
        db_query($deleteQuery);
        $updatePositionQuery = "update $tableName set display_order=display_order-1 where display_order>$position and parent_id=$move_link_parent_id";
        db_query($updatePositionQuery);
    }
} elseif ($mode == "save") {
	foreach($link_names as $key=>$value){
		$link_name = $value["link_name"];
        $sql = "UPDATE $tableName SET link_name='" . $link_name . "' WHERE id=$key";
        db_query($sql);
		
	}
	foreach($link_urls as $key=>$value){
		$link_url = $value["link_url"];
        $sql = "UPDATE $tableName SET link_url='" . $link_url . "' WHERE id=$key";
        db_query($sql);
		
	}
}

$widget = new WidgetTopNavigationV2();
$data = $widget->getDataForAdmin();

$smarty->assign("data",$data);
$smarty->assign("main","topnavigation_widget_v2");
func_display("admin/home.tpl",$smarty);
?>
