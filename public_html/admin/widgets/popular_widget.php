<?php 
require "../auth.php";
require $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/class/widget/class.widget.mostpopular.php";

if ($mode == "save") 
{
	if(empty($link_name)) {
		$link_name = $link_name2;
		$link_url = $link_url2; 
	}
	
    foreach($link_name AS $key=>$value){
        $arr = $link_name[$key];
        $sql = "UPDATE mk_widget_most_popular SET link_name='" . $arr[link_name] . "' WHERE id=$key";
        db_query($sql);
    }
    
	foreach($link_url AS $key=>$value){
        $arr = $link_url[$key];
        $sql = "UPDATE mk_widget_most_popular SET link_url='" . $arr[link_url] . "' WHERE id=$key";
        db_query($sql);
    }
} elseif ( $mode == "add" ) {
	if(!empty($link_url) && !empty($link_name) ) {
		$sql = "INSERT INTO mk_widget_most_popular(link_name, link_url, is_permanent) VALUES('$link_name', '$link_url', $is_permanent)";
		db_query($sql);
	}
} elseif ( $mode == "delete" ) {
	if( !empty($update_link_id) ) {
		$sql = "DELETE FROM mk_widget_most_popular where id=$update_link_id ";
		db_query($sql);
	}
}


$widget = new WidgetMostPopular();
$data = $widget->getDataForAdmin();

$popularlinks_perm = $data['permanent'];
$popularlinks_nonperm = $data['dynamic'];

$smarty->assign("popularlinks_perm",$popularlinks_perm);
$smarty->assign("popularlinks_nonperm",$popularlinks_nonperm);
$smarty->assign("main","popular_widget");
func_display("admin/home.tpl",$smarty);
?>
