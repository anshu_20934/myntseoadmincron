<?php 
require_once( "../auth.php");
require_once($xcart_dir."/include/security.php");
require_once($xcart_dir."/include/func/func.db.php");
require_once($xcart_dir."/include/func/func.utilities.php");

$table = 'mk_widget_banners';
$SIZE_X = 550;
$SIZE_Y = 210;

$mode = mysql_real_escape_string($_POST["mode"]);
$url = mysql_real_escape_string($_POST["url"]);
$alt = mysql_real_escape_string($_POST["alt"]);
$anchor = mysql_real_escape_string($_POST["anchor"]);
$banner = mysql_real_escape_string($_POST["banner"]);

if ($mode == "save") 
{
    if(!empty($banner)){
    	foreach ( $banner as $key=>$banner_arr){
    		$banner_id = $key;
    		$url = $banner_arr["url"];
    		$alt = $banner_arr["alt"];
    		$anchor = $banner_arr["anchor"];
    		
    		$updateWidgetQuery = "UPDATE mk_widget_banners SET url='$url' , alt='$alt', anchor='$anchor' where id=$key";
    		db_query($updateWidgetQuery);
    	}
    }
} elseif ( $mode == "add" ) {
    $file_name = $HTTP_POST_FILES['image']['name'];
	$tmp_file = $HTTP_POST_FILES['image']['tmp_name'];
	
	$lastPositionQuery = "select max(display_order) as max from mk_widget_banners where banner_type='new'";
	$res = db_query($lastPositionQuery);
	if( mysql_num_rows($res) == 0 ){
		$lastPosition = 1;
	} else {
		$lastPositionRes = mysql_fetch_assoc($res);
		$lastPosition = $lastPositionRes["max"] + 1;
	}
	
    if ($tmp_file && !empty($url)) {
    	$insertSql = "INSERT INTO mk_widget_banners(url, anchor, alt, display_order,banner_type) values('$url', '$anchor', '$alt', $lastPosition,'new')";
    	$res = db_query(($insertSql));
    	$id = mysql_insert_id();
    }
    if ($id) {
        $file_location = 'images/banners/' . time() . '-' . $file_name;
        $im = new Imagick($tmp_file);
        $im->writeimage( $xcart_dir ."/". $file_location );
        if(moveToDefaultS3Bucket($file_location)){
            func_array2update($table, array('image' => "$cdn_base/$file_location"), "id='$id'");
        }else{
            func_array2update($table, array('image' => $file_location), "id='$id'");
        }
        
    }   
} elseif ( $mode == "delete" ) {
	if( !empty($banner_id) ) {
		$sql = "DELETE FROM mk_widget_banners where id=$banner_id ";
		db_query($sql);
	}
} elseif ( $mode == "move_up" ) {
	if(!empty($banner_id)) {
		$getPositionQuery  = "select display_order from mk_widget_banners where id=$banner_id";
		$result = func_query_first($getPositionQuery);
		if(!empty($result)) {
			$position = $result['display_order'];
			if($position>1) {
				$updateQuery = "update mk_widget_banners set display_order=display_order-1 where display_order=$position and id=$banner_id";
		        db_query($updateQuery);
		        $earlierposition = $position - 1;
				$updateQuery2 = "update mk_widget_banners set display_order=display_order+1 where display_order=$earlierposition and id!=$banner_id and banner_type='new'";
				db_query($updateQuery2);
			}
		}
	}
} elseif ( $mode == "move_down" ) {
	if(!empty($banner_id)) {
		$getLastPositionQuery  = "select max(display_order) as max from mk_widget_banners where banner_type='new'";
		$resultLast = func_query_first($getLastPositionQuery);
		$positionLast = $resultLast['max'];
		
		$getPositionQuery  = "select display_order from mk_widget_banners where id=$banner_id";
		$result = func_query_first($getPositionQuery);
		if(!empty($result)) {
			$position = $result['display_order'];
			if($position<$positionLast) {
				$updateQuery = "update mk_widget_banners set display_order=display_order+1 where display_order=$position and id=$banner_id";
		        db_query($updateQuery);
		        $earlierposition = $position + 1;
				$updateQuery2 = "update mk_widget_banners set display_order=display_order-1 where display_order=$earlierposition and id!=$banner_id and banner_type='new'";
				db_query($updateQuery2);
			}
		}
	}
}

$data = array();
$banners = func_select_query($table, array('banner_type' => 'new'), null, null, array('display_order' => 'asc'));

$smarty->assign("banners",$banners);
$smarty->assign("main","banners_widget_new");
func_display("admin/home.tpl",$smarty);
?>
