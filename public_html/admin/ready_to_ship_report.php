<?php
require ("./auth.php");
func_header_location("orders_ship_report.php");
require ($xcart_dir."/include/security.php");
require_once ($xcart_dir."/include/func/func.order.php");
require_once $xcart_dir."/include/class/class.xlscreator.php";
require_once($xcart_dir."/include/class/class.html2fpdf.php");
require_once($xcart_dir."/class.imageconverter.php");

function load_all_product_types() {
	global $sql_tbl,$sqllog;	
	$query = "select lower(replace(pt.name,' ','-')) as type_name,pt.id as type_id from mk_product_type pt ";
	$query.= "where pt.is_active = '1' and pt.type = 'P' order by pt.name";
	$product_types = func_query($query);
	$sqllog->debug("SQL Query : ".$query." "." @ line ".__LINE__." in file ".__FILE__);
	return $product_types; 
}


function func_get_courier_types() {
	global $sql_tbl;
	$sql = "select courier_service,code from mk_courier_service where enable_status='1'";
	$couriers=func_query($sql);
	return $couriers;
}


function func_get_source_types(){
	global $sql_tbl;
	$sql = "select type_id,type_name from mk_source_types";
	$source_types=func_query($sql);
	return $source_types;
}


/** Return an array with a html string for each page.
 */ 
function func_get_html_labels($rts_orders) {
	$strArr=array();
	$details_query = "select orderid,s_title,s_firstname,s_lastname,s_address,s_city,s_state,s_country,s_zipcode,mobile ";
	$details_query.= "from xcart_orders where orderid in (";
	$count=0;
	foreach($rts_orders as $order) {
		if($count!=0) {
			$details_query.= ",";
		}
		$count=$count+1;
		$details_query.= $order[orderid];
	}
	$details_query=$details_query.")";
	
	$orders_info=func_query($details_query);
	
	$count=0;
	$page_count=0;
	
	$pdfString = "<html>";
	$pdfString.= "<head>";
	$pdfString.= "<title>quote pdf</title>";
	$pdfString.= "<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />";
	$pdfString.= "</head>";
	$pdfString.= "<body>";
	$pdfString.= "<table width='100%' cellpadding='25' cellspacing='5' border='0' >";
	
	foreach($orders_info as $info) {
		if($count==12) {
			$count=0;
			$pdfString.= "</table>";
			$pdfString.= "</body>";
			$pdfString.= "</html>";
			$strArr[$page_count]=$pdfString;
			// echo $pdfString;exit;
			$page_count++;
			$pdfString="";
			$pdfString.= "<html>";
			$pdfString.= "<head>";
			$pdfString.= "<title>quote pdf</title>";
			$pdfString.= "<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />";
			$pdfString.= "</head>";
			$pdfString.= "<body>";
			$pdfString.= "<table width='100%' cellpadding='25' cellspacing='5' border='0' >";
		}
		
		if(!($count%2)) {
			$pdfString.= "<tr><BR><BR>";
		}
		
		$pdfString.= "<td>";
		//$pdfString.= "<table>";
		//$pdfString.= "<tr><td>".$count." ".$info[s_title]." ".$info[s_firstname]." ".$info[s_lastname]."</td></tr>";
		//$pdfString.= "<tr><td>".$info[s_address]."</td></tr>";
		//$pdfString.= "<tr><td>".$info[s_city]." ".$info[s_zipcode]." ".$info[s_state]." ".$info[s_country]."</td></tr>";
		//$pdfString.= "</table>";
		//$pdfString.="sadfasdfadsfasdf";
		$pdfString.= "<P><BR><BR>&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; ARAMEX (COD) <t> ".$info[orderid]."</P><BR><BR><P><BR> ";
		$pdfString.= "&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;".$info[s_title]." ".$info[s_firstname]." ".$info[s_lastname]."<BR>";
		$pdfString.= "&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;".$info[s_address]."<BR>";
		$pdfString.= "&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;".$info[s_city]." ".$info[s_zipcode]."<BR>&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;".$info[s_state]." ".$info[s_country]."<BR>"."                       "."<BR>";
		$pdfString.= "</P></td>";
		
		if($count%2) {
			$pdfString.= "</tr><tr><td>.</td></tr>";
		}

		$count++;
	}
	
	if(!($count%2)) {
		$pdfString.= "</tr>";
	}
	
	$pdfString.= "</table>";
	$pdfString.= "</body>";
	$pdfString.= "</html>";
	
	//echo $strArr[0];exit;
	
	$strArr[$page_count]=$pdfString;
	if($count==0 && $page_count==0) {
		return "";
	} else {
		return $strArr;
	}
}


/** Gets address and order info in the courier provider format.
 *  Additionally split the order into multiple rows for the courier provides if it needs to be packed in multiple boxes. 
 */

function get_xls_data_from_orders($format, $rts_orders, $paymentmethod) {
	$distinct_order_ids = array();
	foreach ($rts_orders as $order) {
		if(!in_array($order['orderid'], $distinct_order_ids)) {
			$distinct_order_ids[] = $order['orderid'];
		}
	}
	
	if(count($distinct_order_ids) > 0 ) {
		$template_query="select reports_template from mk_courier_service where code='$format'";
		$template=func_query_first_cell($template_query);
		
		if(!$template) {
			$template = 'orderid as "Order Number", CONCAT(s_firstname," ",s_lastname) as "Customer Name", CONCAT(s_address,",",s_country) as Address, s_zipcode as Pincode, s_city as City, mobile as Mobile, phone as Phone';
		}
		
		if($paymentmethod == 'cod') { 
			if($format != 'AR')
				$template .= ', cod as weight, (total) as amount';
			else
				$template .= ', cod as weight, (total) as cod_value';
		}
		
		$order_details_query .= "SELECT  $template from xcart_orders where orderid in (".implode(',', $distinct_order_ids).")";
		$order_details = func_query($order_details_query);
		
		return $order_details;
	}
	return false;
}

function func_get_courier_order_info($format,$rts_orders,$split_orders=true) {
	global $sql_tbl;
	if(!$format || !$rts_orders) {
		return "";
	}
	
	$template_query="select reports_template from mk_courier_service where courier_service='$format'";
	$template=func_query_first_cell($template_query);
	
	if($split_orders == false ) {
		// This is a COD Order, So we need weight and amount columns also in the result
		$template = $template.",`cod` as `weight`, (total) as amount ";
	}
	
	$details_query = "select ".$template." from xcart_orders where orderid in (";
	
	$count=0;
	foreach($rts_orders as $order) {
		if($count!=0) {
			$details_query=$details_query.",";
		}
		$count=$count+1;
		$details_query=$details_query.$order[orderid];
	}
	
	$details_query=$details_query.")";
    
	$order_details=func_query($details_query);

	$return_set=array();

	$count=0;
	if($split_orders == true) {
		foreach ($order_details as $rec) {
			$key="";
			$oid="";
			
			foreach($rec as $k=>$v) {
				$key=$k;
				$oid=$v;
				break;
			}
			
			$packinfo=func_get_packing_info($oid);

			// To divide the COD value by the total box count
			$split_cod_value=0;
			
			if(array_key_exists('cod_value',$rec)) {
				$total_box_count=0;
				
				// Get total count
				foreach($packinfo as $item) {
					$total_box_count+=$item[boxes];
				}
				
				if($total_box_count==0) {
					$total_box_count=1;
				}
				
				$split_cod_value=round($rec['cod_value']/$total_box_count,2);
			}
			
			$box_count=1;
			
			foreach($packinfo as $item) {
				
				for($i=1; $i<=$item[boxes];$i+=1) {
					
					$return_set[$count]=$rec;
					$return_set[$count][$key]=$return_set[$count][$key]."#".$box_count;
					if(0!=$split_cod_value) {
						$return_set[$count]['cod_value']=$split_cod_value;
					}
					$box_count++;
					$count++;
				}
			}
		}
	} else {
		// This modification is done by vaibhav to handle cod order ready to ship report creation.
		// In case of cod there will a single entry for each order
		foreach($order_details as $rec) {
			$key="";
			$oid="";
			
			foreach($rec as $k=>$v) {
				$key=$k;
				$oid=$v;
				break;
			}
			
			if(!empty($rec[$key])) {
				$return_set[$count]=$rec;
				//$return_set[$count][$key]=$return_set[$count][$key]."#1";
				$count++;
			}
		}
	}
	
	return $return_set;
}


function check_courier_incompatibility($format,$couriers) {
	if($couriers=="" || count($couriers)==0) {
		return "YOU GOT TO SELECT THE COURIERS TO FILTER BY - XLS NOT GENERATED";
	}
	
	if(count($couriers)!=1) {
		if($format=="Aramex") {
			return "POSSIBLY COMBINING ARAMEX SHIPMENTS WITH NON ARAMEX ONES!! - XLS NOT GENERATED";
		}
		
		foreach($couriers as $courier) {
			if($courier=="AR") {
				return "POSSIBLY COMBINING ARAMEX SHIPMENTS WITH NON ARAMEX ONES!! - XLS NOT GENERATED";
			}
		}
		
	} else {
		if(($format == "Aramex" && $couriers[0]!=="AR") || ($format!=="Aramex" && $couriers[0]=="AR")) {
			return "POSSIBLY COMBINING ARAMEX SHIPMENTS WITH NON ARAMEX ONES!! - XLS NOT GENERATED";
		}
	}
	
	return "";
}


if($HTTP_POST_VARS) {
	$query_string = http_build_query($HTTP_POST_VARS);
	
} else if($HTTP_GET_VARS) {
	$query_string.= "&".http_build_query($HTTP_GET_VARS);
}

$URL = "ready_to_ship_report.php?".$query_string;
$smarty->assign("navigation_script",$URL);

if($HTTP_POST_VARS['type']) {
	$type=$HTTP_POST_VARS['type'];
	
} else if($HTTP_GET_VARS['type']) {
	$type=$HTTP_GET_VARS['type'];
	
} else {
	$type="QADone";
}

$smarty->assign('type',"$type");

if($HTTP_POST_VARS['operations_location']) {
	$operations_location=$HTTP_POST_VARS['operations_location'];
} else if($HTTP_GET_VARS['operations_location']) {
	$operations_location=$HTTP_GET_VARS['operations_location'];
} else { 
	$operations_location="1";
}

if($HTTP_POST_VARS['source_type']) {
	$source_type=$HTTP_POST_VARS['source_type'];
	
} else if($HTTP_GET_VARS['source_type']) {
	$source_type=$HTTP_GET_VARS['source_type'];
	
} else {
	$source_type=5;
}


/*if($HTTP_POST_VARS['couriers']) {
	$couriers=$HTTP_POST_VARS['couriers'];
} else if($HTTP_GET_VARS['couriers']) {
	$couriers=$HTTP_GET_VARS['couriers'];
} else {
	$couriers[0]="BD";
	$couriers[1]="DD";
	$couriers[2]="HD";
}*/


if($HTTP_POST_VARS['format']) {
	$format=$HTTP_POST_VARS['format'];
	
} else if($HTTP_GET_VARS['format']) {
	$format=$HTTP_GET_VARS['format'];
	
} else {
	$format="BD";
}


/*if($HTTP_POST_VARS['product_type']) {
	$product_types=$HTTP_POST_VARS['product_type'];
} else {
	$product_types=$HTTP_GET_VARS['product_type'];
}*/
$country_location = 'ALL';
if($_POST['country_location']) {
	$country_location=$_POST['country_location'];
} else {
	$country_location=$_GET['country_location'];
}

/*if($_POST['country_location1']) {
	$country_location1=$_POST['country_location1'];
} else {
	$country_location1=$_GET['country_location1'];
}*/

$smarty->assign("country_location", $country_location);
//$smarty->assign("country_location1", $country_location1);

// Load operations locations
$sql = "SELECT * FROM $sql_tbl[operations_location] WHERE status=1";
$sql_result = func_query($sql);
foreach ($sql_result as $key=>$value) {
	$operations_locations[$value['id']] = $value['name'];
}

$smarty->assign('operations_locations',$operations_locations);

if($HTTP_POST_VARS['mode']) {
	$mode=$HTTP_POST_VARS['mode'];
} else if($HTTP_GET_VARS['mode']) {
	$mode=$HTTP_GET_VARS['mode'];
}

if(empty($mode)) {
	$mode ='compute';
}

if($HTTP_POST_VARS['payment_method']) {
	if($HTTP_POST_VARS['payment_method'] == "none") {
		$payment_method = "";
	} else {
		$payment_method = $HTTP_POST_VARS['payment_method'];
	}
	
	/*if($HTTP_POST_VARS['payment_method1'] == "none") {
		$payment_method1 = "";
	} else {
		$payment_method1 = $HTTP_POST_VARS['payment_method1'];
	}*/
}

$smarty->assign("payment_method", $payment_method);
//$smarty->assign("payment_method1", $payment_method1);

//echo "value for payment status is".$payment_method;
$error_message="";
$smarty->assign("error_message",$error_message);

/*if($mode=="getCSV") {
	$error_message=check_courier_incompatibility($format,$couriers);
	
	if(!empty($error_message)) {
		$smarty->assign("error_message",$error_message);
		$mode="compute";
	}
}*/

if($mode=="getCSV") {
	if($type == "QADone") {
		$rts_orders=func_get_ready_to_ship_orders($sort_by,$sort_order,$operations_location,$source_type,$couriers,false,$product_types, $payment_method,$country_location);
		$smarty->assign('title',"Ready To Ship Orders (QA Complete)");
		
	} else if($type == "OpsDone") {
		$rts_orders=func_get_OpsDone_orders($sort_by,$sort_order,$operations_location,$source_type,$payment_method, $country_location);
		$smarty->assign('title',"OpsDone Orders");
	}
	
	if($format == 'SP') {
		$rts_orders = func_filter_speedpost_serviceable_orders($rts_orders);
	} else {
		$rts_orders = func_filter_orders_by_serviceability($rts_orders, $format, $payment_method);
	}
	
	$file_name_suffix = date("dmy");
	$format_tight = str_replace (" ", "", $format);
	$filename="ready_to_ship_report_".$format_tight."_".$file_name_suffix;

	/*if($format=="OrderIds") {
		header('Content-Type: application/csv');
		header("Content-disposition: attachment; filename=$filename.csv");
		
		$string=$rts_orders[0][orderid];
		$count=0;
		
		$lastprinted = '';
		foreach($rts_orders AS $key=>$array) {
			if($array[orderid] == $lastprinted) {
				continue;
			}
			
			if($count) {
				$string.= ",";
				$string.= $array[orderid];
			} else { 
				$count++;
			}
			
			$lastprinted = $array[orderid];
		}
		
		echo "$string";
		
	} else if($format=="Reebok") {
		header('Content-Type: application/csv');
		header("Content-disposition: attachment; filename=$filename.csv");
		
		$string=$rts_orders[0][orderid].",,"."RB";
		$count=0;
		$lastprinted = '';
		foreach($rts_orders AS $key=>$array) {
			if($array[orderid] == $lastprinted) {
				continue;
			}
			
			if($count) {
				$string.= "\n";
				$string.= $array[orderid].",,"."RB";
			} else {
				$count++;
			}
			
			$lastprinted = $array[orderid];
		}
		echo "$string";
		
	} else {*/
		/*if($payment_method == "cod") {
			$order_details=func_get_courier_order_info($format,$rts_orders, false);
		} else {
			$order_details=func_get_courier_order_info($format,$rts_orders, false);
		}*/
		//$order_details=func_get_courier_order_info($format, $rts_orders, false);
		$order_details = get_xls_data_from_orders($format, $rts_orders, $payment_method);
		
		//$filename=func_query_first_cell("select code from mk_courier_service where courier_service='$format'").$file_name_suffix;
		$file_name=$format.$file_name_suffix;
		
		$xlswriter= new xlsCreator();
		$weblog->info("Orders to write - ". count($order_details));
		$str= $xlswriter->xlsConvertQueryResult($order_details, $filename);
		
		//header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$filename.xls");
		//header("Cache-Control: no-cache, must-revalidate");
		//header("Content-Type: application/force-download");
		ob_clean();
		flush();
		echo $str;
	//}
	
} /*else if($mode=="getLabels") {
	if($type == "QADone") {
		$rts_orders=func_get_ready_to_ship_orders($sort_by,$sort_order,$operations_location,$source_types,$couriers,false,$product_types, $payment_method1,$country_location1);
		$smarty->assign('title',"Ready To Ship Orders (QA Complete)");
		
	} else if($type == "OpsDone") {
		$rts_orders=func_get_OpsDone_orders($sort_by,$sort_order,$operations_location,$source_types,$couriers,$product_types);
		$smarty->assign('title',"OpsDone Orders");
	}
	
	$file_name_suffix = time();    
	$filename="Labels_"."_".$file_name_suffix.".pdf";
	
	$htmlStringArray=func_get_html_labels($rts_orders);
	
	$pdf=new HTML2FPDF();    
	foreach($htmlStringArray as $pageno=>$htmlStr) {    
		$pdf->AddPage();
		$pdf->WriteHTML($htmlStr);
	}
	
	$pdf->Output($filename,"D");    
	echo $str;
	
}*/ else if($mode=="compute") {
	$sort_by  = $HTTP_GET_VARS['sort_by'];
	$sort_order  = $HTTP_GET_VARS['sort_order'];
	
	if($type == "QADone") {
		$orders=func_get_ready_to_ship_orders($sort_by,$sort_order,$operations_location,$source_type,$couriers,false,"", $payment_method,$country_location);
		$smarty->assign('title',"Ready To Ship Orders (QA Complete)");
		
	} else if($type == "OpsDone") {
		$orders=func_get_OpsDone_orders($sort_by,$sort_order,$operations_location,$source_type, $payment_method,$country_location);
		$smarty->assign('title',"OpsDone Orders");
	}
	
	if($format == 'SP') {
		$orders = func_filter_speedpost_serviceable_orders($orders);
	} else {
		$orders = func_filter_orders_by_serviceability($orders, $format, $payment_method);
	}
	
	$smarty->assign("orders",$orders);
	
	$sources=func_get_source_types();
	$smarty->assign("sources",$sources);
	$smarty->assign("source_type",$source_type);
	
	$courier_types=func_get_courier_types();
	$smarty->assign("courier_types", $courier_types);
	$smarty->assign("courier", $format);
	
	//$product_types=load_all_product_types();
	//$smarty->assign("product_types",$product_types);
	$smarty->assign("payment_method", $payment_method);
	
	$mode = "display";
	$smarty->assign('mode',$mode);
	$smarty->assign("main","ready_to_ship_report");
	
	func_display("admin/home.tpl",$smarty);
}
?>