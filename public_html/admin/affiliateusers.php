<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: users.php,v 1.56.2.1 2006/06/01 06:30:58 max Exp $
#

require "./auth.php";
require $xcart_dir."/include/security.php";

x_load('export');

x_session_register("search_data");

$location[] = array(func_get_langvar_by_name("lbl_users_management"), "");

$data['_objects_per_page'] = $config["Appearance"]["users_per_page_admin"];
if(empty($_GET['page']))
   $data["page"] = 1;
else 
     $data["page"] = $_GET['page'];


if ($REQUEST_METHOD == "POST")
{
	     if($mode == "update")
	     {
	     	if (is_array($posted_data)) {
			    foreach ($posted_data as $affid=>$v) {
					 if (empty($v["delete"]))
						  continue;
					
					$query_data = array("active" =>  $v["status"]  
						                 )  ;
					func_array2update("mk_affiliates", $query_data, "affiliate_id='$affid'");
						
			   }
			   $top_message = array(
				"content" => "Affiliate successfully updated",
				"type" => ""
				);
			   
	     	 
	         }
	     }
	     
	     if($mode == "delete")
	     {
	     	if (is_array($posted_data)) {
			    foreach ($posted_data as $affid=>$v) {
					 if (empty($v["delete"]))
						  continue;
					 db_query("DELETE FROM $sql_tbl[mk_affiliates] WHERE affiliate_id ='$affid' ");
										
			   }
			   $top_message = array(
				"content" => "Affiliate successfully deleted",
				"type" => ""
				);
			   
	     	 
	         }
	     }
}




       
   if(!empty($_GET['affId']))
   {     
   		$search_condition  = " WHERE affiliate_id = '".$_GET['affId']."' ";        		
   }
   $sort_string = " ORDER BY company_name" ;
   #
   # Calculate the number of rows in the search results
   # 
   $total_items = func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[mk_affiliates] ".$search_condition);

   if ($total_items == 0)
		return false;


   if (!empty($data['_objects_per_page'])) {
		#
		# Prepare the page navigation
		#
		$page = $data["page"];
		$objects_per_page = $data['_objects_per_page'];
		$total_nav_pages = ceil($total_items/$objects_per_page)+1;
	
		include $xcart_dir."/include/navigation.php";
	
		$sort_string .= " LIMIT $first_page, $objects_per_page";
	}

	#
	# Perform the SQL query and getting the search results
	#
	$users = func_query("SELECT  * FROM  $sql_tbl[mk_affiliates] ".$search_condition.$sort_string);
	
	if (!empty($users)) {
		# Assign the Smarty variables
		$smarty->assign("navigation_script", "affiliateusers.php?");
		$smarty->assign("users", $users);
		$smarty->assign("first_item", $first_page+1);
		$smarty->assign("last_item", min($first_page+$objects_per_page, $total_items));
	}

	$smarty->assign("total_items", $total_items);


#
# Assign Smarty variables and show template
#
$smarty->assign("main","affiliateusers");

# Assign the current location line
$smarty->assign("location", $location);

# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
