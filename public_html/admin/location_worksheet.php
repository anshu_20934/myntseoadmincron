<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
include_once($xcart_dir."/include/func/func.itemsearch.php");

if($HTTP_POST_VARS){
	$query_string = http_build_query($HTTP_POST_VARS);
	$locationid = $HTTP_POST_VARS['item_location'];
}
if($HTTP_GET_VARS){
	$query_string = http_build_query($HTTP_GET_VARS);
	$locationid = $HTTP_GET_VARS['item_location'];
}

if($locationid)
{

	#
	#Calling function to create search query for number of records
	#
	$sql = func_load_location_task_list($locationid);
	$sqllog->debug("File name##assignee_worksheet.php##SQL Info::>$sql");
	$_res = db_query($sql);
	$total_items = db_num_rows($_res);
	$smarty->assign("total_items", $total_items);
	if ($total_items > 0)
	{
		$page = $_GET["page"];

		#
		# Prepare the page navigation
		#
		if (!isset($objects_per_page)) {
			if ($current_area == "C")
			$objects_per_page = $config["Appearance"]["products_per_page"];
			else
			$objects_per_page = $config["Appearance"]["products_per_page_admin"];
		}
		$total_nav_pages = ceil($total_items/$objects_per_page)+1;

		require $xcart_dir."/include/navigation.php";

		$search_sql = func_load_location_task_list($locationid);;

		$search_sql .=" ORDER BY a.orderid ASC LIMIT $first_page, $objects_per_page";

		$sqllog->debug("File name##assignee_worksheet.php##SQL Info::>$search_sql");
		$itemResult = func_query($search_sql);

		#
		#navigation url
		#
		if($mode == "search")
		$URL = "location_worksheet.php?".$query_string;
		else
		$URL = "location_worksheet.php";

		$smarty->assign("navigation_script",$URL);
		$smarty->assign("first_item", $first_page+1);
		$smarty->assign("last_item", min($first_page+$objects_per_page, $total_items));
		$smarty->assign("total_items",$total_items);
		$smarty->assign ("itemResult", $itemResult);
		$smarty->assign ("sizeofitemresult", $objects_per_page);

	}
}

#
#Load item locations
#
$sql = "SELECT * FROM $sql_tbl[operation_location] WHERE status=1";
$locations = func_query($sql);

#
#Status array
#
$status = array("N"=>array("status"=>"New", "color"=>"#EE0F14"),
                "A"=>array("status"=>"Assigne", "color"=>"#77AB3D"),
                "OD"=>array("status"=>"Ops Done", 		"color"=>"#99FF22"),
                "QD"=>array("status"=>"QA Done", 		"color"=>"#EF940A"),
                "D"=>array("status"=>"Done", 		"color"=>"#80FFFF"),
                "F"=>array("status"=>"Failed", 		"color"=>"#333FFF"),
                "H"=>array("status"=>"On Hold", "color"=>"#F5FF42"), 
                "S"=>array("status"=>"Sent Outside", "color"=>"#04FF82"));

$smarty->assign ("status", $status);
$smarty->assign ("query_string", $query_string);
$smarty->assign ("mode", $mode);
$smarty->assign("locations",$locations);
$smarty->assign("locationid",$locationid);

$smarty->assign("main","location_worksheet");
func_display("admin/home.tpl",$smarty);
?>
