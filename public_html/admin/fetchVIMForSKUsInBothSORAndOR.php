<?php
include_once("auth.php");
include_once("$xcart_dir/modules/amqpclients/producer/AMQPMessageProducer.php");

$styleChunkSize = 1000;
$conn = mysql_connect('cssdb2.myntra.com:3307','myntraAppDBUser','9eguCrustuBR#',TRUE);

//Check if connection exists
if (!$conn) {
    die('Could not connect: ' . mysql_error() . '\n');
	exit;
}

//Check if DB is accessible
if (mysql_select_db('myntra_vms', $conn) === false) {
    echo "VIM db unaccessible \n";


    exit;
}

$query = "select vendor_id vendorId, style_id styleId, group_concat(sku_id) skuIds from vendor_item_master where style_id in (select style_id from vendor_item_master where commercial_type in ('SOR', 'OUTRIGHT') and style_id is not null and style_id > 0 group by style_id having count(distinct commercial_type)>=2) and commercial_type='SOR' group by vendor_id, style_id";

$db_result = mysql_query($query, $conn);

$dataArray = convertIntoArray($db_result);


while(list($vendorId,$styleArray) = each($dataArray)){

	$styleArrayChunks = array_chunk($styleArray,$styleChunkSize,true);
	while(list(,$styleArrayChunk) = each($styleArrayChunks)){
	//	print_r($styleArrayChunk);
		$vimResponse = createVIMResponse($styleArrayChunk,$vendorId);
		pushMessage($vimResponse);
	}
}
//mysql_close($conn);
exit;


function convertIntoArray($db_result){
	$data = array();
	$styleIndex = 0;
	while($row = mysql_fetch_array($db_result, MYSQL_ASSOC)){
		$vendorId = $row["vendorId"];
		$styleId = $row["styleId"];
		$data[$vendorId][$styleId] = $row["skuIds"];
		$styleIndex++;
	}
	return $data;
}

function createVIMResponse($styleArray,$vendorId){
	$vimResponse = array();
	$vimResponse["data"] = array();
	$entryIndex = 0;
	while(list($styleId,$skuIds) = each($styleArray)){
		$skuIdArray = explode(",",$skuIds);
		while(list(,$skuId) = each($skuIdArray)){
			$vimResponse["data"][$entryIndex]=array();
			$vimResponse["data"][$entryIndex]["vendorId"] = $vendorId;
			$vimResponse["data"][$entryIndex]["styleId"] = $styleId;
			$vimResponse["data"][$entryIndex]["skuId"] = $skuId;
			$entryIndex++;
		}
	}
	return $vimResponse;
}

function pushMessage($vimResponse){
	echo "sending message : " . json_encode($vimResponse);
	echo "\n\n";
//	$client = AMQPMessageProducer::getInstance();
//	$client->sendMessageAsJson("orSorStyleSkus",$vimResponse);
	$client = AMQPMessageProducer::getInstance();
	$client->sendMessage("orSorStyleSkus",$vimResponse, 'VendorItemMasterResponse','json',true, 'wms');
}
?>
