<?php
include "./auth.php";
//include_once("$xcart_dir/include/security.php");
include_once("$xcart_dir/include/func/func.order.php");

ini_set("display_errors",1);
$itemid = $HTTP_GET_VARS['itemid'];
$type = $HTTP_GET_VARS['update'];
$item_check = $HTTP_POST_VARS['check'];
$query_string = $HTTP_POST_VARS['query_string'];

#
#Fetch order id against itemid
#

if($type == 'assign'){
    $itemto = $HTTP_GET_VARS['ia'];
    $loc = $HTTP_GET_VARS['il'];
    $assignmenttime = time();    

        $failure=0;
        foreach($item_check AS $itemid=>$value){
             $orderid = func_query_first_cell("SELECT orderid FROM $sql_tbl[order_details] WHERE itemid='$itemid'");
             $order_status = func_query_first_cell("SELECT status FROM $sql_tbl[orders] WHERE orderid='$orderid'");

             if($order_status == 'Q' || $order_status == 'WP' ){

             if($itemto != ""){
             $sql = "UPDATE $sql_tbl[order_details] SET assignee='$itemto', item_status='A', assignment_time='$assignmenttime' WHERE itemid='$itemid'";      
             $flag = db_query($sql);
             $weblog->info("item status change : $itemid status changed to A : assignee $itemto");
             }
             // Asif - Location should not be changed .. 
             /*if($loc != ""){
             $sql = "UPDATE $sql_tbl[order_details] SET location='$loc' WHERE itemid='$itemid'";
             $flag = db_query($sql);
             $weblog->info("item location change : $sql");
             }*/
                 #
                 #Update order status to WIP
                 #
                 if($flag){
                    $item_orderid = func_query_first_cell("SELECT orderid FROM $sql_tbl[order_details] WHERE itemid='$itemid'");
                    $sql = "UPDATE $sql_tbl[orders] SET status='WP' WHERE orderid='$item_orderid'";
                    db_query($sql);
                    $weblog->info("order status change : $item_orderid status change to WP");
                    
                    
                 }
             }
             else{
                 $failure=1;
             }
            
       }

       $host=$_SERVER['HTTP_HOST'];
       $path='admin/operations/item_assignment.php?';
       header("HTTP/1.1 307 Moved temporarily");
       if($failure==1){
         $path=$path.$query_string."&action=a_failed";
       }
       else{
         $path=$path.$query_string."&action=a_updated";
       }
       header("Location: http://$host/$path");
}elseif($type == 'status'){
    $status = $HTTP_GET_VARS['status'];
    $done_item = $HTTP_GET_VARS['qunatity'];
    $time = time();
    $sql = "UPDATE $sql_tbl[order_details] SET item_status='$status' ";
    
    if($status == 'D'){
        $sql .= ", completion_time='$time' ";
    }elseif($status == 'S'){
        $sql .= ", sent_date='$time' ";
    }elseif($status == 'F' || $status == 'H'){
		$sql .= ", assignee='' ";
    }elseif($status == 'I'){
		$sql_orders = "UPDATE {$sql_tbl[orders]} SET status = 'I' where orderid in (select orderid from {$sql_tbl[order_details]} where itemid='{$itemid}')";
		db_query($sql_orders);
        $weblog->info("order status change : Order corresponding to item $itemid changed to status I ");
	}
        
    $sql .= " WHERE itemid='$itemid'";
    $flag = db_query($sql);
    $weblog->info("item status change : $itemid changed to $status ");

    #
    #Update order status
    #
    if($status == 'H'){
        $desc = $HTTP_GET_VARS['onholdreason'];
        $orderid = func_query_first_cell("SELECT orderid FROM $sql_tbl[order_details] WHERE itemid='$itemid'");
      
        db_query("UPDATE $sql_tbl[orders] SET status='OH' WHERE orderid='" . $orderid . "'");
        
		func_addComment_order($orderid, 'Admin', 'On Hold', "Item $itemid On Hold", $desc);
		
        //Send an e-mail to customer care so that they can get more information about the item.
        $from ="MyntraAdmin";
        $loginEmail="alrt_orderonhold@myntra.com";
        $bcc="";        
        $template = "orderOnHold";
        $args = array("ORDER_ID"	=>$orderid	);        
        sendMessage($template, $args, $loginEmail,$from,$bcc);

        
        $weblog->info("order status change : $orderid changed to OH");
    }
}

echo $flag;
?>
