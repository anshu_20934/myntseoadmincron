<?php
//ini_set('memory_limit','2000M');
require_once "WMSDBConnUtil.php";

function getOms2AtpBoc(&$allSkusSellers,&$oosSkuQueries,&$undersellSkuQueries,&$unAvlblUnRlsdSkuQueries,&$avlblUnrlsdSkuQueries,$myntraSellerIds){


$omsConn = WMSDBConnUtil::getConnection("omsdb2.myntra.com:3306", TRUE, WMSDBConnUtil::IMS_READ);
$atpConn = WMSDBConnUtil::getConnection('atpdbmaster.myntra.com:3306', TRUE, WMSDBConnUtil::IMS_WRITE);
$wmsConn = WMSDBConnUtil::getConnection('wmsdb2.myntra.com:3306', TRUE, WMSDBConnUtil::IMS_READ);
$bountyConn = WMSDBConnUtil::getConnection("order-db1.myntra.com:3306", TRUE, WMSDBConnUtil::IMS_READ);

    if (!$omsConn || !$atpConn || !$wmsConn || !$bountyConn) {
        exit;
    }

    if(mysql_select_db('myntra_oms', $omsConn ) === false) {
        exit;
    }
    if(mysql_select_db('myntra_atp', $atpConn ) === false) {
        exit;
    }
    if(mysql_select_db('myntra_wms', $wmsConn ) === false) {
        exit;
    }

    if(mysql_select_db('myntra_order', $bountyConn ) === false) {
        exit;
    }

    $limit = 30000;
    $hasMore= true;
    $omsMinId = 1;


    $omsMinIdQuery = "select min(id) as minId, max(id) as maxId from order_release where status_code in ('Q','RFR','WP')";

    $omsMinIdResults = mysql_query($omsMinIdQuery, $omsConn);
    if ($omsMinIdResults){
        while ($row = mysql_fetch_array($omsMinIdResults, MYSQL_ASSOC)){
            $omsMinId = $row['minId'];
            $omsMaxId = $row['maxId'];
        }
    }

    @mysql_free_result($omsMinIdResults);

    $OmsSkuId2BocMap = array();

while($hasMore){
    $nextMinId = $omsMinId + $limit;
    $itemQuery = "select ol.sku_id as sku_id, ol.seller_id as seller_id, sum(ol.quantity) as blocked_order_quantity from order_line ol join order_release orl on (orl.id=ol.order_release_id_fk) where orl.id >= ".$omsMinId." and orl.id < ".$nextMinId." and orl.status_code in ('Q','RFR','WP') and ol.seller_id in (".$myntraSellerIds.") and ol.supply_type = 'ON_HAND' and ol.store_line_id is null and ol.status_code != 'IC' group by ol.sku_id, ol.seller_id";

    $itemResults = mysql_query($itemQuery, $omsConn);
    if($itemResults) {
        while ($row = mysql_fetch_array($itemResults, MYSQL_ASSOC)) {
            $skuSeller=$row['sku_id']."_".$row['seller_id'];
            if($OmsSkuId2BocMap[$skuSeller]) {
                $OmsSkuId2BocMap[$skuSeller] = $OmsSkuId2BocMap[$skuSeller] + $row['blocked_order_quantity'];
            }else{
                $OmsSkuId2BocMap[$skuSeller] =$row['blocked_order_quantity'];
            }

        }
    }

    if($nextMinId >= $omsMaxId)
        $hasMore = false;

    $omsMinId = $nextMinId;
    @mysql_free_result($itemResults);
}

    /*  $bountyQuery = "SELECT xod.sku_id as sku_id, sum(xod.amount) as qty FROM xcart_orders xo INNER JOIN xcart_order_details xod ON (xod.orderid = xo.orderid) WHERE status='OH' and supply_type='ON_HAND' GROUP BY xod.sku_id";

      $bountyResults = mysql_query($bountyQuery, $bountyConn);
      if($bountyResults) {
        while ($row = mysql_fetch_array($bountyResults, MYSQL_ASSOC)) {
            if($OmsSkuId2BocMap[$row['sku_id']]) {
            $OmsSkuId2BocMap[$row['sku_id']] = $OmsSkuId2BocMap[$row['sku_id']] + $row['qty'];
          }else{
             $OmsSkuId2BocMap[$row['sku_id']] = $row['qty'];
          }

         }
      }

      @mysql_free_result($bountyResults);
    */
    $issuedItemsWmsQuery="select itm.order_id order_id, itm.sku_id sku_id, count(*) issuedCount from item itm where item_status='ISSUED' group by itm.sku_id, itm.order_id";
    $issuedItemsWmsResults= mysql_query($issuedItemsWmsQuery, $wmsConn);
    while ($itemrow = mysql_fetch_array($issuedItemsWmsResults, MYSQL_ASSOC)){
        $issuedItemsOmsResults = mysql_query("select orl.id as id, ol.seller_id as seller_id, ol.sku_id as sku_id from order_line ol join order_release orl on (orl.id=ol.order_release_id_fk) where ol.order_release_id_fk = ".$itemrow['order_id']." and ol.sku_id =".$itemrow['sku_id']." and ol.status_code != 'IC' and orl.status_code in ('PK', 'SH', 'DL', 'C','L','RTO')", $omsConn);
        $releaseId = false;
        while ($release = mysql_fetch_array($issuedItemsOmsResults, MYSQL_ASSOC)){
            $releaseId = $release['id'];
            $skuSeller= $itemrow['sku_id']."_".$release['seller_id'];
            $missedCount= $itemrow['issuedCount'];
            break;
        }
        if($releaseId) {
            if($OmsSkuId2BocMap[$skuSeller]) {
                $OmsSkuId2BocMap[$skuSeller] = $OmsSkuId2BocMap[$skuSeller] + $missedCount;
            }else{
                $OmsSkuId2BocMap[$skuSeller] = $missedCount;
            }
        }
    }
    @mysql_free_result($issuedItemsWmsResults);
    @mysql_free_result($issuedItemsOmsResults);

    $offset = 0;
    $limit = 200000;
    $hasMore= true;

    while($hasMore) {

        $atpSkuSellerIds= array();
        $atpSkuId2BocMap = array();
        $atpSkuId2InvMap = array();
        $atpInvCountQuery = "select id, sku_id, seller_id, inventory_count, blocked_order_count as blocked_order_quantity from inventory where store_id = 1 and seller_id in (".$myntraSellerIds.") and supply_type = 'ON_HAND' LIMIT $offset, $limit";

        $atpInvResults = mysql_query($atpInvCountQuery, $atpConn);

        if($atpInvResults) {
            while ($row = mysql_fetch_array($atpInvResults, MYSQL_ASSOC)) {
                $sellerId = $row['seller_id'];
                $skuId = $row['sku_id'];
                $skuSeller = $skuId."_".$sellerId;

                $atpSkuSellerIds[] = $skuSeller;
                $atpSkuId2BocMap[$skuSeller] = $row['blocked_order_quantity'];
                $atpSkuId2InvMap[$skuSeller] = $row['inventory_count'];
            }
        }

        @mysql_free_result($atpInvResults);
        if(empty($atpSkuSellerIds)) {
            $hasMore = false;
            continue;
        }

        foreach($atpSkuSellerIds as $skuSeller) {
            $startIndx = strpos($skuSeller,"_");
            $sellerId=substr($skuSeller, $startIndx+1);
            $skuId=substr($skuSeller, 0, $startIndx);

            if(isset($OmsSkuId2BocMap[$skuSeller])) {
                if($atpSkuId2BocMap[$skuSeller] != $OmsSkuId2BocMap[$skuSeller]){
                    $allSkusSellers[] = $skuSeller;
                    if($atpSkuId2BocMap[$skuSeller] < $OmsSkuId2BocMap[$skuSeller]){
                        $query = "update inventory set blocked_order_count = blocked_order_count + ".($OmsSkuId2BocMap[$skuSeller]-$atpSkuId2BocMap[$skuSeller])." where store_id = 1 and sku_id = $skuId and seller_id = $sellerId and supply_type = 'ON_HAND'";
                        $oosSkuQueries[] = $query;
                    }
                    if($atpSkuId2BocMap[$skuSeller] > $OmsSkuId2BocMap[$skuSeller]){
                        $query = "update inventory set blocked_order_count = blocked_order_count - ".($atpSkuId2BocMap[$skuSeller]-$OmsSkuId2BocMap[$skuSeller]).", available_in_warehouses = if(available_in_warehouses='','28,36,89,93,81,118,213',available_in_warehouses) where store_id = 1 and sku_id = $skuId and seller_id = $sellerId and supply_type = 'ON_HAND'";
                        $undersellSkuQueries[] = $query;
                    }
                }
            }
            else if ($atpSkuId2BocMap[$skuSeller] != 0) {
                $allSkusSellers[] = $skuSeller;
                if ($atpSkuId2InvMap[$skuSeller] > 0){
                    $query = "update inventory set blocked_order_count = 0, available_in_warehouses = if(available_in_warehouses='','28,36,89,93,81,118',available_in_warehouses) where store_id = 1 and sku_id = $skuId and seller_id = $sellerId and supply_type = 'ON_HAND'";
                    $avlblUnrlsdSkuQueries[] = $query;
                }
                else{
                    $query = "update inventory set blocked_order_count = 0, available_in_warehouses = if(available_in_warehouses='','28,36,89,93,81,118',available_in_warehouses) where store_id = 1 and sku_id = $skuId and seller_id = $sellerId and supply_type = 'ON_HAND'";
                    $unAvlblUnRlsdSkuQueries[] = $query;
                }
            }
        }

        $offset += $limit;
    }

}

?>

