<?php 

require "./auth.php";
require $xcart_dir."/include/security.php";

include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalClient.php";



function get_all_transactions($request) {
	if(empty($request['userlogin'])){
		return false;
	}
	$start = $request['start'] == null ? 0 : $request['start'];
	$limit = $request['limit'] == null ? 30 : $request['limit'];

	$couponAdapter = CouponAdapter::getInstance();
	$results = LoyaltyPointsPortalClient::getTierLogs($request['userlogin']);
	foreach($results as $id=>$row){
		$results[$id]["id"] = $id;
		$results[$id]["login"] = $request['userlogin'];
		$results[$id]["modifiedOn"] = date("d-m-Y H:i:s",(int) ($row['modifiedOn']/1000));
		$results[$id]["validUpto"] = date("d-m-Y H:i:s",(int) ($row['validUpto']/1000));
		$results[$id]["tNcAcceptedOn"] = date("d-m-Y H:i:s",(int) ($row['tNcAcceptedOn']/1000));
		switch($results[$id]["tier"]){
			case 1:
				$results[$id]["tier"] = "Silver";
			break;
			case 2:
				$results[$id]["tier"] = "Gold";
			break;
			case 3:
				$results[$id]["tier"] = "Platinum";
			break;
		}

	}
	$count = sizeOf($results);
	$count = ($count == 30) ? $count+$start+5 : $start+$count; // if we recieve 30 , notify extjs to enable pagination by specifying more count
	
	return array("results" => $results, "count" =>$count );


}

function compute_tier($request) {
	if(empty($request['userlogin'])){
		return false;
	}

	$couponAdapter = CouponAdapter::getInstance();
	$results = LoyaltyPointsPortalClient::computeTierInfo($request['userlogin']);
	$results = LoyaltyPointsPortalClient::getTierLogs($request['userlogin']);
	foreach($results as $id=>$row){
		$results[$id]["id"] = $id;
		$results[$id]["login"] = $request['userlogin'];
		$results[$id]["modifiedOn"] = date("d-m-Y H:i:s",(int) ($row['modifiedOn']/1000));
		$results[$id]["validUpto"] = date("d-m-Y H:i:s",(int) ($row['validUpto']/1000));
		$results[$id]["tNcAcceptedOn"] = date("d-m-Y H:i:s",(int) ($row['tNcAcceptedOn']/1000));
		switch($results[$id]["tier"]){
			case 1:
				$results[$id]["tier"] = "Silver";
			break;
			case 2:
				$results[$id]["tier"] = "Gold";
			break;
			case 3:
				$results[$id]["tier"] = "Platinum";
			break;
		}

	}
	$count = sizeOf($results);
	return array("results" => $results, "count" =>$count );


}


if ($REQUEST_METHOD == "POST") {
	if ($_POST['action'] == 'getTierLogs') {

		$retarr = get_all_transactions($_POST);

		header('Content-type: text/x-json');
		print json_encode($retarr);
	}
	else if ($_POST['action'] == 'computeTierInfo') {

		$retarr = compute_tier($_POST);

		header('Content-type: text/x-json');
		print json_encode($retarr);
	}


} else {
	
	$smarty->assign("main","loyaltyPointsTier");
	func_display("admin/home.tpl", $smarty);
}
?>
