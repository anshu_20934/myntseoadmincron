<?php
/**
 * Created by Arun Kumar K.
 * Date: 13 May, 2011
 * Time: 12:47:23 PM
 * used to manage crosslinking keywords
 */
require "../auth.php";
require $xcart_dir."/include/security.php";
include_once($xcart_dir."/modules/seo/CrossLinkKeyword.php");

isset($_GET['page']) ? $page = (int)$_GET['page'] : $page = 1;
if($page <= 0) $page = 1;
$start = 0 ;
$limit = 50;

if($page) {
	$start = ($page -1) * $limit;
}

$mode = isset($_POST['mode']) ? $_POST['mode'] : $_GET['mode'];
$search_keyword = isset($_POST['search_keyword']) ? $_POST['search_keyword'] : $_GET['search_keyword'];

$cKeyword = new CrossLinkKeyword();
$totalKeywordsCount = ($mode == "search") ? $cKeyword->getKeywordCount($search_keyword) :  $cKeyword->getKeywordCount();

$totalKeywordsCount > ($page )*$limit ? $pageEnd = ($page )*$limit : $pageEnd = $totalKeywordsCount;   

switch($_POST['mode']){
    case 'add':             $keyword = preg_replace('/\s+/',' ',trim($_POST['crosslink_keyword']));//replce multiple space with single space
    						$keywordUrl = trim($_POST["crosslink_keyword_url"]);
                            $query_data = array("crosslink_keyword"=>$keyword,"crosslink_keyword_url" => $keywordUrl);
                            $keywordInfo = $cKeyword->getKeyword(array("crosslink_keyword"=>$keyword));

                            //keyword can not be a duplicate hence need to check for existence
                            if(empty($keywordInfo)){
                                $flag=$cKeyword->addKeyword($query_data);
                                if($flag){$smarty->assign("message","<strong style='color:green'>Data is added successfully.</strong>");}
                            } else {
                                $smarty->assign("message","<strong style='color:red'>The keyword trying to add is already exists.</strong>");
                            }
                            break;

    case 'update':          $Id = $_POST['selecteditem'];
                            $keyword = preg_replace('/\s+/',' ',trim($_POST["keyword"]["crosslink_keyword"][$Id]));
                            $keywordUrl = trim($_POST["keyword"]["crosslink_keyword_url"][$Id]);
                            $query_data = array("crosslink_keyword"=>$keyword, "is_active"=>$_POST["keyword"]["status"][$Id],
                            					"crosslink_keyword_url" => $keywordUrl);
                            $condition = "id = '{$Id}'";
                            $cKeyword->updateKeyword($query_data,$condition);
                            $smarty->assign("message","<strong style='color:green'>Data is updated successfully.</strong>");
                            break;

    case 'delete':          $Id = $_POST['selecteditem'];
                            $cKeyword->deleteKeyword($Id);
                            break;

    case 'updateall':       if(is_array($_POST['keyword']['selector'])){
                                foreach($_POST['keyword']['selector'] as $Id=>$val){
                                    $keyword = preg_replace('/\s+/',' ',trim($_POST["keyword"]["crosslink_keyword"][$Id]));
                                    $keywordUrl = trim($_POST["keyword"]["crosslink_keyword_url"][$Id]);
                                    $query_data = array("crosslink_keyword"=>$keyword, "is_active"=>$_POST["keyword"]["status"][$Id],
                            					"crosslink_keyword_url" => $keywordUrl);
                                    $condition = "id = '{$Id}'";
                                    $cKeyword->updateKeyword($query_data,$condition);
                                }
                                $smarty->assign("message","<strong style='color:green'>All selected data is updated successfully.</strong>");
                            } else {
                                $smarty->assign("message","<strong style='color:red'>No data is selected to update.</strong>");
                            }
                            break;
}

$orderby = array('crosslink_keyword'=>'asc');
if($mode == "search" && !empty($search_keyword)) { 
	$keywordInfo = $cKeyword->searchKeyword($search_keyword,$filters=null,$orderby,$start,$limit);
	$smarty->assign("mode", "search");
	$smarty->assign("search_keyword", $search_keyword);
} else { 
	$keywordInfo = $cKeyword->getKeyword($filters=null,$orderby,$start,$limit);
}

$smarty->assign("main","crosslink_keyword_manager");
$smarty->assign("totalKeywordsCount",$totalKeywordsCount);
$smarty->assign("page",$page);
$smarty->assign("limit",$limit);
$smarty->assign("start",$start);
$smarty->assign("pageEnd",$pageEnd);

$smarty->assign("data",$keywordInfo);
func_display("admin/home.tpl",$smarty);
