<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
require_once($xcart_dir."/include/func/func.order.php");
require_once($xcart_dir."/include/func/func.itemsearch.php");
require_once($xcart_dir."/modules/discount/DiscountEngine.php");
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");
include_once("$xcart_dir/include/class/class.feature_gate.keyvaluepair.php");
include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");
include_once("$xcart_dir/modules/apiclient/WarehouseApiClient.php");

$quality_check_codes = ItemApiClient::getItemQualityCheckCodes("RETURN_FROM_OPS");
 
/*
array(
	1=>array( 'id'=>1, 'quality' => 'Q1', 'reason_code' => 'QP', 'reason_code_display' => 'Correct Item', 'reason_description' => '', 'user_display' => 'QA Pass'),
	2=>array( 'id'=>2, 'quality' => 'Q2', 'reason_code' => 'MD', 'reason_code_display' => 'Manufacturing Defect', 'reason_description' => 'Damaged', 'user_display' => 'MD - Damaged'),
	3=>array( 'id'=>3, 'quality' => 'Q2', 'reason_code' => 'MD', 'reason_code_display' => 'Manufacturing Defect', 'reason_description' => 'Stained' , 'user_display' => 'MD - Stained'),
	4=>array( 'id'=>4, 'quality' => 'Q2', 'reason_code' => 'MD', 'reason_code_display' => 'Manufacturing Defect', 'reason_description' => 'Broken Stitch', 'user_display' => 'MD - Broken Stitch'),
	5=>array( 'id'=>5, 'quality' => 'Q2', 'reason_code' => 'MD', 'reason_code_display' => 'Manufacturing Defect', 'reason_description' => 'Fabric/Material Defect', 'user_display' => 'MD - Fabric/Material Defect'),
	6=>array( 'id'=>6, 'quality' => 'Q2', 'reason_code' => 'MD', 'reason_code_display' => 'Manufacturing Defect', 'reason_description' => 'Shade Variation', 'user_display' => 'MD - Shade Variation'),
	7=>array( 'id'=>7, 'quality' => 'Q2', 'reason_code' => 'MD', 'reason_code_display' => 'Manufacturing Defect', 'reason_description' => 'Logo Missing', 'user_display' => 'MD - Logo Missing'),
	8=>array( 'id'=>8, 'quality' => 'Q2', 'reason_code' => 'MD', 'reason_code_display' => 'Manufacturing Defect', 'reason_description' => 'Uneven Length ', 'user_display' => 'MD - Uneven Length'),
	9=>array( 'id'=>9, 'quality' => 'Q2', 'reason_code' => 'ACCESSORY_MISSING', 'reason_code_display' => 'ACCESSORY MISSING', 'reason_description' => 'Tag', 'user_display' => 'Accessory Missing - Tag' ),
	10=>array( 'id'=>10, 'quality' => 'Q2', 'reason_code' => 'ACCESSORY_MISSING', 'reason_code_display' => 'ACCESSORY MISSING', 'reason_description' => 'Warranty', 'user_display' => 'Accessory Missing - Warranty' ),
	11=>array( 'id'=>11, 'quality' => 'Q2', 'reason_code' => 'ACCESSORY_MISSING', 'reason_code_display' => 'ACCESSORY MISSING', 'reason_description' => 'Part' , 'user_display' => 'Accessory Missing - Part'),
	12=>array( 'id'=>12, 'quality' => 'Q2', 'reason_code' => 'EXPIRED_PRODUCT', 'reason_code_display' => 'EXPIRED PRODUCT', 'reason_description' => 'Expired Product', 'user_display' => 'Expired Product'),
	13=>array( 'id'=>13, 'quality' => 'Q2', 'reason_code' => 'SIZE_MISMATCH', 'reason_code_display' => 'SIZE_MISMATCH', 'reason_description' => 'Wrong Pair', 'user_display' => 'Size Mismatch - Wrong Pair'),
	14=>array( 'id'=>14, 'quality' => 'Q2', 'reason_code' => 'USED_PRODUCT', 'reason_code_display' => 'Manufacturing Defect', 'reason_description' => 'Used Product' , 'user_display' => 'Used Product'),
	15=>array( 'id'=>15, 'quality' => 'Q2', 'reason_code' => 'TAG_BOX_MISMATCH', 'reason_code_display' => 'USED PRODUCT', 'reason_description' => 'Tag / Box Mismatch', 'user_display' => 'Tag / Box Mismatch'),
	16=>array( 'id'=>16, 'quality' => 'Q2', 'reason_code' => 'PRODUCT_MISMATCH', 'reason_code_display' => 'PRODUCT MISMATCH', 'reason_description' => 'Product Mismatch', 'user_display' => 'Product Mismatch' ),
	17=>array( 'id'=>17, 'quality' => 'Q4', 'reason_code' => 'STICKER_MISMATCH', 'reason_code_display' => 'STICKER MISMATCH', 'reason_description' => 'Sticker Mismatch' , 'user_display' => 'Sticker Mismatch'),
	18=>array( 'id'=>18, 'quality' => 'Q2', 'reason_code' => 'BOX_DAMAGE', 'reason_code_display' => 'BOX DAMAGE', 'reason_description' => 'Box Damage', 'user_display' => 'Box Damage' ),
); */

$print = $_GET['print'];
if($print == 'true'){
	$giftmessage = func_query_first_cell("select notes from xcart_orders where orderid=".$_GET['orderid']);
	$giftmessage = explode("___", $giftmessage);
	$smarty->assign('receiver', $giftmessage[0]);
	$smarty->assign('sender', $giftmessage[1]);
	$smarty->assign('giftmessage', $giftmessage[2]);
	func_display("print_giftmessage.tpl", $smarty);
	exit;
}

$searchParam  = $HTTP_POST_VARS['searchParam'];
$inputVal  = trim($HTTP_POST_VARS['inputVal']);
$warehouseId  = $HTTP_POST_VARS['warehouseId'];

$action  = $HTTP_POST_VARS['action'];
if($action == '')
	$action ='view';

if($action == 'getData') {

	if(empty($warehouseId)) {
		$smarty->assign("errorMsg","Please select your operations location before proceeding");
		$action = "view";
	} else {
		if($searchParam == 'itembarcode') {
			$response = ItemApiClient::get_items_for_barcodes($warehouseId, $inputVal);
			$weblog->info("Input value - ". print_r($response, true));
			$orderid = $response[0]['orderId'];
		} else {
			$orderid = $inputVal;
		}
		if(!isInteger($orderid)){
		    $smarty->assign("errorMsg","No Order found for scanned value - $inputVal");
		    $action = "view";
		} else {
				$giftorder = func_query_first("select shipping_method,gift_status, notes from xcart_orders where orderid=".$orderid);
				
				$smarty->assign("shipping_method", $giftorder['shipping_method']);
				$smarty->assign("gift_message", $giftorder['notes']);
				$smarty->assign("gift_status", $giftorder['gift_status']);
	    	    $orderItems = func_get_items_for_order($orderid, $warehouseId);
	            $smarty->assign("assignedItems", $orderItems['A']);
	            $smarty->assign("qadoneItems", $orderItems['QD']);
                    $smarty->assign("premium_packaging", $orderItems['PREMIUM']);

                    
	            $weblog->info("Assigned Items for order $orderid = ". print_r($orderItems['A'], true));
	            $weblog->info("QA Done Items for order $orderid = ". print_r($orderItems['QD'], true));
	            $courier_details = func_query_first("select * from mk_ordercommentslog where commenttype = 'ORDER_UPDATE' and commenttitle = 'ADDRESS_CHANGE' and orderid = $orderid ");	
	            if(!$courier_details){
	                $smarty->assign("address_changed", "false");
	            }else{
	                $smarty->assign("address_changed", "true");
	            }
	            $sql = "select count(1) as to_be_processed from xcart_orders xo left join mk_old_returns_tracking rto on xo.orderid = rto.orderid where xo.status in ('PK','SH','DL','C') and xo.login = '$productAndCourierDetails[login]' and rto.orderid is null";
				$order_stats = func_query_first_cell($sql,true);
				if($order_stats == 0){
					include_once($xcart_dir."/include/class/widget/class.widget.keyvaluepair.php");
					$first_order_message = WidgetKeyValuePairs::getWidgetValueForKey('firstorderpackinginstruction');
					$smarty->assign("first_order",$first_order_message);
				}
	            $smarty->assign("orderid", $orderid);
	            $action = "showData";
		}
	}
} else if ($action == 'skuitem_status_update') {
	$qcReasonId = mysql_escape_string($HTTP_POST_VARS['qaresult']);
	$qcReason = $quality_check_codes[$qcReasonId];
	$orderid  = $HTTP_POST_VARS['orderid'];
	if($qcReasonId == "61"){
		$failureMsg = func_mark_core_item_qa_pass($HTTP_POST_VARS, $qcReason);
		if($failureMsg != "") {
			$smarty->assign("updateErrorMsg", $failureMsg);	    	
	    }
	} else {
		$failureMsg = func_mark_core_item_qa_fail($HTTP_POST_VARS, $qcReason);
		if($failureMsg != "") {
			$smarty->assign("updateErrorMsg", $failureMsg);	    	
	    }
	}
	
        
	$giftorder = func_query_first("select shipping_method,gift_status, notes from xcart_orders where orderid=".$orderid);
	$smarty->assign("shipping_method", $giftorder['shipping_method']);
	$smarty->assign("gift_message", $giftorder['notes']);
	$smarty->assign("gift_status", $giftorder['gift_status']);
	$orderItems = func_get_items_for_order($orderid);
	$smarty->assign("assignedItems", $orderItems['A']);
	$smarty->assign("qadoneItems", $orderItems['QD']);
        $smarty->assign("premium_packaging", $orderItems['PREMIUM']);
        
	$weblog->info("Assigned Items for order $orderid = ". print_r($orderItems['A'], true));
	$weblog->info("QA Done Items for order $orderid = ". print_r($orderItems['QD'], true));
	$smarty->assign("orderid", $orderid);
	$action = "showData";
} else if ($action == 'update_packaging_status') {
	$orderid = $HTTP_POST_VARS['orderid'];
        $itemid = $HTTP_POST_VARS['itemid'];
        $userid = $HTTP_POST_VARS['userid'];
        
        $result = func_mark_item_premium_packaged($orderid, $itemid, $userid);
   
        if($result != 1){
            $smarty->assign("updateErrorMsg", "Failed to update item - $itemid - ".$result);	    
        }else{
            $smarty->assign("successMsg", "Marked Item $itemid Premium Packed");
        }
        
        
        $giftorder = func_query_first("select shipping_method,gift_status, notes from xcart_orders where orderid=".$orderid);
	$smarty->assign("shipping_method", $giftorder['shipping_method']);
	$smarty->assign("gift_message", $giftorder['notes']);
	$smarty->assign("gift_status", $giftorder['gift_status']);
	$orderItems = func_get_items_for_order($orderid);
	$smarty->assign("assignedItems", $orderItems['A']);
	$smarty->assign("qadoneItems", $orderItems['QD']);
	$smarty->assign("premium_packaging", $orderItems['PREMIUM']);

	$weblog->info("Assigned Items for order $orderid = ". print_r($orderItems['A'], true));
	$weblog->info("QA Done Items for order $orderid = ". print_r($orderItems['QD'], true));
	$smarty->assign("orderid", $orderid);
	$action = "showData";

    
} else if ($action == 'update_item_status') {
	$orderid  = $HTTP_POST_VARS['orderid'];
	$itemid = $HTTP_POST_VARS['itemid'];
	
	$voucherArticleType = WidgetKeyValuePairs::getWidgetValueForKey('freeProducts.articleType');
	$voucherMarked = true;
	if($voucherArticleType){
		$sql = "select itemid from xcart_order_details xod, mk_style_properties sp where xod.product_style = sp.style_id and ( sp.global_attr_article_type = $voucherArticleType or sp.global_attr_sub_category = $voucherArticleType or sp.global_attr_master_category = $voucherArticleType) and xod.itemid = $itemid";
		$is_voucher = func_query_first($sql,true);
		if($is_voucher['itemid'] == $itemid){
			$voucherMarked = DiscountEngine::attachVoucherToOrder($orderid,$_POST['itembarcodes'], $_POST['userid']);	
		}	
	}
	
	if(!$voucherMarked){
		$errMsg = "Item is a voucher and could not be associated with order. Please try again later";
	}
	if(!$errMsg){
	        $errMsg = func_mark_item_qadone($orderid, $itemid, $_POST['itembarcodes'], $_POST['userid'], $warehouseId);	
	}
	
	if($errMsg != "")
		$smarty->assign("updateErrorMsg", "Failed to update item - $itemid - ".$errMsg);	    
	else 
		$smarty->assign("successMsg", "Marked Item $itemid QA Done Successfully");
	
        
	$giftorder = func_query_first("select shipping_method,gift_status, notes from xcart_orders where orderid=".$orderid);
	$smarty->assign("shipping_method", $giftorder['shipping_method']);
	$smarty->assign("gift_message", $giftorder['notes']);
	$smarty->assign("gift_status", $giftorder['gift_status']);
	$orderItems = func_get_items_for_order($orderid);
	$smarty->assign("assignedItems", $orderItems['A']);
	$smarty->assign("qadoneItems", $orderItems['QD']);
        $smarty->assign("premium_packaging", $orderItems['PREMIUM']);
	
	$weblog->info("Assigned Items for order $orderid = ". print_r($orderItems['A'], true));
	$weblog->info("QA Done Items for order $orderid = ". print_r($orderItems['QD'], true));
	$smarty->assign("orderid", $orderid);
	$action = "showData";
}else if($action == 'update_order_weight'){
	$orderid = $HTTP_POST_VARS['orderid'];
	$userid = $HTTP_POST_VARS['userid'];
	$weight = trim($HTTP_POST_VARS['weight']);
	
	if($weight != null){
		func_query("update xcart_orders set weight = $weight where orderid = $orderid");
		$smarty->assign("successMsg", "Updated order weight");
	}else{
		$smarty->assign("updateErrorMsg", "weight is empty, nothing to update!");
	}
	$orderItems = func_get_items_for_order($orderid);
	$smarty->assign("assignedItems", $orderItems['A']);
	$smarty->assign("qadoneItems", $orderItems['QD']);
        $smarty->assign("premium_packaging", $orderItems['PREMIUM']);

	$smarty->assign("orderid", $orderid);
	$action = "showData";
}

$specialPincodeToWarehouseIdMap = json_decode(WidgetKeyValuePairs::getWidgetValueForKey('specialPincodeToWarehouseIdMap'), true);
$specialPincodes = array_keys($specialPincodeToWarehouseIdMap);
$order_details = func_query_first("select s_zipcode, status, courier_service,shipping_method, warehouseid from xcart_orders where orderid = ".$orderid);
$packing_cutoff = func_query_first_cell("select `value` from order_additional_info where order_id_fk = $orderid and `key` = 'EXPECTED_PACKING_TIME'");
$qc_cutoff = func_query_first_cell("select `value` from order_additional_info where order_id_fk = $orderid and `key` = 'EXPECTED_QC_TIME'");


$extraInvoiceMap = json_decode(WidgetKeyValuePairs::getWidgetValueForKey('extraInvoiceMap'), true);
$extraInvoicePincode = $extraInvoiceMap[$order_details['s_zipcode']];
$extraInvoiceCopies =1;
$extraInvoiceAlertMsg = WidgetKeyValuePairs::getWidgetValueForKey('extraInvoiceAlertMsg');

if($extraInvoicePincode && !empty($extraInvoicePincode)){
    $extraInvoiceCopies = $extraInvoicePincode[$order_details['courier_service']];
    if(!$extraInvoiceCopies || $extraInvoiceCopies < 1 || empty($extraInvoiceCopies)){
       $extraInvoiceCopies =1;
    }
}

$warehouse_results = WarehouseApiClient::getAllWarehouses();
$warehouses = array();
foreach($warehouse_results as $row) {
	$warehouses[] = array('id'=>$row['id'], 'display_name'=>$row['name']);
}
if ($order_details['warehouseid'] != $warehouseId) {
    $warehouseName = $warehouseId;
    foreach($warehouses as $warehouseInfo) {
        if ($warehouseInfo['id'] == $warehouseId) {
            $warehouseName = $warehouseInfo['display_name'];
            break;
        }
    }
    $smarty->assign("errorMsg", "The item / shipment does not belong to warehouse : " . $warehouseName);
    $action = "view";
}
$special_pincode =  in_array($order_details['s_zipcode'], $specialPincodes) ? 'true' : 'false';

$skipSpecialPincodeRest = WidgetKeyValuePairs::getWidgetValueForKey('skipSpecialPincodeRestriction');
$s_zipcode=$order_details['s_zipcode'];
$skipSpecialPincodeRestriction =    ($specialPincodeToWarehouseIdMap[$s_zipcode] == $skipSpecialPincodeRest) ? 'true':'false';
$smarty->assign("skipSpecialPincodeRestriction",$skipSpecialPincodeRestriction);

$smarty->assign("msg",$extraInvoiceAlertMsg);
$smarty->assign("extraCopies",$extraInvoiceCopies);
$smarty->assign("special_pincode",$special_pincode);
$smarty->assign("order_status",$order_details['status']);
$smarty->assign("courier",$order_details['courier_service']);
$smarty->assign("shipping_method",$order_details['shipping_method']);
$smarty->assign("qc_cutoff",$qc_cutoff);
$smarty->assign("packing_cutoff",$packing_cutoff);
$smarty->assign('searchParam', $searchParam);
$smarty->assign('inputVal',$inputVal);
$smarty->assign('warehouseId',$warehouseId);
$smarty->assign('action',$action);
$smarty->assign("quality_check_codes", $quality_check_codes);
$smarty->assign("warehouses", $warehouses);
$smarty->assign("main", "qa_actions");
func_display("admin/home.tpl",$smarty);

?>
