<?php 
/*
 * PORTAL-2415: Kundan: Major refactoring of size unification scripts
 * Introduced Incremental Option in command-line, where in it will try to unify 
 * only those styles which are not already unified.
 * In order to use this, one should not use ClearAll
 * 
 * The regular series of steps for Size Unification from command line are:
 * (i) php <this_script_name.php> ClearAll
 * (ii) php <this_script_name.php> UpdateSca [incremental]
 * (iii) php <this_script_name.php> UpdateRep [incremental]
 * (iv) php <this_script_name.php> UpdateIma [incremental]
 * Note that (ii) and (iii) are interchangeable
 * There is a shortcut to this:
 *  php <this_script_name.php> UpdateAll [incremental]
 *  
 * It can also be used from admin UI interface. But there, you can only do for a limited set of styles 
 */ 
require "auth.php";
require "size_unification_admin_new_function.php";

set_time_limit(0);
$retval = ini_set('memory_limit','1600M');

$isCli = (php_sapi_name() === "cli");

if(!$isCli) {
	require_once $xcart_dir."/include/security.php";
	$mode=$_POST["mode"];//should be one of: ClearAll|UpdateSca|UpdateRep|UpdateIma|UpdateAll|ClearCache
	$listofstyles = $_POST["styleId"];//1728,8482,18201,17463
	if(!empty($mode)) {
		if(empty($listofstyles)) {
			$message = "Please enter Comma/newline Separated List of style-ids";
		}
		else {
			//validate this input with regex: style-ids can be separated by , or any whitespace character ie " " \r \n \f \t 
			$isValidListOfStyles = preg_match("/^[0-9]+[0-9\s,]*$/", $listofstyles);//\s is the class of all white-space characters
			if($isValidListOfStyles){
				$styleArr = preg_split("/[\s,]+/", $listofstyles,-1,PREG_SPLIT_NO_EMPTY);
				$styleCSV = implode(",", $styleArr);
				/*$lastCharIndex = strlen($listofstyles)-1;
				//if last cahr is , then remove it 
				if($listofstyles[$lastCharIndex] == ','){
					$listofstyles = substr($listofstyles,0,$lastCharIndex);
				}*/
				if(empty($styleCSV)) {
					$message = "Please enter Comma/newline Separated List of style-ids";
				}
				else {
					echo "<pre>";
					//Invoke top-level action method
					$message = doSizeUnification($mode , $styleCSV, false, false);
					echo "</pre>";
				}
			}
			else {
				$message = "Style-IDs should be Comma Separated or white space separated Numbers";
			}
		}
	}
	$smarty->assign("message",$message);
	$smarty->assign("main","size_unification_admin_new");
	func_display("admin/home.tpl",$smarty);
} else {
	echo "Script started at ".date("r", time())."\n";
	$startTime = time();
	$mode = $_SERVER["argv"][1];//should be one of: ClearAll|UpdateSca|UpdateRep|UpdateIma|UpdateAll
	if(array_search($mode, array("ClearAll","UpdateSca","UpdateRep","UpdateIma","UpdateAll")) !== FALSE ){
		$incremental = $_SERVER["argv"][2];
		$isIncremental = (strtolower($incremental) === "incremental");
		if($isIncremental && $mode==="ClearAll"){
			echo "\nWARNING: incremental option is not applicable for ClearAll, so incremental flag will be ignored\n";
			$isIncremental = false;
		}
		//$listofstyles = "29155,24880";//1728,8482,18201,17463";
		$listofstyles = "";
		$message = doSizeUnification($mode, $listofstyles,true,true, $isIncremental);
		echo "\n----------------------------------------------------------------------";	
		echo "\n$message";
		echo "\n----------------------------------------------------------------------";	
	}
	else {
		echo "\nUsage:";
		echo "\n(i) php <this_script_name.php> ClearAll";
		echo "\n(ii) php <this_script_name.php> UpdateSca [incremental]";
		echo "\n(iii) php <this_script_name.php> UpdateRep [incremental]";
 		echo "\n(iv) php <this_script_name.php> UpdateIma [incremental]";
 		echo "\nThere is a shortcut to this (UpdateSca, UpdateRep, UpdateIma in sequence):";
 		echo "\nphp <this_script_name.php> UpdateAll [incremental]";
	}
	$endTime = time();
	echo "\nTotal Time elapsed in $mode $incremental: ".number_format(($endTime-$startTime)/60,2)." mins";
	echo "\nScript ended at ".date("r", time())."\n";
}
?>
