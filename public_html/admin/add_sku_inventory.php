
<?php
require "./auth.php";
include_once("$xcart_dir/include/security.php");
include_once("../include/func/func.inventory.php");

if(isset($HTTP_POST_VARS["addinventory"])){
	$sku_id= $HTTP_POST_VARS['skuid'];
	$sku_name= $HTTP_POST_VARS['sku'];
	$sku_count= $HTTP_POST_VARS['sku_count'];
	$sku_unitprice= $HTTP_POST_VARS['sku_unitprice'];
$title= $HTTP_POST_VARS['title'];
$addedby= $HTTP_POST_VARS['addedby'];

$vendor= $HTTP_POST_VARS['vendor'];
$quantity_added= $HTTP_POST_VARS['quantity_added'];
$unitprice= $HTTP_POST_VARS['unitprice'];
$totalprice=$HTTP_POST_VARS['totalprice'];
$tax=$HTTP_POST_VARS['tax'];
$shipping=$HTTP_POST_VARS['shipping'];
$notes= $HTTP_POST_VARS['notes'];
$attachment= $HTTP_POST_FILES['attachment']['name'];
$image_name = basename($attachment);
$imagefile_size = $HTTP_POST_FILES['attachment']['size'];
$imagefile_tmp_name = $HTTP_POST_FILES['attachment']['tmp_name'];
$updatedCount=$sku_count+$quantity_added;
//making unit price the weighted average of all the inventories added so far
$updatedunitprice=($sku_count*$sku_unitprice+$quantity_added*$unitprice)/$updatedCount;
$type="A";
$type_desc="Added";
	
	$sku_createddate=time();	
	$sku_query="insert into `mk_inventory_log` (sku_id,date,changedby,title,unitprice,inventory_type,inventory_type_desc,quantity,tax,shipping_cost,notes,attachment1) values ('$sku_id','$sku_createddate','$addedby','$title','$unitprice','$type','$type_desc','$quantity_added','$tax','$shipping','$notes','$image_name')";
	db_query($sku_query);
	$update_count_query="update `mk_inventory_sku` set currentcount='$updatedCount', sku_unitprice ='$updatedunitprice' where sku_id='$sku_id'";
	db_query($update_count_query);
	func_upload_attachment($attachment,$imagefile_size,$imagefile_tmp_name);
	
	$smarty->assign("commentSuccess","Y");
	
	func_display("add_sku_inventory.tpl",$smarty);
	
}
else{
$skuid = $_GET['skuid'];
$date=date("d",time())."-".date("M",time()).",".date("Y",time());
$smarty->assign("skuid", $skuid);
$skuDetails=func_getSkuDetails($skuid);
$smarty->assign("sku_name", $skuDetails[0]);
$smarty->assign("sku_unit", $skuDetails[1]);
$smarty->assign("sku_vendor", $skuDetails[2]);


$smarty->assign("date", $date);
$smarty->assign("sku_count", $skuDetails[3]);

func_display("add_sku_inventory.tpl",$smarty);
}





?>

	
