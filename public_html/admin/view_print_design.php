<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: order.php,v 1.60 2006/01/11 06:55:57 mclap Exp $
#

require "./auth.php";
require $xcart_dir."/include/security.php";

x_load('mail','order');

if ($mode == "update") 
{
	#
	# Update orders info (status)
	#
	if (is_array($order_status) && is_array($order_status_old)) {
		foreach ($order_status as $orderid=>$status) {
			if (is_numeric($orderid) && $status != $order_status_old[$orderid])
				func_change_order_status($orderid, $status);
		}

		func_header_location("orders.php".(empty($qrystring)?"":"?$qrystring"));
	}
}


#
# Collect infos about ordered products
#

require $xcart_dir."/include/view_print_design.php";

$order = $order_data["order"];
$userinfo = $order_data["userinfo"];


$products = $order_data["products"];

//Added hack for autocustimization for Headstrong brandstore
$specialinstruction ;
for($i =0 ; $i < count($products); $i++)
{
	$productdetail = $products[$i];
	if($productdetail['product_style_id'] == 489)
	{
		$specialinstruction = " Please print '".$userinfo['b_firstname']. "' on the black sublimation mug in place of 'Your Name'";
	}
	$smarty->assign("instruction", $specialinstruction);
}

$giftcerts = $order_data["giftcerts"];
$smarty->assign("orderid", $orderid);

if ($mode == "status_change") 
{
	#
	# Update order
	#
	$query_data = array (
		"tracking" => $tracking,
		"customer_notes" => $customer_notes,
		"notes" => $notes
	);
	if (isset($HTTP_POST_VARS['details'])) {
		$query_data['details'] = func_crypt_order_details($details);
	}

	func_array2update("orders", $query_data, "orderid = '$orderid'");

	func_change_order_status($orderid, $status);

	$top_message = array(
		"content" => func_get_langvar_by_name("txt_order_has_been_changed")
	);
	func_header_location("order.php?orderid=".$orderid);
}


$smarty->assign("main","view_print_design");

# Assign the current location line
$smarty->assign("location", $location);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
