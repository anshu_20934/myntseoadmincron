<?php
/*
 * Created on Nov 19, 2011
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 	require "./auth.php";
	require $xcart_dir."/include/security.php";
	require_once $xcart_dir."/include/func/func.order.php";
	require_once $xcart_dir."/include/func/func.sms_alerts.php";
	require_once("$xcart_dir/include/func/func.order.php");
	require_once("$xcart_dir/modules/apiclient/ItemApiClient.php");

    echo "Access to this page is disabled.<br><br>For any issues related to item status, please send mail to lmsoncall@myntra.com";
    exit(0);

	$action  = $HTTP_POST_VARS['action'];
	if($action == '')
    	$action ='upload';
    
    $errorMsg = "";
    if ($action=='verify') {
    	
    	$dataValid = true;
    	$courier = $_POST['courier_service'];
	    $smarty->assign("courier_service", $courier);
	    if($courier == '0'){
	    	$errorMsg .= "Please select a courier service.<br>";
	    	$dataValid = false;
	    }
	    	
	    $update_to = $_POST['update_to'];
	    $smarty->assign("update_to", $update_to);
	    if(!$update_to || $update_to == ""){
	    	$errorMsg .= "Please select shipping option.<br>";
	    	$dataValid = false;
	    }
    	
		$uploadStatus = false;
		if(!isset($_FILES) && isset($HTTP_POST_FILES))
    		$_FILES = $HTTP_POST_FILES;
		
	    if(!empty($_FILES['orderdetail']['name']))
	    {
	    	$uploaddir1 = '../bulkorderupdates/';
	        $extension = explode(".",basename($_FILES['orderdetail']['name']));
	        $uploadfile1 = $uploaddir1 . date('d-m-Y-h-i-s').".".$extension[1];
	        if (move_uploaded_file($_FILES['orderdetail']['tmp_name'], $uploadfile1)) {
	        	$uploadStatus = true;
	        }
	    }
	    
	    if(!$uploadStatus) {
	    	$errorMsg .=  "<br>File not uploaded, try again.. <br>";
	    	$dataValid = false;
	    }
	    
	    if($dataValid) {
	    	$orderids = array();
	        // open the text file
	        $fd = fopen ($uploadfile1, "r");
	        $orderListDisplay =  array();
	        $k = 0;
	        $invalidOrderIds = array();
	        while (!feof ($fd)) {
	        	$buffer = fgetcsv($fd, 4096);
	        	$orderid = trim($buffer[0]);
	        	$trackingno = trim($buffer[1]);
	              	
	            if($orderid=="" || $trackingno=="") continue;
	              	
	            if(!isInteger($orderid)){
	            	$invalidOrderIds[] = $orderid;	
	            	continue;
	            }
	  			$orderids[] = $orderid;	            	
	            $orderListDisplay[$k][0] = $orderid;
	            $orderListDisplay[$k][1] = $trackingno;
	            $orderListDisplay[$k++][2] = $courier;
	            
	            if($k > 50) {
	            	$dataValid = false;
          			$errorMsg .= "Maximum of 50 orders can be marked shipped using bulk uploader in single execution.<br/>";
          			break;
	            }
	        }
	        fclose ($fd);
	        
	        if($dataValid) {
		        if(count($invalidOrderIds) > 0){
		        	$dataValid = false;
	          		$errorMsg .= "Following Orders are invalid: " . implode(", ", $invalidOrderIds). "<br/>";
	        	} else {
	        		if(count($orderids) > 0) {
	        			// there are orders listed in the file..
		        		if($update_to == 'ship') {
			        		$validated_orders = validateOrdersForShipping($orderids);
		        			
		        			$remaining_orderids = $validated_orders['VALID_ORDERS'];
							$cancelled_orderids  = $validated_orders['CANCELLED_ORDERS'];
							$not_wp_orderids = $validated_orders['NOT_WIP_ORDERS'];
							$qa_pending_wp_orderids = $validated_orders['QA_PENDING_ORDERS'];
							$non_serviceable_orderids = $validated_orders['NO_COURIER_ORDERS'];
							$invalid_orders = $validated_orders['INVALID_ORDERS'];
							$shipped_orderids = $validated_orders['SHIPPED_ORDERS'];
							$remaining_orderids = array_merge($remaining_orderids,$non_serviceable_orderids);
			        		if(count($invalid_orders)>0){
			        			$dataValid = false;
								$errorMsg .= "Following Orders do not exist: " . implode(", ", $invalid_orders). "<br/>";
							}
							if(count($cancelled_orderids) > 0) {
								$dataValid = false;
								$errorMsg .= "Following Orders are cancelled: " . implode(", ", $cancelled_orderids). "<br/>";
							}
							if(count($not_wp_orderids) > 0) {
								$dataValid = false;
								$errorMsg .= "Following Orders are not in WP state: " . implode(", ", $not_wp_orderids). "<br/>";
							}
							if(count($qa_pending_wp_orderids) > 0) {
								$dataValid = false;
								$errorMsg .= "Following orders contain items which are not marked QADone -: " . implode(", ", $qa_pending_wp_orderids). "<br/>";
							}
							if(count($shipped_orderids) >0){
								$dataValid = false;
								$errorMsg .= "Following orders are already in shipped state : " . implode(", ", $shipped_orderids). "<br/>";
							}					        
				        } else if($update_to == "reship") {
				        	$shipped_orders_sql = "Select distinct orderid from $sql_tbl[orders] where orderid in (".implode(", ", $orderids).") and status = 'SH'";
							$shipped_orders_rows = func_query($shipped_orders_sql, true);
							$shipped_orderids = array();
							if($shipped_orders_rows) {
								foreach ($shipped_orders_rows as $row) {
									$shipped_orderids[] = $row['orderid'];
								}
							}
							$not_shipped_orderids = array_diff($orderids, $shipped_orderids);
				        	if(!empty($not_shipped_orderids)) {
				        		$dataValid = false;
								$errorMsg .= "Following orders are not in Shipped state - ".implode(', ', $not_shipped_orderids)." <br>";
							} else {
					        	if(count($shipped_orderids) > 0) {
							       	$not_all_sent_items_sql = "SELECT distinct orderid from $sql_tbl[order_details] where orderid in (".implode(",", $shipped_orderids).") and item_status != 'S' and item_status != 'IC'";
									$not_all_sent_items_rows = func_query($not_all_sent_items_sql, true);
									$invalid_item_status_orderids = array();
									if($not_all_sent_items_rows) {
										foreach ($not_all_sent_items_rows as $row) {
											$invalid_item_status_orderids[] = $row['orderid'];
										}
									}	            
									if(!empty($invalid_item_status_orderids)) {
										$dataValid = false;
										$errorMsg .= "Following orders contain items which are not in Sent Outside state- ".implode(',', $invalid_item_status_orderids)." <br>";	
									}
								}
				        	}
				        }
	        		}
	        	}
	        }
	    }
	    
	    if($dataValid) {
	    	$smarty->assign('orderListDisplay',$orderListDisplay);
	        $smarty->assign('uploadorderFile',$uploadfile1);
	        $uniqueId = uniqid();
	        $smarty->assign('progresskey', $uniqueId);
	    } else {
	    	$smarty->assign('errorMsg',$errorMsg);
       		$action ='upload';
	    }
    } else if ($action == 'confirmorder') {
    	$filetoread  = $HTTP_POST_VARS['filetoread'];
		$courier = $HTTP_POST_VARS['courier_service'];
		$update_to = $HTTP_POST_VARS['update_to'];
		$progressKey = $HTTP_POST_VARS['progresskey'];
	    if(file_exists($filetoread)){
	        $orders_data=array();
	        $fd = fopen ($filetoread, "r");
	        $orderIds = array();
	        while (!feof ($fd)) {
	        	$buffer = fgetcsv($fd, 4096, ",");
	            $oid = trim($buffer[0]);
	            if($oid=="")continue;
				
				$orderIds[] = $oid;
				$orders_data[$oid] = array('tracking'=>trim($buffer[1]), 'courier'=>$courier);
	        }
	        fclose($fd);
	        
	        $final_response = mark_orders_shipped($orderIds, $_POST['login'], 'Marked from Bulk Uploader', $progressKey, $orders_data);
	        
			$errorMsg = "";
			if(isset($final_response['unsuccessful'])){
				$uncsuccessfulOrderIds = $final_response['unsuccessful']['orderids'];
				$errorMsg .= "Following order were not marked Shipped - ".implode(", ", $uncsuccessfulOrderIds).". Error - ".$final_response['unsuccessful']['errormsg']."<br>";
			}
			if(isset($final_response['failure']) && count($final_response['failure']) > 0) {
				foreach($final_response['failure'] as $record) {
					$errorMsg .= "Order ".$record['orderid']." cannot be marked Shipped. Error - ".$record['errormsg']."<br>";
				}
			}
			
			if(isset($final_response['invalid'])){
				$errorMsg .= "Folllowing orders are invalid - ".implode(", ", $final_response['invalid']['orderids']);
			}
			if(isset($final_response['successful'])){
				$successMsg = "Folllowing orders were successfully marked Shipped - ".implode(", ", $final_response['successful']['orderids']);
			}
			
			$smarty->assign('msg', $successMsg);
			$smarty->assign("errorMsg", $errorMsg);
			$action = "upload";
	    } else {
	    	$action = "upload";
	    	$errorMsg = "File does not exist";
	        $smarty->assign('errorMsg',$errorMsg);
	    }
    }
    	
    $couriers = get_supported_courier_partners();
	$smarty->assign("action",$action);
	$smarty->assign("couriers", $couriers);
	$template ="upload_bulk_order";
	$smarty->assign("main",$template);
	func_display("admin/home.tpl",$smarty);

?>
