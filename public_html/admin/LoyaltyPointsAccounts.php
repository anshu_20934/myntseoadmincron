<?php 

require "./auth.php";
require $xcart_dir."/include/security.php";

include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalClient.php";



function get_all_transactions($request) {
	if(empty($request['userlogin']) || (empty($request['accountType']))  ) {
		return false;
	}
	$start = $request['start'] == null ? 0 : $request['start'];
	$limit = $request['limit'] == null ? 30 : $request['limit'];

	$couponAdapter = CouponAdapter::getInstance();
	//$results = $couponAdapter->getCashbackTransactionsForUser($request['userlogin'], $start, $limit);
	//$results = MyntCashService::getUserMyntCashTransacionsForMyMyntra($request['userlogin']);
	$actType = LoyaltyPointsPortalClient::ACTIVE_ACCOUNT;
	if($request['accountType'] == "inActivePoints"){
		$actType = LoyaltyPointsPortalClient::INACTIVE_ACCOUNT;
	}
	
	
	
	$results = LoyaltyPointsPortalClient::getTransactionHistory($request['userlogin'],$actType,$start,$limit);
	foreach($results as $id=>$row){
		$results[$id]["login"] = $request['userlogin'];
		$results[$id]["modifiedOn"] = date("d-m-Y H:i:s",(int) ($row['modifiedOn']/1000));
	}
	$count = sizeOf($results);
	
	$count = ($count == 30) ? $count+$start+5 : $start+$count; // if we recieve 30 , notify extjs to enable pagination by specifying more count
	
	return array("results" => $results, "count" =>$count );


}

if ($REQUEST_METHOD == "POST") {
	if ($_POST['action'] == 'search') {

		$retarr = get_all_transactions($_POST);

		header('Content-type: text/x-json');
		print json_encode($retarr);
	}

} else {
	
	$smarty->assign("main","loyaltyPoints");
	func_display("admin/home.tpl", $smarty);
}
?>
