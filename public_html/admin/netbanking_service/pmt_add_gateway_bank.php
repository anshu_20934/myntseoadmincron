<?php
chdir(dirname(__FILE__));

require_once "../auth.php";
require_once $xcart_dir."/include/security.php";
include_once "$xcart_dir/include/class/netbankingHelper.php";

use revenue\payments\service\PaymentServiceInterface;

if($REQUEST_METHOD == "POST") {
	if(empty($gateway_id)) {
		func_header_location($http_location . "/admin/netbanking_service/pmt_add_gateway.php");
	}
	$_time = time();
	if($mode=='add') {
		if(!empty($gateway_bank_code_new)) {
			$gateway_bank_code_new = mysql_escape_string(trim($gateway_bank_code_new));
			PaymentServiceInterface::addMapping($gateway_value_new, $gateway_id, $gateway_bank_code_new);
		}
	} elseif($mode=='delete') {
		if(!empty($mapping_id)) {
			PaymentServiceInterface::deleteMapping($mapping_id);
		}		
	} elseif($mode=='update') {
		$updatenamearray = $_POST['update_name_field'];
		foreach($updatenamearray AS $id=>$gateway_code) {
			if(!empty($gateway_code)) {
				$mapping_id = $id;
				$new_gateway_code = mysql_escape_string(trim($gateway_code));
				PaymentServiceInterface::updateMappingGatewayCode($mapping_id, $new_gateway_code);
			}
		}
	} else if($mode =='clearCache') {
		NetBankingHelper::clearCachedValues();
	}
	func_header_location($http_location. "/admin/netbanking_service/pmt_add_gateway_bank.php?id=$gateway_id"."&name=$gateway_name");	
} else if($REQUEST_METHOD == "GET") {
	if(empty($id)) {
		func_header_location($http_location . "/admin/netbanking_service/pmt_add_gateway.php");
	}
	
	$gateway_id = $id;
	$gateway_name = $name;

	$gateway_bank_list = PaymentServiceInterface::getUsedBanksForGateway($gateway_id);
	$gateway_bank_list = json_decode($gateway_bank_list, true);
	
	
	$idHash = array();
	foreach ($gateway_bank_list as $key => $value) {
		$idHash[] = $value['bank_id'];
	}
	
	$bank_list = PaymentServiceInterface::listBanks();
	$bank_list = json_decode($bank_list, true);
	$gateway_unused_banks = array();

	
	foreach ($bank_list as $key => $value) {
		if(!in_array($value['id'], $idHash)){
			$gateway_unused_banks[] = array("bank_id"=> $value['id'],"bank_name" => $value['name']);
		}
	}
	
	$smarty->assign("gateway_name", $gateway_name);
	$smarty->assign("gateway_id", $gateway_id);
	$smarty->assign("gateway_bank_list", $gateway_bank_list);
	$smarty->assign("gateway_unused_banks", $gateway_unused_banks);
	$smarty->assign("action_php","pmt_add_gateway_bank.php");
	$smarty->assign("main","netbanking_service_gateway_bank");
	func_display("admin/home.tpl",$smarty);	
} else {
	func_header_location($http_location . "/admin/netbanking/add_gateway.php");
}



?>