<?php
chdir(dirname(__FILE__));

require_once "../auth.php";
require_once $xcart_dir."/include/security.php";
include_once "$xcart_dir/include/class/netbankingHelper.php";

use revenue\payments\service\PaymentServiceInterface;

$_time = time();
if($mode=='add') {
	if(!empty($bank_name_new)){
		$name = mysql_escape_string(trim($bank_name_new));
		if(!empty($bank_popular_new)){
			$isPopular = true;	
		} else {
			$isPopular = false;
		}
		PaymentServiceInterface::addBank($name, $isPopular);
	}
	func_header_location($http_location . "/admin/netbanking_service/pmt_bank_list.php");
	
} elseif ($mode=='update') {
	$updatenamearray = $_POST['update_name_field'];
	$updatepopulararray = $_POST['update_popular_field'];
	foreach($updatenamearray AS $bank_id=>$bank_name) {
		if(!empty($bank_name)) {
			$name = mysql_escape_string(trim($bank_name));
			if(!empty($updatepopulararray[$bank_id])){
				PaymentServiceInterface::updateBanks($bank_id, $name, true );
			} else {
				PaymentServiceInterface::updateBanks($bank_id, $name, false );
			}
		}
	}
	//refresh cache here
	func_header_location($http_location . "/admin/netbanking_service/pmt_bank_list.php");
	
} elseif ($mode=='delete') {
	if(!empty($bank_id)) {
		PaymentServiceInterface::deleteBank($bank_id);
		//refresh cache here
	}
	func_header_location($http_location . "/admin/netbanking_service/pmt_bank_list.php");
	
} else if($mode =='clearCache') {
	NetBankingHelper::clearCachedValues();
	func_header_location($http_location . "/admin/netbanking_service/pmt_bank_list.php");
}

$bank_list = PaymentServiceInterface::listBanks();
$bank_list = json_decode($bank_list, true);
//var_dump($bank_list);

$smarty->assign("bank_list", $bank_list);
$smarty->assign("action_php","pmt_bank_list.php");
$smarty->assign("main","pmt_netbanking_bank_list");
func_display("admin/home.tpl",$smarty);




?>
