<?php
use revenue\payments\service\PaymentServiceInterface;

chdir(dirname(__FILE__));
require_once "../auth.php";
require_once $xcart_dir."/include/security.php";
include_once "$xcart_dir/include/class/netbankingHelper.php";


if($REQUEST_METHOD == "POST") {
	if(empty($mappingOrBankId)) {
		func_header_location($http_location . "/admin/netbanking_service/pmt_activate_gateway.php");
	}
	if($mode=='add') {
		$mapping_id = $mappingOrBankId;
		PaymentServiceInterface::activateMapping($mapping_id);
	} else if($mode=='delete') {
		$mapping_id = $mappingOrBankId;
		PaymentServiceInterface::deactivateMapping($mapping_id);
	} else if($mode=='activate') {
		$bank_id = $mappingOrBankId;
		PaymentServiceInterface::activateAllForBank($bank_id);
	} else if($mode=='reset') {
		PaymentServiceInterface::activateAll();
	} else if($mode=='add_gateway') {
		$bank_id = $mappingOrBankId;
		$bank_name = $bankName;
		func_header_location($http_location . "/admin/netbanking_service/pmt_add_bank_gateway.php?id=$bank_id"."&name=$bank_name");
	} else if($mode =='clearCache') {
		NetBankingHelper::clearCachedValues();
	}
	func_header_location($http_location . "/admin/netbanking_service/pmt_activate_gateway.php");
} else {

	$bank_gateway_lists = PaymentServiceInterface::getAllMapping();
	$bank_gateway_lists = json_decode($bank_gateway_lists, true);
	$toPass =array();
	foreach ($bank_gateway_lists as $bank_mapping) {
		$bank_name = $bank_mapping['bank_name'];
		$gateway_name =  $bank_mapping['gateway_name'];
		$is_active = $bank_mapping['is_active'];
		$id = $bank_mapping['id'];
		$bank_id = $bank_mapping['bank_id'];
		$toPass[$bank_name][] = array("id"=> $id,"bank_id" => $bank_id,"gateway_name"=>$gateway_name,"is_active" =>$is_active);
	}	
	
	$smarty->assign("bank_gateway_mapping", $toPass);
	$smarty->assign("action_php","pmt_activate_gateway.php");
	$smarty->assign("main","pmt_netbanking_gateway_mapping");
	func_display("admin/home.tpl",$smarty);		
}

?>