<?php
use revenue\payments\service\PaymentServiceInterface;

chdir(dirname(__FILE__));

require_once "../auth.php";
require_once $xcart_dir."/include/security.php";
include_once "$xcart_dir/include/class/netbankingHelper.php";

if($REQUEST_METHOD == "POST") {
	if(empty($bank_id)) {
		func_header_location($http_location . "/admin/netbanking_service/pmt_bank_list.php");
	}
	$_time = time();
	if($mode=='add') {
		if(!empty($gateway_bank_code_new)) {
			$gateway_bank_code_new = mysql_escape_string(trim($gateway_bank_code_new));
			PaymentServiceInterface::addMapping($bank_id, $gateway_value_new, $gateway_bank_code_new);
		}
	} elseif($mode=='delete') {
		if(!empty($mapping_id)) {
			PaymentServiceInterface::deleteMapping($mapping_id);
		}		
	} elseif($mode=='update') {
		$updatenamearray = $_POST['update_name_field'];
		foreach($updatenamearray AS $id=>$gateway_code) {
			if(!empty($gateway_code)) {
				
				$mapping_id = $id;
				$new_gateway_code = mysql_escape_string(trim($gateway_code));
				PaymentServiceInterface::updateMappingGatewayCode($mapping_id, $new_gateway_code);
			}
		}
	} else if($mode =='clearCache') {
		NetBankingHelper::clearCachedValues();
	}
	func_header_location($http_location. "/admin/netbanking_service/pmt_add_bank_gateway.php?id=$bank_id"."&name=$bank_name");	
} else if($REQUEST_METHOD == "GET") {
	if(empty($id)) {
		func_header_location($http_location . "/admin/netbanking_service/pmt_bank_list.php");
	}
	$bank_id = $id;
	$bank_name = $name;
	$bank_gateway_list = PaymentServiceInterface::getUsedGatewaysForBank($bank_id);
	$bank_gateway_list = json_decode($bank_gateway_list, true);
	
	$idHash = array();
	foreach ($bank_gateway_list as $key => $value) {
		$idHash[] = $value['gateway_id'];  
	}
	
	$gateway_list = PaymentServiceInterface::listGateways();
	$gateway_list = json_decode($gateway_list, true);
	
	$bank_unused_gateways = array();
	
	foreach ($gateway_list as $key => $value) {
		if(!in_array($value['id'], $idHash)){
			$bank_unused_gateways[] = array("gateway_id"=> $value['id'],"gateway_name" => $value['name']);
		}		
	}
	
	$smarty->assign("bank_name", $bank_name);
	$smarty->assign("bank_id", $bank_id);
	$smarty->assign("bank_gateway_list", $bank_gateway_list);
	$smarty->assign("bank_unused_gateways", $bank_unused_gateways);
	$smarty->assign("action_php","pmt_add_bank_gateway.php");
	$smarty->assign("main","pmt_netbanking_bank_gateway");
	func_display("admin/home.tpl",$smarty);	
} else {
	func_header_location($http_location . "/admin/netbanking_service/pmt_bank_list.php");
}



?>