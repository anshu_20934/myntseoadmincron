<?php
chdir(dirname(__FILE__));

require_once "../auth.php";
require_once $xcart_dir."/include/security.php";
include_once "$xcart_dir/include/class/netbankingHelper.php";

use revenue\payments\service\PaymentServiceInterface;

if($REQUEST_METHOD == "POST") {
	$_time = time();
	if($mode=='add') {
		if(!empty($gateway_name_new)&&isset($gateway_weight_new)) {
			$gateway_name_new = mysql_escape_string(trim($gateway_name_new));
			$gateway_weight_new = mysql_escape_string(trim($gateway_weight_new));
			PaymentServiceInterface::addGateway($gateway_name_new, $gateway_weight_new);
		}
	} elseif($mode=='delete') {
		if(!empty($gateway_id)) {
			PaymentServiceInterface::deleteGateway($gateway_id);
		}		
	} elseif($mode=='update') {
		$updatenamearray = $_POST['update_name_field'];
		$update_weight_field = $_POST['update_weight_field'];
		foreach($updatenamearray AS $id=>$gateway_name) {
			$weight = $update_weight_field[$id];
			if(!empty($gateway_name)&&!empty($weight)) {
				$name = mysql_escape_string(trim($gateway_name));
				$weight = mysql_escape_string(trim($weight));
				PaymentServiceInterface::updateGateways($id, $name, $weight);
			}
		}
	} else if($mode =='clearCache') {
		NetBankingHelper::clearCachedValues();
	}	
	func_header_location($http_location . "/admin/netbanking_service/pmt_add_gateway.php");	
} else {
	
	$gateway_list = PaymentServiceInterface::listGateways();
	$gateway_list = json_decode($gateway_list, true);
	
	$smarty->assign("gateway_list", $gateway_list);
	$smarty->assign("action_php","pmt_add_gateway.php");
	$smarty->assign("main","netbanking_service_add_gateway");
	func_display("admin/home.tpl",$smarty);	
}

?>