<?php

include_once("auth.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/modules/amqpclients/producer/AMQPMessageProducer.php");
include_once("updateInventory.php");

$client = AMQPMessageProducer::getInstance();

echo "[" . date('j-m-Y H:i:s') . "(";

#$imsConn = mysql_connect('dbclusters.myntra.com:6001','myntraAppDBUser','9eguCrustuBR',TRUE);
$imsConn = mysql_connect('imsdb1.myntra.com:3306', 'MyntImsUser', '5V9D9i4Yf3j991Q#', TRUE);
#$atpConn = mysql_connect('dbclusters.myntra.com:6001','myntraAppDBUser','9eguCrustuBR',TRUE);
$atpConn = mysql_connect('atpdbmaster.myntra.com:3306', 'MyntAtpUser', 'UrJ4KTteK473Pwk#', TRUE);
$wmsConn = mysql_connect('wmsdb2.myntra.com:3306','myntraAppDBUser','9eguCrustuBR#',TRUE);

$to="wmsteam@myntra.com,nitish.jha@myntra.com,abhijith.pradeep@myntra.com,abhijit.pati@myntra.com,anubhav.agrawal@myntra.com,nitish.jha@myntra.com,arpit.jain@myntra.com,sanjay.yadav@myntra.com,aduri.sabarinath@myntra.com,rahul.kaura@myntra.com,govind.krishnan@myntra.com,narayanan.ps@myntra.com,rajeev.verma1@myntra.com,anshu.verma@myntra.com,bharani.cv@myntra.com,navneet.agarwal@myntra.com,kislay.verma@myntra.com,akanksha.gupta@myntra.com";
$to="bharani.cv@myntra.com,aduri.sabarinath@myntra.com,rajeev.verma1@myntra.com,arunkumar.g@myntra.com";
$fromHeader = "From: inventorysync@myntra.com";
$subject = "[WMSALERT]ERP Inventory: IMS and ATP";

if (!$imsConn || !$atpConn) {
    die('Could not connect: ' . mysql_error());
}

if (mysql_select_db('myntra_ims', $imsConn) === false) {
    echo "WMS db unaccessible \n";
    exit;
}

if (mysql_select_db('myntra_atp', $atpConn) === false) {
    echo "ATP db unaccessible \n";
    exit;
}

if (mysql_select_db('myntra_wms', $wmsConn) === false) {
    echo "WMS db unaccessible \n";
    exit;
}
$finalSkuQueries=array();
$finalAllSkus=array();
 $myntraSellerIds = "1,19,21,25,29";

    $authHeader = "Authorization: Basic YXBpYWRtaW46bTFudHJhUjBja2V0MTMhIw==";
    $appPropUrl = "http://erptools.myntra.com/myntra-tools-service/platform/tools/properties/search/?q=name.eq:wms.myntra.inventory.myntraSeller.ids";
    $output = shell_exec("curl -v -H '" .$authHeader. "' -H 'Content-Type:application/xml' -XGET '".$appPropUrl."' -d '".$data."'");

    $xml=simplexml_load_string($output);

    if ($xml->status->statusType=="SUCCESS")
        $myntraSellerIds=$xml->data->applicationProperty->value;

for ($loop=0;$loop<1;$loop++) {
    $offset = 0;
    $limit = 10000;
    $hasMore = true;
    $allSkus = array();
    $skuQueries = array();
    $avlOS = 0;
    $invOS = 0;
    $invavlOS = 0;

    while ($hasMore) {

        $warningSkus = array();
        $skuId2AtpInvMap = array();
        $skuId2AtpAvlMap = array();
        $skuId2ImsInvMap = array();
        $skuId2ImsAvlMap = array();

        $skuIds = array();
        $skusQuery = "select id from mk_skus where enabled = 1 LIMIT $offset, $limit";
        $sku_result = mysql_query($skusQuery, $wmsConn);
        if ($sku_result) {
            while ($row = mysql_fetch_array($sku_result, MYSQL_ASSOC)) {
                $skuIds[] = $row['id'];
            }
        }
        @mysql_free_result($sku_result);
        if (empty($skuIds)) {
            $hasMore = false;
            continue;
        }


        $atpInvCountQuery = "select seller_id,sku_id, inventory_count as net_count,available_in_warehouses from inventory where store_id = 1 and seller_id in (".$myntraSellerIds.") and supply_type = 'ON_HAND' and sku_id in (" . implode(",", $skuIds) . ")";
        $atpInvResults = mysql_query($atpInvCountQuery, $atpConn);
        if ($atpInvResults) {
            while ($row = mysql_fetch_array($atpInvResults, MYSQL_ASSOC)) {
                $skuId2AtpInvMap[$row['seller_id']][$row['sku_id']] = $row['net_count'];
                $skuId2AtpAvlMap[$row['seller_id']][$row['sku_id']] = $row['available_in_warehouses'];
            }
        }
        @mysql_free_result($atpInvResults);

        $syncSkus = array();
        $imsInvCountQuery = "select sku_id, seller_id, sum(Greatest(inventory_count,0)) as net_count,group_concat(if(Greatest(inventory_count,0)-Greatest(blocked_order_count,0)>0,warehouse_id,null) separator ',') as avlWh  from wh_inventory where sku_id in (" . implode(",", $skuIds) . ") and warehouse_id in (28,36,81,89,93) and store_id =1 and seller_id in (".$myntraSellerIds.") and supply_type = 'ON_HAND' group by sku_id, seller_id";
        $imsInvResults = mysql_query($imsInvCountQuery, $imsConn);
        if ($imsInvResults) {
            while ($row = mysql_fetch_array($imsInvResults, MYSQL_ASSOC)) {
                $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']] = $row['net_count'];
                $skuId2ImsAvlMap[$row['seller_id']][$row['sku_id']] = $row['avlWh'];
                if (isset($skuId2AtpInvMap[$row['seller_id']][$row['sku_id']]) && ($skuId2AtpInvMap[$row['seller_id']][$row['sku_id']] != $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']] || array_diff(explode(",",$skuId2AtpAvlMap[$row['seller_id']][$row['sku_id']]), explode(",",$skuId2ImsAvlMap[$row['seller_id']][$row['sku_id']])))) {
                    $syncSkus[] = $row['sku_id'] . "_" . $row['seller_id'];
                    if ($skuId2AtpInvMap[$row['seller_id']][$row['sku_id']] != $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']] && array_diff(explode(",",$skuId2AtpAvlMap[$row['seller_id']][$row['sku_id']]), explode(",",$skuId2ImsAvlMap[$row['seller_id']][$row['sku_id']]))) {
                        $invavlOS++;
                        $diff = $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']] - $skuId2AtpInvMap[$row['seller_id']][$row['sku_id']];
                        $skuQueries[] = "update inventory set inventory_count=inventory_count + " . $diff . ",available_in_warehouses='" . $skuId2ImsAvlMap[$row['seller_id']][$row['sku_id']] . "', last_modified_on = now()  where supply_type = 'ON_HAND' and store_id = 1 and seller_id=" . $row['seller_id'] . " and sku_id=" . $row['sku_id'];
                    } else if ($skuId2AtpInvMap[$row['seller_id']][$row['sku_id']] != $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']]) {
                        $invOS++;
                        $diff = $skuId2ImsInvMap[$row['seller_id']][$row['sku_id']] - $skuId2AtpInvMap[$row['seller_id']][$row['sku_id']];
                        $skuQueries[] = "update inventory set inventory_count=inventory_count + " . $diff . ", last_modified_on = now()  where supply_type = 'ON_HAND' and store_id = 1 and seller_id=" . $row['seller_id'] . " and sku_id=" . $row['sku_id'];
                    } else {
                        $avlOS++;
                        $skuQueries[] = "update inventory set available_in_warehouses='" . $skuId2ImsAvlMap[$row['seller_id']][$row['sku_id']] . "', last_modified_on = now()  where supply_type = 'ON_HAND' and store_id = 1 and seller_id=" . $row['seller_id'] . " and sku_id=" . $row['sku_id'];
                    }
                }
            }
        }
        @mysql_free_result($imsInvResults);


        $allSkus = array_merge($allSkus, $syncSkus);

        $offset += $limit;
    }
    if ($i==0) {
        $finalSkuQueries=$skuQueries;
        $finalAllSkus=$allSkus;
    } else {
        $finalAllSkus = array_intersect($finalAllSkus,$allSkus);
        $finalSkuQueries = array_intersect($finalSkuQueries,$skuQueries);
    }
}
mysql_close($atpConn);
mysql_close($imsConn);
mysql_close($atpConn);
if (count($allSkus) > 0) {
    $report= "Out of sync:".count($finalAllSkus)."\n";
    $report=$report."Inv and Aval out of sync: ".$invavlOS."\n Inv out of sync:".$invOS."\n Avl out of sync:".$avlOS."\n";
    mail($to,$subject,$report."\n".implode("\n", $allSkus),$fromHeader);
   echo $report;
//    echo implode(",", $allSkus);
    print_r($finalSkuQueries);
} else {
    echo "NA";
}
$errQueries = "";
echo "###errQueries### \n" . $errQueries . "\n";
updateInventory(null,null,null,null,$finalAllSkus,$finalSkuQueries,null,null,null,$errQueries);
//echo "###skuQueries### \n".implode("\n",$skuQueries);
echo ")" . date('j-m-Y H:i:s') . "]";
?>

