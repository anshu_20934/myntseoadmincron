<?php

require_once "WMSDBConnUtil.php";

$releasesNotInOms = array();
$releasesNotInOmsCount = 0;
$releasesStatusMismatch = array();
$releasesStatusMismatchCount = 0;

function getOrderMismatchCount(&$count) {

    ini_set('memory_limit', '-1');
    echo "Started the comparison programme \n \n";
    /* Connecting to New Portal DB */
    $portal_ro = WMSDBConnUtil::getConnection("order-db1.myntra.com:3306", TRUE, WMSDBConnUtil::IMS_READ);

    if (!$portal_ro) {
        die('Could not connect: ' . mysql_error());
    }
    echo "Connected successfully to Portal DB. Changing database to myntra \n";
    if (mysql_select_db('myntra_order', $portal_ro) === false) {
        echo "Portal db unaccessible \n";
        exit;
    }
    echo "Connected to Portal database Shard 1 \n";


    $portal_roShard3 = WMSDBConnUtil::getConnection("192.168.68.77:3306", TRUE, WMSDBConnUtil::IMS_READ);

    if (!$portal_roShard3) {
        die('Could not connect: ' . mysql_error());
    }
    echo "Connected successfully to Portal DB. Changing database to myntra \n";
    if (mysql_select_db('myntra_order', $portal_roShard3) === false) {
        echo "Portal db unaccessible \n";
        exit;
    }
    echo "Connected to Portal database Shard 3 \n";

    // Number of days to query
    $days = "1";

    $portalOrderReleaseFetchQuery = "select order_number from xcart_orders where status in ('PP','PV','Q') and date>unix_timestamp(DATE_SUB(now(), INTERVAL ".$days." DAY))and date < unix_timestamp(DATE_SUB(now(), INTERVAL 2 MINUTE)) and login not like '%sfloadtest%' and login not like '%scmloadtest%'";

    $p_result = mysql_query($portalOrderReleaseFetchQuery, $portal_ro);
    $portalOrdersArray = array();
    if ($p_result) {
        while ($row = mysql_fetch_array($p_result, MYSQL_ASSOC)) {
            $portalOrdersArray[$row['order_number']] = $row;
        }
        echo "Fetched " . count($portalOrdersArray) . " orders from New Portal Shard 1";
        mysql_free_result($p_result);
    } else {
        echo "Fetched no orders from New Portal \n";
    }


    if (mysql_select_db('myntra_order_2', $portal_ro) === false) {
        echo "Portal db unaccessible \n";
        exit;
    }
    echo "Connected to Portal database Shard 2 \n";


    $p_result = mysql_query($portalOrderReleaseFetchQuery, $portal_ro);

    if ($p_result) {
        while ($row = mysql_fetch_array($p_result, MYSQL_ASSOC)) {
            $portalOrdersArray[$row['order_number']] = $row;

        }
        echo "\nFetched " . count($portalOrdersArray) . " orders from New Portal from Shard 1 and 2";
        mysql_free_result($p_result);
    } else {
        echo "Fetched no orders from New Portal \n";
    }


//echo "Entire array from portal: \n".print_r($portalOrdersArray, true);


    mysql_close($portal_ro);

    $p_result = mysql_query($portalOrderReleaseFetchQuery, $portal_roShard3);

    if ($p_result) {
        while ($row = mysql_fetch_array($p_result, MYSQL_ASSOC)) {
            $portalOrdersArray[$row['order_number']] = $row;

        }
        echo "\nFetched " . count($portalOrdersArray) . " orders from New Portal from Shard 1, 2 and 3";
        mysql_free_result($p_result);
    } else {
        echo "Fetched no orders from New Portal \n";
    }

    mysql_close($portal_roShard3);

    echo "\n ********************************************************************************************************* \n";
    $checkForOrderids = array_keys($portalOrdersArray);
    $commaSeperatedOrderids = implode("','", $checkForOrderids);
    $commaSeperatedOrderids = "'".$commaSeperatedOrderids."'";

//    echo $commaSeperatedOrderids;
    /* * ****************************************************************************************************************** */

    /* * ****************************************************************************************************************** */
    /* Connecting to OMS DB */
    $oms_ro = WMSDBConnUtil::getConnection("omsdb1.myntra.com:3306", TRUE, WMSDBConnUtil::IMS_READ);

    if (!$oms_ro) {
        die('Could not connect: ' . mysql_error());
    }
    echo "Connected successfully to ERPDB. Changing database to myntra_oms \n";

    if (mysql_select_db('myntra_oms', $oms_ro) === false) {
        echo "Oms db unaccessible \n";
        exit;
    }
    echo "Changed database to myntra_oms \n";

    echo "Connected to OMS database \n";

    $omsOrderCount =0;
    $omsOrderReleaseFetchQuery = "select count(*) as omsOrderCount from orders where store_order_id in ($commaSeperatedOrderids)";
//    echo "Oms Query:". $omsOrderReleaseFetchQuery;
    $p_resultOms = mysql_query($omsOrderReleaseFetchQuery, $oms_ro);
    $omsOrdersArray = array();
    if ($p_resultOms) {
        while ($row = mysql_fetch_array($p_resultOms, MYSQL_ASSOC)) {

            $omsOrderCount = $row['omsOrderCount'];
        }
        echo "Fetched " .$omsOrderCount . " orders from OMS";
        @mysql_free_result($p_resultOms);
    } else {
        echo "Fetched no orders from OMS";
    }
    mysql_close($oms_ro);
    echo "\n*********************************************************************************************************** \n";
    /* * ****************************************************************************************************************** */
    /* * ****************************************************************************************************************** */
    /* Comparing order params across systems */

//`($omsOrdersArray, $portalOrdersArray);
//pushOrdersNotInOms();

// $count =  ($releasesNotInOmsCount + $releasesStatusMismatchCount)  ;

    $count=abs(count($portalOrdersArray)-$omsOrderCount);
    echo "\nBounty Lag". $count;
    return $count;
}



function compareOrders($omsOrdersArray, $portalOrdersArray) {
    global $releasesNotInOms, $releasesNotInOmsCount, $releasesStatusMismatch, $releasesStatusMismatchCount;
    //$releasesNotInSync = array();
    foreach ($portalOrdersArray as $key => $portalRowDetail) {
        //echo $omsRowDetail['status'] . '\n';
        // echo $portalRowDetail['status'] . '\n';

        if (array_key_exists($key, $omsOrdersArray)) {
            $omsRowDetail = $omsOrdersArray[$key];
            if (($omsRowDetail['status'] === 'PP' || $omsRowDetail['status'] === 'PV') && $portalRowDetail['status'] === 'Q') {
                array_push($releasesStatusMismatch, $key);
                $releasesStatusMismatchCount++;
                //$releasesNotInSync[$key] = "\n release_status (Bounty: " . $portalRowDetail['status'] . ", Oms: " . $omsRowDetail['status'] . ", Login: " . $portalRowDetail['login'] . ")";
            }

            continue;
        }

        array_push($releasesNotInOms, $key);
        $releasesNotInOmsCount++;
    }
}

?>

