<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
require_once $xcart_dir."/modules/coupon/database/CouponAdapter.php";
include_once "$xcart_dir/modules/myntCash/MyntCashTransaction.php";
include_once "$xcart_dir/modules/myntCash/MyntCashService.php";


if ($REQUEST_METHOD == "POST") {
	if($_POST['action'] == 'addtransaction') {
		$couponAdapter = CouponAdapter::getInstance();
		$txn_type = $_POST['txn_type'];
		
		
		global $XCART_SESSION_VARS;
		$username = $XCART_SESSION_VARS['login'];
		
		$specialUsers = WidgetKeyValuePairs::getWidgetValueForKey("myntCashAdminSpecialUsers");
		$specialUsers = explode(",",$specialUsers);
		
		$isUserAllowedForCreditTransaction = true;
		$restrictionMessage = "";
		
		if(!in_array($username,$specialUsers) && $txn_type == 'C' ){			
		
			$dayAmount = MyntCashService::getCreditInFlowFromUser($username,1);
			$weekAmount = MyntCashService::getCreditInFlowFromUser($username,7);
			$monthAmount = MyntCashService::getCreditInFlowFromUser($username,30);
			
			$limits = WidgetKeyValuePairs::getWidgetValueForKey("myntCashAdminInflowLimits");
			$limits = explode(",",$limits);
			
			if(isset($limits[0]) && $limits[0]<($dayAmount["amount"]+$_POST['amount'])) {
				$isUserAllowedForCreditTransaction = false;
				$restrictionMessage = "your daily limit for crediting cashback is reached. limit : Rs ".$limits[0];
			}elseif (isset($limits[1]) && $limits[1]<($weekAmount["amount"]+$_POST['amount'])){
				$isUserAllowedForCreditTransaction = false;
				$restrictionMessage = "your weekly limit for crediting cashback is reached. limit : Rs ".$limits[1];
			}elseif (isset($limits[1]) && $limits[2]<($monthAmount["amount"]+$_POST['amount'])){
				$isUserAllowedForCreditTransaction = false;
				$restrictionMessage = "your monthly limit for crediting cashback is reached. limit : Rs ".$limits[2];
			}
			
			
			//check for user limits
			$dayAmount = MyntCashService::getManualCreditInFlowForUser($_POST['userlogin'],1);
			$weekAmount = MyntCashService::getManualCreditInFlowForUser($_POST['userlogin'],7);
			$monthAmount = MyntCashService::getManualCreditInFlowForUser($_POST['userlogin'],30);
			
			$limits = WidgetKeyValuePairs::getWidgetValueForKey("myntCashAdminInflowLimitsPerCustomer");
			$limits = explode(",",$limits);
			
			if(isset($limits[0]) && $limits[0]<($dayAmount["amount"]+$_POST['amount'])) {
				$isUserAllowedForCreditTransaction = false;
				$restrictionMessage = $restrictionMessage."</br> Customers daily limit for crediting cashback is reached. limit : Rs ".$limits[0];
			}elseif (isset($limits[1]) && $limits[1]<($weekAmount["amount"]+$_POST['amount'])){
				$isUserAllowedForCreditTransaction = false;
				$restrictionMessage = restrictionMessage."</br> Customers weekly limit for crediting cashback is reached. limit : Rs ".$limits[1];
			}elseif (isset($limits[1]) && $limits[2]<($monthAmount["amount"]+$_POST['amount'])){
				$isUserAllowedForCreditTransaction = false;
				$restrictionMessage = restrictionMessage."</br> Customers monthly limit for crediting cashback is reached. limit : Rs ".$limits[2];
			}
			
			
		}		
		
		if($isUserAllowedForCreditTransaction){
			if($txn_type == 'C') {
				//$couponAdapter->creditCashback($_POST['userlogin'], $_POST['amount'], $_POST['description']);
				
				$trs = new MyntCashTransaction($_POST['userlogin'], $_POST['itemType'], $_POST['itemID'],  $_POST['businessProcess'] ,  $_POST['amount'],0, 0,  $_POST['description']);
				$trs->setGoodwillReason($_POST['goodwillReason']);
				$ret = MyntCashService::creditToUserMyntCashAccount($trs);
				
			} else {
				//$couponAdapter->debitCashback($_POST['userlogin'], $_POST['amount'], $_POST['description']);
				
				$trs = new MyntCashTransaction($_POST['userlogin'], $_POST['itemType'], $_POST['itemID'],  $_POST['businessProcess'] , 0, 0, $_POST['amount'],  $_POST['description']);
				$trs->setGoodwillReason($_POST['goodwillReason']);
				$ret = MyntCashService::debitFromUserMyntCashAccount($trs);
			}
			if(isset($ret["status"]) && $ret["status"]=="error"){
				$retarr = array('success'=>false,'msg'=>$ret["message"]);	
			}else{
				$retarr = array('success'=>true,'ret'=>"".$ret);
			}
		}else{
			$retarr = array('success'=>false,'msg'=>$restrictionMessage);
		}
		header('Content-type: text/x-json');
		print json_encode($retarr);
	}
}else {
	$searchlogin = $_GET['login'];
	$searchcoupon = $_GET['coupon'];
	
	$smarty->assign("searchlogin", $searchlogin);
	$smarty->assign("searchcoupon", $searchcoupon);
	
	$smarty->assign("main","cashbackaccounts");
	func_display("admin/home.tpl", $smarty);
}
?>
