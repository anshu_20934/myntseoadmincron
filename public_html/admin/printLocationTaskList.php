<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
include_once($xcart_dir."/include/func/func.itemsearch.php");

if($HTTP_GET_VARS){
	$query_string = http_build_query($HTTP_GET_VARS);
	$locationid = $HTTP_GET_VARS['locationid'];
}

#
#Calling function to create search query for number of records
#
$sql = func_load_location_task_list($locationid);
$sql .=" ORDER BY a.orderid ASC";
$sqllog->debug("File name##printAssigneeTaskList.php##SQL Info::>$sql");
$itemResult = func_query($sql);
$smarty->assign ("itemResult", $itemResult);

#
#Load total assigned item
#
$total_assigned_item = func_query_first_cell("SELECT SUM(amount) FROM $sql_tbl[order_details] a, $sql_tbl[orders] b WHERE a.orderid=b.orderid AND (b.status='Q' OR b.status='P') AND location='$locationid' AND (item_status='A' OR item_status='S') GROUP BY location");

#
#Load location name
#
$location_name = func_query_first_cell("SELECT location_name FROM $sql_tbl[operation_location] WHERE id='$locationid'");

$smarty->assign("total_assigned_item", $total_assigned_item);
$smarty->assign("location_name", $location_name);
$smarty->assign ("query_string", $query_string);
$smarty->assign("locationid",$locationid);

func_display("main/printLocationTaskList.tpl",$smarty);
?>
