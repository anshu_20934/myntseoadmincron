<?php
include_once("auth.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/modules/amqpclients/producer/AMQPMessageProducer.php");
include_once("updateInventory.php");

$client = AMQPMessageProducer::getInstance();

#$imsConn = mysql_connect('dbclusters.myntra.com:6001','myntraAppDBUser','9eguCrustuBR',TRUE);
$imsConn = mysql_connect('imsdb1.myntra.com:3306','MyntImsUser','5V9D9i4Yf3j991Q#',TRUE);
#$atpConn = mysql_connect('dbclusters.myntra.com:6001','myntraAppDBUser','9eguCrustuBR',TRUE);
$atpConn = mysql_connect('atpdbmaster.myntra.com:3306','MyntAtpUser','UrJ4KTteK473Pwk#',TRUE);

if (!$imsConn || !$atpConn) {
    die('Could not connect: ' . mysql_error());
}

if(mysql_select_db('myntra_ims', $imsConn ) === false) {
        echo "IMS db unaccessible \n";
        exit;
}

if(mysql_select_db('myntra_atp', $atpConn ) === false) {
        echo "ATP db unaccessible \n";
        exit;
}

$allSkus = array();
$skuQueries = array();

echo "[".date('j-m-Y H:i:s')."(";
$fileName = "/home/govind.krishnan/syncInventory/allSkus/skuList";
$excludedSkusString = file_get_contents("/home/govind.krishnan/syncInventory/allSkus/excludedSkus");
for ($i=1;$i<=269;$i++){

    $skuId2AtpInvMap = array();
    $skuId2ImsInvMap = array();
    $skuIdsString = file_get_contents($fileName.$i);
    $atpInvCountQuery = "select sku_id, greatest(inventory_count,0) as net_count from inventory where store_id = 1 and seller_id = 1 and supply_type = 'ON_HAND' and sku_id in (".$skuIdsString.")";
    $atpInvResults = mysql_query($atpInvCountQuery, $atpConn);
    if($atpInvResults) {
        while ($row = mysql_fetch_array($atpInvResults, MYSQL_ASSOC)) {
            $skuId2AtpInvMap[$row['sku_id']] = $row['net_count'];
        }
    }
    @mysql_free_result($atpInvResults);

    $imsInvCountQuery = "select sku_id, sum(greatest(inventory_count,0)) as net_count from wh_inventory where store_id = 1 and seller_id = 1 and supply_type = 'ON_HAND' and warehouse_id in (28,36,89,93) and sku_id in (".$skuIdsString.") group by sku_id";
    //$imsInvCountQuery = "select sku_id, sum( (inv_count-( GREATEST(blocked_processing_count,0)+GREATEST(blocked_missed_count,0)+GREATEST(blocked_manually_count,0))) ) as net_count from core_inventory_counts where warehouse_id in (89,28,36,93) and quality = 'Q1' and sku_id in (".$skuIdsString.") and sku_id not in (".$excludedSkusString.") group by sku_id";
    $imsInvResults = mysql_query($imsInvCountQuery, $imsConn);
    if($imsInvResults) {
        while ($row = mysql_fetch_array($imsInvResults, MYSQL_ASSOC)) {
            $skuId2ImsInvMap[$row['sku_id']] = $row['net_count'];
        }
    }
    @mysql_free_result($imsInvResults);
   
    $skuIds = explode(",",$skuIdsString);
    $syncSkus = array();
    foreach($skuIds as $skuId) {
        $imsInv = isset($skuId2ImsInvMap[$skuId])?$skuId2ImsInvMap[$skuId]:0;
        if(!isset($skuId2AtpInvMap[$skuId]) && $imsInv > 0) {
            $syncSkus[] = $skuId;
	    $query = "update inventory set inventory_count = ".$imsInv.", last_modified_on = now() where supply_type = 'ON_HAND' and store_id = 1 and seller_id = 1 and sku_id = $skuId;\n";
            $skuQueries[] = $query;
        }
        if (isset($skuId2AtpInvMap[$skuId]) && $imsInv != $skuId2AtpInvMap[$skuId]) {
            $diff = $imsInv - $skuId2AtpInvMap[$skuId]; 
            $syncSkus[] = $skuId;
            $query = "update inventory set inventory_count = inventory_count + ".$diff.", last_modified_on = now() where supply_type = 'ON_HAND' and store_id = 1 and seller_id = 1 and sku_id = $skuId;\n";
            $skuQueries[] = $query;
        }
    }
    $allSkus = array_merge($allSkus,$syncSkus);
    /*
    if(count($syncSkus) > 0) {
        foreach(array_chunk($syncSkus,100) as $ids) {
            $request = array('handler' => 'Skus', 'data' => array('userId' => 'queue', 'action' => 'enable') );
            foreach($ids as $id) {
                $request['data']['skus'][] = array('sku' => array('id' => $id));
            }
            if(!empty($request['data']['skus'])) {
                $client->sendMessage('skuChangeQueue', $request, 'PortalApiRequest','xml',true);
                $client->sendMessage('testReserveInventoryQueue', $request, 'PortalApiRequest','xml',true);
            }
        }

    }
    */
}
if (count($allSkus) > 0){
    echo implode(",", $allSkus);
}
else{
    echo "NA";
}
$errQueries="";
//updateInventory(null,null,$allSkus,$skuQueries,null,null,null,$errQueries);
//echo "###errQueries### \n".$errQueries."\n";
//echo "###skuQueries### \n".implode("\n",$skuQueries);
echo ")".date('j-m-Y H:i:s')."]";
?>
