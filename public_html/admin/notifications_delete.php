<?php
require_once  "auth.php";
require_once $xcart_dir.'/modules/RestAPI/RestRequest.php';

if($XCART_SESSION_VARS['deleteNotification']){
	$smarty->assign('ntfDeleteFlag','true');
	$XCART_SESSION_VARS['deleteNotification'] = false;		
}
else{
	$smarty->assign('ntfDeleteFlag','false');
}

$url = HostConfig::$notificationsUrl.'/search?q=id.gt:0&sortBy=id&sortOrder=DESC';
$method = 'GET';
$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Content-type: application/json';
$headers[] = 'Accept-Language: en-US';
$request = new RestRequest($url, $method);
$request->setHttpHeaders($headers);
$request->execute();
$info = $request->getResponseInfo();
$body = $request->getResponseBody();
if($info['http_code'] == 200){
	$response = json_decode($body,true);
	if($response['NotificationResponse']['status']['totalCount'] > 0){
		if($response['NotificationResponse']['status']['statusType'] == 'SUCCESS'){
			if(count($response['NotificationResponse']['NotificationList']['data'][0])==0){
				$tempArr = array();
				array_push($tempArr, $response['NotificationResponse']['NotificationList']['data']);
				$smarty->assign('results',$tempArr);
			}
			else{
				$smarty->assign('results',$response['NotificationResponse']['NotificationList']['data']);						
			}
			$smarty->assign('resultCount',$response['NotificationResponse']['status']['totalCount']);
		}
	}	
}
$smarty->assign('main','notifications_delete');
func_display("admin/home.tpl",$smarty);
?>