<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: categories.php,v 1.31 2006/02/14 14:45:23 max Exp $
#
require "./auth.php";
require $xcart_dir."/include/security.php";
$location[] = array(func_get_langvar_by_name("lbl_prdstyles_management"), "admin_product_styles.php");

if ($REQUEST_METHOD == "POST") {
	/* Upload thumbnail and detail image for customization area */
    if(!isset($_FILES) && isset($HTTP_POST_FILES)) {
    	$_FILES = $HTTP_POST_FILES;
    }

	if(!empty($_FILES['img1']['name'])) {
		$th_img ='images/style/carea/T/'.$_FILES['img1']['name'];
		$det_img ='images/style/carea/L/'.$_FILES['det_img']['name'];
		$uploaddir1 = '../images/style/carea/T/';
		$uploaddir2 = '../images/style/carea/L/';
		$uploadfile1 = $uploaddir1 . basename($_FILES['img1']['name']);
		$uploadfile2 = $uploaddir2 . basename($_FILES['det_img']['name']);
		
		if (@move_uploaded_file($_FILES['img1']['tmp_name'], $uploadfile1)) {
			define("THMIMAGE",$th_img);
		}
		
		if (@move_uploaded_file($_FILES['det_img']['tmp_name'], $uploadfile2)) {
			define("DETIMAGE",$det_img);
		}
	}
	
	if(!empty($_FILES['preview_img']['name'])) {
		$preview_img ='images/style/carea/L/'.$_FILES['preview_img']['name'];
		$uploaddir_prw = '../images/style/carea/L/';
		$uploaddir_prw = $uploaddir_prw . basename($_FILES['preview_img']['name']);
		if (@move_uploaded_file($_FILES['det_img']['tmp_name'], $uploaddir_prw)) {
			define("PRWIMAGE",$preview_img);
		}
	}
	
	if(!empty($_FILES['zoom_img']['name'])) {
		$preview_img ='images/style/carea/L/'.$_FILES['zoom_img']['name'];
		$uploaddir_prw = '../images/style/carea/L/';
		$uploaddir_prw = $uploaddir_prw . basename($_FILES['zoom_img']['name']);
		if (@move_uploaded_file($_FILES['zoom_img']['tmp_name'], $uploaddir_prw)) {
			define("ZOOMIMAGE",$preview_img);
		}
	}
	
	/* Upload thumbnail and detail image for orientation */
	if(!empty($_FILES['ortimage']['name'])) {
		/*
		$th_img = $_FILES['ortimage']['name'];
		$uploaddir1 = '../images/ProductStyleImg/Orientation/';
		if (@move_uploaded_file($_FILES['ortimage']['tmp_name'], $uploadfile1)) {
			define("ORTIMAGE",$th_img);
		}
		*/
	}
}

if(empty($mode)) {
	$mode = "";
}

if(!empty($custom_id)) {
	$data = $custom_id;
}

if (!empty($styleid)) {
	$style = $styleid;
}

$modify_orient = "no";
$template = "prd_style_custumization";


function cleanUpLimitedTextCustomizationDetails($orientationid) {
	// Clean up duplicates. In case there are multiple rows with same font_id and more than one size_default columns set to 'Y'
	$getDuplicateInDefaultSize = "select * from mk_text_font_detail where orientation_id='$orientationid' " .
								" and size_default='Y' group by font_id, size_default having count(*) > 1";
	$result = func_query($getDuplicateInDefaultSize);
	
	if(!empty($result)) {
		foreach ($result as $singletextlimitation) {
			$singleid = $singletextlimitation['id'];
			$singlefontid = $singletextlimitation['font_id'];
			func_array2update("mk_text_font_detail", array("size_default" => "N")," orientation_id='$orientationid' and font_id='$singlefontid'");
			func_array2update("mk_text_font_detail", array("size_default" => "Y")," orientation_id='$orientationid' and id='$singleid'");
		}
	}

	$setDefaultSizeForUnsetFonts = "select * from mk_text_font_detail where orientation_id='$orientationid' " .
      			" and font_id not in (select font_id from mk_text_font_detail where size_default='Y' and orientation_id='$orientationid') " .
				" group by font_id";
      	
	$result = func_query($setDefaultSizeForUnsetFonts);
	if(!empty($result)) {
		foreach ($result as $singletextlimitation) {
			$singleid = $singletextlimitation['id'];
			func_array2update("mk_text_font_detail", array("size_default" => "Y")," orientation_id='$orientationid' and id='$singleid'");
		}
	}
}


/** Function to get customization detail */
function getCustmizationDetail($data) {
	global  $sql_tbl;
	$SQL = 	"SELECT $sql_tbl[mk_style_customization_area].name, $sql_tbl[mk_style_customization_area].id, " .
			"$sql_tbl[mk_style_customization_area].label, $sql_tbl[mk_style_customization_area].image_t, " .
			"$sql_tbl[mk_style_customization_area].image_l, $sql_tbl[mk_style_customization_area].isprimary, ". 
			"$sql_tbl[mk_style_customization_area].description, $sql_tbl[mk_style_customization_area].bkgroundimage, " .
			"$sql_tbl[mk_style_customization_area].image_z, $sql_tbl[mk_style_customization_area].customizationcharge " .
			"FROM $sql_tbl[mk_style_customization_area] where $sql_tbl[mk_style_customization_area].id = $data " .
			"order by $sql_tbl[mk_style_customization_area].name ";
	
	$customization = func_query($SQL);
	return $customization; 
}

/** Function to get orientation detail */
function getOrientationDetail($data) {
	global  $sql_tbl;
	$SQL = 	"SELECT $sql_tbl[mk_customization_orientation].id, $sql_tbl[mk_customization_orientation].location_name, " .
			"$sql_tbl[mk_customization_orientation].start_x, $sql_tbl[mk_customization_orientation].start_y, " .
			"$sql_tbl[mk_customization_orientation].size_x, $sql_tbl[mk_customization_orientation].size_y, " .
			"$sql_tbl[mk_customization_orientation].area_id, $sql_tbl[mk_customization_orientation].ort_default, " .
			"$sql_tbl[mk_customization_orientation].orientation_order,$sql_tbl[mk_customization_orientation].width_in, " .
			"$sql_tbl[mk_customization_orientation].height_in, bgstart_x, bgstart_y, bgsize_x, bgsize_y, bgheight_in, bgwidth_in " .
			"from $sql_tbl[mk_customization_orientation], $sql_tbl[mk_style_customization_area] " .
			"where $sql_tbl[mk_customization_orientation].area_id=$sql_tbl[mk_style_customization_area].id " .
			"AND $sql_tbl[mk_customization_orientation].area_id=$data order by $sql_tbl[mk_customization_orientation].location_name";
	
	$orientation = func_query($SQL);
	return $orientation;
}

/** Function to get ops template size details -- AS OF NOW, not used */
function getOpsTemplateDetail($data) {
	global  $sql_tbl;
	//$SQL = "select size_label,width,dist from mk_style_customization_ops_template where area_id=$data";
	//$opstemplate = func_query($SQL);
	return $opstemplate;
}

function getOrientation($ortid) {
     global  $sql_tbl;
	 $SQL = "SELECT $sql_tbl[mk_customization_orientation].id, $sql_tbl[mk_customization_orientation].location_name, " . 
 			"$sql_tbl[mk_customization_orientation].start_x, $sql_tbl[mk_customization_orientation].start_y , " .
 			"$sql_tbl[mk_customization_orientation].size_x,	$sql_tbl[mk_customization_orientation].size_y, ".
 			"$sql_tbl[mk_customization_orientation].area_id,	$sql_tbl[mk_customization_orientation].ort_default, " .
 			"$sql_tbl[mk_customization_orientation].orientation_order,	$sql_tbl[mk_customization_orientation].width_in, " .
 			"$sql_tbl[mk_customization_orientation].height_in,bgstart_x,bgstart_y,bgsize_x,bgsize_y,bgheight_in,bgwidth_in, " .
 			"$sql_tbl[mk_customization_orientation].action,$sql_tbl[mk_customization_orientation].typeid, " .
 			"$sql_tbl[mk_customization_orientation].imageAllowed,	$sql_tbl[mk_customization_orientation].imageCustAllowed, " .
 			"$sql_tbl[mk_customization_orientation].textAllowed, 	$sql_tbl[mk_customization_orientation].textCustAllowed, " .
 			"$sql_tbl[mk_customization_orientation].textLimitation,	$sql_tbl[mk_customization_orientation].textLimitationTpl, " .
 			"$sql_tbl[mk_customization_orientation].textFontColors,	$sql_tbl[mk_customization_orientation].textColorDefault, " .
	 		"$sql_tbl[mk_customization_orientation].pathForImagesToReplaceText " .
			"from $sql_tbl[mk_customization_orientation] where $sql_tbl[mk_customization_orientation].id=$ortid " . 
			"order by $sql_tbl[mk_customization_orientation].location_name ";

	$orientation = func_query($SQL);
	
	if(!empty($orientation)) {
		foreach ($orientation as $key=>$singleOrientation) {
			// Font colors is a comma separated value stored in db. We convert this into an array format.
			if(!empty($singleOrientation['textFontColors']) && $singleOrientation['textFontColors']!='ALL') {
				$singleOrientation['textFontColors'] = explode(",", $singleOrientation['textFontColors']);
				$orientation[$key] = $singleOrientation;
			}
		}
	}
	
	return $orientation;
}

 
function getLimitedTextOrientationData($ortid) {
	if(empty($ortid)) { 
		return null;
	}
		
	$SQL = "select id, orientation_id, font_id, font_size, maxchars, font_default, size_default, chartype, charcase from mk_text_font_detail " .
		   "where orientation_id='$ortid' order by font_id, font_size, font_default" ;

	$limitedTextFontDetails = func_query($SQL);
	return $limitedTextFontDetails;
}

if ($REQUEST_METHOD == "POST") { 
   
	/* update customization area detail */
	if($mode == "updcust") {
		$query_data = array("name" => $area_name, "label" => $area_label, 
							"isprimary" => (!empty($primary) ? 1 : 0 ), "description" => $desc,
							"customizationcharge" => $customizationareaprice		
						);
		func_array2update("mk_style_customization_area", $query_data, " id=$custom_id ");
		$custmization = getCustmizationDetail($data);
		$top_message["content"] = func_get_langvar_by_name("lbl_adm_customization_upd");
		//  func_header_location("productstyle_action.php?mode=view&styleid=$style");
		
	} else if($mode == "updopstmpl") {
		$query_data = array("width" => $width, "dist" => $dist );
		func_array2update("mk_style_customization_ops_template", $query_data, " area_id=$custom_id and style_id=$styleid and size_label='$size_label'");
		$custmization = getCustmizationDetail($data);
		$opstemplate=  getOpsTemplateDetail($data);
		$top_message["content"] = func_get_langvar_by_name("lbl_adm_customization_upd");
		//  func_header_location("productstyle_action.php?mode=view&styleid=$style");

	} else if($mode == "savecust") {
		/* Insert new Customization Area Details */
		func_array2insert("mk_style_customization_area", 
				array(	"style_id" => $styleid,
						"name" => $area_name,
						"label" => $area_label,
						"description" => $desc,
						"isprimary" => (!empty($primary) ? true : false ),
						"default_image" => DETIMAGE,
						"image_t" => THMIMAGE,
						"image_l"  => DETIMAGE,
						"bkgroundimage" => PRWIMAGE,
						"image_z" => ZOOMIMAGE
					)
		);
		
		$top_message["content"] = func_get_langvar_by_name("lbl_adm_customization_upd");
		$new_id  = db_insert_id();
		$data = $new_id;
		$custmization = getCustmizationDetail($data);
		//func_header_location("productstyle_action.php?mode=view&styleid=$style");

	} else if($mode == "addorient") {
		/* Add Orientation Detail */
		func_array2insert("mk_customization_orientation", 
			array( 	location_name => $ort_name,
					start_x => (!empty($startx) ? $startx : 0 ),
					start_y => (!empty($starty) ? $starty : 0 ),
					size_x => (!empty($sizex) ? $sizex : 0 ),
					size_y => (!empty($sizey) ? $sizey : 0 ),
					"height_in" => (!empty($height_in) ? $height_in : 0 ),
					"width_in" => (!empty($width_in) ? $width_in : 0 ),
					area_id => $custom_id,
					ort_default => $default,
					"bgstart_x" => $bgstartx,
					"bgstart_y" => $bgstarty,
					"bgsize_x"=> $bgsizex,
					"bgsize_y"=> $bgsizey,
					"bgheight_in"=> $bgheight_in,
					"bgwidth_in"=> $bgwidth_in,
					"action"=> $action,
					"typeid"=> $typeid,
					"imageAllowed"=> (!empty($imageAllowed) ? $imageAllowed : 0 ),
					"imageCustAllowed"=> (!empty($imageCustAllowed) ? $imageCustAllowed : 0 ),
					"textAllowed"=> (!empty($textAllowed) ? $textAllowed : 0 ),
					"textCustAllowed"=> (!empty($textCustAllowed) ? $textCustAllowed : 0 ),
					"textLimitation"=> (!empty($textLimitation) ? $textLimitation : 0 ),
					"textLimitationTpl"=> (!empty($textLimitationTpl) ? $textLimitationTpl : 0 ),
					"textFontColors"=> implode(",",($textFontColors)),
					"textColorDefault"=> (!empty($textColorDefault) ? $textColorDefault : 0 )
				)
		);
			
		$top_message["content"] = func_get_langvar_by_name("msg_adm_orientation_upd");
		$custmization = getCustmizationDetail($data);
           
	} else if($mode == "updort") {
		/* Update Orientation Detail */
		if (is_array($posted_data)) {
			foreach ($posted_data as $orientationid=>$v) {
				if (!empty($v["to_delete"])) {
					$query_data = array	( 	"orientation_order" => 	$v["display_order"],
											"ort_default" => (!empty($v["default"]) ? 'Y' : 'N' )
										);
					func_array2update("mk_customization_orientation", $query_data, "id=$orientationid ");
				}
			}

			$top_message["content"] = func_get_langvar_by_name("msg_adm_orientation_upd");
			$custmization = getCustmizationDetail($data);
		}
		
	} else if($mode == "delort") {
		/* delete existing  orientation detail */
		if (is_array($posted_data)) {
			foreach ($posted_data as $orientationid=>$v) {
				if (empty($v["to_delete"])) {
					continue;
				}
				
				db_query ("DELETE FROM $sql_tbl[mk_customization_orientation] WHERE id='$orientationid'");
			}
			
		}
		$top_message["content"] = func_get_langvar_by_name("msg_adm_featproducts_del");  	 
		$custmization = getCustmizationDetail($data);
		
	} else if($mode == "modifyort") {
		
		$modify_orient = "yes";
		if (is_array($posted_data)) {
			foreach ($posted_data as $orientationid=>$v) {
				if (!empty($v["to_delete"])) {
					$orientation12 = getOrientation($orientationid);
					$limitTextFontDetails12 = getLimitedTextOrientationData($orientationid);
				}
			}
		}
		
		$custmization = getCustmizationDetail($data);

	} else if($mode == "addtextlimitation") {
		// Adding a new row in mk_text_font_detail table. This is to add any more specific text customization data.
		if(!empty($ortid) && !empty($textlimitationfont_new) && !empty($textlimitationfontsize_new) 
			&& !empty($textlimitationmaxchar_new)) {

			$count = func_query_first_cell("select count(*) as count from mk_text_font_detail where orientation_id=$ortid");
			// Note: In case this is the first new limited text customization, we set its font_default=Y and size_default=Y
			
			func_array2insert("mk_text_font_detail",
				array(	"orientation_id" 	=> "$ortid",
						"font_id" 	=> "$textlimitationfont_new",
						"font_size"	=> "$textlimitationfontsize_new",
						"maxchars" 	=> "$textlimitationmaxchar_new",
						"font_default"	=> (!empty($count) ? "N" : "Y" ),
						"size_default"	=> (!empty($count) ? "N" : "Y" ),
						"chartype" 	=> !(empty($textlimitationchartype_new) ? "$textlimitationchartype_new" : "all"),
						"charcase" 	=> !(empty($textlimitationcharcase_new) ? "$textlimitationcharcase_new" : "all"),
				)
			);
			
			// We need to check if default font/size settings are proper on this. 
			cleanUpLimitedTextCustomizationDetails($ortid);
		}
		
	} else if($mode == "deletetextlimitation") {
		if(!empty($delete_row_id)) {
			
			db_query ("delete from mk_text_font_detail where id=$delete_row_id and font_default!='Y'");
			
			if(!empty($ortid)) {
				cleanUpLimitedTextCustomizationDetails($ortid);
			}
		}
		
	} else if($mode == "updateort") {
		
		$query_data_1 = array(	"location_name" => 	$loc_name,
								"start_x" => (!empty($startx) ? $startx : 0 ),
								"start_y" => (!empty($starty) ? $starty : 0 ),
								"size_x" => (!empty($sizex) ? $sizex : 0 ),
								"size_y" => (!empty($sizey) ? $sizey : 0 ),
								"height_in" => (!empty($height_in) ? $height_in : 0 ),
					    		"width_in" => (!empty($width_in) ? $width_in : 0 ),
					    		"bgstart_x" => $bgstartx,
								"bgstart_y" => $bgstarty,
								"bgsize_x"=> $bgsizex,
								"bgsize_y"=> $bgsizey,
								"bgheight_in"=> $bgheight_in,
								"bgwidth_in"=> $bgwidth_in,
								"action"=> $action,
								"typeid"=> $typeid,
	                            "imageAllowed"=> (!empty($imageAllowed) ? $imageAllowed : 0 ),
								"imageCustAllowed"=> (!empty($imageCustAllowed) ? $imageCustAllowed : 0 ),
								"textAllowed"=> (!empty($textAllowed) ? $textAllowed : 0 ),
								"textCustAllowed"=> (!empty($textCustAllowed) ? $textCustAllowed : 0 ),
								"textLimitation"=> (!empty($textLimitation) ? $textLimitation : 0 ),
								"textLimitationTpl"=> (!empty($textLimitationTpl) ? $textLimitationTpl : 0 ),
								"textFontColors"=> implode(",",($textFontColors)),
								"textColorDefault"=> (!empty($textColorDefault) ? $textColorDefault : 0 ), 
								"pathForImagesToReplaceText" => (!empty($pathForImagesToReplaceText) ? $pathForImagesToReplaceText : "" )
							);
							
		func_array2update("mk_customization_orientation",$query_data_1," id='$ortid'");
		
		if(!empty($defaultfont)) {
	   		func_array2update("mk_text_font_detail", array("font_default" => "N")," orientation_id='$ortid'");
	   		func_array2update("mk_text_font_detail", array("font_default" => "Y")," orientation_id='$ortid' and id=$defaultfont");
	   	}
		   	
		$textlimitationdefaultfontsize = implode(",", $textlimitationdefaultfontsize);
	   	if(!empty($textlimitationdefaultfontsize)) {
	   		func_array2update("mk_text_font_detail", array("size_default" => "N")," orientation_id='$ortid'");
	   		func_array2update("mk_text_font_detail", array("size_default" => "Y"),
	   							" orientation_id='$ortid' and id in ($textlimitationdefaultfontsize)");
	   	}
	   	
	   	if(!empty($textlimitationfont)) {
	   		foreach ($textlimitationfont as $key=>$value) {
	   			func_array2update("mk_text_font_detail", array("font_id" => "$value")," id='$key'");
	   		}
	   	}
		   	
      		
		if(!empty($textlimitationmaxchar) && !empty($textlimitationfontsize)) {
			foreach($textlimitationmaxchar as $key=>$singlefontmaxchar) {
				$singleid = $key;
				$singlefontSize = $textlimitationfontsize[$key];
		   			
	   			$parameters = array("maxchars"=>"$singlefontmaxchar", "font_size"=>"$singlefontSize");
	   			func_array2update("mk_text_font_detail", $parameters," orientation_id='$ortid' and id='$singleid'");
	   		}
		}
		
		if(!empty($textlimitchartype) && !empty($textlimitcharcase)) {
			foreach($textlimitchartype as $key=>$singlefontchartype) {
				$singleid = $key;
				$charcase = $textlimitcharcase[$key];
				$parameters = array("chartype"=>"$singlefontchartype", "charcase"=>"$charcase");
				func_array2update("mk_text_font_detail", $parameters," orientation_id='$ortid' and id='$singleid'");
			}
		}
		
		cleanUpLimitedTextCustomizationDetails($ortid);
		
		$custmization = getCustmizationDetail($data); 
		$template = "prd_style_custumization";
	}
} 


if($mode == "addcustarea") {
	$template = "prd_style_custumization";

} else if($mode == "modifyarea") {
	 $custmization = getCustmizationDetail($data); 
	 $template = "prd_style_custumization";
}

if(!empty($data)) {
	$orientation = getOrientationDetail($data);
	$opstemplate=  getOpsTemplateDetail($data);
}

$customization_types_array = func_query("select typeid,name from customization_type");
$customization_types = array();

foreach($customization_types_array as $key=>$value) {
	$customization_types[$value[typeid]] = $value[name];
}

$font_results=func_query("select * from mk_text_font_color");
$smarty->assign("font_results", $font_results);

$fonts=func_query("select * from mk_fonts order by name");
$smarty->assign("fonts",$fonts);

$smarty->assign("customization_types",$customization_types);
$smarty->assign("styleid",$style);
$smarty->assign("custom_id",$data);
$smarty->assign("mode", $mode);
$smarty->assign("custmization", $custmization);
$smarty->assign("orientation", $orientation);
$smarty->assign("opstemplate", $opstemplate);
$smarty->assign("orient", $orientation12);
$smarty->assign("limitedTextFontDetails", $limitTextFontDetails12);

$smarty->assign ("modify_ort", $modify_orient);
$smarty->assign("main",$template);

x_session_save();

# Assign the current location line
$smarty->assign("location", $location);
$smarty->assign("dialog_tools_data", $dialog_tools_data);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>