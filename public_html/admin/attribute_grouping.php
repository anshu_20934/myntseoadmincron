<?php
use style\dao\StyleGroupDBAdapter;
use style\StyleGroupManager;

require "./auth.php";
require $xcart_dir."/include/security.php";
if($_GET['mode']==='get_group_types'){
	echo json_encode(array('searchResults'=>array(array('group_type'=>'color'))));
	exit;
}
if($_GET['mode']==='search'){
	$adapter = new StyleGroupDBAdapter();
	$sort = mysql_real_escape_string($sort);
	$dir = mysql_real_escape_string($dir);
	if($query_type ==='pattern_name'){
		$styleGroups = $adapter->getAllStyleGroups("pattern_name like '".mysql_real_escape_string($query_str)."%'","order by $sort $dir",$start,$limit);
		$count = $adapter->getAllStyleGroupsCount("pattern_name like '".mysql_real_escape_string($query_str)."%'");
	}else if($query_type ==='group_type'){
		$styleGroups = $adapter->getAllStyleGroups("group_type ='".mysql_real_escape_string($query_str)."'","order by $sort $dir",$start,$limit);
		$count = $adapter->getAllStyleGroupsCount("group_type ='".mysql_real_escape_string($query_str)."'");
	}else if($query_type ==='style'){
		$styleGroups = $adapter->getStyleGroupsForSingleStyle($query_str,false,"order by $sort $dir");
		$count = count($styleGroups);
	}
	$styleGroupArray=array();
	foreach ($styleGroups as $styleGroup){
		$styleGroupArray[]= array('pattern_name'=>$styleGroup->getPatternName(),'group_type'=>$styleGroup->getGroupType(),'group_id'=>$styleGroup->getGroupId(),'styles'=>$styleGroup->getStyleIdsArray());
	}
	echo json_encode(array('searchResults'=>$styleGroupArray,'count'=>"$count"));
	exit;
}
if(isset($_GET['mode']) && $_GET['mode'] =='get_matching_styles'){
	require_once($xcart_dir."/include/func/func.utilities.php");
	require_once($xcart_dir."/include/class/search/SearchProducts.php");
	
	$linkStyleIdsArrayTemp=explode(',',$_GET['linkStyleids']);
	$linkStyleIdsArray=array();
	foreach ($linkStyleIdsArrayTemp as $entry){
		if(!empty($entry)){
			$linkStyleIdsArray[]=$entry;
		}
	}
	$vendorArticleNum= filter_input(INPUT_GET,'vendorarticle',FILTER_SANITIZE_STRING);
	$sql='';
	if(!empty($linkStyleIdsArray)){
		$sql="select style_id,article_number from mk_style_properties where style_id in (".implode(',',$linkStyleIdsArray).") union";
	}
	$sql.=" select style_id,article_number from mk_style_properties where article_number like '%$vendorArticleNum%'  limit 20 ";
	// %vendorArticleNum% query is expensive but we don't have any option
	
	$result_set=func_query($sql,true);
	$articleNum=array();
	foreach ($result_set as $row){
			$matching_style_ids[]=$row['style_id'];
			$articleNum[$row['style_id']]=$row['article_number'];
	}
	$getStatsInstance=false;
	$ajaxSearchProducts = new SearchProducts(null,null,$getStatsInstance);
	$styleSolrData=$ajaxSearchProducts->getMultipleStyleFormattedSolrData($matching_style_ids);
	$ajaxSearchProducts->products=$styleSolrData;
	$ajaxSearchProducts->fixImagePaths($ajaxSearchProducts->products, "styles");
	$products=$ajaxSearchProducts->products;
	foreach ($products as $product){
		$array = array('image_url'=>$product['searchimagepath'],'style_id'=>$product['styleid'],'name'=>$product['product'],'article_number'=>$articleNum[$product['styleid']]);
		if(in_array($product['styleid'],$linkStyleIdsArray)){
			$array['checked']=1;
		}else{
			$array['checked']=0;
		}
		$result[]=$array;
	}
	
	echo json_encode(array('searchResults'=>$result,'count'=>"30"));
	exit;
}if(isset($_GET['mode']) && $_GET['mode'] =='save_matching_styles'){
	
	$linkStyleIds = filter_input(INPUT_POST,'linkStyleids',FILTER_SANITIZE_STRING);
	$group_type = filter_input(INPUT_POST,'group_type',FILTER_SANITIZE_STRING);
	$pattern_name= filter_input(INPUT_POST,'pattern_name',FILTER_SANITIZE_STRING);
	$group_id= filter_input(INPUT_POST,'group_id',FILTER_SANITIZE_NUMBER_INT);
	$linkStyleIdsArrayTemp=explode(',',$linkStyleIds);
	$linkStyleIdsArray=array();
	foreach ($linkStyleIdsArrayTemp as $entry){
		if(!empty($entry) ){
			$linkStyleIdsArray[]=$entry;
		}
	}
	$linkStyleIdsArray=array_unique($linkStyleIdsArray);
	$result_set = func_query("select style_id from mk_style_properties where style_id in ($linkStyleIds)", true);
	if(count($result_set)!=count($linkStyleIdsArray)){
		$result['status']='F';
		$temp=array();
		$temp2=array();
		foreach ($result_set as $row){
			$temp[$row['style_id']]=$row['style_id'];
		}
		foreach ($linkStyleIdsArray as $gstyleid){
			if(empty($temp[$gstyleid])){
				$temp2[]=$gstyleid;
			}
		}
		$result["msg"]="Failed: styles not found[".implode(',',$temp2)."]";
		echo json_encode($result);
		exit;
	}


	$styleManager = new StyleGroupManager();
	try{
		$styleManager->linkStyles($linkStyleIdsArray, $group_type, $pattern_name,$group_id);
		$result['result']='success';
		$result["msg"]="Successfully Linked";
	}catch(Exception $e){
		$result['result']='failed';
		$result["msg"]=$e->getMessage();
	}
	
	
	echo json_encode($result);
	exit;
}

$smarty->assign("main","attribute_groupings");
func_display("admin/home.tpl",$smarty);
