<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
require $xcart_dir."/include/func/func.returns.php";
require_once $xcart_dir."/PHPExcel/PHPExcel.php";
require_once $xcart_dir.'/include/class/excel_reader2.php';
require_once $xcart_dir.'/include/func/func.mk_shipment_tracking.php';

$action  = $_POST['action'];
$uploadStatus = false;
if($action == '')
    $action ='upload';
    
$weblog->info("Returns Bulk Uploader : action = ". $action);

if ($REQUEST_METHOD == "POST" && $action=='verify') {
	if(!isset($_FILES) && isset($HTTP_POST_FILES))
        $_FILES = $HTTP_POST_FILES;

    if(!empty($_FILES['returnsdetail']['name'])) {
      	$uploaddir1 = '../bulkorderupdates/';
        $extension = explode(".",basename($_FILES['returnsdetail']['name']));
        $uploadfile1 = $uploaddir1 . "pickup-report-".date('d-m-Y-h-i-s').".".$extension[1];
        if (move_uploaded_file($_FILES['returnsdetail']['tmp_name'], $uploadfile1)) {
          	$uploadStatus = true;
        }
    }

    if($uploadStatus)
    {
    	$step = $_POST['step'];
    	$weblog->info(" Step To Update = $step");
    	if($step == '0') {
    		$errormsg = 'Please select status to update for Returns';
        	$action = "upload";
    	} elseif($step == '1') {
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
			$weblog->info("Object Created");
            $objPHPExcel = $objReader->load($uploadfile1);
            $weblog->info("file read ");
 	    	$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
 	    	$weblog->info("Total no Of Rows - $lastRow");
 	    	
    		$returnIdCol = '';
 	    	foreach (range('A', 'Z') as $alphabet) {
 	    		$cellObj = ($objPHPExcel->getActiveSheet()->getCell($alphabet.'1'));
 	    		if(trim($cellObj->getvalue()) == 'Return Number') {
 	    			$returnIdCol = $alphabet;
 	    		}
 	    	}
 	    	$errormsg = "";
			if($returnIdCol != '') {
 	    		$allReturnIds = array();
	        	for($i=2; $i<=$lastRow; $i++){//start from second row ignore heading
	   	    		$cell1Obj = ($objPHPExcel->getActiveSheet()->getCell($returnIdCol.$i));
	               	$returnid = trim($cell1Obj->getvalue());
	               	$weblog->info("Read return id - $returnid");
				
	            	$sql = "SELECT * from $sql_tbl[returns] where returnid = $returnid";
		        	$result = func_query_first($sql);
		        	if($result) {
						$weblog->info("Read return request");
						
			        	if($result['return_mode'] == 'self') {
			        		$errormsg .= "Return Number $returnid is a self delivery return. Please remove it from the file and upload again.";
		                	continue;
			        	}
		        	
						if($result['status'] != 'RRQ') {
							$errormsg .= "Return Number $returnid is not in Queued state. Only returns in Queued state can be moved to Awaiting Tracking state. Please upload correct file.";
							continue;
						}
						$allReturnIds[] = $returnid;
		        	} else {
		        		$errormsg .= "Return Number $returnid is not valid. Please upload correct file.";
		        	}
	        	}
	        	if($errormsg == "") {
	        		$count = 0;
	        		foreach ($allReturnIds as $returnid) {
	        			$sql = "SELECT * from $sql_tbl[returns] where returnid = $returnid";
		        		$result = func_query_first($sql);
	        			$new_status = 'RAT';
			        	$return_query_data = array("status" => $new_status, 'sentfortrackingdate'=>time());
						func_array2update("returns", $return_query_data, "returnid=".$returnid);
						func_array2update("order_details", array('item_status'=>$new_status), "itemid=".$result['itemid']);
						$weblog->info("Updated Data");
						add_returns_logs_status_change($result['returnid'], $new_status, $_POST['login'], array());
						$count++;
	        		}
	        		$msg = "$count Returns successfully updated with status to Return Awaiting Tracking.";
	        	}
			} else {
				$errormsg .= "Return Number column is missing from the file. Please upload correct file.";
			}
        	$action = "upload";
		} else if($step == '2'){
            $returnListDisplay =  array();
            $objReader = PHPExcel_IOFactory::createReader('Excel5');
            $objPHPExcel = $objReader->load($uploadfile1);
 	    	$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
        	
 	    	$returnIdCol = '';
 	    	$courierCol = '';
 	    	$trackingNoCol = '';
 	    	foreach (range('A', 'Z') as $alphabet) {
 	    		$cellObj = ($objPHPExcel->getActiveSheet()->getCell($alphabet.'1'));
 	    		if(trim($cellObj->getvalue()) == 'Return Number') {
 	    			$returnIdCol = $alphabet;
 	    		}
 	    		if(trim($cellObj->getvalue()) == 'Tracking No') {
 	    			$trackingNoCol = $alphabet;
 	    		}
 	    		if(trim($cellObj->getvalue()) == 'Courier') {
 	    			$courierCol = $alphabet;
 	    		}
 	    	}
 	    	
 	    	if($returnIdCol != '' && $trackingNoCol != '' && $courierCol != '') {
	 	    	$returnIdsVisited = array();
	 	    	$trackingNosVisited = array();
	 	    	
	 	    	for($i=2; $i<=$lastRow; $i++){//start from second row ignore heading
	   	    		$cell1Obj = ($objPHPExcel->getActiveSheet()->getCell($returnIdCol.$i));
	                $returnid = trim($cell1Obj->getvalue());
	                
	                $cell1Obj = ($objPHPExcel->getActiveSheet()->getCell($courierCol.$i));
	                $courier = trim($cell1Obj->getvalue());
	                
	                if(empty($returnid))
	            		continue;
	                
	                if($returnIdsVisited[$returnid]){
	                	$errormsg = "Some of the rows have duplicate Return Numbers. Please upload correct file.";
	                	$action = "upload";
	                	break;
	                }
	                $returnIdsVisited[$returnid] = true;
	                
	                $sql = "SELECT * from $sql_tbl[returns] where returnid = $returnid";
		        	$result = func_query_first($sql);
	                
		        	if(!$result) {
		        		$errormsg = "Return Number $returnid is invalid. Please upload correct file.";
	                	$action = "upload";
	                	break;
		        	}
		        	
	 	    		if($result['return_mode'] == 'self') {
		        		$errormsg = "Return Number $returnid is a self delivery return. Please remove it from the file and upload again.";
	                	$action = "upload";
	                	break;
		        	}
		        	
	 	    		if( $courier != 'ML' && $result['status'] != 'RAT') {
		        		$errormsg = "Return Number $returnid is not in Awaiting Tracking state. Please upload correct returns.";
	                	$action = "upload";
	                	break;
		        	}
		        	if($courier == 'ML' && $result['status'] != 'RRQ') {
		        		$errormsg = "Return Number $returnid is not in Queued state. Please upload correct returns.";
	                	$action = "upload";
	                	break;
		        	}
		        	
	                $cell1Obj = ($objPHPExcel->getActiveSheet()->getCell($trackingNoCol.$i));
	                $tracking_no = trim($cell1Obj->getvalue());
	                
	                if($courier == 'ML' && empty($tracking_no)) {
	                	$tracking_no = 'ML';
	                }
	                
	                if(empty($tracking_no)) {
	                	$errormsg = "Some of the rows do not have tracking no. Please upload correct file.";
	                	$action = "upload";
	                	break;
	                }
	                if($courier != 'ML' && !is_numeric($tracking_no)) {
	                	$errormsg = "Some of the rows have non numeric tracking nos. Please upload correct file.";
	                	$action = "upload";
	                	break;
	                }
	                
	 	    		if($trackingNosVisited[$tracking_no]){
	                	$errormsg = "Some of the rows have duplicate tracking nos. Please upload correct file.";
	                	$action = "upload";
	                	break;
	                }
	                $trackingNosVisited[$tracking_no] = true;
	                 
	                $returnListDisplay[] = array('returnid'=>$returnid, 'tracking'=>$tracking_no, 'courier'=>get_courier_display_name($courier));
	        	}
 	    	} else {
 	    		$errormsg = "Some of the columns from Return Number, Tracking No, Courier are missing.";
	            $action = "upload";
 	    	}
            $smarty->assign('returnListDisplay',$returnListDisplay);
            $smarty->assign('uploadreturnsFile',$uploadfile1);
            $smarty->assign('status','pickup');
		}else if($step == '3'){
			$returnListDisplay =  array();
            $objReader = PHPExcel_IOFactory::createReader('Excel5');
            $objPHPExcel = $objReader->load($uploadfile1);
 	    	$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
        	
 	    	$returnIdCol = '';
 	    	$courierCol = '';
 	    	$trackingNoCol = '';
 	    	foreach (range('A', 'Z') as $alphabet) {
 	    		$cellObj = ($objPHPExcel->getActiveSheet()->getCell($alphabet.'1'));
 	    		if(trim($cellObj->getvalue()) == 'Return Number') {
 	    			$returnIdCol = $alphabet;
 	    		}
 	    		if(trim($cellObj->getvalue()) == 'Tracking No') {
 	    			$trackingNoCol = $alphabet;
 	    		}
 	    		if(trim($cellObj->getvalue()) == 'Courier') {
 	    			$courierCol = $alphabet;
 	    		}
 	    	}
 	    	
 	    	if($returnIdCol != '' && $trackingNoCol != '' && $courierCol != '') {
	 	    	$returnIdsVisited = array();
	 	    	$trackingNosVisited = array();
	 	    	
	 	    	for($i=2; $i<=$lastRow; $i++){//start from second row ignore heading
	   	    		$cell1Obj = ($objPHPExcel->getActiveSheet()->getCell($returnIdCol.$i));
	                $returnid = trim($cell1Obj->getvalue());
	                
	                $cell1Obj = ($objPHPExcel->getActiveSheet()->getCell($courierCol.$i));
	                $courier = trim($cell1Obj->getvalue());
	                
	                if(empty($returnid))
	            		continue;
	                
	                if($returnIdsVisited[$returnid]){
	                	$errormsg = "Some of the rows have duplicate Return Numbers. Please upload correct file.";
	                	$action = "upload";
	                	break;
	                }
	                $returnIdsVisited[$returnid] = true;
	                
	                $sql = "SELECT * from $sql_tbl[returns] where returnid = $returnid";
		        	$result = func_query_first($sql);
	                
		        	if(!$result) {
		        		$errormsg = "Return Number $returnid is invalid. Please upload correct file.";
	                	$action = "upload";
	                	break;
		        	}
		        	
		        	if($result['status'] != 'RQF') {
		        		$errormsg = "Return Number $returnid is not in QA Failed state. Please upload correct returns.";
	                	$action = "upload";
	                	break;
		        	}
		        	
	                $cell1Obj = ($objPHPExcel->getActiveSheet()->getCell($trackingNoCol.$i));
	                $tracking_no = trim($cell1Obj->getvalue());
	                
	                if($courier == 'ML' && empty($tracking_no)) {
	                	$tracking_no = 'ML';
	                }
	                
	                if(empty($tracking_no)) {
	                	$errormsg = "Some of the rows do not have tracking no. Please upload correct file.";
	                	$action = "upload";
	                	break;
	                }
	                if($courier != 'ML' && !is_numeric($tracking_no)) {
	                	$errormsg = "Some of the rows have non numeric tracking nos. Please upload correct file.";
	                	$action = "upload";
	                	break;
	                }
	                
	 	    		if($trackingNosVisited[$tracking_no]){
	                	$errormsg = "Some of the rows have duplicate tracking nos. Please upload correct file.";
	                	$action = "upload";
	                	break;
	                }
	                $trackingNosVisited[$tracking_no] = true;
	                 
	                $returnListDisplay[] = array('returnid'=>$returnid, 'tracking'=>$tracking_no, 'courier'=>get_courier_display_name($courier));
	        	}
 	    	} else {
 	    		$errormsg = "Some of the columns from Return Number, Tracking No, Courier are missing.";
	            $action = "upload";
 	    	}
            $smarty->assign('returnListDisplay',$returnListDisplay);
            $smarty->assign('uploadreturnsFile',$uploadfile1);
            $smarty->assign('status','reship');
		}
    } else {
    	$action = "upload";
        $errormsg =  "File not uploaded, try again ..";
    }
    $smarty->assign('message',$msg);
    $smarty->assign('errormessage',$errormsg);
}

if ($REQUEST_METHOD == "POST" && $action=='confirmupload') {
	
	$filetoread  = $HTTP_POST_VARS['filetoread'];
	$updateto  = $HTTP_POST_VARS['status'];
    if(file_exists($filetoread))
    {
        $objReader = PHPExcel_IOFactory::createReader('Excel5');
        $objPHPExcel = $objReader->load($filetoread);
        
    	$returnIdCol = '';
 	    $courierCol = '';
 	    $trackingNoCol = '';
 	    foreach (range('A', 'Z') as $alphabet) {
 	    	$cellObj = ($objPHPExcel->getActiveSheet()->getCell($alphabet.'1'));
 	    	if(trim($cellObj->getvalue()) == 'Return Number') {
 	    		$returnIdCol = $alphabet;
 	    	}
 	    	if(trim($cellObj->getvalue()) == 'Tracking No') {
 	    		$trackingNoCol = $alphabet;
 	    	}
 	   	 	if(trim($cellObj->getvalue()) == 'Courier') {
 	    		$courierCol = $alphabet;
 	    	}
 	    }
        
 	    $count = 0;
 	   	$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
        for($i=2; $i<=$lastRow; $i++){//start from second row ignore heading
   	    	$cell1Obj = ($objPHPExcel->getActiveSheet()->getCell($returnIdCol.$i));
            $returnid = trim($cell1Obj->getvalue());
        	
            if(empty($returnid))
            	continue;
            
            $cell1Obj = ($objPHPExcel->getActiveSheet()->getCell($trackingNoCol.$i));
            $trackingno = trim($cell1Obj->getvalue());
	        
            $cell1Obj = ($objPHPExcel->getActiveSheet()->getCell($courierCol.$i));
            $courier = trim($cell1Obj->getvalue());
                
            if($courier == 'ML' && empty($trackingno)) {
               	$trackingno = 'ML';
            }
                
            $sql = "SELECT * from $sql_tbl[returns] where returnid = $returnid";
	        $result = func_query_first($sql);
	        if($result) {
	        	if($updateto == 'pickup') {
					$new_status = 'RPI';
					$return_query_data = array("tracking_no" => $trackingno, "status" => $new_status, 'pickupinitdate'=>time(), 'courier_service'=>$courier);
					func_array2update("returns", $return_query_data, "returnid=".$result['returnid']);
					func_array2update("order_details", array('item_status'=>$new_status), "itemid=".$result['itemid']);
	        		add_returns_logs_status_change($result['returnid'], $new_status, $_POST['login'], array("TRACKING_NO" => $trackingno, "COURIER_SERVICE"=> $courier));
	        		// start tracking this return for pickup
	        		//trackOrder($result['returnid'], $trackingno, $courier, 'r');
	        	} else if($updateto == 'reship') {
	        		$new_status = 'RRS';
	        		$currentime = time();
	        		$return_query_data = array("reship_tracking_no" => $trackingno, "status" => $new_status, 'reshippeddate'=>$currentime, 'reship_courier'=>$courier);
					func_array2update("returns", $return_query_data, "returnid=".$result['returnid']);
					func_array2update("order_details", array('item_status'=>$new_status), "itemid=".$result['itemid']);
					add_returns_logs_status_change($result['returnid'], $new_status, $_POST['login'], array("RESHIP_TRACKING" => $trackingno, "RESHIP_COURIER"=> $courier));
					// start tracking this return for reshipping
					//trackOrder($result['returnid'], $trackingno, $courier, 'rs');
	        	}
				$count++;
	        }
        }
        if($updateto == 'pickup') {
    		$msg =  "$count Return Requests are successfully moved to Pickup Initiated state.";
        } else {
        	$msg =  "$count Return Requests are successfully moved to Reshipped state.";
        }
    }
	$action = 'upload';
	$smarty->assign('message',$msg);
}

$smarty->assign('action',$action);
$smarty->assign("main","upload_bulk_returns");
func_display("admin/home.tpl",$smarty);

?>
