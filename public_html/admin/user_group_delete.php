<?php
require_once  "auth.php";
require_once $xcart_dir.'/modules/RestAPI/RestRequest.php';

if($XCART_SESSION_VARS['deleteGroup']){
	$smarty->assign('deleteGroupFlag','true');
	$XCART_SESSION_VARS['deleteGroup'] = false;		
}
else{
	$smarty->assign('deleteGroupFlag','false');
}

$url = HostConfig::$userGroupServiceUrl.'/';
$method = 'GET';
$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Content-type: application/json';
$headers[] = 'Accept-Language: en-US';
$request = new RestRequest($url, $method);
$request->setHttpHeaders($headers);
$request->execute();
$info = $request->getResponseInfo();
$body = $request->getResponseBody();
if($info['http_code'] == 200){
	$response = json_decode($body,true);
	if($response['groupResponse']['status']['statusType'] == 'SUCCESS'){
		if(count($response['groupResponse']['data']['UserGroup'][0])==0){
			$tempArr = array();
			array_push($tempArr, $response['groupResponse']['data']['UserGroup']);
			$smarty->assign('results',$tempArr);
		}
		else{
			$smarty->assign('results',$response['groupResponse']['data']['UserGroup']);						
		}
		$smarty->assign('resultCount',count($response['groupResponse']['data']['UserGroup']));		
	}
}
$smarty->assign('main','user_group_delete');
func_display("admin/home.tpl",$smarty);
?>