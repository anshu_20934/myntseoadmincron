<?php
@include_once "./auth.php";
require_once($xcart_dir."/include/func/func.utilities.php");
require_once($xcart_dir."/include/class/search/SearchProducts.php");

$query = $_POST["query"];
$page=$_POST["page"];
$noofitems=$_POST['noofitems'];

$cashback_gateway_status = FeatureGateKeyValuePairs::getFeatureGateValueForKey('cashbackenabled');
$cashback_to_display_search = FeatureGateKeyValuePairs::getBoolean('cashback.displayOnSearchPage',false);
if($cashback_gateway_status == "internal") {
	$pos = strpos($login, "@myntra.com");
	if($pos === false)
	$cashback_gateway_status = "off";
	else
	$cashback_gateway_status = "on";
}

$smarty->assign("cashback_gateway_status",$cashback_gateway_status);
$smarty->assign("cashback_to_display_search",$cashback_to_display_search);
$getStatsInstance=false;
$ajaxSearchProducts = new SearchProducts($query,null,$getStatsInstance);
if(is_numeric(trim($query))){
	$ajaxSearchProducts->getStyleByID(trim($query));
}else{
	$ajaxSearchProducts->limit=100;
	$ajaxSearchProducts->getStyleByName(trim($query));
	if(!($ajaxSearchProducts->products[0]['stylename']===trim($query))){
		$ajaxSearchProducts->execute_new();
	}
}

$ajaxSearchProducts->fixImagePaths($ajaxSearchProducts->products, $query);
$products=$ajaxSearchProducts->products;
//print "<pre>";
//print_r($products);
//exit;
$result=array();
if(empty($products)){
	$result['status']='F';
	$result["html"]="<div style='text-align: center;'>No Products Found</div>";
	$result["page_count"]=0;
	$result["current_page"]=0;
	$result["product_count"]=0;
	echo json_encode($result);
	exit;
}
$smarty->assign("products",$products);
$html = func_display("admin/ajax_product_search.tpl", $smarty, false);

$result['status']='S';
$result["html"]=$html;
$result["page_count"]=$ajaxSearchProducts->getTotalPages();
$result["product_count"]=$ajaxSearchProducts->products_count;
$result["current_page"]=($ajaxSearchProducts->offset);
echo json_encode($result);
//echo $html;

