<?php
require '../../auth.php';
//include_once("$xcart_dir/include/security.php");
include_once $xcart_dir.'/include/func/func.mk_shipment_tracking.php';

global $weblog;

$action = $_POST['action'];
$fromDate = null;
$toDate = null;

$weblog->info("Shipment Tracking Call for Action: " . $action);


$presetDate = $_POST['presetDate'];
$fromDateReq = $_POST['fromdate'];
$toDateReq = $_POST['todate'];
$weblog->info("PRE: " . $presetDate . " From: " . $fromDateReq . " To: " . $toDateReq);
try{
	if($presetDate == 'cs'){
		if($fromDateReq != null || $fromDateReq != ""){
			$fromDate = strtotime($fromDateReq);	
		}
		
		if($toDateReq != null || $toDateReq != ""){
			$toDate = strtotime("+1 day", strtotime($toDateReq));
		}
	}else{
		$now = time();
		$toDate = mktime(0, 0, 0, date("m", $now)  , date("d", $now), date("Y", $now));
		$fromDate =  strtotime(substr($presetDate,1)." day",$toDate);
	}
}catch(Exception $e){
	$weblog->info(e);
}
$weblog->info("From Date: " . date('m-d-y h:i:s a', $fromDate) . " To: " . date('m-d-y h:i:s a', $toDate));

if($action == 'loadDashboard'){
	
	$results = getTrackingDashboardReport($fromDate, $toDate);
	$retarr = array("rows" => $results);
	header('Content-type: text/x-json');
	print json_encode($retarr);
}

if($action == 'loadSLAReport'){
	
	$results = getShipmentTrackingSLAReport($fromDate, $toDate);
	$retarr = array("rows" => $results, "count" => '3');
	header('Content-type: text/x-json');
	print json_encode($retarr);
}

?>