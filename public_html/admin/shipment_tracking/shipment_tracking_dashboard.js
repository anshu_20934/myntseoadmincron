Ext.onReady(function() {
	Ext.QuickTips.init();
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
	Ext.Ajax.on('beforerequest', myMask.show, myMask);
	Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
	Ext.Ajax.on('requestexception', myMask.hide, myMask);

	 // example of custom renderer function
    function change(val){
    	return '<div style="background-color:#F9F9FB;font-weight:bold;font-size:12;height:25">' + val + '</div>';
    }
    
	var reader=	new Ext.data.JsonReader({
		root: "rows",
		totalProperty: 'count',
		idProperty:'detail_id',
		fields : ['desc','BD-on', 'BD-cod', 'AR-on', 'AR-cod', 'DH-on', 'DH-cod']
	});

	var dashboardStore = new Ext.data.Store({
		url: 'shipment_tracking_ajax.php',
		pruneModifiedRecords : true,
		reader: reader,
		remoteSort: false
	});    

	var grid = new Ext.grid.GridPanel({
		store: dashboardStore,
		columns: [
		          {header: "", width:170, dataIndex: 'desc', sortable: false, align:'left', renderer: change},
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>Blue Dart</br>(NON COD)</span>", width:100, dataIndex: 'BD-on', sortable: false, align:'center'},
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>Blue Dart</br>(COD)</span>", width:100, dataIndex: 'BD-cod', sortable: false, align:'center'},
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>Aramex</br>(NON COD)</span>", width:100, dataIndex: 'AR-on', sortable: false, align:'center'},
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>Aramex</br>(COD)</span>", width:100, dataIndex: 'AR-cod', sortable: false, align:'center'},
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>DHL</br>(NON COD)</span>", width:100, dataIndex: 'DH-on', sortable: false, align:'center'},
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>DHL</br>(COD)</span>", width:100, dataIndex: 'DH-cod', sortable: false, align:'center'}
		          ],
		          renderTo:'dashboardPanel',
		          width:770,
		          autoHeight:true,
		          listeners : {
		        	  'render' : function(comp) {
		        		  reloadDashboard();
		        	  }
		          }
	});

	function reloadDashboard() {
		var params = {};
		params["action"] = "loadDashboard";
		params["presetDate"] = 'd-1';
		if(dashboardFilterForm != null){
			var form = dashboardFilterForm.getForm();
			
			params['fromdate']	= form.findField('from_dt').getValue();
			params['todate']	= form.findField('to_dt').getValue();
			params["presetDate"] = form.findField('preset_dt').getValue();
		}
		dashboardStore.baseParams = params;
		dashboardStore.load();
	}

	var presetDTStore = new Ext.data.JsonStore({
		fields : [ 'code', 'label' ],
		data : [
		        {"code":"d-1", "label":"Yesterday"},
		        {"code":"d-7", "label":"Last 7 days"},
		        {"code":"d-15", "label":"Last 15 days"},
		        {"code":"d-30", "label":"Last 30 days"},
		        {"code":"cs", "label":"Custom"}
		        ]});

	var presetDT = {
			xtype : 'combo',
			value:'d-1',
			store : presetDTStore,
			mode: 'local',
			displayField : 'label',
			valueField : 'code',
			triggerAction: 'all',
			name : 'preset_dt',
			editable:false,
			listeners : {
	        	  'select' : function(comp) {
	        		  dashboardFilterForm.getForm().findField('from_dt').reset();
	        		  dashboardFilterForm.getForm().findField('to_dt').reset();
	        	  }
	        }
	} ;

	var dashboardFilterForm = new Ext.form.FormPanel({
		id:'dashboardFilterForm',
		renderTo: 'filterPanel',
		autoHeight : true,
		collapsible : false,
		border : true,
		width : 770,
		align: 'right',
		title: 'Delivery tracking report',
		layout: {
			type: 'hbox',
			align: 'right',
			padding:5
		},
		items: [{xtype: 'tbspacer', width:50},
		        presetDT,
		        {xtype: 'tbspacer', width:20}, {
		        	xtype: 'datefield',
		        	emptyText : 'From Date',
		        	format: 'd/m/Y',
		        	editable: false,
		        	name: 'from_dt',
					listeners : {
			        	  'select' : function(comp) {
			        		  dashboardFilterForm.getForm().findField('preset_dt').setValue('cs');
			        	  }
			        }
		        },{xtype: 'tbspacer', width:20},{
		        	xtype: 'datefield',
		        	emptyText : 'To Date',
		        	format: 'd/m/Y',
		        	editable: false,
		        	name: 'to_dt',
					listeners : {
			        	  'select' : function(comp) {
			        		  dashboardFilterForm.getForm().findField('preset_dt').setValue('cs');
			        	  }
			        }
		        },{xtype: 'tbspacer', width:20},{
		        	xtype: 'button',
		        	name: 'overviewrefresh',
		        	text : 'Refresh',
		        	width:75,
		        	handler : function () {
		        		reloadDashboard();
		        	}
		        }]
	});


});