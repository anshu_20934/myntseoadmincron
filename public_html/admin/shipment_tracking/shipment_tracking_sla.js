Ext.onReady(function() {
	Ext.QuickTips.init();
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
	Ext.Ajax.on('beforerequest', myMask.show, myMask);
	Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
	Ext.Ajax.on('requestexception', myMask.hide, myMask);

	 // example of custom renderer function
    function change(val){
    	return '<div style="background-color:#F9F9FB;font-weight:bold;font-size:12;height:25">' + val + '</div>';
    }
    
	var reader=	new Ext.data.JsonReader({
		root: "rows",
		totalProperty: 'count',
		idProperty:'courier_service',
		fields : ['courier_service',
		          'dl_tat_in_1',
		          'dl_tat_bt_2_3',
		          'dl_tat_bt_4_5',
		          'dl_tat_bt_6_8',
		          'dl_tat_bt_9_12',
		          'dl_tat_gt_12',
		          'fs_tat_in_1',
		          'fs_tat_in_2',
		          'fs_tat_in_3',
		          'fs_tat_gt_3',
		          'rt_tat_in_3',
		          'rt_tat_bt_4_6',
		          'rt_tat_bt_7_9',
		          'rt_tat_bt_10_12',
		          'rt_tat_gt_12',]
	});

	var slaStore = new Ext.data.Store({
		url: 'shipment_tracking_ajax.php',
		pruneModifiedRecords : true,
		reader: reader,
		remoteSort: false
	});    

    new Ext.grid.GridPanel({
		store: slaStore,
		columns: [
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>Delivery TAT</span>", width:170, dataIndex: 'courier_service', sortable: false, align:'left', renderer: change},
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>&lt;= 1 day</span>", width:100, dataIndex: 'dl_tat_in_1', sortable: false, align:'center'},
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>2-3 days</span>", width:100, dataIndex: 'dl_tat_bt_2_3', sortable: false, align:'center'},
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>4-5 days</span>", width:100, dataIndex: 'dl_tat_bt_4_5', sortable: false, align:'center'},
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>6-8 days</span>", width:100, dataIndex: 'dl_tat_bt_6_8', sortable: false, align:'center'},
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>9-12 days</span>", width:100, dataIndex: 'dl_tat_bt_9_12', sortable: false, align:'center'},
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>&gt;12 days</span>", width:100, dataIndex: 'dl_tat_gt_12', sortable: false, align:'center'}
		          ],
		          renderTo:'deliveryTATPanel',
		          width:770,
		          autoHeight:true,
		          listeners : {
		        	  'render' : function(comp) {
		        		  reloadDashboard();
		        	  }
		          }
	});

	new Ext.grid.GridPanel({
		store: slaStore,
		columns: [
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>First scan TAT</span>", width:170, dataIndex: 'courier_service', sortable: false, align:'left', renderer: change},
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>1 day</span>", width:100, dataIndex: 'fs_tat_in_1', sortable: false, align:'center'},
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>2 days</span>", width:100, dataIndex: 'fs_tat_in_2', sortable: false, align:'center'},
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>3 days</span>", width:100, dataIndex: 'fs_tat_in_3', sortable: false, align:'center'},
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>&gt;3 days</span>", width:100, dataIndex: 'fs_tat_gt_3', sortable: false, align:'center'},
		          {header: "", width:200, dataIndex: 'JUST_A_FILLER', sortable: false, align:'center'}
		          ],
		          renderTo:'firstScanTATPanel',
		          width:770,
		          autoHeight:true,
		          listeners : {
		        	  'render' : function(comp) {
		        		  reloadDashboard();
		        	  }
		          }
	});

    new Ext.grid.GridPanel({
		store: slaStore,
		columns: [
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>RTO TAT</span>", width:170, dataIndex: 'courier_service', sortable: false, align:'left', renderer: change},
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>1-3 days</span>", width:100, dataIndex: 'rt_tat_in_3', sortable: false, align:'center'},
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>4-6 days</span>", width:100, dataIndex: 'rt_tat_bt_4_6', sortable: false, align:'center'},
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>7-9 days</span>", width:100, dataIndex: 'rt_tat_bt_7_9', sortable: false, align:'center'},
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>10-12 days</span>", width:100, dataIndex: 'rt_tat_bt_10_12', sortable: false, align:'center'},
		          {header: "<span style='background-color:#F9F9FB;font-weight:bold;font-size:12'>&gt;12 days</span>", width:100, dataIndex: 'rt_tat_gt_12', sortable: false, align:'center'},
		          {header: "", width:100, dataIndex: 'JUST_A_FILLER', sortable: false, align:'center'}
		          ],
		          renderTo:'rtoTATPanel',
		          width:770,
		          autoHeight:true,
		          listeners : {
		        	  'render' : function(comp) {
		        		  reloadDashboard();
		        	  }
		          }
	});

	function reloadDashboard() {
		var params = {};
		params["action"] = "loadSLAReport";
		params["presetDate"] = 'd-1';
		if(dashboardFilterForm != null){
			var form = dashboardFilterForm.getForm();
			
			params['fromdate']	= form.findField('from_dt').getValue();
			params['todate']	= form.findField('to_dt').getValue();
			params["presetDate"] = form.findField('preset_dt').getValue();
		}
		slaStore.baseParams = params;
		slaStore.load();
	}

	var presetDTStore = new Ext.data.JsonStore({
		fields : [ 'code', 'label' ],
		data : [
		        {"code":"d-1", "label":"Yesterday"},
		        {"code":"d-7", "label":"Last 7 days"},
		        {"code":"d-15", "label":"Last 15 days"},
		        {"code":"d-30", "label":"Last 30 days"},
		        {"code":"cs", "label":"Custom"}]});

	var presetDT = {
			xtype : 'combo',
			value:'d-1',
			store : presetDTStore,
			mode: 'local',
			displayField : 'label',
			valueField : 'code',
			triggerAction: 'all',
			name : 'preset_dt',
			editable : false,
			listeners : {
	        	  'select' : function(comp) {
	        		  dashboardFilterForm.getForm().findField('from_dt').reset();
	        		  dashboardFilterForm.getForm().findField('to_dt').reset();
	        	  }
	        }
	} ;

	var dashboardFilterForm = new Ext.form.FormPanel({
		id:'dashboardFilterForm',
		renderTo: 'slaFilterPanel',
		autoHeight : true,
		collapsible : false,
		border : true,
		width : 770,
		align: 'right',
		title: 'Shipping vendor SLAs',
		layout: {
			type: 'hbox',
			align: 'right',
			padding:5
		},
		items: [{xtype: 'tbspacer', width:50},
		        presetDT,
		        {xtype: 'tbspacer', width:20}, {
		        	xtype: 'datefield',
		        	emptyText : 'From Date',
		        	format: 'd/m/Y',
		        	editable: false,
		        	name: 'from_dt',
					listeners : {
			        	  'select' : function(comp) {
			        		  dashboardFilterForm.getForm().findField('preset_dt').setValue('cs');
			        	  }
			        }
		        },{xtype: 'tbspacer', width:20},{
		        	xtype: 'datefield',
		        	emptyText : 'To Date',
		        	format: 'd/m/Y',
		        	editable: false,
		        	name: 'to_dt',
					listeners : {
			        	  'select' : function(comp) {
			        		  dashboardFilterForm.getForm().findField('preset_dt').setValue('cs');
			        	  }
			        }
		        },{xtype: 'tbspacer', width:20},{
		        	xtype: 'button',
		        	name: 'overviewrefresh',
		        	text : 'Refresh',
		        	width:75,
		        	handler : function () {
		        		reloadDashboard();
		        	}
		        }]
	});


});