<?php
require_once  "auth.php";
require_once $xcart_dir.'/modules/RestAPI/RestRequest.php';

$dataForBulkDecrement = createJSONForBulkDecrement($_REQUEST['ntfId']);
$dataForTotalCountBulkDecrement = createJSONForTotalCountBulkDecrement($_REQUEST['ntfId']);
$url = HostConfig::$notificationsUrl.'/?id='. $_REQUEST['ntfId'];
$method = 'DELETE';
$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Content-type: application/json';
$headers[] = 'Accept-Language: en-US';
$request = new RestRequest($url, $method);
$request->setHttpHeaders($headers);
$request->execute();
$info = $request->getResponseInfo();
$body = $request->getResponseBody();
if($info['http_code'] == 200){
    $response = json_decode($body,true);
    if($response['NotificationResponse']['status']['statusType'] == 'SUCCESS'){
        $XCART_SESSION_VARS['deleteNotification'] = true;
        if(count($dataForBulkDecrement)){
            restForBulkDecrement($dataForBulkDecrement);
        }
        if(count($dataForTotalCountBulkDecrement)){
            restForBulkDecrement($dataForTotalCountBulkDecrement);
        }
    }
}

function restForBulkDecrement($dataForBulkDecrement){
    $url = HostConfig::$notificationsUpsUrl.'/bulkDecrement';
    $method = 'POST';
    $data = json_encode($dataForBulkDecrement);
    $request = new RestRequest($url, $method, $data);
    $headers = array();
    $headers[] = 'Accept: application/json';
    $headers[] = 'Content-type: application/json';
    $headers[] = 'Accept-Language: en-US';
    $request->setHttpHeaders($headers);
    $request->execute();
}


function createJSONForBulkDecrement($ntfId){
    $url = HostConfig::$notificationsUrl.'/'.$ntfId;
    $method = 'GET';
    $headers = array();
    $headers[] = 'Accept: application/json';
    $headers[] = 'Content-type: application/json';
    $headers[] = 'Accept-Language: en-US';
    $request = new RestRequest($url, $method);
    $request->setHttpHeaders($headers);
    $request->execute();
    $info = $request->getResponseInfo();
    $body = $request->getResponseBody();
    if($info['http_code'] == 200){
        $response = json_decode($body,true);
        if($response['NotificationResponse']['status']['statusType'] == 'SUCCESS'){
            $tempArr = $response['NotificationResponse']['NotificationList']['data']['userList'];
            $jsonArr = array();
            if(count($tempArr[0]) || !$tempArr['read']){
                if(count($tempArr[0])){
                    for($i=0; $i<count($tempArr); $i++){
                        if(!$tempArr[$i]['read']){
                            $jsonArr[$tempArr[$i]['userId']] = array();
                            $jsonArr[$tempArr[$i]['userId']]['attribute'] = 'notificationCounter';
                            $jsonArr[$tempArr[$i]['userId']]['sourceid'] = '3';
                        }
                    }
                }
                else if(!$tempArr['read']){
                    $jsonArr[$tempArr['userId']] = array();
                    $jsonArr[$tempArr['userId']]['attribute'] = 'notificationCounter';
                    $jsonArr[$tempArr['userId']]['sourceid'] = '3';
                }
            }
            return $jsonArr;
        }
    }
}

function createJSONForTotalCountBulkDecrement($ntfId){
    $url = HostConfig::$notificationsUrl.'/'.$ntfId;
    $method = 'GET';
    $headers = array();
    $headers[] = 'Accept: application/json';
    $headers[] = 'Content-type: application/json';
    $headers[] = 'Accept-Language: en-US';
    $request = new RestRequest($url, $method);
    $request->setHttpHeaders($headers);
    $request->execute();
    $info = $request->getResponseInfo();
    $body = $request->getResponseBody();
    if($info['http_code'] == 200){
        $response = json_decode($body,true);
        if($response['NotificationResponse']['status']['statusType'] == 'SUCCESS'){
            $tempArr = $response['NotificationResponse']['NotificationList']['data']['userList'];
            $jsonArr = array();
            if(count($tempArr[0])){
                for($i=0; $i<count($tempArr); $i++){
                        $jsonArr[$tempArr[$i]['userId']] = array();
                        $jsonArr[$tempArr[$i]['userId']]['attribute'] = 'totalNotificationCounter';
                        $jsonArr[$tempArr[$i]['userId']]['sourceid'] = '3';
                }
            }
            else{
                $jsonArr[$tempArr['userId']] = array();
                $jsonArr[$tempArr['userId']]['attribute'] = 'totalNotificationCounter';
                $jsonArr[$tempArr['userId']]['sourceid'] = '3';
            }
            return $jsonArr;
        }
    }
}

header('Location: /admin/notifications_delete.php');

?>
                                                                                                                