<?php
require "./auth.php";
require $xcart_dir."/include/security.php";

if ($REQUEST_METHOD == "POST") {

	  if(!isset($_FILES) && isset($HTTP_POST_FILES))
			 $_FILES = $HTTP_POST_FILES;
    
	     // print_r($_POST);exit;
	   // upload thumb and large image for product type

	   if(!empty($_FILES['img']['name']) && $_POST['imgtype'] == 'sizechart' ){	

			$sizechart_img =  'images/style/sizechart/'.$_FILES['img']['name'];
     		$uploaddir2 = '../images/style/sizechart/';
			$uploadfile2 = $uploaddir2 . basename($_FILES['img']['name']);
			
			if (@move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile2)) {
				define("sizechart_img",$sizechart_img);
			}
			else
			   {
				  define("sizechart_img",'');
			   }
		}
		 if(!empty($_FILES['img']['name']) && $_POST['imgtype'] == 'thumbsmall' ){	
			
			$thumbsmall =  'images/startshopping/'.$_FILES['img']['name'];
     		$uploaddir2 = '../images/startshopping/';
			$uploadfile2 = $uploaddir2 . basename($_FILES['img']['name']);
			
			if (@move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile2)) {
				define("thumbsmall",$thumbsmall);
			}
			else
			   {
				  define("thumbsmall",'');
			   }
		}
		if(!empty($_FILES['img']['name']) && $_POST['imgtype'] == 'thumbbig' ){	

			$thumbbig =  'images/startshopping/'.$_FILES['img']['name'];
     		$uploaddir2 = '../images/startshopping/';
			$uploadfile2 = $uploaddir2 . basename($_FILES['img']['name']);
			
			if (@move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile2)) {
				define("thumbbig",$thumbbig);
			}
			else
			   {
				  define("thumbbig",'');
			   }
		}

	   
	   if(!empty($_FILES['img']['name']) && $_POST['imgtype'] == 'thumb' ){

			  $th_img ='images/type/T/'.$_FILES['img']['name'];
             
			  $uploaddir1 = '../images/type/T/';

			  $uploadfile1 = $uploaddir1 . basename($_FILES['img']['name']);
              
			  if (@move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile1)) {
				    define("THMIMAGE",$th_img);
		 	 }else
			{
				  define("THMIMAGE",'');
			}
		 }
		 if(!empty($_FILES['img']['name']) && $_POST['imgtype'] == 'detail' )
		 {	

			$det_img = 'images/type/T/'.$_FILES['img']['name'];
     		$uploaddir2 = '../images/type/T/';
			$uploadfile2 = $uploaddir2 . basename($_FILES['img']['name']);
			
			if (@move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile2)) {
				define("DETIMAGE",$det_img);
			}
			else
			{
				 define("DETIMAGE",'');
			}

		}
		if(!empty($_FILES['img']['name']) && $_POST['imgtype'] == 'size' )
		 {	

			$siz_img = 'images/type/S/'.$_FILES['img']['name'];
     		$uploaddir3 = '../images/type/S/';
			$uploadfile3 = $uploaddir3 . basename($_FILES['img']['name']);
			
			if (@move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile3)) {
				define("SIZIMAGE",$siz_img);
			}
			else
			{
				 define("SIZIMAGE",'');
			}

		}
		if(!empty($_FILES['img']['name']) && $_POST['imgtype'] == 'iconimg' )
		 {	

			$icon_img = 'images/type/I/'.$_FILES['img']['name'];
     		$uploaddir3 = '../images/type/I/';
			$uploadfile3 = $uploaddir3 . basename($_FILES['img']['name']);
			if (@move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile3)) {
				define("ICONIMAGE",$icon_img);
			}
			else
			{
				 define("ICONIMAGE",'');
			}

		}


		 // upload thumb and large image and icon image for  product style
		 if(!empty($_FILES['img']['name']) && $_POST['imgtype'] == 'thstyle' )
		 {	

				$th_img = 'images/style/T/'.$_FILES['img']['name'];
				$uploaddir2 = '../images/style/T/';
				$uploadfile2 = $uploaddir2 . basename($_FILES['img']['name']);
				
				if (@move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile2))
				{
					  define("THMIMAGE",$th_img);
				}
				else
			   {

					  define("THMIMAGE",'');
			   }

		}
		if(!empty($_FILES['img']['name']) && $_POST['imgtype'] == 'detstyle' )
		{	

			$det_img = 'images/style/L/'.$_FILES['img']['name'];
     		$uploaddir2 = '../images/style/L/';
			$uploadfile2 = $uploaddir2 . basename($_FILES['img']['name']);
			
			if (@move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile2)) 
			{
				 define("DETIMAGE",$det_img);
			}
			else
			   {
				  define("DETIMAGE",'');
			   }


		}

		if(!empty($_FILES['img']['name']) && $_POST['imgtype'] == 'iconstyle' )
		{	

			$icon_img = 'images/style/I/'.$_FILES['img']['name'];
     		$uploaddir2 = '../images/style/I/';
			$uploadfile2 = $uploaddir2 . basename($_FILES['img']['name']);
			
			if (@move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile2)) 
			{
				 define("ICONIMAGE",$icon_img);
			}
			else
			   {
				 define("ICONIMAGE",'');
			   }


		}
		
		/* upload custumization  thumb and detail images */
		
		
		if(!empty($_FILES['img']['name']) && $_POST['imgtype'] == 'thcust' )
		 {	

			$th_img = 'images/style/carea/T/'.$_FILES['img']['name'];
     		$uploaddir2 = '../images/style/carea/T/';
			$uploadfile2 = $uploaddir2 . basename($_FILES['img']['name']);
			
			if (@move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile2)) {
				define("THMIMAGE",$th_img);
			}
			else
			   {
				 define("THMIMAGE",'');
			   }


		}
		
		
		if(!empty($_FILES['img']['name']) && $_POST['imgtype'] == 'detcust' ){	

			$det_img =  'images/style/carea/L/'.$_FILES['img']['name'];
     		$uploaddir2 = '../images/style/carea/L/';
			$uploadfile2 = $uploaddir2 . basename($_FILES['img']['name']);
			
			if (@move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile2)) 
			{
				define("DETIMAGE",$det_img);
			}
			else
			   {
				 define("DETIMAGE",'');
			   }


		}
		
		if(!empty($_FILES['img']['name']) && $HTTP_POST_VARS['imgtype'] == 'previewcust' ){	

			$preview_img =  'images/style/carea/L/'.$_FILES['img']['name'];
     		$uploaddir = '../images/style/carea/L/';
			$uploaddir = $uploaddir.basename($_FILES['img']['name']);
			
			if (@move_uploaded_file($_FILES['img']['tmp_name'], $uploaddir)) 
			{
				define("PREVIEWIMAGE",$preview_img);
			}
			else
			   {
				 define("PREVIEWIMAGE",'');
			   }


		}
      if(!empty($_FILES['img']['name']) && $HTTP_POST_VARS['imgtype'] == 'zoomimage' ){

			$preview_img =  'images/style/carea/L/'.$_FILES['img']['name'];
     		$uploaddir = '../images/style/carea/L/';
			$uploaddir = $uploaddir.basename($_FILES['img']['name']);

			if (@move_uploaded_file($_FILES['img']['tmp_name'], $uploaddir))
			{
				define("ZOOMIMAGE",$preview_img);
			}
			else
			   {
				 define("ZOOMIMAGE",'');
			   }


		}

        /* change mixer images */ 

		if(!empty($_FILES['img']['name']) && $_POST['imgtype'] == 'mixer1' ){	

			$det_img =  'images/Mixers/'.$_FILES['img']['name'];
     		$uploaddir2 = '../images/Mixers/';
			$uploadfile2 = $uploaddir2 . basename($_FILES['img']['name']);
			
			if (@move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile2)) {
				define("DETIMAGE",$det_img);
			}
			else
			   {
				  define("DETIMAGE",'');
			   }


		}

		if(!empty($_FILES['img']['name']) && $_POST['imgtype'] == 'mixer2' ){	

			$det_img =  'images/Mixers/'.$_FILES['img']['name'];
     		$uploaddir2 = '../images/Mixers/';
			$uploadfile2 = $uploaddir2 . basename($_FILES['img']['name']);
			
			if (@move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile2)) {
				define("DETIMAGE",$det_img);
			}
			else
			   {
				  define("DETIMAGE",'');
			   }


		}

		if(!empty($_FILES['img']['name']) && $_POST['imgtype'] == 'otherimg' ){	
                    
			$other_img =  'images/style/carea/O/'.$_FILES['img']['name'];
     		$uploaddir2 = '../images/style/carea/O/';
			$uploadfile2 = $uploaddir2 . basename($_FILES['img']['name']);
			
			if (@move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile2)) 
			{
				define("OTHERIMAGE",$other_img);
			}
			else
			   {
				 define("OTHERIMAGE",'');
			   }


		}

        

		
		

}
?>
<?php

if(empty($imgType)){ $imgType = ""; }


if ($REQUEST_METHOD == "POST") { 

	  if ($mode == "upload") {
	          /* change product type thumb & detail   images */ 
	           if($imgtype == "thumb"){
	  				func_array2update("producttypes",
			          array('image_t'  => THMIMAGE), " id='$tid'"); 
			   }
			   elseif($imgtype == "detail"){
			   		func_array2update("producttypes",
			          array('image_l'  => DETIMAGE), " id='$tid'"); 
			   
			   }
			    elseif($imgtype == "size"){
			   		func_array2update("producttypes",
			          array('image_s'  => SIZIMAGE), " id='$tid'"); 
			   
			   }
			   elseif($imgtype == "iconimg"){
			   		func_array2update("producttypes",
			          array('image_i'  => ICONIMAGE), " id='$tid'"); 
			   
			   }
			    /* change product style thumb & detail  images */ 
			   elseif($imgtype == 'thstyle')
			   {
			   		if($imageid)
			   			$query = "UPDATE mk_style_images SET  image_path='$th_img' WHERE styleid='$tid' AND id='$imageid'";
			   		else
			   			$query ="INSERT INTO mk_style_images(styleid,image_path,type,isdefault,name) values('$tid','$th_img','T','Y','Tumbnail')";
					
					$result   = db_query($query);
			   }
			   elseif($imgtype == 'detstyle')
			   {
			   		if($imageid)
						$query = "UPDATE mk_style_images SET image_path='$det_img' WHERE styleid='$tid' AND id='$imageid'";
					else
						$query ="INSERT INTO mk_style_images(styleid,image_path,type,isdefault,name) values('$tid','$det_img','D','Y','Detail') ";
					$result   = db_query($query);    
			   }
			   elseif($imgtype == 'iconstyle')
			   {
			   		if($imageid)
			   			$query = "UPDATE mk_style_images SET  image_path='$icon_img' WHERE styleid='$tid' AND id='$imageid'";
			   		else
			   	$query ="INSERT INTO mk_style_images(styleid,image_path,type,isdefault,name) values('$tid','$icon_img','I','Y','Icon Image') ";
				  
			      $result   = db_query($query);
			    
			   } 
			   elseif($imgtype == 'sizechart')
			   {
			   		if($imageid)
			   			$query = "UPDATE mk_product_style SET  size_chart_image='$sizechart_img' WHERE id='$tid' ";
			   		else
			   	$query ="INSERT INTO mk_product_style(size_chart_image) values('$sizechart_img') where id='$tid' ";
				  
			      $result   = db_query($query);
			    
			   }
			   elseif($imgtype == 'thumbsmall')
			   {
			   			$query = "UPDATE mk_product_type SET  thumb_small_image_path='$thumbsmall' WHERE id='$tid' ";
			   		//	$query ="INSERT INTO mk_product_type(thumb_small_image_path) values('$thumbsmall') where id='$pid' ";
			      $result   = db_query($query);
			    
			   }
			   elseif($imgtype == 'thumbbig')
			   {
			   			$query = "UPDATE mk_product_type SET  thumb_big_image_path='$thumbbig' WHERE id='$tid' ";
					//	$query ="INSERT INTO mk_product_type(thumb_big_image_path) values('$thumbbig') where id='$pid' ";
					$result   = db_query($query);
			    
			   }
			    /* change custumization thumb & detail  images */  
			   elseif($imgtype == 'thcust')
			   {
			     func_array2update("mk_style_customization_area",
			          array('default_image'  => THMIMAGE,'image_t'  => THMIMAGE), " id='$tid'"); 
			  
			   }
			   // for uploading more images for a style.

               elseif($imgtype == 'otherimg'){
				    $query_data = array(
								styleid => $tid,
	   							image_path => OTHERIMAGE,
								type => 'Other',
								isdefault=>'N');
			       func_array2insert("mk_style_images",$query_data); 
			    
			   }

			   elseif($imgtype == 'detcust'){
			        func_array2update("mk_style_customization_area",
			          array( 'image_l'  => DETIMAGE), " id='$tid'"); 
			    
			   }
			   
			   elseif($imgtype == 'previewcust'){
			        func_array2update("mk_style_customization_area",
			          array( 'bkgroundimage'  => PREVIEWIMAGE), " id='$tid'"); 
			    
			   }
                elseif($imgtype == 'zoomimage'){
			        func_array2update("mk_style_customization_area",
			          array( 'image_z'  => ZOOMIMAGE), " id='$tid'"); 

			   }
			   
			   $success = "Y";
			   $message = func_get_langvar_by_name("msg_adm_image_upd");		   
      }

}
$smarty->assign("imgtype",$imgType);
$smarty->assign("typeid",$id);
$smarty->assign("imageid",$imageid);
$smarty->assign("message",$message);

func_display("admin/main/changeUploadImage.tpl",$smarty);
?>