<?php

/**
 * This is the MongoDB Document model class based on table "seo_pagedata".
 */
class Pagedata extends EMongoDocument
{
    const STATIC_PAGE='1';
    const SEARCH_PAGE='2';
    const PDP_PAGE='3';

    public $_id;
	public $hash; //hash
    public $base_url; //base_url
    public $is_active=0;
    public $page_type = self::STATIC_PAGE;
	public $created_on;
	public $updated_on;
	public $updated_by;

	/**
	 * Returns the static model of the specified AR class.
	 * @return Pagedata the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	

	/**
	 * @return string the associated collection name
	 */
	public function getCollectionName()
	{
		return 'pagedata';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('base_url,page_type', 'required'),
            array('page_type','in','range'=>array(self::STATIC_PAGE,self::SEARCH_PAGE,self::PDP_PAGE)),
            array('is_active','in','range'=>array(1,0)),
            array('base_url','url','allowEmpty'=>false,'pattern'=>'/^(?:https?:\/\/)?(((?:[a-z0-9-]+\.)*(?:[a-z0-9-]+\.)[a-z]+)\/?([-_0-9a-z\/]*))(?:[\?\#])?/'),
            array('base_url','EMongoUniqueValidator','attributes'=>'base_url'),
            array('widgets', 'required'), //required by gii
			
		);
	}

    public function attributeNames() {
         return array(
             '_id'=>'_id',
             'hash'=>'hash',
             'base_url' => 'base_url',
             'page_type' => 'page_type',
             'is_active' => 'is_active',
             'created_on'=>'created_on',
             'updated_on'=>'updated_on',
             'updated_by'=>'updated_by',
             'widgets'=>'widgets', //required by gii
             );
     }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'base_url' => 'Url',
            'page_type' => 'Page Type',
            'is_active' => 'Status'
		);
	}
 
    public function embeddedDocuments()
    {
        return array(
            // property field name => class name to use for this embedded document
            'widgets' => 'Pagedatawidgets',
        );
    }

    public function beforeSave() {
        $this->hash = SeoHashUtil::toHash($this->base_url,$this->page_type);
        if ($this->isNewRecord)
            $this->created_on = time();

        $this->updated_on = time();
        $this->updated_by = Yii::app()->user->username;
        return parent::beforeSave();
    }
}
