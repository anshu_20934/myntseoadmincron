<?php

class PagedataOGwidget extends EMongoEmbeddedDocument
{
    public $head_meta_og_url='http://www.myntra.com';
    public $head_meta_og_title;
    public $head_meta_og_image='http://myntra.myntassets.com/skin2/images/logo.png';
    public $head_meta_og_sitename='Myntra';
    public $head_meta_og_type='website';
    public $head_meta_og_description;

    public function rules()
	{
		return array(
			array('head_meta_og_url,head_meta_og_title,
                head_meta_og_image,head_meta_og_sitename,head_meta_og_type,head_meta_og_description', 'length', 'max'=>255),
            array('head_meta_og_url,head_meta_og_title,
                head_meta_og_image,head_meta_og_sitename,head_meta_og_type,head_meta_og_description', 'required'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'head_meta_og_url' => 'Open Graph Url',
            'head_meta_og_title' => 'Open Graph Title',
            'head_meta_og_image' => 'Open Graph Image',
            'head_meta_og_sitename' => 'Open Graph Sitename',
            'head_meta_og_type' => 'Open Graph type',
            'head_meta_og_description' => 'Open Graph Description',
		);
	}

}
