<?php

class Pagedatawidgets extends EMongoEmbeddedDocument
{
    public $head_title;
    public $head_meta_keywords;
    public $head_meta_description;
    

    public function rules()
	{
		return array(
			array('head_title,head_meta_keywords,head_meta_description', 'length', 'max'=>255),
            //array('head_title,head_meta_keywords,head_meta_description', 'required'),
            array('og', 'required'), //required by gii
		);
	}

    public function attributeNames() {
         return array(
             'head_title' =>'head_title',
             'head_meta_keywords' => 'head_meta_keywords',
             'head_meta_description' => 'head_meta_description',
             'og'=>'og', //required by gii
             );
     }

	public function attributeLabels()
	{
		return array(
			'head_title' => 'Title tag',
            'head_meta_keywords' => 'Meta Keywords',
            'head_meta_description' => 'Meta Description',
		);
	}

     public function embeddedDocuments()
    {
        return array(
            // property field name => class name to use for this embedded document
            'og' => 'PagedataOGWidget',
        );
    }

}
