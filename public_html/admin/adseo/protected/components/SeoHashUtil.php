<?php
//namespace seo;
/**
 * A hash-producing utility built upon php5 hash 
 * functions that turns urls into a 32-char string.
 * 
 *
 * The type of URLs under this scope are:
 * @static:
 * @search:
 * @pdp:
 * For @search-type urls, where there normally exist
 * more than one combination producing the same result
 * set, it generates a canonical-url before returning
 * the hash.
 */

/**
 *@pacakge : seo
 *@author : Rahul Jaimini<rahul.jaimini@myntra.com>
 */

class SeoHashUtil {

    /**
     * a url reg-ex constructed in keeping with the scope
     * of this utility's common usage domain i.e. neither 
     * fool-proof nor rfc compliant
     */
    const URL_PATTERN = "/^(?:https?:\/\/)?(((?:[a-z0-9-]+\.)*(?:[a-z0-9-]+\.)[a-z]+)\/?([-_0-9a-z\/]*))(?:[\?\#])?/";

    /**
     * the host part of a url, including subdomains
     */
    static $_host;

    /**
     * the url path
     */
    static $_path;


    /**
     * the full path
     */
    static $_full_path;



    private static function init($url) {
        if (1 == preg_match(self::URL_PATTERN, $url, $captured_match)) {
            self::$_full_path = $captured_match[1];
            self::$_host = $captured_match[2];
            self::$_path = $captured_match[3];
        }
    }


    /**
     * @url: url of the client page
     * @type: static, search or pdp
     */
    public static function toHash($url, $type) {
        if (isset($url) && !empty($url)) {
            $url = strtolower($url); self::init($url);
            switch ($type) {
                case SeoPageType::STATIC_PAGE : 
                    return md5(self::getFullPath($url));
                case SeoPageType::SEARCH_PAGE :
                    $cPath = self::getCanonicalPath(self::getPath($url));
                    $cPath = trim($cPath);
                    if (empty($cPath)) {
                        return md5(self::getHost($url));
                    }
                    return md5(self::getHost($url).'/'. $cPath); 
                case SeoPageType::PDP_PAGE :
                    return md5(self::getFullPath($url));
                default: break;
            }
        }
        return null;
    }


    /*
     *TODO: a lot to be done
     */
    public static function toHashGroup($type) {
        if (empty($type)) {
            return null;
        }
        switch ($type) {
            case SeoPageType::PDP_PAGE: return md5('pdp.pdp');
            case SeoPageType::SEARCH_PAGE: return md5('search.search');
            case SeoPageType::STATIC_PAGE: return md5('static.static');
            default: return '';
        }
    }

    /**
     *@url
     *@returns: host+path
     */
    private function getFullPath($url) {
        return self::$_full_path;
    }
    
    /**
     *@url
     *@returns: host
     */
    private function getHost($url) {
        return self::$_host;
    }


    /**
     *@url
     *@returns: path
     */
    private function getPath($url) {
        return self::$_path;
    }


    /**
     * To maintain a canonical hash for all possible combinations on 'path'
     * for a particular search
     *@url path, for eg. men-nike-shoes
     *@return: canonical path
     *@NOTE: This is not tightly bound to the 'search' implementation. So
     * change it accordingly
     */
    private function getCanonicalPath($path) {
        if (empty($path)) {
            return '';
        }

        $terms = explode('-', $path);
        sort($terms);
        return implode('-', $terms);
    }
}
?>
