<?php
return array (
  'template' => 'default',
  'connectionId' => 'db',
  'tablePrefix' => 'seo_',
  'modelPath' => 'application.models',
  'baseClass' => 'CActiveRecord',
  'buildRelations' => '1',
);
