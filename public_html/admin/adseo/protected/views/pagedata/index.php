<?php
$this->breadcrumbs=array(
	'Pagedata',
);

$this->menu=array(
	array('label'=>'Create Pagedata', 'url'=>array('create')),
	//array('label'=>'Manage Pagedata', 'url'=>array('admin')),
);
?>

<h1>Pagedata</h1>

<!--	<b>Base Url Created On Update On Update By</b>-->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$dataProvider,
    'columns'=>array(
        'base_url:Text:Url',
        array(
            'name'=>'Status',
            'value'=>'$data->is_active?"active":"inactive"',
        ),
        'page_type:Text:Page Type',
        array(
            'name'=>'Last Updated On',
            'value'=>'date("M j, Y", $data->updated_on)',
        ),
        array(            // display a column with "view", "update" and "delete" buttons
            'class'=>'CButtonColumn',
        ),
    ),
)); ?>

