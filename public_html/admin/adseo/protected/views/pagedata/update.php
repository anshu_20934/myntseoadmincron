<?php
$this->breadcrumbs=array(
	'Pagedatas'=>array('index'),
	//$model->_id=>array('view','id'=>$model->_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Pagedata', 'url'=>array('index')),
	array('label'=>'Create Pagedata', 'url'=>array('create')),
	array('label'=>'View Pagedata', 'url'=>array('view', 'id'=>$model->_id)),
	//array('label'=>'Manage Pagedata', 'url'=>array('admin')),
);
?>

<h1>Update <?php echo $model->base_url; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>