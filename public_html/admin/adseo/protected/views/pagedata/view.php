<?php
$this->breadcrumbs=array(
	'Pagedatas'=>array('index'),
	$model->base_url,
);

$this->menu=array(
	array('label'=>'List Pagedata', 'url'=>array('index')),
	array('label'=>'Create Pagedata', 'url'=>array('create')),
	array('label'=>'Update Pagedata', 'url'=>array('update', 'id'=>$model->_id)),
	array('label'=>'Delete Pagedata', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->_id),'confirm'=>'Are you sure you want to delete this item?')),
//	array('label'=>'Manage Pagedata', 'url'=>array('admin')),
);
?>

<h1>View Pagedata #<?php echo $model->base_url; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'base_url',
        array(
            'label'=>'Status',
            'value'=>$model->is_active?'active':'inactive',
        ),
        array(
            'label'=>'Page Type',
            'value'=>$model->page_type==1?'Static Page':
            ($model->page_type==2?'Search Page':'Pdp Page'),
        ),
		'created_on:datetime',
		'updated_on:datetime',
		'updated_by',
        array(
            'label'=>'Title',
            'value'=>$model->widgets->head_title,
        ),
        array(
            'label'=>'Meta Keywords',
            'value'=>$model->widgets->head_meta_keywords,
        ),
        array(
            'label'=>'Meta Description',
            'value'=>$model->widgets->head_meta_description,
        ),
        array(
            'label'=>'Open Graph url',
            'value'=>$model->widgets->og->head_meta_og_url,
        ),
        array(
            'label'=>'Open Graph Title',
            'value'=>$model->widgets->og->head_meta_og_title,
        ),
        array(
            'label'=>'Open Graph Image',
            'value'=>$model->widgets->og->head_meta_og_image,
        ),
        array(
            'label'=>'Open Graph Sitename',
            'value'=>$model->widgets->og->head_meta_og_sitename,
        ),
        array(
            'label'=>'Open Graph Type',
            'value'=>$model->widgets->og->head_meta_og_type,
        ),
        
		//'widgets',
	),
)); ?>
