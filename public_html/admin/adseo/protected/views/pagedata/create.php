<?php
$this->breadcrumbs=array(
	'Pagedatas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Pagedata', 'url'=>array('index')),
	//array('label'=>'Manage Pagedata', 'url'=>array('admin')),
);
?>

<h1>Create Pagedata</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>