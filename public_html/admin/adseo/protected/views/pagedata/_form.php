<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pagedata-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'base_url'); ?>
		<?php echo $form->textField($model,'base_url'); ?>
		<?php echo $form->error($model,'base_url'); ?>
	</div>

    <div class="row">
		<?php echo $form->labelEx($model,'is_active'); ?>
		<?php echo $form->dropDownList($model,'is_active',
                array(
                    1=>'active',
                    0=>'inactive',
                    )); ?>
		<?php echo $form->error($model,'page_type'); ?>
	</div>

    <div class="row">
		<?php echo $form->labelEx($model,'page_type'); ?>
		<?php echo $form->dropDownList($model,'page_type',
                array(
                    Pagedata::STATIC_PAGE=>'Static Page',
                    Pagedata::SEARCH_PAGE=>'Search Page',
                    Pagedata::PDP_PAGE=>'Pdp Page',
                    )); ?>
		<?php echo $form->error($model,'page_type'); ?>
	</div>
    <br/><br>
	<b><u>Page Widgets</u></b>
    <div class="row">
        <?php echo $form->labelEx($model->widgets,'head_title'); ?>
		<input type="text" name="Pagedata[widgets][head_title]" id ="Pagedata_widgets_head_title"
               value="<?php echo $model->widgets->head_title; ?>" />
    </div><div class="row">
        <?php echo $form->labelEx($model->widgets,'head_meta_keywords'); ?>
		<input type="text" name="Pagedata[widgets][head_meta_keywords]" id ="Pagedata_widgets_head_meta_keywords"
               value="<?php echo $model->widgets->head_meta_keywords; ?>" />
        </div><div class="row">
        <?php echo $form->labelEx($model->widgets,'head_meta_description'); ?>
		<input type="text" name="Pagedata[widgets][head_meta_description]" id ="Pagedata_widgets_head_meta_description"
               value="<?php echo $model->widgets->head_meta_description; ?>" />
        </div>
        <br/>
        
        <b><u>Open Graph Properties</u></b>
        <div class="row">
        <?php echo $form->labelEx($model->widgets->og,'head_meta_og_url'); ?>
		<input type="text" name="Pagedata[widgets][og][head_meta_og_url]" id ="Pagedata_widgets_og_head_meta_og_url"
               value="<?php echo $model->widgets->og->head_meta_og_url; ?>" />
        </div><div class="row">
        <?php echo $form->labelEx($model->widgets->og,'head_meta_og_title'); ?>
		<input type="text" name="Pagedata[widgets][og][head_meta_og_title]" id ="Pagedata_widgets_og_head_meta_og_title"
               value="<?php echo $model->widgets->og->head_meta_og_title; ?>" />
        </div><div class="row">
        <?php echo $form->labelEx($model->widgets->og,'head_meta_og_image'); ?>
		<input type="text" name="Pagedata[widgets][og][head_meta_og_image]" id ="Pagedata_widgets_og_head_meta_og_image"
               value="<?php echo $model->widgets->og->head_meta_og_image; ?>" />
        </div><div class="row">
        <?php echo $form->labelEx($model->widgets->og,'head_meta_og_sitename'); ?>
		<input type="text" name="Pagedata[widgets][og][head_meta_og_sitename]" id ="Pagedata_widgets_head_meta_og_sitename"
               value="<?php echo $model->widgets->og->head_meta_og_sitename; ?>" />
        </div><div class="row">
        <?php echo $form->labelEx($model->widgets->og,'head_meta_og_type'); ?>
		<input type="text" name="Pagedata[widgets][og][head_meta_og_type]" id ="Pagedata_widgets_head_meta_og_type"
               value="<?php echo $model->widgets->og->head_meta_og_type; ?>" />
        </div><div class="row">
        <?php echo $form->labelEx($model->widgets->og,'head_meta_og_description'); ?>
		<input type="text" name="Pagedata[widgets][og][head_meta_og_description]" id ="Pagedata_widgets_head_meta_og_description"
               value="<?php echo $model->widgets->og->head_meta_og_description; ?>" />
        </div>
	



	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->