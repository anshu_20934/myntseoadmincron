<?php
require './auth.php';
require $xcart_dir.'/include/security.php';
require_once $xcart_dir."/include/validate.php";
$errormessage = "";
function check_duplicate_entry($table,$id_array = '',$data_array = ''){
	/*
	##common function to check duplicate entry for 
	##source name, code, source_type, location
	*/
	global $sql_tbl;
	$data_array['DATA'] = mysql_real_escape_string($data_array['DATA']);
	
	##check for duplicate while adding
	if(!$id_array){
		$sql = "select count(*) from {$table} where LOWER({$data_array['FIELD']}) = LOWER('{$data_array['DATA']}')";
		$count = func_query_first_cell($sql);
		if($count > 0)//entry already exists for the data
			return false;
		else 
			return true;
			
	}else{//while editing
		$sql = "select count(*) from {$table} where {$id_array['FIELD']} != '{$id_array['DATA']}' AND LOWER({$data_array['FIELD']}) = LOWER('{$data_array['DATA']}')";
		$count = func_query_first_cell($sql);
		if($count > 0)//entry already exists for the data
			return false;
		else 
			return true;
	}	
}

function _get_sources(){
	/*
	##get sources
	*/
	global $sql_tbl;
	$sql = "select
				s.*,
				st.active as type_active
			from
				{$sql_tbl['sources']} s
			left join
				{$sql_tbl['source_types']} st 
			on
				s.source_type_id = st.type_id order by s.source_id";
	return $types = func_query($sql);
}

function _get_source_types(){
	/*
	##get source types
	*/
	global $sql_tbl;
	$sql = "select * from {$sql_tbl['source_types']}";
	return $types = func_query($sql);
}

function _get_source_locations(){
	/*
	##get source locations
	*/
	global $sql_tbl;
	$sql = "select * from {$sql_tbl['source_locations']}";
	return $types = func_query($sql);
}

function _get_country(){
	/*
	##get country
	*/
	global $sql_tbl;
	$sql = "select code,name from {$sql_tbl['countries']} where status = 1";
	return $types = func_query($sql);
}

function _get_states(){
	/*
	##get country
	*/
	global $sql_tbl;
	$sql = "select code,state from {$sql_tbl['states']}";
	return $types = func_query($sql);
}


/*
##action for source types starts here
*/
if($HTTP_GET_VARS['msg'])
	$admin_info = "Sorry!The source type '<b>{$HTTP_GET_VARS['msg']}</b>' can not be made inactive as it contains one or more active sources!";

if(!empty($add_type)){
	/*
	##save source type
	*/
	if(regex_validate($type_name,'AN_S') && (mysql_real_escape_string($type_name) == $type_name)){
		if(check_duplicate_entry($sql_tbl['source_types'],'',array('FIELD'=>'type_name','DATA'=>$type_name))){
			$query_data = array(
						"type_name" => $type_name,						
						"active" => ($active == 'on')?1:0							
						);
			func_array2insert($sql_tbl['source_types'],$query_data);
			func_header_location("manage_order_sources.php");	
		}else{
			$admin_info = "Source type <b>\"{$type_name}\"</b>alreday exists! Type should be unique.";
		}		
	}else{
		$admin_info = $match_fail_message['AN_S'];//display message		
	}
	
}elseif (!empty($modify_type)){
	/*
	##modify source type
	*/
	if(regex_validate($type_name,'AN_S') && (mysql_real_escape_string($type_name) == $type_name)){
		if(check_duplicate_entry($sql_tbl['source_types'],array('FIELD'=>'type_id','DATA'=>$type_id),array('FIELD'=>'type_name','DATA'=>$type_name))){
			
			##before making a type inactive, check for active sources in it
			if($active != 'on'){
				$sql = "select count(*) from {$sql_tbl['sources']} WHERE  source_type_id = '{$type_id}' and active = '1'";
				$source_count = func_query_first_cell($sql);
				if($source_count < 1){
					$active = '';
				}else{
					$active = 'on';
					$msg = $type_name;//to show admin_info					
				}					
			}
			$query_data = array(
						"type_name" => $type_name,
						"active" => ($active == 'on')?1:0							
						);
			func_array2update($sql_tbl['source_types'], $query_data, "type_id=".$type_id);
			if($msg)
				func_header_location("manage_order_sources.php?msg={$msg}");	
			else 
				func_header_location("manage_order_sources.php");	
		}else{
			$admin_info = "Source type <b>\"{$type_name}\"</b>alreday exists! Type should be unique.";
		}					 
	}else
		$admin_info = $match_fail_message['AN_S'];//display message		
			
}elseif($mode == "delete_type"){
	/*
	##deleting source types details ##
	*/
	$sql = 'select count(*) from '.$sql_tbl['sources'].' WHERE  source_type_id='.$type_id;
	$source_count = func_query_first_cell($sql);
	if($source_count < 1)
		db_query("DELETE FROM ".$sql_tbl['source_types']." WHERE type_id=".$type_id);				
	else
		$admin_info = "Sorry!The source type '<b>{$type_name}</b>' can not be deleted as it contains one or more sources!";			
}
/*==============manage source types======================*/


/*
##action for source locations starts here
*/
if(!empty($add_location)){
	/*
	##save source location
	*/	
	if(
		regex_validate($location_name,'AN_S') 
		&& regex_validate($city,'AN_S')
		&& !empty($state_code)
		&& !empty($country_code)
		&& (mysql_real_escape_string($location_name) == $location_name)
		&& (mysql_real_escape_string($city) == $city)
		&& (mysql_real_escape_string($state_code) == $state_code)
		&& (mysql_real_escape_string($country_code) == $country_code)
		){
		if(check_duplicate_entry($sql_tbl['source_locations'],'',array('FIELD'=>'location_name','DATA'=>$location_name))){
			$query_data = array(
						"location_name" => $location_name,						
						"city" => $city,
						"state" => $state_code,
						"country" => $country_code							
						);
			func_array2insert($sql_tbl['source_locations'],$query_data);
			func_header_location("manage_order_sources.php");	
		}else{
			$location_info = "Source location <b>\"{$location_name}\"</b>alreday exists! Location should be unique.";
		}		
	}else{
		$location_info = $match_fail_message['AN_S'];//display message		
	}
	
}elseif (!empty($modify_location)){
	/*
	##modify source location
	*/
	if(
		regex_validate($location_name,'AN_S') 
		&& regex_validate($city,'AN_S')
		&& !empty($state_code)
		&& !empty($country_code)
		&& (mysql_real_escape_string($location_name) == $location_name)
		&& (mysql_real_escape_string($city) == $city)
		&& (mysql_real_escape_string($state_code) == $state_code)
		&& (mysql_real_escape_string($country_code) == $country_code)
		){
		if(check_duplicate_entry($sql_tbl['source_locations'],array('FIELD'=>'location_id','DATA'=>$location_id),array('FIELD'=>'location_name','DATA'=>$location_name))){
			$query_data = array(
						"location_name" => $location_name,						
						"city" => $city,
						"state" => $state_code,
						"country" => $country_code							
						);
			func_array2update($sql_tbl['source_locations'], $query_data, "location_id=".$location_id);			
		}else{
			$location_info = "Source location <b>\"{$location_name}\"</b>alreday exists! Location should be unique.";
		}
		
	}else
		$location_info = $match_fail_message['AN_S'];//display message		
			
}elseif($mode == "delete_location"){
	/*
	##deleting source locations details ##
	*/
	$sql = 'select count(*) from '.$sql_tbl['sources'].' WHERE  location_id='.$location_id;
	$source_count = func_query_first_cell($sql);
	if($source_count < 1)
		db_query("DELETE FROM ".$sql_tbl['source_locations']." WHERE location_id=".$location_id);				
	else
		$location_info = "Sorry!The source location '<b>{$location_name}</b>' can not be deleted as it contains one or more sources!";			
}
/*==============manage source locations======================*/

if($modify_source == 'modify') {
	#Sanitizing input id
	if(preg_match('/^[0-9]*$/',$source_id)){

		if(
			preg_match('/^[0-9]+$/',$type_id)
			&&	preg_match('/^[0-9]+$/',$location_id)
			&& (mysql_real_escape_string($source_name) == $source_name)
			&& (mysql_real_escape_string($source_sync_username) == $source_sync_username)
			&& (mysql_real_escape_string($source_sync_password) == $source_sync_password)
			&& (mysql_real_escape_string($source_manager_email) == $source_manager_email)
		){
			if(check_duplicate_entry($sql_tbl['sources'],array('FIELD'=>'source_id','DATA'=>$source_id),array('FIELD'=>'source_name','DATA'=>$source_name))){
				$salt = rand()%1000;
				$encrypted_pass = $salt.'-'.md5($salt.'-'.md5($source_sync_password));
				$sql = "UPDATE {$sql_tbl['sources']} SET source_type_id = ".$type_id.",location_id = ".$location_id.", source_name='".mysql_real_escape_string($source_name)."' , source_address='".mysql_real_escape_string($source_address)."', source_sync_username='".mysql_real_escape_string($source_sync_username)."', active='".($status == 'on' ?1:0)."' ";
				if(
					($source_sync_password == $source_sync_password2)
					&& (strlen($source_sync_password)>4)
				){
					$sql .= ", source_sync_password='".mysql_real_escape_string($encrypted_pass)."'";
					$sqlcust = "UPDATE {$sql_tbl[customers]} SET password='".addslashes(text_crypt($source_sync_password))."' where login='source_manager_email'";
				}
				$sql .= " WHERE source_id='{$source_id}'";
				db_query($sql);
				func_header_location('manage_order_sources.php');	
			}else{
				$errorcode = "Source name <b>\"{$source_name}\"</b> already exists, source name should be unique!";
			}
		}
		else
	 		$errorcode = " Input not proper. please avoid special charachters like '\"` etc. and Source code length less than 4";
	}
	else
		$errorcode = "something went wrong try again";
}
elseif($add_source == 'add') {
	if($source_sync_password == $source_sync_password2){
		if(
			strlen($source_code) > 4
			&& preg_match('/^[0-9]+$/',$type_id)
			&& preg_match('/^[0-9]+$/',$location_id)
			&& (mysql_real_escape_string($source_code) == $source_code)
			&& (mysql_real_escape_string($source_name) == $source_name)
			&& (mysql_real_escape_string($source_sync_username) == $source_sync_username)
			&& (mysql_real_escape_string($source_sync_password) == $source_sync_password)
			&& (mysql_real_escape_string($source_manager_email) == $source_manager_email)
			){
				if(check_duplicate_entry($sql_tbl['sources'],'',array('FIELD'=>'source_code','DATA'=>$source_code)) && check_duplicate_entry($sql_tbl['sources'],'',array('FIELD'=>'source_name','DATA'=>$source_name)) && check_duplicate_entry($sql_tbl['customers'],'',array('FIELD'=>'login','DATA'=>$source_manager_email)) && check_duplicate_entry($sql_tbl['sources'],'',array('FIELD'=>'source_manager_email','DATA'=>$source_manager_email))){
					$salt = rand()%1000;
					$encrypted_pass = $salt.'-'.md5($salt.'-'.md5($source_sync_password));
					$cust_table_pass = addslashes(text_crypt($source_sync_password));
					
					$sql = "INSERT INTO {$sql_tbl[sources]} (`source_type_id`,`location_id`,`source_code`, `source_name`, `source_address`, `source_sync_username`, `source_sync_password`,`source_manager_email`, `active`, `synchronizable`) VALUES ({$type_id},{$location_id},'".mysql_real_escape_string($source_code)."', '".mysql_real_escape_string($source_name)."', '".mysql_real_escape_string($source_address)."', '".mysql_real_escape_string($source_sync_username)."', '".mysql_real_escape_string($encrypted_pass)."', '".mysql_real_escape_string($source_manager_email)."', '".($active == 'on' ?1:0)."', '1')";
					db_query($sql);

					$sql = "INSERT INTO {$sql_tbl[customers]}(`login`,`usertype`,`password`,`account_type`) VALUES('".mysql_real_escape_string($source_manager_email)."','C','".$cust_table_pass."','PU')";
					db_query($sql);
					func_header_location('manage_order_sources.php');	
				}else{
					$errorcode = "Either <b>\"{$source_code}\"</b> or <b>\"{$source_name}\"</b> or <b>\"{$source_manager_email}\"</b> already exists, source code,email and name should be unique!";
				}
		}
		else
			$errorcode = " Input not proper. please avoid special charachters like '\"` etc. and Source code length less than 4";
	}
	else 
		$errorcode = "Passwords do not match";
}


#
#List Of Locations
#
$sources = _get_sources();//to get all sources
$types = _get_source_types();//to display all source types
$locations = _get_source_locations();//to display all locations
$country = _get_country();//to display list of country
$states = _get_states();//to display list of states

$smarty->assign('sources', $sources);
$smarty->assign('types', $types);
$smarty->assign('locations', $locations);
$smarty->assign('country', $country);
$smarty->assign('states', $states);
$smarty->assign('errorcode',$errorcode);
$smarty->assign('admin_info',$admin_info);
$smarty->assign('location_info',$location_info);

$smarty->assign("main","manage_order_sources");
func_display("admin/home.tpl",$smarty);
?>
