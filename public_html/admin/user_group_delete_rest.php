<?php
require_once  "auth.php"; 
require_once $xcart_dir.'/modules/RestAPI/RestRequest.php';

// First find group menbers and reset them
$url = HostConfig::$userGroupServiceUrl.'/'.$_REQUEST['groupId'];
$method = 'GET';
$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Content-type: application/json';
$headers[] = 'Accept-Language: en-US';
$request = new RestRequest($url, $method);
$request->setHttpHeaders($headers);
$request->execute();
$info = $request->getResponseInfo();
$body = $request->getResponseBody();
$userIds = array();
if($info['http_code'] == 200){
	$response = json_decode($body,true);
	$userList = $response['groupResponse']['data']['UserGroup']['userGroupMapList'];
	foreach ($userList as $key => $value) {
		array_push($userIds, $value['userId']);
	}
}
bulkUpdateGroups($userIds,$_REQUEST['groupId']);

$url = HostConfig::$userGroupServiceUrl.'/?id='.$_REQUEST['groupId'];
$method = 'DELETE';
$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Content-type: application/json';
$headers[] = 'Accept-Language: en-US';
$request = new RestRequest($url, $method);
$request->setHttpHeaders($headers);
$request->execute();
$info = $request->getResponseInfo();
$body = $request->getResponseBody();
if($info['http_code'] == 200){
	$response = json_decode($body,true);
	if($response['groupResponse']['status']['statusMessage'] == 'Deleted'){
		$XCART_SESSION_VARS['deleteGroup'] = true;		
	}
}


function bulkUpdateGroups($userIds,$groupId){
	$url = HostConfig::$notificationsUpsUrl.'/putAttr?method=delete';
	$method = 'POST';
	$data = getBulkIncrementJSON($userIds,$groupId);
	$request = new RestRequest($url, $method, $data);
	$headers = array();
	$headers[] = 'Accept: application/json';
	$headers[] = 'Content-type: application/json';
	$headers[] = 'Accept-Language: en-US';
	$request->setHttpHeaders($headers);
	$request->execute();
	$info = $request->getResponseInfo();
	if($info['http_code'] == 200){
		return true;
	}
	return false;
}

function getBulkIncrementJSON($userIds,$groupId){
	$jsonArr = array();
	for($i=0; $i<count($userIds); $i++){
		$jsonArr[$userIds[$i]] = array();
		$jsonArr[$userIds[$i]]['attribute'] = 'usergroupid';
		$jsonArr[$userIds[$i]]['sourceid'] = '4';
		$jsonArr[$userIds[$i]]['value'] = $groupId;
	}
	return json_encode($jsonArr);
}

header('Location: /admin/user_group_delete.php');

?>
