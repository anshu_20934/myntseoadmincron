<?php
if(!defined("AUTHORIZED_TO_RENDER")){
	die;
}

define('TTF_DIR',$xcart_dir.'/fonts/');
include $xcart_dir."/config/email_feedback_report.config.php";
include ($xcart_dir."/jpgraph/src/jpgraph.php" );
include ($xcart_dir."/jpgraph/src/jpgraph_pie.php");
include ($xcart_dir."/jpgraph/src/jpgraph_pie3d.php");
include ($xcart_dir."/jpgraph/src/jpgraph_bar.php");
include $xcart_dir.'/utils/MailUtil.php';
define('DISPLAY_XPM4_ERRORS', true); // display XPM4 errors

$action=$_GET['action'];
if(!isset($action)){
	$action=$_POST['action'];
}
if(isset($action)&& $action=='genreport'){
	$toBrowser=true;
}else if(isset($action)&& $action=='mail'){
	$toBrowser=false;
}else{
	die;
}
// path to 'MAIL.php' file from XPM4 package
require_once $xcart_dir.'/XPM/MAIL.php';
$attachments=array();
$attach_index=0;
$attach_ids=array();

//Extended AccBarPlot to add percent values display
class ExtAccBarPlot extends AccBarPlot{
	 protected $plots=null,$nbrplots=0;
	 function __construct($plots){
	 	parent::__construct($plots);
	 	$this->plots = $plots;
        $this->nbrplots = count($plots);
	 }
	 
	 function Stroke($img, $xscale, $yscale){
	 	parent::Stroke($img, $xscale, $yscale);
	 	$total_values=array();
	 	$MAX=1;
	 	//Calculation of aggregated sum 
	 	for($i=0;$i< $this->numpoints-1;$i++){
	 		for($j=0; $j < $this->nbrplots; ++$j ){
	 			$total_values[$i]+=$this->plots[$j]->coords[0][$i];
	 		}
	 		if($total_values[$i]>$MAX)
	 				$MAX=$total_values[$i];
	 	}
	 	for($i=0; $i < $this->numpoints-1; $i++) {
	 		$accy = 0;
	 		$accy_neg = 0;
	 		for($j=0; $j < $this->nbrplots; ++$j ) {
	 			$img->SetColor($this->plots[$j]->color);

	 			if ( $this->plots[$j]->coords[0][$i] >= 0) {
	 				$yt=$yscale->Translate($this->plots[$j]->coords[0][$i]+$accy);
	 				$accyt=$yscale->Translate($accy);
	 				$accy+=$this->plots[$j]->coords[0][$i];
	 			}
	 			else {
	 				//if ( $this->plots[$j]->coords[0][$i] < 0 || $accy_neg < 0 ) {
	 				$yt=$yscale->Translate($this->plots[$j]->coords[0][$i]+$accy_neg);
	 				$accyt=$yscale->Translate($accy_neg);
	 				$accy_neg+=$this->plots[$j]->coords[0][$i];
	 			}

	 			$xt=$xscale->Translate($i);

	 			if( $this->abswidth > -1 ) {
	 				$abswidth=$this->abswidth;
	 			}
	 			else {
	 				$abswidth=round($this->width*$xscale->scale_factor,0);
	 			}

	 			$pts=array($xt,$accyt,$xt,$yt,$xt+$abswidth,$yt,$xt+$abswidth,$accyt);

	 			// If value is NULL or 0, then don't draw a bar at all
	 			if ($this->plots[$j]->coords[0][$i] == 0 ) continue;


	 			// CSIM array

	 			$pts[] = $pts[0];
	 			$pts[] = $pts[1];
	 		}
	 		// Draw labels for each acc.bar

	 		$x=$pts[2]+($pts[4]-$pts[2])/2;
	 		if($this->bar_shadow) $x += $ssh;

	 		// First stroke the accumulated value for the entire bar
	 		// This value is always placed at the top/bottom of the bars
	 		if( $accy_neg < 0 ) {
	 			$y=$yscale->Translate($accy_neg);
	 		}
	 		else {
	 			$y=$yscale->Translate($accy);
	 		}

	 		$accy = 0;
	 		$accy_neg = 0;
	 		for($j=0; $j < $this->nbrplots; ++$j ) {

	 			// We don't print 0 values in an accumulated bar plot
	 			if( $this->plots[$j]->coords[0][$i] == 0 ) continue;

	 			if ($this->plots[$j]->coords[0][$i] > 0) {
	 				$yt=$yscale->Translate($this->plots[$j]->coords[0][$i]+$accy);
	 				$accyt=$yscale->Translate($accy);
	 				if(  $this->plots[$j]->valuepos=='center' ) {
	 					$y = $accyt-($accyt-$yt)/2;
	 				}
	 				elseif( $this->plots[$j]->valuepos=='bottom' ) {
	 					$y = $accyt;
	 				}
	 				else { // top or max
	 					$y = $accyt-($accyt-$yt);
	 				}
	 				$accy+=$this->plots[$j]->coords[0][$i];
	 				if(  $this->plots[$j]->valuepos=='center' ) {
	 					$this->plots[$j]->value->SetAlign("center","center");
	 					$this->plots[$j]->value->SetMargin(0);
	 				}
	 				elseif( $this->plots[$j]->valuepos=='bottom' ) {
	 					$this->plots[$j]->value->SetAlign('center','bottom');
	 					$this->plots[$j]->value->SetMargin(2);
	 				}
	 				else {
	 					$this->plots[$j]->value->SetAlign('center','top');
	 					$this->plots[$j]->value->SetMargin(1);
	 				}
	 			} else {
	 				$yt=$yscale->Translate($this->plots[$j]->coords[0][$i]+$accy_neg);
	 				$accyt=$yscale->Translate($accy_neg);
	 				$accy_neg+=$this->plots[$j]->coords[0][$i];
	 				if(  $this->plots[$j]->valuepos=='center' ) {
	 					$y = $accyt-($accyt-$yt)/2;
	 				}
	 				elseif( $this->plots[$j]->valuepos=='bottom' ) {
	 					$y = $accyt;
	 				}
	 				else {
	 					$y = $accyt-($accyt-$yt);
	 				}
	 				if(  $this->plots[$j]->valuepos=='center' ) {
	 					$this->plots[$j]->value->SetAlign("center","center");
	 					$this->plots[$j]->value->SetMargin(0);
	 				}
	 				elseif( $this->plots[$j]->valuepos=='bottom' ) {
	 					$this->plots[$j]->value->SetAlign('center',$j==0 ? 'bottom':'top');
	 					$this->plots[$j]->value->SetMargin(-2);
	 				}
	 				else {
	 					$this->plots[$j]->value->SetAlign('center','bottom');
	 					$this->plots[$j]->value->SetMargin(-1);
	 				}
	 			}
	 			//Dont show value if bar is less than 20 percent of max
	 			if($this->plots[$j]->coords[0][$i]*1.0/($MAX*1.0)>.2){
	 				$this->plots[$j]->value->Show();
	 				$this->plots[$j]->value->Stroke($img,round(($this->plots[$j]->coords[0][$i]*100.0/$total_values[$i]),0)."%,".$this->plots[$j]->coords[0][$i],$x,$y);
	 			}
	 		}

	 	}
	 	return true;
	 }	 
}

function array_to_csv($array,$field){
	$ret="";
	foreach ($array as $value)
		$ret.="'".trim($value[$field])."',";
	return substr($ret, 0,strlen($ret)-1);
}


$time_curr=time();
$time_this_day_start=$time_curr-(24*60*60);



$todays_feedback_count=func_query("SELECT COUNT(DISTINCT feedback_id) AS TODAY_COUNT FROM mk_feedback_data 
where questionid in (".array_to_csv($feedback_report_questions, "questionid").") and feedback_id in 
(SELECT id from mk_order_feedback WHERE recieved_date>=$time_this_day_start and recieved_date<=$time_curr and id!=5)");
$todays_feedback_count=$todays_feedback_count[0]['TODAY_COUNT'];
$total_feedback_count=func_query("SELECT COUNT(DISTINCT feedback_id) AS TOTAL_COUNT FROM mk_feedback_data 
where questionid in (".array_to_csv($feedback_report_questions, "questionid").") and feedback_id!=5");
$total_feedback_count=$total_feedback_count[0]['TOTAL_COUNT'];

$contents= '<div align="center" style="width:100%">
<table width="80%" border=1 style="border-width: 4px;border-spacing: 2px;border-style: outset;
	border-color: black;border-collapse: collapse;">
<tr align="center" >
<th style="width:10%">S.No.</th>
<th style="width:25%">Query</th>
<th style="width:30%">Today</th>
<th style="width:30%">Life Time Value</th>
</tr>';
$contents.='<tr align="center"  >
<th style="width:2%"></th>
<th style="width:10%">No. of Feedbacks</th>
<th style="width:40%">'.$todays_feedback_count.'</th>
<th style="width:40%">'.$total_feedback_count.'</th>
</tr>';
$questions= func_query("SELECT id,question FROM mk_feedback_questions WHERE id in(".array_to_csv($feedback_report_questions, "questionid").")");


foreach($questions as $index=>$ques){
	foreach($feedback_report_questions as $rep_ques){
		if($rep_ques['questionid']==$ques['id']){
			$questions[$index]['graph_type']=$rep_ques['graph_type'];
			$questions[$index]['refquestion']=$rep_ques['refquestion'];
			$questions[$index]['display']=$rep_ques['display'];
			break;
		}
	}
}

$sno=1;
foreach ( $questions as $q_index=> $value){
	$contents.="<tr>";
	$contents.="<td >$sno</td>";
	$contents.="<td >";
	$contents.="<b>".htmlSafeName($value['display'])."</b>";
	$contents.="<table style=\"width:100%;\">";
	$options = func_query("SELECT id,optionname FROM mk_feedback_options WHERE questionid=".$value['id']);
	
	$opt_count=0;
	
	$response_list=array_to_csv($options,"optionname" );
	foreach($options as $opt_value){
		$contents.="<tr><td style='width:2%'>".constructLeagendEmbeddedImageTag($colors,$opt_count++)."</td><td>".htmlSafeName($option_display_labels[$opt_value['id']])."  </td></tr>";
		
	}
	$properties['setshadow']=true;
	$contents.="</table></td>";
	if($value['graph_type']=='P'){
		$result_today=func_query("SELECT response ,count(response) as res_count from mk_feedback_data 
		 WHERE questionid=".$value['id']."
		  and feedback_id in (SELECT id from mk_order_feedback WHERE recieved_date>=$time_this_day_start and recieved_date<=$time_curr and id!=5) 
		  GROUP BY response ");
		$result_LT=func_query("SELECT response,count(response) as res_count from mk_feedback_data WHERE 
		questionid=".$value['id']." and feedback_id!=5 GROUP BY response ");
		$index=0;
		$data_sum=0;
		$data = array();
		foreach($options as $opt_value){
			$data[$index]=0;
			foreach($result_today as $res_val){
				if(trim($res_val['response'])==trim($opt_value['optionname'])){
					$data[$index]=$res_val['res_count'];
					break;
				}
			}
			$data_sum+=$data[$index];
			$index++;
		}
		$lables= array();
		foreach($options as $key=>$opt_value){
			$lables[$key]="(%.0f%%)".truncate($option_display_labels[$opt_value['id']],10,2).",".$data[$key];
		}
		$properties['lables']=$lables;
		if($data_sum>0){
			$contents.="<td>".constructPNGEmbeddedImageTag(buildPiePlot($data, $properties))."</td>";
		}else{
			$contents.="<td align=center>No Data Available</td>";
		}
		$index=0;
		$data_sum=0;
		$data = array();
		foreach($options as $opt_value){
			$data[$index]=0;
			foreach($result_LT as $res_val){
				if(trim($res_val['response'])==trim($opt_value['optionname'])){
					$data[$index]=$res_val['res_count'];
					break;
				}
			}
			$data_sum+=$data[$index];
			$index++;
		}
		$lables= array();
		foreach($options as $key=>$opt_value){
			$lables[$key]="(%.0f%%)".truncate($option_display_labels[$opt_value['id']],10,2).",".$data[$key];
		}
		$properties['lables']=$lables;
		if($data_sum>0){
			$contents.="<td>".constructPNGEmbeddedImageTag(buildPiePlot($data, $properties))."</td>";
		}else{
			$contents.="<td align=center>No Data Available</td>";
		}
		$contents.="</tr>";
	}
	else if($value['graph_type']=='B'){
		$options_lab = func_query("SELECT id,optionname FROM mk_feedback_options WHERE questionid=".$value['refquestion']);
		$lables= array();
		$response_list="";
		$opt_count=0;
		foreach($options_lab as $opt_value){
			$lables[$opt_count]=truncate($option_display_labels[$opt_value['id']],10,2);
			$response_list.="'".trim($opt_value['optionname'])."',";
			$opt_count++;
		}
		$response_list=substr($response_list, 0,strlen($response_list)-1);
		$properties['lables']=$lables;
		$data_today= array();
		$data_LT= array();
		$data_index=0;
		foreach ($options as $opt_value){
			$result_today=func_query("SELECT b.response as response,count(b.response) as res_count from mk_feedback_data a, 
			mk_feedback_data b WHERE TRIM(a.response)='".trim($opt_value['optionname'])."' and b.feedback_id=a.feedback_id 
			and a.questionid=".$value['id']." and b.questionid=".$value['refquestion']." 
			and   a.feedback_id in (SELECT id from mk_order_feedback 
			WHERE recieved_date>=$time_this_day_start and recieved_date<=$time_curr and id!=5) group by b.response ");
			
			$result_LT=func_query("SELECT b.response as response,count(b.response) as res_count from mk_feedback_data a,
			 mk_feedback_data b WHERE a.questionid=".$value['id']." and b.questionid=".$value['refquestion']." 
			 and TRIM(a.response)='".trim($opt_value['optionname'])."' and b.feedback_id=a.feedback_id and b.feedback_id!=5  
			   group by b.response ");
			$index=0;
			$data_today[$data_index]=array();
			foreach($options_lab as $opt_value){
				$data_today[$data_index][$index]=0;
				foreach($result_today as $res_val){
					if(trim($res_val['response'])==trim($opt_value['optionname'])){
						$data_today[$data_index][$index]=$res_val['res_count'];
						break;
					}
				}
				$index++;
			}
				
			$index=0;
			$data_LT[$data_index]=array();
			foreach($options_lab as $opt_value){
				$data_LT[$data_index][$index]=0;
				foreach($result_LT as $res_val){
					if(trim($res_val['response'])==trim($opt_value['optionname'])){
						$data_LT[$data_index][$index]=$res_val['res_count'];
						break;
					}
				}
				$index++;
			}
			$data_index++;
		}
		$contents.="<td>".constructPNGEmbeddedImageTag(buildGraphPlot($data_today, $properties))."</td>";
		$contents.="<td>".constructPNGEmbeddedImageTag(buildGraphPlot($data_LT, $properties))."</td></tr>";
	}
	
	$sno++;
}

$contents.="</table></div><div>";
$contents.='<div align="center" style="width:100%">'."
<div style='width: 80%;height: 1%'></div><span style='width: 80%;height: 200px;' > <h3>Today's Comments</h3></span>".
'<table width="80%" border=1 style="border-width: 4px;border-spacing: 2px;border-style: outset;
	border-color: black;border-collapse: collapse;">
<tr align="center" >
<th style="width:10%">S.No.</th>
<th style="width:25%">Email ID</th>
<th style="width:45%">Comments</th>
<th style="width:10%">Age</th>
<th style="width:10%">Gender</th>
</tr>';

$rep_details=func_query("SELECT feedback_id,questionid,response FROM mk_feedback_data where questionid
 in (".$table_report['comment_ques_id'].",".$table_report['email_ques_id'].",".$table_report['age_ques_id'].
 ",".$table_report['gender_ques_id'].",".$table_report['intrest_ques_id'].") and feedback_id in (SELECT id from mk_order_feedback WHERE recieved_date>=$time_this_day_start and recieved_date<=$time_curr )
  ORDER BY feedback_id");

$comment_data=array();
foreach($rep_details as $rep_details_value){
	$comment_data[$rep_details_value['feedback_id']][$rep_details_value['questionid']]['response']=htmlSafeName($rep_details_value['response']);
}

$index_array;
$temp;
$sno=1;
$groups=array();
$color_code=array('#FFC34D','#E7E3E0');
foreach($comment_data as $details){
	if(($details[$table_report['email_ques_id']]['response']==null||trim($details[$table_report['email_ques_id']]['response'])=="")
	&&($details[$table_report['comment_ques_id']]['response']==null||trim($details[$table_report['comment_ques_id']]['response'])=="" )&&
	 ($details[$table_report['age_ques_id']]['response']==null||trim($details[$table_report['age_ques_id']]['response'])=="") &&
	 ($details[$table_report['gender_ques_id']]['response']==null || trim($details[$table_report['gender_ques_id']]['response'])==""))
	 continue;
	if(!$details[$table_report['intrest_ques_id']]['response']){
		$grp="Others";
	}else{
		$grp=$details[$table_report['intrest_ques_id']]['response'];
	}
	$groups[$grp]['sno']++;
	$groups[$grp]['text'].="<tr >
			<td align=center>".$groups[$grp]['sno']."</th>
			<td align=center>".$details[$table_report['email_ques_id']]['response']."</th>
			<td align=center>".$details[$table_report['comment_ques_id']]['response']."</th>
			<td align=center>".$details[$table_report['age_ques_id']]['response']."</th>
			<td align=center>".$details[$table_report['gender_ques_id']]['response']."</th>
			</tr>";
	
}
$index=0;
foreach ($groups as $g=>$details){
	$index = 1-$index;
	$contents.="<tr><th colspan=5 BGCOLOR=".$color_code[$index]." >&nbsp;</th></tr><tr><th colspan=5 BGCOLOR=".$color_code[1-$index]." >$g</th></tr>".$details['text'];
}

$contents.="</table></div>";


$m = new MAIL;

// set from address and name
$m->From($email_config['from']['emailid'], $email_config['from']['name']);

$mailIdIterator = new MailIDEnumerator(EMAIL_CUST_FEEDBACK_TO_EMAILS);
while($mailIdIterator->hasMoreElements()){
	$idname=$mailIdIterator->nextElement();
	$m->addTo($idname['id'], $idname['name']);
}

// set subject
$m->Subject("User Feedback Digest: ".date("d/m/y",$time_curr));
$m->Html($contents);
foreach ($attach_ids as $k=>$value){
	$m->Attach($attachments[$k], 'image/png', "$k.png",null , null, 'inline', $value);
}

if($toBrowser){
	$contents="<head><title>User Feedback Digest</title></head>
	<html><div style='position:absolute; left:0px; top:0px; width:100%; height:100%; overflow:auto'>".$contents."</div></html>";
	echo $contents;
}
else// send mail
	echo $m->Send('client') ? 'Mail sent !' : 'Error !';

function buildPiePlot($data,$properties){
	try{
		
	$graph  = new PieGraph ($properties['width'],$properties['height']);
	if($properties['setshadow'])
		$graph->SetShadow();
		
	$graph->title-> Set($properties['title']);

	$p1  = new PiePlot($data);
	$p1->SetSliceColors($properties["slicecolors"]);
	$p1->SetSize(.27);
    $p1->value->SetColor('black');
	$p1->value->SetFont(FF_ARIAL,FS_BOLD,7);
	$p1->value->SetColor("black");
	$p1->SetLabels($properties['lables']);
	$p1->SetLabelPos(1);
	$p1->SetShadow();
	$p1->ShowBorder(true,true);
	$graph->Add( $p1);
	$graph->SetBackgroundGradient('lightsteelblue','white',GRAD_HOR ,BGRAD_MARGIN);
	$graph->img->SetImgFormat('png');
	
	$handle=$graph->Stroke(_IMG_HANDLER);
	ob_start();
	imagepng($handle);
	$contents =  ob_get_contents();
	ob_end_clean();
	//fclose($handle);
	return $contents;
		
	}catch(Exception $e){
		return "Graph not available";
	}
}

function buildGraphPlot($data,$properties){
	try{

		$graph  = new Graph ($properties['width'],$properties['height']);
		if($properties['setshadow'])
		$graph->SetShadow();

		$graph->title-> Set($properties['title']);
		$graph->SetScale("textint");

		$graph->img->SetMargin(40,30,20,40);
		$graph->xaxis->SetTickLabels($properties['lables']);
		$plot_array=array();
		foreach ($data as $k=>$value){
			$bplot = new BarPlot($value);
			$bplot->SetFillColor($properties['slicecolors'][$k]);
			$plot_array[$k]=$bplot;
			$bplot->SetFillGradient($properties['slicecolors'][$k],"lightsteelblue",GRAD_VERT);
			$bplot->SetWidth(1);
			$bplot->valuepos="center";
			$bplot->value->SetFont(FF_ARIAL,FS_BOLD,7);
			$bplot->value->SetColor("black");

		}
		$graph->SetBackgroundGradient('lightsteelblue','white',GRAD_HOR ,BGRAD_MARGIN);
		$graph ->xaxis->SetFont(FF_ARIAL,FS_BOLD,7);
		$graph ->yaxis->hide_labels=true;
		$graph->img->SetImgFormat('png');
		
		// Create the accumulated bar plot
		$gbplot = new ExtAccBarPlot($plot_array);
		$graph->Set90AndMargin();
		// Add the accumulated plot to the graph
		$graph->Add($gbplot);

		$handle=$graph->Stroke(_IMG_HANDLER);
		ob_start();
		imagepng($handle);
		$contents =  ob_get_contents();
		ob_end_clean();

		//fclose($handle);
		return $contents;

	}catch(Exception $e){
		return "Graph not Available";
	}
}



function constructPNGEmbeddedImageTag($contents){

	global $attachments,$attach_index,$attach_ids,$toBrowser;
	$id = MIME::unique()."-$attach_index";
	$attach_ids[$attach_index]=$id;
	$attachments[$attach_index++]=$contents;
	if($toBrowser)
		return "<img src='data:image/png;base64,".base64_encode($contents)."' />";
	return "<img src=\"cid:$id\" />";
}
function constructLeagendEmbeddedImageTag($colors,$color){
	$width = 10;
    $height = 10; 
	$image = ImageCreate($width, $height); 
	loadColors($image,$colors); 
	ImageFill($image, 0, 0, $color); 
	ob_start();
    imagepng($image);
    $contents =  ob_get_contents();
    ob_end_clean();
    global $attachments,$attach_index,$attach_ids,$toBrowser;
	$id = MIME::unique()."-$attach_index";
	$attach_ids[$attach_index]=$id;
	$attachments[$attach_index++]=$contents;
	if($toBrowser)
		return "<img src='data:image/png;base64,".base64_encode($contents)."' />";
	return "<img src=\"cid:$id\" />";
}

function loadColors($image,$colors){
	foreach ( $colors as $value){
		ImageColorAllocate($image, $value[0], $value[1], $value[2]); 
	}
}

function truncate($phrase, $limit = 21, $dots = 3) {
    if (strlen($phrase) >= $limit) {
        $phrase = substr($phrase, 0, $limit - $dots);
        for ($i = 0; $i < $dots; $i++) {
            $phrase .= '.';
        }
    }
    return $phrase;
}

function htmlSafeName($str){
	str_replace("<", "&gt;", $str);
	str_replace(">", "&lt;", $str);
	return $str;
}

?>