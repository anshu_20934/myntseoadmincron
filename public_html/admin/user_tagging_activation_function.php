<?php
include_once($xcart."/include/func/func.db.php");
//get timestamp from a date.. date is in format DD-MM-YYYY-HR-MIN-SEC 
function getTimeStampFromDate($date,$pos){
	if($pos == 0) {$val = 0;$hval=0;}
	elseif($pos == 1) {$hval = 23;$val=59;}

	list( $day,$month,$year,$hr,$min,$sec) = explode('-',$date);
	
	if($hr == NULL){ $hr = $hval;}
	if($min == NULL){ $min = $val;}
	if($sec == NULL){ $sec = $val;}
	/*echo "$day,$month,$year,$hr,$min,$sec";
	echo "      ";*/
	$ts =  mktime($hr, $min,$sec, $month, $day, $year);
	return $ts;
}

function inputValidation($input){	
	$err[0] = true;
	$err[1] = "";
	$pattern = '\'^[0-9][0-9]?-[0-9][0-9]?-[0-9]{4}(-[0-9][0-9]?-[0-9][0-9]?-[0-9][0-9]?)?$\'';
	if(!preg_match($pattern, $input['start_time'])){
		$err[1] = $err[1]."Wrong start_time<br>";
		//$smarty->assign("error",$err);
		$err[0] = false;
	}
	if(!preg_match($pattern, $input['end_time'])){
		$err[1] = $err[1]."Wrong end_time<br>";
		//$smarty->assign("error",$err);
		$err[0] = false;
	}
	if(!preg_match($pattern, $input['display_start_time'])){
		$err[1] = $err[1]."Wrong display_start_time<br>";
		//$smarty->assign("error",$err);
		$err[0] = false;
	}
	if(!preg_match($pattern, $input['display_end_time'])){
		$err[1] = $err[1]."Wrong display_end_time<br>";
		//$smarty->assign("error",$err);
		$err[0] = false;
	}
	if(!preg_match('\'^(list|rule|global)$\'', $input['rule'])){
		$err[1] = $err[1]."Rule can have only two values: list or rule";
		$err[0] = false;
	}
	return $err;
	
}
function setHappyHourLeaderBoard($input){
	$input['name'] = mysql_real_escape_string($input['name']);
	$input['start_time'] = mysql_real_escape_string($input['start_time']);
	$input['end_time'] = mysql_real_escape_string($input['end_time']);
	$input['display_start_time'] = mysql_real_escape_string($input['display_start_time']);
	$input['display_end_time'] = mysql_real_escape_string($input['display_end_time']);
	$input['rule'] = mysql_real_escape_string(trim($input['rule']));
	$input['rule_string'] = mysql_real_escape_string(trim($input['rule_string']));
	$input['ticker_msg'] = mysql_real_escape_string(trim($input['ticker_msg']));
	$input['title'] = mysql_real_escape_string(trim($input['title']));
	$input['banner_url'] = mysql_real_escape_string(trim($input['banner_url']));
	$input['banner_href'] = mysql_real_escape_string(trim($input['banner_href']));
	$input['refresh_duration'] = mysql_real_escape_string($input['refresh_duration']);
	$input['type'] = mysql_real_escape_string(trim($input['type']));
	$input['qualifying_amount'] = mysql_real_escape_string(trim($input['qualifying_amount']));
	$input['qualifying_statement'] = mysql_real_escape_string(trim($input['qualifying_statement']));
	$input['post_qualifying_statement'] = mysql_real_escape_string(trim($input['post_qualifying_statement']));


	$check = inputValidation($input);
	if(!$check[0]){
		return $check;
	}

	$insert_query = "insert into leader_board (name,start_time,end_time,display_start_time,display_end_time,is_enabled,created_by,created_on,refresh_duration,type,qualifying_amount, qualifying_statement, post_qualifying_statement) values (\"".$input['name']."\",".getTimeStampFromDate($input['start_time'],0).",".getTimeStampFromDate($input['end_time'],1).",".getTimeStampFromDate($input['display_start_time'],0).",".getTimeStampFromDate($input['display_end_time'],1).",1,\"".$input['created_by']."\",".time(). ", " .$input['refresh_duration']. ", \"" . $input['type'] ."\", " . $input['qualifying_amount'] . ", \""  . $input['qualifying_statement'] . "\", \" " . $input['post_qualifying_statement'] . "\")";
	/*echo $insert_query;
	exit();*/
	$result = db_query($insert_query);
	/*echo "par";
	exit();*/
	//$get_query = "select id from leader_board where name = \"".$input['name']."\"";
	//$id = func_query_first_cell($get_query);
	$id = db_insert_id();
	//$id = 5;
	if(empty($input['rule_string'])){
		$input['rule_string'] = "null";
	}
	$insert_query = "insert into leader_board_criteria (lb_fk_id,rule,rule_string) values (".$id.",\"".$input['rule']."\",\"".$input['rule_string']."\")";
	//echo $insert_query;
	$result = db_query($insert_query);

	$ticker_insert_query = "insert into leader_board_ticker_messages (lb_fk_id,message,title,banner_url,banner_href) values (" .$id. ",\"" .$input['ticker_msg']. "\",\"" .$input['title']. "\",\"" . $input['banner_url'] . "\",\"" . $input['banner_href'] ."\")";
	$result = db_query($ticker_insert_query);

	//exit();
	$ret_val[0] = true;
	$ret_val[1] = $id;
	return $ret_val;
}
?>