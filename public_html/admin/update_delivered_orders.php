<?php
require "./auth.php";
include_once("$xcart_dir/include/security.php");
include_once("$xcart_dir/include/func/func.courier.php");
include_once("$xcart_dir/include/func/func.order.php");
require_once("$xcart_dir/PHPExcel/PHPExcel.php");
include_once("$xcart_dir/PHPExcel/PHPExcel/Style/NumberFormat.php");
include_once("$xcart_dir/modules/apiclient/WarehouseApiClient.php");
include_once("$xcart_dir/include/class/oms/EventCreationManager.php");
include_once("$xcart_dir/include/class/oms/OrderProcessingMailSender.php");

$action = $_POST['action'];
if(!$action)
	$action = 'view';

if($REQUEST_METHOD == "POST") {
	if($action == 'search') {
		$courier = $_POST['courier_service'];
		$payment_method = $_POST['payment_method'];
		$orderids = $_POST['orderids'];
		$from_date = $_POST['from_date'];
		$warehouse_id = $_POST['warehouse_id'];
		$from_date_break = explode("/",$from_date);
		$final_from_date = $from_date_break[2]."-".$from_date_break[0]."-".$from_date_break[1];
		
		$to_date = $_POST['to_date'];
		$to_date_break = explode("/",$to_date);
		$final_to_date = $to_date_break[2]."-".$to_date_break[0]."-".$to_date_break[1];
		
		$query = "SELECT SQL_CALC_FOUND_ROWS orderid, login, total, date, shippeddate, s_firstname as name, s_address, s_city, s_state, s_country, s_zipcode, 
		  		   mobile, cash_coupon_code, cash_redeemed, coupon, coupon_discount, subtotal as actualamount, warehouseid,
		  		   payment_method, courier_service, tracking from xcart_orders where 
				   (((status='SH' || status='L') and (delivereddate is null or delivereddate = 0)) or (status='L' and (delivereddate is not null or delivereddate != 0))) 
				   and shippeddate is not null";
		
		if(!empty($payment_method)) {
			if($payment_method == 'cod')
				$query .= " and payment_method = 'cod'";
			else if($payment_method == 'on')
				$query .= " and payment_method != 'cod'";
		}
		
		if(!empty($warehouse_id))
			$query .= " and warehouseid = $warehouse_id";
		
		if(!empty($courier)) {
			$query .= " and courier_service = '$courier'";
		}
		
		$query .= " and shippeddate >= unix_timestamp('$final_from_date')";
		$query .= " and shippeddate <= unix_timestamp('$final_to_date 23:59:59')";
		if(!empty($orderids)) {
			$query .= " and orderid in ($orderids)";
		}
		
		if(!empty($request["sort"])) {
			$sql .= " ORDER BY " . $_POST["sort"];
			$sql .= " " . ($_POST["dir"] == 'DESC' ? "DESC" : "ASC");
		}
		
		$start = $_POST['start'] == null ? 0 : $_POST['start'];
		$limit = $_POST['limit'] == null ? 30 : $_POST['limit'];
		$query .=" LIMIT $start, $limit";
		
		$sqllog->info("update_delivered_orders.php : Get Orders - $query");
		$results = func_query($query, TRUE);
		
		foreach ($results as $index=>$order) {
			$results[$index]['shipping_address'] = $order['s_address'].",<br>".$order['s_city'].", ".$order['s_state'].
											" - ".$order['s_zipcode']."<br>".$order['s_country'];
			$results[$index]['shippeddate'] = date('M jS Y', $order['shippeddate']);
			$results[$index]['date'] = date('M jS Y', $order['date']);
		}
		
		$total = func_query("SELECT FOUND_ROWS() total", TRUE);
		
		$returnObj = array("results" => $results, "count" => $total[0]["total"]);
		header('Content-type: text/x-json');
		print json_encode($returnObj);
		exit;	
	} else if($action == "markdelivered") {
		$orderids = $_POST['orderids'];	
		
		$online_orders = "Select orderid from $sql_tbl[orders] where payment_method != 'cod' and orderid in ($orderids)";
		$online_orders_results = func_query($online_orders);
		$online_orderIds = array();
		foreach ($online_orders_results as $row){
			$online_orderIds[] = $row['orderid'];
		}
		
		$orderids = explode(",", $orderids);
		$cod_orderIds = array_unique(array_diff($orderids, $online_orderIds));
		if(!empty($cod_orderIds))
			db_query("update xcart_orders set cod_pay_status='delivered' where orderid in (".implode(",", $cod_orderIds).")");
		
		func_change_order_status($orderids, 'DL', $_POST['user'], 'Marked Delivered Via Bulk Operation');
		func_change_order_status($online_orderIds, 'C', $_POST['user'], 'Marked Completed Via Bulk Operation');
		
		/* $currenttime = time();
		foreach($orderids as $orderid) {
			OrderProcessingMailSender::send_shipment_status_update_mail($orderid, "successful_delivery", date('Y-m-d',$currenttime)."T".date('H:i:s',$currenttime)."+05:30", false,  false);
		} */
		
		$returnObj = "SUCCESS";
		header('Content-type: text/x-json');
		print json_encode($returnObj);
		exit;	
	} else if($action == 'verifybulkdeliveredupdate') {
		
		if(!isset($_FILES) && isset($HTTP_POST_FILES))
        	$_FILES = $HTTP_POST_FILES;

	    if(!empty($_FILES['orderslistfile']['name'])) {
	      	$uploaddir1 = '../bulkorderupdates/';
	        $extension = explode(".",basename($_FILES['orderslistfile']['name']));
	        $uploadfile = $uploaddir1 . "markordersdelivered-".date('d-m-Y-h-i-s').".".$extension[1];
	        if (move_uploaded_file($_FILES['orderslistfile']['tmp_name'], $uploadfile)) {
	          	$uploadStatus = true;
	        }
	    }
	    
	    $errorMsg = "";
	    if($uploadStatus)
    	{
    		$orderId2TimeStamp = array();
    		$objReader = PHPExcel_IOFactory::createReader('Excel5');
            $objPHPExcel = $objReader->load($uploadfile);
 	    	$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
 	    	for($i=2; $i<=$lastRow; $i++){//start from second row ignore heading
	   	    	$cell1Obj = ($objPHPExcel->getActiveSheet()->getCell("A".$i));
	            $orderid = trim($cell1Obj->getvalue());
	            if($orderid == "")
	            	continue;
	            $cell2Obj = ($objPHPExcel->getActiveSheet()->getCell("B".$i));
	            $date = trim($cell2Obj->getvalue());
	            if($date != "") {
	            	$weblog->info("Date for order - $orderid = $date");
	            	$date = PHPExcel_Style_NumberFormat::toFormattedString($date, "YYYY-M-D H:MM:SS");
	            	$timestamp = strtotime($date);
	            	if(!$timestamp)
	            		$timestamp = time();
	            } else {
	            	$weblog->info("Date for order - $orderid in not present");
	            	$timestamp = time();
	            }
	            $prevYear = date("Y")-1;
	            $minTime = strtotime("$prevYear-12-01 00:00:00");
	            $maxTime = time();
	            if($timestamp < $minTime || $timestamp > $maxTime) {
	            	$errorMsg .= "OrderId $orderid has invalid delivered timestamp.<br>";
	            } else {
	            	$orderId2TimeStamp[$orderid] = $timestamp;
	            }
 	    	}
    	} else {
    		$errorMsg .= "Failed to upload file. Please try again later.";
    	}
    	
    	if($errorMsg == "") {
    		if(count($orderId2TimeStamp) > 0) {
    			$orderids = array_keys($orderId2TimeStamp);
    			
    			//check is all orders are in shipped state only
    			$invalid_orders_sql = "select orderid from $sql_tbl[orders] where orderid in (".implode(",", $orderids).") and status != 'SH'";
    			$invalid_orders_result = func_query($invalid_orders_sql);
    			
    			if(!$invalid_orders_result) {
		    		
    				$progressKey = $HTTP_POST_VARS['progresskey'];
    				$orders = "Select orderid, payment_method, shippeddate from $sql_tbl[orders] where orderid in (".implode(",", $orderids).")";
				$orders_results = func_query($orders, true);
				$online_orderIds = array();
				foreach ($orders_results as $row){
                                    $orderIdVsShippedDate = array($row['orderid'] => $row['shippeddate']);
                                    if($row['payment_method'] != 'cod') {
                                        $online_orderIds[] = $row['orderid'];
                                    }
                                }
                                        
					$xcache = new XCache();
					if($progressKey) {
						$progressinfo = array('total'=>count(array_keys($orderId2TimeStamp)), 'completed'=>0);
						$xcache->store($progressKey."_progressinfo", $progressinfo, 86400);
					}
					$count = 0;
		    		foreach ($orderId2TimeStamp as $orderid=>$timestamp) {
		    			$new_status = 'DL';
		    			$is_online = in_array($orderid, $online_orderIds);
		    			if($is_online) {
		    				$new_status = 'C';
		    			}
		    			$count++;
	    				$progressInfo = $xcache->fetch($progressKey."_progressinfo");
	    				$progressInfo['completed'] = $count;
	    				$xcache->store($progressKey."_progressinfo", $progressInfo, 86400);
		    			
		    			$currenttime = time();
		    			
			    		$update_sql = "Update $sql_tbl[orders] set status = '$new_status', delivereddate = IF((shippeddate < $timestamp), $timestamp, $currenttime)";
		    			if($is_online) {
		    				$update_sql .= ", completeddate = IF((shippeddate < $timestamp), $timestamp, $currenttime)";
		    			} else {
		    				$update_sql .= ", cod_pay_status = 'delivered'";
		    			}
			    		$update_sql .= " where orderid = $orderid";
					db_query($update_sql);
						
					$update_sql = "Update $sql_tbl[order_details] set item_status = 'D' where orderid = $orderid and item_status != 'IC'";
					db_query($update_sql);
						
					func_addComment_status($orderid, 'DL', 'SH', 'Marked Delivered Via Bulk Operation', $_POST['user']);
                                        if (empty($timestamp) || $timestamp < $orderIdVsShippedDate[$orderid]) {
                                            $timestamp = $currenttime;
                                        }
                                        
					EventCreationManager::pushReleaseStatusUpdateEvent($orderid, 'DL', 'Marked Delivered Via Bulk Operation', $timestamp, $_POST['user']);
						if($is_online) {
							//EventCreationManager::pushReleaseStatusUpdateEvent($orderid, 'C', 'Marked Completed Via Bulk Operation', $timestamp, $_POST['user']);
							func_addComment_status($orderid, 'C', 'DL', 'Marked Completed Via Bulk Operation', $_POST['user']);
						}
						
						//OrderProcessingMailSender::send_shipment_status_update_mail($orderid, "successful_delivery", date('Y-m-d',$currenttime)."T".date('H:i:s',$currenttime)."+05:30", false,  false);
						
		    		}
		    		$xcache->remove($progressKey."_progressinfo");
	    			$smarty->assign("message", "Updated ".count($orderids)." orders successfully");
    			} else {
    				$invalidOrderIds = array();
    				foreach($invalid_orders_result as $order) 
    					$invalidOrderIds[] = $order['orderid'];
    				$smarty->assign("errormessage", "Following orders are not in shipped status - ". implode(",", $invalidOrderIds));
    			}
    		} else {
    			$smarty->assign("errormessage", "No orders present to update.");
    		}
    	} else {
    		$smarty->assign("errormessage", $errorMsg);
    	}
	}
}

$couriers = get_supported_courier_partners();
$payment_methods = array(
	array("id"=>"all", "name"=>"ALL"),
	array("id"=>"on", "name"=>"Online"),
	array("id"=>"cod", "name"=>"COD")
);

$warehouse_results = WarehouseApiClient::getAllWarehouses();
$warehouses = array();
foreach($warehouse_results as $row) {
	$warehouses[] = array('id'=>$row['id'], 'display_name'=>$row['name']);
}
$smarty->assign("warehouses", json_encode($warehouses));

$uniqueId = uniqid();
$smarty->assign('progresskey', $uniqueId);
$smarty->assign("action", $action);
$smarty->assign("couriers", json_encode($couriers));
$smarty->assign("payment_methods", json_encode($payment_methods));
$smarty->assign("main", "update_delivered_orders");
func_display("admin/home.tpl", $smarty);
?>