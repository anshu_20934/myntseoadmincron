<?php
chdir(dirname(__FILE__));

require_once "../auth.php";
require_once $xcart_dir."/include/security.php";
include_once "$xcart_dir/include/class/netbankingHelper.php";

if($REQUEST_METHOD == "POST") {
	$_time = time();
	if($mode=='add') {
		if(!empty($gateway_name_new)&&isset($gateway_weight_new)) {
			$gateway_name_new = mysql_escape_string(trim($gateway_name_new));
			$gateway_weight_new = mysql_escape_string(trim($gateway_weight_new));
			
			func_query("insert into mk_netbanking_gateways (`name`,`weight`) values ('$gateway_name_new', $gateway_weight_new)");
			NetBankingHelper::logNetBankingChangeActivity($login, $mode,'',$gateway_name_new,"gateway weight set to -$gateway_weight_new");
		}
	} elseif($mode=='delete') {
		if(!empty($gateway_id)) {
			$gateway = func_query_first("select name,weight from mk_netbanking_gateways where id=$gateway_id");
			$gateway_name = $gateway['name'];
			$gateway_weight = $gateway['weight'];
			func_query("delete from mk_netbanking_gateways where id=$gateway_id");
			func_query("delete from mk_netbanking_mapping where gateway_id=$gateway_id");
			NetBankingHelper::logNetBankingChangeActivity($login, $mode,'',$gateway_name,"gateway weight-$gateway_weight all mapping for this gateway also deleted");
		}		
	} elseif($mode=='update') {
		$updatenamearray = $_POST['update_name_field'];
		$update_weight_field = $_POST['update_weight_field'];
		foreach($updatenamearray AS $id=>$gateway_name) {
			$weight = $update_weight_field[$id];
			if(!empty($gateway_name)&&!empty($weight)) {
				$name = mysql_escape_string(trim($gateway_name));
				$weight = mysql_escape_string(trim($weight));
				func_query("update `mk_netbanking_gateways` set `name`='$name',`weight`=$weight where id=$id");
				NetBankingHelper::logNetBankingChangeActivity($login, $mode,'',$name,"gateway weight updated to $weight");				
			}
		}
	} else if($mode =='clearCache') {
		NetBankingHelper::clearCachedValues();
	}	
	func_header_location($http_location . "/admin/netbanking/add_gateway.php");	
} else {
	
	$gateway_list = func_query("select id,name,weight from mk_netbanking_gateways");
	
	$smarty->assign("gateway_list", $gateway_list);
	$smarty->assign("action_php","add_gateway.php");
	$smarty->assign("main","netbanking_add_gateway");
	func_display("admin/home.tpl",$smarty);	
}

?>