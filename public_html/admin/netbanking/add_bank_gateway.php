<?php
chdir(dirname(__FILE__));

require_once "../auth.php";
require_once $xcart_dir."/include/security.php";
include_once "$xcart_dir/include/class/netbankingHelper.php";

if($REQUEST_METHOD == "POST") {
	if(empty($bank_id)) {
		func_header_location($http_location . "/admin/netbanking/bank_list.php");
	}
	$_time = time();
	if($mode=='add') {
		if(!empty($gateway_bank_code_new)) {
			$gateway_bank_code_new = mysql_escape_string(trim($gateway_bank_code_new));
			$bank_name = func_query_first_cell("select name from mk_netbanking_banks where id=$bank_id");
			$gateway_name = func_query_first_cell("select name from mk_netbanking_gateways where id=$gateway_value_new");
			func_query("insert into mk_netbanking_mapping (`bank_id`,`gateway_id`,`gateway_code`) values ($bank_id, $gateway_value_new,'$gateway_bank_code_new')");
			NetBankingHelper::logNetBankingChangeActivity($login, $mode,$bank_name,$gateway_name,"gateway code added $gateway_bank_code_new");
		}
	} elseif($mode=='delete') {
		if(!empty($mapping_id)) {
			$result = func_query_first("select bank_id,gateway_id,gateway_code from mk_netbanking_mapping where id=$mapping_id");
			$bank_id = $result['bank_id'];
			$gateway_id = $result['gateway_id'];
			$gateway_code = $result['gateway_code'];
			$bank_name = func_query_first_cell("select name from mk_netbanking_banks where id=$bank_id");
			$gateway_name = func_query_first_cell("select name from mk_netbanking_gateways where id=$gateway_id");
			
			func_query("delete from mk_netbanking_mapping where id=$mapping_id");
			NetBankingHelper::logNetBankingChangeActivity($login, $mode,$bank_name,$gateway_name,"gateway code deleted $gateway_code");
		}		
	} elseif($mode=='update') {
		$updatenamearray = $_POST['update_name_field'];
		foreach($updatenamearray AS $id=>$gateway_code) {
			if(!empty($gateway_code)) {
				$result = func_query_first("select bank_id,gateway_id,gateway_code from mk_netbanking_mapping where id=$id");
				$bank_id = $result['bank_id'];
				$gateway_id = $result['gateway_id'];
				$old_gateway_code = $result['gateway_code'];
				$bank_name = func_query_first_cell("select name from mk_netbanking_banks where id=$bank_id");
				$gateway_name = func_query_first_cell("select name from mk_netbanking_gateways where id=$gateway_id");
				$name = mysql_escape_string(trim($gateway_code));
				func_query("update `mk_netbanking_mapping` set `gateway_code`='$name' where id=$id");
				if($old_gateway_code!=$name) {
					NetBankingHelper::logNetBankingChangeActivity($login, $mode,$bank_name,$gateway_name,"gateway code updated from $old_gateway_code to $name");
				}
			}
		}
	} else if($mode =='clearCache') {
		NetBankingHelper::clearCachedValues();
	}
	func_header_location($http_location. "/admin/netbanking/add_bank_gateway.php?id=$bank_id");	
} else if($REQUEST_METHOD == "GET") {
	if(empty($id)) {
		func_header_location($http_location . "/admin/netbanking/bank_list.php");
	}
	
	$bank_name = func_query_first_cell("select name from mk_netbanking_banks where id=$id");
	
	$bank_gateway_list = func_query("select m.id as id, g.name as gateway_name,gateway_code from mk_netbanking_mapping m,mk_netbanking_gateways g where m.bank_id=$id and g.id=m.gateway_id");
	
	$bank_unused_gateways = func_query("select id as gateway_id,name as gateway_name from mk_netbanking_gateways where id not in (select gateway_id from mk_netbanking_mapping where bank_id=$id)");
	
	$smarty->assign("bank_name", $bank_name);
	$smarty->assign("bank_id", $id);
	$smarty->assign("bank_gateway_list", $bank_gateway_list);
	$smarty->assign("bank_unused_gateways", $bank_unused_gateways);
	$smarty->assign("action_php","add_bank_gateway.php");
	$smarty->assign("main","netbanking_bank_gateway");
	func_display("admin/home.tpl",$smarty);	
} else {
	func_header_location($http_location . "/admin/netbanking/bank_list.php");
}



?>