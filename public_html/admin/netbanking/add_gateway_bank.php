<?php
chdir(dirname(__FILE__));

require_once "../auth.php";
require_once $xcart_dir."/include/security.php";
include_once "$xcart_dir/include/class/netbankingHelper.php";

if($REQUEST_METHOD == "POST") {
	if(empty($gateway_id)) {
		func_header_location($http_location . "/admin/netbanking/add_gateway.php");
	}
	$_time = time();
	if($mode=='add') {
		if(!empty($gateway_bank_code_new)) {
			$gateway_bank_code_new = mysql_escape_string(trim($gateway_bank_code_new));
			func_query("insert into mk_netbanking_mapping (`bank_id`,`gateway_id`,`gateway_code`) values ($gateway_value_new, $gateway_id,'$gateway_bank_code_new')");
			$bank_name = func_query_first_cell("select name from mk_netbanking_banks where id=$gateway_value_new");
			$gateway_name = func_query_first_cell("select name from mk_netbanking_gateways where id=$gateway_id");
			NetBankingHelper::logNetBankingChangeActivity($login, $mode,$bank_name,$gateway_name,"gateway code added $gateway_bank_code_new");
		}
	} elseif($mode=='delete') {
		if(!empty($mapping_id)) {
			$result = func_query_first("select bank_id,gateway_id,gateway_code from mk_netbanking_mapping where id=$mapping_id");
			$bank_id = $result['bank_id'];
			$gateway_id = $result['gateway_id'];
			$gateway_code = $result['gateway_code'];
			$bank_name = func_query_first_cell("select name from mk_netbanking_banks where id=$bank_id");
			$gateway_name = func_query_first_cell("select name from mk_netbanking_gateways where id=$gateway_id");			
			func_query("delete from mk_netbanking_mapping where id=$mapping_id");
			NetBankingHelper::logNetBankingChangeActivity($login, $mode,$bank_name,$gateway_name,"gateway code deleted $gateway_code");			
		}		
	} elseif($mode=='update') {
		$updatenamearray = $_POST['update_name_field'];
		foreach($updatenamearray AS $id=>$gateway_code) {
			if(!empty($gateway_code)) {
				$result = func_query_first("select bank_id,gateway_id,gateway_code from mk_netbanking_mapping where id=$id");
				$bank_id = $result['bank_id'];
				$gateway_id = $result['gateway_id'];
				$old_gateway_code = $result['gateway_code'];
				$bank_name = func_query_first_cell("select name from mk_netbanking_banks where id=$bank_id");
				$gateway_name = func_query_first_cell("select name from mk_netbanking_gateways where id=$gateway_id");
				
				$name = mysql_escape_string(trim($gateway_code));
				func_query("update `mk_netbanking_mapping` set `gateway_code`='$name' where id=$id");

				if($old_gateway_code!=$name) {
					NetBankingHelper::logNetBankingChangeActivity($login, $mode,$bank_name,$gateway_name,"gateway code updated from $old_gateway_code to $name");
				}
			}
		}
	} else if($mode =='clearCache') {
		NetBankingHelper::clearCachedValues();
	}
	func_header_location($http_location. "/admin/netbanking/add_gateway_bank.php?id=$gateway_id");	
} else if($REQUEST_METHOD == "GET") {
	if(empty($id)) {
		func_header_location($http_location . "/admin/netbanking/add_gateway.php");
	}
	
	$gateway_name = func_query_first_cell("select name from mk_netbanking_gateways where id=$id");
	
	$gateway_bank_list = func_query("select m.id as id, b.name as bank_name,gateway_code from mk_netbanking_mapping m,mk_netbanking_banks b where m.gateway_id=$id and b.id=m.bank_id");
	
	$gateway_unused_banks = func_query("select id as bank_id,name as bank_name from mk_netbanking_banks where id not in (select bank_id from mk_netbanking_mapping where gateway_id=$id)");
	
	$smarty->assign("gateway_name", $gateway_name);
	$smarty->assign("gateway_id", $id);
	$smarty->assign("gateway_bank_list", $gateway_bank_list);
	$smarty->assign("gateway_unused_banks", $gateway_unused_banks);
	$smarty->assign("action_php","add_gateway_bank.php");
	$smarty->assign("main","netbanking_gateway_bank");
	func_display("admin/home.tpl",$smarty);	
} else {
	func_header_location($http_location . "/admin/netbanking/add_gateway.php");
}



?>