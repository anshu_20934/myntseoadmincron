<?php
chdir(dirname(__FILE__));
require_once "../auth.php";
require_once $xcart_dir."/include/security.php";
include_once "$xcart_dir/include/class/netbankingHelper.php";


if($REQUEST_METHOD == "POST") {
	if(empty($mapping_id)) {
		func_header_location($http_location . "/admin/netbanking/activate_gateway.php");
	}
	
	if($mode=='add') {
		$toUpdate= array();
		$toUpdate['is_active'] = 1;
		func_array2update('mk_netbanking_mapping', $toUpdate,"id=$mapping_id");
		
		$result = func_query_first("select bank_id,gateway_id,gateway_code from mk_netbanking_mapping where id=$mapping_id");
		$bank_id = $result['bank_id'];
		$gateway_id = $result['gateway_id'];
		$gateway_code = $result['gateway_code'];
		$bank_name = func_query_first_cell("select name from mk_netbanking_banks where id=$bank_id");
		$gateway_name = func_query_first_cell("select name from mk_netbanking_gateways where id=$gateway_id");
		NetBankingHelper::logNetBankingChangeActivity($login, "activate",$bank_name,$gateway_name,"$bank_name activated for $gateway_name");		
	} else if($mode=='delete') {
		$toUpdate= array();
		$toUpdate['is_active'] = 0;
		func_array2update('mk_netbanking_mapping', $toUpdate,"id=$mapping_id");
		$result = func_query_first("select bank_id,gateway_id from mk_netbanking_mapping where id=$mapping_id");
		$bank_id = $result['bank_id'];
		$gateway_id = $result['gateway_id'];
		$bank_name = func_query_first_cell("select name from mk_netbanking_banks where id=$bank_id");
		$gateway_name = func_query_first_cell("select name from mk_netbanking_gateways where id=$gateway_id");
		NetBankingHelper::logNetBankingChangeActivity($login, "deactivate",$bank_name,$gateway_name,"$bank_name deactivated for $gateway_name");
	} else if($mode=='activate') {
		$bank_name = $mapping_id;
		$bank_id = func_query_first_cell("select id from mk_netbanking_banks where name='$bank_name'");
		$toUpdate= array();
		$toUpdate['is_active'] = 1;
		func_array2update('mk_netbanking_mapping', $toUpdate,"bank_id=$bank_id");
		
		$mapping_list = func_query("select gateway_id from mk_netbanking_mapping where bank_id=$bank_id");
		foreach ($mapping_list as $result) {
			$gateway_id = $result['gateway_id'];
			$gateway_name = func_query_first_cell("select name from mk_netbanking_gateways where id=$gateway_id");
			NetBankingHelper::logNetBankingChangeActivity($login, "activateall",$bank_name,$gateway_name,"$bank_name activated for $gateway_name");
		}
	} else if($mode=='reset') {
		func_query('update mk_netbanking_mapping set is_active=1');
		$mapping_list = func_query("select bank_id,gateway_id from mk_netbanking_mapping");
		foreach ($mapping_list as $result) {
			$bank_id = $result['bank_id'];
			$gateway_id = $result['gateway_id'];
			$bank_name = func_query_first_cell("select name from mk_netbanking_banks where id=$bank_id");
			$gateway_name = func_query_first_cell("select name from mk_netbanking_gateways where id=$gateway_id");
			NetBankingHelper::logNetBankingChangeActivity($login, "resettodefault",$bank_name,$gateway_name,"$bank_name activated for $gateway_name");
		}
	} else if($mode=='add_gateway') {
		$bank_name = $mapping_id;
		$bank_id = func_query_first_cell("select id from mk_netbanking_banks where name='$bank_name'");
		func_header_location($http_location . "/admin/netbanking/add_bank_gateway.php?id=$bank_id");
	} else if($mode =='clearCache') {
		NetBankingHelper::clearCachedValues();
	}
	func_header_location($http_location . "/admin/netbanking/activate_gateway.php");
} else {
	$bank_gateway_lists = func_query("select m.id as id, b.name as bank_name, g.name as gateway_name, is_active from mk_netbanking_mapping m left join mk_netbanking_banks b on m.bank_id=b.id left join mk_netbanking_gateways g on m.gateway_id=g.id order by bank_name, is_active desc");
	$toPass =array();
	foreach ($bank_gateway_lists as $bank_mapping) {
		$bank_name = $bank_mapping['bank_name'];
		$gateway_name =  $bank_mapping['gateway_name'];
		$is_active = $bank_mapping['is_active'];
		$id = $bank_mapping['id'];
		$toPass[$bank_name][] = array("id"=> $id,"gateway_name"=>$gateway_name,"is_active" =>$is_active);
	}	
	
	$smarty->assign("bank_gateway_mapping", $toPass);
	$smarty->assign("action_php","activate_gateway.php");
	$smarty->assign("main","netbanking_gateway_mapping");
	func_display("admin/home.tpl",$smarty);		
}

?>