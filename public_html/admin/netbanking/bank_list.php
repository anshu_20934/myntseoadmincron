<?php
chdir(dirname(__FILE__));

require_once "../auth.php";
require_once $xcart_dir."/include/security.php";
include_once "$xcart_dir/include/class/netbankingHelper.php";

$_time = time();
if($mode=='add') {
	if(!empty($bank_name_new)){
		$name = mysql_escape_string(trim($bank_name_new));
		if(!empty($bank_popular_new)){
			$isPopular=1;	
		} else {
			$isPopular=0;
		}
		func_query("insert into `mk_netbanking_banks` (`name`,`is_popular`) values ('$name',$isPopular)");
		NetBankingHelper::logNetBankingChangeActivity($login, $mode,$name,'',"is_popular-$isPopular");
	}
	func_header_location($http_location . "/admin/netbanking/bank_list.php");
} elseif ($mode=='update') {
	$updatenamearray = $_POST['update_name_field'];
	$updatepopulararray = $_POST['update_popular_field'];
	foreach($updatenamearray AS $bank_id=>$bank_name) {
		if(!empty($bank_name)) {
			$name = mysql_escape_string(trim($bank_name));
			if(!empty($updatepopulararray[$bank_id])){
				func_query("update `mk_netbanking_banks` set `name`='$name',`is_popular`=1 where id=$bank_id");
				NetBankingHelper::logNetBankingChangeActivity($login, $mode,$name,'',"is_popular-1");
			} else {
				func_query("update `mk_netbanking_banks` set `name`='$name',`is_popular`=0 where id=$bank_id");
				NetBankingHelper::logNetBankingChangeActivity($login, $mode,$name,'',"is_popular-0");
			}
		}
	}
	
	//refresh cache here
	func_header_location($http_location . "/admin/netbanking/bank_list.php");
	
} elseif ($mode=='delete') {
	if(!empty($bank_id)) {
		$bank_name = func_query_first_cell("select name from mk_netbanking_banks where id=$bank_id");
		func_query("delete from `mk_netbanking_banks` where id=$bank_id");
		//delete the bank from all dependent table currently i.e mk_netbanking_mapping
		func_query("delete from `mk_netbanking_mapping` where bank_id=$bank_id");
		NetBankingHelper::logNetBankingChangeActivity($login, $mode,$bank_name,'',"mapping for this bank also deleted");
		//refresh cache here
	}
	func_header_location($http_location . "/admin/netbanking/bank_list.php");
	
} else if($mode =='clearCache') {
	NetBankingHelper::clearCachedValues();
	func_header_location($http_location . "/admin/netbanking/bank_list.php");
}

$bank_list = func_query("select id,name,is_popular from mk_netbanking_banks");
$smarty->assign("bank_list", $bank_list);
$smarty->assign("action_php","bank_list.php");
$smarty->assign("main","netbanking_bank_list");
func_display("admin/home.tpl",$smarty);




?>
