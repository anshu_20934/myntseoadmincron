<?php

require "./auth.php";
require $xcart_dir."/include/security.php";
//echo $mode;
if ($REQUEST_METHOD == "POST") {
	   if(!isset($_FILES) && isset($HTTP_POST_FILES))
	    $_FILES = $HTTP_POST_FILES;

		if(!empty($_FILES['thumb_img']['name']) || !empty($_FILES['detail_img']['name'])|| !empty($_FILES['size_img']['name'])|| !empty($_FILES['icon_img']['name'])||!empty($_FILES['thumb_small']['name'])||!empty($_FILES['thumb_big']['name']))        {
             
            $th_img = "images/type/T/".$_FILES['thumb_img']['name'];
			$det_img = "images/type/L/".$_FILES['detail_img']['name'];
			$siz_img = "images/type/S/".$_FILES['size_img']['name'];
			$icon_img = "images/type/I/".$_FILES['icon_img']['name'];
			$thumb_small = "images/startshopping/".$_FILES['thumb_small']['name'];			
			$thumb_big = "images/startshopping/".$_FILES['thumb_big']['name'];
			$uploaddir1 = '../images/type/T/';
			$uploaddir2 = '../images/type/L/';
			$uploaddir3 = '../images/type/S/';
			$uploaddir4 = '../images/type/I/';
			$uploaddir5 = '../images/startshopping/';
			$uploaddir6 = '../images/startshopping/';

			$uploadfile1 = $uploaddir1 . basename($_FILES['thumb_img']['name']);
			$uploadfile2 = $uploaddir2 . basename($_FILES['detail_img']['name']);
			$uploadfile3 = $uploaddir3 . basename($_FILES['size_img']['name']);
			$uploadfile4 = $uploaddir4 . basename($_FILES['icon_img']['name']);
			$uploadfile5 = $uploaddir5.basename($_FILES['thumb_small']['name']);
			$uploadfile6 = $uploaddir6.basename($_FILES['thumb_big']['name']);
			
			if (@move_uploaded_file($_FILES['thumb_img']['tmp_name'], $uploadfile1)) {
				define("THMIMAGE",$th_img);
			}
			if (@move_uploaded_file($_FILES['detail_img']['tmp_name'], $uploadfile2)) {
				define("DETIMAGE",$det_img);
			}
			if (@move_uploaded_file($_FILES['size_img']['tmp_name'], $uploadfile3)) {
				define("SIZIMAGE",$siz_img);
			}
			if (@move_uploaded_file($_FILES['icon_img']['tmp_name'], $uploadfile4 )) {
				define("ICONIMAGE",$icon_img );
			}
			if (@move_uploaded_file($_FILES['thumb_small']['tmp_name'], $uploadfile5 )) {
				define("THUMBSMALL",$thumb_small );
			}
			if (@move_uploaded_file($_FILES['thumb_big']['tmp_name'], $uploadfile6 )) {
				define("THUMBBIG",$thumb_big );
			}
		



		}

}

$template = "";
$producttype="";
$consignmentcapacity="";
if (empty($mode)) $mode = "";


if ($REQUEST_METHOD == "POST") { 
   
 
	  if ($mode == "modify") {
			   $template ="ptype_action";
			   $data ="";
			   
			   if(is_array($posted_data)) {
					foreach ($posted_data as $productid=>$v) {
					   
						  if(!empty($v['delete'])){
						      $data = $productid;
						       $producttype = func_query("SELECT $sql_tbl[mk_product_type].name, $sql_tbl[mk_product_type].label ,$sql_tbl[mk_product_type].image_t ,
							   $sql_tbl[mk_product_type].image_l, $sql_tbl[mk_product_type].description,$sql_tbl[mk_product_type].cod_enabled,
							   $sql_tbl[mk_product_type].type, $sql_tbl[mk_product_type].bulk , $sql_tbl[mk_product_type].price_start,$sql_tbl[mk_product_type].image_s,$sql_tbl[mk_product_type].product_type_groupid
                               ,$sql_tbl[mk_product_type].vat,$sql_tbl[mk_product_type].location,$sql_tbl[mk_product_type].product_code,$sql_tbl[mk_product_type].image_i,$sql_tbl[mk_product_type].processing_time,$sql_tbl[mk_product_type].max_processing_quantity,$sql_tbl[mk_product_type].is_featured,$sql_tbl[mk_product_type].thumb_small_image_path,$sql_tbl[mk_product_type].thumb_big_image_path,$sql_tbl[mk_product_type].display_order,$sql_tbl[mk_product_type].default_style_id
							  from $sql_tbl[mk_product_type] 
							  where $sql_tbl[mk_product_type].id=$data order by $sql_tbl[mk_product_type].name ");
                              
						      $consignmentcapacity = func_query_first_cell("select consignment_capacity from mk_shipping_prod_type where product_type_id='{$data}'");
							 
                           }
					}
				
			  }

	  }
	  //add new product type
	  elseif($mode == "add"){

          $template ="ptype_action";
	  
	  }
	  elseif($mode == "delete"){

		  $template ="ptype_action";
			   
	  }elseif ($mode == "addtype") {
		   if(!empty($type_name) && !empty($type_label) && !empty($_POST['consignmentcapacity']) ) {
				$insert_prodid = func_array2insert(
						"mk_product_type", 
						array(
						"name" => $type_name,
						"label" => $type_label,
						"product_code" => $type_code,
						"description" => mysql_real_escape_string($_POST[desc]),
						"product_type_groupid"=> $ptypegroup,
						"created_date" => time(),
						"image_t"  => THMIMAGE,
						"image_l" => DETIMAGE,
					    "type" => $ptypetype,
					    "bulk" => $bulk,
						"image_s" => SIZIMAGE,
						"price_start"=>$start_from,
						"vat"=>$vatrate,
						"processing_time"=>$processingtime,
						"max_processing_quantity"=>$processingquantity,
						"location"=>$operationlocation,
                        "cod_enabled"=>$cod_enabled,
						"image_i" => ICONIMAGE,
						"thumb_small_image_path"=>THUMBSMALL,
						"thumb_big_image_path"=>THUMBBIG,
						"is_featured"=>$_POST['is_featured'],
						"display_order"=>$_POST['display_order'],
                        "default_style_id"=>$_POST['default_style_id']

					)
				);
				func_array2insert(
					"mk_shipping_prod_type",
					array(
						"product_type_id" 		=> $insert_prodid,
						"consignment_capacity"	=> $_POST['consignmentcapacity']
					)
				);
			
			 func_header_location("admin_product_types.php");
		   }else{
		   
		         $template ="ptype_action";
		     	 $err_msg = "Mandatory field";    
		   }

	 }elseif($mode == "updatetype"){

		 $id= intval($typeid);
			//echo "thumbimage".thumsmall."<br>"."thumbbig".thumbbig."<br>display_order".$_POST['display_order'];
			//exit;
         func_array2update("mk_product_type",
			       array(
					"name" => $type_name,
					"label" => $type_label,
			       "product_code" => $type_code,
					"description" => mysql_real_escape_string($_POST[desc]),
					 "product_type_groupid"=> $ptypegroup,
					"bulk" => $bulk,
                    "modified_date" => time(),
                    "type" => $ptypetype,
					"price_start"=>$start_from,
					"vat"=>$vatrate,
					"processing_time"=>$processingtime,
					"max_processing_quantity"=>$processingquantity,
					"location"=>$operationlocation,
                    "cod_enabled"=>$cod_enabled,
					"is_featured"=>$_POST['is_featured'],
					"display_order"=>$_POST['display_order'],
                    "default_style_id"=>$_POST['default_style_id']
						
			 ),"id=$id");

		 func_array2update("mk_shipping_prod_type",
				 	array(
						"consignment_capacity"	=> $_POST['consignmentcapacity']
					),"product_type_id=$id");

		//echo $mode;exit;
         func_header_location("admin_product_types.php");
	 }
	 // updatating the selected  product types
	  elseif($mode == "update"){

           if (is_array($posted_data)) {
			foreach ($posted_data as $id=>$v) {
             
			   if(!empty($v['delete'])){

				   if(!empty($v["order_by"]))
				   {
					   $order = $v["order_by"];
				   }
				   else
				   {
					     $order = 10;
				   }
								
					 db_query ("UPDATE $sql_tbl[mk_product_type] SET is_active =  $v[avail] , display_order = $order WHERE id=$id ");
			     }
			}
			
			$top_message["content"] = func_get_langvar_by_name("msg_adm_producttype_upd");
			$top_message["anchor"] = "featured";
			func_header_location("admin_product_types.php");
		  }
	    }
		// delete the selected  product types
        elseif($mode == "deltype"){

		  if (is_array($posted_data)) {
			foreach ($posted_data as $id=>$v) {
             
			   if(!empty($v['delete'])){
				   db_query ("DELETE FROM $sql_tbl[mk_product_type] WHERE id='$id' ");
				}
			   
			}
			
			$top_message["content"] = func_get_langvar_by_name("msg_adm_producttype_del");
			$top_message["anchor"] = "featured";
			func_header_location("admin_product_types.php");
		  }
		} //end if delete

}
$ptgresults = db_query("select id,label from mk_product_group");
$producttypegroups = array();
if ($ptgresults)
{
	$i=0;

	while ($row=db_fetch_row($ptgresults))
	{
		$producttypegroups[$i] = $row;
		$i++;
	}
}

#
#Load item locations
#
    $sql = "SELECT * FROM $sql_tbl[operation_location] WHERE status=1";
    $locations = func_query($sql);
$productstyles = func_query("select id,name from mk_product_style");
$smarty->assign("productstyles",$productstyles);
$smarty->assign("locations",$locations);
$smarty->assign("ptgroups",$producttypegroups);
$smarty->assign("typeid",$data);
$smarty->assign("mode", $mode);
$smarty->assign ("products", $producttype);
$smarty->assign ("consignmentcapacity", $consignmentcapacity);
$smarty->assign ("errMsg", $err_msg );
$smarty->assign("main",$template);


# Assign the current location line
//$smarty->assign("location", $location);
$smarty->assign("dialog_tools_data", $dialog_tools_data);
$smarty->assign("img", $img);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
