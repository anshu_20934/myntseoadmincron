<?php 
require_once "auth.php";
require_once $xcart_dir."/include/security.php";
use imageUtils\dao\JpegminiDBAdapter;

$dbAdapter = new JpegminiDBAdapter();
if ($mode == "add") {
    $data = array();
    $image_key = mysql_real_escape_string($add_image_key);
    $enabled = mysql_real_escape_string($add_enabled);
    $data[$image_key] = $enabled;
    $dbAdapter->updateConfig($data);
}elseif ( $mode == "delete" ) {
    if( !empty($delete_key_id) ) {
        $dbAdapter->deleteConfig($delete_key_id);
    }
}elseif ($mode == "save") {
    $data = array();
    foreach($image_key as $key=>$value){
        if(!isset($data[$key])){
            $data[$key] = array();
        }
        $data[$key]["image_key"] = $value;
    }
    foreach($enabled AS $key=>$value){
        $data[$key]["enabled"] = $value;
    }
    $dbAdapter->updateConfig($data);
}

$data = $dbAdapter->getConfig();

$smarty->assign("data",$data);
$smarty->assign("main","jpegmini_config");
func_display("admin/home.tpl",$smarty);
?>
