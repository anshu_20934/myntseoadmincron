<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.order.php";
require_once 'codAdminHelper.php';
$action  = $HTTP_POST_VARS['action'];
$uploadStatus = false;
$message = "";

//putenv("TZ=Asia/Calcutta");
//db_query("SET SESSION time_zone = 'Asia/Calcutta'; ");
if($_POST['mode']== 'search')  {  // this condition is to return the post data in case of search
	// Resolve if any one of the start or end date is empty.
	list($shippingStartDate, $shippingEndDate, $message) = resolveEmpty($_POST['shippingStartDate'], $_POST['shippingEndDate'], $message);
	list($orderStartDate, $orderEndDate, $message) = resolveEmpty($_POST['orderStartDate'],$_POST['orderEndDate'], $message);
	
	$codOrderData = search_cod_orders($shippingStartDate, $shippingEndDate,
						$_POST['paymentStartDate'],$_POST['paymentEndDate'],
					 	$orderStartDate, $orderEndDate,
					  	$_POST['orderStartValue'],$_POST['orderEndValue'],
					   	$_POST['payStatus'], $_POST['orderId'], $_POST['customerName'], $_POST['awbno']);
	if($_POST['codSearchBtn'] == "DownloadAsCsv") {
			printAsCsv($codOrderData, "cod_search_data");
	} 
} 
/* this logic is to render payment info on cod order report page */
$pending = get_order_number_value("pending", $_POST);
$pending[0]["cod_pay_status"]="Pending";
$paidToBlueDart = get_order_number_value("paidtobluedart", $_POST);
$paidToBlueDart[0]["cod_pay_status"]="PaidToBD";
$paid    = get_order_number_value("paid", $_POST);
$paid[0]["cod_pay_status"]="Paid";
$unpaid    = get_order_number_value("unpaid", $_POST);
$unpaid[0]["cod_pay_status"]="Unpaid";
$writtenoff    = get_order_number_value("writtenoff", $_POST);
$writtenoff[0]["cod_pay_status"]="Writtenoff";
$totalValue    = get_order_number_value('',$_POST);
$totalValue[0]["cod_pay_status"]="Total COD";
$otherThenCod = other_then_cod_revenue($_POST);
$otherThenCod[0]["status"]  = "Other Than Cod ";
$valueRatio = array();
$valueRatio[0]['cod_pay_status'] ="Cod%";

if(!empty($totalValue)){
$valueRatio[0]['total_count'] = round(($totalValue[0]['total_count']/($otherThenCod[0]['total_count']+$totalValue[0]['total_count']))*100,2);
$valueRatio[0]['total_amount'] = round(($totalValue[0]['total_amount']/($otherThenCod[0]['total_amount']+$totalValue[0]['total_amount']))*100,2);
//$valueRatio[0]['cod_pay_status'] ="Cod/NonCod%";
}


$pay_status_total = array($pending,$paidToBlueDart,$paid, $unpaid, $writtenoff, $totalValue, $otherThenCod, $valueRatio);
$smarty->assign("pay_status_total",$pay_status_total);

/* this logic is to display pending payment info by days */
$payStatus = "pending,paidtobluedart";
$Dayslessthe15 = pending_payment_data(0,15, $payStatus,$_POST);
$Dayslessthe15[0]["cod_pay_status"]="LessThen15Days";
$Daysbetween16and30= pending_payment_data(15,30, $payStatus, $_POST);
$Daysbetween16and30[0]["cod_pay_status"]="16-30 days";
$Daysbetween31and60= pending_payment_data(30,60, $payStatus, $_POST);
$Daysbetween31and60[0]["cod_pay_status"]="31-60 days";
$Daysbetween61and9999= pending_payment_data(60,9999, $payStatus,$_POST);
$Daysbetween61and9999[0]["cod_pay_status"]="MoreThen60days";
$totalPendingPayment= pending_payment_data(0,9999, $payStatus, $_POST);
$totalPendingPayment[0]["cod_pay_status"]="Total";
$pending_day_data = array($Dayslessthe15, $Daysbetween16and30, $Daysbetween31and60, $Daysbetween61and9999,$totalPendingPayment);
$smarty->assign("pending_payment_data", $pending_day_data);

/* get the info about avg payment cycle */

$avgPaymentCycle = get_avg_payment_cycle($_POST);
$smarty->assign("avgPaymentCycle", $avgPaymentCycle);

/* This logic is to display item status info after shipping a cod order */
$lostItems = item_delivery_status_count('L', $_POST);
$lostItems[0]["item_status"] = "Lost";

$returnItems = item_delivery_status_count('R', $_POST);
$returnItems[0]["item_status"] = "Return";

$deliverItems = item_delivery_status_count('D', $_POST);
$deliverItems[0]["item_status"] = "Delivered";

//$totalShipItems = item_delivery_status_count('L, R, D');
//$totalShipItems[0]["item_status"] = "TotalShipped";
//$totalShhipItems[0]["count(amount)"] = $deliverItems[0]["count(amount)"] + $returnItems[0]["count(amount)"] + $lostItems[0]["count(amount)"];

$itemStatus = array($lostItems, $returnItems, $deliverItems);
$smarty->assign("item_status_data", $itemStatus);

$mode = "display";
$smarty->assign("cod_search_data", $codOrderData);
$smarty->assign("post_data", $_POST);
$smarty->assign('mode',$mode);
$smarty->assign("message", $message);
$smarty->assign("main","cod_order_report");
func_display("admin/home.tpl",$smarty);
?>
