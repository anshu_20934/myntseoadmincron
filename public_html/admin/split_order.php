<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
include_once("../include/func/func.mkcore.php");
include_once("../include/func/func.inventory.php");
include_once("../include/func/func.mk_orderbook.php");
include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");
include_once("$xcart_dir/include/class/oms/ItemManager.php");
include_once($xcart_dir."/include/class/class.mail.multiprovidermail.php");
include_once($xcart_dir."/modules/apiclient/ReleaseApiClient.php");
include_once("$xcart_dir/include/class/oms/EventCreationManager.php");
include_once("$xcart_dir/include/class/oms/OrderWHManager.php");

x_load('db','order', 'mail');

$action = $_POST['action'];

if ($action == 'split') {
	$loggedinuser = $_POST['login'];
	
	$orderid = $_POST['orderid'];
	$original_order = func_query_first("SELECT * from $sql_tbl[orders] WHERE orderid = $orderid");
	
	$item_details = ItemManager::getItemDetailsForItemIds(explode(",", $_POST['itemids']), $orderid);
        $item_split_govt_tax_restamp_required = false; // To check if re-stamping of govt tax is required for original order
	
	if($item_details) {
		
		$new_items = array();
		$old_items = array();
		
		$new_order = $original_order;
		unset($new_order['orderid']);
		$new_order['total'] = 0.00;
		$new_order['subtotal'] = 0.00;
		$new_order['coupon_discount'] = 0.00;
		$new_order['discount'] = 0.00;
		$new_order['cart_discount'] = 0.00;
		$new_order['pg_discount'] = 0.00;
		$new_order['cash_redeemed'] = 0.00;
		$new_order['tax'] = 0.00;
		$new_order["store_credit_usage"] = 0.00;
		$new_order["earned_credit_usage"] = 0.00;
                  $new_order["loyalty_points_used"] = 0.00;
		$new_order["loyalty_points_awarded"] = 0.00;
		$new_order['payment_surcharge'] = 0.00;
		$new_order['cod'] = 0.00;
		$new_order['cashback'] = 0.00;
		$new_order['qtyInOrder'] = 0;
		if($courierServiceabilityVersion != 'new') {
			$new_order['courier_service'] = '';
		}
		$new_order['tracking'] = '';
		$new_order['shipping_cost'] = 0.00;
		$new_order['gift_charges'] = 0.00;
		$new_order['gift_card_amount'] = 0.00;

		$new_order['s_firstname'] = mysql_real_escape_string($new_order['s_firstname']);
		$new_order['s_lastname'] = mysql_real_escape_string($new_order['s_lastname']);
		$new_order['s_address'] = mysql_real_escape_string($new_order['s_address']);
		$new_order['s_city'] = mysql_real_escape_string($new_order['s_city']);
		$new_order['b_address'] = mysql_real_escape_string($new_order['b_address']);
		$new_order['b_city'] = mysql_real_escape_string($new_order['b_city']);
		
		$original_order['s_firstname'] = mysql_real_escape_string($original_order['s_firstname']);
		$original_order['s_lastname'] = mysql_real_escape_string($original_order['s_lastname']);
		$original_order['s_address'] = mysql_real_escape_string($original_order['s_address']);
		$original_order['s_city'] = mysql_real_escape_string($original_order['s_city']);
		$original_order['b_address'] = mysql_real_escape_string($original_order['b_address']);
		$original_order['b_city'] = mysql_real_escape_string($original_order['b_city']);
		
		$comment = "";
		$can_split_order = true;
	
		$skuId2ItemsMap = ItemApiClient::getSkuWiseItemDetailsForOrder($orderid, $original_order['warehouseid']);
		$new_order_created = false;
		
		foreach($item_details as $item) {
			$qty_to_split = intval($_POST['item_quantity_'.$item['itemid']]);
			$original_qty = $item['amount'];
			$can_move_item = true;
			if($skuId2ItemsMap) {
				$skuid = $item['sku_id'];
				$issued_items = $skuId2ItemsMap[$skuid];
				$noOfIssuedItems = count($issued_items);
				$remaining_items = $original_qty - $qty_to_split;
				if($noOfIssuedItems > $remaining_items)
					$can_move_item = false;			
			}
			
			if($can_move_item) {
				$new_item = ItemManager::updateOrderItemMovement($original_order, $new_order, $item, $qty_to_split);
	 			if($new_item) {
		 			$new_items[] = $new_item;
                                        // Re-compute govt tax for the original order since there is an item split
                                        $item_split_govt_tax_restamp_required = true;
		 		}
		 		$old_items[] = $item;
			} else {
				$can_split_order = false;
				$comment .= "Cannnot move $qty_to_split items from item - ".$item['itemid']." to new order as equal no of item pieces have been issued. Please remove required items and then split!<br><br>"; 
			}
		}
		
		if($can_split_order) {
			$new_orderid = create_new_order();
			$new_order['orderid'] = $new_orderid;
			$new_order['invoiceid'] = create_new_invoice_id($new_orderid);
			
			foreach($old_items as $old_item) {
				$item_option_qty = $old_item['item_option_qty'];
				unset($old_item['item_option_qty']);
				unset($old_item['sku_id']);
				unset($old_item['quantity']);
				if(!isset($old_item['orderid']) || empty($old_item['orderid']))
					$old_item['orderid'] = $new_orderid;
				
				func_array2update("order_details", $old_item, "itemid = ".$old_item['itemid']);
				if($item_option_qty)
					func_array2update('mk_order_item_option_quantity', $item_option_qty, 'itemid = '.$old_item['itemid']);
			}
			
			foreach($new_items as $new_item) {
				$option_qty_mapping = $new_item['item_option_qty'];
				unset($new_item['item_option_qty']);
				unset($new_item['sku_id']);
				unset($new_item['quantity']);
				
				$new_item['orderid'] = $new_orderid;
				$new_itemid = func_array2insert("order_details", $new_item);
				$option_qty_mapping['itemid'] = $new_itemid;
				func_array2insert("mk_order_item_option_quantity", $option_qty_mapping);
			}
			
			//$new_order['date'] = time();
			//$new_order['queueddate'] = time();
			$new_order['splitdate'] = time();
			$new_order['status'] = 'WP';
			
			// If all items were removed from this order, then mark this order as rejected.
			$items = func_query("SELECT * from $sql_tbl[order_details] WHERE item_status != 'IC' and orderid = $orderid", true);
			if(!$items){
				$original_order['status'] = 'F';
			}
			
			func_array2update("orders", $original_order, "orderid = ".$original_order['orderid']);
			func_array2insert("orders", $new_order);
				
			$split_reason = $_POST['split_reason'];
			$split_comment = $_POST['split_comment'];
			$commenttime = time();
			
			ReleaseApiClient::pushOrderReleaseToWMS($orderid, $loggedinuser,'update');
			$comment .= "Reason : $split_reason. \nRemarks: $split_comment";
			func_addComment_order($orderid, $loggedinuser, 'Note', 'Order Split', $comment);
			
			ReleaseApiClient::pushOrderReleaseToWMS($new_orderid, $loggedinuser,'');
			
			if($courierServiceabilityVersion == 'new') {
				OrderWHManager::assignTrackingNumberForOrders(array($new_order['warehouseid'].'__'.$new_order['courier_service'] => $new_orderid), $new_order['payment_method'], $new_order['s_zipcode']);
			}
			if ($original_order['courier_service'] == "FD" && $original_order['status'] != "F") {
                            db_query("update xcart_orders set courier_service = '".$original_order['courier_service']."', tracking = '' where orderid = '".$original_order['orderid']."' limit 1");
                            \OrderWHManager::assignTrackingNumberForOrders(array($original_order['warehouseid'].'__'.$original_order['courier_service'] => $original_order['orderid']), $original_order['payment_method'], $original_order['s_zipcode']);
                        }
                        if ($item_split_govt_tax_restamp_required) {
                            \OrderWHManager::stampTax($orderid);
                        }
                        
			EventCreationManager::pushCompleteOrderEvent($original_order['group_id']);
			
			$new_order_comment = "Order $orderid was split to create this order. Reason : $split_reason. \nRemarks: $split_comment";
			func_addComment_order($new_orderid, $loggedinuser, 'Note', 'Order Split', $comment);
			
			$smarty->assign("neworderid", $new_orderid);
		} else {
			$smarty->assign("errorMsg", $comment);
		}
		$smarty->assign("action", "complete");
	} else {
		$smarty->assign("errorMsg", "Selected Items are not present in order - $orderid. Please refresh order page to verify!");
	}
} else {
	$productsInCart = ItemManager::getProductDetailsForItemIds(explode(",", $_GET['itemids']), $orderid);
	if($productsInCart) {
		$smarty->assign("productsInCart", $productsInCart);
		$sql = "SELECT reason, reason_display_name from mk_order_action_reasons where action = 'order_split'";
		$reasons_map = func_query($sql, true);
		
		$smarty->assign("action", "confirm");
		$smarty->assign("reasons", $reasons_map);
	} else {
		$smarty->assign("errorMsg", "Selected Items are not present in order - $orderid. Please refresh order page to verify!");
	}
	
	$smarty->assign("itemids", $_GET['itemids']);
	$smarty->assign("orderid", $_GET['orderid']);
}

$smarty->assign("main","split_order");
func_display("admin/home.tpl",$smarty);
?>
