<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
include_once($xcart_dir."/include/func/func.itemsearch.php");

$order_by="";
$sort_order="";

if($HTTP_GET_VARS){
	$query_string = http_build_query($HTTP_GET_VARS);
	$assigneeid = $HTTP_GET_VARS['assigneeid'];
    $order_by = $HTTP_GET_VARS['order_by'];
    $sort_order = $HTTP_GET_VARS['sort_order'];

}

#
#Calling function to create search query for number of records
#
$sql = func_load_assignee_task_list($assigneeid,$order_by,$sort_order);

$sqllog->debug("File name##printAssigneeTaskList.php##SQL Info::>$sql");
$itemResult = func_query($sql);
$smarty->assign ("itemResult", $itemResult);

#
#Load total assigned item
#
//$total_assigned_item = func_query_first_cell("SELECT SUM(amount) FROM $sql_tbl[order_details] a, $sql_tbl[orders] b WHERE a.orderid=b.orderid AND (b.status='Q' OR b.status='P') AND assignee='$assigneeid' AND (item_status='A' OR item_status='S') GROUP BY assignee");
$total_assigned_item=count($itemResult);

#
#Load location name
#
$assignee_name = func_query_first_cell("SELECT name FROM $sql_tbl[mk_assignee] WHERE id='$assigneeid'");

$smarty->assign("total_assigned_item", $total_assigned_item);
$smarty->assign("assignee_name", $assignee_name);
$smarty->assign ("query_string", $query_string);
$smarty->assign("assigneeid",$assigneeid);

func_display("main/printAssigneeTaskList.tpl",$smarty);
?>
