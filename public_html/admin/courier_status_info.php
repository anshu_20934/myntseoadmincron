<?php
require "./auth.php";
require_once $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.mkcore.php";
require_once $xcart_dir."/include/func/func.courier.php";
require_once $xcart_dir."/PHPExcel/PHPExcel.php";
include_once($xcart_dir."/modules/apiclient/CourierApiClient.php");

$action = $_POST['action'];
if(!$action)
	$action = 'view';

if ($action == 'update_courier_info'){
	$all_couriers = get_all_courier_partners();	
	$errorOccured = false;
	foreach($all_couriers as $courier_row) {
		$update_data = array();
		$update_data['enable_status'] = $_POST["enable_status_".$courier_row['code']];
		$update_data['return_supported'] = $_POST["pickup_status_".$courier_row['code']];
		if($update_data['enable_status'] != $courier_row['enable_status'] ||
			$update_data['return_supported'] != $courier_row['return_supported']){
			$resp = CourierApiClient::updateCourier($courier_row['code'], 
				$update_data['enable_status'], $update_data['return_supported'], $login);
			if($resp['status'] == 'failure'){
				$errorOccured = true;
				$smarty->assign("courier_msg", $resp['msg']);
				break;
			}
			func_array2update("courier_service", $update_data, "code='".$courier_row['code']."'");						
		}
	}
	
	if(!$errorOccured){
		$smarty->assign("courier_msg", "Updated Courier Status Info Successfully!");
	}
}

$smarty->assign("action", $action);
$couriers = get_active_courier_partners();
$smarty->assign("couriers", $couriers);
$all_couriers = get_all_courier_partners();
$smarty->assign("all_couriers", $all_couriers);
$smarty->assign("main", "courier_status_info");
func_display("admin/home.tpl",$smarty);
?>
