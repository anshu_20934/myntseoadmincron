<?php
//help from upload_payment_completed_orders.php
require "./auth.php";
require $xcart_dir."/include/security.php";
require_once "user_tagging_activation_function.php";


$user_tagging = $_POST['upload1'];
$coupon_activation = $_POST['upload2'];
$leader_board_entry = $_POST['upload3'];
$coupon_deactivation = $_POST['upload4'];
//$smarty->assign("aUserData",array());
if($_POST){
	$aUserData = $_POST ;
	$smarty->assign("aUserData",$aUserData);
}

if(!isset($_FILES) && isset($HTTP_POST_FILES)){
    $_FILES = $HTTP_POST_FILES;
}
	
if(!empty($user_tagging)){
	$upload_dir = "$xcart_dir/".$_FILES['tagging_file']['name'];
	$extension = end(explode(".",$_FILES['tagging_file']['name']));
	
	if($extension == 'csv'){
	   if(!move_uploaded_file($_FILES['tagging_file']['tmp_name'],$upload_dir)){
	        echo "can't move\n";
	        exit();
	    }
	    else{
	    $filepath = $upload_dir;//.$_FILES['tagging_file']['name'];
	    }
	    $result = shell_exec("php $xcart_dir/scripts/myntra_privilege_card_users_tagging.php $filepath 'user_tagging'");
	    //echo $result;
	    $smarty->assign("output",$result);
	    unlink($filepath);
	}
	else{
	 	//echo "Please upload csv files only\n";
	 	$smarty->assign("error","Please upload csv files only for user tagging");
	 }
}
elseif (!empty ($coupon_activation)){
   $upload_dir = "$xcart_dir/".$_FILES['activation_file']['name'];
	$extension = end(explode(".",$_FILES['activation_file']['name']));
	
	if($extension == 'csv'){
	   if(!move_uploaded_file($_FILES['activation_file']['tmp_name'],$upload_dir)){
	        echo "can't move\n";
	        exit();
	    }
	    else{
	    $filepath = $upload_dir;//.$_FILES['tagging_file']['name'];
	    }
	    //$result = shell_exec("php $xcart_dir/scripts/paroksh_script.php $filepath 'coupon_activation'");
	    $result = shell_exec("php $xcart_dir/scripts/myntra_privilege_card_users_tagging.php $filepath 'coupon_activation'");
	    //print_r($result);
	    $smarty->assign("output",$result);
	    unlink($filepath);
	}
	else{
	 	//echo "Please upload csv file only\n";
	 	$smarty->assign("error","Please upload csv files only for coupon activation");
	 }
}
elseif (!empty ($coupon_deactivation)){
   $upload_dir = "$xcart_dir/".$_FILES['deactivation_file']['name'];
	$extension = end(explode(".",$_FILES['deactivation_file']['name']));
	
	if($extension == 'csv'){
	   if(!move_uploaded_file($_FILES['deactivation_file']['tmp_name'],$upload_dir)){
	        echo "can't move\n";
	        exit();
	    }
	    else{
	    $filepath = $upload_dir;//.$_FILES['tagging_file']['name'];
	    }
	    //$result = shell_exec("php $xcart_dir/scripts/paroksh_script.php $filepath 'coupon_activation'");
	    $result = shell_exec("php $xcart_dir/scripts/myntra_privilege_card_users_tagging.php $filepath 'coupon_deactivation'");
	    //print_r($result);
	    $smarty->assign("output",$result);
	    unlink($filepath);
	}
	else{
	 	//echo "Please upload csv file only\n";
	 	$smarty->assign("error","Please upload csv files only for coupon deactivation");
	 }
}
elseif (!empty ($leader_board_entry)){
    $upload_dir = "$xcart_dir/".$_FILES['entryInTable_file']['name'];
	$extension = end(explode(".",$_FILES['entryInTable_file']['name']));
	
	if($extension == 'csv'){
	   if(!move_uploaded_file($_FILES['entryInTable_file']['tmp_name'],$upload_dir)){
	        echo "can't move\n";
	        exit();
	    }
	    else{
	    $filepath = $upload_dir;//.$_FILES['tagging_file']['name'];
	    }
	    $id = setHappyHourLeaderBoard($_POST);
	    //echo $id;
	    if($id[0]){
	    	$result = shell_exec("php $xcart_dir/scripts/myntra_privilege_happy_hour.php $filepath $id[1]");
	    	$smarty->assign("output",$result);
	    }
	    else{
	    	$smarty->assign("error",$id[1]);
	    }

	    unlink($filepath);
	}
	else { 
		if ($_POST['rule'] == 'global') {
			$id = setHappyHourLeaderBoard($_POST);
			if(!$id[0])
	    		$smarty->assign("error",$id[1]);
		}
		else {
		 	$smarty->assign("error","Please upload csv files only");
		}
	}
}

$smarty->assign("main",'user_tagging_activation');
func_display("admin/home.tpl",$smarty);

?>