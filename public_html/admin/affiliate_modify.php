<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: user_modify.php,v 1.38.2.1 2006/04/19 13:29:18 max Exp $
#

require "./auth.php";
//require $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.affiliates.php";
define("WIDTH",50);
define("HEIGHT",150);
x_load('mail','user');

define('USER_MODIFY', 1);

$location[] = array("Affiliate management", "");
$themeArray  = array
                             (  
							    array('KEY' => 'MYNTRA_UI' , 'VALUE' => 'Myntra UI'),
								array('KEY' => 'COBRANDED_UI' , 'VALUE' => 'Co-branded UI')
							 );


#
# Update profile only
#
if(!empty($HTTP_GET_VARS[mode]))  $mode = $HTTP_GET_VARS[mode];

$affiliateImagePath = "../images/affiliates";
$email_Template = 'affactivation';
if($REQUEST_METHOD == "POST" )
{
	     $affId  = $HTTP_POST_VARS['affId'];
		
		 
		 if($mode == "update")
	     {
             $query_data = array("active" => $HTTP_POST_VARS['act_status']);
			 func_array2update($sql_tbl['mk_affiliates'] , $query_data,"affiliate_id='$affId' ");
			 $affInfo = func_load_affiliate_details($affId);
			 ## send activation notification mail  
			 if($affInfo[0]['active'] == 'Y')
			 {
			 	$mailto = $affInfo[0]['contact_email'];
			 	$args   = array(
			 	                "FIRST_NAME" => $affInfo[0]['contact_name']
			 	                  
			 	 );
			 	      
			 	sendMessage($email_Template,$args,$mailto,$from);
			 }
			 
			 if($affInfo[0]['active'] == 'C')
			 {
			 	$email_rej_Template =  'affrejection';
			 	$mailto = $affInfo[0]['contact_email'];
			 	$args   = array(
			 	                "FIRST_NAME" => $affInfo[0]['contact_name']
			 	                  
			 	 );
			 	      
			 	sendMessage($email_rej_Template,$args,$mailto,$from);
			 }
			 
	     }
        if($HTTP_POST_VARS[action] == "Upload" )
		{
			 
	        if(!isset($_FILES) && isset($HTTP_POST_FILES))
					 $_FILES = $HTTP_POST_FILES;
			 $baseImageName = basename($_FILES['affiliatelogo']['name']);  
	       	
			  if(!file_exists($affiliateImagePath."/".$affId))
				  	createdir($affiliateImagePath."/".$affId);

			  if(!file_exists($affiliateImagePath."/".$affId."/logo"))
				  	createdir($affiliateImagePath."/".$affId."/logo");
          
			 $afflogopath =  $affiliateImagePath."/".$affId."/logo";

				 $newImageFile =  $afflogopath."/".$baseImageName;
			 $result = @move_uploaded_file($_FILES['affiliatelogo']['tmp_name'], $newImageFile);
             $resizedImagePath = $afflogopath."/r_".basename($newImageFile);
				   
			 resizeImageToDimension($newImageFile,$resizedImagePath,WIDTH,HEIGHT);
           	 $query = "UPDATE $sql_tbl[mk_affiliates] SET 
			 logo = 'r_".basename($newImageFile)."' WHERE  
			 affiliate_id = '".$affId."' ";
			 db_query($query); 
			 @unlink($newImageFile);

		  }
		  if($mode == "modify")
	      {
			     $query_data_c = array(
					                   	 "company_name" => $HTTP_POST_VARS['company'],
										  contact_name  => $HTTP_POST_VARS['contact'],
		                                  contact_email => $HTTP_POST_VARS['email'],
	                                      contact_phone1 => $HTTP_POST_VARS['phone1'],
										  contact_phone2    => $HTTP_POST_VARS['phone2'],
										  affiliate_return_url => $HTTP_POST_VARS['url'],
										  title				=> $HTTP_POST_VARS['title'],
										  themeid           => $HTTP_POST_VARS['theme'], 
										  aff_auth_mechanism => $HTTP_POST_VARS['authmode'],
										  active             => $HTTP_POST_VARS['act_status'],
										  contentprovider    => $HTTP_POST_VARS['cprovider'],
										  postprocess        => $HTTP_POST_VARS['postprocess'],
										  postscript         => $HTTP_POST_VARS['postscript'],
										  customtemplate     => $HTTP_POST_VARS['customtpl']
									  ); 
                            
                func_array2update($sql_tbl['mk_affiliates'], $query_data_c, "affiliate_id='$affId'");
               
                if($HTTP_POST_VARS['personalizegift'] == 'P')
				{
					  $productids = '';
			          foreach($HTTP_POST_VARS['product'] as $key => $value)
					  {
						    $productids .= $value.",";
				      }
					  foreach($HTTP_POST_VARS['category'] as $key => $value)
					  {
						   $categories .=  $value.",";
					  }
				  
					   $product = (!empty($HTTP_POST_VARS['chkProduct']) ? trim($productids,",") : '');
					   $category = (!empty($HTTP_POST_VARS['chkCategory']) ? trim($categories,",") : '');
					   $tags = (!empty($HTTP_POST_VARS['chkTags']) ? $HTTP_POST_VARS['tags'] : '');
					   
					   $query_data_aff =  array(
							  		"contentprovider" => 'N'
							  		);
					   func_array2update($sql_tbl['mk_affiliates'], $query_data_aff, "affiliate_id='$affId'")	;	  		
					   
					   $chkProductMap  = func_get_affiliates_types($affId);
                       $chkCategoryMap = func_get_affiliates_category($affId);
                       $chkTagsMap     = func_get_affiliates_tags($affId,false);
                       # insert product types if affiliates add product later 
                       if(empty($chkProductMap))
                       {
                        	(!empty($product)) ?  func_array2insert($sql_tbl['mk_affiliate_product_map'], array("product_type_id" => $product, "affiliate_id" => $affId)) : '';
                       }
                       else 
                       {
					   
					   		(!empty($product)) ?  func_array2update($sql_tbl['mk_affiliate_product_map'], array("product_type_id" => $product), "affiliate_id='$affId'") : '';
                       }
					   
                       if(empty($chkCategoryMap))
                       {
                        	(!empty($category)) ?  func_array2insert($sql_tbl['mk_affiliate_category_map'], array("category_id" => $category,"affiliate_id" => $affId)) : '';
                       }
                       else 
                       {
					   		(!empty($category)) ?  func_array2update($sql_tbl['mk_affiliate_category_map'], array("category_id" => $category), "affiliate_id='$affId'") : '';
                       }
					   
                       if(empty( $chkTagsMap))
                       {
                        	  (!empty($tags)) ?  func_array2insert($sql_tbl['mk_affiliate_tags'], array("tags" => $tags, "affiliate_id" => $affId)) : '';
                       }
                        else 
                        {
					      (!empty($tags)) ?  func_array2update($sql_tbl['mk_affiliate_tags'], array("tags" => $tags), "affiliate_id='$affId'") : '';
                        }
					 
					     
				}
				######## if affiliate sends  user and content #########################
				elseif($HTTP_POST_VARS['personalizegift'] == 'C')
				{
					 
					 $ptype = '';
					
					 foreach($HTTP_POST_VARS[producttype] as $key => $value)
					 {
					 	 $ptype .= $value.",";
					 }
					 $ptype = trim($ptype,",");
					 $query_data_aff =  array(
							  		"contentprovider" => 'Y'
							  		);
					 func_array2update($sql_tbl['mk_affiliates'], $query_data_aff, "affiliate_id='$affId'")	;	  	
					$chkProductMap  = func_get_affiliates_types($affiliateid);	
					if(empty($chkProductMap))
                    {
                        	(!empty($ptype)) ?  func_array2insert($sql_tbl['mk_affiliate_product_map'], array("product_type_id" => $ptype, "affiliate_id" => $affId)) : '';
                    }
                    else 
                    {
					   
					  		(!empty($ptype)) ?  func_array2update($sql_tbl['mk_affiliate_product_map'], array("product_type_id" => $ptype), "affiliate_id='$affId'") : '';  
                    } 
					
			  }
			 ## send activation notification mail  		
			 $resultSet = db_query("SELECT active FROM $sql_tbl[mk_affiliates] WHERE 
			 affiliate_id ='$affId' ");
			 $rows = db_fetch_array($resultSet);
			   
			 if($rows['active'] == 'Y')
			 {
			 	 $mailto = $HTTP_POST_VARS['email'];
			 	 $args   = array(
			 	                "FIRST_NAME" => $HTTP_POST_VARS['contact']
			 	                  
			 	              );
			 	      
			 	 sendMessage($email_Template,$args,$mailto,$from);
			 }
			 	
			 else if($rows['active'] ==  'C')
			 {
			 	$reasonForRej = $HTTP_POST_VARS['reasonforrej'];
			 	$email_rej_Template =  'affrejection';
			 	$mailto = $HTTP_POST_VARS['email'];
			 	$args   = array(
			 	                "FIRST_NAME" => $HTTP_POST_VARS['contact'],
			 	                "REJ_REASON" => $reasonForRej
			 	                  
			 	 );
			 	      
			 	sendMessage($email_rej_Template,$args,$mailto,$from);
			 }
			 
			    
		  }
          

}

if(isset($HTTP_GET_VARS['affId']))
  $affId = $HTTP_GET_VARS['affId'];

$SQL = "SELECT * FROM $sql_tbl[mk_affiliates] WHERE affiliate_id = '".$affId."' ";
$result = db_query($SQL);
$userinfo = db_fetch_array($result);
$smarty->assign("userinfo", $userinfo);

 $userAuthDetail =  affiliate_user_authentication();
 $smarty->assign ("userAuthDetail",$userAuthDetail);

$SQL = "SELECT * FROM $sql_tbl[mk_affiliate_product_map] WHERE affiliate_id = '".$affId."' ";
$result = db_query($SQL);

if($userinfo['contentprovider'] == 'Y')
{
	 $typeids= db_fetch_array($result);
		 if(!empty($typeids ))
					$productTypes = explode(",",$typeids[product_type_id] );
}
else
{
     $typeids= db_fetch_array($result);
     if(!empty($typeids))
			  $selectedproducts = explode(",", $typeids[product_type_id]);

	 $SQL = "SELECT * FROM $sql_tbl[mk_affiliate_category_map] WHERE affiliate_id = '".$affId."' ";
	 $result = db_query($SQL);
	 $catids= db_fetch_array($result);

	 if(!empty($catids))
			   $selectedcategories = explode(",",$catids[category_id]);

	 $SQL = "SELECT * FROM $sql_tbl[mk_affiliate_tags] WHERE affiliate_id = '".$affId."' ";
	 $result = db_query($SQL);
	 $tags= db_fetch_array($result);
	 $selectedtags =  $tags[tags];
		

}


$products = func_load_all_public_product_type();
$categories = func_load_root_categories();

$smarty->assign("products",$products);
$smarty->assign ("categories",$categories);
$smarty->assign ("productTypes",$productTypes);
$smarty->assign ("selectedProducts",$selectedproducts);
$smarty->assign ("selectedCategories",$selectedcategories);
$smarty->assign ("selectedtags",$selectedtags);

	$smarty->assign ("themes",$themeArray);
$smarty->assign("main", "affiliate_profile");
$smarty->assign("affId",$affId );
$smarty->assign("mode",$mode );



# Assign the current location line
$smarty->assign("location", $location);

# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
