<?php
/* This file has all the required function necessary for cod admin panel and for cod report
 * Author :: Vaibhav Bajpai
 * Date   :: 10/11/2010
 */

include_once("$xcart_dir/include/class/oms/EventCreationManager.php");

function get_orderids_from_tracking_list($trackinglist){
	$orders_tracking=array();
	foreach($trackinglist as $key=>$tracking){
		if($tracking!="")
		$orders_tracking[$tracking]=get_orderid_from_tracking($tracking,'BD');
	}
	return $orders_tracking;
}

function getQueryString($str) {
	$itemArr = explode(",", trim($str, ' '));
	$returnStr = " '".implode("' , '", $itemArr)."'";
	//echo $returnStr;
	return $returnStr;

}
function modify_order_pending_status($orderIdArr, $status) {
	global $weblog;
	global $sqllog;
	$time=time();
	if(empty($orderIdArr)){
		$weblog->info("You didn't select any orderId to modify");
		return;
	}
	
	$codpaystatus = strtolower($status);
	
	if($status == "paid") {
		func_change_order_status($orderIdArr, 'C');
		$orderUpdateQuery = "UPDATE xcart_orders SET cod_pay_status='$codpaystatus' WHERE orderid IN (" . implode(',',$orderIdArr) . ")";
		$sqllog->debug("SQL Query : ".$orderUpdateQuery." "." @ line "._LINE_." in file ".__FILE__);
		return db_query($orderUpdateQuery);
	}
	
	// Pravin, we have removed action items for status='unpaid'
}

function modify_item_delivery_status($orderIds, $deliveryStatus) {
	global $sqllog;
	$queryString = "UPDATE xcart_order_details SET item_status='$deliveryStatus' where orderid In (" . implode(",",$orderIds) . ")";
	$sqllog->debug("SQL Query : ".$queryString." "." @ line "._LINE_." in file ".__FILE__);
	return db_query($queryString);
}

function update_cod_order_data($paidOrders, $riskOrders, $login) {
	if(count($paidOrders) >0){
		foreach (array_chunk($paidOrders, 250) as $orders){
			
			foreach($orders as $order) {
				$sql = "UPDATE xcart_order_details SET item_status='D' WHERE orderid = ".$order['orderId']." AND item_status not in  ('IC','RT')";
			    db_query($sql);
			    
			    if(!empty($order['completedDate']))
			    	$time = strtotime($order['completedDate']);
			    else
			    	$time = time();
			    
				$orderUpdateQuery = "UPDATE xcart_orders SET status = 'C', completeddate = $time, delivereddate = ifnull(delivereddate, $time), cod_pay_status='paid'";
				if(!empty($order['chequeNo'])) {
					$orderUpdateQuery .= ", cheque_no = '".$order['chequeNo']."'";
				}
				$orderUpdateQuery .= " WHERE orderid = ".$order['orderId'];
				db_query($orderUpdateQuery);
				
				func_addComment_order($order['orderId'], $login, 'Note', 'Marking order as complete from cod_reconcile', 'cod pay status is updated to paid and order marked as complete');
				
				EventCreationManager::pushReleaseStatusUpdateEvent($order['orderId'], 'C', 'Marking order as complete from cod_reconcile', $time, $login, array('chequeNo'=>$order['chequeNo']));
			}
		}	
	}
	
	if(count($riskOrders) > 0){
		foreach (array_chunk($riskOrders, 250) as $orders){
			foreach($orders as $order) {
				$orderUpdateQuery = "UPDATE xcart_orders SET cod_pay_status='risk'";
				if(!empty($order['chequeNo'])) {
					$orderUpdateQuery .= ", cheque_no = '".$order['chequeNo']."'";
				}
				$orderUpdateQuery .= " WHERE orderid = ".$order['orderId'];
				db_query($orderUpdateQuery);
				
				func_addComment_order($order['orderId'], $login, 'Note', 'Marking order as risk', 'cod pay status is updated to risk and order marked as risk');
			}	
		}	
	}
	
	return true;
}

/**
 * Restricts the time range between start and end Date to 2 months.
 * If either of the two dates are empty, this function returns the adjusted date for the empty one.
 * Returns: array of start and end Date
 * @param string $startDate
 * @param string $endDate
 */
function resolveEmpty($startDate, $endDate, $message){
	if(empty($startDate) xor empty($endDate)){
		if(empty($startDate)){
			$startDate = date("m/d/Y" , strtotime("-2 months", strtotime($endDate))) ;
		}
		elseif (empty($endDate)){
			$endDate = date("m/d/Y" , strtotime("+2 months", strtotime($startDate)));
		}
		$message = "Orders displayed from date: " . $startDate . " till date: " . $endDate;
	}
	return array($startDate, $endDate, $message);
}

function search_cod_orders($shippingStartDate, $shippingEndDate, $paymentStartDate, $paymentEndDate, $orderStartDate,
$orderEndDate, $orderStartValue, $orderEndValue, $payStatus, $orderId,$customerName, $awbno) {

	$queryBaseString = "select orderid, FROM_UNIXTIME(shippeddate) as shipdate, 
						FROM_UNIXTIME(cod_payment_date) as paydate, FROM_UNIXTIME(date) as orderdate,
						login, tracking, (total + cod) as final_total, cod_pay_status 
						from xcart_orders o where payment_method='cod' and (o.status='DL' or o.status='SH' or o.status = 'C') ";
	
	if(!empty($shippingStartDate) && !empty($shippingEndDate)) {
		$shippingStr = ' shippeddate between '.strtotime($shippingStartDate).' AND '. (strtotime($shippingEndDate)+86400);
	}
	
	if(!empty($paymentStartDate) && !empty($paymentEndDate)) {
		$paymentStr = ' cod_payment_date between '.strtotime($paymentStartDate).' AND '. (strtotime($paymentEndDate)+86400);
	}
	
	if(!empty($orderStartDate) && !empty($orderEndDate)) {
		$orderStr = ' date between '.strtotime($orderStartDate).' AND '. (strtotime($orderEndDate)+86400);
	}
	
	if(!empty($orderStartValue) && !empty($orderEndValue)) {
		$orderValueStr = ' (total + cod) between '.$orderStartValue.' AND '.$orderEndValue;
	}
	
	if(!empty($payStatus)) {
		$payStatusStr  = ' cod_pay_status IN ('.getQueryString($payStatus).')';	
	} else {
		$payStatusStr  = ' cod_pay_status != \'\'';
	}
	
	if(!empty($orderId)) {
		$orderIdStr  = ' o.orderid IN ('.$orderId.')';
	}
	if(!empty($awbno)) {
		$awbnoStr  = 'tracking IN ('.getQueryString($awbno).')';
	}
	if(!empty($customerName)) {
		$customerStr = ' login LIKE \'%'.$customerName.'%\'';
	}
	
	$orderByDate = ' order by orderdate desc';
	
	$cod_search_query = $queryBaseString;
	$cod_search_query .= (empty($shippingStr))?'':" AND $shippingStr ";
	$cod_search_query .= (empty($paymentStr))?'':" AND $paymentStr ";
	$cod_search_query .= (empty($orderStr))?'':" AND  $orderStr ";
	$cod_search_query .= (empty($orderValueStr))?'':" AND $orderValueStr ";
	$cod_search_query .= (empty($payStatusStr))?'':"AND $payStatusStr ";
	$cod_search_query .= (empty($orderIdStr))?'':" AND $orderIdStr ";
	$cod_search_query .= (empty($awbnoStr))?'':" AND $awbnoStr ";
	$cod_search_query .= (empty($customerName))?'':" AND $customerStr ";

	$cod_search_query .= $orderByDate;
	
	$limitStr = ' limit 30000';
	$cod_search_query .= $limitStr;
	
	global $sqllog;
	$sqllog->debug("SQL Query : ".$cod_search_query." "." @ line "._LINE_." in file ".__FILE__);
	return func_query($cod_search_query, true);
	
}

function lost_items_data($orderId, $awbno) {
	$queryBaseString = 'select xcart_orders.orderid, FROM_UNIXTIME(completeddate), itemid, product_type, product_style, tracking, total_amount, item_status from xcart_orders, xcart_order_details where xcart_orders.orderid = xcart_order_details.orderid AND  cod_pay_status != \'\' AND payment_method=\'cod\' ';

	if(!empty($orderId)) {
		$orderIdStr  = ' xcart_orders.orderid IN ('.$orderId.')';
	}
	if(!empty($awbno)) {
		$awbnoStr  = ' tracking IN ('.getQueryString($awbno).')';
	}
	$lost_items_query = $queryBaseString;
	$lost_items_query .= (empty($orderIdStr))?'':" AND $orderIdStr ";
	$lost_items_query .= (empty($awbnoStr))?'':" AND $awbnoStr ";
	//echo $lost_items_query;
	global $sqllog;
	$sqllog->debug("SQL Query : ".$lost_items_query." "." @ line "._LINE_." in file ".__FILE__);
	return func_query($lost_items_query, true);
}

function construct_search_string($postData, $nonCod=false) {

	if(!empty($postData['shippingStartDate']) && !empty($postData['shippingEndDate'])) {
		$shippingStr = ' shippeddate between '.strtotime($postData['shippingStartDate']).' AND '. (strtotime($postData['shippingEndDate'])+86400);
	}
	if(!empty($postData['paymentStartDate']) && !empty($postData['paymentEndDate'])) {
		$paymentStr = ' cod_payment_date between '.strtotime($postData['paymentStartDate']).' AND '. (strtotime($postData['paymentEndDate'])+86400);
	}
	if(!empty($postData['orderStartDate']) && !empty($postData['orderEndDate'])) {
		$orderStr = ' date between '.strtotime($postData['orderStartDate']).' AND '. (strtotime($postData['orderEndDate'])+86400);
	}
	if(!empty($postData['orderStartValue']) && !empty($postData['orderEndValue'])) {
		$orderValueStr = ' (total + shipping_cost + cod) between '.$postData['orderStartValue'].' AND '.$postData['orderEndValue'];
	}
	if(!empty($postData['orderId'])) {
		$orderIdStr  = ' xcart_orders.orderid IN ('.$postData['orderId'].')';
	}
	if(!empty($postData['awbno'])) {
		$awbnoStr  = 'tracking IN ('.getQueryString($postData['awbno']).')';
	}
	if(!empty($postData['payStatus'])) {
		$payStatusStr  = ' cod_pay_status IN ('.getQueryString($postData['payStatus']).')';
	} else {
		$payStatusStr  = ' cod_pay_status != \'\'';
	}
	
	$shippingOrderStr = " AND (xcart_orders.status='SH' or xcart_orders.status='DL') ";
	
	$cod_search_query.= (empty($shippingStr))?'':" AND $shippingStr ";
	$cod_search_query.= (empty($paymentStr))?'':" AND $paymentStr ";
	$cod_search_query.= (empty($orderStr))?'':" AND  $orderStr ";
	$cod_search_query.= (empty($orderValueStr))?'':" AND $orderValueStr ";
	$cod_search_query.= (empty($orderIdStr))?'':" AND $orderIdStr ";
	$cod_search_query.= (empty($awbnoStr))?'':" AND $awbnoStr ";
	if($nonCod == false) {
		$cod_search_query.= (empty($payStatusStr))?'':" AND $payStatusStr ";
	}
	$cod_search_query.=$shippingOrderStr;

	return $cod_search_query;
}
/* This function will return total no of orders and their value for a given pay status
 * Possible type for pay status are { "paid", "unpaid", "pending", "writtenoff" }
 *
 **/

function get_order_number_value($payStatus, $postData) {
	global $sqllog;
	if(empty($postData)) {
		return;
	}
	if(!empty($payStatus)) {
		$payStatusStr  = ' cod_pay_status IN ('.getQueryString($payStatus).') AND ';
	} else {
		$payStatusStr = 'cod_pay_status !=\'\' AND ';
	}
	$queryString = 'SELECT "cod_pay_status", count(orderid) as total_count , sum(total + cod) as total_amount FROM xcart_orders WHERE '.$payStatusStr.' payment_method =\'cod\'';

	$search_string = construct_search_string($postData);
	//we want to calculate values only for orders that have been shipped.
	$discardUnshippedStr = ' AND shippeddate is not null';
	$queryString .= $search_string . $discardUnshippedStr;


	$sqllog->debug("SQL Query : ".$queryString." "." @ line "._LINE_." in file ".__FILE__);
	return func_query($queryString, true);
}
/* This function return total payment paid/unpaid/pending for for a given dayCount( diff between current
 * date and shipped date)
 */
function pending_payment_data($startDateRange, $endDateRange, $payStatus, $postData) {
	global $sqllog;
	if(empty($postData)) {
		return;
	}
	$payStatusStr  = ' cod_pay_status IN ('.getQueryString($payStatus).') AND ';
	$queryString = 'SELECT "cod_pay_status", sum(total + shipping_cost + cod) as total_amount FROM xcart_orders WHERE '.$payStatusStr. ' payment_method=\'cod\' AND (UNIX_TIMESTAMP(NOW()) - shippeddate) BETWEEN ' ;
	$queryString.= $startDateRange*86400 . ' AND '. $endDateRange*86400;
	$search_string = construct_search_string($postData);
	$queryString .= $search_string;
	$sqllog->debug("SQL Query : ".$queryString." "." @ line "._LINE_." in file ".__FILE__);
	return func_query($queryString, true);
}

/**
 *
 * This function is written to get information for cod transcations and value both. Ideally We should compute it seprately
 *
 * @param $postData It has info about all the dates that need to be passed to function
 */
function other_then_cod_revenue($postData){
	global $sqllog;
	if(empty($postData)) {
		return;
	}
	$queryString = 'SELECT status, count(orderid) as total_count, sum(total + cod) as total_amount FROM xcart_orders where payment_method != \'cod\' and status IN (\'C\', \'WP\', \'Q\') and source_id=1 and (orgid=0 or orgid is null)';
	$removeFreeMug = 'AND orderid NOT IN ( select a.orderid from xcart_order_details b, xcart_orders a , mk_style_category_map c where a.orderid=b.orderid and  b.product_style =c.styleid and c.categoryid=162 and a.coupon_discount=\'249.00\'  AND a.shipping_cost = \'50.00\' and a.total=\'0.00\' and a.coupon is not null) ';
	$queryString   .= $removeFreeMug;
	$search_string = construct_search_string($postData, true);
	$queryString .= $search_string;
	$sqllog->debug("SQL Query : ".$queryString." "." @ line "._LINE_." in file ".__FILE__);
	return func_query($queryString, true);
}
/* This function is to get total no of items for a given item status */
function item_delivery_status_count($itemStatus, $postData) {
	if(empty($postData) || empty($postData)) {
		return;
	}
	$queryString = 'SELECT item_status, sum(qtyInOrder) FROM xcart_order_details, xcart_orders WHERE xcart_orders.orderid = xcart_order_details.orderid AND item_status IN ( ' . getQueryString($itemStatus). ' ) and payment_method=\'cod\' AND cod_pay_status !=\'\'';
	$search_string = construct_search_string($postData);
	$queryString .= $search_string;
	global $sqllog;
	$sqllog->debug("SQL Query : ".$queryString." "." @ line "._LINE_." in file ".__FILE__);
	return func_query($queryString, true);

}

/* function to get all cod  item count for a given date range */

function cod_get_item_count($postData) {
	if(empty($postData) || empty($postData)) {
		return;
	}
	$queryString = 'SELECT sum(qtyInOrder) FROM xcart_order_details, xcart_orders WHERE xcart_orders.orderid = xcart_order_details.orderid AND payment_method=\'cod\' AND cod_pay_status !=\'\'';
	$search_string = construct_search_string($postData);
	$queryString .= $search_string;
	global $sqllog;
	$sqllog->debug("SQL Query : ".$queryString." "." @ line "._LINE_." in file ".__FILE__);
	return func_query($queryString, true);
}

function non_cod_get_item_count($postData) {
	global $sqllog;
	if(empty($postData)) {
		return;
	}
	$queryString = 'SELECT sum(amount) FROM xcart_order_details, xcart_orders WHERE xcart_orders.orderid = xcart_order_details.orderid AND payment_method!=\'cod\' AND cod_pay_status !=\'\' and status IN (\'C\', \'WP\', \'Q\') and source_id=1 and (orgid=0 or orgid is null)';
	$removeFreeMug = 'AND orderid NOT IN ( select a.orderid from xcart_order_details b, xcart_orders a , mk_style_category_map c where a.orderid=b.orderid and  b.product_style =c.styleid and c.categoryid=162 and a.coupon_discount=\'249.00\'  AND a.shipping_cost = \'50.00\' and a.total=\'0.00\' and a.coupon is not null) ';
	$queryString   .= $removeFreeMug;
	$search_string = construct_search_string($postData, true);
	$queryString .= $search_string;
	$sqllog->debug("SQL Query : ".$queryString." "." @ line "._LINE_." in file ".__FILE__);
	return func_query($queryString, true);

}

/**
 * Function to get total transcarion info for a given date range
 */
function cod_transcation_info($postData) {
	if(empty($postData)) {
		return;
	}
	$payStatusStr = 'cod_pay_status !=\'\' AND ';
	$queryString = 'select count(orderid) as total_count  FROM xcart_orders WHERE '.$payStatusStr.' payment_method =\'cod\'';
	$search_string = construct_search_string($postData);
	$queryString .= $search_string;
	global $sqllog;
	$sqllog->debug("SQL Query : ".$queryString." "." @ line "._LINE_." in file ".__FILE__);
	return func_query($queryString, true);
}
function cod_value_info($postData) {
	if(empty($postData) || empty($postData)) {
		return;
	}
	if(!empty($payStatus)) {
		$payStatusStr = 'cod_pay_status =\''.$payStatus.'\' AND';
	} else {
		$payStatusStr = 'cod_pay_status !=\'\' AND ';
	}
	$queryString = 'select sum(total + cod) as total_amount  FROM xcart_orders WHERE '.$payStatusStr.' payment_method =\'cod\'';
	$search_string = construct_search_string($postData);
	$queryString .= $search_string;
	global $sqllog;
	$sqllog->debug("SQL Query : ".$queryString." "." @ line "._LINE_." in file ".__FILE__);
	return func_query($queryString, true);
}

function cod_item_per_order($postData) {
	if(empty($postData) || empty($postData)) {
		return;
	}
	if(!empty($payStatus)) {
		$payStatusStr = 'cod_pay_status =\''.$payStatus.'\' AND';
	} else {
		$payStatusStr = 'cod_pay_status !=\'\' AND ';
	}
	$queryString = 'select sum(qtyInOrder)/count(orderid) as item_number FROM xcart_orders WHERE '.$payStatusStr.' payment_method =\'cod\'';
	$search_string = construct_search_string($postData);
	$queryString .= $search_string;
	global $sqllog;
	$sqllog->debug("SQL Query : ".$queryString." "." @ line "._LINE_." in file ".__FILE__);
	return func_query($queryString, true);


}

function non_cod_transcation_info($postData) {
	global $sqllog;
	if(empty($postData)) {
		return;
	}
	$queryString = 'SELECT count(orderid) as total_count FROM xcart_orders where payment_method != \'cod\' and status IN (\'C\', \'WP\', \'Q\') and source_id=1 and (orgid=0 or orgid is null)';
	$removeFreeMug = 'AND orderid NOT IN ( select a.orderid from xcart_order_details b, xcart_orders a , mk_style_category_map c where a.orderid=b.orderid and  b.product_style =c.styleid and c.categoryid=162 and a.coupon_discount=\'249.00\'  AND a.shipping_cost = \'50.00\' and a.total=\'0.00\' and a.coupon is not null) ';
	$queryString   .= $removeFreeMug;
	$search_string = construct_search_string($postData , true);
	$queryString .= $search_string;
	$sqllog->debug("SQL Query : ".$queryString." "." @ line "._LINE_." in file ".__FILE__);
	return func_query($queryString, true);


}

function non_cod_value_info($postData){
	global $sqllog;
	if(empty($postData)) {
		return;
	}
	$queryString = 'SELECT sum(total + cod) as total_amount FROM xcart_orders where payment_method != \'cod\' and status IN (\'C\', \'WP\', \'Q\') and source_id=1 and (orgid=0 or orgid is null)';
	$removeFreeMug = 'AND orderid NOT IN ( select a.orderid from xcart_order_details b, xcart_orders a , mk_style_category_map c where a.orderid=b.orderid and  b.product_style =c.styleid and c.categoryid=162 and a.coupon_discount=\'249.00\'  AND a.shipping_cost = \'50.00\' and a.total=\'0.00\' and a.coupon is not null) ';
	$queryString   .= $removeFreeMug;
	$search_string = construct_search_string($postData, true);
	$queryString .= $search_string;
	$sqllog->debug("SQL Query : ".$queryString." "." @ line "._LINE_." in file ".__FILE__);
	return func_query($queryString, true);

}

function non_cod_item_per_order($postData) {
	global $sqllog;
	if(empty($postData)) {
		return;
	}
	$queryString = 'SELECT sum(qtyInOrder)/count(orderid) as item_number FROM xcart_orders where payment_method != \'cod\' and status IN (\'C\', \'WP\', \'Q\') and source_id=1 and (orgid=0 or orgid is null)';
	$removeFreeMug = 'AND orderid NOT IN ( select a.orderid from xcart_order_details b, xcart_orders a , mk_style_category_map c where a.orderid=b.orderid and  b.product_style =c.styleid and c.categoryid=162 and a.coupon_discount=\'249.00\'  AND a.shipping_cost = \'50.00\' and a.total=\'0.00\' and a.coupon is not null) ';
	$queryString   .= $removeFreeMug;
	$search_string = construct_search_string($postData, true);
	$queryString .= $search_string;
	$sqllog->debug("SQL Query : ".$queryString." "." @ line "._LINE_." in file ".__FILE__);
	return func_query($queryString, true);

}
/**
 *
 * This function is used to print data in scs format for a given array
 * @param array $inputArray      An array to chaneg in to csv format
 * @param string $fileNameInitial  Initial string to add for file name
 */
function printAsCsv($inputArray, $fileNameInitial) {
	$file_name_suffix = date("dmy");
	$format_tight = str_replace (" ", "", $format);
	$filename="$fileNameInitial"."_".$format_tight."_".$file_name_suffix;
	header('Content-Type: application/csv');
	header("Content-disposition: attachment; filename=$filename.csv");
	ob_clean();
	flush();
	$string='';
	foreach($inputArray AS $key=>$array) {
		if($key==0){ // if first row, print the headers.
			foreach($array as $key=>$value) {
				$string .= $key.",";
			}
			$string = $var = substr($string,0,-1);
			$string .="\r\n";
		}
		foreach($array as $key=>$value) {
			if(!empty($value)){
				//if the value contains char:',' , strip it out
				if(stripos($value, ',')!== false){	
					$valueArray = explode(",", $value);
					$value = implode(";", $valueArray);
				}
				$string .= $value.",";
			} else {
				$string .=" ,";
			}
		}
		$string = $var = substr($string,0,-1);
		$string .="\r\n";
	}
	echo "$string";
	die();
}

function get_percentage_return($postData) {
	// For percentage return you need to use shipping date which is "completeddate" in xcart_orders table

	global $sqllog;
	$queryString = "select count(orderid) as unpaid_count from xcart_orders where cod_pay_status='unpaid' and payment_method='cod'";
	$queryString .= construct_search_string($postData);
	$sqllog->debug("SQL Query : ".$queryString." "." @ line ".__LINE__." in file ".__FILE__);
	$unpaidCount = func_query($queryString, true);
	$queryString = "select count(orderid) as shipped_count from xcart_orders where cod_pay_status !='' and payment_method='cod' and completeddate <=
(select max(completeddate) from xcart_orders where cod_pay_status='unpaid' and payment_method='cod')";
	$queryString .= construct_search_string($postData);
	$shippedCount = func_query($queryString, true);
	return ($unpaidCount[0]['unpaid_count']/$shippedCount[0]['shipped_count'])*100;

}
/* this function return you the value for avgPayment cycle that is defined as avg settlement time(cod_payment_date - shipping date) for a order in  given
 *  payment period
 */

function get_avg_payment_cycle($postData) {

	global $sqllog;
	$queryString = "select (sum(cod_payment_date - shippeddate)) as avg_payment_cycle, count(orderid) as total_count from xcart_orders where payment_method='cod' and cod_pay_status='paid' and cod_payment_date !=0 and cod_payment_date is not null and status='C'";
	$queryString .= construct_search_string($postData);
	$sqllog->debug("SQL Query : ".$queryString." "." @ line ".__LINE__." in file ".__FILE__);
	$avgPaymentCycle = func_query($queryString, true);
	$avgPaymentCycleInDays = round(($avgPaymentCycle[0]['avg_payment_cycle']/(86400*$avgPaymentCycle[0]['total_count'])) , 2);
	$sqllog->debug("Avg Payment Cycle:: ".$avgPaymentCycleInDays." "." @ line ".__LINE__." in file ".__FILE__);
	return 	$avgPaymentCycleInDays;
}
function update_bluedart_zipcode($zipcodes) {
	$validZipcodes = array();
	$index = 0;
	if(is_array($zipcodes)) {
		foreach($zipcodes as $singlezipcode) {
			$singlezipcode = trim($singlezipcode);
				
			if(!empty($singlezipcode)) {
				$code = substr($singlezipcode, 0, 6); //preg_replace("/[^0-9]/","",$singlezipcode);
				$validZipcodes[$index] = $code;
				$index++;
			}
		}
	}

	$validZipcodes = array_unique($validZipcodes);
	sort($validZipcodes);
	$validZipcodes = array_values($validZipcodes);

	// At this point, we have an array of zipcodes, without nulls/duplicates, and in sorted order.

	// We delete all existing blue dart provided pin codes, and then reinsert all of them again.
	$queryString = "delete from mk_cod_serviceable_zipcode where zipcode!='' and provider='BD'";
	db_query($queryString);

	$oneLargeQuery = "insert into mk_cod_serviceable_zipcode(provider, zipcode) values ";
	$zipcount = count($validZipcodes);
	$i=1;
	foreach($validZipcodes as $singleValidZipcode) {
		if($i>=$zipcount)
		break;
		$i++;
		$oneLargeQuery = $oneLargeQuery . "('BD', '$singleValidZipcode'),";
	}

	$lastentry = $validZipcodes[count($validZipcodes)-1];
	$oneLargeQuery = $oneLargeQuery . "('BD', '$lastentry')";

	// A single query to insert all values is much faster compares to multiple insert statements.
	db_query($oneLargeQuery);
}
?>
