<?php
require_once("./auth.php");
//require_once $xcart_dir."/include/security.php";

$mode= $_POST['mode'];
if($mode=='checknewrulefilters') {
	$brand = $_POST['brand'];
	$articletype = $_POST['articletype'];
	$fashion = $_POST['fashion'];
	
	$findMatchingRuleQuery = "select ruleid, display_name from mk_discount_rules ";
	$findMatchingRuleQuery.= "where brand='$brand' and articletype='$articletype' and fashion='$fashion' and ruletype=0";
	
	$resultsMatch = func_query_first($findMatchingRuleQuery);
	
	if(empty($resultsMatch)) {
		$resultArray = json_encode(array('status'=>0));
		echo $resultArray; 
		exit;
	} else {
		$message = "Rule cannot be added as a rule with $brand -- $articletype -- $fashion is already set.\n\n";
		$message.= "Rule #" . $resultsMatch['ruleid'] . " : " . $resultsMatch['display_name'];
		$resultArray = json_encode(array('status'=>1, 'message'=>$message));
		echo $resultArray;
		exit;
	}
}

$styleids = $_POST['styleids'];

if(empty($styleids)) {
	$resultArray = json_encode(array('status'=>0));
	echo $resultArray;
	exit;
}

$styleids = explode(",", $styleids);
$styleids = array_filter(array_unique(array_map("trim", $styleids)));
$styleids = implode(",", $styleids);

$dynamicRuleMatchQuery = "select sp.style_id, sp.product_display_name as stylename, dr.ruleid, dr.display_name as rulename from mk_style_properties sp, mk_discount_rules dr ";
$dynamicRuleMatchQuery.= "  where sp.discount_rule=dr.ruleid and sp.style_id in ($styleids) and dr.ruletype=0 order by sp.discount_rule";
$resultsDynamicMatch = func_query($dynamicRuleMatchQuery);

if(!empty($resultsDynamicMatch)) {
	$alertString = "This Static Rule Cannot be created or updated.\nSome of the Styles selected, are already applied to Dynamic rules.";
	
	foreach ($resultsDynamicMatch as $singlematch) {
		$alertString.= "\n\n(" . $singlematch['style_id'] . ") " . $singlematch['stylename'];
		$alertString.= " -- Matched Rule #" . $singlematch['ruleid'] . " : " . $singlematch['rulename'];
	}
	
	$resultArray = json_encode(array('status'=>1, 'message'=>$alertString));
	echo $resultArray;
	exit;
}

$specialRulesMatchQuery = "select sp.style_id, sp.product_display_name as stylename, dr.ruleid, dr.display_name as rulename from mk_style_properties sp, mk_discount_rules dr ";
$specialRulesMatchQuery.= "  where sp.discount_rule=dr.ruleid and sp.style_id in ($styleids) and dr.ruletype=1 order by sp.discount_rule";
$resultsSpecialMatch = func_query($specialRulesMatchQuery);

if(!empty($resultsSpecialMatch)) {
	$alertString = "Some of the styles already have Static Rules Applied to them.\nDo you wish to override them with new static rule ?\n";
	
	foreach ($resultsSpecialMatch as $singlematch) {
		$alertString.= "\n\n(" . $singlematch['style_id'] . ") " . $singlematch['stylename'];
		$alertString.= " -- Matched Static Rule #" . $singlematch['ruleid'] . " : " . $singlematch['rulename'];
	}
	
	$resultArray = json_encode(array('status'=>2, 'message'=>$alertString));
	echo $resultArray;
	exit;
}

$resultArray = json_encode(array('status'=>0));
echo $resultArray;
exit;
?>
