<?php
require "./auth.php";
require $xcart_dir."/include/security.php";

if ($REQUEST_METHOD == "POST"){
	  if(!isset($_FILES) && isset($HTTP_POST_FILES))
			 $_FILES = $HTTP_POST_FILES;
		if(!empty($_FILES['img']['name']) && $_POST['imgtype'] == 'otherimg'){	
			$other_img =  'images/style/O/'.$_FILES['img']['name'];
     		$uploaddir2 = '../images/style/O/';
			$uploadfile2 = $uploaddir2 . basename($_FILES['img']['name']);
			
			if (@move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile2)){
				define("OTHERIMAGE",$other_img);
			}
			else{
				define("OTHERIMAGE",'');
		    }
		}
	  if($mode == "upload"){
	        if($imgtype == 'otherimg'){
			    $query_data = array(
							"styleid" => $tid,
	   						"image_path" => OTHERIMAGE,
							"type" => 'O',
							"isdefault"=>'N',
							"name"=>$imgname);
				func_array2insert("mk_style_images",$query_data); 
		   }
		   $success = "Y";
		   $message = func_get_langvar_by_name("msg_adm_image_upd");		   
      }
}
$smarty->assign("typeid",$id);
$smarty->assign("imageid",$imageid);
$smarty->assign("message",$message);
$smarty->assign("success",$success);

func_display("admin/main/uploadmoreimage.tpl",$smarty);
?>
