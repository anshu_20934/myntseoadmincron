<?php

require "./auth.php";
require $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.order.php";
require_once 'codAdminHelper.php';
$action  = $HTTP_POST_VARS['action'];
$uploadStatus = false;

$mode = $_POST['mode'];

if($mode=='upload') {

	if($action == '')
	$action ='upload';

	if ($REQUEST_METHOD == "POST" ) {
		
		if(!isset($_FILES) && isset($HTTP_POST_FILES))
			$_FILES = $HTTP_POST_FILES;

		if(!empty($_FILES['orderdetail']['name'])) {
			
			$uploaddir1 = '../bulkorderupdates/';
			// Note.. We are using this bulkorderupdates directory, is at already exists.
				
			$extension = explode(".",basename($_FILES['orderdetail']['name']));

			$uploadfile1 = $uploaddir1.'bluedartzipcode_'.date('d-m-Y-h-i-s').".".$extension[1];

			if (move_uploaded_file($_FILES['orderdetail']['tmp_name'], $uploadfile1)) {
				$uploadStatus = true;
			}
		}

		if($uploadStatus)
		{
			// open the text file
			$fd = fopen ($uploadfile1, "r");
			$zipcodes =  array();
			$k = 0;
			while (!feof ($fd)) {
				$buffer = fgets($fd);
				$zipcodes[$k] = $buffer;
				$k++;
			}
				
			fclose ($fd);


			update_bluedart_zipcode($zipcodes);
			
			$action = "uploadcompleted";
		}
		else
		{
			$action = "upload";
			$msg =  "File not uploaded, try again ..";
		}
		$smarty->assign('message',$msg);

	}
}

	
$smarty->assign('action',$action);
$smarty->assign('mode',$mode);
$smarty->assign('post_data', $_POST);
$template ="bluedart_servicability";
$smarty->assign("main",$template);

func_display("admin/home.tpl",$smarty);

?>
