<?php
/*
 * Created on Oct 17, 2012
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../auth.php");
 include_once("$xcart_dir/include/class/leaderboard/LeaderBoard.php");
 use leaderboard\LeaderBoard; 
 $action  = $HTTP_POST_VARS['action'];
 
 if($action == 'updateTickerMessage') {
 	$lbId = $_POST['leaderboard']; 	
 	LeaderBoard::updateLeaderBoardFields($lbId, mysql_real_escape_string($_POST['ticker-message']), mysql_real_escape_string($_POST['title']), mysql_real_escape_string($_POST['banner-url']), mysql_real_escape_string($_POST['banner-href']));
 	$smarty->assign("successMessage", "Updated Ticker Message Successfully");
 }
 
 $leaderboards = LeaderBoard::getAllLeaderBoards();
 $smarty->assign("leaderboards", $leaderboards);
 
 $leaderboard2Messages = LeaderBoard::getAllLeaderBoardFields();
 $smarty->assign("leaderboardmessages", json_encode($leaderboard2Messages));
 
 $smarty->assign("main", 'manage_leaderboard');
 func_display("admin/home.tpl", $smarty);
 
?>
