<?php
require_once "./auth.php";
require $xcart_dir."/include/security.php";
require_once($xcart_dir.'/include/search/class.SearchURLGenerator.php');
require_once($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");
require_once("$xcart_dir/include/solr/solrProducts.php");
function mysort($a,$b){
    return sizeof($b)-sizeof($a);
}
$stringFieldArray = json_decode(FeatureGateKeyValuePairs::getFeatureGateValueForKey("paremetricSearchStringFields"));
$smarty->assign("stringFieldArray",$stringFieldArray);
$facetValues = array();
$response;
try {
$solrProducts =  new solrProducts();
	$aParams = array("facet"=>"true","facet.field"=>$row,"facet.limit"=>10);
		$response = $solrProducts->multipleFacetedSearch("*:*", $stringFieldArray)->facet_fields;
	$response = (array)$response;
}
catch(Exception $e)
{
	echo"<pre>";print_r("Some Error Occured");exit;
}


uasort($response,'mysort');
$ultimateArray =array();
$pos = 0;
foreach ($response as $key=>$row)
{
	
	$row =     (array)$row;
	$row = array_keys($row);
	$rowString = "";
	for($i=0;$i<sizeof($row);$i++)
	{
		if($i%10==0)
		{
			$rowString = 	$rowString."\n";	
		}
		else
		{
			$rowString = $rowString.", ";
		}
		$rowString = $rowString.$row[$i];
	}
	$rowString = trim($rowString);
	$ultimateArray[$pos]['keyIndex'] = $key;
	$ultimateArray[$pos]['keyValue'] = $rowString;
	$pos = $pos+1;
}
$smarty->assign("ultimateArray",$ultimateArray);
$smarty->assign("main","solr_search_help");
$smarty->assign("message",$message);
func_display("admin/home.tpl",$smarty);
?>
