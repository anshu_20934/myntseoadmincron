<?php
require "./auth.php";
require_once $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.mkcore.php";
require_once $xcart_dir."/include/func/func.courier.php";
require_once $xcart_dir."/PHPExcel/PHPExcel.php";
include_once($xcart_dir."/modules/apiclient/CourierApiClient.php");
include_once($xcart_dir."/modules/apiclient/WarehouseApiClient.php");
use style\builder\CachedStyleBuilder;

$action = $_POST['action'];
if(!$action)
	$action = 'view';

if ($action == 'zipcodelookup') {
	$zipcode = $HTTP_POST_VARS['zipcode'];
	
	if(empty($zipcode)){
		global $sql_tbl;
		$orderId = $HTTP_POST_VARS['orderId'];
		if(!empty($orderId)){
			$orders_sql = "SELECT s_zipcode from $sql_tbl[orders] where orderid = $orderId";
			$sZipcode = func_query_first_cell($orders_sql, true);
			if(!empty($sZipcode)){
				$zipcodeTmp = $sZipcode;
			}
		}
	}else{
		$zipcodeTmp = $zipcode;
	}
	
	$whId2SupportedCouriers = array();
	$supported_couriers = array();
	if(isset($zipcodeTmp)){
		/**
		 * TODO: courier serviceability this is deprecated
		 */
		
		$supported_couriers = func_get_serviceable_couriers_for_zipcode($zipcodeTmp);
		$whId2SupportedCouriers = array();
		if($courierServiceabilityVersion == 'new') {
			$styleId = $HTTP_POST_VARS['styleId'];
			if(!empty($styleId)) {
				$skuRow = func_query_first("Select sku_id from mk_styles_options_skus_mapping som, mk_product_options po where som.option_id = po.id and po.is_active = 1 and som.style_id = $styleId", true);
				//$supported_couriers = func_get_serviceable_couriers_for_zipcode($zipcodeTmp);
				$cachedStyleBuilderObj=CachedStyleBuilder::getInstance();
				$styleObj=$cachedStyleBuilderObj->getStyleObject($styleId);
				$discountData= $styleObj->getDiscountData();
				$discountAmount = $discountData->discountAmount;
				$productStyleDetails=$styleObj->getProductStyleDetails();
				$originalprice = $productStyleDetails["price"];
				$product = array('style_id'=> $styleId, 'sku_id'=>$skuRow['sku_id'], 'totalProductPrice'=>$originalprice, 'discount'=>$discountAmount, 'quantity'=>1);
				
				$skuId2InvRowMap = AtpApiClient::getAvailableInventoryForSkus($skuRow['sku_id']);
				$warehouses = explode(",", $skuId2InvRowMap[$skuRow['sku_id']]['availableInWarehouses']); 
				if(!empty($warehouses)) {
					
					$skuDetails = array();
					foreach($warehouses as $whId) {
						$skuDetails[$skuRow['sku_id']]['warehouse2AvailableItems'][$whId] = 1; 
					}
					
					$skuId2WhCourier = getCourierServiceabilityForProdcut($zipcodeTmp, array($product), $skuDetails);
					if($skuId2WhCourier && !empty($skuId2WhCourier) && !empty($skuId2WhCourier[$skuRow['sku_id']])) {
						$whId2Couriers = $skuId2WhCourier[$skuRow['sku_id']];
						
						$courierCode2CourierMap = array();
						foreach($supported_couriers as $row) {
							$courierCode2CourierMap[$row['courierCode']] = $row;
						}
						
						$whId2WarehouseName = WarehouseApiClient::getWarehouseId2NameMap();
						foreach($whId2Couriers as $whId => $couriers) {
							$whId2SupportedCouriers[$whId2WarehouseName[$whId]] = array();
							foreach($couriers as $code) {
								$whId2SupportedCouriers[$whId2WarehouseName[$whId]][] = $courierCode2CourierMap[$code];
							}
						}
					}
				}
				$smarty->assign("styleId", $styleId);
			} else {
				$whId2SupportedCouriers = array('All Warehouses' => $supported_couriers);
			}
		} else {
			$whId2SupportedCouriers = array('All Warehouses' => $supported_couriers);
		}
	}
	$smarty->assign("zipcode", $zipcode);
	$smarty->assign("orderId", $orderId);
	$smarty->assign("whId2SupportedCouriers", $whId2SupportedCouriers);
}

$smarty->assign("action", $action);
/* $couriers = get_active_courier_partners();
$smarty->assign("couriers", $couriers); 
$all_couriers = get_all_courier_partners();
$smarty->assign("all_couriers", $all_couriers); */
$smarty->assign("main", "courier_serviceability");
func_display("admin/home.tpl",$smarty);
?>
