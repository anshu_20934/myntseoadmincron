<?php

include_once("auth.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/modules/amqpclients/producer/AMQPMessageProducer.php");
include_once("getBountyOMSNotInSyncCount.php");
require_once "WMSDBConnUtil.php";

function updateInventory($wmsMoreBocQueries,$wmsLessBocQueries,$imsFKMoreBocQueries,$imsFKLessBocQueries,$allSkusSellers,$oosSkuQueries,$undersellSkuQueries,$unRlsdSkuQueries,$avlblUnrlsdSkuQueries,&$errQueries, $checkBountyLag){
$imsConn = WMSDBConnUtil::getConnection('imsdb1.myntra.com:3306', TRUE, WMSDBConnUtil::IMS_WRITE);
$atpConn = WMSDBConnUtil::getConnection('atpdbmaster.myntra.com:3306', TRUE, WMSDBConnUtil::IMS_WRITE);


if(mysql_select_db('myntra_ims', $imsConn ) === false) {
    $errQueries = $errQueries."IMS DB unaccessible\n";
    exit;
  }
  if(mysql_select_db('myntra_atp', $atpConn ) === false) {
    $errQueries = $errQueries."ATP DB unaccessible\n";
    exit;
  }
  $client = AMQPMessageProducer::getInstance();
  $maxUnderselling=300;
  $checkBountyLag=true;
  if($checkBountyLag){
    $bountyLag=0;
    getOrderMismatchCount($bountyLag);

  echo $bountyLag."---bounty lag";
  $errQueries =$errQueries .$bountyLag."---bounty lag";

  if ($bountyLag>0) {
    $maxUnderselling=0;
  }
}


  if (count($wmsMoreBocQueries)<$maxUnderselling) {
    foreach ($wmsMoreBocQueries as $query) {
      $result = mysql_query($query, $imsConn);
      if(!$result){
        $errQueries = $errQueries.$query."\n";
      }
      @mysql_free_result($result);
    }
  }
  foreach ($wmsLessBocQueries as $query) {
    $result = mysql_query($query, $imsConn);
    if(!$result){
      $errQueries = $errQueries.$query."\n";
    }
    @mysql_free_result($result);
  }

  foreach ($imsFKMoreBocQueries as $query) {
    $result = mysql_query($query, $imsConn);
    if(!$result){
      $errQueries = $errQueries.$query."\n";
    }
    @mysql_free_result($result);
  }

  foreach ($imsFKLessBocQueries as $query) {
    $result = mysql_query($query, $imsConn);
    if(!$result){
      $errQueries = $errQueries.$query."\n";
    }
    @mysql_free_result($result);
  }

  foreach ($oosSkuQueries as $query) {
    $result = mysql_query($query, $atpConn);
    if(!$result){
      $errQueries = $errQueries.$query."\n";
    }
    @mysql_free_result($result);
  }

  if ( count($undersellSkuQueries) + count($unRlsdSkuQueries) + count($avlblUnrlsdSkuQueries) <$maxUnderselling) {
    if (count($undersellSkuQueries)<$maxUnderselling) {
      foreach ($undersellSkuQueries as $query) {
        $result = mysql_query($query, $atpConn);
        if(!$result){
          $errQueries = $errQueries.$query."\n";
        }
        @mysql_free_result($result);
      }
    }

    if (count($unRlsdSkuQueries)<$maxUnderselling) {
      foreach ($unRlsdSkuQueries as $query) {
        $result = mysql_query($query, $atpConn);
        if(!$result){
          $errQueries = $errQueries.$query."\n";
        }
        @mysql_free_result($result);
      }
    }

    if (count($avlblUnrlsdSkuQueries)<$maxUnderselling) {
      foreach ($avlblUnrlsdSkuQueries as $query) {
        $result = mysql_query($query, $atpConn);
        if(!$result){
          $errQueries = $errQueries.$query."\n";
        }
        @mysql_free_result($result);
      }
    }
  }
  //echo "reindexing SKUs\n";
  /*  foreach (array_chunk(array_unique($allSkusSellers), 100) as $ids) {
      $request = array('handler' => 'Skus', 'data' => array('userId' => 'oms2Atp', 'action' => 'reindex') );
      foreach($ids as $skuId) {
        $request['data']['skus'][] = array('sku' => array('id' => $skuId));
      }
      if(!empty($request['data']['skus'])) {
        $client->sendMessage('reindexSkuQueue', $request, 'PortalApiRequest','xml',true);
      }
    }
  */

  foreach (array_unique($allSkusSellers) as $skuSeller) {
    $startIndx = strpos($skuSeller,"_");
    $sellerId=substr($skuSeller, $startIndx+1);
    $skuId=substr($skuSeller, 0, $startIndx);
    $inventoryEntry = array('storeId' => '1', 'sellerId' => $sellerId, 'skuId' => $skuId, 'supplyType' => 'ON_HAND');
    $request = array('inventoryEntry' => $inventoryEntry);
    $client->sendMessage('atpReindexSkuQueue', $request, 'reindexInventory', 'xml', true, 'inv');
  }

}
?>

