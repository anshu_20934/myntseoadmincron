<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: partner_plans.php,v 1.17 2006/01/11 06:55:58 mclap Exp $
#
define("NUMBER_VARS", "basic_commission,min_paid");
require "./auth.php";
require $xcart_dir."/include/security.php";
 
if(!$active_modules['XAffiliate'])
    func_header_location ("error_message.php?access_denied&id=10");

$location[] = array(func_get_langvar_by_name("lbl_affiliate_plans"), "");

#
# Define data for the navigation within section
# 
$dialog_tools_data["right"][] = array("link" => "partner_commissions.php", "title" => func_get_langvar_by_name("lbl_commissions"));

if ($REQUEST_METHOD == "POST") {

#
# Update affiliate plans list (title, status)
#
	$plans = $HTTP_POST_VARS["plans"];

	if ($mode == "delete") {
		db_query("DELETE FROM $sql_tbl[partner_plans] WHERE plan_id='$pid'");
		db_query("UPDATE $sql_tbl[partner_commissions] SET plan_id='0' WHERE plan_id='$pid'");
		if ($config["default_affiliate_plan"] == $pid)
			db_query("UPDATE $sql_tbl[config] SET value='0' WHERE name='default_affiliate_plan' AND category=''");
		func_header_location("partner_plans.php");
	}
	
	if ($mode == "default_plan") {
		if (func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[config] WHERE name='default_affiliate_plan' AND category=''") == 0)
			db_query("INSERT INTO $sql_tbl[config] (name, value, category, defvalue) VALUES ('default_affiliate_plan', '$plan_id', '', '')");
		else
			db_query("UPDATE $sql_tbl[config] SET value='$plan_id' WHERE name='default_affiliate_plan' AND category=''");
		func_header_location("partner_plans.php");
	}
	
	if ($mode == "update") {
		if ($new_plan_title) {
			db_query ("INSERT INTO $sql_tbl[partner_plans] (plan_title, status) VALUES ('$new_plan_title', '$new_status')");

			if ($redirect_to_modify == 'on') {
				$pid = db_insert_id();
				func_header_location("partner_plans.php?mode=edit&planid=$pid");			
			}
		}

		func_header_location("partner_plans.php");
	}
	if($mode == 'edit') {
        if ($plans) {
            foreach ($plans as $pid=>$plan) {
                db_query ("UPDATE $sql_tbl[partner_plans] SET plan_title='$plan[plan_title]', status='$plan[status]'  WHERE plan_id='$pid'");
            }
        }
		func_header_location("partner_plans.php");		
	}

#
# Responce on click "Modify" button in the affiliate plans list
#
	if ($mode == "modify" && !$planid) {
	
		func_header_location("partner_plans.php?mode=edit&planid=$pid");
	}

#
# Edit/Create affiliate plan commission rates
#
	if ($mode == "modify" || $mode == "create" || $mode == "delete_rate") {

#
# PRODUCTSS COMMISSION RATES PROCESSING
#

		if ($form == "products") {
#
# Delete commission rate
#
			if ($mode == "delete_rate" && is_array($productid)) {
				foreach($productid as $prodid)
					db_query("DELETE FROM $sql_tbl[partner_plans_commissions] WHERE plan_id='$planid' AND item_id='$prodid' AND item_type='P'");
				func_header_location("partner_plans.php?mode=edit&planid=$planid");
			}
#
# Update commission rates on products
#
			if (is_array($products)) {
			#
			# Update committions on existing products
			#
				foreach($products as $k=>$v) {
					db_query("UPDATE $sql_tbl[partner_plans_commissions] SET commission='".addslashes(func_convert_number($v["commission"]))."', commission_type='$v[commission_type]' WHERE plan_id='$planid' AND item_id='$k' AND item_type='P'");
				}
			}
			
			if ($product_ids) {
			#
			# Add new commissions
			#
				$product_ids_array = explode(",", $product_ids);
				if (is_array($product_ids_array)) {
					foreach ($product_ids_array as $item_id) {
						$is_exists = (func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[partner_plans_commissions] WHERE plan_id = '$planid' AND item_id = '$item_id' AND item_type = 'P'") > 0);
						if (!$is_exists)
							db_query("INSERT INTO $sql_tbl[partner_plans_commissions] (plan_id, commission, commission_type, item_id, item_type) VALUES ('$planid', '".addslashes(func_convert_number($new_product_commission))."', '$new_product_commission_type', '$item_id', 'P')");
					}
				}	
			}
			
		}
#
# CATEGORIES COMMISSION RATES PROCESSING
#
		elseif ($form == "categories") {
#
# Delete commission rate
#
			if ($mode == "delete_rate" && is_array($categoryid)) {
				foreach($categoryid as $catid)
					db_query("DELETE FROM $sql_tbl[partner_plans_commissions] WHERE plan_id='$planid' AND item_id='$catid' AND item_type='C'");
				func_header_location("partner_plans.php?mode=edit&planid=$planid");
			}
#
# Update commission rates on categories
#
			if (is_array($categories)) {
			#
			# Update committions on existing categories
			#
				foreach($categories as $k=>$v) {
					db_query("UPDATE $sql_tbl[partner_plans_commissions] SET commission='".addslashes(func_convert_number($v["commission"]))."', commission_type='$v[commission_type]' WHERE plan_id='$planid' AND item_id='$k' AND item_type='C'");
				}
			}
			
			if ($new_categoryid) {
			#
			# Add new commissions
			#
				$is_exists = (func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[partner_plans_commissions] WHERE plan_id = '$planid' AND item_id = '$new_categoryid' AND item_type = 'C'") > 0);
				if (!$is_exists)
					db_query("INSERT INTO $sql_tbl[partner_plans_commissions] (plan_id, commission, commission_type, item_id, item_type) VALUES ('$planid', '".addslashes(func_convert_number($new_product_commission))."', '$new_product_commission_type', '$new_categoryid', 'C')");
			}

		}
#
# CATEGORIES COMMISSION RATES PROCESSING
#
		elseif ($form == "general") {
#
# Update general commission rate
#
			if (func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[partner_plans_commissions] WHERE plan_id='$planid' AND item_id='0' AND item_type='G'") == "0")
				db_query("INSERT INTO $sql_tbl[partner_plans_commissions] (plan_id, commission, commission_type, item_type) VALUES('$planid', '$basic_commission', '$basic_commission_type', 'G')");
			else
				db_query("UPDATE $sql_tbl[partner_plans_commissions] SET commission='$basic_commission', commission_type='$basic_commission_type' WHERE plan_id='$planid' AND item_id='0' AND item_type='G'");
			db_query("UPDATE $sql_tbl[partner_plans] SET min_paid = '$min_paid' WHERE plan_id='$planid'");
		}
		func_header_location("partner_plans.php?mode=edit&planid=$planid#$form");
	}

}

if ($mode == "edit") {

	if ($planid) {
	
		include $xcart_dir."/include/categories.php";

		$partner_plan_info = func_query_first ("SELECT * FROM $sql_tbl[partner_plans] WHERE plan_id='$planid'");
		$partner_plans_commissions = func_query("SELECT * FROM $sql_tbl[partner_plans_commissions] WHERE plan_id='$planid'");

		if (is_array($partner_plans_commissions)) {
			foreach($partner_plans_commissions as $k=>$v) {
				if ($v["item_type"] == "P")
					$partner_plans_commissions[$k]["product"] = func_query_first_cell("SELECT product FROM $sql_tbl[products] WHERE productid='$v[item_id]'");
				if ($v["item_type"] == "C")
					$partner_plans_commissions[$k]["category"] = func_query_first_cell("SELECT category FROM $sql_tbl[categories] WHERE categoryid='$v[item_id]'");
				if ($v["item_type"] == "G")
					$general_commission = $v;

			}
		}
		
		$smarty->assign("partner_plans_commissions", $partner_plans_commissions);
		$smarty->assign("general_commission", $general_commission);
		$smarty->assign("partner_plan_info", $partner_plan_info);
		$smarty->assign("mode", "modify");
	}
	else
		func_header_location("partner_plans.php");

	$location[count($location)-1][1] = "partner_plans.php";
	$location[] = array(func_get_langvar_by_name("lbl_modify_plan"), "");
	$smarty->assign ("main", "partner_plans_edit");

}
else {

	$partner_plans = func_query ("SELECT * FROM $sql_tbl[partner_plans] ORDER BY plan_id");
	if (!empty($partner_plans))
		$smarty->assign ("partner_plans", $partner_plans);
	$smarty->assign ("main", "partner_plans");
}

# Assign the current location line
$smarty->assign("location", $location);

# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
