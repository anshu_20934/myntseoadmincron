<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: categories.php,v 1.31 2006/02/14 14:45:23 max Exp $
#

require "./auth.php";
require $xcart_dir."/include/security.php";
require $xcart_dir."/include/func/func.mail.php";

$location[] = array(func_get_langvar_by_name("lbl_designers_management"), "");


if (empty($mode)) $mode = "";

#
# Ajust category_location array
#
require "./location_ajust.php";

$template_used  = 'affiliatepayment'; 


if ($REQUEST_METHOD == "POST") {

	if ($mode == "search") {
		$producttype = getDesignerInfo($mode,$designerid,$designer_name,$joined_after,$joined_before);
		$template_used = 'admin_designer';
	
	} elseif ($mode == "update") {
		
		#
		# update designer values 
		#
		
        if (is_array($posted_data)) {
			foreach ($posted_data as $desigid=>$v) {
              if(!empty($v['delete'])){
		           $did = $desigid;	  
              }
			}
		}
		if(!empty($did)){
		  	$producttype = getDesignerInfo($mode,$did);

		  	/* show pending commission by default */
		  	$commStatus = '1011';
		  	$comm_details = getDesignerCommDetail($commStatus,$did);
			$smarty->assign("comm_details",$comm_details);
			$smarty->assign("status",$commStatus);
			/* end */
			
			$SQL= "SELECT paymentid,payment_method FROM $sql_tbl[payment_methods]";
			$payment_methods = func_query ($SQL);
			$smarty->assign("designerid",$did);
			$comm_history = getPaymentHistory($did);
		 }else{
		     func_header_location("admin_designer.php");
			  
		 }
         $template_used = 'designer_modify';
	    
	
		} elseif ($mode == "save") {
			
			#
			# save designer payment details
			#
			$template_used = 'designer_modify';
			
			$sql_tbl[mk_designer_payment_commission] =  "mk_designer_payment_commission";
	        if(!empty($payamt))
	        {
	        	$query_data = array(
							"status" => '1006'
 				);
 				$time =time();
				$comments = mysql_escape_string($comments);
				#
				# save designer commision details
				#
 				
		        if (is_array($posted_data)) {
					foreach ($posted_data as $desigid=>$v) {
		              if(!empty($v['delete'])){
				           $did = $desigid;	
				           func_array2update("mk_designer_commission", $query_data, "id='$did'");  
				           
		              }
					}
				}
				$SQL = "INSERT INTO $sql_tbl[mk_designer_payment_commission](recipient_id,payment_amt,pay_method,comments,payment_status,payment_date,recipient_type) 
						VALUES($designerid,$payamt,$pay_methods,'".$comments."','1006','".$time."','Designer') " ;
				db_query($SQL);
	        	
				
				
		        $producttype = getDesignerInfo($mode,$designerid);
		        $comm_history = getPaymentHistory($designerid);
		       	$smarty->assign("designerid",$designerid);
		       	
		       	$designerFirstname =  $producttype[0]['firstname'];
		       	$designerEmail =  $producttype[0]['email'];
		       	
		       	$template = "monthlycommissionpayment";
				$args = array( "FIRST_NAME" =>$designerFirstname,
				               "PAYMENT_AMOUNT" => $payamt,
				               "COMMENTS" => $comments
				              );
			 	sendMessage($template, $args,$designerEmail);
				
				$top_message["content"] = "Commission Payment has been made successfully.";
	       }
	      
		} 
		elseif($mode == "show" || $mode="payment")
		{
			$chkArray = array();
			$totalComm = '0'; 
			
			if($mode == 'payment')
			{
			  if (is_array($posted_data)) {
				foreach ($posted_data as $id=>$v) 
				{
              		if(!empty($v['delete']))
              		{
		           		$chkArray[] = $id;
		           		$totalComm = floatval($totalComm) + floatval($v['comm']) ;	  
              		}
              		else 
              		{
              			$chkArray[] ="";
              		}
			   }
			  }
			  $smarty->assign("chkArr",$chkArray);
			  
			}
			$comm_details = getDesignerCommDetail($_POST['commStatus'],$designerid);
			$smarty->assign("comm_details",$comm_details);
			$smarty->assign("status",$_POST['commStatus']);
			$smarty->assign("totalcomm",$totalComm);
			$template_used = 'designer_modify';
			$producttype = getDesignerInfo($mode,$designerid);
			$SQL= "SELECT paymentid,payment_method FROM $sql_tbl[payment_methods]";
			$payment_methods = func_query ($SQL);
			$smarty->assign("designerid",$designerid);
			$comm_history = getPaymentHistory($designerid);
			
			
		}
		
}
if(isset($_GET['designer'])) 
{
	$mode1 ="update";
	$designerid = getDesignerID($_GET['designer']);
	$id = $designerid[0][id];
	$producttype = getDesignerInfo($mode1,$id);
}
$smarty->assign("mode",$mode);

$smarty->assign("products",$producttype);
$smarty->assign("pay_methods",$payment_methods);
$smarty->assign("name",$designer_name);
$smarty->assign("j_after",$joined_after);
$smarty->assign("j_before",$joined_before);
$smarty->assign("comm_history",$comm_history);
$smarty->assign("main",$template_used);

# Assign the current location line
$smarty->assign("location", $location);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
