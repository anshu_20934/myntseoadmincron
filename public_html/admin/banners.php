<?php
require "./auth.php";
require_once("../include/func/func.utilities.php");

$messages = array();

$action = $_GET['action'];
$name = $_POST['name'];
$display_order = $_POST['display_order'];
$type = $_POST['type'];
$url = $_POST['url'];
$enabled = $_POST['enabled'];
$alt = $_POST['alt'];
$title = $_POST['title'];
$page = $_POST['page'];

$table = 'mk_images';

$tmp_file = $HTTP_POST_FILES['image']['tmp_name'];

if ($action == 'edit') {
    $id = $_GET['id'];
    if (!empty($name)) {
        func_array2update($table, array('name' => $name, 'url' => $url, 'display_order' => $display_order, 'type' => $type, 'page' => $page, 'enabled' => $enabled, 'url' => $url, 'alt' => $alt, 'title' => $title), 'id="' . $id . '"');
    }
    $banner = func_select_first($table, array('id' => $id));

    if ($tmp_file) {
        // Delete old image ..
        @unlink($banner['image']);

        $start = stripos($banner['image'],"-");
        $name = substr($banner['image'], $start, strlen($banner['image']) - $start);
        $file_location = 'images/banners/' . time() .$name ; 

        // Move to S3 .. hardcoding myntratestimages need to fix..
        moveToS3($xcart_dir.'/'.$file_location , 'myntratestimages',  $file_location);

        move_uploaded_file($tmp_file, $xcart_dir . "/". $file_location);
        func_array2update($table, array('image' => $file_location), 'id="' . $id . '"');
    }

    $smarty->assign("banner", $banner);
    $smarty->assign("id", $id);
} else if ($action == 'add') {

    $file_name = $HTTP_POST_FILES['image']['name'];

    if ($tmp_file && !empty($name)) {
        $id = func_array2insert($table, array('name' => $name, 'url' => $url, 'display_order' => $display_order, 'type' => $type, 'page' => $page, 'enabled' => $enabled, 'url' => $url, 'alt' => $alt, 'title' => $title));
    }

    if ($id) {
        $file_location = 'images/banners/' . time() . '-' . $file_name;
        move_uploaded_file($tmp_file, $xcart_dir ."/". $file_location);

        // Move to S3 .. hardcoding myntratestimages need to fix..
        moveToS3($xcart_dir.'/'.$file_location , 'myntratestimages',  $file_location);

        func_array2update($table, array('image' => $file_location), 'id="' . $id . '"');
    }   
}

if($file_location){
   moveToS3($xcart_dir .'/'. $file_location, "myntratestimages" ,$file_location);  
}


if (empty($action)) {
    $action = 'add';
}
$smarty->assign("action", $action);
$smarty->assign("dropdown", array('front_banner' => 'Front banner', 'right_banner' => 'Right banner', 'right_bottom_banner' => 'Right bottom banner', 'sports_jerseys' => 'Sports Jerseys', 'sportswear' => 'Sportswear'));

$banners = func_select_query($table, null, null, null, array('datecreated' => 'desc'));

$smarty->assign("messages", $messages);
$smarty->assign("banners", $banners);
@include $xcart_dir . "/modules/gold_display.php";
func_display("admin/banners.tpl", $smarty);
?>
