<?php
require "./auth.php";
require $xcart_dir."/include/security.php";

include_once("$xcart_dir/include/func/func.mk_orderbook.php");
include_once("$xcart_dir/include/func/func.order.php");

$startDate = $_POST['startDate'];
$endDate = $_POST['endDate'];
$mode = $_POST['mode'];
if(!$mode)
	$mode = 'view';

if($mode == 'queuecodorders' || $mode=='cancelcodorders') {
	$time = time();
	
	if($mode=='queuecodorders' && !empty($_POST['orderids'])) {
		$orderids = $_POST['orderids'];
		
		$updateCODOrderQuery = "update $sql_tbl[orders] set status='WP', queueddate='$time' where orderid in (" . implode(',', $orderids) . ")";
		db_query($updateCODOrderQuery);
		$order_detail_query = "UPDATE $sql_tbl[order_details] SET item_status = 'A', assignee=1, assignment_time = ".time()." where orderid in (".implode(",", $orderids).")";
		db_query($order_detail_query);		
		
		foreach ($orderids as $key=>$orderid) {
			func_addComment_order($orderid, $login, 'Manual', 'Order Queued', ' From Cod Bulk OH Change Interface.');
			if($inventory_module && $inventory_module == 'wms') {
				func_addComment_status($orderid, "WP", "Q", "WMS Flow. Moving directly to WP from Q", $login);
			}		
		}
	} else if($mode=='cancelcodorders' && !empty($_POST['cancel_orderids'])) {
		$cancel_orderids = $_POST['cancel_orderids'];
		$weblog->info("Cancel Order Ids - ". print_r($cancel_orderids, true));
		foreach($cancel_orderids as $oid) {
			$oid2cancelfullorder[$ooid] = true;
		}
		$cancel_args = array(
			'cancellation_type' 			=> 'CCC',
			'reason'						=> "ADDPINMM",
			'orderid2fullcancelordermap'	=> $oid2cancelfullorder,
			'generateGoodWillCoupons'		=> true
		);
		func_change_order_status($cancel_orderids, 'F', $login, "From Cod Bulk OH Change Interface.", "", true, '', $cancel_args);
	}
}

$parametersList = "orderid, login, total, from_unixtime(date) as placeddate, firstname, s_address, s_city, s_state, s_country, s_zipcode, 
		  		   mobile, cash_coupon_code, cash_redeemed, coupon, coupon_discount, subtotal as actualamount, 
		  		   cash_redeemed+coupon_discount as totalcoupondiscount, discount as eossdiscount";


$queryCondition = " from xcart_orders where status='OH' and queueddate is null and payment_method='cod'";

$limit=$_GET['limit'];
if(empty($limit)) {
	$limit=10;
}

$page=$_GET['page'];

if(empty($page)) {
	$page=1;
} else {
	$startDate = $_GET['startDate'];
	$startDate = substr($startDate, 1, strlen($startDate)-2);
	$endDate = $_GET['endDate'];
	$endDate = substr($endDate, 1, strlen($endDate)-2);
}

if(!empty($startDate)) {
	$queryCondition.= " and date>=unix_timestamp('$startDate') ";
	$smarty->assign("startDate", $startDate);
}

if(!empty($endDate)) {
	$queryCondition.= " and date<=unix_timestamp('$endDate') ";
	$smarty->assign("endDate", $endDate);
}

$countResults = func_query_first_cell("select count(*) $parametersList $queryCondition");

$query = "select $parametersList $queryCondition ";
$query.= " order by orderid asc ";

$totalPages = ceil($countResults/$limit);

$smarty->assign("pagenum", $page);
$smarty->assign("totalPages", $totalPages);

$query.= " limit " . ($page-1)*$limit . "," . $limit;

$results = func_query($query);
$sqllog->info("Execute Query - $query");

$allOrderids = func_query("select orderid $queryCondition");
$orderList=array();
$pagenumorders = 1;
$index = 1;
foreach ($allOrderids as $singleresult) {
	$orderList[$pagenumorders][] = $singleresult['orderid'];
	if($index%$limit==0) {
		$index=1;
		$pagenumorders++;
	} else {
		$index++;
	}
}

foreach($orderList as $key=>$value) {
	$orderList[$key] = implode(", ", $value);
}

foreach($results as $key=>$row){
	$first2digits = intval($row["s_zipcode"]/10000);
	if($first2digits >=90 & $first2digits < 100){
		$results[$key]["pin_comments"] = "Please check if address corresponds to \"APS(Army Postal Services)\"";
	}else{
		$validationquery = "select count(1) as cnt from valid_pin_combinations where stateCode = '".$row["s_state"]."' and first_2_digits = ".$first2digits;
		$res = func_query($validationquery);
		if($res[0]["cnt"] == 1){
			$results[$key]["pin_comments"] = "";
		}else{
			$results[$key]["pin_comments"] = "Address - Pincode mismatch";
		}	
	}
}
$smarty->assign("orderList", $orderList);

if(!empty($results)) {
	$smarty->assign("codonholdorders", $results);
}

$template ="cod_bulk_change_on_hold_orders";
$smarty->assign("main",$template);
func_display("admin/home.tpl",$smarty);

?>
