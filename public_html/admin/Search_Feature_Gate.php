<?php
require_once "./auth.php";
require_once($xcart_dir.'/include/search/class.SearchURLGenerator.php');

$message;
$searchresult;
$otherResult;
$sql = "select * from search_feature_gate ";
$completeResult = func_query($sql,true);
foreach ($completeResult as $key => $row)
{
	if($row[has_position] == 1)
	{
		$searchresult[] = $completeResult[$key];
	}
	else 
	{
		$otherResult[] = $completeResult[$key];
	}
}
function CheckInteger($val)
{
	//echo"input $val";
	return preg_match('@^(?:[0-9][0-9]*|0)$@', $val);
}
if($REQUEST_METHOD == "POST"){
          ############## Condition to add groups ############
    try 
	{
		if($HTTP_POST_VARS['mode'] == "Update")
		{
			$looktype = "Add";
			$offset = $HTTP_POST_VARS['offset'];
			$searchLanding = $HTTP_POST_VARS['searchLanding'];
			$input = $HTTP_POST_VARS[output];
			$inputWithoutPos = $HTTP_POST_VARS[out];
			$positionArray;
			$sumOfValue=0;
			$allPositionString ="";
			//Checking entries are right
			foreach ($input as $row)
			{
				//print_r($row);
				//Checking if all field values are integer
				if(CheckInteger($row[field_value]))
				{
					//echo" p1 ";
					$sumOfValue += $row[field_value];
					if($sumOfValue>24)
					{
						$message = "Summation of value is Greater than 24 limit is 0-24";
						throw new Exception($message);
					}
				}
				else
				{
					//echo" p2 ";
					$message = "Value entered for field $row[field_name] i.e. $row[field_value] is not a natural Number --<a href=\"http:\/\/en.wikipedia.org/wiki/natural_number\" 
					target=\"\_blank\">Wiki Link natural Numbers</a>";
					throw new Exception($message);
					
				}
				$localPositionArray = explode(',', $row[position]);
				//echo" <pre>array ";print_r($localPositionArray);
				// Number of values in array is same as field_value or not
				$sizeOfLocalPositionArray = sizeof($localPositionArray)-(( (($localPositionArray[sizeof($localPositionArray)-1] == "")))?1:0);
				//echo" <pre>size is $sizeOfLocalPositionArray ";
				if($sizeOfLocalPositionArray!=$row[field_value] )
				{
					$message = "Number of Positions entered for field $row[field_name] is not equal to  $row[field_value] i.e. value in field value
					It can happen due to obvious reason num of positions not same as $row[field_value] or they are not seperated by proper delimter \",\"";
					throw new Exception($message);
				}
				//check if all of them is integer
				for($i=0;!empty($localPositionArray[$i]);$i++)
				{
					if(!(CheckInteger($localPositionArray[$i])))
					{
						$message = "One of the positions entered for field $row[field_name] i.e. $localPositionArray[$i] is not a natural Number --<a href=\"http:\/\/en.wikipedia.org/wiki/natural_number\" 
						target=\"\_blank\">Wiki Link natural Numbers</a> It can also occur due to not using proper delimiter";
						throw new Exception($message);
					}
					else 
					{
						if($localPositionArray[$i]>23)
						{
							$message = "One of the positions entered for field $row[field_name] i.e. $localPositionArray[$i] is Greater than 23";
							throw new Exception($message);
						}	
					}
				}
				if(!empty($allPositionString)&&!empty($row[position]))
				{
					$allPositionString =$allPositionString.",";
				}
				$allPositionString=$allPositionString.$row[position];
			}
			$allPositionArray = explode(',', $allPositionString);
			//echo"all pos array is ";print_r($allPositionString);
			//check that all value are unique
			$reverseIndexArray = array();
			//echo"<Pre>";print_r($allPositionArray);
			for($i=0;!empty($allPositionArray[$i]);$i++)
			{
				
				if(empty($reverseIndexArray[$allPositionArray[$i]])  && $reverseIndexArray[$allPositionArray[$i]] !="0")
				{
					$reverseIndexArray[$allPositionArray[$i]] =$i;
				}
				else
				{
					$message = "position $allPositionArray[$i] has been repeated more than once";
					throw new Exception($message);
				}
			}//Check All Enteries in non position field is right
			foreach ($inputWithoutPos as $row)
			{
				//print_r($row);
				//Checking if all field values are integer
				if(CheckInteger($row[field_value]))
				{
				}
				else 
				{
					$message = "Value entered for field $row[field_name] i.e. $row[field_value] is not a natural Number --<a href=\"http:\/\/en.wikipedia.org/wiki/natural_number\" 
					target=\"\_blank\">Wiki Link natural Numbers</a>";
					throw new Exception($message);
				}
			}
			foreach ($input as $key => $row)
			{
				$query ="Update search_feature_gate
						set field_value = '$row[field_value]',
						position = '$row[position]'
						where id = $key";
				func_query($query);
			}
			foreach ($inputWithoutPos as $key => $row)
			{
				$query ="Update search_feature_gate
						set field_value = '$row[field_value]'
						where id = $key";
				func_query($query);
			}
			$searchresult =array();
			$otherResult = array();
			$sql = "select * from search_feature_gate ";
			$completeResult = func_query($sql,true);
			foreach ($completeResult as $key => $row)
			{
				if($row[has_position] == 1)
				{
					$searchresult[] = $completeResult[$key];
				}
				else 
				{
					$otherResult[] = $completeResult[$key];
				}
			}
			$message ="<h1 style=\"color:#0000FF\"> All Fine ... Passed the scrutiny  Test";
		}
	}
	catch(exception $e)
	{	
		//Filling The Wrong Data's As entered By User
		$input = $HTTP_POST_VARS[output];
		for($i=0;!empty($searchresult[$i]);$i++)
		{
			$searchresult[$i][field_value] =$input[$searchresult[$i][id]][field_value];
			$searchresult[$i][position] =$input[$searchresult[$i][id]][position];
		}
		for($i=0;!empty($otherResult[$i]);$i++)
		{
			$otherResult[$i][field_value] =$inputWithoutPos[$otherResult[$i][id]][field_value];
		}
		$message = "<h1 style=\"color:#FF0000\"> Wrong Data Entry -----".$message." </h1>Note This Gives only the 1st wrong data filling. There might be more mistakes.";
	}
 }
 

$smarty->assign("searchresult",$searchresult);
$smarty->assign("otherresult",$otherResult);
$smarty->assign("message",$message);
$smarty->assign("main","search_gate");
$smarty->assign("message",$message);
func_display("admin/home.tpl",$smarty);
?>