<?php

include_once("auth.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/PHPExcel/PHPExcel/Style/NumberFormat.php");

use oms\OrderDetailRequestProcessor;

$action = $_POST['action'];
if(empty($action))
	$action = 'upload';

$errorMsg = "";
if ($action=='verify') {
    if(!isset($_FILES) && isset($HTTP_POST_FILES))
        $_FILES = $HTTP_POST_FILES;
		
    if(!empty($_FILES['orderfile']['name'])){
    	$uploaddir = '../bulkorderupdates/';
        $extension = explode(".",basename($_FILES['orderfile']['name']));
        $uploadfile = $uploaddir . date('d-m-Y-h-i-s').".".$extension[1];
        if (move_uploaded_file($_FILES['orderfile']['tmp_name'], $uploadfile)) {
        	$uploadStatus = true;
        }
    }
    if(!$uploadStatus) {
    	$errorMsg .=  "<br>File not uploaded, try again.. <br>";
    } else {
    	$objReader = PHPExcel_IOFactory::createReader('Excel5');
		$objPHPExcel = $objReader->load($uploadfile);
 		$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
 		
 		$orderids = array(); $orders = array();
 		for($i=2; $i<=$lastRow; $i++){//start from second row ignore heading
			$order = array();

 			$cellObj = ($objPHPExcel->getActiveSheet()->getCell('A'.$i));	        
 			$orderid = trim($cellObj->getvalue());
	        if(empty($orderid)) {
	        	$errorMsg .= "No Order Id provided in row - $i<br>";
	        	continue;	
	        }
			$order['orderId'] = $orderid;
	        
	        if(in_array($orderid, $orderids))
	        	continue;
	        	
	        $orderids[] = $orderid;

	        $cellObj = ($objPHPExcel->getActiveSheet()->getCell('B'.$i));
	        $userId = trim($cellObj->getvalue());
	        if(empty($userId)) {
	        	$errorMsg .= "No User Id provided in row - $i<br>";
	        }
			$order['userId'] = $userId;
	        
	        $cellObj = ($objPHPExcel->getActiveSheet()->getCell('C'.$i));
	        $attemptNo = trim($cellObj->getvalue());
	        if(empty($attemptNo))
	        	$errorMsg .= "No Attempt Number provided in row - $i<br>";
	        if(!is_numeric($attemptNo))
	        	$errorMsg .= "Attempt Number should be integer in row - $i<br>";
		
			$order['attemptNo'] = $attemptNo;
	        
	        $cellObj = ($objPHPExcel->getActiveSheet()->getCell('D'.$i));
	        $attemptTime = trim($cellObj->getvalue());
	        if(empty($attemptTime))
	        	$errorMsg .= "No Attempt Time provided in row - $i<br>";
			$order['attemptTime'] = PHPExcel_Style_NumberFormat::toFormattedString($attemptTime, "M/D/YYYY H:MM:SS");
	        
	        $cellObj = ($objPHPExcel->getActiveSheet()->getCell('E'.$i));
	        $resolution = trim($cellObj->getvalue());
	        if(empty($resolution))
	        	$errorMsg .= "No Resolution provided in row - $i<br>";
	        $order['resolution'] = $resolution;

	        $cellObj = ($objPHPExcel->getActiveSheet()->getCell('F'.$i));
	        $disposition = trim($cellObj->getvalue());
	        if(empty($disposition))
	        	$errorMsg .= "No Disposition provided in row - $i<br>";
			$order['disposition'] = $disposition;
	        	
	        $sql = "select * from mk_order_action_reasons where action ='oh_disposition' and reason = '$disposition' and parent_reason = '$resolution'";
			$row = func_query_first($sql, true);
	        if(!$row)
				$errorMsg .= "Resolution - $resolution and Disposition - $disposition is not valid combination in row - $i<br>";	
	        
			$cellObj = ($objPHPExcel->getActiveSheet()->getCell('G'.$i));
			$action = trim($cellObj->getvalue());
			if(empty($action))
				$errorMsg .= "No Action provided in row - $i<br>";

			if(!in_array(strtolower($action), array('reattempt', 'modify', 'queue', 'cancel'))) {	
				$errorMsg .= "Invalid action provided in row - $i<br>";
			}
			$order['action'] = $action;
			
			if(strtolower($action) == 'modify') {
				if($disposition == 'AV' || $disposition == 'NAV' || $disposition == 'NAPV') {
					
					$cellObj = ($objPHPExcel->getActiveSheet()->getCell('I'.$i));
					$street = trim($cellObj->getvalue());
					if(empty($street)) {
						$errorMsg .= "Street is not provided for row - $i<br>";
					}
					
					$cellObj = ($objPHPExcel->getActiveSheet()->getCell('J'.$i));
					$locality = trim($cellObj->getvalue());
					if(empty($locality)) {
						$errorMsg .= "Locality is not provided for row - $i<br>";
					}
					
					$cellObj = ($objPHPExcel->getActiveSheet()->getCell('K'.$i));
					$city = trim($cellObj->getvalue());
					if(empty($city)) {
						$errorMsg .= "City is not provided for row - $i<br>";
					}
					
					$cellObj = ($objPHPExcel->getActiveSheet()->getCell('L'.$i));
					$state = trim($cellObj->getvalue());
					if(empty($state)) {
						$errorMsg .= "State is not provided for row - $i<br>";
					}
					
					$cellObj = ($objPHPExcel->getActiveSheet()->getCell('N'.$i));
					$zipcode = trim($cellObj->getvalue());
					if(empty($zipcode)) {
						$errorMsg .= "Zipcode is not provided for row - $i<br>";
					}
				}
					
				if($disposition == 'NPV' || $disposition == 'NAPV') {
					$cellObj = ($objPHPExcel->getActiveSheet()->getCell('O'.$i));
					$phone = trim($cellObj->getvalue());
					if(empty($phone)) {
						$errorMsg .= "Phone No is not provided for row - $i<br>";
					}
				}					
			}
			
			$cellObj = ($objPHPExcel->getActiveSheet()->getCell('P'.$i));
			$remarks = trim($cellObj->getvalue());
			if(empty($remarks))
				$errorMsg .= "No remarks provided in row - $i<br>";
			$order['remarks'] = $remarks;

			$orders[] = $order;
 		}
 		
 		if(count($orderids)>0) {
			$oh_orderids = func_query_column("Select orderid from xcart_orders where orderid in (".implode(",", $orderids).") and status = 'OH'", 0, true);
			$not_oh_orderids = array_diff($orderids, $oh_orderids);
			if(!empty($not_oh_orderids)) {
				$errorMsg .= "Following orders are either not in On-Hold state or are not valid - ".implode(", ", $not_oh_orderids)."<br>";
			}
 		}
    }
 		if(!empty($errorMsg)) {
 			$smarty->assign("errorMsg", $errorMsg);
 			$action = "upload";
 		} else {
 			$uniqueId = uniqid();
        		$smarty->assign('progresskey', $uniqueId);
 			$smarty->assign("uploadorderFile", $uploadfile);
 			$smarty->assign("orderList", $orders);
 			$action = 'confirm';
 		}
} else if($action == 'confirm') {
	
	$filename = $_POST['filetoread'];
	$objReader = PHPExcel_IOFactory::createReader('Excel5');
	
	$processor= new OrderDetailRequestProcessor();
	
	$objPHPExcel = $objReader->load($filename);
	$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
	
	$orders = array(); $orderids = array();
	$progressKey = $HTTP_POST_VARS['progresskey'];
	
	$xcache = new XCache();
	$progressinfo = array('total'=>$lastRow-1, 'completed'=>0);
	$xcache->store($progressKey."_progressinfo", $progressinfo, 86400);
	
	for($i=2; $i<=$lastRow; $i++){//start from second row ignore heading
   	    
		$cellObj = ($objPHPExcel->getActiveSheet()->getCell('A'.$i));
        $orderId = trim($cellObj->getvalue());
        
        $cellObj = ($objPHPExcel->getActiveSheet()->getCell('B'.$i));
        $userId = trim($cellObj->getvalue());
        
        $cellObj = ($objPHPExcel->getActiveSheet()->getCell('C'.$i));
        $attemptNo = trim($cellObj->getvalue());
        
        $cellObj = ($objPHPExcel->getActiveSheet()->getCell('D'.$i));
        $attemptTime = trim($cellObj->getvalue());
		$attemptTime = PHPExcel_Style_NumberFormat::toFormattedString($attemptTime, "YYYY-MM-DD HH:MM:SS");
	    
        $cellObj = ($objPHPExcel->getActiveSheet()->getCell('E'.$i));
        $resolution = trim($cellObj->getvalue());
		
        $cellObj = ($objPHPExcel->getActiveSheet()->getCell('F'.$i));
        $disposition = trim($cellObj->getvalue());
        
        $cellObj = ($objPHPExcel->getActiveSheet()->getCell('G'.$i));
        $action = strtolower(trim($cellObj->getvalue()));
        
        $cellObj = ($objPHPExcel->getActiveSheet()->getCell('P'.$i));
        $remarks = mysql_real_escape_string(trim($cellObj->getvalue()));
        
        $address = false;
        if(strtolower($action) == 'modify') {
        	if($disposition == 'NAV' || $disposition == 'NAPV') {
        		$address = array();
        		$cellObj = ($objPHPExcel->getActiveSheet()->getCell('I'.$i));
        		$address['street'] = trim($cellObj->getvalue());
        		
        		$cellObj = ($objPHPExcel->getActiveSheet()->getCell('J'.$i));
        		$address['locality'] = trim($cellObj->getvalue());

        		$cellObj = ($objPHPExcel->getActiveSheet()->getCell('K'.$i));
        		$address['city'] = trim($cellObj->getvalue());
        			
        		$cellObj = ($objPHPExcel->getActiveSheet()->getCell('L'.$i));
        		$address['state'] = trim($cellObj->getvalue());
        			
        		$cellObj = ($objPHPExcel->getActiveSheet()->getCell('N'.$i));
        		$address['zipcode'] = trim($cellObj->getvalue());
        	}
        		
        	if($disposition == 'NPV' || $disposition == 'NAPV') {
        		$cellObj = ($objPHPExcel->getActiveSheet()->getCell('O'.$i));
        		$phone = trim($cellObj->getvalue());
        	}
        }
        
		$processor->updateOHResponse($orderId, $resolution, $disposition, $attemptNo, $attemptTime, $userId, $remarks, $action, $address, $phone);
			
        $progressInfo = $xcache->fetch($progressKey."_progressinfo");
    	$progressInfo['completed'] = $i-1;
    	$xcache->store($progressKey."_progressinfo", $progressInfo, 86400);
	}
	
	$xcache->remove($progressKey."_progressinfo");
	$smarty->assign("msg", "Updated Orders Successfully");
 	$action = "upload";
}

$smarty->assign("action", $action);
$smarty->assign("main", "cod_oh_bulk_update");
func_display("admin/home.tpl", $smarty);

?>
