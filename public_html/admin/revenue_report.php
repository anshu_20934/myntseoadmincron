<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
require $xcart_dir."/include/class/class.report.php";

/*
##Navigation url
*/
if($HTTP_GET_VARS){	
	$query_string = http_build_query($HTTP_GET_VARS);
	$URL = "revenue_report.php?{$query_string}";
}else 
	$URL = 'revenue_report.php?';

/*
##create filters for report and revenue generation term
*/
$report_filter = array(
	'ORDER_BY'		=> $order_by,
	'SORT_ORDER'	=> $sort_order,
	'GROUP_BY'		=> $group_by,
	'TERM'			=> !empty($term)?$term:'daily'
);

$sql = report::func_create_revenue_query($report_filter);
$sqllog->debug("File name## revenue report.php##SQL Info::>$sql");
$report_result = func_query($sql);


$smarty->assign('navigation_script',$URL);
$smarty->assign('report_result',$report_result);

$smarty->assign('main', 'revenue_report');
func_display("admin/home.tpl", $smarty);
?>
