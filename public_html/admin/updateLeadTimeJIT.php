<?php

include_once("auth.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/modules/amqpclients/producer/AMQPMessageProducer.php");
include_once("updateInventoryForLeadTime.php");

$client = AMQPMessageProducer::getInstance();

echo "[" . date('j-m-Y H:i:s') . "(";

$imsConn = mysql_connect('imsdb3.myntra.com:3306', 'MyntImsUser', '5V9D9i4Yf3j991Q#', TRUE);


$to = "bharani.cv@myntra.com,aduri.sabarinath@myntra.com,rajeev.verma1@myntra.com,arunkumar.g@myntra.com,vijay.sankaran@myntra.com,gyan.prabhat@myntra.com";
$fromHeader = "From: inventorysync@myntra.com";
$subject = "[WMSALERT]ERP Inventory: JIT IMS and ATP Lead time Update";

if (!$imsConn ) {
    die('Could not connect: ' . mysql_error());
}

if (mysql_select_db('myntra_ims', $imsConn) === false) {
    echo "WMS db unaccessible \n";
    exit;
}


$myntraSellerIds = "1,19,21,25,29";
$jitWhIds="20,26,27,44,47,50,53,59,62,91,94,96,97,98,99,100,102,109";

$authHeader = "Authorization: Basic YXBpYWRtaW46bTFudHJhUjBja2V0MTMhIw==";
$appPropBaseUrl = "http://erptools.myntra.com/myntra-tools-service/platform/tools/properties/search/?q=name.eq:";
$appPropSellerUrl = $appPropBaseUrl."wms.myntra.inventory.myntraSeller.ids";
$output = shell_exec("curl -v -H '" . $authHeader . "' -H 'Content-Type:application/xml' -XGET '" . $appPropSellerUrl . "' -d '" . $data . "'");

$xml = simplexml_load_string($output);

if ($xml->status->statusType == "SUCCESS")
    $myntraSellerIds = $xml->data->applicationProperty->value;

$appPropWhUrl = $appPropBaseUrl."jit.warehouse.id";
$output = shell_exec("curl -v -H '" . $authHeader . "' -H 'Content-Type:application/xml' -XGET '" . $appPropWhUrl . "' -d '" . $data . "'");

$xml = simplexml_load_string($output);

if ($xml->status->statusType == "SUCCESS")
    $jitWhIds = $xml->data->applicationProperty->value;


$offset = 0;
$limit = 10000;
$hasMore = true;
$vendorIds = array();
foreach(file('updateLeadTime.csv') as $line) {
    $params=str_getcsv($line);
    if(sizeof($params) == 3){
        $vendorId=$params[0];
        $skuId=$params[1];
        $leadTime=$params[2];

        $updateLeadTimeQueryIms = "update wh_inventory set proc_sla = ".$leadTime." where sku_id = ".$skuId." and vendor_id = ".$vendorId." and store_id = 1 and supply_type = 'JUST_IN_TIME' and seller_id in (".$myntraSellerIds.") and warehouse_id in (".$jitWhIds.")";
        $updateLeadTimeQueryAtp = "update inventory set lead_time = ".$leadTime." where sku_id = ".$skuId." and vendor_id = ".$vendorId." and store_id = 1 and supply_type = 'JUST_IN_TIME' and seller_id in (".$myntraSellerIds.")";
        $updateLeadTimeQueriesIms[] = $updateLeadTimeQueryIms;
        $updateLeadTimeQueriesAtp[] = $updateLeadTimeQueryAtp;
    }

    else if(sizeof($params) == 2) {
        $vendorId=$params[0];
        $leadTime=$params[1];
        $vendorIds[] = $vendorId;
        $vendorIdLeadTime[$vendorId] = $leadTime;
    }
}

if(!empty($vendorIds)){
    $vendorIdsCsv = implode(',', $vendorIds);
    $minIdIms = 1;

    $getSkusFromVendorQuery = "select min(id) as minId, max(id) as maxId from wh_inventory where supply_type = 'JUST_IN_TIME' and store_id = 1 and seller_id in (".$myntraSellerIds.") and warehouse_id in (".$jitWhIds.") and vendor_id in  (".$vendorIdsCsv.")";

    $minIdResult = mysql_query($getSkusFromVendorQuery, $imsConn);
    if ($minIdResult) {
        while ($row = mysql_fetch_array($minIdResult, MYSQL_ASSOC)) {
            $minIdIms = $row['minId'];
            $maxIdIms = $row['maxId'];
        }
    }
    @mysql_free_result($minIdResult);

    while ($hasMore) {
//        $hasMore = false;
        $minIdImsNext = $minIdIms + $limit;
        $getSkusFromVendorQuery = "select id, sku_id as skuId, vendor_id as vendorId from wh_inventory where id >= ".$minIdIms." and id < ".$minIdImsNext." and supply_type = 'JUST_IN_TIME' and store_id = 1 and seller_id in (".$myntraSellerIds.") and warehouse_id in (".$jitWhIds.") and vendor_id in  (".$vendorIdsCsv.") order by id";

        $sku_result = mysql_query($getSkusFromVendorQuery, $imsConn);
        if ($sku_result) {
            while ($row = mysql_fetch_array($sku_result, MYSQL_ASSOC)) {
                $vendorId=$row['vendorId'];
                $skuId = $row['skuId'];
                $vendorSku = $vendorId."_".$skuId;
                $vendorSkuLeadTime[$vendorSku] = $vendorIdLeadTime[$vendorId];
            }
        }
        If($minIdImsNext>=$maxIdIms) {
            $hasMore = false;
        }
        @mysql_free_result($sku_result);
        $minIdIms = $minIdImsNext;
    }
}

foreach($vendorSkuLeadTime as $vendorSku => $leadTime) {
    $startIndx = strpos($vendorSku, "_");
    $vendorId = substr($vendorSku, 0, $startIndx);
    $skuId = substr($vendorSku, $startIndx + 1);
    $updateLeadTimeQueryIms = "update wh_inventory set proc_sla = ".$leadTime." where sku_id = ".$skuId." and vendor_id = ".$vendorId." and store_id = 1 and supply_type = 'JUST_IN_TIME' and seller_id in (".$myntraSellerIds.") and warehouse_id in (".$jitWhIds.")";
    $updateLeadTimeQueryAtp = "update inventory set lead_time = ".$leadTime." where sku_id = ".$skuId." and vendor_id = ".$vendorId." and store_id = 1 and supply_type = 'JUST_IN_TIME' and seller_id in (".$myntraSellerIds.")";
    $updateLeadTimeQueriesIms[] = $updateLeadTimeQueryIms;
    $updateLeadTimeQueriesAtp[] = $updateLeadTimeQueryAtp;

}

echo "updating inventory\n";
$errQueries="";
 updateInventory(null,null,$updateLeadTimeQueriesIms,null,null,$updateLeadTimeQueriesAtp,null,null,null,$errQueries, false);

if (strlen($errQueries) > 0){
    $message = $message."Updates that failed to execute :--\n";
    $message = $message.$errQueries;
    echo $message;
}

mysql_close($imsConn);

//echo "###skuQueries### \n".implode("\n",$skuQueries);
echo ")" . date('j-m-Y H:i:s') . "]";
?>
