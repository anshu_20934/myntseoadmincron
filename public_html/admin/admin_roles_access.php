<?php
/**
 * Created by Ravi Gupta.
 * Date: 17 Oct, 2011
 */
require_once "./auth.php";
require_once $xcart_dir."/include/security.php";
include_once $xcart_dir."/include/class/admin/RolesAccess.php";
include_once $xcart_dir."/include/dao/class/admin/RolesAccessDBAdapter.php";

	$time=time();	
	$login=$XCART_SESSION_VARS['identifiers']['A'][login];
	$rolesAccess = new RolesAccess(mysql_real_escape_string($login));
	
	if($mode=='add'){
		$data = array(
			"pagename"=>mysql_real_escape_string(trim($_POST['access_field_new'])),
			"roles"=>mysql_real_escape_string(implode(",", $_POST['access_roletype_new'])),
			"extra_logins_allowed"=>mysql_real_escape_string(trim($_POST['extra_logins_allowed_new'])),
			"created_on"=>($time),
			"created_by"=>($login),
			"updated_on"=>($time),
			"updated_by"=>($login)
			);
		if($rolesAccess->initRolesAccessObject($data)){
			$dbApdater = RolesAccessDBAdapter::getInstance();
			$message_action = $dbApdater->addRolesAccess($rolesAccess);
		}
		
		//passing the message to top_message of dialog message handler
		if($message_action == RolesAccessDBAdapter::SUCCESS_ADD) {
			$top_message["content"] = "Roles access for the URL is succesfully added.";
		}
		else if($message_action == RolesAccessDBAdapter::ERROR_ADD) {
			$top_message["content"] = "Roles access for the URL could not be added.";
			$top_message["type"]="E";
		}
		else if($message_action == RolesAccessDBAdapter::ERROR_ADD_FILE_ALREADY_EXIST) {
			$top_message["content"] = "Roles access for the URL already exists please use update if you want to make any changes.";
			$top_message["type"]="E";
		}
		
	} else if($mode == 'delete') {
		$rolesAccess->setPageid(mysql_real_escape_string(trim($_POST['roleaccess_id'])));
		$dbApdater = RolesAccessDBAdapter::getInstance();
		$message_action =  $dbApdater->deleteRolesAccess($rolesAccess);
		//passing the message to top_message of dialog message handler
		if($message_action == RolesAccessDBAdapter::SUCCESS_DELETE) {
			$top_message["content"] = "Roles Access for the URL is successfully deleted.";
		}
		else if($message_action == RolesAccessDBAdapter::ERROR_DELETE) {
			$top_message["content"] = "Roles Access for the URL could not be deleted.";
			$top_message["type"]="E";
		}

	} else if($mode == 'update') {
		$updateidarray = $_POST['update_id'];
		$updateaccesarray = $_POST['update_access_field'];
		$updateaccessrolearray = $_POST['roletypes'];
		$updateaccessextraloginsallowedarray = $_POST['update_extra_logins_allowed'];
		$updaterolesaccessarray = array();

		foreach($updateidarray AS $num=>$id) {
			$data = array(
				"id"=>$id,
				"pagename"=>mysql_real_escape_string(trim($updateaccesarray[$id])),
				"roles"=>mysql_real_escape_string(implode(",", $updateaccessrolearray[$id])),
				"extra_logins_allowed"=>mysql_real_escape_string(trim($updateaccessextraloginsallowedarray[$id])),
				"updated_on"=>($time),
				"updated_by"=>($login)
			);
			$rolesAccess = new RolesAccess(mysql_real_escape_string($login));
			$rolesAccess->initRolesAccessObject($data);
			$updaterolesaccessarray[] = $rolesAccess;
		}
		$dbApdater = RolesAccessDBAdapter::getInstance();
		$message_action1 =  $dbApdater->filterUpdateRolesAccess($updaterolesaccessarray);
		if($message_action1 == 1){
			$message_action =  $message_action1;
		}
		
		//passing the message to top_message of dialog message handler
		if($message_action == RolesAccessDBAdapter::SUCCESS_UPDATE) {
			$top_message["content"] = "Roles Access for the URL is successfully updated.";
		}
		else if($message_action == RolesAccessDBAdapter::ERROR_UPDATE) {
			$top_message["content"] = "Roles Access for the URL could not be updated.";
			$top_message["type"]="E";
		}
	}
	
	$dbApdater = RolesAccessDBAdapter::getInstance();
	$rolesAccessArray = $dbApdater->fetchRolesAccessArray();
	
	$roletypesresult = func_query("SELECT name, type FROM mk_roles");

	//create a dictionary using pagename as index in which each index is again
	// a dictionary with role type being the index of this child dictionary and 
	//its value is 0/1 if the page is accessible by this role type or not
	$pageroletype_cb_value = array();
	$results = array();
	foreach($rolesAccessArray as $pageaccess){
		$data = array(
			'id'=>$pageaccess->getPageid(),
			'pagename'=>$pageaccess->getPagename(),
			'extra_logins_allowed'=>$pageaccess->getExtraLoginsAllowed()
		);
		$results[]=$data;
		$rolepid = array();
		foreach($roletypesresult as $roletype){
			if(in_array($roletype["type"], explode(",", $pageaccess->getRoles()))){
				$rolepid[] = 1;
			}
			else {
				$rolepid[] = 0;
			}
		}
		$pageroletype_cb_value[] = $rolepid;
	}

	$smarty->assign("roleaccesspairs",  $results);
	$smarty->assign("pageroleaccess", $pageroletype_cb_value);
	$smarty->assign("roletypes",  $roletypesresult);
	$smarty->assign ("top_message", $top_message);
	$smarty->assign("main","admin_roles_access");
	func_display("admin/home.tpl",$smarty);
?>


