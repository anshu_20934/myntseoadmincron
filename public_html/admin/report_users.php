<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: statistics.php,v 1.39 2006/04/07 11:28:00 svowl Exp $
#

require "./auth.php";
require $xcart_dir."/include/security.php";
$location[] = array(func_get_langvar_by_name("lbl_user_report"), "");
x_session_register("date_range");

if(empty($mode)) { $mode = "" ; }
if(empty($region)){ $region = ""; }
if(empty($report_type)) {$report_type = "" ; }
if(empty($Startdate)) { $Startdate = "" ; }
if(empty($Enddate)) { $Enddate= "" ; }

$i=0;

if ($REQUEST_METHOD == "POST") {


	       $users = array();
		   $duration= array();

          	if ($mode == "search") {

					$Date1 =explode('/',$Startdate);
					$Date2 =explode('/',$Enddate);
					
					
					$startdate = mktime(0,0,0,$Date1[0],$Date1[1],$Date1[2]);
					$enddate   = mktime(0,0,0,$Date2[0],$Date2[1],$Date2[2]);

					//if(!empty($startdate) && !empty($enddate)){

			   if($region != "all"){
							
					$SQLText = "  AND c.s_state= '$region' GROUP BY  o.login";

			   }else{
					 $SQLText = " GROUP BY  o.login";
				}

				while($startdate <= $enddate ){

					
                    
					if($report_type == 'week'){  
                    
						   $stdate = $startdate + 7 * 24 * 60 * 60;
						   
						   $stdate1 = date('Y',$stdate).date('m',$stdate).date('d',$stdate)."000000"; 
						   $startdate1  = date('Y',$startdate).date('m',$startdate).date('d',$startdate)."000000"; 
					
					}elseif($report_type == 'month'){
              
						  $stdate = $startdate + (30 * 24 * 60 * 60); 
						  $stdate1 = date('Y',$stdate).date('m',$stdate).date('d',$stdate)."000000"; 
						  $startdate1  = date('Y',$startdate).date('m',$startdate).date('d',$startdate)."000000"; 
         
					}

							
							  

							$SQL[$i] = "SELECT  count( o.login ) AS num_of_users,count( o.orderid ) AS num_of_orders ,DATE_FORMAT($startdate1,'%m/%d/%y') as duration1,DATE_FORMAT($stdate1,'%m/%d/%y') as duration2 FROM $sql_tbl[customers] as c  ,$sql_tbl[orders] as o WHERE c.login = o.login  AND (date >= $startdate1 AND date <= $stdate1)  ";

							$SQL[$i] = $SQL[$i].$SQLText ;
							
						   $users[$i] = func_query($SQL[$i]);
						 					                       
					       $startdate = $stdate;
                           $i++;  

               
			         } //end of while 

                     // calculate the and total of all sales made
			         $totalusers = 0;
					 $totalorders = 0;
			   
                     for($j=0 ;$j< count($SQL);$j++){
					
					     $rs = mysql_query($SQL[$j]);
				         while($row = mysql_fetch_array($rs)){
                           $totalusers += $row[0] ;
						   $totalorders += $row[1] ;
						 }
					 mysql_free_result($rs);
		         } 
                /*end of for loop*/

					   
				
			}  //end of mode 

}

$regions = func_query ("SELECT distinct $sql_tbl[states].code,$sql_tbl[states].state FROM $sql_tbl[states] order by $sql_tbl[states].state");


#
# Assign Smarty variables and show template
#
$smarty->assign("region", $region);
$smarty->assign("mode", $mode);
$smarty->assign("regions", $regions);
$smarty->assign("users", $users);
$smarty->assign("totalOrders", $totalorders);
$smarty->assign("totalUsers",  $totalusers );
$smarty->assign("region",$region);
$smarty->assign("SDate", $Startdate);
$smarty->assign("EDate",$Enddate);

$smarty->assign("main", "user_report");

# Assign the current location line
$smarty->assign("location", $location);

# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
