<?php
/**
 * Created by Lohiya.
 * Date: 08 Jul, 2011
 */
require_once( "../auth.php");
include_once($xcart_dir."/include/class/landingPage/class.LandingPageUtils.php");
$login=$XCART_SESSION_VARS['identifiers']['A'][login];
$landing_page_admin = new LandingPageAdmin();
$action =$landing_page_admin->getAction();

if(!empty($action)){
    $top_message = $landing_page_admin->executeAction();
}


$query="select lp.id as id, lp.page_url as page_url, pp.page_key page_key, 
            lp.is_autosuggest, lp.alt_text 
            from landing_page lp left join parameterized_page pp 
            on lp.parameterized_page_id=pp.id"; 
$search_keyword = $landing_page_admin->getSearchKeyword();
if(!empty($search_keyword)){
    $search_keyword = mysql_real_escape_string( 
                        $landing_page_admin->getSearchKeyword());
    $query .= " where page_url like '%". $search_keyword ."%'";
}
$page = $landing_page_admin->getPage();
$page_size = $landing_page_admin->getPageSize();
$smarty->assign("page_size",$page_size);

$query .= " order by id desc limit ". ($page*$page_size) . "," 
            . ($page_size);
$landing_pages=func_query($query,true);
$landing_pages_size = sizeof($landing_pages);
if($page > 0){
    $smarty->assign("prev_page",($page -1));
}
if($landing_pages_size == $page_size){
    $smarty->assign("next_page",($page + 1));
}
$smarty->assign("landing_pages",$landing_pages);
$smarty->assign("search_keyword",$search_keyword);
$smarty->assign("main","myntra_landing_page");
@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
