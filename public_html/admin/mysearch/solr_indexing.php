<?php
require_once(dirname(__FILE__)."/../env/RemoteTimeouts.Config.php");

/**
 * Created by Lohiya.
 * Date: 25 Jan, 2012
 */

set_time_limit(0);

require_once( "../auth.php");
global $solr_servers;
$number_of_servers = sizeof($solr_servers);
$smarty->assign("solr_servers",$solr_servers);
$smarty->assign("number_of_servers",$number_of_servers);
$servers = $HTTP_POST_VARS['index_server'];
$thread_num=$HTTP_POST_VARS['threads'];
$server_count = sizeof($servers);

if($REQUEST_METHOD == "POST"){
	$query = "select count(*) as style_count from mk_product_style join mk_product_type t on mk_product_style.product_type=t.id  where mk_product_style.styletype='P' and mk_product_style.is_active='1' and t.type ='P' and mk_product_style.is_customizable='0' order by 1 desc";
	$res = func_query_first($query);

	// 	echo "<pre>";print_r($HTTP_POST_VARS);
	if($HTTP_POST_VARS['mode'] == 'index'){
		if(!empty($HTTP_POST_VARS['only_local_server'])){

			
			$style_count = $res['style_count'];
			$page_size=ceil($style_count / $thread_num);
			$total_threads = ($thread_num * $server_count);
			$message = "style_count :: ".$res['style_count']. "|| thread_num :: $thread_num || total threads :: $total_threads || page_size :: ". $page_size."<br>";//exit;
			$thread_number  = 1;
			// Full Index on all the servers seperately
			if(!empty($servers)){
				//create the multiple cURL handle
				$mh = curl_multi_init();
				$customize_type=$HTTP_POST_VARS['styletype'];
				$ch = array();
				foreach ($servers as $index_on_server) {
					for ($i=1 ; $i<=$thread_num;$i++){
 						$ch[$thread_number] = curl_init("http://$index_on_server/scripts/style_index.php?page_number=$i&page_size=$page_size&customize_type=$customize_type&local=true");
						//echo "http://$index_on_server/scripts/style_index.php?page_number=$i&page_size=$page_size&customize_type=$customize_type&local=true <br>";
						curl_setopt($ch[$thread_number], CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch[$thread_number], CURLOPT_HEADER, 0);
						
						
						curl_setopt($ch[$thread_number], CURLOPT_CONNECTTIMEOUT, \RemoteTimeouts::$localDCConnectTimeout);
						curl_setopt($ch[$thread_number], CURLOPT_TIMEOUT, 0); //it takes a long time to write to solr master ?

						curl_multi_add_handle($mh,$ch[$thread_number]);
						$thread_number++;
					}
				}
				$running = -1;
				do {
					curl_multi_exec($mh,$running);
				} while($running > 0);

				for ($i=1 ; $i<=$total_threads;$i++){
					$result = curl_multi_getcontent($ch[$i]);
					$message .= "Result for thread $i done :".$result."<br>";
				}
				//close the handles
				for ($i=1 ; $i<=$total_threads;$i++){
					curl_multi_remove_handle($mh, $ch[$i]);
				}
				curl_multi_close($mh);
			}
		}else{


			// Equally devide index on all the servers
			if(!empty($servers)){

				$style_count = $res['style_count'];
				$total_pages = $thread_num * $server_count;
				$page_size=ceil($style_count / ($total_pages));
				$message = "style_count :: ".$res['style_count']. "|| thread_num :: $thread_num || server_count :: $server_count || page_size :: ". $page_size."<br>";//exit;
				$page_number = 1;
				//create the multiple cURL handle
				$mh = curl_multi_init();
				$customize_type=$HTTP_POST_VARS['styletype'];
				$ch = array();
				foreach ($servers as $index_on_server) {
					for ($i=1 ; $i<=$thread_num;$i++){
 						$ch[$page_number] = curl_init("http://$index_on_server/scripts/style_index.php?page_number=$page_number&page_size=$page_size&customize_type=$customize_type");
						//echo "http://$index_on_server/scripts/style_index.php?page_number=$page_number&page_size=$page_size&customize_type=$customize_type <br>";
						curl_setopt($ch[$page_number], CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch[$page_number], CURLOPT_HEADER, 0);
						
						curl_setopt($ch[$page_number], CURLOPT_CONNECTTIMEOUT, \RemoteTimeouts::$localDCConnectTimeout);
						curl_setopt($ch[$page_number], CURLOPT_TIMEOUT, 0); //it takes a long time to write to solr master ?
						
						curl_multi_add_handle($mh,$ch[$page_number]);
						$page_number++;
					}
				}
				$running = -1;
				do {
					curl_multi_exec($mh,$running);
				} while($running > 0);
				for ($i=1 ; $i<=$total_pages;$i++){
					$result = curl_multi_getcontent($ch[$i]);
					$message .= "Result for thread $i done : ".$result."<br>";
				}
				//close the handles
				for ($i=1 ; $i<=$total_pages;$i++){
					curl_multi_remove_handle($mh, $ch[$i]);
				}
				curl_multi_close($mh);
			}
		}
	}
}

$smarty->assign("main","solr_indexing");
$smarty->assign("message",$message);
@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
