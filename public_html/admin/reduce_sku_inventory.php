
<?php
require "./auth.php";
include_once("$xcart_dir/include/security.php");
include_once("../include/func/func.inventory.php");

if(isset($HTTP_POST_VARS["reduceinventory"])){
$sku_id= $HTTP_POST_VARS['skuid'];
$sku_name= $HTTP_POST_VARS['sku'];
$sku_count= $HTTP_POST_VARS['sku_count'];
$thresholdcount= $HTTP_POST_VARS['thresholdcount'];
$title= $HTTP_POST_VARS['title'];
$addedby= $HTTP_POST_VARS['reducedby'];
$quantity_added= $HTTP_POST_VARS['quantity'];
$notes= $HTTP_POST_VARS['notes'];
$attachment= $HTTP_POST_FILES['attachment']['name'];
$image_name = basename($attachment);
$imagefile_size = $HTTP_POST_FILES['attachment']['size'];
$imagefile_tmp_name = $HTTP_POST_FILES['attachment']['tmp_name'];
$reduction_type= $HTTP_POST_VARS['reduction_type'];
$type="R";
$type_desc="Reduced";
///////////getting sku details
$skuDetails=func_getSkuproductDetails($sku_id);
$stringtext=$skuDetails[9];
$options="";
$myarray = explode(":",$stringtext);
    for($i=0;$i<count($myarray);$i++)
    	{
       $element = $myarray[$i];
      $option= func_getoption_namevalue($element);
      $options.=$option[0].":".$option[1]." |";
    	}
   $options=substr($options,0,strlen($options)-1);

/////
	$updatedCount=$sku_count-$quantity_added;
	$sku_createddate=time();	
	$sku_query="insert into `mk_inventory_log` (sku_id,date,changedby,title,inventory_type,inventory_type_desc,quantity,notes,attachment1,reduction_type_id) values ('$sku_id','$sku_createddate','$addedby','$title','$type','$type_desc','$quantity_added','$notes','$image_name','$reduction_type')";
	db_query($sku_query);
	//sending Notification if quantity goes below threshold
	if($updatedCount<$thresholdcount){
	               $from ="MyntraAdmin";
					$loginEmail="myntraops@gmail.com,myntracustcare@gmail.com";
					//$bcc="nitin.sirohi@gmail.com";
					$args = array("SKU_NO"	=>$skuDetails[0],"PRODUCT_TYPE"	=>$skuDetails[6],"PRODUCT_STYLE"	=>$skuDetails[7],"CURRENT_COUNT"	=>$updatedCount,"THRESH_COUNT"	=>$thresholdcount);

				    $template = "ThresholdEmail";
					sendMessage($template, $args, $loginEmail,$from,$bcc);
	}
	$update_count_query="update `mk_inventory_sku` set currentcount='$updatedCount' where sku_id='$sku_id'";
	db_query($update_count_query);
	func_upload_attachment($attachment,$imagefile_size,$imagefile_tmp_name);
	
	$smarty->assign("commentSuccess","Y");
	
	func_display("reduce_sku_inventory.tpl",$smarty);
	
}
else{
$skuid = $_GET['skuid'];
$date=date("d",time())."-".date("M",time()).",".date("Y",time());
$smarty->assign("skuid", $skuid);
$skuDetails=func_getSkuDetails($skuid);
$reduction_types=func_getreduc_types();
//$type=2;
//$style=3;
//$option="5:6:7:8";
//$quantity=120;
//$title="Used for fulfilling orderid 12456";
//func_reduceinventory($type,$style,$option,$quantity,$title);
$smarty->assign("sku_name", $skuDetails[0]);
$smarty->assign("sku_count", $skuDetails[3]);
$smarty->assign("thresholdcount", $skuDetails[4]);
$smarty->assign("date", $date);
$smarty->assign("reduction_types", $reduction_types);



func_display("reduce_sku_inventory.tpl",$smarty);
}



?>

	
