<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: statistics.php,v 1.39 2006/04/07 11:28:00 svowl Exp $
#

require "./auth.php";
require $xcart_dir."/include/security.php";
$location[] = array(func_get_langvar_by_name("lbl_discount_report"), "report_discount.php");

x_session_register("date_range");
if(empty($mode)) { $mode = "" ; }
if(empty($report_type)) {$report_type = "" ; }
if(empty($Startdate)) { $Startdate = "" ; }
if(empty($Enddate)) { $Enddate= "" ; }
if(empty($region)) { $region= "" ; }



 /* after submitting the search */

if ($REQUEST_METHOD == "POST") {


	

          	if ($mode == "search") {

                $Date1 =explode('/',$Startdate);
				$Date2 =explode('/',$Enddate);

                $startdate = $Date1[2]."-".$Date1[0]."-".$Date1[1];
				$enddate = $Date2[2]."-".$Date2[0]."-".$Date2[1];
 


				$SQL = "SELECT  discount.discountid,discount.discount,discount.name FROM  $sql_tbl[discounts] as discount ";

                


                /*if($prd_style  == "All"){
                   
				   $SQL .=" GROUP BY p.product_style ";

				}else{
					 
					 $SQL .=" AND ps.id LIKE $prd_style GROUP BY p.product_style ";

				}
				 */


			    $discounts = func_query($SQL);

               
			}

}


#
#Query to get gegions
#
$regions = func_query ("SELECT distinct $sql_tbl[countries].region FROM $sql_tbl[countries] order by $sql_tbl[countries].region");



#
# Assign Smarty variables and show template
#
$smarty->assign("reportType", $report_type);
$smarty->assign("SDate", $Startdate);
$smarty->assign("EDate", $Enddate);
$smarty->assign("rgn", $region);
$smarty->assign("mode", $mode);
$smarty->assign("regions", $regions);
$smarty->assign("discounts", $discounts);

$smarty->assign("main", "discount");

# Assign the current location line
$smarty->assign("location", $location);

# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
