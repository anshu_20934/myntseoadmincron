<?php
require_once  "auth.php";
require_once $xcart_dir.'/modules/RestAPI/RestRequest.php';

$url = HostConfig::$userGroupServiceUrl.'/';
$method = 'GET';
$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Content-type: application/json';
$headers[] = 'Accept-Language: en-US';
$request = new RestRequest($url, $method);
$request->setHttpHeaders($headers);
$request->execute();
$info = $request->getResponseInfo();
$body = $request->getResponseBody();
if($info['http_code'] == 200){
	$response = json_decode($body,true);
	if($response['groupResponse']['status']['statusType'] == 'SUCCESS'){
		if(count($response['groupResponse']['data']['UserGroup'][0])==0){
			$tempArr = array();
			array_push($tempArr, $response['groupResponse']['data']['UserGroup']);
			$smarty->assign('userGroup',$tempArr);
		}
		else{
			$smarty->assign('userGroup',$response['groupResponse']['data']['UserGroup']);						
		}
	}
}

if($XCART_SESSION_VARS['createNotification']){
	$smarty->assign('ntfFlag','true');
	$XCART_SESSION_VARS['createNotification'] = false;		
}
else{
	$smarty->assign('ntfFlag','false');
}
$smarty->assign('main','notifications_create');
func_display("admin/home.tpl",$smarty);
?>