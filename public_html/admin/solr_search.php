<?php
require_once "./auth.php";
require $xcart_dir."/include/security.php";
require_once($xcart_dir.'/include/search/class.SearchURLGenerator.php');
include_once($xcart_dir."/include/dao/class/search/class.MyntraLPAdapter.php");
require_once($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");
include_once($xcart_dir."/Cache/TableCache.php");
use base\dao\BaseDAO;

$parameters = $_GET;
$offset;
$only_page_url=$HTTP_POST_VARS['urlsearch'];
$searchLanding;
$lookType;
$url_open ="blank";

$smarty->assign("url_open",$url_open);

$sql = "select attribute_value from mk_attribute_type_values where attribute_type ='Brand' order by attribute_value";
$brandlist = func_query($sql,true);
$smarty->assign("brandli",$brandlist);

$sql = "select attribute_value from mk_attribute_type_values where attribute_type ='Age Group' order by attribute_value";
$ageGroup = func_query($sql,true);
$smarty->assign("ageGroup",$ageGroup);

$sql = "select attribute_value from mk_attribute_type_values where attribute_type ='Gender' order by attribute_value";
$gender = func_query($sql,true);
$smarty->assign("gender",$gender);

$sql = "select attribute_value from mk_attribute_type_values where attribute_type ='Fashion Type' order by attribute_value";
$fashionType = func_query($sql,true);
$smarty->assign("fashionType",$fashionType);

$sql = "select attribute_value from mk_attribute_type_values where attribute_type ='Usage' order by attribute_value";
$usage = func_query($sql,true);
$smarty->assign("usage",$usage);

$sql = "select attribute_value from mk_attribute_type_values where attribute_type ='Season' order by attribute_value";
$season = func_query($sql,true);
$smarty->assign("season",$season);

$sql = "select attribute_value from mk_attribute_type_values where attribute_type ='Colour' order by attribute_value";
$colour = func_query($sql,true);
$smarty->assign("colour",$colour);

$sql = "select attribute_value from mk_attribute_type_values where attribute_type ='Year' order by attribute_value";
$year = func_query($sql,true);
$smarty->assign("year",$year);

$sql = "select category,max_expiry_days from landing_page_category where is_active=1";
$lp_categories = func_query($sql,true);
$smarty->assign("lpCategories", $lp_categories);

$sql ="SELECT * FROM  mk_catalogue_classification where parent1 !=-1 and parent2 != -1 order by typename asc";
$article_types= func_query($sql,true);
$smarty->assign("article_types",$article_types);

$sql ="SELECT * FROM  mk_catalogue_classification where parent1 !=-1 and parent2 =-1 order by typename asc";
$sub_categories= func_query($sql,true);
$smarty->assign("sub_categories",$sub_categories);

$sql ="SELECT * FROM  mk_catalogue_classification where parent1 =-1 and parent2 = -1 order by typename asc";
$master_categories= func_query($sql,true);
$smarty->assign("master_categories",$master_categories);

$login = ''; $email = '';
if(isSet($XCART_SESSION_VARS['login'])){
    $login =  $XCART_SESSION_VARS['login'];

    $sql = "select email from xcart_customers where login=". $XCART_SESSION_VARS['login'];
    $out_page = func_query_first($sql, true);

    if(isSet($out_page['email']) && !empty($out_page['email'])){
        $email = $out_page['email'];
    }
}

$smarty->assign("login", $login);
$smarty->assign("email", $email);

function GetOrStringFromArray($inputArray)
{
	$orString ="";
	if(!empty($inputArray))
	{
		$orString="";
		for($i=0;!empty($inputArray[$i]);$i++)
		{
			if($i!=0)
			{
				$orString = $orString.",";
			}
			$orString = $orString."$inputArray[$i]";
		}
	}
	return $orString;
}
function GetOrStringFromRange($leftInput,$rightInput)
{
	$orString ="";
	$emptyString ="";
	$leftstring = floatval($leftInput);
	$rightstring = floatval($rightInput);
	if(empty($leftstring) && empty($rightstring))
	{
		return $emptyString;
	}
	if(empty($leftstring))
	{
		if(!empty($leftInput))
		{
			return $emptyString;
		}
		$leftstring = "*";
	}
	if(empty($rightstring))
	{
		if(!empty($rightInput))
		{
			return $emptyString;
		}
		$rightstring = "*";
	}
	$orString = "[$leftstring TO $rightstring]";
	return $orString;
}

function getValidStyles($idArray){
	$validStyles = array();
	foreach($idArray as $id){
		$trimmedId = mysql_real_escape_string($id);
		$result = func_query_first("select style_id from mk_style_properties where style_id = '$trimmedId'");
		if(count($result))
			array_push($validStyles, $trimmedId);
	}
	return $validStyles;
}
function getDataToPopulate($updateid){
	$query = "select * from curated_search_list where fk_parameterized_page_id = $updateid";
	return func_query_first($query);
}

$stringFieldArray = json_decode(FeatureGateKeyValuePairs::getFeatureGateValueForKey("paremetricSearchStringFields"));
//echo "<pre>";print_r($stringFieldArray);exit;
$smarty->assign("stringFieldArray",$stringFieldArray);
$rangeFieldArray = json_decode(FeatureGateKeyValuePairs::getFeatureGateValueForKey("paremetricSearchRangeFields"));
$remainingSlotRules = WidgetKeyValuePairs::getWidgetValueForKey("remainingSlotRules");
$remainingSlotRules = explode(",", $remainingSlotRules);
$smarty->assign("rangeFieldArray",$rangeFieldArray);

//echo"Data is <pre>";print_r($HTTP_POST_VARS);exit;
if($REQUEST_METHOD == "POST"){
	//echo"hitting";
	//echo"Data is <pre>";print_r($HTTP_POST_VARS);
	if($HTTP_POST_VARS['mode'] == "AddMode")
	{
		$looktype = "Add";
		//echo"ADD is <pre>";print_r($HTTP_POST_VARS);
	}
	if($HTTP_POST_VARS['mode'] == "EditMode")
	{
            $sql = "select parameter_string from parameterized_page where id=$updateid";
            $out_page = func_query_first($sql,true);
            $query_string =  substr($out_page["parameter_string"],1,-2);
            $query_params = explode(") AND ",$query_string);
            $query_param_array = array();
            foreach($query_params as $query_param){
                $key_value = explode(":(",$query_param);
                if(substr($key_value[1],0,1) == '"'){
                    $query_param_array[$key_value[0]]=explode('" OR "',substr($key_value[1],1,-1));
                }else if(substr($key_value[1],0,1) == '['){
                    $from_to = explode(' TO ',substr($key_value[1],1,-1));
                    $query_param_array[$key_value[0]]=array("from"=>$from_to[0],"to"=>$from_to[1]);
                }
            }

            $sql = "select landing_page_type, is_active from landing_page where parameterized_page_id = $updateid";
            $out_page = func_query_first($sql, true);

            $smarty->assign("selected_values",$query_param_array);
            $smarty->assign("page_url",$landingpage_url);
            $smarty->assign("parameter_page_id",$updateid);

            if(isSet($out_page["landing_page_type"]) && !empty($out_page["landing_page_type"])){
                $smarty->assign("landingPageType", $out_page["landing_page_type"]);
            }

            if(isSet($out_page["is_active"]) && $out_page["is_active"] == "1"){
                $smarty->assign("isactive", true);
            }

//            echo "<pre>";print_r($query_param_array);exit;
		$looktype = "Edit";
		//echo"ADD is <pre>";print_r($HTTP_POST_VARS);
	}
	if($HTTP_POST_VARS['mode'] == "CuratedEditMode")
	{
		//var_dump($HTTP_POST_VARS['Rule']);
		$toPopulateResult = getDataToPopulate($updateid);
		//var_dump($remainingSlotRules);
		$smarty->assign("rules", $remainingSlotRules);
		$smarty->assign("form_data", $toPopulateResult);
		$smarty->assign("parameter_page_id", $updateid);
		$looktype = "CuratedEdit";
	}
	if($HTTP_POST_VARS['mode'] == "CuratedEditGroup"){
		//var_dump($HTTP_POST_VARS['Rule'][0]);
		$updateid = $HTTP_POST_VARS['parameter_page_id'];
		$styleIdCsv = $HTTP_POST_VARS['styleIdCsv'];
		$inputStyleIds = explode(",", $styleIdCsv);

		for($i=0; $i<count($inputStyleIds); $i++) $inputStyleIds[$i] = trim($inputStyleIds[$i]);
		$inputStyleIds = array_filter($inputStyleIds);

		$validStyleIds = getValidStyles($inputStyleIds);

		$invalidStyleIds = array_diff($inputStyleIds, $validStyleIds);

		$errorMessage = "";
		if(empty($inputStyleIds)){
			$errorMessage .= "List of style ids is empty\n";
		}

		if(!empty($invalidStyleIds)){
			$invalidList = implode(",", $invalidStyleIds);
			$errorMessage .= "Invalid style ids : " . $invalidList ."\n";
		}

		if(count($validStyleIds) > (int)$HTTP_POST_VARS['noSlots']){
			$errorMessage .= "Number of slots is less than the number of valid style ids\n";
		}

		$curatedListCache = TableCache::getTableCache("curated_search_list", "fk_parameterized_page_id");
		if($errorMessage == ""){
			$validStyleList = implode(",", $validStyleIds);
			$curatedValues = array(
				'no_Slots' => $HTTP_POST_VARS['noSlots'],
				'style_id_csv' => mysql_real_escape_string($validStyleList),
				'remaining_slots_rule' => $HTTP_POST_VARS['Rule'],
				'fk_parameterized_page_id' => $updateid,
			);

			$fk_parameterized_page_id = $HTTP_POST_VARS['parameter_page_id'];
			$where = array('fk_parameterized_page_id'=>$updateid);
			$result = func_query_first("select fk_parameterized_page_id from curated_search_list where fk_parameterized_page_id = '$fk_parameterized_page_id'");
			//var_dump($result);
			if(count($result))
				$ret = func_array2updateWithTypeCheck(curated_search_list, $curatedValues, $where);
			else $ret = func_array2insertWithTypeCheck(curated_search_list, $curatedValues);

			$curatedListCache->refresh($fk_parameterized_page_id);
			$dbAdapter=MyntraPageDBAdapter::getInstance($login, $email);
			$dbAdapter->recacheServiceCache($fk_parameterized_page_id, "parameterizedPage");
		}
		else{
			$errorMessages = explode("\n", $errorMessage);
			for($i=0; $i<count($errorMessages); $i++){
				$errorMessages[$i] = "<h1 style=\"color:#FF0000\">$errorMessages[$i]</h1>";
			}
		}
		if($errorMessage == "")	$errorMessages = array("Updated succcessfully");

		$looktype = "CuratedEdit";
		$smarty->assign("errorMessages", $errorMessages);
		$smarty->assign("parameter_page_id", $updateid);
		$toPopulateResult = getDataToPopulate($updateid);
		$smarty->assign("form_data", $toPopulateResult);
		$smarty->assign("rules",$remainingSlotRules);
	}

	if($HTTP_POST_VARS['mode'] == "SearchMode")
	{
		$looktype = "";
		//echo"ADD is <pre>";print_r($HTTP_POST_VARS);
	}
	if($HTTP_POST_VARS['mode'] == "Previous")
	{
		$offset = $HTTP_POST_VARS['offset'];
		$searchLanding = $HTTP_POST_VARS['searchLanding'];
		$offset--;
	}
	if($HTTP_POST_VARS['mode'] == "Next")
	{
		$offset = $HTTP_POST_VARS['offset'];
		$searchLanding = $HTTP_POST_VARS['searchLanding'];
		$offset++;
	}
	if($HTTP_POST_VARS['mode'] == "Delete")
	{
		$deleteIds = explode(",",$HTTP_POST_VARS['deleteIds']);
        $lpAdapter=MyntraLPAdapter::getInstance($login, $email);
        foreach($deleteIds as $deleteId){
		    $hashKey = $deleteId;
            $lpAdapter->deleteMyntraPage($hashKey);
        }
	}
	if($HTTP_POST_VARS['mode'] == "StringSearch")
	{
		$offset = 0;
		$searchLanding = $HTTP_POST_VARS['searchLanding'];
	}
          ############## Condition to add groups ############
	if($HTTP_POST_VARS['mode'] == "addgroup" || $HTTP_POST_VARS['mode'] == "editgroup")
	{
		$looktype = "Add";
		$offset = $HTTP_POST_VARS['offset'];
		$searchLanding = $HTTP_POST_VARS['searchLanding'];
		$input = $HTTP_POST_VARS;
		$andArray = array();
		$ageGroupString =GetOrStringFromArray($input[ageGroup]);
        if(!empty($ageGroupString))
		{
			$andArray["global_attr_age_group"] = $ageGroupString;
		}
		$articleTypeString =GetOrStringFromArray($input[article_types]);
        if(!empty($articleTypeString))
		{
			$andArray["global_attr_article_type_facet"] = $articleTypeString;
		}
		$brandString = GetOrStringFromArray( $input[brand]);
		if(!empty($brandString))
		{
			$andArray["brands_filter_facet"] = $brandString;
		}
		$colourString = GetOrStringFromArray( $input[colour]);
		if(!empty($colourString))
		{
			$andArray["global_attr_base_colour"] = $colourString;
		}
		$discountPriceString = GetOrStringFromRange( $input[DP][0], $input[DP][1]);
        if(!empty($discountPriceString))
		{
			$andArray["discounted_price"] = $discountPriceString;
		}
		$fashionString =GetOrStringFromArray($input[fashionType]);
        if(!empty($fashionString))
		{
			$andArray["global_attr_fashion_type"] = $fashionString;
		}
		$genderString =GetOrStringFromArray($input[gender]);
        if(!empty($genderString))
		{
			$andArray["global_attr_gender"] = $genderString;
		}
		$masterCatString =GetOrStringFromArray($input[master_category]);
        if(!empty($masterCatString))
		{
			$andArray["global_attr_master_category"] = $masterCatString;
		}
		$priceString = GetOrStringFromRange( $input[Price][0], $input[Price][1]);
        if(!empty($priceString))
		{
			$andArray["price"] = $priceString;
		}
		$seasonString =GetOrStringFromArray($input[season]);
        if(!empty($seasonString))
		{
			$andArray["global_attr_season"] = $seasonString;
		}
		$subCategoryString =GetOrStringFromArray($input[sub_category]);
        if(!empty($subCategoryString))
		{
			$andArray["global_attr_sub_category"] = $subCategoryString;
		}
		$usageString =GetOrStringFromArray($input[usage]);
        if(!empty($usageString))
		{
			$andArray["global_attr_usage"] = $usageString;
		}
		if($input[Year][0] == "NA")
		{
			$input[Year][0] = "";
		}
		if($input[Year][1] == "NA")
		{
			$input[Year][1] = "";
		}
		$yearString = GetOrStringFromRange( $input[Year][0], $input[Year][1]);
        if(!empty($yearString))
		{
			$andArray["global_attr_year"] = $yearString;
		}
		if(!empty($input[catAdd]))
		{
			$andArray["global_attr_catalog_add_date"] = $input[catAdd];
		}
		if(!empty($input[keywords]))
		{
			$andArray["keywords"] = $input[keywords];
		}

        $landingPageType = "";
        if(!empty($input[landingPageType])){
            $landingPageType = $input[landingPageType];
        }

        $expiryDate = "";
        if(!empty($input[expiryDate])){
            $expiryDate = $input[expiryDate];
        }

        $isactive = false;
        if($input[isactive] == "on"){
            $isactive = true;
        }

		$input[landingPage] = strtolower($input[landingPage]);
		// Manage extra Keywords introduced
		// Search Strings
		for($i=0; !empty($input["stringFieldName"][$i]);$i++)
		{
			if(!empty($input["stringFieldValue"][$i]))
			{
				$andArray[$input["stringFieldName"][$i]] = $input["stringFieldValue"][$i];
			}
		}
		//
		for($i=0; !empty($input["rangeFieldName"][$i]);$i++)
		{
			$tempString = GetOrStringFromRange( $input[rangeFieldValueLeft][$i], $input[rangeFieldValueRight][$i]);
			if(!empty($tempString))
			{
				$andArray[$input["rangeFieldName"][$i]] = $tempString;
			}
		}
		//echo"\nstriggggng is <pre>";print_r($andArray);exit;
		if($input[landingCheck] == "on")
		{
			$andArray[is_landingpage] = true;

			if(!empty($input[landingPage]))
			{
		        $lpAdapter=MyntraLPAdapter::getInstance($login, $email);
				if($HTTP_POST_VARS['mode'] == "addgroup" && $lpAdapter->isLandingPage($input[landingPage]))
				{
					$message = "<h1 style=\"color:#FF0000\">Landing Page Already exists </h1>";
				}
				$andArray[url_desc] = $input[landingPage];
			}
		}

		$andArray[generatelink] = "y";
		//echo "hitaaa 1\n";print_r($andArray);
		$urlGenerator = new SearchURLGenerator($andArray, $isactive, $expiryDate, $landingPageType);
		//echo "hit 2\n";
	    if($HTTP_POST_VARS['mode'] == "editgroup"){
		    $key_desc = $urlGenerator->getSolrQueryAndDesc($HTTP_POST_VARS['parameter_page_id']);
        }else{
		    $key_desc = $urlGenerator->getSolrQueryAndDesc();
        }

		//echo"Url is ";print_r($key_desc);exit;
		//echo "hit 3\n";
		if(!empty($key_desc[error]))
		{
			$message = "<h1 style=\"color:#FF0000\">$key_desc[error]</h1>";
		}
		//echo "hit 4\n";
		$url_desc = $key_desc['page_desc'];
		$url_desc = str_replace("[", "-", $url_desc);
		$url_desc = str_replace("]", "-", $url_desc);
		$url_desc = str_replace("*", "star", $url_desc);
        if(!empty($key_desc['page_key'])){
            $url_desc = "/../../".$url_desc."/".$key_desc['page_key'].".mnt";
        }else{
            $url_desc = "/../../".$url_desc;
        }
		$smarty->assign("looktype",$looktype);
		$smarty->assign("main","solr_search");
		$smarty->assign("message",$message);
		if(!empty($key_desc['page_desc']))
		{

			$smarty->assign("url_open",$url_desc);
		}
		func_display("admin/home.tpl",$smarty);
		return;

	}
 }




$numberOfentry = 50;


if(empty($offset))
{
	$offset =0;
}
$smarty->assign("offset",$offset);



$sql = "select para.id,land.page_url,para.parameter_string, para.parameter_hash_key as hashkey,para.page_key as pagekey,
para.page_desc as pagedesc,para.parameter_string as searchstring, land.is_active as isactive, land.expiry_date as expiryDate
from parameterized_page as para left join landing_page as land on para.id=land.parameterized_page_id ";
if(!empty($searchLanding) && empty($only_page_url))
{
	$sql = $sql." where land.page_url like '%$searchLanding%' or parameter_string like  '%$searchLanding%' or parameter_hash_key like '%$searchLanding%'   or page_key like '%$searchLanding%'";
}else{
    $sql = $sql." where land.page_url like '$searchLanding%'";
}
$leftLimit = $offset*$numberOfentry;
$rightLimit = $leftLimit + $numberOfentry -1;
$sql = $sql." order by para.id desc";
$sql = $sql." limit $leftLimit ,$rightLimit;";
$SearchResult = func_query($sql,true);
$smarty->assign("searchresult",$SearchResult);
//echo"<pre>";print_r($sql);
$sql = "select count(*) as count from parameterized_page as para left join landing_page as land on para.id=land.parameterized_page_id";
if(!empty($searchLanding))
{
	$sql = $sql." where land.page_url like '%$searchLanding%' or parameter_string like  '%$searchLanding%' or parameter_hash_key like '%$searchLanding%'  or page_key like '%$searchLanding%' ";
}
$countResult = func_query($sql,true);
//echo"<pre>";print_r($countResult);
$maxoffset =ceil (($countResult[0][count]-1)/$numberOfentry) -1;
$smarty->assign("maxoffset",$maxoffset);


$smarty->assign("searchLanding",$searchLanding);

if(!empty($only_page_url)){
    $smarty->assign("urlsearch",$only_page_url);
}

if(empty($looktype))
{
	$looktype = "Search";
}
$smarty->assign("looktype",$looktype);
$smarty->assign("main","solr_search");
$smarty->assign("message",$message);
func_display("admin/home.tpl",$smarty);
?>
