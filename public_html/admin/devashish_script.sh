echo "Starting tat update"
total_line=`wc -l < devashish_updateLeadTime.csv`
interval=100

echo "no. of lines to be updated is:$total_line"

# for loop for fixed intervals
for i in $(seq 1 $interval $total_line)
do
  end_line=$((i+interval-1))
  echo "=======================================================================================================================================================\n"
  echo "updating for $i to $end_line"
  sed "$i,$end_line!d" devashish_updateLeadTime.csv > updateLeadTime.csv
  php updateLeadTime.php
  sleep 10
  echo "=======================================================================================================================================================\n"
done
