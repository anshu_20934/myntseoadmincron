<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: admin_product_types.php,v 1.13 2006/01/11 06:55:57 mclap Exp $
#
# Product Type management module
#
@set_time_limit(1800);
/*
define("IS_MULTILANGUAGE", 1);
*/
define('USE_TRUSTED_POST_VARIABLES',1);
$trusted_post_variables = array("list","message");

require "./auth.php";
require $xcart_dir."/include/security.php";

$location[] = array(func_get_langvar_by_name("lbl_prdtypes_management"), "");

 $SQL = "SELECT pt.id, pt.name,pt.label,pt.is_active,pt.display_order,count(ps.id) as styles from $sql_tbl[mk_product_type] pt LEFT OUTER  JOIN  $sql_tbl[mk_product_style] ps ON pt.id = ps.product_type GROUP BY pt.id order by pt.display_order";

//$SQL = "SELECT  id,name,product_type FROM $sql_tbl[mk_product_style] GROUP BY product_type";
$producttype = func_query($SQL);

  $i =0;


foreach($producttype as $key=>$value )
{
	   $SQL_Count_Product = "SELECT count(p.productid) as no_of_products  from $sql_tbl[mk_product_type] pt 
	   INNER  JOIN  $sql_tbl[products] p ON pt.id = p.product_type_id WHERE p.product_type_id = $value[id] 
	   GROUP BY pt.id";
	   $row_no_of_products = db_query($SQL_Count_Product);
	   $row = db_fetch_array($row_no_of_products);
	   $no_of_products[$i] = $row['no_of_products'];
	   $i++;
}




$smarty->assign ("producttype", $producttype);
$smarty->assign("no_of_products",$no_of_products);
$smarty->assign("main","admin_product_type");


# Assign the current location line
$smarty->assign("location", $location);

# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
