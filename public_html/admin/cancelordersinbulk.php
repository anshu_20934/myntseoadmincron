<?php
require "./auth.php";
require_once $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.mkcore.php";
require_once $xcart_dir."/include/func/func.order.php";
require_once $xcart_dir."/include/func/func.mk_orderbook.php";
require_once $xcart_dir."/PHPExcel/PHPExcel.php";
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");

$action  = $HTTP_POST_VARS['action'];
global $weblog;
$uploadStatus = false;
if($action == '')
    $action ='upload';

    
$cancel_codes = func_query("select * from cancellation_codes where cancellation_mode = 'PART_ORDER_CANCELLATION' and is_active = 1");

$reject_reason_type= array();

$reject_reasons=array();
$reject_reason_type[0] = 'Select cancellation type';
$reject_reasons[0] = array('0'=>'Select cancellation Reason');
foreach($cancel_codes as $cancel_code){
	if($reject_reason_type[$cancel_code['cancel_type']] == null){
		$reject_reason_type[$cancel_code['cancel_type']] = $cancel_code['cancel_type_desc']; 
	}
	if($reject_reasons[$cancel_code['cancel_type']] == null){
		$reject_reasons[$cancel_code['cancel_type']] = array();
	}
	$reject_reasons[$cancel_code['cancel_type']][$cancel_code['cancel_reason']] = $cancel_code['cancel_reason_desc']; 
}    
$smarty->assign("reject_reason_type",$reject_reason_type);
$smarty->assign("reject_reasons",$reject_reasons);
if ($REQUEST_METHOD == "POST" && $action=='verify') {
	
	if(!isset($_FILES) && isset($HTTP_POST_FILES))
    	$_FILES = $HTTP_POST_FILES;

    $file_extn = '';
    if(!empty($_FILES['orderdetailsfile']['name'])) {
            $uploaddir1 = '../bulkorderupdates/';
            $extension = explode(".",basename($_FILES['orderdetailsfile']['name']));
            $uploadfile1 = $uploaddir1 . date('d-m-Y-h-i-s').".".$extension[1];
            $file_extn = $extension[1];
            if (move_uploaded_file($_FILES['orderdetailsfile']['tmp_name'], $uploadfile1)) {
            	$uploadStatus = true;
            }
    }
    $progressKey = $HTTP_POST_VARS['progresskey'];
    
    if($uploadStatus)
    {
    	$cancellation_type = $HTTP_POST_VARS['cancellation_type'];
 		$reason = $HTTP_POST_VARS['cancellation_reason'];
	    if($file_extn == 'xlsx') {
			$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		} else {
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
		}
		
		$objPHPExcel = $objReader->load($uploadfile1);
 		$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
 		$orderids = array();
 		$orderid2couponDetails = array();
 		for($i=2; $i<=$lastRow; $i++){//start from second row ignore heading
	   	    $cellObj = ($objPHPExcel->getActiveSheet()->getCell('A'.$i));
	        $orderid = trim($cellObj->getvalue());
	        
	        if(trim($orderid) != ""){
	        	$orderids[] = $orderid;
	        	
	        	$coupon_details = array();
	        	$cellObj = ($objPHPExcel->getActiveSheet()->getCell('B'.$i));
	        	$coupon_details['amount'] = trim($cellObj->getvalue());
	        	
	        	$cellObj = ($objPHPExcel->getActiveSheet()->getCell('C'.$i));
	        	$coupon_details['min_amount'] = trim($cellObj->getvalue());
	        	
	        	$cellObj = ($objPHPExcel->getActiveSheet()->getCell('D'.$i));
	        	$coupon_details['days'] = trim($cellObj->getvalue());
	        	
	        	$cellObj = ($objPHPExcel->getActiveSheet()->getCell('E'.$i));
	        	$coupon_details['count'] = trim($cellObj->getvalue());
	        	
	        	$cellObj = ($objPHPExcel->getActiveSheet()->getCell('F'.$i));
	        	$full_order_cancellation = trim($cellObj->getvalue())=='true'?true:false;
	        	
	        	$orderid2fullcancellation[$orderid] = false;
	        }
 		}
 		
		if(empty($orderids)) {
 			$errormsg = "No orderids present in the file. Please check the file and upload again.";
 		} else {
 			if( $reason === 'RTO') {
				$valid_orderids_sql = "Select orderid from $sql_tbl[orders] WHERE orderid in (".implode(",", $orderids).") and status in ('SH')"; 				
 			} else {
 				$valid_orderids_sql = "Select orderid from $sql_tbl[orders] WHERE orderid in (".implode(",", $orderids).") and status in ('Q', 'WP', 'OH')";
 			}
 			$valid_orderid_rows = func_query($valid_orderids_sql, TRUE);
 			$valid_orderids = array();
 			foreach ($valid_orderid_rows as $row) {
 				$valid_orderids[] = $row['orderid'];
 			}
 			
 			$invalid_orderids = array_diff($orderids, $valid_orderids);
 			
 			if(!empty($invalid_orderids)) {
 				$errormsg = "Following OrderId(s) were not marked cancelled - <br/>".implode(", ", $invalid_orderids);
 			} 
 			if(!empty($valid_orderids)) {
 				$xcache = new XCache();
        		$progressinfo = array('total'=>count($valid_orderids), 'completed'=>0);
        		$xcache->store($progressKey."_progressinfo", $progressinfo, 86400);
        		
        		//This will automatically not do most of the actions when there is no real change in status
				$sendGoodWillCoupons = $_POST['send_goodwill_coupons']?true:false;
				$update_inv = true;
				if($reason == 'RTO' || $reason == 'RTOCR')
					$update_inv = false;
				
				$cancel_args = array(
					'cancellation_type' 			=> $cancellation_type,
					'reason'						=> $reason,
					'orderid2fullcancelordermap'	=> $orderid2fullcancellation,
					'generateGoodWillCoupons'		=> $sendGoodWillCoupons,
					'orderid2couponDetails'			=> $orderid2couponDetails
				);
				
				$responses = func_change_order_status($valid_orderids, 'F', $login, $_POST['cancellation_comment'], "", true, '', $cancel_args);
 				
 				$msg = implode("<br>", $valid_orderids);
 			}
 			//send a report for the cancelled orders
 			$status = func_send_bulk_cancellation_report($valid_orderids, $invalid_orderids, $_POST['login']);
 			$weblog->info("Sent Mail for cancelled orders report - $status");
 		}
    } else {
    	$errormsg = "Not able to upload file. Please try again";
    }
    $smarty->assign("progresskey", $progressKey);
    $smarty->assign("message", $msg);
    $smarty->assign("errormessage", $errormsg);
}

if($REQUEST_METHOD == "GET") {
	$uniqueId = uniqid();
    $smarty->assign('progresskey', $uniqueId);    	
}
        
$smarty->assign("main", "cancelorders");
func_display("admin/home.tpl",$smarty);
?>
