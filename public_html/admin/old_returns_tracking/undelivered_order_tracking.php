<?php
require "../auth.php";
require $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.returns.php";
require_once $xcart_dir."/include/func/func.mk_old_returns_tracking.php";

$couriers = get_supported_courier_partners(false);

$statuses = _get_old_ud_states(); 
$returnStatuses = array();
foreach($statuses as $key => $value ) {
	$d = array();
	$d['code'] = $key;
	$d['display_name'] = $value;
	$returnStatuses[] = $d;	
}

$smarty->assign("couriers", json_encode($couriers));
$smarty->assign("statuses", json_encode($returnStatuses));
$smarty->assign("alldates", json_encode($returnStatuses));
$smarty->assign("main","old_returnedorders_tracking");
func_display("admin/home.tpl", $smarty);

?>
