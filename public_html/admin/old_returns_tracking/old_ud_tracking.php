<?php
require "../auth.php";
require $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.returns.php";
require_once $xcart_dir."/include/func/func.mk_old_returns_tracking.php";

$mode = $_REQUEST['action'];
$orderid = $_REQUEST['order_id'];

global $weblog;
$userroleentry = $XCART_SESSION_VARS['user_roles'];
$userroles = array();
foreach($userroleentry as $entry) {
    $userroles[] = $entry['role'];
}

if(empty($userroles)){
	$weblog->debug("Setting default admin role");
	$userroles = 'AD';
}	

$weblog->debug($userroles);

$couriers = get_supported_courier_partners(false);
$smarty->assign("couriers", $couriers);

if ($mode == 'update') {
	$prev_state = $_REQUEST['currst'];	
	$curr_state = $_REQUEST['rt_action'];	

	$cc_disposition = null;
	if(!is_null($_REQUEST['ccdisposition']) && !empty($_REQUEST['ccdisposition'])) {
		$cc_disposition = addslashes($_REQUEST['ccdisposition']);	
	}	
	$cc_resolution = null;
	if (!is_null($_REQUEST['resolution']) && !empty($_REQUEST['resolution'])) {
		$cc_resolution = addslashes($_REQUEST['resolution']);
	}
	$comment = '-';
	if(!is_null($_REQUEST['comment']) && !empty($_REQUEST['comment'])) {
		$comment = addslashes($_REQUEST['comment']);
	}

	if (!is_null($cc_disposition)) {
		$ccdispositions = _get_old_ud_cc_dispositions();
		$comment_1 = "CC Disposition : " . $ccdispositions[$cc_disposition];	
		if(!is_null($cc_resolution)) {
                        $ccresolutions = _get_old_ud_cc_resolution();
                        $comment_1 .= "<br/>CC Resolution : ". $ccresolutions[$cc_resolution] ;
        	}
		$comment = $comment_1 . "<br/><br/>" . $comment ;
	}	
	
	$status = addUDAction($orderid, $prev_state, $curr_state, $login, $comment, $cc_disposition, $cc_resolution);
	$weblog->debug(print_r($status,true));		
	if(!$status['status']){
		$smarty->assign("errormsg", $status['reason']);
	}
} else if ($mode == 'add') {
	if (isUDOrder($orderid)) {
		$smarty->assign("errormsg", "This order ($orderid) is already being tracked already.");	
	}else {
		$courier = $_REQUEST['courier'];
		$data = array();
		$data['orderid'] = $orderid;
		$data['currstate'] = addslashes($_REQUEST['rt_action']);
		$data['iscod']	 = addslashes($_REQUEST['iscod']);
		$data['custname']= addslashes($_REQUEST['cust_name']);
		$data['pickupaddress'] = addslashes($_REQUEST['cust_addr']);
		$data['pickupcity'] = addslashes($_REQUEST['cust_city']);
		$data['pickupstate'] = addslashes($_REQUEST['cust_state']);
		$data['pickupcountry'] = addslashes($_REQUEST['cust_country']);
		$data['pickupzipcode'] = addslashes($_REQUEST['cust_zipcode']);
		$data['phonenumber'] = addslashes($_REQUEST['contact_no']);
		$data['courier'] = addslashes($_REQUEST['courier']);
		$data['trackingno'] = addslashes($_REQUEST['tracking_num']);
		$data['attemptcount'] = addslashes($_REQUEST['delivery_attempts']);
		$data['reason'] = addslashes($_REQUEST['reason']);
		createUDOrder($login, $data);		
	}	
}


$orderData 	= getUDOrderData($orderid);

$editHistory    = getUDActions($orderid); 

$rtActions = get_ud_next_states($orderid, $userroles);
$smarty->assign("rtactions", $rtActions);
$smarty->assign("allstates", _get_old_ud_states());

$smarty->assign('ccdispositions', _get_old_ud_cc_dispositions());
$smarty->assign('ccresolution', _get_old_ud_cc_resolution());

$smarty->assign("orderData", $orderData);
$smarty->assign("edithistory", $editHistory);
$smarty->assign("main","old_ud_tracking");
func_display("admin/home.tpl", $smarty);
?>
