<?php
require "../auth.php";
include_once("$xcart_dir/include/security.php");
require $xcart_dir."/include/func/func.mk_old_returns_tracking.php";

global $sqllog;

$action = $_REQUEST['action'];
$uploadstatus = false;
if ($action == '') {
	$action = 'upload';
} else if ($action == 'verify') {
	if(!isset($_FILES) && isset($HTTP_POST_FILES))
                $_FILES = $HTTP_POST_FILES;
	if(!empty($_FILES['orderdetail']['name'])) {
		$uploaddir1 = '../../bulkrtoqueuing/';
                $extension = explode(".",basename($_FILES['orderdetail']['name']));
                $uploadfile1 = $uploaddir1 . "UDLS-" .date('d-m-Y-h-i-s').".".$extension[1];
                if (move_uploaded_file($_FILES['orderdetail']['tmp_name'], $uploadfile1)) {
                         $uploadStatus = true;
                }
		if ($uploadStatus) {
			$fd = fopen ($uploadfile1, "r");
			$orderstoqueue = array();
			$failedtoqueue = array();
			$dataformaterror = "Data Format Should be : {{CellA-Orderid}}";
			$line = 0;
			while(!feof($fd)) {
				$buffer = fgetcsv($fd, 4096);
                                $line = $line + 1;
				if ($line == 1) continue;
                                if (empty($buffer[0])) {
                                        break;
                                }
				
				$fieldCount = count($buffer);
				if($fieldCount <1) {
					$smarty->assign('dataFormatError', "$dataFormatError at line# $line");
                                        break;
				}	
				$orderid 	= $buffer[0];
				$deliverycount 	= 1; //$buffer[1];
		
				$sqllog->debug("IS UD order ?" . isUDOrder($orderid));
	
				if (!isUDOrder($orderid)) {
					$failedtoqueue[] = array('orderid' => $orderid, 'reason' => 'Not a UD order.');
					continue;
				}
				
				$udData = getUDOrderData($orderid);
							
				$sqllog->debug("Data => for $orderid");
				$sqllog->debug($udData);
				if($udData['currstate'] != 'UDCC' ) {
					$failedtoqueue[] = array('orderid' => $orderid, 'reason' => 'Order NOT in UDCC state');	
				} else {
					$orderstoqueue[] = $orderid;
					addUDAction($orderid, 'UDCC', 'UDLS', $login, '- bulk upload for UDLS-');
				}		
			}
			fclose ($fd);
			$sqllog->debug($orderstoqueue);
			$smarty->assign('orders', $orderstoqueue);
			$smarty->assign('failedorders', $failedtoqueue);
		}	
	}

}

$smarty->assign("action", $action);
$smarty->assign("main", "upload_udlss_to_queue");
func_display("admin/home.tpl",$smarty);

?>
