<?php
/*
 * Created on May 14, 2012
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
/* 
 require("../auth.php");
 include_once("$xcart_dir/include/security.php");
 include_once("$xcart_dir/include/func/func.order.php");
 include_once("$xcart_dir/include/func/func.mk_old_returns_tracking.php");
 
 $action = $_REQUEST['action'];
$uploadstatus = false;
if (empty($action) )
	$action = 'upload';

if ($action == 'verify') {
	if(!isset($_FILES) && isset($HTTP_POST_FILES))
		$_FILES = $HTTP_POST_FILES;
	
	$errorMsg = "";
	if(!empty($_FILES['rto_orders']['name'])) {
		$uploaddir = '../../bulkrtoqueuing/';
		$extension = explode(".",basename($_FILES['rto_orders']['name']));
        $uploadfile = $uploaddir . "rto-cancellation-" .date('d-m-Y-h-i-s').".".$extension[1];
        if (move_uploaded_file($_FILES['rto_orders']['tmp_name'], $uploadfile)) {
        	$uploadStatus = true;
        }
        
        if ($uploadStatus) {
			$orderids = array();
			$invalidOrderIds = array();
			$fd = fopen ($uploadfile, "r");
			while (!feof ($fd)) {
	        	$buffer = fgetcsv($fd, 4096);
	        	$orderid = trim($buffer[0]);
	        	if(empty($orderid))
	        		continue;
	        		
	            if(!isInteger($orderid)){
	            	$invalidOrderIds[] = $orderid;	
	            	continue;
	            }
	            
	  			$orderids[] = $orderid;	            	
	            if(count($orderids) > 100) {
          			$errorMsg .= "Maximum of 100 orders can be RTO cancelled using bulk uploader in single execution.<br/>";
          			break;
	            }
	        }
	        fclose ($fd);
	        
	        if(count($orderids) > 0 && empty($errorMsg)) {
	        if(count($orderids) > 0 && empty($errorMsg) ) {
		        $response = validateOrdersForRTOCancellation($orderids);
	            if($response['status'] == 'FAILURE') {
	            	if($response['invalid_rtos'] && !empty($response['invalid_rtos'])){
	            		$errorMsg .= "Following Orders are not valid RTOs: ".implode(", ", $response['invalid_rtos'])."<br/>";
	            	}
	            	if($response['not_old_rtos'] && !empty($response['not_old_rtos'])){
	            		$errorMsg .= "Only RTOs older than 7 days can be cancelled. Following RTO orders are not old: ".implode(", ", $response['not_old_rtos'])."<br/>";
	            	}
	            	if($response['invalid_status_orders'] && !empty($response['invalid_status_orders'])){
	            		$errorMsg .= "Following RTO Orders are not in Shipped/Complete/Delivered status: ".implode(", ", $response['invalid_status_orders'])."<br/>";
	            	}
	            	if($response['invalid_status_rtos'] && !empty($response['invalid_status_rtos'])){
	            		$errorMsg .= "Following RTO Orders are not in Queued/Received status: ".implode(", ", $response['invalid_status_rtos'])."<br/>";
	            	}
	            } 
	        }
	        
	        if(count($invalidOrderIds) > 0)
	        	$errorMsg .= "Following Orders are not valid - ".implode(",", $invalidOrderIds);
	        
        } else {
        	$errorMsg .=  "<br>File not uploaded, try again.. <br>";
        	$action = "upload";
        }
        
	} else {
		$errorMsg .=  "<br>No file name provided..<br>";
		$action = "upload";
	}
	
	if(empty($errorMsg)){
		$uniqueId = uniqid();
		$smarty->assign('progresskey', $uniqueId);
		$smarty->assign('orderids', implode("<br>", $orderids));
		$smarty->assign('uploadorderFile',$uploadfile);		
	} else {
		$action = 'upload';
		$smarty->assign("errorMsg", $errorMsg);
	}
} else if ($action == 'confirm') {
	$filetoread  = $HTTP_POST_VARS['filetoread'];
	$progressKey = $HTTP_POST_VARS['progresskey'];
    if(file_exists($filetoread)){
        $fd = fopen ($filetoread, "r");
        $orderIds = array();
        while (!feof ($fd)) {
        	$buffer = fgetcsv($fd, 4096, ",");
        	$orderid = trim($buffer[0]);
        	if(empty($orderid))
	        		continue;
			$orderIds[] = $orderid;
        }
        fclose($fd);
        
        markRTOsCancelled($orderIds, $login, 'Marked from RTO Bulk Cancellation', $progressKey);
        $smarty->assign("successMsg", "Cancelled ".count($orderIds)." RTO orders successfully!");
    } else {
    	$smarty->assign("errorMsg", "Uploaded file not found.");
    }
    $action = 'upload';
}

$smarty->assign("action", $action);
$smarty->assign("main", "bulk_rto_cancellation");
func_display("admin/home.tpl",$smarty);
*/ 
?>
