<?php
require "../auth.php";
//require $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.returns.php";
require_once $xcart_dir."/include/func/func.mk_old_returns_tracking.php";
require_once $xcart_dir."/PHPExcel/PHPExcel.php";
include_once("$xcart_dir/modules/apiclient/WarehouseApiClient.php");

$action = $_REQUEST['action'];
global $errorlog;
if ($action == 'search') {
        $formatted_results = searchRTOs($_REQUEST);
        $filename = "RTO-report-" . date('Y-m-d-H-i-s');
        $alphabets = range('A','Z');
        $xls_data       = $formatted_results;

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $col_headers = array_keys($xls_data[0]);
        
        $shift = -1;
        foreach($col_headers as $col_num=>$header) {
        	try{
        		$celIndex = $alphabets[$col_num%26];
        		if($celIndex == 'Z'){
        			$shift++;
        		}
        		
        		if($col_num > 25){
        			$celIndex = $alphabets[$shift] . $celIndex;
        		}
                $objPHPExcel->getActiveSheet()->SetCellValue($celIndex."1", $header);
        	}catch(Exception $e){
                $errorlog->error("Column: " . $col_num . " Header:" . $header . " Message:" . $e->getMessage());
            }
        }

        $row_num = 2;
        foreach ($xls_data as $return) {
                $col_num = 0;
                $shift = -1;
                foreach($return as $key=>$value) {
                		try{
                			$celIndex = $alphabets[$col_num%26];
                			if($celIndex == 'Z'){
			        			$shift++;
			        		}
        		
			        		if($col_num > 25){
			        			$celIndex = $alphabets[$shift] . $celIndex;
			        		}
			        		
	                        $objPHPExcel->getActiveSheet()->SetCellValue($celIndex.$row_num, "$value");
	                        $col_num++;
                		}catch(Exception $e){
                			$errorlog->error("Row: " . $row_num . " Column:" . $col_num . " Value:" . $value . " Message:" . $e->getMessage());
                		}
                }
                $row_num++;
        }
        
        header("Content-Disposition: attachment; filename=$filename.xls");
        ob_clean();
        flush();

		try{        
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->setPreCalculateFormulas(false);
                $objWriter->save('php://output');
        }catch(Exception $ex) {
               $errorlog->error("Error Occured = ". $ex->getMessage());
               $errorlog->error($ex);
        }
} else {

	$couriers = get_supported_courier_partners(false);

	$statuses 	= _get_old_rto_states();
	$returnStatuses = array();
	foreach($statuses as $key => $value ) {
		
		$returnStatuses[] = array('code'=> $key, 'display_name' => $value);	
	}
	
	$warehouses = WarehouseApiClient::getAllWarehouses();
	$smarty->assign("warehouses",json_encode($warehouses));
	
	$smarty->assign("couriers", json_encode($couriers));
	$smarty->assign("statuses", json_encode($returnStatuses));
	$smarty->assign("main","old_returnedorders_tracking");
	func_display("admin/home.tpl", $smarty);
}
?>
