<?php
require "../auth.php";
include_once("$xcart_dir/include/security.php");
require $xcart_dir."/include/func/func.mk_old_returns_tracking.php";

echo "Access to this page is disabled.<br><br>For any issues related to item status, please send mail to omsoncall@myntra.com";
exit(0);

global $sqllog;

$action = $_REQUEST['action'];
$uploadstatus = false;
if ($action == '') {
	$action = 'upload';
} else if ($action == 'verify') {
	if(!isset($_FILES) && isset($HTTP_POST_FILES))
                $_FILES = $HTTP_POST_FILES;
	if(!empty($_FILES['orderdetail']['name'])) {
		$uploaddir1 = '../../bulkrtoqueuing/';
                $extension = explode(".",basename($_FILES['orderdetail']['name']));
                $uploadfile1 = $uploaddir1 . "UD-" .date('d-m-Y-h-i-s').".".$extension[1];
                if (move_uploaded_file($_FILES['orderdetail']['tmp_name'], $uploadfile1)) {
                         $uploadStatus = true;
                }
		if ($uploadStatus) {
			$fd = fopen ($uploadfile1, "r");
			$orderstoqueue = array();
			$failedtoqueue = array();
			$dataformaterror = "Data Format Should be : {{CellA-Orderid}}";
			$line = 0;
			while(!feof($fd)) {
				$buffer = fgetcsv($fd, 4096);
                                $line = $line + 1;
				if ($line == 1) continue;
                                if (empty($buffer[0])) {
                                        break;
                                }
				
				$fieldCount = count($buffer);
				if($fieldCount <1) {
					$smarty->assign('dataFormatError', "$dataFormatError at line# $line");
                                        break;
				}	
				$orderid 	= $buffer[0];
				$deliverycount 	= $buffer[1];
				$reason = $buffer[2];
		
				$sqllog->debug("IS UD order ?" . isUDOrder($orderid));
	
				if (isUDOrder($orderid)) {
					$failedtoqueue[] = array('orderid' => $orderid, 'reason' => 'Its a UD order already.');
					continue;
				}

				if (isOrderReturned($orderid)) {
					$failedtoqueue[] = array('orderid' => $orderid, 'reason' => 'Its RT/RTO already');
                                        continue;
				}
				
				$udData = getUDOrderData($orderid);
							
				$sqllog->debug("Data => for $orderid");
				$sqllog->debug($udData);
				if(is_null($udData) || empty($udData) || $udData['custname'] == '' ) {
					$failedtoqueue[] = array('orderid' => $orderid, 'reason' => 'Order does not exist');	
				} else {
					$orderstoqueue[] = $orderid;
					$data = array();
					$data['orderid']	= $orderid;
					$data['iscod']		= $udData['iscod'];
					$data['custname'] 	= addslashes($udData['custname']);
        			$data['pickupaddress'] 	= addslashes($udData['pickupaddress']);
        			$data['pickupcity'] 	= addslashes($udData['pickupcity']);
        			$data['pickupstate'] 	= addslashes($udData['pickupstate']);
        			$data['pickupcountry']	= addslashes($udData['pickupcountry']);
        			$data['pickupzipcode'] 	= addslashes($udData['pickupzipcode']);
					$data['phonenumber']	= addslashes($udData['phonenumber']);
					$data['courier']	= addslashes($udData['courier']);
					$data['trackingno']	= addslashes($udData['trackingnum']);
					$data['attemptcount']	= addslashes($deliverycount);
					$data['reason']	 = addslashes($reason);
					createUDOrder($login, $data);
				}		
			}
			fclose ($fd);
			$sqllog->debug($orderstoqueue);
			$smarty->assign('orders', $orderstoqueue);
			$smarty->assign('failedorders', $failedtoqueue);
		}	
	}

}

$smarty->assign("action", $action);
$smarty->assign("main", "upload_uds_to_queue");
func_display("admin/home.tpl",$smarty);

?>
