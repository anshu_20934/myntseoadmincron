Ext.onReady(function() {
	Ext.QuickTips.init();
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
	Ext.Ajax.on('receivereturns', myMask.hide, myMask);
	Ext.Ajax.on('requestexception', myMask.hide, myMask);
	
	var downloadMask;	
	
	var returnsPanel = new Ext.form.FormPanel({
		renderTo : 'contentPanel',
		title : 'Receive Returns',
		id : 'formPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px; background-color: #ccddff;',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 2},
			defaults : {width : 500, border : false, height : 50,
				layout : 'form', bodyStyle : 'padding-left : 15px; background-color: #ccddff;'},
			items : [{
				items : [{
		        	xtype : 'combo',
		        	store : new Ext.data.JsonStore({
		        		fields : ['name'],
		        		data : [{"name":"Warehouse"}, {"name":"Delivery Center"}],
		        		sortInfo : {
		        			field : 'name',
		        			direction : 'ASC'
		        		}
		        	}),
		        	emptyText : 'Returns received at ..',
		        	editable: false,
		        	displayField : 'name',
		        	fieldLabel : 'Received at',
		        	name : 'receivedatid',
		        	mode: 'local',
		        	triggerAction: 'all',
		        	allowBlank : false,
		        	listeners : {
		                'select' : function(combo, record, index) {
		                	var form = returnsPanel.getForm();
		                	var wh = form.findField("warehouseid");
		                	var dc = form.findField("dcid");
		                	if(record.get('name') == 'Delivery Center'){
		                		wh.disable();
		                		wh.reset();
		                		dc.enable();
		                		dc.EmptyText = "here";
		                		dc.setWidth(210);
		                		wh.setWidth(10);
		                	}
		                	else if(record.get('name') == 'Warehouse'){
		                		dc.disable();
		                		dc.reset();
		                		wh.enable();
		                		wh.setWidth(210);
		                		dc.setWidth(10);
		                	}
		                }
		            }
				}]
			}, {
				items : []
			}, {
				items : [{
		        	xtype : 'combo',
		        	store : new Ext.data.JsonStore({
		        		fields : [ 'id', 'name'],
		        		data : warehouses,
		        		sortInfo : {
		        			field : 'name',
		        			direction : 'ASC'
		        		}
		        	}),
		        	emptyText : 'Select Location',
		        	width : 10,
		        	disabled : true,
		        	editable: false,
		        	displayField : 'name',
		        	valueField : 'id',
		        	fieldLabel : 'Ops Location',
		        	name : 'warehouseid',
		        	mode: 'local',
		        	triggerAction: 'all',
		        	allowBlank : false
				}]
			}, {
				items : [{
		        	xtype : 'combo',
		        	store : new Ext.data.JsonStore({
		        		fields : [ 'code', 'name'],
		        		data : deliveryCenters,
		        		sortInfo : {
		        			field : 'name',
		        			direction : 'ASC'
		        		}
		        	}),
		        	emptyText : 'Select Location',
		        	width : 10,
		        	disabled : true,
		        	editable: false,
		        	displayField : 'name',
		        	valueField : 'code',
		        	fieldLabel : 'DC Location',
		        	name : 'dcid',
		        	mode: 'local',
		        	triggerAction: 'all',
		        	allowBlank : false
				}]
			}, {
				items : [{
					xtype : 'numberfield',
		        	id : 'return_id',
		        	fieldLabel : 'Return Id',
		        	emptyText : 'Enter Return Id',
		        	allowBlank : true
				}]
			}, {
				items : [{
					xtype : 'numberfield',
					id : 'store_return_id',
					fieldLabel : 'Flipkart Return Id',
					emptyText : 'Enter Flipkart Return Id',
					allowBlank : true
				}]
			},
				{
				items : [{
		        	xtype : 'combo',
		        	store : new Ext.data.JsonStore({
		        		fields : [ 'code', 'display_name'],
		        		data : couriers,		        		
		        		sortInfo : {
		        			field : 'display_name',
		        			direction : 'ASC'
		        		}
		        	}),
		        	emptyText : 'Select Courier Service',
		        	typeAhead : true,
		        	displayField : 'display_name',
		        	valueField : 'code',
		        	fieldLabel : 'Courier Service',
		        	name : 'courier_service',
		        	mode: 'local',
		        	triggerAction: 'all',
		        	allowBlank : false
				}]
			}, {
				items :[{
					xtype : 'textfield',
		        	id : 'tracking_no',
		        	fieldLabel : 'Tracking No',
		        	width : 200,
		        	allowBlank : false
				}]
			}, {
				items : [{
                    xtype: 'textarea',
                    style : {height: 35, width: 320},
                    fieldLabel : 'Comments',
                    id : 'comments',
                    allowBlank : false
                }]
			}, {
					items : []
				}]
		}],
		buttons : [{
			text : 'Receive Returns',
			handler : function() {
				var form = returnsPanel.getForm();
				if (form.isValid()) {
					receivereturns();
				}
			}
		}]
	});
	
	function getReturnsPanelParams() {
		var form = returnsPanel.getForm();
		var params = {};
		params["warehouseid"] = form.findField("warehouseid").getValue();
		params["dcid"] = form.findField("dcid").getValue();
		params["return_id"] = form.findField("return_id").getValue();
		params["store_return_id"] = form.findField("store_return_id").getValue();
		params["comments"] = form.findField("comments").getValue();
		params["courier_service"] = form.findField("courier_service").getValue();
		params["tracking_no"] = form.findField("tracking_no").getValue();
		return params;
	}
	
	function receivereturns() {
		var params = getReturnsPanelParams();
		params["action"] = "receivereturns";
		downloadMask = new Ext.LoadMask(Ext.get('formPanel'), {msg: "Updating Returns Details"});
		downloadMask.show();
		Ext.Ajax.request({
			url : 'receive_returns.php',
			params : params,
			success : function(response, action) {
				var jsonObj = Ext.util.JSON.decode(response.responseText);
				downloadMask.hide();
				if(jsonObj[0] == "success") {
					Ext.MessageBox.alert('success', jsonObj[1]);
					var form = returnsPanel.getForm();
                	form.findField("return_id").reset();
					form.findField("store_return_id").reset();
                	form.findField("comments").reset();
                	form.findField("tracking_no").reset();
				} else {
					Ext.MessageBox.alert('Error', jsonObj[1]);
				}
			}
		});
	}
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
		     returnsPanel
		]
	});
});
