Ext.onReady(function() {
        Ext.QuickTips.init();
	var overviewForm = new Ext.form.FormPanel({
		renderTo: 'beforedate',
		autoHeight : true,
        collapsible : true,
        border : false,
        bodyStyle : 'padding: 5px',
        width : 600,
        align: 'right',
	    title: 'Overview',
	    url: 'sla_rtos.php',
	    layout: {
	        type: 'hbox',
	        align: 'middle'
    	},
	    items: [{
		        xtype: 'datefield',
		        fieldLabel: 'Before Date',
		        format: 'd/m/Y',
            	editable: false,
            	allowBlank : false,
		        name: 'overview_dt',
		        value: Date.parseDate(overDate,'d/m/Y')
		 },{
			xtype: 'button',
			name: 'overviewrefresh',
			text : 'Refresh',
			handler : function () {
				var form = overviewForm.getForm();
				if(form.isValid()){
					var formDom = form.getEl().dom.submit();
					formDom.submit();
                }				
			}
		},{  
            xtype:'hidden', 
            name:'action',
            value:'overview'
        },{
        	xtype:'hidden',
        	name:'sla_dt1',
        	value:sladt1
        },{
        	xtype:'hidden',
        	name:'sla_dt2',
        	value:sladt2
        }]
	});	
	
	var slaForm = new Ext.form.FormPanel({
		renderTo: 'sladate',
		autoHeight : true,
        collapsible : true,
        border : false,
        bodyStyle : 'padding: 5px',
        width : 600,
	    title: 'SLA Compliance',
	    url: 'sla_rtos.php',
	    layout: {
	        type: 'hbox',
	        align: 'middle'
    	},
    	items: [{
		        xtype: 'label',
		        forId: 'l1',
		        text: 'Start Date',
		         flex:1
		    },{
		        xtype: 'datefield',
		        fieldLabel: 'Start Date',
		        name: 'sla_dt1',
		        format: 'd/m/Y',
		        allowBlank : false,
		        value: Date.parseDate(sladt1,'d/m/Y'),
		        flex:1
		    },{ xtype: 'label',
		        forId: 'l2',
		        text: 'End Date',
		         flex:1
		    },{
		        xtype: 'datefield',
		        fieldLabel: 'End Date',
		        name: 'sla_dt2',
		        format: 'd/m/Y',
		        allowBlank : false,
		        value: Date.parseDate(sladt2,'d/m/Y'),
		        flex:1
		    },{
			xtype: 'button',
			text : 'Refresh',
			name : 'slacomprefresh',
			handler : function () {
				var form = slaForm.getForm();				
				if(form.isValid()){
					var formDom = form.getEl().dom.submit();
					formDom.submit();
                }
			}
		},{  
                xtype:'hidden', 
                name:'action',
                value:'slacomp'
        },{
        	xtype:'hidden',
        	name:'overview_dt',
        	value:overDate
        }]
	});	
});
