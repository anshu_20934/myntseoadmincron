<?php
require "../auth.php";
//include_once("$xcart_dir/include/security.php");
require_once $xcart_dir."/include/func/func.returns.php";
require_once $xcart_dir."/include/func/func.mk_old_returns_tracking.php";
require_once $xcart_dir."/PHPExcel/PHPExcel.php";


x_load('db');

$couriers = get_supported_courier_partners(false);

$format 	= $_GET['format'];
$returnType	= $_GET['returntype'];
$reportType	= $_GET['reporttype'];
$currState	= '';

$qry = "select * from mk_old_returns_tracking where ";

if ($returnType == 'RTO' && $reportType == 'reship') {
	$qry .= " returntype='$returnType' and currstate='RTORS'";
} else if ($returnType == 'RT') {
	if ($reportType == 'reship') {
		$qry .= " returntype='$returnType' and currstate='RTRS'";	
	} else {
		$qry .= " returntype='$returnType' and currstate='RTQ' and shipmenttype='MS'";
	}
}

$weblog->info("Query - " . $qry);

$results = func_query($qry);
$selected_results = array();
$weblog->info("Results - ". count($results));


if(count($results) > 0) {
	foreach($results as $row) {
		$available_couriers = check_pickup_availability($row['pickupzipcode']);
		//$available_couriers = array(array("name" => 'BD')); //check_pickup_availability($row['pickupzipcode']);
                
		$supported = false;
                foreach ($available_couriers as $courier) {
			$weblog->info($courier);
                        if($courier['name'] == $_GET['format']) {
                                $supported = true;
				break;
                        }
                }
                if($supported) {
                        $selected_results[] = $row;
                }	
	}
}

$weblog->info("Filtered Results - ". count($selected_results));
$return_requests = format_return_data($selected_results);

$filename = "";
$file_name_suffix = date("dmy");
foreach($couriers as $courier) {
        if($courier['code'] == $format) {
                $filename =  $courier['code'].$file_name_suffix;
        }
}

$weblog->info("Its fine till here -:)");

$alphabets = range('A','Z');
if($filename != "") {
	$xls_data = get_xls_data_for_returns($return_requests, $format);	

	$weblog->info(" ==> " .$xls_data);
	
	$objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
	$col_headers = array_keys($xls_data[0]);
        foreach($col_headers as $col_num=>$header) {
                $objPHPExcel->getActiveSheet()->SetCellValue($alphabets[$col_num]."1", $header);
        }

	$row_num = 2;
        foreach ($xls_data as $return) {
                $col_num = 0;
                foreach($return as $key=>$value) {
                        $objPHPExcel->getActiveSheet()->SetCellValue($alphabets[$col_num++].$row_num, "$value");
                }
                $row_num++;
        }

	header("Content-Disposition: attachment; filename=$filename.xls");
        ob_clean();
        flush();
        try {
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->setPreCalculateFormulas(false);
                $objWriter->save('php://output');
        }catch(Exception $ex) {
                $weblog->info("Error Occured = ". $ex->getMessage());
        }
        $weblog->info("Saved");	
}

?>
