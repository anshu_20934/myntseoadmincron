<?php
require "../auth.php";
//require $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.returns.php";
require_once $xcart_dir."/include/func/func.mk_old_returns_tracking.php";
require_once $xcart_dir."/PHPExcel/PHPExcel.php";

$action = $_REQUEST['action'];

if ($action == 'search') {
	global $sqllog;
        $formatted_results = searchUDs($_REQUEST,true);
        
	    $sqllog->debug($formatted_results);

        $filename = "UD-report-" . date('Y-m-d-H-i-s');
        $xls_data       = $formatted_results;

        $objPHPExcel = new PHPExcel();
		$objPHPExcel = func_write_data_to_excel($objPHPExcel, $xls_data);
		
        header("Content-Disposition: attachment; filename=$filename.xls");
        ob_clean();
        flush();

	 try {
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->setPreCalculateFormulas(false);
                $objWriter->save('php://output');
        }catch(Exception $ex) {
               $weblog->info("Error Occured = ". $ex->getMessage());
        }		
	
} else {
	$couriers = get_supported_courier_partners(false);

	$statuses = _get_old_ud_states();
	$returnStatuses = array();
	foreach($statuses as $key => $value ) { 
		$returnStatuses[]   = array('code'=> $key, 'display_name' => $value); 
	}

	$smarty->assign("couriers", json_encode($couriers));
	$smarty->assign("statuses", json_encode($returnStatuses));

	$ccresolution = array();
	foreach(_get_old_ud_cc_resolution() as $k=>$v) {
		$ccresolution[] = array('code'=> $k, 'display_name'=>$v);
	}
	$smarty->assign("ccresolution", json_encode($ccresolution));

	$smarty->assign("main","old_undelivered_order_tracking");
	func_display("admin/home.tpl", $smarty);
} 
?>
