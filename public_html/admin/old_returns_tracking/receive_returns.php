<?php
require "../auth.php";
include_once("$xcart_dir/include/func/func.mkcore.php");
include_once("$xcart_dir/include/func/func.returns.php");
include_once("$xcart_dir/include/func/func.courier.php");
include_once("$xcart_dir/include/class/oms/ReturnsStatusProcessor.php");
include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");
include_once("$xcart_dir/modules/apiclient/LocationApiClient.php");
include_once("$xcart_dir/modules/apiclient/WarehouseApiClient.php");

echo "Access to this page for return related changes is disabled.<br><br>For any issues related to item status, please send mail to omsoncall@myntra.com";
exit(0);

$action  = $_POST['action'];

if($action == 'receivereturns'){
	$returnid = $_POST["return_id"];
	$store_return_id = $_POST["store_return_id"];
	if ($store_return_id && !$returnid) {
		$store_return_details = get_store_return_details(2, false, $store_return_id);
		$returnid = $store_return_details['returnid'];
	}
	$courier_service = $_POST["courier_service"];
	if($courier_service == "other") {
		$courier_service = mysql_real_escape_string($_POST["courier_service_custom"]);
	}
	$tracking_no = $_POST["tracking_no"];
	$user_comment = $_POST['comments'];
	$deliveryCenterCode = $_POST['dcid'];
	$warehouseid = $_POST["warehouseid"];
	$status = 'RADC';
	
	$processor = new ReturnsStatusProcessor();
	if($warehouseid == ''){
		$warehouseid = NULL;
		$status = 'RADC';
	}elseif($deliveryCenterCode == ''){
		$deliveryCenterCode = NULL;
		$status = 'RRC';
	}
	$status_change_response = $processor->changeReturnsStatus($returnid, $status, $courier_service, $tracking_no, $itemBarCode, $qapassquality, $login, $user_comment,'', $warehouseid, $deliveryCenterCode);
	if($status_change_response['status']){
		$result = 'success';
		$msg = 'Returns Received Successfully.';
		if($status_change_response['source_warehouseid'] == 7) {
			$msg .= " This item belongs to Virtual warehouse - ($status_change_response[source_wh_name]). Please do a stock transfer to $status_change_response[source_wh_name]";
		}
	}else{
		$result = 'failure';
		$msg = $status_change_response['reason'];
	}
	echo json_encode(array($result, $msg));
	exit;
}

$couriers = get_supported_courier_partners();
$warehouses = WarehouseApiClient::getAllWarehouses();
$smarty->assign("warehouses", json_encode($warehouses, true));
$deliveryCenters = trim(WidgetKeyValuePairs::getWidgetValueForKey('selfship.dccodes'));
$smarty->assign("deliveryCenters", $deliveryCenters);
$smarty->assign("couriers", json_encode($couriers, true));

$smarty->assign("main","receive_returns");
func_display("admin/home.tpl", $smarty);

?>