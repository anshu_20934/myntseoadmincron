<?php

require "../auth.php";
//include_once("$xcart_dir/include/security.php");
require_once "$xcart_dir/include/func/func.mk_old_returns_tracking.php";
include_once("$xcart_dir/include/func/func.db.php");
include_once("$xcart_dir/include/func/func.mkcore.php");
include_once("$xcart_dir/include/func/func.mail.php");
include_once("$xcart_dir/include/class/class.mail.multiprovidermail.php");

//date_default_timezone_set('Asia/Calcutta');
global $weblog;
$cc_email_id = WidgetKeyValuePairs::getWidgetValueForKey('return-sla-cc-emails');
$ops_email_id = WidgetKeyValuePairs::getWidgetValueForKey('return-sla-ops-emails');

$rtRecords = generateRTRecords();
$udRecords = generateUDRecords();


$TYPE_RTO = "RTO";
$TYPE_UD = "UD";
$DEPT_CUSTOMER_CONNECT = "Customer Connect";
$DEPT_OPERATIONS = "Operations";

$content = makeReport($TYPE_RTO, $DEPT_CUSTOMER_CONNECT, $rtRecords);
$content .= makeReport($TYPE_UD, $DEPT_CUSTOMER_CONNECT, $udRecords);
$subject = $TYPE_RTO . " - " . $TYPE_UD . " report to " . $DEPT_CUSTOMER_CONNECT;
sendMail($cc_email_id, $subject, $content);
$todebug = $content;

$content = makeReport($TYPE_RTO, $DEPT_OPERATIONS, $rtRecords);
$content .= makeReport($TYPE_UD, $DEPT_OPERATIONS, $udRecords);
$subject = $TYPE_RTO . " - " . $TYPE_UD . " report to " . $DEPT_OPERATIONS;
sendMail($ops_email_id, $subject, $content);
$todebug .= $content;
echo $todebug;

function generateRTRecords() {
    global $sqllog;
    $qry = "
SELECT X.currstate, total AS total_sla, sla AS maxtime, exceeding_sla AS exceeding_sla_time
FROM (
SELECT currstate, sla, COUNT(*) AS total FROM mk_old_returns_tracking a,
(SELECT mos.prevstate, MAX(mos.maxtime) AS sla
FROM mk_old_returns_tracking_sla mos GROUP BY mos.prevstate) c
WHERE a.currstate = c.prevstate AND
a.returntype = 'RTO'
GROUP BY currstate) X LEFT JOIN
(
SELECT currstate, COUNT(*) exceeding_sla
FROM mk_old_returns_tracking a,
mk_old_returns_tracking_activity b,
(SELECT mos.prevstate, MAX(mos.maxtime) AS sla
FROM mk_old_returns_tracking_sla mos
GROUP BY prevstate)c
WHERE a.id = b.returnid
AND a.currstate = b.activitycode
AND a.returntype = 'RTO'
AND  a.currstate = c.prevstate
AND TIMESTAMPDIFF(HOUR,b.recorddate, NOW()) > c.sla
GROUP BY a.currstate) Y
ON X.currstate = Y.currstate";
    $sqllog->debug($qry);
    $data = func_query($qry);
    return $data;
}

function generateUDRecords() {
    global $sqllog;
    $qry = "SELECT X.currstate, total_sla, sla AS maxtime, exceeding_sla_time
FROM (
SELECT currstate, sla, COUNT(*) AS total_sla FROM mk_old_returns_tracking_ud a,
(SELECT mos.prevstate, MAX(mos.maxtime) AS sla
FROM mk_old_returns_tracking_sla mos GROUP BY mos.prevstate) c
WHERE a.currstate = c.prevstate
GROUP BY currstate) X LEFT JOIN
(
SELECT currstate, COUNT(*) exceeding_sla_time
FROM mk_old_returns_tracking_ud a,
mk_old_returns_tracking_ud_activity b,
(SELECT mos.prevstate, MAX(mos.maxtime) AS sla
FROM mk_old_returns_tracking_sla mos
GROUP BY prevstate)c
WHERE a.id = b.udid
AND a.currstate = b.activitycode
AND  a.currstate = c.prevstate
AND TIMESTAMPDIFF(HOUR,b.recorddate, NOW()) > c.sla
GROUP BY a.currstate) Y
ON X.currstate = Y.currstate";
    $sqllog->debug($qry);
    $data = func_query($qry);
    return $data;
}

function makeReport($type, $dept, $records) {
    $content = "<div align='left'><h3>$type for $dept</h3>";
    $content .= "<table border='1' width='100%'>
    <tr align='left' style='background-color:bisque';>
    <td><h4>State Code</h4></td>
    <td><h4>Current count of order</h4></td>
    <td><h4>SLA</h4></td>
    <td><h4>No. of orders exceeding SLA</h4></td>
    <td><h4>% of orders exceeding SLA</h4></td>
    </tr>";
    $isRecordsPresent = false;
    foreach ($records as $rtRecord) {
        if (isAllowed($dept, $rtRecord[currstate])) {
            $content .= "<tr><td>" . getFullName($rtRecord[currstate]) . "</td>
        <td>$rtRecord[total_sla]</td>
        <td>$rtRecord[maxtime]</td>";
            if ($rtRecord[exceeding_sla_time] != null) {
                $percentageExceedingSla = round(($rtRecord[exceeding_sla_time] * 100) / $rtRecord[total_sla], 2);
                $content .= "<td>$rtRecord[exceeding_sla_time]</td>
                <td>" . $percentageExceedingSla . "</td></tr>";
            } else {
                $content .= "<td>0</td> <td>0</td></tr>";
            }
            $isRecordsPresent = true;
        }
    }

    if (!$isRecordsPresent) {
        //$content .= "<td>No data present</td>";
        return "";
    }
    $content .= "</table> </div>";
    return $content;
}

function getFullName($shortName) {
    $rtoStates = _get_old_rto_states();
    $udState = _get_old_ud_states();
    if (array_key_exists(strtoupper($shortName), $rtoStates)) {
        return $rtoStates[strtoupper($shortName)];
    } else if (array_key_exists(strtoupper($shortName), $udState)) {
        return $udState[strtoupper($shortName)];
    }
    return $shortName;
}

function sendMail($to, $subject, $content) {
    global $weblog;
    $mail_detail = array(
        "to" => $to,
        "subject" => $subject,
        "content" => $content,
        "from_name" => 'Myntra Admin',
        "from_email" => 'support@myntra.com',
        "headers" => "Content-Type: text/html; charset=ISO-8859-1 \n",
    );

    $weblog->info($mail_detail);
    $multiPartymailer = new MultiProviderMailer($mail_detail);
    return $multiPartymailer->sendMail();
}

function isAllowed($dept, $currState) {
    global $DEPT_CUSTOMER_CONNECT;
    global $DEPT_OPERATIONS;
    $ccAllowed = array("RTOQ", "RTOCAL1", "RTOCAL2", "RTOC", "RTOAI", "UDQ", "UDC1", "UDC2", "UDC3");
    $opAllowed = array("RTOC", "RTORF", "RTORS", "UDCC");

    if ($dept == $DEPT_CUSTOMER_CONNECT) {
        return in_array(strtoupper($currState), array_map('strtoupper', $ccAllowed));
    } else if ($dept == $DEPT_OPERATIONS) {
        return in_array(strtoupper($currState), array_map('strtoupper', $opAllowed));
    }
    return false;
}
?>
