<?php
require '../../auth.php';
//include_once("$xcart_dir/include/security.php");
require $xcart_dir . '/include/func/func.mk_old_returns_tracking.php';
require_once $xcart_dir."/PHPExcel/PHPExcel.php";

global $weblog;

if ($_POST['action'] == 'search') {
	$weblog->info('UD Search AJAX called....');
	$return_requests = 	searchUDs($_POST);
	$total = func_query("SELECT FOUND_ROWS() total", TRUE);
	
	$retarr = array("results" => $return_requests, "count" => $total[0]["total"]);
	
	header('Content-type: text/x-json');
	print json_encode($retarr);
}
