Ext.onReady(function() {
        Ext.QuickTips.init();
        var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
        Ext.Ajax.on('beforerequest', myMask.show, myMask);
        Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
        Ext.Ajax.on('requestexception', myMask.hide, myMask);

	var oderdetailpanel = new Ext.form.FormPanel({
		renderTo: 'contentPanel',
		title : 'Order Details',
                id : 'orderDetailsPanel',
                autoHeight : true,
                collapsible : true,
                border : true,
                bodyStyle : 'padding: 5px',
                width : 1000,
		items : [{
			layout : 'table',
                        border : false,
                        layoutConfig : {columns : 2},
                        defaults : {
                                        width : 450,
                                        border : false,
                                        layout : 'form',
                                        bodyStyle : 'padding-left : 15px'
                                },
			items : [{
				items : [{
                                        xtype : 'numberfield',
                                	id : 'orderid',
                               		fieldLabel : 'Order Id',
					allowBlank : false
                                }]				
			}]	
		}],
		buttons : [{
			text : 'Get Order Details',
			handler : function () {
				getOrderDetailsPage();				
			}
		}]
	});	

	var getOrderDetailsPage = function() {
		var form = oderdetailpanel.getForm();
		var orderid = "";
		if (form.isValid()) {
			orderid = form.findField("orderid").getValue();
		}		
		if (orderid == '') {
			alert("Please enter a valid order id");
			return ;
		}
		var redirectUrl = httpLocation + "/admin/old_returns_tracking/old_ud_tracking.php?order_id=" + orderid;
		window.open(redirectUrl);
	}	

	var couriersStore = new Ext.data.JsonStore( {
                fields : [ 'code', 'display_name' ],
                data : couriers,
                sortInfo : {
                        field : 'display_name',
                        direction : 'ASC'
                }
        });

	var couriersField = {
        	xtype : 'combo',
        	emptyText : 'Select Courier Service',
        	store : couriersStore,
        	mode: 'local',
        	displayField : 'display_name',
        	valueField : 'code',
        	triggerAction: 'all',
        	fieldLabel : 'Courier Partners',
        	name : 'format'
        } ;		

	var statusStore = new Ext.data.JsonStore({
                fields : [ 'code', 'display_name' ],
                data : statusArr,
                sortInfo : {
                        field : 'display_name',
                        direction : 'ASC'
                }
        });

    var return_status = new Ext.ux.form.SuperBoxSelect({
                xtype : 'superboxselect',
                fieldLabel : 'UD Status',
                emptyText : 'Select UD Status',
                name : 'status',
                id : 'return_status',
                anchor : '100%',
                store : statusStore,
                mode : 'local',
                displayField : 'display_name',
                valueField : 'code',
                forceSelection : true
        });

	var ccResolutionStore = new Ext.data.JsonStore({
		fields : ['code', 'display_name'],
		data : ccResolutions
	}); 

	var ccResolutionField = {
		xtype	: 'combo',
		emptyText : 'Select CC Resolution',
		store : ccResolutionStore,
		mode : 'local',
		displayField : 'display_name',
		valueField : 'code',
		triggerAction : 'all',
		fieldLabel : 'CC Resolution',
		name : 'ccresolution',
		id : 'ccresolution',
		anchor : '100%'
	}

	var dateNamesStore = new Ext.data.JsonStore({
                fields : [ 'code', 'display_name' ],
                data : statusArr
        });

        var dateNamesField = {
        	xtype : 'combo',
        	emptyText : 'Select Date Type',
        	store : dateNamesStore,
        	mode: 'local',
        	displayField : 'display_name',
        	valueField : 'code',
        	triggerAction: 'all',
        	fieldLabel : 'Date When',
        	name : 'datetype',
        };

	var searchpanel = new Ext.form.FormPanel({
		renderTo : 'contentPanel',
                title : 'Search Panel',
                id : 'searchPanel',
                autoHeight : true,
                collapsible : true,
                border : true,
                bodyStyle : 'padding: 5px',
                width : 1000,
		items : [{
			layout : 'table',
                        border : false,
                        layoutConfig : {columns : 2},
                        defaults : {
					width : 450, 
					border : false,
                                	layout : 'form', 
					bodyStyle : 'padding-left : 15px'
				},
			items : [{
				items : [{
                                        xtype : 'numberfield',
                                	id : 'order_id',
                                	fieldLabel : 'Order Id'
                                }]
			},{
				items : [return_status]
			},{
                items: [{xtype : 'numberfield',
            	id : 'attempt',
            	fieldLabel : 'Attempts'}]            		
            },{
				items : [ccResolutionField]
			},{
				items : [dateNamesField]
			},{
			},{
				items : [{
                                        xtype : 'datefield',
                        		name : 'from_date',
                        		width : 150,
                        		fieldLabel : "From Date"
                                }]
			},{
				items : [{
                                        xtype : 'datefield',
                                        name : 'to_date',
                                        width : 150,
                                        fieldLabel : "To Date"
                                }]	
			}]
		}],
		buttons : [{
			text : "Search",
			handler : function() {
				reloadGrid();	
			}	
		},{
			text: "Export",
			handler : function() {
				var params = getSearchPanelParams();
				var qrystr = "action=search&orderid=" + params.orderid + "&returnstatus=" + params.returnstatus;
				qrystr = qrystr + "&datetype=" + params.datetype;
				
				if(params.fromdate != null && params.fromdate != ""){
                	qrystr = qrystr +"&fromdate=" + params.fromdate.format("Y-m-d H:i:s");
                }
                
                if(params.todate != null && params.todate != ""){
                	qrystr = qrystr +"&todate=" + params.todate.format("Y-m-d H:i:s");
                }
                
				qrystr = qrystr + "&ccresolution=" + params.ccresolution;
				qrystr = qrystr + "&attempt=" + params.attempt;
				qrystr = qrystr + "&paginate=false";				
				var url = httpLocation+ "/admin/old_returns_tracking/old_undelivered_order_tracking.php?"+qrystr;
                                window.open(url);	
			}
		}]		
	});

	function getSearchPanelParams(){
		var form = searchpanel.getForm();
                if (form.isValid()) {
                        var params = {};
			params['orderid'] 	= form.findField('order_id').getValue();
			params['returnstatus']	= form.findField('status').getValue();
			params['datetype']	= form.findField('datetype').getValue();
			params['fromdate']	= form.findField('from_date').getValue();
			params['todate']	= form.findField('to_date').getValue();
			params['ccresolution']	= form.findField('ccresolution').getValue();
			params['attempt'] = form.findField('attempt').getValue();
			return params;	
		}
		return false;
	}

	function reloadGrid() {

                var params = getSearchPanelParams();
                if (params == false) {
                        Ext.MessageBox.alert('Error', 'Errors in the search form');
                        return false;
                }
                params["action"] = "search";
                params["returntype"] = "RTO";
                params["paginate"]="true";
                searchResultsStore.baseParams = params;
                searchResultsStore.load();
        }

	var sm = new Ext.grid.CheckboxSelectionModel();
	
	var orderIdLinkRender = function(value, p, record) {
                return '<a href="' + httpLocation + '/admin/old_returns_tracking/old_ud_tracking.php?order_id=' + record.data["orderid"] + '" target="_blank">' + record.data["orderid"] + '</a>';
        }

	var statusRenderer = function(value, p, record) {
                var status = record.data['status'];
                for (var i=0; i< statusArr.length; i++) {
                        if (statusArr[i].code == status) {
                                return statusArr[i].display_name;
                        }
                }
                return status;
        }

        var courierRenderer = function(value, p, record) {
                var courier_code = record.data['courier_service'];
                for (var i=0; i< couriers.length; i++) {
                        if (couriers[i].code == courier_code) {
                                return couriers[i].display_name;
                        }
                }
                return courier_code;
        }
	
	var colModel = new Ext.grid.ColumnModel({
                defaults: {
                sortable: false
            },
            columns: [
                sm,
                {id : 'queued_date', header : "Queued Date", sortable: true, width: 125, dataIndex : 'queued_date'},
                {id : 'orderid', header : "Order ID", sortable: true, width: 70, dataIndex : 'orderid', renderer : orderIdLinkRender},
                {id : 'paymentMethod', header: 'Payment Method', sortable: false, width: 75, dataIndex : 'paymentMethod'},
                {id : 'reason', header: 'Return Reason', sortable: false, width: 200, dataIndex : 'reason'},
                {id : 'comment', header: 'Comments', sortable: false, width: 200, dataIndex : 'remark'},
                {id : 'status', header: 'Curr State', sortable: true, width: 125, dataIndex : 'currstate'} ,
                {id : 'custname', header: 'Customer Name', sortable: false, width: 125, dataIndex : 'custname'},
                {id : 'shippeddate', header : "Shipped Date", sortable: true, width: 125, dataIndex : 'shippeddate'},
                {id : 'age', header : "Age in hrs", sortable: true, width: 50, dataIndex : 'age'},
				{id : 'courier', header: 'Courier Service', sortable: false, width: 100, dataIndex : 'courier'},
				{id : 'trackingno', header: 'Tracking#', sortable: false, width: 100, dataIndex : 'trackingno'},
				{id : 'resolution', header: 'Resolution', sortable: false, width: 100, dataIndex : 'resolution'},
				{id : 'activityDate', header: 'Activity Date', sortable: false, width: 100, dataIndex : 'recorddate'},
				{id : 'attempt', header: 'Delivery Attempt#', sortable: false, width: 75, dataIndex : 'attemptcount'},
				{id : 'custaddress', header: 'Customer Address', sortable: false, width: 350, dataIndex : 'address'},
				{id : 'returnid', header : "UD Id", sortable: true, width: 70, dataIndex : 'id'}
            ]
        });

	var searchResultsStore = new Ext.data.Store({
                url: 'udajax.php',
                pruneModifiedRecords : true,
                reader: new Ext.data.JsonReader({
                        root: 'results',
                        totalProperty: 'count',
                        fields : ['shippeddate','id', 'orderid', 'currstate', 'custname', 'address', 'paymentMethod', 'courier', 'trackingno', 'reason', 'resolution', 'recorddate', 'attemptcount','remark','queued_date','age']
                }),
                sortInfo:{field : 'orderid', direction:'DESC'},
                remoteSort: true
        });

	var pagingBar = new Ext.PagingToolbar({
            pageSize : 30,
            store: searchResultsStore,
            displayInfo: true
        });

	var searchResults = new Ext.grid.EditorGridPanel({
            store  : searchResultsStore,
            cm : colModel,
            id : 'searchResults-panel',
            title : 'Search Results',
            frame : true,
                loadMask : false,
                autoHeight : true,
                sm: sm,
                bbar : pagingBar,
                stripeRows : true,
                autoScroll : true,
                viewConfig : {
                        enableRowBody : true
                },
                tbar:[]
	});	

	var mainPanel = new Ext.Panel({
                renderTo : 'contentPanel',
                border : false,
                items : [
                	orderDetailsPanel, searchPanel, searchResults
                ],
                listeners : {}
	});			
});
