<?php
require "../auth.php";
//require $xcart_dir."/include/security.php";
require $xcart_dir."/include/func/func.returns.php";
require $xcart_dir."/include/func/func.mk_old_returns_tracking.php";
require_once $xcart_dir."/PHPExcel/PHPExcel.php";

$action = $_REQUEST['action'];

if ($action == 'search') {
	$formatted_results = searchRTOs($_REQUEST);
	$filename = "Returns-report-" . date('Y-m-d-H-i-s');
	$alphabets = range('A','Z');
	$xls_data	= $formatted_results;
	
	$objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
	$col_headers = array_keys($xls_data[0]);
	foreach($col_headers as $col_num=>$header) {
                $objPHPExcel->getActiveSheet()->SetCellValue($alphabets[$col_num]."1", $header);
        }

	$row_num = 2;
        foreach ($xls_data as $return) {
                $col_num = 0;
                foreach($return as $key=>$value) {
                        $objPHPExcel->getActiveSheet()->SetCellValue($alphabets[$col_num++].$row_num, "$value");
                }
                $row_num++;
        }
	header("Content-Disposition: attachment; filename=$filename.xls");
        ob_clean();
        flush();
	
	try {
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->setPreCalculateFormulas(false);
                $objWriter->save('php://output');
        }catch(Exception $ex) {
               ; //$weblog->info("Error Occured = ". $ex->getMessage());
        }
				
} else {
	$couriers = get_supported_courier_partners(false);

	$statuses = _get_old_return_states();
	//$state_counts   = get_old_rt_state_counts('RT');
	$returnStatuses = array();
	$dtStatuses 	= array();
	foreach($statuses as $key => $value ) {
		$d = array();
		$d['code'] = $key;
		if (array_key_exists($key, $state_counts)) {
                	$d['display_name'] = "$value - ( " . $state_counts[$key] . " )";
        	} else {
                	$d['display_name'] = "$value";
      	  	}
		$returnStatuses[] = $d;
		$dtStatuses[]	= array('code'=> $key, 'display_name' => $value);	
	}

	$smarty->assign("couriers", json_encode($couriers));
	$smarty->assign("statuses", json_encode($returnStatuses));
	$smarty->assign("alldates", json_encode($dtStatuses)); //json_encode($returnStatuses));
	$smarty->assign("main","old_returns_tracking");
	func_display("admin/home.tpl", $smarty);
}
?>
