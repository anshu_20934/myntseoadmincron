<?php
require "../auth.php";
require $xcart_dir."/include/security.php";
require $xcart_dir."/include/func/func.returns.php";
require $xcart_dir."/include/func/func.mk_old_returns_tracking.php";


$smarty->assign('returntype', 'Returned To Origin (RTO)');

$overview_dt = null;
$dt1 = null;
$dt2 = null;		

function getdt($dt){
	$dtcomponents = explode('/', $dt);
        $ts = $dtcomponents[2] . "-" . $dtcomponents[1] . "-" . $dtcomponents[0] . " 00:00:00";	
	return $ts;
}

if(!empty($_REQUEST['overview_dt'])) {
	$overview_dt = getdt($_REQUEST['overview_dt']);
	$smarty->assign('dt', $_REQUEST['overview_dt']);
}

$slaoverview = get_sla_overall_stats_for_rto($overview_dt);
$smarty->assign("overview", $slaoverview);

if(!empty($_REQUEST['sla_dt1'])){
	$dt1 = getdt($_REQUEST['sla_dt1']);
	$smarty->assign('dt1', $_REQUEST['sla_dt1']);
}

if(!empty($_REQUEST['sla_dt2'])) {
	$dt2 = getdt($_REQUEST['sla_dt2']);
	$smarty->assign('dt2', $_REQUEST['sla_dt2']);
}

$slastats = getSLAComplianceStats('RTO', $dt1, $dt2);
$compl_descr=array();
$compl_sla = array();
$compl_tat = array();
$compl_compl=array();
$compl_count=array();
foreach($slastats as $k=>$v) {
	$compl_descr[$k] = $v['descr'];
	$compl_sla[$k]	 = $v['sla'];
	$compl_tat[$k]	 = $v['tat'];
	$compl_compl[$k] = $v['compliance'];
	$compl_count[$k] = $v['currorders'];	
}
$smarty->assign('statedescr', $compl_descr);
$smarty->assign('complsla', $compl_sla);
$smarty->assign('compltat', $compl_tat);
$smarty->Assign('complcompl', $compl_compl);
$smarty->Assign('complcount', $compl_count);

$stateorder = array('RTOQ','RTOCAL1','RTOCAL2','RTOCAL3','RTOC', 'RTOAI', 'RTORSC', 'RTORF');

$smarty->Assign('stateorder', $stateorder);
$smarty->assign("main", "old_rto_rt_sla");
func_display("admin/home.tpl", $smarty);

?>
