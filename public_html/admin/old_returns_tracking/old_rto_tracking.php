<?php

require "../auth.php";
require $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.returns.php";
require_once $xcart_dir."/include/func/func.mk_old_returns_tracking.php";
include_once("$xcart_dir/modules/apiclient/WarehouseApiClient.php");

$mode = $_REQUEST['action'];
$orderid = $_REQUEST['order_id'];
global $weblog;
$userroleentry = $XCART_SESSION_VARS['user_roles'];
$userroles = array();
foreach($userroleentry as $entry) {
    $userroles[] = $entry['role'];
}

if(empty($userroles)){
	$weblog->debug("Setting default admin role");
	$userroles = 'AD';
}	

$weblog->debug($userroles);

$rtocancelreasons = get_old_rto_cancel_reasons();
$smarty->assign("rtocancelreasons", $rtocancelreasons);

$couriers = get_supported_courier_partners(false);
$smarty->assign("couriers", $couriers);

$allstates = _get_old_rto_states();

$timeIntervals = array();
for($i=15; $i<24*60; ) {
	$m = $i % 60;
	$h = (int)($i-$m) / 60;
	$hr = $h;
	if ($h < 10) {
		$hr = "0$h";
	}
	$min = $m;
	if ($m == 0){
		$min = '00'; 
	}
	$ts = "$hr:$min";				 
	$timeIntervals[$ts] = $i;			
	$i += 15;
} 
$smarty->Assign("timeintervals", $timeIntervals);

function getTS($dt, $tm){
	$dtcomponents = explode('/', $dt);
	$ts = $dtcomponents[2] . "-" . $dtcomponents[0] . "-" . $dtcomponents[1] . " ";
	$ts .= 	floor($tm/60). ":" . ($tm%60) . ":00" ;
	return $ts;			
}
$ts = getTS($_REQUEST['activity_dt'], $_REQUEST['activity_time']);

if ($mode == 'update') {
	$prev_state = $_REQUEST['currst'];
	$next_state = $_REQUEST['rto_action'];
	$action	    = $allstates[$next_state];  

	$data = array();
	if ($next_state && !empty($next_state)) {
                $data['currstate']      = $next_state;
                $data['activity']       = $action;
                $data['activitycode']   = $next_state;
    }

	if ($_REQUEST['rto_cancel_reason'] && !empty($_REQUEST['rto_cancel_reason'])) {
		if (array_key_exists($_REQUEST['rto_cancel_reason'], $rtocancelreasons)) {
			$data['rtocancelreason'] = $rtocancelreasons[$_REQUEST['rto_cancel_reason']];	
		}
	}
		
	if ($_REQUEST['courier'] && !empty($_REQUEST['courier'])) {
                $data['courier']        = addslashes($_REQUEST['courier']);
    }

		if ($_REQUEST['tracking_num'] && !empty($_REQUEST['tracking_num'])) {
                $data['trackingno']     = addslashes($_REQUEST['tracking_num']);
        }
		
		if($_REQUEST['items_rej_val'] && !empty($_REQUEST['items_rej_val'])) {
                $data['item_val_rejected'] = $_REQUEST['items_rej_val'];
        }

        if($_REQUEST['items_rstk_val'] && !empty($_REQUEST['items_rstk_val'])) {
                $data['item_val_restocked'] = $_REQUEST['items_rstk_val'];
        }

        if ($_REQUEST['coupon_code'] && !empty($_REQUEST['coupon_code'])) {
                $data['couponcode'] = addslashes($_REQUEST['coupon_code']);
        }

        if ($_REQUEST['coupon_value'] && !empty($_REQUEST['coupon_value'])) {
                $data['couponvalue'] = addslashes($_REQUEST['coupon_value']);
        }

        $data['adminuser'] = $login;

	$comment = '-';
	if(!is_null($_REQUEST['comment']) && !empty($_REQUEST['comment'])) {
		$comment = addslashes($_REQUEST['comment']);
	}
	
	$cc_disposition = null;
	
	$cc_resolution = null;
	
	if( $next_state == 'RTOCAL1' || $next_state == 'RTOCAL2' || $next_state == 'RTOCAL3'){
		
		if(!is_null($_REQUEST['ccdisposition']) && !empty($_REQUEST['ccdisposition'])) {
			$cc_disposition = addslashes($_REQUEST['ccdisposition']);	
		}	
		
		
		if (!is_null($_REQUEST['resolution']) && !empty($_REQUEST['resolution'])) {
			$cc_resolution = addslashes($_REQUEST['resolution']);
		}
		
		if (!is_null($cc_disposition)) {
			$ccdispositions = _get_old_ud_cc_dispositions();
			$comment_1 = "CC Disposition : " . $ccdispositions[$cc_disposition];	
			if(!is_null($cc_resolution)) {
                    $ccresolutions = _get_old_ud_cc_resolution();
                    $comment_1 .= "<br/>CC Resolution : ". $ccresolutions[$cc_resolution] ;
	        	}
			$comment = $comment_1 . "<br/><br/>" . $comment ;
		}
	}
	
    $data['remark'] = $comment;    
	updateReturnsInfo('RTO', $orderid, $prev_state, $next_state, $data, $cc_disposition, $cc_resolution);
	
	/**
	 * Based on customers discretion auto advance the activity
	 */
	if(!is_null($cc_resolution) && $cc_resolution == 'RA'){
		$data = array();
        $data['adminuser']      = $login;
        $data['rtocancelreason']= 'Order reshipping on customer request';				
		$data['activitycode']   = 'RTORS';
        $data['activity']       = 'RTO to be Reshipped';
        $data['remark']         = 'Auto advancing (Customer request) '. $next_state. '->RTORS.';
    	$data['currstate']      = 'RTORS';
    	updateReturnsInfo('RTO', $order_id, $next_state, 'RTORS', $data);
    }else if(!is_null($cc_resolution) && $cc_resolution == 'RJ'){
    	$data = array();
        $data['adminuser']      = $login;
        $data['rtocancelreason']= 'Order cancellation on customer request';	
    	$data['activitycode']   = 'RTOC';
        $data['activity']       = 'RTO Cancelled';
        $data['remark']         = 'Auto advancing (Customer request) '.$next_state.'->RTOC.';
    	$data['currstate']      = 'RTOC';
    	updateReturnsInfo('RTO', $order_id, $next_state, 'RTOC', $data, $cc_disposition, $cc_resolution);
    } else if($next_state == 'RTOCAL3' && !is_null($cc_disposition) && $cc_disposition != 'CM'){
    	$data = array();
        $data['adminuser']      = $login;
        $data['rtocancelreason']= 'Unable to contact customer';	
    	$data['activitycode']   = 'RTOC';
        $data['activity']       = 'RTO Cancelled';
        $data['remark']         = 'Auto advancing (Customer not reachable) '.$next_state. '->RTOC.';
    	$data['currstate']      = 'RTOC';
    	updateReturnsInfo('RTO', $order_id, $next_state, 'RTOC', $data);
    }
} elseif($mode == 'add') {
	if (isOrderReturned($orderid)) {
		$smarty->assign("errormsg", "Order $orderid is Returned already!!!");	
	} else {
		$iscod    = intval($_REQUEST['iscod']);
		$custname = $_REQUEST['cust_name'];
		$custaddr = addslashes($_REQUEST['cust_addr']);
		$custcity = addslashes($_REQUEST['cust_city']);
		$custstate= addslashes($_REQUEST['cust_state']);
		$custcountry = addslashes($_REQUEST['cust_country']);
		$custzipcode = addslashes($_REQUEST['cust_zipcode']);
		$comment = addslashes($_REQUEST['comment']);
		$courier = $_REQUEST['courier'];
		$trackingno = $_REQUEST['tracking_num'];
		$phonenumber    = $_REQUEST['contact_no'];
		$reason = $_REQUEST['returnreason'];
		$warehouseid = $_REQUEST['warehouseid'];
		$comment = "RTO received at warehouse.<br />Courier : " . $courier . " - Tracking # : " . $trackingno . "<br /><br />$comment";
		createRTOOrder($orderid, $iscod, $login,$custname,$custaddr,$custcity,$custstate,$custcountry,$custzipcode,$comment,$courier,$trackingno, $phonenumber, $reason);
	}
}

$rtoActions = get_old_rto_next_states($orderid, $userroles);
$smarty->assign("rtoactions", $rtoActions);
$smarty->assign("allstates", _get_old_rto_states());


$orderData 	= getOrderReturnData($orderid, 'RTO');
$editHistory	= array();
if(!array_key_exists('currstate', $orderData)){
	$orderData['cancreate'] = true;	
}else{
	$orderData['canupdate'] = true;		
	$editHistory = getOrderReturnActions($orderid);
}

$warehouses = WarehouseApiClient::getAllWarehouses();

$smarty->assign('warehouses', $warehouses);
$smarty->assign('ccdispositions', _get_old_rto_cc_dispositions());
$smarty->assign('ccresolution', _get_old_rto_cc_resolution());

$smarty->assign("orderData", $orderData);
$smarty->assign("edithistory", $editHistory);
$smarty->assign("main","old_rto_tracking");

func_display("admin/home.tpl", $smarty);
?>
