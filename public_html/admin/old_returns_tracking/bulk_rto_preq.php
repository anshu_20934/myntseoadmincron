<?php
require "../auth.php";
include_once("$xcart_dir/include/security.php");
require $xcart_dir."/include/func/func.mk_old_returns_tracking.php";

$action = $_REQUEST['action'];
$uploadstatus = false;
if ($action == '') {
	$action = 'upload';
} else if ($action == 'verify') {
	if(!isset($_FILES) && isset($HTTP_POST_FILES))
                $_FILES = $HTTP_POST_FILES;
	if(!empty($_FILES['orderdetail']['name'])) {
		$uploaddir1 = '../../bulkrtoqueuing/';
                $extension = explode(".",basename($_FILES['orderdetail']['name']));
                $uploadfile1 = $uploaddir1 . "RTOPREQ-" .date('d-m-Y-h-i-s').".".$extension[1];
                if (move_uploaded_file($_FILES['orderdetail']['tmp_name'], $uploadfile1)) {
                         $uploadStatus = true;
                }
		if ($uploadStatus) {
			$fd = fopen ($uploadfile1, "r");
			$ordersupdated = array();
			$invalidOrders = array();
			$dataformaterror = "Data Format Should be : {{CellA-Orderid}}";
			$line = 0;
			while(!feof($fd)) {
				$buffer = fgetcsv($fd, 4096);
                $line = $line + 1;
				if ($line == 1) 
					continue;
                if (empty($buffer[0])) {
                    break;
                }
				
				$fieldCount = count($buffer);
				if($fieldCount <1) {
					$smarty->assign('dataFormatError', "$dataFormatError at line# $line");
                    	break;
				}	
				
				$orderid 	= $buffer[0];
				$date		= $buffer[1];
				$reason 	= $buffer[2];
				
				if($date == ''){
					$date = date('Y-m-d H:i:s');
				}
				
				if(is_null($reason)){
					$reason = "";
				}else{
					$reason = addslashes($reason);
				}
				$rtoData = getRTODetailsForOrderId($orderid);
				if($rtoData) {
					$invalidOrders[] = array('orderid' => $orderid, 'reason' => 'RTO is already filed for this order');
				}
				else{
					$orderData = func_order_data($orderid);
					$iscod = $orderData['order']['payment_method'] == 'cod'?1:0;
					if ($orderData && array_key_exists("userinfo", $orderData)) {
						$custaddr= $orderData['userinfo']['s_address'];
						$custcity	= $orderData['userinfo']['s_city'];
						$custstate	= $orderData['userinfo']['s_state'];
						$custcountry= $orderData['userinfo']['s_country'];
						$custzipcode= $orderData['userinfo']['s_zipcode'];
						$custname = $orderData['userinfo']['s_firstname'];
						$phonenumber = $orderData['userinfo']['mobile'];
						$courier    = $orderData['order']['courier_service'];
						if(empty($courier)) {
							$courier = 'N.A.';
						}
						$trackingno  = $orderData['order']['tracking'];
						if(empty($trackingno)) {
							$trackingno = '0';
						}
						$comment = 'RTO PreQueued from bulk uploader';
						try{
							createRTOOrder($orderid, $iscod, $login, $custname,$custaddr,$custcity,
									$custstate,$custcountry,$custzipcode,$comment,$courier,$trackingno, $phonenumber, $reason, false,true,false, $date);
							$ordersupdated[] = $orderid;
						}catch(Exception $e){
							$sqllog->debug($e->getMessage());
							$invalidOrders[] = array('orderid' => $orderid, 'reason' => 'System error happened.');
							$ordersupdated[] = $orderid;
						}
					}
					else{
						$invalidOrders[] = array('orderid' => $orderid, 'reason' => 'No Order data');
					}
				}
			}
			fclose ($fd);
			$weblog->info($ordersupdated);
			$smarty->assign('orders', $ordersupdated);
			$smarty->assign('failedorders', $invalidOrders);
		}	
	}
} else if ($action == 'confirm') {
	foreach ($orderids as $key=> $value){
				
	}	
}

$smarty->assign("action", $action);
$smarty->assign("main", "upload_rtos_preq");
func_display("admin/home.tpl",$smarty);

?>
