<?php
/*
 * Created on Jan 3, 2012
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 	require "../auth.php";
	include_once("$xcart_dir/include/security.php");
	include_once("$xcart_dir/include/func/func.order.php");
	include_once("$xcart_dir/include/func/func.mk_old_returns_tracking.php");
	include_once("$xcart_dir/include/func/func.returns.php");
	include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");
	include_once("$xcart_dir/modules/apiclient/LocationApiClient.php");
	include_once("$xcart_dir/modules/apiclient/WarehouseApiClient.php");
	
	$action = $_REQUEST['action'];
 	if($action == 'order_check') {
 		$colorcode = "";
 		$orderid = trim($_POST['orderid']);
 		$errorMsg = "";
	 	if(!isInteger($orderid)){
			$errorMsg .= "Order $orderid is not valid<br>";	
			$colorcode =  "#FF0000";
		}else{
			$order_sql = "Select qtyInOrder, payment_method, status, shippeddate, warehouseid from xcart_orders where orderid = $orderid";
			$order = func_query_first($order_sql,true);
			if($order) {
				if($order['status'] != 'SH' && $order['status'] != 'DL' && $order['status'] != 'C' && $order['status'] != 'L' && $order['status'] != 'F') {
				    $errorMsg .= "Order $orderid is not in Shipped/Delivered/Complete/Lost state/Cancelled<br>";	
				    $colorcode = "#FF0000";
				}
				/*if(intval($order['warehouseid']) != intval($_POST['warehouseid'])) {
					$errorMsg .= "Order $orderid was Shipped from different warehouse.<br>";
					$colorcode = "#FF0000";
				}*/
			} else {
				$errorMsg .= "Order $orderid is not valid<br>";	
				$colorcode =  "#FF0000";
			}
	 		
	 		$qry = "SELECT orderid, currstate FROM mk_old_returns_tracking WHERE orderid = $orderid";
	 		$rto_record = func_query_first($qry,true);
	 		if ($rto_record && $rto_record['currstate'] != 'RTOPREQ') {
			    $errorMsg .= "Order $orderid is RTO'd already<br>";	
			    $colorcode =  "#A82828";
			}
			
			$return_sql = "select returnid from xcart_returns where orderid = $orderid and status != 'RRD'";
			$returns = func_query($return_sql,true);
			if($returns){
				foreach ($returns as $return){
					$returnsString .= $returnsString==""?"":",";	
					$returnsString .= $return['returnid'];
				}
				$errorMsg .= "Order $orderid has returns filed with return ids -- $returnsString<br>";
				$colorcode =  "#C11B17";
			}	
		}
 			
		if($errorMsg == "") {
			$ud_tracked_sql = "select currstate, resolution from mk_old_returns_tracking_ud where orderid = $orderid";
			$ud_tracked = func_query_first($ud_tracked_sql,true);
			
			$colorcode =  $order['payment_method']=='cod'?"#22B804":"#4009E6";
			if($ud_tracked){
				if($ud_tracked['currstate'] != 'UDCC' && $ud_tracked['currstate'] != 'END'){
					$udErrorMsg = "UDTRACKER Warning: Order $orderid is not properly closed in ud tracker<br>";
				} else {
					if($ud_tracked['currstate'] == 'UDCC' && ($ud_tracked['resolution'] == 'RJ' || empty($ud_tracked['resolution']))) {
						$udErrorMsg = "UDTracker Warning: Order $orderid marked rejected in UD tracker<br>";
						$colorcode =  $order['payment_method']=='cod'?"##F87217":"#46C7C7";
					}
					if($ud_tracked['resolution'] == 'RA') {
						$udErrorMsg = "UDTracker Warning: Order $orderid is marked for Re-Attempt in UD Tracker<br>";
						$colorcode =  $order['payment_method']=='cod'?"#F87217":"#C35617";
					}
				}
			}else{
				$udErrorMsg = "UDTRACKER Warning: Order $orderid is not tracked in ud tracker<br>";
			}

			$order_details_sql = "select seller_id from xcart_order_details where orderid = $orderid";
			$order_details = func_query_first($order_details_sql,true);

			if ($order_details['seller_id'] != null && $order_details['seller_id'] != 1) {
				$valid_items = true;
			} else {
				$valid_items = ItemApiClient::validateOrderItemsStatus(trim($orderid), $order['warehouseid'], array('SHIPPED', 'ACCEPTED_RETURNS', 'CUSTOMER_RETURNED'));
			}
			if($valid_items) {
				$shippeddate = $order['shippeddate']?date('F jS Y', $order['shippeddate']):"";
				$retarr = array('status'=>'Success', 'orderid'=>$orderid, 'type'=>$order['payment_method']=='cod'?'COD':'NON-COD',
							'shippingdate'=>$shippeddate, 'remarks'=>'Successfully marked received at warehouse. <br>'.$udErrorMsg, 'color'=>$colorcode);
			} else {
				$colorcode =  "#800000";
				$retarr = array('status'=>'Failure', 'orderid'=>$orderid, 'type'=>'-',
								'shippingdate'=>'-', 'remarks'=>'Order doesnot have any Shipped items.', 'color'=>$colorcode);
			}
		} else {
			$retarr = array('status'=>'Failure', 'orderid'=>$orderid, 'type'=>'-',
								'shippingdate'=>'-', 'remarks'=>$errorMsg, 'color'=>$colorcode);
		}
		header('Content-type: text/x-json');
		print json_encode($retarr);
		exit(0);
 	} if($action == 'item_check') {
 		$warehouseid = $_POST['warehouseid'];
 		$itembarcode = trim($_POST['itembarcode']);
 		if(!isInteger($itembarcode)){
 			$retarr = array('status'=>'Failure', 'itembarcode'=>$itembarcode, 'remarks'=>'Enter a valid item barcode');
 		}else{
	 		$response = get_return_details_for_item($warehouseid, $itembarcode);
			if($response['success']) {
				$return_details = $response['return_details'];
				/*if($return_details['warehouseid'] != $_POST['warehouseid']) {
					$retarr = array('status'=>'Failure', 'itembarcode'=>$itembarcode, 'remarks'=>'Item was shipped from a different warehouse');
				} else {*/
					$response = LocationApiClient::acceptItemsAtWarehouse($itembarcode, $warehouseid, $login);
					if($response == "") {
						if($return_details) {
							$returnMsg = func_receive_return_item($return_details, $itembarcode, $login, "Item $itembarcode Received at warehouse. Updated via Accept returns Bulk uploader.", "Item Received", $warehouseid);
							$retarr = array('status'=>'Success', 'itembarcode'=>$itembarcode, 'remarks'=>"Successfully marked $itembarcode received at warehouse.<br>".$returnMsg);
						} else {
							$retarr = array('status'=>'Success', 'itembarcode'=>$itembarcode, 'remarks'=>'Successfully marked received at warehouse<br>Warning: No Matching return was found for this item');
						}
					} else {
						$retarr = array('status'=>'Failure', 'itembarcode'=>$itembarcode, 'remarks'=>$response);
					}
				//}
			} else {
				$retarr = array('status'=>'Failure', 'itembarcode'=>$itembarcode, 'remarks'=>$response['errormsg']);
			}	
 		}
		
		header('Content-type: text/x-json');
		print json_encode($retarr);
		exit(0);
 	} else if($action == 'queue_rto_orders') {
 		$errorMsg = "";
 		$orderids = $_POST['orderids'];
 		$warehouseid = $_POST['warehouseid'];
 		$orderids = explode(",", $orderids);
 		$successrtoqueuedorders = array('cod'=>array(), 'on'=>array());
 		$failedrtoqueuedorders = array('cod'=>array(), 'on'=>array());
 		foreach($orderids as $orderid) {
 			$rtoid = func_query_first("select id from mk_old_returns_tracking where orderid = $orderid");
 			$orderData = func_order_data($orderid);
 			$iscod = $orderData['order']['payment_method'] == 'cod'?1:0;
 			$valid_items = ItemApiClient::get_skuitems_for_order(trim($orderid), $warehouseid);
			if($valid_items) {
				$shipped_item_barcodes = array();
				foreach($valid_items as $item){
					if($item['itemStatus'] == 'SHIPPED')
						$shipped_item_barcodes[] = $item['barcode'];
				}
				
 				$response = LocationApiClient::acceptItemsAtWarehouse($shipped_item_barcodes, $warehouseid, $login);
			}
 			if($rtoid['id']){
 				try {
	 				processPrequeuedRTO($orderid, $iscod, $_POST['login'],$warehouseid);
                                        // Mark items as customer returned
                                        $orderid2WarehouseId = array();
                                        $product_details = func_create_array_products_of_order($orderid);
                                        foreach($product_details as $product) {
                                            $skuIds[] = $product['sku_id'];
                                            $orderid2WarehouseId[$product['orderid']] = $product['warehouseid'];
                                        }
                                        $item_details_query = "select od.* from xcart_order_details od where od.orderid = $orderid";
                                        $item_details = func_query_first($item_details_query, true);
                                        $rejectReason = false; $rejectReasonDesc = false; $quality = false;
                                        if ($item_details['supply_type'] === 'JUST_IN_TIME') {
                                            $rejectReason = 'MARKET_PLACE'; $rejectReasonDesc = 'Market Place'; $quality = 'Q4';
                                        }
                                        ItemApiClient::cancelOrderUpdateSkusItems($orderid, $orderid2WarehouseId[$orderid], $_POST['login'], 'CUSTOMER_RETURNED', $quality, $rejectReason, $rejectReasonDesc);
                                        
	 				if ($iscod) {
						$successrtoqueuedorders['cod'][] = $orderid;
					} else {
						$successrtoqueuedorders['on'][] = $orderid;
					}	
 				} catch(Exception $e){
					$sqllog->debug($e->getMessage());
					$failedtoqueue[] = array('orderid' => $orderid, 'reason' => 'System error happened.');	
					if ($orderData['order']['payment_method'] != 'cod') 
						$failedrtoqueuedorders['cod'][] = $orderid;
					else
						$failedrtoqueuedorders['on'][] = $orderid;
				}
 			} else{
	 			if ($orderData && array_key_exists("userinfo", $orderData)) {
					$custaddr= $orderData['userinfo']['s_address'];
					$custcity	= $orderData['userinfo']['s_city'];
					$custstate	= $orderData['userinfo']['s_state'];
					$custcountry= $orderData['userinfo']['s_country'];
					$custzipcode= $orderData['userinfo']['s_zipcode'];
					$custname = $orderData['userinfo']['s_firstname'];
					$phonenumber = $orderData['userinfo']['mobile'];
					$courier    = $orderData['order']['courier_service'];
					if(empty($courier)) {
						$courier = 'N.A.';
					}
					$trackingno  = $orderData['order']['tracking'];
					if(empty($trackingno)) {
						$trackingno = '0';
					}
					$comment = "RTO received at warehouse.<br /><br />Courier : " . $courier . "<br />  Tracking # : " . $trackingno ;
		            try{
						createRTOOrder($orderid, $iscod, $_POST['login'], $custname,$custaddr,$custcity,
							$custstate,$custcountry,$custzipcode,$comment,$courier,$trackingno, $phonenumber, null, false,false,$warehouseid);
						if($iscod) 
							$successrtoqueuedorders['cod'][] = $orderid;
						else
							$successrtoqueuedorders['on'][] = $orderid;
					}catch(Exception $e){
						$sqllog->debug($e->getMessage());
						$failedtoqueue[] = array('orderid' => $orderid, 'reason' => 'System error happened.');	
						if($iscod) 
							$failedrtoqueuedorders['cod'][] = $orderid;
						else
							$failedrtoqueuedorders['on'][] = $orderid;
					}
	 			} 				
 			}			
 		}
 		
 		$retarr = array('queued'=>$successrtoqueuedorders, 'failed'=>$failedrtoqueuedorders);
 		header('Content-type: text/x-json');
		print json_encode($retarr);
		exit(0);
 	}
 	
 	$warehouses = WarehouseApiClient::getAllWarehouses();
 
 	$smarty->assign("action", $action);
 	$smarty->assign("warehouses", $warehouses);
	$smarty->assign("main", "bulk_rto_queueing");
	func_display("admin/home.tpl",$smarty);
?>
