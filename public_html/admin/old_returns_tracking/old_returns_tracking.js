Ext.onReady(function() {
        Ext.QuickTips.init();
        var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
        Ext.Ajax.on('beforerequest', myMask.show, myMask);
        Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
        Ext.Ajax.on('requestexception', myMask.hide, myMask);

	var oderdetailpanel = new Ext.form.FormPanel({
		renderTo: 'contentPanel',
		title : 'Order Details',
                id : 'orderDetailsPanel',
                autoHeight : true,
                collapsible : true,
                border : true,
                bodyStyle : 'padding: 5px',
                width : 1000,
		items : [{
			layout : 'table',
                        border : false,
                        layoutConfig : {columns : 2},
                        defaults : {
                                        width : 450,
                                        border : false,
                                        layout : 'form',
                                        bodyStyle : 'padding-left : 15px'
                                },
			items : [{
				items : [{
                                        xtype : 'numberfield',
                                	id : 'orderid',
                               		fieldLabel : 'Order Id',
					allowBlank : false
                                }]				
			}]	
		}],
		buttons : [{
			text : 'Get Order Details',
			handler : function () {
				getOrderDetailsPage();				
			}
		}]
	});	

	var getOrderDetailsPage = function() {
		var form = oderdetailpanel.getForm();
		var orderid = "";
		if (form.isValid()) {
			orderid = form.findField("orderid").getValue();
		}		
		if (orderid == '') {
			alert("Please enter a valid order id");
			return ;
		}
		var redirectUrl = httpLocation + "/admin/old_returns_tracking/old_rt_tracking.php?order_id=" + orderid;
		window.open(redirectUrl);
	}	

	var couriersStore = new Ext.data.JsonStore( {
                fields : [ 'code', 'display_name' ],
                data : couriers,
                sortInfo : {
                        field : 'display_name',
                        direction : 'ASC'
                }
        });

	var couriersField = {
        	xtype : 'combo',
        	emptyText : 'Select Courier Service',
        	store : couriersStore,
        	mode: 'local',
        	displayField : 'display_name',
        	valueField : 'code',
        	triggerAction: 'all',
        	fieldLabel : 'Courier Partners',
        	name : 'format'
        } ;		

	var reportsPanel = new Ext.form.FormPanel( {
                renderTo : 'contentPanel',
                title : 'Returns Logistics Reports Panel',
                id : 'reportsPanel',
                autoHeight : true,
                collapsible : true,
                border : true,
                bodyStyle : 'padding: 5px',
                width : 1000,
                items : [{
                        layout : 'table',
                        border : false,
                        layoutConfig : {columns : 1},
                        defaults : {width : 400, border : false,
                                layout : 'form', bodyStyle : 'padding-left : 15px'},
                        items : [{
                                items : [couriersField]
                        }]
                }],
                buttons : [ {
                        text : 'Download Ready to Re-Ship report',
                        handler : function() {
                                var form        = reportsPanel.getForm();
                                var format      = "";
                                if (form.isValid()) {
                                        format = form.findField("format").getValue();
                                }
                                if (format == '') {
                                        alert("Please select a courier operator");
                                        return;
                                } else {
                                        window.open(httpLocation+ "/admin/old_returns_tracking/report.php?format="+format+"&returntype=RT&reporttype=reship");
                                }
                        }
                },{
			text : 'Download Ready to Pick-Up Report',
			handler : function () {
				var form        = reportsPanel.getForm();
                                var format      = "";
                                if (form.isValid()) {
                                        format = form.findField("format").getValue();
                                }
                                if (format == '') {
                                        alert("Please select a courier operator");
                                        return;
                                } else {
                                        window.open(httpLocation+ "/admin/old_returns_tracking/report.php?format="+format+"&returntype=RT&reporttype=pickup");
                                }
			}
		}]
        });

	var statusStore = new Ext.data.JsonStore({
                fields : [ 'code', 'display_name' ],
                data : statusArr,
                sortInfo : {
                        field : 'display_name',
                        direction : 'ASC'
                }
        });

        var return_status = new Ext.ux.form.SuperBoxSelect({
                xtype : 'superboxselect',
                fieldLabel : 'Return Status',
                emptyText : 'Select Return Status',
                name : 'status',
		id : 'return_status',
                anchor : '100%',
                store : statusStore,
                mode : 'local',
                displayField : 'display_name',
                valueField : 'code',
                forceSelection : true
        });

	var dateNamesStore = new Ext.data.JsonStore({
                fields : [ 'code', 'display_name' ],
                data : dateNames
        });

        var dateNamesField = {
        	xtype : 'combo',
        	emptyText : 'Select Date Type',
        	store : dateNamesStore,
        	mode: 'local',
        	displayField : 'display_name',
        	valueField : 'code',
        	triggerAction: 'all',
        	fieldLabel : 'Date When',
        	name : 'datetype',
		anchor : '100%'
        };

	var searchpanel = new Ext.form.FormPanel({
		renderTo : 'contentPanel',
                title : 'Search Panel',
                id : 'searchPanel',
                autoHeight : true,
                collapsible : true,
                border : true,
                bodyStyle : 'padding: 5px',
                width : 1000,
		items : [{
			layout : 'table',
                        border : false,
                        layoutConfig : {columns : 2},
                        defaults : {
					width : 450, 
					border : false,
                                	layout : 'form', 
					bodyStyle : 'padding-left : 15px'
				},
			items : [{
				items : [{
                                        xtype : 'numberfield',
                                	id : 'order_id',
                                	fieldLabel : 'Order Id'
                                }]
			},{
				items : [return_status]
			},{
				items : [dateNamesField]
			},{
			},{
				items : [{
                                        xtype : 'datefield',
                        		name : 'from_date',
                        		width : 150,
                        		fieldLabel : "From Date"
                                }]
			},{
				items : [{
                                        xtype : 'datefield',
                                        name : 'to_date',
                                        width : 150,
                                        fieldLabel : "To Date"
                                }]	
			}]
		}],
		buttons : [{
			text : "Search",
			handler : function() {
				reloadGrid();	
			}	
		},{
			text : "Export Report", 
			handler : function() {
				var params = getSearchPanelParams();
				var qrystr = "action=search&orderid=" + params.orderid + "&returntype=RT&returnstatus=" + params.returnstatus;
				qrystr = qrystr + "&datetype=" + params.datetype;
				if(params.fromdate != null && params.fromdate != ""){
                	qrystr = qrystr +"&fromdate=" + params.fromdate.format("Y-m-d H:i:s");
                }
                
                if(params.todate != null && params.todate != ""){
                	qrystr = qrystr +"&todate=" + params.todate.format("Y-m-d H:i:s");
                }
				
				qrystr = qrystr + "&paginate=false";
				var url = httpLocation+ "/admin/old_returns_tracking/old_returns_tracking.php?"+qrystr;
				window.open(url);
			}
		}]		
	});

	function getSearchPanelParams(){
		var form = searchpanel.getForm();
                if (form.isValid()) {
                        var params = {};
			params['orderid'] 	= form.findField('order_id').getValue();
			params['returnstatus']	= form.findField('status').getValue();
			params['datetype']	= form.findField('datetype').getValue();
			params['fromdate']	= form.findField('from_date').getValue();
			params['todate']	= form.findField('to_date').getValue();
			params["paginate"]="true";
			return params;	
		}
		return false;
	}

	function reloadGrid() {

                var params = getSearchPanelParams();
                if (params == false) {
                        Ext.MessageBox.alert('Error', 'Errors in the search form');
                        return false;
                }
                params["action"] = "search";
		params["returntype"] = "RT";

                searchResultsStore.baseParams = params;
                searchResultsStore.load();
        }

	var sm = new Ext.grid.CheckboxSelectionModel();
	
	var orderIdLinkRender = function(value, p, record) {
                return '<a href="' + httpLocation + '/admin/old_returns_tracking/old_rt_tracking.php?order_id=' + record.data["orderid"] + '" target="_blank">' + record.data["orderid"] + '</a>';
        }

	var statusRenderer = function(value, p, record) {
                var status = record.data['status'];
                for (var i=0; i< statusArr.length; i++) {
                        if (statusArr[i].code == status) {
                                return statusArr[i].display_name;
                        }
                }
                return status;
        }

        var courierRenderer = function(value, p, record) {
                var courier_code = record.data['courier_service'];
                for (var i=0; i< couriers.length; i++) {
                        if (couriers[i].code == courier_code) {
                                return couriers[i].display_name;
                        }
                }
                return courier_code;
        }
	
	var colModel = new Ext.grid.ColumnModel({
                defaults: {
                sortable: true
            },
            columns: [
                sm,
                {id : 'returnid', header : "Return Id", sortable: true, width: 75, dataIndex : 'id'},
                {id : 'orderid', header : "Order ID", sortable: true, width: 75, dataIndex : 'orderid', renderer : orderIdLinkRender},
		{id : 'returnreason', header : "Return Reason", sortable: true, width: 175, dataIndex : 'returnreason' },
		{id : 'status', header: 'Curr State', sortable: true, width: 175, dataIndex : 'currstate'} ,
		{id : 'paymentMethod', header: 'Payment Method', sortable: false, width: 100, dataIndex : 'paymentMethod'},
		{id : 'courier', header: 'Courier Service', sortable: false, width: 100, dataIndex : 'courier_service'},
		{id : 'trackingno', header: 'Tracking#', sortable: false, width: 100, dataIndex : 'trackingno'},
		{id : 'queuedDt', header: 'Queued Date', sortable: false, width: 140, dataIndex : 'queued_date'},
		{id : 'activityDt', header: 'Activity Date', sortable: false, width: 140, dataIndex : 'activityDate'},
		{id : 'custname', header: 'Customer Name', sortable: false, width: 125, dataIndex : 'custname'},
		{id : 'custAddr', header: 'Customer Address', sortable: false, width: 500, dataIndex : 'address'}
            ]
        });

	var searchResultsStore = new Ext.data.Store({
                url: 'oldreturnsajax.php',
                pruneModifiedRecords : true,
                reader: new Ext.data.JsonReader({
                        root: 'results',
                        totalProperty: 'count',
                        fields : ['id', 'orderid', 'returnreason', 'currstate', 'custname', 'address', 'queued_date', 'activityDate', 'paymentMethod', 'courier_service', 'trackingno']
                }),
                sortInfo:{field : 'id', direction:'ASC'},
                remoteSort: true
        });

	var pagingBar = new Ext.PagingToolbar({
            pageSize : 30,
            store: searchResultsStore,
            displayInfo: true
        });

	searchResults = new Ext.grid.EditorGridPanel({
            store  : searchResultsStore,
            cm : colModel,
            id : 'searchResults-panel',
            title : 'Search Results',
            frame : true,
                loadMask : false,
                autoHeight : true,
                sm: sm,
                bbar : pagingBar,
                stripeRows : true,
                autoScroll : true,
                viewConfig : {
                        enableRowBody : true
                },
                tbar:[]
	});	

	var mainPanel = new Ext.Panel({
                renderTo : 'contentPanel',
                border : false,
                items : [
                	orderDetailsPanel, reportsPanel, searchPanel, searchResults
                ],
                listeners : {}
	});			
});
