<?php
require "../auth.php";
include_once("$xcart_dir/include/security.php");
require $xcart_dir."/include/func/func.mk_old_returns_tracking.php";

$action = $_REQUEST['action'];
$uploadstatus = false;
if ($action == '') {
	$action = 'upload';
} else if ($action == 'verify') {
	if(!isset($_FILES) && isset($HTTP_POST_FILES))
                $_FILES = $HTTP_POST_FILES;
	if(!empty($_FILES['orderdetail']['name'])) {
		$uploaddir1 = '../../bulkrtoqueuing/';
                $extension = explode(".",basename($_FILES['orderdetail']['name']));
                $uploadfile1 = $uploaddir1 . "RTO-" .date('d-m-Y-h-i-s').".".$extension[1];
                if (move_uploaded_file($_FILES['orderdetail']['tmp_name'], $uploadfile1)) {
                         $uploadStatus = true;
                }
		if ($uploadStatus) {
			$fd = fopen ($uploadfile1, "r");
			$ordersupdated = array();
			$invalidOrders = array();
			$dataformaterror = "Data Format Should be : {{CellA-Orderid}}";
			$line = 0;
			while(!feof($fd)) {
				$buffer = fgetcsv($fd, 4096);
                                $line = $line + 1;
				if ($line == 1) 
					continue;
                if (empty($buffer[0])) {
                    break;
                }
				
				$fieldCount = count($buffer);
				if($fieldCount <1) {
					$smarty->assign('dataFormatError', "$dataFormatError at line# $line");
                    	break;
				}	
				
				$orderid 	= $buffer[0];
				$reason 	= $buffer[1];
				
				if(is_null($reason)){
					$reason = "";
				}else{
					$reason = addslashes($reason);
				}
				
				$weblog->info("BULK UPLOADER: orderid: " . $orderid . " Reason: " . $reason);
		
				$rtoData = getRTODetailsForOrderId($orderid);
				if(!$rtoData) {
					$invalidOrders[] = array('orderid' => $orderid, 'reason' => 'No RTO filed for this order');	
				} else {
    				$update_sql = "update mk_old_returns_tracking set returnreason = '$reason' where orderid = $orderid";
					db_query($update_sql);
					$ordersupdated[] = $orderid;
				}		
			}
			fclose ($fd);
			$weblog->info($ordersupdated);
			$smarty->assign('orders', $ordersupdated);
			$smarty->assign('failedorders', $invalidOrders);
		}	
	}
} else if ($action == 'confirm') {
	foreach ($orderids as $key=> $value){
				
	}	
}

$smarty->assign("action", $action);
$smarty->assign("main", "upload_rtos_to_queue");
func_display("admin/home.tpl",$smarty);

?>
