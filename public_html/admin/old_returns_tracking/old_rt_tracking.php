<?php
require "../auth.php";
require $xcart_dir."/include/security.php";
require $xcart_dir."/include/func/func.returns.php";
require $xcart_dir."/include/func/func.mk_old_returns_tracking.php";

$mode = $_REQUEST['action'];
$orderid = $_REQUEST['order_id'];

global $weblog;
$userroleentry = $XCART_SESSION_VARS['user_roles'];
$userroles = array();
foreach($userroleentry as $entry) {
    $userroles[] = $entry['role'];
}

if(empty($userroles)){
	$weblog->debug("Setting default admin role");
	$userroles = 'AD';
}	

$weblog->debug($userroles);

$allstates = _get_old_return_states();

$couriers = get_supported_courier_partners(false);
$smarty->assign("couriers", $couriers);

$returnReason = get_old_return_reasons();
$smarty->assign('returnreason', $returnReason);

function _getTS($dt, $tm){
	$dtcomponents = explode('/', $dt);
        $ts = $dtcomponents[2] . "-" . $dtcomponents[0] . "-" . $dtcomponents[1] . " ";
	$ts .= ($tm+12) . ":00:00";
	return $ts;  	
}

if ($mode == 'update') {
	$prev_state = $_REQUEST['currst'];
	$next_state = $_REQUEST['rt_action'];
	$comment    = addslashes($_REQUEST['comment']);
	$action	    = $allstates[$next_state];

	$data = array();
	if ($next_state && !empty($next_state)) {
		$data['currstate']	= $next_state;
		$data['activity']	= $action;
		$data['activitycode']	= $next_state;		
	}		
	
	if ($_REQUEST['courier'] && !empty($_REQUEST['courier'])) {
		$data['courier'] 	= $_REQUEST['courier'];
	}		
	
	if ($_REQUEST['tracking_num'] && !empty($_REQUEST['tracking_num'])) {
		$data['trackingno'] 	= addslashes($_REQUEST['tracking_num']);
	}	

	if($_REQUEST['comment'] && !empty($_REQUEST['comment'])){
		$data['remark'] = addslashes($_REQUEST['comment']);	
	}

	if($_REQUEST['items_rej_val'] && !empty($_REQUEST['items_rej_val'])) {
		$data['item_val_rejected'] = $_REQUEST['items_rej_val'];
	}

	if($_REQUEST['items_rstk_val'] && !empty($_REQUEST['items_rstk_val'])) {
		$data['item_val_restocked'] = $_REQUEST['items_rstk_val'];
	}
	
	if ($_REQUEST['coupon_code'] && !empty($_REQUEST['coupon_code'])) {
		$data['couponcode'] = addslashes($_REQUEST['coupon_code']);
	}			

	if ($_REQUEST['coupon_value'] && !empty($_REQUEST['coupon_value'])) {
		$data['couponvalue'] = addslashes($_REQUEST['coupon_value']);
	}
	
	$errorMsg = "";
	if($next_state == 'RTR') {
		if(trim($HTTP_POST_VARS['itembarcodes']) != "") {
			$returnedItems = trim($HTTP_POST_VARS['itembarcodes']);
			$status = validate_return_itembarcodes($returnedItems, $orderid);
			if($status) {
				$data['itembarcodes'] = $returnedItems;
			} else {
				$errorMsg .= "Some of the returned item barcodes are not issued for this order";
			}
		}
	}
	
		if($next_state == 'RTQP') {
			if(trim($HTTP_POST_VARS['itembarcodes']) != "") {
				$returnedItems = trim($HTTP_POST_VARS['itembarcodes']);
				$weblog->info(" Remove items - $returnedItems from order");
				$quality = $HTTP_POST_VARS['qapass-quality'];
				$order = func_query_first("select warehouseid from xcart_orders where orderid = $orderid", true);
				// when qa clears the return of items, we need to mark these items into Return from ops state.
				$status = return_qapass_update_items($orderid, $order['warehouseid'], $returnedItems, $quality, $login);
			}
		}

	if($errorMsg == "") {
		$data['adminuser'] = $login;		
		updateReturnsInfo('RT', $orderid, $prev_state, $next_state, $data, "", "");
	} else {
		$smarty->assign("errormsg", $errorMsg);
	}

} elseif($mode == 'add') {
	if (isOrderReturned($orderid)) {
		$smarty->assign("errormsg", "This order ($orderid) is either returned or an RTO already.");	
	} else {
		$iscod		= intval($_REQUEST['iscod']);
		$custname 	= $_REQUEST['cust_name'];
		$custaddr	= addslashes($_REQUEST['cust_addr']);
		$custcity 	= addslashes($_REQUEST['cust_city']);
		$custstate	= addslashes($_REQUEST['cust_state']);
		$custcountry	= addslashes($_REQUEST['cust_country']);
		$custzipcode	= addslashes($_REQUEST['cust_zipcode']);
		$returnreason 	= addslashes($returnReason[$_REQUEST['return_reason']]); 
		$shippingtype 	= addslashes($_REQUEST['shipping_type']);
		$phonenumber	= $_REQUEST['contact_no'];
		$prefpickupdt	= NULL; 
		if ($shippingtype == 'MS') {
			$prefpickupdt 	= _getTS($_REQUEST['pickup_dt'], $_REQUEST['pickup_time']);	
		}
		$comment    = addslashes($_REQUEST['comment']);
		createReturnOrder($orderid,$iscod,$login,$returnreason,$custname,$custaddr,$custcity,$custstate,$custcountry,$custzipcode,$shippingtype,$comment,$phonenumber,$prefpickupdt);
	}			
}

$orderData 	= getOrderReturnData($orderid);
$editHistory	= array();
if(!array_key_exists('currstate', $orderData)){
	$orderData['cancreate'] = true;	
}else{
	$orderData['canupdate'] = true;		
	$editHistory    = getOrderReturnActions($orderid);
}

$rtActions = get_old_return_next_states($orderid, $userroles);
$smarty->assign("rtactions", $rtActions);
$smarty->assign("allstates", _get_old_return_states());

$smarty->assign("orderData", $orderData);
$smarty->assign("edithistory", $editHistory);
$smarty->assign("main","old_rt_tracking");
func_display("admin/home.tpl", $smarty);
?>
