<?php
require "./auth.php";
require $xcart_dir . "/include/class/excel_reader2.php";
require $xcart_dir . "/include/security.php";

$file = $HTTP_POST_FILES['collection_file']['tmp_name'];
$gateway = $_POST['gateway'];

$messages = array();


if ($file) {
    $data = new Spreadsheet_Excel_Reader($file);
    $cells = $data->sheets[0]["cells"];
    array_shift($cells);
    $count = 0 ;
    foreach ($cells as $key => $cell) {
        if (count($cell) == 3) {
            func_array2update('xcart_orders',
                array(
                    "card_type" => $cell[3],
                    "gateway" => $gateway,
                    "collected_amount" => $cell[2])

            , "orderid='$cell[1]'");
            $count++;
        }
    }
    $messages [] = $count . ' entries from collection file processed successfully';
}

$file = $HTTP_POST_FILES['refund_file']['tmp_name'];
$gateway = $_POST['gateway'];

if ($file) {
    $data = new Spreadsheet_Excel_Reader($file);
    $cells = $data->sheets[0]["cells"];
    array_shift($cells);
    $count = 0 ;
    foreach ($cells as $key => $cell) {
        if (count($cell) == 2) {
            func_array2update('xcart_orders',
                array(
                    "refund_amount" => $cell[2])
            , "orderid='$cell[1]'");
            $count++;
        }
    }
    $messages [] = $count . ' entries from refund file processed successfully';
}

$smarty->assign("messages",$messages); 
@include $xcart_dir . "/modules/gold_display.php";
func_display("admin/order_dump.tpl", $smarty);
?>
