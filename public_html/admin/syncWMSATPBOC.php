<?php

include_once("auth.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/modules/amqpclients/producer/AMQPMessageProducer.php");
include_once("updateInventory.php");

$client = AMQPMessageProducer::getInstance();

echo "[".date('j-m-Y H:i:s')."(";

#$imsConn = mysql_connect('dbclusters.myntra.com:6001','myntraAppDBUser','9eguCrustuBR',TRUE);
$imsConn = mysql_connect('imsdbmaster.myntra.com:3307','MyntImsUser','5V9D9i4Yf3j991Q#',TRUE);
#$atpConn = mysql_connect('dbclusters.myntra.com:6001','myntraAppDBUser','9eguCrustuBR',TRUE);
$atpConn = mysql_connect('atpdbmaster.myntra.com:3306','MyntAtpUser','UrJ4KTteK473Pwk#',TRUE);

if (!$imsConn || !$atpConn) {
    die('Could not connect: ' . mysql_error());
}

if(mysql_select_db('myntra_ims', $imsConn ) === false) {
        echo "WMS db unaccessible \n";
        exit;
}

if(mysql_select_db('myntra_atp', $atpConn ) === false) {
        echo "ATP db unaccessible \n";
        exit;
}

$offset = 0;
$limit = 10000;
$hasMore= true;
$allSkus = array();
$skuQueries = array();
while($hasMore) {

    $warningSkus = array();
    $skuId2AtpInvMap = array();
    $skuId2ImsInvMap = array();

    $skuIds = array();
    $skusQuery = "select id from mk_skus where enabled = 1 LIMIT $offset, $limit";
    $sku_result = mysql_query($skusQuery, $imsConn);
    if($sku_result) {
        while ($row = mysql_fetch_array($sku_result, MYSQL_ASSOC)) {
            $skuIds[] = $row['id'];
        }
    }
    @mysql_free_result($sku_result);
    if(empty($skuIds)) {
        $hasMore = false;
        continue;
    }


    $atpInvCountQuery = "select sku_id, blocked_order_count as net_count from inventory where store_id = 1 and seller_id = 1 and supply_type = 'ON_HAND' and sku_id in (".implode(",", $skuIds).")";
    $atpInvResults = mysql_query($atpInvCountQuery, $atpConn);
    if($atpInvResults) {
        while ($row = mysql_fetch_array($atpInvResults, MYSQL_ASSOC)) {
            $skuId2AtpInvMap[$row['sku_id']] = $row['net_count'];
        }
    }
    @mysql_free_result($atpInvResults);


    $imsInvCountQuery = "select sku_id, sum(blocked_order_count) as net_count from wh_inventory_counts where warehouse_id in (28,36,89,93) and store_id = 1 and seller_id = 1 and supply_type = 'ON_HAND' and sku_id in (".implode(",", $skuIds).") group by sku_id";
    $imsInvResults = mysql_query($imsInvCountQuery, $imsConn);
    if($imsInvResults) {
        while ($row = mysql_fetch_array($imsInvResults, MYSQL_ASSOC)) {
            $skuId2ImsInvMap[$row['sku_id']] = $row['net_count'];
        }
    }
    @mysql_free_result($imsInvResults);
   
    $syncSkus = array();
    foreach($skuIds as $skuId) {
        $imsBoc = isset($skuId2ImsInvMap[$skuId])?$skuId2ImsInvMap[$skuId]:0;
        if (isset($skuId2AtpInvMap[$skuId]) && $imsBoc > $skuId2AtpInvMap[$skuId]) {
            $diff = $imsBoc - $skuId2AtpInvMap[$skuId];
            $syncSkus[] = $skuId;
            $query = "update inventory set blocked_order_count = blocked_order_count + ".$diff.", last_modified_on = now() where supply_type = 'ON_HAND' and store_id = 1 and seller_id = 1 and sku_id = $skuId;\n";
            $skuQueries[] = $query;
        }
    }
    $allSkus = array_merge($allSkus,$syncSkus);
   /* 
    if(count($syncSkus) > 0) {
        foreach(array_chunk($syncSkus,100) as $ids) {
            $request = array('handler' => 'Skus', 'data' => array('userId' => 'queue', 'action' => 'enable') );
            foreach($ids as $id) {
                $request['data']['skus'][] = array('sku' => array('id' => $id));
            }
            if(!empty($request['data']['skus'])) {
                $client->sendMessage('skuChangeQueue', $request, 'PortalApiRequest','xml',true);
            }
        }

    }
    */
    $offset += $limit;
}
if (count($allSkus) > 0){
    echo implode(",", $allSkus);
}
else{
    echo "NA";
}
$errQueries="";
updateInventory(null,null,$allSkus,$skuQueries,null,null,null,$errQueries);
echo "###errQueries### \n".$errQueries."\n";
//echo "###skuQueries### \n".implode("\n",$skuQueries);
echo ")".date('j-m-Y H:i:s')."]";

?>
