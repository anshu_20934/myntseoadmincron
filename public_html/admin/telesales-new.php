<?php
require_once "./auth.php";
require_once \HostConfig::$documentRoot."/include/security.php";
include_once \HostConfig::$documentRoot."/include/class/class.feature_gate.keyvaluepair.php";
include_once \HostConfig::$documentRoot."/sanitizept.inc.php";
include_once \HostConfig::$documentRoot."/exception/MyntraException.php";

include_once \HostConfig::$documentRoot."/modules/coupon/mrp/SmsVerifier.php";

$access_granted=true;//this should be false when we push to prod as only Telesales agents can do this

foreach ($XCART_SESSION_VARS['user_roles'] as $roledetails){
	if($roledetails['role']==='TCS'){
		$access_granted=true;
		break;
	}
}

if(!$access_granted===true){
	echo "Access Denied!!";
	exit;
}

global $weblog;
$cloginid = mysql_real_escape_string(trim($loginid));
$zipcode = mysql_real_escape_string(trim($pincode));

if($action=='login' ){
        $telesalesUser = "unknown";
        if (isset($XCART_SESSION_VARS['identifiers']['A']['login']))
            $telesalesUser = $XCART_SESSION_VARS['identifiers']['A']['login'];
	$customer_details=func_get_customer_detail($cloginid);
	if(!empty($customer_details)){
		$response['status']='S';
		$response['firstname']=$customer_details['firstname'];
		$response['lastname']=$customer_details['lastname'];
		$response['loginid']=$loginid;
		$response['mobile']=$customer_details['mobile'];
                
                $userAuthManager = new user\UserAuthenticationManager();
                $expiry = intval(FeatureGateKeyValuePairs::getFeatureGateValueForKey('telesalesLoginExpiry'));
                if ($expiry == 0)
                    $expiry= 30;
                $password = $userAuthManager->generateNewTeleSalesPwd($loginid, $expiry);
                $response['passwd']= $password;
                $response['root']= \HostConfig::$baseUrl;
	}else{
		$response['status']='F';
		$response['errorcode']='0'; //Record not found
		$response['loginid']=$loginid;
	}	
	showMessage();

}else if($action=='registeruser'){
	$customer_details=func_get_customer_detail($cloginid);
	if(!empty($customer_details)){
		$response['status']='F';
		$response['errorcode']=2;//Already exists
		$response['firstname']=$customer_details['firstname'];
		$response['lastname']=$customer_details['lastname'];
		$response['loginid']=$customer_details['email'];
		$response['mobile']=$customer_details['mobile'];
	}else{
		$profile_values = array();
		$profile_values['email'] = $cloginid;
		$profile_values['login'] = $cloginid;
		$profile_values['mobile'] = mysql_escape_string($phoneno);
		$profile_values['firstname']=mysql_escape_string($username);
		$profile_values['usertype'] = "C";
		$pass=rand(12345,99999)."";
		$crypted = "";
		$profile_values['password'] = $crypted;
		$profile_values['password_md5'] = addslashes(user\UserAuthenticationManager::encryptToMD5($pass));
		$profile_values['termcondition'] = '1';
		$profile_values['addressoption'] = 'M';
		func_array2insert('customers', $profile_values);
                /*logging telesales specific customer signup*/
                $logSignup = array();
                $logSignup["userName"] = $cloginid;
                $telesalesUser=$XCART_SESSION_VARS['identifiers']['A']['login'];
                $logSignup["telesalesUserName"] = $telesalesUser;
                $logSignup["mobile"] = mysql_escape_string($phoneno);
                func_array2insertWithTypeCheck("TelesalesSignup_log", $logSignup);
                /*END: logging telesales specific customer signup*/
		$response['status']='S';
		$response['loginid']=$loginid;
		$response['mobile']=mysql_escape_string($phoneno);
		include_once \HostConfig::$documentRoot."/modules/coupon/database/CouponAdapter.php";
		include_once (\HostConfig::$documentRoot."/modules/coupon/mrp/RuleExecutor.php");
		include_once \HostConfig::$documentRoot."/include/func/func.reset_password.php";
		$smsVerifier = SmsVerifier::getInstance();
		$smsVerifier->addNewMapping($profile_values['mobile'], $profile_values['login']);
		$couponAdapter = CouponAdapter::getInstance();
		$couponAdapter->customerJoined($profile_values['login'], $profile_values['referer']);
		sendResetPasswordMail($cloginid,'telesales_registration',empty($username)?"Customer":$username);
		// Synchronous call to the MRP New Login rule.
		$logging_str= "Generating First login coupons for ".$profile_values['login'];
		$weblog->warn($logging_str);
		$ruleExecutor = RuleExecutor::getInstance();
		$ruleExecutor->runSynchronous("customer", $profile_values['login'], "1");    // "1" is the ruleId for "NewLogin" rule.
		$weblog->warn("Done generating coupons");
	}
	$customers=func_get_customer_detail_from_mobile($response['mobile']);
	foreach ($customers as $cust){
		$res['firstname']=$cust['firstname'];
		$res['lastname']=$cust['lastname'];
		$res['loginid']=$cust['email'];
		$res['mobile']=$cust['mobile'];
		$response['customers'][]=$res;
	}
	showMessage();

}else if($action=='resetpassword'){
	include_once \HostConfig::$documentRoot."/include/func/func.reset_password.php";
	sendResetPasswordMail($cloginid);
	$response['status']='S';
    writeMessage();
	showMessage();

}else if($action=='codcheck'){
	$response['status']='S';
	$response['zipcode']=$zipcode;
	if(is_zipcode_servicable_cod($zipcode)){
		$response['available']='true';
	}else{
		$response['available']='false';
	}
	showMessage();	

}


$smarty->assign("main","telesales_view_new");
func_display("admin/home.tpl",$smarty);


function writeMessage($status=null, $code=null, $message=null){
    global $response;

    if(!empty($status)){
        $response['status'] = $status;
    }
    if(!empty($code)){
        $response['errorcode'] = $code;
    }
    if(!empty($message)){
        $response['errormessage'] = $message;
    }    
}

function showMessage(){
    global $response;

    echo json_encode($response);
    exit;
}
