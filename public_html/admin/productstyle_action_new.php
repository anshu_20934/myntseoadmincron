<?php
/**
 * Created by Nishith.
 * User: myntra
 * Date: Dec 24, 2010
 * Time: 12:45:15 AM
 * To change this template use File | Settings | File Templates.
 */
require "./auth.php";
require $xcart_dir."/include/security.php";
require $xcart_dir."/include/SimpleImage.php";
include_once($xcart_dir."/include/solr/solrProducts.php");
include_once($xcart_dir."/include/solr/solrUpdate.php");
include_once($xcart_dir."/include/func/func.image.php");
include_once($xcart_dir."/modules/discount/DiscountEngine.php");
include_once($xcart_dir."/include/func/func_sku.php");
include_once($xcart_dir."/include/class/style/class.MyntraStyle.php");
@include ($xcart_dir."/include/func/func.displayCategory.php");
include_once("$xcart_dir/modules/apiclient/SkuApiClient.php");
include_once"$xcart_dir/utils/imagetransferS3.php";
include_once ("$xcart_dir/include/func/funcStyleProperties.php");
require_once("$xcart_dir/admin/size_unification_admin_new_function.php");
include_once("$xcart_dir/modules/apiclient/AuditApiClient.php");
include_once ("$xcart_dir/modules/styleService/PortalRequestHandlerClient.php");

use imageUtils\Jpegmini;


$expiryTime = FeatureGateKeyValuePairs::getFeatureGateValueForKey('editNPDepricate', -1);
$currTimeMilliseconds = round(microtime(true) * 1000);
$depricatemsg = "Please start using  <a href = 'http://spoc.mynt.myntra.com/faces/catalog/product/searchStyles.xhtml'> new edit style</a>";


if($currTimeMilliseconds >= $expiryTime)
{
       header("Location: catalog/deprecatedENPMessagePage.php");
       exit();
}



if(!isset($_FILES) && isset($HTTP_POST_FILES))
{
	$_FILES = $HTTP_POST_FILES;
}

$applySizeScaleRestriction = FeatureGateKeyValuePairs::getBoolean('applySizeScaleRestriction');
//if scale restriction is on and for article type scale does not exist then make it off
if($applySizeScaleRestriction && !empty($_GET['mode']) && $_GET['mode'] == 'modifystyle')
{
	$styleIdForScaleRestriction = $_GET['style_id'];
	$articleIdForScaleRestriction = func_query_first_cell("SELECT global_attr_article_type from mk_style_properties where style_id = '$styleIdForScaleRestriction'", TRUE);
	$scaleForScaleRestriction = func_query_first_cell("SELECT id FROM size_chart_scale where id_catalogue_classification_fk = '$articleIdForScaleRestriction'", TRUE);
	if(empty($scaleForScaleRestriction))
	{
		$applySizeScaleRestriction = false;
	}
}
//set $applySizeScaleRestriction value using submitted data
if($applySizeScaleRestriction && isset($_POST['mode']) && $_POST['mode'] == 'updatestyle')
{
	if(isset($_POST['appy_scale_mode']))
	{
		if($_POST['appy_scale_mode'] == 'false')
		{
			$applySizeScaleRestriction = false;
		}
	}
}
if(isset($_GET['mode']) && $_GET['mode'] =='getDiscountRule')
{
	$brand = $_GET['brand'];
	$articletypeid = $_GET['articletypeid'];
	$fashion = $_GET['fashion'];
	
	$str = '';
	$getPossibleDiscountRuleQuery = "select * from mk_discount_rules dr, mk_catalogue_classification cc where cc.typename=dr.articletype and dr.enabled='1' ";  
    $getPossibleDiscountRuleQuery.= "and dr.brand='$brand' and cc.id='$articletypeid' and dr.fashion='$fashion' ";
    $getMatchingRule = func_query_first($getPossibleDiscountRuleQuery, TRUE);
	
    if(!empty($getMatchingRule))
    {
    	$str = "Discount Rule #" . $getMatchingRule['ruleid'] . " - " . $getMatchingRule['display_name'] . " - Will be applied<br/>"; 
    }
    
	echo $str; exit;
}

//get all filters
if(isset($_GET['mode']) && $_GET['mode'] =='getSizeOptionValue')
{
	$scaleId = mysql_escape_string(trim($_GET['scale_id']));
	$sizeOptions = json_encode(func_query("select id, size_value from size_chart_scale_value where id_size_chart_scale_fk = '$scaleId' order by intra_scale_size_order asc", TRUE));
	echo $sizeOptions;
	exit;
}

//get all filters
if(isset($_GET['mode']) && $_GET['mode'] =='getParents')
{
    $id=$_GET['article_id'];
    $style_id=$_GET['style_id'];
    $res=func_query("select * from mk_catalogue_classification where id=".$id, TRUE);
    
    $outstr='';
    
    if(!empty($res[0]['parent1']) && !empty($res[0]['parent2']))
    {
        $sub=func_query("select * from mk_catalogue_classification where id=".$res[0]['parent1'], TRUE);
        $mas=func_query("select * from mk_catalogue_classification where id=".$res[0]['parent2'], TRUE);
        if($sub[0]['is_active'] ==1 && $mas[0]['is_active'] ==1)
        {
        	$outstr= '{"success":"true" , "subcategoryname":"'.$sub[0]['typename'].'","subcategoryid":"'.$sub[0]['id'].'", "mastercategoryname":"'.$mas[0]['typename'].'","mastercategoryid":"'.$mas[0]['id'].'"';
        }
		else
		{
			$outstr= '{"success":"false"';
		}
		
    }
    else
    {
        $outstr= '{"success":"false"';
    }
    
    // Logic to populate article specific attributes
    
    	$outstr= $outstr.', "attributes":[';
    	
	    $result_set=func_query("select * from mk_attribute_type where is_active = '1' and catalogue_classification_id=".$id, TRUE);
    	$firsttimeloop1 = true;
		foreach ($result_set as $row)
		{
			if($firsttimeloop1)
			{
				$firsttimeloop1=false;
				$outstr= $outstr.' {';
			}
			else
			{
				$outstr= $outstr.' , {';
			}
    		// Populate name values
    		$outstr= $outstr.' "name":"'.$row['attribute_type'].'", "id":"'.$row['id'].'", "allowedValues":';
    		$query = "select 	a.attribute_value as 'attribute_name',
    							(select if(count(*)>0,'selected','') from mk_style_article_type_attribute_values where article_type_attribute_value_id=a.id and product_style_id=\"".$style_id."\") as selected,
    							a.id as 'attribute_value_id' 
    							from mk_attribute_type_values a
								where a.is_active = '1' and a.attribute_type_id=".$row['id'];			
    		$result_set_values = json_encode(func_query($query, TRUE));
    		$outstr= $outstr.$result_set_values;
			$outstr = $outstr.'}';
		}

		$outstr = $outstr.']';
    
		//get size scales
		if($applySizeScaleRestriction)
		{
			$sizeScales = json_encode(func_query("select id, scale_name  from size_chart_scale where id_catalogue_classification_fk=".$id, TRUE));
			$outstr= $outstr.', "size_scales":'.$sizeScales;
		}

    //close json
    $outstr = $outstr.'}';
    
    // final output for the json
    echo $outstr;
    exit;
}


if(isset($_GET['mode']) && $_GET['mode'] =='list_skus')
{
	$result_set=SkuApiClient::getSkuWithCodeStartingBy($_GET['query']);
	$str="";
	foreach ($result_set as $row)
	{
		$str.=$row['code'].',';
	}
	echo $str;
	exit;
	
}

if(isset($_GET['mode']) && $_GET['mode'] =='check_skus')
{
	global $weblog;
	$style_id=$_GET['style_id'];
	$sizeOptionValues = explode(", ", mysql_escape_string($_GET['size_option_list']));
	$sizeValueArray = array();
	$dupSizes = "";
	$dupSizesArray = array();
	$boolDuplicateSizes = false;
	$stylePrice = $_GET['stylePrice'];
	$styleStatus = $_GET['styleStatus'];
	//validate price should be +ve numeric value
	if(empty($stylePrice))
	{
		if($styleStatus != 'D')
		{
			echo "Price is blank.";
			exit;
		}
	}
	else
	{
		if (is_numeric($stylePrice))
		{
			$stylePrice = round($stylePrice, 2);
			if($styleStatus != 'D')
			{
				if($stylePrice <= 0)
				{
					echo "Price $stylePrice is not a +ve numeric value.";
					exit;
				}
			}
			elseif($stylePrice < 0)
			{
				echo "Price $stylePrice is not a +ve numeric value.";
				exit;
			}
		}
		else
		{
			echo "Price $stylePrice is not a numeric value.";
			exit;
		}
	} 
	foreach ($sizeOptionValues as $sizeValue)
	{
		$sizeValue = trim($sizeValue);
		if($sizeValue=='')
		{
			continue;
		}
		$sizeValue = strtoupper($sizeValue);
		$sizeValue = str_replace(' ', '', $sizeValue);
		if(in_array($sizeValue, $sizeValueArray))
		{
			if(!in_array($sizeValue, $dupSizesArray))
			{
				$dupSizes.="$sizeValue ";
				array_push($dupSizesArray, $sizeValue);
			}
		}
		$sizeValueArray[] = $sizeValue;
	}
	if(!empty($dupSizes))
	{
		echo "Duplicated Size values: $dupSizes";
		$boolDuplicateSizes = true;
	}
	$codes=explode(" ", mysql_escape_string($_GET['query']));
	$sku_codes;
	$code_array;
	$dup_skus="";
	foreach ($codes as $code)
	{
		$code = trim($code);
		if($code=='')
		{
			continue;
		}

		if(in_array($code, $code_array))
		{
			$dup_skus.="$code ";
		}
		$code_array[]= $code;
	}
	
	if(!empty($dup_skus))
	{
		echo $boolDuplicateSizes?" and ":"";
		echo "Duplicated SKUs: $dup_skus";
		exit;
	}
	
	if($boolDuplicateSizes)
	{
		exit;
	}
	
	$sku_details = SkuApiClient::getSkuDetailsForSkuCode($code_array);
	$skuIds = array();
	$sku_id2code_mapping = array();
	$sku_code2id_mapping = array();
	foreach($sku_details as $sku_row)
	{
		$skuIds[] = $sku_row['id'];
		$sku_id2code_mapping[$sku_row['id']] = trim($sku_row['code']);
		$sku_code2id_mapping[trim($sku_row['code'])] = $sku_row['id'];
	}
	
	$invalid_sku_codes = array();
	if(!empty($skuIds))
	{
		$sql="SELECT sku_id, style_id from  mk_styles_options_skus_mapping where sku_id in ( ".implode(",", $skuIds).") ";
		if(!empty($style_id) and $style_id != "")
		{
			$sql .= " and style_id != $style_id";
		}
		$sql .= " group by sku_id";
	
		$mapped_skus=func_query($sql, TRUE);

		$invalid_sku_entries = array();		
		if($mapped_skus)
		{
			foreach($mapped_skus as $row)
			{
				$skucode = $sku_id2code_mapping[$row['sku_id']];
				$invalid_sku_entries[] = "(".$sku_id2code_mapping[$row['sku_id']]." - ".$row['style_id']." )";
			}
		}
		
		if(!empty($invalid_sku_entries))
		{
			echo "Duplicate SKU Mappings Found in Inventory (Please contact Inventory team with following information): ". implode(" ", $invalid_sku_entries);
			exit;
		}
		
		foreach($code_array as $code)
		{ 
			if(!isset($sku_code2id_mapping[$code]))
			{
				$invalid_sku_codes[] = $code;
			}
		}
	}
	else
	{
		$invalid_sku_codes = $code_array;
	}
	
	if(!empty($invalid_sku_codes))
	{
		echo "Invalid Sku code(s) - ".implode(", ", $invalid_sku_codes);
		exit;
	}
	
	echo '1';
	exit;
	
}

if(isset($_GET['mode']) && $_GET['mode'] =='checkArticleNumber')
{
    $article_number=$_GET['article_number'];
    if(!empty($_GET['style_id']))
    {
        $check_s=func_query("select * from mk_style_properties where style_id=".$_GET['style_id'], FALSE);
        if($check_s[0]['article_number'] == $article_number)
        {
            echo "true";exit;
        }

    }
    $check=func_query("select * from mk_style_properties where article_number='".$article_number."'", FALSE);
    if(!empty($check))
    {
        echo "false";exit;
    }
    else
    {
        echo "true";exit;
    }
   exit;

}

if(isset($_POST['mode']) && $_POST['mode'] =='remove_image')
{
    $style_id = mysql_real_escape_string($_POST['style_id']);
    $image_type = mysql_real_escape_string($_POST['image_type']);
    if($image_type == 'default_image')
    {// Double checking to not delete the default_image
        exit;
    }
    $query = "update mk_style_properties set $image_type='' where style_id = '$style_id'";
    $sqllog->debug("productstyle_action_new.php : Removing image $image_type for styleid : $style_id with query : $query");
    func_query($query);
    PortalRequestHandlerClient::refreshStyles(array($style_id));
    exit;
}

function getAllFilters()
{
    $main_array=array();
    $filter_groups=array();
    $sql ="SELECT * FROM  mk_filter_group where group_name not like '%_size' order by display_order ";
    $grouprecords = func_query($sql, TRUE);
    foreach($grouprecords as $groups)
    {
        $filter_sql="select f.*,g.group_name,g.group_label from mk_filters f join mk_filter_group g on f.filter_group_id=g.id where f.is_active and f.filter_group_id=".$groups['id'];
        $filter_by_groups = func_query($filter_sql, TRUE);
        $main_array[$groups['group_label']]=$filter_by_groups;
        $filter_groups[$groups['group_name']]=$groups['id'];
    }
    return $main_array;

}

/* function to get product styles */
function getProductStyle($styleid)
{
        global  $sql_tbl;
		$query = "SELECT a.id, a.product_type,
		a.name,
		a.label,
		a.description,
		a.material,
		a.is_active,
		a.price,
		a.iventory_count,
		a.styletype,
		a.default_store_markdown,
		a.default_store_rate,
		a.bulk,
		a.style_code,
		a.is_customizable,
		a.size_chart_image

		from $sql_tbl[mk_product_style] a
		where a.id='$styleid' order by a.name ";
		$prstyle =   func_query($query, FALSE);

		$query = "SELECT image_path, name FROM $sql_tbl[mk_style_images] WHERE styleid = '$styleid' AND isdefault = 'Y'";
		$result = db_query($query);
		$styleimages = array();
		while($row= db_fetch_array($result))
		{
			$styleimages[$row['name']] = $row['image_path'];
		}

		$prstyle[0]['images'] = $styleimages;

		return $prstyle;

}

function getAllStyleProperties($styleid)
{
    $properties=func_query("select * from mk_style_properties where style_id='$styleid'", FALSE);
    if(!empty($properties) && !empty($properties[0]))
    {
        if(!empty($properties[0]['global_attr_sub_category']))
        {
             $properties[0]['global_attr_sub_category_name']=func_query_first_cell("select typename from mk_catalogue_classification where id=".$properties[0]['global_attr_sub_category'], TRUE);
        }
        if(!empty($properties[0]['global_attr_master_category']))
        {
             $properties[0]['global_attr_master_category_name']=func_query_first_cell("select typename from mk_catalogue_classification where id=".$properties[0]['global_attr_master_category'], TRUE);   
        }
        $properties=$properties[0];
    }
    return $properties;
}

/*function to get all the inages */
function getImage($styleid)
{
     $imagepath=func_query("select id,image_path,type,isdefault from mk_style_images where styleid='$styleid' and isdefault='Y' ", TRUE);
	 return $imagepath;

}

/* function to get product option */
function getProductOption($styleid, $optionid=0)
{
        global  $sql_tbl, $applySizeScaleRestriction;

		$SQL ="SELECT $sql_tbl[mk_product_options].price,".
					" $sql_tbl[mk_product_options].name,".
					" $sql_tbl[mk_product_options].id,".
					" $sql_tbl[mk_product_options].default_option,".
					" $sql_tbl[mk_product_options].value,".
					" $sql_tbl[mk_product_options].is_active";
		if($applySizeScaleRestriction)
		{
			$SQL = $SQL.", $sql_tbl[mk_product_options].id_size_chart_scale_value_fk";
		}
		$SQL = $SQL." FROM  $sql_tbl[mk_product_options], $sql_tbl[mk_product_style]".
					" WHERE $sql_tbl[mk_product_options].style = $sql_tbl[mk_product_style].id".
					" AND  $sql_tbl[mk_product_style].id = $styleid  ";
		
		if($optionid != 0)
		{
			$SQL .= " AND $sql_tbl[mk_product_options].id = $optionid  ";
		}

		 $SQL .= "  ORDER by $sql_tbl[mk_product_options].name ";

		$prd_options =   func_query($SQL, TRUE);
		return $prd_options;

}

function getSkus($style_id, $option_id = 0)
{
	global $applySizeScaleRestriction;
	$sql = "SELECT sm.id, sm.sku_id, po.is_active sku_enabled, po.id option_id, po.value option_value";
	if($applySizeScaleRestriction)
	{
		$sql = $sql.", po.id_size_chart_scale_value_fk size_id";
	}
	$sql = $sql." FROM mk_styles_options_skus_mapping sm".
				" LEFT JOIN mk_product_options po ON (po.id = sm.option_id)".
				" WHERE sm.style_id = $style_id";
	
	if($option_id != 0)
	{
		$sql = $sql . " AND sm.option_id = $option_id";
	}
	
	$result=func_query($sql, TRUE);
	$skuids;
	if(count($result)==1)
	{
		$skuids=$result[0]['sku_id'];
	}
	else
	{
		foreach ($result as $row)
		{
			$skuids[]=$row['sku_id'];
		}
	}
	$skudetails=SkuApiClient::getSkuDetails($skuids);
	foreach($skudetails as $skudetail)
	{
		$skudetailmap[$skudetail['id']]=$skudetail;
	}
	
	foreach ($result as $key=>$row)
	{
		$result[$key]['sku_code']=$skudetailmap[$row['sku_id']]['code'];
		$result[$key]['sku_inv_count']=$skudetailmap[$row['sku_id']]['inv_count'];
	}
	return $result;
}

/* function to get product custumization */
function getProductCust($styleid)
{
        global  $sql_tbl;

		$SQL ="SELECT $sql_tbl[mk_style_customization_area].name,$sql_tbl[mk_style_customization_area].id,$sql_tbl[mk_style_customization_area].image_t,$sql_tbl[mk_style_customization_area].image_l  FROM  $sql_tbl[mk_style_customization_area],$sql_tbl[mk_product_style] where  $sql_tbl[mk_style_customization_area].style_id = $sql_tbl[mk_product_style].id AND $sql_tbl[mk_product_style].id = $styleid order by $sql_tbl[mk_style_customization_area].name ";
        $prd_cust =   func_query($SQL, TRUE);
		return $prd_cust;
}

function getAllFilterGroups()
{
	$groups=array();
	$result=func_query("select * from mk_filter_group where group_name not like '%_size'", TRUE);
	foreach($result as $group)
	{
		$trim_space=str_replace(" ","",$group['group_name']);
		$lower_case=strtolower(str_replace("/","",$trim_space));
		$groups[$group['id']]=$lower_case;
	}	
	return $groups;
}

    
if(isset($_POST['mode']))
{
	$display_name = preg_replace("/[\s]+/", "-", trim($product_display_name));
	$display_name = preg_replace("/[^a-zA-Z0-9,-_&]/","", $display_name);
	$display_name = preg_replace("/\./","", $display_name);
	$imagePrefix =  $display_name."_";

	$default_image_path = $_FILES['default_image']['tmp_name'];
	$default_image_name = $_FILES['default_image']['name']; 
	$default_image_url = func_resize_backup_uploaded_product_image($default_image_path, $default_image_name, "default", true, $imagePrefix);
	$front_image_path = $_FILES['front_image']['tmp_name'];
	$front_image_name = $_FILES['front_image']['name'];
	$front_image_url = func_resize_backup_uploaded_product_image($front_image_path, $front_image_name, "others", true, $imagePrefix);
    
	$back_image_path = $_FILES['back_image']['tmp_name'];
	$back_image_name = $_FILES['back_image']['name'];
	$back_image_url = func_resize_backup_uploaded_product_image($back_image_path, $back_image_name, "others", true, $imagePrefix);

	$right_image_path = $_FILES['right_image']['tmp_name'];
	$right_image_name = $_FILES['right_image']['name'];
	$right_image_url = func_resize_backup_uploaded_product_image($right_image_path, $right_image_name, "others", true, $imagePrefix);	
	
	$left_image_path = $_FILES['left_image']['tmp_name'];
	$left_image_name = $_FILES['left_image']['name'];
	$left_image_url = func_resize_backup_uploaded_product_image($left_image_path, $left_image_name, "others", true, $imagePrefix);	
	
	$top_image_path = $_FILES['top_image']['tmp_name'];
	$top_image_name = $_FILES['top_image']['name'];
	$top_image_url = func_resize_backup_uploaded_product_image($top_image_path, $top_image_name, "others", true, $imagePrefix);
	
	$bottom_image_path = $_FILES['bottom_image']['tmp_name'];
	$bottom_image_name = $_FILES['bottom_image']['name'];
	$bottom_image_url = func_resize_backup_uploaded_product_image($bottom_image_path, $bottom_image_name, "others", true, $imagePrefix);
    
	$tex_color_image_path = $_FILES['tex_color_image']['tmp_name'];
	$tex_color_image_name = $_FILES['tex_color_image']['name'];
	$tex_color_image_url = func_resize_backup_uploaded_product_image($tex_color_image_path, $tex_color_image_name, "others", true, $imagePrefix);
    
	if(!empty($_POST['size_chart_image_name'])){
		$name=str_replace(" ","_",$_POST['size_chart_image_name']);
		$s_upload_dir= dirname(__FILE__).'/../images/style/sizechartimages/';
		$filename = $s_upload_dir.$name.".".$img_arr[1];
		 if (is_file($filename)) {
                $file_prefix=$name."_".time();
            }else{
                $file_prefix=$name;
            }
    }
	else {
		$file_prefix=md5(rand().time());
	}
		 
	$size_chart_image_path = $_FILES['size_chart_image']['tmp_name'];
	$img_arr=explode(".",$_FILES['size_chart_image']['name']);
	$size_chart_image_name = $file_prefix.".".$img_arr[1];
    if(!empty($_FILES['size_chart_image']['name'])){// Only process when image uploaded
        $size_chart_image_url_uploaded = func_resize_backup_uploaded_product_image($size_chart_image_path, $size_chart_image_name, "sizechart", true, $imagePrefix);
    }
    
    if(!empty($size_chart_image_url_uploaded))
    {//In case there is image uploaded then override the size_chart_image_url
        $size_chart_image_url = $size_chart_image_url_uploaded;
    }
    else if(strpos($size_chart_image_url, $cdn_base) === FALSE)
    {
        $size_chart_image_url = $cdn_base."/".$size_chart_image_url;
    }
    
    if($default_image_url === null || $top_image_url === null || $front_image_url === null || $back_image_url === null ||
        $right_image_url === null || $left_image_url === null || $bottom_image_url === null || $size_chart_image_url === null || $tex_color_image_url === null)
    {
        $error_msg = "Error while processing one or more uploaded images.";
    }
    else if($_POST['mode'] == 'addstyle' && empty($default_image_url))
    {
        $error_msg = "No default image submitted while adding a new product";
    }
    else if($_POST['mode'] == 'addstyle')
    {
        if($default_image_url === null)
        {
        	$default_image_url = "";
        }
        if($front_image_url === null)
        {
        	$front_image_url = "";
        }
        if($back_image_url === null)
        {
        	$back_image_url = "";
        }
        if($right_image_url === null)
        {
        	$right_image_url = "";
        }
        if($left_image_url === null)
        {
        	$left_image_url = "";
        }
        if($top_image_url === null)
        {
        	$top_image_url = "";
        }
        if($bottom_image_url === null)
        {
        	$bottom_image_url = "";
        }
        if($size_chart_image_url === null)
        {
        	$size_chart_image_url = "";
        }
        else
        {
        	$sizeChartFileExt = strtolower(substr($size_chart_image_url, -4));
        	if($sizeChartFileExt != ".jpg")
        	{
        		$size_chart_image_url = "";
        	}
        }
        if($tex_color_image_url === null)
        {
        	$tex_color_image_url="";
        }

		$description = mysql_real_escape_string($_POST['product_desc']);
        $query_data = array(
					"product_type" => $product_type,
					"name" => trim($product_display_name),
					"label" => trim($product_display_name),
					"description" => $description,
					"price" => $price,
					"iventory_count"=> 100,
					"styletype" => 'P',
					"material" =>'',
					"bulk" => 'Y',
					"style_code" => 'NPP',
					"default_store_markdown"	=> 0,
					"default_store_rate"		=> 0,
                    "size_chart_image"  => $size_chart_image_url,
					"is_customizable"		=> $is_customizable
			   );
        func_array2insert("mk_product_style", $query_data);
        $styleid  = db_insert_id();
		$s_filter=implode(",", $HTTP_POST_VARS['filters']);
        $search_images=explode(",", $default_image_url);
        $productTags = str_replace(" ,", ",", str_replace(", ", ",", trim($product_tags)));
        $styleNote = mysql_real_escape_string($_POST['styleNote']);
        $materialsCareDesc = mysql_real_escape_string($_POST['materialsCareDesc']);
        $sizeFitDesc = mysql_real_escape_string($_POST['sizeFitDesc']);
        $sports_query_data = array(
                    "style_id" 			=> $styleid,
                    "article_number" 			=> $article_number,
                    "product_tags"				=> $productTags,
                    "product_display_name"		=> trim($product_display_name),
                    "variant_name" 				=> trim($article_name),
                    "discount_type" 			=> $discount_type,
                    "discount_value"    		=> $discount_value,
                    "myntra_rating"		        => !empty($rating) ? $rating : 0,
                    "add_date"                  => time(),
                    "target_url"                => trim($target_url),
                    "default_image"             => $search_images[0],
                    "search_image"              => $search_images[2],
                    "search_zoom_image"         => $search_images[1],
         			"tex_color_image"			=> $tex_color_image_url,
                    "global_attr_article_type"  => $global_attr_article_type,
                    "global_attr_sub_category"  => $global_attr_sub_category,
                    "global_attr_master_category"=>$global_attr_master_category,
                    "global_attr_brand"         => $global_attr_brand,
                    "global_attr_age_group"     => $global_attr_age_group,
                    "global_attr_gender"        => $global_attr_gender,
                    "global_attr_base_colour"   => $global_attr_base_colour,
                    "global_attr_colour1"       => $global_attr_colour1,
                    "global_attr_colour2"       => $global_attr_colour2,
                    "global_attr_fashion_type"  => $global_attr_fashion_type,
                    "global_attr_season"        => $global_attr_season,
                    "global_attr_year"          => $global_attr_year,
                    "global_attr_usage"         => $global_attr_usage,
                    "global_attr_catalog_add_date"=> time(),
                    "style_note"                => $styleNote,
        			"materials_care_desc"       => $materialsCareDesc,
        			"size_fit_desc"             => $sizeFitDesc,
        			"comments"                  => trim($style_comments)

        );
        $sports_query_data["front_image"] = $front_image_url;
        $sports_query_data["back_image"] = $back_image_url;
        $sports_query_data["left_image"] = $left_image_url;
        $sports_query_data["right_image"] = $right_image_url;
        $sports_query_data["top_image"] = $top_image_url;
        $sports_query_data["bottom_image"] = $bottom_image_url;
        
    	//check if the status is active while product is added then set catalog date to current time
        if($query_data["styletype"] == 'P')
        {
                $sports_query_data["catalog_date"] = time();
        }
        
        $insert_id = func_array2insert("mk_style_properties", $sports_query_data);
		// Update article_type specific attributes to db
		$time = time();	
		$login = $XCART_SESSION_VARS['identifiers']['A'][login];
		for ($i = 0; $i < count($_POST["article_type_attribute_id"]); $i++)
		{
			$option_id = $_POST["article_type_attribute_id"][$i];
			if(!empty($option_id))
			{
				$sql = "INSERT INTO mk_style_article_type_attribute_values 
				(product_style_id, article_type_attribute_value_id, is_active,created_on, created_by,updated_on,updated_by) 
				VALUES('$styleid','$option_id',1,'$time','$login','$time','$login')";
				func_query($sql);
			}
		}

		//Audit style's price, status, comments and images
		$stylePriceAudit = array(
					"entityId" => $sports_query_data['style_id'],
					"entityName" => 'com.myntra.StyleProperties',
					"entityProperty" => 'Price',
					"entityPropertyOldValue" => '0',
					"entityPropertyNewValue" => $query_data['price'],
					"operationType" => 'INSERT');
		
		//auditing status
		$newStatus = func_query_first_cell("select style_status_name_to from style_status_transition_rules where style_status_code_to = '".$query_data['styletype']."'", true);
		$styleStatusAudit = array(
				"entityId" => $sports_query_data['style_id'],
				"entityName" => 'com.myntra.StyleProperties',
				"entityProperty" => 'Status',
				"entityPropertyOldValue" => 'NotAdded',
				"entityPropertyNewValue" => $newStatus,
				"operationType" => 'INSERT');
		//auditing comments
		$styleCommentsAudit = array(
				"entityId" => $sports_query_data['style_id'],
				"entityName" => 'com.myntra.StyleProperties',
				"entityProperty" => 'Comments',
				"entityPropertyOldValue" => 'No Comments',
				"entityPropertyNewValue" => $sports_query_data['comments'],
				"operationType" => 'INSERT');
				
		$uploadedImages = getUploadedImageType($sports_query_data);
		//auditing image uploading
		$styleImageAudit = array(
				"entityId" => $sports_query_data['style_id'],
				"entityName" => 'com.myntra.StyleProperties',
				"entityProperty" => 'Image Upload',
				"entityPropertyOldValue" => 'Image Upload',
				"entityPropertyNewValue" => "Uploaded $uploadedImages images",
				"operationType" => 'Image Upload');
		$styleAuditData = array($stylePriceAudit, $styleStatusAudit, $styleCommentsAudit, $styleImageAudit);
		$auditResult = AuditApiClient::sendStyleAuditInfo($styleAuditData, $login,'com.myntra.admin.productstyle_action_new.php');
		
		$group_names=getAllFilterGroups();
		$applied_filters=array();
		foreach($group_names as $id=>$group)
		{
			$applied_filters=implode(",",$_POST[$group."_filters"]);
			$filter_data=array("generic_type"=>"style",
					"generic_id" => $styleid,
					"filter_group_id" =>$id,
					"applied_filters" => $applied_filters,
					"is_active" => 1
					);
			func_array2insert("mk_applied_filters", $filter_data);
		}
	        
        // Here we have to get the options
        if(isset($_POST['size_option_check']) && $_POST['size_option_check'] ==1)
        {
            for ($i = 0; $i < count($_POST["size_option_value"]); $i++)
            {
                $option_value = $_POST["size_option_value"][$i];
                $option_name = $_POST['size_option_name'][$i];
				$sku_code_value = trim($_POST['sku_code_value'][$i]);
				if($applySizeScaleRestriction)
				{
					$option_size_value_id = $_POST['attr_type_size_value'][$i];//get the scale value id
				}
                $option_id = '';
                $sku_id = MyntraStyle::getSkuId($sku_code_value);
			
                if(!empty($option_name))
                {
                	$productOptionsArray = array(
                			"style" => $styleid,
                			"name" => $option_name,
                			"value" => $option_value,
                			"is_active" => 1
                			);
                	if($applySizeScaleRestriction)
                	{
                		$productOptionsArray["id_size_chart_scale_value_fk"] = $option_size_value_id;
                	}
                    $option_id = func_array2insert("mk_product_options", $productOptionsArray);
                }

                // now insert the row in the mapping table
                func_array2insert("mk_styles_options_skus_mapping",
                    array("style_id" => $styleid, "option_id" => $option_id, "sku_id" => $sku_id));
            }
        }
        else
        {
            $option_value = "Freesize";
            $option_name = "Size";
            $sku_code_value= trim( $_POST['sku_code_freesize']);
            $sku_id = MyntraStyle::getSkuId($sku_code_value);
			$productOptionsArray = array(
					"style" => $styleid,
					"name" => $option_name,
					"value" => $option_value,
					"is_active" => 1
					);
            $option_id = func_array2insert("mk_product_options", $productOptionsArray);
            // now insert the row in the mapping table
            func_array2insert("mk_styles_options_skus_mapping",
                array("style_id" => $styleid, "option_id" => $option_id, "sku_id" => $sku_id));

        }
        //adding the style to discount if any
        DiscountEngine::asignDiscountToStyle($styleid);
        //add display category
		updateDisplayCategoryForStyle($style_id);
        //add styles to index
        addStyleToSolrIndex($styleid,false);
        
        //for non-customizable style adding default customization area because of existing customization structure
        if($is_customizable == '0')
        {
            $area_id=func_array2insert(
                               "mk_style_customization_area",
                                array(
                                "style_id" => $styleid,
                                "name" => "default",
                                "label" => "default",
                                "description" => "default",
                                "isprimary" => true,
                                "default_image" => 'skin1/images/spacer.gif',
                                "image_t" => 'skin1/images/spacer.gif',
                                "image_l"  => 'skin1/images/spacer.gif',
                                "bkgroundimage" => 'skin1/images/spacer.gif',
                                "image_z" => 'skin1/images/spacer.gif'
                                )
                        );
            func_array2insert(
					"mk_customization_orientation",
					array(
					"location_name" => "default",
					"start_x" => 116,
				    "start_y" => 80,
				    "size_x" => 156,
				    "size_y" => 192,
				    "height_in" => 16,
				    "width_in" => 13,
				    "area_id" => $area_id 	,
					"ort_default" => 'Y',
					"bgstart_x" => 89,
					"bgstart_y" => 58,
					"bgsize_x"=> 99,
					"bgsize_y"=> 121,
					"bgheight_in"=> 16,
					"bgwidth_in"=> 13,
					"action"=> "customize your product",
					"typeid"=> 1,
					"imageAllowed"=> 0,
					"imageCustAllowed"=> 0,
					"textAllowed"=> 0,
					"textCustAllowed"=> 0,
					"textLimitation"=> 0,
					"textLimitationTpl"=> 0

				)
			);

        }
        else
        {
            /* Upload thumbnail and detail image for customization area */
                    if(!isset($_FILES) && isset($HTTP_POST_FILES))
                    {
                    	$_FILES = $HTTP_POST_FILES;
                    }

                    if(!empty($_FILES['img1']['name']))
                    {
                        $th_img ='images/style/carea/T/'.$_FILES['img1']['name'];
                        $det_img ='images/style/carea/L/'.$_FILES['det_img']['name'];
                        $uploaddir1 = '../images/style/carea/T/';
                        $uploaddir2 = '../images/style/carea/L/';
                        $uploadfile1 = $uploaddir1 . basename($_FILES['img1']['name']);
                        $uploadfile2 = $uploaddir2 . basename($_FILES['det_img']['name']);
                        if(@move_uploaded_file($_FILES['img1']['tmp_name'], $uploadfile1))
                        {
                            define("THMIMAGE",$th_img);
                        }
                        if(@move_uploaded_file($_FILES['det_img']['tmp_name'], $uploadfile2))
                        {
                            define("DETIMAGE",$det_img);
                        }
                    }
                    if(!empty($_FILES['preview_img']['name']))
                    {
                        $preview_img ='images/style/carea/L/'.$_FILES['preview_img']['name'];
                        $uploaddir_prw = '../images/style/carea/L/';
                        $uploaddir_prw = $uploaddir_prw . basename($_FILES['preview_img']['name']);
                        if(@move_uploaded_file($_FILES['det_img']['tmp_name'], $uploaddir_prw))
                        {
                            define("PRWIMAGE",$preview_img);
                        }
                    }
                    if(!empty($_FILES['zoom_img']['name']))
                    {
                        $preview_img ='images/style/carea/L/'.$_FILES['zoom_img']['name'];
                        $uploaddir_prw = '../images/style/carea/L/';
                        $uploaddir_prw = $uploaddir_prw . basename($_FILES['zoom_img']['name']);
                        if(@move_uploaded_file($_FILES['zoom_img']['tmp_name'], $uploaddir_prw))
                        {
                            define("ZOOMIMAGE",$preview_img);
                        }
                    }

           $area_id=func_array2insert(
				   "mk_style_customization_area",
					array(
				    "style_id" => $styleid,
					"name" => $area_name,
					"label" => $area_label,
					"description" => $desc,
			        "isprimary" => (!empty($primary) ? true : false),
					"default_image" => DETIMAGE,
				    "image_t" => THMIMAGE,
				    "image_l"  => DETIMAGE,
				    "bkgroundimage" => PRWIMAGE,
                    "image_z" => ZOOMIMAGE

				)
			);
            func_array2insert(
					"mk_customization_orientation",
					array(
					"location_name" => $ort_name,
					"start_x" => (!empty($startx) ? $startx : 0),
				    "start_y" => (!empty($starty) ? $starty : 0),
				    "size_x" => (!empty($sizex) ? $sizex : 0),
				    "size_y" => (!empty($sizey) ? $sizey : 0),
				    "height_in" => (!empty($height_in) ? $height_in : 0),
				    "width_in" => (!empty($width_in) ? $width_in : 0),
				    "area_id" => $area_id 	,
					"ort_default" => $default,
					"bgstart_x" => $bgstartx,
					"bgstart_y" => $bgstarty,
					"bgsize_x"=> $bgsizex,
					"bgsize_y"=> $bgsizey,
					"bgheight_in"=> $bgheight_in,
					"bgwidth_in"=> $bgwidth_in,
					"action"=> $action,
					"typeid"=> $typeid,
					"imageAllowed"=> (!empty($imageAllowed) ? $imageAllowed : 0),
					"imageCustAllowed"=> (!empty($imageCustAllowed) ? $imageCustAllowed : 0),
					"textAllowed"=> (!empty($textAllowed) ? $textAllowed : 0),
					"textCustAllowed"=> (!empty($textCustAllowed) ? $textCustAllowed : 0),
					"textLimitation"=> (!empty($textLimitation) ? $textLimitation : 0),
					"textLimitationTpl"=> (!empty($textLimitationTpl) ? $textLimitationTpl : 0),
					"textFontColors"=> implode(",",($textFontColors)),
					"textColorDefault"=> (!empty($textColorDefault) ? $textColorDefault : 0)

				)
			);
            foreach($morecustomization as $key=>$v)
            {
                        if(!empty($_FILES['img1_'.$v]['name']))
                        {
	                        $th_img ='images/style/carea/T/'.$_FILES['img1_'.$v]['name'];
	                        $det_img ='images/style/carea/L/'.$_FILES['det_img_'.$v]['name'];
	                        $uploaddir1 = '../images/style/carea/T/';
	                        $uploaddir2 = '../images/style/carea/L/';
	                        $uploadfile1 = $uploaddir1 . basename($_FILES['img1_'.$v]['name']);
	                        $uploadfile2 = $uploaddir2 . basename($_FILES['det_img_'.$v]['name']);
	                        if(@move_uploaded_file($_FILES['img1_'.$v]['tmp_name'], $uploadfile1))
	                        {
	                            define("THMIMAGE",$th_img);
	                        }
	                        if(@move_uploaded_file($_FILES['det_img_'.$v]['tmp_name'], $uploadfile2))
	                        {
	                            define("DETIMAGE",$det_img);
	                        }
                    }
                    if(!empty($_FILES['preview_img_'.$v]['name']))
                    {
                        $preview_img ='images/style/carea/L/'.$_FILES['preview_img_'.$v]['name'];
                        $uploaddir_prw = '../images/style/carea/L/';
                        $uploaddir_prw = $uploaddir_prw . basename($_FILES['preview_img_'.$v]['name']);
                        if(@move_uploaded_file($_FILES['det_img_'.$v]['tmp_name'], $uploaddir_prw))
                        {
                            define("PRWIMAGE",$preview_img);
                        }
                    }
                    if(!empty($_FILES['zoom_img_'.$v]['name']))
                    {
                        $preview_img ='images/style/carea/L/'.$_FILES['zoom_img_'.$v]['name'];
                        $uploaddir_prw = '../images/style/carea/L/';
                        $uploaddir_prw = $uploaddir_prw . basename($_FILES['zoom_img_'.$v]['name']);
                        if(@move_uploaded_file($_FILES['zoom_img_'.$v]['tmp_name'], $uploaddir_prw))
                        {
                            define("ZOOMIMAGE",$preview_img);
                        }
                    }
                    $primary_new=$_POST["primary_".$v];
           $area_id_new=func_array2insert(
				   "mk_style_customization_area",
					array(
				    "style_id" => $styleid,
					"name" => $_POST["area_name_".$v],
					"label" =>$_POST["area_label_".$v],
					"description" => $_POST["desc_".$v],
			        "isprimary" => (!empty($primary_new) ? true : false),
					"default_image" => DETIMAGE,
				    "image_t" => THMIMAGE,
				    "image_l"  => DETIMAGE,
				    "bkgroundimage" => PRWIMAGE,
                    "image_z" => ZOOMIMAGE

				)
			);
            func_array2insert(
					"mk_customization_orientation",
					array(
					"location_name" => $_POST["ort_name_".$v],
					"start_x" => (!empty($_POST["startx_".$v]) ? $_POST["startx_".$v] : 0),
				    "start_y" => (!empty($_POST["starty_".$v]) ? $_POST["starty_".$v] : 0),
				    "size_x" => (!empty($_POST["sizex_".$v]) ? $_POST["sizex_".$v] : 0),
				    "size_y" => (!empty($_POST["sizey_".$v]) ? $_POST["sizey_".$v] : 0),
				    "height_in" => (!empty($_POST["height_in_".$v]) ? $_POST["height_in_".$v] : 0),
				    "width_in" => (!empty($_POST["width_in_".$v]) ? $_POST["width_in_".$v] : 0),
				    "area_id" => $area_id_new 	,
					"ort_default" => $_POST["default_".$v],
					"bgstart_x" => $_POST["bgstartx_".$v],
					"bgstart_y" => $_POST["bgstarty_".$v],
					"bgsize_x"=> $_POST["bgsizex_".$v],
					"bgsize_y"=> $_POST["bgsizey_".$v],
					"bgheight_in"=> $_POST["bgheight_in_".$v],
					"bgwidth_in"=> $_POST["bgwidth_in_".$v],
					"action"=> $_POST["action_".$v],
					"typeid"=> $_POST["typeid_".$v],
					"imageAllowed"=> (!empty($_POST["imageAllowed_".$v]) ? $_POST["imageAllowed_".$v] : 0),
					"imageCustAllowed"=> (!empty($_POST["imageCustAllowed_".$v]) ? $_POST["imageCustAllowed_".$v] : 0),
					"textAllowed"=> (!empty($_POST["textAllowed_".$v]) ? $_POST["textAllowed_".$v] : 0),
					"textCustAllowed"=> (!empty($_POST["textCustAllowed_".$v]) ? $_POST["textCustAllowed_".$v] : 0),
					"textLimitation"=> (!empty($_POST["textLimitation_".$v]) ? $_POST["textLimitation_".$v] : 0),
					"textLimitationTpl"=> (!empty($_POST["textLimitationTpl_".$v]) ? $_POST["textLimitationTpl_".$v] : 0),
					"textFontColors"=> implode(",",($_POST["textFontColors_".$v])),
					"textColorDefault"=> (!empty($_POST["textColorDefault_".$v]) ? $_POST["textColorDefault_".$v] : 0)

				)
			);
            }

        }
		for($i=0; $i<count($stylecatmap); $i++)
		{
			$categoryid = $stylecatmap[$i];
			$sql = "INSERT INTO $sql_tbl[style_category_map](styleid, categoryid) VALUES('$styleid', '$categoryid')";
			db_query($sql);
		}
//		updateDisplayCategoryForStyle($style_id);
		doSizeUnification("ClearAll", $styleid);
		doSizeUnification("UpdateAll", $styleid);
        func_header_location($http_location . "/admin/productstyle_action_new.php?mode=modifystyle&style_id=".$styleid."&msg=added");
    }
    else if($_POST['mode'] == 'updatestyle')
    {
        $style_properties=getAllStyleProperties($style_id);
        $productstyle = getProductStyle($style_id);
        $default_image_url = empty($default_image_url) ? $style_properties["default_image"] : $default_image_url;
        $front_image_url = empty($front_image_url) ? $style_properties["front_image"] : $front_image_url;
        $back_image_url = empty($back_image_url) ? $style_properties["back_image"] : $back_image_url;
        $left_image_url = empty($left_image_url) ? $style_properties["left_image"] : $left_image_url;
        $right_image_url = empty($right_image_url) ? $style_properties["right_image"] : $right_image_url;
        $top_image_url = empty($top_image_url) ? $style_properties["top_image"] : $top_image_url;
        $bottom_image_url = empty($bottom_image_url) ? $style_properties["bottom_image"] : $bottom_image_url;
        $size_chart_image_url = empty($size_chart_image_url) ?  $productstyle['size_chart_image'] : $size_chart_image_url;
        if($size_chart_image_url != "")
        {
        	$sizeChartFileExt = strtolower(substr($size_chart_image_url, -4));
        	if($sizeChartFileExt != ".jpg")
        	{
        		$size_chart_image_url = "";
        	}
        }
        $login=$XCART_SESSION_VARS['identifiers']['A'][login];
        
        //Audit style's price, status and comments
        $stylePriceAudit = array(
        		"entityId" => $style_id,
        		"entityName" => 'com.myntra.StyleProperties',
        		"entityProperty" => 'Price',
        		"entityPropertyOldValue" => $productstyle[0]['price'],
        		"entityPropertyNewValue" => $price,
        		"operationType" => 'UPDATE');
        
        $oldStatusCode = $productstyle[0]['styletype'];
        $oldStatus = func_query_first_cell("select style_status_name_to from style_status_transition_rules where style_status_code_to = '$oldStatusCode'", TRUE);
        $newStatus = func_query_first_cell("select style_status_name_to from style_status_transition_rules where style_status_code_to = '$styletype'", TRUE);
        $styleStatusAudit = array(
        		"entityId" => $style_id,
        		"entityName" => 'com.myntra.StyleProperties',
        		"entityProperty" => 'Status',
        		"entityPropertyOldValue" => $oldStatus,
        		"entityPropertyNewValue" => $newStatus,
        		"operationType" => 'UPDATE');
        
        $styleCommentsAudit = array(
        		"entityId" => $style_id,
        		"entityName" => 'com.myntra.StyleProperties',
        		"entityProperty" => 'Comments',
        		"entityPropertyOldValue" => $style_properties['comments'],
        		"entityPropertyNewValue" => $style_comments,
        		"operationType" => 'UPDATE');
        
        $description = mysql_real_escape_string($_POST['product_desc']);
        $query_data = array(
					"product_type" => $product_type,
	   				"name" => trim($product_display_name),
					"label" => trim($product_display_name),
					"description" => $description,
					"price" => $price,
					"iventory_count"=> 100,
					"styletype" => $styletype,
					"material" =>'',
					"bulk" => 'Y',
					"style_code" => 'NPP',
					"default_store_markdown"	=> 0,
					"default_store_rate"		=> 0,
                    "size_chart_image"  => $size_chart_image_url,
					"is_customizable"		=> $is_customizable


			   );
        func_array2update("mk_product_style", $query_data, "id='$style_id' ");
        $search_images=explode(",",$default_image_url);

        /*Need to check if are editing any of the discount filter columns*/
        $oldStyle = func_query_first("select global_attr_brand, global_attr_season, global_attr_year, global_attr_fashion_type, global_attr_article_type from mk_style_properties where style_id='$style_id'", FALSE);
      
        $needToApplyDiscount = true;
        if($oldStyle['global_attr_brand']==$global_attr_brand && 
           $oldStyle['global_attr_season']==$global_attr_season && 
           $oldStyle['global_attr_year']==$global_attr_year && 
           $oldStyle['global_attr_fashion_type']==$global_attr_fashion_type && 
           $oldStyle['global_attr_article_type']==$global_attr_article_type ) {
            $needToApplyDiscount = false;
        }
        
        $productTags = str_replace(" ,", ",", str_replace(", ", ",", trim($product_tags)));
        $styleNote = mysql_real_escape_string($_POST['styleNote']);
        $materialsCareDesc = mysql_real_escape_string($_POST['materialsCareDesc']);
        $sizeFitDesc = mysql_real_escape_string($_POST['sizeFitDesc']);
        
         $sports_query_data = array(
                    "article_number" 			=> $article_number,
                    "product_tags"				=> $productTags,
                    "product_display_name"		=> trim($product_display_name),
                    "variant_name" 				=> trim($article_name),
                    "discount_type" 			=> $discount_type,
                    "discount_value"    		=> $discount_value,
                    "myntra_rating"		        => !empty($rating) ? $rating : 0,
                    "target_url"                => trim($target_url),
                    "top_image"                 => $top_image_url,
                    "front_image"               => $front_image_url,
                    "back_image"                => $back_image_url,
                    "left_image"                => $left_image_url,
                    "bottom_image"              => $bottom_image_url,
                    "right_image"               => $right_image_url,
         			"tex_color_image"			=> $tex_color_image_url,
                    "global_attr_article_type"  => $global_attr_article_type,
                    "global_attr_sub_category"  => $global_attr_sub_category,
                    "global_attr_master_category"=>$global_attr_master_category,
                    "global_attr_brand"         => $global_attr_brand,
                    "global_attr_age_group"     => $global_attr_age_group,
                    "global_attr_gender"        => $global_attr_gender,
                    "global_attr_base_colour"   => $global_attr_base_colour,
                    "global_attr_colour1"       => $global_attr_colour1,
                    "global_attr_colour2"       => $global_attr_colour2,
                    "global_attr_fashion_type"  => $global_attr_fashion_type,
                    "global_attr_season"        => $global_attr_season,
                    "global_attr_year"          => $global_attr_year,
                    "global_attr_usage"         => $global_attr_usage,
        			"style_note"                => $styleNote,
        			"materials_care_desc"       => $materialsCareDesc,
        			"size_fit_desc"             => $sizeFitDesc,
         			"comments"                  => $style_comments

         );
         //check if the status is active and catalog_date is null then set catalog date to current time
        if($query_data["styletype"] == 'P')
        {
                //check if catalog date is null
                $catalog_date = func_query_first_cell("select catalog_date from mk_style_properties where style_id = $style_id", FALSE);
                if(is_null($catalog_date))
                {
                        $sports_query_data["catalog_date"] = time();
                }
        }
         
         if(isset($update_recency)&&($update_recency=='true'||$update_recency=='on'))
         {
         	$sports_query_data['add_date']=time();
         }
         if(!empty($search_images[0]))
         {
         	$sports_query_data["default_image"]=$search_images[0];
         }
         if(!empty($search_images[1]))
         {
         	$sports_query_data["search_zoom_image"]=$search_images[1];
         }
         if(!empty($search_images[2]))
         {
         	$sports_query_data["search_image"]=$search_images[2];
         }
         global $sqllog;
         $sqllog->debug("User: $login ; Style id = $style_id");
         $sqllog->debug($sports_query_data);
         func_array2update("mk_style_properties", $sports_query_data, "style_id='$style_id' ");
         
         $uploadedImages = getUploadedImageType($sports_query_data);
         //auditing image uploading
         $styleImageAudit = array(
         		"entityId" => $style_id,
         		"entityName" => 'com.myntra.StyleProperties',
         		"entityProperty" => 'Image Upload',
         		"entityPropertyOldValue" => 'Image Upload',
         		"entityPropertyNewValue" => "Uploaded $uploadedImages images",
         		"operationType" => 'Image Upload');
         $styleAuditData = array($stylePriceAudit, $styleStatusAudit, $styleCommentsAudit, $styleImageAudit);
         $auditResult = AuditApiClient::sendStyleAuditInfo($styleAuditData, $login,'com.myntra.admin.productstyle_action_new.php');

		// Update article_type specific attributes to db
		// first delete the existing mappings
		$sql = "DELETE FROM mk_style_article_type_attribute_values where product_style_id=$style_id";
		func_query($sql);
		$time=time();	
		$login=$XCART_SESSION_VARS['identifiers']['A'][login];
		for($i = 0; $i < count($_POST["article_type_attribute_id"]); $i++)
		{
			$option_id = $_POST["article_type_attribute_id"][$i];
			if(!empty($option_id))
			{
				$sql = "INSERT INTO mk_style_article_type_attribute_values 
					(product_style_id, article_type_attribute_value_id, is_active,created_on, created_by,updated_on,updated_by) 
					VALUES('$style_id','$option_id',1,'$time','$login','$time','$login')";
				func_query($sql);
			}
		}
		
		$group_names=getAllFilterGroups();
        $applied_filters=array();
	
        foreach($group_names as $id=>$group)
        {
			$applied_filters=implode(",",$_POST[$group."_filters"]);
			$check=func_query("select * from mk_applied_filters where generic_id='".$style_id."' and filter_group_id='".$id."'", TRUE);	
			if(!empty($check[0]))
			{		
				func_query("update mk_applied_filters set applied_filters='".$applied_filters."' where generic_id='".$style_id."' and filter_group_id ='".$id."'");
			}
			else
			{
				$filter_data=array("generic_type"=>"style",
                                        "generic_id" => $style_id,
                                        "filter_group_id" =>$id,
                                        "applied_filters" => $applied_filters,
                                        "is_active" => 1
                                        );
				func_array2insert("mk_applied_filters",$filter_data);	
			}
        }
         //add style options
         
        // If the id is not present then we have to insert else do the update
        global $weblog;
        $weblog ->info($_POST["size_option_value"]);
        for($i = 0; $i < count($_POST["size_option_value"]); $i++)
        {
        	$option_value = $_POST["size_option_value"][$i];
        	$option_name = $_POST['size_option_name'][$i];
        	$option_value = trim($option_value);
        	if($applySizeScaleRestriction)
        	{
        		$option_size_value_id = $_POST["attr_type_size_value"][$i];
        	}
        	//enabling/disabling option when sku is enabled/disabled
        	$sku_code_value= trim( $_POST['sku_code_value'][$i]);
            $sku_id = MyntraStyle::getSkuId($sku_code_value);
        	$option_id = '';
        	if(!empty($option_name) && $option_name == 'Size')
        	{
	        	if(empty($_POST['option_id'][$i]))
	        	{
	        		$productOptionsArray = array(
	        								"style" => $style_id,
	        								"name" => $option_name,
	        								"value" => $option_value,
	        								"is_active" => 1
	        				 				);
	        		if($applySizeScaleRestriction)
	        		{
	        			$productOptionsArray["id_size_chart_scale_value_fk"] = $option_size_value_id;
	        		}
	        		$option_id = func_array2insert("mk_product_options", $productOptionsArray);

					// Here we need to check if there is sku_id 
		        	if(!empty($_POST['sku_code_id'][$i]))
		        	{
		        		// need to update the styles_options_skus mapping
		        		$sku_id = $_POST['sku_code_id'][$i];
		        		$sku_id = trim($sku_id);
		        		$sql = "UPDATE mk_styles_options_skus_mapping SET option_id = $option_id 
		        					WHERE style_id = $style_id AND sku_id = $sku_id";
		        		
		        		func_query($sql);
		        	}
	        	}
	        	else
	        	{
	        		$option_id = $_POST['option_id'][$i];//($tbl, $arr)
	        		$productOptionsArray2Up = array(
	        							"name" => $option_name,
	        							"value" => $option_value,
	        					        "id_size_chart_scale_value_fk" => 'NULL'			
	        							);
	        		if($applySizeScaleRestriction)
	        		{
	        			$productOptionsArray2Up["id_size_chart_scale_value_fk"] = $option_size_value_id;
	        		}
	        		//$updateProductOption = func_array2updateWithTypeCheck("mk_product_options", $productOptionsArray2Up, "id = '$option_id'");
	        		
					$sql = "UPDATE mk_product_options SET name = '$option_name', value='$option_value', id_size_chart_scale_value_fk= Null WHERE id = '$option_id'";
					$null = "Null";
					if($applySizeScaleRestriction)
					{
						$sql = "UPDATE mk_product_options SET name = '$option_name', value='$option_value', id_size_chart_scale_value_fk= '$option_size_value_id' WHERE id = '$option_id'";
					}
					if(!empty($_POST['sku_code_id'][$i]))
					{
		        		// need to update the styles_options_skus mapping
		        		$sku_id = $_POST['sku_code_id'][$i];
		        		$sku_id = trim($sku_id);
		        		$extra_info = array();
						if($option_enabled)
						{
							$value = 'true';
							$extra_info['reason']=getReasonByCode("ISAE");
						}
						else
						{
							$value = 'false';
							$extra_info['reason']=getReasonByCode("OSAD");
						}
						$extra_info['reasonid'] = $extra_info['reason']['id'];
						$extra_info['user'] = "AUTOSYSTEM";
						$extra_info['value'] = $value;
						updateSKUPropertyWMS($sku_id, 'enabled', $value, "AUTOSYSTEM", $extra_info, false, false);
					}
					func_query($sql);
	        	}
        	}
        	else if(empty($option_name) || $option_name == 'None')
        	{
        		// Check for the option_id is not empty from request
        		if(!empty($_POST['option_id'][$i]))
        		{
        			$sql2 = "DELETE FROM mk_styles_options_skus_mapping WHERE option_id = " . $_POST['option_id'][$i];
        			func_query($sql2);
        		}
        	}
			$weblog ->info($_POST['sku_enabled'][$i]);
			if(empty($_POST['sku_code_id'][$i]))
			{
	        	// now insert the row in the mapping table
	        	func_array2insert("mk_styles_options_skus_mapping", array("style_id" => $style_id, "option_id" => $option_id, "sku_id" => $sku_id));
			}
			else
			{
				if(!empty($option_id))
				{
					$sku_code_value= trim( $_POST['sku_code_value'][$i]);
					$skuDetail=SkuApiClient::getSkuDetailsForSkuCode($sku_code_value);
					$sql="UPDATE mk_styles_options_skus_mapping SET style_id=$style_id , option_id=$option_id,sku_id=".$skuDetail[0]['id']." where id=".$_POST['option_mapping_id'][$i];
					db_query($sql);
				}

			}
        }
        
		$stylecatmapdelete = "DELETE FROM $sql_tbl[style_category_map] WHERE styleid='$style_id'";
		db_query($stylecatmapdelete);
	    for($i=0; $i<count($stylecatmap); $i++)
		{
			$categoryid = $stylecatmap[$i];
			$catid = func_query_first_cell("SELECT categoryid FROM $sql_tbl[style_category_map] WHERE styleid='$style_id' AND categoryid='$categoryid'", TRUE);	
			if(!$catid)
			{
				$sql = "INSERT INTO $sql_tbl[style_category_map](styleid, categoryid) VALUES('$style_id', '$categoryid')";
				db_query($sql);
			}
		}
		//add display category
		updateDisplayCategoryForStyle($style_id);

        if($needToApplyDiscount){
            DiscountEngine::asignDiscountToStyle($style_id);
        }
                
        //add styles to index if styletype is 'P' else delete from index 
        if($styletype == 'P')
        {
        	addStyleToSolrIndex($style_id,false);
        }
        else
        {
        	deleteStyleFromSolrIndex($style_id,false);
        }
        
        doSizeUnification("ClearAll", $style_id);
        doSizeUnification("UpdateAll", $style_id);
        func_header_location($http_location . "/admin/productstyle_action_new.php?mode=modifystyle&style_id=".$style_id."&msg=updated");
    }
}
if(!empty($_GET['mode']) && $_GET['mode'] == 'modifystyle')
{
    $data=$_GET['style_id'];
    $productstyle = getProductStyle($data);
    $style_properties=getAllStyleProperties($data);
    
    $imagepath = getImage($data);
    $productoptions = getProductOption($data);
    $skus = getSkus($data);
    $p_options='';
	if($applySizeScaleRestriction)
    {
    	$articleTypeId = $style_properties['global_attr_article_type'];
    	$sizeScales = func_query("SELECT id, scale_name FROM size_chart_scale WHERE id_catalogue_classification_fk= '$articleTypeId'", TRUE);//get size scales
    	$optionScaleId = ''; //scale id corresponding to the scale used in this style/product options
    	$oneOfOptionSizeValueId = '';//used for finding the scale id using the size id stored in product options
    	$sizeChartValues = ''; //sizes corresponding to scale used in this style/product options
    	foreach($productoptions as $options)
    	{
    		$oneOfOptionSizeValueId = $options['id_size_chart_scale_value_fk'];
    	}
    	$optionScaleId = 0;
    	if(!is_null($oneOfOptionSizeValueId))
    	{
    		$optionScaleId = func_query_first_cell("SELECT id_size_chart_scale_fk FROM size_chart_scale_value WHERE id = '$oneOfOptionSizeValueId'", TRUE);
    		$sizeChartValues = func_query("SELECT id, size_value, intra_scale_size_order FROM size_chart_scale_value WHERE id_size_chart_scale_fk = '$optionScaleId' ORDER BY intra_scale_size_order asc", TRUE);
    	}
    }
    foreach($productoptions as $options)
    {
        $p_options[]= $options['value'];
    }
    
    $p_options_str=implode(",",$p_options);
    $selected_filters='';
    $result=func_query("select * from mk_applied_filters where generic_id='".$data."' and generic_type='style'", TRUE);
    foreach($result as $key=>$row)
    {
       if(!empty($row['applied_filters']))
       {
           if($key ==0)
               $selected_filters= $row['applied_filters'];
           else
               $selected_filters.= ",".$row['applied_filters'];               
       }
    }
	$selected_filters=explode(",",$selected_filters);
    $other_images_array=array($style_properties['front_image'],$style_properties['back_image'],$style_properties['top_image'],$style_properties['left_image'],$style_properties['right_image'],);
    $custumization = getProductCust($data);
    if($applySizeScaleRestriction)
    {
	    $smarty->assign("optionScaleId", $optionScaleId);
	    $smarty->assign("sizeChartValues", $sizeChartValues);
	    $smarty->assign("sizeScales", $sizeScales);
    }
    $smarty->assign("selected_filters",$selected_filters);
    $smarty->assign("style_details",$productstyle);
    $smarty->assign("style_properties",$style_properties);
    $smarty->assign("other_images_array",$other_images_array);
    $smarty->assign("customization",$custumization);
    $smarty->assign("product_options",$p_options_str);
    $smarty->assign("skus",$skus);
    $results = db_query("select size_chart_image from mk_product_style where id = '".$data."'");
    $row = db_fetch_array($results) ;
    $sizechart_img = $row['size_chart_image'] ;
    $smarty->assign("sizechart_img",$sizechart_img);
}

$smarty->assign('applySizeScaleRestriction', $applySizeScaleRestriction);
if($applySizeScaleRestriction)
{
	$smarty->assign('applySizeScaleRestrictionPageMode', 'true');
}
else
{
	$smarty->assign('applySizeScaleRestrictionPageMode', 'false');
}
//get all style status
$allStyleStatus = json_decode(getStyleStatus(), 1);
$smarty->assign("allStyleStatus", $allStyleStatus);
$possibleStatusTrans = getPossibleStatusTransition();
$smarty->assign("possibleStatusTrans", $possibleStatusTrans);
$all_filters=getAllFilters();
$smarty->assign ("all_filters",$all_filters);
$smarty->assign("main","prd_style_new");
$product_type_groups=func_query("select * from mk_product_group order by display_order desc", TRUE);

$prd_type=array();
foreach($product_type_groups as $groups)
{
    $PrdTypes = func_query ("SELECT id, name FROM mk_product_type where type = 'P' and product_type_groupid=".$groups['id']." ORDER BY name ");
    $prd_type[$groups['name']]= $PrdTypes;
}

$smarty->assign("producttypes", $prd_type);
#
#Load subcategories of root category Product
#
$tb_categories=$sql_tbl["categories"];
$query = "select categoryid, category, categoryid_path FROM $tb_categories where categoryid 
				NOT IN (SELECT parentid FROM xcart_categories) AND avail='Y'";
//$result =	 db_query($query);
$temp_subcategories = func_query($query);
$subcategories= array();
$count=0;
foreach($temp_subcategories as $value)
{
	$mixed_str=str_replace('/',',',$value['categoryid_path']);
	$disabled_cate=func_query("SELECT COUNT(categoryid) as count_cat FROM $tb_categories WHERE categoryid IN($mixed_str) and avail='N'");
	if($disabled_cate[0]['count_cat']==0)
	{
		$subcategories[$count]=$value;
		$count++;
	}
	
}
$smarty->assign("subcategories",$subcategories);
$stylecatmapid = func_query("SELECT categoryid FROM $sql_tbl[style_category_map] WHERE styleid='$data'");
$stylemaparray = ($stylecatmapid)?$stylecatmapid:$stylecatmap;
$smarty->assign("stylemaparray",$stylemaparray);

# Assign the current location line
$smarty->assign("location", $location);
$smarty->assign("dialog_tools_data", $dialog_tools_data);
$customization_types_array = func_query("select typeid,name from customization_type", TRUE);
$customization_types = array();
foreach($customization_types_array as $key=>$value)
{
	$customization_types[$value[typeid]] = $value[name];
}
$font_results=func_query("select * from mk_text_font_color");

$smarty->assign("font_results",$font_results);
$smarty->assign("customization_types",$customization_types);
/*new catalog admin */
$article_type=func_query("select * from mk_catalogue_classification where parent1 !='-1' and parent2 !='-1' and is_active=1 order by typename asc",true);
$smarty->assign("article_types",$article_type);
$attributes_types=func_query("select * from mk_attribute_type where is_active=1",TRUE);
$attr_array=array();
foreach($attributes_types as $t)
{
    $attributes=func_query("select * from mk_attribute_type_values where is_active=1 and attribute_type='".$t['attribute_type']."' order by attribute_value asc ");
    foreach($attributes as $a)
    {
        $attr_array[str_replace(" ","",$t['attribute_type'])][]=$a['attribute_value'];
    }
}
//echo "<pre>";print_r($attr_array);die;
$smarty->assign("attributes",$attr_array);

if($_GET['msg'] && $_GET['msg'] == 'added')
{
	PortalRequestHandlerClient::refreshStyles(array($_GET['style_id']));
    $smarty->assign("success_msg","Product Added successfully");
}
else if($_GET['msg'] && $_GET['msg'] == 'updated')
{
	PortalRequestHandlerClient::refreshStyles(array($_GET['style_id']));
    $smarty->assign("success_msg","Product Updated successfully"); 
}
$smarty->assign("error_msg", $error_msg);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
