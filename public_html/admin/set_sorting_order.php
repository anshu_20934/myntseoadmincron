<?php
/**
 * Created by Nishith Shrivastav.
 * User: myntra
 * Date: Nov 13, 2010
 * Time: 7:53:54 PM
 */
#
# Purpose : This file is created to add, edit and delete priority ######
#

require "./auth.php";

require $xcart_dir."/include/security.php";
$location[] = array("Filter Groups", "");
define('FORM_ACTION','set_sorting_order.php');
if($REQUEST_METHOD == "POST"){

          $isError = false;
          switch($HTTP_POST_VARS['mode']){
          ############## Condition to add groups ############
          	/*case  "addgroup" :

                    if(empty($HTTP_POST_VARS['name'])){
                        $isError = true;
                    }

                    if(!$isError)
                    {
                        $query_data = array(
                            "name"  =>(!empty($HTTP_POST_VARS["name"]) ? trim($HTTP_POST_VARS["name"]) : ''),
                            "label"  =>(!empty($HTTP_POST_VARS["label"]) ? trim($HTTP_POST_VARS["label"]) : ''),
                            "priority"  =>(!empty($HTTP_POST_VARS["priority"]) ? trim($HTTP_POST_VARS["priority"]) : ''),
                            "display_order"  =>(!empty($HTTP_POST_VARS["display_order"]) ? $HTTP_POST_VARS["display_order"] : ''),
                            "is_active" =>  (!empty($HTTP_POST_VARS["is_active"]) ? '1' : '0')
                            ) ;

                        func_array2insert ('mk_solr_sprod_priority', $query_data);
                        $top_message["content"] = "Added new Priority.";
                    }
                    else
                    {
                        $top_message["content"] = "Error! Please input valid data.";
                        $top_message["anchor"] = "featured";
                    }

            break;*/
		   ########### Condition to update ###############
          	case "update":
			   if (is_array($posted_data))
               {

				    foreach ($posted_data as $action=>$value)
                    {
                        $query_data = array(
                            "weightage"  =>(!empty($value["weightage"]) ? trim($value["weightage"]) : '1'),
                            "cutoff_days"  =>(!empty($value["cutoff_days"]) ? $value["cutoff_days"] : '15'),
                        ) ;
                        func_array2update("mk_transients_config", $query_data, "action='$action'");
                    }
	                      $top_message["content"] = "Configuration changed successfully!!";
                          $top_message["anchor"] = "featured";

                }
                       break;

          ########### Condition to delete the record ###############
          	/*case  "delete" :
			   if (is_array($posted_data)) {
				    foreach ($posted_data as $p_id=>$v) {
						 if (empty($v["to_delete"]))
							  continue;
						 db_query("delete from mk_solr_sprod_priority where id ='".$p_id."'") ;
					}

				  $top_message["content"] = "Deleted!!";
	              $top_message["anchor"] = "featured";

	           }
	           break;

          ########### Condition to delete the record ###############
          case "modify":
		  	    if (is_array($posted_data)) {
				    foreach ($posted_data as $groupid=>$v) {
						 if (empty($v["to_delete"]))
							  continue;
					     $groupToModify =  $groupid;

					}

				  ##$top_message["content"] = func_get_langvar_by_name("msg_adm_prdstyle_upd");
	              ##func_header_location("admin_product_styles.php");

	           }
	           break;*/
        }  ####### End of switch case

}

$sql ="SELECT * FROM  mk_transients_config";
$records = func_query($sql);
$smarty->assign("transients",$records);

$smarty->assign("main","set_sorting_order");


# Assign the current location line
$smarty->assign("location", $location);
$smarty->assign("form_action", FORM_ACTION);
# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
