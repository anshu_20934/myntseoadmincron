<?php
require_once "../admin/auth.php";
require_once "../include/security.php";

$i=0;
$report_list[$i++] = array("name"=>"Online Catalog Report","link"=>"../admin/AdminReportViewer.php?report_name=/reports/ReportRenderer.php&format=html&prompt_filters=yes&action=genreport&report=1");
$report_list[$i++] = array("name"=>"Online Order Report","link"=>"../admin/AdminReportViewer.php?report_name=/reports/ReportRenderer.php&format=html&prompt_filters=yes&action=genreport&report=2");
$report_list[$i++] = array("name"=>"Online Customer Report","link"=>"../admin/AdminReportViewer.php?report_name=/reports/ReportRenderer.php&format=html&prompt_filters=yes&action=genreport&report=3");
$report_list[$i++] = array("name"=>"Customer Feedback Digest","link"=>"../admin/AdminReportViewer.php?report_name=/admin/feedback_report.php&format=html&prompt_filters=no&action=genreport");

$smarty->assign("report_list",$report_list);
$smarty->assign("main","reports_link_list");
func_display("admin/home.tpl",$smarty);