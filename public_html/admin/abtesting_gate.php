<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
include_once("$xcart_dir/include/class/class.ab_testing_variables.php");
include_once "$xcart_dir/include/dao/class/class.GenericDAO.php";

if($mode==='uploadjscss'){
	
	define('S3_BUCKET', SystemConfig::$amazonS3Bucket);
	define('CDN_BASE', HostConfig::$cdnBase);
	
	function gzip($src, $level = 5){
	    if (!file_exists($src)) {;
	        return '';
	    }
	    $tmp = sys_get_temp_dir();
	    $dst = tempnam($tmp, 'cdn_');
	    $src_handle = fopen($src, "r");
	    $dst_handle = gzopen($dst, "w$level");
	    while(!feof($src_handle)){
	        $chunk = fread($src_handle, 2048);
	        gzwrite($dst_handle, $chunk);
	    }
	    fclose($src_handle);
	    gzclose($dst_handle);
	
	    return $dst;
	}
	
	$jsFileSource=$HTTP_POST_FILES['ufileJs']['tmp_name'];
	$s3 = new S3(SystemConfig::$amazonAccessKey, SystemConfig::$amazonSecretKey);
	$jsError = false;
	$msg;
	if(!empty($HTTP_POST_FILES['ufileJs']['tmp_name'])){
		$s3path = "skin1/js/abtest/" . ($abtestname.time()) . '_js.jgz';
		$jsFileSource = gzip($jsFileSource);
		$headers = array("Content-Encoding"=>"gzip", "Content-Type"=>"text/javascript");
		$cdn_url = CDN_BASE . '/' .$s3path;
		$ret= $s3->putObjectFile($jsFileSource, S3_BUCKET, $s3path, S3::ACL_PUBLIC_READ, array(), $headers, 31536000);
		if (url_exists($cdn_url)) {
	    	$jsFileUrl=$s3path;
	    	unlink($jsFileSource);
		}else{
			$jsError=true;
			$msg="Error uploading js to cdn";
		}
	}
	$cssError = false;
	$cssFileSource=$HTTP_POST_FILES['ufileCss']['tmp_name'];
	if(!empty($HTTP_POST_FILES['ufileCss']['tmp_name'])){
		$s3path = "skin1/js/abtest/" . ($abtestname.time()) . '_css.jgz';
		$cssFileSource = gzip($cssFileSource);
		$headers = array("Content-Encoding"=>"gzip", "Content-Type"=>"text/css");
		$cdn_url = CDN_BASE . '/' . $s3path;
		$s3->putObjectFile($cssFileSource, S3_BUCKET, $s3path, S3::ACL_PUBLIC_READ, array(), $headers, 31536000);
		if (url_exists($cdn_url)) {
	    	$cssFileUrl=$s3path;
	    	unlink($cssFileSource);
		}else{
			$cssError=true;
			$msg="<br>Error uploading css to cdn";
		}
	}
	if($cssError || $jsError){
		echo json_encode(array('success'=>false,'uFileJs'=>$jsFileUrl,'uFileCss'=>$cssFileUrl,'msg'=>$msg));
		exit;
	}
	echo json_encode(array('success'=>true,'uFileJs'=>$jsFileUrl,'uFileCss'=>$cssFileUrl));
	exit;
}
if($mode==='get_available_filters'){
	//New FIlters addded should be added here
	$filters_Array[] = array('filter_name'=>'Myntra Employee','filter_class'=>'abtest\\filters\\impl\\MABMyntraUserFilter');
	$filters_Array[] = array('filter_name'=>'Myntra Admin','filter_class'=>'abtest\\filters\\impl\\MABAdminUserFilter');
	$filters_Array[] = array('filter_name'=>'Myntra Non Employee Users','filter_class'=>'abtest\\filters\\impl\\MABNonMyntraUserFilter');
	echo json_encode(array('searchResults'=>$filters_Array));
	exit;
}
if($mode==='get_seg_algos'){
	//New Algos added should be added here
	$algo_Array[] = array('algo_name'=>'Random Segmentation','algo_class'=>'abtest\\algos\\impl\\RandomSegmentationAlgo');
	$algo_Array[] = array('algo_name'=>'New User/Old User Random Segmentation','algo_class'=>'abtest\\algos\\impl\\NewUserOldUserRandomSegmentationAlgo');
	echo json_encode(array('searchResults'=>$algo_Array));
	exit;
}
if($mode=='auditlogs'){
	$sort=mysql_real_escape_string($sort);
	$dir=mysql_real_escape_string($dir);
	$start=mysql_real_escape_string($start);
	$limit=mysql_real_escape_string($limit);
	$abtestid=mysql_real_escape_string($abtestid);
	$adapter= \abtest\dao\MABDBAdapter::getInstance();
	if($abtestid==='global'){
		$result=func_query("select login,t.name test,lastmodified,details,comment from mk_abtest_audit_logs,mk_abtesting_tests t where t.id=abtestid order by $sort $dir limit $start,$limit",true);
		$count=func_query_first_cell("select count(*) from mk_abtest_audit_logs ",true);
	}else{
		$result=func_query("select login,t.name test,lastmodified,details,comment from mk_abtest_audit_logs ,mk_abtesting_tests t where t.id=abtestid  and abtestid='$abtestid' order by $sort $dir limit $start,$limit",true);
		$count=func_query_first_cell("select count(*) from mk_abtest_audit_logs where abtestid='$abtestid' ",true);
	}
	
	echo json_encode(array('auditlogs'=>$result,'count'=>"$count"));
	exit;
}else if($mode=='completetest'){
	$adapter= \abtest\dao\MABDBAdapter::getInstance();
	$r=$adapter->completeTest($abtest,$rolledvariation,$comment);
	
	if($r===true){
		echo json_encode(array('results'=>"success",'msg'=>"Successfully Completed !"));
	}else{
		echo json_encode(array('results'=>"error",'msg'=>"Not Updated !"));
	}
	exit;
}else if($mode=='saveupdatenew'){
	$adapter= \abtest\dao\MABDBAdapter::getInstance();
	$abtestObject=\abtest\dao\MABDBAdapter::convertToArrayRecursive(json_decode($_POST['abtest']));
	$comment=$_POST['comment'];
	unset($abtestObject[search]);
	try{
		$r=$adapter->addOrUpdateFromArray($abtestObject,$comment);
		if($r===true){
			echo json_encode(array('results'=>"success",'msg'=>"Successfully Updated !"));
		}else{
			echo json_encode(array('results'=>"error",'msg'=>"Not Updated !"));
		}
	}catch(Exception $ex){
		echo json_encode(array('results'=>"error",'msg'=>$ex->getMessage()));
	}
	exit;
}
$adapter= \abtest\dao\MABDBAdapter::getInstance();
$results=$adapter->fetchALLABTestArray();
foreach ($results as $testName=>$test){
	$var_str="$testName";
	foreach($test['variations'] as $variation){
		$var_str.=$variation['name'];
	}
	$results[$testName]['search']=$var_str;
}
$smarty->assign("abtests",  json_encode($results));
$smarty->assign("action_php","abtesting_gate.php?object=list");
$smarty->assign("main","abtesting_tests");
func_display("admin/home.tpl",$smarty);
