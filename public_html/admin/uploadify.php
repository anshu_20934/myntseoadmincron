<?php
/*
Uploadify v2.1.4
Release Date: November 8, 2010

Copyright (c) 2010 Ronnie Garcia, Travis Nickels

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
require "../include/SimpleImage.php";
include_once("../utils/imagetransferS3.php");

function resizeUploadedImage($org_file,$resized_file,$x,$y='',$imagetype,$newdir='')
{
	if($newdir =='')
		$p_upload_dir='../images/style/properties/';
	else
		$p_upload_dir='../images/style/'.$newdir.'/';
	$image = new SimpleImage();
	$image->load("../images/style/properties/".$org_file);
    //move all the images to amazon
	if($newdir =='')
		$backup_path="./images/style/properties/";
	else
		$backup_path="./images/style/".$newdir."/";
	if($y=='')
    {
        $image->resizeToWidth($x);
        $image->save($p_upload_dir.$resized_file."_images_".$x."_".$x.".".$imagetype);
        myntra\utils\cdn\imagetransferS3::backup($backup_path.$resized_file."_images_".$x."_".$x.".".$imagetype);
    }
	else
    {
        $image->resize($x,$y);
        $image->save($p_upload_dir.$resized_file."_images_".$x."_".$y.".".$imagetype);
        myntra\utils\cdn\imagetransferS3::backup($backup_path.$resized_file."_images_".$x."_".$y.".".$imagetype);
    }
}
if(isset($_POST['mode']) && $_POST['mode'] == 'uploadImage')
{
    $image_type=$_POST['imageType'];
    $p_upload_dir='../images/style/properties/';
    $upload_dir_db="./images/style/properties/";
    if($image_type == 'default')
    {
        $tempFile = $_FILES['Filedata']['tmp_name'];
        $img_arr=explode(".",$_FILES['Filedata']['name']);
        $file_prefix=md5(rand().time());
        $org_file=$file_prefix."_images.".$img_arr[1];
		
        if (@move_uploaded_file($tempFile, $p_upload_dir.$org_file))
        {
//for super zoom image
		resizeUploadedImage($org_file,$file_prefix,1080,1440,$img_arr[1]);
//intermediate zoom image
		resizeUploadedImage($org_file,$file_prefix,540,720,$img_arr[1]);
//product preview image
        resizeUploadedImage($org_file,$file_prefix,360,480,$img_arr[1]);
//for thumb image on detail page
		resizeUploadedImage($org_file,$file_prefix,48,64,$img_arr[1]);
//for search image
		resizeUploadedImage($org_file,$file_prefix,240,320,$img_arr[1],'style_search_image');
//for cart image
		resizeUploadedImage($org_file,$file_prefix,96,128,$img_arr[1]);

        }

        echo $upload_dir_db.$org_file.",'',./images/style/style_search_image/".$file_prefix."_images_240_320.".$img_arr[1];exit;
    }
    else if($image_type == 'others')
    {
        $tempFile = $_FILES['Filedata']['tmp_name'];
        $img_arr=explode(".",$_FILES['Filedata']['name']);
        $file_prefix=md5(rand().time());
        $org_file=$file_prefix."_images.".$img_arr[1];
        if (@move_uploaded_file($tempFile, $p_upload_dir.$org_file))
        {
            resizeUploadedImage($org_file,$file_prefix,1080,1440,$img_arr[1]);
            resizeUploadedImage($org_file,$file_prefix,540,720,$img_arr[1]);
            resizeUploadedImage($org_file,$file_prefix,360,480,$img_arr[1]);
            resizeUploadedImage($org_file,$file_prefix,48,64,$img_arr[1]);
        }
        echo $upload_dir_db.$org_file;exit;
    }
	else if($image_type == 'sizechart')
    {
		
		$s_upload_dir='../images/style/sizechartimages/';
		$s_upload_dir_db="./images/style/sizechartimages/";
        $tempFile = $_FILES['Filedata']['tmp_name'];
        $img_arr=explode(".",$_FILES['Filedata']['name']);
		if(!empty($_POST['imageName']))
		{
			$name=str_replace(" ","_",$_POST['imageName']);
			$filename = $s_upload_dir.$name.".".$img_arr[1];
			if (file_exists($filename)) {
				$file_prefix=$name."_".time();
			}
			else
				$file_prefix=$name;
		}
		else
			$file_prefix=md5(rand().time());
        $org_file=$file_prefix.".".$img_arr[1];
        if (@move_uploaded_file($tempFile, $s_upload_dir.$org_file))
        {
			echo $s_upload_dir_db.$org_file;exit;
        }
		else
		{
			echo "error";
			exit;
		}
        
    }
    else{

        $tempFile = $_FILES['Filedata']['tmp_name'];
        $img_arr=explode(".",$_FILES['Filedata']['name']);
        $file_prefix=md5(rand().time());
        $org_file=$file_prefix."_images.".$img_arr[1];
        if (@move_uploaded_file($tempFile, $p_upload_dir.$org_file))
        {
          echo $p_upload_dir_db.$org_file;exit;
        }
        else echo "error";
    }

}
?>
