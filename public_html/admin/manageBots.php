<?php 
require "./auth.php";
require $xcart_dir."/include/security.php";

if($mode=='add_bot') {
	if(!empty($botType) && !empty($botName)) {
		$insertBotQuery = "insert into mk_bots(bot_type,bot_name) values ('$botType' , '$botName')";
		db_query($insertBotQuery);
	}
}

elseif($mode=='remove_bot') {
	if(!empty($removeBots) && count($removeBots)>0) {
		$botids = implode(",", $removeBots);
		$deleteBotsQuery = "delete from mk_bots where id in($botids)";
		db_query($deleteBotsQuery);
	}
}

$sql = "SELECT * FROM mk_bots order by bot_type, bot_name";
$bots = func_query($sql);
$smarty->assign("bots",$bots);

$smarty->assign("main","manageBots");
func_display("admin/home.tpl",$smarty);
?>