<?php
require_once  "../auth.php";
require_once $xcart_dir.'/modules/RestAPI/RestRequest.php';

set_time_limit(0);
$url = HostConfig::$notificationsUrl.'/';
$method = 'POST';
$notification = array();
$notification['createdBy'] = $XCART_SESSION_VARS['login'];
$notification['title'] = $_REQUEST['ntfTitle'];
$notification['message'] = $_REQUEST['ntfMessage'];
$notification['image'] = $_REQUEST['ntfImageUrl'];
$notification['startDate'] = $_REQUEST['startDate'].'T00:00:00+05:30';
$notification['endDate'] = $_REQUEST['endDate'].'T00:00:00+05:30';

$userIds = array();
$userIdList = trim($_REQUEST['userIds']);
if (!empty($userIdList)) {
    $userIds = explode(',',$userIdList);
}
$groupId = $_REQUEST['userGroup'];
if($groupId != -1){
	$userIds = getGroupIds($groupId,$userIds);
}
$notification['userList'] = array();
for($i=0; $i<count($userIds); $i++){
	$notification['userList'][$i] = array();
	$notification['userList'][$i]['read'] = 'false';
	$notification['userList'][$i]['userId'] = $userIds[$i];
}
$data = array();
$data['Notification'] = $notification;
$data = json_encode($data);
$request = new RestRequest($url, $method, $data);
$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Content-type: application/json';
$headers[] = 'Accept-Language: en-US';
$request->setHttpHeaders($headers);
$request->execute();
$info = $request->getResponseInfo();
$body = $request->getResponseBody();
if($info['http_code'] == 200){
	$response = json_decode($body,true);
	if($response['NotificationResponse']['status']['statusType'] == 'SUCCESS'){
		if(strtotime($_REQUEST['startDate']) == strtotime(date('Y-m-d'))){
			bulkIncrementService($userIds);
			bulkNewNotificationIncrementService($userIds);
		}
		$XCART_SESSION_VARS['createNotification'] = true;
	}
}

function bulkIncrementService($userIds){
	$url = HostConfig::$notificationsUpsUrl.'/bulkIncrement';
	$method = 'POST';
	$data = getBulkIncrementJSON($userIds);
	$request = new RestRequest($url, $method, $data);
	$headers = array();
	$headers[] = 'Accept: application/json';
	$headers[] = 'Content-type: application/json';
	$headers[] = 'Accept-Language: en-US';
	$request->setHttpHeaders($headers);
	$request->execute();
	$info = $request->getResponseInfo();
	if($info['http_code'] == 200){
		return true;
	}
	return false;
}

function getBulkIncrementJSON($userIds){
	$jsonArr = array();
	for($i=0; $i<count($userIds); $i++){
		$jsonArr[$userIds[$i]] = array();
		$jsonArr[$userIds[$i]]['attribute'] = 'totalNotificationCounter';
		$jsonArr[$userIds[$i]]['sourceid'] = '3';
	}
	return json_encode($jsonArr);
}

function bulkNewNotificationIncrementService($userIds){
	$url = HostConfig::$notificationsUpsUrl.'/bulkIncrement';
	$method = 'POST';
	$data = getNewNotificationBulkIncrementJSON($userIds);
	$request = new RestRequest($url, $method, $data);
	$headers = array();
	$headers[] = 'Accept: application/json';
	$headers[] = 'Content-type: application/json';
	$headers[] = 'Accept-Language: en-US';
	$request->setHttpHeaders($headers);
	$request->execute();
	$info = $request->getResponseInfo();
	if($info['http_code'] == 200){
		return true;
	}
	return false;
}

function getNewNotificationBulkIncrementJSON($userIds){
	$jsonArr = array();
	for($i=0; $i<count($userIds); $i++){
		$jsonArr[$userIds[$i]] = array();
		$jsonArr[$userIds[$i]]['attribute'] = 'notificationCounter';
		$jsonArr[$userIds[$i]]['sourceid'] = '3';
	}
	return json_encode($jsonArr);
}

function getGroupIds($groupId,$userIds){
	$url = HostConfig::$userGroupServiceUrl.'/'.$groupId;
	$method = 'GET';
	$headers = array();
	$headers[] = 'Accept: application/json';
	$headers[] = 'Content-type: application/json';
	$headers[] = 'Accept-Language: en-US';
	$request = new RestRequest($url, $method);
	$request->setHttpHeaders($headers);
	$request->execute();
	$info = $request->getResponseInfo();
	$body = $request->getResponseBody();
	if($info['http_code'] == 200){
		$response = json_decode($body,true);
		if($response['groupResponse']['status']['statusType'] == 'SUCCESS'){
			if(count($response['groupResponse']['data']['UserGroup']['userGroupMapList'])){
				for($i=0; $i<count($response['groupResponse']['data']['UserGroup']['userGroupMapList']); $i++){
					array_push($userIds, $response['groupResponse']['data']['UserGroup']['userGroupMapList'][$i]['userId']);
				}
			}			
		}
	}
	return $userIds;
}
header('Location: /admin/notifications_create.php');
?>
