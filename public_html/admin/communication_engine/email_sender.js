Ext.onReady(function() {
	
	Ext.QuickTips.init();
	
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
	Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
	Ext.Ajax.on('requestexception', myMask.hide, myMask);

	var searchPanel = new Ext.form.FormPanel({
		renderTo : 'contentPanel',
		title : 'Emails {Allowed varibales are  [ORDER_ID]/[RETURN_ID],[FIRST_NAME],[LAST_NAME],[TRACKING_NUMBER] }',
		id : 'formPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 2},
			defaults : { border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [  {
				items : [{
		        	xtype : 'textarea',
		        	id:'ids',
		        	fieldLabel:'Data',
		        	width:550
		        	
				}]
			},  {
				items: []
			},{
	           items: [{ // Use the default, automatic layout to distribute the controls evenly
	            // across a single row
	            xtype: 'radiogroup',
	            fieldLabel: 'Filter Type',
	            id: 'filtertype',
	            //cls: 'x-check-group-alt',
	            items: [
	                {boxLabel: 'Order Id', name: 'cb-auto-1',id:'orderid', checked:true},
	                {boxLabel: 'Return Id', name: 'cb-auto-1',id:'returnid'}
	               ]}]
	        },  {
				items: []
			},{
				items :[{
					xtype : 'textfield',
		        	id : 'subject',
		        	fieldLabel : 'Subject',
		        	width : 550
				}]
			}, {
				items: []
			},{
				items :[{
					xtype : 'htmleditor',
		        	id : 'body',
		        	fieldLabel : 'Body',
		        	width : 550,
		        	height: 300
		        	
				}]
			}]
		}],
		buttons: [{
			text: 'Send',
			handler : function()
			{
				send();
			}
		}]
	});

	
	function saveTemplate()
	{
		
	}
	
	
	function send()
	{
		var params = getSearchPanelParams();
		var form = searchPanel.getForm();
		Ext.Ajax.request({
			url:'emailSenderAjax.php',
			params:{"action":"preview","ids":params["ids"],"subject":params["subject"],"body":params["body"],"filterid":params["filterid"]},
			success: function(response,action)
			{
				alert("Sent");
			}
				
			
		});
		
	}
	
	
	function getSearchPanelParams()
	{
		//alert("e");
		var form = searchPanel.getForm();
		
		if(form.isValid())
		{
			var params = {};
			params["ids"] = form.findField("ids").getValue();
			params["subject"] = form.findField("subject").getValue();
			params["body"] = form.findField("body").getValue();
			
			var count = form.findField("filtertype").items.items.length;
			for(var i=0;i<count;i++)
			{
				var it = form.findField("filtertype").items.items[i];
				if(it.checked)
					{
						params["filterid"]=it.id;
					}
			}
			return params;
		}
		else
		{
			
			return false;
		}
	}
	
   mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
		      searchPanel
		]
	});
});
