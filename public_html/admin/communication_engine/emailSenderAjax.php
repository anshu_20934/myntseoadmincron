<?php
require '../auth.php';
require_once $xcart_dir."/include/func/func.order.php";
require_once($xcart_dir."/modules/apiclient/NotificationApiClient.php");
require_once($xcart_dir."/include/func/func.returns.php");

global $weblog;
$return = array();
if($_POST['action'] == 'preview'){
	// get the comma seperated list of order ids
	$idlist = $_POST['ids'];
	$subject = trim($_POST['subject']);
	$body = trim($_POST['body']);
	$filtertype = trim($_POST['filterid']);

    if($filtertype == 'orderid')   {
		$orderArray = explode(",", $idlist);
		foreach($orderArray as $orderId){
    		$tempBody = $body;
    		$tempSubject = $subject;
	    	// get order related data
	    	$data = func_order_data(trim($orderId));
	    	//replacing variables in body
	   		$tempBody = str_replace('[FIRST_NAME]',$data['order']['s_firstname'],$tempBody);
	   		$tempBody = str_replace('[LAST_NAME]',$data['order']['s_lastname'],$tempBody);
	   		$tempBody = str_replace('[TRACKING_NUMBER]',$data['order']['tracking'],$tempBody);
	    	$tempBody = str_replace('[ORDER_ID]', $orderId, $tempBody);
			$tempBody = htmlentities($tempBody);
			// replace in subject
	    	$tempSubject = str_replace('[ORDER_ID]',$orderId,$tempSubject);
	    	
	    	include_once($xcart_dir."/include/class/class.mail.multiprovidermail.php");
	    	$customKeywords = array_merge($subjectargs, $args);
	        $mailDetails = array(
	            "subject" => $tempSubject,
	            "content" => $tempBody,
	            "to" => $data['order']['login'],
	            "bcc" => "myntramailarchive@gmail.com",
	            "header" => 'header',
	            "footer" => 'footer',
	            "mail_type" => \MailType::CRITICAL_TXN
	        );
	        $multiPartymailer = new \MultiProviderMailer($mailDetails, $customKeywords);
	        $flag = $multiPartymailer->sendMail();
        
    		//$msg = NotificationApiClient::sendEmail($data['order']['login'], 'support@myntra.com', $tempSubject, $tempBody, 'text/html');
    	}
    	$return[$orderId]=$msg;
    }else if($filtertype == 'returnid') {
    	$returnsArray = explode(",", $idlist);
		foreach($returnsArray as $returnId) {
    		$tempBody = $body;
    		$tempSubject = $subject;
	    	$data = get_return_details(trim($returnId));
	    	
	    	//replacing variables in body
	   		$tempBody = str_replace('[FIRST_NAME]',$data['name'],$tempBody);
	   		$tempBody = str_replace('[LAST_NAME]',"",$tempBody);
	   		$tempBody = str_replace('[TRACKING_NUMBER]',$data['tracking_no'],$tempBody);
	    	$tempBody = str_replace('[RETURN_ID]', $returnId, $tempBody);
	    	$tempBody = str_replace('[ORDER_ID]', $data['orderid'], $tempBody);
	    	$tempBody = htmlentities($tempBody);
	    	//replacing variables in subject
	    	$tempSubject = str_replace('[RETURN_ID]',$returnId,$tempSubject);
	    	
	    	$weblog->debug("subject ".$tempSubject);
    		$msg = NotificationApiClient::sendEmail($data['login'], 'support@myntra.com', $tempSubject, $tempBody, 'text/html');
    	}
    	
    	$t = array("id"=>$returnId,"msg"=>$msg);
    	array_push($return,$t);
    }
    header('Content-type: text/x-json');
	print json_encode(array("results"=>$return));
}

?>
