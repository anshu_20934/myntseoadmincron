Ext.onReady(function() {
	Ext.QuickTips.init();
	
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
	Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
	Ext.Ajax.on('requestexception', myMask.hide, myMask);

	var searchPanel = new Ext.form.FormPanel({
		renderTo : 'contentPanel',
		title : 'Emails',
		id : 'formPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 2},
			defaults : { border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [  {
				items : [{
		        	xtype : 'combo',
		        	store : new Ext.data.JsonStore({
		        		fields : [ 'id', 'name'],
		        		data : emailTemplates,
		        		sortInfo : {
		        			field : 'id',
		        			direction : 'ASC'
		        		}
		        	}),
		        	emptyText : 'Select Template to Edit',
		        	displayField : 'name',
		        	valueField : 'name',
		        	fieldLabel : 'Template Name',
		        	name : 'template_id',
		        	mode: 'local',
		        	triggerAction: 'all',
		        	listeners : {
		        	    'select' : function selectionListenerFunc(comp)
							{	
		        	    		 getTemplate();
							}
		        	    
		        	}
				}]
			},  {
				items: []
			},{
				items :[{
					xtype : 'textfield',
		        	id : 'subject',
		        	fieldLabel : 'Subject',
		        	width : 200
				}]
			}, {
				items: []
			},{
				items :[{
					xtype : 'htmleditor',
		        	id : 'body',
		        	fieldLabel : 'Body',
		        	width : 550,
		        	height: 300
		        	
				}]
			}]
		}],
		buttons: [ {
			text: 'Edit',
			handler : function()
			{
				getTemplate();
			}
		}, {
			text: 'Save',
			handler : function()
			{
				saveTemplate();
			}
		}]
	});

	function saveTemplate()
	{
		var params = getSearchPanelParams();
		var form = searchPanel.getForm();
		Ext.Ajax.request({
			   url:'emaileditajax.php',
			   params:{"action": "save","template_id":params["template_id"],
				       "body":params["body"],"subject":params["subject"]},
			   success: function(response,action)
			   {
				   alert("Template Saved");
			   }
		   });
	}
	
	
	function getTemplate()
	{
		var params = getSearchPanelParams();
		if(params == false)
		{
			Ext.MessageBox.alert('Error', 'Errors in the search form');
			return false;
		}
	
	   Ext.Ajax.request({
		   url:'emaileditajax.php',
		   params:{"action": "search","template_id":params["template_id"]},
		   success: function(response,action)
		   {
			   //alert(response.responseText);
			   var jsonObj = Ext.util.JSON.decode(response.responseText);
			   Ext.getCmp('subject').setValue(jsonObj.results.subject);
			   Ext.getCmp('body').setValue(jsonObj.results.body);
		   }
		   
			   
	   });
	}
	
	function getSearchPanelParams()
	{
		//alert("e");
		var form = searchPanel.getForm();
		
		if(form.isValid())
		{
			var params = {};
			params["template_id"] = form.findField("template_id").getValue();
			params["subject"] = form.findField("subject").getValue();
			params["body"] = form.findField("body").getValue();
			
			return params;
		}
		else
		{
			
			return false;
		}
	}
	
   mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
		      searchPanel
		]
	});
});
