<?php
$errorMessages = array();
$errorMessages['USER_ALREADY_EXIST'] = "This user email id is not available. Please try with another email id!!";
$errorMessages['COMPANY_HEADER_IMAGE'] = "Company header image can not be uploaded. Please try again!!";
$errorMessages['COMPANY_LOGO_IMAGE'] = "Company logo image can not be uploaded. Please try again!!";
$errorMessages['ACCOUNT_URL_EXIST'] = "Account URL is not available. Please try again!!";
$errorMessages['ACCOUNT_NAME_EXIST'] = "Account name is not available. Please try again!!";
$errorMessages['STATUS_NOT_CHANGED'] = "Account status could not be change. Please contact to developement administrator!!";
$errorMessages['NO_STYLES'] = "No product styles activeor added for this product. Please contact to administrator";
$errorMessages['STYLESTATUS_NOT_CHANGED'] = "Style status can not be chnged. Please contact to administrator";
$errorMessages['COMPANY_INI_FILE'] = "Company ini file can not be uploaded. Please try again!!";
$errorMessages['COMPANY_CSS_FILE'] = "Company css file can not be uploaded. Please try again!!";


?>