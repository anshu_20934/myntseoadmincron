<?php
/*
 * Created on May 15, 2012
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 require("auth.php");
 include_once("$xcart_dir/include/security.php");
 include_once("$xcart_dir/include/func/func.mkcore.php");
 include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");
 
 if($REQUEST_METHOD == "POST") {
 	if($_POST['action'] == 'save') {
 		$selectedStates = trim($_POST['states']);
 		//$selectedPaymentMethods = trim($_POST['paymentMethods']);
 		
 		WidgetKeyValuePairs::setWidgetValueForKey("", 'codOH.firstpurchase.states', $selectedStates, 'First orders place by customers from these states will go to on hold state');
 		//WidgetKeyValuePairs::setWidgetValueForKey("", 'codOH.firstpurchase.paymentMethods', $selectedPaymentMethods, 'First orders place by customers from these payment methods will go to on hold state');
 			
 		echo json_encode(array('SUCCESS'));
 		exit;
 	}
 	if($_POST['action'] == 'upload') {
 		$filename = $HTTP_POST_FILES['zipcodes']['name'];
 		$tmpName = $HTTP_POST_FILES['zipcodes']['tmp_name'];
 		$ext = pathinfo($filename, PATHINFO_EXTENSION);
 		
 		if($ext != 'csv'){
 			echo '{"success":false, "message":"Please upload a .csv file!"}';
 			exit;
 		}		
 		$fp      = fopen($tmpName, 'r');
 		$zipcodes = fread($fp, filesize($tmpName));
 		$zipcodes = preg_split("/[\s,]+/", $zipcodes, -1, PREG_SPLIT_NO_EMPTY);
		fclose($fp);
		
		$pricerange = explode(",", $_POST['rvalue']);
		foreach($pricerange as &$rvalue){
			$rvalue = "total+cod+payment_surcharge ".$rvalue;
		}
		
		$zipcodesPricePoints = array("zipcodes" => $zipcodes, "pricerange" => $pricerange);
		WidgetKeyValuePairs::setWidgetValueForKey("", 'codOH.zipcodes.price.points', json_encode($zipcodesPricePoints), 'ZipCodes and Price Range for which orders should be put on hold');
		
 		echo '{"success":true}';
 		exit;
 	}
 }
 
 $displayRange = json_decode(trim(WidgetKeyValuePairs::getWidgetValueForKey('codOH.zipcodes.price.points')), true);
 $zipcodesChosen = $displayRange['zipcodes'];
 $displayRange = $displayRange['pricerange'];
 foreach($displayRange as &$rvalue)
 	$rvalue = substr($rvalue, 41);
 $displayRange = array('displayRange' => implode(", ", $displayRange));
 $zipcodesChosen = array('zipcodesChosen' => implode(", ", $zipcodesChosen));
 $smarty->assign("displayRange", json_encode(array($displayRange)));
 $smarty->assign("zipcodesChosen", json_encode(array($zipcodesChosen)));
 
 $selectedStates = trim(WidgetKeyValuePairs::getWidgetValueForKey('codOH.firstpurchase.states'));
 $smarty->assign("selectedStates", json_encode($selectedStates));
 
 $selectedPaymentMethods = trim(WidgetKeyValuePairs::getWidgetValueForKey('codOH.firstpurchase.paymentMethods'));
 $smarty->assign("selectedPaymentMethods", json_encode($selectedPaymentMethods));
 
 $states = func_get_states('IN');
 $smarty->assign("states", json_encode($states));
 
 $range = array(
 		array('id'=>"0", "range" => "BETWEEN 0 AND 200"), 
 		array('id'=>"1", "range" => "BETWEEN 200 AND 400"), 
 		array('id'=>"2","range" => "BETWEEN 400 AND 600"),
 		array('id'=>"3","range" => "BETWEEN 600 AND 800"),
 		array('id'=>"4","range" => "BETWEEN 800 AND 1000"),
 		array('id'=>"5","range" => "BETWEEN 1000 AND 1200"),
 		array('id'=>"6","range" => "BETWEEN 1200 AND 1400"),
 		array('id'=>"7","range" => "BETWEEN 1400 AND 1600"),
 		array('id'=>"8","range" => "BETWEEN 1600 AND 1800"),
 		array('id'=>"9","range" => "BETWEEN 1800 AND 2000"),
 		array('id'=>"10","range" => "> 2000")
 		);
 $smarty->assign("range", json_encode($range));
 
 $smarty->assign("main","cod_onhold_settings");
 func_display("admin/home.tpl", $smarty);
 
?>
