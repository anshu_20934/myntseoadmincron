<?php
require "./auth.php";
require $xcart_dir."/include/security.php";

if($mode=='delay'){
	
	$email = "myntramailarchive@gmail.com,myntracustcare@gmail.com";

	$args = array( 	"ORDER_ID" 	=> $_POST['orderid'],
		"DELAY_REASON" 		=> $_POST['delayreason'],
		"ACTION_NEEDED"		=> $_POST['customeraction'],
		"SHIPMENT_DATE" 	=> $_POST['shipmentdate']);

	$subjectarray = array( 	"ORDER_ID" 	=> $_POST['orderid']);


	$sendemailto="myntramailarchive@gmail.com,myntracustcare@gmail.com";
	$template = 'orderdelaynotification';
	$flag = sendMessageDynamicSubject($template, $args, $sendemailto,$from,$subjectarray,$bcc);

	$expected_date = date('Y-m-d', strtotime($_POST['shipmentdate']));
	
	$query = "INSERT INTO mk_delay_notifications(order_id,delay_reason,expected_date,action_needed,user_email) VALUES('{$_POST['orderid']}','{$_POST['delayreason']}','{$expected_date}','{$_POST['customeraction']}','{$email}')";
	$sqllog->debug("sql query : ".$query);
	$result=db_query($query);

    if($flag)
    	func_header_location("$http_location/admin/delay_notification.php?d=I");
    else  
	func_header_location("$http_location/admin/delay_notification.php?d=N");
}

func_display("admin/main/delay_notification.tpl",$smarty);
?>
