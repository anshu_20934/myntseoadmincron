<?php

require "./auth.php";
require $xcart_dir."/include/security.php";

if (empty($mode)) $mode = "";
if (!empty($styleid)) $data = $styleid;


if ($REQUEST_METHOD == "POST") { 
   
    
	    if ($mode == "add") {

         #
		 # Insert product style 
		 #
		  $SQl= "INSERT INTO $sql_tbl[styleoptions](style,name,value,description)VALUES ($styleid,'$option_name','$option_value','$desc')";
		  db_query($SQl);
		  $top_message["content"] = func_get_langvar_by_name("msg_adm_featproducts_upd");
           func_header_location("admin_product_styles.php");
		      
        }  

		 elseif($mode == "update") {

         #
		 # Update product style 
		 #
		   if (is_array($posted_data)) {
			    foreach ($posted_data as $optionid=>$v) {
					if (empty($v["to_delete"]))
							continue;
					
					$active = (!empty($v["is_active"]) ? 1 : 0 );
					$def =    (!empty($v["default"]) ? 'Y' : 'N' );
						
						$query_data = array(
							"is_active" => $active,
						    "display_order" => 	$v["display_order"],
							"style_default" => $def


						);
				func_array2update("styleoptions", $query_data, " id=$optionid ");
			   }
			
			  $top_message["content"] = func_get_langvar_by_name("msg_adm_featproducts_upd");
              func_header_location("admin_product_styles.php");
			 
           }
        }  //end of 
         elseif($mode == "delete") {
         #
		 # Update product style 
		 #
		   if (is_array($posted_data)) {
			    foreach ($posted_data as $optionid=>$v) {
					if (empty($v["to_delete"]))
							continue;
					db_query ("DELETE FROM $sql_tbl[styleoptions] WHERE id='$optionid'");
							
			   }
			
			  $top_message["content"] = func_get_langvar_by_name("msg_adm_featproducts_del");
              func_header_location("admin_product_styles.php");
			 
           }
        }  //end of if
		
		

        
}			

$smarty->assign("styleid",$data);
$smarty->assign("mode", $mode);
$smarty->assign ("products", $productstyle);
$smarty->assign("main",$template);


x_session_save();

//$smarty->assign var="foo" value="`../skin1/ptypeimages/Thumbnail/$products[prod_num].type_thumb_img`"}
# Assign the current location line
$smarty->assign("location", $location);
$smarty->assign("dialog_tools_data", $dialog_tools_data);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>