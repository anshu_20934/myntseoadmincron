
<?php

/**
* This class exposes functions to retrieve credentials for accessing DB
*/
class WMSDBConnUtil
{
  const WMS_READ = "wmscron|wms_mysql_read";
  const WMS_WRITE = "wmscron|wms_mysql_write";
  const IMS_READ = "imscron|ims_mysql_read";
  const IMS_WRITE = "imscron|ims_mysql_write";
  const ATP_READ = "atpcron|atp_mysql_read";
  const ATP_WRITE = "atocron|atp_mysql_write";

  private static function getCredentials($service_name, $credential_name)
  {
      $cmd = "/usr/local/bin/keycli_linux_amd64 -service_name ". $service_name . " -credential_name " . $credential_name;
//        print "Cmd: " . $cmd . "\n";
      $json_raw = exec($cmd);
      $json = json_decode($json_raw);

      if ($json->{'error'}) {
          die("Error fetching credentials: " . $json->{'message'});
      }
//        print $json->{'credential_id'};
//        print "\n";
//        print $json->{'credential_secret'};
      return array('username' => $json->{'credential_id'}, 'password' => $json->{'credential_secret'});
  }

  private static function getCredentialsFromConfig($CONFIG)
  {
      $explode = explode("|", $CONFIG);
      return WMSDBConnUtil::getCredentials($explode[0], $explode[1]);
  }

  public static function getConnection($server, $new_link, $CONFIG) {
    $credentials = WMSDBConnUtil::getCredentialsFromConfig($CONFIG);
    $conn = mysql_connect($server, $credentials['username'], $credentials['password'], $new_link);
    if(!$conn) {
      die('Could not connect: '.$server.' :  '. mysql_error());
    } else {
      return $conn;
    }
    
  }
}

