<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: categories.php,v 1.31 2006/02/14 14:45:23 max Exp $
#

require "./auth.php";
require $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.core.php";
//require_once $xcart_dir."/include/solr/solrStatsSearch.php";
//require_once $xcart_dir."/include/solr/solrQueryBuilder.php";
define("BANNER_IMAGES_DIR",'../images/banners');

$location[] = array(func_get_langvar_by_name("lbl_prdstyles_management"), "");
$search_mode = ($HTTP_POST_VARS['mode'])? $HTTP_POST_VARS['mode'] : $HTTP_GET_VARS['mode'] ;
$keyword = ($HTTP_POST_VARS['keyword'])? $HTTP_POST_VARS['keyword'] : $HTTP_GET_VARS['keyword'] ;
$searchtype = (isset($HTTP_POST_VARS['searchtype'])) ? $HTTP_POST_VARS['searchtype'] : $HTTP_GET_VARS['searchtype'] ;
$sortby = (isset($HTTP_POST_VARS['sortby'])) ? $HTTP_POST_VARS['sortby'] : $HTTP_GET_VARS['sortby'] ;
$addnewkeyword = isset($_POST['addNewKeyword']) ? trim($_POST['addNewKeyword']) : ''; 

if($search_mode == "search"){
	
	$limit = 40;
	if(empty($pg) || $pg=='0'){
		$pg = (isset($_GET['pg']) && ctype_digit($_GET['pg'])) ? $_GET['pg'] : 1;
	}else{
		$pg = $_GET['pg'];
	}
	$offset = $limit * $pg - $limit;


	if($searchtype=='keyword'){
        $queryBuilder = " keyword like '%".mysql_real_escape_string($keyword)."%'";

    }else{
        $queryBuilder = " key_id = '".mysql_real_escape_string($keyword)."'";
    }

	if(!empty($sortby)){
		if($sortby=='count_of_products'){
			if($_GET['cp_flag']==1){
				$sortbycondition='count_of_products asc';
				$countofproductsflag=0;
			}else{
				$sortbycondition='count_of_products desc';
				$countofproductsflag=1;
			}
		}
		if($sortby=='count_of_searches'){
			if($_GET['cs_flag']==1){
				$sortbycondition='count_of_searches asc';
				$countofsearchesflag=0;
			}else{
				$sortbycondition='count_of_searches desc';
				$countofsearchesflag=1;
			}
			
		}
        
	}else{
		 $sortbycondition='';
	}
	$sort_count_of_products=$_SERVER['PHP_SELF']."?mode=search&searchtype=".$searchtype."&cp_flag=".$countofproductsflag."&keyword=".$keyword."&sortby=count_of_products"."&pg=".$pg;
	$sort_count_of_searches=$_SERVER['PHP_SELF']."?mode=search&searchtype=".$searchtype."&cs_flag=".$countofsearchesflag."&keyword=".$keyword."&sortby=count_of_searches"."&pg=".$pg;

    $sql = "select * from mk_stats_search";
    $sql .= " where $queryBuilder";
    if($sortbycondition != ''){
        $sql .= " order by $sortbycondition";
    }
    $producttype = sanitizedQueryBuilder($sql, $filter=null, $order=null, $offset, $limit,true);
    $total_products = func_query_first_cell("select count(*) as count from mk_stats_search where $queryBuilder",true);

	if($total_products < 1) {
		$total_products = 'empty';
	}
	$totalpages = ceil($total_products/$limit);

	$sorturl = $_SERVER['PHP_SELF']."?mode=search&searchtype=".$searchtype."&keyword=".$keyword."&sortby=".$sortby."&pg=";
	$paginator = paginate($limit, $pg, $total_products, $sorturl);
} else if ($mode == 'addnewkeyword'){
	if($addnewkeyword) {
		$sql = "SELECT count(*) cnt from mk_stats_search where keyword = '".mysql_real_escape_string($addnewkeyword)."'";
		$res = func_query($sql);
		if($res[0]['cnt'] > 0) {
			$addkeyword_message = '"'.$addnewkeyword.'" already exists';
		} else {
			$insertData['keyword'] = $addnewkeyword;
			$insertData['date_added'] = time();
			$insertData['last_search_date'] = time();
			$insertData['orignal_keyword'] = $addnewkeyword;
			$insertData['is_valid'] = 'Y';
			$insertData['count_of_products'] = 1;
			$insertData['count_of_searches'] = 0;
			
			$r = func_array2insertWithTypeCheck('mk_stats_search',$insertData);
			if($r) {
				$addkeyword_message = '"'.$addnewkeyword.'" added';
			} else {
				$addkeyword_message = ' There was error in adding keyword. Please try again later';
			}
		}
	} else {
		$addkeyword_message = 'Keyword incorrect. Not added';
	}
	$smarty->assign ("addkeyword_message", $addkeyword_message);
}

$smarty->assign ("sort_count_of_products", $sort_count_of_products);
$smarty->assign ("sort_count_of_searches", $sort_count_of_searches);
$smarty->assign ("sortby", $sortby);
$smarty->assign ("products", $producttype);
$smarty->assign ("no_of_products", $no_of_products);
$smarty->assign("main","admin_seo_search");
$smarty->assign ("types", $types);
$smarty->assign ("selectedkeyword", $keyword);
$smarty->assign ("searchtype", $searchtype);
$smarty->assign("paginator",$paginator);
$smarty->assign("totalpages",$totalpages);

# Assign the current location line
$smarty->assign("location", $location);

# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
