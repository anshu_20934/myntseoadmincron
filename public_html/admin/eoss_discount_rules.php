<?php
require_once "./auth.php";
require_once $xcart_dir."/include/security.php";
require_once $xcart_dir."/modules/discount/DiscountConfig.php";
require_once $xcart_dir."/include/solr/solrProducts.php";
include_once $xcart_dir."/include/func/func.mail.php";

$template ="eoss_discount_rules";
$smarty->assign("main",$template);

if($isAccessAllowed == false) {
	// This variable is set in role_access_control.php
	func_display("admin/home.tpl",$smarty);
	exit;
} 

$solrProducts = new solrProducts();

function sendDiscountRuleModificationEmail($heading=null, $newRuleData=null, $oldRuleData=null, $stylesAdded=null, $stylesRemoved=null, $additionalEmailContent=null) {
	global $XCART_SESSION_VARS;
	$loggedInAdminUser = $XCART_SESSION_VARS['identifiers']['A']['login'];

	if(!empty($newRuleData)) {
		$emailSubject = "$heading : Rule #" . $newRuleData['ruleid'] . " - '" . $newRuleData['display_name'] . "' by $loggedInAdminUser";
	} else {
		$emailSubject = "$heading by $loggedInAdminUser";
	}
	
	$emailContent = "$emailSubject<br/><br/>";
	
	if(!empty($newRuleData)) {
		$newRuleData['start_date'] = date("d-M-Y H:i", $newRuleData['start_date']);
		$newRuleData['end_date'] = date("d-M-Y H:i", $newRuleData['end_date']);
		$newRuleData['ruletype'] = $newRuleData['ruletype'] == 0 ? 'DYNAMIC' : 'STATIC';
		$emailContent.= "[RULE DETAILS]<br/><PRE>" . print_r($newRuleData, true) . "</PRE><br/><br/>";
	}
	
	if(!empty($stylesAdded)) {
		$emailContent.= "[STYLES ADDED] " . implode(", ", $stylesAdded). "<br/><br/>";
	}
	
	if(!empty($stylesRemoved)) {
		$emailContent.= "[STYLES REMOVED] " . implode(", ", $stylesRemoved). "<br/><br/>";
	}
	
	if(!empty($oldRuleData)) {
		$emailContent.= '<hr/>';
		$oldRuleData['start_date'] = date("d-M-Y H:i", $newRuleData['start_date']);
		$oldRuleData['end_date'] = date("d-M-Y H:i", $newRuleData['end_date']);
		$oldRuleData['ruletype'] = $oldRuleData['ruletype'] == 0 ? 'DYNAMIC' : 'STATIC';
		$emailContent.= "[RULE EARLIER]<br/><PRE>" . print_r($oldRuleData, true) . "</PRE><br/><br/>";
	}
	
	if(!empty($additionalEmailContent)) {
		$emailContent.= "<br/>$additionalEmailContent<br/>";
	}
	
	$subjectargs = array("TITLE"=>$emailSubject);
	$args = array("CONTENT" => $emailContent);
	
	//echo "<PRE>"; print_r($subjectargs); print_r($args);
	sendMessageDynamicSubject("discount_rule_changes", $args, DISCOUNT_RULE_MODIFICATION_ALERT_EMAILS ,'MyntraAdmin-DiscountRule', $subjectargs);
}

function updateRuleStyles($rules){
	global $solrProducts;
	if(!is_array($rules)){
		$rules = array($rules);
	}
	$allstylesToUpdate = array();
	foreach($rules as $rule){
		$ruleData = func_query_first("select * from mk_discount_rules where ruleid='$rule'");
		$articleType = $ruleData['articletype'];
		$brand = $ruleData['brand'];
		$fashion = $ruleData['fashion'];
		$articleTypeId = func_query_first_cell("select * from mk_catalogue_classification where typename = '$articleType' and parent1 !='-1' and parent2 !='-1' and is_active=1");
		$styles = func_select_query("mk_style_properties", array('global_attr_brand'=>"$brand", 'global_attr_article_type'=>"$articleTypeId", 'global_attr_fashion_type'=>"$fashion"));
		foreach($styles as $style){
			$allstylesToUpdate[$style['style_id']] = $style['style_id'];
		}
	}
	//Update all styles in solr index
	$solrProducts->addStylesToIndex($allstylesToUpdate,true);
}

function addExceptionToRule($ruleid, $styleids,$updateSolr=false){
        global $solrProducts;
        $exceptionList = func_query_first_cell("select exceptionstyles from mk_discount_rules where ruleid='$ruleid'");
        if(!empty($exceptionList)) {
                $exceptionList = explode(",", $exceptionList);
        } else {
                $exceptionList = array();
        }
        if(is_array($styleids)){
                $exceptionList = array_merge($exceptionList, $styleids);
        }else{
                $exceptionList[] = $styleids;
        }

        $exceptionList = array_filter(array_unique(array_map("trim", $exceptionList)));
        $exceptionList = implode(",", $exceptionList);
        $exceptionListwoQuotes = $exceptionList;
        if(empty($exceptionList)) {
                $exceptionList = "null";
        } else {
                $exceptionList = "'$exceptionList'";
        }
        $updateRuleQuery = "update mk_discount_rules set exceptionstyles='$exceptionListwoQuotes' where ruleid='$ruleid'";
        func_query($updateRuleQuery);
        if($updateSolr && !empty($exceptionListwoQuotes)){
                $styleArray = explode(",", $exceptionListwoQuotes);
                $solrProducts->addStylesToIndex($styleArray, false);
        }
}

function removeExceptionFromRule($ruleid, $styleid, $updateSolr=false){
	$exceptionList = func_query_first_cell("select exceptionstyles from mk_discount_rules where ruleid='$ruleid'");
	if(!empty($exceptionList)) {
		$exceptionList = explode(",", $exceptionList);
		$exceptionList = array_filter(array_unique(array_map("trim", $exceptionList)));
		$exceptionList = array_diff($exceptionList, array($styleid));
		$exceptionList = implode(",", $exceptionList);

		if(empty($exceptionList)) {
			func_query("update mk_discount_rules set exceptionstyles=null where ruleid='$ruleid'");
		} else {
			$updateRuleQuery = "update mk_discount_rules set exceptionstyles='$exceptionList' where ruleid='$ruleid'";
			func_query($updateRuleQuery);
		}
	}
}

$mode = $_POST['mode'];

if(empty($mode)) {
	$mode='viewdiscountrules';
}

if($mode=='deletediscountrule') {
	$ruleid = $_POST['modifydiscountruleid'];
	$oldruledetails = func_query_first("select * from mk_discount_rules where ruleid='$ruleid'");
	
	if(!empty($ruleid)) {
		$deleteRuleQuery = "delete from mk_discount_rules where ruleid='$ruleid'";
		func_query($deleteRuleQuery);
		sendDiscountRuleModificationEmail("DELETED RULE", $oldruledetails, null, null, null, null);
	}
	
	// Note: Discount rule can be deleted, only when there are no styles associated to it.
	// We can safely delete such a rule, and there won't be any styles which will need to be reindexed.
	
	// We redirect to page showing all discount rules
	$mode='viewdiscountrules';
}


$viewruleid = $_GET['managediscountruleid'];

if(!empty($viewruleid)) {
	$mode = 'managediscountrule';
}

$smarty->assign('mode', $mode);

// We fetch all logical combinations of Brand / ArticleType / Fashion - from database.
$optionsQuery = "select sp.global_attr_brand as brand, cc.typename as articletype, "; 
$optionsQuery.= "sp.global_attr_fashion_type as fashion from mk_style_properties sp, mk_catalogue_classification cc "; 
$optionsQuery.= "where sp.global_attr_article_type=cc.id group by brand, articletype, fashion";

$optionResults = func_query($optionsQuery);

$ruleoptions = array();	// JSON format for rules it sent to tpl.
$allBrands = array();	// Store all brands obtained from above query.

foreach($optionResults as $key=>$value) {
	$ruleoptions[$value['brand']][$value['articletype']][$value['fashion']] = $key;
	
	if(empty($allBrands[$value['brand']])) {
		$allBrands[$value['brand']]=$value['brand'];
	}
}

$smarty->assign('allBrands', $allBrands);
$smarty->assign('ruleoptions',$ruleoptions);
$smarty->assign('ruleoptionsjson',json_encode($ruleoptions));

// In case any filter was applied to search, we need to retain them after any operation.
$searchSelectedBrand = $_POST['brand']; 
$searchSelectedArticleType = $_POST['articletype'];
$searchSelectedFashion = $_POST['fashion'];

if(!empty($searchSelectedBrand)) {
	$smarty->assign('searchSelectedBrand', $searchSelectedBrand);
}

if(!empty($searchSelectedArticleType)) {
	$smarty->assign('searchSelectedArticleType', $searchSelectedArticleType);
}

if(!empty($searchSelectedFashion)) {
	$smarty->assign('searchSelectedFashion', $searchSelectedFashion);	
}

$allArticleTypes = array();
$allFashions = array();

if(!empty($searchSelectedBrand)) {

	foreach($ruleoptions[$searchSelectedBrand] as $key=>$value) {
		$allArticleTypes[$key] = $key;

		if($key == $searchSelectedArticleType) {
			foreach ($ruleoptions[$searchSelectedBrand][$searchSelectedArticleType] as $key2=>$value2) {
				$allFashions[$key2] = $key2;
			}
			
			$smarty->assign('allFashions', $allFashions);
		}
	}
	$smarty->assign('allArticleTypes', $allArticleTypes);
}


if($mode=='searchstyles') {
	
	$searchStylesQuery = "select sp.style_id, sp.product_display_name, sp.discount_rule from mk_style_properties sp, mk_catalogue_classification cc ";
	$searchStylesQuery.= " where sp.global_attr_article_type=cc.id ";
	$searchStylesQuery.= " and sp.global_attr_brand='$searchSelectedBrand' ";
	
	if(!empty($searchSelectedArticleType)) {
		$searchStylesQuery.= " and cc.typename='$searchSelectedArticleType' ";
	}
	
	if(!empty($searchSelectedFashion)) {
		$searchStylesQuery.= " and sp.global_attr_fashion_type='$searchSelectedFashion' ";
	}
	
	$searchStylesQuery.= "ORDER by product_display_name"; 
	
	$matchingStyles = func_query($searchStylesQuery);
	
	$smarty->assign('matchingStyles', $matchingStyles);
	
} else if ($mode=='viewexceptiondiscountrule') {

	$styleExceptionsQuery = "select style_id, product_display_name from mk_style_properties where discount_rule='0' order by product_display_name";
	
	$styleExceptionsResult = func_query($styleExceptionsQuery);
	
	if(!empty($styleExceptionsResult)) {
		$smarty->assign('stylesExceptions', $styleExceptionsResult);
	}
	
} else if ($mode=='adddiscountrule' || $mode=='addexceptiondiscountrule') {
	
	$discountruletype = $_POST['discountruletype'];

	if($discountruletype == DiscountConfig::$discountConfigMap[DiscountConfig::FLAT_DISCOUNT_ID][DiscountConfig::DISCOUNT_IMPL_CLASS]) {
		// Handling Flat discount;
		
		$discountrulename = $_POST['discountrulename'];
		$discountvalue = $_POST['discountvalue'];
		
		$flatdiscounttype = $_POST['flatdiscounttype'];
		
		if($flatdiscounttype == 'percentage') {
			$flatdiscounttype = 1;
		} else {
			$flatdiscounttype = 0;
		}
		
		$data = json_encode(array('isPercentage'=>$flatdiscounttype, 'value'=>$discountvalue));
		
		$selectedBrand = empty($_POST['brand'])? "null" : "'" . $_POST['brand'] . "'";
		$selectedArticleType = empty($_POST['articletype'])? "null" : "'" . $_POST['articletype'] . "'";;
		$selectedFashion = empty($_POST['fashion'])? "null" : "'" . $_POST['fashion'] . "'";;

		$discountrulestartdate = $_POST['discountrulestartdate'];
		$discountruleenddate = $_POST['discountruleenddate'];
		
		if(empty($discountrulestartdate)) {
			$discountrulestartdate="null";
		} else {
			$discountrulestartdate="unix_timestamp('$discountrulestartdate')";
		}
		
		if(empty($discountruleenddate)) {
			$discountruleenddate="null";
		} else {
			$discountruleenddate="unix_timestamp('$discountruleenddate')";
		}
		
		$discountruledisplaytext = $_POST['discountruledisplaytext'];
		$discountruletermsandconditions = $_POST['discountruletermsandconditions'];
		
		$ruletype = 0; // Default for normal dynamic discount ruletype
		if($mode=='addexceptiondiscountrule') {
			$ruletype = 1;
		}
		
		// Adding new flat discount Rule.
		
		$createDiscountRuleQuery = "insert into `mk_discount_rules` (display_name, ruletype, algoid, data, start_date, end_date, brand, articletype, fashion, display_text, terms) ";
		$createDiscountRuleQuery.= "values (";
		$createDiscountRuleQuery.= 	"'" . mysql_real_escape_string($discountrulename) . "',";
		$createDiscountRuleQuery.= 	"$ruletype,";
		$createDiscountRuleQuery.= 	"'" . mysql_real_escape_string(DiscountConfig::FLAT_DISCOUNT_ID) . "',";
		$createDiscountRuleQuery.= 	"'" . mysql_real_escape_string($data) . "',";
		$createDiscountRuleQuery.= 	"$discountrulestartdate, $discountruleenddate, ";
		$createDiscountRuleQuery.= 	"$selectedBrand, $selectedArticleType, $selectedFashion,";
		$createDiscountRuleQuery.= 	"'" . mysql_real_escape_string($discountruledisplaytext) . "',";
		$createDiscountRuleQuery.= 	"'" . mysql_real_escape_string($discountruletermsandconditions) . "')";
		
		db_query($createDiscountRuleQuery);
		$newruleid = mysql_insert_id();
		
		$smarty->assign('newruleid', $newruleid);
		$flatrulemessage = "$discountrulename : $discountvalue";
		if($flatdiscounttype==1) { 
			$flatrulemessage.= "% Off";
		} else { 
			$flatrulemessage.= " Rs. Off";
		}
		$smarty->assign('newrulemessage', $flatrulemessage);
		
		
		if($mode=='addexceptiondiscountrule') {
			$commaSeparatedExceptionStyles = $_POST['commaSeparatedExceptionStyles'];
			$selectedStyle = $_POST['selectedStyle'];
			
			$commaSeparatedExceptionStyles.= "," . implode(",", $selectedStyle);
			
			$commaSeparatedExceptionStyles = explode(",", $commaSeparatedExceptionStyles);
			$commaSeparatedExceptionStyles = array_filter(array_unique(array_map("trim", $commaSeparatedExceptionStyles)));
			$commaSeparatedExceptionStyles = implode(",", $commaSeparatedExceptionStyles);
			
			if(!empty($commaSeparatedExceptionStyles)) {
				$styleData = func_query("select style_id, discount_rule from mk_style_properties where style_id in ($commaSeparatedExceptionStyles)");
				foreach($styleData as $data){
					if(empty($data['discount_rule'])){
						continue;
					}
					removeExceptionFromRule($data['discount_rule'], $data['style_id']);
				}
				$updateStyleQuery = "update mk_style_properties set discount_rule='$newruleid' where style_id in ($commaSeparatedExceptionStyles)";
				func_query($updateStyleQuery);
				$updateExceptionStyleQuery = "update mk_discount_rules set exceptionstyles='$commaSeparatedExceptionStyles' where ruleid='$newruleid'";
				func_query($updateExceptionStyleQuery);
				$solrProducts->addStylesToIndex(explode(",", $commaSeparatedExceptionStyles), false);
			}
			
			$newStylesAdded = empty($commaSeparatedExceptionStyles) ? null : explode(",", $commaSeparatedExceptionStyles);
			$newruledata = func_query_first("select * from mk_discount_rules where ruleid='$newruleid'");
			
			// Email alerts for adding new static rule.
			sendDiscountRuleModificationEmail("ADDED NEW STATIC RULE", $newruledata, null, $newStylesAdded, null, null);
			
			header("Location: eoss_discount_rules.php?managediscountruleid=$newruleid&new=1");
			exit;
		}
	}
		
} else if($mode=='viewdiscountrules' || $mode=='enablediscountrules' || $mode=='filterdiscountrules' || $mode=='multiplediscountrulechange') {
	
	$filterquerycondition = "";
	$discountenabledRules = $_POST['discountenabledrules'];
	$selectedRules = $_POST['selectedRules'];
	$filterapplied = $_POST['filterapplied'];
	
	if($mode=='enablediscountrules' && !empty($discountenabledRules)) {
		foreach ($discountenabledRules as $key=>$value) {
			$ruleids[] = $key;
		}
		$ruleids = implode(",", $ruleids);
		$query = "select ruleid,enabled from mk_discount_rules where ruleid in ($ruleids)";
		$ruleIdData = func_query($query);
		$ruleIdToEnabledMap = array();
		foreach($ruleIdData as $arr){
			$ruleIdToEnabledMap[$arr['ruleid']] = $arr['enabled'];
		}

		$rulesToUpdate = array();
		$emailContentForEnableDisableRules = "";
		
		foreach ($discountenabledRules as $key=>$ruleyesno) {
			if(empty($filterapplied) || in_array($key, $selectedRules)) {
				// Note; empty($filterapplied) check is applied when all discount rules are seen, and we need to disable all.
				if($_POST['actionOnSelectedRule'] == 1) {
					// Disable selected rules.
					$value=0;
					if($ruleIdToEnabledMap[$key]!=$value) {
						$emailContentForEnableDisableRules.= "<br/>Rule #$key : Disabled";
					}
				} else {
					// User has clicked on update button on filtered rules. We need to find the yes/no value selected for that rule.
					$value=$ruleyesno;
					if($ruleIdToEnabledMap[$key]!=$value) {
						$emailContentForEnableDisableRules.= "<br/>Rule #$key : ";
						$emailContentForEnableDisableRules.= empty($value) ? 'Disabled' : 'Enabled';
					}
				}
				if($ruleIdToEnabledMap[$key] == $value){
				    continue;
				}
				$rulesToUpdate[] = $key;
				$enableDiscountQuery = "update mk_discount_rules set enabled='$value' where ruleid='$key'";
				func_query($enableDiscountQuery);
			}
		}
		
		if(!empty($emailContentForEnableDisableRules)) {
			sendDiscountRuleModificationEmail("ENABLE/DISABLE DISCOUNT RULES", null, null, null, null, $emailContentForEnableDisableRules);
		}
		
		updateRuleStyles($rulesToUpdate);
		
	} else if($mode=='multiplediscountrulechange' && !empty($discountenabledRules)) {
		
		$alldiscountruletype = $_POST['alldiscountruletype'];

		if($alldiscountruletype == DiscountConfig::$discountConfigMap[DiscountConfig::FLAT_DISCOUNT_ID][DiscountConfig::DISCOUNT_IMPL_CLASS]) {
			// Changing to flat discount on all the rules;
			$alldiscountvalue = $_POST['alldiscountvalue'];
			$allflatdiscounttype = $_POST['allflatdiscounttype'];
			
			if($allflatdiscounttype == 'percentage') {
				$allflatdiscounttype = 1;
			} else {
				$allflatdiscounttype = 0;
			}
			
			$alldata = json_encode(array('isPercentage'=>$allflatdiscounttype, 'value'=>$alldiscountvalue));
			
			$alldiscountrulestartdate = $_POST['alldiscountrulestartdate'];
			$alldiscountruleenddate = $_POST['alldiscountruleenddate'];
			
			if(empty($alldiscountrulestartdate)) {
				$alldiscountrulestartdate="null";
			} else {
				$alldiscountrulestartdate="unix_timestamp('$alldiscountrulestartdate')";
			}
			
			if(empty($alldiscountruleenddate)) {
				$alldiscountruleenddate="null";
			} else {
				$alldiscountruleenddate="unix_timestamp('$alldiscountruleenddate')";
			}
			
			$alldiscountruledisplaytext = $_POST['alldiscountruledisplaytext'];
			$alldiscountruletermsandconditions = $_POST['alldiscountruletermsandconditions'];
			
			$ruleids = array();
			foreach ($discountenabledRules as $key=>$value) {
				if(in_array($key, $selectedRules)) {
					$ruleids[] = $key;
				}
			}
			
			$ruleids = implode(",", $ruleids); 
			
			if(!empty($ruleids)) {
				
				$emailContentForMultipleRuleChanges = "";
				
				$modifiedDataArray = array();
				$oldRuleDataArray = func_query("select * from mk_discount_rules where ruleid in ($ruleids)");
				foreach($oldRuleDataArray as $singleRuleData) {
					$singleRuleData['start_date'] = date("d-M-Y H:i", $singleRuleData['start_date']);
					$singleRuleData['end_date'] = date("d-M-Y H:i", $singleRuleData['end_date']);
					$singleRuleData['ruletype'] = $singleRuleData['ruletype'] == 0 ? 'DYNAMIC' : 'STATIC';
					$modifiedDataArray[$singleRuleData['ruleid']]['old'] = $singleRuleData;
				}
				
				$updateRuleQuery = "update mk_discount_rules set "; 
				$updateRuleQuery.= " algoid='" . DiscountConfig::FLAT_DISCOUNT_ID . "', ";
				$updateRuleQuery.= " data='" . mysql_real_escape_string($alldata) . "', ";
				$updateRuleQuery.= " start_date=$alldiscountrulestartdate, ";
				$updateRuleQuery.= " end_date=$alldiscountruleenddate, ";
				$updateRuleQuery.= " display_text='" . mysql_real_escape_string($alldiscountruledisplaytext) . "', ";
				$updateRuleQuery.= " terms='" . mysql_real_escape_string($alldiscountruletermsandconditions) . "' ";
				$updateRuleQuery.= " where ruleid in ($ruleids)";
				func_query($updateRuleQuery);
				
				$newRuleDataArray = func_query("select * from mk_discount_rules where ruleid in ($ruleids)");
				foreach($newRuleDataArray as $singleRuleData) {
					$singleRuleData['start_date'] = date("d-M-Y H:i", $singleRuleData['start_date']);
					$singleRuleData['end_date'] = date("d-M-Y H:i", $singleRuleData['end_date']);
					$singleRuleData['ruletype'] = $singleRuleData['ruletype'] == 0 ? 'DYNAMIC' : 'STATIC';
					$modifiedDataArray[$singleRuleData['ruleid']]['new'] = $singleRuleData;
				}
				
				// Update all style which are attached to the rules
				$styleids = func_query("select style_id from mk_style_properties where discount_rule in($ruleids)");
				$solrProducts->addStylesToIndex($styleids,true);
				
				if(!empty($modifiedDataArray)) {
					foreach($modifiedDataArray as $singleruleid=>$rulemodificationdata) {
						$emailContentForMultipleRuleChanges.= "[RULE $singleruleid CHANGED]<br/>";
						$emailContentForMultipleRuleChanges.= "[NEW RULE DATA]<br/><PRE>" . print_r($rulemodificationdata['new'], true) . "</PRE><br/>";
						$emailContentForMultipleRuleChanges.= "[OLD RULE DATA]<br/><PRE>" . print_r($rulemodificationdata['old'], true) . "</PRE><br/><br/><hr/>";
					}
					
					if(!empty($emailContentForMultipleRuleChanges)) {
						sendDiscountRuleModificationEmail("MULTIPLE RULE CHANGES", null, null, null, null, $emailContentForMultipleRuleChanges);
					}
				}
			}
		}
	
	} 
		
	$filterbrand = $_POST['brand'];
	$filterarticletype = $_POST['articletype'];
	$filterfashion = $_POST['fashion'];
	
	if(!empty($filterbrand)) {
		$filterquerycondition.=" and brand='$filterbrand' ";
	}
	
	if(!empty($filterarticletype)) {
		$filterquerycondition.=" and articletype='$filterarticletype' ";
	}
	
	if(!empty($filterfashion)) {
		$filterquerycondition.=" and fashion='$filterfashion' ";
	}
		
	$allDiscountRulesQuery = "select ruleid, ruletype, display_name, algoid, brand, articletype, fashion, data, enabled, display_text, terms,  from_unixtime(start_date, '%Y-%m-%d %H:%i') as start_date, exceptionstyles,  ";
	$allDiscountRulesQuery .= "from_unixtime(end_date, '%Y-%m-%d %H:%i') as end_date, display_text ";
	$allDiscountRulesQuery .= " from mk_discount_rules ";
	if(!empty($filterquerycondition)) {
		$allDiscountRulesQuery.= " where 1=1 $filterquerycondition ";
	}
	$allDiscountRulesQuery.= " order by enabled desc, ruleid asc";
	$activeStyleCountQuery = "select discount_rule, count(*) as count from mk_style_properties where discount_rule > 0 group by discount_rule";
	
	$allDiscountRules = func_query($allDiscountRulesQuery);
	$activeStyleCountRes = func_query($activeStyleCountQuery);
	$activeStyleCountArr = array();

	foreach($activeStyleCountRes as $key=>$activeStyleCount){
		$activeStyleCountArr[$activeStyleCount['discount_rule']] = $activeStyleCount['count'];
	}

	foreach($allDiscountRules as $key=>$singleDiscountRule){
		$brand = $singleDiscountRule['brand'];
		$articleType = $singleDiscountRule['articletype'];
		$fashion = $singleDiscountRule['fashion'];
		$ruleid = $singleDiscountRule['ruleid'];
		
		$exceptionstyles = $singleDiscountRule['exceptionstyle']==null ? '0' : $singleDiscountRule['exceptionstyle'];
		$totalStylesQuery = "select count(*) as total from mk_style_properties as mksp inner join mk_catalogue_classification as mkcc ";
		$totalStylesQuery .= " on (mksp.global_attr_article_type = mkcc.id) where global_attr_brand='$brand' and global_attr_fashion_type='$fashion' ";
		$totalStylesQuery .= " and mkcc.typename='$articleType' and global_attr_article_type=mkcc.id and mkcc.parent1 !='-1' and mkcc.parent2 !='-1' and mkcc.is_active=1 and mksp.style_id not in ($exceptionstyles)";
		$totalStylesQuery .= " and (mksp.global_attr_year != '2011' or (mksp.global_attr_season !='Winter' and mksp.global_attr_season != 'Fall'))";
		$totalStyles = func_query_first_cell($totalStylesQuery);
		$allDiscountRules[$key]['totalcount'] = $totalStyles;
		if(!empty($activeStyleCountArr[$ruleid])){
			$allDiscountRules[$key]['count'] = $activeStyleCountArr[$ruleid];
		}else{
			$allDiscountRules[$key]['count'] = 0;
		}
		
		if($singleDiscountRule['algoid']=='FLAT') {
			$discountdata = json_decode($singleDiscountRule['data'], true);
			if(empty($discountdata['isPercentage'])) {
				$allDiscountRules[$key]['discountdata'] = "Rs. " . $discountdata['value'];
			} else {
				$allDiscountRules[$key]['discountdata'] = $discountdata['value'] . " %";
			}
		}
	}
	
	if(!empty($allDiscountRules)) {
		$allDynamicDiscountRules = array();
		$allSpecialDiscountRules = array();
		
		foreach($allDiscountRules as $key=>$singleRule) {
			if($singleRule['ruletype']==0) {	
				$allDynamicDiscountRules[] = $singleRule;
			} else if($singleRule['ruletype']==1) {
				$allSpecialDiscountRules[] = $singleRule;
			}
		}
		
		if(!empty($allDynamicDiscountRules)) {
			$smarty->assign('allDynamicDiscountRules', $allDynamicDiscountRules);
		}
		
		if(!empty($allSpecialDiscountRules)) {
			$smarty->assign('allSpecialDiscountRules', $allSpecialDiscountRules);
		}
		
		$smarty->assign('totalcount', count($allDiscountRules));
	}
	
} else if($mode=='managediscountrule') {
	
	if(isset($_POST['modifydiscountruleid'])) {
		// Update button clicked for ruleid
		$viewruleid = $_POST['modifydiscountruleid']; 
	} else {
		// Just for viewing ruleid
		$viewruleid = $_GET['managediscountruleid'];	
	}
	
	if(isset($_POST['modifydiscountruleid'])) {
		
		$smarty->assign('rulesuccessfullymodified', 1);
		
		$oldRuleData = func_query_first("select * from mk_discount_rules where ruleid='$viewruleid'");
		$ruletype = $oldRuleData['ruletype'];
		
		if(empty($_POST['moveToRule']) && empty($_POST['moveToException'])) {
			// Note: In case add style to rule, or move style to exception buttons are clicked, we should not modify rule properties.
			
			$oldRuleData = func_query_first("select * from mk_discount_rules where ruleid='$viewruleid'"); 
			
			$newrulename = $_POST['discountrulename'];
			$newdiscounttype = $_POST['flatdiscounttype'];
			
			if($newdiscounttype=='percentage') {
				$newdiscounttype = 1;
			} else {
				$newdiscounttype = 0;
			}
			
			$newdiscountvalue = $_POST['discountvalue'];
			
			$newdata = json_encode(array("isPercentage"=>$newdiscounttype, "value"=>$newdiscountvalue));
			$newDisplayText = $_POST['discountruledisplaytext'];
			$newTermsAndConditions = $_POST['discountruletermsandconditions'];
			$newStartDate = $_POST['discountrulestartdate'];
			$newEndDate = $_POST['discountruleenddate'];
			
			if(empty($newStartDate)) {
				$newStartDate="null";
			} else {
				$newStartDate="unix_timestamp('$newStartDate')";
			}
			
			if(empty($newEndDate)) {
				$newEndDate="null";
			} else {
				$newEndDate="unix_timestamp('$newEndDate')";
			}
			
			$updateRuleQuery = "update mk_discount_rules set ";
			$updateRuleQuery.= " display_name='" . mysql_real_escape_string($newrulename) . "', ";
			$updateRuleQuery.= " data='" . mysql_real_escape_string($newdata) . "',";
			$updateRuleQuery.= " display_text='" . mysql_real_escape_string($newDisplayText) . "',";
			$updateRuleQuery.= " terms='" . mysql_real_escape_string($newTermsAndConditions) . "',";
			$updateRuleQuery.= " start_date=$newStartDate, end_date=$newEndDate ";
			$updateRuleQuery.= " where ruleid='$viewruleid'";
			
			func_query($updateRuleQuery);
			
			$styleidsToAddToExceptionRule = $_POST['selectedStyle'];
			if(!empty($styleidsToAddToExceptionRule)) {
				$styleidsToAddToExceptionRule = implode(",", $styleidsToAddToExceptionRule);
				$updateStyleQuery = "update mk_style_properties set discount_rule='$viewruleid' where style_id in ($styleidsToAddToExceptionRule)";
				func_query($updateStyleQuery);
				addExceptionToRule($viewruleid, explode(",",$styleidsToAddToExceptionRule),true);
			}
			
			$styleidsToAddToExceptionRule = empty($styleidsToAddToExceptionRule) ? null:explode(",",$styleidsToAddToExceptionRule);
			$newRuleData = func_query_first("select * from mk_discount_rules where ruleid='$viewruleid'");
			sendDiscountRuleModificationEmail("MODIFY RULE", $newRuleData, $oldRuleData,$styleidsToAddToExceptionRule , null, null);
		}
		
		if(!empty($_POST['moveToException']) && $_POST['movestyles']=="1") {
			$styleids = $_POST['moveToException'];
			$styleidsCommaSeparated = implode(",", $styleids);
			
			$updateStyleQuery = "update mk_style_properties set discount_rule='0' where style_id in ($styleidsCommaSeparated)";
			func_query($updateStyleQuery);
			if(empty($ruletype)) {
				// Handling dynamic rule. We need to add styleid to dynamic rule's exceptionstyles column
				addExceptionToRule($viewruleid, $styleids, true);
				
				$newRuleData = func_query_first("select * from mk_discount_rules where ruleid='$viewruleid'");
				sendDiscountRuleModificationEmail("REMOVING STYLES FROM DYNAMIC RULE", $newRuleData, $oldRuleData, null, $styleids, null);
				
			} else {
				//Remove the style id from exception style field of an exception rule
				$exceptionList = func_query_first_cell("select exceptionstyles from mk_discount_rules where ruleid='$viewruleid'");
				if(!empty($exceptionList)) {
					$exceptionList = explode(",", $exceptionList);
					$exceptionList = array_diff($exceptionList, $styleids);
					$exceptionList = implode(",", $exceptionList);
				}
				
				if(empty($exceptionList)) {
					$exceptionList = "null";
				} else {
					$exceptionList = "'$exceptionList'";
				}

				$updateRuleQuery = "update mk_discount_rules set exceptionstyles=$exceptionList where ruleid='$viewruleid'";
				func_query($updateRuleQuery);
				
				$newRuleData = func_query_first("select * from mk_discount_rules where ruleid='$viewruleid'");
				sendDiscountRuleModificationEmail("REMOVING STYLES FROM STATIC RULE", $newRuleData, $oldRuleData, null, $styleids, null);
			}
			
			if(!empty($styleids)) {
				$solrProducts->addStylesToIndex($styleids,true);
			}
		}
		
		if(!empty($_POST['moveToRule']) && $_POST['movestyles']=="2") {
			$styleids = $_POST['moveToRule']; 
			
			foreach($styleids as $singlestyleid) {
				$specialruleid = func_query_first_cell("select discount_rule from mk_style_properties where style_id='$singlestyleid'");
				if(!empty($specialruleid)){
					removeExceptionFromRule($specialruleid,$singlestyleid);
				}
			}
			
			$styleidsCommaSeparated = implode(",", $styleids);
			$updateStyleQuery = "update mk_style_properties set discount_rule='$viewruleid' where style_id in ($styleidsCommaSeparated)";
			func_query($updateStyleQuery);
			
			$exceptionList = func_query_first_cell("select exceptionstyles from mk_discount_rules where ruleid='$viewruleid'");
			foreach($styleids as $singlestyleid) {
				removeExceptionFromRule($viewruleid,$singlestyleid);
			}
			
			if(!empty($styleids)) {
				$solrProducts->addStylesToIndex($styleids,true);
			}
			
			$newRuleData = func_query_first("select * from mk_discount_rules where ruleid='$viewruleid'");
			sendDiscountRuleModificationEmail("ADDING STYLES TO DYNAMIC RULE", $newRuleData, $oldRuleData, $styleids, null, null);
		}
		
		if(!empty($ruletype) && !empty($_POST['commaSeparatedExceptionStyles'])) {
			$addStylesToExceptionRule = $_POST['commaSeparatedExceptionStyles'];
			
			$addStylesToExceptionRule = explode(",", $addStylesToExceptionRule);
			$addStylesToExceptionRule = array_filter(array_unique(array_map("trim", $addStylesToExceptionRule)));
        		$addStylesToExceptionRule = implode(",", $addStylesToExceptionRule);
			
			if(!empty($addStylesToExceptionRule)) {
				$updateStyleQuery = "update mk_style_properties set discount_rule='$viewruleid' where style_id in ($addStylesToExceptionRule)";
				func_query($updateStyleQuery);
				
				$styleidarray = empty($addStylesToExceptionRule) ? array() :  explode(",", $addStylesToExceptionRule);
				
				foreach($styleidarray as $singlestyleid) {
					// There is a possibility that we removed one exception style from one static rule into another one.
					// In such a case, we will have to remove this styleid from exceptionstyles column of other static rules.
					$checkOtherStaticRulesQuery = "select * from mk_discount_rules where ruleid!=$viewruleid and ruletype=1 and " ;
					$checkOtherStaticRulesQuery.="(exceptionstyles like '$singlestyleid' or exceptionstyles like '%,$singlestyleid,%' or exceptionstyles like '$singlestyleid,%' or exceptionstyles like '%,$singlestyleid')";
					$staticRuleData = func_query_first($checkOtherStaticRulesQuery);

					if(!empty($staticRuleData)) {
						removeExceptionFromRule($staticRuleData['ruleid'], $singlestyleid);
					}
				}
				addExceptionToRule($viewruleid, $styleidarray, true);
				$newRuleData = func_query_first("select * from mk_discount_rules where ruleid='$viewruleid'");
				sendDiscountRuleModificationEmail("ADDING STYLES TO STATIC RULE", $newRuleData, $oldRuleData, $styleidarray, null, null);
			}
		}
		
		//Update style index for the rule
		if($ruletype == 0){
			updateRuleStyles($viewruleid);
		} else if($ruletype == 1){
			$exceptionstyles = func_query_first_cell("select exceptionstyles from mk_discount_rules where ruleid ='$viewruleid'");
			if(!empty($exceptionstyles)){
				$solrProducts->addStylesToIndex(explode(",", $exceptionstyles), false);
			}
		}
	}
	
	$singleDiscountRuleQuery = "select ruleid, ruletype, display_name, algoid, data, enabled, from_unixtime(start_date, '%Y-%m-%d %H:%i') as start_date, "; 
	$singleDiscountRuleQuery.= " from_unixtime(end_date, '%Y-%m-%d %H:%i') as end_date, display_text, terms, brand, articletype, fashion ";
	$singleDiscountRuleQuery.= " from mk_discount_rules where ruleid='$viewruleid'";
	$singleDiscountRule = func_query_first($singleDiscountRuleQuery);
	
	$ruletype = $singleDiscountRule['ruletype'];
	
	if($singleDiscountRule['algoid'] == DiscountConfig::FLAT_DISCOUNT_ID) {
		$flatdiscountdata = json_decode($singleDiscountRule['data'], true);
		$isPercentage = $flatdiscountdata['isPercentage'];
		$value = $flatdiscountdata['value'];
		
		$smarty->assign('singleDiscountRuleType', $isPercentage);
		$smarty->assign('singleDiscountRuleValue', $value);
	}
	
	$smarty->assign('singleDiscountRule', $singleDiscountRule);
	
	$getStylesRuleAppliedQuery = "select style_id, product_display_name, article_number from mk_style_properties ";
	$getStylesRuleAppliedQuery.= " where discount_rule='$viewruleid' order by product_display_name";
	$stylesRuleApplied = func_query($getStylesRuleAppliedQuery);
	if(!empty($stylesRuleApplied)) {
		$smarty->assign('stylesRuleApplied', $stylesRuleApplied);
	}

	
	if(empty($ruletype) && !empty($singleDiscountRule['brand'])) {
		$getStylesRuleWillBeAppliedQuery = "select sp.style_id, sp.product_display_name, sp.article_number from mk_style_properties sp, mk_catalogue_classification cc";
		$getStylesRuleWillBeAppliedQuery.= " where sp.global_attr_article_type=cc.id and sp.discount_rule is null";
		$getStylesRuleWillBeAppliedQuery.= " and sp.global_attr_brand='" . $singleDiscountRule['brand'] . "'";
		$getStylesRuleWillBeAppliedQuery.= " and cc.typename='" . $singleDiscountRule['articletype'] . "'";
		$getStylesRuleWillBeAppliedQuery.= " and sp.global_attr_fashion_type='" . $singleDiscountRule['fashion'] . "'";
		$getStylesRuleWillBeAppliedQuery.= " and (sp.global_attr_year != '2011' or (sp.global_attr_season !='Winter' and sp.global_attr_season != 'Fall'))";
		$getStylesRuleWillBeAppliedQuery.= " order by sp.product_display_name";
		$stylesRuleWillBeApplied = func_query($getStylesRuleWillBeAppliedQuery);
		if(!empty($stylesRuleWillBeApplied)) {
			$smarty->assign('stylesRuleWillBeApplied', $stylesRuleWillBeApplied);
		}
	}
	
	if(empty($ruletype)) {
		$getStylesRemovedFromRuleQuery = "select sp.style_id, sp.product_display_name, sp.article_number from mk_style_properties sp, mk_catalogue_classification cc";
		$getStylesRemovedFromRuleQuery.= " where sp.global_attr_article_type=cc.id and sp.discount_rule=0";
		$getStylesRemovedFromRuleQuery.= " and sp.global_attr_brand='" . $singleDiscountRule['brand'] . "'";
		$getStylesRemovedFromRuleQuery.= " and cc.typename='" . $singleDiscountRule['articletype'] . "'";
		$getStylesRemovedFromRuleQuery.= " and sp.global_attr_fashion_type='" . $singleDiscountRule['fashion'] . "'";
		$getStylesRemovedFromRuleQuery.= " order by sp.product_display_name";
		$stylesRemovedFromRule = func_query($getStylesRemovedFromRuleQuery);
		if(!empty($stylesRemovedFromRule)) {
			$smarty->assign('stylesRemovedFromRule', $stylesRemovedFromRule);
		}
	
		$getStylesWithSpecialRuleQuery = "select sp.style_id, sp.product_display_name, sp.article_number, dr.ruleid, dr.display_name from mk_style_properties sp, mk_catalogue_classification cc, mk_discount_rules dr";
		$getStylesWithSpecialRuleQuery.= " where sp.global_attr_article_type=cc.id and (dr.ruletype=1 and sp.discount_rule=dr.ruleid)";
		$getStylesWithSpecialRuleQuery.= " and sp.global_attr_brand='" . $singleDiscountRule['brand'] . "'";
		$getStylesWithSpecialRuleQuery.= " and cc.typename='" . $singleDiscountRule['articletype'] . "'";
		$getStylesWithSpecialRuleQuery.= " and sp.global_attr_fashion_type='" . $singleDiscountRule['fashion'] . "'";
		$getStylesWithSpecialRuleQuery.= " order by sp.product_display_name";
		$stylesWithSpecialRule = func_query($getStylesWithSpecialRuleQuery);
		if(!empty($stylesWithSpecialRule)) {
			$smarty->assign('stylesWithSpecialRule', $stylesWithSpecialRule);
		}
	}
	
	if(!empty($ruletype)) {
		$styleExceptionsQuery = "select style_id, product_display_name from mk_style_properties where discount_rule='0' order by product_display_name";
	
		$styleExceptionsResult = func_query($styleExceptionsQuery);
		
		if(!empty($styleExceptionsResult)) {
			$smarty->assign('stylesExceptions', $styleExceptionsResult);
		}
	}
	
	if(isset($_GET['new'])) {
		$smarty->assign('newrulecreated', '1');
	}
}

func_display("admin/home.tpl",$smarty);
?>

