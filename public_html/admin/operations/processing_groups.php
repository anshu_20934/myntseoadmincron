<?php
require "../auth.php";
require $xcart_dir."/include/security.php";
require_once($xcart_dir."/include/func/func.itemsearch.php");
require_once($xcart_dir."/include/sendmail.class.php");

$sql = "SELECT id, group_id, name from {$sql_tbl[mk_product_type]}";
$product_types = func_query($sql);

$smarty->assign("product_types", json_encode($product_types));

$smarty->assign("main","processing_groups");
func_display("admin/home.tpl",$smarty);
?>

