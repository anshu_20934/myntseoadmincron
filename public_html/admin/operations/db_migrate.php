<?php 
require "../auth.php";
require $xcart_dir."/include/security.php";

global $weblog;

$options_arr = array("S" => "Small", "M" => "Medium", "L" => "Large", "XL" => "Extra Large");

$sql = "SELECT ps.id AS style_id, po.id AS option_id, ps.name, po.value 
		FROM mk_product_style ps
		LEFT JOIN mk_product_options po ON (po.style = ps.id
			AND po.name = 'size')";

$results = func_query($sql);

$sku_sql = "TRUNCATE TABLE mk_skus; INSERT INTO mk_skus (id, name) VALUES ";
$mapping_sql = "TRUNCATE TABLE mk_styles_options_skus_mapping; INSERT INTO mk_styles_options_skus_mapping (style_id, option_id, sku_id) VALUES ";

$i = 1;
foreach ($results as $result) {
	$sku_name = $result["name"] . ($result["value"] == null ? "" : " " . $options_arr[$result['value']]);
	
	$sku_sql .= " ($i, '" . mysql_real_escape_string($sku_name) . "'), ";
	
	// params 
	$style_id = $result['style_id'];
	$option_id = empty($result['option_id']) ? 'NULL' : $result['option_id'];
	$mapping_sql .= " ($style_id, $option_id, $i), ";
	
	$i++;
}

// Write the sqls to the files
$fh = fopen("create_skus.sql", 'w+') or die("can't open file");
fwrite($fh, substr($sku_sql, 0, strlen($sku_sql) - 2) . ";");
fclose($fh);

$f = fopen("create_skus_mapping.sql", 'w+') or die("can't open file");
fwrite($f, substr($mapping_sql, 0, strlen($mapping_sql) - 2) . ";");
fclose($f);

?>
