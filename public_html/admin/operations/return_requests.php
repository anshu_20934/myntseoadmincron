<?php
require "../auth.php";
require $xcart_dir."/include/security.php";
require $xcart_dir."/include/func/func.returns.php";

$searchreturnid = $_GET['returnid'];
$searchstatus = $_GET['status'];
$searchorderid = $_GET['orderid'];
$searchlogin = $_GET['login'];

$couriers = get_supported_courier_partners();
$statuses = get_all_return_statuses();
$all_dates = get_all_return_statedates();

$smarty->assign("couriers", json_encode($couriers));
$smarty->assign("statuses", json_encode($statuses));
$smarty->assign("alldates", json_encode($all_dates));
$smarty->assign("returnid", $searchreturnid);
$smarty->assign("status", $searchstatus);
$smarty->assign("orderid", $searchorderid);
$smarty->assign("searchlogin", $searchlogin);

$smarty->assign("main","return_requests");
func_display("admin/home.tpl", $smarty);

?>