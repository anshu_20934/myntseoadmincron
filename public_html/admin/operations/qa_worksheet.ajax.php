<?php
require '../auth.php';
require $xcart_dir."/include/security.php";
require_once($xcart_dir."/include/func/func.itemsearch.php");
require_once($xcart_dir."/include/func/func.order.php");

$action = $HTTP_POST_VARS['action'];
$sort_by  = $HTTP_GET_VARS['sort_by'] == "" ? "orderid" : $HTTP_GET_VARS['sort_by'];
$sort_order  = $HTTP_GET_VARS['sort_order'] == "" ? "desc" : $HTTP_GET_VARS['sort_order'];
$item_id  = $HTTP_POST_VARS['item_id'];
$order_id  = $HTTP_POST_VARS['order_id'];
$associateName  = $HTTP_POST_VARS['associateName'];

function qa_fail_procedure($itemid, $orderid, $reasoncode, $reason, $qa_associate) {
    global $weblog;
    $sql = "UPDATE xcart_order_details SET item_status='A' WHERE itemid='$itemid'";    
    db_query($sql);
    
    func_addComment_order($orderid, $qa_associate, 'QaFailure', $reasoncode, $reason);
    
    $weblog->info("item status change : $item_id QA fail");
}

function qa_pass_procedure($item_id, $order_id, $reasoncode, $qa_associate) {
    global $sql_tbl,$weblog;
    $sql = "UPDATE $sql_tbl[order_details] SET item_status='QD' WHERE itemid='$item_id'";
    db_query($sql);
    func_addComment_order($order_id, $qa_associate, 'QA Pass', $reasoncode, 'Item Marked QA Passed');
    $weblog->info("item status change : $item_id QA pass");
}

global $weblog;
if ($action == 'getData') {	
	$where_clause="";

    if($item_id != "")
       $where_clause = $where_clause . " AND xod.itemid = $item_id ";

    if($order_id!="")
       $where_clause = $where_clause . " AND xod.orderid = $order_id ";
	
    $sql = "SELECT xod.itemid, xod.orderid, a.name assigneeName, xc.image, xc.area_id, xc.orientation_id oid, item_status,
					xo.mobile, queueddate, payment_method, xo.changerequest_status, sp.article_number AS article_number,
			    productid,extra_data,ps.name, amount,pt.group_id AS group_id, 
			    xcl.area_image_final AS preview_image, xc.orientation_final_image_display original_image
			FROM mk_assignee a, mk_product_type pt,mk_product_style ps
				LEFT JOIN mk_style_properties sp on (sp.style_id = ps.id), 
				xcart_orders xo,
				((xcart_order_details xod 
					LEFT JOIN mk_xcart_order_details_customization_rel xc ON (xc.orderid = xod.orderid AND xc.itemid = xod.productid))
					LEFT JOIN mk_xcart_order_customized_area_image_rel xcl ON (xcl.orderid = xod.orderid AND xcl.item_id = xod.productid))
				WHERE pt.id=xod.product_type AND item_status='OD' AND a.id=assignee AND ps.id=product_style AND xod.orderid = xo.orderid";
    
    $sql .= $where_clause;
    
    // TODO this sql has to be changed if the product has more one than one preview image 
    $results = func_query($sql);
	
    foreach ($results as $key => $value) {
    	$results[$key]['queueddate'] = date('d-m-Y H:i:s', $results[$key]['queueddate']);
    }
    
	header('Content-type: text/x-json');
	print json_encode(array("results" => $results));

} else if ($action == "qaUpdate") {
	$item_status = $HTTP_POST_VARS['qaResult'];

	if($item_status == "pass"){
		qa_pass_procedure($item_id, $order_id, "item " . $item_id, $associateName . "/" . $login);
		$weblog->info("item status change : $item_id QA pass");
	} else {
       	$reject_reason  = $HTTP_POST_VARS['reject_reason'];
       	
       	qa_fail_procedure($item_id, $order_id,"item ". $item_id, $item_status . " : ". $reject_reason, $associateName."/".$login);

       	$from ="MyntraAdmin";
       	$loginEmail="alrt_qafailure@myntra.com";

       	$bcc="";
       	$template = "QaFailure";
	   	$args = array("ORDER_ID"	=>$order_id,
		              "ITEM_ID"	=>$item_id, 
					  "REASON"	=> $item_status.": ".$reject_reason,
                      "REPORTED_BY"	=> $associateName	);
       	//sendMessage($template, $args, $loginEmail,$from,$bcc);
   	}

	header('Content-type: text/x-json');
	print json_encode(array("success" => true));
    
} else if ($action == "cvsuploaded") {
	// TODO this is not yet functional in production even
}

?>
