<?php
require '../auth.php';
//include_once("$xcart_dir/include/security.php");
include_once("$xcart_dir/include/func/func.courier.php");
require_once $xcart_dir . '/include/func/func.order.php';
require $xcart_dir . '/include/func/func.itemsearch.php';
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";
include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");
include_once("$xcart_dir/modules/apiclient/SkuApiClient.php");
function parse_request_params($param) {
	if(!empty($param)) {
		$tmp = explode(",", $param);
		$tmp_str = "";
		foreach($tmp AS $key=>$value) {
			$tmp_str .= ","." '".$value."' ";
		}
		return trim($tmp_str, ",");
	}
	return "";
}

function create_search_query($request) {
	
    $sql = "SELECT SQL_CALC_FOUND_ROWS f.order_name, a.orderid, a.itemid, a.item_status, f.status as order_status, f.gift_status, e.name AS product_type_name, d.name AS stylename, f.shipping_method AS shippingMethod, 
				po.value option_value, oq.quantity, sop.sku_id as sku_id, a.jit_purchase, a.amount AS qty, f.qtyInOrder AS total_qty, f.queueddate AS queueddate, 
				f.date as ordered_date, f.s_city as city, f.s_zipcode as zipcode, i.source_name AS source_name, a.completion_time AS completion_time, f.payment_method as payment_method, 
				f.login AS login, a.is_customizable AS customizable, sp.article_number AS article_number, sp.global_attr_article_type AS article_type, sp.global_attr_sub_category AS sub_category, a.product_style, qtyInOrder as totalItems, f.courier_service, f.tracking, f.warehouseid 
			FROM ((((((( xcart_order_details a 
				LEFT JOIN mk_assignee b ON a.assignee=b.id) 
				LEFT JOIN mk_product_style d ON a.product_style=d.id) 
				LEFT JOIN mk_product_type e ON a.product_type=e.id) 
				LEFT JOIN mk_style_properties sp ON a.product_style=sp.style_id)
				LEFT JOIN mk_product_group pg ON pg.id = e.group_id) 
				LEFT JOIN xcart_orders f ON a.orderid=f.orderid) 
				LEFT JOIN mk_sources i ON f.source_id=i.source_id)
				LEFT JOIN mk_order_item_option_quantity oq ON a.itemid=oq.itemid
				LEFT JOIN mk_product_options po ON po.id= oq.optionid
				LEFT JOIN mk_styles_options_skus_mapping sop ON (sop.style_id = sp.style_id AND sop.option_id = po.id)
				";
    
   	$gift = ''; $customizable = "";
   	
	$sql .= " WHERE f.orgid!='1' AND a.item_status != 'IC' AND f.source_id = 1";
	
	if (!empty($gift)) {
		$sql .= " AND f.gift_status = '$gift'";	
	}

	if ($customizable != '') {
		$sql .= " AND a.is_customizable IN (" . substr($customizable, 1) . ")";	
	}
		
	// Warehouse filter
	$wh_id = parse_request_params($request['warehouse_id']);
	if ($wh_id != "") {
		$sql .=	" AND f.warehouseid = $wh_id";
	}
	
	// item status filter
	$ret_str = parse_request_params($request['item_status']);
	if ($ret_str != "") {
		$sql .=	" AND a.item_status IN ($ret_str)";
	}
	

	// order status filter
	$ret_str = parse_request_params($request['order_status']);
	if ($ret_str != "") {
		$sql .=	" AND f.status IN ($ret_str)";
	} 

	// customer name filter - case insensitive pattern match
	if(!empty($request['customer_name'])) {
		//$uc_customer_name = strtoupper($request['customer_name']);
		$uc_customer_name = $request['customer_name'];
		$sql .=" AND f.b_firstname LIKE '%{$uc_customer_name}%'";
	}

	if(!empty($request['order_id'])) {
		$order_id = $request['order_id'];
		$sql .=" AND a.orderid = '$order_id' ";
	}
	
	if(!empty($request['item_id'])) {
		$item_id = $request['item_id'];
		$sql .=" AND a.itemid = '$item_id'";
	}
	
	$from_date = $request['from_date']; $to_date = $request['to_date'];
    if(!empty($from_date) && !empty($to_date))
	{
        $from_date_break = explode("/", $from_date);
        $from_date = $from_date_break[2] . "-" . $from_date_break[0] . "-" . $from_date_break[1];
        //$from_date_timestamp = mktime(0, 0, 0, $from_date_break[0], $from_date_break[1], $from_date_break[2]);

        $to_date_break = explode("/",$to_date);
        $to_date = $to_date_break[2]."-".$to_date_break[0]."-".$to_date_break[1];
        //$to_date_timestamp = mktime(0, 0, 0, $to_date_break[0], $to_date_break[1], $to_date_break[2]);

		//$sql .=" AND (f.queueddate >= '$from_date_timestamp' AND f.queueddate <= '$to_date_timestamp')";
        $sql .= " AND f.queueddate >= unix_timestamp('$from_date') AND f.queueddate <= unix_timestamp('$to_date 23:59:59')";
	}
	
	$jit_purchase = $request['jit_purchase'] == 'true'?true:false;
	if($jit_purchase) {
		$sql .= " AND a.jit_purchase=1";
	}
	
	$is_fragile = $request['is_fragile'] == 'true'?true:false;
	$fragileItems = ereg_replace("[^0-9,]", "", FeatureGateKeyValuePairs::getFeatureGateValueForKey('fragile.article.types'));
	if($is_fragile) {
		$sql .= " AND (sp.global_attr_sub_category in ($fragileItems) OR sp.global_attr_article_type in ($fragileItems))";
	}
	
	$order_from_date = $request['order_from_date']; $order_to_date = $request['order_to_date'];
	
	if (!empty($order_from_date)) {
		$order_from_date_break = explode("/", $order_from_date);
        $order_from_date = $order_from_date_break[2] . "-" . $order_from_date_break[0] . "-" . $order_from_date_break[1];
        
        $sql .= " AND (FROM_UNIXTIME(f.date,'%Y-%m-%d') >= '$order_from_date')";
	}
	
	if (!empty($order_to_date)) {
		$order_to_date_break = explode("/",$order_to_date);
        $order_to_date = $order_to_date_break[2]."-".$order_to_date_break[0]."-".$order_to_date_break[1];
        
        $sql .= " AND (FROM_UNIXTIME(f.date,'%Y-%m-%d') <= '$order_to_date')";
	}
		
	if (!$request['order_items_search'] == 1) {
		$sql .= " and ((f.payment_method='cod' and f.cod_pay_status!='') or f.payment_method!='cod') " ;	
	}
	
	if($request['payment_method'] == "COD") {
		$sql .= " and f.payment_method = 'cod'";
    } else if($request['payment_method'] == "On") {
        $sql .= " and f.payment_method != 'cod' " ;
    }
    
    if($_POST['action'] == 'assign_courier'){
    	$sql .= " and (courier_service is NULL or courier_service = '') and (tracking is NULL or tracking = '')"; 
    }
    
    if($_POST['action'] == 'assign_tracking'){
    	$sql .= " and courier_service is not NULL and courier_service != '' and (tracking is NULL or tracking = '')";
    }
    
	$sort_map_arr = array("itemid" => "a.itemid",
			"orderid" => "a.orderid",
			"payment_method" => "f.payment_method",
			"order_name" => "f.order_name",
			"item_status" => "a.item_status",
			"gift_status" => "f.gift_status",
			"product_type_name" => "e.name",
			"stylename" => "d.name",
			"qty" => "f.qtyInOrder",
			"location_name" => "g.location_name",
			"assignee_name" => "b.name",
			"queueddate" => "f.queueddate",
			"source_name" => "i.source_name",
			"assignment_time" => "a.assignment_time",
			"completion_time" => "a.completion_time"
	);
		
	$sql .= " ORDER BY " . ($request["sort"] != "" ? $sort_map_arr[$request["sort"]] : "a.orderid");
	$sql .= " " . ($request["dir"] == 'DESC' ? "DESC" : "ASC");
	return $sql;
}


if ($_POST['action'] == 'search') {
	$search_sql = create_search_query($_POST);
	
	$start = $_POST['start'] == null ? 0 : $_POST['start'];
	$limit = $_POST['limit'] == null ? 30 : $_POST['limit'];
	
	$search_sql .=" LIMIT $start, $limit";

	$sqllog->debug("File name##item_assignment.php##SQL Info::>$search_sql");
	$itemResults = func_query($search_sql, TRUE);
	$total = func_query("SELECT FOUND_ROWS() total", TRUE);
	
	$final_results = array();
	if($itemResults && count($itemResults) > 0 ) {
		foreach($itemResults as $item) {
			if(!empty($item['sku_id']))
				$skuIds[] = $item['sku_id'];
		}
		
		$sku_details = SkuApiClient::getSkuDetails($skuIds);
		$skuId2CodeMap = array();
		$skuCodes = array();
		foreach($sku_details as $sku) {
			$skuId2CodeMap[$sku['id']] = $sku['code'];
			$skuCodes[] = trim($sku['code']);
		}
		
		$skuCode2Locations = ItemApiClientOld::get_items_locations_for_skucodes($skuCodes, $_POST['warehouse_id']);
		
		$hideOOSOrders = FeatureGateKeyValuePairs::getFeatureGateValueForKey('hideOOSOrdersFromOps');	
		foreach($itemResults AS $key=>$order_item){
			$oosOrder = false;
			if ( $hideOOSOrders && $hideOOSOrders == 'true') {
				if($order_item['sku_id'] && $order_item['sku_id'] != '') {
					$inv_change_log_sql = "select value, pre_inv_count from mk_skus_inv_order_logs il, mk_skus_logs sl where il.sku_log_id = sl.id and sl.reasonid = 12 and il.order_id = ".$order_item['orderid']." and sl.skuid = ".$order_item['sku_id'];
					$inv_change_log_row = func_query_first($inv_change_log_sql);
					$changeIncount = $inv_change_log_row['pre_inv_count'] + $inv_change_log_row['value'];
					if($changeIncount < 0) {
						$oosOrder = true;
					}	
				} else {
					$oosOrder = true;
				}
			}
			
			
			$itemResults[$key]['sku_code'] = $skuId2CodeMap[$order_item['sku_id']];
			if(!$oosOrder) {
				$itemResults[$key]['completion_time'] = $order_item['completion_time'] != null ? date('d-m-Y H:i:s', $order_item['completion_time']) : "--";
				$itemResults[$key]['queueddate'] = $order_item['queueddate'] != null ? date('d-m-Y H:i:s', $order_item['queueddate']) : "--";
				$itemResults[$key]['assignment_time'] = $order_item['assignment_time'] != null ? date('d-m-Y H:i:s', $order_item['assignment_time']) : "--";
				$fragileItems = explode(',', trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('fragile.article.types')));
				$itemResults[$key]['is_fragile'] = (in_array($order_item['sub_category'], $fragileItems) || in_array($order_item['article_type'], $fragileItems)) ?'Yes':'No';
				if(count($skuCode2Locations) > 0) {
					$itemResults[$key]['locationbarcode'] = implode("<br>", $skuCode2Locations[$itemResults[$key]['sku_code']]);
				}
				$itemResults[$key]['local_delivery_location'] = '';
				if($order_item['payment_method'] == 'cod') {
					$serviceable = func_check_courier_cod_serviceability($order_item['zipcode'],'ML');
				}else {
					$serviceable = func_check_courier_prepaid_serviceability($order_item['zipcode'],'ML');
				}
				if($serviceable) {
					$itemResults[$key]['local_delivery_location'] = 'ML';
				}
				$final_results[] = $itemResults[$key];
			}
		}
	}
	
	$retarr = array("results" => $final_results, "count" => $total[0]["total"]);

	header('Content-type: text/x-json');
	print json_encode($retarr);
	
} else if($_POST['action'] == 'assign_courier'){	
	$search_sql = create_search_query($_POST);	
	$itemResultDone = func_query($search_sql, TRUE);
	$orderids = array();
	foreach($itemResultDone as $item){
		$orderids[]=$item['orderid'];
	}	
	
	$uniqueOrderids = array_unique($orderids);
	autoCourierAssignment($_POST['warehouse_id'], $uniqueOrderids);
} else if($_POST['action'] == 'assign_tracking'){
		$search_sql = create_search_query($_POST);
		$itemResultDone = func_query($search_sql, TRUE);
		$orderids = array();
		foreach($itemResultDone as $item){
			$orderids[]=$item['orderid'];
		}
	
		$uniqueOrderids = array_unique($orderids);
		autoTrackingNoAssignment($uniqueOrderids);
}else if ($_POST['action'] == 'download_report') {
	$progresskey = $_REQUEST['progresskey'];
	$xcache = new XCache();
    $progressinfo = array('status'=>'PROCESSING');
    $xcache->store($progresskey."_progressinfo", $progressinfo, 3600);
    
	$search_sql = create_search_query($_POST);
	$itemResultDone = func_query($search_sql, TRUE);

	#
	#generate table
	#
	$file_name_suffix = time();
	$assignment_report = fopen($xcart_dir."/admin/item_assignment_report/item_assignment_report_$file_name_suffix.csv","a");
	$summary_table = "Orderid, Source, ItemId, Orderd Date, Article Id, Status, Gift Status, Is_Fragile, Product Type, Shipping Method, Item Name, Size Option, Sku Code, Location BarCode, Material Sourcing, Quantity, Queued date, Item Assignee, Assignment date, Completed date, Customer Login, Payment Method, TotalItems, City, Ops Location";
	fwrite($assignment_report, $summary_table);
	fwrite($assignment_report, "\r\n");

	if($itemResultDone && count($itemResultDone) > 0) {
	
		foreach($itemResultDone as $item) {
			if(!empty($item['sku_id']))
				$skuIds[] = $item['sku_id'];
		}
		
		$sku_details = SkuApiClient::getSkuDetails($skuIds);
		$skuId2CodeMap = array();
		$skuCodes = array();
		foreach($sku_details as $sku) {
			$skuId2CodeMap[$sku['id']] = $sku['code'];
			if(trim($sku['code']) != ""){
				$skuCodes[] = trim($sku['code']);
			}
		}
		
		$skuCode2Locations = ItemApiClientOld::get_items_locations_for_skucodes($skuCodes, $_POST['warehouse_id']);
	
		$hideOOSOrders = FeatureGateKeyValuePairs::getFeatureGateValueForKey('hideOOSOrdersFromOps');	
	
		$string = "";
		foreach($itemResultDone AS $key=>$array){
			$oosOrder = false;
			if ( $hideOOSOrders && $hideOOSOrders == 'true') {
	           if($array['sku_id'] && $array['sku_id'] != '') {
					$inv_change_log_sql = "select value, pre_inv_count from mk_skus_inv_order_logs il, mk_skus_logs sl where il.sku_log_id = sl.id and sl.reasonid = 12 and il.order_id = ".$array['orderid']." and sl.skuid = ".$array['sku_id'];
					$inv_change_log_row = func_query_first($inv_change_log_sql);
					$changeIncount = $inv_change_log_row['pre_inv_count'] + $inv_change_log_row['value'];
					if($changeIncount < 0) {
						$oosOrder = true;
					}	
				} else {
					$oosOrder = true;
				}
			}
			
			$array['sku_code'] = $skuId2CodeMap[$array['sku_id']];
			
			if(!$oosOrder) {
				$array['ordered_date'] = $array['ordered_date'] != null ? date('d-m-Y H:i:s', $array['ordered_date']) : "--";
				$array['completion_time'] = $array['completion_time'] != null ? date('d-m-Y H:i:s', $array['completion_time']) : "--";
				$array['queueddate'] = $array['queueddate'] != null ? date('d-m-Y H:i:s', $array['queueddate']) : "--";
				$array['assignment_time'] = $array['assignment_time'] != null ? date('d-m-Y H:i:s', $array['assignment_time']) : "--";
				$fragileItems = explode(',', trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('fragile.article.types')));
				$array['is_fragile'] = (in_array($array['sub_category'], $fragileItems) || in_array($array['article_type'], $fragileItems)) ?'Yes':'No';
				$array['size_option'] = $array['option_value'];
				$array['total_quantity'] = $array['quantity'] != null ? $array['quantity'] : $array['qty'];
				
				$arry['size_option'] = str_replace("," , " || ", $array['size_option']);
				$locationbarcodes = implode("#", $skuCode2Locations[$array['sku_code']]);
				
				$status = array("UA" =>  "Unassigned",
						"A" => "Assigned", "I" => "In Progress", "OD" => "Ops Done", "QD" => "QA Done",
		                "D" => "Done", "F" => "Failed", "H" => "On Hold", "S" => "Sent Outside");
				
				$item_status = $status[$array['item_status']];
				
				$string .= $array[orderid] . ", " . $array[source_name] . ", " . $array[itemid] . ", " . $array['ordered_date'] . ", " . $array[article_number] . ", " . $item_status . ", " . 
							$array[gift_status] . ", " . $array[is_fragile]. ", ". $array[product_type_name] . ", " . $array['shippingMethod'] . ", " . $array[stylename]. ", " . $array['size_option'] . "," . $array['sku_code'] . "," . $locationbarcodes. "," . ($array['jit_purchase']==1?"JIT":"") . "," .$array['total_quantity'] . ", " . 
							$array['queueddate'] . ", " . $array['assignee_name'] . ", " . $array[assignment_time]. ", " . $array[completion_time] . ", " .
							$array['login'] . ", " . $array['payment_method']. ", ". $array['totalItems']. ", ". $array['city'];
				$string .= "\r\n";
			}
		}
		fwrite($assignment_report, $string);
		fwrite($assignment_report, "\r\n");
	}
	#
	#Download
	#
	$file = "item_assignment_report_$file_name_suffix.csv";
	
	$progressinfo = array('status'=>'DONE','filename' =>$file);
	$xcache->store($progresskey."_progressinfo", $progressinfo, 3600);
	
} else if ($_POST['action'] == 'assign') {
	
	$itemto = $_POST['ia'];
	$loc = $_POST['il'];
	if ($_POST['item_check'] != '') $item_check = explode(",", $_POST['item_check']);
	assign_items($item_check, $itemto, $loc);
	
	header('Content-type: text/x-json');
	print json_encode(array("success" => true));
	
} else if ($_POST['action'] == 'assign_all') {
	$search_sql = create_search_query($_POST);
	
	$itemResult = func_query($search_sql, TRUE);
	
	$items = "";
	foreach ($itemResult as $key => $value) {
		$items = $items . "," . $itemResult[$key]['itemid'];
	}
	$items = substr($items, 1);
	$item_check = explode(",", $items);
	
	assign_items($item_check, $_POST['ia'], $_POST['il']);
	
	header('Content-type: text/x-json');
	print json_encode(array("success" => true));
	
} else if($_POST['action'] == 'status_change') {
	
	$status = $_POST['status'];
	$time = time();
	$item_id = $_POST['item_id'];
	
	$sql = "UPDATE xcart_order_details SET item_status='" . $_POST['status'] . "'";

	if ($status == "D") {
		$sql = $sql . ", completion_time='$time'";
	} else if ($status == "S") {
		$sql = $sql . ", sent_date='$time'";
	} else if ($status == "F" || $status == "H") {
		$sql = $sql . ", assignee = '' ";
	} else if ($status == "I") {
		$sql_orders = "UPDATE xcart_orders SET status = 'I' WHERE orderid in (SELECT orderid FROM xcart_order_details WHERE itemid='$item_id')";
        db_query($sql_orders);
	}	
	
	$sql = $sql . " WHERE itemid = " . $_POST['item_id'];

	global $weblog;
	$weblog->info($sql);
	
	func_query($sql);
	
	if($status == 'H'){
        $desc = $_POST['onholdreason'];
        $order_id = func_query_first_cell("SELECT orderid FROM xcart_order_details WHERE itemid='$item_id'");

        db_query("UPDATE $sql_tbl[orders] SET status='OH' WHERE orderid='" . $order_id . "'");
        func_addComment_order($order_id, 'Admin', 'note', "Item $item_id On  Hold", $desc);
        
        //Send an e-mail to customer care so that they can get more information about the item.
        $from ="MyntraAdmin";
        $loginEmail="alrt_orderonhold@myntra.com";
        $bcc="";
        $template = "orderOnHold";
        $args = array("ORDER_ID" => $order_id);
        sendMessage($template, $args, $loginEmail, $from, $bcc);

        $weblog->info("order status change : $order_id changed to OH");
    }
	
	header('Content-type: text/x-json');
	print json_encode(array("success" => true));
}

?>
