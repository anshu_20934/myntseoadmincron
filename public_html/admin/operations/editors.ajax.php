<?php
require '../../auth.php';
//include_once("$xcart_dir/include/security.php");

global $weblog;

if ($_POST['action'] == 'create') {
	// Get the parameters from the request and save it into the tables
	$editor_id = $_POST["editor_id"];
	$editor_name = $_POST["editor_name"];
	$description = $_POST["description"];
	
	if (empty($editor_id)) {
		$params = array("name" => $editor_name, "description" => $description);
		$editor_id = func_array2insert("mk_editors", $params);
	} else {
		// Update the mk_editors table
		$sql = "UPDATE mk_editors SET name = '$editor_name', description = '$description' WHERE id = $editor_id";
		func_query($sql);
		
		// delete if there are any existing assignee for this editor
		$sql = "DELETE FROM mk_assignee_editors WHERE editor_id = $editor_id";
		func_query($sql);
	}
	
	// get the id from the previous query 
	$ids = explode(",", $_POST["assignee"]);
	foreach($ids as $assignee) {
		// insert the data into the processing_group_types
		$params = array("editor_id" => $editor_id, "assignee_id" => $assignee);
		func_array2insert("mk_assignee_editors", $params);
	}
	
	$retarr = array("success" => true);

	header('Content-type: text/x-json');
	print json_encode($retarr);

} else if ($_POST['action'] == 'load_data') {
	// Get the parameters from the request and save it into the tables
	$sql = "SELECT e.id, e.name, e.description, GROUP_CONCAT(a.name) assignee, GROUP_CONCAT(a.id) assignee_ids
				FROM mk_editors e, mk_assignee_editors ae, mk_assignee a
					WHERE e.id = ae.editor_id
						AND ae.assignee_id = a.id
					GROUP BY e.id";
					
	$results = func_query($sql);
	
	$retarr = array("success" => true, "results" => $results);

	header('Content-type: text/x-json');
	print json_encode($retarr);

} else if ($_POST['action'] == "delete") {
	$editor_id = $_POST['editor_id'];
	
	$sql = "DELETE FROM mk_editors WHERE id = $editor_id";
	func_query($sql);
	
	// also delete all the assignee_editors from the other table as well
	$sql = "DELETE FROM mk_assignee_editors WHERE editor_id = $editor_id";
	func_query($sql);
	
	header('Content-type: text/x-json');
	print json_encode(array("success" => true));
} 
?>