<?php
require '../auth.php';
include_once("$xcart_dir/include/security.php");
require $xcart_dir . '/admin/download_attachment.php';

// First get the type and the file name
// based on the type we need to link the corresponding folder name
if ($_GET['type'] == "item_assignment") {
	force_download($_GET["filename"], "../../admin/item_assignment_report/");
} else if ($_GET['type'] == "sku_picklist") {
	force_download($_GET["filename"], "../../admin/sku_picklist_report/");
} else if ($_GET['type'] == "sku_list_reports") {
	force_download($_GET["filename"], "../../admin/sku_list_reports/");
} else if ($_GET['type'] == "styles_bulk_upload") {
	force_download($_GET["filename"], "../../admin/report_download/");
}else if($_GET['type'] == "bulk_coupon_generation_output"){
	force_download($_GET["filename"], "../../var/tmp/bulk_coupons_output/");
}


?>