Ext.onReady(function() {
	Ext.QuickTips.init();
	
	var ADDING_MODE = "Now you are in adding mode of New Editor";
	var EDITING_MODE = "Editing mode of Editor now you are..";
	
	var fromStore = new Ext.data.JsonStore({
        data : assigneeData,
        fields: ['id','name'],
        sortInfo: {field: 'name', direction: 'ASC'}
    });
	
	var toStore = new Ext.data.JsonStore({
        data : [],
        fields: ['id','name'],
        sortInfo: {field: 'name', direction: 'ASC'}
    });
	
	var editorsFormPanel = new Ext.FormPanel({
      	xtype: 'form', // since we are not using the default 'panel' xtype, we must specify it
		waitMsgTarget: true,
	 	bodyStyle:'padding:0px 15px 15px 15px',
		labelPad: 20,
		id : 'editorsFormPanel',
		autoWidth : true,
		autoScroll : true,
		frame : true,
		collapsible : true,
		title : "Add/Edit Editors",
		labelWidth : 150,
		layoutConfig: {
			labelSeparator: ':'
		},
		defaults : {
			selectOnFocus: true,
			msgTarget: 'side'
		},
		items: [{
			xtype : 'panel',
			id : 'editor_title',
			title : 'Add the Editor from here'
		}, {
			xtype : 'hidden',
			name : 'editor_id',
			value : ""
		}, {
			xtype : 'textfield',
			name : 'editor_name',
			fieldLabel : 'Editor Name',
			width : 250,
			allowBlank : false
	    }, {
	    	xtype : 'textarea',
	    	name : 'description',
	    	fieldLabel : 'Description',
	    	width : 250,
	    	allowBlank : false
	    }, {
			xtype : 'itemselector',
			name : 'assignee',
			id : 'assignee',
			imagePath : httpLocation + "/admin/extjs/resources/images/",
			fieldLabel: 'Assignee',
			allowBlank : false,
			multiselects: [{
                width: 300,
                height: 200,
                store: fromStore,
                displayField : 'name',
                valueField : 'id'
            },{
                width: 300,
                height: 200,
                store: toStore,
                displayField : 'name',
                valueField : 'id',
                tbar:[{
                    text: 'clear',
                    handler:function(){
                    	editorsFormPanel.getForm().findField('assignee').reset();
	                }
                }]
            }]
	
	    }],
		buttons: [{
			text: 'Save',
			onClick : function() {
				if (editorsFormPanel.getForm().isValid()) {
					editorsFormPanel.getForm().submit({
						url : 'editors.ajax.php',
						params : {"action" : "create"},
	                	waitMsg : "Saving data..",
	                    success : function() {
	                    	editorsFormPanel.getForm().reset();
	                    	editorsStore.load();

	                    	Ext.getCmp("editor_title").setTitle(ADDING_MODE);
	                    },
	    	        	failure : function(form, action) {
		                    editorsFormPanel.getForm().reset();

	    	        		Ext.MessageBox.alert('Error', 'Your request is not submitted please submit it again...');
							return false;
	    	        	}
					});
				}
			}
		}, {
			text: 'Cancel',
			handler : function() {
                editorsFormPanel.getForm().reset();

				Ext.getCmp("editor_title").setTitle(ADDING_MODE);
			}
		}]
	});

	var editorsStore = new Ext.data.Store({
		url: 'editors.ajax.php',
		baseParams : {"action" : "load_data"},
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	fields : ["id", 'name', "description", "assignee", "assignee_ids"]
		}),
		sortInfo:{field : 'name', direction:'ASC'}
	});

	var colModel = new Ext.grid.ColumnModel({
		defaults: {width: 120, sortable: true},
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {header : "Editor Name", sortable: true, dataIndex : 'name'},
	        {header : "Desciption", dataIndex : 'description'},
	        {header : "Assignee", dataIndex : 'assignee', width : 300},
	    ]
	});
	
	var editors = new Ext.grid.GridPanel({
	    store  : editorsStore,
	    cm : colModel,
	    id : 'editors-panel',
	    title : 'Editors',
	    frame : true,
		loadMask : true,
		autoHeight : true,
		stripeRows : true,
		autoScroll : true,
		sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
		viewConfig : {
			enableRowBody : true
		},
	    tbar: [{
			text : 'Add Editors',
			tooltip : 'Add Editors Here',
	   		iconCls : 'add',
			handler : function() {
				Ext.getCmp("editor_title").setTitle(ADDING_MODE);
				
				// reset the form here
				editorsFormPanel.getForm().reset();
			}
		}, '-', {
			text : 'Edit Editor',
	   		iconCls : 'Edit',
			handler : function() {
				var selected = editors.initialConfig.sm.getSelected();
				
				if (selected == undefined) {
					Ext.MessageBox.alert('Error', 'Here the dialog will come with the add form');
					return false;
				} else {
					// Need to set the values
					editorsFormPanel.getForm().reset();
					
					editorsFormPanel.getForm().findField("editor_name").setValue(selected.data["name"]);
					editorsFormPanel.getForm().findField("description").setValue(selected.data["description"]);
					
					// Need to set the product types
					var assigneeIds = selected.data["assignee_ids"].split(",");
					for (var i = 0; i < assigneeIds.length; i++) {
						fromStore.each(function(record) {
                            if (record.data.id == assigneeIds[i]) {
                                toStore.add([record]);
                                fromStore.remove(record);
                            }
	                    });
					}
					editorsFormPanel.getForm().findField("editor_id").setValue(selected.data["id"]);
					Ext.getCmp("editor_title").setTitle(EDITING_MODE);
				}
			}
		}, '-', {
			text : 'Refresh',
	   		iconCls : 'refresh',
			handler : function() {
				editorsStore.load();
				Ext.getCmp("editor_title").setTitle(ADDING_MODE);
			}
		}, '-', {
			text : 'Delete',
			iconCls : 'delete',
			handler : function() {
				var selected = editors.initialConfig.sm.getSelected();
				
				if (selected == undefined) {
					Ext.MessageBox.alert('Error', 'Select atleast one group to delete!!');
					return false;
				} else {
					Ext.MessageBox.confirm('Confirm', 'Are you sure want to delete this editor?', function(choice) {
						if (choice == 'yes') {
							Ext.Ajax.request({
								url : 'editors.ajax.php',
								params : {"action" : "delete", "editor_id" : selected.data["id"]},
							    success : function(response, action) {
							    	editorsStore.load();
									Ext.getCmp("editor_title").setTitle(ADDING_MODE);
								}, failure : function(form, action) {
									Ext.MessageBox.alert('Error', 'There is some problem in deleting the record. Please try again..');
									return false;
								}
							});
						} else {
							return false;
						}
					});
				}
				Ext.getCmp("editor_title").setTitle(ADDING_MODE);
			}
		}]
	});
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
	         editorsFormPanel,
	         editors
		],
		listeners : {
			"render" : function(comp) {
				editorsStore.load();
				Ext.getCmp("editor_title").setTitle(ADDING_MODE);
			}
		}
	});
});