Ext.onReady(function() {
	Ext.QuickTips.init();
	
	var downloaderStore = new Ext.data.Store({
		url: 'downloader.ajax.php',
		baseParams : {"action" : "load_data"},
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  		totalProperty: 'count',
	  	  	fields : ["id", 'run_number', 'item_id', "order_id", "status", "assignee", "editor_name", "processing_group", "product_style_name"]
		}),
		sortInfo:{field : 'item_id', direction:'ASC'},
		remoteSort: true
	});

	var colModel = new Ext.grid.ColumnModel({
		defaults: {width: 120, sortable: true},
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {header : "Run Number", sortable: true, dataIndex : 'run_number'},
	        {header : "Item #", sortable: true, dataIndex : 'item_id'},
	        {header : "Order #", sortable: true, dataIndex : 'order_id'},
	        {header : "Status", sortable: true, dataIndex : 'status'},
	        {header : "Assignee Name", sortable: true, dataIndex : 'assignee'},
	        {header : "Editor Name", sortable: true, dataIndex : 'editor_name'},
	        {header : "Processing Group", sortable: true, dataIndex : 'processing_group'},
	        {header : "Product Style Name", dataIndex : 'product_style_name'},
	    ]
	});
	
	var pagingBar = new Ext.PagingToolbar({
		pageSize : 30,
	    store: downloaderStore,
	    displayInfo: true
	});
	
	var downloaderGridPanel = new Ext.grid.GridPanel({
	    store  : downloaderStore,
	    cm : colModel,
	    id : 'downloder-panel',
	    title : 'Downloader Dashboard',
	    frame : true,
	    bbar : pagingBar,
		loadMask : true,
		autoHeight : true,
		stripeRows : true,
		autoScroll : true,
		sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
		viewConfig : {
			enableRowBody : true
		}
	});
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
		         downloaderGridPanel
		],
		listeners : {
			"render" : function(comp) {
				downloaderStore.load();
			}
		}
	});
});