<?php
require "../auth.php";
require $xcart_dir."/include/security.php";
require_once($xcart_dir."/include/func/func.itemsearch.php");

$sql = "SELECT id, name FROM mk_assignee where status = 1";
$assignee = func_query($sql);

$smarty->assign("assignee", json_encode($assignee));

$smarty->assign("main","editors");
func_display("admin/home.tpl",$smarty);
?>