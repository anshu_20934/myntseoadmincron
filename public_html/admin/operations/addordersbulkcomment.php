<?php 
include_once "../auth.php";
include_once "$xcart_dir/include/security.php";
include_once "$xcart_dir/include/func/func.order.php";
include_once "$xcart_dir/PHPExcel/PHPExel.php";

$action  = $HTTP_POST_VARS['action'];
global $weblog;
$uploadStatus = false;
if(!$action || $action == '')
    $action ='upload';

if ($REQUEST_METHOD == "POST" && $action=='verify') {
	if(!isset($_FILES) && isset($HTTP_POST_FILES))
    	$_FILES = $HTTP_POST_FILES;

    $file_extn = '';
    if(!empty($_FILES['orderidsfile']['name'])) {
            $uploaddir1 = '../../bulkorderupdates/';
            $extension = explode(".",basename($_FILES['orderidsfile']['name']));
            $uploadfile1 = $uploaddir1 . date('d-m-Y-h-i-s').".".$extension[1];
            $file_extn = $extension[1];
            if (move_uploaded_file($_FILES['orderidsfile']['tmp_name'], $uploadfile1)) {
            	$uploadStatus = true;
            }
    }
    $progressKey = $HTTP_POST_VARS['progresskey'];
    
    if($uploadStatus)
    {
    	$commenttitle = $_POST['comment_title'];
	 	$commenttext = $_POST['comment_text'];
	 	
	    if($commenttitle == "") {
	    	$errormsg = "Comment Title is missing";	
	    }
    	if($commenttext == "") {
	    	$errormsg .= "<br>Comment Text is missing";
	    }
	    
	    if(!$errormsg || $errormsg == "") {
	    	
	    	if($file_extn == 'xlsx') {
				$objReader = PHPExcel_IOFactory::createReader('Excel2007');
			} else {
				$objReader = PHPExcel_IOFactory::createReader('Excel5');
			}
			
			$objPHPExcel = $objReader->load($uploadfile1);
	 		$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
	 		$orderids = array();
	 		for($i=2; $i<=$lastRow; $i++){//start from second row ignore heading
		   	    $cellObj = ($objPHPExcel->getActiveSheet()->getCell('A'.$i));
		        $orderid = trim($cellObj->getvalue());
		        
		        if(trim($orderid) != ""){
		        	$orderids[] = $orderid;
		        }
	 		}
	 		
			if(empty($orderids)) {
	 			$errormsg = "No orderids present in the file. Please check the file and upload again.";
	 		} else {
	 			$xcache = new XCache();
	        	$progressinfo = array('total'=>count($orderids), 'completed'=>0);
	        	$xcache->store($progressKey."_progressinfo", $progressinfo, 86400);
	        	foreach ($orderids as $orderid) {
	        		func_addComment_order($orderid, $_POST['login'], 'Note', $commenttitle, $commenttext);
	        	}
	 			$msg = "Added comment for ".count($orderids)." orders successfully";
	 		}
	    }
    } else {
    	$errormsg = "Not able to upload file. Please try again";
    }
    $smarty->assign("progresskey", $progressKey);
    $smarty->assign("message", $msg);
    $smarty->assign("errormessage", $errormsg);
}    
    
if($REQUEST_METHOD == "GET") {
	$uniqueId = uniqid();
	$smarty->assign('progresskey', $uniqueId);  
}

$smarty->assign("action", $action);
$smarty->assign("main", "orders_comments");
func_display("admin/home.tpl", $smarty);

?>