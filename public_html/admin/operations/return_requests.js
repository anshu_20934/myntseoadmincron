Ext.onReady(function() {
	Ext.QuickTips.init();
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
	Ext.Ajax.on('beforerequest', myMask.show, myMask);
	Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
	Ext.Ajax.on('requestexception', myMask.hide, myMask);
	
	var couriersStore = new Ext.data.JsonStore( {
		fields : [ 'code', 'display_name' ],
		data : couriers,
		sortInfo : {
			field : 'display_name',
			direction : 'ASC'
		}
	});
	
	var couriersField = {
    	xtype : 'combo',
    	emptyText : 'Select Courier Service',
    	store : couriersStore,
    	mode: 'local',
    	displayField : 'display_name',
    	valueField : 'code',
    	triggerAction: 'all',
    	fieldLabel : 'Courier Partners',
    	name : 'format'
	} ;
		
	var downloadPanel = new Ext.form.FormPanel( {
		renderTo : 'contentPanel',
		title : 'Download Panel',
		id : 'downloadPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 1},
			defaults : {width : 400, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [{
				items : [couriersField]
			}]
		}],
		buttons : [ {
			text : 'Download Ready to pickup report',
			handler : function() {
				downloadAction('pickup');
			}
		}, {
			text : 'Download Ready to Re-Ship report',
			handler : function() {
				downloadAction('reship');
			}
		}]
	});
	
	var downloadAction = function(reportFor) {
		var form = downloadPanel.getForm();
		var format = "";
		if (form.isValid()) {
			format = form.findField("format").getValue();
		}
		if(format == "") {
			alert("Please select courier service format");
			return;
		}
		if(reportFor == 'pickup') {
			window.open(httpLocation + "/admin/ready_to_pickup_report.php?format="+format);
		} else if(reportFor == 'reship') {
			window.open(httpLocation + "/admin/ready_to_reship_report.php?format="+format);
		}
	}
	
	var statusStore = new Ext.data.JsonStore({
		fields : [ 'code', 'display_name' ],
		data : statusArr,
		sortInfo : {
			field : 'display_name',
			direction : 'ASC'
		}
	});

	var return_status = new Ext.ux.form.SuperBoxSelect({
		xtype : 'superboxselect',
		fieldLabel : 'Return Status',
		emptyText : 'Select Return Status',
		name : 'status',
		anchor : '100%',
		store : statusStore,
		mode : 'local',
		displayField : 'display_name',
		valueField : 'code',
		forceSelection : true
	});
	
	var dateNamesStore = new Ext.data.JsonStore({
		fields : [ 'code', 'display_name' ],
		data : dateNames
	});
	
	var dateNamesField = {
    	xtype : 'combo',
    	emptyText : 'Select Date Type',
    	store : dateNamesStore,
    	mode: 'local',
    	displayField : 'display_name',
    	valueField : 'code',
    	triggerAction: 'all',
    	fieldLabel : 'Date When',
    	name : 'datetype'
	};
	
	var searchPanel = new Ext.form.FormPanel({
		renderTo : 'contentPanel',
		title : 'Search Panel',
		id : 'formPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 2},
			defaults : {width : 450, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [{
				items : [{
					xtype : 'numberfield',
		        	id : 'return_id',
		        	fieldLabel : 'Return Id'
				}]
			}, {
				items: [return_status]
			}, {
				items : [{
					xtype : 'numberfield',
		        	id : 'order_id',
		        	fieldLabel : 'Order Id'
				}]
			}, {
				items : [{
					xtype : 'numberfield',
		        	id : 'item_id',
		        	fieldLabel : 'Item Id'
				}]
			}, {
				items :[{
					xtype : 'textfield',
		        	id : 'login',
		        	fieldLabel : 'Login',
		        	width : 200
				}]
			}, {
				items :[{
					xtype : 'textfield',
		        	id : 'tracking_no',
		        	fieldLabel : 'Tracking No',
		        	width : 200
				}]
			}, {
				items: [dateNamesField]
			}, {
				items: []
			}, {
				items : [{
					xtype : 'datefield',
	                name : 'from_date',
	                width : 150,
	                fieldLabel : "From Date"
				}]
			}, {
				items : [{
					xtype : 'datefield',
					name : 'to_date',
					width : 150,
					fieldLabel : "To Date"
				}]
			}]
		}],
		buttons : [ {
			text : 'Search',
			handler : function() {
				reloadGrid();
			}
		},{
			text : 'Download Report',
			handler : function() {
				var params = getSearchPanelParams();
				params["action"] = "downloadreport";
				Ext.Ajax.request({
					url : 'returnsajax.php',
					params : params,
				    success : function(response, action) {
						var jsonObj = Ext.util.JSON.decode(response.responseText);
						if(jsonObj.success) {
							window.open(httpLocation + jsonObj.file_location);
						} else {
							Ext.MessageBox.alert('Error', jsonObj.msg);
						}
					}
				});
			}
		}]
	});
	
	
	
	function getSearchPanelParams() {
		var form = searchPanel.getForm();
		
		if (form.isValid()) {
			var params = {};
			params["return_id"] = form.findField("return_id").getValue();
			params["order_id"] = form.findField("order_id").getValue();
			params["item_id"] = form.findField("item_id").getValue();
			params["status"] = form.findField("status").getValue();
			params["login"] = form.findField("login").getValue();
			params["tracking_no"] = form.findField("tracking_no").getValue();
			params['date_type'] = form.findField("datetype").getValue();
			params["from_date"] = form.findField("from_date").getRawValue();
			params["to_date"] = form.findField("to_date").getRawValue();
			
			return params;
		} else {
			return false;
		}
	}
	
	function reloadGrid() {
		
		var params = getSearchPanelParams();
		
		if (params == false) {
			Ext.MessageBox.alert('Error', 'Errors in the search form');
			return false;
		}
		params["action"] = "search";
		
		searchResultsStore.baseParams = params;
		searchResultsStore.load();
	}
	
	var sm = new Ext.grid.CheckboxSelectionModel();
	
	var requestIdLinkRender = function(value, p, record) {
		return '<a href="' + httpLocation + '/admin/return_request.php?id=' + record.data["returnid"] + '" target="_blank">' + record.data["returnid"] + '</a>';
	}
	
	var statusRenderer = function(value, p, record) {
		var status = record.data['status'];
		for (var i=0; i< statusArr.length; i++) {
			if (statusArr[i].code == status) {
				return statusArr[i].display_name;
			}
		}
		return status;
	}
	
	var courierRenderer = function(value, p, record) {
		var courier_code = record.data['courier_service'];
		for (var i=0; i< couriers.length; i++) {
			if (couriers[i].code == courier_code) {
				return couriers[i].display_name;
			}
		}
		return courier_code;
	}
	
	var colModel = new Ext.grid.ColumnModel({
		defaults: {
	        sortable: true
	    },
	    columns: [
	        sm,
	        {id : 'returnid', header : "Return Id", sortable: true, width: 75, dataIndex : 'returnid', renderer : requestIdLinkRender},
	        {id : 'orderid', header : "Order ID", sortable: true, width: 75, dataIndex : 'orderid'},
	        {id : 'itemid', header : "Item Id", sortable: false, width: 75, dataIndex : 'itemid'},
	        {id : 'skucode', header : "SKUCode", sortable: false, width: 100, dataIndex : 'skucode'},
	        {id : 'product_display_name', header : "Product Name", sortable: false, width: 100, dataIndex : 'product_display_name'},
	        {id : 'productStyleName', header : "Style Name", sortable: false, width: 100, dataIndex : 'productStyleName'},
	        {id : 'article_number', header : "Article No", sortable: false, width: 100, dataIndex : 'article_number'},
	        {id : 'quantity', header : "Quantity", sortable: false, width: 50, dataIndex : 'quantity'},
	        {id : 'status', header : "Return Status", sortable: false, width: 125, dataIndex: 'status', renderer : statusRenderer},
	        {id : 'return_mode', header : "Return Mode", sortable: false, width: 75, dataIndex : 'return_mode'},
	        {id : 'courier_service', header : "Courier", sortable: false, width: 75, dataIndex : 'courier_service', renderer : courierRenderer},
	        {id : 'couponcode', header : "Refund Coupon", sortable: false, width: 125, dataIndex : 'couponcode'},
	        {id : 'refundamount', header : "Refund Amount", sortable: false, width: 100, dataIndex : 'refundamount'},
	        {id : 'login', header : "Customer Login", sortable: true, width: 150, dataIndex : 'login'},
	        {id : 'name', header : "Customer Name", sortable: true, width: 150, dataIndex : 'name'},
	        {id : 'pickup_address', header : "Address", sortable: false, width: 300, dataIndex : 'pickup_address'},
	        {id : 'createddate', header : "Request Date", sortable: true, width: 125, dataIndex : 'createddate'},
	        {id : 'sentfortrackingdate', header : "Sent Tracking Date", sortable: true, width: 125, dataIndex : 'sentfortrackingdate'},
	        {id : 'pickupinitdate', header : "Pikcup Init Date", sortable: true, width: 125, dataIndex : 'pickupinitdate'},
	        {id : 'pickupdate', header : "PickUp Date", sortable: true, width: 125, dataIndex : 'pickupdate'},
	        {id : 'delivereddate', header : "Delivered Date", sortable: true, width: 125, dataIndex : 'delivereddate'},
	        {id : 'receiveddate', header : "Received Date", sortable: true, width: 125, dataIndex : 'receiveddate'},
	        {id : 'qapassdate', header : "QA Pass Date", sortable: true, width: 125, dataIndex : 'qapassdate'},
	        {id : 'qafaildate', header : "QA Fail Date", sortable: true, width: 125, dataIndex : 'qafaildate'},
	        {id : 'reshippeddate', header : "Re-Shipped Date", sortable: true, width: 125, dataIndex : 'reshippeddate'},
	        {id : 'redelivereddate', header : "Re-Delivered Date", sortable: true, width: 125, dataIndex : 'redelivereddate'}
	    ]
	});
		
	var searchResultsStore = new Ext.data.Store({
		url: 'returnsajax.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['returnid', 'orderid', 'itemid', 'article_number', 'product_display_name', 'productStyleName', 
	  	  	          'skucode', 'quantity', 'status', 'return_mode', 'courier_service', 'couponcode', 'refundamount', 
	  	  	          'login', 'name', 'pickup_address', 'createddate', 'sentfortrackingdate', 'pickupinitdate',
	  	  	          'pickupdate', 'delivereddate', 'receiveddate', 'qapassdate', 'qafaildate', 'reshippeddate', 
	  	  	          'redelivereddate']
		}),
		sortInfo:{field : 'returnid', direction:'ASC'},
		remoteSort: true
	});

	var pagingBar = new Ext.PagingToolbar({
		pageSize : 30,
	    store: searchResultsStore,
	    displayInfo: true
	});
	
	var menuAction = function(status) {
    	var selected = searchResults.initialConfig.sm.getSelected();
		
    	Ext.Ajax.request({
			url : 'returnsajax.php',
			params : {"action" : "status_change", "returnid" : selected.data.returnid,
						"itemid" : selected.data.itemid, "status" : status,
						"login": loginUser},
		    success : function(response, action) {
				reloadGrid();
			}
		});
	}
	
	// from Return Request Queued, pickup initiated or picked up 
	var statusMenu1 = new Ext.menu.Menu({ 
		items: [ 
	        {
	        	text : 'Return Received',
	 	        tooltip : 'Return Received',
	 	        iconCls : 'add',
	 	        handler : function() {
	 	        	menuAction("RRC");
	 	        }
	        }
		] 
	});
	
	// from Return Received 
	var statusMenu2 = new Ext.menu.Menu({ 
		items: [ 
	        {
	        	text : 'Return QA Fail',
        		tooltip : 'Return QA Fail',
        		iconCls : 'add',
        		handler : function() {
        			menuAction("RQF");
        		}
	        }
		] 
	});
	
	// From QA Pass, Refund Issued - 
	var statusMenu3 = new Ext.menu.Menu({ 
		items: [ 
	        {
	        	text : 'Return Item Stocked',
		        tooltip : 'Return Item Stocked',
		        iconCls : 'add',
		        handler : function() {
		        	menuAction("RIS");
		        }
	        }, {
	        	text : 'Return Item Rejected',
		        tooltip : 'Return Item Rejected',
		        iconCls : 'add',
		        handler : function() {
		        	menuAction("RIJ");
		        }
	        }
		] 
	});
	
	// from QA Fail - 
	var statusMenu4 = new Ext.menu.Menu({
	    items: [
	    {
	        text : 'Return Re-shipped',
	        tooltip : 'Return Re-shipped',
	        iconCls : 'add',
	        handler : function() {
	        	menuAction("RRS");
	        }
        }
	]
	});
	
	// From return re-shipped state
	var statusMenu5 = new Ext.menu.Menu({
	    items: [
		{
	        text : 'Reshipped Return Delivered',
	        tooltip : 'Reshipped Return Delivered',
	        iconCls : 'add',
	        handler : function() {
	        	menuAction("RSD");
	        }
        }]
	});
	
	searchResults = new Ext.grid.EditorGridPanel({
	    store  : searchResultsStore,
	    cm : colModel,
	    id : 'searchResults-panel',
	    title : 'Search Results',
	    frame : true,
		loadMask : false,
		autoHeight : true,
		sm: sm,
		bbar : pagingBar,
		stripeRows : true,
		autoScroll : true,
		viewConfig : {
			enableRowBody : true
		},
		tbar:[],
		listeners : {
			'render' : function(comp) {
				// TODO get the focus on the orderid
				Ext.getCmp("return_id").focus();
			},
			"rowcontextmenu" : function(grid, row, event) {
	    		grid.getSelectionModel().selectRow(row);
	    		
	    		var selected = grid.initialConfig.sm.getSelected();
	    		
	    		if ((selected.data.status == 'RPI' || selected.data.status == 'RPU') && selected.data.return_mode != "self") {
	    			/*event.preventDefault();
		    	    event.stopEvent();
		    	    statusMenu1.showAt(event.getXY());*/
	    		} else if (selected.data.status == 'RRC') {
	    			event.preventDefault();
		    	    event.stopEvent();
		    	    statusMenu2.showAt(event.getXY());
	    		} else if (selected.data.status == 'RQP') {
	    			event.preventDefault();
		    	    event.stopEvent();
		    	    statusMenu3.showAt(event.getXY());
	    		} else if (selected.data.status == 'RQF') {
	    			/*event.preventDefault();
		    	    event.stopEvent();
		    	    statusMenu4.showAt(event.getXY());*/
	    		} else if (selected.data.status == 'RRS') {
	    			event.preventDefault();
		    	    event.stopEvent();
		    	    statusMenu5.showAt(event.getXY());
	    		} 
	    	}
		}
	});
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
		     downloadPanel, searchPanel, searchResults
		],
		listeners : {
			'afterrender' : function(comp) {
				if(get_status && get_status != "") {
					searchPanel.getForm().findField("status").setValue(get_status);
				} else {
					searchPanel.getForm().findField("status").setValue("RRQ");
				}
				if(get_login && get_login != "") {
					searchPanel.getForm().findField("login").setValue(get_login);
				}
				if(get_returnid && get_returnid != "") {
					searchPanel.getForm().findField("return_id").setValue(get_returnid);
				}
				if(get_orderid && get_orderid != "") {
					searchPanel.getForm().findField("order_id").setValue(get_orderid);
				}
				reloadGrid();
			}
		}
	});
});
