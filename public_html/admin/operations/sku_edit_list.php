<?php
require_once "../auth.php";
require_once "$xcart_dir/include/security.php";
include_once($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");

$smarty->assign("access", false);
$smarty->assign("main","sku_edit_list");
func_display("admin/home.tpl",$smarty);

/*$tb_mk_skus=$sql_tbl['mk_skus'];


function skuToString($sku){
	$del='[[//]]';
	$str=$sku['sku_id'].$del.$sku['sku_code'].$del.$sku['sku_enabled'].$del.$sku['sku_inv_count'].$del.$sku['sku_wh_count'].$del.$sku['style_name'].$del.$sku['option_value'].$del.$sku['jit_sourced'];
	return $str;
}

if(isset($_GET['mode']) && $_GET['mode'] =='viewlog'){
	require_once '../../include/func/func_sku.php';
	$skucode = mysql_escape_string($_GET["code"]);
	$skuid=getSkuIDByCode($skucode);
	if(empty($skuid))
		$skuid=-1;
	$page = $_GET["page"];
	$page_size=25;
	$start_limit=($page-1)*$page_size;
	$sql="select sl.change_by changeby,sl.value value,sl.lastmodified logdate,sl.extra_info extrainfo,sl.pre_wh_count
	 pwhcount,sl.pre_inv_count pinvcount,r.text rtext,ol.order_id orderid from mk_skus_logs sl inner join 
		mk_inventory_movement_reasons r on r.id=sl.reasonid  and sl.skuid=$skuid left join mk_skus_inv_order_logs ol  on ol.sku_log_id=sl.id order by sl.lastmodified desc limit $start_limit,$page_size";
	$result_set=func_query($sql);
	$sku_logs=array();
	$i=0;
	foreach ($result_set as $row){
		$sku_logs[$i]['changeby']=$row['changeby'];
		$sku_logs[$i]['value']=$row['value'];
		$sku_logs[$i]['logdate']=$row['logdate'];
		$sku_logs[$i]['extrainfo']=$row['extrainfo'];
		$sku_logs[$i]['pwhcount']=$row['pwhcount'];
		$sku_logs[$i]['pinvcount']=$row['pinvcount'];
		
		$sku_logs[$i]['rtext']=$row['rtext'];
		$sku_logs[$i]['orderid']=$row['orderid'];
		$i++;
	}
	$sql="select count(*) as totalcount from mk_skus_logs where skuid=$skuid";
	$result_set=func_query($sql);
	$total_pages=ceil($result_set[0]['totalcount']/$page_size);
	$start_page=$page-4;
	if($start_page<1){
		$start_page=1;
	}
	$end_page=$start_page+8;
	if($end_page>$total_pages){
		$end_page=$total_pages;
	}
	$smarty->assign("start_page",$start_page);
	$smarty->assign("end_page",$end_page);
	$smarty->assign("page",$page);
	$smarty->assign("sku_id",$skuid);
	$smarty->assign("skucode",$skucode);
	$smarty->assign("total_pages",$total_pages);
	$smarty->assign("time",date("m/d/Y - h:i:s",time()));
	$smarty->assign("sku_logs",$sku_logs);
	func_display("admin/sku_logs.tpl",$smarty);
	exit;
}else if(isset($_GET['mode']) && $_GET['mode'] =='getvendor'){
	$sql="select name from mk_inv_vendor_master where code ='".mysql_escape_string($_GET['query'])."'";
	$result_set=func_query($sql);
	if(empty($result_set)){
		echo "INVALID";
		exit;
	}
	$str="";
	foreach ($result_set as $row){
		$str="$row[name]";
	}
	echo $str;
	exit;
}else if(isset($_GET['mode']) && $_GET['mode'] =='getarticle'){
	$sql="select s.code code , s.id, sp.article_number anum,sp.variant_name vnam,s.inv_count as invcount,s.wh_count as whcount from mk_skus s left join mk_styles_options_skus_mapping m on  m.sku_id=s.id 
 	left join mk_style_properties sp on m.style_id=sp.style_id where s.code ='".mysql_escape_string($_GET['query'])."'";
	$result_set=func_query($sql);
	if(empty($result_set)){
		echo "INVALID";
		exit;
	}
	$str="";
	foreach ($result_set as $row){
		$sql = "select lastmodified from mk_skus_logs where skuid = " . $row["id"] . " and reasonid=(select id from mk_inventory_movement_reasons where code ='SINV') ORDER BY id desc LIMIT 0,1";
		$results = func_query($sql);
		
		$tmp = $results[0];
		$str="$row[code][[//]]$row[anum][[//]]$row[vnam][[//]]$row[invcount][[//]]$row[whcount][[//]]$tmp[lastmodified]";
	}
	echo $str;
	exit;
}
else if(isset($_GET['mode']) && $_GET['mode'] =='list_skus')
{
	$sql="SELECT code as code from mk_skus where code like '".mysql_escape_string($_GET['query'])."%'";
	$result_set=func_query($sql);
	$str="";
	foreach ($result_set as $row){
		$str.=$row['code'].',';
	}
	echo $str;
	exit;
	
}else if($_GET['mode']=='update'){
	require_once '../../include/func/func_sku.php';
	if ($_GET['property'] == "SET") {
                // get the sku code and update the inventory and warehouse count to the quantity passed
		$quantity = $_GET["value"];
                $skuCode = $_GET["code"];

		$sql = "SELECT COUNT(*) count
				FROM xcart_orders xo, 
					xcart_order_details xod,
					mk_styles_options_skus_mapping sos,
					mk_skus ms
				WHERE xo.orderid = xod.orderid
					AND xo.status IN ('Q', 'WIP', 'OH')
					AND ((xod.item_status = 'A' AND xod.assignee IN 
						(SELECT id FROM mk_assignee 
							WHERE NAME NOT IN ('No Stock Jersey', 'No Stock NP', 'No Stock Brandshop', 'No Stock T-Shirts')
						)) ||
						xod.item_status NOT IN ('OD', 'QD'))
					AND sos.style_id = xod.product_style
					AND sos.sku_id = ms.id
					AND ms.code = '" . $skuCode . "'";
		
		$rows = func_query($sql);

		$inv_count = $quantity - $rows[0]['count'];
		if ($inv_count < 0) $inv_count = 0;
		$skuid=getSkuIDByCode($skuCode);
		$extra_info['user']=$login;
		$extra_info['value']=$inv_count;
		$extra_info["reason"]=getReasonByCode('SINV');
		$extra_info["reasonid"]=$extra_info["reason"]['id'];
		updateSKUProperty($skuid, 'inv_count', $inv_count, $login, false, $extra_info,true);
        echo "Success: Last operation was successful[[//]]";
        exit();
      }

	$extra_info['user']=$login;
	$extra_info['reasonid']=mysql_real_escape_string($reasonid);
	$skuid=mysql_real_escape_string($_GET['sku_id']);
	$value=mysql_real_escape_string($_GET['value']);
	$extra_info['value']=$value;
	$extra_info["extra_info"]=mysql_real_escape_string($extra_reason);
	if(empty($skuid) && !empty($code)){
		$skuid=getSkuIDByCode(mysql_real_escape_string($code));
	}
	//INV and THRS are always taken as diff while updating
	if($_GET['property']=='INV'){
		$extra_info['reason']=getReasonById($extra_info['reasonid']);
		if($extra_info['reason']['action_type']==='D' && $extra_info['reason']['action_mode_manual']==1){
			$sku_counts=getSkuCounts($skuid);
			if($extra_info['reason']['inv_update']==='1' && $sku_counts['inv_count']+$value<0){
				echo "ERROR: Quantity to decrease cannot be greater than System Inventory Count";
				exit();
			}
			if($extra_info['reason']['wh_update']==='1' && $sku_counts['wh_count']+$value<0){
				echo "ERROR: Quantity to decrease cannot be greater than Warehouse Count";
				exit();
			}
		}
		$extra_info['lotid']=mysql_real_escape_string($lot_id);
		$extra_info['price']=mysql_real_escape_string($price);
		$split=explode('/',$lot_id);
		$extra_info['vendorid']=getVendorIdByCode($split[1]);
		$extra_info['inward_type'] = mysql_real_escape_string($_GET['inwardtype']);
		$updated_sku=updateSKUProperty($skuid, 'inv_count', $value, $login, true,$extra_info);
		echo "Success: Last operation was successful[[//]]".skuToString($updated_sku[$skuid]);
	} else if($_GET['property']=='ENABLED'){
		if($value==='true'){
			$can_enable=canEnableSku($skuid);
			if(!$can_enable){
				echo "ERROR: SKU cannot be enabled ,System Inventory Count is less than 1";
				exit();
			}
			$extra_info['reason']=getReasonByCode("MANE");
		}else{
			$extra_info['reason']=getReasonByCode("MAND");
		}
		$extra_info["reasonid"]=$extra_info["reason"]['id'];
		$updated_sku=updateSKUProperty($skuid, 'enabled', $value, $login, true,$extra_info);
		echo "Success: Last operation was successful[[//]]".skuToString($updated_sku[$skuid]);
	}else if($_GET['property']=='THRS'){
		$extra_info['reason']=getReasonByCode("MANT");
		$extra_info["reasonid"]=$extra_info["reason"]['id'];
		$updated_sku=updateSKUProperty($skuid, 'thrs_count', $value, $login, true,$extra_info);
		echo "Success: Last operation was successful[[//]]".skuToString($updated_sku[$skuid]);
	} else if ($_GET['property']=='JIT_SOURCED') { 
		if($value === 'true'){
			$extra_info['reason']=getReasonByCode("MJE");
		}else{
			$extra_info['reason']=getReasonByCode("MJD");
		}
		$extra_info["reasonid"]=$extra_info["reason"]['id'];
		$updated_sku=updateSKUProperty($skuid, 'jit_sourced', $value, $login, true, $extra_info);
		echo "Success: Last operation was successful[[//]]".skuToString($updated_sku[$skuid]);
	}else{
		echo "Failed: Property Not Found";
	}
	exit();
}else if ($_GET['mode']=='addnew'){

	require_once '../../include/func/func_sku.php';

	$sku_code=mysql_real_escape_string($_GET['sku_code']);
	$sku_inv_count=mysql_real_escape_string($_GET['sku_inv_count']);
	$sku_wh_count=mysql_real_escape_string($_GET['sku_wh_count']);
	if(empty($sku_code)){
		echo "Failed:   SKU code cannot be empty";
		exit();
	}
	if(isSkuExistingByCode($sku_code)){
		echo "Failed:   SKU ($sku_code) already exists";
		exit();
	}
	$updated_sku=addNewSku($sku_code, $sku_inv_count, 1);
	echo "Success: Added new SKU ($sku_code)[[//]]".skuToString($updated_sku[$sku_code]);
	exit();
}

$skus=array();
$inv_reason=array("I"=>array(),"D"=>array());
if(isset($filter)){
	$sql="SELECT ps.name as style_name,po.name as option_name,po.value as option_value,s.id as sku_id,code as sku_code,po.is_active as sku_enabled,s.inv_count as
	sku_inv_count,s.wh_count as sku_wh_count,s.thrs_count as sku_thrs_count, s.jit_sourced from mk_skus s left join mk_styles_options_skus_mapping m on 
    s.id=m.sku_id   left join mk_product_options po on m.option_id=po.id 
    left join mk_product_style ps on ps.id=m.style_id 
    where  s.code is not null and trim(s.code)!='' and s.code like '".$filter."%'
 ORDER BY code";
	$skus_resultset=func_query($sql);
	foreach ($skus_resultset as $row){
		$skus[$row['sku_id']]['sku_id']=$row['sku_id'];
		$skus[$row['sku_id']]['sku_code']=$row['sku_code'];
		$skus[$row['sku_id']]['sku_enabled']=$row['sku_enabled'];
		$skus[$row['sku_id']]['sku_inv_count']=$row['sku_inv_count'];
		$skus[$row['sku_id']]['sku_thrs_count']=$row['sku_thrs_count'];
		$skus[$row['sku_id']]['sku_wh_count']=$row['sku_wh_count'];
		$skus[$row['sku_id']]['jit_sourced']=$row['jit_sourced'];
		$skus[$row['sku_id']]['style_name']=$row['style_name'];
		$skus[$row['sku_id']]['option_value']=$row['option_value'];
	}
}

$sql="SELECT id id,text text,action_type type,code code from mk_inventory_movement_reasons where action_mode_manual=1 and action_type in ('I','D')";
$skus_resultset=func_query($sql);
$others_id_inc;
$others_id_dec;
foreach ($skus_resultset as $row){
	if($row['code']=='NEWP')
		$new_purchase_id_inc=$row['id'];
	$inv_reason[$row['type']][$row['id']]=$row['text'];
	if($row['code']=='OTHI')
		$others_id_inc=$row['id'];
	else if($row['code']=='OTHD')
		$others_id_dec=$row['id'];
}
$checkInStock=FeatureGateKeyValuePairs::getFeatureGateValueForKey('checkInStock');
$smarty->assign("checkInStock", $checkInStock==='true');
$smarty->assign('skus',$skus);
$smarty->assign('new_purchase_id_inc',$new_purchase_id_inc);
$smarty->assign('filter',$filter);
$smarty->assign('others_id_inc',$others_id_inc);
$smarty->assign('others_id_dec',$others_id_dec);
$smarty->assign('inv_reason',$inv_reason);
$smarty->assign("alphabets",array ("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"));
$smarty->assign("main","sku_edit_list");
func_display("admin/home.tpl",$smarty);*/
?>
