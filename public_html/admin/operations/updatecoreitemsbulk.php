<?php
	
include_once('../auth.php');
include_once("$xcart_dir/include/security.php");
include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");

$action = $HTTP_POST_VARS['action'];
if(!$action) 
	$action = 'view';


$quality_check_codes = ItemApiClient::getItemQualityCheckCodes("STORED");

if($action == 'updateitems') {
	$property = $_POST['property'];
	$itemBarcodes = $_POST['item_barcodes_list'];
	$itemBarcodes = explode(",", $itemBarcodes);
	
	if($property == "status") {
		$transition = $_POST['status_change'];
		$transition_states = explode("-", $transition);
		$weblog->info("Perform transition states - ". print_r($transition_states, true));
		$response = ItemApiClientOld::validate_current_item_status($itemBarcodes, $transition_states[0]);
		if($response['success'] == true) {
			$status = ItemApiClientOld::update_items_in_bulk(1, $response['itemids'], array('itemStatus'=> $transition_states[1]), $_POST['user']);
			if($status) {
				$successMsg = "Successfully updated Item Status to $transition_states[1] for all Items";		
			} else {
				$errorMsg = "Failed to update Item Status to  $transition_states[1]. Please try again later";
			}
		}
		else 
			$errorMsg = $response['errorMsg'];
		
	} else {
		$itemIds = ItemApiClientOld::get_itemids_for_barcodes(1, $itemBarcodes, false);
		if(count($itemIds) > 0) {
			$quality_change_reason_id = $_POST['quality_change_reason'];
			$update_data['quality'] = $quality_check_codes[$quality_change_reason_id]['quality'];;
			$update_data['rejectReason'] = $quality_check_codes[$quality_change_reason_id]['rejectReason'];
			$update_data['rejectReasonDescription'] = $quality_check_codes[$quality_change_reason_id]['rejectReasonDescription'];
			
			$response = ItemApiClientOld::update_items_in_bulk(1, $itemIds, $update_data, $_POST['user']);
			if($response === true) {
				$successMsg = "Successfully updated Quality Level to $update_data[quality] for all Items";		
			} else {
				$errorMsg = "Failed to update Quality Level to $update_data[quality]. $response";
			}
		} else {
			$errorMsg = "Failed to fetch Item details. Please try again later";
		}
	}
	
	$smarty->assign("errorMsg", $errorMsg);
	$smarty->assign("successMsg", $successMsg);
}
	
$smarty->assign("action", $action);
$smarty->assign("quality_check_codes", $quality_check_codes);
/*
if($login == 'sanjay.yadav' || $login == 'shrinath'){
	$smarty->assign("main", "update_core_items1");
} else {
	$smarty->assign("main", "update_core_items");
} */
$smarty->assign("main", "update_core_items");
func_display("admin/home.tpl", $smarty);

?>
