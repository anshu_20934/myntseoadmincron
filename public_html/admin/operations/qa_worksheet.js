Ext.onReady(function() {
	Ext.QuickTips.init();

	var headerPanel = new Ext.form.FormPanel( {
		title : 'Select QA Associate',
		id : 'formPanel',
		autoHeight : true,
		bodyStyle : 'padding: 5px',
		width : 500,
		items : [{
        	xtype : 'combo',
        	emptyText : 'Select QA Associate',
        	store : new Ext.data.JsonStore({
        	    fields: ['id', 'name'],
        	    data : assigneeList
        	}),
        	mode: 'local',
        	displayField : 'name',
        	valueField : 'id',
        	triggerAction: 'all',
        	fieldLabel : 'QA Associate',
        	name : 'associate_name',
        	listeners : {
				"select" : function(comp, record, index) {
					var msg = "<div style='color : red'>If you are not <b>" + record.data["name"] + "</b> choose your name from the drop down.</div>";
					Ext.getCmp("msg").update(msg);
				}
			}
		}, {
			xtype : 'panel',
			id : 'msg',
			border : false,
			html : ''
		}]
	});
	
	function getSearchParams() {		
		var params = {};
		params["action"] = "getData";
		params["item_id"] = Ext.getCmp("item_id").getValue();
		params["order_id"] = Ext.getCmp("order_id").getValue();
		
		return params;
	}
	
	function reloadGrid() {
		var params = getSearchParams();
		searchResultsStore.baseParams = params;
		searchResultsStore.load();
	}
	
	var qaPassOrFailRender = function(value, p, record) {
		var retLink = "QA Pass/Fail?";
		return retLink;
	}
	
	var orderIdLinkRender = function(value, p, record) {
		var retLink = '<a target="_blank" href="' + httpLocation + '/admin/view_print_design.php?productId=' + record.data["productid"] + '&orderid' + record.data["orderid"] + '">' + record.data["itemid"] + '</a>';
		return retLink;
	}
	
	var itemIdLinkRender = function(value, p, record) {
		var retLink = '<a target="_blank" href="' + httpLocation + '/admin/order.php?orderid=' + record.data["orderid"] + '">' + record.data["orderid"] + '</a>';
		return retLink;
	}
	
	var expanderTemplate = '<table style="color:#292929; padding-left:4px;padding-bottom:4px;" border="0" width="100%">' +
			'<tr>' +
			'<td width="35%">' +
				'<tpl>' +
					'<img src="' + httpLocation + '/{original_image}" style="height:300px;"/>' +
				'</tpl>'+
			'</td>' +
			'<td width="35%">' +
				'<tpl>' +
					'<img src="' + httpLocation + '/{preview_image}" style="height:300px;"/>' +
				'</tpl>'+
			'</td>' + 
			'<td>' +
				'<div>' +
					'<tpl>'+
						'<table width="100%" >' +
						    '<tr><td ><p><b>Mobile Number : </b><span> {mobile}</span></p></td></tr>' +
							'<tr><td><p><b>Order Date : </b>{ordereddate}</p></td></tr>' +
							'<tr><td ><p><b>Order Queued Date : </b>{queueddate}</p></td></tr>' +
							'<tr><td ><p><b>Payment Method : </b> {payment_method}</p></td></tr>' +
							'<tr><td ><p><b>Change Request Status : </b> {changerequest_status}</p></td></tr>' +
							'<tr><tpl if="changerequest_status==\'Y\'"><b>There is change request associated with this order please check the order details</tpl></tr>' +
						'</table>'+
					'</tpl>' +
				'</div>' +
		'</td></tr></table>';
	
	var expander = new Ext.ux.grid.RowExpander({
        tpl : new Ext.XTemplate(expanderTemplate) 
         /*   '<p><b>Mobile Number : </b> {itemid}</p><br>',
            '<p><b>Order ID:</b> {orderid}</p><br>',
            '<p><b>Item Style Name:</b> {name}</p><br>',
            '<p><b>Amount:</b> {amount}</p><br>',
            '<p><b>assigneeName:</b> {assigneeName}</p><br>',
            '<p><b>Quality Breakup:</b> {quantity_breakup}</p><br>'
        )*/
    });
	
	var itemStatusMenu = new Ext.menu.Menu({
	    items: [
	    {
	        text : 'QA Pass',
	        tooltip : 'QA Pass',
	        handler : function(comp) {
	        	showResultText("ok", "", "pass");
	        }
	    }, {
	        text : 'QAFail: Apparel : T-shirt Size',
	        tooltip : 'QAFail: Apparel : T-shirt Size',
	        handler : function() {
	        	rejectReasonDialog("QAFail: Apparel : T-shirt Size");
	        }
	    }, {
	        text : 'QAFail: Apparel : T-shirt Print not matching',
	        tooltip : 'QAFail: Apparel : T-shirt Print not matching',
	        handler : function() {
	        	rejectReasonDialog('QAFail: Apparel : T-shirt Print not matching');
	        }
	    }, {
	        text : 'QAFail: Apparel : T-shirt Stained',
	        tooltip : 'QAFail: Apparel : T-shirt Stained',
	        handler : function() {
	        	rejectReasonDialog('QAFail: Apparel : T-shirt Stained');
	        }
	    }, {
	        text : 'QAFail: Apparel : T-shirt Torn',
	        tooltip : 'QAFail: Apparel : T-shirt Torn',
	        handler : function() {
	        	rejectReasonDialog('QAFail: Apparel : T-shirt Torn');
	        }
	    }, {
	        text : 'QAFail: Apparel : T-shirt Brandname',
	        tooltip : 'QAFail: Apparel : T-shirt Brandname',
	        handler : function() {
	        	rejectReasonDialog('QAFail: Apparel : T-shirt Brandname');
	        }
	    }, {
	        text : 'QAFail: Non-Apparel : Print not matching',
	        tooltip : 'QAFail: Non-Apparel : Print not matching',
	        handler : function() {
	        	rejectReasonDialog('QAFail: Non-Apparel : Print not matching');
	        }
	    }, {
	        text : 'QAFail: Non-Apparel : Material damaged',
	        tooltip : 'QAFail: Non-Apparel : Material damaged',
	        handler : function() {
	        	rejectReasonDialog('QAFail: Non-Apparel : Material damaged');
	        }
	    }, {
	        text : 'QAFail: OTHER',
	        tooltip : 'QAFail: OTHER',
	        handler : function(comp) {
	        	rejectReasonDialog("QAFail: OTHER");
	        }
	    }]
	});
	
	var qaResultData = [["pass", "QA Pass"], 
			["QAFail: Apparel : T-shirt Size", "QAFail: Apparel : T-shirt Size"],
		    ["QAFail: Apparel : T-shirt Print not matching", "QAFail: Apparel : T-shirt Print not matching"],
		    ["QAFail: Apparel : T-shirt Stained", "QAFail: Apparel : T-shirt Stained"],
		    ["QAFail: Apparel : T-shirt Torn", "QAFail: Apparel : T-shirt Torn"],
		    ["QAFail: Apparel : T-shirt Brandname", "QAFail: Apparel : T-shirt Brandname"],
		    ["QAFail: Non-Apparel : Print not matching", "QAFail: Non-Apparel : Print not matching"],
		    ["QAFail: Non-Apparel : Material damaged", "QAFail: Non-Apparel : Material damaged"],
		    ["QAFail: OTHER", "QAFail: OTHER"]];
	
	var qaResult = "pass";
	
	var rejectReasonDialog = function(msg) {
		qaResult = msg;
		Ext.MessageBox.show({
			title: 'Reject Reason',
			msg: 'Please enter your reject reason:',
			width : 300,
			buttons: Ext.MessageBox.OKCANCEL,
			multiline : true,
			fn : showResultText,
			animEl: 'qafail'
		});
	}
	
	function showResultText(btn, text, msg) {
		if (btn == "ok") {
			var selected = searchResults.initialConfig.sm.getSelected();
			
			if (headerPanel.getForm().findField("associate_name").getRawValue() == "") {
				Ext.MessageBox.alert('Error', 'Please select the QA Associate.');
				return false;
			}
			
			Ext.Ajax.request({
				url : 'qa_worksheet.ajax.php',
				waitMsgTarget : true,
				params : {"action" : "qaUpdate", "associateName" : headerPanel.getForm().findField("associate_name").getRawValue(), 
						"reject_reason" : text, "update" : "update", "order_id" : selected.data["orderid"],
						"item_id" : selected.data["itemid"], "qaResult" : qaResult},
			    success : function(response, action) {
					reloadGrid();
				}
			});
		}
	}
	
	var colModel = new Ext.grid.ColumnModel({
		defaults: {
	        width: 120,
	        sortable: true
	    },
	    columns: [expander,
	        {id : 'order_name', header : "Select QA Pass/Fail", width : 200,
	        	dataIndex : 'order_name', renderer : qaPassOrFailRender, 
	        	editor : {
	        		xtype : 'combo',
	        		store : new Ext.data.ArrayStore({
		        	    fields: ['id', 'name'],
		        	    data : qaResultData
		        	}),
		        	mode: 'local',
		        	displayField : 'name',
		        	valueField : 'id',
		        	triggerAction: 'all',
		        	id : 'qafail',
		        	listeners : {
	        			'select' : function(comp, oldValue, newValue) {
	        				// Here I have to send the Ajax Call to the 
	    					var combo = headerPanel.getForm().findField("associate_name");
	    					var associate = combo.getValue();
	    					
	    					if (associate == "") {
	    						Ext.MessageBox.alert('Error', 'PLease select the QA Associate.');
	    						return false;
	    					}
	    					
	    					if (comp.getValue() != "pass") {
		    					Ext.MessageBox.show({
		    						title: 'Reject Reason',
		    						msg: 'Please enter your reject reason:',
		    						width : 300,
		    						buttons: Ext.MessageBox.OKCANCEL,
		    						multiline : true,
		    						fn : showResultText,
		    						animEl: 'qafail'
		    					});
							} else {
								showResultText("ok", "");
							}
	        			}
	        		}
		        }
	        },
	        {id : 'item_id', header : "Item ID", dataIndex : 'itemid', renderer : orderIdLinkRender},
	        {id : 'order_id', header : "Order ID", dataIndex : 'orderid', renderer : itemIdLinkRender},
	        {id : 'item_style_name', header : "Item Style Name", dataIndex: 'name'},
	        {id : 'article_number', header : "Article Number", dataIndex: 'article_number'},
	        {id : 'quantity', header : "Quantity", dataIndex : 'amount'},
	        {id : 'assignee', header : "Assignee", dataIndex : 'assigneeName'},
	        {id : 'quantity_breakup', header : "Quantity Breakup", dataIndex : 'quantity_breakup'},
	        {id : 'extra_data', header : "Extra Data", dataIndex : 'extra_data'}
	    ]
	});
		
	var searchResultsStore = new Ext.data.Store({
		url: 'qa_worksheet.ajax.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	fields : ['order_name', 'itemid', 'orderid', 'name', 'amount', 'assigneeName', 
	  	  	          'productid', 'quantity_breakup', 'extra_data', 'mobile', 'ordereddate', 'oid',
	  	  	          'area_id', 'payment_method', 'queueddate', 'image', 'date', 
	  	  	          'original_image', 'preview_image', 'changerequest_status', 'article_number']
		}),
		sortInfo:{field : 'orderid', direction:'ASC'},
		listeners : {
			"load" : function(store, records, options) {
				if (records.length > 0) {
					expander.expandRow(0);
				}
			}
		}
	});
	
	var searchResults = new Ext.grid.EditorGridPanel({
	    store  : searchResultsStore,
	    cm : colModel,
	    id : 'searchResults-panel',
	    title : 'Search Results',
		loadMask : true,
		autoHeight : true,
		frame : true,
		sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
		plugins: expander,
		stripeRows : true,
		autoScroll : true,
		tbar : [{
			xtype : 'label',
			html : 'Item ID : '
		}, {
			xtype : 'textfield',
			fieldLabel : 'Item ID',
        	emptyText : 'Search Item ID',
        	id : 'item_id',
        	width : 200
        }, '-', {
			xtype : 'label',
			html : 'Order ID : '
		}, {
        	xtype : 'textfield',
			fieldLabel : 'Order ID',
        	id : 'order_id',
        	width : 200,
        	listeners : {
        		"render" : function(comp) {
        			Ext.getCmp("order_id").focus();
        		}
        	}
		}, '-', {
			text : '<b>Search</b>',
			iconCls : 'submit',
			handler : function() {
				reloadGrid();
			}
		}],
		listeners : {
			"rowcontextmenu" : function(grid, row, event) {
	    		grid.getSelectionModel().selectRow(row);
	    		
	    		var selected = grid.initialConfig.sm.getSelected();
    		
    			event.preventDefault();
	    	    event.stopEvent();
		        itemStatusMenu.showAt(event.getXY());
	    	}
		}
	});
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		id : 'main-panel',
		items : [/*{
			layout : 'table',
			baseCls:'x-plain',
			layoutConfig : {columns : 2, width : '50%'},
			items :[headerPanel, uploadPanel]
		}*/headerPanel, searchResults
		],
		listeners : {
			"render" : function() {
				reloadGrid();
			}
		}
	});
});
