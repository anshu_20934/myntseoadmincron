<?php
require '../../auth.php';
require $xcart_dir."/include/security.php";

global $weblog;

if ($action == "load_data") {
	# need to load all the service codes to the UI
	$sql = "SELECT code, courier_service, website, enable_status FROM mk_courier_service";
	$results = func_query($sql);

	header('Content-type: text/x-json');
	print json_encode(array("results" => $results));
} else if ($action == "add_code") {
	$enable_status = isset($_POST["enable_status"]) ? "1" : NULL;
	$sql = "INSERT INTO mk_courier_service (courier_service, code, website, enable_status) 
				VALUES ('$courier_service', '$code', '$website', $enable_status)";
	$weblog->info($sql);

	db_query($sql);
	
	header('Content-type: text/x-json');
	print json_encode(array("success" => true));
}

?>