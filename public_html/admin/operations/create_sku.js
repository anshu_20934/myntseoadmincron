Ext.onReady(function() {
	Ext.QuickTips.init();
	
	showConfirmWindow = function(results, flag) {
		var html = "";
		for (var i = 0; i < results.length; i++) {
			html = html + "<p>Vendor Article No : " + results[i].vendor_article_no + "</p>";
			html = html + "<p>Vendor Article Name : " + results[i].vendor_article_name + "</p>";
			html = html + "<p>Size : " + results[i].size + "</p>";
			html = html + "<p>Sku Code : " + results[i].code + "</p>";
			html = html + "<p>Remarks : " + results[i].remarks + "</p>";
			html = html + "<p>Brand Name : " + results[i].brand_name + "</p>";
			html = html + "<p>Article Type : " + results[i].article_type_name + "</p>";
			html = html + "<br/><br/>";
		}

		var p2 = new Ext.Panel({
            html : html, 
            buttons : [{
            	text : "Go Back",
            	handler : function() {
            		showSkusWindow.close();
            		Ext.getCmp("generate_add_another").enable();
                	Ext.getCmp("generate_sku_code").enable();
            	}
            }, {
            	text : "Proceed Any Way",
            	handler : function() {
            		submitCreateForm(flag);
            		showSkusWindow.close();
            	}
            }]
        });
        
        showSkusWindow = new Ext.Window({
			title: "WARNING : Similar SKU Codes found!",
			modal: true,
			y: 100,		
			width: 500,
			closable : false
		});
        
        showSkusWindow.add(p2);	
        showSkusWindow.show();
	}
	
	
	function submitCreateForm(flag) {
		createSkuFormPanel.getForm().submit({
			url : httpLocation + '/admin/operations/skus.ajax.php',
			params : {"action" : "create"},
        	waitMsg : "Generating SKU Code",
            success : function() {
            	if (flag) {
            		createSkuFormPanel.getForm().findField("id").setValue('');
            		createSkuFormPanel.getForm().findField("brand_id").setReadOnly(true);
            		createSkuFormPanel.getForm().findField("brand_id").addClass("x-item-disabled");
            		createSkuFormPanel.getForm().findField("article_type_id").setReadOnly(true);
                	createSkuFormPanel.getForm().findField("article_type_id").addClass("x-item-disabled");
                	createSkuFormPanel.getForm().findField("size").setValue('');
                	createSkuFormPanel.getForm().findField("size").clearInvalid();
                	createSkuFormPanel.getForm().findField("remarks").setValue('');
                	createSkuFormPanel.getForm().findField("remarks").clearInvalid()
            	} else {
            		createSkuWindow.close();
            	}
            	reloadGrid();
            	Ext.getCmp("generate_add_another").enable();
            	Ext.getCmp("generate_sku_code").enable();
			},
        	failure : function(form, action) {
				createSkuWindow.close();
			}
		});
	}
	
	function validateAndSubmitCreateForm(createSkuFormPanel, flag) {
		Ext.Ajax.request({
			url : 'skus.ajax.php',
			loadMask: {msg: 'Processing the request'},
			params : {"action" : 'validate_sku',
				"vendor_article_no" : createSkuFormPanel.getForm().findField("vendor_article_no").getValue(),
				"vendor_article_name" : createSkuFormPanel.getForm().findField("vendor_article_name").getValue(),
				"size" : createSkuFormPanel.getForm().findField("size").getValue(),
				"id" : createSkuFormPanel.getForm().findField("id").getValue()},
		    success : function(response, action) {
				var jsonObj = Ext.util.JSON.decode(response.responseText);
				
				if (jsonObj.results != undefined) {
					showConfirmWindow(jsonObj.results, flag);
				} else {
					submitCreateForm(flag);
				}
			}
		});
	}
	
	createNewSku = function(data) {
		createSkuWindow = new Ext.Window({
			title: "Create SKU Code",
			modal: true,
			y: 100,		
			width: 500
		});
	      
		createSkuFormPanel = new Ext.FormPanel({
			waitMsgTarget: true,
		 	bodyStyle:'padding:0px 15px 15px 15px',
			labelPad: 20,
			id : 'createSkuFormPanel',
			autoWidth : true,
		    frame : true,
			autoScroll : true,
			labelWidth : 150,
			layoutConfig: {
				labelSeparator: ':'
			},
			defaults : {
				selectOnFocus: true,
				msgTarget: 'side',
				width : 200
			},
			items: [{
				xtype : 'hidden',
				name : 'id'
			}, {
				xtype : 'textfield',
				name : 'vendor_article_no',
				fieldLabel : 'Vendor Article No.',
				allowBlank : false
			}, {
				xtype : 'textfield',
				name : 'vendor_article_name',
				allowBlank : false,
				fieldLabel : 'Vendor Article Name'
			}, {
				xtype : 'textfield',
				name : 'size',
				allowBlank : false,
				fieldLabel : 'Size'
			}, {
				xtype : 'combo',
        		store : new Ext.data.JsonStore({
	        	    fields: ['id', 'name'],
	        	    data : brandsData
	        	}),
	        	mode: 'local',
	        	displayField : 'name',
	        	valueField : 'id',
	        	hiddenName : 'brand_id',
	        	triggerAction: 'all',
	        	allowBlank : false,
	        	fieldLabel : 'Brand'
			}, {
				xtype : 'combo',
        		store : new Ext.data.JsonStore({
	        	    fields: ['id', 'name'],
	        	    data : articleTypesData
	        	}),
	        	mode: 'local',
	        	displayField : 'name',
	        	valueField : 'id',
	        	triggerAction: 'all',
	        	hiddenName : 'article_type_id',
	        	allowBlank : false,
	        	fieldLabel : 'Article Type'
			}, {
				xtype : 'textfield',
				name : 'remarks',
				allowBlank : false,
				fieldLabel : 'Remarks'
			}],
			buttons: [{
				text: 'Generate and Add Another Size',
				id : 'generate_add_another',
				handler : function(button) {
					if (createSkuFormPanel.getForm().isValid()) {
						Ext.getCmp("generate_add_another").disable();
		            	Ext.getCmp("generate_sku_code").disable();
						validateAndSubmitCreateForm(createSkuFormPanel, true);
					}
				}
			}, {
				text: 'Generate SKU code',
				id : 'generate_sku_code',
				handler : function(button) {
					if (createSkuFormPanel.getForm().isValid()) {
						Ext.getCmp("generate_add_another").disable();
		            	Ext.getCmp("generate_sku_code").disable();
						validateAndSubmitCreateForm(createSkuFormPanel, false);
					}
				}
			}]
		});
		
		if (data != undefined) {
			// set the values of the form from the data
			createSkuFormPanel.getForm().findField("id").setValue(data.id);
			createSkuFormPanel.getForm().findField("vendor_article_no").setValue(data.vendor_article_no);
			createSkuFormPanel.getForm().findField("vendor_article_name").setValue(data.vendor_article_name);
			createSkuFormPanel.getForm().findField("size").setValue(data.size);
			createSkuFormPanel.getForm().findField("brand_id").setValue(data.brand_id);
			createSkuFormPanel.getForm().findField("article_type_id").setValue(data.article_type_id);
			createSkuFormPanel.getForm().findField("remarks").setValue(data.remarks);
			
			// disable brand_id and article_type_id
			createSkuFormPanel.getForm().findField("brand_id").setReadOnly(true);
			createSkuFormPanel.getForm().findField("brand_id").addClass("x-item-disabled");
			createSkuFormPanel.getForm().findField("article_type_id").setReadOnly(true);
			createSkuFormPanel.getForm().findField("article_type_id").addClass("x-item-disabled");
		}
		createSkuWindow.add(createSkuFormPanel);	
		createSkuWindow.show();
	}

	var searchPanel = new Ext.form.FormPanel( {
		renderTo : 'contentPanel',
		title : 'Search Panel',
		id : 'formPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 2},
			defaults : {width : 450, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [{
				items : [{
		        	xtype : 'combo',
		        	emptyText : 'Select Brand',
		        	store : new Ext.data.JsonStore({
		        	    fields: ['id', 'name'],
		        	    data : brandsData
		        	}),
		        	mode: 'local',
		        	displayField : 'name',
		        	valueField : 'id',
		        	hideenName : 'brand',
		        	triggerAction: 'all',
		        	fieldLabel : 'Brands',
		        	name : 'brand'
				}]
			}, {
				items : [{
		        	xtype : 'combo',
		        	emptyText : 'Select Article Type',
		        	store : new Ext.data.JsonStore({
		        	    fields: ['id', 'name'],
		        	    data : articleTypesData
		        	}),
		        	mode: 'local',
		        	displayField : 'name',
		        	valueField : 'id',
		        	triggerAction: 'all',
		        	fieldLabel : 'Article Type',
		        	name : 'article_type',
		        	hiddenName : 'article_type'
		        		
				}]
			}, {
				items : [{
					xtype : 'datefield',
	                name : 'from_date',
	                format : 'Y-m-d',
	                width : 150,
	                fieldLabel : "Code Creation between"
				}]
			}, {
				items : [{
					xtype : 'datefield',
					name : 'to_date',
					format : 'Y-m-d',
					width : 150,
					fieldLabel : "to"
				}]
			}, {
				items :[{
					xtype : 'textfield',
		        	id : 'article',
		        	fieldLabel : 'Vendor Article No'
				}]
			}]
		}],
		buttons : [{
			text : 'Search',
			handler : function() {
				reloadGrid();
			}
		}, {
			text : 'Download As Excel',
			handler : function() {
				var params = getSearchPanelParams();
				params["action"] = "download_report";
				Ext.Ajax.request({
					url : 'skus.ajax.php',
					params : params,
				    success : function(response, action) {
						var jsonObj = Ext.util.JSON.decode(response.responseText);
						window.open(httpLocation + "/admin/operations/download.php?type=sku_list_reports&filename=" + jsonObj.file_location);
					}
				});
			}
		}, {
			text : 'Create New SKU Code',
			handler : function() {
				createNewSku();
			}
		}]
	});
	
	function getSearchPanelParams() {
		var form = searchPanel.getForm();
		
		if (form.isValid()) {
			var params = {};
			params["brand"] = form.findField("brand").getValue();
			params["article_type"] = form.findField("article_type").getValue();
			params["from_date"] = form.findField("from_date").getValue();
			params["to_date"] = form.findField("to_date").getValue();
			params["article"] = form.findField("article").getValue();
			
			return params;
		} else {
			return false;
		}
	}
	
	function reloadGrid() {
		var params = getSearchPanelParams();
		
		if (params == false) {
			Ext.MessageBox.alert('Error', 'Errors in the search form');
			return false;
		}
		params["action"] = "search";
		
		searchResultsStore.baseParams = params;
		searchResultsStore.load();
	}
	
	function editSku() {
		var selected = searchResults.getSelectionModel().getSelected();
		if (selected == undefined) {
			Ext.MessageBox.alert('Error', 'Select the row before editing it');
			return false;
		}
		createNewSku(selected.data);
	}
	
	var colModel = new Ext.grid.ColumnModel({
		defaults: {
	        width: 120,
	        sortable: true
	    },
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {header : "Vendor Article No.", dataIndex : 'vendor_article_no'},
	        {header : "Vendor Article Name", dataIndex : 'vendor_article_name'},
	        {header : "Size", dataIndex : 'size'},
	        {header : "Brand",dataIndex : 'brand_name'},
	        {header : "Article Type", dataIndex : 'article_type_name'},
	        {header : "Sku Code", dataIndex : 'code'},
	        {header : "Remarks", dataIndex : 'remarks'},
	        {header : "Created On", dataIndex : 'created_on'},
	        {header : "Created By", dataIndex : 'created_by'},
	        {header : "Updated By", dataIndex : 'lastuser'},
	        {header : "Updated On", dataIndex : 'last_modified_on'}
	    ]
	});
		
	var searchResultsStore = new Ext.data.Store({
		url: 'skus.ajax.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['brand_name', 'vendor_article_no', 'vendor_article_name', 'article_type_name', 'size', 
	  	  	          'code', 'remarks', 'created_on', 'created_by', 'article_type_id', 'brand_id', 'id',
	  	  	          'last_modified_on', 'lastuser']
		}),
		sortInfo:{field : 'brand_name', direction:'ASC'},
		remoteSort: true
	});
	

	var pagingBar = new Ext.PagingToolbar({
		pageSize : 30,
	    store: searchResultsStore,
	    displayInfo: true
	});
	
	searchResults = new Ext.grid.EditorGridPanel({
	    store  : searchResultsStore,
	    cm : colModel,
	    id : 'searchResults-panel',
	    title : 'Search Results',
	    frame : true,
		loadMask : true,
		autoHeight : true,
		sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
		bbar : pagingBar,
		stripeRows : true,
		autoScroll : true,
		viewConfig : {
			enableRowBody : true
		},
		tbar : [{
    		text : "Edit",
    		handler : function() {
    			editSku();
    		}
    	}, {
    		text : "Refresh",
    		handler : function() {
    			reloadGrid();
    		}
    	}],
		listeners : {
			'render' : function(comp) {
				reloadGrid();
			}
		}
	});
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
		     searchPanel, searchResults
		],
		listeners : {
			'render' : function(comp) {
				reloadGrid();
			}
		}
	});
});
