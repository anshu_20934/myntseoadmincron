Ext.onReady(function() {
	
	var similarProducts = {};

	showChangeItemWindow = function() {
		changeItemWindow = new Ext.Window({
			title: "Change Request",
			modal: true,
			y: 100,		
			closable: false,
			width: 700,
			height : 400,
			autoScroll : true,
			listeners : {
				"show" : function(comp) {
					changeItemFormPanel.getForm().findField("order_id").setValue(orderId);
				}
			}
		});
		
		var buildProductOptions = function(productOptions, styleId, itemid) {
			
			var items = [];
			for (var j = 0; j < productOptions.length; j++) {
				var tmp = {
						xtype : 'numberfield',
						id : productOptions[j].value + ":" + styleId,
						fieldLabel : productOptions[j].value
				};
				items[items.length] = tmp;
			}
						
			var panel = {
					xtype : 'panel',
					title : 'testing:' + itemid + ":" + styleId,
					layout : 'form',
					hidden : true,
					id : 'options:' + styleId,
					items : items
			}
			return panel;
		}
		
		var getImagesRender = function(images) {
			var ret = "<div>";
			
			for (var i = 0; i < images.length; i++) {
				ret = ret + '<img src="' + httpLocation + images[i].thumb_image + '"/>';
			}
			return ret + "</div>"
		}
		
		var staticFields = function(productDetails, i) {
			
			var tmp = {
	            xtype:'fieldset',
	            checkboxToggle:true,
	            title: 'Item Details : ' + productDetails.name + "(" + 
	            			productDetails.amount + ", " + productDetails.quantity_breakup +")", 
	            autoHeight:true,
	            width : 500,
	            defaults: {width: 210},
	            defaultType: 'textfield',
	            collapsed: true,
	            items :[{
	            	xtype : 'hidden',
	            	id : 'item_id:' + i,
	            	value : productDetails.itemid
	            }, {
	            	xtype : 'hidden',
	            	id : 'amount:' + i,
	            	value : productDetails.amount
	            }, {
					xtype : 'panel',
					name : 'original_image',
					html : getImagesRender(productDetails.images)
				}, {
					xtype : 'fileuploadfield',
					width : 200,
					fieldLabel : 'Upload the new image file'
	            }, {
	            	xtype : 'combo',
					store : new Ext.data.JsonStore({
		        	    fields: ['id', 'name'],
		        	    data : []
		        	}),
		        	mode: 'local',
		        	displayField : 'name',
		        	valueField : 'id',
		        	triggerAction: 'all',
		        	id : 'product_style:' + i,
		        	fieldLabel : 'Product Style Name (Similar Products)',
		        	listeners : {
	            		"afterrender" : function(combo) {
			            	if (combo.initialConfig.store.getAt(0) == undefined) {
								Ext.Ajax.request({
									url : httpLocation + '/admin/operations/order.ajax.php',
									waitMsgTarget : true,
									params : {"action" : "similar_product_styles",
												"item_id" : productDetails.itemid},
								    success : function(response, action) {
								    	similarProducts = Ext.util.JSON.decode(response.responseText).results;
								    	
								    	comboData = [];
								    	for (var i = 0; i < similarProducts.length; i++) {
								    		var tmp = {};
								    		tmp["id"] = similarProducts[i]["id"];
								    		tmp["name"] = similarProducts[i]["name"];
								    		comboData[comboData.length] = tmp;
								    		
								    		var panel = buildProductOptions(similarProducts[i]["product_options"], 
								    				similarProducts[i]["id"], productDetails.itemid);
								    		
								    		Ext.getCmp("size:" + productDetails.itemid).add(panel);
							    			Ext.getCmp("size:" + productDetails.itemid).doLayout();
								    	}
								    	
										combo.initialConfig.store.loadData(comboData, false);
										
										var id = combo.getId().split(":");
										Ext.getCmp("item_id:" + id[1]).setValue(productDetails.itemid);
										Ext.getCmp("product_style:" + id[1]).setValue(productDetails.product_style_id);
										
										// Here I have to find the panel with the itemid and in that panel 
										// I have to set the style id
										var items = Ext.getCmp("size:" + productDetails.itemid).items.items;
										for (var i = 0; i < items.length; i++) {
											var optionsId = ("options:" + productDetails.product_style_id);
											if (items[i].id == optionsId) {
												// Show this item 
												items[i].show();
											}
										}
										// Ext.getCmp("options:" + product_style_id).show();
									}
								});
							}
	            		},
	            		"select" : function(combo, record, index) {
	            			// First hide all the options panels then show only required panel
	            			var items = Ext.getCmp("size:" + productDetails.itemid).items.items;
	            			for (var i = 0; i < items.length; i++) {
	            				Ext.getCmp(items[i].id).disable();
	            				Ext.getCmp(items[i].id).hide();
	            			}
	            			Ext.getCmp("options:" + record.data.id).enable();
	            			Ext.getCmp("options:" + record.data.id).show();
	            		}
	            	}
	            }, {
	            	xtype : 'fieldset',
	            	autoWidth : true,
	            	title : 'Product options',
	            	id : 'size:' + productDetails.itemid,
	            	items : []
                }]
	        };
			return tmp;
		}
		
		var productStylesFields = function(productDetails) {
			var fields = [];
			for (var i = 0; i < productDetails.length; i++) {
				var tmp = productDetails[i];
				var similarProductStyles = [];
				
				field = staticFields(productDetails[i], i + 1);
				fields[fields.length] = field;
			}
			return fields;
		}
		
		changeItemFormPanel = new Ext.FormPanel({
			waitMsgTarget: true,
		 	bodyStyle:'padding:0px 15px 15px 15px',
			labelPad: 20,
			id : 'changeItemFormPanel',
			autoWidth : true,
		    frame : true,
			autoScroll : true,
			labelWidth : 150,
			layoutConfig: {
				labelSeparator: ':'
			},
			defaults : {
				selectOnFocus: true,
				msgTarget: 'side',
				width : 200
			},
			items: [{
				xtype : 'hidden',
				name : 'order_id',
				value : ""
			}, {
				xtype : 'datefield',
				name : 'added_at',
				fieldLabel : 'Added At',
				allowBlank : false,
				format : 'd-m-Y',
				value : new Date()
			}, {
				xtype : 'textfield',
				name : 'added_by',
				allowBlank : false,
				fieldLabel : 'Added By'
			}, {
				xtype : 'textarea',
				name : 'description',
				allowBlank : false,
				fieldLabel : "Additional Comments"
			},
			productStylesFields(orderDetails)],
			
			buttons: [{
				text: 'Change',
				handler : function() {
					var form = changeItemFormPanel.getForm();
					if (form.isValid()) {
						var params = {"action" : "items_change", "items_count" : orderDetails.length};
						for (var i = 1; i <= orderDetails.length; i++) {
							var styleId = Ext.getCmp("product_style:" + i).getValue();
							params["product_style_id:" + i] = styleId;
							params["item_id:" + i] = Ext.getCmp("item_id:" + i).getValue();
							params["order_id:" + i] = orderDetails[i-1].orderid;
							
							// Here need to get the options and the amount and need to compare them
							var amount = parseInt(Ext.getCmp("amount:" + i).getValue());

							var optionItems = Ext.getCmp("options:" + styleId).items.items;
							
							var tmp = 0; var quantity_breakup = "";
							
							for (var k = 0; k < optionItems.length; k++) {
								if (optionItems[k].getValue() != "") {
									var id = optionItems[k].id.split(":")[0];
									if (id != "") {
										quantity_breakup = quantity_breakup + "," + id + ":" + optionItems[k].getValue() ;  
									}
									tmp = tmp + parseInt(optionItems[k].getValue());
								}
							}
								
							if (tmp == 0) tmp = 1;
							// Here I have to add the quantity breakup
							params["quantity_breakup:" + i] = quantity_breakup.substr(1); 
								
							if (tmp != amount) {
								// if the old amount and new amount not matched
								Ext.MessageBox.alert('Error', 'number of items are not matched!!!');
								return false;
							}
						}
						
						form.submit({
							url : httpLocation + '/admin/operations/order.ajax.php',
							params : params,
		                	waitMsg : "Adding the change request",
		                    success : function() {
								changeItemWindow.hide();
								itemCommentsStore.load();
								
							},
		    	        	failure : function(form, action) {
								changeItemWindow.hide();
							}
						});
					}
				}
			}, {
				text: 'Cancel',
				handler : function() {
					changeItemWindow.hide();
				}
			}]
		});
		
		changeItemWindow.add(changeItemFormPanel);	
		//changeItemFormPanel.getForm().reset();
		changeItemWindow.show();
	}

	var itemCommentsStore = new Ext.data.Store({
		url: httpLocation + '/admin/operations/order.ajax.php',
		baseParams : {"action" : "load_item_change_log", "order_id" : orderId},
	 	reader: new Ext.data.JsonReader ({
			root: 'results',
	  	  	fields: ['added_by', 'order_id', 'item_id', 'product_style_name', 'quantity_breakup', 'image_path', 'description', 'created_at']
		}),
		sortInfo:{field : 'created_at', direction:'DSC'}
	});
	
	var cm = new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{header: "Changed By", dataIndex: 'added_by'},
		{header: "Orderid", dataIndex: 'order_id'},
		{header: "Itemid", dataIndex: 'item_id'},
		{header: "Product Style Name", dataIndex: 'style_id'},
		{header: "Quantity Breakup", dataIndex: 'quantity_breakup'},
		{header: "View Attachment", dataIndex: 'image_path'},
		{header: "Description", dataIndex: 'description'},
		{header: "Changed At", dataIndex: 'created_at'}
	]);

	var itemCommentsGrid = new Ext.grid.GridPanel({
	    store: itemCommentsStore,
	    cm: cm,
	    title : 'Item Change Log',
	    frame : true,
		autoWidth : true,
		loadMask : true,
		autoHeight : true,
		sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
		stripeRows: true, 
	    listeners : {
	    	"render" : function(comp) {
				itemCommentsStore.load();
			}
	    }
	});
	
	var itemChangesMainPanel = new Ext.Panel({
		renderTo : 'itemsCommentsPanel',
		border : false,
		items : [
	         itemCommentsGrid
		]
	});
	
	var button = Ext.get('change-item-btn');
	
	button.on('click', function(){
		if (typeof(changeItemWindow) == "undefined") {
			showChangeItemWindow();
		} else {
			changeItemWindow.show();
		}
	});
});