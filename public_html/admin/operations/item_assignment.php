<?php
require "../auth.php";
require $xcart_dir."/include/security.php";
require_once($xcart_dir."/include/func/func.itemsearch.php");
require_once($xcart_dir."/include/sendmail.class.php");
require_once($xcart_dir."/modules/apiclient/WarehouseApiClient.php");
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";

$uniqueId = uniqid();
$smarty->assign('progresskey', $uniqueId);

/*$sql = "SELECT * FROM $sql_tbl[operation_location] WHERE status=1";
if (isset($operations_location))
	$sql .= " and operations_location = '$operations_location'";
$locations = func_query($sql);*/

/*$sql = "SELECT * FROM $sql_tbl[mk_assignee] WHERE status=1";
if (isset($operations_location))
	$sql .= " and operations_location = '$operations_location'";
$assignee = func_query($sql);*/

/*$sql = "SELECT id, name from mk_product_group";
$product_groups = func_query($sql);

$sql = "SELECT g.id, g.name, GROUP_CONCAT(pt.id) product_type_ids  
			FROM mk_processing_groups g,	mk_processing_group_types gt, mk_product_type pt 
			WHERE gt.product_type_id = pt.id
				AND gt.processing_group_id = g.id
			GROUP BY g.id";
$processing_groups = func_query($sql);

$sql = "SELECT id, group_id, name from {$sql_tbl[mk_product_type]}";
$product_types = func_query($sql);

$sql = "SELECT id, product_type, name from {$sql_tbl[mk_product_style]}";
if($product_type != "")
  $sql.=" where product_type=$product_type";
$product_styles = func_query($sql);

// Get the sources
$sql = "SELECT source_id AS id, source_name FROM mk_sources";
$pos = func_query($sql);
*/

$myntraOwnedWarehouses = FeatureGateKeyValuePairs::getFeatureGateValueForKey('wms.myntra.warehouses');
$myntraOwnedWarehouses = explode(",", $myntraOwnedWarehouses);

$warehouse_results = WarehouseApiClient::getAllWarehouses();
$warehouses = array();
foreach($warehouse_results as $row) {
	$warehouses[] = array('id'=>$row['id'], 'display_name'=>$row['name']);
}
$smarty->assign("warehouses", json_encode($warehouses));
//$smarty->assign("product_groups", json_encode($product_groups));
//$smarty->assign("processing_groups", json_encode($processing_groups));
//$smarty->assign("product_types", json_encode($product_types));
//$smarty->assign("product_styles", json_encode($product_styles));
//$smarty->assign("locations", json_encode($locations));
//$smarty->assign("assignee", json_encode($assignee));
//$smarty->assign("pos", json_encode($pos));

$smarty->assign("main","new_item_assignment");
func_display("admin/home.tpl",$smarty);
?>

