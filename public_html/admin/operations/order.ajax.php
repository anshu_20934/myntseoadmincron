<?php
require '../auth.php';
//require $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.order.php";

global $weblog;
$time = strtotime($added_at . " " . date('H:i:s'));

/**
 * validate the item aganist the inventory count 
 * 
 * @param $item_id
 * @param $option_id
 * @param $value
 */
function validateItem($item_id, $option_id, $value) {
	$ret = array();
	$sql = "SELECT sosm.sku_id, po.value
				FROM xcart_order_details xod,
					mk_product_options po,
					mk_styles_options_skus_mapping sosm
				WHERE xod.itemid = $item_id
				AND xod.product_style = po.style
				AND po.id = sosm.option_id
				AND po.id = $option_id";
	$results = func_query($sql);
	
	$skuId2InvCount = SkuApiClient::getAvailableInvCountMap($results[0]['sku_id']);
	$invCount = array_sum($skuId2InvCount);
	if ($invCount < $value) {
		$ret["item_id"] = $item_id;
		$ret["message"] = "Ordered count is " . $value . " and inventory count is " . $invCount . " for Item Id : " . $item_id;
	}
	
	return $ret;
}

if ($action == 'load_comments') {
	$sql = "SELECT commentid, commenttype, commentaddedby, FROM_UNIXTIME(commentdate) as commentdate, commenttitle, description,
				newaddress, giftwrapmessage, description, attachmentname 
				FROM `mk_ordercommentslog` 
				WHERE orderid='$order_id' 
				ORDER BY commentdate desc";

	$results = func_query($sql);

	header('Content-type: text/x-json');
	print json_encode(array("results" => $results));

} else if ($action == "add_comment") {
	
	func_addComment_order($order_id, $added_by, $comment_type, $comment_subject, $details);
	
	header('Content-type: text/x-json');
	print json_encode(array("success" => true));
	
} else if ($action == "similar_product_styles") {
	// For now the algo will work like find all the product style whose price is the same as
	// given itemid and belongs to the same product type
	// TODO the same query should return quantity as well
	$sql = "SELECT mps.name, mps.id, '' as product_options
				FROM mk_product_style mps, 
					(SELECT ps.name, ps.price, ps.product_type
					FROM xcart_order_details xod, mk_product_style ps
					WHERE xod.itemid = $item_id
					AND xod.product_style = ps.id) r
				WHERE mps.price = r.price
				AND r.product_type = mps.product_type";

	$results = func_query($sql);
	
	for($i = 0; $i < sizeof($results); $i++) {
		$sql = "SELECT id, value FROM mk_product_options where style = " . $results[$i]["id"];
		$tmp = func_query($sql);
		
		if (isset($tmp[0])) {
			$results[$i]["product_options"] = $tmp;	
		}
	}
	
	header('Content-type: text/x-json');
	print json_encode(array("results" => $results));
	
} else if ($action == "change_request") {
	// Update the change request and add the comment log
	$shipping_addr = "";
	$shipping_addr .= $s_address != "" ? ", s_address = '" . $s_address . "'" : "";
	$shipping_addr .= $s_country != "" ? ", s_country = '" . $s_country . "'" : "";
	$shipping_addr .= $s_state != "" ? ", s_state = '" . $s_state. "'" : "";
	$shipping_addr .= $s_city != "" ? ", s_city = '" . $s_city . "'" : "";
	$shipping_addr .= $s_zipcode != "" ? ", s_zipcode = '" . $s_zipcode . "'" : "";

	$flag = false;
	if ($shipping_addr != "") {
		global $weblog;
		// Add the update code and add the log
		$sql = "UPDATE xcart_orders SET " . substr($shipping_addr, 1) . " WHERE orderid = " . $order_id;
		db_query($sql);

		$address = $s_address . ", " . $s_city;
		func_addComment_order($order_id, $added_by, 'Change Request', 'Shipping Address has been changed..', $description, $address);
		
		$flag = true;
	}

	// Here I have to update gift wrap message
	if ($gift_wrap_msg != "") {
		// TODO here I have to first check weather the current order is gift or not
		$sql = "UPDATE xcart_orders SET notes = '" . $gift_wrap_msg . "' WHERE gift_pack = 'Y' AND orderid = " . $order_id;
		db_query($sql);

		func_addComment_order($order_id, $added_by, 'Change Request', 'Gift wrap message has been changed', $description, '', $gift_wrap_msg);

		$flag = true;
	}

	if ($flag) {
		// We have to set the updatestatus flag to Y
		$sql = "UPDATE xcart_orders SET changerequest_status = 'Y'";
		db_query($sql);
	}
	header('Content-type: text/x-json');
	print json_encode(array("success" => true));
}

else if ($action == "items_change") {
	// Add the current xcart_order_details to the audit table
	$sql = "SELECT itemid, orderid, '', product_style, quantity_breakup FROM xcart_order_details WHERE orderid = " . $_POST['order_id:1'];
	$results = func_query($sql);
	
	foreach($results as $result) {
		// Insert the row into the audit table 
		$sql = "INSERT INTO audit_item_changes(item_id, order_id, image_path, style_id, quantity_breakup, added_by, description) VALUES (";
		$sql .= $result['itemid'] . "," . $result['orderid'] . ", '', " . $result['product_style'] . ", '" . $result['quantity_breakup'] . "', '$added_by', '$description')";
				
		func_query($sql);
	}
		
	// Execute the update query for each item in the order
	for($i = 1; $i <= $_POST['items_count']; $i++) {
		$sql = "UPDATE xcart_order_details SET product_style = " . $_POST['product_style_id:' . $i] . 
					", " . "quantity_breakup = '" . $_POST['quantity_breakup:' . $i] . "' WHERE itemid = " . $_POST["item_id:" . $i];
		func_query($sql);
	}
	
	// UPdate the mkordercomments_log
	func_addComment_order($_POST['order_id:1'], $added_by, 'Change Request', 'Product Items in this order has been changed', $description);
	
	header('Content-type: text/x-json');
	print json_encode(array("success" => true));
}

else if ($action == "load_item_change_log") {
	$sql = "SELECT created_at, order_id, item_id, image_path, quantity_breakup, description FROM audit_item_changes WHERE orderid = $order_id order by id desc";
	$results = func_query($sql);
		
	header('Content-type: text/x-json');
	print json_encode(array("success" => true, "results" => $results));
} else if ($action == "add_replacement_order") {
	// response type is json	
	header('Content-type: text/x-json');
	
	// get the list of items from the order
	$sql = "SELECT orderid, itemid, amount FROM xcart_order_details where orderid = " . $_REQUEST['order_id'];
	$results = func_query($sql);
	
	// validate the request aganist the current inventory count
	$errors = array(); $data = array("order_id" => $_REQUEST["order_id"], "reason" => $_REQUEST["reason"], "items" => array());
	foreach ($results as $index => $row) {
		// check there is replacement exist for this item
		if (isset($_REQUEST[$row['itemid'] . "-checkbox"]) && $_REQUEST[$row['itemid'] . "-checkbox"] == 'on') {
			// now validate the item 
			$option = $_REQUEST['option-' . $row['itemid']];
			$value = $_REQUEST['value-' . $row['itemid']];
			
			$res = array();
			if ($value > $row['amount']) {
				$res["item_id"] = $row["itemid"];
				$res["message"] = "Amount should not be more than " . $row['amount'] . " for item " . $row['itemid'];
			} else {
				// get all the items for the same style
				$sql = "SELECT xod.itemid from xcart_order_details xod, xcart_order_details xod1
							WHERE xod.orderid = xod1.orderid
								AND xod.product_style = xod1.product_style
								AND xod1.itemid = " . $row['itemid'];
				
				$items = func_query($sql);
				$total_value = 0;

				foreach ($items as $index => $item) {
					if (isset($_REQUEST[$item['itemid'] . "-checkbox"]) && $_REQUEST[$item['itemid'] . "-checkbox"] == 'on') {
						if ($_REQUEST['option-' . $item['itemid']] == $option) {
							$total_value = $total_value + $_REQUEST['value-' . $item['itemid']];	
						}
					}
				}
				$res = validateItem($row['itemid'], $option, $total_value);	
			}
			
			$tmp = array("item_id" => $row['itemid'], "option" => $option, "value" => $value);
			
			$data["items"][] = $tmp;
			if (isset($res['item_id'])) {
				$errors[] = $res;
			}
		}
	}
	
	// if there are no items selected for replacement throw add it to errors
	if (!isset($data["items"][0])) {
		$errors[] = array("message" => "No items are selected for replacement");
	}
	
	// throw the validation errors
	if (isset($errors[0])) {
		print json_encode(array("failure"=> true, "errors" => $errors));
		return;
	}
	
	// insert the new order and update the inventory there it self
	$arr = func_create_new_replacement_order($data, $login);
	
	// check the orderid is created or not if it is add the entry in the replacement order info table
	if ($arr['order_id'] == 0) {
		print json_encode(array("failure"=> true, "failureMsg" => $arr['message']));
		return;
	}
    $replacementInfoQuery = "Insert into replacement_order_info value( " . $arr['order_id'] . ", " . $_REQUEST['order_id'] 
    				. ", " .time().")";
    func_query($replacementInfoQuery);
    
    // dispactch the final response
	print json_encode(array("success" => true, "message" => $arr["message"]));
}
?>