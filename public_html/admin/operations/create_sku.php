<?php
require "../auth.php";
require $xcart_dir."/include/security.php";

$allow = false;

if($allow) { 
	$sql = "SELECT id, attribute_value AS name FROM mk_attribute_type_values WHERE attribute_type = 'Brand' and is_active = 1";
	$brands = func_query($sql);
	
	$sql = "SELECT id, typename as name from mk_catalogue_classification where parent1 != -1 and parent2 != -1";
	$articleTypes = func_query($sql);
	
	$smarty->assign("brands", json_encode($brands));
	$smarty->assign("articleTypes", json_encode($articleTypes));
}
$smarty->assign("accessallowed", $allow);
$smarty->assign("main","create_sku");
func_display("admin/home.tpl", $smarty);
?>

