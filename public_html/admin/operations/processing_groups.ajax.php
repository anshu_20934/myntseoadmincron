<?php
require '../../auth.php';

global $weblog;

if ($_POST['action'] == 'create') {
	// Get the parameters from the request and save it into the tables
	$group_id = $_POST["group_id"];
	$group_name = $_POST["group_name"];
	$description = $_POST["description"];
	
	if (empty($group_id)) {
		$params = array("name" => $group_name, "description" => $description);
		$group_id = func_array2insert("mk_processing_groups", $params);
	} else {
		// Update the mk_processing_groups table
		$sql = "UPDATE mk_processing_groups SET name = '$group_name', description = '$description' WHERE id = $group_id";
		func_query($sql);
		
		// delete if there are any existing product types for this group
		$sql = "DELETE FROM mk_processing_group_types WHERE processing_group_id = $group_id";
		func_query($sql);
	}
	
	// get the id from the previous query 
	$types = explode(",", $_POST["product_types"]);
	foreach($types as $product_type) {
		// insert the data into the processing_group_types
		$params = array("processing_group_id" => $group_id, "product_type_id" => $product_type);
		func_array2insert("mk_processing_group_types", $params);
	}
	
	$retarr = array("success" => true);

	header('Content-type: text/x-json');
	print json_encode($retarr);

} else if ($_POST['action'] == 'load_data') {
	// Get the parameters from the request and save it into the tables
	$sql = "SELECT g.id, g.name, g.description, GROUP_CONCAT(pt.name) product_types, GROUP_CONCAT(pt.id) product_type_ids
			FROM mk_processing_groups g, mk_processing_group_types t, mk_product_type pt
				WHERE g.id = t.processing_group_id
					AND t.product_type_id = pt.id
					GROUP BY g.id";
					
	$results = func_query($sql);
	
	$retarr = array("success" => true, "results" => $results);

	header('Content-type: text/x-json');
	print json_encode($retarr);

} else if ($_POST['action'] == "delete") {
	$group_id = $_POST['group_id'];
	
	$sql = "DELETE FROM mk_processing_groups WHERE id = $group_id";
	func_query($sql);
	
	// also delete all the processing_group_types from the other table as well
	$sql = "DELETE FROM mk_processing_group_types WHERE processing_group_id = $group_id";
	func_query($sql);
	
	header('Content-type: text/x-json');
	print json_encode(array("success" => true));
} 
?>