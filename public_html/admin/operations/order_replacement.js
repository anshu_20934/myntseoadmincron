Ext.onReady(function() {
	
	/** 
	 * this function returns the item formpanel 
	 * based on the items in the order
	 */
	function getItemDetails() {
		var items = [];

		for (var i = 0; i < orderDetails.length; i++) {
			var hiddenName = 'option-' + orderDetails[i].itemid;
			items[items.length] = {
				xtype : 'fieldset',
				checkboxToggle:true,
				collapsed: true,
				id : orderDetails[i].itemid,
				title : orderDetails[i].itemid + " - " + orderDetails[i].name + " - " + orderDetails[i].quantity_breakup,
				items : [{
					layout:'column',
					items : [{
						columnWidth : .3,
						items : [{
							xtype : 'label',
							html : '<img src="' + orderDetails[i].item_image + '"/>'
						}]
					}, {
						columnWidth : .3,
						layout : 'form',
						labelWidth : 50,
						items : [{
							xtype : 'combo',
							store : new Ext.data.JsonStore({
				        	    fields: ['id', 'value'],
				        	    data : orderDetails[i].product_options
				        	}),
				        	mode: 'local',
				        	displayField : 'value',
				        	valueField : 'id',
				        	hiddenName : hiddenName,
				        	triggerAction: 'all',
				        	forceSelection : true,
				        	name : 'option-' + orderDetails[i].itemid,
				        	width : 50,
				        	fieldLabel : 'Option'
						}]
					}, {
						columnWidth : .3,
						layout : 'form',
						labelWidth : 50,
						items : [{
							xtype : 'numberfield',
							width : 50,
							minValue : 1,
							msgTarget: 'side',
				        	fieldLabel : 'Value',
				        	name : 'value-' + orderDetails[i].itemid,
				        	id : 'value-' + orderDetails[i].itemid,
				        	listeners : {
				        		"blur" : function(field) {
				        			// Check the new value with the quantity of this item
				        			var itemid = field.name.split("-")[1];
				        			for (var i = 0; i < orderDetails.length; i++) {
				        				if (orderDetails[i].itemid == itemid && orderDetails[i].amount < field.getValue()) {
				        					// mark the field as invalid
				        					field.markInvalid("Replacement quantity should not be more than ordered");
				        					return false;
				        				}
				        			}
				        		}
				        	}
						}]
					}]
				}], 
				listeners : {
					"beforeexpand" : function(panel) {
					}
				}
			};
		}
		return items;
	}

	/**
	 * this function to display the dialog for the send replacements
	 */
	showAddReplacementWindow = function() {
		addReplacementWindow = new Ext.Window({
			title: "Create Replacement Order",
			modal: true,
			y: 100,		
			closable: false,
			width: 700
		});
	      
		addReplacementFormPanel = new Ext.FormPanel({
			waitMsgTarget: true,
		 	bodyStyle:'padding:0px 15px 15px 15px',
			labelPad: 20,
			id : 'addReplacementFormPanel',
			autoWidth : true,
		    frame : true,
			autoScroll : true,
			labelWidth : 150,
			layoutConfig: {
				labelSeparator: ':'
			},
			defaults : {
				selectOnFocus: true,
				msgTarget: 'side',
				width : 200
			},
			items: [{
				xtype : 'hidden',
				name : 'order_id',
				value : orderId
			}, {
				xtype : 'textfield',
				name : 'name',
				value : userName,
				fieldLabel : 'Added By',
				readOnly : true
			}, {
				xtype : 'panel',
				layout : 'form',
				width : '100%',
				labelPad: 20,
				items : getItemDetails() 
			}, {
				xtype : 'textarea',
				name : 'reason',
				allowBlank : false,
				fieldLabel : 'Replacement Reason'
			}],
			buttons: [{
				text: 'Create Replacement Order',
				handler : function() {
					if (addReplacementFormPanel.getForm().isValid()) {
						addReplacementFormPanel.getForm().submit({
							url :  httpLocation + '/admin/operations/order.ajax.php',
							params : {"action" : "add_replacement_order"},
		                	waitMsg : "Saving the comment",
		                    success : function(form, action) {
		                    	var jsonObj = Ext.util.JSON.decode(action.response.responseText);
		                    	Ext.MessageBox.alert('SUCCESS', jsonObj.message);
		                    	
		                    	addReplacementWindow.close();
							},
		    	        	failure : function(form, action) {
		    	        		// here display the errors 
		    	        		var errors = Ext.util.JSON.decode(action.response.responseText).errors;
		    	        		
		    	        		var msgs = "";
		    	        		for(var i = 0; i < errors.length; i++) {
		    	        			msgs += errors[i].message + "<br/>";
		    	        		}
		    	        		Ext.MessageBox.alert('Error', msgs);
		    	        		return false;
							}
						});
					}
				}
			}, {
				text: 'Cancel',
				handler : function() {
					addReplacementWindow.close();
				}
			}]
		});
		
		if ((paymentMethod == 'on' && orderStatus != "C") || 
				(paymentMethod == 'cod' && (orderStatus != 'SH' && orderStatus != "C"))) {
			// just throw an alert saying replacement order cannot be created
			Ext.MessageBox.alert('Error', 'You cannot create the replacement order because order is correct state (' 
					+ paymentMethod + " - " + orderStatus + ')');
			return false;
		}
		addReplacementWindow.add(addReplacementFormPanel);
		addReplacementWindow.show();
	}
	
});