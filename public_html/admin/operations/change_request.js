Ext.onReady(function() {
	
	var similarProducts = {};

	showAddCommentWindow = function() {
		addCommentWindow = new Ext.Window({
			title: "Add Comment",
			modal: true,
			y: 100,		
			closable: false,
			width: 700,
			listeners : {
				"show" : function(comp) {
					addCommentFormPanel.getForm().findField("order_id").setValue(orderId);
				}
			}
		});
	      
		addCommentFormPanel = new Ext.FormPanel({
			waitMsgTarget: true,
		 	bodyStyle:'padding:0px 15px 15px 15px',
			labelPad: 20,
			id : 'addCommentFormPanel',
			autoWidth : true,
		    frame : true,
			autoScroll : true,
			labelWidth : 150,
			layoutConfig: {
				labelSeparator: ':'
			},
			defaults : {
				selectOnFocus: true,
				msgTarget: 'side',
				width : 200
			},
			items: [{
				xtype : 'hidden',
				name : 'order_id',
				value : ''
			}, {
				xtype : 'datefield',
				name : 'added_at',
				fieldLabel : 'Added At',
				format : 'd-m-Y',
				allowBlank : false,
				value : new Date()
			}, {
				xtype : 'textfield',
				name : 'added_by',
				allowBlank : false,
				fieldLabel : 'Added By'
			}, {
				xtype : 'combo',
        		store : new Ext.data.ArrayStore({
	        	    fields: ['id', 'name'],
	        	    data : [['Operations Note', 'Operations Note'], 
	        	            ['Customer Service Note', 'Customer Service Note'], 
	        	            ['Logistics Note', 'Logistics Note']]
	        	}),
	        	mode: 'local',
	        	displayField : 'name',
	        	valueField : 'id',
	        	triggerAction: 'all',
	        	id : 'comment_type',
	        	fieldLabel : 'Comment Type'
			}, {
				xtype : 'textfield',
				name : 'comment_subject',
				fieldLabel : 'Comment Subject'
			}, {
				xtype : 'textarea',
				name : 'details',
				allowBlank : false,
				fieldLabel : 'Details'
			}],
			buttons: [{
				text: 'Change',
				handler : function() {
					if (addCommentFormPanel.getForm().isValid()) {
						addCommentFormPanel.getForm().submit({
							url : httpLocation + '/admin/operations/order.ajax.php',
							params : {"action" : "add_comment"},
		                	waitMsg : "Saving the comment",
		                    success : function() {
								addCommentWindow.hide();
								commentsStore.load();
							},
		    	        	failure : function(form, action) {
								addCommentWindow.hide();
							}
						});
					}
				}
			}, {
				text: 'Cancel',
				handler : function() {
					addCommentWindow.hide();
				}
			}]
		});
		
		addCommentWindow.add(addCommentFormPanel);	
		addCommentWindow.show();
	}
	
	showChangeRequestWindow = function() {
		changeRequestWindow = new Ext.Window({
			title: "Change Request",
			modal: true,
			y: 100,		
			closable: false,
			width: 700,
			height : 400,
			autoScroll : true,
			listeners : {
				"show" : function(comp) {
					changeRequestFormPanel.getForm().findField("order_id").setValue(orderId);
				}
			}
		});

		changeRequestFormPanel = new Ext.FormPanel({
			waitMsgTarget: true,
		 	bodyStyle:'padding:0px 15px 15px 15px',
			labelPad: 20,
			id : 'changeRequestFormPanel',
			autoWidth : true,
		    frame : true,
			autoScroll : true,
			labelWidth : 150,
			layoutConfig: {
				labelSeparator: ':'
			},
			defaults : {
				selectOnFocus: true,
				msgTarget: 'side',
				width : 200
			},
			items: [{
				xtype : 'hidden',
				name : 'order_id',
				value : ""
			}, {
				xtype : 'datefield',
				name : 'added_at',
				fieldLabel : 'Added At',
				allowBlank : false,
				format : 'd-m-Y',
				value : new Date()
			}, {
				xtype : 'textfield',
				name : 'added_by',
				allowBlank : false,
				fieldLabel : 'Added By'
			}, {
				xtype : 'textfield',
				name : 'gift_wrap_msg',
				fieldLabel : 'Gift Wrap Message'
			}, {
				xtype : 'textarea',
				name : 'description',
				allowBlank : false,
				fieldLabel : "Additional Comments"
			},//ordersGrid, 
			// Display the current shipping address here
			{
				xtype : 'panel',
				width : 500,
				bodyStyle : 'padding-bottom : 10px;',
				title : "Current Shipping address : " + orderDetails[0].s_address + ", " + orderDetails[0].s_city + ", " + 
							orderDetails[0].s_state + ", " + orderDetails[0].s_country + ", " + orderDetails[0].s_zipcode
			}, {
				xtype:'fieldset',
				id : 'shippingaddress',
	            title: 'Change Shipping Address',
	            collapsible: true,
	            collapsed: true,
	            width : 500,
	            autoHeight : true,
	            defaults : {width : 210},
	            items : [{
					xtype : 'textarea',
					name : 's_address',
					fieldLabel : 'Address'
				}, {
					xtype : 'combo',
					store : new Ext.data.JsonStore({
		        	    fields: ['code', 'name'],
		        	    data : countries
		        	}),
		        	mode: 'local',
		        	displayField : 'name',
		        	valueField : 'code',
		        	triggerAction: 'all',
		        	name : 'country',
		        	fieldLabel : 'Country'
				}, {
					xtype : 'combo',
					store : new Ext.data.JsonStore({
		        	    fields: ['stateid', 'name', 'code'],
		        	    data : states
		        	}),
		        	mode: 'local',
		        	displayField : 'name',
		        	valueField : 'code',
		        	triggerAction: 'all',
		        	name : 'state',
		        	fieldLabel : 'State'
				}, {
					xtype : 'textfield',
					name : 's_city',
					fieldLabel : 'City'
				}, {
					xtype : 'numberfield',
					name : 's_zipcode',
					fieldLabel : 'Zipcode'
				}]
			}],
			
			buttons: [{
				text: 'Change',
				handler : function() {
					var form = changeRequestFormPanel.getForm();
					if (form.isValid()) {
						var params = {"action" : "change_request", "items_count" : orderDetails.length,
								"s_state" : form.findField("state").getValue(),
								"s_country" : form.findField("country").getValue()};
						form.submit({
							url : httpLocation + '/admin/operations/order.ajax.php',
							params : params,
		                	waitMsg : "Adding the change request",
		                    success : function() {
								changeRequestWindow.hide();
								commentsStore.load();
								
							},
		    	        	failure : function(form, action) {
								changeRequestWindow.hide();
							}
						});
					}
				}
			}, {
				text: 'Cancel',
				handler : function() {
					changeRequestWindow.hide();
				}
			}]
		});
		
		changeRequestWindow.add(changeRequestFormPanel);	
		changeRequestWindow.show();
	}

	var requestTypeRender = function(value, p, record) {
		if (record.data["type"] == "change_req") {
			return "Change Request";
		} 
		return record.data["commenttype"];
	}
	
	var commentsStore = new Ext.data.Store({
		url: httpLocation + '/admin/operations/order.ajax.php',
		baseParams : {"action" : "load_comments", "order_id" : orderId},
	 	reader: new Ext.data.JsonReader ({
			root: 'results',
	  	  	fields: ['commentid', 'commentaddedby', 'commentdate', 'commenttitle', 'commenttype', 'description',
	  	  	         'newaddress', 'giftwrapmessage']
		})
	});
	
	var cm = new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{header: "Type", dataIndex: 'commenttype', width : 70, renderer : requestTypeRender},
		{header: "Added By", dataIndex: 'commentaddedby'},
		{header: "Title", dataIndex: 'commenttitle', width : 300},
		{header: "New Address", dataIndex: 'newaddress'},
		{header: "GiftWrap Msg", dataIndex: 'giftwrapmessage'},
		{header: "Comment Date", dataIndex: 'commentdate'},
		{header: "Description", dataIndex: 'description', width : 300},
		{header: "View Attachment", dataIndex: 'attachmentname'},
		{header: "Image Text", dataIndex: 'image_text'}
	]);

	var commentsGrid = new Ext.grid.GridPanel({
	    store: commentsStore,
	    cm: cm,
	    title : 'Order Comments',
	    frame : true,
		autoWidth : true,
		loadMask : true,
		autoHeight : true,
		sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
		stripeRows: true, 
	    tbar: [{
			text : 'Add Comment',
			tooltip : 'Add comments',
	   		iconCls : 'add',
			handler : function() {
		    	if (typeof(addCommentWindow) == "undefined") {
		    		showAddCommentWindow();
				} else {
					addCommentWindow.show();	
				}
	    	}
		}, '-', {
			text : 'Add Change Request',
			tooltip : 'Add Change Request',
	   		iconCls : 'add',
			handler : function() {
				if (typeof(changeRequestWindow) == "undefined") {
					showChangeRequestWindow();
				} else {
					changeRequestWindow.show();	
				}
			}
		}, '-', {
			text : "Refresh",
			iconCls : 'refresh',
			handler : function() {
				commentsStore.load();
			}
		}],
	    listeners : {
	    	"render" : function(comp) {
				commentsStore.load();
			}
	    }
	});
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
	         commentsGrid
		]
	});
});