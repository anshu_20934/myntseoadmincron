<?php

include_once "../auth.php";
include_once "$xcart_dir/include/security.php";
include_once "$xcart_dir/modules/apiclient/ItemApiClient.php";
include_once "$xcart_dir/modules/apiclient/LocationApiClient.php";

$action = $HTTP_POST_VARS['action'];

if(!$action) 
	$action = 'view';
	
if($action == 'validateCartonOrders') {
	$cartonBarCodes = trim($HTTP_POST_VARS['cartonBarCodes']);
	$cartonBarCodes = explode("\n", $cartonBarCodes);
	$results = ItemApiClient::getOrderIdsForItemsInCartons($cartonBarCodes);
	$moveableCartonBarCodes = $results[0];
	$unmoveableCartonBarCodes = $results[1];
	$invalidCartonBarcodes = $results[2];
	$carton2OrderIdMap = $results[3];
	$weblog->info("Movable Cartons = ". print_r($moveableCartonBarCodes, true));
	$weblog->info("Un Movable Cartons = ". print_r($unmoveableCartonBarCodes, true));
	$weblog->info("Invalid Cartons = ". print_r($invalidCartonBarcodes, true));
	
	$orderIds = array_values($carton2OrderIdMap);
	$orderIds = array_unique(array_diff($orderIds, array('Invalid', 'Empty')));
	
	$weblog->info("Orderids found in all cartons - ".print_r($orderIds, true));
	$orderId2StatusMap = array();
	
	if($orderIds && count($orderIds) > 0) {
		if(count($orderIds) == 1) {
			$orderStatusSql = "Select orderid, status from $sql_tbl[orders] where orderid = ".$orderIds[0];
		} else {
			$orderStatusSql = "Select orderid, status from $sql_tbl[orders] where orderid in (".implode(",", $orderIds).")";		
		}
		$results = func_query($orderStatusSql);
		
		foreach ($results as $row) {
			$orderId2StatusMap[$row['orderid']] = $row['status'];
		}
	}
	$weblog->info("Order statuses - ".print_r($orderId2StatusMap, true));
	
	$response = array('Success', $moveableCartonBarCodes, $unmoveableCartonBarCodes, $invalidCartonBarcodes, $carton2OrderIdMap, $orderId2StatusMap);
	header('Content-type: text/x-json');
	print json_encode($response);
	exit;
}

if($action == 'markItemShipped') {
	$cartonBarcodesToMove = trim($HTTP_POST_VARS['cartonBarcodes']);
	$userid = $HTTP_POST_VARS['userid'];
	$cartonBarCodes = explode(",", $cartonBarcodesToMove);
	$statusMsgs = LocationApiClient::moveCartonsToExitBin($cartonBarCodes, $userid);
	
	$smarty->assign("successMsg", $statusMsgs[0]);
	$smarty->assign("errorMsg", $statusMsgs[1]);
}
	
$smarty->assign("action", $action);
$smarty->assign("main", "process_item_exit");
func_display("admin/home.tpl", $smarty);

?>