var today = new Date();
var dateOffset = (24*60*60*1000) * 7;
var lastweek = new Date();
lastweek.setTime(today.getTime() - dateOffset);
var from_date_str = (lastweek.getMonth()+1)+"/"+lastweek.getDate()+"/"+lastweek.getFullYear();
var to_date_str = (today.getMonth()+1)+"/"+today.getDate()+"/"+today.getFullYear();
var timerid;
Ext.onReady(function() {
	Ext.QuickTips.init();
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
	var downloadMask;
	
	function trackStatus() {
		Ext.Ajax.request({
			url : "/admin/getprogressinfo.php?type=status&progresskey="+progresskey,
			method : 'GET',
			mask: false,
		    success : function(response, action) {
		    	var jsonObj = Ext.util.JSON.decode(response.responseText);
		    	if(jsonObj.status == 'DONE'){
		    		clearInterval(timerid);
		    		downloadMask.hide();
		    		var html = "Click <a href = '"+httpLocation + "/admin/operations/download.php?type=item_assignment&filename=" + jsonObj.filename+"' target='_blank'> here </a> to download file";
		    		Ext.Msg.show({
		    		    title:        "Download Link",
		    		    msg:        html,
		    		    icon:        Ext.MessageBox.INFO
		    		});
		    	}
			}
		});
	}
	
	Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
	Ext.Ajax.on('requestexception', myMask.hide, myMask);
	
	var orderStatusArr = [["WP", "WIP"], ["OH", "On Hold"]];
	
	var itemStatusArr = [["A", "Assigned"], ["RP", "Repick"], ["QD", "QA Done"]];
	
	var itemStatusStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : itemStatusArr,
	    sortInfo : {field : 'name', direction : 'ASC'}
	});

	var itemStatus = new Ext.ux.form.SuperBoxSelect({
		xtype : 'superboxselect',
		fieldLabel : 'Item Status',
		emptyText : 'Select Item Status',
		name : 'itemStatus',
		anchor : '100%',
		store : itemStatusStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'id',
		forceSelection : true,
		value: 'UA,A'
	});
	
	var orderStatusStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : orderStatusArr,
	    sortInfo : {field : 'name', direction : 'ASC'}
	});

	var orderStatus = new Ext.ux.form.SuperBoxSelect({
		xtype : 'superboxselect',
		fieldLabel : 'Order Status',
		emptyText : 'Select Order Status',
		name : 'orderStatus',
		anchor : '100%',
		store : orderStatusStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'id',
		forceSelection : true,
		value: 'Q,WP'
	});
	
	var searchPanel = new Ext.form.FormPanel( {
		renderTo : 'contentPanel',
		title : 'Search Panel',
		id : 'formPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 2},
			defaults : {width : 450, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [{
				items : [orderStatus]
			}, {
				items : [itemStatus]
			}, {
				items : [{
		        	xtype : 'combo',
		        	emptyText : 'Select the payment method',
		        	store : new Ext.data.ArrayStore({
		        	    fields: ['id', 'name'],
		        	    data : [["On", "Online"], ["COD", "COD"]]
		        	}),
		        	mode: 'local',
		        	displayField : 'name',
		        	valueField : 'id',
		        	triggerAction: 'all',
		        	fieldLabel : 'Payment Method',
		        	name : 'payment_method'
				}]
			}, {
				items : [{
		        	xtype : 'combo',
		        	store : new Ext.data.JsonStore({
		        		fields : [ 'id', 'display_name'],
		        		data : warehouseData,
		        		sortInfo : {
		        			field : 'display_name',
		        			direction : 'ASC'
		        		}
		        	}),
		        	emptyText : 'Select Ops Location',
		        	displayField : 'display_name',
		        	valueField : 'id',
		        	fieldLabel : 'Ops Location',
		        	name : 'warehouse_id',
		        	mode: 'local',
		        	triggerAction: 'all'
				}]
			}, {
				items :[{
					xtype : 'textfield',
		        	id : 'customer_name',
		        	fieldLabel : 'Customer Name',
		        	width : 300
				}]
			},{
				items : [{
					xtype : 'numberfield',
		        	id : 'order_id',
		        	fieldLabel : 'Order Id'
				}]
			}, {
				items : [{
					xtype : 'numberfield',
		        	id : 'item_id',
		        	fieldLabel : 'Item ID'
				}]
			}, {
				items: []
			},{
				items: [{
					xtype: 'checkbox',
					name: 'jit_purchase',
					fieldLabel: 'JIT Purchase'				
				}]
			}, {
				items: [{
					xtype: 'checkbox',
					name: 'is_fragile',
					fieldLabel: 'Fragile Item'				
				}]
			}, {
				items : [{
					xtype : 'datefield',
	                name : 'from_date',
	                width : 150,
	                fieldLabel : "Queued Start Date",
	                value: from_date_str,
	                maxValue : new Date(),
	                listeners: {
	                    select: function (datefield) {
	                    	var enddatefield = searchPanel.getForm().findField("to_date");
	                    	var enddate = enddatefield.getValue();
	                    	var startdate = new Date(datefield.getValue());
	                    	if(!enddate || enddate==""){
	                    		enddate = new Date();
	                    	} else {
	                    		enddate = new Date(enddate);
	                    	}
	                    	
	                    	if( (enddate.getTime() - startdate.getTime())/(24*60*60*1000) > 31) {
	                    		enddate.setTime(startdate.getTime() + 31*24*60*60*1000);
	                    		if((enddate.getTime() - today.getTime())/(24*60*60*1000) > 0){
	                    			enddate = new Date();
	                    		}
	                    	} else if((startdate.getTime() - enddate.getTime())/(24*60*60*1000) >= 0) {
	                    		enddate.setTime(startdate.getTime() + 31*24*60*60*1000);
	                    		if((enddate.getTime() - today.getTime())/(24*60*60*1000) > 0){
	                    			enddate = new Date();
	                    		}
	                    	}
	                    	enddatefield.setValue(enddate);
	                  	}
	                }
				}]
			}, {
				items : [{
					xtype : 'datefield',
					name : 'to_date',
					width : 150,
					fieldLabel : "Queued End Date",
					value: to_date_str,
					maxValue : new Date(),
					listeners: {
	                    select: function (datefield) {
	                    	var startdatefield = searchPanel.getForm().findField("from_date");
	                    	var startdate = startdatefield.getValue();
	                    	if(!startdate || startdate==""){
	                    		startdate = new Date();
	                    	} else {
	                    		startdate = new Date(startdate);
	                    	}
	                    	var enddate = new Date(datefield.getValue());
	                    	// if end date is selected out of range..
	                    	if( (enddate.getTime() - startdate.getTime())/(24*60*60*1000) > 31 ){
	                    		startdate.setTime(enddate.getTime() - 31*24*60*60*1000);
		                    	startdatefield.setValue(startdate);
	                    	}
	                    	if( (enddate.getTime() - startdate.getTime())/(24*60*60*1000) < 0 ){
	                    		startdate.setTime(enddate.getTime() - 31*24*60*60*1000);
		                    	startdatefield.setValue(startdate);
	                    	}
	                    }
	                }
				}]
			}]
		}],
		buttons : [ {
			text : 'Search',
			handler : function() {
				reloadGrid();
			}
		},
			/*{
                        text : 'Assign Tracking',
                        handler : function() {
                                var params = getSearchPanelParams();
                                if(params == false) {
                                        return false;
                                }
                                params["action"] = "assign_tracking";
                                myMask.show();
                                Ext.Ajax.request({
                                        url : 'itemsajax.php',
                                        params : params,
                                    success : function(response, action) {
                                        reloadGrid();
                                        }
                                });
                        }
                }, */
                {
			text : 'Download Report',
			handler : function() {
				var params = getSearchPanelParams();
				if(params == false) {
					return false;
				}
				params["action"] = "download_report";
				params["progresskey"] = progresskey;
				downloadMask = new Ext.LoadMask(Ext.get('formPanel'), {msg: "Generating File"});
				downloadMask.show();
				Ext.Ajax.request({
					url : 'itemsajax.php',
					params : params,
				    success : function(response, action) {
				    	//downloadMask.show();
					}
				});
				timerid = setInterval(trackStatus, 3000);
			}
		}
		]
	});
	
	function getSearchPanelParams() {
		var form = searchPanel.getForm();
		
		if (form.isValid()) {
			var params = {};
			if(form.findField("from_date").getRawValue() == "" || 
					form.findField("to_date").getRawValue() == "" ) {
				Ext.MessageBox.alert('Error', 'Date range can not be empty');
				return false;
			}
			if(form.findField("warehouse_id").getValue() == ""){
				Ext.MessageBox.alert('Error', 'Please select the operations location');
				return false;
			}
			params["item_id"] = form.findField("item_id").getValue();
			params["order_id"] = form.findField("order_id").getValue();
			params["customer_name"] = form.findField("customer_name").getValue();
			params["from_date"] = form.findField("from_date").getRawValue();
			params["to_date"] = form.findField("to_date").getRawValue();
			params["warehouse_id"] = form.findField("warehouse_id").getValue();
			params["item_status"] = form.findField("itemStatus").getValue();
			params["order_status"] = form.findField("orderStatus").getValue();
			params["jit_purchase"] = form.findField("jit_purchase").getValue();
			params["is_fragile"] = form.findField("is_fragile").getValue();
			
			return params;
		} else {
			Ext.MessageBox.alert('Error', 'Error in date range');
			return false;
		}
	}
	
	function reloadGrid() {
		var params = getSearchPanelParams();
		
		if (params == false) {
			return false;
		}
		myMask.show();
		params["action"] = "search";
		
		searchResultsStore.baseParams = params;
		searchResultsStore.load();
	}
	
	var sm = new Ext.grid.CheckboxSelectionModel();
	
	var previewImageLinkRender = function(value, p, record) {
		var retLink = '<a href="' + httpLocation + '/admin/view_print_design.php?productId=' + record.data['productid'] + '&orderid=' 
				+ record.data["orderid"] + '" target="_blank">Preview Image</a>';
		
		if (record.data["customizable"] == '0') {
			retLink = '<a href="' + httpLocation + '/admin/imagePreview.php?style_id=' + record.data['product_style'] + '" target="_blank">Preview Image</a>';
		}
		return retLink;
	}
	
	var orderIdLinkRender = function(value, p, record) {
		var retLink = '<a href="' + httpLocation + '/admin/order.php?orderid=' + record.data["orderid"] + '" target="_blank">' + record.data["orderid"] + '</a>';
		return retLink;
	}
	
	var orderNameLinkRender = function(value, p, record) {
		var retLink = '<a href="' + httpLocation + '/admin/order.php?orderid=' + record.data["orderid"] + '" target="_blank">' + record.data["order_name"] + '</a>';
		if (record.data['customizable'] == '0') {
			retLink = retLink + "<span style='color:red'> (NP) <span>";
		}
		return retLink;
	}
	
	var itemStatusRenderer = function(value, p, record) {
		var status = record.data['item_status'];
		for (var i = 0; i < itemStatusArr.length; i++) {
			if (itemStatusArr[i][0] == status) {
				return itemStatusArr[i][1];
			}
		}
		return "NOT SPECIFIED";
	}
	
	var sizeOptionRender = function(value, p, record) {
		return (record.data["option_value"] != null && record.data["option_value"] != "") ? record.data["option_value"] : record.data["quantity_breakup"];
	}
	
	var quantityRender = function(value, p, record) {
		return (record.data["quantity"] != null && record.data["quantity"] != "" && record.data["quantity"] != "0") ? record.data["quantity"] : record.data["qty"];
	}
	
	var materialSourcingRender = function(value, p, record) {
		if (record.data["jit_purchase"] == "1") {
			return "JIT";
		} else {
			return "-";
		}
	}
	
	var cityNameRenderer = function(value, p, record) {
		if (record.data["local_delivery_location"] == "ML") {
			return '<span style="background-color: #218003;">'+record.data["city"]+'</span>';
		} 
		return record.data["city"];
	}
	
	var colModel = new Ext.grid.ColumnModel({
		defaults: {
	        width: 120,
	        sortable: true
	    },
	    columns: [
	        sm,
	        {id : 'order_name', header : "Order Name", sortable: true, dataIndex : 'order_name', renderer : orderNameLinkRender},
	        {id : 'payment_method', header : "Payment Method", sortable: true, dataIndex : 'payment_method'},
	        {id : 'order_id', header : "Order ID", sortable: true, dataIndex : 'orderid', renderer : orderIdLinkRender, sortable: true},
	        {id : 'item_id', header : "Item ID", sortable: true, dataIndex : 'itemid'},
	        {id : 'article_number', header : "Article Id", sortable: true, dataIndex : 'article_number'},
	        {id : 'status', header : "Status", sortable: true, dataIndex: 'item_status', renderer : itemStatusRenderer
	        	/*editor : {
	        		xtype : 'combo',
	        		store : new Ext.data.ArrayStore({
		        	    fields: ['id', 'name'],
		        	    data : orderStatusArr
		        	}),
		        	mode: 'local',
		        	displayField : 'name',
		        	valueField : 'id',
		        	triggerAction: 'all',
		        	listners : {
	        			'select' : function(comp, oldValue, newValue) {
	        				// TODO Here I have to write the code for the status change
	        				console.log("this is for test");
	        			}
	        		}*/
	        },
	        {id : 'gift_status', header : "Gift Status", sortable: true, dataIndex : 'gift_status'},
	        {id : 'is_fragile', header : "Is Fragile", sortable: true, dataIndex : 'is_fragile'},
	        {id : 'jit_purchase', header : "JIT Purchase", sortable: true, dataIndex : 'jit_purchase'},
	        {id : 'product_type', header : "Product Type", sortable: true, dataIndex : 'product_type_name'},
	        {id : 'item_name', header : "Item Name", sortable: true, dataIndex : 'stylename'},
	        {id : 'option_value', header : "Size Option", sortable: true, renderer : sizeOptionRender},
	        {id : 'sku_code', header : "SKU Code", sortable: true, dataIndex : 'sku_code'},
	        {id : 'locationbarcode', header : "Location BarCode", sortable: false, dataIndex : 'locationbarcode'},
	        //{id : 'jit_purchase', header : "Material Sourcing", sortable: true, dataIndex : 'jit_purchase', renderer : materialSourcingRender},
	        {id : 'quantity', header : "Quantity", sortable: true, renderer : quantityRender},
	        {id : 'courier', header : "Courier", sortable: true, dataIndex : 'courier_service'},
	        {id : 'tracking', header : "Tracking", sortable: true, dataIndex : 'tracking'},	        
	        //{id : 'location', header : "Location", sortable: true, dataIndex : 'location_name'},
	        //{id : 'assignee', header : "Assignee", sortable: true, dataIndex : 'assignee_name'},
	        {id : 'city', header : "City", sortable: true, dataIndex : 'city', renderer: cityNameRenderer},
	        {id : 'queued_date', header : "Queued Date", sortable: true, dataIndex : 'queueddate'},
	        //{id : 'source', header : "Source", sortable: true, dataIndex : 'source_name'},
	        //{id : 'assigned_date', header : "Assigned Date", sortable: true, dataIndex : 'assignment_time'},
	        {id : 'completed_date', header : "Completed Date", sortable: true, dataIndex : 'completion_time'},
	        {id : 'login', header : "Customer Login", sortable: true, dataIndex : 'login'},
	        {id : 'preview_image', header : "Preview Image", dataIndex: 'preview_image', renderer : previewImageLinkRender},
            {id : 'totalItems', header : "Total Items", dataIndex : 'totalItems'}
	    ]
	});
		
	var searchResultsStore = new Ext.data.Store({
		url: 'itemsajax.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['order_name', 'orderid', 'productid', 'itemid', 'item_status', 'product_type_name', 'stylename',
	  	  	          'quantity_breakup', 'qty', 'location_name', 'assignee_name', 'queueddate', 'gift_status', 'is_fragile',
	  	  	          'source_name', 'assignment_time', 'completion_time', 'price', 'payment_method', 'login', 'totalItems',
	  	  	          'customizable', 'article_number', 'product_style',  'option_value' ,'quantity', 'sku_code', 
	  	  	          'locationbarcode', 'jit_purchase', 'city', 'courier_service', 'tracking','local_delivery_location']
		}),
		sortInfo:{field : 'orderid', direction:'ASC'},
		remoteSort: true
	});
	
	var pagingBar = new Ext.PagingToolbar({
		pageSize : 30,
	    store: searchResultsStore,
	    displayInfo: true
	});
	
	var menuAction = function(itemStatus) {
    	var selected = searchResults.initialConfig.sm.getSelected();
		
    	Ext.Ajax.request({
			url : 'itemsajax.php',
			params : {"action" : "status_change", "item_id" : selected.data.itemid, "status" : itemStatus},
		    success : function(response, action) {
				reloadGrid();
			}
		});
	}
	
	var itemStatusMenu = new Ext.menu.Menu({
	    items: [
	    {
	        text : 'Ops Done',
	        tooltip : 'Ops Done',
	        iconCls : 'add',
	        handler : function() {
	        	menuAction("OD");
	        }
	    }, {
	        text : 'Done',
	        tooltip : 'Done',
	        iconCls : 'add',
	        handler : function() {
	        	menuAction("D");
	        }
        }, {
	        text : 'Sent Outside',
	        tooltip : 'Sent Outside',
	        iconCls : 'add',
	        handler : function() {
	        	menuAction("S");
	        }
        }, {
	        text : 'Failed',
	        tooltip : 'Failed',
	        iconCls : 'add',
	        handler : function() {
	        	menuAction("F");
	        }
        }, {
	        text : 'On Hold',
	        tooltip : 'On Hold',
	        iconCls : 'add',
	        handler : function() {
	        	menuAction("H");
	        }
        }]
	});
	
	searchResults = new Ext.grid.EditorGridPanel({
	    store  : searchResultsStore,
	    cm : colModel,
	    id : 'searchResults-panel',
	    title : 'Search Results',
	    frame : true,
		autoHeight : true,
		sm: sm,
		bbar : pagingBar,
		stripeRows : true,
		autoScroll : true,
		viewConfig : {
			enableRowBody : true
		},
		tbar:[/*{
			xtype: 'label',
			html: 'Location: '
		}, {
			xtype : 'combo',
        	emptyText : 'Select Location',
        	store : new Ext.data.JsonStore({
        	    fields: ['id', 'location_name'],
        	    data : locationsData
        	}),
        	mode: 'local',
        	displayField : 'location_name',
        	valueField : 'id',
        	triggerAction: 'all',
        	name : 'location',
        	id : 'location'
        }, '-', {
			xtype: 'label',
			html: 'Assignee: '
		}, {
			xtype : 'combo',
        	emptyText : 'Select Assignee',
        	store : new Ext.data.JsonStore({
        	    fields: ['id', 'name'],
        	    data : assigneeData
        	}),
        	mode: 'local',
        	displayField : 'name',
        	valueField : 'id',
        	triggerAction: 'all',
        	name : 'assignee',
        	id : 'assignee'
        }, '-', {
			text : '<b>Assign Selected</b>',
			iconCls : 'assign',
			handler : function() {
        		// First get the location and the assignee from the selection
	        	var location = Ext.getCmp("location").getValue();
	    		if (location == null || location == "") {
	    			Ext.MessageBox.alert('Error', 'Select location from the drop down.');
	    			return false;
	    		} 
        		
        		var assignee = Ext.getCmp("assignee").getValue();
        		if (assignee == null || assignee == "") {
        			Ext.MessageBox.alert('Error', 'Select assignee from the drop down');
        			return false;
        		} 
        		
        		var selections = searchResults.initialConfig.sm.selections.items;
        		
        		if (selections.length == 0) {
        			Ext.MessageBox.alert('Error', 'No items are selected to assign');
        			return false;
        		}
        		
        		var itemCheck = "";
        		for (var i = 0; i < selections.length; i++) {
        			itemCheck = itemCheck + "," + selections[i].data.itemid;
        		}
        		
        		var params = getSearchPanelParams();
        		
        		Ext.Ajax.request({
					url : 'itemsajax.php',
					params : {"item_check" : itemCheck.substring(1, itemCheck.length), "ia" : assignee, "il" : location, "action" : "assign"},
				    success : function(response, action) {
						Ext.getCmp("location").clearValue();
						Ext.getCmp("assignee").clearValue();
						// else reload the grid
						reloadGrid();
					}
				});
        	}
		}, '-', {
			text : '<b>Assign All Pages</b>',
			iconCls : 'x-btn-text',
			handler : function() {
	    		// First get the location and the assignee from the selection
	        	var location = Ext.getCmp("location").getValue();
	    		if (location == null || location == "") {
	    			Ext.MessageBox.alert('Error', 'Select location from the drop down.');
	    			return false;
	    		} 
	    		
	    		var assignee = Ext.getCmp("assignee").getValue();
	    		if (assignee == null || assignee == "") {
	    			Ext.MessageBox.alert('Error', 'Select assignee from the drop down');
	    			return false;
	    		} 
	    		
	    		// Show the alert to the user saying really wants to assign to all the items
	    		Ext.MessageBox.confirm('Confirm', 'You are going to assign to all the pages. Really want to do it?', function(choice) {
					if (choice == 'yes') {
						var params = searchResultsStore.baseParams;
			    		params["action"] = "assign_all";
			    		params["ia"] = assignee;
			    		params["li"] = location;
			    		
			    		Ext.Ajax.request({
							url : 'itemsajax.php',
							params : params,
						    success : function(response, action) {
								Ext.getCmp("location").clearValue();
								Ext.getCmp("assignee").clearValue();
								// else reload the grid
								reloadGrid();
							}
						});
					} else {
						return false;
					}
				});
			}
		}, '-',*/ {
			text : '<b>Print Shipping Label</b>',
			iconCls : 'x-btn-text',
			handler : function() {
        		var selections = searchResults.initialConfig.sm.selections.items;
        		
        		if (selections.length == 0) {
        			Ext.MessageBox.alert('Error', 'No items are selected to assign');
        			return false;
        		}
        		
        		var orderCheck = "";
        		for (var i = 0; i < selections.length; i++) {
        			orderCheck = orderCheck + "," + selections[i].data.orderid;
        		}

        		window.open('/shipping_label.php?orderid=' + orderCheck.substring(1, orderCheck.length));        		
        	}
		}],
		listeners : {
			'render' : function(comp) {
				searchPanel.getForm().findField("order_id").focus();
			},
			"rowcontextmenu" : function(grid, row, event) {
	    		grid.getSelectionModel().selectRow(row);
	    		
	    		var selected = grid.initialConfig.sm.getSelected();
	    		
	    	}
		}
	});
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
		     searchPanel, searchResults
		],
		listeners : {
			'render' : function(comp) {
				//reloadGrid();
			}
		}
	});
});
