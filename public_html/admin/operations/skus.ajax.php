<?php
require '../auth.php';
require $xcart_dir."/include/security.php";

global $weblog;

function generateSearchSql($request) {
	$conditions = "";
	
	$conditions .= (isset($request['article_type']) && $request['article_type'] != "") ? " AND s.article_type_id = " . $request['article_type'] : "";
	$conditions .= (isset($request['brand']) && $request['brand'] != "") ? " AND s.brand_id = " . $request['brand'] : "";
	$conditions .= (isset($request['article']) && $request['article'] != "") ? " AND s.vendor_article_no like '%" . $request['article'] . "%'" : "";
	$conditions .= (isset($request['from_date']) && $request['from_date'] != "") ? " AND s.created_on >= '" . $request['from_date'] . "'" : "";
	$conditions .= (isset($request['to_date']) && $request['to_date'] != "") ? " AND s.created_on <= '" . $request['to_date'] . "'" : "";
	
	$sql = "SELECT SQL_CALC_FOUND_ROWS s.id, s.name, s.code, s.enabled, s.inv_count, s.thrs_count, lastuser, av.attribute_value as brand_name, c.typename AS article_type_name, s.created_by,
				s.created_on, s.vendor_article_no, s.vendor_article_name, s.remarks, s.size, s.brand_id, s.article_type_id, s.last_modified_on
				FROM mk_skus s
					LEFT JOIN mk_attribute_type_values av ON (av.id = s.brand_id AND av.attribute_type = 'Brand')
					LEFT JOIN mk_catalogue_classification c ON (c.id = s.article_type_id) ";
	
	if ($conditions != "") {
		$sql = $sql . " WHERE " . substr($conditions, 4);
	}
	$sql = $sql . " ORDER BY id DESC";
	
	return $sql;
}

if ($action == 'search') {
	
	$start = (isset($start) && $start != "") ? $start : 0;
	$limit = (isset($limit) && $limit != "") ? $limit : 30;
		
	$sql = generateSearchSql($_REQUEST) . " LIMIT " . $start . ", " . $limit;
	
	$results = func_query($sql);
	
	$total = func_query("SELECT FOUND_ROWS() total");
	$arr = array("results" => $results, "count" => $total[0]["total"]);

	header('Content-type: text/x-json');
	print json_encode($arr);

} else if ($action == "download_report") {
	$sql = generateSearchSql($_REQUEST);
	
	$results = func_query($sql);

	$file_name_suffix = time();
	$skus_report = fopen($xcart_dir."/admin/sku_list_reports/sku_list_report_$file_name_suffix.csv","a");
	
	$summary_table = "Vendor Article No, Vendor Article Name, Size, Brand, Article Type, SkuCode, Remarks, Created On, Created By, Updated On, Updated By";
	fwrite($skus_report, $summary_table);
	fwrite($skus_report, "\r\n");
	$string = "";
	foreach($results AS $key => $array){
		$string .= $array[vendor_article_no] . ", " . $array[vendor_article_name] . ", " . $array[size] . ", " . $array[article_type_name] . ", " . 
					$array[brand_name] . ", " . $array[code] . ", " . $array[remarks]. "," . $array[created_on] . ", " . $array[created_by] . ", " .
					$array[lastuser] . ", " . $array[last_modified_on];
		$string .= "\r\n";
	}
	fwrite($skus_report, $string);
	fwrite($skus_report, "\r\n");

	#
	#Download
	#
	$file = "sku_list_report_$file_name_suffix.csv";
	$retarr = array("success" => true, "file_location" => $file);
	
	header('Content-type: text/x-json');
	print json_encode($retarr);
	
} else if ($action == "create") {
	if (isset($_REQUEST['id']) && $_REQUEST['id'] != "") {
		$sql = "update mk_skus set remarks = '$remarks', size = '$size', vendor_article_no = '$vendor_article_no', 
					vendor_article_name = '$vendor_article_name', last_modified_on = current_timestamp, lastuser = '$login' 
					WHERE id = '".$_REQUEST['id']."'";
		
		func_query($sql);
	} else {
		$skuCode = generateSkuCode($brand_id, $article_type_id);
	
		$sql = "INSERT INTO mk_skus (brand_id, article_type_id, size, vendor_article_no, vendor_article_name, code, remarks, created_by, lastuser, created_on, last_modified_on)
				VALUES ($brand_id, $article_type_id, '$size', '$vendor_article_no', '$vendor_article_name', '$skuCode', '$remarks', '$login', '$login', current_timestamp, current_timestamp)";
		func_query($sql);
	}
	
	header('Content-type: text/x-json');
	print json_encode(array("success" => true));
	
} else if ($action == "validate_sku") {
	// Need to check are there any skus already exists with 
	// vendor article no and article name and the size
	header('Content-type: text/x-json');
	$arr = array("success" => true);
	
	$sql = "SELECT vendor_article_no, vendor_article_name, remarks, code, size,
				av.attribute_value as brand_name, c.typename AS article_type_name
			FROM mk_skus s
				LEFT JOIN mk_attribute_type_values av ON (av.id = s.brand_id AND av.attribute_type = 'Brand')
				LEFT JOIN mk_catalogue_classification c ON (c.id = s.article_type_id)
			WHERE size = '" . $size . "'";
	
	if (isset($_REQUEST['id']) && $_REQUEST['id'] != "") {
		$sql = $sql . " AND s.id != " . $_REQUEST['id'];
	}
	
	$rows = func_query($sql);
	
	$ret = array();
	
	foreach ($rows as $row) {
		$tmp = ereg_replace("[^A-Za-z0-9]", "", $row['vendor_article_no']);
		$tmp1 = ereg_replace("[^A-Za-z0-9]", "", $vendor_article_no);

		if (!(stristr($tmp, $tmp1) === FALSE)) {
			$ret[] = $row;
		} else {
			$tmp = ereg_replace("[^A-Za-z0-9]", "", $row['vendor_article_name']);
			$tmp1 = ereg_replace("[^A-Za-z0-9]", "", $vendor_article_name);

			if (!(stristr($tmp, $tmp1) === FALSE)) {
				$ret[] = $row;
			} 
		}
	}
	if (isset($ret[0])) $arr["results"] = $ret;
	print json_encode($arr);
	exit();
}

?>