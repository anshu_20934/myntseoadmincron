<?php
require '../../auth.php';
require $xcart_dir . '/include/func/func.returns.php';
require_once $xcart_dir."/PHPExcel/PHPExcel.php";

function create_search_query($request) {
	global $weblog;
	$weblog->info("returnsajax.php:  Create query with params - ".$request['return_id'].
	", ".$request['order_id'].", ".$request['status'].", ".$request['login']. ", ". $request['date_type'].
	", ". $request['from_date']. ", ".$request['to_date'].", ".$request['item_id']);
	
	$sql = "select SQL_CALC_FOUND_ROWS * from xcart_returns ";
	
	$cond_params = array();
	if(!empty($request['return_id'])) {
		$cond_params[] = "returnid = ".$request['return_id'];
	}
	if(!empty($request['order_id'])) {
		$cond_params[] = "orderid = ".$request['order_id'];
	}
	if(!empty($request['item_id'])) {
		$cond_params[] = "itemid = ".$request['item_id'];
	}
	if(!empty($request['login'])) {
		$cond_params[] = "login = '".$request['login']."'";
	}
	if(!empty($request['tracking_no'])) {
		$cond_params[] = "tracking_no = '".$request['tracking_no']."'";
	}
	if(!empty($request['status'])) {
		$statuses = explode(",",$request['status']);
		$status_query = '';
		foreach($statuses as $status) {
			if($status_query != '') { $status_query .= ' OR '; }
			$status_query .= "status = '$status'";
		}
		$cond_params[] = "($status_query)";
	}
	
	if(!empty($request['date_type'])) {
		$datafield = $request['date_type'];
		if(!empty($request['from_date'])) {
			$return_from_date_break = explode("/",$request['from_date']);
	        $return_from_date = $return_from_date_break[2]."-".$return_from_date_break[0]."-".$return_from_date_break[1];
			$cond_params[] = "FROM_UNIXTIME(".$datafield.",'%Y-%m-%d') >= '". $return_from_date. "'"; 
		}
	
		if(!empty($request['to_date'])) {
			$return_to_date_break = explode("/",$request['to_date']);
	        $return_to_date = $return_to_date_break[2]."-".$return_to_date_break[0]."-".$return_to_date_break[1];
			$cond_params[] = "FROM_UNIXTIME(".$datafield.",'%Y-%m-%d') <= '". $return_to_date. "'"; 
		}	
	}
	
	if(count($cond_params) > 0) {
		$sql .= "WHERE ".implode(" AND ", $cond_params);
	}

	$weblog->info("returnsajax.php: sort - ".$request["sort"].", ".$request["dir"]);
	if(!empty($request["sort"])) {
		$sql .= " ORDER BY " . $request["sort"];
		$sql .= " " . ($request["dir"] == 'DESC' ? "DESC" : "ASC");
	}
	$weblog->info("returnsajax.php: Execute Query ". $sql); 
	
	return $sql;	
}

if ($_POST['action'] == 'search') {
	$search_sql = create_search_query($_POST);
	$start = $_POST['start'] == null ? 0 : $_POST['start'];
	$limit = $_POST['limit'] == null ? 30 : $_POST['limit'];
	$search_sql .=" LIMIT $start, $limit";
	
	$results = func_query($search_sql, TRUE);
	
	$return_requests = format_return_requests_fields($results);
	
	$total = func_query("SELECT FOUND_ROWS() total", TRUE);
	
	$retarr = array("results" => $return_requests, "count" => $total[0]["total"]);

	header('Content-type: text/x-json');
	print json_encode($retarr);
}

if ($_POST['action'] == 'downloadreport') {
	$search_sql = create_search_query($_POST);
	$results = func_query($search_sql, TRUE);
	$return_requests = format_return_requests_fields($results);
	
	$return_data = array();
	if($return_requests) {
		$xls_data = get_xls_data_from_requests($return_requests, 'report');
		$alphabets = range('A','Z');
		$file_name_suffix = date('d-m-y-H:i:s');
		$filename = 'returns-report-'.$file_name_suffix.".xls";
		$fullpath = "/admin/returns_report/".$filename;
		
		$weblog->info("Write to file - $fullpath");
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		
		$col_headers = array_keys($xls_data[0]);
		foreach($col_headers as $col_num=>$header) {
			$objPHPExcel->getActiveSheet()->SetCellValue($alphabets[$col_num]."1", $header);
		}
		$weblog->info("Headers Created");
		$row_num = 2;
		foreach ($xls_data as $return) {
			$col_num = 0;
			foreach($return as $key=>$value) {
				$objPHPExcel->getActiveSheet()->SetCellValue($alphabets[$col_num++].$row_num, "$value");
			}
			$row_num++;
		}
		$weblog->info("Rows Created");
	
		try {
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save($xcart_dir.$fullpath);
		}catch(Exception $ex) {
			$weblog->info("Error Occured = ". $ex->getMessage());
		}
		$weblog->info("Saved");
		$return_data['success'] = true;
		$return_data['file_location'] = $fullpath;
	} else {
		$return_data['success'] = false;
		$return_data['msg'] = "No data to generate return";
	}
	
	header('Content-type: text/x-json');
	print json_encode($return_data);
}

if($_POST['action'] == 'status_change') {
	$weblog->info("returnsajax.php: Status Change for Return Req - ". $_POST['returnid']. " and item id - ".$_POST['itemid']." to - ". $_POST['status']); 
	
	$returnid = $_POST['returnid'];
	$return_details = format_return_request_fields($returnid);
	
	$status = $_POST['status'];

	// If QA Passed then generate coupon for user.
	$return_data = array('status'=>$status);
	if($status == 'RQP') {
		$return_req = format_return_request_fields($_POST['returnid']);
		$refund_params = issue_refund_for_return($return_req,  $_POST['login']);
		$weblog->info("returnsajax.php: Refund Issued Successfully");
		$return_data['couponcode'] = $refund_params['coupon'];
		$return_data['refundamount'] = $refund_params['refund'];
		$args['AMOUNT'] = $return_data['refundamount'];
		$args['COUPON'] = $return_data['couponcode'];
	}
	
	$datefield = get_datefield_for_status($status);
	if($datefield) {
		$return_data[$datefield] = time(); 
	}
	
	func_array2update("order_details", array('item_status'=>$status), "itemid = ".$_POST['itemid']);
	func_array2update("returns", $return_data, "returnid = ".$_POST['returnid']);
	
	$args = array();
	if($status == 'RRC') {
		$args['RECEIVED_DATE'] = date('d-m-Y H:i:s', $return_data['receiveddate']);
	}
	
	if($status == 'RSD') {
		$args['REDELIVER_DATE'] = date('d-m-Y H:i:s', $return_data['redelivereddate']);
		$args['RESHIP_COURIER'] = $return_details['reship_courier'];
		$args['RESHIP_TRACKING]'] = $return_details['reship_tracking_no'];
	}
	
	add_returns_logs_status_change($_POST['returnid'], $status, $_POST['login'], $args);
	
	header('Content-type: text/x-json');
	print json_encode(array("success" => true));
}