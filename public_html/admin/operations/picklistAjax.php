<?php
require '../../auth.php';
//include_once("$xcart_dir/include/security.php");
require $xcart_dir . '/include/func/func.itemsearch.php';

function create_picklist_query($request) {
	global $weblog;
	$search_sql = "SELECT SQL_CALC_FOUND_ROWS Concat_ws(' - ', ps.name, po.VALUE) AS product_name, 
					m.sku_id                             AS sku_id, 
					CAST(sum(
					    CASE  LOCATE(po.value,od.quantity_breakup)
					       when 0 then 0
					       when 1 then (SUBSTRING_INDEX(substr(od.quantity_breakup FROM (LOCATE(po.value,od.quantity_breakup)+1 +length(po.value))), ',',1) )
					       else (CASE LOCATE(CONCAT(',',po.value),od.quantity_breakup)
							   when 0 then 0
							   else (SUBSTRING_INDEX(substr(od.quantity_breakup FROM (LOCATE(CONCAT(',',po.value),od.quantity_breakup)+2 +length(po.value))), ',',1) )
						       END)
					    END)
					as CHAR) as qty
				FROM   xcart_orders o 
				       INNER JOIN xcart_order_details od 
				         ON o.orderid = od.orderid 
				       INNER JOIN mk_product_style ps 
				         ON ps.id = od.product_style 
				       LEFT JOIN mk_product_options po 
				         ON po.style = od.product_style 
				       INNER JOIN mk_styles_options_skus_mapping m 
				         ON ( ps.id = m.style_id 
				              AND po.id = m.option_id ) 
				WHERE  o.status IN ( 'C', 'Q' )";

	$from_date = $request['from_date']; $to_date = $request['to_date'];
	if(!empty($from_date) && !empty($to_date))
	{
		$from_date_break = explode("/", $from_date);
		$from_date = $from_date_break[2] . "-" . $from_date_break[0] . "-" . $from_date_break[1];

		$to_date_break = explode("/",$to_date);
		$to_date = $to_date_break[2]."-".$to_date_break[0]."-".$to_date_break[1];

		$search_sql .= " AND (FROM_UNIXTIME(o.queueddate,'%Y-%m-%d') >= '$from_date' AND FROM_UNIXTIME(o.queueddate,'%Y-%m-%d') <= '$to_date')";
	}

	$search_sql .= " GROUP  BY ps.id, po.VALUE";
	
	/*$sort_map_arr = array("product_name" => "ps.name",
			"skuId" => "ps.id",
			"item_status" => "a.item_status",
			"qty" => "SUM(od.amount)"
	);
	$search_sql .= " ORDER BY " . ($request["sort"] != "" ? $sort_map_arr[$request["sort"]] : "SUM(od.amount)");
	$search_sql .= " " . ($request["dir"] == 'ASC' ? "ASC" : "DESC");*/

	return $search_sql;
}

if ($_POST['action'] == 'search') {
	$search_sql = create_picklist_query($_POST);
	$start = $_POST['start'] == null ? 0 : $_POST['start'];
	$limit = $_POST['limit'] == null ? 30 : $_POST['limit'];
	$search_sql .=" LIMIT $start, $limit";

	$sqllog->debug("File name##picklistAjax.php##SQL Info::>$search_sql");
	$itemResult = func_query($search_sql);
	$weblog->info("$search_sql");

	$total = func_query("SELECT FOUND_ROWS() total");
	$retarr = array("results" => $itemResult, "count" => $total[0]["total"]);

	header('Content-type: text/x-json');
	print json_encode($retarr);
	
} else if ($_POST['action'] == 'download_report') {
	$search_sql = create_picklist_query($_POST);
	$itemResultDone = func_query($search_sql);
	$weblog->info("$search_sql");

	#generate table
	$file_name_suffix = time();
	$sku_picklist_report = fopen($xcart_dir."/admin/sku_picklist_report/sku_picklist_report_$file_name_suffix.csv","a");
	$summary_table = "product_name, skuId, qty";
	fwrite($sku_picklist_report, $summary_table);
	fwrite($sku_picklist_report, "\r\n");
	$string = "";
	foreach($itemResultDone AS $key=>$array){
		$string .= $array[product_name] . ", " . $array[skuId] . ", " . $array[qty] ;
		$string .= "\r\n";
	}
	fwrite($sku_picklist_report, $string);
	fwrite($sku_picklist_report, "\r\n");

	#Download
	$file = "sku_picklist_report_$file_name_suffix.csv";
	$weblog->info($file." created---------");
	$retarr = array("success" => true, "file_location" => $file);

	print json_encode($retarr);
}

?>