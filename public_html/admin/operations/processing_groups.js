Ext.onReady(function() {
	Ext.QuickTips.init();
	
	var ADDING_MODE = "Now you are in adding mode of Group";
	var EDITING_MODE = "Editing mode of Group now you are..";
	
	
	var showGroupWindow = function() {
		// Here we need to render the dialog with the form that we are showing up there..
		Ext.MessageBox.alert('Error', 'Here the dialog will come with the add form');
		return false;
	}
	
	var fromStore = new Ext.data.JsonStore({
        data : productTypesData,
        fields: ['id','name'],
        sortInfo: {field: 'name', direction: 'ASC'}
    });
	
	var toStore = new Ext.data.JsonStore({
        data : [],
        fields: ['id','name'],
        sortInfo: {field: 'name', direction: 'ASC'}
    });
	
	var processingGroupFormPanel = new Ext.FormPanel({
      	xtype: 'form', // since we are not using the default 'panel' xtype, we must specify it
		waitMsgTarget: true,
	 	bodyStyle:'padding:0px 15px 15px 15px',
		labelPad: 20,
		id : 'processingGroupFormPanel',
		autoWidth : true,
		autoScroll : true,
		frame : true,
		collapsible : true,
		title : "Add/Edit Processing Group",
		labelWidth : 150,
		layoutConfig: {
			labelSeparator: ':'
		},
		defaults : {
			selectOnFocus: true,
			msgTarget: 'side'
		},
		items: [{
			xtype : 'panel',
			id : 'group_title',
			title : 'Add the Group from here'
		}, {
			xtype : 'hidden',
			name : 'group_id',
			value : ""
		}, {
			xtype : 'textfield',
			name : 'group_name',
			fieldLabel : 'Group Name',
			width : 250,
			allowBlank : false
	    }, {
	    	xtype : 'textarea',
	    	name : 'description',
	    	fieldLabel : 'Description',
	    	width : 250,
	    	allowBlank : false
	    }, {
			xtype : 'itemselector',
			name : 'product_types',
			id : 'product_types',
			imagePath : httpLocation + "/admin/extjs/resources/images/",
			fieldLabel: 'Product Types',
			allowBlank : false,
			width : 700,
			multiselects: [{
                width: 300,
                height: 300,
                store: fromStore,
                displayField : 'name',
                valueField : 'id'
            },{
                width: 300,
                height: 300,
                store: toStore,
                displayField : 'name',
                valueField : 'id',
                tbar:[{
                    text: 'clear',
                    handler:function(){
                    	processingGroupFormPanel.getForm().findField('product_types').reset();
	                }
                }]
            }]
	
	    }],
		buttons: [{
			text: 'Save',
			onClick : function() {
				if (processingGroupFormPanel.getForm().isValid()) {
					processingGroupFormPanel.getForm().submit({
						url : 'processing_groups.ajax.php',
						params : {"action" : "create"},
	                	waitMsg : "Saving data..",
	                    success : function() {
	                    	processingGroupFormPanel.getForm().reset();
	                    	processingGroupsStore.load();

	                    	Ext.getCmp("group_title").setTitle(ADDING_MODE);
	                    },
	    	        	failure : function(form, action) {
		                    processingGroupFormPanel.getForm().reset();

	    	        		Ext.MessageBox.alert('Error', 'Your request is not submitted please submit it again...');
							return false;
	    	        	}
					});
				}
			}
		}, {
			text: 'Cancel',
			handler : function() {
                processingGroupFormPanel.getForm().reset();

				Ext.getCmp("group_title").setTitle(ADDING_MODE);
			}
		}]
	});

	var processingGroupsStore = new Ext.data.Store({
		url: 'processing_groups.ajax.php',
		baseParams : {"action" : "load_data"},
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	fields : ["id", 'name', "description", "product_types", "product_type_ids"]
		}),
		sortInfo:{field : 'name', direction:'ASC'}
	});

	var colModel = new Ext.grid.ColumnModel({
		defaults: {width: 120, sortable: true},
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {header : "Group Name", sortable: true, dataIndex : 'name'},
	        {header : "Desciption", dataIndex : 'description'},
	        {header : "Product Types", dataIndex : 'product_types', width : 300},
	    ]
	});
	
	var processingGroups = new Ext.grid.GridPanel({
	    store  : processingGroupsStore,
	    cm : colModel,
	    id : 'processingGroups-panel',
	    title : 'Processing Groups',
	    frame : true,
		loadMask : true,
		autoHeight : true,
		stripeRows : true,
		autoScroll : true,
		sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
		viewConfig : {
			enableRowBody : true
		},
	    tbar: [{
			text : 'Add Groups',
			tooltip : 'Add Groups Here',
	   		iconCls : 'add',
			handler : function() {
				Ext.getCmp("group_title").setTitle(ADDING_MODE);
			}
		}, '-', {
			text : 'Edit Group',
	   		iconCls : 'Edit',
			handler : function() {
				var selected = processingGroups.initialConfig.sm.getSelected();
				
				if (selected == undefined) {
					Ext.MessageBox.alert('Error', 'Here the dialog will come with the add form');
					return false;
				} else {
					// Need to set the values
					processingGroupFormPanel.getForm().reset();
					
					processingGroupFormPanel.getForm().findField("group_name").setValue(selected.data["name"]);
					processingGroupFormPanel.getForm().findField("description").setValue(selected.data["description"]);
					
					// Need to set the product types
					var productTypeIds = selected.data["product_type_ids"].split(",");
					for (var i = 0; i < productTypeIds.length; i++) {
						fromStore.each(function(record) {
                            if (record.data.id == productTypeIds[i]) {
                                toStore.add([record]);
                                fromStore.remove(record);
                            }
	                    });
					}
					processingGroupFormPanel.getForm().findField("group_id").setValue(selected.data["id"]);
					Ext.getCmp("group_title").setTitle(EDITING_MODE);
				}
			}
		}, '-', {
			text : 'Refresh',
	   		iconCls : 'refresh',
			handler : function() {
				processingGroupsStore.load();
				Ext.getCmp("group_title").setTitle(ADDING_MODE);
			}
		}, '-', {
			text : 'Delete',
			iconCls : 'delete',
			handler : function() {
				var selected = processingGroups.initialConfig.sm.getSelected();
				
				if (selected == undefined) {
					Ext.MessageBox.alert('Error', 'Select atleast one group to delete!!');
					return false;
				} else {
					Ext.MessageBox.confirm('Confirm', 'Are you sure want to delete this group?', function(choice) {
						if (choice == 'yes') {
							Ext.Ajax.request({
								url : 'processing_groups.ajax.php',
								params : {"action" : "delete", "group_id" : selected.data["id"]},
							    success : function(response, action) {
							    	processingGroupsStore.load();
									Ext.getCmp("group_title").setTitle(ADDING_MODE);
								}, failure : function(form, action) {
									Ext.MessageBox.alert('Error', 'There is some problem in deleting the record. Please try again..');
									return false;
								}
							});
						}
					});
				}
				Ext.getCmp("group_title").setTitle(ADDING_MODE);
			}
		}]
	});
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
	         processingGroupFormPanel,
	         processingGroups
		],
		listeners : {
			"render" : function(comp) {
				processingGroupsStore.load();
				Ext.getCmp("group_title").setTitle(ADDING_MODE);
			}
		}
	});
});