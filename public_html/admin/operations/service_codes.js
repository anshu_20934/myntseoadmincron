Ext.onReady(function() {
	Ext.QuickTips.init();
	
	var showAddServiceCodeWindow = function() {
		addServiceCodeWindow = new Ext.Window({
			title: "Add Service Code",
			modal: true,
			y: 100,		
			closable: false,
			width: 600,
			autoScroll : true,
			autoHeight : true,
			listeners : {
				"show" : function(comp) {
					addServiceCodeFormPanel.getForm().reset();
				}
			}
		});
		
		addServiceCodeFormPanel = new Ext.FormPanel({
			waitMsgTarget: true,
		 	bodyStyle:'padding:0px 15px 15px 15px',
			labelPad: 20,
			id : 'addServiceCodeFormPanel',
			autoWidth : true,
		    frame : true,
			autoScroll : true,
			labelWidth : 150,
			layoutConfig: {
				labelSeparator: ':'
			},
			defaults : {
				selectOnFocus: true,
				msgTarget: 'side',
				width : 200
			},
			items: [{
				xtype : 'textfield',
				name : 'courier_service',
				fieldLabel : 'Courier Service'
			}, {
				xtype : 'textfield',
				name : 'code',
				fieldLabel : 'Code'
			}, {
				xtype : 'textfield',
				name : 'website',
				fieldLabel : 'Website'
			}, {
				xtype : 'checkbox',
				name : 'enable_status',
				fieldLabel : "Enable?",
				inputValue : 1
			}],
			buttons: [{
				text: 'Add',
				handler : function() {
					if (addServiceCodeFormPanel.getForm().isValid()) {
						addServiceCodeFormPanel.getForm().submit({
							url : 'service_codes.ajax.php',
							params : {"action" : "add_code"},
		                	waitMsg : "Adding new service code",
		                    success : function() {
								addServiceCodeWindow.hide();
								courierServiceStore.load();
							},
		    	        	failure : function(form, action) {
								addServiceCodeWindow.hide();
							}
						});
					}
				}
			}, {
				text: 'Cancel',
				handler : function() {
					addServiceCodeWindow.hide();
				}
			}]
		});
		
		addServiceCodeWindow.add(addServiceCodeFormPanel);	
		addServiceCodeFormPanel.getForm().reset();
		addServiceCodeWindow.show();
	}
	

	var statusRenderer = function(value, p, record) {
		if (record.data["enable_status"] == "1") {
			return "Yes"
		} else {
			return "No";
		}
	}
	
	
	var colModel = new Ext.grid.ColumnModel({
		defaults: {
	        width: 120,
	        sortable: true
	    },
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {id : 'courier_service', header : "Courier Service",  dataIndex : 'courier_service'},
	        {id : 'code', header : "Code", dataIndex : 'code'},
	        {id : 'website', header : "Website", dataIndex : 'website', width : 250},
	        {id : 'enable_status', header : "Enable Status", dataIndex : 'enable_status', renderer : statusRenderer}
	    ]
	});courierServiceStore
		
	var courierServiceStore = new Ext.data.Store({
		url: 'service_codes.ajax.php',
		baseParams : {"action" : "load_data"},
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	fields : ['courier_service', 'code', 'website', 'enable_status']
		}),
		sortInfo:{field : 'code', direction:'ASC'}
	});
	
	var courierServiceGrid = new Ext.grid.GridPanel({
	    store  : courierServiceStore,
	    cm : colModel,
	    id : 'courierServiceGrid-panel',
	    title : 'Courier Service Providers',
	    frame : true,
		loadMask : true,
		autoHeight : true,
		sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
		stripeRows : true,
		autoScroll : true,
		viewConfig : {
			enableRowBody : true
		},
		tbar:[{
			text : '<b>Add New Courier Serive</b>',
			iconCls : 'add',
			onClick : function() {
				// Show the dialog here
				if (typeof(addServiceCodetWindow) == "undefined") {
					showAddServiceCodeWindow();
				} else {
					addServiceCodeFormPanel.getForm().reset();
					addServiceCodetWindow.show();	
				}
        	}
		}, '-', {
			text : '<b>Refresh</b>',
			iconCls : 'refresh',
			onClick : function() {
				// refresh the grid
				courierServiceStore.load();
        	}
		}]
	});
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
	         courierServiceGrid
		],
		listeners : {
			'render' : function(comp) {
				courierServiceStore.load();
			}
		}
	});
});
