Ext.onReady(function() {
	Ext.QuickTips.init();
	
	var orderStatusArr = [["Q", "Queued"], ["PV", "PV"], ["WP", "WIP"], ["B", "Back Ordered"],
	 		    	["D", "Declined"], ["F", "Failed"], ["C", "Complete"],
		            ["OH", "On Hold"]];
	
	var itemStatusArr = [["UA", "Unassigned"], ["A", "Assigned"], ["I", "In Progress"],
      	    ["OD", "Ops Done"], ["QD", "QA Done"], ["D", "Done"],
            ["F", "Failed"], ["H", "On Hold"], ["S", "Sent Outside"]];
	
	var itemStatusStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : itemStatusArr,
	    sortInfo : {field : 'name', direction : 'ASC'}
	});

	var itemStatus = new Ext.ux.form.SuperBoxSelect({
		xtype : 'superboxselect',
		fieldLabel : 'Item Status',
		emptyText : 'Select Item Status',
		name : 'itemStatus',
		anchor : '100%',
		store : itemStatusStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'id',
		forceSelection : true,
		value: 'UA'
	});
	
	var orderStatusStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : orderStatusArr,
	    sortInfo : {field : 'name', direction : 'ASC'}
	});

	var orderStatus = new Ext.ux.form.SuperBoxSelect({
		xtype : 'superboxselect',
		fieldLabel : 'Order Status',
		emptyText : 'Select Order Status',
		name : 'orderStatus',
		anchor : '100%',
		store : orderStatusStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'id',
		forceSelection : true,
		value: 'Q,PV'
	});
	
	var searchPanel = new Ext.form.FormPanel( {
		renderTo : 'contentPanel',
		title : 'Search Panel',
		id : 'formPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 2},
			defaults : {width : 450, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [{
				items : [itemStatus]
			}, {
				items : [orderStatus]
			}, {
				items : [{
					xtype : 'datefield',
	                name : 'order_from_date',
	                width : 150,
	                fieldLabel : "Order Placed Start Date"
				}]
			}, {
				items : [{
					xtype : 'datefield',
					name : 'order_to_date',
					width : 150,
					fieldLabel : "Order Placed End Date"
				}]
			}]
		}],
		buttons : [ {
			text : 'Search',
			handler : function() {
				reloadGrid();
			}
		}, {
			text : 'Download Report',
			handler : function() {
				var params = getSearchPanelParams();
				params["action"] = "download_order_items_report";
				Ext.Ajax.request({
					url : 'itemsajax.php',
					params : params,
				    success : function(response, action) {
						var jsonObj = Ext.util.JSON.decode(response.responseText);
						window.open(httpLocation + "/admin/operations/download.php?type=item_assignment&filename=" + jsonObj.file_location);
					}
				});
			}
		}]
	});
	
	function getSearchPanelParams() {
		var form = searchPanel.getForm();
		
		if (form.isValid()) {
			var params = {};
			params["item_status"] = form.findField("itemStatus").getValue();
			params["order_status"] = form.findField("orderStatus").getValue();
			params["order_from_date"] = form.findField("order_from_date").getRawValue();
			params["order_to_date"] = form.findField("order_to_date").getRawValue();
			
			return params;
		} else {
			return false;
		}
	}
	
	function reloadGrid() {
		
		var params = getSearchPanelParams();
		
		if (params == false) {
			Ext.MessageBox.alert('Error', 'Errors in the search form');
			return false;
		}
		params["action"] = "order_items_search";
		
		searchResultsStore.baseParams = params;
		searchResultsStore.load();
	}
	
	var sm = new Ext.grid.CheckboxSelectionModel();
	
	var previewImageLinkRender = function(value, p, record) {
		var retLink = '<a href="' + httpLocation + '/admin/view_print_design.php?productId=' + record.data['productid'] + '&orderid=' 
				+ record.data["orderid"] + '" target="_blank">Preview Image</a>';
		
		if (record.data["customizable"] == '0') {
			retLink = '<a href="' + httpLocation + '/admin/imagePreview.php?style_id=' + record.data['product_style'] + '" target="_blank">Preview Image</a>';
		}
		return retLink;
	}
	
	var orderIdLinkRender = function(value, p, record) {
		var retLink = '<a href="' + httpLocation + '/admin/order.php?orderid=' + record.data["orderid"] + '" target="_blank">' + record.data["orderid"] + '</a>';
		return retLink;
	}
	
	var orderNameLinkRender = function(value, p, record) {
		var retLink = '<a href="' + httpLocation + '/admin/order.php?orderid=' + record.data["orderid"] + '" target="_blank">' + record.data["order_name"] + '</a>';
		if (record.data['customizable'] == '0') {
			retLink = retLink + "<span style='color:red'> (NP) <span>";
		}
		return retLink;
	}
	
	var itemStatusRenderer = function(value, p, record) {
		var status = record.data['item_status'];
		for (var i = 0; i < itemStatusArr.length; i++) {
			if (itemStatusArr[i][0] == status) {
				return itemStatusArr[i][1];
			}
		}
		return "NOT SPECIFIED";
	}
	
	var colModel = new Ext.grid.ColumnModel({
		defaults: {
	        width: 120,
	        sortable: true
	    },
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {id : 'order_id', header : "Order ID", sortable: true, dataIndex : 'orderid', renderer : orderIdLinkRender, sortable: true},
	        {id : 'ordered_date', header : "Ordered Date", sortable: true, dataIndex : 'ordered_date'},
	        {id : 'order_status', header : "Order Status", sortable: true, dataIndex : 'order_status'},
	        {id : 'item_id', header : "Item ID", sortable: true, dataIndex : 'itemid'},
	        {id : 'article_number', header : "Article Id", sortable: true, dataIndex : 'article_number'},
	        {id : 'status', header : "Status", sortable: true, dataIndex: 'item_status', renderer : itemStatusRenderer},
	        {id : 'product_type', header : "Product Type", sortable: true, dataIndex : 'product_type_name'},
	        {id : 'item_name', header : "Item Name", sortable: true, dataIndex : 'stylename'},
	        {id : 'option_value', header : "Option", sortable: true, dataIndex : 'option_value'},
	        {id : 'quantity', header : "SKU Quantity", sortable: true, dataIndex : 'quantity'},
		{id : 'quantity_breakup', header : "Quantity Breakup", sortable: true, dataIndex : 'quantity_breakup'},
	        {id : 'qty', header : "QTY", sortable: true, dataIndex : 'qty'},
	        {id : 'queued_date', header : "Queued Date", sortable: true, dataIndex : 'queueddate'}
	    ]
	});
		
	var searchResultsStore = new Ext.data.Store({
		url: 'itemsajax.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['order_name', 'orderid', 'productid', 'itemid', 'order_status', 'item_status', 'product_type_name', 'stylename',
	  	  	          'quantity_breakup', 'qty', 'location_name', 'assignee_name', 'queueddate', 'gift_status', 'ordered_date',
	  	  	          'source_name', 'assignment_time', 'completion_time', 'price', 'payment_method', 'login', 'customizable', 
	  	  	          'article_number', 'product_style', 'option_value' ,'quantity']
		}),
		sortInfo:{field : 'orderid', direction:'ASC'},
		remoteSort: true
	});

	var pagingBar = new Ext.PagingToolbar({
		pageSize : 30,
	    store: searchResultsStore,
	    displayInfo: true
	});
	
	searchResults = new Ext.grid.EditorGridPanel({
	    store  : searchResultsStore,
	    cm : colModel,
	    id : 'searchResults-panel',
	    title : 'Search Results',
	    frame : true,
		loadMask : true,
		autoHeight : true,
		sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
		bbar : pagingBar,
		stripeRows : true,
		autoScroll : true,
		viewConfig : {
			enableRowBody : true
		},
		listeners : {
			'render' : function(comp) {
				// TODO get the focus on the orderid
				reloadGrid();
			}
		}
	});
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
		     searchPanel, searchResults
		],
		listeners : {
			'render' : function(comp) {
				reloadGrid();
			}
		}

	});
});
