Ext.onReady(function() {
	Ext.QuickTips.init();
	
	var searchPanel = new Ext.form.FormPanel( {
		renderTo : 'contentPanel',
		title : 'Search Panel',
		id : 'formPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 2},
			defaults : {width : 450, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [
			{
				items : [{
					xtype : 'datefield',
	                name : 'from_date',
	                width : 150,
	                fieldLabel : "From Date"
				}]
			}, {
				items : [{
					xtype : 'datefield',
					name : 'to_date',
					width : 150,
					fieldLabel : "To Date"
				}]
			}]
		}],
		buttons : [ {
			text : 'Search',
			handler : function() {
				reloadGrid();
			}
		}, {
			text : 'Download Report',
			handler : function() {
				var params = getSearchPanelParams();
				params["action"] = "download_report";
				Ext.Ajax.request({
					url : 'picklistAjax.php',
					params : params,
				    success : function(response, action) {
						var jsonObj = Ext.util.JSON.decode(response.responseText);
						window.open(httpLocation + "/admin/operations/download.php?type=sku_picklist&filename=" + jsonObj.file_location);
					}
				});
			}
		}]
	});
	
	function getSearchPanelParams() {
		var form = searchPanel.getForm();
		
		if (form.isValid()) {
			var params = {};
			params["from_date"] = form.findField("from_date").getRawValue();
			params["to_date"] = form.findField("to_date").getRawValue();			
			return params;
		} else {
			return false;
		}
	}
	
	function reloadGrid() {
		
		var params = getSearchPanelParams();
		
		if (params == false) {
			Ext.MessageBox.alert('Error', 'Errors in the search form');
			return false;
		}
		params["action"] = "search";
		
		searchResultsStore.baseParams = params;
		searchResultsStore.load();
	}
	
	var searchResultsStore = new Ext.data.Store({
		url: 'picklistAjax.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['product_name', 'skuId', 'qty']
		})/*,
		sortInfo:{field : 'qty', direction:'DESC'},
		remoteSort: true*/
	});
	
	var pagingBar = new Ext.PagingToolbar({
		pageSize : 30,
	    store: searchResultsStore,
	    displayInfo: true
	});
	
	var colModel = new Ext.grid.ColumnModel({
		defaults: {
	        width: 250,
	        sortable: false
	    },
	    columns: [
		new Ext.grid.RowNumberer(),
	        {id : 'product_name', header : "Product Name", /*sortable: true,*/ dataIndex : 'product_name', width : 600},
	        {id : 'skuId', header : "SKU ID", /*sortable: true,*/ dataIndex : 'skuId'},
	        {id : 'qty', header : "Total Qty", /*sortable: true,*/ dataIndex : 'qty'}
	    ]
	});
	
	var searchResults = new Ext.grid.EditorGridPanel({
	    store  : searchResultsStore,
	    cm : colModel,
	    sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
	    id : 'searchResults-panel',
	    title : 'Search Results',
	    frame : true,
		loadMask : true,
		autoHeight : true,
		width : 1000,
		bbar : pagingBar,
		stripeRows : true,
		autoScroll : true,
		viewConfig : {
			enableRowBody : true
		}
	});
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
		     searchPanel, searchResults
		]
	});
});
