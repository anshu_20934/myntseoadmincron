<?php
/**
 * Created by Arun Kumar K.
 * User: Myntraadmin
 * Date: 13 May, 2011
 * Time: 12:47:23 PM
 * To change this template use File | Settings | File Templates.
 */
require "../auth.php";
require $xcart_dir."/include/security.php";
include_once($xcart_dir."/include/func/func.utilities.php");
include_once($xcart_dir."/modules/sem/Sem.php");

$sem = new Sem();

switch($_POST['mode']){
    case 'add':             $query_data = array("url"=>$_POST['url'],"landing_url"=>$_POST['landing_url'],"created_date"=>time());
                            $routingInfo = $sem->getRoutingInfo(array("url"=>$_POST['url']));

                            //url is unique hence need to check for existence
                            if(empty($routingInfo)){
                                $flag=$sem->addRoutingInfo($query_data);
                                if($flag){$smarty->assign("message","<strong style='color:green'>Data is added successfully.</strong>");}
                            } else {
                                $smarty->assign("message","<strong style='color:red'>The url trying to add is already exist.</strong>");
                            }
                            break;

    case 'update':          $Id = $_POST['selecteditem'];
                            $query_data = array("url"=>$_POST["routing"]["url"][$Id],"landing_url"=>$_POST["routing"]["landing_url"][$Id],"is_active"=>$_POST["routing"]["status"][$Id]);
                            $condition = "id = '{$Id}'";
                            $sem->updateRoutingInfo($query_data,$condition);
                            $smarty->assign("message","<strong style='color:green'>Data is updated successfully.</strong>");
                            break;

    case 'delete':          $Id = $_POST['selecteditem'];
                            $sem->deleteRoutingInfo($Id);
                            break;

    case 'updateall':       if(is_array($_POST['routing']['selector'])){
                                foreach($_POST['routing']['selector'] as $Id=>$val){
                                    $query_data = array("url"=>$_POST["routing"]["url"][$Id],"landing_url"=>$_POST["routing"]["landing_url"][$Id],"is_active"=>$_POST["routing"]["status"][$Id]);
                                    $condition = "id = '{$Id}'";
                                    $sem->updateRoutingInfo($query_data,$condition);
                                }
                                $smarty->assign("message","<strong style='color:green'>All selected data is updated successfully.</strong>");
                            } else {
                                $smarty->assign("message","<strong style='color:red'>No data is selected to update.</strong>");
                            }
                            break;

}

$orderby = array('created_date'=>'desc');
$routingInfo = $sem->getRoutingInfo($filters=null,$orderby);

$smarty->assign("main","sem_routing");
$smarty->assign("data",$routingInfo);
func_display("admin/home.tpl",$smarty);