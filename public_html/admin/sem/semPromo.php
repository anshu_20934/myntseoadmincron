<?php 
require "../auth.php";
require $xcart_dir."/include/security.php";
include_once($xcart_dir."/include/func/func.utilities.php");
include_once($xcart_dir."/modules/sem/Sem.php");

$sem = new Sem();

switch($_POST['mode']){
    case 'addpromo':        $query_data = array("identifier"=>$_POST['identifier'],"promo_content"=>$_POST['promo_content'],"promo_url"=>$_POST['promo_url'],"promo_created_date"=>time());
                            $promoInfo = $sem->getPromo(array("identifier"=>$_POST['identifier']));

                            //promo identifier is unique hence need to check for existence
                            if(empty($promoInfo)){
                                $flag=$sem->addPromo($query_data);
                                if($flag){$smarty->assign("promomessage","<strong style='color:green'>Promo is added successfully.</strong>");}
                            } else {
                                $smarty->assign("promomessage","<strong style='color:red'>The Promo trying to add is already exist.</strong>");
                            }
                            break;

    case 'updatepromo':     $promoId = $_POST['selectedpromo'];
                            $query_data = array("identifier"=>$_POST["promo"]["identifier"][$promoId],"promo_content"=>$_POST["promo"]["promo_content"][$promoId],"promo_url"=>$_POST["promo"]["promo_url"][$promoId]);
                            $condition = "id = '{$promoId}'";
                            $sem->updatePromo($query_data,$condition);
                            $smarty->assign("promomessage","<strong style='color:green'>Promo is updated successfully.</strong>");
                            break;

    case 'deletepromo':     $promoId = $_POST['selectedpromo'];
                            $sem->deletePromo($promoId);
                            break;

    case 'updateallpromo':  if(is_array($_POST['promo']['selector'])){
                                foreach($_POST['promo']['selector'] as $promoId=>$val){
                                    $query_data = array("identifier"=>$_POST["promo"]["identifier"][$promoId],"promo_content"=>$_POST["promo"]["promo_content"][$promoId],"promo_url"=>$_POST["promo"]["promo_url"][$promoId]);
                                    $condition = "id = '{$promoId}'";
                                    $sem->updatePromo($query_data,$condition);
                                }
                                $smarty->assign("promomessage","<strong style='color:green'>All selected promos are updated successfully.</strong>");
                            } else {
                                $smarty->assign("promomessage","<strong style='color:red'>No promo is selected to update.</strong>");
                            }
                            break;

}

$orderby = array('promo_created_date'=>'desc');
$promos = $sem->getPromo($filters=null,$orderby);
$smarty->assign("main","sem_promo");
$smarty->assign("promos",$promos);
func_display("admin/home.tpl",$smarty);
?>