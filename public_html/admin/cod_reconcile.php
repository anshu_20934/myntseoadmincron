<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.order.php";
require_once 'codAdminHelper.php';

$action  = $HTTP_POST_VARS['action'];
$uploadStatus = false;

if($_POST['mode']== 'search')  {  // this condition is to return the post data in case of search

	$codOrderData = search_cod_orders($_POST['shippingStartDate'], $_POST['shippingEndDate'],
		$_POST['paymentStartDate'],$_POST['paymentEndDate'],
		$_POST['orderStartDate'],$_POST['orderEndDate'],
		$_POST['orderStartValue'],$_POST['orderEndValue'],
		$_POST['payStatus'], $_POST['orderId'], $_POST['customerName'], $_POST['awbno']);
		
	if($_POST['codSearchBtn'] == "DownloadAsCsv") {
		printAsCsv($codOrderData, "cod_search_data");
	} 
	
} else if($_POST['mode']== 'upload') { 
	// This condtion is to return date if someone upload some csv
		
	if(empty($action)) {
		$action ='upload';
	}
	
	if ($REQUEST_METHOD == "POST" ) {
		if(!isset($_FILES) && isset($HTTP_POST_FILES)) {
			$_FILES = $HTTP_POST_FILES;
		}
		
		if(!empty($_FILES['orderdetail']['name'])) {
			$uploaddir1 = '../bulkorderupdates/';
			$extension = explode(".",basename($_FILES['orderdetail']['name']));
			
			$uploadfile1 = $uploaddir1.'cod_'.date('d-m-Y-h-i-s').".".$extension[1];
			
			if (move_uploaded_file($_FILES['orderdetail']['tmp_name'], $uploadfile1)) {
				$uploadStatus = true;
			}
		}
		global $weblog;
		if($uploadStatus) {
			// Open the text file
			$fd = fopen ($uploadfile1, "r");
			$orderUpdateData =  array();
			
			$paidOrders = array();
			$riskOrders = array();
			while (!feof ($fd)) {
				$buffer = fgetcsv($fd, 4096);
				$orderId =  trim($buffer[0]);
				if (!empty($orderId) && isInteger($orderId)) {
					
					$codPayStatus = strtolower(trim($buffer[1]));
					$completionDateStr = trim($buffer[2]);
					$chequeNo = trim($buffer[3]);
					
					$order = array('orderId'=>$orderId, 'completedDate'=>$completionDateStr, 'chequeNo'=> $chequeNo);
					
					if($codPayStatus == 'paid'){
						$paidOrders[] = $order;
					} else if($codPayStatus == 'risk'){
						$riskOrders[] = $order;
					} else {
						$msg .= "Order $orderId has invalid codPayStatus - $codPayStatus<br>";
					}
				}
			}
			fclose ($fd);
			$weblog->debug("cod_reconcile ::: ".count($paidOrders)+count($riskOrders)." valid orderids");
			
			if(!empty($paidOrders) || !empty($riskOrders)){
				update_cod_order_data($paidOrders, $riskOrders, $login);
				$msg .= count($paidOrders)+count($riskOrders)." orders processed";
				$weblog->debug("cod_reconcile :::".$msg);
			}else{
				$msg = "No orders present with codpaystatus paid or risk";
			}
		} else {
			$action = "upload";
			$msg =  "File not uploaded, try again ..";
		}
		$weblog->debug("cod_reconcile --- Message: $msg");
		$smarty->assign('message',$msg);
	}
	
} else if ($_POST['mode']=='lost') {
	$lostItemData = lost_items_data($_POST['lostOrderId'], $_POST['lostAwbNo']);
	
	if ($_POST['codSearchBtn'] == "DownloadAsCsv") {
		printAsCsv($lostItemData, "cod_lost_item_data");	
	}
	
} else if ($_POST['mode']=='updateOrderStatus') {
	if(!empty($_POST['codSelect']) && !empty($_POST['orderStatus'])) {
		modify_order_pending_status($_POST['codSelect'], $_POST['orderStatus']);
	}
	
} else if ($_POST['mode']=='lostItemUpdate') {
	modify_item_delivery_status($_POST['lostItemSelect'], $_POST['lostItemStatus']);
}

$smarty->assign('action',$action);
$smarty->assign('mode',$_POST['mode']);
$smarty->assign('post_data', $_POST);
$template ="cod_reconcile";
$smarty->assign('cod_search_data', $codOrderData);
$smarty->assign('lost_item_data', $lostItemData);
$smarty->assign("main",$template);
func_display("admin/home.tpl",$smarty);
?>