
<?php
require "./auth.php";
include_once("$xcart_dir/include/security.php");
include_once("../include/func/func.inventory.php");

if(isset($HTTP_POST_VARS["register"])){
	$sku_name= $HTTP_POST_VARS['sku'];
$sku_unitprice= $HTTP_POST_VARS['standard_unit_price'];
$vendor= $HTTP_POST_VARS['preferred_vendor'];
$currentcount= $HTTP_POST_VARS['current_value'];
$thresholdcount= $HTTP_POST_VARS['threshold_count'];
$thresholdemail=$HTTP_POST_VARS['threshold_email'];
$notes=$HTTP_POST_VARS['notes'];
$stringtext=$HTTP_POST_VARS['optionstr'];
$product_type_id=$HTTP_POST_VARS['ProductType'];
$style_id=$HTTP_POST_VARS['ProductStyle'];	
$product_code="";
	$style_code="";
	$option_code="";
	$optionstr="";
    $myarray = explode("#",$stringtext);
    for($i=0;$i<count($myarray);$i++)
    	{
       $element = $myarray[$i];
       $name_value = explode(":",$element);
       if($name_value[0]=="ProductType"){
       	$product_code=func_getproductcode($name_value[1]);
       }
    	elseif($name_value[0]=="ProductStyle"){
       	$style_code=func_getstylecode($name_value[1]);
       }
       else{
       	$option_code.=$name_value[1].':';
       	$option_String= func_getoption_namevalue($name_value[1]);
       	$optionstr.=$option_String[0].":".$option_String[1]." |";
        
       }
    	}
    	$option_code=substr($option_code,0,strlen($option_code)-1);
    	$optionstr=substr($optionstr,0,strlen($optionstr)-1);
    	
	$sku_createddate=time();	
	$sku_query="insert into `mk_inventory_sku` (sku_name,sku_unitprice,sku_createddate, ".
	"vendor,currentcount,thresholdcount,thresholdemail,notes,options,options_id,product_type_id,style_id) ".
	"values ('$sku_name',$sku_unitprice,'$sku_createddate','$vendor',$currentcount,$thresholdcount, ".
	"'$thresholdemail','$notes','$optionstr','$option_code',$product_type_id,$style_id)";
	db_query($sku_query);
	$smarty->assign("commentSuccess","Y");
	
	func_display("register_sku.tpl",$smarty);
	
}
elseif(isset($HTTP_POST_VARS["pr_id"])){
	$pr_id = $HTTP_POST_VARS["pr_id"];
	$pr_styles = func_get_productstyles_forproduct($pr_id);
	$smarty->assign("styles",$pr_styles);
	//$searchresults=func_get_producttypes();
	//$smarty->assign("types",$searchresults);
	//$smarty->assign("prselect",$pr_id);
	func_display("style_values.tpl",$smarty);
}
elseif(isset($HTTP_POST_VARS["st_id"])){
	$st_id = $HTTP_POST_VARS["st_id"];
	$pr_options = func_get_productoptions_forstyle($st_id);
	$smarty->assign("options",$pr_options);
	//$searchresults=func_get_producttypes();
	//$smarty->assign("types",$searchresults);
	//$smarty->assign("prselect",$pr_id);
	func_display("style_options.tpl",$smarty);
}
elseif(isset($HTTP_POST_VARS["validate"])){
	
	$stringtext = $HTTP_POST_VARS["validate"];
	$sku_message="This is a valid SKU";
	$product_code="";
	$style_code="";
	$option_code="";
	$sku_name="";
	$skustatus="Y";
	$skuarray = explode("@SKU",$stringtext);
	$producttext=$skuarray[0];
	$sku_name=$skuarray[1];
    $myarray = explode("#",$producttext);
    print_r($producttext);
    print_r($sku_name);
   // exit;
    for($i=0;$i<count($myarray);$i++)
    	{
       $element = $myarray[$i];
       $name_value = explode(":",$element);
       if($name_value[0]=="ProductType"){
       	$product_code=func_getproductcode($name_value[1]);
       if(empty($product_code))
       	{
       		$sku_message="Invalid Sku --> Product Type code not defined";
       		$skustatus="N";
       	}
       }
    	elseif($name_value[0]=="ProductStyle"){
       	$style_code=func_getstylecode($name_value[1]);
       	if(empty($style_code))
       	{
       		$sku_message="Invalid Sku --> Style code not defined";
       		$skustatus="N";
       	}
       }
       else{
       	$option=func_getoption($name_value[1]);
       	$option_code.=substr($option,0,1);
       }
    	}
	//$pr_code = func_get_productcode($st_id);
	$sku_no=$product_code.":".$style_code.":".$option_code;
	if($option_code=="")
	$sku_no=substr($sku_no,0,strlen($sku_no)-1);
	//To be done check sku by matching all three product_code,style and option1:option2
	$validsku=func_checksku($sku_no);
	if($sku_name!="" &&$sku_name!=$sku_no){
		$validsku=func_checksku($sku_name);
		$sku_no=$sku_name;
	}
	if(!empty($validsku)){
		$sku_message="Invalid --> SKU already in database.edit manually and validate again";
		$skustatus="N";
	}
	$smarty->assign("skumessage",$sku_message);
	$smarty->assign("skuno",$sku_no);
	$smarty->assign("optionstr",$producttext);
	$smarty->assign("skustatus",$skustatus);
	//$smarty->assign("options",$pr_options);
	//$searchresults=func_get_producttypes();
	//$smarty->assign("types",$searchresults);
	//$smarty->assign("prselect",$pr_id);
	func_display("sku_test.tpl",$smarty);
}
else{
$date=date("d",time())."-".date("M",time()).",".date("Y",time());
$searchresults=func_get_producttypes();
//$stylesresults=func_get_productstyles();


$smarty->assign("types",$searchresults);

//$smarty->assign("styles",$stylesresults);
$smarty->assign("date", $date);
func_display("register_sku.tpl",$smarty);
}

?>