<?php

require_once( "../auth.php");
require_once $xcart_dir."/include/security.php";
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");


//block for widget key value
if(!empty($_POST["key-update"])){
	$updatekey =  mysql_real_escape_string($_POST['widget-key']);
	$updatevalue =  mysql_real_escape_string($_POST['widget-val']);
	$sql = "UPDATE mk_widget_key_value_pairs SET `value`='$updatevalue' WHERE `key`='$updatekey'";
	db_query($sql);
	\cache\keyvaluepairs\WidgetKeyValuePairs::getKeyValuePairInstance()->refresh($updatekey);
}

$fest_month_start_date = WidgetKeyValuePairs::getWidgetValueForKey('fest_month_start_date');
$fest_month_end_date= WidgetKeyValuePairs::getWidgetValueForKey('fest_month_end_date');
$fest_week_start_day = WidgetKeyValuePairs::getWidgetValueForKey('fest_week_start_day');
$fest_week_end_day = WidgetKeyValuePairs::getWidgetValueForKey('fest_week_end_day');
$fest_leader_board_id = WidgetKeyValuePairs::getWidgetValueForKey('fest_leader_board_id');
$fest_cache_invalidate_time = WidgetKeyValuePairs::getWidgetValueForKey('fest_cache_invalidate_time');
$fest_topNbuyers = WidgetKeyValuePairs::getWidgetValueForKey('fest_topNbuyers');
$fest_topNCategoryBuyers = WidgetKeyValuePairs::getWidgetValueForKey('fest_topNCategoryBuyers');
$fest_lucky_draw_increment_amount = WidgetKeyValuePairs::getWidgetValueForKey('fest_lucky_draw_increment_amount');
$fest_lucky_draw_min_amount = WidgetKeyValuePairs::getWidgetValueForKey('fest_lucky_draw_min_amount');
$fest_shop_more_to_win_msg = WidgetKeyValuePairs::getWidgetValueForKey('fest_shop_more_to_win_msg');
$fest_top_in_all_cat_msg = WidgetKeyValuePairs::getWidgetValueForKey('fest_top_in_all_cat_msg');

$fest_prizes_semicolon_separated = WidgetKeyValuePairs::getWidgetValueForKey('fest_prizes_semicolon_separated');
$fest_prizes_men_semicolon_separated = WidgetKeyValuePairs::getWidgetValueForKey('fest_prizes_men_semicolon_separated');
$fest_prizes_women_semicolon_separated = WidgetKeyValuePairs::getWidgetValueForKey('fest_prizes_women_semicolon_separated');


$smarty->assign("fest_month_start_date", $fest_month_start_date);
$smarty->assign("k_fest_month_start_date", 'fest_month_start_date');
$smarty->assign("fest_month_end_date", $fest_month_start_date);
$smarty->assign("k_fest_month_start_date", 'fest_month_end_date');
$smarty->assign("fest_week_start_day", $fest_week_start_day);
$smarty->assign("k_fest_week_start_day", 'fest_week_start_day');
$smarty->assign("fest_week_end_day", $fest_week_end_day);
$smarty->assign("k_fest_week_end_day", 'fest_week_end_day');
$smarty->assign("fest_leader_board_id", $fest_leader_board_id);
$smarty->assign("k_fest_leader_board_id", 'fest_leader_board_id');
$smarty->assign("fest_cache_invalidate_time", $fest_cache_invalidate_time);
$smarty->assign("k_fest_cache_invalidate_time", 'fest_cache_invalidate_time');
$smarty->assign("fest_topNbuyers", $fest_topNbuyers);
$smarty->assign("k_fest_topNbuyers", 'fest_topNbuyers');
$smarty->assign("fest_topNCategoryBuyers", $fest_topNCategoryBuyers);
$smarty->assign("k_fest_topNCategoryBuyers", 'fest_topNCategoryBuyers');
$smarty->assign("fest_lucky_draw_increment_amount", $fest_lucky_draw_increment_amount);
$smarty->assign("k_fest_lucky_draw_increment_amount", 'fest_lucky_draw_increment_amount');
$smarty->assign("fest_lucky_draw_min_amount", $fest_lucky_draw_min_amount);
$smarty->assign("k_fest_lucky_draw_min_amount", 'fest_lucky_draw_min_amount');
$smarty->assign("fest_shop_more_to_win_msg", $fest_shop_more_to_win_msg);
$smarty->assign("k_fest_shop_more_to_win_msg", 'fest_shop_more_to_win_msg');
$smarty->assign("fest_top_in_all_cat_msg", $fest_top_in_all_cat_msg);
$smarty->assign("k_fest_top_in_all_cat_msg", 'fest_top_in_all_cat_msg');

//SMARTY ASSIGN FOR SEMI COLON SEPERATED
$fest_prizes_semicolon_separated = explode(";",$fest_prizes_semicolon_separated);
$fest_prizes_men_semicolon_separated =  explode(";",$fest_prizes_men_semicolon_separated);
$fest_prizes_women_semicolon_separated = explode(";",$fest_prizes_women_semicolon_separated);

$smarty->assign("fest_prizes_semicolon_separated", $fest_prizes_semicolon_separated);
$smarty->assign("j_fest_prizes_semicolon_separated", json_encode($fest_prizes_semicolon_separated));
$smarty->assign("k_fest_prizes_semicolon_separated", 'fest_prizes_semicolon_separated');
$smarty->assign("fest_prizes_men_semicolon_separated", $fest_prizes_men_semicolon_separated);
$smarty->assign("j_fest_prizes_men_semicolon_separated", json_encode($fest_prizes_men_semicolon_separated));
$smarty->assign("k_fest_prizes_men_semicolon_separated", 'fest_prizes_men_semicolon_separated');
$smarty->assign("fest_prizes_women_semicolon_separated", $fest_prizes_women_semicolon_separated);
$smarty->assign("j_fest_prizes_women_semicolon_separated", json_encode($fest_prizes_women_semicolon_separated));
$smarty->assign("k_fest_prizes_women_semicolon_separated", 'fest_prizes_women_semicolon_separated');

$smarty->assign("main",'shopping_fest_leader');
func_display("admin/home.tpl",$smarty);
?>
