<?php
/**
 * Created by Lohiya.
 * Date: 08 Jul, 2011
 */
require_once( "../auth.php");
require_once $xcart_dir."/include/security.php";
require_once $xcart_dir."/modules/coupon/SpecialCouponGenerator.php";
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");
/*
*
mode = 1 ->INSERT, 2-> update, 3-> delete
key = columname
value = value
*
*/

$mode = $_POST['mode'];
if(!empty($mode)){
	$slab = array();
	$slab["id"] = intval($_POST['id']);
	$slab["lowerBound"] = intval($_POST['lowerBound']);
	$slab["noOfCoupons"] = intval($_POST['noOfCoupons']);
	$slab["CouponValue"] = intval($_POST['CouponValue']);
	$slab["CouponMinPurchaseValue"] = intval($_POST['CouponMinPurchaseValue']);
	$slab["isActive"] = 1;
        $loggedInAdminUser = $XCART_SESSION_VARS['identifiers']['A']['login'];
	$slab["updatedBy"] =  $loggedInAdminUser;
	$slab["updatedOn"] = "now()";

	$retVal = 0;	
	if(intVal($mode)==1)
		$slab["createdOn"] = "now()";
	if(intVal($mode)==3)
		SpecialCouponGenerator::deleteSlab($slab["id"]);	
	else
		$retVal = SpecialCouponGenerator::setSlabs($slab, intval($mode));
	
	if($retVal<0){
		if($retVal==-1)
			$errorMsg = "Slab already exists. Duplicate not allowed!";
		else if($retVal==-2)
			$errorMsg = "0 is not allowed for any field";
		else if($retVal==-3)
			$errorMsg = "Dont hack anything. Contact dev team if you are not!";
		$smarty->assign('error_msg',$errorMsg);
	}
}
//block for widget key value
if(!empty($_POST["key-update"])){
	$updatekey =  mysql_real_escape_string($_POST['widget-key']);
	$updatevalue =  mysql_real_escape_string($_POST['widget-val']);
	if($updatekey != 'myntraFestOrderConfirmationMailMsg'){
		$updatevalue=preg_replace("/ +/","",$updatevalue);
	}
	$sql = "UPDATE mk_widget_key_value_pairs SET `value`='$updatevalue' WHERE `key`='$updatekey'";
	db_query($sql);
	\cache\keyvaluepairs\WidgetKeyValuePairs::getKeyValuePairInstance()->refresh($updatekey);
}


$coupon_slabs = SpecialCouponGenerator::getSlabs();
$smarty->assign("coupon_slabs", $coupon_slabs);
$couponsPrefix = WidgetKeyValuePairs::getWidgetValueForKey('myntraFestCouponPrefix');
$couponsGroupName = WidgetKeyValuePairs::getWidgetValueForKey('myntraFestCouponGroupName');
$orderConfirmationMsg = WidgetKeyValuePairs::getWidgetValueForKey('myntraFestOrderConfirmationMailMsg');

$smarty->assign("couponPrefix", $couponsPrefix);
$smarty->assign("couponGroupName", $couponsGroupName);
$smarty->assign("orderConfirmationMsg", $orderConfirmationMsg);
$smarty->assign("couponPrefixKey", "myntraFestCouponPrefix");
$smarty->assign("couponGroupNameKey", "myntraFestCouponGroupName");
$smarty->assign("orderConfirmationMsgKey",'myntraFestOrderConfirmationMailMsg');


$smarty->assign("main",'shopping_fest_coupon');
func_display("admin/home.tpl",$smarty);
?>
