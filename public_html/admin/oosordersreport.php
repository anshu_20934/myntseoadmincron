<?php

include_once 'auth.php';
include_once $xcart_dir.'/include/security.php';
include_once $xcart_dir.'/include/func/func.order.php';

$action = $HTTP_POST_VARS['action'];
if(!$action)
	$action = 'view';

if($action == 'search') {
	$response = func_get_OOS_orders($HTTP_POST_VARS);
	header('Content-type: text/x-json');
	print json_encode($response);
	exit;	
} else if($action == 'download_report') {
	$response = func_get_OOS_orders($HTTP_POST_VARS, false);
	$objPHPExcel = func_create_results_file($response['results']);
	$filename = func_save_results_file($objPHPExcel, "OOSorders");
	header('Content-type: text/x-json');
	print json_encode($filename);
	exit;
}

$smarty->assign("action", $action);
$smarty->assign("main", "oosordersreport");
func_display("admin/home.tpl", $smarty);

?>