<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: partner_orders.php,v 1.10 2006/01/11 06:55:58 mclap Exp $
#

require "./auth.php";
require $xcart_dir."/include/security.php";
 
if (!$active_modules['XAffiliate'])
    func_header_location ("error_message.php?access_denied&id=27");

$location[] = array(func_get_langvar_by_name("lbl_partners_orders"), "");

#
# Define data for the navigation within section
#
$dialog_tools_data["right"][] = array("link" => "partner_report.php", "title" => func_get_langvar_by_name("lbl_partner_accounts"));
$dialog_tools_data["right"][] = array("link" => "payment_upload.php", "title" => func_get_langvar_by_name("lbl_payment_upload"));

if ($StartDay)
	$search['start_date'] = mktime(0, 0, 0, $StartMonth, $StartDay, $StartYear);

if ($EndDay)
	$search['end_date'] = mktime(23, 59, 59, $EndMonth, $EndDay, $EndYear);

if ($search) {
	$where = array();

	if ($search['start_date'] && $search['end_date'])
		$where[] = $search['end_date']." > $sql_tbl[orders].date AND $sql_tbl[orders].date > ".$search['start_date'];
	if ($search['login'])
		$where[] = "$sql_tbl[partner_payment].login = '$search[login]'";

	if($search['status'])
		$where[] = "$sql_tbl[orders].status = '$search[status]'";

	if($search['orderid'])
		$where[] = "$sql_tbl[orders].orderid = '$search[orderid]'";

	if ($search['paid'])
		$where[] = " IF($sql_tbl[partner_payment].paid = 'Y', 'Y', IF($sql_tbl[orders].status IN ('C','P'), 'A', 'N')) = '$search[paid]'";

	if ($where)
		$where_condition = " AND ".implode(" AND ", $where);

	$report = func_query("SELECT $sql_tbl[partner_payment].*, $sql_tbl[customers].*, $sql_tbl[orders].subtotal, $sql_tbl[orders].date, $sql_tbl[orders].status AS order_status, IF($sql_tbl[partner_payment].paid = 'Y', 'Y', IF($sql_tbl[orders].status IN ('C','P'), 'A', '')) as paid FROM $sql_tbl[partner_payment], $sql_tbl[orders], $sql_tbl[customers] WHERE $sql_tbl[partner_payment].login=$sql_tbl[customers].login AND $sql_tbl[partner_payment].orderid=$sql_tbl[orders].orderid AND $sql_tbl[customers].status = 'Y' AND $sql_tbl[customers].usertype = 'B'".$where_condition." ORDER BY $sql_tbl[partner_payment].add_date, $sql_tbl[customers].login");
	if ($mode == 'export') {
		$smarty->assign ("delimiter", $delimiter);
		if ($report) {
			foreach ($report as $key=>$value) {
				foreach ($value as $rk=>$rv) {
					$report[$key][$rk] = '"' . str_replace ("\"", "\"\"", $report[$key][$rk]) . '"';
				}
			}
		}

		$smarty->assign ("report", $report);

		header ("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=partner_orders.csv");
		func_display("admin/main/partner_orders_export.tpl",$smarty);
		exit;
	}

	$smarty->assign ("orders", $report);
}

$smarty->assign ("main", "partner_orders");

$smarty->assign("partners", func_query("SELECT login FROM $sql_tbl[customers] WHERE usertype = 'B' AND status = 'Y' ORDER BY login"));

$smarty->assign("search", $search);
$smarty->assign ("month_begin", mktime(0,0,0,date('m'),1,date('Y')));

$smarty->assign("dialog_tools_data", $dialog_tools_data);

# Assign the current location line
$smarty->assign("location", $location);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
