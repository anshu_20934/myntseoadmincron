<?php
require_once "./auth.php";
require_once($xcart_dir.'/include/search/class.SearchURLGenerator.php');
$parameters = $_GET;
$offset;
$searchLanding;
$lookType;
$addLimit =10;
$addArray = array();
for($i=0;$i<$addLimit;$i++)
{
	$addArray[$i]=$i;
}
$smarty->assign("addArray",$addArray);
		

if($REQUEST_METHOD == "POST"){
	try {
	//echo"hitting";
		$input =  $HTTP_POST_VARS;
		//echo"Data is <pre>";print_r($input);
		if($HTTP_POST_VARS['mode'] == "AddMode")
		{
			$looktype = "Add";
			//echo"ADD is <pre>";print_r($HTTP_POST_VARS);
		}
		if($HTTP_POST_VARS['mode'] == "SearchMode")
		{
			$looktype = "";
			//echo"ADD is <pre>";print_r($HTTP_POST_VARS);
		}
		if($HTTP_POST_VARS['mode'] == "Previous")
		{
			$offset = $HTTP_POST_VARS['offset'];
			$searchLanding = $HTTP_POST_VARS['searchLanding'];
			$offset--;
		}
		if($HTTP_POST_VARS['mode'] == "Next")
		{
			$offset = $HTTP_POST_VARS['offset'];
			$searchLanding = $HTTP_POST_VARS['searchLanding'];
			$offset++;
		}
		if($HTTP_POST_VARS['mode'] == "delete")
		{
			$offset = $HTTP_POST_VARS['offset'];
			$searchLanding = $HTTP_POST_VARS['searchLanding'];
			$offset = 0;
			$id = $HTTP_POST_VARS['id'];
			$deleteQuery = "delete  from search_synonym_table where id = $id";
			func_query($deleteQuery);
			//echo"Data is <pre>";print_r($HTTP_POST_VARS);
		}
		if($HTTP_POST_VARS['mode'] == "update")
		{
			$offset = $HTTP_POST_VARS['offset'];
			$searchLanding = $HTTP_POST_VARS['searchLanding'];
			$offset = 0;
			$id = $HTTP_POST_VARS['id'];
			$selectQuery = "select original from search_synonym_table where id = $id";		
			$selectOriginalResult = func_query($selectQuery);
			$originalString = $input[output][$id][original];
			$correctedString = $input[output][$id][corrected];
			$originalString = strtolower($originalString);
			$correctedString = strtolower($correctedString);
			if($selectOriginalResult[0][original] !=$originalString)
			{
				$CountCheckquery  = "select count(*) as count from search_synonym_table where original = '$originalString'";
				$countCheck = func_query($CountCheckquery);
				if($countCheck[0][count]>0)
				{
					$message = "<h1 style=\"color:#FF0000\">Error Message:Already  one other search result is mapped to $originalString</h1>";
					throw new Exception($message);
				}
			}
			$updateQuery = "Update search_synonym_table 
							set original='$originalString',
							corrected = '$correctedString'
							where id = $id";
			func_query($updateQuery);
			//echo"Data is <pre>";print_r($HTTP_POST_VARS);
		}
		if($HTTP_POST_VARS['mode'] == "StringSearch")
		{
			$offset = 0;
			$searchLanding = $HTTP_POST_VARS['searchLanding'];
		}
	          ############## Condition to add groups ############
		if($HTTP_POST_VARS['mode'] == "addgroup")
		{
			$message = "";	
			$errorMessage = "";
			$CorrectMessage ="";
			$searchLanding = $HTTP_POST_VARS['searchLanding'];
			$addOutput = $input[addOutput];
			foreach ($addOutput as $key=>$row )	
			{
				if(empty($row[original])&&empty($row[corrected]))
				{
					continue;
				}
				else if(empty($row[corrected]))
				{
					$errorMessage = $errorMessage."--$row[original] has empty correct value--";
					continue;
				}
				else if(empty($row[original]))
				{
					$errorMessage = $errorMessage."--$row[corrected] has empty no original value--";
					continue;
				}
				else 
				{
					//Look for value is present or not;
					$originalString= strtolower($row[original]);
					$correctedString = strtolower($row[corrected]);
					$CountCheckquery  = "select count(*) as count from search_synonym_table where original = '$originalString'";
					$countCheck = func_query($CountCheckquery);
					if($countCheck[0][count]>0)
					{
						$errorMessage = $errorMessage."--Already  one other search result is mapped to $originalString, delete that to add more--";
						continue;
					}
					else 
					{
						$updateQuery = "insert into  search_synonym_table 
						(original, corrected) values ('$originalString','$correctedString')";
						func_query($updateQuery);
						$CorrectMessage = $CorrectMessage."--Updated For $originalString with $correctedString--";
					}
				}
				
			}
			if(!empty($errorMessage))
			{
				$errorMessage = "<h1 style=\"color:#FF0000\">Error Message(s)".$errorMessage."</h1>";
			}
			if(!empty($CorrectMessage))
			{
				$CorrectMessage = "<h1 style=\"color:#00FF00\">Succes Message(s)".$CorrectMessage."</h1>";
			}
			$message = $errorMessage."<pre>".$CorrectMessage;
		}
	}
	catch(Exception $e)
	{}
 }
          



$numberOfentry = 50;


if(empty($offset))
{
	$offset =0;
}
$smarty->assign("offset",$offset);



$sql = "select id,original,corrected from search_synonym_table ";
if(!empty($searchLanding))
{
	$sql = $sql." where original like '%$searchLanding%' or corrected like  '%$searchLanding%'";
}
$leftLimit = $offset*$numberOfentry;
$rightLimit = $leftLimit + $numberOfentry -1;
$sql = $sql." limit $leftLimit ,$rightLimit;";
$SearchResult = func_query($sql,true);
$smarty->assign("searchresult",$SearchResult);
//echo"<pre>";print_r($sql);
$sql = "select count(*) as count from search_synonym_table";
if(!empty($searchLanding))
{
	$sql = $sql." where original like '%$searchLanding%' or corrected like  '%$searchLanding%'";
}
$countResult = func_query($sql,true);
//echo"<pre>";print_r($countResult);
$maxoffset =ceil (($countResult[0][count]-1)/$numberOfentry) -1;
$smarty->assign("maxoffset",$maxoffset);


$smarty->assign("searchLanding",$searchLanding);

if(empty($looktype))
{
	$looktype = "Search";
}
$smarty->assign("looktype",$looktype);
$smarty->assign("main","search_synonym_admin");
$smarty->assign("message",$message);
func_display("admin/home.tpl",$smarty);
?>