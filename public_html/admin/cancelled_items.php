<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
include_once("../include/func/func.mkcore.php");
include_once("../include/func/func.inventory.php");
include_once("../include/class/class.orders.php");
include_once("../include/func/func_sku.php");

x_load('db', 'order');

function create_new_qty_breakup($old_qty_breakup, $new_qty) {
	$new_qty_breakup = array();
	$qty_breakup_array = explode(",", $old_qty_breakup);
	foreach ($qty_breakup_array as $qty_breakup) {
		$size_qty_array = explode(":", $qty_breakup);
		if($size_qty_array[1]) {
			$new_qty_breakup[] = $size_qty_array[0].":".$new_qty;
		} else 
			$new_qty_breakup[] = $qty_breakup;
	}
	return implode(",", $new_qty_breakup);
}

$action = $_POST['action'];
if(!$action) {
	$action = 'view';
}

if($action == 'revert-cancel'){
	$orderid = $_POST['orderid'];
	$weblog->info("Order Id - ". $orderid);
	$order_details = func_query("SELECT status, total, subtotal, coupon_discount, qtyInOrder from $sql_tbl[orders] WHERE orderid = $orderid");
	$order_status = $order_details[0]['status'];
	$order_data = array('total'=>$order_details[0]['total'], 'subtotal'=>$order_details[0]['subtotal'],
						'coupon_discount' => $order_details[0]['coupon_discount'], 'qtyInOrder'=>$order_details[0]['qtyInOrder']);
	
	$cancelled_item_details = func_query("SELECT * from $sql_tbl[order_details] WHERE itemid in (".$_POST['itemids'].")");
	$comment = "";
	$message = "";
	foreach ($cancelled_item_details as $cancelled_item) {
		$qty_to_add = $cancelled_item['amount'];
		$uncancelled_part_for_item = func_query("SELECT * from $sql_tbl[order_details] WHERE productid = ".$cancelled_item['productid']." AND $sql_tbl[order_details].item_status != 'IC' AND orderid = ".$orderid);
		$itemid = $cancelled_item['itemid'];
		$can_add_item = false;
		
		if(!$uncancelled_part_for_item) {
			$weblog->info("Uncancelled item not there for this item - ". $itemid);
			$sku_counts = get_sku_counts($cancelled_item['itemid']);
			if($sku_counts['inv_count'] >= $qty_to_add) {
				$query_data = array("item_status" => "UA");
				func_array2update("order_details", $query_data, "itemid = ".$cancelled_item['itemid']);
				
				// update quantites in item options table
				$option_qty = func_query("SELECT * from mk_order_item_option_quantity where itemid = ".$cancelled_item['itemid']);
				$itemid = $cancelled_item['itemid']; 
				$optionid = $option_qty[0]['optionid'];
				func_array2update("mk_order_item_option_quantity", array("quantity"=>$cancelled_item['amount']), "itemid = $itemid and optionid = $optionid");
				
				$can_add_item = true;
			}
		} else {
			$weblog->info("Uncancelled item is present for this item - ". $itemid);
			$uncancelled_part = $uncancelled_part_for_item[0];
			$sku_counts = get_sku_counts($uncancelled_part['itemid']);
			if($sku_counts['inv_count'] >= $qty_to_add) {
				$new_qty = $uncancelled_part['amount']+$qty_to_add;
				$total_amount = $cancelled_item['total_amount'] + $uncancelled_part['total_amount'];
				$tax_amount = $cancelled_item['taxamount'] + $uncancelled_part['taxamount'];
				$coupon_discount = $cancelled_item['coupon_discount_product'] + $uncancelled_part['coupon_discount_product'];
				$query_data = array(
					"amount" => $new_qty, 'total_amount'=> $total_amount, 'taxamount'=>$tax_amount,
					"coupon_discount_product" => $coupon_discount,
					"quantity_breakup" =>create_new_qty_breakup($uncancelled_part['quantity_breakup'], $new_qty));
				
				func_array2update("order_details", $query_data, "itemid = ".$uncancelled_part['itemid']);
				
				// remove the cancelled row.. 
				$query = "delete from $sql_tbl[order_details] WHERE itemid=".$cancelled_item['itemid'];
				db_query($query);
				$query = "delete from mk_order_item_option_quantity WHERE itemid=".$cancelled_item['itemid'];
				db_query($query);
				
				// update quantites in item options table
				$option_qty = func_query("SELECT * from mk_order_item_option_quantity where itemid = ".$uncancelled_part['itemid']);
				$itemid = $uncancelled_part['itemid']; 
				$optionid = $option_qty[0]['optionid'];
				func_array2update("mk_order_item_option_quantity", array("quantity"=>$new_qty), "itemid = $itemid and optionid = $optionid");
				
				$can_add_item = true;
			}
		}
		$weblog->info("Can add item - ".$can_add_item);
		if($can_add_item) {
			$price_increase = $cancelled_item['price']*$qty_to_add;
			$order_data['total'] += ($price_increase - $cancelled_item['coupon_discount_product']);
			$order_data['subtotal'] += $price_increase;
			$order_data['coupon_discount'] += $cancelled_item['coupon_discount_product'];
			if($order_status == 'F') {
				$order_data['status'] = 'Q';
			}
		
			$comment .= "Adding $qty_to_add pieces back to item - ".$itemid." in order. ";
			
			//reduce the quantity from inventory for the product
			update_sku_for_item_quantity($itemid, false, $qty_to_add, 'CIA', $orderid);
			
			$message .= "$qty_to_add pieces of Item - $itemid are added back to the order.<br/>";
			$order_data['qtyInOrder'] += $qty_to_add;
		} else {
			$message .= "$qty_to_add pieces of Item - $itemid could not be added back because of no stock.<br>";
		}
	}
	$weblog->info("Update order details - ".$can_add_item);
	func_array2update("orders", $order_data, "orderid = $orderid");
	if($comment != "") {
		$commenttime = time();
		$cancelledby = $_POST['login'];
		$query = "insert into `mk_ordercommentslog` (orderid,commentaddedby,commenttitle,commenttype,newaddress,giftwrapmessage,description,attachmentname,commentdate) values ($orderid,'$cancelledby','Items Added','Note','','','$comment','',$commenttime)";
		db_query($query);
	}
	$weblog->info("End");
	$smarty->assign("message", $message);
	$smarty->assign("action", "complete");
	
} else {
	$productsInCart =
		func_query("SELECT gift_card_amount,itemid, product, '' AS sku_code, sp.article_number AS article_number, productid, product_style, product_type, price, amount, discount, cart_discount_split_on_ratio as cart_discount, coupon_discount_product, pg_discount, cash_redeemed, cashback, difference_refund, taxamount, item_loyalty_points_used, $sql_tbl[order_details].quantity_breakup as quantity_breakup FROM $sql_tbl[order_details]
		LEFT JOIN mk_style_properties sp on (sp.style_id = $sql_tbl[order_details].product_style) WHERE $sql_tbl[order_details].item_status = 'IC' AND $sql_tbl[order_details].orderid = ".$_GET['orderid']);
    $payment_method = func_query_first_cell("select payment_method from xcart_orders where orderid = ".$_GET['orderid']);
    $loyalty_points_conversion_factor = func_query_first_cell("select loyalty_points_conversion_factor from xcart_orders where orderid = ".$_GET['orderid']);
	foreach($productsInCart as $key => $order)
	{
		//Break-up quantities
		if($productsInCart[$key]['quantity_breakup'] != "")
		{
			$breakup_qty = explode(",", $productsInCart[$key]['quantity_breakup']);
			$productsInCart[$key]['quantity_breakup'] = '';
			foreach ($breakup_qty as $qty) {
				$tmp = explode(":", $qty);
				if($tmp[1]) {
					$productsInCart[$key]['quantity_breakup'] .= $qty.'&nbsp;'; 
				}	
			}
		}
		$totalPrice = ($productsInCart[$key]['price']*$productsInCart[$key]['amount']);		
		$productsInCart[$key]['totalPrice'] = number_format($totalPrice, 2);
        if($payment_method == 'cod') {
            $productsInCart[$key]['final_amount'] = number_format($totalPrice - $productsInCart[$key]['cart_discount'] - $productsInCart[$key]['discount'] - $productsInCart[$key]['coupon_discount_product'] - $productsInCart[$key]['pg_discount'] - $productsInCart[$key]['cash_redeemed'] + $productsInCart[$key]['taxamount'] - $productsInCart[$key]['gift_card_amount'], 2);
        }else{
            $productsInCart[$key]['final_amount'] = number_format($totalPrice - $productsInCart[$key]['cart_discount'] - $productsInCart[$key]['discount'] - $productsInCart[$key]['coupon_discount_product'] - $productsInCart[$key]['pg_discount'] - $productsInCart[$key]['cash_redeemed'] + $productsInCart[$key]['taxamount'], 2);
        }
        $productsInCart[$key]['item_loyalty_credit'] = number_format(($loyalty_points_conversion_factor * $productsInCart[$key]['item_loyalty_points_used']), 2);
        
		//Query for retriving the style name from mk_product_style
		$stylename = "SELECT name,label FROM $sql_tbl[mk_product_style] WHERE id = ".$productsInCart[$key]['product_style']."";
		$styleresult = db_query($stylename);
		$stylerow = db_fetch_array($styleresult);
		$productsInCart[$key]['productStyleName'] = $stylerow['name'];
	    $productsInCart[$key]['productStyleLabel'] = $stylerow['label'];
	
	    $productname = "SELECT product FROM xcart_products WHERE productid = ".$productsInCart[$key]['productid']."";
		$productresult = db_query($productname);
		$productrow = db_fetch_array($productresult);
	    if(!empty($productrow))
		$productsInCart[$key]['productName'] = $productrow['product'];
	
		$typename = "SELECT name, label, image_t FROM $sql_tbl[producttypes] WHERE id = ".$productsInCart[$key]['product_type']."";
		$typeresult = db_query($typename);
		$row = db_fetch_array($typeresult);
		//$productsInCart[$key]['productStyleName'] = $row['name'];
	
		$areaId = func_get_all_orientation_for_default_customization_area($productsInCart[$key]['product_style']);
	
		$defaultImage = "SELECT image_portal_T AS ProdImage
								FROM $sql_tbl[products] odc
								WHERE productid = ".$productsInCart[$key]['productid']."";
	
		$ImageResult = db_query($defaultImage);
	
		$ImageRow = db_fetch_array($ImageResult);
		//$productsInCart[$key]['productImage'] = $ImageRow['ProdImage'];
		$productsInCart[$key]['product_type'] = $row['name'];
		$productsInCart[$key]['productTypeLabel'] = $row['label'];
		if(preg_match('/http:\/\//',$ImageRow['ProdImage']))
	   {
	        	$productsInCart[$key]['designImagePath'] = $ImageRow['ProdImage'];
	    }
		else
		{
			  $productsInCart[$key]['designImagePath'] = ".".$ImageRow['ProdImage'];
		}
		//$productsInCart[$key]['designImagePath'] = $ImageRow['ProdImage'];
		$productsInCart[$key]['totalPrice'] = $productsInCart[$key]['price'] * $productsInCart[$key]['amount'];

	}
	$smarty->assign("productsInCart", $productsInCart);
	$smarty->assign("action", "view");
	
	$order_status_row = func_query("SELECT status from $sql_tbl[orders] WHERE orderid = ".$_GET['orderid']);
	$smarty->assign("status", $order_status_row[0]['status']);
	$smarty->assign("orderid", $_GET['orderid']);
}

$smarty->assign("main","cancelled_items");
func_display("admin/home.tpl",$smarty);
?>
