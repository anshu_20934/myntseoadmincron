<?php

sendMailForCriticalAlert(100);

function sendMailForCriticalAlert($discrepancyCount) {
    $message = "Discrepancy count more than threshold. Circuit breaker have kicked in! Please check and ensure data is fixed! \n You may check 'keep and sync on' mail to get more details..'";
    $from = "From: wmsoncallalert@myntra.com";
    $to = "engg_erp_wms@myntra.com, engg_erp_oms@myntra.com, wmsoncall@myntra.com, omsoncall@myntra.com, noc@myntra.com, myntra.sms.alerts@gmail.com";
    $subject = "[CRITICAL-ALERT] BOC Watchdog has breached threshold. Circuit breaker has kicked in! Check and fix! DiscrepancyCount:".$discrepancyCount;
    mail($to,$subject,$message,$from);
}
?>
