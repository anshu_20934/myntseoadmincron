
<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
include_once("../include/func/func.reports.php");
include_once("../sanitizept.inc.php");
if(isset($HTTP_POST_VARS["editreport"])){
	$reportid= $HTTP_POST_VARS['reportid'];
	$reportname= $HTTP_POST_VARS['reportname'];
	$reportgroup= $HTTP_POST_VARS['reportgroup'];
	$reporttype=$HTTP_POST_VARS['reporttype'];
	$que= $HTTP_POST_VARS['query'];
	$sumque= $HTTP_POST_VARS['sumquery'];
	$chkstring = strtoupper($que);
	$chksum = strtoupper($sumque);
	$ckhflag=false;
	$ckhflagsum=false;
	$findme   = array("DROP","UPDATE","DELETE","INSERT");
	foreach ($findme as $chk) {
		$pos = strpos($chkstring, $chk);
		$posum = strpos($chksum, $chk);
		if ($pos === false) {
		}
		else{
			$chkflag=true;

		}
		
		if ($posum === false) {
		}
		else{
			$chkflagsum=true;

		}
			
	}
	if($chkflag||$chkflagsum){
		$smarty->assign("commentSuccess","NA");
		//$smarty->assign("commentmsg1","Not authorized to add these queries");
		func_display("edit_report.tpl",$smarty);
	}
	else{
	$mailto= $HTTP_POST_VARS['mailto'];
	$filternames = $_POST['fname'];
	$filterids = $_POST['fid'];
	$createddate=time();
//	$query="insert into `mk_reports` (name,createddate,groupid,query,sumquery) ".
//	"values ('$reportname','$createddate','$reportgroup','$query','$sumquery') ";
    $que=str_replace("LT","<",$que);
    $que=str_replace("GT",">",$que);
    $sumque=str_replace("LT","<",$sumque);
    $sumque=str_replace("GT",">",$sumque);
  // $que=addslashes($que);
   //$sumque=addslashes($sumque);
	$query="update `mk_reports` set name='$reportname' ,groupid='$reportgroup' ,query='$que' ,
	sumquery='$sumque' , mailto='$mailto',report_type='$reporttype' where id='$reportid' ";
	db_query($query);
//	$gen_reportid = mysql_insert_id();
//	$filternames = $HTTP_POST_VARS['fname'];
//	$filtertypes = $HTTP_POST_VARS['ftype'];
//	$filteropts = $HTTP_POST_VARS['fopt'];
//	for($i=0;$i<count($filternames);$i++){
//		$fquery = "insert into `mk_reports_filters` (report_id,name,datatype_id,attribute)".
//		"values ('$gen_reportid','$filternames[$i]','$filtertypes[$i]','$filteropts[$i]');";
//		db_query($fquery);
//	}
  
	for($i=0;$i<count($filternames);$i++){
		$default_value=$_POST[$filternames[$i]];
		$filterid=$filterids[$i];
		$fquery = "update `mk_reports_filters` set default_value='$default_value' where id='$filterid'";
		db_query($fquery);
	}
	$smarty->assign("commentSuccess","EDIT");

	func_display("edit_report.tpl",$smarty);
	}

}
if(isset($HTTP_POST_VARS["schedule"])){
	$reportid= $HTTP_POST_VARS['reportid'];
	$scheduletype=$HTTP_POST_VARS['scheduletype'];
	$scheduledate=$HTTP_POST_VARS['scheduledate'];
	$mailto= $HTTP_POST_VARS['mailto'];
	$optstring="";
	$nextrun=111111;
	//$createddate=time();
                if($scheduletype=="DAL"){
                	$optstring=$HTTP_POST_VARS['daltime'];
                	$myarray =explode(":",$optstring);
                	$today=mktime($myarray[0], $myarray[1], $myarray[2], date("m")  , date("d"), date("Y"));
                	$tomorrow  = mktime($myarray[0],$myarray[1], $myarray[2], date("m")  , date("d")+1, date("Y"));
                	$time=time();
                	$nextrun=$today;
                	if($today<$time){
                	$nextrun=$tomorrow;
                	}
	                }
	                elseif ($scheduletype=="WEK"){
	                	$dayofweek=$HTTP_POST_VARS['dayofweek'];
	                	$optstr=$HTTP_POST_VARS['wektime'];
	                	$myarray =explode(":",$optstr);
	                	$optstring=$dayofweek."|".$optstr;
	                	$day=date('w');
	                	$daydif;
	                	if($day<$dayofweek){
	                	$daydif=$dayofweek-$day;
	                	}
	                	else{
	                		$daydif=7-$day+$dayofweek;
	                	}
	                $today=mktime($myarray[0], $myarray[1], $myarray[2], date("m")  , date("d"), date("Y"));
                	$nextrun=mktime($myarray[0],$myarray[1], $myarray[2], date("m")  , date("d")+$daydif, date("Y"));
               	
	                }
                    elseif ($scheduletype=="MON"){
                    	$dayofmonth=$HTTP_POST_VARS['dayofmonth'];
	                	$optstr=$HTTP_POST_VARS['montime'];
	                	$myarray =explode(":",$optstr);
	                	$optstring=$dayofmonth."|".$optstr;
	                	$daymonth=date('j');
	                	$today=mktime($myarray[0], $myarray[1], $myarray[2], date("m")  , date("d"), date("Y"));
	                	if($daymonth<$dayofmonth){
	                	$nextrun  = mktime($myarray[0],$myarray[1], $myarray[2], date("n")  ,$dayofmonth, date("Y"));
	                	}
	                	else{
	                	$nextrun  = mktime($myarray[0],$myarray[1], $myarray[2], date("n")+1  ,$dayofmonth, date("Y"));	
	                	}
	               
	                }

	$query="insert into mk_reports_schedule (reportid,type,dateid,nextrun,optionstr,mailto) 
	values ('$reportid','$scheduletype','$scheduledate',$nextrun,'$optstring','$mailto') ";
	db_query($query);
    $smarty->assign("nextrun",date('d-m-Y H:i:s',$nextrun));
    
	$smarty->assign("commentSuccess","schedule");
    $smarty->assign("reportid","$reportid");
	func_display("schedule_report.tpl",$smarty);

}
else if($_GET["action"]=="edit"){
	$rid = $_GET["reportid"];
	$date=date("d",time())."-".date("M",time()).",".date("Y",time());
    $reportdetails=func_getreportquery($rid);
	$reportname=$reportdetails[0];
	$reportquery=$reportdetails[1];
	$sumquery=$reportdetails[2];
	$mailto=$reportdetails[3];
	$grp=$reportdetails[4];
	$reporttype=$reportdetails[5];
	$reportgroups=func_getreportgroups();
	$datatypes=func_getdatatypes();
	$optstring ="";
	for ($i=0;$i<count($datatypes);$i++){
		$optstring = $optstring."<option value=".$datatypes[$i]['id'].">".$datatypes[$i]['name']."</option>";
	}
	$reportquery=str_replace("<","LT",$reportquery);
    $reportquery=str_replace(">","GT",$reportquery);
   $sumquery=str_replace("<","LT",$sumquery);
   $sumquery=str_replace(">","GT",$sumquery);
   $filtersarray=func_get_filters($rid);
   $filters = array();
	for($j=0;$j<count($filtersarray);$j++){
		$filters[$j][0]=$filtersarray[$j]['name'];
		$filters[$j][1]=$filtersarray[$j]['dataname'];
		$filters[$j][2]=$filtersarray[$j]['default_value'];
		$filters[$j][3]=str_replace(" ","_",$filtersarray[$j]['name']);
		$filters[$j][4]=$filtersarray[$j]['id'];
	
	}
   
	$smarty->assign("optstring",$optstring);
	$smarty->assign("date", $date);
	$smarty->assign("reportid",$rid);
	$smarty->assign("filters",$filters);
	$smarty->assign("grp",$grp);
	$smarty->assign("reportname",$reportname);
	$smarty->assign("reporttype",$reporttype);
	$smarty->assign("query", $reportquery);
	$smarty->assign("sumquery", $sumquery);
	$smarty->assign("group", $reportgroups);
	$smarty->assign("mailto", $mailto);
	$smarty->assign("commentSuccess","Y");

	func_display("edit_report.tpl",$smarty);
}
else if ($_GET["action"]=="delete"){
	$rid = $_GET["reportid"];
	//print_r("inside delete");
	$delfilter="delete from `mk_reports_filters` where report_id='$rid' ";
	db_query($delfilter);
	$delrep="delete from `mk_reports` where id='$rid' ";
	db_query($delrep);
	
	$delschedule = "delete from `mk_reports_schedule` where reportid='$rid'";
	db_query($delschedule);
	
	$smarty->assign("commentSuccess","DEL");

	func_display("edit_report.tpl",$smarty);


}
else if ($_GET["action"]=="schedule"){
	$rid = $_GET["reportid"];
	$reportdetails=func_getreportquery($rid);
	$filters=func_get_filters($rid); 
	$chkflag=true;
	foreach ($filters as $filter) {
		if($filter['type']='DAT'){
			$chkflag=false;
		}
	}        
	$reportname=$reportdetails[0];
	$mailto=$reportdetails[3];
	$days= array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
	$daysofmonth= array(1 =>"1","2","3","3","5","6","7","8","9","10","11","12",
	"13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28");
	$reportschedules=func_getreportschedules();
	$scheduledate=func_getreportscheduledate();
	$smarty->assign("days",$days);
	$smarty->assign("daysofmonth",$daysofmonth);
	$smarty->assign("reportid",$rid);
	$smarty->assign("reportname",$reportname);
	$smarty->assign("mailto", $mailto);
	$smarty->assign("group", $reportschedules);
	if(!$chkflag){
	$smarty->assign("scheduledate", $scheduledate);
	}
	$smarty->assign("commentSuccess","Y");    
	func_display("schedule_report.tpl",$smarty);


}
else if($_GET["action"]=="viewschedules"){
    $rid = $_GET["reportid"];    
	$schedules=func_getallschedules($rid);
	$smarty->assign("comments",$schedules);
    $smarty->assign("reportid",$rid);
	func_display("view_report_schedules.tpl",$smarty);
	
	
}
else if($_GET["action"]=="delschedule"){
	$schid=$_GET["schid"];
	$delquery="delete from mk_reports_schedule where id='$schid'";
	db_query($delquery);
    $rid=$_GET["reportid"];
	$schedules=func_getallschedules($rid);
	$smarty->assign("comments",$schedules);
    if($rid != "")
    $smarty->assign("reportid",$rid);
	func_display("view_report_schedules.tpl",$smarty);
	
	
}
else if($_GET["action"]=="editschedule"){
	$schid=$_GET["schid"];
	$delquery="delete from mk_reports_schedule where id='$schid'";
	db_query($delquery);
	$schedules=func_getallschedules();
	$smarty->assign("comments",$schedules);
	func_display("view_report_schedules.tpl",$smarty);
	
	
}



?>


