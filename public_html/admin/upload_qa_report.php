<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
require_once($xcart_dir."/include/func/func.itemsearch.php");
require_once($xcart_dir."/include/func/func.mail.php");
require_once($xcart_dir."/include/func/func.order.php");

	header("Location:qa_actions.php");
	exit;

function qa_fail_procedure($itemid,$orderid,$reasoncode,$reason,$qa_associate)
{
    global $sql_tbl,$weblog;
    $sql = "UPDATE $sql_tbl[order_details] SET item_status='A' WHERE itemid='$itemid'";    
    db_query($sql);
    $qa_time=time();
    
    func_addComment_order($orderid, $qa_associate, 'QaFailure', $reasoncode, $reason);
    
    $weblog->info("item status change : $item_id QA fail");
}
function qa_pass_procedure($itemid,$orderid,$reasoncode,$qa_associate)
{
    global $sql_tbl,$weblog;
    $sql = "UPDATE $sql_tbl[order_details] SET item_status='QD' WHERE itemid='$itemid'";
    db_query($sql);
    func_addComment_order($orderid, $qa_associate, 'QA Pass', $reasoncode, 'QA Pass');
    $weblog->info("item status change : $item_id QA pass");
}

$action  = $HTTP_POST_VARS['action'];
$itemid  = $HTTP_POST_VARS['itemid'];
$orderid  = $HTTP_POST_VARS['orderid'];
$sort_by = 'itemid';
$sort_order = 'ASC';
//$associateName  = $HTTP_POST_VARS['associateName'];

$uploadStatus = false;
if($action == '')
	$action ='view';

if($sort_by == '')
	$sort_by ='orderid';

if($sort_order == '')
	$sort_order ='desc';

/*$assigneeQuery = "SELECT name,id from mk_assignee where name != \"\"";
$assigneeList=func_query($assigneeQuery);    
$smarty->assign("assigneeList",$assigneeList);*/

if($action == 'getData'){
    $smarty->assign("associateName",$associateName);    
    if($orderid == "" && $itemid == "") {
    	$smarty->assign("errorMsg","No value input for orderid / itemid.");
    	$action = "view";
    } else {
	    $sql=func_load_all_items_pending_QA($sort_by,$sort_order,$itemid,$orderid);
	    $sqllog->info("SQl to get Ops Done items - $sql");
	    $opsdoneItems = func_query($sql);
		$smarty->assign("opsdoneItems",$opsdoneItems);
		$smarty->assign("orderid",$orderid);
		$smarty->assign("itemid",$itemid);
	    $action = "showData";
    }
} else if ($REQUEST_METHOD == "POST" && $action=='qaUpdate') {
   	//Get each item checked and if the item is pass, mark it QAComplete - 'Q'
   	//If it fails, change it to assigned
   	$item_status = $HTTP_POST_VARS['qaResult'];
   	$item_check = $HTTP_POST_VARS['check'];
   	$item_id = $HTTP_POST_VARS['itemid'];
   	$orderid = $HTTP_POST_VARS['orderid'];
   	$is_customizable = $HTTP_POST_VARS['is_customizable'];
   	//$associateName  = $HTTP_POST_VARS['associateName'];
	$smarty->assign("orderid",$orderid);
	if($item_status == "pass"){
    	//foreach ($item_check AS $itemid=>$value){
      	//$sql = "UPDATE $sql_tbl[order_details] SET item_status='QD' WHERE itemid='$item_id'";
      	//db_query($sql);
       	qa_pass_procedure($item_id,$orderid,"item ".$item_id, $login);
      
      	//Check if order is QA done and if so get all itiems of the order.
		if($is_customizable =='0')
		{
			$sql = "SELECT order_details.is_customizable,order_details.itemid AS item_id,order_details.orderid as orderid,product_style.name AS item_name, order_details.amount AS item_count,xcart_products.image_portal_t as image
                   FROM {$sql_tbl['mk_product_style']} product_style INNER JOIN {$sql_tbl['order_details']} order_details join xcart_products
                   WHERE order_details.orderid='$orderid' AND product_style.id=order_details.product_style and order_details.productid=xcart_products.productid
                   and order_details.item_status='QD' order by order_details.itemid";
		}
		else
		{
      		$sql = "SELECT order_details.itemid AS item_id,order_details.orderid as orderid,product_style.name AS item_name, order_details.amount AS item_count,c.thumb_image as image
                   FROM {$sql_tbl['mk_product_style']} product_style INNER JOIN {$sql_tbl['order_details']} order_details INNER JOIN {$sql_tbl['mk_xcart_product_customized_area_rel']} c
                   WHERE order_details.orderid='$orderid' AND product_style.id=order_details.product_style and c.product_id=order_details.productid
                   and order_details.item_status='QD' order by order_details.itemid";
      	}
      	$rts_info=func_query($sql);
      	$smarty->assign("rts_info",$rts_info);
      	$weblog->info("item status change : $item_id QA pass");
	} else {
       //reassign
       $reject_reason  = $HTTP_POST_VARS['reject_reason'];
       
       qa_fail_procedure($item_id,$orderid,"item ".$item_id,$item_status." : ".$reject_reason, $login);

       $from ="MyntraAdmin";
       $loginEmail="alrt_qafailure@myntra.com";
       //$bcc="customerservice@myntra.com";
       $bcc="";
       $template = "QaFailure";
	   $args = array("ORDER_ID"	=>$orderid,
		              "ITEM_ID"	=>$item_id, 
					  "REASON"	=> $item_status.": ".$reject_reason,
                      "REPORTED_BY"	=> $associateName	);
       sendMessage($template, $args, $loginEmail,$from,$bcc);
	}

	$sql=func_load_all_items_pending_QA($sort_by,$sort_order, $item_id, $orderid);
    $opsdoneItems = func_query($sql);
	$smarty->assign("opsdoneItems", $opsdoneItems);
	
    $action = "showData";

}else if ($REQUEST_METHOD == "POST" && $action=='csvupload') {
	if(!isset($_FILES) && isset($HTTP_POST_FILES))
		$_FILES = $HTTP_POST_FILES;

    if(!empty($_FILES['qareport']['name']))
	{
    	$uploaddir1 = $xcart_dir.'/qareports/';
		$uploadfile1 = $uploaddir1 . date('d-m-Y-h-i-s-').$_FILES['qareport']['name'];
                              
		if (move_uploaded_file($_FILES['qareport']['tmp_name'], $uploadfile1)) {
			$uploadStatus = true;
		}

        //From file
	    if($uploadStatus)
		{
        	$msg="Item Order      Status <BR><BR>";
			// open the text file
			$fd = fopen ($uploadfile1, "r");
			$itemListDisplay =  array();
			$k = 0;
			while (!feof ($fd)) {
				$buffer = fgetcsv($fd, 4096);
				for ($i = 0; $i < 3; ++$i){
					if ($buffer[$i] == ""){
						$buffer[$i] = " ";
    				}
      				$itemListDisplay[$k][$i] = $buffer[$i];
				}
                    
           		$itemId=$itemListDisplay[$k][0];
           		$orderId=$itemListDisplay[$k][1];

				$msg=$msg.$itemId." ".$orderId." ".$itemListDisplay[$k][2]."<BR>";

            	$associateName  = $HTTP_POST_VARS['associateName'];
            	if($associateName == ""){
            		$associateName="Admin";
            	}
                
            	if($itemListDisplay[$k][2] == "P") {
					qa_pass_procedure($itemId,$orderId,"csv update","admin");
                } else{                      
                	qa_fail_procedure($itemid,$orderid,"csv update","Unknown Details","admin");
                }
                $k++;      
			}
			fclose ($fd);            
			
			$msg =  $msg." \nFile successfully uploaded";
                
            $action = "confirmUpload";
            $smarty->assign('message',$msg);
		} else { 
			 $msg =  "File not uploaded, try again ..";
             $action = "showData";
             $smarty->assign('message',$msg);
		}		
	}
}

$smarty->assign('action',$action);
$template ="upload_qa_report";
$smarty->assign("main",$template);
func_display("admin/home.tpl",$smarty);
?>
