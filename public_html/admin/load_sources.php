<?php
require './auth.php';

$s_location = $HTTP_GET_VARS['s_location'];
$s_type = $HTTP_GET_VARS['s_type'];

$sql = "select
			s.source_id,
			s.source_code,
			s.source_name,
			s.location_id,
			loc.location_name
		from
			{$sql_tbl['sources']} s
		left join 
			{$sql_tbl['source_locations']} loc
		on
			s.location_id = loc.location_id
		WHERE 
			s.source_type_id={$s_type}";

if(!empty($s_location))
	$sql .= " AND s.location_id = {$s_location}";
	
$order_sources = func_query($sql);

$select_box = '<select name="sourceid[]" size="4" style="width:200px;" multiple><option value="">--select source name/s--</option>';
foreach ($order_sources as $key=>$val){
	$select_box .= "<option value='{$val['source_id']}'>{$val['source_name']} ({$val['source_code']})</option>";
}
$select_box .= '</select>';
echo $select_box;
?>
