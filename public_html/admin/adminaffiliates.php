<?php
/*****************************************************************************\
 +-----------------------------------------------------------------------------+
 | X-Cart                                                                      |
 | Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
 | All rights reserved.                                                        |
 +-----------------------------------------------------------------------------+
 | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
 | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
 | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
 |                                                                             |
 | THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
 | THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
 | FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
 | AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
 | PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
 | CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
 | COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
 | (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
 | LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
 | AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
 | OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
 | AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
 | THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
 | THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
 |                                                                             |
 | The Initial Developer of the Original Code is Ruslan R. Fazliev             |
 | Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
 | Ruslan R. Fazliev. All Rights Reserved.                                     |
 +-----------------------------------------------------------------------------+
 \*****************************************************************************/

#
# $Id: categories.php,v 1.31 2006/02/14 14:45:23 max Exp $
#

require "./auth.php";
require $xcart_dir."/include/security.php";
require $xcart_dir."/include/func/func.adminaffiliates.php";
require_once $xcart_dir."/include/func/func.mail.php";

$location[] = array(func_get_langvar_by_name("lbl_affiliate"), "");


if (empty($mode)) $mode = "";

#
# Ajust category_location array
#
require "./location_ajust.php";

$template_used  = 'affiliatepayment';
$recipient_type ='Affiliate';


if ($REQUEST_METHOD == "POST") {

	if ($mode == "search") {
		$searchText = $HTTP_POST_VARS['affiliatecontact'];
		$aff_status = $HTTP_POST_VARS['aff_status'];
		$affiliates = func_getAffiliatesInfo($mode,$affiliateid,$searchText,$aff_status);
		$template_used = 'affiliatepayment';

	}
	#
	# Update affiliates status
	#
	elseif ($mode == "updatestatus") {
		if (is_array($posted_data)) {
			foreach ($posted_data as $affid=>$v) {
				if (empty($v["delete"]))
				continue;
				@header("location:affiliate_modify.php?affId=$affid&mode=modify");
				exi;

			}
			$top_message = array(
				"content" => "Affiliate successfully updated",
				"type" => ""
				);
				$smarty->assign("top_message",$top_message);

		}
		//$affiliates = func_getAffiliatesInfo($mode,$affiliateid,$searchText);
		//$template_used = 'affiliatepayment';
	}


	elseif ($mode == "update") {

		#
		# update affiliates
		#


		if (is_array($posted_data)) {
			foreach ($posted_data as $id=>$v) {
				if(!empty($v['delete'])){
					$affiliateid = $id;
				}
			}
		}

		if(!empty($affiliateid)){
			$affiliates = func_getAffiliatesInfo($mode,$affiliateid);

			/* show pending commission by default */
			$commStatus = '1011';
			$comm_details = getAffiliateCommDetail($commStatus,$affiliateid);
			$smarty->assign("comm_details",$comm_details);
			$smarty->assign("status",$commStatus);
			/* end */
				
			$SQL= "SELECT paymentid,payment_method FROM $sql_tbl[payment_methods]";
			$payment_methods = func_query ($SQL);
			$smarty->assign("affiliateid",$affiliateid);
			$comm_history = getAffiliatePaymentHistory($affiliateid,$recipient_type);
		}else{
			func_header_location("adminaffiliates.php");
				
		}
		$template_used = 'affiliate_comm_update';
	  

	}elseif ($_POST['mode_report'] == "updatePending") {

		#
		# update designer values
		#
		$mode='update';
		$affiliateid=$_POST['designid'];
		if(!empty($affiliateid)){
			$affiliates = func_getAffiliatesInfo($mode,$affiliateid);

			/* show pending commission by default */
			$commStatus = '1011';
			$comm_details = getAffiliateCommDetail($commStatus,$affiliateid);
			$smarty->assign("comm_details",$comm_details);
			$smarty->assign("status",$commStatus);
			/* end */
				
			$SQL= "SELECT paymentid,payment_method FROM $sql_tbl[payment_methods]";
			$payment_methods = func_query ($SQL);
			$smarty->assign("affiliateid",$affiliateid);
			$comm_history = getAffiliatePaymentHistory($affiliateid,$recipient_type);
		}else{
			func_header_location("adminaffiliates.php");
				
		}
		$template_used = 'affiliate_comm_update';
	  

	} elseif ($mode == "save") {
			
		#
		# save designer payment details
		#
		$template_used = 'affiliate_comm_update';
			
		$sql_tbl[mk_designer_payment_commission] =  "mk_designer_payment_commission";
		if(!empty($payamt))
		{
			$query_data = array(
							"status" => '1006'
							);
							$time =time();
							$comments = mysql_escape_string($comments);
							#
							# save designer commision details
							#
								
							if (is_array($posted_data)) {
								foreach ($posted_data as $id=>$v) {
									if(!empty($v['delete'])){
										$rowid = $id;
										func_array2update("mk_affiliate_statistics", $query_data, "id='$rowid'");
										 
									}
								}
							}
							$SQL = "INSERT INTO $sql_tbl[mk_designer_payment_commission](recipient_id,payment_amt,pay_method,comments,payment_status,payment_date,recipient_type)
						VALUES('".$affiliateid."','".$payamt."','".$pay_methods."','".$comments."','1006','".$time."','".$recipient_type."') " ;
							db_query($SQL);



							$affiliates = func_getAffiliatesInfo($mode,$affiliateid);
							$comm_history = getAffiliatePaymentHistory($affiliateid,$recipient_type);
							$smarty->assign("affiliateid",$affiliateid);

							$affiliateName =  $affiliates[0]['contact_name'];
							$affiliateEmail =  $affiliates[0]['contact_email'];

							$template = "affiliatecommission";
							$args = array( "FIRST_NAME" =>$affiliateName,
				               "PAYMENT_AMOUNT" => $payamt,
				               "COMMENTS" => $comments
							);
							sendMessage($template, $args,$affiliateEmail);

							$top_message["content"] = "Commission Payment has been made successfully.";
		}
		 
	}
	elseif($mode == "show" || $mode="payment")
	{
		$chkArray = array();
		$totalComm = '0';
			
		if($mode == 'payment')
		{
			if (is_array($posted_data)) {
				foreach ($posted_data as $id=>$v)
				{
					if(!empty($v['delete']))
					{
						$chkArray[] = $id;
						$totalComm = floatval($totalComm) + floatval($v['comm']) ;
					}
					else
					{
						$chkArray[] ="";
					}
				}
			}
			$smarty->assign("chkArr",$chkArray);
				
		}
			
		$comm_details = getAffiliateCommDetail($_POST['commStatus'],$affiliateid);
		$smarty->assign("comm_details",$comm_details);
		$smarty->assign("status",$_POST['commStatus']);
		$smarty->assign("totalcomm",$totalComm);
		$template_used = 'affiliate_comm_update';
		$affiliates = func_getAffiliatesInfo($mode,$affiliateid);
		$SQL= "SELECT paymentid,payment_method FROM $sql_tbl[payment_methods]";
		$payment_methods = func_query ($SQL);
		$smarty->assign("affiliateid",$affiliateid);
		$comm_history = getAffiliatePaymentHistory($affiliateid,$recipient_type);
			
			
	}

}
else
{
	$affiliates = func_getAffiliatesInfo($mode,$affiliateid,$searchText);
	$template_used = 'affiliatepayment';
}
if(isset($_GET['designer']))
{
	$mode1 ="update";
	$affiliateid = getAffiliateID($_GET['designer']);
	$id = $affiliateid[0][id];
	$affiliates = func_getAffiliatesInfo($mode1,$id);
}

$pending_com_report=getPendingComm_AffiliatesReport();

$smarty->assign("designers",$pending_com_report);
$smarty->assign("mode",$mode);

$smarty->assign("affiliates",$affiliates);
$smarty->assign("pay_methods",$payment_methods);
$smarty->assign("searchText",$searchText);
$smarty->assign("selectedStatus",$aff_status);

$smarty->assign("comm_history",$comm_history);
$smarty->assign("main",$template_used);

# Assign the current location line
$smarty->assign("location", $location);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
