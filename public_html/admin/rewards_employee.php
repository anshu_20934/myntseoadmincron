<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
require $xcart_dir."/include/func/func.RRRedeemPoints.php";
$action  = $HTTP_POST_VARS['action'];
$uploadStatus = false;

function splitName($name,&$firstname,&$lastname){
	$firstname=" ";$lastname=" ";
	//$array=split(" ",$name);//preg_split takes care of blank spaces at the end
	$array=preg_split("/ /",$name,-1,PREG_SPLIT_NO_EMPTY);
	$size=count($array);
	if($size<1) return;
	$firstname=$array[0];
	if($size>1){
		$lastname=$array[$size-1];
		for($i=1;$i<($size-1);$i++)
		$firstname=$firstname." ".$array[$i];
	}
	return;
}

if($action == '')
    $action ='upload';
if ($REQUEST_METHOD == "POST" && $action=='verify') {
       if(!isset($_FILES) && isset($HTTP_POST_FILES))
        $_FILES = $HTTP_POST_FILES;

        if(!empty($_FILES['orderdetail']['name']))
        {
                    $uploaddir1 = '../bulkorderupdates/';
                    $extension = explode(".",basename($_FILES['orderdetail']['name']));
                    $uploadfile1 = $uploaddir1 . date('d-m-Y-h-i-s').".".$extension[1];
                    if (move_uploaded_file($_FILES['orderdetail']['tmp_name'], $uploadfile1)) {
                             $uploadStatus = true;
                    }
        }

        if($uploadStatus)
        {
                // open the text file
            $fd = fopen ($uploadfile1, "r");
                $orderListDisplay =  array();
                $k = 0;
                while (!feof ($fd)) {
                        $buffer = fgetcsv($fd, 9096,',','\'');
                        for ($i = 0; $i < 17; ++$i){
                             if ($buffer[$i] == ""){
                                $buffer[$i] = " ";
                            }
                           $orderListDisplay[$k][$i] = $buffer[$i];
                    }
                    $k++;

                }
                fclose ($fd);
                $smarty->assign('orderListDisplay',$orderListDisplay);
                $smarty->assign('uploadorderFile',$uploadfile1);
        }
        else
        {
             $action = "upload";
             $msg =  "File not uploaded, try again ..";
        }
        $smarty->assign('message',$msg);

}
########## Confirm the order     ##############3
if ($REQUEST_METHOD == "POST" && $action=='confirmorder') {
        
        $filetoread  = $HTTP_POST_VARS['filetoread'];
        if(file_exists($filetoread))
        {
            # get list of valid courier codes

            $emp_data=array();
			$fd = fopen ($filetoread, "r");
			$i=0;
            while (!feof ($fd)) {
                $buffer = fgetcsv($fd, 9096,',','\'');

				if($buffer[0]=="")continue;
				//function CreateNewCNWEmployee($id,$firstname,$lastname,$team,$level,$email,$managerId,$mobile,$phone,$address,$city,$state,$zipcode,$country,$peer_account,$icr_account,$manager_account){
				$j=0;
				$emp_data[$i][employee_id]=$buffer[$j++];
				$firstname="";$lastname="";
				splitName($buffer[$j++],$firstname,$lastname);
                $emp_data[$i][firstname]=$firstname;
                $emp_data[$i][lastname]=$lastname;
                $emp_data[$i][dept]=$buffer[$j++];
				$emp_data[$i][role]=$buffer[$j++];
				$status=$buffer[$j++];
				if($status == "Resigned")$status="R";
				else $status="A";
                $emp_data[$i][status]=$status;
                $emp_data[$i][manager_id]=$buffer[$j++];
                $emp_data[$i][address]=$buffer[$j++];
                $emp_data[$i][city]=$buffer[$j++];
                $emp_data[$i][state]=$buffer[$j++];
                $emp_data[$i][zipcode]=$buffer[$j++];
                $emp_data[$i][country]=$buffer[$j++];
                $emp_data[$i][email]=$buffer[$j++];
                $emp_data[$i][mobile]=$buffer[$j++];
                $emp_data[$i][phone]=$buffer[$j++];
                $emp_data[$i][personal_account]=$buffer[$j++];
                $emp_data[$i][handshake_account]=$buffer[$j++];
				$emp_data[$i][prodigy_account]=$buffer[$j++];


				$i++;
			}
			$result="";
			$p=0;$h=0;
			$prodigies=array();
			$handshake=array();
			foreach($emp_data as $key=>$emp){                           

				$status=CreateNewCNWEmployee($emp[employee_id],$emp[firstname],$emp[lastname],$emp[dept],$emp[role],$emp[status],$emp[email],$emp[manager_id],$emp[mobile],$emp[phone],$emp[address],$emp[city],$emp[state],$emp[zipcode],$emp[country],$emp[handshake_account],$emp[personal_account],$emp[prodigy_account]);				
				$result.="<BR>".$emp[email]."  ".$status;				

				if($status == "success"){	
					if($emp[role]=='LM' && $emp[prodigy_account]=='Y')
						$prodigies[$p++]=$emp[email];
					if( $emp[handshake_account]=='Y')
						$handshake[$h++]=$emp[email];
				}
                             
			}

			foreach($prodigies as $prodigy)
				AutoPopulateAccount($prodigy,'MANAGER');
			foreach($handshake as $account)
				AutoPopulateAccount($account,'PEER');

			$result.="<BR>";
            fclose ($fd);
            $action = "upload";
            $smarty->assign('message',$result);
        }
        else
        {
               $msg = "File does not exist";
               $smarty->assign('message',$msg);
        }
}

$smarty->assign('action',$action);
$template ="rewards_employee";
$smarty->assign("main",$template);
func_display("admin/home.tpl",$smarty);


?>
