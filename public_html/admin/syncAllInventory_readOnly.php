<?php
//ini_set('memory_limit','-1');

$xcart_dir = "/home/myntradomain/public_html";
include_once("oms2atp.php");
include_once("oms2wms.php");
include_once("oms2imsfk.php");
include_once("updateInventory.php");

$wmsMoreBocQueries = array();
$wmsLessBocQueries = array();

$imsFKMoreBocQueries = array();
$imsFKLessBocQueries = array();

$oosSkuQueries = array();
$undersellSkuQueries = array();
$unAvlblUnRlsdSkuQueries = array();
$avlblUnrlsdSkuQueries = array();
$allSkusSellers = array();

$tmpWmsMoreBocQueries = array();
$tmpWmsLessBocQueries = array();
$tmpWhLvlWmsMoreBocQueries = array();
$tmpWhLvlWmsLessBocQueries = array();

$tmpImsFKMoreBocQueries = array();
$tmpImsFKLessBocQueries = array();
$tmpWhLvlImsFKMoreBocQueries = array();
$tmpWhLvlImsFKLessBocQueries = array();

$tmpOosSkuQueries = array();
$tmpUndersellSkuQueries = array();
$tmpUnAvlblUnRlsdSkuQueries = array();
$tmpAvlblUnrlsdSkuQueries = array();
$tmpAllSkusSellers = array();

$message = "";
$message = $message."\n%%%%%% Syncing ERP inventory started on ".date('j-m-Y H:i:s')." %%%%%%\n";
$to = "engg_erp_wms@myntra.com,engg_erp_oms@myntra.com";

$fromHeader = "From: inventorysync@myntra.com";
$subject = "[OMSALERT][WMSALERT][READ-ONLY] ERP Inventory: keep calm and sync on!";

$myntraSellerIds = "1,19,21,25,29";
$orderEnabledWhIds="28,36,89,93,81,118,213";
$extSellerIds= "19,32";
$activeWarehouses = explode(",", $orderEnabledWhIds);

$authHeader = "Authorization: Basic YXBpYWRtaW46bTFudHJhUjBja2V0MTMhIw==";
$appPropBaseUrl = "http://erptools.myntra.com/myntra-tools-service/platform/tools/properties/search/?q=name.eq:";


$appPropSellerUrl = $appPropBaseUrl."wms.myntra.inventory.myntraSeller.ids";
$output = shell_exec("curl -v -H '" . $authHeader . "' -H 'Content-Type:application/xml' -XGET '" . $appPropSellerUrl . "' -d '" . $data . "'");
$xml = simplexml_load_string($output);
if ($xml->status->statusType == "SUCCESS")
    $myntraSellerIds = $xml->data->applicationProperty->value;


$appPropWhUrl = $appPropBaseUrl."wms.myntra.inventory.warehouse.ids";
$output = shell_exec("curl -v -H '" . $authHeader . "' -H 'Content-Type:application/xml' -XGET '" . $appPropWhUrl . "' -d '" . $data . "'");
$xml = simplexml_load_string($output);
if ($xml->status->statusType == "SUCCESS"){
    $orderEnabledWhIds = $xml->data->applicationProperty->value;
    $activeWarehouses = array();
    $activeWarehouses = explode(",", $orderEnabledWhIds);
}


$appPropExtSellerUrl = $appPropBaseUrl."wms.myntra.inventory.activeExtStoreSeller.id";
$output = shell_exec("curl -v -H '" . $authHeader . "' -H 'Content-Type:application/xml' -XGET '" . $appPropExtSellerUrl . "' -d '" . $data . "'");
$xml = simplexml_load_string($output);
if ($xml->status->statusType == "SUCCESS")
    $extSellerIds = $xml->data->applicationProperty->value;


for ($i =0; $i < 3; $i++){
    echo "starting\n";
    foreach ($activeWarehouses as $whId) {
	getOms2WmsBoc($whId,$tmpWhLvlWmsMoreBocQueries,$tmpWhLvlWmsLessBocQueries,$myntraSellerIds);
        $tmpWmsMoreBocQueries = array_merge($tmpWmsMoreBocQueries,$tmpWhLvlWmsMoreBocQueries);
        $tmpWmsLessBocQueries = array_merge($tmpWmsLessBocQueries,$tmpWhLvlWmsLessBocQueries);
        $tmpWhLvlWmsMoreBocQueries = array();
        $tmpWhLvlWmsLessBocQueries = array();
    }
    /*
    echo "for i = ".$i."\n";
    echo "tmpMoreCount: ".count($tmpWmsMoreBocQueries)."\n";
    echo "tmpLessCount: ".count($tmpWmsLessBocQueries)."\n";
    */
    if ($i == 0){
        $wmsMoreBocQueries = $tmpWmsMoreBocQueries;
        $wmsLessBocQueries = $tmpWmsLessBocQueries;
    }
    else{
        $wmsMoreBocQueries = array_intersect($wmsMoreBocQueries,$tmpWmsMoreBocQueries);
        $wmsLessBocQueries = array_intersect($wmsLessBocQueries,$tmpWmsLessBocQueries);
    }
    $tmpWmsMoreBocQueries = array();
    $tmpWmsLessBocQueries = array();
    echo "for i = ".$i."\n";
    echo "moreCount: ".count($wmsMoreBocQueries)."\n";
    echo "lessCount: ".count($wmsLessBocQueries)."\n";


    echo "starting ims fk\n";
    foreach ($activeWarehouses as $whId) {
        getOms2ImsFKBoc($whId,$tmpWhLvlImsFKMoreBocQueries,$tmpWhLvlImsFKLessBocQueries,$extSellerIds);
        $tmpImsFKMoreBocQueries = array_merge($tmpImsFKMoreBocQueries,$tmpWhLvlImsFKMoreBocQueries);
        $tmpImsFKLessBocQueries = array_merge($tmpImsFKLessBocQueries,$tmpWhLvlImsFKLessBocQueries);
        $tmpWhLvlImsFKMoreBocQueries = array();
        $tmpWhLvlImsFKLessBocQueries = array();

    }
    /*
    echo "for i = ".$i."\n";
    echo "tmpMoreCount: ".count($tmpImsFKMoreBocQueries)."\n";
    echo "tmpLessCount: ".count($tmpImsFKLessBocQueries)."\n";
    */
    if ($i == 0){
        $imsFKMoreBocQueries = $tmpImsFKMoreBocQueries;
        $imsFKLessBocQueries = $tmpImsFKLessBocQueries;
    }
    else{
        $imsFKMoreBocQueries = array_intersect($imsFKMoreBocQueries,$tmpImsFKMoreBocQueries);
        $imsFKLessBocQueries = array_intersect($imsFKLessBocQueries,$tmpImsFKLessBocQueries);
    }
    $tmpImsFKMoreBocQueries = array();
    $tmpImsFKLessBocQueries = array();
    echo "for i = ".$i."\n";
    echo "moreCount: ".count($imsFKMoreBocQueries)."\n";
    echo "lessCount: ".count($imsFKLessBocQueries)."\n";



    getOms2AtpBoc($tmpAllSkusSellers,$tmpOosSkuQueries,$tmpUndersellSkuQueries,$tmpUnAvlblUnRlsdSkuQueries,$tmpAvlblUnrlsdSkuQueries,$myntraSellerIds);
    /*
    echo "for i = ".$i."\n";
    echo "tmpAllSkus count: ".count($tmpAllSkusSellers)." \n";
    echo "tmpOosSkus count: ".count($tmpOosSkuQueries)." \n";
    echo "tmpUndersellSkus count: ".count($tmpUndersellSkuQueries)." \n";
    echo "tmpUnAvlblUnRlsdSkus count: ".count($tmpUnAvlblUnRlsdSkuQueries)." \n";
    echo "tmpAvlblUnrlsdSkus count: ".count($tmpAvlblUnrlsdSkuQueries)." \n";
    */
    if ($i == 0){
        $allSkusSellers = $tmpAllSkusSellers;
        $oosSkuQueries = $tmpOosSkuQueries;
        $undersellSkuQueries = $tmpUndersellSkuQueries;
        $unAvlblUnRlsdSkuQueries = $tmpUnAvlblUnRlsdSkuQueries;
        $avlblUnrlsdSkuQueries = $tmpAvlblUnrlsdSkuQueries;
    }
    else{
        $allSkusSellers = array_intersect($allSkusSellers,$tmpAllSkusSellers);
        $oosSkuQueries = array_intersect($oosSkuQueries,$tmpOosSkuQueries);
        $undersellSkuQueries = array_intersect($undersellSkuQueries,$tmpUndersellSkuQueries);
        $unAvlblUnRlsdSkuQueries = array_intersect($unAvlblUnRlsdSkuQueries,$tmpUnAvlblUnRlsdSkuQueries);
        $avlblUnrlsdSkuQueries = array_intersect($avlblUnrlsdSkuQueries,$tmpAvlblUnrlsdSkuQueries);
    }
    $tmpAllSkusSellers = array();
    $tmpOosSkuQueries = array();
    $tmpUndersellSkuQueries = array();
    $tmpUnAvlblUnRlsdSkuQueries = array();
    $tmpAvlblUnrlsdSkuQueries = array();
    echo "for i = ".$i."\n";
    echo "allSkusSellers count: ".count($allSkusSellers)." \n";
    echo "oosSkus count: ".count($oosSkuQueries)." \n";
    echo "undersellSkus count: ".count($undersellSkuQueries)." \n";
    echo "unAvlblUnRlsdSkus count: ".count($unAvlblUnRlsdSkuQueries)." \n";
    echo "avlblUnrlsdSkus count: ".count($avlblUnrlsdSkuQueries)." \n";

    if ($i != 2){
        echo "sleeping\n";
        sleep(10);
    }
}
echo "updating inventory\n";
$errQueries="";
#updateInventory($wmsMoreBocQueries,$wmsLessBocQueries,$imsFKMoreBocQueries,$imsFKLessBocQueries,$allSkusSellers,$oosSkuQueries,$undersellSkuQueries,$unAvlblUnRlsdSkuQueries,$avlblUnrlsdSkuQueries,$errQueries, true);

$message = $message."IMS overselling SKUs count: ".count($wmsLessBocQueries)."\n";
$message = $message."IMS underselling SKUs count: ".count($wmsMoreBocQueries)."\n";
$message = $message."IMS FK overselling SKUs count: ".count($imsFKLessBocQueries)."\n";
$message = $message."IMS FK underselling SKUs count: ".count($imsFKMoreBocQueries)."\n";
$message = $message."ATP overselling SKUs count: ".count($oosSkuQueries)."\n";
$message = $message."ATP underselling SKUs count: ".count($undersellSkuQueries)."\n";
$message = $message."ATP available unreleased SKUs count: ".count($avlblUnrlsdSkuQueries)."\n";
$message = $message."ATP 0-inventory unreleased SKUs count: ".count($unAvlblUnRlsdSkuQueries)."\n";
$message = $message."\n\n###############[SKU:- SELLER]#############\n\n";
$message = $message."#########################################\n\n";
$message = $message."WMS overselling w:r:to OMS :--";
$message = $message."\n";
$message = $message.extractSkusFromQuery($wmsLessBocQueries,false);
$message = $message."\n"."#########################################\n\n";
$message = $message."WMS underselling w:r:to OMS :--";
$message = $message."\n";
$message = $message.extractSkusFromQuery($wmsMoreBocQueries,false);
$message = $message."\n"."#########################################\n\n";
$message = $message."IMS FK overselling w:r:to OMS :--";
$message = $message."\n";
$message = $message.extractSkusFromQuery($imsFKLessBocQueries,false);
$message = $message."\n"."#########################################\n\n";
$message = $message."IMS FK underselling w:r:to OMS :--";
$message = $message."\n";
$message = $message.extractSkusFromQuery($imsFKMoreBocQueries,false);
$message = $message."\n"."#########################################\n\n";
$message = $message."ATP overselling w:r:to OMS :--";
$message = $message."\n";
$message = $message.extractSkusFromQuery($oosSkuQueries,true);
$message = $message."\n"."#########################################\n\n";
$message = $message."ATP underselling w:r:to OMS :--";
$message = $message."\n";
$message = $message.extractSkusFromQuery($undersellSkuQueries,true);
$message = $message."\n"."#########################################\n\n";
$message = $message."ATP available unreleased SKUs w:r:to OMS :--";
$message = $message."\n";
$message = $message.extractSkusFromQuery($avlblUnrlsdSkuQueries,true);
$message = $message."\n"."#########################################\n\n";
$message = $message."ATP 0-inventory unreleased SKUs w:r:to OMS :--";
$message = $message."\n";
$message = $message.extractSkusFromQuery($unAvlblUnRlsdSkuQueries,true);
$message = $message."\n"."#########################################\n\n";
if (strlen($errQueries) > 0){
    $message = $message."Updates that failed to execute :--\n";
    $message = $message.$errQueries;
}

$wms2AtpInvSyncData = file_get_contents("/var/log/myntra/syncWMSInventory.log");
unlink("/var/log/myntra/syncWMSInventory.log");
//$wms2AtpInvSyncData = file_get_contents("syncWMSInventory.log");
//unlink("syncWMSInventory.log");
$wms2AtpBocSyncData = file_get_contents("/var/log/myntra/syncWMSATPBOC.log");
unlink("/var/log/myntra/syncWMSATPBOC.log");
//$wms2AtpBocSyncData = file_get_contents("syncWMSATPBOC.log");
//unlink("syncWMSATPBOC.log");
$message = $message."\n"."#########################################\n\n";
$message = $message."SKUs whose net inventory were synced from WMS :--\n";
$message = $message.$wms2AtpInvSyncData;
$message = $message."\n"."#########################################\n\n";
$message = $message."SKUs whose BoC were synced from WMS :--\n";
$message = $message.$wms2AtpBocSyncData;

$message = $message."\n\n%%%%%% Syncing ERP inventory completed on ".date('j-m-Y H:i:s')." %%%%%%";

//echo $message;
echo "error Queries:\n".$errQueries."\n";
//echo "Message:". $message;
mail($to,$subject,$message,$fromHeader);

$alertThreshold = 300;

$wmsDiscrepencyCount = count($wmsLessBocQueries) + count($wmsMoreBocQueries);
if($wmsDiscrepencyCount > $alertThreshold) {
    sendMailForCriticalAlert($wmsDiscrepencyCount);
}

function sendMailForCriticalAlert($discrepancyCount) {
    $message = "Discrepancy count is more than threshold. Circuit breaker has kicked! Please check and ensure data is fixed! \n You may check 'keep calm and sync on' mail to get more details..'";
    $from = "From: wmsoncallalert@myntra.com";
    $to = "engg_erp_wms@myntra.com, engg_erp_oms@myntra.com, wmsoncall@myntra.com, omsoncall@myntra.com, noc@myntra.com, myntra.sms.alerts@gmail.com";
    $subject = "[***CRITICAL***] Discrepency b/w OMS and IMS BOC Count. Circuit breaker has kicked! Check and fix! DiscrepancyCount:".$discrepancyCount;
    mail($to,$subject,$message,$from);
}


function extractSkusFromQuery($queries,$isSuffix){
    $skuString="\n\n";
    foreach ($queries as $query){
        // echo "Query:". $query."\n\n";
        $startIndx = strpos($query,"sku_id = ");
        if ($startIndx !== false){
            $endIndx = strpos($query," and ",$startIndx);
            $skuString = $skuString.substr($query,$startIndx+9,$endIndx-$startIndx-9);

        }
        $skuString=$skuString."-> ";
        $startIndx = strpos($query,"seller_id = ");
        if ($startIndx !== false){
            $endIndx = strpos($query," and ",$startIndx);
            $skuString = $skuString.substr($query,$startIndx+12,$endIndx-$startIndx-12)."\n";

        }

    }
    return $skuString;
}
?>

