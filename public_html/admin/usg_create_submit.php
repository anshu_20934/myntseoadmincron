<?php
require_once  "auth.php";
require_once $xcart_dir.'/modules/RestAPI/RestRequest.php';

$emailIds = array();

if(!isset($_FILES) && isset($HTTP_POST_FILES))
{
	$_FILES = $HTTP_POST_FILES;
	if(!empty($_FILES['group_csv']['name'])){
		$jsonArr = array();
		$jsonArr['UserGroup'] = array();
		$jsonArr['UserGroup']['description'] = $_REQUEST['group_desc'];
		$jsonArr['UserGroup']['title'] = $_REQUEST['group_name'];
		$jsonArr['UserGroup']['type'] = $_REQUEST['group_type'];
		$jsonArr['UserGroup']['userGroupMapList'] = array();
		$i = 0;
		$handle = fopen($_FILES['group_csv']['tmp_name'],"r");
		if($handle){
            while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
                    foreach ($data as $key => $value) {
                            if(!empty($value)){
                            		$value = trim($value);
                                    $jsonArr['UserGroup']['userGroupMapList'][$i++]['userId'] = $value;
                                    array_push($emailIds, $value);
                            }
                    }
            }
            fclose($handle);
        }

	}
}

$url = HostConfig::$userGroupServiceUrl.'/';
$method = 'POST';
$data = json_encode($jsonArr);
$request = new RestRequest($url, $method, $data);
$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Content-type: application/json';
$headers[] = 'Accept-Language: en-US';
$request->setHttpHeaders($headers);
$request->execute();
$info = $request->getResponseInfo();
$body = $request->getResponseBody();
if($info['http_code'] == 200){
	$response = json_decode($body,true);
	if($response['groupResponse']['status']['statusType'] == 'SUCCESS'){
		$XCART_SESSION_VARS['createUserGroup'] = true;
		$XCART_SESSION_VARS['groupId'] = $response['groupResponse']['data']['UserGroup']['id'];
		bulkUpdateGroups($emailIds,$response['groupResponse']['data']['UserGroup']['id']);
	}
}

function bulkUpdateGroups($userIds,$groupId){
	$url = HostConfig::$notificationsUpsUrl.'/putAttr';
	$method = 'POST';
	$data = getBulkIncrementJSON($userIds,$groupId);
	$request = new RestRequest($url, $method, $data);
	$headers = array();
	$headers[] = 'Accept: application/json';
	$headers[] = 'Content-type: application/json';
	$headers[] = 'Accept-Language: en-US';
	$request->setHttpHeaders($headers);
	$request->execute();
	$info = $request->getResponseInfo();
	if($info['http_code'] == 200){
		return true;
	}
	return false;
}

function getBulkIncrementJSON($userIds,$groupId){
	$jsonArr = array();
	for($i=0; $i<count($userIds); $i++){
		$jsonArr[$userIds[$i]] = array();
		$jsonArr[$userIds[$i]]['attribute'] = "usergroupid";
		$jsonArr[$userIds[$i]]['sourceid'] = "4";
		$jsonArr[$userIds[$i]]['value'] = "$groupId";
		$jsonArr[$userIds[$i]]['doAppend'] = "1";
	}
	return json_encode($jsonArr);
}

header('Location: /admin/user_group_create.php');
?>
