<?php
/**
 * Created by Lohiya.
 */
chdir(dirname(__FILE__));
require_once "./auth.php";
require_once($xcart_dir."/include/security.php");
require_once($xcart_dir."/PHPExcel/PHPExcel.php");
require_once($xcart_dir."/include/SimpleImage.php");
include_once($xcart_dir."/include/solr/solrProducts.php");
include_once($xcart_dir."/include/solr/solrUpdate.php");
include_once($xcart_dir."/modules/discount/DiscountEngine.php");
include_once($xcart_dir."/include/func/func_sku.php");
include_once($xcart_dir."/include/func/func.files.php");
include_once($xcart_dir."/include/func/func.utilities.php");
include_once($xcart_dir."/include/class/style/class.MyntraStyle.php");
require_once($xcart_dir."/include/func/func.displayCategory.php");
require_once($xcart_dir."/admin/size_unification_admin_new_function.php");
include_once "$xcart_dir/modules/apiclient/AuditApiClient.php";
include_once "$xcart_dir/include/func/func.image.php";
include_once "$xcart_dir/include/func/funcStyleProperties.php";
include_once ("$xcart_dir/modules/styleService/PortalRequestHandlerClient.php");

include_once "$xcart_dir/utils/imagetransferS3.php";
$action  = $HTTP_POST_VARS['mode'];
global $weblog;

$p_upload_dir='../images/style/properties/';
$upload_dir_db="images/style/properties/";
$s_upload_dir='../images/style/sizechartimages/';
$s_upload_dir_db="images/style/sizechartimages/";
$s_cdn_upload_dir_db="$cdn_base/images/style/sizechartimages/";

$applySizeScaleRestriction = FeatureGateKeyValuePairs::getBoolean('applySizeScaleRestriction');
$smarty->assign('applySizeScaleRestriction', $applySizeScaleRestriction);

$imageUploadMaxSize = FeatureGateKeyValuePairs::getInteger('styleImageUploadMaxSize', 300);

if($REQUEST_METHOD == "POST" && $action=='upload')
{
	if(!isset($_FILES) && isset($HTTP_POST_FILES))
	{
		$_FILES = $HTTP_POST_FILES;
	}

	$file_extn = '';
	$uniqueId = uniqid();
	$smarty->assign('progresskey', $uniqueId);

	if(!empty($_FILES['skusfile']['name']))
	{
		$objReader = PHPExcel_IOFactory::createReader('Excel5');//use xls file, use excel2007 for 2007 format
		$objPHPExcel1 = $objReader->load($_FILES['skusfile']['tmp_name']);
		$style_sheet=$objPHPExcel1->getSheetByName('Sheet1');
		$lastRow = $style_sheet->getHighestRow();
		$upload_styles = array();
		$article_numbers_check = array();
		$sku_code_check = array();
		for($i=2; $i<=$lastRow; $i++)
		{
			$upload_style['validated_error']='';
			$upload_style= array();
			$style_id=(string)trim($style_sheet->getCell('A'.$i)->getValue());
			$vendor_article_number=(string)trim($style_sheet->getCell('B'.$i)->getValue());
			$vendor_article_name=(string)trim($style_sheet->getCell('C'.$i)->getValue());
			$status=(string)trim($style_sheet->getCell('D'.$i)->getValue());
			$article_type=(string)trim($style_sheet->getCell('E'.$i)->getValue());
			$brand=(string)trim($style_sheet->getCell('F'.$i)->getValue());
			$age_group=(string)trim($style_sheet->getCell('G'.$i)->getValue());
			$gender=(string)trim($style_sheet->getCell('H'.$i)->getValue());
			$base_color=(string)trim($style_sheet->getCell('I'.$i)->getValue());
			$color1=(string)trim($style_sheet->getCell('J'.$i)->getValue());
			$color2=(string)trim($style_sheet->getCell('K'.$i)->getValue());
			$fashion_type=(string)trim($style_sheet->getCell('L'.$i)->getValue());
			$usage=(string)trim($style_sheet->getCell('M'.$i)->getValue());;
			$year=(string)trim($style_sheet->getCell('N'.$i)->getValue());
			
			if($applySizeScaleRestriction)
			{
				$size_chart_scale = (string)trim($style_sheet->getCell('O'.$i)->getValue());
				$size_skus = (string)trim($style_sheet->getCell('P'.$i)->getValue());
				$mrp=(string)trim($style_sheet->getCell('Q'.$i)->getValue());
				$description=$style_sheet->getCell('R'.$i)->getValue();
				$styleNote = $style_sheet->getCell('S'.$i)->getValue();
				$materialsCareDesc = $style_sheet->getCell('T'.$i)->getValue();
				$sizeFitDesc = $style_sheet->getCell('U'.$i)->getValue();
				$size_chart_path=(string)trim($style_sheet->getCell('V'.$i)->getValue());
				$product_display_name=(string)trim($style_sheet->getCell('W'.$i)->getValue());
				$tags=(string)trim($style_sheet->getCell('X'.$i)->getValue());
				$product_type=(string)trim($style_sheet->getCell('Y'.$i)->getValue());
				$classification_brand=(string)trim($style_sheet->getCell('Z'.$i)->getValue());
				$season_attr=(string)trim($style_sheet->getCell('AA'.$i)->getValue());
				$comments = (string)trim($style_sheet->getCell('AB'.$i)->getValue());
				$uploadImage = (string)trim($style_sheet->getCell('AC'.$i)->getValue());
			}
			else
			{
				$size_skus = (string)trim($style_sheet->getCell('O'.$i)->getValue());
				$mrp=(string)trim($style_sheet->getCell('P'.$i)->getValue());
				$description=$style_sheet->getCell('Q'.$i)->getValue();
				$styleNote = $style_sheet->getCell('R'.$i)->getValue();
				$materialsCareDesc = $style_sheet->getCell('S'.$i)->getValue();
				$sizeFitDesc = $style_sheet->getCell('T'.$i)->getValue();
				$size_chart_path=(string)trim($style_sheet->getCell('U'.$i)->getValue());
				$product_display_name=(string)trim($style_sheet->getCell('V'.$i)->getValue());
				$tags=(string)trim($style_sheet->getCell('W'.$i)->getValue());
				$product_type=(string)trim($style_sheet->getCell('X'.$i)->getValue());
				$classification_brand=(string)trim($style_sheet->getCell('Y'.$i)->getValue());
				$season_attr=(string)trim($style_sheet->getCell('Z'.$i)->getValue());
				$comments = (string)trim($style_sheet->getCell('AA'.$i)->getValue());
				$uploadImage = (string)trim($style_sheet->getCell('AB'.$i)->getValue());
			}

			$upload_style['style_id']=$style_id;
			if(!empty($style_id))
			{
				$style_id_res=func_query("select * from mk_product_style where id='$style_id'", TRUE);
				if(empty($style_id_res))
				{
					$upload_style['validated_error'].=" invalid style id";
				}
			}
			elseif($status != 'Draft')
			{
				$upload_style['validated_error'].=" style id is blank";
			}

			$upload_style['vendor_article_number']=$vendor_article_number;
			if(empty($vendor_article_number))
			{
				$upload_style['validated_error'].=" vendor_article_number is blank";
			}
			else
			{
				if(empty($style_id))
				{
					$check=func_query("select * from mk_style_properties where article_number='".$vendor_article_number."'", TRUE);
				}
				else
				{
					$check=func_query("select * from mk_style_properties where style_id!='".$style_id."' and article_number='".$vendor_article_number."'", TRUE);
				}
				global $catalogue_image_path;
				if(!empty($check))
				{
					$upload_style['validated_error'].=" article_number already exists.";
				}
				
				//style image related validation 
				if(strtolower($uploadImage) == 'no')
				{
					$uploadImage = FALSE;
					$upload_style['upload_image'] = 'No';
				}
				else
				{
					$uploadImage = TRUE;
					$upload_style['upload_image'] = 'Yes';
				}
				if(($status == 'Active') && (!empty($style_id)))
				{
					//check if atleast default image exists
                    $allFilesList = scandir($catalogue_image_path/$style_id);
                    $defaultImageExists = false;
                    foreach($allFilesList as $file){
                        if(is_file($file)){
                            $defaultImageExists = true;
                            break;
                        }
                    }
					if((!$uploadImage) || $defaultImageExists)
					{
						//if image does not exist check if there is a db entry for image or not
						$dbDefaultImageEntry = func_query_first_cell("SELECT default_image FROM mk_style_properties WHERE style_id='$style_id'", TRUE);
						if(is_null($dbDefaultImageEntry) || empty($dbDefaultImageEntry))
						{
							$upload_style['validated_error'].=" default image does not exist";
						}
					}
					else
					{
						//if image exists check file size limit for each image
						$imagePath = "$catalogue_image_path/$style_id/";
						$allFiles = scandir($imagePath);
						$imageName = array();
						$notJpgFiles = '';
						foreach($allFiles as $key=>$value)
						{
							$fileExt = strtolower(substr($value, -4));
							if($fileExt == ".jpg")
							{
								array_push($imageName, $value);
							}
							elseif($value != '.' && $value != '..' && $value != 'Thumbs.db')
							{
								$notJpgFiles .= ', '.$value;
							}
						}
						if($notJpgFiles != '')
						{
							$upload_style['validated_error'].= " Files ".substr($notJpgFiles, 1)."  are not jpg files";
						}
						$imagesExceedingSizeLimit = '';
						foreach($imageName as $image)
						{
							if(filesize($imagePath.$image) > 1024*$imageUploadMaxSize)
							{
								$imagesExceedingSizeLimit .= ', '.$image;
							}
						}
						if($imagesExceedingSizeLimit != '')
						{
							$upload_style['validated_error'].= " Images ".substr($imagesExceedingSizeLimit, 1)."  exceed size limit of $imageUploadMaxSize KB.";
						}
					}
				}
				
				if(in_array($vendor_article_number, $article_numbers_check))
				{
					$upload_style['validated_error'].=" duplicate article number";
				}
				else
				{
					array_push($article_numbers_check, $vendor_article_number);
				}
			}

			$upload_style['vendor_article_name']=$vendor_article_name;
			if(empty($vendor_article_name))
			{
				$upload_style['validated_error'].=" vendor_article_name is blank";
			}

			//validate status and its transition
			$styleStatus = strtolower($status);
			$styleStatus =  str_replace(" ", "", $styleStatus);
			if(empty($style_id) && ($styleStatus != 'draft'))
			{
				$upload_style['validated_error'].=" new style can be added in Draft status only";
			}
			elseif(!empty($style_id))
			{
				//validate status transition
				$statusQuery = "SELECT styletype FROM mk_product_style WHERE id = '$style_id'";
				$currentStatusCode = func_query_first_cell($statusQuery, TRUE);
				$possibleStatus = getPossibleStatusFromCode($currentStatusCode);
				$correctStatusTransition = false;
				foreach($possibleStatus as $statusCode => $statusName)
				{
					$posStaName = strtolower($statusName);
					$posStaName = str_replace(" ", "", $posStaName);
					if($posStaName == $styleStatus)
					{
						$status = $statusName;
						$correctStatusTransition = true;
						break;
					}
				}
				if(!$correctStatusTransition)
				{
					$upload_style['validated_error'].=" From ".$possibleStatus[$currentStatusCode]." status this style can go to ".implode(",", $possibleStatus)."; ";
				}
			}
			
			$upload_style['status']=$status;

			$upload_style['article_type']=$article_type;
			if(empty($article_type))
			{
				$upload_style['validated_error'].=" article_type is blank";
			}
			else
			{
				$article_res=func_query("select * from mk_catalogue_classification where BINARY typename='$article_type' and parent1 !='-1' and parent2 !='-1' and is_active=1 order by typename asc", TRUE);
				if(empty($article_res))
				{
					$upload_style['validated_error'].=" article_type is wrong";
				}
			}

			$upload_style['brand']=$brand;
			if(empty($brand))
			{
				$upload_style['validated_error'].=" brand is blank";
			}
			else
			{
				$brand_res=func_query("select * from mk_attribute_type_values where attribute_type='Brand' and BINARY attribute_value='$brand' and is_active=1", TRUE);
				if(empty($brand_res))
				{
					$upload_style['validated_error'].=" brand is wrong";
				}
			}

			$upload_style['age_group']=$age_group;
			if(empty($age_group))
			{
				if($status != 'Draft')
					$upload_style['validated_error'].=" age_group is blank";
			}
			else
			{
				$res=func_query("select * from mk_attribute_type_values where attribute_type_id='2' and BINARY attribute_value='$age_group' and is_active=1", TRUE);
				if(empty($res))
				{
					$upload_style['validated_error'].=" age group is wrong";
				}
			}

			$upload_style['gender']=$gender;
			if(empty($gender))
			{
				$upload_style['validated_error'].=" gender is blank";
			}
			else
			{
				$res=func_query("select * from mk_attribute_type_values where attribute_type_id='3' and BINARY attribute_value='$gender' and is_active=1", TRUE);
				if(empty($res))
				{
					$upload_style['validated_error'].=" gender is wrong";
				}
			}

			$upload_style['base_color']=$base_color;
			if(empty($base_color))
			{
				if($status != 'Draft')
				{
					$upload_style['validated_error'].=" base_color is blank";
				}
					
			}
			else
			{
				$res=func_query("select * from mk_attribute_type_values where attribute_type_id='4' and BINARY attribute_value='$base_color' and is_active=1", TRUE);
				if(empty($res))
				{
					$upload_style['validated_error'].=" base color is wrong";
				}
			}

			$upload_style['color1']=$color1;

			if(!empty($color1))
			{
				$res=func_query("select * from mk_attribute_type_values where attribute_type_id='4' and BINARY attribute_value='$color1' and is_active=1", TRUE);
				if(empty($res))
				{
					$upload_style['validated_error'].=" color1 is wrong";
				}
			}

			$upload_style['color2']=$color2;

			if(!empty($color2))
			{
				$res=func_query("select * from mk_attribute_type_values where attribute_type_id='4' and BINARY attribute_value='$color2' and is_active=1", TRUE);
				if(empty($res))
				{
					$upload_style['validated_error'].=" color2 is wrong";
				}
			}


			$upload_style['fashion_type']=$fashion_type;
			if(empty($fashion_type))
			{
				if($status != 'Draft')
				{
					$upload_style['validated_error'].=" fashion_type is blank";
				}
			}
			else
			{
				$res=func_query("select * from mk_attribute_type_values where attribute_type_id='7' and BINARY attribute_value='$fashion_type' and is_active=1", TRUE);
				if(empty($res))
				{
					$upload_style['validated_error'].=" fashion type is wrong";
				}
			}

			$upload_style['global_attr_season']=$season_attr;
			if(empty($season_attr))
			{
				if($status != 'Draft')
				{
					$upload_style['validated_error'].=" Seasion is blank";
				}
			}
			else
			{
				$res=func_query("select * from mk_attribute_type_values where attribute_type_id='8' and BINARY attribute_value='$season_attr' and is_active=1", TRUE);
				if(empty($res))
				{
					$upload_style['validated_error'].=" Season is wrong";
				}
			}

			$upload_style['year']=$year;
			if(empty($year))
			{
				if($status != 'Draft')
				{
					$upload_style['validated_error'].=" year is blank";
				}
			}
			else
			{
				$res=func_query("select * from mk_attribute_type_values where attribute_type_id='9' and BINARY attribute_value='$year' and is_active=1", TRUE);
				if(empty($res))
				{
					$upload_style['validated_error'].=" year is wrong";
				}
			}

			$upload_style['usage']=$usage;
			if(empty($usage))
			{
				if($status != 'Draft')
				{
					$upload_style['validated_error'].=" usage is blank";
				}
			}
			else
			{
				$res=func_query("select * from mk_attribute_type_values where attribute_type_id='10' and BINARY attribute_value='$usage' and is_active=1", TRUE);
				if(empty($res))
				{
					$upload_style['validated_error'].=" usage is wrong";
				}
			}
			
			if($applySizeScaleRestriction)
			{
				$upload_style['size_chart_scale']=$size_chart_scale;
				$size_chart_scale_set = true;
				if(empty($size_chart_scale))
				{
					$upload_style['validated_error'].=" need to provide size chart scale";
					$size_chart_scale_set = false;
				}
				else
				{
					$article_id = getArticleTypeId($article_type);
					$size_chart_scale_arr = func_query_column("select scale_name from size_chart_scale where id_catalogue_classification_fk = '$article_id'", 0, TRUE);
					if(!in_array($size_chart_scale, $size_chart_scale_arr))
					{
						$upload_style['validated_error'].=" wrong size chart scale for scale for ".$article_type;
						$size_chart_scale_set = false;
					}
				}
			}
			$size_skus = str_replace(' ', '', $size_skus);//remove any space from size value or sku code
			$upload_style['size_skus']=$size_skus;
			$size_skus_arr = explode(",", $size_skus);
			$size_sku_check='size';
			if(empty($size_skus))
			{
				$upload_style['validated_error'].=" need to provide atlease one sku";
			}
			
			if(sizeof($size_skus_arr)%2 !=0)
			{
				$upload_style['validated_error'].=" invalid sku size";
			}
			
			if($applySizeScaleRestriction && $size_chart_scale_set)
			{
				$size_chart_scale_id = getSizeChartScaleId($size_chart_scale, $article_id);
				$size_chart_scale_value_arr = func_query_column("select size_value from size_chart_scale_value where id_size_chart_scale_fk = '$size_chart_scale_id'", 0, TRUE);
				foreach ($size_skus_arr as $size_sku)
				{
					if($size_sku_check == 'size')
					{
						//check if the size is in the database
						$size_chart_scale1 = str_replace(' ', '', $size_chart_scale);//remove any space from scale
						$size_chart_scale_value = trim(str_replace($size_chart_scale1, '', $size_sku));
						if(!in_array($size_chart_scale_value, $size_chart_scale_value_arr))
						{
							$upload_style['validated_error'].=" wrong size $size_sku, ";
						}
						$size_sku_check='sku';
					}
					else
					{
						$size_sku_check='size';
					}
				}
			}
			$size_option_check = array();
			$size_option_duplicate = array();
			$noStyleSkuMap = 0; //no. of style-sku mapping
			foreach ($size_skus_arr as $size_sku)
			{
				if($size_sku_check == 'size')
				{
					$noStyleSkuMap++;
					$size_sku = strtoupper($size_sku);
					$size_sku = str_replace(' ', '', $size_sku);
					if(in_array($size_sku, $size_option_check))
					{
						if(!in_array($size_sku, $size_option_duplicate))
						{
							$upload_style['validated_error'].=" duplicate size value $size_sku,";
							array_push($size_option_duplicate, $size_sku);
						}
					}
					else
					{
						array_push($size_option_check, $size_sku);
					}
					$size_sku_check='sku';
				}
				else if(empty($size_sku))
				{
					$upload_style['validated_error'].=" empty sku";
				}
				else
				{
					$sku_validation_error = check_sku($size_sku, $style_id);
					if($sku_validation_error != "")
					{
						$upload_style['validated_error'].= $sku_validation_error;
					}
					
					if(in_array($size_sku, $sku_code_check))
					{
						$upload_style['validated_error'].=" duplicate sku number";
					}
					else
					{
						array_push($sku_code_check, $size_sku);
					}
					$size_sku_check='size';
				}
			}

			$upload_style['mrp']=$mrp;
			//mrp should be +ve numeric value
			if(empty($mrp))
			{
				if($status != 'Draft')
				{
					$upload_style['validated_error'].=" mrp is blank";
				}
			}
			else
			{
				if (is_numeric($mrp))
				{
					$mrp = round($mrp, 2);
					if($status != 'Draft')
					{
						if($mrp <= 0)
						{
							$upload_style['validated_error'].=" $mrp is not a +ve numeric value";
						}
					}
					elseif($mrp < 0)
					{
						$upload_style['validated_error'].=" $mrp is not a +ve numeric value";
					}
				}
				else
				{
					$upload_style['validated_error'].=" $mrp is not a numeric value";
				}
			}
			
			$upload_style['description'] = richTextORObject2HTML($description);
			$upload_style['styleNote'] = richTextORObject2HTML($styleNote);
			$upload_style['materialsCareDesc'] = richTextORObject2HTML($materialsCareDesc);
			$upload_style['sizeFitDesc'] = richTextORObject2HTML($sizeFitDesc);
			$upload_style['comments'] = $comments;
			
			$upload_style['size_chart_path']=$size_chart_path;

			//size chart image validation
			if(($status == 'Active') && (!empty($style_id)))
			{
				if(!empty($size_chart_path))
				{
					//check if a size chart image exists with size restriction
					global $catalogue_image_path;
					$sizeChartImageAddress = "$catalogue_image_path/$size_chart_path"; 
					if(!is_file($sizeChartImageAddress))
					{
						$upload_style['validated_error'].=" mentioned size chart image does not exists.";
					}
					else
					{
						$sizeChartFileExt = strtolower(substr($size_chart_path, -4));
						if($sizeChartFileExt != ".jpg")
						{
							$upload_style['validated_error'].=" size chart image is not a jpg file.";
						}
						elseif(filesize($sizeChartImageAddress) > 1024*$imageUploadMaxSize)
						{
							$upload_style['validated_error'].= " size chart image exceeds size limit of $imageUploadMaxSize KB";
						}
					}
				}
				else
				{
					//check if there is a size chart image in db and check if there is only 1 sku size chart is not mandatory
					$dbSizeChartImageEntry = func_query_first_cell("SELECT size_chart_image FROM mk_product_style WHERE id='$style_id'", TRUE);
					if(($noStyleSkuMap > 1) && (is_null($dbSizeChartImageEntry) || empty($dbSizeChartImageEntry)))
					{
						$upload_style['validated_error'].=" size chart image does not exists.";
					}
				}
                if(empty($upload_style['description'])){
					$description = func_query_first_cell("SELECT description FROM mk_product_style WHERE id='$style_id'", TRUE);
                    if(empty($description)){
                        $upload_style['validated_error'].= " Description can not be empty when in active state";
                    }
                }
			}


			$upload_style['product_type']=$product_type;
			if(empty($product_type))
			{
				if($status != 'Draft')
				{
					$upload_style['validated_error'].=" product_type is blank";
				}
			}
			else
			{
				$res=func_query("SELECT id, name FROM mk_product_type where type = 'P' and BINARY name='$product_type'", TRUE);
				if(empty($res))
				{
					$upload_style['validated_error'].=" product type is wrong";
				}
			}


			if(!empty($product_display_name))
			{
					$upload_style['product_display_name']=$product_display_name;
			}
			else
			{
				if($status != 'Draft')
				{
					$upload_style['product_display_name']=$brand." ".$gender." ".$vendor_article_name." ".$base_color." ".$article_type;
				}
			}


			$upload_style['tags']=$tags;
			if(empty($tags))
			{
				if($status != 'Draft')
				{
					$upload_style['validated_error'].=" tags is blank";
				}
			}


			$upload_style['classification_brand']=$classification_brand;
			if(empty($classification_brand))
			{
				if($status != 'Draft')
				{
					$upload_style['validated_error'].=" classification_brand is blank";
				}
			}
			else
			{
				$brand_res=func_query("select * from mk_filters where filter_group_id='2' and BINARY filter_name='$classification_brand' and is_active=1", TRUE);
				if(empty($brand_res))
				{
					$upload_style['validated_error'].=" classification brand is wrong";
				}
			}
			
			if(!empty($upload_style['validated_error']))
			{
				$upload_style['validated']='N';
			}
			else
			{
				$upload_style['validated']='Y';
			}
			$upload_style['specific_attributes']=$specific_attributes;
			$upload_style['article_type_attribute_ids']=$article_type_attribute_ids;
			array_push($upload_styles, $upload_style);
		}
		$smarty->assign("upload_styles",$upload_styles);
	}
	else
	{
		$errormsg = "Not able to upload file. Please try again";
	}
}
else if($REQUEST_METHOD == "POST" && $action=='save')
{
	$login=$XCART_SESSION_VARS['identifiers']['A'][login];
	$updateStyleIds = '';
	$createdStyleIds = '';
	$styleIdUpdated = '';
	$progressKey = $_POST['progresskey'];
	// Start Progress bar
	$xcache = new XCache();
	$progressinfo = array('total'=>count($posted_data), 'completed'=>0);
	$xcache->store($progressKey."_progressinfo", $progressinfo, 86400);
	$count=0;
	$styleAuditData = array();
	
	$allStyles = array();
	
	for($j = 0; $j < count($_POST['posted_data']); $j++)
	{
		if(!empty($_POST['posted_data'][$j]['validated_style']) && $_POST['posted_data'][$j]['validated_style']=='Y')
		{
			$row_style_id='';
			$row_style_id=$_POST['posted_data'][$j]['style_id'];
			$row_product_display_name = trim($_POST['posted_data'][$j]['product_display_name']);
			$display_name = preg_replace("/[\s]+/", "-", $row_product_display_name);
			$display_name = preg_replace("/[^a-zA-Z0-9-_&]/","", $display_name);
			$display_name = preg_replace("/\./","", $display_name);	
			
			$imagePrefix =  $display_name."_";
			$size_chart_image_url=$_POST['posted_data'][$j]['size_chart_path'];
			$status = $_POST['posted_data'][$j]['status'];
			global $catalogue_image_path;
			$dbImageUrlUpdate = false;
			$dbSizeChartImageUpdate = false;
			$uploadImage = ($_POST['posted_data'][$j]['upload_image'] == 'No')?FALSE:TRUE;
			if(($status == 'Active') && (!empty($row_style_id)) && ($uploadImage))
			{
				$image_path="$catalogue_image_path/$row_style_id/";
				//check if atleast default image exists
                $allFilesList = scandir($image_path);
                $imageFilesWOExt = str_ireplace(".jpg", "", $allFilesList);
                natsort($imageFilesWOExt);
				if(count($imageFilesWOExt) > 0 )
				{
					$dbImageUrlUpdate = true;
					$image_name = array();
                    $count = 0;
					foreach($imageFilesWOExt as $key=>$valueWOExt)
					{
						$value = $allFilesList[$key];
						$fileExt = strtolower(substr($value, -4));
                        if($fileExt != ".jpg"){
                            continue;
                        }
                        if($count == 0){
					        $default_image_url = func_resize_backup_uploaded_product_image("$image_path/$value", $value, "default", false, $imagePrefix);
                            if($default_image_url == null){
                                break;//Break the loop as default image is not uploaded successfully
                            }
                        } else { 
							array_push($image_name, $value);
                        }
                        $count++;
					}
				}
			}
			
			if($dbImageUrlUpdate)
			{
				$front_image_url = null;
				$back_image_url = null;
				$right_image_url = null;
				$left_image_url = null;
				$top_image_url = null;
				$bottom_image_url = null;
	
				if(!empty($image_name[0]))
				{
					$front_image_url = func_resize_backup_uploaded_product_image($image_path.'/'.$image_name[0],$image_name[0], "others",false,$imagePrefix);
				}
				if(!empty($image_name[1]))
				{
					$back_image_url = func_resize_backup_uploaded_product_image($image_path.'/'.$image_name[1],$image_name[1], "others",false,$imagePrefix);
				}
				if(!empty($image_name[2]))
				{
					$right_image_url = func_resize_backup_uploaded_product_image($image_path.'/'.$image_name[2],$image_name[2], "others",false,$imagePrefix);
				}
				if(!empty($image_name[3]))
				{
					$left_image_url = func_resize_backup_uploaded_product_image($image_path.'/'.$image_name[3],$image_name[3], "others",false,$imagePrefix);
				}
				if(!empty($image_name[4]))
				{
					$top_image_url = func_resize_backup_uploaded_product_image($image_path.'/'.$image_name[4],$image_name[4], "others",false,$imagePrefix);
				}
				if(!empty($image_name[5]))
				{
					$bottom_image_url = func_resize_backup_uploaded_product_image($image_path.'/'.$image_name[5],$image_name[5], "others",false,$imagePrefix);
				}
				
				if($default_image_url === null)
				{
					$default_image_url = "";
				}
				if($front_image_url === null)
				{
					$front_image_url = "";
				}
				if($back_image_url === null)
				{
					$back_image_url = "";
				}
				if($right_image_url === null)
				{
					$right_image_url = "";
				}
				if($left_image_url === null)
				{
					$left_image_url = "";
				}
				if($top_image_url === null)
				{
					$top_image_url = "";
				}
				if($bottom_image_url === null)
				{
					$bottom_image_url = "";
				}
			}
			
			if(is_file("$catalogue_image_path/$size_chart_image_url"))
			{
				// check if the size_chart already exists
				if(!is_file($s_upload_dir.$size_chart_image_url))
				{
					$size_chart_image_url = func_resize_backup_uploaded_product_image($catalogue_image_path."/".$size_chart_image_url,$size_chart_image_url, "sizechart",false);
				}
				else
				{
					$size_chart_image_url=$s_cdn_upload_dir_db.$size_chart_image_url;
				}
				$dbSizeChartImageUpdate = true;
			}
			
			if($dbSizeChartImageUpdate)
			{
				if($size_chart_image_url === null)
				{
					$size_chart_image_url = "";
				}
				else
				{
					$sizeChartFileExt = strtolower(substr($size_chart_image_url, -4));
					if($sizeChartFileExt != ".jpg")
					{
						$size_chart_image_url = "";
					}
				}
			}
			
			$product_type=getProductType($_POST['posted_data'][$j]['product_type']);
			$product_display_name=$_POST['posted_data'][$j]['product_display_name'];
			$product_display_name = $product_display_name;
			$price=$_POST['posted_data'][$j]['price'];

			$is_customizable='0';
			$is_active = $_POST['posted_data'][$j]['status'];
			$is_active = strtolower($is_active);
			$is_active =  str_replace(" ", "", $is_active);
			$allStatus = json_decode(getStyleStatus(), true);
			foreach ($allStatus as $status)
			{
				$styleStatus = strtolower($status['name']);
				$styleStatus =  str_replace(" ", "", $styleStatus);
				if($is_active == $styleStatus)
				{
					$is_active = $status['id'];
					break;
				}
			}
			
			//Audit style's price, status and comments before updating db
			if(!empty($row_style_id))
			{
				$styleid  = $row_style_id;
				$productStyle = func_query("select styletype, price from mk_product_style where id = '$styleid'", TRUE);
				$oldPrice = $productStyle[0]['price'];
				$oldStatusCode = $productStyle[0]['styletype'];
				$oldStatus = func_query_first_cell("select style_status_name_to from style_status_transition_rules where style_status_code_to = '$oldStatusCode'", TRUE);
				$operationType = 'UPDATE';
			}
			else
			{
				$oldPrice = '0';
				$oldStatus = "NotAdded";
				$operationType = 'INSERT';
			}
			
			$description=$_POST['posted_data'][$j]['description'];
			$query_data = array(
					"product_type" => $product_type,
					"name" => trim($product_display_name),
					"label" => trim($product_display_name),
					"description" => $description,
					"price" => $price,
					"iventory_count"=> 100,
					"styletype" => $is_active,
					"material" =>'',
					"bulk" => 'Y',
					"style_code" => 'NPP',
					"default_store_markdown"	=> 0,
					"default_store_rate"		=> 0,
					"is_customizable"		=> $is_customizable,
					"is_active" => 1
			);
            $query_data = func_remove_empty_values($query_data);
			if($dbSizeChartImageUpdate)
			{
				$query_data["size_chart_image"]  = $size_chart_image_url;
			}
			if(!empty($row_style_id))
			{
				func_array2updateWithTypeCheck("mk_product_style", $query_data, "id='$row_style_id' ");
			}
			else
			{
				func_array2insertWithTypeCheck("mk_product_style", $query_data);
			}

			if(!empty($row_style_id))
			{
				$styleid  = $row_style_id;
				$updateStyleIds .= "$styleid".", ";
			}
			else
			{
				$styleid  = db_insert_id();
				$createdStyleIds .= "$styleid".", ";
			}
			
			$styleIdUpdated .= "$styleid".", ";
			
			//Auditing continues, auditing price and status
			$stylePriceAudit = array(
					"entityId" => $styleid,
					"entityName" => 'com.myntra.StyleProperties',
					"entityProperty" => 'Price',
					"entityPropertyOldValue" => $oldPrice,
					"entityPropertyNewValue" => round($query_data['price'], 2),
					"operationType" => $operationType);
			
			$newStatus = func_query_first_cell("select style_status_name_to from style_status_transition_rules where style_status_code_to = '$is_active'", TRUE);
			$styleStatusAudit = array(
					"entityId" => $styleid,
					"entityName" => 'com.myntra.StyleProperties',
					"entityProperty" => 'Status',
					"entityPropertyOldValue" => $oldStatus,
					"entityPropertyNewValue" => $newStatus,
					"operationType" => $operationType);
			
			$styleAuditData[] = $stylePriceAudit;
			$styleAuditData[] = $styleStatusAudit;
			
			$article_number=$_POST['posted_data'][$j]['article_number'];
			$product_tags = $_POST['posted_data'][$j]['product_tags'];
			$article_name = $_POST['posted_data'][$j]['article_name'];

			$global_attr_article_type=getArticleTypeId($_POST['posted_data'][$j]['global_attr_article_type']);
			$subMasterCatSQL = "SELECT parent1, parent2  FROM mk_catalogue_classification WHERE id = '$global_attr_article_type'";
			$subMasterCatResult = func_query_first($subMasterCatSQL, TRUE);
			$global_attr_sub_category = $subMasterCatResult['parent1'];
			$global_attr_master_category = $subMasterCatResult['parent2'];
			
			$global_attr_brand=$_POST['posted_data'][$j]['global_attr_brand'];
			$global_attr_age_group=$_POST['posted_data'][$j]['age_group'];
			$global_attr_gender=$_POST['posted_data'][$j]['global_attr_gender'];
			$global_attr_base_colour=$_POST['posted_data'][$j]['global_attr_base_colour'];
			$global_attr_colour1=$_POST['posted_data'][$j]['colour1'];
			$global_attr_colour2=$_POST['posted_data'][$j]['colour2'];
			$global_attr_fashion_type=$_POST['posted_data'][$j]['global_attr_fashion_type'];
			$global_attr_season=$_POST['posted_data'][$j]['global_attr_season'];
			$global_attr_year=$_POST['posted_data'][$j]['global_attr_year'];
			$global_attr_usage=$_POST['posted_data'][$j]['global_attr_usage'];

			$styleNote = $_POST['posted_data'][$j]['styleNote'];
			$materialsCareDesc = $_POST['posted_data'][$j]['materialsCareDesc'];
			$sizeFitDesc = $_POST['posted_data'][$j]['sizeFitDesc'];
			$comments = $_POST['posted_data'][$j]['comments'];

			//Auditing comments
			$oldComments = func_query_first_cell("select comments from mk_style_properties where style_id = '$styleid'", TRUE);
			$styleCommentsAudit = array(
					"entityId" => $styleid,
					"entityName" => 'com.myntra.StyleProperties',
					"entityProperty" => 'Comments',
					"entityPropertyOldValue" => $oldComments,
					"entityPropertyNewValue" => $comments,
					"operationType" => $operationType);
			
			$styleAuditData[] = $styleCommentsAudit;
			
			$sports_query_data = array(
                    "style_id" 			=> $styleid,
                    "article_number" 			=> $article_number,
                    "product_tags"				=> str_replace(" ,", ",", str_replace(", ", ",", trim($product_tags))),
                    "product_display_name"		=> trim($product_display_name),
                    "variant_name" 				=> trim($article_name),
                    "discount_type" 			=> $discount_type,
                    "discount_value"    		=> $discount_value,
                    "myntra_rating"		        => !empty($rating) ? $rating : 0,
					"modified_date"				=> time(),
                    "target_url"                => trim($target_url),
                    "global_attr_article_type"  => $global_attr_article_type,
                    "global_attr_sub_category"  => $global_attr_sub_category,
                    "global_attr_master_category"=>$global_attr_master_category,
                    "global_attr_brand"         => $global_attr_brand,
                    "global_attr_age_group"     => $global_attr_age_group,
                    "global_attr_gender"        => $global_attr_gender,
                    "global_attr_base_colour"   => $global_attr_base_colour,
                    "global_attr_colour1"       => $global_attr_colour1,
                    "global_attr_colour2"       => $global_attr_colour2,
                    "global_attr_fashion_type"  => $global_attr_fashion_type,
                    "global_attr_season"        => $global_attr_season,
                    "global_attr_year"          => $global_attr_year,
                    "global_attr_usage"         => $global_attr_usage,
                    "global_attr_catalog_add_date"=> time(),
					"style_note"                => $styleNote,
					"materials_care_desc"       => $materialsCareDesc,
					"size_fit_desc"             => $sizeFitDesc,
					"comments"                  => $comments
			);
			
			if($dbImageUrlUpdate)
			{
				$search_images=explode(",",$default_image_url);
				$sports_query_data["default_image"] = $search_images[0];
				$sports_query_data["search_image"] = $search_images[2];
				$sports_query_data["search_zoom_image"] = $search_images[1];
				$sports_query_data["front_image"]= $front_image_url;
				$sports_query_data["back_image"]= $back_image_url;
				$sports_query_data["left_image"]= $left_image_url;
				$sports_query_data["right_image"]= $right_image_url;
				$sports_query_data["top_image"]= $top_image_url;
				$sports_query_data["bottom_image"]= $bottom_image_url;
				
				$uploadedImages = getUploadedImageType($sports_query_data);
				//auditing image uploading
				$styleImageAudit = array(
						"entityId" => $styleid,
						"entityName" => 'com.myntra.StyleProperties',
						"entityProperty" => 'Image Upload',
						"entityPropertyOldValue" => 'Image Upload',
						"entityPropertyNewValue" => "Uploaded $uploadedImages images",
						"operationType" => 'Image Upload');
				$styleAuditData[] = $styleImageAudit;
			}

                        $needToApplyDiscount = true;
			if(!empty($row_style_id))
			{
                                /*Need to check if are editing any of the discount filter columns*/
                                $oldStyle = func_query_first("select global_attr_brand, global_attr_season, global_attr_year, global_attr_fashion_type, global_attr_article_type from mk_style_properties where style_id='$style_id'", TRUE);
                                
                                if($oldStyle['global_attr_brand']==$global_attr_brand && 
                                $oldStyle['global_attr_season']==$global_attr_season && 
                                $oldStyle['global_attr_year']==$global_attr_year && 
                                $oldStyle['global_attr_fashion_type']==$global_attr_fashion_type && 
                                $oldStyle['global_attr_article_type']==$global_attr_article_type ) {
                                    $needToApplyDiscount = false;
                                }

                                //check if the status is active and catalog_date is null then set catalog date to current time
				if($query_data["styletype"] == 'P')
				{
					//check if catalog date is null
					$catalog_date = func_query_first_cell("select catalog_date from mk_style_properties where style_id = $row_style_id", TRUE);
					if(is_null($catalog_date))
					{
						$sports_query_data["catalog_date"] = time();
					}
				}
				global $sqllog;
				$sqllog->debug("User: $login ; Style id = $row_style_id");
				$sqllog->debug($sports_query_data);
                $sports_query_data = func_remove_empty_values($sports_query_data);
				$insert_id = func_array2updateWithTypeCheck("mk_style_properties", $sports_query_data, "style_id='$row_style_id' ");
			}
			else
			{
				$sports_query_data["add_date"] = time();
				if($query_data["styletype"] == 'P')
				{
					$sports_query_data["catalog_date"] = $sports_query_data["add_date"];
				}
                $sports_query_data = func_remove_empty_values($sports_query_data);
				$insert_id = func_array2insertWithTypeCheck("mk_style_properties", $sports_query_data);
			}
                        
                        //add style to discount
                        if($needToApplyDiscount){
                            DiscountEngine::asignDiscountToStyle($styleid);
                        }
                        
			$applied_filters=$_POST['posted_data'][$j]['classification_brand'];
			if(!empty($applied_filters))
			{
				$appliedFiltersSQL = "select * from mk_applied_filters where generic_id='".$style_id."' and filter_group_id='2' and applied_filters='$applied_filters'"; 
				$check=func_query($appliedFiltersSQL, TRUE);
				if(!empty($check[0]))
				{
					func_query("update mk_applied_filters set applied_filters='".$applied_filters."' where generic_id='".$style_id."' and filter_group_id ='2'");
				}
				else
				{
					$filter_data=array("generic_type"=>"style",
					"generic_id" => $styleid,
					"filter_group_id" =>2,
					"applied_filters" => $applied_filters,	
					"is_active" => 1
					);
					func_array2insert("mk_applied_filters",$filter_data);
				}
			}
			else
			{
				func_query("delete from mk_applied_filters where generic_id='".$style_id."' and filter_group_id ='2'");
			}

			if($applySizeScaleRestriction)
			{
				$size_chart_scale = $_POST['posted_data'][$j]['size_chart_scale'];
				$size_chart_scale_id = getSizeChartScaleId($size_chart_scale, $global_attr_article_type);
			}
			
			if(empty($row_style_id))
			{
				$weblog->info("Bulk Upload new style: creating sku");
				// Delete SKUs
				// delete the skus which are not in the list
				$sql = "DELETE FROM mk_product_options WHERE style='$styleid'";
				func_query($sql);
				$sql = "DELETE FROM mk_styles_options_skus_mapping WHERE style_id='$styleid'";
				func_query($sql);
				$weblog->info("Bulk Upload new style: deleted existing entries");
				//Add skus
				$size_skus_list = explode(",",$_POST['posted_data'][$j]["size_skus"]);
				$option_name_value_list="'";
				$sku_id_list="'";
				for ($i = 0; $i < count($size_skus_list); $i=$i+2)
				{
					$option_value = $size_skus_list[$i];
					$sku_code_value= trim($size_skus_list[$i+1]);
					$option_name = 'Size';
						
					if($applySizeScaleRestriction)
					{
						$size_chart_scale_value = trim(str_replace($size_chart_scale, '', $option_value));
						$size_chart_scale_value_id = getSizeChartScaleValueId($size_chart_scale_value, $size_chart_scale_id);
					}
										
					$option_id = '';
					 
					$weblog->info("Bulk Upload new style: checking enable:option value: $option_value: sku : $sku_code_value");
					$sku_id = MyntraStyle::getSkuId($sku_code_value);
					if(!empty($option_name))
					{
						$productOptionArray = array(
												"style" => $styleid,
												"name" => $option_name,
												"value" => $option_value,
												"is_active" => 1
												);
						if($applySizeScaleRestriction)
						{
							$productOptionArray["id_size_chart_scale_value_fk"] = $size_chart_scale_value_id;
						}						 
						$option_id = func_array2insert("mk_product_options", $productOptionArray);
					}
					// now insert the row in the mapping table
					func_array2insert("mk_styles_options_skus_mapping",
					array("style_id" => $styleid, "option_id" => $option_id, "sku_id" => $sku_id));
				}
			}
			else
			{
				// Call for updating the skus
				$size_skus_list = explode(",",$_POST['posted_data'][$j]["size_skus"]);
				$option_name_value_list="'";
				$sku_id_list="'";
				$mapping_id_list=array();
				for($i = 0; $i < count($size_skus_list); $i=$i+2)
				{
					$option_value = $size_skus_list[$i];
					$sku_code_value= trim($size_skus_list[$i+1]);
					$option_name = 'Size';
						
					$option_id = '';
					
					if($applySizeScaleRestriction)
					{
						$size_chart_scale_value = trim(str_replace($size_chart_scale, '', $option_value));
						$size_chart_scale_value_id = getSizeChartScaleValueId($size_chart_scale_value, $size_chart_scale_id);
					}
					
					 
					$sku_id = MyntraStyle::getSkuId($sku_code_value);
						
					// Check if the combination of option already exist if not then create it
					$sql="SELECT id from mk_product_options where style='$styleid' and name='$option_name' and value='$option_value' order by id desc";
					$results=func_query($sql, TRUE);
					$option_exist=false;
					foreach($results as $resul_tmp)
					{
						if(!empty($resul_tmp))
						{
							$option_id=$resul_tmp['id'];
							$option_exist=true;
							break;
						}
					}

					if(!$option_exist)
					{
						$productOptionArray = array(
												"style" => $styleid,
												"name" => $option_name,
												"value" => $option_value,
												"is_active" => 1
												);
						if($applySizeScaleRestriction)
						{
							$productOptionArray["id_size_chart_scale_value_fk"] = $size_chart_scale_value_id;
						}
						$option_id = func_array2insert("mk_product_options",$productOptionArray);
					}
					else if($applySizeScaleRestriction)
					{
						//check if the scale value id is not null if it is then update it
						$sql="SELECT id_size_chart_scale_value_fk from mk_product_options where id='$option_id'";
						$id_size_chart_scale_value_fk = func_query_first_cell($sql, TRUE);
						$errorlog->error("size id: ".$size_chart_scale_value_id);
						if(is_null($id_size_chart_scale_value_fk))
						{
							func_query("update mk_product_options set id_size_chart_scale_value_fk='$size_chart_scale_value_id' where id='$option_id'");
						}
					}
					// check if the mapping already exist if not then create it.
					$sql="SELECT id from mk_styles_options_skus_mapping where style_id='$styleid' and option_id='$option_id' and sku_id='$sku_id'";
					$results=func_query($sql, TRUE);
					$option_map_exist=false;
					foreach ($results as $result_tmp)
					{
						if(!empty($result_tmp))
						{
							$mapping_id=$result_tmp['id'];
							$option_map_exist=true;
							break;
						}
					}
					if(!$option_map_exist)
					{
						// now insert the row in the mapping table
						$mapping_id = func_array2insert("mk_styles_options_skus_mapping",
						array("style_id" => $styleid, "option_id" => $option_id, "sku_id" => $sku_id));
					}
					// to check if mapping is still in use or not in use anymore
					array_push($mapping_id_list,$mapping_id);
				}
				// delete the mappings not in use anymore
				if(!empty($mapping_id_list))
				{
					$id_list=implode(',', $mapping_id_list);
					$sql = "DELETE FROM mk_styles_options_skus_mapping WHERE style_id='$styleid' and id not in ($id_list)";
					func_query($sql);
				}
			}
			updateDisplayCategoryForStyle($styleid);
			//add/delete style to solr
			
			array_push($allStyles, $styleid);

			if(empty($row_style_id))
			{		
				$area_id=func_array2insert(
                               "mk_style_customization_area",
				array(
                                "style_id" => $styleid,
                                "name" => "default",
                                "label" => "default",
                                "description" => "default",
                                "isprimary" => true,
                                "default_image" => 'skin1/images/spacer.gif',
                                "image_t" => 'skin1/images/spacer.gif',
                                "image_l"  => 'skin1/images/spacer.gif',
                                "bkgroundimage" => 'skin1/images/spacer.gif',
                                "image_z" => 'skin1/images/spacer.gif'
                                )
                                );
                                func_array2insert(
					"mk_customization_orientation",
                                array(
					"location_name" => "default",
					"start_x" => 116,
				    "start_y" => 80,
				    "size_x" => 156,
				    "size_y" => 192,
				    "height_in" => 16,
				    "width_in" => 13,
				    "area_id" => $area_id 	,
					"ort_default" => 'Y',
					"bgstart_x" => 89,
					"bgstart_y" => 58,
					"bgsize_x"=> 99,
					"bgsize_y"=> 121,
					"bgheight_in"=> 16,
					"bgwidth_in"=> 13,
					"action"=> "customize your product",
					"typeid"=> 1,
					"imageAllowed"=> 0,
					"imageCustAllowed"=> 0,
					"textAllowed"=> 0,
					"textCustAllowed"=> 0,
					"textLimitation"=> 0,
					"textLimitationTpl"=> 0

                                )
                                );

			}
		}
		$count++;
		$progressInfo = $xcache->fetch($progressKey."_progressinfo");
		$progressInfo['completed'] = $count;
		$xcache->store($progressKey."_progressinfo", $progressInfo, 86400);
	}
	
	//bulk auditing of catalog changes
	$auditResult = AuditApiClient::sendStyleAuditInfo($styleAuditData, $login);
	if(isset($allStyles)){
		PortalRequestHandlerClient::refreshStyles($allStyles);
	}	
	$xcache->remove($progressKey."_progressinfo");

	$styleIdUpdated = substr($styleIdUpdated, 0, -2);
	doSizeUnification("ClearAll", $styleIdUpdated);
	doSizeUnification("UpdateAll", $styleIdUpdated);

	$message = '';
	if($createdStyleIds != '')
	{
		$message = 'Created Styles with id '.$createdStyleIds;
	}
	if($updateStyleIds != '')
	{
		$message .= ' Updated Styles with id '.$updateStyleIds;
	}
	
	func_header_location($http_location . "/admin/style_bulk_upload.php?message=$message");
}

/**
 * function to convert rich text to html text
 * @param rtf object or html text $textData
 */
function richTextORObject2HTML($textData)
{
	$htmlText = '';
	if(is_object($textData))
	{
		$elements = $textData->getRichTextElements();
		foreach ($elements as $item)
		{
			if(is_object($item->getFont()))
			{
				$htmlText .= '<font style="color: #'.$item->getFont()->getColor()->getRGB().'; font-style: '.($item->getFont()->getItalic() ? 'italic' : 'normal').'; text-decoration: '.($item->getFont()->getUnderline()== 'single' ? 'underline' : 'normal').'; font-weight: '.($item->getFont()->getBold() ? 'bold' : 'normal').'">'.nl2br($item->getText()).'</font>';
			}
			else
			{
				$htmlText.=''.nl2br($item->getText()).'';
			}
		}
	}
	else
	{
		$htmlText = nl2br($textData);
	}
	return $htmlText;
}

function getProductType($product_type)
{
	$sql = "SELECT id, name FROM mk_product_type where type = 'P' and name='$product_type'";
	$product_type=func_query_first($sql, TRUE);
	if(!empty($product_type))
	{
		return $product_type['id'];
	}
	else
	{
		return null;
	}
}

function getArticleTypeId($article_type)
{
	$sql = "SELECT id from mk_catalogue_classification where typename = '" . $article_type."' and parent1 !='-1' and parent2 !='-1' and is_active=1";
	$row = func_query_first($sql, TRUE);
	if(!empty($row))
	{
		return $row['id'];
	}
	else
	{
		return null;
	}
}

function getSizeChartScaleId($size_chart_scale, $article_id)
{
	$sql = "select id from size_chart_scale where scale_name = '$size_chart_scale' and id_catalogue_classification_fk = '$article_id'";
	$row = func_query_first($sql);
	if(!empty($row))
	{
		return $row['id'];
	}
	else
	{
		return null;
	}	
}

function getSizeChartScaleValueId($size_chart_scale_value, $size_chart_scale_id)
{
	$sql = "select id from size_chart_scale_value where size_value = '$size_chart_scale_value' and id_size_chart_scale_fk = '$size_chart_scale_id'";
	$row = func_query_first($sql, TRUE);
	if(!empty($row))
	{
		return $row['id'];
	}
	else
	{
		return null;
	}
}

function check_sku($sku_check,$style_id_sku_check)
{
	$code_string=$sku_check;
	$sku_details = SkuApiClient::getSkuDetailsForSkuCode($code_string);
	
	if(empty($sku_details))
	{
		return "Sku $sku_check is not Valid";
	}
	
	$skuId = $sku_details[0]['id'];
	
	$sql="SELECT sku_id, style_id from mk_styles_options_skus_mapping where sku_id = $skuId";
	if(!empty($style_id_sku_check) and $style_id_sku_check != "")
	{
		$sql .= " and style_id != $style_id_sku_check";
	}
		
	$existing_mapping = func_query_first($sql, TRUE);
	
	if(!empty($existing_mapping) && $existing_mapping)
	{
		return "Sku $sku_check is already mapped to Style - ".$existing_mapping['style_id'];
	}
	
	return "";
}

function getAttributeTypeValueId($attribute_type_value,$attributes_type_value_id)
{
	$sql="SELECT id from mk_attribute_type_values where attribute_type_id='$attributes_type_value_id' and Binary attribute_value='$attribute_type_value'";
	$result_set=func_query_first($sql, TRUE);
	if(!empty($result_set))
	{
		return $result_set['id'];
	}
	else
	{
		return null;
	}
}

$smarty->assign("main","style_bulk_upload");
$smarty->assign("message",$_GET['message']);
func_display("admin/home.tpl",$smarty);
?>
