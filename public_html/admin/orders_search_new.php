<?php
/*
 * Created on Feb 8, 2012
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

echo "Access to this page for viewing any order related information is disabled.<br>You can view the relevant information on Prism  (http://prism.mynt.myntra.com)<br><br>Please send an email to servicedesk@myntra.com and send the following details :<br><br>Name :<br>Location :<br>Manager :<br>Business Justification :   (( what info in this xcart page was used by you )<br><br>Request servicedesk@myntra.com for access for prism.mynt.myntra.com with prism readonly role.";
exit(0);
 
require "./auth.php";
require_once $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/class/class.ordersearch.php";
include_once $xcart_dir."/modules/apiclient/WarehouseApiClient.php";
include_once $xcart_dir."/admin/enums/GiftOrderStatus.php";

$action = $_POST['action'];
if(!$action)
	$action = 'view';
	
if($action == 'search') {
	
	$query = ordersearch::func_create_search_query($_POST);
	$sqllog->info("Order Search Query = $query");
	
	$orders_results = func_query($query, true);
	
	foreach($orders_results as $index=>$order) {
		$orders_results[$index]['date'] = date('d-m-Y H:i:s', $order['date']);
		if( !empty($order['queueddate']) && $order['queueddate'] != 0 )
			$orders_results[$index]['queueddate'] = date('d-m-Y H:i:s', $order['queueddate']);
		else
			$orders_results[$index]['queueddate'] = "-";
	}
	
	echo json_encode(array('status'=>true, 'results'=>$orders_results));
	exit(0);
}else if($action == 'giftsearch') {
	$query = ordersearch::func_create_giftsearch_query($_POST);
	$sqllog->info("Order Search Query = $query");
	
	$orders_results = func_query($query, true);
	foreach($orders_results as $index=>$order) {
		$orders_results[$index]['date'] = $order['created_on'];
		$orders_results[$index]['status'] = GiftOrderStatus::getEnumNameForValue($order['status']);
	}
	echo json_encode(array('status'=>true, 'results'=>$orders_results));
	exit(0);
}

$warehouse_rows = WarehouseApiClient::getAllWarehouses();
$ops_locations = array();
foreach($warehouse_rows as $row) {
	$ops_locations[] = array('id'=>$row['id'], 'name'=>$row['name']);
}

$smarty->assign('ops_locations', $ops_locations);
$smarty->assign('main', 'orders_search_new');
func_display("admin/home.tpl", $smarty); 
 
?>
