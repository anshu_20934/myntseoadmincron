<?php
require_once dirname(__FILE__)."/auth.php";
require_once $xcart_dir."/include/security.php";

use cache\InvalidateXCache;
if($REQUEST_METHOD == "POST") {
	
	$xCacheInvalidator =  InvalidateXCache::getInstance();
	
	if(isset($_POST['clearPHPCache'])) {
		$xCacheInvalidator->invalidateAllPHPCache();		
	}
	
	if(isset($_POST['clearVarCache'])) {
		$xCacheInvalidator->invalidateAllVarCache();
	}
	
	if(isset($_POST['clearAllCache'])) {
		$xCacheInvalidator->invalidateCompleteXCache();
	}
	
	if(isset($_POST['clearKey'])) {
		$key = $_POST['clearKey'];
		$xCacheInvalidator->invalidateCacheForKey($key);
	}
	
	func_header_location($http_location . "/admin/xcache_common.php");
}
$smarty->assign("main","xcache_common");
func_display("admin/home.tpl",$smarty);
exit;
?>