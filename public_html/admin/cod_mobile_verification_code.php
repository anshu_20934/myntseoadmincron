<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
include_once "$xcart_dir/modules/coupon/mrp/SmsVerifier.php";

$smsVerifier = SmsVerifier::getInstance();
$mode = $_GET['mode'];
$login = $_GET['login'];

if(!empty($mode) && !empty($login))	{
	
	$smsVerifier->addNewMapping($mobile,$login);
	$retval = $smsVerifier->verifyMapping($mobile,$login);
	$smarty->assign("mobileVerificationSuccess","Mobile number: $mobile successfully verified");
}

if(!empty($login)) {
		
		$query = "select mobile from xcart_customers where login='$login';";
		$ret = func_query_first_cell($query);
		$user = $smsVerifier->getVerifiedLoginFor($ret);
			
		if($user != null && $user != $login)
		{	
			$failure = "This number:".$ret." is already verified against user ID: ".$user;
			$smarty->assign("failure", $failure);
			$smarty->assign("norecordsmessage", "No records found for '$login'");
			
		} else {
			
			$query = "select mm.mobile as mobile, mc.code as code, mm.status as status, mm.login as login from mk_mobile_mapping as mm, mk_mobile_code as mc where mm.mobile='$ret' and mm.login='$login' and mc.login='$login' 
			and  mm.mobile=mc.mobile;";
			$result = func_query($query);
			
			foreach($result as $key=>$value) {
				// mobile number registered with an existing user
				
				switch($value['status']) {
					case 'O': $value['statusmeaning'] = 'Old';
							  break;
					case 'U': $value['statusmeaning'] = 'Unverified';
							  break;
					case 'V': $value['statusmeaning'] = 'Verified';
							  break;
					default:$value['statusmeaning'] = $value['status'];
				}
				
				$result[$key] = $value;
			}
						
			if(!empty($result)) { 
				$smarty->assign("mobileresult", $result);
			} else {
				$smarty->assign("norecordsmessage", "No records found for '$login'");
				$smarty->assign("failure", "Please ask the customer to click on verify button for mobile num: $ret.");
			}
		}
}

$template ="cod_mobile_verification_code";
$smarty->assign("main",$template);
func_display("admin/home.tpl",$smarty);

?>
