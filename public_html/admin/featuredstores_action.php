<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: contest_action.php,v 1.31 2006/02/14 14:45:23 max Exp $
#

define("IS_MULTILANGUAGE", true);
define('USE_TRUSTED_POST_VARIABLES',1);
$trusted_post_variables = array("desc");
$location[] = array("Featured Stores", "admin_featuredstores.php");
require "./auth.php";
require $xcart_dir."/include/security.php";

if ($REQUEST_METHOD == "POST") {
	//modify the stores
	if ($mode == "modify") {
		$data ="";
		if(is_array($posted_data)) {
			foreach ($posted_data as $contestid=>$v) {
				if(!empty($v['delete'])){
					$data = $contestid;
					$contests = func_query("select * from mk_featured_stores where id='".$data."' ");
				}
			}
		}
		$template = "featuredstores";
	}

	// delete the stores
	elseif($mode == "delete"){
		if (is_array($posted_data)) {
			foreach ($posted_data as $cid=>$v) {
				if(!empty($v['delete'])){
					db_query ("DELETE FROM mk_featured_stores WHERE id='$cid' ");
				}
			}
			$top_message["content"] = func_get_langvar_by_name("msg_adm_producttype_del");
			$top_message["anchor"] = "featured";
			func_header_location("admin_featuredstores.php");
		}
	}
	//add the store
	elseif ($mode == "add") {
		func_header_location("featured_stores.php");
	}
}

$smarty->assign("mode",$mode);
$smarty->assign("contestid",$data);
$smarty->assign("mode", $mode);
$smarty->assign("errMsg",$error);

$smarty->assign ("contest", $contests);
$smarty->assign("main",$template);
$smarty->assign("location", $location);

$smarty->assign("dialog_tools_data", $dialog_tools_data);
$smarty->assign("img", $img);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>