<?php
require("auth.php");
require $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.order.php";

if($_POST['action'] == 'upload') {
	
	$progresskey = $_REQUEST['progresskey'];
	$xcache = new XCache();
	$progressinfo = array('status'=>'PROCESSING');
	$xcache->store($progresskey."_progressinfo", $progressinfo, 3600);
	
	$filename = $HTTP_POST_FILES['lostorders']['name'];
	$tmpName = $HTTP_POST_FILES['lostorders']['tmp_name'];

	$ext = pathinfo($filename, PATHINFO_EXTENSION);
	if($ext != 'csv'){
		echo '{"success":false, "message":"Please upload a .csv file!"}';
		exit;
	}
	$fp      = fopen($tmpName, 'r');
	$lostorders = fread($fp, filesize($tmpName));
	$lostorders = preg_split("/[\s,]+/", $lostorders, -1, PREG_SPLIT_NO_EMPTY);
	fclose($fp);
	
	$message = "";
	$cancellation_codes = func_query("select cancel_reason from cancellation_codes where cancel_type = 'LOC'", true);
	$lost_codes = array();
	foreach($cancellation_codes as $codesarray){
		$lost_codes[] = $codesarray['cancel_reason'];
	}
	
	for($i = 0; $i < count($lostorders); $i+=2){
		$lostorderid = ereg_replace("[^0-9]", "", $lostorders[$i]);
		$lostreason = ereg_replace("[^A-Za-z]", "", $lostorders[$i+1]);
		if(!in_array($lostreason, $lost_codes)){
			$message.= "\n Order $lostorderid has an invalid lost reason code '$lostreason' ";
		} else {
			$response = cancel_lost_orders($lostorderid, $lostreason);
			if($response['status'] == 'failure'){
				$message.= "\n Order $lostorderid not marked as lost, $response[message] ";
			} elseif($response['exOrder'] === true) {
				$message.= "\n Order $lostorderid marked as lost, no refund issued for this exchange order ";
			} else {
				$message.= "\n Order $lostorderid marked as lost and refund issued ";
			}
		}	
	}
	$message = nl2br($message, false);
	$return_data = array("status" => 'DONE', "success" => true, "message" => $message);
	$xcache->store($progresskey."_progressinfo", $return_data, 3600);
	echo json_encode($return_data);
	exit;
} elseif($_POST['action'] == 'cancelSingleOrder'){
	$orderid = $_POST['order_id'];
	$lost_reason = $_POST['lost_reason'];
	
	$response = cancel_lost_orders($orderid, $lost_reason);
	if($response['status'] == 'failure'){
		echo json_encode(array("failure"," Order $orderid not marked as lost, $response[message] "));
		exit;
	}
	if($response['exOrder'] === true){
		echo json_encode(array("success"," Order $orderid marked as lost, $response[message] "));
		exit;
	}
	echo json_encode(array("success","Order $orderid marked as lost and refund issued "));
	exit;
}

function cancel_lost_orders($orderid, $lost_reason){
	$cancel_args = array(
			'cancellation_type'                     => 'LOC',
			'reason'                                => "$lost_reason",
			'generateGoodWillCoupons'               => false,
			'is_oh_cancellation'                    => false,
			'orderid2fullcancelordermap' 			=> array($orderid=>false)
	);
	return func_change_order_status($orderid, 'L','System Script', "Marking the order as Lost", '', false, '', $cancel_args);
}
$cancellation_codes = func_query("select cancel_reason, cancel_reason_desc from cancellation_codes where cancel_type = 'LOC'", true);		
$smarty->assign("cancellation_codes", json_encode($cancellation_codes));

$smarty->assign("action",$action);
$smarty->assign("main",'cancel-lost-orders');
func_display("admin/home.tpl",$smarty);
?>