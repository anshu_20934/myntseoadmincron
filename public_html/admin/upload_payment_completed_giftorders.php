<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.order.php";
require_once($xcart_dir."/modules/giftcards/GiftCardsHelper.php");
include_once("../include/func/func.mkcore.php");
include_once("../include/func/func.mk_orderbook.php");
include_once("../include/func/func_sku.php");
include_once("../include/func/func.mkspecialoffer.php");
include_once("../include/func/func.inventory.php");
include_once("../include/func/func.mail.php");
include_once("$xcart_dir/include/class/oms/OrderWHManager.php");
require_once $xcart_dir."/include/class/class.mail.multiprovidermail.php";
include_once("$xcart_dir/env/Notifications.Config.php");
include_once $xcart_dir."/include/func/func.db.php";
include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");

$action  = $HTTP_POST_VARS['action'];
$uploadStatus = false;
if($action == '')
	$action ='upload';
elseif($action == 'verify'){
	if(!isset($_FILES) && isset($HTTP_POST_FILES))
		$_FILES = $HTTP_POST_FILES;
	if(!empty($_FILES['orderdetail']['name']))
	{
  		$uploaddir1 = '../bulkqueuedorders/';
		$extension = explode(".",basename($_FILES['orderdetail']['name']));
		$uploadfile1 = $uploaddir1 . date('d-m-Y-h-i-s').".".$extension[1];
		if (move_uploaded_file($_FILES['orderdetail']['tmp_name'], $uploadfile1)) {
		     	 $uploadStatus = true;
		}
	    if($uploadStatus) {
		// open the text file
		    $fd = fopen ($uploadfile1, "r");
		    $orderListDisplay =  array();
		    $failedOrderListDisplay = array();
		    
		    $dataFormatError = "Data Format Error: uploaded data does not confirm to either CCAvenue - {{cell-A: orderid, cell-E: amount}} or EBS - {{ cell-A: orderid, cell-B: amount}} format";
			$line = 0;
			while (!feof ($fd)) {
				$buffer = fgetcsv($fd, 4096);
				$line = $line + 1;
				
				if (empty($buffer[0])) {
					break;
				}
				if (!preg_match('/^\d+$/', $buffer[0])) {
					$smarty->assign('dataFormatError', "Invalid 'orderid' at line# $line");		
					break;	
				}
				$fieldCount = count($buffer);
				$csvOrderID = null;
				$csvAmount  = null;
				if ($fieldCount == 2) {
					$csvOrderID = $buffer[0];
					$csvAmount  = $buffer[1];
				} else if ($fieldCount >= 5) {
					$csvOrderID = $buffer[0];
					$csvAmount  = $buffer[4];	
				} else {
					$smarty->assign('dataFormatError', "$dataFormatError at line# $line");
					break;
				}
				
				if (! preg_match('/^([-+])?\d+(.\d+)?$/', $csvAmount)) {
					$smarty->assign('dataFormatError', "Invalid 'amount' at line# $line");
					break;
				}	

				$giftOrderSql = "Select gift_card_id, status, total from gift_cards_orders where orderid = $csvOrderID";
				$row = func_query_first($giftOrderSql);
				if($row && $row['status'] == 0 && $row['total'] == $csvAmount) {
					$orderListDisplay[] = $csvOrderID;
				} else if ($row && $row['status'] == 0 && $row['total'] != $csvAmount) {
					$failedOrderListDisplay[] = array('orderid' => $csvOrderID, 'msg' => "Amount did not match");
				}else if($row && $row['status'] == 1) {
					$failedOrderListDisplay[] = array('orderid' => $csvOrderID, 'msg'=>'Gift Order Already Completed');
				} else {
					$sql = "select status,cancellation_code from xcart_orders where orderid='$csvOrderID' ";
					$result = func_query_first($sql);
					if($result) {
						$failedOrderListDisplay[] = array('orderid' => $csvOrderID, 'msg'=>'Regular Orders cannot be completed from here');
					} else {
						$failedOrderListDisplay[] = array('orderid' => $csvOrderID, 'msg'=>'Invalid OrderId');
					}
				}					
			}
			fclose ($fd);
			$smarty->assign('orderListDisplay',$orderListDisplay);
			$smarty->assign('failedOrderListDisplay', $failedOrderListDisplay);
			$smarty->assign('uploadorderFile',$uploadfile1);
		} else {
			$action ='upload';
			$smarty->assign("errorMsg", 'Failed to upload file.');
		}
	}
}
elseif($action == 'confirm')
{
    $content = '<div align="center" style="width:100%"><div>Run by: '.$login.'</div><br/>';
    $content .=  '<h3 align="center">Orders Queued</h3>'; 

    $queuedOrdersContent = '<table width="80%" border="1"><tr align="left" style="background-color:bisque;"><th>S.No.</th><th>Order Id</th><th>Previous Status</th><th>Current Status</th></tr>';
    $failedOrdersContent = '<table width="40%" border="1"><tr align="left" style="background-color:bisque;"><th>S.No.</th><th>Order Id</th><th>Reason</th></tr>';
    $i = 1;
    $j = 1;
    $k = 1;
    $queuedOrders = array();	
    $failedorderids = array();
    foreach ($orderids as $orderid){
        if( preg_match('/^\d+$/', $orderid)){
        	$order_info = func_query_first("select gift_card_id, status from gift_cards_orders where orderid = $orderid");
			$wasSuccess = true;
			$result = \GiftCardsHelper::setGiftCardOrderComplete($order_info['gift_card_id']);
			if($result->status != "failed"){
				$commentArrayToInsert = Array ("orderid" => $orderid,
						"commenttype" => "Automated",
						"commenttitle" => "Gift Order Completed",
						"commentaddedby" => $login,
						"description" => "The Order is completed for Gift card id ".$order_info['gift_card_id'],
						"commentdate" => time());
				func_array2insert("mk_ordercommentslog", $commentArrayToInsert);
			} else {
				$errorMsg = $result->message;
				$wasSuccess = false;
			}
			
			if($wasSuccess) {
				$queuedOrdersContent .= ' <tr align="left"> <td> ' . $i++ . ' </td> <td>' . $orderid . '</td> <td>' . $order_info['status'] . '</td> <td> Complete </td> </tr> ';
				$queuedOrders[] = $orderid;
			} else {
				$failedOrdersContent .= "<tr align='left'><td>".$k++."</td><td>$orderid</td><td>$errorMsg</td></tr>";
				$failedorderids[] = $orderid;
			}
	    }
    }
	
    $content .= $queuedOrdersContent. '</table><br/><h3 align="center">Orders not queued</h3>';
    $content .= $failedOrdersContent . '</table>'; 
    $content .= "</div>";
    
    if (!empty($queuedOrders)) {
		$smarty->assign("googleEcommerceTracking", 1);
		$smarty->assign("ga_acc", $ga_acc);
    }
    $smarty->assign("summary", $content);
    $action = 'upload';
}
$smarty->assign("action",$action);
$smarty->assign("main",'upload_payment_completed_giftorders');
func_display("admin/home.tpl",$smarty);

?>