<?php
require_once "../auth.php";
require_once "$xcart_dir/include/security.php";
include_once "$xcart_dir/include/func/funcStyleProperties.php";
require_once "styleAttributeId.php";

$uniqueId = uniqid();
$smarty->assign('progresskey', $uniqueId);

//get brand, age-group, gender, fashion type, colour, season, year, usage
$brand       = getAttributeTypeValueArray(ATTRIBUTE_BRAND_ID);
$ageGroup    = getAttributeTypeValueArray(ATTRIBUTE_AGE_GROUP_ID);
$gender      = getAttributeTypeValueArray(ATTRIBUTE_GENDER_ID);
$fashionType = getAttributeTypeValueArray(ATTRIBUTE_FASHION_TYPE_ID);
$colour      = getAttributeTypeValueArray(ATTRIBUTE_COLOUR_ID);
$season      = getAttributeTypeValueArray(ATTRIBUTE_SEASON_ID);
$year        = getAttributeTypeValueArray(ATTRIBUTE_YEAR_ID);
$usage       = getAttributeTypeValueArray(ATTRIBUTE_USAGE_ID);

//get different categories
$masterCategory = getCatalogCategory("MasterCategory");
$subCategory    = getCatalogCategory("SubCategory");
$articleType    = getCatalogCategory("ArticleType");

//get all style status
$styleStatus = getStyleStatus();
$smarty->assign("styleStatus", $styleStatus);

$smarty->assign("brands",         $brand);
$smarty->assign("ageGroups",      $ageGroup);
$smarty->assign("genders",        $gender);
$smarty->assign("fashionTypes",   $fashionType);
$smarty->assign("colours",        $colour);
$smarty->assign("seasons",        $season);
$smarty->assign("years",          $year);
$smarty->assign("usages",         $usage);
$smarty->assign("masterCategory", $masterCategory);
$smarty->assign("subCategory",    $subCategory);
$smarty->assign("articleType",    $articleType);

$smarty->assign("main","search_styles");
func_display("admin/home.tpl",$smarty);
?>