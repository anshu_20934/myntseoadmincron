<?php
require_once "../auth.php";
require_once "$xcart_dir/include/security.php";
include_once "$xcart_dir/include/func/funcStyleProperties.php";
require_once "styleAttributeId.php";

/**
 * this function returns an array of array of styles with key being article type id
 * @param comma separated $styleIdList
 */
function getStyleArrayArticleIdKey($styleIdList)
{
	$selSql = "SELECT style_id, global_attr_article_type AS article_type_id FROM mk_style_properties WHERE style_id IN ($styleIdList) ORDER BY global_attr_article_type ASC";
	$resultArr = func_query($selSql, TRUE);
	$pairArr = array();
	foreach($resultArr as $row)
	{
		$pairArr[$row['article_type_id']][] = $row['style_id'];
	}
	return $pairArr;
}

/**
 * @param array $articles an array of article ids
 * @return an array of article id with its name
 */
function getArticleTypeName($articles)
{
	$articleIdList = implode(",", $articles);
	$selSql = "SELECT id, typename AS name FROM mk_catalogue_classification WHERE id IN ($articleIdList)";
	return func_query($selSql, TRUE);
}

/**
 * if style ids is not provided redirect to style search page
 * $style_ids - comma separated style ids
 */
if(empty($style_ids))
{
	func_header_location($http_location . "/admin/catalog/search_styles.php");
}
else
{
	$styleArticleGroup = getStyleArrayArticleIdKey($style_ids);
	$smarty->assign("style_ids", $style_ids);
	$articleIds = array_keys($styleArticleGroup);
	$smarty->assign("articleIds", getArticleTypeName($articleIds));
	if(empty($aid) || !(in_array($aid, $articleIds)))
	{
		$articleTypeId = $articleIds[0];
	}
	else
	{
		$articleTypeId = $aid;
	}
	$smarty->assign("articleTypeId", $articleTypeId);
	$styleIdArray = $styleArticleGroup[$articleTypeId];//array of styles in the selected article typs id;
	$styleSpecificDetails = getSpecificAttributes($styleIdArray);//specific attribute
	$styleCompleteData = getStyleCompleteDetails($styleIdArray); //complete data
	$styleClassificationBrand = getStyleClassificationBrand($styleIdArray);
	$styleAll = array();
	foreach($styleCompleteData as $key=>$style)
	{
		$style['content_copy_style'] = $style['style_id'];
		$styleAll[$key] = array_merge($style, $styleSpecificDetails[$key], $styleClassificationBrand[$key]);
	}
	$styleColKeys = array_keys($styleAll[0]);
	
	$stylePropertyName = array('style_id' => 'Style Id', 'style_status' => 'Status', 'vendor_article_no' => 'Vendor Article No',
			'vendor_article_name' => 'Vendor Article Name', 'article_type_id' => 'Article Type', 'brand' => 'Brand', 'classification_brand' => 'Classification Brand',
			'age_group' => 'Age Group', 'gender' => 'Gender', 'base_colour' => 'Base Colour', 'colour1' => 'Colour 1',
			'colour2' => 'Colour 2', 'fashion_type' => 'Fashion Type', 'season' => 'Season', 'year' => 'Year',
			'usage' => 'Usage', 'product_display_name'  => 'Product Display Name', 'tags' => 'Tags', 'price' => 'Price (Rs.)',
			'weight' => 'Weight (Gms)', 'comments'=>'Comments', 'product_type' => 'Product Type');
	$smarty->assign("stylePropertyName", json_encode($stylePropertyName));

	$styleIdJson = array();
	$styleIndex = 0;
	foreach($styleIdArray as $stId)
	{
		$styleIdJson[] = array('id'=>"$styleIndex", 'name'=>"$stId");
		$styleIndex++;
	}
	
	$smarty->assign("styleIdList", json_encode($styleIdJson));
	$smarty->assign("styleAll", json_encode($styleAll));
	$smarty->assign("styleColKeys", json_encode($styleColKeys));
	
	/**
	 * generic data for any style
	 */
	//get brand, age-group, gender, fashion type, colour, season, year, usage
	$brand       = getAttributeTypeValueArray(ATTRIBUTE_BRAND_ID);
	$ageGroup    = getAttributeTypeValueArray(ATTRIBUTE_AGE_GROUP_ID);
	$gender      = getAttributeTypeValueArray(ATTRIBUTE_GENDER_ID);
	$fashionType = getAttributeTypeValueArray(ATTRIBUTE_FASHION_TYPE_ID);
	$colour      = getAttributeTypeValueArray(ATTRIBUTE_COLOUR_ID);
	$season      = getAttributeTypeValueArray(ATTRIBUTE_SEASON_ID);
	$year        = getAttributeTypeValueArray(ATTRIBUTE_YEAR_ID);
	$usage       = getAttributeTypeValueArray(ATTRIBUTE_USAGE_ID);
	
	$classificationBrand = getClassificationBrand();
	//all specific attributes corresponding to the article type
	$specificAttribute = getArticleAttributeTypeWithValues($articleTypeId);
	if(empty($specificAttribute))
	{
		$smarty->assign("specificAttributeExist", 'false');
	}
	else
	{
		$smarty->assign("specificAttributeExist", 'true');
		$smarty->assign("specificAttribute", $specificAttribute);
		$specificAttributeList = json_decode($specificAttribute,true);
		$specificAttributeName = array();
		foreach($specificAttributeList as $specificAttributeElement)
		{
			$specificAttributeName[] = $specificAttributeElement['name'];
		}
		$smarty->assign("specificAttributeName", json_encode($specificAttributeName));
	}
		
	//get different categories
	$masterCategory = getCatalogCategory("MasterCategory");
	$subCategory    = getCatalogCategory("SubCategory");
	$articleType    = getCatalogCategory("ArticleType");
	$productType    = getProductTypes();
	
	/**
	 * style status related stuff
	 */
	//get all style status
	$styleStatus = getStyleStatus();
	$smarty->assign("styleStatus", $styleStatus);
	//get possible status list
	$possibleStatusTrans = getPossibleStatusTransition();
	$smarty->assign("possibleStatusTrans", $possibleStatusTrans);
	
	$smarty->assign("brand",          $brand);
	$smarty->assign("ageGroup",       $ageGroup);
	$smarty->assign("gender",         $gender);
	$smarty->assign("fashionType",    $fashionType);
	$smarty->assign("colour",         $colour);
	$smarty->assign("season",         $season);
	$smarty->assign("year",           $year);
	$smarty->assign("usage",          $usage);
	$smarty->assign("masterCategory", $masterCategory);
	$smarty->assign("subCategory",    $subCategory);
	$smarty->assign("articleType",    $articleType);
	$smarty->assign("productType",    $productType);
	
	$smarty->assign("classificationBrand", $classificationBrand);
	$smarty->assign("main","style_bulk_update");
	func_display("admin/home.tpl",$smarty);
}
?>