<?php
require_once "../auth.php";
require_once "$xcart_dir/include/security.php";
include_once "$xcart_dir/include/func/funcStyleProperties.php";

if(empty($style_ids))
{
	func_header_location($http_location . "/admin/catalog/search_styles.php");
}
else
{
	$smarty->assign("style_ids", $style_ids);
	$styleIdArray = explode(",", $style_ids);
	asort($styleIdArray);
	$styleIdJson = array();
	foreach ($styleIdArray as $styleId)
	{
		$styleIdJson[] = array('id'=>$styleId,'name'=>$styleId);
	}
	$smarty->assign("styleId", json_encode($styleIdJson));
	
	//get all style status
	$styleStatus = getStyleStatus();
	$smarty->assign("styleStatus", $styleStatus);
	//get possible status list
	$possibleStatusTrans = getPossibleStatusTransition();
	$smarty->assign("possibleStatusTrans", $possibleStatusTrans);
		
	$smarty->assign("main","style_content_update");
	func_display("admin/home.tpl",$smarty);
}
?>