<?php
require "../auth.php";
require "$xcart_dir/include/security.php";
require "$xcart_dir/include/SimpleImage.php";
include_once "$xcart_dir/include/func/func.image.php";
include_once "$xcart_dir/utils/imagetransferS3.php";
include_once "$xcart_dir/modules/apiclient/AuditApiClient.php";
include_once "$xcart_dir/include/solr/solrProducts.php";
include_once "$xcart_dir/include/solr/solrUpdate.php";
include_once "$xcart_dir/modules/cms/CMSMigrationClient.php";

use imageUtils\Jpegmini;

$expiryTime = FeatureGateKeyValuePairs::getFeatureGateValueForKey('styleImageUploadExpiryTime', -1);	
$imageUploadMaxSize = FeatureGateKeyValuePairs::getInteger('styleImageUploadMaxSize', 300);
$currTimeMilliseconds = round(microtime(true) * 1000);
echo 'In feature gate expiry time is : '.$imageUploadMaxSize;
echo 'currenttime is : '.$currTimeMilliseconds;
if($currTimeMilliseconds >= $expiryTime)
{	
	header("Location: deprecatedMessagePage.php"); /* Redirect browser */
	exit();
}

if(($REQUEST_METHOD == "POST") && ($_POST['mode'] == 'uploadImages'))
{
	$login = $XCART_SESSION_VARS['identifiers']['A']['login'];
	
	//verify style id
	$styleId = mysql_real_escape_string(filter_input(INPUT_POST, 'style_id'));
	$validStyle = func_query("SELECT * FROM mk_style_properties WHERE style_id ='$styleId'", TRUE);
	if(empty($validStyle))
	{
		$errorMessage = "$styleId is not a valid style id.";
		$smarty->assign("errorMessage",$errorMessage);
	}
	else 
	{
		if(!isset($_FILES) && isset($HTTP_POST_FILES))
		{
			$_FILES = $HTTP_POST_FILES;
		}
		if(isset($_FILES) && !empty($_FILES))
		{
			$errorMessage ='';
			//check and validate image type, size and presence of default images
			$imagesExceedingSizeLimit = '';
			$fileNotImage = '';
			$imageFiles = $_FILES['styleImagefile']['name'];
			$imageFileLocations = $_FILES['styleImagefile']['tmp_name'];
			$defaultImageExist = FALSE;
			foreach($imageFiles as $index=>$file)
			{
				$file = strtolower($file);
				$fileExt = substr($file, -4);
                $location = $imageFileLocations[$index];
                $image = new SimpleImage();
                $image->load($location);
				if( ($fileExt != ".jpg") || ( !$image->isJpeg()) )
				{
					$fileNotImage .= ', '.$file;
				}
				elseif($_FILES['styleImagefile']['size'][$index] > 1024*$imageUploadMaxSize)
				{
					$imagesExceedingSizeLimit .= ', '.$file;
				}
				else
				{
					$defaultImageExist = TRUE;
				}
                if ( ($image->getWidth()/$image->getHeight()) != 0.75 ) {
                    $incorrectAspectRatioImages .= ",".$file;
                }
			}
			if($imagesExceedingSizeLimit != '')
			{
				$errorMessage .= "Files ".substr($imagesExceedingSizeLimit, 1)."  exceed size limit of $imageUploadMaxSize KB.";
			}
			if($incorrectAspectRatioImages != '')
			{
				$errorMessage .= "Files ".substr($incorrectAspectRatioImages, 1)."  aspect ratio of image(s) is not 3:4.";
			}
			if($fileNotImage != '')
			{
				$errorMessage .= (($errorMessage == '')?"":"<br/> ")."Files ".substr($fileNotImage, 1)."  not jpeg image(s).";
			}
			if(!$defaultImageExist)
			{
				$errorMessage .= (($errorMessage == '')?"":"<br/> ")."Default image does not exist.";
			}
		}
		else
		{
			//no image uploaded
			$errorMessage = "Please select and upload proper images.";
		}
		
		if($errorMessage != '')
		{
			$errorMessage .= "<br/>No images uploaded please correct all the above errors.";
			$smarty->assign("errorMessage",$errorMessage);
		}
		else
		{
			//if no errors then upload all images which are sorted naturally
			$tempSql = "select product_display_name from mk_style_properties where style_id = $styleId";
			$displayName = func_query_first_cell($tempSql, TRUE);
			if(empty($displayName))
			{
				$tempSql = "select global_attr_brand as brand, global_attr_gender as gender, global_attr_article_type as article_id from mk_style_properties where style_id = '$styleId'";
				$styleProperty = func_query_first($tempSql, TRUE);
				$displayName = $styleProperty['brand']." ".$styleProperty['gender']." ";
				$tempSql = "select typename from mk_catalogue_classification where id = '".$styleProperty['article_id']."'";
				$displayName .= func_query_first_cell($tempSql, TRUE);
			}
			$displayName = preg_replace("/[\s]+/", "-", trim($displayName));
			$displayName = preg_replace("/[^a-zA-Z0-9-_&]/","", $displayName);
			$displayName = preg_replace("/\./","", $displayName);
			$imagePrefix = $displayName."_";
			
			$imageFiles = $_FILES['styleImagefile']['name'];
			$imageFilesWOExt = str_ireplace(".jpg", "", $imageFiles);
			natsort($imageFilesWOExt);
			$imageUploadUrl = array( 'default_image' => '', 'front_image' => '', 'back_image' => '','right_image' => '', 'left_image' => '', 'top_image' => '', 'bottom_image' => '');
            $imageUploadUrlKeys = array_keys($imageUploadUrl);
			$count = 0;
			foreach($imageFilesWOExt as $key=>$value)
			{
				$value = strtolower($value);
				if($count == 0)
				{
					$imageType = "default";
					$imageUrlkey = "default_image"; 
				}
				else
				{
					$imageType = "others";
					$imageUrlkey = $imageUploadUrlKeys[$count];
				}
				$imagePath = $_FILES['styleImagefile']['tmp_name'][$key];
				$imageName = $_FILES['styleImagefile']['name'][$key];
				$imageUrl = func_resize_backup_uploaded_product_image($imagePath, $imageName, $imageType, true, $imagePrefix);
				$imageUploadUrl[$imageUrlkey] = $imageUrl;
				$count ++;
			}
			
			$errorUploadingImages = false;
			foreach($imageUploadUrl as $url)
			{
				if($url === null)
				{
					$errorUploadingImages = TRUE;
					break;
				}
			}
			
			if($errorUploadingImages)
			{
				$errorMessage = "Error while processing one or more uploaded images for $url";
				$smarty->assign("errorMessage",$errorMessage);
			}
			else
			{
				$message = "Uploaded Images successfully for style: $styleId";
				$defaultImages = explode(",", $imageUploadUrl["default_image"]);
				$imageUploadUrl["default_image"] = $defaultImages[0];
				$imageUploadUrl["search_image"] = $defaultImages[2];
				$imageUploadUrl["search_zoom_image"] = $defaultImages[1];
				//make the image entry in database
				global $sqllog;
				$sqllog->debug("User: $login ; Style id = $styleId");
				$sqllog->debug($imageUploadUrl);
				func_array2updateWithTypeCheck("mk_style_properties", $imageUploadUrl, "style_id = $styleId");
				//Calling CMS Client for remigration
				$cmsMigrationResponse = CMSMigrationClient::bulkRemigrate(array($styleId));
				if(!$cmsMigrationResponse){
					$message .= " but migration to cms failed. Please report to cms team.";
				}
				$uploadedImages = '';
				$imageTypeArray = array('default_image' => 'Default Image', 'search_image' => 'Search Image', 'search_zoom_image' => 'Search Zoom Image', 'front_image' => 'Front Image',
						'back_image' => 'Back Image', 'right_image' => 'Right Image', 'left_image' => 'Left Image', 'top_image' => 'Top Image', 'bottom_image' => 'Bottom Image');
				foreach($imageUploadUrl as $key=>$value)
				{
					if($value != '')
					{
						$uploadedImages .= $imageTypeArray[$key].", ";
					}
				}
				$uploadedImages = substr($uploadedImages, 0, -2);
				$styleImageAudit = array(
						"entityId" => $styleId,
						"entityName" => 'com.myntra.StyleProperties',
						"entityProperty" => 'Image Upload',
						"entityPropertyOldValue" => 'Image Upload',
						"entityPropertyNewValue" => "Uploaded $uploadedImages images",
						"operationType" => 'Image Upload');
				
				$styleAuditData = array($styleImageAudit);
				$auditResult = AuditApiClient::sendStyleAuditInfo($styleAuditData, $login);
				//add/delete style to solr
				$styleStatus = func_query_first_cell("SELECT styletype FROM mk_product_style WHERE id = '$styleId'", TRUE);
				if($styleStatus == 'P')
				{
					addStyleToSolrIndex($styleId,false);
				}
				else
				{
					deleteStyleFromSolrIndex($styleId,false);
				}
				$smarty->assign("message",$message);
			}
		}
	}
}
$smarty->assign("main","styleUploadImages");
func_display("admin/home.tpl",$smarty);
?>
