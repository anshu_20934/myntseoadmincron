<?php
/**
 * Created by Lohiya.
 * Date: 08 Jul, 2011
 */
require_once( "../auth.php");
require_once $xcart_dir."/include/security.php";
define('FUNC_SKU_CALL', 'Y');
@include ($xcart_dir."/include/func/func.filter_order.php");
@include ($xcart_dir."/include/func/func.displayCategory.php");
include_once($xcart_dir."/include/solr/solrProducts.php");
include_once($xcart_dir."/include/solr/solrUpdate.php");

define('FORM_ACTION','display_categories.php');

if($REQUEST_METHOD == "POST"){
	switch($_POST['mode']){
		############## search ############
		case  "search" :
			$sql = "select * from mk_display_categories" ;
			$display_category_search_name = $_POST['display_category_search_name'];
			if(!empty($display_category_search_name)){
				$sql.=" where lower(name) like lower('%".$display_category_search_name."%')";
			}
			$display_categories = func_query($sql);
			$smarty->assign("display_categories_search_results",$display_categories);
			//echo "<pre>";print_r($display_categories);exit;
			break;
		case  "run" :
			updateDisplayCategory($_POST['id']);
			$sql = "select * from mk_display_categories order by id desc" ;
			$display_categories = func_query($sql);
			$smarty->assign("display_categories_search_results",$display_categories);
			$smarty->assign("message","finished running display category");
			break;
		case "delete":
			//delete the reocrd
			update_filter_order("mk_display_categories",$_POST['id'],"");
			update_display_category($_POST['id']);
			$sql = "delete from mk_display_categories where id='".$_POST['id']."'" ;
			db_query($sql);
			// now fetch the results
			$sql = "select * from mk_display_categories order by id desc" ;
			$display_categories = func_query($sql);
			$smarty->assign("display_categories_search_results",$display_categories);
			$smarty->assign("message","Deleted Display Category");
			break;	
	}
}else{
	$sql = "select * from mk_display_categories order by id desc" ;
	$display_categories = func_query($sql);
	$smarty->assign("display_categories_search_results",$display_categories);
}


$smarty->assign("main","display_categories");
# Assign the current location line
$smarty->assign("location", $location);
$smarty->assign("form_action", FORM_ACTION);
# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);
$smarty->assign ("top_message", $top_message);
@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
