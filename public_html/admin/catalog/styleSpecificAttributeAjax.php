<?php
require '../auth.php';
include_once "$xcart_dir/include/security.php";
include_once "$xcart_dir/include/func/funcStyleProperties.php";

$login=$XCART_SESSION_VARS['identifiers']['A']['login'];

//add new specific attribute
if(isset($_POST) && ($_POST["action"] == "saveSpecificAttributeType"))
{
	$result = array();
	$insertArray = array(
			'catalogue_classification_id' => $_POST['article_type_id'],
			'attribute_type' => $_POST['attribute_type'],
			'is_active' => $_POST['is_active'],
			'is_filter' => $_POST['is_filter'],
			'created_on' => time(),
			'created_by' => $login,
			'updated_on' => time(),
			'updated_by' => $login);
	$result['id'] = func_array2insertWithTypeCheck('mk_attribute_type', $insertArray);
	$result['added'] = true;
	echo json_encode($result);
	exit;
}

//update specific attribute type
if(isset($_POST) && ($_POST["action"] == "updateSpecificAttributeType"))
{
	$result = array();
	$rowId = $_POST['id'];
	$insertArray = array(
			'attribute_type' => $_POST['attribute_type'],
			'is_active' => $_POST['is_active'],
			'is_filter' => $_POST['is_filter'],
			'updated_on' => time(),
			'updated_by' => $login);
	func_array2updateWithTypeCheck('mk_attribute_type', $insertArray, "id = $rowId");
	$result['id'] = $rowId;
	$result['updated'] = true;
	echo json_encode($result);
	exit;
}

//add/upate new specific attribute type values
if(isset($_POST) && ($_POST["action"] == "saveSpecificAttributes"))
{
	$validateResult = array();
	$attributeTypeId = $_POST['attribute_type_id'];
	$maxFilterOrder = 1 + func_query_first_cell("select max(filter_order) from mk_attribute_type_values where attribute_type_id = '$attributeTypeId'", TRUE);
	$result = array();
	$result['updated'] = false;
	$result['speAttUpdated'] = '';
	$result['added'] = false;
	$result['speAttAdded'] = '';
	foreach($_POST as $key=>$value)
	{
		if($key != "action")
		{
			$specificAttribute = get_object_vars(json_decode($value));
			if(!empty($specificAttribute['id']))
			{
				$updateArray = array(
						'attribute_value' => $specificAttribute['name'],
						'is_active' => $specificAttribute['is_active'],
						'updated_on' => time(),
						'updated_by' => $login);
				func_array2updateWithTypeCheck('mk_attribute_type_values', $updateArray, 'id = '.$specificAttribute['id']);
				$result['speAttUpdated'] .= $specificAttribute['id'].' ';
				$result['updated'] = true;
			}
			elseif($specificAttribute['isNew'])
			{
				$insertArray = array(
						'attribute_value' => $specificAttribute['name'],
						'is_active' => $specificAttribute['is_active'],
						'attribute_type_id' => $attributeTypeId,
						'filter_order' => $maxFilterOrder,
						'created_on' => time(),
						'created_by' => $login,
						'updated_on' => time(),
						'updated_by' => $login);
				$result['speAttAdded'] .= func_array2insertWithTypeCheck('mk_attribute_type_values', $insertArray).' ';
				$result['added'] = true;
				$maxFilterOrder += 1;
			}
		}
	}
	echo json_encode($result);
	exit;
}