<?php
require '../auth.php';
include_once "$xcart_dir/include/security.php";
include_once "$xcart_dir/include/func/funcStyleProperties.php";
include_once "$xcart_dir/include/func/func.utilities.php";

define("FUNC_SKU_CALL",'Y');
include_once "$xcart_dir/include/solr/solrProducts.php";
include_once "$xcart_dir/include/solr/solrUpdate.php";
include_once "$xcart_dir/modules/apiclient/AuditApiClient.php";

$login=$XCART_SESSION_VARS['identifiers']['A']['login'];

//changes article type, master, sub category and remove specific attributes
if(isset($_POST) && ($_POST["action"] == "changeArticleType"))
{
	$styleId = $_POST['styleId'];
	$articleTypeId = $_POST['articleTypeId'];
	$articleCategory = func_query("SELECT parent1, parent2 FROM mk_catalogue_classification WHERE id = $articleTypeId", TRUE);
	$changeArticleArray = array(
			'global_attr_article_type' => $articleTypeId,
			'global_attr_sub_category' => $articleCategory[0]['parent1'],
			'global_attr_master_category' => $articleCategory[0]['parent2']
			);
	$result = array();
	global $sqllog;
	$sqllog->debug("User: $login ; Style id = $styleId");
	$sqllog->debug($changeArticleArray);
	$result['success'] = func_array2updateWithTypeCheck('mk_style_properties', $changeArticleArray, array("style_id"=>$styleId));
	
	$delectSpeAttrSQL = "DELETE FROM mk_style_article_type_attribute_values WHERE product_style_id = $styleId";
	$result['speattr'] = func_query($delectSpeAttrSQL);
	echo json_encode($result);
	exit;
}

if(isset($_POST) && ($_POST["action"] == "copyStyle"))
{
	$styleId = $_POST['styleId'];
	$styleIdArray = array($styleId);
	//first verify that style ids are of same article types
	$articleTypeId = $_POST['articleTypeId'];
	$copyStyleArticleType = func_query_first_cell("SELECT global_attr_article_type FROM mk_style_properties WHERE style_id = '$styleId'", TRUE);
	if($articleTypeId != $copyStyleArticleType)
	{
		$response['success'] = false;
		echo json_encode($response);
		exit;
	}
	$response['success'] = true;
	$styleSpecificDetails = getSpecificAttributes($styleIdArray);//specific attribute
	$styleCompleteData = getStyleCompleteDetails($styleIdArray); //complete data
	
	$styleAll = array();
	foreach($styleCompleteData as $key=>$style)
	{
		$styleAll[$key] = array_merge($style, $styleSpecificDetails[$key]);
	}
	$response['style'] = $styleAll;
	echo json_encode($response);
	exit;
}

/**
 * For saving the style in DB
 */
if(isset($_POST) && ($_POST["action"] == "saveStyle"))
{
	$validateResult = array();
	$styleAuditData = array();
	foreach($_POST as $key=>$styleVal)
	{
		if($key != "action")
		{
			$style = get_object_vars(json_decode($styleVal));
			array_walk($style, 'trim_value');
			$validateResult[$style['style_id']] = validateStyle($style);
			//if valid data then update the db
			$isValidStyle = $validateResult[$style['style_id']]['valid']; 
			if($isValidStyle)
			{
				$modifiedTime = time();
				
				//update mk_style_properties
				$updateStyleProperties = array(
						"article_number" => $style['vendor_article_no'],
						"product_tags" => $style['tags'],
						"product_display_name" => $style['product_display_name'],
						"variant_name" => $style['vendor_article_name'],
						"modified_date" => $modifiedTime,
						"global_attr_brand" => $style['brand'],
						"global_attr_age_group" => $style['age_group'],
						"global_attr_gender" => $style['gender'],
						"global_attr_base_colour" => $style['base_colour'],
						"global_attr_colour1" => $style['colour1'],
						"global_attr_colour2" => $style['colour2'],
						"global_attr_fashion_type" => $style['fashion_type'],
						"global_attr_season" => $style['season'],
						"global_attr_year" => $style['year'],
						"global_attr_usage" => $style['usage'],
						"weight" => $style['weight'],
						"comments" => $style['comments']
				);
				//check catalog date
				$styleId = $style['style_id'];
				$styleStatus = $style['style_status'];
				if($styleStatus == 'P')
				{
					//check if catalog date is null
					$catalog_date = func_query_first_cell("select catalog_date from mk_style_properties where style_id = $styleId", TRUE);
					if(is_null($catalog_date))
					{
						$updateStyleProperties["catalog_date"] = $modifiedTime;
					}
				}
				
				//update mk_product_style
				$updateProductStyle = array(
						"product_type" => $style['product_type'],
						"name" => $style['product_display_name'],
						"label" => $style['product_display_name'],
						"price" => $style['price'],
						"styletype" => $styleStatus
				);
				
				//check if content need to copy from other style
				if($style['content_copy_style'] != $styleId)
				{
					$copyFromStyle = $style['content_copy_style'];
					$styleContent = getStyleContent(array($copyFromStyle));
					//style properties
					$updateStyleProperties['style_note'] = $styleContent[0]['style_note'];
					$updateStyleProperties['materials_care_desc'] = $styleContent[0]['materials_care_desc'];
					$updateStyleProperties['size_fit_desc'] = $styleContent[0]['size_fit_desc'];
					//product style
					$updateProductStyle['description'] = $styleContent[0]['description'];
				}
				
				//Audit style's price, status and comments before updating db
				$productStyle = func_query("select styletype, price from mk_product_style where id = '$styleId'", true);
				$oldPrice = $productStyle[0]['price'];
				$stylePriceAudit = array(
						"entityId" => $styleId,
						"entityName" => 'com.myntra.StyleProperties',
						"entityProperty" => 'Price',
						"entityPropertyOldValue" => $oldPrice,
						"entityPropertyNewValue" => $style['price'],
						"operationType" => 'UPDATE');
				
				$oldStatusCode = $productStyle[0]['styletype'];
				$oldStatus = func_query_first_cell("select style_status_name_to from style_status_transition_rules where style_status_code_to = '$oldStatusCode'", true);
				$newStatus = func_query_first_cell("select style_status_name_to from style_status_transition_rules where style_status_code_to = '$styleStatus'", true);
				$styleStatusAudit = array(
						"entityId" => $styleId,
						"entityName" => 'com.myntra.StyleProperties',
						"entityProperty" => 'Status',
						"entityPropertyOldValue" => $oldStatus,
						"entityPropertyNewValue" => $newStatus,
						"operationType" => 'UPDATE');
				$oldComments = func_query_first_cell("select comments from mk_style_properties where style_id = '$styleId'", true);
				$styleCommentsAudit = array(
						"entityId" => $styleId,
						"entityName" => 'com.myntra.StyleProperties',
						"entityProperty" => 'Comments',
						"entityPropertyOldValue" => $oldComments,
						"entityPropertyNewValue" => $style['comments'],
						"operationType" => 'UPDATE');
												
				global $sqllog;
				$sqllog->debug("User: $login ; Style id = $styleId");
				$sqllog->debug($updateStyleProperties);
				//send db queries
				func_array2updateWithTypeCheck("mk_style_properties", $updateStyleProperties, "style_id='$styleId'");
				func_array2updateWithTypeCheck("mk_product_style", $updateProductStyle, "id='$styleId'");
				
				$styleAuditData[] = $stylePriceAudit;
				$styleAuditData[] = $styleStatusAudit;
				$styleAuditData[] = $styleCommentsAudit;
				
				//update the classification brand
				$appliedClassificationBrand = $style['classification_brand'];
				$checkClassificationBrand = func_query("SELECT * FROM mk_applied_filters WHERE generic_id='".$styleId."' AND filter_group_id='2'", TRUE);
				if(!empty($checkClassificationBrand[0]))
				{
					$filterData=array("applied_filters" => $appliedClassificationBrand);
					func_array2updateWithTypeCheck("mk_applied_filters", $filterData, "generic_id = '$styleId' AND filter_group_id ='2'");
				}
				else
				{
					$filterData=array(
							"generic_type"=>"style",
							"generic_id" => $styleId,
							"filter_group_id" =>2,
							"applied_filters" => $appliedClassificationBrand,
							"is_active" => 1
					);
					func_array2insertWithTypeCheck("mk_applied_filters",$filterData);
				}
				
				//update the specific properties
				//first delete the old style attributes
				$deleteSpeAttrSQL = "DELETE FROM mk_style_article_type_attribute_values WHERE product_style_id = '$styleId'";
				func_query($deleteSpeAttrSQL);
				
				//insert new specific attribute values
				$nonSpecificAttributesKey = array('style_id', 'style_status', 'vendor_article_no', 'vendor_article_name', 'brand', 'classification_brand', 'article_type_id', 'product_type',  
						'age_group', 'gender', 'base_colour', 'colour1', 'colour2', 'fashion_type', 'season', 'year', 'usage', 'price', 'weight', 'product_display_name', 'tags', 'comments', 'content_copy_style');
				foreach($style as $key=>$value)
				{
					if(!in_array($key, $nonSpecificAttributesKey))
					{
						$insertSpeAttr = array(
								'product_style_id'=> $styleId,
								'article_type_attribute_value_id'=>$value,
								'created_on'=>time(),
								'created_by'=>$login,
								'is_active'=>'1',
								'updated_on'=>time(),
								'updated_by'=>$login,
								);
						func_array2insertWithTypeCheck('mk_style_article_type_attribute_values', $insertSpeAttr);
					}
					
				}
				//add/delete style to solr index
				if($styleStatus == 'P')
				{
					addStyleToSolrIndex($styleId,false);
				}
				else
				{
					deleteStyleFromSolrIndex($styleId,false);
				}
			} 
			
		}
	}
	$auditResult = AuditApiClient::sendStyleAuditInfo($styleAuditData, $login,'com.myntra.admin.catalog.styleBulkUpdateAjax.php');
	echo json_encode($validateResult);
	exit;	
}

/**
 * for loading style images in style content page
 */
if(isset($_POST) && ($_POST["action"] == "getStyleImages"))
{
	$result['success'] = false;
	$styleId = $_POST['styleId'];
	if(!empty($styleId))
	{
		$query = "SELECT default_image, front_image, back_image, right_image, left_image, top_image, bottom_image FROM mk_style_properties WHERE style_id = '$styleId'";
		$styleImages = func_query_first($query, TRUE);
		if(!is_null($styleImages) && ($styleImages != ''))
		{
			$result['success'] = TRUE;
			$miniStyleImages = str_replace('.jpg', '_81_108.jpg', $styleImages);
			$htmlHead = '<table border="0"><tr>';
			$htmlTail = '</tr></table>';
			$html = '';
			$imageTypeArray = array('default_image' => 'Default Image', 'front_image' => 'Front Image', 'back_image' => 'Back Image', 'right_image' => 'Right Image', 'left_image' => 'Left Image', 'top_image' => 'Top Image', 'bottom_image' => 'Bottom Image');
			foreach($imageTypeArray as $key=>$value)
			{
				if(!is_null($styleImages[$key]) && trim($styleImages[$key]) != '')
				{
					$html .= '<td><h2>'.$value.'</h2><a href="'.$styleImages[$key].'" target="_blank"><img src="'.$miniStyleImages[$key].'" alt="'.$value.' Missing" width="81" height="108" /></a></td>';
				}
				else
				{
					$html .= '<td><h2>'.$value.'</h2><img src="'.$miniStyleImages[$key].'" alt="'.$value.' Missing" width="81" height="108" /></td>';
				}
			}			
			$imageHTML = $htmlHead.$html.$htmlTail;
			$result['html'] = $imageHTML;
		}
	}
	echo json_encode($result);
	exit;
}

/**
 * for changing style in content update screen
 */
if(isset($_POST) && ($_POST["action"] == "loadStyleContent"))
{
	$load2styleId = array($_POST['load2styleId']);
	$load4mstyleId = array($_POST['load4mstyleId']);
	$result = array();
	$result['styleId'] = $load2styleId;
	if(is_null($load2styleId))
	{
		$result['success'] = false;
		$result['error'] = 'Style id missing';
	}
	else
	{
		$styleExist = func_query("SELECT style_id FROM mk_style_properties WHERE style_id = '".$load4mstyleId[0]."'", TRUE);
		if(empty($styleExist))
		{
			$result['success'] = false;
			$result['error'] = 'Invalid Style Id';
		}
		else
		{
			$styleContent = getStyleContent($load4mstyleId);
			if($load2styleId[0] != $load4mstyleId[0])
			{
				$sqlStatus = "SELECT styletype FROM mk_product_style WHERE id = '$load2styleId[0]'";
				$styleStatus = func_query_first_cell($sqlStatus, TRUE);
				$styleContent[0]['status'] = $styleStatus;
			}
			$result['success'] = true;
			$result['styleContent'] = $styleContent;
			
			$styleAllProp = getDisplayStyleAllData($load2styleId[0]);
			$result['styleAttribute'] = $styleAllProp;
		}
	}
	echo json_encode($result);
	exit;
}

/**
 * for saving style contents in DB
 */
if(isset($_POST) && ($_POST["action"] == "saveContent"))
{
	$styleId = $_POST["style_id"];
	$validContent = validateStyleContent($_POST);
	if($validContent['valid'])
	{
		array_walk($_POST, 'trim_value');
		$insertContent = array(
				'style_note' => $_POST['style_note'],
				'materials_care_desc' => $_POST['materials_care_desc'],
				'size_fit_desc' => $_POST['size_fit_desc']
		);
		
		$insertContentStatus =  array('description' => $_POST['description']);
		$updateStyleStatus = false;
		if(isset($_POST['styletype']) && !empty($_POST['styletype']))
		{
			$insertContentStatus['styletype'] = $_POST['styletype'];
			$newStatusCode = $insertContentStatus['styletype'];
			$updateStyleStatus = true;
			//if style is made active 1st time update catalog date
			if($newStatusCode == 'P')
			{
				//check if catalog date is null
				$catalogDate = func_query_first_cell("select catalog_date from mk_style_properties where style_id = $styleId", TRUE);
				if(is_null($catalogDate))
				{
					$insertContent["catalog_date"] = time();
				}
			}
		}
		
		//auditing style status
		if($updateStyleStatus)
		{
			$oldStatusCode = func_query_first_cell("select styletype from mk_product_style where id = $styleId", TRUE);
			$oldStatus = func_query_first_cell("select style_status_name_to from style_status_transition_rules where style_status_code_to = '$oldStatusCode'", true);
			$newStatus = func_query_first_cell("select style_status_name_to from style_status_transition_rules where style_status_code_to = '$newStatusCode'", true);
			$styleStatusAudit = array(
					"entityId" => $styleId,
					"entityName" => 'com.myntra.StyleProperties',
					"entityProperty" => 'Status',
					"entityPropertyOldValue" => $oldStatus,
					"entityPropertyNewValue" => $newStatus,
					"operationType" => 'UPDATE');
			$styleAuditData = array($styleStatusAudit);
			$auditResult = AuditApiClient::sendStyleAuditInfo($styleAuditData, $login, 'com.myntra.admin.catalog.styleBulkUpdateAjax.php');
		}
		global $sqllog;
		$sqllog->debug("User: $login ; Style id = $styleId");
		$sqllog->debug($insertContent);
		func_array2updateWithTypeCheck('mk_style_properties', $insertContent, "style_id = $styleId");
		func_array2updateWithTypeCheck('mk_product_style', $insertContentStatus, "id = $styleId");
		
		//add/delete style to solr index
		$styleStatus = func_query_first_cell("select styletype from mk_product_style where id = $styleId", TRUE);
		if($styleStatus == 'P')
		{
			addStyleToSolrIndex($styleId,false);
		}
		else
		{
			deleteStyleFromSolrIndex($styleId,false);
		}
	}
	echo json_encode($validContent);
	exit;
}

/**
 * this method validate contents of a style
 */
 function validateStyleContent($style)
 {
 	$description = $style['description'];
 	$styleNote = $style['style_note'];
 	$materialsCareDesc = $style['materials_care_desc'];
 	$sizeFitDesc = $style['size_fit_desc'];

 	$descWithoutSpace = str_replace('&nbsp;', '', $description);
 	$descWithoutSpace = trim(str_replace('<br>', '', $descWithoutSpace));
 	
 	$result['valid'] = false;
 	if(empty($descWithoutSpace))
 	{
 		$result['error'] = 'Description is empty';
 	}
 	elseif (!isAscii($description)){
 		$result['error'] = 'Description contains non-ASCII character';
 	}
 	elseif (!isAscii($styleNote)){
 		$result['error'] = 'Style Note contains non-ASCII character';
 	}
 	elseif (!isAscii($materialsCareDesc)){
 		$result['error'] = 'Materials Care Description contains non-ASCII character';
 	}
 	elseif (!isAscii($sizeFitDesc)){
 		$result['error'] = 'Size Fit Description contains non-ASCII character';
 	}
 	else {
 		$result['valid'] = true;
 	} 
 	return $result;
 }

/**
 * this method validates the style data and returns any error
 * @param array $style
 */
function validateStyle($style)
{
	$updateError = '';
	$styleId = $style['style_id'];
	$styleStatus = $style['style_status'];
	$vendorArticleNo = $style['vendor_article_no'];
	$vendorArticleName = $style['vendor_article_name'];
	$articleTypeId = $style['article_type_id'];
	$productType = $style['product_type'];
	$brand = $style['brand'];
	$ageGroup = $style['age_group'];
	$gender = $style['gender'];
	$baseColour = $style['base_colour'];
	$colour1 = $style['colour1'];
	$colour2 = $style['colour2'];
	$fashionType = $style['fashion_type'];
	$season = $style['season'];
	$year = $style['year'];
	$usage = $style['usage'];
	$price = $style['price'];
	$weight = $style['weight'];
	$productDisplayName = $style['product_display_name'];
	$tags = $style['tags'];
	$classificationBrand = $style['classification_brand'];
	
	//Product Display Name should not contain any Non-Ascii Character
	if(!isAscii($productDisplayName)){
		$updateError .= " Product Display Name contains one or more Non ASCII character(s);";
	}
	
	$validStatus = validateStatus($styleId, $styleStatus);
	if($styleStatus == 'D')
	{
		$status='Draft';
	}
	if(!$validStatus)
	{
		$updateError .= " Style status is wrong;";
	}
	
	//validate vendor article no
	if(empty($vendorArticleNo))
	{
		$updateError .= " Vendor Article No is blank;";
	}
	else
	{
		$check=func_query("select * from mk_style_properties where style_id!='".$styleId."' and article_number='".$vendorArticleNo."'", TRUE);
		if(!empty($check))
		{
			$updateError .= " Article Number already exists;";
		}
	}
	
	//validate vendor article name
	if(empty($vendorArticleName))
	{
		$updateError .= " Vendor Article Name is blank;";
	}
	
	//validate article type
	if(empty($articleTypeId))
	{
		$updateError .= " Article Type is blank;";
	}
	else
	{
		$articleRes = func_query("select * from mk_catalogue_classification where id = '$articleTypeId' and parent1 !='-1' and parent2 !='-1' and is_active=1", TRUE);
		if(empty($articleRes))
		{
			$updateError .= " Article Type is wrong;";
		}
	}
	
	//validate product type
	if(empty($productType))
	{
		$updateError .= " Product Type is blank;";
	}
	else
	{
		$validProductType = func_query("SELECT * FROM mk_product_type WHERE type = 'P' AND id = '$productType'", TRUE);
		if(empty($validProductType))
		{
			$updateError .= " Product Type is wrong;";
		}
	}
	
	//validate brand
	if(empty($brand))
	{
		$updateError .= " Brand is blank;";
	}
	else
	{
		$brandRes=func_query("select * from mk_attribute_type_values where attribute_type='Brand' and BINARY attribute_value='$brand' and is_active=1", TRUE);
		if(empty($brandRes))
		{
			$updateError .= " Brand is wrong;";
		}
	}
	
	//validate age group
	if(empty($ageGroup))
	{
		if($status != 'Draft')
			$updateError .= " Age Group is blank;";
	}
	else
	{
		$res=func_query("select * from mk_attribute_type_values where attribute_type_id='2' and BINARY attribute_value='$ageGroup' and is_active=1", TRUE);
		if(empty($res))
		{
			$updateError .= " Age Group is wrong;";
		}
	}
	
	//validate gender
	if(empty($gender))
	{
		$updateError .= " Gender is blank;";
	}
	else
	{
		$res=func_query("select * from mk_attribute_type_values where attribute_type_id='3' and BINARY attribute_value='$gender' and is_active=1", TRUE);
		if(empty($res))
		{
			$updateError .= " Gender is wrong;";
		}
	}
	
	//validate the base colour
	if(empty($baseColour))
	{
		if($status != 'Draft')
		{
			$updateError .= " Base Colour is blank;";
		}
	}
	else
	{
		$res=func_query("select * from mk_attribute_type_values where attribute_type_id='4' and BINARY attribute_value='$baseColour' and is_active=1", TRUE);
		if(empty($res))
		{
			$updateError .= " Base Colour is wrong;";
		}
	}
	
	//validate the colour 1
	if(empty($colour1))
	{
		if($status != 'Draft')
		{
			$updateError .= " Colour 1 is blank;";
		}
	}
	elseif(!empty($colour1))
	{
		$res=func_query("select * from mk_attribute_type_values where attribute_type_id='4' and BINARY attribute_value='$colour1' and is_active=1", TRUE);
		if(empty($res) && ($colour2 != 'NA'))
		{
			$updateError .= " Colour1 is wrong;";
		}
	}
	
	//validate the colour 2
	if(empty($colour2))
	{
		if($status != 'Draft')
		{
			$updateError .= " Colour 2 is blank;";
		}
	}
	elseif(!empty($colour2))
	{
		$res=func_query("select * from mk_attribute_type_values where attribute_type_id='4' and BINARY attribute_value='$colour2' and is_active=1", TRUE);
		if(empty($res) && ($colour2 != 'NA'))
		{
			$updateError .= " Color2 is wrong;";
		}
	}
	
	//validate fashion type
	if(empty($fashionType))
	{
		if($status != 'Draft')
		{
			$updateError .= " Fashion Type is blank;";
		}
	}
	else
	{
		$res=func_query("select * from mk_attribute_type_values where attribute_type_id='7' and BINARY attribute_value='$fashionType' and is_active=1", TRUE);
		if(empty($res))
		{
			$updateError .= " Fashion Type is wrong;";
		}
	}
	
	//validate season
	if(empty($season))
	{
		if($status != 'Draft')
		{
			$updateError .= " Seasion is blank;";
		}
	}
	else
	{
		$res=func_query("select * from mk_attribute_type_values where attribute_type_id='8' and BINARY attribute_value='$season' and is_active=1", TRUE);
		if(empty($res))
		{
			$updateError .= " Season is wrong;";
		}
	}
	
	//validate year
	if(empty($year))
	{
		if($status != 'Draft')
		{
			$updateError .= " Year is blank;";
		}
	}
	else
	{
		$res=func_query("select * from mk_attribute_type_values where attribute_type_id='9' and BINARY attribute_value='$year' and is_active=1", TRUE);
		if(empty($res))
		{
			$updateError .= " Year is wrong;";
		}
	}
	
	//validate usage
	if(empty($usage))
	{
		if($status != 'Draft')
		{
			$updateError .= " Usage is blank;";
		}
	}
	else
	{
		$res=func_query("select * from mk_attribute_type_values where attribute_type_id='10' and BINARY attribute_value='$usage' and is_active=1", TRUE);
		if(empty($res))
		{
			$updateError .= " Usage is wrong;";
		}
	}
	
	//validate price should be +ve numeric value
	if(empty($price))
	{
		if($status != 'Draft')
		{
			$updateError .= " Price is blank;";
		}
	}
	else
	{
		if (is_numeric($price))
		{
			$price = round($price, 2);
			if($status != 'Draft')
			{
				if($price <= 0)
				{
					$updateError .= " Price $price is not a +ve numeric value;";
				}
			}
			elseif($price < 0)
			{
				$updateError .= " Price $price is not a +ve numeric value;";
			}
		}
		else
		{
			$updateError .= " Price $price is not a numeric value;";
		}
	}
	
	//validate weight
	/* disabling weight for now
	if(empty($weight))
	{
		if($status != 'Draft')
		{
			$updateError .= " Weight is blank;";
		}
	}
	else
	{
		if($weight ==  0)
		{
			$updateError .= " Weight is blank;";
		}
	}
	*/
	
	//validate classification brand
	if(empty($classificationBrand))
	{
		if($status != 'Draft')
		{
			$updateError .= " Classification Brand is blank;";
		}
	}
	else
	{
		$classificationBrandResult = func_query("SELECT * FROM mk_filters WHERE filter_group_id='2' AND BINARY filter_name='$classificationBrand' AND is_active=1", TRUE);
		if(empty($classificationBrandResult))
		{
			$updateError .= " Classification Brand is wrong;";
		}
	}
	
	//validate product tags
	if(empty($tags))
	{
		if($status != 'Draft')
		{
			$updateError .= " Tags is blank;";
		}
	}
	
	//validate specific attributes each specific attribute is mandatory
	//non specific attribute keys
	$nonSpecificAttributesKey = array('style_id', 'style_status', 'article_type_id', 'product_type', 'vendor_article_no', 'tags', 'product_display_name', 'vendor_article_name',
			'brand', 'age_group', 'gender', 'base_colour', 'colour1', 'colour2', 'fashion_type', 'season', 'year', 'usage', 'weight', 'price', 'comments', 'classification_brand');
	foreach($style as $key=>$value)
	{
		if(!in_array($key, $nonSpecificAttributesKey))
		{
			if(($value === 0) || empty($value))
			{
				$updateError .= " $key is blank;";
			}
		}
	}
	
	$retVal = array();
	if(empty($updateError))
	{
		$retVal['valid'] = true;
	}
	else
	{
		$retVal['valid'] = false;
		$retVal['error'] = substr($updateError, 0, -1);
	}
	
	return $retVal;
}

function trim_value(&$value)
{
	$value = trim($value);
}

function escape_mysql_string(&$value)
{
	$value = mysql_real_escape_string($value);
}

function validateStatus($styleId, $newStatus)
{
	if(empty($styleId))
	{
		return false;
	}
	$curStatus = func_query_first_cell("SELECT styletype FROM mk_product_style WHERE id = '$styleId'", TRUE);
	if($curStatus == $newStatus)
	{
		return true;
	}
	else
	{
		$possStatus = func_query_column("SELECT style_status_code_to FROM style_status_transition_rules WHERE style_status_code_from ='$curStatus'", 0, TRUE);
		return (in_array($newStatus, $possStatus))?true:false;
	}
	return false;
}

?>
