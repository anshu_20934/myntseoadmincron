var timerid;
Ext.onReady(function() {
	Ext.QuickTips.init();
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
    Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
    Ext.Ajax.on('requestexception', myMask.hide, myMask);
    
	var downloadMask;
	
	function trackStatus() {
		Ext.Ajax.request({
			url : "/admin/getprogressinfo.php?type=status&progresskey="+progresskey,
			method : 'GET',
			mask: false,
		    success : function(response, action) {
		    	var jsonObj = Ext.util.JSON.decode(response.responseText);
		    	if(jsonObj == null){
		    		return
		    	}
		    	else if(jsonObj.status == 'DONE'){
		    		clearInterval(timerid);
		    		downloadMask.hide();
		    		var html = "Click <a href = '"+httpLocation + "/admin/operations/download.php?type=styles_bulk_upload&filename=" + jsonObj.filename+"' target='_blank'> here </a> to download file";
		    		Ext.Msg.show({
		    		    title: "Download Link",
		    		    msg:   html,
		    		    icon:  Ext.MessageBox.INFO
		    		});
		    	}
		    	else if(jsonObj.status == 'NONE'){
		    		clearInterval(timerid);
		    		downloadMask.hide();
		    		Ext.MessageBox.alert('Error', 'No results found');
		    	}
		    	else if(jsonObj.status == 'TOO'){
		    		clearInterval(timerid);
		    		downloadMask.hide();
		    		Ext.MessageBox.alert('Error', 'Too many results found');
		    	}
			}
		});
	}
	
	var styleIdTF = new Ext.form.TextArea({
		id : 'styleId',
		name : 'styleId',
		fieldLabel : 'Style Ids',
		emptyText : 'Enter Style Ids',
		width: 180
	})
	
	var vendorArticleNoTF = new Ext.form.TextArea({
		id : 'vendorArticleNo',
		name : 'vendorArticleNo',
		fieldLabel : 'Vendor Article No.',
		emptyText : 'Enter Vendor Article No.',
		width: 180
	})
	
	var styleStatusStore = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : styleStatusData,
	    sortInfo : {field : 'name', direction : 'ASC'}
	});

	var styleStatus = new Ext.ux.form.SuperBoxSelect({
		id : 'styleStatus',
		name : 'styleStatus',
		fieldLabel : 'Status',
		emptyText : 'Select Style Status',
		anchor : '100%',
		store : styleStatusStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'id',
		forceSelection : true
	});
	
	var styleNameTF = new Ext.form.TextArea({
		id : 'styleName',
		name : 'styleName',
		fieldLabel : 'Style Name',
		emptyText : 'Enter Style Name',
		width: 180
	})
	
	var skuCodeTF = new Ext.form.TextArea({
		id : 'skuCode',
		name : 'skuCode',
		fieldLabel : 'Sku Code',
		emptyText : 'Enter Sku codes',
		width: 180
	})
	
	var brandsStore = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : brandsData,
		sortInfo : {field : 'name', direction : 'ASC'}
	});
	
	var brands = new Ext.ux.form.SuperBoxSelect({
		id : 'brands',
		name : 'brands',
		fieldLabel : 'Brands',
		emptyText : 'Select Brands',
		resizable : true,
		store : brandsStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'name',
		forceSelection : true
	});
	
	var masterCategoryStore = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : masterCategoryData,
		sortInfo : {field : 'name', direction : 'ASC'}
	});

	var masterCategory = new Ext.ux.form.SuperBoxSelect({
		id : 'masterCategory',
		name : 'masterCategory',
		fieldLabel : 'Master Category',
		emptyText : 'Select Master Category',
		resizable : true,
		store : masterCategoryStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'id',
		forceSelection : true
	});
	
	var subCategoryStore = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : subCategoryData,
		sortInfo : {field : 'name', direction : 'ASC'}
	});

	var subCategory = new Ext.ux.form.SuperBoxSelect({
		id : 'subCategory',
		name : 'subCategory',
		fieldLabel : 'Sub Category',
		emptyText : 'Select Sub Category',
		resizable : true,
		store : subCategoryStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'id',
		forceSelection : true
	});
	
	var articleTypeStore = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : articleTypeData,
		sortInfo : {field : 'name', direction : 'ASC'}
	});

	var articleType = new Ext.ux.form.SuperBoxSelect({
		id : 'articleType',
		name : 'articleType',
		fieldLabel : 'Article Type',
		emptyText : 'Select Article Type',
		resizable : true,
		store : articleTypeStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'id',
		forceSelection : true
	});
	
	var imageAvailArr = [['1', 'Yes'],['2', 'No']];
	
	var imageAvailStore = new Ext.data.ArrayStore({
        fields: ['id', 'name'],
        data : imageAvailArr
    });

	var styleImageAvail = new Ext.ux.form.SuperBoxSelect({
              id:'styleImageAvail',
              name:'styleImageAvail',
              fieldLabel : 'Image Available',
              emptyText : 'Check for Image Availability',
              typeAhead: true,
              triggerAction: 'all',
              mode: 'local',
              store: imageAvailStore,
              displayField: 'name',
              valueField: 'id',
              forceSelection : true
    });
	
	var addAttributeFilter = new Ext.Button({
		id : 'attributeFilter',
		iconCls : 'add',
		text : 'Add Product Specific Attribute Filters',
		handler : function()
		    	{
					myMask.show();
       		 		addArticleTypeAttributeValue();
		        }
	});
	
	var showGlobalAttribute = new Ext.Button({
		id : 'globalAttribute',
		iconCls : 'add',
		text : 'Show Global Attributes Filters',
		handler : function()
		    	{
       		 		showHideGlobalAttribute();
		        },
		listeners : {
		            click: function(button,event) {
		            	if(showHideGAP)
		            	{
		            		button.setIconClass('add');
		            		button.setText('Show Global Attributes Filters');
		            	}
		            	else
		            	{
		            		button.setIconClass('add');
		            		button.setText('Hide Global Attributes Filters');
		            	}
		            }
		        }      
	});
	
	var attributeValueValTF = new Ext.form.TextArea({
		id : 'attributeValuesVal',
		name : 'attributeValuesVal',
		fieldLabel : 'Attribute Values Ids',
		emptyText : 'Use the button below to add attribute values',
		readOnly : true,
		disabled : true,
		width : 180
	})
	
	var attributeValueIdTF = new Ext.form.TextArea({
		id : 'attributeValuesId',
		name : 'attributeValuesId',
		readOnly : true,
		hidden : true
	})
	
	//global attribute panel datastore and ui component
	var ageGroupsStore = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : ageGroupsData,
		sortInfo : {field : 'name', direction : 'ASC'}
	});

	var ageGroups = new Ext.ux.form.SuperBoxSelect({
		id : 'ageGroups',
		name : 'ageGroups',
		fieldLabel : 'Age Groups',
		emptyText : 'Select Age Groups',
		resizable : true,
		store : ageGroupsStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'name',
		forceSelection : true
	});
	
	var gendersStore = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : gendersData,
		sortInfo : {field : 'name', direction : 'ASC'}
	});

	var genders = new Ext.ux.form.SuperBoxSelect({
		id : 'genders',
		name : 'genders',
		fieldLabel : 'Genders',
		emptyText : 'Select Genders',
		resizable : true,
		store : gendersStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'name',
		forceSelection : true
	});
	
	var fashionTypesStore = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : fashionTypesData,
		sortInfo : {field : 'name', direction : 'ASC'}
	});

	var fashionTypes = new Ext.ux.form.SuperBoxSelect({
		id : 'fashionTypes',
		name : 'fashionTypes',
		fieldLabel : 'Fashion Types',
		emptyText : 'Select Fashion Types',
		resizable : true,
		store : fashionTypesStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'name',
		forceSelection : true
	});
	
	var baseColoursStore = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : coloursData,
		sortInfo : {field : 'name', direction : 'ASC'}
	});

	var baseColours = new Ext.ux.form.SuperBoxSelect({
		id : 'baseColours',
		name : 'baseColours',
		fieldLabel : 'Base Colours',
		emptyText : 'Select Base Colours',
		resizable : true,
		store : baseColoursStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'name',
		forceSelection : true
	});
	
	var colour1Store = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : coloursData,
		sortInfo : {field : 'name', direction : 'ASC'}
	});

	var colour1 = new Ext.ux.form.SuperBoxSelect({
		id : 'colour1',
		name : 'colour1',
		fieldLabel : 'Colour1',
		emptyText : 'Select Colour1',
		resizable : true,
		store : colour1Store,
		mode : 'local',
		displayField : 'name',
		valueField : 'name',
		forceSelection : true
	});
	
	var colour2Store = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : coloursData,
		sortInfo : {field : 'name', direction : 'ASC'}
	});

	var colour2 = new Ext.ux.form.SuperBoxSelect({
		id : 'colour2',
		name : 'colour2',
		fieldLabel : 'Colour2',
		emptyText : 'Select Colour2',
		resizable : true,
		store : colour2Store,
		mode : 'local',
		displayField : 'name',
		valueField : 'name',
		forceSelection : true
	});
	
	var seasonsStore = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : seasonsData,
		sortInfo : {field : 'name', direction : 'ASC'}
	});

	var seasons = new Ext.ux.form.SuperBoxSelect({
		id : 'seasons',
		name : 'seasons',
		fieldLabel : 'Seasons',
		emptyText : 'Select Seasons',
		resizable : true,
		store : seasonsStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'name',
		forceSelection : true
	});
	
	var yearsStore = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : yearsData,
		sortInfo : {field : 'name', direction : 'ASC'}
	});

	var years = new Ext.ux.form.SuperBoxSelect({
		id : 'years',
		name : 'years',
		fieldLabel : 'Years',
		emptyText : 'Select Years',
		resizable : true,
		store : yearsStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'name',
		forceSelection : true
	});
	
	var usagesStore = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : usagesData,
		sortInfo : {field : 'name', direction : 'ASC'}
	});

	var usages = new Ext.ux.form.SuperBoxSelect({
		id : 'usages',
		name : 'usages',
		fieldLabel : 'Usages',
		emptyText : 'Select Usages',
		resizable : true,
		store : usagesStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'name',
		forceSelection : true
	});
	
	var showHideGAP = true;
	var attributeFilterOnOff = false;
	
	var globalAttributePanel = new Ext.form.FormPanel({
		title : 'Global Attributes',
		id : 'globalAttributePanel',
		autoHeight : true,
		collapsible : false,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 930,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 3},
			defaults : {width : 300, border : false, layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [
			         {items : [ageGroups]},
			         {items : [genders]},
			         {items : [fashionTypes]},
			         {items : [baseColours]},
			         {items : [colour1]},
			         {items : [colour2]},
			         {items : [seasons]},
			         {items : [years]},
			         {items : [usages]}
			        ]
			}]
	});
	//end of global attribute panel
	
	var searchPanel = new Ext.form.FormPanel({
		id : 'formPanel',
		autoHeight : true,
		collapsible : false,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 3},
			defaults : {width : 320, border : false, layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [
			         {items : [styleIdTF]},
			         {items : [masterCategory]},
			         {items : [styleStatus]},
			         {items : [styleNameTF]},
			         {items : [subCategory]},
			         {items : [brands]},
			         {items : [vendorArticleNoTF]},
			         {items : [articleType]},
			         {items : [styleImageAvail]},
			         {items : [skuCodeTF]},
			         {items : [attributeValueValTF]},
			         {items : [attributeValueIdTF]},
			         {items : [{xtype: 'component', fieldLabel: '&nbsp;', labelSeparator: ' '}]},//empty component
			         {items : [addAttributeFilter]},
			         {items : [showGlobalAttribute]}
			        ]
			}]
		
	});
	
	var mainSearchPanel = new Ext.Panel({
		title : 'Search Panel',
		id : 'mainSearchPanelId',
		autoHeight : true,
		collapsible : false,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 1},
			defaults : {width : 1000, border : false, layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [searchPanel, globalAttributePanel]
		}],
		buttons : [
				    {
						text : 'Search',
					    handler : function()
					    	{
					    		reloadGrid();
					        }
				    },
				    {

						text : 'Download File',
						handler : function() {
							var params = getSearchPanelParams();
							if (params == false)
							{
								Ext.MessageBox.alert('Error', 'Please set at least one search parameter');
								return false;
							}
							params["action"] = "downloadCSV";
							params["progresskey"] = progresskey;
							downloadMask = new Ext.LoadMask(Ext.get('mainSearchPanelId'), {msg: "Generating File"});
							downloadMask.show();
							Ext.Ajax.request({
								url : 'search_styles_ajax.php',
								params : params,
							    success : function(response, action) {
								}
							});
							timerid = setInterval(trackStatus, 3000);
						}
					
				    }]
	});
	
	function reloadGrid()
	{
		var params = getSearchPanelParams();
		
		if (params == false)
		{
			Ext.MessageBox.alert('Error', 'Please enter at least one search parameter');
			return false;
		}
		else
		{
			myMask.show();
    		params["action"] = "search";
    		searchResultsStore.baseParams = params;
    		searchResultsStore.load();
		}
	}
	
	function getSearchPanelParams()
	{
		var form = searchPanel.getForm();
		
		if (form.isValid())
		{
			var params = {};
			params["styleId"] = form.findField("styleId").getValue();
			params["vendorArticleNo"] = form.findField("vendorArticleNo").getValue();
			params["styleStatus"] = form.findField("styleStatus").getValue();
			params["styleName"] = form.findField("styleName").getValue();
			params["skuCode"] = form.findField("skuCode").getValue();
			params["brands"] = form.findField("brands").getValue();
			params["masterCategory"] = form.findField("masterCategory").getValue();
			params["subCategory"] = form.findField("subCategory").getValue();
			params["articleType"] = form.findField("articleType").getValue();
			params["styleImageAvail"] = form.findField("styleImageAvail").getValue();
			if(showHideGAP)
			{
				var globalAttributeForm = globalAttributePanel.getForm();
				if (globalAttributeForm.isValid())
				{
					params["ageGroups"] = globalAttributeForm.findField("ageGroups").getValue();
					params["genders"] = globalAttributeForm.findField("genders").getValue();
					params["fashionTypes"] = globalAttributeForm.findField("fashionTypes").getValue();
					params["baseColours"] = globalAttributeForm.findField("baseColours").getValue();
					params["colour1"] = globalAttributeForm.findField("colour1").getValue();
					params["colour2"] = globalAttributeForm.findField("colour2").getValue();
					params["seasons"] = globalAttributeForm.findField("seasons").getValue();
					params["years"] = globalAttributeForm.findField("years").getValue();
					params["usages"] = globalAttributeForm.findField("usages").getValue();
				}
			}
			if(attributeFilterOnOff)
			{
				if(params["articleType"].indexOf(",") >= 0)
				{
					
					alert('As multiple article type is select Attribute Values are being removed.');
		            setAttributeValuesId('', '');
					attributeFilterOnOff = false;
				}
				else if(params["articleType"] == '')
				{
					alert('As no article type is select Attribute Values are being removed.');
		            setAttributeValuesId('', '');
					attributeFilterOnOff = false;
				}
				else
				{
					params["attributeValues"] = form.findField("attributeValuesId").getValue();
				}
			}
			
			var emptyParams = true;
			$.each(params, function(key, value) {
				  if(value != '')
				  {
					  emptyParams = false;
					  return params;
				  }
			});
			if(emptyParams)
			{
				return false;
			}
			return params;
		}
		else
		{
			return false;
		}
	}
	
	function showResult(btn)
	{
		if(btn == 'no')
		return false;
	}
	
	function setAttributeValuesId(attVal, attId)
	{
		Ext.getCmp('attributeValuesVal').setValue(attVal);
		Ext.getCmp('attributeValuesId').setValue(attId);
	}
	
	function showHideGlobalAttribute()
	{
		showHideGAP = !showHideGAP;
		if(showHideGAP)
		{
			globalAttributePanel.show();
		}
		else
		{
			globalAttributePanel.hide();
		}
		
	}
	
	var searchResultsStore = new Ext.data.Store({
		url: 'search_styles_ajax.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['style_id', 'vendor_article_no', 'product_display_name', 'style_status', 'brand', 'article_type', 'fasion_type', 'season', 'year']
		})
	});
	
	var editNPLinkRender = function(value, p, record){
		if(value == '')
		{
			value = "NO NAME AVAILABLE";
		}
		var retLink = '<a href="' + httpLocation + '/admin/productstyle_action_new.php?mode=modifystyle&style_id=' + record.data["style_id"] + '" target="_blank">' + value + '</a>';
		return retLink;
	}
	
    //status renderer
    var styleStatusRender = function(value, p, record){
        var styleStatusStore = new Ext.data.JsonStore({
            fields : ['id', 'name'],
            data : styleStatusData,
            sortInfo : {field : 'name', direction : 'ASC'}
        });
        var rec = styleStatusStore.getById(value);
        return rec ? rec.get('name') : value;
    }
    
	var sm = new Ext.grid.CheckboxSelectionModel();
	var textFieldEdit = new Ext.form.TextField();
	var colModel = new Ext.grid.ColumnModel({
		defaults: {sortable: true},
	    columns: [
	        new Ext.grid.RowNumberer(),
	        sm,
	        {id : 'style_id', header : "Style Id", sortable: false, width: 70, dataIndex : 'style_id'},
	        {id : 'vendor_article_no', header : "Vendor Article No", sortable: false, width: 150, dataIndex : 'vendor_article_no', editor: textFieldEdit},
	        {id : 'product_display_name', header : "Product Display Name", sortable: false, width: 400, dataIndex : 'product_display_name', renderer : editNPLinkRender},
	        {id : 'style_status', header : "Status", sortable: false, width: 140, dataIndex : 'style_status', renderer : styleStatusRender},
	        {id : 'brand', header : "Brand", sortable: false, width: 100, dataIndex : 'brand'},
	        {id : 'article_type', header : "Article Type", sortable: false, width: 100, dataIndex : 'article_type'},
	        {id : 'fasion_type', header : "Fashion Type", sortable: false, width: 100, dataIndex : 'fasion_type'},
	        {id : 'season', header : "Season", sortable: false, width: 70, dataIndex : 'season'},
	        {id : 'year', header : "Year", sortable: false, width: 70, dataIndex : 'year'}
  	  	]
	});
	
	var pagingBar = new Ext.PagingToolbar({
		pageSize : 30,
	    store: searchResultsStore,
	    displayInfo: true
	});
	
	var searchResults = new Ext.grid.EditorGridPanel({
	    store : searchResultsStore,
	    cm : colModel,
	    id : 'searchResults-panel',
	    title : 'Search Results',
	    sm:sm,
	    frame : true,
		loadMask : false,
		autoHeight : true,
		bbar : pagingBar,
		stripeRows : true,
		autoScroll : true,
		viewConfig : {enableRowBody : true},
		listeners : { afteredit: function(e){
                         e.record.reject();
                      }
                    },
		tbar: [{text: 'Update Selected Styles Details',
		        icon: 'images/table_add.png',
		        cls: 'x-btn-text-icon',
                handler: function(){
                	var selRows = searchResults.getSelectionModel().getSelections();
                	var noRows = selRows.length;
                	if(noRows == 0)
                	{
                		alert("Please select a style id to update");
                		return;
                	}
                	var style_ids = '';
                    for(var i = 0; i < noRows; i++)
                    {
                        style_ids = style_ids + ',' + selRows[i].get('style_id');                    	
                    }
                    style_ids = style_ids.substring(1);
                    window.open(httpLocation+'/admin/catalog/styleBulkUpdate.php?style_ids='+style_ids);
                }
		      },
		      {text: 'Update Selected Styles Contents',
                icon: 'images/table_add.png',
                cls: 'x-btn-text-icon',
                handler: function(){
                    var selRows = searchResults.getSelectionModel().getSelections();
                    var noRows = selRows.length;
                    if(noRows == 0)
                    {
                        alert("Please select a style id to update");
                        return;
                    }
                    var style_ids = '';
                    for(var i = 0; i < noRows; i++)
                    {
                        style_ids = style_ids + ',' + selRows[i].get('style_id');                       
                    }
                    style_ids = style_ids.substring(1);
                    window.open(httpLocation+'/admin/catalog/styleContentUpdate.php?style_ids='+style_ids);
                }
              }
		      ]
    	});
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [mainSearchPanel, searchResults]
	});
	
	showHideGlobalAttribute();
	
	function addArticleTypeAttributeValue()
	{
		var articleIdForAttributes = Ext.getCmp('articleType').getValue();
		if(articleIdForAttributes.indexOf(",") >= 0)
		{
			Ext.MessageBox.alert('Error', 'Please select only one article type.');
			setAttributeValuesId('', '');
			attributeFilterOnOff = false;
			myMask.hide();
			return false;
		}
		else if(articleIdForAttributes == '')
		{
			Ext.MessageBox.alert('Error', 'Please select one article type.');
			setAttributeValuesId('', '');
			attributeFilterOnOff = false;
			myMask.hide();
			return false;
		}
		
		attributeFilterOnOff = true;
		
		Ext.Ajax.request({
			url: 'search_styles_ajax.php',
			params : {"action" : "getAttributeValues", "articleId" : articleIdForAttributes},
		    success : function(response, action) {
		    	var attributeValues = jQuery.parseJSON(response.responseText);
		    	var attributeValBox = new Array();
		    	if(!attributeValues)
		    	{
		    		Ext.MessageBox.alert('Error', 'No Specific Attribute Type Found.');
		    		setAttributeValuesId('', '');
		    		return false;
		    	}
		    	var noAttr = attributeValues.length;
		    	for(var i = 0; i < noAttr; i++)
		        {
		    		var newEle = new Ext.ux.form.SuperBoxSelect({
			    							id : 'atbox'+i,
								    		name : 'atbox'+i,
								    		fieldLabel : attributeValues[i].name,
								    		emptyText : ' ' + attributeValues[i].name,
								    		resizable : true,
								    		store : new Ext.data.JsonStore({
											    		fields : ['id', 'name'],
											    		data : attributeValues[i].value,
											    		sortInfo : {field : 'name', direction : 'ASC'}
											    	}),
								    		mode : 'local',
								    		displayField : 'name',
								    		valueField : 'id',
								    		forceSelection : true
			    						});
		    		attributeValBox[i] = {items : [newEle]};
		        }
		    	var widthPopWin = 700;
		    	if(noAttr < 2)
		    	{
		    		widthPopWin = 350;
		    	}
		    	
		    	var addTxnPanel = new Ext.form.FormPanel({
					id : 'addTxnPanel',
					width: widthPopWin,
					border : false,
					bodyStyle : 'padding: 5px',
					items : [{
						layout : 'table',
						border : false,
						layoutConfig : {columns : 2},
						defaults : {border : false, layout : 'form', bodyStyle : 'padding-left : 15px'},
						items: attributeValBox
					}]
		    	});
		    	
		    	var window =  new Ext.Window({
					title: 'Add Attribute Values',
					id: 'newtxnwindow',
					width: widthPopWin,
					items: addTxnPanel,
					buttons: [{
						 id: 'newtxnadd',
						 text: 'Add',
						 handler: function() {
							var attValuesTBVal = '';
							var attValuesTBId = '';
							var firstAttr = true;
							for(var i = 0; i < noAttr; i++)
					        {
								var eleAtBox = Ext.getCmp('atbox'+i).getValueEx();
								for(var j = 0; j < eleAtBox.length; j++)
								{
									if(firstAttr)
									{
										attValuesTBVal = attValuesTBVal + eleAtBox[j].name;
										attValuesTBId = attValuesTBId + eleAtBox[j].id;
										firstAttr = false;
									}
									else
									{
										attValuesTBVal = attValuesTBVal + "," + eleAtBox[j].name;
										attValuesTBId = attValuesTBId + "," + eleAtBox[j].id;
									}
									
								}
					        }
							setAttributeValuesId(attValuesTBVal, attValuesTBId);
							window.destroy();
						 }
					},
					{
						id: 'newtxncancel',
						text: 'Close',
						handler: function() {
							window.destroy();
						}
					}]
				}).show();
				window.center();
			}
		});
		return;
		
	}
});