<?php
/**
 * Created by Lohiya.
 * Date: 08 Jul, 2011
 */
require_once( "../auth.php");
require_once $xcart_dir."/include/security.php";
@include ($xcart_dir."/include/func/func.filter_order.php");
define('FORM_ACTION','add_display_category.php');
$mode = 'add';
$submit_button_text = 'Add Display Category';

$sql = "select * from  mk_attribute_type where catalogue_classification_id is null and is_active=1" ;
$global_attributes=func_query($sql);
$smarty->assign("global_attributes",$global_attributes);

$attr_array=array();
foreach($global_attributes as $t)
{
	$attributes=func_query("select * from mk_attribute_type_values where is_active=1 and attribute_type='".$t['attribute_type']."' order by attribute_value asc ");
	$values = array();
	foreach($attributes as $a)
	{
		array_push($values, $a['attribute_value']);
//		$attr_array[$t['attribute_type']][$a['id']]=$a['attribute_value'];
	}
	$attr_array[$t['attribute_type']]=$values;
}

$smarty->assign("attributes",$attr_array);
$smarty->assign("start_counter",'1');
$smarty->assign("message",$_GET['message']);
$login=$XCART_SESSION_VARS['identifiers']['A'][login];
if($_POST['mode'] == 'add'){
	$query_data = array(
        "name" => $_POST['display_category_name'],
   		"category_rule" => json_encode($_POST),
		"created_on" => time(),
		"created_by" => $login,
        "updated_on" => time(),
		"updated_by"=> $login,
		"filter_order" => $_POST['filter_order'],
		"is_active" => $_POST['is_active'],
	);

	update_filter_order("mk_display_categories",'',!empty($_POST["filter_order"]) ? $_POST["filter_order"] : '0');
	func_array2insert("mk_display_categories",$query_data);
	$new_id  = db_insert_id();
	func_header_location("add_display_category.php?mode=edit&id=$new_id&message=added display category");

}else if($_POST['mode'] == 'update'){
	$query_data = array(
        "name" => $_POST['display_category_name'],
   		"category_rule" => json_encode($_POST),
		"updated_on" => time(),
		"updated_by"=> $login,
		"filter_order" => $_POST['filter_order'],
		"is_active" => $_POST['is_active'],
	);
	update_filter_order("mk_display_categories",$_POST[display_category_id],!empty($_POST["filter_order"]) ? $_POST["filter_order"] : '0');
	func_array2update('mk_display_categories', $query_data, "id='".$_POST[display_category_id]."'");
	func_header_location("add_display_category.php?mode=edit&id=$_POST[display_category_id]&message=updated display category");
}else if($_GET['mode'] == 'edit'){
	$sql = "select * from  mk_display_categories where id='".$_GET['id']."'" ;
	$display_category=func_query_first($sql);
	$smarty->assign("display_category_name",$display_category['name']);
	$smarty->assign("is_active",$display_category['is_active']);
	$smarty->assign("filter_order",$display_category['filter_order']);
	$smarty->assign("rules",json_decode($display_category['category_rule'])->row);
	$smarty->assign("article_type_id_list",json_decode($display_category['category_rule'])->article_type_id);
	$smarty->assign("display_category_id",$_GET['id']);
	$row_count=count(json_decode($display_category['category_rule'])->row)+1;
	$smarty->assign("start_counter",$row_count);
	$submit_button_text="Update Display Category";
	$mode='update';
}else if(isset($_GET['mode']) && $_GET['mode'] =='getAttributeValues'){
	$attribute_values=$attr_array[$_GET[attribute_type]];
	echo json_encode($attribute_values,JSON_FORCE_OBJECT);
	exit;
}else if(isset($_GET['mode']) && $_GET['mode'] =='checkForDisplayCategory'){
	$sql = "select name from  mk_display_categories where name='".$_GET['name']."'" ;
	$display_category=func_query_first($sql);
	if(!empty($display_category)){
		echo 'yes';
	}else{
		echo 'no';
	}
	exit;
}

$article_types=func_query("select * from mk_catalogue_classification where parent1 !='-1' and parent2 !='-1' and is_active=1 order by typename asc");
$smarty->assign("article_types",$article_types);

$sql = "select * from  mk_attribute_type_values where attribute_type_id=1 and is_active=1 order by attribute_value asc" ;
$default_global_attribute_values=func_query($sql);
$smarty->assign("default_global_attribute_values",$default_global_attribute_values);
$smarty->assign("main","add_display_categories");
# Assign the current location line
$smarty->assign("location", $location);
$smarty->assign("form_action", FORM_ACTION);
# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);
$smarty->assign ("top_message", $top_message);
$smarty->assign ("mode", $mode);
$smarty->assign ("submit_button_text", $submit_button_text);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
