/**
 * function to display a pop window for selecting or searching style id for copying purpose
 * it passes the style id to 'copyStyleAjax' funciton, which should be called in parent window 
 * for copying different properties
 * @param string copyBulkOrContent: 'BULK', 'CONTENT'
 */
    function selectORseachStyle(originalStyleId, copyBulkOrContent)
    {
        var widthPopWin = 350;
        var copyStyleCB = new Array();
        var copyFromStyle = {
        	xtype: 'textfield',
            id: 'copyFromStyle',
            allowBlank : false,
            fieldLabel:"Copy from Style*",
            width:"90%"
        };
        var searchStyleArticle = { 
           xtype: 'fieldset',
           layout: 'column',
           items:[
                  {
                   xtype: 'label',
                   allowBlank : true,
                   text:"Or Search by Article number  :",
                   width:"100"
                  },
                  {
                   xtype: 'textfield',
                   id: 'vendor_article',
                   allowBlank : true
                  },
                  {
                   xtype: 'button',
                   text:"Search",
                   width:"100",
                   handler:function()
                   {
                       if($.trim(Ext.getCmp("vendor_article").getValue())=="")
                       {
                           Ext.MessageBox.alert('Error', "Enter Article/Vendor Article number to Search");
                           return;
                       }
                       var dialog= new StylePickerDialog({vendorarticle:Ext.getCmp("vendor_article").getValue(),selectedStyles:Ext.getCmp("copyFromStyle").getValue()});
                       dialog.show();
                    }
                    }
                ]
        };
        copyStyleCB[0] = {items: copyFromStyle};
        copyStyleCB[1] = {items: searchStyleArticle};
        var copyStyleForm = new Ext.form.FormPanel({
                    width: widthPopWin,
                    border : false,
                    bodyStyle : 'padding: 5px',
                    items : [{
                        layout : 'table',
                        border : false,
                        layoutConfig : {columns : 1},
                        defaults : {border : false, layout : 'form', bodyStyle : 'padding-left : 15px'},
                        items: copyStyleCB
                    }]
                });
                
        var copyStyleWindow = new Ext.Window({
                    id:'copyStyleWindowId',
                    title: 'Copy values from Style',
                    width: widthPopWin,
                    items: copyStyleForm,
                    buttons: [{
                         id: 'newtxnadd',
                         text: 'Copy',
                         handler: function() {
                            var copy4mStyleId = Ext.getCmp('copyFromStyle').getValue();
                            copyStyleAjax(originalStyleId, copy4mStyleId, copyBulkOrContent);
                        }
                    },
                    {
                        id: 'newtxncancel',
                        text: 'Close',
                        handler: function() {
                            copyStyleWindow.destroy();
                        }
                    }]
                
        }).show();
    }