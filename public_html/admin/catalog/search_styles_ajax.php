<?php
require '../auth.php';
include_once "$xcart_dir/include/security.php";
include_once "$xcart_dir/modules/apiclient/SkuApiClient.php";
include_once "$xcart_dir/include/func/funcStyleProperties.php";

function createSelectSQL($mode)
{
	if($mode == 'search')
	{
		$selectSQL = "
				SELECT SQL_CALC_FOUND_ROWS 
				msp.style_id AS style_id, 
				msp.article_number AS vendor_article_no,
				msp.product_display_name AS product_display_name, 
				mps.styletype AS style_status, 
				msp.global_attr_brand AS brand, 
				mcc.typename AS article_type, 
				msp.global_attr_fashion_type AS fasion_type, 
				msp.global_attr_season AS season, 
				msp.global_attr_year AS 'year' 
				FROM mk_product_style mps 
				LEFT JOIN mk_style_properties msp ON msp.style_id = mps.id 
				LEFT JOIN mk_catalogue_classification mcc ON msp.global_attr_article_type = mcc.id ";
		return $selectSQL;
	}
	elseif($mode == 'downloadCSV')
	{
		$selectSQL = "
				SELECT SQL_CALC_FOUND_ROWS 
				msp.style_id AS style_id, 
				msp.article_number AS vendor_article_no, 
				msp.variant_name AS vendor_article_name, 
				mps.styletype AS style_status, 
				mcc.typename AS article_type, 
				msp.global_attr_brand AS brand, 
				msp.global_attr_age_group AS age_group, 
				msp.global_attr_gender AS gender, 
				msp.global_attr_base_colour AS base_colour, 
				msp.global_attr_colour1 AS colour1, 
				msp.global_attr_colour2 AS colour2, 
				msp.global_attr_fashion_type AS fasion_type, 
				msp.global_attr_usage AS 'usage', 
				msp.global_attr_year AS 'year', 
				mps.price AS price, 
				mps.description AS description, 
				msp.style_note AS style_note, 
				msp.materials_care_desc AS materials_care_desc,
      			msp.size_fit_desc AS size_fit_desc, 
				mps.size_chart_image AS size_chart_image, 				
				msp.product_display_name AS product_display_name, 
				msp.product_tags AS tags, 
				mps.product_type AS product_type, 
				msp.global_attr_season AS season, 
				msp.comments AS comments 
				FROM mk_product_style mps 
				LEFT JOIN mk_style_properties msp ON msp.style_id = mps.id 
				LEFT JOIN mk_catalogue_classification mcc ON msp.global_attr_article_type = mcc.id ";
		return $selectSQL;
	}
	return false;
}

function createSQLQuery($param)
{
	$SQLHead = createSelectSQL($param["action"]);
	if(!$SQLHead)
	{
		return false;
	}
	
	$SQLWhere = " WHERE ";
	$boolWhere = false;
	array_walk($param, 'trim_value');
	if(!empty($param['styleId']))
	{
		$SQLWhere .= getWhereInSQL(false, 'msp.style_id', $param['styleId'], false);
		$boolWhere = true;
	}
	
	if(!empty($param['vendorArticleNo']))
	{
		$noVendorArticleNo = count(explode("\n", $param['vendorArticleNo']));
		if($noVendorArticleNo == 1)
		{
			$SQLWhere .= $boolWhere?" AND ":"";
			$colVal = mysql_real_escape_string($param['vendorArticleNo']);
			$SQLWhere .= " msp.article_number like '%".$colVal."%' ";
		}
		else
		{
			$SQLWhere .= getWhereInSQL($boolWhere, 'msp.article_number', $param['vendorArticleNo'], false);
		}
		$boolWhere = true;
	}
	
	if(!empty($param['styleStatus']))
	{
		$SQLWhere .= getWhereInSQL($boolWhere, 'mps.styletype', $param['styleStatus'], true);
		$boolWhere = true;
	}
	
	if(!empty($param['styleName']))
	{
		$noStyleName = count(explode("\n", $param['styleName']));
		if($noStyleName == 1)
		{
			$SQLWhere .= $boolWhere?" AND ":"";
			$colVal = mysql_real_escape_string($param['styleName']);
			$SQLWhere .= " msp.product_display_name like '%".$colVal."%' ";
		}
		else
		{
			$SQLWhere .= getWhereInSQL($boolWhere, 'msp.product_display_name', $param['styleName'], false);
		}
		$boolWhere = true;
	}
	
	if(!empty($param['skuCode']))
	{
		
		$skuCodes = explode("\n", $param['skuCode']);
		array_walk($skuCodes, 'trim_value');
		array_walk($skuCodes, 'removeNonAlphaNumericVal');
		$styleIdArray = getStyleIdFromSkuCode($skuCodes);
		if(empty($styleIdArray))
		{
			return false;
		}
		else
		{
			$SQLWhere .= getWhereInSQL($boolWhere, 'msp.style_id', implode(",", $styleIdArray), true);
			$boolWhere = true;
		}
	}
	
	if(!empty($param['brands']))
	{
		$SQLWhere .= getWhereInSQL($boolWhere, 'msp.global_attr_brand', $param['brands'], true);
		$boolWhere = true;
	}
	
	if(!empty($param['masterCategory']))
	{
		$SQLWhere .= getWhereInSQL($boolWhere, 'msp.global_attr_master_category', $param['masterCategory'], true);
		$boolWhere = true;
	}
	
	if(!empty($param['subCategory']))
	{
		$SQLWhere .= getWhereInSQL($boolWhere, 'msp.global_attr_sub_category', $param['subCategory'], true);
		$boolWhere = true;
	}
	
	if(!empty($param['articleType']))
	{
		$SQLWhere .= getWhereInSQL($boolWhere, 'msp.global_attr_article_type', $param['articleType'], true);
		$boolWhere = true;
	}
	
	//global attributes related search
	if(!empty($param['ageGroups']))
	{
		$SQLWhere .= getWhereInSQL($boolWhere, 'msp.global_attr_age_group', $param['ageGroups'], true);
		$boolWhere = true;
	}
	
	if(!empty($param['genders']))
	{
		$SQLWhere .= getWhereInSQL($boolWhere, 'msp.global_attr_gender', $param['genders'], true);
		$boolWhere = true;
	}
	
	if(!empty($param['baseColours']))
	{
		$SQLWhere .= getWhereInSQL($boolWhere, 'msp.global_attr_base_colour', $param['baseColours'], true);
		$boolWhere = true;
	}
	
	if(!empty($param['colour1']))
	{
		$SQLWhere .= getWhereInSQL($boolWhere, 'msp.global_attr_colour1', $param['colour1'], true);
		$boolWhere = true;
	}
	
	if(!empty($param['colour2']))
	{
		$SQLWhere .= getWhereInSQL($boolWhere, 'msp.global_attr_colour2', $param['colour2'], true);
		$boolWhere = true;
	}
	
	if(!empty($param['fashionTypes']))
	{
		$SQLWhere .= getWhereInSQL($boolWhere, 'msp.global_attr_fashion_type', $param['fashionTypes'], true);
		$boolWhere = true;
	}
	
	if(!empty($param['seasons']))
	{
		$SQLWhere .= getWhereInSQL($boolWhere, 'msp.global_attr_season', $param['seasons'], true);
		$boolWhere = true;
	}
	
	if(!empty($param['years']))
	{
		$SQLWhere .= getWhereInSQL($boolWhere, 'msp.global_attr_year', $param['years'], true);
		$boolWhere = true;
	}
	
	if(!empty($param['usages']))
	{
		$SQLWhere .= getWhereInSQL($boolWhere, 'msp.global_attr_usage', $param['usages'], true);
		$boolWhere = true;
	}
	//end of global attribute search
	
	if(!empty($param['attributeValues']))
	{
		$attributeValues = getCommaArray($param['attributeValues']);
		$styleIdFromAttrValueArray = func_query_column("SELECT distinct(product_style_id) FROM mk_style_article_type_attribute_values WHERE article_type_attribute_value_id IN ($attributeValues)", 0, TRUE);
		if(empty($styleIdFromAttrValueArray))
		{
			return false;
		}
		else
		{
			$SQLWhere .= getWhereInSQL($boolWhere, 'msp.style_id', implode(",", $styleIdFromAttrValueArray), true);
			$boolWhere = true;
		}
	}
	
	if(!empty($param['styleImageAvail']))
	{
		if($param['styleImageAvail'] == 1)
		{
			$SQLWhere .= $boolWhere?" AND ":"";
			$SQLWhere .= ' (msp.default_image is not null and msp.default_image <> "") ';
			$boolWhere = true;
		}
		elseif($param['styleImageAvail'] == 2)
		{
			$SQLWhere .= $boolWhere?" AND ":"";
			$SQLWhere .= ' (msp.default_image is null or msp.default_image = "") ';
			$boolWhere = true;
		}
	}
	
	$SQLTail = " ORDER BY msp.style_id DESC";
	$SQL = $SQLHead;
	if($boolWhere)
	{
		$SQL .= $SQLWhere;
	}
	else
	{
		return false;		
	}
	$SQL .=$SQLTail;
	return $SQL;
}

function removeNonAlphaNumericVal(&$value)
{
	$value = preg_replace("/[^a-zA-Z0-9]/","", $value);
}

function trim_value(&$value)
{
	$value = trim($value);
}

function escape_mysql_string(&$value)
{
	$value = mysql_real_escape_string($value);
}

function removeSemiColon(&$value)
{
	$value = str_replace(";", " ", $value);
}

function getNewLineArray($rawData)
{
	$data = explode("\n", $rawData);
	array_walk($data, 'trim_value');
	array_walk($data, 'escape_mysql_string');
	$data = "'".implode("','", $data)."'";
	return $data;
}

function getCommaArray($rawData)
{
	$data = explode(",", $rawData);
	$data = "'".implode("','", $data)."'";
	return $data;
}

function getWhereInSQL($boolWhere, $colName, $values, $commaNewLine)
{
	$SQLWhere = $boolWhere?" AND ":"";
	$colValues = $commaNewLine?getCommaArray($values):getNewLineArray($values);
	$SQLWhere .= " $colName IN (".$colValues.") ";
	return $SQLWhere;
}
/**
 * Returs an array of style id given an array of sku codes
 * @param Array $skuCode an array of sku codes
 */
function getStyleIdFromSkuCode($skuCode)
{
	//send sku details api request in bulk for 200 skucodes maximum in a single request
	$maxBulkRequest = 200;
	$totalSkuCount = sizeof($skuCode);
	$noLoop = floor($totalSkuCount/$maxBulkRequest);
	$skuDetails = array();
	for($i = 0; $i <= $noLoop; $i++)
	{
		$requestSkuCodes = array_slice($skuCode, $i*$maxBulkRequest, $maxBulkRequest);
		$requestSkuDetails = SkuApiClient::getSkuDetailsForSkuCode($requestSkuCodes);
		if(!empty($requestSkuDetails))
		{
			$skuDetails = array_merge($skuDetails, $requestSkuDetails);
		}
	}
	$skuIdArray = array();
	foreach($skuDetails as $sku)
	{
		$skuIdArray[] = $sku['id'];
	}
	$styleId = array();
	if(!empty($skuIdArray))
	{
		$skuId = getCommaArray(implode(",", $skuIdArray));
		$styleId = func_query_column("SELECT style_id FROM mk_styles_options_skus_mapping WHERE sku_id in ($skuId)", 0, TRUE);
	}
	return $styleId;
}

function getSizeSkuPairFromStyleId($styleIdArray)
{
	$styleIdList = implode(",", $styleIdArray);
	$styleOptionSku = func_query("SELECT style_id, option_id, sku_id FROM mk_styles_options_skus_mapping WHERE style_id IN ($styleIdList)", TRUE);
	$skuIdArray = array();
	$optionIdArray = array();
	$styleSizeSkuMap = array();
	foreach ($styleOptionSku as $option)
	{
		$skuIdArray[] = $option['sku_id'];
		$optionIdArray[] = $option['option_id'];
		$styleSizeSkuMap[$option['style_id']] = "";
	}
	
	$optionIds = implode(",", $optionIdArray);
	$optionValues = func_query("SELECT id, value FROM mk_product_options WHERE id IN ($optionIds)", TRUE);
	$optionIdValueMap = array();
	foreach ($optionValues as $option)
	{
		$optionIdValueMap[$option['id']] = $option['value'];
	}
	
	//send sku details api request in bulk for 200 skucodes maximum in a single request
	$maxBulkRequest = 200;
	$totalSkuCount = sizeof($skuIdArray);
	$noLoop = floor($totalSkuCount/$maxBulkRequest);
	$skuDetails = array();
	for($i = 0; $i <= $noLoop; $i++)
	{
		$requestSkuIds = array_slice($skuIdArray, $i*$maxBulkRequest, $maxBulkRequest);
		$requestSkuDetails = SkuApiClient::getSkuDetails($requestSkuIds);
		if(!empty($requestSkuDetails))
		{
			$skuDetails = array_merge($skuDetails, $requestSkuDetails);
		}
	}
	
	$skuIdCodeMap = array();
	foreach ($skuDetails as $sku)
	{
		$skuIdCodeMap[$sku['id']] = $sku['code'];
	}
	
	foreach ($styleOptionSku as $option)
	{
		$styleSizeSkuMap[$option['style_id']] .= $optionIdValueMap[$option['option_id']].",";
		$styleSizeSkuMap[$option['style_id']] .= $skuIdCodeMap[$option['sku_id']].",";
	}
	return $styleSizeSkuMap;
}

function removeNewLine($text)
{
	$text = str_replace("\r\n", "", $text);
	$text = str_replace("\n", "", $text);
	return $text;
}

function getStyleDataRow($style)
{	
	//size chart scale
	$sizeChartImage = explode('/sizechartimages/', $style['size_chart_image']);
	$style['size_chart_image'] = $sizeChartImage[count($sizeChartImage)-1];

	//product type
	$style['product_type'] = func_query_first_cell("SELECT name FROM mk_product_type WHERE id = '".$style['product_type']."'", TRUE);

	//classification brand
	$style['classification_brand'] = func_query_first_cell("SELECT applied_filters FROM mk_applied_filters WHERE filter_group_id='2' AND generic_id='".$style['style_id']."'", TRUE);

	//remove new line char from product content fields
	$style['description'] = removeNewLine($style['description']);
	$style['style_note'] = removeNewLine($style['style_note']);
	$style['materials_care_desc'] = removeNewLine($style['materials_care_desc']);
	$style['size_fit_desc'] = removeNewLine($style['size_fit_desc']);
	
	//if age group, color1 and color2 are NA in DB then it will be empty for csv file
	$style['age_group'] = ($style['age_group']=='NA')?'':$style['age_group'];
	$style['colour1'] = ($style['colour1']=='NA')?'':$style['colour1'];
	$style['colour2'] = ($style['colour2']=='NA')?'':$style['colour2'];

	array_walk($style, 'trim_value');
	array_walk($styleAtt, 'trim_value');
	
	array_walk($style, 'removeSemiColon');
	array_walk($styleAtt, 'removeSemiColon');
	
	$styleData =  $style['style_id'].";".$style['vendor_article_no'].";".$style['vendor_article_name'].";".
			$style['style_status'].";".$style['article_type'].";".$style['brand'].";".$style['age_group'].";".$style['gender'].";".
			$style['base_colour'].";".$style['colour1'].";".$style['colour2'].";".$style['fasion_type'].";".
			$style['usage'].";".$style['year'].";".$style['size_sku_pair'].";".$style['price'].";".$style['description'].";".
			$style['style_note'].";".$style['materials_care_desc'].";".$style['size_fit_desc'].";".$style['size_chart_image'].";".
			$style['product_display_name'].";".$style['tags'].";".$style['product_type'].";".$style['classification_brand'].";".
			$style['season'].";".$style['comments'].";"."No"."\n";

	return $styleData;
}

if(isset($_POST) && ($_POST["action"] == "search"))
{
	$SQL = 	createSQLQuery($_POST);
	if(!$SQL)
	{
		$return_array['results'] = '';
		$return_array["count"] = 0;
		print json_encode($return_array);
		exit;
	}
	
	$start = $_POST['start'] == null ? 0 : $_POST['start'];
	$limit = $_POST['limit'] == null ? 30 : $_POST['limit'];
	
	$SQL .=" LIMIT $start, $limit";
	
	$searchResult = func_query($SQL, TRUE);
	$total = func_query("SELECT FOUND_ROWS() total", TRUE);
	
	$return_array['results'] = $searchResult;
	$return_array["count"] = $total[0]["total"];
	
	print json_encode($return_array);
	exit;
}

if(isset($_POST) && ($_POST["action"] == "getAttributeValues"))
{
	$articleId = $_POST['articleId'];
	$articleAttributeValues = getArticleAttributeTypeWithValues($articleId);
	print $articleAttributeValues;
	exit;
}

if (isset($_POST) && $_POST["action"] == 'downloadCSV')
{
	$progresskey = $_REQUEST['progresskey'];
	$xcache = new XCache();
	$progressinfo = array('status'=>'PROCESSING');
	$xcache->store($progresskey."_progressinfo", $progressinfo, 3600);
		
	$SQL = 	createSQLQuery($_POST);
	
	//no resutls found
	if(!$SQL){
		echo "No Result Found";
		$progressinfo = array('status'=>'NONE');
		$xcache->store($progresskey."_progressinfo", $progressinfo, 3600);
		exit;
	}
	$stylesResult = func_query($SQL, TRUE);
	$total = func_query_first_cell("SELECT FOUND_ROWS() total", TRUE);
	
	if($total < 1)
	{
		echo "No Result Found";
		$progressinfo = array('status'=>'NONE');
		$xcache->store($progresskey."_progressinfo", $progressinfo, 3600);
		exit;
	}
	
	//Too Many Search Result
	if($total > 200)
	{
		echo "Too Many Result Found";
		$progressinfo = array('status'=>'TOO');
		$xcache->store($progresskey."_progressinfo", $progressinfo, 3600);
		exit;
	}
		
	#
	#generate table
	#
	$file_name_suffix = time();
	$assignment_report = fopen($xcart_dir."/admin/report_download/styles_bulk_upload_$file_name_suffix.txt","a");
	
	$fileHeader = "Style ID;Vendor Article Number;Vendor Article Name;Active Status;Article type;".
				  "Brand;Age Group;Gender;Base Colour;Colour1;Colour2;Fashion Type;Usage;Year;".
				  "Size - SKU code pair;MRP;Style Description;Style Note;Materials and Care Description;Size and Fit Description;".
				  "Size Chart Path;Product Display Name;Tags;Product Type;Classification Brand;Season;Comments;Upload Images";
	
	fwrite($assignment_report, $fileHeader);
	fwrite($assignment_report, "\r\n");
	
	$styleIdArray = array();
	foreach($stylesResult as $style)
	{
		$styleIdArray[] = $style['style_id'];
	}
	
	$styleIdSizeSkuMap = getSizeSkuPairFromStyleId($styleIdArray);
	$allStatus = getStyleStatus(true);
	$stylesResultNew = array();
	foreach($stylesResult as $style)
	{
		$style['size_sku_pair'] = substr($styleIdSizeSkuMap[$style['style_id']], 0, -1);
		$style['style_status'] = $allStatus[$style['style_status']];
		$stylesResultNew[] = $style;
	}
	
	foreach($stylesResultNew as $style)
	{
		$styleDataRow = getStyleDataRow($style);
		fwrite($assignment_report, $styleDataRow);
	}
	
	#
	#Download
	#
	$file = "styles_bulk_upload_$file_name_suffix.txt";
	
	$progressinfo = array('status'=>'DONE','filename' =>$file);
	$xcache->store($progresskey."_progressinfo", $progressinfo, 3600);
	
	exit;
}

?>