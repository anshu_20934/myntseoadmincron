<?php
/**
 * Created by Ravi Gupta.
 * Date: 18 Jan, 2012
 */
require_once "../auth.php";
require_once $xcart_dir."/include/security.php";
include_once $xcart_dir."/admin/catalog/photoshoot_issue_rules_ajax_handler.php";

use admin\photoshootIssueRule\dataobject\PhotoshootIssueRule;
use admin\photoshootIssueRule\dao\PhotoshootIssueRuleDAO;

//this function returns values for brand, gender, usage etc.
function getAttributeTypeValueArray($attributeTypeId)
{
	$sql = "SELECT id, attribute_value as name FROM mk_attribute_type_values WHERE attribute_type_id = '$attributeTypeId'  AND is_active = 1 ORDER BY attribute_value ASC";
	$result = func_query($sql);
	return $result;
}

$time=time();	
$login=$XCART_SESSION_VARS['identifiers']['A'][login];
$photoshootIssueRule = new PhotoshootIssueRule(mysql_real_escape_string($login));
$searchMode = false;
$directSearch = false;

if(isset($_POST['mode']) && $_POST['mode'] =='add')
{
	$searchMode = true;
	$directSearch = true;
	$brandId = mysql_real_escape_string(trim($_POST['add_brand']));
	$articleId = mysql_real_escape_string(trim($_POST['add_article_type']));
	$genderId = mysql_real_escape_string(trim($_POST['add_gender']));
	$ageGroupId = mysql_real_escape_string(trim($_POST['add_age_group']));
	$usageId = mysql_real_escape_string(trim($_POST['add_usage']));
	$sizeOption1 = mysql_real_escape_string(trim($_POST['add_size_option_1']));
	$sizeOption2 = mysql_real_escape_string(trim($_POST['add_size_option_2']));
	$sizeOption3 = mysql_real_escape_string(trim($_POST['add_size_option_3']));
	
	$data = array(
			"id_attribute_type_values_fk_brand"     => $brandId,
			"id_catalogue_classification_fk"        => $articleId,
			"id_attribute_type_values_fk_gender"    => $genderId,
			"id_attribute_type_values_fk_age_group" => $ageGroupId,
			"id_attribute_type_values_fk_usage"     => $usageId,
			"size_option_1" 						=> $sizeOption1,
			"size_option_2"							=> $sizeOption2,
			"size_option_3" 						=> $sizeOption3,
			"created_on"							=> ($time),
			"created_by"							=> ($login),
			"updated_on"							=> ($time),
			"updated_by"							=> ($login)
		);
	if($photoshootIssueRule->initPhotoshootIssueRuleObject($data))
	{
		$dbApdater = PhotoshootIssueRuleDAO::getInstance();
		$messageAction = $dbApdater->addPhotoshootIssueRule($photoshootIssueRule);
	}
	
	//passing the message to top_message of dialog message handler
	if($messageAction == PhotoshootIssueRuleDAO::SUCCESS_ADD)
	{
		$topMessage["content"] = "Photoshoot issue rule is succesfully added.";
	}
	else if($messageAction == PhotoshootIssueRuleDAO::ERROR_ADD)
	{
		$topMessage["content"] = "Photoshoot issue rule could not be added.";
		$topMessage["type"]="E";
	}
	else if($messageAction == PhotoshootIssueRuleDAO::ERROR_ADD_ISSUE_ALREADY_EXIST)
	{
		$topMessage["content"] = "Photoshoot issue rule already exists please use update if you want to make any changes.";
		$topMessage["type"]="E";
	}
	else if($messageAction == PhotoshootIssueRuleDAO::ERROR_ADD_SAME_SIZE)
	{
		$topMessage["content"] = "Same size values is selected for the Photoshoot issue rule so not added.";
		$topMessage["type"]="E";
	}
	
}
else if(isset($_POST['mode']) && $_POST['mode'] =='delete')
{
	$deleteIdArray = $_POST['update_id'];
	$dbApdater = PhotoshootIssueRuleDAO::getInstance();
	foreach($deleteIdArray as $num=>$id)
	{
		$photoshootIssueRule->setId($id);
		$messageAction =  $dbApdater->deletePhotoshootIssueRule($photoshootIssueRule);
	}
	
	//passing the message to top_message of dialog message handler
	if($messageAction == PhotoshootIssueRuleDAO::SUCCESS_DELETE)
	{
		$topMessage["content"] = "Photoshoot issue rule successfully deleted.";
	}
	else if($messageAction == PhotoshootIssueRuleDAO::ERROR_DELETE) {
		$topMessage["content"] = "Photoshoot issue rule could not be deleted.";
		$topMessage["type"]="E";
	}

}
else if(isset($_POST['mode']) && $_POST['mode'] =='update')
{
	$updateIdArray = $_POST['update_id'];
	$updateBrandArray = $_POST['update_brand'];
	$updateArticleTypeArray = $_POST['update_article_type'];
	$updateGenderArray = $_POST['update_gender'];
	$updateAgeGroupArray = $_POST['update_age_group'];
	$updateUsageArray = $_POST['update_usage'];
	$updateSizeOption1Array = $_POST['update_size_option_1'];
	$updateSizeOption2Array = $_POST['update_size_option_2'];
	$updateSizeOption3Array = $_POST['update_size_option_3'];
	
	$updatePhotoshootIssueRuleSrray = array();
	
	foreach($updateIdArray AS $num=>$id)
	{
		$data = array(
			"id"                                    => $id,
			"id_attribute_type_values_fk_brand"     => $updateBrandArray[$id],
			"id_catalogue_classification_fk"        => $updateArticleTypeArray[$id],
			"id_attribute_type_values_fk_gender"    => $updateGenderArray[$id],
			"id_attribute_type_values_fk_age_group" => $updateAgeGroupArray[$id],
			"id_attribute_type_values_fk_usage"     => $updateUsageArray[$id],
			"size_option_1" 						=> $updateSizeOption1Array[$id],
			"size_option_2" 						=> $updateSizeOption2Array[$id],
			"size_option_3" 						=> $updateSizeOption3Array[$id],
			"updated_on"							=> ($time),
			"updated_by"							=> ($login)
		);
		$photoshootIssueRule = new PhotoshootIssueRule(mysql_real_escape_string($login));
		$photoshootIssueRule->initPhotoshootIssueRuleObject($data);
		$updatePhotoshootIssueRuleSrray[] = $photoshootIssueRule;
	}
	$dbApdater = PhotoshootIssueRuleDAO::getInstance();
	$updateFailedId =  $dbApdater->updatePhotoshootIssueRule($updatePhotoshootIssueRuleSrray);
	//passing the message to top_message of dialog message handler
	if(empty($updateFailedId))
	{
		$topMessage["content"] = "Photoshoot issue rule successfully updated.";
	}
	else
	{	
		$failedId = implode(",", $updateFailedId);
		$topMessage["content"] = "The red colored Photoshoot issue rule rows could not be updated.";
		$topMessage["type"]="E";
	}
}
else if(isset($_POST['mode']) && $_POST['mode'] =='search')
{
	$directSearch = true;
	$searchMode = true;
	$brandId = mysql_real_escape_string(trim($_POST['brand']));
	$articleId = mysql_real_escape_string(trim($_POST['article_type']));
	$genderId = mysql_real_escape_string(trim($_POST['gender']));
	$ageGroupId = mysql_real_escape_string(trim($_POST['age_group']));
	$usageId = mysql_real_escape_string(trim($_POST['usage']));
}

if(isset($_POST['searchParam']))
{
	$directSearch = true;
	$searchMode = true;
	$searchParamArray = explode(",", $_POST['searchParam']);
	$brandId = $searchParamArray[0];
	$articleId = $searchParamArray[1];
	$genderId = $searchParamArray[2];
	$ageGroupId = $searchParamArray[3];
	$usageId = $searchParamArray[4];
}
if($searchMode)
{
	$searchParam = $brandId.",".$articleId.",".$genderId.",".$ageGroupId.",".$usageId;
	$condition = array(
			'id_attribute_type_values_fk_brand' => $brandId,
			'id_catalogue_classification_fk' => $articleId
			);
	if(!empty($genderId))
	{
		$condition['id_attribute_type_values_fk_gender'] = $genderId;
	}
	if(!empty($ageGroupId))
	{
		$condition['id_attribute_type_values_fk_age_group'] = $ageGroupId;
	}
	if(!empty($usageId))
	{
		$condition['id_attribute_type_values_fk_usage'] = $usageId;
	}
	if(!$directSearch)
	{
		$condition['size_option_1'] = $sizeOption1;
		$condition['size_option_2'] = $sizeOption2;
		$condition['size_option_3'] = $sizeOption3;
	}

	$dbApdater = PhotoshootIssueRuleDAO::getInstance();
	$searchedRuleObjects =  $dbApdater->getPhotoshootIssueRule($condition);
	$searchedRules = array();
	$sizeOptions = getSizeOptionsArray($articleId, $brandId);
	foreach ($searchedRuleObjects as $num=>$rule)
	{
		$searchedRules[$num]['id'] = $rule->getId();
		$searchedRules[$num]['brand_id'] = $rule->getIdBrand();
		$searchedRules[$num]['article_type_id'] = $rule->getIdArticleType();
		$searchedRules[$num]['gender_id'] = $rule->getIdGender();
		$searchedRules[$num]['age_group_id'] = $rule->getIdAgeGroup();
		$searchedRules[$num]['usage_id'] = $rule->getIdUsage();
		$searchedRules[$num]['size_option_1'] = $rule->getSizeOption1();
		$searchedRules[$num]['size_option_2'] = $rule->getSizeOption2();
		$searchedRules[$num]['size_option_3'] = $rule->getSizeOption3();
		$searchedRules[$num]['error_update'] = 0;
		if(isset($updateFailedId))
		{
			if(in_array($rule->getId(), $updateFailedId))
			{
				$searchedRules[$num]['error_update'] = 1;
			}
		}
	}
}

//get brand, gender, age-group, usage
$brands = getAttributeTypeValueArray(1);
$genders = getAttributeTypeValueArray(3);
$ageGroups = getAttributeTypeValueArray(2);
$usages = getAttributeTypeValueArray(10);

//get article type
$articleTypes = func_query("SELECT id, typename as name FROM mk_catalogue_classification where is_active = 1 AND parent1 <> -1 AND parent2 <> -1 ORDER BY typename");

if($searchMode)
{
	$smarty->assign("postmode",  "searchmode");
	$smarty->assign("searchedRules", $searchedRules);
	$smarty->assign("sizeOptions", $sizeOptions);
	$smarty->assign("searchParam", $searchParam);
}
else
{
	$smarty->assign("postmode",  "browsemode");
}
$smarty->assign("brands",  $brands);
$smarty->assign("genders", $genders);
$smarty->assign("ageGroups", $ageGroups);
$smarty->assign("usages", $usages);
$smarty->assign("articleTypes", $articleTypes);
$smarty->assign ("top_message", $topMessage);
$smarty->assign("main","photoshoot_issue_rules");
func_display("admin/home.tpl",$smarty);
?>