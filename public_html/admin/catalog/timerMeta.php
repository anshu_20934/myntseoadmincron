<?php 
require_once "../auth.php";
require_once $xcart_dir."/include/security.php";
use admin\catalog\dao\TimerDBAdapter;

$dbAdapter = new TimerDBAdapter();
if ($mode == "add") {
    $data = $dbAdapter->getMetaData();
    $role = mysql_real_escape_string($add_role);
    $description = mysql_real_escape_string($add_desc);
    $data[$role] = $description;
    $dbAdapter->updateMetaData($data);
}elseif ($mode == "save") {
    $role = $_POST['role'];
    $desc = $_POST['desc'];
    $data = array();                                                                 
    foreach($role as $key=>$value){                                                
        $data[$value] = $desc[$key];                                                                                                                                                        
    }                                                                                
    $dbAdapter->updateMetaData($data);
}

$data = $dbAdapter->getMetaData();

$smarty->assign("data",$data);
$smarty->assign("main","timer_meta");
func_display("admin/home.tpl",$smarty);
?>
