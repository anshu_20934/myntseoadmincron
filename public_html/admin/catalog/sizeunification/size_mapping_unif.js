Ext.onReady(function() {
	Ext.QuickTips.init();
	var loadingMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
    Ext.Ajax.on('requestcomplete', loadingMask.hide, loadingMask);
    Ext.Ajax.on('requestexception', loadingMask.hide, loadingMask);
    var SizeMapping = Ext.data.Record.create([
        {name: 'mappingid',type: 'int',useNull: true},                                         
        {name: 'articletypeid',type: 'int',useNull: true}, 
        {name: 'brandid',type: 'int',useNull: true},
        {name: 'agegroup',useNull: true},
        {name: 'styleid',type: 'int',useNull: true},
        {name: 'scaleid',type: 'int',useNull: true},
        'scale', 
        {name: 'sizeid',type: 'int',useNull: true},
        'size',
        {name: 'unifiedscaleid',type: 'int',useNull: true},
        'unifiedscale',
        {name: 'unifiedsizeid',type: 'int',useNull: true},
        'unifiedsize'
    ]);
    
    var MatchingStyles = Ext.data.Record.create([
	     {name: 'styleid',type: 'int'},                                         
	     {name: 'vendorarticlenumber'}, 
	     {name: 'brandid',type: 'int',useNull: true},
	     {name: 'displayname'},
	     {name: 'articleType'},
	     {name: 'brand'},
	     {name: 'ageGroup'},
	     {name: 'status'},
	     {name: 'sizes'},
	     {name: 'mappedsizes'},
	     {name: 'numsizes'},
	     {name: 'nummappedsizes'},
	     {name: 'nummeasuredsizes'},
	     {name: 'isunified',type:'boolean'},
	 ]);
    
	/////////////////////////Beginning of components for search panel ///////////////////////////////
	///// Article-type selector: combo box
	var articleTypeStore = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : articleTypeData,//this is assigned through smarty in PHP
		sortInfo : {field : 'name', direction : 'ASC'}
	});
	var articleTypeSelector = new Ext.form.ComboBox({
		id : 'articleType',
		name : 'articleType',
		fieldLabel : 'Article Type',
		emptyText : 'Select Article Type',
		resizable : true,
		store : articleTypeStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'id',
		width: 140,
		forceSelection : true,
		triggerAction: 'all'
	});
	
	
	///// Brand selector: combo box
	var brandStore = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : brandsData,//this is assigned through smarty in PHP
		sortInfo : {field : 'name', direction : 'ASC'}
	});
	var brandSelector = new Ext.form.ComboBox({
		id : 'brand',
		name : 'brand',
		fieldLabel : 'Brand',
		emptyText : 'Select a Brand',
		resizable : true,
		store : brandStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'id',
		width: 140,
		forceSelection : true,
		triggerAction: 'all'
	});
	
	
	///// Age-Group selector: combo box
	var ageGroupStore = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : ageGroupsData,//this is assigned through smarty in PHP
		sortInfo : {field : 'name', direction : 'ASC'}
	});
	
	var ageGroupSelector = new Ext.form.ComboBox({
		id : 'ageGroup',
		name : 'ageGroup',
		fieldLabel : 'Age-Group',
		emptyText : 'Select an Age-Group',
		resizable : true,
		store : ageGroupStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'name',
		width: 140,
		forceSelection : true,
		triggerAction: 'all'
	});
	
	//// Style-ID
	var styleIdField = new Ext.form.NumberField({
		id: 'styleId',
		name: 'styleId',
		fieldLabel: 'Style-ID',
		resizeable: true,
		width: 140,
		tabIndex: -1,
	});
	styleIdField.on('change',function(field,newValue,oldValue){
		if(newValue!=''){
			loadingMask.show(); 
			fetchStyleDetails(newValue,null);	
		}
	});

	//// Vendor Article Number
	var vendorArticleNumberField = new Ext.form.TextField({
		id: 'vendorArticleNumber',
		name: 'vendorArticleNumber',
		fieldLabel: 'Vendor Article Number',
		resizeable: true,
		width: 140,
		tabIndex: -1,
	});
	vendorArticleNumberField.on('change',function(field,newValue,oldValue){
		if(newValue!=''){
			loadingMask.show();
			fetchStyleDetails(null,newValue);
		}
	});
	
	//////////////////////////////Search Panel: Unification Mappings/////////////////////////////////
	var searchPanel = new Ext.form.FormPanel({
		id : 'formPanel',
		title: 'Size Unification Mappings',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		labelWidth: 240,
		items : [
		         {
					layout : 'form',
					border : true,
					title: 'Search by Article Type,Brand,Age-Group',
					items : [articleTypeSelector,brandSelector,ageGroupSelector]
		         },
		         {
		        	layout : 'form',
					border : true,
					title: 'Search by Style-ID',
					items : [styleIdField]
		         },
		         {
		        	layout : 'form',
					border : true,
					title: 'Search by Vendor Article Number',
					items : [vendorArticleNumberField]
		         }
		],
		buttons : [
				    {
						text : 'Search',
					    handler : function() {
					    	reloadGrid();
					    }
				    },
				    {
						text : 'Reset',
					    handler : function() {
				    		articleTypeSelector.clearValue();
				    		brandSelector.clearValue();
				    		ageGroupSelector.clearValue();
				    		unifiedScaleSelector.clearValue();
				    		styleIdField.reset();
				    		vendorArticleNumberField.reset();
					    	mappingStore.removeAll();
					    	unifiedScaleStore.removeAll();
					    	scaleStore.removeAll();
					    	sizeStore.removeAll();
					    	matchingStylesStore.removeAll();
					    }
				    },
				    {
						text : '<< Copy Mappings from Parent',
					    handler : function(){copyMappingsFromParentAction();}
				    },
				    {
				    	text: 'Copy Mappings To Target >>',
				    	handler: function(){
				    		//pre-validate for appearance of copy window
				    		var params = getSearchPanelParams();
				    		if (validateForm(params) == false) {
				    			Ext.MessageBox.alert("Cannot copy!!","You have not yet searched for mappings. First search for some mappings and try copying again");
				    		}
				    		else if(mappingStore.getCount() == 0){
				    			Ext.MessageBox.alert("Cannot copy!!","You have either not yet searched,or your search didn't yield any results. First select a criteria which leads to some results and then try copying again");
				    		}
				    		else if(mappingStore.getModifiedRecords().length > 0){
				    			Ext.MessageBox.alert("Cannot copy now","There are un-saved changes in mappings.Please save them before attempting to copy");
				    		}
				    		else {
				    			copyMappingsWindow.show();
				    		}
				    	}
				    }
		]	
	});

	/////////////////////////End of components for search panel ///////////////////////////////
	
	///////////////////////////// Beginning of components for copy-target panel ///////////////
	var targetBrandSelector = new Ext.form.ComboBox({
		id : 'targetBrand',
		name : 'targetBrand',
		fieldLabel : 'Target Brand',
		emptyText : 'Select Target Brand',
		resizable : true,
		store : brandStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'id',
		width: 140,
		forceSelection : true,
		triggerAction: 'all'
	});
	
	var targetAgeGroupSelector = new Ext.form.ComboBox({
		id : 'targetAgeGroup',
		name : 'targetAgeGroup',
		fieldLabel : 'Target Age-Group',
		emptyText : 'Select Target Age-Group',
		resizable : true,
		store : ageGroupStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'name',
		width: 140,
		forceSelection : true,
		triggerAction: 'all'
	});
	
	//// Style-ID
	var targetStyleIdsField = new Ext.form.TextArea({
		id: 'targetStyleIds',
		name: 'targetStyleIds',
		fieldLabel: 'Target Style-IDs',
		resizeable: true,
		width: 140,		
		height: 100,
	});

	var deleteTargetMappingsBeforeCopyField = new Ext.form.Checkbox({
		id: 'deleteTargetMappingsBeforeCopy',
		name: 'deleteTargetMappingsBeforeCopy',
		fieldLabel: 'Delete Target Mappings Before Copy',
		resizeable: true,
	});
 
	var convertToCSV = function(multilinestr){
		var styleIds = [];
		var startIndex = -1;
		for(var i = 0; i< multilinestr.length; i++){
			while(/^\d$/.test(multilinestr.charAt(i)) == false && ++i < multilinestr.length);
			if(/^\d$/.test(multilinestr.charAt(i))){
				startIndex = i;
				while(/^\d$/.test(multilinestr.charAt(i)) && ++i < multilinestr.length);
				styleIds.push(multilinestr.slice(startIndex,i));
			}
		}
		return styleIds.join(",");
	}
	
	var copyMappingsFromParentAction = function(){
		if(validateForCopyFromParent(true)){
			loadingMask.show();
			var mainParams = getSearchPanelParams();
			mainParams['xaction'] = 'copymappings';
			mainParams['styleId'] = '';
			mainParams['vendorArticleNumber'] = '';
			mainParams['targetStyleIds'] = styleIdField.getValue();
			mainParams['deleteTargetMappingsBeforeCopy'] = true;
			copyMappings(mainParams);
		}
	}
	
	var copyMappingsToTargetAction = function(){
		if(validateForCopyToTarget(true)){
			loadingMask.show();
			var mainParams = getSearchPanelParams();
			mainParams['xaction'] = 'copymappings';
			mainParams['targetBrand'] = targetBrandSelector.getValue();
			mainParams['targetAgeGroup'] = targetAgeGroupSelector.getValue();
			mainParams['targetStyleIds'] = convertToCSV(targetStyleIdsField.getValue());
			mainParams['deleteTargetMappingsBeforeCopy'] = deleteTargetMappingsBeforeCopyField.getValue();
			copyMappings(mainParams);
		}
	}
	
	
	var copyMappings = function(mainParams){
		Ext.Ajax.request({
	    	url : '/admin/class/web/controllers/SizeMappingController.php',
	    	timeout: 1800,//30s
	    	params : mainParams,
	    	success : function(response){
	    			if(response.status == "failed"){
						Ext.Msg.show({
							title: "Failed to copy",
							msg: " Failed to copy mappings.."
						}); 
					} 
					else {
						var resp,respHtml;
						try{
							resp = $.parseJSON(response.responseText);
							respHtml = resp.replace(/\n/g,"<br/>");
						}
						catch(error){
							respHtml = response.responseText;
						}
						Ext.MessageBox.show({title:"Copy Results",msg:respHtml});
						copyMappingsWindow.hide();
	        	    }
	        }
	     });
	}
	
	var validateForCopyToTarget = function(showError){
		//Need to validate the following:
		//1.Either style-id is filled, or combination of Brand,Age-Group is filled, but not both
		//2.Source Brand,Target Brand & Source Age-Group,Target Age-Group both should not be the same
		var targetBrand = targetBrandSelector.getValue();
		var targetAgeGroup = targetAgeGroupSelector.getValue();
		var targetStyleIds = targetStyleIdsField.getValue().trim();
		
		if((targetBrand == '' || targetAgeGroup == '') && targetStyleIds ==''){
			if(showError){
				Ext.MessageBox.alert('Error', 'Please select (brand and age-group) or Style-id first');				
			}
			return false;
		}
		if(targetBrand != '' && targetAgeGroup != '' && targetStyleIds!=''){
			if(showError){
				Ext.MessageBox.alert('Error', 'You can either copy to a different Brand-Age-group combination or different style-id, but not both. Please enter values in either Brand-Age-group OR Style-ID, but not both');				
			}
			return false;
		}
		var mainParams = getSearchPanelParams();
		if(targetBrand !='' && targetAgeGroup!=''){
			if(targetBrand == mainParams['brand'] && targetAgeGroup == mainParams['ageGroup']){
				if(showError){
					Ext.MessageBox.alert('Error', 'Source & Target Brand & Age Group cannot be the same.Please select a different Brand or Age-Group.');				
				}
				return false;
			}	
		}
		if(targetStyleIds !=''){
			for(var eachStyleId in targetStyleIds.split(",")){
				if(eachStyleId !=''){
					if(eachStyleId == mainParams['styleId']){
						if(showError){
							Ext.MessageBox.alert('Error', 'Source & Target style-id cannot be the same.Please select a different Brand or Age-Group.');				
						}
						return false;
					}					
				}
			}
		}
 		return true;
	}
	
	var validateForCopyFromParent = function(showError){
		//Need to validate the following:
		//1.style-id has to be filled and it parent brand, article-type,age-group also filled
		var targetStyleIds = styleIdField.getValue();
		if(targetStyleIds ==''){
			if(showError){
				Ext.MessageBox.alert('Error', 'Copy From Parent only works for Individual style-id with available parent Article-type,Brand & Age-Group. Please Enter Style-Id, tab-out and then re-attempt');				
			}
			return false;
		}
		return validateForm(null,true);
	}
	
	//////////////////////////////Copy Size Mappings - Panel/////////////////////////////////
	var copyMappingsWindow = new Ext.Window({
		id : 'copyMappingsWindow',
		title: 'Copy Size Unification Mappings',
		autoHeight : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 480,
		labelWidth: 240,
		closeAction: 'hide',
		items: [{
			xtype: 'form',
			items : [
			         {
						layout : 'form',
						border : true,
						title: 'Copy to Brand,Age-Group combination(Same Article Type)',
						items : [targetBrandSelector,targetAgeGroupSelector]
			         },
			         {
			        	layout : 'form',
						border : true,
						title: 'Copy to Style-IDs(Comma separated)',
						items : [targetStyleIdsField]
			         }
			         ,
			         {
			        	layout : 'form',
						border : true,
						title: 'Delete Mappings in Target before Copying',
						items : [deleteTargetMappingsBeforeCopyField],
						labelWidth: 240
			         }
			]
		}],
		buttons : [
				    {
						text : 'Copy Mappings To Target',
					    handler : function() { copyMappingsToTargetAction();}
				    },
		]
	});
	////////////////////////////End of components for target panel////////////////////////////////////////
	
	
	
	//// Size Scale Combo Selector in Row - data source
	var scaleStore = new Ext.data.Store({
		url: '/admin/class/web/controllers/SizeScaleController.php',
		baseParams: {'asCombo':1,'xaction':'read'},
		pruneModifiedRecords: true,
		autoSave: false,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['scaleid','scale']
		})
	});
	var scaleSelector = new Ext.form.ComboBox({
		id : 'scale',
		name : 'scale',
		fieldLabel : 'Size Scale',
		emptyText : 'Select a Scale',
		resizable : true,
		store : scaleStore,
		mode : 'local',
		displayField : 'scale',
		valueField : 'scaleid',
		width: 140,
		forceSelection : true,
		triggerAction: 'all'
	});
	//TODO: scale index selection is not working??
	scaleSelector.on('expand',function(){
		if(lastScaleRowIndex)
			scaleSelector.setSelectedIndex = lastScaleRowIndex; 		
	});
	//// Size Combo Selector in Row - data source
	var sizeStore = new Ext.data.Store({
		url: '/admin/class/web/controllers/SizeScaleValueController.php',
		baseParams: {'asCombo':1,'xaction':'read','scaleid':null},
		pruneModifiedRecords: true,
		//autoSave: false,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['sizeid','size']
		})
	});
	var sizeSelector = new Ext.form.ComboBox({
		id : 'size',
		name : 'size',
		fieldLabel : 'Size',
		emptyText : 'Select a Size',
		resizable : true,
		store : sizeStore,
		mode : 'local',
		displayField : 'size',
		valueField : 'sizeid',
		width: 140,
		forceSelection : true,
		triggerAction: 'all'
	});
	//
	sizeStore.on('beforeload',function(store,records,options){ 
		sizeSelector.setValue("Loading..");
	});
	sizeStore.on('exception',function(store,action){ 
		sizeSelector.clearValue();
	});
	
	//after the store is loaded, select the size as per selection in Grid Row
	sizeStore.on('load',function(store,records,options){ 
		if(lastSizeIdToSet)
			sizeSelector.setValue(lastSizeIdToSet);
		else 
			sizeSelector.clearValue();
		if(lastSizeRowIndex)
			sizeSelector.setSelectedIndex = lastSizeRowIndex;
	});
	//on change of scale, re-populate the sizeStore
	scaleSelector.on('select',function(store,newValue,oldValue){
		lastSizeIdToSet = null;
		lastSizeRowIndex = null;
		sizeStore.removeAll();
		sizeStore.baseParams["scaleid"]=newValue.get("scaleid");
		sizeStore.load();
	});
	
	//// Unified Size Combo Selector in Row - data source
	var unifiedSizeStore = new Ext.data.Store({
		url: '/admin/class/web/controllers/SizeScaleValueController.php',
		baseParams: {'asCombo':1,'xaction':'read','unified':1,'scaleid':null},
		pruneModifiedRecords: true,
		//autoSave: false,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['unifiedsizeid','unifiedsize']
		})
	});
	var unifiedSizeSelector = new Ext.form.ComboBox({
		id : 'unifiedsize',
		name : 'unifiedsize',
		fieldLabel : 'Unified Size',
		emptyText : 'Select a Unified Size',
		resizable : true,
		store : unifiedSizeStore,
		mode : 'local',
		displayField : 'unifiedsize',
		valueField : 'unifiedsizeid',
		width: 140,
		forceSelection : true,
		triggerAction: 'all'
	});
	//after the store is loaded, select the size as per selection in Grid Row
	unifiedSizeStore.on('load',function(store,records,options){ 
		if(lastUnifiedSizeIdToSet)
			unifiedSizeSelector.setValue(lastUnifiedSizeIdToSet);
		if(lastUnifiedSizeRowIndex)
			unifiedSizeSelector.setSelectedIndex = lastUnifiedSizeRowIndex;
	});
	
	
	////Unified Size Scale Combo Selector in Row - data source
	var unifiedScaleStore = new Ext.data.Store({
		url: '/admin/class/web/controllers/SizeMappingController.php',
		baseParams: {'xaction':'fetchunifiedscales'},
		pruneModifiedRecords: true,
		//autoSave: false,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['unifiedscaleid','unifiedscale']
		})
	});
	unifiedScaleStore.on('load',function(store,records,options){
		if(records.length > 0){
			//by default, the unified scale selector should be Text Size or UK Size
			var indexToSelect = unifiedScaleStore.find('unifiedscale','UK');
			if(indexToSelect == -1){
				indexToSelect = unifiedScaleStore.find('unifiedscale','Text Size');
			}
			indexToSelect = (indexToSelect < 0) ? 0 : indexToSelect; 
			unifiedScaleSelector.setSelectedIndex = indexToSelect;
			unifiedScaleSelector.setValue(records[indexToSelect].get("unifiedscaleid"));	
		}else {
			Ext.MessageBox.alert("Error!","No Scales Exist for the Article-Type; Please define scales for the selected article-type");
		}
	});
	var unifiedScaleSelector = new Ext.form.ComboBox({
		id : 'unifiedscale',
		name : 'unifiedscale',
		fieldLabel : 'Unified Size Scale',
		emptyText : 'Select a Scale',
		resizable : true,
		store : unifiedScaleStore,
		mode : 'local',
		displayField : 'unifiedscale',
		valueField : 'unifiedscaleid',
		width: 140,
		forceSelection : true,
		triggerAction: 'all'
	});
	var unifiedScaleSelectionPanel = new Ext.form.FormPanel({
		id : 'unifiedScaleSelectformPanel',
		title: 'Select Unified Scale',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		labelWidth: 240,
		items : [
		         {
					layout : 'form',
					border : true,
					items : [unifiedScaleSelector]
		         }
		]	
	});
	
	
	///////////////////////////////////////// Apply unification panel/////////////////////////////////////
	//// these two fields are not used now.
	////Style-IDs to exclude from applying/clearing unification
	var styleIdsToExcludeField = new Ext.form.NumberField({
		id: 'styleIdsToExclude',
		name: 'styleIdsToExclude',
		fieldLabel: 'Style-IDs to Exclude(comma separated)',
		resizeable: true,
		width: 460,
		validationEvent: 'blur',
		validator:function(value){
			var van = vendorArticleNumbersToExcludeField.getValue();
			if(vendorArticleNumbersToExcludeField.isValid() && van!='' && value != ''){
				return "You can either search by Style-IDs or Vendor Article Number, but not both";
			}
			return true;
		},
		hidden: true
	});

	//// Vendor Article Numbers to exclude from applying/clearing unification
	var vendorArticleNumbersToExcludeField = new Ext.form.TextField({
		id: 'vansToExclude',
		name: 'vansToExclude',
		fieldLabel: 'Vendor Article Numbers to Exclude(comma separated)',
		resizeable: true,
		width: 460,
		validationEvent: 'blur',
		validator:function(value){
			var styleId = styleIdsToExcludeField.getValue();
			if(styleId!='' && value != ''){
				return "You can either search by Style-IDs or Vendor Article Number, but not both";
			}
			return true;
		},
		hidden: true
	});
	
	var applyUnificationPanel = new Ext.form.FormPanel({
		id : 'applyUnificationformPanel',
		title: 'Apply Unification Mappings to Styles',
		autoHeight : true,
		collapsible : false,
		border : true,
		bodyStyle : 'padding: 5px',
		labelWidth: 320,
		items : [
		         {
					layout : 'form',
					border : true,
					items : [styleIdsToExcludeField,vendorArticleNumbersToExcludeField]
		         }
		],
		buttons : [
				    {
						text : 'Re-Apply Mappings',
					    handler : function() {
					    	reapplyUnification();
					    }
				    }
		]
	});

	///////////////////////////// Matching styles; grid and its components /////////////////////
	var matchingStylesStore = new Ext.data.Store({
		url: '/admin/class/web/controllers/SizeMappingController.php',
		pruneModifiedRecords: true,
		baseParams: {'xaction':'fetchmatchingstyles'},
		//autoSave: false,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : MatchingStyles
		})
	});

	var matchingStylesGrid = new Ext.grid.GridPanel({
	    store : matchingStylesStore,
	    columns: [
	        new Ext.grid.RowNumberer({width: 30}),
	        {id : 'styleid', header : "StyleId", sortable: true, width: 50, dataIndex : 'styleid'},
	        {id : 'vendorarticlenumber', header : "Vendor Article No.", sortable: true, width: 90, dataIndex : 'vendorarticlenumber'},
	        {id : 'displayname', header : "Display Name", sortable: true, width: 110, dataIndex : 'displayname'},
	        {id : 'articletype', header : "Article Type", sortable: true, width: 80, dataIndex : 'articleType'},
	        {id : 'brandid', header : "Brand", sortable: true, width: 80, dataIndex : 'brand'},
	        {id : 'agegroup', header : "Age-Group", sortable: true, width: 80, dataIndex : 'ageGroup'},
	        {id : 'status', header : "State", sortable: true, width: 40, dataIndex : 'status'},
	        {id : 'isunified', header : "Unified?", sortable: true, width: 45, dataIndex : 'isunified'},
	        {id : 'numsizes', header : "#Sizes", sortable: true, width: 40, dataIndex : 'numsizes'},
	        {id : 'nummappedsizes', header : "#Mapped Sizes", sortable: true, width: 55, dataIndex : 'nummappedsizes'},
	        {id : 'numsizeswithmeasurements', header : "#Measured Sizes", sortable: true, width: 60, dataIndex : 'nummeasuredsizes'},
	        {id : 'sizes', header : "Sizes", sortable: true, width: 100, dataIndex : 'sizes'},
	        {id : 'mappedsizes', header : "Mapped Sizes", sortable: true, width: 100, dataIndex : 'mappedsizes'},
	  	],
	    id : 'matchingStylesGrid-panel',
	    title : 'Styles matching selection criteria',
	    frame : true,
		loadMask : false,
		autoHeight : true,
		stripeRows : true,
		autoScroll : true,
		collapsible: true,
		tbar: [
				{
					text: 'Select All',
				    handler: function() {
				    	matchingStylesGrid.getSelectionModel().selectAll();
				    }
				},
				{
					text: '-',
					xtype: 'tbseparator'
				},
				{
					text: 'Select None',
					handler: function() {
						matchingStylesGrid.getSelectionModel().clearSelections();
					}
				}
		      ]
    });
	
	var lastSizeIdToSet = null, lastUnifiedSizeIdToSet = null, lastSizeRowIndex = null, lastUnifiedSizeRowIndex = null, lastScaleRowIndex = null;  
	/// Row Editor for each Mapping 
	var editor = new Ext.ux.grid.RowEditor({
	    listeners: {
	    	beforeedit: function(editorL,rowIndex){
	    		var store = editorL.grid.store;
	    		var record = store.getAt(rowIndex);
	    		//by default, when editor opens, since the field is tied to the id field, it would show id on load.
	    		//but the display text is available in combo-box only after the store for the combo-box is loaded. 
	    		//so initialize it with loading text.
	    		sizeSelector.setValue("Loading...");
	    		unifiedSizeSelector.setValue("Loading...");

	    		var scaleid = record.get("scaleid"),unifiedscaleid = record.get("unifiedscaleid");
	    		//given the scaleid, find its row-index in the scaleStore
	    		//scale store is loaded during search itself, so
	    		var indexOfScaleSelection = scaleStore.find('scaleid',scaleid);
	    		scaleSelector.setSelectedIndex = indexOfScaleSelection;
	    			    		
                //load the sizeStore & unifiedSizeStore based on the current selection value of size-scale
	    		sizeStore.baseParams['scaleid']=scaleid;
	    		sizeStore.load();
	    		unifiedSizeStore.baseParams['scaleid']=unifiedscaleid;
	    		unifiedSizeStore.load();
	    		
	    		var indexOfSizeSelection = sizeStore.find('sizeid',record.get("sizeid"));
	    		var indexOfUnifiedSizeSelection = unifiedSizeStore.find('unifiedsizeid',record.get("unifiedsizeid"));
	    		
	    		//set some global variables so as to select relevant indices & values in combo-boxes after the combo store loads
	    		lastSizeIdToSet = record.get("sizeid"), 
	    		lastSizeRowIndex = indexOfSizeSelection, 
	    		lastUnifiedSizeIdToSet = record.get("unifiedsizeid"),
	    		lastUnifiedSizeRowIndex = indexOfUnifiedSizeSelection,
	    		lastScaleRowIndex = indexOfScaleSelection;
	    		return true;
	    	},
	    	validateedit: function(editor,changes,record,rowIndex){
	    		//there are changes in size selection and record doesn't already have size : so it is a new record
	    		if(changes.sizeid && !(record.get("sizeid"))) {
					var indexOfThisSize = editor.grid.getStore().find("sizeid",changes.sizeid);
					if(indexOfThisSize > -1){
						Ext.MessageBox.alert("Invalid Data","Selected Size is already mapped. Cannot re-map the same size");
						return false;
					}
	    		}
	    		return true;
	    	},
	        canceledit: function(editorL,forced){
		       if(forced){//means explicit cancel was pressed
		    	   editorL.stopEditing();
		    	   var store = editorL.grid.getStore();
			       var grid = editorL.grid;
		    	   var selections = grid.getSelectionModel().getSelections();
		    	   for (var i = 0; i < selections.length; i++) {
    		          var row = selections[i];
    		          //if the record doesn't have measurementid, then it is a phantom record created on client side
    		          if (row.data.articletypeid && !row.data.mappingid){
    		        	  store.remove(row);    		        	  
    		          }
    		       }
		       }
	        },
		    afteredit: function(editorL,changes,record,rowIndex){
		    	var store = editorL.grid.getStore();
		    	store.commitChanges();
		    	store.reload();
		    }
	    }//end listeners
	 });
 
	/////////////////// Store for the Main mappings Grid ///////////////////////////	
	var mappingStore = new Ext.data.Store({
		url: '/admin/class/web/controllers/SizeMappingController.php',
		pruneModifiedRecords: true,
		autoSave: true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : SizeMapping
		}),
		writer: new Ext.data.JsonWriter({
			returnJson: true,
			writeAllFields: false,
		})
	});
	//////////////////// The main Mappings Grid ////////////////////////////////////
	
	var getMappingToolbar = function(){
		return [
				{
					icon: '../images/table_add.png',
					cls: 'x-btn-text-icon',
					text: 'Add New Mapping',
					handler: addMapping,
				},
				{
					text: '-',
					xtype: 'tbseparator'
				},
				{
					icon: '/admin/extjs/resources/images/icons/reload.gif',
					cls: 'x-btn-text-icon',
					text: 'Refresh',
					handler: reloadGrid
				},
				{
					text: '-',
					xtype: 'tbseparator'
				},
				{
					text: "Open Scales Screen",
					handler: function(){
						openWindow("size_scales_unif.php");
					}
				},
				{
					text: '-',
					xtype: 'tbseparator'
				},
				{
					text: "Open Measurement Screen",
					handler: function(){
						openWindow("size_measurement_unif.php");
					}
				}
		      ];//end of toolbar
	}
	
	var sizeMappingsGrid = new Ext.grid.GridPanel({
	    store : mappingStore,
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {id : 'mappingid', header : "MappingId", sortable: true, width: 70, dataIndex : 'mappingid'},
	        {id : 'articletypeid', header : "ArticleTypeId", sortable: true, width: 75, dataIndex : 'articletypeid'},
	        {id : 'brandid', header : "Brand-Id", sortable: true, width: 70, dataIndex : 'brandid'},
	        {id : 'agegroup', header : "Age-Group", sortable: true, width: 90, dataIndex : 'agegroup'},
	        {id : 'styleid', header : "StyleId", sortable: true, width: 60, dataIndex : 'styleid'},
	        {id : 'scale', header : "Scale", sortable: false, width: 70, dataIndex : 'scale',editor: scaleSelector},
	        {id : 'size', header : "Size", sortable: false, width: 70, dataIndex : 'sizeid', editor: sizeSelector, 
	        		renderer: function(val, metaData, record, rowIndex, colIndex, store){
	        			return record.get("size");
	        		}
	        },
            {id : 'unifiedscale', header : "Unified Scale", sortable: false, width: 80, dataIndex : 'unifiedscale'},
            {id : 'unifiedsize', header : "Unified Size", sortable: false, width: 70, dataIndex : 'unifiedsizeid', editor: unifiedSizeSelector,
            	renderer: function(val, metaData, record, rowIndex, colIndex, store){
        			return record.get("unifiedsize");
        		}	
            },
            {id: 'delete', header: 'Delete', sortable: false, width: 80, 
	        	renderer: function(val, metaData, record, rowIndex, colIndex, store){
	        		return "<span class='link delete'>Delete</span>";
	        	},
	        }
	  	],
	  	
	  	
	  	listeners: {
	  		cellclick: function (o, rowid, column, e) {
	  			var store = o.getStore();
  	            var record = store.getAt(rowid);
  	            if(e.target.classList.contains("delete")){
  	            	deleteMapping(store,record);
			    }
	  		}
	    }//end listeners
	  	,
	    id : 'sizeMappingsGrid-panel',
	    baseTitle: 'Mappings for Selected',
	    title : 'Size Mappings',
	    plugins: [editor],
	    frame : true,
		loadMask : false,
		autoHeight : true,
		collapsible: true,
		stripeRows : true,
		autoScroll : false,
		tbar: getMappingToolbar()    	
	});

	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [searchPanel,unifiedScaleSelectionPanel,sizeMappingsGrid,matchingStylesGrid,applyUnificationPanel],
		width : 1024,
	});
	
	function openWindow(basePhpUrl){
		var params = getSearchPanelParams();
		var paramStr = '';
		for(p in params){
			if(params.hasOwnProperty(p)){
				if(paramStr !=''){
					paramStr += "&";
				}
				paramStr += (p+"="+encodeURI(params[p]));
			}
		}
		window.open(basePhpUrl+"?"+paramStr);		
	}
	
	populateSearchParamsOnLoad();
	
	//Select the Search Parameters on page load 
	function populateSearchParamsOnLoad(){
		
		if(selectedArticleTypeId !=''){
			articleTypeSelector.getStore().clearFilter();
			articleTypeSelector.setValue(parseInt(selectedArticleTypeId));
		}
		if(selectedBrandId !=''){
			brandSelector.getStore().clearFilter();
			brandSelector.setValue(parseInt(selectedBrandId));
		}
		if(selectedAgeGroup !=''){
			ageGroupSelector.getStore().clearFilter();
			ageGroupSelector.setValue(selectedAgeGroup);
		}
		if(selectedStyleId !=''){
			styleIdField.setRawValue(selectedStyleId);	
		}
		if(selectedVendorArticleNumber !=''){
			vendorArticleNumberField.setRawValue(selectedVendorArticleNumber);	
		}
		if(validateForm(null,false)){
			reloadGrid();
		}
	}
	
	function addMapping(){
		var params = getSearchPanelParams();
		if(!validateForm(params)){
			return;
		}
	  	var newSizeMapping = new SizeMapping({
	  		mappingid: null,
	  		brandid: params['brand'],
	  		articletypeid: params['articleType'],
	  		agegroup: params['ageGroup'],
	  		styleid: params['styleId'],
	  		sizeid: '',
	  		unifiedscaleid: unifiedScaleSelector.getValue(),
	  		unifiedscale: unifiedScaleSelector.getRawValue(),
	  		unifiedsizeid:''
	  	});
	    editor.stopEditing();
	    var store = editor.grid.store;
	    store.insert(0, newSizeMapping);
	    sizeMappingsGrid.getView().refresh();
	    sizeMappingsGrid.getSelectionModel().selectRow(0);
	    editor.startEditing(0);
	}
	
	function deleteMapping(store,record){
		
		Ext.Msg.confirm('Confirm Deletion', 'Are you sure you want to delete? It cannot be undone', function(button) {
		    if (button === 'yes') {
			    Ext.Ajax.request({
			    	url : '/admin/class/web/controllers/SizeMappingController.php',
			    	params : {
		    			'xaction': "delete",
		    			'mappingid': record.get('mappingid')
			    	},
			    	success : function(response){
			    			response = $.parseJSON(response.responseText);
							if(response.status == "failed"){
								Ext.Msg.show({
									title: response.title,
									msg: response.message
									
								}); 
							} 
							else {
								Ext.MessageBox.alert("Deleted","Scale Deleted Successfully");
			        	    }
							store.reload();
			        }
			     });
		    }
		});
	}
	function fetchStyleDetails(styleId,vendorArticleNumber){
		Ext.Ajax.request({
	    	url : '/admin/class/web/controllers/SizeMappingController.php',
	    	params : {
    			'xaction': "fetchstyledetails",
    			'styleId': styleId,
    			'vendorArticleNumber': vendorArticleNumber
	    	},
	    	success : function(response){
	    			response = $.parseJSON(response.responseText);
					if(response.status == "failed"){
						Ext.Msg.show({
							title: response.title,
							msg: response.message
						}); 
					} 
					else if(response.results && response.count > 0){
						var fields = response.results[0];
						//Clear all the selection and filter applid due to previous selection
						articleTypeSelector.getStore().clearFilter();
			    		brandSelector.getStore().clearFilter();
			    		ageGroupSelector.getStore().clearFilter();
						
			    		articleTypeSelector.setValue(fields.articletypeid);
						brandSelector.setValue(fields.brandid);
						ageGroupSelector.setValue(fields.agegroup);
						
						styleIdField.setRawValue(fields.styleid);
						vendorArticleNumberField.setRawValue(fields.vendorarticlenumber);
	        	    }
	        }
	     });
	}
	
	function getSelectedStyleIds(){
		var sm = matchingStylesGrid.getSelectionModel();
		var selections = sm.getSelections();
		if(selections.length > 0){
			var styleIdArr = []; 
			for(var i=0;i<selections.length;i++){
				styleIdArr.push(selections[i].data["styleid"]);
			}
			return styleIdArr.join(",");	
		}
		return false;
	}
	
	function reapplyUnification(){
		var styleIds = getSelectedStyleIds();
		if(styleIds == false){
			Ext.MessageBox.alert("Cannot Apply Unification Mappings","No Style Selected to Apply Unification Mappings. Please select one or more styles");
			return;
		}
		//Need to send a ajax request to clear and re-apply unification
		sendAjaxRequestForUnification("reapplymappingsforstyles",styleIds,"Re-Apply Unification Mappings");
	}
	
	function clearUnification(){
		var styleIds = getSelectedStyleIds();
		if(styleIds == false){
			Ext.MessageBox.alert("Cannot Clear Unification Mappings","No Style Selected to Clear Unification Mappings. Please select one or more styles");
			return;
		}
		//Need to send a ajax request to clear unification
		sendAjaxRequestForUnification("clearmappingsforstyles",styleIds,"Clear Unification Mappings");
	}

	function sendAjaxRequestForUnification(xaction,styleIds,task){
		loadingMask.show();
		Ext.Ajax.request({
	    	url : '/admin/class/web/controllers/SizeUnificationController.php',
	    	timeout: 3600000,//1 hour
	    	params : {
    			'xaction': xaction,
    			'styleIds': styleIds
	    	},
	    	success : function(response){
	    			if(response.status == "failed"){
						Ext.Msg.show({
							title: "Failed",
							msg: task+" to selected styles Failed.."
							
						}); 
					} 
					else {
						var resp,respHtml;
						try{
							resp = $.parseJSON(response.responseText);
							respHtml = resp.Message.replace(/\n/g,"<br/>");
						}
						catch(error){
							respHtml = response.responseText;
						}
						Ext.Msg.show({title:"Successful!",msg:respHtml});
	        	    }
	        }
	     });

	}
	function validateForm(params,showError){
		if(typeof params == 'undefined'||params==null){
			var params = getSearchPanelParams();
		}
		if(typeof showError == 'undefined'){
			var showError = true;
		}
		if (!(params["articleType"] && params["ageGroup"] && params["brand"]) && (!params["styleId"]) && (!params["vendorArticleNumber"])) {
			if(showError){
				Ext.MessageBox.alert('Error', 'Please select (article-type,brand and age-group) or (Enter a style-id) or (Enter a vendor-article-number) first');	
			}
			return false;
		}
		return true;
	}
	
	function reloadGrid() {
		var params = getSearchPanelParams();
		if (validateForm(params)) {
			loadingMask.show();
			var form = searchPanel.getForm();
			
			if(params["styleId"]|| params["vendorArticleNumber"]){
				params['searchBy']='style';
			}
			else if(params["articleType"] && params["ageGroup"] && params["brand"]){
				params['searchBy']='combination';
			}
    		mappingStore.baseParams = params;
    		mappingStore.removeAll();
    		mappingStore.load();
    		for(var key in params){
    			if(params.hasOwnProperty(key)){
    				scaleStore.baseParams[key] = params[key];
    				unifiedScaleStore.baseParams[key] = params[key];
    				matchingStylesStore.baseParams[key] = params[key];
    			}
    		}
    		scaleStore.removeAll();
    		scaleStore.load();
    		unifiedScaleStore.removeAll();
    		unifiedScaleStore.load();
    		matchingStylesStore.removeAll();
    		matchingStylesStore.load();
    		//var articleTypeField = form.findField("articleType");
    		//sizeMappingsGrid.setTitle(sizeMappingsGrid.baseTitle + " - "+articleTypeField.lastSelectionText+"("+articleTypeField.value+")");
		}
	}
	
	function getSearchPanelParams() {
		var form = searchPanel.getForm();
		if (form.isValid()) {
			var params = {};
			params["articleType"] = form.findField("articleType").getValue();
			params["ageGroup"] = form.findField("ageGroup").getValue();
			params["brand"] = form.findField("brand").getValue();
			params["styleId"] = form.findField("styleId").getValue();
			params["vendorArticleNumber"] = form.findField("vendorArticleNumber").getValue();
			return params;
		}
		return false;
	}
});