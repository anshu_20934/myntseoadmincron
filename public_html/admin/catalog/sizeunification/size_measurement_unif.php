<?php
require_once "../../auth.php";
require_once "$xcart_dir/include/security.php";
include_once "$xcart_dir/include/func/funcStyleProperties.php";
require_once "../styleAttributeId.php";
$uniqueId = uniqid();
$smarty->assign('progresskey', $uniqueId);

//get brand, age-group, gender, fashion type, colour, season, year, usage
$articleType = getCatalogCategory("ArticleType",false);
$brand       = getAttributeTypeValueArray(ATTRIBUTE_BRAND_ID);
$ageGroup    = getAttributeTypeValueArray(ATTRIBUTE_AGE_GROUP_ID);

$smarty->assign("brands",         $brand);
$smarty->assign("ageGroups",      $ageGroup);
$smarty->assign("articleType",    $articleType);

$smarty->assign("selectedBrandId",RequestVars::getVar("brand",null));
$smarty->assign("selectedArticleTypeId",RequestVars::getVar("articleType",null));
$smarty->assign("selectedAgeGroup",RequestVars::getVar("ageGroup",null));
$smarty->assign("selectedStyleId",RequestVars::getVar("styleId",null));
$smarty->assign("selectedVendorArticleNumber",RequestVars::getVar("vendorArticleNumber",null));

$smarty->assign("main","size_measurement_unif");
func_display("admin/home.tpl",$smarty);
?>