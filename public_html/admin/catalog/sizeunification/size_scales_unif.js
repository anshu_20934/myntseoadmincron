Ext.onReady(function() {
	Ext.QuickTips.init();
	var loadingMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
    Ext.Ajax.on('requestcomplete', loadingMask.hide, loadingMask);
    Ext.Ajax.on('requestexception', loadingMask.hide, loadingMask);
    
    var SizeScale = Ext.data.Record.create([
        {name: 'scaleid',type: 'int',useNull: true},                                         
        'name', 
        {name:'articletypeid',type:'int'},
        'human_readable_scale_name', 
        'backward_compat_scale_name'
    ]);
    
    var SizeScaleValue = Ext.data.Record.create([
        {name: 'scalevalueid',type: 'int',useNull: true},                                         
        'sizevalue', 
        {name:'scaleid',type:'int'}, 
        {name:'sortorder',type:'int'}
    ]);
    
	var articleTypeStore = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : articleTypeData,//this is assigned through smarty in PHP
		sortInfo : {field : 'name', direction : 'ASC'}
	});
	
	var articleType = new Ext.form.ComboBox({
		id : 'articleType',
		name : 'articleType',
		fieldLabel : 'Article Type',
		emptyText : 'Select Article Type',
		resizable : true,
		store : articleTypeStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'id',
		width: 140,
		forceSelection : true,
		triggerAction: 'all',
		listeners: {
			select: function(field, newValue, oldValue){
				reloadScalesGrid();
			}
		}
	});
	

	//// Size Scale Combo Selector in Row - data source
	var scalesStore = new Ext.data.Store({
		url: '/admin/class/web/controllers/SizeScaleController.php',
		pruneModifiedRecords: true,
		//autoSave: false,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['scaleid','scale']
		})
	});
	
	var searchPanel = new Ext.form.FormPanel({
		id : 'formPanel',
		autoHeight : true,
		collapsible : false,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 720,
		title: 'Select Article Type to Manage Size-Scales',
		items : [{
			layout : 'form',
			border : false,
			items : [
			         {items : [articleType]},
			        ]
			}],
	});
	
	function reloadScalesGrid() {
		var params = getSearchPanelParams();
		
		if (params == false) {
			Ext.MessageBox.alert('Error', 'Please select an article-type first');
			return false;
		}
		else {
			loadingMask.show();
			var form = searchPanel.getForm();
			
			scalesStore.baseParams = params;
    		scalesStore.baseParams['asCombo']=1;
    		scalesStore.baseParams['xaction']='read';
    		scalesStore.load();
    		
    		articleTypeBasedScalesStore.baseParams = params;
    		articleTypeBasedScalesStore.baseParams['asCombo']=0;
    		articleTypeBasedScalesStore.load();
    		var articleTypeField = form.findField("articleType");
    		sizeScalesGrid.setTitle(sizeScalesGrid.baseTitle + " - "+articleTypeField.lastSelectionText+"("+articleTypeField.value+")");
		}
	}
	
	function getSearchPanelParams() {
		var form = searchPanel.getForm();
		if (form.isValid()) {
			var params = {};
			params["articleType"] = form.findField("articleType").getValue();
			return params;
		}
		return false;
	}
	
	var articleTypeBasedScalesStore = new Ext.data.Store({
		url: '/admin/class/web/controllers/SizeScaleController.php',
		pruneModifiedRecords: true,
		//autoSave: false,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : SizeScale
		}),
		writer: new Ext.data.JsonWriter({
			returnJson: true,
			writeAllFields: false,
		})
	});

	var sizeScaleValueStore = new Ext.data.Store({
		url: '/admin/class/web/controllers/SizeScaleValueController.php',
		pruneModifiedRecords: true,
		//autoSave: false,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : SizeScaleValue
		}),
		writer: new Ext.data.JsonWriter({
			returnJson: true,
			writeAllFields: false,
		})
	});

	
	var editor = new Ext.ux.grid.RowEditor({
	    listeners: {
	        canceledit: function(editorL,forced){
		       if(forced){//means explicit cancel was pressed
			       var store = editorL.grid.getStore();
			       store.rejectChanges();
			       store.reload();
		       }
	        },
		    afteredit: function(editorL,changes,record,rowIndex){
		    	var store = editorL.grid.getStore();
		    	store.commitChanges();
		    	store.reload();
		    }
	    }//end listeners
	 });
	
	var addNewScale = function(){

	  	var form = searchPanel.getForm();
		var articleTypeId = form.findField("articleType").getValue();
		if(!articleTypeId){
			Ext.Msg.show({
				title: "Cannot add size scale now",
				msg: "Please select an Article-Type First"
			});							
			return;
		}
	  	var newSizeScale = new SizeScale({
	  		scaleid: null,                                         
	  	    name: '',
	  	    articletypeid: articleTypeId, 
	  	    human_readable_scale_name: '', 
	  	    backward_compat_scale_name: ''                		  
	  	});
	  	  
	  	var store = sizeScalesGrid.getStore();
	    editor.stopEditing();
	    store.insert(0, newSizeScale);
	    sizeScalesGrid.getView().refresh();
	    sizeScalesGrid.getSelectionModel().selectRow(0);
	    editor.startEditing(0);
	}

	var getToolBarButtons = function(){
		return [
				{
					icon: '../images/table_add.png',
					cls: 'x-btn-text-icon',
					text: 'Add New Scale',
					handler: addNewScale
				},
				{
					text: '-',
					xtype: 'tbseparator'
				},
				{
					icon: '/admin/extjs/resources/images/icons/reload.gif',
					cls: 'x-btn-text-icon',
					text: 'Refresh',
					handler: reloadScalesGrid
				},
				{
					text: '-',
					xtype: 'tbseparator'
				}
				,
				{
					html: "<span style='color:darkblue;text-decoration:underline'>Open Mapping Screen</span>",
					handler: function(){
						openWindow("size_mapping_unif.php");
					}
				},
				{
					text: '-',
					xtype: 'tbseparator'
				},
				{
					html: "<span style='color:darkblue;text-decoration:underline'>Open Measurement Screen</span>",
					handler: function(){
						openWindow("size_measurement_unif.php");
					}
				}
		      ];//end of toolbar
	}
	
	function openWindow(basePhpUrl){
		var params = getSearchPanelParams();
		var paramStr = '';
		for(p in params){
			if(params.hasOwnProperty(p)){
				if(paramStr !=''){
					paramStr += "&";
				}
				paramStr += (p+"="+encodeURI(params[p]));
			}
		}
		window.open(basePhpUrl+"?"+paramStr);		
	}
	
	var fetchSizes = function(record){
		//console.log("move up at row(0 based):"+rowid+" ,col(1-based):"+column);
      	//over here: set the baseParams for 2nd data store (related to scale values: Sizes)
		loadingMask.show();
		var selectedScaleId = record.get('scaleid');
		sizeScaleValueStore.removeAll();
		sizeScaleValueStore.baseParams = {"xaction": "read","scaleid": selectedScaleId};
		sizeScaleValueStore.load();
		sizeScaleValuesGrid.sizeScaleId = selectedScaleId;
		sizeScaleValuesGrid.setTitle(sizeScaleValuesGrid.baseTitle+ " - "+record.get('name')+"("+selectedScaleId+")");
	}
	
	var deleteScale = function(record, store){
      	Ext.Msg.confirm('Confirm Deletion', 'Are you sure you want to delete? It cannot be undone', function(button) {
		    if (button === 'yes') {
			    Ext.Ajax.request({
			    	url : '/admin/class/web/controllers/SizeScaleController.php',
			    	params : {
		    			'xaction': "delete",
		    			'articleType':record.get('articletypeid'),
		    			'scaleid': record.get('scaleid')
			    	},
			    	success : function(response){
			    		var wasSuccessful = true;
			    		if(response.responseText){
			    			//try to JSON-parse the response text
			    			responseJson = $.parseJSON(response.responseText);
							if(responseJson && responseJson.status == "failed"){
								Ext.Msg.show({
									title: responseJson.title,
									msg: responseJson.message
								});
								wasSuccessful = false;
							}
			    		}
			    		if(wasSuccessful) {
			    			Ext.MessageBox.alert("Deleted","Scale Deleted Successfully");
			        	}	
			    		store.reload();
			        }
			     });
    		    }
      	});
	}
	
	var sizeScalesGrid = new Ext.grid.GridPanel({
	    store : articleTypeBasedScalesStore,
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {id : 'scaleid', header : "Scale Id", sortable: true, width: 50, dataIndex : 'scaleid', editor: null},
	        {id : 'name', header : "Scale Name", sortable: true, width: 80, dataIndex : 'name', editor: {
                xtype: 'textfield',
                allowBlank: false
            }},
	        {id : 'articletypeid', header : "Article Type", sortable: false, width: 70, dataIndex : 'articletypeid', editor: null},
	        {id : 'human_readable_scale_name', header : "Human Readable Name", sortable: false, width: 100, dataIndex : 'human_readable_scale_name', editor:{
                xtype: 'textfield',
                allowBlank: false
            }},
	        {id : 'backward_compat_scale_name', header : "Backward Compat Name", sortable: false, width: 100, dataIndex : 'backward_compat_scale_name', editor: {
                xtype: 'textfield',
                allowBlank: true
            }},
	        {id: 'fetchsizes', header: 'Fetch Sizes', sortable: false, width: 80, 
	        	renderer: function(val, metaData, record, rowIndex, colIndex, store){
	        		return "<span class='link fetch-sizes'>Fetch Sizes</span>";
	        	},
	        },
	        {id: 'delete', header: 'Delete', sortable: false, width: 80, 
	        	renderer: function(val, metaData, record, rowIndex, colIndex, store){
	        		return "<span class='link delete'>Delete Scale</span>";
	        	},
	        }
	  	],
	  	
	  	
	  	listeners: {
	  		cellclick: function (o, rowid, column, e) {
	  			var store = o.getStore();
  	            var record = store.getAt(rowid);
  	            if(e.target.classList.contains("fetch-sizes")){
  	            	fetchSizes(record);
  	            } 
  	            else if(e.target.classList.contains("delete")){
  	            	deleteScale(record,store);
  	            }
	  		}
	    }//end listeners
	  	,
	    id : 'sizeScalesGrid-panel',
	    baseTitle: 'Scales for Selected Article-Type',
	    title : 'Scales for Selected Article-Type',
	    plugins: [editor],
	    frame : true,
		loadMask : false,
		autoHeight : true,
		//bbar : pagingBar,
		stripeRows : true,
		autoScroll : true,
		tbar: getToolBarButtons()    	
	});
	
	var editorForSize = new Ext.ux.grid.RowEditor({
	    listeners: {
	        canceledit: function(editorL,forced){
		       if(forced){//means explicit cancel was pressed
			       var store = editorL.grid.getStore();
			       store.rejectChanges();
			       store.reload();
		       }
	        },
		    afteredit: function(editorL,changes,record,rowIndex){
		    	var store = editorL.grid.getStore();
		    	store.commitChanges();
		    	store.reload();
		    }
	    }//end listeners
	 });
	
	var sizeMoveUp = function(record,store){
        var params = {
	    			'xaction': "moveUp",
	    			'scaleid':record.get('scaleid'),
	    			'sortorder':record.get('sortorder'),
	    			'scalevalueid': record.get('scalevalueid')
		    	}, 
		    successMessageTitle = "Moved!", 
		    successMessage = "Size Moved Up Successfully",
		    task = "Move Up";
        sizeAction(params,store,successMessageTitle,successMessage,task);
	}
	var sizeMoveDown = function(record,store){
		var params = {
	    			'xaction': "moveDown",
	    			'scaleid':record.get('scaleid'),
	    			'sortorder':record.get('sortorder'),
	    			'scalevalueid': record.get('scalevalueid')
		    	}, 
		    successMessageTitle = "Moved!", 
		    successMessage = "Size Moved Down Successfully",
		    task = "Move Down";
		sizeAction(params,store,successMessageTitle,successMessage,task);
	}
	var sizeDelete = function(record,store){
    	var params = {
    			'xaction': "delete",
    			'scalevalueid': record.get('scalevalueid')
	    	}, 
	    	successMessageTitle = "Deleted!", 
	    	successMessage = "Size Deleted Successfully",
	    	task = "Delete";
    	sizeAction(params,store,successMessageTitle,successMessage,task);
	}
	var sizeAction = function(params,store,successMessageTitle,successMessage,task){
		if(params){
    		store.reload();
    		store.sort('sortorder','ASC');
    		Ext.Msg.confirm('Confirm '+task, 'Are you sure you want to '+task+'? It cannot be undone', function(button) {
  		    if (button === 'yes') {
	  	    	Ext.Ajax.request({
			    	url : '/admin/class/web/controllers/SizeScaleValueController.php',
			    	params : params,
			    	success : function(response){
			    		var wasSuccessful = true;
			    		if(response.responseText){
			    			//try to JSON-parse the response text
			    			var responseJson = $.parseJSON(response.responseText);
							if(responseJson && responseJson.status == "failed"){
								Ext.Msg.show({
									title: responseJson.title,
									msg: responseJson.message
								});
								wasSuccessful = false;
							}
			    		}
			    		if(wasSuccessful) {
								Ext.MessageBox.alert(successMessageTitle,successMessage);
			        	}
						store.reload();
						store.sort('sortorder','ASC');
			        }
			    });//end of ajax request
  		    }//end of yes
  		});//end of confirm
		}//end of if(params)
	}
	var addNewSize = function(){
    	if(!sizeScaleValuesGrid.sizeScaleId){
    		Ext.Msg.show({
				title: "Cannot add size value now",
				msg: "Please select a size scale first & fetch its sizes"
			});							
			return;return;
    	}
    	var newSize = new SizeScaleValue({
    						scalevalueid: null,                                         
    	                    sizevalue: '', 
    	                    scaleid:sizeScaleValuesGrid.sizeScaleId, 
    	                    sortorder: ''
    	});
    	var store = sizeScaleValuesGrid.getStore();
    	editorForSize.stopEditing();
    	store.insert(0, newSize);
    	sizeScaleValuesGrid.getView().refresh();
    	sizeScaleValuesGrid.getSelectionModel().selectRow(0);
    	editorForSize.startEditing(0);
	}
	
	var getSizeToolbar = function(){
		return [
			{
				icon: '../images/table_add.png',
				cls: 'x-btn-text-icon',
				text: 'Add New Size',
			    handler: addNewSize
			},
			{
				text: '-',
				xtype: 'tbseparator'
			},
			{
				icon: '/admin/extjs/resources/images/icons/reload.gif',
				cls: 'x-btn-text-icon',
				text: 'Refresh',
				handler: function() {
					var store = sizeScaleValuesGrid.getStore();
					store.removeAll();
					store.reload();
				}
			}
	      ];
	}
	
	var sizeScaleValuesGrid = new Ext.grid.GridPanel({
	    store : sizeScaleValueStore,//'scalevalueid', 'sizevalue',  'scaleid', 'sortorder',
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {id : 'scalevalueid', header : 'Size Id', sortable: true, width: 70, dataIndex : 'scalevalueid'},
	        {id : 'sizevalue', header : 'Size Name', sortable: true, width: 150, dataIndex : 'sizevalue', editor: {
                xtype: 'textfield',
                allowBlank: false
            }},
	        {id : 'scaleid', header : 'Scale ID', sortable: true, width: 70, dataIndex : 'scaleid'},
	        {id: 'sortorder', header: 'Sort Order', sortable: true, width: 70, dataIndex: 'sortorder','type':'int'},
	        {id: 'moveup', header: 'Move Up', sortable: false, width: 70, 
	        	renderer: function(val, metaData, record, rowIndex, colIndex, store){
	        		return "<span class='link move-up'>Move Up</span>";
	        	},
	        },
	        {id: 'movedown', header: 'Move Down', sortable: false, width: 70, 
	        	renderer: function(val, metaData, record, rowIndex, colIndex, store){
	        		return "<span class='link move-down'>Move Down</span>";
	        	},
	        },
	        {id: 'delete', header: 'Delete', sortable: false, width: 70, 
	        	renderer: function(val, metaData, record, rowIndex, colIndex, store){
	        		return "<span class='link delete'>Delete</span>";
	        	},
	        }
	  	],
	  	listeners: {
	  		cellclick: function (o, rowid, column, e) {
	  			var store = o.getStore();
  	            var record = store.getAt(rowid);
  	            if(e.target.classList.contains("move-up")){
  	            	sizeMoveUp(record,store);
  	            } 
  	            else if(e.target.classList.contains("move-down")){
  	            	sizeMoveDown(record,store);
  	            }
  	            else if(e.target.classList.contains("delete")){
  	            	sizeDelete(record,store);
	  	    	}
	  		}//end of cell click
	  	}//end of listeners
	  	,
	    id : 'sizeScaleValuesGrid-panel',
	    baseTitle: 'Sizes in Selected Scale',
	    title : 'Sizes in Selected Scale',
	    sizeScaleId: null,
	    plugins: [editorForSize],
	    frame : true,
		loadMask : false,
		autoHeight : true,
		stripeRows : true,
		autoScroll : true,
		tbar: getSizeToolbar()
    });
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [searchPanel, sizeScalesGrid, sizeScaleValuesGrid],
		width : 720,
	});
	populateSearchParamsOnLoad();
	
	//Select the Search Parameters on page load 
	function populateSearchParamsOnLoad(){
		if(selectedArticleTypeId !=''){
			articleType.getStore().clearFilter();
			articleType.setValue(parseInt(selectedArticleTypeId));
			reloadScalesGrid();
		}
	}
});