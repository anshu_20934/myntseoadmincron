Ext.onReady(function() {
	Ext.QuickTips.init();
	var loadingMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
    Ext.Ajax.on('requestcomplete', loadingMask.hide, loadingMask);
    Ext.Ajax.on('requestexception', loadingMask.hide, loadingMask);
    var SizeMeasurement = Ext.data.Record.create([
        {name: 'measurementid',type: 'int',useNull: true},                                         
        {name: 'articletypeid',type: 'int',useNull: true}, 
        {name: 'brandid',type: 'int',useNull: true},
        {name: 'agegroup',useNull: true},
        {name: 'styleid',type: 'int',useNull: true},
        {name: 'sizeid',type: 'int',useNull: true},
        'size',
        'measurement',
        'fields',
        'values',
        'unit'
    ]);
    
    var MatchingStyles = Ext.data.Record.create([
	     {name: 'styleid',type: 'int'},                                         
	     {name: 'vendorarticlenumber'}, 
	     {name: 'brandid',type: 'int',useNull: true},
	     {name: 'displayname'},
	     {name: 'articleType'},
	     {name: 'brand'},
	     {name: 'ageGroup'},
	     {name: 'sizes'},
	     {name: 'mappedsizes'},
	     {name: 'numsizes'},
	     {name: 'nummappedsizes'},
	     {name: 'nummeasuredsizes'},
	     {name: 'status'},
	     {name: 'isunified',type:'boolean'},
	 ]);
    
	///// Article-type selector: combo box
	var articleTypeStore = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : articleTypeData,//this is assigned through smarty in PHP
		sortInfo : {field : 'name', direction : 'ASC'}
	});
	var articleTypeSelector = new Ext.form.ComboBox({
		id : 'articleType',
		name : 'articleType',
		fieldLabel : 'Article Type',
		emptyText : 'Select Article Type',
		resizable : true,
		store : articleTypeStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'id',
		width: 140,
		forceSelection : true,
		triggerAction: 'all'
	});
	
	
	///// Brand selector: combo box
	var brandStore = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : brandsData,//this is assigned through smarty in PHP
		sortInfo : {field : 'name', direction : 'ASC'}
	});
	var brandSelector = new Ext.form.ComboBox({
		id : 'brand',
		name : 'brand',
		fieldLabel : 'Brand',
		emptyText : 'Select a Brand',
		resizable : true,
		store : brandStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'id',
		width: 140,
		forceSelection : true,
		triggerAction: 'all'
	});
	
	
	///// Age-Group selector: combo box
	var ageGroupStore = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : ageGroupsData,//this is assigned through smarty in PHP
		sortInfo : {field : 'name', direction : 'ASC'}
	});
	
	var ageGroupSelector = new Ext.form.ComboBox({
		id : 'ageGroup',
		name : 'ageGroup',
		fieldLabel : 'Age-Group',
		emptyText : 'Select an Age-Group',
		resizable : true,
		store : ageGroupStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'name',
		width: 140,
		forceSelection : true,
		triggerAction: 'all'
	});
	
	//// Style-ID
	var styleIdField = new Ext.form.NumberField({
		id: 'styleId',
		name: 'styleId',
		fieldLabel: 'Style-ID',
		resizeable: true,
		width: 140,
		tabIndex: -1
	});
	styleIdField.on('change',function(field,newValue,oldValue){
		if(newValue!=''){
			loadingMask.show(); 
			fetchStyleDetails(newValue,null);	
		}
	});

	//// Vendor Article Number
	var vendorArticleNumberField = new Ext.form.TextField({
		id: 'vendorArticleNumber',
		name: 'vendorArticleNumber',
		fieldLabel: 'Vendor Article Number',
		resizeable: true,
		width: 140,
		tabIndex: -1,
	});
	vendorArticleNumberField.on('change',function(field,newValue,oldValue){
		if(newValue!=''){
			loadingMask.show();
			fetchStyleDetails(null,newValue);
		}
	});
	
	//// Size Combo Selector in Row - data source
	var sizeStore = new Ext.data.Store({
		url: '/admin/class/web/controllers/SizeMappingController.php',
		baseParams: {'xaction':'fetchunifiedsizes'},
		pruneModifiedRecords: true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	idProperty: 'unifiedsizeid',
	  	  	fields : ['unifiedsizeid','unifiedsize']
		})
	});
	var sizeSelector = new Ext.form.ComboBox({
		id : 'size',
		name : 'size',
		fieldLabel : 'Size',
		emptyText : 'Select a Unified Size',
		resizable : true,
		store : sizeStore,
		mode : 'local',
		displayField : 'unifiedsize',
		valueField : 'unifiedsizeid',
		width: 140,
		forceSelection : true,
		triggerAction: 'all'
	});
	//
	sizeStore.on('beforeload',function(store,records,options){ 
		sizeSelector.setValue("Loading..");
	});
	sizeStore.on('exception',function(store,action){ 
		sizeSelector.clearValue();
	});
	//after the store is loaded, select the size as per selection in Grid Row
	sizeStore.on('load',function(store,records,options){
		if(!records || records.length < 1){
			Ext.MessageBox.alert("Fatal Error","There are no unified sizes available for the search criteria. Please define at least 1 unified size for the same criteria in the Mapping Screen before you can add Measurements for the same criteria");
			sizeSelector.emptyText = 'No Unified Size.Define 1 in Mapping Screen';
		}
		else {
			sizeSelector.emptyText = 'Select Unified Size';
		}
		if(lastSizeIdToSet){
			sizeSelector.setValue(lastSizeIdToSet);
		}
		else {
			sizeSelector.clearValue();
		} 
		if(lastSizeRowIndex) {
			sizeSelector.setSelectedIndex = lastSizeRowIndex;		
		}
	});
	
	var searchPanel = new Ext.form.FormPanel({
		id : 'formPanel',
		title: 'Size Unification Measurements',
		autoHeight : true,
		collapsible : false,
		border : true,
		bodyStyle : 'padding: 5px',
		labelWidth: 240,
		collapsible: true,
		items : [
		         {
					layout : 'form',
					border : true,
					title: 'Search by Article Type,Brand,Age-Group',
					items : [articleTypeSelector,brandSelector,ageGroupSelector]
		         },
		         {
		        	layout : 'form',
					border : true,
					title: 'Search by Style-ID',
					items : [styleIdField]
		         },
		         {
		        	layout : 'form',
					border : true,
					title: 'Search by Vendor Article Number',
					items : [vendorArticleNumberField]
		         }
		],
		buttons : [
				    {
						text : 'Search',
					    handler : function() {
					    	reloadGrid();
					    }
				    },
				    {
						text : 'Reset',
					    handler : function() {
				    		articleTypeSelector.clearValue();
				    		brandSelector.clearValue();
				    		ageGroupSelector.clearValue();
				    		styleIdField.reset();
				    		vendorArticleNumberField.reset();
					    	measurementStore.removeAll();
					    	matchingStylesStore.removeAll();
					    	sizeStore.removeAll();
					    }
				    },
				    {
						text : '<< Copy Measurements from Parent',
					    handler : function(){copyMeasurementsFromParentAction();}
				    },
				    {
				    	text: 'Copy Measurements To Target >>',
				    	handler: function(){
				    		//pre-validate for appearance of copy window
				    		if (validateSearchForm(null,false) == false) {
				    			Ext.MessageBox.alert("Cannot copy!!","You have not yet searched for measurements. First search for some measurements and try copying again");
				    		}
				    		else if(measurementStore.getCount() == 0){
				    			Ext.MessageBox.alert("Cannot copy!!","You have either not yet searched,or your search didn't yield any results. First select a criteria which leads to some results and then try copying again");
				    		}
				    		else if(measurementStore.getModifiedRecords().length > 0){
				    			Ext.MessageBox.alert("Cannot copy now","There are un-saved changes in measurements.Please save them before attempting to copy");
				    		}
				    		else {
				    			copyMeasurementsWindow.show();
				    		}
				    	}
				    }
		]	
	});
/////////////////////////End of components for search panel ///////////////////////////////
	
	///////////////////////////// Beginning of components for copy-target panel ///////////////
	var targetBrandSelector = new Ext.form.ComboBox({
		id : 'targetBrand',
		name : 'targetBrand',
		fieldLabel : 'Target Brand',
		emptyText : 'Select Target Brand',
		resizable : true,
		store : brandStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'id',
		width: 140,
		forceSelection : true,
		triggerAction: 'all'
	});
	
	var targetAgeGroupSelector = new Ext.form.ComboBox({
		id : 'targetAgeGroup',
		name : 'targetAgeGroup',
		fieldLabel : 'Target Age-Group',
		emptyText : 'Select Target Age-Group',
		resizable : true,
		store : ageGroupStore,
		mode : 'local',
		displayField : 'name',
		valueField : 'name',
		width: 140,
		forceSelection : true,
		triggerAction: 'all'
	});
	
	//// Style-ID
	var targetStyleIdsField = new Ext.form.TextArea({
		id: 'targetStyleIds',
		name: 'targetStyleIds',
		fieldLabel: 'Target Style-IDs',
		resizeable: true,
		width: 140,		
		height: 100,
	});

	var deleteTargetMeasurementsBeforeCopyField = new Ext.form.Checkbox({
		id: 'deleteTargetMeasurementsBeforeCopy',
		name: 'deleteTargetMeasurementsBeforeCopy',
		fieldLabel: 'Delete Target Measurements Before Copy',
		resizeable: true,
	});
 
	var convertToCSV = function(multilinestr){
		var styleIds = [];
		var startIndex = -1;
		for(var i = 0; i< multilinestr.length; i++){
			while(/^\d$/.test(multilinestr.charAt(i)) == false && ++i < multilinestr.length);
			if(/^\d$/.test(multilinestr.charAt(i))){
				startIndex = i;
				while(/^\d$/.test(multilinestr.charAt(i)) && ++i < multilinestr.length);
				styleIds.push(multilinestr.slice(startIndex,i));
			}
		}
		return styleIds.join(",");
	}
	
	var copyMeasurementsFromParentAction = function(){
		if(validateForCopyFromParent(true)){
			loadingMask.show();
			var mainParams = getSearchPanelParams();
			mainParams['xaction'] = 'copymeasurements';
			mainParams['styleId'] = '';
			mainParams['vendorArticleNumber'] = '';
			mainParams['targetStyleIds'] = styleIdField.getValue();
			mainParams['deleteTargetMeasurementsBeforeCopy'] = true;
			copyMeasurements(mainParams);
		}
	}
	
	var copyMeasurementsToTargetAction = function(){
		if(validateForCopyToTarget(true)){
			loadingMask.show();
			var mainParams = getSearchPanelParams();
			mainParams['xaction'] = 'copymeasurements';
			mainParams['targetBrand'] = targetBrandSelector.getValue();
			mainParams['targetAgeGroup'] = targetAgeGroupSelector.getValue();
			mainParams['targetStyleIds'] = convertToCSV(targetStyleIdsField.getValue());
			mainParams['deleteTargetMeasurementsBeforeCopy'] = deleteTargetMeasurementsBeforeCopyField.getValue();
			copyMeasurements(mainParams);
		}
	}
	
	
	var copyMeasurements = function(mainParams){
		Ext.Ajax.request({
	    	url : '/admin/class/web/controllers/SizeMeasurementController.php',
	    	timeout: 1800,//30s
	    	params : mainParams,
	    	success : function(response){
	    			if(response.status == "failed"){
						Ext.Msg.show({
							title: "Failed to copy",
							msg: " Failed to copy measurements.."
						}); 
					} 
					else {
						var resp,respHtml;
						try{
							resp = $.parseJSON(response.responseText);
							respHtml = resp.replace(/\n/g,"<br/>");
						}
						catch(error){
							respHtml = response.responseText;
						}
						Ext.MessageBox.show({title:"Copy Results",msg:respHtml});
						copyMeasurementsWindow.hide();
	        	    }
	        }
	     });
	}
	
	var validateForCopyToTarget = function(showError){
		//Need to validate the following:
		//1.Either style-id is filled, or combination of Brand,Age-Group is filled, but not both
		//2.Source Brand,Target Brand & Source Age-Group,Target Age-Group both should not be the same
		var targetBrand = targetBrandSelector.getValue();
		var targetAgeGroup = targetAgeGroupSelector.getValue();
		var targetStyleIds = targetStyleIdsField.getValue().trim();
		
		if((targetBrand == '' || targetAgeGroup == '') && targetStyleIds ==''){
			if(showError){
				Ext.MessageBox.alert('Error', 'Please select (brand and age-group) or Style-id first');				
			}
			return false;
		}
		if(targetBrand != '' && targetAgeGroup != '' && targetStyleIds!=''){
			if(showError){
				Ext.MessageBox.alert('Error', 'You can either copy to a different Brand-Age-group combination or different style-id, but not both. Please enter values in either Brand-Age-group OR Style-ID, but not both');				
			}
			return false;
		}
		var mainParams = getSearchPanelParams();
		if(targetBrand !='' && targetAgeGroup!=''){
			if(targetBrand == mainParams['brand'] && targetAgeGroup == mainParams['ageGroup']){
				if(showError){
					Ext.MessageBox.alert('Error', 'Source & Target Brand & Age Group cannot be the same.Please select a different Brand or Age-Group.');				
				}
				return false;
			}	
		}
		if(targetStyleIds !=''){
			for(var eachStyleId in targetStyleIds.split(",")){
				if(eachStyleId !=''){
					if(eachStyleId == mainParams['styleId']){
						if(showError){
							Ext.MessageBox.alert('Error', 'Source & Target style-id cannot be the same.Please select a different Brand or Age-Group.');				
						}
						return false;
					}					
				}
			}
		}
 		return true;
	}
	
	var validateForCopyFromParent = function(showError){
		//Need to validate the following:
		//1.style-id has to be filled and it parent brand, article-type,age-group also filled
		var targetStyleIds = styleIdField.getValue();
		if(targetStyleIds ==''){
			if(showError){
				Ext.MessageBox.alert('Error', 'Copy From Parent only works for Individual style-id with available parent Article-type,Brand & Age-Group. Please Enter Style-Id, tab-out and then re-attempt');				
			}
			return false;
		}
		return validateSearchForm(null,true);
	}
	
	//////////////////////////////Copy Size Measurements - Panel/////////////////////////////////
	var copyMeasurementsWindow = new Ext.Window({
		id : 'copyMeasurementsWindow',
		title: 'Copy Size Unification Measurements',
		autoHeight : true,
		closeAction: 'hide',
		border : true,
		bodyStyle : 'padding: 5px',
		width : 480,
		labelWidth: 240,
		items: [{
			xtype: 'form',
			items : [
			         {
						layout : 'form',
						border : true,
						title: 'Copy to Brand,Age-Group combination(Same Article Type)',
						items : [targetBrandSelector,targetAgeGroupSelector]
			         },
			         {
			        	layout : 'form',
						border : true,
						title: 'Copy to Style-IDs(Comma separated)',
						items : [targetStyleIdsField]
			         }
			         ,
			         {
			        	layout : 'form',
						border : true,
						title: 'Delete Measurements in Target before Copying',
						items : [deleteTargetMeasurementsBeforeCopyField],
						labelWidth: 240
			         }
			]
		}],
		buttons : [
				    {
						text : 'Copy Measurements to Target',
					    handler : copyMeasurementsToTargetAction
				    },
				    
		]
	});
	////////////////////////////End of components for target panel////////////////////////////////////////
	

	
	
	
	
	
	////Measurement Selection Panel
	var measurementCheckboxGroup = new Ext.form.CheckboxGroup({
		id:'measurements',
		xtype: 'checkboxgroup',
		fieldLabel: 'Select Measurements to add',
		 itemCls: 'x-check-group-alt',
		    // Put all controls in a single column with width 100%
		    columns: 4,
		    items: getMeasurementCBsToAdd(),
	});
	var unitsStore = new Ext.data.ArrayStore({
		fields: ['unit'],
		data :  [['inches'],['cm'],['mm'],['gm'],['mg']]
	});
	var unitSelectionComboField = new Ext.form.ComboBox({
		store: unitsStore,
		displayField:'unit',
		fieldLabel: 'Select Unit',
		width: 100,
		typeAhead: true,
		valueField:'unit',
	    mode: 'local',
	    forceSelection: true,
	    triggerAction: 'all',
	    emptyText:'Select Unit',
	    selectOnFocus:true,
	});
	//This is the master set of all possible measurements: ideally this should come from sizechart-position.js
	function getMeasurementCBsToAdd(){
		var items = [];
		var allMeasurementNames = {};
		if(typeof sizePositionObj != 'undefined'){
			for(var ageGroup in sizePositionObj){ 
				if(sizePositionObj.hasOwnProperty(ageGroup)){ 
					for(var articleType in sizePositionObj[ageGroup]){
						if(sizePositionObj[ageGroup].hasOwnProperty(articleType)){
							for(var measureName in sizePositionObj[ageGroup][articleType]){
								if(sizePositionObj[ageGroup][articleType].hasOwnProperty(measureName)){
									allMeasurementNames[measureName] = '';
								}
							}
						}
					} 
				} 
			}
			var allMeaurementsArray = [];
			for(var eachMeasurement in allMeasurementNames){
				allMeaurementsArray.push(eachMeasurement);
			}
			allMeaurementsArray.sort();
			for(var i=0; i<allMeaurementsArray.length;i++){
				eachMeasurement = allMeaurementsArray[i];
				items.push({boxLabel: eachMeasurement, name: "m-"+eachMeasurement, visible: false});				
			}
		}
		else {
				var labels = [
		              	"Length","Width","Waist","Chest","Shoulder","Bottom","Thigh",
		              	"Bottom Opening","Poocha","Collar","Cuffs","Bust","Overbust","Underbust",
		              	"Hips","Across Shoulder","To fit Bust","To fit Hips",
		              	"To fit Waist","To fit Chest","To fit foot length","To fit foot width",
		              	"Body Length","Inseam Length","Outseam Length","Sleeve Length"
		              ];
				for(var i=0; i<labels.length;i++){
					items.push({boxLabel: labels[i], name: "m-"+labels[i], visible: false});
				}
		}
		return items;
	}
	var selectMeasurementsPanel = new Ext.form.FormPanel({
		id : 'selectMeasurementsFormPanel',
		title: 'Select Measurements & Unit to be used for adding measurements',
		autoHeight : true,
		collapsible : true,
		collapsed: true,
		border : true,
		bodyStyle : 'padding: 5px',
		labelWidth: 200,
		items : [
		         {
					layout : 'form',
					border : true,
					items : [unitSelectionComboField,measurementCheckboxGroup]
		         }
		],
		buttons : [
				    {
						text : 'Apply',
					    handler : function() {
					    	var mparams = getMeasureSelectionPanelParams();
							if(validateAddMeasurementForm(mparams)){
								selectMeasurementsPanel.collapse();
							}
							else {
								Ext.MessageBox.alert("Invalid Measurements","You need to select 1 or more measurement(s) and Unit");
							}
					    }
				    },
				    {
						text : 'Reset',
					    handler : function() {
					    	measurementCheckboxGroup.reset();
					    	unitSelectionComboField.reset();					    	
					    }
				    }
		]
	});	
	
	//// Apply unification panel
	////Style-IDs to exclude from applying/clearing unification
	var styleIdsToExcludeField = new Ext.form.TextField({
		id: 'styleIdsToExclude',
		name: 'styleIdsToExclude',
		fieldLabel: 'Style-IDs to Exclude(comma separated)',
		resizeable: true,
		width: 460,
		validationEvent: 'blur',
		validator:function(value){
			var van = vendorArticleNumbersToExcludeField.getValue();
			if(vendorArticleNumbersToExcludeField.isValid() && van!='' && value != ''){
				return "You can either search by Style-IDs or Vendor Article Number, but not both";
			}
			return true;
		},
		hidden: true
	});

	//// Vendor Article Numbers to exclude from applying/clearing unification
	var vendorArticleNumbersToExcludeField = new Ext.form.TextField({
		id: 'vansToExclude',
		name: 'vansToExclude',
		fieldLabel: 'Vendor Article Numbers to Exclude(comma separated)',
		resizeable: true,
		width: 460,
		validationEvent: 'blur',
		validator:function(value){
			var styleId = styleIdsToExcludeField.getValue();
			if(styleId!='' && value != ''){
				return "You can either search by Style-IDs or Vendor Article Number, but not both";
			}
			return true;
		},
		hidden: true
	});
	
	var applyUnificationPanel = new Ext.form.FormPanel({
		id : 'applyUnificationformPanel',
		title: 'Apply Unification Measurements to Styles',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		labelWidth: 320,
		items : [
		         {
					layout : 'form',
					border : true,
					items : [styleIdsToExcludeField,vendorArticleNumbersToExcludeField]
		         }
		],
		buttons : [
				    {
						text : 'Re-Apply Measurements',
					    handler : function() {
					    	reapplyUnification();
					    }
				    },
				    {
				    	text : 'Re-Apply Size-Chart Image',
					    handler : function() {
					    	reapplySizeChartImages();
					    }
				    },
				    {
				    	text : 'Clear Style Cache(PDP,SEARCH)',
					    handler : function() {
					    	clearStyleCache();
					    }
				    },
		]
	});
	
	var measurementStore = new Ext.data.Store({
		url: '/admin/class/web/controllers/SizeMeasurementController.php',
		pruneModifiedRecords: true,
		autoSave: true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : SizeMeasurement
		}),
		writer: new Ext.data.JsonWriter({
			returnJson: true,
			writeAllFields: false,
		})
	});
	measurementStore.on('load',function(store,records,options){
		if(!records || records.length < 1){
			selectMeasurementsPanel.expand();
			return;
		}
		store.autoSave = false;
		for(var i=0;i<records.length;i++){
			var measureParams = getMeasurementParams(records[i].get("measurement"));
			records[i].set('fields',measureParams['fields']);
			records[i].set('values',measureParams['values']);
			records[i].set('unit',measureParams['unit']);
		}
		var xaction = store.baseParams['xaction'];
		store.baseParams['xaction'] = 'nothing';
		store.commitChanges();
		store.baseParams['xaction'] = xaction;
		store.autoSave = true;
	});
	
	var matchingStylesStore = new Ext.data.Store({
		url: '/admin/class/web/controllers/SizeMappingController.php',
		pruneModifiedRecords: true,
		baseParams: {'xaction':'fetchmatchingstyles'},
		//autoSave: false,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : MatchingStyles
		})
	});
	var matchingStylesGrid = new Ext.grid.GridPanel({
	    store : matchingStylesStore,
	    columns: [
	        new Ext.grid.RowNumberer({width: 30}),
	        {id : 'styleid', header : "StyleId", sortable: true, width: 60, dataIndex : 'styleid'},
	        {id : 'vendorarticlenumber', header : "Vendor Article No.", sortable: true, width: 90, dataIndex : 'vendorarticlenumber'},
	        {id : 'displayname', header : "Display Name", sortable: true, width: 100, dataIndex : 'displayname'},
	        {id : 'articletype', header : "Article Type", sortable: true, width: 80, dataIndex : 'articleType'},
	        {id : 'brandid', header : "Brand", sortable: true, width: 80, dataIndex : 'brand'},
	        {id : 'agegroup', header : "Age-Group", sortable: true, width: 80, dataIndex : 'ageGroup'},
	        {id : 'status', header : "State", sortable: true, width: 40, dataIndex : 'status'},
	        {id : 'isunified', header : "Unified?", sortable: true, width: 60, dataIndex : 'isunified'},
	        {id : 'numsizes', header : "#Sizes", sortable: true, width: 40, dataIndex : 'numsizes'},
	        {id : 'nummappedsizes', header : "#Mapped Sizes", sortable: true, width: 50, dataIndex : 'nummappedsizes'},
	        {id : 'nummeasuredsizes', header : "#Measured Sizes", sortable: true, width: 60, dataIndex : 'nummeasuredsizes'},
	        {id : 'sizes', header : "Sizes", sortable: true, width: 100, dataIndex : 'sizes'},
	        {id : 'unifiedsizes', header : "Unified Sizes", sortable: true, width: 100, dataIndex : 'mappedsizes',"style":{"margin-right": "4px"}},
	  	],
	    id : 'matchingStylesGrid-panel',
	    title : 'Styles matching selection criteria',
	    frame : true,
		loadMask : false,
		autoHeight : true,
		stripeRows : true,
		autoScroll : false,
		collapsible: true,
		tbar: [
				{
					text: 'Select All',
				    handler: function() {
				    	matchingStylesGrid.getSelectionModel().selectAll();
				    }
				},
				{
					text: '-',
					xtype: 'tbseparator'
				},
				{
					text: 'Select None',
					handler: function() {
						matchingStylesGrid.getSelectionModel().clearSelections();
					}
				}
		      ]
    });
	
	var lastSizeIdToSet = null, lastSizeRowIndex = null;  
	/// Row Editor for each Measurement 
	var editor = new Ext.ux.grid.RowEditor({
	    listeners: {
	    	beforeedit: function(editorL,rowIndex){
	    		
	    		var store = editorL.grid.store;
	    		var record = store.getAt(rowIndex);
	    		//by default, when editor opens, since the field is tied to the id field, it would show id on load.
	    		//but the display text is available in combo-box only after the store for the combo-box is loaded. 
	    		//so initialize it with loading text.
	    		sizeSelector.setValue("Loading...");
	    		
                //load the sizeStore based on the current selection value of size-scale
	    		var indexOfSizeSelection = sizeStore.find('sizeid',record.get("sizeid"));
	    		
	    		//set some global variables so as to select relevant indices & values in combo-boxes after the combo store loads
	    		lastSizeIdToSet = record.get("sizeid"), 
	    		lastSizeRowIndex = indexOfSizeSelection; 
	    		return true;
	    	},
	    	validateedit: function(editor,changes,record,rowIndex){
	    		//if measurement values were changed, validate that
	    		if(changes.values){
		    		var fieldsStr = record.get("fields").trim(),
		    			valuesStr = changes.values,
		    			unit	= record.get("unit").trim(),
		    			valar	= valuesStr.split(','); 

		    		//count the number of commas and it should be equal in measurement-names & values
					if(valar.length != fieldsStr.split(',').length){
						Ext.MessageBox.alert("Invalid Measurements","Number of measurement values is not equal to number of measurements");
						return false; //"Number of measurement values is not equal to number of measurements";
					}
					var valar = valuesStr.split(',');
					for(var i=0;i<valar.length;i++){
						if(valar[i]!=''){
							//each number has to be a natural/decimal & it should not end with .0 or . 
							if(!(/^\d+[.]?(\d){0,2}$/.test(valar[i])) || (/[.]0$/.test(valar[i])) ||(/[.]$/.test(valar[i])) || (/^0\d/.test(valar[i]))){
								Ext.MessageBox.alert("Invalid Measurements","Please enter comma separated natural/decimal numbers for measurements.Max 2 digits are allowed after decimal");
								return false;
							}
						}
					}
	    		}
	    		else if(!record.get("values")){
	    			Ext.MessageBox.alert("Invalid Measurements","You did not enter measurements,neither it is already there in the record");
	    			return false;
	    		}
				if(!changes.sizeid && !(record.get("sizeid"))){
					Ext.MessageBox.alert("Invalid Data","No Unified Size Selected to define measurements");
					return false;
				}
				//we also need to verify that the unified size for which we are trying to define measurements doesn't already have a defined measurement
				//for this, we need to search for 
				else if(changes.sizeid && !(record.get("sizeid"))) {
					var indexOfThisSize = editor.grid.getStore().find("sizeid",changes.sizeid);
					if(indexOfThisSize > -1){
						Ext.MessageBox.alert("Invalid Data","Selected Unified Size already has measurements defined. Cannot redefine");
						return false;
					} 
				}
				return true;
				/*
				//TODO: add regexp support
				var isMatched = /^(\d+(?:[.]?\d)*)+(?:,(\d+(?:[.]?\d)*)+)*$/.test(valuesStr);
				//known bug: if the number is entered with dual decimal points, it would still pass validation
				//also,if there are blanks in regex,it would be treated as invalid
				return isMatched;*/
	    	},
	        canceledit: function(editorL,forced){
		       if(forced){//means explicit cancel was pressed
		    	   editorL.stopEditing();
		    	   var store = editorL.grid.getStore();
			       var grid = editorL.grid;
		    	   var selections = grid.getSelectionModel().getSelections();
		    	   for (var i = 0; i < selections.length; i++) {
    		          var row = selections[i];
    		          //if the record doesn't have measurementid, then it is a phantom record created on client side
    		          if (row.data.articletypeid && !row.data.measurementid){
    		        	  store.remove(row);    		        	  
    		          }
    		       }
		    	   store.reload();
		       }
	        },
		    afteredit: function(editorL,changes,record,rowIndex){
		    	var fields = record.get("fields"),
					values = record.get("values"),
					unit   = record.get("unit");
		    	var measurementJson = getMeasurementJson(fields,values,unit);
		    	record.set("measurement",measurementJson);
		    	var store = editorL.grid.getStore();
		    	store.commitChanges();
		    	store.reload();
		    }
	    }//end listeners
	 });
 
	var measurementValuesEditorField = new Ext.form.TextField({
		id: 'measurementValues',
		name: 'measurementValues',
		fieldLabel: 'Measurement Values',
		resizeable: true,
		width: 140,
		maskRe: /^[0-9.,]*$/,/*
		validateOnBlur: false,
		validationEvent: 'change',
		invalidText: "Please enter only as many comma separated decimal numbers as there are measurements",
		validator: function(value){
			var valar = value.split(",");
			for(var i=0;i<valar.length;i++){
				console.log(valar[i]);
				if(valar!='' && /^\d+(?:.\d)*$/.test(valar[i]) == false){
					return "Please enter only as many comma separated decimal numbers as there are measurements";
				}
			}
			return true;
			if(/^(\d+(?:[.]?\d)*)+(?:,(\d+(?:[.]?\d)*)+)*$/.test(value) == false){
				return "Please enter only as many comma separated decimal numbers as there are measurements";
			}else {
				return true;
			}
		}*/
	});
	
	var sizeMeasurementsGrid = new Ext.grid.GridPanel({
	    store : measurementStore,
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {id : 'measurementid', header : "MeasureId", sortable: true, width: 60, dataIndex : 'measurementid'},
	        {id : 'articletypeid', header : "ArticleTypeId", sortable: true, width: 75, dataIndex : 'articletypeid'},
	        {id : 'brandid', header : "Brand-Id", sortable: true, width: 50, dataIndex : 'brandid'},
	        {id : 'agegroup', header : "Age-Group", sortable: true, width: 80, dataIndex : 'agegroup'},
	        {id : 'styleid', header : "StyleId", sortable: true, width: 60, dataIndex : 'styleid'},
	        {id : 'size', header : "Size", sortable: false, width: 70, dataIndex : 'sizeid', editor: sizeSelector, 
	        		renderer: function(val, metaData, record, rowIndex, colIndex, store){
	        			return record.get("size");
	        		}
	        },
	        {id : 'measurementname', header : "Fields", sortable: false, width: 180,dataIndex:'fields'},
	        {id : 'measurementvalues', header : "Values", sortable: false, width: 120,dataIndex:'values',  
	        	editor:measurementValuesEditorField
	        },
	        {id : 'measurementunit', header : "Unit", sortable: false, width: 65,dataIndex:'unit'},
            {id: 'delete', header: 'Delete', sortable: false, width: 70, 
	        	renderer: function(val, metaData, record, rowIndex, colIndex, store){
	        		return "<span class='link delete'>Delete</span>";
	        	},
	        }
	  	],
	  	
	  	listeners: {
	  		cellclick: function (o, rowid, column, e) {
	  			var store = o.getStore();
  	            var record = store.getAt(rowid);
  	            if(e.target.classList.contains("delete")){
  	            	deleteMeasurement(store,record);
			    }
	  		}
	    }//end listeners
	  	,
	    id : 'sizeMeasurementsGrid-panel',
	    baseTitle: 'Measurements for Selected',
	    title : 'Size Measurements',
	    plugins: [editor],
	    frame : true,
		loadMask : false,
		boxMinHeight: 240,
		autoHeight : true,
		collapsible: true,
		//bbar : pagingBar,
		stripeRows : true,
		autoScroll : true,
		tbar: [
				{
					icon: '../images/table_add.png',
					cls: 'x-btn-text-icon',
					text: 'Add New Measurement',
					handler: addMeasurement,
				},
				{
					text: '-',
					xtype: 'tbseparator'
				},
				{
					icon: '/admin/extjs/resources/images/icons/reload.gif',
					cls: 'x-btn-text-icon',
					text: 'Refresh',
					handler: reloadGrid
				},
				{
					text: '-',
					xtype: 'tbseparator'
				},
				{
					html: "Open Scales Screen",
					handler: function(){
						openWindow("size_scales_unif.php");
					}
				},
				{
					text: '-',
					xtype: 'tbseparator'
				},
				{
					html: "Open Mapping Screen",
					handler: function(){
						openWindow("size_mapping_unif.php");
					}
				}
		],//end of toolbar
    });//end of grid panel


	function openWindow(basePhpUrl){
		var params = getSearchPanelParams();
		var paramStr = '';
		for(p in params){
			if(params.hasOwnProperty(p)){
				if(paramStr !=''){
					paramStr += "&";
				}
				paramStr += (p+"="+encodeURI(params[p]));
			}
		}
		window.open(basePhpUrl+"?"+paramStr);		
	}
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [searchPanel,selectMeasurementsPanel,sizeMeasurementsGrid,matchingStylesGrid,applyUnificationPanel],
		width : 1024,
	});
	
	populateSearchParamsOnLoad();
	
	//Select the Search Parameters on page load 
	function populateSearchParamsOnLoad(){
		
		if(selectedArticleTypeId !=''){
			articleTypeSelector.getStore().clearFilter();
			articleTypeSelector.setValue(parseInt(selectedArticleTypeId));
		}
		if(selectedBrandId !=''){
			brandSelector.getStore().clearFilter();
			brandSelector.setValue(parseInt(selectedBrandId));
		}
		if(selectedAgeGroup !=''){
			ageGroupSelector.getStore().clearFilter();
			ageGroupSelector.setValue(selectedAgeGroup);
		}
		if(selectedStyleId !=''){
			styleIdField.setValue(selectedStyleId);	
		}
		if(selectedVendorArticleNumber !=''){
			vendorArticleNumberField.setValue(selectedVendorArticleNumber);	
		}
		if(validateSearchForm(null,false)){
			reloadGrid();
		}
	}
	function getMeasurementParams(measureJsonStr){
		var fields ='',values='',unit='',ret=[];
		if(measureJsonStr){
			var measurementJSON = $.parseJSON(measureJsonStr);
			for(measureName in measurementJSON){
				if(measurementJSON[measureName].type && measurementJSON[measureName].type == 'flat'){
					fields += ((fields != '')?",":"")+measureName;
					if(measurementJSON[measureName].value.value){
						values += ((values != '')?",":"")+ measurementJSON[measureName].value.value;	
					}
					if(unit == '' && measurementJSON[measureName].value.unit){
						unit = measurementJSON[measureName].value.unit;
					}					
				}  
			}
		}
		ret['fields'] = fields;
		ret['values'] = values;
		ret['unit'] = unit;
		return ret;
	}
	
	function getMeasurementJson(fieldsStr,valuesStr,unitStr){
		var fields = fieldsStr.split(","),
			values = valuesStr.split(","),
			unit   = unitStr,
			num	   = 0;//number of fields, values
		num = fields.length;
		//each of these is a comma separated string: need to combine them to prepare a Json string
		var jsonStr = '';
		for(var i=0; i<num; i++){
			if(fields[i]){
				var singleMeasurement = '';
				var val = values[i].trim();
				var field = fields[i].trim();
				if(val.length > 0){
					if(jsonStr != ''){
						jsonStr = jsonStr+","; 
					}
					singleMeasurement = "\""+field+"\":{\"type\":\"flat\",\"value\":{\"value\":"+val+",\"unit\":\""+unit+"\"}}";
					jsonStr += singleMeasurement;
				}
			}
		}
		if(jsonStr.length > 0){
			return "{"+jsonStr+"}";
		}else {
			return "";
		}
	}
	
	
	function addMeasurement(){
		
		//First validate if there is search criteria entered.
		var params = getSearchPanelParams();
		if(!validateSearchForm(params)){
			Ext.MessageBox.alert("Cannot Add now!!","You need to 1st enter a valid search criteria,and search, before you can add");
			return;
		}
		
		var measurementFields, unit;
		var firstRecord = measurementStore.getAt(0);
		if(firstRecord == null){
			//means I need to get the record params from measurement selection panel
			var mparams = getMeasureSelectionPanelParams();
			if(validateAddMeasurementForm(mparams)){
				measurementFields = mparams["measurements"];
				unit = mparams["unit"];
			}
			else {
				//no records are available to copy from, neither is measurement criteria entered
				Ext.MessageBox.alert("Cannot Add now!!","You need to select Measurement(s) and Unit before you add a measurement");
				selectMeasurementsPanel.expand();				
				return;
			}
		}
		else {
			//record(s) are available, so copy fields and units into this new record
			measurementFields = firstRecord.get("fields");
			unit = firstRecord.get("unit");
		}
	  	var newSizeMeasurement = new SizeMeasurement({
	  		measurementid: null,
	  		brandid: params['brand'],
	  		articletypeid: params['articleType'],
	  		agegroup: params['ageGroup'],
	  		styleid: params['styleId'],
	  		sizeid: '',
	  		size: '',
	  		fields: measurementFields,
	  		unit: unit,
	  		values: ''
	  	});
	    editor.stopEditing();
	    var store = editor.grid.store;
	    store.insert(0, newSizeMeasurement);
	    sizeMeasurementsGrid.getView().refresh();
	    sizeMeasurementsGrid.getSelectionModel().selectRow(0);
	    editor.startEditing(0);
	}
	
	function deleteMeasurement(store,record){
		Ext.Msg.confirm('Confirm Deletion', 'Are you sure you want to delete? It cannot be undone', function(button) {
		    if (button === 'yes') {
			    Ext.Ajax.request({
			    	url : '/admin/class/web/controllers/SizeMeasurementController.php',
			    	params : {
		    			'xaction': "delete",
		    			'measurementid': record.get('measurementid')
			    	},
			    	success : function(response){
			    			response = $.parseJSON(response.responseText);
							if(response.status == "failed"){
								Ext.Msg.show({
									title: response.title,
									msg: response.message
									
								}); 
							} 
							else {
								Ext.MessageBox.alert("Deleted","Scale Deleted Successfully");
			        	    }
							store.reload();
			        }
			     });
		    }
		});
		
	}
	function fetchStyleDetails(styleId,vendorArticleNumber){
		Ext.Ajax.request({
	    	url : '/admin/class/web/controllers/SizeMappingController.php',
	    	params : {
    			'xaction': "fetchstyledetails",
    			'styleId': styleId,
    			'vendorArticleNumber': vendorArticleNumber
	    	},
	    	success : function(response){
	    			response = $.parseJSON(response.responseText);
					if(response.status == "failed"){
						Ext.Msg.show({
							title: response.title,
							msg: response.message
							
						}); 
					} 
					else if(response.results && response.count > 0){
						//select fields based on response
						var fields = response.results[0];
						
						//Clear all the selection and filter applid due to previous selection
						articleTypeSelector.getStore().clearFilter();
			    		brandSelector.getStore().clearFilter();
			    		ageGroupSelector.getStore().clearFilter();
			    		
			    		articleTypeSelector.setValue(fields.articletypeid);
						brandSelector.setValue(fields.brandid);
						ageGroupSelector.setValue(fields.agegroup);
						styleIdField.setRawValue(fields.styleid);
						vendorArticleNumberField.setRawValue(fields.vendorarticlenumber);
	        	    }
	        }
	     });
	}
	
	function getSelectedStyleIds(){
		var sm = matchingStylesGrid.getSelectionModel();
		var selections = sm.getSelections();
		if(selections.length > 0){
			var styleIdArr = []; 
			for(var i=0;i<selections.length;i++){
				styleIdArr.push(selections[i].data["styleid"]);
			}
			return styleIdArr.join(",");	
		}
		return false;
	}
	
	function reapplyUnification(){
		var styleIds = getSelectedStyleIds();
		if(styleIds == false){
			Ext.MessageBox.alert("Cannot Apply Unification Measurements","No Style Selected to Apply Unification Measurements. Please select one or more styles");
			return;
		}
		//Need to send a ajax request to clear and re-apply unification
		sendAjaxRequestForUnification("reapplymeasurementsforstyles",styleIds,"Re-Apply Unification Measurements");
	}
	
	function reapplySizeChartImages(){
		var styleIds = getSelectedStyleIds();
		if(styleIds == false){
			Ext.MessageBox.alert("Cannot Apply Size-Chart Images","No Style Selected to Apply Size Chart Images. Please select one or more styles");
			return;
		}
		//Need to send a ajax request to clear and re-apply images
		sendAjaxRequestForUnification("reapplyimageforstyles",styleIds,"Re-Apply Size-Chart Images");
	}
	function clearStyleCache(){
		var styleIds = getSelectedStyleIds();
		if(styleIds == false){
			Ext.MessageBox.alert("Cannot Apply Size-Chart Images","No Style Selected to Apply Size Chart Images. Please select one or more styles");
			return;
		}
		//Need to send a ajax request to clear and re-apply images
		sendAjaxRequestForUnification("clearcacheforstyles",styleIds,"Clearing of Cache for Images");
	}
	function clearUnification(){
		var styleIds = getSelectedStyleIds();
		if(styleIds == false){
			Ext.MessageBox.alert("Cannot Clear Unification Measurements","No Style Selected to Clear Unification Measurements. Please select one or more styles");
			return;
		}
		//Need to send a ajax request to clear unification
		sendAjaxRequestForUnification("clearmeasurementsforstyles",styleIds,"Clear Unification Measurements");
	}
	
	function sendAjaxRequestForUnification(xaction,styleIds,task){
		loadingMask.show();
		Ext.Ajax.request({
	    	url : '/admin/class/web/controllers/SizeUnificationController.php',
	    	timeout: 3600000,//1 hour
	    	params : {
    			'xaction': xaction,
    			'styleIds': styleIds
	    	},
	    	success : function(response){
	    			if(response.status == "failed"){
						Ext.Msg.show({
							title: "Failed",
							msg: task+" to selected styles Failed.."
							
						}); 
					} 
					else {
						var resp,respHtml;
						try{
							resp = $.parseJSON(response.responseText);
							respHtml = resp.Message.replace(/\n/g,"<br/>");
						}
						catch(error){
							respHtml = response.responseText;
						}
						Ext.Msg.show({title:"Successful!",msg:respHtml});
	        	    }
	        }
	     });
	}
	function validateAddMeasurementForm(params){
		if(!params){
			var params = getMeasureSelectionPanelParams(); 
		}
		if(params["measurements"]=='' || params["unit"]==''){
			return false;
		}
		return true;
		
	}
	
	function validateSearchForm(params,showError){
		if(typeof params=='undefined' || !params){
			var params = getSearchPanelParams();
		}
		if(typeof showError == 'undefined'){
			showError = true;
		}
		if (!(params["articleType"] && params["ageGroup"] && params["brand"]) && (!params["styleId"]) && (!params["vendorArticleNumber"])) {
			if(showError){
				Ext.MessageBox.alert('Error', 'Please select (article-type,brand and age-group) or (Enter a style-id) or (Enter a vendor-article-number) first');	
			}
			return false;
		}
		return true;
	}
	
	function showHideMeasurements(){
        //
        //this variable is fetched from sizechart-position.js from skin2, loaded in size_measurement_unif.tpl
        if(typeof(sizePositionObj)!='undefined'){
                var form = searchPanel.getForm();
                if (form.isValid()) {
                        var articleType = form.findField("articleType").getRawValue().toLowerCase();
                        var ageGroup = form.findField("ageGroup").getRawValue().toLowerCase();
                        var measureJsonPos = sizePositionObj[ageGroup][articleType];
                        var applicableMeasurements = [];
                        for(var measureName in measureJsonPos){
                                if(measureJsonPos.hasOwnProperty(measureName)){
                                        applicableMeasurements.push(measureName);                                               
                                }
                        }
                        if(applicableMeasurements.length < 1){
                                alert("Warning!! Measurements you define here will not get rendered on PDP Size Chart");
                                alert("There are no measurements defined in sizechart-position.js for ArticleType:"+articleType+" and AgeGroup:"+ageGroup+". Please contact Engineering to get your measurements added in JS. The measurements which you define below will get saved in backend,but will not get rendered in PDP SizeChart");
                                /*Ext.Msg.show({
                                        title:"Warning!! Measurements you define here will not get rendered on PDP Size Chart",
                                        msg:"There are no measurements defined in sizechart-position.js for ArticleType:"+articleType+" and AgeGroup:"+ageGroup+".<br> Please contact Engineering to get your measurements added in JS. The measurements which you define below will get saved in backend,but will not get rendered in PDP SizeChart"
                                        //buttons: Ext.Msg.OK,
                                });*/
                                measurementCheckboxGroup.items.each(function(cb){
                                        cb.setVisible(true);            
                                });
                        }
                        else {
                                measurementCheckboxGroup.items.each(function(cb){
                                        if(applicableMeasurements.indexOf(cb.boxLabel.toLowerCase())==-1){
                                                cb.setVisible(false);
                                        }
                                        else {
                                                cb.setVisible(true);            
                                        } 
                                });                                     
                        }
                        measurementCheckboxGroup.panel.doLayout();
                }
        }
	}


	function reloadGrid() {
		var params = getSearchPanelParams();
		if (validateSearchForm(params)) {
			loadingMask.show();
			
			//show hide measurement checkboxes
			showHideMeasurements();
			var form = searchPanel.getForm();
			
			if(params["styleId"]|| params["vendorArticleNumber"]){
				params['searchBy']='style';
			}
			else if(params["articleType"] && params["ageGroup"] && params["brand"]){
				params['searchBy']='combination';
			}
    		measurementStore.baseParams = params;
    		measurementStore.removeAll();
    		measurementStore.load();
    		for(var key in params){
    			if(params.hasOwnProperty(key)){
    				sizeStore.baseParams[key] = params[key];
    				matchingStylesStore.baseParams[key] = params[key];
    			}
    		}
    		sizeStore.baseParams['xaction'] = 'fetchunifiedsizes';
    		matchingStylesStore.removeAll();
    		matchingStylesStore.load();
    		sizeStore.removeAll();
    		sizeStore.load();
		}
	}
	
	function getSearchPanelParams() {
		var form = searchPanel.getForm();
		if (form.isValid()) {
			var params = {};
			params["articleType"] = form.findField("articleType").getValue();
			params["ageGroup"] = form.findField("ageGroup").getValue();
			params["brand"] = form.findField("brand").getValue();
			params["styleId"] = form.findField("styleId").getValue();
			params["vendorArticleNumber"] = form.findField("vendorArticleNumber").getValue();
			return params;
		}
		return false;
	}
	
	function getMeasureSelectionPanelParams() {
		var form = selectMeasurementsPanel.getForm();
		if (form.isValid()) {
			var params = {};
			params["measurements"]="";
			params["unit"] = unitSelectionComboField.getValue();
			var selectedCBs = measurementCheckboxGroup.getValue();
			for(var i=0;i<selectedCBs.length;i++){
				if(params["measurements"]!=""){
					params["measurements"] += ",";	
				}
				params["measurements"] += selectedCBs[i].boxLabel; 
			}
			return params;
		}
		return false;
	}
});