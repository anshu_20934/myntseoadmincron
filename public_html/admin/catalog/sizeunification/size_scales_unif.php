<?php
require_once "../../auth.php";
require_once "$xcart_dir/include/security.php";
include_once "$xcart_dir/include/func/funcStyleProperties.php";
$uniqueId = uniqid();
$smarty->assign('progresskey', $uniqueId);

$articleType    = getCatalogCategory("ArticleType",false);
$smarty->assign("articleType",    $articleType);

$smarty->assign("selectedArticleTypeId",RequestVars::getVar("articleType",null));

$smarty->assign("main","size_scales_unif");
func_display("admin/home.tpl",$smarty);
?>