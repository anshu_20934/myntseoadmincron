/**
 * this function copies from the style id of same article type using ajax
 */
function copyStyleAjax(originalStyleId, copy4mStyleId, copyBulkOrContent)
{
	if(copyBulkOrContent == 'BULK')
	{
		copyBulkInfo(copy4mStyleId);
		return;
	}
	if(copyBulkOrContent == 'CONTENT')
	{
		copyContentInfo(originalStyleId, copy4mStyleId);
		return;
	}
}

/**
 * this fucntion copies the bulk info from the given style
 * @param int styleId from which data will be copied
 */
function copyBulkInfo(styleId)
{
    var params = {};
    params['action'] = 'copyStyle';
    params['styleId'] = styleId;
    params['articleTypeId'] = Ext.getCmp('mainEditGrid').getSelectionModel().getSelected().get('article_type_id');
    var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Copying..."});
    Ext.getCmp('copyStyleWindowId').destroy();
    myMask.show();
    Ext.Ajax.request({
        url : 'styleBulkUpdateAjax.php',
        params : params,
        success : function(response, action) {
            var jsonObj = Ext.util.JSON.decode(response.responseText);
            myMask.hide();
            if(!jsonObj.success)
            {
                Ext.MessageBox.alert("Copy Style", 'Please select a Style of same article type');
                return;
            }
            else
            {
                var selRows = Ext.getCmp('mainEditGrid').getSelectionModel().getSelections();
                var noRows = selRows.length;
                if(noRows != 1)
                {
                    Ext.MessageBox.alert("Please select a style id to update");
                    return;
                }
                
                var styleJson = jsonObj.style[0];
                for (var key in styleJson)
                {
                    if (styleJson.hasOwnProperty(key))
                    {
                        if((key !='style_id') && (key != 'style_status') && (key != 'vendor_article_no'))
                        {
                            selRows[0].set(key, styleJson[key]);
                        }
                    }
                }
                selRows[0].set('content_copy_style', styleJson['style_id']);
                myMask.hide();
                Ext.MessageBox.alert('Data Copied from other Style', 'Style successfully copied from choosen other style.');
                return;
            }    
        }
    });
}

/**
 * function to display the content html editor
 * for the styleBulkUpdate.php page
 */
function displayContentEditor(originalStyleId, styleAttribute, styleContent)
{
    var htmlCmp = Ext.getCmp('mainwin');
    if(htmlCmp)
    {
        htmlCmp.destroy();
    }
    var widthPanel = 1100;
    var heightPanel = 600;
    var changeStatus = false;
    var htmlEditorFormPanel = getHtmlEditorFormPanel(originalStyleId, styleContent, widthPanel, heightPanel, changeStatus);
    
    var win = new Ext.Window({
        title: 'Style Content Window',
        id: 'mainwin',
        closable: true,
        width: widthPanel,
        height: heightPanel,
        layout: 'border',
        items: [htmlEditorFormPanel]
    });
    win.show(this);
}

Ext.onReady(function() {
	Ext.Ajax.timeout = 120000;
	
	var noGridColumns = styleColKeys.length;
	var noGridRows = styleIdList.length;
	var styleRecordFormat = [];
    for(var i = 0; i < noGridColumns; i++)
    {
    	styleRecordFormat.push(styleColKeys[i]);
    }
	
    //Style data record
	var Style = Ext.data.Record.create(styleRecordFormat);
    
/**
 * main grid style data store
 */
    var store = new Ext.data.Store({
    	data: styleAll,
        reader: new Ext.data.JsonReader({
        	idProperty:'id'
          }, Style),
        autoLoad: true
    });
    
/**
 * renderer for various columns
 */
    //status renderer
    var styleStatusRender = function(value, p, record){
    	var styleStatusStore = getJsonStore(styleStatusData);
    	var rec = styleStatusStore.getById(value);
    	return rec ? rec.get('name') : value;
    }
    
    //article type renderer
    var articleTypeRenderer = function(value, p, record){
    	var articleTypeStore = getJsonStore(articleTypeData);
    	var rec = articleTypeStore.getById(value);
        return rec ? rec.get('name') : value;
    }
    
    //product type renderer
    var productTypeRenderer = function(value, p, record){
    	if(value == 0)
        {
            return 'Not Set';
        }
        var productTypeStore = getJsonStore(productTypeData);
        var rec = productTypeStore.getById(value);
        return rec ? rec.get('name') : value;
    }
    
    //product display name renderer
    //<Brand> <Gender> <Colour> <Vendor Article Name> <ArticleType>
    var productDisplayNameRenderer = function(value, p, record){
    	if(value == '')
    	{
    		var article_type = articleTypeRenderer(record.data['article_type_id'], null, null);
    		value = record.data['brand']+' '+record.data['gender']+' '+record.data['base_colour']+' '+record.data['vendor_article_name']+' '+article_type;
    		record.data['product_display_name'] = value;
    	}
        return value;
    }
    
    //specific attribute renderer
    var speAttrRender = function(value, metaData, record, rowIndex, colIndex, store){
    	metaData.attr = 'style="background-color:#94FF94;"';
    	var noSpeAttr = specificAttributeData.length;
    	if(value == 0)
    	{
    		return 'Not Set';
    	}
    	var speAttrRec = value;
        for(var i = 0; i < noSpeAttr; i++)
        {
            var speAttrDataStore = getJsonStore(specificAttributeData[i].value);
            var rec = speAttrDataStore.getById(value);
            speAttrRec = rec ? rec.get('name') : speAttrRec;
        }
        return speAttrRec;
    }
    
    
/**
 * editors for various columns
 */
    var textFieldEdit = new Ext.form.TextField();
    var statusEdit = getComboBox(styleStatusData, 'name', 'id');    
    var brandEdit = getComboBox(brandData, 'name', 'name');
    var classificationBrandDataEdit = getComboBox(classificationBrandData, 'name', 'name');
    var articleTypeEdit = getComboBox(articleTypeData, 'name', 'id');
    var productTypeEdit = getComboBox(productTypeData, 'name', 'id');
    var ageGroupEdit = getComboBox(ageGroupData, 'name', 'name');
    var genderEdit = getComboBox(genderData, 'name', 'name');
    var fashionTypeEdit = getComboBox(fashionTypeData, 'name', 'name');
    var colourEdit = getComboBox(colourData, 'name', 'name');
    var seasonEdit = getComboBox(seasonData, 'name', 'name');
    var yearEdit = getComboBox(yearData, 'name', 'name');
    var usageEdit = getComboBox(usageData, 'name', 'name');
    
/**
 * make the column model for the editor grid
 */
    var colModel = new Ext.grid.ColumnModel({
           defaultSortable: false,
           columns: getGridColumns()
    });
    
/**
 * main grid for editing styles
 */
    var sm = new Ext.grid.CheckboxSelectionModel();
    var grid = new Ext.grid.EditorGridPanel({
    	id : 'mainEditGrid',
        renderTo: 'contentPanel',
        frame: true,
        title: 'Bulk Style Updates',
        enableColumnMove : false,
        autoHeight : true,
        store: store,
        clickstoEdit: 1,
        colModel: colModel,
        sm : sm,
        listeners: {
        	afteredit: function(e){
        		if(e.record.json[e.field] == e.value)
        		{
    				e.record.set(e.field, e.record.json[e.field]);
        			return;
        		}
        		if(e.field == 'style_status')
        		{
        			validateNewStatus(e);
        			return;
        		}
        		if(e.field == 'article_type_id')
        		{
        			validateArticleChange(e);
        			return;
        		}
        	}
        },
        tbar: [
        	{
        	   text: 'Save selected rows changes',
                icon: 'images/table_add.png',
                cls: 'x-btn-text-icon',
                handler: updateStyles
            },
            {
                text: 'Copy from a Style',
                icon: 'images/table_add.png',
                cls: 'x-btn-text-icon',
                handler: function(){
                	var selRows = grid.getSelectionModel().getSelections();
                    var noRows = selRows.length;
                    if(noRows != 1)
                    {
                        Ext.MessageBox.alert("Select a Style", "Please select a style id to which you want to copy");
                        return;
                    }
                    var originalStyleId = selRows[0].get('style_id');
                    selectORseachStyle(originalStyleId, 'BULK');
                }
            },
            {
                text: 'Update Content of Selected Style',
                icon: 'images/table_add.png',
                cls: 'x-btn-text-icon',
                handler: updateSelectedStyleContent
            },
            {
                text: 'Show Style Summary',
                icon: 'images/table_add.png',
                cls: 'x-btn-text-icon',
                handler: showStyleSummary
            },
            {
                text: 'Update All Content',
                icon: 'images/table_add.png',
                cls: 'x-btn-text-icon',
                handler: function(){
                    var r = confirm("Are you sure, you want to navigate to other page? Any unsaved changes will be lost.");
                    if(r == true)
                    {
                        var url = httpLocation+'/admin/catalog/styleContentUpdate.php?style_ids='+style_ids;
                        window.open(url, '_self', false);
                    }
                }
            }
        ]
    });
    
/**
 * given json data, display and value field key get a combo box
 */
    function getComboBox(jsonData, displayField, valueField)
    {
    	var store = getJsonStore(jsonData);
    	var genre_edit = new Ext.form.ComboBox({
              typeAhead: true,
              triggerAction: 'all',
              mode: 'local',
              store: store,
              displayField: displayField,
              valueField: valueField,
              allowBlank: false
        });
        return genre_edit;
    }
    
/**
  * get a jsonstore where data has id and name as keys
  */
    function getJsonStore(jsonData)
    {
        var jsonStore = new Ext.data.JsonStore({
        	   fields : ['id', 'name'],
               data : jsonData
        });
        return jsonStore;
    }
    
/**
 * this function attachs the specific attributes to the end of 
 * standard style properties and gives the columns for grid
 */
    function getGridColumns()
    {
    	var columns = [
    	        new Ext.grid.RowNumberer(),
    	        new Ext.grid.CheckboxSelectionModel(),
    		    {header: "Style Id", dataIndex: 'style_id', tdCls: 'red'},
                {header: "Status", width: 150, dataIndex: 'style_status', editor: statusEdit, renderer:styleStatusRender},
                {header: "Vendor Article No.", width: 200, dataIndex: 'vendor_article_no', editor: textFieldEdit},
                {header: "Vendor Article Name", width: 275, dataIndex: 'vendor_article_name', editor: textFieldEdit},
                {header: "Brand", dataIndex: 'brand', editor:brandEdit},
                {header: "Classification Brand", dataIndex: 'classification_brand', editor:classificationBrandDataEdit},
                {header: "Article Type", dataIndex: 'article_type_id', renderer: articleTypeRenderer, editor:articleTypeEdit},
                {header: "Product Type", dataIndex: 'product_type', renderer: productTypeRenderer, editor:productTypeEdit},
                {header: "Age Group", dataIndex: 'age_group', editor: ageGroupEdit},
                {header: "Gender", dataIndex: 'gender', editor: genderEdit},
                {header: "Base Colour", dataIndex: 'base_colour', editor: colourEdit},
                {header: "Colour 1", dataIndex: 'colour1', editor: colourEdit},
                {header: "Colour 2", dataIndex: 'colour2', editor: colourEdit},
                {header: "Fashion Type", dataIndex: 'fashion_type', editor: fashionTypeEdit},
                {header: "Season", dataIndex: 'season', editor: seasonEdit},
                {header: "Year", dataIndex: 'year', editor: yearEdit},
                {header: "Usage", dataIndex: 'usage', editor: usageEdit}
            ];
        if(specificAttributeExist)
        {
        	var noSpeAttr = specificAttributeData.length;
            for(var i = 0; i < noSpeAttr; i++)
            {
            	var specificAttributeEditor;
            	for(var j = 0; j < noSpeAttr; j++)
            	{
            		if(specificAttributeData[j].name == specificAttributeName[i])
            		{
            			specificAttributeEditor = getComboBox(specificAttributeData[j].value, 'name', 'id');
            		}
            	}
            	columns.push({id: specificAttributeName[i], header: specificAttributeName[i], dataIndex: specificAttributeName[i], editor:specificAttributeEditor, renderer: speAttrRender});
            }
        }
        
        columns.push({header: "Price", dataIndex: 'price', xtype:'numbercolumn', editor: textFieldEdit, format: 'Rs 0,0.00'});
        columns.push({header: "Weight", dataIndex: 'weight', xtype:'numbercolumn', editor: textFieldEdit, format: '0,0 gms'});
        columns.push({header: "Product Display Name", width: 350, dataIndex: 'product_display_name', editor: textFieldEdit, renderer: productDisplayNameRenderer});
        columns.push({header: "Product Tags", width: 200, dataIndex: 'tags', editor: textFieldEdit});
        columns.push({header: "Comments", width: 200, dataIndex: 'comments', editor: textFieldEdit});
        
        return columns;
    }
    
/**
 * function to save the changes of selected styles
 */
    function updateStyles()
    {
        var selRows = grid.getSelectionModel().getSelections();
        var noRows = selRows.length;
        if(noRows == 0)
        {
            Ext.MessageBox.alert("Update Style", "Please select style ids to update");
            return;
        }
        var params = {};
        params['action'] = 'saveStyle';
        for(var i = 0; i < noRows; i++)
        {
        	params[i+1] = Ext.util.JSON.encode(selRows[i].data);                       
        }
        var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Updating..."});
        myMask.show();
        Ext.Ajax.request({
            url : 'styleBulkUpdateAjax.php',
            params : params,
            success : function(response, action) {
                var jsonObj = Ext.util.JSON.decode(response.responseText);
                var styleSaved = '';
                var styleCommit = [];
                var styleNotSaved = '';
                for (var key in jsonObj)
                {
                    if (jsonObj.hasOwnProperty(key))
                    {
                    	if(jsonObj[key]['valid'])
                    	{
                    		styleSaved += key +' ';
                    		styleCommit.push(key);
                    	}
                    	else
                    	{
                    		styleNotSaved += key + ':' + jsonObj[key]['error'] + "<br/>";
                    	}
                    }
                }
                
                var html = '';
                if(styleSaved != '')
                {
                	html += "Styles saved:"+styleSaved+" <br/> ";
                	commitRecordStore(styleCommit);
                }
                if(styleNotSaved != '')
                {
                	html += "Styles not saved: <br/>" +styleNotSaved;
                }
                myMask.hide();
                Ext.Msg.show({
                    title: "Update Message",
                    msg:   html,
                    icon:  Ext.MessageBox.INFO
                });
            }
        });
    }
    
/**
 * after succesful saving of record commit the changes in store
 */
    function commitRecordStore(styleCommit)
    {
    	var selRows = grid.getSelectionModel().getSelections();
        var noRows = selRows.length;
        for(var i = 0; i < noRows; i++)
        {
        	var style_id_commit = selRows[i].get('style_id');
            if($.inArray(style_id_commit, styleCommit) != -1)
            {
            	selRows[i].set('content_copy_style', style_id_commit);
                selRows[i].commit();
            }
        }
    }
    
/**
 * checks the changed status and suggest if it is wrong
 */
    function validateNewStatus(e)
    {
    	var oldStatus = e.record.json.style_status;
    	var selStatus = e.value;//selected status
    	var toStatusCode = possibleStatusTrans[oldStatus].to_code;
    	if($.inArray(selStatus, toStatusCode) == -1)
    	{
    		Ext.MessageBox.alert('Valid Status', 'Allowed status from current status are ' + possibleStatusTrans[oldStatus].to_name);
    		e.record.set(e.field, e.record.json[e.field]);
    		return;
    	}
    }
    
/**
 * checks if possible to change article type or not
 * if not alert the user
 * else change the article type and reload the page
 */
    function validateArticleChange(e)
    {
    	var curStyleStatus = e.record.json.style_status;
    	if(curStyleStatus != 'D')
    	{
    		Ext.MessageBox.alert('Want to change Article Type?', 'You can change article type of a style from draft status only.');
    		e.record.set(e.field, e.record.json[e.field]);
            return;
    	}
    	else
    	{
    		var r = confirm("Are you sure? You want to change article, article related data will be lost.");
            if(r == true)
            {
            	changeStyleArticleType(e.record.json.style_id, e.value);
            }
            else
            {
            	Ext.MessageBox.alert('Do no change Article Type', 'You took a wise decision. Article Type not changed.');
            	e.record.set(e.field, e.record.json[e.field]);
            	return;
            }
    	}
    }
    
/**
 * function which changes the article type of a style and reloads the page if successfull
 */
    function changeStyleArticleType(style_id, article_type_id)
    {
    	var params = {};
        params['action'] = 'changeArticleType';
        params['styleId'] = style_id;
        params['articleTypeId'] = article_type_id;
        var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Changing article type..."});
        myMask.show();
        Ext.Ajax.request({
            url : 'styleBulkUpdateAjax.php',
            params : params,
            success : function(response, action) {
                var jsonObj = Ext.util.JSON.decode(response.responseText);
                myMask.hide();
                if(!jsonObj.success)
                {
                    Ext.MessageBox.alert('Some error!');
                    return;
                }
                else
                {
                	alert('Article Type successfully changed. Page will be reloaded to reflect the changes.');
                	var aid = article_type_id;
                    var url = httpLocation+'/admin/catalog/styleBulkUpdate.php?aid='+aid+'&style_ids='+style_ids;
                    window.open(url, '_self', false);
                }
            }
        });
    }
    
/**
 * function to display and save content info.
 */
    function updateSelectedStyleContent()
    {
    	var selRows = grid.getSelectionModel().getSelections();
        var noRows = selRows.length;
        if(noRows != 1)
        {
        	Ext.MessageBox.alert('Want to load style content?', "Please select only one style id for content update.");
            return;
        }
        var load2StyleId = selRows[0].get('style_id');
        var load4mStyleId = selRows[0].get('content_copy_style');
        loadStyleContent(load2StyleId, load4mStyleId);
    }
    
/**
 * function to render specific attributes for style summary
 */
    //specific attribute renderer
    function summarySpeAttrRender(value){
        var noSpeAttr = specificAttributeData.length;
        if(value == 0)
        {
            return 'Not Set';
        }
        var speAttrRec = value;
        for(var i = 0; i < noSpeAttr; i++)
        {
            var speAttrDataStore = getJsonStore(specificAttributeData[i].value);
            var rec = speAttrDataStore.getById(value);
            speAttrRec = rec ? rec.get('name') : speAttrRec;
        }
        return speAttrRec;
    }
/**
 * function to show changes summary
 */
    function showStyleSummary()
    {
    	var selRows = grid.getSelectionModel().getSelections();
        var noRows = selRows.length;
        if(noRows != 1)
        {
            Ext.MessageBox.alert('Show Style Summary!', "Please select only one style id for summary display.");
            return;
        }
        var summaryStore = [];
        var name = '';
        var value = '';
        for(var i = 0; i < noGridColumns; i++)
        {
        	name = (typeof stylePropertyName[styleColKeys[i]] === "undefined")?styleColKeys[i]:stylePropertyName[styleColKeys[i]];
        	value = selRows[0].get(styleColKeys[i]);
        	if(styleColKeys[i] == 'content_copy_style')
        	{
        		continue;
        	}
        	if(styleColKeys[i] == 'style_status')
        	{
        		value = styleStatusRender(value, null, null);
        	}
        	if(styleColKeys[i] == 'article_type_id')
            {
            	value = articleTypeRenderer(value, null, null);
            }
            if(styleColKeys[i] == 'product_type')
            {
                value = productTypeRenderer(value, null, null);
            }
            if(specificAttributeExist)
            {
            	if($.inArray(styleColKeys[i], specificAttributeName) != -1)
            	{
            		value = summarySpeAttrRender(value, null, null);
            	}
            }
        	
            summaryStore.push({'name': name, 'value': value});
        }
        //attribute panel
        var styleAttributeStore = new Ext.data.JsonStore({
               fields : ['name', 'value'],
               data : summaryStore
        });
        
        var nav = new Ext.grid.GridPanel({
            region: 'center',
            width: 350,
            margins:'3 0 3 3',
            cmargins:'3 3 3 3',
            store: styleAttributeStore,
            columns: [{
                header   : 'Name', 
                width    : 150, 
                sortable : false,
                dataIndex: 'name'
            },
            {
                header   : 'Value', 
                width    : 200, 
                sortable : false, 
                dataIndex: 'value'
            }]
        });
        
        var win = new Ext.Window({
            title: 'Style Summary Window',
            id: 'mainwin2',
            closable: true,
            width: 400,
            height: 500,
            layout: 'border',
            items: [nav]
            
        });
        win.show(this);
    }
});