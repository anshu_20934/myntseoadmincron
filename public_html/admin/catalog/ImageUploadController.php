<?php

require "../auth.php";


//include_once "$xcart_dir/admin/class/imageUpload/action/ImageUploadAction.php";

use imageUpload\action\ImageUploadAction;

//if(!isset($_FILES) && isset($HTTP_POST_FILES))
//{
//	$_FILES = $HTTP_POST_FILES;
//}

class ImageUploadController{
	public function run(){
		$xaction = $_REQUEST['mode'];
		$request = array();
		switch ($xaction) {
			case 'uploadSizeChart' :
				$imageUploadAction = new ImageUploadAction();
				$result = $imageUploadAction->uploadSizeChartImage();
				break;
			default:
				break;
		}
		return $result;

	}
}

$controller = new ImageUploadController();
$result = $controller->run();

$smarty->assign("errorMessage",$result['errorMessage']);
$smarty->assign("message",$result['message']);

$smarty->assign("main","imageUploader");
func_display("admin/home.tpl",$smarty);
?>