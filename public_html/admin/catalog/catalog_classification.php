<?php
/**
 * Created by Nishith.
 * Date: 30 Mar, 2011
 * Time: 12:45:05 PM
 */
require_once( "../auth.php");

@include ($xcart_dir."/include/func/func.filter_order.php");

//require_once($xcart_dir."/include/security.php");
define('FORM_ACTION','admin/catalog/catalog_classification.php');
$location[] = array("Catalog Classification", "");
$groupToModify = '';

if($REQUEST_METHOD == "POST"){

          $isError = false;
          
          switch($_POST['mode']){
          ############## Condition to add groups ############
          	case  "addgroup" :

                    if(empty($_POST['typename'])){
                        $isError = true;
                        $top_message["content"] = "Error! Please input valid data.";
                    }
                    
                    if($_POST['addtype'] == 'article')
                    {
                        $res_a=func_query("select * from mk_catalogue_classification where typename='".$_POST['typename']."' and parent1 !=-1 and parent2 != -1");

                        if(!empty($res_a))
                        {
                            $top_message["content"] = "Article type '".$_POST['typename']."' already exists.";
                            $isError = true;
                        }
                        else if(!empty($_POST['typecode'])){
                            $res_t=func_query("select * from mk_catalogue_classification where typecode='".$_POST['typecode']."' and parent1 !=-1 and parent2 != -1");
                            if(!empty($res_t))
                            {
                                $top_message["content"] = "Type Code '".$_POST['typecode']."' already exists.";
                                $isError = true;
                            }
                        }
                    }
                    else if($_POST['addtype'] == 'sub')
                    {
                        $res_a=func_query("select * from mk_catalogue_classification where typename='".$_POST['typename']."' and parent1 !=-1 and parent2 = -1");
                        if(!empty($res_a))
                        {
                            $top_message["content"] = "Sub Category '".$_POST['typename']."' already exists.";
                            $isError = true;
                        }
                    }
                    else if($_POST['addtype'] == 'master')
                    {
                        $res_a=func_query("select * from mk_catalogue_classification where typename='".$_POST['typename']."' and parent1 =-1 and parent2 = -1");
                        if(!empty($res_a))
                        {
                            $top_message["content"] = "Master Category '".$_POST['typename']."' already exists.";
                            $isError = true;
                        }
                    }
                    if(!$isError)
                    {
                        $query_data = array(
                            "typename"  =>(!empty($_POST["typename"]) ? trim($_POST["typename"]) : ''),
                            "typecode"  =>(!empty($_POST["typecode"]) ? trim($_POST["typecode"]) : ''),
                            "parent1"  =>(!empty($_POST["parent1"]) ? $_POST["parent1"] : -1),
                            "parent2"  =>(!empty($_POST["parent2"]) ? $_POST["parent2"] : -1),
                            "is_active" =>  (!empty($_POST["is_active"]) ? '1' : '0'),
                        	"enable_social_sharing" =>  (!empty($_POST["enable_social_sharing"]) ? '1' : '0'),
                        	"filter_order" => (!empty($_POST["filter_order"]) ? $_POST["filter_order"] : '0')
                            ) ;
						update_filter_order("mk_catalogue_classification",'',!empty($_POST["filter_order"]) ? $_POST["filter_order"] : '0');
                        func_array2insert ('mk_catalogue_classification', $query_data);
                        $top_message["content"] = "Added new Classification.";
                        if($_POST['addtype']=='article'){
                        	global $xcache;
                          	$xcache->remove('social_article_type_exclude_list');
                        }
                    }
                

            break;
		   ########### Condition to update ###############
          	case "update":
			   if (is_array($posted_data))
               {        
               		usort($posted_data,'compare_filter_order');
				    foreach ($posted_data as $id=>$v)
                    {
                        if (empty($v["to_delete"]))
                          continue;
                        $parent1=(!empty($v["parent1"]) ? $v["parent1"] : -1);
                        $parent2=(!empty($v["parent2"]) ? $v["parent2"] : -1);
                        $res=func_query("select * from mk_catalogue_classification where typename='".trim($v["typename"])."' and parent1=$parent1 and parent2=$parent2 and id !='".$v['id']."'");

                        if(!empty($res))
                        {
                            $isError = true;
                        }
                        else{
                            $query_data = array(
                            "typename"  =>(!empty($v["typename"]) ? trim($v["typename"]) : ''),
                            "typecode"  =>(!empty($v["typecode"]) ? trim($v["typecode"]) : ''),
                            "parent1"  =>(!empty($v["parent1"]) ? $v["parent1"] : -1),
                            "parent2"  =>(!empty($v["parent2"]) ? $v["parent2"] : -1),
                            "is_active" =>  (!empty($v["is_active"]) ? '1' : '0'),
                            "enable_social_sharing" =>  (!empty($v["enable_social_sharing"]) ? '1' : '0'),
                            "filter_order" => (!empty($v["filter_order"]) ? $v["filter_order"] : '0')
                            ) ;
                            update_filter_order("mk_catalogue_classification",$v['id'],$v["filter_order"]);
                            func_array2update("mk_catalogue_classification", $query_data, "id='".$v['id']."'");
                        	global $xcache;
                          	$xcache->remove('social_article_type_exclude_list');
                        
                        }

                    }
                        if($isError)
                          $top_message["content"] = "Error while updating ,please check the values again!";
                        else{
                          $top_message["content"] = "Classification Edited successfully";
                        }
                }
                       break;

          ########### Condition to delete the record ###############
          	case  "delete" :
			   if (is_array($posted_data)) {
				    foreach ($posted_data as $id=>$v) {
						 if (empty($v["to_delete"]))
							  continue;
						 update_filter_order("mk_catalogue_classification",$id,"");
						 db_query("delete from mk_catalogue_classification where id ='".$id."'") ;
					}

				  $top_message["content"] = "Classification deleted!!";
	           }
	           break;

          ########### Condition to delete the record ###############
          case "modify":
		  	    if (is_array($posted_data)) {
				    foreach ($posted_data as $groupid=>$v) {
						 if (empty($v["to_delete"]))
							  continue;
					     $groupToModify =  $groupid;

					}

				  ##$top_message["content"] = func_get_langvar_by_name("msg_adm_prdstyle_upd");
	              ##func_header_location("admin_product_styles.php");

	           }
	           break;
        }  ####### End of switch case

}
function compare_filter_order($a, $b)
  {
    return strnatcmp($a['filter_order'], $b['filter_order']);
  }
$sql ="SELECT * FROM  mk_catalogue_classification order by typename asc";
$a_types= func_query($sql);
$smarty->assign("articles",$a_types);

$sql ="SELECT * FROM  mk_catalogue_classification where parent1 !=-1 and parent2 != -1 order by filter_order";
$article_types= func_query($sql);

$smarty->assign("article_types",$article_types);
$sql ="SELECT * FROM  mk_catalogue_classification where parent1 !=-1 and parent2 =-1 order by typename asc";
$sub_categories= func_query($sql);
$smarty->assign("sub_categories",$sub_categories);
$sql ="SELECT * FROM  mk_catalogue_classification where parent1 =-1 and parent2 = -1 order by typename asc";
$master_categories= func_query($sql);
$smarty->assign("master_categories",$master_categories);

$smarty->assign("main","catalog_classification");
# Assign the current location line
$smarty->assign("location", $location);
$smarty->assign("form_action", FORM_ACTION);
# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);
$smarty->assign ("top_message", $top_message);
@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
