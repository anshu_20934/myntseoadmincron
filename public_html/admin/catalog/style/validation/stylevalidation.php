<?php

/**
 * this method validate contents of a style
 */
function validateStyleContent($style)
{
	$description = $style['description'];
	$styleNote = $style['style_note'];
	$materialsCareDesc = $style['materials_care_desc'];
	$sizeFitDesc = $style['size_fit_desc'];

	$descWithoutSpace = str_replace('&nbsp;', '', $description);
	$descWithoutSpace = trim(str_replace('<br>', '', $descWithoutSpace));

	$result['valid'] = false;
	if(empty($descWithoutSpace))
	{
		$result['error'] = 'Description is empty';
	}
	elseif (!isAscii($description)){
		$result['error'] = 'Description contains non-ASCII character';
	}
	elseif (!isAscii($styleNote)){
		$result['error'] = 'Style Note contains non-ASCII character';
	}
	elseif (!isAscii($materialsCareDesc)){
		$result['error'] = 'Materials Care Description contains non-ASCII character';
	}
	elseif (!isAscii($sizeFitDesc)){
		$result['error'] = 'Size Fit Description contains non-ASCII character';
	}
	else {
		$result['valid'] = true;
	}
	return $result;
}

/**
 * this method validates the style data and returns any error
 * @param array $style
 */
function validateStyle($style)
{
	$updateError = '';
	$styleId = $style['style_id'];
	$styleStatus = $style['style_status'];
	$vendorArticleNo = $style['vendor_article_no'];
	$vendorArticleName = $style['vendor_article_name'];
	$articleTypeId = $style['article_type_id'];
	$productType = $style['product_type'];
	$brand = $style['brand'];
	$ageGroup = $style['age_group'];
	$gender = $style['gender'];
	$baseColour = $style['base_colour'];
	$colour1 = $style['colour1'];
	$colour2 = $style['colour2'];
	$fashionType = $style['fashion_type'];
	$season = $style['season'];
	$year = $style['year'];
	$usage = $style['usage'];
	$price = $style['price'];
	$weight = $style['weight'];
	$productDisplayName = $style['product_display_name'];
	$tags = $style['tags'];
	$classificationBrand = $style['classification_brand'];

	//Product Display Name should not contain any Non-Ascii Character
	if(!isAscii($productDisplayName)){
		$updateError .= " Product Display Name contains one or more Non ASCII character(s);";
	}

	$validStatus = validateStatus($styleId, $styleStatus);
	if($styleStatus == 'D')
	{
		$status='Draft';
	}
	if(!$validStatus)
	{
		$updateError .= " Style status is wrong;";
	}

	//validate vendor article no
	if(empty($vendorArticleNo))
	{
		$updateError .= " Vendor Article No is blank;";
	}
	else
	{
		$check=func_query("select * from mk_style_properties where style_id!='".$styleId."' and article_number='".$vendorArticleNo."'", TRUE);
		if(!empty($check))
		{
			$updateError .= " Article Number already exists;";
		}
	}

	//validate vendor article name
	if(empty($vendorArticleName))
	{
		$updateError .= " Vendor Article Name is blank;";
	}

	//validate article type
	if(empty($articleTypeId))
	{
		$updateError .= " Article Type is blank;";
	}
	else
	{
		$articleRes = func_query("select * from mk_catalogue_classification where id = '$articleTypeId' and parent1 !='-1' and parent2 !='-1' and is_active=1", TRUE);
		if(empty($articleRes))
		{
			$updateError .= " Article Type is wrong;";
		}
	}

	//validate product type
	if(empty($productType))
	{
		$updateError .= " Product Type is blank;";
	}
	else
	{
		$validProductType = func_query("SELECT * FROM mk_product_type WHERE type = 'P' AND id = '$productType'", TRUE);
		if(empty($validProductType))
		{
			$updateError .= " Product Type is wrong;";
		}
	}

	//validate brand
	if(empty($brand))
	{
		$updateError .= " Brand is blank;";
	}
	else
	{
		$brandRes=func_query("select * from mk_attribute_type_values where attribute_type='Brand' and BINARY attribute_value='$brand' and is_active=1", TRUE);
		if(empty($brandRes))
		{
			$updateError .= " Brand is wrong;";
		}
	}

	//validate age group
	if(empty($ageGroup))
	{
		if($status != 'Draft')
			$updateError .= " Age Group is blank;";
	}
	else
	{
		$res=func_query("select * from mk_attribute_type_values where attribute_type_id='2' and BINARY attribute_value='$ageGroup' and is_active=1", TRUE);
		if(empty($res))
		{
			$updateError .= " Age Group is wrong;";
		}
	}

	//validate gender
	if(empty($gender))
	{
		$updateError .= " Gender is blank;";
	}
	else
	{
		$res=func_query("select * from mk_attribute_type_values where attribute_type_id='3' and BINARY attribute_value='$gender' and is_active=1", TRUE);
		if(empty($res))
		{
			$updateError .= " Gender is wrong;";
		}
	}

	//validate the base colour
	if(empty($baseColour))
	{
		if($status != 'Draft')
		{
			$updateError .= " Base Colour is blank;";
		}
	}
	else
	{
		$res=func_query("select * from mk_attribute_type_values where attribute_type_id='4' and BINARY attribute_value='$baseColour' and is_active=1", TRUE);
		if(empty($res))
		{
			$updateError .= " Base Colour is wrong;";
		}
	}

	//validate the colour 1
	if(empty($colour1))
	{
		if($status != 'Draft')
		{
			$updateError .= " Colour 1 is blank;";
		}
	}
	elseif(!empty($colour1))
	{
		$res=func_query("select * from mk_attribute_type_values where attribute_type_id='4' and BINARY attribute_value='$colour1' and is_active=1", TRUE);
		if(empty($res) && ($colour2 != 'NA'))
		{
			$updateError .= " Colour1 is wrong;";
		}
	}

	//validate the colour 2
	if(empty($colour2))
	{
		if($status != 'Draft')
		{
			$updateError .= " Colour 2 is blank;";
		}
	}
	elseif(!empty($colour2))
	{
		$res=func_query("select * from mk_attribute_type_values where attribute_type_id='4' and BINARY attribute_value='$colour2' and is_active=1", TRUE);
		if(empty($res) && ($colour2 != 'NA'))
		{
			$updateError .= " Color2 is wrong;";
		}
	}

	//validate fashion type
	if(empty($fashionType))
	{
		if($status != 'Draft')
		{
			$updateError .= " Fashion Type is blank;";
		}
	}
	else
	{
		$res=func_query("select * from mk_attribute_type_values where attribute_type_id='7' and BINARY attribute_value='$fashionType' and is_active=1", TRUE);
		if(empty($res))
		{
			$updateError .= " Fashion Type is wrong;";
		}
	}

	//validate season
	if(empty($season))
	{
		if($status != 'Draft')
		{
			$updateError .= " Seasion is blank;";
		}
	}
	else
	{
		$res=func_query("select * from mk_attribute_type_values where attribute_type_id='8' and BINARY attribute_value='$season' and is_active=1", TRUE);
		if(empty($res))
		{
			$updateError .= " Season is wrong;";
		}
	}

	//validate year
	if(empty($year))
	{
		if($status != 'Draft')
		{
			$updateError .= " Year is blank;";
		}
	}
	else
	{
		$res=func_query("select * from mk_attribute_type_values where attribute_type_id='9' and BINARY attribute_value='$year' and is_active=1", TRUE);
		if(empty($res))
		{
			$updateError .= " Year is wrong;";
		}
	}

	//validate usage
	if(empty($usage))
	{
		if($status != 'Draft')
		{
			$updateError .= " Usage is blank;";
		}
	}
	else
	{
		$res=func_query("select * from mk_attribute_type_values where attribute_type_id='10' and BINARY attribute_value='$usage' and is_active=1", TRUE);
		if(empty($res))
		{
			$updateError .= " Usage is wrong;";
		}
	}

	//validate price should be +ve numeric value
	if(empty($price))
	{
		if($status != 'Draft')
		{
			$updateError .= " Price is blank;";
		}
	}
	else
	{
		if (is_numeric($price))
		{
			$price = round($price, 2);
			if($status != 'Draft')
			{
				if($price <= 0)
				{
					$updateError .= " Price $price is not a +ve numeric value;";
				}
			}
			elseif($price < 0)
			{
				$updateError .= " Price $price is not a +ve numeric value;";
			}
		}
		else
		{
			$updateError .= " Price $price is not a numeric value;";
		}
	}

	//validate classification brand
	if(empty($classificationBrand))
	{
		if($status != 'Draft')
		{
			$updateError .= " Classification Brand is blank;";
		}
	}
	else
	{
		$classificationBrandResult = func_query("SELECT * FROM mk_filters WHERE filter_group_id='2' AND BINARY filter_name='$classificationBrand' AND is_active=1", TRUE);
		if(empty($classificationBrandResult))
		{
			$updateError .= " Classification Brand is wrong;";
		}
	}

	//validate product tags
	if(empty($tags))
	{
		if($status != 'Draft')
		{
			$updateError .= " Tags is blank;";
		}
	}

	//validate specific attributes each specific attribute is mandatory
	//non specific attribute keys
	$nonSpecificAttributesKey = array('style_id', 'style_status', 'article_type_id', 'product_type', 'vendor_article_no', 'tags', 'product_display_name', 'vendor_article_name',
			'brand', 'age_group', 'gender', 'base_colour', 'colour1', 'colour2', 'fashion_type', 'season', 'year', 'usage', 'weight', 'price', 'comments', 'classification_brand');
	foreach($style as $key=>$value)
	{
		if(!in_array($key, $nonSpecificAttributesKey))
		{
			if(($value === 0) || empty($value))
			{
				$updateError .= " $key is blank;";
			}
		}
	}

	$retVal = array();
	if(empty($updateError))
	{
		$retVal['valid'] = true;
	}
	else
	{
		$retVal['valid'] = false;
		$retVal['error'] = substr($updateError, 0, -1);
	}
	return $retVal;
}

function validateStatus($styleId, $newStatus)
{
	if(empty($styleId))
	{
		return false;
	}
	$curStatus = func_query_first_cell("SELECT styletype FROM mk_product_style WHERE id = '$styleId'", TRUE);
	if($curStatus == $newStatus)
	{
		return true;
	}
	else
	{
		$possStatus = func_query_column("SELECT style_status_code_to FROM style_status_transition_rules WHERE style_status_code_from ='$curStatus'", 0, TRUE);
		return (in_array($newStatus, $possStatus))?true:false;
	}
	return false;
}
