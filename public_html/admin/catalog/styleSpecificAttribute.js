Ext.onReady(function() {
	Ext.QuickTips.init();
    /**
     *get a json store
     */  
    function getJsonStore(jsonData, fields)
    {
        var jsonStore = new Ext.data.JsonStore({
               fields : fields,
               data : jsonData
        });
        return jsonStore;
    }
    
    /**
     * this function handles the combox selection
     */
    function comboBoxSelectListener(id, combo, record, index)
    {
        changeSpecificAttributeType(record);
    }
    
    /**
     * change the article type
     */
    function changeArticleType(articleTypeId)
    {
    	var url = httpLocation+'/admin/catalog/styleSpecificAttribute.php?articleTypeId='+articleTypeId;
        window.open(url, '_self', false);
    }
    
    var articleStore = getJsonStore(articleTypeData, ['id', 'name']);
    var articleSelect = new Ext.form.ComboBox({
    	id: 'articleSelectComboId',
        triggerAction: 'all',
        mode: 'local',
        store: articleStore,
        displayField: 'name',
        valueField: 'id',
        forceSelection : true,
        value: articleTypeId,
        listeners: {
            select: function(combo, record, index) {
                Ext.MessageBox.confirm('Confirm', 'Are you sure you want change Article Type?', function(button){
                    if (button == 'no')
                    {
                        combo.setValue(combo.startValue);
                    }
                    else
                    {
                    	changeArticleType(combo.getValue());
                    }
                });
            }
        }
    });
    
    if(spset == 'true')
    {
        var specificAttributeTypeStore = getJsonStore(specificAttribute, ['id', 'name']);
        var specificAttributeTypeSelect = new Ext.form.ComboBox({
              id: 'speAttrComboId',
              triggerAction: 'all',
              mode: 'local',
              store: specificAttributeTypeStore,
              displayField: 'name',
              valueField: 'id',
              forceSelection : true,
              value: specificAttributeId,
              valueNotFoundText: 'Invalid Selection',
              listeners: {
                    select: function(combo, record, index) {
                        comboBoxSelectListener(combo.id, combo, record, index);
                    }
                }
        });
        var spItem1 = {
                xtype : 'compositefield',
                anchor: '-20',
                msgTarget: 'side',
                fieldLabel: 'Select Specific Attribute',
                items:[specificAttributeTypeSelect]
                };
                
         var spButton1 = new Ext.Button({text: 'Modify Specific Attribute Type',
                    icon: 'images/table_add.png',
                    cls: 'x-btn-text-icon',
                    handler: modifySpecificAttributeType});
                    
        var spButton2 = new Ext.Button({text: 'Add Specific Attribute Type',
                    icon: 'images/table_add.png',
                    cls: 'x-btn-text-icon',
                    handler: addNewSpecificAttributeType});

        var spButton = {items : [{
            layout : 'table',
            border : false,
            layoutConfig : {columns : 2},
            defaults : {width: 180, border : false, layout : 'form'},
            items : [
                {items: spButton1},
                {items: spButton2}
                ]
            }]
        };
        
        var spItem = {items : [{
            layout : 'table',
            border : false,
            layoutConfig : {columns : 1},
            defaults : {width: 400, border : false, layout : 'form'},
            items : [
            	{items: spItem1},
                {items: spButton}
                ]
            }]
        };
    }
    else
    {
    	var spItem = new Ext.Button({
    		text: 'Add New Specific Attribute Type',
            icon: 'images/table_add.png',
            cls: 'x-btn-text-icon',
            handler: addNewSpecificAttributeType}
            );
    }
        
    var articleSelectPanel = new Ext.FormPanel({
        renderTo: 'formPanel',
        frame:true,
        title: 'Update Specific Attribute Types and Values',
        bodyStyle:'padding:5px 5px 0',
        width:380,
        labelWidth: 140,
        items: [{xtype : 'compositefield',
                 anchor: '-20',
                 msgTarget: 'side',
                 fieldLabel: 'Select Article Type',
                 items:[articleSelect]},
                 spItem
                ]
    });   
    
    if(spset == 'true')
    {
    	var sAtt = getSpecificAttributeById(specificAttributeId);
    	var saValue;
    	if(sAtt && sAtt.value)
    	   saValue = sAtt.value;
    	var saValueExists = true;
    	if(!saValue)
    	{
    		saValueExists = false;
    		saValue = [{'id':'', 'name':'', 'is_active': true, 'isNew':true}];
    	}
        var store = getJsonStore(saValue, ['id', 'name', {name: 'is_active', type: 'bool'}]);
        if(!saValueExists)
        {
            store.loadData(saValueExists);
        }
        
        var colModel = new Ext.grid.ColumnModel({
               defaultSortable: false,
               columns: [
                    new Ext.grid.RowNumberer(),
                    new Ext.grid.CheckboxSelectionModel(),
                    {header: "Specific Attribute Value", width: 250, dataIndex: 'name',editor: new Ext.form.TextField({
                    allowBlank: false
                })},
                    {
                xtype: 'booleancolumn',
                header: 'Active',
                dataIndex: 'is_active',
                width: 90,
                trueText: 'Yes',
                falseText: 'No',
                editor: {
                    xtype: 'checkbox'
                }}
                    ]
        });
        
        var sm = new Ext.grid.CheckboxSelectionModel();
        var grid = new Ext.grid.EditorGridPanel({
            id : 'mainEditGrid',
            renderTo: 'contentPanel',
            enableColumnMove : false,
            width:380,
            autoHeight : true,
            store: store,
            clickstoEdit: 1,
            colModel: colModel,
            sm: sm,
            bbar: [{text: 'Add New Specific Attribute Value',
                    icon: 'images/table_add.png',
                    cls: 'x-btn-text-icon',
                    handler: addNewSpecificAttributeValue},
                    {text: 'Save Changes',
                    icon: 'images/table_add.png',
                    cls: 'x-btn-text-icon',
                    handler: saveChanges}]
        });
        
        var win = new Ext.Panel({
            id:'mainwin',
            closable:true,
            width:380,
            height:950,
            plain:true,
            layout: 'border',
            items: [grid]
        });
    }
    else
    {
    	Ext.MessageBox.alert('Specific Attributes', 'Selected article type do not have any attribute types defined');
    }
    
    function changeSpecificAttributeType(record)
    {
        store.loadData(record.json.value);
    }
    
    function saveChanges()
    {
    	var selRows = grid.getSelectionModel().getSelections();
        var noRows = selRows.length;
        if(noRows == 0)
        {
            Ext.MessageBox.alert("Update Specific Attributes", "Please select rows which you want to save.");
            return;
        }
        var params = {};
        params['action'] = 'saveSpecificAttributes';
        params['attribute_type_id'] = Ext.getCmp('speAttrComboId').getValue();
        for(var i = 0; i < noRows; i++)
        {
            params[i+1] = Ext.util.JSON.encode(selRows[i].data);
        }
        var myMask = new Ext.LoadMask(Ext.get('wholeExt'), {msg: "Updating..."});
        myMask.show();
        Ext.Ajax.request({
            url : 'styleSpecificAttributeAjax.php',
            params : params,
            success : function(response, action) {
            	myMask.hide();
                var jsonObj = Ext.util.JSON.decode(response.responseText);
                var saUpdate = jsonObj['updated'];
                var saAdd = jsonObj['added'];
                var html = 'Specific Attribute values successfully';
                if(saUpdate)
                {
                    html += " updated";
                    if(saAdd)
                    {
                        html += " and added.";
                    }
                }
                else if(saAdd)
                {
                    html += " added.";
                }
                
                if(saAdd)
                {
                	alert(html);
                    var articleTypeId = Ext.getCmp('articleSelectComboId').getValue();
                    var specificAttributeId = Ext.getCmp('speAttrComboId').getValue();
                    var url = httpLocation+'/admin/catalog/styleSpecificAttribute.php?articleTypeId='+articleTypeId+'&specificAttributeId='+specificAttributeId;
                    window.open(url, '_self', false);
                }
                else if(saUpdate)
                {
                	alert(html);
                	var selRows = grid.getSelectionModel().getSelections();
                    var noRows = selRows.length;
                    for(var i = 0; i < noRows; i++)
                    {
                        selRows[i].commit();
                    }
                	return;
                }
            }
        });
    }
    
    function addNewSpecificAttributeValue()
    {
    	var Rec = grid.getStore().recordType;
        var p = new Rec({
        	id: '',
            name: 'New',
            is_active: true
        });
        grid.stopEditing();
        var newRow = store.getCount();
        p.data.isNew = true;
        grid.getStore().insert(newRow, p);
        grid.startEditing(newRow, 0);
    }
    
    var SAWindowExists = false;
    /**
     * function to add new SA type window
     * 
     */
    function addNewSpecificAttributeType()
    {
    	if(SAWindowExists)
    	{
    		return;
    	}
    	SAWindowExists = true;
    	var widthPopWin = 350;
    	
        var articleSelectCB = new Ext.form.ComboBox({
            id: 'articleSelectCBId',
            triggerAction: 'all',
            mode: 'local',
            fieldLabel : 'Article Type',
            store: articleStore,
            displayField: 'name',
            valueField: 'id',
            forceSelection : true,
            value: articleTypeId
        });
    	
    	var specificAttributeTypeTF = new Ext.form.TextField({
            id : 'specificAttributeTypeId',
            fieldLabel : 'Specific Attribute Type',
            emptyText : 'Enter Specific Attribute Type',
            allowBlank : false,
            width: '90%'
        });
        
        //creating a group of radiobuttons  
        var isActiveItems = new Ext.form.RadioGroup({
        	id : 'isActiveItemsId',
        	fieldLabel: 'Active',  
            columns: 2, //display the radiobuttons in two columns  
            items: [  
                  {boxLabel: 'Yes', name: 'isActive', inputValue: '1', checked: true},  
                  {boxLabel: 'No', name: 'isActive', inputValue: '0'}
             ]
        });
        
        //creating a group of radiobuttons  
        var isFilterItems = new Ext.form.RadioGroup({
            id : 'isFilterItemsId',
            fieldLabel: 'Filter',  
            columns: 2, //display the radiobuttons in two columns  
            items: [  
                  {boxLabel: 'Yes', name: 'isFilter', inputValue: '1', checked: true},  
                  {boxLabel: 'No', name: 'isFilter', inputValue: '0'}
             ]
        });
        
        var SAForm = new Ext.FormPanel({
        	width: widthPopWin,
            border : false,
            bodyStyle : 'padding: 5px',
            items : [articleSelectCB, specificAttributeTypeTF, isActiveItems, isFilterItems]
        });
                
        var SAWindow = new Ext.Window({
            id:'SAWindowId',
            title: 'Add New Specific Attribute Type',
            width: widthPopWin,
            items: SAForm,
            closable: false,
            buttons: [
            	{
                    text: 'Add Type',
                    handler: function()
                    {
                    	var params = {};
                        params['action'] = 'saveSpecificAttributeType';
                        params['article_type_id'] = Ext.getCmp('articleSelectCBId').getValue();
                    	var newSA = $.trim(Ext.getCmp('specificAttributeTypeId').getValue());
                    	if(!newSA)
                    	{
                    		Ext.MessageBox.alert('Error!', 'Please enter a specific attribute type name.');
                            return;
                    	}
                    	params['attribute_type'] = newSA;
                    	params['is_active'] = Ext.getCmp('isActiveItemsId').getValue().getGroupValue();
                    	params['is_filter'] = Ext.getCmp('isFilterItemsId').getValue().getGroupValue();
                    	
                    	SAWindowExists = false;
                        SAWindow.destroy();
                        var myMask = new Ext.LoadMask(Ext.get('wholeExt'), {msg: "Adding..."});
                        myMask.show();
                        Ext.Ajax.request({
                            url : 'styleSpecificAttributeAjax.php',
                            params : params,
                            success : function(response, action) {
                                myMask.hide();
                                var jsonObj = Ext.util.JSON.decode(response.responseText);
                                var saAdd = jsonObj['added'];
                                var html = 'New Specific Attribute Type successfully added';
                                if(saAdd)
                                {
                                	alert(html);
                                    var articleTypeId = Ext.getCmp('articleSelectComboId').getValue();
                                    var specificAttributeId = jsonObj['id'];
                                    var url = httpLocation+'/admin/catalog/styleSpecificAttribute.php?articleTypeId='+articleTypeId+'&specificAttributeId='+specificAttributeId;
                                    window.open(url, '_self', false);
                                }
                            }
                        });
                    }
                },
            {
                text: 'Cancel',
                handler: function() {
                	SAWindowExists = false;
                    SAWindow.destroy();
                }
            }]
        }).show();
    }
    
    function modifySpecificAttributeType()
    {
        if(SAWindowExists)
        {
            return;
        }
        SAWindowExists = true;
        var widthPopWin = 350;
        var saTypeId = Ext.getCmp('speAttrComboId').getValue();
        
        var specificAttributeTypeCB = new Ext.form.ComboBox({
            id: 'specificAttributeTypeCBId',
            triggerAction: 'all',
            mode: 'local',
            fieldLabel : 'Select Specific Attribute Type',
            store: specificAttributeTypeStore,
            displayField: 'name',
            valueField: 'id',
            forceSelection : true,
            value: saTypeId,
            listeners: {
                select: function(combo, record, index) {
                	setValuesForWindow(record.get('id'));
                }
            }
        });
        
        var specificAttributeTypeTF = new Ext.form.TextField({
            id : 'specificAttributeTypeId',
            fieldLabel : 'Rename To',
            emptyText : 'Enter Specific Attribute Type Name',
            allowBlank : false,
            width: '90%'
        });
        
        //creating a group of radiobuttons  
        var isActiveItems = new Ext.form.RadioGroup({
            id : 'isActiveItemsId',
            fieldLabel: 'Active',  
            columns: 2, //display the radiobuttons in two columns  
            items: [  
                  {boxLabel: 'Yes', name: 'isActive', inputValue: '1', checked: true},  
                  {boxLabel: 'No', name: 'isActive', inputValue: '0'}
             ]
        });
        
        //creating a group of radiobuttons  
        var isFilterItems = new Ext.form.RadioGroup({
            id : 'isFilterItemsId',
            fieldLabel: 'Filter',  
            columns: 2, //display the radiobuttons in two columns  
            items: [  
                  {boxLabel: 'Yes', name: 'isFilter', inputValue: '1', checked: true},  
                  {boxLabel: 'No', name: 'isFilter', inputValue: '0'}
             ]
        });
        
        var SAForm = new Ext.FormPanel({
            width: widthPopWin,
            border : false,
            bodyStyle : 'padding: 5px',
            items : [specificAttributeTypeCB, specificAttributeTypeTF, isActiveItems, isFilterItems]
        });
                
        var SAWindow = new Ext.Window({
            id:'SAWindowId',
            title: 'Modify Specific Attribute Type',
            width: widthPopWin,
            items: SAForm,
            closable: false,
            buttons: [
                {
                    text: 'Update Type',
                    handler: function()
                    {
                        var params = {};
                        params['action'] = 'updateSpecificAttributeType';
                        params['id'] = Ext.getCmp('specificAttributeTypeCBId').getValue();
                        var newSA = $.trim(Ext.getCmp('specificAttributeTypeId').getValue());
                        if(!newSA)
                        {
                            Ext.MessageBox.alert('Error!', 'Please enter a specific attribute type name.');
                            return;
                        }
                        params['attribute_type'] = newSA;
                        params['is_active'] = Ext.getCmp('isActiveItemsId').getValue().getGroupValue();
                        params['is_filter'] = Ext.getCmp('isFilterItemsId').getValue().getGroupValue();
                        
                        SAWindowExists = false;
                        SAWindow.destroy();
                        var myMask = new Ext.LoadMask(Ext.get('wholeExt'), {msg: "Updating..."});
                        myMask.show();
                        Ext.Ajax.request({
                            url : 'styleSpecificAttributeAjax.php',
                            params : params,
                            success : function(response, action) {
                                myMask.hide();
                                var jsonObj = Ext.util.JSON.decode(response.responseText);
                                var saAdd = jsonObj['updated'];
                                var html = 'Specific Attribute Type successfully updated';
                                if(saAdd)
                                {
                                	alert(html);
                                    var articleTypeId = Ext.getCmp('articleSelectComboId').getValue();
                                    var specificAttributeId = jsonObj['id'];
                                    var url = httpLocation+'/admin/catalog/styleSpecificAttribute.php?articleTypeId='+articleTypeId+'&specificAttributeId='+specificAttributeId;
                                    window.open(url, '_self', false);
                                }
                            }
                        });
                    }
                },
            {
                text: 'Cancel',
                handler: function() {
                    SAWindowExists = false;
                    SAWindow.destroy();
                }
            }]
        }).show();
        setValuesForWindow(saTypeId);
    }
    
    function setValuesForWindow(id)
    {
    	var sAtt = getSpecificAttributeById(id);
    	Ext.getCmp('specificAttributeTypeId').setValue(sAtt.name);
    	Ext.getCmp('isActiveItemsId').setValue(sAtt.is_active);
    	Ext.getCmp('isFilterItemsId').setValue(sAtt.is_filter);
    }
    
    function getSpecificAttributeById(id)
    {
    	var noSpeAttr = specificAttribute.length;
        for(var i = 0; i < noSpeAttr; i++)
        {
            if(id == specificAttribute[i].id)
            {
                return specificAttribute[i];
            }
        }
    }
});