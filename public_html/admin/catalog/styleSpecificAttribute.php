<?php
require_once "../auth.php";
require_once "$xcart_dir/include/security.php";
include_once "$xcart_dir/include/func/funcStyleProperties.php";

if(empty($articleTypeId))
{
	$articleTypeId = func_query_first_cell("SELECT id FROM mk_catalogue_classification WHERE is_active = '1' AND parent1 <> '-1' AND parent2 <> '-1' ORDER BY typename ASC", TRUE);
}
//get specific attributes for the selected article type
$specificAttribute = getArticleAttributeTypeWithValuesForModify($articleTypeId);
$smarty->assign("articleTypeId", $articleTypeId);

$articleType = getCatalogCategory("ArticleType");
$smarty->assign("articleType", $articleType);

if(!empty($specificAttribute))
{
	if(!empty($specificAttributeId))
	{
		$smarty->assign("specificAttributeId", $specificAttributeId);
	}
	else
	{
		$smarty->assign("specificAttributeId", $specificAttribute[0]['id']);
	}
	
	$smarty->assign("specificAttribute", json_encode($specificAttribute));
}
$smarty->assign("main","style_specific_attribute");
func_display("admin/home.tpl",$smarty);
?>