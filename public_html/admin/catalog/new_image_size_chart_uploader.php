<?php
require "../auth.php";
include_once "$xcart_dir/include/func/func.image.php";
include_once "$xcart_dir/utils/imagetransferS3.php";

//getting max size of uploaded image from feature gate
$imageUploadMaxSize = FeatureGateKeyValuePairs::getInteger('styleImageUploadMaxSize', 300);
$s_upload_dir='../../images/style/sizechartimages/';
$s_cdn_upload_dir_db="$cdn_base/images/style/sizechartimages/";

if(($REQUEST_METHOD == "POST") && ($_POST['mode'] == 'uploadImages')){

	//get list of style ids, extract each style id, trim them and see if these style ids exists in db
	$styleIdList = $_POST['style_id_list'];
	$styleIds = explode(",", $styleIdList);//array of style ids read as input

	$errorMessage ='';
	$styleIdsCSV = '';
	$totalStyles = count($styleIds);

	//loop through all style ids and sanitize them. If non numberic style id found, then give error
	foreach ($styleIds as $index=>$styleId){
		$escapedStyleId = trim(mysql_real_escape_string($styleId));
		$styleIds[$index] = $escapedStyleId;
		if(!is_numeric($escapedStyleId)){
			$errorMessage .= "</br/>Style id : ".$escapedStyleId." is not numeric";
		}
		if($index  < $totalStyles-1){
			$styleIdsCSV .= $escapedStyleId.",";
		}
		else{
			$styleIdsCSV .= $escapedStyleId;
		}
	}

	//if sanitized styles found, check if these styles are present in our table
	if($errorMessage == ''){
		$validStyle = true;
		$styleProperties = func_query("SELECT * FROM mk_style_properties WHERE style_id in ($styleIdsCSV)", TRUE);

		if(count($styleProperties) != count($styleIds)){ //if rows corresponding to some style ids not fetched
			foreach ($styleProperties as $index=>$value){
				$styleIdsFromDb[$index] = $value['style_id'];
			}
			foreach ($styleIds as $index => $value){
				if(!in_array($value, $styleIdsFromDb)){
					$errorMessage .= "Style Id ".$value." is not a valid style id";
					$validStyle = false;
				}
			}
		}

		//if all styles are valid, check for image
		if($validStyle){
			if(!isset($_FILES) && isset($HTTP_POST_FILES))
			{
				$_FILES = $HTTP_POST_FILES;
			}
			if(isset($_FILES) && !empty($_FILES)){
				$imagesExceedingSizeLimit = '';
				$imageFile = $_FILES['sizeChartImage']['name'][0];
				$imageSize = $_FILES['sizeChartImage']['size'][0];
				$imageFileLocations = $_FILES['sizeChartImage']['tmp_name'][0];

				/*var_dump($imageSize);
				var_dump($imageFileLocations);
				var_dump($imageFile);*/
				$sizeChartImageExt = strtolower(substr($imageFile, -4));
				if($sizeChartImageExt != '.jpg'){
					$errorMessage .= "Size chart image uploaded is not a jpg file";
				}
				elseif ($imageSize > 1024*$imageUploadMaxSize){
					$errorMessage .= "Size chart image exceeds size limit of $imageUploadMaxSize Kb";
				}

			}
			else{
				$errorMessage .= "</br> Please select and upload proper images";
			}
		}
	}

	if($errorMessage != '')
	{
		$errorMessage .= "<br/>No images uploaded please correct all the above errors.";
		$smarty->assign("errorMessage",$errorMessage);
	}
	else{//perform uploading and saving part
		$imageFile = $_FILES['sizeChartImage']['name'][0];
		$imageFileLocations = $_FILES['sizeChartImage']['tmp_name'][0];
		/*echo "</br>".$s_cdn_upload_dir_db;
		echo "</br>".$s_upload_dir;*/
		//check if size chart already exists
		echo "<br/> File path is ".$s_upload_dir.$imageFile;
		if(!is_file($s_upload_dir.$imageFile)){//if not exists, then upload it
			$sizeChartImageURL = func_resize_backup_uploaded_product_image($imageFileLocations, $imageFile, 'sizechart', true);
			echo "<br/> url is ".$sizeChartImageURL;
		}
		else{//else create image url from cdn directory
			echo "<br/>image already present";
			$sizeChartImageURL=$s_cdn_upload_dir_db.$imageFile;
			echo "<br/>".$sizeChartImageURL;
		}
		if($sizeChartImageURL == null)
		{
			$sizeChartImageURL = "";
		}
		//delete below line
		//$sizeChartImageURL = "".$imageFile;
		//updating db
		$query_data["size_chart_image"]  = $sizeChartImageURL;
		foreach ($styleIds as $index=>$value){
			func_array2updateWithTypeCheck("mk_product_style", $query_data, "id='$value' ");
		}
		$smarty->assign("message","Uploaded size chart image successfully for styles: $styleIdsCSV");
	}
}

$smarty->assign("main","new_image_size_chart_uploader");
func_display("admin/home.tpl",$smarty);
?>