<?php
require_once "../auth.php";
require_once "$xcart_dir/include/security.php";
include_once "$xcart_dir/include/func/funcStyleProperties.php";
require_once "styleAttributeId.php";

use admin\catalog\dao\TimerDBAdapter;
/**
 * this function returns an array containing the style data
 * @param $styleId int value of styleid
 */
function getStyleData($styleId)
{
    if($styleId == null){
        return null;
    }
    $sql = "SELECT style_id, global_attr_article_type AS article_type_id, typename as article_type FROM mk_style_properties as mksp join mk_catalogue_classification as mcc on mksp.global_attr_article_type = mcc.id WHERE style_id = '$styleId'";
    $resultArr = func_query($sql, TRUE);
    return $resultArr[0];
}

$table = 'cms_catalog_timer';

$roleMetaData = TimerDBAdapter::getMetaData();
if($mode == "saveRole"){
    $catalogRole = $_POST['catalogRole'];
    $smarty->assign('catalogRole', $catalogRole);
    $XCART_SESSION_VARS['catalogRole'] = $catalogRole;
}
// Ajax handlers for timer actions
if($mode == "updateTimer"){
    $res = array('status' => 'error');
    $action = $_POST['action'];
    $styleData = getStyleData($styleId);
    $catalogRole = $XCART_SESSION_VARS['catalogRole'];
    $styleId = $styleData['style_id'];
    $articleType = $styleData['article_type'];
    $articleTypeId = $styleData['article_type_id'];

    if($styleData == null || empty($styleId)){
        $res['message'] = "Incorrect style id specified : ". $_POST['styleId'];
    } else {
        if($action == 'start'){
            if(TimerDBAdapter::startTimer($styleId, $login, $catalogRole, $articleTypeId, $articleType) != false){
                $res['status'] = 'success';
                $res['message'] = "Timer started for style : ".$styleData['style_id'];
            }
        } elseif ($action == 'stop') {
            if(($timeTaken = TimerDBAdapter::stopTimer($styleId, $login, $catalogRole, $end)) !== false){
                $res['status'] = 'success';
                $res['message'] = "Timer stopped for style : ".$styleData['style_id']. " Total time taken : $timeTaken seconds";
            }else{
                $res['status'] = 'error';
                $res['message'] = "Timer not started yet for style : $styleId, login = '$login', role = '$catalogRole'. Please refresh the page and start the timer.";
            }
        } else {
            $res['status'] = 'error';
            $res['message'] = 'No action specified';
        }
    }
    echo json_encode($res);
    exit;
}


$catalogRole = $XCART_SESSION_VARS['catalogRole'];
$description = $roleMetaData[$catalogRole];
$smarty->assign('catalogRole', $catalogRole);
$smarty->assign('description', $description);

$catalogRoles = TimerDBAdapter::getRoles();
$smarty->assign("catalogRoles", $catalogRoles);

$smarty->assign("main","timer");
func_display("admin/home.tpl",$smarty);
?>
