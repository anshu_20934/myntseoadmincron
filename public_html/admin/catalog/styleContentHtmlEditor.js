/**
 * get html editor for content
 */
    function getHtmlEditor(defaultValue, id, label, width, height, readOnly)
    {
        var newHtmlEditor = new Ext.form.HtmlEditor({
            id:'styleContentHtmlEditor'+id,
            fieldLabel: label,
            width: width,
            height: height,
            enableColors: false,
            readOnly: false,
            enableFontSize: false,
            enableAlignments: false,
            enableFont: false,
            defaultValue:defaultValue,
            readOnly: readOnly
        });
        return newHtmlEditor;
    }
    
/**
 * generate the form panel for content html editor
 * @param {json} styleContent content
 * @return form panel
 */
    function getHtmlEditorFormPanel(originalStyleId, styleContent, widthPanel, heightPanel, changeStatus)
    {
    	//html content editors
    	var width = widthPanel - 10;
    	var height = heightPanel;
    	var columns = 1;
    	if(widthPanel > 1000)
    	{
    		width = widthPanel/2 - 20;
    		height = heightPanel/2 - 50;
    		columns = 2;
    	}
    	
        var htmlEditor1 = getHtmlEditor(styleContent.description, 1, 'Style Description', width, height, false);
        var htmlEditor2 = getHtmlEditor(styleContent.style_note, 2, 'Style Note', width, height, false);
        var htmlEditor3 = getHtmlEditor(styleContent.materials_care_desc, 3, 'Materials and Care Description', width, height, false);
        var htmlEditor4 = getHtmlEditor(styleContent.size_fit_desc, 4, 'Size and Fit Description', width, height, false);
        
        if(changeStatus == true)
        {
        	//style status combo box
            var styleStatuslStore = new Ext.data.JsonStore({
                fields: ['id', 'name'],
                data : styleStatusData,
                sortInfo : {field : 'name', direction : 'ASC'}
            });
        
            var styleStatusCBContent = new Ext.form.ComboBox({
                      id:'styleStatusCBContent',
                      name:'styleStatusCBContent',
                      fieldLabel : 'Style Status',
                      typeAhead: true,
                      triggerAction: 'all',
                      mode: 'local',
                      store: styleStatuslStore,
                      displayField: 'name',
                      valueField: 'id',
                      forceSelection : true,
                      value: styleContent.status,
                      listeners:{
                         select: function (field, newVal, oldVal) {
                            validateStyleNewStatus(newVal['id'], styleContent.status);
                         }
                    }
            });
            
            var items = [{items : [styleStatusCBContent]},
            	         {items : [htmlEditor1]},
                         {items : [htmlEditor2]},
                         {items : [htmlEditor3]},
                         {items : [htmlEditor4]}];
        }
        else
        {
        	var items = [{items : [htmlEditor1]},
                         {items : [htmlEditor2]},
                         {items : [htmlEditor3]},
                         {items : [htmlEditor4]}];
        }
        
        var htmlEditorFormPanel = new Ext.FormPanel({
            id: 'htmlEditorFormPanelId',
            region :'center',
            labelAlign: 'top',
            frame:true,
            bodyStyle:'padding:5px 5px',
            width: widthPanel,
            items: [{
            	layout : 'table',
                layoutConfig : {columns : columns},
                border : false,
                defaults : {layout : 'form'},
                items : items
                }],
                buttons: [{
                            text: 'Copy from Other Style',
                             handler : function()
                            {
                                selectORseachStyle(originalStyleId, "CONTENT");
                            }
                          },
                          {
                            text: 'Save',
                             handler : function()
                            {
                                saveStyleContents(changeStatus);
                            }
                          }
                ]
        });
        return htmlEditorFormPanel;
    }
    
/**validate style status*/  
function validateStyleNewStatus(newStatus, oldStatus)
{
    var toStatusCode = possibleStatusTrans[oldStatus].to_code;
    if($.inArray(newStatus, toStatusCode) == -1)
    {
        Ext.MessageBox.alert('Valid Status', 'Allowed status from current status are ' + possibleStatusTrans[oldStatus].to_name);
        Ext.getCmp('styleStatusCBContent').setValue(oldStatus);
        return;
    }
}
    
/**
 * save content to db for style
 */
    function saveStyleContents(changeStatus)
    {
        if(updateContentStyleId == '')
        {
            alert('Style Id missing');
            return;
        }
        var params = {};
        params['style_id'] = updateContentStyleId;
        params['action'] = 'saveContent';
        params['description'] = Ext.getCmp('styleContentHtmlEditor1').getValue();
        params['style_note'] = Ext.getCmp('styleContentHtmlEditor2').getValue();
        params['materials_care_desc'] = Ext.getCmp('styleContentHtmlEditor3').getValue();
        params['size_fit_desc'] = Ext.getCmp('styleContentHtmlEditor4').getValue();
        if(changeStatus == true)
        {
            params['styletype'] = Ext.getCmp('styleStatusCBContent').getValue();
        }
        
        var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Saving style content..."});
        myMask.show();
        Ext.Ajax.request({
                url : 'styleBulkUpdateAjax.php',
                params : params,
                success : function(response, action) {
                    myMask.hide();
                    var jsonObj = Ext.util.JSON.decode(response.responseText);
                    if(jsonObj.valid)
                    {
                        Ext.Msg.show({
                            title: "Update Message",
                            msg:   'Style Content Successfully Saved',
                            icon:  Ext.MessageBox.INFO
                        });
                    }
                    else
                    {
                        Ext.Msg.show({
                            title: "Error Message",
                            msg:   'Content not saved. '+jsonObj.error,
                            icon:  Ext.MessageBox.INFO
                        });
                    }
                    
                }
        });
    }
    
/**
 * this functin loads the content of the style id to be updated
 */
    function loadStyleContent(load2styleId, load4mstyleId)
    {
        var params = {};
        params['action'] = 'loadStyleContent';
        params['load2styleId'] = load2styleId;
        params['load4mstyleId'] = load4mstyleId;
        var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading style content..."});
        myMask.show();
        Ext.Ajax.request({
            url : 'styleBulkUpdateAjax.php',
            params : params,
            success : function(response, action) {
                myMask.hide();
                var jsonObj = Ext.util.JSON.decode(response.responseText);
                if(jsonObj.success)
                {
                    displayContentEditor(load2styleId, jsonObj.styleAttribute, jsonObj.styleContent[0]);
                    updateContentStyleId = load2styleId;
                    Ext.MessageBox.alert("Style Content", "Style content successfully loaded. Don't forget to save content.");
                }
                else
                {
                    alert('Error: '+jsonObj.error);
                }
                myMask.hide();
                return;
            }
        });
    }
    
/**
 * this function copies the content from one style to other
 */
 function copyContentInfo(originalStyleId, copy4mStyleId)
 {
 	Ext.getCmp('copyStyleWindowId').destroy();
 	loadStyleContent(originalStyleId, copy4mStyleId);
 }
    