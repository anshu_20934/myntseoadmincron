<?php
/**
 * Created by Ravi Gupta.
 * Date: 18 Jan, 2012
 */
require_once "../auth.php";
require_once $xcart_dir."/include/security.php";

//get all the size values corresponding to scale id
if(isset($_GET['mode']) && ($_GET['mode'] == 'getSizeOptions'))
{
	$articleId = mysql_real_escape_string($_GET['article_id']);
	$brandId = mysql_real_escape_string($_GET['brand_id']);
	$sizeValuesJson = json_encode(getSizeOptionsArray($articleId, $brandId));
	echo $sizeValuesJson;
	exit;
}

function getSizeOptionsArray($articleId, $brandId)
{
	$brandValue = func_query_first_cell("SELECT attribute_value FROM mk_attribute_type_values WHERE id = '$brandId'");
	$styleIdArray = func_query_column("SELECT style_id FROM mk_style_properties WHERE global_attr_article_type ='$articleId' AND global_attr_brand = '$brandValue'");
	if (empty($styleIdArray))
	{
		return;
	}
	$styleIds = implode(",", $styleIdArray);
	$sizeValues = func_query_column("SELECT DISTINCT(value) FROM mk_product_options WHERE style in ($styleIds)");
	$sizeValues = implode(",", $sizeValues);
	$sizeValues = str_replace(' ', '', $sizeValues);
	$sizeValues = array_unique(explode(",", $sizeValues));
	natsort($sizeValues);
	$sizeValuesArray = array();
	foreach ($sizeValues as $key=>$value)
	{
		$sizeValuesArray[]['value']=$value;
	}
	return $sizeValuesArray;
}
?>