<?php
/**
 * Created by Nishith.
 * Date: 30 Mar, 2011
 * Time: 12:45:05 PM
 */
require_once( "../auth.php");
require_once ($xcart_dir."/include/security.php");
@include ($xcart_dir."/include/func/func.filter_order.php");

//require_once($xcart_dir."/include/security.php");
define('FORM_ACTION','admin/catalog/attribute_values.php');
$location[] = array("Attribute Values", "");
$groupToModify = '';

if(isset($_GET['mode']) && $_GET['mode'] =='getArticleAttributes')
{
    $id=$_GET['article_id'];
    
    $outstr='{';
    
    
    // Logic to populate article specific attributes
    
    	$outstr= $outstr.' "attributes":[';

		if(isset($_GET['article_id']) && !empty($_GET['article_id'])){
		    $result_set=func_query("select * from mk_attribute_type where catalogue_classification_id=".$id." order by attribute_type asc", TRUE);
    	}else{
    		$result_set=func_query("select * from mk_attribute_type where catalogue_classification_id is null order by attribute_type asc", TRUE);
    	}
    	
	    $firsttimeloop1 = true;
		foreach ($result_set as $row){
			if($firsttimeloop1){
				$firsttimeloop1=false;
				$outstr= $outstr.' {';
			}else{
				$outstr= $outstr.' , {';
			}
    		// Populate name values
    		$outstr= $outstr.' "name":"'.$row['attribute_type'].'", "id":"'.$row['id'].'"';	

			$outstr = $outstr.'}';
			
			
		}

		$outstr = $outstr.']';
    
    //close json
    $outstr = $outstr.'}';
    
    // final output for the json
    echo $outstr;
    exit;
}


if($REQUEST_METHOD == "POST"){

          $isError = false;
          $time=time();	
		  $login=$XCART_SESSION_VARS['identifiers']['A'][login];
          switch($_POST['mode']){
          ############## Condition to add groups ############
          	case  "addgroup" :

                    if(empty($_POST['attribute_value']) || empty($_POST['attribute_type_id'])){
                        $isError = true;
                    }
                    if(!$isError)
                    {
                        $res=func_query("select * from mk_attribute_type_values where attribute_type_id='".$_POST['attribute_type_id']."' and  attribute_value='".$_POST['attribute_value']."' and applicable_set='".$_POST['applicable_set']."' ", TRUE);
                        if(empty($res))
                        {
                        	// check for unique code for brands attribute_type_id is 1
                        	if ($_POST['attribute_type_id'] == "1") {
                        		if (empty($_POST['attribute_code'])) {
									$top_message["content"] = "Attribute Code should not be empty for brand";
                        			break;
                        		}
                        		$sql = "SELECT COUNT(*) AS count from mk_attribute_type_values WHERE attribute_type_id = '1' AND attribute_code = '" . $_POST['attribute_code'] . "'";
                        		$rows = func_query($sql ,TRUE);
                        		if ($rows[0]['count'] != 0) {
                        			$top_message["content"] = "Attribute Code already exists for the brand";
                        			break;
                        		}
                        	}
                        	
                        // Since few places we are using attribute type in search criteria. populating this field for global attributes not for article specific attributes
                        $sql_attribute_type = "SELECT attribute_type from mk_attribute_type WHERE id='".$_POST['attribute_type_id']."' and catalogue_classification_id is null";
                        $res_attribute_type=func_query_first($sql_attribute_type, TRUE);
                        
                        $query_data = array(
                            "attribute_type_id"  =>(!empty($_POST["attribute_type_id"]) ? trim($_POST["attribute_type_id"]) : ''),
                            "attribute_value"  => (!empty($_POST["attribute_value"]) ? trim($_POST["attribute_value"]) : ''),
                        	"attribute_code" => (!empty($_POST["attribute_code"]) ? trim($_POST["attribute_code"]) : ''),
                            "applicable_set"  =>(!empty($_POST["applicable_set"]) ? $_POST["applicable_set"] : "Default"),
	                        "created_on"  =>($time),
	                        "created_by"  =>($login),
	                        "updated_on"  =>($time),
	                        "updated_by"  =>($login),
                            "is_active" =>  (!empty($_POST["is_active"]) ? '1' : '0'),
                        	"is_featured" =>  (!empty($_POST["is_featured"]) ? '1' : '0'),
                        	"is_latest" =>  (!empty($_POST["is_latest"]) ? '1' : '0'),
                        	"filter_order" => (!empty($_POST["filter_order"]) ? $_POST["filter_order"] : '0')	
                            ) ;

                            //if global attribute populate the attribute_type
                            if(!empty($res_attribute_type['attribute_type'])){
                            	$query_data["attribute_type"]=$res_attribute_type['attribute_type'];
                            }
						update_filter_order("mk_attribute_type_values",'',!empty($_POST["filter_order"]) ? $_POST["filter_order"] : '0',"attribute_type_id='".$_POST['attribute_type_id']."'");                            
                        func_array2insert ('mk_attribute_type_values', $query_data);
                        $top_message["content"] = "Added new Value.";
                        //invalidate xcache
                        if($v["attribute_type_id"]==1){
                          	//invalidate xcache
                          	global $xcache;
                          	$xcache->remove('brands_list_array');
                          }
                        }
                        else
                        {
                            $top_message["content"] = "Attribute Value already exists";
                        }
                    }
                    else
                    {
                        $top_message["content"] = "Error! Please input valid data.";
                    }

            break;
		   ########### Condition to update ###############
          	case "update":
			   if (is_array($posted_data))
               {	// array was not coming in the same order when it was displaying on front end.
               		// Now sorting it accordint to the filter order
               		usort($posted_data,'compare_filter_order');
				    foreach ($posted_data as $id=>$v)
                    {
                        if (empty($v["to_delete"]))
                          continue;

	                    if(!empty($v["attribute_type_id"])){
	                    	$attributeType = func_query_first_cell("select attribute_type from mk_attribute_type where id = '".trim($v["attribute_type_id"])."'", TRUE);
	                    	$query_data = array(
	                            "attribute_type_id"  =>(!empty($v["attribute_type_id"]) ? trim($v["attribute_type_id"]) : ''),
	                    		"attribute_type" => $attributeType,
	                            "attribute_value"  =>(!empty($v["attribute_value"]) ? trim($v["attribute_value"]) : ''),
	                            "applicable_set"  =>(!empty($v["applicable_set"]) ? $v["applicable_set"] : "Default"),
	                        	"updated_on"  =>($time),
	                        	"updated_by"  =>($login),
	                            "is_active" =>  (!empty($v["is_active"]) ? '1' : '0'),
	                    		"is_featured" =>  (!empty($v["is_featured"]) ? '1' : '0'),
								"is_latest" =>  (!empty($v["is_latest"]) ? '1' : '0'),	                    	
	                        	"filter_order" => (!empty($v["filter_order"]) ? $v["filter_order"] : '0')
	                            ) ;
	                        update_filter_order("mk_attribute_type_values",$v['id'],$v["filter_order"],"attribute_type_id='".$v['attribute_type_id']."'");    
	                        func_array2update("mk_attribute_type_values", $query_data, "id='".$v['id']."'");
	                    } else{
	             			$is_error=true;	       	
	                    }
                          
                    }
                    	 if($is_error){
							$top_message["content"] = "Ignored values without attribute type and Edited others successfully!!";	                    	 	
                    	 }	else{
                          $top_message["content"] = "Value Edited successfully!!";
                          if($v["attribute_type_id"]==1){
                          	//invalidate xcache
                          	global $xcache;
                          	$xcache->remove('brands_list_array');
                          }
                    	 }
                }
                       break;

          ########### Condition to delete the record ###############
          	case  "delete" :
			   if (is_array($posted_data)) {
			   		$flag = false;
				    foreach ($posted_data as $id=>$v) {
						 if (empty($v["to_delete"])) {
						 	continue;
						 }
						 
						 if ($v["attribute_type_id"] == "1") {
						 	$flag = true;
						 	continue;
						 }
						 
						 update_filter_order("mk_attribute_type_values",$id,"","attribute_type_id='".$v['attribute_type_id']."'");
						 db_query("delete from mk_attribute_type_values where id ='".$id."'") ;
					}
					
					if ($flag) {
						$top_message["content"] = "Brands cannot be deleted!!";
					} else {
						$top_message["content"] = "Value deleted!!";
						if($v["attribute_type_id"]==1){
                          	//invalidate xcache
                          	global $xcache;
                          	$xcache->remove('brands_list_array');
                        }	
					}
	           }
	           break;

          ########### Condition to delete the record ###############
          case "modify":
		  	    if (is_array($posted_data)) {
				    foreach ($posted_data as $groupid=>$v) {
						 if (empty($v["to_delete"]))
							  continue;
					     $groupToModify =  $groupid;

					}

				  ##$top_message["content"] = func_get_langvar_by_name("msg_adm_prdstyle_upd");
	              ##func_header_location("admin_product_styles.php");

	           }
	           break;
        }  ####### End of switch case

}

function compare_filter_order($a, $b)
  {
    return strnatcmp($a['filter_order'], $b['filter_order']);
  }

$sql ="SELECT atv.id, atv.attribute_type, atv.attribute_value, atv.applicable_set, atv.is_active, atv.is_featured, atv.is_latest, atv.attribute_code, atv.attribute_type_id, atv.filter_order, at.catalogue_classification_id
			 FROM  mk_attribute_type_values atv left join mk_attribute_type at on atv.attribute_type_id=at.id";

    $search_type=$_POST['search_type'];
    $search_article_type_id=$_POST['search_article_type_id'];
    if(!empty($search_type)){
    	$sql.= " where atv.attribute_type_id='".$search_type."'";
    }else if(!empty($search_article_type_id)){
    	$sql.= " where at.catalogue_classification_id='".$search_article_type_id."'";
    }else{
    	$sql.= " where at.catalogue_classification_id is null";
    }
    
    //order by clause depending on condition
    if(!empty($search_type)){
    	$sql.=" order by atv.filter_order ";
    }else{
	    $sql.=" order by at.catalogue_classification_id asc,at.attribute_type asc ";
    }
    $smarty->assign("search_type",$search_type);
    $smarty->assign("search_article_type_id",$search_article_type_id);
$a_types= func_query($sql, TRUE);
$smarty->assign("attributes",$a_types);

$article_types=func_query("select * from mk_catalogue_classification where parent1 !='-1' and parent2 !='-1' and is_active=1 order by typename asc", TRUE);
$smarty->assign("article_types",$article_types);

$sql ="select id,attribute_type,catalogue_classification_id from mk_attribute_type where catalogue_classification_id is null order by attribute_type asc;";
$attribute_types= func_query($sql, TRUE);
$smarty->assign("attribute_types",$attribute_types);

$article_type_attributes=array("global" => $attribute_types);
foreach($article_types as $t)
{
    $attributes=func_query("select * from mk_attribute_type where catalogue_classification_id='".$t[id]."'", TRUE);
	$tmpAttr=array();
    foreach($attributes as $a)
    {
		array_push($tmpAttr,array("id" =>$a[id],"attribute_type" =>$a[attribute_type]));
    }
    $article_type_attributes[$t['id']]=$tmpAttr;
}

$smarty->assign("article_type_attributes",$article_type_attributes);

/*echo "<pre/>";print_r($article_type_attributes);
exit;
*/
$smarty->assign("main","attribute_values");
# Assign the current location line
$smarty->assign("location", $location);
$smarty->assign("form_action", FORM_ACTION);
# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);
$smarty->assign ("top_message", $top_message);
@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>

