/**
 * this function copies from the style id of smae article type using ajax
 */
function copyStyleAjax(originalStyleId, copy4mStyleId, copyBulkOrContent)
{
    if(copyBulkOrContent == "CONTENT")
    {
        copyContentInfo(originalStyleId, copy4mStyleId);
    }
}

/**
 * function to display the content html editor
 * for the styleContentUpdate.php page
 */
function displayContentEditor(originalStyleId, styleAttribute, styleContent)
{
    var htmlCmp = Ext.getCmp('mainwin');
    if(htmlCmp)
    {
        htmlCmp.destroy();
    }
    
    //attribute panel
    var styleAttributeStore = new Ext.data.JsonStore({
           fields : ['name', 'value'],
           data : styleAttribute
    });
    
    var nav = new Ext.grid.GridPanel({
        region: 'west',
        split: true,
        width: 350,
        margins:'3 0 3 3',
        cmargins:'3 3 3 3',
        store: styleAttributeStore,
        columns: [{
            header   : 'Name', 
            width    : 150, 
            sortable : false,
            dataIndex: 'name'
        },
        {
            header   : 'Value', 
            width    : 200, 
            sortable : false, 
            dataIndex: 'value'
        }]
    });
       
    var changeStatus = true;
    var htmlEditorFormPanel = getHtmlEditorFormPanel(originalStyleId, styleContent, 575, 200, changeStatus);
    
    var win = new Ext.Panel({
        id:'mainwin',
        renderTo: 'contentPanel',
        closable:true,
        width:950,
        height:1050,
        plain:true,
        layout: 'border',
        items: [nav, htmlEditorFormPanel]
        
    });
}


Ext.onReady(function()
{
	Ext.QuickTips.init();
	var styleSelect = getComboBox(styleIdData, 'styleSelectId', '');
	var styleText = {xtype: 'textfield', id : 'styleTextId',
	                   listeners: {
                          specialkey: function(f,e){
                            if (e.getKey() == e.ENTER)
                            {
                            	var styleId = f.getValue();
                                confirmStyleChange(styleId);
                            }
                          }
                        }};
    var styleLoadButton = {xtype: 'button', text: 'Load Content', 
                            handler: function(){
                            	var styleId = Ext.getCmp('styleTextId').getValue();
                            	if(styleId == '')
                            	{
                            		Ext.MessageBox.alert('Load Content', 'Please enter a Style Id which you want to modify.');
                            		return;
                            	}
                            	else
                            	{
                            		confirmStyleChange(styleId);
                            	}
                            }
                            };
    var styleBulkUpdateButton = {
    	       xtype:'button',
                text: 'Update Other Properties',
                handler: function(){
                    var r = confirm("Are you sure, you want to navigate to other page? Any unsaved changes will be lost.");
                    if(r == true)
                    {
                        var url = httpLocation+'/admin/catalog/styleBulkUpdate.php?style_ids='+style_ids;
                        window.open(url, '_self', false);
                    }
                }
            };
     var styleImageShowButton = {
               xtype:'button',
                text: 'Show Images',
                handler: function(){
                	if(updateContentStyleId == '')
                	{
                		Ext.MessageBox.alert('Load Content', 'Please load a Style Id which you want to modify.');
                        return;
                	}
                	else
                	{
                		getStyleImages(updateContentStyleId);
                        return;
                	}
                }
            };
	var styleSelectPanel = new Ext.FormPanel({
		renderTo: 'formPanel',
        frame:true,
        title: 'Update Contents of Style',
        bodyStyle:'padding:5px 5px 0',
        width: 950,
        labelWidth: 280,
        items: [{
                xtype : 'compositefield',
                anchor: '-20',
                msgTarget: 'side',
                fieldLabel: 'Select or Enter Style which you want to modify',
                items:[styleSelect, styleText, styleLoadButton, styleBulkUpdateButton, styleImageShowButton]
                }]
    });
    var startStyleId = styleIdData[0].id; 
    loadStyleContent(startStyleId, startStyleId);
    Ext.getCmp('styleSelectId').setValue(startStyleId);
/**
 * given json data, display and value field key get a combo box
 */
    function getComboBox(jsonData, id, label)
    {
        var store = getJsonStore(jsonData);
        var genre_edit = new Ext.form.ComboBox({
        	  id:id,
        	  fieldLabel: label,
              typeAhead: true,
              triggerAction: 'all',
              mode: 'local',
              store: store,
              displayField: 'name',
              valueField: 'id',
              forceSelection : true,
              listeners: {
                    select: function(combo, record, index) {
                        var styleId = record.id;
                        confirmStyleChange(styleId);
                    }
                }
        });
        return genre_edit;
    }
    
/**
  * get a jsonstore where data has id and name as keys
  */
    function getJsonStore(jsonData)
    {
        var jsonStore = new Ext.data.JsonStore({
               fields : ['id', 'name'],
               data : jsonData
        });
        return jsonStore;
    }
    
/**
 * ask for confimation before chnging style
 */
    function confirmStyleChange(styleId)
    {	
        if(styleId == '')
        {
            Ext.MessageBox.alert('Error', 'Please enter a Style Id which you want to modify.');
            return;
        }
    	if(styleId == updateContentStyleId)
        {
        	Ext.MessageBox.alert('Load Content', 'You are updating the selected Style Id only.');
            return;
        }
        else
        {
            var r = false;
            if(updateContentStyleId == '')
            {
                r = confirm("Do you want to load this style?");
            }
            else
            {
                r = confirm("Do you want to update other style, any unsaved changes here will be lost?");
            }
            if(r == true)
            {
                loadStyleContent(styleId, styleId);
            }
            else
            {
                Ext.getCmp('styleSelectId').setValue(updateContentStyleId);
            }
        }
    }
    
/**
 * function to get style images
 */
    function getStyleImages(styleId)
    {
    	var params = {};
        params['action'] = 'getStyleImages';
        params['styleId'] = styleId;
        var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading style images..."});
        myMask.show();
        Ext.Ajax.request({
            url : 'styleBulkUpdateAjax.php',
            params : params,
            success : function(response, action) {
                myMask.hide();
                var jsonObj = Ext.util.JSON.decode(response.responseText);
                if(jsonObj.success)
                {
                    var win = new Ext.Window({
                        html: jsonObj.html,
                        height: 150,
                        widht: 600
                    });
                    win.show();
                }
                else
                {
                    Ext.MessageBox.alert("Image Error", "No images for this style has been uploaded.");
                }
                return;
            }
        });
    }
});