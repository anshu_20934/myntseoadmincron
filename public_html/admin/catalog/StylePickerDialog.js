/**
 * for searching style using style vendor article no.
 */
var StylePickerDialog = Ext.extend(Ext.Window, {
    constructor : function(config) {
        this.selectedStyles=config.selectedStyles;
        this.renderMode=config.renderMode;
        this.selectHandler=config.selectHandler;
        this.vendorarticle=config.vendorarticle;
        this.items = this.generateItems();
        var myObject=this;
        config = Ext.apply({
            id: 'searchedStyle',
            header : true,
            border : true,
            labelPad : 10,
            autoScroll : false,
            header : false,
            modal:true,
            frame:true,
            labelWidth : 100,
            validateOnBlur : true,
            fileUpload : false,
            timeout : 100,
            width : "550",
            height:'400',
            layout:"anchor",
            resizable:false,
            defaults : {
                selectOnFocus: true,
                msgTarget: 'side'
            },
            buttons:[{
                text : "Select",
                handler : function() {
                    myObject.close();
                }
            }]
            
        }, config);

        StylePickerDialog.superclass.constructor.call(this, config);
    },
    generateItems : function() {
        var myObject=this;
        var items = [];
        store=new Ext.data.JsonStore({
            fields: ['style_id','image_url','name','article_number','checked'],
            url: "/admin/attribute_grouping.php?mode=get_matching_styles&linkStyleids="+this.selectedStyles+"&vendorarticle="+this.vendorarticle,
            totalProperty:"totalCount",
            remoteSort: false,
            root:"searchResults"
        });
        store.load();
        var i=0;
        items[i++] = new Ext.grid.GridPanel({
            store : store,
            border : false,
            header : false,
            width: 'auto',
            height: 380,
            frame: true,
            labelSeparator : ' :',
            labelWidth : 100,
            padding : 10,
            sortable: true,
            emptyText: 'Nothing to display',
            autoScroll : true,
            
            columnLines: true,
            columns: [{
                header: 'Style Image',
                width: 130,
                sortable: true,
                dataIndex: 'image_url',
                renderer:function (val) {
                    return '<img src="' + val + '" height="160" width="120" >';
                }
            },{
                width: 200,
                header: 'Style Name',
                sortable: true,
                dataIndex: 'name'
            },
            {
                width: 100,
                header: 'Article No',
                sortable: true,
                dataIndex: 'article_number'
            },
            {
                width: 50,
                id: 'radio'+i,
                header: 'Select',
                renderer:function (val,cell,record) {
                    if(record.data.checked==0){
                        return '<div style="text-align:center;width:100%;height:100%" ><input type="radio" name="copystyle" onclick="productCheckBoxClickHandle('+val+')"></div>';
                    }
                    return '<div style="text-align:center;width:100%;height:100%" ><input type="radio" name="copystyle" onclick="productCheckBoxClickHandle('+val+')" checked="checked"></div>';
                },
                dataIndex: 'style_id'
            }]
        
        });

        return items;
    }
});

/**
 * this funciton sets select and sets style id to the style id value
 */
function productCheckBoxClickHandle(styleId)
{
    Ext.getCmp("copyFromStyle").setValue(styleId);
    return;
}
