<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: statistics.php,v 1.39 2006/04/07 11:28:00 svowl Exp $
#

require "./auth.php";
require $xcart_dir."/include/security.php";

$location[] = array(func_get_langvar_by_name("lbl_comm_report"), "report_commission.php");


if(empty($mode)) {	$mode = "" ; }
if(empty($product_type)) {	$product_type = "" ; }
if(empty($report_type)) {	$report_type = "" ; }
if(empty($Startdate)) {	$Startdate = "" ; }
if(empty($Enddate)) {	$Enddate = "" ; }

$i= 0;
if ($REQUEST_METHOD == "POST") {


           $commission = array();
		   $SQL = array();
		   $SQL1 = array();

          	if ($mode == "search") {
				$Date1 =explode('/',$Startdate);
				$Date2 =explode('/',$Enddate);
				
                
                $startdate = mktime(0,0,0,$Date1[0],$Date1[1],$Date1[2]);
				$enddate   = mktime(0,0,0,$Date2[0],$Date2[1],$Date2[2]);

								
				if($product_type == "all" ){
                   $SQLText =" GROUP BY    pt.name ";

				}else{
					 $SQLText =" AND pt.id='$product_type'  GROUP BY pt.name";

				}

                  while($startdate <= $enddate ){

					         
                    if($report_type == 'week'){  
                    
						   $stdate = $startdate + 7 * 24 * 60 * 60;
						   
						   $stdate1 = date('Y',$stdate).date('m',$stdate).date('d',$stdate)."000000"; 
						   $startdate1  = date('Y',$startdate).date('m',$startdate).date('d',$startdate)."000000"; 
					
					}elseif($report_type == 'month'){
              
						  $stdate = $startdate + (30 * 24 * 60 * 60); 
						  $stdate1 = date('Y',$stdate).date('m',$stdate).date('d',$stdate)."000000"; 
						  $startdate1  = date('Y',$startdate).date('m',$startdate).date('d',$startdate)."000000"; 
         
					}

					$SQL[$i] = "SELECT  count(p.productid) as num_of_products, pt.name,  DATE_FORMAT($startdate1,'%m/%d/%y') as duration1,DATE_FORMAT($stdate1,'%m/%d/%y') as duration2  FROM  $sql_tbl[producttypes] as pt INNER JOIN $sql_tbl[productstyles] as ps ON pt.id=ps.product_type INNER JOIN  $sql_tbl[products] p ON ps.id = p.product_style INNER JOIN  $sql_tbl[designers] as d ON p.designer = d.id INNER JOIN  $sql_tbl[designer_comm] as  dc ON d.id=dc.designer WHERE  (date_sold >= $startdate1 AND date_sold  <= $stdate1)  ";
					 $SQL[$i] =  $SQL[$i].$SQLText;
					 $commission[$i] = func_query($SQL[$i]);

					 $SQL1[$i] = "SELECT  count(p.productid) as num_of_products, DATE_FORMAT($startdate1,'%m/%d/%y') as duration1,DATE_FORMAT($stdate1,'%m/%d/%y') as duration2  FROM  $sql_tbl[producttypes] as pt INNER JOIN $sql_tbl[productstyles] as ps ON pt.id=ps.product_type INNER JOIN  $sql_tbl[products] p ON ps.id = p.product_style INNER JOIN  $sql_tbl[designers] as d ON p.designer = d.id INNER JOIN  $sql_tbl[designer_comm] as  dc ON d.id=dc.designer WHERE  (date_sold >= $startdate1 AND date_sold  <= $stdate1)  AND d.customerid = 'admin' ";
					 $SQL1[$i] =  $SQL1[$i].$SQLText;
					 $commission1[$i] = func_query($SQL1[$i]);


					 $SQL2[$i] = "SELECT  FORMAT(sum(commission),2) as camt   FROM   $sql_tbl[designer_comm]  WHERE  (date_sold >= $startdate1 AND date_sold  <= $stdate1)  ";
					 $commission2[$i] = func_query($SQL2[$i]);


				 

				              
				 $startdate = $stdate;
                 $i++;  


		     }

			 // calculate the and total of all sales made
			    $totalComm = 0;
				$totalorders = 0;
			    for($j=0 ;$j< count($SQL2);$j++){
					
					   $rs = mysql_query($SQL2[$j]);
				       while($row = mysql_fetch_array($rs)){
                           $totalComm += $row[0] ;
					   }
					   mysql_free_result($rs);
		        } 
                /*end of for loop*/

				
               
		}

}


$product_types = func_query ("SELECT $sql_tbl[producttypes].id, $sql_tbl[producttypes].name FROM $sql_tbl[producttypes] order by $sql_tbl[producttypes].name");

#
# Assign Smarty variables and show template
#

$smarty->assign("mode", $mode);
$smarty->assign("reportType", $report_type);
$smarty->assign("prdType", $product_type);
$smarty->assign("SDate", $Startdate);
$smarty->assign("EDate", $Enddate);

$smarty->assign("prd_types", $product_types);
$smarty->assign("commission", $commission);
$smarty->assign("commission1", $commission1);
$smarty->assign("commission2", $commission2);
$smarty->assign("total", $totalComm);

$smarty->assign("main", "comm");

# Assign the current location line
$smarty->assign("location", $location);

# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
