<?php
	include_once("auth.php");
	require $xcart_dir."/include/security.php";
	include_once("../include/func/func.sms_alerts.php");
	
	global $XCART_SESSION_VARS;
	
	if(!isset($_POST['method'])){
		$queryResult = 'default';
	}
	else if(isset($_POST['method']) && ($_POST['method']) == 'searchMobile'){
		$mobileNumber = $_POST['mobileNoToSearch'];
		$campaignCode = 1; //for free mug campaign, for others will have to be changed.
		$couponMobileQuery = "SELECT coupon FROM $sql_tbl[coupon_mobile] WHERE mobile_number = '$mobileNumber' and campaign_id = '$campaignCode'";
		$couponMobileQueryResult = db_query($couponMobileQuery);
		$row = db_fetch_array($couponMobileQueryResult);
		if($row == null || $row[coupon] == null){
			$queryResult = 'couponNotPresentForThisNumber';	
		}
		else {
			$couponCode = $row[coupon];
			$queryResult = calculateCouponStatus($couponCode, $mobileNumber);
			/*
			$couponUsageQuery = "select times_used, expire from $sql_tbl[discount_coupons] where coupon = '$couponCode'";
				$couponUsageQueryResult = db_query($couponUsageQuery);
				$row = db_fetch_array($couponUsageQueryResult);
				$times_used = $row[times_used];
				$expire = $row[expire];
				if($times_used > 0){
					$queryResult = "couponAlreadyUsed";
				}else if($expire < time()){
					$queryResult = "couponExpired";
				}else{
					$queryResult = "couponPresentAndUsable";
					x_session_register("registrationCouponCode", $couponCode);
					x_session_register("registrationCouponMobile", $mobileNumber);
				}*/
		}		
	}
	else if(isset($_POST['method']) && ($_POST['method']) == 'searchEmail'){
		$emailId = $_POST['emailIdToSearch'];
		$loginExistsQuery = "Select * from $sql_tbl[customers] where login='$emailId'";
		$loginExistsQueryResult = db_query($loginExistsQuery);
		$row = db_fetch_array($loginExistsQueryResult);
		if($row == null || $row['reg_coupon_gen_key'] == null){
			$queryResult = 'unregisterdUser';
		}
		else {
			$emailHash = $row['reg_coupon_gen_key'];
			$campaignCode = 1;
			$couponLoginQuery = "SELECT coupon, mobile_number FROM $sql_tbl[coupon_mobile] WHERE login = '$emailId' and campaign_id = '$campaignCode'";
			$couponLoginQueryResult = db_query($couponLoginQuery);
			$row = db_fetch_array($couponLoginQueryResult);
			if($row == null || $row[coupon] == null){
				$queryResult = 'couponNotGenerated';
				$genearteCouponLink = $http_location . "/sendCoupon.php?email=" .$emailHash;
			}
			else {
				$couponCode = $row[coupon];
				$mobileNumber = $row[mobile_number];
				$queryResult = calculateCouponStatus($couponCode, $mobileNumber);
			}
		}
		
	}
	else if(isset($_POST['method']) && ($_POST['method']) == 'resendCoupon'){
		func_send_sms_cellnext($XCART_SESSION_VARS[registrationCouponMobile], "Your free mug coupon code is : " . $XCART_SESSION_VARS[registrationCouponCode] . " - Thanks for calling Myntra");
		$queryResult = "couponResent";
	}
	
	function calculateCouponStatus($couponCode, $mobileNumber){
		global $sql_tbl;
		$couponUsageQuery = "select times_used, times_locked, expire from $sql_tbl[discount_coupons] where coupon = '$couponCode'";
		$couponUsageQueryResult = db_query($couponUsageQuery);
		$row = db_fetch_array($couponUsageQueryResult);
		$times_used = $row[times_used];
		$expire = $row[expire];
		if($times_used > 0 || $times_locked > 0){
			$couponStatus = "couponAlreadyUsed";
		}else if($expire < time()){
			$couponStatus = "couponExpired";
		}else{
			$couponStatus = "couponPresentAndUsable";
			x_session_register("registrationCouponCode", $couponCode);
			x_session_register("registrationCouponMobile", $mobileNumber);
		}
		return $couponStatus;
	}
	
	$smarty->assign("couponCode",$couponCode);	
	$smarty->assign("queryResult",$queryResult);
	$smarty->assign("genearteCouponLink", $genearteCouponLink);
	$smarty->assign("main","search_mobile_coupon");
	func_display("admin/home.tpl", $smarty);