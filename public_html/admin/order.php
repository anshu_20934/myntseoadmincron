<?php
#
# $Id: order.php,v 1.60 2006/01/11 06:55:57 mclap Exp $
#

echo "Access to this page for viewing any order related information is disabled.<br>You can view the relevant information on Prism  (http://prism.mynt.myntra.com)<br><br>Please send an email to servicedesk@myntra.com and send the following details :<br><br>Name :<br>Location :<br>Manager :<br>Business Justification :   (( what info in this xcart page was used by you )<br><br>Request servicedesk@myntra.com for access for prism.mynt.myntra.com with prism readonly role.";
exit(0);

require "./auth.php";
require_once $xcart_dir."/include/security.php";
include_once $xcart_dir."/include/func/func.order.php";
include_once $xcart_dir."/include/func/func.courier.php";
include_once $xcart_dir."/include/func/funcactivated_on.courier.php";
include_once $xcart_dir."/admin/enums/GiftCardType.php";
include_once $xcart_dir."/admin/enums/GiftCardStatus.php";
include_once $xcart_dir."/admin/enums/GiftOrderStatus.php";
include_once($xcart_dir."/include/class/widget/class.widget.keyvaluepair.php");

use oms\OrderDetailRequestProcessor;
use revenue\payments\service\PaymentServiceInterface;

$orderid = $_GET["orderid"];
$orderid = trim($orderid);

if(isInteger($orderid)) {
	$weblog->info("Mode - $mode");
	$processor= new OrderDetailRequestProcessor();
	$orderid = $_GET['orderid'];
    
    $specialPincodeToWarehouseIdMap = json_decode(WidgetKeyValuePairs::getWidgetValueForKey('specialPincodeToWarehouseIdMap'), true);
	$specialPincodes = array_keys($specialPincodeToWarehouseIdMap);
    $s_zipcode = func_query_first_cell("select s_zipcode from xcart_orders where orderid = ".$orderid);
	$special_pincode =  in_array($s_zipcode, $specialPincodes) ? 'true' : 'false';
    $smarty->assign("special_pincode",$special_pincode);

        $skipSpecialPincodeRest = WidgetKeyValuePairs::getWidgetValueForKey('skipSpecialPincodeRestriction');
        $skipSpecialPincodeRestriction =    ($specialPincodeToWarehouseIdMap[$s_zipcode] == $skipSpecialPincodeRest) ? 'true':'false';
        $smarty->assign("skipSpecialPincodeRestriction",$skipSpecialPincodeRestriction);

    
	if ($mode == 'pptocodqueued' && $orderid) {
		func_header_location("$http_location/pptocod.php?adminchangepptocod=true&orderid=" . $orderid);
	} elseif ($mode == 'resetcouriertracking' && $orderid) {
		$processor->processRequestToResetCourierTracking($orderid);
	} elseif ($mode == 'queue_oos_order' && $orderid) {
		$response = $processor->checkAvailabilityAndQueueOrder($orderid);
		echo json_encode($response);
		exit(0);
	} elseif ($mode == 'queue_oos_release' && $orderid) {
		$response = $processor->checkAvailabilityAndQueueRelease($orderid);
		echo json_encode($response);
		exit(0);
	}elseif($mode == 'queue_cberr_order' && $orderid){
		$response = $processor->checkCashBackAndQueueOrder($orderid);
		echo json_encode($response);
		exit(0);
	}elseif ($mode == 'removeskuitem' && $orderid) {
		$skuItemCodeToRemove = $HTTP_POST_VARS['skuItemCode'];
		$skuId = $HTTP_POST_VARS['skuId'];
		$userid = $_POST['userid'];
		$processor->processRequestToRemoveItem($userid,$orderid,$skuItemCodeToRemove,$skuId);
	} else if ($mode == 'get_data' && $orderid) {
		$view = $HTTP_POST_VARS['view'];
		switch($view) {
			case 'paymentlogs':
					$smarty->assign("orderid", $orderid);
				
					$orderPaymentLog = func_query_first("select * from mk_payments_log where orderid=$orderid");
					$smarty->assign("orderPaymentLog", $orderPaymentLog);
					$html=func_display("admin/main/payments_detail_info.tpl", $smarty, false);
					
					$orderPaymentServiceLog = PaymentServiceInterface::getPaymentLog($orderid);
					$orderPaymentServiceLog = json_decode($orderPaymentServiceLog, true);
					$smarty->assign("orderPaymentServiceLog", $orderPaymentServiceLog);
					$html.=func_display("admin/main/payments_service_detail_info.tpl", $smarty, false);
					break;
					
			case 'returns':
				$returns = get_return_request_for_order($orderid);
				$returned_itemids = array();
				if($returns) {
					foreach ($returns as $return)
						$returned_itemids[] = $return['itemid'];
				}
				$smarty->assign("order_returns", $returns);
				$smarty->assign("returned_itemids", implode(",", $returned_itemids));
				$html=func_display("admin/main/order_returns.tpl", $smarty, false);
				break;
			case 'tracking':
				$order = func_query_first("select courier_service, tracking, shippeddate, packeddate from xcart_orders where orderid=$orderid");
				if($order['packeddate'] && $order['packeddate'] != 0 || 
						$order['shippeddate'] && $order['shippeddate'] != 0) { // shippeddate is either NULL or 0 for WP orders
					$shipment_tracking_details = getOrderTrackingDetails($orderid, true, $order['courier_service'], $order['tracking'], 'LEVEL2');
					if ($shipment_tracking_details) {
						$smarty->assign("shipment_tracking_details", $shipment_tracking_details);
					}
				}
				$html=func_display("admin/main/order_shipment_tracking.tpl", $smarty, false);
				break;
			case 'trip':
				$orderTripInfo = TripApiClient::getOrderTripDetail($orderid);
				if ($orderTripInfo) {
					$smarty->assign("order_trip_info", $orderTripInfo);
				} 
				$html=func_display("admin/main/order_trips_detail.tpl", $smarty, false);
				break;
			case 'comments':
				$commentQuery = "select commenttype as type,commentaddedby,commentdate as date, commenttitle,SUBSTRING(description,1,15) as desc_part,newaddress,giftwrapmessage,description,attachmentname from `mk_ordercommentslog` where orderid='$orderid' order by commentid desc;" ;
				$comments = func_query($commentQuery);
				$smarty->assign("comments",$comments);
				$changerequest_query="select distinct changerequest_status, status from xcart_orders where orderid='$orderid'";
				$changerequest_row= func_query_first($changerequest_query);
				$smarty->assign("orderid", $orderid);
				$smarty->assign("order_status", $changerequest_row['status']);
				$smarty->assign("changerequest_status",$changerequest_row['changerequest_status']);
				$oh_disp_comments = func_query("select a.*,moar.reason_display_name as primary_reason from mk_order_action_reasons moar,(select cdd.*,mr.reason_display_name from (select id,orderid,disposition,reason,comment,created_by,trial_number,created_time from cod_oh_orders_dispostions where orderid = $orderid union select refid as id,orderid,disposition,reason,comment,created_by,trial_number,created_time from cod_oh_orders_dispostions_log where orderid = $orderid) cdd,mk_order_action_reasons mr where mr.action = 'oh_disposition' and cdd.reason = mr.reason) a where a.disposition = moar.reason and moar.action = 'oh_disposition'");
				$smarty->assign("comments_log", $oh_disp_comments);
				$html=func_display("admin/main/order_comments.tpl", $smarty, false);
				break;
			case 'giftordercomments':
				$giftcardId = $_GET['extradata'];
				$giftcommentslog = func_query("select * from gift_card_logs where gift_card_id = $giftcardId order by id desc", true);
				$ordercommentslog = func_query("select * from mk_ordercommentslog where orderid = $orderid order by commentid desc", true);
				
				foreach($giftcommentslog as &$comment){
					$comment['activated_on'] = !empty($comment['activated_on'])?date('d-m-Y H:i:s', $comment['activated_on']):"-";
				}
				$smarty->assign("ordercomments", $ordercommentslog);
				$smarty->assign("comments", $giftcommentslog);
				$smarty->assign("orderid", $orderid);
				$html=func_display("admin/main/gift_order_comments.tpl", $smarty, false);
				break;
			case 'replacements':
				$replacementslistQuery="select orderid, replacement_date, replacement_itemid,replacement_quantity, SUBSTRING(replacement_reason,1,200) as replacement_reason, replacement_type, replacement_address, replacement_city, replacement_state, replacement_country, replacement_zipcode, replacement_amount from `{$sql_tbl['replacements']}` where orderid='$orderid' ;" ;
				$replacementslist= func_query($replacementslistQuery);
				$smarty->assign("replacementslist",$replacementslist);
				$html=func_display("admin/main/order_replacements_list.tpl", $smarty, false);
				break;
			case 'delaynotifications':
				$delaynotificationsQuery="select notificationid , order_id, SUBSTRING(delay_reason,1,200) as delay_reason, expected_date, SUBSTRING(action_needed,1,200) as action_needed, user_email from `{$sql_tbl['delay_notifications']}` where order_id='$orderid' ;" ;
				$delaynotifications=func_query($delaynotificationsQuery);
				$smarty->assign("delaynotifications",$delaynotifications);
				$html=func_display("admin/main/order_delay_notifications.tpl", $smarty, false);
				break;
			case 'sr':
				include $xcart_dir."/admin/manageOrderSR.php";
				$html=func_display("admin/ServiceRequest/orderServiceRequest.tpl", $smarty, false);
				break;
			case 'ohtrial' :
				$oh_disp_comments = func_query("select a.*,moar.reason_display_name as primary_reason from mk_order_action_reasons moar,(select cdd.*,mr.reason_display_name from (select id,orderid,disposition,reason,comment,created_by,trial_number,created_time from cod_oh_orders_dispostions where orderid = $orderid union select refid as id,orderid,disposition,reason,comment,created_by,trial_number,created_time from cod_oh_orders_dispostions_log where orderid = $orderid) cdd,mk_order_action_reasons mr where mr.action = 'oh_disposition' and cdd.reason = mr.reason) a where a.disposition = moar.reason and moar.action = 'oh_disposition'");
				$smarty->assign("comments_log", $oh_disp_comments);
				$html=func_display("admin/main/order_oh_disp_log.tpl", $smarty, false);
				break;
			case 'wmsitems' :
				$skuItems = $processor->getItemDetailsForOrder($orderid);
				$smarty->assign("order_skuItems", $skuItems);
				$html=func_display("admin/main/order_skuitems.tpl", $smarty, false);
				break;	
		}
	    echo $html."#####$view";
	    exit;
	}elseif($mode == 'oh_resolution_update'){
		$oh_res = $_POST['oh_res'];
		$oh_sub_res = $_POST['oh_sub_res'];
		$oh_res_comment = $_POST['oh_res_comment'];
		$orderid = $_REQUEST['orderid'];
		$processor->processRequestForOHResolutionUpdate($login,$orderid,$oh_res,$oh_sub_res,$oh_res_comment);
	}elseif ($mode == "status_change") {
		$sale_type= $_POST[sale_type];
		$oh_reason = $_POST['oh_reason'];
		$sales_agent = $_POST[sales_agent];
		$status_change_comment_msg = $_POST['status_change_comment_msg'];
		$ccavenue = $_POST['ccavenue'];
		$force_inv_update = $_POST['force_inv_update'];
		$processor->processRequestForOrderStatusChange($orderid,$login,$status,$tracking,$courier_service,$cancellation_reason,$cancellation_type,$oh_reason,$details,$sale_type,$sales_agent,$status_change_comment_msg,$ccavenue,$force_inv_update);
	}elseif($mode == "gift_order") {
		global $portalapilog;
		$giftorder = func_query_first("select gc.id as gift_card_id, gc.gift_card_type, gc.status as gift_card_status, gc.sender_email, gc.recipient_email, gc.sender_name, gc.recipient_name, gc.amount, gc.quantity, gc.activated_date, gc.email_last_sent, gc.created_on, gc.updated_on, gco.orderid, gco.login, gco.sub_total, gco.total, gco.discount, gco.cod_charge, gco.payment_surcharge, gco.cash_redeemed, gco.pg_discount, gco.cart_discount, gco.payment_option, gco.status as gift_order_status from gift_cards gc, gift_cards_orders gco where gco.orderid = ".$orderid." and gc.id = gco.gift_card_id", true);
		$giftorder['gift_card_type'] = GiftCardType::getEnumNameForValue($giftorder['gift_card_type']);
		$giftorder['gift_card_status_display'] = GiftCardStatus::getEnumNameForValue($giftorder['gift_card_status']);
		$giftorder['gift_order_status_display'] = GiftOrderStatus::getEnumNameForValue($giftorder['gift_order_status']);
		$smarty->assign("order_type", "gift_order");
		$smarty->assign("order", $giftorder);
	}elseif($mode == ""){
		$processor->processRequestToShowOrderDetails($orderid);
	}
} else {
	$smarty->assign("invalidorder","true");
    $smarty->assign("orderid",$orderid);
    $gift_status = func_query_first_cell("select gift_status from xcart_orders where orderid = ".$orderid);
    $smarty->assign("gift_status",$gift_status);
}
$smarty->assign("main", "history_order");
func_display("admin/home.tpl", $smarty);
?>
