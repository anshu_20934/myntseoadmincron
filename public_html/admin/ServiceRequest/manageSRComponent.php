<?php
/**
 * controller file to manage all service request components(priority,status,category,sub-category)
 * @author: Arun Kumar K
 * legends:
 *      SR - service request 
 */
require "../auth.php";
require $xcart_dir."/include/security.php";
include_once($xcart_dir."/modules/serviceRequest/ServiceRequest.php");
include_once($xcart_dir."/modules/serviceRequest/ServiceRequestException.php");

$mode = (!empty($_GET['mode']))?$_GET['mode']:$_POST['mode'];

$SR = new ServiceRequest();

switch($mode){

    case 'addSRPriority':
        $flag=true;
        try{
            //administrator
            $flag = $SR->isValidUserRoleToManage($login);
            if($flag == false){
                throw new ServiceRequestException('NOT_SUPER_ADMIN');
            }

            //check for priority name
            if(empty($_POST['sr_priority'])){
                throw new ServiceRequestException('SR_PRIORITY_BLANK');
            }

            //check for priority name already exists(since is unique)
            $flag = $SR->priorityObj->isValidSRPriority($_POST['sr_priority']);
            if($flag === false){
                throw new ServiceRequestException('NOT_VALID_SR_PRIORITY');
            }

            //check for priority default
            $flag = $SR->priorityObj->isDefaultSRPrioritySet($_POST['sr_priority_default']);
            if($flag === false){
                throw new ServiceRequestException('NO_DEFAULT_SR_PRIORITY');
            }

            //insert SR priority
            $flag = $SR->priorityObj->addSRPriority( $_POST['sr_priority'],$_POST['sr_priority_desc'],
                                                     $_POST['sr_priority_active'],$_POST['sr_priority_default']);
            if($flag === false){
                throw new ServiceRequestException('CREATE_SR_PRIORITY_ERROR');
            }

            //on success of all subtasks
            $top_message["content"] = "Priority is added successfully";

        }catch(ServiceRequestException $SREx){
            $top_message["content"] = $SREx->getMessage();
        }

        break;


    case 'editSRPriority':
        $flag=true;
        try{
            //administrator
            $flag = $SR->isValidUserRoleToManage($login);
            if($flag === false){
                throw new ServiceRequestException('NOT_SUPER_ADMIN');
            }

            //check for priority name already exists(since is unique)
            $flag = $SR->priorityObj->isValidSRPriority($_POST['sr_priority'], $_POST['sr_priority_id']);
            if($flag === false){                
                throw new ServiceRequestException('NOT_VALID_SR_PRIORITY');
            }

            //check for priority default
            $flag = $SR->priorityObj->isDefaultSRPrioritySet($_POST['sr_priority_default'],$_POST['sr_priority_id']);
            if($flag === false){
                throw new ServiceRequestException('NO_DEFAULT_SR_PRIORITY');
            }

            //insert SR priority
            $flag = $SR->priorityObj->setSRPriority( $_POST['sr_priority_id'],$_POST['sr_priority'],
                                                     $_POST['sr_priority_desc'],$_POST['sr_priority_active'],$_POST['sr_priority_default']);
            if($flag === false){
                throw new ServiceRequestException('UPDATE_SR_PRIORITY_ERROR');
            }

            //on success of all subtasks
            $top_message["content"] = "Priority is updated successfully";

        }catch(ServiceRequestException $SREx){
            $top_message["content"] = $SREx->getMessage();
        }

        break;


    case 'addSRStatus':
            $flag=true;
            try{
                //administrator
                $flag = $SR->isValidUserRoleToManage($login);
                if($flag == false){
                    throw new ServiceRequestException('NOT_SUPER_ADMIN');
                }

                //check for status name
                if(empty($_POST['sr_status'])){
                    throw new ServiceRequestException('SR_STATUS_BLANK');
                }

                //check for status name already exists(since is unique)
                $flag = $SR->statusObj->isValidSRStatus($_POST['sr_status']);
                if($flag === false){
                    throw new ServiceRequestException('NOT_VALID_SR_STATUS');
                }

                //insert SR status
                $flag = $SR->statusObj->addSRStatus( $_POST['sr_status'],$_POST['sr_status_desc'],
                                                         $_POST['sr_status_active']);
                if($flag === false){
                    throw new ServiceRequestException('CREATE_SR_STATUS_ERROR');
                }

                //on success of all subtasks
                $top_message["content"] = "Status is added successfully";

            }catch(ServiceRequestException $SREx){
                $top_message["content"] = $SREx->getMessage();
            }

            break;


        case 'editSRStatus':
            $flag=true;
            try{
                //administrator
                $flag = $SR->isValidUserRoleToManage($login);
                if($flag === false){
                    throw new ServiceRequestException('NOT_SUPER_ADMIN');
                }

                //check for status name already exists(since is unique)
                $flag = $SR->statusObj->isValidSRStatus($_POST['sr_status'], $_POST['sr_status_id']);
                if($flag === false){
                    throw new ServiceRequestException('NOT_VALID_SR_STATUS');
                }

                //update SR status
                $flag = $SR->statusObj->setSRStatus( $_POST['sr_status_id'],$_POST['sr_status'],
                                                         $_POST['sr_status_desc'],$_POST['sr_status_active']);
                if($flag === false){
                    throw new ServiceRequestException('UPDATE_SR_STATUS_ERROR');
                }

                //on success of all subtasks
                $top_message["content"] = "Status is updated successfully";

            }catch(ServiceRequestException $SREx){
                $top_message["content"] = $SREx->getMessage();
            }

            break;

    case 'addSRCategory':
        $flag=true;
        try{
            //administrator
            $flag = $SR->isValidUserRoleToManage($login);
            if($flag == false){
                throw new ServiceRequestException('NOT_SUPER_ADMIN');
            }

            //check for category name
            if(empty($_POST['sr_category'])){
                throw new ServiceRequestException('SR_CATEGORY_BLANK');
            }

            //check for category name already exists for the SR type
            $flag = $SR->categoryObj->isValidSRCategory($_POST['sr_category'],$_POST['sr_category_type']);
            if($flag === false){
                throw new ServiceRequestException('NOT_VALID_SR_CATEGORY');
            }

            //calculate SLA in terms of seconds from day:hour
            $SLAinSec = $SR->convertDayHourToTime($_POST['sr_category_SLA_day'],$_POST['sr_category_SLA_hour']);
            /*if($SLAinSec == null){
                throw new ServiceRequestException('SLA_CATEGORY_IS_NULL');
            }*/

            //insert SR category
            $flag = $SR->categoryObj->addSRCategory( $_POST['sr_category'],$_POST['sr_category_type'],$SLAinSec,
                                                 $_POST['sr_category_desc'],$_POST['sr_category_active']);
            if($flag === false){
                throw new ServiceRequestException('CREATE_SR_CATEGORY_ERROR');
            }

            //on success of all subtasks
            $top_message["content"] = "Category is added successfully";

        }catch(ServiceRequestException $SREx){
            $top_message["content"] = $SREx->getMessage();
        }

        break;


    case 'editSRCategory':
        $flag=true;
        try{
            //administrator
            $flag = $SR->isValidUserRoleToManage($login);
            if($flag === false){
                throw new ServiceRequestException('NOT_SUPER_ADMIN');
            }

            //check for category name already exists for the SR type
            $flag = $SR->categoryObj->isValidSRCategory($_POST['sr_category'], $_POST['sr_category_type'],$_POST['sr_category_id']);
            if($flag === false){
                throw new ServiceRequestException('NOT_VALID_SR_CATEGORY');
            }

            //calculate SLA in terms of seconds from day:hour
            $SLAinSec = $SR->convertDayHourToTime($_POST['sr_category_SLA_day'],$_POST['sr_category_SLA_hour']);

            //update SR category
            $flag = $SR->categoryObj->setSRCategory( $_POST['sr_category_id'],$_POST['sr_category'],$_POST['sr_category_type'],
                                                     $SLAinSec,$_POST['sr_category_desc'],$_POST['sr_category_active']);
            if($flag === false){
                throw new ServiceRequestException('UPDATE_SR_CATEGORY_ERROR');
            }

            //on success of all subtasks
            $top_message["content"] = "Category is updated successfully";

        }catch(ServiceRequestException $SREx){
            $top_message["content"] = $SREx->getMessage();
        }
        break;


    case 'addSRSubCategory':
        $flag=true;
        try{
            //administrator
            $flag = $SR->isValidUserRoleToManage($login);
            if($flag == false){
                throw new ServiceRequestException('NOT_SUPER_ADMIN');
            }

            //check for subcategory name
            if(empty($_POST['sr_subcategory'])){
                throw new ServiceRequestException('SR_SUBCATEGORY_BLANK');
            }

            //check for subcategory name already exists for the SR type
            $flag = $SR->subCategoryObj->isValidSRSubCategory($_POST['sr_subcategory'],$_POST['sr_category_id']);
            if($flag === false){
                throw new ServiceRequestException('NOT_VALID_SR_SUBCATEGORY');
            }

            //calculate SLA in terms of seconds from day:hour
            $SLAinSec = $SR->convertDayHourToTime($_POST['sr_subcategory_SLA_day'],$_POST['sr_subcategory_SLA_hour']);
            /*if($SLAinSec == null){
                throw new ServiceRequestException('SLA_SUBCATEGORY_IS_NULL');
            }*/

            //insert SR subcategory
            $flag = $SR->subCategoryObj->addSRSubCategory( $_POST['sr_subcategory'],$_POST['sr_category_id'],$SLAinSec,
                                                 $_POST['sr_subcategory_desc'],$_POST['sr_subcategory_active']);
            if($flag === false){
                throw new ServiceRequestException('CREATE_SR_SUBCATEGORY_ERROR');
            }

            //on success of all subtasks
            $top_message["content"] = "Sub-Category is added successfully";

        }catch(ServiceRequestException $SREx){
            $top_message["content"] = $SREx->getMessage();
        }

        break;


    case 'editSRSubCategory':
        $flag=true;
        try{
            //administrator
            $flag = $SR->isValidUserRoleToManage($login);
            if($flag === false){
                throw new ServiceRequestException('NOT_SUPER_ADMIN');
            }

            //check for subcategory name already exists for the SR type
            $flag = $SR->subCategoryObj->isValidSRSubCategory($_POST['sr_subcategory'], $_POST['sr_category_id'],$_POST['sr_subcategory_id']);
            if($flag === false){
                throw new ServiceRequestException('NOT_VALID_SR_SUBCATEGORY');
            }

            //calculate SLA in terms of seconds from day:hour
            $SLAinSec = $SR->convertDayHourToTime($_POST['sr_subcategory_SLA_day'],$_POST['sr_subcategory_SLA_hour']);

            //update SR sub-category
            $flag = $SR->subCategoryObj->setSRSubCategory( $_POST['sr_subcategory_id'],$_POST['sr_subcategory'],$_POST['sr_category_id'],
                                                     $SLAinSec,$_POST['sr_subcategory_desc'],$_POST['sr_subcategory_active']);
            if($flag === false){
                throw new ServiceRequestException('UPDATE_SR_SUBCATEGORY_ERROR');
            }

            //on success of all subtasks
            $top_message["content"] = "Sub-Category is updated successfully";

        }catch(ServiceRequestException $SREx){
            $top_message["content"] = $SREx->getMessage();
        }
        break;
}

//category
$allCategory = $SR->categoryObj->getSRCategory();
foreach($allCategory as $idx=>$cat){
    //convert SLA in second to day and hour
    $catTimeString = $SR->convertSRTimeDifference($cat['SLA_in_sec']);
    $allCategory[$idx]['SLA_day'] = $catTimeString['d'];
    $allCategory[$idx]['SLA_hour'] = $catTimeString['h'];
}

//SLA days
$SLAdays = array();
for($i=0;$i<=31;$i++){
    $SLAdays[] = $i;
}

//SLA hours
$SLAhours = array();
for($i=0;$i<=23;$i++){
    $SLAhours[] = $i;
}

//subcategory
$allSubCategory = $SR->subCategoryObj->getAllSRSubCategoryViewList();
foreach($allSubCategory as $idx=>$subcat){
    //convert SLA in second to day and hour
    $subcatTimeString = $SR->convertSRTimeDifference($subcat['SLA_in_sec']);
    $allSubCategory[$idx]['SLA_day'] = $subcatTimeString['d'];
    $allSubCategory[$idx]['SLA_hour'] = $subcatTimeString['h'];
}

//all priority
$allPriority = $SR->priorityObj->getSRPriority();

//all status
$allStatus = $SR->statusObj->getSRStatus();

$top_message["anchor"] = "featured";


$smarty->assign("main","manage_sr_component");
$smarty->assign("sr_category",$allCategory);
$smarty->assign("sr_subcategory",$allSubCategory);
$smarty->assign("sr_priority",$allPriority);
$smarty->assign("sr_status",$allStatus);
$smarty->assign("SLA_days",$SLAdays);
$smarty->assign("SLA_hours",$SLAhours);

$smarty->assign("top_message", $top_message);
@include $xcart_dir."/modules/gold_display.php";

func_display("admin/home.tpl",$smarty);