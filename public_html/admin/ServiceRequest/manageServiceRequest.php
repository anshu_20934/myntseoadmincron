<?php
/**
 * controller file to manage all service requests(view,sort,search,export result)
 * @author: Arun Kumar K
 * legends:
 *      SR - service request
 *      fSRid - filter by SR_id
 *      sbSRid - sort by SR_id 
 */
require "../auth.php";
require $xcart_dir."/include/security.php";
include_once $xcart_dir.'/include/func/func.order.php';
include_once($xcart_dir."/modules/serviceRequest/ServiceRequest.php");
include_once($xcart_dir."/modules/serviceRequest/SRNonOrder.php");
include_once($xcart_dir."/modules/serviceRequest/SROrder.php");

$mode = (!empty($_GET['mode']))?$_GET['mode']:$_POST['mode'];
$nonOrderSRId = (!empty($_GET['non_order_sr_id']))?$_GET['non_order_sr_id']:$_POST['non_order_sr_id'];
$SR = new ServiceRequest();

###search filter###
$filter = array();
if (!empty($_GET['fSRid'])) {
    $filter += array('fSRid'=>$_GET['fSRid']);
}
if (!empty($_GET['fCSRid'])) {
    $filter += array('fCSRid'=>$_GET['fCSRid']);
}
if (!empty($_GET['fSROid'])) {
    $filter += array('fSROid'=>$_GET['fSROid']);
}
if (!empty($_GET['fSRType'])) {
    $filter += array('fSRType'=>$_GET['fSRType']);
}
if (!empty($_GET['fSRRef'])) {
    $filter += array('fSRRef'=>$_GET['fSRRef']);
}
if (!empty($_GET['fSRComp'])) {
    $filter += array('fSRComp'=>$_GET['fSRComp']);
}        
if (!empty($_GET['fCat'])) {
    $filter += array('fCat'=>$_GET['fCat']);
}
if (!empty($_GET['fSCat'])) {
    $filter += array('fSCat'=>$_GET['fSCat']);
}
if (!empty($_GET['fStat'])) {
    $filter += array('fStat'=>$_GET['fStat']);
}
if (!empty($_GET['SR_department'])) {
    $filter += array('SR_department'=>$_GET['SR_department']);
}
if (!empty($_GET['SR_channel'])) {
    $filter += array('SR_channel'=>$_GET['SR_channel']);
}
if (!empty($_GET['fStat'])) {
    $filter += array('fStat'=>$_GET['fStat']);
}
if (!empty($_GET['fPri'])) {
    $filter += array('fPri'=>$_GET['fPri']);
}
if (!empty($_GET['fStartCallback'])) {
    $startCBTime = strtotime($_GET['fStartCallback']);
    $filter += array('fStartCallback'=>$startCBTime);
}
if (!empty($_GET['fEndCallback'])) {
    $endCBTime = strtotime($_GET['fEndCallback']);
    $filter += array('fEndCallback'=>$endCBTime);
}
if (!empty($_GET['fStartCrDate'])) {
    $startCRTime = $SR->getDateToTime($_GET['fStartCrDate'],'start');
    $filter += array('fStartCrDate'=>$startCRTime);
}
if (!empty($_GET['fEndCrDate'])) {
    $endCRTime = $SR->getDateToTime($_GET['fEndCrDate'],'end');
    $filter += array('fEndCrDate'=>$endCRTime);
}
if (!empty($_GET['fStartDuDate'])) {
    $startDuTime = $SR->getDateToTime($_GET['fStartDuDate'],'start');
    $filter += array('fStartDuDate'=>$startDuTime);
}
if (!empty($_GET['fEndDuDate'])) {
    $endDuTime = $SR->getDateToTime($_GET['fEndDuDate'],'end');
    $filter += array('fEndDuDate'=>$endDuTime);
}
if (!empty($_GET['fStartClDate'])) {
    $startClTime = $SR->getDateToTime($_GET['fStartClDate'],'start');
    $filter += array('fStartClDate'=>$startClTime);
}
if (!empty($_GET['fEndClDate'])) {
    $endClTime = $SR->getDateToTime($_GET['fEndClDate'],'end');
    $filter += array('fEndClDate'=>$endClTime);
}
if (!empty($_GET['fAge'])) {
    $filter += array('fAge'=>$_GET['fAge']);
}
if (!empty($_GET['fRepo'])) {
    $filter += array('fRepo'=>$_GET['fRepo']);
}
if (!empty($_GET['fReso'])) {
    $filter += array('fReso'=>$_GET['fReso']);
}
if (!empty($_GET['fCust'])) {
    $filter += array('fCust'=>$_GET['fCust']);
}
###search filter###

###sort###
$order = array();

//if not sorted then sort by SR_id by default
$isSorted = false;

if(!empty($_GET['sbSRid']) && !empty($_GET['obSRid'])){
    $order += array($_GET['sbSRid']=>$_GET['obSRid']);
    $isSorted = true;
}
if(!empty($_GET['sbCat']) && !empty($_GET['obCat'])){
    $order += array($_GET['sbCat']=>$_GET['obCat']);
    $isSorted = true;
}
if(!empty($_GET['sbSCat']) && !empty($_GET['obSCat'])){
    $order += array($_GET['sbSCat']=>$_GET['obSCat']);
    $isSorted = true;
}
if(!empty($_GET['sbPri']) && !empty($_GET['obPri'])){
    $order += array($_GET['sbPri']=>$_GET['obPri']);
    $isSorted = true;
}
if(!empty($_GET['sbStat']) && !empty($_GET['obStat'])){
    $order += array($_GET['sbStat']=>$_GET['obStat']);
    $isSorted = true;
}
if(!empty($_GET['sbCbDate']) && !empty($_GET['obCbDate'])){
    $order += array($_GET['sbCbDate']=>$_GET['obCbDate']);
    $isSorted = true;
}
if(!empty($_GET['sbCrDate']) && !empty($_GET['obCrDate'])){
    $order += array($_GET['sbCrDate']=>$_GET['obCrDate']);
    $isSorted = true;
}
if(!empty($_GET['sbDuDate']) && !empty($_GET['obDuDate'])){
    $order += array($_GET['sbDuDate']=>$_GET['obDuDate']);
    $isSorted = true;
}
if(!empty($_GET['sbDeviation']) && !empty($_GET['obDeviation'])){
    $order += array($_GET['sbDeviation']=>$_GET['obDeviation']);
    $isSorted = true;
}
if(!empty($_GET['sbClDate']) && !empty($_GET['obClDate'])){
    $order += array($_GET['sbClDate']=>$_GET['obClDate']);
    $isSorted = true;
}
if(!empty($_GET['sbRepo']) && !empty($_GET['obRepo'])){
    $order += array($_GET['sbRepo']=>$_GET['obRepo']);
    $isSorted = true;
}
//customer email exists both in order and non-order SR(hence sorting in both)
if(!empty($_GET['sbCust']) && !empty($_GET['obCust'])){    
    $order += array('osr.customer_email'=>$_GET['obCust']);
    $order += array('nosr.customer_email'=>$_GET['obCust']);
    $isSorted = true;
}
if(!empty($_GET['sbResol']) && !empty($_GET['obResol'])){
    $order += array($_GET['sbResol']=>$_GET['obResol']);
    $isSorted = true;
}

//default sort by SR_id(desc)
if($isSorted === false){
    $order += array('SR_id'=>'desc');
}
###sort###


switch($mode){
    case 'exportSR':
        $allSR = $SR->getSR($filter,$order,null,null,$useRO=true);        

        //not to have these columns in xls
        $allSRLimitedColumnToExport = array();
        $requiredColumns = array('SR_id','customer_SR_id','SR_category','SR_subcategory','SR_priority','SR_status','SR_department',
                                 'SR_channel', 'callbacktime_date', 'created_date',
                                 'ageing','due_date','deviation','reporter','create_summary','updatedby', 'modified_date',
                                 'customer_email','first_name','mobile','closed_date','resolver','close_summary',
                                 'order_id', 'status', 'payment_method', 'delivery_ageing', 'warehouseid', 'courier_service', 'tracking',
                                 'order_address','order_state','order_city','order_zipcode','DC_info',
                                 'refund', 'refund_type', 'refund_value',
                                 'compensation', 'compensation_type', 'compensation_value',
                            );
        foreach($allSR as $idx=>$SRInfo){
            foreach($requiredColumns as $includeColumn){
                $allSRLimitedColumnToExport[$idx][$includeColumn] = $SRInfo[$includeColumn];
            }
        }

        $objPHPExcel = func_create_results_file($allSRLimitedColumnToExport, "service_requests");
        $fileName = "service_requests_".date("dmy");
        header("Content-Disposition: attachment; filename=$fileName.xls");
        ob_clean();
        flush();
        try {
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->setPreCalculateFormulas(false);
            $objWriter->save('php://output');
        }catch(Exception $ex) {
            $weblog->info("Error Occured = ". $ex->getMessage());
            $message = $ex->getMessage();
        }

        //to control furthur export
        unset($_GET['exportSR']);
        break;
}

###pagination###
//max records in page
$limitArray = array(20,40,60,80,100);
$limit = !empty($_GET['pagelimit'])? $_GET['pagelimit'] : 20;

//current page
if(empty($pg) || $pg==0){
    $pg = (isset($_GET['pg']) && ctype_digit($_GET['pg'])) ?$_GET['pg'] : 1;
}

//offset
$offset = $limit * $pg - $limit;

//total recs
$total = $SR->getSRCount($filter,$useRO=true);
$totalpages= ceil($total/$limit);

//build url
$queryString = http_build_query($_GET);
$sorturl = "manageServiceRequest.php?{$queryString}&pg=";
$paginator = paginate($limit, $pg, $total, $sorturl, $sortparam);
$url = "manageServiceRequest.php?". $queryString;

//to show [showing x-y of z records]
$offsetEnd = $offset+$limit;
while($offsetEnd>$total){
    $offsetEnd--;
}

$smarty->assign("limitarray",$limitArray);
$smarty->assign("pg",$pg);
$smarty->assign("paginator",$paginator);
$smarty->assign("totalpages",$totalpages);
$smarty->assign("pagelimit",$limit);
$smarty->assign("offset_start",$offset+1);
$smarty->assign("offset_end",$offsetEnd);
$smarty->assign("total_record",$total);
###pagination###

//all order sr
$allSR = $SR->getSR($filter,$order,$offset,$limit,$useRO=true);

//cat/subcat
$allCategory = $SR->categoryObj->getSRCategory();
foreach($allCategory as $idx=>$cat){
    $allSubCategoryForCat = $SR->subCategoryObj->getSRSubCategory($cat['id']);

    //convert SLA to string for sub-category
    foreach($allSubCategoryForCat as $sidx=>$sCat){
        $subTimeString = $SR->convertSRTimeDifference($sCat['SLA_in_sec']);
        $allSubCategoryForCat[$sidx]['SLA_string'] = $subTimeString['d:h'];
    }

    $catTimeString = $SR->convertSRTimeDifference($cat['SLA_in_sec']);
    $allCategory[$idx]['SLA_string'] = $catTimeString['d:h'];

    $allCategory[$idx]['subCategories'] = $allSubCategoryForCat;
}

//all priority
$allPriority = $SR->priorityObj->getSRPriority();

//all status
$allStatus = $SR->statusObj->getSRStatus();

//all departments
$allDepartment = $SR->getSRDepartment();

//all channels
$allChannel = $SR->getSRChannel();

//administrator
$adminFlag = $SR->isValidUserRoleToManage($login);
// to give edit permission to all CC folks
$adminFlag = 1;

//ageing array for 15 days
$ageingArray = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15);

$smarty->assign("main","manage_sr");
$smarty->assign("sr_category",$allCategory);
$smarty->assign("sr_priority",$allPriority);
$smarty->assign("sr_status",$allStatus);
$smarty->assign("sr_department",$allDepartment);
$smarty->assign("sr_channel",$allChannel);
$smarty->assign("all_sr",$allSR);
$smarty->assign("message",$message);
$smarty->assign("url",$url);
$smarty->assign("query_string",$queryString);
$smarty->assign("ageingarray",$ageingArray);
$smarty->assign("SR_admin",$adminFlag);

func_display("admin/home.tpl",$smarty);