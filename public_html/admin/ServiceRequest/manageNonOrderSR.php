<?php
/**
 * controller file to manage all non-order related service requests(add,edit,update,close,delete)
 * @author: Arun Kumar K
 * legends:
 *      SR - service request
 *      nosr - non-order service request
 */
require "../auth.php";
require $xcart_dir."/include/security.php";
include_once($xcart_dir."/modules/serviceRequest/SRNonOrder.php");
include_once($xcart_dir."/modules/serviceRequest/SROrder.php");

$mode = (!empty($_GET['mode']))?$_GET['mode']:$_POST['mode'];
$nonOrderSRId = (!empty($_GET['non_order_sr_id']))?$_GET['non_order_sr_id']:$_POST['non_order_sr_id'];
$SRPostMode = (!empty($_GET['sr_post_mode']))?$_GET['sr_post_mode']:$_POST['sr_post_mode'];

$nonOrderSR = new SRNonOrder();

//cat/subcat
$allCategory = $nonOrderSR->categoryObj->getSRCategory();
foreach($allCategory as $idx=>$cat){
    $allSubCategoryForCat = $nonOrderSR->subCategoryObj->getSRSubCategory($cat['id']);

    //convert SLA to string for sub-category
    foreach($allSubCategoryForCat as $sidx=>$sCat){
        $subTimeString = $nonOrderSR->convertSRTimeDifference($sCat['SLA_in_sec']);
        $allSubCategoryForCat[$sidx]['SLA_string'] = $subTimeString['d:h'];
    }

    $catTimeString = $nonOrderSR->convertSRTimeDifference($cat['SLA_in_sec']);
    $allCategory[$idx]['SLA_string'] = $catTimeString['d:h'];

    $allCategory[$idx]['subCategories'] = $allSubCategoryForCat;
}

//all priority
$allPriority = $nonOrderSR->priorityObj->getSRPriority();

//all status
$allStatus = $nonOrderSR->statusObj->getSRStatus();
//remove status 'close' from the list(for update)
foreach($allStatus as $idx=>$status){
    if(strtolower($status['SR_status']) == 'close'){
        unset($allStatus[$idx]);
    }
}

//all departments
$allDepartment = $nonOrderSR->getSRDepartment();

//all channels
$allChannel = $nonOrderSR->getSRChannel();


//$smarty->assign("main","manage_non_order_sr");
$smarty->assign("sr_category",$allCategory);
$smarty->assign("sr_priority",$allPriority);
$smarty->assign("sr_status",$allStatus);
$smarty->assign("sr_department",$allDepartment);
$smarty->assign("sr_channel",$allChannel);
$smarty->assign("non_order_sr_id",$nonOrderSRId);

switch($mode){

    case 'addNonOrderServieRequest':
        $smarty->assign("form_type",'addNonOrderServieRequest');
        $smarty->assign("success","N");

        //handle post form submit
        if($SRPostMode == 1){
            $flag=true;//add status

            $categoryInfo = $nonOrderSR->subCategoryObj->getCategoryOnSubCategory($_POST['subcategory']);
            $categoryId = $categoryInfo[0]['cat_id'];

            //calculate due date based on SLA(of cat/subcategory) and override by subcategory
            $SLA = $categoryInfo[0]['SLA_in_sec'];

            try{

                //callback time validate
                if($_POST['callback'] == 'Y'){
                    $callbackTime = strtotime($_POST['callbacktime']);

                    if($callbackTime < time()){
                        throw new ServiceRequestException('NOT_VALID_CALLBACK_TIME');
                    }

                } else {
                    $callbackTime = null;
                }

                //insert SR
                $flag = $nonOrderSR->createSR( $categoryId, $_POST['subcategory'], $_POST['priority'], $SLA,
                                                $_POST['department'], $_POST['channel'], $callbackTime,                                                
                                                $login, $assignee=null, $copyto=null, $_POST['create_summary'], $noteStatus=1);
                if(empty($flag)){
                    throw new ServiceRequestException('CREATE_SR_ERROR');
                }

                //inseret non-order SR on successful SR
                $lastSRId = $flag;
                $flag = $nonOrderSR->createNonOrderSR($lastSRId, $_POST['customer_email'],$_POST['customer_name'],
                                                        $_POST['customer_phone']);
                if(empty($flag)){
                    $nonOrderSR->deleteSRIrreversible($lastSRId);
                    throw new ServiceRequestException('CREATE_NON-ORDER_SR_ERROR');
                }

                //on success of all subtasks
                $smarty->assign("success","Y");

            }catch(ServiceRequestException $SR){
                $smarty->assign("success","N");
                $smarty->assign("message",$SR->getMessage());
            }
        }

        func_display("admin/ServiceRequest/manageNonOrderSR.tpl",$smarty);
        break;


case 'updateNonOrderServieRequest':        
        $NonOrderServiceRequest = $nonOrderSR->getNonOrderSR(array('id'=>$nonOrderSRId));

        //format callbacktime according to jquery datetimepicker
        if(!empty($NonOrderServiceRequest[0]['callbacktime'])){
            $smarty->assign("callbackTime",date('m/d/Y H:i',$NonOrderServiceRequest[0]['callbacktime']));
        }

        $smarty->assign("form_type",'updateNonOrderServieRequest');
        $smarty->assign("non_order_sr",$NonOrderServiceRequest);
        $smarty->assign("success","N");

        //handle post form submit
        if($SRPostMode == 1){
            $flag=true;//add status

            $categoryInfo = $nonOrderSR->subCategoryObj->getCategoryOnSubCategory($_POST['subcategory']);
            $categoryId = $categoryInfo[0]['cat_id'];

            //calculate due date based on SLA(of cat/subcategory) and override by subcategory
            $SLA = $categoryInfo[0]['SLA_in_sec'];

            try{

                //callback time validate
                if($_POST['callback'] == 'Y'){
                    $callbackTime = strtotime($_POST['callbacktime']);

                    //callback time should not be a past time if is updated to new time
                    if($callbackTime != $NonOrderServiceRequest[0]['callbacktime'] && $callbackTime < time()){
                        throw new ServiceRequestException('NOT_VALID_CALLBACK_TIME');
                    }

                } else {
                    $callbackTime = null;
                }

                //update SR (taken care of 'close' status and changing from 'close' status)
                $flag = $nonOrderSR->updateSR( $NonOrderServiceRequest[0]['SR_id'], $updatedBy=$login, $categoryId,
                                               $_POST['subcategory'], $_POST['priority'], $_POST['status'], $SLA,
                                               $_POST['department'], $_POST['channel'], $callbackTime,
                                               $assignee=null, $copyto=null, $_POST['create_summary'], $_POST['comment'], 
                                               $noteStatus=null);
                if($flag === false){
                    throw new ServiceRequestException('UPDATE_SR_ERROR');
                }

                //update non-order SR on successful SR
                $flag = $nonOrderSR->updateNonOrderSR($nonOrderSRId, $_POST['customer_email'],$_POST['customer_name'],
                                                        $_POST['customer_phone']);
                if($flag === false){
                    throw new ServiceRequestException('UPDATE_NON-ORDER_SR_ERROR');
                }

                //on success of all subtasks
                $smarty->assign("success","Y");

            }catch(ServiceRequestException $SR){
                $smarty->assign("success","N");
                $smarty->assign("message",$SR->getMessage());
            }

        }

        func_display("admin/ServiceRequest/manageNonOrderSR.tpl",$smarty);
        break;


    case 'closeNonOrderServieRequest':        
        $NonOrderServiceRequest = $nonOrderSR->getNonOrderSR(array('id'=>$nonOrderSRId));

        $smarty->assign("form_type",'closeNonOrderServieRequest');
        $smarty->assign("non_order_sr",$NonOrderServiceRequest);
        $smarty->assign("success","N");

        //handle post form submit
        if($SRPostMode == 1){
            $flag=true;//add status

            try{

                //close SR
                $flag = $nonOrderSR->closeSR( $NonOrderServiceRequest[0]['SR_id'],$resolver=$login, $_POST['close_summary'],
                                               $copyto=null, $noteStatus=null, $_POST['MFBSendStatus']);
                if($flag === false){
                    throw new ServiceRequestException('CLOSE_SR_ERROR');
                }

                //on success of all subtasks
                $smarty->assign("success","Y");

            }catch(ServiceRequestException $SR){
                $smarty->assign("success","N");
                $smarty->assign("message",$SR->getMessage());
            }

        }

        func_display("admin/ServiceRequest/manageNonOrderSR.tpl",$smarty);
        break;


    case 'deleteNonOrderServieRequest':        
        $nonOrderServiceRequest = $nonOrderSR->getNonOrderSR(array('id'=>$nonOrderSRId));
        $flag=true;//delete status

        try{

            //check for already deleted
            if($nonOrderServiceRequest[0]['sr_active'] == 'N'){
                throw new ServiceRequestException('DELETED_SR');
            }

            //check for user authenticity and delete SR
            $flag = $nonOrderSR->deleteSR( $nonOrderServiceRequest[0]['SR_id'],$deletedBy=$login);
            if($flag === false){
                throw new ServiceRequestException('DELETE_SR_ERROR');
            }

            //on success of all subtasks
            echo "Service Request is deleted successfully";

        }catch(ServiceRequestException $SR){
            echo $SR->getMessage();
        }

        //to stop output further for ajax response
        exit;
        break;
}
