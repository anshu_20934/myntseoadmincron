<?php
/**
 * bulk SR update controller
 * @author: Arun Kumar K
 * legends:
 *      SR - service request
 *      fSRid - filter by SR_id 
 */
require "../auth.php";
require $xcart_dir."/include/security.php";
include_once $xcart_dir.'/include/func/func.order.php';
include_once($xcart_dir."/modules/serviceRequest/ServiceRequest.php");
include_once($xcart_dir."/modules/serviceRequest/SRNonOrder.php");
include_once($xcart_dir."/modules/serviceRequest/SROrder.php");
include_once($xcart_dir."/PHPExcel/PHPExel.php");

// set max execution time to 1 hour
set_time_limit(3600);

define("UPLOAD_FILE_DIR",\HostConfig::$documentRoot.DIRECTORY_SEPARATOR."admin".DIRECTORY_SEPARATOR."ServiceRequest".DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR);

/**
 * function to show error
 * @param $errMsg - message to show
 * @return void
 */ 
function showError($errMsg){
    global $smarty;
    $smarty->assign("display_loading", "none");
    $smarty->assign("errormessage", $errMsg);
    //echo $errMsg;
    //exit;
}

/*
 * function to log error in an array
 * @param $log - log array reference
 * @param $idx - index for arry
 * @param $SR_or_cell - sr id or cell number of i/p xls/x file
 * @param $reason - reason for error
 */
function logError(&$log, $idx, $SR_or_cell, $reason){
    $log[$idx]["SR_Id_Cell_Name"] = $SR_or_cell;
    $log[$idx]["Reason_for_error"] = $reason;
}

$SR = new ServiceRequest();
$orderSR = new SROrder();
$nonOrderSR = new SRNonOrder();

//authenticate user for upload
$adminFlag = $SR->isValidUserRoleToManage($login);
// to give edit permission to all CC folks
$adminFlag = 1;
//authenticate updated for Customer Support person before processing file
$CCUser = $SR->isValidUserRoleToChangeStatus($login);



if ($REQUEST_METHOD == "POST"){

    if($adminFlag && $CCUser) {
        if(!isset($_FILES) && isset($HTTP_POST_FILES)){
            $_FILES = $HTTP_POST_FILES;
        }

        // allow only xls/xlsx file types
        $allowedMimeType = array("application/octet-stream", "application/vnd.ms-excel");
        // 2 MB file size at max
        $maxFileSize = 2097152;

        if(!empty($_FILES['updateSRFile']['name'])){
            if ($_FILES["updateSRFile"]["error"] == UPLOAD_ERR_OK) {
                if(in_array($_FILES["updateSRFile"]["type"], $allowedMimeType) && $_FILES["contactus_file"]["size"] <= $maxFileSize){

                    $tmp_name = $_FILES["updateSRFile"]["tmp_name"];
                    $name = $_FILES["updateSRFile"]["name"];
                    // to avoid confliction of same names on concurrent upload
                    $baseName = time()."_".basename($name);

                    $fileExtension = explode(".",$_FILES['updateSRFile']['name']);
                    $fileExtension = $fileExtension[count($fileExtension)-1];

                    if(strtolower($fileExtension) == 'xls' || strtolower($fileExtension) == 'xlsx' ){

                        //create file upload directory
                        if(!file_exists(UPLOAD_FILE_DIR)){
                            mkdir(UPLOAD_FILE_DIR,0777) ;
                        }
                        
                        if(move_uploaded_file($tmp_name, UPLOAD_FILE_DIR.$baseName)){                                                        

                            // get the appripriate phpExcel object based on xls/x
                            if($fileExtension == 'xlsx') {
                                $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                            } else {
                                $objReader = PHPExcel_IOFactory::createReader('Excel5');
                            }

                            $objPHPExcel = $objReader->load(UPLOAD_FILE_DIR.$baseName);
                            $lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();

                            if($lastRow < 2){
                                showError("File is empty or does not have any data other than column names.");
                            }

                            // log error in to the array
                            $errorSRLog = array();
                            logError($errorSRLog, 0, '', '');

                            // successful update counter
                            $successCount = 0;
                            
                            //start from second row ignore heading
                            for($i=2;$i<=$lastRow;$i++){

                                // default values for SR components to update
                                $catId = null;
                                $subCatId = null;
                                $sla = null;
                                $priorityId = null;
                                $statusId = null;
                                $department = null;
                                $channel = null;

                                // sr id
                                $cell1Obj = ($objPHPExcel->getActiveSheet()->getCell('A'.$i));
                                $SRId = trim($cell1Obj->getvalue());//returns the value in the cell
                                if(empty($SRId)){
                                    logError($errorSRLog, $i, 'A'.$i, "SRId column is blank");
                                    continue;
                                }

                                // get SR detail and check for SR existence
                                $SRDetail = $SR->getSR(array("fSRid"=>$SRId));
                                if(empty($SRDetail[0]['SR_id'])){
                                    logError($errorSRLog, $i, $SRId, "SRId does not exist");
                                    continue;
                                }
                                $SRId = $SRDetail[0]['SR_id'];

                                // get the create summary since is a stateful field for sr update
                                // and is mandatorily required for an update to be stateful
                                $createSummary = $SRDetail[0]['create_summary'];

                                // get order sr id, non order sr id
                                $orderSRId = $SRDetail[0]['order_sr_id'];
                                $nonOrderSRId = $SRDetail[0]['id'];

                                // cat/sub cat
                                $cell2Obj = ($objPHPExcel->getActiveSheet()->getCell('B'.$i));
                                $catName = trim($cell2Obj->getvalue());

                                $cell3Obj = ($objPHPExcel->getActiveSheet()->getCell('C'.$i));
                                $subCatName = trim($cell3Obj->getvalue());

                                // any one among cat or subcat name do not update cat, subcat and sla
                                if(!empty($catName) && !empty($subCatName)){
                                    $catDetail = $SR->subCategoryObj->getCatSubCategoryOnName($subCatName,$catName, $isActive=1);
                                    if(empty($catDetail[0]['sub_cat_id'])){
                                        logError($errorSRLog, $i, $SRId, "Combination of the category and subcategory does not exist or is in-active");
                                        continue;
                                    }
                                    $catId = $catDetail[0]['cat_id'];
                                    $subCatId = $catDetail[0]['sub_cat_id'];
                                    $sla = $catDetail[0]['SLA_in_sec'];
                                } else {
                                    // assign new values or retain old to show in stateful summary(create sumary)
                                    $catId = $SRDetail[0]['category_id'];
                                    $subCatId = $SRDetail[0]['sub_category_id'];
                                }

                                //priority
                                $cell4Obj = ($objPHPExcel->getActiveSheet()->getCell('D'.$i));
                                $priorityName = trim($cell4Obj->getvalue());
                                if(!empty($priorityName)){
                                    $priorityDetail = $SR->priorityObj->getSRPriority(array("SR_priority"=>$priorityName,"priority_active"=>"Y"));
                                    if(empty($priorityDetail[0]['id'])){
                                        logError($errorSRLog, $i, $SRId, "Priority does not exist or is in-active");
                                        continue;
                                    }
                                    $priorityId = $priorityDetail[0]['id'];
                                } else {
                                    $priorityId = $SRDetail[0]['priority_id'];
                                }

                                // status
                                $cell5Obj = ($objPHPExcel->getActiveSheet()->getCell('E'.$i));
                                $statusName = trim($cell5Obj->getvalue());
                                if(!empty($statusName)){
                                    $statusDetail = $SR->statusObj->getSRStatus(array("SR_status"=>$statusName,"status_active"=>"Y"));
                                    if(empty($statusDetail[0]['id'])){
                                        logError($errorSRLog, $i, $SRId, "Status does not exist or is in-active");
                                        continue;
                                    }
                                    $statusId = $statusDetail[0]['id'];
                                } else {
                                    $statusId = $SRDetail[0]['status_id'];
                                }

                                // department
                                $cell6Obj = ($objPHPExcel->getActiveSheet()->getCell('F'.$i));
                                $department = trim($cell6Obj->getvalue());
                                if(!empty($department)){
                                    // department does not exist in the list
                                    $isExistsDepartment = false;
                                    $departmentDetail = $SR->getSRDepartment();
                                    foreach($departmentDetail as $dept){
                                        if(strtolower($department) == strtolower($dept)){
                                            // take the department name exactly as in the array
                                            // so that data can not be corrupted for department names
                                            $department = $dept;
                                            $isExistsDepartment = true;
                                        }
                                    }
                                    if(!$isExistsDepartment){
                                        logError($errorSRLog, $i, $SRId, "Department does not exist");
                                        continue;
                                    }
                                } else {
                                    $department = $SRDetail[0]['SR_department'];
                                }

                                // channel
                                $cell7Obj = ($objPHPExcel->getActiveSheet()->getCell('G'.$i));
                                $channel = trim($cell7Obj->getvalue());
                                if(!empty($channel)){
                                    // channel does not exist in the list
                                    $isExistsChannel = false;
                                    $channelDetail = $SR->getSRChannel();
                                    foreach($channelDetail as $chan){
                                        if(strtolower($channel) == strtolower($chan)){
                                            // take the channel name exactly as in the array
                                            // so that data can not be corrupted for channelnames
                                            $channel = $chan;
                                            $isExistsChannel = true;
                                        }
                                    }
                                    if(!$isExistsChannel){
                                        logError($errorSRLog, $i, $SRId, "Channel does not exist");
                                        continue;
                                    }
                                } else {
                                    $channel = $SRDetail[0]['SR_channel'];
                                }

                                //callback time
                                try{
                                    $cell8Obj = ($objPHPExcel->getActiveSheet()->getCell('H'.$i));
                                    $callbackTime = trim($cell8Obj->getvalue());
                                    if(!empty($callbackTime)){
                                        $callbackTime = strtotime($callbackTime);

                                        /* getting callbacktime from date because
                                         * from_unixtime() in mysql and date() in php gives different result
                                         * for same timestamp.                                         
                                         */
                                        $SRCallBackTime = strtotime($SRDetail[0]['callbacktime_date']);
                                        
                                        //callback time should not be a past time if is updated to new time
                                        if($callbackTime != $SRCallBackTime && $callbackTime < time()){                                            
                                            throw new ServiceRequestException('NOT_VALID_CALLBACK_TIME');
                                        }

                                        /* restore the timestamp stored in DB again in case there is no change in
                                         * callback time because $date passed in strtotime($date) is 1 sec lesser than
                                         * the actual timestamp stored in DB.
                                         * When we fetch this timestamp from DB using from_unixtime(), it gives the
                                         * date which is 1 sec lesser                                         *
                                         */
                                        if($callbackTime == $SRCallBackTime){
                                            $callbackTime = $SRDetail[0]['callbacktime'];                                            
                                        }
                                    }
                                } catch(ServiceRequestException $SRException){
                                    // on exception capture SRid into a file for later verification
                                    logError($errorSRLog, $i, $SRId, $SRException->getMessage());
                                    continue;
                                }

                                // comment
                                $cell9Obj = ($objPHPExcel->getActiveSheet()->getCell('I'.$i));
                                $comment = trim($cell9Obj->getvalue());

                                // email
                                $cell10Obj = ($objPHPExcel->getActiveSheet()->getCell('J'.$i));
                                $email = trim($cell10Obj->getvalue());
                                if(empty($email)){
                                    $email = ($SRDetail[0]['order_customer_email'])? $SRDetail[0]['order_customer_email'] : $SRDetail[0]['customer_email'] ;
                                }

                                // name
                                $cell11Obj = ($objPHPExcel->getActiveSheet()->getCell('K'.$i));
                                $name = trim($cell11Obj->getvalue());
                                if(empty($name)){
                                    $name = $SRDetail[0]['first_name'];
                                }

                                // mobile
                                $cell12Obj = ($objPHPExcel->getActiveSheet()->getCell('L'.$i));
                                $mobile = trim($cell12Obj->getvalue());
                                if(empty($mobile)){
                                    $mobile = $SRDetail[0]['mobile'];
                                }

                                // check order or non order sr
                                if(empty($nonOrderSRId)){
                                    // order sr
                                    try{
                                        //check for validity of order SR
                                        $flag = $orderSR->isValidOrderSR($SRDetail[0]['order_id'],$catId,$subCatId,$orderSRId);
                                        if($flag === false){
                                            throw new ServiceRequestException('NOT_VALID_ORDER_SR');
                                        }

                                        // update SR
                                        $flag = $orderSR->updateSR( $SRId, $updatedBy=$login, $catId,
                                                                    $subCatId, $priorityId, $statusId, $sla,
                                                                    $department, $channel, $callbackTime,
                                                                    $assignee=null, $copyto=null, $createSummary, $comment,
                                                                    $noteStatus=null);
                                        if($flag === false){
                                            throw new ServiceRequestException('UPDATE_SR_ERROR');
                                        }

                                    } catch(ServiceRequestException $SRException){                                        
                                        logError($errorSRLog, $i, $SRId, $SRException->getMessage());
                                        continue;

                                    }

                                } else {
                                    // nonorder sr
                                    try{
                                        // update Sr
                                        $flag = $nonOrderSR->updateSR( $SRId, $updatedBy=$login, $catId,
                                                                        $subCatId, $priorityId, $statusId, $sla,
                                                                        $department, $channel, $callbackTime,
                                                                        $assignee=null, $copyto=null, $createSummary, $comment,
                                                                        $noteStatus=null);
                                        if($flag === false){
                                            throw new ServiceRequestException('UPDATE_SR_ERROR');
                                        }

                                        //update non-order SR on successful SR
                                        $flag = $nonOrderSR->updateNonOrderSR($nonOrderSRId, $email, $name, $mobile);
                                        if($flag === false){
                                            throw new ServiceRequestException('UPDATE_NON-ORDER_SR_ERROR');
                                        }

                                    } catch(ServiceRequestException $SRException){
                                        logError($errorSRLog, $i, $SRId, $SRException->getMessage());
                                        continue;
                                    }
                                }
                                $successCount++;
                            }

                            // unlink the uploaded file to free space on disk
                            unlink(UPLOAD_FILE_DIR.$baseName);

                            // write errors into log file and download
                            if(count($errorSRLog) > 0){

                                // script run status for successful updation and error
                                logError($errorSRLog, $i++, "Success", $successCount);
                                logError($errorSRLog, $i++, "Error", ($lastRow-1)-$successCount);


                                $objPHPWriteExcel = func_create_results_file($errorSRLog, "error_service_requests");
                                $fileName = "service_requests_error_".date("dmy");

                                header("Content-Disposition: attachment; filename=$fileName.xls");
                                ob_clean();
                                flush();
                                try {
                                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPWriteExcel, 'Excel5');
                                    $objWriter->setPreCalculateFormulas(false);
                                    $objWriter->save('php://output');
                                    
                                }catch(Exception $ex) {
                                    showError("Error PHP Excel : ". $ex->getMessage());
                                }
                            }
                            //sleep(10);
                        }// upload
                        else {
                            showError("Problem in uploading the file, please try later");
                        }
                    }// xls/xlsx
                    else{
                        showError("Either type or size of file in invalid");
                    }

                } else {
                    showError("Either type or size of file in invalid");
                }
            } else {
                showError("Problem in uploading the file, please try later");
            }
        } else {
            showError("File is not uploaded");
        }
        
    // unauthorised user
    } else {
        showError("You are not permitted to upload the file.");
    }

}

$smarty->assign("display_loading", "none");
$smarty->assign("main", "bulkSRUpdate");
func_display("admin/home.tpl", $smarty);
?>