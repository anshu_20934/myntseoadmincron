<?php
/**
 * generates reports for service requests
 * @author: Arun Kumar K
 * legends:
 *      SR - service request
 */
require "../auth.php";
require $xcart_dir."/include/security.php";
include_once($xcart_dir."/modules/serviceRequest/SRNonOrder.php");
include_once($xcart_dir."/modules/serviceRequest/SROrder.php");

$generalFilter = (!empty($_GET['gfilter']))?$_GET['gfilter']:$_POST['gfilter'];
$from = (!empty($_GET['fStartCrDate']))?$_GET['fStartCrDate']:$_POST['fStartCrDate'];
$end = (!empty($_GET['fEndCrDate']))?$_GET['fEndCrDate']:$_POST['fEndCrDate'];
$mode = (!empty($_GET['mode']))?$_GET['mode']:$_POST['mode'];

$queryString = http_build_query($_GET);

//initialize SR
$SR = new ServiceRequest();

if(!empty($generalFilter)){
    $fromTime = time()-($generalFilter*86400);//last many days
    $fromDate = $SR->getDateToTime(date('m/d/Y',$fromTime),'start');

    $endDate = $SR->getDateToTime(date('m/d/Y',time()),'end');

    $filter = array('fStartCrDate'=>$fromDate);
    $filter += array('fEndCrDate'=>$endDate);

} else if(!empty($from) || !empty($end)) {
    if(!empty($from)){        
        $fromDate = $SR->getDateToTime($from,'start');
        
    } else {
        $fromDate = null;
    }

    if(!empty($end)){
        $endDate = $SR->getDateToTime($end,'end');
    } else {
        $endDate = null;
    }

    $filter = array(
        'fStartCrDate'=>$fromDate,
        'fEndCrDate'=>$endDate
    );

} else {
    $filter=array();    
}

if(!empty($filter)){
    
    //all Sr
    $countSR = $SR->getSRCount($filter);
    //get status for 'close'
    $status = $SR->statusObj->getSRStatusByName('close');
    $countClosedSR = $SR->getSRCount($filter + array('fStat'=>$status[0]['id']));
    $closeSRPercent = ($countClosedSR/$countSR)*100;

    //order SR
    $orderSR = new SROrder();
    $countOrderSR = $orderSR->getOrderSRCount($filter);
    $countClosedOrderSR = $orderSR->getOrderSRCount($filter + array('fStat'=>$status[0]['id']));
    $closeOrderSRPercent = ($countClosedOrderSR/$countOrderSR)*100;
    $countOrderSRWithComp = $orderSR->getOrderSRCount($filter + array('fSRCompType'=>'Y'));
    $countOrderSRWithCompPercent = ($countOrderSRWithComp/$countClosedOrderSR)*100;

    //count number of orders in orderSR
    $orderSql = "select
                count(orderid) as order_count
            from
                xcart_orders
            where
                status not in ('PP','PV','D')
            and
                1";
    if (!empty($filter['fStartCrDate'])) {
        $createTime = $filter['fStartCrDate'];
        $orderSql .= " AND date >= '".sanitizeDBValues($createTime)."'";
    }
    if (!empty($filter['fEndCrDate'])) {
        $endCreateTime = $filter['fEndCrDate'];
        $orderSql .= " AND date <= '".sanitizeDBValues($endCreateTime)."'";
    }
    $countOrder = func_query_first_cell($orderSql, true);
    if(empty($countOrder)){
        $countOrder = 1;//to avoid divide / 0
    }
    $complaintRatioPercent = ($countOrderSR/$countOrder)*100;
    

    //calculate compensation amount for order SR
    $compAmountCredit = 0;
    $compAmountDebit = 0;
    $allOrderSR = $orderSR->getOrderSR($filter + array('fSRCompType'=>'Y'));
    foreach($allOrderSR as $idx=>$osr){
        $compTypes = explode(',',$osr['compensation_type']);

        //check for coupon
        if($compTypes == 'coupon'){
            $couponExists = true;
        } else if(in_array('coupon',$compTypes)){
            $couponExists = true;
        }

        //check for cashback
        if($compTypes == 'cashback'){
            $cashBackExists = true;
        } else if(in_array('cashback',$compTypes)){
            $cashBackExists = true;
        }

        //calculate comp from caoupon and cashback
        if($couponExists === true || $cashBackExists === true){
            $compValJsonDecode = json_decode($osr['compensation_value']);

            foreach($compValJsonDecode as $idx=>$compTypeDetail){
                //get coupon amount when coupon is of absolute
                if($compTypeDetail->type == 'coupon'){
                    if($compTypeDetail->detail->coupon-type == 'percentage'){
                        $compAmountCredit += $compTypeDetail->detail->absolute-discount;
                    }
                }

                //get cashback amount added when 'credit' and deducted when 'debit'
                if($compTypeDetail->type == 'cashback'){
                    //given to customer
                    if($compTypeDetail->detail->transaction == 'credit'){
                        $compAmountCredit += $compTypeDetail->detail->amount;

                    //taken from customer as freight
                    } else {
                        $compAmountDebit += $compTypeDetail->detail->amount;
                    }
                }
            }
        }
    }


    //non-order SR
    $nonOrderSR = new SRNonOrder();
    $countNonOrderSR = $nonOrderSR->getNonOrderSRCount($filter);
    $countClosedNonOrderSR = $nonOrderSR->getNonOrderSRCount($filter + array('fStat'=>$status[0]['id']));
    $closeNonOrderSRPercent = ($countClosedNonOrderSR/$countNonOrderSR)*100;   
}

$generalFilter = array(
    0=>array('num'=>1,'name'=>'Last 1 Day'),
    1=>array('num'=>7,'name'=>'Last 7 Days'),
    2=>array('num'=>30,'name'=>'Last 30 Days'),
);

$smarty->assign("main","sr_report");
$smarty->assign("generalFilter",$generalFilter);
$smarty->assign("query_string",$queryString);

//SR
$smarty->assign("countSR",$countSR);
$smarty->assign("countClosedSR",$countClosedSR);
$smarty->assign("closeSRPercent",number_format($closeSRPercent,2));

//order SR
$smarty->assign("countOrderSR",$countOrderSR);
$smarty->assign("countClosedOrderSR",$countClosedOrderSR);
$smarty->assign("closeOrderSRPercent",number_format($closeOrderSRPercent,2));
$smarty->assign("countOrderSRWithComp",$countOrderSRWithComp);
$smarty->assign("countOrderSRWithCompPercent",number_format($countOrderSRWithCompPercent,2));
$smarty->assign("compAmountCredit",number_format($compAmountCredit,2));//given to customer
$smarty->assign("compAmountDebit",number_format($compAmountDebit,2));//taken to customer
$smarty->assign("compAmount",number_format($compAmountCredit-$compAmountDebit,2));

//complaint ratio
$smarty->assign("countOrder",$countOrder);
$smarty->assign("complaintRatioPercent",number_format($complaintRatioPercent,2));

//non-order SR
$smarty->assign("countNonOrderSR",$countNonOrderSR);
$smarty->assign("countClosedNonOrderSR",$countClosedNonOrderSR);
$smarty->assign("closeNonOrderSRPercent",number_format($closeNonOrderSRPercent,2));

switch($mode){
    case 'exportSRReport':
        $fileName = "service_requests_report_".date("dmy");
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$fileName.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        $HTML = func_display("admin/ServiceRequest/service_request_report_export.tpl", $smarty, false);        
        echo $HTML;
        exit;
        break;
}

func_display("admin/home.tpl",$smarty);