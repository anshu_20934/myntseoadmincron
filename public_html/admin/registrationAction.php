<?php

require_once "./auth.php";
require_once $xcart_dir."/include/security.php";

function checkRegistrationClass($var){
	//echo "$var <br />";
	return preg_match("/Registration/", $var);
	
}

function showOptionsDrop($array){
	$string = '';
	foreach($array as $k => $v){
		$string .= '<option value="'.$k.'"'.$s.'>'.$v.'</option>'."\n";
	}
	return $string;
}


function loadFiles(){
	global $xcart_dir;
	$directory = $xcart_dir."/modules/Registration/";	
	
    $handler = opendir($directory);
	
    while ($file = readdir($handler)) {
      if ($file != "." && $file != ".." && $file != "apidocs") {
      		if (!is_dir($file)) {
				//echo $directory.$file."<br/>";			
				require_once $directory.$file;
			}
      }

    }

    closedir($handler);
}

$error =false;
if ($REQUEST_METHOD == "POST"){
	print_r($HTTP_POST_VARS); 	
	$reg_class_values=array();
	$reg_class_values['registration_class'] = $class_name;
	$reg_class_values['channel'] = $channel_code;
	if(empty($channel_code)){
		if(!$error) {
			$error =true;
			$err_message = "Channel Code cannot be blank";
		}
	}
	
	if(!$error) {
		$old_channel_code = func_query_first_cell("SELECT channel FROM mk_registration_class WHERE registration_class='$class_name'");
		execute_sql("delete FROM mk_registration_class WHERE registration_class='$class_name'");
		execute_sql("delete FROM mk_registration_actions WHERE channel='$old_channel_code'");
		execute_sql("delete FROM mk_registration_emails WHERE channel = '$old_channel_code' AND start_date < $current_time AND end_date > $current_time");			
		$reg_class = func_query_first_cell("SELECT registration_class FROM mk_registration_class WHERE channel='$channel_code'");
		if($reg_class['registration_class']){			
			$error = true;
			$err_message = "Duplicate Entry with the same channel code";
		} else {
			func_array2insert('mk_registration_class',$reg_class_values);
		}	
	}		
	
	if(!$error){		
		foreach ($HTTP_POST_VARS as $key => $value) {
			if($key != "current_time" && $key != "class_name" && $key != "channel_code" && !preg_match("/from-/", $key) && !preg_match("/to-/", $key) && !preg_match("/email_tempelate-/", $key)) {
				if(!empty($value)){
					if(is_numeric($value)){
						$exceution_values = array();
						$exceution_values['channel'] = $channel_code;
						$exceution_values['execution_order'] = $value;
						$exceution_values['action'] = $key;
						func_array2insert('mk_registration_actions',$exceution_values);
					} else {
						$error = true;
						$err_message = $err_message." One or more exceution order values are not integers.";
					}
				}
			} elseif(preg_match("/email_tempelate-/", $key)){
				if(!empty($value)){
					$pos = strpos($key, '-');
					$remaining = substr($key, $pos+1);
					$from = $HTTP_POST_VARS["from-".$remaining];
					$to = $HTTP_POST_VARS["to-".$remaining];
					if(!empty($from)&&!empty($to)){
						$email_template_values = array();
						$email_template_values['channel'] = $channel_code; 
						$email_template_values['start_date'] = DateTime::createFromFormat('m/d/Y', $from)->getTimestamp();
						$email_template_values['end_date'] = DateTime::createFromFormat('m/d/Y', $to)->getTimestamp();
						$email_template_values['registration_email_template'] = $value;
						if($email_template_values['end_date'] >= $email_template_values['start_date'] ) {
							func_array2insert('mk_registration_emails',$email_template_values);
						}							
						else {
							$error = true;
							$err_message = $err_message." End date should be greater or equal to start date.";
						}
						//print_r($email_template_values);
					}
				}
			}
		}
	}
	
	if(!$error) {
		$err_message = "Registration actions saved";
	}
	
	func_header_location("/admin/registrationAction.php?message=$err_message", true);
}

$smarty->assign('message', $_GET['message']);

loadFiles();
$registration_classes = array_filter(get_declared_classes(),"checkRegistrationClass");
//print_r($registration_classes);
$class_methods = array();
$action_array = array();
$template_array = array();
$channel = array();
$currentTime = time();



foreach ($registration_classes as $classes) {
	$class_methods[$classes] = get_class_methods($classes);
	//sort($class_methods[$classes]);
	$registrationClassQuery = "SELECT channel FROM mk_registration_class where registration_class='$classes'";
	$class_channel = func_query_first($registrationClassQuery);
	//print_r($class_channel);
	$channel[$classes] =$class_channel['channel']; 
	if(!empty($channel[$classes])){
		$actionItemsQuery = "SELECT action,execution_order FROM mk_registration_actions WHERE channel = '$channel[$classes]' order by action";
		$actionItemResults = func_query($actionItemsQuery);		 
		$mailQuery = "select registration_email_template, start_date, end_date from mk_registration_emails where channel = '". $channel[$classes] ."' and start_date < $currentTime and end_date > $currentTime order by id desc";
		$temp = func_query($mailQuery);
		$new = array();
		
		foreach($temp as $t) {
			$t['start_date'] = date("m/d/Y",$t['start_date']);
			$t['end_date'] = date("m/d/Y",$t['end_date']);
			$new[] = $t;
		}
				
		$template_array[$classes] = $new;
				
		foreach ($actionItemResults as $results) {
			$action_array[$classes][$results["action"]] = $results["execution_order"];
		}
	}	
}

$emailTemplatesQuery = "SELECT name from mk_email_notification where name like '%regis%'";
$emailTemplates = func_query($emailTemplatesQuery);
foreach ($emailTemplates as $template){
	$emailTemplate[] = $template['name'];
}

$smarty->assign('emailTemplates', $emailTemplate);
$smarty->assign('current_time', $currentTime);
$smarty->assign('reg_classes', $registration_classes);
$smarty->assign('class_methods', $class_methods);
$smarty->assign('action_array', $action_array);
$smarty->assign('class_channel', $channel);
$smarty->assign('mail_template',$template_array);
func_display('admin/registrationAction.tpl',$smarty);
?>