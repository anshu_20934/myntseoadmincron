<?php
/**
 * controller file to view all contactus issues with their fields
 * @author: Arun Kumar K  
 */
require_once "../auth.php";
require_once $xcart_dir."/include/security.php";

use contactus\MContactUsIssueManager;

$mContactUsIssueManager = new MContactUsIssueManager();

// get all issues with their fields
$issueDetail = $mContactUsIssueManager->getContactUsIssueWithField();
$issueDetail = $issueDetail['issueArray'];

$smarty->assign("main","contactus_controller");
$smarty->assign("issue_detail",$issueDetail);

func_display("admin/home.tpl",$smarty);