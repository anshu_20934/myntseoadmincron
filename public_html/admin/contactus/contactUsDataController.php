<?php
/**
 * controller file to manage contact-us issues(add,edit)
 * @author: Arun Kumar K
 */
require_once "../auth.php";
require_once \HostConfig::$documentRoot."/include/security.php";

use contactus\MContactUsIssueManager;
use contactus\dao\MContactUsDAO;
use contactus\MContactUsException;

$mode = (!empty($_GET['mode']))?$_GET['mode']:$_POST['mode'];
$issueId = (!empty($_GET['contactus_issue_id']))?$_GET['contactus_issue_id']:$_POST['contactus_issue_id'];
$postMode = (!empty($_GET['post_mode']))?$_GET['post_mode']:$_POST['post_mode'];

// filter params
$mode = filter_var($mode, FILTER_SANITIZE_STRING);
$issueId = filter_var($issueId, FILTER_SANITIZE_NUMBER_INT);
$postMode = filter_var($postMode, FILTER_SANITIZE_NUMBER_INT);

$mContactUsIssueManager = new MContactUsIssueManager();
$mContactUsDAO = new MContactUsDAO();

try{
    if($mode == 'addContactUsIssue'){

        // get issue levels(1 and 2)
        $issueLevel = $mContactUsDAO->getContactUsIssueLevel($active=false);
        $issueLevel_1 = $issueLevel["level_1"];
        $issueLevel_2 = $issueLevel["level_2"];

        $smarty->assign("issue_level_1",$issueLevel_1);
        $smarty->assign("issue_level_2",$issueLevel_2);

        $smarty->assign("form_type",'addContactUsIssue');
        $smarty->assign("success","N");

        //handle post form submit
        if($postMode == 1){

            //echo "<pre>";print_r($_POST);exit;

            // get issue detail from user (sanitized)
            $filterOption = array(
                "issue_level_1" => FILTER_SANITIZE_STRING,
                "issue_level_2" => FILTER_SANITIZE_STRING,
                "add_issue" => FILTER_SANITIZE_STRING,
                "support_email" => FILTER_SANITIZE_STRING,
                "issue_action" => FILTER_SANITIZE_STRING,
                "is_active_issue" => FILTER_SANITIZE_NUMBER_INT,

            );

            $inputParams = filter_input_array(INPUT_POST, $filterOption);

            $issueLevel_1 = $inputParams['issue_level_1'];
            $issueLevel_2 = $inputParams['issue_level_2'];
            $newIssue = $inputParams['add_issue'];
            $supportEmail = $inputParams['support_email'];
            $issueAction = $inputParams['issue_action'];
            $isActiveIssue = $inputParams['is_active_issue'];

            // get issue field details from user(sanitized)
            $fieldName = filter_input(INPUT_POST, "field_name", FILTER_SANITIZE_STRING, array("flags"=>FILTER_REQUIRE_ARRAY));
            $fieldType = filter_input(INPUT_POST, "field_type", FILTER_SANITIZE_STRING, array("flags"=>FILTER_REQUIRE_ARRAY));
            $maxchar = filter_input(INPUT_POST, "maxchar", FILTER_SANITIZE_NUMBER_INT, array("flags"=>FILTER_REQUIRE_ARRAY));
            $fieldAction = filter_input(INPUT_POST, "field_action", FILTER_SANITIZE_STRING, array("flags"=>FILTER_REQUIRE_ARRAY));
            $isActiveField = filter_input(INPUT_POST, "is_active_field", FILTER_SANITIZE_NUMBER_INT, array("flags"=>FILTER_REQUIRE_ARRAY));            

            try{

                $insertIssueId = $mContactUsIssueManager->addContactUsIssue($newIssue, $issueLevel_1, $issueLevel_2, $supportEmail, $issueAction, $isActiveIssue,
                                      $fieldName, $fieldType, $maxchar, $fieldAction, $isActiveField);

                //on success of all subtasks
                $smarty->assign("success","Y");

            } catch(MContactUsException $e){
                $smarty->assign("success","N");
                $smarty->assign("message",$e->getMessage());
            }
        }

        func_display("admin/contactus/contactUsDataController.tpl",$smarty);

    }
    
} catch(MContactUsException $e){

    $smarty->assign("success","N");
    $smarty->assign("message",$e->getMessage());
    
    func_display("admin/contactus/contactUsDataController.tpl",$smarty);
}

try{
    if($mode == 'editContactUsIssue'){

        // get issue detial        
        $issueDetail = $mContactUsIssueManager->getContactUsIssueWithField($issueId, $active=false);
        $issueDetail = $issueDetail['issueArray'];

        try{
            // get the level from the DB to show in edit form to edit
            $issueLevel = $mContactUsIssueManager->getIssueLevelFromDB($issueDetail[0]['issue_level_1'], $issueDetail[0]['issue_level_2'],
                                                $issueDetail[0]['issue_level_3']);

        } catch(MContactUsException $e){
            $smarty->assign("message",$e->getMessage());
        }

        $smarty->assign("issue_detail",$issueDetail[0]);
        $smarty->assign("issue_field_detail",$issueDetail[0]['field']);
        $smarty->assign("contactus_issue_id",$issueId);
        $smarty->assign("issue_level",$issueLevel);

        $smarty->assign("form_type",'editContactUsIssue');
        $smarty->assign("success","N");

        //handle post form submit
        if($postMode == 1){

            //echo "<pre>";print_r($_POST);exit;

            // get issue detail from user (sanitized)
            $filterOption = array(
                "edit_issue" => FILTER_SANITIZE_STRING,
                "support_email" => FILTER_SANITIZE_STRING,
                "issue_action" => FILTER_SANITIZE_STRING,
                "is_active_issue" => FILTER_SANITIZE_NUMBER_INT,

            );

            $inputParams = filter_input_array(INPUT_POST, $filterOption);

            $editIssue = $inputParams['edit_issue'];
            $supportEmail = $inputParams['support_email'];
            $issueAction = $inputParams['issue_action'];            
            $isActiveIssue = $inputParams['is_active_issue'];

            // get issue field details from user(editing existing one)
            $fieldIdEdit = filter_input(INPUT_POST, "field_id_edit", FILTER_SANITIZE_NUMBER_INT, array("flags"=>FILTER_REQUIRE_ARRAY));
            $fieldNameEdit = filter_input(INPUT_POST, "field_name_edit", FILTER_SANITIZE_STRING, array("flags"=>FILTER_REQUIRE_ARRAY));
            $fieldTypeEdit = filter_input(INPUT_POST, "field_type_edit", FILTER_SANITIZE_STRING, array("flags"=>FILTER_REQUIRE_ARRAY));
            $maxcharEdit = filter_input(INPUT_POST, "maxchar_edit", FILTER_SANITIZE_NUMBER_INT, array("flags"=>FILTER_REQUIRE_ARRAY));
            $fieldActionEdit = filter_input(INPUT_POST, "field_action_edit", FILTER_SANITIZE_STRING, array("flags"=>FILTER_REQUIRE_ARRAY));
            $isActiveFieldEdit = filter_input(INPUT_POST, "is_active_field_edit", FILTER_SANITIZE_NUMBER_INT, array("flags"=>FILTER_REQUIRE_ARRAY));

            // get issue field details from user to newly add
            $fieldName = filter_input(INPUT_POST, "field_name", FILTER_SANITIZE_STRING, array("flags"=>FILTER_REQUIRE_ARRAY));
            $fieldType = filter_input(INPUT_POST, "field_type", FILTER_SANITIZE_STRING, array("flags"=>FILTER_REQUIRE_ARRAY));
            $maxchar = filter_input(INPUT_POST, "maxchar", FILTER_SANITIZE_NUMBER_INT, array("flags"=>FILTER_REQUIRE_ARRAY));
            $fieldAction = filter_input(INPUT_POST, "field_action", FILTER_SANITIZE_STRING, array("flags"=>FILTER_REQUIRE_ARRAY));
            $isActiveField = filter_input(INPUT_POST, "is_active_field", FILTER_SANITIZE_NUMBER_INT, array("flags"=>FILTER_REQUIRE_ARRAY));            

            try{                

                $mContactUsIssueManager->editContactUsIssue($issueId, $editIssue, $supportEmail, $issueAction, $isActiveIssue,
                                    $fieldIdEdit, $fieldNameEdit, $fieldTypeEdit, $maxcharEdit, $fieldActionEdit, $isActiveFieldEdit,
                                    $fieldName, $fieldType, $maxchar, $fieldAction, $isActiveField);

                //on success of all subtasks
                $smarty->assign("success","Y");

            } catch(MContactUsException $e){
                $smarty->assign("success","N");
                $smarty->assign("message",$e->getMessage());
            }

        }

        func_display("admin/contactus/contactUsDataController.tpl",$smarty);

    }
    
} catch(MContactUsException $e){
    
    $smarty->assign("success","N");
    $smarty->assign("message",$e->getMessage());

    $smarty->assign("issue_detail",$issueDetail[0]);
    $smarty->assign("issue_field_detail",$issueDetail[0]['field']);
    $smarty->assign("contactus_issue_id",$issueId);
    $smarty->assign("issue_level",$issueLevel);

    $smarty->assign("form_type",'editContactUsIssue');
    $smarty->assign("success","N");

    func_display("admin/contactus/contactUsDataController.tpl",$smarty);
}

