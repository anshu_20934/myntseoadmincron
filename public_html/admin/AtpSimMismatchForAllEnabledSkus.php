<?php
//include_once("AMQPMessageProducer.php");
include_once("auth.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/modules/amqpclients/producer/AMQPMessageProducer.php");
require_once "WMSDBConnUtil.php";


$client = AMQPMessageProducer::getInstance();
// todo change prod to seller dbs 
$imsConn = WMSDBConnUtil::getConnection('imsdb1.myntra.com:3306', TRUE, WMSDBConnUtil::IMS_READ);
//$imsConn = WMSDBConnUtil::getConnection('mdb.scmqa.myntra.com:3307', TRUE, WMSDBConnUtil::IMS_READ);
$atpConn = WMSDBConnUtil::getConnection('atpdbmaster.myntra.com:3306', TRUE, WMSDBConnUtil::IMS_WRITE);
//$atpConn = WMSDBConnUtil::getConnection('mdb.scmqa.myntra.com:3307', TRUE, WMSDBConnUtil::IMS_READ);
$simConn = WMSDBConnUtil::getConnection("sellerdb2.myntra.com:3307", TRUE, WMSDBConnUtil::IMS_READ);
//$simConn = WMSDBConnUtil::getConnection("mdb.scmqa.myntra.com:3307", TRUE, WMSDBConnUtil::IMS_READ);

//$to = "engg_erp_wms@myntra.com";
$to = "kondapaka.prashanth@myntra.com";
$fromHeader = "From: inventorysync@myntra.com";
$subject = "[WMSALERT]ERP Inventory:  SIM and ATP Seller MisMatch Report";


if (!$imsConn  || !$atpConn  || !$simConn) {
    die('Could not connect: ' . mysql_error());
}

if (mysql_select_db('myntra_atp', $atpConn) === false) {
    echo "ATP db unaccessible \n";
    exit;
}

if (mysql_select_db('myntra_ims', $imsConn) === false) {
    echo "WMS db unaccessible \n";
    exit;
}

if (mysql_select_db('myntra_seller', $simConn) === false) {
    echo "SIM db unaccessible \n";
    exit;
}

$finalSimAtpMismatch = array();
$finalAllDisabledSkusWithInv = array();
$invDicrepancyThreshold = 100;

$authHeader = "Authorization: Basic YXBpYWRtaW46bTFudHJhUjBja2V0MTMhIw==";
$appPropBaseUrl = "http://erptools.myntra.com/myntra-tools-service/platform/tools/properties/search/?q=name.eq:";
//$appPropBaseUrl = "http://tools.scmqa.myntra.com/myntra-tools-service/platform/tools/properties/search/?q=name.eq:";

$appPropWhUrl = $appPropBaseUrl."atp.AtpSimMisMatch.inv.dicrepancies.threshold";
$output = shell_exec("curl -v -H '" . $authHeader . "' -H 'Content-Type:application/xml' -XGET '" . $appPropWhUrl . "' -d '" . $data . "'");
$xml = simplexml_load_string($output);
if (isset($xml->status->statusType) && $xml->status->statusType == "SUCCESS" && isset($xml->data->applicationProperty->value)){
    $invDicrepancyThreshold = $xml->data->applicationProperty->value;
}

echo "invDicrepancyThreshold: ".$invDicrepancyThreshold. "\n";

for ($loop = 0; $loop < 1; $loop++) {
    $offset = 0;
    $limit = 3000;
    $hasMore = true;
    $allDisabledSkusWithInv = array();
    $allSimAtpMismatch = array();
//    $allSkuIds = array();

    
    while ($hasMore) {
        $skuIds = array();
         $skusQuery = "select sku_id, seller_id from seller_item_master where enabled=1 limit ".$offset.", ".$limit;
        $skuId2SellerMap = array();
        $sku_result = mysql_query($skusQuery, $simConn);
        if ($sku_result) {
            while ($row = mysql_fetch_array($sku_result, MYSQL_ASSOC)) {
                $skuId2SellerMap[$row['sku_id']] = $row['seller_id'];
                $skuIds[] = $row['sku_id'];
            }
        }
        @mysql_free_result($sku_result);
       
        echo "printing all the skus \n";
        echo implode(",", $skuIds);
        echo "\n printing all the skus  done\n";
 
        if (empty($skuIds)) {
            $hasMore = false;
            continue;
        }
  
        $atpInvCountQuery = "select seller_id,sku_id, inventory_count as net_count,enabled,blocked_order_count from inventory where store_id = 1 and sku_id in (" . implode(",", $skuIds) . ")";
        $atpInvResults = mysql_query($atpInvCountQuery, $atpConn);
        if ($atpInvResults) {
            while ($row = mysql_fetch_array($atpInvResults, MYSQL_ASSOC)) {
                if ($row['net_count'] > $row['blocked_order_count'] && $row['enabled'] == 0) {
                    $allDisabledSkusWithInv[] = $row['sku_id'];
                }
                if(!isset($skuId2SellerMap[$row['sku_id']]) && $row['enabled']==1){
                    $allSimAtpMismatch[] = $row['sku_id'] . "_0";
                }else if(isset($skuId2SellerMap[$row['sku_id']]) && (($row['enabled']=='1' && $skuId2SellerMap[$row['sku_id']] != $row['seller_id']) || ($row['enabled']=='0' && $skuId2SellerMap[$row['sku_id']] == $row['seller_id']))){      
                     $allSimAtpMismatch[] = $row['sku_id'] . "_" . $skuId2SellerMap[$row['sku_id']];
                }
            }
        }
        echo "size of skuId2SellerMap : ".sizeof($skuId2SellerMap)."\n";
        echo "size of allSimAtpMismatch : ".sizeof($allSimAtpMismatch);
        @mysql_free_result($atpInvResults);

        echo "Print allSimAtpMismatch values \n";
        foreach($allSimAtpMismatch as $arr){
            echo $arr;
            echo "\n";
        }
        @mysql_free_result($atpInvResults);
        $offset += $limit;
    }
    if ($loop == 0) {
        $finalSimAtpMismatch = array_unique($allSimAtpMismatch);
    } else {
        $finalSimAtpMismatch = array_intersect($finalSimAtpMismatch,array_unique($allSimAtpMismatch));
   }
}
    mysql_close($imsConn);
    mysql_close($simConn);

    $finalSimAtpMismatchString = "\n";
    $update = true;

if (count($finalSimAtpMismatch) > 0) {

    foreach ($finalSimAtpMismatch as $simATPMismatch) {
        $arr = explode("_", $simATPMismatch);
        $finalSimAtpMismatchString = $finalSimAtpMismatchString . "SkuId : " . $arr[0] . ", sim_enabled_seller : " . ($arr[1]=="0" ? "null": $arr[1]) . "\n";
    }
    echo "finalSimAtpMismatchString :" . $finalSimAtpMismatchString;
}
if (count($finalAllDisabledSkusWithInv) > 0 || count($finalSimAtpMismatch > 0)) {
    $report = "";
    if(count($finalSimAtpMismatch) > $invDicrepancyThreshold){
  //      $to = $to . ",wmsoncall@myntra.com,noc@myntra.com,myntra.sms.alerts@gmail.com";
        $subject = "[CRITICAL] READONLY ".$subject;
        $update = false;
        $report = "This is READONLY report. Please adjust  atp.AtpSimMisMatch.inv.dicrepancies.threshold accordingly and run the script to correct the below discrepancies.\n";
    }
    $report = $report."\n Disabled Skus with sellable Inventory : ".count($finalAllDisabledSkusWithInv)."\n Skus:".implode(",",array_unique($finalAllDisabledSkusWithInv));
    if(count($finalSimAtpMismatch) > 0){
        $report = $report . "\nSIM ATP Enabled Seller Mismatch Count : " . count($finalSimAtpMismatch) . "\n";
        $report = $report . "\n SIM ATP Enabled Seller Mismatch : " . $finalSimAtpMismatchString . "\n";
    }

    mail($to, $subject, $report, $fromHeader);
    echo $report;
} else {
    echo "NA";
}
if (count($finalSimAtpMismatch) > 0 && $update) {
    echo "Sending message to queue \n";
    foreach ($finalSimAtpMismatch as $simATPMismatch) {
        $arr = explode("_", $simATPMismatch);
        $simEntryEntry=array();
        $newEntry = array('skuId' => $arr[0]);
        $simEntryEntry['newEntry'] = $newEntry;
    //    print_r($simEntryEntry);
        $client->sendMessage('imsSyncSellerInventoryQueue', $simEntryEntry, "", 'json', true, 'ims');
    }
}

foreach($finalSimAtpMismatch as $arr){
            echo $arr;
            echo "\n";
}
mysql_close($atpConn);
?>


