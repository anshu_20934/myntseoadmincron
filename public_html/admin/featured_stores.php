<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: admin_product_types.php,v 1.13 2006/01/11 06:55:57 mclap Exp $
#
# Product Type management module
#
require_once "./auth.php";
require_once $xcart_dir."/include/security.php";

$location[] = array("Featured Stores", "admin_featuredstores.php");
//define("MAILER_IMAGES_DIR",'../skin1/mkimages/startshopping/');
define("MAILER_IMAGES_DIR",'../images/featuredstores/');

if($_POST['mode'] =='modify'){
	for($i=0;$i<count($_POST['storename']);$i++){
		if(!empty($HTTP_POST_FILES['storeimage1']['name'][$i])&&!empty($HTTP_POST_FILES['storeimage2']['name'][$i])){
			$uploadfile1 = MAILER_IMAGES_DIR.basename($HTTP_POST_FILES['storeimage1']['name'][$i]);
			$uploadfile2 = MAILER_IMAGES_DIR.basename($HTTP_POST_FILES['storeimage2']['name'][$i]);
			if(is_uploaded_file($HTTP_POST_FILES['storeimage1']['tmp_name'][$i])&&is_uploaded_file($HTTP_POST_FILES['storeimage2']['tmp_name'][$i])){
				if (move_uploaded_file($HTTP_POST_FILES['storeimage1']['tmp_name'][$i], $uploadfile1)&&move_uploaded_file($HTTP_POST_FILES['storeimage2']['tmp_name'][$i], $uploadfile2)) {
					$message = "Store information has updated successfully";
					db_query( "update mk_featured_stores set storename='".$_POST['storename'][$i]."',storeurl='".$_POST['storeurl'][$i]."',
			storeimage_h='".str_replace("../","",$uploadfile1)."',storeimage_v='".str_replace("../","",$uploadfile2)."',display_order='".$_POST['displayorder'][$i]."' where id='".$_POST['ids'][$i]."' ");
				}
			}else {
				$message = "Problem uploading the image";
			}

		}
		if(empty($HTTP_POST_FILES['storeimage1']['name'][$i])&&empty($HTTP_POST_FILES['storeimage2']['name'][$i])){
			db_query("update mk_featured_stores set storename='".$_POST['storename'][$i]."',storeurl='".$_POST['storeurl'][$i]."',
			display_order='".$_POST['displayorder'][$i]."' where id='".$_POST['ids'][$i]."' ");
			$message = "Store information has updated successfully";

		}
	}

}else{
	if ($REQUEST_METHOD == "POST") {
		$storename = $_POST['storename'];
		$storeurl = $_POST['storeurl'];
		$displayorder = $_POST['displayorder'];

		if(!isset($_FILES) && isset($HTTP_POST_FILES))
		$_FILES = $HTTP_POST_FILES;

		$isError = false;
		$message = '';
		if(empty($HTTP_POST_FILES['storeimage1']['name'])&&empty($HTTP_POST_FILES['storeimage2']['name'])){
			$isError = true;
			$message = "Browse the image to upload";
		}

		if(!$isError) {
			if(!file_exists(MAILER_IMAGES_DIR)){
				mkdir(MAILER_IMAGES_DIR,0777) ;

			}else{
				$uploadfile1 = MAILER_IMAGES_DIR.DIRECTORY_SEPARATOR.basename($HTTP_POST_FILES['storeimage1']['name']);
				$uploadfile2 = MAILER_IMAGES_DIR.DIRECTORY_SEPARATOR.basename($HTTP_POST_FILES['storeimage2']['name']);
				if(is_uploaded_file($HTTP_POST_FILES['storeimage1']['tmp_name'])&&is_uploaded_file($HTTP_POST_FILES['storeimage2']['tmp_name'])){
					if (move_uploaded_file($HTTP_POST_FILES['storeimage1']['tmp_name'], $uploadfile1)&&move_uploaded_file($HTTP_POST_FILES['storeimage2']['tmp_name'], $uploadfile2)) {
						$message = "Store information has inserted successfully.";
						db_query("insert into mk_featured_stores (storename,storeurl,storeimage_h,storeimage_v,display_order) values ('".$storename."','".$storeurl."','".str_replace("../","",$uploadfile1)."','".str_replace("../","",$uploadfile2)."','".$displayorder."')");
					}
				}else {
					$message = "Problem uploading the image";
				}

			}
		}


	}
}
$top_message["content"] = $message;
$top_message["anchor"] = "featured";
unset($_FILES);
# Assign the result
$smarty->assign("main","featuredstores");
$smarty->assign("top_message", $top_message);
# Assign the current location line
$smarty->assign("location", $location);
# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
