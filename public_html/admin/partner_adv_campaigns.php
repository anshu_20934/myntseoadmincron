<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: partner_adv_campaigns.php,v 1.7 2006/01/24 07:31:32 max Exp $
#
# Manage advertising campaigns
#

define("NUMBER_VARS", "add[per_visit],add[per_period]");
require "./auth.php";
require $xcart_dir."/include/security.php";

if(!$active_modules['XAffiliate'])
	func_header_location ("error_message.php?access_denied&id=29");

$location[] = array(func_get_langvar_by_name('lbl_adv_campaigns_management'), "");

#
# Define data for the navigation within section
#
$dialog_tools_data["right"][] = array("link" => "partner_adv_stats.php", "title" => func_get_langvar_by_name("lbl_adv_statistics"));

if($close) {
	$mode = 'close';
} elseif($mode == 'add' && !empty($add['campaign']) && ($add['per_visit'] > 0 || $add['per_period'] > 0)) {
	if($StartDay)
		$add['start_period'] = mktime(0, 0, 0, $StartMonth, $StartDay, $StartYear);
	if($EndDay)
		$add['end_period'] = mktime(23, 59, 59, $EndMonth, $EndDay, $EndYear);
	if($campaignid) {
		db_query("UPDATE $sql_tbl[partner_adv_campaigns] SET campaign = '$add[campaign]', type = '$add[type]', data = '$add[data]', per_visit = '$add[per_visit]', per_period = '$add[per_period]', start_period = '$add[start_period]', end_period = '$add[end_period]' WHERE campaignid = '$campaignid'");
	} else {
		db_query("INSERT INTO $sql_tbl[partner_adv_campaigns] (campaign, type, data, per_visit, per_period, start_period, end_period) VALUES ('$add[campaign]', '$add[type]', '$add[data]', '$add[per_visit]', '$add[per_period]', '$add[start_period]', '$add[end_period]')");
		if($add['type'] == 'L')
			func_header_location("partner_adv_campaigns.php?campaignid=".db_insert_id());
	}
} elseif($mode == 'delete' && $campaignid) {
	db_query("DELETE FROM $sql_tbl[partner_adv_campaigns] WHERE campaignid = '$campaignid'");
}

if($mode) {
	func_header_location("partner_adv_campaigns.php");
}

if($campaignid) {
	$campaign = func_query_first("SELECT * FROM $sql_tbl[partner_adv_campaigns] WHERE campaignid = '$campaignid'");
	$smarty->assign("campaign", $campaign);
}

$campaigns = func_query("SELECT * FROM $sql_tbl[partner_adv_campaigns]");
$smarty->assign("campaigns", $campaigns);

$smarty->assign ("main", "partner_adv_campaigns");

$smarty->assign ("month_begin", mktime(0,0,0,date('m'),1,date('Y')));

# Assign the current location line
$smarty->assign("location", $location);

# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
