<?php
require "../auth.php";

if(!empty($_POST) and !empty($_POST['display_name'])) func_array2update('mk_feature_gate_key_value_pairs', array('value'=>$_POST['display_name']), "`key` = 'display_name'");
if(!empty($_POST) and !empty($_POST['display_birth_year'])) func_array2update('mk_feature_gate_key_value_pairs', array('value'=>$_POST['display_birth_year']), "`key` = 'display_birth_year'");
if(!empty($_POST) and !empty($_POST['is_name_optional'])) func_array2update('mk_feature_gate_key_value_pairs', array('value'=>$_POST['is_name_optional']), "`key` = 'is_name_optional'");
if(!empty($_POST) and !empty($_POST['is_birth_year_optional'])) func_array2update('mk_feature_gate_key_value_pairs', array('value'=>$_POST['is_birth_year_optional']), "`key` = 'is_birth_year_optional'");

if(!empty($_POST) and !empty($_POST['birth_year_range_low'])) func_array2update('mk_widget_key_value_pairs', array('value'=>$_POST['birth_year_range_low']), "`key` = 'birth_year_range_low'");
if(!empty($_POST) and !empty($_POST['birth_year_range_high'])) func_array2update('mk_widget_key_value_pairs', array('value'=>$_POST['birth_year_range_high']), "`key` = 'birth_year_range_high'");

$display_name = FeatureGateKeyValuePairs::getBoolean('display_name');
$display_birth_year = FeatureGateKeyValuePairs::getBoolean('display_birth_year');
$is_name_optional = FeatureGateKeyValuePairs::getBoolean('is_name_optional');
$is_birth_year_optional = FeatureGateKeyValuePairs::getBoolean('is_birth_year_optional');
$birth_year_range_low = WidgetKeyValuePairs::getWidgetValueForKey('birth_year_range_low');
$birth_year_range_high = WidgetKeyValuePairs::getWidgetValueForKey('birth_year_range_high');

$birth_year_range_low = intval($birth_year_range_low);
$birth_year_range_high = intval($birth_year_range_high);

$smarty->assign('display_name', $display_name);
$smarty->assign('display_birth_year', $display_birth_year);
$smarty->assign('is_name_optional', $is_name_optional);
$smarty->assign('is_birth_year_optional', $is_birth_year_optional);

$smarty->assign('birth_year_range_low', $birth_year_range_low);
$smarty->assign('birth_year_range_high', $birth_year_range_high);

$smarty->assign("main","new_signup_control");
func_display("admin/home.tpl", $smarty);

?>
