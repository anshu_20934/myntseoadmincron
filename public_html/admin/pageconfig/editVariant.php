<?php 
require_once  "../auth.php";
require_once HostConfig::$documentRoot."/include/security.php";
require_once HostConfig::$documentRoot."/include/func/func.utilities.php";

use enums\pageconfig\PageConfigSectionConstants;

$locationType = $_REQUEST['locationType'];
$pageLocation = $_REQUEST['pageLocation'];
$section = $_REQUEST['section'];
$variantFlag = $_REQUEST['variantFlag'];
$parentImageId = $_REQUEST['parentImageId'];
$imageRuleId = $_REQUEST['imageRuleId'];
if(empty($section)) {
	$section = PageConfigSectionConstants::StaticImage;
}
if($variantFlag == 'true'){
	$variantFlag = true;
}
global $smarty;
$smarty->assign("variantPage", $variantFlag);

require_once "pageconfig.php";

?>