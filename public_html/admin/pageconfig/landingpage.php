<?php 
require_once  "../auth.php";
require_once HostConfig::$documentRoot."/include/security.php";
require_once HostConfig::$documentRoot."/include/func/func.utilities.php";

use enums\pageconfig\PageConfigLocationConstants;
use enums\pageconfig\PageConfigSectionConstants;

$locationType = PageConfigLocationConstants::LandingPage;
$pageLocation = RequestVars::getVar("pageLocation","");
$pageLocationId = RequestVars::getVar("pageLocationId","");
$section = RequestVars::getVar("section");
if(empty($section)) {
	$section = PageConfigSectionConstants::StaticImage;
}
require_once "pageconfig.php";
?>