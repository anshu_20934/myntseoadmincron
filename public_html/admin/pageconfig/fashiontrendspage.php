<?php 
require_once  "../auth.php";
require_once HostConfig::$documentRoot."/include/security.php";
require_once HostConfig::$documentRoot."/include/func/func.utilities.php";

use enums\pageconfig\PageConfigLocationConstants;
use enums\pageconfig\PageConfigSectionConstants;

$locationType = PageConfigLocationConstants::FashionTrends;
$pageLocation = "";
$section = RequestVars::getVar("section");
if(empty($section)) {
	$section = PageConfigSectionConstants::StaticImage;
}
require_once "pageconfig.php";
?>