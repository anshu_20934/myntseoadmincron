<?php 

use enums\base\ActionType;
use pageconfig\action\PageConfigAddAction;
use pageconfig\action\PageConfigDeleteAction;
use pageconfig\action\PageConfigMoveAction;
use pageconfig\action\PageConfigUpdateAction;
use pageconfig\action\PageConfigViewAction;
use pageconfig\exception\ImageValidationException;
//$pageLocation= space_to_hyphen($pageLocation);
$actionType = RequestVars::getVar("actionType");
try {
	switch($actionType) {
		case ActionType::Add:
			$action = new PageConfigAddAction($locationType,$section,$pageLocation);
			$action->run();
			break;
		case ActionType::Update:
			try{
				$action = new PageConfigUpdateAction($locationType, $section, $pageLocation, $variantFlag, $parentImageId, $imageRuleId);
			}catch (ImageValidationException $e) {
				$smarty->assign("errorMessage", "One: ".$e->getDisplayMessage());
				break;
			}
			try{
			$action->run();
			            }catch (ImageValidationException $e) {
                $smarty->assign("errorMessage", "Two: ".$e->getDisplayMessage());
            }
			break;
		case ActionType::Delete:
			$action = new PageConfigDeleteAction($locationType, $section, $pageLocation, $variantFlag, $parentImageId, $imageRuleId);
			$action->run();
			break;
		case ActionType::MoveUp:
			$action = new PageConfigMoveAction($locationType, $section, $pageLocation, ActionType::MoveUp);
			$action->run();
			break;
		case ActionType::MoveDown:
			$action = new PageConfigMoveAction($locationType, $section, $pageLocation, ActionType::MoveDown);
			$action->run();
			break;
	}
} catch (ImageValidationException $e) {
	$smarty->assign("errorMessage", $e->getDisplayMessage());
}
$url = HostConfig::$userGroupServiceUrl.'/';
$method = 'GET';
$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Content-type: application/json';
$headers[] = 'Accept-Language: en-US';
$request = new RestRequest($url, $method);
$request->setHttpHeaders($headers);
$request->execute();
$info = $request->getResponseInfo();
$body = $request->getResponseBody();
if($info['http_code'] == 200){
	$response = json_decode($body,true);
	if($response['groupResponse']['status']['statusType'] == 'SUCCESS'){
		if(count($response['groupResponse']['data']['UserGroup'][0])==0){
			$tempArr = array();
			array_push($tempArr, $response['groupResponse']['data']['UserGroup']);
			$smarty->assign('userGroup',$tempArr);
		}
		else{
			$smarty->assign('userGroup',$response['groupResponse']['data']['UserGroup']);						
		}
	}
}

//Location Service

$url = HostConfig::$locationGroupServiceUrl;
$method = 'GET';
$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Content-type: application/json';
$headers[] = 'Accept-Language: en-US';
$request = new RestRequest($url, $method);
$request->setHttpHeaders($headers);
$request->execute();
$info = $request->getResponseInfo();
$body = $request->getResponseBody();
if($info['http_code'] == 200){
	$response = json_decode($body,true);
	if($response['locationGroupResponse']['status']['statusType'] == 'SUCCESS'){
		if(count($response['locationGroupResponse']['data']['LocationGroup'][0])==0){
			$tempArr = array();
			array_push($tempArr, $response['locationGroupResponse']['data']['LocationGroup']);
			$smarty->assign('locationGroup',$tempArr);
		}
		else{
			$smarty->assign('locationGroup',$response['locationGroupResponse']['data']['LocationGroup']);						
		}
	}
}

if($variantFlag){
	$action = new PageConfigViewAction($locationType,$section,$pageLocation, $variantFlag, $parentImageId, $imageRuleId);	
}
else{
	$action = new PageConfigViewAction($locationType,$section,$pageLocation);
}
$action->run();

?>
