<?php
/*
 * Created on Feb 7, 2012
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
require "./auth.php";
require $xcart_dir."/include/security.php";
include_once("$xcart_dir/include/func/func_sku.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/modules/apiclient/WarehouseApiClient.php");
include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");
include_once("$xcart_dir/include/class/oms/OrderWHManager.php");

x_load('db','order', 'mail');

$action = $_POST['action'];

$courierServiceabilityVersion = FeatureGateKeyValuePairs::getFeatureGateValueForKey('lms.courierserviceability.version', 'old');

if($action == 'get_wh_info') {
	$itemid = $_POST['itemid'];
	$product_results = ItemManager::getItemDetailsForItemIds($itemid);
	$product = $product_results[0];
	
	$price = ($product['total_amount'] - $product['coupon_discount_product'] - $product['cash_redeemed'] - $product['pg_discount'])/$product['quantity'];
	
	$order_details = func_query_first("select s_zipcode,payment_method from xcart_orders where orderid = $orderid",true);
	
	$skuId2ItemsMap = ItemApiClient::getSkuWiseItemDetailsForOrder($orderid, $order_details['warehouseid']);

	if( count($skuId2ItemsMap[$product['sku_id']]) > ($product['amount'] - $_POST['qtySelected'])) {
		$response = array('FAILURE', 'Items have been issued for selected no of units from current warehouse.');
	} else {
		$skuIds = array($product['sku_id']);
		if($product['supply_type'] == 'ON_HAND' && $product['seller_id'] == 1) {
			$sku_details = OrderWHManager::loadWarehouseInvCounts($skuIds);
		} else {
			$skuId2InvRow = AtpApiClient::getAvailableInventoryForSkus($skuIds);
			$invRow = $skuId2InvRow[$product['sku_id']];
			$sku_details = array();
			$sku_details[$product['sku_id']]['availableItems'] = $invRow['availableCount'];
			$sku_details[$product['sku_id']]['warehouse2AvailableItems'] = array();
			if(!empty($invRow['availableInWarehouses'])) {
				foreach(explode(",", $invRow['availableInWarehouses']) as $whId)
				$sku_details[$product['sku_id']]['warehouse2AvailableItems'][$whId] = $invRow['availableCount'];
			}
		}
		
		$order_details = func_query_first("select s_zipcode,payment_method from xcart_orders where orderid = $orderid");
		
		if($courierServiceabilityVersion == 'new') {
			$product_results[0]['finalPrice'] = $price;
			OrderWHManager::loadServiceabilityDetails($product_results, $sku_details, $order_details['s_zipcode'],$order_details['payment_method'],'DELIVERY', true);
		}
		
		$warehouses = WarehouseApiClient::getAllWarehouses();
		$whId2NameMap = array();
		foreach($warehouses as $warehouse) {
			$whId2NameMap[$warehouse['id']] = $warehouse['name'];
		}
		
		$current_wh_id = $_POST['current_warehouseid'];
		$applicable_warehouses = array();
		
		if($courierServiceabilityVersion == 'new') {
			$warehouseCourier2AvailablityData = $sku_details[$product['sku_id']]['warehouse2AvailableItems'];
			foreach($warehouseCourier2AvailablityData as $whCourier => $availablityData) {
				if($availablityData['invCount'] >= $_POST['qtySelected'] && ( $order_details['payment_method'] != 'cod' || ($order_details['payment_method'] == 'cod' && $availablityData['CODLimit'] >= $price) )) {
					$whIdCourierArr = explode("__",$whCourier);
					if($whIdCourierArr[0] != $current_wh_id && !isset($applicable_warehouses[$whIdCourierArr[0]]))
						$applicable_warehouses[$whIdCourierArr[0]] = $whId2NameMap[$whIdCourierArr[0]];
				}
			}
		} else {
			$warehouse2AvailableCount = $sku_details[$product['sku_id']]['warehouse2AvailableItems'];
			foreach($warehouse2AvailableCount as $whId=>$count) {
				if($whId != $current_warehouseid) {
					if($count >= $_POST['qtySelected'])
						$applicable_warehouses[$whId] = $whId2NameMap[$whId];
				}
			}
		}
		$weblog->info("Other wh with availability - ".print_r($applicable_warehouses, true));
			
		if(empty($applicable_warehouses))
			$response = array('FAILURE', 'Not available in any warehouses');
		else {
			$response = array('SUCCESS', $applicable_warehouses);
		}
	}
	
	echo json_encode($response);
	exit(0);
}else if($action == 'confirm') {
	$orderid = $_GET['orderid'];
	$itemid = $_POST['itemid'];
	$qty = $_POST['qty'];
	$warehouseId = $_POST['warehouseid'];
	
	if($courierServiceabilityVersion == 'new') {
		$response = OrderWHManager::assignOrderItemsToNewWarehouse($orderid, $itemid, $qty, $warehouseId, $login);
	} else {
		$response = OrderWHManagerOld::assignOrderItemsToNewWarehouse($orderid, $itemid, $qty, $warehouseId, $login);
	}
		
	echo json_encode($response);
	exit(0);
} else {
	
	$productsInCart = ItemManager::getProductDetailsForItemIds(explode(",", $_GET['itemids']));
	
	$smarty->assign("action", "confirm");
	$smarty->assign("current_warehouseid", $_GET['current_warehouseid']);
	$smarty->assign("productsInCart", $productsInCart);
	$smarty->assign("itemids", $_GET['itemids']);
	$smarty->assign("orderid", $_GET['orderid']);
}

$smarty->assign("main","reassign_warehouse");
func_display("admin/home.tpl", $smarty);
 
?>
