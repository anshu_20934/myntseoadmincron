<?php
require "./auth.php";
require $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.order.php";
include_once("../include/func/func.mkcore.php");
include_once("../include/func/func.mk_orderbook.php");
include_once("../include/func/func_sku.php");
include_once("../include/func/func.mkspecialoffer.php");
include_once("../include/func/func.inventory.php");
include_once("../include/func/func.mail.php");
include_once("$xcart_dir/include/class/oms/OrderWHManager.php");
require_once $xcart_dir."/include/class/class.mail.multiprovidermail.php";
include_once("$xcart_dir/env/Notifications.Config.php");
include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
include_once("$xcart_dir/modules/apiclient/ReleaseApiClient.php");
include_once("$xcart_dir/include/class/oms/EventCreationManager.php");

$action  = $HTTP_POST_VARS['action'];
$uploadStatus = false;
if($action == '')
	$action ='upload';
elseif($action == 'verify'){
	if(!isset($_FILES) && isset($HTTP_POST_FILES))
		$_FILES = $HTTP_POST_FILES;
	if(!empty($_FILES['orderdetail']['name']))
	{
  		$uploaddir1 = '../bulkqueuedorders/';
		$extension = explode(".",basename($_FILES['orderdetail']['name']));
		$uploadfile1 = $uploaddir1 . date('d-m-Y-h-i-s').".".$extension[1];
		if (move_uploaded_file($_FILES['orderdetail']['tmp_name'], $uploadfile1)) {
		     	 $uploadStatus = true;
		}
	    if($uploadStatus) {
		// open the text file
		    $fd = fopen ($uploadfile1, "r");
		    $orderListDisplay =  array();
		    $failedOrderListDisplay = array();
		    
		    $dataFormatError = "Data Format Error: uploaded data does not confirm to either CCAvenue - {{cell-A: orderid, cell-E: amount}} or EBS - {{ cell-A: orderid, cell-B: amount}} format";
			$line = 0;
			while (!feof ($fd)) {
				$buffer = fgetcsv($fd, 4096);
				$line = $line + 1;
				
				if (empty($buffer[0])) {
					break;
				}
				if (!preg_match('/^\d+$/', $buffer[0])) {
					$smarty->assign('dataFormatError', "Invalid 'orderid' at line# $line");		
					break;	
				}
				$fieldCount = count($buffer);
				$csvOrderID = null;
				$csvAmount  = null;
				if ($fieldCount == 2) {
					$csvOrderID = $buffer[0];
					$csvAmount  = $buffer[1];
				} else if ($fieldCount >= 5) {
					$csvOrderID = $buffer[0];
					$csvAmount  = $buffer[4];	
				} else {
					$smarty->assign('dataFormatError', "$dataFormatError at line# $line");
					break;
				}
				
				if (! preg_match('/^([-+])?\d+(.\d+)?$/', $csvAmount)) {
					$smarty->assign('dataFormatError', "Invalid 'amount' at line# $line");
					break;
				}	

				$sql = "select status,cancellation_code from xcart_orders ";
				$sql = $sql . " where orderid='$csvOrderID' and ";
				$sql = $sql . " total+payment_surcharge = $csvAmount";
				$result = func_query_first($sql);
				if($result) {
					$status = $result['status'];
					$cancellationCode = $result['cancellation_code'];
					if (!$status) {
						$failedOrderListDisplay[] = array('orderid' => $csvOrderID, 
							'msg' => "Amount did not match"
						);
					} else {
						if($status == 'PP' || $status == 'D' || ($status == 'F'&& empty($cancellationCode))) {						
							$orderListDisplay[] = $csvOrderID;					
						} else if ($status == 'F'&& !empty($cancellationCode)) {
							$failedOrderListDisplay[] = array('orderid' => $csvOrderID, 'msg'=>'Order has been rejected manually');
						} else {
							$failedOrderListDisplay[] = array('orderid' => $csvOrderID, 'msg'=>'Invalid Status '.$status);
						}
					}
				} else {
					$giftOrderSql = "Select gift_card_id from gift_cards_orders where orderid = $csvOrderID";
					$row = func_query_first($giftOrderSql);
					if($row) {
						$failedOrderListDisplay[] = array('orderid' => $csvOrderID, 'msg'=>'Gift Order cannot be completed from here');
					} else {
						$failedOrderListDisplay[] = array('orderid' => $csvOrderID, 'msg'=>'Invalid OrderId');
					}
				}					
			}
			fclose ($fd);
			$smarty->assign('orderListDisplay',$orderListDisplay);
			$smarty->assign('failedOrderListDisplay', $failedOrderListDisplay);
			$smarty->assign('uploadorderFile',$uploadfile1);
		} else {
			$action ='upload';
			$smarty->assign("errorMsg", 'Failed to upload file.');
		}
	}
}
elseif($action == 'confirm')
{
    $content = '<div align="center" style="width:100%"><div>Run by: '.$login.'</div><br/>';
    $content .=  '<h3 align="center">Orders Queued</h3>'; 

    $queuedOrdersContent = '<table width="80%" border="1"><tr align="left" style="background-color:bisque;"><th>S.No.</th><th>Order Id</th><th>Previous Status</th><th>Current Status</th></tr>';
    $noInventoryOrdersContent = '<table width="40%" border="1"><tr align="left" style="background-color:bisque;"><th>S.No.</th><th>Order Id</th></tr>';
    $failedOrdersContent = '<table width="40%" border="1"><tr align="left" style="background-color:bisque;"><th>S.No.</th><th>Order Id</th><th>Reason</th></tr>';
    $notServiceableOrdersContent = '<table width="40%" border="1"><tr align="left" style="background-color:bisque;"><th>S.No.</th><th>Order Id</th></tr>';
    $i = 1;
    $j = 1;
    $k = 1;
    $l = 1;
    $queuedOrders = array();	
    $failedorderids = array();
    foreach ($orderids as $orderid){
        //$arr = explode("-", $value);
        if( preg_match('/^\d+$/', $orderid)){
            $order_info = func_query_first("select * from $sql_tbl[orders] where orderid = $orderid");
        	$productsInCart = func_create_array_products_of_order($orderid);
		    $itemsAvailable = check_order_item_availability_atp($productsInCart);
		    if (!$itemsAvailable) {
				$noInventoryOrdersContent .= "<tr align='left'><td>".$j++."</td><td>$orderid</td></tr>";
				continue;	
		    }
            
		    $courierServiceabilityVersion = FeatureGateKeyValuePairs::getFeatureGateValueForKey('lms.courierserviceability.version', 'old');
		    if($courierServiceabilityVersion == 'new') {
				$serviceabilityResponse = OrderWHManager::loadServiceabilityDetails($productsInCart, $itemsAvailable, $order_info['s_zipcode'],$order_info['payment_method']);
 				if(!$serviceabilityResponse['status']){
 					$notServiceableOrdersContent .= "<tr align='left'><td>".$l++."</td><td>$orderid</td></tr>";
 					continue;
 				}
		    }	
			$wasSuccess = true;
			try{	
                $updateSql = "UPDATE xcart_orders SET status='Q',queueddate='".time()."', response_server='".$server_name."', cashback_processed = 1 WHERE orderid = $orderid";
                db_query($updateSql);

                func_addComment_status($orderid, "Q", $order_info['status'], "Queuing order from PP-Q script.", "AUTOSYSTEM");
				
				EventCreationManager::pushInventoryUpdateToAtpEvent($orderid);
                EventCreationManager::pushOrderProcessEvent($orderid, array('comment' => "Queuing order from PP-Q script."));
                
			}catch(Exception $e) {
				$errorMsg = $e->getMessage();
				$wasSuccess = false;
			}
			
			if($wasSuccess) {
				$tracker = new BaseTracker(BaseTracker::CONFIRMATION);
				$orderLogDetails = func_query_first("select * from mk_payments_log where orderid = $orderid");
				$toLog = array();
				if(!empty($orderLogDetails)) {
					$toLog['time_return'] = time();
					$toLog['is_complete'] = 1;
					$toLog['is_flagged'] = 0;
					$toLog['completed_via'] = 'Manual';
					func_array2updateWithTypeCheck('mk_payments_log', $toLog,"orderid=$orderid");
				}
				if(!is_null($tracker->confirmationData))
				{
					$couponCashDetails = array('coupon' => $order_info['coupon'], 'coupon_discount' => $order_info['coupon_discount'], 'cash_coupon_code' => $order_info['cash_coupon_code'], 'cash_redeemed' => $order_info['cash_redeemed']);
					$tracker->confirmationData->setOrderId($orderid);
					$tracker->confirmationData->setPaymentData(array_merge($orderLogDetails, $toLog,$couponCashDetails));
					$tracker->confirmationData->setCartData($productArray);
				}
				$tracker->fireRequest();
				$queuedOrdersContent .= ' <tr align="left"> <td> ' . $i++ . ' </td> <td>' . $orderid . '</td> <td>' . $order_info['status'] . '</td> <td> Queued </td> </tr> ';
				$queuedOrders[] = $orderid;
			} else {
				$failedOrdersContent .= "<tr align='left'><td>".$k++."</td><td>$orderid</td><td>$errorMsg</td></tr>";
				$failedorderids[] = $orderid;
			}
	    }
    }
	
    $content .= $queuedOrdersContent."</table><br/>";
    $content .= '<h3 align="center">Orders not queued - insufficient inventory</h3>'.$noInventoryOrdersContent."</table><br/>";
    $content .= "<h3 align='center'>Order Verification Failed -- Due to some items are not serviceable in this order</h3>".$notServiceableOrdersContent."</table><br/>";
    $content .= "<h3 align='center'>Order Verification Failed -- Due to some locked/used coupons on this order</h3>".$failedOrdersContent . '</table>'; 
    $content .= "</div>";

    $headers = "Content-Type: text/html; charset=ISO-8859-1 \n";    
    $to = NotificationsConfig::$ppToQueueNotification;
    
    $subject = "PP-Q Script run - " . date("d/M/Y : H:i:s", time());
    $weblog->info("Send PP-Q mail with contents - $content to $to");
    $mail_detail = array(
                        "to"=>$to,
                        "subject"=>$subject,
                        "content"=>$content,
                        "from_name"=>'Myntra Admin',
    					"mail_type" => MailType::NON_CRITICAL_TXN,
                        "from_email"=>'admin@myntra.com',
                        "header"=>$headers
                    );

    $multiPartymailer = new MultiProviderMailer($mail_detail);
    $flag = $multiPartymailer->sendMail();
    $smarty->assign("message", "");
    $weblog->info("Sent PP-Q mail");
    if (!empty($queuedOrders)) {
		$smarty->assign("googleEcommerceTracking", 1);
		$smarty->assign("ga_acc", $ga_acc);
		$smarty->assign("ecomtrackingdata", prepareGoogleEcommerceTrackingData($queuedOrders));		
    }
    $smarty->assign("summary", $content);
    $action = 'upload';
}
$smarty->assign("action",$action);
$smarty->assign("main",'upload_payment_completed_orders');
func_display("admin/home.tpl",$smarty);

function prepareGoogleEcommerceTrackingData($orderids){
	$data = array();
	foreach($orderids as $orderid) {
		$orderdata = array();
		$orderdata['orderid']	= $orderid;
		$orderdata['userinfo']	= func_customer_order_address_detail_by_orderid($orderid);
		$sql = "SELECT subtotal,firstname,issues_contact_number, coupon_discount, coupon AS couponcode, shipping_cost, tax, ordertype, ref_discount, gift_status,gift_charges,payment_surcharge as payment_surcharge, total, cod, qtyInOrder, additional_info FROM xcart_orders WHERE orderid = '".$orderid."'";
		$amountdetail = func_query_first($sql);
		$amountafterdiscount = $amountdetail['total'] + $amountdetail['tax'] + $amountdetail['payment_surcharge'];
				
		$amountafterdiscount = number_format($amountafterdiscount,2,".",'');
		$orderdata['amountafterdiscount']= $amountafterdiscount;
		$orderdata['vat'] 		= number_format($amountdetail['tax'],2,".",'');
		$orderdata['shippingRate'] 	= number_format($amountdetail['shipping_cost'],2,".",'');
		$orderdata['productsInCart']	= func_create_array_products_of_order($orderid);
		$data[] = $orderdata;			
	}
	return $data;							
}
?>