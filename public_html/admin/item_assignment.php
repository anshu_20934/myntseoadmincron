<?php
require "./auth.php";
$host=$_SERVER['HTTP_HOST'];
$path='admin/operations/item_assignment.php';
header("HTTP/1.1 307 Moved temporarily");
header("Location: http://$host/$path");
exit;

require $xcart_dir."/include/security.php";
require_once($xcart_dir."/include/func/func.itemsearch.php");
require_once($xcart_dir."/include/sendmail.class.php");

if($HTTP_POST_VARS){
	$query_string = http_build_query($HTTP_POST_VARS);
	$mode = $HTTP_POST_VARS['mode'];
	
}
if($HTTP_GET_VARS){
	$query_string = http_build_query($HTTP_GET_VARS);
	$mode = $HTTP_GET_VARS['mode'];
    $action = $HTTP_GET_VARS['action'];
}
if($HTTP_POST_VARS['payment_method']) {
		if($HTTP_POST_VARS['payment_method'] == "none") {

			$payment_method = "";
		} else {
			$payment_method = $HTTP_POST_VARS['payment_method'];
		}
}
if($HTTP_GET_VARS['payment_method']) {
		if($HTTP_GET_VARS['payment_method'] == "none") {
			$payment_method = "";
		} else {
			$payment_method = $HTTP_GET_VARS['payment_method'];
		}
}
if($HTTP_GET_VARS['sourceid']) {
	$sourceid = $HTTP_GET_VARS['sourceid'];
}
if($mode == "search"){
	/*
	##get order all order sources which are active
	*/
	$sql = "select
				s.source_id,
				s.source_code,
				s.source_name,
				s.source_manager_email,
				s.location_id
			from
				{$sql_tbl['sources']} s
			left join 
				{$sql_tbl['source_locations']} loc
			on
				s.location_id = loc.location_id
			WHERE 
				s.active=1
			ORDER BY
				s.source_name";
	$order_sources = func_query($sql);	
	$smarty->assign('order_sources',$order_sources);
	#
	#Calling function to create search query for number of records
 	#
   
	$count_sql = func_create_search_query($item_status, $item_assignee, $item_location, $item_id, $order_id, $order_status, $customer_name, $product_type, $product_style,$order_by, $sort_order, $from_date, $to_date,$operations_location, true,$payment_method,$is_customizable,$sourceid);
    
	$sqllog->debug("File name##item_assignment.php##SQL Info::>$count_sql");
	$total_items = func_query_first_cell($count_sql);
	$smarty->assign("total_items", $total_items);
	if ($total_items > 0){
		$page = $_GET["page"];

		#
		# Prepare the page navigation
		#
		if (!isset($objects_per_page)) {
			if ($current_area == "C")
			$objects_per_page = $config["Appearance"]["products_per_page"];
			else
			$objects_per_page = $config["Appearance"]["products_per_page_admin"];
		}
		$total_nav_pages = ceil($total_items/$objects_per_page)+1;

		require $xcart_dir."/include/navigation.php";

		$search_sql = func_create_search_query($item_status, $item_assignee, $item_location, $item_id, $order_id, $order_status, $customer_name, $product_type, $product_style,$order_by, $sort_order, $from_date, $to_date,$operations_location,false, $payment_method,$is_customizable,$sourceid);

		$search_sql .=" LIMIT $first_page, $objects_per_page";

		$sqllog->debug("File name##item_assignment.php##SQL Info::>$search_sql");
		$itemResult = func_query($search_sql);

            foreach($itemResult AS $key=>$array){

            	if($array['assignee']){
                	$sql = "SELECT name FROM mk_assignee WHERE id='".$array['assignee']."'";
	                $itemResult[$key]['assignee_name'] = func_query_first_cell($sql);
        	}else{
               		$itemResult[$key]['assignee_name'] = "Not Assigned";
            	}

            if($array['location']){
                $sql = "SELECT location_name FROM mk_operation_location WHERE id='".$array['location']."'";
                $itemResult[$key]['location_name'] = func_query_first_cell($sql);
            }else{
               $itemResult[$key]['location_name'] = "Not Assigned";
            }
                //get design name from xcart_products
                $productname = "SELECT product FROM xcart_products WHERE productid = ".$itemResult[$key]['productid']."";
                $productresult = db_query($productname);
                $productrow = db_fetch_array($productresult);
                $itemResult[$key]['designName'] = empty($productrow)?'':$productrow['product'];    


        }
        #
        #navigation url
        #
        if($mode == "search")
        $URL = "item_assignment.php?".$query_string;
        else
        $URL = "item_assignment.php";
        $smarty->assign("navigation_script",$URL);
        $smarty->assign("first_item", $first_page+1);
        $smarty->assign("last_item", min($first_page+$objects_per_page, $total_items));
        $smarty->assign("total_items",$total_items);
        $smarty->assign ("itemResult", $itemResult);
        $smarty->assign ("sizeofitemresult", $objects_per_page);
     }
}

#
#Bydefault assignment of photo products to Ganesh kumar
#
$assignmenttime = time();
$sql = "UPDATE $sql_tbl[order_details] SET assignee=4, item_status='A', assignment_time='$assignmenttime' WHERE product_type=39 AND item_status='WP'";
db_query($sql);

#
#Load item locations
#
$sql = "SELECT * FROM $sql_tbl[operation_location] WHERE status=1";
if (isset($operations_location))
	$sql .= " and operations_location = '$operations_location'";
$locations = func_query($sql);

#
#Load operations locations
#
$sql = "SELECT * FROM $sql_tbl[operations_location] WHERE status=1";
$sql_result = func_query($sql);
foreach ($sql_result as $key=>$value){
	$operations_locations[$value['id']] = $value['name'];

}


#
#Load assigee list
#
$sql = "SELECT * FROM $sql_tbl[mk_assignee] WHERE status=1";
if (isset($operations_location))
	$sql .= " and operations_location = '$operations_location'";
$assignee = func_query($sql);

#
#Load Product Types
#
$sql = "SELECT * from {$sql_tbl[mk_product_type]}";
$sql_result = func_query($sql);
foreach ($sql_result as $key=>$value){
	$product_types[$value['id']] = $value['name'];
}

#
#Load Style names
#
$sql = "SELECT * from {$sql_tbl[mk_product_style]}";
if($product_type != "")
  $sql.=" where product_type=$product_type";
$sql_result = func_query($sql);
foreach ($sql_result as $key=>$value){
	$product_styles[$value['id']] = $value['name'];
}


#
#Load number of items assigned
#
$sql = "SELECT SUM(a.amount) AS assigned_item, a.assignee AS aid FROM $sql_tbl[order_details] a, $sql_tbl[mk_assignee] b, $sql_tbl[orders] c WHERE a.orderid=c.orderid AND c.status='WP' AND a.assignee=b.id  AND (a.item_status = 'A' OR a.item_status = 'S') GROUP BY a.assignee";
$assigned_item = func_query($sql);

#
#Item Status array
#
$status = array("UA"=>array("status"=>"Unassigned", "color"=>"#EE0F14"),
				"A"=>array("status"=>"Assigned", 	"color"=>"#77AB3D"),
				"I"=>array("status"=>"In Progress",	"color"=>"#55FFFF"),
                "OD"=>array("status"=>"Ops Done", 		"color"=>"#99FF22"),
                "QD"=>array("status"=>"QA Done", 		"color"=>"#EF940A"),
                "D"=>array("status"=>"Done", 		"color"=>"#80FFFF"),
                "F"=>array("status"=>"Failed", 		"color"=>"#333FFF"),
                "H"=>array("status"=>"On Hold", 	"color"=>"#F5FF42"),
                "S"=>array("status"=>"Sent Outside","color"=>"#04FF82"));
$smarty->assign ("status", $status);

#
#Order Status array
#
$order_statuses = array("Q" => "Queued",
                     "WP" => "WIP",
                     "B" => "Back Ordered",
                     "D" => "Declined",
                     "F" => "Failed",
                     "C" => "Complete",
                     "OH" => "On Hold");
$smarty->assign ("order_status", $order_statuses);





#
#Sending confirmation to operation head of unassigned items
#

if($action=="send_confirmation"){

    $search_sql = func_create_search_query($item_status, $item_assignee, $item_location, $item_id, $order_id, $order_status, $customer_name, $product_type, $order_by, $sort_order, $from_date, $to_date,$operations_location, false, $payment_method);
    $itemResultDone = func_query($search_sql);
    
    #n
    #generate table
    #
    
    $file_name_suffix = time();
    $unassigned = fopen($xcart_dir."/admin/item_assignment_report/item_assignment_report_$file_name_suffix.csv","a");
    $summary_table = "Orderid, Source, ItemId,  Status, Product Type, Item Name, Quantity";
    fwrite($unassigned, $summary_table);
    fwrite($unassigned, "\r\n");
    $string = "";
    foreach($itemResultDone AS $key=>$array){

        $item_status = $status[$array['item_status']]['status'];
        $string .= $array[orderid] . ", " . $array[source_name] . ", " . $array[itemid] . ", " . $item_status . ", " . $array[product_type_name] . ", " . $array[stylename] . ", " . $array[amount];
        $string .= "\r\n";
        
    }
    fwrite($unassigned, $string);
    fwrite($unassigned, "\r\n");
    $toemail="alrt_itemassignment@myntra.com";
	$template = "itemassignmentdone";
	$template_query = "SELECT * FROM $sql_tbl[mk_email_notification] WHERE name = '$template'";
	$template_result = db_query($template_query);
	$row = db_fetch_array($template_result);
	$subject = $row['subject'];
	$content = $row['body'];
	$args = array( "Name" =>"Operation Head");
	$keys = array_keys($args);
	$key ;
	for($i = 0; $i < count($keys); $i++){
		$key = $keys[$i];
		$content = str_replace("[".$key."]",$args[$key],$content);
	}
	$filename = $xcart_dir."/admin/item_assignment_report/item_assignment_report_$file_name_suffix.csv";
	$mail = new sendmail();
	$mail->from("Myntra Support", "support@myntra.com");
	$mail->to($toemail);
	//$mail->cc("partners@myntra.com");
	$mail->subject($subject);
    $mail->text($content);
    $mail->attachment($filename);
    $mail->send();

}

if($mode == "download_report"){
    $search_sql = func_create_search_query($item_status, $item_assignee, $item_location, $item_id, $order_id, $order_status, $customer_name, $product_type, $product_style,$order_by, $sort_order, $from_date, $to_date,$operations_location, false,$payment_method,'',$sourceid);
    $sqllog->debug("File name##item_assignment.php##SQL Info for CSV download::>$search_sql");
    $itemResultDone = func_query($search_sql);

    #
    #generate table
    #
    $file_name_suffix = time();
    $assignment_report = fopen($xcart_dir."/admin/item_assignment_report/item_assignment_report_$file_name_suffix.csv","a");
    $summary_table = "Orderid, Source, LoginId, ItemId,  Status, Product Type, Item Name, Quantity breakup, Design Name, Quantity, Queued date, Item Assignee, Assignment date, Completed date";
    fwrite($assignment_report, $summary_table);
    fwrite($assignment_report, "\r\n");
    $string = "";
    foreach($itemResultDone AS $key=>$array){
         //get design name from xcart_products
        $productname = "SELECT product FROM xcart_products WHERE productid = ".$itemResultDone[$key]['productid']."";
        $productresult = db_query($productname);
        $productrow = db_fetch_array($productresult);
        $array['designName'] = empty($productrow)?'':$productrow['product'];
	    $array['completion_time'] = empty($array['completion_time']) ? "--" : date('d-m-Y H:i:s', $array['completion_time']);
        $array['queueddate'] = empty($array['queueddate']) ? "--" : date('d-m-Y H:i:s', $array['queueddate']);
        $array['assignment_time'] = empty($array['assignment_time']) ? "--" : date('d-m-Y H:i:s', $array['assignment_time']);

        if($array['assignee']){
            $sql = "SELECT name FROM mk_assignee WHERE id='".$array['assignee']."'";
            $assignee_name = func_query_first_cell($sql);
        }else{
            $assignee_name = "Not Assigned";
        }
        $quantity_breakup = str_replace("," , " || ", $array[quantity_breakup]);
        $item_status = $status[$array['item_status']]['status'];
        $string .= $array[orderid] . ", " . $array[source_name] . ", " .$itemResultDone[$key]['login']. "," . $array[itemid] . ", " . $item_status . ", " . $array[product_type_name] . ", " . $array[stylename]. "," . $quantity_breakup . ", ". mysql_real_escape_string($array[designName]) . ", " . $array[amount] . ", " . $array[queueddate] . ", " . $assignee_name . ", " . $array[assignment_time]. ", " . $array[completion_time];
        $string .= "\r\n";
    }
    fwrite($assignment_report, $string);
    fwrite($assignment_report, "\r\n");

#
#Download
#
    $filename = "item_assignment_report_$file_name_suffix.csv";
	$dir = "../admin/item_assignment_report/";
	force_download($filename, $dir);
exit;
}

/*if($action=="a_failed"){
    $smarty->assign ("action", $action);
}*/
$smarty->assign ("query_string", $query_string);
$smarty->assign ("mode", $mode);
$smarty->assign("assigned_item",$assigned_item);

$smarty->assign("payment_method", $payment_method);
$smarty->assign("assignee",$assignee);
$smarty->assign("product_types",$product_types);
$smarty->assign("product_styles",$product_styles);
$smarty->assign("locations",$locations);
$smarty->assign("operations_locations",$operations_locations);
$smarty->assign("main","item_assignment");
func_display("admin/home.tpl",$smarty);
?>
