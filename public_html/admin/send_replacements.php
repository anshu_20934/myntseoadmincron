<?php
require "./auth.php";
ini_set("diaplay_errors",1);
require $xcart_dir."/include/security.php";
require $xcart_dir."/include/func/func.mk_orderbook.php";
require_once $xcart_dir."/include/func/func.order.php";
require $xcart_dir."/mkstatesarray.php";
require_once $xcart_dir."/include/func/func.mail.php";
require_once($xcart_dir.'/include/class/class.ordertime.php');

function send_replacement_message($neworderid,$oldorderid){

    $cust_details=func_query_first("select firstname,login from xcart_orders where orderid='$neworderid'");
    $login=$cust_details[login];
    $firstname=$cust_details[firstname];
    $item_details=func_query("select itemid,name,amount from xcart_order_details od,mk_product_style ps where orderid='$neworderid' and ps.id=od.product_style");

    $itemtext="";
    foreach($item_details as $item){
        $itemtext.="$item[name] (# $item[itemid] ) : Quantity - $item[amount] <BR><BR>";
    }
   
   $delivery_date = ordertime::get_order_time_string($neworderid);   

   $from ="CustomerCare";
   $loginEmail=$login;
   $bcc="customerservice@myntra.com";

   $template = "replacementorder";
   $args = array("OLDORDERID"	=>$oldorderid,
                  "NEWORDERID"	=>$neworderid,
                  "FIRSTNAME"	=> $firstname,
                  "DELIVERYDATE"	=> $delivery_date,
                  "ITEMDETAILS"	=> $itemtext);

   sendMessage($template, $args, $loginEmail,$from,$bcc);
}

if($mode=='replace'){
    //Create a new order and a new item for the order
    $orderid=$HTTP_POST_VARS['orderid'];
    $added_by=$HTTP_POST_VARS['added_by'];
    
    $item_counts=array();
    $quantity_breakups=array();
    $itemids = $HTTP_POST_VARS['itemid'];
    $quantities = $HTTP_POST_VARS['quantity'];
    $quantity_breakup = $HTTP_POST_VARS['quantity_breakup'];
    foreach($itemids as $index=>$value){
        $item_counts[$value]=$quantities[$index];
        $quantity_breakups[$value]=$quantity_breakup[$index];
    }
    $neworderid=func_create_replacement_order($orderid,$item_counts,$quantity_breakups,$HTTP_POST_VARS['replacement_reason'],$added_by);
    
    // Insert this info in to replacement order info table 
    $replacementInfoQuery = "Insert into replacement_order_info value($neworderid , $orderid , " .time().")";
    if($neworderid == 0 ){
      $smarty->assign('mode', "failure");
    }
    else{           
      //Send e-mail to customer service and customer that a replacement order has been placed.      
      send_replacement_message($neworderid,$orderid);

      $smarty->assign('neworderid', $neworderid);
      $smarty->assign('mode', "success");
    }
	
	
}
elseif(!isset($_GET['d'])){
    //$orderid=$_POST['orderid'];
	$smarty->assign('replacement_date',date("m/d/Y"));

	//Retrieving the Item list in the current Order
	$orderquery = "SELECT order_details.itemid AS item_id,product_style.name AS item_name,product_style.id as style_id,order_details.amount AS item_count,order_details.quantity_breakup as quantity_breakup,p.image_portal_t as image
                   FROM {$sql_tbl['order_details']} order_details 
                   LEFT JOIN {$sql_tbl['mk_product_style']} product_style on product_style.id=order_details.product_style 
                   LEFT JOIN {$sql_tbl['products']} p on p.productid=order_details.productid
                   WHERE order_details.orderid='{$_GET['orderid']}'";

	$items_hash=func_query($orderquery);

	foreach($items_hash as $item){
    	$size_option_query="select value from mk_product_options where style=$item[style_id]";
    	//$size_option_query="select value from mk_product_options where style=$item[style_id] and name='size'";
		$size_options[$item[item_id]]=func_query($size_option_query);
	}
	/* get the item delivery status for all cod orders */
	$paymentMethodQuery = "select payment_method, tracking from xcart_orders where orderid=".$_GET['orderid'];
	$result= func_query_first($paymentMethodQuery);
	$paymentMethod = $result['payment_method'];
	$awbNo    = $result['tracking'];
	if($paymentMethod == 'cod') {
		$deliveryStatus = get_bluedart_status($awbNo);
		$smarty->assign("payment_method", $paymentMethod);
		$smarty->assign("awbno", $awbNo);
		$smarty->assign("delivery_status", $deliveryStatus);		
	}  
    $smarty->assign('item_hash', $items_hash);
    $smarty->assign('size_options', $size_options);
    $smarty->assign('mode', "getData");
    //$smarty->assign('item_hash', $orderid);
	
}	
func_display("admin/main/send_replacements.tpl",$smarty);
//func_display("admin/main/replacement_form.tpl",$smarty);
?>
