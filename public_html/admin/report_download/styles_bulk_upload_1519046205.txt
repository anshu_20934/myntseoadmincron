Style ID;Vendor Article Number;Vendor Article Name;Active Status;Article type;Brand;Age Group;Gender;Base Colour;Colour1;Colour2;Fashion Type;Usage;Year;Size - SKU code pair;MRP;Style Description;Style Note;Materials and Care Description;Size and Fit Description;Size Chart Path;Product Display Name;Tags;Product Type;Classification Brand;Season;Comments;Upload Images
2503808;KT01ST075515-Purple-2503808;Textured Silk Tie;Draft;Ties;next;Adults-Men;Men;Purple;;;Fashion;Casual;2018;ONE,NEXTTIES00000097;1200.00;;;;<p>7cm blade width</p>;;;next,JIT;zerotype;;Winter;;No
2503807;KT05ST239509-Blue-2503807;Sigture Silk Printed Floral Tie;Draft;Ties;next;Adults-Men;Men;Blue;;;Fashion;Casual;2018;ONE,NEXTTIES00000096;2000.00;;;;<p>7cm blade width</p>;;;next,JIT;zerotype;;Winter;;No
2503806;KT01085876-Red-2503806;Textured Silk Tie;Draft;Ties;next;Adults-Men;Men;Red;;;Fashion;Party;2018;ONE,NEXTTIES00000095;1200.00;;;;<p>7cm blade width</p>;;;next,JIT;zerotype;;Winter;;No
2503805;KT05ST239507-Multi-2503805;Sigture Floral Print Tie;Draft;Ties;next;Adults-Men;Men;Multi;;;Fashion;Casual;2018;ONE,NEXTTIES00000094;2000.00;;;;<p>7cm blade width</p>;;;next,JIT;zerotype;;Winter;;No
2503804;KT01ST075515-Turquoise Blue-2503804;Textured Silk Tie;Draft;Ties;next;Adults-Men;Men;Turquoise Blue;;;Fashion;Party;2018;ONE,NEXTTIES00000093;1200.00;;;;<p>7cm blade width</p>;;;next,JIT;zerotype;;Winter;;No
2503803;KT10ST200560-Navy Blue-2503803;Circle Pattern Tie And Tie Clip;Draft;Ties;next;Adults-Men;Men;Navy Blue;;;Fashion;Formal;2018;ONE,NEXTTIES00000092;1000.00;;;;<p>6cm blade width</p>;;;next,JIT;zerotype;;Winter;;No
2503802;KT01ST238840-Blue-2503802;Textured Silk Tie;Draft;Ties;next;Adults-Men;Men;Blue;;;Fashion;Formal;2018;ONE,NEXTTIES00000091;1200.00;;;;;;;next,JIT;zerotype;;Winter;;No
2503801;KT02ST177602-Blue-2503801;Textured Tie And Pocket Square Set;Draft;Ties;next;Adults-Men;Men;Blue;;;Fashion;Formal;2018;ONE,NEXTTIES00000090;1000.00;;;;<p>6cm blade width</p>;;;next,JIT;zerotype;;Winter;;No
2503799;KT01ST215933-Maroon-2503799;Striped Tie;Draft;Ties;next;Adults-Men;Men;Maroon;;;Fashion;Formal;2018;ONE,NEXTTIES00000089;800.00;;;;;;;next,JIT;zerotype;;Winter;;No
2503798;KT11ST238660-Black-2503798;Floral Bow Tie;Draft;Ties;next;Adults-Men;Men;Black;;;Fashion;Party;2018;ONE,NEXTTIES00000088;800.00;;;;;;;next,JIT;zerotype;;Winter;;No
2503797;KT01ST237986-Magenta-2503797;Super Skinny Tie;Draft;Ties;next;Adults-Men;Men;Magenta;;;Fashion;Formal;2018;ONE,NEXTTIES00000087;800.00;;;;;;;next,JIT;zerotype;;Winter;;No
2503796;KT01ST215940-Blue-2503796;Spot Tie;Draft;Ties;next;Adults-Men;Men;Blue;;;Fashion;Formal;2018;ONE,NEXTTIES00000086;800.00;;;;;;;next,JIT;zerotype;;Winter;;No
2503795;KT25ST234379-Navy Blue-2503795;Placement Floral Print Tie;Draft;Ties;next;Adults-Men;Men;Navy Blue;;;Fashion;Party;2018;ONE,NEXTTIES00000085;800.00;;;;;;;next,JIT;zerotype;;Winter;;No
2503794;KT01ST215934-Navy Blue-2503794;Spot Tie;Draft;Ties;next;Adults-Men;Men;Navy Blue;;;Fashion;Formal;2018;ONE,NEXTTIES00000084;800.00;;;;;;;next,JIT;zerotype;;Winter;;No
2503793;KT25ST234378-Black Coral-2503793;Placement Floral Print Tie;Draft;Ties;next;Adults-Men;Men;Black;Coral;;Fashion;Smart Casual;2018;ONE,NEXTTIES00000083;800.00;;;;;;;next,JIT;zerotype;;Winter;;No
2503792;KT11ST238776-Blue-2503792;Paisley Bow Tie;Draft;Ties;next;Adults-Men;Men;Blue;;;Fashion;Formal;2018;ONE,NEXTTIES00000082;800.00;;;;;;;next,JIT;zerotype;;Winter;;No
2503791;KT01ST238838-Peach-2503791;Fine Textured Tie;Draft;Ties;next;Adults-Men;Men;Peach;;;Fashion;Formal;2018;ONE,NEXTTIES00000081;1000.00;;;;<p>6cm blade width</p>;;;next,JIT;zerotype;;Winter;;No
2503790;KT11ST238775-Pink-2503790;Silk Floral Bow Tie;Draft;Ties;next;Adults-Men;Men;Pink;;;Fashion;Formal;2018;ONE,NEXTTIES00000080;1200.00;;;;;;;next,JIT;zerotype;;Winter;;No
2503789;KT05ST216204-Black-2503789;Sigture Italian Silk Textured Tie;Draft;Ties;next;Adults-Men;Men;Black;;;Fashion;Formal;2018;ONE,NEXTTIES00000079;2800.00;;;;;;;next,JIT;zerotype;;Winter;;No
2503788;KT11ST238774-Black-2503788;Velvet Bow Tie;Draft;Ties;next;Adults-Men;Men;Black;;;Fashion;Formal;2018;ONE,NEXTTIES00000078;800.00;;;;;;;next,JIT;zerotype;;Winter;;No
2503787;KT05ST238655-Navy Blue-2503787;Sigture Floral Tie;Draft;Ties;next;Adults-Men;Men;Navy Blue;;;Fashion;Smart Casual;2018;ONE,NEXTTIES00000077;2000.00;;;;;;;next,JIT;zerotype;;Winter;;No
2503786;KT01ST238839-Lavender-2503786;Silk Fine Textured Tie;Draft;Ties;next;Adults-Men;Men;Lavender;;;Fashion;Formal;2018;ONE,NEXTTIES00000076;1200.00;;;;<p>7cm blade width</p>;;;next,JIT;zerotype;;Winter;;No
2503785;KT05ST238654-Purple-2503785;Sigture Silk Paisley Tie;Draft;Ties;next;Adults-Men;Men;Purple;;;Fashion;Casual;2018;ONE,NEXTTIES00000075;2000.00;;;;<p>7cm blade width</p>;;;next,JIT;zerotype;;Winter;;No
2503783;KT01ST075515-Maroon-2503783;Textured Silk Tie;Draft;Ties;next;Adults-Men;Men;Maroon;;;Fashion;Formal;2018;ONE,NEXTTIES00000074;1200.00;;;;<p>7cm blade width</p>;;;next,JIT;zerotype;;Winter;;No
2503782;KT05ST239506-Purple-2503782;Sigture Dot Pattern Tie;Draft;Ties;next;Adults-Men;Men;Purple;;;Fashion;Casual;2018;ONE,NEXTTIES00000073;2000.00;;;;<p>7cm blade width</p>;;;next,JIT;zerotype;;Winter;;No
2503781;KT01ST239474-Maroon-2503781;Knit Tie;Draft;Ties;next;Adults-Men;Men;Maroon;;;Fashion;Casual;2018;ONE,NEXTTIES00000072;1000.00;;;;;;;next,JIT;zerotype;;Winter;;No
2503780;KT01ST177816-Blue-2503780;Pattern Tie;Draft;Ties;next;Adults-Men;Men;Blue;;;Fashion;Formal;2018;ONE,NEXTTIES00000071;800.00;;;;;;;next,JIT;zerotype;;Winter;;No
2503779;KT01ST238838-Sea Green-2503779;Fine Textured Tie;Draft;Ties;next;Adults-Men;Men;Sea Green;;;Fashion;Formal;2018;ONE,NEXTTIES00000070;1000.00;;;;<p>6cm blade width</p>;;;next,JIT;zerotype;;Winter;;No
2503778;KT02ST198472-Lavender-2503778;Pattern Occasion Tie And Pocket Square Set;Draft;Ties;next;Adults-Men;Men;Lavender;;;Fashion;Formal;2018;ONE,NEXTTIES00000069;1800.00;;;;<p>7cm blade width</p>;;;next,JIT;zerotype;;Winter;;No
