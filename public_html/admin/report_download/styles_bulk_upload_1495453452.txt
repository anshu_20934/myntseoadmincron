Style ID;Vendor Article Number;Vendor Article Name;Active Status;Article type;Brand;Age Group;Gender;Base Colour;Colour1;Colour2;Fashion Type;Usage;Year;Size - SKU code pair;MRP;Style Description;Style Note;Materials and Care Description;Size and Fit Description;Size Chart Path;Product Display Name;Tags;Product Type;Classification Brand;Season;Comments;Upload Images
1918293;BR91007BL-MMP-Black-1918293;Bern Black Color Genuine Leather Bi-Fold;Draft;Wallets;Bern;Adults-Men;Men;;;;Fashion;;2017;Onesize,BERNWLLT00000079;595.00;;;;;;;;zerotype;;Summer;;No
1918292;BR91007BR-MMP-Brown-1918292;Bern Brown Color Genuine Leather Bi-Fold;Draft;Wallets;Bern;Adults-Men;Men;;;;Fashion;;2017;Onesize,BERNWLLT00000078;595.00;;;;;;;;zerotype;;Summer;;No
1918291;BR91010BL-MMP-Black-1918291;Bern Black Color Genuine Leather Bi-Fold;Draft;Wallets;Bern;Adults-Men;Men;;;;Fashion;;2017;Onesize,BERNWLLT00000077;645.00;;;;;;;;zerotype;;Summer;;No
1918290;BR91010BR-MMP-Brown-1918290;Bern Brown Color Genuine Leather Bi-Fold;Draft;Wallets;Bern;Adults-Men;Men;;;;Fashion;;2017;Onesize,BERNWLLT00000076;645.00;;;;;;;;zerotype;;Summer;;No
1918289;BR91006BR-MMP-Brown-1918289;Bern Brown Color Genuine Leather Bi-Fold;Draft;Wallets;Bern;Adults-Men;Men;;;;Fashion;;2017;Onesize,BERNWLLT00000075;645.00;;;;;;;;zerotype;;Summer;;No
1918288;BR91006BL-MMP-Black-1918288;Bern Black Color Genuine Leather Bi-Fold;Draft;Wallets;Bern;Adults-Men;Men;;;;Fashion;;2017;Onesize,BERNWLLT00000074;645.00;;;;;;;;zerotype;;Summer;;No
1918287;BR91003BL-MMP-Black-1918287;Bern Black Color Genuine Leather Bi-Fold;Draft;Wallets;Bern;Adults-Men;Men;;;;Fashion;;2017;Onesize,BERNWLLT00000073;750.00;;;;;;;;zerotype;;Summer;;No
1918286;BR91009BR-MMP-Brown-1918286;Bern Brown Color Genuine Leather Tri-Fol;Draft;Wallets;Bern;Adults-Men;Men;;;;Fashion;;2017;Onesize,BERNWLLT00000072;595.00;;;;;;;;zerotype;;Summer;;No
1918285;BR91009BL-MMP-Black-1918285;Bern Black Color Genuine Leather Tri-Fol;Draft;Wallets;Bern;Adults-Men;Men;;;;Fashion;;2017;Onesize,BERNWLLT00000071;595.00;;;;;;;;zerotype;;Summer;;No
1918284;BR91008BR-MMP-Brown-1918284;Bern Brown Color Genuine Leather Unisex;Draft;Wallets;Bern;Adults-Unisex;Unisex;;;;Fashion;;2017;Onesize,BERNWLLT00000070;545.00;;;;;;;;zerotype;;Summer;;No
1918283;BR4219B-MJP-Beige-1918283;Bern Beige Color Semi-Formal PU Handbag;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000270;2045.00;;;;;;;;zerotype;;Summer;;No
1918282;BR4219T-MJP-Tan-1918282;Bern Tan Color Semi-Formal PU Handbag Fo;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000269;2045.00;;;;;;;;zerotype;;Summer;;No
1918281;BR4219CO-MJP-Coffee Brown-1918281;Bern Coffee Color Semi-Formal PU Handbag;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000268;2045.00;;;;;;;;zerotype;;Summer;;No
1918280;BR4219BL-MJP-Black-1918280;Bern Black Color Semi-Formal PU Handbag;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000267;2045.00;;;;;;;;zerotype;;Summer;;No
1918279;BR4219CH-MJP-Cherry-1918279;Bern Cherry  Color Semi-Formal PU Handba;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000266;2045.00;;;;;;;;zerotype;;Summer;;No
1918278;BR4211BL-MJP-Black-1918278;Bern Black  Color Semi-Formal PU Sling B;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000265;1525.00;;;;;;;;zerotype;;Summer;;No
1918277;BR4211R-MJP-Red-1918277;Bern Red Color Semi-Formal PU Sling Bag;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000264;1525.00;;;;;;;;zerotype;;Summer;;No
1918276;BR4163CABR-MJP-Camel-1918276;Bern Camel Brown and Brown Color Stylish;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000263;1675.00;;;;;;;;zerotype;;Summer;;No
1918275;BR4163BLR-MJP-black-1918275;Bern Black and Red Color Stylish PU Tote;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000262;1675.00;;;;;;;;zerotype;;Summer;;No
1918274;BR4165RS-MJP-Red-1918274;Bern Red and Silver Color PU Handbag For;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000261;1545.00;;;;;;;;zerotype;;Summer;;No
1918273;BR4165GB-MJP-Green-1918273;Bern Green and Black Color PU Handbag Fo;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000260;1545.00;;;;;;;;zerotype;;Summer;;No
1918272;BR4221BL-MJP-Black-1918272;Bern Black Color PU Sling Bag For Women;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000259;1195.00;;;;;;;;zerotype;;Summer;;No
1918271;BR4221GR-MJP-Green-1918271;Bern Green Color PU Sling Bag For Women;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000258;1195.00;;;;;;;;zerotype;;Summer;;No
1918270;BR4221O-MJP-Orange-1918270;Bern Orange Color PU Sling Bag For Women;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000257;1195.00;;;;;;;;zerotype;;Summer;;No
1918269;BR4221BU-MJP-Blue-1918269;Bern Blue Color PU Sling Bag For Women;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000256;1195.00;;;;;;;;zerotype;;Summer;;No
1918268;BR4221BO-MJP-Purple-1918268;Bern Light Purple Color PU Sling Bag For;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000255;1195.00;;;;;;;;zerotype;;Summer;;No
1918267;BR4170BLB-MJP-Black-1918267;Bern Black and Beige Color PU Handbag Fo;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000254;2045.00;;;;;;;;zerotype;;Summer;;No
1918266;BR4170GBL-MJP-Gold-1918266;Bern Gold and Black  Color PU Handbag Fo;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000253;2045.00;;;;;;;;zerotype;;Summer;;No
1918265;BR4170BT-MJP-Blue-1918265;Bern Blue and Tan Color PU Handbag For W;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000252;2045.00;;;;;;;;zerotype;;Summer;;No
1918264;BR4210BR-MJP-Brown-1918264;Bern Brown Color PU Handbag For Women;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000251;2045.00;;;;;;;;zerotype;;Summer;;No
1918263;BR4210CH-MJP-Maroon-1918263;Bern Cherry Color PU Handbag For Women;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000250;2045.00;;;;;;;;zerotype;;Summer;;No
1918262;BR4210BL-MJP-Black-1918262;Bern Black Brown Color PU Handbag For Wo;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000249;2045.00;;;;;;;;zerotype;;Summer;;No
1918261;BR4176GBL-MJP-Green-1918261;Bern Green and Black Color PU Handbag Fo;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000248;1445.00;;;;;;;;zerotype;;Summer;;No
1918260;BR4176BLC-MJP-Black-1918260;Bern Black and Camel Brown Color PU Hand;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000247;1445.00;;;;;;;;zerotype;;Summer;;No
1918259;BR4176BRT-MJP-Brown-1918259;Bern Brown and Tan Color PU Handbag For;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000246;1445.00;;;;;;;;zerotype;;Summer;;No
1918258;BR4223FL-MJP-Fluorescent Green-1918258;Bern Floroscent Green Color Semi-Formal;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000245;1945.00;;;;;;;;zerotype;;Summer;;No
1918257;BR4223BE-MJP-Beige-1918257;Bern Beige Color Semi-Formal PU Handbag;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000244;1945.00;;;;;;;;zerotype;;Summer;;No
1918256;BR4223R-MJP-Red-1918256;Bern Red Color Semi-Formal PU Handbag Fo;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000243;1945.00;;;;;;;;zerotype;;Summer;;No
1918255;BR4223T-MJP-Tan-1918255;Bern Tan Brown Color Semi-Formal PU Hand;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000242;1945.00;;;;;;;;zerotype;;Summer;;No
1918254;BR4223G-MJP-Green-1918254;Bern Green Color Semi-Formal PU Handbag;Draft;Handbags;Bern;Adults-Women;Women;;;;Fashion;;2017;Onesize,BERNHDBG00000241;1945.00;;;;;;;;zerotype;;Summer;;No
