Style ID;Vendor Article Number;Vendor Article Name;Active Status;Article type;Brand;Age Group;Gender;Base Colour;Colour1;Colour2;Fashion Type;Usage;Year;Size - SKU code pair;MRP;Style Description;Style Note;Materials and Care Description;Size and Fit Description;Size Chart Path;Product Display Name;Tags;Product Type;Classification Brand;Season;Comments;Upload Images
1805269;ComboRRRSS17008-Silver, Gold and Blue-1805269;Rubber Band Set;Draft;Hair Accessory;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSHRAC00000008;899.00;;;;;;;;zerotype;;Spring;;No
1805268;ComboCHHSS17007-Gold, White and Black-1805268;Clip and Hair Band Set;Draft;Hair Accessory;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSHRAC00000007;1099.00;;;;;;;;zerotype;;Spring;;No
1805267;ComboCHRSS17006-Gold-1805267;Clip, Hair Band and Rubber Band Set;Draft;Hair Accessory;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSHRAC00000006;999.00;;;;;;;;zerotype;;Spring;;No
1805262;ComboHHRSS17001-Gold, Black-1805262;Hair Band and Rubber Band Set;Draft;Hair Accessory;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSHRAC00000001;1099.00;;;;;;;;zerotype;;Spring;;No
1805242;ComboNRSS1700110-Gold, Navy-blue, Black-1805242;Necklace and Ring Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000194;899.00;;;;;;;;zerotype;;Spring;;No
1805241;ComboNNSS1700109-Silver-1805241;Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000193;999.00;;;;;;;;zerotype;;Spring;;No
1805240;ComboENSS1700108-Gold, Black-1805240;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000192;1099.00;;;;;;;;zerotype;;Spring;;No
1805237;ComboNNSS1700105-Silver, Blue-1805237;Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000189;999.00;;;;;;;;zerotype;;Spring;;No
1805235;ComboENSS1700103-Silver, Black-1805235;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000187;999.00;;;;;;;;zerotype;;Spring;;No
1805229;ComboEBSS170097-Gold, Blue-1805229;Earrings and Bracelet Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000181;899.00;;;;;;;;zerotype;;Spring;;No
1805223;ComboENSS170091-Black, Gold-1805223;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000175;1099.00;;;;;;;;zerotype;;Spring;;No
1805219;ComboEESS170087-Gold, Blue-1805219;Earrings Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000171;1199.00;;;;;;;;zerotype;;Spring;;No
1805210;ComboEBSS170078-Gold, White-1805210;Earrings and Bracelet Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000162;999.00;;;;;;;;zerotype;;Spring;;No
1805204;ComboNBSS170072-Gold-1805204;Necklace and Bracelet Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000156;1099.00;;;;;;;;zerotype;;Spring;;No
1805203;ComboBBSS170071-Gold, White-1805203;Bracelet Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000155;999.00;;;;;;;;zerotype;;Spring;;No
1805202;ComboEBSS170070-Gold, Silver-1805202;Earrings and Bracelet Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000154;999.00;;;;;;;;zerotype;;Spring;;No
1805200;ComboEBSS170068-Gold, Black-1805200;Necklace and Bracelet Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000152;999.00;;;;;;;;zerotype;;Spring;;No
1805195;ComboNBSS170063-Gold, White-1805195;Necklace and Bracelet Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000147;899.00;;;;;;;;zerotype;;Spring;;No
1805193;ComboENRSS170061-Gold-1805193;Earrings, Necklace and Ring Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000145;1099.00;;;;;;;;zerotype;;Spring;;No
1805192;ComboENRSS170060-Gold, Silver, White-1805192;Earrings, Necklace and Ring Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000144;1099.00;;;;;;;;zerotype;;Spring;;No
1805191;ComboENSS170059-Gold, Blue-1805191;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000143;1099.00;;;;;;;;zerotype;;Spring;;No
1805189;ComboENSS170057-Gold, Black-1805189;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000141;1099.00;;;;;;;;zerotype;;Spring;;No
1805188;ComboENSS170056-Gold, Blue-1805188;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000140;1099.00;;;;;;;;zerotype;;Spring;;No
1805185;ComboENSS170053-Gold, Black-1805185;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000137;1099.00;;;;;;;;zerotype;;Spring;;No
1805184;ComboENSS170052-Gold, Blue-1805184;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000136;1199.00;;;;;;;;zerotype;;Spring;;No
1805183;ComboENSS170051-White, Blue, Gold-1805183;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000135;1199.00;;;;;;;;zerotype;;Spring;;No
1805182;ComboENSS170050-Silver, Blue, Black-1805182;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000134;1099.00;;;;;;;;zerotype;;Spring;;No
1805181;ComboENSS170049-Gold, Black-1805181;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000133;1099.00;;;;;;;;zerotype;;Spring;;No
1805180;ComboENSS170048-Silver, Black-1805180;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000132;1199.00;;;;;;;;zerotype;;Spring;;No
1805179;ComboENSS170047-Gold, Black-1805179;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000131;1099.00;;;;;;;;zerotype;;Spring;;No
1805175;ComboENSS170043-Silver-1805175;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000127;999.00;;;;;;;;zerotype;;Spring;;No
1805171;ComboENSS170039-Silver, White-1805171;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000123;999.00;;;;;;;;zerotype;;Spring;;No
1805167;ComboENRSS170035-Silver-1805167;Earrings, Necklace and Ring Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000119;1099.00;;;;;;;;zerotype;;Spring;;No
1805165;ComboEENSS170033-Silver, Gold-1805165;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000117;999.00;;;;;;;;zerotype;;Spring;;No
1805164;ComboEENSS170032-Silver-1805164;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000116;999.00;;;;;;;;zerotype;;Spring;;No
1805163;ComboENRSS170031-Silver-1805163;Earrings, Necklace and Ring Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000115;1099.00;;;;;;;;zerotype;;Spring;;No
1805162;ComboEEESS170030-Gold, Silver-1805162;Earrings Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000114;899.00;;;;;;;;zerotype;;Spring;;No
1805161;ComboEENSS170029-Silver, White-1805161;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000113;999.00;;;;;;;;zerotype;;Spring;;No
1805160;ComboEENSS170028-Silver-1805160;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000112;999.00;;;;;;;;zerotype;;Spring;;No
1805158;ComboENRSS170026-Silver, Gold-1805158;Earrings, Necklace and Ring Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000110;1099.00;;;;;;;;zerotype;;Spring;;No
1805157;ComboENRSS170025-Silver-1805157;Earrings, Necklace and Ring Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000109;1199.00;;;;;;;;zerotype;;Spring;;No
1805156;ComboEENSS170024-Silver-1805156;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000108;999.00;;;;;;;;zerotype;;Spring;;No
1805154;ComboENRSS170022-Silver-1805154;Earrings, Necklace and Ring Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000106;1099.00;;;;;;;;zerotype;;Spring;;No
1805153;ComboEENSS170021-Silver-1805153;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000105;999.00;;;;;;;;zerotype;;Spring;;No
1805152;ComboEENSS170020-Silver-1805152;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000104;1099.00;;;;;;;;zerotype;;Spring;;No
1805151;ComboEENSS170019-Silver-1805151;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000103;1099.00;;;;;;;;zerotype;;Spring;;No
1805147;ComboEENSS170015-Gold-1805147;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000099;999.00;;;;;;;;zerotype;;Spring;;No
1805146;ComboENSS170014-Gold, Silver-1805146;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000098;1099.00;;;;;;;;zerotype;;Spring;;No
1805145;ComboENRSS170013-Gold, White-1805145;Earrings, Necklace and Ring Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000097;999.00;;;;;;;;zerotype;;Spring;;No
1805141;ComboENSS17009-Gold-1805141;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000093;899.00;;;;;;;;zerotype;;Spring;;No
1805140;ComboENSS17008-Gold-1805140;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000092;999.00;;;;;;;;zerotype;;Spring;;No
1805138;ComboENRSS17006-Gold, Silver, White-1805138;Earrings, Necklace and Ring Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000090;1199.00;;;;;;;;zerotype;;Spring;;No
1805135;ComboENSS17003-Gold-1805135;Earrings and Necklace Set;Draft;Jewellery Set;Globus;Adults-Women;Women;;;;Fashion;;2017;Onesize,GLBSJWST00000087;899.00;;;;;;;;zerotype;;Spring;;No
