Style ID;Vendor Article Number;Vendor Article Name;Active Status;Article type;Brand;Age Group;Gender;Base Colour;Colour1;Colour2;Fashion Type;Usage;Year;Size - SKU code pair;MRP;Style Description;Style Note;Materials and Care Description;Size and Fit Description;Size Chart Path;Product Display Name;Tags;Product Type;Classification Brand;Season;Comments;Upload Images
1944105;JUKE75TMIB-Blue-1944105;JUKE STROLLY 75 360 (CR.TEX) MIB;Draft;Trolley Bag;Aristocrat;Adults-Unisex;Unisex;Blue;;;Core;;2017;L,ATCTTRBA00000068;8900.00;;;;;;;;zerotype;;Spring;;No
1944104;JUKE75TMGP-Grey-1944104;JUKE STROLLY 75 360 (CR.TEX) MGP;Draft;Trolley Bag;Aristocrat;Adults-Unisex;Unisex;Grey;;;Core;;2017;L,ATCTTRBA00000067;8900.00;;;;;;;;zerotype;;Spring;;No
1944103;JUKE65TMIB-Blue-1944103;JUKE STROLLY 65 360 (CR.TEX) MIB;Draft;Trolley Bag;Aristocrat;Adults-Unisex;Unisex;Blue;;;Core;;2017;M,ATCTTRBA00000066;7300.00;;;;;;;;zerotype;;Spring;;No
1944102;JUKE65TMGP-Grey-1944102;JUKE STROLLY 65 360 (CR.TEX) MGP;Draft;Trolley Bag;Aristocrat;Adults-Unisex;Unisex;Grey;;;Core;;2017;M,ATCTTRBA00000065;7300.00;;;;;;;;zerotype;;Spring;;No
1944101;JUKE55TMIB-Blue-1944101;JUKE STROLLY 55 360 (CR.TEX) MIB;Draft;Trolley Bag;Aristocrat;Adults-Unisex;Unisex;Blue;;;Core;;2017;S,ATCTTRBA00000064;5600.00;;;;;;;;zerotype;;Spring;;No
1944100;JUKE55TMGP-Grey-1944100;JUKE STROLLY 55 360 (CR.TEX) MGP;Draft;Trolley Bag;Aristocrat;Adults-Unisex;Unisex;Grey;;;Core;;2017;S,ATCTTRBA00000063;5600.00;;;;;;;;zerotype;;Spring;;No
1944099;STNILW66PPL-Purple-1944099;NILE 4W EXP STROLLY 66 PURPLE;Draft;Trolley Bag;Aristocrat;Adults-Unisex;Unisex;Purple;;;Core;;2017;M,ATCTTRBA00000062;7200.00;;;;;;;;zerotype;;Spring;;No
1944098;STNILW66BLK-Black-1944098;NILE 4W EXP STROLLY 66 BLACK;Draft;Trolley Bag;Aristocrat;Adults-Unisex;Unisex;Black;;;Core;;2017;M,ATCTTRBA00000061;7200.00;;;;;;;;zerotype;;Spring;;No
1944097;STNILW54PPL-Purple-1944097;NILE 4W EXP STROLLY 54 PURPLE;Draft;Trolley Bag;Aristocrat;Adults-Unisex;Unisex;Purple;;;Core;;2017;S,ATCTTRBA00000060;5800.00;;;;;;;;zerotype;;Spring;;No
1944096;STNILW54BLK-Black-1944096;NILE 4W EXP STROLLY 54 BLACK;Draft;Trolley Bag;Aristocrat;Adults-Unisex;Unisex;Black;;;Core;;2017;S,ATCTTRBA00000059;5800.00;;;;;;;;zerotype;;Spring;;No
1944095;LPBPURBPDGY-Dark Grey-1944095;URBAN PRO LAPTOP BACKPACK DGY;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Grey;;;Core;;2017;Onesize,ATCTBAPK00000051;2590.00;;;;;;;;zerotype;;Summer;;No
1944094;LPBPURBPBLK-Black-1944094;URBAN PRO LAPTOP BACKPACK BLK;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Black;;;Core;;2017;Onesize,ATCTBAPK00000050;2590.00;;;;;;;;zerotype;;Summer;;No
1944093;LPBPGUS2GRN-Green-1944093;GUSTO 2 LAPTOP BACKPACK GREEN;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Green;;;Core;;2017;Onesize,ATCTBAPK00000049;2390.00;;;;;;;;zerotype;;Summer;;No
1944092;LPBPGUS2BLK-Black-1944092;GUSTO 2 LAPTOP BACKPACK BLACK;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Black;;;Core;;2017;Onesize,ATCTBAPK00000048;2390.00;;;;;;;;zerotype;;Summer;;No
1944091;LPBPGUS2BLU-Blue-1944091;GUSTO 2 LAPTOP BACKPACK BLUE;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Blue;;;Core;;2017;Onesize,ATCTBAPK00000047;2390.00;;;;;;;;zerotype;;Summer;;No
1944090;LPBPGUS2GRY-Grey-1944090;GUSTO 2 LAPTOP BACKPACK GREY;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Grey;;;Core;;2017;Onesize,ATCTBAPK00000046;2390.00;;;;;;;;zerotype;;Summer;;No
1944089;LPBPGUS1GRN-Green-1944089;GUSTO 1 LAPTOP BACKPACK GREEN;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Green;;;Core;;2017;Onesize,ATCTBAPK00000045;1990.00;;;;;;;;zerotype;;Summer;;No
1944088;LPBPGUS1BLU-Blue-1944088;GUSTO 1 LAPTOP BACKPACK BLUE;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Blue;;;Core;;2017;Onesize,ATCTBAPK00000044;1990.00;;;;;;;;zerotype;;Summer;;No
1944087;LPBPGUS1RED-Red-1944087;GUSTO 1 LAPTOP BACKPACK RED;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Red;;;Core;;2017;Onesize,ATCTBAPK00000043;1990.00;;;;;;;;zerotype;;Summer;;No
1944086;BPREVO2BLK-Black-1944086;REVO 2 BACKPACK BLACK;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Black;;;Core;;2017;Onesize,ATCTBAPK00000042;1790.00;;;;;;;;zerotype;;Summer;;No
1944085;BPREVO2BLU-Blue-1944085;REVO 2 BACKPACK BLUE;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Blue;;;Core;;2017;Onesize,ATCTBAPK00000041;1790.00;;;;;;;;zerotype;;Summer;;No
1944084;BPREVO2RED-Red-1944084;REVO 2 BACKPACK RED;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Red;;;Core;;2017;Onesize,ATCTBAPK00000040;1790.00;;;;;;;;zerotype;;Summer;;No
1944083;BPREVO1BLU-Blue-1944083;REVO 1 BACKPACK BLUE;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Blue;;;Core;;2017;Onesize,ATCTBAPK00000039;1660.00;;;;;;;;zerotype;;Summer;;No
1944082;BPREVO1RED-Red-1944082;REVO 1 BACKPACK RED;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Red;;;Core;;2017;Onesize,ATCTBAPK00000038;1660.00;;;;;;;;zerotype;;Summer;;No
1944081;LPBPZIN3SBL-Sky Blue-1944081;ZING 3 LAPTOP BACKPACK SKY BLUE;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Blue;;;Core;;2017;Onesize,ATCTBAPK00000037;1570.00;;;;;;;;zerotype;;Summer;;No
1944080;LPBPZIN3BLK-Black-1944080;ZING 3 LAPTOP BACKPACK BLACK;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Black;;;Core;;2017;Onesize,ATCTBAPK00000036;1570.00;;;;;;;;zerotype;;Summer;;No
1944079;LPBPZIN3BLU-Blue-1944079;ZING 3 LAPTOP BACKPACK BLUE;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Blue;;;Core;;2017;Onesize,ATCTBAPK00000035;1570.00;;;;;;;;zerotype;;Summer;;No
1944078;LPBPZIN3RED-Red-1944078;ZING 3 LAPTOP BACKPACK RED;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Red;;;Core;;2017;Onesize,ATCTBAPK00000034;1570.00;;;;;;;;zerotype;;Summer;;No
1944077;BPZING2PPL-Purple-1944077;ZING 2 BACKPACK PURPLE;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Purple;;;Core;;2017;Onesize,ATCTBAPK00000033;1300.00;;;;;;;;zerotype;;Summer;;No
1944076;BPZING2FWN-Fawn-1944076;ZING 2 BACKPACK FAWN;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Beige;;;Core;;2017;Onesize,ATCTBAPK00000032;1300.00;;;;;;;;zerotype;;Summer;;No
1944075;BPZING2GRY-Grey-1944075;ZING 2 BACKPACK GREY;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Grey;;;Core;;2017;Onesize,ATCTBAPK00000031;1300.00;;;;;;;;zerotype;;Summer;;No
1944074;BPZING1GRN-Green-1944074;ZING 1 BACKPACK GREEN;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Green;;;Core;;2017;Onesize,ATCTBAPK00000030;1140.00;;;;;;;;zerotype;;Summer;;No
1944073;BPZING1BLK-Black-1944073;ZING 1 BACKPACK BLACK;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Black;;;Core;;2017;Onesize,ATCTBAPK00000029;1140.00;;;;;;;;zerotype;;Summer;;No
1944072;BPZING1FWN-Fawn-1944072;ZING 1 BACKPACK FAWN;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Beige;;;Core;;2017;Onesize,ATCTBAPK00000028;1140.00;;;;;;;;zerotype;;Summer;;No
1944071;BPZING1BLU-Blue-1944071;ZING 1 BACKPACK BLUE;Draft;Backpacks;Aristocrat;Adults-Unisex;Unisex;Blue;;;Core;;2017;Onesize,ATCTBAPK00000027;1140.00;;;;;;;;zerotype;;Summer;;No
1944070;DFTVOL55BLU-Blue-1944070;VOLT DUFFLE TROLLEY 55 BLUE;Draft;Duffel Bag;Aristocrat;Adults-Unisex;Unisex;Blue;;;Core;;2017;S,ATCTDFBG00000002;3200.00;;;;;;;;zerotype;;Summer;;No
1944069;DFTVOL55PPL-Purple-1944069;VOLT DUFFLE TROLLEY 55 PURPLE;Draft;Duffel Bag;Aristocrat;Adults-Unisex;Unisex;Purple;;;Core;;2017;S,ATCTDFBG00000001;3200.00;;;;;;;;zerotype;;Summer;;No
