Style ID;Vendor Article Number;Vendor Article Name;Active Status;Article type;Brand;Age Group;Gender;Base Colour;Colour1;Colour2;Fashion Type;Usage;Year;Size - SKU code pair;MRP;Style Description;Style Note;Materials and Care Description;Size and Fit Description;Size Chart Path;Product Display Name;Tags;Product Type;Classification Brand;Season;Comments;Upload Images
1940050;TT3311ABlue-Blue-1940050;Toteteca Shoulder Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Blue;;;Fashion;;2017;Onesize,TTCAHDBG00000668;1899.00;;;;;;;;zerotype;;Fall;;No
1940049;TT3268Maroon-Maroon-1940049;Toteteca Shoulder Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Maroon;;;Fashion;;2017;Onesize,TTCAHDBG00000667;1999.00;;;;;;;;zerotype;;Fall;;No
1940048;TT3103ATan-Tan-1940048;Toteteca Shoulder Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Tan;;;Fashion;;2017;Onesize,TTCAHDBG00000666;1899.00;;;;;;;;zerotype;;Fall;;No
1940046;TT3648Peach-Peach-1940046;Toteteca Sling Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Peach;;;Fashion;;2017;Onesize,TTCAHDBG00000665;2400.00;;;;;;;;zerotype;;Fall;;No
1940045;TT3641Pink-Pink-1940045;Toteteca Sling Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Pink;;;Fashion;;2017;Onesize,TTCAHDBG00000664;1899.00;;;;;;;;zerotype;;Fall;;No
1940044;TT3641Maroon-Maroon-1940044;Toteteca Sling Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Maroon;;;Fashion;;2017;Onesize,TTCAHDBG00000663;1899.00;;;;;;;;zerotype;;Fall;;No
1940043;TT3641LimeGreen-Lime Green-1940043;Toteteca Sling Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Lime Green;;;Fashion;;2017;Onesize,TTCAHDBG00000662;1899.00;;;;;;;;zerotype;;Fall;;No
1940042;TT3641Blue-Blue-1940042;Toteteca Sling Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Blue;;;Fashion;;2017;Onesize,TTCAHDBG00000661;1899.00;;;;;;;;zerotype;;Fall;;No
1940041;TT3640Offwhite-Off White-1940041;Toteteca Shoulder Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Off White;;;Fashion;;2017;Onesize,TTCAHDBG00000660;2800.00;;;;;;;;zerotype;;Fall;;No
1940040;TT3640Navy-Navy Blue-1940040;Toteteca Shoulder Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Navy Blue;;;Fashion;;2017;Onesize,TTCAHDBG00000659;2800.00;;;;;;;;zerotype;;Fall;;No
1940039;TT3640Green-Green-1940039;Toteteca Shoulder Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Green;;;Fashion;;2017;Onesize,TTCAHDBG00000658;2800.00;;;;;;;;zerotype;;Fall;;No
1940038;TT3619Red-Red-1940038;Toteteca Sling Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Red;;;Fashion;;2017;Onesize,TTCAHDBG00000657;1699.00;;;;;;;;zerotype;;Fall;;No
1940036;TT3604Tan-Tan-1940036;Toteteca Shoulder Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Tan;;;Fashion;;2017;Onesize,TTCAHDBG00000656;1799.00;;;;;;;;zerotype;;Fall;;No
1940035;TT3604Grey-Grey-1940035;Toteteca Shoulder Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Grey;;;Fashion;;2017;Onesize,TTCAHDBG00000655;1799.00;;;;;;;;zerotype;;Fall;;No
1940034;TT3604Khaki-Khaki-1940034;Toteteca Shoulder Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Khaki;;;Fashion;;2017;Onesize,TTCAHDBG00000654;1799.00;;;;;;;;zerotype;;Fall;;No
1940033;TT3594Tan-Tan-1940033;Toteteca Sling Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Tan;;;Fashion;;2017;Onesize,TTCAHDBG00000653;1799.00;;;;;;;;zerotype;;Fall;;No
1940032;TT3571Tan-Tan-1940032;Toteteca Shoulder Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Tan;;;Fashion;;2017;Onesize,TTCAHDBG00000652;1799.00;;;;;;;;zerotype;;Fall;;No
1940031;TT3571Khaki-Khaki-1940031;Toteteca Shoulder Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Khaki;;;Fashion;;2017;Onesize,TTCAHDBG00000651;1799.00;;;;;;;;zerotype;;Fall;;No
1940030;TT3571Grey-Grey-1940030;Toteteca Shoulder Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Grey;;;Fashion;;2017;Onesize,TTCAHDBG00000650;1799.00;;;;;;;;zerotype;;Fall;;No
1940029;TT3554White-White-1940029;Toteteca Sling Bag;Draft;Handbags;Toteteca;Adults-Women;Women;White;;;Fashion;;2017;Onesize,TTCAHDBG00000649;1699.00;;;;;;;;zerotype;;Fall;;No
1940028;TT3525Tan-Tan-1940028;Toteteca Shoulder Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Tan;;;Fashion;;2017;Onesize,TTCAHDBG00000648;2800.00;;;;;;;;zerotype;;Fall;;No
1940026;TT3516Brown-Brown-1940026;Toteteca Sling Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Brown;;;Fashion;;2017;Onesize,TTCAHDBG00000647;1699.00;;;;;;;;zerotype;;Fall;;No
1940025;TT3516Black-Black-1940025;Toteteca Sling Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Black;;;Fashion;;2017;Onesize,TTCAHDBG00000646;1699.00;;;;;;;;zerotype;;Fall;;No
1940024;TT3470Lazer-Blue-1940024;Toteteca Shoulder Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Blue;;;Fashion;;2017;Onesize,TTCAHDBG00000645;2199.00;;;;;;;;zerotype;;Fall;;No
1940023;TT3470Bow-Blue-1940023;Toteteca Shoulder Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Blue;;;Fashion;;2017;Onesize,TTCAHDBG00000644;2199.00;;;;;;;;zerotype;;Fall;;No
1940022;TT3337Black-Black-1940022;Toteteca Sling Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Black;;;Fashion;;2017;Onesize,TTCAHDBG00000643;1899.00;;;;;;;;zerotype;;Fall;;No
1940021;TT3241BBrown-Brown-1940021;Toteteca Sling Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Brown;;;Fashion;;2017;Onesize,TTCAHDBG00000642;1699.00;;;;;;;;zerotype;;Fall;;No
1940020;TT3108AMustard-Mustard-1940020;Toteteca Sling Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Mustard;;;Fashion;;2017;Onesize,TTCAHDBG00000641;1799.00;;;;;;;;zerotype;;Fall;;No
1940019;TT3108ABlue-Blue-1940019;Toteteca Sling Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Blue;;;Fashion;;2017;Onesize,TTCAHDBG00000640;1799.00;;;;;;;;zerotype;;Fall;;No
1940018;TT2899Brown-Brown-1940018;Toteteca Sling Bag;Draft;Handbags;Toteteca;Adults-Women;Women;Brown;;;Fashion;;2017;Onesize,TTCAHDBG00000639;1599.00;;;;;;;;zerotype;;Fall;;No
