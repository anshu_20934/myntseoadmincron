Style ID;Vendor Article Number;Vendor Article Name;Active Status;Article type;Brand;Age Group;Gender;Base Colour;Colour1;Colour2;Fashion Type;Usage;Year;Size - SKU code pair;MRP;Style Description;Style Note;Materials and Care Description;Size and Fit Description;Size Chart Path;Product Display Name;Tags;Product Type;Classification Brand;Season;Comments;Upload Images
1942695;O-MY17HH0022435-Blue-1942695;Ornate Flower Hand Harness;Draft;Bracelet;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABABRCT00000300;799.00;;;;;;;;zerotype;;Summer;;No
1942694;O-MY17BR0022434-Teal-1942694;Teal Chevron Bracelet;Draft;Bracelet;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABABRCT00000299;699.00;;;;;;;;zerotype;;Summer;;No
1942693;O-MY17BR0022428-Green-1942693;Polka Bracelet;Draft;Bracelet;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABABRCT00000298;799.00;;;;;;;;zerotype;;Summer;;No
1942692;O-MY17BR0022427-Orange-1942692;Tango Bracelet;Draft;Bracelet;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABABRCT00000297;749.00;;;;;;;;zerotype;;Summer;;No
1942691;O-MA17ER0022352-White-1942691;White Tassel Gold Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000954;1099.00;;;;;;;;zerotype;;Summer;;No
1942690;O-MA17ER0022361-Multi-1942690;Multi-Colour Dangler Earring;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000953;999.00;;;;;;;;zerotype;;Summer;;No
1942689;O-MA17ER0022317-Gold-1942689;Colourful Blossom Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000952;1199.00;;;;;;;;zerotype;;Summer;;No
1942688;O-MA17ER0022347-Gold-1942688;Gold Boutique Earring;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000951;799.00;;;;;;;;zerotype;;Summer;;No
1942686;O-MA17ER0022354-Pink-1942686;Baby Pink Tassel Gold Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000950;1099.00;;;;;;;;zerotype;;Summer;;No
1942685;O-MA17ER0022308-Blue-1942685;Pom-Pom Hoop Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000949;949.00;;;;;;;;zerotype;;Summer;;No
1942684;O-MA17ER0022316-Peach-1942684;Peach Blossom Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000948;1199.00;;;;;;;;zerotype;;Summer;;No
1942683;O-MA17ER0022358-Purple-1942683;Dark Pink Tassel Gold Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000947;1099.00;;;;;;;;zerotype;;Summer;;No
1942682;O-MA17ER0022356-Green-1942682;Green Tassel Gold Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000946;1099.00;;;;;;;;zerotype;;Summer;;No
1942681;O-MA17ER0022304-Red-1942681;Pom-Pom Red Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000945;949.00;;;;;;;;zerotype;;Summer;;No
1942680;O-MA17ER0022305-Blue-1942680;Spherical Blue Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000944;1099.00;;;;;;;;zerotype;;Summer;;No
1942679;O-MA17ER0022355-Blue-1942679;Sea Blue Tassel Gold Earrrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000943;1099.00;;;;;;;;zerotype;;Summer;;No
1942678;O-MA17ER0022306-Peach-1942678;Peach Ball Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000942;1099.00;;;;;;;;zerotype;;Summer;;No
1942677;O-MA17ER0022353-Yellow-1942677;Yellow Tassel Gold Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000941;1099.00;;;;;;;;zerotype;;Summer;;No
1942676;O-MA17ER0022310-Blue-1942676;Undivided Spehrical Silver Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000940;1099.00;;;;;;;;zerotype;;Summer;;No
1942675;O-MA17ER0022307-Silver-1942675;Disco Ball Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000939;1099.00;;;;;;;;zerotype;;Summer;;No
1942674;O-MA17ER0022309-Gold-1942674;Undivided Spehrical Golden Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000938;1099.00;;;;;;;;zerotype;;Summer;;No
1942673;O-MA17ER0022357-Pink-1942673;Pink Tassel Gold Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000937;1099.00;;;;;;;;zerotype;;Summer;;No
1942672;O-MA17ER0022320-Gold-1942672;Jelly Translucent Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000936;999.00;;;;;;;;zerotype;;Summer;;No
1942671;O-MY17ER0022432-Orange-1942671;Quadrille Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000935;999.00;;;;;;;;zerotype;;Summer;;No
1942670;O-MY17ER0022431-Orange-1942670;Flamenco Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000934;1099.00;;;;;;;;zerotype;;Summer;;No
1942669;O-MY17ER0022430-Green-1942669;Aster Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000933;999.00;;;;;;;;zerotype;;Summer;;No
1942668;O-MY17ER0022423-Green-1942668;Macarena Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000932;899.00;;;;;;;;zerotype;;Summer;;No
1942667;O-MY17ER0022422-Multi-1942667;Waltz Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000931;899.00;;;;;;;;zerotype;;Summer;;No
1942666;O-MA17ER0022421-Yellow-1942666;Samba Earrings;Draft;Earrings;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABAEARR00000930;999.00;;;;;;;;zerotype;;Summer;;No
1942665;O-MA17RN0022321-Gold-1942665;Triad Rings;Draft;Ring;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABARING00000270;799.00;;;;;;;;zerotype;;Summer;;No
1942664;O-MA17NL0022340-Multi-1942664;Colouful Handcrafted Choker Necklace;Draft;Necklace and Chains;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABANECK00000617;1199.00;;;;;;;;zerotype;;Summer;;No
1942663;O-MA17NL0022359-Gold-1942663;Five Layered Gold Wrapped Choker Necklace;Draft;Necklace and Chains;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABANECK00000616;1499.00;;;;;;;;zerotype;;Summer;;No
1942662;O-MA17NL0022319-Gold-1942662;Colossal Gold Choker Necklaces;Draft;Necklace and Chains;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABANECK00000615;1399.00;;;;;;;;zerotype;;Summer;;No
1942661;O-MY17NL0022433-Gold-1942661;Crystal Tassel Necklace;Draft;Necklace and Chains;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABANECK00000614;799.00;;;;;;;;zerotype;;Summer;;No
1942660;O-MY17NL0022426-Multi-1942660;Cha-cha Tassel Necklace;Draft;Necklace and Chains;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABANECK00000613;899.00;;;;;;;;zerotype;;Summer;;No
1942659;O-MY17NL0022425-Red-1942659;Kabuki Necklace;Draft;Necklace and Chains;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABANECK00000612;899.00;;;;;;;;zerotype;;Summer;;No
1942658;O-MY17NL0022424-Pink-1942658;Salsa Tassel Necklace;Draft;Necklace and Chains;Pipa Bella;Adults-Women;Women;;;;Fashion;;2017;Onesize,PABANECK00000611;999.00;;;;;;;;zerotype;;Summer;;No
