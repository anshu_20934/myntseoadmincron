Style ID;Vendor Article Number;Vendor Article Name;Active Status;Article type;Brand;Age Group;Gender;Base Colour;Colour1;Colour2;Fashion Type;Usage;Year;Size - SKU code pair;MRP;Style Description;Style Note;Materials and Care Description;Size and Fit Description;Size Chart Path;Product Display Name;Tags;Product Type;Classification Brand;Season;Comments;Upload Images
1852023;ISA-119242-Multi-1852023;ID PERFECT BLUSH BRUSH;Draft;Makeup Brushes;IsaDora;Adults-Women;Women;;;;Core;;2017;Onesize,ISDRMPBS00000002;1050.00;;;;;;;;zerotype;;Spring;;No
1852022;ISA-119230-Multi-1852022;ID FACE SCULPTING BRUSH;Draft;Makeup Brushes;IsaDora;Adults-Women;Women;;;;Core;;2017;Onesize,ISDRMPBS00000001;1225.00;;;;;;;;zerotype;;Spring;;No
1852021;ISA-122460-Nude-1852021;ID EYE COLOR BAR 60 NUDE ESSENTIALS 5GM;Draft;Eyeshadow;IsaDora;Adults-Women;Women;;;;Core;;2017;5GM,ISDREYSD00000001;1450.00;;;;;;;;zerotype;;Spring;;No
1852020;ISA-111738-Pink-1852020;ID MULTI VIT GLO 38 PINK BERRIES 7ML;Draft;Lip Gloss;IsaDora;Adults-Women;Women;;;;Core;;2017;7ML,ISDRLPGS00000001;1325.00;;;;;;;;zerotype;;Spring;;No
1852019;ISA-121368-STD-Multi-1852019;ID SCULPTING LIP LINER WP 68 RUM RAISIN;Draft;Lip Liner;IsaDora;Adults-Women;Women;;;;Core;;2017;Onesize,ISDRLPLN00000006;925.00;;;;;;;;zerotype;;Spring;;No
1852018;ISA-111436-red-1852018;ID PERF LIPLINER 36 RUBY RED 1.2GM;Draft;Lip Liner;IsaDora;Adults-Women;Women;;;;Core;;2017;1.2GM,ISDRLPLN00000005;850.00;;;;;;;;zerotype;;Spring;;No
1852017;ISA-111432-Orange-1852017;ID PERF LIPLINER 32 MANDARINE 1.2GM;Draft;Lip Liner;IsaDora;Adults-Women;Women;;;;Core;;2017;1.2GM,ISDRLPLN00000004;850.00;;;;;;;;zerotype;;Spring;;No
1852016;ISA-111431-Red-1852016;ID PERF LIPLINER 31 PRIME RED 1.2GM;Draft;Lip Liner;IsaDora;Adults-Women;Women;;;;Core;;2017;1.2GM,ISDRLPLN00000003;850.00;;;;;;;;zerotype;;Spring;;No
1852015;ISA-111428-Nude-1852015;ID PERF LIPLINER 28 NUDE SKIN 1.2GM;Draft;Lip Liner;IsaDora;Adults-Women;Women;;;;Core;;2017;1.2GM,ISDRLPLN00000002;850.00;;;;;;;;zerotype;;Spring;;No
1852014;ISA-111425-Peach-1852014;ID PERF LIPLINER 25 PEACH MELBA 1.2GM;Draft;Lip Liner;IsaDora;Adults-Women;Women;;;;Core;;2017;1.2GM,ISDRLPLN00000001;850.00;;;;;;;;zerotype;;Spring;;No
1852013;ISA-123860-black-1852013;ID PER CONT KAJAL WP 60 BLACK 1.2GM;Draft;Kajal and Eye Pencil;IsaDora;Adults-Women;Women;;;;Core;;2017;1.2GM,ISDRKEPL00000004;925.00;;;;;;;;zerotype;;Spring;;No
1852012;ISA-123839-grey-1852012;ID PER CONT KAJAL WP 39 DEEP GREY 1.2GM;Draft;Kajal and Eye Pencil;IsaDora;Adults-Women;Women;;;;Core;;2017;1.2GM,ISDRKEPL00000003;925.00;;;;;;;;zerotype;;Spring;;No
1852011;ISA-113721-dark brown-1852011;ID EYB PEN 21 DARK BROWN 1.1GM;Draft;Kajal and Eye Pencil;IsaDora;Adults-Women;Women;;;;Core;;2017;1.1GM,ISDRKEPL00000002;825.00;;;;;;;;zerotype;;Spring;;No
1852010;ISA-113720-black-1852010;ID EYB PEN 20 BLACK 1.1GM;Draft;Kajal and Eye Pencil;IsaDora;Adults-Women;Women;;;;Core;;2017;1.1GM,ISDRKEPL00000001;825.00;;;;;;;;zerotype;;Spring;;No
1852008;ISA-122920-Black-1852008;ID B UP WP MAS EX VOL 20 BLACK 12ML;Draft;Mascara;IsaDora;Adults-Women;Women;;;;Core;;2017;12ML,ISDRMSCR00000004;1290.00;;;;;;;;zerotype;;Spring;;No
1852007;ISA-122905-Royal blue-1852007;ID B UP MAS EX VOL 05 ROYAL BLUE 12ML;Draft;Mascara;IsaDora;Adults-Women;Women;;;;Core;;2017;12ML,ISDRMSCR00000003;1290.00;;;;;;;;zerotype;;Spring;;No
1852006;ISA-122904-Navy blue-1852006;ID B UP MAS EX VOL 04 NAVY BLUE 12ML;Draft;Mascara;IsaDora;Adults-Women;Women;;;;Core;;2017;12ML,ISDRMSCR00000002;1290.00;;;;;;;;zerotype;;Spring;;No
1852005;ISA-122901-Jet Black-1852005;ID B UP MAS EX VOL 01 SUPER BLACK 12ML;Draft;Mascara;IsaDora;Adults-Women;Women;;;;Core;;2017;12ML,ISDRMSCR00000001;1290.00;;;;;;;;zerotype;;Spring;;No
1852004;ISA-212176-rose-1852004;ID PERF MOIST LS 176 BOHEMIAN ROSE 4.5GM;Draft;Lipstick;IsaDora;Adults-Women;Women;;;;Fashion;;2017;4.5GM,ISDRLPST00000008;1190.00;;;;;;;;zerotype;;Spring;;No
1852003;ISA-212168-coral-1852003;ID PERF MOIST LS 168 CORAL CREAM 4.5GM;Draft;Lipstick;IsaDora;Adults-Women;Women;;;;Fashion;;2017;4.5GM,ISDRLPST00000007;1190.00;;;;;;;;zerotype;;Spring;;No
1852002;ISA-212165-pink-1852002;ID PERF MOIST LS 165 HONEYSUCKLE 4.5GM;Draft;Lipstick;IsaDora;Adults-Women;Women;;;;Fashion;;2017;4.5GM,ISDRLPST00000006;1190.00;;;;;;;;zerotype;;Spring;;No
1852001;ISA-121870-pink-1852001;ID TWIST-UP MATT LS 70 VINTAGE PINK 3.3G;Draft;Lipstick;IsaDora;Adults-Women;Women;;;;Fashion;;2017;3.3GM,ISDRLPST00000005;1290.00;;;;;;;;zerotype;;Spring;;No
1852000;ISA-121866-Purple-1852000;ID TWIST-UP MATT LS 66 PURPLE PRUNE 3.3G;Draft;Lipstick;IsaDora;Adults-Women;Women;;;;Fashion;;2017;3.3GM,ISDRLPST00000004;1290.00;;;;;;;;zerotype;;Spring;;No
1851998;ISA-121862-red-1851998;ID TWIST-UP MATT LS 62 RAVING RED 3.3GM;Draft;Lipstick;IsaDora;Adults-Women;Women;;;;Fashion;;2017;3.3GM,ISDRLPST00000003;1290.00;;;;;;;;zerotype;;Spring;;No
1851997;ISA-111811-pink-1851997;ID TWIST-UP GL STI 11 POPPY PEONY 3.3GM;Draft;Lipstick;IsaDora;Adults-Women;Women;;;;Fashion;;2017;3.3GM,ISDRLPST00000002;1290.00;;;;;;;;zerotype;;Spring;;No
1851996;ISA-111802-BROWN-1851996;ID TWIST-UP GL STI 02 BISCUIT 3.3GM;Draft;Lipstick;IsaDora;Adults-Women;Women;;;;Fashion;;2017;3.3GM,ISDRLPST00000001;1290.00;;;;;;;;zerotype;;Spring;;No
1851995;ISA-114530-Beige-1851995;ID 16H ACT MO MUP 30 OPAL BEIGE 30ML;Draft;Foundation and Primer;IsaDora;Adults-Women;Women;;;;Core;;2017;30ML,ISDRFDPR00000001;1475.00;;;;;;;;zerotype;;Spring;;No
1851994;ISA-124320-bronze-1851994;ID BB CREAM 20 BRONZER 35ML;Draft;BB and CC Cream;IsaDora;Adults-Women;Women;;;;Core;;2017;35ML,ISDRBCCM00000003;1450.00;;;;;;;;zerotype;;Spring;;No
1851993;ISA-124316-almond beige-1851993;ID BB CREAM 16 ALMOND BEIGE 35ML;Draft;BB and CC Cream;IsaDora;Adults-Women;Women;;;;Core;;2017;35ML,ISDRBCCM00000002;1450.00;;;;;;;;zerotype;;Spring;;No
1851991;ISA-124314-cool beige-1851991;ID BB CREAM 14 COOL BEIGE 35ML;Draft;BB and CC Cream;IsaDora;Adults-Women;Women;;;;Core;;2017;35ML,ISDRBCCM00000001;1450.00;;;;;;;;zerotype;;Spring;;No
