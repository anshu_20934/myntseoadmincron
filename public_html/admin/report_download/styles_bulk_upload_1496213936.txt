Style ID;Vendor Article Number;Vendor Article Name;Active Status;Article type;Brand;Age Group;Gender;Base Colour;Colour1;Colour2;Fashion Type;Usage;Year;Size - SKU code pair;MRP;Style Description;Style Note;Materials and Care Description;Size and Fit Description;Size Chart Path;Product Display Name;Tags;Product Type;Classification Brand;Season;Comments;Upload Images
1940150;VV3197Black-Black-1940150;Viva Functional Crossbody;Draft;Handbags;VIVA;Adults-Women;Women;Black;;;Fashion;;2017;Onesize,VIVAHDBG00000059;2999.00;;;;;;;;zerotype;;Fall;;No
1940149;VV3197Maroon-Maroon-1940149;Viva Functional Crossbody;Draft;Handbags;VIVA;Adults-Women;Women;Maroon;;;Fashion;;2017;Onesize,VIVAHDBG00000058;2799.00;;;;;;;;zerotype;;Fall;;No
1940148;VV3197Beige-Beige-1940148;Viva Functional Crossbody;Draft;Handbags;VIVA;Adults-Women;Women;Beige;;;Fashion;;2017;Onesize,VIVAHDBG00000057;2799.00;;;;;;;;zerotype;;Fall;;No
1940147;VV3183Beige-Beige-1940147;Viva Functional Shoulder;Draft;Handbags;VIVA;Adults-Women;Women;Beige;;;Fashion;;2017;Onesize,VIVAHDBG00000056;3699.00;;;;;;;;zerotype;;Fall;;No
1940146;VV3183Brown-Brown-1940146;Viva Functional Shoulder;Draft;Handbags;VIVA;Adults-Women;Women;Brown;;;Fashion;;2017;Onesize,VIVAHDBG00000055;4199.00;;;;;;;;zerotype;;Fall;;No
1940144;VV3183Blue-Blue-1940144;Viva Functional Shoulder;Draft;Handbags;VIVA;Adults-Women;Women;Blue;;;Fashion;;2017;Onesize,VIVAHDBG00000054;4199.00;;;;;;;;zerotype;;Fall;;No
1940143;VV3178Beige-Beige-1940143;Viva Carryall Shoulder;Draft;Handbags;VIVA;Adults-Women;Women;Beige;;;Fashion;;2017;Onesize,VIVAHDBG00000053;3499.00;;;;;;;;zerotype;;Fall;;No
1940142;VV3178Blue-Blue-1940142;Viva Carryall Shoulder;Draft;Handbags;VIVA;Adults-Women;Women;Blue;;;Fashion;;2017;Onesize,VIVAHDBG00000052;3699.00;;;;;;;;zerotype;;Fall;;No
1940141;VV3176Yellow-Yellow-1940141;Viva Convertible Shoulder;Draft;Handbags;VIVA;Adults-Women;Women;Yellow;;;Fashion;;2017;Onesize,VIVAHDBG00000051;3499.00;;;;;;;;zerotype;;Fall;;No
1940140;VV3176Maroon-Maroon-1940140;Viva Convertible Shoulder;Draft;Handbags;VIVA;Adults-Women;Women;Maroon;;;Fashion;;2017;Onesize,VIVAHDBG00000050;3499.00;;;;;;;;zerotype;;Fall;;No
1940139;VV3176Black-Black-1940139;Viva Convertible Shoulder;Draft;Handbags;VIVA;Adults-Women;Women;Black;;;Fashion;;2017;Onesize,VIVAHDBG00000049;3499.00;;;;;;;;zerotype;;Fall;;No
1940137;VV3176Beige-Beige-1940137;Viva Convertible Shoulder;Draft;Handbags;VIVA;Adults-Women;Women;Beige;;;Fashion;;2017;Onesize,VIVAHDBG00000048;3499.00;;;;;;;;zerotype;;Fall;;No
1940136;VV3174Brown-Brown-1940136;Viva Weekend Shoulder;Draft;Handbags;VIVA;Adults-Women;Women;Brown;;;Fashion;;2017;Onesize,VIVAHDBG00000047;3099.00;;;;;;;;zerotype;;Fall;;No
1940135;VV3174White-White-1940135;Viva Weekend Shoulder;Draft;Handbags;VIVA;Adults-Women;Women;White;;;Fashion;;2017;Onesize,VIVAHDBG00000046;3699.00;;;;;;;;zerotype;;Fall;;No
1940134;VV3174Pink-Pink-1940134;Viva Weekend Shoulder;Draft;Handbags;VIVA;Adults-Women;Women;Pink;;;Fashion;;2017;Onesize,VIVAHDBG00000045;3099.00;;;;;;;;zerotype;;Fall;;No
1940133;VV3172White-White-1940133;Viva Minimal Shoulder;Draft;Handbags;VIVA;Adults-Women;Women;White;;;Fashion;;2017;Onesize,VIVAHDBG00000044;3799.00;;;;;;;;zerotype;;Fall;;No
1940132;VV3172BBrown-Brown-1940132;Viva Minimal Shoulder;Draft;Handbags;VIVA;Adults-Women;Women;Brown;;;Fashion;;2017;Onesize,VIVAHDBG00000043;3299.00;;;;;;;;zerotype;;Fall;;No
1940131;VV3171Beige-Beige-1940131;Viva Vacation Shoulder;Draft;Handbags;VIVA;Adults-Women;Women;Beige;;;Fashion;;2017;Onesize,VIVAHDBG00000042;3199.00;;;;;;;;zerotype;;Fall;;No
1940129;VV3171Offwhite-Off White-1940129;Viva Vacation Shoulder;Draft;Handbags;VIVA;Adults-Women;Women;Off White;;;Fashion;;2017;Onesize,VIVAHDBG00000041;3999.00;;;;;;;;zerotype;;Fall;;No
1940128;VV3171Blue-Blue-1940128;Viva Vacation Shoulder;Draft;Handbags;VIVA;Adults-Women;Women;Blue;;;Fashion;;2017;Onesize,VIVAHDBG00000040;3199.00;;;;;;;;zerotype;;Fall;;No
1940127;VV3169Maroon-Maroon-1940127;Viva Collegiate Shoulder;Draft;Handbags;VIVA;Adults-Women;Women;Maroon;;;Fashion;;2017;Onesize,VIVAHDBG00000039;3999.00;;;;;;;;zerotype;;Fall;;No
1940126;VV3168BBrown-Brown-1940126;Viva Everyday Shoulder;Draft;Handbags;VIVA;Adults-Women;Women;Brown;;;Fashion;;2017;Onesize,VIVAHDBG00000038;3199.00;;;;;;;;zerotype;;Fall;;No
1940125;VV3163Beige-Beige-1940125;Viva Workaholic Shoulder;Draft;Handbags;VIVA;Adults-Women;Women;Beige;;;Fashion;;2017;Onesize,VIVAHDBG00000037;3599.00;;;;;;;;zerotype;;Fall;;No
1940124;VV3161Beige-Beige-1940124;Viva DayTrip Sling;Draft;Handbags;VIVA;Adults-Women;Women;Beige;;;Fashion;;2017;Onesize,VIVAHDBG00000036;2699.00;;;;;;;;zerotype;;Fall;;No
1940123;VV3161Blue-Blue-1940123;Viva DayTrip Sling;Draft;Handbags;VIVA;Adults-Women;Women;Blue;;;Fashion;;2017;Onesize,VIVAHDBG00000035;2699.00;;;;;;;;zerotype;;Fall;;No
1940122;VV3160White-White-1940122;Viva Tech Crossbody;Draft;Handbags;VIVA;Adults-Women;Women;White;;;Fashion;;2017;Onesize,VIVAHDBG00000034;4399.00;;;;;;;;zerotype;;Fall;;No
1940120;VV3160Black-Black-1940120;Viva Tech Crossbody;Draft;Handbags;VIVA;Adults-Women;Women;Black;;;Fashion;;2017;Onesize,VIVAHDBG00000033;3399.00;;;;;;;;zerotype;;Fall;;No
1940119;VV3150Offwhite-Off White-1940119;Viva Commuter Crossbody;Draft;Handbags;VIVA;Adults-Women;Women;Off White;;;Fashion;;2017;Onesize,VIVAHDBG00000032;3099.00;;;;;;;;zerotype;;Fall;;No
1940118;VV3149Blue-Blue-1940118;Viva Classic Crossbody;Draft;Handbags;VIVA;Adults-Women;Women;Blue;;;Fashion;;2017;Onesize,VIVAHDBG00000031;2899.00;;;;;;;;zerotype;;Fall;;No
1940117;VV3136Maroon-Maroon-1940117;Viva Commuter Shoulder;Draft;Handbags;VIVA;Adults-Women;Women;Maroon;;;Fashion;;2017;Onesize,VIVAHDBG00000030;3399.00;;;;;;;;zerotype;;Fall;;No
