Style ID;Vendor Article Number;Vendor Article Name;Active Status;Article type;Brand;Age Group;Gender;Base Colour;Colour1;Colour2;Fashion Type;Usage;Year;Size - SKU code pair;MRP;Style Description;Style Note;Materials and Care Description;Size and Fit Description;Size Chart Path;Product Display Name;Tags;Product Type;Classification Brand;Season;Comments;Upload Images
1967657;BD-CREAM-Cream-1967657;Lenora Cream-Coloured Georgette Embroidered S;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000100;6500.00;;;;;;;;zerotype;;Spring;;No
1967656;BD-BLUE-Brown-1967656;Lenora Blue & Brown -Coloured Georgette Silk;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000099;6500.00;;;;;;;;zerotype;;Spring;;No
1967655;VNPR-17702-Red-1967655;Lenora Red Georgette Embroidered Saree;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000098;7500.00;;;;;;;;zerotype;;Spring;;No
1967654;VNPR-17701-Blue-1967654;Lenora Blue Georgette Embroidered Saree;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000097;7500.00;;;;;;;;zerotype;;Spring;;No
1967653;NKS-6-Pink-1967653;Lenora Pink & Blue-Coloured Georgette silk Em;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000096;7500.00;;;;;;;;zerotype;;Spring;;No
1967652;NKS-5-Pink-1967652;Lenora Pink & Purple-Coloured Georgette Embro;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000095;7500.00;;;;;;;;zerotype;;Spring;;No
1967651;LN-SN-14-Green-1967651;Lenora Green Net Embroidered Saree;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000094;6875.00;;;;;;;;zerotype;;Spring;;No
1967650;LN-SN-07-Blue-1967650;Lenora Blue & Cream-Coloured Lycra Net Embroi;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000093;6625.00;;;;;;;;zerotype;;Spring;;No
1967649;LN-SN-02-Blue-1967649;Lenora Blue & Cream-Coloured Silk Net Embroid;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000092;10000.00;;;;;;;;zerotype;;Spring;;No
1967648;LN-SN-01-Green-1967648;Lenora Green & Cream-Coloured Silk Net Embroi;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000091;10000.00;;;;;;;;zerotype;;Spring;;No
1967647;LN-KT-1564-Purple-1967647;Lenora Purple & Cream Cotton-Silk Net Embroid;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000090;11000.00;;;;;;;;zerotype;;Spring;;No
1967646;LN-KT-1563-Red-1967646;Lenora Red & White Georgette Net Embroidered;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000089;11000.00;;;;;;;;zerotype;;Spring;;No
1967645;LN-KT-1561-Pink-1967645;Lenora Pink jacquard Net Embroidered Half-and;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000088;9250.00;;;;;;;;zerotype;;Spring;;No
1967644;LN-KT-1560-Blue-1967644;Lenora Blue & Cream Georgette, Silk Net Embro;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000087;9500.00;;;;;;;;zerotype;;Spring;;No
1967643;LN-KT-1559-Orange-1967643;Lenora Orange & Cream Georgette Net Embroider;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000086;9000.00;;;;;;;;zerotype;;Spring;;No
1967642;LN-KT-1558-Green-1967642;Lenora Green & Cream Georgette,Silk Net Embro;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000085;9000.00;;;;;;;;zerotype;;Spring;;No
1967641;LN-KT-1557-Pink-1967641;Lenora Pink & Cream Georgette Net Embroidered;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000084;9250.00;;;;;;;;zerotype;;Spring;;No
1967640;LN-KT-1556-Orange-1967640;Lenora Orange & Cream Georgette Silk Embroide;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000083;8250.00;;;;;;;;zerotype;;Spring;;No
1967639;LN-KT-1555-Pink-1967639;Lenora Pink & Cream Georgette Net Embroidered;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000082;10250.00;;;;;;;;zerotype;;Spring;;No
1967638;LN-KT-1554-Peach-1967638;Lenora Peach Georgette Net Embroidered Half-a;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000081;9750.00;;;;;;;;zerotype;;Spring;;No
1967637;LN-KT-1553-Green-1967637;Lenora Green Georgette Net Embroidered Half-a;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000080;10000.00;;;;;;;;zerotype;;Spring;;No
1967636;LN-KT-1552-Pink-1967636;Lenora Pink & Cream Georgette Net Embroidered;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000079;10500.00;;;;;;;;zerotype;;Spring;;No
1967635;LN-KT-1551-Red-1967635;Lenora Red & Cream Georgette Net Embroidered;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000078;8500.00;;;;;;;;zerotype;;Spring;;No
1967634;LN-KT-1550-Red-1967634;Lenora Red & Cream Georgette Net Embroidered;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000077;10125.00;;;;;;;;zerotype;;Spring;;No
1967633;LN-KT-1549-Red-1967633;Lenora Red & White Georgette Net Embroidered;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000076;7750.00;;;;;;;;zerotype;;Spring;;No
1967632;LN-KT-1548-Pink-1967632;Lenora Pink Silk Georgette Net Embroidered Ha;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000075;8500.00;;;;;;;;zerotype;;Spring;;No
1967631;LN-KT-1547-Grey-1967631;Lenora Grey & Pink Silk Net Embroidered Half-;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000074;8625.00;;;;;;;;zerotype;;Spring;;No
1967630;LN-KT-1546-Red-1967630;Lenora Red & Cream Silk Net Embroidered Half-;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000073;8625.00;;;;;;;;zerotype;;Spring;;No
1967629;LN-KT-1545-Peach-1967629;Lenora Peach Silk Net Embroidered Half-and-Ha;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000072;7750.00;;;;;;;;zerotype;;Spring;;No
1967628;LN-KT-1544-Pink-1967628;Lenora Pink Silk Net Embroidered Half-and-Hal;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000071;8375.00;;;;;;;;zerotype;;Spring;;No
1967627;LN-KT-1543-Red-1967627;Lenora Red & Cream -Coloured Georgette Silk &;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000070;9125.00;;;;;;;;zerotype;;Spring;;No
1967626;LN-215-Pink-1967626;Lenora Pink Georgette Embroidered Saree;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000069;5625.00;;;;;;;;zerotype;;Spring;;No
1967625;LN-214-Pink-1967625;Lenora Pink Georgette Embroidered Saree;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000068;6125.00;;;;;;;;zerotype;;Spring;;No
1967624;LN-213-Peach-1967624;Lenora Peach Georgette Embroidered Saree;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000067;4500.00;;;;;;;;zerotype;;Spring;;No
1967623;LN-203-Red-1967623;Lenora Red & Black Georgette Embroidered Sare;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000066;5375.00;;;;;;;;zerotype;;Spring;;No
1967622;LN-202-Blue-1967622;Lenora Blue Georgette Embroidered Saree;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000065;5750.00;;;;;;;;zerotype;;Spring;;No
1967621;LN-201-Multi-1967621;Lenora MultiColoured Georgette Net Embroidere;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000064;6250.00;;;;;;;;zerotype;;Spring;;No
1967620;LN-197-Cream-1967620;Lenora Cream Silk Embroidered Saree;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000063;5000.00;;;;;;;;zerotype;;Spring;;No
1967619;LN-196-Green-1967619;Lenora Green & Yellow-Coloured Net jacquard E;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000062;6500.00;;;;;;;;zerotype;;Spring;;No
1967618;LN-195-Yellow-1967618;Lenora Yellow & Blue-Coloured Georgette Net E;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000061;5875.00;;;;;;;;zerotype;;Spring;;No
1967617;LN-192-Yellow-1967617;Lenora Yellow Silk Embroidered Saree;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000060;6250.00;;;;;;;;zerotype;;Spring;;No
1967616;LN-191-Green-1967616;Lenora Green & White-Coloured Net jacquard Em;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000059;6750.00;;;;;;;;zerotype;;Spring;;No
1967615;LN-173-Pink-1967615;Lenora PInk Georgette Mirror work Embroidered;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000058;5875.00;;;;;;;;zerotype;;Spring;;No
1967614;LN-166-Black-1967614;Lenora Black & Cream-Coloured Georgette Lycra;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000057;5125.00;;;;;;;;zerotype;;Spring;;No
1967613;LN-164-Green-1967613;Lenora Green Net Georgette Embroidered Half-a;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000056;5250.00;;;;;;;;zerotype;;Spring;;No
1967612;LN-160-Orange-1967612;Lenora Orange & pink-Coloured Georgette Silk;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000055;6250.00;;;;;;;;zerotype;;Spring;;No
1967611;LN-159-Red-1967611;Lenora Red & Cream-Coloured Georgette Silk Em;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000054;6750.00;;;;;;;;zerotype;;Spring;;No
1967610;LN-158-Red-1967610;Lenora Red Georgette Embroidered Saree;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000053;4875.00;;;;;;;;zerotype;;Spring;;No
1967609;LN-157-Blue-1967609;Lenora Blue & Yellow-Coloured Georgette net E;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000052;6500.00;;;;;;;;zerotype;;Spring;;No
1967608;LN-156-Pink-1967608;Lenora Pink & Cream-Coloured Georgette net Em;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000051;7625.00;;;;;;;;zerotype;;Spring;;No
1967607;LN-155-Red-1967607;Lenora Red Georgette Embroidered Saree;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000050;4125.00;;;;;;;;zerotype;;Spring;;No
1967606;LN-154-Off White-1967606;Lenora Off white Georgette Embroidered Saree;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000049;4750.00;;;;;;;;zerotype;;Spring;;No
1967605;LN-153-Pink-1967605;Lenora Pink Georgette Embroidered Saree;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000048;5000.00;;;;;;;;zerotype;;Spring;;No
1967604;LN-151-Multi-1967604;Lenora Multicolored Georgette Embroidered Sar;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000047;7000.00;;;;;;;;zerotype;;Spring;;No
1967603;LN-136-Green-1967603;Lenora Aqua Green & Orange-Coloured Georgette;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000046;4500.00;;;;;;;;zerotype;;Spring;;No
1967602;LN-133-Pink-1967602;Lenora PInk Georgette Mirror work Embroidered;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000045;5375.00;;;;;;;;zerotype;;Spring;;No
1967601;LN-118-Green-1967601;Lenora Aqua Green Georgette Embroidered Saree;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000044;5500.00;;;;;;;;zerotype;;Spring;;No
1967600;LN-116-Yellow-1967600;Lenora Yellow & Green-Coloured Georgette Silk;Draft;Sarees;Lenora;Adults-Women;Women;;;;Fashion;;2017;Onesize,LNRASARS00000043;6625.00;;;;;;;;zerotype;;Spring;;No
