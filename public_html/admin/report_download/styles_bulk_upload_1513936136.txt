Style ID;Vendor Article Number;Vendor Article Name;Active Status;Article type;Brand;Age Group;Gender;Base Colour;Colour1;Colour2;Fashion Type;Usage;Year;Size - SKU code pair;MRP;Style Description;Style Note;Materials and Care Description;Size and Fit Description;Size Chart Path;Product Display Name;Tags;Product Type;Classification Brand;Season;Comments;Upload Images
2361417;2129702-Love Potion-Aop _PPMP-2361417;Minions beanie;Draft;Caps;Puma;Kids-Unisex;Unisex;Pink;;;Core;;2017;KIDS,PUMACAPS00097636;1299.00;;;;<p>24 cm</p>;;;;zerotype;;Fall;;No
2361416;2126202-Dark Gray Heather-Puma Black_PPMP-2361416;Layered Beanie;Draft;Caps;Puma;Adults-Unisex;Unisex;Grey;;;Core;;2017;ADULT,PUMACAPS00097635;3799.00;;;;<p>27 cm</p>;;;;zerotype;;Fall;;No
2361415;2129402-Love Potion-D.Purple_PPMP-2361415;Minions cap;Draft;Caps;Puma;Kids-Unisex;Unisex;Blue;;;Core;;2017;KIDS,PUMACAPS00097634;1099.00;;;;<p>24 cm</p>;;;;zerotype;;Fall;;No
2361414;2127902-Love Potion-D.Purple_PPMP-2361414;STYLE WM pom pom beanie;Draft;Caps;Puma;Adults-Women;Women;Pink;;;Core;;2017;ADULT,PUMACAPS00097633;1099.00;;;;<p>27 cm</p>;;;;zerotype;;Fall;;No
2361413;2134102-Peacoat _PPMP-2361413;EVOLUTION curved cap;Draft;Caps;Puma;Adults-Unisex;Unisex;Blue;;;Core;;2017;ADULT,PUMACAPS00097632;1299.00;;;;<p>27 cm</p>;;;;zerotype;;Fall;;No
2361412;2133902-Olive Night _PPMP-2361412;Velvet rope cap;Draft;Caps;Puma;Adults-Women;Women;Green;;;Core;;2017;XS,PUMACAPS00097630,S/M,PUMACAPS00097631;1499.00;;;;;;;;zerotype;;Fall;;No
2361411;2133103-Medium Gray Heather _PPMP-2361411;ACTIVE slouchy beanie;Draft;Caps;Puma;Adults-Unisex;Unisex;Grey;;;Core;;2017;ADULT,PUMACAPS00097629;999.00;;;;<p>27 cm</p>;;;;zerotype;;Fall;;No
2361410;2133101-Puma Black_PPMP-2361410;ACTIVE slouchy beanie;Draft;Caps;Puma;Adults-Unisex;Unisex;Black;;;Core;;2017;ADULT,PUMACAPS00097628;999.00;;;;<p>27 cm</p>;;;;zerotype;;Fall;;No
2361409;2118111-Puma Black-Nightcat _PPMP-2361409;Pure running cap;Draft;Caps;Puma;Adults-Unisex;Unisex;Black;;;Core;;2017;ADULT,PUMACAPS00097627;1299.00;;;;<p>27 cm</p>;;;;zerotype;;Fall;;No
2361408;5313918-Puma Black-White-Quiet Shade_PPMP-2361408;Script Fitted Cap;Draft;Caps;Puma;Adults-Men;Men;Black;;;Core;;2017;S/M,PUMACAPS00097624,M/L,PUMACAPS00097625,L/XL,PUMACAPS00097626;1499.00;;;;;;;;zerotype;;Fall;;No
2361407;2126901-Puma Black_PPMP-2361407;PUMA Metal Cat Cap;Draft;Caps;Puma;Adults-Unisex;Unisex;Black;;;Core;;2017;ADULT,PUMACAPS00097623;999.00;;;;<p>27 cm</p>;;;;zerotype;;Fall;;No
2361406;2133501-Puma Black_PPMP-2361406;ARCHIVE 5 panels cap;Draft;Caps;Puma;Adults-Unisex;Unisex;Black;;;Core;;2017;ADULT,PUMACAPS00097622;999.00;;;;<p>27 cm</p>;;;;zerotype;;Fall;;No
2361405;5313002-Puma Black_PPMP-2361405;SF Fanwear Key Ring;Draft;Key chain;Puma;Adults-Unisex;Unisex;Black;;;Core;;2016;x,PUMAKYCN00000012;599.00;;;;;;;;zerotype;;Fall;;No
2361404;8281401-White-Black-Silver_PPMP-2361404;ELITE 2.2 FUSION FifaQuality;Draft;Footballs;Puma;Adults-Unisex;Unisex;White;Grey;;Core;;2017;5,PUMAFTBS00000172;1999.00;;;;;;;;zerotype;;Fall;;No
2361403;8275801-White-Team Power Blue-Black _PPMP-2361403;Puma Big Cat Ball;Draft;Footballs;Puma;Adults-Unisex;Unisex;White;;;Core;;2017;5,PUMAFTBS00000169,4,PUMAFTBS00000170,3,PUMAFTBS00000171;1299.00;;;;;;;;zerotype;;Fall;;No
2361402;8278301-White-Atomic Blue-Yellow_PPMP-2361402;ISL evoPOWER 1.3 FIFA Pro;Draft;Sports Accessories;Puma;Adults-Unisex;Unisex;White;Blue;;Core;;2017;5,PUMASPAC00000347;6999.00;;;;;;;;zerotype;;Fall;;No
2361401;5326601-Puma Black-Peach-Turquoise_PPMP-2361401;PR Womens Arm pocket;Draft;Sports Accessories;Puma;Adults-Women;Women;Black;;;Core;;2017;X,PUMASPAC00000346;1299.00;;;;;;;;zerotype;;Fall;;No
2361400;3063801-Mykonos Blue-Ultra Yellow _PPMP-2361400;evoPOWER Chrome guard;Draft;Sports Accessories;Puma;Adults-Unisex;Unisex;Blue;Green;;Core;;2017;L,PUMASPAC00000343,M,PUMASPAC00000344,S,PUMASPAC00000345;1499.00;;;;;;;;zerotype;;Fall;;No
2361399;89366201-Sharks Blue-Fluro Peach-Flur_PPMP-2361399;evoSPEED HPI Pre Prepared '1;Draft;Sports Accessories;Puma;Adults-Men;Men;Blue;;;Core;;2016;6,PUMASPAC00000342;3999.00;;;;;;;;zerotype;;Fall;;No
2361398;7512202-Puma Black-Navy Blue_PPMP-2361398;PUMA Maze Backpack;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Navy Blue;;;Core;;2017;x,PUMABAPK00098856;2199.00;;;;<p>50x27x22</p>;;;;zerotype;;Fall;;No
2361397;7512203-Red Blast-Puma Black_PPMP-2361397;PUMA Maze Backpack;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Orange;;;Core;;2017;x,PUMABAPK00098855;2199.00;;;;<p>50x27x22</p>;;;;zerotype;;Fall;;No
2361396;7512201-Quiet Shade-Puma Black_PPMP-2361396;PUMA Maze Backpack;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Grey;;;Core;;2017;x,PUMABAPK00098854;2199.00;;;;<p>50x27x22</p>;;;;zerotype;;Fall;;No
2361395;7511904-Quarry-Daffodil _PPMP-2361395;PUMA Power Backpack;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Grey;;;Core;;2017;x,PUMABAPK00098853;1799.00;;;;<p>50x25x22</p>;;;;zerotype;;Fall;;No
2361394;7511903-Safety Yellow-Navy Blue _PPMP-2361394;PUMA Power Backpack;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Navy Blue;;;Core;;2017;x,PUMABAPK00098852;1799.00;;;;<p>50x25x22</p>;;;;zerotype;;Fall;;No
2361393;7511902-Steel Gray-Vibrant Orange _PPMP-2361393;PUMA Power Backpack;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Grey;;;Core;;2017;x,PUMABAPK00098851;1799.00;;;;<p>50x25x22</p>;;;;zerotype;;Fall;;No
2361392;7511901-Puma Black-Quiet Shade_PPMP-2361392;PUMA Power Backpack;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Black;;;Core;;2017;x,PUMABAPK00098850;1799.00;;;;<p>50x25x22</p>;;;;zerotype;;Fall;;No
2361391;7471802-Lapis Blue_PPMP-2361391;PUMA Pioneer Backpack II;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Blue;;;Core;;2017;x,PUMABAPK00098849;1299.00;;;;<p>43x29x21</p>;;;;zerotype;;Fall;;No
2361390;7471805-Toreador_PPMP-2361390;PUMA Pioneer Backpack II;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Red;;;Core;;2017;x,PUMABAPK00098848;1299.00;;;;<p>43x29x21</p>;;;;zerotype;;Fall;;No
2361389;7471801-Puma Black_PPMP-2361389;PUMA Pioneer Backpack II;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Black;;;Core;;2017;x,PUMABAPK00098847;1299.00;;;;<p>43x29x21</p>;;;;zerotype;;Fall;;No
2361388;7471804-Olive Night _PPMP-2361388;PUMA Pioneer Backpack II;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Olive;;;Core;;2017;x,PUMABAPK00098846;1299.00;;;;<p>43x29x21</p>;;;;zerotype;;Fall;;No
2361387;7471803-Dark Purple _PPMP-2361387;PUMA Pioneer Backpack II;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Purple;;;Core;;2017;x,PUMABAPK00098845;1299.00;;;;<p>43x29x21</p>;;;;zerotype;;Fall;;No
2361386;7489802-Puma Red-Puma Black _PPMP-2361386;Pro Training II Backpack;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Red;;;Core;;2017;x,PUMABAPK00098844;1299.00;;;;<p>43x29x21</p>;;;;zerotype;;Fall;;No
2361385;7489803-Royal Blue-Puma Black _PPMP-2361385;Pro Training II Backpack;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Blue;;;Core;;2017;x,PUMABAPK00098843;1299.00;;;;<p>43x29x21</p>;;;;zerotype;;Fall;;No
2361384;7489801-Puma Black_PPMP-2361384;Pro Training II Backpack;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Black;;;Core;;2017;x,PUMABAPK00098842;1299.00;;;;<p>43x29x21</p>;;;;zerotype;;Fall;;No
2361383;7483403-Olive Night _PPMP-2361383;Evo Blaze Work Backpack;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Olive;;;Core;;2017;x,PUMABAPK00098841;3499.00;;;;<p>44x37x10</p>;;;;zerotype;;Fall;;No
2361382;7483402-Peacoat-Apple Cinnamon_PPMP-2361382;Evo Blaze Work Backpack;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Navy Blue;;;Core;;2017;x,PUMABAPK00098840;3499.00;;;;<p>44x37x10</p>;;;;zerotype;;Fall;;No
2361381;7482802-Peacoat-Apple Cinnamon_PPMP-2361381;Evo Blaze Backpack;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Navy Blue;;;Core;;2017;x,PUMABAPK00098839;3999.00;;;;<p>45.5x27x17 (22L)</p>;;;;zerotype;;Fall;;No
2361380;7482801-Puma Black_PPMP-2361380;Evo Blaze Backpack;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Black;;;Core;;2017;x,PUMABAPK00098838;3999.00;;;;<p>45.5x27x17 (22L)</p>;;;;zerotype;;Fall;;No
2361379;7482101-Olive Night-Puma Black_PPMP-2361379;VR Combat Backpack;Draft;Backpacks;Puma;Adults-Women;Women;Olive;;;Core;;2017;x,PUMABAPK00098837;3999.00;;;;<p>43x29x17</p>;;;;zerotype;;Fall;;No
2361378;7479801-Puma Black-Sneaker Graphic_PPMP-2361378;Campus Backpack;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Black;White;;Core;;2017;x,PUMABAPK00098836;2499.00;;;;<p>39x24x14</p>;;;;zerotype;;Fall;;No
2361377;7339109-Fuchsia Purple_PPMP-2361377;PUMA Pioneer Backpack I;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Pink;;;Core;;2016;x,PUMABAPK00098835;1199.00;;;;<p>43x29x21</p>;;;;zerotype;;Fall;;No
2361376;7215902-Victoria Blue-Dark Shadow _PPMP-2361376;Foundation Prime Backpack;Draft;Backpacks;Puma;Adults-Unisex;Unisex;Black;Grey;;Core;;2016;x,PUMABAPK00098834;2299.00;;;;<p>47x31x17</p>;;;;zerotype;;Fall;;No
2361375;7413401-Puma Black-Royal Blue _PPMP-2361375;Fit AT Sports Duffle;Draft;Duffel Bag;Puma;Adults-Women;Women;Black;;;Core;;2016;x,PUMADFBG00000187;2999.00;;;;<p>25.5 x 52 x 25.5 (33 L)</p>;;;;zerotype;;Fall;;No
2361374;91098002-Purple / Pink _PPMP-2361374;PUMA WOMEN QUARTER STRIPE 2P;Draft;Socks;Puma;Adults-Women;Women;Multi;;;Core;;2017;35/38,PUMASCKS00095279;499.00;;;;;;;;zerotype;;Fall;;No
2361373;91023905-Black _PPMP-2361373;PUMA FOOTIE 2P UNISEX;Draft;Socks;Puma;Adults-Unisex;Unisex;Black;;;Core;;2017;37/40,PUMASCKS00095278;449.00;;;;;;;;zerotype;;Fall;;No
2361372;91017103-Dark Green_PPMP-2361372;PUMA UNISEX SNEAKER PLAIN 3P;Draft;Socks;Puma;Adults-Unisex;Unisex;Multi;;;Core;;2017;37/40,PUMASCKS00095277;499.00;;;;;;;;zerotype;;Fall;;No
2361371;91017102-Pastel Lavender _PPMP-2361371;PUMA UNISEX SNEAKER PLAIN 3P;Draft;Socks;Puma;Adults-Unisex;Unisex;Multi;;;Core;;2017;37/40,PUMASCKS00095276;499.00;;;;;;;;zerotype;;Fall;;No
2361370;91017101-Black / Yellow_PPMP-2361370;PUMA UNISEX SNEAKER PLAIN 3P;Draft;Socks;Puma;Adults-Unisex;Unisex;Multi;;;Core;;2017;37/40,PUMASCKS00095275;499.00;;;;;;;;zerotype;;Fall;;No
2361369;91022903-Middle Grey Melange _PPMP-2361369;PUMA FOOTIE 3P UNISEX;Draft;Socks;Puma;Adults-Unisex;Unisex;Grey;;;Core;;2017;37/40,PUMASCKS00095274;499.00;;;;;;;;zerotype;;Fall;;No
2361368;91022902-White _PPMP-2361368;PUMA FOOTIE 3P UNISEX;Draft;Socks;Puma;Adults-Unisex;Unisex;White;;;Core;;2017;37/40,PUMASCKS00095273;499.00;;;;;;;;zerotype;;Fall;;No
2361367;91022901-Black/White _PPMP-2361367;PUMA FOOTIE 3P UNISEX;Draft;Socks;Puma;Adults-Unisex;Unisex;Black;;;Core;;2017;37/40,PUMASCKS00095272;499.00;;;;;;;;zerotype;;Fall;;No
2361366;91018401-Black/White _PPMP-2361366;Puma Footie (2P);Draft;Socks;Puma;Adults-Unisex;Unisex;Multi;;;Core;;2017;37/40,PUMASCKS00095271;499.00;;;;;;;;zerotype;;Fall;;No
2361365;91088905-Neon Yellow/Neon Blue/Neon P_PPMP-2361365;PUMA LIFESTYLE SNEAKERS 3P;Draft;Socks;Puma;Adults-Unisex;Unisex;Multi;;;Core;;2017;37/40,PUMASCKS00095270;499.00;;;;;;;;zerotype;;Fall;;No
2361364;91088904-Pink/Purple/Olive _PPMP-2361364;PUMA LIFESTYLE SNEAKERS 3P;Draft;Socks;Puma;Adults-Unisex;Unisex;Multi;;;Core;;2017;37/40,PUMASCKS00095269;499.00;;;;;;;;zerotype;;Fall;;No
2361363;91009603-Dark Grey/Red/Navy_PPMP-2361363;Ankle Length Half Terry sock;Draft;Socks;Puma;Adults-Men;Men;Multi;;;Core;;2017;37/40,PUMASCKS00095268;499.00;;;;;;;;zerotype;;Fall;;No
2361362;91077601-White/White/White _PPMP-2361362;PUMA Men Sport Quarters 3 Pa;Draft;Socks;Puma;Adults-Men;Men;White;;;Core;;2017;39/42,PUMASCKS00095267;449.00;;;;;;;;zerotype;;Fall;;No
2361361;74981501-Cyber Yellow-Black_PPMP-2361361;BVB Hooped Socks;Draft;Socks;Puma;Adults-Men;Men;Yellow;Black;;Core;;2016;2,PUMASCKS00095262,5,PUMASCKS00095263,3,PUMASCKS00095264,4,PUMASCKS00095265,1,PUMASCKS00095266;999.00;;;;;;;;zerotype;;Fall;;No
2361360;7456702-Puma White-Color Blend_PPMP-2361360;Prime Pouch P;Draft;Travel Accessory;Puma;Adults-Women;Women;Off White;;;Core;;2016;x,PUMATRAC00000003;999.00;;;;;;;;zerotype;;Fall;;No
2361359;5254601-Black-Black _PPMP-2361359;PUMA Bytes Phone Cover;Draft;Mobile Accessories;Puma;Adults-Unisex;Unisex;Black;;;Core;;2016;x,PUMAMBAC00000028;1099.00;;;;;;;;zerotype;;Fall;;No
2361358;7479501-Puma Black-Sneaker Graphic_PPMP-2361358;Campus Reporter;Draft;Messenger Bag;Puma;Adults-Unisex;Unisex;Black;White;;Core;;2017;x,PUMAMGBG00000141;2499.00;;;;;;;;zerotype;;Fall;;No
2361357;5287201-Black-White _PPMP-2361357;PUMA Swimming Goggle Regular;Draft;Swimwear Accessories;Puma;Adults-Unisex;Unisex;Black;;;Core;;2017;x,PUMASWMA00000027;899.00;;;;;;;;zerotype;;Fall;;No
