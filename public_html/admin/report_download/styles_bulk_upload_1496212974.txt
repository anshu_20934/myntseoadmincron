Style ID;Vendor Article Number;Vendor Article Name;Active Status;Article type;Brand;Age Group;Gender;Base Colour;Colour1;Colour2;Fashion Type;Usage;Year;Size - SKU code pair;MRP;Style Description;Style Note;Materials and Care Description;Size and Fit Description;Size Chart Path;Product Display Name;Tags;Product Type;Classification Brand;Season;Comments;Upload Images
1940116;OP00416-Golden-1940116;Rubans Kundan Gold Metal & White Beads Earring;Draft;Earrings;Rubans;Adults-Women;Women;Gold;;;Fashion;;2017;Onesize,RBNSEARR00000588;950.00;;;;;;;;zerotype;;Summer;;No
1940115;OP00415-Golden-1940115;Rubans Gold Jhumki with Beads;Draft;Earrings;Rubans;Adults-Women;Women;Gold;;;Fashion;;2017;Onesize,RBNSEARR00000587;850.00;;;;;;;;zerotype;;Summer;;No
1940114;OP00414-Golden-1940114;Rubans Golden Jhumki With Beads Hanging;Draft;Earrings;Rubans;Adults-Women;Women;Gold;;;Fashion;;2017;Onesize,RBNSEARR00000586;1199.00;;;;;;;;zerotype;;Summer;;No
1940113;OP00413-Golden-1940113;Rubans Gold Jhumki with Beads;Draft;Earrings;Rubans;Adults-Women;Women;Gold;;;Fashion;;2017;Onesize,RBNSEARR00000585;1199.00;;;;;;;;zerotype;;Summer;;No
1940112;OP00411-Silver-1940112;Rubans Silver Metal Hoop;Draft;Earrings;Rubans;Adults-Women;Women;Silver;;;Fashion;;2017;Onesize,RBNSEARR00000584;899.00;;;;;;;;zerotype;;Summer;;No
1940111;OP00410-Silver-1940111;Rubans Silver Jhumki with Beads;Draft;Earrings;Rubans;Adults-Women;Women;Silver;;;Fashion;;2017;Onesize,RBNSEARR00000583;1050.00;;;;;;;;zerotype;;Summer;;No
1940110;OP00409-Silver-1940110;Rubans Silver Jhumki with Beads;Draft;Earrings;Rubans;Adults-Women;Women;Silver;;;Fashion;;2017;Onesize,RBNSEARR00000582;1050.00;;;;;;;;zerotype;;Summer;;No
1940109;OP00408-Silver-1940109;Rubans Silver Jhumki with Beads;Draft;Earrings;Rubans;Adults-Women;Women;Silver;;;Fashion;;2017;Onesize,RBNSEARR00000581;1050.00;;;;;;;;zerotype;;Summer;;No
1940108;OP00407-Silver-1940108;Rubans Silver Jhumki with Beads;Draft;Earrings;Rubans;Adults-Women;Women;Silver;;;Fashion;;2017;Onesize,RBNSEARR00000580;999.00;;;;;;;;zerotype;;Summer;;No
1940107;OP00406-Silver-1940107;Rubans Silver Jhumki with Beads;Draft;Earrings;Rubans;Adults-Women;Women;Silver;;;Fashion;;2017;Onesize,RBNSEARR00000579;999.00;;;;;;;;zerotype;;Summer;;No
1940106;OP00405-Silver-1940106;Rubans Silver Earring;Draft;Earrings;Rubans;Adults-Women;Women;Silver;;;Fashion;;2017;Onesize,RBNSEARR00000578;1050.00;;;;;;;;zerotype;;Summer;;No
1940105;OP00395-Silver-1940105;Rubans Dual Tone Jhumki with beads;Draft;Earrings;Rubans;Adults-Women;Women;Silver;;;Fashion;;2017;Onesize,RBNSEARR00000577;1050.00;;;;;;;;zerotype;;Summer;;No
1940104;OP00394-Silver-1940104;Rubans Dual Tone Jhumki with beads;Draft;Earrings;Rubans;Adults-Women;Women;Silver;;;Fashion;;2017;Onesize,RBNSEARR00000576;1050.00;;;;;;;;zerotype;;Summer;;No
1940103;OP00393-Silver-1940103;Rubans Dual Tone Jhumki;Draft;Earrings;Rubans;Adults-Women;Women;Silver;;;Fashion;;2017;Onesize,RBNSEARR00000575;1099.00;;;;;;;;zerotype;;Summer;;No
1940102;OP00392-Golden-1940102;Rubans Jhumki with beads;Draft;Earrings;Rubans;Adults-Women;Women;Gold;;;Fashion;;2017;Onesize,RBNSEARR00000574;1199.00;;;;;;;;zerotype;;Summer;;No
1940101;OP00391-Golden-1940101;Rubans Kundan Green Beads Hanging Earrings;Draft;Earrings;Rubans;Adults-Women;Women;Gold;;;Fashion;;2017;Onesize,RBNSEARR00000573;899.00;;;;;;;;zerotype;;Summer;;No
1940100;OP00390-Golden-1940100;Rubans Kundan Beads Hanging Earrings;Draft;Earrings;Rubans;Adults-Women;Women;Gold;;;Fashion;;2017;Onesize,RBNSEARR00000572;899.00;;;;;;;;zerotype;;Summer;;No
1940099;OP00389-Golden-1940099;Rubans Golden Tone Kundan  Jhumki;Draft;Earrings;Rubans;Adults-Women;Women;Gold;;;Fashion;;2017;Onesize,RBNSEARR00000571;2199.00;;;;;;;;zerotype;;Summer;;No
1940098;OP00388-Golden-1940098;Rubans Golden Tone Kundan  Jhumki;Draft;Earrings;Rubans;Adults-Women;Women;Gold;;;Fashion;;2017;Onesize,RBNSEARR00000570;2199.00;;;;;;;;zerotype;;Summer;;No
1940097;OP00387-Silver-1940097;Rubans Silver Earrings with Beads;Draft;Earrings;Rubans;Adults-Women;Women;Silver;;;Fashion;;2017;Onesize,RBNSEARR00000569;799.00;;;;;;;;zerotype;;Summer;;No
1940096;OP00386-Dual Tone-1940096;Rubans Dual Tone Metal Jhumki;Draft;Earrings;Rubans;Adults-Women;Women;Silver;;;Fashion;;2017;Onesize,RBNSEARR00000568;1099.00;;;;;;;;zerotype;;Summer;;No
1940095;OP00385-Dual Tone-1940095;Rubans Dual Tone Metal Jhumki;Draft;Earrings;Rubans;Adults-Women;Women;Silver;;;Fashion;;2017;Onesize,RBNSEARR00000567;999.00;;;;;;;;zerotype;;Summer;;No
1940094;OP00384-Silver-1940094;Rubans Silver Metal Chain Jhumki;Draft;Earrings;Rubans;Adults-Women;Women;Silver;;;Fashion;;2017;Onesize,RBNSEARR00000566;1099.00;;;;;;;;zerotype;;Summer;;No
