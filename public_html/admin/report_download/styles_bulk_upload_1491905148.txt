Style ID;Vendor Article Number;Vendor Article Name;Active Status;Article type;Brand;Age Group;Gender;Base Colour;Colour1;Colour2;Fashion Type;Usage;Year;Size - SKU code pair;MRP;Style Description;Style Note;Materials and Care Description;Size and Fit Description;Size Chart Path;Product Display Name;Tags;Product Type;Classification Brand;Season;Comments;Upload Images
1850234;CR-MK-1617/O22-Blue-1850234;Mohawk Chic Summer Tote;Draft;Travel Accessory;Mohawk;Adults-Women;Women;;;;Fashion;;2017;Onesize,MHWKTRAC00000003;2199.00;;;;;;;;zerotype;;Summer;;No
1850233;CR-MK-TRF4-Peach-1850233;Mohawk Coral Cosmetic Pouch;Draft;Travel Accessory;Mohawk;Adults-Women;Women;;;;Fashion;;2017;Onesize,MHWKTRAC00000002;999.00;;;;;;;;zerotype;;Summer;;No
1850232;CR-MK-TRF3-Peach-1850232;Mohawk Coral 13" Laptop Tote;Draft;Travel Accessory;Mohawk;Adults-Women;Women;;;;Fashion;;2017;Onesize,MHWKTRAC00000001;1999.00;;;;;;;;zerotype;;Summer;;No
1850231;CR-MK-1617/T16-Off White-1850231;Mohawk Suave White Travel Bag;Draft;Duffel Bag;Mohawk;Adults-Unisex;Unisex;;;;Fashion;;2017;Onesize,MHWKDFBG00000054;2999.00;;;;;;;;zerotype;;Summer;;No
1850230;CR-MK-TRF5-Peach-1850230;Mohawk Coral Gym Bag;Draft;Duffel Bag;Mohawk;Adults-Women;Women;;;;Fashion;;2017;Onesize,MHWKDFBG00000053;3499.00;;;;;;;;zerotype;;Summer;;No
1850229;CR-MK-1617/T23-Blue-1850229;Mohawk Lagoon Travel Bag;Draft;Duffel Bag;Mohawk;Adults-Unisex;Unisex;;;;Fashion;;2017;Onesize,MHWKDFBG00000052;1999.00;;;;;;;;zerotype;;Summer;;No
1850228;CR-MK-1617/T22-Brown-1850228;Mohawk Musk Travel Bag;Draft;Duffel Bag;Mohawk;Adults-Unisex;Unisex;;;;Fashion;;2017;Onesize,MHWKDFBG00000051;2999.00;;;;;;;;zerotype;;Summer;;No
1850227;CR-MK-1617/T21-Blue-1850227;Mohawk Marine Travel Bag;Draft;Duffel Bag;Mohawk;Adults-Unisex;Unisex;;;;Fashion;;2017;Onesize,MHWKDFBG00000050;2499.00;;;;;;;;zerotype;;Summer;;No
1850226;CR-MK-1617/T20-Black-1850226;Mohawk Ace Black Travel Bag;Draft;Duffel Bag;Mohawk;Adults-Unisex;Unisex;;;;Fashion;;2017;Onesize,MHWKDFBG00000049;3199.00;;;;;;;;zerotype;;Summer;;No
1850225;CR-MK-1617/T19-Blue-1850225;Mohawk Marshal Denim Travel Bag;Draft;Duffel Bag;Mohawk;Adults-Unisex;Unisex;;;;Fashion;;2017;Onesize,MHWKDFBG00000048;2499.00;;;;;;;;zerotype;;Summer;;No
1850224;CR-MK-1617/T18-Grey-1850224;Mohawk Krayr Grey Travel Bag;Draft;Duffel Bag;Mohawk;Adults-Unisex;Unisex;;;;Fashion;;2017;Onesize,MHWKDFBG00000047;2999.00;;;;;;;;zerotype;;Summer;;No
1850223;CR-MK-1617/T17-Black-1850223;Mohawk Cross Travel Bag;Draft;Duffel Bag;Mohawk;Adults-Unisex;Unisex;;;;Fashion;;2017;Onesize,MHWKDFBG00000046;2499.00;;;;;;;;zerotype;;Summer;;No
1850222;CR-MK-TRF6-Peach-1850222;Mohawk Coral Travel Duffle Bag;Draft;Duffel Bag;Mohawk;Adults-Women;Women;;;;Fashion;;2017;Onesize,MHWKDFBG00000045;3499.00;;;;;;;;zerotype;;Summer;;No
1850221;CR-MK-1617/O26-Black-1850221;Mohawk Luxe 14" Black Laptop Bag;Draft;Laptop Bag;Mohawk;Adults-Men;Men;;;;Fashion;;2017;Onesize,MHWKLABA00000046;2499.00;;;;;;;;zerotype;;Summer;;No
1850220;CR-MK-1617/O23-Grey-1850220;Mohawk Tread 14" Laptop Bag;Draft;Laptop Bag;Mohawk;Adults-Unisex;Unisex;;;;Fashion;;2017;Onesize,MHWKLABA00000045;2999.00;;;;;;;;zerotype;;Summer;;No
1850219;CR-MK-1617/O20-Blue-1850219;Mohawk Azure 14" Laptop Bag;Draft;Laptop Bag;Mohawk;Adults-Unisex;Unisex;;;;Fashion;;2017;Onesize,MHWKLABA00000044;2199.00;;;;;;;;zerotype;;Summer;;No
1850218;CR-MK-1617/O19-Grey-1850218;Mohawk Solid 14" Laptop Bag;Draft;Laptop Bag;Mohawk;Adults-Unisex;Unisex;;;;Fashion;;2017;Onesize,MHWKLABA00000043;1499.00;;;;;;;;zerotype;;Summer;;No
1850217;CR-MK-1617/O18-Blue-1850217;Mohawk Caelum 14" Laptop Bag;Draft;Laptop Bag;Mohawk;Adults-Unisex;Unisex;;;;Fashion;;2017;Onesize,MHWKLABA00000042;1999.00;;;;;;;;zerotype;;Summer;;No
1850216;CR-MK-1617/O17-Grey-1850216;Mohawk Eli basic 14" Herringbone Laptop Bag;Draft;Laptop Bag;Mohawk;Adults-Men;Men;;;;Fashion;;2017;Onesize,MHWKLABA00000041;1999.00;;;;;;;;zerotype;;Summer;;No
1850215;CR-MK-1617/O16-Grey-1850215;Mohawk Grayknight 14" Laptop Bag;Draft;Laptop Bag;Mohawk;Adults-Unisex;Unisex;;;;Fashion;;2017;Onesize,MHWKLABA00000040;2199.00;;;;;;;;zerotype;;Summer;;No
1850214;CR-MK-1617/O14-Black-1850214;Mohawk Vault 14" Black Slim Laptop Bag;Draft;Laptop Bag;Mohawk;Adults-Men;Men;;;;Fashion;;2017;Onesize,MHWKLABA00000039;1499.00;;;;;;;;zerotype;;Summer;;No
1850213;CR-MK-1617/C3-Grey-1850213;Mohawk Mosaic 14" Laptop Bag;Draft;Laptop Bag;Mohawk;Adults-Unisex;Unisex;;;;Fashion;;2017;Onesize,MHWKLABA00000038;1499.00;;;;;;;;zerotype;;Summer;;No
1850212;CR-MK-TRF1-Peach-1850212;Mohawk Coral 14" Laptop Bag;Draft;Laptop Bag;Mohawk;Adults-Women;Women;;;;Fashion;;2017;Onesize,MHWKLABA00000037;2999.00;;;;;;;;zerotype;;Summer;;No
1850211;CR-MK-1617/O25-Black-1850211;Mohawk Fincher 14" Black Laptop Backpack;Draft;Backpacks;Mohawk;Adults-Unisex;Unisex;;;;Fashion;;2017;Onesize,MHWKBAPK00000012;2999.00;;;;;;;;zerotype;;Summer;;No
1850210;CR-MK-1617/O15-Grey-1850210;Mohawk Eli 14" Herringbone Laptop Backpack;Draft;Backpacks;Mohawk;Adults-Unisex;Unisex;;;;Fashion;;2017;Onesize,MHWKBAPK00000011;2199.00;;;;;;;;zerotype;;Summer;;No
1850209;CR-MK-1617/O13-Grey-1850209;Mohawk Bloc 14" Grey Laptop Backpack;Draft;Backpacks;Mohawk;Adults-Unisex;Unisex;;;;Fashion;;2017;Onesize,MHWKBAPK00000010;2999.00;;;;;;;;zerotype;;Summer;;No
1850208;CR-MK-1617/O12-Yellow-1850208;Mohawk Bloc 14" Yellow Laptop Backpack;Draft;Backpacks;Mohawk;Adults-Unisex;Unisex;;;;Fashion;;2017;Onesize,MHWKBAPK00000009;2999.00;;;;;;;;zerotype;;Summer;;No
1850207;CR-MK-1617/O11-Black-1850207;Mohawk Bloc 14" Black Laptop Backpack;Draft;Backpacks;Mohawk;Adults-Unisex;Unisex;;;;Fashion;;2017;Onesize,MHWKBAPK00000008;2999.00;;;;;;;;zerotype;;Summer;;No
1850206;CR-MK-TRF2-Peach-1850206;Mohawk Coral 14" Laptop Backpack;Draft;Backpacks;Mohawk;Adults-Women;Women;;;;Fashion;;2017;Onesize,MHWKBAPK00000007;2999.00;;;;;;;;zerotype;;Summer;;No
