<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: partner_top_performers.php,v 1.7 2006/01/11 06:55:58 mclap Exp $
#
# Display top performers statistics
#

require "./auth.php";
require $xcart_dir."/include/security.php";
 
if(!$active_modules['XAffiliate'])
    func_header_location ("error_message.php?access_denied&id=24");

$location[] = array(func_get_langvar_by_name("lbl_top_performers"), "");

#
# Define data for the navigation within section
# 
$dialog_tools_data["left"][] = array("link" => "banner_info.php", "title" => func_get_langvar_by_name("lbl_banners_statistics"));
$dialog_tools_data["left"][] = array("link" => "referred_sales.php", "title" => func_get_langvar_by_name("lbl_referred_sales"));
$dialog_tools_data["left"][] = array("link" => "partner_top_performers.php", "title" => func_get_langvar_by_name("lbl_top_performers"));
$dialog_tools_data["left"][] = array("link" => "affiliates.php", "title" => func_get_langvar_by_name("lbl_affiliates_tree"));
$dialog_tools_data["left"][] = array("link" => "partner_adv_stats.php", "title" => func_get_langvar_by_name("lbl_adv_statistics"));

if($StartDay)
	$search['start_date'] = mktime(0, 0, 0, $StartMonth, $StartDay, $StartYear);

if($EndDay)
    $search['end_date'] = mktime(23, 59, 59, $EndMonth, $EndDay, $EndYear);

if($search) {
	$where = array();
    if($search['start_date'] && $search['end_date'])
        $where[] = $search['end_date']." > $sql_tbl[partner_clicks].add_date AND $sql_tbl[partner_clicks].add_date > ".$search['start_date'];
	if($where)
		$where_condition = " AND ".implode(" AND ", $where);
	$result = func_query("SELECT $sql_tbl[partner_clicks].*, $sql_tbl[partner_clicks].$search[report] as name, COUNT($sql_tbl[partner_clicks].$search[report]) as clicks, SUM($sql_tbl[orders].subtotal) as sales, COUNT($sql_tbl[orders].subtotal) as num_sales  FROM $sql_tbl[partner_clicks] LEFT JOIN $sql_tbl[orders] ON $sql_tbl[partner_clicks].clickid = $sql_tbl[orders].clickid WHERE 1 ".$where_condition." GROUP BY $sql_tbl[partner_clicks].$search[report] ORDER BY ".$search['sort']." DESC");
	if($result) {
		$smarty->assign("result", $result);
	}
}

$smarty->assign ("main", "partner_top_performers");

$smarty->assign("search", $search);

$smarty->assign ("month_begin", mktime(0,0,0,date('m'),1,date('Y')));

# Assign the current location line
$smarty->assign("location", $location);

# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>
