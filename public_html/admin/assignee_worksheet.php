<?php 
require "./auth.php";
require $xcart_dir."/include/security.php";

include_once($xcart_dir."/include/func/func.itemsearch.php");

$order_by="";
$sort_order="";
$mode="";
$assigneeid="";

if($HTTP_POST_VARS){
        $query_string = http_build_query($HTTP_POST_VARS);  
        $assigneeid = $HTTP_POST_VARS['item_assignee'];
        $mode = $HTTP_POST_VARS['mode'];
    }
if($HTTP_GET_VARS){
        $query_string = http_build_query($HTTP_GET_VARS);  
        $assigneeid = $HTTP_GET_VARS['item_assignee'];
        $mode = $HTTP_GET_VARS['mode'];
        $order_by=$HTTP_GET_VARS['order_by'];
        
        $sort_order=$HTTP_GET_VARS['sort_order'];
    }
if($mode=="download_csv"){
    $sql = func_load_assignee_task_list($assigneeid,$order_by,$sort_order);

    $items=func_query($sql);
    $assignee_name = func_query_first_cell("SELECT name FROM $sql_tbl[mk_assignee] WHERE id='$assigneeid'");
    #
    #generate table
    #
    $file_name_suffix = time();
  
    $summary_table = "Order name, Order id, Login, Item Id, Status, item name, Quantity, Breakup, location";
    $string = $summary_table."\r\n";
    foreach($items AS $key=>$array){

        
        $quantity_breakup = str_replace("," , " || ", $array[quantity_breakup]);
        //$item_status = $status[$array['item_status']]['status'];
        $string .= $array[order_name]."," .$array[orderid] . ", " .$array[login].",". $array[itemid] . ", " . $array[item_status] . ", " . $array[stylename] . ",". $array[amount]  . ",". $quantity_breakup . ", " .$array[location_name] ;
        $string .= "\r\n";
    }
    $string.="\r\n";
  
    $filename="$assignee_name"."_task_list_".$file_name_suffix;
    header('Content-Type: application/csv');
    header("Content-disposition: attachment; filename=$filename.csv");
    echo $string;
    exit;
//	force_download($filename, $dir);
}
if($mode=="download_xls"){
    $sql = func_load_assignee_task_list($assigneeid,$order_by,$sort_order);

    $items=func_query($sql);
    $assignee_name = func_query_first_cell("SELECT name FROM $sql_tbl[mk_assignee] WHERE id='$assigneeid'");
    #
    #generate table
    #
    $file_name_suffix = time();

    $summary_table = "Order name \t  Order id \t  Login \t  Item Id \t  Status \t  item name \t  Quantity \t  Breakup \t  location";
    $string = $summary_table."\r\n";
    foreach($items AS $key=>$array){


        $quantity_breakup = str_replace("," , " || ", $array[quantity_breakup]);
        //$item_status = $status[$array['item_status']]['status'];
        $string .= $array[order_name]." \t " .$array[orderid] . " \t  " .$array[login]." \t ". $array[itemid] . " \t  " . $array[item_status] . " \t  " . $array[stylename] . " \t ". $array[amount]  . " \t ". $quantity_breakup . " \t  " .$array[location_name] ;
        $string .= "\r\n";
    }
    $string.="\r\n";

    $filename="$assignee_name"."_task_list_".$file_name_suffix;
    header('Content-Type: application/xls');
    header("Content-disposition: attachment; filename=$filename.xls");
    echo $string;
    exit;
//	force_download($filename, $dir);
}
if(($assigneeid)){
#
#Calling function to create search query for number of records
#
    $sql = func_load_assignee_task_list_count($assigneeid);


    $colorcode=array();
    $colorcode['late']="#FF3333";
    $colorcode['qafail']="#FF66CC";

    
	$sqllog->debug("File name##assignee_worksheet.php##SQL Info::>$sql");
	$total_items = func_query_first_cell($sql);
	$smarty->assign("total_items", $total_items);
	if ($total_items > 0) {
		$page = $_GET["page"];
		#
		# Prepare the page navigation
		#
		if (!isset($objects_per_page)) {
			if ($current_area == "C")
				$objects_per_page = $config["Appearance"]["products_per_page"];
			else
				$objects_per_page = $config["Appearance"]["products_per_page_admin"];
		}
		$total_nav_pages = ceil($total_items/$objects_per_page)+1;

		require $xcart_dir."/include/navigation.php";
		
		$search_sql = func_load_assignee_task_list($assigneeid,$order_by,$sort_order);
	    
        $search_sql .=" LIMIT $first_page, $objects_per_page";
        
		$sqllog->debug("File name##assignee_worksheet.php##SQL Info::>$search_sql");
		$itemResult = func_query($search_sql);

        //Look at the order comment log for these 50 orders. This is a very
        //inefficient way of doing it - but this is what we have for now.
        // (seperate table/ column for QA in next OMS release ~lavanya)
        foreach($itemResult AS $key=>$details){
            $itemid=$details[itemid];
            $orderid=$details[orderid];

            //Determine colorcode
            $itemResult[$key]['ccode']="";

            //Late items
            if($itemResult[$key]['estimated_date'] ){
               if ($itemResult[$key]['estimated_date'] < time()){
                      $itemResult[$key]['ccode']=$colorcode["late"];
               }
               $itemResult[$key]['estimated_date']=date("d/m/y : H:i:s",$itemResult[$key]['estimated_date']);
            }

            //QA Fail items
            
            $qa_query="select description from mk_ordercommentslog where commenttype='QaFailure' and orderid=$orderid and commenttitle = 'item $itemid' order by commentdate desc";
            $qaResult = func_query_first_cell($qa_query);
            
            if($qaResult != "") {
                $itemResult[$key]['ccode']=$colorcode["qafail"];
                $itemResult[$key]['failreason']=$qaResult;
            }
            
        }
        #
		#navigation url
		#
		if($mode == "search")
		      $URL = "assignee_worksheet.php?".$query_string;
        else
              $URL = "assignee_worksheet.php";  

		$smarty->assign("navigation_script",$URL);
		$smarty->assign("first_item", $first_page+1);
		$smarty->assign("last_item", min($first_page+$objects_per_page, $total_items));
		$smarty->assign("total_items",$total_items);
		$smarty->assign ("itemResult", $itemResult);
        $smarty->assign ("colorcode", $colorcode);
        $smarty->assign ("current_date", time());
		$smarty->assign ("sizeofitemresult", $objects_per_page);
	}
	
}

#
#Load assigee list
#
    $sql = "SELECT a.id, CONCAT('',b.name,' - ',a.name) as name, a.status, a.threshold FROM {$sql_tbl[mk_assignee]} a LEFT JOIN {$sql_tbl[operations_location]} b on a.operations_location = b.id WHERE a.status=1 order by b.name,a.name";
    $assignee = func_query($sql);
    
#
#Status array
#   
$status = array(//"UA"=>array("status"=>"Unassigned", "color"=>"#EE0F14"),
                "A"=>array("status"=>"Assigne", "color"=>"#77AB3D"),
                "OD"=>array("status"=>"Ops Done", 		"color"=>"#99FF22"),
                //"QD"=>array("status"=>"QA Done", 		"color"=>"#EF940A"),
                //"D"=>array("status"=>"Done", 		"color"=>"#80FFFF"),
                //"F"=>array("status"=>"Failed", 		"color"=>"#333FFF"),
                "H"=>array("status"=>"On Hold", "color"=>"#F5FF42"), 
                "S"=>array("status"=>"Sent Outside", "color"=>"#04FF82"));
 
$smarty->assign ("status", $status);   
$smarty->assign ("query_string", $query_string);    
$smarty->assign ("mode", $mode);       
$smarty->assign("assignee",$assignee);
$smarty->assign("assigneeid",$assigneeid);

$smarty->assign("main","assignee_worksheet");
func_display("admin/home.tpl",$smarty);
?>
