<?php 
/*
 * It can be used from admin UI interface. Pass a limited set of styleIds Comma Separated in UI 
 */ 
require "auth.php";
require_once $xcart_dir."/include/solr/solrProducts.php";
require_once $xcart_dir."/modules/discount/DiscountEngine.php";
use vtr\client\VirtualTrialRoomClient;

set_time_limit(0);
$retval = ini_set('memory_limit','1600M');
require_once $xcart_dir."/include/security.php";
$mode=$_POST["mode"];//should be one of: vtr-enable|vtr-disable
$listofstyles = $_POST["styleId"];//1728,8482,18201,17463
if(!empty($mode)) {
	if(empty($listofstyles)) {
		$message = "Please enter Comma Separated List of style-ids";
	}
	else {
		//validate this input with regex: CSV of style-ids 
		$isValidListOfStyles = preg_match("/^[0-9]+[0-9,]*$/", $listofstyles);
		if($isValidListOfStyles){
			$lastCharIndex = strlen($listofstyles)-1;
			//if last cahr is , then remove it 
			if($listofstyles[$lastCharIndex] == ','){
				$listofstyles = substr($listofstyles,0,$lastCharIndex);
			}
			if(empty($listofstyles)) {
				$message = "Please enter Comma Separated List of style-ids";
			}
			else {
				
				//Invoke top-level action method
				$message = disableStylesInVTR($listofstyles, ($mode ==="vtr-enable"));
			}
		}
		else {
			$message = "Style-IDs should be Comma Separated Numbers without spaces or any other characters";
		}
	}
}
$smarty->assign("message",$message);
$smarty->assign("main","vtr_enable_disable_styles");
func_display("admin/home.tpl",$smarty);

/**
 * Disable (or enable) comma separated list of styles for VTR indexing in Solr 
 * @param unknown_type $listOfStyleIds
 * @param unknown_type $enable
 */
function disableStylesInVTR($listOfStyleIds, $enable = false){
	//disable/enable styles from vtr-service side: to mark them disabled/enabled for indexing
	$msg = VirtualTrialRoomClient::disableStyles($listOfStyleIds, $enable);
	//get style-indexing data (applicable torsos,client-types etc) for these style-ids
	$vtrStyleData = VirtualTrialRoomClient::getStyleDataForIndexing($listOfStyleIds);
	$styleIdArrayToIndex = explode(",",$listOfStyleIds);
	//get discount data for these styles
	$discountDetails = DiscountEngine::getDataForMultipleStyles("search", $styleIdArrayToIndex);
	
	$solrProducts = new SolrProducts();
	//re-index these styles in solr: since they might have been enabled or disabled
	foreach ($styleIdArrayToIndex as $styleid) {
		$solrProducts->deleteStyleFromIndex($styleid,true,true);
		$solrProducts->addStyleToIndex($styleid,true,false,true,$discountDetails->$styleid,$vtrStyleData[$styleid]);
	}
	$solrProducts->commitToAllServers();
	
	//verify whether they were actually added/removed from solr for vtr
	$retval = verifyInSolr($solrProducts, $listOfStyleIds, $enable);
	if(!$retval){
		if(count($styleIdArrayToIndex)>100){
			$msg = "Enable/disable style may not have been successful since you requested to change more than 100 styles. Pls check mannually";
		}
		else {
			$msg = "Enable/disable style failed: either at service level or solr level";
		}
	}
	return $msg;	
}

/**
 * Verify by firing a solr query: styleid must be in the list of passed styles and torsos either 1 or 2
 * @param unknown_type $solrProducts
 * @param unknown_type $listOfStyleIds
 * @param unknown_type $enable
 */
function verifyInSolr($solrProducts, $listOfStyleIds, $enable = false){
	$styleIdArrayToIndex = explode(",",$listOfStyleIds);
	$results = performSolrQuery($solrProducts, buildSolrQueryForVerification($styleIdArrayToIndex));
	if($enable and count($styleIdArrayToIndex) == count($results)){
		return true;			
	}
	elseif (!$enable and empty($results)){
		return true;
	}
	else {
		return false;
	}
}

/**
 * Verify by firing a solr query: styleid must be in the list of passed styles and torsos either 1 or 2
 * @param unknown_type $styleIdArrayToIndex
 */
function buildSolrQueryForVerification($styleIdArrayToIndex){
	$solrStyleQry = array();
	foreach ($styleIdArrayToIndex as $styleId) {
		array_push($solrStyleQry, "(styleid:".$styleId.")");
	}
	$solrStyleQryPhrase = "(".implode("OR", $solrStyleQry).")";
	$solrTorsosQryPhrase = "((torsos:1)OR(torsos:2))";
	$solrQry = $solrStyleQryPhrase."AND".$solrTorsosQryPhrase;
	return $solrQry; 
}

/**
 * Perform a solr query and get results
 * @param unknown_type $solrProducts
 * @param unknown_type $query
 */
function performSolrQuery($solrProducts, $query){
	//assume that we'll at maximum pass list of 100 styles
 	$search_results = $solrProducts->searchAndSort($query,0,100);
 	$products = getSingleFieldInDoc($search_results->docs,"styleid");
 	//$products_count = $search_results->numFound;
 	return $products;
 }
 
 function getSingleFieldInDoc($documents,$field){
 	$documents_array = array();
 	foreach($documents as $document) {
 		array_push($documents_array, $document->__get($field));
 	}
 	return $documents_array;
 }
?>
