<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/**
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 **/

return array(
    'js' => array(
        '//skin1/myntra_js/jquery.min.js',
        '//skin1/myntra_js/jquery.tools.min.js',
        '//skin1/myntra_js/jquery.cycle.all.min.js',
        '//skin1/myntra_js/jquery.hoverIntent.minified.js',
        '//skin1/myntra_js/custom.js',
        '//skin1/myntra_js/common.js',
        '//skin1/myntra_js/thickbox3.js',
		'//skin1/myntra_js/jquery.marquee.js',
		'//skin1/myntra_js/sizechart.js',
		'//skin1/myntra_js/sizechart-position.js',
		'//skin1/myntra_js/jquery.ajaxq-0.0.1.js'
    ),

    'css' => array(
        '//skin1/myntra_css/screen.css',
        '//skin1/myntra_css/newskin-styles.css',
        '//skin1/myntra_css/common.css',
        '//skin1/myntra_css/ab-styles.css',
		'//skin1/myntra_css/megamenu.css',
        '//skin1/myntra_css/thickbox3.css',
    	'//skin1/myntra_css/sizechart.css'
    ),

    'admin_js' => array(
        '//skin1/myntra_js/jquery.1.4.2.min.js',
        '//skin1/js_script/jquery.validate.js',
    	'//skin1/myntra_js/ns.js',
    	'//skin1/myntra_js/ns.js',
    	'//skin1/myntra_js/mk-base.js',
    	'//skin1/myntra_js/lightbox.js',
    	'//skin1/myntra_js/jquery.ui.js'
    ),

    'secure_js' => array(
        '//skin1/myntra_js/jquery.min.js',     
    ),

    'secure_css' => array(
        '//skin1/myntra_css/screen.css',
        '//skin1/myntra_css/newskin-styles.css',
        '//skin1/myntra_css/common.css'
    )
);
