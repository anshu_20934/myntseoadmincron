<?php
use mcart\exception\ProductAlreadyInCartException;
include_once("auth.php");
include_once($xcart_dir."/include/func/func.mkmisc.php");
include_once ("$xcart_dir/include/class/mcart/class.MCart.php");
include_once ("$xcart_dir/include/func/func.utilities.php");

if(defined("CART_PROXY")){
	$cartitem = $requestData->itemId;
}else{
$cartitem = (int)filter_input(INPUT_POST, 'cartitem', FILTER_SANITIZE_NUMBER_INT);
}

if(!checkCSRF($_REQUEST["_token"], "modifycart")){
	func_header_location("mkmycart.php?at=c&pagetype=productdetail");
	exit;
}

$mcartFactory= new MCartFactory();
$myCart = $mcartFactory->getCurrentUserCart();
$fromCombo = $_POST["fromComboOverlay"]?addslashes($_POST["fromComboOverlay"]):addslashes($_GET["fromComboOverlay"]);
if( isset($HTTP_POST_VARS["update"]) && $HTTP_POST_VARS['update'] === 'size' )
{
	if(defined("CART_PROXY")){
		$qty = abs($requestData->quantity);
		$skuId = $requestData->skuId;
	}else{
    $qty = (int)filter_input(INPUT_POST, $cartitem.'qty', FILTER_SANITIZE_NUMBER_INT);
    $qty = abs($qty);
    $skuId = (int)filter_input(INPUT_POST, $cartitem.'size', FILTER_SANITIZE_NUMBER_INT);
	}

    if (!$qty || !$skuId) {
        func_header_location("mkmycart.php?at=c&pagetype=productdetail");
        exit;
    }

    //If there is no cart object available, addition and removal of object is not possible
    if($myCart!=null)
    {
        // remove the item
        $myCart->updateItem($cartitem, $skuId, $qty);
        MCartUtils::persistCart($myCart);
        $cartLog = array();
        $cartLog["product_id"]= $skuId;
        global $login;
        $cartLog["user_id"] = $login;
        global $telesalesUser;
        $cartLog["agent_id"] = $telesalesUser;
        $cartLog["action"]="updatesize";
        $cartLog["quantity"]=$qty;
        global $telesales;
        if ($telesales){
            func_array2insertWithTypeCheck("Cart_log",$cartLog);
        }
    }
    if(!defined("CART_PROXY")){    
        func_header_location("mkmycart.php?at=c&pagetype=productdetail&msg=" . $msg);
        exit;
    }
}
else if (isset($HTTP_POST_VARS["update"]) && $HTTP_POST_VARS["update"] === "customize") {
    if ($myCart!=null) {
        $mcartItem = $myCart->getProduct($cartitem);
        if ($mcartItem != null) {
        	$myCart->removeItem($mcartItem);
        }

        $styleArray = $mcartItem->getStyleArray();
        $mcartItem->setCustomizable((bool)$styleArray['is_customizable'] &&
                                    FeatureGateKeyValuePairs::getBoolean('personalization.enabled'));

        if ($mcartItem->isCustomizable()) {
            $doCustomize = $requestData->doCustomize;
            $customName = $requestData->customName;
            $customNumber = $requestData->customNumber;

            if (!$doCustomize) {
                $mcartItem->setCustomized(false); 
                $mcartItem->setCustomName("");
                $mcartItem->setCustomNumber("");
            } else {
                if (!empty($customName) &&
                    !empty($customNumber) && 
                    strlen($customName) <= FeatureGateKeyValuePairs::getFeatureGateValueForKey('personalization.name.maxlen') &&
                    strlen($customNumber) <= FeatureGateKeyValuePairs::getFeatureGateValueForKey('personalization.number.maxlen') &&
                    preg_match("/[a-zA-Z]+/", $customName) &&
                    preg_match("/[0-9]+/", $customNumber)) {

                    $mcartItem->setCustomized(true);
                    $mcartItem->setCustomName($customName);
                    $mcartItem->setCustomNumber($customNumber);
                    $mcartItem->setCustomizedMessage($customNumber.":".$customName);
                } else {
                    $mcartItem->setCustomized(false);
                    $mcartItem->setCustomName("");
                    $mcartItem->setCustomNumber("");
                }
            }

            try {
                $myCart->addProduct($mcartItem, true);
            } catch (Exception $e) {
                global $errorlog;
                $errorlog->error("Adding item to the cart failed due to following error: ".$e->getMessage());
            }
            MCartUtils::persistCart($myCart);
        }
    }
}
else
if( isset($HTTP_POST_VARS["update"])  )
{
    $uQty = (int)filter_input(INPUT_POST, $cartitem.'qty', FILTER_SANITIZE_NUMBER_INT);
    if(!$uQty > 0){
    	  $uQty = 0;
    }
	if($myCart!=null){

		$product=$myCart->getProduct($cartitem);
		if($product!=null){
			//AVAIL
			$styleId=$product->getStyleId();
			//END AVAIL
			$myCart->updateItemQuantity($product,$uQty);
			if ($uQty > 1) {
			    // no personalization for multi-qty skus
			    $product->setCustomizable(false);
			    $product->setCustomized(false);
			    $product->setCustomName("");
			    $product->setCustomNumber("");
 			}
			MCartUtils::persistCart($myCart);
                        $cartLog = array();
                        $cartLog["product_id"]= $styleId;
                        global $login;
                        $cartLog["user_id"] = $login;
                        global $telesalesUser;
                        $cartLog["agent_id"] = $telesalesUser;
                        $cartLog["action"]="updatequantity";
                        $cartLog["quantity"]=$uQty;
                        global $telesales;
                        if ($telesales){
                            func_array2insertWithTypeCheck("Cart_log",$cartLog);
                        }
		}
	}
}
else
if( isset($HTTP_GET_VARS["remove"]) && $HTTP_GET_VARS["remove"] == 'true' )
{
	// remove all the temp images for this product
	// delete the original image only if the productid is not published in xcart_products
	$cartitem = $HTTP_GET_VARS["itemid"];
	$orderId = $XCART_SESSION_VARS["order_id"];
	$removingFreeItem = $HTTP_GET_VARS["removingFreeItem"];
	if($myCart!=null){
		$product=$myCart->getProduct($cartitem);
		if($product!=null){
			//AVAIL
			$styleId=$product->getStyleId();
			//END AVAIL
			if($removingFreeItem){
				$myCart->removeItem($product,false);
			}
			else	{
				$myCart->removeItem($product);
			}
			MCartUtils::persistCart($myCart);
                        $cartLog = array();
                        $cartLog["product_id"]= $styleId;
                        global $login;
                        $cartLog["user_id"] = $login;
                        global $telesalesUser;
                        $cartLog["agent_id"] = $telesalesUser;
                        $cartLog["action"]="updatequantity";
                        $cartLog["quantity"]=-1;
                        global $telesales;
                        if ($telesales){
                            func_array2insertWithTypeCheck("Cart_log",$cartLog);
                        }
		}
	}

}
else
if( isset($HTTP_GET_VARS["removeall"]) && $HTTP_GET_VARS["removeall"] == 'true' )
{
	$styleIds=array();
	$useractionclearcart=true;
	if($myCart!=null){
		foreach ($myCart->getProducts() as $product) {
			array_unshift($styleIds,$product->getStyleId());
		}
		$styleIds=array_unique($styleIds);
		$styleId=implode(',',$styleIds);
		MCartUtils::deleteCart($myCart);
	}

}//FOR NEW COMBO PAGE
else if(isset($HTTP_POST_VARS["removecombo"]) && $HTTP_POST_VARS["removecombo"] == 1 ){

	$cartitem = $HTTP_POST_VARS["itemid"];
	$product=$myCart->getProduct($cartitem);
	if($product!=null){
		//AVAIL
		$styleId=$product->getStyleId();
		//END AVAIL
		$myCart->removeItem($product);
		MCartUtils::persistCart($myCart);
}

}

if(!defined("CART_PROXY")){
	if(isset($_POST['comboProduct']) && $_POST['comboProduct'] == 1) {
    	echo json_encode(array("status"=>"success",
    		"message" => "ITEM REMOVED FROM CART, time to get combo details here",
    		"total"=>$myCart->getCartSubtotal(),
            "count"=>$myCart->getItemQuantity()
    		));
    	exit;
	}
	
if($myCart==null || count($myCart->getProducts())==0){
	func_header_location("clearcart.php?av_id_removed=".$styleId);
	exit;

}
if($fromCombo == 1){
	$msg = array ( 'msg' => 'success' );
	echo json_encode($msg);
} else {
func_header_location("mkmycart.php?at=c&pagetype=productdetail&useractionclearcart=$useractionclearcart"."&av_id_removed=".$styleId);
}
}

