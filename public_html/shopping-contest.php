<?php
require "./auth.php";
include_once './mrp_description.php';

global $cashback_gateway_status, $mrpCouponConfiguration;
// Add the word describing the number of coupons
convert_coupon_numbers_to_words($mrpCouponConfiguration);

$nonfbregValue = $mrpCouponConfiguration['mrp_firstLogin_nonfbreg_numCoupons']*$mrpCouponConfiguration['mrp_firstLogin_nonfbreg_mrpAmount'];
$fbregValue = $mrpCouponConfiguration['mrp_firstLogin_fbreg_numCoupons']*$mrpCouponConfiguration['mrp_firstLogin_fbreg_mrpAmount'];
$refregValue = $mrpCouponConfiguration['mrp_refRegistration_numCoupons']*$mrpCouponConfiguration['mrp_refRegistration_mrpAmount'];
$refpurValue = $mrpCouponConfiguration['mrp_refFirstPurchase_numCoupons']*$mrpCouponConfiguration['mrp_refFirstPurchase_mrpAmount'];

$smarty->assign("nonfbregValue", $nonfbregValue);
$smarty->assign("fbregValue", $fbregValue);
$smarty->assign("refregValue", $refregValue);
$smarty->assign("refpurValue", $refpurValue);
$smarty->assign("mrpCouponConfiguration", $mrpCouponConfiguration);
func_display("shopping-contest.tpl", $smarty);
?>