<?php
require_once "./auth.php";
require_once($xcart_dir.'/include/search/class.SearchURLGenerator.php');
$parameters = $_GET;
if(!empty($parameters)){
	$urlGenerator = new SearchURLGenerator($parameters);
	$key_desc = $urlGenerator->getSolrQueryAndDesc();
	func_header_location($key_desc['page_desc']."/".$key_desc['page_key'].".mnt");
}else{
	echo "no parameter passed";
}