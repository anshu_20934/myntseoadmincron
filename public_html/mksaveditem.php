<?php
include_once("./auth.php");
require_once \HostConfig::$documentRoot."/include/func/func.utilities.php";
use enums\saveditems\SavedItemActionType;
use saveditems\action\SavedItemAddAction;
use saveditems\action\SavedItemAction;
use saveditems\action\SavedItemAddFromCartAction;
use saveditems\action\SavedItemMoveToCartAction;
use saveditems\action\SavedItemRemoveAction;
use saveditems\action\SavedItemUpdateQuantityAction;
use saveditems\action\SavedItemUpdateSkuAction;
use enums\base\RedirectLocations;
$actionType = RequestVars::getVar("actionType");
$redirectTo = RequestVars::getVar("redirectTo");
$styleIdToAdd = RequestVars::getVar("productId");
$savedActionStatus=0;
$wlreq=false;
$cartLog = array();
$cartLog["product_id"]= $styleIdToAdd;
if ($cartLog["product_id"] == 0)
    $cartLog["product_id"]= RequestVars::getVar("styleid");
global $login;
$cartLog["user_id"] = $login;
global $telesalesUser;
$cartLog["agent_id"] = $telesalesUser;
$cartLog["action"]=$actionType;

if ($actionType=="updateQuantity"){
    $cartLog["action"]= $cartLog["action"]."_wishlist";
    $qty = RequestVars::getVar("itemId")."qty";
    $cartLog["quantity"] = RequestVars::getVar($qty);
}
if ($actionType=="updateSku"){
    $cartLog["action"]= $cartLog["action"]."_wishlist";
    $itemid = RequestVars::getVar("itemId")."size";
    $cartLog["product_id"] = RequestVars::getVar($itemid);
}
if ($actionType=="add")
    $cartLog["product_id"] = RequestVars::getVar("productSKUID");
global $telesales;

switch($actionType) {
    case \enums\saveditems\SavedItemActionType::View:
        break;
    case \enums\saveditems\SavedItemActionType::AddItemFromCart:
        $action = new SavedItemAddFromCartAction();
        $returnVal=$action->run();    
        if($returnVal=='saved')
        {
            $savedActionStatus=1;
            $wlreq=true;
        }
        break;
    case \enums\saveditems\SavedItemActionType::MoveItemToCart:
    	$wlreq=false;
        $action = new SavedItemMoveToCartAction();
        $sku = $action->getSavedItem()->getSKUArray();
        $cartLog["product_id"] = $sku['id'];
        $action->run();
        break;

    case \enums\saveditems\SavedItemActionType::Add:
    	$wlreq=true;
        $action = new SavedItemAddAction();
        $val = $action->run();
        if(empty($redirectTo)) {
            echo $val;
        }    
        break;

    case \enums\saveditems\SavedItemActionType::Delete:
        $action = new SavedItemRemoveAction();
        $wlreq=true;
        $sku = $action->getSavedItem()->getSKUArray();
        $cartLog["product_id"] = $sku['id'];
        $action->run();
        break;
    case \enums\saveditems\SavedItemActionType::UpdateSkuInSaved:
    $wlreq=true;
        $action = new SavedItemUpdateSkuAction();
        $action->run();
        break;
    case \enums\saveditems\SavedItemActionType::UpdateQuantity:
    	$wlreq=true;
        $action = new SavedItemUpdateQuantityAction();
        $action->run();
        break;
    case \enums\saveditems\SavedItemActionType::GetSavedItemCount:
        $action = new SavedItemAction();
        return $action->getSavedItemCount();
        break;
    case \enums\saveditems\SavedItemActionType::GetCartItemCount:
        $action = new SavedItemAction();
        return $action->getCartItemCount();
        break;
    case \enums\saveditems\SavedItemActionType::GetStyleIdsInCart:
        $action = new SavedItemAction();
        return $action->getStyleIdsInCart();
        break;
    case \enums\saveditems\SavedItemActionType::GetStyleIdsInSaved:
        $action = new SavedItemAction();
        return $action->getStyleIdsInSaved();
        break;
    default:
        break;
}
if ($telesales){
    func_array2insertWithTypeCheck("Cart_log",$cartLog);
}

$locations = new RedirectLocations();
$redirectLocation = $locations->$redirectTo;
if(!empty($redirectLocation)) {
	if($skin=='skinmobile' && $wlreq){
		func_header_location($redirectLocation.'?status='.$savedActionStatus.'&style='.$styleIdToAdd."&wlreq=true&#wishlist");   
	}
	else if($skin=='skinmobile' && !wlreq){
		func_header_location($redirectLocation.'?status='.$savedActionStatus.'&style='.$styleIdToAdd);   
	}
	else {
		func_header_location($redirectLocation.'?status='.$savedActionStatus.'&style='.$styleIdToAdd);   
	} 
}
?>
