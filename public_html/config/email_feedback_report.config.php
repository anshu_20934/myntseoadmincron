<?php





//Questions in order
$feedback_report_questions= array(
 		array("questionid"=>22,"graph_type"=>'P',"refquestion"=>null,'display'=>"Likeness for Myntra"),
        array("questionid"=>23,"graph_type"=>'P',"refquestion"=>null,'display'=>"Interest"),
        array("questionid"=>24,"graph_type"=>'B',"refquestion"=>23,'display'=>"Satisfied with offering"),
        array("questionid"=>26,"graph_type"=>'P',"refquestion"=>null,'display'=>"Source"),
        array("questionid"=>27,"graph_type"=>'B',"refquestion"=>23,'display'=>"Gender"),
        array("questionid"=>28,"graph_type"=>'P',"refquestion"=>null,'display'=>"Occupation"),
        array("questionid"=>29,"graph_type"=>'P',"refquestion"=>null,'display'=>"Age"),
        array("questionid"=>30,"graph_type"=>'P',"refquestion"=>null,'display'=>"Education")

);

//Chart Colors
$colors=array(array( 205,92,92), array(152,251,152), array(175,238,238), array(102,204,204), array(102,255,255), array(186,85,211), array(255,69,0));
//Chart default properties
$properties=array('height'=>250,'width'=>420,'slicecolors'=>$colors,'lables'=>null);

$feedback_type_id=4;

$option_display_labels= array(
	 	52=>"Rocks!",53=>"Love it",54=>"Like",55=>"OK",56=>"Sucks",
        57=>"Self",58=>"Gift",59=>"Bulk",60=>"WS",61=>"Company info",62=>"Job",63=>"Others",
        64=>"Yes",65=>"No",
        67=>"Direct",68=>"Search",69=>"Social",70=>"SEM",71=>"Emailer",72=>"Radio",
        73=>"Male",74=>"Female",
        75=>"Student",76=>"Professional",77=>"Self Employed",
        78=>"<18",79=>"18-25",80=>"26-30",81=>"31-35",82=>">35",
        83=>"UG",84=>"Grad",85=>"PG"

);

$email_config= array(
	"subject"=>"User Feedback Digest",
	"from"=>array('emailid'=>"support@myntra.com","name"=>" Myntra Reporting Service")

);

$table_report =array(
	 "comment_ques_id"=>25,
     "email_ques_id"=>31,
     "age_ques_id"=>29,
     "gender_ques_id"=>27,
	 "intrest_ques_id"=>23
);

date_default_timezone_set("Asia/Calcutta");
?>