<?php
require_once  "../auth.php";
require_once $xcart_dir.'/modules/RestAPI/RestRequest.php';

$notificationIds = implode(',',$_REQUEST['notificationIds']);
$url = HostConfig::$notificationsUrl.'/markreadnew/?notification_id_list='. $notificationIds .'&user_id='.$XCART_SESSION_VARS['login'];
$method = 'POST';
$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Content-type: application/json';
$headers[] = 'Accept-Language: en-US';
$request = new RestRequest($url, $method);
$request->setHttpHeaders($headers);
$request->execute();
$info = $request->getResponseInfo();
$body = $request->getResponseBody();
if($info['http_code'] == 200){
	$response = json_decode($body,true);
	if($response['NotificationResponse']['status']['statusType'] == 'SUCCESS'){
		$url = HostConfig::$notificationsUpsUrl.'/putAttr?userid='. $XCART_SESSION_VARS['login'] .'&attribute=notificationCounter&value='. $_REQUEST['notificationCount'] .'&sourceid=3';
		$method = 'GET';
		$request = new RestRequest($url, $method);
		$request->setHttpHeaders($headers);
		$request->execute();
		$info = $request->getResponseInfo();
		$body = $request->getResponseBody();
		if($info['http_code'] == 200){
			$body = json_decode($body,true);
			if($body['errorCode'] == 0 && $body['body']['notificationCounter'] == $_REQUEST['notificationCount']){
				echo 'SUCCESS';
			}
		} 
	}
}
?>