<?php
require_once '../auth.php';
require_once $xcart_dir.'/modules/RestAPI/RestRequest.php';

$url = HostConfig::$notificationsUpsUrl.'/putAttr?userid='.$XCART_SESSION_VARS['login'].'&attribute=notificationCounter&value=-1&sourceid=3';
$method = 'GET';
$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Content-type: application/json';
$headers[] = 'Accept-Language: en-US';
$request = new RestRequest($url, $method);
$request->setHttpHeaders($headers);
$request->execute();
?>