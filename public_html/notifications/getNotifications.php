<?php
require_once '../auth.php';
require_once $xcart_dir.'/modules/RestAPI/RestRequest.php';

$url = HostConfig::$notificationsUrl.'/searchbyuser?user_id='.$XCART_SESSION_VARS['login'];
$method = 'GET';
$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Content-type: application/json';
$headers[] = 'Accept-Language: en-US';
$request = new RestRequest($url, $method);
$request->setHttpHeaders($headers);
$request->execute();
$info = $request->getResponseInfo();
$body = $request->getResponseBody();
if($info['http_code'] == 200){
	$response = json_decode($body,true);
	if($response['NotificationResponse']['status']['statusType'] == 'SUCCESS'){
		if($response['NotificationResponse']['status']['totalCount'] == 1){
			$tempArr = array();
			$tempArr[] = $response['NotificationResponse']['NotificationList']['data'];
			$response['NotificationResponse']['NotificationList']['data'] = $tempArr;
		}
		echo json_encode($response['NotificationResponse']['NotificationList']);		
	}
}
?>