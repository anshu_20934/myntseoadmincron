<?php
require_once  "../auth.php";
require_once $xcart_dir.'/modules/RestAPI/RestRequest.php';

$userId = $XCART_SESSION_VARS['login'];
$url = HostConfig::$notificationsUrl.'/?id='. $_REQUEST['notificationId'] .'&user_id='.$XCART_SESSION_VARS['login'];
$method = 'DELETE';
$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Content-type: application/json';
$headers[] = 'Accept-Language: en-US';
$request = new RestRequest($url, $method);
$request->setHttpHeaders($headers);
$request->execute();
$info = $request->getResponseInfo();
$body = $request->getResponseBody();
if($info['http_code'] == 200){
	$response = json_decode($body,true);
	if($response['NotificationResponse']['status']['statusType'] == 'SUCCESS'){
		if(getTotalNotificationCount($userId) == 0){
			makePutRequest($userId);
		}else{
			makeDecrementRequest($userId);
		}		
	}
}

function getTotalNotificationCount($userId){
	$url = HostConfig::$notificationsUrl.'/searchbyuser?user_id='.$userId;
	$method = 'GET';
	$headers = array();
	$headers[] = 'Accept: application/json';
	$headers[] = 'Content-type: application/json';
	$headers[] = 'Accept-Language: en-US';
	$request = new RestRequest($url, $method);
	$request->setHttpHeaders($headers);
	$request->execute();
	$info = $request->getResponseInfo();
	$body = $request->getResponseBody();
	if($info['http_code'] == 200){
		$response = json_decode($body,true);
		if($response['NotificationResponse']['status']['statusType'] == 'SUCCESS'){
			return $response['NotificationResponse']['status']['totalCount'];
		}
	}
	return 1;
}

function makePutRequest($userId){
	$url = HostConfig::$notificationsUpsUrl.'/putAttr?userid='. $userId .'&attribute=totalNotificationCounter&value=-1&sourceid=3';
	$method = 'GET';
	$request = new RestRequest($url, $method);
	$request->setHttpHeaders($headers);
	$request->execute();
}

function makeDecrementRequest($userId){
	$url = HostConfig::$notificationsUpsUrl.'/bulkDecrement';
    $method = 'POST';

    $jsonArr = array();
    $jsonArr[$userId] = array();
    $jsonArr[$userId]['attribute'] = 'totalNotificationCounter';
    $jsonArr[$userId]['sourceid'] = '3';
    
    $data = json_encode($jsonArr);
    $request = new RestRequest($url, $method, $data);
    $headers = array();
    $headers[] = 'Accept: application/json';
    $headers[] = 'Content-type: application/json';
    $headers[] = 'Accept-Language: en-US';
    $request->setHttpHeaders($headers);
    $request->execute();
}
?>