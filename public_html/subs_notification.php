<?php
include_once("./auth.php"); 
include_once("$xcart_dir/include/func/func.notify.php");
include_once("$xcart_dir/include/func/func.utilities.php");


$notifymeEmail=filter_input(INPUT_POST, 'notifymeEmail', FILTER_SANITIZE_EMAIL);
$sku=filter_input(INPUT_POST, 'sku', FILTER_SANITIZE_NUMBER_INT);
$notifymeMobile="";
$token = $_POST["_token"];

/**
* Changes for New PDP
**/
$input = json_decode(file_get_contents('php://input'));
$newPDP = filter_var($input->{'newPDP'}, FILTER_SANITIZE_STRING);

if(!empty($newPDP)){
    $notifymeEmail= filter_var($input->{'notifymeEmail'}, FILTER_SANITIZE_EMAIL);
    $sku= filter_var($input->{'sku'}, FILTER_SANITIZE_NUMBER_INT);
    $notifymeMobile="";
    $token = $input->{'_token'};
}

if(!checkCSRF($token, "subs_notification")){
    if(!empty($newPDP)){
        header('Content-Type:application/json');
        echo json_encode(array('status' => 'Failed'));
        exit;
    }else {
        echo "Failed";
        exit;
    }
}


func_subscribe_notification_instock(trim(mysql_real_escape_string( $notifymeEmail)), trim(mysql_real_escape_string($notifymeMobile)),mysql_real_escape_string($sku));


if(!empty($newPDP)){
    header('Content-Type:application/json');
    echo json_encode(array('status' => 'Success'));
}else {
    echo "Success";
}