<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: register.php,v 1.7 2006/01/11 06:55:57 mclap Exp $
#
require_once "./auth.php";

use abtest\MABTest;
use revenue\payments\service\PaymentServiceInterface;

include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
$tracker = new BaseTracker(BaseTracker::REGISTRATION);

require_once $xcart_dir."/include/categories.php";
include_once("./include/func/func.mkcore.php");
include_once ("$xcart_dir/include/class/mcart/class.MCart.php");
$productsInCart = $XCART_SESSION_VARS["productsInCart"];
$login = $XCART_SESSION_VARS['identifiers']['C']['login'];

//FOR SECURE LOGIN 
$secureLoginFeatureGate=\FeatureGateKeyValuePairs::getBoolean('secure.login');
$secureLoginABTest=$_MABTestObject->getUserSegment("secureSignin");
if($secureLoginABTest == 'secure' && $secureLoginFeatureGate){
	$secureLoginEnabled = true; 
}
else{
	$secureLoginEnabled = false; 
}

if(empty($secureLoginEnabled)){
	func_header_location($http_location."/index.php", false);
	exit;
}

if(empty($_SERVER['HTTPS'])){
	//Page to be accessed only over http redirect him to https
	 func_header_location($https_location .$_SERVER['REQUEST_URI'],false);
	 exit;
}

if($login && !PaymentServiceInterface::showLoginPromptForUser($login)){
	func_header_location($http_location."/index.php", false);
}

//get the referret value and put it in SESSION
if (!x_session_is_registered("secureloginreferrer")){ 
	$redirectUrl = filter_input(INPUT_GET, 'referrer', FILTER_SANITIZE_STRING);
	x_session_register("secureloginreferrer",$redirectUrl);
}
	
$tracker->fireRequest();

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setContextualPageData(AnalyticsBase::REGISTER);
$analyticsObj->setTemplateVars($smarty);

$newSignupAB = MABTest::getInstance()->getUserSegment('newSignupAB');


$smarty->assign("referer", str_replace('[AT]', '@', $referer));
$smarty->assign("newSignupAB", $newSignupAB);
$smarty->assign("newRegister",1);
$smarty->display("register-new.tpl");


