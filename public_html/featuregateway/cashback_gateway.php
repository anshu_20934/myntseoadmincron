<?php

include_once "../auth.php";
function gatewayStatus($login='')
{
	global $cashback_feature_gateway, $weblog;
	$weblog->info("entered function");
	if($cashback_feature_gateway == 'none')	{
		return "off";
	}
	
	if($cashback_feature_gateway == 'all') {
		return "on";
	}

	if(empty($login) && ($cashback_feature_gateway == "internal")) {
		return "off";
	} 
	
	if(!empty($login) && ($cashback_feature_gateway == "internal")) {
	
		// config is internal
		$pattern="/@myntra.com$/";
		$retval = preg_match($pattern, $login);
		if($retval == 0) {
			return "off";
		} else {
			return "on";
		}
	}
}
?>
