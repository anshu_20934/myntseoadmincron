<?php
require_once __DIR__.'/checkout_common.php';
include_once __DIR__.'/captcha/SimpleCaptcha.php';

//Check whether User account is suspended , if yes then invalidate the session and redirect to home page
$esc_login = mysql_real_escape_string($login);
$userStatus = func_query_first_cell("SELECT status FROM $sql_tbl[customers] WHERE login='$esc_login'");
if($userStatus == 'N'){
        clearSessionVarsOnLogout(true);
        func_header_location($http_location,false);
}

chdir(dirname(__FILE__)."/captcha");
$captch = new SimpleCaptcha();
$captchaText = $captch->GetCaptchaText();
global $XCART_SESSION_VARS;
$XCART_SESSION_VARS["captchaCache"]["codVerificationPage"]=$captchaText;
$XCART_SESSION_VARS['captchasAtMyntra']["codVerificationPage"] = $captchaText;

$giftcardCaptchaText = $captch->GetCaptchaText();
$XCART_SESSION_VARS["captchaCache"]["giftCaptcha"]=$giftcardCaptchaText;
$XCART_SESSION_VARS['captchasAtMyntra']["giftCaptcha"] =$giftcardCaptchaText;
