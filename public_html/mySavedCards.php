<?php

include_once("./auth.php");
include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
include_once "$xcart_dir/include/func/func.mkcore.php";
if (empty($login)) {
	echo "<meta http-equiv=\"Refresh\" content=\"0;URL=$http_location/register.php?view=savedcards\" >";
    exit;
}
if(empty($_SERVER['HTTPS'])){
	//Page to be accessed only over http redirect him to https
	func_header_location($https_location .$_SERVER['REQUEST_URI'],false);
}
$pageName = 'mymyntra';
include_once $xcart_dir."/webengage.php";
include_once $xcart_dir."/gtm.php";

$tracker = new BaseTracker(BaseTracker::PAYMENT_PAGE);
//get countries list
$countryList = func_get_countries();
$PCIDSSCompliant = \FeatureGateKeyValuePairs::getBoolean('payments.service.PCI-DSS-Compliant') || in_array($login, \FeatureGateKeyValuePairs::getStrArray('payments.savedcards.override'));
$smarty->assign('PCIDSSCompliant',$PCIDSSCompliant);
//Separating expressbuy and stored cards. 
$mymyntraExpressBuy = \FeatureGateKeyValuePairs::getBoolean('payments.mymyntra.expressbuy.enabled');
$smarty->assign('mymyntraExpressBuy',$mymyntraExpressBuy);
$mymyntraStoredCards = \FeatureGateKeyValuePairs::getBoolean('payments.mymyntra.storedcards.enabled');
$smarty->assign('mymyntraStoredCards',$mymyntraStoredCards);

$secureSessionExpireMessage = WidgetKeyValuePairs::getWidgetValueForKey('login.secureSessionExpireMessage');

if(empty($PCIDSSCompliant)){
	func_header_location($http_location."/mymyntra.php",false);
}
$smarty->assign("paySerHost",HostConfig::$paymentServiceHost);
$smarty->assign("secureSessionExpireMessage",$secureSessionExpireMessage);
$smarty->assign('countryList',json_encode($countryList));
$smarty->assign('expressCheckout',true);
$smarty->assign("notAllowedMsg","Access not allowed");
$smarty->display("my/saved-cards.tpl");