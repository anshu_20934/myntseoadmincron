<?php

use revenue\payments\gateway\AtomGateway;
use revenue\payments\gateway\CODGateway;
use revenue\payments\gateway\AxisBankGateway;
use revenue\payments\dao\AxisBankDAO;
use revenue\payments\gateway\CCAvenueGateway;
use revenue\payments\gateway\EBSGateway;
use revenue\payments\gateway\HDFCBankGateway;
use revenue\payments\gateway\ICICIBankGateway;
use revenue\payments\gateway\CITIBankGateway;
use revenue\payments\gateway\TekProcessGateway;
use revenue\payments\gateway\BillDeskGateway;

use enums\revenue\payments\CardType;
use enums\revenue\payments\PaymentType;
use enums\revenue\payments\Gateways;

if($prev_status=='PP' || $prev_status == 'PV'||$prev_status == 'D'){
		} else {
			func_header_location($redirectToInCaseOfReload,false);
		}
		
	 	if($_GET['type']=='cod' && $paymentoption==PaymentType::COD) {
			$gatewayWentTo = Gateways::COD;
	 	}		
		
		switch($gatewayWentTo) {
			case Gateways::COD:
				$gateway = new CODGateway();
				$gatewayResponse = $_GET;
				break;
			case Gateways::AxisBank:
				$gateway = new AxisBankGateway(null, null, null, null);
				$gatewayResponse = $_GET;
				if(empty($gatewayResponse)) $gatewayResponse = $_POST;
				$axisBankDAO = new AxisBankDAO();
				$axisBankDAO->insertIntoDB($orderid, $login, $gatewayResponse);
				break;
			case Gateways::ICICIBank:
			case Gateways::ICICIBank3EMI:
			case Gateways::ICICIBank6EMI:				
				$gateway = new ICICIBankGateway(null, null, null, null);
				$gatewayResponse = func_query_first("select status, purchaseamount, response, epg_trasaction_id, gateway_transaction_id, gateway_cvv_response_code, gateway_response_code, gateway_response_message from mk_icici_payments_log where orderid = '$orderid'");
				break;
			case Gateways::CCAvenue:
				$gateway = new CCAvenueGateway(null, null, null, null, null, null);
				$gatewayResponse = $_POST;
				break;
			case Gateways::EBS:
				$gateway = new EBSGateway(null, null);
				$gatewayResponse = $_GET;
				break;
			case Gateways::tekProcess:
				$gateway = new TekProcessGateway(null, null);
				$tek_msg=trim($_REQUEST['msg']);
				$gatewayResponse=explode("|",$tek_msg);
				break;
			case Gateways::HDFCBank:
				$gateway = new HDFCBankGateway(null, null, null, null);
				$gatewayResponse = $_POST;
				break;
	case Gateways::CITIBank3EMI:
	case Gateways::CITIBank6EMI:
				$gateway = new CITIBankGateway(null, null, null, null);
				$gatewayResponse = $_POST;
				break;
			case Gateways::BillDesk:
				$gateway = new BillDeskGateway(null, null);
				$gatewayResponse = $_POST;
				break;
			default:
				func_header_location($redirectToInCaseOfReload,false);
		}
		
		unset($_POST);
		unset($HTTP_POST_VARS);
		try {
			$gateway->setOrderId($orderid);
			$gateway->verifyPaymentData($gatewayResponse, $amountToBePaid, $prev_status);
			$toLog = $gateway->updateLogData($orderid);
                        $gateway->pushMetrics($orderLogDetails);
			$isPaymentDataCorrect = $gateway->getPaymentDataCorrect();
			$isPaymentSuccessfull = $gateway->getPaymentCompleted();
			$isPaymentValid = $gateway->validatePayment();
		} catch (Exception $e) {
			func_header_location($https_locations . "/mkpaymentoptions.php?transaction_status=N",false);	
		}
		
		if(FeatureGateKeyValuePairs::getBoolean("validation.coupon.enabled")===true&&(!empty($couponCode)||!empty($cashCouponCode))) {
			$validator = CouponValidator::getInstance();
			if(!empty($couponCode)) {
				$usedCoupon=$couponCode;
			} else {
				$usedCoupon=$cashCouponCode;
			}
			try {
				$coupon = $adapter->fetchCoupon($usedCoupon);
				$isValid = $validator->isCouponAllowedForUse($coupon, $login);
			} catch (CouponException $ex) {
				$isPaymentDataCorrect = false;
				$couponValidationFailedMessage = "\nCoupon ($usedCoupon) validation has failed. [{$ex->getMessage()}]";
			}
		
		}
?>
