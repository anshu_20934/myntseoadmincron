<?php
	
	function func_get_serviceable_couriers_for_zipcode_old($zipcode) {
		global $sql_tbl;
		$supported_courier_results = PincodeApiClient::getCouriersForPincode($zipcode);
		$couriers = get_supported_courier_partners();
		$courierCode2NameMap = array();
		foreach($couriers as $courier_row) {
			$courierCode2NameMap[$courier_row['code']] = $courier_row['display_name']; 
		}
		foreach ($supported_courier_results as $index=>$row) {
			$supported_courier_results[$index]['courierName'] = $courierCode2NameMap[$row['courierCode']];
		}
		
		return $supported_courier_results;
	}
	
	function func_get_serviceable_payment_modes_for_zipcode($zipcode) {
		$supported_courier_results = PincodeApiClient::getCouriersForPincode($zipcode);
		$serviceable_payment_modes = array();
		foreach ($supported_courier_results as $index=>$row) {
			if($row["codSupported"] == 1){
				$serviceable_payment_modes[] = 'cod';
			}
			if($row["nonCodSupported"] == 1){
				$serviceable_payment_modes[] = 'on';
			}
		}
		return array_values(array_unique($serviceable_payment_modes));
	}
	
	function is_zipcode_servicable($zipcode) {
		$supported_courier_results = PincodeApiClient::getCouriersForPincode($zipcode);
		foreach ($supported_courier_results as $row){
			if($row['codSupported'] == 1 || $row['nonCodSupported'] == 1 || $row['pickupSupported'] == 1)
				return true;
		}
		return false;
	}

	function func_check_cod_serviceability($zipcode, $codAllZipCodeEnable=false,$jewelleryItemsInOrder='none') {
		if($codAllZipCodeEnable){
			return true;
		}
		$jewellerySupportingCouriers = explode(',', trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('finejewellery.supporting.couriers')));
		$generalServiceability = false;
		$jewelleryServiceability = false;
		
		$supported_courier_results = PincodeApiClient::getCouriersForPincode($zipcode);
		foreach ($supported_courier_results as $index=>$row) {
			if(!$jewelleryServiceability && in_array($row["courierCode"], $jewellerySupportingCouriers) && $row["codSupported"] == 1){
				$jewelleryServiceability = true;
			} 
			if(!$generalServiceability && !in_array($row["courierCode"], $jewellerySupportingCouriers) && $row["codSupported"] == 1){
				$generalServiceability = true;
			}
		}
		switch($jewelleryItemsInOrder){
			case 'none':
				return $generalServiceability;
			case 'all':
				return $jewelleryServiceability;
			case 'some':
				return ($generalServiceability && $jewelleryServiceability);
		}
		return false;
	}
	
	function func_check_prepaid_serviceability($zipcode,$jewelleryItemsInOrder='none') {
		$jewellerySupportingCouriers = explode(',', trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('finejewellery.supporting.couriers')));
		$generalServiceability = false;
		$jewelleryServiceability = false;
		
		$supported_courier_results = PincodeApiClient::getCouriersForPincode($zipcode);
		foreach ($supported_courier_results as $index=>$row) {
			if(!$jewelleryServiceability && in_array($row["courierCode"], $jewellerySupportingCouriers) && $row["nonCodSupported"] == 1){
				$jewelleryServiceability = true;
			} 
			if(!$generalServiceability && !in_array($row["courierCode"], $jewellerySupportingCouriers) && $row["nonCodSupported"] == 1){
				$generalServiceability = true;
			}
		}
		switch($jewelleryItemsInOrder){
			case 'none':
				return $generalServiceability;
			case 'all':
				return $jewelleryServiceability;
			case 'some':
				return ($generalServiceability && $jewelleryServiceability);
		}
		return false;
	}
	
	function func_check_courier_prepaid_serviceability($zipcode, $courier) {
		$supported_courier_results = PincodeApiClient::getCouriersForPincode($zipcode);
		
		foreach ($supported_courier_results as $index=>$row) {
			if($row["courierCode"] == $courier && $row["nonCodSupported"] ==1){
				return true;
			}
		}
		return false;
	}

	function func_check_courier_cod_serviceability($zipcode, $courier) {
		$supported_courier_results = PincodeApiClient::getCouriersForPincode($zipcode);
		foreach ($supported_courier_results as $index=>$row) {
			if($row["courierCode"] == $courier && $row["codSupported"] == 1){
				return true;
			}
		}
		return false;
	}
		
	function func_get_serviceable_couriers($zipcode, $payment_method, $country) {
		$couriers = array();
		$supported_courier_results = PincodeApiClient::getCouriersForPincode($zipcode);
		if(!empty($supported_courier_results)) {
			$couriers = array();
			foreach ($supported_courier_results as $row){
				if($payment_method == 'cod') {
					if($row["codSupported"] == 1){
						$couriers[] = $row['courierCode'];
					}
				} else {
					if($row["nonCodSupported"] == 1){
						$couriers[] = $row['courierCode'];
					}
				}
			}
		}

		if($country != 'IN' && $country != 'RU' && $payment_method != 'cod') {
			$couriers[] = 'DH';
		}
		return $couriers;
	}
	
	function func_check_pickup_availability($zipcode) {
		global $sql_tbl;
		$supported_courier_results = PincodeApiClient::getCouriersForPincode($zipcode);
		$couriers = array();
		$jewellerySupportingCouriers = explode(',', trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('finejewellery.supporting.couriers')));
		foreach ($supported_courier_results as $row){
			if(!in_array($row["courierCode"], $jewellerySupportingCouriers)){
				if($row["pickupSupported"] == 1){
					$couriers[] = $row['courierCode'];
				}
			}
		}

		$courierFinal = array();
		if(!empty($couriers)){
			$query = "SELECT code as name from mk_courier_service cs " .
				"where cs.code in ('". implode("','", $couriers) ."') and cs.return_supported = 1 order by ifnull(return_pickup_order,1000) limit 1";
			$results = func_query($query, true);
			if($results) {
				foreach ($results as $row) {				
					$courierFinal[] = $row['name'];
				}
			}	
		}
		return $courierFinal;;
	}
	
	function func_check_exchange_serviceability($zipcode) {
		global $sql_tbl, $weblog;
		$couriers = array();
		$supported_courier_results = PincodeApiClient::getCouriersForPincode($zipcode);
		$weblog->info(print_r($supported_courier_results, true));
		$jewellerySupportingCouriers = explode(',', trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('finejewellery.supporting.couriers')));
		foreach ($supported_courier_results as $row){
			if(!in_array($row["courierCode"], $jewellerySupportingCouriers)){
				if($row["exchangeSupported"] == 1){
					$couriers[] = $row['courierCode'];
				}
			}
		}
	
		$courierFinal = array();
		if(!empty($couriers)){
			$query = "SELECT code as name from mk_courier_service cs " .
					"where cs.code in ('". implode("','", $couriers) ."') and cs.return_supported = 1 order by ifnull(return_pickup_order,1000) limit 1";
			$results = func_query($query, true);
			if($results) {
				foreach ($results as $row) {
					$courierFinal[] = $row['name'];
				}
			}
		}
		return $courierFinal;
	}
	
   	/*
   	 * The function was added just for testing.
   	 * 
   	 * function test_serviceability($zipcode){
		global $weblog;
		if(func_check_cod_serviceability($zipcode, true)){
			$weblog->debug("func_check_cod_serviceability($zipcode, true)" . "- success");
		}else{
			$weblog->debug("func_check_cod_serviceability($zipcode, true)" . "- failed");
		}
		
		if(func_check_cod_serviceability($zipcode)){
			$weblog->debug("func_check_cod_serviceability($zipcode)" . "- success");
		}else{
			$weblog->debug("func_check_cod_serviceability($zipcode)" . "- failed");
		}
				
		if(func_check_courier_cod_serviceability($zipcode, 'ML')){
			$weblog->debug("func_check_courier_cod_serviceability($zipcode, 'ML')" . "- success");
		}else{
			$weblog->debug("func_check_courier_cod_serviceability($zipcode, 'ML')" . "- failed");
		}
		
		if(func_check_courier_cod_serviceability($zipcode, 'BD')){
			$weblog->debug("func_check_courier_cod_serviceability($zipcode, 'BD')" . "- success");
		}else{
			$weblog->debug("func_check_courier_cod_serviceability($zipcode, 'BD')" . "- failed");
		}
		
		$weblog->debug("func_check_courier_prepaid_serviceability($zipcode, 'ML'): " . print_r(func_check_courier_prepaid_serviceability($zipcode, 'ML'), true));
		$weblog->debug("func_check_courier_prepaid_serviceability($zipcode, 'BD'): " . print_r(func_check_courier_prepaid_serviceability($zipcode, 'BD'), true));
		$weblog->debug("is_zipcode_servicable($zipcode): " . print_r(is_zipcode_servicable($zipcode), true));
		$weblog->debug("func_check_pickup_availability($zipcode): " . print_r(func_check_pickup_availability($zipcode), true));
		
	}*/
?>
