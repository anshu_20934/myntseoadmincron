<?php

/**
 * This file contains the utility functions for images.
 */
include_once("func.mkcore.php");
include_once($xcart_dir."/mkImageTransform.php");
include_once($xcart_dir."/include/TempImages.php");
include_once("func.randnum.php");
if (!defined('XCART_START'))
    {
    header ("Location: ../");
    die ("Access denied");
    }

function resizeImageToDimension($sourceImagePath,$targetImagePath,$resizeWidth,$resizeHeight){
    global $weblog,$sqllog;
    $weblog->info("Function Name : resizeImageToDimension() File Name : ".__FILE__);
    $weblog->info("Call  : mkImageTransform class() File Name : ".__FILE__);
	$imageManipulator = new mkImageTransform();
	$weblog->info("Source Image File  : $sourceImagePath File Name : ".__FILE__);
	$weblog->info("Target Image File  : $targetImagePath File Name : ".__FILE__);
	$imageManipulator->sourceFile = $sourceImagePath;
	$imageManipulator->targetFile = $targetImagePath;
	list($width_orig, $height_orig) = getimagesize($sourceImagePath);
	$ratio_orig = $width_orig/$height_orig;

	//if original ratio is greater than 1, width is greater, set resizeToWidth
	//else set resizeToHeight
	if (($width_orig > $resizeWidth) && ($width_orig >= $height_orig)){
	    $widthinCA = $resizeWidth;
	    $heightinCA = $widthinCA/$ratio_orig;
	    if ($heightinCA > $resizeHeight){
	    	$heightinCA = $resizeHeight;
	    	$widthinCA = $heightinCA * $ratio_orig;
	    }
	}elseif (($height_orig >= $resizeHeight) && ($height_orig >= $width_orig)){
	    $heightinCA = $resizeHeight;
	    $widthinCA = $ratio_orig * $heightinCA;
	    if ($widthinCA > $resizeWidth){
	    	$widthinCA = $resizeWidth;
	    	$heightinCA = $widthinCA/$ratio_orig;
	    }
	}elseif (($width_orig <= $resizeWidth) && ($width_orig >= $height_orig)){
	    $widthinCA = $width_orig;
	    $heightinCA = $widthinCA/$ratio_orig;
	    if ($heightinCA > $resizeHeight){
	    	$heightinCA = $resizeHeight;
	    	$widthinCA = $heightinCA * $ratio_orig;
	    }
	}elseif (($height_orig < $resizeHeight) && ($height_orig >= $width_orig)){
	    $heightinCA = $height_orig;
	    $widthinCA = $ratio_orig * $heightinCA;
	    if ($widthinCA > $resizeWidth){
	    	$widthinCA = $resizeWidth;
	    	$heightinCA = $widthinCA/$ratio_orig;
	    }
	}

	$imageManipulator->resizeToWidth = $widthinCA;
	$imageManipulator->resizeToHeight = $heightinCA;

	$success = $imageManipulator->resize();
	if (!$success) { //TODO decide what to do ??
		echo $imageManipulator->error;
	}
}

function generateFinalCustomizationAreaImageWithDesign($customizationArray,$caId,$oId,$originalUploadedImagePath,$type,$targetDir="./images/customized")
{

	global $weblog ;
	$weblog->info("Inside generateFinalCustomizationAreaImageWithDesign function in func.mkimage.php");
	$weblog->info("orignal image path : $originalUploadedImagePath");
	if($type == "ifs")$type="jpg";
    
	$root = substr($targetDir, 0, strpos($targetDir, 'images'));
	$originalUploadedImagePath = $root.strstr($originalUploadedImagePath, 'images');

	$customizedImages = array();
	$weblog->info("called func_get_height_width_for_orientation function with arg orientation id : $oId");
	$orientationDetails = func_get_height_width_for_orientation($oId);
	$weblog->info("end of func_get_height_width_for_orientation");
	//setting the height and width in inches
	$height_in = $orientationDetails[8];
	$width_in = $orientationDetails[9];

	$width_in_pixels = $width_in * 300;//TODO need to confirm about the screen dpi(72/96)
	$height_in_pixels = $height_in * 300;
	$image = imagecreatetruecolor($width_in_pixels,$height_in_pixels);
	imagesavealpha($image,true);

	// creating transparent image for text
	$textImage = imagecreatetruecolor($width_in_pixels,$height_in_pixels);
	imagesavealpha($textImage,true);
	imagealphablending($textImage,false);
	$transparentColor = imagecolorallocatealpha($textImage, 100, 100, 200, 127);
	imagefill($textImage, 0, 0, $transparentColor);

	// check if the bg color is set, the background needs to be painted in that color
	if (isset($customizationArray['bgcolor'])){
	    $bgcolor = html2rgb($customizationArray['bgcolor']);
	    list($rgb, $r, $g, $b) = split('[(,)]', $bgcolor);
	    $backgroundColor = imagecolorallocate($image,$r,$g,$b);
		imagefill($image,0,0,$backgroundColor);
		// else the background is transparent
	}else{
		imagealphablending($image,false);
		$transparentColor = imagecolorallocatealpha($image, 100, 100, 200, 127);
		imagefill($image, 0, 0, $transparentColor);
	}
	// get the scaling factor
	$xScalingFactor = $width_in_pixels/$orientationDetails[4];
	$yScalingFactor = $height_in_pixels/$orientationDetails[5];
	// check if the image has been uploaded or is it a text only design
	$weblog->info("check the uploaded image path");
	if ($originalUploadedImagePath != ""){
		// get the new scaled up starting coordinates for the image in customization area
		$xStart = $customizationArray['dxstart'] * $xScalingFactor;
		$yStart = $customizationArray['dystart'] * $yScalingFactor;
		// scale up and resize the image, taking care of any rotation
		$designImageInCustArea = $root.strstr($customizationArray['pastefile'], 'images');
		list($width_in_CA, $height_in_CA) = getimagesize($designImageInCustArea);
		$width_in_final_design = $width_in_CA * $xScalingFactor;
		$height_in_final_design = $height_in_CA * $yScalingFactor;
		// get the original image and do transformations on it to get a better quality
		$weblog->info("Resize the image by calling function mkImageTransform class");
		$imageManipulator = new mkImageTransform();
		$imageManipulator->sourceFile = $originalUploadedImagePath;
		
		if ($type == "jpg"  || $type == "jpeg"){
			$imageManipulator->jpegOutputQuality  = 100;
		}
		if ($customizationArray['rotation'] != 0){
			$imageManipulator->targetFile = $targetDir."/tempgenerateddesign".time().get_rand_id(4).".".$type;
			$imageManipulator->rotate($customizationArray['rotation'],0);
			$imageManipulator->sourceFile = $imageManipulator->targetFile;
		}

		$imageManipulator->targetFile = $targetDir."/finalgenerateddesign".time().get_rand_id(4).".".$type;

		$imageManipulator->resizeToWidth =  $width_in_final_design;
		$imageManipulator->resizeToHeight = $height_in_final_design;
		$imageManipulator->resize();
		
		// now paint the image
		if ($type == "jpg" || $type == "jpeg"){
			$scaledDesignImage = imagecreatefromjpeg($imageManipulator->targetFile);
		}else{
			$scaledDesignImage = imagecreatefrompng($imageManipulator->targetFile);
		}
		//echo $imageManipulator->targetFile ;

		
		// The correction logic below has been put in to avoid the approximation error that was happening because
		// the pixels cannot be in fractions. The scaled image generated above has a width which is less than what the actual
		// scaled width should be. The correction is being distributed on both sides of the image as of now.
		$xcorrection = $width_in_final_design - imagesx($scaledDesignImage);
		$ycorrection = $height_in_final_design - imagesy($scaledDesignImage);
		$width_in_final_design = imagesx($scaledDesignImage);
		$height_in_final_design = imagesy($scaledDesignImage);
		$xStart = $xStart + $xcorrection/2;
		$yStart = $yStart + $ycorrection/2;
		// added the latest components to master customization area
		$customizedImages['width_in_final']= $width_in_final_design;
		$customizedImages['height_in_final']= $height_in_final_design;
		$customizedImages['dxstart_image']= $xStart;
		$customizedImages['dystart_image']= $yStart;
		// hold up all the clean up images
		$customizedImages['finalgeneratedcleanUpimage'] =  $imageManipulator->targetFile; ### giving the wrong path //substr($imageManipulator->targetFile,4);

		imagecopyresampled($image,$scaledDesignImage,$xStart,$yStart,0,0,$width_in_final_design,$height_in_final_design,$width_in_final_design,$height_in_final_design);
	
	}
	// next is the text
	$customizedTextContent = $customizationArray['customizedtext'];
	$weblog->info("FONT TEXT >>>>>>>> ".$customizedTextContent['fonttext']);
	$weblog->info("FONT TYPE >>>>>>>> ".$customizedTextContent['fonttype']);

	if ($customizedTextContent['fonttext'] != ""){
		$customizedImages['dxstart_text']= $customizedTextContent['textX']*$xScalingFactor;
	    $customizedImages['dystart_text']= $customizedTextContent['textY']*$yScalingFactor;
		imageTextWrapped($image,$customizedTextContent['textX']*$xScalingFactor,$customizedTextContent['textY']*$yScalingFactor,$width_in_pixels,$customizedTextContent['fonttype'],$customizedTextContent['fontcolor'],$customizedTextContent['fonttext'],$customizedTextContent['fontsize']*$xScalingFactor,'c');
		imageTextWrapped($textImage,$customizedTextContent['textX']*$xScalingFactor,$customizedTextContent['textY']*$yScalingFactor,$width_in_pixels,$customizedTextContent['fonttype'],$customizedTextContent['fontcolor'],$customizedTextContent['fonttext'],$customizedTextContent['fontsize']*$xScalingFactor,'c');
	}
    $weblog->info("Creating the large image and thumbnail image");

	$finalCustomizedDesignLargeImage = $targetDir."/customized_design_images/L/finalcustomizeddesign".time().get_rand_id(4).".png";
	$finalCustomizedTextImage = $targetDir."/customized_design_images/L/finalcustomizedtext".time().get_rand_id(4).".png";
    $weblog->info("Large Image : $finalCustomizedDesignLargeImage");
    $weblog->info("Creating the large image of text with path > ".$finalCustomizedTextImage);
	if(!imagepng($image,$finalCustomizedDesignLargeImage))
		$weblog->info("Unable to create image :$finalCustomizedDesignLargeImage");
    addToTempImagesList($finalCustomizedDesignLargeImage);

	if(!imagepng($textImage,$finalCustomizedTextImage))
	   $weblog->info("Unable to create image :$finalCustomizedTextImage");
    addToTempImagesList($finalCustomizedTextImage);
	
	$finalCustomizedDesignThumbnailImage = $targetDir."/customized_design_images/T/finalcustomizeddesign".time().get_rand_id(4).".png";
	$customizedImages['finalthumbnailcleanUpimage']=$finalCustomizedDesignThumbnailImage;
	$weblog->info("Thumbnail image :$finalCustomizedDesignThumbnailImage");
	// now resize the actual generated image which would be shown on the start shopping screen
	resizeImageToDimension($finalCustomizedDesignLargeImage,$finalCustomizedDesignThumbnailImage,100,100);
	// now return back the array containing the location of the images
	$customizedImages['large'] = $finalCustomizedDesignLargeImage;
	$customizedImages['thumbnail'] = $finalCustomizedDesignThumbnailImage;
	//added text image to master customization area
	$customizedImages['textimage'] = $finalCustomizedTextImage;
	//end
	$weblog->info("end calling generateFinalCustomizationAreaImageWithDesign function");
	return $customizedImages;
}

function imageTextWrapped(&$img, $x, $y, $width, $font, $color, $text, $textSize, $align="l") {
   //Recalculate X and Y to have the proper top/left coordinates instead of TTF base-point
   $y += $textSize;
   $dimensions = imagettfbbox($textSize, 0, $font, " "); //use a custom string to get a fixed height.
   $x -= $dimensions[4]-$dimensions[0];

   $text = str_replace ("\r", '', $text); //Remove windows line-breaks
   $srcLines = split ("\n", $text); //Split text into "lines"
   $dstLines = Array(); // The destination lines array.
   foreach ($srcLines as $currentL) {
       $line = '';
       $words = split (" ", $currentL); //Split line into words.
       foreach ($words as $word) {
           $dimensions = imagettfbbox($textSize, 0, $font, $line.$word);
           $lineWidth = $dimensions[4] - $dimensions[0]; // get the length of this line, if the word is to be included
           if ($lineWidth > $width && !empty($line) ) { // check if it is too big if the word was added, if so, then move on.
               $dstLines[] = ' '.trim($line); //Add the line like it was without spaces.
               $line = '';
           }
           $line .= $word.' ';
       }
       $dstLines[] =  ' '.trim($line); //Add the line when the line ends.
   }
   //Calculate lineheight by common characters.
   $dimensions = imagettfbbox($textSize, 0, $font, "MXQJPmxqjp123"); //use a custom string to get a fixed height.
   $lineHeight = $dimensions[1] - $dimensions[5]; // get the heightof this line

   $align = strtolower(substr($align,0,1)); //Takes the first letter and converts to lower string. Support for Left, left and l etc.
   
   foreach ($dstLines as $nr => $line) {
     if ($align != "l") {
           $dimensions = imagettfbbox($textSize, 0, $font, $line);
           $lineWidth = $dimensions[4] - $dimensions[0]; // get the length of this line
           if ($align == "r") { //If the align is Right
               $locX = $x + $width - $lineWidth;
           } else { //If the align is Center
               $locX = $x + ($width/2) - ($lineWidth/2);
           }
       } else { //if the align is Left
           $locX = $x;
       }
       $locY = $y + ($nr * $lineHeight);
       //Print the line.
        imagettftext($img, $textSize, 0, $locX, $locY, $color, $font, $line);
   }
}

function html2rgb($color)
{
    if ($color[0] == '#') {
        $color = substr($color, 1);
    }
    else {
	  return $color;
    }

    if (strlen($color) == 6)
        list($r, $g, $b) = array($color[0].$color[1],
                                 $color[2].$color[3],
                                 $color[4].$color[5]);
    elseif (strlen($color) == 3)
        list($r, $g, $b) = array($color[0], $color[1], $color[2]);
    else
        return false;

    $r = hexdec($r); $g = hexdec($g); $b = hexdec($b);

    return "rgb(".$r.",".$g.",".$b.")";
}

function generateFinalImages(&$masterCustomizationArray,$chosenProductConfiguration,$targetDir="./images/customized"){

	//$masterCustomizationArray = $XCART_SESSION_VARS['masterCustomizationArray'];
	//$chosenProductConfiguration = $XCART_SESSION_VARS['chosenProductConfiguration'];
	$cleanUpArray = array();
	foreach($masterCustomizationArray as $key=>$value){
		$caId = $key;
		$customizationArray = $value;
		$oId = $customizationArray['orientationid'];
		$imagekey= $caId."uploadedImagePath";
		$type = strtolower(substr($chosenProductConfiguration[$imagekey], strrpos($chosenProductConfiguration[$imagekey], ".") + 1));
		    $returnImage = generateFinalCustomizationAreaImageWithDesign($customizationArray,$caId,$oId,$chosenProductConfiguration[$imagekey],$type,$targetDir);
			$masterCustomizationArray[$caId][$caId]['finalcustomizeimage']= $returnImage['large'];
			$masterCustomizationArray[$caId][$caId]['finalcustomizeThumbimage']= $returnImage['thumbnail'];
			//added text image to master customization area
			$masterCustomizationArray[$caId][$caId]['finaltextimage']= $returnImage['textimage'];
			//end
			$masterCustomizationArray[$caId]['width_in_final']=$returnImage['width_in_final'];
		    $masterCustomizationArray[$caId]['height_in_final']= $returnImage['height_in_final'];
		    $masterCustomizationArray[$caId]['dxstart_image']= $returnImage['dxstart_image'];
		    $masterCustomizationArray[$caId]['dystart_image'] = $returnImage['dxstart_image'];
		    $masterCustomizationArray[$caId]['dxstart_text']= $returnImage['dxstart_text'];
		    $masterCustomizationArray[$caId]['dystart_text']= $returnImage['dxstart_text'];
			addToTempImagesList($returnImage['finalgeneratedcleanUpimage']);
		   
		
	}

	//x_session_register('masterCustomizationArray');
}
###########  generate images for photo album ################# 
function generateFinalImagesForPhotoAlbum(&$masterCustomizationArray,$chosenProductConfiguration,&$imageCleanUpList=null,$targetDir="./images/customized"){
	$cleanUpArray = array();
	foreach($masterCustomizationArray as $key=>$_pageData){ ### key=> Page_no & and value is info within that page
		$pageId = $key;
		list($pgText,$pageNumber) = explode("_",$pageId);
		foreach($_pageData as $_k => $_val){
			if(is_array($_val)){
				$orientationId = $_k;
				$customizationArray = $_val;
				$caId = $chosenProductConfiguration['customizationAreaId'];
				$imagekey= $pageNumber."_".$orientationId."_".$caId."_uploadedImagePath";
				$type = strtolower(substr($chosenProductConfiguration[$imagekey], strrpos($chosenProductConfiguration[$imagekey], ".") + 1));
				$returnImage = generateFinalCustomizationAreaImageWithDesign($customizationArray,$caId,$orientationId,$chosenProductConfiguration[$imagekey],$type,$targetDir);
				$masterCustomizationArray[$pageId][$orientationId]['finalcustomizeimage']= $returnImage['large'];
				$masterCustomizationArray[$pageId][$orientationId]['finalcustomizeThumbimage']= $returnImage['thumbnail'];
					//added text image to master customization area
				$masterCustomizationArray[$pageId][$orientationId]['finaltextimage']= $returnImage['textimage'];
				//end
				$masterCustomizationArray[$pageId][$orientationId]['width_in_final'] = $returnImage['width_in_final'];
			    $masterCustomizationArray[$pageId][$orientationId]['height_in_final']= $returnImage['height_in_final'];
			    $masterCustomizationArray[$pageId][$orientationId]['dxstart_image']= $returnImage['dxstart_image'];
			    $masterCustomizationArray[$pageId][$orientationId]['dystart_image'] = $returnImage['dxstart_image'];
			    $masterCustomizationArray[$pageId][$orientationId]['dxstart_text']= $returnImage['dxstart_text'];
			    $masterCustomizationArray[$pageId][$orientationId]['dystart_text']= $returnImage['dxstart_text'];
				addToTempImagesList($returnImage['finalgeneratedcleanUpimage']);
			}
		}
		   
		
	}

	//x_session_register('masterCustomizationArray');
}  

function generateImagesForProducts(&$masterCustomizationArray,$chosenProductConfiguration,&$imageCleanUpList=null,$targetDir="./images/customized"){
	$cleanUpArray = array();
	foreach($masterCustomizationArray as $customization => $orientationInfo){ ### key=> customization id & and value is info within that page
		
			   foreach($orientationInfo as $_oid => $_val){
				if(is_array($_val)){
                  
					$orientationId = $_oid;
					$customizationArray = $_val;
					$caId = $customization;
					$imagekey= $customization."_".$orientationId."_uploadedImagePath";
					$type = strtolower(substr($chosenProductConfiguration[$imagekey], strrpos($chosenProductConfiguration[$imagekey], ".") + 1));
					if(!empty($chosenProductConfiguration["selectedtemplate"])){
						$tplid = $chosenProductConfiguration["selectedtemplate"];
						$returnImage = generateFinalCustomizationAreaImageWithDesignForTemplate($customizationArray,$customization,$orientationId,$chosenProductConfiguration[$imagekey],$type,$targetDir,$tplid);
					}else{
						$returnImage = generateFinalCustomizationAreaImageWithDesign($customizationArray,$customization,$orientationId,$chosenProductConfiguration[$imagekey],$type,$targetDir);
					}
					$masterCustomizationArray[$customization][$orientationId]['finalcustomizeimage']= $returnImage['large'];
					$masterCustomizationArray[$customization][$orientationId]['finalcustomizeThumbimage']= $returnImage['thumbnail'];
						//added text image to master customization area
					$masterCustomizationArray[$customization][$orientationId]['finaltextimage']= $returnImage['textimage'];
					//end
					$masterCustomizationArray[$customization][$orientationId]['width_in_final'] = $returnImage['width_in_final'];
				    $masterCustomizationArray[$customization][$orientationId]['height_in_final']= $returnImage['height_in_final'];
				    $masterCustomizationArray[$customization][$orientationId]['dxstart_image']= $returnImage['dxstart_image'];
				    $masterCustomizationArray[$customization][$orientationId]['dystart_image'] = $returnImage['dxstart_image'];
				    $masterCustomizationArray[$customization][$orientationId]['dxstart_text']= $returnImage['dxstart_text'];
				    $masterCustomizationArray[$customization][$orientationId]['dystart_text']= $returnImage['dxstart_text'];
					addToTempImagesList($returnImage['finalgeneratedcleanUpimage']);

				}
		   }	   
		
	}
	
}

function generateFinalCustomizationAreaImageWithDesignForTemplate($customizationArray,$caId,$oId,$originalUploadedImagePath,$type,$targetDir="./images/customized",$templateid=0)
{
	 

	global $weblog ;
	$weblog->info("Inside generateFinalCustomizationAreaImageWithDesign function in func.mkimage.php");
	$weblog->info("orignal image path : $originalUploadedImagePath");
	if($type == "ifs")$type="jpg";
    
	$root = substr($targetDir, 0, strpos($targetDir, 'images'));
	$originalUploadedImagePath = $root.strstr($originalUploadedImagePath, 'images');

	require_once("func.multiple_orientation.php");   
	
	$customizedImages = array();
	$weblog->info("called func_get_height_width_for_orientation function with arg orientation id : $oId");
    $subtplid = getSubTemplateId($templateid,$caId); // load sub template id

	$orientationDetails = loadTemplateOrientation($subtplid['template_id'],$oId); // get template id
	$weblog->info("end of func_get_height_width_for_orientation");
	//setting the height and width in inches
	$height_in = $orientationDetails['height_in'];
	$width_in = $orientationDetails['width_in'];

	$width_in_pixels = $width_in * 300;//TODO need to confirm about the screen dpi(72/96)
	$height_in_pixels = $height_in * 300;
	$image = imagecreatetruecolor($width_in_pixels,$height_in_pixels);
	imagesavealpha($image,true);

	// creating transparent image for text
	$textImage = imagecreatetruecolor($width_in_pixels,$height_in_pixels);
	imagesavealpha($textImage,true);
	imagealphablending($textImage,false);
	$transparentColor = imagecolorallocatealpha($textImage, 100, 100, 200, 127);
	imagefill($textImage, 0, 0, $transparentColor);

	// check if the bg color is set, the background needs to be painted in that color
	if (isset($customizationArray['bgcolor'])){
	    $bgcolor = html2rgb($customizationArray['bgcolor']);
	    list($rgb, $r, $g, $b) = split('[(,)]', $bgcolor);
	    $backgroundColor = imagecolorallocate($image,$r,$g,$b);
		imagefill($image,0,0,$backgroundColor);
		// else the background is transparent
	}else{
		imagealphablending($image,false);
		$transparentColor = imagecolorallocatealpha($image, 100, 100, 200, 127);
		imagefill($image, 0, 0, $transparentColor);
	}
	// get the scaling factor
	$xScalingFactor = $width_in_pixels/$orientationDetails['width'];
	$yScalingFactor = $height_in_pixels/$orientationDetails['height'];
	// check if the image has been uploaded or is it a text only design
	$weblog->info("check the uploaded image path");
	if ($originalUploadedImagePath != ""){
		// get the new scaled up starting coordinates for the image in customization area
		$xStart = $customizationArray['dxstart'] * $xScalingFactor;
		$yStart = $customizationArray['dystart'] * $yScalingFactor;
		// scale up and resize the image, taking care of any rotation
		$designImageInCustArea = $root.strstr($customizationArray['pastefile'], 'images');
		list($width_in_CA, $height_in_CA) = getimagesize($designImageInCustArea);
		$width_in_final_design = $width_in_CA * $xScalingFactor;
		$height_in_final_design = $height_in_CA * $yScalingFactor;
		// get the original image and do transformations on it to get a better quality
		$weblog->info("Resize the image by calling function mkImageTransform class");
		$imageManipulator = new mkImageTransform();
		$imageManipulator->sourceFile = $originalUploadedImagePath;
		
		if ($type == "jpg"  || $type == "jpeg"){
			$imageManipulator->jpegOutputQuality  = 100;
		}
		if ($customizationArray['rotation'] != 0){
			$imageManipulator->targetFile = $targetDir."/tempgenerateddesign".time().get_rand_id(4).".".$type;
			$imageManipulator->rotate($customizationArray['rotation'],0);
			$imageManipulator->sourceFile = $imageManipulator->targetFile;
		}

		$imageManipulator->targetFile = $targetDir."/finalgenerateddesign".time().get_rand_id(4).".".$type;

		$imageManipulator->resizeToWidth =  $width_in_final_design;
		$imageManipulator->resizeToHeight = $height_in_final_design;
		$imageManipulator->resize();
		
		// now paint the image
		if ($type == "jpg" || $type == "jpeg"){
			$scaledDesignImage = imagecreatefromjpeg($imageManipulator->targetFile);
		}else{
			$scaledDesignImage = imagecreatefrompng($imageManipulator->targetFile);
		}
		//echo $imageManipulator->targetFile ;

		
		// The correction logic below has been put in to avoid the approximation error that was happening because
		// the pixels cannot be in fractions. The scaled image generated above has a width which is less than what the actual
		// scaled width should be. The correction is being distributed on both sides of the image as of now.
		$xcorrection = $width_in_final_design - imagesx($scaledDesignImage);
		$ycorrection = $height_in_final_design - imagesy($scaledDesignImage);
		$width_in_final_design = imagesx($scaledDesignImage);
		$height_in_final_design = imagesy($scaledDesignImage);
		$xStart = $xStart + $xcorrection/2;
		$yStart = $yStart + $ycorrection/2;
		// added the latest components to master customization area
		$customizedImages['width_in_final']= $width_in_final_design;
		$customizedImages['height_in_final']= $height_in_final_design;
		$customizedImages['dxstart_image']= $xStart;
		$customizedImages['dystart_image']= $yStart;
		// hold up all the clean up images
		$customizedImages['finalgeneratedcleanUpimage'] =  $imageManipulator->targetFile; ### giving the wrong path //substr($imageManipulator->targetFile,4);

		imagecopyresampled($image,$scaledDesignImage,$xStart,$yStart,0,0,$width_in_final_design,$height_in_final_design,$width_in_final_design,$height_in_final_design);
	
	}
	// next is the text
	$customizedTextContent = $customizationArray['customizedtext'];
	$weblog->info("FONT TEXT >>>>>>>> ".$customizedTextContent['fonttext']);
	$weblog->info("FONT TYPE >>>>>>>> ".$customizedTextContent['fonttype']);

	if ($customizedTextContent['fonttext'] != ""){
		$customizedImages['dxstart_text']= $customizedTextContent['textX']*$xScalingFactor;
	    $customizedImages['dystart_text']= $customizedTextContent['textY']*$yScalingFactor;
		imageTextWrapped($image,$customizedTextContent['textX']*$xScalingFactor,$customizedTextContent['textY']*$yScalingFactor,$width_in_pixels,$customizedTextContent['fonttype'],$customizedTextContent['fontcolor'],$customizedTextContent['fonttext'],$customizedTextContent['fontsize']*$xScalingFactor,'c');
		imageTextWrapped($textImage,$customizedTextContent['textX']*$xScalingFactor,$customizedTextContent['textY']*$yScalingFactor,$width_in_pixels,$customizedTextContent['fonttype'],$customizedTextContent['fontcolor'],$customizedTextContent['fonttext'],$customizedTextContent['fontsize']*$xScalingFactor,'c');
	}
    $weblog->info("Creating the large image and thumbnail image");

	$finalCustomizedDesignLargeImage = $targetDir."/customized_design_images/L/finalcustomizeddesign".time().get_rand_id(4).".png";
	$finalCustomizedTextImage = $targetDir."/customized_design_images/L/finalcustomizedtext".time().get_rand_id(4).".png";
    $weblog->info("Large Image : $finalCustomizedDesignLargeImage");
	$weblog->info("Creating the large image of text with path > ".$finalCustomizedTextImage);

	if(!imagepng($image,$finalCustomizedDesignLargeImage))
	   $weblog->info("Unable to create image :$finalCustomizedDesignLargeImage");
    addToTempImagesList($finalCustomizedDesignLargeImage);
	if(!imagepng($textImage,$finalCustomizedTextImage))
	   $weblog->info("Unable to create image :$finalCustomizedTextImage");
    addToTempImagesList($finalCustomizedTextImage);
	
	$finalCustomizedDesignThumbnailImage = $targetDir."/customized_design_images/T/finalcustomizeddesign".time().get_rand_id(4).".png";
	$customizedImages['finalthumbnailcleanUpimage']=$finalCustomizedDesignThumbnailImage;
	$weblog->info("Thumbnail image :$finalCustomizedDesignThumbnailImage");
	// now resize the actual generated image which would be shown on the start shopping screen
	resizeImageToDimension($finalCustomizedDesignLargeImage,$finalCustomizedDesignThumbnailImage,100,100);
	// now return back the array containing the location of the images
	$customizedImages['large'] = $finalCustomizedDesignLargeImage;
	$customizedImages['thumbnail'] = $finalCustomizedDesignThumbnailImage;
	//added text image to master customization area
	$customizedImages['textimage'] = $finalCustomizedTextImage;
	//end
	$weblog->info("end calling generateFinalCustomizationAreaImageWithDesign function");
	return $customizedImages;
}



?>
