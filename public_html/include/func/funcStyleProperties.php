<?php
//this has multiple style related functions which returns results in json or based on function
include_once "$xcart_dir/modules/apiclient/SkuApiClient.php";
/**
 * This function takes article type id and return specific attributes for that article
 * @param int $articleId id of article type
 * @return json of specific attributes of the article type
 */
function getArticleAttributeTypeWithValues($articleId)
{
	$selectAttributeTypeSql = "SELECT id, 
								attribute_type AS name, 
								is_active 
								FROM mk_attribute_type 
								WHERE is_active = '1' 
								AND catalogue_classification_id = '$articleId'";
	
	$attributeTypeArray = func_query($selectAttributeTypeSql, TRUE);

	$firstTimeLoop = true;
	$attributeValues = array();
	foreach ($attributeTypeArray as $attributeType)
	{
		$articleAttributeValueSQL = "SELECT id, 
									attribute_value AS name, 
									is_active
									FROM mk_attribute_type_values 
									WHERE is_active = '1' 
									AND attribute_type_id = '".$attributeType['id']."'";
		$articleAttributeValues = func_query($articleAttributeValueSQL, TRUE);
		if(!empty($articleAttributeValues))
		{
			$atType['name'] = $attributeType['name'];
			$atType['is_active'] = $attributeType['is_active'];
			$atType['value'] = $articleAttributeValues;
			$attributeValues[] =  $atType;
		}
	}
	if(empty($attributeValues))
	{
		return false;
	}
	return json_encode($attributeValues);
}

/**
 * This function returns list of id and name of category(article type, master, sub cat.) in catalog
 * @param string $catalogType article type, master or sub category
 * @return json with id and name
 */
function getCatalogCategory($catalogType='ArticleType', $onlyApparelAndFootwear = false)
{
	$selectSQL = "SELECT id, 
				typename AS name 
				FROM mk_catalogue_classification 
				WHERE is_active = '1' AND ";
	switch ($catalogType)
	{
		case "MasterCategory":
			$selectSQL .= "parent1 = '-1' AND parent2 = '-1'";
			break;
		case "SubCategory":
			$selectSQL .= "parent1 <> '-1' AND parent2 = '-1'";
			break;
		case "ArticleType":
			$selectSQL .= "parent1 <> '-1' AND parent2 <> '-1'";
			if($onlyApparelAndFootwear){
				$selectSQL .= " AND parent2 in (9,10)";
			}
			break;
		default:
			return false;
	}
	$selectSQL .= " ORDER BY name ASC";
	
	$selectCatalog = func_query($selectSQL, TRUE);
	return json_encode($selectCatalog);
}

/**
 * this function returns different values for brand, gender, usage etc.
 * @param int $attributeTypeId id for global attribute type
 * @return json id,name list of corresponding global attribute values
 */
function getAttributeTypeValueArray($attributeTypeId)
{
	$sql = "SELECT id, 
			attribute_value AS name 
			FROM mk_attribute_type_values 
			WHERE attribute_type_id = '$attributeTypeId' 
			AND is_active = 1 
			ORDER BY attribute_value ASC";
	$result = func_query($sql, TRUE);
	return json_encode($result);
}

/**
 * this function returns specific attributes with attribute type name and value id
 * @param Array $styleId
 * @return json of specific attribute with name and id e.g. {'style_id': '123', 'spe_att': [{Composition':'123'}]}
 */
function getSpecificAttributes($styleId)
{
	if(!is_array($styleId) || empty($styleId))
	{
		return false;
	}
	
	$styleIdList = implode(",", $styleId);
	
	$sql = "SELECT
			msatav.product_style_id AS style_id, 
			msatav.article_type_attribute_value_id AS id, 
			mat.attribute_type AS name 
			FROM mk_style_article_type_attribute_values msatav 
			LEFT JOIN mk_attribute_type_values matv ON matv.id = msatav.article_type_attribute_value_id 
			LEFT JOIN mk_attribute_type mat ON mat.id = matv.attribute_type_id 
			WHERE msatav.product_style_id IN ($styleIdList) AND mat.is_active = '1' AND matv.is_active = '1'";
	$result = func_query($sql, FALSE);
	$styleSpecificAttribute = array();
	foreach($result as $specificAttribute)
	{
		$styleSpecificAttribute[$specificAttribute['style_id']][$specificAttribute['name']] = $specificAttribute['id'];
	}
	$finres = array();
	//this is done to get if for some specific attribute there is no values assigned
	foreach($styleId as $sId)
	{
		$spAttName = getSpecificAttributeName($sId);
		$temp = array();
		foreach($spAttName as $sName)
		{
			$spId = $styleSpecificAttribute[$sId][$sName];
			$temp[$sName] =  empty($spId)?0:$spId;
		}
		$finres[] = $temp;
	}
	return $finres;
}

/**
 * given a style id returns an array of name of active and non-empty specific attribute corresponding to the article type of that style
 */
function getSpecificAttributeName($styleId)
{
	$selectArticleIdSql = "SELECT global_attr_article_type 
							FROM mk_style_properties 
							WHERE style_id = '$styleId'";
	$articleId = func_query_first_cell($selectArticleIdSql, FALSE);
	$sql = "SELECT id, attribute_type AS name 
			FROM mk_attribute_type 
			WHERE catalogue_classification_id = '$articleId' AND is_active = '1'";
	$result = func_query($sql, TRUE);
	$nonEmptyAttributeType = array();
	foreach ($result as $attrType)
	{
		$attrId = $attrType['id'];
		$countAttrTypeValue = func_query_first_cell("select count(*) from mk_attribute_type_values where is_active = 1 and attribute_type_id = $attrId");
		if($countAttrTypeValue > 0)
		{
			$nonEmptyAttributeType[] = $attrType['name'];
		}
	}
	
	return $nonEmptyAttributeType;
}

/**
 * This function gets all the details corresponding to an array of styles
 * @param array $styleId
 */
function getStyleCompleteDetails($styleId)
{
	if(!is_array($styleId) || empty($styleId))
	{
		return false;
	}
	
	$styleIdList = implode(",", $styleId);
	$sql = "SELECT
			msp.style_id AS style_id,
			mps.styletype AS style_status,
			msp.article_number AS vendor_article_no,
			msp.variant_name AS vendor_article_name,
			msp.global_attr_article_type as article_type_id,
			mps.product_type as product_type,
			msp.global_attr_brand AS brand,
			msp.global_attr_age_group AS age_group,
			msp.global_attr_gender AS gender,
			msp.global_attr_base_colour AS base_colour,
			msp.global_attr_colour1 AS colour1,
			msp.global_attr_colour2 AS colour2,
			msp.global_attr_fashion_type AS fashion_type,
			msp.global_attr_season AS season,
			msp.global_attr_year AS 'year',
			msp.global_attr_usage AS 'usage',
			msp.product_display_name AS product_display_name,
			msp.product_tags AS tags, 
			mps.price AS price, 
			msp.weight AS weight, 
			msp.comments AS comments 
			FROM mk_product_style mps
			LEFT JOIN mk_style_properties msp ON msp.style_id = mps.id
			WHERE msp.style_id IN ($styleIdList)";
	$result = func_query($sql, FALSE);
	return $result;
}

/**
 * function to get style classification data given an array of style ids
 */
function getStyleClassificationBrand($styleId)
{
	if(!is_array($styleId) || empty($styleId))
	{
		return false;
	}

	$styleClassificationBrand = array();
	foreach ($styleId as $style)
	{
		$sql = "SELECT applied_filters AS classification_brand FROM mk_applied_filters WHERE filter_group_id = '2' AND generic_id = '$style' ORDER BY id DESC LIMIT 1";
		$value = func_query_first($sql, TRUE);
		if(empty($value))
		{
			$styleClassificationBrand[] = array('classification_brand'=>'Not Set');
		}
		else
		{
			$styleClassificationBrand[] = $value;
		}
	}
	return $styleClassificationBrand;
}

/**
 * this function gets you all the content fields
 * @param array $styleId
 */
function getStyleContent($styleId)
{
	if(!is_array($styleId) || empty($styleId))
	{
		return false;
	}

	$styleIdList = implode(",", $styleId);
	$sql = "SELECT
			msp.style_id AS style_id,
			IFNULL(msp.style_note, ' ') AS style_note,
			IFNULL(msp.materials_care_desc, ' ') AS materials_care_desc,
			IFNULL(msp.size_fit_desc, ' ') AS size_fit_desc,
			IFNULL(mps.description, ' ') AS description,
			mps.styletype AS status 
			FROM mk_product_style mps
			LEFT JOIN mk_style_properties msp ON msp.style_id = mps.id
			WHERE msp.style_id IN ($styleIdList)";
	$result = func_query($sql, FALSE);
	return $result;
}

/**
 * get all style status states from db
 */
function getStyleStatus($codeNameAsKeyValuePair = FALSE)
{
	$sql = "SELECT style_status_code_to AS id, style_status_name_to AS name FROM style_status_transition_rules GROUP BY style_status_code_to ORDER BY style_status_name_to";
	$result = func_query($sql, FALSE);
	if($codeNameAsKeyValuePair)
	{
		$statusArray = array();
		foreach($result as $status)
		{
			$statusArray[$status['id']] = $status['name'];
		}
		return $statusArray;
	}
	return json_encode($result);
}

/**
 * get all possible style status transition
 * @return json:
 * 				key-> from code,
 * 				to_code -> array of possible status codes,
 * 				to_name -> comma separated status name 
 */
function getPossibleStatusTransition()
{
	$sql = "SELECT style_status_code_from AS from_code,  group_concat(style_status_code_to) AS to_code,  group_concat(style_status_name_to) AS to_name from style_status_transition_rules group by style_status_code_from";
	$result = func_query($sql, TRUE);
	$statusTransition = array();
	foreach ($result as $status)
	{
		$statusTransition[$status['from_code']]['to_code'] = explode(",", $status['to_code']);
		$statusTransition[$status['from_code']]['to_name'] = $status['to_name'];
	}
	return json_encode($statusTransition);
}

/**
 * given a status code get possible status codes and code name inclusing itself
 * code as key and name as value
 */
function getPossibleStatusFromCode($fromStatusCode)
{
	$statusQuery = " SELECT style_status_code_from AS from_code,
					 style_status_name_from AS from_name,
					 style_status_code_to AS to_code,
					 style_status_name_to AS to_name
					 FROM style_status_transition_rules
					 WHERE style_status_code_from = '$fromStatusCode'";
	
	$statusQueryResult = func_query($statusQuery, TRUE);
	$possibleStatus= array();
	foreach($statusQueryResult as $status)
	{
		$possibleStatus[$status['from_code']] = $status['from_name'];
		$possibleStatus[$status['to_code']] = $status['to_name'];
	}
	return $possibleStatus;
}

/**
 * funciton to get list of active product type
 */
function getProductTypes()
{
	$sql = "SELECT id, name FROM mk_product_type WHERE type = 'P' ORDER BY name";
	$result = func_query($sql, TRUE);
	return json_encode($result);
}

/**
 * function to get all the style propoerties given a style for grid like display
 * value 0 or empty as not set
 * @param int $styleId 
 */
function getDisplayStyleAllData($styleId)
{
	//get all attributes of style
	$styleCompleteData = getStyleCompleteDetails(array($styleId)); //complete data
	$styleSpecificDetails = getSpecificAttributes(array($styleId));//specific attribute
	$styleSpecificNameValue = array();
	$styleAll = array();
	if(!empty($styleSpecificDetails[0]))
	{
		foreach($styleSpecificDetails[0] as $type=>$id)
		{
			if($id == 0)
			{
				$styleSpecificNameValue[0][$type] = 'Not Set';
			}
			else
			{
				$valueName = func_query_first_cell("SELECT attribute_value FROM mk_attribute_type_values WHERE id = '$id'", TRUE);
				$styleSpecificNameValue[0][$type] = $valueName;
			}
		}
		foreach($styleCompleteData as $key=>$style)
		{
			$styleAll[$key] = array_merge($style, $styleSpecificNameValue[$key]);
		}
	}
	else
	{
		$styleAll = $styleCompleteData;
	}
	
	$styleAllProp = array();
	$i = 0;
	$propertyName = array('style_id' => 'Style Id', 'style_status' => 'Status', 'vendor_article_no' => 'Vendor Article No',
						  'vendor_article_name' => 'Vendor Article Name', 'article_type_id' => 'Article Type', 'brand' => 'Brand',
						  'age_group' => 'Age Group', 'gender' => 'Gender', 'base_colour' => 'Base Colour', 'colour1' => 'Colour 1',
						  'colour2' => 'Colour 2', 'fashion_type' => 'Fashion Type', 'season' => 'Season', 'year' => 'Year',
						  'usage' => 'Usage', 'product_display_name'  => 'Product Display Name', 'tags' => 'Tags', 'price' => 'Price (Rs.)',
						  'weight' => 'Weight (Gms)', 'comments'=>'Comments', 'product_type' => 'Product Type');
	foreach($styleAll[0] as $name=>$value)
	{
		if($name == 'style_status')
		{
			$value = func_query_first_cell("SELECT style_status_name_to FROM style_status_transition_rules WHERE style_status_code_to = '$value'", TRUE);
		}
		elseif($name == 'article_type_id')
		{
			$value = func_query_first_cell("SELECT typename FROM mk_catalogue_classification WHERE id = '$value'", TRUE);
		}
		$styleAllProp[$i]['name'] = isset($propertyName[$name])?$propertyName[$name]:$name;
		$styleAllProp[$i]['value'] = valueNotSet($value)?'Not Set':$value;
		$i += 1;
	}
	//get list of size options and sku code
	$sqlQuery = "SELECT option_id, sku_id FROM mk_styles_options_skus_mapping WHERE style_id = '$styleId'";
	$sqlResult = func_query($sqlQuery, FALSE);
	$styleAllProp[$i]['name'] = "Size-SKU Mapping";
	if(!empty($sqlResult))
	{
		$optionSkuMap = array();
		foreach($sqlResult as $result)
		{
			$optionSkuMap[$result['sku_id']] = $result['option_id'];
		}
		$skuDetails = SkuApiClient::getSkuDetails(array_keys($optionSkuMap));
		if(empty($skuDetails))
		{
			$styleAllProp[$i]['value'] = 'No size-sku mapping found';
		}
		else
		{
			$optionIdList = implode(",", $optionSkuMap);
			$sqlQuery = "SELECT id,value FROM mk_product_options WHERE id IN ($optionIdList)";
			$optionValueList = func_query($sqlQuery, FALSE);
			$optionValueId = array();
			foreach($optionValueList as $opVal)
			{
				$optionValueId[$opVal['id']] = $opVal['value'];
			}
			$optionSkuMapping = '';
			foreach($skuDetails as $sku)
			{
				$skuCode = $sku['code']."(".$sku['size'].")";
				$optionId = $optionSkuMap[$sku['id']];
				$optionValue = $optionValueId[$optionId];
				$optionSkuMapping .= "$optionValue:$skuCode, ";
			}
			$optionSkuMapping = substr($optionSkuMapping, 0, -2);
			$styleAllProp[$i]['value'] = $optionSkuMapping;
		}
		
	}
	else
	{
		$styleAllProp[$i]['value'] = 'No size-sku mapping found';
	}
	
	return $styleAllProp;
}

/**
 * function to check if value is 0 or null
 */
function valueNotSet($value)
{
	
	if(!isset($value))
	{
		return true;
	}
	
	if($value == '0')
	{
		return true;
	}
	
	if($value == '')
	{
		return true;
	}
	
	return false;
}

/**
 * This function takes article type id and return specific attributes for that article
 * returns all active or disabled for modification
 * @param int $articleId id of article type
 * @return json of specific attributes of the article type
 */
function getArticleAttributeTypeWithValuesForModify($articleId)
{
	$selectAttributeTypeSql = "SELECT id,
	attribute_type AS name,
	is_active,
	is_filter
	FROM mk_attribute_type
	WHERE catalogue_classification_id = '$articleId'";

	$attributeTypeArray = func_query($selectAttributeTypeSql, TRUE);

	$firstTimeLoop = true;
	$attributeValues = array();
	foreach ($attributeTypeArray as $attributeType)
	{
		$articleAttributeValueSQL = "SELECT id,
		attribute_value AS name,
		IF(is_active='1', TRUE, FALSE) AS 'is_active'
		FROM mk_attribute_type_values
		WHERE attribute_type_id = '".$attributeType['id']."'";
		$atType['id'] = $attributeType['id'];
		$atType['name'] = $attributeType['name'];
		$atType['is_active'] = $attributeType['is_active'];
		$atType['is_filter'] = $attributeType['is_filter'];
		$atType['value'] = func_query($articleAttributeValueSQL, TRUE);
		$attributeValues[] =  $atType;
	}
	if(empty($attributeValues))
	{
		return false;
	}
	return $attributeValues;
}

/**
 * function to get all classification brands
 * @return json encoded filter_name as id and name both
 */
function getClassificationBrand()
{
	$classificationBrand = func_query("SELECT filter_name AS id, filter_name AS name FROM mk_filters WHERE filter_group_id='2' AND is_active = '1' ORDER BY filter_name ASC", TRUE);
	return json_encode($classificationBrand);
}

/**
 * given an array of style properties
 * returns image types which are uploaded/update from this array
 */
function getUploadedImageType($styleProperties)
{
	$uploadedImages = '';
	$imageTypeArray = array('default_image' => 'Default Image', 'search_image' => 'Search Image', 'search_zoom_image' => 'Search Zoom Image', 'front_image' => 'Front Image',
			'back_image' => 'Back Image', 'right_image' => 'Right Image', 'left_image' => 'Left Image', 'top_image' => 'Top Image', 'bottom_image' => 'Bottom Image');
	foreach($styleProperties as $styleKey=>$styleValue)
	{
		if(array_key_exists($styleKey, $imageTypeArray))
		{
			if($styleValue != '')
			{
				$uploadedImages .= $imageTypeArray[$styleKey].", ";
			}
		}
	}
	$uploadedImages = substr($uploadedImages, 0, -2);
	return $uploadedImages;
}
?>