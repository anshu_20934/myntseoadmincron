<?php
/* Function for SEO  */
if (!defined('XCART_START'))
{
    @header ("Location: ../");
    die ("Access denied");
}

/**
[related to utm cookie]
*/
define("UTM_COOKIE_NAME", 'utm_track_1');
define("SECS_IN_30DAYS", 30*24*60*60);

require_once "func.mkcore.php";


################## function to get the page title ####################
$tagname ='';
function getTitleTag($pagename)
{
	 global $sql_tbl,$tagname;
	 $isRewrite  = true;
	 $queryString = $_SERVER['QUERY_STRING'];
	 
	 // check whether url rewriting is done or not 
	 if(stristr($queryString,'qstring'))
	 {
		$typeArray =  explode("=",$queryString);
		$typeArray =  explode("/",$typeArray[1]);
		$tagname = $typeArray[1];
		
	 }
	 elseif(!empty($queryString))
	 {
	 	$typeArray = explode("&",$queryString);
	 	$isRewrite = false;
	 }
		
 	$title = getDefaultMetaValues('title');

	return $title;

}

################## function to get the meta description 

function getMetaDescription($pagename)
{
	 global $sql_tbl,$tagname;
	 $queryString = $_SERVER['QUERY_STRING'];
	 $isRewrite = true;
	 // check whether url rewriting is done or not 
	 if(stristr($queryString,'qstring'))
	 {
		$typeArray =  explode("=",$queryString);
		$typeArray =  explode("/",$typeArray[1]);
		
	 }
	 elseif(!empty($queryString))
	 {
	 	$typeArray = explode("&",$queryString);
	 	$isRewrite = false;
	 }
	 
	 $sql = "SELECT description,tagtype,query_identifier FROM $sql_tbl[seo] WHERE pageurl = '".$pagename."'";  
	 $rsQuery = db_query($sql, true);
	 if(db_num_rows($rsQuery) > 0)
	 { 
		 $row = db_fetch_array($rsQuery);
		 if(stristr($row['description'], '#'))
		 {
		 	if($isRewrite)
		 	  $id =  $typeArray[$row['query_identifier']];
		 	else 
		 	  $id=getQueryStringValue($typeArray,$row['query_identifier']);
		 	
		 	$metadescription = $row['description'];
		 	while(strpos($metadescription,"#") !== false)
		  	{	
		 		 $string= get_string_between($metadescription,"#","#");
		 		 $qualifier = getQualifier(".",$string);
		 		 $metadescription = replaceTitle($qualifier,$id,$metadescription);
				 if(strpos($metadescription,"#product.desc#") !== false)
				 {
					$desc  = getProductDescription($id);
					$metadescription = str_ireplace("#product.desc#", $desc,$metadescription);
				 }
		 	 		  
		  	}
		 	
		 	 $description = $metadescription;
		 	 
		 	
		 	 
		 }
		 else 
		 {
		 	$description = $row['description'];
		 }
	 }else{
	 	$description = getDefaultMetaValues('description');
	 }
	 return $description;  
	 
	
}

################## function to get the meta keywords 

function getMetaKeywords($pagename)
{
	 global $sql_tbl,$tagname;
	 $queryString = $_SERVER['QUERY_STRING'];
	 $isRewrite = true; 
	 // check whether url rewriting is done or not 
	 if(stristr($queryString,'qstring'))
	 {
		$typeArray =  explode("=",$queryString);
		$typeArray =  explode("/",$typeArray[1]);
		
	 }
	 elseif(!empty($queryString))
	 {
	 	$typeArray = explode("&",$queryString);
	 	$isRewrite = false;
	 }
	 
	  $sql = "SELECT keywords,qualifier,tagtype,query_identifier,qualifier_pos FROM $sql_tbl[seo] WHERE pageurl = '".$pagename."'";  
	 $rsQuery = db_query($sql, true);
	 if(db_num_rows($rsQuery) > 0)
	 { 
		 $row = db_fetch_array($rsQuery);
		 if(stristr($row['keywords'], '#') )
		 {
		 	 
		 	if($isRewrite)
		 	  $id =  $typeArray[$row['query_identifier']];
		 	else 
		 	  $id=getQueryStringValue($typeArray,$row['query_identifier']);
		 	  
		 	 $metaKeywordsStr = $row['keywords'];
		 	 
		 	 $keywords = $metaKeywordsStr ;
		 			 	 
		 	 while(strpos($keywords,"#") !== false )
		  	 {
		  	 	 $string= get_string_between($keywords,"#","#");
				 if(stristr($string,"keywords"))
				 {
				 	
				 	$qualifier = getQualifier(".",$string);
				 	$metaKeys = replaceKeywords($qualifier,$id);
				 	$keywords = str_ireplace("#$string#","||keywords||",$keywords);
				 }
				 else 
				 {
				 	$qualifier = getQualifier(".",$string);
			 		$keywords = replaceTitle($qualifier,$id,$keywords);
				 }
						
		  	 }
		  $metaKeywordsStr = $keywords;
		  	if(!empty($metaKeys))
		  	{
		  	   $metakeywords = 
		 	   generateKeywordString($metaKeywordsStr,$metaKeys);
		 	   $keywords  = $metakeywords;
		  	}
		  	else 
		  	{
		 		$keywords = $metaKeywordsStr;
		  	}
		 				  	 
		  	 
		 	 		 	 
		 }
		 else 
		 {
		 	 
		 	    $keywords = $row['keywords'];	
		 }
	 }
	 else 
	 {
	 	$keywords = getDefaultMetaValues('keywords');
	 }
	 return $keywords;
	
}

function getQueryStringValue($typeArray,$type)
{
	$qryResult = '';
	foreach($typeArray as $key=>$value)
	{
		 if(stristr($value,$type))
		 {
		     $result = explode("=",$value) ;
		     $qryResult =  $result[1];
		 }
	}
	return $qryResult;
	 
	
}
function generateKeywordString($keyStr,$keywords)
{
	$result = '';
	
	
	
	foreach($keywords as $key=>$value)
	{
		 $result .= str_ireplace("||keywords||",$value[0],$keyStr).",";	      
		 
	}
	$result = trim($result,",");
	
	return $result;
	
	
}



function getFileName($fileName)
{
	 $pageUrl = basename($_SERVER['SCRIPT_NAME']);
	 return $pageUrl;
	
}

function getTitle($id,$type)
{
	  global $sql_tbl,$tagname;
	  $products = $sql_tbl['products'];
	  $products_type = $sql_tbl['mk_product_type'];
	  $categories = $sql_tbl['categories'];
	  $tags = $sql_tbl['mk_tags'];
	  $product_style = $sql_tbl['mk_product_style'];
	  
	  
	  
	  if($type == 'ptype' )
	  {
	  	if(strlen($id) < 5)
	  	{
		  	$query = "SELECT  name FROM  $products_type WHERE  id = '".$id."' ";

		 	$result  =db_query($query, true);
	  	}
	  	else
	  	{
	  		 $query = "SELECT  pt.name FROM  $products_type as pt INNER JOIN  $products as p
	  		ON pt.id = p.product_type_id WHERE  p.productid = '".$id."' ";
	  		
		 	$result  =db_query($query, true);
	  	}
	 	$row = db_fetch_array($result);
		if(empty($row['name'])) return $id;
	 	return $row['name'];
	  }
	  elseif($type == 'product')
	  {
	  	$query = "SELECT  product FROM  $products  WHERE  productid = '".$id."'  ";
	 	$result  =db_query($query, true);
	 	$row = db_fetch_array($result);
		if(empty($row['product'])) return $id;
	 	return $row['product'];
	  	
	  }
	  else if($type == 'category')
	  {
	  	$query = "SELECT  category FROM  $categories WHERE  categoryid = '".$id."' ";
	 	$result  =db_query($query, true);
	 	$row = db_fetch_array($result);
		//If category id does not fetch any result ==> the id itself is the name of the category
		if(empty($row['category'])) return $id;
	 	return $row['category'];
	  }
	  else if($type == 'pstyle')
	  {
	  	if(strlen($id) < 5)
	  	{
		  	$query = "SELECT  name FROM  $product_style WHERE  id = '".$id."' ";
		 	$result  =db_query($query, true);
	  	}
	  	else 
	  	{
	  		$query = "SELECT  ps.name FROM  $products_style as ps INNER JOIN  $products as p
	  		ON pt.id = p.product_style_id WHERE  p.productid = '".$id."' ";
		 	$result  =db_query($query, true);
	  	}
	 	$row = db_fetch_array($result);
		if(empty($row['name'])) return $id;
	 	return $row['name']; 
	  }
	  else if($type == 'tag')
	  {
	  	$query = "SELECT  tagname FROM  $tags WHERE  tagid = '".$id."' ";
	 	$result  =db_query($query, true);
	 	$row = db_fetch_array($result);
		if(empty($row['tagname'])) return $id;
	 	return $row['tagname'];
	  	
	  }
	  else if($type == 'shop')
	  {
	  	return $id;	  	
	  }
	  else if($type == 'designer')
	  {
	  	$query = "SELECT  firstname FROM  $sql_tbl[mk_designer] as d INNER JOIN $sql_tbl[customers] as c
	  	ON c.login = d.customerid WHERE  d.id = '".$id."' ";
	 	$result  =db_query($query, true);
	 	$row = db_fetch_array($result);
	 	if(empty($row['firstname'])) return $id;
		return $row['firstname'];
	  	
	  }
	  
	
}

function getProductDescription($productid)
{
	$sql ="select fulldescr from xcart_products where productid='".$productid."'";
	$result  =db_query($sql, true);
	$row = db_fetch_array($result);
	return $row['fulldescr'];
	 	
}

function getDefaultMetaValues($fieldname)
{     
	global $sql_tbl;
	$page = 'default.php';
	$sql = "SELECT ". $fieldname."  FROM $sql_tbl[seo] WHERE pageurl = '".$page."'";  
	$rsQuery = db_query($sql, true);
	$row = db_fetch_array($rsQuery);
	$defaultValue = $row[$fieldname];
	return  $defaultValue; 
}
function checkSpecialChar($fieldName,$splChar)
{
	 $flag = false;
	 if(stristr($fieldName, $splChar))
	 {
	 	 $flag = true;
	 }
	 return $flag;
	
}



function get_string_between($string, $start, $end){
    $string = " ".$string;
     $ini = strpos($string,$start);
     if ($ini == 0) return "";
     $ini += strlen($start);    
     $len = strpos($string,$end,$ini) - $ini;
     return substr($string,$ini,$len);
}

function getQualifier($explodeOn,$string)
{
	$array = explode($explodeOn,$string);
	return $array[0];
}

function replaceTitle($type,$id,$str)
{
	global $tagname;
	switch($type)
		 	 {
		 	 	case 'product' :
		 	 		    $title = getTitle($id,$type);
		 	 		    $title = str_ireplace("#product.name#", $title,$str);
						$title = parse($id,$type,$title);
						break;
		 	 	case 'ptype' :
		 	 		     $title = getTitle($id,$type);
		 	 		     $title = str_ireplace("#ptype.name#", $title,$str);
		 	 		     $title = parse($id,$type,$title);
		 	 		     break;	    
		 	 	case 'category' :
		 	 		     $title = getTitle($id,$type);
		 	 			 $title = str_ireplace("#category.name#", $title, $str);
		 	    	    break;
		 	    case 'pstyle' :
		 	 		    $title = getTitle($id,$type);
		 	 		    $title = str_ireplace("#pstyle.name#", $title,$str);
		 	    	    break;	
		 	     case 'shop' :
		 	     		$title = getTitle($id,$type);
		 	 		    $title = str_ireplace("#shop.name#", $title,$str);
		 	    	    break;		    
		 	    	        	
		 	 	case 'tag' :
		 	 		   $title = $tagname;
		 	 		   $title = str_ireplace("#tag.name#",$title,$str);
		 	 		    break;
		 	    case 'designer' :
		 	    	    $title = getTitle($id,$type);
		 	 		    $title = str_ireplace("#designer.name#", $title,$str);
		 	    	    break;		    
		 	    	   
		 	 		  
		 	 }
		 	 return $title;
	
}


function replaceKeywords($type,$id)
{
	 global $sql_tbl;
	 switch($type)
	 {
		 	 	case 'product' :
		 	 		    $keywords = get_tags_for_product($id);
		 	 		    break;
		 	 	case 'ptype' :
		 	 		    $keywords = func_get_product_tags($id);
		 	 		    break;	    
		 	 	case 'category' :
		 	 		    $keywords = func_get_category_tags($id);
		 	 			break;	
		 	    	    
		 	  /* case 'pstyle' :
		 	 		    $metakeywords = getTitle($id,$row['tagtype']);
		 	 			$keywords = $metakeywords;
		 	 			$keywords = str_ireplace("#title#", $keywords, $row['keywords']);
		 	 			$keywords = str_ireplace("#keywords#", $keywords, $keywords);
		 	    	    break;*/
		 	    case 'tag' :
		 	    	    $title = $tagname;
		 	 		    $title = str_ireplace("#tag.name#",$title,$str);
		 	 		    break;
		 	 		 	
		 	    	 	  
		 	 }
		 return $keywords;	 
	
}
function get_tags_for_product($pid)
{
	  global $sql_tbl;
	  $tag_tbl = $sql_tbl['mk_tags'];
	  $product_tag_tbl =$sql_tbl['mk_product_tag_map'];
	  $producttags = "SELECT  t.tagname,t.tagid FROM  $tag_tbl as t INNER JOIN $product_tag_tbl as pt
	  ON t.tagid = pt.tagid WHERE  pt.productid = '".$pid."' ";
	  $result  =db_query($producttags);
	  $products=array();
		
	  if ($result)
	  {
		 $i=0;
		 while ($row=db_fetch_row($result))
		 {
			$products[$i] = $row;
			$i++;
		 }
	  }
		
	 return $products;

}

function parse($id,$type,$str)
{
    global $sql_tbl; 
	switch($type)
	{
		case 'product' :
		     $rsProductInfo = db_query("SELECT p.product,pt.name as pname,ps.name as sname,c.firstname FROM $sql_tbl[products] as p
		     INNER JOIN $sql_tbl[mk_product_type] as pt ON pt.id = p.product_type_id 
		     INNER JOIN $sql_tbl[mk_product_style] as ps ON ps.id = p.product_style_id 
		     INNER JOIN $sql_tbl[mk_designer] as d ON d.id = p.designer INNER JOIN $sql_tbl[customers] as c 
		     ON c.login = d.customerid WHERE p.productid = '".$id."' ");
		     $row = db_fetch_array($rsProductInfo);
	 		 $result =  str_ireplace("#ptype.name#", $row[pname],$str);
	 		 $result =  str_ireplace("#stype.name#", $row[sname],$result);
	 		 $result =  str_ireplace("#designer.name#", $row[product],$result);
	
		 	 break;
		case "ptype": 
			 $rsProductInfo = db_query("SELECT ps.name FROM $sql_tbl[mk_product_style] as ps WHERE 
			 ps.product_type='".$id."' ");
			 while($row = db_fetch_array($rsProductInfo))
			      $qryStr =$row[name]. ",";
			 $qryStr = trim($qryStr,",");     
			 $result =  str_ireplace("#pstyle.name#", $qryStr ,$str);
			 break;
			 
	}
	return $result;
		
}
#fetch Extera contents like rssfeedds m external contents,image source .....
function getExtraContents($fileName){
	global $sql_tbl ;
	$pagename  = getFileName($fileName);
	$sql = "SELECT external_content,comments,rssfeeds,h1tag,h2tag,relatedkeywords,imagesource FROM $sql_tbl[seo] 
	       WHERE  pageurl = '".$pagename."' ";
	$rsQuery = db_query($sql, true);
	$rows = db_fetch_array($rsQuery);
	return $rows;
}

/**
 set affiliate active for later transactions
*/
function setActive($utm_source) {
    global $smarty;

    if (isAffiliate($utm_source)) {
        $trackingCookie = array();
        $trackingCookie[$utm_source]['trackstatus'] = 1;
        $smarty->assign('trackingCookie',$trackingCookie);
        return true;
    }
    return false;
}

/* set/reset the utm cookie
 * @param:(string)$utm_source
 * NOTE : post GTM migration, no validation would be checked for,
 * essentially meaning that all the other code in this context
 * is dead from now on and should be cleaned-up once the whole
 * control-flow is delpoyed and verified on production.
 */
function setUTM($utm_source, $utm_medium='') {
    $utm_1['utm_source']    = $utm_source;
    $utm_1['utm_medium']    = $utm_medium;
    $utm_1['trackstart']    = time();
    $utm_1['trackend']      = time() + SECS_IN_30DAYS;
    //the following call implicitly includes replacing the last affiliate or renewing expiry for the same, if applies
    setSerializedCookie(UTM_COOKIE_NAME, $utm_1, $utm_1['trackend'], "/", $GLOBALS['cookiedomain']);
}


/* set/reset non-affiliate in the utm cookie
 * @param:(string)$utm_source
 */
function setUTMforNaf($utm_source, $utm_medium) {

    if (!isAffiliate($utm_source)) {
        $utm_1 = array();

        //set data
        $utm_1['utm_type']      = 'naf';
        $utm_1['utm_source']    = $utm_source;
        $utm_1['utm_medium']    = $utm_medium;
        $utm_1['trackstart']    = time();
        $utm_1['trackend']      = time() + SECS_IN_30DAYS;

        //the following call implicitly includes replacing the last non-affiliate or renewing expiry for the same, if applies
        setSerializedCookie(UTM_COOKIE_NAME, $utm_1, $utm_1['trackend'], "/", $GLOBALS['cookiedomain']);
        return $utm_1;
    }
    return null;
}



/* set/reset affiliate in the utm cookie
 * @param:(string)$utm_source
 */
function setUTMforAf($utm_source, $utm_medium='') {

    if (isAffiliate($utm_source)) {
        $utm_1 = array();

        //set data
        $utm_1['utm_type']      = 'af';
        $utm_1['utm_source']    = $utm_source;
        $utm_1['utm_medium']    = $utm_medium;
        $utm_1['trackstart']    = time();
        $utm_1['trackend']      = time() + getAffiliateTrackspan($utm_source);

        //the following call implicitly includes replacing the last affiliate or renewing expiry for the same, if applies
        setSerializedCookie(UTM_COOKIE_NAME, $utm_1, $utm_1['trackend'], "/", $GLOBALS['cookiedomain']);
        return $utm_1;
    }
    return null;
}

function isAffiliate($utm_source) {
    if (empty($utm_source)) return false;
    $query = "select tagname from mk_ad_affiliates where tagname='".$utm_source."' and status='A'";

    if (false === ($result = func_query_first_cell($query)))
        return false;
    else
        return ($result == $utm_source);
}

function getAffiliateTrackspan($utm_source) {
    $query = "select trackspan from mk_ad_affiliates where tagname='".$utm_source."' and status='A'";

    return ($result = func_query_first_cell($query)) ? $result : 0;
}

/**
return the utm_source set in utm
*/
function getUTMSource() {
    $utm = getUnserializedCookieContent(UTM_COOKIE_NAME);
    if (is_array($utm) && !empty($utm)) {
        return $utm['utm_source'];
    }
    return null;
}

/**
return the utm_medium set in utm
*/
function getUTMMedium() {
    $utm = getUnserializedCookieContent(UTM_COOKIE_NAME);
    if (is_array($utm) && !empty($utm)) {
        return $utm['utm_medium'];
    }
    return null;
}



/**
return the utm type set in utm
*/
function getUTMType() {
    $utm = getUnserializedCookieContent(UTM_COOKIE_NAME);
    if (is_array($utm)) {
        return $utm['utm_type'];
    }
    return null;
}

/**
check utm for expiry or existence
*/
function isUTMValid() {
    $utm = getUnserializedCookieContent(UTM_COOKIE_NAME);
    //Raghu : For New Homepage (Node js stack
    //PHP gives a 10 digit timestamp whereas Javascript works on 13 digit timestamps.
    //If the utm cookie was set by Node then the timestamps would be 13 digits which gets stripped to 10 digits.
    $utmTrackStartTime = strval($utm['trackstart']);
    $utmTrackEndTime = strval($utm['trackend']);

    $utmTrackStartTime = intval(substr($utmTrackStartTime, 0, 10));
    $utmTrackStartEnd = intval(substr($utmTrackEndTime,0 , 10));

    if (is_array($utm)) {
        return (($utmTrackStartTime < time()) && (time() < $utmTrackEndTime));
    } else {
        return false;
    }
}

/**
return the utm_campaign set in utm
*/
function getUTMCampaign() {
    $utm = getUnserializedCookieContent(UTM_COOKIE_NAME);
    if (is_array($utm) && !empty($utm)) {
        return $utm['utm_campaign'];
    }
    return null;
}

/**
return the campaign_id set in utm
*/
function getUTMCampaignId() {
    $utm = getUnserializedCookieContent(UTM_COOKIE_NAME);
    if (is_array($utm) && !empty($utm)) {
        return $utm['campaign_id'];
    }
    return null;
}

/**
return the octane_email set in utm
*/
function getUTMOctaneEmail() {
    $utm = getUnserializedCookieContent(UTM_COOKIE_NAME);
    if (is_array($utm) && !empty($utm)) {
        return $utm['octane_email'];
    }
    return null;
}

/**
return the affiliate set in utm_track_30
NOTE: no other affiliates possible except the
      ones checked for here
*/
function getAffiliateUTM30($utm_30_data) {
    if (is_array($utm_30_data)) {
        if (isset($utm_30_data['aff'])) {
            return 'aff';
        }
        if (isset($utm_30_data['aff-dgm'])) {
            return 'aff-dgm';
        }
        if (isset($utm_30_data['aff-ibibo'])) {
            return 'aff-ibibo';
        }
        return null;
    }
}


/**
This module checks for any utm_track_30s on the client side; if found, transforms
them to the new utm_track_1 format
TODO: to be phased out in later changes
*/
function transformUTM30to1() {

    $utm_30 = getUnserializedCookieContent("utm_track_30");

    if ($utm_30 === false) {
        //there's nothing here
        return;
    } else {
        $new_utm = array();

        $new_utm['utm_type']    = 'af';
        $new_utm['utm_source']  = getAffiliateUTM30($utm_30);
        $new_utm['utm_medium']  = $new_utm['utm_source'];
        $new_utm['trackstart']  = $utm_30['trackstart'];
        $new_utm['trackend']    = $utm_30['trackend'];

        setSerializedCookie(UTM_COOKIE_NAME, $new_utm, $new_utm['trackend'], "/", $GLOBALS['cookiedomain']);

        //delete the utm_track_30
        deleteMyntraCookie("utm_track_30");
    }
}

?>
