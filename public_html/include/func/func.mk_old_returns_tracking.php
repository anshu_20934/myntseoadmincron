<?php

include_once("func.order.php");
include_once("func.mkcore.php");
include_once("func.mail.php");
include_once("func.mk_orderbook.php");
include_once("$xcart_dir/modules/apiclient/WarehouseApiClient.php");
/**
*	get the details of an order to-be/has-been returned
* 	@param $order_id
* 	@return a dictionary of details as outlines below:
*		Key		=>	Value
*
*		orderid		=>	...
* 		returntype	=>	... [Optional]
* 		custname	=>	...
* 		pickupaddress	=>	...
* 		currstate	=>	... [optional]
*		shippingtype	=>	... [optional]
*		courierservice	=>	... [optional]
*		trackingno	=>	... [optional]
**/

function getRTODetailsForOrderId($order_id) {
	global $sqllog;
	$returnsQry = "SELECT * FROM mk_old_returns_tracking WHERE orderid = $order_id AND returntype='RTO'";
	$sqllog->debug("SQL Query : ".$returnsQry." "." @ line ".__LINE__." in file ".__FILE__);
	$returnsData = func_query_first($returnsQry);
	return $returnsData;
}

function getOrderReturnData($order_id, $return_type='RT'){
	global $sqllog;
	$returnsQry = "SELECT * FROM mk_old_returns_tracking WHERE orderid = '$order_id' AND returntype='$return_type'";
	$sqllog->debug("SQL Query : ".$returnsQry." "." @ line ".__LINE__." in file ".__FILE__);
	$returnsData = func_query_first($returnsQry);
	if ($returnsData) {
		if (!empty($returnsData['returnreason']) || !is_null($returnsData['returnreason'])) {
			$return_reasons = get_old_return_reasons();
			foreach($return_reasons as $k => $v){
				if ($v == $returnsData['returnreason']) {
					$returnsData['returnreasoncode'] = $k;
					break; 
				}		
			}	
		}
		$warehouse = WarehouseApiClient::getWarehouseDetails($returnsData['warehouseid']);
		$returnsData['wh_name'] = $warehouse['name'];
		if($returnsData['wh_name'] == '' || empty($returnsData['wh_name'])){
			$returnsData['wh_name'] = 'None';
		}
		$sqllog->debug($returnsData);
		return $returnsData;						
	}
		
	$result = array("orderid" => $order_id);	
	$orderData = func_order_data($order_id);
	if ($orderData && array_key_exists("userinfo", $orderData)) {
		$result["pickupaddress"]= $orderData['userinfo']['s_address'];
		$result["pickupcity"]	= $orderData['userinfo']['s_city'];
		$result["pickupstate"]	= $orderData['userinfo']['s_state'];
		$result["pickupcountry"]= $orderData['userinfo']['s_country'];
		$result["pickupzipcode"]= $orderData['userinfo']['s_zipcode'];
		$result['custname'] = $orderData['userinfo']['s_firstname'];
		$result['phonenumber'] = $orderData['userinfo']['mobile'];
		if ($orderData['order']['payment_method'] != 'cod') {
			$result['iscod'] = 0;
		} else {
			$result['iscod'] = 1;
		}
		$result['courier']      = $orderData['order']['courier_service'];
                $result['trackingnum']  = $orderData['order']['tracking'];
	}
	$sqllog->debug($result);
	return $result;
}

function get_old_rt_state_counts($return_type, $start_date=null, $end_date=null) {
	global $sqllog;
	
	$qry = "SELECT currstate, COUNT(*) AS count FROM mk_old_returns_tracking A, mk_old_returns_tracking_activity B " .
			"WHERE A.returntype='RTO' and A.id = B.returnid and A.currstate = B.activitycode ";
	if(!is_null($start_date)) {
		$qry.= "AND B.recorddate >= '" . substr($start_date,0,10)." 00:00:01'";
	}
	
	if (!is_null($end_date)) {
			$qry.= " AND B.recorddate <= '" . substr($end_date,0,10)." 23:59:59'";
	}	
	
	$qry .= " GROUP BY A.currstate"; 
	$sqllog->debug("SQL Query : ".$qry." "." @ line ".__LINE__." in file ".__FILE__);
	$data = func_query($qry, true);
	$sqllog->debug("Data: " . $data);
	$result = array();
	if ($data) {
		foreach($data as $rec) {
			$result[$rec['currstate']] = $rec['count'];	
		}
	}
	$sqllog->debug($result);
	return $result;	  
} 

function _formatSLATime($mins){

	$result = '';
	$days 	= floor($mins / (24*60));
	$result .= "$days"."d ";
	
	$minsleft = $mins % (24*60);	
	$hrs = floor($minsleft / (60));
	if ($hrs < 10 ) {
		$result .= "0$hrs"."h:";
	} else {
		$result .= "$hrs"."h:";
	}
	$minsleft = $minsleft % 60;
	if($minsleft < 10){
		$result .= "0$minsleft"."m";
	}else{
		$result .= "$minsleft"."m";
	}	
	return $result;
} 

function getOrderReturnActions($order_id) {
	global $sqllog;
	$qry = "SELECT * FROM mk_old_returns_tracking_activity WHERE returnid = ";
	$qry .= "(select id from mk_old_returns_tracking where orderid=$order_id) order by recorddate desc;";		
	$sqllog->debug("SQL Query : ".$qry." "." @ line ".__LINE__." in file ".__FILE__);	
	$data = func_query($qry);
	if ($data){
		foreach($data as &$d) {
			$d['sla_time_descr'] = _formatSLATime($d['sla_time']);
			$d['time_since_last_activity_descr'] =  _formatSLATime($d['time_since_last_activity']);	
		}	
		return $data;
	}		
	return array();	
}

function getLatestReturnAction($order_id, $action_code=null){
	global $sqllog;
        $qry = "SELECT A.* FROM mk_old_returns_tracking_activity AS A,  mk_old_returns_tracking  AS B ";
        $qry .= " WHERE B.orderid=$order_id AND A.returnid = B.id ";
	if (!is_null($action_code)) {
		$qry .= " AND A.activitycode='$action_code'";	
	} 
        $qry .= " ORDER BY A.recorddate DESC LIMIT 1";
        $sqllog->debug("SQL Query : ".$qry." "." @ line ".__LINE__." in file ".__FILE__);
        $result = func_query_first($qry);
        if (!$result) {
                return null;
        }
	$sqllog->debug($result);
        return $result;
}

/**
*	Checks if an order has been returned - RT / RTO 
*
**/
function isOrderReturned($order_id){
	global $sqllog;
	$qry = "SELECT orderid FROM mk_old_returns_tracking WHERE orderid = '$order_id'";
	$sqllog->debug("SQL Query : ".$qry." "." @ line ".__LINE__." in file ".__FILE__);
	$data = func_query_first($qry);	
	if ($data) {
		return true;
	}	
	return false;
}

function isRT($order_id) {
	global $sqllog;
        $qry = "SELECT orderid FROM mk_old_returns_tracking WHERE orderid = '$order_id' AND returntype='RT'";
        $sqllog->debug("SQL Query : ".$qry." "." @ line ".__LINE__." in file ".__FILE__);
        $data = func_query_first($qry);
        if ($data) {
                return true;
        }
        return false;
}

function isRTO($order_id) {
	global $sqllog;
        $qry = "SELECT orderid FROM mk_old_returns_tracking WHERE orderid = '$order_id' AND returntype='RTO'";
        $sqllog->debug("SQL Query : ".$qry." "." @ line ".__LINE__." in file ".__FILE__);
        $data = func_query_first($qry);
        if ($data) {
                return true;
        }
        return false;
}

function isUDOrder($order_id) {
	global $sqllog;
	$qry = "SELECT orderid FROM mk_old_returns_tracking_ud WHERE orderid = '$order_id'";
	$sqllog->debug("SQL Query : ".$qry." "." @ line ".__LINE__." in file ".__FILE__);
	$data = func_query_first($qry);
        if ($data) {
                return true;
        }
        return false;
}

/**
*	Check if an activty has already been performed on an order
*
**/
function isActivityPerformed($orderid, $activity_code) {
	global $sqllog;
	$qry = "SELECT * FROM mk_old_returns_tracking_activity WHERE activitycode='$activity_code' AND returnid = ";
	$qry .= "(select id from mk_old_returns_tracking where orderid='$orderid')";
	$sqllog->debug("SQL Query : ".$qry." "." @ line ".__LINE__." in file ".__FILE__);
        $data = func_query_first($qry);
        if ($data) {
		$sqllog->debug($data);
                return true;
        }
	$sqllog->debug("No records found");
        return false;
}

function prequeueRTO($orderid, $login, $reason=null){
	$status = true;
	$orderData = func_order_data($orderid);
	$iscod = $orderData['order']['payment_method'] == 'cod'?1:0;
	if ($orderData && array_key_exists("userinfo", $orderData)) {
		$custaddr= $orderData['userinfo']['s_address'];
		$custcity	= $orderData['userinfo']['s_city'];
		$custstate	= $orderData['userinfo']['s_state'];
		$custcountry= $orderData['userinfo']['s_country'];
		$custzipcode= $orderData['userinfo']['s_zipcode'];
		$custname = $orderData['userinfo']['s_firstname'];
		$phonenumber = $orderData['userinfo']['mobile'];
		$courier    = $orderData['order']['courier_service'];
		if(empty($courier)) {
			$courier = 'N.A.';
		}
		$trackingno  = $orderData['order']['tracking'];
		if(empty($trackingno)) {
			$trackingno = '0';
		}
				
		$comment = 'RTO pre-queued.';
		try{
			createRTOOrder($orderid, $iscod, $login, $custname,$custaddr,$custcity,
					$custstate,$custcountry,$custzipcode,$comment,$courier,$trackingno, $phonenumber, $reason, false,true,false, $date);
		}catch(Exception $e){
			$msg = "RTO preque for orderid $orderid failed. Reason: ". $e->getMessage();
			$sqllog->debug($msg);
			$status = $msg;
		}
	}
	else{
		$status = 'Order ' . $orderid . ' not found.';
	}
	
	return $status;
}
/**
*	Create an RTO Order
*
**/
function createRTOOrder($order_id,$iscod,$created_by,$cust_name, $pickup_address,$cust_city,$cust_state,$cust_country,$cust_zipcode,$comment, $courier, $trackingno, $phonenum=null, $reason=null, $isBulkUpload=false,$isFromUD = false,$warehouseid = false, $date='') {
	global $sqllog;
	
	$cust_name = mysql_real_escape_string($cust_name);
	$pickup_address = mysql_real_escape_string($pickup_address);
	$cust_city=mysql_real_escape_string($cust_city);
	$cust_state=mysql_real_escape_string($cust_state);
	$cust_country=mysql_real_escape_string($cust_country);
	$comment=mysql_real_escape_string($comment);
	$is_UDOrder = isUDOrder($order_id);
	$udstatus = "";
	if($is_UDOrder){
		$udData = getUDOrderData($order_id);
		$udstatus = $udData['currstate']."--".$udData['resolution'];
	}
	$rtostatus = 'RTOPREQ';
	if(!$isFromUD){
		$rtostatus = 'RTOQ';	
	}
	if(!$warehouseid){
		$warehouseid = NULL;
	}
	$orderQuery = "INSERT INTO mk_old_returns_tracking (orderid,iscod,returntype,currstate,custname,pickupaddress,pickupcity,pickupstate,pickupcountry,";
	$orderQuery .= "pickupzipcode, courier, trackingno, returnreason,ud_status,warehouseid, phonenumber) VALUES ";
	$orderQuery .= "($order_id, $iscod, 'RTO','$rtostatus','$cust_name','$pickup_address','$cust_city','$cust_state','$cust_country','$cust_zipcode',";
	$orderQuery .= "'$courier','$trackingno','$reason','$udstatus','$warehouseid'";
	if(is_null($phonenum)) {
		$orderQuery .= ",'')";
	} else {
		$orderQuery .= ", '$phonenum')";
	}
	$sqllog->debug("SQL Query : ".$orderQuery." "." @ line ".__LINE__." in file ".__FILE__);
	db_query($orderQuery);	
	if(!$isFromUD){
		
		_addAction($order_id, '', 'RTOQ', 'Queued', $created_by,  $comment, $date);
		add_order_comment('RTO', $order_id, $created_by, "RTOQ", "Queued", $comment);
		
		updateRTOOnUDResolution($order_id, $iscod, $created_by, $is_UDOrder, $udData);
		
	}else{
		_addAction($order_id, '', 'RTOPREQ', 'Pre Queued', $created_by,  $comment, $date);
	}
}

function updateRTOOnUDResolution($order_id, $iscod, $created_by, $is_UDOrder=false, $udData=false) {
    $cancel_rto = false;
	if($is_UDOrder){
		$cancel_rto = false;
	    // If Ud Calling is complete with RJ or empty resolution.. then reject order..
	    if($udData['currstate'] == 'UDCC' && ($udData['resolution'] == 'RJ' || empty($udData['resolution']))) {
	        $cancel_rto = true;
	    }
	} else{
	    // as there is no UD data here for this RTO.. if it is a cod auto cancel it..
	    $cancel_rto = $iscod?true:false;
	}
	$cancel_rto = $iscod?true:false;
	if($cancel_rto) {
	    $data = array();
            $data['activitycode']   = 'RTOC';
            $data['activity']       = 'RTO Cancelled';
            $data['remark']         = 'UD desposition Calling Complete (Rejected). Auto advancing RTOQ->RTOC.';
            $data['adminuser']      = $created_by;
            $data['rtocancelreason']= 'UD Reject';
            $data['currstate']      = 'RTOC';
    	    updateReturnsInfo('RTO', $order_id, 'RTOQ', 'RTOC', $data);
	}
}

function processPrequeuedRTO($order_id, $iscod, $created_by,$warehouseid){
	
	$is_UDOrder = isUDOrder($order_id);
	$udstatus = "";
	if($is_UDOrder){
		$udData = getUDOrderData($order_id);
		$udstatus = $udData['currstate']."--R".$udData['resolution'];
	}
	$data = array();
    $data['activitycode']   = 'RTOQ';
    $data['activity']       = 'RTO Queued';
    $data['remark']         = 'RTO Received at warehouse';
    $data['adminuser']      = $created_by;
    $data['currstate']      = 'RTOQ';
    $data['warehouseid']    = $warehouseid;
	updateReturnsInfo('RTO', $order_id, 'RTOPREQ', 'RTOQ', $data);
	add_order_comment('RTO', $order_id, $created_by, "RTOQ", "Queued", 'RTO Received at warehouse.');
	
	updateRTOOnUDResolution($order_id, $iscod, $created_by, $is_UDOrder, $udData);
}

function addActivityToRTOOrder($order_id, $order_state, $action, $created_by, $created_on, $comment="-") {
	global $sqllog;
	$data = array('currstate' => $order_state);
	func_array2update ('mk_old_returns_tracking', $data, $where = "orderid='$order_id'");
	
	return _addAction($order_id, $order_state, $action, $created_by, $created_on, $comment);	
}  

function createReturnOrder($order_id,$is_cod,$created_by,$return_reason,$cust_name,$cust_address,$cust_city,$cust_state,$cust_country,$cust_zipcode,$shipping_type,$comment,$phone_num,$pref_pickupdt=NULL) {
	global $sqllog;

        $created_by = mysql_real_escape_string($created_by);
        $return_reason = mysql_real_escape_string($return_reason);
	$cust_name = mysql_real_escape_string($cust_name);
	$cust_address=mysql_real_escape_string($cust_address);
	$cust_city=mysql_real_escape_string($cust_city);
	$cust_state=mysql_real_escape_string($cust_state);
	$cust_country=mysql_real_escape_string($cust_country);
        $cust_zipcode=mysql_real_escape_string($cust_zipcode);
        $comment=mysql_real_escape_string($comment);
        $phone_num=mysql_real_escape_string($phone_num);

	$returnQuery = "INSERT INTO mk_old_returns_tracking (orderid,iscod,returntype,currstate,returnreason,";
	if(!is_null($pref_pickupdt)) {
		$returnQuery .= "pickupdate,";		
	}
	$returnQuery .= "custname,pickupaddress,pickupcity,pickupstate,pickupcountry,pickupzipcode,shipmenttype,phonenumber) VALUES ";
	$returnQuery .= "($order_id, $is_cod,'RT', 'RTQ', '$return_reason', ";
	if (!is_null($pref_pickupdt)) {
		$returnQuery .= "'$pref_pickupdt',";
	}
	$returnQuery .= "'$cust_name', '$cust_address', '$cust_city', '$cust_state', '$cust_country', '$cust_zipcode', '$shipping_type', '$phone_num')";
	$sqllog->debug("SQL Query : ".$returnQuery." "." @ line ".__LINE__." in file ".__FILE__);
	db_query($returnQuery);

	if($shipping_type == 'SS') {
		$comment = "To be Shipped by Customer <br /><br />" . $comment;
	} else {
		$comment = "Pickup by Myntra. <br />Prefered pickup time : $pref_pickupdt <br /><br />" . $comment ;
	}		

	_addAction($order_id, '', 'RTQ', 'Queued', $created_by, $comment);
	
	add_order_comment('RT', $order_id, $created_by, "RTQ", "Queued", $comment);
}

/**
* common functions to both RT and RTO orders
**/
function _addAction($order_id, $prevactioncode, $actioncode, $action, $created_by, $comment="-", $cc_disposition, $cc_resolution, $date=''){
	global $sqllog;
	date_default_timezone_set('Asia/Calcutta');
	$curr_tm = date('Y-m-d H:i:s');
	if($date != '')
		$curr_tm = $date;
	$sla = null;
	if ($actioncode == 'RTAI' || $actioncode == 'RTRF') { 
		$sla = getSLA('RTQPA', $actioncode);
	} else if ($actioncode == 'RTOAI' || $actioncode == 'RTORF') { 
		$sla = getSLA('RTOC', $actioncode);
		$sqllog->debug("RTO SLA : $sla" );	
	}else {
		$sla = getSLA($prevactioncode, $actioncode);
	}
	$elapsed_tm = NULL;
	if ($sla) {
		$prevactiondata = null;
		if ($actioncode == 'RTRF' || $actioncode == 'RTAI') {
			$prevactiondata = getLatestReturnAction($order_id, "RTQPA");
		} else if ($actioncode == 'RTOAI' || $actioncode == 'RTORF') {
			$prevactiondata = getLatestReturnAction($order_id, "RTOC");
			$sqllog->debug($prevactiondata);
		}else {
			$prevactiondata = getLatestReturnAction($order_id);
		}
		$prev_tm = $prevactiondata['recorddate'];
		$elapsed_tm = floor(abs(strtotime($curr_tm) - strtotime($prev_tm))/60);
		$sqllog->debug("SLA : ". $sla. " elapsed time : ". $elapsed_tm);	
	} else {
		$sqllog->debug("SLA not set for $actioncode");	
	}
	$activityQuery = "INSERT INTO mk_old_returns_tracking_activity (returnid,activitycode,activity,adminuser,remark,recorddate,time_since_last_activity,sla_time";
	
	if($cc_disposition){
		$activityQuery .=", cc_disposition";
	}
	
	if($cc_resolution){
		$activityQuery .= ", cc_resolution";
	}
	
	$activityQuery .=")  VALUES";
	
	
        $activityQuery .= " ((SELECT id FROM mk_old_returns_tracking WHERE orderid='$order_id'), '$actioncode', '$action', '$created_by', '$comment', '$curr_tm'";
	if ($sla) {
		$activityQuery .= ", $elapsed_tm, $sla";
	}else {
		$activityQuery .= ",0,0";
	}
	
	if($cc_disposition){
		$activityQuery .=", '$cc_disposition'";
	}
	
	if($cc_resolution){
		$activityQuery .= ", '$cc_resolution'";
	}
	
	$activityQuery .=")";
        $sqllog->debug("SQL Query : ".$activityQuery." "." @ line ".__LINE__." in file ".__FILE__);
        db_query($activityQuery);
        return true;	
}

function getSLA($prevactivity, $activity) {
	global $sqllog;
	$qry = "SELECT maxtime FROM mk_old_returns_tracking_sla WHERE prevstate='$prevactivity' AND nextstate = '$activity'";
	$sqllog->debug("SQL Query : ".$qry." "." @ line ".__LINE__." in file ".__FILE__);
	$result = func_query_first($qry);
	if(!$result) {
		return false;
	}
	return $result['maxtime'];				
}


function updateReturnsInfo($returntype, $order_id, $prev_state, $next_state, $data, $cc_disposition, $cc_resolution) {
	$activitycode	= $data['activitycode'];
	$activity 	= $data['activity'];
	$comment	= $data['remark'];
	$adminuser	= $data['adminuser'];
	
	unset($data['activitycode']);
	unset($data['activity']);
	unset($data['remark']);
	unset($data['adminuser']);
	
	if($next_state == 'RTRSC' || $next_state == 'RTORSC') {
		if(array_key_exists('trackingno', $data)) {
			$data['revtrackingno'] = $data['trackingno'];
			unset($data['trackingno']);
		}	
		if (array_key_exists('courier', $data)) {
			$data['revcourier'] = $data['courier'];
			unset($data['courier']);
		}
		$comment = "Reshipping <br /> Courier : ". $data['revcourier']. "<br /> Tracking # : " . $data['revtrackingno']. "<br /><br />$comment";
	}	
	
	if ($next_state == 'RTPI') {
		$comment = "Pickup Initiated <br /> Courier : " . $data['courier'] . "<br /> Rererence # : " . $data['trackingno'] . "<br /><br />$comment";
	} else if ($next_state == 'RTCSD') {
		$comment = "Customer shipped items <br /> Courier : " . $data['courier'] . "<br /> Tracking # : " . $data['trackingno'] . "<br /><br />$comment";
	} else if ($next_state == 'RTPTU') {
		$comment = "Tracking number Assigned : <br /> Tracking # : " . $data['trackingno'] . "<br /> <br />$comment";
	} else if ($next_state == 'RTRF' || $next_state == 'RTORF') {
		$comment = "Refund : <br/> Coupon-code: ".$data['couponcode']. "<br/ >Coupon-Value : ".$data['couponvalue']."<br/><br/>$comment";	
	} else if ($next_state == 'RTAI' || $next_state == 'RTOAI') {
		$comment = "Restocked : <br/> Items Value Rejected : ".$data['item_val_rejected']."<br/>Items Value Restocked : ".$data['item_val_restocked']."<br/><br/>$comment";
	} else if( $next_state == 'RTOCAL1' || $next_state == 'RTOCAL2' || $next_state == 'RTOCAL3'){
		$comment = "Customer contacted. " . "<br /><br />$comment";
	}

	global $sqllog;
	$sqllog->debug($data);	
	func_array2update('mk_old_returns_tracking', $data, $where = "orderid='$order_id'");
	_addAction($order_id, $prev_state, $activitycode, $activity, $adminuser, $comment, $cc_disposition, $cc_resolution);	
	add_order_comment( $returntype, $order_id, $adminuser, $next_state, $activity, $comment);
	if ( ( $activitycode == 'RTRF' && !is_null(getLatestReturnAction($order_id, 'RTAI')) ) || 
		( $activitycode == 'RTAI' && !is_null(getLatestReturnAction($order_id, 'RTRF'))) || 
		( $activitycode == 'RTRSC' ) ) {
		
		func_array2update('mk_old_returns_tracking', array('currstate' => 'END'), $where = "orderid='$order_id'");	
	}


	$orderData = getOrderReturnData($order_id, 'RTO');	
	$iscod = $orderData['iscod'];	

	if ( ( $activitycode == 'RTORSC' ) || 
		( $activitycode == 'RTORF' && $iscod == 0 && !is_null(getLatestReturnAction($order_id, 'RTOAI')) ) || 
		( $activitycode == 'RTOAI' && $iscod == 0 && !is_null(getLatestReturnAction($order_id, 'RTORF'))) || 
		( $activitycode == 'RTOAI' && $iscod == 1 ) ) {
	
		func_array2update('mk_old_returns_tracking', array('currstate' => 'END'), $where = "orderid='$order_id'");	
	}	

	$sqllog->debug($next_state);	
	if ($next_state == 'RTCL' || $next_state == 'RTOCL') {
		$sqllog->debug($next_state);	
		func_array2update('mk_old_returns_tracking', array('currstate' => 'END'), $where = "orderid='$order_id'");	
	}
	
	sendStateChangeMail($order_id, $prev_state, $next_state, 'RTO');
	
	if( $next_state == 'RTOC'){ 
		//|| (!is_null($cc_disposition) && $cc_disposition == 'CM' && !is_null($cc_resolution) && $cc_resolution == 'RJ')) {
		
		$customerReject = ($cc_disposition == 'CM' && $cc_resolution == 'RJ')?true:false;
		rto_ud_cancelled_order_update_items($order_id, $adminuser, $customerReject, $comment);
		
		// as the order is cancelled, its amount is also refunded.. Need to auto advance it to RTORF state
		
		$next_state_data = array();
        $next_state_data['activitycode']   = 'RTORF';
        $next_state_data['activity']       = 'RTO Refunded';
        $next_state_data['remark']         = 'Auto advancing RTOC -> RTORF.';
        $next_state_data['adminuser']      = $data['adminuser'];
        $next_state_data['currstate']      = 'RTORF';
	
        updateReturnsInfo('RTO', $order_id, 'RTOC', 'RTORF', $next_state_data);
        
	}
}

/**
 * Add comment
 */
 function add_order_comment($returntype, $orderid, $addedby, $next_state,  $activity, $comment){
 	$all_states = null;
 	if($returntype == 'RTO'){
 		$all_states = _get_old_rto_states(); 
 	}else{
 		$all_states = _get_old_return_states();
 	}
	$commenttitle = "Order in " . $all_states[$next_state];
	$commenttype  = $returntype. " processing";
    $order   = array('<br />', '<br / >', '<br/>');
	$replace = "\n";
	$comment = str_replace($order, $replace, $comment);
   	$comment = "Activity: " . $activity . "\n\n" . $comment;
    func_addComment_order($orderid, $addedby, $commenttype, $commenttitle, $comment);
 }

/**
* Data Formatting funcitons 
**/
function populate_ud_data($return_request) {
        $udstates = _get_old_ud_states();
		$return_request['currstate'] = $udstates[$return_request['currstate']];
		$resolution = _get_old_ud_cc_resolution();
		$return_request['resolution'] = $resolution[$return_request['resolution']];
        return $return_request;
}

function format_ud_data($return_requests){
	if($return_requests) {
                foreach ($return_requests as $index=>$request) {
                        $return_requests[$index] = populate_ud_data($request);
                }
                return $return_requests;
        }
        return false;
}
function populate_return_data($return_request) {
	$returnStatuses = _get_old_return_states();
	$rtoStatuses = _get_old_rto_states(); 
	if (array_key_exists($return_request['currstate'], $returnStatuses)) {
		$return_request['currstate'] = $returnStatuses[$return_request['currstate']];
	} else {
		$return_request['currstate'] = $rtoStatuses[$return_request['currstate']];
	}
	$warehouse = WarehouseApiClient::getWarehouseDetails($return_request['warehouseid']);
	$return_request['wh_name'] = $warehouse['name'];
	return $return_request;
}


function format_return_data($return_requests){
	if($return_requests) {
                foreach ($return_requests as $index=>$request) {
                        $return_requests[$index] = populate_return_data($request);
                }
                return $return_requests;
        }
        return false;	
}

function get_xls_data_for_returns($results, $format) {
	$ret_set = $results;
        if($format == 'BD') {
                $ret_set = array();
                foreach($results as $request) {
                        $new_req = array();
                        $new_req['Return Number'] = $request['id'];
			$new_req['Order Id'] = $request['orderid'];
                        $new_req['Name'] = $request['custname'];
                        $new_req['Product'] = '';
                        $new_req['Address1'] = $request['pickupaddress'];
                        $new_req['Address2'] = "";
                        $new_req['Address3'] = $request['pickupcity'].",".$request['pickupstate'].",".$request['pickupcountry'];
                        $new_req['Pincode'] = $request['pickupzipcode'];
                        $new_req['Phone'] = $request['phonenumber'];
                        $new_req['Mobile'] = $request['phonenumber'];
                        $new_req['Pieces'] = '';
                        $new_req['Weight'] = "";
                        $new_req['Amount'] = '';
                        $new_req['Courier'] = 'BD';
                        $new_req['Tracking No'] = '';
                        $ret_set[] = $new_req;
                }
        } else if ($format == 'DD') {
		$ret_set = array();
                foreach($results as $request) {
                        $new_req = array();
                        $new_req['Return Number'] = $request['id'];
			$new_req['Order Id'] = $request['orderid'];
                        $new_req['Customer Name'] = $request['custname'];
                        $new_req['Product'] = '';
                        $new_req['Address'] = $request['pickupaddress'].",".$request['pickupcity'].",".$request['pickupstate'].",".$request['pickupcountry'];
                        $new_req['Pincode'] = $request['pickupzipcode'];
                        $new_req['City'] = $request['pickupcity'];
                        $new_req['Phone'] = $request['phonenumber'];
                        $new_req['Mobile'] = $request['phonenumber'];
                        $new_req['Pieces'] = "";
                        $new_req['weight'] = "";
                        $new_req['Amount'] = "";
                        $new_req['Courier'] = 'DD';
                        $new_req['Tracking No'] = '';
                        $ret_set[] = $new_req;
                }
	} elseif ($format == 'HD') {
                $ret_set = array();
                foreach($results as $request) {
                        $new_req = array();
                        $new_req['Return Number'] = $request['id'];
			$new_req['Order Id'] = $request['orderid'];
                        $new_req['Customer Name'] = $request['custname'];
                        $new_req['Product'] = "";
                        $new_req['Address'] = $request['pickupaddress'].",".$request['pickupcity'].",".$request['pickupstate'].",".$request['pickupcountry'];
                        $new_req['Pincode'] = $request['pickupzipcode'];
                        $new_req['City'] = $request['pickupcity'];
                        $new_req['Phone'] = $request['phonenumber'];
                        $new_req['Mobile'] = $request['phonenumber'];
                        $new_req['Pieces'] = "";
                        $new_req['Amount'] = "";
                        $new_req['Courier'] = 'HD';
                        $new_req['Tracking No'] = '';
                        $ret_set[] = $new_req;
                }
        } 
	return $ret_set;
		
}

/**
** Old return && RTO statuses 
**/
function get_old_return_reasons(){
        return array('PQI' => 'Product Quality Issue',
                        'FI' => 'Fitment Issue',
                        'PNAD' => 'Product Not As Displayed',
                        'WPS' => 'Wrong Product Shipped',
			'WPO' => 'Wrong Product Ordered'
                );
}

function _get_old_return_states(){
	return array('RTQ' => 'Return Queued',
                        'RTCSD' => 'Return Customer Shipment Details',
                        'RTPI' => 'Return Pickup Initiated',
                        'RTPTU' => 'Tracking # for the pickup updated.',
                        'RTR' => 'Return Received At Warehouse',
                        'CMMNT' => 'Add Comment',
                        'RTCC' => 'Customer Contacted',
                        'RTRS' => 'Order To Be Reshipped',
                        'RTRSC' => 'Order Reshipped',
                        'RTQP' => 'QA Passed',
                        'RTQF' => 'QA Failed',
                        'RTQPA' => 'QA Pass Approved By Supervisor',
                        'RTQPNA' => 'QA Pass Not Approved By Supervisor',
                        'RTRF' => 'Order Refunded',
                        'RTAI' => 'Items Restocked',
			'RTCL' => 'Stop Tracking',
			'END'  => 'Processing Complete'
                );	
}

function _get_old_return_valid_transitions() {
	return array('RTQ' => array('RTPI', 'RTCSD', 'RTCL'),
                                        'RTPI'  => array('RTPTU', 'RTCL'),
                                        'RTPTU' => array('RTR'),
                                        'RTR'   => array('RTQP', 'RTQF'),
                                        'RTQP'  => array('RTQPA', 'RTQPNA'),
                                        'RTQPA' => array('RTRF', 'RTAI'),
                                        'RTRF'  => array('RTAI'),
                                        'RTAI'  => array('RTRF'),
                                        'RTQPNA'=> array('RTCC'),
                                        'RTCC'  => array('RTRS'),
                                        'RTRS'  => array('RTRSC'),
                                        'RTQF'  => array('RTCC'),
                                        'RTCSD' => array('RTR', 'RTCL'),
                                        'CMMNT' => array('RTCSD','RTPI','RTPTU','RTR','RTCC','RTRS','RTRSC','RTQP','RTQF','RTQPA','RTQPNA','RTRF','RTAI')
                      );
}

function _get_old_return_all_user_perms() {
	$result = array();
        $result['*']    = array('RTCL'); //array('CMMNT');
        $result['CC']   = array('RTQ', 'RTCSD', 'RTRF', 'RTCC', 'RTRS');
        $result['LG']   = array('RTPI', 'RTPTU', 'RTR', 'RTRSC');
	$result['QA']	= array('RTQP', 'RTQF', 'RTQPA', 'RTQPNA');
	$result['IN']	= array('RTAI');
        return $result;
}

function _get_old_return_user_perms($user_roles) {
	global $weblog; 
	$perms = _get_old_return_all_user_perms();
	$retVal = array();
	if (in_array('AD', $user_roles)){
        $retVal = array_merge($retVal, $perms['*'], $perms['CC'], $perms['LG'], $perms['QA'], $perms['IN']);
    }
    
    if (in_array('CS', $user_roles)) {
        $retVal = array_merge($retVal, $perms['*'], $perms['CC']);
    } 
    
    if (in_array('OP', $user_roles)) {
        $retVal = array_merge($retVal, $perms['*'], $perms['LG'], $perms['QA'], $perms['IN']);
    }
	
	$retVal = array_unique($retVal);
	$weblog->debug($retVal);
	return $retVal;	
}

function get_old_return_next_states($orderid, $user_roles) {
	$states = _get_old_return_states();
	$valid_transitions =_get_old_return_valid_transitions();
	$user_perms     = _get_old_return_user_perms($user_roles);	
	
	$returnData = getOrderReturnData($orderid); 
	$curr_state = Null;	
	$next_states	= array();
	if( !array_key_exists('returntype', $returnData)){
		$next_states = array('RTQ');
		$curr_state  = '';	
	}else{
		$next_states = $valid_transitions[$returnData['currstate']];	
		$curr_state  = $returnData['currstate'];
	}

	$final_states 	= array_intersect($user_perms, $next_states);

	
	$filterred_states = array();
	foreach($final_states as $st) {
		if ($curr_state == 'RTQ') {
			if ($st == 'RTCSD') { 
				if ( ($returnData['shipmenttype'] == 'SS') && (in_array('AD', $user_roles) || in_array('CS', $user_roles))) {
					$filterred_states[] = $st;		
				}	
			} else if ($st == 'RTPI') {
				if ( ($returnData['shipmenttype'] == 'MS') && (in_array('AD', $user_roles) || in_array('OP', $user_roles))) {
					$filterred_states[] = $st;
				}		
			} else {
				$filterred_states[] = $st;
			} 
		} else if ($curr_state == 'RTRF') {
			if ( $st == 'RTAI' ) {
				if (!isActivityPerformed($orderid, 'RTAI') ) {
					$filterred_states[] = $st;		
				}
			}				
		} else if ($curr_state == 'RTAI') {	
			if ($st == 'RTRF') {
				if (!isActivityPerformed($orderid, 'RTRF')) {
					$filterred_states[] = $st;
				}
			}
		} else {
			$filterred_states[] = $st;
		}		
	}	
	
	global $sqllog;
        $sqllog->debug("Next States ==> ");
        $sqllog->debug($next_states);
        $sqllog->debug("User perms ==> ");
        $sqllog->debug($user_perms);
        $sqllog->debug("final states => ");
        $sqllog->debug($final_states);
	$sqllog->debug("Filtered states => ");
	$sqllog->debug($filterred_states);
	
	$result = array();
	foreach($filterred_states as $st) {
		$result[$st] = $states[$st];
	}				
	
	return $result;
}

function _get_old_rto_states(){
	return array(
		'RTOPREQ' 	=> 'RTO Pre Queue State',
		'RTOQ' 	=> 'RTO Queued', 
		'RTOCAL1'	=> 'RTO Calling Attempt - 1',
		'RTOCAL2' 	=> 'RTO Calling Attempt - 2',
		'RTOCAL3' 	=> 'RTO Calling Attempt - 3',
		'RTORSC'	=> 'RTO Reshipped', 
		'RTOC' 		=> 'RTO Cancelled', 
		'RTORS' 	=> 'RTO to be Reshipped', 
		'RTORF' 	=> 'RTO Refunded', 
		'RTOAI' 	=> 'Items Restocked', 
		'CMMNT' 	=> 'Add Comment',
		'END'  		=> 'Processing Complete'
	);	
}

function _get_old_rto_valid_transitions() {
	return array(
			'RTOQ' => array('RTOCAL1'), 
			'RTOCAL1' => array('RTOCAL2', 'RTOC', 'RTORS'),
			'RTOCAL2' => array('RTOCAL3', 'RTOC', 'RTORS'),
			'RTOCAL3' => array('RTOC', 'RTORS'),
			'RTOC' => array('RTORF', 'RTOAI'), 
			'RTORS' => array('RTORSC'), 
			'RTORF' => array('RTOAI'), 
			'RTOAI' => array('RTORF')
		);
}

function _get_old_rto_all_user_perms() {
	$results = array();
	$results['*']   = array(); // array('CMMNT');
        $results['CC']  = array('RTOC', 'RTORS', 'RTORF', 'RTOCAL1', 'RTOCAL2', 'RTOCAL3');
        $results['LG']	= array('RTOQ', 'RTORSC');
        $results['IN']	= array('RTOAI');
	return $results;
}

function _get_old_rto_user_perms($user_roles) {
	global $weblog; 
	$perms = _get_old_rto_all_user_perms();
	$retVal = array();

	if (in_array('AD', $user_roles)) {
        $retVal = array_merge($retVal, $perms['*'], $perms['CC'], $perms['LG'], $perms['IN']);
    } 
    
    if (in_array('CS', $user_roles)) {
        $retVal = array_merge($retVal, $perms['*'], $perms['CC']);
    }
    
    if (in_array('OP', $user_roles)) {
        $retVal = array_merge($retVal, $perms['*'], $perms['LG'], $perms['IN']);
    }
	
	$retVal = array_unique($retVal);
	$weblog->debug($retVal);
	return $retVal;
}

function get_old_rto_next_states($orderid, $user_roles) {

	$states = _get_old_rto_states();
	$valid_transitions = _get_old_rto_valid_transitions();
	$user_perms     = _get_old_rto_user_perms($user_roles);	

	$returnData = getOrderReturnData($orderid, 'RTO');
        $curr_state = Null;
        $next_states    = array();
        if( !array_key_exists('returntype', $returnData)){
                $next_states = array('RTOQ');
                $curr_state  = '';
        }else{
                $next_states = $valid_transitions[$returnData['currstate']];
                $curr_state  = $returnData['currstate'];
        }	

	$final_states   = array_intersect($user_perms, $next_states);

	$filterred_states = array();
	
	foreach($final_states as $st) {
		if ($curr_state == 'RTOC') {
			if ( $st == 'RTORF') {
				if ($returnData['iscod'] == 0 && (in_array('AD',$user_roles) || in_array('CS',$user_roles))) {
					$filterred_states[] = $st;	
				}
			} else if ($st == 'RTOAI') {
				if (in_array('AD',$user_roles) || in_array('OP',$user_roles)) {
					$filterred_states[] = $st;
				}	
			}
		} else if ($curr_state == 'RTORF') {
			if ($st == 'RTOAI') {
				if (!isActivityPerformed($orderid, 'RTOAI')) {
					$filterred_states[] = $st;
				}
			}
		} else if ($curr_state == 'RTOAI') {
			if ($st == 'RTORF') {
                                if (!isActivityPerformed($orderid, 'RTORF') && $returnData['iscod'] == 0) {
                                        $filterred_states[] = $st;
                                }
                        }
		} else {
			$filterred_states[] = $st;
		}
	}
	
	
	global $sqllog;
	$sqllog->debug("Return Data => ");
	$sqllog->debug($returnData);
        $sqllog->debug("Next States ==> ");
        $sqllog->debug($next_states);
        $sqllog->debug("User perms ==> ");
        $sqllog->debug($user_perms);
        $sqllog->debug("final states => ");
        $sqllog->debug($final_states);
        $sqllog->debug("Filtered states => ");
        $sqllog->debug($filterred_states);
	
	$result = array();
        foreach($filterred_states as $st) {
                $result[$st] = $states[$st];
        }

        return $result;	 
}

function get_old_rto_statuses($user_role='AD') {
	
	$common_actions 	= array('CMMNT' => 'Add Comment');	

	$logistics_actions	= array('RTOQ' => 'RTO Queued', 
					'RTORSC' => 'RTO Reshipped');
	$cc_actions 		= array('RTOC' => 'RTO Cancelled', 
					'RTORS' => 'RTO to be Reshipped');
	$inventory_actions	= array('RTORF' => 'RTO Refunded', 
					'RTOAI' => 'Items Restocked');
	
	if ($user_role == 'AD') {
		return array_merge($common_actions, $logistics_actions, $cc_actions, $inventory_actions);
	} else if ($user_role == 'CS') {
		return array_merge($common_actions, $cc_actions);
	} else if ($user_role == 'OP') {
		return array_merge($common_actions, $logistics_actions, $inventory_actions);
	}
}

function get_old_rto_cancel_reasons() {
	return array('CR' => 'Customer Request', 
			'CU' => 'Customer Unreachable');		
}


/**
** UD - Undelivery helper functions 
**/

function _get_old_ud_states() {
	return array('UDQ' => 'UD Queued',
			'UDC1' => 'UD Calling Attemp - 1',
			'UDC2' => 'UD Calling Attemp - 2',
			'UDC3' => 'UD Calling Attemp - 3',
			'UDCC'  => 'UD Calling Complete',
			'UDLS' 	=> 'UD List Sent',
			'END'   => 'Processing Complete' ) ;
}

function _get_old_ud_cc_dispositions() {
	return array('NRB' => 'Not Reachable',
			'NRS' => 'No Response',
			'BSY' => 'Busy',
			'WRN'=>'Wrong number',
			'INV' =>'Phone number invalid',
			'PSO' =>'Phone switched off',
			'CM' => 'Customer contacted',
	);		
}

function _get_old_ud_cc_resolution() {
	return array(
			'CB'  => 'Call back later',
			'RJ' => 'RTO Order',
			'RA' => 'Reattempt Delivery',
			'SD' => 'Already Delivered'
	);
}

function _get_old_rto_cc_dispositions() {
	return array('NRB' => 'Not Reachable',
			'NRS' => 'Not Responding',
			'BSY' => 'Busy. Call back later',
			'CM' => 'Customer contacted.');		
}

function _get_old_rto_cc_resolution() {
	return array('RJ' => 'Cancel Order',
			'RA' => 'Reship order');
}

function _get_old_ud_all_user_perms() {
	$result = array();
	$result['*']	= array(); //array('CMMNT');
	$result['CC'] 	= array('UDC1', 'UDC2', 'UDC3', 'UDCC');
	$result['LG']	= array('UDQ', 'UDLS');
	return $result;
}

function _get_old_ud_user_perms($user_roles) {
	global $weblog; 
	$result = _get_old_ud_all_user_perms();
	
	$retVal = array();
	if (in_array('AD', $user_roles)) {
		 $retVal = array_merge($retVal, $result['*'], $result['CC'], $result['LG']);
	} 
	
	if (in_array('CS', $user_roles)) {
		 $retVal = array_merge($retVal, $result['*'], $result['CC']);
	}
	
	if (in_array('OP', $user_roles)) {
		 $retVal = array_merge($retVal, $result['*'], $result['LG']);
	}
	
	$retVal = array_unique($retVal);
	$weblog->debug($retVal);
	return $retVal;
}

function _get_old_ud_valid_transitions() {
	return array('UDQ' => array('UDC1'),
			'UDC1' => array('UDC2'),
			'UDC2' => array('UDC3'),
			'UDC3' => array('UDCC'),
			'UDCC' => array('UDLS'));
}


function get_ud_next_states($orderid, $user_roles) {
	$states	= _get_old_ud_states();
	$valid_transitions = _get_old_ud_valid_transitions();
	
	$user_perms 	= _get_old_ud_user_perms($user_roles);
	$ud_data	= getUDOrderData($orderid);
	$curr_state	= null;
	$next_states	= array();
	if (!array_key_exists('resolution', $ud_data)) {
		$next_states = array('UDQ');
		$curr_state = '';
	} else {
		$next_states = $valid_transitions[$ud_data['currstate']];	
		$curr_state  = $ud_data['currstate'];
	}		 	
	
	$final_states   = array_intersect($user_perms, $next_states);
	
	$filterred_states = $final_states;
	// TODO: filtering
	global $sqllog;
        $sqllog->debug("UD Data => ");
        $sqllog->debug($ud_data);
        $sqllog->debug("Next States ==> ");
        $sqllog->debug($next_states);
        $sqllog->debug("User perms ==> ");
        $sqllog->debug($user_perms);
        $sqllog->debug("final states => ");
        $sqllog->debug($final_states);
        $sqllog->debug("Filtered states => ");
        $sqllog->debug($filterred_states);

        $result = array();
        foreach($filterred_states as $st) {
                $result[$st] = $states[$st];
        }

        return $result;				
}

function getUDOrderData($order_id) {
        global $sqllog;
        $result = array("orderid" => $order_id);
        $qry = "SELECT * FROM mk_old_returns_tracking_ud WHERE orderid = '$order_id'";
        $sqllog->debug("SQL Query : ".$qry." "." @ line ".__LINE__." in file ".__FILE__);
        $udData = func_query_first($qry);
        foreach($udData as $k=>$v) {
                $result[$k] = $v;
        }

        $orderData = func_order_data($order_id);
        if ($orderData && array_key_exists("userinfo", $orderData)) {
            $result["pickupaddress"]= $orderData['userinfo']['s_address'];
            $result["pickupcity"]   = $orderData['userinfo']['s_city'];
            $result["pickupstate"]  = $orderData['userinfo']['s_state'];
            $result["pickupcountry"]= $orderData['userinfo']['s_country'];
            $result["pickupzipcode"]= $orderData['userinfo']['s_zipcode'];
            $result['custname']     = $orderData['userinfo']['s_firstname'];
            $result['phonenumber']  = $orderData['userinfo']['mobile'];
            $result['shippeddate']  = date('Y-m-d H:i:s',$orderData['order']['shippeddate']);
			if ($orderData['order']['payment_method'] != 'cod') {
                $result['iscod'] = 0;
            } else {
                $result['iscod'] = 1;
            }
			$result['courier']	= $orderData['order']['courier_service'];
			$result['trackingnum']	= $orderData['order']['tracking'];
			$result['age']	= round((time()-$orderData['order']['shippeddate'])/3600,1);
        }
        return $result;
}

function addUDAction($order_id, $prev_action_code,$action_code,$created_by,$comment='-',$cc_disposition=null, $cc_resolution=null,$reason='') {
	global $sqllog, $login,$weblog;
	date_default_timezone_set('Asia/Calcutta');
        $curr_tm = date('Y-m-d H:i:s');
	$data = array();
	$data['recorddate'] = $curr_tm;
	$orderData      = getUDOrderData($order_id);
	$data['udid']   = $orderData['id'];
	$data['activitycode']	= $action_code;
	$data['adminuser']	= $created_by;
	$data['remark']		= $comment;
	if (!is_null($cc_disposition)) {
		$data['cc_disposition']	= $cc_disposition;					
	}
	if (!is_null($cc_resolution)) {
		$data['cc_resolution'] = $cc_resolution;
	}
	func_array2insert("mk_old_returns_tracking_ud_activity", $data);
	$error = false;
	$errorMsg = "";
	if( $cc_resolution == 'RJ'  || ( $action_code == 'UDC3' &&  is_null($cc_resolution) ) ) {
		if($orderData['courier'] == 'ML'){
			$rtofiled = ShipmentApiClient::fileRto($order_id);
			if(!$rtofiled['status']){
				$error = true;
				$errorMsg = $rtofiled['reason'];
				$weblog->debug("UD - RTO lms integration failed ::::: ".$errorMsg);
			}	
		}
	}
	if($error){
		return array("status" => false,"reason" => "rto filing failed with lms -- ".$errorMsg );
	}
	if($action_code == 'UDQ'){
		
		$masterData = array('currstate' => $action_code);
		if($reason != ''){
			$masterData['reason'] = $reason;
		}
		func_array2update ('mk_old_returns_tracking_ud', $masterData, $where = "orderid='$order_id'");
		
	}elseif ( ($action_code == 'UDC1' || $action_code == 'UDC2') ) {
		if (is_null($cc_resolution)) {
			$masterData = array('currstate' => $action_code);
			func_array2update ('mk_old_returns_tracking_ud', $masterData, $where = "orderid='$order_id'"); 		
		} else {
			unset($data['cc_disposition']);
			unset($data['cc_resolution']);
			$data['activitycode'] = 'UDCC';
			$data['remark'] = '-';
			func_array2insert("mk_old_returns_tracking_ud_activity", $data);
		
			$masterData = array('currstate' => 'UDCC', 'resolution' => $cc_resolution);
        		func_array2update ('mk_old_returns_tracking_ud', $masterData, $where = "orderid='$order_id'");
		}
	} else if ($action_code == 'UDC3') {
		unset($data['cc_disposition']);
                unset($data['cc_resolution']);
                $data['activitycode'] = 'UDCC';
		$data['remark'] = '-';
                func_array2insert("mk_old_returns_tracking_ud_activity", $data);	
		
		$masterData = array('currstate' => 'UDCC');
		if (is_null($cc_resolution)) {
			$masterData['resolution'] = 'RJ';		
		} else {
			$masterData['resolution'] = $cc_resolution;		
		}
		func_array2update ('mk_old_returns_tracking_ud', $masterData, $where = "orderid='$order_id'");
	} else if ($action_code == 'UDLS') {
		//$data['activitycode'] = 'END';
		//$data['remark'] = '-';
		//func_array2insert("mk_old_returns_tracking_ud_activity", $data);

		$masterData = array('currstate' => 'END');
		func_array2update ('mk_old_returns_tracking_ud', $masterData, $where = "orderid='$order_id'");	
	}	
	if( ($cc_disposition == 'CM' && $cc_resolution == 'SD')  || ($action_code == 'UDC3' &&  $cc_resolution == 'SD') ) {
		func_change_order_status($order_id, 'DL','AUTOSYSTEM','Marking delivered from rto tracker');
		if(!$orderData['iscod']){
			func_change_order_status($order_id, 'C','AUTOSYSTEM','Marking complete from rto tracker');	
		}
	}
	if( $cc_resolution == 'RJ'  || ( $action_code == 'UDC3' &&  is_null($cc_resolution) ) ) {
		if(!isRTO($order_id)){
			$orderData = func_order_data($order_id);
			$custaddr= $orderData['userinfo']['s_address'];
			$custcity	= $orderData['userinfo']['s_city'];
			$custstate	= $orderData['userinfo']['s_state'];
			$custcountry= $orderData['userinfo']['s_country'];
			$custzipcode= $orderData['userinfo']['s_zipcode'];
			$custname = $orderData['userinfo']['s_firstname'];
			$phonenumber = $orderData['userinfo']['mobile'];
			if ($orderData['order']['payment_method'] != 'cod') {
				$iscod = 0;
			} else {
				$iscod = 1;
			}
			$courier    = $orderData['order']['courier_service'];
			if(empty($courier)) {
				$courier = 'N.A.';
			}
			$trackingno  = $orderData['order']['tracking'];
			if(empty($trackingno)) {
				$trackingno = '0';
			}
			$comment = "Pre Queuing RTO from ud tracker";
	
			createRTOOrder($order_id, $iscod, $created_by, $custname,$custaddr,$custcity,$custstate,$custcountry,$custzipcode,$comment,$courier,$trackingno, $phonenumber,null,false,true);
		}else{
			updateRTOOnUDResolution($order_id, $orderData['iscod'] ,$created_by,true, $masterData);			
		}
	}
	
	sendStateChangeMail($order_id, $prev_action_code, $action_code, 'UD');
	return array("status" => true);
}	

function createUDOrder($adminuser, $data,$remark = ''){
	func_array2insert ("mk_old_returns_tracking_ud", $data);
	addUDAction($data['orderid'], '', 'UDQ', $adminuser, 'UD Queued -- '.$remark);	
}

function getUDActions($order_id) {
        global $sqllog;
	$states = _get_old_ud_states();
        $qry = "SELECT * FROM mk_old_returns_tracking_ud_activity WHERE udid = ";
        $qry .= "(select id from mk_old_returns_tracking_ud where orderid=$order_id) order by recorddate desc;";
        $sqllog->debug("SQL Query : ".$qry." "." @ line ".__LINE__." in file ".__FILE__);
        $data = func_query($qry);
        if ($data){
                foreach($data as &$d) {
			$d['activity'] = $states[$d['activitycode']];
                        $d['sla_time_descr'] = _formatSLATime($d['sla_time']);
                        $d['time_since_last_activity_descr'] =  _formatSLATime($d['time_since_last_activity']);
                }
                return $data;
        }
        return array();
}

/**
**	SLA Statistics
**/

function getSLAComplianceStats($return_type, $start_date=null, $end_date=null){
	$states = null;
	if ($return_type == 'RT') {
		$states = _get_old_return_states();
	} else {
		$states = _get_old_rto_states(); 	
	}

	$st = array();
    foreach($states as $k=>$v) {
    	$st[] = "'$k'";
    }
    $st = "(". implode(",", $st) .")";

	$results = array();
	$results['RTOQ'] = array('descr' => $states['RTOQ'], 
							'sla' => 'NA',
							'tat' => 'NA',
							'compliance' => 'NA',
							'currorders' => 0
						);	
						
	$slas = _get_sla_time($st);
	foreach($slas as $rec) {
		$results[$rec['nextstate']] = array('descr' => $states[$rec['nextstate']], 
							'sla' => _formatSLATime($rec['sla']),
							'tat' => _formatSLATime(0),
							'compliance' => 0,
							'currorders' => 0
						);	
	}
	
	if(!is_null($start_date) && !is_null($end_date)){
		$tat = _get_sla_turn_around_times($st, $start_date, $end_date);		
		foreach($tat as $rec) {
			$k = $rec['activitycode'];
			$v = floor($rec['tat']);
			if(array_key_exists($k, $results) ) {
				$results[$k]['tat'] = _formatSLATime((int)floor($v));
			}			
		}
	
		$compliance = _get_sla_compliance($st, $start_date, $end_date);
		foreach($compliance as $k=>$v) {
			if (array_key_exists($k, $results)) {
				$results[$k]['compliance'] = $v;  	
			}	
		}
	
		if (is_null($end_date)) {
			date_default_timezone_set('Asia/Calcutta');
	                $end_date = date('Y-m-d 00:00:00');
	                
	        if(is_null($start_date)){
	 				$start_date = date('Y-m-d 00:00:00',mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")));       	
	        }
		}
	
		$currorders = get_old_rt_state_counts($return_type, $start_date, $end_date);
		foreach($currorders as $k=>$v) {
			if (array_key_exists($k, $results) ) {
				$results[$k]['currorders'] = (int)$v; 
			}
		}
	}
	$results['RTOQ']['tat'] = 'NA';
	$results['RTOQ']['compliance'] = 'NA';

	return $results;	
}  

function _get_sla_time($states) {
	global $sqllog;
	$qry = "SELECT nextstate, max(maxtime) AS sla FROM mk_old_returns_tracking_sla WHERE nextstate IN $states GROUP BY nextstate";
	$sqllog->debug("SQL Query : ".$qry." "." @ line ".__LINE__." in file ".__FILE__);
	$data = func_query($qry, true);
	$sqllog->debug($data);	
	return $data;
}

function _get_sla_turn_around_times($states, $start_dt, $end_dt) {
	global $sqllog;
	
	$qry = "SELECT activitycode, SUM(time_since_last_activity)/COUNT(*) AS tat FROM mk_old_returns_tracking_activity ";
	$qry .= " WHERE activitycode IN $states ";
	if(!is_null($start_dt)){
		$qry .= " AND recorddate >= '" . substr($start_dt,0,10). " 00:00:01'";
	}
	if (!is_null($end_dt)) {
		$qry .= " AND recorddate <= '" . substr($end_dt,0,10). " 23:59:59'";
	}
	$qry .= " GROUP BY activitycode";
	$sqllog->debug("SQL Query : ".$qry." "." @ line ".__LINE__." in file ".__FILE__);
	$data = func_query($qry, true);
	$sqllog->debug($data);
	if (!$data)
		return array();
	return $data;
}

function _get_sla_compliance($states, $start_dt, $end_dt) {
	global $sqllog;
	$hits 	= _get_sla_hit_counts($states, $start_dt, $end_dt);
	$misses = _get_sla_miss_counts($states, $start_dt, $end_dt);
	$sqllog->debug($hits);
	$sqllog->debug($misses);
	$data = array();
	foreach($hits as $h) {
		$data[$h['activitycode']] = array('hits' => $h['count'],
							'misses' => 0);	
	}		 			
	foreach($misses as $m) {
		if (!array_key_exists($m['activitycode'], $data)) {
			$data[$m['activitycode']] = array('hits' => 0,
							'misses' => $m['count']); 		
		} else {
			$data[$m['activitycode']]['misses'] = $m['count'];
		}
	}

	$sqllog->debug($data);
	$result = array();
	foreach($data as $k=>$v) {
		$result[$k] = round(($v['hits'] / ($v['hits'] + $v['misses'])) * 100, 2); 	
	}	
	return $result;
}

function _get_sla_hit_counts($states, $start_dt, $end_dt) {
	global $sqllog;

	$qry = "SELECT activitycode, count(*) as count FROM mk_old_returns_tracking_activity WHERE time_since_last_activity <= sla_time ";
	$qry .= " AND activitycode IN $states " ;
	if(!is_null($start_dt)){
                $qry .= " AND recorddate >= '" . substr($start_dt,0,10). " 00:00:01'";
        }
        if (!is_null($end_dt)) {
                $qry .= " AND recorddate <= '" . substr($end_dt,0,10). " 23:59:59'";
        }	
	$qry .= " GROUP BY activitycode" ;
	$sqllog->debug("SQL Query : ".$qry." "." @ line ".__LINE__." in file ".__FILE__);
	$hitdata = func_query($qry, true);
	if (!$hitdata)
		return array();
	return $hitdata;	
}

function _get_sla_miss_counts($states, $start_dt, $end_dt) {
	global $sqllog;

        $qry = "SELECT activitycode, count(*) as count FROM mk_old_returns_tracking_activity WHERE time_since_last_activity > sla_time ";
        $qry .= " AND activitycode IN $states " ;
        if(!is_null($start_dt)){
                $qry .= " AND recorddate >= '" . substr($start_dt,0,10). " 00:00:01'";
        }
        if (!is_null($end_dt)) {
                $qry .= " AND recorddate <= '" . substr($end_dt,0,10). " 23:59:59'";
        }
        $qry .= " GROUP BY activitycode" ;
        $sqllog->debug("SQL Query : ".$qry." "." @ line ".__LINE__." in file ".__FILE__);
        $missdata = func_query($qry, true);
        if (!$missdata)
                return array();
        return $missdata;
	
}

function get_sla_overall_stats_for_rt($dt=null){
	global $sqllog;
	
	$refdate = null;
	if (is_null($dt)){
		date_default_timezone_set('Asia/Calcutta');
        	$refdate = date('Y-m-d 00:00:00');
	} else {
		$refdate = $dt;
	}			

	$refdate_start_1 = date('Y-m-d H:i:s', strtotime('-1 day', strtotime($refdate)));
	$refdate_start_7 = date('Y-m-d H:i:s', strtotime('-7 day', strtotime($refdate)));
	$refdate_start_30= date('Y-m-d H:i:s', strtotime('-30 day', strtotime($refdate)));	

	$results = array();

	$return_states  = _get_old_return_states();
	$return_states['RTR'] = 'Returns Received';
	$return_states['RTQPA'] = 'Refund Approved';
	$return_states['RTQPNA']= 'Refund Declined';
	$return_states['RTAI']  = 'Restocked';
	$return_states['RTQF']  = 'Rejected';
	
	$return_reasons = get_old_return_reasons();
	foreach(array_values($return_reasons) as $reason ) {
		$results[$reason] = array(0,0,0);		
	}
		
	$qry1 = "SELECT A.returnreason as reason, count(*) as count FROM (select * from mk_old_returns_tracking where returntype='RT') AS A INNER JOIN ";
	$qry1 .= "(select * from mk_old_returns_tracking_activity where activitycode='RTQ' AND recorddate< '$refdate' AND recorddate >= '$refdate_start_1') AS B ";
	$qry1 .= " ON (A.id = B.returnid) GROUP BY A.returnreason";
	$data = func_query($qry1, true);	
	foreach($data as $rec){
		if (array_key_exists($rec['reason'], $results)) {
			$results[$rec['reason']][0] = $rec['count'];	
		}		
	}
		
	$qry1_2 = "SELECT A.returnreason as reason, count(*) as count FROM (select * from mk_old_returns_tracking where returntype='RT') AS A INNER JOIN ";
        $qry1_2 .= "(select * from mk_old_returns_tracking_activity where activitycode='RTQ' AND recorddate< '$refdate' AND recorddate >= '$refdate_start_7') AS B ";
        $qry1_2 .= " ON (A.id = B.returnid) GROUP BY A.returnreason";
        $data = func_query($qry1_2, true);
        foreach($data as $rec){
                if (array_key_exists($rec['reason'], $results)) {
                        $results[$rec['reason']][1] = $rec['count'];
                }
        }

	$qry1_3 = "SELECT A.returnreason as reason, count(*) as count FROM (select * from mk_old_returns_tracking where returntype='RT') AS A INNER JOIN ";
        $qry1_3 .= "(select * from mk_old_returns_tracking_activity where activitycode='RTQ' AND recorddate< '$refdate' AND recorddate >= '$refdate_start_30') AS B ";
        $qry1_3 .= " ON (A.id = B.returnid) GROUP BY A.returnreason";
        $data = func_query($qry1_3, true);
        foreach($data as $rec){
                if (array_key_exists($rec['reason'], $results)) {
                        $results[$rec['reason']][2] = $rec['count'];
                }
        }	
	
	$results[' '] = array(' ',' ',' ');
	$results[$return_states['RTR']] = array(0,0,0);
	$results['  '] = array('','','');
	$results[$return_states['RTQPA']]= array(0,0,0);
	$results['Total Refund Amount']	 = array(0,0,0);	
	$results[$return_states['RTQPNA']] = array(0,0,0);
	$results['   '] = array('','','');
	$results[$return_states['RTAI']]= array(0,0,0);
	$results['Total Restock Amount']= array(0,0,0);
	$results['Total Reject Amount'] = array(0,0,0);
	$results[$return_states['RTQF']]= array(0,0,0);
	$qry4 = "select activitycode, count(*) as count from mk_old_returns_tracking_activity where activitycode in ('RTR','RTQPA','RTQPNA','RTAI','RTQF')";
	$qry4 .= " AND recorddate< '$refdate' AND recorddate >= '$refdate_start_1' group by activitycode";					
	$data = func_query($qry4, true);
	foreach($data as $rec) {
		$results[$return_states[$rec['activitycode']]][0] = $rec['count'];					
	}					
	
	
	$qry4_0_1 = "select activitycode, count(*) as count from mk_old_returns_tracking_activity where activitycode in ('RTR','RTQPA','RTQPNA','RTAI','RTQF')";
	$qry4_0_1 .= " AND recorddate< '$refdate' AND recorddate >= '$refdate_start_7' group by activitycode";					
	$data = func_query($qry4_0_1, true);
	foreach($data as $rec) {
		$results[$return_states[$rec['activitycode']]][1] = $rec['count'];					
	}
						
	$qry4_0_2 = "select activitycode, count(*) as count from mk_old_returns_tracking_activity where activitycode in ('RTR','RTQPA','RTQPNA','RTAI','RTQF')";
	$qry4_0_2 .= " AND recorddate< '$refdate' AND recorddate >= '$refdate_start_30' group by activitycode";					
	$data = func_query($qry4_0_2, true);
	foreach($data as $rec) {
		$results[$return_states[$rec['activitycode']]][2] = $rec['count'];					
	}					

	$qry4_1 = "select sum(A.couponvalue) as refund, sum(A.item_val_restocked) as restockvalue, sum(A.item_val_rejected) as rejectvalue from ";
	$qry4_1 .= " ( select * from mk_old_returns_tracking ) as A INNER JOIN ";
	$qry4_1 .= " (select * from mk_old_returns_tracking_activity where activitycode='RTQ' AND ";
	$qry4_1 .= " recorddate< '$refdate' AND recorddate >= '$refdate_start_1') AS B ";
	$qry4_1 .= " ON (A.id = B.returnid)";
	$data = func_query($qry4_1, true);
	foreach($data as $rec) {
		$results['Total Refund Amount'][0] 	= $rec['refund'];
		$results['Total Restock Amount'][0]	= $rec['restockvalue'];
		$results['Total Reject Amount'][0]	= $rec['rejectvalue'];	
	}

	$qry4_2 = "select sum(A.couponvalue) as refund, sum(A.item_val_restocked) as restockvalue, sum(A.item_val_rejected) as rejectvalue from ";
	$qry4_2 .= " ( select * from mk_old_returns_tracking ) as A INNER JOIN ";
	$qry4_2 .= " (select * from mk_old_returns_tracking_activity where activitycode='RTQ' AND ";
	$qry4_2 .= " recorddate< '$refdate' AND recorddate >= '$refdate_start_7') AS B ";
	$qry4_2 .= " ON (A.id = B.returnid)";
	$data = func_query($qry4_2, true);
	foreach($data as $rec) {
		$results['Total Refund Amount'][1] 	= $rec['refund'];
		$results['Total Restock Amount'][1]	= $rec['restockvalue'];
		$results['Total Reject Amount'][1]	= $rec['rejectvalue'];	
	}

	$qry4_3 = "select sum(A.couponvalue) as refund, sum(A.item_val_restocked) as restockvalue, sum(A.item_val_rejected) as rejectvalue from ";
	$qry4_3 .= " ( select * from mk_old_returns_tracking ) as A INNER JOIN ";
	$qry4_3 .= " (select * from mk_old_returns_tracking_activity where activitycode='RTQ' AND ";
	$qry4_3 .= " recorddate< '$refdate' AND recorddate >= '$refdate_start_30') AS B";
	$qry4_3 .= " ON (A.id = B.returnid)";
	$data = func_query($qry4_3, true);
	foreach($data as $rec) {
		$results['Total Refund Amount'][2] 	= $rec['refund'];
		$results['Total Restock Amount'][2]	= $rec['restockvalue'];
		$results['Total Reject Amount'][2]	= $rec['rejectvalue'];	
	}

	$results['Pending (Not in any of the end states)'] = array('','','');
	$results['Pending > 48 hours'] = array(0,0,0);

	$qry5 = "select count(*) as count from (select * from mk_old_returns_tracking where returntype='RT' and currstate != 'END') AS A ";
	$qry5 .= " inner join (select * from mk_old_returns_tracking_activity where activitycode='RTQ' and recorddate < '$refdate' and ";
	$qry5 .= " recorddate >= '$refdate_start_1' and date_add(recorddate, INTERVAL 48 HOUR) < current_timestamp()) AS B on (A.id = B.returnid)";	
	$data = func_query_first($qry5, true);
	$results['Pending > 48 hours'][0] = $data['count'];		

	$qry6 = "select count(*) as count from (select * from mk_old_returns_tracking where returntype='RT' and currstate != 'END') AS A ";
        $qry6 .= " inner join (select * from mk_old_returns_tracking_activity where activitycode='RTQ' and recorddate < '$refdate' and ";
        $qry6 .= " recorddate >= '$refdate_start_7' and date_add(recorddate, INTERVAL 48 HOUR) < current_timestamp()) AS B on (A.id = B.returnid)";
        $data = func_query_first($qry6, true);
        $results['Pending > 48 hours'][1] = $data['count'];	

	$qry7 = "select count(*) as count from (select * from mk_old_returns_tracking where returntype='RT' and currstate != 'END') AS A ";
        $qry7 .= " inner join (select * from mk_old_returns_tracking_activity where activitycode='RTQ' and recorddate < '$refdate' and ";
        $qry7 .= " recorddate >= '$refdate_start_30' and date_add(recorddate, INTERVAL 48 HOUR) < current_timestamp()) AS B on (A.id = B.returnid)";
        $data = func_query_first($qry7, true);
        $results['Pending > 48 hours'][2] = $data['count'];	

	$results['Pending > 7 Days'] = array(0,0,0);
	
	$qry8 = "select count(*) as count from (select * from mk_old_returns_tracking where returntype='RT' and currstate != 'END') AS A ";
        $qry8 .= " inner join (select * from mk_old_returns_tracking_activity where activitycode='RTQ' and recorddate < '$refdate' and ";
        $qry8 .= " recorddate >= '$refdate_start_1' and date_add(recorddate, INTERVAL 168 HOUR) < current_timestamp()) AS B on (A.id = B.returnid)";
        $data = func_query_first($qry8, true);
        $results['Pending > 7 Days'][0] = $data['count'];

	$qry9 = "select count(*) as count from (select * from mk_old_returns_tracking where returntype='RT' and currstate != 'END') AS A ";
        $qry9 .= " inner join (select * from mk_old_returns_tracking_activity where activitycode='RTQ' and recorddate < '$refdate' and ";
        $qry9 .= " recorddate >= '$refdate_start_7'  and date_add(recorddate, INTERVAL 168 HOUR) < current_timestamp()) AS B on (A.id = B.returnid)";
        $data = func_query_first($qry9, true);
        $results['Pending > 7 Days'][1] = $data['count'];

	$qry10 = "select count(*) as count from (select * from mk_old_returns_tracking where returntype='RT' and currstate != 'END') AS A ";
        $qry10 .= " inner join (select * from mk_old_returns_tracking_activity where activitycode='RTQ' and recorddate < '$refdate' and ";
        $qry10 .= " recorddate >= '$refdate_start_30' and date_add(recorddate, INTERVAL 168 HOUR) < current_timestamp()) AS B on (A.id = B.returnid)";
        $data = func_query_first($qry10, true);
        $results['Pending > 7 Days'][2] = $data['count'];		
	
	$sqllog->debug($results);
	return $results;		
}  

function get_sla_overall_stats_for_rto($dt=null){
	global $sqllog;
	
	$results = array();
	
	$results['No. of orders'] = array('','','');
	$results['COD']	= array(0,0,0);
	$results['Non-COD'] = array(0,0,0);	

	$results[' '] = array('','','');

	$rto_states  = _get_old_rto_states();
    $rto_states['RTORSC'] = 'Reshipped';
    $rto_states['RTOC'] = 'Cancelled';
    $rto_states['RTOAI']= 'Restocked';
	
	$results[$rto_states['RTORSC']] = array(0,0,0);
	$results[$rto_states['RTOC']]  = array(0,0,0);
	$results[$rto_states['RTOAI']] = array(0,0,0);	
	$results['  '] = array('','','');
	
	$results['Total Refund Amount']  = array(0,0,0);
    $results['Total Restock Amount']= array(0,0,0);
    $results['Total Reject Amount'] = array(0,0,0);
	
    $refdate = null;
    if (!is_null($dt)){
       $refdate = $dt;
	
		$refdate_start_1 = date('Y-m-d H:i:s', strtotime('-1 day', strtotime($refdate)));
	    $refdate_start_7 = date('Y-m-d H:i:s', strtotime('-7 day', strtotime($refdate)));
	    $refdate_start_30= date('Y-m-d H:i:s', strtotime('-30 day', strtotime($refdate)));
	
	
		$qry1 = "select iscod as cod, couponvalue as refund, item_val_restocked as restockvalue, item_val_rejected as rejectvalue, queued_date as qdate " .
					"from mk_old_returns_tracking  where returntype='RTO' " .
					"and queued_date < '$refdate' and queued_date >= '$refdate_start_30' " .
					"order by queued_date asc";
		$sqllog->debug("SQL Query : ".$qry1." "." @ line ".__LINE__." in file ".__FILE__);
		$data = func_query($qry1, true);
		
		foreach($data as $rec) {
			$temp_date = date('Y-m-d H:i:s', strtotime($rec['qdate']));
			if($temp_date >= $refdate_start_1){
				update_results_array($results, $rec, 0);
			}
			
			if($temp_date >= $refdate_start_7){
				update_results_array($results, $rec, 1);
			}
			
		    update_results_array($results, $rec, 2); 
	    }
	
		$qry2 = "select activitycode as activitycode, recorddate as rdate from mk_old_returns_tracking_activity where activitycode in ('RTORSC','RTOC','RTOAI') ".
			 " AND recorddate< '$refdate' AND recorddate >= '$refdate_start_30' order by recorddate asc";	
		$sqllog->debug("SQL Query : ".$qry2." "." @ line ".__LINE__." in file ".__FILE__);
		$data = func_query($qry2, true);
	    foreach($data as $rec) {
	    	$temp_date = date('Y-m-d H:i:s', strtotime($rec['rdate']));
	    	if($temp_date >= $refdate_start_1){
				 $results[$rto_states[$rec['activitycode']]][0] +=1;
			}
			
			if($temp_date >= $refdate_start_7){
				$results[$rto_states[$rec['activitycode']]][1] +=1;
			}
			
	        $results[$rto_states[$rec['activitycode']]][2] +=1;
	    }	
    }
	return $results;
} 

function update_results_array(&$results, $rec, $index){
	global $sqllog;
	if($rec['cod'] == 0){
            $results['Non-COD'][$index] +=1;
    } else {
            $results['COD'][$index] +=1;
    }
    
    $results['Total Refund Amount'][$index]      +=$rec['refund'];
    $results['Total Restock Amount'][$index]     +=$rec['restockvalue'];
    $results['Total Reject Amount'][$index]      +=$rec['rejectvalue'];
}

function getSLAComplianceStatsForUD($start_date, $end_date){
	global $sqllog;
	$states = _get_old_ud_states();
	$st = array();
        foreach($states as $k=>$v) {
                $st[] = "'$k'";
        }
        $st = "(". implode(",", $st) .")";

	$slas = _get_sla_time($st);
	foreach($slas as $rec) {
                $results[$rec['nextstate']] = array('descr' => $states[$rec['nextstate']],
                                                        'sla' => _formatSLATime($rec['sla']),
                                                        'tat' => _formatSLATime(0),
                                                        'compliance' => 0,
                                                        'currorders' => 0
                                                );
        }        
	$sqllog->debug($results);
	
	if(!is_null($start_date) && !is_null($end_date)){
		$tat = _get_sla_turn_around_times_for_ud($st, $start_date, $end_date);
	        foreach($tat as $rec) {
	                $k = $rec['activitycode'];
	                $v = floor($rec['tat']);
	                if(array_key_exists($k, $results) ) {
	                        $results[$k]['tat'] = _formatSLATime((int)floor($v));
	                }
	        }
		$compliance = _get_sla_compliance_for_ud($st, $start_date, $end_date);
	        foreach($compliance as $k=>$v) {
	                if (array_key_exists($k, $results)) {
	                        $results[$k]['compliance'] = $v;
	                }
	        }
		
		
		if (is_null($end_date)) {
	                date_default_timezone_set('Asia/Calcutta');
	                $end_date = date('Y-m-d 00:00:00');
	        }
	
		$currorders =  get_ud_state_counts($start_date, $end_date);
	        foreach($currorders as $k=>$v) {
	                if ( array_key_exists($k, $results) ) {
	                        $results[$k]['currorders'] = (int)$v;
	                }
	        }	
	}
	return $results;	
				
}

function _get_sla_turn_around_times_for_ud($states, $start_dt, $end_dt) {
        global $sqllog;

        $qry = "SELECT activitycode, SUM(time_since_last_activity)/COUNT(*) AS tat FROM mk_old_returns_tracking_ud_activity ";
        $qry .= " WHERE activitycode IN $states ";
        if(!is_null($start_dt)){
                $qry .= " AND recorddate >= '" . substr($start_dt,0,10). " 00:00:01'";
        }
        if (!is_null($end_dt)) {
                $qry .= " AND recorddate <= '" . substr($end_dt,0,10). " 23:59:59'";
        }
        $qry .= " GROUP BY activitycode";
        $sqllog->debug("SQL Query : ".$qry." "." @ line ".__LINE__." in file ".__FILE__);
        $data = func_query($qry, true);
        $sqllog->debug($data);
        if (!$data)
                return array();
        return $data;
}

function _get_sla_compliance_for_ud($states, $start_dt, $end_dt) {
        global $sqllog;
        $hits   = _get_sla_hit_counts_for_ud($states, $start_dt, $end_dt);
        $misses = _get_sla_miss_counts_for_ud($states, $start_dt, $end_dt);
        $sqllog->debug($hits);
        $sqllog->debug($misses);
        $data = array();
        foreach($hits as $h) {
                $data[$h['activitycode']] = array('hits' => $h['count'],
                                                        'misses' => 0);
        }
        foreach($misses as $m) {
                if (!array_key_exists($m['activitycode'], $data)) {
                        $data[$m['activitycode']] = array('hits' => 0,
                                                        'misses' => $m['count']);
                } else {
                        $data[$m['activitycode']]['misses'] = $m['count'];
                }
        }

        $sqllog->debug($data);
        $result = array();
        foreach($data as $k=>$v) {
                $result[$k] = ($v['hits'] / ($v['hits'] + $v['misses'])) * 100;
        }
        return $result;
}

function _get_sla_hit_counts_for_ud($states, $start_dt, $end_dt) {
        global $sqllog;

        $qry = "SELECT activitycode, count(*) as count FROM mk_old_returns_tracking_ud_activity WHERE time_since_last_activity <= sla_time ";
        $qry .= " AND activitycode IN $states " ;
        if(!is_null($start_dt)){
                $qry .= " AND recorddate >= '" . substr($start_dt,0,10). " 00:00:01'";
        }
        if (!is_null($end_dt)) {
                $qry .= " AND recorddate <= '" . substr($end_dt,0,10). " 23:59:59'";
        }
        $qry .= " GROUP BY activitycode" ;
        $sqllog->debug("SQL Query : ".$qry." "." @ line ".__LINE__." in file ".__FILE__);
        $hitdata = func_query($qry, true);
        if (!$hitdata)
                return array();
        return $hitdata;
}

function _get_sla_miss_counts_for_ud($states, $start_dt, $end_dt) {
        global $sqllog;

        $qry = "SELECT activitycode, count(*) as count FROM mk_old_returns_tracking_ud_activity WHERE time_since_last_activity > sla_time ";
        $qry .= " AND activitycode IN $states " ;
        if(!is_null($start_dt)){
                $qry .= " AND recorddate >= '" . substr($start_dt,0,10). " 00:00:01'";
        }
        if (!is_null($end_dt)) {
                $qry .= " AND recorddate <= '" . substr($end_dt,0,10). " 23:59:59'";
        }
        $qry .= " GROUP BY activitycode" ;
        $sqllog->debug("SQL Query : ".$qry." "." @ line ".__LINE__." in file ".__FILE__);
        $missdata = func_query($qry, true);
        if (!$missdata)
                return array();
        return $missdata;

}

function get_ud_state_counts($start_date=null, $end_date=null) {
        global $sqllog;
        
        $qry = "select A.currstate, count(*) AS count
				from mk_old_returns_tracking_ud A, 
				mk_old_returns_tracking_ud_activity B
				where A.id = B.udid 
				AND A.currstate = B.activitycode"; 
		if(!is_null($start_date)) {
        	$qry .= " AND recorddate >= '" . substr($start_date,0,10)." 00:00:01'";
        }
        
        if (!is_null($end_date)){
        	$qry .= " AND recorddate <= '" . substr($end_date,0,10)." 23:59:59'";
        }
        
        $qry.= "GROUP BY A.currstate";

        $sqllog->debug("SQL Query : ".$qry." "." @ line ".__LINE__." in file ".__FILE__);
        $data = func_query($qry, true);
        $result = array();
        if ($data) {
                foreach($data as $rec) {
                        $result[$rec['currstate']] = $rec['count'];
                }
        }
        $sqllog->debug($result);
        return $result;
}

function get_sla_overall_stats_for_ud($dt=null) {
	global $sqllog;

	$results = array();
	
	$results['No. of orders'] = array('','','');
	$results['COD']	= array(0,0,0);
	$results['Non-COD'] = array(0,0,0);	

	$results[' '] = array('','','');

	$results['Reshipped'] = array(0,0,0);
	$results['Cancelled'] = array(0,0,0);
    $refdate = null;
    if (!is_null($dt)){
        $refdate = $dt;
           
		$refdate_start_1 = date('Y-m-d H:i:s',strtotime('-1 day', strtotime($refdate)));
	    $refdate_start_7 = date('Y-m-d H:i:s', strtotime('-7 day', strtotime($refdate)));
	    $refdate_start_30= date('Y-m-d H:i:s', strtotime('-30 day', strtotime($refdate)));
		$sql = "select SUM(IF((iscod =1 and queued_date >= '$refdate_start_1'), 1, 0)) cod_yesterday,
				SUM(IF((iscod =1 and queued_date >= '$refdate_start_7'), 1, 0)) cod_week,
				SUM(IF((iscod =1 and queued_date >= '$refdate_start_30'), 1, 0)) cod_month,
				SUM(IF((iscod =0 and queued_date >= '$refdate_start_1'), 1, 0)) nncod_yesterday,
				SUM(IF((iscod =0 and queued_date >= '$refdate_start_7'), 1, 0)) nncod_week,
				SUM(IF((iscod =0 and queued_date >= '$refdate_start_30'), 1, 0)) nncod_month
				from mk_old_returns_tracking_ud  
				where queued_date between '$refdate_start_30' and '$refdate'";
		$sqllog->debug($sql);
	    $data = func_query_first($sql, true);
	    $results['COD'][0] = $data['cod_yesterday'];
	    $results['Non-COD'] [0] = $data['nncod_yesterday'];
        
        $results['COD'][1] = $data['cod_week']; 
        $results['Non-COD'] [1] = $data['nncod_week'];
        
        $results['COD'][2] = $data['cod_month'];
        $results['Non-COD'] [2] = $data['nncod_month'];


		$sqlresoluton = "select SUM(IF((cc_resolution ='RJ' and recorddate >= '$refdate_start_1'), 1, 0)) rj_yesterday,
				SUM(IF((cc_resolution ='RJ' and recorddate >= '$refdate_start_7'), 1, 0)) rj_week,
				SUM(IF((cc_resolution ='RJ' and recorddate >= '$refdate_start_30'), 1, 0)) rj_month,
				SUM(IF((cc_resolution ='RA' and recorddate >= '$refdate_start_1'), 1, 0)) ra_yesterday,
				SUM(IF((cc_resolution ='RA' and recorddate >= '$refdate_start_7'), 1, 0)) ra_week,
				SUM(IF((cc_resolution ='RA' and recorddate >= '$refdate_start_30'), 1, 0)) ra_month
				 from mk_old_returns_tracking_ud_activity
				where recorddate between '$refdate_start_30' and '$refdate'";
				
		$sqllog->debug($sqlresoluton);
	    $data = func_query_first($sqlresoluton, true);
	    
   		$results['Cancelled'][0] = $data['rj_yesterday'];	
		$results['Reshipped'][0] = $data['ra_yesterday'];
		
		$results['Cancelled'][1] = $data['rj_week'];	
		$results['Reshipped'][1] = $data['ra_week'];
		
		$results['Cancelled'][2] = $data['rj_month'];	
		$results['Reshipped'][2] = $data['ra_month'];
    }		
	$sqllog->debug($results);
	return $results;
}

/**
** Mailer functions : sends mails on state transitions 
** A mail gets sent to  all the departments responsible for performing the next activity
**/

function sendStateChangeMail($orderid, $prev_state, $next_state, $tracking_type) {
	global $sqllog;
	global $weblog;
	$cc_alert_email_alias = WidgetKeyValuePairs::getWidgetValueForKey('return-tracking-cc-emails');
	$ops_alert_email_alias = WidgetKeyValuePairs::getWidgetValueForKey('return-tracking-ops-emails');
	$depts = array();
	$following_states = null;
	$stateData = array();
	$states = null;
	$perms = null;
	if(strcasecmp($tracking_type,'UD') == 0) {
		$following_states = get_ud_next_states($orderid);	
		$states = _get_old_ud_states();
        	$perms  = _get_old_ud_all_user_perms();	
	} else if (strcasecmp($tracking_type,'RT') == 0) {
		
        	$states = _get_old_return_states();
        	$perms  = _get_old_return_all_user_perms();
		$following_states = get_old_return_next_states($orderid);   
	} else if (strcasecmp($tracking_type,'RTO') == 0) {
		$following_states = get_old_rto_next_states($orderid);
		$states = _get_old_rto_states();
		$perms  = _get_old_rto_all_user_perms();
	} else {
		$sqllog->info("Unknown tracking type: " . $tracking_type);
		return;
	}
			
	foreach($perms as $k=>$v) {
                if ($k == 'CC') {
                        foreach($v as $item) {
                                $stateData[$item] = 'cc';
                        }

                } else if ($k == 'LG' || $k == 'QA' || $k == 'IN') {
                        foreach($v as $item) {
                                $stateData[$item] = 'op';
                        }
                }
        }		
	$sqllog->debug('===');
	$sqllog->debug($following_states);
	
	foreach($following_states as $nextstate=>$nextstate_descr) {
		if(!is_null($stateData[$nextstate]) && ! array_key_exists($stateData[$nextstate] ,$depts)) {
			$depts[] = $stateData[$nextstate];	
		}		
	}			

	$sqllog->debug($depts);
	
	$url = getDetailUrl($tracking_type, $orderid);
	$content = "Hi,<br/><br/>The order shipment#{$orderid} has been moved from {$states[$prev_state]} state to {$states[$next_state]} state. " .
			"Click <a href=\"{$url}\">here</a> to view the order entry on the {$tracking_type} tracker.<br/><br/>Regards,<br/>Myntra Admin";
	$weblog->debug($content);						
	foreach ($depts as $dept) {
		$to = null;
		if ($dept == 'cc') {
			$to = $cc_alert_email_alias;
		} else {
			$to = $ops_alert_email_alias;
		}
		$weblog->debug("Sending State Change Email To: " . $to);
		$mail_detail = array(
                        "to"=>$to,
                        "subject"=>"State Change for order : $orderid",
                        "content"=>$content,
                        "from_name"=>'Myntra Admin',
                        "from_email"=>'admin@myntra.com',
                        "bcc"=>'myntramailarchive@gmail.com',
                        "header"=> "Content-Type: text/html; charset=ISO-8859-1 \n",
                    );
                send_mail_on_domain_check($mail_detail);	
	}	
}

function getDetailUrl($trackingType, $orderid) {
		global $http_location;
        if (strcasecmp($trackingType,'RT') == 0) {
                return "{$http_location}/admin/old_returns_tracking/old_rt_tracking.php?order_id=$orderid";
        } else if (strcasecmp($trackingType,'RTO') == 0) {
                return "{$http_location}/admin/old_returns_tracking/old_rto_tracking.php?order_id=$orderid";
        } else if (strcasecmp($trackingType,'UD') == 0) {
                return "{$http_location}/admin/old_returns_tracking/old_ud_tracking.php?order_id=$orderid";
        }
        return '';
}

/*function validate_return_itembarcodes($itembarcodes, $orderid) {
	global $xcart_dir;
	include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");

	$issuedItems = ItemApiClient::get_issued_itembarcodes_for_order($orderid);
	
	$itembarcodes = explode("\n", $itembarcodes);
	foreach ($itembarcodes as $barcode) {
		if(!in_array(trim($barcode), $issuedItems)) {
			return false;
		}
	}
	return true;
}*/

function rto_ud_cancelled_order_update_items($orderid, $login, $customerReject, $comment) {
	$cancel_args = array(
		'cancellation_type' 			=> 'CCC',
		'reason'						=> $customerReject==true?"RTOCR":"RTO",
		'orderid2fullcancelordermap'	=> array($orderid=>false),
		'generateGoodWillCoupons'		=> false,
		'orderid2couponDetails'			=> false
	);
	func_change_order_status($orderid, 'F', $login, "Comments from cancellation in RTO tracker -- ".$comment, "", false, '', $cancel_args);
}

function return_qapass_update_items($orderid, $warehouseid, $itemBarcodes, $quality, $login) {
	global $xcart_dir;
	include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");
	$itemBarcodes = explode("\n", $itemBarcodes);
	//$itemBarCode2ItemId = ItemApiClient::get_itemids_for_barcodes($warehouseid, $itemBarcodes);
	$status = ItemApiClient::remove_item_order_association($orderid, $warehouseid, $itemBarCodes, 'CUSTOMER_RETURNED', $quality, $login);
	return $status;
}

function searchUDs($request,$fromDownload = false){
	global $weblog;
	$weblog->info("Inside search UDs...");
	$weblog->info($request);
	if($fromDownload){
		$sql = "select A.id,A.orderid,A.courier,A.trackingno,A.reason,A.currstate,A.resolution,A.attemptcount,A.custname,A.phonenumber,B.recorddate,B.remark,B.cc_disposition,B.cc_resolution,from_unixtime(o.shippeddate,'%Y-%m-%d %H:%i:%s') as shippeddate,(case when A.currstate in ('UDCC','UDLS','END') then ( unix_timestamp(B.recorddate) - unix_timestamp(A.queued_date))/3600 else (unix_timestamp(now()) - unix_timestamp(A.queued_date))/3600 end) as age,concat(A.pickupaddress,A.pickupcity,A.pickupstate,A.pickupcountry,'  ',A.pickupzipcode) as address,(case when iscod = 1 then 'cod' else 'on' end) as paymentMethod,A.pickupcity,A.pickupzipcode";
	}else{
		$sql = "select SQL_CALC_FOUND_ROWS A.*,B.recorddate,B.remark,B.cc_disposition,B.cc_resolution,from_unixtime(o.shippeddate,'%Y-%m-%d %H:%i:%s') as shippeddate,(case when A.currstate in ('UDCC','UDLS','END') then ( unix_timestamp(B.recorddate) - unix_timestamp(A.queued_date))/3600 else (unix_timestamp(now()) - unix_timestamp(A.queued_date))/3600 end) as age,concat(A.pickupaddress,A.pickupcity,A.pickupstate,A.pickupcountry,'<br/>',A.pickupzipcode) as address,(case when iscod = 1 then 'cod' else 'on' end) as paymentMethod";	
	}
	
	
	$no_params = true;
	
	if(!empty($request['orderid'])) {
		$no_params = false;
		$sql .= " from mk_old_returns_tracking_ud A left outer join mk_old_returns_tracking_ud_activity B on (A.id = B.udid and A.currstate = B.activitycode) join xcart_orders o where o.orderid = A.orderid and A.orderid = ".$request['orderid'];
	}else{
		$sql .= " from mk_old_returns_tracking_ud A join xcart_orders o on (A.orderid = o.orderid) ";
		if(!empty($request['datetype'])) {
			$no_params = false;
			$params = array();
			$params[] = "activitycode= '".$request['datetype']."'";
			if (!empty($request['fromdate'])) {
				$params[] = "recorddate >= '". str_replace("T"," ", $request['fromdate']) ."'";
			}
			if (!empty($request['todate'])) {
				$params[] = "recorddate <= '". substr($request['todate'],0,10) . " 23:59:59'";
			}

			if(!empty($request['returnstatus'])) {
				$no_params = false;
				$cond_params[] = "currstate IN ('". str_replace(",", "','", $request['returnstatus'])  ."')";	
			}
				
			if(!empty($request['ccresolution'])) {
				$no_params = false;
				$cond_params[] = "resolution = '". $request['ccresolution'] ."'";
			}
		
			if(!empty($request['attempt'])) {
				$no_params = false;
				$cond_params[] = "attemptcount = ". $request['attempt'];
			}
			
			$sql .= " INNER JOIN (select * from mk_old_returns_tracking_ud_activity ";
			$sql .= " WHERE ".implode(" AND ", $params);
			$sql .= " ) AS B ON (A.id = B.udid)";
			
			if(count($cond_params) > 0) {
		                $sql .= " WHERE ".implode(" AND ", $cond_params);
		    }		
		}else{
			$sql .= " left outer join mk_old_returns_tracking_ud_activity B on (A.id = B.udid and A.currstate = B.activitycode) ";			
			if(!empty($request['returnstatus'])) {
				$no_params = false;
				$cond_params[] = "currstate IN ('". str_replace(",", "','", $request['returnstatus'])  ."')";	
			}
				
			if(!empty($request['ccresolution'])) {
				$no_params = false;
				$cond_params[] = "resolution = '". $request['ccresolution'] ."'";
			}
		
			if(!empty($request['attempt'])) {
				$no_params = false;
				$cond_params[] = "attemptcount = ". $request['attempt'];
			}
			if(count($cond_params) > 0) {
		        $sql .= " WHERE ".implode(" AND ", $cond_params);
		    }	
		}	
	}
	
	if($no_params){
		$sql .= " where A.currstate = 'UDQ'";
	}
		
	if(!empty($request["sort"])) {
                $sql .= " ORDER BY " . $request["sort"];
                $sql .= " " . ($request["dir"] == 'DESC' ? "DESC" : "ASC");
    }
	
	if(!empty($request["paginate"]) && $request["paginate"]=="true") {
		$start = $request['start'] == null ? 0 : $request['start'];
		$limit = $request['limit'] == null ? 30 : $request['limit'];
		$sql .=" LIMIT $start, $limit";
	}
	
	$weblog->debug("SQL Query : ".$sql." "." @ line ".__LINE__." in file ".__FILE__);
	$results = func_query($sql, TRUE);
	
	$return_requests = format_ud_data($results); 
	return $return_requests;
}

function searchRTOs($request){
	global $weblog;
	global $sqllog;
	global $errorlog;
	
	$weblog->info($request);
	
	$returnTypeFltr = null;
	if(!empty($request['returntype'])) {
        $returnTypeFltr = "returntype = '".$request['returntype']."'";
    }else{
    	$errorlog->fatal("No return typle found in search RTOs. Can't execute search without return type");
    	return;
    }
	
	$cond_params = array();
	
	if(!empty($request['orderid'])) {
		$cond_params[] = "orderid = ".$request['orderid'];
	}	
	
	if(!empty($request['returnstatus'])) {
		$cond_params[] = "currstate IN ('". str_replace(",", "','", $request['returnstatus'])  ."')";	
	}
	
	if(!empty($request['warehouseid'])) {
		$cond_params[] = "A.warehouseid = ".$request['warehouseid'];	
	}
	
	$newsql = "select SQL_CALC_FOUND_ROWS A.id, orderid, IF(iscod = 1, 'COD', 'ONLINE') AS paymentMethod, 
		courier_service, trackingno, returnreason, currstate, queued_date, custname, 
		CONCAT(pickupaddress, ',',pickupcity, ',',pickupstate, ',',pickupcountry, ', PIN-', pickupzipcode) address,pickupcity,pickupzipcode,warehouseid";
		
	if(!empty($request['datetype'])) {
		$newsql .= " ,B.recorddate as activityDate";
	}	 
		
	$newsql .= " from mk_courier_service mcs right join mk_old_returns_tracking A on (mcs.code = A.courier)";
		
	if(!empty($request['datetype'])) {
		$newsql .= ",mk_old_returns_tracking_activity B WHERE A.id = B.returnid AND " . $returnTypeFltr;
	}else{
		$newsql .= "WHERE " . $returnTypeFltr;
	}
	
	
	if(count($cond_params) > 0) {
		$newsql .= ' AND ' . implode(" AND ", $cond_params);
	}
	 
	if(!empty($request['datetype'])) {
		$params = array();
		$params[] = "activitycode= '".$request['datetype']."'";
		if (!empty($request['fromdate'])) {
			$params[] = "recorddate >= '". str_replace("T"," ", $request['fromdate']) ."'";
		}
		if (!empty($request['todate'])) {
			$params[] = "recorddate <= '". substr($request['todate'],0,10) . " 23:59:59'";
		}									
		
		$newsql .= ' AND ' . implode(" AND ", $params);
	}			
	
	if(!empty($request["sort"])) {
        $newsql .= " ORDER BY A." . $request["sort"];
        $newsql .= " " . ($request["dir"] == 'DESC' ? "DESC" : "ASC");
    }
    
	if(!empty($request["paginate"]) && $request["paginate"]=="true") {
		$start = $request['start'] == null ? 0 : $request['start'];
		$limit = $request['limit'] == null ? 30 : $request['limit'];
		$newsql .=" LIMIT $start, $limit";
	}
	
	$sqllog->info("oldreturnsajax.php: Execute Query ". $newsql);
	
	$results = func_query($newsql, TRUE);
	
	$return_requests = format_return_data($results);
	
	return $return_requests;					
}

function validateOrdersForRTOCancellation($orderids) {
	$validRTOSql = "select orderid, queued_date, currstate from mk_old_returns_tracking where orderid in (".implode(",", $orderids).")";
	$validRTOOrders = func_query($validRTOSql,true);
	if($validRTOOrders) {
		$rtoAgeThreshold = trim(WidgetKeyValuePairs::getWidgetValueForKey("rto.cancelbulk.age.threshold"));
		$rtoAgeThreshold = empty($rtoAgeThreshold)?7:intval($rtoAgeThreshold);
		$validRTOs = array(); $notTooOldRTOs = array();
		$minAgingDate = date('Y-m-d H:i:s',strtotime("-$rtoAgeThreshold days"));
		foreach($validRTOOrders as $rto) {
			if($rto['queued_date'] > $minAgingDate) {
				$notTooOldRTOs[] = $rto['orderid'];
			} else if($rto['currstate'] != 'RTOQ' && $rto['currstate'] != 'RTOPREQ' && $rto['currstate'] != 'RTOCAL1'
				&& $rto['currstate'] != 'RTOCAL2') {
				$invalidStatusRTOs[] = $rto['orderid'];
			} else {
				$validRTOs[] = $rto['orderid'];
			}
		}
		
		$invalidRTOs = array_diff($orderids, $notTooOldRTOs, $validRTOs);
		if(!empty($invalidRTOs)) {
			return array('status'=>"FAILURE", 'invalid_rtos'=>$invalidRTOs);
		}
		if(!empty($notTooOldRTOs)) {
			return array('status'=>"FAILURE", 'not_old_rtos'=>$notTooOldRTOs);
		}
		if(!empty($invalidStatusRTOs)) {
			return array('status'=>"FAILURE", 'invalid_status_rtos'=>$invalidStatusRTOs);
		}
	} else {
		return array('status'=>"FAILURE", 'invalid_rtos'=>$orderids);
	}
	
	$validOrderStatusSql = "Select orderid from xcart_orders where orderid in (".implode(",", $orderids).") and status in ('SH', 'DL', 'C')";
	$validOrders = func_query($validOrderStatusSql,true);
	if($validOrders) {
		$invalidOrders = array_diff($orderids, $validRTOs);
		if(!empty($invalidOrders)) {
			return array('status'=>"FAILURE", 'invalid_status_orders'=>$invalidOrders);
		}
	} else {
		return array('status'=>"FAILURE", 'invalid_status_orders'=>$orderids);
	} 
	
	return array('status'=>"SUCCESS");
}

function markRTOsCancelled($orderIds, $login, $comment, $progressKey) {
	
	$data = array();
	$data['activitycode']   = 'RTOC';
	$data['activity']       = 'RTO Cancelled';
	$data['remark']         = 'Cancelling RTO in bulk';
	$data['adminuser']      = $login;
	$data['rtocancelreason']= 'Rejecting';
	$data['currstate']      = 'RTOC';
	
	$validRTOSql = "select orderid, queued_date, currstate from mk_old_returns_tracking where orderid in (".implode(",", $orderids).")";
	$xcache = new XCache();
	if($progressKey) {
		$progressinfo = array('total'=>count($orderIds), 'completed'=>0);
    	$xcache->store($progressKey."_progressinfo", $progressinfo, 86400);
	}
	
	foreach ($orderIds as $index=>$orderid){
		if($progressKey) {
        	$progressInfo = $xcache->fetch($progressKey."_progressinfo");
        	$progressInfo['completed'] = $index+1;
        	$xcache->store($progressKey."_progressinfo", $progressInfo, 86400);
		}
        updateReturnsInfo('RTO', $orderid, 'RTOQ', 'RTOC', $data, '', '');
	}
}


?>
