<?php

if ( !defined('XCART_START') ) { header("Location: ../"); die("Access denied"); }

require_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
require_once "$xcart_dir/include/func/func.mail.php";
include_once($xcart_dir."/include/func/func.sms_alerts.php");
include_once("$xcart_dir/include/func/func.courier.php");
include_once("$xcart_dir/include/class/class.mymyntra.php");
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");
include_once($xcart_dir."/include/class/class.mail.multiprovidermail.php");
include_once("$xcart_dir/modules/apiclient/SkuApiClient.php");
include_once("$xcart_dir/modules/apiclient/ShipmentApiClient.php");
include_once($xcart_dir."/include/class/oms/ReturnsStatusProcessor.php");
include_once($xcart_dir."/include/class/oms/ItemManager.php");
include_once("$xcart_dir/env/Host.config.php");
include_once("$xcart_dir/modules/apiclient/WarehouseApiClient.php");
include_once $xcart_dir."/include/func/func.mk_old_returns_tracking.php";

include_once "$xcart_dir/modules/myntCash/MyntCashTransaction.php";
include_once "$xcart_dir/modules/myntCash/MyntCashService.php";
include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalClient";
include_once($xcart_dir."/modules/apiclient/RefundCrmApiClient.php");

use feedback\instance\MFBInstance;
use feedback\manage\MFB;

x_load('db','order');

function func_calculate_refund_amount($itemid, $quantity, $returnMode) {
	global $sql_tbl;

	$item = func_query_first("Select * from $sql_tbl[order_details] where itemid = $itemid");
	$itemprice = $item['price'];
	$unitDiscount = $item['discount']/$item['amount'] + $item['cart_discount_split_on_ratio']/$item['amount'];
	$unitpgdiscount = $item['pg_discount']/$item['amount'];
	$unitCouponDiscount  = $item['coupon_discount_product']/$item['amount'];
	$unitCashback = $item['cashback']/$item['amount'];
	$unitdifferenceRefund = $item['difference_refund']/$item['amount'];
	$unitVatAmount = $item['taxamount']/$item['amount'];
	$unitCashbackRedeemed = $item['cash_redeemed']/$item['amount'];

	$pickupCharges = WidgetKeyValuePairs::getWidgetValueForKey('returnPickupCharges');
	$selfDeliveryCredit = WidgetKeyValuePairs::getWidgetValueForKey('returnSelfDeliveryCredit');

	$refundAmount = ($itemprice - $unitDiscount - $unitCouponDiscount - $unitpgdiscount - $unitCashback - $unitdifferenceRefund + $unitVatAmount)*$quantity;

	if($returnMode == 'pickup') {
		$refundAmount -= $pickupCharges;
	}

	$refundAmount = $refundAmount<0 ? 0:$refundAmount;

	if(!empty($unitCashbackRedeemed) && $unitCashbackRedeemed>0){
		return array('refundAmount'=>$refundAmount+0.00,'cashbackRedeemed'=>($unitCashbackRedeemed*$quantity)+0.00);
	} else {
		return array('refundAmount'=>$refundAmount+0.00,'cashbackRedeemed'=>0.00);
	}
}


function func_get_item_returnId($itemid) {
	global $sql_tbl;
	$sql = "SELECT returnid from $sql_tbl[returns] where itemid = $itemid";
	$row = func_query_first($sql);
	if($row)
		return $row['returnid'];

	return false;
}

function get_all_return_statuses() {
	global $sql_tbl;
	$sql = "SELECT code, display_name from $sql_tbl[return_statuses] order by display_order";
   	$results = func_query($sql);
	return $results;
}

function getReturnStatusByCode($statusCode) {
	global $sql_tbl;
	$sql = "SELECT display_name from $sql_tbl[return_statuses] where code = '$statusCode'";
	$result = func_query_first($sql);
	return $result['display_name'];
}

function getMyMyntraStatusByCode($statusCode) {
	global $sql_tbl;
	$sql = "SELECT mymyntra_status from $sql_tbl[return_statuses] where code = '$statusCode'";
	$result = func_query_first($sql);
	return $result['mymyntra_status'];
}


function get_all_transitions_for_status($current_status) {
	$query = "SELECT to_state from mk_return_state_transitions where from_state = '$current_status'";
	$return_states = func_query($query,true);
	$ret = array();
	foreach ($return_states as $row){
		$ret[] = $row['to_state'];
	}
	return $ret;
}

function get_allowed_status_transitions($current_status, $username , $adminOnly=true) {
	$roleQuery="select mr.type as role,mr.allowed_actions as actions from mk_roles mr,mk_user_roles mur where mur.roleid=mr.id and login = '$username'";
	$roleresult=db_query($roleQuery);
	$roles=array();
	if ($roleresult){
		$i=0;
		while ($row=db_fetch_array($roleresult)){
			$roles[$i] = $row;
			$i++;
		}
	}
	foreach($roles as $role) {
		$userRoles[] = $role['role'];
	}

	$state_details_sql = "SELECT code, display_name from mk_return_status_details where code = '$current_status'";
	$results = func_query_first($state_details_sql);

	$transitions = array($current_status=>$results['display_name']);

	if($adminOnly)
		$query = "SELECT to_state,roles_allowed from mk_return_state_transitions where from_state = '$current_status' and admin_access = 1";
	else
		$query = "SELECT to_state,roles_allowed from mk_return_state_transitions where from_state = '$current_status' and admin_access = 0";

	$results = func_query($query);

	foreach ($results as $index => $transition){
		$roleAllowedForPage = explode(",",$transition['roles_allowed']);
		if(count(array_intersect($userRoles, $roleAllowedForPage)) == 0){
			unset($results[$index]);
		}
	}

	$allowed_transition_states = array();
	foreach ($results as $row) {
		$allowed_transition_states[] = $row['to_state'];
	}

	if(count($allowed_transition_states) > 0) {
		$cond = "";
		foreach ($allowed_transition_states as $state) {
			$cond .= $cond==""?"":",";
			$cond .= "'".trim($state)."'";
		}
		$allowed_transitions_states_sql = "SELECT code, display_name from mk_return_status_details where code in ($cond)";
		$allowed_transition_states = func_query($allowed_transitions_states_sql);
		foreach ($allowed_transition_states as $row) {
			$transitions[$row['code']] = $row['display_name'];
		}
	}

	return $transitions;
}

function get_all_return_reasons($customer_visible='') {
	global $sql_tbl;
	$return_reasons_sql = "SELECT code, displayname FROM $sql_tbl[return_reasons]";
	if($customer_visible){
		$return_reasons_sql.=" where customer_visible = 1";
	}

	if(FeatureGateKeyValuePairs::getBoolean('return.reason.rand.enabled',false) === true){
		$return_reasons_sql.= " order by rand()";
	} else {
		$return_reasons_sql.= " order by display_order";
	}

	$return_reasons = func_query($return_reasons_sql);
	return $return_reasons;
}

function get_all_return_reasons_after_30days() {
	global $sql_tbl;
	$return_reasons_sql = "SELECT code, displayname FROM $sql_tbl[return_reasons] where code in ('PMD', 'PMDU') order by display_order";
	$return_reasons = func_query($return_reasons_sql);
	return $return_reasons;
}

function get_reason_displayname($reasoncode) {
	global $sql_tbl;
	$return_reason_sql = "SELECT displayname FROM $sql_tbl[return_reasons] where code = '$reasoncode'";
	$return_reason_row = func_query_first($return_reason_sql);
	return $return_reason_row['displayname'];
}

function getMyMyntraStatusMsgForReturn($status, $args) {
	global $sql_tbl;

   	if($status == 'RRC' || $status == 'RPI' || $status == 'RPU' || $status == 'RDU') {
   		$courier_service = $args["COURIER_SERVICE"];
   		if($courier_service == 'ML') {
   			$args['COURIER_INFO'] = "Myntra Logistics";
   		} elseif($courier_service == 'User'){
   			$args['COURIER_INFO'] = "Self Delivery";
   		} else {
   			$args['COURIER_INFO'] = get_courier_display_name($courier_service);
   		}
   	}

	if($status == 'RRS' || $status == 'RSD') {
   		$courier_service = $args["RESHIP_COURIER"];
   		if($courier_service == 'ML') {
   			$args['RESHIP_COURIER_INFO'] = "Myntra Logistics";
   		} else {
   			$args['RESHIP_COURIER_INFO'] = get_courier_display_name($courier_service);
   		}
   	}

   	$sql = "SELECT display_name, mymyntra_msg from $sql_tbl[return_statuses] where code = '$status'";
   	$results = func_query_first($sql);
   	$mymyntra_msg = $results['mymyntra_msg'];
   	foreach ($args as $key=>$value) {
   		$mymyntra_msg = str_replace("[".$key."]",$value, $mymyntra_msg);
   	}

   	return $mymyntra_msg;
}

function populate_basic_return_request_details($return_request){
	$return_request['pickup_promise_date'] = $return_request['createddate']+7*24*60*60;
	$return_request['createddate']= date('F jS Y', $return_request['createddate']);
	$return_request['createddate_display'] = $return_request['createddate'];
	$return_request['pickup_promise_date'] = $return_request['createddate'] != null ? date('F jS Y', $return_request['pickup_promise_date']) : "--";
	$return_request['status_display'] = getReturnStatusByCode($return_request['status']);
	$return_request['courier_service_display'] = get_courier_display_name($return_request['courier_service']);
	$return_request['reship_courier_display'] = get_courier_display_name($return_request['reship_courier']);
	$return_request['style_id'] = $return_request['product_style'];
	$return_request['productStyleName'] = $return_request['name'];
	$return_request['product_display_name'] = $return_request['productStyleName'];
	$return_request['designImagePath'] = str_replace("_images", "_images_96_128", $return_request['default_image']);

	return $return_request;
}

function populate_return_request_details($return_request) {
	global $sql_tbl;
	$http_location = HostConfig::$httpHost;
	$baseUrl = HostConfig::$baseUrl;
	$audits = func_query("select * from mk_return_transit_details where returnid = $return_request[returnid]");
	foreach ($audits as $audit){
		$return_request[$audit['to_status'].'_date'] = $audit['audit_time'];
		$return_request[$audit['to_status'].'_fdate'] = date('F jS Y', $audit['audit_time']);
		if($audit['to_status'] == 'RRQS' || $audit['to_status'] == 'RRQS'){
			$return_request['createddate'] = $audit['audit_time'];
		}
		if($audit['to_status'] == 'RQP' || $audit['to_status'] == 'RQSP' || $audit['to_status'] == 'RQCP'){
			$return_request['qapass'] = $audit['audit_time'];
		}
		if($audit['to_status'] == 'RQF' || $audit['to_status'] == 'RQSP' || $audit['to_status'] == 'RQCP'){
			$return_request['qapass'] = $audit['audit_time'];
		}
	}
	if($return_request['is_refunded']){
		$return_request['refunddate'] = ($return_request['RPU_date'] != null)? date('F jS Y', $return_request['RPU_date']):date('F jS Y',$return_request['qapass']);
	}
	$return_request['qapassfdate'] = date('F jS Y', $return_request['qapass']);
	$return_request['pickup_promise_date'] = $return_request['createddate']+7*24*60*60;

	$return_request['createddate']= date('F jS Y', $return_request['createddate']);
	$return_request['createddate_display'] = $return_request['createddate'];
	$return_request['processing_promise_date'] = $return_request['RRC_date']+2*24*60*60;
	$return_request['processing_promise_date'] = $return_request['RRC_date'] != null ? date('F jS Y', $return_request['processing_promise_date']) : "--";
	$return_request['pickup_promise_date'] = $return_request['createddate'] != null ? date('F jS Y', $return_request['pickup_promise_date']) : "--";


	$return_request['pickup_address'] = $return_request['address'].",<br>".$return_request['city'].", ".$return_request['state']." - ".$return_request['zipcode']."<br>".$return_request['country'];
	$return_request['status_display'] = getReturnStatusByCode($return_request['status']);

	if($return_request['is_refunded']){
		$return_request['mymyntra_status'] = getMyMyntraStatusByCode('RFI');
	}else{
		$return_request['mymyntra_status'] = getMyMyntraStatusByCode($return_request['status']);
	}


	$args = array("TRACKING_NO"=>$return_request['tracking_no'], "COURIER_SERVICE"=>$return_request['courier_service'],
				  "PICKUP_INIT_DATE"=>$return_request['RPI_fdate'], "PICKUP_DATE"=>$return_request['RPU_fdate'],
				  "RECEIVED_DATE"=>$return_request['RRC_fdate'],
				  "REASON"=>$return_request['qafail_reason'], "RESHIP_COURIER"=>$return_request['reship_courier'],
				  "RESHIP_TRACKING"=>$return_request['reship_tracking_no'],
				  "RESHIP_DATE"=>$return_request['RRS_fdate'],
				  "REDELIVER_DATE"=>$return_request['RSD_fdate'],
				  "CANCEL_REASON"=>$return_request['cancel_reason'],
				  "PROCESSING_PROMISE_DATE"=>$return_request['processing_promise_date'],
				  "REFUND_AMOUNT"=>$return_request['refundamount'],
				  "REFUND_DATE"=>$return_request['refunddate'],
				  "GUIDE_LINES_LINK" => $baseUrl."faqs#returns",
				  "PICKUP_PROMISE_DATE" => $return_request['pickup_promise_date'],
				  "RPI_2_PROMISE_DATE" => $return_request['pickup_promise_date'],
				  "CUST_CARE" => WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall'));

	if($return_request['is_refunded']){
		$return_request['status_msg'] = getMyMyntraStatusMsgForReturn('RFI', $args);
	}else{
		$return_request['status_msg'] = getMyMyntraStatusMsgForReturn($return_request['status'], $args);
	}


	$return_request['courier_service_display'] = get_courier_display_name($return_request['courier_service']);
	$return_request['reship_courier_display'] = get_courier_display_name($return_request['reship_courier']);

	if($return_request['payment_surcharge'] > 0 ) {
		$activeOrderid = findActiveOrderid($return_request['orderid'], $return_request['itemid']);
		if(!empty($activeOrderid)) {
			$return_request['payment_surcharge'] = 0;
		}
	}

	$item_details_sql = "SELECT o.warehouseid, o.coupon, o.group_id, o.loyalty_points_conversion_factor, od.*, po.value as size FROM (((($sql_tbl[orders] o LEFT JOIN $sql_tbl[order_details] od ON o.orderid = od.orderid) LEFT JOIN mk_order_item_option_quantity oq on od.itemid = oq.itemid) LEFT JOIN mk_product_options po on oq.optionid = po.id) LEFT JOIN mk_styles_options_skus_mapping som on oq.optionid = som.option_id) where o.orderid = ".$return_request['orderid']." and od.itemid = ".$return_request['itemid'];
	$item = func_query_first($item_details_sql);
	$return_request['groupid'] = $item['group_id'];
	$return_request['source_warehouseid'] = $item['warehouseid'];
	$source_warehouse = WarehouseApiClient::getWarehouseDetails($item['warehouseid']);
	$return_request['source_wh_name'] = $source_warehouse['name'];

	$unifiedsize=Size_unification::getUnifiedSizeByStyleId($item["product_style"],"array",true);
	$return_request['option'] = $unifiedsize[$item['size']];
	$return_request['size'] = $unifiedsize[$item['size']];

	$return_request['item_price'] = $item['price'];
	$return_request['item_qty'] = $item['amount'];
	$return_request['item_discount'] =  $item['discount'];
	$return_request['difference_refund'] = $item['difference_refund'];
	$return_request['item_coupon_discount'] =  $item['coupon_discount_product'];
	$return_request['cashback_issued'] = $item['cashback'];
	$return_request['item_total_amount'] = $item['total_amount'];
	$return_request['pg_discount'] = $item['pg_discount'];
	$return_request['taxamount'] = $item['taxamount'];
	$return_request['cart_discount'] = $item['cart_discount_split_on_ratio'];
	$return_request['item_loyalty_points_used'] = $item['item_loyalty_points_used'];
	$return_request['item_loyalty_points_awarded'] = $item['item_loyalty_points_awarded'];
	$return_request['loyalty_credit'] = $item['item_loyalty_points_used'] * $item['loyalty_points_conversion_factor'];

	if(!empty($item['coupon_code'])) {
		$return_request['coupon_code'] = $item['coupon_code'];
	} else {
		$return_request['coupon_code'] = $item['coupon'];
	}
	$return_request['item_cash_redeemed'] = $item['cash_redeemed'];
	// MyntCash usage fields (necessary to redeem cashback/myntcash at the time of returns)
	$return_request['store_credit_usage'] = $item["store_credit_usage"];
	$return_request['earned_credit_usage'] = $item["earned_credit_usage"];


	$return_request['style_id'] = $item['product_style'];

	$return_request['subtotal'] = number_format($item['price']*$item['amount'], 2, '.', '');
	if($item['discount'] != 0.00) {
		$return_request['discount_percent'] = round(($item['discount']/($item['price']*$item['amount']))*100);
		$return_request['discounted_price'] = number_format(($item['price']*$item['amount'] - $item['discount']), 2, '.','');
	}
	if($item['cart_discount'] != 0.00) {
		$return_request['discounted_price'] -= number_format(($item['cart_discount_split_on_ratio']), 2, '.','');
	}
	$return_request['final_price_paid'] = number_format(($item['price']*$item['amount']) - $item['discount'] - $item['cart_discount_split_on_ratio']  - $item['coupon_discount_product'] - $item['cash_redeemed'] - $item['pg_discount'], 2, '.', '');
	$return_request['final_price_before_cashback'] = number_format($return_request['final_price_paid'] + $item['cash_redeemed'], 2, '.', '');
	$return_request['final_payment_made'] = number_format($return_request['final_price_paid'], 2, '.', '');

	$stylename = "SELECT name,label FROM $sql_tbl[mk_product_style] WHERE id = ".$item['product_style']."";
	$styleresult = db_query($stylename);
	$stylerow = db_fetch_array($styleresult);
	$return_request['productStyleName'] = $stylerow['name'];
    $return_request['productStyleLabel'] = $stylerow['label'];
    $return_request['productNameSize'] = $stylerow['name'].":Size-".$return_request['size'].":Qty-".$return_request['quantity'];
    $productname = "SELECT product FROM xcart_products WHERE productid = ".$item['productid']."";
	$productresult = db_query($productname);
	$productrow = db_fetch_array($productresult);
    if(!empty($productrow))
		$return_request['productName'] = $productrow['product'];

	$style_properties_sql = "SELECT article_number, product_display_name, default_image FROM mk_style_properties WHERE style_id = ".$item['product_style']."";
	$style_properties = func_query_first($style_properties_sql);
	$return_request['article_number'] = $style_properties['article_number'];
	$return_request['product_display_name'] = $style_properties['product_display_name'];
	$return_request['designImagePath'] = str_replace("_images", "_images_96_128", $style_properties['default_image']);
    $return_request['productNameSize'] .= ":SKU code - ".$return_request['skucode'].":Article No - ".$style_properties['article_number'].":MRP per qty - ".$item['price'];

	$warehouse = WarehouseApiClient::getWarehouseDetails($return_request['warehouseid']);
	$return_request['wh_name'] = $warehouse['name'];

	$store_return_details = "SELECT * FROM xcart_store_returns WHERE returnid = ".$return_request[returnid];
	$store_return_result = db_query($store_return_details);
	$store_return_row = db_fetch_array($store_return_result);
	if(!empty($store_return_row)) {
		$return_request['store_return_id'] = $store_return_row['store_return_id'];
		$return_request['store_order_id'] = $store_return_row['store_order_id'];
		$return_request['store_line_id'] = $store_return_row['store_line_id'];
		$return_request['store_id'] = $store_return_row['store_id'];
	}

	return $return_request;
}

function format_return_requests_fields($return_requests) {
	if($return_requests) {
		foreach ($return_requests as $index=>$request) {
			$return_requests[$index] = populate_return_request_details($request);
		}
		return $return_requests;
	}
	return false;
}

function get_return_details($returnid)
{
	global $sql_tbl;
	$sql = "SELECT * FROM $sql_tbl[returns] WHERE returnid = $returnid";
	global $weblog;
	$weblog->debug($sql);
	$return_request = func_query_first($sql,true);
	return $return_request;
}

function format_return_request_fields($returnid) {
	global $sql_tbl;
	$sql = "SELECT * FROM $sql_tbl[returns] WHERE returnid = $returnid";
	$return_request = func_query_first($sql);
	if(!$return_request){
		return false;
	}
	return populate_return_request_details($return_request);
}

function get_return_request_for_order($orderid) {
	global $sql_tbl;
	$sql = "SELECT * FROM $sql_tbl[returns] WHERE orderid = $orderid";
	$return_requests = func_query($sql);
	if($return_requests && count($return_requests)>0) {
		foreach ($return_requests as $index=>$request) {
			$return_requests[$index] = populate_return_request_details($request);
		}
	}
	return $return_requests;
}

function get_return_request_for_return($returnid) {
	global $sql_tbl;
	$sql = "SELECT * FROM $sql_tbl[returns] WHERE returnid = $returnid";
	global $weblog;
	$weblog->debug($sql);
	$return_requests = func_query($sql);
	if ($return_requests && count($return_requests) > 0) {
		foreach ($return_requests as $index => $request) {
			$return_requests[$index] = populate_return_request_details($request);
		}
	}
	return $return_requests;
}

function get_return_request_for_user($login) {
	global $sql_tbl;
	$returnLimit = FeatureGateKeyValuePairs::getFeatureGateValueForKey('return.api.login.limit', '40');
	$sql = "SELECT * FROM $sql_tbl[returns] WHERE login = '$login' order by createddate desc LIMIT $returnLimit";

	$return_requests = func_query($sql);
	if($return_requests && count($return_requests)>0) {
		foreach ($return_requests as $index=>$request) {
			$return_requests[$index] = populate_return_request_details($request);
		}
	}
	return $return_requests;
}

function get_store_return_details($store_id, $store_line_id = false, $store_return_id = false){

	$sqlquery = "SELECT * FROM xcart_store_returns WHERE store_id = $store_id";
	if($store_return_id){
		$sqlquery .= " AND store_return_id = $store_return_id";
	}
	if($store_line_id){
		$sqlquery .= " AND store_line_id = $store_line_id";
	}

	$store_return_details = func_query_first($sqlquery);

	return $store_return_details;
}

function func_add_new_store_return_request($orderid, $itemid, $skucode, $quantity, $option, $customer_login, $address, $reason,
										   $description, $userid, $tracking_no, $courier_service,$store_return_id, $store_order_id,
										   $store_line_id, $storeid)
{

	$return_mode = "self";
	$return_request['orderid'] = $orderid;
	$return_request['itemid'] = $itemid;
	$return_request['skucode'] = $skucode;
	$return_request['quantity'] = $quantity;
	$return_request['sizeoption'] = $option;
	if($tracking_no) {
		$return_request['status'] = 'RPU';
	}else{
		$return_request['status'] = 'RRQP';
	}
	$return_request['delivery_credit'] = 0.00;
	$return_request['is_refunded'] = true;
	$return_request['refundeddate'] = time();
	$return_request['login'] = $customer_login;
	$return_request['return_mode'] = $return_mode;
	$return_request['reason'] = $reason;
	$return_request['description'] = $description;
	if ($userid) {
		$return_request['createdby'] = $userid;
	} else {
		$return_request['createdby'] = $customer_login;
	}
	$created_date = time();
	$return_request['createddate'] = $created_date;
	$return_request['refund_mode'] = "CASHBACK";
	$return_request['courier_service'] = $courier_service;
	$return_request['tracking_no'] = $tracking_no;


	$return_request['name'] = mysql_real_escape_string($address['name']);
	$return_request['address'] = mysql_real_escape_string($address['address']);
	$return_request['city'] = mysql_real_escape_string($address['city']);
	$return_request['state'] = mysql_real_escape_string($address['state']);
	$return_request['country'] = mysql_real_escape_string($address['country']);
	$return_request['zipcode'] = mysql_real_escape_string($address['pincode']);
	$return_request['mobile'] = mysql_real_escape_string($address['mobile']);
	$return_request['email'] = mysql_real_escape_string($address['email']);
	$return_request['phone'] = mysql_real_escape_string($address['phone']);

	$item_details_sql = "SELECT * FROM xcart_order_details WHERE itemid = $itemid";
	$item = func_query_first($item_details_sql);

	$return_sql = "select returnid from xcart_returns where itemid = $itemid and status != 'RRD'";
	$return_exists = func_query_first($return_sql);
	if ($return_exists['returnid']) {
		$processor = new ReturnsStatusProcessor();
		$return_array["status"] = true;
		$return_array["returnid"] = $return_exists['returnid'];
		$return_array["return_req"] = $processor->getReturnDetails($return_exists['returnid']);
		return $return_array;
	}

	if($item['amount'] != $quantity && $item['amount'] > $quantity && 0 < $quantity) {
		$new_item_row = ItemManager::splitItem($item, $quantity);

		$item_opt_qty = $new_item_row['item_option_qty'];
		unset($new_item_row['itemid']);
		unset($new_item_row['sku_id']);
		unset($new_item_row['item_option_qty']);

		$new_itemid = func_array2insert('order_details', $new_item_row);
		$item_opt_qty['itemid'] = $new_itemid;
		func_array2insert('mk_order_item_option_quantity', $item_opt_qty);

		$item_opt_qty = $item['item_option_qty'];
		unset($item['item_option_qty']);
		unset($item['sku_id']);

		func_array2update('order_details', $item, "itemid = ".$item['itemid']);
		if($item_opt_qty)
			func_array2update('mk_order_item_option_quantity', $item_opt_qty, "itemid = ".$item['itemid']);

		$itemid = $new_itemid;
	}

	$return_request['skucode'] = $skucode;
	$return_request['sizeoption'] = $option;
	$return_request['itemid'] = $itemid;


	$returnid = func_array2insert("xcart_returns", $return_request);
	if ($returnid == 0 || $returnid == false) {
		$returnid = func_query_first("select returnid from xcart_returns where login = '" . $return_request['login'] . "' and orderid = " . $return_request['orderid'] . " and createddate = " . $created_date);
		$returnid = $returnid['returnid'];
	}

	$return_request = format_return_request_fields($returnid);
	add_returns_logs_status_change($returnid, $return_request['status'], "AutoSystem", array("ITEM_NAME" => addslashes($return_request['productStyleName']), "QTY" => $return_request['quantity']));
	add_returns_logs_usercomment($returnid, 'Return request created with return mode : ' . $return_mode, $return_request['createdby'], "Remarks (" . getReturnStatusByCode($return_request['status']) . ")", null, $return_request['status']);

	$return_array = array();
	$return_array["returnid"] = $returnid;
	$return_array["status"] = true;
	$return_id = insert_store_return_mapping($returnid, $store_return_id, $store_order_id,
		$store_line_id, $storeid);
	\EventCreationManager::pushRMSSyncEvent($returnid);
	return $return_array;
}

/**
 * @param $returnid
 * @param $store_return_id
 * @param $store_order_id
 * @param $store_line_id
 * @return bool|int
 */
function insert_store_return_mapping($returnid, $store_return_id, $store_order_id, $store_line_id, $store_id)
{
	$store_return = array();
	$store_return['returnid'] = $returnid;
	$store_return['store_return_id'] = $store_return_id;
	$store_return['store_order_id'] = $store_order_id;
	$store_return['store_line_id'] = $store_line_id;
	$store_return['store_id'] = $store_id;
	$returnid = func_array2insert("xcart_store_returns", $store_return);
	return $returnid;
}

function get_basic_return_request_for_user($login) {
	$returnLimit = FeatureGateKeyValuePairs::getFeatureGateValueForKey('return.api.login.limit', '40');
	$return_requests = func_query("select xr.refundamount, mps.name, msp.default_image,xr.returnid,xr.pickupinitdate,xr.refundeddate,xr.redelivereddate,xr
        .reshippeddate,xr.receiveddate,xr.reship_tracking_no,xr.reship_courier,xo.group_id groupid,xr.orderid,xr.itemid,xr.quantity,xr
        .status,xr.login,xr.return_mode,xr.courier_service,xr.tracking_no,xr.reason,xr.createddate,xr.refund_mode,xod.productid,xod.product_style
        from xcart_returns xr join xcart_orders xo on xr.orderid = xo.orderid join xcart_order_details xod on xod.itemid = xr.itemid
        join mk_style_properties msp on msp.style_id = xod.product_style join mk_product_style mps on mps.id = xod.product_style
        where xr.login='$login' order by createddate desc LIMIT $returnLimit");
	if($return_requests && count($return_requests)>0) {
		foreach ($return_requests as $index=>$request) {
			$return_requests[$index] = populate_basic_return_request_details($request);
		}
	}
	return $return_requests;
}


function func_add_new_return_request($orderid,$itemid,$option,$quantity,$customer_login,
		$userid,$reasoncode,$details,$return_mode,$selected_address_id,
		$from_admin = false,$pickup_courier='',$exchange_address=false, $trackingNumber = false,
		$refundMode,$refundNeftAccountId=null, $api=false) {
	global $xcart_dir, $sql_tbl, $weblog;

	$order_details = func_query_first("SELECT * FROM xcart_orders WHERE orderid = $orderid");

	$exchangeOrderExists = func_query_first("SELECT ex.* from exchange_order_mappings ex, xcart_orders o where "
			."ex.itemid = $itemid and o.orderid = ex.exchange_orderid and o.status not in ('F', 'D', 'L')
			order by exchange_orderid DESC");
	if($exchangeOrderExists != ''){
		$exchangeRTOd = isRTO($exchangeOrderExists['exchange_orderid']);
		if(!$exchangeRTOd && !$exchange_address){
			$return_array = array();
			$return_array["status"] = false;
			return $return_array;
		}
	}

    if( (strtolower($order_details['login']) != strtolower($customer_login)) && !$from_admin){
    	$return_array = array();
    	$return_array["status"] = false;
    	return $return_array;
    }

	$item_details_sql = "SELECT * FROM $sql_tbl[order_details] WHERE itemid = $itemid";
	$item = func_query_first($item_details_sql);

	$return_sql = "select returnid from xcart_returns where itemid = $itemid and status != 'RRD'";
	$return_exists = func_query_first($return_sql);
	if($return_exists['returnid']){
        if($api){
            $processor = new ReturnsStatusProcessor();
            $return_array = array();
            $return_address = WidgetKeyValuePairs::getWidgetValueForKey('returnAddress');
            $return_array["status"] = true;
            $return_array["returnid"] = $return_exists['returnid'];
            $details = $processor->getReturnDetails($return_exists['returnid']);
            $return_array["return_req"] = $details['return_details'];
            $return_array["return_address"] = $return_address;
            return $return_array;
        }
		$processor = new ReturnsStatusProcessor();
		$return_array = array();
		$return_address = WidgetKeyValuePairs::getWidgetValueForKey('returnAddress');
		$return_array["status"] = true;
		$return_array["returnid"] = $return_exists['returnid'];
		$return_array["return_req"] = $processor->getReturnDetails($return_exists['returnid']);
		$return_array["return_address"] = $return_address;
		return $return_array;
	}


	$current_item_option_qty = func_query_first("SELECT * from mk_order_item_option_quantity where itemid = ".$itemid);
	$optionid = $current_item_option_qty['optionid'];
	// create new item row

	if($item['amount'] != $quantity && $item['amount'] > $quantity && 0 < $quantity) {
		$new_item_row = ItemManager::splitItem($item, $quantity);

		$item_opt_qty = $new_item_row['item_option_qty'];
		unset($new_item_row['itemid']);
		unset($new_item_row['sku_id']);
		unset($new_item_row['item_option_qty']);

		$new_itemid = func_array2insert('order_details', $new_item_row);
		$item_opt_qty['itemid'] = $new_itemid;
		func_array2insert('mk_order_item_option_quantity', $item_opt_qty);

		$item_opt_qty = $item['item_option_qty'];
		unset($item['item_option_qty']);
		unset($item['sku_id']);

		func_array2update('order_details', $item, "itemid = ".$item['itemid']);
		if($item_opt_qty)
			func_array2update('mk_order_item_option_quantity', $item_opt_qty, "itemid = ".$item['itemid']);

		$itemid = $new_itemid;
	}

	// get sku related things
	$skucodesql = "select sku_id from mk_styles_options_skus_mapping som where option_id = $optionid and style_id = ".$item['product_style'];
	$sku_results = func_query_first($skucodesql);
	$skuId = $sku_results['sku_id'];
	$skudetails = SkuApiClient::getSkuDetails($skuId);
	$skucode = $skudetails[0]['code'];

	$reason = get_reason_displayname($reasoncode);

	// create a return request
	$return_request = array();
	$return_request['orderid'] = $orderid;
	$return_request['itemid'] = $itemid;
	$return_request['skucode'] = $skucode;
        //$return_request['productNameSize'] = $return_request['productNameSize'] . ": SKU Code - ".$skucode;
	$return_request['quantity'] = $quantity;
	$return_request['sizeoption'] = $option;
	$return_request['status'] = 'RRQS';
	if($return_mode == 'pickup'){
		$return_request['status'] = 'RRQP';
	}else{
		$selfDeliveryCredit = WidgetKeyValuePairs::getWidgetValueForKey('returnSelfDeliveryCredit');
		$return_request['delivery_credit'] = $selfDeliveryCredit;
	}
	$return_request['login'] = $customer_login;

	// populate address details

	if(($selected_address_id && $selected_address_id != "") || ($exchange_address && $exchange_address !="")) {
		if($selected_address_id && $selected_address_id != "") {
			$address_sql = "Select * from mk_customer_address where id=$selected_address_id";
			$address = func_query_first($address_sql);
		} elseif($exchange_address && $exchange_address !="") {
			$address = $exchange_address;
			$return_request['is_refunded'] = 1;
			$return_request['status'] = 'RPI';
			$return_request['tracking_no'] = $trackingNumber;
		}
		$return_request['name'] = mysql_real_escape_string($address['name']);
		$return_request['address'] = mysql_real_escape_string($address['address']);
		$return_request['city'] = mysql_real_escape_string($address['city']);
		$return_request['state'] = mysql_real_escape_string($address['state']);
		$return_request['country'] = mysql_real_escape_string($address['country']);
		$return_request['zipcode'] = mysql_real_escape_string($address['pincode']);
		$return_request['mobile'] = mysql_real_escape_string($address['mobile']);
		$return_request['email'] = mysql_real_escape_string($address['email']);
		$return_request['phone'] = mysql_real_escape_string($address['phone']);
	}else{
		$return_request['name'] = mysql_real_escape_string($order_details['s_firstname'].' '.$order_details['s_lastname']);
		$return_request['address'] = mysql_real_escape_string($order_details['s_address']);
		$return_request['city'] = mysql_real_escape_string($order_details['s_city']);
		$return_request['state'] = mysql_real_escape_string($order_details['s_state']);
		$return_request['country'] = mysql_real_escape_string($order_details['s_country']);
		$return_request['zipcode'] = mysql_real_escape_string($order_details['s_zipcode']);
		$return_request['mobile'] = mysql_real_escape_string($order_details['mobile']);
		$return_request['email'] = mysql_real_escape_string($order_details['s_email']);
		$return_request['phone'] = mysql_real_escape_string($order_details['phone']);
	}

	if($return_mode == 'pickup') {
		$return_request['pickup_charges'] = WidgetKeyValuePairs::getWidgetValueForKey('returnPickupCharges');
	}

	$return_request['return_mode'] = $return_mode;
	$return_request['reason'] = $reason;
	$return_request['description'] = $details;
	if($userid) {
		$return_request['createdby'] = $userid;
	} else {
		$return_request['createdby'] = $customer_login;
	}
	$created_date = time();
	$return_request['createddate'] = $created_date;
	if($return_mode == 'pickup' && $pickup_courier != '') {
		$return_request['courier_service'] = $pickup_courier;
	}
	// add refund mode
	$return_request['refund_mode'] = $refundMode;
	$return_request['refund_neft_account_id'] = $refundNeftAccountId;

	$returnid = func_array2insert("xcart_returns", $return_request);
	if($returnid == 0 || $returnid == false){
		$returnid = func_query_first("select returnid from xcart_returns where login = '".$return_request['login']."' and orderid = ".$return_request['orderid']." and createddate = ".$created_date);
		$returnid = $returnid['returnid'];
	}

	$return_request = format_return_request_fields($returnid);
	add_returns_logs_status_change($returnid, $return_request['status'], "AutoSystem", array("ITEM_NAME"=>addslashes($return_request['productStyleName']), "QTY"=>$return_request['quantity']));
	add_returns_logs_usercomment($returnid, 'Return request created with return mode : '.$return_mode, $return_request['createdby'], "Remarks (".getReturnStatusByCode($return_request['status']).")",null,$return_request['status']);

	if($return_mode == 'pickup' && !$exchange_address){
		$push_response = push_return_to_lms($returnid, $return_request['zipcode'], $pickup_courier, 'RPI', $return_request['createdby']);
		if(!$push_response['status']){
			$warning_msg = $push_response['reason'];
		}
	}

	if($exchange_address && $exchange_address !=""){
		// Don't send a mail/sms for the return created via LMS
	}else{
		if($return_mode == 'pickup'){

			$msg = "Hi $return_request[name], we have received your request to return a product purchased on Myntra."
				." The return ID is $returnid and the order id is $order_details[group_id].";

			if($pickup_courier != 'ML'){
				$msg .= " Please refer to an email sent to your registered email id for some important details about your pickup. We will pick up the return product by ".$return_request['pickup_promise_date'].".";
			} else {
				$msg .= " We will pickup the shipment by ".$return_request['pickup_promise_date'].".";
			}

		// self ship
        }else{
			$msg = "Hi $return_request[name], we have received your request to return a product purchased on Myntra. ".
        		"The return ID is $returnid and the order id is $order_details[group_id]. ".
        		"Please ship the product to our address mentioned in the return form as soon as possible.";
		}

		// refund mode is OR/NEFT
		if(strtoupper($refundMode) == "OR" || strtoupper($refundMode) == "NEFT"){
			$msg .= " Post verification of the product, refund amount will be credited back to your card/bank account.";
		}

		func_send_sms($return_request['mobile'], $msg);

		send_return_confirmation_mail($return_request);
	}
	$return_array = array();
	$return_address = WidgetKeyValuePairs::getWidgetValueForKey('returnAddress');
	$return_array["returnid"] = $returnid;
	$return_array["return_req"] = $return_request;
	$return_array["return_address"] = $return_address;
	$return_array['warning_message'] = $warning_msg;
	$return_array["status"] = true;

	\EventCreationManager::pushRMSSyncEvent($returnid);
	return $return_array;
}

function push_return_to_lms($returnid,$zipcode, $courier, $to_status,$login){
	$processor = new ReturnsStatusProcessor();
	if(is_return_eligible_for_pushing_to_ld($courier)){
		$return_pickup_response = ShipmentApiClient::createReturnPickUpRequest($returnid, $courier);
		if($return_pickup_response['status'] == 'success'){
			$processor->changeReturnsStatus($returnid, $to_status, $courier, $return_pickup_response['tracking_number'], '', '',$login ,'Marking as return pick up initiated, after initiating pick up in LMS','',null,$return_pickup_response['deliveryCenterCode']);
			return array('status'=>true);
		}else{
			$warning_msg = "Pick up request was not filed with lms. please notify engineering team";
			return array('status'=>false,'reason' => $warning_msg);
		}
	} else {
		//$processor->changeReturnsStatus($returnid, $to_status, $courier, '', '', '', $login ,'Marking as return pick up initiated');
	}

	return array('status'=>false,'reason' => 'pickup for this zipcode is not supported by ML');
}


function add_returns_logs_usercomment($returnid, $comment, $login, $title,$from_status= '',$to_status='') {
	global $sql_tbl;
	$commenttime = time();
	$comment = mysql_real_escape_string($comment);
	$title = mysql_real_escape_string($title);
	$query = "insert into $sql_tbl[return_logs] (returnid,commenttype,commenttitle,addedby,description,adddate) values ($returnid,'Note', '$title', '$login', '$comment', $commenttime)";
	db_query($query);
	if($from_status != '' || $to_status != ''){
               $sql = "insert into mk_return_transit_details values (NULL,$returnid,'$from_status','$to_status',$commenttime,'$login')";
               func_query($sql);
    }
}

function add_returns_logs_status_change($returnid, $status, $login, $args) {
	global $sql_tbl;
	$sql = "Select display_name, comments_template from $sql_tbl[return_statuses] where code = '$status'";
	$row = func_query_first($sql);
	$comment = $row['comments_template'];
	if($comment) {
		if(!empty($args)) {
			foreach($args as $key=>$val) {
				$comment = str_replace("[".$key."]", $val, $comment);
			}
		}
		$commenttime = time();
		$comment = mysql_real_escape_string($comment);
		$query = "insert into $sql_tbl[return_logs] (returnid,commenttype,commenttitle,addedby,description,adddate) values ($returnid,'Note', '".$row['display_name']."', '$login', '$comment', $commenttime)";
		db_query($query);
	}
}

function get_all_return_logs($returnid) {
	global $sql_tbl;
	$commentsSql = "select * from $sql_tbl[return_logs] where returnid = $returnid order by commentid";
	$comments = func_query($commentsSql);
	foreach($comments as $index=>$comment) {
		$comments[$index]['adddate'] = $comment['adddate'] != null ? date('d-m-Y H:i:s', $comment['adddate']) : "--";
	}
	return $comments;
}

function get_all_return_logs_for_orderid($orderid) {
	global $sql_tbl;
	$commentsSql = "select log.* from mk_returns_comments_log log,xcart_returns xr where xr.returnid = log.returnid and xr.orderid = $orderid order by xr.returnid,log.commentid";
	$comments = func_query($commentsSql);
	foreach($comments as $index=>$comment) {
		$comments[$index]['adddate'] = $comment['adddate'] != null ? date('d-m-Y H:i:s', $comment['adddate']) : "--";
	}
	return $comments;
}

function func_get_refund_amount($return_req) {
	$refund_amount = $return_req['item_total_amount'] + $return_req['taxamount'] - $return_req['item_coupon_discount'] - $return_req['pg_discount'];
	$refund_amount -= $return_req['cashback_issued'];
	$refund_amount -= $return_req['loyalty_credit'];

	$refund_amount -= $return_req['pickup_charges'];

	if($return_req['return_mode'] == 'self') {
		$refund_amount += $return_req['delivery_credit'];
	}

	if (!empty($return_req['payment_surcharge']) && $return_req['payment_surcharge'] > 0){
		$refund_amount += $return_req['payment_surcharge'];
	}


	if($return_req['difference_refund'] > 0){
		$refund_amount -= $return_req['difference_refund'];
	}

	if($refund_amount < 0){
		$refund_amount = 0;
	}

	return $refund_amount;
}

// Coupon Value = Price - any coupon discount - (decrement CB coupon - remaining CB) - 99
function issue_refund_for_return($return_req, $admin_user) {
	global $cashback_gateway_status;

	$couponAdapter = CouponAdapter::getInstance();
	$startDate = strtotime(date('d F Y', time()));
	$endDate = strtotime('31st Dec 2037');

	$orderid = $return_req['orderid'];
	$itemid = $return_req['itemid'];

	$emi_charge_applied = func_query_first_cell("select payment_surcharge from xcart_orders where orderid='$orderid'",true);
	if($emi_charge_applied>0) {
		$active_order_id = findActiveOrderid($orderid, $itemid);
		if(empty($active_order_id)) {
			//no active order found refund emi charges also.
			$return_req['payment_surcharge'] = $emi_charge_applied;
		} else {
			//an active orderid is found shift emi charge to that order if it is a different orderid
			if($active_order_id!=$orderid) {
				func_query("update xcart_orders set payment_surcharge='0.00' where orderid=$orderid");
				func_query("update xcart_orders set payment_surcharge='$emi_charge_applied' where orderid=$active_order_id");
			}
		}
	}

	$refund_amount = func_get_refund_amount($return_req);
    $used_loyalty_points_refunded = $return_req['item_loyalty_points_used'];
    $awarded_loyalty_points_reverted = $return_req['item_loyalty_points_awarded'];

	if($cashback_gateway_status == "on") {
		// Credit teh refund amount into users cashback account.
		$product_name = $return_req['product_display_name'];
		if($return_req['quantity'] > 1){
			$product_name .= "(".$return_req['quantity']." pieces)";
		}

		//$couponCode = $couponAdapter->creditCashback($return_req['login'], $refund_amount, "Refund for return of $product_name of Return No. ".$return_req['re

		$trs = new MyntCashTransaction($return_req['login'], MyntCashItemTypes::ITEM_ID, $return_req["itemid"], MyntCashBusinessProcesses::RETURN_PRODUCT , 0, 0 , 0, "Refund for return of $product_name of Return No. ".$return_req['returnid']);
		MyntCashService::creditMyntCashForItemReturn($trs, $refund_amount, $return_req);

        if($used_loyalty_points_refunded != 0.00){
            $ret = LoyaltyPointsPortalClient::creditForItemCancellation($return_req['login'], $return_req['orderid'], $used_loyalty_points_refunded,
                    "Refunding loyalty points used for return no. $return_req[returnid] in order $return_req[orderid]");
            if($ret['status']['statusType'] == 'ERROR'){
                $refund_comment .= " Refund of ".number_format($used_loyalty_points_refunded, 2)." loyalty-points-used FAILED for returned item in order $return_req[orderid].\n";
            } else {
                $refund_comment .= " Refund of ".number_format($used_loyalty_points_refunded, 2)." loyalty-points-used for returned item in order $return_req[orderid].\n";
            }
        }

        if($awarded_loyalty_points_reverted != 0.00){
            $ret = LoyaltyPointsPortalClient::debitForItemCancellation($return_req['login'], $return_req['orderid'], $awarded_loyalty_points_reverted,
                    "Reversing credit of points for return no. $return_req[returnid] of order no. $return_req[orderid]");
            if($ret['status']['statusType'] == 'ERROR'){
                $refund_comment .= " Reverting ".number_format($awarded_loyalty_points_reverted, 2)." loyalty-points-awarded FAILED for returned item in order $return_req[orderid].\n";
            } else {
                $refund_comment .= " Reverting ".number_format($awarded_loyalty_points_reverted, 2)." loyalty-points-awarded for returned item in order $return_req[orderid].\n";
            }
        }

		add_returns_logs_usercomment($return_req['returnid'], "Refund Rs. ".number_format($refund_amount, 2)." issued to user cashback account".$refund_comment, $admin_user, "Refund Issued");
	} else {
		$desc = "Generated for user after returning product - ".addslashes($return_req['productStyleName'])." with item id - ".$return_req['itemid'];
		// Generate the coupon
		$couponCode = $couponAdapter->generateSingleCouponForUser("SC", 9, "StoreCredit", $startDate, $endDate, $return_req['login'],
											'absolute',	$refund_amount, '', 0.00, 'Store Credit for Return');

		add_returns_logs_usercomment($return_req['returnid'], "StoreCredit - $couponCode generated for user.", $admin_user, "Refund Issued");
	}

	$replacement_coupon_text = "";
	if($return_req['item_coupon_discount'] > 0) {
		$expirytime = time()+(30*24*60*60);

		//$replacementcouponCode = $couponAdapter->generateCouponForCancellation( $return_req['coupon_code'],'RFND', 6, 'REFUNDS', time(), $expirytime,$return_req['login'], 'dual', $return_req['item_coupon_discount'], ($return_req['item_coupon_discount'] / $return_req['item_total_amount']) * 100 , $return_req['item_total_amount'],'',false);
		$replacementcouponCode = $couponAdapter->generateRefundCouponForCancellation(
			$return_req['coupon_code'],	$return_req['login'], $return_req['item_coupon_discount'],
				$return_req['item_total_amount']);

		$replacement_coupon_text = "We are creating a replacement for the coupon ".$return_req['coupon_code']." used on this item. You could use the coupon code: $replacementcouponCode on your next transaction on Myntra. The coupon would be valid for the next 30 days, so do make use of it within ".date('F jS Y', $expirytime).".";
		$comment .= "Replacement coupon: ".$replacementcouponCode['description']." issued\n";
		add_returns_logs_usercomment($return_req['returnid'], $comment, $admin_user, "Replacement Coupon");
	}


	// make refund with bank in case of refund mode is OR and NEFT and cashback gateway is on(is always on)
	// if cashback gateway is off anyway we are giving full amount(cashback + customer paid) as coupon.
	// if cahback gateway is off then cashback credit will not happen with that crm service
	// will fail to debit the same and we should not be calling the crm service to refund as well at this time
	// since we would have already given the refund coupon to the customer
	// -- engg_erp_crm@myntra.com
	if(strtoupper($return_req['refund_mode']) == "OR"
			|| strtoupper($return_req['refund_mode']) == "NEFT"){

		// -cashbackredeemed (xcart_order_details
		$refund_amount-= $return_req['item_cash_redeemed'];

		// Remove delivery credit for bank refund. (Delivery Credit goes as cashback only)
		if($return_req['return_mode'] == 'self') {
			$refund_amount-= $return_req['delivery_credit'];
		}

		// construct refund entry array
		$refundEntry = array(
				"login"=>$return_req["login"],
				"orderId"=>$return_req["orderid"],
				"bankRefundAmount"=>$refund_amount,
				"refundMode"=>$return_req["refund_mode"],
				"refundPaymentAccountId"=>$return_req["refund_neft_account_id"],
				"returnId"=>$return_req["returnid"],
				"erpFailReason"=>""// message construct for all erp fail cases with ## separated
		);

		// make the actual refund with bank
		RefundCrmApiClient::generateRefundForReturn($refundEntry);
	}

	// make refund with bank incase of refund mode is OR and NEFT and cashback gateway is always on
	if(strtoupper($return_req['refund_mode']) == "OR"
			|| strtoupper($return_req['refund_mode']) == "NEFT"){
		$msg = "Hi $return_req[name], your return with return ID $return_req[returnid] has been processed."
			." Refund of Rs. ".number_format($refund_amount, 2)." has been initiated to your card/bank account."
			." This amount will reflect in your account within 7-10 working days";

	} else {
		$msg = "Hi $return_req[name], your return with return ID $return_req[returnid] has been processed. We have added a cashback of Rs. ".number_format($refund_amount, 2)." to your Myntra account. Thank you for shopping on Myntra";
	}

	func_send_sms($return_req['mobile'], $msg);

	send_return_process_success_mail($couponCode, $refund_amount, $return_req, $replacement_coupon_text, $awarded_loyalty_points_reverted);

	return array('coupon'=>$couponCode, 'refund'=>$refund_amount);
}

function send_return_process_success_mail($couponCode, $refund_amount, $return_req, $replacement_coupon_text, $awarded_loyalty_points_reverted = 0.00) {

	$bodyArgs = array();
	// set different template incase of refund mode is OR and NEFT
	if(strtoupper($return_req['refund_mode']) == "OR"
			|| strtoupper($return_req['refund_mode']) == "NEFT"){

		$template = "return_process_success_with_NEFT_OR";

		if(strtoupper($return_req['refund_mode']) == "NEFT"){
			$bodyArgs = array('REFUND_STATEMENT'=>'We have initiated the process to refund Rs.'.$refund_amount.' to your bank account.'.
					' This amount will reflect in your account within 3-5 working days.');

		} else if(strtoupper($return_req['refund_mode']) == "OR"){
			$bodyArgs = array('REFUND_STATEMENT'=>'We have initiated the process to refund Rs.'.$refund_amount.' to your card/bank account.'.
					' This amount will reflect in your account within 7-10 working days.');
		}


	} else {
		$template = "return_process_success";
	}

	$return_details = create_return_details_section($return_req, $return_req['cashback_issued'], $return_req['pickup_charges'], $refund_amount);
	$http_location = HostConfig::$httpHost;

    if($awarded_loyalty_points_reverted > 0){
        $loyaltyReversed = floor($awarded_loyalty_points_reverted)." points awarded for this item have been reversed.";
    }



    //build content and subject of the mail
    $subjectArgs = array("RETURN_ID"=>$return_req['returnid']);
    $bodyArgs += array(
                    "RETURN_ID"=>$return_req['returnid'],
                    "USER"=>$return_req['name'],
					"RETURN_DETAILS"=>$return_details,
                    "COUPON_NAME"=>$couponCode,
					"REFUND_AMOUNT"=>$refund_amount,
                    "LOYALTY_REVERSED"=>$loyaltyReversed,
                    "REPLACEMENT_COUPON_MSG"=>$replacement_coupon_text,
	                "MYNT_CREDITS_URL"=>$http_location."/mymyntra.php?view=mymyntcredits#",
	                "MY_RETURNS_URL"=>$http_location."/mymyntra.php?view=myreturns#".$return_req['returnid']
                );

    $MFBConfigArray = array(
    		"CUSTOMER_EMAIL"=> $return_req['email'],
    		"REFERENCE_ID"=>$return_req['returnid'],
    		"REFERENCE_TYPE"=>"RETURN",// SHIPMENT,ORDER,RETURN,SR
    		"FEEDBACK_NAME"=>"return"
    );

    $MFBInstanceObj = new MFBInstance();
    $MFBContent = $MFBInstanceObj->getMFBMailContent($MFBConfigArray);

    if($MFBContent !== false){
    	//send mail with MFB content appended as (type-critical)
    	$bodyArgs['MFB_FORM'] = $MFBContent['MFBMailHTML'];
    } else {
    	$bodyArgs['MFB_FORM'] = "";
    }


    $customKeywords = array_merge($subjectArgs, $bodyArgs);
    $mailDetails = array(
            "template" => $template,
            "to" => $return_req['email'],
            "bcc" => "myntramailarchive@gmail.com",
            "header" => 'header',
            "footer" => 'footer',
            "mail_type" => \MailType::CRITICAL_TXN
        );
    $multiPartymailer = new \MultiProviderMailer($mailDetails, $customKeywords);
    $flag = $multiPartymailer->sendMail();

    if(!$flag) {
    	if($MFBContent !== false){
    		$MFBInstanceObj->deleteMFBInstance($MFBContent['MFBInstanceId']);
    	}
    }

    return $flag;
}

function send_return_process_failed_mail($return_req, $reason) {
	$http_location = HostConfig::$httpHost;

	$return_details = create_return_details_section($return_req, 0.00, 0.00, 0.00, false);

	$myReturnsUrl = $http_location."/mymyntra.php?view=myreturns#".$return_req['returnid'];

	//email template
    $template = "return_process_failed";

    //build content and subject of the mail
    $subjectArgs = array("RETURN_ID"=>$return_req['returnid']);
    $bodyArgs = array(
                    "RETURN_ID"=>$return_req['returnid'],
                    "USER"=>$return_req['name'],
					"RETURN_DETAILS"=>$return_details,
                    "MY_RETURNS_URL"=>$myReturnsUrl,
					"QAFAIL_REASON" => $reason
                );
    $customKeywords = array_merge($subjectArgs, $bodyArgs);
    $mailDetails = array(
            "template" => $template,
            "to" => $return_req['email'],
            "bcc" => "myntramailarchive@gmail.com",
            "header" => 'header',
            "footer" => 'footer',
            "mail_type" => \MailType::CRITICAL_TXN
        );
    $multiPartymailer = new \MultiProviderMailer($mailDetails, $customKeywords);
    $flag = $multiPartymailer->sendMail();

    return $flag;
}

function send_return_confirmation_mail($return_req) {
	$http_location = HostConfig::$httpHost;
	$pickup_charges = 0.00;
	$bodyArgs = array();

	// set different template incase of refund mode is OR and NEFT
	if(strtoupper($return_req['refund_mode']) == "OR"
			|| strtoupper($return_req['refund_mode']) == "NEFT"){

		// in case of neft refund show bank detail in mail
		if(strtoupper($return_req['refund_mode']) == "NEFT"){
			$bodyArgs += array('REFUND_STATEMENT'=>'This could take a maximum of 3-5 business days from the date the return is picked up.');

			$bankRespo = RefundCrmApiClient::getNEFTBankAccount($return_req["login"], $return_req['refund_neft_account_id']);
			if($bankRespo->isSuccess()){
				$neftAccount = $bankRespo->getData();
				$neftAccount = $neftAccount['neftAccountEntry'][0];
				if(!empty($neftAccount)){
					$bodyArgs += array('NEFT_ACCOUNT'=>
							'<br/>Please find below the bank detail to which NEFT will be made:'.
							'<br/><br/><b>NEFT Account Detail</b>'.
							'<br/>Account Name: '.$neftAccount["accountName"].
							'<br/>Account Number: '.maskString($neftAccount["accountNumber"]).
							'<br/>Type: '.$neftAccount["accountType"].
							'<br/>Bank: '.$neftAccount["bankName"].
							'<br/>Branch: '.$neftAccount["branch"].
							'<br/>IFSC: '.$neftAccount["ifscCode"].
							'<br/><br/>'
							);
				}else {
					$bodyArgs += array('NEFT_ACCOUNT'=>'');
				}
			} else {
				$bodyArgs += array('NEFT_ACCOUNT'=>'');
			}

		} else if(strtoupper($return_req['refund_mode']) == "OR"){
			$bodyArgs += array('REFUND_STATEMENT'=>'This could take a maximum of 7-10 business days from the date the return is picked up.');
			// since OR also uses the same template, need to set with blank string
			$bodyArgs += array('NEFT_ACCOUNT'=>'');
		}

		if($return_req['return_mode'] == 'pickup') {
			if($return_req['courier_service'] != 'ML'){
				$template = "return_confirmation_tpl_pickup_NEFT_OR";

			} else {
				$template = "return_confirmation_pickup_NEFT_OR";
			}

		}else{
			$template = "return_confirmation_self_NEFT_OR";
		}


	} else {
		if($return_req['return_mode'] == 'pickup') {
			if($return_req['courier_service'] != 'ML'){
				$template = "return_confirmation_tpl_pickup";
			} else {
				$template = "return_confirmation_pickup";
			}
		}else{
			$template = "return_confirmation_self";
		}
	}



	$cashback_issued = $return_req['cashback_issued'];

	$return_details = create_return_details_section($return_req, 0, 0, 0, false);
	$myReturnsUrl = $http_location."/mymyntra.php?view=myreturns#".$return_req['returnid'];

	$return_address = WidgetKeyValuePairs::getWidgetValueForKey('returnAddress');

	if ($return_request['courier_service'] != 'ML'){
		$addressToPincodeMap = json_decode(WidgetKeyValuePairs::getWidgetValueForKey('addressToPincodeMap'), true);

		if(in_array($return_req['zipcode'], $addressToPincodeMap[0]['zipcodes'])){
			$return_address = $addressToPincodeMap[0]['address'];
		} elseif(in_array($return_req['zipcode'], $addressToPincodeMap[1]['zipcodes'])){
			$return_address = $addressToPincodeMap[1]['address'];
		}
	}

    //build content and subject of the mail
    $subjectArgs = array("RETURN_ID"=>$return_req['returnid']);
    $bodyArgs += array(
                    "RETURN_ID"=>$return_req['returnid'],
    				"ORDER_ID"=>$return_req['orderid'],
                    "USER"=>$return_req['name'],
				    "RETURN_DETAILS"=>$return_details,
                    "MY_RETURNS_URL"=>$myReturnsUrl,
				    "MYNTRA_ADDRESS"=>$return_address,
				    "PICK_UP_PROMISE_DATE" =>$return_req['pickup_promise_date']
                );

    $customKeywords = array_merge($subjectArgs, $bodyArgs);
    $mailDetails = array(
            "template" => $template,
            "to" => $return_req['email'],
            "bcc" => "myntramailarchive@gmail.com",
            "header" => 'header',
            "footer" => 'footer',
            "mail_type" => \MailType::CRITICAL_TXN
        );
    $multiPartymailer = new \MultiProviderMailer($mailDetails, $customKeywords);
    $flag = $multiPartymailer->sendMail();

    return $flag;

}

function send_return_cancellation_mail($return_req, $cancel_reason) {
	$http_location = HostConfig::$httpHost;
	$return_details = create_return_details_section($return_req, 0, 0, 0, false);

	$reship_msg="";
	if($return_req['status'] == 'RRC' || $return_req['status'] == 'RPU' || $return_req['status'] == 'RDL') {
		$reship_msg = "<p>We would be arranging for reshipment of your returned item within the next 2 working days.</p>";
	}
	$myReturnsUrl = $http_location."/mymyntra.php?view=myreturns#".$return_req['returnid'];

	//email template
    $template = "return_process_success";

    //build content and subject of the mail
    $subjectArgs = array("RETURN_ID"=>$return_req['returnid']);
    $bodyArgs = array(
                    "RETURN_ID"=>$return_req['returnid'],
                    "USER"=>$return_req['name'],
					"RETURN_DETAILS"=>$return_details,
                    "CANCEL_REASON"=>$cancel_reason,
					"RESHIP_MSG"=>$reship_msg,
                    "MY_RETURNS_URL"=>$myReturnsUrl
                );
    $customKeywords = array_merge($subjectArgs, $bodyArgs);
    $mailDetails = array(
            "template" => $template,
            "to" => $return_req['email'],
            "bcc" => "myntramailarchive@gmail.com",
            "header" => 'header',
            "footer" => 'footer',
            "mail_type" => \MailType::CRITICAL_TXN
        );
    $multiPartymailer = new \MultiProviderMailer($mailDetails, $customKeywords);
    $flag = $multiPartymailer->sendMail();

    return $flag;
}

function send_return_reshipped_mail($returnId) {
	$http_location = HostConfig::$httpHost;
	$return_req = format_return_request_fields($returnId);
	$return_details = create_return_details_section($return_req, 0, 0, 0, false);
	$myReturnsUrl = $http_location."/mymyntra.php?view=myreturns#".$return_req['returnid'];


	$bodyArgs = array(
                    "RETURN_ID"=>$return_req['returnid'],
                    "USER"=>$return_req['name'],
					"RETURN_DETAILS"=>$return_details,
                    "CANCEL_REASON"=>$cancel_reason,
					"RESHIP_COURIER_INFO"=>$reship_msg,
  				    "RESHIP_TRACKING"=>$return_req['reship_tracking_no'],
                    "MY_RETURNS_URL"=>$myReturnsUrl
                );
	if($return_req['reship_courier'] == 'ML') {
   		$bodyArgs['RESHIP_COURIER_INFO'] = "Myntra Logistics";
   	} else {
   		$bodyArgs['RESHIP_COURIER_INFO'] = get_courier_display_name($return_req['reship_courier']);
   	}

    //email template
    $template = "return_reshipped";

    //build content and subject of the mail
    $subjectArgs = array("RETURN_ID"=>$return_req['returnid']);

    $customKeywords = array_merge($subjectArgs, $bodyArgs);
    $mailDetails = array(
            "template" => $template,
            "to" => $return_req['email'],
            "bcc" => "myntramailarchive@gmail.com",
            "header" => 'header',
            "footer" => 'footer',
            "mail_type" => \MailType::CRITICAL_TXN
        );
    $multiPartymailer = new \MultiProviderMailer($mailDetails, $customKeywords);
    $flag = $multiPartymailer->sendMail();

    return $flag;
}

function send_return_recieved_mail($return_req) {
	$http_location = HostConfig::$httpHost;
	$return_details = create_return_details_section($return_req, 0, 0, 0, false);
	$myReturnsUrl = $http_location."/mymyntra.php?view=myreturns#".$return_req['returnid'];

	//email template
    $template = "return_recieved";

    //build content and subject of the mail
    $subjectArgs = array("RETURN_ID"=>$return_req['returnid']);
    $bodyArgs = array(
                    "RETURN_ID"=>$return_req['returnid'],
                    "USER"=>$return_req['name'],
					"RETURN_DETAILS"=>$return_details,
  				    "MY_RETURNS_URL"=>$myReturnsUrl,
                    "RETURN_CREATED_DATE" =>$return_req['createddate'],
                    "PROCESSING_PROMISE_DATE" => $return_req['processing_promise_date']
                );
    $customKeywords = array_merge($subjectArgs, $bodyArgs);
    $mailDetails = array(
            "template" => $template,
            "to" => $return_req['email'],
            "bcc" => "myntramailarchive@gmail.com",
            "header" => 'header',
            "footer" => 'footer',
            "mail_type" => \MailType::CRITICAL_TXN
        );
    $multiPartymailer = new \MultiProviderMailer($mailDetails, $customKeywords);
    $flag = $multiPartymailer->sendMail();

    return $flag;
}

function send_return_update_reminder_mail($return_req) {
	$http_location = HostConfig::$httpHost;
	$return_details = create_return_details_section($return_req, 0, 0, 0, false);
	$myReturnsUrl = $http_location."/mymyntra.php?view=myreturns#".$return_req['returnid'];

    //email template
    $template = "return_update_reminder";

    //build content and subject of the mail
    $subjectArgs = array("RETURN_ID"=>$return_req['returnid']);
    $bodyArgs = array(
                    "RETURN_ID"=>$return_req['returnid'],
                    "USER"=>$return_req['name'],
					"RETURN_DETAILS"=>$return_details,
  				    "MY_RETURNS_URL"=>$myReturnsUrl,
                    "RETURN_CREATED_DATE" => date('F jS Y', $return_request['createddate'])
                );
    $customKeywords = array_merge($subjectArgs, $bodyArgs);
    $mailDetails = array(
            "template" => $template,
            "to" => $return_req['email'],
            "bcc" => "myntramailarchive@gmail.com",
            "header" => 'header',
            "footer" => 'footer',
            "mail_type" => \MailType::CRITICAL_TXN
        );
    $multiPartymailer = new \MultiProviderMailer($mailDetails, $customKeywords);
    $flag = $multiPartymailer->sendMail();

    return $flag;
}


function create_return_details_section($return_req, $cb_issued, $pickup_charges, $refund_amount, $show_refund_details = TRUE){
	$item_price = number_format(func_get_refund_amount($return_req), 2);
	$itemdetails = '';
	$itemdetails .= '<tr>';
	$itemdetails .= '<td style="padding: 10px 5px;">'.$return_req['productStyleName'].'</td>';
	$itemdetails .= '<td style="padding: 10px 5px;">Rs '.number_format($return_req['item_price'], 2).'</td>';
	$itemdetails .= '<td style="padding: 10px 5px;">Rs '.number_format(($return_req['item_discount']+$return_req['cart_discount']), 2).'</td>';
	$itemdetails .= '<td style="padding: 10px 5px;">'.$return_req['sizeoption'].'</td>';
	$itemdetails .= '<td style="padding: 10px 5px;">'.$return_req['quantity'].'</td>';
	$itemdetails .= '<td style="padding: 10px 5px;" align=right>Rs '.$item_price.'</td>';
	$itemdetails .= '</tr>';
	$itemdetails .= "\n";
	if($show_refund_details) {
		$itemdetails .= '<tr><td style="padding: 5px;" colspan=5 align=right>Total Amount (Rs):</td><td style="padding: 5px;" align=right>'.$item_price.'</td></tr>';
		if (($return_req['refund_mode'] == "OR" || $return_req['refund_mode'] == "NEFT") && $return_req['item_cash_redeemed'] != 0.00) {
			$itemdetails .= '<tr><td style="padding: 5px;" colspan=5 align=right>Cashback Used (Rs):</td><td style="padding: 5px;" align=right>-'.number_format($return_req['item_cash_redeemed'], 2).'</td></tr>';
		}
		if ($return_req['item_coupon_discount'] != 0.00) {
			$itemdetails .= '<tr><td style="padding: 5px;" colspan=5 align=right>Coupon Discount (Rs):</td><td style="padding: 5px;" align=right>-'.number_format($return_req['item_coupon_discount'], 2).'</td></tr>';
		}
		if ($return_req['pg_discount'] != 0.00) {
			$itemdetails .= '<tr><td style="padding: 5px;" colspan=5 align=right>Payment Gateway Discount (Rs):</td><td style="padding: 5px;" align=right>-'.number_format($return_req['pg_discount'], 2).'</td></tr>';
		}
		if ($cb_issued != 0.00) {
			$itemdetails .= '<tr><td style="padding: 5px;" colspan=5 align=right>Cashback Reversal (Rs):</td><td style="padding: 5px;" align=right>-'.number_format($cb_issued, 2).'</td></tr>';
		}
		if (!empty($return_req['loyalty_credit']) && $return_req['loyalty_credit'] > 0) {
			$itemdetails .= '<tr><td style="padding: 5px;" colspan=5 align=right>Loyalty Credit Refund (Rs):</td><td style="padding: 5px;" align=right>'.number_format($return_req['loyalty_credit'], 2).'</td></tr>';
		}
		if (!empty($return_req['payment_surcharge']) && $return_req['payment_surcharge'] > 0) {
			$itemdetails .= '<tr><td style="padding: 5px;" colspan=5 align=right>EMI Charges Refund (Rs):</td><td style="padding: 5px;" align=right>'.number_format($return_req['payment_surcharge'], 2).'</td></tr>';
		}
		if($pickup_charges != 0.00) {
			$itemdetails .= '<tr><td style="padding: 5px;" colspan=5 align=right>Convinience Charges (Rs):</td><td style="padding: 5px;" align=right>-'.number_format($pickup_charges, 2).'</td></tr>';
		}
		if ($return_req['return_mode'] == 'self' && $return_req['delivery_credit'] != 0.00 && $return_req['refund_mode'] != "OR" && $return_req['refund_mode'] != "NEFT") {
			$itemdetails .= '<tr><td style="padding: 5px;" colspan=5 align=right>Self-Shipping Credit (Rs):</td><td style="padding: 5px;" align=right>'.number_format($return_req['delivery_credit'], 2).'</td></tr>';
		}
		if ($return_req['difference_refund'] != 0.00){
			$itemdetails .= '<tr><td style="padding: 5px;" colspan=5 align=right>Price mismatch refund (Rs):</td><td style="padding: 5px;" align=right>-'.number_format($return_req['difference_refund'], 2).'</td></tr>';
		}
        if ($return_req['taxamount'] != 0.00){
			$itemdetails .= '<tr><td style="padding: 5px;" colspan=5 align=right>VAT (Rs):</td><td style="padding: 5px;" align=right>'.number_format($return_req['taxamount'], 2).'</td></tr>';
		}

		$itemdetails .= '<tr><td colspan=7><hr style="border:1px dotted #AAAAAA;"></td></tr>';
		$itemdetails .= '<tr><td style="padding: 5px;" colspan=5 align=right>Total Refund Amount (Rs):</td><td style="padding: 5px;" align=right>'.number_format($refund_amount, 2).'</td></tr>';
		$itemdetails .= "\n";
	}

	$return_details_table = get_returns_mail_table_template();
	$return_details = str_replace("[ITEM_DETAILS]", $itemdetails, $return_details_table);
	return $return_details;
}

function get_returns_mail_table_template() {
	$template_query = "SELECT * FROM mk_email_notification WHERE name = 'return_details_table'";
	$template_result = db_query($template_query);
	$row = db_fetch_array($template_result);
	$table_contents = $row['body'];
	return $table_contents;
}

function get_xls_data_from_requests($results, $format) {
	$ret_set = $results;
	if($format == 'report') {
		$ret_set = array();
		foreach($results as $request) {
			$new_req = array();
			$new_req['Return Id'] = $request['returnid'];
			$new_req['Order Id'] = $request['orderid'];
			$new_req['Item Id'] = $request['itemid'];
			$new_req['SKU Code'] = $request['skucode'];
			$new_req['Product'] = $request['productStyleName'];
			$new_req['Size'] = $request['sizeoption'];
			$new_req['Pieces'] = $request['quantity'];
			$new_req['Amount'] = $request['item_total_amount'];
			$new_req['Name'] = $request['name'];
			$new_req['Address'] = $request['address'].",".$request['city'].",".$request['state'].",".$request['country'];
			$new_req['Pincode'] = $request['zipcode'];
			$new_req['city'] = $request['city'];
			$new_req['Mobile'] = $request['mobile'];
			$new_req['Return Mode'] = $request['return_mode'];
			$new_req['Courier Service'] = $request['courier_service'];
			$new_req['Tracking No'] = $request['tracking_no'];
			$new_req['Status'] = getReturnStatusByCode($request['status']);
			if($request['status'] == 'RFI') {
				$new_req['Coupon Issued'] = $request['couponcode'];
				$new_req['Refund Amount'] = $request['refundamount'];
			}
			if($request['status'] == 'RQF') {
				$new_req['Fail Reason'] = $request['qafail_reason'];
			}
			if($request['status'] == 'RRS' || $request['status'] == 'RSD') {
				$new_req['Reshipped Courier'] = $request['reship_courier'];
				$new_req['Reshipped Tracking'] = $request['reship_tracking_no'];
			}

			$new_req['Request Date'] = $request['createddate'];
			$new_req['Pickup Init Date'] = $request['RPI_fdate'];
			$new_req['Pickup Date'] = $request['RPU_fdate'];
			$new_req['Received Date'] = $request['RRC_fdate'];
			$new_req['QA Pass Date'] = $request['qapassfdate'];
			$new_req['QA Fail Date'] = $request['RQCF_fdate'];
			$new_req['Restocked Date'] = $request['RIS_fdate'];
			$new_req['Rejected Date'] = $request['RRD_fdate'];
			$new_req['ReShipped Date'] = $request['RRS_fdate'];
			$new_req['Delivery Centre'] = $request['dc_code'];
			$new_req['Warehouse'] = $request['wh_name'];
			$ret_set[] = $new_req;
		}
	} else {
		// fetch returns data from returns table based on the template..


	}
	return $ret_set;
}

/*function validate_return_itembarcode($itembarcode, $orderid,$itemid) {
	global $xcart_dir;
	include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");
	$issuedItems = ItemApiClient::get_issued_itembarcodes_for_order($orderid);
	
	if(!in_array($itembarcode, $issuedItems)) {
		return false;
	}
	
	return true;
}*/

function get_return_details_for_item($warehouseid, $itembarcode) {
	global $xcart_dir;
	include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");
	$item = ItemApiClient::get_items_for_barcodes($warehouseid, $itembarcode);
	if($item[0]['itemStatus'] != 'SHIPPED') {
		return array("success"=>false, 'errormsg'=>"Item is not in SHIPPED state");
	}

	$sku_id = $item[0]['sku']['id'];
	$orderid = $item[0]['orderId'];
	$sql = "select rt.* from mk_styles_options_skus_mapping som, mk_order_item_option_quantity oq, xcart_order_details od, xcart_returns rt, xcart_orders o where som.option_id = oq.optionid and oq.itemid = od.itemid and od.itemid = rt.itemid and som.sku_id = $sku_id and od.orderid = o.orderid and rt.status != 'RRD' and o.orderid = $orderid";
	$return_row = func_query_first($sql, true);
	return array("success"=>true, 'return_details'=>$return_row);
}

function validate_return_itembarcode($itembarcodes, $orderid, $itemid, $warehouseid) {
	global $xcart_dir, $weblog;
	if(!is_array($itembarcodes)){
		$itembarcodes = array($itembarcodes);
	}
	$sql = "select msosm.sku_id from xcart_order_details xod,mk_order_item_option_quantity moq,mk_styles_options_skus_mapping msosm where xod.itemid = moq.itemid and xod.product_style = msosm.style_id and moq.optionid = msosm.option_id and xod.itemid = $itemid";
	$row = func_query_first($sql,true);
	$sku_id = $row['sku_id'];

	include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");
	$itembarcode2ItemDetails = ItemApiClient::get_returnable_itembarcodes_and_skuids_for_order($orderid);
	$returnableItemBarcodes = array_keys($itembarcode2ItemDetails);

	$weblog->info("Returnable Item Barcodes - ". print_r($returnableItemBarcodes, true));

	$shipped_items = array();
	$skuids = array();
	foreach ($itembarcodes as $itembarcode){
		$itembarcode = trim($itembarcode);
		if(!in_array($itembarcode, $returnableItemBarcodes)) {
			return array('status'=>false,'reason'=>'item_barcode_not_matched');
		}

		if(isset($itembarcode2ItemDetails[$itembarcode])) {
			$skuids[] = $itembarcode2ItemDetails[$itembarcode]['skuId'];

			if($itembarcode2ItemDetails[$itembarcode]['itemStatus'] == 'SHIPPED'){
				$shipped_items[] = $itembarcode;
			}
		}
	}

	if(!in_array($sku_id, $skuids)){
		return array('status'=>false,'reason'=>'sku_id_not_matched','items'=>implode(",", $shipped_items));
	}

	return array('status'=>true,'items'=>implode(",", $shipped_items));
}

function move_returned_items_to_CR($orderid, $warehouseid, $itemBarcodesStr, $quality, $login, $rejectReason = false, $rejectDescription = false) {
	global $xcart_dir;
	include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");
	return ItemApiClient::remove_item_order_association($orderid, $warehouseid, explode(",", $itemBarcodesStr), "CUSTOMER_RETURNED", $quality, $login, $rejectReason, $rejectDescription);
}

function func_modify_qty_breakup_for_option($old_qty_breakup, $selected_option, $new_qty) {
	$option_qty_mappings = explode(",", $old_qty_breakup);
	$old_breakup = array(); $new_breakup = array();
	foreach($option_qty_mappings as $option_qty_str) {
		$option_qty = explode(":", $option_qty_str);
		if($option_qty[0] == $selected_option) {
			if(intval($option_qty[1]) == intval($new_qty)) {
				$new_breakup[] = $option_qty[0].":".$new_qty;
				$old_breakup[] = $option_qty[0].":";
			} else {
				$old_qty = intval($option_qty[1]) - intval($new_qty);
				$new_breakup[] = $option_qty[0].":".$new_qty;
				$old_breakup[] = $option_qty[0].":".$old_qty;
			}
		} else {
			$old_breakup[] = $option_qty_str;
			$new_breakup[] = $option_qty[0].":";
		}
	}
	return array('old'=>implode(",", $old_breakup), 'new'=>implode(",", $new_breakup));
}

function func_get_returns_overview($noOfDays) {
	global $sql_tbl;
	$returns_query  = "Select * from $sql_tbl[returns] where createddate > unix_timestamp(DATE_SUB(CURDATE(),INTERVAL $noOfDays DAY))";
	$return_requests = func_query($returns_query, true);

	$reasons = get_all_return_reasons();

	$data = array();
	$data['Returns Initiated'] = 0;
	$data['Refund Approved'] = 0;
	$data['Refund Declined'] = 0;
	$data['Restocked'] = 0;
	$data['Rejected'] = 0;
	foreach ($reasons as $row) {
		$data[$row['displayname']] = 0;
	}

	if($return_requests && count($return_requests) > 0 ) {
		$data['Returns Initiated'] = count($return_requests);
		foreach ($return_requests as $return) {
			$reason = $return['reason'];
			$data[$reason] += 1;

			switch ($return['status']) {
				case 'RFI':
					$data['Refund Approved'] += 1;
					break;
				case 'RIS':
					$data['Refund Approved'] += 1;
					$data['Restocked'] += 1;
					break;
				case 'RIJ':
					$data['Refund Approved'] += 1;
					$data['Rejected'] += 1;
					break;
				case 'RQF':
				case 'RRS':
				case 'RSD':
					$data['Refund Declined'] += 1;
					break;
				case 'RJS':
					$data['Refund Declined'] += 1;
					$data['Restocked'] += 1;
					break;
			}
		}
	} else {
		$data['Returns Initiated'] = 0;
	}

	return $data;
}

function func_create_pending_retuns_data($createdBeforeDays) {
	global $sql_tbl;

	$code_sql = "Select display_name, code from mk_return_status_details where end_state = 0";
	$non_end_states = func_query($code_sql, true);
	$state_cond = "";
	$statusCode2DisplayName = array();
	$data = array();
	foreach ($non_end_states as $state_row) {
		$statusCode2DisplayName[$state_row['code']] = $state_row['display_name'];
		$state_cond .= $state_cond == ""?"":",";
		$state_cond .= "'".$state_row['code']."'";
		$data[$state_row['display_name']] = 0;
 	}

	$returns_query = "Select * from $sql_tbl[returns] where createddate < unix_timestamp(DATE_SUB(CURDATE(), INTERVAL $createdBeforeDays DAY)) and status in ($state_cond)";
	$return_requests = func_query($returns_query);

	if($return_requests && count($return_requests) > 0 ) {
		foreach ($return_requests as $return) {
			$statusName = $statusCode2DisplayName[$return['status']];
			$data[$statusName] += 1;
		}
	}

	return $data;
}

function func_create_sla_compliance_data($from_date, $to_date) {
	global $sql_tbl;
	$returns_query = "Select * from $sql_tbl[returns] where createddate >= unix_timestamp('$from_date') and createddate <= unix_timestamp('$to_date 23:59:59')";
	$return_requests = func_query($returns_query, true);

	$return_data = array();

	$all_transitions = func_query("SELECT * from mk_return_state_transitions");
	foreach ($all_transitions as $transition_row) {

		$from_state_date = get_datefield_for_status($transition_row['from_state']);
		$to_state_date = get_datefield_for_status($transition_row['to_state']);
		$sla_time = intval($transition_row['sla']);
		$sla_time = $sla_time*60*60*1000;

		$total_transitions_num = 0;
		$compliant_transition_num = 0;
		$total_time_for_all_transitions = 0;

		foreach ($return_requests as $return) {
			// if both from state and to date are present, then that action was performed.
			// this is ok because one transition can be performed only once..
			if($return[$from_state_date] && $return[$from_state_date] != 0 &&
				$return[$to_state_date] && $return[$to_state_date] != 0) {
				$time_taken_for_action = $return[$to_state_date] - $return[$from_state_date];
				if($time_taken_for_action <= $sla_time){
					$compliant_transition_num++;
				}
				$total_transitions_num++;
				$total_time_for_all_transitions += $time_taken_for_action;
			}
		}

		$data = array();
		$data['action'] = getReturnStatusByCode($transition_row['from_state'])." -> ".getReturnStatusByCode($transition_row['to_state']);
		$data['sla'] = $transition_row['sla'];
		$data['total'] = $total_transitions_num;
		if($total_transitions_num > 0) {
			$data['tat'] = number_format(($total_time_for_all_transitions/($total_transitions_num*1000*60)), 2);
			$data['compliance'] = ($compliant_transition_num/$total_transitions_num)*100;
		} else {
			$data['tat'] = "-";
			$data['compliance'] = "-";
		}

		$return_data[] = $data;
	}

	return $return_data;
}

function updateCourierDetailsForReturn($returnid, $itemid, $courier_service, $tracking_no,$login) {
    global $sql_tbl;
    $return_data['courier_service'] = $courier_service;
	$return_data['tracking_no'] = $tracking_no;
	$return_data['status'] = 'RDU';

	func_array2update("returns", $return_data, "returnid=".$returnid);

	$args = array('COURIER_SERVICE'=>$courier_service, 'TRACKING_NO'=>$tracking_no);

	$title = "Remarks (".getReturnStatusByCode('RDU').")";
	add_returns_logs_usercomment($returnid, 'Customer updated his courier and tracking details for self ship', $login, $title,'RRQS','RDU');
}

function func_receive_return_item($return_details, $itembarcodes, $admin, $usercomment, $title, $warehouseid) {
	if(!is_array($itembarcodes))
		$itembarcodes = array($itembarcodes);

	$return_details = populate_return_request_details($return_details);

	if(!empty($return_details['itembarcode']))
		$returned_itembarcodes = explode(",", $return_details['itembarcode']);

	foreach($itembarcodes as $itembarcode) {
		if(!in_array(trim($itembarcode), $returned_itembarcodes))
			$returned_itembarcodes[] = $itembarcode;
	}
	$return_data = array('itembarcode'=>implode(",",$returned_itembarcodes));

	add_returns_logs_usercomment($return_details['returnid'], $usercomment, $admin, $title);

	if(count($returned_itembarcodes) == $return_details['quantity'] && $return_details['status'] != 'RRC') {
		//all items have been received..
		$return_data['status'] = 'RRC';
		$return_data['warehouseid'] = $warehouseid;
		$order_details['item_status'] = 'RT';
		$title = "Remarks (".getReturnStatusByCode('RRC').")";
		add_returns_logs_usercomment($return_details['returnid'], 'All items received. Moving Return to Received state.', $admin, $title,$return_details['status'],'RRC');
		$msg = "Marked return - ".$return_details['returnid']." Received at warehouse";
	}
	if($order_details)
		func_array2update("order_details", $order_details, "itemid=".$return_details['itemid']);

	func_array2update("returns", $return_data, "returnid=".$return_details['returnid']);

	if(isset($return_data['status'])) {
		send_return_recieved_mail($return_details);
	}

	return $msg;
}

function send_return_onhold_mail($return_id, $template) {
	$return_req = get_return_details($return_id);
	$return_req = populate_return_request_details($return_req);
	$http_location = HostConfig::$httpHost;
	$bodyArgs = array(
			"RETURN_ID"=>$return_req['returnid'],
			"USER"=>$return_req['name'],
			"ITEM_NAME"=>$return_req['product_display_name'],
			"ITEM_PRICE"=>$return_req['item_price'],
			"ITEM_DISCOUNT"=>$return_req['item_discount'],
			"ITEM_SIZE"=>$return_req['size'],
			"ITEM_QUANTITY"=>$return_req['quantity'],
			"TOTAL_PRICE"=>$return_req['item_total_amount'],
			"MY_RETURNS_URL"=>$http_location."/mymyntra.php?view=myreturns#".$return_req['returnid']
	);
	global $configMode;
	$to = $return_req['email'];
	if($configMode != "prod"){
		$to = 'spaluvuri@gmail.com';
	}
	$mailDetails = array(
			"template" => $template,
			"to" => $to,
			"bcc" => "myntramailarchive@gmail.com",
			"header" => 'header',
			"footer" => 'footer',
			"mail_type" => \MailType::CRITICAL_TXN
	);
	$multiPartymailer = new \MultiProviderMailer($mailDetails, $bodyArgs);
	$flag = $multiPartymailer->sendMail();
	$msg = "";

	if($template == "return_onhold_email"){
		$msg = "Dear $return_req[name], We regret to inform you that your return request for return id. $return_req[returnid] "
		."has been put on-hold by our delivery team. Please contact our customer care team for further details/assistance.";
	}elseif("return_onhold_approved_email" == $template){
		$msg = "Dear $return_req[name], We would like to inform you that your return request for return id. $return_req[returnid] "
		."has been has been approved. Our delivery staff will pickup the item within the next 2 - 4 days.";
	}elseif("return_onhold_rejected_email" == $template){
		$msg = "Dear $return_req[name], We regret to inform you that your return request for return id. $return_req[returnid] "
		."has been rejected.";
	}
	$to = $return_req['mobile'];
	if($configMode != "prod"){
		$to = '9945668118';
	}
	func_send_sms($to, $msg);
	return $flag;
}

function is_return_eligible_for_pushing_to_ld($courier){
	$isenabled = FeatureGateKeyValuePairs::getFeatureGateValueForKey('lms.return.push.enabled');
	if($isenabled === 'true'){
		$courierSupported = WidgetKeyValuePairs::getWidgetValueForKey('lms.return.push.couriers');
		$courierSupported = explode(",", $courierSupported);
		if(in_array($courier, $courierSupported)){
			return true;
		}
	}
	return false;
}

function get_time_filters(){
	$timefilters = array( array("code"=>"1","display_name"=>"< 2days"),array("code"=>"2","display_name"=>"2<= days <5"),array("code"=>"3","display_name"=>"5<= days <10"),array("code"=>"4", "display_name"=>">= 10 days"));
	return $timefilters;
}

?>
