<?php
include_once("$xcart_dir/modules/apiclient/OrderTrackingApiClient.php");
/**
 *
 *  feed order details to be tracked in to the shipment tracking system
 *  @param $order_id
 *  @param $tracking_id
 *  @param $courier_operator
 *  @param $shipment_type - type of the shipment 's' - forward shipping, 'r'- return, 'rs' - reshipping
 *  @return bool
 **/
function trackOrder($order_id, $tracking_id, $courier_operator, $shipment_type='s'){	
		if (($shipment_type=='s')||($shipment_type=='r')||($shipment_type=='rs')) {
			try{
				$courier = strpos($courier_operator, "ML") === false?$courier_operator:"ML";
				return _track($order_id, 'o', $tracking_id, $courier, $shipment_type);	
			}catch(Exception $e) {
				$weblog->info("Error Occured - ".$e->getMessage());
				return false;
			}
		}
	return false;
}

function trackItem($item_id, $tracking_id, $courier_operator, $shipment_type){
	if (($shipment_type=='s')||($shipment_type=='r')||($shipment_type=='rs')) {
		return _track($item_id, 'i', $tracking_id, $courier_operator, $shipment_type);
	}
	return false;
}

function untrackOrder($order_id, $tracking_id) {
	return _untrack($order_id, 'o', $tracking_id);	
}  

function untrackItem($item_id, $tracking_id) {
	return _untrack($item_id, 'i', $tracking_id);
}

function _track($ref_id, $ref_type, $tracking_id, $courier_operator, $shipment_type){
	global $sqllog;
	$qry = "INSERT INTO mk_shipment_tracking (ref_id,ref_type,shipment_type,tracking_no,courier_operator) ";
	$qry .= " VALUES ($ref_id,'".$ref_type."', '".$shipment_type."','$tracking_id','".$courier_operator."')";
	$sqllog->debug("SQL Query : ".$qry." "." @ line ".__LINE__." in file ".__FILE__);
	if (!db_query($qry)){
		return false;
	}		
	return true;
}

function _untrack($ref_id, $ref_type, $tracking_id){
	return true;
}


function getOrderTrackingDetails($order_id, $rev_chronological_order=true, $courieroperator='', $trackingNo='', $level= 'LEVEL1') {
	return _getTrackingDetails($order_id, 'o', $rev_chronological_order, $courieroperator, $trackingNo,$level);	
}

function getItemTrackingDetails($item_id, $rev_chronological_order=true) {
	return _getTrackingDetails($item_id, 'i', $rev_chronological_order);
}

function getTrackingDashboardReport($startDt, $endDt){
	global $sqllog;
	$query = "select a.courier_operator, a.payment_method, a.delivery_status, a.ds_count, b.total_count, (a.ds_count *100/b.total_count) as percent from 
		(select xo.courier_service as courier_operator, xo.payment_method, mst.delivery_status, count(*) ds_count from mk_shipment_tracking mst, xcart_orders xo
		where mst.ref_id = xo.orderid
		and mst.courier_operator = xo.courier_service ";
		
		if($startDt != null){
			$query = $query." and xo.shippeddate >= " .$startDt;
		}
		
		if($endDt != null){
			$query = $query . " and xo.shippeddate < " .$endDt;
		}
		
	$query = $query. " group by xo.courier_service, xo.payment_method, mst.delivery_status ) a,
		(select xo.courier_service as courier_operator, xo.payment_method, count(*) as total_count from mk_shipment_tracking mst, xcart_orders xo
		where mst.ref_id = xo.orderid
		and mst.courier_operator = xo.courier_service ";
		
	if($startDt != null){
		$query = $query." and xo.shippeddate >= " .$startDt;
	}
	
	if($endDt != null){
		$query = $query . " and xo.shippeddate < " .$endDt;
	}
		 
	$query = $query . " group by xo.courier_service, xo.payment_method) b
		where a.courier_operator = b.courier_operator
		and a.payment_method = b.payment_method";
	
	$sqllog->debug("SQL Query : ".$query." "." @ line ".__LINE__." in file ".__FILE__);
	$result = func_query($query, true);
	$sqllog->info($result);
	$dashboardReport = array('TOTAL' => array('desc'=>'Total shipped')
		, 'DL' => array('desc'=>'Delivered'), 'DL-PC'=> array('desc'=>'Delivered %')
		,'FIT' => array('desc'=>'In transit - forward'), 'FIT-PC'=> array('desc'=>'In transit - forward %')
		,'RIT' => array('desc'=>'In transit - return'), 'RIT-PC'=> array('desc'=>'In transit - return %')
		,'RT' => array('desc'=>'RTO'), 'RT-PC'=> array('desc'=>'RTO %'));
	
	foreach($result as $key=>$row){
		$dashboardReport['TOTAL'][$row['courier_operator']. '-' . $row['payment_method']] = $row['total_count'];
		$dashboardReport[$row['delivery_status']][$row['courier_operator']. '-'.$row['payment_method']] = $row['ds_count'];
		$dashboardReport[$row['delivery_status']."-PC"][$row['courier_operator']. '-'.$row['payment_method']] = round($row['percent'],2).'%';
	}
	
	$justrows = array();
	foreach($dashboardReport as $k=>$v){
		$justrows[] = $v;
	}
	
	return $justrows;
}

function getShipmentTrackingSLAReport($startDt, $endDt){
	global $sqllog;
	$query = "select cs.courier_service, 
		SUM(IF((dl_tat <= 24*60*60), 1, 0)) dl_tat_in_1 ,
		SUM(IF((dl_tat > 24*60*60 and dl_tat <= 24*60*60 *3), 1, 0)) dl_tat_bt_2_3,
		SUM(IF((dl_tat > 24*60*60*3 and dl_tat <= 24*60*60 *5), 1, 0)) dl_tat_bt_4_5,
		SUM(IF((dl_tat > 24*60*60*5 and dl_tat <= 24*60*60 *8), 1, 0)) dl_tat_bt_6_8,
		SUM(IF((dl_tat > 24*60*60*8 and dl_tat <= 24*60*60 *12), 1, 0)) dl_tat_bt_9_12,
		SUM(IF((dl_tat > 24*60*60*12), 1, 0)) dl_tat_gt_12,
		SUM(IF((fs_tat <= 24*60*60), 1, 0)) fs_tat_in_1,
		SUM(IF((fs_tat > 24*60*60 and fs_tat <= 24*60*60*2), 1, 0)) fs_tat_in_2,
		SUM(IF((fs_tat > 24*60*60*2 and fs_tat <= 24*60*60*3), 1, 0)) fs_tat_in_3,
		SUM(IF((fs_tat > 24*60*60*3), 1, 0)) fs_tat_gt_3,
		SUM(IF((rt_tat <= 24*60*60*3), 1, 0)) rt_tat_in_3 ,
		SUM(IF((rt_tat > 24*60*60*3 and rt_tat <= 24*60*60 *6), 1, 0)) rt_tat_bt_4_6,
		SUM(IF((rt_tat > 24*60*60*6 and rt_tat <= 24*60*60 *9), 1, 0)) rt_tat_bt_7_9,
		SUM(IF((rt_tat > 24*60*60*9 and rt_tat <= 24*60*60 *12), 1, 0)) rt_tat_bt_10_12,
		SUM(IF((rt_tat > 24*60*60*12), 1, 0)) rt_tat_gt_12
		from (select courier_operator, if(mst.delivery_status = 'DL', (UNIX_TIMESTAMP(mst.delivery_date) - xo.shippeddate), null) as dl_tat, 
		(UNIX_TIMESTAMP(mst.pickup_date) - xo.shippeddate) as fs_tat,
		if(mst.delivery_status = 'RT', (UNIX_TIMESTAMP(mst.delivery_date) - xo.shippeddate), null) as rt_tat	
		from mk_shipment_tracking mst, xcart_orders xo
		where mst.ref_id = xo.orderid ";
		
		if($startDt != null){
			$query = $query." and xo.shippeddate >= " .$startDt;
		}
		
		if($endDt != null){
			$query = $query . " and xo.shippeddate < " .$endDt;
		}
				
		$query = $query. ") report, 
		mk_courier_service cs
		where cs.code = report.courier_operator
		group by courier_service";
	
	$sqllog->debug("SQL Query : ".$query." "." @ line ".__LINE__." in file ".__FILE__);
	$result = func_query($query, true);
	$sqllog->info($result);
	return $result;
}

function _getTrackingDetails($ref_id, $ref_type, $rev_chronological_order, $courieroperator, $trackingNo, $level='LEVEL1') {
	$courierKey = trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('lms.shipmentTracking.supportedCourierOperator'));
	
	if(!$courierKey || empty($courierKey) || ($courierKey && in_array($courieroperator, explode(',', $courierKey)))){
		if(!empty($trackingNo) && !empty($courieroperator)){
			$orderTracking = OrderTrackingApiClient::getTrackingDetailsForOrder($courieroperator, $trackingNo, $level);
			$trackingDetailsResponse = $orderTracking[0]["orderTrackingDetails"];
			if($trackingDetailsResponse) {
				if(isset($trackingDetailsResponse['actionDate']))
					$trackingDetailsResponse = array($trackingDetailsResponse);
			
				foreach ($trackingDetailsResponse as $row){
					$trackingDetails[] = array('remarks'=>$row['remark'], 'scan'=>$row['activityType'], 'scanDate'=>$row['actionDate'], 'scannedlocation'=>$row['location']);
				}
			}
		}
	}
	else{	
		global $sqllog;
		$qry = 'SELECT * FROM mk_shipment_tracking_detail WHERE id = ';
		$qry .= ' (SELECT id FROM mk_shipment_tracking WHERE ref_id='.$ref_id.' AND ref_type="'.$ref_type.'") ';
		if ($rev_chronological_order) {
			$qry .= ' ORDER BY action_date DESC';
		} else {
			$qry .= ' ORDER BY action_date ASC';
		}
		$sqllog->debug("SQL Query : ".$qry." "." @ line ".__LINE__." in file ".__FILE__);
		$results = func_query($qry);
		foreach ($results as $row){
			$trackingDetails[] = array('remarks'=>$row['remark'], 'scan'=>$row['activity_type'], 'scanDate'=>$row['action_date'], 'scannedlocation'=>$row['location']);
		}
	}
	return $trackingDetails;
}

?>
