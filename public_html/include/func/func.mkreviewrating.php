<?php 
include_once($xcart_dir.'/include/solr/solrProducts.php');
	/** 
	 * Function to check if the generic type, on which rating/review is to be applied, is properly used
	 * Enter description here ...
	 * @param $generictype
	 */
	function func_validate_generic_type($generictype) {
		$queryTypeExists = "select * from mk_mapping_generic where generic_type='$generictype'";
		$result = db_query($queryTypeExists);
		$mappingcount = db_num_rows($result);
		
		if($mappingcount==1) {
			// There should be exactly one entry for each generic type, in the mk_mapping_generic table.
			return true;
		} else { 
			return false;
		}
	}

	
	function func_insert_generic_rating($loginid, $remoteip, $sessionid, $generictype, $generictypeid, $votevalue, $usertype=0)
	{
		global $sql_tbl,$weblog,$sqllog;
		
		$weblogdata = "Inside function : " . __FUNCTION__ . " params: Login $loginid, RemoteIP $remoteip, SessionID $sessionid, ";
		$weblogdata.= "GenericType $generictype, GenericTypeid $generictypeid, Votevalue $votevalue, UserType $usertype, in file " . __FILE__;
		$weblog->info($weblogdata);

		$insertRatingQuery = "insert into mk_rating_generic " ;
		$insertRatingQuery.= "(login, remote_ip,	sessionid, generic_type, generic_type_id, vote_value, user_type, vote_date) values ";
		$insertRatingQuery.= "('$loginid', '$remoteip', '$sessionid', '$generictype', $generictypeid, $votevalue, $usertype, unix_timestamp(NOW()))";
			
		$sqllog->debug("File Name::>func.mkreviewrating.php##Function Name::>func_insert_generic_rating()####SQL Info::>".$insertRatingQuery); 
		db_query($insertRatingQuery);
		
		$id = mysql_insert_id();
        $non_cust_products = new SolrProducts();
        $non_cust_products->addStyleToIndex($generictypeid); 
		return $id;
	}
	
	
	function func_insert_generic_review($loginid, $remoteip, $generictype, $generictypeid, $review, $lastgenericratingid)
	{
		global $sql_tbl,$weblog,$sqllog;
		
		$weblogdata = "Inside function : " . __FUNCTION__ . " params: Login $loginid, RemoteIP $remoteip, ";
		$weblogdata.= "GenericType $generictype, GenericTypeid $generictypid, Comment $comment, in file " . __FILE__;
		$weblog->info($weblogdata);
		
		$reviewFormatted = mysql_real_escape_string($review);
		if(empty($lastgenericratingid))
			$lastgenericratingid='null';
		
		$insertReviewQuery = "insert into mk_review_generic " ;
		$insertReviewQuery.= "(login, remote_ip, generic_type, generic_type_id, review,  review_date, rating_vote_id) values ";
		$insertReviewQuery.= "('$loginid', '$remoteip', '$generictype', $generictypeid, '$reviewFormatted', unix_timestamp(NOW()), $lastgenericratingid)";
		
		$sqllog->debug("File Name::>func.mkreviewrating.php##Function Name::>func_insert_generic_review()####SQL Info::>".$insertReviewQuery); 
		db_query($insertReviewQuery);
	}

	
	function func_get_reviews_generic_type($generictype, $generictypeid, $sortmode='none') {
		global $sql_tbl,$weblog,$sqllog;
		
		$weblogdata = "Inside function : " . __FUNCTION__ . " params: GenericType $generictype, GenericTypeid $generictypid, SortMode $sortmode in file " . __FILE__;
		$weblog->info($weblogdata);
		
		$selectReviews = "select x.firstname, x.lastname, r.login, ";
		$selectReviews.= " r.review, g.vote_value from mk_review_generic r inner join ";
		$selectReviews.= " xcart_customers x on x.login=r.login left join mk_rating_generic g on g.vote_id=r.rating_vote_id ";
		$selectReviews.= " where r.generic_type='$generictype' and r.generic_type_id=$generictypeid ";
		
		if($sortmode=='high' || $sortmode=='low') {
			$selectReviews.= " and g.vote_value is not null ";
		}
		
		$selectReviews.= " order by ";

		if($sortmode=='high') {
			$selectReviews.= " g.vote_value desc,";
		} else if($sortmode=='low') {
			$selectReviews.= " g.vote_value asc,";
		}
		
		$selectReviews.= " review_date desc";
		
		$sqllog->debug("File Name::>func.mkreviewrating.php##Function Name::>func_get_reviews_generic_type()####SQL Info::>".$insertReviewQuery); 
		$result = db_query($selectReviews);
		
		$all_reviews=array();

		if ($result)
		{
			$i=0;
			while ($row=db_fetch_row($result))
			{
				$all_reviews[$i]['name'] = $row[0] . " " . $row[1];
				$all_reviews[$i]['login'] = $row[2];
				$all_reviews[$i]['review'] = $row[3];
				$all_reviews[$i]['rating'] = $row[4];
				$i++;
			}
		}

		return $all_reviews;
	}
	
	function func_get_average_rating($generictype, $generictypeid) {
		global $sql_tbl,$weblog,$sqllog;
		
		$weblogdata = "Inside function : " . __FUNCTION__ . " params: GenericType $generictype, GenericTypeid $generictypid in file " . __FILE__;
		$weblog->info($weblogdata);
		
		$selectAverageRating = "select avg(vote_value) as avg from mk_rating_generic ";
		$selectAverageRating.= " where generic_type='$generictype' and generic_type_id=$generictypeid and vote_value is not null";
		
		$sqllog->debug("File Name::>func.mkreviewrating.php##Function Name::>func_get_average_rating()####SQL Info::>" . $selectAverageRating); 
		$result = db_query($selectAverageRating);
		
		$row=db_fetch_array($result);
		
		$averageRating = $row['avg'];

		if(empty($averageRating))
			$averageRating = 0;
			
		return round($averageRating,2);
	}	
?>