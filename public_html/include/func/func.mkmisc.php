<?php
if (!defined('XCART_START'))
    {
    header ("Location: ../");
    die ("Access denied");
    }

function func_get_orientation_option_name_value_pairs($optionDetailsArray = array()){
    if (empty($optionDetailsArray))
        return array();
    
    $nameValuePairArray = array();
    
    foreach ($optionDetailsArray as $index => $subarray){
        //print_r($subarray);
        //echo " ";
        $name = $subarray[0];//This is Option Name 
        $token = strtok($subarray[1],",");
        $values = array();//These are values for that option
        while($token){
            //echo $token;
            $values[] = $token;
            $token = strtok(",");
        }
        //print_r($values);
        $nameValuePairArray[] = array($name,$values);
    }
    //echo "<br>";
    //print_r($nameValuePairArray);
    $keys = array_values($nameValuePairArray);
    //echo "<br>";
    //print_r($keys);
    return $nameValuePairArray;
}

function func_get_option_name_value_pairs($optionDetailsArray = array()){
    if (empty($optionDetailsArray))
        return array();
                                              
    $optionNameValueArray = array();        
    $prevName = "";
    $values = array();
    for($i=0;$i<=count($optionDetailsArray);$i++){
        $name = $optionDetailsArray[$i][1];
        if ($name != $prevName){
            if ($i != 0){
                $optionNameValueArray[] = array($prevName,$values);
            }
            $prevName = $name;
            $values = array();
        }
        $values[] = array($optionDetailsArray[$i][2],$optionDetailsArray[$i][0],$optionDetailsArray[$i][3],$optionDetailsArray[$i][4]);
//        if ($i == count($optionDetailsArray)){
//            $optionNameValueArray[] = array($prevName,$values);
//        }
    }
  //  print_r($optionNameValueArray[0]);
    return $optionNameValueArray;
}  
//function added by nishith for getting size chart in new customization
function func_load_product_options_new_cust($productStyleId,$shopMasterId=0)
{
	global $sql_tbl;
	$productOptionsTable=$sql_tbl["mk_product_options"];
	$productStyleTable      =   $sql_tbl["mk_product_style"];
	
		$queryString        = "select	a.id,a.name,a.value,a.default_option,b.chest,b.full_length,b.sleeve_l,b.sleeve_w,b.shoulder from
         	                        $productOptionsTable a,mk_product_options_details b
         	                        where a.id=b.product_option_id and a.style=$productStyleId and b.is_active=1 ORDER BY a.id ASC";
		 $result             =db_query($queryString);
		 $productOptionDetails=array();

		 if ($result)
		 {
			$i=0;

			while ($row=db_fetch_row($result))
			{
				for($j=4;$j<=8;$j++)
				$row[$j]=htmlentities($row[$j]);
				
				//logic for overall size = 2*chest size
				$tsize=trim($row[4]);
         	    $pos=strpos($tsize," ");
         	    if($pos===false) $pos=strpos($tsize,"-");
         	    if($pos===false) $pos=strpos($tsize,".");
         	    if($pos!==false) {
         		   $newrow=substr($tsize,0,$pos+1)*2;
         	    }
         	    else $newrow=$tsize*2;
				array_push($row, $newrow);
				//logic ends
				$productOptionDetails[$i] = $row;
				$i++;
			}
		 }

		 return $productOptionDetails;
}
function func_get_option_name_value_pairs_new_cust($optionDetailsArray = array()){

    if (empty($optionDetailsArray))
        return array();
                                            
    $optionNameValueArray = array();        
    $prevName = "";
    $values = array();
    for($i=0;$i<=count($optionDetailsArray);$i++){
        $name = $optionDetailsArray[$i][1];
        if ($name != $prevName){
            if ($i != 0){
                $optionNameValueArray[] = array($prevName,$values);
            }
            $prevName = $name;
            $values = array();
        }
		
        $values[$i] = array($optionDetailsArray[$i][2],$optionDetailsArray[$i][0],$optionDetailsArray[$i][3],$optionDetailsArray[$i][4]);
		for($j=5;$j<count($optionDetailsArray[$i]);$j++){
			$values[$i][]=$optionDetailsArray[$i][$j];
			
		}
//        if ($i == count($optionDetailsArray)){
//            $optionNameValueArray[] = array($prevName,$values);
//        }

    }
    
    return $optionNameValueArray;
}
//function func_get_product_options_display_array($optionDetailsArray = array()){
//    if (empty($optionDetailsArray))
//        return array();
//                                              
//    $optionNameValueArray = array();        
//    $prevName = "";
//    $values = array();
//    for($i=0;$i<count($optionDetailsArray);$i++){
//        $optionNameValueArray[$optionDetailsArray[$i][0]] = $optionDetailsArray[$i][1].",".$optionDetailsArray[$i][2];
//    }
//    print_r($optionNameValueArray);
//    return $optionNameValueArray;
//}  

function store_cart_products($uuid,$login='',$productsInCart=array(),$productids=array(),$customizeArray = array())
{
	/******************************************************************************/
	/* -------------- persist the master customization array if user create  a product ---*/
	global $sql_tbl,$weblog,$sqllog,$XCART_SESSION_VARS;
	global $max_cart_validity_time;

    $weblog->info("Check whether user is loggin in");

		$weblog->info("Store all the products info in cartArray array");
		$cartArray['cart'] = $productsInCart;
		$cartArray['products'] = $productids;
		(!empty($customizeArray)) ? $cartArray['masterCustomize'] = $customizeArray : $cartArray['masterCustomize'] = $XCART_SESSION_VARS['masterCustomizationArrayD'];
		$sql = "SELECT * FROM $sql_tbl[mk_cart] WHERE cookie ='".$uuid."'";
		$rsCheckCartUser = db_query($sql);
		$sqllog->debug("File Name::>".$scriptname."####"."##SQL INFO::>".$sql); 
		$weblog->info("Check whether user have data in cart in DB then update otherwise insert it. ");
		if(db_num_rows($rsCheckCartUser) > 0)
		{
			$sql = "UPDATE $sql_tbl[mk_cart] SET cart_data = '".addslashes(serialize($cartArray))."', login = '".$login."', last_modified = ".time()." WHERE cookie='".$uuid."'";
			db_query($sql);
			$sqllog->debug("File Name::>".$scriptname."####"."##SQL INFO::>".$sql); 
		}
		else 
		{
		    $expiryDate=time() + $max_cart_validity_time;
			$sql = "INSERT INTO $sql_tbl[mk_cart](cookie,login,cart_data,expiryDate,last_modified) VALUES('".$uuid."','".$login."','".addslashes(serialize($cartArray))."','".$expiryDate."',".time().")";
			//$sql = "INSERT INTO $sql_tbl[mk_cart](cookie,login,cart_data) VALUES('".$uuid."','".$login."','".addslashes(serialize($cartArray))."')";
			//(!empty($cartArray['products'])) ? db_query($sql) : '';
			
			// Pravin - We need to save this cookie data even if $cartArray['products'] is empty.
			// The earlier logic was causing email link login to fail for some jersey cases for COD option.  
			db_query($sql);
			$sqllog->debug("File Name::>".$scriptname."####"."##SQL INFO::>".$sql); 
		}

}

/* aded by arun to limit a numbered array for pagination
 * @param:page_index,offset,limit,array to be limited
 * @return:part of the input array indexed 
 * 	consecutively starting from [0] and without any 
 *  empty element. 
 */
function func_limit_array($page,$offset,$limit,$array_to_limit){
	$limit = $limit * $page;//number of iterations
	for($i=$offset;$i<$limit;$i++){
		if(!empty($array_to_limit[$i]))
			$limit_array[] = $array_to_limit[$i];
		else 
			continue;//do not assign blank values to the resultant array
	}
	return $limit_array;
}

function recreateCartFromShoppingCookie($uuid, $cartdata=null) {
	global $XCART_SESSION_VARS, $sql_tbl;
	if(empty($uuid)) {
		return;
	}

	if(empty($cartdata)) {
		$sql = "select cart_data from $sql_tbl[mk_cart] where cookie = '$uuid'";
		$rset = db_query($sql);
		$records = db_fetch_array($rset);
		$cookie_data =  (unserialize(stripslashes($records['cart_data'])));
	} else {
		$cookie_data = (unserialize(stripslashes($cartdata)));
	}
	
	$productsInCart = $XCART_SESSION_VARS["productsInCart"] = $cookie_data["cart"];
	
	if(!empty($productsInCart)) {
		foreach($productsInCart as $key=>$product)
		{
			$productid = $product["productId"];
			$productids[]=$productid;
			$vatRate = $product["vatRate"];
			$unitPrice = $product["unitPrice"];
			$unitVat = $unitPrice * ($vatRate/100);
			$productPrice=$unitPrice + $unitVat;
			$product["unitVat"] = $unitVat;

			$quantity = $product["quantity"];
			$product["totalPrice"] = $quantity * $unitPrice;
			$product["productPrice"] = $productPrice;
			$vat = $vat + ($unitVat * $quantity);
			$amount = $amount + ($unitPrice * $quantity);
			$totalMRP = $totalMRP + ($productPrice * $quantity);
			$totalQuantity +=  $quantity;

			$productsInCart[$key] = $product;
		}
	}
	
	x_session_register('productsInCart', $productsInCart);
	$totalMRP = number_format($totalMRP,2,".",'');
	$vat = number_format($vat,2,".",'');
	$amount = number_format($amount,2,".",'');
	$totalAmount = $totalMRP;
	if( $totalAmount < 0)
	$totalAmount = 0;
	$totalAmount = number_format($totalAmount,2,".",'');
	$XCART_SESSION_VARS["mrp"] =  $totalMRP;
	$XCART_SESSION_VARS["vat"]=number_format($vat,2,".",'');
	$XCART_SESSION_VARS["amount"]= number_format($amount,2,".",'');
	$XCART_SESSION_VARS["totalAmount"] = number_format($totalAmount,2,".",'');
	$XCART_SESSION_VARS["totalQuantity"] = $totalQuantity;
}

?>
