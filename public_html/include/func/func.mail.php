<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: func.mail.php,v 1.11 2006/04/12 07:36:02 svowl Exp $
#
if ( !defined('XCART_START') ) { header("Location: ../"); die("Access denied"); }

include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");
require_once $xcart_dir.'/XPM/MAIL.php';

x_load('files');

function func_mail_quote($string, $charset) {
	return "=?".$charset."?B?".base64_encode($string)."?=";
}

#
# Send mail abstract function
# $from - from/reply-to address
#
function func_send_mail($to, $subject_template, $body_template, $from, $to_admin, $crypted=false) {
	global $mail_smarty, $sql_tbl;
	global $config;
	global $current_language, $store_language, $shop_language;
	global $to_customer;
	global $override_lng_code;
	global $http_location;

	if (empty($to)) return;

	$from = preg_replace('![\x00-\x1f].*$!sm', '', $from);

	$encrypt_mail = $crypted && $config["Security"]["crypt_method"];

	$lng_code = "";
	if ($to_admin) {
		$lng_code = ($current_language?$current_language:$config["default_admin_language"]);
	}
	elseif ($to_customer) {
		$lng_code = $to_customer;
	}
	else {
		$lng_code = $shop_language;
	}

	$charset = func_query_first_cell ("SELECT charset FROM $sql_tbl[countries] WHERE code='$lng_code'");
	$override_lng_code = $lng_code;

	$mail_smarty->assign_by_ref ("config", $config);

	$lend = (X_DEF_OS_WINDOWS?"\r\n":"\n");

	# Get masil subject
	$mail_subject = chop(func_display($subject_template,$mail_smarty,false));

	# Get messages array
	$msgs = array(
		"header" => array (
			"Content-Type" => "multipart/related;$lend\ttype=\"multipart/alternative\""
		),
		"content" => array()
	);

	if ($config["Email"]["html_mail"] != "Y")
		$mail_smarty->assign("plain_text_message", 1);

	 $body_template .= "<p><i><b>Join the Myntra Designer Community and <b> earn a 10% royalty</b> each time any of your designs gets used by a shopper.<a href='http://www.myntra.com/designers-community'>Upload your designs </a> on Myntra and see your creativity earn you the big bucks.</b></i></p>" ; 		
		
	$mail_message = func_display($body_template,$mail_smarty,false);

	if (X_DEF_OS_WINDOWS) {
		$mail_message = preg_replace("/(?<!\r)\n/S", "\r\n", $mail_message);
	}

	if ($encrypt_mail)
		$mail_message = func_pgp_encrypt ($mail_message);

	$msgs['content'][] = array (
		"header" => array (
			"Content-Type" => "multipart/alternative"
		),
		"content" => array (
			array (
				"header" => array (
					"Content-Type" => "text/plain;$lend\tcharset=\"$charset\"",
					"Content-Transfer-Encoding" => "8bit"
				),
				"content" => strip_tags($mail_message)
			)
		)
	);

	if ($config["Email"]["html_mail"] == "Y" && !$encrypt_mail) {
		if (file_exists($mail_smarty->template_dir."/mail/html/".basename($body_template))) {
			$mail_smarty->assign("mail_body_template","mail/html/".basename($body_template));
			$mail_message = func_display("mail/html/html_message_template.tpl",$mail_smarty,false);

			list($mail_message, $files) = func_attach_images($mail_message);

			$msgs['content'][0]['content'][] = array (
				"header" => array (
					"Content-Type" => "text/html;$lend\tcharset=\"$charset\"",
					"Content-Transfer-Encoding" => "8bit"
				),
				"content" => $mail_message
			);

			if (!empty($files)) {
				foreach ($files as $v) {
					$msgs['content'][] = array (
						"header" => array (
							"Content-Type" => "$v[type];$lend\tname=\"$v[name]\"",
							"Content-Transfer-Encoding" => "base64",
							"Content-ID" => "<$v[name]>"
						),
						"content" => chunk_split(base64_encode($v['data']))
					);
				}
			}
		}
	}

	list($message_header, $mail_message) = func_parse_mail($msgs);

	$mail_from = $from;
	if ($config["Email"]["use_base64_headers"] == "Y")
		$mail_subject = func_mail_quote($mail_subject,$charset);

    $mail_from = from_no_reply($mail_from);
    
	$headers = "From: ".$mail_from.$lend."X-Mailer: PHP/".phpversion().$lend."MIME-Version: 1.0".$lend.$message_header;
	if (trim($mail_from) != "")
		$headers .= "Reply-to: ".$from.$lend;//no-reply@myntra.com should not have any reply sent back, it should be the intended person.

	if (preg_match('/([^ @,;<>]+@[^ @,;<>]+)/S', $from, $m)) {
		return @mail($to,$mail_subject,$mail_message,$headers, "-f".$m[1]);
	} else {

		return @mail($to,$mail_subject,$mail_message,$headers);
	}

}

#
# Parse tree of messages to message header and body
#
function func_parse_mail($msgs, $level = 0) {

	if (empty($msgs))
		return false;

	$lend = (X_DEF_OS_WINDOWS?"\r\n":"\n");
	$head = "";
	$msg = "";

	# Subarray
	if (is_array($msgs['content'])) {
		# Subarray is full
		if(count($msgs['content']) > 1) {
			$boundary = substr(uniqid(time()+rand()."_"), 0, 16);
			$msgs['header']['Content-Type'] .= ";$lend\t boundary=\"$boundary\"";
			foreach($msgs['header'] as $k => $v)
				$head .= $k.": ".$v.$lend;

			if($level > 0)
				$msg = $head.$lend;

			for($x = 0; $x < count($msgs['content']); $x++) {
				$res = func_parse_mail($msgs['content'][$x], $level+1);
				$msg .= "--".$boundary.$lend.$res[1].$lend;
			}

			$msg .= "--".$boundary."--".$lend;
		} else {
			# Subarray have only one element
			list($msgs['header'], $msgs['content']) = func_parse_mail($msgs['content'][0], $level);
		}
	}

	# Current array - atom
	if (!is_array($msgs['content'])) {
		if (is_array($msgs['header']))
			foreach ($msgs['header'] as $k => $v)
				$head .= $k.": ".$v.$lend;

		if ($level > 0)
			$msg = $head.$lend;

		$msg .= $msgs['content'].$lend;
	}

	# Header substitute
	if (empty($head)) {
		if (is_array($msgs['header'])) {
			foreach ($msgs['header'] as $k => $v)
				$head .= $k.": ".$v.$lend;
		} else {
			$head = $msgs['header'];
		}
	}

	return array($head, $msg);
}

#
# Send mail using prepared $body as source (non-templates based)
#
function func_send_simple_mail($to, $subject, $body, $from, $extra_headers=array()) {
	global $config;
	global $current_language;
	global $sql_tbl;
	global $http_location;

	if (empty($to)) return;

	$from = preg_replace('![\x00-\x1f].*$!sm', '', $from);

	if (X_DEF_OS_WINDOWS) {
		$body = preg_replace("/(?<!\r)\n/S", "\r\n", $body);
		$lend = "\r\n";
	}
	else {
		$lend = "\n";
	}

	if (!empty($current_language))
		$charset = func_query_first_cell ("SELECT charset FROM $sql_tbl[countries] WHERE code='$current_language'");

	if (empty($charset))
		$charset = func_query_first_cell ("SELECT charset FROM $sql_tbl[countries] WHERE code='".$config["default_admin_language"]."'");

	$m_from = $from;
	$m_subject = $subject;

	if ($config["Email"]["use_base64_headers"] == "Y") {
		$m_subject = func_mail_quote($m_subject,$charset);
	}

	$headers = array (
		"X-Mailer" => "PHP/".phpversion(),
		"MIME-Version" => "1.0",
		"Content-Type" => "text/plain"
	);

	if (trim($m_from) != "") {
		$headers["From"] = from_no_reply($m_from);
		$headers["Reply-to"] = $m_from;
	}

	$headers = func_array_merge($headers, $extra_headers);

	if (strpos($headers["Content-Type"], "charset=") === FALSE)
		$headers["Content-Type"] .= "; charset=".$charset;

	$headers_str = "";
	foreach ($headers as $hfield=>$hval)
		$headers_str .= $hfield.": ".$hval.$lend;
	
	$body .="<p><i><b>Join the Myntra Designer Community and <b> earn a 10% royalty</b> each time any of your designs gets used by a shopper.<a href='http://www.myntra.com/designers-community'>Upload your designs </a> on Myntra and see your creativity earn you the big bucks.</b></i></p>" ;
	if (preg_match('/([^ @,;<>]+@[^ @,;<>]+)/S', $from, $m))
		@mail($to,$m_subject,$body,$headers_str, "-f".$m[1]);
	else
		@mail($to,$m_subject,$body,$headers_str);
}

function func_pgp_encrypt($message) {
	global $config;

	if (!$config['Security']['crypt_method']) {
		return $message;
	}

	$fn = func_temp_store($message);
	$gfile = func_temp_store("");
	if ($config['Security']['crypt_method'] == 'G') {
		if (empty($config["Security"]["gpg_key"]))
			return $message;

		putenv("GNUPGHOME=".$config["Security"]["gpg_home_dir"]);

		$gpg_prog = func_shellquote($config["Security"]["gpg_prog"]);
		$gpg_key = $config["Security"]["gpg_key"];

		@exec($gpg_prog.' --always-trust -a --batch --yes --recipient "'.$gpg_key.'" --encrypt '.func_shellquote($fn)." 2>".func_shellquote($gfile));
	}
	else {
		if (empty($config["Security"]["pgp_key"]))
			return $message;

		putenv("PGPPATH=".$config["Security"]["pgp_home_dir"]);
		putenv("PGPHOME=".$config["Security"]["pgp_home_dir"]);

		$pgp_prog = func_shellquote($config["Security"]["pgp_prog"]);
		$pgp_key = $config["Security"]["pgp_key"];

		if ($config["Security"]["use_pgp6"] == "Y") {
			@exec($pgp_prog." +batchmode +force -ea ".func_shellquote($fn)." \"$pgp_key\" 2>".func_shellquote($gfile));
		}
		else {
			@exec($pgp_prog.' +batchmode +force -fea "'.$pgp_key.'" < '.func_shellquote($fn).' > '.func_shellquote($fn).".asc 2>".func_shellquote($gfile));
		}
	}

	$af = preg_replace('!\.[^\\\/]+$!S', '', $fn).".asc";
	$message = func_temp_read($af, true);
	$config["PGP_output"] = func_temp_read($gfile, true);
	@unlink($fn);

	return $message;
}

function func_pgp_remove_key() {
	global $config;

	if (!$config['Security']['crypt_method']) {
		return false;
	}

	if ($config['Security']['crypt_method'] == 'G') {
		putenv("GNUPGHOME=".$config["Security"]["gpg_home_dir"]);

		$gpg_prog = func_shellquote($config["Security"]["gpg_prog"]);
		$gpg_key = $config["Security"]["gpg_key"];

		@exec($gpg_prog." --batch --yes --delete-key '$gpg_key'");
	}
	else {
		putenv("PGPPATH=".$config["Security"]["pgp_home_dir"]);
		putenv("PGPHOME=".$config["Security"]["pgp_home_dir"]);

		$pgp_prog = func_shellquote($config["Security"]["pgp_prog"]);
		$pgp_key = $config["Security"]["pgp_key"];

		if ($config["Security"]["use_pgp6"] == "Y") {
			@exec($pgp_prog." -kr +force +batchmode '$pgp_key'");
		}
		else {
			@exec($pgp_prog." -kr +force '$pgp_key'");
		}
	}
}

function func_pgp_add_key() {
	global $config;

	if (!$config['Security']['crypt_method']) {
		return false;
	}

	if ($config['Security']['crypt_method'] == 'G') {
		putenv("GNUPGHOME=".$config["Security"]["gpg_home_dir"]);

		$gpg_prog = func_shellquote($config["Security"]["gpg_prog"]);
		$gpg_key = $config["Security"]["gpg_key"];

		$fn = func_temp_store($config["Security"]["gpg_public_key"]);
		chmod($fn, 0666);

		@exec($gpg_prog.' --batch --yes --import '.func_shellquote($fn));
	}
	else {
		putenv("PGPPATH=".$config["Security"]["pgp_home_dir"]);
		putenv("PGPHOME=".$config["Security"]["pgp_home_dir"]);

		$fn = func_temp_store( $config["Security"]["pgp_public_key"]);

		$pgp_prog = func_shellquote($config["Security"]["pgp_prog"]);
		$pgp_key = $config["Security"]["pgp_key"];

		$ftmp = func_temp_store('');
		if ($config["Security"]["use_pgp6"] == "Y") {
			@exec($pgp_prog.' +batchmode -ka '.func_shellquote($fn).' 2> '.func_shellquote($ftmp));
			@exec($pgp_prog.' +batchmode -ks "'.$pgp_key.'"');
		}
		else {
			@exec($pgp_prog.' -ka +force +batchmode '.func_shellquote($fn).' 2> '.func_shellquote($ftmp));
			@exec($pgp_prog.' +batchmode -ks "'.$pgp_key.'"');
		}

		unlink($ftmp);
	}

	unlink($fn);
}

#
# This function checks if email is valid
#
function func_check_email($email) {
	#
	# Simplified checking
	#
	$email_regular_expression = "^([-\d\w][-.\d\w]*)?[-\d\w]@([-!#\$%&*+\\/=?\w\d^_`{|}~]+\.)+[a-zA-Z]{2,6}$";

	#
	# Full checking according to RFC 822
	# Uncomment the line below to use it (change also check_email_script.tpl)
	#	$email_regular_expression = "^[^.]{1}([-!#\$%&'*+.\\/0-9=?A-Z^_`a-z{|}~])+[^.]{1}@([-!#\$%&'*+\\/0-9=?A-Z^_`a-z{|}~]+\\.)+[a-zA-Z]{2,6}$";

	return preg_match("/".$email_regular_expression."/iS", stripslashes($email));
}

#
# Search images in  message body and return message body and images array
#
function func_attach_images($message) {
	global $http_location, $xcart_web_dir, $xcart_dir, $current_location;

	# Get images location
	$hash = array();
	if (preg_match_all("/\s(?:src=|background=|(?:style=['\"].*url\())['\"]([^'\"]+)['\"]/SsUi", $message, $preg))
		$hash = $preg[1];

	if (empty($hash))
		return array($message, array());

	# Get images data
	$names = array();
	$images = array();
	foreach ($hash as $v) {
		$orig_name = $v;
		$parse = parse_url($v);
		$data = "";
		$file_path = "";
		if (empty($parse['scheme'])) {
			$v = str_replace($xcart_web_dir, "", $parse['path']);
			$file_path = $xcart_dir.str_replace("/", DIRECTORY_SEPARATOR, $v);
			$v = $http_location.$v;
			if (!empty($parse['query']))
				$v .= "?".$parse['query'];

		} elseif (strpos($v, $current_location) === 0) {
			$file_path = $xcart_dir.str_replace("/", DIRECTORY_SEPARATOR,substr($v, strlen($current_location)));
		}

		if (!empty($file_path) && strpos($file_path, ".php") === false && strpos($file_path, ".asp") === false) {
			if (file_exists($file_path) && is_readable($file_path)) {
				$fp = @fopen($file_path, "rb");
				if ($fp) {
					if (filesize($file_path) > 0)
						$data = fread($fp, filesize($file_path));
					fclose($fp);
				}
			}
			else {
				continue;
			}
		}

		if (!empty($images[$v])) {
			continue;
		}

		$tmp = array("name" => basename($v), "url" => $v, "data" => $data);
		if ($names[$tmp['name']]) {
			$cnt = 1;
			$name = $tmp['name'];
			while ($names[$tmp['name']]) {
				$tmp['name'] = $name.$cnt++;
			}
		}

		$names[$tmp['name']] = true;
		if (empty($tmp['data'])) {
			if ($fp = @fopen($tmp['url'], "rb")) {
				do {
					$tmpdata = fread($fp, 8192);
					if (strlen($tmpdata) == 0) {
						break;
					}
					$tmp['data'] .= $tmpdata;
				} while (true);

				fclose($fp);
			} else {
				continue;
			}
		}

		list($tmp1, $tmp2, $tmp3, $tmp['type']) = func_get_image_size(empty($data) ? $tmp['url'] : $file_path);
		if (empty($tmp['type']))
			continue;

		$message = preg_replace("/(['\"])".str_replace("/", "\/", preg_quote($orig_name))."(['\"])/Ss", "\\1cid:".$tmp['name']."\\2", $message);
		$images[$tmp['url']] = $tmp;
	}

	return array($message, $images);
}


/************************************************************/
/*Code By :			Nikhil Gupta				Date: 21/04/07								*/
/*Function Name :	sendMessage()															*/
/*Parameter :			email address, message name, arguments							*/
/************************************************************/

function sendMessage($template, $args, $mailto, $from='', $bcc='')
	{
		global $sql_tbl, $xcart_dir,$http_location;
		$tb_email_notification  = $sql_tbl["mk_email_notification"];
        $sendMail = true;   
		$template_query = "SELECT * FROM $tb_email_notification WHERE name = '$template'";
		$template_result = db_query($template_query);
		$row = db_fetch_array($template_result);

		$subject = $row['subject'];
		$content = $row['body'];
		$email = trim($mailto);
        if(empty($content)){
        	$sendMail = false;
        	########### Mail empty mail template to Ashutosh ######
        	$flag = @mail("alrt_templatenotavailable@myntra.com", "Empty Mail Content template","The template is not available in our DB : ".$template,"From: Myntra Customer Service <support@myntra.com> \n");
        }else{
			$keys = array_keys($args);
			$key ;
	
			for($i = 0; $i < count($keys); $i++)
			{
				$key = $keys[$i];
				$content = str_replace("[".$key."]",$args[$key],$content);
				$subject = str_replace("[".$key."]",$args[$key],$subject);
			}
			if($row['from_name'] != "" & $row['from_designation'] != "")
			{
				$content = str_replace("[FROM_NAME]", $row['from_name'],$content);
				$content = str_replace("[FROM_DESIGNATION]", $row['from_designation'],$content);
			}
			
			$content .= "<br/><br/>";
			
			/*if($from == "Reebok Store"){
				$headers = "Content-Type: text/html; charset=ISO-8859-1 " . "\n";
				$headers .= "From: Reebok Store <support@myntra.com>" . "\n";
				$headers .= "Bcc: myntramailarchive@gmail.com" . "\n";
				 if(!empty($bcc))
					 $headers .= "Bcc: $bcc" . "\n"; 
			}elseif($from == "Nike Store"){
				$headers = "Content-Type: text/html; charset=ISO-8859-1 " . "\n";
				$headers .= "From: Nike Store <support@myntra.com>" . "\n";
				$headers .= "Bcc: myntramailarchive@gmail.com" . "\n";
				 if(!empty($bcc))
					 $headers .= "Bcc: $bcc" . "\n"; 
			}elseif($from == "C&W Rewards Portal"){
				$headers = "Content-Type: text/html; charset=ISO-8859-1 " . "\n";
				$headers .= "From: C&W Rewards Portal <cwsupport@myntra.com>" . "\n";
				$headers .= "Bcc: myntramailarchive@gmail.com" . "\n";
				 if(!empty($bcc))
					 $headers .= "Bcc: $bcc" . "\n";*/ 
			//}else{
//				$content .= "<p><i><b>Join the Myntra Designer Community and <b> earn a 10% royalty</b> each time any of your designs gets used by a shopper.<a href='http://www.myntra.com/designers-community'>Upload your designs </a> on Myntra and see your creativity earn you the big bucks.</i></b></p>" ; 		
				$headers = "Content-Type: text/html; charset=ISO-8859-1 " . "\n";
				$headers .= "From: Myntra Customer Service <support@myntra.com>" . "\n";
				$headers .= "Bcc: myntramailarchive@gmail.com" . "\n";
				 if(!empty($bcc))
					 $headers .= "Bcc: $bcc" . "\n"; 
			//}
        }
        if($sendMail){
		 	$flag = @mail($email, $subject, $content,$headers);
        }
		return $flag;
	}

	
	/**
	 * Get Standard or MRP header and footer template and replace keywords
	 * @param unknown_type $isMRP
	 */
	function getEmailHeaderAndFooter($isMRP=FALSE) {
		global $sql_tbl;
		$header = $isMRP? "mrp_header" : "header";
		$footer = $isMRP? "mrp_footer" : "footer";
	    $header_footer_query = "SELECT name,body FROM ".$sql_tbl['mk_email_notification']." WHERE name in('$footer','$header')";
	    $header_footer_row = db_query($header_footer_query);
		$hdr_ftr = Array();
		while ($eachHdrFtr = db_fetch_row($header_footer_row)) {
			//do a keyword-replacement on header and footer
			if($eachHdrFtr[0] == $footer){
				$hdr_ftr['footer'] = replaceKeywordsWithValues($eachHdrFtr[1]);			
			}
			else if($eachHdrFtr[0] == $header){
				$hdr_ftr['header'] = replaceKeywordsWithValues($eachHdrFtr[1]);			
			}		
		}
		return $hdr_ftr;
	}
	
	/**
	 * 
	 * Replace keywords with values: both common keywords and custom keywords are supported
	 * @param $input
	 * @param $customKeywordValues this should be an associative array with keyword as key: should be without []. These will be replaced inside the input along with reqular definded keywords
	 * e.g. $customKeywordValues = Array("CustomerName"=>"Kundan Burnwal","Designation"=>"Senior Software Engineer");
	 * Note that it doesn't do in-place replacement. Instead the replaced string is returned
	 */
	function replaceKeywordsWithValues($input,$customKeywordValues) {
		global $cashback_gateway_status;
		$callNumber = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
		$callPeriod = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportTime');
		$shippingPeriod = WidgetKeyValuePairs::getWidgetValueForKey('shippingperiod');
		$cashbackMessage = ($cashback_gateway_status == "on")? "10% CASHBACK<br>": "";
		
		$keywordVsValues = Array();
		$keywordVsValues['[MYNTRA_PHONE]'] = $callNumber;
		$keywordVsValues['[CASHBACK_MESSAGE]'] = $cashbackMessage;
		$keywordVsValues['[MYNTRA_CC_PHONE]'] = $callNumber.' ('.$callPeriod.')';
		$keywordVsValues['[SHIPPING_PERIOD]'] = $shippingPeriod;
		
		if(!empty($customKeywordValues)){
			foreach ($customKeywordValues as $keyword => $value) {
				$keywordVsValues["[$keyword]"] = $value;
			}
		}
		$output = $input;
		foreach ($keywordVsValues as $keyword=>$value){
			$output = str_replace($keyword, $value, $output);	
		}
		return $output;
	}
	

/************************************************************/


/************************************************************/
/*Code By :			Nikhil Gupta				Date: 08/10/07								*/
/*Function Name :	sendMessageDynamicSubject()										*/
/*Parameter :			email address, message name, arguments							*/
/************************************************************/

function sendMessageDynamicSubject($template, $args, $mailto, $from='', $subjectarr='',$bcc='', $cc='', $external_mail = true)
{
	global $sql_tbl, $xcart_dir, $maillog, $cashback_gateway_status;
	
	//$call_number_row = func_query_first("SELECT * FROM mk_widget_key_value_pairs where `key` = 'customerSupportCall'");
	//$call_period_row = func_query_first("SELECT * FROM mk_widget_key_value_pairs where `key` = 'customerSupportTime'");
	//$shipping_period_row = func_query_first("SELECT * FROM mk_widget_key_value_pairs where `key` = 'shippingperiod'");
	//$shipping_period = $shipping_period_row['value'];
	//$widgetKeyValueModel = new WidgetKeyValuePairs();
	$call_number = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
	$call_period = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportTime');
	$shipping_period = WidgetKeyValuePairs::getWidgetValueForKey('shippingperiod');
	
	$maillog->info("Send Mail : send mail for template - ".$template);
	$tb_email_notification  = $sql_tbl["mk_email_notification"];
     $sendMail = true; 
	$template_query = "SELECT * FROM $tb_email_notification WHERE name = '$template'";
	$template_result = db_query($template_query);
	$row = db_fetch_array($template_result);
	$headerFooter = getEmailHeaderAndFooter($from == "MRP");
	$header = $headerFooter['header'];
	$footer = $headerFooter['footer'];
	
	$subject = $row['subject']; 
	$content = "\r\n".$row['body']; ########### load content for the #template
	#
	#Replace subject key with values
	#
	$args['MYNTRA_CC_PHONE'] = $call_number.' ('.$call_period.')';
	$args['SHIPPING_PERIOD'] = $shipping_period;
	
	if(is_array($subjectarr)){
		$subjectkeys = array_keys($subjectarr);
		for($i = 0; $i < count($subjectkeys); $i++)
		{
			$key = $subjectkeys[$i];
			$subject = str_replace("[".$key."]",$subjectarr[$key], $subject);
		}	
	} else {
		$subject = (!empty($subjectarr))? $subjectarr : $subject;//subject is the string passed or fromm DB	
	}
	
    if(empty($content)){
        	$sendMail = false;
        	########### Mail empty mail template to Ashutosh ######
        	$flag = @mail("alrt_templatenotavailable@myntra.com", "Empty Mail Content template","The template is not available in our DB : ".$template,"From: Myntra Customer Service <support@myntra.com> \n");
    }else{
		##$content = $row['body'];
		$email = trim($mailto);
	
		if($from == "MRP") {
			//adding extra keys for mrp header and mrp footer
			$args['MRP_HEADER'] = $header;
			$args['MRP_FOOTER'] = $footer;
		}
		$keys = array_keys($args);
		$key ;
	
		for($i = 0; $i < count($keys); $i++)
		{
			$key = $keys[$i];
			$content = str_replace("[".$key."]",$args[$key],$content);
		}
		if($row['from_name'] != "" & $row['from_designation'] != "")
		{
			$content = str_replace("[FROM_NAME]", $row['from_name'],$content);
			$content = str_replace("[FROM_DESIGNATION]", $row['from_designation'],$content);
		}
		$content .= "\r\n";
			
		if(file_exists($xcart_dir."/log"))
		{
			$myFile = "log/log.txt";
			$fh = @fopen($myFile, 'a+') ;//or die("can't open file");
			$stringData = $template." ---- ".$mailto."---------------".date('d/m/Y')."\n";
			fwrite($fh, $stringData);
			fclose($fh);
		}
		else
		{
			mkdir($xcart_dir."/log", 0777);
			$ourFileName = $xcart_dir."/log/log.txt";
			$fh = fopen($ourFileName, 'w') ;//or die("can't open file");
			$stringData = $template." ---- ".$mailto."---------------".date('d/m/Y')."\n";
			fwrite($fh, $stringData);
			fclose($fh);
		}
    	if($from == "C&W Rewards Portal"){
			$headers = "Content-Type: text/html; charset=ISO-8859-1 " . "\n";
			$headers .= "From: C&W Rewards Portal <cwsupport@myntra.com>" . "\n";
			$headers .= "Bcc: myntramailarchive@gmail.com" . "\n";
			 if(!empty($cc))
				 $headers .= "Cc: $cc \n"; 
		} else if($from == "MRP") {
			$headers = "Content-Type: text/html; charset=ISO-8859-1\r\n";
			$headers .= "From: Mynt Club <no-reply@myntra.com>\r\n";
            /* commenting this because these mrp mails are over flooded in myntramail archive
			if($external_mail)
					$headers .= "Bcc: myntramailarchive@gmail.com\r\n";*/
				if(!empty($cc))
					$headers .= "Cc: $cc \r\n";
		} else{
			$headers = "Content-Type: text/html; charset=ISO-8859-1\r\n";
		    if(!empty($from))
		    	$headers .= "From: ".$from." <support@myntra.com>\r\n";
		    else
		        $headers .= "From: Myntra Customer Service <support@myntra.com>\r\n";
		        if($external_mail)
					$headers .= "Bcc: myntramailarchive@gmail.com\r\n";
				if(!empty($cc))
					$headers .= "Cc: $cc\r\n";
		}
    }
    $maillog->info("Send Mail : send mail with contents - ".$content);
	if($sendMail){
        if($from == "MRP")
            $flag = @mail($email, $subject, $content, $headers);
        else
		    $flag = @mail($email, $subject, $header."<br><br>".$content."<br><br>".$footer, $headers);
	}
	return $flag;
}



/************************************************************/
/*Code By :			Kundan Burnwal				Date: 06-July-2011								*/
/*Function Name :	function sendMessageInlineBody($content, $subject, $mailto, $from='', $bcc='', $cc='', $external_mail = true)*/
/*Parameter :		content, subject, args, to, email address, message name, arguments							*/
/************************************************************/

function sendMessageInlineBody($content, $subject, $mailto, $from='', $bcc='', $cc='', $external_mail = true)
{
	global $sql_tbl, $xcart_dir, $weblog;
	$cashbackMessage = "";
	$weblog->debug("Send Mail : send mail for body with header");
	$headerFooter = getEmailHeaderAndFooter($from == "MRP");
	$header = $headerFooter['header'];
	$footer = $headerFooter['footer'];
	$sendMail = true; 
	
	if(empty($content)){
        	$sendMail = false;
        	$flag = @mail("alrt_templatenotavailable@myntra.com", "Empty Mail Content(Body) in argument","Mail body (content) is empty","From: Myntra Customer Service <support@myntra.com> \n");
    }else{
		$email = trim($mailto);
		$content .= "\r\n";
		$headers = "Content-Type: text/html; charset=ISO-8859-1\r\n";
	    if(!empty($from))
	    	$headers .= "From: ".$from." <support@myntra.com>\r\n";
	    else
	        $headers .= "From: Myntra Customer Service <support@myntra.com>\r\n";
        if($external_mail)
			$headers .= "Bcc: myntramailarchive@gmail.com\r\n";
		if(!empty($cc))
			$headers .= "Cc: $cc\r\n";
    }
    $weblog->debug("Send Mail : send mail with contents - ".$content);
    //The entire mail body should be in a fixed width div of 960px, so that, headers, footers, content etc are displayed properly
    $bodyInclHdrFtr = "<div style='width:960px'>".$header.$content.$footer."</div>";
    $flag = @mail($email, $subject, $bodyInclHdrFtr, $headers);
	return $flag;
}


function sendMessageQuoteRequest($template, $args, $mailto,$from='', $subjectarr,$bcc='')
{
	global $sql_tbl, $xcart_dir;
	$tb_email_notification  = $sql_tbl["mk_email_notification"];
    $sendMail = true; 
	$template_query = "SELECT * FROM $tb_email_notification WHERE name = '$template'";
	$template_result = db_query($template_query);
	$row = db_fetch_array($template_result);

	$subject = $row['subject']; 
	$content = $row['body']; ########### load content for the #template
	#
	#Replace subject key with values
	#
	$subjectkeys = array_keys($subjectarr);
	for($i = 0; $i < count($subjectkeys); $i++)
	{
		$key = $subjectkeys[$i];
		$subject = str_replace("[".$key."]",$subjectarr[$key],$subject);
	}
    if(empty($content)){
        	$sendMail = false;
        	########### Mail empty mail template to Ashutosh ######
        	$flag = @mail("alrt_templatenotavailable@myntra.com", "Empty Mail Content template","The template is not available in our DB : ".$template,"From: Myntra Customer Service <support@myntra.com> \n");
    }else{
		##$content = $row['body'];
		$email = trim($mailto);
	
		$keys = array_keys($args);
		$key ;
	
		for($i = 0; $i < count($keys); $i++)
		{
			$key = $keys[$i];
			$content = str_replace("[".$key."]",$args[$key],$content);
		}
		if($row['from_name'] != "" & $row['from_designation'] != "")
		{
			$content = str_replace("[FROM_NAME]", $row['from_name'],$content);
			$content = str_replace("[FROM_DESIGNATION]", $row['from_designation'],$content);
		}
			$content .= "<br/><br/>";
			
		if(file_exists($xcart_dir."/log"))
		{
			$myFile = "log/log.txt";
			$fh = @fopen($myFile, 'a+') ;//or die("can't open file");
			$stringData = $template." ---- ".$mailto."---------------".date('d/m/Y')."\n";
			fwrite($fh, $stringData);
			fclose($fh);
		}
		else
		{
			mkdir($xcart_dir."/log", 0777);
			$ourFileName = $xcart_dir."/log/log.txt";
			$fh = fopen($ourFileName, 'w') ;//or die("can't open file");
			$stringData = $template." ---- ".$mailto."---------------".date('d/m/Y')."\n";
			fwrite($fh, $stringData);
			fclose($fh);
		}
			
			$headers = "Content-Type: text/html; charset=ISO-8859-1 " . "\n";
			$headers .= "From: Myntra Customer Service <support@myntra.com>" . "\n";
			$headers .= "Cc: sales@myntra.com" . "\n";
			$headers .= "Bcc: myntramailarchive@gmail.com" . "\n";
    }
	if($sendMail){	
		$flag = @mail($email, $subject, $content,$headers);
	}
	return $flag;

}
	

function func_send_mail_async($to, $cc,$subject, $body, $from){
	$array=array("queuedtime"=>date("Y/m/d H:i:s"),'sent'=>1);
	$id=func_array2insert("myntra_notification_message_details", $array);
	$array=array("email_details_id"=>$id,"mode"=>1,"sendto"=>$to,"cc"=>$cc,"bcc"=>"myntramailarchive@gmail.com",'sender'=>$from,'subject'=>mysql_escape_string($subject),'mimetype'=>'HTML','body'=>mysql_escape_string($body));
	func_array2insert("myntra_notification_email_details", $array);
}


function func_send_mail_with_attachment($to, $subject, $body, $ccList='', $from='', $filepath, $filename) {
	$m = new MAIL;
	if($from == '') 
		$from = 'Myntra Customer Care';
		
	$m->From('support@myntra.com', $from);
	$m->AddTo($to, '');
	if($ccList != '') {			
		foreach($ccList as $cc){
			$m->addcc($cc, '');
		}
	}
	
	// set subject
	$m->Subject($subject);
	$m->Html($body);
	$xpmFunc=new FUNC5();
	$xpmMime= new MIME5();
	$m->Attach(file_get_contents($filepath.$filename),  $xpmFunc->mime_type($filename), $filename, null, null, 'inline',$xpmMime->unique());
			
	$mailresult=$m->Send('client') ? true : false;
	
	return $mailresult;
}

/************************************************************/

?>
