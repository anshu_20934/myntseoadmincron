<?php
function send_customer_enquiry($name, $cnumber, $contact_email, $timelines, $enquiry_details){
    
    $headers = "Content-Type: text/html; charset=ISO-8859-1 " . "\n";
	$headers .= "From: ". $name . "<noreply@myntra.com>" . "\n";
    $headers .= "Reply-to: ". $name . "<" . $contact_email . ">" . "\n";
    $email = "sales@myntra.com";
    //$email = "nikhil.gupta@myntra.com";
    $subject = "New lead for bulk order";

    $content = "Dear Sales,"."<br/><br/>";
    $content .= "<p>We have received a new lead for bulk order. Please see below for details and contact immediately. </p>"."<br/><br/>";
    $content .= "<b>Customer Name : </b>". $name . "<br/>";
    $content .= "<b>Contact Number : </b>". $cnumber . "<br/>";
    $content .= "<b>Contact E-mail : </b>". $contact_email . "<br/>";
    $content .= "<b>Timeline : </b>". $timelines . "<br/>";
    $content .= "<b>Customer Enquiry : </b>". $enquiry_details . "<br/>";

    $flag = @mail($email, $subject, $content,$headers);

    return $flag;
}
?>