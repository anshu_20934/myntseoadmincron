<?php

include_once($xcart_dir . "/include/func/func.courier_old.php");
include_once($xcart_dir . "/modules/apiclient/PincodeApiClient.php");
include_once("$xcart_dir/modules/apiclient/CourierApiClient.php");
include_once("$xcart_dir/include/class/oms/OrderWHManager.php");
include_once("$xcart_dir/modules/apiclient/ServiceabilityApiClient.php");

function get_courier_by_code($code) {
    global $sql_tbl;
    $sql = "SELECT * FROM $sql_tbl[courier_service] where code = '$code'";
    $courier = func_query_first($sql, true);
    return $courier;
}

function get_courier_display_name($code) {
    if (!$code || empty($code))
        return "";

    global $sql_tbl;
    $sql = "SELECT courier_service as display_name FROM $sql_tbl[courier_service] WHERE code='$code'";
    $courier_row = func_query_first($sql, true);
    if ($courier_row)
        return $courier_row['display_name'];

    return $code;
}

function get_all_courier_partners() {
    global $sql_tbl;
    $sql = "SELECT code, courier_service as display_name FROM $sql_tbl[courier_service]";
    $couriers = func_query($sql, true);
    return $couriers;
}

function get_active_courier_partners() {
    return CourierApiClient::getActiveCouriers();
}

function get_pickup_courier_partners() {
    $courierDetails = CourierApiClient::getAllCourierDetails();
    $return_couriers = array();
    if ($courierDetails) {
        foreach ($courierDetails as $courier) {
            if (isset($courier['serviceTypes'])) {
                $serviceTypes = $courier['serviceTypes']['serviceType'];
                if (!isset($serviceTypes[0]))
                    $serviceTypes = array($serviceTypes);
                foreach ($serviceTypes as $serviceType) {
                    if ($serviceType['name'] == 'isPickup' && $serviceType['value'] == 1)
                        $return_couriers[] = array('code' => $courier['code'], 'display_name' => $courier['name']);
                }
            }
        }
    }
    return $return_couriers;
}

function get_supported_courier_partners() {
    return CourierApiClient::getAllCouriers();
}

/**
 * used in new address screen for delivery
 */
function is_zipcode_servicable_delivery($zipcode) {
    global $courierServiceabilityVersion;
    $codAllZipCodeEnable = FeatureGateKeyValuePairs::getBoolean('codEnableAllZipCode', false);
    if ($courierServiceabilityVersion == 'new') {
        $serviceability_details = ServiceabilityApiClient::getServiceabilityDetailsForZipcode($zipcode);
        if ($serviceability_details && !empty($serviceability_details)) {
            foreach ($serviceability_details as $index => $row) {
                if (isset($row['serviceTypes'])) {
                    $serviceTypes = $row['serviceTypes']['serviceType'];
                    if (!isset($serviceTypes[0]))
                        $serviceTypes = array($serviceTypes);
                    foreach ($serviceTypes as $serviceType) {
                        if ($codAllZipCodeEnable || (($serviceType['name'] == 'isCOD' || $serviceType['name'] == 'isOnline') && $serviceType['value'] == true)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    } else {
        return is_zipcode_servicable($zipcode);
    }
}

function is_zipcode_servicable_cod($zipcode) {
    global $courierServiceabilityVersion;
    $codAllZipCodeEnable = FeatureGateKeyValuePairs::getBoolean('codEnableAllZipCode', false);
    if ($courierServiceabilityVersion == 'new') {
        $serviceability_details = ServiceabilityApiClient::getServiceabilityDetailsForZipcode($zipcode);
        if ($serviceability_details && !empty($serviceability_details)) {
            foreach ($serviceability_details as $index => $row) {
                if (isset($row['serviceTypes'])) {
                    $serviceTypes = $row['serviceTypes']['serviceType'];
                    if (!isset($serviceTypes[0]))
                        $serviceTypes = array($serviceTypes);
                    foreach ($serviceTypes as $serviceType) {
                        if ($codAllZipCodeEnable || ($serviceType['name'] == 'isCOD' && $serviceType['value'] == true)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    } else {
        return func_check_cod_serviceability($zipcode, $codAllZipCodeEnable);
    }
}

/**
 * used only in courier preference page... 
 */
function func_get_serviceable_couriers_for_zipcode($zipcode) {
    global $courierServiceabilityVersion;
    if ($courierServiceabilityVersion == 'new') {
        $serviceability_details = ServiceabilityApiClient::getServiceabilityDetailsForZipcode($zipcode);

        foreach ($serviceability_details as $index => $row) {
            $supported_courier_results[$index]['courierCode'] = $row['code'];
            $supported_courier_results[$index]['courierName'] = $row['name'];

            if (isset($row['serviceTypes'])) {
                $serviceTypes = $row['serviceTypes']['serviceType'];
                if (!isset($serviceTypes[0]))
                    $serviceTypes = array($serviceTypes);

                foreach ($serviceTypes as $serviceType) {
                    if ($serviceType['name'] == 'isOnline') {
                        $supported_courier_results[$index]['nonCodSupported'] = $serviceType['value'];
                    }
                    if ($serviceType['name'] == 'isCOD') {
                        $supported_courier_results[$index]['codSupported'] = $serviceType['value'];
                    }
                    if ($serviceType['name'] == 'isPickup') {
                        $supported_courier_results[$index]['pickupSupported'] = $serviceType['value'];
                    }
                    if ($serviceType['name'] == 'isExchange') {
                        $supported_courier_results[$index]['exchangeSupported'] = $serviceType['value'];
                    }
                    if ($serviceType['name'] == 'isHazmat') {
                        $supported_courier_results[$index]['hazmatSupported'] = $serviceType['value'];
                    }
                    if ($serviceType['name'] == 'isFragile') {
                        $supported_courier_results[$index]['fragileSupported'] = $serviceType['value'];
                    }
                    if ($serviceType['name'] == 'isJewellery') {
                        $supported_courier_results[$index]['jewellerySupported'] = $serviceType['value'];
                    }
                    if ($serviceType['name'] == 'isComforter') {
                        $supported_courier_results[$index]['comforterSupported'] = $serviceType['value'];
                    }
                    if ($serviceType['name'] == 'codLimit') {
                        $supported_courier_results[$index]['codLimit'] = $serviceType['value'];
                    }
                    if ($serviceType['name'] == 'isExpress') {
                        $supported_courier_results[$index]['expressSupported'] = $serviceType['value'];
                    }
                }
            }
        }
        return $supported_courier_results;
    } else {
        return func_get_serviceable_couriers_for_zipcode_old($zipcode);
    }
}

/**
 * used for serviceability widget
 * also used internally for checkout flow serviceability checks
 */
function getServiceablePaymentModesForProducts($zipcode, $productsInCart, $paymentMethod = false, $warehouseid = false, $skuDetails = false) {
    global $courierServiceabilityVersion;
    if ($courierServiceabilityVersion == 'new') {
        $serviceable_payment_modes = false;
        $requestData = constructRequestObjectForServiceabilityCheck($productsInCart, $zipcode, 'DELIVERY', $paymentMethod, $warehouseid, $skuDetails);
        if ($requestData) {
            $serviceabilityResponse = ServiceabilityApiClient::getServiceabilityDetails($requestData);
            if ($serviceabilityResponse && !empty($serviceabilityResponse) &&  !(is_string($serviceabilityResponse) && strpos($serviceabilityResponse,'ERROR :') !== false) ) {
                foreach ($serviceabilityResponse as $skuId => $paymentModes) {
                    if (!$paymentModes || empty($paymentModes)) {
                        $serviceable_payment_modes = array();
                    } else {
                        if ($serviceable_payment_modes === false) {
                            $serviceable_payment_modes = $paymentModes;
                        } else {
                            $serviceable_payment_modes = array_intersect($serviceable_payment_modes, $paymentModes);
                        }
                    }
                }
            } else {
                $serviceable_payment_modes = array();
            }
            $codAllZipCodeEnable = FeatureGateKeyValuePairs::getBoolean('codEnableAllZipCode', false);
            if ($codAllZipCodeEnable)
                $serviceable_payment_modes[] = 'cod';

            $serviceable_payment_modes = array_values(array_unique($serviceable_payment_modes));
        }
        return $serviceable_payment_modes;
    } else {
        return func_get_serviceable_payment_modes_for_zipcode($zipcode);
    }
}

/**
 * used in checkout flow
 */
function func_check_cod_serviceability_products($zipcode, $productsInCart, $jewelleryItemsInOrder, $warehouseid = false, $skuDetails = false) {
    global $courierServiceabilityVersion;
    if ($courierServiceabilityVersion == 'new') {
        $serviceable_payment_modes = getServiceablePaymentModesForProducts($zipcode, $productsInCart, 'cod', $warehouseid, $skuDetails);
        if (!empty($serviceable_payment_modes) && in_array('cod', $serviceable_payment_modes))
            return true;

        return false;
    } else {
        $codAllZipCodeEnable = FeatureGateKeyValuePairs::getBoolean('codEnableAllZipCode', false);
        return func_check_cod_serviceability($zipcode, $codAllZipCodeEnable, $jewelleryItemsInOrder);
    }
}

/**
 * used in checkout flow
 */
function func_check_prepaid_serviceability_products($zipcode, $productsInCart, $warehouseid = false) {
    $serviceable_payment_modes = getServiceablePaymentModesForProducts($zipcode, $productsInCart, 'cod', $warehouseid);
    if (!empty($serviceable_payment_modes) && in_array('on', $serviceable_payment_modes))
        return true;

    return false;
}

function func_get_pickup_couriers($zipcode, $styleId, $skuId) {
    global $courierServiceabilityVersion;
    if ($courierServiceabilityVersion == 'new') {
        $productsInCart[0]['sku_id'] = $skuId;
        $productsInCart[0]['style_id'] = $styleId;
        $couriers = array();
        $requestData = constructRequestObjectForServiceabilityCheck($productsInCart, $zipcode, 'PICKUP', 'on');
        if ($requestData) {
            $serviceabilityResponse = ServiceabilityApiClient::getServiceabilityDetails($requestData, 'on');
            foreach ($serviceabilityResponse as $skuId => $whId2CourierCode2LimitRuleMap) {
                foreach ($whId2CourierCode2LimitRuleMap as $whId => $courier2LimitRuleMap) {
                    $couriers = array_merge($couriers, array_values(array_keys($courier2LimitRuleMap)));
                }
            }
        }
        return array_values(array_unique($couriers));
    } else {
        return func_check_pickup_availability($zipcode);
    }
}

function func_check_exchange_serviceability_products($zipcode, $productsInCart, $warehouseids = false, $paymentMethod = 'on') {
    global $courierServiceabilityVersion;
    if ($courierServiceabilityVersion == 'new') {
        $requestData = constructRequestObjectForServiceabilityCheck($productsInCart, $zipcode, 'EXCHANGE', $paymentMethod, $warehouseids);
        $serviceable_payment_modes = false;
        if ($requestData) {
            $serviceabilityResponse = ServiceabilityApiClient::getServiceabilityDetails($requestData);
            if ($serviceabilityResponse && !empty($serviceabilityResponse) && !(is_string($serviceabilityResponse) && strpos($serviceabilityResponse,'ERROR : ') !== false)) {
                foreach ($serviceabilityResponse as $skuId => $paymentModes) {
                    if (!$paymentModes || empty($paymentModes)) {
                        $serviceable_payment_modes = false;
                    } else {
                        if ($serviceable_payment_modes === false) {
                            $serviceable_payment_modes = $paymentModes;
                        } else {
                            $serviceable_payment_modes = array_intersect($serviceable_payment_modes, $paymentModes);
                        }
                    }
                }
            }
            if ($serviceable_payment_modes && !empty($serviceable_payment_modes) && count($serviceable_payment_modes) > 0)
                return true;
        }
        return false;
    } else {
        $couriers = func_check_exchange_serviceability($zipcode);
        if ($couriers && count($couriers) > 0)
            return true;
        else
            return false;
    }
}

function func_get_order_serviceabile_courier($paymentMethod, $zipcode, $productsInCart, $warehouseId, $orderTotal, $serviceType = 'DELIVERY') {
    global $courierServiceabilityVersion;
    if ($courierServiceabilityVersion == 'new') {
        $requestData = constructRequestObjectForServiceabilityCheck($productsInCart, $zipcode, $serviceType, $paymentMethod, $warehouseId, false, false, $orderTotal);
        if ($requestData) {
            $serviceabilityResponse = ServiceabilityApiClient::getServiceabilityDetails($requestData, $paymentMethod);
            if ($serviceabilityResponse && !empty($serviceabilityResponse) && !(is_string($serviceabilityResponse) && strpos($serviceabilityResponse,'ERROR : ') !== false)) {
                foreach ($serviceabilityResponse as $skuId => $whId2Courier2LimitRule) {
                    $courierRuleMap = $whId2Courier2LimitRule[$warehouseId];
                    if (!empty($courierRuleMap))
                        return array_keys($courierRuleMap);
                }
            }
        }
        return false;
    } else {
        return func_get_serviceable_couriers($zipcode, $paymentMethod, 'IN');
    }
}

function getCourierServiceabilityForProdcut($zipcode, $productsInCart, $skuDetails = false) {
    $requestData = constructRequestObjectForServiceabilityCheck($productsInCart, $zipcode, 'DELIVERY', false, false, $skuDetails);
    if ($requestData) {
        $serviceabilityResponse = ServiceabilityApiClient::getWarehouseCourierServiceability($requestData);
        return $serviceabilityResponse;
    }
    return false;
}

function constructRequestObjectForServiceabilityCheck($productsInCart, $zipcode, $serviceType = 'DELIVERY', $paymentMode = false, $warehouseids = false, $skuDetails = false, $checkForPrice = true, $orderPrice = false) {
    $sku_style_map = array();
    foreach ($productsInCart as $product) {
        $sku_style_map[$product['sku_id']] = $product['style_id'];
    }
    $style_tags_map = getTagsForStyle(array_values($sku_style_map));
    $order = array('pincode' => $zipcode, 'serviceType' => $serviceType);
    if ($paymentMode)
        $order['paymentMode'] = strtolower($paymentMode);

    if ($serviceType != 'PICKUP') {

        if (!$warehouseids) {
            if (!$skuDetails) {
                $skuIds = array();
                foreach ($productsInCart as $product) {
                    $skuIds[] = $product['sku_id'];
                }
                $skuDetails = OrderWHManager::loadWarehouseInvCounts($skuIds);
            }

            if (!$skuDetails)
                return false;
            else {
                foreach ($productsInCart as $product) {
                    $skuDetail = $skuDetails[$product['sku_id']];
                    $item = array('skuId' => $product['sku_id']);
                    foreach ($skuDetail['warehouse2AvailableItems'] as $whId => $invCount) {
                        if ($invCount > 0) {
                            $item['warehouses'][] = array('warehouseId' => $whId);
                        }
                    }
                    if (empty($item['warehouses']))
                        return false;

                    $style_tags = $style_tags_map[$product['style_id']];
                    foreach ($style_tags as $style_tag) {
                        $item['itemTypes'][] = array('itemType' => $style_tag);
                    }
                    if ($checkForPrice && ($paymentMode == false || strtolower($paymentMode) == 'cod')) {
                        if (!$product['finalPrice'])
                            $finalPrice = ($product['totalProductPrice'] - $product['discount'] - $product['couponDiscount'] - $product['cartDiscount'] - $product['totalMyntCashUsage']) / $product['quantity'];
                        else
                            $finalPrice = $product['finalPrice'];
                        $item['itemTypes'][] = array('itemType' => array('name' => 'codValue', 'value' => $finalPrice, 'dataType' => 'NUMBER'));
                    }
                    $order['items'][] = array('item' => $item);
                }
            }
        }else {
            if (!is_array($warehouseids))
                $warehouseids = array($warehouseids);

            foreach ($productsInCart as $product) {
                $item = array();
                $item['skuId'] = $product['sku_id'];
                foreach ($warehouseids as $whId) {
                    $item['warehouses'][] = array('warehouseId' => $whId);
                }
                $style_tags = $style_tags_map[$product['style_id']];
                foreach ($style_tags as $style_tag) {
                    $item['itemTypes'][] = array('itemType' => $style_tag);
                }
                // For already placed order.. address change.. reassign warehouse.. order split
                if ($orderPrice !== false) {
                    $item['itemTypes'][] = array('itemType' => array('name' => 'codValue', 'value' => $orderPrice, 'dataType' => 'NUMBER'));
                } else {
                    if ($checkForPrice && ($paymentMode == false || strtolower($paymentMode) == 'cod')) {
                        $finalPrice = ($product['totalProductPrice'] - $product['discount'] - $product['couponDiscount'] - $product['cartDiscount'] - $product['totalMyntCashUsage']) / $product['quantity'];
                        $item['itemTypes'][] = array('itemType' => array('name' => 'codValue', 'value' => $finalPrice, 'dataType' => 'NUMBER'));
                    }
                }
                $order['items'][] = array('item' => $item);
            }
        }
    } else {
        foreach ($productsInCart as $product) {
            $item = array();
            $item['skuId'] = $product['sku_id'];
            $item['warehouses'] = array();
            $order['items'][] = array('item' => $item);
        }
    }

    $requestObject = array();
    $requestObject['data'][] = array('order' => $order);
    return $requestObject;
}

?>
