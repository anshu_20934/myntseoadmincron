<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: func.db.php,v 1.9.2.2 2006/06/23 07:14:18 max Exp $
#
ini_set('memory_limit', '1024M');
if ( !defined('XCART_START') ) { header("Location: ../"); die("Access denied"); }

include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");
include_once("$xcart_dir/Profiler/Profiler.php");
require_once "$xcart_dir/include/func/func.utilities.php";
#
# Database abstract layer functions
#
function db_connect($sql_host, $sql_user, $sql_password) {
	return mysql_connect($sql_host, $sql_user, $sql_password, TRUE);
}

function db_select_db($sql_db, $connection) {
	return mysql_select_db($sql_db, $connection);
}

/**
 * 
 * Execute a given SQL Query in batches of batchSize
 * For each row of the query, execute callBackFunction with the entire row
 * as argument (which is an associative array). Each row returned from callback function
 * is concatenated to form an array and returned
 * 
 * @param $sql
 * @param $batchSize
 * @param $func
 * @author Kundan 
 *
function sqlBatchQuery($sql, $callBackFunction, $batchSize = 500) {
	if(!function_exists($callBackFunction)) {
		throw new Exception(__FILE__.":".__LINE__.": could not find callback function: $callBackFunction");
	}
	$map = Array();
	$row = true;
	for($i = 0; $row; $i++) {
		$low = $i*$batchSize;
		$sqlToRun = $sql." limit $low,$batchSize ";
		$rset = db_query($sqlToRun, TRUE);
		while($row = mysql_fetch_assoc($rset)) {
			$ret = call_user_func($callBackFunction, $row);
			if(!empty($ret)){
				$map[] = $ret;
			}
		}
	}
	if(!empty($map)) {
		return $map;
	}
}
*/
/**
 * Performs a select query and makes it into a 2-level associative array.
 * At the first level, the key is equal to the primary key column value for this row
 * The value for this key is an associative array, where keys are respective column names
 * e.g. given the query: "select orderid, login, firstname, mobile from xcart_orders" given the primary_key_col_name as "orderid"
 * This will return an associative array of the form:
 *
 * Array("12334"=>array("login"=>"kundan.burnwal@myntra.com", "firstname"=>"Kundan Burnwal", "mobile"=>"9972597888"),
 * 		 "90903"=>array("login"=>"yogesh.pandey@myntra.com", "firstname"=>"Yogesh Pandey", "mobile"=>"9972597899"),
 *  );
 *  Note: the primaryKeyColName must be a part of the select sql query, else the behavior is unpredictable
 *  The first column must be primary key or at least unique across the resultset
 */
function getResultsetAsPrimaryKeyValuePair($sql, $primaryKeyColName, $singleValueColumnName="",$use_ro=false) {
	$ret;
	$result = db_query($sql, $use_ro);
	if(!empty($singleValueColumnName)){
		while($row = db_fetch_array($result)){
			$ret[$row[$primaryKeyColName]] = $row[$singleValueColumnName];
		}	
	}
	else {
		while($row = db_fetch_row($result)){
			$ret[$row[0]] = $row;
		}
	}
	db_free_result($result);
	return $ret;
}
/**
 * Get only the 1st column of the resultset of the array as a single column
 * e.g. if the result set for the sql returned:
 * 200101044 Kundan
 * 200201001 Burnwal
 * this function will return: array("200101044","200201001")
 * @param unknown_type $sql
 * @param unknown_type $use_ro
 * @return array
 */
function getSingleColumnAsArray($sql, $use_ro = false){
	$ret;
	$result = db_query($sql, $use_ro);
	while($row = db_fetch_row($result)){
		$ret[] = $row[0];
	}
	db_free_result($result);
	return $ret;
}

function init_db_connections(){
    global $rw_connection,$ro_connection,$xcart_dir;
    if(!empty($ro_connection) && !empty($rw_connection)){
        return;
    }
    #
    # Connect to database.
    # We check whether the constant 'REPORTS_CONNECTION' is defined. If yes, we open the reportsro connection i.e., to secondary slave
    # Otherwise, we create a ro connection , i.e. to primary slave
    # In case ro connection fails we fallback to using rw connection parameters for ro connection
    # Connect to rw connection at the last to enable mysql_* functions use this connection by default
    #
    if (defined("REPORTS_CONNECTION")){
        $ro_connection = mysql_connect(DBConfig::$reportsro['host'], DBConfig::$reportsro['user'], DBConfig::$reportsro['password'],TRUE);
        if (mysql_select_db(DBConfig::$reportsro['db'], $ro_connection ) == false){
            echo "reportsro connection cannot be established \n";
            exit;
        }
    }
    else {
        $ro_connection = mysql_connect(DBConfig::$ro['host'], DBConfig::$ro['user'], DBConfig::$ro['password'],TRUE);
    }
    $rw_connection = mysql_connect(DBConfig::$rw['host'], DBConfig::$rw['user'], DBConfig::$rw['password']);
    if(mysql_select_db(DBConfig::$rw['db'], $rw_connection ) == false){
        Profiler::increment('db-rw-fail');
        header('HTTP/1.1 500 Internal Server Error');
        //@mail(EmailListsConfig::$dbConnectionFailure, 'CRITICAL : Portal database not accessible - RW', 'Failed while initializing read write db connections - '.$_SERVER['REQUEST_URI']);
        include($xcart_dir."/maintenance.html");
        exit;
    }
    if(mysql_select_db(DBConfig::$ro['db'], $ro_connection ) == false){
        Profiler::increment('db-ro-fail');
        $ro_connection = $rw_connection;
    }

}

/**
 * @param  $query - CRUD query
 * @param bool $use_ro - true , for admin/expensive queries which need not be real time (maximum of 1 minute delay can be expected )
 * @return bool|resource
 */
function db_query($query, $use_ro = false) {
    global $ro_connection,$rw_connection;
	if(check_injection_attack($query)){
		return null;
	}
    init_db_connections();
	global $debug_mode;
	global $mysql_autorepair, $sql_max_allowed_packet;
	if (defined("START_TIME")) {
		global $__sql_time;
		$t = func_microtime();
	}
	if ($sql_max_allowed_packet && strlen($query) > $sql_max_allowed_packet)
		return false;

	__add_mark();

    if($use_ro) {
        // echo "Using RO";
        global $ro_connection;
        $profilerHandle = Profiler::startTiming("db-ro");
        mysql_set_charset('utf8',$ro_connection);
        $result = mysql_query($query, $ro_connection);
        Profiler::endTiming($profilerHandle);
    } else {
        // echo "Using RW";
        global $rw_connection ;
        $profilerHandle = Profiler::startTiming("db-rw");
        mysql_set_charset('utf8',$rw_connection);
        $result = mysql_query($query, $rw_connection);
        Profiler::endTiming($profilerHandle);
    }

	$t_end = func_microtime();
	if (defined("START_TIME")) {
		$__sql_time += func_microtime()-$t;
	}

	#
	# Auto repair
	#
	if (!$result && $mysql_autorepair && preg_match("/'(\S+)\.(MYI|MYD)/",mysql_error(), $m)) {
		$stm = "REPAIR TABLE $m[1] EXTENDED";
		error_log("Repairing table $m[1]", 0);
		if ($debug_mode == 1 || $debug_mode == 3) {
			$mysql_error = mysql_errno()." : ".mysql_error();
			echo "<b><font COLOR=DARKRED>Repairing table $m[1]...</font></b>$mysql_error<br />";
			flush();
		}

		$result = mysql_query($stm);
		if (!$result)
			error_log("Repaire table $m[1] is failed: ".mysql_errno()." : ".mysql_error(), 0);
		else
			$result = mysql_query($query); # try repeat query...
	}

	if (db_error($result, $query) && $debug_mode==1)
		exit;

	$explain = array();
	if (
		defined("BENCH") && constant("BENCH") &&
		!defined("BENCH_BLOCK") && !defined("BENCH_DISPLAY") &&
		defined("BENCH_DISPLAY_TYPE") && constant("BENCH_DISPLAY_TYPE") == "A" &&
		!strncasecmp("SELECT", $query, 6)
	) {
		$r = mysql_query('EXPLAIN '.$query);
		if ($r !== false) {
			while ($arr = db_fetch_array($r))
				$explain[] = $arr;

			db_free_result($r);
		}
	}

	__add_mark(array("query" => $query, "explain" => $explain), "SQL");

	return $result;
}

function db_result($result, $offset) {
	return mysql_result($result, $offset);
}

function db_fetch_row($result) {
	return mysql_fetch_row($result);
}

function db_fetch_array($result, $flag=MYSQL_ASSOC) {
	return mysql_fetch_array($result, $flag);
}

function db_fetch_field($result, $num = 0) {
	return mysql_fetch_field($result, $num);
}

function db_free_result($result) {
	@mysql_free_result($result);
}

function db_num_rows($result) {
	return mysql_num_rows($result);
}

function db_num_fields($result) {
	return mysql_num_fields($result);
}

function db_insert_id() {
	global $rw_connection;
	return mysql_insert_id($rw_connection);
}

function db_affected_rows() {
	return mysql_affected_rows();
}

function db_error($mysql_result, $query) {
	global $config, $login, $REMOTE_ADDR, $current_location;

	if ($mysql_result)
		return false;

	$mysql_error = mysql_errno()." : ".mysql_error();
	$msg  = "Site        : ".$current_location."\n";
	$msg .= "Remote IP   : $REMOTE_ADDR\n";
	$msg .= "Logged as   : $login\n";
	$msg .= "SQL query   : $query\n";
	$msg .= "Error code  : ".mysql_errno()."\n";
	$msg .= "Description : ".mysql_error();

	db_error_generic($query, $mysql_error, $msg);

	return true;
}

function db_error_generic($query, $query_error, $msg) {
	global $debug_mode, $config, $XCART_SESSION_VARS,$errorlog;
return;
	$email = false;

	if (@$config["Email_Note"]["admin_sqlerror_notify"]=="Y") {
		$email = array ($config["Company"]["site_administrator"]);
	}

	if ($debug_mode == 1 || $debug_mode == 3) {

		$email = "alrt_sqlerror@myntra.com";
		$login = $XCART_SESSION_VARS['login'];
		$subject = "Sql error occured in myntra.com";
		$ip = $_SERVER['REMOTE_ADDR'];

		$error = "User id:$login"."  "."IP address:$ip"."     ".htmlspecialchars($query_error)."   Query => ".htmlspecialchars($query);
		$body = trim($error);

		$flag = mail($email, $subject, $body, "From: admin@myntra.com\nContent-Type: text/html; charset=iso-8859-1");
		$errorlog->error($body);

		$errormessage = "We are experiencing traffic overload and won't be able to process your request at this moment. " .
						"We sincerely apologize for the inconvenience and request you to try again after some time. " .
						"If there is something else we can help you with, please send a mail to support@myntra.com. " .
						"We will get back to you within 24 hours. ";
		
		$call_number = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
		if($call_number) {
			$errormessage.= "You can also reach us on our helpline. " . $call_number;
		}
		$XCART_SESSION_VARS['errormessage'] = $errormessage;
		
		header("Location:mksystemerror.php");
        exit;

	}

	$do_log = ($debug_mode == 2 || $debug_mode == 3);

	if ($email !== false || $do_log)
		x_log_add('SQL', $msg, true, 1, $email, !$do_log);
}

function db_prepare_query($query, $params) {
	static $prepared = array();

	if (!empty($prepared[$query])) {
		$info = $prepared[$query];
		$tokens = $info['tokens'];
	}
	else {
		$tokens = preg_split('/((?<!\\\)\?)/S', $query, -1, PREG_SPLIT_DELIM_CAPTURE);

		$count = 0;
		foreach ($tokens as $k=>$v) if ($v === '?') $count ++;

		$info = array (
			'tokens' => $tokens,
			'param_count' => $count
		);
		$prepared[$query] = $info;
	}

	if (count($params) != $info['param_count']) {
		return array (
			'info' => 'mismatch',
			'expected' => $info['param_count'],
			'actual' => count($params));
	}

	$pos = 0;
	foreach ($tokens as $k=>$val) {
		if ($val !== '?') continue;

		if (!isset($params[$pos])) {
			return array (
				'info' => 'missing',
				'param' => $pos,
				'expected' => $info['param_count'],
				'actual' => count($params));
		}

		$val = $params[$pos];
		if (is_array($val)) {
			$val = func_array_map('addslashes', $val);
			$val = implode("','", $val);
		}
		else {
			$val = addslashes($val);
		}

		$tokens[$k] = "'" . $val . "'";
		$pos ++;
	}

	return implode('', $tokens);
}

#
# New DB API: Executing parameterized queries
# Example1:
#   $query = "SELECT * FROM table WHERE field1=? AND field2=? AND field3='\\?'"
#   $params = array (val1, val2)
#   query to execute:
#      "SELECT * FROM table WHERE field1='val1' AND field2='val2' AND field3='\\?'"
# Example2:
#   $query = "SELECT * FROM table WHERE field1=? AND field2 IN (?)"
#   $params = array (val1, array(val2,val3))
#   query to execute:
#      "SELECT * FROM table WHERE field1='val1' AND field2 IN ('val2','val3')"
#
# Warning:
#  1) all parameters must not be escaped with addslashes()
#  2) non-parameter symbols '?' must be escaped with a '\'
#
function db_exec($query, $params=array()) {
	global $config, $login, $REMOTE_ADDR, $current_location;

	if (!is_array($params))
		$params = array ($params);

	$prepared = db_prepare_query($query, $params);

	if (!is_array($prepared)) {
		return db_query($prepared);
	}

	$error = "Query preparation failed";
	switch ($prepared['info']) {
	case 'mismatch':
		$error .= ": parameters mismatch (passed $prepared[actual], expected $prepared[expected])";
		break;
	case 'missing':
		$error .= ": parameter $prepared[param] is missing";
		break;
	}

	$msg  = "Site        : ".$current_location."\n";
	$msg .= "Remote IP   : $REMOTE_ADDR\n";
	$msg .= "Logged as   : $login\n";
	$msg .= "SQL query   : $query\n";
	$msg .= "Description : ".$error;

	db_error_generic($query, $error, $msg);

	return false;
}

#
# Execute mysql query and store result into associative array with
# column names as keys
#
function func_query($query, $use_ro = false) {
	$result = false;
//echo($query);
//echo("--------------------------------------------");
	if ($p_result = db_query($query, $use_ro)) {
		while ($arr = db_fetch_array($p_result))
			$result[] = $arr;
		//print_r($result);
		db_free_result($p_result);
	}

	return $result;
}

#
# Execute mysql query and store result into associative array with
# column names as keys and then return first element of this array
# If array is empty return array().
#
function func_query_first($query, $use_ro = false) {
	if ($p_result = db_query($query, $use_ro )) {
		$result = db_fetch_array($p_result);
		db_free_result($p_result);
        }

        return is_array($result) ? $result : array();
}

#
# Execute mysql query and store result into associative array with
# column names as keys and then return first cell of first element of this array
# If array is empty return false.
#
function func_query_first_cell($query, $use_ro = false) {
	if ($p_result = db_query($query, $use_ro )) {
		$result = db_fetch_row($p_result);
		db_free_result($p_result);
	}

	return is_array($result) ? $result[0] : false;
}

function func_query_column($query, $column = 0, $use_ro = false) {
	$result = array();

	$fetch_func = is_int($column) ? 'db_fetch_row' : 'db_fetch_array';

	if ($p_result = db_query($query, $use_ro )) {
		while ($row = $fetch_func($p_result))
			$result[] = $row[$column];

		db_free_result($p_result);
	}

	return $result;
}

/**
 * This function takes care of both sanitizing DB values conditionally
 * as well as inserting an associative array in DB
 * for each key, the value should be either a scalar, or an array
 * If the value is a scalar, it is assumed to be a string and it is enclosed in quotes
 * If the value is an array, then it expects 2 keys:
 * 1)"value": which contains the actual value
 * 2)"type": which should be one of number, string, binary, function
 * Format of binary number is: b'1001'
 * Format of function is: now()
 * Only for string types, the value is escaped and it is enclosed in quotes. 
 * Note that numbers can be passed in the value for type = string
 * The arr input should be raw. It should not be pre-sanitized.
 * @param $tbl
 * @param $arr
 * @param $is_replace
 */
function func_array2insertWithTypeCheck($tbl, $arr, $is_replace = false) {
	global $sql_tbl;
	if (empty($tbl) || empty($arr) || !is_array($arr))
		return false;
	if (!empty($sql_tbl[$tbl]))
		$tbl = $sql_tbl[$tbl];

	if ($is_replace )
		$query = "REPLACE";
	else
		$query = "INSERT";
	foreach ( $arr as $arr_key=>$arr_value) {
		$arr[$arr_key] = conditionallyEncloseInQuotes($arr_value);
	}
	$query .= " INTO $tbl (" . implode(", ", array_keys($arr)) . ") VALUES (" . implode(", ", $arr) . ")";	
	$r = db_query($query);
	if ($r) {
		return db_insert_id();
	}
	return false;
}

function func_array2insertWithTypeCheck_log($tbl, $arr, $add_log = false, $add_fields) {
			
	add_tick($tbl, 0);
	$tbl_log = $tbl."_log";
	add_tick($tbl, 1); add_tick($tbl_log, 1);	
	
	$arr = add_tick_to_keys($arr, 1); $add_fields = add_tick_to_keys($add_fields, 1);	
	
	$insert_status = func_array2insertWithTypeCheck($tbl, $arr);
	
	if($add_log){
	
		$add_fields["time_id"] = time();		
		$add_fields["operation_type"] = "I";
		if(!$add_fields["`user_name`"]) $add_fields["`user_name`"] = "system";

		$log_arr = $arr;
		$log_array = add_tick_to_keys($log_arr, 0); $add_fields = add_tick_to_keys($add_fields, 0);		
		
		if(!empty($add_fields))
			$log_arr = array_merge($log_arr, $add_fields);
		
		add_tick($log_Array, 1);
		unset($log_arr["id"]);		
		func_array2insertWithTypeCheck($tbl_log, $log_arr);

                
	}	
	return $insert_status;
}


#
# Insert array data to table
#
/**
 * 
 * Insert an associative array in Database. This is deprecated and now func_array2insertWithTypeCheck should be used for this purpose
 * @param $tbl
 * @param $arr
 * @param $is_replace
 * @throws Exception
 * @deprecated
 */
function func_array2insert ($tbl, $arr, $is_replace = false) {
	global $sql_tbl;

	if (empty($tbl) || empty($arr) || !is_array($arr))
		return false;

	if (!empty($sql_tbl[$tbl]))
		$tbl = $sql_tbl[$tbl];

	if ($is_replace )
		$query = "REPLACE";
	else
		$query = "INSERT";
	foreach ( $arr as $arr_key=>$arr_value)
		$arr[$arr_key] = $arr_value;
	$query .= " INTO $tbl (" . implode(", ", array_keys($arr)) . ") VALUES ('" . implode("', '", $arr) . "')";

	$r = db_query($query);
	if ($r) {
		return db_insert_id();
	}

	return false;
}

#
# Update array data to table + where statament
#
/**
 * 
 /**
 * This function takes care of both sanitizing DB values conditionally
 * as well as updating an associative array in DB
 * for each key, the value should be either a scalar, or an array
 * If the value is a scalar, it is assumed to be a string and it is enclosed in quotes
 * If the value is an array, then it expects 2 keys:
 * 1)"value": which contains the actual value
 * 2)"type": which should be one of number, string, binary, function
 * Format of binary number is: b'1001'
 * Format of function is: now()
 * Only for string types, the value is escaped and it is enclosed in quotes. 
 * Note that numbers can be passed in the value for type = string
 * The arr input should be raw. It should not be pre-sanitized.
 * The $where clause could be both a string as well as an associative array
 * if it is an associative array, its format is the same as that of $arr
 * @param $tbl
 * @param $arr
 * @param $where
 */
function func_array2updateWithTypeCheck ($tbl, $arr, $where = '') {
	global $sql_tbl;

	if (empty($tbl) || empty($arr) || !is_array($arr))
		return false;

	if ($sql_tbl[$tbl])
		$tbl = $sql_tbl[$tbl];
	
	$updateSqlFragments = array();	
	foreach ($arr as $k => $v) {
		$updateSqlFragments[] = $k ."=".conditionallyEncloseInQuotes($v);		
	}
	$setSqlClause = implode(", ", $updateSqlFragments);
	
	if(empty($where)) {
		$whereSqlClause = "";
	}
	else if(!is_array($where)) {
		$whereSqlClause = $where;
	}
	else {
		$whereSqlFragments = array();
		foreach ($where as $key => $value) {
			$whereSqlFragments[] = (gettype($value) != "NULL") ? ($key."=".conditionallyEncloseInQuotes($value)) : $key." is null";
		} 
		$whereSqlClause = implode(" and ", $whereSqlFragments);
	}
	if(!empty($whereSqlClause)) {
		$whereSqlClause = " WHERE ".$whereSqlClause;
	}
	$updateQuery = "UPDATE $tbl SET ". $setSqlClause . $whereSqlClause;
	return db_query($updateQuery);
}

function func_array2updateWithTypeCheck_log ($tbl, $arr, $where = '', $add_log = false, $add_fields) {
	
	add_tick($tbl, 0);
	$tbl_log = $tbl."_log";
	add_tick($tbl, 1); add_tick($tbl_log, 1);

	if($add_log){		
	
		$arr = add_tick_to_keys($arr, 0); $where = add_tick_to_keys($where, 0); $add_fields = add_tick_to_keys($add_fields, 0);
	
		global $sql_tbl;

		if (empty($tbl) || empty($arr) || !is_array($arr))
			return false;

		if ($sql_tbl[$tbl])
			$tbl = $sql_tbl[$tbl];
			
		$whereSqlClause = getSQLWhereClause($where);
		$select_query = "SELECT * from $tbl " . $whereSqlClause;
		
		$select_result = db_query($select_query);	

		$curr_log_row = array();
		$insert_values = array();
		
		$add_fields["time_id"] = time();		
		$add_fields["operation_type"] = "U";
		if(!$add_fields["`user_name`"]) $add_fields["`user_name`"] = "system";
		
		while($tmp_arr = mysql_fetch_array($select_result, MYSQL_ASSOC)) {
				$curr_log_row = array_merge($tmp_arr, $arr);
				if(!empty($add_fields))		                
					$curr_log_row = array_merge($curr_log_row, $add_fields);
				
				unset($curr_log_row["id"]);
	
				foreach ( $curr_log_row as $arr_key=>$arr_value) {
                			$curr_log_row[$arr_key] = conditionallyEncloseInQuotes($arr_value);
        			}	
				$insert_values[] = "(". implode(", ", array_values($curr_log_row)) . ")"; 				   	

		}	

		$column_names = array_keys($curr_log_row);
		add_tick($column_names, 1);	
		$log_keys = "(". implode(", ", $column_names) .")";

		$log_insert_clause = "INSERT INTO $tbl_log ". $log_keys . " values ". implode(", ", $insert_values);	
		
		$arr = add_tick_to_keys($arr, 1); $where = add_tick_to_keys($where, 1); $add_fields = add_tick($add_fields, 1);	
				
		$update_status = func_array2updateWithTypeCheck($tbl, $arr, $where);	
		
		if($update_status){
			db_query($log_insert_clause);		
		}
		return $update_status;			
	}
	$arr = add_tick_to_keys($arr); $where = add_tick_to_keys($where); $add_fields = add_tick_to_keys($add_fields, 1);
	return func_array2updateWithTypeCheck($tbl, $arr, $where);
}


/**
 * 
 * Returns the sql query constructed when func_array2update gets called
 * @param $tbl 
 * @param $arr  
 * @param $where
 * This function does not result in database modification.
 */
function query_func_array2update($tbl, $arr, $where = '' ){
	global $sql_tbl;

	if (empty($tbl) || empty($arr) || !is_array($arr))
		return false;

	if ($sql_tbl[$tbl])
		$tbl = $sql_tbl[$tbl];

	foreach ($arr as $k => $v) {
		if (is_int($k)) {
			$r .= ($r ? ", " : "") . $v;
		} else {
			$r .= ($r ? ", " : "") . $k . "='" . $v . "'";
		}
	}

	$updateQuery = "UPDATE $tbl SET ". $r . ($where ? " WHERE " . $where : "");
	$updateQuery .= ";"; //end the query with semicolon
	return $updateQuery;
}

/**
 * 
 * Update array data to table + where statament.
 * This is derecated now and will soon be deleted.
 * This is replaced by func_array2updateWithTypeCheck
 * @param $tbl
 * @param $arr
 * @param $where
 * @deprecated
 */
function func_array2update ($tbl, $arr, $where = '') {
	global $sql_tbl;

	if (empty($tbl) || empty($arr) || !is_array($arr))
		return false;

	if ($sql_tbl[$tbl])
		$tbl = $sql_tbl[$tbl];

	foreach ($arr as $k => $v) {
		if (is_int($k)) {
			$r .= ($r ? ", " : "") . $v;
		} else {
			$r .= ($r ? ", " : "") . $k . "='" . $v . "'";
		}
	}

	$updateQuery = "UPDATE $tbl SET ". $r . ($where ? " WHERE " . $where : "");
	return db_query($updateQuery);
}

/**
 * Delete rows from a table with some where condition
 * @param unknown_type $tbl
 * @param unknown_type $where
 */
function func_array2deleteWithTypeCheck ($tbl, $where) {
	global $sql_tbl;

	if (empty($tbl) || empty($where))
	return false;

	if ($sql_tbl[$tbl])
	$tbl = $sql_tbl[$tbl];
	if(!is_array($where)) {
		$whereSqlClause = $where;
	}
	else {
		$whereSqlFragments = array();
		foreach ($where as $key => $value) {
			$whereSqlFragments[] = (gettype($value) != "NULL") ? ($key."=".conditionallyEncloseInQuotes($value)) : $key." is null";
		}
		$whereSqlClause = implode(" and ", $whereSqlFragments);
	}
	if(!empty($whereSqlClause)) {
		$whereSqlClause = " WHERE ".$whereSqlClause;
	}
	$deleteQuery = "DELETE FROM $tbl " . $whereSqlClause;
	db_query($deleteQuery);
	return db_affected_rows();
}

function func_query_hash($query, $column = false, $is_multirow = true, $only_first = false) {
	$result = array();
	$is_multicolumn = false;

	if ($p_result = db_query($query)) {
		if ($column === false) {

			# Get first field name
			$c = db_fetch_field($p_result);
			$column = $c->name;

		} elseif (is_array($column)) {
			if (count($column) == 1) {
				$column = current($column);

			} else {
				$is_multicolumn = true;
			}
		}

		while ($row = db_fetch_array($p_result)) {

			# Get key(s) column value and remove this column from row
			if ($is_multicolumn) {

				$keys = array();
				foreach ($column as $c) {
					$keys[] = $row[$c];
					func_unset($row, $c);
				}
				$keys = implode('"]["', $keys);

			} else {
				$key = $row[$column];
				func_unset($row, $column);
			}

			if ($only_first)
				$row = array_shift($row);

			if ($is_multicolumn) {

				# If keys count > 1
				if ($is_multirow) {
					eval('$result["'.$keys.'"][] = $row;');

				} else {
					eval('$is = isset($result["'.$keys.'"]);');
					if (!$is) {
						eval('$result["'.$keys.'"] = $row;');
					}
				}

			} elseif ($is_multirow) {
				$result[$key][] = $row;

			} elseif (!isset($result[$key])) {
				$result[$key] = $row;
			}
		}

		db_free_result($p_result);
	}

	return $result;
}

#
# Generate unique id
#  $type - one character
# Currently used types:
#  U - for users (anonymous)
#
function func_genid($type) {
	global $sql_tbl;

	db_query("INSERT INTO $sql_tbl[counters] (type) VALUES ('$type')");
	$value = db_insert_id();

	if ($value < 1)
		trigger_error("Cannot generate unique id", E_USER_ERROR);

	db_query("DELETE FROM $sql_tbl[counters] WHERE type='$type' AND value<'$value'");

	return $value;
}

/**
 * @param  $tbl
 * @param  $filters
 * @param  $start
 * @param int $count
 * @param  $orders
 * @return void
 */
function func_select_first($tbl, $filters = NULL, $start = 0, $count = 1, $orders = NULL) {
    $result = func_select_query($tbl, $filters , $start , $count , $orders );
    if(count($result) > 0){
        return $result [0] ;
    }
    return NULL;
}

/**
 * @param  $tbl
 * @param  $filters
 * @param  $start
 * @param int $count
 * @param  $orders
 * @return array
 */
function func_select_query($tbl, $filters = NULL, $start = NULL, $count = 10, $orders = NULL) {
    $sql = 'SELECT * FROM '.$tbl;
    if(is_array($filters)){
        $sql .=' WHERE ';
        $index = 0 ;
        foreach($filters as $key => $value){
            if($index > 0 ){
                $sql.= ' AND ';
            }
            $sql .= (gettype($value) != "NULL")?($key.'="'. $value.'"'):($key." is null");
            $index++;
        }
    }


    if(is_array($orders)){
        $sql .=' ORDER BY ';
        $index = 0 ;
        foreach($orders as $key => $value){
            if($index > 0 ){
                $sql.= ' , ';
            }
            $sql .= $key.' '. $value;
            $index++;
        }
    }

    if(isset($start) && $start >= 0 && isset($count) && $count >=0) {
        $sql.=' LIMIT '.$start.','.$count;
    }
    return execute_sql($sql) ;
}

function getSQLWhereClause($where){
	if(empty($where)) {
		$whereSqlClause = "";
	}
	else if(!is_array($where)) {
		$whereSqlClause = $where;
	}
	else {
		$whereSqlFragments = array();
		foreach ($where as $key => $value) {
			$whereSqlFragments[] = (gettype($value) != "NULL") ? ($key."=".conditionallyEncloseInQuotes($value)) : $key." is null";
		} 
		$whereSqlClause = implode(" and ", $whereSqlFragments);
	}
	if(!empty($whereSqlClause)) {
		$whereSqlClause = " WHERE ".$whereSqlClause;
	}
	return $whereSqlClause;
}

function getSQLOrderByClause($orders){
	if(is_array($orders)){
		$orderClasue = array();
		foreach ($orders as $key=>$value){
			$orderClasue[] .= $key." ".$value;
		}
		$orders = implode(",",$orderClasue);
	}
	return (!empty($orders))? (" ORDER BY ".$orders):"";
}

function getSQLLimitClause($start,$count){
	if(isset($start) && $start >= 0 && !empty($count)) {
		return "LIMIT ".$start.",".$count;
	}
	else {
		return "";
	}
}
/**
 * @param  $tbl
 * @param $columns: the columns to return
 * @param  $filters
 * @param  $start
 * @param int $count
 * @param  $orders
 * @return array
 */
function func_select_columns_query($tbl, $columns = "*", $filters = NULL, $start = NULL, $count = 10, $orders = NULL, $fetchMode="array") {
    if (is_array($columns)) {
    	$columns = implode(", ", $columns);
    }
	$sql = "SELECT $columns FROM ".$tbl;
    if(is_array($filters)){
        $sql .=' WHERE ';
        $index = 0 ;
        foreach($filters as $key => $value){
            if($index > 0 ){
                $sql.= ' AND ';
            }
            $sql .= (gettype($value) != "NULL")?($key.'="'. $value.'"'):($key." is null");
            $index++;
        }
    }


    if(is_array($orders)){
        $sql .=' ORDER BY ';
        $index = 0 ;
        foreach($orders as $key => $value){
            if($index > 0 ){
                $sql.= ' , ';
            }
            $sql .= $key.' '. $value;
            $index++;
        }
    }

    if(isset($start) && $start >= 0 && isset($count) && $count >=0) {
        $sql.=' LIMIT '.$start.','.$count;
    }
    return execute_sql($sql, $fetchMode) ;
}

/**
 * @param  $sql
 * @return
 */
function execute_sql_fetch_first($sql) {
    $results = execute_sql($sql);
    if(count($results) > 0){
        return $results [0] ;
    }
    return NULL;
}

/**
 * Kundan: introduced a new mode: row, and also: one-column-row
 * When it is row, each row will be an indexed(0 based,numeric) array containg all the projections
 * When it is onecolumnrow, each row will be only the 1st column value
 * @param  $sql
 * @param $mode array or assoc or row or onecolumnrow
 * @return array
 */
function execute_sql($sql, $mode="array") {
    $result = db_query($sql);
    $results = array();
	$fetchFunction;
	$oneColumnRow = false;
    switch($mode){
    	case "array": $fetchFunction = "mysql_fetch_array"; break;
    	case "assoc": $fetchFunction = "mysql_fetch_assoc"; break;
    	case "row"  : $fetchFunction = "mysql_fetch_row"; break;
    	case "onecolumnrow": $fetchFunction = "mysql_fetch_row"; $oneColumnRow = true; break;
    }
    while($row = $fetchFunction($result)){
	    $results[] = $oneColumnRow ? $row[0] : $row;
    } 
    return  count($results) > 0 ? $results : NULL;
}


/* result set based on sql
 * @param:(array)$filter - array of filters
 * @param:(array)$order - array for order by
 * @param:(array)$offset - start point of result set
 * @param:(array)$limit - count of records
 * @return:(array)$resultset
 */
function sanitizedQueryBuilder($sql, $filter=null, $order=null, $offset=null, $limit=null, $use_ro = false){
    if(!empty($filter)){
        sanitizeDBValues($filter);

        $sql .=' WHERE ';
        $index = 0 ;
        foreach($filter as $key => $value){
            if($index > 0 ){
                $sql.= ' AND ';
            }
            $sql .= $key.'="'. $value.'"';
            $index++;
        }
    }

    if(!empty($order)){
        sanitizeDBValues($order);
        $sql .=' ORDER BY ';
        $index = 0 ;
        foreach($order as $key => $value){
            if($index > 0 ){
                $sql.= ' , ';
            }
            $sql .= $key.' '. $value;
            $index++;
        }
    }

    if(isset($offset) && $offset >= 0) {
        $sql.=' LIMIT '.$offset.','.$limit;
    }    

    return $result = func_query($sql,$use_ro);
}

function check_injection_attack($query){
	if(stripos($query,"31303235343830303536")!==false){
		logSqlInjectionAttempt($query);
		return true;
	}
	if(stripos($query,"information_schema")!==false){
		logSqlInjectionAttempt($query);
		return true;
	}if(stripos($query,"696E666F726D6174696F6E5F736368656D61")!==false){
		logSqlInjectionAttempt($query);
		return true;
	}
}

function logSqlInjectionAttempt($query){
	global $fraudOrderLog;
	if($fraudOrderLog!=null){
		$fraudOrderLog->error("SQLINJECTION:$query");
	}
}

function add_tick_to_keys(&$arr, $add){
	foreach($arr as $key => $value){
		add_tick($key, $add);	
		$ret[$key] = $value;												
	}
	return $ret;
}

function add_tick(&$arr, $add){
	if(!is_array($arr)){
		if($add){
			if($arr[0]!='`')
			$arr = "`".$arr."`";
		}
		else{
			$arr = str_replace("`", "", $arr);
		}
		return ;
	}
	foreach($arr as &$key){
		if($add){
			if($key[0] != '`'){
				$key = "`".$key."`";
			}
		}
		else{
			$key = str_replace("`", "", $key);
		}
	}
}


?>
