<?php
if (!defined('XCART_START'))
 {
	 header ("Location: ../");
	die ("Access denied");
 }
	
include_once("func.mkimage.php");
include_once("func.mail.php");
include_once("func.crypt.php");
include_once("func.randnum.php");
require_once($xcart_dir.'/include/sessionstore/session_store_wrapper.php');


function func_autoregister_contest_user(&$register_info)
{
	//echo "<br/>in-autoregister";
	
	global  $config,$sql_tbl,$weblog;
	if(!empty($register_info['d_name'])){
		$firstname =  $register_info['d_name'];
	}

	/*if(!empty($lastname)){
		$lastname = $lastname;
	}*/

	/*if(empty($firstname) && empty($lastname)){
		$name = explode("@",$email);
		$firstname = $name[0];
		$lastname = $name[0];
	}*/

	$password  =get_rand_id(8);
	$email = mysql_real_escape_string($register_info['d_email']);
	$firstname = mysql_real_escape_string($firstname);

	######## Hold the values , which need to be inserted for the users
	$profile_values = array();
	$profile_values['login'] = $email;
	$profile_values['usertype'] = 'C';
	$crypted = addslashes(text_crypt($password));
	$profile_values['password'] = $crypted;
	$profile_values['firstname'] = $firstname;
	//$profile_values['lastname'] = $lastname;
	$profile_values['b_firstname'] = $firstname;
	//$profile_values['b_lastname'] = $lastname;
	$profile_values['s_firstname'] = $firstname;
	//$profile_values['s_lastname'] = $lastname;
	$profile_values['email'] =$email;
	$profile_values['termcondition'] = '1';
	$profile_values['addressoption'] = '';
	$profile_values['account_type'] = '';
	$profile_values['company'] = $register_info['d_college'];
	$profile_values['b_city'] = $register_info['d_city'];
	$profile_values['s_city'] = $register_info['d_city'];
	
	$weblog->info("Calling function func_array2insert to insert user credentials @line:".__LINE__." in file".__FILE__);
	$result = func_array2insert('customers', $profile_values);
	if($result !== false)
		return text_decrypt($profile_values['password']);
	else 
		return false;
}
 
function func_autologin_contest_user($email,$firstname='')
{
    global $XCARTSESSID,$sql_tbl,$identifiers , $userfirstname,$weblog;
    //echo "<br/>in-autologin".$XCARTSESSID;
    x_session_register("userfirstname");
	$_curtime = time();
   	#db_query("update mk_session set updated='Y' where sessionid= '$XCARTSESSID' ");
	updateSessionUpdatedFlagBySessionId($XCARTSESSID, 'Y');
  	db_query("UPDATE $sql_tbl[customers] SET last_login='$_curtime', first_login='$_curtime' WHERE login = '$email' ");
	
  	//$auto_login = true;
	
	$weblog->info("Auto login user >>> ".$email);
	$login_type = "C";
	x_session_register("identifiers",array());
	$identifiers[$login_type] = array (
		'login' => $email,
		'firstname' => $userfirstname,
		'login_type' => $login_type
	);				
}

function func_calculate_coupon_discount($uname,$discount='',$expiryDate='',$couponType='',$usage='',$peruser='',$minAmt='' , $productid=''){
	//echo "<br/>in-coupon";
	global $sql_tbl,$sqllog;
	$couponcode = get_rand_id(10);
	$expirydate = (!empty($expiryDate)) ? $expiryDate : time() + 14*24*60*60;
	$discount   = (!empty($discount)) ? $discount : 50 ;
	$couponType = (empty($couponType)) ? "percent" : "absolute" ;
	$usage      = (!empty($usage)) ? $usage : "1" ;
	$peruser    = (!empty($peruser)) ? 'Y' : "N" ;
	$productId  = (empty($productid)) ? '0' :  $productid;

	$minimumAmt = (empty($minAmt)) ? '0' :  $minAmt;
	$query = "INSERT INTO $sql_tbl[discount_coupons] (coupon, discount, groupid, coupon_type, minimum, times, per_user, expire, status, provider, productid, categoryid, recursive, apply_category_once, apply_product_once,freeship,styleid,producttypeid) VALUES ('$couponcode', '".$discount."', NULL, '".$couponType."', '".$minimumAmt."', '".$usage."', '".$peruser."', '$expirydate', 'A', 'myntraProvider', '".$productId."', '0', 'N', 'N', 'N', 'N', '0','2')";
	$sqllog->debug("Query ::".$query);
	$result = mysql_query($query);
	if(!$result){
		return false;	
	}else {
		return $couponcode;
	}	
}

?>