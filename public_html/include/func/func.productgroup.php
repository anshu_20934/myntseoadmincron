<?php
/**
 * @return #Ffunc_select_query|array|?
 */
function get_product_groups() {
    $product_groups = func_select_query('mk_product_group',array('is_active' => 'Y'), null, null, array('display_order' => 'asc'));
    $pga = array();
    foreach ($product_groups as $pg) {
        if ($pg['type'] == 2) {
            // nothing to do
        } else if ($pg['type'] == 1) {
            // Forward to product group ..
            $pg['url'] = trim($pg['name']) . '/pg/' . trim($pg['id']);
        } else if ($pg['type'] == 3) {
            //  unclear at this time  keep the same url
			$pg['url'] = trim($pg['name']) . '/pg/' . trim($pg['id']);
        }
        $pga [] = $pg;
    }
    $product_groups = $pga;
    return $product_groups;
}

function get_product_groups_for_portal() {
    $product_groups = func_select_query('mk_product_group',array('is_active' => 'Y'), null, null, array('display_order' => 'asc'));
    $pga = array();
    foreach ($product_groups as $pg) {
    	if($pg['url'] == 'romantic-gifts') {
    		// We don't modify url for romantic-gifts.
    	} else if($pg['default_style_id'] != 701 && $pg['default_style_id'] != 1163){
            $pg['url'] = 'create-my/' . str_replace(' ', '-', trim($pg['name'])) . '/' . trim($pg['default_style_id']);
        }
        $pga [] = $pg;
    }
    $product_groups = $pga;
    return $product_groups;
}

?>
