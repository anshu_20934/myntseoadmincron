<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: func.mail.php,v 1.11 2006/04/12 07:36:02 svowl Exp $
#

if ( !defined('XCART_START') ) { header("Location: ../"); die("Access denied"); }

x_load('files');
#
#This function returns all product types defined in the system with thier codes and names
#
function func_get_producttypes(){
$product_type_query="select id ,name from mk_product_type";
$commentresult=db_query($product_type_query);
$comments=array();
if ($commentresult)
	{
		$i=0;

		while ($row=db_fetch_array($commentresult))
		{
			$comments[$i] = $row;
			$i++;
		}
	}
	$searchresults=array();
	foreach($comments as $comment){
			$searchresults[$comment['id']]=$comment['name'];
	}
	return $searchresults;
}
#
#This function returns all product styles defined in the system with thier codes and names
#
function func_get_productstyles(){
$style_query="select id,name from mk_product_style where parent_style is NULL"; 
$styleresult=db_query($style_query);
$styles=array();
if ($styleresult)
	{
		$i=0;

		while ($row=db_fetch_array($styleresult))
		{
			$styles[$i] = $row;
			$i++;
		}
	}
	$stylesresults=array();
	foreach($styles as $style){
			$stylesresults[$style['id']]=$style['name'];
	}
	return $stylesresults;
}
#
#This function returns all product styles defined for a particular product id
#
function func_get_productstyles_forproduct($productid){
$style_query="select id,name from mk_product_style where product_type='$productid' and parent_style is NULL ";
$styleresult=db_query($style_query);
$styles=array();
if ($styleresult)
	{
		$i=0;

		while ($row=db_fetch_array($styleresult))
		{
			$styles[$i] = $row;
			$i++;
		}
	}
	$stylesresults=array();
	foreach($styles as $style){
			$stylesresults[$style['id']]=$style['name'];
	}
	return $stylesresults;
}
#
#This function returns all product options defined for a particular product style
#
function func_get_productoptions_forstyle($productid){
$style_query="select name,value,id from mk_product_options where style='$productid'";
$styleresult=db_query($style_query);
$styles=array();
if ($styleresult)
	{
		$i=0;

		while ($row=db_fetch_array($styleresult))
		{
			$styles[$i] = $row;
			$i++;
		}
	}
	//$stylesresults=array();
	$stylesresultsgrp=array();
	//$oldname="";
	foreach($styles as $style){
		//$newname=$style['name'];
		//if($newname!=$oldname){
			//$oldname=$style['name'];
			$stylesresultsgrp[$style['name']][$style['id']]=$style['value'];
			//if(!empty($styleresults))
			//$stylesresults=array();
		//}
		//$stylesresults[$style['value']]=$style['value'];
			
			
	}

	return $stylesresultsgrp;
}

function func_get_skulist(){
$sku_list_query="select pr_type.name as type,pr_style.name as style,sku.sku_id as id,sku.sku_name as sku_name,IF(sku.status='Y','Active','Inactive') as sku_status,sku.options as sku_options,sku.currentcount as sku_count,sku.sku_unitprice as sku_unit ,sku.thresholdcount as sku_thresh ,(sku.currentcount*sku.sku_unitprice)as sku_value
from mk_product_type pr_type,mk_product_style pr_style,mk_inventory_sku sku where pr_type.id=pr_style.product_type 
and pr_type.id=sku.product_type_id and pr_style.id=sku.style_id order by sku.sku_createddate desc ";
$sku_result=db_query($sku_list_query);
$sku_list=array();
if ($sku_result)
	{
		$i=0;

		while ($row=db_fetch_array($sku_result))
		{
			$sku_list[$i] = $row;
			$i++;
		}
	}
	return $sku_list;
}
#
#This function gets the sku details for given product_type and product_style
#
function func_get_skusearchlist($prod_type,$prod_style,$sku_inven){
	$type_cond;
	$style_cond;
	$sku_cond;
	$type_all="false";
	$style_all="false";
	if(count($prod_type)!=0){
	$type_cond="(";
	for($i=0;$i<count($prod_type);$i++){
		$type_cond.=($prod_type[$i].",");
		if($prod_type[$i]=="all"){
			$type_all="True";
		}
		
	}
	$type_cond=substr($type_cond,0,strlen($type_cond)-1);
	$type_cond=$type_cond.")";
	}
if(count($prod_style)!=0){
	$style_cond="(";
	for($i=0;$i<count($prod_style);$i++){
		$style_cond.=($prod_style[$i].",");
	if($prod_style[$i]=="all"){
			$style_all="True";
		}
		
	}
	$style_cond=substr($style_cond,0,strlen($style_cond)-1);
	$style_cond=$style_cond.")";
	}
if(count($sku_inven)!=0){
	$sku_cond="(";
	for($i=0;$i<count($sku_inven);$i++){
		$sku_cond.=($sku_inven[$i].",");
		
	}
	$sku_cond=substr($sku_cond,0,strlen($sku_cond)-1);
	$sku_cond=$sku_cond.")";
	}
	
$sku_list_query="select pr_type.name as type,pr_style.name as style,sku.sku_id as id,sku.sku_name as sku_name,IF(sku.status='Y','Active','Inactive') as sku_status,sku.options as sku_options,sku.currentcount as sku_count,sku.sku_unitprice as sku_unit ,sku.thresholdcount as sku_thresh ,(sku.currentcount*sku.sku_unitprice)as sku_value
from mk_product_type pr_type,mk_product_style pr_style,mk_inventory_sku sku where pr_type.id=pr_style.product_type 
and pr_type.id=sku.product_type_id and pr_style.id=sku.style_id ";
if($type_cond!=null&&$type_all!="True"){
	$sku_list_query=$sku_list_query." and sku.product_type_id in ".$type_cond;
}
if($style_cond!=null&&$style_all!="True"){
	$sku_list_query=$sku_list_query." and sku.style_id in ".$style_cond;
}
if($sku_cond!=null){
	$sku_list_query=$sku_list_query." and sku.sku_id in ".$sku_cond;
}
$orderby=" order by sku.sku_createddate desc ";
$sku_list_query=$sku_list_query.$orderby;
$sku_result=db_query($sku_list_query);
$sku_list=array();
if ($sku_result)
	{
		$i=0;

		while ($row=db_fetch_array($sku_result))
		{
			$sku_list[$i] = $row;
			$i++;
		}
	}
	return $sku_list;
	
}
#
#This function gets the sku details for given skuno
#
function func_upload_attachment($imagefile_name,$imagefile_size,$imagefile_tmp_name){
if(!empty($imagefile_name)){
$image_name = basename($imagefile_name);
/*echo "$image_name";*/
/*echo "$orderid";*/

				//names to be stored in database.
					$image_name = basename($imagefile_name);

				//upload the image file to data directory
					$data_dir = "data";
					$max_filesize = "5242880";
					/*if(isset($_ENV['WINDIR'])) {
						$userfile2 = str_replace("////","//",$videofile_name);
					}*/
					if($imagefile_size <= 0) die ("$image_name is empty.");

					if($imagefile_size > $max_filesize) {
						echo "Error: $image_name is too big. " .
						number_format($max_filesize) . "bytes is the limit.";
					}

					if(!@move_uploaded_file($imagefile_tmp_name, "$data_dir/$image_name"))
                      echo "Can't copy $imagefile_name to $file_name.";
}
	
}
#
#This function gets the sku details for given skuno
#
function func_get_skudetails($skuno){
	
	
}
#
#This function gets the sku details for given skuno
#
function func_getSkuDetails($skuid){
$commentQuery="select sku_name,sku_unitprice,vendor,currentcount,thresholdcount from `mk_inventory_sku` where sku_id='$skuid'";
$commentresult=db_query($commentQuery);
$comments=db_fetch_row($commentresult);
return $comments;
	
	
}
 function func_getreduc_types(){
 	$style_query="select id,reduction_type from mk_reduction_types";
$styleresult=db_query($style_query);
$styles=array();
if ($styleresult)
	{
		$i=0;

		while ($row=db_fetch_array($styleresult))
		{
			$styles[$i] = $row;
			$i++;
		}
	}
	$stylesresults=array();
	foreach($styles as $style){
			$stylesresults[$style['id']]=$style['reduction_type'];
	}
	return $stylesresults;
 	
 }
#
#This function gets the sku details for given skuno
#
function func_getSkuproductDetails($sku_id){
$commentQuery="select sku.sku_name,sku.sku_unitprice as unitprice,(sku.sku_unitprice*sku.currentcount) as currentvalue,".
"sku.thresholdcount,sku.thresholdemail,sku.vendor ,pr.name as prtype,st.name as style ,sku.notes as notes,sku.options,sku.status ". 
"from mk_inventory_sku sku,mk_product_type pr,mk_product_style st where sku.sku_id='$sku_id'  ". 
"and sku.product_type_id=pr.id and sku.style_id=st.id";
$commentresult=db_query($commentQuery);
$comments=db_fetch_row($commentresult);
return $comments;
	
	
}
function func_getproductcode($skuid){
$commentQuery="select distinct TRIM(product_code) from mk_product_type where id='$skuid'";
$commentresult=db_query($commentQuery);
$comments=db_fetch_row($commentresult);
return $comments[0];
	
	
}
function func_getStyle_id($skuid){
$commentQuery="select IFNULL(parent_style,id)as id from mk_product_style where id='$skuid'";
$commentresult=db_query($commentQuery);
$comments=db_fetch_row($commentresult);
return $comments[0];
	
	
}
#
#This function automatically reduces the inventory for a particular sku given its product_type_id, styleid and options
#The optionstr should be in format "option1id:option2id:Option3id" in ascending order
#Quantity is the quantity to be reduced and notes(optional) is some description of the event
#title shold be of format "Used for fulfilling orderid=$orderid"
#
function func_reduceinventory($producttype_id,$styleid,$optionstr,$quantity,$title,$notes=""){
$commentQuery="select sku_id,currentcount,thresholdcount from `mk_inventory_sku` where  product_type_id='$producttype_id' and style_id='$styleid' and options_id='$optionstr'";
$commentresult=db_query($commentQuery);
if(mysql_num_rows($commentresult)==1){
$comments=db_fetch_row($commentresult);
$sku_id= $comments[0];
$sku_count= $comments[1];
$thresholdcount= $comments[2];
$type="R";
$type_desc="Reduced";
$addedby='admin';
$reduction_id=5;
	$updatedCount=$sku_count-$quantity;
	$sku_createddate=time();	
	$sku_query="insert into `mk_inventory_log` (sku_id,date,changedby,title,inventory_type, ".
	"inventory_type_desc,quantity,notes ) values ".
	"('$sku_id','$sku_createddate','$addedby','$title','$type','$type_desc','$quantity','$notes')";
	db_query($sku_query);
	//sending Notification if quantity goes below threshold
	if($updatedCount<$thresholdcount){
	               $from ="MyntraAdmin";
					$loginEmail="operations@myntra.com";
					$bcc="mgmt@myntra.com";
					$args = array("SKU_ID"	=>$sku_id );

				    $template = "Thresholdlimit";
					sendMessage($template, $args, $loginEmail,$from,$bcc);
	}
	$update_count_query="update `mk_inventory_sku` set currentcount='$updatedCount' where sku_id='$sku_id'";
	db_query($update_count_query);
	
}
	
}
#
#This function returns style code a style id
#
function func_getstylecode($skuid){
$commentQuery="select distinct TRIM(style_code) from mk_product_style where id='$skuid'";
$commentresult=db_query($commentQuery);
$comments=db_fetch_row($commentresult);
return $comments[0];
	
	
}
function func_checksku($sku_no){
$commentQuery="select sku_id from mk_inventory_sku where sku_name='$sku_no'";
$commentresult=db_query($commentQuery);
$comments=db_fetch_row($commentresult);
return $comments[0];
}
function func_getoption($skuid){
$commentQuery="select distinct TRIM(value) from mk_product_options where id='$skuid'";
$commentresult=db_query($commentQuery);
$comments=db_fetch_row($commentresult);
return $comments[0];
	
	
}
function func_getoption_namevalue($skuid){
$commentQuery="select name,value from mk_product_options where id='$skuid'";
$commentresult=db_query($commentQuery);
$comments=db_fetch_row($commentresult);
return $comments;
	
	
}
#
#This function gets all the comments for given skuid
#
function func_getcomments_sku($sku_id){
$commentQuery="select changedby as commentby,date,quantity,notes,inventory_type_desc as type,title as summary,attachment1,attachment2 from mk_inventory_log where sku_id='$sku_id' order by date desc";
$commentresult=db_query($commentQuery);
$comments=array();
if ($commentresult)
	{
		$i=0;

		while ($row=db_fetch_array($commentresult))
		{
			$comments[$i] = $row;
			$i++;
		}
	}
	return $comments;
	
	
}
#
#This function gets the sku details for given skuno
#
function func_get_allsku(){
$style_query="select sku_id,sku_name from mk_inventory_sku";
$styleresult=db_query($style_query);
$styles=array();
if ($styleresult)
	{
		$i=0;

		while ($row=db_fetch_array($styleresult))
		{
			$styles[$i] = $row;
			$i++;
		}
	}
	$stylesresults=array();
	foreach($styles as $style){
			$stylesresults[$style['sku_id']]=$style['sku_name'];
	}
	return $stylesresults;
	
	
}

?>
