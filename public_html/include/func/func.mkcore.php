<?php
if (!defined('XCART_START')) {
    header ("Location: ../");
    die ("Access denied");
}
include_once $xcart_dir."/include/func/func.courier.php";
include_once $xcart_dir."/include/func/func.core.php";
include_once $xcart_dir."/include/func/func.order.php";
include_once("$xcart_dir/modules/apiclient/SkuApiClient.php");
include_once("$xcart_dir/modules/apiclient/AtpApiClient.php");
include_once ("$xcart_dir/include/class/class.feature_gate.keyvaluepair.php");
include_once("$xcart_dir/modules/coupon/database/couponAPIClient.php");

/**************************************************************************************************************************/

function soap_object_to_array($data)
{
	if(is_array($data) || is_object($data))
	{
		$result = array();
		$j=0;
		foreach($data as $key => $value)
		{
			$result[$j++] = soap_object_to_array($value);
		}
		return $result;
	}
	return $data;
}


function convertSoapObjectListToPHPArray($rootCategory)
{
	$finalArray = array();
	$noOfObjectInSoapObjectList = 0;
	while($rootCategory[$noOfObjectInSoapObjectList] != '')
	{
		$arrayValue = array();
		$j = 0;
		foreach ($rootCategory[$noOfObjectInSoapObjectList] as $key=>$value)
		{
			if (is_object($value) || is_array($value))
			{
				$arrayValue[$j]=convertSoapObjectListToPHPArray($value);
			}
			else
			{
				$arrayValue[$j]=$value;
			}
			$j = $j + 1;
		}
		$finalArray[$noOfObjectInSoapObjectList] = $arrayValue;
		$noOfObjectInSoapObjectList = $noOfObjectInSoapObjectList + 1;
	}
	return $finalArray;
}

/**************************************************************************************************************************/

function func_load_product_type_home($accountType = NULL, $shopMasterId = NULL)
{
	global $sql_tbl;
	$producttypetable=$sql_tbl["mk_product_type"];

	if($accountType == 1)
	$query = "select id, name, label, image_t, type, price_start,thumb_small_image_path,thumb_big_image_path,is_featured  from mk_product_type where id in (SELECT distinct(mk_product_style.product_type) FROM mk_product_style , mk_shop_masters_catalog  where mk_shop_masters_catalog.product_style_id=mk_product_style.id and shop_master_id=$shopMasterId and enable=1) and is_active=1 AND type='P' order by display_order asc";
	else
	$query           =
        "select $producttypetable.id, $producttypetable.name, $producttypetable.label,$producttypetable.image_t, $producttypetable.type,price_start,$producttypetable.thumb_small_image_path,$producttypetable.thumb_big_image_path,$producttypetable.is_featured  from $producttypetable where  $producttypetable.is_active=1 AND $producttypetable.type='P' order by $producttypetable.display_order asc";
	$result          =db_query($query);

	$producttypes=array();

	if ($result)
	{
		$i=0;

		while ($row=db_fetch_row($result))
		{
			$producttypes[$i] = $row;
			$i++;
		}
	}

	return $producttypes;
}

function func_load_active_coupon_groups()
{

    /*global $sql_tbl;
    $query = "SELECT groupname from " . $sql_tbl['coupon_group'] . " WHERE active='A'";
    $result = db_query($query, true);
    $groupsArray = CouponAPIClinet::searchCouponGroups(null);
    $groups = array();
    if ($result) {
        while ($row = db_fetch_row($result)) {
			array_push($groups, $row[0]);
		}
    }
    
    return $groups;*/
    $groupsArray = CouponAPIClient::searchCouponGroups(null);
    $groups = array();
    foreach ($groupsArray as $id=>$value)
        {
            
                array_push($groups, $value['groupname']);
            
        }
    return $groups;
}

function func_get_channel($id)
{
    global $sql_tbl;
    $result = db_query("SELECT subdomain 
        		 		  FROM " . $sql_tbl['org_accounts'] . " 
        				 WHERE id = '$id';");
        
    $row = db_fetch_array($result);
    return $row['subdomain'];
}

/*
 * behavious changed from legacy org channels to device and order channels
 */
function func_load_active_channels()
{
    /*global $sql_tbl;
    $query = "SELECT subdomain FROM " . $sql_tbl['org_accounts'] . " WHERE status=1";
    $result = db_query($query, true);
    
    $channels = array();
    array_push($channels, 'online');
    
    if ($result) {
        while ($row = db_fetch_row($result)) {
			array_push($channels, $row[0]);
		}
    }*/
    
    return array("online","mobile","tablet", "mobile_app","complete_mobile_domain");
}

function func_load_product_type($accountType = NULL, $shopMasterId = NULL)
{
	global $sql_tbl;
	$producttypetable=$sql_tbl["mk_product_type"];

	if($accountType == 1)
	$query = "select id, name, label, image_t, type, price_start,thumb_small_image_path,thumb_big_image_path,is_featured  from mk_product_type where id in (SELECT distinct(mk_product_style.product_type) FROM mk_product_style , mk_shop_masters_catalog  where mk_shop_masters_catalog.product_style_id=mk_product_style.id and shop_master_id=$shopMasterId and enable=1) and is_active=1 AND type='P' order by display_order asc limit 17";
	else
	$query           =
        "select $producttypetable.id, $producttypetable.name, $producttypetable.label,$producttypetable.image_t, $producttypetable.type,price_start,$producttypetable.thumb_small_image_path,$producttypetable.thumb_big_image_path,$producttypetable.is_featured,mk_product_group.name, mk_product_group.id  from $producttypetable, mk_product_group where  $producttypetable.is_active=1 AND $producttypetable.type='P' and mk_product_group.id=mk_product_type.product_type_groupid order by $producttypetable.display_order asc limit 17";
	$result          =db_query($query);

	$producttypes=array();

	if ($result)
	{
		$i=0;

		while ($row=db_fetch_row($result))
		{
			$producttypes[$i] = $row;
			$i++;
		}
	}

	return $producttypes;
}

//Used By Vineet . To Do : Sync Up
function func_get_all_product_types($useDisplayOrder = false)
{
	global $sql_tbl;
	$productTypeTable=$sql_tbl["mk_product_type"];

	$queryString     =
        "select
                                    a.id,
                                    a.label,
                                    a.image_t,
									a.type,
									a.price_start,
									a.description

                                 from
                                 $productTypeTable a
                                 where
                                    a.is_active=1 and type='P'  ";
                                 //LIMIT 6";
                                 if($useDisplayOrder)
                                 {
                                 	$queryString  .= "  ORDER BY a.display_order ASC";
                                 }

                                 $result          =db_query($queryString);

                                 $productTypeDetails=array();

                                 if ($result)
                                 {
                                 	$i=0;

                                 	while ($row=db_fetch_row($result))
                                 	{
                                 		$productTypeDetails[$i] = $row;
                                 		$i++;
                                 	}
                                 }

                                 return $productTypeDetails;
}

/**************************************************************************************************************************/

function getParamFromOptionsList($list,$field){
	$pattern = '/[^a-zA-Z0-9]*'.$field.'\s*=\s*([^\s\n]*)/';
	preg_match($pattern,$list,$matches);
	return $matches[1];
}



/**************************************************************************************************************************/
function func_load_all_product_style_details_markup($productTypeId,$productid,$priceForPOS,$shopMasterId)
{
	//	echo $productTypeId;
	//	echo $productid;
	global $sql_tbl;
	$productTable=$sql_tbl["products"];
	$productStyleTable=$sql_tbl["mk_product_style"];
	$productTypeTable =$sql_tbl["mk_product_type"];

	if($priceForPOS ==1 )
	{
		$queryString      =
        "select
        $productStyleTable.id,
        $productStyleTable.name,
        $productStyleTable.label,
        $productStyleTable.description,
        $productStyleTable.image_l,
							    mk_shop_masters_catalog.store_rate as price,
							    $productStyleTable.image_t,
							    $productStyleTable.image_i

                             from
                             $productStyleTable,
                             $productTypeTable,
                             $productTable,
                             mk_shop_masters_catalog 
                             where
                             $productTypeTable.id=$productTypeId and
                             $productTable.productid=$productid and
                             $productTypeTable.id=$productStyleTable.product_type and
                             $productStyleTable.is_active=1 and
                             $productStyleTable.iventory_count > 0 and
                             $productStyleTable.styletype='P' and
                             mk_shop_masters_catalog.product_style_id = $productStyleTable.id and
                             mk_shop_masters_catalog.shop_master_id = $shopMasterId";
	}
	else
	$queryString      =
        "select
        $productStyleTable.id,
        $productStyleTable.name,
        $productStyleTable.label,
        $productStyleTable.description,
        $productStyleTable.image_l,
							    FORMAT($productStyleTable.price+IFNULL($productStyleTable.price*$productTable.markup/100,0),2) as price,
							    $productStyleTable.image_t,
							    $productStyleTable.image_i

                             from
                             $productStyleTable,
                             $productTypeTable,
                             $productTable
                             where
                             $productTypeTable.id=$productTypeId and
                             $productTable.productid=$productid and
                             $productTypeTable.id=$productStyleTable.product_type and
                             $productStyleTable.is_active=1 and
                             $productStyleTable.iventory_count > 0 and
                             $productStyleTable.styletype='P' ";

                             $result           =db_query($queryString);

                             $productStyleDetails=array();

                             if ($result)
                             {
                             	$i=0;

                             	while ($row=db_fetch_row($result))
                             	{
                             		$productStyleDetails[$i] = $row;
                             		$i++;
                             	}
                             }

                             //    print_r($productStyleDetails);
                             return $productStyleDetails;
}

function func_load_all_product_style_details($productTypeId)
{
	global $sql_tbl;
	$productStyleTable=$sql_tbl["mk_product_style"];
	$productTypeTable =$sql_tbl["mk_product_type"];

	$queryString      =
        "select
        $productStyleTable.id,
        $productStyleTable.name,
        $productStyleTable.label,
        $productStyleTable.description,
        $productStyleTable.image_l,
        $productStyleTable.price,
        $productStyleTable.image_t,
        $productStyleTable.image_i

                             from
                             $productStyleTable,
                             $productTypeTable
                             where
                             $productTypeTable.id=$productTypeId and
                             $productTypeTable.id=$productStyleTable.product_type and
                             $productStyleTable.is_active=1 and
                             $productStyleTable.iventory_count > 0";

                             $result           =db_query($queryString);

                             $productStyleDetails=array();

                             if ($result)
                             {
                             	$i=0;

                             	while ($row=db_fetch_row($result))
                             	{
                             		$productStyleDetails[$i] = $row;
                             		$i++;
                             	}
                             }

                             //    print_r($productStyleDetails);
                             return $productStyleDetails;
}

/**************************************************************************************************************************/

function func_load_all_product_options($productStyleId, $shopMasterId) {
	global $sql_tbl, $weblog, $xcart_dir, $login;
	include_once("$xcart_dir/modules/apiclient/SkuApiClient.php");
	include_once("$xcart_dir/databaseinit.php");
	$productOptionsTable=$sql_tbl["mk_product_options"];
	$productStyleTable = $sql_tbl["mk_product_style"];
	
	$priceRow = func_query_first("select price from $productStyleTable where id=$productStyleId");
	
	$styleprice = $priceRow[0];
	
	$queryString = "SELECT po.id, po.name, po.value, po.price, po.default_option, som.sku_id as sku_id, po.is_active from mk_product_options po, mk_styles_options_skus_mapping som where po.id = som.option_id and po.style = $productStyleId order by po.id";
	$results = func_query($queryString);
	foreach ($results as $row) {
		$skuIds[] = $row['sku_id'];
	}
	
	//$skuId2AvailableCountMap = SkuApiClient::getAvailableInvCountMap($skuIds);
	$skuId2AvailableCountMap = AtpApiClient::getAvailableInventoryForSkus($skuIds);
	$weblog->info("sku available count for style - $productStyleId = ". print_r($skuId2AvailableCountMap, true));
	foreach ($results as $index=>$row) {
		$available_count = $skuId2AvailableCountMap[$row['sku_id']]['availableCount'];
		$results[$index]['is_active'] = $row['is_active'] == 1 && ($available_count > 0);
		$col_no = 0;
		foreach($row as $col_name=>$col_value){
			$results[$index][$col_no++] = $col_value;
		}
		$results[$index]['price'] = $styleprice;
		$results[$index][3] = $styleprice;
		$results[$index]['sku_count'] =$available_count;  	
	}
	
	return $results;
}

function func_load_product_options($productStyleId,$shopMasterId=0, $include_disabled=false, $keepKeys=false, $callApi=true)
{
	global $sql_tbl, $weblog, $xcart_dir, $login;

	$productOptionsTable=$sql_tbl["mk_product_options"];
	$productStyleTable      =   $sql_tbl["mk_product_style"];
	
	$priceRes = db_query("select price from $productStyleTable where id=$productStyleId",true);
	
	$priceRow = db_fetch_row($priceRes);
	$styleprice = $priceRow[0];
	if($include_disabled==true) {
		$queryString = "SELECT po.id, po.name, po.value, po.price, po.default_option, som.sku_id as sku_id from $productOptionsTable po, mk_styles_options_skus_mapping som where po.id = som.option_id and po.style = $productStyleId";
		/*$queryString        = "select a.id,a.name,a.value,a.price,a.default_option from
		$productOptionsTable a
         	                        where a.style=$productStyleId  ORDER BY a.id ASC";*/
	} else {

		$queryString = "SELECT po.id, po.name, po.value, po.price, po.default_option, som.sku_id as sku_id from $productOptionsTable po, mk_styles_options_skus_mapping som where po.id = som.option_id AND po.is_active = 1 and po.style = $productStyleId";

	}
	
	if($keepKeys) {
		$productOptionDetails = func_query($queryString,true);
		foreach ($productOptionDetails as $index=>$row) {
			if(empty($row['price'])) {
				$productOptionDetails[$index]['price'] = $styleprice;
			}
		}
	} else {
		$result = db_query($queryString,true);
	    $productOptionDetails=array();
	
	    if ($result) {
	    	$i=0;
			while ($row=db_fetch_row($result)) {
				if( empty($row[3]) )
	            	$row[3] =  $styleprice;  // if price is not defined for this this, consider style price
	          	$productOptionDetails[$i] = $row;
	            $i++;
	        }
	    }
	}
	
    return $productOptionDetails;
}

function func_load_product_addons($productStyleId,$prod_option,&$defaultAddon,$shopMasterId)
{
	$productAddons = array();
	return $productAddons;
}


/**************************************************************************************************************************/
function func_load_product_style_details_markup($productStyleId,$productid,$priceForPOS,$shopMasterId)
{
	global $sql_tbl,$weblog;
	$productTable=$sql_tbl["products"];
	$productStyleTable=$sql_tbl["mk_product_style"];
	$styleimages1= $sql_tbl["mk_style_images"];
	$styleimages2= $sql_tbl["mk_style_images"];
	if($priceForPOS == 1)
	{
		$queryString      =
        "select
                                    a.id,
                                    a.name,
                                    a.label,
                                    a.description,
                                    a.image_l,
                                    mk_shop_masters_catalog.store_rate as price,
									c.image_path as image_t,
									d.image_path as image_i
                                 from
                                 $productStyleTable a,
                                 $productTable b,
                                 $styleimages1 c,
                                 $styleimages2 d,
                                 mk_shop_masters_catalog
                                    
                                 where
                                    a.id = $productStyleId and
                                   
                                    b.productid=$productid and
                                    a.is_active=1 and
                                    a.iventory_count > 0
									and c.styleid=a.id
									and d.styleid=a.id
									and c.type='T'
									and d.type='I'
									and c.isdefault='Y'
									and d.isdefault='Y'
									and mk_shop_masters_catalog.product_style_id= $productStyleId
									and mk_shop_masters_catalog.shop_master_id = $shopMasterId";
	}
	else
	$queryString      =
        "select
                                    a.id,
                                    a.name,
                                    a.label,
                                    a.description,
                                    a.image_l,
                                    FORMAT(a.price+IFNULL(a.price*b.markup/100,0),2) as price,
									c.image_path as image_t,
									d.image_path as image_i,
									a.l_width,
                                    a.l_height,
                                    a.description,
									a.size_chart_image
                                 from
                                 $productStyleTable a,
                                 $productTable b,
                                 $styleimages1 c,
                                 $styleimages2 d
                                    
                                 where
                                    a.id = $productStyleId and
                                   
                                    b.productid=$productid and
                                    a.is_active=1 and
                                    a.iventory_count > 0
									and c.styleid=a.id
									and d.styleid=a.id
									and c.type='T'
									and d.type='I'
									and c.isdefault='Y'
									and d.isdefault='Y'";
                                 //$weblog->info($queryString);

                                 //print_r($queryString);
                                 //exit;

                                 $result           =db_query($queryString);

                                 $productStyleDetails=array();

                                 if ($result)
                                 {
                                 	$i=0;

                                 	while ($row=db_fetch_row($result))
                                 	{
                                 		$productStyleDetails[$i] = $row;
                                 		//$weblog->info($productStyleDetails[$i]);

                                 		$i++;
                                 	}
                                 }

                                 //    print_r($productStyleDetails);
                                 return $productStyleDetails;
}


function func_load_product_style_details($productStyleId,$priceForPOS=0,$shopMasterId=0)
{
	global $sql_tbl;
	$productStyleTable=$sql_tbl["mk_product_style"];
	if($priceForPOS == 1)
	{
		$queryString      =
        "select                     a.id,
                                    a.name,
                                    a.label,
                                    a.description,
                                    a.image_l,
                                    mk_shop_masters_catalog.store_rate,
									a.image_t,
									a.image_i,
                                    a.l_width,
                                    a.l_height,
                                    a.description
                                 from
                                 $productStyleTable a,
                                 mk_shop_masters_catalog
                                 where
                                    a.id = $productStyleId and
                                    a.is_active=1 and
                                    a.iventory_count > 0
									and mk_shop_masters_catalog.product_style_id= $productStyleId
									and mk_shop_masters_catalog.shop_master_id = $shopMasterId";
	}
	else
	$queryString      =
        "select                     a.id,
                                    a.name,
                                    a.label,
                                    a.description,
                                    a.image_l,
                                    a.price,
									a.image_t,
									a.image_i,
                                    a.l_width,
                                    a.l_height,
                                    a.description,
									a.size_chart_image,
									a.is_customizable
                                 from
                                 $productStyleTable a
                                 where
                                    a.id = $productStyleId and
                                    a.is_active=1 and
                                    a.iventory_count > 0";

                                 $result           =db_query($queryString,true);

                                 $productStyleDetails=array();

                                 if ($result)
                                 {
                                 	$i=0;

                                 	while ($row=db_fetch_row($result))
                                 	{
                                 		$productStyleDetails[$i] = $row;
                                 		$i++;
                                 	}
                                 }

                                 //    print_r($productStyleDetails);
                                 return $productStyleDetails;
}

/**************************************************************************************************************************/

function func_get_all_orientation_for_default_customization_area($productStyleId)
{
	global $sql_tbl;
	$customizationAreaTable     =   $sql_tbl["mk_style_customization_area"];
	$orientationTable           =   $sql_tbl["mk_customization_orientation"];

	$queryString                =   "select
                                        o.location_name,
                                        o.options,
                                        o.start_x,
                                        o.start_y,
                                        o.size_x,
                                        o.size_y,
                                        o.area_id,
                                        o.id,
                                        o.height_in,
                                        o.width_in,
										c.bkgroundimage,
										o.bgstart_x,
										o.bgstart_y,
										o.bgsize_x,
										o.bgsize_y,
										o.textLimitation,
                                        o.textFontColors,
                                        o.textColorDefault
                                     from
                                     $customizationAreaTable c,
                                     $orientationTable o
                                     where
                                     c.style_id='$productStyleId' and
                                     o.ort_default='Y' and isprimary = '1' and
                                     c.id=o.area_id";

                                     $result                     =    db_query($queryString);
                                     $orientationDetails         =    array();
                                     if ($result){
                                     	$i=0;
                                     	while ($row=db_fetch_row($result)){
                                     		$orientationDetails[$i] = $row;
                                     		$i++;
                                     	}
                                     }
                                     return $orientationDetails;
}
/**************************************************************************************************************************/
function func_get_orientation_for_customization_area($customizationAreaId,$orientationId = ''){
	global $sql_tbl;
	$customizationAreaTable     =   $sql_tbl["mk_style_customization_area"];
	$orientationTable           =   $sql_tbl["mk_customization_orientation"];

	$queryString                =   "select
                                        o.location_name,
                                        o.options,
                                        o.start_x,
                                        o.start_y,
                                        o.size_x,
                                        o.size_y,
                                        o.area_id,
                                        o.id,
                                        o.height_in,
                                        o.width_in,
                                        o.textLimitation,
                                        o.textFontColors,
                                        o.textColorDefault
                                     from
                                     $customizationAreaTable c,
                                     $orientationTable o
                                     where
                                     c.id=$customizationAreaId and
                                     c.id=o.area_id";
                                     if(!empty($orientationId)) $queryString .= " AND o.id = '".$orientationId."' ";
                                     $result                     =    db_query($queryString);
                                     $orientationDetails         =    array();
                                     if ($result){
                                     	$i=0;
                                     	while ($row=db_fetch_row($result)){
                                     		$orientationDetails[$i] = $row;
                                     		$i++;
                                     	}
                                     }
                                     //print_r($orientationDetails);
                                     return $orientationDetails;
}

/**************************************************************************************************************************/
function func_get_height_width_for_orientation($oId){
	global $sql_tbl;
	//$customizationAreaTable     =   $sql_tbl["mk_style_customization_area"];
	$orientationTable           =   $sql_tbl["mk_customization_orientation"];

	$queryString                =   "select
                                        o.location_name,
                                        o.options,
                                        o.start_x,
                                        o.start_y,
                                        o.size_x,
                                        o.size_y,
                                        o.area_id,
                                        o.id,
                                        o.height_in,
                                        o.width_in
                                     from
                                     $orientationTable o
                                     where
                                     o.id=$oId";
                                     $result                     =    db_query($queryString);
                                     $orientationDetails         =    array();
                                     if ($result){
                                     	$i=0;
                                     	while ($row=db_fetch_row($result)){
                                     		$orientationDetails[$i] = $row;
                                     		$i++;
                                     	}
                                     }
                                     //print_r($orientationDetails);
                                     return $orientationDetails[0];
}
/**************************************************************************************************************************/

function func_get_all_customization_area_for_product_style($productStyleId)
{
	global $sql_tbl;
	$customizationAreaTable=$sql_tbl["mk_style_customization_area"];

	$queryString           =
        "select
                                        c.id,
                                        c.label,
                                        c.image_l,
                                        c.image_t
                                     from
                                     $customizationAreaTable c ,mk_customization_orientation o
                                     where
                                     o.area_id=c.id
                                     AND c.style_id='{$productStyleId}'
                                     group by c.id";
                                     $result                =db_query($queryString);

                                     $customizationAreas=array();

                                     if ($result)
                                     {
                                     	$i=0;

                                     	while ($row=db_fetch_row($result))
                                     	{
                                     		$customizationAreas[$i] = $row;
                                     		$i++;
                                     	}
                                     }

                                     //    print_r($customizationAreas);
                                     return $customizationAreas;
}

/*********************************************************************************************************/
function func_get_price_for_product($productId){
	global $sql_tbl;
	$productTable       = $sql_tbl["products"];
	$query              = "select
                            p.list_price
                          from
                          $productTable p
                          where
                            p.productid = $productId";
                          $result             = db_query($query);
                          $price              = array();

                          if($result)
                          {
                          	$row           = db_fetch_row($result);
                          	$price         = $row;
                          }
                          return $price[0];
}
/*******************************************************************************************************/
function func_get_label_for_product_type($productTypeId){
	global $sql_tbl;
	$productTypeTable   = $sql_tbl["mk_product_type"];
	$query              = "select
                            p.label
                          from
                          $productTypeTable p
                          where
                            p.id = $productTypeId";
                          $result             = db_query($query);
                          $label              = array();

                          if($result)
                          {
                          	$row           = db_fetch_row($result);
                          	$label         = $row;
                          }
                          return $label[0];
}

/*****************************************************************************************************/

function func_get_product_details_by_id($productTypeId){
	global $sql_tbl;
	$productTypeTable       =   $sql_tbl["mk_product_type"];

	$queryString            =   "select
                                    a.id,
                                    a.label,
                                    a.image_t,
                                    a.description
                                 from
                                 $productTypeTable a
                                 where
                                    a.id=$productTypeId";

                                 $result                 =   db_query($queryString);
                                 $productDetails     =   array();

                                 if ($result){
                                 	$i=0;
                                 	while ($row=db_fetch_row($result)){
                                 		$productDetails[$i] = $row;
                                 		$i++;
                                 	}
                                 }
                                 //print_r($productDetails);
                                 return $productDetails;

}

/**********************************************************************************************/
function func_load_subcategoriesByName($name)
{
	if ('' == $name)
	{
		return array();
	}


	global $sql_tbl;
	$tb_categories=$sql_tbl["categories"];
	$query ="select categoryid from $tb_categories where category='$name'";
	$parentid = 0;
	$result =db_query($query);
	if($result)
	{
		$row=db_fetch_row($result);
		$parentid = $row[0];
	}
	if($parentid == 0)
	{
		return array();
	}
	$tb_categories=$sql_tbl["categories"];
	$tbl_images =$sql_tbl["images_C"];
	$query        ="SELECT categoryid,category , image_path,description FROM $tb_categories left join  $tbl_images
	on  categoryid=$tbl_images.id
	WHERE parentid='$parentid' order by  category asc";

	$result       =db_query($query);

	$returnArray=array();

	if ($result)
	{
		$i=0;

		while ($row=db_fetch_row($result))
		{
			$returnArray[$i] = $row;
			$i++;
		}
	}

	return $returnArray;
}
/*****************************************************************************************************/

function func_get_order_history_for_user($userId)
{
	global $sql_tbl;
	$orderTable = $sql_tbl["orders"];

	$query	=	"SELECT  	$orderTable.status,
							CONCAT($orderTable.title, ' ', $orderTable.firstname, ' ', $orderTable.lastname) AS Name,
							CONVERT_TZ($orderTable.date,'+00:00','+10:30'),
							$orderTable.total

				FROM		$orderTable
				where		$orderTable.login='$userId'";

							$result      =db_query($query);

							$orderHistory=array();

							if ($result)
							{
								$i=0;

								while ($row=db_fetch_row($result))
								{
									if("I" == $row[0])
									{
										$row[0] = "Not finished";
									}
									elseif("Q" == $row[0])
									{
										$row[0] = "Queued";
									}
									elseif("P" == $row[0])
									{
										$row[0] = "Processed";
									}
									elseif("B" == $row[0])
									{
										$row[0] = "Backordered";
									}
									elseif("D" == $row[0])
									{
										$row[0] = "Declined";
									}
									elseif("F" == $row[0])
									{
										$row[0] = "Failed";
									}
									elseif("C" == $row[0])
									{
										$row[0] = "Complete";
									}
									$row[2] = gmdate("d-m-Y H:i:s", $row[2]);
									$orderHistory[$i] = $row;
									$i++;
								}
							}

							return $orderHistory;
}
/********************************************************************************************************/
function getTimeStamp($inputDate, $hour, $min, $sec)
{
	$date1 = substr("$inputDate",0,strpos($inputDate,"/"));
	$inputDate = substr("$inputDate", strpos($inputDate, "/")+1, strlen($inputDate));
	$month1 = substr("$inputDate",0,strpos($inputDate,"/"));
	$year1 = substr("$inputDate", strpos($inputDate, "/")+1, strlen($inputDate));
	return mktime($hour, $min, $sec, $month1, $date1, $year1);
}

/*******************************************************************************************************/
function func_get_coupon_details($couponCode){
	global $sql_tbl;
	$couponTable        = $sql_tbl["discount_coupons"];
	$query              = "select
                            c.discount,
                            c.coupon_type,
                            c.productid,
                            c.categoryid,
                            c.minimum,
                            c.times,
                            c.per_user,
                            c.times_used,
                            c.expire,
                            c.status,
                            c.provider,
                            c.recursive,
                            c.apply_category_once,
                            c.apply_product_once,
							c.groupid,
							c.freeship,
							c.styleid,
							c.maximum,
							c.producttypeid,
							c.promotionid,
							c.times_locked,
							c.startdate
                          from
                          $couponTable c
                          where
                            c.coupon = '$couponCode' and status = 'A' ";
                          $result             = db_query($query);
                          $couponDetails      = array();

                          if($result)
                          {
                          	$row           = db_fetch_row($result);
                          	$couponDetails = $row;
                          }
                          return $couponDetails;

}
/********************************************************************************************/
function func_get_all_subcategories_for_category($categoryId){
	global $sql_tbl;
	$categoryTable          = $sql_tbl["categories"];
	$query                  = "select
                                c.categoryid
                              from
                              $categoryTable c
                              where
                                c.categoryid=$categoryId or c.parentid=$categoryId";
                              $result                 = db_query($query);
                              $categories             = array();
                              if ($result){
                              	$i=0;
                              	while ($row=db_fetch_row($result)){
                              		$categories[$i] = $row;
                              		$i++;
                              	}
                              }
                              return $categories;
}
/****************************************************************************************/
function func_get_categories_for_products($productIdString){
	global $sql_tbl;
	$productCategoryTable   = $sql_tbl["products_categories"];
	$query                  = "select
                                c.productid,
                                c.categoryid
                              from
                              $productCategoryTable c
                              where
                                c.productid in ($productIdString)";

                              $result                 = db_query($query);
                              $categories             = array();
                              if ($result){
                              	$i=0;
                              	while ($row=db_fetch_row($result)){
                              		$categories[$i] = $row;
                              		$i++;
                              	}
                              }
                              return $categories;

}
/****************************************************************************************/
function func_get_priceId($productId){
	global $sql_tbl;
	$pricingTable           = $sql_tbl["pricing"];
	$query                  = "select
                                p.priceid
                              from
                              $pricingTable p
                              where
                                p.productid = $productId";
                              //echo "%%%%%%%%%%%%%%%%%%%%%%%%".$query;
                              $result                 = db_query($query);
                              $priceId                = array();
                              if($result)
                              {
                              	$row               = db_fetch_row($result);
                              	$priceId           = $row;
                              }

                              //print_r($priceId);
                              return $priceId[0];

}
/******************************************************************************************/
function func_get_all_categories(){
	global $sql_tbl;
	$categoryTable          = $sql_tbl["categories"];
	$query                  = "SELECT
                                x.category,
                                x.categoryid,
                                x.categoryid_path
                              FROM
                                xcart_categories x
                              WHERE
                                x.category <> 'Themes' AND
                                x.parentid not in (SELECT categoryid
                                                  FROM xcart_categories a
                                                 WHERE a.category = 'Themes')
                                ORDER BY x.categoryid_path";
	$result                 = db_query($query);
	$categories             = array();
	if ($result){
		$i=0;
		while ($row=db_fetch_row($result)){
			$categories[$i] = $row;
			$i++;
		}
	}
	//print_r($parentcategory);
	return $categories;
}

/********************************************************************************************/

function func_get_data_for_user($login){
	global $sql_tbl;
	$userTable              = $sql_tbl["customers"];
	$query                  = "select
                                c.firstname,
                                c.lastname,
                                c.email
                              from
                              $userTable c
                              where c.login = '$login'";
                              $result                 = db_query($query);
                              $email                  = array();
                              if($result)
                              {
                              	$row               = db_fetch_row($result);
                              	$email             = $row;
                              }

                              //print_r($priceId);
                              return $email;
}

/***********************************************************************************************************/
function func_all_product_styles_by_product_type($productTypeId){
	global $sql_tbl;
	$productStyleTable       =   $sql_tbl["mk_product_style"];

	$queryString            =   "SELECT $productStyleTable.id,product_type,$productStyleTable.name,b.image_path as image_t,price
                                  FROM  $productStyleTable,mk_style_images b
                                 where
                                    product_type=$productTypeId and $productStyleTable.id != '25'
                                    and b.styleid =$productStyleTable.id and b.type='T'  and b.isdefault='Y'";

	$productDetails                 =  func_query($queryString);
	/*$productDetails     =   array();

	if ($result){
	$i=0;
	while ($row=db_fetch_row($result)){
	$productDetails[$i] = $row;
	$i++;
	}
	}
	//print_r($productDetails);*/
	return $productDetails;

}

/***********************************************************************************************************/
function func_all_product_styles_by_product_type_for_user($productTypeId, $accountType, $shopMasterId){
	global $sql_tbl;
	$productStyleTable       =   $sql_tbl["mk_product_style"];

	if($accountType == 1)
	{
		$queryString = "SELECT a.id,a.product_type,a.name,b.image_path as image_t,mk_shop_masters_catalog.store_rate as price,a.description
                                  FROM  $productStyleTable a ,mk_style_images b, mk_shop_masters_catalog
                                 where
                                    a.product_type=$productTypeId and a.id != '25' and a.is_active=1 and  a.styletype='P'
								  and b.styleid =a.id and b.type='T'  and b.isdefault='Y' and mk_shop_masters_catalog.product_style_id = b.styleid and
								  mk_shop_masters_catalog.enable=1 and shop_master_id = $shopMasterId";
	}
	else
	$queryString            =   "SELECT a.id,a.product_type,a.name,b.image_path as image_t,a.price,a.description
                                  FROM  $productStyleTable a ,mk_style_images b
                                 where
                                    a.product_type=$productTypeId and a.id != '25' and a.is_active=1 and  a.styletype='P'
								  and b.styleid =a.id and b.type='T'  and b.isdefault='Y' 
								  ";

	$productDetails                 =  func_query($queryString);
	/*$productDetails     =   array();

	if ($result){
	$i=0;
	while ($row=db_fetch_row($result)){
	$productDetails[$i] = $row;
	$i++;
	}
	}
	//print_r($productDetails);*/
	return $productDetails;

}

/********************************************************************************************/

function func_get_product_detail($productid) {
	global $sql_tbl;
	$image_T = $sql_tbl["images_T"];
	$products = $sql_tbl["products"];

	$query = "SELECT image_path
				  FROM $image_T, $products
				  WHERE $image_T.id = $products.productid
				  AND $image_T.id = ".$productid." AND $products.productid = ".$productid."";

	$result = db_query($query);
	$row = db_fetch_array($result);

	return $row;
}
/********************************************************************************************/

function func_load_product_style_icon_details($productTypeId,$priceForPOS,$shopMasterId,$bigicon = false)
{
	global $sql_tbl,$sqllog,$affiliateInfo,$weblog;
	$weblog->info("affiliate-id :: ".$affiliateInfo[0]['affiliate_id']." in ".__FUNCTION__." @ line ".__LINE__);

	if( $bigicon )
		$type = "B";
	else
		$type = "I";

	$productStyleTable=$sql_tbl["mk_product_style"];
	/* $queryString = "select a.id, a.product_type, a.image_i, a.name, a.description, a.price from $productStyleTable a where
	 a.product_type = $productTypeId and a.is_active=1 and a.iventory_count > 0 and id != 25  and styletype='P'";*/
	if($priceForPOS == 1)
	{
		$queryString = "SELECT a.id,product_type,b.image_path as image_i,mk_shop_masters_catalog.store_rate as price,a.name
                   FROM  $productStyleTable as a ,mk_style_images b, mk_shop_masters_catalog
                   where a.product_type='".($productTypeId)."' and a.id != '25'
                   and b.styleid = a.id and mk_shop_masters_catalog.product_style_id=b.styleid and mk_shop_masters_catalog.shop_master_id = $shopMasterId and mk_shop_masters_catalog.enable=1 and b.type='".$type."'  and b.isdefault='Y' and a.is_active = 1 and styletype = 'P' order by a.id desc";
	}else
	$queryString = "SELECT a.id,product_type,b.image_path as image_i,a.price,a.name
                   FROM  $productStyleTable as a ,mk_style_images b
                   where a.product_type='".($productTypeId)."' and a.id != '25'
                   and b.styleid = a.id and b.type='".$type."'  and b.isdefault='Y' and a.is_active = 1 and styletype = 'P' order by a.id desc ";
	$sqllog->debug("SQL Query : ".$queryString." :: affiliate-id :: ".$affiliateInfo[0]['affiliate_id']." in ".__FUNCTION__." @ line ".__LINE__);
	$result_details = func_query($queryString);
	return  $result_details;
}
function func_load_product_style_icon_details_markup($productTypeId,$productid,$priceForPOS,$shopMasterId)
{
	global $sql_tbl;
	$productStyleTable=$sql_tbl["mk_product_style"];
	$productTable=$sql_tbl["products"];
	$styleimages =$sql_tbl["mk_style_images"];
	if($priceForPOS == 1)
	{
		$queryString = "select a.id, a.product_type, c.image_path as image_i, a.name, a.description, mk_shop_masters_catalog.store_rate as price from $productStyleTable a,$productTable b , $styleimages c, mk_shop_masters_catalog where
					a.product_type = $productTypeId and a.is_active=1 and b.productid=$productid and a.iventory_count > 0 and a.id != 25 and a.styletype='P'
					and a.id=c.styleid and c.type='I' and c.isdefault='Y' and mk_shop_masters_catalog.product_style_id = a.id and mk_shop_masters_catalog.shop_master_id=$shopMasterId and mk_shop_masters_catalog.enable=1 order by a.id desc ";
	}
	else
	$queryString = "select a.id, a.product_type, c.image_path as image_i, a.name, a.description, FORMAT(a.price+IFNULL(a.price*b.markup/100,0),2) as price from $productStyleTable a,$productTable b , $styleimages c where
					a.product_type = $productTypeId and a.is_active=1 and b.productid=$productid and a.iventory_count > 0 and a.id != 25 and a.styletype='P'
					and a.id=c.styleid and c.type='I' and c.isdefault='Y' order by a.id desc ";
	$result_details = func_query($queryString);
	return  $result_details;
}

/*************************************************************************/
function func_load_product_customized_area_rel_details($productId,$areaid=0)
{
	global $sql_tbl;
	$productTable=$sql_tbl["products"];
	$productCustomizedAreaRel = $sql_tbl['mk_xcart_product_customized_area_rel'];
	$queryString      ="select c.area_id, c.image_name, p.is_customizable, p.image_portal_T, c.style_id, c.thumb_image, d.label as area_name,c.bgimageprw,c.text
                             from $productTable as p,
                             $productCustomizedAreaRel as c,
                             $sql_tbl[mk_style_customization_area] as d
	     where p.productid= c.product_id and d.id=c.area_id and c.product_id  = '$productId' ";

                             if($area_id != 0){
                             	$queryString .="	and area_id = '$areaid'";
                             }

                             $queryString	.= " order by c.area_id ";

                             $result          =   func_query($queryString);

                             return $result;
}

/*************************************************************************/

// function to get all the tags

function func_get_category_tags($catid)
{
	global $sql_tbl;
	$tb_tags           =$sql_tbl["mk_tags"];
	//$tb_cat_map   =$sql_tbl["mk_category_tag_mapping"];
	$tb_cat_count  =$sql_tbl["mk_tag_category_count"];

	$query  ="select  tagname, t.tagid, tc.categoryid
						  FROM  $tb_tags as t INNER JOIN $tb_cat_count as tc
						  ON t.tagid =  tc.tagid
						  WHERE tc.categoryid = '$catid' AND t.approved = 'Y' order by tc.tagcount desc, rand() limit 40 ";

	$result  =db_query($query);

	$products=array();

	if ($result)
	{
		$i=0;

		while ($row=db_fetch_row($result))
		{
			$products[$i] = $row;
			$i++;
		}
	}

	return $products;


}

// function to get all the products tags

function func_get_product_tags($prodtypeid)
{

	global $sql_tbl;
	$tb_tags           =$sql_tbl["mk_tags"];
	$tb_prod_count  =$sql_tbl["tag_type_count"];

	$query  ="select  t.tagname, t.tagid, tc.product_type_id, tc.tagcount,t.count
						  FROM  $tb_tags as t INNER JOIN $tb_prod_count as tc
						  ON t.tagid =  tc.tagid
						  WHERE tc.product_type_id = '$prodtypeid' AND t.approved = 'Y' order by (t.count*tc.tagcount) desc limit 40 ";
	/*$query = "select  t.tagname, t.tagid, tc.product_type_id, tc.tagcount,t.count
	 FROM  mk_tags as t INNER JOIN mk_tag_product_type_count as tc
	 ON t.tagid =  tc.tagid
	 WHERE tc.product_type_id = '$prodtypeid'
	 AND t.approved = 'Y'
	 group by tc.tagcount
	 order by t.tagname asc
	 limit 0,40 ";*/

	$result  =db_query($query);

	$products=array();

	if ($result)
	{
		$i=0;

		while ($row=db_fetch_row($result))
		{
			$products[$i] = $row;
			$i++;
		}
	}

	return $products;
}

/***************************************************************/

function func_tags_to_id($tags){
	$results = db_query("select tagid from mk_tags where tagname='".$tags."'  " );
	$row=db_fetch_array($results);
	$tagid = $row['tagid'];
	return $tagid ;

}

function func_id_to_tags($tagid){
	$results = db_query("select tagname from mk_tags where tagid='".trim($tagid)."'  " );
	$row=db_fetch_array($results);
	$tagname = $row['tagname'];
	return $tagname ;

}
function func_pid_to_pname($pid){
	$results = db_query("select name from mk_product_type where id='".$pid."'  " );
	$row=db_fetch_array($results);
	$pname = $row['name'];
	return $pname ;

}

function func_pname_to_pid($pname){
	$results = db_query("select id from mk_product_type where name='".trim($pname)."'  " );
	$row=db_fetch_array($results);
	$pid = $row['id'];

	return $pid ;

}
function func_sort_string($tags,$rel_tags){
	$arr_tags = array();
	$arr_tags = explode("/",$tags);
	$arr_tags[count($arr_tags)] = $rel_tags ;
	sort($arr_tags);
	$sort_tags = implode("/",$arr_tags);
	$sort_tags = trim($sort_tags,"/ ");
	return $sort_tags ;
}

function func_tags_ids($arr_tags_str){
	$arr_tags = array();
	$arr_tagids = array();

	$arr_tags = explode("/",$arr_tags_str);
	foreach($arr_tags as $arr_tag){
		$arr_tagids[] = func_tags_to_id($arr_tag);
	}
	$str_tagids = implode("/", $arr_tagids);
	$str_tagids = trim($str_tagids,"/");
	return $str_tagids ;


}

//function to get the font names and details
function func_getFontDetails($limit, $offset) {
	global $sql_tbl;
	$fontTable=$sql_tbl["mk_fonts"];

	$query       ="SELECT name, image_name FROM $fontTable LIMIT $limit OFFSET $offset";


	$result      =db_query($query);

	$fonts=array();

	if ($result)
	{
		$i=0;

		while ($row=db_fetch_row($result))
		{
			$fonts[$i] = $row;
			$i++;
		}
	}

	return $fonts;
}
//function to get the number of fonts
function func_getFontNos() {
	global $sql_tbl;
	$fontTable=$sql_tbl["mk_fonts"];

	$query       ="SELECT COUNT(*) FROM $fontTable";

	$result                 =db_query($query);

	if ($result)
	{
		$row=db_fetch_row($result);
		return $row[0];
	}

	return 0;
}

/**************************************************************************/
/*      COD Related Function  */

/**************************************************************************/

/* Function to check if a zipcode is COD servicable. (Cash on Delivery Feature)
 * In this, we check purely for zipcodes servicable by Blue Dart.
 * The COD Spec doc created at this time removes dependency on Aramex.
 */
function func_check_cod_updaid_order($login)
{
	// We are making use of mk_cod_serviceable_zipcode, which currently stores the COD serviceable zipcodes of bluedart.
   $sql="select count(*) as count from xcart_orders where cod_pay_status = 'unpaid' and login='$login'";
   $res=func_query_first_cell($sql);

   if($res==0) return false;
   else return true;
}


/* Function to do stringent checks on mobile no. for COD orders
 * We perform various checks, to see if the same mobile no. is used in previous unpaid orders.
 */
function func_cod_check_mobile_number_fraud($mobile) {
	/* For COD fraud checks, we check if the mobile no. on which user wants to send sms verification codes, is matched with following criteria
	 * 1. If that mobile no. was earlier used, as part of shipping info, for any order which is marked as UNPAID (Means.. returned back to myntra by bluedart)
	 * 2. If that moblie no. was used in such unpaid orders, and that mobile was entered as alternate no. on which sms is to be sent
	 * 
	 * Basically, it boils down to checking if the mobile being check for, lies in one of these columns, for unpaid cod orders
	 * 1. 'mobile' column in xcart_orders
	 * 2. 'contact_number' column in cash_on_delivery_info
	 */
	
	$sqlquery1 = "select count(*) from xcart_orders where  cod_pay_status = 'unpaid' and  payment_method='cod' and ";
	$sqlquery1.= "mobile='$mobile'";
	$result1=func_query_first_cell($sqlquery1);
	
	if($result1>0) 
		return true;
	
	$sqlquery2 = "select count(*) from cash_on_delivery_info where contact_number='$mobile' and " ;
	$sqlquery2.= "order_id in (select orderid from xcart_orders where cod_pay_status = 'unpaid' and payment_method='cod' )";
	
	$result2=func_query_first_cell($sqlquery2);
	
	if($result2>0) 
		return true;
		
	return false;
}

/* Function to calculate the total amount, on COD pending amount orders, which a user has to pay.
 */
function func_cod_pending_amount($login) {

	/* All information for computing the total amount to pay for each order is obtained by the following calculation :
	 * 1. 'Subtotal' column has the amount of the products, without considering any discount
	 * 2. Total = Subtotal - discount - coupon_discount     (So Total is amount for the products after all discounts)
	 * 3. Customer Pays :  Total + shipping_cost + gift_charges
	 * 4. So in our query to find total pending amount, we sum the values in Point 3, for all cod orders in pending status
	 */
	
	$sqlquery = "select sum(total) from xcart_orders where cod_pay_status='pending' and payment_method='cod' and status IN('Q', 'WP', 'OH', 'SH') and login='$login'";
	
	$result = func_query_first_cell($sqlquery);
	if(empty($result)) // In case user is placing a cod order for the first time, the $result will hold null value.
		return 0;

	return $result;
}

function getGiftingCharge($productsInCart) {
	return 20;
}

function func_calculate_shipping_charges($cartSubtotal) {
	$system_free_shipping_amount = WidgetKeyValuePairs::getWidgetValueForKey('shipping.charges.cartlimit');
	$system_shipping_rate = WidgetKeyValuePairs::getWidgetValueForKey('shipping.charges.amount');
	if(round($cartSubtotal) < $system_free_shipping_amount) {
		$shippingCharges = $system_shipping_rate;
	} else {
		$shippingCharges = 0;
	}
	return $shippingCharges;
}

/************************************************************************/
/*Code by: Nikhil Gupta	                                                */
/*Purpose: Calculation of shipping rate                                 */
/************************************************************************/

function func_shipping_rate_re($s_country,$s_state,$s_city, $productsInCart, $productids, $coupon,$logisticId,$s_zipcode="", $amount = null)
{
	global $sql_tbl,$weblog,$sqllog;

	$tb_shipping  = $sql_tbl["mk_shipping"];
	$tb_shipping_prod_type  = $sql_tbl["shipping_prod_type"];

	# trim and replace spaces in city name with '-'

	$s_city = trim($s_city);
	$s_city = str_ireplace(" ","-",$s_city);

	$serviceable='N';
    $shipping_calculated=false;
		if(!empty($s_city) && !empty($s_state) && !empty($s_country)){
			$query  = "SELECT shiprate FROM $tb_shipping WHERE city='".mysql_escape_string($s_city)."' AND state='".mysql_escape_string($s_state)."' AND country='".mysql_escape_string($s_country)."'  AND logistic_provider = '".mysql_escape_string($logisticId)."'";
			$sqllog->debug($query);
			$shipRate = func_query_first($query);
			$weblog->info("Ship rate by Country State City combination : ".$shipRate['shiprate']);
            if(!empty($shipRate))
            $shipping_calculated=true;
		}
        if($s_country=='IN' && !$shipping_calculated){
            $s_city = 'restofindia';
            $query = "SELECT shiprate FROM $tb_shipping WHERE city='".mysql_escape_string($s_city)."' AND logistic_provider = '".mysql_escape_string($logisticId)."' ";
            $sqllog->debug($query);
            $shipRate = func_query_first($query);
            $weblog->info("Ship rate by country (India) and city  : ".$shipRate['shiprate']);
            if(!empty($shipRate))
            $shipping_calculated=true;
			}
        if(!$shipping_calculated){
        	$query = "SELECT shiprate FROM $tb_shipping WHERE country ='".mysql_escape_string($s_country)."' AND logistic_provider = '".mysql_escape_string($logisticId)."' ";
            $shipRate = func_query_first($query);
			$weblog->info("Ship rate by country : ".$shipRate['shiprate']);
        }
	$shippingRate = $shipRate['shiprate'];
	$totalshippingrate = 0;
	$typearray = array();
	########### check the free shipping status,in case user enter coupon code ###################
	$freeShippingStatus = 'N';
	if (($coupon != null) && ($s_country == "IN")) {
	    if ($coupon->isFreeShipping()) {
	        $freeShippingStatus = 'Y';
	    }
	}

	$productstr="";
	foreach($productsInCart AS $key=>$value) {
		$productDetail =  $value;
		$typeid = $productDetail['producTypeId'];
		$productstr=$productstr."product: $value , type : $typeid , quantity $productDetail[quantity]";

		###### Check for free shipping status
		if($freeShippingStatus == 'N'){
			if(array_key_exists($typeid, $typearray)) {
				$typearray[$typeid] =  $typearray[$typeid] + $productDetail['quantity'];
			}
			else {
				$typearray[$typeid] = $productDetail['quantity'];
			}
		}
		else {
			$typearray[$typeid] = 0;
		}
	}

	$box_count=0;
	foreach($typearray AS $type=>$quantity) {
		$cquery = func_query_first("SELECT consignment_capacity FROM $tb_shipping_prod_type WHERE product_type_id='".$type."'");
		$climit=$cquery[consignment_capacity];
		$totalunits = ceil($quantity/$climit);
		$box_count+=$totalunits;
		$shippingCost = $totalunits *  $shippingRate;
		$totalshippingrate = $totalshippingrate + $shippingCost;
	}

    global $system_free_shipping_amount, $system_shipping_rate;
    if( ($s_country == "IN" || $freeShippingStatus == 'Y') && intval($amount) >= $system_free_shipping_amount ){
        $totalshippingrate = 0 ;
    }else {
    	$totalshippingrate = $system_shipping_rate ;
    }/*
    else if($s_country != "IN") {
		//PORTAL-406, for international orders above MinOrderForFreeIntlShipping (in Rs), no shipping-rate, if FreeIntlShippingEnabled is true
		$allowFreeIntlShipping = FeatureGateKeyValuePairs::getBoolean('FreeIntlShipping.Enabled');
		$minOrderForFreeIntlShipping = FeatureGateKeyValuePairs::getInteger('FreeIntlShipping.MinOrder');
		if($allowFreeIntlShipping && $minOrderForFreeIntlShipping != -1 && $amount >= $minOrderForFreeIntlShipping) {
			$totalshippingrate = 0;
		} 
    }*/

	if($shippingRate!=0 && $totalshippingrate==0)
		$freeShippingStatus='Y';

	if($box_count==0)$box_count=1;
	$return_array = array("shiprate"=>$shippingRate,  "totalshippingamount" => $totalshippingrate,"freeShippingStatus" =>$freeShippingStatus,"box_count"=>$box_count);

	global $XCARTSESSID; //This is introduced for the shipping calculation log. Please remove this later.
	$weblog->info("SessionId: $XCARTSESSID s_country $s_country s_state $s_state s_city $s_city s_zipcode $s_zipcode logistic $logisticId");
	$weblog->info("SessionId: $XCARTSESSID Free Shipping status: $freeShippingStatus totalshippingamount: $totalshippingrate  shiprate: $shippingRate productsincart: $productstr ");

	return $return_array;
}
function func_shipping_rate($s_country,$s_state,$s_city, $productsInCart, $productids, $coupon,$logisticId,$s_zipcode="",$amount = null)
{
	global $sql_tbl,$weblog,$sqllog,$XCART_SESSION_VARS;
	
	$tb_shipping  = $sql_tbl["mk_shipping"];
	$tb_shipping_prod_type  = $sql_tbl["shipping_prod_type"];
	$orgId=$XCART_SESSION_VARS['orgId'];

	# trim and replace spaces in city name with '-'

	$s_city = trim($s_city);
	$s_city = str_ireplace(" ","-",$s_city);

	$serviceable='N';

	#
	#Load city id city is not null
	#

	if( $logisticId=="AR"){
		$sqllog->debug("SELECT shiprate FROM $tb_shipping WHERE zipcode='$s_zipcode' AND country='".$s_country."'  AND logistic_provider = '".$logisticId."'");
		$shipRate = func_query_first("SELECT shiprate FROM $tb_shipping WHERE zipcode='".$s_zipcode."' AND country='".$s_country."'  AND logistic_provider = '".$logisticId."' ");
		$weblog->info("Ship rate by Zipcode : ".$shipRate['shiprate']);
		$is_calculated_by_zipcode = true;
	}

	if($s_city!="" && $is_calculated_by_zipcode==false){
		$is_calculated_by_city = false;
		$sql = "SELECT city FROM $tb_shipping  WHERE identifier='1' AND logistic_provider = '".$logisticId."' ";
		$result = func_query($sql);
		$cityArray = array();
		$i=0;
		foreach ($result AS $key => $value ) {
			$cityArray[$i] = $value['city'];
			$i++;
		}

		if(in_array($s_city, $cityArray)){
			$sqllog->debug("SELECT shiprate FROM $tb_shipping WHERE city='".$s_city."' AND country='".$s_country."'  AND logistic_provider = '".$logisticId."'");
			$shipRate = func_query_first("SELECT shiprate FROM $tb_shipping WHERE city='".$s_city."' AND country='".$s_country."'  AND logistic_provider = '".$logisticId."' ");
			$weblog->info("Ship rate by city : ".$shipRate['shiprate']);
			$is_calculated_by_city = true;
		}else{
			if($s_country=='IN'){
				$s_city = 'restofindia';
				$sqllog->debug("SELECT shiprate FROM $tb_shipping WHERE city='".$s_city."' AND logistic_provider = '".$logisticId."' ");
				$shipRate = func_query_first("SELECT shiprate FROM $tb_shipping WHERE city='".$s_city."' AND logistic_provider = '".$logisticId."' ");
				$is_calculated_by_city = true;
			}
		}
	}


	#
	#Load state if state is not null and is_calculated_by_city is false
	#
	if($s_state!=""){
		if($is_calculated_by_city==false && $is_calculated_by_zipcode==false){

			$is_calculated_by_state = false;
			$sql = "SELECT state FROM $tb_shipping  WHERE identifier='2'";
			$result = func_query($sql);
			$stateArray = array();
			$i=0;
			foreach ($result AS $key => $value ) {
				$stateArray[$i] = $value['state'];
				$i++;
			}

			if(in_array($s_state, $stateArray)){
				$sqllog->debug("SELECT shiprate FROM $tb_shipping WHERE state ='".$s_state."' AND country='".$s_country."' AND logistic_provider = '" . $logisticId . "'");
				$shipRate = func_query_first("SELECT shiprate FROM $tb_shipping WHERE state ='".$s_state."' AND country='".$s_country."' AND logistic_provider = '" . $logisticId . "' ");
				$is_calculated_by_state = true;
				$weblog->info("Ship rate by state : ".$shipRate['shiprate']);
			}
		}
	}

	#
	#Load country if country is not null
	#
	if($s_country!=""){
		if($is_calculated_by_city==false && $is_calculated_by_state==false && $is_calculated_by_zipcode==false){
			$shipRate = func_query_first("SELECT shiprate FROM $tb_shipping WHERE country ='".$s_country."' AND logistic_provider = '".$logisticId."' ");
			$weblog->info("Ship rate by country : ".$shipRate['shiprate']);
		}
	}
	$shippingRate = $shipRate['shiprate'];


	$totalshippingrate = 0;
	$typearray = array();
	########### check the free shipping status,in case user enter coupon code ###################
	$freeShippingStatus = 'N';
	if ($coupon != null) {
	    if ($coupon->isFreeShipping()) {
	        $freeShippingStatus = 'Y';
	    }
	}

	$productstr="";
	foreach($productids AS $key=>$value)
	{
		$productDetail =   $productsInCart[$value];
		$typeid = $productDetail['producTypeId'];
		$productstr=$productstr."product: $value , type : $typeid , quantity $productDetail[quantity]";

		###### Check for free shipping status
		if($freeShippingStatus == 'N'){
			if(array_key_exists($typeid, $typearray))
			{
				$typearray[$typeid] =  $typearray[$typeid] + $productDetail['quantity'];
			}
			else
			{
				$typearray[$typeid] = $productDetail['quantity'];
			}
		}
		else
		{
			$typearray[$typeid] = 0;

		}
	}

	$box_count=0;
	foreach($typearray AS $type=>$quantity)
	{
		//calculate shipping for brandstore(for the time being only RRportal) deparately
		if($orgId == 25 || $orgId == 31)
			$cquery = func_query_first("SELECT consignment_capacity FROM mk_org_product_type_map WHERE orgid='{$orgId}' and product_type_id='{$type}'");
		else
			$cquery = func_query_first("SELECT consignment_capacity FROM $tb_shipping_prod_type WHERE product_type_id='".$type."'");
		$climit=$cquery[consignment_capacity];
		$totalunits = ceil($quantity/$climit);
		$box_count+=$totalunits;
		$shippingCost = $totalunits *  $shippingRate;
		$totalshippingrate = $totalshippingrate + $shippingCost;
	}
	
 	global $system_free_shipping_amount;
    if($s_country == "IN" && $amount >  $system_free_shipping_amount || $freeShippingStatus == 'Y'){
        $totalshippingrate = 0 ;
    }

    if($shippingRate!=0 && $totalshippingrate==0)
		$freeShippingStatus='Y';

	if($box_count==0)$box_count=1;
	$return_array = array("shiprate"=>$shippingRate,  "totalshippingamount" => $totalshippingrate,"freeShippingStatus" =>$freeShippingStatus,"box_count"=>$box_count);

	global $XCARTSESSID; //This is introduced for the shipping calculation log. Please remove this later.
	$weblog->info("SessionId: $XCARTSESSID s_country $s_country s_state $s_state s_city $s_city s_zipcode $s_zipcode logistic $logisticId");
	$weblog->info("SessionId: $XCARTSESSID Free Shipping status: $freeShippingStatus totalshippingamount: $totalshippingrate  shiprate: $shippingRate productsincart: $productstr ");

	return $return_array;
}

//Product IDs is the value stored in the SESSION
function func_determine_cod_enabled_re($productsInCart,$productids)
{
	global $sql_tbl,$weblog,$sqllog;

	/* Cash on Delivery Settings are now for Each product type, as well as style type
	 * In case, the cart contains even a single product, that is that Cash-On-Delivery able, then we return false
	 */
	$tb_prod_type  = $sql_tbl["mk_product_type"];

	$tb_prod_style  = $sql_tbl["mk_product_style"];
	
	foreach($productsInCart AS $key=>$value)
	{
		$productDetail = $value;
		
		// Note: the column name in cart is defined as 'producTypeId' and not 'productTypeId'.
		$typeid = $productDetail['productTypeId'];
		
		$styleid = $productDetail['productStyleId'];
		
		if(empty($typeid) && empty($styleid)) {
			// There is a known bug. We encountered a case, when we had 10 items in cart, all with Zero value. and no typeid set.
			continue;
		}
		if(!empty($typeid) && 'Y'!=func_query_first_cell("select cod_enabled from $tb_prod_type where id=$typeid"))
			return false;
			
		if(!empty($styleid) && 'Y'!=func_query_first_cell("select cod_enabled from $tb_prod_style where id=$styleid"))
			return false;
	}
	
	return true;
}

//Product IDs is the value stored in the SESSION
function func_determine_cod_enabled($productsInCart,$productids)
{
	global $sql_tbl,$weblog,$sqllog;
	
	$tb_prod_type  = $sql_tbl["mk_product_type"];

	foreach($productids AS $key=>$value)
	{
		$productDetail =   $productsInCart[$value];
		$typeid = $productDetail['producTypeId'];
		if('Y'!=func_query_first_cell("select cod_enabled from $tb_prod_type where id=$typeid"))
		return false;

	}
	return true;
}

function func_get_countries()
{
	//Load list of countries
	global $sql_tbl;
	//	$tb_countries = $sql_tbl[countries];
	$tb_languages = $sql_tbl[languages];

	$query = "SELECT value,name FROM $tb_languages where topic='Countries' and enable_status='1' order by value";
	$CountriesResult = db_query($query);

	$returnCountry = array();

	while($row=db_fetch_array($CountriesResult))
	{
		$countrycode = substr($row['name'],8);
		$returnCountry[$countrycode] = $row['value'];

	}

	return $returnCountry;

}


function func_get_states($country_code="")
{
	//Load states of the given country code
	global $sql_tbl;
	$tb_state = $sql_tbl[states];

	$country_code = ($country_code!="")?$country_code:'IN';

	$query = "SELECT state,code FROM $tb_state where country_code='". $country_code. "' ";
	$statesResult = db_query($query);
	$i = 0;
	while($row=db_fetch_array($statesResult))
	{
		$returnState[$i] = $row;
		$i++;
	}
	return $returnState;

}


function func_indian_state($stateCode='0')
{
	//Load states of india
	global $sql_tbl;
	$tb_state = $sql_tbl[states];
	/*echo "====>>>".$stateCode;
	 exit;*/
	if($stateCode == '0')
	{
		$query = "SELECT state, code FROM $tb_state WHERE country_code='IN'";
		$statesResult = db_query($query);
		$i = 0;
		while($row=db_fetch_array($statesResult))
		{
			$returnState[$i] = $row;
			$i++;
		}
		return $returnState;
	}
	else
	{
		$query = "SELECT state FROM $tb_state WHERE code='".$stateCode."'";
		$statesResult = db_query($query);
		$row=db_fetch_array($statesResult);

		return $row['state'];
	}


}
/**************************************************************************/

function get_product_tags($pid)
{
	global $sql_tbl;
	$tag_tbl = $sql_tbl['mk_tags'];
	$product_tag_tbl =$sql_tbl['mk_product_tag_map'];
	//$producttags = func_query("SELECT  t.tagname,t.tagid FROM  $tag_tbl as t INNER JOIN $product_tag_tbl as pt
	 // ON t.tagid = pt.tagid WHERE  pt.productid = '".$pid."' ");
	 $producttags = func_query("SELECT  t.tagname,st.key_id FROM  mk_tags as t,mk_product_tag_map as pt,mk_stats_search as st
	  WHERE   t.tagid = pt.tagid and pt.productid = '".$pid."' and st.orignal_keyword=t.tagname and st.is_valid='Y' and st.count_of_products >3 ");
	
	return    $producttags;
}
/***********************************/

// get all product data if csv of product ids are known
function getProductData($productsids,$offset="",$limit=""){
	global $sql_tbl;
	$tb_product           =$sql_tbl["products"];
	$producttype          =$sql_tbl["mk_product_type"];
	$styleTable  =$sql_tbl["mk_product_style"];
	$products = array();

	$query = "SELECT
						a.productid,
						a.product,
						a.image_portal_t,
						a.myntra_rating,
						a.image_portal_thumbnail_160,
						a.product_style_id,
						a.list_price,
						e.id,
						e.name,
						f.price,
						f.name,
						f.description,
						g.bkgroundimage as background,
						r.bgimageprw as image
						
				FROM
				$tb_product a , $producttype e, $styleTable f , mk_style_customization_area g, mk_xcart_product_customized_area_rel r

				WHERE 
						e.id = a.product_type_id
				and
						f.id  = a.product_style_id
				and
						a.statusid = 1008
				and
					    g.style_id = a.product_style_id
				and
				
						r.area_id = g.id

				and 
						g.style_id = r.style_id
				and             r.product_id=a.productid

				and 
						g.isprimary = 1
				and
						a.productid in (".$productsids.") ";

				/*AND (MATCH (a.keywords) AGAINST('%$tagname%') or MATCH(a.fulldescr) AGAINST('%$tagname%') or MATCH(a.product)AGAINST('%$tagname%')or (keywords LIKE '%$tagname%') )";*/



				$query = $query." order by a.myntra_rating desc ";

				if ($limit)
				{
					$query=$query . "  LIMIT $limit";
				}
					

				if ($offset && $limit)
				{
					$query=$query . " OFFSET $offset";
				}

				//exit;

				$result=db_query($query);
				$i=0;
				if ($result)
				{


					while ($row=db_fetch_row($result))
					{
						$products[$i] = $row;
						$i++;
					}

				}

				//Access popup image path

				$arrayKey = array_keys($products);

				$arraysize = count($arrayKey);
				for($i=0;$i<count($arrayKey);$i++){
					$prodarray = $arrayKey[$i];
					$productDetail = $products[$prodarray];
					$namelength = strlen($productDetail[1]);
					$productname = ($namelength >= '22')?substr($productDetail[1], 0, 22)." ...":$productDetail[1];
					$arraysize = count($products[$prodarray]);

					$popupQuery = "SELECT popup_image_path
					FROM $sql_tbl[mk_xcart_product_customized_area_rel] a, $sql_tbl[mk_style_customization_area] b, $sql_tbl[products] c
					WHERE a.product_id = '$productDetail[0]'
					AND a.product_id = c.productid
					AND a.style_id = b.style_id
					AND a.area_id = b.id
					AND b.isprimary = 1
					AND c.statusid = 1008";
					$popupResult = db_query($popupQuery);
					$popupRow = db_fetch_array($popupResult);
					$popupImage =  $popupRow['popup_image_path'];
					$products[$prodarray][$arraysize] = $popupImage;
					$products[$prodarray]['prodname'] = $productname;

				}


				return $products;

}

#
#Get label for courier_provider
#
function get_courier_provider_label($courier_provider_id){
	global $sql_tbl;
	$sql = "SELECT courier_service FROM {$sql_tbl['courier_service']} where code='".$courier_provider_id."'";
	$courier_provider_label = func_query_first_cell($sql);

	return $courier_provider_label;

}

function calculateVat($productsInCart,$productids,$couponCode)
{
	include_once("./include/func/func.mkcore.php");
	include_once("./include/func/func.db.php");
	include_once("./include/func/func.mkmisc.php");
	include_once("./include/func/func.mkspecialoffer.php");
	include_once("./mkdiscountrules.php");
	include_once("./include/func/func.affiliates.php");
	include_once("include/func/func.shop.php");
	include($xcart_dir."/include/func/func.orginterface.php");

	$grandTotal = 0;
	$totaldiscount = 0;
	$subTotal = 0;

	$optionCounter = array();
	$tb_product_category = $sql_tbl["products_categories"];
	//Computing grand total for cart for the session
	$i = 0;
	$ratioArray = array();
	foreach($productids AS $key=>$value)
	{
		$optionCount = count($productsInCart[$value]['optionNames']);
		$productsInCart[$value]['optionCount'] = $optionCount;
		$productDetail =   $productsInCart[$value];
		//$tid = func_query_first_cell("select product_type_id from xcart_products where productid = '$value'");
		$tid=$productDetail["producTypeId"];
		$vatRate = func_query_first_cell("select vat from mk_product_type where id='$tid'");

		$subTotal = $subTotal + $productDetail['totalPrice'];
		if($productsInCart[$value]['updateddiscount'])
		{
			$grandTotal = $grandTotal + ( $productDetail['totalPrice'] - $productDetail['updateddiscount']) ;
			$totaldiscount = $totaldiscount + $productDetail['updateddiscount'];
			$grandTotalTax = $grandTotalTax+($vatRate*( $productDetail['totalPrice'] - $productDetail['updateddiscount']))/100;
		}
		else
		{
			$grandTotal = $grandTotal + ($productDetail['totalPrice'] - $productDetail['discount']) ;
			$totaldiscount = $totaldiscount + $productDetail['discount'];
			$grandTotalTax = $grandTotalTax+($vatRate*( $productDetail['totalPrice'] - $productDetail['discount']))/100;
		}


		$ratioArray[$i] = ($productDetail['totalPrice']*$vatRate)/100;
		$ratioArrayTotal = $ratioArrayTotal+$ratioArray[$i];
		$i++;
	}

	$grandTotalForTaxCalculation = $grandTotal;

	//Adding Gift certifcates to the grand total
	include_once("include/func/func.shop.php");
	$giftcerts = $cart["giftcerts"];

	$count =count($giftcerts);
	$certificate;
	$gc2cart= $XCART_SESSION_VARS['gc2cart'];


	if($gc2cart =="true")
	{
		foreach($giftcerts AS $key=>$value)
		{
			$certificate = $cart["giftcerts"][$key];
			$subTotal = $subTotal + $certificate["totalamount"];
			$grandTotal = $grandTotal + $certificate["totalamount"];
		}

	}
	include_once("./include/func/func.mkspecialoffer.php");
	$discountOnTotal = func_get_special_offer_discount_for_Total($grandTotal);
	$grandTotal = $grandTotal - $discountOnTotal;

	$coupondiscount = 0;
	$XCART_SESSION_VARS['couponCode'];
	if($XCART_SESSION_VARS['couponCode'] != NULL && $XCART_SESSION_VARS['couponCode'] != "")
	{
		$couponCode = $XCART_SESSION_VARS['couponCode'];
		$coupondiscount = $XCART_SESSION_VARS['coupondiscount'];
		$couponpercent = $XCART_SESSION_VARS['couponpercent'];
	}

	/*$c=strip_tags($c);
	 $c=sanitize_int($c);
	 $couponRedemption=$c;
	 if ($couponRedemption == '1')
	 {*/

	//}
	$totaldiscountonoffer = $discountOnTotal + $totaldiscount;
	if(!x_session_is_registered('totaldiscountonoffer'))
	{
		x_session_register('totaldiscountonoffer');
	}
	$XCART_SESSION_VARS['totaldiscountonoffer'] = $totaldiscountonoffer;

	$amt = $subTotal - $totaldiscountonoffer;
	$subTotal = $amt;
	$amountAfterDiscount = $subTotal - $coupondiscount;
	if($amountAfterDiscount < 0)
	$amountAfterDiscount =0;

	$totalAfterDiscount = $amountAfterDiscount;
	$totalCouponAndDiscountOnTotal = $coupondiscount+$discountOnTotal;

	if ($amountAfterDiscount > 0)
	$vatCst = $grandTotalTax-(($ratioArrayTotal*$totalCouponAndDiscountOnTotal)/$grandTotalForTaxCalculation);
	else
	$vatCst = 0;
	return number_format($vatCst,2,".",'');
}

function calculateVatWithoutDiscount($productsInCart,$productids,$couponCode)
{
	include_once("./include/func/func.mkcore.php");
	include_once("./include/func/func.db.php");
	include_once("./include/func/func.mkmisc.php");
	include_once("./include/func/func.mkspecialoffer.php");
	include_once("./mkdiscountrules.php");
	include_once("./include/func/func.affiliates.php");
	include_once("include/func/func.shop.php");
	include($xcart_dir."/include/func/func.orginterface.php");

	$grandTotal = 0;
	$totaldiscount = 0;
	$subTotal = 0;

	$optionCounter = array();
	$tb_product_category = $sql_tbl["products_categories"];
	//Computing grand total for cart for the session
	$i = 0;
	$ratioArray = array();
	foreach($productids AS $key=>$value)
	{
		$optionCount = count($productsInCart[$value]['optionNames']);
		$productsInCart[$value]['optionCount'] = $optionCount;
		$productDetail =   $productsInCart[$value];
		//$tid = func_query_first_cell("select product_type_id from xcart_products where productid = '$value'");
		$tid=$productDetail["producTypeId"];
		$vatRate = func_query_first_cell("select vat from mk_product_type where id='$tid'");

		$subTotal = $subTotal + $productDetail['totalPrice'];
		$grandTotal = $grandTotal + ($productDetail['totalPrice'] ) ;
		$grandTotalTax = $grandTotalTax+($vatRate*( $productDetail['totalPrice'] ))/100;

		$ratioArray[$i] = ($productDetail['totalPrice']*$vatRate)/100;
		$ratioArrayTotal = $ratioArrayTotal+$ratioArray[$i];
		$i++;
	}

	$grandTotalForTaxCalculation = $grandTotal;

	//Adding Gift certifcates to the grand total
	include_once("include/func/func.shop.php");
	$giftcerts = $cart["giftcerts"];

	$count =count($giftcerts);
	$gc2cart= $XCART_SESSION_VARS['gc2cart'];


	if($gc2cart =="true")
	{
		foreach($giftcerts AS $key=>$value)
		{
			$certificate = $cart["giftcerts"][$key];
			$subTotal = $subTotal + $certificate["totalamount"];
			$grandTotal = $grandTotal + $certificate["totalamount"];
		}

	}
	include_once("./include/func/func.mkspecialoffer.php");
	$discountOnTotal = func_get_special_offer_discount_for_Total($grandTotal);
	$grandTotal = $grandTotal - $discountOnTotal;

	$coupondiscount = 0;


	$amt = $subTotal - $totaldiscountonoffer;
	$subTotal = $amt;
	$amountAfterDiscount = $subTotal - $coupondiscount;
	if($amountAfterDiscount < 0)
	$amountAfterDiscount =0;

	$totalAfterDiscount = $amountAfterDiscount;
	$totalCouponAndDiscountOnTotal = $coupondiscount+$discountOnTotal;

	if ($amountAfterDiscount > 0)
	$vatCst = $grandTotalTax-(($ratioArrayTotal*$totalCouponAndDiscountOnTotal)/$grandTotalForTaxCalculation);
	else
	$vatCst = 0;
	return number_format($vatCst,2,".",'');
}

function get_style_addon_name($addonid)
{
	return "";
}
//function to check whether old customization will be merged with new customization or not 
function isOldCustMergableWithNewCust($oldChosenProductConfiguration, $thisChosenProductConfiguration, $oldCustomizationArray, $thisCustomizationArray)
{
        $areanames=array();
        $orientationnames=array();
        $areanamesnew=array();
        $orientationnamesnew=array();
        if( $oldChosenProductConfiguration["productTypeGroupId"] == $thisChosenProductConfiguration["productTypeGroupId"] )
        {
                foreach($oldCustomizationArray["areas"] as $key=>$area)
                {
                        $areanames[] = $key;
                        foreach( $area['orientations'] as $key=>$individualorientation )
                                $orientationnames[] = $key;
                }
                foreach($thisCustomizationArray["areas"] as $key=>$area)
                {
                        $areanamesnew[]=$key;
                        foreach( $area['orientations'] as $key=>$individualorientation )
                                $orientationnamesnew[] = $key;
                }
                if( (count($areanames) == count($areanamesnew)) && !array_diff($areanames,$areanamesnew) )
                {
                        if( (count($orientationnames) == count($orientationnamesnew)) && !array_diff($orientationnames, $orientationnamesnew) )
                                return true;
                        else
                                return false;
                }
                else
                        return false;
        }
        else
                return false;
}

function get_next_column_name($alphabets,$num){
	$colname = "";
	$num = $num+1;
	while($num != 0){
		$num = $num -1;
		$rem = intval($num % 26);
		$num = intval($num/26);
		$colname = $alphabets[$rem].$colname;
	}	
	return $colname;
}

function func_write_data_to_excel($objPHPExcel, $xls_data, $sheetindex=0, $sheetname=false, $multiCol=true, $colName='', $allstringcol=false) {
	global $weblog, $xcart_dir;
	require_once $xcart_dir."/PHPExcel/PHPExcel.php";
	if($sheetindex != 0) {
		$objPHPExcel->createSheet();
	}
	$objPHPExcel->setActiveSheetIndex($sheetindex);
	if($sheetname)
		$objPHPExcel->getActiveSheet()->setTitle($sheetname);
	if($multiCol) {	
		$alphabets = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
		$col_headers = array_keys($xls_data[0]);
		$col_num = 0;
		foreach($col_headers as $col_num=>$header) {
			$colname = get_next_column_name($alphabets,$col_num);
			$objPHPExcel->getActiveSheet()->SetCellValue($colname."1", $header);
			$col_num++;
		}
		$row_num = 2;
		foreach ($xls_data as $return) {
			$col_num = 0;
			foreach($return as $key=>$value) {
				$colname = get_next_column_name($alphabets,$col_num);
				if($allstringcol == true){
					$objPHPExcel->getActiveSheet()->setCellValueExplicit($colname.$row_num, "$value", PHPExcel_Cell_DataType::TYPE_STRING);
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue($colname.$row_num, "$value");
				}
				$col_num++;
			}
			$row_num++;
		}
	} else {
		$objPHPExcel->getActiveSheet()->SetCellValue("A1", $colName);
		$row_num = 2;
		foreach ($xls_data as $value) {
			if($allstringcol == true){
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("A".$row_num, $value, PHPExcel_Cell_DataType::TYPE_STRING);
			}else{
				$objPHPExcel->getActiveSheet()->SetCellValue("A".$row_num, $value);
			}
			$row_num++;
		}
	}
	
	return $objPHPExcel;
}




function indian_rs_format_simple($number, $decimals=0) {
	$number = round($number,$decimals);
	$format=number_format($number, 0, '.', ',');
	$strsplit=split(',',$format);
	$out=$strsplit[count($strsplit)-1];
	$icount=0;
	for($i=count($strsplit)-2;$i>=0;$i--){
		for($j=strlen($strsplit[$i])-1;$j>=0;$j--,$icount++){
			if($icount%2==0){
				$out=','.$out;
			}
			$out=$strsplit[$i][$j].$out;

		}
	}
	return $out;
}

function verifyCODElegiblityForUser($login,$mobile,$country,$totalAmount,$zipcode,$productsInCart,$productids,$cashBackRedeemed=0,$jewelleryItemsInOrder='none',$giftWrapOrder=false, $paymentMethods = false){
	$codAllZipCodeEnable = FeatureGateKeyValuePairs::getBoolean('codEnableAllZipCode');
	$codAmountRange = trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('cod.limit.range'));	
	$codAmountRangeArray = explode("-", $codAmountRange);
	$codMinVal = $codAmountRangeArray[0];
	$codMaxVal = $codAmountRangeArray[1];
	$codErrorCode = 0;
	$codErrorMessage = '';
	if($giftWrapOrder){
		$codErrorCode = 9;
		$codErrorMessage = "Cash on Delivery not available for gift orders.";
	}else if($country!='IN') {
		$codErrorCode = 7;
		$codErrorMessage = "COD is applicable only for India";
	} else if( ($totalAmount+$cashBackRedeemed) < $codMinVal || $totalAmount>$codMaxVal) {
		// If amount is not between 300-10000 Rs., then COD option should not be selected.
		// Upper limit of Rs. 10000/- is set as a security measure for upper limit on COD order.
		$codErrorCode = 1;
		$codErrorMessage = "Cash on Delivery is not available.<br/><br/>";
		$codErrorMessage.= "Cash on Delivery is currently available for order values between Rs. $codMinVal to Rs. $codMaxVal only";
	} else if( ($paymentMethods && !in_array('cod', $paymentMethods)) || ($paymentMethods===false && !func_check_cod_serviceability_products($zipcode, $productsInCart, $jewelleryItemsInOrder))) {
		// Check if the zipcode is servicable by  we do the zip code serviceable check.
		$codErrorCode = 2;
		$codErrorMessage = "Cash on Delivery is not available for your Pincode.";
		
	} else if(!func_determine_cod_enabled_re($productsInCart,$productids)) {
		// Check if all products in the cart can be placed using COD.
		$codErrorCode = 3;
		$codErrorMessage = "Cash on Delivery is not available<br/><br/>";
		$codErrorMessage.= "There are some items in your cart, for which Cash on Delivery option is currently not available.";
		
	} else if(func_check_cod_updaid_order($login)) {
		// For loginid fraud check, we see if the customer has any cod order, which was sent back. 
		// Basically, customer refused to accept the order which they placed online using COD, and then did not pay bluedart for it.
		$codErrorCode = 4;
		$codErrorMessage = "Cash on Delivery is not available for your account<br/><br/>";
		$codErrorMessage.= "There is an outstanding payment against your account.<br/><br/>"; 
	
	} else if(func_cod_check_mobile_number_fraud($mobile)) {
		// For mobile fraud check, we see if user has use a mobile no., which was earlier used for any unpaid order (by any loginid)
		$codErrorCode = 5;
		$codErrorMessage = "Cash on Delivery is not available for your account<br/><br/>";
		$codErrorMessage.= "There are  your account.<br/><br/>"; 
	} else if (!is_cod_enabled_for_user($login)){
		$codErrorCode = 7;
		$codErrorMessage = "Due to some issues with your past orders, Cash on Delivery option has been disabled for your account.<br/><br/>";
	}
	else {
		$codpendingamount = func_cod_pending_amount($login);
		
		$codCreditBalance = $codMaxVal - $codpendingamount;
		if($codCreditBalance < 0)
			$codCreditBalance = 0;
			
		if(($totalAmount + $codpendingamount) > $codMaxVal) {
			// User has placed some order currently, and that amount summed with previous COD pending payments, has exceeded codMaxVal.
			$codErrorCode = 6;
			$codErrorMessage = "Cash on Delivery is Not available as you have insufficient COD Credit<br/><br/>";
			$codErrorMessage.= "You have placed orders using COD option in the recent past and there are outstanding payment(s) due for the same.<br/><br/>"; 
			$codErrorMessage.= "Please order within available credit limit of Rs. ".indian_rs_format_simple($codCreditBalance)." or use alternative payment modes.<br/><br/>";
			$codErrorMessage.= "Your credit will be extended again once you make the outstanding payments.";
		}
		
	}
	
	return array("errorCode" => $codErrorCode, "errorMessage" =>$codErrorMessage);
	
}


function func_get_available_inventory_count($style_id,$size_name,$skuid = 0){
	global $xcart_dir, $login;
	$__MAX_AVAILABILITY=9999;
	
	$available_count = 0;
	$sku_result = SkuApiClient::getSkuDetailsWithInvCountsForIds($skuid);
	$sku = $sku_result[0];
	
	global $weblog;
	if($sku['jitSourced'] == 1) {
		$available_count = $__MAX_AVAILABILITY;
	} else {
		$available_count = $sku['invCount'];
	}
	return ($available_count>0?$available_count:0);
}

function func_is_style_instock($style_id) {
    if (!empty($style_id)) {
        $style_options = func_load_all_product_options($style_id);
        foreach($style_options as $id => $option) {
            if($option['sku_count'] > 0) {
                return true;
            }
        }
    } 

    return false;
}

?>
